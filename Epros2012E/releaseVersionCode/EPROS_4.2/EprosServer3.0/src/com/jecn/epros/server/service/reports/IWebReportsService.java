package com.jecn.epros.server.service.reports;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.temp.TempApproveTimeHeadersBean;
import com.jecn.epros.server.bean.process.temp.TempTaskApproveTime;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.AgingProcessBean;
import com.jecn.epros.server.webBean.reports.ProcessAccessBean;
import com.jecn.epros.server.webBean.reports.ProcessActivitiesBean;
import com.jecn.epros.server.webBean.reports.ProcessAnalysisBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationDetailsBean;
import com.jecn.epros.server.webBean.reports.ProcessConstructionBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.server.webBean.reports.ProcessInputOutputBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIFollowBean;
import com.jecn.epros.server.webBean.reports.ProcessRoleBean;
import com.jecn.epros.server.webBean.reports.ProcessScanOptimizeBean;

public interface IWebReportsService extends IBaseService<JecnFlowStructureT, Long> {

	/**
	 * 查询流程时效性统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            . 流程ID
	 * @param time
	 *            1,2，，，，12 代表选取的 12个月中 一个
	 * @param updateType
	 *            0全部 ， 1未更新 ， 2已更新
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> findAgingProcess(long resOrgId, long processId, long time, int updateType,
			long projectId, int start, int limit) throws Exception;

	/**
	 * 查询流程时效性统计的分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param time
	 *            1,2，，，，12 代表选取的 12个月中 一个
	 * @param updateType
	 *            0全部 ， 1未更新 ， 2已更新
	 * @return
	 * @throws Exception
	 */
	public int findAgingProcessCount(long resOrgId, long processId, long time, int updateType, long projectId)
			throws Exception;

	/**
	 * 查询流程时效性统计（饼状图）
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param time
	 *            1,2，，，，12 代表选取的 12个月中 一个
	 * @param updateType
	 *            0全部 ， 1未更新 ， 2已更新
	 * @return
	 * @throws Exception
	 */
	public AgingProcessBean findAgingProcessImage(long resOrgId, long processId, long time, int updateType,
			long projectId) throws Exception;

	/**
	 * 查询全部的流程时效性统计(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param time
	 *            1,2，，，，12 代表选取的 12个月中 一个
	 * @param updateType
	 *            0全部 ， 1未更新 ， 2已更新
	 * @return
	 * @throws Exception
	 */
	public AgingProcessBean findAllAgingProcess(long resOrgId, long processId, long time, int updateType, long projectId)
			throws Exception;

	/**
	 * 查询流程建设度
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param constructionType
	 *            建设状态 0是全部 ，1是审批中 2是已发布 3是待建
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> findProcessConstruction(long resOrgId, long processId, int constructionType,
			long projectId, int start, int limit) throws Exception;

	/**
	 * 查询流程建设度分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param constructionType
	 *            建设状态 0是全部 ，1是审批中 2是已发布 3是待建
	 * @return
	 * @throws Exception
	 */
	public int findProcessConstructionCount(long resOrgId, long processId, int constructionType, long projectId)
			throws Exception;

	/**
	 * 查询流程建设度（饼状图）
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param constructionType
	 *            建设状态 0是全部 ，1是审批中 2是已发布 3是待建
	 */
	public ProcessConstructionBean findProcessConstructionImage(long resOrgId, long processId, int constructionType,
			long projectId) throws Exception;

	/**
	 * 查询全部的流程建设度(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param constructionType
	 *            建设状态 0是全部 ，1是审批中 2是已发布 3是待建
	 */
	public ProcessConstructionBean findAllProcessConstruction(long resOrgId, long processId, int constructionType,
			long projectId) throws Exception;

	/**
	 * 查询流程角色数统计(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessRoleBean> findAllProcessRole(long processId, long projectId) throws Exception;

	/**
	 * 查询流程角色数统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessRoleBean> findProcessRole(long processId, long projectId) throws Exception;

	/**
	 * 查询流程角色数统计详情
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param typeNumber
	 *            角色数 1~16
	 * @param projectId
	 *            项目ID
	 * @param start
	 *            开始页
	 * @param endNum
	 *            结束页
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> findProcessRoleDetails(long processId, int typeNumber, long projectId, int start,
			int limit) throws Exception;

	/**
	 * 查询流程角色数统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param typeNumber
	 *            角色数
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public int findProcessRoleCount(long processId, int typeNumber, long projectId) throws Exception;

	/**
	 * 查询流程活动数统计（excel下载）
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessActivitiesBean> findAllProcessActivities(long processId, long projectId) throws Exception;

	/**
	 * 查询流程活动数统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessActivitiesBean> findProcessActivities(long processId, long projectId) throws Exception;

	/**
	 * 查询流程活动数详情统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param startNum
	 *            开始个数
	 * @param endNum
	 *            结束个数
	 * @param projectId
	 *            项目ID
	 * @param start
	 *            开始页
	 * @param endNum
	 *            结束页
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> findProcessActivitiesDetails(long processId, int startNum, int endNum, long projectId,
			int start, int limit) throws Exception;

	/**
	 * 查询流程活动数统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param startNum
	 *            开始个数
	 * @param endNum
	 *            结束个数
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public int findProcessActivitiesCount(long processId, int startNum, int endNum, long projectId) throws Exception;

	/**
	 * 流程应用人数统计(详情)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @param projectId
	 *            项目ID
	 * @param start
	 *            开始页
	 * @param endNum
	 *            结束页
	 * @return
	 * @throws Exception
	 */
	public ProcessApplicationDetailsBean findProcessApplicationDetails(long processId) throws Exception;

	/**
	 * 流程应用人数 Excel
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessApplicationDetailsBean> findProcessApplicationDetails(long processId, long resOrgId,
			long projectId) throws Exception;

	/**
	 * 流程应用人数统计（分页）
	 * 
	 * @author fuzhh Mar 6, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId责任部门ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessApplicationBean> findProcessApplication(long processId, long resOrgId, long projectId,
			int start, int limit) throws Exception;

	/**
	 * 流程应用人数统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @return
	 * @throws Exception
	 */
	public int findProcessApplicationCount(long processId, long resOrgId, long projectId) throws Exception;

	/**
	 * 流程输入输出统计(excle下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessInputOutputBean> findAllProcessInputOutput(long processId, long resOrgId, long projectId)
			throws Exception;

	/**
	 * 流程输入输出统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessInputOutputBean> findProcessInputOutput(long processId, long resOrgId, long projectId,
			int start, int limit) throws Exception;

	/**
	 * 流程输入输出统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @return
	 * @throws Exception
	 */
	public int findProcessInputOutputCount(long processId, long resOrgId, long projectId) throws Exception;

	/**
	 * 流程分析统计(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param timeType
	 *            时间类型 0是全部 ， 1是年，2是季，3是月，4是周，5是日
	 * @param processType
	 *            流程类别
	 * @param posId
	 *            岗位ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessAnalysisBean> findAllProcessAnalysisExcel(long processId, long timeType, long processType,
			long posId, long projectId) throws Exception;

	/**
	 * 流程分析统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param timeType
	 *            时间类型 0是全部 ， 1是年，2是季，3是月，4是周，5是日
	 * @param processType
	 *            流程类别
	 * @param posId
	 *            岗位ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessAnalysisBean> findAllProcessAnalysis(long processId, long timeType, long processType,
			long posId, long projectId) throws Exception;

	/**
	 * 流程分析统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param timeType
	 *            时间类型 0是全部 ， 1是年，2是季，3是月，4是周，5是日
	 * @param processType
	 *            流程类别
	 * @param posId
	 *            岗位ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> findProcessAnalysis(long processId, long timeType, long processType, long posId,
			long projectId, int start, int limit) throws Exception;

	/**
	 * 流程分析统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param timeType
	 *            时间类型 0是全部 ， 1是年，2是季，3是月，4是周，5是日
	 * @param processType
	 *            流程类别
	 * @param posId
	 *            岗位ID
	 * @return
	 * @throws Exception
	 */
	public int findProcessAnalysisCount(long processId, long timeType, long processType, long posId, long projectId)
			throws Exception;

	/**
	 * 流程访问数统计或流程下载数统计(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param type
	 *            0是流程访问数统计 1是流程下载数统计
	 * @return
	 * @throws Exception
	 */
	public List<ProcessAccessBean> findAllProcessAccess(long processId, long resOrgId, String startTime,
			String endTime, long projectId, int type) throws Exception;

	/**
	 * 流程访问数统计或流程下载数统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param type
	 *            0是流程访问数统计 1是流程下载数统计
	 * @return
	 * @throws Exception
	 */
	public List<ProcessAccessBean> findProcessAccess(long processId, long resOrgId, String startTime, String endTime,
			long projectId, int start, int limit, int type) throws Exception;

	/**
	 * 流程访问数统计或流程下载数统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param resOrgId
	 *            责任部门ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param type
	 *            0是流程访问数统计 1是流程下载数统计
	 * @return
	 * @throws Exception
	 */
	public int findProcessAccessCount(long processId, long resOrgId, String startTime, String endTime, long projectId,
			int type) throws Exception;

	/**
	 * 流程访问人统计或流程下载人统计(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param userId
	 *            访问人ID
	 * @param processId
	 *            流程ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param resOrgId
	 *            责任部门ID
	 * @param orgId
	 *            部门ID
	 * @param posId
	 *            岗位ID
	 * @param type
	 *            0是流程访问数统计 1是流程下载数统计
	 * @return
	 * @throws Exception
	 */
	public List<ProcessAccessBean> findAllProcessInterviewer(long userId, long processId, String startTime,
			String endTime, long resOrgId, long orgId, long posId, long projectId, int type) throws Exception;

	/**
	 * 流程访问人统计或流程下载人统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param userId
	 *            访问人ID
	 * @param processId
	 *            流程ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param resOrgId
	 *            责任部门ID
	 * @param orgId
	 *            部门ID
	 * @param posId
	 *            岗位ID
	 * @return
	 * @throws Exception
	 */
	public List<ProcessAccessBean> findProcessInterviewer(long userId, long processId, String startTime,
			String endTime, long resOrgId, long orgId, long posId, long projectId, int start, int limit, int type)
			throws Exception;

	/**
	 * 流程访问人统计或流程下载人统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param userId
	 *            访问人ID
	 * @param processId
	 *            流程ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param resOrgId
	 *            责任部门ID
	 * @param orgId
	 *            部门ID
	 * @param posId
	 *            岗位ID
	 * @return
	 * @throws Exception
	 */
	public int findProcessInterviewerCount(long userId, long processId, String startTime, String endTime,
			long resOrgId, long orgId, long posId, long projectId, int type) throws Exception;

	/**
	 * 分页查询流程文件审批时间统计
	 * 
	 * @author huoyl
	 * @param start
	 *            开始
	 * @param limit
	 *            分页大小
	 * @param taskType
	 *            任务类型
	 * @return
	 * @throws Exception
	 */
	public List<TempTaskApproveTime> findTaskApproveTime(int start, int limit, int taskType) throws Exception;

	/**
	 * 流程审批的总条数
	 * 
	 * @author huoyl
	 * @param taskType
	 *            任务类型
	 * 
	 * @return
	 * @throws Exception
	 */
	public int findTaskApproveTimeCount(int taskType) throws Exception;

	/**
	 * 根据任务类型统计审批时间
	 * 
	 * @author huoyl
	 * @param taskType
	 *            任务类型
	 * @return
	 * @throws Exception
	 */
	public List<TempTaskApproveTime> findAllTaskApproveTime(int taskType) throws Exception;

	/**
	 * 根据任务类型获得相应的表头用于Ext显示
	 * 
	 * @author huoyl
	 * @param taskType
	 *            任务类型
	 * @return
	 * @throws Exception
	 */
	public String findTaskApproveTimeHeader(int taskType) throws Exception;

	/**
	 * 根据任务类型获得任务内容和表头序列
	 * 
	 * @author huoyl
	 * @param taskType
	 *            任务类型
	 * @return
	 * @throws Exception
	 */
	public TempApproveTimeHeadersBean findApproveTimeHeadersBean(int taskType) throws Exception;

	/**
	 * 查询这些责任部门和流程地图下的流程清单
	 * 
	 * @param dutyOrgIds
	 *            责任部门ids 使用,分隔
	 * @param processMapIds
	 *            流程地图ids 使用,分隔
	 * @param endTime
	 *            结束时间
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public ProcessDetailBean findProcessDetailBean(Map<String, Object> param) throws Exception;

	/**
	 * 查询责任人下的流程的审视优化报表
	 * 
	 * @param dutyOrgIds责任部门ids
	 *            使用,分隔
	 * @param dutyOrgNames责任部门names
	 *            使用 \n分隔
	 * @param processMapIds
	 *            流程架构的ids 使用,分隔
	 * @param processMapNames
	 *            流程架构的names 使用 \n分隔
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param projectId
	 *            项目id
	 * @param type
	 * @return
	 */
	public ProcessScanOptimizeBean findProcessScanOptimizeBean(Map<String, Object> param) throws Exception;

	/**
	 * 查询KPI跟踪表
	 * 
	 * @param dutyOrgIds责任部门ids
	 *            使用,分隔
	 * @param processMapIds
	 *            流程架构的ids 使用,分隔
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param projectId
	 *            项目id
	 * @return
	 */
	public ProcessKPIFollowBean findProcessKPIFollowBean(String dutyOrgIds, String processMapIds, String startTime,
			String endTime, long projectId) throws Exception;

	/**
	 * 查询流程审视优化维护
	 * 
	 * @param dutyOrgIds
	 *            责任部门ids 使用,分隔
	 * @param processMapIds
	 *            流程架构/流程的ids 使用,分隔
	 * @param projectId
	 *            项目id
	 * @param type
	 * @return
	 */
	public ProcessExpiryMaintenanceBean findProcessExpiryMaintenanceBean(String dutyOrgIds, String processMapIds,
			long projectId, int type) throws Exception;

}
