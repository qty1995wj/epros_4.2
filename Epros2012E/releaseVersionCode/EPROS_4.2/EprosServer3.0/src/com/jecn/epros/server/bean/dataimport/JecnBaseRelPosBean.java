package com.jecn.epros.server.bean.dataimport;

public class JecnBaseRelPosBean {
    
	/**主键ID*/
	private String id;
	/**基准岗位主键ID*/
	private String basePosId;
	/**岗位ID*/
	private String posId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBasePosId() {
		return basePosId;
	}
	public void setBasePosId(String basePosId) {
		this.basePosId = basePosId;
	}
	public String getPosId() {
		return posId;
	}
	public void setPosId(String posId) {
		this.posId = posId;
	}
	
	
}
