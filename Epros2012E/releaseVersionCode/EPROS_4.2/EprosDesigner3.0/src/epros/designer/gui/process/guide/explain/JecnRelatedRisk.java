package epros.designer.gui.process.guide.explain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnRisk;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnControlPointT;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.swing.JecnPanel;

/**
 * 相关风险
 * 
 * @author user
 * 
 */
public class JecnRelatedRisk extends JecnFileDescriptionTable {

	private Logger log = Logger.getLogger(JecnRelatedRisk.class);
	protected Long flowId = null;
	private ProcessOperationDialog dialog;

	public JecnRelatedRisk(int index, String name, boolean isRequest, JecnPanel contentPanel, Long flowId,
			ProcessOperationDialog dialog, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		this.dialog = dialog;
		this.initTable();

		table.setTableColumn(1, 180);
	}

	@Override
	protected void dbClickMethod() {

	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("riskNum"));
		title.add(JecnProperties.getValue("riskName"));
		return new JecnTableModel(title, getContent());

	}

	private Vector<Vector<String>> getContent() {
		List<JecnRisk> rilsList = new ArrayList<JecnRisk>();
		try {
			rilsList = ConnectionPool.getProcessAction().getRiskByRiskIds(getRiskList());
		} catch (Exception e) {
			log.error("JecnRelatedRisk getContent is error", e);
		}
		Vector<Vector<String>> riskContent = new Vector<Vector<String>>();
		for (JecnRisk jecnRisk : rilsList) {
			Vector<String> riskContentStr = new Vector<String>();
			// ID
			riskContentStr.add(String.valueOf(jecnRisk.getId()));
			// 编号
			riskContentStr.add(jecnRisk.getRiskCode());
			// 名称
			riskContentStr.add(jecnRisk.getRiskName());

			riskContent.add(riskContentStr);
		}
		return riskContent;
	}

	/**
	 * 获取流程 风险点ID
	 * 
	 * @author fuzhh 2013-11-28
	 * @return
	 */
	public Set<Long> getRiskList() {
		Set<Long> riskSet = new HashSet<Long>();
		List<JecnActivityShowBean> listAllActivityShow = dialog.getFlowOperationBean().getListAllActivityShow();
		if (listAllActivityShow != null && listAllActivityShow.size() > 0) {
			for (JecnActivityShowBean activityShowBean : listAllActivityShow) {
				// 获取活动对应的控制点集合
				List<JecnControlPointT> jecnControlPointTList = activityShowBean.getActiveFigure().getFlowElementData()
						.getJecnControlPointTList();
				if (jecnControlPointTList == null) {// 为空执行下一个活动
					continue;
				}
				for (JecnControlPointT controlPointT : jecnControlPointTList) {
					if (controlPointT != null && controlPointT.getRiskId() != null) {
						riskSet.add(controlPointT.getRiskId());
					}
				}
			}
		}
		return riskSet;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
