package epros.designer.gui.popedom.role;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 
 * 新建角色目录
 * 
 * @author zhangjie 2012-05-04
 * 
 */
public class AddRoleDirNameDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddRoleDirNameDialog.class);

	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddRoleDirNameDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	


	// Dialog标题
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addDirI");
	}


	// 角色名称Lab
	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name,
				TreeNodeType.roleDir);
	}

	@Override
	public void saveData() {
		JecnRoleInfo jecnRoleInfo = new JecnRoleInfo();
		jecnRoleInfo.setRoleName(getName());
		jecnRoleInfo.setIsDir(1);
		jecnRoleInfo.setPerId(pNode.getJecnTreeBean().getId());
		jecnRoleInfo.setSortId(JecnTreeCommon.getMaxSort(pNode));
		jecnRoleInfo.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRoleInfo.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRoleInfo.setProjectId(JecnConstants.projectId);
		try {
			Long id = ConnectionPool.getJecnRole().addRoleDir(jecnRoleInfo,JecnConstants.getUserId());
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.roleDir);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddRoleDirNameDialog saveData is error！", e);
		}

	}
	protected void validateEnglishName() {// 验证英文名称
	}

	protected void addEnNamePanel(Insets insets) {
	}
	
}
