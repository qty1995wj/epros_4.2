package com.jecn.epros.domain;

import io.swagger.annotations.ApiModelProperty;

public class UserResourceStatistics {

    @ApiModelProperty(value = "我的流程数量", position = 1)
    private int processCount;

    @ApiModelProperty(value = "我的制度数量", position = 2)
    private int ruleCount;

    @ApiModelProperty(value = "我的风险/内控数量", position = 3)
    private int riskCount;

    public int getProcessCount() {
        return processCount;
    }

    public void setProcessCount(int processCount) {
        this.processCount = processCount;
    }

    public int getRuleCount() {
        return ruleCount;
    }

    public void setRuleCount(int ruleCount) {
        this.ruleCount = ruleCount;
    }

    public int getRiskCount() {
        return riskCount;
    }

    public void setRiskCount(int riskCount) {
        this.riskCount = riskCount;
    }

}
