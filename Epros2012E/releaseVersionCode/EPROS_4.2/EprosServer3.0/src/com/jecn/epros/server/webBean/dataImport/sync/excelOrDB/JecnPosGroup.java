package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;

/**
 * 岗位组同步，数据bean(和岗EPROS岗位组同步数据表)
 * 
 * @author ZXH
 * 
 */
public class JecnPosGroup extends AbstractBaseBean {
	private String num = null;
	private String name = null;
	private String parentNum = null;
	/** 0是岗位组，1是目录 */
	private int isDir;

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentNum() {
		return parentNum;
	}

	public void setParentNum(String parentNum) {
		this.parentNum = parentNum;
	}

	public int getIsDir() {
		return isDir;
	}

	public void setIsDir(int isDir) {
		this.isDir = isDir;
	}

	@Override
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 部门编号
		strBuf.append("编号:");
		strBuf.append(num);
		strBuf.append(SyncConstant.EMTRY_SPACE);

		// 部门名称
		strBuf.append("名称:");
		strBuf.append(name);
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 上级部门编号
		strBuf.append("上级编号:");
		strBuf.append(parentNum);
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 上级部门编号
		strBuf.append("目录");
		strBuf.append(isDir);
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 错误信息
		strBuf.append("错误信息:");
		strBuf.append(this.getError());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}
}
