package com.jecn.epros.service.system;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.system.SyncConfig;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.ViewRoleAuth;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.ProcessBaseInfoBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.system.*;
import com.jecn.epros.security.DataAccessAuthorityBean;

import javax.management.relation.RoleInfo;
import java.util.List;
import java.util.Map;

public interface SystemService {

    public int getTotalUser(UserWebSearchBean userWebSearchBean);

    public List<UserWebInfoBean> fetchUserInfos(UserWebSearchBean userWebSearchBean, Paging paging);

    public int getTotalRole(String roleName);

    public int getTotalViewRole(String roleName);

    public List<RoleWebInfoBean> fetchUserRoles(String roleName);

    public RoleManageBean fetchUserRoleInfos(Map<String, Object> paramMap, Paging paging);

    public List<ViewRoleDetailBean> fetchViewRoles(String roleName, Paging paging);

    public List<TreeNode> getViewRoleAuthList(String roleId);

    public int saveViewRoleAuthList(ViewRoleAuth viewRoleAuth);

    public int getProcessCheckCount(Integer processCheckType, long projectId);

    public List<ProcessBaseInfoBean> fetchProcessCheckList(Integer processCheckType, long projectId, Paging paging);

    public MessageResult addRole(ViewRoleDetailBean role);

    public List<Map<String, Object>>  findRolesInfo(ViewRoleDetailBean role);

    public MessageResult deleteRole(ViewRoleDetailBean role);

    public void addRoles(DataAccessAuthorityBean authorityBean,Long pepoleId);

    public MessageResult fetchLogNames(String version);

    public byte[] getLogByte(String version, String name);

}
