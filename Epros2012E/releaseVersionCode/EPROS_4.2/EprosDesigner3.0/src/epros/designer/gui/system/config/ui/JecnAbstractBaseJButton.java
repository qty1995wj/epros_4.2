package epros.designer.gui.system.config.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;

/**
 * 
 * 加入、编辑、删除、上移、下移、默认等按钮父类
 * 
 * @author Administrator
 * 
 */
public abstract class JecnAbstractBaseJButton extends JButton implements
		ActionListener {
	/** 选中序号 */
	protected int[] selectedIndexs = null;
	/** 配置界面 */
	protected JecnAbtractBaseConfigDialog dialog = null;

	public JecnAbstractBaseJButton(JecnAbtractBaseConfigDialog dialog) {
		if (dialog == null) {
			throw new IllegalArgumentException("JecnAbstractBaseJButton is error");
		}
		this.dialog = dialog;

		// 初始化组件
		initComponents();
	}

	protected void initComponents() {
		// 大小
		Dimension size = new Dimension(85, 20);
		this.setPreferredSize(size);
		this.setMaximumSize(size);
		this.setMinimumSize(size);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
}