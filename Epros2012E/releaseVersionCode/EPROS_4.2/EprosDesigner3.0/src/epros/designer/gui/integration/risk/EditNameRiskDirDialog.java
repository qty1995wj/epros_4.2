package epros.designer.gui.integration.risk;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 风险目录重命名
 * 2013-10-31
 *
 */
public class EditNameRiskDirDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditNameRiskDirDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	
	public EditNameRiskDirDialog(JecnTreeNode selectNode, JTree jTree){
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		try {
			this.setName(selectNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("EditNameRiskDirDialog is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties
					.getValue("serverConnException"));
		}
	}
	
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		try {
			if(!"".equals(JecnUserCheckUtil.checkName(getName()))){
				promptLab.setText(JecnUserCheckUtil.checkName(getName()));
				return;
			}
			// 风险目录重命名
			ConnectionPool.getJecnRiskAction().reRiskName(this.getName(), selectNode.getJecnTreeBean().getId(), JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("EditNameRiskDirDialog saveData is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties
					.getValue("serverConnException"));
		}
		// 重命名
		JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		if(selectNode != null){
			return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
		}
		return false;
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
