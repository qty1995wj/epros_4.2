package com.jecn.epros.server.action.web.login.ad.kinglong;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.landray.sso.client.EKPSSOClient;

public class KLEKPSSOClient extends EKPSSOClient {
	public void init(FilterConfig config) throws ServletException {
		init(config.getInitParameter("filterConfigFile"));
	}

	public void init(String filterConfigFile) throws ServletException {
		super.init(filterConfigFile);
	}

	public void destroy() {
		if (processingFilters != null)
			for (int i = 0; i < processingFilters.length; i++)
				processingFilters[i].destroy();
		processingFilters = null;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
			ServletException {
		super.doFilter(req, res, chain);
	}
}
