package epros.designer.gui.integration.risk;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class RiskMoveChooseDialog extends JecnMoveChooseDialog {
	private Logger log = Logger.getLogger(RiskMoveChooseDialog.class);

	public RiskMoveChooseDialog(List<JecnTreeNode> listMoveNodes, JecnTree moveTree) {
		super(listMoveNodes, moveTree);
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyRiskMoveTree(this.getListIds());
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 名称
		return title;
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		// if ("0".equals(pid.toString())) {
		// JecnOptionPane.showMessageDialog(this, "风险点不能移动到根节点");
		// return false;
		// }
		try {
			ConnectionPool.getJecnRiskAction().moveNodes(ids, pid, JecnConstants.getUserId(), this.getMoveNodeType());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("RiskMoveChooseDialog savaData is error", e);
			// 增加提示
			return false;

		}
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = ConnectionPool.getJecnRiskAction().getChildsRisk(pid, JecnConstants.projectId);
			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}
				}
			}
		} catch (Exception e) {
			log.error("RiskMoveChooseDialog validateName is error", e);
			return false;
		}
		return false;
	}

}
