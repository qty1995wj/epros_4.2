//流程文档下载人统计
function processDocumentPeopleGrid() {
	processDocumentPeopleGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "processDocumentPeople.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "flowId",
								mapping : "processWebBean.flowId"
							}, {
								name : "flowName",
								mapping : "processWebBean.flowName"
							}, {
								name : "flowIdInput",
								mapping : "processWebBean.flowIdInput"
							}, {
								name : "resPeopleName",
								mapping : "processWebBean.resPeopleName"
							}, {
								name : "orgName",
								mapping : "processWebBean.orgName"
							},{
								name : "accessName",
								mapping : "accessName"
							},{
								name : "accessTime",
								mapping : "accessTime"
							}])

			});

	processDocumentPeopleGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			},{
				header : i_downloadPeople,
				dataIndex : "accessName",
				align : "center",
				menuDisabled : true
			},{
				header : i_downloadTime,
				dataIndex : "accessTime",
				align : "center",
				menuDisabled : true
			}

	]);
	processDocumentPeopleGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processDocumentPeopleGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processDocumentPeopleGridPanel = new Ext.grid.GridPanel({
				id : "processDocumentPeopleGridPanel_id",
				renderTo : 'processDocumentPeopleGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-195,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processDocumentPeopleGridStore,
				colModel : processDocumentPeopleGridColumnModel,
				bbar : processDocumentPeopleGridBbar
			})
		reports_index_id='reports_processDocumentPeople_id';     	
}

/**
 * 流程文档下载人统计查询
 */
function processDocumentPeopleGridSearch(){
     // 访问人ID
     var userId = document.getElementById("userId_people").value;
	 //流程ID
	 var processId=document.getElementById("processId_people_id").value;
	  //责任部门ID 
	 var resOrgId=document.getElementById("orgDutyId_people_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
	 // 部门ID
	 var orgId = document.getElementById("orgLookId_people_id").value;
	 // 岗位ID
	 var posId = document.getElementById("participateInPostId_people_id").value.split("_")[0];
	  // 结束时间大于开始时间
	 if (startTime > endTime) {
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return;
	 }
   	 processDocumentPeopleGridStore.setBaseParam("userId", userId);
     processDocumentPeopleGridStore.setBaseParam("processId", processId);
     processDocumentPeopleGridStore.setBaseParam("resOrgId", resOrgId);
     processDocumentPeopleGridStore.setBaseParam("startTime", startTime);
     processDocumentPeopleGridStore.setBaseParam("endTime", endTime);
     processDocumentPeopleGridStore.setBaseParam("orgId", orgId);
     processDocumentPeopleGridStore.setBaseParam("posId", posId);

	 processDocumentPeopleGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
				
}

/**
 * 流程文档下载人统计重置
 **/
function processDocumentPeopleSearchReset(){
        document.getElementById("orgDutyName_people_id").value = "";
		document.getElementById("processName_people_id").value = "";
		document.getElementById("orgDutyId_people_id").value = "-1";
		document.getElementById("processId_people_id").value = "-1";
		document.getElementById("startTime").value = "";
		document.getElementById("endTime").value = "";
		document.getElementById("userName_people").value = "";
		document.getElementById("orgLookName_people_id").value = "";
		document.getElementById("participateInPostName_people_id").value = "";
		document.getElementById("userId_people").value = "-1";
		document.getElementById("orgLookId_people_id").value = "-1";
		document.getElementById("participateInPostId_people_id").value = "-1";
}

// 流程文档下载人下载
function processDocumentPeopleDownloadExcel(){
     // 访问人ID
     var userId = document.getElementById("userId_people").value;
	 //流程ID
	 var processId=document.getElementById("processId_people_id").value;
	  //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_people_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
	 // 部门ID
	 var orgId = document.getElementById("orgLookId_people_id").value;
	 // 岗位ID
	 var posId = document.getElementById("participateInPostId_people_id").value.split("_")[0];
     window.location.href="downloadProcessDocumentPeople.action?userId="+userId+"&processId="+processId+"&resOrgId="+resOrgId
	                        +"&startTime="+startTime+"&endTime="+endTime+"&orgId="+orgId+"&posId="+posId;
}