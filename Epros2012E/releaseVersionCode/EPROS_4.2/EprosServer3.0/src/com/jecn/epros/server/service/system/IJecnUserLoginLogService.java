package com.jecn.epros.server.service.system;

import java.util.List;

/***
 * 登录日志(JECN_USER_LOGIN_LOG)操作类
 * @author 201305
 *
 */
public interface IJecnUserLoginLogService {
	
	
	/***
	 * 添加登录日志数据
	 * 用户登录
	 * @param userId  用户名
	 * @param sessionId  sessionID
	 * @param loginType 0：浏览端，1：设计器端
	 */
	public int addUserLoginLog(Long userId,String sessionId,int loginType);
	
	
	/***
	 * 更新登录日志数据
	 * 用户退出
	 * @param loginOutType  1，session失效活正常退出，2，服务器异常停止
	 * @param loginType 0：浏览端，1：设计器端
	 */
	public void updateUserLoginLog(String sessionId,int loginOutType,int loginType);

	/***
	 * 获取登录日志数据
	 * @param loginType 0：浏览端，1：设计器端
	 * @return
	 */
	public List<String> getUserLoginLogSessionID(int loginType);
	
	/****
	 * 服务器异常停止，更新浏览端所有用户退出时间为空的数据
	 * @param loginType 0：浏览端，1：设计器端
	 */
	public int updateLogiOutTime();
}
