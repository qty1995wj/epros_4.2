
package com.jecn.webservice.zhongsh.risk.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�����ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchTaskCountResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchTaskCountResult"
})
@XmlRootElement(name = "SearchTaskCountResponse")
public class SearchTaskCountResponse {

    @XmlElement(name = "SearchTaskCountResult")
    protected int searchTaskCountResult;

    /**
     * ��ȡsearchTaskCountResult���Ե�ֵ��
     * 
     */
    public int getSearchTaskCountResult() {
        return searchTaskCountResult;
    }

    /**
     * ����searchTaskCountResult���Ե�ֵ��
     * 
     */
    public void setSearchTaskCountResult(int value) {
        this.searchTaskCountResult = value;
    }

}
