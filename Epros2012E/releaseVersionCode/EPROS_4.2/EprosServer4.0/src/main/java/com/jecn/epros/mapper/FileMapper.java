package com.jecn.epros.mapper;

import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.file.*;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.sqlprovider.FileSqlProvider;
import org.apache.ibatis.annotations.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface FileMapper {

    @SelectProvider(type = FileSqlProvider.class, method = "fetchAttachmentByIdSql")
    @ResultMap("mapper.Attachment.attachment")
    Attachment fetchAttachmentById(Long id);

    /**
     * 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = FileSqlProvider.class, method = "findChildFiles")
    @ResultMap("mapper.File.fileTreeBeans")
    public List<TreeNode> findChildFiles(Map<String, Object> map);

    /**
     * 基本信息
     */
    @SelectProvider(type = FileSqlProvider.class, method = "findBaseInfo")
    @ResultMap("mapper.File.fileBaseInfoBean")
    public FileBaseInfoBean findBaseInfo(Map<String, Object> map);

    /**
     * 文件使用情况
     */
    @SelectProvider(type = FileSqlProvider.class, method = "findFileUseDetail")
    @ResultMap("mapper.File.fileUseDetailBean")
    public List<FileUseDetailBean> findFileUseDetail(Map<String, Object> map);

    /**
     * 文件搜索
     */
    @SelectProvider(type = FileSqlProvider.class, method = "searchFile")
    @ResultMap("mapper.File.searchFileResultBean")
    public List<SearchFileResultBean> searchFile(Map<String, Object> map);

    /**
     * 制度搜索 总数
     */
    @SelectProvider(type = FileSqlProvider.class, method = "searchFileTotal")
    @ResultType(Integer.class)
    public int searchFileTotal(Map<String, Object> map);

    /**
     * 获取文件名称
     *
     * @param map key:fileId,isPub
     * @return
     */
    public String findFileNameById(Map<String, Object> map);

    @Select("select JF.FILE_ID fileId,JF.FILE_NAME fileName," +
            "       JF.PEOPLE_ID peopleID," +
            "       JF.PER_FILE_ID perFileId," +
            "       JF.CREATE_TIME createTime," +
            "       JF.UPDATE_PERSON_ID updatePersonId," +
            "       JF.UPDATE_TIME updateTime," +
            "       JF.IS_DIR isDir," +
            "       JF.ORG_ID orgId," +
            "       JF.DOC_ID docId," +
            "       JF.IS_PUBLIC isPublic," +
            "       JF.FLOW_ID flowId," +
            "       JF.SORT_ID sortId," +
            "       JF.SAVETYPE saveType," +
            "       JF.PROJECT_ID projectId," +
            "       JF.VERSION_ID versionId," +
            "       JF.HISTORY_ID historyId," +
            "       JF.DEL_STATE delState," +
            "       JF.T_PATH tPath," +
            "       JF.T_LEVEL tLevel," +
            "       JF.PUB_TIME pubTime," +
            "       JF.CONFIDENTIALITY_LEVEL confidentialityLevel," +
            "       JF.VIEW_SORT viewSort" +
            "  from JECN_FILE_T JF" +
            " where JF.FILE_ID =#{fileId} ")
    public FileBeanT getFileBeanT(Long fileId);

    @Select("select JF.FILE_ID fileId,JF.FILE_NAME fileName," +
            "       JF.PEOPLE_ID peopleID," +
            "       JF.PER_FILE_ID perFileId," +
            "       JF.CREATE_TIME createTime," +
            "       JF.UPDATE_PERSON_ID updatePersonId," +
            "       JF.UPDATE_TIME updateTime," +
            "       JF.IS_DIR isDir," +
            "       JF.ORG_ID orgId," +
            "       JF.DOC_ID docId," +
            "       JF.IS_PUBLIC isPublic," +
            "       JF.FLOW_ID flowId," +
            "       JF.SORT_ID sortId," +
            "       JF.SAVETYPE saveType," +
            "       JF.PROJECT_ID projectId," +
            "       JF.VERSION_ID versionId," +
            "       JF.HISTORY_ID historyId," +
            "       JF.DEL_STATE delState," +
            "       JF.T_PATH tPath," +
            "       JF.T_LEVEL tLevel," +
            "       JF.PUB_TIME pubTime," +
            "       JF.CONFIDENTIALITY_LEVEL confidentialityLevel," +
            "       JF.VIEW_SORT viewSort" +
            "  from JECN_FILE_H JF" +
            " where JF.FILE_ID =#{fileId} AND JF.HISTORY_ID=#{historyId}")
    public FileBeanT getFileBeanHistory(Map<String, Object> map);

    @Select("select ft.ID fileId, ft.FILE_NAME fileName" +
            "  from FILE_CONTENT ft" +
            " inner join jecn_file_t jf" +
            "    on jf.version_id = ft.id" +
            "    where ft.ID = #{fileId} and jf.del_state = 0")
    public FileBeanT getFileContent(Long fileId);


    @SelectProvider(type = FileSqlProvider.class, method = "findBaseInfo")
    @ResultMap("mapper.File.fileBaseInfoBean")
    public FileBaseInfoBean getFileBaseInfoBean(Long fileId);

    @Select("select FILE_ID fileId,FILE_NAME fileName from JECN_FILE where FILE_ID=#{fileId}")
    public FileBean getFileBean(Long fileId);

    @Select("select count(FILE_ID) from JECN_FILE_H where FILE_ID=#{id} AND HISTORY_ID = #{historyId}")
    public int getFileBeanCount(Map<String, Object> map);

    @Select("select FILE_ID fileId,FILE_NAME fileName from JECN_FILE_T  where del_State=0 and file_ID in ${ids}")
    List<FileBeanT> fetchFileBeanT(Map<String, Object> paramMap);

    @Update("update jecn_file_t set history_id=#{historyId},update_time=#{updateTime},is_public=#{isPublic} where file_id=#{fileId}")
    void updateFileBeanT(FileBeanT jecnFileBeanT);

    @Delete("delete from jecn_file where file_id in ${ids}")
    void deleteFileByIds(Map<String, Object> paramMap);

    @Delete("delete from jecn_org_access_permissions where type=1 and relate_id in ${ids}")
    void deleteOrgAccessPermissionByIds(Map<String, Object> paramMap);

    @Delete("delete from jecn_access_permissions where type=1 and relate_id in ${ids}")
    void deleteAccessPermissionByIds(Map<String, Object> paramMap);

    @Delete("delete from jecn_group_permissions where type=1 and relate_id in ${ids}")
    void deleteGroupPermissionByIds(Map<String, Object> paramMap);

    @Update("update FILE_CONTENT set type=1 where id in (select t.VERSION_ID from jecn_file_t t where t.file_id in ${ids})")
    void updateFileContentTypeToOneByIds(Map<String, Object> paramMap);

    @Insert("insert into JECN_ACCESS_PERMISSIONS (ID, FIGURE_ID, RELATE_ID, TYPE) "
            + "SELECT ID, FIGURE_ID, RELATE_ID, TYPE FROM JECN_ACCESS_PERMISSIONS_T WHERE type=1 and RELATE_ID in ${ids}")
    void insertAccessPermissionByIds(Map<String, Object> paramMap);

    @Insert("insert into JECN_ORG_ACCESS_PERMISSIONS (ID, ORG_ID, RELATE_ID, TYPE)"
            + "SELECT ID, ORG_ID, RELATE_ID, TYPE FROM JECN_ORG_ACCESS_PERM_T WHERE type=1 and RELATE_ID in ${ids}")
    void insertOrgAccessPermissionByIds(Map<String, Object> paramMap);

    @Insert("insert into JECN_GROUP_PERMISSIONS(ID, POSTGROUP_ID, RELATE_ID, TYPE) "
            + " select ID, POSTGROUP_ID, RELATE_ID, TYPE from JECN_GROUP_PERMISSIONS_T WHERE TYPE=1 AND RELATE_ID in ${ids}")
    void insertGroupPermissionByIds(Map<String, Object> paramMap);

    @Insert("insert into jecn_file(FILE_ID, FILE_NAME, PEOPLE_ID, PER_FILE_ID,CREATE_TIME, UPDATE_PERSON_ID,UPDATE_TIME, IS_DIR, ORG_ID, DOC_ID, IS_PUBLIC, FLOW_ID, SORT_ID, SAVETYPE, PROJECT_ID,FILE_PATH,VERSION_ID, HISTORY_ID,DEL_STATE,T_PATH,T_LEVEL)"
            + " select FILE_ID, FILE_NAME, PEOPLE_ID, PER_FILE_ID,CREATE_TIME, UPDATE_PERSON_ID,UPDATE_TIME, IS_DIR, ORG_ID, DOC_ID, IS_PUBLIC, FLOW_ID, SORT_ID, SAVETYPE, PROJECT_ID,FILE_PATH,VERSION_ID, HISTORY_ID,DEL_STATE,T_PATH,T_LEVEL from jecn_file_t where file_Id in ${ids}")
    void insertFilesByIds(Map<String, Object> paramMap);

    @Select("SELECT FC.FILE_ID AS FILEID,FC.FILE_NAME AS FILENAME," + "FC.UPDATE_PEOPLE_ID AS UPDATEPEOPLEID,FC.FILE_STREAM FILESTREAM, FC.TYPE AS TYPE"
            + " FROM JECN_FILE_T FT" + " INNER JOIN FILE_CONTENT  FC" + " ON FT.VERSION_ID = FC.ID"
            + " WHERE FT.FILE_ID IN ${ids}")
    List<Map<String, Object>> fetchFileRelatedContent(Map<String, Object> paramMap);

    @Select("SELECT FT.FILE_ID fileId,FT.FILE_NAME fileName FROM JECN_FILE FT WHERE FT.FILE_ID IN ${ids}")
    List<FileBean> fetchFileBeanByIds(Map<String, Object> paramMap);

    @Update("update jecn_file set version_id=#{versionId},update_time=#{updateTime} where file_id=#{fileId}")
    void updateFileBean(FileBean jecnFileBean);


    @SelectKey(before = true, keyProperty = "id", resultType = Long.class, statement = {"select JECN_FILE_SEQUENCE.nextval from dual"})
    @InsertProvider(type = FileSqlProvider.class, method = "saveFileContent")
    void saveFileContentOracle(FileContent curContent);

    @SelectKey(before = false, keyProperty = "id", resultType = Long.class, statement = {"select @@identity"})
    @InsertProvider(type = FileSqlProvider.class, method = "saveFileContent")
    void saveFileContentSqlServer(FileContent curContent);

    @SelectProvider(type = FileSqlProvider.class, method = "fetchInventory")
    @ResultMap("mapper.File.fetchInventory")
    List<FileDetailWebBean> fetchInventory(Map<String, Object> paramMap);

    @SelectProvider(type = FileSqlProvider.class, method = "fetchInventoryParent")
    @ResultMap("mapper.File.fetchInventory")
    List<FileDetailWebBean> fetchInventoryParent(Map<String, Object> paramMap);

    @SelectProvider(type = FileSqlProvider.class, method = "judgmentFileExists")
    String judgmentFileExists(Map<String, Object> paramMap);
}
