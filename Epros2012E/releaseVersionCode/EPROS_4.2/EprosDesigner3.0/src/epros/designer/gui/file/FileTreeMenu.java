package epros.designer.gui.file;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultTreeModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnLookSetDialog;
import epros.designer.gui.error.ErrorCommon;
import epros.designer.gui.file.fileMove.FileMoveNodeDialog;
import epros.designer.gui.publish.FilePublishDialog;
import epros.designer.gui.rule.RuleDoControlPanel;
import epros.designer.gui.task.config.TempletChooseDialog;
import epros.designer.gui.task.jComponent.JecnFileTaskJDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

public class FileTreeMenu extends JPopupMenu {

	private static Logger log = Logger.getLogger(FileTreeMenu.class);
	private JecnTree jTree = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 创建目录 */
	private JMenuItem createDir;

	/** 重命名 */
	private JMenuItem reName;

	/** 上传文件 */
	private JMenuItem uploadFile;
	/** 更新文件 */
	private JMenuItem updateFile;
	/** 提交审批 */
	private JMenuItem submitAppItem;
	/** 发布 */
	private JMenuItem releaseItem = new JMenuItem(JecnProperties.getValue("release"), new ImageIcon(
			"images/menuImage/release.gif"));
	/** 撤销发布 */
	private JMenuItem cancelReleaseItem = new JMenuItem(JecnProperties.getValue("cancelRelease"), new ImageIcon(
			"images/menuImage/cancelRelease.gif"));
	/** 直接发布-不记录版本 */
	private JMenuItem directReleaseItem = new JMenuItem(JecnProperties.getValue("directRelease"), new ImageIcon(
			"images/menuImage/directRelease.gif"));

	/** 打开文件 */
	private JMenuItem openFile;

	/** 文件使用情况 */
	private JMenuItem usageFile;

	/** 文件版本信息情况 */
	private JMenuItem versionFile;

	/** 文件属性 */
	private JMenuItem fileProperty;

	/** 刷新 */
	private JMenuItem refresh;

	/** 查阅权限 */
	private JMenuItem accessAuthorityItem;

	/** 查阅权限(岗位) */
	// private JMenuItem searchPos;
	/** 删除 */
	private JMenuItem deleteMenu;

	/** 节点移动 */
	private JMenuItem treeMove;

	/** 节点排序 */
	private JMenuItem treeSort;

	/** 文控信息 */
	private JMenuItem historyItem = new JMenuItem(JecnProperties.getValue("historyRecord"), new ImageIcon(
			"images/menuImage/historyRecord.gif"));

	/** 显示/隐藏 */
	private JMenuItem hideItem;

	private Separator separatorOne = new Separator();
	private Separator separatorTwo = new Separator();
	private Separator separatorThree = new Separator();
	private Separator separatorFour = new Separator();
	/** 文件管理框 */
	private FileManageDialog jecnManageDialog = null;
	/** * 文件选择框 */
	private FileChooseDialog fileChooseDialog = null;
	/** 文件名称,文件路径 */
	private List<Object[]> fileNamePathList = new ArrayList<Object[]>();

	/** 批量发布-不记录版本 */
	private JMenuItem batchReleaseNotReCordItem = new JMenuItem(JecnProperties.getValue("batchReleaseNoRecord"));
	/** 批量撤销发布 */
	private JMenuItem batchCancelReleaseItem = new JMenuItem(JecnProperties.getValue("cancelRelease"));

	/** 任务配置 */
	private JMenuItem taskEdit = new JMenuItem(JecnProperties.getValue("file_Task_Seeting"));
	/** 自动编号 */
	private JMenuItem autoCodeMenu = new JMenuItem(JecnProperties.getValue("autoNum"));

	/** 是否允许自动创建目录 */
	private boolean isAllowAutoCreateDir = JecnConfigTool.isAutoCreateFileDir();

	// 提交废止任务
	private JMenuItem desuetudeItem = new JMenuItem(JecnProperties.getValue("submitAbolish"));

	/** 设置专员和责任人 */
	private JMenuItem setCommissionAndResPeople;
	/** 发布提醒 **/
	private JMenuItem pubNoteItem = new JMenuItem();

	public FileTreeMenu(JecnTree jTree, List<JecnTreeNode> listSelectNode, FileManageDialog jecnManageDialog) {
		this.jTree = jTree;
		this.jecnManageDialog = jecnManageDialog;
		this.listNode = listSelectNode;
		initCompotents();

	}

	public FileTreeMenu(JecnTree jTree, List<JecnTreeNode> listSelectNode, FileChooseDialog fileChooseDialog) {
		this.jTree = jTree;
		this.fileChooseDialog = fileChooseDialog;
		this.listNode = listSelectNode;
		initCompotents();
	}

	private void initCompotents() {
		// // 自动创建目录
		// automaticDir = new JMenu("自动创建目录");
		//
		// // 以组织的结构来创建目录
		// useOrderDir = new JMenuItem("以组织的结构来创建");
		//
		// // 以流程的结构来创建
		// useFlowDir = new JMenuItem("以流程的结构来创建");
		if (listNode.size() > 0) {
			selectNode = listNode.get(0);
			pubNoteItem = JecnUtil.createPubNoteMenu(selectNode);
			pubNoteItem.setVisible(false);
		}

		// 创建目录
		createDir = new JMenuItem(JecnProperties.getValue("createDir"),
				new ImageIcon("images/menuImage/newRuleDir.gif"));

		// 重命名
		reName = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon("images/menuImage/rename.gif"));

		// 上传文件
		uploadFile = new JMenuItem(JecnProperties.getValue("uploadFile"));

		// // 上传文件任务
		// uploadFileTask = new JMenuItem(JecnProperties
		// .getValue("uploadFileTaskI"));

		// 更新文件
		updateFile = new JMenuItem(JecnProperties.getValue("updateFile"));
		// // 直接更新文件
		// directUpdateFile = new JMenuItem(JecnProperties
		// .getValue("directUpdateI"));

		// 提交审批
		submitAppItem = new JMenuItem(JecnProperties.getValue("submitApp"), new ImageIcon(
				"images/menuImage/submitApp.gif"));

		// 打开文件
		openFile = new JMenuItem(JecnProperties.getValue("openFile"));

		// 文件属性
		fileProperty = new JMenuItem(JecnProperties.getValue("fileProperty"));

		// 文件使用情况
		usageFile = new JMenuItem(JecnProperties.getValue("usageInfo"));
		// 文件版本信息
		versionFile = new JMenuItem(JecnProperties.getValue("VersionRule"));

		// 刷新
		refresh = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon("images/menuImage/refresh.gif"));

		// 查阅权限
		accessAuthorityItem = new JMenuItem(JecnProperties.getValue("lookPermissions"), new ImageIcon(
				"images/menuImage/lookPermissions.gif"));

		// 删除
		deleteMenu = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon("images/menuImage/delete.gif"));

		// 显示或隐藏
		hideItem = new JMenuItem();

		// 节点移动
		treeMove = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon("images/menuImage/nodeMove.gif"));

		// 节点排序
		treeSort = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon("images/menuImage/nodeSort.gif"));

		// 设置专员和责任人
		setCommissionAndResPeople = new JMenuItem(JecnProperties.getValue("settingCommissionerOrPersonLiable"));
		initAddMenu();

		// 创建目录
		createDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createDirPerformed();
			}
		});
		// 文件属性
		fileProperty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filePropertyPerformed();
			}
		});
		// 文件使用情况
		usageFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				usageFilePerformed();

			}
		});
		versionFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				versionFilePerformed();

			}
		});
		// 上传
		uploadFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				uploadFileAction();
			}
		});
		// 更新
		updateFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnFileCommon.updateFileAction(selectNode, jTree);
			}
		});
		// directUpdateFile.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// updateFileAction(true);
		// }
		// });
		// 发布
		releaseItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				pubFile();
			}
		});
		// 不记录版本发布
		directReleaseItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				directReleaseItemAction();
			}
		});
		// 撤销发布
		cancelReleaseItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelReleaseItemAction();
			}
		});
		openFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openFileAction();
			}
		});
		// 刷新
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshItemAction();
			}
		});

		// 重命名
		reName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reNamePerformed();
			}
		});

		// 删除
		deleteMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteMenuPerformed();
			}
		});

		// 显示或隐藏
		hideItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				hideItemPerformed();
			}
		});

		treeSort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				treeSortAction();
			}
		});

		// 节点排序
		treeMove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				treeMoveAction();
			}
		});
		// // 上传文件任务
		// uploadFileTask.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// fileAppItemAction();
		// }
		// });
		// 更新文件任务
		submitAppItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileAppItemAction(0);
			}
		});

		desuetudeItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileAppItemAction(2);
			}
		});

		accessAuthorityItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
							selectNode.getJecnTreeBean().getId(), 1);

					JecnLookSetDialog d = new JecnLookSetDialog(lookPopedomBean, selectNode, 1, true);
					d.setVisible(true);
					// List<JecnTreeBean> list =
					// ConnectionPool.getOrganizationAction()
					// .getOrgAccessPermissions(
					// selectNode.getJecnTreeBean().getId(), 1);
					// JecnCommon.setDeptLook(selectNode, list, 1);
				} catch (Exception e1) {
					log.error("accessAuthorityItem is error！", e1);
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
				}
			}
		});
		// 文控信息
		historyItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				historyItemAction();
			}
		});

		batchReleaseNotReCordItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				batchReleaseNotRecordAction();
			}

		});

		batchCancelReleaseItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				batchCancelReleaseNotRecordAction();
			}

		});

		taskEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				taskEditAction();

			}
		});
		autoCodeMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				autoCodeAction();
			}
		});// 设置专员和责任人
		setCommissionAndResPeople.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setCommissionAndResPeopleAction();

			}
		});

	}

	protected void setCommissionAndResPeopleAction() {
		SetCommissionAndResPeopleDialog setCommissionAndResPeopleDialog = new SetCommissionAndResPeopleDialog(
				selectNode);
		setCommissionAndResPeopleDialog.setVisible(true);
	}

	private void autoCodeAction() {
		JecnDesignerCommon.autoCodeAction(selectNode, jecnManageDialog == null ? fileChooseDialog : jecnManageDialog,
				false);
	}

	protected void taskEditAction() {
		/*
		 * String curSelectTempletId =
		 * JecnTreeCommon.getTaskTempletIdBySelf(selectNode, 1); // 弹出一个面板
		 * TaskConfigSelectDialog taskEditDialog = new
		 * TaskConfigSelectDialog(selectNode, curSelectTempletId, 1);
		 * taskEditDialog.setVisible(true);
		 */
		TempletChooseDialog taskEditDialog = new TempletChooseDialog(selectNode.getJecnTreeBean().getId(), 1);
		taskEditDialog.setVisible(true);
	}

	private void batchReleaseNotRecordAction() {
		// 是否处于任务审批中
		for (JecnTreeNode selectNode : listNode) {
			if (JecnTool.isBeInTaskWithTipName(selectNode)) {
				return;
			}
			// 检查错误信息
			if (ErrorCommon.INSTANCE.checkFileBaseInfo(selectNode.getJecnTreeBean().getId())) {
				return;
			}
		}

		// 提示是否发布
		int option = JecnOptionPane
				.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("isnotRemberPublish"),
						null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			List<Long> ids = new ArrayList<Long>();
			for (JecnTreeNode selectNode : listNode) {
				ids.add(selectNode.getJecnTreeBean().getId());
			}
			ConnectionPool.getFileAction().batchReleaseNotRecord(ids, JecnUtil.getUserId());
			for (JecnTreeNode selectNode : listNode) {
				selectNode.getJecnTreeBean().setPub(true);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			}

			// 发布成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
		} catch (Exception e1) {
			log.error("batchReleaseNotRecordAction is error！", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	// TODO
	private void batchCancelReleaseNotRecordAction() {
		// 提示是否发布
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("ifCancelRelease"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			List<Long> ids = new ArrayList<Long>();
			for (JecnTreeNode selectNode : listNode) {
				ids.add(selectNode.getJecnTreeBean().getId());
			}
			ConnectionPool.getFileAction().batchCancelReleaseNotRecord(ids, JecnUtil.getUserId());
			for (JecnTreeNode selectNode : listNode) {
				selectNode.getJecnTreeBean().setPub(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			}
		} catch (Exception e1) {
			log.error("batchCancelReleaseNotRecordAction is error！", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	/***************************************************************************
	 * 不记录文控发布
	 * 
	 */
	private void directReleaseItemAction() {
		// 是否处于任务审批中
		if (!JecnTool.isBeInTask(selectNode)) {
			return;
		}
		// 检查错误信息
		if (ErrorCommon.INSTANCE.checkFileBaseInfo(selectNode.getJecnTreeBean().getId())) {
			return;
		}
		// 提示是否发布
		int option = JecnOptionPane
				.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("isnotRemberPublish"),
						null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			ConnectionPool.getFileAction().directRelease(selectNode.getJecnTreeBean().getId(), JecnUtil.getUserId());
			selectNode.getJecnTreeBean().setPub(true);
			selectNode.getJecnTreeBean().setUpdate(false);
			((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			// 发布成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
		} catch (Exception e1) {
			log.error("directReleaseItemAction is error！", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	/***************************************************************************
	 * 撤销发布
	 * 
	 */
	private void cancelReleaseItemAction() {
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("ifCancelRelease"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			ConnectionPool.getFileAction().cancelRelease(selectNode.getJecnTreeBean().getId(), JecnUtil.getUserId());
			selectNode.getJecnTreeBean().setPub(false);
			((DefaultTreeModel) jTree.getModel()).reload(selectNode);
		} catch (Exception e) {
			log.error("cancelReleaseItemAction is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
	}

	/**
	 * 文控信息
	 */
	private void historyItemAction() {
		RuleDoControlPanel ruleDoControlPanel = new RuleDoControlPanel(selectNode,
				selectNode.getJecnTreeBean().getId(), 1);
		ruleDoControlPanel.setVisible(true);
	}

	/**
	 * 文件发布
	 * 
	 */
	private void pubFile() {
		if (selectNode == null) {
			// TODO 选中节点为空
			return;
		}
		// 是否处于任务审批中
		if (!JecnTool.isBeInTask(selectNode)) {
			return;
		}
		if (!JecnTreeCommon.canPub(selectNode)) {
			return;
		}
		// 检查错误信息
		if (ErrorCommon.INSTANCE.checkFileBaseInfo(selectNode.getJecnTreeBean().getId())) {
			return;
		}
		FilePublishDialog filePublishDialog = new FilePublishDialog(selectNode);
		filePublishDialog.setVisible(true);
		if (filePublishDialog.isOkBut()) {
			JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean().getTreeNodeType().toString());
			selectNode.getJecnTreeBean().setPub(true);
			selectNode.getJecnTreeBean().setUpdate(false);
			((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			// 发布成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
		} else {
			return;
		}
	}

	/**
	 * 
	 * 上传或更新文件任务
	 */
	private void fileAppItemAction(int approveType) {
		if (selectNode == null) {
			// TODO 选中节点为空
			return;
		}

		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		if (approveType == 2) {
			boolean isPub = treeBean.isPub(); // 是否发布
			if (!isPub) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileUnpublished"));
				return;
			}
		}
		// 判断编号是否必填
		if (JecnConfigTool.isLiBangLoginType() && DrawCommon.isNullOrEmtryTrim(treeBean.getNumberId())) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("numberIsNull"));
			return;
		}

		try {
			boolean isInTask = JecnTool.submitToTask(treeBean);
			if (isInTask) {
				return;
			}
			// 检查错误信息
			if (ErrorCommon.INSTANCE.checkFileBaseInfo(selectNode.getJecnTreeBean().getId())) {
				return;
			}
		} catch (Exception e) {
			log.error("fileAppItemAction is error!" + "treeBean.getTreeNodeType() = "
					+ treeBean.getTreeNodeType().toString(), e);
		}
		if (TreeNodeType.file == selectNode.getJecnTreeBean().getTreeNodeType()) {// 判断节点是文件
			try {
				if (ConnectionPool.getTaskRecordAction().isInTask(selectNode.getJecnTreeBean().getId(),
						selectNode.getJecnTreeBean().getTreeNodeType())) {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));

					return;
				}
			} catch (Exception e) {
				log.error("fileAppItemAction is error！", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("approveing"));
			}
		}

		if (!JecnTreeCommon.canApprove(selectNode, approveType)) {
			return;
		}
		JecnFileTaskJDialog fileTaskJDialog = new JecnFileTaskJDialog(selectNode, jTree, approveType);
		// 设置显示在最前
		fileTaskJDialog.setModal(true);
		fileTaskJDialog.setVisible(true);
	}

	/**
	 * 创建目录
	 */
	private void createDirPerformed() {
		if (selectNode == null) {
			return;
		}
		AddFileDirDialog addFileDirDialog = new AddFileDirDialog(selectNode, jTree);
		addFileDirDialog.setVisible(true);
	}

	/***************************************************************************
	 * 文件属性
	 */
	private void filePropertyPerformed() {
		if (selectNode == null) {
			return;
		}
		// 权限验证
		if (JecnTreeCommon.isDesigner(selectNode) && !JecnTool.isBeInTask(selectNode)) {// 如果是设计者，并且处于任务中退出
			return;
		}
		FilePropertyDialog filePropertyDialog = new FilePropertyDialog(selectNode, jTree);
		filePropertyDialog.setVisible(true);
	}

	/**
	 * 文件版本信息
	 */
	protected void versionFilePerformed() {
		if (selectNode == null) {
			return;
		}
		VersionFileDialog versionFileDialog = new VersionFileDialog(selectNode, jTree);
		versionFileDialog.setVisible(true);
	}

	/**
	 * 文件使用情况
	 */
	protected void usageFilePerformed() {
		if (selectNode == null) {
			return;
		}
		FileUsageDialog fileUsageDialog = new FileUsageDialog(selectNode);
		fileUsageDialog.setVisible(true);

	}

	// 上传文件
	private void uploadFileAction() {
		if (selectNode == null) {
			return;
		}
		// 是否存在浏览端，如果存在浏览端，直接上传文件到树节点
		if (JecnConstants.isPubShow()) {
			if (JecnConfigTool.isLiBangLoginType()) {
				AddLiBangFileDialog fileDialog = new AddLiBangFileDialog(selectNode, jTree);
				fileDialog.setVisible(true);
			} else {
				AddFileDialog addFileDialog = new AddFileDialog(selectNode, jTree);
				addFileDialog.setVisible(true);
			}
		} else {
			// 获取默认路径
			String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileManageDirectory();
			// 获取截取后正确的路径
			if (pathUrl != null && !"".equals(pathUrl)) {
				pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
			}
			JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
			fileChoose.setEidtTextFiled(false);

			// 可以同时上传多个文件
			fileChoose.setMultiSelectionEnabled(true);
			// 设置标题
			fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
			int i = fileChoose.showSaveDialog(this);
			if (i == JFileChooser.APPROVE_OPTION) {
				// 选择的文件
				File[] files = fileChoose.getSelectedFiles();
				// 获取输出文件的路径
				String filePath = fileChoose.getSelectedFile().getPath();
				// 修改配置文件中的路径
				JecnSystemData.readCurrSystemFileIOData().setSaveFileManageDirectory(filePath);
				JecnSystemData.writeCurrSystemFileIOData();

				F: for (File f : files) {
					Object[] obj = new Object[2];
					// 文件名称
					obj[0] = JecnUtil.getFileName(f.getPath());
					// 文件路径
					obj[1] = f.getPath();
					fileNamePathList.add(obj);
				}

				UploadIsPubShowTask uploadIsPubShowTask = new UploadIsPubShowTask();
				JecnLoading.start(uploadIsPubShowTask);
			}
		}

	}

	// 排序
	private void treeSortAction() {
		FileSortDialog sd = new FileSortDialog(selectNode, jTree);
		sd.setVisible(true);
	}

	// 节点移动
	private void treeMoveAction() {
		if (!JecnDesignerCommon.validateRepeatNodesName(listNode)) {
			return;
		}
		FileMoveNodeDialog fileMoveNodeDialog = new FileMoveNodeDialog(listNode, jTree);
		fileMoveNodeDialog.setFocusableWindowState(true);
		fileMoveNodeDialog.setVisible(true);
	}

	// 打开文件
	private void openFileAction() {
		JecnDesignerCommon.openFile(selectNode.getJecnTreeBean().getId());
	}

	/***************************************************************************
	 * 刷新
	 */
	private void refreshItemAction() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getFileAction().getChildFiles(id, JecnConstants.projectId);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("refurbishPerformed is error", ex);
		}
	}

	/***************************************************************************
	 * 重命名
	 */
	private void reNamePerformed() {
		EditNameDialog editNameDialog = new EditNameDialog(selectNode, jTree);
		editNameDialog.setVisible(true);
	}

	/***************************************************************************
	 * 删除
	 */
	private void deleteMenuPerformed() {
		// 是否删除提示框
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		if (listNode.size() == 0) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode node : listNode) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		try {
			if (!JecnFileCommon.fileIsDel(listIds)) {
				return;
			}

			// 删除节点
			ConnectionPool.getFileAction().deleteFileManage(listIds, JecnConstants.projectId,
					JecnConstants.getUserId(), listNode.get(0).getJecnTreeBean().getTreeNodeType());
			/** 删除节点 */
			JecnTreeCommon.removeNodes(jTree, listNode);
			// 要删除的数据名称
			if (jecnManageDialog != null) {
				Vector<Vector<Object>> vs = ((DefaultTableModel) jecnManageDialog.getJecnTable().getModel())
						.getDataVector();
				for (int i = vs.size() - 1; i >= 0; i--) {
					Vector<Object> vob = vs.get(i);
					for (Long longID : listIds) {
						if (vob.get(0).toString().equals(longID.toString())) {
							((DefaultTableModel) jecnManageDialog.getJecnTable().getModel()).removeRow(i);
						}
					}
				}
			} else if (fileChooseDialog != null) {
				// 结果集显示Table
				Vector<Vector<Object>> vs = ((DefaultTableModel) fileChooseDialog.getResultTable().getModel())
						.getDataVector();
				for (int i = vs.size() - 1; i >= 0; i--) {
					Vector<Object> vob = vs.get(i);
					for (Long longID : listIds) {
						if (vob.get(0).toString().equals(longID.toString())) {
							((DefaultTableModel) fileChooseDialog.getResultTable().getModel()).removeRow(i);
							fileChooseDialog.getListJecnTreeBean().remove(i);
						}
					}
				}
				// 搜索集显示Table
				Vector<Vector<Object>> vss = ((DefaultTableModel) fileChooseDialog.getSearchTable().getModel())
						.getDataVector();
				for (int i = vss.size() - 1; i >= 0; i--) {
					Vector<Object> vob = vss.get(i);
					for (Long longID : listIds) {
						if (vob.get(0).toString().equals(longID.toString())) {
							((DefaultTableModel) fileChooseDialog.getSearchTable().getModel()).removeRow(i);
							fileChooseDialog.getListJecnTreeBean().remove(i);
						}
					}
				}
			}
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("deleteSuccess"));
		} catch (Exception e) {
			log.error("deleteMenuPerformed is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}
		// }

	}

	/***************************************************************************
	 * 显示或隐藏
	 */
	private void hideItemPerformed() {
		// 0 隐藏 1 显示
		int hideValue = JecnProperties.getValue("folderHide").equals(hideItem.getText()) ? 0 : 1;
		try {
			Long folderId = selectNode.getJecnTreeBean().getId();
			ConnectionPool.getFileAction().updateHideFiles(folderId, hideValue);
			boolean hideFlag = hideValue == 1 ? true : false;
			selectNode.getJecnTreeBean().setPub(hideFlag);
			// 刷新子节点的Hide值
			setFolderChildHideValue(hideFlag, selectNode);
		} catch (Exception e) {
			log.error("hideItemPerformed is error！", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
	}

	/**
	 * @author weidp
	 * @description 更新选中节点的子节点中，类型为文件夹的节点的Hide值
	 * @param hideFlag
	 * @param node
	 */
	private void setFolderChildHideValue(boolean hideFlag, JecnTreeNode node) {
		Enumeration e = null;
		if (node.children() != null) {
			e = node.children(); // 获取子节点集合
		}
		while (e.hasMoreElements()) {
			Object o = e.nextElement();
			JecnTreeNode treeNode = (JecnTreeNode) o;
			if (treeNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.fileDir)) {
				treeNode.getJecnTreeBean().setPub(hideFlag);
				if (!treeNode.isLeaf()) { // 非叶子节点的情况下进行递归
					setFolderChildHideValue(hideFlag, treeNode);
				}
			}
		}
	}

	/**
	 * 初始化
	 */
	private void init() {
		createDir.setVisible(false);
		reName.setVisible(false);
		uploadFile.setVisible(false);
		releaseItem.setVisible(false);
		directReleaseItem.setVisible(false);
		cancelReleaseItem.setVisible(false);
		updateFile.setVisible(false);
		submitAppItem.setVisible(false);
		desuetudeItem.setVisible(false);
		openFile.setVisible(false);
		fileProperty.setVisible(false);
		usageFile.setVisible(false);
		versionFile.setVisible(false);
		refresh.setVisible(false);
		hideItem.setVisible(false);
		accessAuthorityItem.setVisible(false);
		deleteMenu.setVisible(false);
		treeMove.setVisible(false);
		treeSort.setVisible(false);
		separatorOne.setVisible(false);
		separatorTwo.setVisible(false);
		separatorThree.setVisible(false);
		historyItem.setVisible(false);
		separatorFour.setVisible(false);
		batchReleaseNotReCordItem.setVisible(false);
		batchCancelReleaseItem.setVisible(false);
		taskEdit.setVisible(false);
		autoCodeMenu.setVisible(false);
	}

	private void initAddMenu() {
		this.add(createDir);
		this.add(reName);
		this.add(uploadFile);
		this.add(separatorOne);
		this.add(openFile);
		this.add(updateFile);
		this.add(separatorTwo);// 添加分隔符
		this.add(submitAppItem);
		this.add(pubNoteItem);
		this.add(releaseItem);// 发布
		this.add(directReleaseItem);// 发布-不记录版本
		this.add(cancelReleaseItem);// 撤销发布
		this.add(batchReleaseNotReCordItem);// 批量发布（不记录文控）
		this.add(batchCancelReleaseItem);
		this.add(desuetudeItem);
		this.add(taskEdit);
		this.add(separatorThree);// 添加分隔符
		this.add(refresh);
		this.add(deleteMenu);
		this.add(hideItem);
		this.add(treeMove);
		this.add(treeSort);
		this.add(separatorFour);// 添加分隔符
		this.add(fileProperty);
		this.add(usageFile);
		this.add(versionFile);
		this.add(accessAuthorityItem);
		this.add(historyItem);
		if (JecnConfigTool.isShowAutoNumber()) {// 是否允许自动编号
			this.add(autoCodeMenu);
		}

	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		// JecnTreeCommon.autoExpandNode(jTree, selectNode);
		// refreshItemAction();
		init();
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case fileRoot:
			refresh.setVisible(true);
			if (JecnDesignerCommon.isAdmin()) {
				// automaticDir.setVisible(true);
				createDir.setVisible(true);
				treeSort.setVisible(true);
			}
			taskEdit.setVisible(false);
			break;
		// 目录节点
		case fileDir:
			refresh.setVisible(true);
			int authTypeFileDir = JecnTreeCommon.isAuthNode(selectNode);
			if (authTypeFileDir == 1 || authTypeFileDir == 2) {
				separatorOne.setVisible(true);
				createDir.setVisible(true);
				uploadFile.setVisible(true);
				accessAuthorityItem.setVisible(true);
				treeSort.setVisible(true);
				if (authTypeFileDir == 2) {
					reName.setVisible(true);
					deleteMenu.setVisible(true);
					treeMove.setVisible(true);
				}
				separatorFour.setVisible(true);
			}
			if (JecnDesignerCommon.isAdmin()) {
				hideItem.setVisible(true);
				taskEdit.setVisible(true);
				syncHideValueToHideItem();
			}
			historyItem.setVisible(false);
			// 自动生成的目录
			if (isAllowAutoCreateDir && selectNode.getJecnTreeBean().getRelationId() != null) {
				createDir.setVisible(false);
				reName.setVisible(false);
				deleteMenu.setVisible(false);
				treeMove.setVisible(false);
			}
			if (!JecnDesignerCommon.isAdmin() && selectNode.getJecnTreeBean().getNodeType() == 1) {// 自动生成
				deleteMenu.setVisible(false);
			}
			break;
		// 文件节点
		case file:
			refresh.setVisible(true);
			int authTypeFile = JecnTreeCommon.isAuthNode(selectNode);
			if (authTypeFile == 2 || authTypeFile == 1) {
				autoCodeMenu.setVisible(true);
				separatorOne.setVisible(false);
				openFile.setVisible(true);
				fileProperty.setVisible(true);
				usageFile.setVisible(true);
				versionFile.setVisible(true);
				updateFile.setVisible(true);
				separatorTwo.setVisible(true);
				historyItem.setVisible(true);
				if (JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignFileAdmin()) {
					// directUpdateFile.setVisible(true);
					directReleaseItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode,
							JecnConstants.loginBean.isDesignFileAdmin()));
					releaseItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignFileAdmin()));
					// 撤销发布
					cancelReleaseItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode,
							JecnConstants.loginBean.isDesignFileAdmin()));
					if (selectNode.getJecnTreeBean().isPub()) {
						cancelReleaseItem.setEnabled(true);
					} else {
						cancelReleaseItem.setEnabled(false);
					}
					separatorThree.setVisible(true);

				}
				separatorFour.setVisible(true);
				submitAppItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignFileAdmin()));
				desuetudeItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignFileAdmin()));
				if (authTypeFile == 2) {
					deleteMenu.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignFileAdmin()));
					treeMove.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignFileAdmin()));
				}
				if (!deleteMenuVisible()) {
					deleteMenu.setVisible(false);
				}
				taskEdit.setVisible(true);
				if (selectNode.getJecnTreeBean().isPub() && selectNode.getJecnTreeBean().getApproveType() == -1) {
					pubNoteItem.setVisible(true);
				}
				JecnUtil.setMenuDisableThenInTaskExceptAuthorAdmin(selectNode, reName, updateFile);
				if (!JecnDesignerCommon.isAdmin() && selectNode.getJecnTreeBean().getNodeType() == 1) {// 自动生成
					deleteMenu.setVisible(false);
				}
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 是否可以执行删除操作
	 * 
	 * @return
	 */
	private boolean deleteMenuVisible() {
		if (!(JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignFileAdmin())
				&& selectNode.getJecnTreeBean().isPub()) {// 非管理员且非发布的文件，可有删除权限
			return false;
		}
		return true;
	}

	/**
	 * 节点为文件夹时执行此方法，更改控制显示隐藏的Item的Text值 根据节点的isPub属性（来源于文件夹的Hide值），展示对应的Text值
	 * 0-->false text为隐藏 1-->true 显示
	 */
	private void syncHideValueToHideItem() {
		// 父节点为隐藏状态时子节点不显示按钮
		JecnTreeNode pNode = (JecnTreeNode) selectNode.getParent();
		if (!"0".equals(pNode.getJecnTreeBean().getId().toString()) && !pNode.getJecnTreeBean().isPub()) {
			hideItem.setVisible(false);
			return;
		}

		hideItem.setText(JecnProperties.getValue("folderShow"));
		if (selectNode.getJecnTreeBean().isPub()) {
			hideItem.setText(JecnProperties.getValue("folderHide"));
		}
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		desuetudeItem.setVisible(false);
		boolean isFile = listNode.get(0).getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.file) ? true : false;
		if (JecnTreeCommon.isAuthNodes(listNode)) {

			if (isFile) {
				// 删除
				deleteMenu.setVisible(true);
				// 移动
				treeMove.setVisible(true);
				if (JecnDesignerCommon.isAuthorAdmin() || JecnDesignerCommon.isFileAdmin()) {
					// 批量发布
					batchReleaseNotReCordItem.setVisible(true);
					boolean batchCancelEnable = true;
					for (JecnTreeNode node : listNode) {
						if (!node.getJecnTreeBean().isPub()) {
							batchCancelEnable = false;
							break;
						}
					}
					batchCancelReleaseItem.setVisible(true);
					batchCancelReleaseItem.setEnabled(batchCancelEnable);
				}
			} else {
				boolean isOperator = true;
				for (JecnTreeNode node : listNode) {
					if (node.getJecnTreeBean().getRelationId() != null) {
						isOperator = false;
						break;
					}
				}
				if (isOperator) {
					// 删除
					deleteMenu.setVisible(true);
					// 移动
					treeMove.setVisible(true);
				}
			}
		}
	}

	public void setListNode(List<JecnTreeNode> listNode) {
		this.listNode = listNode;
		if (listNode.size() == 1) {
			this.selectNode = listNode.get(0);
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

	public List<JecnTreeNode> getListNode() {
		return listNode;
	}

	class UploadIsPubShowTask extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			try {

				Long relatedFlowId = null;
				if (JecnConfigTool.isArchitectureUploadFile()) {
					JecnFileBeanT fileBeanT = ConnectionPool.getFileAction().getFilBeanById(
							selectNode.getJecnTreeBean().getId());
					relatedFlowId = fileBeanT.getFlowId();
				}
				List<JecnFileBeanT> list = new ArrayList<JecnFileBeanT>();
				// 用于验证文件是否重复
				List<String> names = new ArrayList<String>();
				// 获取父节点的hide值
				int hide = selectNode.getJecnTreeBean().isPub() ? 1 : 0;
				for (Object[] obj : fileNamePathList) {
					JecnFileBeanT fileBean = null;
					fileBean = new JecnFileBeanT();
					fileBean.setFileName(obj[0].toString());
					fileBean.setPeopleID(JecnConstants.loginBean.getJecnUser().getPeopleId());
					fileBean.setPerFileId(selectNode.getJecnTreeBean().getId());
					fileBean.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
					fileBean.setIsDir(1);// 1是文件 0是目录
					fileBean.setOrgId(null);
					fileBean.setDocId("");
					fileBean.setHide(hide); // 0 隐藏 1显示
					fileBean.setIsPublic(0L);
					fileBean.setNodeType(0);
					fileBean.setSortId(JecnTreeCommon.getMaxSort(selectNode) + 1);
					fileBean.setProjectId(JecnConstants.projectId);
					fileBean.setFileStream(JecnUtil.changeFileToByte(obj[1].toString()));
					fileBean.setDelState(0);// 0:未删除；1：已删除
					fileBean.setFlowId(relatedFlowId);
					list.add(fileBean);
					names.add(obj[0].toString());
				}

				try {
					String[] ns = ConnectionPool.getFileAction().validateAddName(names,
							selectNode.getJecnTreeBean().getId(), 1);
					if (ns != null && ns.length > 0) {
						StringBuffer tip = new StringBuffer();
						for (String n : ns) {
							if (tip.length() == 0) {
								tip.append(n).append(",");
							} else {
								tip.append("<br>");
								tip.append(n).append(",");
							}

						}
						String tipText = tip.toString().substring(0, tip.toString().length() - 1) + " "
								+ JecnProperties.getValue("haved");
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), tipText);
						return null;
					}
					ConnectionPool.getFileAction().addFiles(list, null, false, -1);
					List<JecnTreeBean> listNode = ConnectionPool.getFileAction().getChildFiles(
							selectNode.getJecnTreeBean().getId(), JecnConstants.projectId);
					JecnTreeCommon.refreshNode(selectNode, listNode, jTree);
					JecnTreeCommon.autoExpandNode(jTree, selectNode);
				} catch (Exception e) {
					log.error("新建文件出错！", e);
				}
			} catch (Exception e) {
				log.error("文件上传异常", e);
				done();
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}
}
