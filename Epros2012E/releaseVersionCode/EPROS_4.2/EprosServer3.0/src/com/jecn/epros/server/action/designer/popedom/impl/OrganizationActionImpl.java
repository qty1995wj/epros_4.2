package com.jecn.epros.server.action.designer.popedom.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.action.designer.popedom.IOrganizationAction;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.DownloadPopedomBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.popedom.PositonInfo;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.popedom.IOrgAccessPermissionsService;
import com.jecn.epros.server.service.popedom.IOrganizationService;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class OrganizationActionImpl implements IOrganizationAction {

	private IOrganizationService organizationService;
	private IOrgAccessPermissionsService orgAccessPermissionsService;

	public IOrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(IOrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	public IOrgAccessPermissionsService getOrgAccessPermissionsService() {
		return orgAccessPermissionsService;
	}

	public void setOrgAccessPermissionsService(IOrgAccessPermissionsService orgAccessPermissionsService) {
		this.orgAccessPermissionsService = orgAccessPermissionsService;
	}

	@Override
	public List<JecnTreeBean> getChildOrgs(Long pId, Long projectId) throws Exception {
		return organizationService.getChildOrgs(pId, projectId);
	}

	@Override
	public List<JecnTreeBean> getAllOrgs(Long projectId) throws Exception {
		return organizationService.getAllOrgs(projectId);
	}

	@Override
	public List<JecnTreeBean> getAllOrgAndPosition(Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JecnTreeBean> getChildOrgsAndPositon(Long id, Long projectId, boolean isLeaf) throws Exception {
		return organizationService.getChildOrgsAndPositon(id, projectId, isLeaf);
	}

	@Override
	public Long addOrg(String name, Long pId, String orgNumber, Long projectId, int sortId, String orgNum)
			throws Exception {
		// TODO Auto-generated method stub
		return organizationService.addOrg(name, pId, orgNumber, projectId, sortId, orgNum);
	}

	@Override
	public Long addPosition(JecnFlowOrgImage jecnFlowOrgImage) throws Exception {
		return organizationService.addPosition(jecnFlowOrgImage);
	}

	@Override
	public JecnFlowOrg getOrgInfo(Long id) throws Exception {
		return organizationService.get(id);
	}

	@Override
	public PositonInfo getPositionInfo(Long id) throws Exception {
		return organizationService.getPositionInfo(id);
	}

	@Override
	public void reNameOrg(Long id, String newName) throws Exception {
		organizationService.reNameOrg(id, newName);
	}

	@Override
	public void reNamePosition(Long id, String newName) throws Exception {
		organizationService.reNamePosition(id, newName);
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId) throws Exception {
		organizationService.updateSortNodes(list, pId, projectId);

	}

	@Override
	public void deleteOrgs(List<Long> listIds, Long updatePersonId) throws Exception {
		organizationService.deleteOrgs(listIds, updatePersonId);
	}

	@Override
	public void deletePosition(List<Long> listIds) throws Exception {
		organizationService.deletePosition(listIds);
	}

	@Override
	public void moveNodesOrg(List<Long> listIds, Long pId, String orgNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception {
		organizationService.moveNodesOrg(listIds, pId, orgNumber, updatePersonId, moveNodeType);

	}

	@Override
	public void moveNodesPos(List<Long> listIds, Long pId, String posNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception {
		organizationService.moveNodesPos(listIds, pId, posNumber, updatePersonId, moveNodeType);

	}

	@Override
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type) throws Exception {

		return orgAccessPermissionsService.getOrgAccessPermissions(relateId, type);
	}

	@Override
	public List<JecnTreeBean> getPositionAccessPermissions(Long relateId, int type) throws Exception {
		return null;
		// // TODO Auto-generated method stub
		// return positionAccessPermissionsService.getPositionAccessPermissions(
		// relateId, type);
	}

	@Override
	public void updateOrgDuty(Long orgId, String duty, String orgNum) throws Exception {
		organizationService.updateOrgDuty(orgId, duty, orgNum);
	}

	@Override
	public void updatePosition(JecnPositionFigInfo positionInfoBean, String upPositionIds, String lowerPositionIds,
			String innerRuleIds, String outRuleIds, Long posId, String posName, String posNum) throws Exception {
		organizationService.updatePosition(positionInfoBean, upPositionIds, lowerPositionIds, innerRuleIds, outRuleIds,
				posId, posName, posNum);
	}

	@Override
	public List<JecnTreeBean> searchPositionByName(String name, Long projectId) throws Exception {

		return organizationService.searchPositionByName(name, projectId);
	}

	@Override
	public List<JecnTreeBean> searchPositionByName(Map<String, Object> nameMap, Long projectId) throws Exception {
		return organizationService.searchPositionByName(nameMap, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodesOrg(Long orgId, Long projectId) throws Exception {
		return organizationService.getPnodesOrg(orgId, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodesPos(Long posId, Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return organizationService.getPnodesPos(posId, projectId);
	}

	@Override
	public JecnOpenOrgBean openOrgMap(long orgId) throws Exception {
		// TODO Auto-generated method stub
		return organizationService.openOrgMap(orgId);
	}

	@Override
	public void saveOrgMap(JecnFlowOrg jecnFlowOrg, List<JecnFlowOrgImage> saveList, List<JecnFlowOrgImage> updateList,
			List<Long> deleteListLine, List<Long> deleteList) throws Exception {
		organizationService.saveOrgMap(jecnFlowOrg, saveList, updateList, deleteListLine, deleteList);
	}

	@Override
	public Set<Long> deleteIdsOrg(List<Long> ListIds, Long updatePeopleId) throws Exception {
		return organizationService.deleteIdsOrg(ListIds, updatePeopleId);
	}

	@Override
	public List<Object[]> getDelsOrg(Long projectId) throws Exception {
		return organizationService.getDelsOrg(projectId);
	}

	@Override
	public void updateRecoverDelOrg(List<Long> ids) throws Exception {
		organizationService.updateRecoverDelOrg(ids);
	}

	@Override
	public List<JecnTreeBean> getParentsContainSelf(Long projectId, Long id) throws Exception {
		return organizationService.getParentsContainSelf(projectId, id);
	}

	@Override
	public List<JecnFlowOrg> getOrgInfos(Long pId) throws Exception {
		return organizationService.getOrgInfos(pId);
	}

	@Override
	public boolean updateOrgCanOpen(Long userId, Long id) throws Exception {
		return organizationService.updateOrgCanOpen(userId, id);
	}

	@Override
	public void moveEdit(Long id) throws Exception {
		organizationService.moveEdit(id);

	}

	@Override
	public LookPopedomBean getAccessPermissions(Long relateId, int type) throws Exception {
		return orgAccessPermissionsService.getAccessPermissions(relateId, type);
	}

	@Override
	public LookPopedomBean getOnlyAccessPermissions(Long relateId, int type) throws Exception {
		return orgAccessPermissionsService.getOnlyAccessPermissions(relateId, type);
	}

	@Override
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, int orgType, int posType, int userType, Long projectId, int posGroupType, int saveMode,
			AccessId accId, Long userId) throws Exception {
		orgAccessPermissionsService.saveOrUpdateAccessPermissions(relateId, type, nodeType, isPublic, securityType,
				orgType, posType, userType, projectId, posGroupType, saveMode, accId, userId);

	}

	@Override
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, String orgIds, int orgType, String posIds, int posType, int userType, Long projectId,
			String posGroupIds, int posGroupType, int saveMode) throws Exception {
		orgAccessPermissionsService.saveOrUpdateAccessPermissions(relateId, type, nodeType, isPublic, securityType,
				orgIds, orgType, posIds, posType, userType, projectId, posGroupIds, posGroupType, saveMode);

	}

	@Override
	public List<JecnTreeBean> getPositionsByIds(String ids) throws Exception {
		return organizationService.getPositionsByIds(ids);
	}

	@Override
	public boolean validateAddUpdatePosNum(String posNum, Long id) throws Exception {
		return organizationService.validateAddUpdatePosNum(posNum, id);
	}

	@Override
	public boolean validateAddUpdateOrgNum(String orgNum, Long id) throws Exception {
		return organizationService.validateAddUpdateOrgNum(orgNum, id);
	}

	// -----------------------------新添加--------------------

	@Override
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pid) throws Exception {

		return organizationService.getRecycleJecnTreeBeanList(projectId, pid);
	}

	@Override
	public void recoverCur(List<Long> recycleNodesIdList) throws Exception {

		organizationService.recoverCurOrgs(recycleNodesIdList);

	}

	@Override
	public void recoverCurAndChild(List<Long> recycleNodesIdList, Long projectId) throws Exception {

		organizationService.recoverCurAndChildOrgs(recycleNodesIdList, projectId);
	}

	@Override
	public void trueDelete(List<Long> ListIds, Long updatePeopleId) throws Exception {
		organizationService.deleteIdsOrg(ListIds, updatePeopleId);

	}

	@Override
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId) throws Exception {

		return organizationService.getDelAuthorityByName(name, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception {

		return organizationService.getRecyleSelectedNodes(id, projectId);
	}

	@Override
	public JecnTreeBean getJecnTreeBeanByposId(Long posId) throws Exception {
		// TODO Auto-generated method stub
		return organizationService.getJecnTreeBeanByposId(posId);
	}

	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		return organizationService.searchRoleAuthByName(name, projectId, peopleId);
	}

	public List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception {
		return organizationService.searchByName(name, projectId);
	}

	@Override
	public JecnTreeBean getCurrentOrg(Long orgId, Long projectId) throws Exception {
		return organizationService.getCurrentOrg(orgId, projectId);
	}

	@Override
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId, Long startId) throws Exception {
		return organizationService.getDelAuthorityByName(name, projectId, startId);
	}

	@Override
	public JecnTreeBean getCurrentOrgContainDel(Long orgId, Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return organizationService.getCurrentOrgContainDel(orgId, projectId);
	}

	@Override
	public List<JecnTreeBean> searchPositionByName(String name, Long projectId, Long startId) throws Exception {
		return organizationService.searchPositionByName(name, projectId, startId);
	}

	@Override
	public DownloadPopedomBean getFlowFileDownloadPermissions(Long id) throws Exception {
		return organizationService.getFlowFileDownloadPermissions(id);
	}

	@Override
	public void saveOrUpdateFlowFileDownloadPermissions(Long flowId, String posIds) throws Exception {
		organizationService.saveOrUpdateFlowFileDownloadPermissions(flowId, posIds);
	}

	/**
	 * 暂时废弃的接口
	 */
	@Override
	public boolean peopleHasFlowFileDownloadPerm(Set<Long> flowSet, Long userId) throws Exception {
		return orgAccessPermissionsService.peopleHasFlowFileDownloadPerm(flowSet, userId);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception {
		return organizationService.getRoleAuthChildFiles(pId, projectId, peopleId);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildPersons(Long pId, Long projectId, Long peopleId) throws Exception {
		if (peopleId == null) {
			return organizationService.getChildOrgsAndPositon(pId, projectId, true);
		}
		return organizationService.getRoleAuthChildPersonOrgs(pId, projectId, peopleId);
	}

	@Override
	public List<JecnDictionary> getDictionarys(DictionaryEnum name) throws Exception {
		return organizationService.getDictionarys(name);
	}

	@Override
	public AccessId getPepoleOrgAndPosAndPosPosGroup(long userId,Long projectId) {
		return organizationService.getPepoleOrgAndPosAndPosPosGroup(userId,projectId);
	}

}
