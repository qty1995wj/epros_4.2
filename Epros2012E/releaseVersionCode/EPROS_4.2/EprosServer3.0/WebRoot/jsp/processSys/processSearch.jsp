<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div  style="height: 100%;">
			<div id="search">
				<div id="processSearchText" style="overflow: hidden;"
					class="divBorder" align="center">
					<table width="100%">
						<tr align="left">
							<td height="25px">
								<input type="radio" name="radiobutton" value="radiobutton"
									id="processRadio" checked onclick="selectRadioClick()">
								<s:text name="process" />
								<input type="radio" name="radiobutton" value="radiobutton"
									id="processMapRadio" onclick="selectRadioClick()">
								<s:text name="processMap" />
							</td>
							<td style="vertical-align: middle;" align="right">
								<a onClick="toggle(processSearchText)" style="cursor: pointer;"
									class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
					</table>
					<div id="processSearch" style="display: block; height: 110px;"
						class="minWidth" align="center">
						<table width="100%">
							<tr>
								<td width="10%" height="25px" align="right">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
								</td>
								<td width="40%" align="left">
									<input type="text" style="width: 80%" id="processName_id"
										maxLength="32" />
								</td>

								<td width="10%" align="right">
									<!-- 流程编号： -->
									<s:text name="processNumberC"></s:text>
								</td>
								<td width="40%" align="left">
									<input type="text" style="width: 80%" id="processNumber_id"
										maxLength="32" />
								</td>
							</tr>

							<tr>
								<td height="25px;" align="right">
									<!-- 流程类别: -->
									<s:text name="processTypeC" />
								</td>
								<td align="left">
									<select style="width: 80%" id="processType_id">
										<option value="-1">
											<s:text name="all" />
										</option>
										<s:iterator value="#request.processTypeList">
											<option value="<s:property value="typeId" />">
												<s:property value="typeName" />
											</option>
										</s:iterator>
									</select>
								</td>


								<td align="right">
									<!-- 密级: -->
									<s:text name="intensiveC" />
								</td>
								<td align="left">
									<select style="width: 80%" id="processIntensive_id">
										<option value="-1">
											<s:text name="all" />
										</option>
										<option value="1">
											<s:property value="#application.public"/>
										</option>
										<option value="0">
											${secret}
										</option>
									</select>
								</td>

							</tr>

							<tr>
								<td height="25px" align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_process_id"
										onkeyup="search(orgDutyName_process_id,orgDutyId_process_id,search_process_orgDutyName,'org');"
										onblur="divNode(search_process_orgDutyName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_process_id","orgDutyId_process_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br>
									<div id="search_process_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td align="right">
									<!-- 参与岗位: -->
									<s:text name="participateInPostC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="participateInPostName_process_id"
										onkeyup="search(participateInPostName_process_id,participateInPostId_process_id,search_process_participateInPost,'pos');"
										onblur="divNode(search_process_participateInPost)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("position","participateInPostName_process_id","participateInPostId_process_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_process_participateInPost"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>
							</tr>
							<tr>
								<td height="25px;" align="right">
									<!-- 查阅部门: -->
									<s:text name="referToTheDepartmentC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgLookName_process_id"
										onkeyup="search(orgLookName_process_id,orgLookId_process_id,search_process_orgLookName,'org');"
										onblur="divNode(search_process_orgLookName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgLookName_process_id","orgLookId_process_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_process_orgLookName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td align="right">
									<!-- 查阅岗位: -->
									<s:text name="referToThePostC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="posLookName_process_id"
										onkeyup="search(posLookName_process_id,posLookId_process_id,search_process_posLookName,'pos');"
										onblur="divNode(search_process_posLookName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("position","posLookName_process_id","posLookId_process_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_process_posLookName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

							</tr>
						</table>
					</div>


					<div id="processMapSearch" style="display: none; height: 110px;"
						class="minWidth" align="center">
						<table width="100%">
							<tr>
								<td height="25px" width="11%;" align="right">
									<!-- 流程地图名称: -->
									<s:text name="processMapNameC" />
								</td>
								<td width="39%" align="left">
									<input type="text" style="width: 80%" id="processMapName_id"
										maxLength="32" />
								</td>


								<td width="11%" align="right">
									<!-- 密集: -->
									<s:text name="intensiveC" />
								</td>
								<td width="39%" align="left">
									<select style="width: 80%" id="processMapIntensive_id">
										<option value="-1">
											<s:text name="all" />
										</option>
										<option value="1">
											<s:property value="#application.public"/>
										</option>
										<option value="0">
											${secret}
										</option>
									</select>
								</td>
							</tr>

							<tr>
								<td height="25px" align="right">
									<!-- 查阅部门: -->
									<s:text name="referToTheDepartmentC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgMapLookName_mapProcess_id"
										onkeyup="search(orgMapLookName_mapProcess_id,orgMapLookId_mapProcess_id,search_process_orgMapLookName,'org');"
										onblur="divNode(search_process_orgMapLookName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgMapLookName_mapProcess_id","orgMapLookId_mapProcess_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_process_orgMapLookName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td align="right">
									<!-- 查阅岗位: -->
									<s:text name="referToThePostC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="posMapLookName_mapProcess_id"
										onkeyup="search(posMapLookName_mapProcess_id,posMapLookId_mapProcess_id,search_process_posMapLookName,'pos');"
										onblur="divNode(search_process_posMapLookName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("position","posMapLookName_mapProcess_id","posMapLookId_mapProcess_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_process_posMapLookName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div id="searchBut" align="center">
					<img src="${basePath}images/iParticipateInProcess/search.gif"
						style="cursor: pointer;" onclick="processSysSearch()" />
					<img src="${basePath}images/iParticipateInProcess/reset.gif"
						style="cursor: pointer;" onclick="processReset()" />
				</div>

				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_process_id" value="-1" />
				<!-- 查阅部门ID -->
				<input type="hidden" id="orgLookId_process_id" value="-1" />
				<!-- 查阅岗位ID -->
				<input type="hidden" id="posLookId_process_id" value="-1" />
				<!-- 参与岗位 -->
				<input type="hidden" id="participateInPostId_process_id" value="-1" />
				<!-- 流程地图查询部门 -->
				<input type="hidden" id="orgMapLookId_mapProcess_id" value="-1">
				<!-- 流程地图查询岗位 -->
				<input type="hidden" id="posMapLookId_mapProcess_id" value="-1">
			</div>

			<div id="processGrid_id" style="width: 100%; height: 100%"></div>
			<div id="processMapGrid_id"
				style="width: 100%; height: 100%; display: none;"></div>
		</div>
	</body>
</html>