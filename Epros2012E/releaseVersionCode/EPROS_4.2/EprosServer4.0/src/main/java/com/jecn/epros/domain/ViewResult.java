package com.jecn.epros.domain;

/**
 * 给页面返回的结果bean，只提示成功还是失败
 */
public class ViewResult {

    public static final ViewResult SUCCESS = new ViewResult(true);
    public static final ViewResult FAIL = new ViewResult(false);

    private boolean success = true;

    public ViewResult(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
