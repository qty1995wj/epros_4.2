package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKJButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.RadioData;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 文件基本信息面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFileBasicInfoPanel extends JecnAbstractPropertyBasePanel implements ActionListener {


	/** 其他项面板 */
	private JecnPanel otherPanel = null;
	/** 文件责任人面板 */
	private JecnPanel flowResponsePanel = null;
	/** 标签 */
	private JLabel flowResponseLab = null;
	/** 人员 */
	private JecnConfigRadioButton peopleRadio = null;
	/** 岗位 */
	private JecnConfigRadioButton posRadio = null;


	public JecnFileBasicInfoPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 表面板
		tableScrollPane = new JecnThreeTableScrollPane(dialog);
		// 其他项面板
		otherPanel = new JecnPanel();
		// 文件责任人面板
		flowResponsePanel = new JecnPanel();
		// 标签
		flowResponseLab = new JLabel(JecnProperties.getValue("task_fileResponsePerC"));
		// 人员
		peopleRadio = new JecnConfigRadioButton(JecnProperties.getValue("person"));
		// 岗位
		posRadio = new JecnConfigRadioButton(JecnProperties.getValue("position"));
		// / 责任人 按钮实现 单选效果
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(peopleRadio);
		bgpp.add(posRadio);
		
		flowResponsePanel.setOpaque(false);
		flowResponsePanel.setBorder(null);
		// 流程责任人：人员
		peopleRadio.setOpaque(false);
		peopleRadio.setSelected(true);
		// 流程责任人：岗位
		posRadio.setOpaque(false);
		// 人员
		peopleRadio.addActionListener(this);
		// 岗位
		posRadio.addActionListener(this);
	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		// 是否存在浏览端，如果存在浏览端则隐藏表格
		if (JecnConstants.isPubShow()) {
			this.add(tableScrollPane, BorderLayout.CENTER);
			this.add(otherPanel, BorderLayout.SOUTH);
		} else {
			this.add(otherPanel, BorderLayout.NORTH);
		}
		// 角色、活动上下限面板、
		Insets insets = new Insets(5, 5, 5, 5);
		GridBagConstraints c = null;
		int rows = 0;
		// 流程责任人面板
		c = new GridBagConstraints(0, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		otherPanel.add(flowResponsePanel, c);
		// 流程责任人:标签
		c = new GridBagConstraints(rows, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		flowResponsePanel.add(flowResponseLab, c);
		rows++;
		// 流程责任人:人员
		c = new GridBagConstraints(rows, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		flowResponsePanel.add(peopleRadio, c);
		rows++;
		// 流程责任人: 岗位
		c = new GridBagConstraints(rows, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets,
				0, 0);
		flowResponsePanel.add(posRadio, c);
	}



	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;
		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String value = itemBean.getValue();
			if (ConfigItemPartMapMark.basicFileResPeople.toString().equals(mark)) {// 文件责任人类型：0:
				// 选择人
				// 1:选择岗位
				peopleRadio.setItemBean(itemBean);
				posRadio.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_FLOW_USER.equals(value)) {// 文件责任人类型是人
					peopleRadio.setSelected(true);
				} else {
					posRadio.setSelected(true);
				}
			}
		}
		// 排序过滤隐藏项
		List<JecnConfigItemBean> showTableItemList = tableScrollPane.getShowItem(configTypeDesgBean.getTableItemList(),
				JecnConfigContents.TYPE_SMALL_ITEM_BASICINFO);
		// 显示表数据
		tableScrollPane.initData(showTableItemList);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigRadioButton) {
			// 流程责任人类型：0: 选择人 1:选择岗位
			JecnConfigRadioButton configRadioButton = (JecnConfigRadioButton) e.getSource();
			if (configRadioButton.isSelected()) {// 选中
				if (configRadioButton == peopleRadio) {// 人员
					peopleRadio.getItemBean().setValue(JecnConfigContents.ITEM_FLOW_USER);
				} else if (configRadioButton == posRadio) {// 岗位
					peopleRadio.getItemBean().setValue(JecnConfigContents.ITEM_FLOW_POS);
				}
			}
		} 
	}

	/**
	 * 
	 * 校验数据
	 * 
	 * @return boolean 校验成功：true ；校验失败：false
	 * 
	 */
	@Override
	public boolean check() {
		return true;
	}

	private JecnUserInfoTextArea getInfoLable() {
		return dialog.getOkCancelJButtonPanel().getInfoLabel();
	}

	private JButton getOkJButton() {
		return dialog.getOkCancelJButtonPanel().getOkJButton();
	}

	/**
	 * 判断字符串是否为数字
	 * 
	 * @param strValidity
	 * @param isSelect
	 * @return true:存在错误信息
	 */
	private boolean getNumCheckInfo(String strValidity) {

		return false;
	}
}
