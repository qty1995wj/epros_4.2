package epros.designer.gui.process.flowtool;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnManageDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;

public class FlowToolManageDialog extends JecnManageDialog {
	private static Logger log = Logger.getLogger(FlowToolManageDialog.class);
	int[] selectedRow = null;

	@Override
	public void buttonOneAction() {
	}

	@Override
	public String buttonOneName() {
		return null;
	}

	@Override
	public void buttonTwoAction() {
		// 判断是否选中Table中的一行，没选中，提示选中一行
		selectedRow = this.getJecnTable().getSelectedRows();
		if (selectedRow.length != 1) {
			// JecnOptionPane.showMessageDialog(this, JecnProperties
			// .getValue("chooseOneRow"));
			this.getjLabelTip().setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		JecnTreeBean jecnTreeBean = this.getSearchList().get(selectedRow[0]);
		// 更新支持工具框
		UpdateFlowToolDialog updateFlowToolDialog = new UpdateFlowToolDialog(jecnTreeBean, this.getjTree());
		updateFlowToolDialog.setVisible(true);
		if (updateFlowToolDialog.isOk()) {
			// 更新Table上的数据
			getSearchList().get(selectedRow[0]).setName(updateFlowToolDialog.getName());
			// 重新获得更新后的数据
			getJecnTable().getModel().setValueAt(updateFlowToolDialog.getName(), selectedRow[0], 0);
			// 得到节点
			JecnTreeNode jecnTreeNode = JecnTreeCommon.getTreeNode(jecnTreeBean, this.getjTree());
			if (jecnTreeNode != null) {
				// 更新树节点
				JecnTreeCommon.reNameNode(this.getjTree(), jecnTreeNode, updateFlowToolDialog.getName());
				// 选中树节点
				JecnTreeCommon.selectNode(this.getjTree(), jecnTreeNode);
			}
		}
	}

	@Override
	public String buttonTwoName() {
		return JecnProperties.getValue("editBtn");
	}

	@Override
	public void buttonThreeAction() {
	}

	@Override
	public String buttonThreeName() {
		return null;
	}

	@Override
	public boolean deleteData(List<Long> listIds) {
		try {
			ConnectionPool.getFlowTool().deleteSustainTools(listIds, JecnUtil.getUserId());
		} catch (Exception e) {
			log.error("FlowToolManageDialog deleteData is error", e);
			return false;
		}
		return true;
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("flowTool");
	}

	@Override
	public String getSerachLabelName() {

		return JecnProperties.getValue("nameC");
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> vector = new Vector<String>();
		vector.add(JecnProperties.getValue("name"));
		vector.add("ID");
		return vector;
	}

	@Override
	public String getTreePanelTitle() {
		return JecnProperties.getValue("flowToolTree");
	}

	@Override
	public JecnTree initJecnTree() {
		// if (JecnConstants.isMysql) {
		// return new RoutineFlowToolTree(0L);
		// } else {
		// return new HighEfficiencyFlowToolTree(this);
		return new HighEfficiencyFlowToolTree(0L, this);
		// }
	}

	@Override
	public boolean isAddButtonOne() {
		return false;
	}

	@Override
	public boolean isAddButtonThree() {
		return false;
	}

	@Override
	public boolean isAddButtonTwo() {
		return true;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		// if (JecnConstants.isMysql) {
		// List<JecnTreeBean> list = JecnTreeCommon.findAllTreeNodes(this
		// .getjTree(), name);
		// return list;
		// } else {
		try {
			return ConnectionPool.getFlowTool().getSustainToolsByName(name);
		} catch (Exception e) {
			log.error("FlowToolManageDialog searchByName is error", e);
			return new ArrayList<JecnTreeBean>();
		}
		// }
	}

	@Override
	public Vector<Vector<String>> updateTableContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		for (JecnTreeBean jecnTreeBean : list) {
			Vector<String> data = new Vector<String>();
			data.add(jecnTreeBean.getName());
			data.add(jecnTreeBean.getId().toString());
			vector.add(data);
		}
		return vector;
	}
}
