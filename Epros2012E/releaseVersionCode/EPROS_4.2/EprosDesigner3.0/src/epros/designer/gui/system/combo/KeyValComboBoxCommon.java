package epros.designer.gui.system.combo;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ItemListener;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JLabel;

import org.apache.log4j.Logger;

import epros.designer.gui.file.AddFileDialog;
import epros.designer.gui.system.BasicComponent;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;

public abstract class KeyValComboBoxCommon implements BasicComponent {
	private static Logger log = Logger.getLogger(AddFileDialog.class);
	private JLabel jecnLab = new JLabel();
	/** 保密级别ComboBox */
	protected KeyValComboBox fileTypeCombox = null;
	private String key;
	protected String firstKey;
	protected boolean visible = true;
	protected static String colon = JecnResourceUtil.getLocale() == Locale.ENGLISH ? ":" : "：";

	public KeyValComboBoxCommon(int row, JecnPanel infoPanel, Insets insets, String key, JecnDialog jecnDialog,
			String labName) {
		this(row, infoPanel, insets, key, jecnDialog, labName, false);
	}

	public KeyValComboBoxCommon(int row, JecnPanel infoPanel, Insets insets, String key, JecnDialog jecnDialog,
			String labName, boolean isRequest) {
		this.key = key;

		GridBagConstraints c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		jecnLab.setText(labName);
		infoPanel.add(jecnLab, c);
		initCombo(key);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(fileTypeCombox, c);
		c = new GridBagConstraints(3, row, 1, 1, 0.0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		if (isRequest) {
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
		if (key == null || "".equals(key)) {
			this.key = firstKey;
		}
	}

	protected void initCombo(String key) {
		Vector model = getListCheckBoxPo();
		fileTypeCombox = new KeyValComboBox(model);
		fileTypeCombox.setSelectedItem(key);
	}

	public abstract Vector getListCheckBoxPo();

	public String getKey() {
		return fileTypeCombox.getSelectedKey();
	}

	public String getValue() {
		return fileTypeCombox.getSelectedValue();
	}

	public CheckBoxPo getObj() {
		return fileTypeCombox.getSelectedObj();
	}

	@Override
	public boolean isUpdate() {
		if (!key.equals(fileTypeCombox.getSelectedKey())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	@Override
	public void setEnabled(boolean enable) {
		fileTypeCombox.setEnabled(false);
	}

	public JLabel getJecnLab() {
		return jecnLab;
	}

	public void setJecnLab(JLabel jecnLab) {
		this.jecnLab = jecnLab;
	}

	@Override
	public boolean isRequest() {
		return false;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		jecnLab.setVisible(visible);
		fileTypeCombox.setVisible(visible);
	}

	public void addItemListener(ItemListener listener) {
		fileTypeCombox.addItemListener(listener);
	}

	public int getSelectedIndex() {
		return fileTypeCombox.getSelectedIndex();
	}

	public void setSelectedIndex(int index) {
		fileTypeCombox.setSelectedIndex(index);
	}

	public void removeItemListener(ItemListener listener) {
		fileTypeCombox.removeItemListener(listener);
	}

}
