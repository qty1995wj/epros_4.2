package com.jecn.Ding;

import java.util.Date;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.dongh.JecnDesKeyTools;
import com.jecn.epros.server.common.BaseAction;
import com.alibaba.fastjson.JSONObject;

public class DingAction extends BaseAction {

	private static final Logger log = Logger.getLogger(DingAction.class);
	
	public void getDingSignature() throws Exception {
		
		String ticket = Dingtools.getJSAPITicket();
		Long timeStamp = new Date().getTime();

		String signature = Dingtools.sign(ticket, "epros", timeStamp, DingAfterItem.getValue("LOCAL_URL"));
		System.out.println("{signature:"+signature+",timeStamp:"+timeStamp+"}");
		outJsonString("{\"signature\":\""+signature+"\",\"timeStamp\":\""+timeStamp+"\"}");
	}
	
	public void getDingLoginUrl() throws Exception {
		String code = this.getRequest().getParameter("code");
		JSONObject json = Dingtools.getUseridByCode(code);
		String userId = json.getString("userid");
		JecnDesKeyTools desTool = new JecnDesKeyTools();
		
		String token = desTool.encode(userId);
		
		String loginUrl = DingAfterItem.getValue("LOGIN_URL")+token;
		
		outJsonString("{\"url\":\""+loginUrl+"\"}");
	}
	
}
