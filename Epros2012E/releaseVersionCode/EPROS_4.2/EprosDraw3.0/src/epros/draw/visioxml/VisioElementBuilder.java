package epros.draw.visioxml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import visioxml.bean.ConnectProperty;
import visioxml.bean.PageProperty;
import visioxml.constant.Constants.CellType;
import visioxml.constant.Constants.LineRoute;
import visioxml.shape.BaseShape;
import visioxml.util.Unit;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnManhattanLineData;

public class VisioElementBuilder {
	// 转换后的数据集合
	private Map<String, BaseShape> visioShapeMap;
	// 转换后的连接线数据
	private List<ConnectProperty> visioConnectList;
	// 画图面板数据
	private JecnFlowMapData flowMapData;

	private VisioElementBuilderHelp buildHelp;

	public VisioElementBuilder(JecnFlowMapData flowMapData) {
		this.flowMapData = flowMapData;
		this.visioShapeMap = new HashMap<String, BaseShape>();
		this.visioConnectList = new ArrayList<ConnectProperty>();
		this.buildHelp = new VisioElementBuilderHelp(flowMapData.getWorkflowHeight(), flowMapData.getWorkflowWidth());
	}

	@SuppressWarnings("unchecked")
	public List<BaseShape> getVisioShapeList() {
		// 先构造图形（构造线时需要获取图形的UniqueId，如果图形没有构建，会报NullException
		for (JecnFigureData elementData : flowMapData.getFigureList()) {
			buildVisioElement(elementData);
		}
		for (JecnBaseLineData elementData : flowMapData.getLineList()) {
			// 曼哈顿线没有UUID故生成一个
			if (elementData.getFlowElementUUID() == null) {
				elementData.setFlowElementUUID(UUID.randomUUID().toString());
			}
			buildVisioElement(elementData);
			// 添加连接线属性
			addConnectProperty(elementData);
		}
		List<BaseShape> list = new ArrayList<BaseShape>(visioShapeMap.values());
		Collections.sort(list);
		return list;
	}

	/**
	 * 构造通用的图形
	 * 
	 * @param elementData
	 */
	private void buildVisioElement(JecnAbstractFlowElementData elementData) {
		BaseShape baseShape = buildHelp.initVisioShape(elementData);
		if (baseShape == null) {
			return;
		}
		visioShapeMap.put(elementData.getFlowElementUUID(), baseShape);
	}

	/**
	 * 创建连接线和图形之间的关系
	 */
	private void addConnectProperty(JecnBaseLineData baseLine) {
		if (!(baseLine instanceof JecnManhattanLineData)) {
			return;
		}
		JecnManhattanLineData manhattanLine = (JecnManhattanLineData) baseLine;
		// Xfrom1D 图形ID
		int fromSheet = getVisioShapeIdByUUID(manhattanLine.getFlowElementUUID());

		// 开始连接属性
		// 起始图形ID
		int begToSheet = getVisioShapeIdByUUID(manhattanLine.getStartFigureUUID());
		// 开始连接图形位置点
		LineRoute begLineRoute = LineRoute.getLineRoute(manhattanLine.getStartEditPointType().toString());

		// 结束连接属性
		// 结束图形ID
		int endToSheet = getVisioShapeIdByUUID(manhattanLine.getEndFigureUUID());
		// 结束连接图形位置点
		LineRoute endLineRoute = LineRoute.getLineRoute(manhattanLine.getEndEditPointType().toString());

		ConnectProperty beginConnect = new ConnectProperty(fromSheet, CellType.BEGINX, begToSheet, begLineRoute);
		ConnectProperty endConnect = new ConnectProperty(fromSheet, CellType.ENDX, endToSheet, endLineRoute);

		visioConnectList.add(beginConnect);
		visioConnectList.add(endConnect);
	}

	/**
	 * 获取visio图形ID
	 * 
	 * @param uuid
	 * @return
	 */
	private int getVisioShapeIdByUUID(String uuid) {
		return visioShapeMap.get(uuid).getUniqueId();
	}

	/**
	 * 获取连接线连接属性
	 * 
	 * @return
	 */
	public List<ConnectProperty> getVisioConnectList() {
		return visioConnectList;
	}

	/***
	 * 获取页面属性
	 * 
	 * @return
	 */
	public PageProperty getPageProperty() {
		double pageWidth = Unit.pxToMM(flowMapData.getWorkflowWidth());
		double pageHeight = Unit.pxToMM(flowMapData.getWorkflowHeight());
		return new PageProperty(pageWidth, pageHeight);
	}

}
