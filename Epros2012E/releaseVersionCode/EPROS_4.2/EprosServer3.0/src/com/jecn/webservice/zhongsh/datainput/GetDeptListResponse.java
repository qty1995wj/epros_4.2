
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDeptListResult" type="{http://tempuri.org/}ArrayOfRole" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDeptListResult"
})
@XmlRootElement(name = "GetDeptListResponse")
public class GetDeptListResponse {

    @XmlElement(name = "GetDeptListResult")
    protected ArrayOfRole getDeptListResult;

    /**
     * 获取getDeptListResult属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRole }
     *     
     */
    public ArrayOfRole getGetDeptListResult() {
        return getDeptListResult;
    }

    /**
     * 设置getDeptListResult属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRole }
     *     
     */
    public void setGetDeptListResult(ArrayOfRole value) {
        this.getDeptListResult = value;
    }

}
