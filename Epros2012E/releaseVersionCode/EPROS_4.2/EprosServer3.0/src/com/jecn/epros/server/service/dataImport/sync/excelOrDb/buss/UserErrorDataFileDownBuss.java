package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

public class UserErrorDataFileDownBuss extends AbstractFileDownBuss {

	/**
	 * 
	 * 导出文件路径
	 * 
	 * @return String 文件路径
	 */
	public String getOutFilepath() {
		return SyncTool.getOutPutExcelErrorDataPath();
	}

	/**
	 * 
	 * 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	protected String getOutFileName() {
		return SyncConstant.OUTPUT_EXCEL_ERROR_DATA_NAME;
	}

}
