package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.Date;
/***
 * 登录日志 JECN_USER_LOGIN_LOG 对应的Bean
 * @author Administrator
 *
 */
public class JecnUserLoginLog implements Serializable  {
	/**主键Id*/
	private String id;
	/**用户ID*/
	private Long userId;
	/**浏览器SESSION_ID*/
	private String sessionId;
	/**登录时间*/
	private Date loginTime;
	/**退出时间*/
	private Date loginOutTime;
	/**退出类型*/
	private int loginOutType;
	/**登录类型 0:浏览端，1：设计器*/
	private int loginType;
	
	public int getLoginType() {
		return loginType;
	}
	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public Date getLoginOutTime() {
		return loginOutTime;
	}
	public void setLoginOutTime(Date loginOutTime) {
		this.loginOutTime = loginOutTime;
	}
	public int getLoginOutType() {
		return loginOutType;
	}
	public void setLoginOutType(int loginOutType) {
		this.loginOutType = loginOutType;
	}
	
	
}
