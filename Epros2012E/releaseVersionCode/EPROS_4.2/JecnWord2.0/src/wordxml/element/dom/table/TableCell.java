package wordxml.element.dom.table;

import java.util.List;

import wordxml.constant.Constants.Valign;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.table.CellBean;
import wordxml.element.dom.IBody;
import wordxml.element.dom.paragraph.Paragraph;

/***
 * 单元格数据bean
 */
public class TableCell extends IBody {
	/********************* 实例变量 ***************/
	/*** 单元格属性 默认获取所属行中的默认单元格样式 */
	private CellBean cellBean;
	/** 所属行 */
	private TableRow tableRow;
	/** 列宽 */
	private float width;

	/** 文本垂直对齐方式 ****/
	private Valign vAlign = Valign.top;

	/** 跨列 默认为1 */
	private int colspan = 1;
	/** 跨行 默认为1 */
	private int rowspan = 1;
	/** 跨行开始标识 默认不跨行 **/
	private boolean vmergeBeg = false;
	/** 跨行结束标识 **/
	private boolean vmerge = false;

	public TableCell() {

	}

	/**
	 * 创建文本段落
	 * 
	 * @author weidp
	 * @date 2014-10-16 下午01:44:24
	 * @param text
	 */
	public Paragraph createParagraph(String text) {
		Paragraph p = new Paragraph(text);
		p.setParent(this);
		this.paragraphs.add(p);
		this.bodyElements.add(p);
		return p;
	}

	/**
	 * 创建文本段落，并设置样式
	 * 
	 * @author weidp
	 * @date 2014-10-16 下午01:43:43
	 * @param text
	 * @param pBean
	 */
	public Paragraph createParagraph(String text, ParagraphBean pBean) {
		Paragraph p = createParagraph(text);
		p.setParagraphBean(pBean);
		return p;
	}

	/**
	 * 创建文本段落，并设置样式
	 * 
	 * @author weidp
	 * @date 2014-10-16 下午01:43:43
	 * @param text
	 * @param pBean
	 */
	public Paragraph createParagraph(String text, FontBean font) {
		Paragraph p = createParagraph(text);
		p.setParagraphBean(new ParagraphBean(font));
		return p;
	}

	public TableRow getTableRow() {
		return tableRow;
	}

	public void setTableRow(TableRow tableRow) {
		this.tableRow = tableRow;
	}

	public CellBean getCellBean() {
		if (cellBean == null) {
			cellBean = new CellBean();
		}
		return cellBean;
	}

	public void setCellBean(CellBean cellBean) {
		this.cellBean = cellBean;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float witdh) {
		this.width = witdh;
	}

	public int getColspan() {
		return colspan;
	}

	public void setColspan(int colspan) {
		if (colspan > 1) {
			// 设置宽度为跨列后的宽度
			this.setWidth(getColspanWidth(colspan));
			this.colspan = colspan;
		}
	}

	/**
	 * 获取跨列后的宽度
	 * 
	 * @author weidp
	 * @date 2014-10-22 上午11:51:12
	 * @param colspan
	 * @return
	 */
	private int getColspanWidth(int colspan) {
		// 所有列宽
		float[] colWidths = this.getTableRow().getTable().getColWidths();
		// 获取当前是第几列
		int widthPos = 0;
		List<TableCell> cells = this.getTableRow().getCells();
		// 当前列索引
		int curCellIndex = cells.indexOf(this);
		for (int i = 0; i < curCellIndex; i++) {
			widthPos += cells.get(i).getColspan();
		}

		// 获取当前列跨列后的宽度
		int curCellWidth = 0;
		for (int i = widthPos; i < widthPos + colspan; i++) {
			curCellWidth += colWidths[i];
		}
		return curCellWidth;
	}

	public int getRowspan() {
		return rowspan;
	}

	public void setRowspan(int rowspan) {
		this.rowspan = rowspan;
	}

	public Valign getvAlign() {
		return vAlign;
	}

	public void setvAlign(Valign vAlign) {
		this.vAlign = vAlign;
	}

	public boolean isVmergeBeg() {
		return vmergeBeg;
	}

	public void setVmergeBeg(boolean vmergeBeg) {
		this.vmergeBeg = vmergeBeg;
	}

	public boolean isVmerge() {
		return vmerge;
	}

	public void setVmerge(boolean vmerge) {
		this.vmerge = vmerge;
	}

}
