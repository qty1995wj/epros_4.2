package epros.designer.gui.popedom.position;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.integration.MultiLineCellRenderer;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;

/**
 * @author yxw 2012-6-14
 * @description：岗位选择择
 */
public class PositionChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(PositionChooseDialog.class);

	/** 搜索名称 */
	protected JLabel searchDeptLabel;
	/** 搜索内容 */
	protected JTextField searchDeptText;

	/** 搜索查询输入区域面板 */
	protected JPanel searchDeptInputPanel;

	/** 搜索名称 */
	protected JLabel searchPeopleLabel;
	/** 搜索内容 */
	protected JTextField searchPeopleText;
	/** 搜索按钮 */
	protected JButton searchPeopleButton;

	/** 搜索查询输入区域面板 */
	protected JPanel searchPeopleInputPanel;

	public PositionChooseDialog(List<JecnTreeBean> list) {
		super(list, 1);
		// 岗位选择
		this.setTitle(JecnProperties.getValue("positionSelect"));

	}

	public PositionChooseDialog(List<JecnTreeBean> list, boolean isShowBut) {
		super(list, 1, isShowBut);
		// 岗位选择
		this.setTitle(JecnProperties.getValue("positionSelect"));

	}

	public PositionChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 1, jecnDialog);
		// 岗位选择
		this.setTitle(JecnProperties.getValue("positionSelect"));

	}

	public PositionChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog, boolean isShowBut) {
		super(list, 1, jecnDialog, isShowBut);
		// 岗位选择
		this.setTitle(JecnProperties.getValue("positionSelect"));

	}

	public PositionChooseDialog(List<JecnTreeBean> list, Long permOrg) {
		super(list, 1, permOrg);
		// 岗位选择
		this.setTitle(JecnProperties.getValue("positionSelect"));
	}

	/***
	 * 批量添加面板
	 * 
	 * @param c
	 * @param insets
	 */
	protected void addPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(addAllPanel, c);
	}

	protected void departmentAndPeopleComponents() {

		// 初始化搜索标签名称

		searchDeptInputPanel = new JPanel();
		searchDeptInputPanel.setLayout(new FlowLayout());
		searchDeptInputPanel.setOpaque(false);
		searchDeptInputPanel.setBorder(null);
		searchDeptLabel = new JLabel(JecnProperties.getValue("deptNameC"));
		searchDeptText = new JTextField(60);
		// 搜索名称
		searchDeptInputPanel.add(searchDeptLabel);
		// 搜索内容
		searchDeptInputPanel.add(searchDeptText);

		searchInputPanel.add(searchDeptInputPanel, BorderLayout.CENTER);

		// 初始化搜索标签名称
		searchPeopleInputPanel = new JPanel();
		searchPeopleInputPanel.setLayout(new FlowLayout());
		searchPeopleInputPanel.setOpaque(false);
		searchPeopleInputPanel.setBorder(null);
		searchPeopleButton = new JButton(JecnProperties.getValue("search"));
		searchPeopleLabel = new JLabel(JecnProperties.getValue("personNameC"));
		if (JecnConfigTool.isIflytekLogin()) {
			searchPeopleText = new JTextField(50);
		} else {
			searchPeopleText = new JTextField(60);
		}
		// 搜索名称
		searchPeopleInputPanel.add(searchPeopleLabel, FlowLayout.LEFT);
		// 搜索内容
		searchPeopleInputPanel.add(searchPeopleText, FlowLayout.CENTER);
		if (JecnConfigTool.isIflytekLogin()) {
			// 搜索按钮
			searchPeopleInputPanel.add(searchPeopleButton, FlowLayout.RIGHT);
		}
		searchInputPanel.add(searchPeopleInputPanel, BorderLayout.SOUTH);

		searchDeptText.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				searchAction();

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				searchAction();

			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub

			}
		});
		if (JecnConfigTool.isIflytekLogin()) {
			searchPeopleButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					searchAction();
				}
			});

		} else {
			searchPeopleText.getDocument().addDocumentListener(new DocumentListener() {

				@Override
				public void removeUpdate(DocumentEvent e) {
					searchAction();

				}

				@Override
				public void insertUpdate(DocumentEvent e) {
					searchAction();

				}

				@Override
				public void changedUpdate(DocumentEvent e) {
					// TODO Auto-generated method stub

				}
			});
		}
	}

	public void searchAction() {
		String posName = searchText.getText().trim();// 岗位名称
		String deptName = searchDeptText.getText().trim();// 岗位名称
		String peopleName = searchPeopleText.getText().trim();// 人员名称
		if (!DrawCommon.isNullOrEmtryTrim(posName) || !DrawCommon.isNullOrEmtryTrim(deptName)
				|| !DrawCommon.isNullOrEmtryTrim(peopleName)) {
			Map<String, Object> nameMap = new HashMap<String, Object>();
			nameMap.put("posName", posName);
			nameMap.put("deptName", deptName);
			if (StringUtils.isNotBlank(peopleName)) {
				if (JecnConfigTool.isIflytekLogin()) {
					List<String> name = new ArrayList<String>();
					name.add(peopleName);
					Map<String, String> jecnIftekSyncUser = ConnectionPool.getPersonAction().getJecnIftekSyncUser(name);
					List<String> loginNames = new ArrayList<String>();
					for (Map.Entry<String, String> bean : jecnIftekSyncUser.entrySet()) {
						loginNames.add(bean.getKey());
					}
					nameMap.put("peopleNames", loginNames);
				} else {
					nameMap.put("peopleName", peopleName);
				}

			}
			searchListJecnTreeBean = searchPosByName(nameMap);
			searchTable.reAddVectors(getContent(searchListJecnTreeBean));
		}
	}

	public List<JecnTreeBean> searchPosByName(Map<String, Object> nameMap) {
		try {

			return ConnectionPool.getOrganizationAction().searchPositionByName(nameMap, JecnConstants.projectId);
		} catch (Exception e) {
			log.error("PositionChooseDialog searchByName is error！", e);
		}
		return null;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getOrganizationAction().searchPositionByName(name, JecnConstants.projectId);
		} catch (Exception e) {
			log.error("PositionChooseDialog searchByName is error！", e);
		}
		return null;
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("positionNameC");
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyOrgPositionTree();
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("positionName"));// 岗位名称
		title.add(JecnProperties.getValue("deptName"));// 部门名称
		return title;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {

		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null && !list.isEmpty()) {
			getParentsName(list); // 根据部门IDS 获取父级目录
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				data.add(jecnTreeBean.getPname() == null ? " " : jecnTreeBean.getPname());
				content.add(data);
			}
		}
		return content;

	}

	private void getParentsName(List<JecnTreeBean> list) {
		try {
			List<Long> childrenIds = new ArrayList<Long>();
			Map<Long, String> ParentsNames = new HashMap<Long, String>();
			for (JecnTreeBean jecnTreeBean : list) {// 获取要查询父节点的 ID
				childrenIds.add(jecnTreeBean.getId());
			}
			ParentsNames = ConnectionPool.getProcessAction().getNodeParentById(childrenIds, false,
					TreeNodeType.position);
			for (JecnTreeBean jecnTreeBean : list) {
				jecnTreeBean.setPname(ParentsNames.get(jecnTreeBean.getId()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		data.add(jecnTreeBean.getPname());
		return data;
	}

	/**
	 * 点击表格 定位树节点
	 */
	public void isClickTable() {
		int row = this.resultTable.getSelectedRow();
		if (row == -1) {
			return;
		}
		JecnTreeBean treeBean = getListJecnTreeBean().get(row);
		try {
			if (treeBean == null) {
				return;
			}
			treeBean = ConnectionPool.getOrganizationAction().getJecnTreeBeanByposId(treeBean.getId());
			if (treeBean == null) {
				return;
			}
			HighEfficiencyOrgPositionTree jTree = ((HighEfficiencyOrgPositionTree) this.getjTree());
			jTree.setAllowExpand(false);
			JecnTreeCommon.searchNodePos(treeBean, jTree);
			jTree.setAllowExpand(true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("PositionChooseDialog isClickTable is error！", e);
		}
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:双击Table
	 */
	protected void searchTablemousePressed(MouseEvent evt) {
		if (evt.getClickCount() == 2) {
			if (this.selectMutil && isKeyCtrlorShift) {
				int[] rows = searchTable.getSelectedRows();
				for (int row : rows) {
					singleSearchTable(row);
				}
			} else {
				int row = searchTable.getSelectedRow();
				singleSearchTable(row);
			}

		}
		if (evt.getClickCount() == 1) {
			int row = searchTable.getSelectedRow();
			int newRowHeight = searchTable.getRowHeight(row);
			if (row != -1) {
				if (oldRowHeight >= newRowHeight) {
					MultiLineCellRenderer multiLineCellRenderer = new MultiLineCellRenderer(row, 1, 0);
					multiLineCellRenderer.setToolTipText(searchTable.getValueAt(row, 1).toString());
					searchTable.setDefaultRenderer(Object.class, multiLineCellRenderer);
				} else if (oldRowHeight < newRowHeight) {
					// 选中后高度设置
					if (newRowHeight != oldRowHeight) {
						searchTable.setRowHeight(row, oldRowHeight);
						MultiLineCellRenderer multiLineCellRenderer = new MultiLineCellRenderer(row, 1, 1);
						multiLineCellRenderer.setToolTipText(searchTable.getValueAt(row, 1).toString());
						searchTable.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 1, 1));
					}
				}
			}
		}
	}
}
