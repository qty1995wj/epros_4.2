package epros.draw.gui.figure.shape;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 画虚拟边框
 * 
 * @author ZHANGXH
 * 
 */
public class JecnPaintFigure {
	/**
	 * 活动元素边框
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintRoundRect(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		// 圆弧矩形边框
		g2d.drawRoundRect(localX, localY, userWidth, userHeight, 20, 20);
	}

	/**
	 * 活动决策元素边框
	 * 
	 * @param g2
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintRhombus(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		int x1 = localX + userWidth / 2;
		int y1 = localY;
		int x2 = localX + userWidth;
		int y2 = localY + userHeight / 2;
		int x3 = localX + userWidth / 2;
		int y3 = localY + userHeight - 1;
		int x4 = localX;
		int y4 = localY + userHeight / 2;
		// 圆弧矩形边框
		g2d.drawPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
	}

	/**
	 * 活动IT决策元素边框
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintITRhombus(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		int x1 = localX + userWidth / 2;
		int y1 = localY;
		int x2 = localX + userWidth;
		int y2 = localY + userHeight / 2;
		int x3 = localX + userWidth / 2;
		int y3 = localY + userHeight - 1;
		int x4 = localX;
		int y4 = localY + userHeight / 2;
		// 圆弧矩形边框
		g2d.drawPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
		// 圆弧矩形边框
		g2d.drawPolygon(new int[] { x1, x2 - 10, x3, x4 + 10 }, new int[] { y1 + 10, y2, y3 - 10, y4 }, 4);
	}

	/**
	 * 圆形边框
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintCircle(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		g2d.drawOval(localX, localY, userWidth, userHeight);
	}

	/**
	 * 长方形边框
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintRect(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		g2d.drawRect(localX, localY, userWidth, userHeight);
	}

	/**
	 * 数据体
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintDataImage(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		double sacle = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int ovalHeight = DrawCommon.convertDoubleToInt(20 * sacle);
		int x1 = localX;
		int y1 = localY + DrawCommon.convertDoubleToInt(15 * sacle);

		userHeight = userHeight - DrawCommon.convertDoubleToInt(30 * sacle);
		g2d.drawOval(x1, y1 - ovalHeight / 2 + userHeight, userWidth, ovalHeight);

		g2d.drawLine(x1, y1, x1, localY + userHeight + ovalHeight / 2 + DrawCommon.convertDoubleToInt(5 * sacle));
		g2d.drawLine(localX + userWidth, y1, localX + userWidth, localY + userHeight + ovalHeight / 2
				+ DrawCommon.convertDoubleToInt(3 * sacle));
		g2d.drawOval(x1, y1 - ovalHeight / 2, userWidth, ovalHeight);
	}

	/**
	 * 
	 * 协作框
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintDottedRect(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		g2d.drawRect(localX, localY, userWidth, userHeight);
	}

	/**
	 * 
	 * 关键活动
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintKeyActive(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		g2d.drawOval(localX, localY, userWidth, userHeight);
	}

	/**
	 * 
	 * 事件
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintEvent(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		int x1 = localX;
		int y1 = localY + userHeight / 2;
		int x2 = localX + 20;
		int y2 = localY;
		int x3 = localX + userWidth - 20;
		int y3 = localY;
		int x4 = localX + userWidth;
		int y4 = localY + userHeight / 2;
		int x5 = localX + userWidth - 20;
		int y5 = localY + userHeight;
		int x6 = localX + 20;
		int y6 = localY + userHeight;

		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
	}

	/**
	 * 
	 * 流程开始/流程结束
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintFlowFigure(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		int arcWidth;
		int arcHeight;
		if (userWidth > userHeight) {
			arcWidth = 3 * userWidth / 5;
		} else {
			arcWidth = 3 * userHeight / 5;
		}
		arcHeight = arcWidth;

		g2d.drawRoundRect(localX, localY, userWidth, userHeight, arcWidth, arcHeight);
	}

	/**
	 * 
	 * 三角形
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintTriangle(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		int y2 = 0;
		int x3 = 0;
		int y3 = 0;
		if (currentAngle == 0.0) {
			x1 = localX + userWidth / 2;
			y1 = localY;
			x2 = localX;
			y2 = localY + userHeight;
			x3 = localX + userWidth;
			y3 = localY + userHeight;
		} else if (currentAngle == 90.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth;
			y2 = localY + userHeight / 2;
			x3 = localX;
			y3 = localY + userHeight;
		} else if (currentAngle == 180.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth;
			y2 = localY;
			x3 = localX + userWidth / 2;
			y3 = localY + userHeight;
		} else if (currentAngle == 270.0) {
			x1 = localX;
			y1 = localY + userHeight / 2;
			x2 = localX + userWidth;
			y2 = localY;
			x3 = localX + userWidth;
			y3 = localY + userHeight;
		}
		g2d.drawPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
	}

	/**
	 * 
	 * 三角形
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintTriangleAR(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		int y2 = 0;
		int x3 = 0;
		int y3 = 0;
		if (currentAngle == 0.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth;
			y2 = localY;
			x3 = localX + userWidth / 2;
			y3 = localY + userHeight;
		} else if (currentAngle == 90.0) {
			x1 = localX;
			y1 = localY + userHeight / 2;
			x2 = localX + userWidth;
			y2 = localY;
			x3 = localX + userWidth;
			y3 = localY + userHeight;
		} else if (currentAngle == 180.0) {
			x1 = localX + userWidth / 2;
			y1 = localY;
			x2 = localX;
			y2 = localY + userHeight;
			x3 = localX + userWidth;
			y3 = localY + userHeight;
		} else if (currentAngle == 270.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth;
			y2 = localY + userHeight / 2;
			x3 = localX;
			y3 = localY + userHeight;
		}
		g2d.drawPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
	}

	/**
	 * 椭圆边框（椭圆和子流程）
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintOval(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		g2d.drawOval(localX, localY, userWidth, userHeight);
	}

	/**
	 * 返工符 返入
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 *            旋转角度
	 */
	public static void paintArrowHead(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		if (currentAngle == 0.0) {
			x1 = localX;
			y1 = localY + userHeight;
			x2 = localX + userWidth / 2;
			y2 = localY + userHeight / 2;
			x3 = localX + userWidth;
			y3 = localY + userHeight;
		} else if (currentAngle == 90.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth / 2;
			y2 = localY + userHeight / 2;
			x3 = localX;
			y3 = localY + userHeight;
		} else if (currentAngle == 180.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth;
			y2 = localY;
			x3 = localX + userWidth / 2;
			y3 = localY + userHeight / 2;
		} else if (currentAngle == 270.0) {
			x1 = localX + userWidth / 2;
			y1 = localY + userHeight / 2;
			x2 = localX + userWidth;
			y2 = localY;
			x3 = localX + userWidth;
			y3 = localY + userHeight;
		}
		g2d.drawPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
		if (currentAngle == 90.0) {
			g2d.drawLine(localX + userWidth / 2, localY + userHeight / 2, localX + userWidth, localY + userHeight / 2);
		} else if (currentAngle == 180.0) {
			g2d.drawLine(localX + userWidth / 2, localY + userHeight / 2, localX + userWidth / 2, localY + userHeight);
		} else if (currentAngle == 270.0) {
			g2d.drawLine(localX, localY + userHeight / 2, localX + userWidth / 2, localY + userHeight / 2);
		} else if (currentAngle == 0.0) {
			g2d.drawLine(localX + userWidth / 2, localY, localX + userWidth / 2, localY + userHeight / 2);
		}
	}

	/**
	 * 返工符 返出
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 *            旋转角度
	 */
	public static void paintRightArrowHead(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		if (currentAngle == 0.0) {
			x1 = localX;
			y1 = localY + userHeight / 2;
			x2 = localX + userWidth;
			y2 = localY + userHeight / 2;
			x3 = localX + userWidth / 2;
			y3 = localY + userHeight;
		} else if (currentAngle == 90.0) {
			x1 = localX;
			y1 = localY + userHeight / 2;
			x2 = localX + userWidth / 2;
			y2 = localY;
			x3 = localX + userWidth / 2;
			y3 = localY + userHeight;
		} else if (currentAngle == 180.0) {
			x1 = localX;
			y1 = localY + userHeight / 2;
			x2 = localX + userWidth / 2;
			y2 = localY;
			x3 = localX + userWidth;
			y3 = localY + userHeight / 2;
		} else if (currentAngle == 270.0) {
			x1 = localX + userWidth / 2;
			y1 = localY;
			x2 = localX + userWidth;
			y2 = localY + userHeight / 2;
			x3 = localX + userWidth / 2;
			y3 = localY + userHeight;
		}
		g2d.drawPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
		if (currentAngle == 0.0) {
			g2d.drawLine(localX + userWidth / 2, localY, localX + userWidth / 2, localY + userHeight / 2);
		} else if (currentAngle == 90.0) {
			g2d.drawLine(localX + userWidth / 2, localY + userHeight / 2, localX + userWidth, localY + userHeight / 2);
		} else if (currentAngle == 180.0) {
			g2d.drawLine(localX + userWidth / 2, localY + userHeight / 2, localX + userWidth / 2, localY + userHeight);
		} else if (currentAngle == 270.0) {
			g2d.drawLine(localX, localY + userHeight / 2, localX + userWidth / 2, localY + userHeight / 2);
		}
	}

	/**
	 * 六边形
	 * 
	 * @param g2d
	 * @param localX
	 * @param localY
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintRightHexagon(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		int initVale = 20;
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		initVale = DrawCommon.convertDoubleToInt(initVale * scale);
		if (currentAngle == 0.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth - initVale;
			y2 = localY;
			x3 = localX + userWidth;
			y3 = localY + userHeight / 2;
			x4 = localX + userWidth - initVale;
			y4 = localY + userHeight;
			x5 = localX;
			y5 = localY + userHeight;
			x6 = localX + initVale;
			y6 = localY + userHeight / 2;

		} else if (currentAngle == 90.0) {
			x1 = localX;
			y1 = localY + initVale;
			x2 = localX + userWidth / 2;
			y2 = localY;
			x3 = localX + userWidth;
			y3 = localY + initVale;
			x4 = localX + userWidth;
			y4 = localY + userHeight;
			x5 = localX + userWidth / 2;
			y5 = localY + userHeight - initVale;
			x6 = localX;
			y6 = localY + userHeight;
		} else if (currentAngle == 180.0) {
			x1 = localX + initVale;
			y1 = localY;
			x2 = localX + userWidth - 1;
			y2 = localY;
			x3 = localX + userWidth - initVale;
			y3 = localY + userHeight / 2;
			x4 = localX + userWidth - 1;
			y4 = localY + userHeight - 1;
			x5 = localX + initVale;
			y5 = localY + userHeight - 1;
			x6 = localX;
			y6 = localY + userHeight / 2;
		} else if (currentAngle == 270.0) {
			x1 = localX;
			y1 = localY;
			x2 = localX + userWidth / 2;
			y2 = localY + initVale;
			x3 = localX + userWidth - 1;
			y3 = localY;
			x4 = localX + userWidth - 1;
			y4 = localY + userHeight - initVale;
			x5 = localX + userWidth / 2;
			y5 = localY + userHeight - 1;
			x6 = localX;
			y6 = localY + userHeight - initVale;
		}

		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
	}

	/**
	 * 分隔符 横线
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintVHLine(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		int intX = userWidth;
		int intY = userHeight;
		if (userWidth > userHeight) {
			g2d.drawLine(localX, localY, localX + intX, localY + 0);
		} else {
			g2d.drawLine(localX, localY, localX + 0, localY + intY);
		}

	}

	/**
	 * 接口
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintImplFigure(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		x1 = localX + userWidth / 5;
		y1 = localY + (userHeight - userHeight / 5) / 2 + userHeight / 5;
		x2 = localX + userWidth / 5 + 20 - 10;
		y2 = localY + userHeight / 5;
		x3 = localX + userWidth - 20 + 10;
		y3 = localY + userHeight / 5;
		x4 = localX + userWidth;
		y4 = localY + (userHeight - userHeight / 5) / 2 + userHeight / 5;
		x5 = localX + userWidth - 20 + 10;
		y5 = localY + userHeight;
		x6 = localX + userWidth / 5 + 20 - 10;
		y6 = localY + userHeight;

		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
		g2d.drawRoundRect(localX, localY, userWidth * 7 / 10, userHeight * 4 / 5, 15, 15);
	}

	/**
	 * 注释框
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintCommentText(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int heightLine = userHeight - 10;
		if (currentAngle == 0.0) {
			g2d.drawLine(localX, localY, localX, localY + userHeight);
			g2d.drawLine(localX, localY, localX + 10, localY);
			g2d.drawLine(localX, localY + userHeight, localX + 10, localY + userHeight);
		} else if (currentAngle == 90.0) {
			g2d.drawLine(localX + 0, localY + 0, localX + 0, localY + 10);
			g2d.drawLine(localX + 0, localY + 0, localX + userWidth, localY + 0);
			g2d.drawLine(localX + userWidth, localY + 0, localX + userWidth, localY + 10);
		} else if (currentAngle == 180.0) {
			g2d.drawLine(localX + userWidth, localY + 0, localX + userWidth, localY + userHeight);
			g2d.drawLine(localX + userWidth - 10, localY + 0, localX + userWidth, localY + 0);
			g2d.drawLine(localX + userWidth - 10, localY + userHeight, localX + userWidth, localY + userHeight);
		} else if (currentAngle == 270.0) {
			g2d.drawLine(localX, localY + heightLine, localX, localY + userHeight);
			g2d.drawLine(localX, localY + userHeight, localX + userWidth, localY + userHeight);
			g2d.drawLine(localX + userWidth, localY + userHeight, localX + userWidth, localY + heightLine);
		}
	}

	/**
	 * 五边形
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintPentagon(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int initVale = 20;
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		initVale = DrawCommon.convertDoubleToInt(initVale * scale);
		if (currentAngle == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth - initVale;
			y2 = 0;
			x3 = userWidth - 1;
			y3 = userHeight / 2;
			x4 = userWidth - initVale;
			y4 = userHeight - 1;
			x5 = 0;
			y5 = userHeight - 1;
		} else if (currentAngle == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth - 1;
			y2 = 0;
			x3 = userWidth - 1;
			y3 = userHeight - initVale;
			x4 = userWidth / 2;
			y4 = userHeight - 1;
			x5 = 0;
			y5 = userHeight - initVale;
		} else if (currentAngle == 180.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = initVale;
			y2 = 0;
			x3 = userWidth - 1;
			y3 = 0;
			x4 = userWidth - 1;
			y4 = userHeight - 1;
			x5 = initVale;
			y5 = userHeight - 1;
		} else if (currentAngle == 270.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth - 1;
			y2 = initVale;
			x3 = userWidth - 1;
			y3 = userHeight - 1;
			x4 = 0;
			y4 = userHeight - 1;
			x5 = 0;
			y5 = initVale;
		}

		g2d.drawPolygon(new int[] { localX + x1, localX + x2, localX + x3, localX + x4, localX + x5 }, new int[] {
				localY + y1, localY + y2, localY + y3, localY + y4, localY + y5 }, 5);
	}

	/**
	 * 单向箭头
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintOneArrowLine(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		int x7 = 0;
		int y7 = 0;
		if (currentAngle == 0.0) {
			x1 = 0;
			y1 = userHeight * 2 / 5;
			x2 = userWidth * 4 / 5;
			y2 = userHeight * 2 / 5;
			x3 = userWidth * 4 / 5;
			y3 = userHeight * 1 / 5;
			x4 = userWidth - 1;
			y4 = userHeight / 2;
			x5 = userWidth * 4 / 5;
			y5 = userHeight * 4 / 5;
			x6 = userWidth * 4 / 5;
			y6 = userHeight * 3 / 5;
			x7 = 0;
			y7 = userHeight * 3 / 5;
		} else if (currentAngle == 45.0) {
			x1 = userWidth * 16 / 50;
			y1 = userHeight * 9 / 50;
			x2 = userWidth * 41 / 50;
			y2 = userHeight * 34 / 50;
			x3 = userWidth * 19 / 20;
			y3 = userHeight * 11 / 20;
			x4 = userWidth * 9 / 10;
			y4 = userHeight * 9 / 10;
			x5 = userWidth * 11 / 20;
			y5 = userHeight * 19 / 20;
			x6 = userWidth * 34 / 50;
			y6 = userHeight * 41 / 50;
			x7 = userWidth * 9 / 50;
			y7 = userHeight * 16 / 50;
		} else if (currentAngle == 90.0) {
			x1 = userWidth * 3 / 5;
			y1 = 0;
			x2 = userWidth * 3 / 5;
			y2 = userHeight * 4 / 5;
			x3 = userWidth * 4 / 5;
			y3 = userHeight * 4 / 5;
			x4 = userWidth / 2;
			y4 = userHeight - 1;
			x5 = userWidth * 1 / 5;
			y5 = userHeight * 4 / 5;
			x6 = userWidth * 2 / 5;
			y6 = userHeight * 4 / 5;
			x7 = userWidth * 2 / 5;
			y7 = 0;
		} else if (currentAngle == 135.0) {
			x1 = userWidth * 41 / 50;
			y1 = userHeight * 16 / 50;
			x2 = userWidth * 16 / 50;
			y2 = userHeight * 41 / 50;
			x3 = userWidth * 9 / 20;
			y3 = userHeight * 19 / 20;
			x4 = userWidth / 10;
			y4 = userHeight * 9 / 10;
			x5 = userWidth * 1 / 20;
			y5 = userHeight * 11 / 20;
			x6 = userWidth * 9 / 50;
			y6 = userHeight * 34 / 50;
			x7 = userWidth * 34 / 50;
			y7 = userHeight * 9 / 50;
		} else if (currentAngle == 180.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = userWidth / 5;
			y2 = userHeight / 5;
			x3 = userWidth / 5;
			y3 = userHeight * 2 / 5;
			x4 = userWidth;
			y4 = userHeight * 2 / 5;
			x5 = userWidth;
			y5 = userHeight * 3 / 5;
			x6 = userWidth / 5;
			y6 = userHeight * 3 / 5;
			x7 = userWidth / 5;
			y7 = userHeight * 4 / 5;

		} else if (currentAngle == 225.0) {
			x1 = userWidth * 1 / 10;
			y1 = userHeight * 1 / 10;
			x2 = userWidth * 9 / 20;
			y2 = userHeight * 1 / 20;
			x3 = userWidth * 16 / 50;
			y3 = userHeight * 9 / 50;
			x4 = userWidth * 41 / 50;
			y4 = userHeight * 34 / 50;
			x5 = userWidth * 34 / 50;
			y5 = userHeight * 41 / 50;
			x6 = userWidth * 9 / 50;
			y6 = userHeight * 16 / 50;
			x7 = userWidth * 1 / 20;
			y7 = userHeight * 9 / 20;
		} else if (currentAngle == 270.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth * 4 / 5;
			y2 = userHeight / 5;
			x3 = userWidth * 3 / 5;
			y3 = userHeight / 5;
			x4 = userWidth * 3 / 5;
			y4 = userHeight - 1;
			x5 = userWidth * 2 / 5;
			y5 = userHeight - 1;
			x6 = userWidth * 2 / 5;
			y6 = userHeight * 1 / 5;
			x7 = userWidth / 5;
			y7 = userHeight / 5;
		} else if (currentAngle == 315.0) {
			x1 = userWidth * 9 / 10;
			y1 = userHeight / 10;
			x2 = userWidth * 19 / 20;
			y2 = userHeight * 9 / 20;
			x3 = userWidth * 41 / 50;
			y3 = userHeight * 16 / 50;
			x4 = userWidth * 16 / 50;
			y4 = userHeight * 41 / 50;
			x5 = userWidth * 9 / 50;
			y5 = userHeight * 34 / 50;
			x6 = userWidth * 34 / 50;
			y6 = userHeight * 9 / 50;
			x7 = userWidth * 11 / 20;
			y7 = userHeight / 20;
		}
		g2d.drawPolygon(new int[] { localX + x1, localX + x2, localX + x3, localX + x4, localX + x5, localX + x6,
				localX + x7 }, new int[] { localY + y1, localY + y2, localY + y3, localY + y4, localY + y5,
				localY + y6, localY + y7 }, 7);
	}

	/**
	 * 双向箭头
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintTwoArrowLine(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		int x7 = 0;
		int y7 = 0;
		int x8 = 0;
		int y8 = 0;
		int x9 = 0;
		int y9 = 0;
		int x10 = 0;
		int y10 = 0;

		if (currentAngle == 0.0 || currentAngle == 180.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = userWidth / 5;
			y2 = userHeight / 5;
			x3 = userWidth / 5;
			y3 = userHeight * 2 / 5;
			x4 = userWidth * 4 / 5;
			y4 = userHeight * 2 / 5;
			x5 = userWidth * 4 / 5;
			y5 = userHeight / 5;
			x6 = userWidth - 1;
			y6 = userHeight / 2;
			x7 = userWidth * 4 / 5;
			y7 = userHeight * 4 / 5;
			x8 = userWidth * 4 / 5;
			y8 = userHeight * 3 / 5;
			x9 = userWidth / 5;
			y9 = userHeight * 3 / 5;
			x10 = userWidth / 5;
			y10 = userHeight * 4 / 5;
		} else if (currentAngle == 45.0 || currentAngle == 225.0) {
			x1 = userWidth * 1 / 10;
			y1 = userHeight * 1 / 10;
			x2 = userWidth * 9 / 20;
			y2 = userHeight * 1 / 20;
			x3 = userWidth * 16 / 50;
			y3 = userHeight * 9 / 50;
			x4 = userWidth * 41 / 50;
			y4 = userHeight * 34 / 50;
			x5 = userWidth * 19 / 20;
			y5 = userHeight * 11 / 20;
			x6 = userWidth * 9 / 10;
			y6 = userHeight * 9 / 10;
			x7 = userWidth * 11 / 20;
			y7 = userHeight * 19 / 20;
			x8 = userWidth * 34 / 50;
			y8 = userHeight * 41 / 50;
			x9 = userWidth * 8 / 50;
			y9 = userHeight * 16 / 50;
			x10 = userWidth * 1 / 20;
			y10 = userHeight * 9 / 20;

		} else if (currentAngle == 90.0 || currentAngle == 270.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth * 4 / 5;
			y2 = userHeight / 5;
			x3 = userWidth * 3 / 5;
			y3 = userHeight / 5;
			x4 = userWidth * 3 / 5;
			y4 = userHeight * 4 / 5;
			x5 = userWidth * 4 / 5;
			y5 = userHeight * 4 / 5;
			x6 = userWidth / 2;
			y6 = userHeight - 1;
			x7 = userWidth / 5;
			y7 = userHeight * 4 / 5;
			x8 = userWidth * 2 / 5;
			y8 = userHeight * 4 / 5;
			x9 = userWidth * 2 / 5;
			y9 = userHeight * 1 / 5;
			x10 = userWidth / 5;
			y10 = userHeight / 5;

		} else if (currentAngle == 135.0 || currentAngle == 315.0) {
			x1 = userWidth * 9 / 10;
			y1 = userHeight / 10;
			x2 = userWidth * 19 / 20;
			y2 = userHeight * 9 / 20;
			x3 = userWidth * 41 / 50;
			y3 = userHeight * 16 / 50;
			x4 = userWidth * 16 / 50;
			y4 = userHeight * 41 / 50;
			x5 = userWidth * 9 / 20;
			y5 = userHeight * 19 / 20;
			x6 = userWidth / 10;
			y6 = userHeight * 9 / 10;
			x7 = userWidth * 1 / 20;
			y7 = userHeight * 11 / 20;
			x8 = userWidth * 9 / 50;
			y8 = userHeight * 34 / 50;
			x9 = userWidth * 34 / 50;
			y9 = userHeight * 9 / 50;
			x10 = userWidth * 11 / 20;
			y10 = userHeight * 1 / 20;

		}
		g2d.drawPolygon(new int[] { localX + x1, localX + x2, localX + x3, localX + x4, localX + x5, localX + x6,
				localX + x7, localX + x8, localX + x9, localX + x10 },
				new int[] { localY + y1, localY + y2, localY + y3, localY + y4, localY + y5, localY + y6, localY + y7,
						localY + y8, localY + y9, localY + y10 }, 10);
	}

	/**
	 * 等腰梯形
	 * 
	 * @param g2d
	 * @param localX
	 * @param localY
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintTrapezoidFigure(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		if (currentAngle == 0) {// 默认零度
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth - 25;
			y3 = userHeight;
			x4 = 25;
			y4 = userHeight;
		} else if (currentAngle == 90.0) {
			x1 = 0;
			y1 = 25;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight - 25;
		} else if (currentAngle == 180.0) {
			x1 = 25;
			y1 = 0;
			x2 = userWidth - 25;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (currentAngle == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 25;
			x3 = userWidth;
			y3 = userHeight - 25;
			x4 = 0;
			y4 = userHeight;
		}
		g2d.drawPolygon(new int[] { localX + x1, localX + x2, localX + x3, localX + x4 }, new int[] { localY + y1,
				localY + y2, localY + y3, localY + y4 }, 4);
	}

	/**
	 * 手动输入
	 * 
	 * @param g2d
	 * @param localX
	 * @param localY
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintInputHandFigure(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		if (currentAngle == 0.0) {
			x1 = 0;
			y1 = userHeight / 3;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (currentAngle == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = userWidth / 3;
			y4 = userHeight;
		} else if (currentAngle == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight * 2 / 3;
			x4 = 0;
			y4 = userHeight;
		} else if (currentAngle == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth * 2 / 3;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		}

		g2d.drawPolygon(new int[] { localX + x1, localX + x2, localX + x3, localX + x4 }, new int[] { localY + y1,
				localY + y2, localY + y3, localY + y4 }, 4);
	}

	/**
	 * 文档
	 * 
	 * @param g2d
	 * @param localX
	 * @param localY
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintFileImage(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		int y5 = 0;
		int y6 = 0;
		/** Double 的最大可能值 */
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		List<Integer> xList = new ArrayList<Integer>();
		List<Integer> yList = new ArrayList<Integer>();
		if (currentAngle == 0.0) {
			x1 = userWidth;
			y1 = 0;

			x2 = 0;
			y2 = y1;

			x3 = x2;
			y3 = 7 * userHeight / 8;

			x4 = userWidth / 4;
			y4 = userHeight;

			x5 = 3 * userWidth / 4;
			y5 = 3 * userHeight / 4;

			x6 = userWidth;
			y6 = 7 * userHeight / 8;
		} else if (currentAngle == 90.0) {
			x1 = userWidth;
			y1 = 0;

			x2 = userWidth / 8;
			y2 = y1;

			x3 = 0;
			y3 = userHeight / 4;

			x4 = userWidth / 4;
			y4 = 3 * userHeight / 4;

			x5 = userWidth / 8;
			y5 = userHeight;

			x6 = userWidth;
			y6 = userHeight;
		} else if (currentAngle == 180.0) {
			x1 = userWidth;
			y1 = userHeight / 8;

			x2 = 3 * userWidth / 4;
			y2 = 0;

			x3 = userWidth / 4;
			y3 = userHeight / 4;

			x4 = 0;
			y4 = userHeight / 8;

			x5 = 0;
			y5 = userHeight;

			x6 = userWidth;
			y6 = userHeight;
		} else if (currentAngle == 270.0) {
			x1 = 7 * userWidth / 8;
			y1 = 0;

			x2 = 0;
			y2 = y1;

			x3 = x2;
			y3 = userHeight;

			x4 = 7 * userWidth / 8;
			y4 = userHeight;

			x5 = userWidth;
			y5 = 6 * userHeight / 8;

			x6 = 6 * userWidth / 8;
			y6 = 1 * userHeight / 4;
		}

		if (currentAngle == 0.0) {
			double[] X = { localX + x3, localX + x4, localX + x5, localX + x6 };
			double[] Y = { localY + y3, localY + y4, localY + y5, localY + y6 };
			for (int i = 0; i < X.length; i++) {
				if (X[i] > max) {
					max = X[i];
				}
				if (X[i] < min) {
					min = X[i];
				}
			}
			for (double x = min; x <= max; x = x + 0.01) {
				xList.add((int) x);
				yList.add((int) lagrange(X, Y, x));
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = localX + x1;
			yy[0] = localY + y1;
			xx[1] = localX + x2;
			yy[1] = localY + y2;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}

			g2d.drawPolygon(xx, yy, xx.length);
		} else if (currentAngle == 90.0) {
			double[] X = { localX + x5, localX + x4, localX + x3, localX + x2 };
			double[] Y = { localY + y5, localY + y4, localY + y3, localY + y2 };
			for (int i = 0; i < Y.length; i++) {
				if (Y[i] > max) {
					max = Y[i];
				}
				if (Y[i] < min) {
					min = Y[i];
				}
			}
			for (double y = min; y <= max; y = y + 0.01) {
				xList.add((int) lagrange(Y, X, y));
				yList.add((int) y);
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = localX + x1;
			yy[0] = localY + y1;
			xx[yList.size() + 1] = localX + x6;
			yy[yList.size() + 1] = localY + y6;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 1] = xList.get(i);
				yy[i + 1] = yList.get(i);
			}

			g2d.drawPolygon(xx, yy, xx.length);

		} else if (currentAngle == 180.0) {

			double[] X = { localX + x4, localX + x3, localX + x2, localX + x1 };
			double[] Y = { localY + y4, localY + y3, localY + y2, localY + y1 };
			for (int i = 0; i < X.length; i++) {
				if (X[i] > max) {
					max = X[i];
				}
				if (X[i] < min) {
					min = X[i];
				}
			}
			for (double x = min; x <= max; x = x + 0.01) {
				xList.add((int) x);
				yList.add((int) lagrange(X, Y, x));
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[xList.size()] = localX + x6;
			yy[xList.size()] = localY + y6;
			xx[xList.size() + 1] = localX + x5;
			yy[xList.size() + 1] = localY + y5;

			for (int i = 0; i < xList.size(); i++) {
				xx[i] = xList.get(i);
				yy[i] = yList.get(i);
			}
			g2d.drawPolygon(xx, yy, xx.length);

		} else if (currentAngle == 270.0) {
			double[] X = { localX + x1, localX + x6, localX + x5, localX + x4 };
			double[] Y = { localY + y1, localY + y6, localY + y5, localY + y4 };
			min = Double.MAX_VALUE;
			max = Double.MIN_VALUE;
			for (int i = 0; i < Y.length; i++) {
				if (Y[i] > max) {
					max = Y[i];
				}
				if (Y[i] < min) {
					min = Y[i];
				}
			}
			for (double y = min; y <= max; y = y + 0.01) {
				xList.add((int) lagrange(Y, X, y));
				yList.add((int) y);
			}
			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = localX + x3;
			yy[0] = localY + y3;
			xx[1] = localX + x2;
			yy[1] = localY + y2;
			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}

			g2d.drawPolygon(xx, yy, xx.length);
		}
	}

	/**
	 * 平滑曲线,多个点的变化
	 * 
	 * @param x
	 *            横向坐标集合
	 * @param y
	 *            纵向坐标集合
	 * @param value
	 * @return
	 */
	public static double lagrange(double x[], double y[], double value) {
		double sum = 0;
		double L;
		for (int i = 0; i < x.length; i++) {
			L = 1;
			for (int j = 0; j < x.length; j++) {
				if (j != i) {
					L = L * (value - x[j]) / (x[i] - x[j]);
				}
			}
			sum = sum + L * y[i];
		}
		return sum;
	}

	/**
	 * 阶段分隔符
	 * 
	 * @param g2d
	 * @param localX
	 *            虚框位置点X
	 * @param localY
	 *            虚框位置点Y
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintPartitionFigure(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		// 分隔符直角高度
		int partitionwW = 36;
		if (currentAngle == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = partitionwW / 2;
			x3 = 0;
			y3 = partitionwW;
			x4 = 0;
			y4 = userHeight;
		} else if (currentAngle == 90.0) {
			x4 = 0;
			y4 = 0;
			x3 = userWidth - partitionwW;
			y3 = 0;
			x2 = userWidth - partitionwW / 2;
			y2 = userHeight;
			x1 = userWidth;
			y1 = 0;
		} else if (currentAngle == 180.0) {
			x4 = userWidth;
			y4 = 0;
			x3 = userWidth;
			y3 = userHeight - partitionwW;
			x2 = 0;
			y2 = userHeight - partitionwW / 2;
			x1 = userWidth;
			y1 = userHeight;
		} else if (currentAngle == 270.0) {
			x1 = 0;
			y1 = userHeight;
			x2 = partitionwW / 2;
			y2 = 0;
			x3 = partitionwW;
			y3 = userHeight;
			x4 = userWidth;
			y4 = userHeight;
		}

		g2d.setStroke(new BasicStroke(1));// 实线样式
		g2d.drawLine(localX + x1, localY + y1, localX + x2, localY + y2);
		g2d.drawLine(localX + x2, localY + y2, localX + x3, localY + y3);
		g2d.drawLine(localX + x3, localY + y3, localX + x4, localY + y4);
	}

	/**
	 * 活动元素边框
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 */
	public static void paintRoundRectAR(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight) {
		// 圆弧矩形边框
		g2d.drawRoundRect(localX, localY, userWidth, userHeight, 6, 6);
	}

	/**
	 * 试制报告
	 * 
	 * @param g2d
	 * @param userWidth
	 * @param userHeight
	 * @param currentAngle
	 */
	public static void paintTrialReport(Graphics2D g2d, int localX, int localY, int userWidth, int userHeight,
			double currentAngle) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		if (currentAngle == 0.0) {
			x1 = userWidth / 5;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight / 5;
		} else if (currentAngle == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth * 4 / 5;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight / 5;
			x4 = userWidth;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
		} else if (currentAngle == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight * 4 / 5;
			x4 = userWidth * 4 / 5;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
		} else if (currentAngle == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = userWidth / 5;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight * 4 / 5;
		}
		g2d.drawPolygon(new int[] { localX + x1, localX + x2, localX + x3, localX + x4, localX + x5 }, new int[] {
				localY + y1, localY + y2, localY + y3, localY + y4, localY + y5 }, 5);
	}
}
