package epros.draw.gui.workflow.aid;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * 
 * 编辑流程元素内容框监听类，主要实现编辑流程元素内容框大小改变时编辑框所在的滚动面板大小也改变
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTextAreaComponentAdapter extends ComponentAdapter {

	public JecnTextAreaComponentAdapter() {
		super();
	}

	/**
	 * Invoked when the component's size changes.
	 */
	public void componentResized(ComponentEvent e) {
		componentActionPerformed(e);
	}

	private void componentActionPerformed(ComponentEvent e) {
		if (e.getSource() instanceof JecnTextArea) {// 编辑流程元素内容框
			// 编辑流程元素内容框
			JecnTextArea textArea = (JecnTextArea) e.getSource();
			// 设置大小
			textArea.getEditScrollPane().setSize(textArea.getSize());
		}
	}
}
