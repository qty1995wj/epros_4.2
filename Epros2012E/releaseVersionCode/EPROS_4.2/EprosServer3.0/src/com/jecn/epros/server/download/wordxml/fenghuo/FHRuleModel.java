package com.jecn.epros.server.download.wordxml.fenghuo;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;

import com.jecn.epros.server.bean.download.RuleDownloadBean;
import com.jecn.epros.server.bean.download.RuleFileBean;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.download.wordxml.RuleItem;
import com.jecn.epros.server.download.wordxml.RuleModel;

public class FHRuleModel extends RuleModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/****** 段落行距固定值20磅 *********/
	private ParagraphLineRule lineRule = new ParagraphLineRule(18, LineRuleType.EXACT);

	/**
	 * 默认模版
	 * 
	 * @param ruleDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public FHRuleModel(RuleDownloadBean ruleDownloadBean, String path) {
		super(ruleDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(15.56F);
		getDocProperty().setNewRowHeight(0.6F);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle(" ", textTitleStyle());
		setDefAscii("黑体");
		setHandNumber();
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(2.82F, 2.82F, 3.5F, 2.82F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	private SectBean getTitleSectBean() {
		return getSectBean();
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommhtr(sect, HeaderOrFooterType.odd);
		createCommftr(sect, HeaderOrFooterType.odd);
	}

	private void createCommhtr(Sect sect, HeaderOrFooterType odd) {
		Header header = sect.createHeader(odd);
		Image logo = new Image(docProperty.getRuleLogoImage(this.getClass()));
		logo.setSize(15.98F, 1.43F);
		Paragraph p = header.createParagraph("");
		p.setParagraphBean(new ParagraphBean(Align.right));
		p.appendGraphRun(logo);
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		Footer footer = sect.createFooter(type);
		ParagraphBean pBean = new ParagraphBean(Align.center, Fonts.HEI, Constants.xiaosi, false);
		footer.createParagraph("公司机密,未经批准不得扩散", pBean);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		firstSect.setSectBean(getSectBean());
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		createCommhdrftr(titleSect);
		titleSect.setSectBean(getTitleSectBean());
		titleSect.createMutilEmptyParagraph(14);
		titleSect
				.createParagraph(ruleDownloadBean.getRuleName(), new ParagraphBean(Align.center, Fonts.SONG, 40, true));
		ParagraphBean pBean = new ParagraphBean(Align.left, Fonts.SONG, Constants.sanhao, false);
		pBean.setParagraphSpaceBean(new ParagraphSpaceBean(0F, 0F, new ParagraphLineRule(1.5F, LineRuleType.AUTO)));
		titleSect.createMutilEmptyParagraph(4);
		titleSect.createParagraph("				文件编号：" + nullToEmpty(ruleDownloadBean.getRuleInputNum()), pBean);
		titleSect.createParagraph("				文件版本：" + nullToEmpty(ruleDownloadBean.getVersion()), pBean);
		titleSect.createParagraph("				文件密级：" + nullToEmpty(ruleDownloadBean.getRuleIsPublicStr()), pBean);
		titleSect.createMutilEmptyParagraph(3);
		word.createHistoryTable(titleSect);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaosi, false);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		/** 默认表格样式 ****/
		TableBean tblStyle = new TableBean();// 
		// 所有边框 1.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, Fonts.SONG, Constants.shiyihao, true);
		tblTitileStyle.setAlign(Align.center);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaosi, false);
		// 2字符转厘米 0.4240284
		textContentStyle.setInd(0, 0, 0, 0.8480568f);
		textContentStyle.setSpace(0.2f, 0.2f, lineRule);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean("黑体", Constants.sanhao, false));
		textTitleStyle.setInd(0, 0, 0.01F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(0F, 0F, vLine1); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	protected void beforeHandle() {

	}

	protected void fhfile(RuleTitleT ruleTitle, RuleItem ruleFileItem, List<RuleFileBean> list, boolean hasTitle) {
		String[] titles = { "文件名称" };
		List<String[]> contents = new ArrayList<String[]>();
		float[] width = { 14 };
		if (list == null) {
			contents = null;
		} else {
			for (RuleFileBean c : list) {
				contents.add(new String[] { c.getFileName() });
			}
		}
		createTableItem(ruleFileItem, titles, contents, width, hasTitle);
	}

}
