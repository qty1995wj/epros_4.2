package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;
import epros.designer.util.JecnProperties;

/**
 * 
 * 任务界面显示项面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTaskShowPanel extends JecnAbstractPropertyBasePanel {

	/** 试运行面板 */
	private JecnTestRunPropertyPanel testRunPropertyPanel = null;

	public JecnTaskShowPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 表面板
		tableScrollPane = new JecnThreeTableScrollPane(dialog);
		// 试运行面板
		testRunPropertyPanel = new JecnTestRunPropertyPanel(dialog);

		this.add(tableScrollPane, BorderLayout.CENTER);
		this.add(testRunPropertyPanel, BorderLayout.SOUTH);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			String mark = itemBean.getMark();
			if (ConfigItemPartMapMark.taskShowRunCycle.toString().equals(mark)) {// 试运行周期(天数)默认1；
				testRunPropertyPanel.getTestRunCycleSpinner().setItemBean(itemBean);
			} else if (ConfigItemPartMapMark.taskShowRunRealseCycle.toString().equals(mark)) {// 发送试运行报告周期(天数)
				// 默认1；
				testRunPropertyPanel.getSendTestRunRecordSpinner().setItemBean(itemBean);
			} else {// 表中数据
				if (ConfigItemPartMapMark.taskShowTestRun.toString().equals(mark)) {// 试运行
					testRunPropertyPanel.initData(itemBean);
				}
			}
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getTableItemList()) {// 表数据
			// 唯一标识
			String mark = itemBean.getMark();
			if (ConfigItemPartMapMark.taskShowTestRun.toString().equals(mark)) {// 试运行
				testRunPropertyPanel.initData(itemBean);
			}
		}

		// 排序过滤隐藏项
		List<JecnConfigItemBean> tableItemList = tableScrollPane.getShowItem(configTypeDesgBean.getTableItemList(),
				JecnConfigContents.TYPE_SMALL_ITEM_TASKSHOW);
		// 显示表数据
		tableScrollPane.initData(tableItemList);
	}

	@Override
	public boolean check() {
		return testRunPropertyPanel.check();
	}
}
