package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.journal.SearchUserOperates;
import com.jecn.epros.domain.journal.SearchUserVisits;
import com.jecn.epros.domain.journal.SearchResourceVisits;
import com.jecn.epros.domain.journal.SearchWebLogBean;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.Contants;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class JournalSqlProvider extends BaseSqlProvider {

    public String searchNotice(Map<String, Object> map) {
        return this.searchWebLogCommon(map, false);
    }

    public String searchNoticeTotal(Map<String, Object> map) {
        return this.searchWebLogCommon(map, true);
    }

    public String getAuthPubSqlByTypeWebServer(Map<String, Object> map) {
        StringBuilder str = new StringBuilder();
        str.append(" select * from (");
        SearchWebLogBean bean = new SearchWebLogBean();
        bean.setFileType(1);
        map.put("searchBean", bean);
        str.append(searchWebLogCommon(map, false));
        bean.setFileType(3);
        str.append(" union ");
        str.append(searchWebLogCommon(map, false));
        str.append(" ) webLog ");
        return str.toString();
    }

    public String getFlowAndRuleAndFlowMapByCodeWebServer(Map<String, Object> map) {
        String flowAccessSql = getAccessSql(map, 0, "flow");
        String ruleAccessSql = getAccessSql(map, 3, "rule");
        String sql = "select id, name, type, pub_time, ju.true_name RES_PEOPLE" +
                "  from (select rule.id as id," +
                "               rule.rule_name as name," +
                "               'RULE' as type," +
                "               rule.pub_time," +
                "               rule.res_people_id " +
                "          From jecn_rule rule";
        if (StringUtils.isNotBlank(ruleAccessSql)) {
            sql += ruleAccessSql;
        } else {
            sql += "         where rule.is_dir <> 0 ";
        }
        sql += "        union all" +
                "        select flow.flow_id as id," +
                "               flow.flow_name as name," +
                "               case" +
                "                 when flow.isflow = 0 then" +
                "                  'PROCESS'" +
                "                 else" +
                "                  'PROCESS_MAP'" +
                "               end as type," +
                "               flow.pub_time," +
                "               flow_info.res_people_id" +
                "          from Jecn_Flow_Structure flow" +
                "          left join jecn_flow_basic_info flow_info" +
                "            on flow_info.flow_id = flow.flow_id";
        if (StringUtils.isNotBlank(flowAccessSql)) {
            sql += flowAccessSql;
        }
        sql += "            ) info" +
                "  left join jecn_user ju" +
                "    on info.res_people_id = ju.people_id" +
                "   and ju.islock = 0";
        return sql;
    }

    private String getAccessSql(Map<String, Object> map, int fileType, String fildStr) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        boolean isAdmin = (boolean) map.get("isAdmin");
        AuthSqlConstant.TYPEAUTH authType = getAuthType(fileType);
        map.put("typeAuth", authType);
        // 是不是搜索所有流程节点
        boolean isAll = false;
        String sql = "";
        if (isAdmin) {
            isAll = true;
        } else if (Contants.isFlowAdmin && fileType == 1) {
            isAll = true;
        } else if (Contants.isMainFlowAdmin && fileType == 0) {
            isAll = true;
        } else if (Contants.isSystemAdmin && fileType == 3) {
            isAll = true;
        }
        if (!isAll && (fileType == 0 || fileType == 1)) {
            sql = " Inner join " + CommonSqlTPath.getFlowAuthorityNodes(map) + " on MY_AUTH_FILES.Flow_id=" + fildStr + ".flow_id";
        } else if (!isAll && (fileType == 3)) {
            sql += " Inner join " + CommonSqlTPath.getFlowAuthorityNodes(map) + " on MY_AUTH_FILES.ID=" + fildStr + ".ID";
        }
        return sql;
    }

    /**
     * 流程搜索 通用bean
     *
     * @param map
     * @param isCount
     * @return
     */
    private String searchWebLogCommon(Map<String, Object> map, boolean isCount) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchWebLogBean searchBean = (SearchWebLogBean) map.get("searchBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        AuthSqlConstant.TYPEAUTH authType = getAuthType(searchBean.getFileType());
        map.put("typeAuth", authType);
        // 是不是搜索所有流程节点
        boolean isAll = false;
        if (isAdmin) {
            isAll = true;
        } else if (Contants.isFlowAdmin && searchBean.getFileType() == 1) {
            isAll = true;
        } else if (Contants.isMainFlowAdmin && searchBean.getFileType() == 0) {
            isAll = true;
        } else if (Contants.isSystemAdmin && searchBean.getFileType() == 3) {
            isAll = true;
        }
        String sql = getAuthPubSql(map, isCount, projectId, searchBean, isAll);

        return sql;
    }

    private String getAuthPubSql(Map<String, Object> map, boolean isCount, Long projectId, SearchWebLogBean searchBean, boolean isAll) {
        String sql = getAuthPubSqlByType(map, searchBean, isAll);

        // 流程名称
        if (StringUtils.isNotBlank(searchBean.getName())) {
            sql = sql + " and a.name like '%" + searchBean.getName() + "%'";
        }
        // 流程编号
        if (StringUtils.isNotBlank(searchBean.getNumber())) {
            sql = sql + " and a.num like '%" + searchBean.getNumber() + "%'";
        }
        // 责任部门
        if (searchBean.getOrgId() != null) {
            sql = sql + " and a.org_id=" + searchBean.getOrgId();
        }
        // 密级
        if (searchBean.getSecret() != -1) {
            sql = sql + " and a.is_public=" + searchBean.getSecret();
        }
        if (isCount) {
            sql = "select count(webLog.id) from (" + sql + ") webLog ";
//            if (searchBean.getViewType() != -1) {
//                sql = sql + " WHERE webLog.VIEW_TYPE=" + searchBean.getViewType();
//            }
        } else {
            sql = "select webLog.* from (" + sql + ") webLog ";
//            if (searchBean.getViewType() != -1) {
//                sql = sql + "WHERE webLog.VIEW_TYPE=" + searchBean.getViewType();
//            }
        }
        return sql;
    }


    private String getAuthPubSqlByType(Map<String, Object> map, SearchWebLogBean searchBean, boolean isAll) {
        // 1是流程，0是流程架构，2文件，3制度
        int fileType = searchBean.getFileType();
        // log type 0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构
        // 流程是否更新判断依据是流程如果是发布不记录文控，那么不算更新，如果流程没有文控一样的记录日子不算更新，其它都算更新
        String sql = "";
        if (fileType == 0 || fileType == 1) {
            String tableName = " jecn_flow_basic_info ";
            int isflow = 1;
            int logType = 0;
            if (fileType == 0) {
                tableName = " jecn_main_flow ";
                isflow = 0;
                logType = 7;
            }
            sql = "select distinct case when t.isflow = 1 then" +
                    "                           'PROCESS'" +
                    "                          else" +
                    "                           'PROCESS_MAP'" +
                    "                        end as type,t.flow_id as id,t.flow_name as name,  ft.flow_name as p_name,t.flow_id_input as num,org.org_id,org.org_name"
                    + "       ,jfbi.type_res_people,jfbi.res_people_id,"
                    + "       case when jfbi.type_res_people=0 then ju.true_name"
                    + "       when jfbi.type_res_people=1 then jfoi.figure_text end as res_people"
                    + "       ,t.pub_time,t.is_public,"
                    + "           CASE "
                    + "           WHEN T.HISTORY_ID IS NULL THEN "
                    + "           1 "
                    + "        WHEN JOL.ID IS NOT NULL THEN "
                    + "       1 "
                    + "      ELSE "
                    + "      0 "
                    + " END AS VIEW_TYPE, his.version_id";
            sql = sql + " from jecn_flow_structure t  left join jecn_flow_structure ft " +
                    "            on t.pre_flow_id = ft.flow_id " +
                    "           and ft.del_state = 0";
            sql += "    left join jecn_task_history_new his   " +
                    "      on t.flow_id = his.relate_id" +
                    "       and t.history_id = his.id";
            sql = sql + " LEFT JOIN  JECN_OPR_LOG JOL ON JOL.RELATED_TYPE=" + logType + " AND t.flow_id=JOL.RELATED_ID AND t.history_id=JOL.history_id";
            sql = sql + "," + tableName + " jfbi" + " left join jecn_flow_related_org jfro on jfro.flow_id=jfbi.flow_id"
                    + " left join jecn_flow_org org on jfro.org_id=org.org_id and org.del_state=0"
                    + " left join jecn_flow_org_image jfoi on  jfoi.figure_id=jfbi.res_people_id"
                    + " left join jecn_user ju on  ju.islock=0 and ju.people_id=jfbi.res_people_id";

            if (!isAll) {
                sql = sql + " Inner join " + CommonSqlTPath.getFlowAuthorityNodes(map) + " on MY_AUTH_FILES.Flow_id=jfbi.flow_id";
            }
            sql = sql + " where t.isflow=" + isflow + " and t.del_state=0 and t.flow_id=jfbi.flow_id";
        } else if (fileType == 2) {
            throw new RuntimeException("最新发布的文件未实现");
        } else if (fileType == 3) {// 制度
            sql = "SELECT DISTINCT 'RULE' as TYPE,T.ID AS ID," +
                    "                T.RULE_NAME AS NAME,    ft.Rule_Name as p_name," +
                    "                T.RULE_NUMBER AS NUM," +
                    "                ORG.ORG_ID," +
                    "                ORG.ORG_NAME," +
                    "                T.TYPE_RES_PEOPLE," +
                    "                T.RES_PEOPLE_ID," +
                    "                CASE" +
                    "                  WHEN T.TYPE_RES_PEOPLE = 0 THEN" +
                    "                   JU.TRUE_NAME" +
                    "                  WHEN T.TYPE_RES_PEOPLE = 1 THEN" +
                    "                   JFOI.FIGURE_TEXT" +
                    "                END AS RES_PEOPLE," +
                    "                T.PUB_TIME," +
                    "                T.IS_PUBLIC," +
                    "                CASE" +
                    "                  WHEN T.HISTORY_ID IS NULL THEN" +
                    "                   1" +
                    "                  WHEN JOL.ID IS NOT NULL THEN" +
                    "                   1" +
                    "                  ELSE" +
                    "                   0" +
                    "                END AS VIEW_TYPE, his.version_id" +
                    "  FROM JECN_RULE T" +
                    "             left join JECN_RULE ft " +
                    "          on t.per_id = ft.id " +
                    "          and ft.del_state = 0 " +
                    "    left join jecn_task_history_new his   " +
                    "      on T.id = his.relate_id" +
                    "       and T.history_id = his.id" +
                    "  LEFT JOIN JECN_OPR_LOG JOL" +
                    "    ON JOL.RELATED_TYPE = 1" +
                    "   AND T.ID = JOL.RELATED_ID" +
                    "   AND T.HISTORY_ID = JOL.HISTORY_ID" +
                    "  LEFT JOIN JECN_FLOW_ORG ORG" +
                    "    ON T.ORG_ID = ORG.ORG_ID" +
                    "   AND ORG.DEL_STATE = 0" +
                    "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI" +
                    "    ON  JFOI.FIGURE_ID = T.RES_PEOPLE_ID" +
                    "  LEFT JOIN JECN_USER JU" +
                    "    ON  JU.ISLOCK = 0" +
                    "   AND JU.PEOPLE_ID = T.RES_PEOPLE_ID";
            if (!isAll) {
                sql = sql + " INNER JOIN " + CommonSqlTPath.getRuleAuthorityNodes(map) + " on MY_AUTH_FILES.ID=T.ID";
            }
            sql = sql + " WHERE T.IS_DIR<>0 AND T.DEL_STATE=0 ";
        }

        return "select * from (" + sql + ")a where 1=1 ";
    }

    private boolean hasAuth(AuthSqlConstant.TYPEAUTH authType) {
        switch (authType) {
            case FILE:
                return Contants.isFileAdmin;
            case FLOW:
                return Contants.isFlowAdmin;
            case RULE:

        }
        return false;
    }

    /**
     * -1是全部，1是流程，0是流程架构，2文件，3制度
     *
     * @param fileType
     * @return
     */
    private AuthSqlConstant.TYPEAUTH getAuthType(int fileType) {
        switch (fileType) {
            case 0:
            case 1:
                return AuthSqlConstant.TYPEAUTH.FLOW;
            case 2:
                return AuthSqlConstant.TYPEAUTH.FILE;
            case 3:
                return AuthSqlConstant.TYPEAUTH.RULE;

        }
        throw new IllegalArgumentException("getAuthType不支持的类型：" + fileType);
    }

    /**
     * 日志详情 总数
     *
     * @param map
     * @return
     */
    public String getLogDetailResultTotal(Map<String, Object> map) {
        return "select count(*) " + getLogDetailCommon(map);
    }

    /***
     * 日志详情 通用
     *
     * @param map
     * @return
     */
    private String getLogDetailCommon(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchUserVisits searchLogDetailBean = (SearchUserVisits) map.get("searchLogDetailBean");
        String sql = " FROM (";
        if (searchLogDetailBean.getRelatedType() == -1) {// 全部
            sql += getAllRelatedTableSql();
        } else {
            sql += getRelatedTableSqlByType(searchLogDetailBean.getRelatedType());
        }
        sql += ") JOL WHERE 1=1 ";

        if (StringUtils.isNotBlank(searchLogDetailBean.getRelatedName())) {
            sql += " AND JOL.RELATED_NAME LIKE '%" + searchLogDetailBean.getRelatedName() + "%'";
        }

        // 用户类型 0是人员，1是岗位，2是部门
        if (searchLogDetailBean.getOperationUserId() != null && searchLogDetailBean.getOperationUserId() != 0) {
            switch (searchLogDetailBean.getUserType()) {
                case 0:
                    sql += " and JOL.PEOPLE_ID = " + searchLogDetailBean.getOperationUserId();
                    break;
                case 1:
                    sql += " and JOL.PEOPLE_ID in (" + SqlCommon.getUserByPosId(searchLogDetailBean.getOperationUserId()) + ")";
                    break;
                case 2:
                    sql += " and JOL.PEOPLE_ID in (" + SqlCommon.getUserByOrgId(searchLogDetailBean.getOperationUserId(), projectId)
                            + ")";
            }
        }
        // 开始时间 结束时间
        if (StringUtils.isNotBlank(searchLogDetailBean.getStartTime()) && StringUtils.isNotBlank(searchLogDetailBean.getEndTime())) {
            // 字符串转换成时间拼接时、分、秒
            String startTime = searchLogDetailBean.getStartTime() + " 00:00:00";
            String endTime = searchLogDetailBean.getEndTime() + " 23:59:59";

            sql += " and jol.operator_time>=" + getStrToDBFunction(startTime) + " and jol.operator_time<="
                    + getStrToDBFunction(endTime);
        }
        return sql;
    }

    /**
     * 日志统计 通用
     *
     * @param map
     * @return
     */
    private String getLogStatisticsCommon(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchResourceVisits searchLogStatisticsBean = (SearchResourceVisits) map.get("searchLogStatisticsBean");
        String sql = " FROM (";
        if (searchLogStatisticsBean.getRelatedType() == -1) {// 全部
            sql += getAllRelatedTableTotalSql(searchLogStatisticsBean);
        } else {
            sql += getRelatedTableTotalSqlByType(searchLogStatisticsBean.getRelatedType(), searchLogStatisticsBean);
        }
        sql += ") JOL WHERE 1=1 ";
        return sql;
    }

    private String getLogStatisticsConditionSql(SearchResourceVisits searchLogStatisticsBean, String name) {
        String sql = "";
        if (StringUtils.isNotBlank(searchLogStatisticsBean.getRelatedName())) {
            sql += (" and " + name + " LIKE '%" + searchLogStatisticsBean.getRelatedName() + "%'");
        }
        // 开始时间 结束时间
        if (StringUtils.isNotBlank(searchLogStatisticsBean.getStartTime()) && StringUtils.isNotBlank(searchLogStatisticsBean.getEndTime())) {
            // 字符串转换成时间拼接时、分、秒
            String startTime = searchLogStatisticsBean.getStartTime() + " 00:00:00";
            String endTime = searchLogStatisticsBean.getEndTime() + " 23:59:59";
            sql += (" and jol.operator_time>=" + getStrToDBFunction(startTime) + " and jol.operator_time<="
                    + getStrToDBFunction(endTime));
        }
        return sql;
    }

    /**
     * 获取所有资源的访问日志
     *
     * @return
     */
    private String getAllRelatedTableSql() {
        return getRelatedFlowSql() +
                " UNION ALL " +
                getRelatedRuleSql() +
                " UNION ALL " +
                getRelatedFileSql() +
                " UNION ALL " +
                getRelatedStandardSql() +
                " UNION ALL " +
                getRelatedRiskSql() +
                " UNION ALL " +
                getRelatedOrgSql() +
                " UNION ALL " +
                getRelatedPositionSql() +
                " UNION ALL " +
                getRelatedFlowMapSql();
    }

    /**
     * 资源访问统计
     *
     * @param condition
     * @return
     */
    private String getAllRelatedTableTotalSql(SearchResourceVisits condition) {
        return getRelatedFlowTotalSql(condition) +
                " UNION ALL " +
                getRelatedRuleTotalSql(condition) +
                " UNION ALL " +
                getRelatedFileTotalSql(condition) +
                " UNION ALL " +
                getRelatedStandardTotalSql(condition) +
                " UNION ALL " +
                getRelatedRiskTotalSql(condition) +
                " UNION ALL " +
                getRelatedOrgTotalSql(condition) +
                " UNION ALL " +
                getRelatedPositionTotalSql(condition) +
                " UNION ALL " +
                getRelatedFlowMapTotalSql(condition);
    }

    private String getRelatedTableSqlByType(int relatedType) {
        String sql = "";
        switch (relatedType) {
            case 0:
                sql = getRelatedFlowSql();
                break;
            case 1:
                sql = getRelatedRuleSql();
                break;
            case 2:
                sql = getRelatedFileSql();
                break;
            case 3:
                sql = getRelatedStandardSql();
                break;
            case 4:
                sql = getRelatedRiskSql();
                break;
            case 5:
                sql = getRelatedOrgSql();
                break;
            case 6:
                sql = getRelatedPositionSql();
                break;
            case 7:
                sql = getRelatedFlowMapSql();
                break;
            default:
                break;
        }
        return sql;
    }

    private String getRelatedTableTotalSqlByType(int relatedType, SearchResourceVisits condition) {
        String sql = "";
        switch (relatedType) {
            case 0:
                sql = getRelatedFlowTotalSql(condition);
                break;
            case 1:
                sql = getRelatedRuleTotalSql(condition);
                break;
            case 2:
                sql = getRelatedFileTotalSql(condition);
                break;
            case 3:
                sql = getRelatedStandardTotalSql(condition);
                break;
            case 4:
                sql = getRelatedRiskTotalSql(condition);
                break;
            case 5:
                sql = getRelatedOrgTotalSql(condition);
                break;
            case 6:
                sql = getRelatedPositionTotalSql(condition);
                break;
            case 7:
                sql = getRelatedFlowMapTotalSql(condition);
                break;
            default:
                break;
        }
        return sql;
    }

    private String getRelatedFlowSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       FS.FLOW_NAME      AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON FS.FLOW_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 0" +
                "   AND FS.ISFLOW = 1" +
                " WHERE FS.FLOW_NAME IS NOT NULL AND FS.DEL_STATE = 0";
    }

    private String getRelatedFlowMapSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       FS.FLOW_NAME      AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON FS.FLOW_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 7" +
                "   AND FS.ISFLOW = 0" +
                " WHERE FS.FLOW_NAME IS NOT NULL AND FS.DEL_STATE = 0";
    }

    private String getRelatedRuleSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       J_R.RULE_NAME     AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_RULE J_R" +
                "    ON J_R.ID = JOL.RELATED_ID AND J_R.DEL_STATE=0" +
                "   AND JOL.RELATED_TYPE = 1" +
                " WHERE J_R.RULE_NAME IS NOT NULL";
    }

    private String getRelatedFileSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       JF.FILE_NAME      AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FILE JF" +
                "    ON JF.FILE_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 2" +
                " WHERE JF.FILE_NAME IS NOT NULL AND JF.DEL_STATE = 0";
    }

    private String getRelatedStandardSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       JCC.CRITERION_CLASS_NAME AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_CRITERION_CLASSES JCC" +
                "    ON JCC.CRITERION_CLASS_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 3" +
                " WHERE JCC.CRITERION_CLASS_NAME IS NOT NULL";
    }

    private String getRelatedRiskSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       JR.NAME           AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_RISK JR" +
                "    ON JR.ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 4" +
                " WHERE JR.NAME IS NOT NULL";
    }

    private String getRelatedOrgSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       O.ORG_NAME        AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG O" +
                "    ON O.ORG_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 5" +
                " WHERE O.ORG_NAME IS NOT NULL";
    }

    private String getRelatedPositionSql() {
        return "SELECT JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATED_ID," +
                "       OI.FIGURE_TEXT    AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       JOL.OPERATOR_TIME,JOL.OPERATOR_TYPE" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE OI" +
                "    ON OI.FIGURE_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 6" +
                " WHERE OI.FIGURE_TEXT IS NOT NULL";
    }


    private String getRelatedFlowTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       FS.FLOW_NAME      AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(FS.FLOW_NAME) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON FS.FLOW_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 0" +
                "   AND FS.ISFLOW = 1 AND FS.DEL_STATE = 0" +
                " WHERE FS.FLOW_NAME IS NOT NULL " + getLogStatisticsConditionSql(condition, "FS.FLOW_NAME") +
                "  GROUP BY JOL.RELATED_ID, FS.FLOW_NAME, JOL.RELATED_TYPE";
    }

    private String getRelatedFlowMapTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       FS.FLOW_NAME      AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(FS.FLOW_NAME) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON FS.FLOW_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 7" +
                "   AND FS.ISFLOW = 0 AND FS.DEL_STATE = 0" +
                " WHERE FS.FLOW_NAME IS NOT NULL" + getLogStatisticsConditionSql(condition, "FS.FLOW_NAME") +
                "  GROUP BY JOL.RELATED_ID, FS.FLOW_NAME, JOL.RELATED_TYPE";
    }

    private String getRelatedRuleTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       J_R.RULE_NAME     AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(J_R.RULE_NAME) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_RULE J_R" +
                "    ON J_R.ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 1 AND J_R.DEL_STATE = 0" +
                " WHERE J_R.RULE_NAME IS NOT NULL" + getLogStatisticsConditionSql(condition, "J_R.RULE_NAME") +
                "  GROUP BY JOL.RELATED_ID, J_R.RULE_NAME, JOL.RELATED_TYPE";
    }

    private String getRelatedFileTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       JF.FILE_NAME      AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(JF.FILE_NAME) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FILE JF" +
                "    ON JF.FILE_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 2 AND JF.DEL_STATE = 0" +
                " WHERE JF.FILE_NAME IS NOT NULL" + getLogStatisticsConditionSql(condition, "JF.FILE_NAME") +
                "  GROUP BY JOL.RELATED_ID, JF.FILE_NAME, JOL.RELATED_TYPE";
    }

    private String getRelatedStandardTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       JCC.CRITERION_CLASS_NAME AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(JCC.CRITERION_CLASS_NAME) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_CRITERION_CLASSES JCC" +
                "    ON JCC.CRITERION_CLASS_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 3" +
                " WHERE JCC.CRITERION_CLASS_NAME IS NOT NULL" + getLogStatisticsConditionSql(condition, "JCC.CRITERION_CLASS_NAME") +
                "  GROUP BY JOL.RELATED_ID, JCC.CRITERION_CLASS_NAME, JOL.RELATED_TYPE";
    }

    private String getRelatedRiskTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       JR.NAME           AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(JR.NAME) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_RISK JR" +
                "    ON JR.ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 4" +
                " WHERE JR.NAME IS NOT NULL" + getLogStatisticsConditionSql(condition, "JR.NAME") +
                "  GROUP BY JOL.RELATED_ID, JR.NAME, JOL.RELATED_TYPE";
    }

    private String getRelatedOrgTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       O.ORG_NAME        AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(O.ORG_NAME) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG O" +
                "    ON O.ORG_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 5" +
                " WHERE O.ORG_NAME IS NOT NULL" + getLogStatisticsConditionSql(condition, "O.ORG_NAME") +
                "  GROUP BY JOL.RELATED_ID, O.ORG_NAME , JOL.RELATED_TYPE";
    }

    private String getRelatedPositionTotalSql(SearchResourceVisits condition) {
        return "SELECT JOL.RELATED_ID," +
                "       OI.FIGURE_TEXT    AS RELATED_NAME," +
                "       JOL.RELATED_TYPE," +
                "       COUNT(OI.FIGURE_TEXT) OPERATOR_TOTAL" +
                "  FROM JECN_OPR_LOG JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.OPERATOR_PEOPLE = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE OI" +
                "    ON OI.FIGURE_ID = JOL.RELATED_ID" +
                "   AND JOL.RELATED_TYPE = 6" +
                " WHERE OI.FIGURE_TEXT IS NOT NULL" + getLogStatisticsConditionSql(condition, "OI.FIGURE_TEXT") +
                "  GROUP BY JOL.RELATED_ID, OI.FIGURE_TEXT , JOL.RELATED_TYPE";
    }


    /**
     * 日志详情
     *
     * @param map
     * @return
     */
    public String getLogDetailResult(Map<String, Object> map) {
        String sql = "SELECT JOL.RELATED_ID, JOL.RELATED_NAME,JOL.RELATED_TYPE,JOL.OPERATOR_TYPE,JOL.OPERATOR_NAME,JOL.OPERATOR_TIME";
        return sql + getLogDetailCommon(map);
    }

    /**
     * 资源访问日志统计
     *
     * @param map
     * @return
     */
    public String getLogStatisticsResult(Map<String, Object> map) {
        String sql = "SELECT JOL.RELATED_ID, JOL.RELATED_NAME,JOL.RELATED_TYPE,JOL.OPERATOR_TOTAL ";
        return sql + getLogStatisticsCommon(map);
    }

    /**
     * 资源访问日志统计
     *
     * @param map
     * @return
     */
    public String getLogStatisticsResultTotal(Map<String, Object> map) {
        String sql = "select count(*) ";
        return sql + getLogStatisticsCommon(map);
    }

    /**
     * 根据log类型获得文控id
     *
     * @param map
     * @return
     */
    public String getHistoryIdByLogType(Map<String, Object> map) {
        // logType 日志类型：0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位 7流程架构
        // type 文控信息类型：0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
        int logType = Integer.valueOf(map.get("relatedType").toString());
        String basicSql = "select max(a.id) from jecn_task_history_new a where a.relate_id=#{relatedId} and ";
        String sql = "";
        if (logType == 0) {
            sql += basicSql + " type=0 ";
        } else if (logType == 1) {
            sql += basicSql + " (type=2 or type=3) ";
        } else if (logType == 2) {
            sql += basicSql + " type=1";
        } else if (logType == 7) {
            sql += basicSql + " type=4 ";
        } else {
            throw new IllegalArgumentException("该日志类型没有文控信息,logType:" + logType);
        }
        return sql;
    }

    /**
     * 日志详情 总数
     *
     * @param map
     * @return
     */
    public String getLogOperateResultTotal(Map<String, Object> map) {
        return "select count(*) " + getLogOperateCommon(map);
    }

    /**
     * 日志详情
     *
     * @param map
     * @return
     */
    public String getLogOperateResult(Map<String, Object> map) {
        String sql = "SELECT JOL.ID,JOL.RELATE_ID, JOL.RELATED_NAME,JOL.TYPE,JOL.OPERATOR_TYPE,JOL.OPERATOR_NAME,JOL.UPDATE_TIME,JOL.RELATE_NAME,JOL.SECOND_OPERATOR_TYPE";
        return sql + getLogOperateCommon(map);
    }

    /***
     * 日志详情 通用
     *
     * @param map
     * @return
     */
    private String getLogOperateCommon(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchUserOperates searchLogDetailBean = (SearchUserOperates) map.get("searchLogDetailBean");
        String sql = " FROM (";
        if (searchLogDetailBean.getRelatedType() == -1) {// 全部
            sql += getOperateAllRelatedTableSql();
        } else {
            sql += getOperateRelatedTableSqlByType(searchLogDetailBean.getRelatedType());
        }
        sql += ") JOL WHERE 1=1 ";

        if (StringUtils.isNotBlank(searchLogDetailBean.getRelatedName())) {
            sql += " AND (JOL.RELATED_NAME LIKE '%" + searchLogDetailBean.getRelatedName() + "%' OR JOL.RELATE_NAME LIKE '%" + searchLogDetailBean.getRelatedName() + "%')";
        }

        // 用户类型 0是人员，1是岗位，2是部门
        if (searchLogDetailBean.getOperationUserId() != null && searchLogDetailBean.getOperationUserId() != 0) {
            switch (searchLogDetailBean.getUserType()) {
                case 0:
                    sql += " and JOL.PEOPLE_ID = " + searchLogDetailBean.getOperationUserId();
                    break;
                case 1:
                    sql += " and JOL.PEOPLE_ID in (" + SqlCommon.getUserByPosId(searchLogDetailBean.getOperationUserId()) + ")";
                    break;
                case 2:
                    sql += " and JOL.PEOPLE_ID in (" + SqlCommon.getUserByOrgId(searchLogDetailBean.getOperationUserId(), projectId)
                            + ")";
            }
        }
        // 开始时间 结束时间
        if (StringUtils.isNotBlank(searchLogDetailBean.getStartTime()) && StringUtils.isNotBlank(searchLogDetailBean.getEndTime())) {
            // 字符串转换成时间拼接时、分、秒
            String startTime = searchLogDetailBean.getStartTime() + " 00:00:00";
            String endTime = searchLogDetailBean.getEndTime() + " 23:59:59";

            sql += " and jol.update_time>=" + getStrToDBFunction(startTime) + " and jol.update_time<="
                    + getStrToDBFunction(endTime);
        }
        return sql;
    }

    /**
     * 日志类型 1是流程，2是文件，3是角色,4是制度,5是标准,6是组织,9是风险,15 术语定义 16岗位组 ，17支持工具， 18风险点，
     * 19制度模板 ， 20模型
     */
    private String getOperateAllRelatedTableSql() {
        return getOperateRelatedFlowSql() +
                " UNION ALL " + getOperateRelatedRoleSql() +
                " UNION ALL " + getOperateRelatedRuleSql() +
                " UNION ALL " + getOperateRelatedFileSql() +
                " UNION ALL " + getOperateRelatedStandardSql() +
                " UNION ALL " + getOperateRelatedRiskSql() +
                " UNION ALL " + getOperateRelatedOrgSql() +
                " UNION ALL " + getOperateRelatedLibSql() +
                " UNION ALL " + getOperateRelatedGroupSql() +
                " UNION ALL " + getOperateRelatedToolsSql() +
                " UNION ALL " + getOperateRelatedRiskPointSql() +
                " UNION ALL " + getOperateRelatedRuleModelSql() +
                " UNION ALL " + getOperateRelatedModelSql();
    }

    /**
     * 日志类型 1是流程，2是文件，3是角色,4是制度,5是标准,6是组织,9是风险,15 术语定义 16岗位组 ，17支持工具， 18风险点，
     * 19制度模板 ， 20模型
     */
    private String getOperateRelatedTableSqlByType(int relatedType) {
        String sql = "";
        switch (relatedType) {
            case 1:
                sql = getOperateRelatedFlowSql();
                break;
            case 3:
                sql = getOperateRelatedRoleSql();
                break;
            case 4:
                sql = getOperateRelatedRuleSql();
                break;
            case 2:
                sql = getOperateRelatedFileSql();
                break;
            case 5:
                sql = getOperateRelatedStandardSql();
                break;
            case 6:
                sql = getOperateRelatedOrgSql();
                break;
            case 9:
                sql = getOperateRelatedRiskSql();
                break;
            case 15:
                sql = getOperateRelatedLibSql();
                break;
            case 16:
                sql = getOperateRelatedGroupSql();
                break;
            case 17:
                sql = getOperateRelatedToolsSql();
                break;
            case 18:
                sql = getOperateRelatedRiskPointSql();
                break;
            case 19:
                sql = getOperateRelatedRuleModelSql();
                break;
            case 20:
                sql = getOperateRelatedModelSql();
                break;
            default:
                throw new IllegalArgumentException("getOperateRelatedTableSqlByType()未知的类型：" + relatedType);
        }
        return sql;
    }

    private String getOperateRelatedModelSql() {
        return getOperateSql("JECN_FLOW_TEMPLATE_FOLLOW", "id", "name", 20);
    }

    private String getOperateRelatedRuleModelSql() {
        return getOperateSql("jecn_mode", "id", "mode_name", 19);
    }

    private String getOperateSql(String table, String idCol, String nameCol, int type) {
        return "SELECT JOL.ID," +
                "               JU.PEOPLE_ID," +
                "               JU.TRUE_NAME      OPERATOR_NAME," +
                "               JOL.RELATE_ID," +
                "               O." + nameCol + "            AS RELATED_NAME," +
                "               JOL.TYPE," +
                "               JOL.UPDATE_TIME," +
                "               JOL.OPERATOR_TYPE," +
                "               JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "          FROM JECN_JOURNAL JOL" +
                "          LEFT JOIN JECN_USER JU" +
                "            ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "          LEFT JOIN " + table + " O" +
                "            ON O." + idCol + " = JOL.RELATE_ID" +
                "         WHERE JOL.TYPE = " + type;
    }

    private String getOperateRelatedRiskPointSql() {
        return "SELECT JOL.ID," +
                "               JU.PEOPLE_ID," +
                "               JU.TRUE_NAME      OPERATOR_NAME," +
                "               JOL.RELATE_ID," +
                "               O.NAME            AS RELATED_NAME," +
                "               JOL.TYPE," +
                "               JOL.UPDATE_TIME," +
                "               JOL.OPERATOR_TYPE," +
                "               JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "          FROM JECN_JOURNAL JOL" +
                "          LEFT JOIN JECN_USER JU" +
                "            ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "          LEFT JOIN jecn_control_guide O" +
                "            ON O.ID = JOL.RELATE_ID" +
                "         WHERE JOL.TYPE = 18";
    }

    private String getOperateRelatedToolsSql() {
        return "SELECT JOL.ID," +
                "               JU.PEOPLE_ID," +
                "               JU.TRUE_NAME      OPERATOR_NAME," +
                "               JOL.RELATE_ID," +
                "               O.FLOW_SUSTAIN_TOOL_DESCRIBE            AS RELATED_NAME," +
                "               JOL.TYPE," +
                "               JOL.UPDATE_TIME," +
                "               JOL.OPERATOR_TYPE," +
                "               JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "          FROM JECN_JOURNAL JOL" +
                "          LEFT JOIN JECN_USER JU" +
                "            ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "          LEFT JOIN jecn_flow_sustain_tool O" +
                "            ON O.FLOW_SUSTAIN_TOOL_ID = JOL.RELATE_ID" +
                "         WHERE JOL.TYPE = 17";
    }

    private String getOperateRelatedGroupSql() {
        return "SELECT JOL.ID," +
                "               JU.PEOPLE_ID," +
                "               JU.TRUE_NAME      OPERATOR_NAME," +
                "               JOL.RELATE_ID," +
                "               O.NAME            AS RELATED_NAME," +
                "               JOL.TYPE," +
                "               JOL.UPDATE_TIME," +
                "               JOL.OPERATOR_TYPE," +
                "               JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "          FROM JECN_JOURNAL JOL" +
                "          LEFT JOIN JECN_USER JU" +
                "            ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "          LEFT JOIN jecn_position_group O" +
                "            ON O.ID = JOL.RELATE_ID" +
                "         WHERE JOL.TYPE = 16 ";
    }

    private String getOperateRelatedRoleSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       O.ROLE_NAME        AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_ROLE_INFO O" +
                "    ON O.ROLE_ID = JOL.RELATE_ID" +
                " WHERE JOL.TYPE = 3 ";
    }

    private String getOperateRelatedLibSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       O.NAME        AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN TERM_DEFINITION_LIBRARY O" +
                "    ON O.ID = JOL.RELATE_ID" +
                "   AND JOL.TYPE = 15" +
                " WHERE JOL.TYPE = 15 ";
    }

    private String getOperateRelatedOrgSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       O.ORG_NAME        AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG O" +
                "    ON O.ORG_ID = JOL.RELATE_ID" +
                "   AND JOL.TYPE = 6" +
                " WHERE JOL.TYPE = 6 ";
    }

    private String getOperateRelatedRiskSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       JR.NAME           AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_RISK JR" +
                "    ON JR.ID = JOL.RELATE_ID" +
                "   AND JOL.TYPE = 9" +
                " WHERE JOL.TYPE = 9 ";
    }

    private String getOperateRelatedStandardSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       JCC.CRITERION_CLASS_NAME AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_CRITERION_CLASSES JCC" +
                "    ON JCC.CRITERION_CLASS_ID = JOL.RELATE_ID" +
                "   AND JOL.TYPE = 5" +
                " WHERE JOL.TYPE = 5 ";
    }

    private String getOperateRelatedFileSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       JF.FILE_NAME      AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FILE_T JF" +
                "    ON JF.FILE_ID = JOL.RELATE_ID" +
                "   AND JOL.TYPE = 2" +
                " WHERE JOL.TYPE = 2 ";
    }

    private String getOperateRelatedRuleSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       J_R.RULE_NAME     AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_RULE_T J_R" +
                "    ON J_R.ID = JOL.RELATE_ID " +
                " WHERE JOL.TYPE = 4 ";
    }

    private String getOperateRelatedFlowSql() {
        return "SELECT  JOL.ID,JU.PEOPLE_ID,JU.TRUE_NAME OPERATOR_NAME," +
                "       JOL.RELATE_ID," +
                "       FS.FLOW_NAME      AS RELATED_NAME," +
                "       JOL.TYPE," +
                "       JOL.UPDATE_TIME,JOL.OPERATOR_TYPE," +
                "       JOL.RELATE_NAME," +
                "       JOL.SECOND_OPERATOR_TYPE" +
                "  FROM JECN_JOURNAL JOL" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON JOL.UPDATE_PEOPLE_ID = JU.PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_T FS" +
                "    ON FS.FLOW_ID = JOL.RELATE_ID" +
                " WHERE JOL.TYPE = 1 ";
    }

    public String operateTimeBigEqCount(Map<String, Object> map) {
        Long historyId = (Long) map.get("historyId");
        String sql = "select count(1)" +
                "  from JECN_OPR_LOG" +
                " where related_type = #{relatedType}" +
                "   and related_id = #{relatedId}" +
                "   and operator_type = #{compareType}" +
                "   and operator_people = #{operatorPeople}" +
                "   and operator_time >= #{compareTime}";
        if (historyId != null) {
            sql += "   and history_id=#{historyId}";
        }
        return sql;
    }

}
