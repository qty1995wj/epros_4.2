package com.jecn.epros.domain.process;

/**
 * @author yxw 2012-10-25
 * @description：流程地图基本信息临时表对表bean
 */
public class MainFlowT implements java.io.Serializable {
    private Long flowId;//流程ID
    private String flowAim;//目的
    private String flowArea;//范围
    private String flowInput;//输入
    private String flowOutput;//输出
    private String flowShowStep;//步骤
    private String flowNounDefine;//术语定义
    private String fileName;//文件名称（不存放在数据库中）
    private Long updateOrnot;//更新与否
    private Long fileId;//文件ID

    public MainFlowT() {
    }

    public MainFlowT(Long flowId) {
        this.flowId = flowId;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowAim() {
        return flowAim;
    }

    public void setFlowAim(String flowAim) {
        this.flowAim = flowAim;
    }

    public String getFlowArea() {
        return flowArea;
    }

    public void setFlowArea(String flowArea) {
        this.flowArea = flowArea;
    }

    public String getFlowInput() {
        return flowInput;
    }

    public void setFlowInput(String flowInput) {
        this.flowInput = flowInput;
    }

    public String getFlowOutput() {
        return flowOutput;
    }

    public void setFlowOutput(String flowOutput) {
        this.flowOutput = flowOutput;
    }

    public String getFlowShowStep() {
        return flowShowStep;
    }

    public void setFlowShowStep(String flowShowStep) {
        this.flowShowStep = flowShowStep;
    }

    public String getFlowNounDefine() {
        return flowNounDefine;
    }

    public void setFlowNounDefine(String flowNounDefine) {
        this.flowNounDefine = flowNounDefine;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getUpdateOrnot() {
        return updateOrnot;
    }

    public void setUpdateOrnot(Long updateOrnot) {
        this.updateOrnot = updateOrnot;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }
}
