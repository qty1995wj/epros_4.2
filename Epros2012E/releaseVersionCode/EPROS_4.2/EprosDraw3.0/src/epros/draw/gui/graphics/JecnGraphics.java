package epros.draw.gui.graphics;

import java.awt.Graphics2D;
import java.awt.Polygon;

import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 图形算法
 * 
 * @author ZHOUXY
 * 
 */
public class JecnGraphics {

	/**
	 * 
	 * 画箭头
	 * 
	 * @param g
	 *            Graphics2D 画笔
	 * @param editPointType
	 *            EditPointType 箭头对应的图形的编辑点类型
	 * @param w
	 *            箭头矩形的宽
	 * @param h
	 *            箭头矩形的高
	 */
	public static void paintTriangleShape(Graphics2D g,
			EditPointType editPointType, int w, int h) {

		if (g == null || editPointType == null) {
			return;
		}

		// 多边线对象
		Polygon triangle = new Polygon();

		switch (editPointType) {
		case bottom:// 图形下编辑点，箭头顶点在北面
			triangle.addPoint(w / 2, 0);
			triangle.addPoint(w, h);
			triangle.addPoint(0, h);
			break;
		case top:// 图形上编辑点，箭头顶点在南面
			triangle.addPoint(w / 2, h);
			triangle.addPoint(w, 0);
			triangle.addPoint(0, 0);
			break;
		case right:// 图形右编辑点，箭头顶点在西面
			triangle.addPoint(0, h / 2);
			triangle.addPoint(w, 0);
			triangle.addPoint(w, h);
			break;
		default:// 图形左编辑点， 箭头顶点在东面
			triangle.addPoint(w, h / 2);
			triangle.addPoint(0, h);
			triangle.addPoint(0, 0);
			break;
		}
		g.fill(triangle);
	}

	/**
	 * 
	 * 画直线:斜线、水平垂直直线
	 * 
	 * @param g
	 *            Graphics2D 画笔
	 * @param flowElementData
	 *            JecnDividingLineData 线属性
	 */
	public static void paintLineShape(Graphics2D g,
			JecnBaseDivedingLineData flowElementData) {
		if (g == null || flowElementData == null) {
			return;
		}
		// 线条属性
		g.setStroke(flowElementData.getBasicStroke());
		// 线条颜色
		g.setColor(flowElementData.getBodyColor());
		g.setRenderingHints(JecnUIUtil.getRenderingHints());
		// 画线条
		g.drawLine(flowElementData.getDiviLineCloneable().getStartXInt(),
				flowElementData.getDiviLineCloneable().getStartYInt(),
				flowElementData.getDiviLineCloneable().getEndXInt(),
				flowElementData.getDiviLineCloneable().getEndYInt());
	}

}
