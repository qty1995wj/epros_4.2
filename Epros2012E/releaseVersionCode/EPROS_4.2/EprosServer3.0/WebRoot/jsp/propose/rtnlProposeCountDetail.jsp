<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
</head>
<body>
		<div id="proposeDiv" width="100%" class="div" style="height: 99%;">
			<div id="search" width="100%" class="divBorder" style="border-width: 1px; border-style: solid; border-color: RGB(199, 199, 199); margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 0px;">
			   <div class="minWidth">
				<table width="100%">
					<tr style="margin-top: 4px">
						<td align="left" width="100px"></td>
						<td align="center">
						     <div style="width: 100%">
						     &nbsp;
						     </div>
						</td>
						<td align="right" width="240px" style="margin-right: 2px">
							<s:text name="startTimeC" />
							<s:textfield readonly="true" cssClass="Wdate" id="startTime3"
								cssStyle="border-color: #7F9DB9;margin-right: 4px"
								onclick="WdatePicker();" theme="simple" />
						</td>
						<td align="right" width="240px" style="margin-right: 2px">
							<s:text name="endTimeC" />
							<s:textfield readonly="true" cssClass="Wdate" id="endTime3"
								cssStyle="border-color: #7F9DB9;margin-right: 4px"
								onclick="WdatePicker();" theme="simple" />
						</td>
					</tr>
				</table>
			    <table width="100%">
					<tr style="margin-top: 4px">
						<td align="left" width="100px">
							<s:text name="proposeSendPerson"></s:text>
						</td>
						<td align="center" style="margin-right: 2px">
							<div id="select_person_id_detail" style="width: 100%">
							&nbsp;
							</div>
						</td>
						<td align="right" width="150px">

							<div align="right">
							<!-- 参数说明 1.类型 2隐藏域中的id的集合 3隐藏域中选中的名称 4标示不同选择 例如，有两个责任部门需要知道选中的是哪个-->
								<span
									  onclick='orgPosPerMultipleSelectHasLimit("person","personName_access_id_detail","personId_access_id_detail","select_person_id_detail",20)'><img
										style="vertical-align: right;cursor: pointer;" 
										src="${basePath}images/common/select.gif" /> </span>
								<span onclick='closeAllOrganization("personName_access_id_detail","personId_access_id_detail","select_person_id_detail")'><img
										style="vertical-align: right; margin-right: 4px;cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>
					</tr>
                    <!--部门选择-->
					
						<tr style="margin-top: 4px">
						<td align="left" width="100px">
							<s:text name="orgNameC"></s:text>
						</td>
						<td align="center" style="margin-right: 2px">
							<div id="select_organization_id_detail" style="width: 100%">
							&nbsp;
							</div>
						</td>
						<td align="right" width="150px">

							<div align="right">
								<span
									onclick='orgPosPerMultipleSelectHasLimit("organization","orgName_access_id_detail","orgId_access_id_detail","select_organization_id_detail",20)'><img
										style="vertical-align: right;cursor: pointer;" 
										src="${basePath}images/common/select.gif" /> </span>
								<span onclick='closeAllOrganization("orgName_access_id_detail","orgId_access_id_detail","select_organization_id_detail")'><img
										style="vertical-align: right; margin-right: 4px;cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>

					</tr>
				</table>
				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="proposeDetailSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="proposeDetailReset()" />
				</div>
				</div>
			</div>
			
			 <!-- 统计页面 数据显示 -->
            <div id="proposeDetail_id" width="100%" style="padding-left:12px;padding-right:12px;">
           
            </div>
		    <!-- 下载  -->
			<div  style="padding-top: 10px;" align="center">
					    <a onclick="proposeDetailDownloadExcel()"
						style="cursor: pointer; color: #005EA7;" class="table"><s:text
							name="downLoadExcel" /> </a>
		    </div>
		</div>
		
		<div>
			<form action="" id="proposeDetailForm" method="post">
				<!-- 人员ID -->
				<input name="personIds" type="hidden" id="personId_access_id_detail"
					value="-1" />
				<input name="personNames" type="hidden" id="personName_access_id_detail"
					value="" />
				<!-- 部门ID -->
				<input name="orgIds" type="hidden" id="orgId_access_id_detail"
					value="-1" />
				<input name="orgNames" type="hidden" id="orgName_access_id_detail"
					value="" />
			</form>
		</div>
	</body>
</html>