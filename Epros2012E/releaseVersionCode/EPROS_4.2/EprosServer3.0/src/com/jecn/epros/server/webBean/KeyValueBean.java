package com.jecn.epros.server.webBean;

public class KeyValueBean {
	/**主键*/
	private Long id;
	/**名称*/
	private String name;
	/**编号*/
	private String number;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
