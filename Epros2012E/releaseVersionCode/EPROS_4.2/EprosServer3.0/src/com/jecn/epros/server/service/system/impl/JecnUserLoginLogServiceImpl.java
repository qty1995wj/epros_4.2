package com.jecn.epros.server.service.system.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.dao.system.IJecnUserLoginLogDao;
import com.jecn.epros.server.service.system.IJecnUserLoginLogService;

/***
 * 登录日志(JECN_USER_LOGIN_LOG)操作类
 * @author 201305
 *
 */
public class JecnUserLoginLogServiceImpl implements IJecnUserLoginLogService {
	
	private final Log log = LogFactory.getLog(JecnUserLoginLogServiceImpl.class);

	/** Dao层对象 */
	private IJecnUserLoginLogDao jecnUserLoginLogDao = null;

	public IJecnUserLoginLogDao getJecnUserLoginLogDao() {
		return jecnUserLoginLogDao;
	}

	public void setJecnUserLoginLogDao(IJecnUserLoginLogDao jecnUserLoginLogDao) {
		this.jecnUserLoginLogDao = jecnUserLoginLogDao;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public int addUserLoginLog(Long userId, String sessionId,int loginType) {
		return jecnUserLoginLogDao.addUserLoginLog(userId, sessionId, loginType);
		
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public void updateUserLoginLog(String sessionId,int loginOutType,int loginType) {
		jecnUserLoginLogDao.updateUserLoginLog(sessionId, loginOutType, loginType);
		
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<String> getUserLoginLogSessionID(int loginType) {
		return jecnUserLoginLogDao.getUserLoginLogSessionID(loginType);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public int updateLogiOutTime() {
		return jecnUserLoginLogDao.updateLogiOutTime();
	}
}
