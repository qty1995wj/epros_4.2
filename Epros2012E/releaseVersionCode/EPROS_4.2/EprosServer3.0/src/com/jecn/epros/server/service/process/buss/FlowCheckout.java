package com.jecn.epros.server.service.process.buss;

import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.jecn.epros.bean.checkout.CheckoutItem;
import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.server.util.JecnUtil;

public class FlowCheckout extends BaseCheckout {
	protected List<CheckoutResult> checkouts;

	public FlowCheckout(List<CheckoutResult> checkouts) {
		this.checkouts = checkouts;
	}

	protected HSSFWorkbook createWorkBook() {
		// 声明一个工作薄
		HSSFWorkbook workbook = new HSSFWorkbook();
		try {
			// 生成一个表格
			HSSFSheet sheet = workbook.createSheet("data");
			HSSFCellStyle titleStyle = SheetCellStyle.getInventoryTitleCellStyle(workbook);
			HSSFCellStyle contentStyle = SheetCellStyle.getInventoryContentCellStyle(workbook);
			titleStyle.setWrapText(true);
			contentStyle.setWrapText(true);
			createTitle(sheet, titleStyle);
			createContent(sheet, contentStyle, checkouts);
		} catch (Exception e) {
			log.error("", e);
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {

			}
		}
		return workbook;
	}

	private void createContent(HSSFSheet sheet, HSSFCellStyle style, List<CheckoutResult> checkouts) {
		int[] index = new int[1];
		index[0] = 1;
		String flowName;
		for (CheckoutResult result : checkouts) {
			flowName = result.getName() + "(当前流程)";
			createInoutRow(sheet, style, index, flowName, result.getIns(), "主流程输入");
			createInoutRowRight(sheet, style, index, flowName, result.getOuts(), "主流程输出");
			if (JecnUtil.isNotEmpty(result.getSons())) {
				for (CheckoutResult r : result.getSons()) {
					String sonFLowName = r.getName() + "(子流程)";
					createInoutRow(sheet, style, index, sonFLowName, r.getIns(), "子流程输入");
					createInoutRowSonRight(sheet, style, index, sonFLowName, r.getOuts(), "子流程输出");
				}
			}
		}
	}

	private void createInoutRowSonRight(HSSFSheet sheet, HSSFCellStyle style, int[] index, String flowName,
			List<CheckoutItem> inouts, String inoutName) {
		if (JecnUtil.isEmpty(inouts)) {
			return;
		}
		for (CheckoutItem item : inouts) {
			createRow(style, sheet, index[0]++, flowName, item.getName(), inoutName, "", JecnUtil.concat(item
					.getCellA(), "\n"), "", "");
		}
	}

	private void createInoutRow(HSSFSheet sheet, HSSFCellStyle style, int[] index, String flowName,
			List<CheckoutItem> inouts, String inoutName) {
		if (JecnUtil.isEmpty(inouts)) {
			return;
		}
		for (CheckoutItem item : inouts) {
			createRow(style, sheet, index[0]++, flowName, item.getName(), inoutName, JecnUtil.concat(item.getCellA(),
					"\n"), JecnUtil.concat(item.getCellB(), "\n"), "", "");
		}
	}

	private void createInoutRowRight(HSSFSheet sheet, HSSFCellStyle style, int[] index, String flowName,
			List<CheckoutItem> inouts, String inoutName) {
		if (JecnUtil.isEmpty(inouts)) {
			return;
		}
		for (CheckoutItem item : inouts) {
			createRow(style, sheet, index[0]++, flowName, item.getName(), inoutName, "", "", JecnUtil.concat(item
					.getCellA(), "\n"), JecnUtil.concat(item.getCellB(), "\n"));
		}
	}

	private int createTitle(HSSFSheet sheet, HSSFCellStyle style) {
		String[] titles = new String[] { "流程名称", "输入输出名称", "类型", "是否有来源", "是否在本流程被使用", "是否在下游流程被使用", "是否在本流程产生" };
		createRow(style, sheet, 0, titles);
		sheet.setColumnWidth(0, 2 * 22 * 256);
		sheet.setColumnWidth(1, 2 * 22 * 256);
		sheet.setColumnWidth(2, 1 * 22 * 256);
		sheet.setColumnWidth(3, 2 * 22 * 256);
		sheet.setColumnWidth(4, 2 * 22 * 256);
		sheet.setColumnWidth(5, 2 * 22 * 256);
		sheet.setColumnWidth(6, 2 * 22 * 256);
		return 0;
	}

	private void createRow(HSSFCellStyle style, HSSFSheet sheet, int index, String... names) {
		HSSFRow hssfRow = sheet.createRow(index);
		int col = 0;
		for (String title : names) {
			HSSFCell cell = hssfRow.createCell(col);
			cell.setCellStyle(style);
			cell.setCellValue(new HSSFRichTextString(title));
			col++;
		}
	}
}
