package com.jecn.epros.server.bean.system;

/**
 * 必须要抛出的异常
 * 
 * @author hyl
 * 
 */
public class NeedThrowException extends RuntimeException {
	public NeedThrowException() {
		super();
	}

	public NeedThrowException(String message) {
		super(message);
	}

	public NeedThrowException(String message, Throwable cause) {
		super(message, cause);
	}
}
