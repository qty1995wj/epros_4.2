package epros.designer.gui.process.flow.file;

import java.util.List;
import java.util.Vector;

import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.rule.edit.RelatedFilePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 流程(文件) 相关制度
 * 
 * @author ZXH
 * @date 2017-4-20下午04:40:46
 */
public class FlowRelatedRulePanel extends RelatedFilePanel {
	

	public FlowRelatedRulePanel(List<JecnTreeBean> treeBeanRelateRuleList) {
		super(treeBeanRelateRuleList, RelatedNodeType.rule);
	}
	
	public FlowRelatedRulePanel(List<JecnTreeBean> treeBeanRelateRuleList,Long ruleId) {
		super(treeBeanRelateRuleList, RelatedNodeType.rule,ruleId);
	}

	protected Vector<Object> initTableRowData(JecnTreeBean treeBean) {
		Vector<Object> row = new Vector<Object>();
		row = new Vector<Object>();
		row.add(treeBean.getId());
		// 文件名称
		row.add(treeBean.getName());
		row.add(this.newEditJLabel());
		row.add(treeBean.getTreeNodeType());
		return row;
	}


	/**
	 * 获取表头
	 * 
	 * @return
	 */
	@Override
	protected Vector<Object> getTitle() {
		Vector<Object> title = new Vector<Object>();
		title.add("id");
		// 名称
		title.add(JecnProperties.getValue("name"));
		// 操作
		title.add(JecnProperties.getValue("operational"));
		// 类型
		title.add(JecnProperties.getValue("type"));
		return title;
	}

	/**
	 * 隐藏table列
	 * 
	 */
	@Override
	protected void hiddenColumns() {
		// 渲染表格
		relatedFileTable.getColumnModel().getColumn(2).setCellRenderer(this.newEditJLabel());
		TableColumnModel columnModel = relatedFileTable.getColumnModel();
		columnModel.getColumn(2).setMaxWidth(100);
		columnModel.getColumn(2).setMinWidth(100);
		TableColumn tableColumn = columnModel.getColumn(0);
		// 默认隐藏
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		columnModel.getColumn(3).setMinWidth(0);
		columnModel.getColumn(3).setMaxWidth(0);
		// 行高 20
		relatedFileTable.setRowHeight(20);
	}

	/**
	 * 
	 * 详情双击
	 * 
	 * @return
	 */
	// @Override
	public void isClickTableEdit() {
		if (relatedFileTable.getSelectedColumn() == 2) {
			int row = relatedFileTable.getSelectedRow();
			if (row != -1) {
				TreeNodeType relateType = TreeNodeType.valueOf(relatedFileTable.getValueAt(row, 3).toString());
				Long ruleId = Long.valueOf(relatedFileTable.getValueAt(row, 0).toString());
				JecnDesignerCommon.openRuleInfoDialog(ruleId, relateType);
			}
		}
	}

}
