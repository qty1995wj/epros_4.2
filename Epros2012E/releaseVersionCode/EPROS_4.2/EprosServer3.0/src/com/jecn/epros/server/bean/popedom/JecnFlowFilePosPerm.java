package com.jecn.epros.server.bean.popedom;

import java.io.Serializable;

public class JecnFlowFilePosPerm implements Serializable {

	/** 主键 **/
	private Long id;
	/** 岗位id **/
	private Long figureId;
	/** 关联id **/
	private Long relateId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public Long getRelateId() {
		return relateId;
	}

	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}

}
