package com.jecn.epros.domain;

import java.util.List;

/**
 * Created by user on 2017/3/20.
 */
public class MyStars {

    private int total;
    private List<MyStar> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<MyStar> getData() {
        return data;
    }

    public void setData(List<MyStar> data) {
        this.data = data;
    }
}
