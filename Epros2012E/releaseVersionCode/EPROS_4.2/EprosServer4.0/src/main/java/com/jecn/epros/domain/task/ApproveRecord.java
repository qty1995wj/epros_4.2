package com.jecn.epros.domain.task;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ApproveRecord {

    /**
     * 阶段名称
     **/
    private String stateName;
    /**
     * 操作名称
     **/
    private String taskElseStateName;
    /**
     * 评审意见
     */
    private String option;
    /**
     * 变更说明
     */
    private String taskFixedDesc;
    /**
     * 审批人
     **/
    private String handleName;
    /**
     * 操作时间
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date updateTime;
    /**
     * 是否操作0审批 1正在审批 2未审批
     **/
    private int isApprove;

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getTaskElseStateName() {
        return taskElseStateName;
    }

    public void setTaskElseStateName(String taskElseStateName) {
        this.taskElseStateName = taskElseStateName;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getTaskFixedDesc() {
        return taskFixedDesc;
    }

    public void setTaskFixedDesc(String taskFixedDesc) {
        this.taskFixedDesc = taskFixedDesc;
    }

    public String getHandleName() {
        return handleName;
    }

    public void setHandleName(String handleName) {
        this.handleName = handleName;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getIsApprove() {
        return isApprove;
    }

    public void setIsApprove(int isApprove) {
        this.isApprove = isApprove;
    }

}
