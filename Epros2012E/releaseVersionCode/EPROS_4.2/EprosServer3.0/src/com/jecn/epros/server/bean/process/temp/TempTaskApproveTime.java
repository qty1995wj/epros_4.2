package com.jecn.epros.server.bean.process.temp;

public class TempTaskApproveTime {

	/**任务id*/
	private Long taskId;
	/**任务名称*/
	private String taskName;
	/**拟稿人阶段*/
	private Integer peopleMake;
	/**整理意见阶段*/
	private Integer finishingViews;
	/**文控审核*/
	private Integer documentControl;
	/**部门审核*/
	private Integer department;
	/**评审人会审*/
	private Integer reviewPeople;
	/**批准人审核*/
	private Integer approval;
	/**各业务体系负责人*/
	private Integer eachBusiness;
	/**IT总监*/
	private Integer director;
	/**事业部经理*/
	private Integer departmentManager;
	/**总经理*/
	private Integer manager;
	/**任务类型*/
	private String taskType;
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Integer getFinishingViews() {
		return finishingViews;
	}
	public void setFinishingViews(Integer finishingViews) {
		this.finishingViews = finishingViews;
	}
	public Integer getDocumentControl() {
		return documentControl;
	}
	public void setDocumentControl(Integer documentControl) {
		this.documentControl = documentControl;
	}
	public Integer getDepartment() {
		return department;
	}
	public void setDepartment(Integer department) {
		this.department = department;
	}
	public Integer getReviewPeople() {
		return reviewPeople;
	}
	public void setReviewPeople(Integer reviewPeople) {
		this.reviewPeople = reviewPeople;
	}
	public Integer getApproval() {
		return approval;
	}
	public void setApproval(Integer approval) {
		this.approval = approval;
	}
	public Integer getEachBusiness() {
		return eachBusiness;
	}
	public void setEachBusiness(Integer eachBusiness) {
		this.eachBusiness = eachBusiness;
	}
	public Integer getDirector() {
		return director;
	}
	public void setDirector(Integer director) {
		this.director = director;
	}
	public Integer getDepartmentManager() {
		return departmentManager;
	}
	public void setDepartmentManager(Integer departmentManager) {
		this.departmentManager = departmentManager;
	}
	public Integer getManager() {
		return manager;
	}
	public void setManager(Integer manager) {
		this.manager = manager;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public final Integer getPeopleMake() {
		return peopleMake;
	}
	public final void setPeopleMake(Integer peopleMake) {
		this.peopleMake = peopleMake;
	}
	
	
	
	
	
	
	

}
