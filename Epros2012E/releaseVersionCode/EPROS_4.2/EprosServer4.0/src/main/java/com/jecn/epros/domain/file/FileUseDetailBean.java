package com.jecn.epros.domain.file;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.TreeNodeUtils;

public class FileUseDetailBean {
    private Long id;
    private String name;
    /**
     * 0是流程架构、1是流程、2是制度、3标准、4是组织 、5 岗位 6风险
     */
    private int type;

    public String getTypeStr() {
        ResourceType type = getResourceTypeByType();
        if (type != null) {
            return AuthenticatedUserUtil.isLanguageType() == 0 ? type.getName() : type.getEnName();
        }
        return "";
    }

    private ResourceType getResourceTypeByType() {
        switch (type) {// 0是流程架构、1是流程、2是制度、3标准、4是组织 、5 岗位 6 风险
            case 0:
                return ResourceType.PROCESS_MAP;
            case 1:
                return ResourceType.PROCESS;
            case 2:
                return ResourceType.RULE;
            case 3:
                return ResourceType.STANDARD;
            case 4:
                return ResourceType.ORGANIZATION;
            case 5:
                return ResourceType.POSITION;
            case 6:
                return ResourceType.RISK;
            default:
                return null;
        }
    }

    public LinkResource getLink() {
        return new LinkResource(id, name, getResourceTypeByType(), TreeNodeUtils.NodeType.file);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
