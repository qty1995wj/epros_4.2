package com.jecn.authentication;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jecn.epros.bean.authetication.AuthenticationRequest;
import com.jecn.epros.bean.authetication.TokenAnalysis;
import com.jecn.epros.bean.authetication.TokenAnalysis.TokenStatus;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.common.JecnConfigTool;

final class AuthenticationTokenUtils {

	private AuthenticationTokenUtils() {
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationTokenUtils.class);

	private static final String CLAIM_KEY_USERNAME = "sub";
	private static final String CLAIM_KEY_CREATED = "created";
	private static final String secret = "EprosSecret"; // 秘钥
//	private int expiration = 30; // 单位：分钟
//
//	public int getExpiration() {
//		return Integer.parseInt(ConfigItemPartMapMark.cookieTime.toString());
//	}


	public static TokenAnalysis analyzeToken(String token) {
		TokenAnalysis analysis = new TokenAnalysis();
		try {
			if (token == null) {
				analysis.setStatus(TokenStatus.NULL);
			}
			// TODO 通过其他方式记录和实现 Token 失效
//			if (InvalidTokenSet.contains(token)) {
//				analysis.setStatus(TokenStatus.INVALID);
//			}
			TokenAnalysis ta = null;
			String newToken = AuthenticationTokenUtils.updateToken(token);
			
			if(!token.equals(newToken)){
//				System.out.println(newToken+"----"+token);
				//如果token快过期了，需要重置
				analysis.setToken(newToken);
			}else{
				analysis.setToken(token);
			}
			String username = TokenParser.getUsernameFromToken(token);
			analysis.setStatus(TokenStatus.VALID);
			analysis.setUsername(username);
			
		} catch (Exception e) {
			LOGGER.error("Token 非法 或超时");
			analysis.setStatus(TokenStatus.ILLEGAL);
		}
		return analysis;
	}

	public static String generateToken(AuthenticationRequest request) {
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put(CLAIM_KEY_USERNAME, request.getUsername());
		claims.put(CLAIM_KEY_CREATED, new Date());
		return generateToken(claims);
	}

	private static String generateToken(Map<String, Object> claims) {
		return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate()).signWith(SignatureAlgorithm.HS512, secret)
				.compact();
	}

	private static Date generateExpirationDate() {
		return new Date(System.currentTimeMillis() + (Integer.parseInt(JecnConfigTool.getItemValue(ConfigItemPartMapMark.cookieTime.toString()))) * 60 * 1000);
	}


	private static Boolean isTokenExpired(String token) {
		final Date expiration = TokenParser.getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}
	
	private static String updateToken(String token){
		String newToken = "";
		final Date expiration = TokenParser.getExpirationDateFromToken(token);
		long minutes = (expiration.getTime() - new Date().getTime())/1000;  //计算相差多少秒
		if(minutes<60&&minutes>0) {
			Map<String, Object> claims = new HashMap<String, Object>();
			claims.put(CLAIM_KEY_USERNAME, TokenParser.getUsernameFromToken(token));
			claims.put(CLAIM_KEY_CREATED, new Date());
			newToken = generateToken(claims);
		}else{
			newToken = token;
		}
		
		return newToken;
	}

	/**
	 * 解析 Token
	 * 
	 */
	private static class TokenParser {

		static String getUsernameFromToken(String token) {
			final Claims claims = getClaimsFromToken(token);
			String username = claims.getSubject();
			return username;
		}

		static Date getExpirationDateFromToken(String token) {
			final Claims claims = getClaimsFromToken(token);
			Date expiration = claims.getExpiration();
			return expiration;
		}

		static Claims getClaimsFromToken(String token) {
			Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
			return claims;
		}
	}
	
	
}