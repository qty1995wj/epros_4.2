package epros.designer.gui.process;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.process.mode.choose.FlowModeChooseDialog;
import epros.designer.gui.system.CommonJScrollPaneTextArea;
import epros.designer.gui.system.CommonJTextField;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.data.figure.JecnImplData;
import epros.draw.gui.designer.JecnDesignerFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程接口描述
 * 
 * @author ZXH 2017-02-06
 */
public class FlowSetFlowInterfaceDialogNew extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/** * 类型Lab */
	private JLabel typeLab = new JLabel(JecnProperties.getValue("typeC"));

	private static boolean isNotIflyketLogin = !JecnConfigTool.isIflytekLogin();

	/** 类型ComboBox */
	private JComboBox typeCombox = new JComboBox(isNotIflyketLogin ? new String[] { JecnProperties.getValue("topFlow"),
			JecnProperties.getValue("bottomFlow"), JecnProperties.getValue("interfaceFlow"),
			JecnProperties.getValue("sonFlow") } : new String[] { JecnProperties.getValue("topFlow"),
			JecnProperties.getValue("bottomFlow"), JecnProperties.getValue("interfaceFlow") });

	/** * 关联流程Lab */
	private JLabel relatedFlowLab = new JLabel(JecnProperties.getValue("relatedFlowC"));

	/** * 关联流程Filed */
	private JTextField relatedFlowField = new JTextField();

	/** * 关联流程按钮 */
	private JButton relatedFlowBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 接口名称 */
	private CommonJTextField implNameField = null;

	/** 流程描述 */
	private CommonJScrollPaneTextArea processRequireMentsScroll = null;

	/** 备注 */
	private CommonJScrollPaneTextArea implNoteScroll = null;

	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** * 清空 */
	// private JButton clearBut = new
	// JButton(JecnProperties.getValue("emptyBtn"));
	/** 设置连接关联的流程ID */
	private Long relationId = null;
	private JecnBaseFigurePanel elementPanel;
	private MapType mapType;
	/** 流程编号 **/
	private String numberId = "";

	private JLabel verfyLab = null;

	public FlowSetFlowInterfaceDialogNew(JecnBaseFigurePanel elementPanel, MapType mapType) {

		this.elementPanel = elementPanel;
		// 设置连接关联的流程ID
		relationId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
		// 已存在设置连接
		relatedFlowField.setText(elementPanel.getFlowElementData().getFigureText());

		String lineType = elementPanel.getFlowElementData().getDesignerFigureData().getLineType();
		if (!DrawCommon.isNullOrEmtry(lineType)) {
			typeCombox.setSelectedIndex(Integer.valueOf(lineType) - 1);
		} else {
			typeCombox.setSelectedIndex(0);
		}
		this.mapType = mapType;
		this.setSize(500, 400);
		if (JecnConfigTool.isIflytekLogin()) {
			this.setSize(500, 200);
		}
		this.setTitle(JecnProperties.getValue("setFlowInterface"));
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		relatedFlowField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		relatedFlowField.setEditable(false);
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 确定

		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		relatedFlowBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				relatedFlowButPerformed();
			}
		});

		// 窗口点X关闭箭头
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

		verfyLab = new JLabel();
		verfyLab.setForeground(Color.RED);

		initLayout();
		initData();
	}

	/**
	 * 初始化数据
	 */
	private void initData() {
		if (!DrawCommon.isImplFigure(elementPanel.getFlowElementData().getMapElemType())) {
			return;
		}
		JecnImplData implData = (JecnImplData) elementPanel.getFlowElementData();
		if (isNotIflyketLogin) {
			implNameField.getTextField().setText(getImplName());
			processRequireMentsScroll.getTextArea().setText(implData.getProcessRequirements());
			implNoteScroll.getTextArea().setText(implData.getImplNote());
		}
	}

	private String getImplName() {
		JecnImplData implData = (JecnImplData) elementPanel.getFlowElementData();
		// 1是上游流程,2是下游流程,3是过程流程,4是子流程
		// String lineType =
		// elementPanel.getFlowElementData().getDesignerFigureData().getLineType();
		List<JecnFigureRefManhattanLine> figureRefManhattanLines = elementPanel.getListFigureRefManhattanLine();
		if (figureRefManhattanLines.size() == 0 || !DrawCommon.isNullOrEmtryTrim(implData.getImplName())) {
			return implData.getImplName();
		}
		// if ("1".equals(lineType)) {// 上游流程，获取结束图形
		for (JecnFigureRefManhattanLine jecnFigureRefManhattanLine : figureRefManhattanLines) {
			if (elementPanel == jecnFigureRefManhattanLine.getManhattanLinePanel().getStartFigure()) {
				JecnBaseFigurePanel endFigure = jecnFigureRefManhattanLine.getManhattanLinePanel().getEndFigure();
				if (endFigure.getFlowElementData().getMapElemType() == MapElemType.EventFigure
						&& !DrawCommon.isNullOrEmtryTrim(endFigure.getFlowElementData().getFigureText())) {// 事件
					return endFigure.getFlowElementData().getFigureText();
				}
			}
		}
		// } else if ("2".equals(lineType)) {// 下游流程，回去开始图形
		for (JecnFigureRefManhattanLine jecnFigureRefManhattanLine : figureRefManhattanLines) {
			if (elementPanel == jecnFigureRefManhattanLine.getManhattanLinePanel().getEndFigure()) {
				JecnBaseFigurePanel startFigure = jecnFigureRefManhattanLine.getManhattanLinePanel().getStartFigure();
				if (startFigure.getFlowElementData().getMapElemType() == MapElemType.EventFigure
						&& !DrawCommon.isNullOrEmtryTrim(startFigure.getFlowElementData().getFigureText())) {// 事件
					return startFigure.getFlowElementData().getFigureText();
				}
			}
		}
		// }
		return implData.getImplName();
	}

	protected boolean isUpdate() {
		JecnImplData implData = (JecnImplData) elementPanel.getFlowElementData();
		JecnDesignerFigureData designerFigureData = elementPanel.getFlowElementData().getDesignerFigureData();

		if (designerFigureData.getLineType() != null
				&& !DrawCommon
						.checkStringSame(designerFigureData.getLineType(), typeCombox.getSelectedIndex() + 1 + "")
				|| (!DrawCommon.checkLongSame(designerFigureData.getLinkId(), relationId))) {
			return true;
		}
		if (isNotIflyketLogin) {// 科大讯飞 不验证 名称 描述 备注
			if (!DrawCommon.checkStringSame(implNameField.getTextField().getText(), implData.getImplName())
					|| !DrawCommon.checkStringSame(processRequireMentsScroll.getTextArea().getText(), implData
							.getProcessRequirements())
					|| !DrawCommon.checkStringSame(implNoteScroll.getTextArea().getText(), implData.getImplNote())) {
				return true;
			}
		}

		return false;
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);

		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		// 按钮
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);

		int rows = 0;
		// 类型
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(typeLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(typeCombox, c);
		rows++;
		// 关联流程
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(relatedFlowLab, c);
		c = new GridBagConstraints(1, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(relatedFlowField, c);
		c = new GridBagConstraints(2, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(relatedFlowBut, c);
		rows++;
		if (isNotIflyketLogin) {
			// 接口名称
			implNameField = new CommonJTextField(rows, infoPanel, insets, JecnProperties.getValue("implNameC"));
			rows++;
			// 流程描述
			processRequireMentsScroll = new CommonJScrollPaneTextArea(rows, infoPanel, insets, JecnProperties
					.getValue("processRequireMentsC"));
			rows++;
			implNoteScroll = new CommonJScrollPaneTextArea(rows, infoPanel, insets, JecnProperties.getValue("remarkC"));
			rows++;
		}
		c = new GridBagConstraints(0, rows, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		// infoPanel.add(new JLabel(), c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			// 关闭窗体
			this.dispose();
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				// 关闭窗体
				this.dispose();
			}
		}
	}

	/***************************************************************************
	 * 确定
	 * 
	 * @param args
	 */
	private void okButPerformed() {
		if (!DrawCommon.isImplFigure(elementPanel.getFlowElementData().getMapElemType()) || validateStringNotPass()) {
			return;
		}

		if (!isUpdate()) {
			this.dispose();
			return;
		}
		int index = typeCombox.getSelectedIndex();
		// 元素增加链接ID
		if (relationId != null) {
			elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(relationId);
		}
		if (!DrawCommon.isNullOrEmtryTrim(relatedFlowField.getText())) {
			elementPanel.getFlowElementData().setFigureText(
					JecnTreeCommon.getShowName(relatedFlowField.getText(), numberId));
			elementPanel.updateUI();
		}
		switch (index) {
		case 0:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("1");
			break;
		case 1:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("2");
			break;
		case 2:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("3");
			break;
		case 3:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("4");
			break;
		default:
			break;
		}

		JecnImplData implData = (JecnImplData) elementPanel.getFlowElementData();
		if (isNotIflyketLogin) {
			implData.setImplName(implNameField.getTextField().getText());
			implData.setProcessRequirements(processRequireMentsScroll.getTextArea().getText());
			implData.setImplNote(implNoteScroll.getTextArea().getText());
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		if (relationId != null) {
			JecnDesignerCommon.addElemLink(elementPanel, mapType);
		}

		this.dispose();
	}

	/**
	 * 输入内容长度验证
	 * 
	 * @return
	 */
	private boolean validateStringNotPass() {
		if (isNotIflyketLogin) {
			if (JecnValidateCommon.validateFieldNotPass(implNameField.getTextField().getText(), verfyLab,
					JecnProperties.getValue("implName"))) {
				return true;
			} else if (JecnValidateCommon.validateAreaNotPass(processRequireMentsScroll.getTextArea().getText(),
					verfyLab, JecnProperties.getValue("processRequireMents"))) {
				return true;
			} else if (JecnValidateCommon.validateAreaNotPass(implNoteScroll.getTextArea().getText(), verfyLab,
					JecnProperties.getValue("remark"))) {
				return true;
			}
		}
		return false;
	}

	/***************************************************************************
	 * 关联流程
	 * 
	 * @param args
	 */
	private void relatedFlowButPerformed() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		JecnSelectChooseDialog d = null;
		if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getDesignerData().getModeType() != ModeType.none) {
			d = new FlowModeChooseDialog(list, 11);
		} else {
			if (relationId != null) {
				// 设置连接默认显示已存在的值
				JecnTreeBean bean = new JecnTreeBean();
				bean.setId(relationId);
				bean.setName(elementPanel.getFlowElementData().getFigureText());
				list.add(bean);
			}
			d = new ProcessChooseDialog(list);
			// 设置只能选择流程13
			d.setChooseType(13);

		}
		d.removeEmptyBtn();
		d.setSelectMutil(false);
		d.setVisible(true);
		if (d.isOperation()) {
			if (list.size() == 1) {
				JecnTreeBean treeBean = list.get(0);
				relationId = treeBean.getId();
				numberId = treeBean.getNumberId();
				relatedFlowField.setText(treeBean.getName());
			}
		}
	}
}
