package com.jecn.epros.server.service.process.buss;

import java.awt.Color;
import java.awt.Font;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.temp.TempSearchKpiBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.dao.process.IProcessKPIValueDao;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 流程KPI JFreeChart 创建
 * 
 * @author Administrator
 * @date： 日期：Jan 18, 2013 时间：4:13:42 PM
 */
public class FlowJFreeChart {
	private static final Logger log = Logger.getLogger(FlowJFreeChart.class);
	private TempSearchKpiBean searchKpiBean;
	/** KPI JFreeChart 显示数据集 */
	private CategoryDataset dataset = null;
	/***/
	private IProcessKPIValueDao kPIValueDao;
	/** 曲线图 */
	private String KPI_TITLE_VALUE = JecnUtil.getValue("Graph");
	/** KPI数值 */
	private String KPI_VALUE = JecnUtil.getValue("KPI_NUM");
	/** 周类型转换成时间拼装 year + WEEK_TO_TIME +ｗｅｅｋ */
	private String WEEK_TO_TIME = "-01-01 01:";

	public FlowJFreeChart(TempSearchKpiBean searchKpiBean,
			IProcessKPIValueDao kPIValueDao) {
		this.kPIValueDao = kPIValueDao;
		this.searchKpiBean = searchKpiBean;
	}

	/**
	 * 创建流程KPI曲线图
	 * 
	 * @return
	 * @throws Exception
	 */
	public String createFreeChart(JecnFlowKpiName jecnFlowKpiName)
			throws Exception {
		// 获取数据集
		dataset = getDataSet(searchKpiBean, jecnFlowKpiName);
		JFreeChart chart = ChartFactory.createLineChart(searchKpiBean
				.getKpiName()
				+ KPI_TITLE_VALUE, // 图表标题"曲线图"
				KPIConstants.KPI_VALUE + "{"
						+ jecnFlowKpiName.getKpiHorizontal() + "}", // 目录轴的显示标签"KPI数值"
				KPI_VALUE + "{" + jecnFlowKpiName.getKpiVertical() + "}", // 数值轴的显示标签"KPI数值"
				dataset, // 数据集
				PlotOrientation.VERTICAL, // 图表方向:水平、垂直
				true, // 是否显示图例(对于简单的柱状图必须是false)
				false, // 是否生成工具
				false // 是否生成URL链接
				);

		/*----------设置消除字体的锯齿渲染(解决中文问题)--------------*/
		chart.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);

		/*------------配置图表属性--------------*/
		// 1,设置整个图表背景颜色
		chart.setBackgroundPaint(Color.pink);

		/*------------设定Plot参数-------------*/
		CategoryPlot plot = chart.getCategoryPlot();
		// 2,设置详细图表的显示细节部分的背景颜色
		// plot.setBackgroundPaint(Color.PINK);
		// 3,设置垂直网格线颜色
		plot.setDomainGridlinePaint(Color.black);
		// 4,设置是否显示垂直网格线
		plot.setDomainGridlinesVisible(true);
		// 5,设置水平网格线颜色
		plot.setRangeGridlinePaint(Color.blue);
		// 6,设置是否显示水平网格线
		plot.setRangeGridlinesVisible(true);

		LineAndShapeRenderer xyitem = new LineAndShapeRenderer();
		// 显示折点数据
		xyitem.setBaseItemLabelsVisible(true);
		xyitem.setBaseShapesVisible(true);
		// 折点数据显示位置
		xyitem.setBasePositiveItemLabelPosition(new ItemLabelPosition(
				ItemLabelAnchor.OUTSIDE1, TextAnchor.BASELINE_LEFT));
		xyitem
				.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		// 折点数据字体
		xyitem.setBaseItemLabelFont(new Font("Dialog", 1, 10));
		xyitem.setBaseItemLabelPaint(Color.black);
		xyitem.setSeriesPaint(0, Color.red);
		// 数值距离点的距离
		xyitem.setItemLabelAnchorOffset(2);
		xyitem.setBaseFillPaint(Color.yellow);
		plot.setRenderer(0, xyitem);

		// 计算生成图片大小
		int w = 0, h = 0;
		int screen = (int) Toolkit.getDefaultToolkit().getScreenSize()
				.getHeight();
		if (screen == KPIConstants.SCREEN_H_MIN) {// 分辨率1024 *７６８
			h = KPIConstants.PIE_MIN_HEIGHT;
			w = KPIConstants.PIE_MIN_WIDTH;
		} else if (screen == KPIConstants.SCREEN_H_MAX) {// 分辨率1024 *７６８
			h = KPIConstants.PIE_MAX_HEIGHT;
			w = KPIConstants.PIE_MAX_WIDTH;
		} else {// 默认大小
			h = KPIConstants.PIE_MIN_HEIGHT;
			w = KPIConstants.PIE_MIN_WIDTH;
		}

		if (dataset.getColumnCount() != 0) {
			if (dataset.getColumnCount() > 14) {
				w = dataset.getColumnCount() * 40;
			} else if (dataset.getColumnCount() > 10) {
				w = dataset.getColumnCount() * 60;
			} else if (dataset.getColumnCount() > 6) {
				w = dataset.getColumnCount() * 70;
			}
		}
		String path = JecnPath.APP_PATH;
		// 获取存储临时文件的目录
		String tempFileDir = path + "images/tempImage/" + "FLOW_KPI_IMAGE/";
		// 创建临时文件目录
		File fileDir = new File(tempFileDir);
		fileDir.mkdirs();
		File[] delFile = fileDir.listFiles();
		for (int i = 0; i < delFile.length; i++) {// 目录下只能存在一个临时文件
			delFile[i].delete();
		}
		// 获取UUID随机数
		String timefile = UUID.randomUUID().toString();
		String fileName = tempFileDir + timefile + ".jpeg";
		FileOutputStream fos = new FileOutputStream(fileName);
		if (fos != null) {
			ChartUtilities.writeChartAsJPEG(fos, new Float(1), chart, w, h,
					null);
			fos.flush();
			fos.close();
		}
		return "images/tempImage/" + "FLOW_KPI_IMAGE/" + timefile + ".jpeg";
		// request.setAttribute("path", IMAGE_PATH + timefile
		// + ".jpeg");
		// filePath = IMAGE_PATH + timefile + ".jpeg";
	}

	/**
	 * 
	 * @param dayFalse
	 *            day:0 月;1; 季度2 ,week3: year4
	 * @param kpiName
	 * @param startTime
	 * @param endTime
	 * @param jecnFlowKpiName
	 * @return
	 * @throws Exception
	 */
	public CategoryDataset getDataSet(TempSearchKpiBean searchKpiBean,
			JecnFlowKpiName jecnFlowKpiName) throws Exception {

		String kpiHorType = searchKpiBean.getKpiHorType();
		String startTime = searchKpiBean.getStartTime();
		String endTime = searchKpiBean.getEndTime();

		Date startDate = null;
		Date endDate = null;

		if (!JecnCommon.isNullOrEmtryTrim(startTime)
				&& !JecnCommon.isNullOrEmtryTrim(endTime)) {
			if (kpiHorType.equals(FlowKPIHorType.season.toString())) {// 季度
				startTime = getStartDateBySeason(startTime);
				endTime = getStartDateBySeason(endTime);
			} else if (kpiHorType.equals(FlowKPIHorType.month.toString())) {// 月
				// yyyy-MM-dd
				startTime = startTime + "-01";
				endTime = endTime + "-01";
			} else if (kpiHorType.equals(FlowKPIHorType.week.toString())) {// 周
				String[] sWeekValue = startTime.split(",");
				int sYear = Integer.valueOf(sWeekValue[0].trim());
				int sWeek = Integer.valueOf(sWeekValue[1].trim());
				String[] eWeekValue = endTime.split(",");
				int eYear = Integer.valueOf(eWeekValue[0].trim());
				int eWeek = Integer.valueOf(eWeekValue[1].trim());

				// 日期转换成字符串时间格式 到时分秒 yyyy-MM-dd HH:MM:SS
				String startStrTime = String.valueOf(sYear) + WEEK_TO_TIME
						+ String.valueOf(sWeek) + ":01";
				String endStrTime = String.valueOf(eYear) + WEEK_TO_TIME
						+ String.valueOf(eWeek) + ":01";
				startDate = getDateTimeByString(startStrTime);
				endDate = getDateTimeByString(endStrTime);
			} else if (kpiHorType.equals(FlowKPIHorType.year.toString())) {// 年
				// yyyy-MM-dd
				startTime = startTime + "-01-01";
				endTime = endTime + "-01-01";
			}
		}

		// 日期转换成字符串时间格式 到时分秒 yyyy-MM-dd
		if (startDate == null && endDate == null
				&& !JecnCommon.isNullOrEmtryTrim(startTime)
				&& !JecnCommon.isNullOrEmtryTrim(endTime)) {
			startDate = JecnCommon.getDateByString(startTime);
			endDate = JecnCommon.getDateByString(endTime);
		}

		List<JecnFlowKpi> jecnFlowKpiList = null;
		if (startDate != null && endDate != null) {
			jecnFlowKpiList = kPIValueDao.findJecnFlowKpiList(jecnFlowKpiName
					.getKpiAndId(), startDate, endDate);
		} else {
			jecnFlowKpiList = kPIValueDao.getJecnFlowKpiListBy(jecnFlowKpiName
					.getKpiAndId());
		}

		DefaultCategoryDataset dcd = new DefaultCategoryDataset();

		for (int i = 0; i < jecnFlowKpiList.size(); i++) {

			// this.setKpiHorValue(getKpiHorByYear(jecnFlowKpiList) + "("
			// + jecnFlowKpiName.getKpiHorizontal() + ")");
			// this.setKpiVerValue(jecnFlowKpiName.getKpiVertical());

			String kpiHorValues = getKpiHorValeByDate(jecnFlowKpiList.get(i)
					.getKpiHorVlaue(), jecnFlowKpiName.getKpiHorizontal());

			dcd.addValue(Double.valueOf(jecnFlowKpiList.get(i).getKpiValue()),
					jecnFlowKpiName.getKpiName(), kpiHorValues);

		}
		return dcd;
	}

	/**
	 * 字符串转换时间类型
	 * 
	 * @param str
	 * @return
	 */
	public static Date getDateTimeByString(String str) {
		if (JecnCommon.isNullOrEmtryTrim(str)) {
			return null;
		}
		SimpleDateFormat dateformat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		try {
			return dateformat.parse(str);
		} catch (ParseException e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * 根据搜索季度值转换月份日期
	 * 
	 * @param startTime
	 * @return
	 */
	private String getStartDateBySeason(String startTime) {
		if (JecnCommon.isNullOrEmtryTrim(startTime)) {
			return null;
		}
		String[] str = startTime.split("-");
		if (str.length > 1 && str[1] != null) {
			String strMatch = str[1].toString();
			String strYear = str[0].toString();
			if (strMatch.endsWith("2")) {
				strMatch = "2";
			} else if (strMatch.endsWith("3")) {
				strMatch = "7";
			} else if (strMatch.endsWith("4")) {
				strMatch = "10";
			}
			return strYear + "-" + strMatch + "-01";
		}
		return null;
	}

	/**
	 * 
	 * KPI纵坐标，标签显示
	 * 
	 * @param jecnFlowKpiList
	 * @return
	 */
	public String getKpiHorByYear(List<JecnFlowKpi> jecnFlowKpiList) {
		if (jecnFlowKpiList.size() == 0 || jecnFlowKpiList == null) {
			return null;
		}
		String firstKpiHorValue = jecnFlowKpiList.get(0).getKpiHorVlaue()
				.toString();
		firstKpiHorValue = firstKpiHorValue.substring(0, 4);
		String lastKpiHorValue = null;
		if (jecnFlowKpiList.size() > 1) {
			lastKpiHorValue = jecnFlowKpiList.get(jecnFlowKpiList.size() - 1)
					.getKpiHorVlaue().toString();
			lastKpiHorValue = lastKpiHorValue.substring(0, 4);
		} else {
			lastKpiHorValue = firstKpiHorValue;
		}
		String kpiHorByYear = null;
		if (Integer.parseInt(firstKpiHorValue) == Integer
				.parseInt(lastKpiHorValue)) {
			kpiHorByYear = firstKpiHorValue;
		} else {
			kpiHorByYear = firstKpiHorValue + "-" + lastKpiHorValue;
		}
		return kpiHorByYear;
	}

	/**
	 * 流程KPi曲线图横线坐标（时间轴）值
	 * 
	 * @param kpiHorValue
	 *            横向坐标时间
	 * @param kpiHorType
	 * @return
	 */
	public String getKpiHorValeByDate(Date kpiHorValue, String kpiHorType) {
		String kpiValue = kpiHorValue.toString();
		String year = kpiValue.substring(0, 4);

		if (kpiHorType.equals(FlowKPIHorType.day) || "天".equals(kpiHorType)) {
			kpiValue = year + " " + kpiValue.substring(5, 10) + "D";
		}
		if (kpiHorType.equals(FlowKPIHorType.month) || "月".equals(kpiHorType)) {
			kpiValue = year + " " + kpiValue.substring(5, 8) + "M";
		}
		if (kpiHorType.equals(FlowKPIHorType.season) || "季度".equals(kpiHorType)) {
			kpiValue = kpiValue.substring(5, 7);
			kpiValue = getSeasonByDataSeason(Integer.parseInt(kpiValue));
			if (1 == Integer.parseInt(kpiValue)) {
				kpiValue = year + "year " + "1";
				return kpiValue;
			} else if (2 == Integer.parseInt(kpiValue)) {
				kpiValue = year + "year " + "2";
				return kpiValue;
			} else if (3 == Integer.parseInt(kpiValue)) {
				kpiValue = year + "year " + "3";
				return kpiValue;
			} else if (4 == Integer.parseInt(kpiValue)) {
				kpiValue = year + "year " + "4";
				return kpiValue;
			}
		} else if (kpiHorType.equals(FlowKPIHorType.week)
				|| "周".equals(kpiHorType)) {
			// 日期转换 yyyy-MM-dd HH:mm:sss
			Date weekDate = JecnCommon.getDateTimeByString(kpiValue);
			// 获取分钟， 分钟为当前日期的周数
			int week = weekDate.getMinutes();
			kpiValue = year + " " + week;
		} else if (kpiHorType.equals(FlowKPIHorType.year)
				|| "年".equals(kpiHorType)) {
			kpiValue = year;
		}
		return kpiValue;
	}

	/**
	 * 获取数据库数据 季度值转换
	 * 
	 * @param kpiValue
	 * @return String
	 */
	private String getSeasonByDataSeason(int kpiValue) {
		switch (kpiValue) {
		case 1:
			return "1";
		case 4:
			return "2";
		case 7:
			return "3";
		case 10:
			return "4";
		}
		return "0";
	}

	/**
	 * 流程KPI横向坐标值
	 * 
	 */
	public enum FlowKPIHorType {
		day, // 天
		week, // 周
		month, // 月
		season, // 季度
		year
		// 天
	}

	public IProcessKPIValueDao getKPIValueDao() {
		return kPIValueDao;
	}

	public void setKPIValueDao(IProcessKPIValueDao valueDao) {
		kPIValueDao = valueDao;
	}
}
