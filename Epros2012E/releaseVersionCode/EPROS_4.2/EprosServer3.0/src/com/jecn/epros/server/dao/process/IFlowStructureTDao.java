package com.jecn.epros.server.dao.process;


import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.IBaseDao;

public interface IFlowStructureTDao extends IBaseDao<JecnFlowStructureT, Long> {

	
}
