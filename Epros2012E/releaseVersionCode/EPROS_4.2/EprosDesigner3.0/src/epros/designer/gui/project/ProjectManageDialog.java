package epros.designer.gui.project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.bean.project.JecnProject;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.ResourceHighEfficiencyTree;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.service.JecnDesignerImpl;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnUIUtil;

/***
 * 项目管理
 * 
 * @author 2012-05-23
 * 
 */
public class ProjectManageDialog extends JecnDialog {

	public static final Logger log = Logger.getLogger(ProjectManageDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 项目信息显示面板 */
	private JPanel projecPanel = null;

	/** 按钮面板1 */
	private JPanel buttonPanel1 = null;

	/** 按钮面板2 */
	private JPanel buttonPanel2 = null;

	/** 项目滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 项目Table */
	public JecnTable projecTable = null;

	/** 打开项目 */
	private JButton openProBut = null;

	/** 新建项目 */
	private JButton addProBut = null;

	/** 重命名 */
	private JButton realNameBut = null;

	/** 设置主项目 */
	private JButton setMainProBut = null;

	/** 删除 */
	private JButton deleteBut = null;

	/** 取消 */
	private JButton cancelBut = null;

	/** 设置大小 */
	Dimension dimension = null;
	List<JecnProject> projectList = null;
	protected JLabel projectVerLab = new JLabel();

	public List<JecnProject> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<JecnProject> projectList) {
		this.projectList = projectList;
	}

	public ProjectManageDialog() {
		initCompotents();
		initLayout();
	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 项目信息显示面板
		projecPanel = new JPanel();

		// 按钮面板
		buttonPanel1 = new JPanel();

		// 按钮面板
		buttonPanel2 = new JPanel();

		// 滚动面板
		resultScrollPane = new JScrollPane();

		// 项目Table
		projecTable = new JecnProjectTable();

		// 打开项目
		openProBut = new JButton(JecnProperties.getValue("openProject"));

		// 新建项目
		addProBut = new JButton(JecnProperties.getValue("addBtn"));

		// 重命名
		realNameBut = new JButton(JecnProperties.getValue("rename"));

		// 设置主项目
		setMainProBut = new JButton(JecnProperties.getValue("setMainPro"));

		// 删除
		deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

		// 取消
		cancelBut = new JButton(JecnProperties.getValue("closeBtn"));

		// 设置大小
		dimension = new Dimension();

		// 设置窗体大小可改变
		this.setResizable(true);
		this.setSize(500, 430);
		this.setLocationRelativeTo(null);
		this.setTitle(JecnProperties.getValue("projectMange"));
		this.setModal(true);

		// 设置主面板默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		projecPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置button面板默认背景色
		buttonPanel1.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置button面板默认背景色
		buttonPanel2.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 滚动面板
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		resultScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());

		// dimension = new Dimension(475, 300);
		// projecPanel.setPreferredSize(dimension);
		// projecPanel.setMaximumSize(dimension);
		// projecPanel.setMinimumSize(dimension);
		//
		// // 设置滚动面板的大小
		// dimension = new Dimension(450, 275);
		// resultScrollPane.setPreferredSize(dimension);
		// resultScrollPane.setMaximumSize(dimension);
		// resultScrollPane.setMinimumSize(dimension);
		//
		// //提示信息
		// dimension = new Dimension(310, 20);
		// projectVerLab.setPreferredSize(dimension);
		// projectVerLab.setMaximumSize(dimension);
		// projectVerLab.setMinimumSize(dimension);
		projectVerLab.setForeground(Color.red);

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});
		// 新建项目
		addProBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}
		});
		// 删除
		deleteBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}
		});
		// 重命名
		realNameBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reNameButPerformed();
			}
		});
		// 设置主项目
		setMainProBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setMainProButPerformed();
			}
		});
		openProBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openProButAction();
			}
		});
		if (!JecnDesignerCommon.isAdmin()) {
			addProBut.setEnabled(false);
			deleteBut.setEnabled(false);
			realNameBut.setEnabled(false);
			setMainProBut.setEnabled(false);
		}
	}

	/***
	 * 布局
	 */
	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);

		// 项目信息面板布局
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(projecPanel, c);

		projecPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), ""));
		projecPanel.setLayout(new GridBagLayout());
		// 项目集合
		resultScrollPane.setViewportView(projecTable);
		// projecPanel.add(resultScrollPane);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		projecPanel.add(resultScrollPane, c);

		// 按钮面板1布局
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel1, c);
		buttonPanel1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel1.add(openProBut);
		buttonPanel1.add(addProBut);
		buttonPanel1.add(realNameBut);

		buttonPanel1.add(deleteBut);
		buttonPanel1.add(setMainProBut);
		// 按钮面板2布局
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel2, c);
		buttonPanel2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel2.add(projectVerLab);
		buttonPanel2.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 取消按钮事件
	 */
	private void cancelButPerformed() {
		this.dispose();
	}

	/***
	 * 新建项目
	 */
	private void addButPerformed() {
		AddProjectNameDialog addProjectNameDialog = new AddProjectNameDialog(
				this);
		addProjectNameDialog.setVisible(true);
		if (addProjectNameDialog.isOk()) {
			JecnProject jecnProject = addProjectNameDialog.getJecnProject();
			projectList.add(jecnProject);
			Vector<String> v = new Vector<String>();
			v.add(jecnProject.getProjectName());
			projecTable.addRow(v);
			int contRow = projecTable.getRowCount();
			projecTable.getModel().setValueAt(jecnProject.getProjectName(),
					contRow - 1, 1);
		}
	}

	/***
	 * 删除
	 */
	private void deleteButPerformed() {
		projectVerLab.setText("");
		int[] selectRows = projecTable.getSelectedRows();
		if (selectRows.length == 0) {
			projectVerLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 项目ID
		// Long projectId = Long.valueOf(projectList.get(selectRows[0])
		// .getProjectId());
		// 判断项目是否已经打开,打开则提示不能删除
		for (int i = selectRows.length - 1; i >= 0; i--) {
			if (JecnConstants.projectId != null) {
				if (projectList.get(selectRows[i]).getProjectId().longValue() == JecnConstants.projectId
						.longValue()) {
					projectVerLab.setText(JecnProperties
							.getValue("notdelOpenProj"));
					return;
				}
			}
		}
		int option = JecnOptionPane.showConfirmDialog(this,
				JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		// 保存要删除的id，传到后台删除数据
		List<Long> listIds = new ArrayList<Long>();
		for (int i = selectRows.length - 1; i >= 0; i--) {
			listIds.add(projectList.get(selectRows[i]).getProjectId());

		}
		// 更改项目删除状态
		if (!deleteData(listIds)) {
			// 提示删除失败
			projectVerLab.setText(JecnProperties.getValue("delfailure"));
			return;
		}
		// 删除table选中的数据
		// projecTable.removeRows(selectRows);

		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) projecTable.getModel())
					.removeRow(selectRows[i]);
		}
		// 删除项目结合projectList中的数据
		List<JecnProject> oldProjectList = projectList;
		projectList = new ArrayList<JecnProject>();
		for (JecnProject jecnProject : oldProjectList) {
			if (!listIds.contains(jecnProject.getProjectId())) {
				projectList.add(jecnProject);
			}
		}
	}

	private boolean deleteData(List<Long> listIds) {
		try {
			ConnectionPool.getProject().deleteProject(listIds);
			return true;
		} catch (Exception e) {
			log.error("ProjectManageDialog deleteData is error", e);
		}
		return false;
	}

	/***
	 * 重命名
	 */
	private void reNameButPerformed() {
		projectVerLab.setText("");
		int[] selectRows = projecTable.getSelectedRows();
		if (selectRows.length !=1) {
			//请选择一行！
			projectVerLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		EditProjectNameDialog editProjectNameDialog = new EditProjectNameDialog(
				projectList.get(selectRows[0]));
		editProjectNameDialog.setVisible(true);
		if (editProjectNameDialog.isOk()) {
			// 重新获得更新后的数据
			this.projecTable.getModel().setValueAt(
					editProjectNameDialog.getName(), selectRows[0], 1);
			if (JecnConstants.projectId!=null&&projecTable.getModel().getValueAt(selectRows[0], 0).toString()
					.equals(JecnConstants.projectId.toString())) {
				ResourceHighEfficiencyTree jTree = JecnDesignerMainPanel
						.getDesignerMainPanel().getTreePanel().getTree();
				JecnTreeNode proTreeNode = (JecnTreeNode) jTree.getModel()
						.getRoot();

				// 更新资源管理器树节点项目名称
				JecnTreeCommon.reNameNode(jTree, proTreeNode,
						editProjectNameDialog.getName());
			}

		}
	}

	/**
	 * 设置主项目
	 */
	private void setMainProButPerformed() {
		projectVerLab.setText("");
		int[] selectRows = projecTable.getSelectedRows();
		if (selectRows.length == 0 || selectRows.length > 1) {
			projectVerLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 取消主项目
		int rows = projecTable.getRowCount();
		for (int i = 0; i < rows; i++) {
			projecTable.setValueAt("", i, 2);
		}
		// 项目ID
		Long curProId = Long.valueOf(projectList.get(selectRows[0])
				.getProjectId());
		try {
			ConnectionPool.getCurProject().updateCurProject(curProId);
			// 重新设置新主项目列为“是”
			projecTable.setValueAt(JecnProperties.getValue("mainProject"),
					selectRows[0], 2);
			projectVerLab.setText(JecnProperties.getValue("successSetProject"));
		} catch (Exception e) {
			log.error("ProjectManageDialog setMainProButPerformed is error", e);
			projectVerLab.setText(JecnProperties.getValue("faliureSetProject"));
		}
	}

	/**
	 * @author yxw 2012-8-21
	 * @description:打开项目
	 */
	private void openProButAction() {
		// 有没有打开的面板，如果有提示返回
		int tabCount = JecnDrawMainPanel.getMainPanel().getTabbedPane()
				.getTabCount();
		if (tabCount>0) {
			JecnOptionPane.showMessageDialog(this,JecnProperties.getValue("closeAllOpenDesktopPane"));
			this.dispose();
			return;
		}

		int[] selectRows = projecTable.getSelectedRows();
		if (selectRows.length == 0 || selectRows.length > 1) {
			projectVerLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 项目ID
		Long projectId = Long.valueOf(projectList.get(selectRows[0])
				.getProjectId());
		// 判断项目是否已经打开
		if (JecnConstants.projectId != null) {
			if (projectId.longValue() == JecnConstants.projectId.longValue()) {
				projectVerLab.setText(JecnProperties.getValue("projecnOpened"));
				return;
			}
		}
		JecnConstants.projectId = projectId;

		// 更改资源管理树
		JecnDesignerMainPanel.getDesignerMainPanel().getTreePanel()
				.setTree(new ResourceHighEfficiencyTree());

		// 读取配置文件输入流
		InputStream in = null;
		FileInputStream fileInput = null;
		FileOutputStream out = null;
		Properties props = null;
		try {
			// 保存上传文件最后一次选择的路径
			props = new Properties();
			// 配置文件地址
			String path = JecnDesignerImpl.class.getResource("/").toURI()
					.getPath()
					+ "resources/JecnConfig.properties";
			fileInput = new FileInputStream(path);
			in = new BufferedInputStream(fileInput);
			props.load(in);
			out = new FileOutputStream(path);
			if (JecnConstants.projectId != null) {
				props.setProperty("projectId",
						JecnConstants.projectId.toString());
			}
			props.store(out, "");
		} catch (Exception e) {
			log.error("ProjectManageDialog openProButAction is error", e);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
				if (fileInput != null) {
					fileInput.close();
				}
			} catch (IOException e) {
				log.error("ProjectManageDialog openProButAction is error", e);
			}

		}

		this.dispose();
	}

	class JecnProjectTable extends JecnTable {

		public JecnProjectTable() {
			DefaultTableCellRenderer render = new DefaultTableCellRenderer();
			render.setHorizontalAlignment(SwingConstants.CENTER);
			TableColumn tableColumn = columnModel.getColumn(2);
			tableColumn.setCellRenderer(render);
			tableColumn.setMinWidth(50);
			tableColumn.setMaxWidth(50);
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTitleTable(), getContentTable());
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0 };
		}

	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得table的标题
	 * @return
	 */
	private Vector<String> getTitleTable() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("projectName"));
		title.add("");
		return title;
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得table的内容
	 * @return
	 */
	private Vector<Vector<String>> getContentTable() {
		Vector<Vector<String>> vectords = new Vector<Vector<String>>();

		try {
			projectList = ConnectionPool.getProject().getListProject();
			JecnCurProject jecnCurProject = ConnectionPool.getCurProject()
					.getJecnCurProjectById();

			for (JecnProject jecnProject : projectList) {

				Vector<String> data = new Vector<String>();
				data.add(jecnProject.getProjectId().toString());
				data.add(jecnProject.getProjectName().trim());
				if (jecnCurProject != null
						&& jecnProject.getProjectId().equals(
								jecnCurProject.getCurProjectId())) {
					data.add(JecnProperties.getValue("mainProject"));
				}
				vectords.add(data);
			}
			return vectords;
		} catch (Exception e) {
			log.error("ProjectManageDialog getContentTable is error", e);
		}
		return null;
	}

}
