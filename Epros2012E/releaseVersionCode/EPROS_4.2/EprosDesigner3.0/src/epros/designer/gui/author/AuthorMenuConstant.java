package epros.designer.gui.author;

import java.util.ArrayList;
import java.util.List;

import epros.designer.gui.common.JecnDesignerCommon;

public enum AuthorMenuConstant {
	INSTANCE;

	/**
	 * 根据角色初始化菜单集合
	 */
	public void initMenuEnum() {
		if (JecnDesignerCommon.isAdmin()) {
			this.initAdminAuthorMenu();
		} else if (JecnDesignerCommon.isSecondAdmin()) {
			this.initSecondAdminAuthorMenu();
		} else {
			this.initDesignerAuthorMenu();
		}
	}

	/**
	 * 
	 * 系统管理员菜单权限集合
	 */
	private final List<AuthorMenuEnum> adminAuthorMenuEnum = new ArrayList<AuthorMenuEnum>();
	/**
	 * 二级管理员菜单权限集合
	 */
	private final List<AuthorMenuEnum> secondAdminAuthorMenuEnum = new ArrayList<AuthorMenuEnum>();

	/**
	 * 设计者菜单权限集合
	 */
	private final List<AuthorMenuEnum> designerAuthorMenuEnum = new ArrayList<AuthorMenuEnum>();

	/**
	 * 根据角色获取权限菜单集合
	 * 
	 * @return
	 */
	public List<AuthorMenuEnum> getAhthorMenuList() {
		if (JecnDesignerCommon.isAdmin()) {
			return adminAuthorMenuEnum;
		} else if (JecnDesignerCommon.isSecondAdmin()) {
			return secondAdminAuthorMenuEnum;
		} else {
			return designerAuthorMenuEnum;
		}
	}

	/**
	 * 初始化系统管理员菜单
	 */
	private void initAdminAuthorMenu() {
		for (AuthorMenuEnum menuEnum : AuthorMenuEnum.values()) {
			if (menuEnum.isAdmin()) {
				adminAuthorMenuEnum.add(menuEnum);
			}
		}
	}

	/**
	 * 初始化二级管理员
	 */
	private void initSecondAdminAuthorMenu() {
		for (AuthorMenuEnum menuEnum : AuthorMenuEnum.values()) {
			if (menuEnum.isSecondAdmin()) {
				secondAdminAuthorMenuEnum.add(menuEnum);
			}
		}
	}

	private void initDesignerAuthorMenu() {
		for (AuthorMenuEnum menuEnum : AuthorMenuEnum.values()) {
			if (menuEnum.isDesigner()) {
				designerAuthorMenuEnum.add(menuEnum);
			}
		}
	}
}
