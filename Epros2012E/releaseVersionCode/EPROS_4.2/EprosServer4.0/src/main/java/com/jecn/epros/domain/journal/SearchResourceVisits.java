package com.jecn.epros.domain.journal;

import io.swagger.annotations.ApiModelProperty;

/**
 * 日志统计 搜索
 *
 * @author lenovo
 */
public class SearchResourceVisits {

    @ApiModelProperty(value = "文件ID", required = true)
    private Long relatedId;
    @ApiModelProperty(value = "文件类型 0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构", required = true)
    private int relatedType;
    @ApiModelProperty(value = "资源名称")
    private String relatedName;
    @ApiModelProperty(required = true)
    private String startTime;
    @ApiModelProperty(required = true)
    private String endTime;

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public int getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(int relatedType) {
        this.relatedType = relatedType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }
}
