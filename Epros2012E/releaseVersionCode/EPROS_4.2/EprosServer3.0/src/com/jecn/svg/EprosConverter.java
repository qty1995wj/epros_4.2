package com.jecn.svg;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.svg.dataModel.ImmutableStyle;
import com.jecn.svg.dataModel.SvgBaseGraph;
import com.jecn.svg.dataModel.SvgGraphStyle;
import com.jecn.svg.dataModel.SvgTextStyle;
import com.jecn.svg.dataModel.epros.SvgEprosGraphData;
import com.jecn.svg.dataModel.epros.SvgEprosLineData;
import com.jecn.svg.dataModel.graphs.SvgGraph;
import com.jecn.svg.dataModel.graphs.SvgLinkIcon;
import com.jecn.svg.dataModel.graphs.SvgLinkIcon.IconName;
import com.jecn.svg.dataModel.line.SvgConnectingLine;
import com.jecn.svg.dataModel.line.SvgDividingLine;

public class EprosConverter {

	private SvgEprosGraphData curResource;
	private SvgBaseGraph curConvertionGraph = null;
	private List<SvgEprosLineData> allManhattanLineResource;
	private List<SvgLinkIcon> allLinkIcons;
	private String figureType;
	// 所有元素集合
	private List<SvgEprosGraphData> eprosGraphResource;
	// 0：角色元素ID，1：关联的岗位类型（0 岗位，1：岗位组），2：岗位/岗位组名称
	private List<Object[]> listPosts;

	public EprosConverter() {

	}

	public EprosConverter(List<SvgEprosLineData> mLineResource, List<SvgLinkIcon> linkIcons,
			List<SvgEprosGraphData> eprosGraphResource, List<Object[]> listPosts) {
		this.allManhattanLineResource = mLineResource;
		this.allLinkIcons = linkIcons;
		this.eprosGraphResource = eprosGraphResource;
		this.listPosts = listPosts;
	}

	/**
	 * 根据Epros元素初始化对应的svg图形
	 * 
	 * @param eprosGraph
	 * @param eprosGraph
	 * @return
	 */
	public SvgBaseGraph newInstanceAndInitializationWithEprosGraph(SvgEprosGraphData eprosGraph, int languageType) {
		curResource = eprosGraph;
		figureType = curResource.getFigureType();
		curConvertionGraph = getInstanceByCurFigureType();
		initBaseAttrs();
		initStyleAttrs();

		if (curConvertionGraph instanceof SvgDividingLine) {
			initDividingLine();
		} else if (curConvertionGraph instanceof SvgConnectingLine) {
			initManhattanLine();
		} else {
			initGraph(languageType);
			fillSvgConnectionLink();
		}
		ImmutableStyle.setImmutableStyleToGraph(curConvertionGraph);
		return curConvertionGraph;
	}

	/**
	 * 根据图形类型获取对应的svg实例
	 * 
	 * @param eprosGraph
	 * @return
	 */
	private SvgBaseGraph getInstanceByCurFigureType() {
		SvgBaseGraph newInstance = null;
		if (figureType.equals("ParallelLines") || figureType.equals("VerticalLine")
				|| figureType.equals("VDividingLine") || figureType.equals("HDividingLine")
				|| figureType.equals("DividingLine")) {
			newInstance = new SvgDividingLine();
		} else if (figureType.equals("ManhattanLine") || figureType.equals("CommonLine")) {
			newInstance = new SvgConnectingLine();
		} else {
			newInstance = new SvgGraph();
		}
		return newInstance;
	}

	/**
	 * 初始化基本属性
	 */
	private void initBaseAttrs() {
		curConvertionGraph.setFigureId(curResource.getFigureId()).setType(figureType).setText(
				curResource.getFigureText());
	}

	/**
	 * 初始化样式属性
	 */
	private void initStyleAttrs() {
		curConvertionGraph.setGraphStyle(getSvgGraphStyle()).setTextStyle(getSvgTextStyle());
		curConvertionGraph.getGraphStyle().setGraph3dStyle(castSvgGraph3dStyle());
		curConvertionGraph.getGraphStyle().setShadow(castShadow());
	}

	/**
	 * 转换阴影
	 * 
	 * @return
	 */
	private String castShadow() {
		if (StringUtils.isBlank(curResource.getBGColor())) {
			return null;
		}
		if (curResource.getHavaShadow() != null && curResource.getHavaShadow().intValue() == 1) {
			return castFill(curResource.getShadowColor());
		}
		return null;
	}

	/**
	 * 获取文本样式
	 * 
	 * @param eprosGraph
	 * @return
	 */
	private SvgTextStyle getSvgTextStyle() {
		SvgTextStyle svgTextStyle = new SvgTextStyle();
		svgTextStyle.setFamily(curResource.getFontType());
		if (curResource.getFontSize() != null) {
			svgTextStyle.setSize(curResource.getFontSize().intValue());
		}
		svgTextStyle.setWeight(castFontWeight());
		svgTextStyle.setFill(castFill(curResource.getFontColor()));
		svgTextStyle.setAnchor(castTextAnchor());
		return svgTextStyle;
	}

	/**
	 * 转换文本方向
	 * 
	 * @return
	 */
	private String castTextAnchor() {
		if (curResource.getFontPosition() != null) {
			return SvgTextStyle.textAnchors[Integer.valueOf(curResource.getFontPosition())];
		} else if (curConvertionGraph instanceof SvgConnectingLine) {
			return SvgTextStyle.textAnchors[4];
		} else {
			return null;
		}
	}

	/**
	 * 添加水平分割线
	 * 
	 * @param eprosGraph
	 */
	private SvgGraphStyle getSvgGraphStyle() {
		SvgGraphStyle style = new SvgGraphStyle();
		style.appendTransform(castRotate());
		if (figureType.equals("DateFreeText") || figureType.equals("MapNameText") || figureType.equals("FreeText")) {
			if (StringUtils.isBlank(curResource.getBGColor())) {
				style.setStrokeWidth("0");
				return style;
			}
		}
		style.setFill(castFill(curResource.getBGColor()));
		style.setStroke(castStroke());
		style.setStrokeWidth(curResource.getLineThickness() + "");
		style.setStrokeDashArray(castStrokeDashArray());
		return style;
	}

	/**
	 * 转换填充色
	 * 
	 * @param color
	 *            eg:("137,188,255,192,220,255,4")
	 */
	private String castFill(String color) {
		if (StringUtils.isBlank(color)) {
			return "none";
		}
		// 单双色处理
		int[] fillArray = spiltAndCastTypeToInt(color);
		// 双色 渐变
		switch (fillArray.length) {
		case 7:
			Color color1 = new Color(fillArray[0], fillArray[1], fillArray[2]);
			Color color2 = new Color(fillArray[3], fillArray[4], fillArray[5]);
			int angle = 0;// 默认为水平
			switch (fillArray[6]) {
			case 1:// 向上倾斜
				angle = 350;
				break;
			case 2:// 向下倾斜
				angle = 165;
				break;
			case 3:// 垂直
				angle = 0;
				break;
			case 4:// 水平
				angle = 90;
				Color temp = color1;
				color1 = color2;
				color2 = temp;
				break;
			}
			angle += getEprosRotate();
			return angle + "-" + colorToRGB(color1) + "-" + colorToRGB(color2);
		case 3:
		case 4:
		default:
			Color singleColor = new Color(fillArray[0], fillArray[1], fillArray[2]);
			return colorToRGB(singleColor);
		}
	}

	/**
	 * 拆分Epros颜色数据
	 * 
	 * @param source
	 * @return
	 */
	private static int[] spiltAndCastTypeToInt(String source) {
		String[] strArray = source.split(",");
		int[] intArray = new int[strArray.length];
		for (int i = 0; i < intArray.length; i++) {
			intArray[i] = Integer.valueOf(strArray[i]);
		}
		return intArray;
	}

	/**
	 * 颜色转换
	 * 
	 * @param color
	 * @return
	 */
	private static String colorToRGB(Color color) {
		String R = Integer.toHexString(color.getRed());
		R = R.length() < 2 ? ('0' + R) : R;
		String G = Integer.toHexString(color.getGreen());
		G = G.length() < 2 ? ('0' + G) : G;
		String B = Integer.toHexString(color.getBlue());
		B = B.length() < 2 ? ('0' + B) : B;
		return "#" + R + G + B;
	}

	/**
	 * 转换边框颜色
	 * 
	 * @param stroke
	 * @return
	 */
	private String castStroke() {
		String stroke = curResource.getBodyColor();
		if (StringUtils.isBlank(stroke)) {
			stroke = curResource.getLineColor();
		}
		if (StringUtils.isBlank(stroke)) {
			return "#000";
		}
		int[] strokeColor = spiltAndCastTypeToInt(stroke);
		return colorToRGB(new Color(strokeColor[0], strokeColor[1], strokeColor[2]));
	}

	/**
	 * 转换字体大小
	 * 
	 * @param fontBody
	 * @return
	 */
	private String castFontWeight() {
		return curResource.getFontBody() == null || curResource.getFontBody().intValue() == 0 ? null : "bold";
	}

	/**
	 * 转换边框样式
	 * 
	 * @param bodyLine
	 * @return
	 */
	private String castStrokeDashArray() {
		if ("1".equals(curResource.getBodyLine())) {
			return "eprosdash";
		} else if ("2".equals(curResource.getBodyLine())) {
			return "eprosdotdash";
		} else if ("3".equals(curResource.getBodyLine())) {
			return "-";
		}
		return null;
	}

	/**
	 * 转换角度
	 * 
	 * @param circumgyrate
	 * @return
	 */
	private String castRotate() {
		return curResource.getCircumgyrate() != null ? "r" + curResource.getCircumgyrate() : null;
	}

	/**
	 * 初始化分割线数据
	 * 
	 * @param eprosGraph
	 * @param curConvertionGraph
	 */
	private void initDividingLine() {
		SvgDividingLine dLine = (SvgDividingLine) curConvertionGraph;
		dLine.setStartX(curResource.getPoLongx());
		dLine.setStartY(curResource.getPoLongy());
		if (figureType.equals("ParallelLines")) {
			dLine.setEndY(curResource.getPoLongy());
		} else if (figureType.equals("VDividingLine") || figureType.equals("HDividingLine")) {
			dLine.setStartX(curResource.getStartFigure());
			dLine.setStartY(curResource.getEndFigure());
			dLine.setEndX(curResource.getPoLongx());
			dLine.setEndY(curResource.getPoLongy());
		} else if (figureType.equals("VerticalLine")) {
			dLine.setEndX(curResource.getPoLongx());
		} else if (figureType.equals("DividingLine")) {
			int diff = Integer.valueOf(dLine.getGraphStyle().getStrokeWidth()) / 2;
			if (curResource.getEndFigure().intValue() != curResource.getPoLongy()) {
				dLine.setStartY(curResource.getEndFigure());
				dLine.setEndY(curResource.getPoLongy());
			} else {
				dLine.setStartY(curResource.getEndFigure() - diff);
				dLine.setEndY(curResource.getPoLongy() - diff);
			}
			if (curResource.getStartFigure() < curResource.getPoLongx()) {
				diff = -diff;
			}
			dLine.setStartX(curResource.getStartFigure() + diff);
			dLine.setEndX(curResource.getPoLongx() - diff);
		}
	}

	/**
	 * 初始化曼哈顿线数据
	 * 
	 * @param curConvertionGraph
	 */
	private void initManhattanLine() {
		SvgConnectingLine mLine = (SvgConnectingLine) curConvertionGraph;
		// 连接线开始图形
		mLine.setStartFigureId(curResource.getStartFigure());
		// 联系连接图形 位置
		mLine.setPoLongx(curResource.getPoLongx().intValue());

		if (StringUtils.isNotBlank(curResource.getFigureText()) && curResource.getWidth() != null
				&& curResource.getWidth() != 0) {
			mLine.setTextPoint(new Point(curResource.getWidth().intValue(), curResource.getHeight().intValue()));
		}

		for (SvgEprosLineData lineSegment : allManhattanLineResource) {
			if (mLine.getFigureId() == lineSegment.getFigureId()) {

				// 小线段相交点
				List<int[]> xyPoint = null;
				if (lineSegment.horizontalLine()) {
					xyPoint = getXYPoint(lineSegment);
				}
				mLine.addLineData(lineSegment.getStartX(), lineSegment.getStartY(), lineSegment.getEndX(), lineSegment
						.getEndY(), xyPoint);
			}
		}
		// 设置是否显示连接线
		mLine.setIsTwoArrowLine(curResource.getIsOnLine());
		mLine.setTextPosition();
		// 小线段排序
		mLine.reOrderLinePoint(eprosGraphResource);
	}

	/**
	 * 获取横线在面板上与竖线的交点
	 * 
	 * @param horLine
	 * @return
	 */
	private List<int[]> getXYPoint(SvgEprosLineData horLine) {
		List<int[]> xyPointArr = new ArrayList<int[]>();
		Set<Point> pointSet = new HashSet<Point>();
		Point crossOverPoint = null;
		for (SvgEprosLineData lineSegment : allManhattanLineResource) {
			crossOverPoint = getXYPoint(horLine, lineSegment);
			if (crossOverPoint == null) {
				continue;
			}
			pointSet.add(crossOverPoint);
		}
		for (Point point : pointSet) {
			xyPointArr.add(new int[] { point.x, point.y });
		}
		return xyPointArr;
	}

	private Point getXYPoint(SvgEprosLineData horLine, SvgEprosLineData verLine) {
		// 理想中的横线纵线焦点
		Point xyPoint = new Point(verLine.getIntSX(), horLine.getIntSY());
		// 2、验证交点是否在线段上
		if (horLine.getIntSX() > horLine.getIntEX()) {
			if (horLine.getIntEX() < xyPoint.x && xyPoint.x < horLine.getIntSX()) {// 交点坐标必须在横线上，X坐标在横线长度范围内
				if (verLine.getIntSY() < verLine.getIntEY()) {
					if (verLine.getIntSY() < xyPoint.y && xyPoint.y < verLine.getIntEY()) {
						// 满足条件 交点存在
						return xyPoint;
					}
				}
				if (verLine.getIntSY() > verLine.getIntEY()) {
					if (verLine.getIntEY() < xyPoint.y && xyPoint.y < verLine.getIntSY()) {
						// 满足条件 交点存在
						return xyPoint;
					}
				}
			}
		} else if (horLine.getIntSX() < horLine.getIntEX()) {
			if (horLine.getIntSX() < xyPoint.x && xyPoint.x < horLine.getIntEX()) {
				if (verLine.getIntSY() < verLine.getIntEY()) {
					if (verLine.getIntSY() < xyPoint.y && xyPoint.y < verLine.getIntEY()) {
						// 满足条件 交点存在
						return xyPoint;
					}
				}
				if (verLine.getIntSY() > verLine.getIntEY()) {
					if (verLine.getIntEY() < xyPoint.y && xyPoint.y < verLine.getIntSY()) {
						// 满足条件 交点存在
						return xyPoint;
					}
				}
			}
		}
		return null;
	}

	/**
	 * 初始化图形数据
	 * 
	 * @param curResource
	 * @param curConvertionGraph
	 * @return
	 */
	private SvgGraph initGraph(int languageType) {
		SvgGraph graph = (SvgGraph) curConvertionGraph;
		graph.setX(castX()).setY(castY()).setWidth(castWidth()).setHeight(castHeight());
		setSpecialData(graph);
		if (JecnConfigTool.useNewInout()) {
			setTermHoverText(graph, languageType);
		} else {
			setHoverText(graph, languageType);
		}
		return graph;
	}

	/**
	 * 设置悬浮文本
	 * 
	 * @param graph
	 */
	private void setHoverText(SvgGraph graph, int languageType) {
		if (isActivity() || JecnUtil.isActivityFlow(figureType)) {
			StringBuffer buffer = new StringBuffer();
			if (allLinkIcons != null && allLinkIcons.size() > 0) {
				for (SvgLinkIcon linkIcon : allLinkIcons) {
					if (linkIcon.getIconName() == IconName.ACTIVE_IN
							&& curResource.getFigureId().intValue() == linkIcon.getFigureId().intValue()) {
						if (linkIcon.getFileId() != "" && linkIcon.getFileId().length() != 0)
							buffer.append(linkIcon.getFileName()).append(JecnUtil.getValue("Template", languageType))
									.append("<br>");
					}
				}
			}

			if (StringUtils.isNotBlank(curResource.getActivityInput()) || StringUtils.isNotBlank(buffer.toString())) {
				if (buffer.length() > 0) {
					buffer.append("<br>");
				}
				String[] input = getHverText(JecnUtil.getValue("inputC", languageType), "<br>"
						+ JecnCommon.replaceWarp(curResource.getActivityInput()), "<br>" + buffer.toString());
				graph.addHoverText(input);
			}
			buffer = new StringBuffer();
			if (allLinkIcons != null && allLinkIcons.size() > 0) {
				for (SvgLinkIcon linkIcon : allLinkIcons) {
					if (linkIcon.getIconName() == IconName.ACTIVE_OUT
							&& curResource.getFigureId().intValue() == linkIcon.getFigureId().intValue()) {
						if (linkIcon.getFileId() != "" && linkIcon.getFileId().length() != 0)
							buffer.append(linkIcon.getFileName()).append(JecnUtil.getValue("Template", languageType))
									.append("<br>");
					}
				}
			}

			// 输出:
			if (StringUtils.isNotBlank(curResource.getActivityOutput()) || StringUtils.isNotBlank(buffer)) {
				if (buffer.length() > 0) {
					buffer.append("<br>");
				}
				String[] output = getHverText(JecnUtil.getValue("outPutC", languageType), "<br>"
						+ JecnCommon.replaceWarp(curResource.getActivityOutput()), "<br>" + buffer.toString());
				graph.addHoverText(output);
			}

			if (StringUtils.isNotBlank(curResource.getActivityShow())) {
				// 活动说明:
				graph.addHoverText(new String[] { JecnUtil.getValue("activityDescriptionC", languageType),
						"<br>" + JecnCommon.replaceWarp(curResource.getActivityShow()) + "<br><br>" });
			}
			if (StringUtils.isNotBlank(curResource.getRoleRes())) {
				// 关键活动:
				graph.addHoverText(new String[] { JecnUtil.getValue("keyExplanationC", languageType),
						"<br>" + JecnCommon.replaceWarp(curResource.getRoleRes()) });
			}
		} else if (StringUtils.isNotBlank(curResource.getActivityShow())) {
			// 描述:
			String tipText = JecnUtil.getValue("rescriptionC", languageType);

			graph.addHoverText(new String[] { isFreeText() ? "" : tipText,
					"<br>" + JecnCommon.replaceWarp(curResource.getActivityShow()) });
		} else if (isRole()) {
			getRoleRelatedPost(graph, languageType);
			if (StringUtils.isNotBlank(curResource.getRoleRes())) {
				// 角色职责：
				graph.addHoverText(new String[] { JecnUtil.getValue("roleResponsibilitiesC", languageType),
						"<br>" + JecnCommon.replaceWarp(curResource.getRoleRes()) });
			}
		} else if ("TermFigureAR".equals(figureType)) {
			if (StringUtils.isNotBlank(curResource.getFigureText())) {
				// 名称
				graph.addHoverText(new String[] { JecnUtil.getValue("nameC", languageType),
						curResource.getFigureText() + "<br>" });
			}
			if (StringUtils.isNotBlank(curResource.getTermDefine())) {
				// 定义
				graph.addHoverText(new String[] { JecnUtil.getValue("definitionC", languageType),
						"<br>" + curResource.getTermDefine() });
			}
		}
	}

	/**
	 * 设置新版悬浮文本
	 * 
	 * @param graph
	 */
	private void setTermHoverText(SvgGraph graph, int languageType) {
		if (isActivity() || JecnUtil.isActivityFlow(figureType)) {
			StringBuffer buffer = new StringBuffer();
			String kong = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";
			if (allLinkIcons != null && allLinkIcons.size() > 0) {
				// 输入
				for (SvgLinkIcon linkIcon : allLinkIcons) {
					if ((linkIcon.getIconName() == IconName.ACTIVE_IN
							|| linkIcon.getIconName() == IconName.ACTIVE_FILE_IN || linkIcon.getIconName() == IconName.ACTIVE_IN_TEXT)
							&& curResource.getFigureId().intValue() == linkIcon.getFigureId().intValue()) {
						if (StringUtils.isNotBlank(linkIcon.getName())) {
							buffer.append(kong + linkIcon.getName() + "<br>");
							if (linkIcon.getSample() != null && !linkIcon.getSample().isEmpty())
								for (JecnFigureInoutSampleT sample : linkIcon.getSample()) {
									if (sample.getType() == 0) {
										buffer.append(
												kong + kong + JecnUtil.getValue("Template", languageType)
														+ "&nbsp;&nbsp;").append(sample.getFileName()).append("<br>");
									}
								}
						}
					}
				}
				if (StringUtils.isNotBlank(buffer.toString())) {
					String[] input = getHverText(JecnUtil.getValue("inputC", languageType), kong, buffer.toString());
					graph.addHoverText(input);
				}
				// 输出
				buffer = new StringBuffer();
				for (SvgLinkIcon linkIcon : allLinkIcons) {
					if ((linkIcon.getIconName() == IconName.ACTIVE_OUT
							|| linkIcon.getIconName() == IconName.ACTIVE_FILE_OUT || linkIcon.getIconName() == IconName.ACTIVE_OUT_TEXT)
							&& curResource.getFigureId().intValue() == linkIcon.getFigureId().intValue()) {
						if (StringUtils.isNotBlank(linkIcon.getName())) {
							buffer.append(kong + linkIcon.getName() + "<br>");
							for (JecnFigureInoutSampleT sample : linkIcon.getSample()) {
								if (sample.getType() == 1) {
									buffer.append(
											kong + kong + JecnUtil.getValue("sample", languageType) + "&nbsp;&nbsp;")
											.append(sample.getFileName()).append("<br>");
								} else {
									buffer.append(
											kong + kong + JecnUtil.getValue("Template", languageType) + "&nbsp;&nbsp;")
											.append(sample.getFileName()).append("<br>");
								}

							}
						}
					}
				}
				if (StringUtils.isNotBlank(buffer.toString())) {
					String[] output = getHverText(JecnUtil.getValue("outPutC", languageType), kong, buffer.toString());
					graph.addHoverText(output);
				}

				// 操作规范
				buffer = new StringBuffer();
				for (SvgLinkIcon linkIcon : allLinkIcons) {
					if (linkIcon.getIconName() == IconName.FILE
							&& curResource.getFigureId().intValue() == linkIcon.getFigureId().intValue()) {
						if (StringUtils.isNotBlank(linkIcon.getFileId()))
							buffer.append(kong + linkIcon.getFileName()).append("<br>");
					}
				}
				if (StringUtils.isNotBlank(buffer.toString())) {
					String[] operation = getHverText(JecnUtil.getValue("operation", languageType), kong, buffer
							.toString());
					graph.addHoverText(operation);
				}
				// 关联标准
				buffer = new StringBuffer();
				for (SvgLinkIcon linkIcon : allLinkIcons) {
					if (linkIcon.getIconName() == IconName.ACTIVE_STANDARD
							&& curResource.getFigureId().intValue() == linkIcon.getFigureId().intValue()) {
						if (StringUtils.isNotBlank(linkIcon.getFileId()))
							buffer.append(kong + linkIcon.getFileName()).append("<br>");
					}
				}
				if (StringUtils.isNotBlank(buffer.toString())) {
					String[] refStand = getHverText(JecnUtil.getValue("refStand", languageType), kong, buffer
							.toString());
					graph.addHoverText(refStand);
				}

			}

			if (StringUtils.isNotBlank(curResource.getActivityShow())) {
				// 活动说明:
				graph.addHoverText(new String[] { JecnUtil.getValue("activityDescriptionC", languageType),
						"<br>" + kong + JecnCommon.replaceWarp(curResource.getActivityShow()) + "<br>" });
			}
			if (StringUtils.isNotBlank(curResource.getRoleRes())) {
				// 关键活动:
				graph.addHoverText(new String[] { JecnUtil.getValue("keyExplanationC", languageType),
						"<br>" + kong + JecnCommon.replaceWarp(curResource.getRoleRes()) });
			}

		} else if (StringUtils.isNotBlank(curResource.getActivityShow())) {
			// 描述:
			String tipText = JecnUtil.getValue("rescriptionC", languageType);

			graph.addHoverText(new String[] { isFreeText() ? "" : tipText,
					"<br>" + JecnCommon.replaceWarp(curResource.getActivityShow()) });
		} else if (isRole()) {
			getRoleRelatedPost(graph, languageType);
			if (StringUtils.isNotBlank(curResource.getRoleRes())) {
				// 角色职责：
				graph.addHoverText(new String[] { JecnUtil.getValue("roleResponsibilitiesC", languageType),
						"<br>" + JecnCommon.replaceWarp(curResource.getRoleRes()) });
			}
		} else if ("TermFigureAR".equals(figureType)) {
			if (StringUtils.isNotBlank(curResource.getFigureText())) {
				// 名称
				graph.addHoverText(new String[] { JecnUtil.getValue("nameC", languageType),
						curResource.getFigureText() + "<br>" });
			}
			if (StringUtils.isNotBlank(curResource.getTermDefine())) {
				// 定义
				graph.addHoverText(new String[] { JecnUtil.getValue("definitionC", languageType),
						"<br>" + curResource.getTermDefine() });
			}
		}
	}

	private void getRoleRelatedPost(SvgGraph graph, int languageType) {
		StringBuffer postBuffer = new StringBuffer();
		StringBuffer postGroupBuffer = new StringBuffer();
		// 0：角色元素ID，1：关联的岗位类型（0 岗位，1：岗位组），2：岗位/岗位组名称
		for (Object[] obj : listPosts) {
			if (!curResource.getFigureId().toString().equals(obj[0].toString())) {
				continue;
			}
			if ("0".equals(obj[1].toString())) {
				if (postBuffer.length() > 0) {
					postBuffer.append("/");
				}
				postBuffer.append(obj[2].toString());
			} else {
				if (postGroupBuffer.length() > 0) {
					postGroupBuffer.append("/");
				}
				postGroupBuffer.append(obj[2].toString());
			}
		}

		if (postBuffer.length() > 0) {
			String[] post = getHverText(JecnUtil.getValue("positionNameC", languageType), "<br>"
					+ JecnCommon.replaceWarp(curResource.getActivityOutput()), postBuffer.toString() + "<br><br>");
			// 岗位：
			graph.addHoverText(post);
		}
		if (postGroupBuffer.length() > 0) {
			String[] postGroup = getHverText(JecnUtil.getValue("positionGroupC", languageType), "<br>"
					+ JecnCommon.replaceWarp(curResource.getActivityOutput()), postGroupBuffer.toString() + "<br><br>");
			// 岗位组
			graph.addHoverText(postGroup);
		}
	}

	private String[] getHverText(String title, String strActive, String fileString) {
		if (StringUtils.isBlank(strActive) && StringUtils.isBlank(fileString)) {
			return null;
		} else if (StringUtils.isBlank(strActive) && StringUtils.isNotBlank(fileString)) {
			return new String[] { title, fileString };
		} else if (StringUtils.isNotBlank(strActive) && StringUtils.isBlank(fileString)) {
			return new String[] { title, strActive };
		}
		return new String[] { title, strActive + "<br>" + fileString.toString() };
	}

	/**
	 * 是否为角色
	 * 
	 * @return
	 */
	private boolean isRole() {
		return JecnUtil.isRole(figureType);
	}

	/**
	 * 是否为活动
	 * 
	 * @return
	 */
	private boolean isActivity() {
		// 活动或者十所接口
		return JecnUtil.isActive(figureType);
	}

	private boolean isFreeText() {
		return "FreeText".equals(figureType);
	}

	/**
	 * 设置特殊图形实现时所需的数据
	 * 
	 * @param graph
	 */
	private void setSpecialData(SvgGraph graph) {
		if (figureType.equals("CommentText")) {
			graph.setSpecialData(curResource.getStartFigure() + "," + curResource.getEndFigure());
		} else if (figureType.equals("ModelFigure")) {
			graph.setSpecialData(curResource.getDividingX() + "");
		} else if (figureType.equals("IconFigure")) {
			graph.setSpecialData(curResource.getLinkFlowId() == null ? null : curResource.getLinkFlowId().toString());
		} else if (isActivity()) {
			graph.setSpecialData(curResource.getActivityId() + "," + curResource.getOnLineName());
		} else if (JecnUtil.isActivityFlow(figureType)) {
			graph.setSpecialData(curResource.getActivityId());
		}
	}

	/**
	 * 按照Epros元数据，补全图标数据
	 * 
	 * @return
	 */
	private void fillSvgConnectionLink() {
		if (curResource.getLinkFlowId() == null || isNoLinkFigure(figureType)) {
			return;
		}
		SvgLinkIcon linkIcon = new SvgLinkIcon();
		linkIcon.setFigureId(curResource.getFigureId());
		linkIcon.setFileId(curResource.getLinkFlowId() + "");
		if ("ImplFigure".equals(figureType) || "OvalSubFlowFigure".equals(figureType)
				|| "InterfaceRightHexagon".equals(figureType) || JecnUtil.isActivityFlow(figureType)) {
			if (curResource.getImplType() != 0) {
				if (curResource.getImplType() == 1) {
					linkIcon.setIconName(IconName.UPPER_FLOW);
				} else if (curResource.getImplType() == 2) {
					linkIcon.setIconName(IconName.LOWER_FLOW);
				} else if (curResource.getImplType() == 3) {
					linkIcon.setIconName(IconName.PROCEDURE_FLOW);
				} else if (curResource.getImplType() == 4) {
					linkIcon.setIconName(IconName.SON_FLOW);
				}
			}
		} else {
			if (figureType.equals("SystemFigure")) {
				linkIcon.setIconName(IconName.LINK_RULE);
			} else if (figureType.equals("Rect")) {
				linkIcon.setIconName(IconName.LINK_ORG);
			} else {
				if ("1".equals(curResource.getLinkIsFlow())) {
					linkIcon.setIconName(IconName.LINK_FLOW);
				} else {
					linkIcon.setIconName(IconName.LINK_FLOW_MAP);
				}
			}
		}
		allLinkIcons.add(linkIcon);
	}

	private boolean isNoLinkFigure(String figureType) {
		return "IconFigure".equals(figureType) || "TermFigureAR".equals(figureType);
	}

	/**
	 * 根据角度转换X
	 * 
	 * @param x
	 * @param rotate
	 * @param width
	 * @return
	 */
	private Long castX() {
		long newX = 0;
		switch (getEprosRotate()) {
		case 90:
		case 270:
			newX = curResource.getPoLongx() + curResource.getWidth() / 2 - curResource.getHeight() / 2;
			break;
		default:
			newX = curResource.getPoLongx();
			break;
		}
		return newX;
	}

	/**
	 * 根据角度转换Y
	 * 
	 * @param y
	 * @param rotate
	 * @param height
	 * @return
	 */
	private Long castY() {
		long newY = 0;
		switch (getEprosRotate()) {
		case 90:
		case 270:
			newY = curResource.getPoLongy() + curResource.getHeight() / 2 - curResource.getWidth() / 2;
			break;
		default:
			newY = curResource.getPoLongy();
			break;
		}
		return newY;
	}

	/**
	 * 根据角度转换宽
	 * 
	 * @param rotate
	 * @param width
	 * @param height
	 * @return
	 */
	private Long castWidth() {
		return getEprosRotate() == 90 || getEprosRotate() == 270 ? curResource.getHeight() : curResource.getWidth();
	}

	/**
	 * 根据角度转换高
	 * 
	 * @param rotate
	 * @param width
	 * @param height
	 * @return
	 */
	private Long castHeight() {
		return getEprosRotate() == 90 || getEprosRotate() == 270 ? curResource.getWidth() : curResource.getHeight();
	}

	/**
	 * 获取Epros角度值
	 * 
	 * @return
	 */
	private int getEprosRotate() {
		if (curResource.getFigureType().equals("DataImage") || curResource.getFigureType().equals("ImplFigure")) {
			return 0;
		}
		if (StringUtils.isNotBlank(curResource.getCircumgyrate())) {
			return Double.valueOf(curResource.getCircumgyrate()).intValue();
		}
		return 0;
	}

	/**
	 * 获取3d效果所需的样式
	 * 
	 * @param fillColor
	 * @param baseGraphStyle
	 * @return
	 */
	private SvgGraphStyle castSvgGraph3dStyle() {
		if ((curResource.getIs3DEffect() != null && curResource.getIs3DEffect().intValue() == 0)
				|| StringUtils.isBlank(curResource.getBGColor())) {
			return null;
		}
		SvgGraphStyle graph3dStyle = curConvertionGraph.getGraphStyle().clone();
		int[] fillArray = spiltAndCastTypeToInt(curResource.getBGColor());
		Color baseColor = new Color(fillArray[0], fillArray[1], fillArray[2]);
		String brighterColor = colorToRGB(baseColor.brighter().brighter().brighter());
		String darkColor = colorToRGB(baseColor.darker());
		int angle = getEprosRotate() + 270;
		graph3dStyle.setFill(angle + "-" + brighterColor + "-" + darkColor + ":50" + "-" + darkColor);
		graph3dStyle.setStroke(darkColor);
		return graph3dStyle;
	}
}
