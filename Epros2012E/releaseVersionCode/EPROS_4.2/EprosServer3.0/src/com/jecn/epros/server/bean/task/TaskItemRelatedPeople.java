package com.jecn.epros.server.bean.task;

import java.io.Serializable;

/**
 * 任务配置项与审批人员的关联表
 * @author user
 *
 */
public class TaskItemRelatedPeople implements Serializable{
	
	private String id;
	/**任务配置项主键id**/
	private String configId;
	/** 人员id**/
	private Long peopleId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

}
