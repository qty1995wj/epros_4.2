package epros.draw.gui.box;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.draw.gui.box.JecnToolBoxMainPanel.ButtonShowType;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素&模型容器的父类
 * 
 * @author ZHOUXY
 * 
 */
abstract class JecnToolBoxAbstractContainPanel extends JecnPanel implements
		ActionListener {

	/** 流程元素库主面板 */
	protected JecnToolBoxMainPanel toolBoxMainPanel = null;

	/** 标题面板按钮 */
	protected JecnBoxTitlePanel titlePanel = null;
	/** 图标按钮所在面板的容器 */
	protected JPanel btnsPanel = null;

	/** 标题名称 */
	protected JLabel nameLabel = null;
	/** 选择图标按钮显示类型 */
	protected JecnToolBoxBarButton typeBtn = null;

	/** 流程元素：添加图标按钮；模型：显示标题/片段 */
	protected JecnToolBoxBarButton commonBtn = null;

	/** 按钮显示类型 */
	protected ButtonShowType buttonShowType = ButtonShowType.topBotton;

	/** 转换按钮图标显示方式 */
	protected JecnCoverButtonPopupMenu coverButtonPopupMenu = null;

	public JecnToolBoxAbstractContainPanel(JecnToolBoxMainPanel toolBoxMainPanel) {
		this.toolBoxMainPanel = toolBoxMainPanel;
		initComponent();
		initLayout();
		initFlowElemTitle();
	}

	private void initComponent() {
		// 标题面板按钮
		titlePanel = new JecnBoxTitlePanel();
		// 图标按钮所在面板
		btnsPanel = new JPanel();
		// 标题名称
		nameLabel = new JLabel();
		// 选择图标按钮显示类型
		typeBtn = new JecnToolBoxBarButton();
		// 流程元素：添加图标按钮；模型：显示标题/片段
		commonBtn = new JecnToolBoxBarButton();

		// 背景
		btnsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 无边框
		titlePanel.setBorder(null);
		// 选中不显示焦点框
		titlePanel.setFocusable(false);
		// 透明
		titlePanel.setOpaque(false);

		// 边框
		this.setBorder(JecnUIUtil.getTootBarBorder());

		// 布局管理器
		titlePanel.setLayout(new GridBagLayout());
		btnsPanel.setLayout(new BorderLayout());

		// 事件
		commonBtn.addActionListener(this);
		typeBtn.addActionListener(this);
	}

	private void initLayout() {
		// 标题面板按钮
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(titlePanel, c);
		// 图标所在面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(btnsPanel, c);
	}

	/**
	 * 
	 * 初始化流程元素标题
	 * 
	 */
	private void initFlowElemTitle() {

		// 名称
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
						.getInsets3(), 0, 0);
		titlePanel.add(nameLabel, c);
		// 空闲区域
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		titlePanel.add(new JLabel(), c);
		// 流程元素标题按钮：选择图标显示类型
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		titlePanel.add(typeBtn.getToolBar(), c);
		// 流程元素添加符号按钮
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 6), 0, 0);
		titlePanel.add(commonBtn.getToolBar(), c);
	}

	/**
	 * 
	 * 获取ButtonShowType类型对应的按钮大小
	 * 
	 * @return Dimension 按钮大小
	 */
	Dimension getButtonShowTypeSize() {
		Dimension dimension = null;
		switch (buttonShowType) {
		case leftRight: // 图片左内容右
			dimension = this.toolBoxMainPanel.getLeftRightBtnSize();
			break;
		case topBotton: // 图片上内容下
			dimension = this.toolBoxMainPanel.getTopButtonBtnSize();
			break;
		case titleText:// 显示详细信息
			dimension = this.toolBoxMainPanel.getTitleTextBtnSize();
			break;
		default:
			dimension = this.toolBoxMainPanel.getTopButtonBtnSize();
			break;
		}
		return dimension;
	}

	/**
	 * 
	 * 添加给定的组件到btnsPanel组件中
	 * 
	 * @param toolBoxScrollPane
	 *            JecnToolBoxAbstractScrollPane
	 */
	void addButtonToThis(JScrollPane toolBoxScrollPane) {
		if (toolBoxScrollPane == null) {
			return;
		}
		removeBtnsPanel();
		this.btnsPanel.add(toolBoxScrollPane);
	}

	void removeBtnsPanel() {
		this.btnsPanel.removeAll();
	}

	protected ButtonShowType getButtonShowType() {
		return buttonShowType;
	}

	protected void setButtonShowType(ButtonShowType buttonShowType) {
		this.buttonShowType = buttonShowType;
	}

	/**
	 * 
	 * 流程元素库标题栏面板
	 * 
	 * @author ZHOUXY
	 * 
	 */
	class JecnBoxTitlePanel extends JPanel {
		JecnBoxTitlePanel() {
		}

		protected void paintComponent(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;
			// 整体渐变
			g2d.setPaint(new GradientPaint(0, 0, JecnUIUtil
					.getFrameTitleTopColor(), 0, this.getHeight() - 1,
					JecnUIUtil.getBoxTitleBottomColor()));
			g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		}
	}
}
