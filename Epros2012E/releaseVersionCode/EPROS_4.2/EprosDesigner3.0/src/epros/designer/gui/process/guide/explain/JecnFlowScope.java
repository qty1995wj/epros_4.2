package epros.designer.gui.process.guide.explain;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.Border;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;

import epros.designer.gui.system.BasicComponent;
import epros.designer.gui.system.area.TextAreaFour;
import epros.designer.gui.system.field.TextField;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

/**
 * 流程范围（包括输入输出开始结束）
 * 
 * @author user
 * 
 */
public class JecnFlowScope extends JecnFileDescriptionComponent {

	private TextField start = null;
	private TextField end = null;
	private TextAreaFour in = null;
	private TextAreaFour out = null;
	private List<BasicComponent> components = new ArrayList<BasicComponent>();
	private JecnFlowBasicInfoT flowBasicInfoT;

	public JecnFlowScope(int index, String name, JecnPanel contentPanel, boolean isRequest,
			JecnFlowBasicInfoT flowBasicInfoT, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowBasicInfoT = flowBasicInfoT;
		initCom();
	}

	private void initCom() {
		int rows = 0;
		Insets insets = new Insets(2, 2, 2, 2);
		start = new TextField(rows, centerPanel, insets, JecnProperties.getValue("startActiveC"), flowBasicInfoT
				.getFlowStartpoint(), JecnProperties.getValue("startActive"));
		rows++;
		end = new TextField(rows, centerPanel, insets, JecnProperties.getValue("endActiveC"), flowBasicInfoT
				.getFlowEndpoint(), JecnProperties.getValue("endActive"));
		rows++;
		in = new TextAreaFour(rows, centerPanel, insets, JecnProperties.getValue("inLabC"), flowBasicInfoT.getInput(),
				JecnProperties.getValue("inLab"));
		rows++;
		out = new TextAreaFour(rows, centerPanel, insets, JecnProperties.getValue("outLabC"),
				flowBasicInfoT.getOuput(), JecnProperties.getValue("outLab"));

		components.add(start);
		components.add(end);
		components.add(in);
		components.add(out);

		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
	}

	@Override
	public boolean isUpdate() {
		for (BasicComponent c : components) {
			if (c.isUpdate()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		for (BasicComponent c : components) {
			if (c.isValidateNotPass(verfyLab)) {
				return true;
			}
		}
		return false;
	}

	public String getFlowIn() {
		return in.getResultStr();
	}

	public String getFlowOut() {
		return out.getResultStr();
	}

	public String getFlowStart() {
		return start.getResultStr();
	}

	public String getFlowEnd() {
		return end.getResultStr();
	}

}
