package epros.designer.gui.system.area;

import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class TextAreaUnlimited extends TextAreaFour {

	public TextAreaUnlimited(int rows, JPanel infoPanel, Insets insets, String labName, String text, String tipName,
			boolean isRequest) {
		super(rows, infoPanel, insets, labName, text, tipName,isRequest);
	}

	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}
}
