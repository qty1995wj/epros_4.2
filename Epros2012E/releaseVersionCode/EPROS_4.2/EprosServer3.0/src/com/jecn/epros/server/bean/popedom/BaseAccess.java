package com.jecn.epros.server.bean.popedom;

import java.io.Serializable;

public class BaseAccess implements Serializable {
	/** 0查阅 1下载 **/
	protected Integer accessType = 0;

	public Integer getAccessType() {
		return accessType;
	}

	public void setAccessType(Integer accessType) {
		this.accessType = accessType;
	}

}
