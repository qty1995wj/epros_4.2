package com.jecn.epros.server.bean.tree.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.util.JecnUtil;

public class BaseNode<T> implements Node<T> {

	private Node<T> parent = null;
	private final List<Node<T>> childs = new ArrayList<Node<T>>();
	private T data;
	private Long dbId;
	private Long sort;
	private Integer level;
	private String path;
	private String viewSort;

	@Override
	public void add(Node<T> c) {
		this.childs.add(c);
		c.setParent(this);
	}

	@Override
	public boolean hasChilds() {
		return childs.size() > 0;
	}

	@Override
	public List<Node<T>> getChilds() {
		return childs;
	}

	@Override
	public T getData() {
		return data;
	}

	@Override
	public Long getDbId() {
		validateDbId();
		return dbId;
	}

	@Override
	public Node<T> getParent() {
		return parent;
	}

	@Override
	public Long getSort() {
		return sort;
	}

	@Override
	public int getLevel() {
		if (parent == null || this.level != null) {
			return this.level;
		}
		this.level = parent.getLevel() + 1;
		return this.level;
	}

	@Override
	public String getViewSort() {
		validateDbId();
		if (parent == null || StringUtils.isNotBlank(this.viewSort)) {
			return this.viewSort;
		}
		this.viewSort = JecnUtil.concatViewSort(parent.getViewSort(), sort.intValue());
		return this.viewSort;
	}

	@Override
	public String getPath() {
		validateDbId();
		if (parent == null || StringUtils.isNotBlank(this.path)) {
			return this.path;
		}
		this.path = parent.getPath() + dbId + "-";
		return this.path;
	}

	protected void validateDbId() {
		if (dbId == null) {
			throw new IllegalArgumentException("DB id 为空");
		}
	}

	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setViewSort(String viewSort) {
		this.viewSort = viewSort;
	}

	@Override
	public void setParent(Node<T> parent) {
		this.parent = parent;
	}

	@Override
	public void setData(T t) {
		this.data = t;
	}

}
