package com.jecn.epros.util;

import com.jecn.epros.exception.domain.EprosBussinessException;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * Created by thinkpad on 2017/1/3.
 */
public class BeanUtils {

    public static void copyProperties(Object dest, Object ori) {
        try {
            PropertyUtils.copyProperties(dest, ori);
        } catch (Exception e) {
            throw new EprosBussinessException("对象属性Copy赋值异常", e);
        }
    }
}
