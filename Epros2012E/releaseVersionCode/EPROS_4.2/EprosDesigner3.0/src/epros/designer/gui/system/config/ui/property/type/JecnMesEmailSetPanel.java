package epros.designer.gui.system.config.ui.property.type;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 消息邮件配置
 * 
 * @author Administrator
 * 
 */
public class JecnMesEmailSetPanel extends JecnAbstractPropertyBasePanel implements ActionListener {

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 审批任务面板 */
	private JecnPanel approverTaskPanel = new JecnPanel();
	/** 创建任务面板 */
	private JecnPanel createTaskPanel = new JecnPanel();
	/** 发布任务面板 */
	private JecnPanel publishTaskPanel = new JecnPanel();
	/** 邮件 */
	private String strMail = JecnProperties.getValue("mail");

	/** 审批任务：审批人 审批人： */
	private JLabel approverLab = new JLabel(JecnProperties.getValue("approverC"));
	/** 审批任务：拟稿人： */
	private JLabel draftPeopleLab = new JLabel(JecnProperties.getValue("draftPeopleC"));
	/** 创建任务：审批人： */
	private JLabel addApproverLab = new JLabel(JecnProperties.getValue("approverC"));
	/** *发布任务：具有查阅权限的人： */
	private JLabel publishCompetenceLab = new JLabel(JecnProperties.getValue("hasAccessPeopleC"));
	/** 发布任务：审批人： */
	private JLabel publishApproverLab = new JLabel(JecnProperties.getValue("approverC"));
	/** 发布任务：拟稿人： */
	private JLabel publishDraftPeoLab = new JLabel(JecnProperties.getValue("draftPeopleC"));
	/** 发布任务：参与人： */
	private JLabel publishParticipantsLab = new JLabel(JecnProperties.getValue("partInPeopleC"));

	/** 审批任务：审批人：--邮件 */
	private JecnConfigCheckBox emailApproverCheckBox = new JecnConfigCheckBox(strMail);
	/** 审批任务：拟稿人--邮件 */
	private JecnConfigCheckBox emailDraftPeopleCheckBox = new JecnConfigCheckBox(strMail);
	/** 创建任务：审批人--邮件 */
	private JecnConfigCheckBox emailAddApproverCheckBox = new JecnConfigCheckBox(strMail);
	/** *发布任务：具有查阅权限的人--邮件 */
	private JecnConfigCheckBox emailPublishCompetenceCheckBox = new JecnConfigCheckBox(strMail);
	/** 发布任务：审批人--邮件 */
	private JecnConfigCheckBox emailPublishApproverCheckBox = new JecnConfigCheckBox(strMail);
	/** 发布任务：拟稿人--邮件 */
	private JecnConfigCheckBox emailPublishDraftPeoCheckBox = new JecnConfigCheckBox(strMail);
	/** 发布任务：参与人--邮件 */
	private JecnConfigCheckBox emailPublishParticipantsCheckBox = new JecnConfigCheckBox(strMail);

	/** 超时未审批任务提醒Lab */
	private JLabel mailTaskApproveTipLab = new JLabel(JecnProperties.getValue("taskApproveTipDayC"));
	/** 超时未审批任务提醒Field */
	private JecnConfigTextField mailTaskApproveTipField = new JecnConfigTextField();
	/** 超时未审批任务提醒提示信息 */
	private JecnUserInfoTextArea mailTaskApproveTipInfoArea = new JecnUserInfoTextArea();
	/** 是否开启超时提醒 */
	private JecnConfigCheckBox mailTaskApproveTipCheckBox = new JecnConfigCheckBox(JecnProperties
			.getValue("openTaskApproveTip"));

	private String TASK_APPING = JecnProperties.getValue("approvalTaskSetting");

	private String TASK_FINISH = JecnProperties.getValue("TaskMailSetting");

	/** **********************【流程有效期配置面板 】 *********************************/
	private JecnPanel flowValidPanel = null;
	/** 流程拟制人 */
	private JecnConfigCheckBox mailFlowFictionBox = null;
	/** 流程监护人 */
	private JecnConfigCheckBox mailFlowGuardianBox = null;
	/** 流程责任人 */
	private JecnConfigCheckBox mailFlowRefRationBox = null;
	/** 系统管理员 */
	private JecnConfigCheckBox mailFlowSystemManagerBox = null;

	private JecnPanel ruleValidPanel = null;
	/** 制度监护人 */
	private JecnConfigCheckBox mailRuleGuardianBox = null;
	/** 制度责任人 */
	private JecnConfigCheckBox mailRuleRefRationBox = null;
	/** 系统管理员 */
	private JecnConfigCheckBox mailRuleSystemManagerBox = null;

	/******* JLabel *******/
	/** 流程拟制人 */
	private JLabel flowFictionJLabel = null;
	/** 流程监护人 */
	private JLabel flowGuardianJLabel = null;
	/** 流程责任人 */
	private JLabel flowRefRationJLabel = null;
	/** 系统管理员 */
	private JLabel flowSystemManagerJLabel = null;

	/** 制度监护人 */
	private JLabel ruleGuardianJLabel = null;
	/** 制度责任人 */
	private JLabel ruleRefRationJLabel = null;
	/** 系统管理员 */
	private JLabel ruleSystemManagerJLabel = null;

	/*********** 审批人员缺失消息邮件提醒面板 ******************/
	private JecnPanel approvalLessEmailMesTipPanel = null;

	/*** 提醒时间间隔(日)： */
	private JLabel timeTipLab = null;
	/*** 提醒时间间隔(日)：选中启用，不选中不启用 */
	private JecnConfigCheckBox timeTipCheckBox = null;
	/** 提醒时间间隔(日)Field */
	private JecnConfigTextField timeTipField = new JecnConfigTextField();
	/** 提醒时间间隔(日)提示信息 */
	private JecnUserInfoTextArea timeTipInfoArea = new JecnUserInfoTextArea();
	/** 文件发布邮件提醒面板 */
	private JecnPanel pubMailPanel = new JecnPanel();
	/** 文件发布给管理员发送邮件 */
	private JecnConfigCheckBox mailPubToAdminBox = null;
	/** 文件发布给固定收件人发送邮件 */
	private JecnConfigCheckBox mailPubToFixedBox = null;
	/** 指定收件人label */
	private JLabel fixedPeopleLab = null;

	/*** 合理化建议邮件面板 */
	private JecnPanel rtnlProposePanel = new JecnPanel();
	/** 合理化建议给管理员发送邮件 */
	private JecnConfigCheckBox mailProposeToHandleBox = null;
	/** 合理化建议给合理化建议提交人邮件 */
	private JecnConfigCheckBox mailProposeToSubmitBox = null;
	/** 管理员label */
	private JLabel mailProposeToHandleLabel = null;
	/** 管理员label */
	private JLabel mailProposeToSubmitLabel = null;

	private JScrollPane jScrollPane = null;

	/*** 收藏文件更新邮件面板 */
	private JecnPanel favoritePanel = new JecnPanel();
	private JecnConfigCheckBox mailFavoriteBox = null;
	private JLabel mailFavoriteLabel = null;

	/*** 驱动规则更新邮件面板 */
	private JecnPanel driverPanel = new JecnPanel();
	private JecnConfigCheckBox mailDriverBox = null;
	private JLabel mailDriverLabel = null;
	private JecnConfigCheckBox tipMailDriverBox = null;
	private JLabel tipMailDriverLabel = null;

	/*** 有效期过期提醒 **/
	/*** 提醒时间间隔(日)： */
	private JLabel timeAdvanceExpiryTipLab = null;
	private JLabel timeFrequencyExpiryTipLab = null;
	/*** 提醒时间间隔(日)：选中启用，不选中不启用 */
	private JecnConfigCheckBox timeExpiryTipCheckBox = new JecnConfigCheckBox(JecnProperties
			.getValue("task_isAutoSave"));
	private JecnConfigTextField timeAdvanceExpiryTipField = new JecnConfigTextField();
	private JecnConfigTextField timeFrequencyExpiryTipField = new JecnConfigTextField();
	private JecnUserInfoTextArea timeAdvanceExpiryTipInfoArea = new JecnUserInfoTextArea();
	private JecnUserInfoTextArea timeFrequencyExpiryTipInfoArea = new JecnUserInfoTextArea();
	/** 流程文件有效期面板 **/
	private JecnPanel expiryPanel = new JecnPanel();

	public JecnMesEmailSetPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	private void initComponents() {

		jScrollPane = new JScrollPane();

		flowValidPanel = new JecnPanel();
		// 流程有效期邮件提醒
		flowValidPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("flowEndDateSendMail")));
		flowValidPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		ruleValidPanel = new JecnPanel();
		// 流程有效期邮件提醒
		ruleValidPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("mailRuleRemind")));
		ruleValidPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 邮件
		mailFlowFictionBox = new JecnConfigCheckBox(strMail);
		mailFlowGuardianBox = new JecnConfigCheckBox(strMail);
		mailFlowRefRationBox = new JecnConfigCheckBox(strMail);
		mailFlowSystemManagerBox = new JecnConfigCheckBox(strMail);

		mailRuleGuardianBox = new JecnConfigCheckBox(strMail);
		mailRuleRefRationBox = new JecnConfigCheckBox(strMail);
		mailRuleSystemManagerBox = new JecnConfigCheckBox(strMail);

		// 流程拟制人 ：
		flowFictionJLabel = new JLabel(JecnProperties.getValue("fictionPeopleC"));
		// 流程监护人：
		flowGuardianJLabel = new JLabel(JecnProperties.getValue("flowGuardianPeopleC"));
		// "流程责任人 ："
		flowRefRationJLabel = new JLabel(JecnProperties.getValue("flowResponsePerC"));
		// "系统管理员 ："
		flowSystemManagerJLabel = new JLabel(JecnProperties.getValue("systemMangePersonC"));

		// 制度监护人：
		ruleGuardianJLabel = new JLabel(JecnProperties.getValue("RuleGuardianPeopleC"));
		// "制度责任人 ："
		ruleRefRationJLabel = new JLabel(JecnProperties.getValue("ruleResponsiblepersonC"));
		// "系统管理员 ："
		ruleSystemManagerJLabel = new JLabel(JecnProperties.getValue("systemMangePersonC"));

		// 提醒时间间隔(日)
		approvalLessEmailMesTipPanel = new JecnPanel();
		timeTipLab = new JLabel(JecnProperties.getValue("timeTipC"));
		timeTipCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("task_isAutoSave"));
		timeTipCheckBox.setSelected(false);
		timeTipCheckBox.setOpaque(false);
		timeTipCheckBox.addActionListener(this);
		timeTipField.setEditable(false);
		timeTipInfoArea.setVisible(false);
		timeTipField.getDocument().addDocumentListener(new JecnDocumentListener(this, timeTipField, timeTipInfoArea));

		emailApproverCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		emailDraftPeopleCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		emailAddApproverCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		emailPublishCompetenceCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		emailPublishApproverCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		emailPublishDraftPeoCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		emailPublishParticipantsCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		timeExpiryTipCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 默认不选中
		emailApproverCheckBox.setSelected(false);
		emailDraftPeopleCheckBox.setSelected(false);
		emailAddApproverCheckBox.setSelected(false);
		emailPublishCompetenceCheckBox.setSelected(false);
		emailPublishApproverCheckBox.setSelected(false);
		emailPublishDraftPeoCheckBox.setSelected(false);
		emailPublishParticipantsCheckBox.setSelected(false);
		timeExpiryTipCheckBox.setSelected(false);

		emailApproverCheckBox.setOpaque(false);
		emailDraftPeopleCheckBox.setOpaque(false);
		emailAddApproverCheckBox.setOpaque(false);
		emailPublishCompetenceCheckBox.setOpaque(false);
		emailPublishApproverCheckBox.setOpaque(false);
		emailPublishDraftPeoCheckBox.setOpaque(false);
		emailPublishParticipantsCheckBox.setOpaque(false);
		timeExpiryTipCheckBox.setOpaque(false);

		emailApproverCheckBox.addActionListener(this);
		emailDraftPeopleCheckBox.addActionListener(this);
		emailAddApproverCheckBox.addActionListener(this);
		emailPublishCompetenceCheckBox.addActionListener(this);
		emailPublishApproverCheckBox.addActionListener(this);
		emailPublishDraftPeoCheckBox.addActionListener(this);
		emailPublishParticipantsCheckBox.addActionListener(this);
		timeExpiryTipCheckBox.addActionListener(this);

		/************* 流程有效期提醒 *******************/
		mailFlowFictionBox.setSelected(false);
		mailFlowGuardianBox.setSelected(false);
		mailFlowRefRationBox.setSelected(false);
		mailFlowSystemManagerBox.setSelected(false);

		/************* 制度有效期提醒 *******************/
		mailRuleGuardianBox.setSelected(false);
		mailRuleRefRationBox.setSelected(false);
		mailRuleSystemManagerBox.setSelected(false);

		mailFlowFictionBox.setOpaque(false);
		mailFlowGuardianBox.setOpaque(false);
		mailFlowRefRationBox.setOpaque(false);
		mailFlowSystemManagerBox.setOpaque(false);

		mailRuleGuardianBox.setOpaque(false);
		mailRuleRefRationBox.setOpaque(false);
		mailRuleSystemManagerBox.setOpaque(false);

		mailFlowFictionBox.addActionListener(this);
		mailFlowGuardianBox.addActionListener(this);
		mailFlowRefRationBox.addActionListener(this);
		mailFlowSystemManagerBox.addActionListener(this);

		mailRuleGuardianBox.addActionListener(this);
		mailRuleRefRationBox.addActionListener(this);
		mailRuleSystemManagerBox.addActionListener(this);

		// 任务审批超时提醒
		mailTaskApproveTipCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mailTaskApproveTipCheckBox.setOpaque(false);
		mailTaskApproveTipCheckBox.addActionListener(this);
		mailTaskApproveTipCheckBox.setSelected(false);
		mailTaskApproveTipField.setEditable(false);
		mailTaskApproveTipInfoArea.setVisible(false);
		mailTaskApproveTipField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, mailTaskApproveTipField, mailTaskApproveTipInfoArea));

		// 流程架构/流程/制度/文件发布发送邮件提醒
		mailPubToAdminBox = new JecnConfigCheckBox(strMail);
		mailPubToFixedBox = new JecnConfigCheckBox(strMail);

		mailPubToAdminBox.addActionListener(this);
		mailPubToFixedBox.addActionListener(this);

		mailPubToAdminBox.setSelected(false);
		mailPubToFixedBox.setSelected(false);

		mailPubToAdminBox.setOpaque(false);
		mailPubToFixedBox.setOpaque(false);

		mailPubToAdminBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mailPubToFixedBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		fixedPeopleLab = new JLabel(JecnProperties.getValue("pointReceivePeopleC"));

		// 合理化建议邮件
		mailProposeToHandleBox = new JecnConfigCheckBox(strMail);
		mailProposeToSubmitBox = new JecnConfigCheckBox(strMail);

		mailProposeToHandleBox.addActionListener(this);
		mailProposeToSubmitBox.addActionListener(this);

		mailProposeToHandleBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mailProposeToSubmitBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		mailProposeToHandleLabel = new JLabel(JecnProperties.getValue("submitProposals"));
		mailProposeToSubmitLabel = new JLabel(JecnProperties.getValue("rationProposals"));

		mailFavoriteBox = new JecnConfigCheckBox(strMail);
		mailFavoriteLabel = new JLabel(JecnProperties.getValue("fileCollectionPeopleC"));
		mailFavoriteBox.addActionListener(this);
		mailFavoriteBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		mailDriverBox = new JecnConfigCheckBox(strMail);
		mailDriverLabel = new JLabel(JecnProperties.getValue("roleRelevantPeopleC"));
		mailDriverBox.addActionListener(this);
		mailDriverBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		tipMailDriverBox = new JecnConfigCheckBox(strMail);
		tipMailDriverLabel = new JLabel(JecnProperties.getValue("driverTipPeopleC"));
		tipMailDriverBox.addActionListener(this);
		tipMailDriverBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 提前xx天，每隔xx天
		timeAdvanceExpiryTipLab = new JLabel(JecnProperties.getValue("advanceDay"));
		timeFrequencyExpiryTipLab = new JLabel(JecnProperties.getValue("frequencyDay"));
		/*** 提醒时间间隔(日)：选中启用，不选中不启用 */
		timeAdvanceExpiryTipField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, timeAdvanceExpiryTipField, timeAdvanceExpiryTipInfoArea));
		timeFrequencyExpiryTipField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, timeFrequencyExpiryTipField, timeFrequencyExpiryTipInfoArea));

	}

	/***************************************************************************
	 * 布局
	 */
	private void initLayout() {

		jScrollPane.setViewportView(mainPanel);
		this.add(jScrollPane);

		Insets insets5555 = new Insets(0, 5, 0, 5);
		mainPanel.setLayout(new GridBagLayout());
		// 主面板
		// mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		// mainPanel.setBorder(BorderFactory.createTitledBorder("审批任务"));
		int count = 0;
		// 创建任务
		addCreateTaskPanel(c, insets5555, count);
		count++;
		// 审批任务
		addApproverTaskPanel(c, insets5555, count);
		count++;
		// 发布任务
		addPublishTaskPanel(c, insets5555, count);
		count++;
		// 流程有效期面板初始化
		addFlowValidPanel(c, insets5555, count);
		count++;
		// 制度有效期面板初始化
		addRuleValidPanel(c, insets5555, count);
		count++;
		// 审批人员缺失邮件提醒Panel
		addApprovalLessEmailMesTipPanel(c, insets5555, count);
		count++;
		// 发布发送邮件
		addPubMailPanel(c, insets5555, count);
		count++;
		// 合理化建议邮件配置
		addProposePanel(c, insets5555, count);
		count++;
		// 流程驱动提醒
		addDriverTipPanel(c, insets5555, count);
		count++;
		// 收藏更新提醒
		addFavoriteUpdatePanel(c, insets5555, count);
		count++;
		// 有效期提醒
		addExpiryPanel(c, insets5555, count);

		// 空白
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(new JLabel(), c);
	}

	private void addExpiryPanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(expiryPanel, c);
		expiryPanel.setLayout(new GridBagLayout());
		expiryPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("expiryTip")));

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		expiryPanel.add(timeExpiryTipCheckBox, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		expiryPanel.add(timeAdvanceExpiryTipLab, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		expiryPanel.add(timeAdvanceExpiryTipField, c);
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		expiryPanel.add(timeFrequencyExpiryTipLab, c);
		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		expiryPanel.add(timeFrequencyExpiryTipField, c);
		// 提示框
		c = new GridBagConstraints(1, 1, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		expiryPanel.add(timeAdvanceExpiryTipInfoArea, c);
		c = new GridBagConstraints(3, 1, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		expiryPanel.add(timeFrequencyExpiryTipInfoArea, c);

	}

	private void addFavoriteUpdatePanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(favoritePanel, c);
		favoritePanel.setLayout(new GridBagLayout());
		favoritePanel
				.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("CollectionFileUpdateRemind")));

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		favoritePanel.add(mailFavoriteLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		favoritePanel.add(mailFavoriteBox, c);

	}

	private void addDriverTipPanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(driverPanel, c);
		driverPanel.setLayout(new GridBagLayout());
		driverPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("flowDriveRemind")));

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		driverPanel.add(mailDriverLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		driverPanel.add(mailDriverBox, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		driverPanel.add(tipMailDriverLabel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		driverPanel.add(tipMailDriverBox, c);

	}

	private void addProposePanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(rtnlProposePanel, c);
		rtnlProposePanel.setLayout(new GridBagLayout());
		rtnlProposePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("proposeEmailTip")));

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		rtnlProposePanel.add(mailProposeToHandleLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		rtnlProposePanel.add(mailProposeToHandleBox, c);

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		rtnlProposePanel.add(mailProposeToSubmitLabel, c);
		c = new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		rtnlProposePanel.add(mailProposeToSubmitBox, c);

	}

	/**
	 * 创建任务，消息邮件配置
	 * 
	 * @param c
	 * @param insets5555
	 * @param count
	 */
	private void addCreateTaskPanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(createTaskPanel, c);
		createTaskPanel.setLayout(new GridBagLayout());
		// 创建任务，邮件配置
		createTaskPanel
				.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("createTaskMessageMailSys")));

		// 创建任务：审批人Lab
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		createTaskPanel.add(addApproverLab, c);

		// 创建任务：审批人邮件
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		createTaskPanel.add(emailAddApproverCheckBox, c);
	}

	/**
	 * 任务审批过程，各阶段审批配置
	 * 
	 * @param c
	 * @param insets5555
	 */
	private void addApproverTaskPanel(GridBagConstraints c, Insets insets5555, int count) {
		// 审批任务面板
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(approverTaskPanel, c);

		approverTaskPanel.setLayout(new GridBagLayout());
		// 审批任务 TASK_APPING
		approverTaskPanel.setBorder(BorderFactory.createTitledBorder(TASK_APPING));
		// 审批任务：审批人Lab
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		approverTaskPanel.add(approverLab, c);

		// 审批任务：审批人邮件

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		approverTaskPanel.add(emailApproverCheckBox, c);

		// 审批任务：拟稿人Lab
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		approverTaskPanel.add(draftPeopleLab, c);

		// 审批任务：拟稿人邮件

		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		approverTaskPanel.add(emailDraftPeopleCheckBox, c);
		// 审批任务：超时提醒Lab
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		approverTaskPanel.add(mailTaskApproveTipLab, c);
		// 审批任务：超时提醒启用CheckBox
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		approverTaskPanel.add(mailTaskApproveTipCheckBox, c);
		// 审批任务：超时提醒Field
		c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 8, 0, 5), 0, 0);
		approverTaskPanel.add(mailTaskApproveTipField, c);
		// 审批任务：输入错误提示信息Area
		c = new GridBagConstraints(0, 3, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		approverTaskPanel.add(mailTaskApproveTipInfoArea, c);
	}

	/**
	 * 发布任务
	 * 
	 * @param c
	 * @param insets5555
	 * @param count
	 */
	private void addPublishTaskPanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(publishTaskPanel, c);
		publishTaskPanel.setLayout(new GridBagLayout());
		publishTaskPanel.setBorder(BorderFactory.createTitledBorder(TASK_FINISH));

		// 发布任务：审批人Lab
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		publishTaskPanel.add(publishApproverLab, c);

		// 发布任务：审批人--邮件
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		publishTaskPanel.add(emailPublishApproverCheckBox, c);

		// 发布任务：拟稿人Lab
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		publishTaskPanel.add(publishDraftPeoLab, c);

		// 发布任务：拟稿人--邮件
		c = new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		publishTaskPanel.add(emailPublishDraftPeoCheckBox, c);

	}

	private void addFlowValidPanel(GridBagConstraints c, Insets insets5555, int count) {
		// 流程有效期邮件提醒
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(flowValidPanel, c);

		flowValidPanel.setLayout(new GridBagLayout());

		// 流程拟制人
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		flowValidPanel.add(flowFictionJLabel, c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		flowValidPanel.add(mailFlowFictionBox, c);

		// 流程监护人
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		flowValidPanel.add(flowGuardianJLabel, c);

		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		flowValidPanel.add(mailFlowGuardianBox, c);

		// 流程责任人
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		flowValidPanel.add(flowRefRationJLabel, c);

		c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		flowValidPanel.add(mailFlowRefRationBox, c);

		// 系统管理员
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		flowValidPanel.add(flowSystemManagerJLabel, c);

		c = new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		flowValidPanel.add(mailFlowSystemManagerBox, c);

	}

	private void addRuleValidPanel(GridBagConstraints c, Insets insets5555, int count) {
		// 制度有效期邮件提醒
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(ruleValidPanel, c);

		ruleValidPanel.setLayout(new GridBagLayout());

		// 制度监护人
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		ruleValidPanel.add(ruleGuardianJLabel, c);

		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		ruleValidPanel.add(mailRuleGuardianBox, c);

		// 制度责任人
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		ruleValidPanel.add(ruleRefRationJLabel, c);

		c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		ruleValidPanel.add(mailRuleRefRationBox, c);

		// 系统管理员
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		ruleValidPanel.add(ruleSystemManagerJLabel, c);

		c = new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets5555,
				0, 0);
		ruleValidPanel.add(mailRuleSystemManagerBox, c);

	}

	private void addApprovalLessEmailMesTipPanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(approvalLessEmailMesTipPanel, c);
		approvalLessEmailMesTipPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("approvalLessEmailMesTip")));
		// *************提醒时间间隔(日)Start*********
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		approvalLessEmailMesTipPanel.add(timeTipLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		approvalLessEmailMesTipPanel.add(timeTipCheckBox, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		approvalLessEmailMesTipPanel.add(timeTipField, c);
		c = new GridBagConstraints(0, 1, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		approvalLessEmailMesTipPanel.add(timeTipInfoArea, c);
	}

	/**
	 * 发布邮件配置
	 * 
	 * @param c
	 * @param insets5555
	 * @param count
	 */
	private void addPubMailPanel(GridBagConstraints c, Insets insets5555, int count) {
		c = new GridBagConstraints(0, count, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		mainPanel.add(pubMailPanel, c);
		pubMailPanel.setLayout(new GridBagLayout());
		pubMailPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("pubMailTip")));
		// 给admin发送邮件的label
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		pubMailPanel.add(new JLabel(JecnProperties.getValue("systemMangePersonC")), c);
		// 给admin复选框
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		pubMailPanel.add(mailPubToAdminBox, c);
		// 给固定收件人label
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		pubMailPanel.add(fixedPeopleLab, c);
		// 给固定收件人复选框
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		pubMailPanel.add(mailPubToFixedBox, c);

		// 发布任务：具有查阅权限的人Lab
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		pubMailPanel.add(publishCompetenceLab, c);

		// 发布任务：具有查阅权限的人--邮件
		c = new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		pubMailPanel.add(emailPublishCompetenceCheckBox, c);

		// 发布任务：参与人Lab
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 1, 0, 1), 0, 0);
		pubMailPanel.add(publishParticipantsLab, c);

		// 发布任务：参与人--邮件
		c = new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 1, 0, 1), 0, 0);
		pubMailPanel.add(emailPublishParticipantsCheckBox, c);
	}

	@Override
	public boolean check() {
		// 如果未启用任务审批提醒则不进行验证
		if (!mailTaskApproveTipCheckBox.isSelected() && !timeTipCheckBox.isSelected()
				&& !timeExpiryTipCheckBox.isSelected()) {
			return true;
		}
		boolean isMatch = true;
		String errorInfo = null;
		// 超时未审批任务提醒
		if (mailTaskApproveTipCheckBox.isSelected()) {
			errorInfo = JecnProperties.getValue("taskApproveTipDayErrorInfo");
			isMatch &= getIsFlag(mailTaskApproveTipField, mailTaskApproveTipInfoArea, errorInfo);
		}
		// 提醒时间间隔(日)
		if (timeTipCheckBox.isSelected()) {
			errorInfo = JecnProperties.getValue("timeTipErrorInfo");
			isMatch &= getIsFlag(timeTipField, timeTipInfoArea, errorInfo);
		}
		// 有效期提醒
		if (timeExpiryTipCheckBox.isSelected()) {
			errorInfo = JecnProperties.getValue("timeTipErrorInfo");
			boolean a = getIsFlag(timeAdvanceExpiryTipField, timeAdvanceExpiryTipInfoArea, errorInfo);
			boolean b = getIsFlag(timeFrequencyExpiryTipField, timeFrequencyExpiryTipInfoArea, errorInfo);
			boolean c = a & b;
			if (c) {
				int i = Integer.valueOf(timeAdvanceExpiryTipField.getText());
				int j = Integer.valueOf(timeFrequencyExpiryTipField.getText());
				if (i < j) {
					c = false;
					timeAdvanceExpiryTipInfoArea.setText(timeAdvanceExpiryTipLab.getText()
							+ JecnProperties.getValue("bigEqualThan") + timeFrequencyExpiryTipLab.getText());
					timeAdvanceExpiryTipInfoArea.setVisible(true);
				} else {
					timeAdvanceExpiryTipInfoArea.setVisible(false);
				}
			}
			isMatch &= c;
		}
		return isMatch;
	}

	/**
	 * 验证输入框内容
	 * 
	 * @param configTipField
	 *            输入框
	 * @param userInfoTipInfoArea
	 *            提示信息显示框
	 * @param errorInfo提示信息内容
	 * @return
	 */
	private boolean getIsFlag(JecnConfigTextField configTipField, JecnUserInfoTextArea userInfoTipInfoArea,
			String errorInfo) {
		final String regex = "^[0-9]{1,2}$";
		String value = null;
		value = configTipField.getText();
		boolean isMatch = true;
		// 过滤非1-2位数字情况
		if (!value.matches(regex)) {
			isMatch = false;
		}
		// 过滤用户输入空值0或00的情况
		if (isMatch && Integer.valueOf(value) == 0) {
			isMatch = false;
		}
		if (isMatch) {
			value = Integer.valueOf(value).toString(); // 去除0开头的数字（01-->1）
			configTipField.getItemBean().setValue(value);
		} else {
			userInfoTipInfoArea.setText(errorInfo);
		}
		// 空值OkButton的Enable
		changeOkButEnableValue(isMatch, userInfoTipInfoArea);
		return isMatch;
	}

	/**
	 * 控制错误信息和Ok按钮的状态（隐藏和禁用）
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-14 时间：下午03:12:19
	 * @param isMatch
	 *            true-->错误信息隐藏，按钮可用 false-->错误信息显示，按钮不可用
	 */
	private void changeOkButEnableValue(boolean isMatch, JecnUserInfoTextArea userInfoTip) {
		JButton okBut = dialog.getOkCancelJButtonPanel().getOkJButton();
		if (isMatch) {
			userInfoTip.setVisible(false);
			okBut.setEnabled(true);
		} else {
			if (userInfoTip != null) {
				userInfoTip.setVisible(true);
				okBut.setEnabled(false);
			}
		}
	}

	/***************************************************************************
	 * 初始化数据
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {

		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			String mark = itemBean.getMark();
			// 值1:选中，0：未选中
			if (ConfigItemPartMapMark.emailApprover.toString().equals(mark)) { // 审批任务：审批人--邮件
				initCheckBox(itemBean, emailApproverCheckBox);
			} else if (ConfigItemPartMapMark.emailDraftPeople.toString()// 审批任务：拟稿人--邮件
					.equals(mark)) {
				initCheckBox(itemBean, emailDraftPeopleCheckBox);
			} else if (ConfigItemPartMapMark.emailAddApprover.toString().equals(mark)) {// 创建任务：审批人--邮件
				initCheckBox(itemBean, emailAddApproverCheckBox);
			} else if (ConfigItemPartMapMark.emailPulishCompetence.toString().equals(mark)) {// 发布任务：具有查阅权限的人--邮件
				initCheckBox(itemBean, emailPublishCompetenceCheckBox);
			} else if (ConfigItemPartMapMark.emailPulishApprover.toString().equals(mark)) {// 发布任务：审批人--邮件
				initCheckBox(itemBean, emailPublishApproverCheckBox);
			} else if (ConfigItemPartMapMark.emailPulishDraftPeople.toString().equals(mark)) { // 发布任务：拟稿人--邮件
				initCheckBox(itemBean, emailPublishDraftPeoCheckBox);
			} else if (ConfigItemPartMapMark.emailPulishParticipants.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, emailPublishParticipantsCheckBox);
			} else if (ConfigItemPartMapMark.mailFlowFictionPeople.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, mailFlowFictionBox);
			} else if (ConfigItemPartMapMark.mailFlowGuardianPeople.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, mailFlowGuardianBox);
			} else if (ConfigItemPartMapMark.mailFlowRefRationPeople.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, mailFlowRefRationBox);
			} else if (ConfigItemPartMapMark.mailSystemManager.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, mailFlowSystemManagerBox);
			} else if (ConfigItemPartMapMark.mailTaskApproveTip.toString().equals(mark)) {// 审核任务：是否启用超时提醒--邮件
				initCheckBox(itemBean, mailTaskApproveTipCheckBox);
				// 是否启用对应文本框是否可编辑
				mailTaskApproveTipField.setEditable(mailTaskApproveTipCheckBox.isSelected());
			} else if (ConfigItemPartMapMark.mailTaskApproveTipValue.toString().equals(mark)) {// 审核任务：超时提醒Field--邮件
				mailTaskApproveTipField.setItemBean(itemBean);
				// 默认值时不显示0
				if ("0".equals(itemBean.getValue())) {
					mailTaskApproveTipField.setText("");
				} else {
					mailTaskApproveTipField.setText(itemBean.getValue());
				}
			} else if (ConfigItemPartMapMark.proposalTaskTip.toString().equals(mark)) {// 审核任务：是否启用超时提醒--邮件
				initCheckBox(itemBean, timeTipCheckBox);
				// 是否启用对应文本框是否可编辑
				timeTipField.setEditable(timeTipCheckBox.isSelected());
			} else if (ConfigItemPartMapMark.proposalTaskTipValue.toString().equals(mark)) {// 
				timeTipField.setItemBean(itemBean);
				// 默认值时不显示0
				if ("0".equals(itemBean.getValue())) {
					timeTipField.setText("");
				} else {
					timeTipField.setText(itemBean.getValue());
				}
			} else if (ConfigItemPartMapMark.mailPubToAdmin.toString().equals(mark)) {// 发布-给管理员发送邮件
				initCheckBox(itemBean, mailPubToAdminBox);
				// 是否启用对应文本框是否可编辑
				mailTaskApproveTipField.setEditable(mailPubToAdminBox.isSelected());
			} else if (ConfigItemPartMapMark.mailPubToFixed.toString().equals(mark)) {// 发布-给指定人员发送邮件
				initCheckBox(itemBean, mailPubToFixedBox);
				// 是否启用对应文本框是否可编辑
				mailTaskApproveTipField.setEditable(mailPubToFixedBox.isSelected());
			} else if (ConfigItemPartMapMark.mailProposeToHandle.toString().equals(mark)) {
				initCheckBox(itemBean, mailProposeToHandleBox);
			} else if (ConfigItemPartMapMark.mailProposeToSubmit.toString().equals(mark)) {
				initCheckBox(itemBean, mailProposeToSubmitBox);
			} else if (ConfigItemPartMapMark.mailFlowDriverTip.toString().equals(mark)) {
				initCheckBox(itemBean, mailDriverBox);
			} else if (ConfigItemPartMapMark.mailFlowDriverProcessFileTip.toString().equals(mark)) {
				initCheckBox(itemBean, tipMailDriverBox);
			} else if (ConfigItemPartMapMark.mailFavoriteUpdate.toString().equals(mark)) {
				initCheckBox(itemBean, mailFavoriteBox);
			} else if (ConfigItemPartMapMark.mailRuleGuardianPeople.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, mailRuleGuardianBox);
			} else if (ConfigItemPartMapMark.mailRuleRefRationPeople.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, mailRuleRefRationBox);
			} else if (ConfigItemPartMapMark.mailRuleSystemManager.toString().equals(mark)) {// 发布任务：参与人--邮件
				initCheckBox(itemBean, mailRuleSystemManagerBox);
			} else if (ConfigItemPartMapMark.mailExpiryTip.toString().equals(mark)) {// 有效期提醒
				initCheckBox(itemBean, timeExpiryTipCheckBox);
				timeAdvanceExpiryTipField.setEditable(mailPubToFixedBox.isSelected());
				timeFrequencyExpiryTipField.setEditable(mailPubToFixedBox.isSelected());
			} else if (ConfigItemPartMapMark.mailExpiryAdvanceTip.toString().equals(mark)) {
				timeAdvanceExpiryTipField.setItemBean(itemBean);
				// 默认值时不显示0
				if ("0".equals(itemBean.getValue())) {
					timeAdvanceExpiryTipField.setText("");
				} else {
					timeAdvanceExpiryTipField.setText(itemBean.getValue());
				}
			} else if (ConfigItemPartMapMark.mailExpiryFrequencyTip.toString().equals(mark)) {
				timeFrequencyExpiryTipField.setItemBean(itemBean);
				// 默认值时不显示0
				if ("0".equals(itemBean.getValue())) {
					timeFrequencyExpiryTipField.setText("");
				} else {
					timeFrequencyExpiryTipField.setText(itemBean.getValue());
				}
			}
		}

	}

	/**
	 * 初始化复选框
	 * 
	 * @param itemBean
	 * @param checkBox
	 */
	private void initCheckBox(JecnConfigItemBean itemBean, JecnConfigCheckBox checkBox) {
		if (itemBean.getValue() != null && "1".equals(itemBean.getValue())) {
			checkBox.setSelected(true);
		} else {
			checkBox.setSelected(false);
		}
		checkBox.setItemBean(itemBean);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			JecnConfigCheckBox checkBox = (JecnConfigCheckBox) e.getSource();
			if (checkBox.getItemBean() == null) {
				throw new NullPointerException();
			}

			if (checkBox.isSelected()) {// 选中
				checkBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
			} else {
				checkBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
			}
			// 超时提醒CheckBox
			if (checkBox == mailTaskApproveTipCheckBox) {
				if (checkBox.isSelected()) {
					mailTaskApproveTipCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);// 启用审批提醒
					mailTaskApproveTipField.setEditable(true);// 文本框允许编辑
					mailTaskApproveTipField.setText("1");// 设置默认值1天
				} else {
					mailTaskApproveTipCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);// 关闭审批提醒
					mailTaskApproveTipField.setEditable(false);
					mailTaskApproveTipField.setText("");
					mailTaskApproveTipField.getItemBean().setValue("0");// 恢复默认值0
					// 改变按钮状态
					changeOkButEnableValue(true, mailTaskApproveTipInfoArea);
				}
			} else if (checkBox == timeTipCheckBox) {
				if (checkBox.isSelected()) {
					timeTipCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);// 启用
					timeTipField.setEditable(true);// 文本框允许编辑
					timeTipField.setText("1");// 设置默认值1天
				} else {
					timeTipCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);// 不启用
					timeTipField.setEditable(false);
					timeTipField.setText("");
					timeTipField.getItemBean().setValue("0");// 恢复默认值0
					// 改变按钮状态
					changeOkButEnableValue(true, timeTipInfoArea);
				}
			} else if (checkBox == timeExpiryTipCheckBox) {
				if (checkBox.isSelected()) {
					timeAdvanceExpiryTipField.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					timeAdvanceExpiryTipField.setEditable(true);// 文本框允许编辑
					timeAdvanceExpiryTipField.setText("1");// 设置默认值1天

					timeFrequencyExpiryTipField.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					timeFrequencyExpiryTipField.setEditable(true);// 文本框允许编辑
					timeFrequencyExpiryTipField.setText("1");// 设置默认值1天
				} else {
					timeAdvanceExpiryTipField.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					timeAdvanceExpiryTipField.setEditable(false);
					timeAdvanceExpiryTipField.setText("");
					timeAdvanceExpiryTipField.getItemBean().setValue("0");// 恢复默认值0
					changeOkButEnableValue(true, timeAdvanceExpiryTipInfoArea);

					timeFrequencyExpiryTipField.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					timeFrequencyExpiryTipField.setEditable(false);
					timeFrequencyExpiryTipField.setText("");
					timeFrequencyExpiryTipField.getItemBean().setValue("0");// 恢复默认值0
					changeOkButEnableValue(true, timeFrequencyExpiryTipInfoArea);
				}
			}
		}

	}

}
