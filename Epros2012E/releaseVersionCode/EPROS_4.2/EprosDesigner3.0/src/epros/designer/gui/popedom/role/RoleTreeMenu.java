package epros.designer.gui.popedom.role;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.popedom.role.roleMove.RoleMoveNodeDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class RoleTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(RoleTreeMenu.class);
	/** 新建目录 */
	private JMenuItem dirMenu;
	/** 新建角色 */
	private JMenuItem addMenu;
	/** 更新角色 */
	private JMenuItem updateMenu;
	/** 重命名 */
	private JMenuItem reNameMenu;
	/** 删除 */
	private JMenuItem delMenu;
	/** 刷新 */
	private JMenuItem refurbishMenu;
	/** 节点移动 */
	private JMenuItem dragNode;
	/** 节点排序 */
	private JMenuItem nodeSort;

	/** 新建流程管理员 */
	private JMenuItem addSecondAdminMenu;

	private JecnTree jTree = null;

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;

	/** 角色对话框 */
	private RoleManageDialog jecnManageDialog = null;

	public RoleTreeMenu(JecnTree jTree, RoleManageDialog jecnManageDialog) {
		this.jTree = jTree;
		this.jecnManageDialog = jecnManageDialog;
		// "新建目录"
		dirMenu = new JMenuItem(JecnProperties.getValue("newDir"));
		// 新建角色
		addMenu = new JMenuItem(JecnProperties.getValue("newRole"));
		// 重命名
		reNameMenu = new JMenuItem(JecnProperties.getValue("rename"));
		// 删除
		delMenu = new JMenuItem(JecnProperties.getValue("delete"));
		// 刷新
		refurbishMenu = new JMenuItem(JecnProperties.getValue("refresh"));
		// 节点排序
		nodeSort = new JMenuItem(JecnProperties.getValue("nodeSort"));// 排序
		// 节点移动
		dragNode = new JMenuItem(JecnProperties.getValue("nodeMove"));
		// 更新角色
		updateMenu = new JMenuItem(JecnProperties.getValue("updateRole"));

		dirMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				dirMenuActionPerformed(evt);
			}
		});

		addMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				addMenuActionPerformed(evt);
			}
		});

		reNameMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				eidtMenuActionPerformed(evt);
			}
		});

		delMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delMenuActionPerformed(evt);
			}
		});

		updateMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateMenuActionPerformed(evt);
			}
		});

		refurbishMenu.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				refurbishMenuActionPerformed(evt);
			}
		});

		nodeSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				nodeSortActionPerformed(evt);
			}
		});
		dragNode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				dragNodeActionPerformed(evt);
			}
		});

		addSecondAdminMenu = new JMenuItem(JecnProperties.getValue("newFlowJurisdiction"));
		addSecondAdminMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addSecondAdminAction();
			}
		});
		this.add(dirMenu);
		this.add(addMenu);
		this.addSeparator();
		this.add(reNameMenu);
		this.add(delMenu);
		this.add(refurbishMenu);
		this.add(nodeSort);
		this.add(dragNode);
		this.addSeparator();
		this.add(updateMenu);
	}

	/**
	 * 初始化
	 */
	private void init() {
		dirMenu.setEnabled(false);
		addMenu.setEnabled(false);
		reNameMenu.setEnabled(false);
		delMenu.setEnabled(false);
		refurbishMenu.setEnabled(false);
		nodeSort.setEnabled(false);
		dragNode.setEnabled(false);
		updateMenu.setEnabled(false);
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		TreeNodeType nodeType = selectNode.getJecnTreeBean().getTreeNodeType();
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		switch (nodeType) {
		// 根节点
		case roleRoot:
			// 新建目录
			dirMenu.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 刷新
			refurbishMenu.setEnabled(true);
			// 新建角色
			addMenu.setEnabled(true);
			break;
		// 目录节点
		case roleDir:
			// 新建目录
			dirMenu.setEnabled(true);
			// 新建角色
			addMenu.setEnabled(true);
			// 重命名
			reNameMenu.setEnabled(true);
			// 删除
			delMenu.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 移动
			dragNode.setEnabled(true);
			// 刷新
			refurbishMenu.setEnabled(true);
			if (!JecnDesignerCommon.isAdmin() && isSecondAdminNode()) {// 流程管理员不允许删除自身节点，不允许排序
				delMenu.setEnabled(false);
				dragNode.setEnabled(false);
				reNameMenu.setEnabled(false);
			}
			break;
		// 角色节点
		case role:
			// 重命名
			reNameMenu.setEnabled(true);
			// 删除
			delMenu.setEnabled(true);

			// 移动
			dragNode.setEnabled(true);
			// 更新角色
			updateMenu.setEnabled(true);
			break;
		case roleSecondAdminDefaultDir:
			this.removeAll();
			this.add(addSecondAdminMenu);
			
			if (selectNode.getJecnTreeBean().getId() != -1) {
				this.add(dirMenu);
				this.add(refurbishMenu);
				this.add(addMenu);
				dirMenu.setEnabled(true);
				refurbishMenu.setEnabled(true);
				addMenu.setEnabled(true);
			}
			break;
		case roleSecondAdmin:
			// 重命名
			reNameMenu.setEnabled(true);
			// 删除
			delMenu.setEnabled(true);
			// 更新角色
			updateMenu.setEnabled(true);
			break;
		default:
			break;
		}

	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		// 删除
		delMenu.setEnabled(true);
		// 移动
		dragNode.setEnabled(true);
	}

	/**
	 * 刷新 按钮
	 */
	public void refurbishMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();

		try {
			List<JecnTreeBean> list = JecnRoleCommon.getRoleTreeBeans(selectNode);
			// 根节点
			if (id == 0) {
				List<JecnTreeBean> listDefault = JecnRoleCommon.getDefaultRoleTreeBeans();
				jTree.setModel(JecnRoleCommon.getTreeModel(listDefault, list));
			} else {
				JecnTreeCommon.refreshNode(selectNode, list, jTree);
			}
		} catch (Exception ex) {
			log.error("RoleTreeMenu refurbishMenuActionPerformed is error", ex);
		}
	}

	/**
	 * 节点移动
	 * 
	 * @param evt
	 */
	public void dragNodeActionPerformed(ActionEvent evt) {
		if (listNode.size() > 0) {
			// RoleNodeMoveDialog roleNodeMoveDialog = new RoleNodeMoveDialog(
			// listNode, jTree);
			// roleNodeMoveDialog.setFocusableWindowState(true);
			// roleNodeMoveDialog.setVisible(true);
			// 不能选择相同的角色角色名称
			if (!JecnDesignerCommon.validateRepeatNodesName(listNode)) {
				return;
			}
			RoleMoveNodeDialog roleMoveNodeDialog = new RoleMoveNodeDialog(listNode, jTree);
			roleMoveNodeDialog.setFocusableWindowState(true);
			roleMoveNodeDialog.setVisible(true);
		}
	}

	/**
	 * 节点排序
	 * 
	 * @param evt
	 */
	public void nodeSortActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		RoleSortDialog roleSortDialog = new RoleSortDialog(selectNode, jTree);
		roleSortDialog.setVisible(true);

	}

	/**
	 * 添加目录
	 * 
	 * @param evt
	 */
	public void dirMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		AddRoleDirNameDialog addRoleDirNameDialog = new AddRoleDirNameDialog(selectNode, jTree);
		addRoleDirNameDialog.setVisible(true);
	}

	/**
	 * 添加角色
	 * 
	 * @param evt
	 */
	public void addMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		AddRoleDialog addRoleDialog = new AddRoleDialog(selectNode, jTree);
		addRoleDialog.setVisible(true);
	}

	/**
	 * 重命名
	 * 
	 * @param evt
	 */
	public void eidtMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		EditRoleDirDialog editRoleDirDialog = new EditRoleDirDialog(selectNode, jTree);
		editRoleDirDialog.setVisible(true);
	}

	/**
	 * 删除
	 * 
	 * @param evt
	 */
	public void delMenuActionPerformed(ActionEvent evt) {
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		// if (JecnConstants.isMysql) {
		// listIds = JecnTreeCommon.getAllChildIds(listNode);
		// } else {
		for (JecnTreeNode node : listNode) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		// }
		if (listIds.size() == 0) {
			return;
		}

		try {
			ConnectionPool.getJecnRole().deleteRoles(listIds, JecnConstants.projectId, JecnConstants.getUserId());
			/** 删除节点 */
			JecnTreeCommon.removeNodes(jTree, listNode);
			/**
			 * 删除搜索表格中的数据
			 */
			// 要删除的数据名称
			Vector<Vector<Object>> vs = ((DefaultTableModel) jecnManageDialog.getJecnTable().getModel())
					.getDataVector();
			for (int i = vs.size() - 1; i >= 0; i--) {
				Vector<Object> vob = vs.get(i);
				for (Long longId : listIds) {
					if (vob.get(1).toString().equals(longId.toString())) {
						((DefaultTableModel) jecnManageDialog.getJecnTable().getModel()).removeRow(i);
					}
				}
			}
		} catch (Exception e) {
			log.error("RoleTreeMenu delMenuActionPerformed is error", e);
		}

	}

	/**
	 * 更新角色
	 * 
	 * @param evt
	 */
	public void updateMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		RoleDialog roleDialog = null;
		JecnTreeNode pNode = (JecnTreeNode) selectNode.getParent();
		if (pNode != null && pNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.roleSecondAdminDefaultDir) {// 流程管理员
			roleDialog = new SecondAdminRoleDialog(selectNode, jTree);
		} else {
			roleDialog = new EditRoleDialog(selectNode.getJecnTreeBean(), jTree);
		}
		roleDialog.setVisible(true);
		if (roleDialog.isOperation()) {
			JecnTreeCommon.reNameNode(jTree, selectNode, roleDialog.getRoleName());
			JecnTreeCommon.selectNode(jTree, selectNode);
			// 提示更新成功
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("updateSuccess"));
		}

	}

	public void setListNode(List<JecnTreeNode> listNode) {
		this.listNode = listNode;
		if (listNode.size() == 1) {
			this.selectNode = listNode.get(0);
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

	protected void addSecondAdminAction() {
		SecondAdminRoleDialog secondAdminRoleDialog = new SecondAdminRoleDialog(selectNode, jTree);
		secondAdminRoleDialog.setVisible(true);
	}

	/**
	 * 流程管理员节点
	 * 
	 * @return
	 */
	protected boolean isSecondAdminNode() {
		return "secondAdmin".equals(this.selectNode.getJecnTreeBean().getNumberId());
	}
}
