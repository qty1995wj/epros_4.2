package com.jecn.epros.server.dao.popedom.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnAccessPermissionsT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class PositionAccessPermissionsDaoImpl extends AbsBaseDao<JecnAccessPermissionsT, Long> implements
		IPositionAccessPermissionsDao {

	/**
	 * 设置单一节点部门查阅权限（oracle数据库）
	 * 
	 * @param relateId
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 * @param isModifyPub
	 *            true 是连同发布一起修改
	 */
	private void setNodeAuths(List<Long> list, int type, String posIds, boolean isModifyPub) {
		if (isModifyPub) {
			String hql = "delete from JecnAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
		}
		if (posIds == null || "".equals(posIds)) {
			String hql = "delete from JecnAccessPermissionsT where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
			return;
		}

		List<JecnAccessPermissionsT> listOldT = new ArrayList<JecnAccessPermissionsT>();
		String hql = "from JecnAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		for (String str : listStr) {
			List<JecnAccessPermissionsT> listJecnAccessPermissionsT = this.listHql(str, type);
			for (JecnAccessPermissionsT jecnAccessPermissionsT : listJecnAccessPermissionsT) {
				listOldT.add(jecnAccessPermissionsT);
			}
		}
		String[] idsArr = posIds.split(",");
		for (Long relateId : list) {
			for (String str : idsArr) {
				if (str == null || "".equals(str)) {
					continue;
				}
				Long posId = Long.valueOf(str);
				boolean isExist = false;
				for (JecnAccessPermissionsT jecnAccessPermissionsT : listOldT) {
					if (relateId.equals(jecnAccessPermissionsT.getRelateId())
							&& posId.equals(jecnAccessPermissionsT.getFigureId())
							&& jecnAccessPermissionsT.getType().intValue() == type) {
						isExist = true;
						listOldT.remove(jecnAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
					jecnAccessPermissionsT.setRelateId(relateId);
					jecnAccessPermissionsT.setType(type);
					jecnAccessPermissionsT.setFigureId(posId);
					this.save(jecnAccessPermissionsT);
				}
			}
		}
		for (JecnAccessPermissionsT jecnAccessPermissionsT : listOldT) {
			this.deleteObject(jecnAccessPermissionsT);
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "insert into jecn_access_permissions (id, FIGURE_ID, relate_id, type)  select id, FIGURE_ID, relate_id, type from jecn_access_permissions_t where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, list);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}

	}

	
	/**
	 * 设置单一节点部门查阅权限（oracle数据库）
	 * 
	 * @param relateId
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 * @param isModifyPub
	 *            true 是连同发布一起修改
	 */
	private void setNodeAuths(List<Long> list, int type, List<AccessId> posIds, boolean isModifyPub) {
		if (isModifyPub) {
			String hql = "delete from JecnAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
		}
		if (posIds == null || posIds.isEmpty()) {
			String hql = "delete from JecnAccessPermissionsT where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
			return;
		}

		List<JecnAccessPermissionsT> listOldT = new ArrayList<JecnAccessPermissionsT>();
		String hql = "from JecnAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		for (String str : listStr) {
			List<JecnAccessPermissionsT> listJecnAccessPermissionsT = this.listHql(str, type);
			for (JecnAccessPermissionsT jecnAccessPermissionsT : listJecnAccessPermissionsT) {
				listOldT.add(jecnAccessPermissionsT);
			}
		}
		for (Long relateId : list) {
			for (AccessId ids : posIds) {
				if (ids == null || ids.getAccessId() == null) {
					continue;
				}
				boolean isExist = false;
				for (JecnAccessPermissionsT jecnAccessPermissionsT : listOldT) {
					if (relateId.equals(jecnAccessPermissionsT.getRelateId())
							&& ids.getAccessId().equals(jecnAccessPermissionsT.getFigureId())
							&& jecnAccessPermissionsT.getType().intValue() == type && jecnAccessPermissionsT.getAccessType().intValue() == (ids.isDownLoad() ? 1 : 0)) {
						isExist = true;
						listOldT.remove(jecnAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
					jecnAccessPermissionsT.setRelateId(relateId);
					jecnAccessPermissionsT.setType(type);
					jecnAccessPermissionsT.setFigureId(ids.getAccessId());
					jecnAccessPermissionsT.setAccessType(ids.isDownLoad() ? 1:0);
					this.save(jecnAccessPermissionsT);
				}
			}
		}
		for (JecnAccessPermissionsT jecnAccessPermissionsT : listOldT) {
			this.deleteObject(jecnAccessPermissionsT);
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "insert into jecn_access_permissions (id, FIGURE_ID, relate_id, type,ACCESS_TYPE)  select id, FIGURE_ID, relate_id, type,ACCESS_TYPE from jecn_access_permissions_t where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, list);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}

	}
	@Override
	public void savaOrUpdate(Long relateId, int type, String posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.setNodeAuths(listIds, type, posIds, isModifyPub);
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, List<AccessId> posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.setNodeAuths(listIds, type, posIds, isModifyPub);
	}
	
	@Override
	public List<JecnTreeBean> getPositionAccessPermissions(Long relateId, int type, boolean isPub) throws Exception {
		String sql = "";
		if (isPub) {
			sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name,jap.access_type"
					+ " from jecn_flow_org_image t,jecn_flow_org jfo,jecn_access_permissions jap where t.org_id=jfo.org_id and jfo.del_state=0"
					+ " and t.figure_id = jap.figure_id and jap.relate_id=? and jap.type=?"
					+ " order by t.org_id,t.sort_id";
		} else {
			// 流程
			sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name,jap.access_type"
					+ " from jecn_flow_org_image t,jecn_flow_org jfo,jecn_access_permissions_t jap where t.org_id=jfo.org_id and jfo.del_state=0"
					+ " and t.figure_id = jap.figure_id and jap.relate_id=? and jap.type=?"
					+ " order by t.org_id,t.sort_id";
		}
		List<Object[]> listPos = listNativeSql(sql, relateId, type);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listPos) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPosAcccess(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	@Override
	public List<JecnTreeBean> getPositionAccessPermissionsGroup(Long relateId, int type, boolean isPub)
			throws Exception {

		String sql = "";
		// 判断是否是发布状态
		if (isPub) {
			sql = "select jpg.id,jpg.name,jpg.per_id,jpg.is_dir from JECN_POSITION_GROUP jpg,JECN_GROUP_PERMISSIONS jgp where jpg.id=jgp.postgroup_id and  jgp.relate_id=? and type=?";
		} else {
			sql = "select jpg.id,jpg.name,jpg.per_id,jpg.is_dir from  JECN_POSITION_GROUP jpg,JECN_GROUP_PERMISSIONS_T jgpt where jpg.id=jgpt.postgroup_id and jgpt.relate_id=? and jgpt.type=?";
		}
		List<Object[]> listGroups = listNativeSql(sql, relateId, type);
		// 将插查询到的岗位组对象转换成树对象
		List<JecnTreeBean> listJecnTreeBeans = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listGroups) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectGroup(obj);
			if (jecnTreeBean != null) {
				listJecnTreeBeans.add(jecnTreeBean);
			}
		}
		return listJecnTreeBeans;
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, String posIds, boolean isSave) throws Exception {
		if (isSave) {
			if (posIds != null && !"".equals(posIds)) {
				String[] idsArr = posIds.split(",");
				for (String str : idsArr) {
					JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
					jecnAccessPermissionsT.setType(type);
					jecnAccessPermissionsT.setRelateId(relateId);
					jecnAccessPermissionsT.setFigureId(Long.valueOf(str));
					this.save(jecnAccessPermissionsT);
				}
			}
		} else {
			String hql = "from JecnAccessPermissionsT where type=? and relateId=?";
			List<JecnAccessPermissionsT> listJecnAccessPermissions = this.listHql(hql, type, relateId);
			// 新添加对象集合
			if (posIds != null && !"".equals(posIds)) {

				String[] idsArr = posIds.split(",");
				for (String str : idsArr) {
					if (str == null || "".equals(str)) {
						continue;
					}
					boolean isExist = false;
					for (JecnAccessPermissionsT jecnAccessPermissionsT : listJecnAccessPermissions) {
						if (str.equals(jecnAccessPermissionsT.getFigureId().toString())) {
							listJecnAccessPermissions.remove(jecnAccessPermissionsT);
							isExist = true;
							break;
						}
					}
					if (!isExist) {
						JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
						jecnAccessPermissionsT.setType(type);
						jecnAccessPermissionsT.setRelateId(relateId);
						jecnAccessPermissionsT.setFigureId(Long.valueOf(str));
						this.save(jecnAccessPermissionsT);
					}
				}
			}
			for (JecnAccessPermissionsT jecnAccessPermissionsT : listJecnAccessPermissions) {
				this.deleteObject(jecnAccessPermissionsT);
			}
		}
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, List<AccessId> Ids, boolean isSave) throws Exception {
		if (isSave) {
			if (Ids != null && !Ids.isEmpty()) {
				for (AccessId accessId : Ids) {
					JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
					jecnAccessPermissionsT.setType(type);
					jecnAccessPermissionsT.setRelateId(relateId);
					jecnAccessPermissionsT.setFigureId(accessId.getAccessId());
					jecnAccessPermissionsT.setAccessType(accessId.isDownLoad() ? 1 : 0);//是否有下载权限
					this.save(jecnAccessPermissionsT);
				}
			}
		} else {
			String hql = "from JecnAccessPermissionsT where type=? and relateId=?";
			List<JecnAccessPermissionsT> listJecnAccessPermissions = this.listHql(hql, type, relateId);
			// 新添加对象集合
			if (Ids != null && !Ids.isEmpty()) {
				for (AccessId accessId : Ids) {
					if (accessId.getAccessId() == null) {
						continue;
					}
					boolean isExist = false;
					for (JecnAccessPermissionsT jecnAccessPermissionsT : listJecnAccessPermissions) {
						if (accessId.getAccessId().equals(jecnAccessPermissionsT.getFigureId().toString())) {
							listJecnAccessPermissions.remove(jecnAccessPermissionsT);
							isExist = true;
							break;
						}
					}
					if (!isExist) {
						JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
						jecnAccessPermissionsT.setType(type);
						jecnAccessPermissionsT.setRelateId(relateId);
						jecnAccessPermissionsT.setFigureId(accessId.getAccessId());
						jecnAccessPermissionsT.setAccessType(accessId.isDownLoad() ? 1 : 0);//是否有下载权限
						this.save(jecnAccessPermissionsT);
					}
				}
			}
			for (JecnAccessPermissionsT jecnAccessPermissionsT : listJecnAccessPermissions) {
				this.deleteObject(jecnAccessPermissionsT);
			}
		}
	}

	@Override
	public void del(Long relateId, int type, String posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (StringUtils.isBlank(posIds)) {
			return;
		}
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, posIds, isModifyPub);

	}

	@Override
	public void del(Long relateId, int type, List<AccessId> posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (posIds == null || posIds.isEmpty()) {
			return;
		}
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, posIds, isModifyPub);

	}
	
	private void delNodeAuths(List<Long> list, int type, List<AccessId> posIds, boolean isModifyPub) throws Exception {

		if (posIds == null || list == null || list.size() == 0) {
			return;
		}
		if (isModifyPub) {
			String hql = "delete from JecnAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			String lastHql = "";
			for (String subHql : listStr) {
				for (AccessId ids : posIds) {
					lastHql = subHql + " and figureId =?";
					this.execteHql(lastHql, type, ids.getAccessId());
				}
			}

		}

		String hql = "delete from JecnAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		String lastHql = "";
		for (String subHql : listStr) {
			for (AccessId ids : posIds) {
				lastHql = subHql + " and figureId =?";
				this.execteHql(lastHql, type, ids.getAccessId());
			}
		}

	}

	private void delNodeAuths(List<Long> list, int type, String posIdsStr, boolean isModifyPub) throws Exception {

		if (StringUtils.isBlank(posIdsStr) || list == null || list.size() == 0) {
			return;
		}
		List<String> figureIds = Arrays.asList(posIdsStr.split(","));
		if (isModifyPub) {
			String hql = "delete from JecnAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			String lastHql = "";
			for (String subHql : listStr) {
				for (String orgId : figureIds) {
					lastHql = subHql + " and figureId =?";
					this.execteHql(lastHql, type, Long.valueOf(orgId));
				}
			}

		}

		String hql = "delete from JecnAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		String lastHql = "";
		for (String subHql : listStr) {
			for (String figureId : figureIds) {
				lastHql = subHql + " and figureId =?";
				this.execteHql(lastHql, type, Long.valueOf(figureId));
			}
		}

	}
	
	@Override
	public void add(Long relateId, int type, String posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (StringUtils.isBlank(posIds)) {
			return;
		}
		// 获得需要赋予权限的节点集合
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, posIds, isModifyPub);

		// 添加
		String[] idsArr = posIds.split(",");
		for (Long nodeId : listIds) {
			for (String str : idsArr) {
				JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
				jecnAccessPermissionsT.setType(type);
				jecnAccessPermissionsT.setRelateId(nodeId);
				jecnAccessPermissionsT.setFigureId(Long.valueOf(str));
				this.save(jecnAccessPermissionsT);
			}
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "delete from jecn_access_permissions where type=? and relate_id in";
			List<String> listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}

			sql = "insert into jecn_access_permissions (id, FIGURE_ID, relate_id, type)  select id, FIGURE_ID, relate_id, type from jecn_access_permissions_t where type=? and relate_id in";
			listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}
		}

	}
	@Override
	public void add(Long relateId, int type, List<AccessId> posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (posIds == null || posIds.isEmpty()) {
			return;
		}
		// 获得需要赋予权限的节点集合
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, posIds, isModifyPub);

		// 添加
		for (Long nodeId : listIds) {
			for (AccessId ids : posIds) {
				JecnAccessPermissionsT jecnAccessPermissionsT = new JecnAccessPermissionsT();
				jecnAccessPermissionsT.setType(type);
				jecnAccessPermissionsT.setRelateId(nodeId);
				jecnAccessPermissionsT.setFigureId(ids.getAccessId());
				jecnAccessPermissionsT.setAccessType(ids.isDownLoad() ? 1 : 0);
				this.save(jecnAccessPermissionsT);
			}
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "delete from jecn_access_permissions where type=? and relate_id in";
			List<String> listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}

			sql = "insert into jecn_access_permissions (id, FIGURE_ID, relate_id, type,ACCESS_TYPE)  select id, FIGURE_ID, relate_id, type,ACCESS_TYPE from jecn_access_permissions_t where type=? and relate_id in";
			listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}
		}

	}
	private List<Long> getNeedChangeAuthNodes(Long relateId, int type, int posType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = new ArrayList<Long>();
		if (posType == 0) {
			listIds = JecnUtil.getNeedSetAuthNodesSingle(relateId, type, nodeType, this);
		} else if (posType == 1) {
			listIds = JecnUtil.getNeedSetAuthNodesContainChildNode(relateId, type, projectId, this);
		}
		return listIds;
	}

}
