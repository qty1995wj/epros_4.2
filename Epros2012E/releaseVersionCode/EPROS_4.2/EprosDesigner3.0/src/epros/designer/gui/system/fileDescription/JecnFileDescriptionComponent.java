package epros.designer.gui.system.fileDescription;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextArea;

import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 操作说明自定义通用组件的父类，非操作说明不得继承
 * 
 * @author hyl
 * 
 */
public abstract class JecnFileDescriptionComponent {

	protected int index;
	protected String name;
	protected Insets insets = new Insets(3, 3, 3, 3);
	protected boolean isRequest;
	/** 包含在contentPanel中的主面板，mainPanel包含两个panel一个是titlePanel,一个是centerPanel **/
	protected JecnPanel mainPanel;
	/** 标题面板，默认是流布局，包含标题名称 **/
	protected JecnPanel titlePanel;
	/** 内容面板，没有布局，需要子类自己设置 **/
	protected JecnPanel centerPanel = new JecnPanel();
	private JLabel titleLabel;

	protected String tipName;

	public JecnFileDescriptionComponent(int index, String name, boolean isRequest, JecnPanel contentPanel) {
		this(index, name, isRequest, contentPanel, "");
	}

	public JecnFileDescriptionComponent(int index, String name, boolean isRequest, JecnPanel contentPanel, String tip) {
		this.index = index;
		this.name = name;
		tipName = index + "、" + name;
		titleLabel = new JLabel(tipName);
		mainPanel = new JecnPanel();
		titlePanel = new JecnPanel();

		GridBagConstraints c = new GridBagConstraints(0, index, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 2, 0, 2), 0, 0);
		contentPanel.add(mainPanel, c);

		mainPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(titlePanel, c);

		if (tip != null && !"".equals(tip)) {
			c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0);
			JTextArea tipArea = new JTextArea(tip);
			tipArea.setForeground(Color.BLUE);
			tipArea.setFont(new Font("宋体", 2, 12));
			tipArea.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			tipArea.setEditable(false);
			tipArea.setLineWrap(true);
			tipArea.setWrapStyleWord(true);
			mainPanel.add(tipArea, c);
		}

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(centerPanel, c);

		titlePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		titlePanel.add(titleLabel);
		if (isRequest) {
			titlePanel.add(new CommonJlabelMustWrite());
		}
	}

	public abstract boolean isUpdate();

	public abstract boolean isValidateNotPass(JLabel verfyLab);

}
