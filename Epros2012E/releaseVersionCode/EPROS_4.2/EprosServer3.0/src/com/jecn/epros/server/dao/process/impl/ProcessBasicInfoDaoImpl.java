package com.jecn.epros.server.dao.process.impl;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.process.FlowOrgT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.util.JecnUtil;

public class ProcessBasicInfoDaoImpl extends
		AbsBaseDao<JecnFlowBasicInfoT, Long> implements IProcessBasicInfoDao {

	@Override
	public Object[] getFlowRelateOrgIds(Long flowId, boolean isPub)
			throws Exception {
		if (isPub) {
			String hql = " select b.orgId,b.orgName from FlowOrg as a,JecnFlowOrg as b where a.orgId=b.orgId and b.delState=0 and a.flowId=?";
			return this.getObjectHql(hql, flowId);
		} else {
			String hql = " select b.orgId,b.orgName from FlowOrgT as a,JecnFlowOrg as b where a.orgId=b.orgId and b.delState=0 and a.flowId=?";
			return this.getObjectHql(hql, flowId);
		}
	}

	@Override
	public List<Object[]> getFlowRelateRules(Long flowId, boolean isPub)
			throws Exception {
		if (isPub) {
			String sql = "select j.* from (select jr.id," +
					" case when jr.is_dir=2 and jr.is_file_local=0 then jf.file_name else jr.rule_name end as ruleName"
					+ "       ,jr.rule_number,jr.file_id,jr.is_dir"
					+ "        from flow_related_criterion t,jecn_rule jr"
					+ "        left join jecn_file jf on jf.file_id=jr.file_id and jf.del_state=0"
					+ "       where t.criterion_class_id=jr.id and jr.del_state=0 and t.flow_id=?)j where j.ruleName is not null";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select j.* from (select jr.id,case when jr.is_dir=2 and jr.is_file_local=0 then jf.file_name else jr.rule_name end as ruleName"
					+ "       ,jr.rule_number,jr.file_id,jr.is_dir"
					+ "        from flow_related_criterion_t t,jecn_rule_t jr"
					+ "        left join jecn_file_t jf on jf.file_id=jr.file_id and jf.del_state=0"
					+ "       where t.criterion_class_id=jr.id and jr.del_state=0 and t.flow_id=?)j where j.ruleName is not null";
			return this.listNativeSql(sql, flowId);
		}

	}
	
	/**
	 * 
	 * 农夫三泉操作说明下载时候专用
	 * 
	 * @description：获得流程相关制度
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRelateRulesNFSQ(Long flowId, boolean isPub)
			throws Exception{
		String sql=null;
		if (isPub) {
			 sql = "select jr.is_dir, case when jr.is_dir = 2 and jr.is_file_local=0 then  jf.Doc_Id else  jr.rule_number end as ruleNum," +
				"       case when jr.is_dir = 2 and jr.is_file_local=0 then jf.file_name else jr.rule_name end as ruleName,o.org_name" + 
				"  from flow_related_criterion t, jecn_rule jr" + 
				"  left join jecn_file jf on jf.file_id = jr.file_id and jf.del_state=0" + 
				"  left join jecn_flow_org o on o.org_id = jr.Org_Id" + 
				" where t.criterion_class_id = jr.id and jr.del_state=0" + 
				"   and t.flow_id = ?";
		} else {
			 sql = "select jr.is_dir, case when jr.is_dir = 2 and jr.is_file_local=0 then  jf.Doc_Id else  jr.rule_number end as ruleNum," +
			"       case when jr.is_dir = 2 and jr.is_file_local=0 then jf.file_name else jr.rule_name end as ruleName,o.org_name" + 
			"  from flow_related_criterion_t t, jecn_rule_t jr" + 
			"  left join jecn_file_t jf on jf.file_id = jr.file_id and jf.del_state=0" + 
			"  left join jecn_flow_org o on o.org_id = jr.Org_Id" + 
			" where t.criterion_class_id = jr.id and jr.del_state=0" + 
			"   and t.flow_id = ?";
		}
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<Object[]> getFlowRelateRulesWeb(Long flowId, boolean isPub)
			throws Exception {
		if (isPub) {
			String sql = "select j.* from (select jr.id,case when jr.is_dir=2 and jr.is_file_local=0 "
					+ "       then jf.file_name else jr.rule_name end as ruleName"
					+ "       ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.doc_id else jr.rule_number end as ruleNumber"
					+ "       ,jr.file_id,jr.is_dir,jr.type_id,jft.type_name,jfo.org_id,jfo.org_name,jr.is_public,jr.pub_time"
					+ "       from flow_related_criterion t,jecn_rule jr"
					+ "       left join jecn_flow_type jft on jft.type_id=jr.type_id"
					+ "       left join jecn_flow_org jfo on jfo.org_id = jr.org_id"
					+ "       left join jecn_file jf on jf.file_id = jr.file_id and jf.del_state=0"
					+ "        where t.criterion_class_id=jr.id  and t.flow_id=?)j where j.ruleName is not null";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select j.* from (select jr.id,case when jr.is_dir=2 and jr.is_file_local=0"
					+ "       then jf.file_name else jr.rule_name end as ruleName"
					+ "       ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.doc_id else jr.rule_number end as ruleNumber"
					+ "       ,jr.file_id,jr.is_dir,jr.type_id,jft.type_name,jfo.org_id,jfo.org_name,jr.is_public,jr.pub_time"
					+ "       from flow_related_criterion_t t,jecn_rule_t jr"
					+ "       left join jecn_flow_type jft on jft.type_id=jr.type_id"
					+ "       left join jecn_flow_org jfo on jfo.org_id = jr.org_id"
					+ "       left join jecn_file_t jf on jf.file_id = jr.file_id and jf.del_state=0"
					+ "        where t.criterion_class_id=jr.id and t.flow_id=?)j where j.ruleName is not null";
			return this.listNativeSql(sql, flowId);
		}

	}

	@Override
	public List<Object[]> getFlowRelateStandardBeans(Long flowId, boolean isPub)
			throws Exception {
		if (isPub) {
			String sql = "select b.criterion_class_id,b.criterion_class_name,b.related_id,b.stan_type"
					+ "          from JECN_FLOW_STANDARD a, JECN_CRITERION_CLASSES b"
					+ "          where a.criterion_class_id = b.criterion_class_id"
					+ "          and a.flow_id = ?";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select b.criterion_class_id,b.criterion_class_name,b.related_id,b.stan_type"
					+ "          from JECN_FLOW_STANDARD_T a, JECN_CRITERION_CLASSES b"
					+ "          where a.criterion_class_id = b.criterion_class_id"
					+ "          and a.flow_id = ?";
			return this.listNativeSql(sql, flowId);
		}
	}

	@Override
	public FlowOrgT getFlowOrgTByFlowId(Long flowId) throws Exception {
		String hql = " select a from FlowOrgT as a,JecnFlowOrg as b where a.orgId=b.orgId and b.delState=0 and a.flowId=?";
		List<FlowOrgT> list = this.listHql(hql, flowId);
		if (list.size() == 0 || list.get(0) == null) {
			return null;
		}
		return list.get(0);
	}

	public String getUserName(Long userId) throws Exception {
		String hql = "select trueName from JecnUser where peopleId=?";
		List<String> list = this.listHql(hql, userId);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public String getPosName(Long posId) throws Exception {
		String hql = "select figureText from JecnFlowOrgImage where figureId=?";
		List<String> list = this.listHql(hql, posId);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	public List<String> getPosNameByUserId(Long userId) throws Exception {
		String sql = "SELECT OI.FIGURE_TEXT POS_NAME "
				+ "  FROM JECN_FLOW_ORG_IMAGE OI "
				+ " INNER JOIN JECN_USER_POSITION_RELATED UPR ON OI.FIGURE_ID = UPR.FIGURE_ID "
				+ " WHERE UPR.PEOPLE_ID = ?";
		return this.listNativeSql(sql, userId);
	}

	public JecnFlowBasicInfoT2 getFlowBasicInfoT2(Long flowId) throws Exception {
		String hql="select a from JecnFlowBasicInfoT2 as a where a.flowId=?";
		return (JecnFlowBasicInfoT2)getObjectHql(hql, flowId);
	}
	
	/**
	 * 获得该流程基本信息表临时表
	 * @param rid 流程id
	 * @return
	 */
	public JecnFlowBasicInfoT getFlowBasicInfoT(Long flowId) throws Exception {
		String hql="select a from JecnFlowBasicInfoT as a where a.flowId=?";
		return (JecnFlowBasicInfoT)getObjectHql(hql, flowId);
	}

	@Override
	public void updateExpiry(Long flowId,Long docId, int expiry, String nextScandDate,int type)
			throws Exception {
		 
		String sql =  "select count(jt.id) from jecn_task_history_new jt where jt.id=? and (jt.expiry <> ? OR ";
		if (JecnCommon.isSQLServer()) {
			sql = sql + " datediff(day,convert(varchar(10),'" + nextScandDate
					+ "',23), JT.NEXT_SCAN_DATE) <> 0)";
		} else if (JecnCommon.isOracle()) {
			sql = sql
					+ " to_date(to_char(JT.NEXT_SCAN_DATE,'yyyy-mm-dd'),'yyyy-mm-dd')-to_date('"
					+ nextScandDate + "','yyyy-MM-dd') <> 0) ";
		}
		
		int count = this.countAllByParamsNativeSql(sql,docId,expiry);
		if(count==0){
			return;
		}
		
		if (type == 0) {// 流程
			sql = "update jecn_flow_basic_info_T set expiry = " + expiry + " where flow_id =" + flowId
					+ " and expiry<>" + expiry;
			this.execteNative(sql);

			sql = "update jecn_flow_basic_info set expiry = " + expiry + " where flow_id =" + flowId + " and expiry<>"
					+ expiry;
			this.execteNative(sql);
		} else if (type == 2) {// 制度
			sql = "update JECN_RULE_T set expiry = " + expiry + " where id =" + flowId
					+ " and expiry<>" + expiry;
			this.execteNative(sql);

			sql = "update JECN_RULE set expiry = " + expiry + " where id =" + flowId + " and expiry<>"
					+ expiry;
			this.execteNative(sql);
		}
		
		if (JecnCommon.isSQLServer()) {
			sql = "update jecn_task_history_new " + " set expiry = " + expiry
					+ ", next_scan_date = convert(varchar(10),'" + nextScandDate + "',23)"
					+ " where id =? and (expiry <> ? OR " + " datediff(day,convert(varchar(10),'" + nextScandDate
					+ "',23), NEXT_SCAN_DATE) <> 0)";
		} else if (JecnCommon.isOracle()) {
			sql = "update jecn_task_history_new " + " set expiry = " + expiry + ", next_scan_date = to_date('"
					+ nextScandDate + "', 'yyyy-mm-dd')" + " where id =? and (expiry <> ? OR "
					+ " to_date(to_char(NEXT_SCAN_DATE,'yyyy-mm-dd'),'yyyy-mm-dd')-to_date('" + nextScandDate
					+ "','yyyy-MM-dd') <> 0) ";
		}
		this.execteNative(sql, docId, expiry);
	}

	@Override
	public List<JecnFlowOrg> getFlowOrgPerm(Long flowId, boolean isPub)
			throws Exception {
		
		String permTableName="JECN_ORG_ACCESS_PERM_T";
		if(isPub){
			permTableName="JECN_ORG_ACCESS_PERMISSIONS";
		}
		String sql="select o.* from jecn_flow_org o inner join  "+permTableName+" t on o.org_id=t.org_id and t.type=0 and t.relate_id="+flowId+" order by o.org_id,o.sort_id";
		return this.getSession().createSQLQuery(sql).addEntity(JecnFlowOrg.class).list();
	}

	@Override
	public List<String[]> getFlowRelateStandardFile(Long flowId, boolean isPub) {
		String pub=isPub?"":"_T";
		String sql=" select A.ID,B.FILE_ID,B.FILE_NAME,B.DOC_ID from FLOW_STANDARDIZED_FILE"+pub+" A" +
		" INNER JOIN JECN_FILE"+pub+" B ON A.FILE_ID=B.FILE_ID AND B.del_state=0 AND A.FLOW_ID=?";
		List<Object[]> objs = this.listNativeSql(sql, flowId);
		List<String[]> result=new ArrayList<String[]>();
		for(Object[] obj:objs){
			result.add(new String[]{JecnUtil.objToStr(obj[0]),JecnUtil.objToStrWithNull(obj[1]),JecnUtil.objToStr(obj[2]),JecnUtil.objToStr(obj[3])});
		}
		return result;
	}

	@Override
	public List<FlowFileHeadData> findFlowFileHeadInfo(Long flowId) {
		String sql = "select head.*" + 
			"    from jecn_flow_structure_t T" + 
			"    left join jecn_flow_structure_t jr" + 
			"      on " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("jr")+
			"    left join JECN_FLOW_FILE_HEADINFO head" + 
			"    on head.flow_id = jr.flow_id" + 
			"   where T.flow_id = "+flowId+" and jr.isflow = 0 and jr.del_state=0 and head.id is not null" + 
			"   order by  jr.t_path desc";
		return this.getSession().createSQLQuery(sql).addEntity(FlowFileHeadData.class).list();
	
	}
	
}
