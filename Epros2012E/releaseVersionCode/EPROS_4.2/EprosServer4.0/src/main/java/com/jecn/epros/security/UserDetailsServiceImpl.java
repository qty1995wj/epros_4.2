package com.jecn.epros.security;

import com.jecn.epros.configuration.cache.CacheManager;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.security.AuthorityNodesData;
import com.jecn.epros.domain.security.CurProject;
import com.jecn.epros.domain.security.NodeData;
import com.jecn.epros.domain.security.NodeIdsData;
import com.jecn.epros.domain.user.PermissionsMenu;
import com.jecn.epros.mapper.SecurityMapper;
import com.jecn.epros.mapper.UserMapper;
import com.jecn.epros.service.system.SystemService;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SecurityMapper securityMapper;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SystemService systemService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            UserDetails user = CacheManager.INSTANCE.getUser(username);
            if (user != null) {
                // 重新登录，加载缓存数据
                JecnUser userData = securityMapper.findByUsername(username);
                AuthenticatedUser authenticatedUser = (com.jecn.epros.security.AuthenticatedUser) CacheManager.INSTANCE.getUser(username);
                resetDataAccessAuthorityByUserId(authenticatedUser.getDataAccessAuthorityBean(), userData.getId());
            }
            return user;
        } catch (CacheManager.UserIsNotInCacheException e) {
            JecnUser user = securityMapper.findByUsername(username);
            if (user == null) {
                throw new UsernameNotFoundException(String.format("未找到用户，登录名称：'%s'.", username));
            } else {
                DataAccessAuthorityBean authorityBean = getDataAccessAuthorityByUserId(user.getId());
                UserDetails authenticatedUser = AuthenticatedUserFactory.create(user, authorityBean, getApiAuthorities(authorityBean, user.getId()));
                CacheManager.INSTANCE.putUser(username, authenticatedUser);
                return authenticatedUser;
            }
        }
    }

    /**
     * 用户菜单权限API router
     *
     * @param peopeleId
     * @return
     */
    /**
     * 用户菜单权限API router
     *
     * @param authorityBean
     * @return
     */
    private Collection<SimpleGrantedAuthority> getApiAuthorities(DataAccessAuthorityBean authorityBean, Long pepoleId) {

        // 设置人员为管理员或者浏览管理员时 ，未设置角色菜单和菜单的关联关系
        //insert into USER_ROLE_PERMISSIONS (GUID, PEOPLE_ID, ROLE_ID) values ('1', 1, '1');
        /**
         * 添加角色
         */
        systemService.addRoles(authorityBean, pepoleId);
        Map<String, Object> map = new HashedMap();
        map.put("roleIds", authorityBean.getRowId());
        //获取 语言类型
        map.put("languageType", 0);
        List<PermissionsMenu> dataPermissionsMenus = userMapper.getPermissionsMenuById(map);
        List<SimpleGrantedAuthority> apiAuthorityList = new ArrayList<>();
        SimpleGrantedAuthority simpleGrantedAuthority = null;
        for (PermissionsMenu permissionsMenu : dataPermissionsMenus) {
            if (StringUtils.isBlank(permissionsMenu.getRoute())) {
                continue;
            }
            simpleGrantedAuthority = new SimpleGrantedAuthority(permissionsMenu.getRoute());
            apiAuthorityList.add(simpleGrantedAuthority);
        }
        return apiAuthorityList;
    }

    /**
     * 获取用户的数据权限
     *
     * @param userId
     * @return
     */
    private DataAccessAuthorityBean getDataAccessAuthorityByUserId(Long userId) {
        DataAccessAuthorityBean dataAccessAuthorityBean = Common.getDataAccessAuthorityByUserId(userId, securityMapper);
        dataAccessAuthorityBean.setNodesData(getAuthorityDataByAuthorityBean(dataAccessAuthorityBean, userId));
        return dataAccessAuthorityBean;
    }


    private void resetDataAccessAuthorityByUserId(DataAccessAuthorityBean dataAccessAuthorityBean, Long userId) {
        // 获得项目
        CurProject curProject = securityMapper.findCurProject();
        Map<String, Object> map = new HashMap<>();
        map.put("peopleId", userId);
        map.put("projectId", curProject.getProjectId());

        // 获得用户下所有岗位
        List<Map<String, Object>> subPostAndOrgMaps = securityMapper.findPosIds(map);
        Set<Long> listPosIds = new HashSet<>();
        Set<Long> listSubOrgIds = new HashSet<>();
        for (Map<String, Object> subMap : subPostAndOrgMaps) {
            listPosIds.add(Long.valueOf(subMap.get("FIGURE_ID").toString()));
            listSubOrgIds.add(Long.valueOf(subMap.get("ORG_ID").toString()));
        }
        dataAccessAuthorityBean.setListPosIds(listPosIds);
        dataAccessAuthorityBean.setListSubOrgIds(listSubOrgIds);
        // 获得用户下所有部门
        Set<Long> listOrgIds = securityMapper.findOrgIds(map);
        dataAccessAuthorityBean.setListOrgIds(listOrgIds);
        // 获得用户默认权限的关键字
        List<String> filters = securityMapper.findDefaultRoleFilter(map);
        for (String str : filters) {
            // 是不是系统管理员
            if (str.equals(SqlCommon.getStrAdmin())) {
                dataAccessAuthorityBean.setAdmin(true);
            }
            // 是不是浏览管理员
            if (str.equals(SqlCommon.getStrViewAdmin())) {
                dataAccessAuthorityBean.setViewAdmin(true);
            }
            // 是不是下载管理员
            if (str.equals(SqlCommon.getStrDownLoadAdmin())) {
                dataAccessAuthorityBean.setDownLoadAdmin(true);
            }
            // 是不是二级管理员
            if (str.equals(SqlCommon.getStrSecondAdmin())) {
                dataAccessAuthorityBean.setSecondAdmin(true);
            }
        }
        dataAccessAuthorityBean.setNodesData(getAuthorityDataByAuthorityBean(dataAccessAuthorityBean, userId));
        // 如果适用范围启用部门选择
        dataAccessAuthorityBean.setOrgScope(Common.getOrgScope(securityMapper, userId));
    }

    /**
     * 根据用户信息获取用户有权限的数据节点
     *
     * @param authorityBean
     * @return
     */
    public AuthorityNodesData getAuthorityDataByAuthorityBean(DataAccessAuthorityBean authorityBean, Long peopleId) {
        Map<String, Object> map = new HashedMap();
        map.put("listFigureIds", authorityBean.getListPosIds());
        map.put("listOrgIds", authorityBean.getListOrgIds());
        map.put("projectId", authorityBean.getProjectId());
        map.put("peopleId", peopleId);

        AuthorityNodesData nodesData = new AuthorityNodesData();
        if (!isViewAdmin(authorityBean)) {
            nodesData.setProcessNode(getNodeIdsDataByType(AuthSqlConstant.TYPEAUTH.FLOW, map));
        }
        if (!isViewAdmin(authorityBean)) {
            nodesData.setRuleNode(getNodeIdsDataByType(AuthSqlConstant.TYPEAUTH.RULE, map));
        }
        if (!isViewAdmin(authorityBean)) {
            nodesData.setFileNode(getNodeIdsDataByType(AuthSqlConstant.TYPEAUTH.FILE, map));
        }
        if (!isViewAdmin(authorityBean)) {
            nodesData.setStandardNode(getNodeIdsDataByType(AuthSqlConstant.TYPEAUTH.STANDARD, map));
        }
        return nodesData;
    }

    private NodeIdsData getNodeIdsDataByType(AuthSqlConstant.TYPEAUTH type, Map<String, Object> map) {
        map.put("typeAuth", type);
        return getAuthorityNodeData(securityMapper.getAuthorityNodes(map), type);
    }

    private boolean isFlowAdmin(DataAccessAuthorityBean authorityBean) {
        return isViewAdmin(authorityBean) || (JecnContants.isFlowAdmin && JecnContants.isMainFlowAdmin);
    }

    private boolean isFileAdmin(DataAccessAuthorityBean authorityBean) {
        return isViewAdmin(authorityBean) || JecnContants.isFileAdmin;
    }

    private boolean isRuleAdmin(DataAccessAuthorityBean authorityBean) {
        return isViewAdmin(authorityBean) || JecnContants.isSystemAdmin;
    }

    private boolean isStandardAdmin(DataAccessAuthorityBean authorityBean) {
        return isViewAdmin(authorityBean) || JecnContants.isCriterionAdmin;
    }

    /**
     * 浏览管理员
     *
     * @param authorityBean
     * @return
     */
    private boolean isViewAdmin(DataAccessAuthorityBean authorityBean) {
        return authorityBean.isAdmin() || authorityBean.isViewAdmin();
    }

    private NodeIdsData getAuthorityNodeData(List<NodeData> authorityDatas, AuthSqlConstant.TYPEAUTH type) {
        NodeIdsData nodeData = new NodeIdsData();
        if (authorityDatas == null) {
            return nodeData;
        }

        for (NodeData data : authorityDatas) {
            addNodeIds(data, nodeData, type);
        }
        return nodeData;
    }

    /**
     * 添加权限节点
     *
     * @param data
     * @param nodeData
     */
    private void addNodeIds(NodeData data, NodeIdsData nodeData, AuthSqlConstant.TYPEAUTH type) {
        if (StringUtils.isBlank(data.gettPath())) {
            return;
        }
        boolean flag = true;
        switch (type) {  // 文件全局公开后 不查询操作查阅权限 但是要查询 下载权限
            case FILE:
                if (JecnContants.isFileAdmin) {
                    flag = false;
                }
                break;
            case FLOW:
                if ((JecnContants.isFlowAdmin && JecnContants.isMainFlowAdmin)) {
                    flag = false;
                }
                break;
            case RULE:
                if (JecnContants.isSystemAdmin) {
                    flag = false;
                }
                break;
            default:
                break;
        }
        if (flag) {
            // path 为展开路径，所有节点都是有展开权限
            List<String> pathIds = Arrays.asList(data.gettPath().split("-"));
            for (String id : pathIds) {
                nodeData.getExpandIds().add(Long.parseLong(id));
            }
            nodeData.getOperationIds().add(data.getId());
        }
        if (data.getAccessType() != null && data.getAccessType() == 1) {
            nodeData.getDownLoadIds().add(data.getId());
        }
    }


}
