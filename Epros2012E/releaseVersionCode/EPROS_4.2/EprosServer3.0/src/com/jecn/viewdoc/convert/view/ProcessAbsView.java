package com.jecn.viewdoc.convert.view;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * 流程预览抽象类
 * 
 * @author hyl
 * 
 */
public abstract class ProcessAbsView implements View {
	protected static final Logger log = Logger.getLogger(View.class);
	protected IWebProcessService webProcessService;
	protected Long relatedId;
	protected TreeNodeType nodeType;
	protected boolean isPub;
	protected Long userId;
	protected Long historyId;
	/** 文件(未发布)0 文件(已发布)1 制度文件(未发布) 2 */
	protected Integer type;
	protected JecnCreateDoc createDoc = null;
	protected HttpServletRequest request;
	protected HttpServletResponse response;

	public ProcessAbsView(IWebProcessService webProcessService, Long relatedId, TreeNodeType nodeType, boolean isPub,
			Long userId, Long historyId, HttpServletRequest request, HttpServletResponse response) {
		this.webProcessService = webProcessService;
		this.relatedId = relatedId;
		this.nodeType = nodeType;
		this.isPub = isPub;
		this.userId = userId;
		this.request = request;
		this.response = response;

		try {
			createDoc = webProcessService.printFlowDoc(relatedId, nodeType, isPub, userId, historyId);
		} catch (Exception e) {
			throw new ViewException(getDataError(), e);
		}
	}

	private String getDataError() {
		return "ProcessAbsView() 获得数据异常" + toString();
	}

	@Override
	public String toString() {
		return "ProcessAbsView [historyId=" + historyId + ", isPub=" + isPub + ", nodeType=" + nodeType
				+ ", relatedId=" + relatedId + ", type=" + type + ", userId=" + userId + "]";
	}

	@Override
	public boolean allowView() {
		return true;
	}

	@Override
	public boolean allowViewExtension(String extension) {
		return true;
	}

	@Override
	public boolean allowViewExtension() {
		return true;
	}

	@Override
	public Set<String> allowViewExtensions() {
		return new HashSet<String>();
	}

	@Override
	public String getFileName() {
		String fileName = "操作说明.doc";
		if (createDoc != null) {
			fileName = createDoc.getFileName();
		}
		if (!fileName.endsWith(".doc")) {
			fileName = JecnUtil.getShowName(fileName, createDoc.getIdInput());
			fileName = fileName + ".doc";
		}
		return fileName;
	}

	protected String assembleFileNameAndDownloadURL(String file) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("?name=");
		try {
			buffer.append(URLEncoder.encode(getFileName(), "UTF-8").replace("+", "%20"));
			buffer.append("&file=" + file);
			buffer.append("&");

			buffer.append("downloadURL=");
			/** 文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 */

			ViewInfo info = new ViewInfo();
			info.setRelatedId(relatedId);
			info.setType(type);
			buffer.append(ViewUtils.getDownloadFileURL(info));
		} catch (Exception e) {
			log.error("转码异常", e);
		}
		return buffer.toString();
	}

}
