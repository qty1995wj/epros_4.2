package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnAddItemJDialog;

/**
 * 
 * 添加按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAddJButton extends JecnAbstractBaseJButton {

	public JecnAddJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (dialog.getSelectedPropContPanel() == null) {
			return;
		}
		// 加入对话框
		JecnAddItemJDialog addDialog = new JecnAddItemJDialog(dialog);
		//标题
		addDialog.setTitle(this.getText());

		addDialog.setVisible(true);
	}
}
