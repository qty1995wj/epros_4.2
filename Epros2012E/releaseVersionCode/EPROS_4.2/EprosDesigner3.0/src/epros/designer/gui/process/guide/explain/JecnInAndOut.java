package epros.designer.gui.process.guide.explain;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;

import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.operationConfig.JecnTextArea;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 输入和输出
 * 
 */
public class JecnInAndOut extends JecnFileDescriptionComponent {

	/** 输入Lab */
	protected JLabel inLab = new JLabel();

	/** 输入Field */
	protected JecnTextArea inArea = new JecnTextArea();

	/** 输出Lab */
	protected JLabel outLab = new JLabel();

	/** 输出Field */
	protected JecnTextArea outArea = new JecnTextArea();

	/** 输入滚动面板 */
	protected JScrollPane inScrollPanel = new JScrollPane(inArea);
	/** 输出滚动面板 */
	protected JScrollPane outScrollPanel = new JScrollPane(outArea);
	/** 数据 */
	private JecnFlowBasicInfoT flowBasicInfoT;

	public JecnInAndOut(int index, String paragrapHeadingName, JecnPanel contentPanel,
			boolean isRequest, JecnFlowBasicInfoT jecnFlowBasicInfoT, String tip) {
		super(index, paragrapHeadingName, isRequest, contentPanel, tip);
		this.flowBasicInfoT = jecnFlowBasicInfoT;

		initCom();
		initLayout();
		initData();

	}

	private void initData() {
		inArea.setText(flowBasicInfoT.getInput());
		outArea.setText(flowBasicInfoT.getOuput());
	}

	public void initLayout() {

		centerPanel.setLayout(new GridBagLayout());
		// 输入和输出
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));

		// 输入Lab
		inLab.setText(JecnProperties.getValue("inLabC"));
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(inLab, c);
		// 输入Field
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(inScrollPanel, c);

		// 输出Lab
		outLab.setText(JecnProperties.getValue("outLabC"));
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		centerPanel.add(outLab, c);
		// 输出Filed
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(outScrollPanel, c);

	}

	private void initCom() {
		inScrollPanel.setBorder(null);
		inScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		outScrollPanel.setBorder(null);
		outScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		inArea.setRows(4);
		outArea.setRows(4);
	}

	@Override
	public boolean isUpdate() {
		String input = inArea.getText();
		String output = outArea.getText();
		if (DrawCommon.checkStringSame(input, flowBasicInfoT.getInput())
				&& DrawCommon.checkStringSame(output, flowBasicInfoT.getOuput())) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
	    
		if (JecnValidateCommon.validateAreaNotPass(inArea.getText(), verfyLab, JecnProperties.getValue("actIn"))) {
			return true;
		}
		if (JecnValidateCommon.validateAreaNotPass(outArea.getText(), verfyLab, JecnProperties.getValue("actOut"))) {
			return true;
		}
		return false;
	}
	
	public String getIn(){
		return inArea.getText().trim();
	}
	
	public String getOut(){
		return outArea.getText().trim();
	}

}
