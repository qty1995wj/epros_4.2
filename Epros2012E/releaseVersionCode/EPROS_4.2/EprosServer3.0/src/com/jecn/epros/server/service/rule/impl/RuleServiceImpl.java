package com.jecn.epros.server.service.rule.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import oracle.sql.BLOB;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.lob.SerializableBlob;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleInstitutionBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleRiskBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleStandardBeanT;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.rule.AddRuleFileData;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.JecnFlowCommon;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnRuleApplyOrgT;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.rule.JecnRuleCommon;
import com.jecn.epros.server.bean.rule.JecnRuleData;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.JecnRuleUseRecord;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleContent;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleFile;
import com.jecn.epros.server.bean.rule.RuleFileT;
import com.jecn.epros.server.bean.rule.RuleModeTitleBean;
import com.jecn.epros.server.bean.rule.RuleStandardizedFileT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.bean.rule.sync.SyncRule;
import com.jecn.epros.server.bean.rule.sync.SyncRules;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnRelatedDirFileUtil;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.dao.autoCode.IAutoCodeDao;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IFlowOrgImageDao;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.popedom.IPosGroupnAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.dao.rule.IJecnRuleUseRecordDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.standard.IStandardDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.system.IProcessRuleTypeDao;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.download.word.SelectDownloadProcess;
import com.jecn.epros.server.service.autoCode.AutoCodeEnum;
import com.jecn.epros.server.service.autoCode.IStandardizedAutoCodeService;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.popedom.IOrganizationService;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.service.vatti.buss.DominoOperation;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.rule.RuleFileWebBean;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.rule.RuleSearchBean;
import com.jecn.epros.server.webBean.rule.RuleSystemListBean;
import com.sun.xml.internal.txw2.IllegalAnnotationException;

@Transactional(rollbackFor = Exception.class)
@Component
public class RuleServiceImpl extends AbsBaseService<RuleT, Long> implements IRuleService {
	private Logger log = Logger.getLogger(RuleServiceImpl.class);
	private IRuleDao ruleDao;
	private IOrgAccessPermissionsDao orgAccessPermissionsDao;
	private IJecnDocControlDao docControlDao;
	private IProcessRuleTypeDao processRuleTypeDao;
	private IOrganizationService organizationService;
	private IFileDao fileDao;
	private IJecnConfigItemDao configItemDao;
	private IJecnRuleUseRecordDao jecnRuleUseRecordDao;
	private IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao;
	private IPersonDao userDao;
	private IFlowOrgImageDao flowOrgImageDao;

	private IStandardDao standardDao;
	@Resource
	private IPersonService personService;
	@Resource
	private IAutoCodeDao autoCodeDao;
	@Resource(name = "FileServiceImpl")
	private IFileService fileService;
	@Resource(name = "standardizedAutoCodeServiceImpl")
	private IStandardizedAutoCodeService autoCodeService;

	public IFlowOrgImageDao getFlowOrgImageDao() {
		return flowOrgImageDao;
	}

	@Resource
	public void setFlowOrgImageDao(IFlowOrgImageDao flowOrgImageDao) {
		this.flowOrgImageDao = flowOrgImageDao;
	}

	public IPosGroupnAccessPermissionsDao getPosGroupAccessPermissionsDao() {
		return posGroupAccessPermissionsDao;
	}

	public void setPosGroupAccessPermissionsDao(IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao) {
		this.posGroupAccessPermissionsDao = posGroupAccessPermissionsDao;
	}

	public IJecnRuleUseRecordDao getJecnRuleUseRecordDao() {
		return jecnRuleUseRecordDao;
	}

	public void setJecnRuleUseRecordDao(IJecnRuleUseRecordDao jecnRuleUseRecordDao) {
		this.jecnRuleUseRecordDao = jecnRuleUseRecordDao;
	}

	public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
		this.orgAccessPermissionsDao = orgAccessPermissionsDao;
	}

	public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
		this.positionAccessPermissionsDao = positionAccessPermissionsDao;
	}

	private IPositionAccessPermissionsDao positionAccessPermissionsDao;

	// /** 组织 */
	// private IOrganizationDao organizationDao;
	public IJecnDocControlDao getDocControlDao() {
		return docControlDao;
	}

	public void setDocControlDao(IJecnDocControlDao docControlDao) {
		this.docControlDao = docControlDao;
	}

	public IRuleDao getRuleDao() {
		return ruleDao;
	}

	public void setRuleDao(IRuleDao ruleDao) {
		this.ruleDao = ruleDao;
		this.baseDao = ruleDao;
	}

	// public IOrganizationDao getOrganizationDao() {
	// return organizationDao;
	// }
	//
	// public void setOrganizationDao(IOrganizationDao organizationDao) {
	// this.organizationDao = organizationDao;
	// }
	public IPersonDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IPersonDao userDao) {
		this.userDao = userDao;
	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：制度对象转换成树对象
	 * @param obj
	 * @return
	 */
	private List<JecnTreeBean> getJecnTreeBeanByObject(List<Object[]> list, List<Object[]> listTask) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
					|| obj[6] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 制度节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			if (obj[1] != null) {
				// 制度节点名称
				jecnTreeBean.setName(obj[1].toString());
			} else {
				jecnTreeBean.setName("");
			}
			// 制度节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 制度节点父类名称
			jecnTreeBean.setPname("");
			// 制度类型
			if ("0".equals(obj[3].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleDir);
			} else if ("1".equals(obj[3].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeFile);
			} else {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleFile);
			}
			// 制度编号
			if (obj[4] != null) {
				jecnTreeBean.setNumberId(obj[4].toString());
			} else {
				jecnTreeBean.setNumberId("");
			}
			// 是否发布
			if ("0".equals(obj[5].toString())) {
				jecnTreeBean.setPub(false);
			} else {
				jecnTreeBean.setPub(true);
			}
			// 是否子节点
			if (Integer.parseInt(obj[6].toString()) > 0) {
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			// 制度模板
			if (obj[7] != null) {
				jecnTreeBean.setModeId(Long.valueOf(obj[7].toString()));
			} else {
				jecnTreeBean.setModeId(null);
			}
			if (obj.length >= 9 && obj[8] != null) {
				jecnTreeBean.setRelationId(Long.valueOf(obj[8].toString()));
			}
			if (obj.length >= 11 && obj[10] != null) {
				jecnTreeBean.setDataType(Integer.valueOf(obj[10].toString()));
			}
			if (obj.length >= 12 && obj[11] != null) {
				if (Integer.valueOf(obj[11].toString()) == 1) {
					jecnTreeBean.setUpdate(true);
				} else {
					jecnTreeBean.setUpdate(false);
				}
			}
			JecnCommon.setTaskState(jecnTreeBean.getId(), listTask, jecnTreeBean);
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：制度对象转换成树对象
	 * @param obj
	 * @return
	 */
	private List<JecnTreeBean> getJecnTreeBeanByObject(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
					|| obj[6] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 制度节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			if (obj[1] != null) {
				// 制度节点名称
				jecnTreeBean.setName(obj[1].toString());
			} else {
				jecnTreeBean.setName("");
			}
			// 制度节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 制度节点父类名称
			jecnTreeBean.setPname("");
			// 制度类型
			if ("0".equals(obj[3].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleDir);
			} else if ("1".equals(obj[3].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeFile);
			} else {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleFile);
			}
			// 制度编号
			if (obj[4] != null) {
				jecnTreeBean.setNumberId(obj[4].toString());
			} else {
				jecnTreeBean.setNumberId("");
			}
			// 是否发布
			if ("0".equals(obj[5].toString())) {
				jecnTreeBean.setPub(false);
			} else {
				jecnTreeBean.setPub(true);
			}
			// 是否子节点
			if (Integer.parseInt(obj[6].toString()) > 0) {
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			// 制度模板
			if (obj[7] != null) {
				jecnTreeBean.setModeId(Long.valueOf(obj[7].toString()));
			} else {
				jecnTreeBean.setModeId(null);
			}
			if (obj.length >= 9 && obj[8] != null) {
				jecnTreeBean.setRelationId(Long.valueOf(obj[8].toString()));
			}
			if (obj.length >= 11 && obj[10] != null) {
				jecnTreeBean.setDataType(Integer.valueOf(obj[10].toString()));
			}
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	// 制度目录下制度节点
	public List<JecnTreeBean> getChildRules(Long pid, Long projectId) throws Exception {
		return getChildRules(pid, projectId, 0);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception {
		List<Object[]> list = ruleDao.getRoleAuthChildFiles(pId, projectId, peopleId);
		return getJecnTreeBeanByObject(list);
	}

	@Override
	// 所有制度集合
	public List<JecnTreeBean> getAllRule(Long projectId) throws Exception {
		try {
			String sql = "select distinct t.id," + "                 t.rule_name," + "                 t.per_id,"
					+ "                 t.is_dir," + "                 t.rule_number," + "                 case"
					+ "                   when jr.id is null then" + "                    '0'"
					+ "                   else" + "                    '1'" + "                 end as is_pub,"
					+ "                 case when sub.id is null then '0' else '1' end as count,"
					+ "                 t.TEMPLATE_ID," + "                 t.FILE_ID," + "                 t.sort_id"
					+ "   from jecn_rule_t t" + "   left join jecn_rule jr on t.id = jr.id"
					+ "   left join jecn_rule_t sub on sub.per_id=t.id" + "  where t.project_id = ?"
					+ "  order by t.per_id, t.sort_id, t.id";
			List<Object[]> list = ruleDao.listNativeSql(sql, projectId);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 根据PID查询出制度目录
	public List<JecnTreeBean> getChildRuleDirs(Long pid, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.id,t.rule_name,t.per_id,"
					+ "case when ju.id is null then '0' else '1' end as count,t.sort_id"
					+ " from jecn_rule_t t "
					+ " left join jecn_rule_t ju on ju.per_id=t.id and ju.is_dir=0"
					+ " where t.per_id=? and t.project_id=? and t.del_state=0 and t.is_dir=0 order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = ruleDao.listNativeSql(sql, pid, projectId);
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
					continue;
				}
				JecnTreeBean jecnTreeBean = new JecnTreeBean();

				// 制度节点ID
				jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
				// 制度节点名称
				jecnTreeBean.setName(obj[1].toString());
				// 制度节点父ID
				jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
				// 制度节点父类名称
				jecnTreeBean.setPname("");
				// 制度类型
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleDir);

				if (Integer.parseInt(obj[3].toString()) > 0) {
					// 文件节点是否有子节点
					jecnTreeBean.setChildNode(true);
				} else {
					jecnTreeBean.setChildNode(false);
				}

				jecnTreeBean.setModeId(null);

				listResult.add(jecnTreeBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 重命名
	public void reName(String newName, Long id, Long updatePersonId, int userType) throws Exception {
		try {
			String hql = "update RuleT set ruleName=?,updateDate=?,updatePeopleId=? where id=? and delState <> 2";
			ruleDao.execteHql(hql, newName, new Date(), updatePersonId, id);

			RuleT ruleT = ruleDao.get(id);
			if (ruleT.getIsDir() != 0 && JecnConfigTool.isAllowedPubReName()) {// 重命名是否直接发布，不直接发布，编号不更新
				hql = "update Rule set ruleName=? where id=?";
				ruleDao.execteHql(hql, newName, id);
			} else if (ruleT.getIsDir() == 0) {
				hql = "update Rule set ruleName=? where id=?";
				ruleDao.execteHql(hql, newName, id);
			}

			// 添加日志
			JecnUtil.saveJecnJournal(id, newName, 4, 2, updatePersonId, ruleDao, 4);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	@Override
	// 根据名称搜索
	public List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.id," + "                           t.rule_name,"
					+ "                           t.per_id," + "                           t.is_dir,"
					+ "                           t.rule_number," + "                           case"
					+ "                             when jr.id is null then" + "                              '0'"
					+ "                             else" + "                              '1'"
					+ "                           end as is_pub,"
					+ "                            case when sub.id is null then '0' else '1' end as count,"
					+ "                           t.TEMPLATE_ID," + "                           t.FILE_ID,"
					+ "                           t.sort_id" + "             from jecn_rule_t t"
					+ "             left join jecn_rule jr on t.id = jr.id"
					+ "             left join jecn_rule_t sub on sub.per_id=t.id"
					+ "             where t.DEL_STATE=0 and t.rule_name like ? and t.project_id = ?"
					+ "             order by t.per_id, t.sort_id, t.id";
			List<Object[]> list = ruleDao.listNativeSql(sql, 0, 10, "%" + name + "%", projectId);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 根据名称搜索
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		try {
			String sql = "select distinct t.id," + "                           t.rule_name,"
					+ "                           t.per_id," + "                           t.is_dir,"
					+ "                           t.rule_number," + "                           case"
					+ "                             when jr.id is null then" + "                              '0'"
					+ "                             else" + "                              '1'"
					+ "                           end as is_pub,"
					+ "                            case when sub.id is null then '0' else '1' end as count,"
					+ "                           t.TEMPLATE_ID," + "                           t.FILE_ID,"
					+ "                           t.sort_id" + "             from jecn_rule_t t"
					+ "             left join jecn_rule jr on t.id = jr.id"
					+ "             left join jecn_rule_t sub on sub.per_id=t.id";
			// 拼装角色权限sql
			sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(3, peopleId, projectId);
			sql = sql + "             where t.rule_name like ? and t.project_id = ?"
					+ "             order by t.per_id, t.sort_id, t.id";
			List<Object[]> list = ruleDao.listNativeSql(sql, 0, 10, "%" + name + "%", projectId);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long ruleId, Long projectId) throws Exception {
		try {
			RuleT rule = ruleDao.get(ruleId);
			Set<Long> setIds = JecnCommon.getPositionTreeIds(rule.gettPath(), ruleId);
			String sql = " select distinct t.id,  t.rule_name, t.per_id,"
					+ "  t.is_dir, t.rule_number, case when jr.id is null then  '0' else '1'"
					+ "  end as is_pub,case when sub.id is null then '0' else '1' end as count,"
					+ "   t.TEMPLATE_ID, t.FILE_ID, t.sort_id,t.IS_FILE_LOCAL" + "  from jecn_rule_t t"
					+ "  left join jecn_rule jr on t.id = jr.id"
					+ "  left join jecn_rule_t sub on sub.per_id=t.id and sub.del_state = 0 " + " where t.PER_ID in "
					+ JecnCommonSql.getIdsSet(setIds)
					+ " AND t.DEL_STATE=0 AND t.PROJECT_ID=? ORDER BY t.PER_ID,t.Sort_Id,t.ID";
			List<Object[]> list = ruleDao.listNativeSql(sql, projectId);
			sql = "select t.id,JECN_TASK.STATE,JECN_TASK.APPROVE_TYPE,JECN_TASK.EDIT" + "  from jecn_rule_t t"
					+ " INNER JOIN JECN_TASK_BEAN_NEW JECN_TASK" + "   ON JECN_TASK.R_ID = T.Id"
					+ "  AND JECN_TASK.IS_LOCK = 1" + "  AND JECN_TASK.STATE <> 5"
					+ "  AND JECN_TASK.TASK_TYPE in (2, 3)" + " where t.PER_ID in " + JecnCommonSql.getIdsSet(setIds)
					+ "   AND t.DEL_STATE = 0" + "   AND t.PROJECT_ID = ?";
			List<Object[]> listTask = ruleDao.listNativeSql(sql, projectId);
			return getJecnTreeBeanByObject(list, listTask);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 节点排序
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception {
		try {

			String pViewSort = "";
			if (pId.intValue() != 0) {
				String hql = "from RuleT where id=?";
				List<RuleT> data = ruleDao.listHql(hql, pId);
				pViewSort = data.get(0).getViewSort();
			}

			String hql = "from RuleT where perId=? ";
			List<RuleT> listRuleT = ruleDao.listHql(hql, pId);
			List<RuleT> listUpdateT = new ArrayList<RuleT>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (RuleT ruleT : listRuleT) {
					if (jecnTreeDragBean.getId().equals(ruleT.getId())) {
						if (ruleT.getSortId() == null || (jecnTreeDragBean.getSortId() != ruleT.getSortId().intValue())) {
							ruleT.setSortId(jecnTreeDragBean.getSortId());
							ruleT.setViewSort(JecnUtil.concatViewSort(pViewSort, jecnTreeDragBean.getSortId()));
							listUpdateT.add(ruleT);
							break;
						}
					}
				}

			}

			for (RuleT ruleT : listUpdateT) {
				ruleDao.update(ruleT);
				// 添加日志
				JecnUtil.saveJecnJournal(ruleT.getId(), ruleT.getRuleName(), 4, 5, updatePersonId, ruleDao);
				hql = "update Rule set sortId=? where id=?";
				ruleDao.execteHql(hql, ruleT.getSortId(), ruleT.getId());
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 节点移动
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		try {
			String t_path = "";
			int level = -1;
			RuleT rule = this.get(pId);
			if (pId.intValue() != 0) {
				t_path = rule.gettPath();
				level = rule.gettLevel();
			}
			JecnCommon.updateNodesChildTreeBeans(listIds, pId, "", new Date(), updatePersonId, t_path, level, ruleDao,
					moveNodeType, 1);
			for (Long id : listIds) {
				// 添加日志
				JecnUtil.saveJecnJournal(id, null, 4, 4, updatePersonId, ruleDao);
			}

			if (JecnConfigTool.isRuleArchitectureUploadFile()) {
				// 获取移动的节点对应关联的文件目录集合
				List<Long> moveRelatedIds = this.ruleDao.getRelatedFileIdsByRuleIds(listIds);
				if (moveRelatedIds.isEmpty()) {
					return;
				}
				Long parentRelatedId = -1L;
				if (pId.intValue() == 0) {
					parentRelatedId = 0L;
				} else {
					// 获取节点移动，父节点对应的文件目录ID
					parentRelatedId = createRelatedFileDirByPath(rule.getId(), rule.getProjectId(), updatePersonId);
				}

				fileService.moveFiles(moveRelatedIds, parentRelatedId, updatePersonId, TreeNodeType.fileDir);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author yxw 2012-6-7
	 * @description:删除制度
	 * @param listIds
	 * @return int 0 删除异常，1删除成功；2：存在节点处于任务中
	 * @throws Exception
	 */
	@Override
	// 删除制度
	public void deleteRule(List<Long> listIds, Long updatePersonId) throws Exception {
		try {
			Set<Long> setIds = new HashSet<Long>();
			// 所有制度文件与制度模板文件ID集合
			Set<Long> setRuleIds = new HashSet<Long>();
			String sql = JecnCommonSql.getChildObjectsSqlByTypeDelState(listIds, TreeNodeType.ruleDir);
			List<Object[]> listAll = ruleDao.listNativeSql(sql);
			for (Object[] obj : listAll) {
				if (obj == null || obj[0] == null || obj[1] == null) {
					continue;
				}
				setIds.add(Long.valueOf(obj[0].toString()));
				if (!"0".equals(obj[1].toString())) {
					setRuleIds.add(Long.valueOf(obj[0].toString()));
				}
			}
			// 删除本地上传制度的本地文件
			List<String> listFilePath = new ArrayList<String>();

			if (setRuleIds.size() > 0) {
				// 删除本地上传并放在本地的文件
				sql = "SELECT DISTINCT R_FILE.FILE_PATH FROM JECN_RULE_T JR,G020_RULE_FILE_CONTENT R_FILE"
						+ " WHERE R_FILE.IS_VERSION_LOCAL=0 AND R_FILE.RULE_ID=JR.ID AND JR.IS_DIR=2 AND JR.IS_FILE_LOCAL=1 AND JR.ID IN ("
						+ JecnCommonSql.getChildObjectsSqlGetSingleIdRule(setRuleIds) + ")";
				listFilePath = ruleDao.listNativeSql(sql);

				// 合理化建议附件表中的合理化主键ID集合
				List<String> setrtnlFileIds = new ArrayList<String>();
				// 获取合理化建议附件表所在合理化建议ID集合
				sql = "select id from jecn_rtnl_propose where  RELATION_TYPE = 1 and RELATION_ID in "
						+ JecnCommonSql.getIdsSet(setRuleIds);
				List<Object> list = ruleDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setrtnlFileIds.add(obj.toString());
				}
				// 删除制度相关的合理化建议
				sql = "delete from jecn_rtnl_propose where RELATION_TYPE = 1 and RELATION_ID in "
						+ JecnCommonSql.getIdsSet(setRuleIds);
				ruleDao.execteNative(sql);
				// 删除合理化建议附件表
				if (setrtnlFileIds.size() > 0) {
					sql = "delete from JECN_RTNL_PROPOSE_FILE where PROPOSE_ID in "
							+ JecnCommonSql.getStrs(setrtnlFileIds);
					this.ruleDao.execteNative(sql);
				}
			}

			if (setIds.size() > 0) {
				// 添加日志
				JecnUtil.saveJecnJournals(setIds, 4, 3, updatePersonId, ruleDao);
				ruleDao.deleteRules(setIds);
			}

			if (JecnUtil.isVattiLogin()) {
				DominoOperation.deleteDataFromDomino(setIds, "Rule");
			}
			// 删除本地上传制度的本地文件
			for (String filePath : listFilePath) {
				JecnFinal.deleteFile(filePath);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 删除制度
	public void deleteFalseRule(Set<Long> setIds, Long updatePersonId, Long projectId) throws Exception {
		if (setIds.size() > 0) {
			String sql = "update JECN_RULE_T set del_state=1 where del_state <>2 and  ID in ("
					+ JecnCommonSql.getChildObjectsSqlGetSingleIdRule(setIds) + ")";
			fileDao.execteNative(sql);
			sql = "update JECN_RULE set del_state=1 where  del_state <>2 and  ID in ("
					+ JecnCommonSql.getChildObjectsSqlGetSingleIdRule(setIds) + ")";
			fileDao.execteNative(sql);
			// 添加日志
			JecnUtil.saveJecnJournals(setIds, 4, 6, updatePersonId, ruleDao);

			if (JecnConfigTool.isRuleArchitectureUploadFile()) {// 流程、流程架构允许维护文档管理（支撑文件）
				List<Long> deleteIds = new ArrayList<Long>();
				deleteIds.addAll(setIds);
				// 获取可维护的文件节点
				List<Long> fileIds = ruleDao.getRelatedFileIdsByRuleIds(deleteIds);
				if (fileIds == null || fileIds.isEmpty()) {
					return;
				}
				fileService.deleteFileManage(fileIds, projectId, updatePersonId, TreeNodeType.fileDir);
			}
		}
		if (JecnUtil.isVattiLogin()) {
			DominoOperation.deleteDataFromDomino(setIds, "Rule");
		}
	}

	@Override
	// 上传制度文件
	public void addRuleFile(AddRuleFileData ruleFileData) throws Exception {
		try {
			List<RuleT> list = ruleFileData.getList();
			List<AccessId> orgIds = ruleFileData.getAccIds().getOrgAccessId();
			List<AccessId> posIds = ruleFileData.getAccIds().getPosAccessId();
			List<AccessId> posGroupIds = ruleFileData.getAccIds().getPosGroupAccessId();
			Long updatePersonId = ruleFileData.getUpdatePersonId();
			Date nowDate = new Date();
			List<Long> ids = new ArrayList<Long>();
			for (RuleT ruleT : list) {
				if (ruleT.getCreateDate() == null) {
					ruleT.setCreateDate(nowDate);
				}
				if (ruleT.getUpdateDate() == null) {
					ruleT.setUpdateDate(nowDate);
				}

				ruleDao.save(ruleT);

				// 制度文件（本地上传方式）
				if (ruleT.getIsDir() == 2 && ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 1) {
					// 文件保存数据库
					// 记录文件内容
					G020RuleFileContent jecnFileContent = new G020RuleFileContent();
					// 制度Id
					jecnFileContent.setRuleId(ruleT.getId());
					jecnFileContent.setFileName(ruleT.getRuleName());
					jecnFileContent.setUpdatePeopleId(ruleT.getUpdatePeopleId());
					if (ruleT.getCreateDate() == null) {
						jecnFileContent.setUpdateTime(nowDate);
					} else {
						jecnFileContent.setUpdateTime(ruleT.getCreateDate());
					}

					// 存储数据库，标识为1
					jecnFileContent.setIsVersionLocal(1);
					if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
						jecnFileContent.setFileStream(Hibernate.createBlob(ruleT.getFileBytes()));
						ruleDao.getSession().save(jecnFileContent);
					} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
						jecnFileContent.setFileStream(Hibernate.createBlob(new byte[1]));
						ruleDao.getSession().save(jecnFileContent);
						ruleDao.getSession().flush();
						ruleDao.getSession().refresh(jecnFileContent, LockMode.UPGRADE);
						SerializableBlob sb = (SerializableBlob) jecnFileContent.getFileStream();
						java.sql.Blob wrapBlob = sb.getWrappedBlob();
						BLOB blob = (BLOB) wrapBlob;
						OutputStream out = null;
						try {
							out = blob.getBinaryOutputStream();
							out.write(ruleT.getFileBytes());
						} catch (Exception ex) {
							log.error("写blob大字段错误！", ex);
							throw ex;
						} finally {
							if (out != null) {
								out.close();
							}
						}
					}
					ruleT.setFileId(jecnFileContent.getId());
					ruleDao.update(ruleT);
				}
				updateTpathAndTLevel(ruleT);
				// 添加日志
				JecnUtil.saveJecnJournal(ruleT.getId(), ruleT.getRuleName(), 4, 1, updatePersonId, ruleDao);
				// 保存岗位查阅权限
				positionAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, posIds, true);
				// 保存部门查阅权限
				orgAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, orgIds, true);
				// 岗位组查阅权限
				posGroupAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, posGroupIds, true);
				ids.add(ruleT.getId());
			}

			JecnTaskCommon.inheritTaskTemplate(ruleDao, ids, 2, list.get(0).getCreatePeopleId());

			if (JecnConfigTool.isRuleArchitectureUploadFile()) {
				RuleT ruleT = list.get(0);
				uploadFileByRuleLocal(ruleT, updatePersonId, ruleFileData);
			}

			// 更新编号流水号
			if (JecnConfigTool.isMengNiuViewFileCode()) {
				autoCodeDao.updateAutoCodeTableByRelatedId(list.get(0).getPerId(), 2, ruleFileData.getTotalCode());
			}
			// 标准 自动编号
			if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {
				autoCodeDao.updateStandardizedAutoCode(list.get(0).getPerId(), 2, ruleFileData.getTotalCode());
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 制度本地上传-关联标准化文件
	 * 
	 * @param ruleT
	 * @param updatePersonId
	 * @param ruleFileData
	 * @throws Exception
	 */
	private void uploadFileByRuleLocal(RuleT ruleT, Long updatePersonId, AddRuleFileData ruleFileData) throws Exception {
		if (ruleFileData.getStandardizedFileTs() == null || ruleFileData.getStandardizedFileTs().isEmpty()) {
			return;
		}
		Long fileId = createRelatedFileDirByPath(ruleT.getId(), ruleT.getProjectId(), updatePersonId);
		// 上传文件
		if (fileId != null) {
			List<JecnFileBeanT> fileList = new ArrayList<JecnFileBeanT>();
			int sortId = 0;

			for (RuleStandardizedFileT standardizedFileT : ruleFileData.getStandardizedFileTs()) {
				fileList.add(getFileBeanT(standardizedFileT, fileId, sortId, ruleT));
				sortId++;
			}
			fileService.addFiles(fileList, new AccessId(), false, -1);

			// // 保存制度相关标准化文件
			// saveOrUpdateRelatedStandardizedFiles(ruleT.getId(), fileIds);
		}
	}

	/**
	 * 更新适用部门
	 * 
	 * @param orgIds
	 * @param ruleId
	 */
	private void updateApplyOrgs(List<JecnTreeBean> list, Long ruleId) {
		String hql = "from JecnRuleApplyOrgT where relateId=?";
		List<JecnRuleApplyOrgT> listOld = ruleDao.listHql(hql, ruleId);
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Long orgId = jecnTreeBean.getId();
				if (orgId != null) {
					boolean isExist = false;
					for (JecnRuleApplyOrgT jecnRuleApplyOrgT : listOld) {
						if (jecnRuleApplyOrgT.getOrgId().equals(orgId)) {
							isExist = true;
							listOld.remove(jecnRuleApplyOrgT);
							break;
						}
					}
					if (!isExist) {
						JecnRuleApplyOrgT jecnRuleApplyOrgT = new JecnRuleApplyOrgT();
						jecnRuleApplyOrgT.setId(JecnCommon.getUUID());
						jecnRuleApplyOrgT.setOrgId(orgId);
						jecnRuleApplyOrgT.setRelateId(ruleId);
						ruleDao.getSession().save(jecnRuleApplyOrgT);
					}

				}
			}
		}
		for (JecnRuleApplyOrgT jecnRuleApplyOrgT : listOld) {
			ruleDao.getSession().delete(jecnRuleApplyOrgT);
		}
	}

	@Override
	// 更新制度信息
	public Long updateRule(JecnRuleData ruleData) throws Exception {
		RuleT ruleT = ruleData.getRuelBean();
		List<JecnRuleOperationCommon> listJecnRuleOperationCommon = ruleData.getListJecnRuleOperationCommon();
		List<AccessId> orgIds = ruleData.getAccIds().getOrgAccessId();
		List<AccessId> posIds = ruleData.getAccIds().getPosAccessId();
		List<AccessId> posGroupIds = ruleData.getAccIds().getPosGroupAccessId();

		Long updatePersonId = ruleData.getUpdatePersonId();
		Date nowDate = new Date();
		boolean isCreate = false;
		try {
			ruleT.setUpdateDate(nowDate);
			boolean isSave = false;
			if (ruleT.getId() == null) {
				isCreate = true;
				ruleT.setCreateDate(new Date());
				// 标准 自动编号
				if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {
					AutoCodeResultData resultData = AutoCodeEnum.INSTANCE.getStandardizedAutoCodeResultData(ruleT
							.getPerId(), 2, autoCodeService);
					ruleT.setRuleNumber(resultData.getCode());
				}
				isSave = true;
				ruleDao.save(ruleT);
				// 添加日志
				JecnUtil.saveJecnJournal(ruleT.getId(), ruleT.getRuleName(), 4, 1, updatePersonId, ruleDao);
				// 保存岗位查阅权限
				positionAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, posIds, true);
				// 保存部门查阅权限
				orgAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, orgIds, true);
				// 保存岗位组查阅权限
				posGroupAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, posGroupIds, true);
			} else {
				ruleDao.update(ruleT);
				// 修改日志
				JecnUtil.saveJecnJournal(ruleT.getId(), ruleT.getRuleName(), 4, 2, updatePersonId, ruleDao);
				// 保存岗位查阅权限
				positionAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, posIds, false);
				// 保存部门查阅权限
				orgAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, orgIds, false);
				// 保存岗位组查阅权限
				posGroupAccessPermissionsDao.savaOrUpdate(ruleT.getId(), 3, posGroupIds, false);
			}
			// 业务范围和其他范围
			JecnRuleBaiscInfoT dbRuleBaiscInfoT = this.getJecnRuleBaiscInfoTById(ruleT.getId());

			if (dbRuleBaiscInfoT == null) {
				dbRuleBaiscInfoT = new JecnRuleBaiscInfoT();
				updateRuleBasicInfo(ruleT, dbRuleBaiscInfoT);
				ruleDao.getSession().save(dbRuleBaiscInfoT);
			} else {
				updateRuleBasicInfo(ruleT, dbRuleBaiscInfoT);
				ruleDao.getSession().update(dbRuleBaiscInfoT);
				ruleDao.getSession().flush();
			}

			// 组织范围
			this.updateApplyOrgs(ruleT.getOrgScopes(), ruleT.getId());

			// 制度模板
			if (listJecnRuleOperationCommon != null && ruleT.getIsDir() != null && ruleT.getIsDir().intValue() == 1) {
				List<RuleFileT> listRuleFileTData = new ArrayList<RuleFileT>();
				if (!isSave) {
					/** 文件表单更新 */
					String hql = "from RuleFileT where ruleTitleId in (select id from RuleTitleT where ruleId=?)";
					listRuleFileTData = ruleDao.listHql(hql, ruleT.getId());
				}
				for (JecnRuleOperationCommon jecnRuleOperationCommon : listJecnRuleOperationCommon) {
					RuleTitleT ruleTitleT = jecnRuleOperationCommon.getRuleTitleT();
					ruleTitleT.setRuleId(ruleT.getId());
					if (ruleTitleT.getId() == null) {
						ruleDao.getSession().save(ruleTitleT);
					} else {
						ruleDao.getSession().update(ruleTitleT);
					}
					Long ruleTitleId = ruleTitleT.getId();
					int type = ruleTitleT.getType();
					if (type == 0) {
						RuleContentT ruleContentT = jecnRuleOperationCommon.getRuleContentT();
						ruleContentT.setRuleTitleId(ruleTitleId);
						if (ruleContentT.getId() == null) {
							ruleDao.getSession().save(ruleContentT);
						} else {
							ruleDao.getSession().update(ruleContentT);
						}
					} else if (type == 1) {
						List<RuleFileT> listRuleFileT = jecnRuleOperationCommon.getListFileBean();
						for (RuleFileT ruleFileT : listRuleFileT) {
							ruleFileT.setRuleTitleId(ruleTitleId);
							boolean isExist = false;
							for (RuleFileT ruleFileTData : listRuleFileTData) {
								if (!ruleTitleId.equals(ruleFileTData.getRuleTitleId())) {
									continue;
								}
								if (ruleFileT.getRuleFileId().equals(ruleFileTData.getRuleFileId())) {
									isExist = true;
									// 不需要删除文件
									listRuleFileTData.remove(ruleFileTData);
									break;
								}
							}
							if (!isExist) {
								ruleDao.getSession().save(ruleFileT);
							}
						}
					}
				}
				for (RuleFileT ruleFileTData : listRuleFileTData) {
					ruleDao.getSession().delete(ruleFileTData);
				}
			}
			// 制度相关风险
			if (ruleData.getRuleRiskTList() != null) {// 
				// 获取当前制度对应的相关风险
				String hql = "from JecnRuleRiskBeanT t where t.ruleId = ?";
				List<JecnRuleRiskBeanT> list = this.ruleDao.listHql(hql, ruleT.getId());
				// 获取当前制度相关风险
				for (Long riskId : ruleData.getRuleRiskTList()) {
					boolean isExist = false;
					for (JecnRuleRiskBeanT riskBeanT : list) {
						if (riskId.equals(riskBeanT.getRiskId())) {
							isExist = true;
							list.remove(riskBeanT);
							break;
						}
					}
					if (!isExist) {
						JecnRuleRiskBeanT riskBeanT = new JecnRuleRiskBeanT();
						riskBeanT.setId(JecnCommon.getUUID());
						riskBeanT.setRiskId(riskId);
						riskBeanT.setRuleId(ruleT.getId());
						ruleDao.getSession().save(riskBeanT);
					}
				}
				for (JecnRuleRiskBeanT riskBean : list) {
					ruleDao.getSession().delete(riskBean);
				}
			}
			// 制度相关制度
			if (ruleData.getRuleInstitutionList() != null) {// 
				// 获取当前制度对应的相关风险
				String hql = "from JecnRuleInstitutionBeanT t where t.ruleId = ?";
				List<JecnRuleInstitutionBeanT> list = this.ruleDao.listHql(hql, ruleT.getId());
				// 获取当前制度相关风险
				for (Long ruleId : ruleData.getRuleInstitutionList()) {
					boolean isExist = false;
					for (JecnRuleInstitutionBeanT ruleBeanT : list) {
						if (ruleId.equals(ruleBeanT.getRelatedId())) {
							isExist = true;
							list.remove(ruleBeanT);
							break;
						}
					}
					if (!isExist) {
						JecnRuleInstitutionBeanT ruleBeanT = new JecnRuleInstitutionBeanT();
						ruleBeanT.setId(JecnCommon.getUUID());
						ruleBeanT.setRuleId(ruleT.getId());
						ruleBeanT.setRelatedId(ruleId);
						ruleDao.getSession().save(ruleBeanT);
					}
				}
				for (JecnRuleInstitutionBeanT ruleBean : list) {
					ruleDao.getSession().delete(ruleBean);
				}
			}
			// 制度相关标准
			if (ruleData.getStandardTList() != null) {// 制度相关标准
				// 获取当前制度对应的相关风险
				String hql = "from JecnRuleStandardBeanT t where t.ruleId = ?";
				List<JecnRuleStandardBeanT> list = this.ruleDao.listHql(hql, ruleT.getId());
				// 获取当前制度相关标准
				for (Long stanId : ruleData.getStandardTList()) {
					boolean isExist = false;
					for (JecnRuleStandardBeanT standardBeanT : list) {
						if (stanId.equals(standardBeanT.getRelationId())) {
							isExist = true;
							list.remove(standardBeanT);
							break;
						}
					}
					if (!isExist) {
						JecnRuleStandardBeanT standardBeanT = new JecnRuleStandardBeanT();
						standardBeanT.setId(JecnCommon.getUUID());
						standardBeanT.setStandardId(stanId);
						standardBeanT.setRuleId(ruleT.getId());
						ruleDao.getSession().save(standardBeanT);
					}
				}

				for (JecnRuleStandardBeanT standardBeanT : list) {
					ruleDao.getSession().delete(standardBeanT);
				}
			}

			// 标准化文件
			JecnDaoUtil.saveOrUpdateRuleRelatedStandardizedFiles(ruleDao, ruleT.getId(), JecnCommon
					.getIdsByTreeBeans(ruleData.getRelatedStandardizeds()));
			// 更新tpath 和tlevel
			updateTpathAndTLevel(ruleT);
			if (isCreate) {
				JecnTaskCommon.inheritTaskTemplate(ruleDao, ruleT.getId(), 2, ruleT.getCreatePeopleId());
			}
			return ruleT.getId();
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private void updateRuleBasicInfo(RuleT ruleT, JecnRuleBaiscInfoT dbRuleBaiscInfoT) {
		JecnRuleBaiscInfoT srcRuleBaiscInfoTBean = ruleT.getJecnRuleBaiscInfoT();
		dbRuleBaiscInfoT.setRuleId(ruleT.getId());
		// 注入属性
		dbRuleBaiscInfoT.setBusinessScope(ruleT.getBusinessScope());
		dbRuleBaiscInfoT.setOtherScope(ruleT.getOtherScope());
		if (srcRuleBaiscInfoTBean != null) {
			dbRuleBaiscInfoT.setPurpose(srcRuleBaiscInfoTBean.getPurpose());
			dbRuleBaiscInfoT.setApplicability(srcRuleBaiscInfoTBean.getApplicability());
			dbRuleBaiscInfoT.setDraftingUnit(srcRuleBaiscInfoTBean.getDraftingUnit());
			dbRuleBaiscInfoT.setDraftman(srcRuleBaiscInfoTBean.getDraftman());
			dbRuleBaiscInfoT.setWriteDept(srcRuleBaiscInfoTBean.getWriteDept());
			dbRuleBaiscInfoT.setTestRunFile(srcRuleBaiscInfoTBean.getTestRunFile());
			dbRuleBaiscInfoT.setChangeVersionExplain(srcRuleBaiscInfoTBean.getChangeVersionExplain());
		}
	}

	/**
	 * 制度相关风险
	 * 
	 * @param riskTList
	 *            制度相关风险集合
	 * @param ruleId
	 *            制度主键ID
	 */
	@Override
	public void editRuleRiskT(List<JecnRuleRiskBeanT> riskTList, Long ruleId) throws Exception {
		if (riskTList != null) {// 制度相关标准
			// 获取当前制度对应的相关风险
			String hql = "from JecnRuleRiskBeanT t where t.ruleId = ?";
			List<JecnRuleRiskBeanT> list = this.ruleDao.listHql(hql, ruleId);
			// 获取当前制度相关风险
			List<String> refIds = new ArrayList<String>();
			for (JecnRuleRiskBeanT riskBeanT : riskTList) {
				if (riskBeanT.getId() == null) {
					riskBeanT.setId(JecnCommon.getUUID());
					ruleDao.getSession().save(riskBeanT);
				}
				refIds.add(riskBeanT.getId());
			}

			for (JecnRuleRiskBeanT riskBean : list) {// 循环数据库已存在的风险集合
				if (refIds.contains(riskBean.getId())) {// 如果在当前数据中，数据库ID不存在，责记录删除
					continue;
				} else {
					ruleDao.getSession().delete(riskBean);
				}
			}
		}
	}

	/**
	 * 
	 * 制度相关标准
	 * 
	 * @param standardTList
	 *            制度相关标准集合
	 * @param ruleId
	 *            主动主键ID
	 */
	@Override
	public void editRuleStandardT(List<JecnRuleStandardBeanT> standardTList, Long ruleId) throws Exception {
		if (standardTList != null) {// 制度相关标准
			// 获取当前制度对应的相关风险
			String hql = "from JecnRuleStandardBeanT t where t.ruleId = ?";
			List<JecnRuleStandardBeanT> list = this.ruleDao.listHql(hql, ruleId);
			// 获取当前制度相关标准
			List<String> refIds = new ArrayList<String>();
			for (JecnRuleStandardBeanT standardBeanT : standardTList) {
				if (standardBeanT.getId() == null) {
					standardBeanT.setId(JecnCommon.getUUID());
					ruleDao.getSession().save(standardBeanT);
				}
				refIds.add(standardBeanT.getId());
			}

			for (JecnRuleStandardBeanT standardBeanT : list) {
				if (refIds.contains(standardBeanT.getId())) {
					continue;
				} else {
					ruleDao.getSession().delete(standardBeanT);
				}
			}
		}
	}

	@Override
	// 新建制度信息
	public Long addRule(RuleT ruelBean, String posIds, String orgIds, Long modeId, Long updatePersonId)
			throws Exception {
		try {
			ruelBean.setCreateDate(new Date());
			ruelBean.setUpdateDate(new Date());
			Long ruleId = ruleDao.save(ruelBean);
			// 添加日志
			JecnUtil.saveJecnJournal(ruelBean.getId(), ruelBean.getRuleName(), 4, 1, updatePersonId, ruleDao);
			if (modeId != null) {
				String hql = "from RuleModeTitleBean where modeId=? order by sortId";
				List<RuleModeTitleBean> listRuleModeTitleBean = ruleDao.listHql(hql, modeId);
				for (RuleModeTitleBean ruleModeTitleBean : listRuleModeTitleBean) {
					RuleTitleT ruleTitleT = new RuleTitleT();
					ruleTitleT.setRuleId(ruelBean.getId());
					ruleTitleT.setOrderNumber(ruleModeTitleBean.getSortId());
					ruleTitleT.setTitleName(ruleModeTitleBean.getTitleName());
					ruleTitleT.setType(ruleModeTitleBean.getType());
					ruleDao.getSession().save(ruleTitleT);
				}
			}

			// 保存岗位查阅权限
			positionAccessPermissionsDao.savaOrUpdate(ruelBean.getId(), 3, posIds, true);
			// 保存部门查阅权限
			orgAccessPermissionsDao.savaOrUpdate(ruelBean.getId(), 3, orgIds, true);

			return ruleId;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 根据制度ID获得制度操作说明信息
	public List<JecnRuleOperationCommon> findRuleOperationShow(Long ruleId, boolean isPub) throws Exception {
		try {
			// 通过制度ID找到制度内容
			List<JecnRuleOperationCommon> listResult = new ArrayList<JecnRuleOperationCommon>();
			// 通过制度ID找到制度标题
			List<RuleTitleT> listRuleTitleT = ruleDao.findRuleTitle(ruleId);
			if (listRuleTitleT.size() == 0) {
				return null;
			}
			// 通过制度ID找到制度流程
			List<JecnFlowCommon> listJecnFlowCommon = ruleDao.findRuleFlow(ruleId, 1L, 0, isPub);
			// 通过制度ID找到制度流程地图
			List<JecnFlowCommon> listJecnFlowMapCommon = ruleDao.findRuleFlow(ruleId, 0L, 0, isPub);
			// 通过制度ID找到制度标准
			List<JecnStandarCommon> listJecnStandarCommon = ruleDao.findRuleStandar(ruleId, 0);
			// 通过制度ID找到制度风险
			List<JecnRiskCommon> listJecnRiskCommon = ruleDao.findRuleRisk(ruleId, 0);
			// 通过制度ID找到制度关联制度
			List<JecnRuleCommon> listJecnRuleCommon = ruleDao.findRule(ruleId, 0, isPub);
			if (isPub) {
				List<RuleContent> listResultRule = ruleDao.findRuleContent(ruleId);
				// 通过制度ID找到制度文件
				List<RuleFile> listRuleFile = ruleDao.findRuleFile(ruleId);
				// 制度对象
				Rule rule = ruleDao.getRuleById(ruleId);
				for (RuleTitleT ruleTitleT : listRuleTitleT) {
					JecnRuleOperationCommon jecnRuleOperationCommon = new JecnRuleOperationCommon();
					// 制度名称
					jecnRuleOperationCommon.setRuleName(rule.getRuleName());
					jecnRuleOperationCommon.setRuleId(ruleId);
					jecnRuleOperationCommon.setRuleTitleT(ruleTitleT);
					Long ruleTitleId = ruleTitleT.getId();
					Integer type = ruleTitleT.getType();
					if (type != null) {
						if (type.intValue() == 0) {
							/** 通过制度标题Id来找到制度内容 */
							jecnRuleOperationCommon.setRuleContent(this.getRuleContentByRuleTitleId(ruleTitleId,
									listResultRule));
						} else if (type.intValue() == 1) {
							/** 通过制度标题Id来找到制度文件 */
							jecnRuleOperationCommon.setListRuleFile(this.getRuleFileByRuleTitleId(ruleTitleId,
									listRuleFile));
						} else if (type.intValue() == 2) {
							/** 通过制度标题Id来找到制度流程 */
							jecnRuleOperationCommon.setListJecnFlowCommon(this.getJecnFlowCommonByRuleTitleId(
									ruleTitleId, listJecnFlowCommon));
						} else if (type.intValue() == 3) {
							/** 通过制度标题Id来找到制度流程地图 */
							jecnRuleOperationCommon.setListJecnFlowMapCommon(this.getJecnFlowMapCommonByRuleTitleId(
									ruleTitleId, listJecnFlowMapCommon));
						} else if (type.intValue() == 4) {
							/** 通过制度标题Id来找到制度标准 */
							jecnRuleOperationCommon.setListJecnStandarCommon(this.getJecnStandarCommonByRuleTitleId(
									ruleTitleId, listJecnStandarCommon));
						} else if (type.intValue() == 5) {
							/** 通过制度标题Id来找到制度风险 */
							jecnRuleOperationCommon.setListJecnRiskCommon(this.getJecnRiskCommonByRuleTitleId(
									ruleTitleId, listJecnRiskCommon));
						} else if (type.intValue() == 6) {
							/** 通过制度标题Id来找到制度 */
							jecnRuleOperationCommon.setListJecnRuleCommon(this.getJecnRuleCommonByRuleTitleId(
									ruleTitleId, listJecnRuleCommon));
						}
					}
					jecnRuleOperationCommon.setShowPub(true);
					listResult.add(jecnRuleOperationCommon);
				}
				return listResult;
			} else {
				List<RuleContentT> listResultRuleT = ruleDao.findRuleContentT(ruleId);
				// 通过制度ID找到制度文件
				List<RuleFileT> listRuleFileT = ruleDao.findRuleFileT(ruleId);
				// 制度对象
				RuleT ruleT = ruleDao.getRuleTById(ruleId);
				for (RuleTitleT ruleTitleT : listRuleTitleT) {
					JecnRuleOperationCommon jecnRuleOperationCommon = new JecnRuleOperationCommon();
					// 制度名称
					jecnRuleOperationCommon.setRuleName(ruleT.getRuleName());
					jecnRuleOperationCommon.setRuleTitleT(ruleTitleT);
					jecnRuleOperationCommon.setRuleId(ruleId);
					Long ruleTitleId = ruleTitleT.getId();
					Integer type = ruleTitleT.getType();
					if (type != null) {
						if (type.intValue() == 0) {
							/** 通过制度标题Id来找到制度内容 */
							jecnRuleOperationCommon.setRuleContentT(this.getRuleContentTByRuleTitleId(ruleTitleId,
									listResultRuleT));
						} else if (type.intValue() == 1) {
							/** 通过制度标题Id来找到制度文件 */
							jecnRuleOperationCommon.setListFileBean(this.getRuleFileTByRuleTitleId(ruleTitleId,
									listRuleFileT));
						} else if (type.intValue() == 2) {
							/** 通过制度标题Id来找到制度流程 */
							jecnRuleOperationCommon.setListJecnFlowCommon(this.getJecnFlowCommonByRuleTitleId(
									ruleTitleId, listJecnFlowCommon));
						} else if (type.intValue() == 3) {
							/** 通过制度标题Id来找到制度流程地图 */
							jecnRuleOperationCommon.setListJecnFlowMapCommon(this.getJecnFlowMapCommonByRuleTitleId(
									ruleTitleId, listJecnFlowMapCommon));
						} else if (type.intValue() == 4) {
							/** 通过制度标题Id来找到制度标准 */
							jecnRuleOperationCommon.setListJecnStandarCommon(this.getJecnStandarCommonByRuleTitleId(
									ruleTitleId, listJecnStandarCommon));
						} else if (type.intValue() == 5) {
							/** 通过制度标题Id来找到制度风险 */
							jecnRuleOperationCommon.setListJecnRiskCommon(this.getJecnRiskCommonByRuleTitleId(
									ruleTitleId, listJecnRiskCommon));
						} else if (type.intValue() == 6) {
							/** 通过制度标题Id来找到制度 */
							jecnRuleOperationCommon.setListJecnRuleCommon(this.getJecnRuleCommonByRuleTitleId(
									ruleTitleId, listJecnRuleCommon));
						}
					}
					jecnRuleOperationCommon.setShowPub(false);
					listResult.add(jecnRuleOperationCommon);
				}
				return listResult;
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 通过制度标题Id来找到制度内容
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private RuleContentT getRuleContentTByRuleTitleId(Long ruleTitleId, List<RuleContentT> listRuleContentT) {
		for (RuleContentT ruleContentT : listRuleContentT) {
			if (ruleContentT.getRuleTitleId() != null && ruleTitleId.equals(ruleContentT.getRuleTitleId())) {
				return ruleContentT;
			}
		}
		return new RuleContentT();
	}

	/**
	 * 通过制度标题Id来找到制度内容
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private RuleContent getRuleContentByRuleTitleId(Long ruleTitleId, List<RuleContent> listRuleContent) {
		for (RuleContent ruleContent : listRuleContent) {
			if (ruleContent.getRuleTitleId() != null && ruleTitleId.equals(ruleContent.getRuleTitleId())) {
				return ruleContent;
			}
		}
		return new RuleContent();
	}

	/**
	 * 通过制度标题Id来找到制度文件
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private List<RuleFileT> getRuleFileTByRuleTitleId(Long ruleTitleId, List<RuleFileT> listRuleFileT) {
		List<RuleFileT> listResult = new ArrayList<RuleFileT>();
		for (RuleFileT ruleFileT : listRuleFileT) {
			if (ruleFileT.getRuleTitleId() != null && ruleTitleId.equals(ruleFileT.getRuleTitleId())) {
				listResult.add(ruleFileT);
			}
		}
		return listResult;
	}

	/**
	 * 通过制度标题Id来找到制度文件
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private List<RuleFile> getRuleFileByRuleTitleId(Long ruleTitleId, List<RuleFile> listRuleFile) {
		List<RuleFile> listResult = new ArrayList<RuleFile>();
		for (RuleFile ruleFile : listRuleFile) {
			if (ruleFile.getRuleTitleId() != null && ruleTitleId.equals(ruleFile.getRuleTitleId())) {
				listResult.add(ruleFile);
			}
		}
		return listResult;
	}

	/**
	 * 通过制度标题Id来找到制度流程
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private List<JecnFlowCommon> getJecnFlowCommonByRuleTitleId(Long ruleTitleId,
			List<JecnFlowCommon> listJecnFlowCommon) {
		List<JecnFlowCommon> listResult = new ArrayList<JecnFlowCommon>();
		for (JecnFlowCommon jecnFlowCommon : listJecnFlowCommon) {
			if (jecnFlowCommon.getRuleTitleId() != null && ruleTitleId.equals(jecnFlowCommon.getRuleTitleId())) {
				listResult.add(jecnFlowCommon);
			}
		}
		return listResult;
	}

	/**
	 * 通过制度标题Id来找到制度流程地图
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private List<JecnFlowCommon> getJecnFlowMapCommonByRuleTitleId(Long ruleTitleId,
			List<JecnFlowCommon> listJecnFlowMapCommon) {
		List<JecnFlowCommon> listResult = new ArrayList<JecnFlowCommon>();
		for (JecnFlowCommon jecnFlowMapCommon : listJecnFlowMapCommon) {
			if (jecnFlowMapCommon.getRuleTitleId() != null && ruleTitleId.equals(jecnFlowMapCommon.getRuleTitleId())) {
				listResult.add(jecnFlowMapCommon);
			}
		}
		return listResult;
	}

	/**
	 * 通过制度标题Id来找到制度标准
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private List<JecnStandarCommon> getJecnStandarCommonByRuleTitleId(Long ruleTitleId,
			List<JecnStandarCommon> listJecnStandarCommon) {
		List<JecnStandarCommon> listResult = new ArrayList<JecnStandarCommon>();
		for (JecnStandarCommon jecnStandarCommon : listJecnStandarCommon) {
			if (jecnStandarCommon.getRuleTitleId() != null && ruleTitleId.equals(jecnStandarCommon.getRuleTitleId())) {
				listResult.add(jecnStandarCommon);
			}
		}
		return listResult;
	}

	/**
	 * 通过制度标题Id来找到制度风险
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private List<JecnRiskCommon> getJecnRiskCommonByRuleTitleId(Long ruleTitleId,
			List<JecnRiskCommon> listJecnRiskCommon) {
		List<JecnRiskCommon> listResult = new ArrayList<JecnRiskCommon>();
		for (JecnRiskCommon jecnRiskCommon : listJecnRiskCommon) {
			if (jecnRiskCommon.getRuleTitleId() != null && ruleTitleId.equals(jecnRiskCommon.getRuleTitleId())) {
				listResult.add(jecnRiskCommon);
			}
		}
		return listResult;
	}

	/**
	 * 通过制度标题Id来找到相关制度
	 * 
	 * @param ruleTitleId
	 * @param listRuleContentT
	 * @return
	 */
	private List<JecnRuleCommon> getJecnRuleCommonByRuleTitleId(Long ruleTitleId,
			List<JecnRuleCommon> listJecnRuleCommon) {
		List<JecnRuleCommon> listResult = new ArrayList<JecnRuleCommon>();
		for (JecnRuleCommon jecnRuleCommon : listJecnRuleCommon) {
			if (jecnRuleCommon.getRuleTitleId() != null && ruleTitleId.equals(jecnRuleCommon.getRuleTitleId())) {
				listResult.add(jecnRuleCommon);
			}
		}
		return listResult;
	}

	@Override
	@SuppressWarnings("all")
	public RuleT getRuleT(Long id) throws Exception {
		try {
			// 通过制度ID查找制度临时表信息
			RuleT ruleT = ruleDao.get(id);
			if (ruleT == null) {
				return null;
			} else {
				if (ruleT.getIsDir() != null && ruleT.getIsDir().intValue() == 2 && ruleT.getIsFileLocal() != null
						&& ruleT.getIsFileLocal().intValue() == 0 && ruleT.getFileId() != null) {
					JecnFileBeanT jecnFileBean = fileDao.get(ruleT.getFileId());
					if (jecnFileBean != null && jecnFileBean.getDocId() != null) {
						ruleT.setRuleNumber(jecnFileBean.getDocId());
					}
				}

				if (ruleT.getOrgId() != null) {
					JecnFlowOrg jecnFlowOrg = organizationService.get(ruleT.getOrgId());
					if (jecnFlowOrg != null && jecnFlowOrg.getDelState() != null && jecnFlowOrg.getOrgName() != null) {
						ruleT.setOrgName(jecnFlowOrg.getOrgName());
					}
				}
				if (ruleT.getTypeId() != null) {
					ProceeRuleTypeBean processRuleType = this.processRuleTypeDao.get(ruleT.getTypeId());
					if (processRuleType != null && processRuleType.getTypeName() != null) {
						ruleT.setTypeName(processRuleType.getTypeName());
					}
				}
				if (ruleT.getAttchMentId() != null) {
					if (ruleT.getTypeResPeople() == 0) { // 人员
						JecnUser jecnUser = this.userDao.get(ruleT.getAttchMentId());
						if (jecnUser != null) {
							ruleT.setAttchMentName(jecnUser.getTrueName());
						}
					} else { // 岗位
						JecnFlowOrgImage flowOrgImage = this.flowOrgImageDao.get(ruleT.getAttchMentId());
						if (flowOrgImage != null) {
							ruleT.setAttchMentName(flowOrgImage.getFigureText());
						}
					}
				}
				// 监护人
				if (ruleT.getGuardianId() != null) {
					JecnUser jecnUser = this.userDao.get(ruleT.getGuardianId());
					if (jecnUser != null) {
						ruleT.setGuardianName(jecnUser.getTrueName());
					}
				}
				if (ruleT.getFictionPeopleId() != null) {
					JecnUser jecnUser = this.userDao.get(ruleT.getFictionPeopleId());
					if (jecnUser != null) {
						ruleT.setFictionPeopleName(jecnUser.getTrueName());
					}
				}
				JecnRuleBaiscInfoT jecnRuleBaiscInfoT = getJecnRuleBaiscInfoTById(id);
				ruleT.setJecnRuleBaiscInfoT(jecnRuleBaiscInfoT == null ? new JecnRuleBaiscInfoT() : jecnRuleBaiscInfoT);
				if (jecnRuleBaiscInfoT != null) {
					ruleT.setBusinessScope(jecnRuleBaiscInfoT.getBusinessScope() == null ? "" : jecnRuleBaiscInfoT
							.getBusinessScope());
					ruleT.setOtherScope(jecnRuleBaiscInfoT.getOtherScope() == null ? "" : jecnRuleBaiscInfoT
							.getOtherScope());
				}
				// 组织范围
				String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,0"
						+ " from jecn_flow_org jfo,JECN_RULE_APPLY_ORG_T t where jfo.org_id = t.org_id and t.RELATED_ID=? and jfo.del_state=0"
						+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
				List<Object[]> listOrg = userDao.listNativeSql(sql, id);
				List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
				for (Object[] obj : listOrg) {
					JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrg(obj);
					if (jecnTreeBean != null) {
						listResult.add(jecnTreeBean);
					}
				}
				ruleT.setOrgScopes(listResult);

				// 专员
				if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleCommissioner)
						&& ruleT.getCommissionerId() != null) {
					JecnUser jecnUser = this.userDao.get(ruleT.getCommissionerId());
					if (jecnUser != null) {
						ruleT.setCommissionerName(jecnUser.getTrueName());
					}
				}
			}
			return ruleT;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private JecnRuleBaiscInfoT getJecnRuleBaiscInfoTById(Long ruleId) throws Exception {
		return ruleDao.getJecnRuleBaiscInfoTById(ruleId);
	}

	@Override
	public void updateRuleFile(long id, long fileId, String fileName, Long updatePersonId) throws Exception {
		try {
			String hql = "update RuleT set fileId=?,ruleName=?,updatePeopleId=?,updateDate=? where id=?";
			ruleDao.execteHql(hql, fileId, fileName, updatePersonId, new Date(), id);
			// 添加日志
			JecnUtil.saveJecnJournal(id, fileName, 4, 2, updatePersonId, ruleDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateRuleMode(long ruleId, long modeId, Long updatePersonId, Long longFlag) throws Exception {
		try {
			RuleT ruleT = ruleDao.get(ruleId);
			if ("0".equals(longFlag.toString())) {
				ruleT.setTemplateId(null);
			} else if ("1".equals(longFlag.toString())) {
				ruleT.setTemplateId(modeId);
			}
			ruleDao.update(ruleT);
			// 设置制度模板
			// 添加日志
			JecnUtil.saveJecnJournal(ruleT.getId(), ruleT.getRuleName(), 4, 2, updatePersonId, ruleDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void cancelRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
		try {
			List<Long> list = new ArrayList<Long>();
			list.add(id);
			if (type == TreeNodeType.ruleFile) {
				ruleDao.revokeRuleFileData(list);
			} else if (type == TreeNodeType.ruleModeFile) {
				ruleDao.revokeRuleModeData(list);
			}
			JecnUtil.saveJecnJournals(list, 4, 18, peopleId, ruleDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void directRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
		try {
			List<Long> list = new ArrayList<Long>();
			list.add(id);
			// 获取文件更新时间
			String sql = "SELECT pub_time FROM JECN_RULE WHERE ID = ?";
			Date pubTime = ruleDao.getObjectNativeSql(sql, id);
			if (pubTime == null) {
				pubTime = new Date();
			}
			// 还原发布时间:(不记录文控信息发布，不能更改发布时间)
			sql = " UPDATE JECN_RULE_T SET UPDATE_DATE =?,pub_time=? WHERE ID = ?";
			this.ruleDao.execteNative(sql, pubTime, pubTime, id);

			if (type == TreeNodeType.ruleFile) { // 制度文件
				ruleDao.releaseRuleFileData(list);
				// 未发布文件
				List<Long> listFileIds = ruleDao.findRuleFileNoPubFiles(id);
				if (listFileIds.size() > 0) {
					this.fileDao.releaseFiles(listFileIds);
				}

			} else if (type == TreeNodeType.ruleModeFile) {// 制度模板文件
				ruleDao.releaseRuleModeData(list);
				if (!JecnTaskCommon.validateRelateFile(2)) {
					return;
				}
				// 未发布文件
				List<Long> listFileIds = ruleDao.findRuleModeNoPubFiles(id);
				if (listFileIds.size() > 0) {
					this.fileDao.releaseFiles(listFileIds);
				}
			}
			JecnUtil.saveJecnJournals(list, 4, 17, peopleId, ruleDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public JecnCreateDoc getRuleFile(Long ruleId, boolean isPub) throws Exception {
		try {
			return DownloadUtil.getRuleFile(ruleId, isPub, ruleDao, docControlDao, configItemDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<RuleInfoBean> searchRule(RuleSearchBean ruleSearchBean, int start, int limit) throws Exception {
		try {
			String sql = "select s_rule.* from" + getSearchRuleSQLAdmin(ruleSearchBean);
			if (!ruleSearchBean.isAdmin() && !JecnContants.isSystemAdmin) {
				sql = sql
						+ "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(ruleSearchBean.getPeopleId(), ruleSearchBean
								.getProjectId(), TYPEAUTH.RULE) + " on MY_AUTH_FILES.id=s_rule.id";
			}
			sql += " order by s_rule.per_id,s_rule.sort_id,s_rule.id";
			List<Object[]> listObjects = ruleDao.listNativeSql(sql, start, limit);
			List<RuleInfoBean> listRuleInfoBean = new ArrayList<RuleInfoBean>();
			for (Object[] obj : listObjects) {
				RuleInfoBean ruleInfoBean = JecnUtil.getRuleInfoBeanByObject(obj);
				if (ruleInfoBean != null)
					listRuleInfoBean.add(ruleInfoBean);
			}
			return listRuleInfoBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 制度管理 制度搜索 管理员ROLE
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	private String getSearchRuleSQLAdmin(RuleSearchBean ruleSearchBean) {
		String sql = " (select distinct jr.id,case when jr.is_dir=2 and jr.is_file_local=0 "
				+ "        then jf.file_name else jr.rule_name end as ruleName"
				+ "        ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.doc_id else jr.rule_number end as ruleNumber"
				+ "        ,jf.file_id,jr.is_dir,jr.type_id,jft.type_name,jfo.org_id,jfo.org_name,jr.is_public,jr.pub_time,jr.per_id,jr.sort_id"
				+ " from jecn_rule jr" + " left join jecn_flow_type jft on jft.type_id=jr.type_id"
				+ " left join jecn_flow_org jfo on jfo.del_state=0 and jfo.org_id = jr.org_id"
				+ " left join jecn_file jf on jf.file_id = jr.file_id and jf.del_state=0"
				+ " LEFT JOIN (SELECT GPT.RELATE_ID, PGR.FIGURE_ID FROM JECN_GROUP_PERMISSIONS GPT "
				+ " INNER JOIN JECN_POSITION_GROUP_R PGR ON GPT.POSTGROUP_ID = " + "   PGR.GROUP_ID "
				+ "   AND GPT.TYPE = 3 " + "  UNION " + "  SELECT AP.RELATE_ID, AP.FIGURE_ID "
				+ "   FROM JECN_ACCESS_PERMISSIONS AP " + "   WHERE AP.TYPE = 3) jap ON " + " jap.relate_id=jr.id"
				+ " left join jecn_org_access_permissions joap on joap.type=3 and joap.relate_id = jr.id"
				+ " where jr.is_dir<>0 and jr.project_id=" + ruleSearchBean.getProjectId();
		// 制度目录
		if (ruleSearchBean.getRuleId() != -1) {
			sql = sql + " and jr.per_id=" + ruleSearchBean.getRuleId();
		}
		// 制度名称
		if (StringUtils.isNotBlank(ruleSearchBean.getRuleName())) {
			sql = sql + " and jr.rule_name like '%" + ruleSearchBean.getRuleName() + "%'";
		}
		// 制度编号
		if (StringUtils.isNotBlank(ruleSearchBean.getRuleMum())) {
			sql = sql + " and (jr.rule_number like '%" + ruleSearchBean.getRuleMum() + "%' or jf.doc_id like '%"
					+ ruleSearchBean.getRuleMum() + "%')";
		}
		// 责任部门
		if (ruleSearchBean.getOrgId() != -1) {
			sql = sql + " and jfo.org_id=" + ruleSearchBean.getOrgId();
		}
		// 类别
		if (ruleSearchBean.getRuleType() != -1) {
			sql = sql + " and jr.type_id=" + ruleSearchBean.getRuleType();
		}
		// 密级
		if (ruleSearchBean.getSecretLevel() != -1) {
			sql = sql + " and jr.is_public=" + ruleSearchBean.getSecretLevel();
		}
		// 查阅岗位权限
		if (ruleSearchBean.getPosLookId() != -1) {
			sql = sql + " and jap.figure_id=" + ruleSearchBean.getPosLookId();
		}
		// 查阅部门权限
		if (ruleSearchBean.getOrgLookId() != -1) {
			sql = sql + " and joap.org_id=" + ruleSearchBean.getOrgLookId();
		}
		sql = sql + " )s_rule ";
		return sql;
	}

	@Override
	public int searchRule(RuleSearchBean ruleSearchBean) throws Exception {
		try {
			String sql = "select count (distinct s_rule.id) from" + getSearchRuleSQLAdmin(ruleSearchBean);
			if (!ruleSearchBean.isAdmin() && !JecnContants.isSystemAdmin) {
				sql = sql
						+ "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(ruleSearchBean.getPeopleId(), ruleSearchBean
								.getProjectId(), TYPEAUTH.RULE) + " on MY_AUTH_FILES.id=s_rule.id";
			}
			sql = sql + " where s_rule.ruleName is not null";
			return ruleDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public IProcessRuleTypeDao getProcessRuleTypeDao() {
		return processRuleTypeDao;
	}

	public void setProcessRuleTypeDao(IProcessRuleTypeDao processRuleTypeDao) {
		this.processRuleTypeDao = processRuleTypeDao;
	}

	@Override
	public RuleInfoBean getRuleInfoBean(Long ruleId, boolean isPub, Long peopleId) throws Exception {
		JecnRuleUseRecord ruleUseRecord = new JecnRuleUseRecord();
		ruleUseRecord.setId(JecnCommon.getUUID());
		ruleUseRecord.setAccessDate(new Date());
		ruleUseRecord.setPeopleId(peopleId);
		ruleUseRecord.setRuleId(ruleId);
		ruleUseRecord.setUseType(0);
		ruleDao.getSession().save(ruleUseRecord);
		RuleInfoBean ruleInfoBean = new RuleInfoBean();
		// 制度ID
		ruleInfoBean.setRuleId(ruleId);
		if (isPub) {
			// 制度对象
			Rule rule = ruleDao.getRuleById(ruleId);
			if (rule == null) {
				return null;
			}
			// 制度名称
			ruleInfoBean.setRuleName(rule.getRuleName());
			// 制度编号
			ruleInfoBean.setRuleNum(rule.getRuleNumber());
			// 秘密
			if (rule.getIsPublic() != null) {
				ruleInfoBean.setSecretLevel(rule.getIsPublic().intValue());
			} else {
				ruleInfoBean.setSecretLevel(0);
			}
			// 制度类别
			if (rule.getTypeId() != null) {
				ProceeRuleTypeBean processRuleTypeBean = this.processRuleTypeDao.get(rule.getTypeId());
				if (processRuleTypeBean != null && processRuleTypeBean.getTypeName() != null) {
					ruleInfoBean.setRuleTypeName(processRuleTypeBean.getTypeName());

				} else {
					ruleInfoBean.setRuleTypeName("");
				}
			} else {
				ruleInfoBean.setRuleTypeName("");
			}
			// 责任部门
			if (rule.getOrgId() != null) {
				JecnFlowOrg jecnFlowOrg = organizationService.get(rule.getOrgId());
				if (jecnFlowOrg != null) {
					ruleInfoBean.setDutyOrgId(rule.getOrgId());
					if (jecnFlowOrg.getOrgName() != null) {
						ruleInfoBean.setDutyOrg(jecnFlowOrg.getOrgName());
					} else {
						ruleInfoBean.setDutyOrg("");
					}
				} else {
					ruleInfoBean.setDutyOrgId(-1);
					ruleInfoBean.setDutyOrg("");
				}
			} else {
				ruleInfoBean.setDutyOrgId(-1);
				ruleInfoBean.setDutyOrg("");
			}

			// 责任人
			if (rule.getAttchMentId() != null && rule.getTypeResPeople() != null) {
				if (rule.getTypeResPeople().intValue() == 0) { // 人员
					JecnUser jecnUser = this.userDao.get(rule.getAttchMentId());
					if (jecnUser != null && jecnUser.getTrueName() != null) {
						// 设置人员的真实名称
						ruleInfoBean.setResponsibleName(jecnUser.getTrueName());
					} else {
						ruleInfoBean.setResponsibleName("");
					}
				} else { // 岗位
					JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(rule.getAttchMentId());
					if (jecnFlowOrgImage != null) {
						// 设置岗位名称
						ruleInfoBean.setResponsibleName(jecnFlowOrgImage.getFigureText());
					} else {
						ruleInfoBean.setResponsibleName("");
					}
				}
				ruleInfoBean.setResponsibleId(rule.getAttchMentId());
			} else {
				ruleInfoBean.setResponsibleId(-1);
				ruleInfoBean.setResponsibleName("");
			}
		} else {
			// 制度对象
			RuleT ruleT = ruleDao.getRuleTById(ruleId);
			if (ruleT == null) {
				return null;
			}
			// 制度名称
			ruleInfoBean.setRuleName(ruleT.getRuleName());
			// 制度编号
			ruleInfoBean.setRuleNum(ruleT.getRuleNumber());
			// 秘密
			if (ruleT.getIsPublic() != null) {
				ruleInfoBean.setSecretLevel(ruleT.getIsPublic().intValue());
			} else {
				ruleInfoBean.setSecretLevel(0);
			}
			// 制度类别
			if (ruleT.getTypeId() != null) {
				ProceeRuleTypeBean processRuleTypeBean = this.processRuleTypeDao.get(ruleT.getTypeId());
				if (processRuleTypeBean != null && processRuleTypeBean.getTypeName() != null) {
					ruleInfoBean.setRuleTypeName(processRuleTypeBean.getTypeName());

				} else {
					ruleInfoBean.setRuleTypeName("");
				}
			} else {
				ruleInfoBean.setRuleTypeName("");
			}
			// 责任部门
			if (ruleT.getOrgId() != null) {
				JecnFlowOrg jecnFlowOrg = organizationService.get(ruleT.getOrgId());
				if (jecnFlowOrg != null) {
					ruleInfoBean.setDutyOrgId(ruleT.getOrgId());
					if (jecnFlowOrg.getOrgName() != null) {
						ruleInfoBean.setDutyOrg(jecnFlowOrg.getOrgName());
					} else {
						ruleInfoBean.setDutyOrg("");
					}
				} else {
					ruleInfoBean.setDutyOrgId(-1);
					ruleInfoBean.setDutyOrg("");
				}
			} else {
				ruleInfoBean.setDutyOrgId(-1);
				ruleInfoBean.setDutyOrg("");
			}
			// 责任人
			if (ruleT.getAttchMentId() != null && ruleT.getTypeResPeople() != null) {
				if (ruleT.getTypeResPeople().intValue() == 0) { // 人员
					JecnUser jecnUser = this.userDao.get(ruleT.getAttchMentId());
					if (jecnUser != null && jecnUser.getTrueName() != null) {
						// 设置人员的真实名称
						ruleInfoBean.setResponsibleName(jecnUser.getTrueName());
					} else {
						ruleInfoBean.setResponsibleName("");
					}
				} else { // 岗位
					JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(ruleT.getAttchMentId());
					if (jecnFlowOrgImage != null) {
						// 如果是岗位存储岗位名称
						ruleInfoBean.setResponsibleName(jecnFlowOrgImage.getFigureText());
					} else {
						ruleInfoBean.setResponsibleName("");
					}
				}
				ruleInfoBean.setResponsibleId(ruleT.getAttchMentId());
			} else {
				ruleInfoBean.setResponsibleId(-1);
				ruleInfoBean.setResponsibleName("");
			}
		}
		return ruleInfoBean;
	}

	/**
	 * @author yxw 2012-6-7
	 * @description:根据父id查找子节点
	 * @param pid
	 *            父节点ID
	 * @param posIds
	 *            岗位ID集合
	 * @param orgIds
	 *            部门ID集合
	 * @param isAdmin
	 *            是否为管理员 true：管理员
	 * @param projectId
	 *            项目ID
	 * @return List<JecnTreeBean> 子节点集合
	 * @throws Exception
	 */
	@Override
	public List<WebTreeBean> getChildRulesForWeb(Long pid, Long projectId, Long peopleId, boolean isAdmin)
			throws Exception {
		String sql = "";
		if (!isAdmin && !JecnContants.isSystemAdmin) {
			sql = JecnCommonSqlTPath.INSTANCE.getTreeAppend(peopleId, projectId, TYPEAUTH.RULE);
		}
		sql = sql + " select distinct t.id, t.rule_name "
				+ ",t.is_dir,case when ju.id is null then '0' else '1' end as count,t.sort_id,t.per_id"
				+ " from jecn_rule t " + " left join jecn_rule ju on ju.per_id=t.id";
		if (!isAdmin && !JecnContants.isSystemAdmin) {
			sql = sql + JecnCommonSqlTPath.INSTANCE.getSqlFuncSubStr();
		}
		sql = sql + " where t.per_id= " + pid + " and t.project_id=" + projectId + " order by t.per_id,t.sort_id,t.id";
		List<Object[]> list = ruleDao.listNativeSql(sql, pid);
		List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[2] == null || obj[3] == null) {
				continue;
			}
			WebTreeBean webTreeBean = new WebTreeBean();
			// 制度ID
			webTreeBean.setId(obj[0].toString());
			// 制度名称
			if (obj[1] != null) {
				webTreeBean.setText(obj[1].toString());
			} else {
				webTreeBean.setText("");
			}
			if ("0".equals(obj[2].toString())) {
				// 制度目录
				webTreeBean.setType(TreeNodeType.ruleDir.toString());
				webTreeBean.setIcon("images/treeNodeImages/" + TreeNodeType.ruleDir.toString() + ".gif");
			} else if ("1".equals(obj[2].toString())) {
				// 制度模板
				webTreeBean.setType(TreeNodeType.ruleModeFile.toString());
				webTreeBean.setIcon("images/treeNodeImages/" + TreeNodeType.ruleModeFile.toString() + ".gif");
			} else {
				// 制度文件
				webTreeBean.setType(TreeNodeType.ruleFile.toString());
				webTreeBean.setIcon("images/treeNodeImages/" + TreeNodeType.ruleFile.toString() + ".gif");
			}
			// 长度
			if (Integer.parseInt(obj[3].toString()) > 0) {
				webTreeBean.setLeaf(false);
			} else {
				webTreeBean.setLeaf(true);
			}
			listWebTreeBean.add(webTreeBean);
		}
		return listWebTreeBean;
	}

	public IOrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(IOrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	@Override
	public RuleFileWebBean getRuleFileBean(Long ruleId, boolean isPub, Long peopleId) throws Exception {
		// JecnRuleUseRecord ruleUseRecord = new JecnRuleUseRecord();
		// ruleUseRecord.setId(JecnCommon.getUUID());
		// ruleUseRecord.setAccessDate(new Date());
		// ruleUseRecord.setPeopleId(peopleId);
		// ruleUseRecord.setRuleId(ruleId);
		// ruleUseRecord.setUseType(0);
		// ruleDao.getSession().save(ruleUseRecord);

		RuleFileWebBean ruleFileWebBean = new RuleFileWebBean();
		RuleInfoBean ruleInfoBean = this.getRuleInfoBean(ruleId, isPub, peopleId);
		ruleFileWebBean.setRuleInfoBean(ruleInfoBean);
		if (isPub) {
			Rule rule = ruleDao.getRuleById(ruleId);
			if (rule == null) {
				return null;
			}
			// 制度文件
			if (rule.getFileId() != null) {
				// 本地上传方式
				if (rule.getIsFileLocal() != null && rule.getIsFileLocal().intValue() == 1) {
					FileWebBean fileCommonBean = new FileWebBean();
					fileCommonBean.setFileId(rule.getFileId());
					fileCommonBean.setFileName(rule.getRuleName());
					fileCommonBean.setIsFileLocal(1);
					ruleFileWebBean.setFileCommonBean(fileCommonBean);

				} else {
					JecnFileBean jecnFileBean = this.fileDao.findJecnFileBeanById(rule.getFileId());
					if (jecnFileBean != null) {
						FileWebBean fileCommonBean = new FileWebBean();
						fileCommonBean.setFileId(rule.getFileId());
						if (StringUtils.isNotBlank(jecnFileBean.getFileName())) {
							fileCommonBean.setFileName(jecnFileBean.getFileName());
						} else {
							fileCommonBean.setFileName("");
						}
						ruleFileWebBean.setFileCommonBean(fileCommonBean);
					}
				}
			}
		} else {
			RuleT rule = ruleDao.getRuleTById(ruleId);
			if (rule == null) {
				return null;
			}
			// 制度文件
			if (rule.getFileId() != null) {
				// 本地上传方式
				if (rule.getIsFileLocal() != null && rule.getIsFileLocal().intValue() == 1) {
					FileWebBean fileCommonBean = new FileWebBean();
					fileCommonBean.setFileId(rule.getFileId());
					fileCommonBean.setFileName(rule.getRuleName());
					fileCommonBean.setIsFileLocal(1);
					ruleFileWebBean.setFileCommonBean(fileCommonBean);

				} else {
					JecnFileBeanT jecnFileBean = this.fileDao.get(rule.getFileId());
					if (jecnFileBean != null && jecnFileBean.getDelState() != null
							&& jecnFileBean.getDelState().intValue() == 0) {
						FileWebBean fileCommonBean = new FileWebBean();
						fileCommonBean.setFileId(rule.getFileId());
						if (StringUtils.isNotBlank(jecnFileBean.getFileName())) {
							fileCommonBean.setFileName(jecnFileBean.getFileName());
						} else {
							fileCommonBean.setFileName("");
						}
						ruleFileWebBean.setFileCommonBean(fileCommonBean);
					}
				}
			}
		}
		// 相关流程
		List<Object[]> listObj = ruleDao.getRuleRelateFlows(ruleId, isPub);
		List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
		for (Object[] obj : listObj) {
			ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObject(obj);
			if (processWebBean != null)
				listProcessWebBean.add(processWebBean);
		}
		ruleFileWebBean.setListProcessBean(listProcessWebBean);
		return ruleFileWebBean;
	}

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	public List<JecnTaskHistoryNew> getJecnTaskHistoryNewList(Long refId, int type) throws Exception {
		return docControlDao.getJecnTaskHistoryNewList(refId, type);
	}

	/**
	 * 通过id获取 制度文控信息
	 * 
	 * @author fuzhh Nov 29, 2012
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long id) throws Exception {
		return docControlDao.getJecnTaskHistoryNew(id);
	}

	@Override
	public boolean validateAddName(String name, Long pid) throws Exception {
		String hql = "from RuleT where perId =? and isDir=1 and ruleName=? and delState <> 2";
		List<RuleT> list = ruleDao.listHql(hql, pid, name);
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	public IOrgAccessPermissionsDao getOrgAccessPermissionsDao() {
		return orgAccessPermissionsDao;
	}

	public IPositionAccessPermissionsDao getPositionAccessPermissionsDao() {
		return positionAccessPermissionsDao;
	}

	/**
	 * 获得制度搜索的公共sql
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	private String getSql(PublicSearchBean publicSearchBean, Long projectId) {
		String sql = " from jecn_rule ju" + " left join jecn_file jf on jf.file_id=ju.file_id and jf.del_state=0"
				+ " left join jecn_flow_org jfo on jfo.org_id=ju.org_id and jfo.del_state=0"
				+ " where ju.is_dir<>0 and ju.project_id=" + projectId;
		// getSql 新添加密级条件(不能是默认 is_public =1)
		if (-1 != publicSearchBean.getSecretLevel()) { // -1:全部,0秘密,1公开
			// 不拼接is_public条件
			sql = sql + "  and ju.is_public= " + publicSearchBean.getSecretLevel();
		}
		// 制度名称
		if (StringUtils.isNotBlank(publicSearchBean.getName())) {
			sql = sql + " and ju.rule_name like '%" + publicSearchBean.getName() + "%'";
		}
		// 制度编号
		if (StringUtils.isNotBlank(publicSearchBean.getNumber())) {
			sql = sql + " and (ju.rule_number like '%" + publicSearchBean.getNumber() + "%' or jf.doc_id like '%"
					+ publicSearchBean.getNumber() + "%')";
		}
		// 责任部门
		if (StringUtils.isNotBlank(publicSearchBean.getOrgName())) {
			if (publicSearchBean.getOrgId() != -1) {
				sql = sql + " and ju.org_id=" + publicSearchBean.getOrgId();
			} else {
				sql = sql + " and ju.org_id in (select jfo.org_id from jecn_flow_org jfo where jfo.org_name like '%"
						+ publicSearchBean.getOrgName() + "%' )";
			}
		}
		return sql;
	}

	@Override
	public int getTotalPublicRule(PublicSearchBean publicSearchBean, Long projectId) throws Exception {
		try {
			String sql = "select count (distinct ju.id)";
			sql = sql + getSql(publicSearchBean, projectId);
			return ruleDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<PublicFileBean> getPublicRule(PublicSearchBean publicSearchBean, int start, int limit, Long projectId)
			throws Exception {
		try {
			String sql = "select ju.id,case when ju.is_dir=2 "
					+ " then jf.file_name else ju.rule_name end as fileName," + " ju.rule_number,"
					+ " ju.is_dir,ju.org_id,ju.file_id,jfo.org_name,ju.is_public ";
			sql = sql + getSql(publicSearchBean, projectId);
			sql += " order by ju.per_id,ju.sort_id,ju.id";
			List<Object[]> listObjects = ruleDao.listNativeSql(sql, start, limit);
			List<PublicFileBean> listResult = new ArrayList<PublicFileBean>();
			for (Object[] obj : listObjects) {
				if (obj == null || obj[0] == null || obj[3] == null) {
					continue;
				}
				PublicFileBean publicFileBean = new PublicFileBean();
				publicFileBean.setId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					publicFileBean.setName(obj[1].toString());
				} else {
					publicFileBean.setName("");
				}
				if (obj[2] != null) {
					publicFileBean.setNumber(obj[2].toString());
				} else {
					publicFileBean.setNumber("");
				}
				if ("1".equals(obj[3].toString())) {
					publicFileBean.setType(5);
				} else {
					publicFileBean.setType(4);
				}
				if (obj[5] != null) {
					if (obj[1] != null) {
						publicFileBean.setRelationName(obj[1].toString());
					} else {
						publicFileBean.setRelationName("");
					}
					publicFileBean.setRelationId(Long.valueOf(obj[5].toString()));
					publicFileBean.setRelationType(0);
				}
				if (obj[6] != null) {
					publicFileBean.setOrgName(obj[6].toString());
				} else {
					publicFileBean.setOrgName("");
				}
				if (obj[7] != null) {
					publicFileBean.setSecretLevel(Integer.parseInt(obj[7].toString()));
				}
				listResult.add(publicFileBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int getTotalAttenTionRule(AttenTionSearchBean attenTionSearchBean, Long projectId) throws Exception {
		try {
			String sql = getMyRuleSql(projectId, attenTionSearchBean, true);
			if (!"".equals(sql)) {
				int counts = ruleDao.countAllByParamsNativeSql(sql);
				return counts;
			}
			return 0;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<RuleInfoBean> getAttenTionRule(AttenTionSearchBean attenTionSearchBean, int start, int limit,
			Long projectId) throws Exception {
		try {
			String sql = getMyRuleSql(projectId, attenTionSearchBean, false);
			List<RuleInfoBean> listRuleInfoBean = new ArrayList<RuleInfoBean>();
			if (!"".equals(sql)) {
				sql += " ORDER BY MY_RULE.pub_time DESC ";
				List<Object[]> listObjects = ruleDao.listNativeSql(sql, start, limit);
				for (Object[] obj : listObjects) {
					RuleInfoBean ruleInfoBean = JecnUtil.getRuleInfoBeanByObject(obj);
					if (ruleInfoBean != null)
						listRuleInfoBean.add(ruleInfoBean);
				}
			}
			return listRuleInfoBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 
	 * 我的制度
	 * 
	 * @param projectId
	 * @param attenTionSearchBean
	 * @return String sql
	 * 
	 */
	private String getMyRuleSql(Long projectId, AttenTionSearchBean attenTionSearchBean, boolean isCount) {
		Long peopleId = attenTionSearchBean.getUserId();
		if (peopleId == -1) {
			return "";
		}
		String sql = "";
		if (isCount) {
			sql = sql + " SELECT count(MY_RULE.ID) ";
		} else {
			sql = sql + " SELECT MY_RULE.* ";
		}
		sql = sql
				+ " FROM (SELECT DISTINCT JR.ID,"
				+ "                CASE WHEN JR.IS_DIR = 2 AND JR.IS_FILE_LOCAL=0 THEN JF.FILE_NAME ELSE JR.RULE_NAME END AS RULENAME,"
				+ "                CASE WHEN JR.IS_DIR = 2 AND JR.IS_FILE_LOCAL=0 THEN JF.DOC_ID  ELSE JR.RULE_NUMBER END AS RULENUMBER,"
				+ "                JF.FILE_ID," + "                JR.IS_DIR," + "                JR.TYPE_ID,"
				+ "                JFT.TYPE_NAME," + "                JFO.ORG_ID," + "                JFO.ORG_NAME,"
				+ "                JR.IS_PUBLIC," + "                JR.pub_time," + "                JR.PER_ID,"
				+ "                JR.SORT_ID" + "  FROM JECN_RULE JR"
				+ "  LEFT JOIN JECN_FLOW_TYPE JFT ON JFT.TYPE_ID = JR.TYPE_ID"
				+ "  LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JR.ORG_ID"
				+ "  LEFT JOIN JECN_FILE JF ON JF.FILE_ID = JR.FILE_ID AND JR.IS_DIR=2 AND JR.IS_FILE_LOCAL=0"
				// + " WHERE JR.ID IN (SELECT ID FROM MY_AUTH_FILES)) MY_RULE";
				+ ") MY_RULE";
		if (!attenTionSearchBean.isAdmin() && !JecnContants.isSystemAdmin) {
			sql = sql
					+ "  inner join "
					+ JecnCommonSqlTPath.INSTANCE.getFileSearch(attenTionSearchBean.getUserId(), projectId,
							TYPEAUTH.RULE) + " on MY_AUTH_FILES.id=MY_RULE.id";
		}
		sql = sql + " WHERE MY_RULE.RULENAME IS NOT NULL  AND MY_RULE.IS_DIR<>0";
		// 制度名称 0：目录1：制度模板文件2：制度文件
		if (StringUtils.isNotBlank(attenTionSearchBean.getName())) {
			sql += " AND MY_RULE.RULENAME LIKE '%" + attenTionSearchBean.getName() + "%'";
		}
		// 制度编号0：目录1：制度模板文件2：制度文件
		if (StringUtils.isNotBlank(attenTionSearchBean.getNumber())) {
			sql += "AND MY_RULE.RULENUMBER LIKE '%" + attenTionSearchBean.getNumber() + "%'";
		}
		// 责任部门
		if (attenTionSearchBean.getOrgId() != -1) {
			sql = sql + "  AND MY_RULE.ORG_ID =" + attenTionSearchBean.getOrgId();
		}
		// 密级
		if (attenTionSearchBean.getSecretLevel() != -1) {
			sql = sql + "  AND MY_RULE.is_public= " + attenTionSearchBean.getSecretLevel();
		}
		return sql;
	}

	public IJecnConfigItemDao getConfigItemDao() {
		return configItemDao;
	}

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 * @throws Exception
	 */
	private void updateTpathAndTLevel(RuleT rulet) throws Exception {
		// 获取上级节点
		RuleT preRule = ruleDao.get(rulet.getPerId());
		if (preRule == null) {
			rulet.settLevel(0);
			rulet.settPath(rulet.getId() + "-");
			rulet.setViewSort(JecnUtil.concatViewSort("", rulet.getSortId()));
		} else {
			rulet.settLevel(preRule.gettLevel() + 1);
			rulet.settPath(preRule.gettPath() + rulet.getId() + "-");
			rulet.setViewSort(JecnUtil.concatViewSort(preRule.getViewSort(), rulet.getSortId()));
		}
		// 更新
		ruleDao.update(rulet);
		ruleDao.getSession().flush();
	}

	@Override
	public Long addRuleDir(RuleT rulet) throws Exception {
		rulet.setCreateDate(new Date());
		rulet.setUpdateDate(rulet.getCreateDate());
		ruleDao.save(rulet);
		// 更新tpath 和tlevel
		updateTpathAndTLevel(rulet);
		ruleDao.getSession().flush();

		orgAccessPermissionsDao.addPermissionisParentNode(rulet.getPerId(), rulet.getId());
		// 把目录增加到正式表
		String sql = "insert into JECN_RULE(ID, PROJECT_ID, PER_ID, RULE_NUMBER, RULE_NAME, IS_PUBLIC, TYPE_ID, IS_DIR, ORG_ID, SORT_ID, FILE_ID, TEMPLATE_ID, TEST_RUN_NUMBER, CREATE_DATE, CREATE_PEOPLE_ID, UPDATE_DATE, UPDATE_PEOPLE_ID, HISTORY_ID,T_PATH,T_LEVEL,VIEW_SORT,DEL_STATE) "
				+ " select ID, PROJECT_ID, PER_ID, RULE_NUMBER, RULE_NAME, IS_PUBLIC, TYPE_ID, IS_DIR, ORG_ID, SORT_ID, FILE_ID, TEMPLATE_ID, TEST_RUN_NUMBER, CREATE_DATE, CREATE_PEOPLE_ID, UPDATE_DATE, UPDATE_PEOPLE_ID, HISTORY_ID,T_PATH,T_LEVEL,VIEW_SORT,DEL_STATE from JECN_RULE_T t where t.ID = "
				+ rulet.getId();
		ruleDao.execteNative(sql);
		JecnTaskCommon.inheritTaskTemplate(ruleDao, rulet.getId(), 2, rulet.getCreatePeopleId());
		return rulet.getId();
	}

	@Override
	public String[] validateAddNames(List<String> names, Long pId) throws Exception {
		try {
			String hql = "select ruleName from RuleT where perId=? and isDir=2 and delState <> 2";
			List<String> listData = ruleDao.listHql(hql, pId);
			List<String> listResult = new ArrayList<String>();
			for (String newName : names) {
				if (listData.contains(newName)) {
					listResult.add(newName);
				}
			}
			if (listResult.size() == 0) {
				return null;
			}
			String[] strArr = new String[listResult.size()];
			for (int i = 0; i < listResult.size(); i++) {
				strArr[i] = listResult.get(i);
			}
			return strArr;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchISNotRuleDirByName(String name, Long projectId) throws Exception {
		try {
			String sql = "select t.id,t.rule_name,t.per_id,t.is_dir,t.rule_number,"
					+ "    case when jr.id is null then '0' else '1' end as is_pub,"
					+ "    '0',t.TEMPLATE_ID"
					+ "    from jecn_rule_t t left join jecn_rule jr on t.id=jr.id where t.del_state=0 and t.rule_name like ? and t.IS_DIR <> 0 and t.project_id=?"
					+ "    order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = ruleDao.listNativeSql(sql, 0, 10, "%" + name + "%", projectId);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public JecnCreateDoc downloadRuleFile(long ruleId, boolean isp) throws Exception {
		JecnCreateDoc createDoc = DownloadUtil.getRuleFile(ruleId, isp, ruleDao, docControlDao, configItemDao);
		return createDoc;
	}

	@Override
	public List<RuleInfoBean> searchRuleList(Long pId, boolean isAdmin, Long projectId, Long peopleId, int start,
			int limit) throws Exception {
		try {
			String sql = "";
			// 向下递归
			sql = sql + " with click_Dir AS" + " (SELECT C.id ,C.per_id FROM  jecn_rule C"
					+ " INNER JOIN jecn_rule P ON C.T_PATH LIKE " + JecnCommonSql.getJoinFunc("P")
					+ " WHERE P.ID =? AND C.is_dir<>0)";
			sql = sql
					+ " select C_RULE.* from (select jr.id,case when jr.is_dir=2 and jr.is_file_local=0 "
					+ " then jf.file_name else jr.rule_name end as ruleName"
					+ " ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.doc_id else jr.rule_number end as ruleNumber"
					+ " ,jf.file_id,jr.is_dir,jr.type_id,jft.type_name,jfo.org_id,jfo.org_name,jr.is_public,jr.pub_time,jr.sort_id,jr.per_id"
					+ ",case when jr.is_dir=2 and jr.is_file_local=0 and jf.file_id is null then '1' else '0' end as isDelete"
					+ " from jecn_rule jr inner join click_Dir on click_Dir.id = jr.id"
					+ "	left join jecn_flow_type jft on jft.type_id=jr.type_id"
					+ " left join jecn_flow_org jfo on jfo.org_id = jr.org_id"
					+ " left join jecn_file jf on jf.file_id = jr.file_id and jr.is_file_local=0"
					+ "	where jr.is_dir<>0) C_RULE ";
			if (!isAdmin && !JecnContants.isSystemAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.RULE)
						+ " on MY_AUTH_FILES.id=C_RULE.id";
			}
			sql = sql + " where C_RULE.isDelete='0'" + " order by C_RULE.per_id,C_RULE.sort_id,C_RULE.id";
			List<Object[]> listObjects = ruleDao.listNativeSql(sql, start, limit, pId);

			List<RuleInfoBean> listRuleInfoBean = new ArrayList<RuleInfoBean>();
			for (Object[] obj : listObjects) {
				RuleInfoBean ruleInfoBean = JecnUtil.getRuleInfoBeanByObject(obj);
				if (ruleInfoBean != null)
					listRuleInfoBean.add(ruleInfoBean);
			}
			return listRuleInfoBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int searchRuleListTotal(Long pId, boolean isAdmin, Long projectId, Long peopleId) throws Exception {
		try {
			String sql = "";
			sql = sql + " with click_Dir AS" + " (SELECT C.id ,C.per_id FROM  jecn_rule C"
					+ " INNER JOIN jecn_rule P ON C.T_PATH LIKE " + JecnCommonSql.getJoinFunc("P")
					+ " WHERE P.ID =? AND C.is_dir<>0)";
			sql = sql
					+ " select count(c_rule.id) from("
					+ "       select jr.id,case when jr.is_dir=2 and jr.is_file_local=0 and jf.file_id is null then '1' else  '0' end as isDelete"
					+ "       from jecn_rule jr inner join click_Dir on click_Dir.id = jr.id"
					+ "       left join jecn_file jf on jr.is_dir=2 and  jf.file_id = jr.file_id and jf.del_state=0"
					+ "       where jr.is_dir<>0) C_RULE";
			if (!isAdmin && !JecnContants.isSystemAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.RULE)
						+ " on MY_AUTH_FILES.id=C_RULE.id";
			}
			sql = sql + " WHERE C_RULE.isDelete='0'";
			return ruleDao.countAllByParamsNativeSql(sql, pId);

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 制度清单数据
	 */
	public RuleSystemListBean findRuleSystemListBean(long ruleId, long projectId, int ruleSysLevel, boolean showAll)
			throws Exception {
		try {
			List<Long> listIds = new ArrayList<Long>();
			listIds.add(ruleId);
			String sql = "select jr.id,jr.per_id,jr.is_dir"
					+ "            ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.file_name else jr.rule_name end as ruleName"
					+ "              ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.doc_id else jr.rule_number end as ruleNumber"
					+ "               ,jf.file_id,jfo.org_id,jfo.org_name,jr.is_public,t.pub_time"
					+ "                ,t.id as rule_pub_id,jtbn.state,jt.version_id,jr.t_level"
					+ "              from jecn_rule_t jr " + "              inner join ("
					+ JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.ruleDir)
					+ ") ruleId on ruleId.id=jr.id" + "                left join jecn_rule t on t.id=jr.id"
					+ "                left join jecn_flow_org jfo on jfo.org_id = jr.org_id"
					+ "                 left join jecn_file_t jf on jf.file_id = jr.file_id "
					+ "                 left join jecn_task_history_new jt on jt.id = jr.history_id"
					+ "                  left join jecn_task_bean_new jtbn on jtbn.task_type in (2,3)"
					+ "                and jtbn.r_id = jr.id"
					+ "                and jtbn.is_lock = 1 and jtbn.state<>5"
					+ "                order by jr.per_id,jr.sort_id,jr.id";
			List<Object[]> listAll = this.ruleDao.listNativeSql(sql, projectId);
			List<RuleInfoBean> listResult = new ArrayList<RuleInfoBean>();
			RuleSystemListBean ruleSystemListBean = new RuleSystemListBean();
			// 级别最大值
			int maxCount = 0;
			if (listAll != null) {
				for (Object[] obj : listAll) {
					if (obj == null || obj[0] == null || obj[1] == null || obj[13] == null) {
						continue;
					}
					RuleInfoBean ruleInfoBean = new RuleInfoBean();
					// 制度ID
					ruleInfoBean.setRuleId(Long.valueOf(obj[0].toString()));
					// 制度父ID
					ruleInfoBean.setRulePerId(Long.valueOf(obj[1].toString()));
					// 是否是目录
					if (obj[2] != null && !"".equals(obj[2])) {
						ruleInfoBean.setIsDir(Integer.parseInt(obj[2].toString()));
					}
					// 制度/文件名称
					if (obj[3] != null && !"".equals(obj[3])) {
						ruleInfoBean.setRuleName(obj[3].toString());
					} else {
						ruleInfoBean.setRuleName("");
					}
					// 制度/文件编号
					if (obj[4] != null && !"".equals(obj[4])) {
						ruleInfoBean.setRuleNum(obj[4].toString());
					} else {
						ruleInfoBean.setRuleNum("");
					}
					// 制度文件ID
					if (obj[5] != null && !"".equals(obj[5])) {
						ruleInfoBean.setFileId(Long.valueOf(obj[5].toString()));
					}
					// 责任部门ID
					if (obj[6] != null && !"".equals(obj[6])) {
						ruleInfoBean.setDutyOrgId(Long.valueOf(obj[6].toString()));
					}
					// 责任部门名称
					if (obj[7] != null && !"".equals(obj[7])) {
						ruleInfoBean.setDutyOrg(obj[7].toString());
					} else {
						ruleInfoBean.setDutyOrg("");
					}
					// 密级
					if (obj[8] != null && !"".equals(obj[9])) {
						ruleInfoBean.setSecretLevel(Integer.parseInt(obj[8].toString()));
					}
					int num = Integer.valueOf(obj[13].toString());

					if (ruleInfoBean.getIsDir() == 0) {
						if (num > maxCount) {
							maxCount = num;
						}
					}
					// 处于级别
					ruleInfoBean.setLevel(num);
					// 版本号
					if (obj[12] != null) {
						ruleInfoBean.setVersionId(obj[12].toString());
					} else {
						ruleInfoBean.setVersionId("");
					}
					// 制度发布状态
					if (obj[10] != null) {
						// 发布
						ruleInfoBean.setTypeByData(2);
						// 发布日期,并且判断是否是目录（0：目录）,如果是目录时间不赋值
						if (obj[9] != null && obj[2] != null && Integer.parseInt(obj[2].toString()) != 0) {
							ruleInfoBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[9]
									.toString())));
						} else {
							ruleInfoBean.setPubDate("");
						}
					} else {
						if (obj[11] != null) {
							// 审批中
							ruleInfoBean.setTypeByData(1);
						} else {
							// 待建
							ruleInfoBean.setTypeByData(0);
						}
						ruleInfoBean.setPubDate("");
					}
					listResult.add(ruleInfoBean);
				}
			}

			// 总级别数
			ruleSystemListBean.setLevelTotal(maxCount + 1);

			// IntegerType intype = new IntegerType();
			// 父Id为0
			List<Long> list0 = new ArrayList<Long>();
			for (Object[] obj : listAll) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
					continue;
				}
				if ("0".equals(obj[1].toString())) {
					list0.add(Long.valueOf(obj[0].toString()));
				}
			}
			// 根据目录集合添加子节点到该目录下，按序排列0级：目录---制度，1级：目录----制度.......
			List<RuleInfoBean> listSortResult = new ArrayList<RuleInfoBean>();
			Set<Long> ruleIdSet = new HashSet<Long>();
			getAllRuleInfos(ruleId, listResult, listSortResult, ruleIdSet);
			// ruleSystemList.setListRuleInfoBean(listSortResult);
			ruleSystemListBean.setListRuleInfoBean(listSortResult);

			/** 制度个数 */
			int ruleTotal = 0;
			/** 待建制度个数 */
			int noPubFlowTotal = 0;
			/** 审批制度个数 */
			int approveFlowTotal = 0;
			/** 发布制度个数 */
			int pubFlowTotal = 0;
			for (RuleInfoBean ruleInfoBean : listResult) {
				if (ruleInfoBean.getIsDir() == 1 || ruleInfoBean.getIsDir() == 2) { // 制度文件或者是制度模板文件
					ruleTotal++;
					if (ruleInfoBean.getTypeByData() == 0) {
						noPubFlowTotal++;
					} else if (ruleInfoBean.getTypeByData() == 1) {
						approveFlowTotal++;
					} else if (ruleInfoBean.getTypeByData() == 2) {
						pubFlowTotal++;
					}
				}
			}
			ruleSystemListBean.setRuleTotal(ruleTotal);
			ruleSystemListBean.setNoPubRuleTotal(noPubFlowTotal);
			ruleSystemListBean.setApproveRuleTotal(approveFlowTotal);
			ruleSystemListBean.setPubRuleTotal(pubFlowTotal);

			return ruleSystemListBean;

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private void getAllRuleInfos(Long ruleId, List<RuleInfoBean> listResult, List<RuleInfoBean> listSortResult,
			Set<Long> ruleIdSet) {
		for (RuleInfoBean ruleInfoBean : listResult) {
			if (ruleId.equals(ruleInfoBean.getRuleId())) {
				listSortResult.add(ruleInfoBean);
			} else if (ruleId.equals(ruleInfoBean.getRulePerId())) {
				boolean isQuery = false;
				for (Long ruleSyId : ruleIdSet) {
					if (ruleSyId.equals(ruleInfoBean.getRuleId())) {
						isQuery = true;
					}
				}
				if (!isQuery) {
					ruleIdSet.add(ruleInfoBean.getRuleId());
					getAllRuleInfos(ruleInfoBean.getRuleId(), listResult, listSortResult, ruleIdSet);
				}
			}
		}
	}

	@Override
	public Rule getRule(Long id) throws Exception {
		return (Rule) ruleDao.getSession().get(Rule.class, id);
	}

	/***************************************************************************
	 * 记录制度下载日志
	 * 
	 * @param ruleId
	 * @param peopleId
	 * @throws Exception
	 */
	@Override
	public void saveRuleUseRecord(Long ruleId, Long peopleId) throws Exception {
		JecnRuleUseRecord ruleUseRecord = new JecnRuleUseRecord();
		ruleUseRecord.setId(JecnCommon.getUUID());
		ruleUseRecord.setAccessDate(new Date());
		ruleUseRecord.setPeopleId(peopleId);
		ruleUseRecord.setRuleId(ruleId);
		ruleUseRecord.setUseType(1);
		ruleDao.getSession().save(ruleUseRecord);
	}

	@Override
	public List<JecnRuleOperationCommon> getRuleRelateFile(Long ruleId) throws Exception {

		// 通过制度ID找到制度内容
		List<JecnRuleOperationCommon> listResult = new ArrayList<JecnRuleOperationCommon>();
		// 通过制度ID找到制度流程
		List<JecnFlowCommon> listJecnFlowCommon = ruleDao.findRuleFlow(ruleId, 1L, 1, true);
		// 通过制度ID找到制度流程地图
		List<JecnFlowCommon> listJecnFlowMapCommon = ruleDao.findRuleFlow(ruleId, 0L, 1, true);
		// 通过制度ID找到制度标准
		List<JecnStandarCommon> listJecnStandarCommon = ruleDao.findRuleStandar(ruleId, 1);
		// 通过制度ID找到制度风险
		List<JecnRiskCommon> listJecnRiskCommon = ruleDao.findRuleRisk(ruleId, 1);

		JecnRuleOperationCommon jecnRuleOperationCommon = new JecnRuleOperationCommon();
		jecnRuleOperationCommon.setListJecnFlowCommon(listJecnFlowCommon);
		jecnRuleOperationCommon.setListJecnFlowMapCommon(listJecnFlowMapCommon);
		jecnRuleOperationCommon.setListJecnStandarCommon(listJecnStandarCommon);
		jecnRuleOperationCommon.setListJecnRiskCommon(listJecnRiskCommon);

		listResult.add(jecnRuleOperationCommon);
		return listResult;
	}

	@Override
	public int findRuleSysSevel(Long ruleId) throws Exception {
		return this.ruleDao.get(ruleId).gettLevel();
	}

	@Override
	public Long updateLocalRuleFile(RuleT paramRule) throws Exception {
		return updateLocalRuleFile(paramRule, true);
	}

	private Long updateLocalRuleFile(RuleT paramRule, boolean needLog) throws Exception {
		Date nowDate = new Date();
		try {
			// 将当前id添加到正式表和临时表
			RuleT ruleT = ruleDao.get(paramRule.getId());
			// 记录文件内容
			G020RuleFileContent jecnFileContent = new G020RuleFileContent();
			// 制度Id
			jecnFileContent.setRuleId(paramRule.getId());
			jecnFileContent.setFileName(paramRule.getRuleName());
			jecnFileContent.setUpdatePeopleId(paramRule.getUpdatePeopleId());
			jecnFileContent.setUpdateTime(nowDate);
			jecnFileContent.setIsVersionLocal(1);
			// 保存当前版本文件到数据库
			saveVersionRuleFileToDataBase(paramRule, jecnFileContent);
			// 保存版本到本地
			saveVersionFileToLocal(ruleT.getId(), jecnFileContent.getId(), paramRule.getUpdatePeopleId());

			ruleT.setFileId(jecnFileContent.getId());
			ruleT.setUpdateDate(nowDate);
			ruleT.setRuleName(paramRule.getRuleName());
			ruleT.setUpdatePeopleId(paramRule.getUpdatePeopleId());
			ruleT.setRuleNumber(paramRule.getRuleNumber());
			ruleDao.update(ruleT);

			ruleDao.getSession().flush();

			if (needLog) {
				// 添加日志
				JecnUtil.saveJecnJournal(paramRule.getId(), paramRule.getRuleName(), 4, 2, paramRule
						.getUpdatePeopleId(), ruleDao);
			}
			return jecnFileContent.getId();
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private void saveVersionRuleFileToDataBase(RuleT paramRule, G020RuleFileContent jecnFileContent) throws Exception {
		if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
			jecnFileContent.setFileStream(Hibernate.createBlob(paramRule.getFileBytes()));
			ruleDao.getSession().save(jecnFileContent);
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			jecnFileContent.setFileStream(Hibernate.createBlob(new byte[1]));
			ruleDao.getSession().save(jecnFileContent);
			ruleDao.getSession().flush();
			ruleDao.getSession().refresh(jecnFileContent, LockMode.UPGRADE);
			SerializableBlob sb = (SerializableBlob) jecnFileContent.getFileStream();
			java.sql.Blob wrapBlob = sb.getWrappedBlob();
			BLOB blob = (BLOB) wrapBlob;
			try {
				OutputStream out = blob.getBinaryOutputStream();
				out.write(paramRule.getFileBytes());
				out.close();
			} catch (SQLException ex) {
				log.error("写blob大字段错误！", ex);
				throw ex;
			} catch (IOException ex) {
				log.error("写blob大字段错误！", ex);
				throw ex;
			}
		}
	}

	private void saveVersionFileToLocal(Long ruleId, Long contentId, Long updatePersonId) throws Exception {
		String hql = "from G020RuleFileContent where ruleId = ? and isVersionLocal = 1 and id <> " + contentId;
		List<G020RuleFileContent> fileContents = ruleDao.listHql(hql, ruleId);

		if (fileContents.size() == 0) {
			return;
		}
		// 获取 正式表
		Rule rule = ruleDao.getRuleById(ruleId);
		for (G020RuleFileContent fileContent : fileContents) {
			if (rule != null && rule.getFileId() != null && fileContent.getId().equals(rule.getFileId())) {
				// 过滤掉发布版本
				continue;
			}
			byte[] bytes = JecnDaoUtil.blobToBytes(fileContent.getFileStream());
			if (bytes == null) {
				log.error("saveVersionFileToLocal 保存制度版本文件到本地异常！");
				throw new IllegalArgumentException("保存制度版本文件到本地异常！");
			}
			// 2、更新版本文件标识
			fileContent.setIsVersionLocal(0);
			fileContent.setUpdateTime(new Date());
			fileContent.setUpdatePeopleId(updatePersonId);
			fileContent.setFileStream(null);
			// 3、保存版本文件到本地
			String filePath = JecnFinal.saveFileToLocal(bytes, fileContent.getFileName(), JecnFinal.RULE_FILE_RELEASE);
			if (filePath == null) {
				throw new IllegalAnnotationException("JecnFinal.saveFileToLocal获取文件路径非法!");
			}
			fileContent.setFilePath(filePath);

			ruleDao.getSession().update(fileContent);
			ruleDao.getSession().flush();
		}
	}

	@Override
	public FileOpenBean g020OpenRuleFile(long id) throws Exception {
		FileOpenBean fileOpenBean = null;
		try {

			fileOpenBean = new FileOpenBean();
			String hql = "from G020RuleFileContent where id=?";
			G020RuleFileContent fileContent = ruleDao.getObjectHql(hql, id);
			if (fileContent != null) {
				fileOpenBean.setId(fileContent.getId());
				fileOpenBean.setFileByte(JecnDaoUtil.blobToBytes(fileContent.getFileStream()));
				fileOpenBean.setName(fileContent.getFileName());
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return fileOpenBean;
	}

	@Override
	public G020RuleFileContent getG020RuleFileContent(Long fileId) throws Exception {
		return ruleDao.getG020RuleFileContent(fileId);
	}

	/**
	 * 获取制度文件名称和数据库存储制度内容表的主键ID
	 * 
	 * @param ruleId
	 * @param isPub
	 * @return Map<String, String>
	 * @throws Exception
	 */
	@Override
	public Map<String, String> getRuleNameAndContentId(long ruleId, boolean isPub) throws Exception {
		Map<String, String> content = new HashMap<String, String>();
		G020RuleFileContent fileContent = getG020RuleFileContent(ruleId);
		content.put("FileName", fileContent.getFileName());
		content.put("ContentId", fileContent.getId().toString());
		return content;

		/*
		 * if (isPub) { Rule rule = ruleDao.getRuleById(ruleId); if
		 * (rule.getIsFileLocal() == 1) {// 本地上传 content.put("FileName",
		 * rule.getRuleName()); content.put("ContentId",
		 * rule.getFileId().toString()); } else {// 保存数据库 JecnFileBean
		 * jecnFileBean = this.fileDao.findJecnFileBeanById(rule.getFileId());
		 * content.put("FileName", rule.getRuleName()); content.put("ContentId",
		 * jecnFileBean.getVersionId().toString()); } } else { RuleT ruleT =
		 * ruleDao.get(ruleId); if (ruleT.getIsFileLocal() == 1) {// 本地上传
		 * content.put("FileName", ruleT.getRuleName());
		 * content.put("ContentId", ruleT.getFileId().toString()); } else {//
		 * 保存数据库 JecnFileBeanT jecnFileBean =
		 * this.fileDao.get(ruleT.getFileId()); content.put("FileName",
		 * ruleT.getRuleName()); content.put("ContentId",
		 * jecnFileBean.getVersionId().toString()); } } return content;
		 */
	}

	@Override
	public G020RuleFileContent getG020RuleFileContentByRuleId(Long ruleId, boolean isPub) throws Exception {
		try {
			String table = "jecn_rule_t";
			if (isPub) {
				table = "jecn_rule";
			}
			String sql = " select a.* from g020_rule_file_content a inner join " + table + " r"
					+ " on a.id=r.file_id and r.is_file_local = 1 and r.id=" + ruleId;
			Query query = this.fileDao.getSession().createSQLQuery(sql).addEntity(G020RuleFileContent.class);
			List<G020RuleFileContent> list = query.list();
			return list.size() == 0 ? null : list.get(0);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getRuleTreeBeanByFlowId(Long flowId) throws Exception {
		List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();
		try {
			String sql = "select jr.id,"
					+ "         case"
					+ "           when jr.is_dir = 2 and jr.is_file_local = 0 then"
					+ "            jf.file_name"
					+ "           else"
					+ "            jr.rule_name"
					+ "         end as ruleName,"
					+ "         jp.id as pid,"
					+ "         jr.IS_DIR,"
					+ "         case when jr.is_dir = 2 and jr.is_file_local = 0 then jf.doc_id else jr.rule_number end as rule_number,"
					+ "         case when jrr.id is null then 0"
					+ "         else 1 end as ispub,0 as count,jr.TEMPLATE_ID"
					+ "    from flow_related_criterion_t t, jecn_rule_t jr"
					+ "    inner join jecn_rule_t jp on jr.per_id=jp.id" + "    left join jecn_file_t jf"
					+ "      on jf.file_id = jr.file_id" + "    left join jecn_rule jrr on jr.id=jrr.id"
					+ "     and jf.del_state = 0"
					+ "   where  t.criterion_class_id = jr.id and jr.DEL_STATE=0 and t.flow_id = ?";

			List<Object[]> objs = ruleDao.listNativeSql(sql, flowId);
			treeBeans = getJecnTreeBeanByObject(objs);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return treeBeans;

	}

	@Override
	public List<JecnTreeBean> getRuleTreeBeanByRuleId(Long ruleId) throws Exception {
		List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();
		try {
			String sql = "select jr.id,"
					+ "         case"
					+ "           when jr.is_dir = 2 and jr.is_file_local = 0 then"
					+ "            jf.file_name"
					+ "           else"
					+ "            jr.rule_name"
					+ "         end as ruleName,"
					+ "         jp.id as pid,"
					+ "         jr.IS_DIR,"
					+ "         case when jr.is_dir = 2 and jr.is_file_local = 0 then jf.doc_id else jr.rule_number end as rule_number,"
					+ "         case when jrr.id is null then 0"
					+ "         else 1 end as ispub,0 as count,jr.TEMPLATE_ID"
					+ "    from JECN_RULE_RELATED_RULE_T t, jecn_rule_t jr"
					+ "    inner join jecn_rule_t jp on jr.per_id=jp.id" + "    left join jecn_file_t jf"
					+ "      on jf.file_id = jr.file_id" + "    left join jecn_rule jrr on jr.id=jrr.id"
					+ "     and jf.del_state = 0"
					+ "   where  t.related_id = jr.id and jr.DEL_STATE=0 and t.rule_id = ?";

			List<Object[]> objs = ruleDao.listNativeSql(sql, ruleId);
			treeBeans = getJecnTreeBeanByObject(objs);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return treeBeans;
	}

	@Override
	public List<JecnTreeBean> getStandardizedFiles(Long ruleId) throws Exception {
		String sql = "SELECT FRSF.ID, FRSF.RELATED_ID, FRSF.FILE_ID, FT.FILE_NAME ,FT.DOC_ID"
				+ "  FROM RULE_STANDARDIZED_FILE_T FRSF" + " INNER JOIN JECN_FILE_T FT"
				+ "    ON FT.FILE_ID = FRSF.FILE_ID AND FT.DEL_STATE=0" + " WHERE FRSF.RELATED_ID = ?";
		List<Object[]> list = ruleDao.listNativeSql(sql, ruleId);
		List<JecnTreeBean> treeBeanList = new ArrayList<JecnTreeBean>();
		JecnTreeBean treeBean = null;
		for (Object[] obj : list) {
			if (obj[2] == null) {
				continue;
			}
			treeBean = new JecnTreeBean();
			treeBean.setId(Long.valueOf(obj[2].toString()));
			treeBean.setName(obj[3].toString());
			treeBean.setNumberId(JecnUtil.objToStr(obj[4]));
			treeBeanList.add(treeBean);
		}
		return treeBeanList;
	}

	@Override
	public Map<EXCESS, List<FileOpenBean>> ruleFilesDownLoad(Long ruleId, boolean isPub, Long peopleId)
			throws Exception {
		JecnUser jecnUser = personService.get(peopleId);
		WebLoginBean bean = personService.getWebLoginBean(jecnUser.getLoginName(), null, false);
		return SelectDownloadProcess.ruleFilesDownLoad(ruleId, ruleDao, fileDao, isPub, bean);
	}

	@Override
	public FileOpenBean openFile(Long fileId, boolean isPub) throws Exception {
		return SelectDownloadProcess.openFile(fileId, isPub, fileDao);
	}

	/**
	 * webservice 接口根据登录名获取我的制度（公开和有查阅权限）
	 * 
	 * @param loginName
	 * @param start
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getRuleByWebService(String loginName, int start, int pageSize) throws Exception {
		WebLoginBean bean = personService.getWebLoginBean(loginName, null, false);
		if (bean == null || bean.getJecnUser() == null) {
			return null;
		}
		String sql = "SELECT MY_RULE.* FROM (SELECT DISTINCT T.ID," + "     CASE"
				+ "                  WHEN T.IS_DIR = 2 AND T.IS_FILE_LOCAL = 0 THEN"
				+ "                   JF.FILE_NAME" + "  ELSE" + "  T.RULE_NAME" + "                END AS RULENAME,"
				+ "   CASE" + "                  WHEN T.IS_DIR = 2 AND T.IS_FILE_LOCAL = 0 THEN" + "    JF.DOC_ID"
				+ "                  ELSE" + "      T.RULE_NUMBER" + "                END AS RULENUMBER,"
				+ "                CREATE_T.TRUE_NAME," + "                O.ORG_NAME,"
				+ "                T.PUB_TIME,T.IS_DIR" + "  FROM JECN_RULE T" + "  LEFT JOIN JECN_FILE JF"
				+ "    ON JF.FILE_ID = T.FILE_ID" + "   AND T.IS_DIR = 2" + "   AND T.IS_FILE_LOCAL = 0"
				+ "  LEFT JOIN JECN_USER CREATE_T" + "    ON CREATE_T.PEOPLE_ID = T.CREATE_PEOPLE_ID"
				+ "  LEFT JOIN JECN_USER JU" + "    ON JU.PEOPLE_ID = T.UPDATE_PEOPLE_ID"
				+ "  LEFT JOIN JECN_FLOW_ORG O"
				+ "    ON O.ORG_ID = T.ORG_ID WHERE T.IS_DIR <> 0 AND T.DEL_STATE = 0 ) MY_RULE";

		if (!bean.isAdmin() && !bean.isViewAdmin()) {
			sql = sql + "  inner join " + "("
					+ JecnCommonSqlTPath.INSTANCE.getAuthSQLCommon(bean.getProjectId(), TYPEAUTH.RULE, bean)
					+ ") MY_AUTH_FILES" + " on MY_AUTH_FILES.id=MY_RULE.id";
		}

		sql += " ORDER BY MY_RULE.PUB_TIME DESC";
		if (pageSize == -1) {// 获取所有
			return this.ruleDao.listNativeSql(sql);
		}
		return ruleDao.listNativeSql(sql, start, pageSize);
	}

	@Override
	public List<G020RuleFileContent> getG020RuleFileContents(Long id) {
		// 版本信息
		String hql = "select id,ruleId,updateTime,updatePeopleId,fileName from G020RuleFileContent where ruleId=? ORDER BY updateTime DESC";
		List<Object[]> list = fileDao.listHql(hql, id);
		List<G020RuleFileContent> listJecnFileContent = new ArrayList<G020RuleFileContent>();
		RuleT jecnFileBeanT = this.get(id);
		// JecnFileBean jecnFileBean = this.fileDao.findJecnFileBeanById(id);

		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[4] == null) {
				continue;
			}
			Long versionId = Long.valueOf(obj[0].toString());
			if (versionId.equals(jecnFileBeanT.getFileId())) {
				continue;
			}
			G020RuleFileContent jecnFileContent = new G020RuleFileContent();
			jecnFileContent.setId(versionId);
			jecnFileContent.setRuleId((Long) obj[1]);
			if (obj[2] != null) {
				jecnFileContent.setUpdateTime((Date) obj[2]);
			}
			if (obj[3] != null)
				jecnFileContent.setUpdatePeopleId((Long) obj[3]);
			jecnFileContent.setFileName(obj[4].toString());
			listJecnFileContent.add(jecnFileContent);
		}
		return listJecnFileContent;
	}

	@Override
	public void updateRecovery(long ruleId, long versionId, long updatePersonId) throws Exception {
		// TODO Auto-generated method stub
		try {
			RuleT rule = ruleDao.get(ruleId);
			if (rule == null) {
				return;
			}
			G020RuleFileContent jecnFileContent = (G020RuleFileContent) fileDao.getSession().get(
					G020RuleFileContent.class, versionId);
			if (jecnFileContent == null) {
				return;
			}
			byte[] bytes = null;
			if (jecnFileContent.getIsVersionLocal() == 0) {
				bytes = JecnFinal.getBytesByFilePath(jecnFileContent.getFilePath());
			} else {
				SerializableBlob sb = (SerializableBlob) jecnFileContent.getFileStream();
				Blob wrapBlob = sb.getWrappedBlob();
				bytes = new byte[(int) wrapBlob.length()];

				InputStream in = null;
				try {
					in = wrapBlob.getBinaryStream();
					in.read(bytes);
				} catch (Exception e) {
					log.error("", e);
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (Exception e) {
							log.error("", e);
						}
					}
				}
			}
			if (bytes == null) {
				return;
			}
			rule.setFileName(jecnFileContent.getFileName());
			rule.setRuleName(jecnFileContent.getFileName());
			rule.setUpdatePeopleId(updatePersonId);
			rule.setFileBytes(bytes);
			// 调用更新文件的方法
			updateLocalRuleFile(rule, false);
			JecnUtil.saveJecnJournal(rule.getId(), rule.getRuleName(), 4, 15, updatePersonId, fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	@Override
	public void deleteVersion(List<Long> listIds, Long ruleId, long userId) throws Exception {
		// TODO Auto-generated method stub
		try {
			RuleT jecnFileBeanT = ruleDao.get(ruleId);
			jecnFileBeanT.setUpdatePeopleId(userId);
			jecnFileBeanT.setUpdateDate(new Date());
			ruleDao.update(jecnFileBeanT);
			// if (jecnFileBeanT.getSaveType().intValue() == 0) {
			/** 获得本地文件版本信息 */
			String hql = "select filePath from G020RuleFileContent where id in" + JecnCommonSql.getIds(listIds);
			List<String> pathLists = this.fileDao.listHql(hql);
			for (String str : pathLists) {
				JecnFinal.deleteFile(str);
			}

			hql = "delete from G020RuleFileContent where id in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, listIds);
			for (String str : listStr) {
				ruleDao.execteHql(str);
			}
			JecnUtil.saveJecnJournal(jecnFileBeanT.getId(), jecnFileBeanT.getRuleName(), 4, 16, userId, fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public FileOpenBean openVersion(Long ruleId, Long versionId) throws Exception {
		try {
			return JecnDaoUtil.openRuleLocalFile(ruleDao, versionId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pId) throws Exception {
		try {
			String sql = "select distinct t.id," + "                t.rule_name," + "                t.per_id,"
					+ "                t.is_dir," + "                t.rule_number," + "                case"
					+ "                  when jr.id is null then" + "                   '0'" + "                  else"
					+ "                   '1'" + "                end as is_pub," + "                case"
					+ "                  when jup.id is null then" + "                   '0'"
					+ "                  else" + "                   '1'" + "                end as count,"
					+ "                t.TEMPLATE_ID," + "                t.FILE_ID,"
					+ "                t.sort_id,t.IS_FILE_LOCAL,t.del_state" + "  from jecn_rule_t t"
					+ "  left join jecn_rule jr on t.id = jr.id" + "  left join jecn_rule_t jup on jup.per_id = t.id"
					+ " where t.per_id = ?" + "  and t.del_state <> 2 and t.project_id = ?"
					+ " order by t.per_id, t.sort_id, t.id";
			List<Object[]> list = ruleDao.listNativeSql(sql, pId, projectId);
			return findByListObjectsRecy(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description： 回收站 封装Bean
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjectsRecy(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = this.getJecnTreeBeanByObjectRecy(obj);
			if (jecnTreeBean != null)
				listResult.add(jecnTreeBean);

		}
		return listResult;
	}

	private JecnTreeBean getJecnTreeBeanByObjectRecy(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
				|| obj[6] == null) {
			return null;
		}
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		// 制度节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		if (obj[1] != null) {
			// 制度节点名称
			jecnTreeBean.setName(obj[1].toString());
		} else {
			jecnTreeBean.setName("");
		}
		// 制度节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 制度节点父类名称
		jecnTreeBean.setPname("");
		// 制度类型
		if ("0".equals(obj[3].toString())) {
			jecnTreeBean.setTreeNodeType(TreeNodeType.ruleDir);
		} else if ("1".equals(obj[3].toString())) {
			jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeFile);
		} else {
			jecnTreeBean.setTreeNodeType(TreeNodeType.ruleFile);
		}
		// 制度编号
		if (obj[4] != null) {
			jecnTreeBean.setNumberId(obj[4].toString());
		} else {
			jecnTreeBean.setNumberId("");
		}
		// 是否发布
		if ("0".equals(obj[5].toString())) {
			jecnTreeBean.setPub(false);
		} else {
			jecnTreeBean.setPub(true);
		}
		// 是否子节点
		if (Integer.parseInt(obj[6].toString()) > 0) {
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}
		// 制度模板
		if (obj[7] != null) {
			jecnTreeBean.setModeId(Long.valueOf(obj[7].toString()));
		} else {
			jecnTreeBean.setModeId(null);
		}
		if (obj.length >= 9 && obj[8] != null) {
			jecnTreeBean.setRelationId(Long.valueOf(obj[8].toString()));
		}
		if (obj.length >= 11 && obj[10] != null) {
			jecnTreeBean.setDataType(Integer.valueOf(obj[10].toString()));
		}
		// 是否删除
		if (obj.length >= 12 && obj[11] != null) {
			jecnTreeBean.setIsDelete(Integer.parseInt(obj[11].toString()));
		}
		return jecnTreeBean;
	}

	@Override
	public List<JecnTreeBean> getAllFilesByName(String name, Long projectId, Set<Long> ruleIds, boolean isAdmin)
			throws Exception {
		String sql = "SELECT DISTINCT T.ID," + "                T.RULE_NAME," + "                T.PER_ID,"
				+ "                T.IS_DIR," + "                T.RULE_NUMBER," + "                CASE"
				+ "                  WHEN JR.ID IS NULL THEN" + "                   '0'" + "                  ELSE"
				+ "                   '1'" + "                END AS IS_PUB," + "                CASE"
				+ "                  WHEN JUP.ID IS NULL THEN" + "                   '0'" + "                  ELSE"
				+ "                   '1'" + "                END AS COUNT," + "                T.TEMPLATE_ID,"
				+ "                T.FILE_ID," + "                T.SORT_ID," + "                T.IS_FILE_LOCAL,"
				+ "                T.DEL_STATE" + "  FROM JECN_RULE_T T" + "  LEFT JOIN JECN_RULE JR ON T.ID = JR.ID"
				+ "  LEFT JOIN JECN_RULE_T JUP ON JUP.PER_ID = T.ID";
		// id的集合
		if (!isAdmin && !JecnContants.dbType.equals(DBType.MYSQL)) {
			if (ruleIds.size() > 0) {
				sql = sql + " INNER JOIN (" + JecnCommonSql.getChildObjectsSqlGetSingleIdRule(ruleIds)
						+ ") DESIGN_AUTH ON DESIGN_AUTH.ID=T.ID";
			} else {
				return new ArrayList<JecnTreeBean>();
			}
		}

		sql = sql + " WHERE T.RULE_NAME LIKE '%" + name + "%' AND T.DEL_STATE=1" + "   AND T.PROJECT_ID = ?";

		if (!isAdmin && !JecnContants.dbType.equals(DBType.MYSQL)) {
			if (ruleIds.size() > 0) {
				sql = sql + " and T.ID not in " + JecnCommonSql.getIdsSet(ruleIds);
			}
		}
		if ("".equals(sql)) {
			return new ArrayList<JecnTreeBean>();
		}

		List<Object[]> objs = ruleDao.listNativeSql(sql, 0, 40, projectId);
		return findByListObjectsRecy(objs);
	}

	@Override
	public void getRecycleFileIds(List<Long> listIds, Long projectId) throws Exception {
		if (listIds != null && listIds.size() > 0) {
			String sql = "update JECN_RULE_T set del_state=0 where  del_state <> 2  and   ID in ("
					+ JecnCommonSql.getChildObjectsSqlGetSingleIdRule(listIds) + ")";
			fileDao.execteNative(sql);
			sql = "update JECN_RULE set del_state=0 where del_state <> 2 and   ID in ("
					+ JecnCommonSql.getChildObjectsSqlGetSingleIdRule(listIds) + ")";
			fileDao.execteNative(sql);
			// 判断流程是否开启维护支撑文件
			if (JecnConfigTool.isArchitectureUploadFile()) {
				List<Long> relatedIds = this.ruleDao.getRelatedFileIdsByRuleIds(listIds);
				if (relatedIds.isEmpty()) {
					return;
				}
				fileService.updateRecycleFileOrDirIds(relatedIds, projectId);
			}
		}
	}

	@Override
	public void updateRecycleFile(List<Long> ids, Long projectId) throws Exception {
		if (ids != null && ids.size() > 0) {
			String sql = "update JECN_RULE_T set del_state=0 where ID in" + JecnCommonSql.getIds(ids);
			fileDao.execteNative(sql);
			sql = "update JECN_RULE set del_state=0 where ID in" + JecnCommonSql.getIds(ids);
			fileDao.execteNative(sql);

			// 判断流程是否开启维护支撑文件
			if (JecnConfigTool.isArchitectureUploadFile()) {
				List<Long> relatedIds = this.ruleDao.getRelatedFileIdsByRuleIds(ids);
				if (relatedIds.isEmpty()) {
					return;
				}
				fileService.updateRecycleFileOrDirIds(relatedIds, projectId);
			}
		}
	}

	@Override
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception {
		try {
			RuleT rule = ruleDao.get(id);
			Set<Long> setIds = JecnCommon.getPositionTreeIds(rule.gettPath(), id);
			String sql = " select distinct t.id,  t.rule_name, t.per_id,"
					+ "  t.is_dir, t.rule_number, case when jr.id is null then  '0' else '1'"
					+ "  end as is_pub,case when sub.id is null then '0' else '1' end as count,"
					+ "   t.TEMPLATE_ID, t.FILE_ID, t.sort_id,t.IS_FILE_LOCAL,t.del_state" + "  from jecn_rule_t t"
					+ "  left join jecn_rule jr on t.id = jr.id" + "  left join jecn_rule_t sub on sub.per_id=t.id"
					+ " where t.PER_ID in " + JecnCommonSql.getIdsSet(setIds)
					+ " AND t.PROJECT_ID=? ORDER BY t.PER_ID,t.Sort_Id,t.ID";
			List<Object[]> list = ruleDao.listNativeSql(sql, projectId);
			return findByListObjectsRecy(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 根据path 创建制度 所属目录(标准化文件)
	 * 
	 * @param id
	 * @param projectId
	 * @param createPeopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long createRelatedFileDirByPath(Long id, Long projectId, Long createPeopleId) throws Exception {
		List<Object[]> parentRelateFiles;
		// 0:流程ID，1关联的文件ID,流程名称,3:sortId
		Long parentId = 0L;
		try {

			// 根据path 获取流程的目录,判断在文件目录中是否存在流程的目录结构
			parentRelateFiles = ruleDao.getParentRelatedFileByPath(id);

			for (Object[] objects : parentRelateFiles) {
				Long ruleId = Long.valueOf(objects[0].toString());
				Integer sortId = Integer.valueOf(objects[3].toString());
				if (objects[1] == null) {
					// 创建文件目录
					parentId = createFileDir(projectId, createPeopleId, parentId, objects[2].toString(), ruleId,
							sortId, id == ruleId.intValue());
					// 更新关联文件ID
					ruleDao.updateParentRelatedFileId(ruleId, parentId);
				} else {
					parentId = Long.valueOf(objects[1].toString());
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return parentId;
	}

	private Long createFileDir(Long projectId, Long createPeopleId, Long parentId, String fileName, Long flowId,
			Integer sortId, boolean isFile) throws Exception {
		if (isFile) {
			// 文件名称过滤，生成的目录不包含扩展
			fileName = fileName.indexOf(".") != -1 ? fileName.substring(0, fileName.lastIndexOf(".")) : fileName;
		}
		return fileService.addFileDir(JecnRelatedDirFileUtil.getFileBean(projectId, createPeopleId, parentId, fileName,
				flowId, sortId, 1));
	}

	private JecnFileBeanT getFileBeanT(RuleStandardizedFileT standardizedFileT, Long pId, int sortId, RuleT ruleT) {
		JecnFileBeanT fileBean = new JecnFileBeanT();
		fileBean.setFileName(standardizedFileT.getFileName());
		fileBean.setPeopleID(ruleT.getUpdatePeopleId());
		fileBean.setPerFileId(pId);
		fileBean.setUpdatePersonId(ruleT.getUpdatePeopleId());
		fileBean.setIsDir(1);// 1是文件 0是目录
		fileBean.setOrgId(null);
		fileBean.setDocId(standardizedFileT.getFileCode());
		fileBean.setHide(1); // 0 隐藏 1显示
		fileBean.setIsPublic(ruleT.getIsPublic());
		fileBean.setSortId(sortId);
		fileBean.setProjectId(ruleT.getProjectId());
		fileBean.setFileStream(standardizedFileT.getFileStream());
		fileBean.setDelState(0);
		fileBean.setRuleId(ruleT.getId());
		// 保密级别
		fileBean.setConfidentialityLevel(ruleT.getConfidentialityLevel());
		return fileBean;
	}

	@Override
	public Long getRelatedFileId(Long ruleId) throws Exception {
		return ruleDao.getRelatedFileId(ruleId);
	}

	@Override
	public boolean isNotAbolishOrNotDelete(Long id) throws Exception {
		String sql = "select count(t.id) from jecn_rule_t t where t.id = ? and t.del_state = 0";
		return ruleDao.countAllByParamsNativeSql(sql, id) > 0 ? true : false;
	}

	@Override
	public boolean validateAddNameDirRepeat(Long pid, String name) {
		String sql = "select count(*) from jecn_rule_t where is_dir = 0 and per_id=? and rule_name = ?";
		return ruleDao.countAllByParamsNativeSql(sql, pid, name) > 0;
	}

	@Override
	public String validateNamefullPath(String ruleName, Long id, int type) {
		String hql = "from RuleT where isDir =? and delState = 0 and ruleName = ? and perId <> ?";
		List<RuleT> listNativeSql = ruleDao.listHql(hql, type, ruleName, id);
		if (listNativeSql != null && !listNativeSql.isEmpty()) {
			return listNativeSql.get(0).getRuleName();
		}
		return "";
	}

	@Override
	public String[] validateNamefullPath(List<String> names, Long id, int type) {
		try {
			String hql = "select ruleName from RuleT where perId <> ? and isDir=? and delState = 0";
			List<String> listData = ruleDao.listHql(hql, id, type);
			List<String> listResult = new ArrayList<String>();
			for (String newName : names) {
				if (listData.contains(newName)) {
					listResult.add(newName);
				}
			}
			if (listResult.size() == 0) {
				return null;
			}
			String[] strArr = new String[listResult.size()];
			for (int i = 0; i < listResult.size(); i++) {
				strArr[i] = listResult.get(i);
			}
			return strArr;
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	@Override
	public void batchReleaseNotRecord(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) {
		try {
			for (Long id : ids) {
				directRelease(id, treeNodeType, peopleId);
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	@Override
	public void batchCancelRelease(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) {
		try {
			for (Long id : ids) {
				cancelRelease(id, treeNodeType, peopleId);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	@Override
	public void syncRules(SyncRules syncRule) throws Exception {
		Long toId = syncRule.getToId();
		SyncRule rule = null;
		for (int i = 0; i < syncRule.getRules().size(); i++) {
			rule = syncRule.getRules().get(i);
			if (rule.isDir()) {
				saveDir(rule, toId);
			} else {
				saveRule(rule, toId);
			}
			syncRule.getRules().set(i, null);
		}
	}

	private void saveRule(SyncRule rule, Long toId) throws Exception {
		List<RuleT> u = new ArrayList<RuleT>();
		RuleT self = rule.getSelf();
		setFileBytes(self);
		self.setPerId(toId);
		u.add(self);
		AddRuleFileData data = new AddRuleFileData();
		data.setList(u);
		data.setAccIds(new AccessId());
		addRuleFile(data);
	}

	private void saveDir(SyncRule rule, Long toId) throws Exception {
		RuleT self = rule.getSelf();
		self.setPerId(toId);
		addRuleDir(self);
		if (JecnUtil.isNotEmpty(rule.getRules())) {
			List<SyncRule> rules = rule.getRules();
			SyncRule r = null;
			for (int i = 0; i < rules.size(); i++) {
				r = rules.get(i);
				if (r.isDir()) {
					saveDir(r, self.getId());
				} else {
					saveRule(r, self.getId());
				}
				// 防止内存溢出
				rules.set(i, null);
			}
		}
	}

	private void setFileBytes(RuleT c) throws Exception {
		if (JecnConfigTool.isJinKe()) {// 从本地读取数据
			Object extra = c.getSyncExtra();
			if (extra != null) {
				InputStream in = null;
				try {
					in = new FileInputStream(new File(extra.toString()));
					c.setFileBytes(IOUtils.toByteArray(in));
				} catch (Exception e) {
					throw new Exception("金科获取附件异常，附件可能不存在，路径：" + extra, e);
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (Exception e2) {
						}
					}
				}
			}
		}
	}

	@Override
	public List<JecnTreeBean> getChildRules(Long pid, Long projectId, int... delState) throws Exception {
		try {

			String delCondition = "";
			if (delState.length == 0) {
				throw new IllegalArgumentException("getChildRules length==0");
			} else if (delState.length == 1) {
				delCondition = " = " + delState[0];
			} else {
				List<Integer> temp = new ArrayList<Integer>();
				for (int i : delState) {
					temp.add(i);
				}
				delCondition = " in " + JecnCommonSql.getIds(temp);
			}
			String sql = "select distinct t.id,"
					+ "                t.rule_name,"
					+ "                t.per_id,"
					+ "                t.is_dir,"
					+ "                t.rule_number,"
					+ "                case"
					+ "                  when jr.id is null then"
					+ "                   '0'"
					+ "                  else"
					+ "                   '1'"
					+ "                end as is_pub,"
					+ "                case"
					+ "                  when jup.id is null then"
					+ "                   '0'"
					+ "                  else"
					+ "                   '1'"
					+ "                end as count,"
					+ "                t.TEMPLATE_ID,"
					+ "                t.FILE_ID,"
					+ "                t.sort_id,t.IS_FILE_LOCAL, case when t.pub_time <> t.update_date then   1 else 0 end as isUpdate "
					+ "  from jecn_rule_t t" + "  left join jecn_rule jr on t.id = jr.id"
					+ "  left join jecn_rule_t jup on jup.per_id = t.id" + " where t.per_id = ? and t.del_state"
					+ delCondition + " and t.project_id = ?" + " order by t.per_id, t.sort_id, t.id";
			List<Object[]> list = ruleDao.listNativeSql(sql, pid, projectId);
			sql = "select T.ID,JECN_TASK.STATE,JECN_TASK.APPROVE_TYPE,JECN_TASK.EDIT" + "  from jecn_rule_t t"
					+ " INNER JOIN JECN_TASK_BEAN_NEW JECN_TASK" + "    ON JECN_TASK.R_ID = T.Id"
					+ "   AND JECN_TASK.IS_LOCK = 1" + "   AND JECN_TASK.STATE <> 5"
					+ "   AND JECN_TASK.TASK_TYPE in (2,3)" + " where t.per_id = ?" + "   and t.del_state = 0"
					+ "   and t.project_id = ? ";
			List<Object[]> listTask = ruleDao.listNativeSql(sql, pid, projectId);
			return getJecnTreeBeanByObject(list, listTask);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}
}
