package com.jecn.epros.server.webBean.integration;

import java.util.HashMap;
import java.util.Map;

/**
 * 风险清单bean
 * 
 * @author fuzhh 2013-12-13
 * 
 */
public class RiskListBean {
	/** 主键ID */
	private Long id;
	/** 级别 */
	private int level;
	/** 父ID */
	private Long parentId;
	/** 编号 */
	private String riskCode;
	/** 是否是目录：0：是，1：否 */
	private int isDir;
	/** 风险描述或目录名 */
	private String riskName;
	/** 等级 1：高 2：中 3：低 */
	private int grade;
	/** 序号 */
	private Integer sort;
	/** 标准化控制 */
	private String standardControl;
	/** 相关要求 */
	private String requirements;
	/** 控制目标集合 */
	private Map<Long, ControlTargetListBean> controlTargetMap = new HashMap<Long, ControlTargetListBean>();
	/** 内控ID 对应的内控 */
	private Map<Long, String> guideMap = new HashMap<Long, String>();
	/** 风险对应制度 */
	private Map<Long, RiskRuleBean> riskRuleMap = new HashMap<Long, RiskRuleBean>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public int getIsDir() {
		return isDir;
	}

	public void setIsDir(int isDir) {
		this.isDir = isDir;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getStandardControl() {
		return standardControl;
	}

	public void setStandardControl(String standardControl) {
		this.standardControl = standardControl;
	}

	public String getRequirements() {
		return requirements;
	}

	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}

	public Map<Long, String> getGuideMap() {
		return guideMap;
	}

	public void setGuideMap(Map<Long, String> guideMap) {
		this.guideMap = guideMap;
	}

	public Map<Long, ControlTargetListBean> getControlTargetMap() {
		return controlTargetMap;
	}

	public void setControlTargetMap(
			Map<Long, ControlTargetListBean> controlTargetMap) {
		this.controlTargetMap = controlTargetMap;
	}

	public Map<Long, RiskRuleBean> getRiskRuleMap() {
		return riskRuleMap;
	}

	public void setRiskRuleMap(Map<Long, RiskRuleBean> riskRuleMap) {
		this.riskRuleMap = riskRuleMap;
	}
}
