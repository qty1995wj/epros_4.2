package epros.designer.gui.popedom.organization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.bean.popedom.PositonInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 岗位属性
 * 
 * @author 2012-06-08
 *  
 * 修改人：chehuanbo
 * 时间：2014-10-23
 * 描述:添加同一部门下允许岗位名称相同
 */
public class PosPropertyDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(PosPropertyDialog.class);

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(530, 660);

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel(530, 377);

	/** 任职条件面板Qualifications */
	private JecnPanel qualificationsPanel = new JecnPanel(510, 200);

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(520, 30);

	/** 名称Lab */
	private JLabel posNameLab = new JLabel(JecnProperties.getValue("nameC"));

	/** 名称FIeld */
	private JecnTextField posField = new JecnTextField(350, 20);

	/** 名称必填提示Lab */
	private JLabel posNameRequriedLab = new JLabel("*");

	/** 编号Lab */
	private JLabel posNumLab = new JLabel(JecnProperties
			.getValue("actBaseNumC"));

	/** 编号FIeld */
	private JecnTextField posNumField = new JecnTextField(350, 20);

	/** 编号必填提示Lab */
	private JLabel posNumRequriedLab = new JLabel("*");

	/** 上级岗位Lab */
	private JLabel higtherLelPosLab = new JLabel(JecnProperties
			.getValue("upPositionC"));

	/** 上级岗位TestArea */
	private JecnTextArea higtherLelArea = new JecnTextArea();// 350, 40
	/** 上级岗位滚动面板 */
	private JScrollPane higtherLelScrollPane = new JScrollPane(higtherLelArea);

	/** 上级岗位IDTestArea */
	private String higtherLelId = "";

	/** 上级岗位选择Button */
	private JButton higtherLelBut = new JButton(JecnProperties
			.getValue("selectBtn"));

	/** 下属岗位Lab */
	private JLabel lowerLelPosLab = new JLabel(JecnProperties
			.getValue("downPostionC"));

	/** 下属岗位TestArea */
	private JecnTextArea lowerLelArea = new JecnTextArea(); // 350, 40
	/** 下属岗位滚动面板 */
	private JScrollPane lowerLelScrollPane = new JScrollPane(lowerLelArea);

	/** 下属岗位IDTestArea */
	private String lowerLelId = "";

	/** 下属岗位选择Button */
	private JButton lowerLelBut = new JButton(JecnProperties
			.getValue("selectBtn"));

	/** 部门内相关制度Lab */
	private JLabel deptInLab = new JLabel(JecnProperties
			.getValue("deptInRuleC"));

	/** 部门内相关制度TestArea */
	private JecnTextArea deptInArea = new JecnTextArea();// 350, 40
	/** 部门内相关制度滚动面板 */
	private JScrollPane deptInScrollPane = new JScrollPane(deptInArea);

	/** 部门内相关制度deptInRuleIds */
	private String deptInRuleIds = "";

	/** 部门内相关制度选择Button */
	private JButton deptInBut = new JButton(JecnProperties
			.getValue("selectBtn"));

	/** 部门外相关制度Lab */
	private JLabel deptOutLab = new JLabel(JecnProperties
			.getValue("deptOutRuleC"));

	/** 部门外相关制度TestArea */
	private JecnTextArea deptOutArea = new JecnTextArea();// 350, 40
	/** 部门外相关制度滚动面板 */
	private JScrollPane deptOutScrollPane = new JScrollPane(deptOutArea);

	/** 部门外相关制度IDTestArea */
	private String deptOutRuleIds = "";

	/** 部门外相关制度选择Button */
	private JButton deptOutBut = new JButton(JecnProperties
			.getValue("selectBtn"));

	/** 附件Lab */
	private JLabel posFileLab = new JLabel(JecnProperties.getValue("theAttachmentC"));

	/** 附件TextArea */
	private JecnTextArea posFileArea = new JecnTextArea();// 350, 40
	/** 附件滚动面板 */
	private JScrollPane posFileScrollPane = new JScrollPane(posFileArea);

	/** 附件IDTextArea */
	private String fileId = "";

	/** 附件选择Button */
	private JButton posFileBut = new JButton(JecnProperties
			.getValue("selectBtn"));

	/** 岗位类别Lab */
	private JLabel posTypeLab = new JLabel(JecnProperties
			.getValue("postionTypeC"));

	/** 岗位类别TestArea */
	private JecnTextArea posTypeArea = new JecnTextArea();// 410, 40
	/** 岗位类别滚动面板 */
	private JScrollPane posTypeScrollPane = new JScrollPane(posTypeArea);

	/** 岗位类别验证提示信息Lab */
	private VerifLabel posTypeVerfyLab = new VerifLabel(390);

	/** 核心价值Lab */
	private JLabel coreValueLab = new JLabel(JecnProperties
			.getValue("coreValueC"));

	/** 核心价值TestArea */
	private JecnTextArea coreValueArea = new JecnTextArea(); // 410, 40
	/** 核心价值滚动面板 */
	private JScrollPane coreValueScrollPane = new JScrollPane(coreValueArea);

	/** 核心价值验证提示信息Lab */
	private VerifLabel coreValueVerfyLab = new VerifLabel(390);

	/** 任职条件中的空白 */
	private JLabel qualLab = new JLabel();

	/** 学历Lab */
	private JLabel degreeLab = new JLabel(JecnProperties.getValue("education"));

	/** 学历TestArea */
	private JecnTextArea degreeArea = new JecnTextArea();// 150, 40
	/** 学历滚动面板 */
	private JScrollPane degreeScrollPane = new JScrollPane(degreeArea);

	/** 学历验证提示信息Lab */
	private VerifLabel degreeVerfyLab = new VerifLabel(130);

	/** 知识Lab */
	private JLabel knowledgeLab = new JLabel(JecnProperties
			.getValue("knowledge"));

	/** 知识TestArea */
	private JecnTextArea knowledgeArea = new JecnTextArea();// 150, 40
	/** 知识滚动面板 */
	private JScrollPane knowledgeScrollPane = new JScrollPane(knowledgeArea);

	/** 知识验证提示信息Lab */
	private VerifLabel knowledgeVerfyLab = new VerifLabel(130);

	/** 技能Lab */
	private JLabel skillLab = new JLabel(JecnProperties.getValue("skill"));

	/** 技能TestArea */
	private JecnTextArea skillArea = new JecnTextArea();// 150, 40
	/** 技能滚动面板 */
	private JScrollPane skillScrollPane = new JScrollPane(skillArea);

	/** 技能验证提示信息Lab */
	private VerifLabel skillVerfyLab = new VerifLabel(130);

	/** 素质模型Lab */
	private JLabel qualityModelLab = new JLabel(JecnProperties
			.getValue("diathesisModel"));

	/** 素质模型TestArea */
	private JecnTextArea qualityModelArea = new JecnTextArea();// 150, 40
	/** 素质模型滚动面板 */
	private JScrollPane qualityModelScrollPane = new JScrollPane(
			qualityModelArea);

	/** 素质模型验证提示信息Lab */
	private VerifLabel qualityModelVerfyLab = new VerifLabel(130);

	private JLabel verfyLab = new JLabel();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties
			.getValue("cancelBtn"));
	
	/**设置统一部门下岗位名称相同 0:不允许 1：允许*/
	private Long allowSamePosName=null;
	
	/** 设置大小 */
	private Dimension dimension = null;

	private JecnTreeNode selectNode = null;
	private JecnTree jTree = null;

	/** 岗位ID */
	Long posId = null;

	/** 岗位名称 */
	String posName = null;
	/** 岗位编号 */
	String posNum = null;
	private List<JecnTreeBean> upPositionList;
	private List<JecnTreeBean> lowerPositionList;
	protected List<JecnTreeBean> inListRule;
	protected List<JecnTreeBean> outListRule;
	private List<JecnTreeBean> posFileList;

	/*** 判断 点击取消 是否提示关闭按钮 */
	private boolean isUpdate = false;
	// 岗位基础信息
	JecnPositionFigInfo jecnPositionFigInfo = null;

	public PosPropertyDialog(JecnTreeNode selectNode, JecnTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setTitle(JecnProperties.getValue("positionProp"));
		this.setSize(550, 650);
		this.setLocationRelativeTo(null);
		this.setModal(true);

		// 上级岗位
		higtherLelArea.setBorder(null);
		higtherLelScrollPane.setBorder(BorderFactory
				.createLineBorder(Color.gray));
		higtherLelScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		higtherLelScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		lowerLelArea.setBorder(null);
		lowerLelScrollPane
				.setBorder(BorderFactory.createLineBorder(Color.gray));
		lowerLelScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		lowerLelScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 部门内相关制度l
		deptInArea.setBorder(null);
		deptInScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		deptInScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		deptInScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 部门外相关制度
		deptOutArea.setBorder(null);
		deptOutScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		deptOutScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		deptOutScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 岗位说明书
		posFileArea.setBorder(null);
		posFileScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		posFileScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		posFileScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 岗位类别
		posTypeArea.setBorder(null);
		posTypeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		posTypeScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		posTypeScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 核心价值
		coreValueArea.setBorder(null);
		coreValueScrollPane.setBorder(BorderFactory
				.createLineBorder(Color.gray));
		coreValueScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		coreValueScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 学历
		degreeArea.setBorder(null);
		degreeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		degreeScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		degreeScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 知识
		knowledgeArea.setBorder(null);
		knowledgeScrollPane.setBorder(BorderFactory
				.createLineBorder(Color.gray));
		knowledgeScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		knowledgeScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 技能
		skillArea.setBorder(null);
		skillScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		skillScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		skillScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 素质模型滚动面板 */
		qualityModelArea.setBorder(null);
		qualityModelScrollPane.setBorder(BorderFactory
				.createLineBorder(Color.gray));
		qualityModelScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		qualityModelScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		verfyLab.setForeground(Color.red);
		/**
		 * 读取同一部门下是否允许岗位名称相同
		 * 
		 * @author cheuhanbo
		 * 
		 * */
		try {
			allowSamePosName = Long.valueOf(ConnectionPool.getConfigAciton()
					.selectValue(5, "allowSamePosName"));
		} catch (Exception e) {
			log.error("PosPropertyDialog rows：198 is error", e);
		}
		//==============end===========================
		initCompotents();
		initLayout();
		actionInit();

	}

	private void initCompotents() {
		// 设置面板背景颜色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		qualificationsPanel.setBackground(JecnUIUtil
				.getDefaultBackgroundColor());
		// 任职条件中的空白qualLab
		dimension = new Dimension(50, 0);
		qualLab.setPreferredSize(dimension);
		qualLab.setMaximumSize(dimension);
		qualLab.setMinimumSize(dimension);

		posNameRequriedLab.setForeground(Color.red);
		posNumRequriedLab.setForeground(Color.red);

		// 根据岗位ID获取岗位属性信息
		posId = selectNode.getJecnTreeBean().getId();
		posName = selectNode.getJecnTreeBean().getName();
		posNum = selectNode.getJecnTreeBean().getNumberId();
		try {
			PositonInfo positonInfo = ConnectionPool.getOrganizationAction()
					.getPositionInfo(posId);
			upPositionList = positonInfo.getUpPositionList();
			lowerPositionList = positonInfo.getLowerPositionList();
			inListRule = positonInfo.getInListRule();
			outListRule = positonInfo.getOutListRule();
			posFileList = positonInfo.getFileList();
			// 名称
			this.posField.setText(posName);
			// 岗位编号
			this.posNumField.setText(posNum);
			// this.posNumField.setText();
			higtherLelId = JecnDesignerCommon.getLook(upPositionList, higtherLelArea);
			lowerLelId = JecnDesignerCommon.getLook(lowerPositionList, lowerLelArea);
			deptInRuleIds = JecnDesignerCommon.getLook(inListRule, deptInArea);
			deptOutRuleIds = JecnDesignerCommon.getLook(outListRule, deptOutArea);
			fileId = JecnDesignerCommon.getLook(posFileList, posFileArea);

			jecnPositionFigInfo = positonInfo.getPositionBean();
			if (jecnPositionFigInfo != null) {

				// 岗位类别
				String posType = jecnPositionFigInfo.getPositionsNature();
				if (posType != null && !"".equals(posType)) {
					this.posTypeArea.setText(posType);
				}

				// 核心价值
				String coreValue = jecnPositionFigInfo.getSchoolExperience();
				if (coreValue != null && !"".equals(coreValue)) {
					this.coreValueArea.setText(coreValue);
				}
				// 学历
				String strDegree = jecnPositionFigInfo.getKnowledge();
				if (strDegree != null && !"".equals(strDegree)) {
					this.degreeArea.setText(strDegree);
				}
				// 知识
				String strKnowle = jecnPositionFigInfo.getPositionSkill();
				if (strKnowle != null && !"".equals(strKnowle)) {
					this.knowledgeArea.setText(strKnowle);
				}
				// 技能
				String strKill = jecnPositionFigInfo.getPositionImportance();
				if (strKill != null && !"".equals(strKill)) {
					this.skillArea.setText(strKill);
				}
				// 素质模型
				String strModel = jecnPositionFigInfo
						.getPositionDiathesisModel();
				if (strModel != null && !"".equals(strModel)) {
					this.qualityModelArea.setText(strModel);
				}
			}
		} catch (Exception e1) {
			log.error("PosPropertyDialog initCompotents is error", e1);
		}
		higtherLelArea.setEditable(false);
		lowerLelArea.setEditable(false);
		deptInArea.setEditable(false);
		deptOutArea.setEditable(false);
		posFileArea.setEditable(false);
	}

	/**
	 *布局
	 */
	private void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setBorder(BorderFactory.createTitledBorder(""));
		GridBagConstraints c = null;
		Insets insetop = new Insets(0, 2, 0, 2);
		Insets insets = new Insets(1, 2, 0, 2);
		Insets insetEm = new Insets(8, 2, 8, 2);
		Insets insetTy = new Insets(8, 2, 0, 2);
		// infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetop, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		// infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 名称
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(posNameLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetEm, 0, 0);
		infoPanel.add(posField, c);
		// 名称必填提示符
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(posNameRequriedLab, c);
		// 编号
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(posNumLab, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetEm, 0, 0);
		infoPanel.add(posNumField, c);
		// 名称必填提示符
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(posNumRequriedLab, c);

		// 上级岗位
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(higtherLelPosLab, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insetEm, 0, 0);
		infoPanel.add(higtherLelScrollPane, c);
		c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(higtherLelBut, c);

		// 下属岗位
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(lowerLelPosLab, c);
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insetEm, 0, 0);
		infoPanel.add(lowerLelScrollPane, c);
		c = new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(lowerLelBut, c);
		if (JecnConstants.versionType==99||JecnConstants.versionType==100) {
			// 部门内相关制度
			c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm,
					0, 0);
			infoPanel.add(deptInLab, c);
			c = new GridBagConstraints(1, 4, 1, 1, 1.0, 1.0,
					GridBagConstraints.WEST, GridBagConstraints.BOTH, insetEm,
					0, 0);
			infoPanel.add(deptInScrollPane, c);
			c = new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm,
					0, 0);
			infoPanel.add(deptInBut, c);

			// 部门外相关制度
			c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm,
					0, 0);
			infoPanel.add(deptOutLab, c);
			c = new GridBagConstraints(1, 5, 1, 1, 1.0, 1.0,
					GridBagConstraints.WEST, GridBagConstraints.BOTH, insetEm,
					0, 0);
			infoPanel.add(deptOutScrollPane, c);
			c = new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm,
					0, 0);
			infoPanel.add(deptOutBut, c);
		}

		// 岗位说明书
		c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(posFileLab, c);
		c = new GridBagConstraints(1, 6, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insetEm, 0, 0);
		infoPanel.add(posFileScrollPane, c);
		c = new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetEm, 0, 0);
		infoPanel.add(posFileBut, c);

		// 岗位类别
		c = new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetTy, 0, 0);
		infoPanel.add(posTypeLab, c);
		c = new GridBagConstraints(1, 7, 2, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insetTy, 0, 0);
		infoPanel.add(posTypeScrollPane, c);
		// 岗位类别信息验证
		c = new GridBagConstraints(1, 8, 2, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(posTypeVerfyLab, c);

		// 核心价值
		c = new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(coreValueLab, c);
		c = new GridBagConstraints(1, 9, 2, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(coreValueScrollPane, c);
		// 核心价值信息验证
		c = new GridBagConstraints(1, 10, 2, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(coreValueVerfyLab, c);

		// qualificationsPanel
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetop, 0,
				0);
		mainPanel.add(qualificationsPanel, c);
		qualificationsPanel.setLayout(new GridBagLayout());
		qualificationsPanel.setBorder(BorderFactory.createTitledBorder("任职条件"));
		// 学历
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(degreeLab, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		qualificationsPanel.add(degreeScrollPane, c);
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(degreeVerfyLab, c);

		// 空白
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(qualLab, c);
		// 知识
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(knowledgeLab, c);
		c = new GridBagConstraints(2, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		qualificationsPanel.add(knowledgeScrollPane, c);
		c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(knowledgeVerfyLab, c);

		// 技能
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(skillLab, c);
		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		qualificationsPanel.add(skillScrollPane, c);
		c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(skillVerfyLab, c);

		// 素质模型
		c = new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(qualityModelLab, c);
		c = new GridBagConstraints(2, 4, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		qualificationsPanel.add(qualityModelScrollPane, c);
		c = new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		qualificationsPanel.add(qualityModelVerfyLab, c);

		// buttonPanel
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		// buttonPanel.setBorder(BorderFactory.createTitledBorder(""));

		this.getContentPane().add(mainPanel);

	}

	private void actionInit() {
		higtherLelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				higtherLelId = JecnDesignerCommon.setPositionLook(upPositionList,
						higtherLelArea, PosPropertyDialog.this, higtherLelId);

			}
		});
		lowerLelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lowerLelId = JecnDesignerCommon.setPositionLook(lowerPositionList,
						lowerLelArea, PosPropertyDialog.this, lowerLelId);
			}
		});
		deptInBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deptInRuleIds = JecnDesignerCommon.setRuleLook(inListRule, deptInArea,
						PosPropertyDialog.this, 5, deptInRuleIds);
			}
		});
		posFileBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileId = JecnDesignerCommon.setFileLook(posFileList, posFileArea,
						PosPropertyDialog.this, fileId);
			}
		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		deptOutBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deptOutRuleIds = JecnDesignerCommon.setRuleLook(outListRule,
						deptOutArea, PosPropertyDialog.this, 5, deptOutRuleIds);
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				if (checkCancel()) {// true：需要执行关闭 false：不要执行关闭
					return true;
				}
				return false;
			}
		});
	}

	/**
	 * 取消
	 */
	private void cancelButPerformed() {
		if (checkCancel()) {
			this.dispose();
		}
	}

	/***
	 * 点击取消时，判断面板可输入控件值是否与初始值一直
	 * 
	 * @return
	 */
	private boolean checkCancel() {
		// 岗位类别
		if (jecnPositionFigInfo != null
				&& jecnPositionFigInfo.getPositionsNature() != null) {
			if (!jecnPositionFigInfo.getPositionsNature().equals(
					posTypeArea.getText())) {
				isUpdate = true;
			}
		} else if (posTypeArea.getText() != null
				&& !"".equals(posTypeArea.getText())) {
			isUpdate = true;
		}
		// 核心价值
		if (jecnPositionFigInfo != null
				&& jecnPositionFigInfo.getSchoolExperience() != null) {
			if (!jecnPositionFigInfo.getSchoolExperience().equals(
					coreValueArea.getText())) {
				isUpdate = true;
			}
		} else if (coreValueArea.getText() != null
				&& !"".equals(coreValueArea.getText())) {
			isUpdate = true;
		}
		// 学历
		if (jecnPositionFigInfo != null
				&& jecnPositionFigInfo.getKnowledge() != null) {
			if (!jecnPositionFigInfo.getKnowledge()
					.equals(degreeArea.getText())) {
				isUpdate = true;
			}
		} else if (degreeArea.getText() != null
				&& !"".equals(degreeArea.getText())) {
			isUpdate = true;
		}
		// 知识
		if (jecnPositionFigInfo != null
				&& jecnPositionFigInfo.getPositionSkill() != null) {
			if (!jecnPositionFigInfo.getPositionSkill().equals(
					knowledgeArea.getText())) {
				isUpdate = true;
			}
		} else if (knowledgeArea.getText() != null
				&& !"".equals(knowledgeArea.getText())) {
			isUpdate = true;
		}
		// 技能
		if (jecnPositionFigInfo != null
				&& jecnPositionFigInfo.getPositionImportance() != null) {
			if (!jecnPositionFigInfo.getPositionImportance().equals(
					skillArea.getText())) {
				isUpdate = true;
			}
		} else if (skillArea.getText() != null
				&& !"".equals(skillArea.getText())) {
			isUpdate = true;
		}
		// 素质模型
		if (jecnPositionFigInfo != null
				&& jecnPositionFigInfo.getPositionDiathesisModel() != null) {
			if (!jecnPositionFigInfo.getPositionDiathesisModel().equals(
					qualityModelArea.getText())) {
				isUpdate = true;
			}
		} else if (qualityModelArea.getText() != null
				&& !"".equals(qualityModelArea.getText())) {
			isUpdate = true;
		}
		if (isUpdate) {
			int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
					.getValue("yncancel"), null, JecnOptionPane.YES_NO_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return false;
			}
			return true;
		}
		return true;
	}

	/**
	 * 确定
	 */
	private void okButPerformed() {
		verfyLab.setText("");
		// 岗位基础信息
		if (jecnPositionFigInfo == null) {
			jecnPositionFigInfo = new JecnPositionFigInfo();
		}
		jecnPositionFigInfo.setFigureId(posId);
		// 学历
		jecnPositionFigInfo.setKnowledge(this.degreeArea.getText().trim());
		// 素质模型
		jecnPositionFigInfo.setPositionDiathesisModel(this.qualityModelArea
				.getText().trim());
		// 技能
		jecnPositionFigInfo.setPositionImportance(this.skillArea.getText().trim());
		// 知识
		jecnPositionFigInfo.setPositionSkill(this.knowledgeArea.getText().trim());
		// 岗位类别
		jecnPositionFigInfo.setPositionsNature(this.posTypeArea.getText().trim());
		// 核心价值
		jecnPositionFigInfo.setSchoolExperience(this.coreValueArea.getText().trim());
		// 岗位说明书
		if (DrawCommon.isNullOrEmtry(fileId)) {
			jecnPositionFigInfo.setFileId(null);
		} else {
			jecnPositionFigInfo.setFileId(Long.valueOf(fileId));
		}
		// 岗位名称
		String strPosName = this.posField.getText().trim();
		// 岗位编号
		String strPosNum = this.posNumField.getText().trim();
		// 验证名称
		String checkString = JecnUserCheckUtil.checkNullName(strPosName);
		if (!DrawCommon.isNullOrEmtryTrim(checkString)) {
			verfyLab.setText(JecnProperties.getValue("name") + checkString);
			return;
		}
		// 验证岗位编号
		String checkPosNum = JecnUserCheckUtil.checkNullName(strPosNum);
		if (!DrawCommon.isNullOrEmtryTrim(checkPosNum)) {
			verfyLab.setText(JecnProperties.getValue("actBaseNum")
					+ checkPosNum);
			return;
		}
		// 验证岗位类别
		if (checkNameMaxLength(this.posTypeArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("postionType")
					+ JecnProperties.getValue("inputContentLength")
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 验证核心价值
		if (checkNameMaxLength(this.coreValueArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("coreValue")
					+ JecnProperties.getValue("inputContentLength")
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 学历
		if (checkNameMaxLength(this.degreeArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("education")
					+ JecnProperties.getValue("inputContentLength")
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 知识
		if (checkNameMaxLength(this.knowledgeArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("knowledge")
					+ JecnProperties.getValue("inputContentLength")
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 技能
		if (checkNameMaxLength(this.skillArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("skill")
					+ JecnProperties.getValue("inputContentLength")
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 素质模型
		if (checkNameMaxLength(this.qualityModelArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("diathesisModel")
					+ JecnProperties.getValue("inputContentLength")
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		try {
            //判断同一部门下是否允许岗位名称相同 0：不允许 1:允许
			if (allowSamePosName == 0) {
				// 判断相同父节点下的组织名名称是否重复 getOrgInfo
				if (JecnTreeCommon.validateRepeatNameEidt(strPosName,
						selectNode)) {
					verfyLab.setText(JecnProperties.getValue("nameHaved"));
					return;
				}
			}
			// 验证编号全表唯一
			if (ConnectionPool.getOrganizationAction().validateAddUpdatePosNum(
					strPosNum, selectNode.getJecnTreeBean().getId())) {
				verfyLab.setText(JecnProperties.getValue("posNumHaved"));
				return;
			} else {
				verfyLab.setText("");
			}

			// 更新岗位属性信息
			ConnectionPool.getOrganizationAction()
					.updatePosition(jecnPositionFigInfo, higtherLelId,
							lowerLelId, deptInRuleIds, deptOutRuleIds,
							selectNode.getJecnTreeBean().getId(), strPosName,
							strPosNum);
			// // 岗位重命名
			// ConnectionPool.getOrganizationAction().reNamePosition(
			// selectNode.getJecnTreeBean().getId(), strPosName);
			// 重命名
			selectNode.getJecnTreeBean().setNumberId(strPosNum);
			JecnTreeCommon.reNameNode(jTree, selectNode, strPosName);
			this.dispose();
		} catch (Exception e) {
			log.error("PosPropertyDialog okButPerformed is error", e);
		}

	}

	public static boolean checkNameMaxLength(String str) {
		// 因为在不同编码格式下  getBytes.length 对于汉字的字节数计算不同 需要统一转换为GBK 格式
		try {
			return (str != null && str.trim().getBytes("GBK").length > 1200) ? true
					: false;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	class VerifLabel extends JLabel {
		public VerifLabel(int widthInt) {
			initComponents(widthInt);
		}

		private void initComponents(int widthInt) {
			// 项的内容大小
			dimension = new Dimension(widthInt, 15);
			this.setPreferredSize(dimension);
			this.setMinimumSize(dimension);
			this.setMaximumSize(dimension);
			this.setForeground(Color.red);
		}
	}

}
