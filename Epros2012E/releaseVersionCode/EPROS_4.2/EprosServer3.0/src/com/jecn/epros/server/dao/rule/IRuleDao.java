package com.jecn.epros.server.dao.rule;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.JecnFlowCommon;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfo;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.rule.JecnRuleCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleContent;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleFile;
import com.jecn.epros.server.bean.rule.RuleFileT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.WebLoginBean;

public interface IRuleDao extends IBaseDao<RuleT, Long> {
	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除制度
	 * @param setIds
	 * @throws Exception
	 */
	public void deleteRules(Set<Long> setIds) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除制度
	 * @param projectId
	 * @throws Exception
	 */
	public void deleteRules(Long projectId) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：撤销制度（文件）的数据
	 * @param listIds
	 * @throws Exception
	 */
	public void revokeRuleFileData(List<Long> listIds) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：发布制度的数据
	 * @param listIds
	 * @throws Exception
	 */
	public void releaseRuleFileData(List<Long> listIds) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：撤销制度（模版）的数据
	 * @param listIds
	 * @throws Exception
	 */
	public void revokeRuleModeData(List<Long> listIds) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：发布制度的数据
	 * @param listIds
	 * @throws Exception
	 */
	public void releaseRuleModeData(List<Long> listIds) throws Exception;

	/**
	 * 查询制度标题信息
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @throws Exception
	 */
	public List<RuleTitleT> findRuleTitle(Long ruleId) throws Exception;

	/**
	 * 查询制度操作说明内容 (临时表)
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @throws Exception
	 */
	public List<RuleContentT> findRuleContentT(Long ruleId) throws Exception;

	/**
	 * 查询制度操作说明内容 (非临时表)
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @throws Exception
	 */
	public List<RuleContent> findRuleContent(Long ruleId) throws Exception;

	/**
	 * 查询制度操作说明文件信息 （临时表）
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @return
	 * @throws Exception
	 */
	public List<RuleFileT> findRuleFileT(Long ruleId) throws Exception;

	/**
	 * 查询制度操作说明文件信息 （非临时表）
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @return
	 * @throws Exception
	 */
	public List<RuleFile> findRuleFile(Long ruleId) throws Exception;

	/**
	 * 查询制度操作说明流程信息
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowCommon> findRuleFlow(Long ruleId, Long isFlow, int isOpration, boolean isPub) throws Exception;

	/**
	 * 查询制度操作说明标准信息
	 * 
	 * @param ruleId
	 *            制度ID
	 * @param isOpration
	 *            是否是操作说明使用的数据0:是，1：否
	 * @return
	 * @throws Exception
	 */
	public List<JecnStandarCommon> findRuleStandar(Long ruleId, int isOpration) throws Exception;

	/**
	 * 查询制度操作说明风险信息
	 * 
	 * @param ruleId
	 *            制度ID
	 * @param isOpration
	 *            是否是操作说明使用的数据0:是，1：否
	 * @return
	 * @throws Exception
	 */
	public List<JecnRiskCommon> findRuleRisk(Long ruleId, int isOpration) throws Exception;
	
	/**
	 * 查询制度操作说明风险信息
	 * 
	 * @param ruleId
	 *            制度ID
	 * @param isOpration
	 *            是否是操作说明使用的数据0:是，1：否
	 * @return
	 * @throws Exception
	 */
	public List<JecnRuleCommon> findRule(Long ruleId, int isOpration, boolean isPub) throws Exception;

	/**
	 * 获得制度文件发布文件
	 * 
	 * @param ruleId
	 * @return
	 * @throws Exception
	 */
	public List<Long> findRuleFileNoPubFiles(Long ruleId) throws Exception;

	/**
	 * 获得制度模版发布文件
	 * 
	 * @param ruleId
	 * @return
	 * @throws Exception
	 */
	public List<Long> findRuleModeNoPubFiles(Long ruleId) throws Exception;

	/**
	 * 制度对象
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Rule getRuleById(Long id) throws Exception;

	public RuleT getRuleTById(Long id) throws Exception;

	/**
	 * 获得相关流程
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRuleRelateFlows(Long id, boolean isPub) throws Exception;

	/**
	 * @author yxw 2012-12-12
	 * @description:当文件修改时，同时修改对应的制度名称
	 * @param newName
	 * @param relationId
	 */
	public void reNameRuleByRelationId(String newName, Long relationId);

	/**
	 * 获取制度责任部门名称
	 * 
	 * @author fuzhh 2014-1-21
	 * @param orgId
	 * @throws Exception
	 */
	public String getOrgName(Long orgId) throws Exception;

	List<Object[]> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception;

	List<Long> getRuleStandardizedFileIds(Long id, String isPub, WebLoginBean bean) throws Exception;
	

	List<Long> getRuleFileIds(Long id, String isPub, WebLoginBean bean) throws Exception;

	JecnRuleBaiscInfoT getJecnRuleBaiscInfoTById(Long ruleId) throws Exception;

	JecnRuleBaiscInfo getJecnRuleBaiscInfoById(Long ruleId) throws Exception;

	List<Long> getStandFileByFlowId(Long ruleId, WebLoginBean loginBean, String isPub) throws Exception;

	void updateParentRelatedFileId(Long id, Long relatedFileId) throws Exception;

	List<Object[]> getParentRelatedFileByPath(Long id) throws Exception;

	Long getRelatedFileId(Long ruleId) throws Exception;

	public List<Long> getRelatedFileIdsByRuleIds(List<Long> listIds);

	public List<Long> getRuleStandardizedFileIds(Long ruleId, String pubStr, WebLoginBean bean, int accessType);

	public List<Long> getStandFileByFlowId(Long ruleId, WebLoginBean bean, String pubStr, int accessType);
	
	public G020RuleFileContent getG020RuleFileContent(Long fileId);

	public List<Long> getRuleFileIds(Long ruleId, String pubStr, WebLoginBean bean, int fileAccessType);

	public List<Long> getRuleModelFileIds(Long ruleId, String pubStr, WebLoginBean bean, int fileAccessType);

}
