package com.jecn.svg.dataModel.epros;

public class TimeLineBaseData {

	private Long id;
	private String type;
	private String number;
	/** 目标值 */
	private Double targetValue;
	/** 现状值 */
	private Double statusValue;
	private Long startDataId;
	private Long endDataId;
	private Long x;
	private Long y;
	private Long width;
	private Long height;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Long getStartDataId() {
		return startDataId;
	}

	public void setStartDataId(Long startDataId) {
		this.startDataId = startDataId;
	}

	public Long getEndDataId() {
		return endDataId;
	}

	public void setEndDataId(Long endDataId) {
		this.endDataId = endDataId;
	}

	public Long getX() {
		return x;
	}

	public void setX(Long x) {
		this.x = x;
	}

	public Long getY() {
		return y;
	}

	public void setY(Long y) {
		this.y = y;
	}

	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}

	public Double getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(Double targetValue) {
		this.targetValue = targetValue;
	}

	public Double getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(Double statusValue) {
		this.statusValue = statusValue;
	}

}
