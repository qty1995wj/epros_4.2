package com.jecn.epros.domain.rule;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.domain.process.Path;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author yxw 2012-11-26
 * @description：制度搜索显示列表对应bean
 */
public class RuleInfoBean implements Serializable {

    private List<Path> paths = new ArrayList<Path>();

    /**
     * 制度目录ID
     */
    private Long ruleId;
    /**
     * 制度名称
     */
    private String ruleName;
    /**
     * 制度编号
     */
    private String ruleNum;
    /**
     * 制度类别ID
     */
    private Long ruleTypeNameId;
    /**
     * 制度类别
     */
    private String ruleTypeName;
    /**
     * 责任部门
     */
    private Long dutyOrgId;
    /**
     * 责任部门
     */
    private String dutyOrg;
    /**
     * 责任部门包含父部门集合
     */
    private List<Map<String,Object>> dutyOrgList;
    /**
     * 密级
     */
    private int secretLevel;
    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date pubDate;
    /**
     * 制度类型 0：目录1：制度模板文件2：制度文件
     */
    private Integer isDir;
    /**
     * 制度文件ID
     */
    private Long fileId;
    /**
     * 版本号
     */
    private String versionId;
    /**
     * 处于什么阶段 0是待建，1是审批，2是发布
     */
    private Integer typeByData;
    /**
     * 级别
     */
    private Integer level;
    /**
     * 制度父节点ID
     */
    private Long rulePerId;
    /**
     * 制度责任人ID
     */
    private Long responsibleId;
    /**
     * 制度责任人名称
     */
    private String responsibleName;
    /**
     * 制度责任人类型 0是人员 1 是岗位
     */
    private Integer responsibleType = 0;
    /**
     * path
     **/
    private String path;
    /**
     * 是否发布 非空则是发布，不为空则是未发布
     **/
    private Integer isPub;
    /**
     * 审批阶段
     **/
    private Integer state;
    /**
     * 发布时间
     */
    private String updateDate;

    /**
     * 流程有效期
     */
    private Integer expiry;

    /**
     * 保密级别
     */
    private Integer confidentialityLevel;

    /**
     * 制度监护人ID
     */
    private Long guardianPeopleId;
    /**
     * 制度监护人名称
     */
    private String guardianPeopleName;
    /**
     * 制度拟稿人
     */
    private String draftPeople;

    public String getDraftPeople() {
        return draftPeople;
    }

    public void setDraftPeople(String draftPeople) {
        this.draftPeople = draftPeople;
    }

    public String getGuardianPeopleName() {
        return guardianPeopleName;
    }

    public void setGuardianPeopleName(String guardianPeopleName) {
        this.guardianPeopleName = guardianPeopleName;
    }

    public Long getGuardianPeopleId() {
        return guardianPeopleId;
    }

    public void setGuardianPeopleId(Long guardianPeopleId) {
        this.guardianPeopleId = guardianPeopleId;
    }

    public Integer getResponsibleType() {
        return responsibleType;
    }

    public void setResponsibleType(Integer responsibleType) {
        this.responsibleType = responsibleType;
    }

    public List<Map<String, Object>> getDutyOrgList() {
        return dutyOrgList;
    }

    public void setDutyOrgList(List<Map<String, Object>> dutyOrgList) {
        this.dutyOrgList = dutyOrgList;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public void setDutyOrgId(Long dutyOrgId) {
        this.dutyOrgId = dutyOrgId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleNum() {
        return ruleNum;
    }

    public void setRuleNum(String ruleNum) {
        this.ruleNum = ruleNum;
    }

    public Long getRuleTypeNameId() {
        return ruleTypeNameId;
    }

    public void setRuleTypeNameId(Long ruleTypeNameId) {
        this.ruleTypeNameId = ruleTypeNameId;
    }

    public String getRuleTypeName() {
        return ruleTypeName;
    }

    public void setRuleTypeName(String ruleTypeName) {
        this.ruleTypeName = ruleTypeName;
    }

    public Long getDutyOrgId() {
        return dutyOrgId;
    }

    public String getDutyOrg() {
        return dutyOrg;
    }

    public void setDutyOrg(String dutyOrg) {
        this.dutyOrg = dutyOrg;
    }

    public int getSecretLevel() {
        return secretLevel;
    }

    public void setSecretLevel(int secretLevel) {
        this.secretLevel = secretLevel;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public Integer getIsDir() {
        return isDir;
    }

    public void setIsDir(Integer isDir) {
        this.isDir = isDir;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public Integer getTypeByData() {
        return typeByData;
    }

    public void setTypeByData(Integer typeByData) {
        this.typeByData = typeByData;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getRulePerId() {
        return rulePerId;
    }

    public void setRulePerId(Long rulePerId) {
        this.rulePerId = rulePerId;
    }

    public Long getResponsibleId() {
        return responsibleId;
    }

    public void setResponsibleId(Long responsibleId) {
        this.responsibleId = responsibleId;
    }

    public String getResponsibleName() {
        return responsibleName;
    }

    public void setResponsibleName(String responsibleName) {
        this.responsibleName = responsibleName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getIsPub() {
        return isPub;
    }

    public void setIsPub(Integer isPub) {
        this.isPub = isPub;
    }

    public Integer getExpiry() {
        return expiry;
    }

    public void setExpiry(Integer expiry) {
        this.expiry = expiry;
    }

    public Integer getConfidentialityLevel() {
        return confidentialityLevel;
    }

    public void setConfidentialityLevel(Integer confidentialityLevel) {
        this.confidentialityLevel = confidentialityLevel;
    }


}
