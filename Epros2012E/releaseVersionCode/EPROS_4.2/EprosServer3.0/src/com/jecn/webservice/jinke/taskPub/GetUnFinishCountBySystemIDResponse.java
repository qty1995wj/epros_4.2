/**
 * GetUnFinishCountBySystemIDResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetUnFinishCountBySystemIDResponse  implements java.io.Serializable {
    private com.jecn.webservice.jinke.taskPub.BpmRetrunValue getUnFinishCountBySystemIDResult;

    public GetUnFinishCountBySystemIDResponse() {
    }

    public GetUnFinishCountBySystemIDResponse(
           com.jecn.webservice.jinke.taskPub.BpmRetrunValue getUnFinishCountBySystemIDResult) {
           this.getUnFinishCountBySystemIDResult = getUnFinishCountBySystemIDResult;
    }


    /**
     * Gets the getUnFinishCountBySystemIDResult value for this GetUnFinishCountBySystemIDResponse.
     * 
     * @return getUnFinishCountBySystemIDResult
     */
    public com.jecn.webservice.jinke.taskPub.BpmRetrunValue getGetUnFinishCountBySystemIDResult() {
        return getUnFinishCountBySystemIDResult;
    }


    /**
     * Sets the getUnFinishCountBySystemIDResult value for this GetUnFinishCountBySystemIDResponse.
     * 
     * @param getUnFinishCountBySystemIDResult
     */
    public void setGetUnFinishCountBySystemIDResult(com.jecn.webservice.jinke.taskPub.BpmRetrunValue getUnFinishCountBySystemIDResult) {
        this.getUnFinishCountBySystemIDResult = getUnFinishCountBySystemIDResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUnFinishCountBySystemIDResponse)) return false;
        GetUnFinishCountBySystemIDResponse other = (GetUnFinishCountBySystemIDResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getUnFinishCountBySystemIDResult==null && other.getGetUnFinishCountBySystemIDResult()==null) || 
             (this.getUnFinishCountBySystemIDResult!=null &&
              this.getUnFinishCountBySystemIDResult.equals(other.getGetUnFinishCountBySystemIDResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetUnFinishCountBySystemIDResult() != null) {
            _hashCode += getGetUnFinishCountBySystemIDResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetUnFinishCountBySystemIDResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetUnFinishCountBySystemIDResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getUnFinishCountBySystemIDResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetUnFinishCountBySystemIDResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "BpmRetrunValue"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
