package com.jecn.epros.server.bean.rule;

import java.io.Serializable;

/**
 * 制度标准化文件
 * 
 * @author ZXH
 * @date 2017-6-30上午10:49:35
 */
public class RuleStandardizedFile implements Serializable {
	private String id;
	private Long relatedId;
	private Long fileId;
	private String fileName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
