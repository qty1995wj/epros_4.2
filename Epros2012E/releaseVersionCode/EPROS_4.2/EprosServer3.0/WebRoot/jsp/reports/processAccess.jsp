<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div" style="height: 99%;">
			<form action="" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="processAccessSearchText" style="overflow: hidden;"
						class="minWidth">
						<table width="100%">
							<tr>
								<td width="10%" height="25px" align="right">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="processName_access_id" maxLength="32" readonly="readonly"/>
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectProcessOrMaWindow("processName_access_id","processId_access_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
								</td>
								<td width="10%" align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_access_id"
										onkeyup="search(orgDutyName_access_id,orgDutyId_access_id,search_access_orgDutyName,'org');"
										onblur="divNode(search_access_orgDutyName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_access_id","orgDutyId_access_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_access_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td style="vertical-align: middle; width: 10%;" align="center">
									<a onClick="toggle(processAccessSearchText)"
										style="cursor: pointer;" class="packUp"> <img id="packUp"
											src="${basePath}images/iParticipateInProcess/on.gif"
											align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
												name="packUp" /> </span> </a>
								</td>
							</tr>

							<tr>
								<td width="8%" align="right">
									<!-- 时间段: -->
									<s:text name="timeC" />
								</td>
								<td width="24%" align="left">
									<s:textfield readonly="true" cssClass="Wdate" id="startTime"
										cssStyle="width:36%;border-color: #7F9DB9;"
										onclick="WdatePicker();" theme="simple" />
									-
									<s:textfield readonly="true" cssClass="Wdate" id="endTime"
										cssStyle="width:36%;border-color: #7F9DB9;"
										onclick="WdatePicker();" theme="simple" />
								</td>

								<td></td>
								<td></td>

								<td></td>
							</tr>
						</table>
					</div>
					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;" onclick="processAccessGridSearch()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;" onclick="processAccessSearchReset()" />
					</div>
				</div>
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_access_id" value="-1" />
				<!-- 流程ID -->
				<input type="hidden" id="processId_access_id" value="-1" />
			</form>

			<div id="processAccessGrid_id">
			</div>

			<div align="center" style="padding-top: 10px;">
				<a onclick="processAccessDownloadExcel()"
					style="cursor: pointer; color: #005EA7;" class="table"><s:text
						name="downLoadExcel" /> </a>
			</div>
		</div>
	</body>
</html>