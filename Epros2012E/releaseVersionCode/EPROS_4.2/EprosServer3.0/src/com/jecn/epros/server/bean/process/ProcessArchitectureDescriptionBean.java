package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class ProcessArchitectureDescriptionBean implements Serializable {
	private Long flowId;
	private String flowName;
	/** 流程编号 */
	private String numId;

	private JecnMainFlowT jecnMainFlowT;
	/** 上一级层级 */
	private JecnTreeBean pubBean;
	/** 下一级层级 */
	private List<JecnTreeBean> subList;

	/** 流程责任部门ID */
	private Long dutyOrgId;
	/** 流程责任部门名称 */
	private String dutyOrgName;
	/** 流程责任类型 0岗位 1人员 */
	private int dutyUserType;
	/** 责任人ID */
	private Long dutyUserId;

	/** 流程责任人名称 */
	private String dutyUserName;

	/** Level */
	private int level;

	/** 专员 */
	private Long commissionerId;
	private String commissionerName;

	/** 关键字 */
	private String keyWord;
	private Long updatePeopleId;
	
	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public String getNumId() {
		return numId;
	}

	public void setNumId(String numId) {
		this.numId = numId;
	}

	public JecnTreeBean getPubBean() {
		return pubBean;
	}

	public void setPubBean(JecnTreeBean pubBean) {
		this.pubBean = pubBean;
	}

	public List<JecnTreeBean> getSubList() {
		return subList;
	}

	public void setSubList(List<JecnTreeBean> subList) {
		this.subList = subList;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public JecnMainFlowT getJecnMainFlowT() {
		return jecnMainFlowT;
	}

	public void setJecnMainFlowT(JecnMainFlowT jecnMainFlowT) {
		this.jecnMainFlowT = jecnMainFlowT;
	}

	public Long getDutyOrgId() {
		return dutyOrgId;
	}

	public void setDutyOrgId(Long dutyOrgId) {
		this.dutyOrgId = dutyOrgId;
	}

	public String getDutyOrgName() {
		return dutyOrgName;
	}

	public void setDutyOrgName(String dutyOrgName) {
		this.dutyOrgName = dutyOrgName;
	}

	public int getDutyUserType() {
		return dutyUserType;
	}

	public void setDutyUserType(int dutyUserType) {
		this.dutyUserType = dutyUserType;
	}

	public Long getDutyUserId() {
		return dutyUserId;
	}

	public void setDutyUserId(Long dutyUserId) {
		this.dutyUserId = dutyUserId;
	}

	public String getDutyUserName() {
		return dutyUserName;
	}

	public void setDutyUserName(String dutyUserName) {
		this.dutyUserName = dutyUserName;
	}

	public Long getCommissionerId() {
		return commissionerId;
	}

	public void setCommissionerId(Long commissionerId) {
		this.commissionerId = commissionerId;
	}

	public String getCommissionerName() {
		return commissionerName;
	}

	public void setCommissionerName(String commissionerName) {
		this.commissionerName = commissionerName;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
}
