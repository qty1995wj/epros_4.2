package com.jecn.epros.server.action.designer.integration.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.integration.IJecnRiskAction;
import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.integration.JecnRuleRiskBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleStandardBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.integration.IJecnRiskService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/***
 * 风险Action实现 2013-10-30
 * 
 */
public class JecnRiskActionImpl implements IJecnRiskAction {
	private IJecnRiskService jecnRiskService;

	public IJecnRiskService getJecnRiskService() {
		return jecnRiskService;
	}

	public void setJecnRiskService(IJecnRiskService jecnRiskService) {
		this.jecnRiskService = jecnRiskService;
	}

	@Override
	public Long addJecnRiskOrDir(JecnRisk jecnRisk) throws Exception {
		return jecnRiskService.addJecnRiskOrDir(jecnRisk);
	}

	@Override
	public void reRiskName(String newName, Long riskId, Long updatePersonId) throws Exception {
		jecnRiskService.reRiskName(newName, riskId, updatePersonId);
	}

	@Override
	public int deleteRisk(List<Long> riskIds,Long peopleId) throws Exception {
		return jecnRiskService.deleteRisk(riskIds,peopleId);
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		jecnRiskService.moveNodes(listIds, pId, updatePersonId, moveNodeType);
	}

	@Override
	public List<JecnTreeBean> getChildsRisk(Long pid, Long projectId) throws Exception {
		return jecnRiskService.getChildsRisk(pid, projectId);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildsRisk(Long pid, Long projectId, Long peopleId) throws Exception {
		return jecnRiskService.getRoleAuthChildsRisk(pid, projectId, peopleId);
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception {
		jecnRiskService.updateSortNodes(list, pId, updatePersonId);
	}

	@Override
	public List<JecnTreeBean> getAllRisk() throws Exception {
		return jecnRiskService.getAllRisk();
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id) throws Exception {
		return null;
	}

	@Override
	public void addJecnControlTarget(List<JecnControlTarget> jecnControlTargetList) throws Exception {
		// 添加控制目标
		jecnRiskService.addJecnControlTarget(jecnControlTargetList);
	}

	@Override
	public JecnRisk getJecnRiskById(Long id) throws Exception {
		return jecnRiskService.getJecnRiskById(id);
	}

	@Override
	public Long EditJecnRiskProperty(JecnRisk jecnRisk) throws Exception {
		return jecnRiskService.EditJecnRiskProperty(jecnRisk);
	}

	@Override
	public List<JecnTreeBean> getChildsRiskDirs(Long pid, Long projectId) throws Exception {
		return jecnRiskService.getChildsRiskDirs(pid, projectId);
	}

	@Override
	public List<JecnControlTarget> getAllControlTargetByRiskId(Long riskId) throws Exception {
		return jecnRiskService.getAllControlTargetByRiskId(riskId);
	}

	@Override
	public List<JecnTreeBean> searchRiskByName(String name, Long peopleId) throws Exception {
		if (peopleId == null) {
			return jecnRiskService.searchRiskByName(name);
		}
		return jecnRiskService.searchRoleAuthByName(name, peopleId);
	}

	@Override
	public List<JecnTreeBean> getJecnRiskGuide(Long riskId) throws Exception {
		return jecnRiskService.getJecnRiskGuide(riskId);
	}

	@Override
	public int riskNumIsRepeat(String riskNum, Long riskId) throws Exception {
		return jecnRiskService.riskNumIsRepeat(riskNum, riskId);
	}

	/**
	 * 根据制度ID获取制度关联风险信息
	 * 
	 * @param ruleId
	 *            制度主键ID
	 * @return List<JecnRuleRiskBeanT>制度相关风险点集合
	 */
	@Override
	public List<JecnTreeBean> findJecnRuleRiskBeanTByRuleId(Long ruleId) throws Exception {
		return jecnRiskService.findJecnRuleRiskBeanTByRuleId(ruleId);
	}

	/**
	 * 制度相关标准信息
	 * 
	 * @param ruleId
	 *            制度ID
	 * @return List<JecnRuleStandardBeanT>制度相关标准信息
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> findJecnRuleStandardBeanTByRuleId(Long ruleId) throws Exception {
		return jecnRiskService.findJecnRuleStandardBeanTByRuleId(ruleId);
	}

	@Override
	public List<JecnTreeBean> getChooseDialogChildsRisk(Long pid, Long projectId) throws Exception {
		return jecnRiskService.getChooseDialogChildsRisk(pid, projectId);
	}

}
