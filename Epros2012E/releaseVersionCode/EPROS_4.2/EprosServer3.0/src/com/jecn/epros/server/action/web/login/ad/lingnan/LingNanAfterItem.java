package com.jecn.epros.server.action.web.login.ad.lingnan;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

public class LingNanAfterItem {
	private static Logger log = Logger.getLogger(LingNanAfterItem.class);
	private static ResourceBundle lingnanConfig;

	static {
		lingnanConfig = ResourceBundle.getBundle("cfgFile/lingnan/lingnanConfig");
	}

	public static String getValue(String key) {
		return lingnanConfig.getString(key);
	}

	public static String getOrgData() {
		return httpPost(LingNanAfterItem.getValue("ORG_SERVICE"));
	}

	public static String getUserPostData() {
		return httpPost(LingNanAfterItem.getValue("USER_POST_SERVICE"));
	}

	public static String getUserEmail() {
		return httpPost(LingNanAfterItem.getValue("USER_EMAIL"));
	}

	private static String httpPost(String postUrl) {
		JSONObject param = new JSONObject();
		param.put("app_code", LingNanAfterItem.getValue("app_code"));
		param.put("token", LingNanAfterItem.getValue("token"));
		// return
		// JSONObject.fromObject(HttpRequest.bidsHttpRequestPost("http://192.168.1.75/bids/api/bids/tree/queryProjectInfoByCode"
		String result = "";
		String query = param.toString();
		try {
			URL url = new URL(postUrl);// 组织关系
			// URL url = new
			// URL("http://192.168.1.75/bids/api/bids/shr/empPostion"); //人员岗位信息
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-type", "application/json;charset=UTF-8");
			connection.setRequestProperty("Charset", "UTF-8");
			String timestamp = System.currentTimeMillis() + "";
			String esbLoginToken = LingNanAfterItem.getValue("LOGIN_TOKEN");
			String signature = getSHA256(timestamp + esbLoginToken + timestamp);
			connection.setRequestProperty("timestamp", timestamp);
			connection.setRequestProperty("signature", signature);
			// 设置文件字符集:

			connection.connect();

			OutputStream os = connection.getOutputStream();

			os.write(query.getBytes("UTF-8"));
			os.flush();
			os.close();
			// 定义BufferedReader输入流来读取URL的响应
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
			// log.info(result);

			connection.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return result;
	}

	public static String getSHA256(String str) {
		MessageDigest messageDigest;
		String encodestr = "";
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.update(str.getBytes("UTF-8"));
			encodestr = byte2Hex(messageDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodestr;
	}

	/**
	 * 将byte转为16进制
	 * 
	 * @param bytes
	 * @return
	 */
	private static String byte2Hex(byte[] bytes) {
		StringBuffer stringBuffer = new StringBuffer();
		String temp = null;
		for (int i = 0; i < bytes.length; i++) {
			temp = Integer.toHexString(bytes[i] & 0xFF);
			if (temp.length() == 1) {
				// 1得到一位的进行补0操作
				stringBuffer.append("0");
			}
			stringBuffer.append(temp);
		}
		return stringBuffer.toString();
	}
}
