package com.jecn.epros.domain.process.processFile;

import java.util.Date;

/**
 * Created by zhr on 2017/11/8.
 */
public class JecnActiveOnLineBean {
    private String id;
    /** 流程ID */
    private Long processId;
    /** 活动ID */
    private Long activeId;
    /** 支持工具ID */
    private Long toolId;
    /** 上线时间 */
    private Date onLineTime;
    /** 系统名称 */
    private String sysName;


    /**信息化说明*/
    private String informationDescription;


    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public Long getActiveId() {
        return activeId;
    }

    public void setActiveId(Long activeId) {
        this.activeId = activeId;
    }

    public Long getToolId() {
        return toolId;
    }

    public void setToolId(Long toolId) {
        this.toolId = toolId;
    }

    public Date getOnLineTime() {
        return onLineTime;
    }

    public void setOnLineTime(Date onLineTime) {
        this.onLineTime = onLineTime;
    }

    public String getInformationDescription() {
        return informationDescription;
    }

    public void setInformationDescription(String informationDescription) {
        this.informationDescription = informationDescription;
    }



}
