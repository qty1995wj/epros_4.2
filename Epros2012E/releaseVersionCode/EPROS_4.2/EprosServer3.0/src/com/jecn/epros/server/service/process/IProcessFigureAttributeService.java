package com.jecn.epros.server.service.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowFigure;
import com.jecn.epros.server.bean.process.JecnFlowFigureNow;
import com.jecn.epros.server.common.IBaseService;

public interface IProcessFigureAttributeService extends IBaseService<JecnFlowFigureNow, Long>{
	
	/**
	 * @author yxw 2012-7-27
	 * @description:查询出全部元素当前属性
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowFigureNow> getAll()throws Exception;
	
	/**
	 * @author yxw 2012-7-27
	 * @description:查询出全部元素默认属性
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowFigure> getAllDefault()throws Exception;
	
	/**
	 * @author yxw 2012-7-27
	 * @description:修改
	 * @param jecnFlowFigureNowList
	 * @throws Exception
	 */
	public void updateAll(List<JecnFlowFigureNow> jecnFlowFigureNowList)throws Exception;
	
}
