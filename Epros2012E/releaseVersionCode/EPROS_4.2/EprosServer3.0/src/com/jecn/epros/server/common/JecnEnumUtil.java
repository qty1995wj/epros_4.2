package com.jecn.epros.server.common;

/**
 * 枚举类Util
 * 
 *@author admin
 * @date 2016-5-3下午03:42:34
 */
public class JecnEnumUtil {
	/**
	 * 浏览器版本判断
	 * 
	 * @author ZXH
	 * @date 2016-5-3下午03:42:14
	 */
	public enum BrowserName {
		ie9, ie10, firefox, ie11, chrome, Others
	}

	public enum AuthorName {
		admin, secondAdmin, design
	}
}
