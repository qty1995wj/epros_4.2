package com.jecn.epros.domain.report;

/**
 * Created by user on 2017/3/29.
 * 流程审视优化报表页面bean
 */
public class ProcessScan extends ReportBaseBean{

    private Integer finishNum;
    private Integer totalNum;
    private Integer percentage;
    public Integer getFinishNum() {
        return finishNum;
    }

    public void setFinishNum(Integer finishNum) {
        this.finishNum = finishNum;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}
