package com.jecn.epros.service.user;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.*;
import com.jecn.epros.domain.org.SearchPosResultBean;
import com.jecn.epros.domain.process.FlowSearchResultBean;
import com.jecn.epros.domain.process.map.DeptAerialData;
import com.jecn.epros.domain.report.base.CharItem;
import com.jecn.epros.domain.report.base.LineChartOptions;
import com.jecn.epros.domain.report.base.SerieData;
import com.jecn.epros.domain.risk.SearchRiskResultBean;
import com.jecn.epros.domain.rule.SearchRuleResultBean;
import com.jecn.epros.domain.user.*;
import com.jecn.epros.mapper.SecurityMapper;
import com.jecn.epros.mapper.UserMapper;
import com.jecn.epros.security.EprosPasswordEncoder;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.IDUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Params;
import com.jecn.epros.util.Utils;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Autowired
    private SecurityMapper securityMapper;

    @Override
    public List<MyResponsibilityProcess> findMyResponsibilityProcess(Map<String, Object> map) {
        Paging paging = (Paging) map.get(Params.PG);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "");
        return userMapper.findMyResponsibilityProcess(map);
    }


    @Override
    public List<MyResponsibilityRule> findMyResponsibilityRule(Map<String, Object> map) {
        Paging paging = (Paging) map.get(Params.PG);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "");
        return userMapper.findMyResponsibilityRule(map);
    }


    @Override
    public List<MyStar> getRecommendStars(Map<String, Object> map) {
        Paging paging = (Paging) map.get(Params.PG);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "sorted_Date,create_date desc");
        return userMapper.getRecommendStars(map);
    }

    @Override
    public List<MyStar> getMyStars(Map<String, Object> map) {
        Paging paging = (Paging) map.get(Params.PG);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "create_date desc");
        return userMapper.getMyStars(map);
    }


    @Override
    public MyStar getStar(Map<String, Object> params) {
        return userMapper.getStar(params);
    }

    @Override
    public void deleteStar(Map<String, Object> map) {
        userMapper.deleteStar(map);
    }

    @Override
    public void updateStar(Map<String, Object> map) {
        map.put("sortedDate", new Date());
        userMapper.updateStar(map);
    }

    @Override
    public void addStar(Map<String, Object> map) {
        map.put("createDate", new Date());
        map.put("id", Common.getUUID());
        userMapper.addStar(map);
    }

    @Override
    public UserResourceStatistics getMyResources(Map<String, Object> paramMap) {
        return userMapper.getMyResources(paramMap);
    }

    @Override
    public List<SearchRuleResultBean> getMyRules(Map<String, Object> paramMap) {
        return userMapper.getMyRules(paramMap);
    }

    @Override
    public List<SearchRiskResultBean> getMyRisks(Map<String, Object> paramMap) {
        return userMapper.getMyRisks(paramMap);
    }

    @Override
    public List<FlowSearchResultBean> getMyProcesses(Map<String, Object> paramMap) {
        return userMapper.getMyProcesses(paramMap);
    }

    @Override
    public List<TreeNodeAccess> getDefaultWebAccess() {
        List<String> listRoleId = userMapper.getDefaultWebAccessRoleId();
        if (listRoleId.size() > 0) {
            String roleId = listRoleId.get(0);
            if (!"".equals(roleId)) {
                Map<String, Object> paramMap = new java.util.HashMap<String, Object>();
                paramMap.put("roleId", roleId);
                List<TreeNodeAccess> listAll = userMapper.getWebAccesssByRoleId(paramMap);
                return sortTreeNodeAccess(listAll);
            }
        }
        return new ArrayList<TreeNodeAccess>();

    }

    private List<TreeNodeAccess> sortTreeNodeAccess(List<TreeNodeAccess> list) {
        List<TreeNodeAccess> resultList = new ArrayList<TreeNodeAccess>();
        for (TreeNodeAccess treeNodeAccess : list) {
            if ("0".equals(treeNodeAccess.getpId())) {
                resultList.add(treeNodeAccess);
                this.addChildTreeNodeAccess(resultList, treeNodeAccess, list);
            }
        }
        return resultList;
    }

    private void addChildTreeNodeAccess(List<TreeNodeAccess> resultList, TreeNodeAccess pTreeNodeAccess, List<TreeNodeAccess> list) {
        for (TreeNodeAccess treeNodeAccess : list) {
            if (pTreeNodeAccess.getId().equals(treeNodeAccess.getpId())) {
                pTreeNodeAccess.setIsLeaf(1);
                resultList.add(treeNodeAccess);
                this.addChildTreeNodeAccess(resultList, treeNodeAccess, list);
            }
        }
    }

    @Override
    public List<TreeNodeAccess> getWebAccessByPeopleId(Map<String, Object> paramMap) {
        List<String> listRoleId = userMapper.getWebAccessRoleIdByPeopleId(paramMap);
        if (listRoleId.size() > 0) {
            String roleId = listRoleId.get(0);
            if (!"".equals(roleId)) {
                paramMap = new java.util.HashMap<String, Object>();
                paramMap.put("roleId", roleId);
                List<TreeNodeAccess> listAll = userMapper.getWebAccesssByRoleId(paramMap);
                return sortTreeNodeAccess(listAll);
            }
        }
        return new ArrayList<TreeNodeAccess>();
    }

    @Override
    public void updateDefaultWebAccess(Map<String, Object> paramMap) {
        Long peopleId = (Long) paramMap.get("peopleId");
        List<String> listRoleId = userMapper.getDefaultWebAccessRoleId();
        List<String> menusList = (List<String>) paramMap.get("menusList");
        String roleId = "";
        if (listRoleId.size() > 0) {
            roleId = listRoleId.get(0);
        }
        if (!"".equals(roleId)) {
            updateRoleWebAccess(roleId, menusList, peopleId);
        }
    }


    private void updateRoleWebAccess(String roleId, List<String> menusList, Long peopleId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("roleId", roleId);
        List<RolePermissionsMenu> rolePermissionsMenuList = userMapper.getRolePermissionsMenusByRoleId(paramMap);
        for (String menuId : menusList) {
            boolean isExist = false;
            for (RolePermissionsMenu rolePermissionsMenu : rolePermissionsMenuList) {
                if (menuId.equals(rolePermissionsMenu.getMenuId())) {
                    isExist = true;
                    rolePermissionsMenuList.remove(rolePermissionsMenu);
                    break;

                }
            }
            if (!isExist) {
                paramMap = new HashMap<String, Object>();
                paramMap.put("guid", IDUtils.makeUUID());
                paramMap.put("roleId", roleId);
                paramMap.put("menuId", menuId);
                userMapper.addWebAccessRoleMenu(paramMap);
            }
        }

        for (RolePermissionsMenu rolePermissionsMenu : rolePermissionsMenuList) {
            paramMap = new HashMap<String, Object>();
            paramMap.put("guid", rolePermissionsMenu.getGuId());
            userMapper.deleteWebAccessRoleMenu(paramMap);
        }
        paramMap = new HashMap<String, Object>();
        paramMap.put("updateTime", new Date());
        paramMap.put("updatePeopleId", peopleId);
        paramMap.put("roleId", roleId);
        userMapper.updateWebAccessRole(paramMap);
    }

    @Override
    public void updateWebAccess(Map<String, Object> paramMap) {
        List<String> menusList = (List<String>) paramMap.get("menusList");
        List<String> listRoleId = userMapper.getWebAccessRoleIdByPeopleId(paramMap);
        Long peopleId = (Long) paramMap.get("peopleId");
        String roleId = "";
        if (listRoleId.size() > 0) {
            roleId = listRoleId.get(0);
        }
        if ("".equals(roleId)) {
            roleId = IDUtils.makeUUID();
            paramMap = new HashMap<String, Object>();
            paramMap.put("id", roleId);
            paramMap.put("name", peopleId.toString());
            paramMap.put("type", 1);
            paramMap.put("createTime", new Date());
            paramMap.put("createPeopleId", peopleId);
            paramMap.put("updateTime", new Date());
            paramMap.put("updatePeopleId", peopleId);
            userMapper.addWebAccessRole(paramMap);
            for (String menuId : menusList) {
                paramMap = new HashMap<String, Object>();
                paramMap.put("guid", IDUtils.makeUUID());
                paramMap.put("roleId", roleId);
                paramMap.put("menuId", menuId);
                userMapper.addWebAccessRoleMenu(paramMap);
            }

        } else {
            updateRoleWebAccess(roleId, menusList, peopleId);
        }
    }

    /**
     * 获取登录人员对应的权限菜单
     *
     * @param roleId
     * @return
     */
    @Override
    public PermissionsMenuInfo getPermissionsMenuById(List<String> roleId,int languageType) {
        Map<String,Object> doMap = new HashedMap();
        doMap.put("roleIds",roleId);
        doMap.put("languageType",languageType);
        List<PermissionsMenu> dataPermissionsMenus = userMapper.getPermissionsMenuById(doMap);
        return getResultMenuInfo(dataPermissionsMenus);
    }


    /**
     * 获取用户默认的权限菜单
     *
     * @return
     */
    private PermissionsMenuInfo getResultMenuInfo(List<PermissionsMenu> dataMenus) {
        PermissionsMenuInfo menuInfo = new PermissionsMenuInfo();
        if (dataMenus == null) {
            return menuInfo;
        }
        menuInfo.setSidebarMenus(getSidebarMenus(dataMenus));
        menuInfo.setSidebarContentMenuMap(getSidebarContentMenuMap(dataMenus));
        return menuInfo;
    }

    /**
     * 获取sidebar
     *
     * @param dataMenus
     * @return
     */
    private List<PermissionsMenu> getSidebarMenus(List<PermissionsMenu> dataMenus) {
        // sortId(10,10-01,10-02...11,11-01,11-02...)
        PermissionsMenu sidebarMenu = null;
        List<PermissionsMenu> sidebarMenus = new ArrayList<>();
        // 菜单对应的操作menu集合
        for (PermissionsMenu menu : dataMenus) {
            if (!"NULL".equals(menu.getpId())) { // 根据 数据库 PID  'NULL' 来判断是否父节点
                continue;
            }
            if (menu.getType() == 1) {//0：菜单节点;1：功能模块节点'
                continue;
            }
            findChildren(dataMenus, menu);
            sidebarMenus.add(menu);
        }
        return sidebarMenus;
    }

    private void findChildren(List<PermissionsMenu> dataMenus, PermissionsMenu menu) { //查询子节点
        for (PermissionsMenu menus : dataMenus) {
            if (menus.getType() == 1) {//0：菜单节点;1：功能模块节点'
                continue;
            }
            if (menu.getId().equals(menus.getpId())) {
                if (menu.getChildren() == null) {
                    List<PermissionsMenu> myChildrens = new ArrayList<PermissionsMenu>();
                    myChildrens.add(menus);
                    menu.setChildren(myChildrens);
                    findChildren(dataMenus,menus); //递归 继续查询 子节点
                } else {
                    menu.getChildren().add(menus);
                    findChildren(dataMenus,menus);
                }
            }

        }

    }


    /**
     * sidebar对应的功能菜单集合
     *
     * @param dataMenus
     * @return
     */
    private Map<PermissionsMenuEnum, List<PermissionsMenu>> getSidebarContentMenuMap
    (List<PermissionsMenu> dataMenus) {
        Map<PermissionsMenuEnum, List<PermissionsMenu>> sidebarContentMenuMap = new HashMap<>();
        PermissionsMenu sidebarMenu = null;
        for (PermissionsMenu menu : dataMenus) {
            if (sidebarMenu == null || !isModuleMenu(sidebarMenu.getSortId(), menu.getSortId())) {// 第一次循环，或sortId节点变更
                sidebarMenu = menu;
                sidebarContentMenuMap.put(PermissionsMenuEnum.valueOf(sidebarMenu.getModule()), new ArrayList<>());
                continue;
            }
            if (menu.getType() == 0) {//0：菜单节点;1：功能模块节点'
                continue;
            }
            sidebarContentMenuMap.get(PermissionsMenuEnum.valueOf(sidebarMenu.getModule())).add(menu);
        }
        return sidebarContentMenuMap;
    }

    /**
     * 是否父节点
     *
     * @param sidebarSortId
     * @param moduleSortId
     * @return
     */

    private boolean isModuleMenu(String sidebarSortId, String moduleSortId) {
        return (moduleSortId.contains(sidebarSortId) && moduleSortId.length() - sidebarSortId.length() == 3);
    }

    @Override
    public int getMyStarsNum(Map<String, Object> params) {
        return userMapper.getMyStarsNum(params);
    }

    @Override
    public LineChartOptions getVisitTopProcess(Map<String, Object> paramMap) {
        int topN = Integer.parseInt(paramMap.get("topN").toString());
        Date now = new Date();
        String endTime = Utils.getLastDayOfMonth(now, 0);
        String startTime = Utils.getLastDayOfMonth(now, -1);

        paramMap.put("startTime", startTime);
        paramMap.put("endTime", endTime);
        PageHelper.startPage(0, topN);
        LineChartOptions graph = new LineChartOptions();
        List<String> months = getOneYearBeforeMonths(now);
        graph.setxAxis(months);
        List<String> ids = userMapper.getVisitTopProcessIds(paramMap);
        if (ids.size() == 0) {
            return graph;
        }
        paramMap.put("ids", SqlCommon.getStrs(ids));
        List<ProcessVisitItem> data = userMapper.getVisitTopProcess(paramMap);
        graph.setSeries(getSeries(data, months));
        return graph;
    }

    @Override
    public List<CharItem> getProposeTopProcess(Map<String, Object> paramMap) {
        PageHelper.startPage(0, Integer.valueOf(paramMap.get("topN").toString()));
        List<CharItem> data = userMapper.getProposeTopProcess(paramMap);
        return data;
    }

    @Override
    public List<CharItem> getProposeTopRule(Map<String, Object> paramMap) {
        PageHelper.startPage(0, Integer.valueOf(paramMap.get("topN").toString()));
        List<CharItem> data = userMapper.getProposeTopRule(paramMap);
        return data;
    }

    @Override
    public List<CharItem> getVisitTopProcessHistogram(Map<String, Object> paramMap) {
        Date now = new Date();
        // 需要整整12个月份
        String endTime = Utils.getLastDayOfMonth(now, 0);
        String startTime = Utils.getLastDayOfMonth(now, -1);

        paramMap.put("startTime", startTime);
        paramMap.put("endTime", endTime);
        paramMap.put("typeAuth", AuthSqlConstant.TYPEAUTH.FLOW);
        PageHelper.startPage(0, Integer.valueOf(paramMap.get("topN").toString()));
        List<CharItem> data = userMapper.getVisitTopProcessHistogram(paramMap);
        return data;
    }

    @Override
    public List<CharItem> getVisitTopRuleHistogram(Map<String, Object> paramMap) {
        Date now = new Date();
        // 需要整整12个月份
        String endTime = Utils.getLastDayOfMonth(now, 0);
        String startTime = Utils.getLastDayOfMonth(now, -1);

        paramMap.put("startTime", startTime);
        paramMap.put("endTime", endTime);
        PageHelper.startPage(0, Integer.valueOf(paramMap.get("topN").toString()));
        List<CharItem> data = userMapper.getVisitTopRuleHistogram(paramMap);
        return data;
    }

    @Override
    public JecnUser getUser(Map<String, Object> paramMap) {
        return userMapper.getUser(Long.valueOf(paramMap.get("peopleId").toString()));
    }

    @Override
    public List<SearchPosResultBean> listUserOrgAndPosInfos(Map<String, Object> paramMap) {
        return userMapper.listUserOrgAndPosInfos(paramMap);
    }

    @Override
    public ViewResultWithMsg alterUserPwd(Map<String, Object> paramMap) {
        EprosPasswordEncoder pwdEncoder = new EprosPasswordEncoder();
        paramMap.put("pwd", pwdEncoder.encode(paramMap.get("oldPwd").toString().trim()));
        // 校验旧密码
        int oldPwdAccess = userMapper.validateUserPwd(paramMap);
        if (oldPwdAccess <= 0) {
            return new ViewResultWithMsg(false, JecnProperties.getValue("originalPasswordError"));
        }
        // 加密的密码
        String pwd = pwdEncoder.encode(paramMap.get("newPwd").toString().trim());
        paramMap.put("pwd", pwd);
        // 更新新密码
        userMapper.updateUserPwd(paramMap);
        return new ViewResultWithMsg(true, JecnProperties.getValue("passwordModificationSuccess"));
    }

    private List<SerieData> getSeries(List<ProcessVisitItem> data, List<String> months) {

        // key 为id value为一个map，该map的key为月份日期，value为值
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        for (ProcessVisitItem visitItem : data) {
            if (dataMap.get(visitItem.getId()) == null) {
                Map<String, String> link = fill(months);
                link.put(visitItem.getMonth(), visitItem.getNum());
                dataMap.put(visitItem.getId(), link);
            } else {
                Map<String, String> link = dataMap.get(visitItem.getId());
                link.put(visitItem.getMonth(), visitItem.getNum());
            }
        }

        List<SerieData> graphData = new ArrayList<>();
        SerieData serie;
        List<String> valueData;
        Set<String> ids = new HashSet<>();
        for (ProcessVisitItem visitItem : data) {
            if (ids.contains(visitItem.getId())) {
                continue;
            }
            ids.add(visitItem.getId());
            serie = new SerieData();
            graphData.add(serie);
            serie.setId(visitItem.getId());
            valueData = new ArrayList<>();
            valueData.addAll(dataMap.get(visitItem.getId()).values());
            serie.setData(valueData);
            serie.setName(concatNameAndNum(visitItem.getName(), valueData));
            dataMap.get(visitItem.getId());
        }
        return graphData;
    }

    private String concatNameAndNum(String name, List<String> valueData) {

        int total = 0;
        for (String value : valueData) {
            total += Integer.parseInt(value);
        }
        return name + "（" + total + "）";
    }

    private Map<String, String> fill(List<String> months) {
        Map<String, String> link = new LinkedHashMap<>();
        for (String month : months) {
            link.put(month, "0");
        }
        return link;
    }

    private List<String> getOneYearBeforeMonths(Date date) {
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(date);
        endCalendar.set(Calendar.DAY_OF_MONTH, 1);

        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        startCalendar.add(Calendar.YEAR, -1);

        List<String> months = new ArrayList<>();
        while (true) {
            if (endCalendar.compareTo(startCalendar) < 0) {
                break;
            }
            String tempTime = Common.getStringbyDate(startCalendar.getTime(), "yyyy-MM");
            months.add(tempTime);
            startCalendar.add(Calendar.MONTH, 1);
        }
        return months;
    }
    /**
     * 获取用户所属部门鸟瞰图
     *
     * @param map
     * @return
     */
    @Override
    public List<DeptAerialData> findDeptAerialMapsByPeople(Map<String, Object> map) {
        return userMapper.findDeptAerialMapsByPeople(map);
    }

    @Override
    public Long getUserIdByLoginName(Map map) {
        return userMapper.getUserIdByLoginName(map);
    }
}
