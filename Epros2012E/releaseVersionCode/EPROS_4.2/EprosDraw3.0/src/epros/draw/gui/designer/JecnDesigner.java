package epros.draw.gui.designer;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPanel;

import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.popup.JecnCloseableTabbedPopupMenu;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 对外接口
 * 
 * 画图工具嵌入设计器接口类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesigner {
	/** true:管理员；false：非管理员 */
	public static boolean isAdmin = false;

	/**
	 * 
	 * 添加工具栏标题按钮
	 * 
	 * @param text
	 *            标题按钮名称
	 * @param jpanel
	 *            标题按钮对应的工具栏面板
	 */
	public static void addTitleButton(String text, JPanel jpanel) {
		JecnDrawMainPanel.getMainPanel().getToolbar().addTitleButton(text,
				jpanel);
	}

	/**
	 * 
	 * 添加工具栏标题按钮
	 * 
	 * @param text
	 *            标题按钮名称
	 * @param jpanel
	 *            标题按钮对应的工具栏面板
	 */
	public static void insertTitleButton(String text, JPanel jpanel, int index) {
		JecnDrawMainPanel.getMainPanel().getToolbar().insertTitleButton(text,
				jpanel, index);
	}

	/**
	 * 
	 * 添加设计器树面板
	 * 
	 * @param panel
	 *            设计器树面板
	 */
	public static void addTreePanelToWestPanel(JPanel panel) {
		if (panel == null) {
			return;
		}
		JecnDrawMainPanel.getMainPanel().add(panel, BorderLayout.WEST);
	}

	/**
	 * 
	 * 流程图：画图面板右键菜单
	 * 
	 * @param menuItemlistList
	 *            <JMenuItem> 菜单模块
	 */
	public static void addPartMapWorkflowMenuItem(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null) {
			return;
		}
		JecnDesignerToolData.setPartMapWorkflowMenuItemlist(menuItemlist);
	}

	/**
	 * 
	 * 流程地图：画图面板右键菜单
	 * 
	 * @param menuItemlistList
	 *            <JMenuItem> 菜单模块
	 */
	public static void addTotalMapWorkflowMenuItem(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null) {
			return;
		}
		JecnDesignerToolData.setTotalMapWorkflowMenuItemlist(menuItemlist);
	}

	/**
	 * 
	 * 组织图：画图面板右键菜单
	 * 
	 * @param menuItemlistList
	 *            <JMenuItem> 菜单模块
	 */
	public static void addOrgMapWorkflowMenuItem(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null) {
			return;
		}
		JecnDesignerToolData.setOrgMapWorkflowMenuItemlist(menuItemlist);
	}

	/**
	 * 
	 * 流程图：流程元素右键菜单
	 * 
	 * @param menuItemlistList
	 *            <JMenuItem> 菜单模块
	 */
	public static void addPartMapFlowElemMenuItem(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null) {
			return;
		}
		JecnDesignerToolData.setPartMapFlowElemMenuItemlist(menuItemlist);
	}

	/**
	 * 
	 * 流程地图：流程元素右键菜单
	 * 
	 * @param menuItemlistList
	 *            <JMenuItem> 菜单模块
	 */
	public static void addTotalMapFlowElemMenuItem(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null) {
			return;
		}
		JecnDesignerToolData.setTotalMapFlowElemMenuItemlist(menuItemlist);
	}

	/**
	 * 
	 * 组织图：流程元素右键菜单
	 * 
	 * @param menuItemlistList
	 *            <JMenuItem> 菜单模块
	 */
	public static void addOrgMapFlowElemMenuItem(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null) {
			return;
		}
		JecnDesignerToolData.setOrgMapFlowElemMenuItemlist(menuItemlist);
	}

	/**
	 * 
	 * 设置Epros软件类型 默认是画图工具
	 * 
	 * @param eprosType
	 *            EprosType Epros软件类型
	 */
	public static void setEprosType(EprosType eprosType) {
		JecnSystemStaticData.setEprosType(eprosType);
	}

	/**
	 * 
	 * 添加设计器操作画图工具的对象
	 * 
	 * @param designer
	 *            JecnIDesigner 设计器操作画图工具的对象
	 */
	public void setDesigner(JecnIDesigner designer) {
		JecnDesignerProcess.getDesignerProcess().setDesigner(designer);
	}
	/**
	 * 删除所有画图面板且退出登录（设计器专用）
	 * 
	 * @param flag
	 *            boolean 是-否-取消 true: OK-NO-CANCEL；false：OK-NO
	 * @return boolean true:关闭所有画图面板成功 false:失败或没有关闭
	 * 
	 */
	public static boolean removeAllPanelAndLoginOut(boolean flag) {
		boolean isExitAll = JecnCloseableTabbedPopupMenu.removeAllPanel(flag);
		if (isExitAll && JecnSystemStaticData.isEprosDesigner()) {
			// 设计器用户退出
			JecnDesignerProcess.getDesignerProcess().loginOut();
		}
		return isExitAll;
	}
}
