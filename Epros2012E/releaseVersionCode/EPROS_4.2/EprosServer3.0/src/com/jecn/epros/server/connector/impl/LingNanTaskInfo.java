package com.jecn.epros.server.connector.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONObject;

import com.google.common.collect.Lists;
import com.jecn.epros.server.action.web.login.ad.lingnan.LingNanAfterItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.lingnan.task.ISysNotifyTodoWebService;
import com.jecn.webservice.lingnan.task.ISysNotifyTodoWebServiceService;
import com.jecn.webservice.lingnan.task.ISysNotifyTodoWebServiceServiceLocator;
import com.jecn.webservice.lingnan.task.NotifyTodoAppResult;
import com.jecn.webservice.lingnan.task.NotifyTodoRemoveContext;
import com.jecn.webservice.lingnan.task.NotifyTodoSendContext;

/**
 * 岭南代办
 * 
 * @author duanzh
 * @date 2019-03-23
 * 
 */
public class LingNanTaskInfo extends JecnBaseTaskSend {

	private IPersonDao personDao;

	private IPersonService personService;

	private static ISysNotifyTodoWebService webService = null;
	private static SimpleDateFormat df = null;// 设置日期格式

	static {
		ISysNotifyTodoWebServiceService serviceService = new ISysNotifyTodoWebServiceServiceLocator();
		try {
			webService = serviceService.getISysNotifyTodoWebServicePort();
			df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew taskBeanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long cancelPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (taskBeanNew.getUpState() == 0 && taskBeanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
			log.info("创建代办开始~");
			// 创建代办
			handlerTaskToDo(taskBeanNew, handlerPeopleIds, 1);
		} else if (taskBeanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
			myTaskBean.setTaskStage(taskBeanNew.getUpState());

			Set<Long> cancelIds = new HashSet<Long>();
			cancelIds.add(cancelPeopleId);
			handlerTaskToDo(taskBeanNew, cancelIds, 2);
		} else {// 一条from的数据,一条to 的数据
			// 代办转已办,是否为多审判断
			// 设置上一环节任务待办任务状态为false，表示未已审批；
			myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
			myTaskBean.setTaskStage(taskBeanNew.getUpState());

			Set<Long> cancelIds = new HashSet<Long>();
			cancelIds.add(cancelPeopleId);
			handlerTaskToDo(taskBeanNew, cancelIds, 2);
			if ((taskBeanNew.getTaskElseState() == 2 || taskBeanNew.getTaskElseState() == 3)
					|| taskBeanNew.getState() != taskBeanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				// 审批任务，发送下一环节任务待办
				handlerTaskToDo(taskBeanNew, handlerPeopleIds, 1);
			}
		}
	}

	/**
	 * 
	 * @param myTaskBean
	 * @param taskBeanNew
	 * @param handlerPeopleIds
	 * @param type
	 *            代办状态： 待办为1，已办为2,完成3
	 * @throws Exception
	 */
	private void handlerTaskToDo(JecnTaskBeanNew taskBeanNew, Set<Long> handlerPeopleIds, int type) {
		NotifyTodoAppResult result = null;
		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());
		// 发送代办
		try {
			for (Long handlerPeopleId : handlerPeopleIds) {
				switch (type) {
				case 1:// 创建待办
					NotifyTodoSendContext ntc = new NotifyTodoSendContext();
					ntc.setAppName(LingNanAfterItem.getValue("app_code"));
					ntc.setModelName(getTypeStr(taskBeanNew.getTaskType()));
					ntc.setModelId(getTaskOrderId(taskBeanNew, handlerPeopleId, 1));
					ntc.setSubject(taskBeanNew.getTaskName());// 待办标题
					ntc.setLink(getPCViewURL(taskBeanNew, handlerPeopleId, true));
					ntc.setDocCreator(getJsonValue("PersonNo", String.valueOf(createUser.getLoginName())).toString());
					ntc.setType(1);
					ntc.setTargets(setDataRows(taskBeanNew, handlerPeopleId));
					ntc.setCreateTime(df.format(new Date()));
					log.info("创建代办 josn = " + ntc.toString()+":"+ntc.getDocCreator());
					result = webService.sendTodo(ntc);
					break;
				case 2:// 已办
					NotifyTodoRemoveContext nrc = new NotifyTodoRemoveContext();
					nrc.setAppName(LingNanAfterItem.getValue("app_code"));
					nrc.setModelName(getTypeStr(taskBeanNew.getTaskType()));
					nrc.setModelId(getTaskOrderId(taskBeanNew, handlerPeopleId, 0));
					nrc.setOptType(1);
					nrc.setTargets(setDataRows(taskBeanNew, handlerPeopleId));
					log.info("代办转已办 josn = " + nrc.toString());
					result = webService.setTodoDone(nrc);
					break;
				default:
					break;
				}
		 	log.info("result  = " + result.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String getTypeStr(int taskType) {
		String str = "";
		/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
		switch (taskType) {
		case 0:
		case 4:
			str = "流程任务";
			break;
		case 1:
			str = "文件任务";
			break;
		case 2:
		case 3:
			str = "制度任务";
			break;
		default:
			break;
		}
		return str;
	}

	private String setDataRows(JecnTaskBeanNew taskBeanNew, Long handlerPeopleId) {
		List<JSONObject> jUserList = Lists.newArrayList();
		JecnUser handlerUser = personDao.get(handlerPeopleId);
		jUserList.add(getJsonValue("PersonNo", String.valueOf(handlerPeopleId)));
		jUserList.add(getJsonValue("DeptNo", ""));
		jUserList.add(getJsonValue("PostNo", ""));
		jUserList.add(getJsonValue("GroupNo", ""));
		jUserList.add(getJsonValue("LoginName", handlerUser.getLoginName()));
		jUserList.add(getJsonValue("Keyword", ""));
		jUserList.add(getJsonValue("LdapDN", ""));
		return jUserList.toString();
	}

	private JSONObject getJsonValue(String key, Object value) {
		JSONObject result = new JSONObject();
		result.put(key, value);
		return result;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		JecnUser createUser = null;
		if (personDao == null) {
			createUser = personService.get(beanNew.getCreatePersonId());
		} else {
			createUser = personDao.get(beanNew.getCreatePersonId());
		}

		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew.getTaskName()) + "审批");
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setRid(beanNew.getRid().toString());
		myTaskBean.setCreatePeopleLoginName(createUser.getLoginName());
		myTaskBean.setOperationType(beanNew.getTaskElseState());
		myTaskBean.setStageName(beanNew.getStateMark());
		myTaskBean.setCreatePeopleId(beanNew.getCreatePersonId());
		return myTaskBean;
	}

	private String getTaskName(String name) {
		return name.indexOf(".") != -1 ? name.substring(0, name.lastIndexOf(".")) : name;
	}

	private String getPCViewURL(JecnTaskBeanNew beanNew, Long handlerPeopleId, boolean isView) {
		return getMailUrl(beanNew.getId(), handlerPeopleId, false, isView);
	}

	private String getMobileViewURL(MyTaskBean myTaskBean, Long handlerPeopleId, boolean isView) {
		return getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, true, isView);
	}

	/**
	 * 删除代办
	 */
	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew taskBeanNew, JecnUser jecnUser, JecnUser createUser) {

	}

	/**
	 * 取消代办或删除代办
	 * cancelPeoples 审核人的id
	 * curPeopleId 操作人id
	 * 
	 */
	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;

		// 删除任务
		if (curPeopleId == null && (handlerPeopleIds == null || handlerPeopleIds.size() == 0)) {
			deleteTask(beanNew, cancelPeoples);
			return;
		}

		// 并行 - 代办转已办
		Set<Long> setCancelIds = new HashSet<Long>();
		setCancelIds.addAll(cancelPeoples);
		handlerTaskToDo(beanNew, setCancelIds, 2);

		// 审批任务，发送下一环节任务待办
		if (handlerPeopleIds != null && handlerPeopleIds.size() > 0) {
			Set<Long> setHandlerIds = new HashSet<Long>();
			setHandlerIds.addAll(handlerPeopleIds);
			handlerTaskToDo(beanNew, setHandlerIds, 1);
		}
	}

	private void deleteTask(JecnTaskBeanNew beanNew, List<Long> cancelPeoples) {
		try {
			for (Long cancelPeLongId : cancelPeoples) {
				NotifyTodoRemoveContext nrc = new NotifyTodoRemoveContext();
				nrc.setAppName(LingNanAfterItem.getValue("app_code"));
				nrc.setModelName(getTypeStr(beanNew.getTaskType()));
				nrc.setModelId(getTaskOrderId(beanNew, cancelPeLongId, 0));
				nrc.setOptType(1);
				nrc.setTargets(setDataRows(beanNew, cancelPeLongId));
				log.info("接收人" + setDataRows(beanNew, cancelPeLongId));
				log.info("唯一标识" + getTaskOrderId(beanNew, cancelPeLongId, 0));
				NotifyTodoAppResult result = webService.deleteTodo(nrc);
				if (2 == result.getReturnState()) {
					log.info("删除发送成功~");
				} else if (1 == result.getReturnState()) {
					log.error("删除发送失败~:" + result.getMessage());
				} else {
					log.info("删除未操作~");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
