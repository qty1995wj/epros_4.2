package com.jecn.epros.server.download.wordxml.gzjd;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.util.Unit;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/***
 * 广州机电操作说明模版下载
 * 
 * @author admin
 * 
 */
public class GZJDProcessModel extends ProcessFileModel {
	/*** 段落行距单倍倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);
	/*** 段落行距固定值23磅 *****/
	private ParagraphLineRule vLineFix23 = new ParagraphLineRule(23F,
			LineRuleType.EXACT);
	/*** 段落行距固定值36磅 *****/
	private ParagraphLineRule vLineFix36 = new ParagraphLineRule(36F,
			LineRuleType.EXACT);
	/******* 页脚数据要重审批环节里面获取数据 定义全局属性 *****************/
	// 流程审核人
	String deptReviewName = "";
	// 批准人
	String allowedName = "";

	/**
	 * 
	 * @param processDownloadBean
	 * @param path
	 * @param flowChartDirection
	 *            true 横向
	 */
	public GZJDProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setFlowChartScaleValue(6.7F);
		setDocStyle("．", textTitleStyle());
		getDocProperty().setNewTblWidth(17.47F);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.7F, 1.7F, 1.7F, 1.7F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	@Override
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 角色职责
		String roleResponsibility = JecnUtil.getValue("responsibilities");
		rowData.add(0, new String[] { roleName, roleResponsibility });
		Table tbl = createTableItem(processFileItem,
				new float[] { 3.5F, 13.97F }, rowData);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), false, true);
	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);

		ParagraphBean sectTitleStyle = new ParagraphBean(Constants.Align.left,
				"宋体", 24, true);
		sectTitleStyle.setSpace(Float.valueOf(0.0F), Float.valueOf(0.0F),
				this.vLine1);
		sectTitleStyle.setpStyle(PStyle.LVL1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(
				sectTitleStyle);
	}

	/**
	 * 活动说明
	 */
	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles,
			List<String[]> rowData) {
		createTitle(processFileItem);
		// 段落显示活动说明
		// 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 （长春轨道加入 6指标）
		for (int i = 0; i < rowData.size(); i++) {
			processFileItem.getBelongTo().createParagraph(
					(rowData.get(i)[0].isEmpty() ? "" : "[" + rowData.get(i)[0]
							+ "] ")
							+ rowData.get(i)[2], activityTitleStyle_());
			processFileItem.getBelongTo().createParagraph(
					JecnWordUtil.getContents(rowData.get(i)[3]),
					getDocProperty().getTextContentStyle());
		}
	}

	/**
	 * 流程记录
	 */
	@Override
	protected void a11(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		// 文件名称
		if (rowData.size() == 0) {
			processFileItem.getBelongTo().createParagraph(blank,
					getDocProperty().getTextContentStyle());
			return;
		}
		for (int i = 0; i < rowData.size(); i++) {
			/*
			 * if (rowData.get(i)[1] != null) { // 处理掉文件后缀名 String fileName =
			 * rowData.get(i)[1].toString(); fileName = RemoveExtName(fileName);
			 * processFileItem.getBelongTo().createParagraph(fileName,
			 * getDocProperty().getTextContentStyle());
			 * 
			 * }
			 */
			if (rowData.get(i)[1] != null) {
				String fileName = RemoveExtName(rowData.get(i)[1].toString());
				processFileItem.getBelongTo().createParagraph(
						"附件" + (i + 1) + "：" + "《" + fileName + "》",
						getDocProperty().getTextContentStyle());
			}
			/*
			 * if (i + 1 == rowData.size()) {
			 * processFileItem.getBelongTo().createParagraph( "附件" + (i + 1) +
			 * "：" + "《" + rowData.get(i)[1] + "》" + "。",
			 * getDocProperty().getTextContentStyle()); } else {
			 * processFileItem.getBelongTo().createParagraph( "附件" + (i + 1) +
			 * "：" + "《" + rowData.get(i)[1] + "》" + "；",
			 * getDocProperty().getTextContentStyle()); }
			 */
		}
	}

	/**
	 * 处理掉文件后缀名
	 * 
	 * @param fileName
	 * @return
	 */
	private String RemoveExtName(String fileName) {
		if (fileName != null && fileName.indexOf(".") != -1) {
			fileName = fileName.substring(0, fileName.lastIndexOf("."));
		}
		return fileName;
	}

	/**
	 * 相关制度
	 */
	@Override
	protected void a14(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		if (rowData.size() == 0) {
			processFileItem.getBelongTo().createParagraph(blank,
					getDocProperty().getTextContentStyle());
			return;
		}
		for (int i = 0; i < rowData.size(); i++) {
			if (i + 1 == rowData.size()) {
				processFileItem.getBelongTo().createParagraph(
						"（" + (i + 1) + "）  《" + rowData.get(i)[0] + "》" + "。",
						getDocProperty().getTextContentStyle());
			} else {
				processFileItem.getBelongTo().createParagraph(
						"（" + (i + 1) + "）  《" + rowData.get(i)[0] + "》" + "；",
						getDocProperty().getTextContentStyle());
			}
		}
	}

	/****
	 * 输入和输出
	 * 
	 * @param titleName
	 * @param inputorout
	 */
	@Override
	protected void a32(ProcessFileItem processFileItem, String[] input,
			String[] output) {
		createTitle(processFileItem);
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.xiaosi, false);
		pBean.setSpace(0.2F, 0.2F, vLine1);
		// 输入
		Paragraph p = processFileItem.getBelongTo().createParagraph(
				JecnUtil.getValue("inputC"), pBean);
		if (input != null && input.length > 1) {
			p.appendTextRun(input[0], getDocProperty().getTextContentStyle()
					.getFontBean());
			for (int i = 1; i < input.length; i++) {
				processFileItem.getBelongTo().createParagraph(
						"      " + input[i], pBean);
			}
		} else {
			p.appendTextRun(input, getDocProperty().getTextContentStyle()
					.getFontBean());
		}
		// 输出
		p = processFileItem.getBelongTo().createParagraph(
				JecnUtil.getValue("outputC"), pBean);
		if (output != null && output.length > 1) {
			p.appendTextRun(output[0], getDocProperty().getTextContentStyle()
					.getFontBean());
			for (int i = 1; i < output.length; i++) {
				processFileItem.getBelongTo().createParagraph(
						"      " + output[i], pBean);
			}
		} else {
			p.appendTextRun(output, getDocProperty().getTextContentStyle()
					.getFontBean());
		}

	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent(Sect titleSect) {
		/** 评审人集合 0 评审人类型 1名称 2日期3评审阶段标识 */
		List<Object[]> peopleList = processDownloadBean.getPeopleList();
		/** 评审人集合 NEW 0 阶段标识 1评审人类型 2评审人名称 3部门名称 4更新时间 5 排序 6用户ID */
		List<Object[]> peopleListNew = processDownloadBean.getPeopleListNew();
		// 　存储过滤重复人员数据后的结果（累加部门）
		// 存储新结果，过滤掉阶段标识stageMark和peopleId 一样的重复的数据 同时清空部门数据
		List<Object[]> finalPeopleListNew = new ArrayList<Object[]>();
		for (Object[] objects : peopleListNew) {
			if (notExits(objects, finalPeopleListNew)) {
				// 清空部门信息
				Object[] obj = objects.clone();
				obj[3] = "";
				finalPeopleListNew.add(obj);
			}
		}
		// 循环便利原始数据和过滤之后的数据 追加部门
		for (Object[] objects : peopleListNew) {
			for (Object[] finalObj : finalPeopleListNew) {
				// 如果finalObj中 （stageMark 、 peopleId）字段值 和objects对象的（stageMark
				// 、peopleId）字段值 相等 则把 objects 的部门追加到 finalObj部门中
				if (finalObj[0].toString().equals(objects[0].toString())
						&& finalObj[6].toString().equals(objects[6].toString())) {
					finalObj[3] = finalObj[3].toString().equals("") ? objects[3]
							.toString()
							: finalObj[3].toString() + "/"
									+ objects[3].toString();
				}
			}
		}
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		ParagraphBean pBean = new ParagraphBean(Align.left, "黑体",
				Constants.xiaosi, false);
		pBean.setSpace(0f, 0f, vLine1);
		for (int i = 0; i < 9; i++) {
			titleSect.createParagraph(" ", pBean);
		}
		// 华文中宋 30号
		pBean = new ParagraphBean(Align.center, "华文中宋", Constants.sanshihao,
				true);
		pBean.setSpace(0f, 0f, vLineFix36);
		titleSect.createParagraph(flowName, pBean);
		// 表格单元格高度是1cm 计算出需要添加多少个换行
		// 都不存在数据的时候是420磅 集合存在数据时候用450- 集合个数占用的的磅值
		float vline = 420;
		if (finalPeopleListNew != null && finalPeopleListNew.size() > 0) {
			double cm = (double) finalPeopleListNew.size();
			vline = (float) (450 - Unit.cmToPt(cm));
		}
		if ((finalPeopleListNew == null || finalPeopleListNew.size() == 0)
				&& peopleList != null && peopleList.size() > 0) {
			double cm = (double) peopleList.size();
			vline = (float) (450 - Unit.cmToPt(cm));
		}
		ParagraphLineRule vLineFix = new ParagraphLineRule(vline,
				LineRuleType.EXACT);
		pBean = new ParagraphBean(Align.left, "黑体", Constants.xiaosi, false);
		pBean.setSpace(0f, 0f, vLineFix);
		// 添加换行 目的是让审批列表能在一页显示
		titleSect.createParagraph(" ", pBean);
		TableBean tabBean = new TableBean();
		tabBean.setBorder(1.5F);
		tabBean.setBorderInsideH(1);
		tabBean.setBorderInsideV(1);
		tabBean.setTableLeftInd(0.35F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		/**** 添加评审人数据集合 此处分为两种 审批和直接发布 ******/
		List<String[]> rowData = new ArrayList<String[]>();
		String[] str = null;
		rowData.add(new String[] { "审批栏", "姓名", "部门", "签名", "日期" });
		if ((peopleList == null || peopleList.size() == 0)
				&& (finalPeopleListNew == null || finalPeopleListNew.size() == 0)) {
			rowData.add(new String[] { "", "", "", "", "" });
		}
		if (finalPeopleListNew != null && finalPeopleListNew.size() != 0) {
			for (Object[] obj : finalPeopleListNew) {
				// 审批阶段名称 姓名 部门 签名 日期
				String type = "";
				String name = "";
				String dept = "";
				String date = "";
				if (obj[1] != null) {
					type = obj[1].toString();
				}
				if (obj[2] != null) {
					name = obj[2].toString();
				}
				if (obj[3] != null) {
					dept = obj[3].toString();
				}
				if (obj[4] != null) {
					// 由于sqlserver时间 是2015-03-20 10:45:56.56 去掉后面的.56
					// 然后oracle 取出来是2015-03-20 没有时分秒
					date = obj[4].toString().replaceAll("\\.\\d+", "");
				}
				str = new String[] { type, name, dept, "", date };
				rowData.add(str);
				/******** 添加页脚数据 ************/
				// 部门审核阶段
				if ("2".equals(obj[0].toString())) {
					this.deptReviewName = name;
				}
				// 流程批准阶段
				if ("4".equals(obj[0].toString())) {
					this.allowedName = name;
				}
			}
		} else if (peopleList != null && peopleList.size() != 0) {
			for (Object[] obj : peopleList) {
				// 审批阶段名称 姓名 部门 签名 日期
				String type = "";
				String name = "";
				String dept = "";
				String date = "";
				if (obj[0] != null) {
					type = obj[0].toString();
				}
				if (obj[1] != null) {
					name = obj[1].toString();
				}
				if (obj[2] != null) {
					date = obj[2].toString();
				}
				str = new String[] { type, name, dept, "", date };
				rowData.add(str);
				/******** 添加页脚数据 ************/
				// 部门审核阶段
				if ("2".equals(obj[3].toString())) {
					this.deptReviewName = name;
				}
				// 流程批准阶段
				if ("4".equals(obj[3].toString())) {
					this.allowedName = name;
				}
			}
		}
		// 宋体 五号 居中
		Table tab = JecnWordUtil.createTab(titleSect, tabBean, new float[] {
				4.34F, 2.2F, 3.41F, 3.45F, 3.56F }, rowData, new ParagraphBean(
				Align.center, "宋体", Constants.wuhao, false));

		/******* 设置第一行样式 黑体五号 *********/
		pBean = new ParagraphBean(Align.center, "黑体", Constants.wuhao, true);
		// 设置行样式
		tab.setRowStyle(0, pBean);
		pBean = new ParagraphBean("黑体", Constants.wuhao, true);
		// 设置第一列样式
		tab.setCellStyle(0, pBean);
		/**** 设置行高和 垂直居中 ******/
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.getRow(i).setHeight(1);
			tab.setRowStyle(i, Valign.center);
		}
	}

	/**
	 * 判断是否存在此审批数据（依据为数据的stageMark一致,peopleId一致)
	 * 
	 * @param objects
	 * @param finalPeopleListNew
	 * @return true不存在 false 存在
	 */
	private boolean notExits(Object[] objects, List<Object[]> finalPeopleListNew) {
		if (finalPeopleListNew.size() == 0) {
			return true;
		}
		// 便利finalPeopleListNew
		for (Object[] finalObj : finalPeopleListNew) {
			// 如果传入对象的 （stageMark 、 peopleId） 和集合中某一个对象的（stageMark 、 peopleId）
			// 相等 表示存在返回false
			if (finalObj[0].toString().equals(objects[0].toString())
					&& finalObj[6].toString().equals(objects[6].toString())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		String flowName = processDownloadBean.getFlowName();
		String flowInputNum = processDownloadBean.getFlowInputNum();
		String flowVersion = processDownloadBean.getFlowVersion();
		List<String[]> rowData = new ArrayList<String[]>();
		/**** 设置表格样式 *****/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(1.5F);
		tabBean.setBorderInsideH(1);
		tabBean.setBorderInsideV(1);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		rowData.add(new String[] { "", "文件名称", flowName, "密级", "（商）秘密▲3年" });
		rowData.add(new String[] { "", "编号", flowInputNum, "版本", flowVersion });
		/**** 表格内容默认字体 黑体 五号 居中 *******/
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体",
				Constants.shidianwuhao, false);
		/***** 页眉 ********/
		Header hdr = sect.createHeader(type);
		Table table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 7.07F,
				1.13F, 4.71F, 1.16F, 3.28F }, rowData, pBean);
		// 第一个单元格图片数据
		Paragraph p = table.getRow(0).getCell(0).getParagraph(0);
		Image image = new Image(getDocProperty().getLogoImage());
		image.setSize(6.75F, 0.82F);
		p.appendGraphRun(image);
		table.getRow(0).getCell(0).setRowspan(2);
		table.setRowStyle(0, Valign.center);
		table.setRowStyle(1, Valign.center);
		table.getRow(0).setHeight(0.89F);
		table.getRow(1).setHeight(0.96F);
		hdr.createParagraph("");

	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 黑体 五号 居中
		ParagraphBean pBean = new ParagraphBean("黑体", Constants.wuhao, false);
		pBean.setAlign(Align.center);
		Footer ftr = sect.createFooter(type);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(1.5F);
		tabBean.setBorderInsideH(1);
		tabBean.setBorderInsideV(1);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		// /** 流程责任属性 0流程监护人 1流程责任人 2流程责任部门 */
		ProcessAttributeBean responFlow = processDownloadBean.getResponFlow();
		// 流程责任部门
		String orgName = responFlow.getDutyOrgName();
		// 流程责任人
		String dutyName = responFlow.getDutyUserName();
		// 流程审核人
		String deptReviewName = this.deptReviewName;
		// 批准人
		String allowedName = this.allowedName;
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "编制部门", orgName, "流程责任人", dutyName, "审核",
				deptReviewName, "批准", allowedName, "第" });
		Table table = JecnWordUtil.createTab(ftr, tabBean, new float[] { 2.3F,
				3F, 2.42F, 1.67F, 1.21F, 2, 1.25F, 1.5F, 2.84F }, rowData,
				pBean);
		Paragraph p = table.getRow(0).getCell(8).getParagraph(0);
		p.appendCurPageRun();
		p.appendTextRun("页共");
		p.appendTotalPagesRun();
		p.appendTextRun("页");
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = this.getSectBean();
		sectBean.setSize(29.7F, 21);
		sectBean.setHorizontal(true);
		flowChartSect.setSectBean(sectBean);
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = this.getSectBean();
		secondSect.setSectBean(sectBean);
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = this.getSectBean();
		titleSect.setSectBean(sectBean);
		// 添加封皮节点数据
		addTitleSectContent(titleSect);
		createCommhdr(titleSect, HeaderOrFooterType.first);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体",
				Constants.shierhao, false);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		/** 默认表格样式 ****/
		TableBean tblStyle = new TableBean();// 
		// 所有边框 1.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.01 厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(Align.center, "宋体",
				Constants.shiyihao, true);
		return tblTitleStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		/*** 初始化标题内容段落属性 ****/
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, "宋体",
				Constants.xiaosi, false);
		textContentStyle.setInd(0, 0, 0, 0.7F);
		textContentStyle.setSpace(0F, 0F, vLineFix23);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		/**** 默认标题样式 *****/
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "宋体",
				Constants.xiaosi, true);
		textTitleStyle.setSpace(0.5F, 0.5F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	/**
	 * 使用于活动段落标题
	 * 
	 * @return
	 */
	public ParagraphBean activityTitleStyle_() {
		/**** 默认标题样式 *****/
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "宋体",
				Constants.xiaosi, true);
		textTitleStyle.setSpace(0.5F, 0.5F, vLine1);
		return textTitleStyle;
	}
}
