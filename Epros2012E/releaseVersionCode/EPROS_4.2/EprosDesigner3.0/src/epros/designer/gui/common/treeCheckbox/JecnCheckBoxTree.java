package epros.designer.gui.common.treeCheckbox;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.TreeSelectionModel;

import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.draw.util.JecnUIUtil;

public abstract class JecnCheckBoxTree extends JTree {
	/** 节点类型 0是流程地图；1是流程；2是组织 */
	private int nodeType = -1;
	private Long id;
	/**勾选过节点集合*/
	private List<JecnTreeNode> listNodes = new ArrayList<JecnTreeNode>();

	public JecnCheckBoxTree() {

		this.addMouseListener(new CheckBoxTreeNodeSelectionListener(listNodes));
		this.setModel(getTreeModel());
		this.setCellRenderer(new CheckBoxTreeCellRenderer(this));
		this.addTreeExpansionListener(getTreeExpansionListener(this));
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
		}

		/** 添加鼠标事件 */
		this.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	public abstract JecnTreeModel getTreeModel();
	
	public abstract Long getTreeId();

	/**
	 * 
	 * @description：设置Tree是否多选
	 * @return
	 */
	public abstract boolean isSelectMutil();

	/**
	 * @description:添加鼠标事件
	 * @return
	 */
	public abstract void jTreeMousePressed(MouseEvent evt);
	
	public abstract JecnTreeListener getTreeExpansionListener(JecnCheckBoxTree checkTree);

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<JecnTreeNode> getListNodes() {
		return listNodes;
	}

	public void setListNodes(List<JecnTreeNode> listNodes) {
		this.listNodes = listNodes;
	}

}
