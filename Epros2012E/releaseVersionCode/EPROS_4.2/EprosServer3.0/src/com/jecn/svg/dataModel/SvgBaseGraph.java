package com.jecn.svg.dataModel;


public abstract class SvgBaseGraph {

	private long figureId;

	private String type;

	private String text;
	private SvgTextStyle textStyle;

	private SvgGraphStyle graphStyle;

	public String getType() {
		return type;
	}

	public SvgBaseGraph setType(String type) {
		this.type = type;
		return this;
	}

	public long getFigureId() {
		return figureId;
	}

	public SvgBaseGraph setFigureId(long figureId) {
		this.figureId = figureId;
		return this;
	}

	public SvgGraphStyle getGraphStyle() {
		return graphStyle;
	}

	public SvgBaseGraph setGraphStyle(SvgGraphStyle graphStyle) {
		this.graphStyle = graphStyle;
		return this;
	}

	public String getText() {
		return text;
	}

	public SvgBaseGraph setText(String text) {
		this.text = text;
		return this;
	}

	public SvgTextStyle getTextStyle() {
		return textStyle;
	}

	public SvgBaseGraph setTextStyle(SvgTextStyle textStyle) {
		this.textStyle = textStyle;
		return this;
	}

}
