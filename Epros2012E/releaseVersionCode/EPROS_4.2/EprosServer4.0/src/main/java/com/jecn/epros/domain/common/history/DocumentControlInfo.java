package com.jecn.epros.domain.common.history;

import java.util.List;

/**
 * 发布流程、制度版本信息记录
 *
 * @author ZHANGXIAOHU
 */
public class DocumentControlInfo {
    /**
     * 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
     */
    private DocumentControl documentControl;
    private List<DocumentControlFollow> controlFollows;

    public DocumentControl getDocumentControl() {
        return documentControl;
    }

    public void setDocumentControl(DocumentControl documentControl) {
        this.documentControl = documentControl;
    }

    public List<DocumentControlFollow> getControlFollows() {
        return controlFollows;
    }

    public void setControlFollows(List<DocumentControlFollow> controlFollows) {
        this.controlFollows = controlFollows;
    }

}
