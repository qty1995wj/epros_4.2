package com.jecn.epros.server.service.tree.data;

import java.util.SortedMap;
import java.util.TreeMap;

public class DirManager<T> {

	private final SortedMap<Integer, T> m = new TreeMap<Integer, T>();

	/**
	 * 
	 * 插入数据并且只保留小于key的数据
	 * 
	 * @param key
	 * @param value
	 */
	public void put(Integer key, T value) {
		SortedMap<Integer, T> temp = new TreeMap<Integer, T>(m.headMap(key));
		temp.put(key, value);
		m.clear();
		m.putAll(temp);
	}

	public T getParent(Integer key) {
		SortedMap<Integer, T> headMap = m.headMap(key);
		if (!headMap.isEmpty()) {
			Integer lastKey = headMap.lastKey();
			if (lastKey != null) {
				return headMap.get(lastKey);
			}
		}
		return null;
	}

	public static void main(String[] args) {
		DirManager<Integer> m = new DirManager<Integer>();
		m.put(1, 1);
		m.put(2, 2);
		m.put(3, 3);
		m.put(4, 4);
		m.put(5, 5);
		m.put(6, 6);
		System.out.println(m.getParent(8));
	}

}
