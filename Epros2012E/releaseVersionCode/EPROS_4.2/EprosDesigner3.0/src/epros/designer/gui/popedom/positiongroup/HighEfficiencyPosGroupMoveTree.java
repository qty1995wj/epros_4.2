package epros.designer.gui.popedom.positiongroup;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

/**
 * 获得岗位组树
 * @author 2012-05-18
 *
 */
public class HighEfficiencyPosGroupMoveTree extends JecnHighEfficiencyTree{
	
	private static Logger log = Logger.getLogger(HighEfficiencyPosGroupMoveTree.class);
	/** 当前项目根节点下的岗位组目录 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	
	public HighEfficiencyPosGroupMoveTree(List<Long> listIds){
		super(listIds);
	}
	
	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new PosGroupMoveTreeListener(this.getListIds(),this);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getPosGroup().getChildPositionGroupDirs(Long.valueOf(0), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighEfficiencyPosGroupMoveTree getTreeModel is error" ,e);
		}
		return PosGroupCommon.getTreeMoveModel(list, this.getListIds());
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
