function publicFileGrid() {

	publicFileGridStore = new Ext.data.Store({
				 autoLoad : {
					 params : {
					 start : 0,
					 limit : pageSize
					 }
				 },
				 baseParams:{
				 	type:0//默认流程
				 },
				proxy : new Ext.data.HttpProxy({
							url : "getPublicFileList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
//							id:"id"
							idProperty : "named" //TODO 指定一个错误的就好了。  
						}, [{
									name : "id",
									mapping : "id"
								}, {
									name : "name",
									mapping : "name"
								}, {
									name : "number",
									mapping : "number"
								}, {
									name : "type",
									mapping : "type"
								}, {
									name : "orgName",
									mapping : "orgName"
								}, {
									name : "relationType",
									mapping : "relationType"
								}, {
									name : "relationId",
									mapping : "relationId"
								},{
									name : "secretLevel",
									mapping : "secretLevel"
								}])

			});

	publicFileGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), 
			{
				header : i_name,
				dataIndex : "name",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:publicFileNameRenderer
			}, {
				header : i_number,
				dataIndex : "number",
				align : "center",
				menuDisabled : true
			}, {
				header : i_type,
				dataIndex : "type",
				align : "center",
				menuDisabled : true,
				renderer:publicFileTypeRenderer
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true,
				sortable : true
			},{ //新添加 密级  要指定dataIndex
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : false,
				sortable : true,
				renderer:publicFileSecretLevelRenderer
			}

	]);
	publicFileGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : publicFileGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	publicFileGridGridPanel = new Ext.grid.GridPanel({
				id : "publicFileGridGridPanel_id",
				renderTo : 'publicFileGrid_id',

//				height : Ext.getCmp("main-panel").getInnerHeight()-189, TODO 

				height : Ext.getCmp("main-panel").getInnerHeight()-173,

				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : publicFileGridStore,
				colModel : publicFileGridColumnModel,
				bbar : publicFileGridBbar
			})
	myhome_index_id = "myhome_publicFile_id";
}

function publicFileTypeRenderer(value, cellmeta, record){
	// 0是流程，1是流程地图，2是文件，3是标准文件，4是制度文件，5制度模板文件
	if(value==0){
		return i_procesChart;
	}else if(value==1){
		return i_processMap;
	}else if(value==2){
		return i_file;
	}else if(value==3){
		if(record.data['relationType']==1){
			return	i_standardFile
		}else if(record.data['relationType']==2){
			return i_procesChart;
		}else if(record.data['relationType']==3){
			return i_processMap;
		}
		
	}else if(value==4){
		return i_ruleFile;
	}else if(value==5){
		return i_ruleModeFile;
	}
}
function publicFileSecretLevelRenderer(value, cellmeta , record){
	//密级  0:秘密  1:公开
	if(value==0){
		return i_secret;	
	} else if (value ==1){
		return i_public;
	}
}

function publicFileGridSearch() {
	// 名称
	var publicName = document.getElementById("publicName_id").value;
	// 编号
	var publicNumber = document.getElementById("publicNumber_id").value;
	// 类型
	var publicType = document.getElementById("publicType_id").value;
	// 责任部门
	var publicDutyOrgName = document.getElementById("public_orgDutyName_id").value;
	// 责任部门ID
	var publicDutyOrgId = document.getElementById("public_orgDutyId_id").value;
	// 密级 -1:全部  0:密级  1:公开  
	var isPublic = document.getElementById("is_public").value;
	
	publicFileGridStore.setBaseParam("fileName", publicName);
	publicFileGridStore.setBaseParam("fileNum", publicNumber);
	publicFileGridStore.setBaseParam("type", publicType);
	publicFileGridStore.setBaseParam("orgName", publicDutyOrgName);
	publicFileGridStore.setBaseParam("orgId", publicDutyOrgId);
	publicFileGridStore.setBaseParam("secretLevel",isPublic);
	
	publicFileGridStore.load({
				params : {
					start : 0,
					limit : pageSize
				}
			});

}


/**
* 重置公共文件搜索条件
*/
function publicFileReset(){
    document.getElementById("publicName_id").value = "";
    document.getElementById("publicNumber_id").value = "";
    document.getElementById("publicType_id").options[0].selected=true;
    document.getElementById("public_orgDutyName_id").value = "";
    document.getElementById("public_orgDutyId_id").value=-1;
    // 密级
	document.getElementById("is_public").options[1].selected = true;
}

function publicFileNameRenderer(value, cellmeta, record){
	//类型 0是流程，1是流程地图，2是文件，3是标准文件，4是制度文件，5制度模板文件
	if(record.data['type']==0){
		return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['id']+"' >"+value+"</a>";
	}else if(record.data['type']==1){
		return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=processMap&flowId="+record.data['id']+"' >"+value+"</a>";
	}else if(record.data['type']==2){
		return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='downloadFile.action?fileId="+record.data['id']+"' >"+value+"</a>";
	}else if(record.data['type']==3){
		if(record.data['relationType']==1){
			return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='downloadFile.action?fileId="+record.data['relationId']+"' >"+value+"</a>";
		}else if(record.data['relationType']==2){
			return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['relationId']+"' >"+value+"</a>";
		}else if(record.data['relationType']==3){
			return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=processMap&flowId="+record.data['relationId']+"' >"+value+"</a>";
		}
	}else if(record.data['type']==4){
		return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='"+basePath+"rule.action?type=ruleFile&ruleId="+record.data['id']+"'>"+value+"</a>";
	}else if(record.data['type']==5){
		return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='"+basePath+"rule.action?type=ruleModeFile&ruleId="+record.data['id']+"'>"+value+"</a>";
	}else {
		return "<div  ext:qtip='"+value+"'>"+value+"</div>";
	}
	
}
