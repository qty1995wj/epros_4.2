package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.List;

/**
 * 流程地图RMI数据Bean
 * 
 * @author Administrator
 * @date： 日期：2013-11-6 时间：上午09:57:21
 */
public class ProcessMapData implements Serializable {
	/** 流程地图保存 */
	private JecnFlowStructureT jecnFlowStructureT;
	/** 添加元素集合 */
	private List<ProcessMapFigureData> saveFigureDataList;
	/** 修改元素集合 */
	private List<ProcessMapFigureData> updateFigureDataList;
	/** 删除线元素ID集合 */
	private List<String> deleteListLine;
	/** 删除元素ID集合 */
	private List<String> deleteList;

	// /** 流程地图元素附件集合 */
	// private List<JecnFigureFileTBean> listJecnFigureFileTBean;
	/** 当前画图数据是否保存标识： true：需要保存；false：不需要保存 (默认不保存) */
	private boolean isSave = false;

	public List<ProcessMapFigureData> getSaveFigureDataList() {
		return saveFigureDataList;
	}

	public void setSaveFigureDataList(List<ProcessMapFigureData> saveFigureDataList) {
		this.saveFigureDataList = saveFigureDataList;
	}

	public List<ProcessMapFigureData> getUpdateFigureDataList() {
		return updateFigureDataList;
	}

	public void setUpdateFigureDataList(List<ProcessMapFigureData> updateFigureDataList) {
		this.updateFigureDataList = updateFigureDataList;
	}

	public JecnFlowStructureT getJecnFlowStructureT() {
		return jecnFlowStructureT;
	}

	public void setJecnFlowStructureT(JecnFlowStructureT jecnFlowStructureT) {
		this.jecnFlowStructureT = jecnFlowStructureT;
	}

	public List<String> getDeleteListLine() {
		return deleteListLine;
	}

	public void setDeleteListLine(List<String> deleteListLine) {
		this.deleteListLine = deleteListLine;
	}

	public List<String> getDeleteList() {
		return deleteList;
	}

	public void setDeleteList(List<String> deleteList) {
		this.deleteList = deleteList;
	}

	public boolean isSave() {
		return isSave;
	}

	public void setSave(boolean isSave) {
		this.isSave = isSave;
	}
}
