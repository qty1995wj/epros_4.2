package com.jecn.epros.domain.process.processFile;

public class ProcessFileText extends BaseProcessFileBean {
    /**
     * 文本内容
     */
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
