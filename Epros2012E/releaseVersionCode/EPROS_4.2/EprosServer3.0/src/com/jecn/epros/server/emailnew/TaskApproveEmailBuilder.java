package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.email.buss.TaskEmailParams;
import com.jecn.epros.server.email.buss.ToolEmail;

public class TaskApproveEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

		Object[] data = getData();
		JecnTaskBeanNew taskBeanNew = (JecnTaskBeanNew) data[0];
		int toPeopleType = Integer.valueOf(data[1].toString());
		TaskEmailParams emailParams = (TaskEmailParams) data[2];
		return createEmail(user, taskBeanNew, toPeopleType, emailParams);
	}

	/**
	 * 任务审批过程中发送邮件
	 * 
	 * @param peopleId
	 *            审批或者拟稿人id
	 * @param taskBeanNew
	 *            任务主表
	 * @param toPeopleType
	 *            0、为审批人员 1、为拟稿人
	 */
	public EmailBasicInfo createEmail(JecnUser user, JecnTaskBeanNew taskBeanNew, int toPeopleType,
			TaskEmailParams emailParams) {

		String taskContent = "";
		// 邮件内容
		if (toPeopleType == 0) {
			taskContent = ToolEmail.getTaskDescNeedApprove(taskBeanNew, emailParams);
		} else {
			taskContent = ToolEmail.getTaskDesc(taskBeanNew, emailParams);
		}

		EmailContentBuilder builder = new EmailContentBuilder();
		builder.setContent(taskContent);
		builder.setResourceUrl(getUrl(toPeopleType, taskBeanNew.getId(), user.getPeopleId()));
		EmailBasicInfo basicInfo = new EmailBasicInfo();
		basicInfo.setSubject(getSubject(toPeopleType, taskBeanNew));
		basicInfo.setContent(builder.buildContent());
		basicInfo.addRecipients(user);

		return basicInfo;

	}

	private String getSubject(int toPeopleType, JecnTaskBeanNew taskBeanNew) {
		String subject = "";
		if (0 == toPeopleType) {
			// 审批人 邮件标题 任务xxx已提交，请审批！
			subject = ToolEmail.getTaskSubject(taskBeanNew);
		} else if (1 == toPeopleType) {
			// 拟稿人 邮件标题 xxx正在审批
			subject = ToolEmail.getTaskSubjectForPeopleMake(taskBeanNew);
		}
		return subject;
	}

	private String getUrl(int toPeopleType, long taskId, long toPeopleId) {

		// 获取任务审批 指定下一个操作人的连接地址
		String httpUrl = ToolEmail.getTaskHttpUrl(taskId, toPeopleId);
		return httpUrl;

	}

}
