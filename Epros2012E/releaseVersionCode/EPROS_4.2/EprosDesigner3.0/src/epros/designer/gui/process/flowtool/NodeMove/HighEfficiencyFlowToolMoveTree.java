package epros.designer.gui.process.flowtool.NodeMove;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.process.flowtool.JecnFlowToolCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class HighEfficiencyFlowToolMoveTree extends JecnHighEfficiencyTree {

	private Logger log = Logger.getLogger(HighEfficiencyFlowToolMoveTree.class);
	/** 当前项目根节点下的角色目录 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	public HighEfficiencyFlowToolMoveTree(List<Long> listIds){
		super(listIds);
	}
	
	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new FlowToolMoveTreeListener(this.getListIds(),jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFlowTool().getChildSustainTools(
					Long.valueOf(0));
		} catch (Exception e) {
			log.error("HighEfficiencyFlowToolMoveTree getTreeModel is error", e);
		}
		return JecnFlowToolCommon.getFlowToolMoveTreeModel(list,this.getListIds());
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}
	

}
