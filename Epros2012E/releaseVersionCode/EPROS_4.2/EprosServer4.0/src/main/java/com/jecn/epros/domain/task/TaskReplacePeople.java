package com.jecn.epros.domain.task;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * 人员替换
 */
public class TaskReplacePeople {

    private Long from;
    private Long to;

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }
}
