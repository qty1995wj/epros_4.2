package epros.draw.gui.figure.unit;

import java.awt.Point;

import epros.draw.util.DrawCommon;

/**
 * 网格设置
 * 
 * @author ZHANGXH
 * @date： 日期：May 8, 2012 时间：10:13:30 AM
 */
public class SnapToGrid {
	/** 默认网格宽度为10 */
	private static double gridWidth = 10;
	/** 默认网格高度为10 */
	private static double gridHeight = 10;

	/** Creates a new instance of SnapToGrid */
	public SnapToGrid() {
	}

	/**
	 * 
	 * 输入点网格处理
	 * 
	 * @param inputPoint输入目标点
	 * @param resultPoint
	 *            返回网格处理后的点
	 */
	public static void snapPoint(Point inputPoint, Point resultPoint) {
		resultPoint.x = DrawCommon
				.convertDoubleToInt(gridWidth
						* Math
								.floor(((inputPoint.getX() + gridWidth / 2.0) / gridWidth)));
		resultPoint.y = DrawCommon
				.convertDoubleToInt(gridHeight
						* Math
								.floor(((inputPoint.getY() + gridHeight / 2.0) / gridHeight)));
	}

	/**
	 * 
	 * 如果存在网格，按网格设置大小
	 * 
	 * @param figure
	 * @param x
	 * @param y
	 */
	public static Point checkIsGrid(int x, int y) {
		Point newPoint = new Point();
		// 输入点网格处理
		snapPoint(new Point(x, y), newPoint);
		return newPoint;
	}
}
