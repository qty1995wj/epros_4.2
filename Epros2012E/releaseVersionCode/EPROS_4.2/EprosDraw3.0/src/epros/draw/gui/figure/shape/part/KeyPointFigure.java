package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;

/**
 * 
 * 关键控制点
 * 
 * @author ZHOUXY
 * 
 */
public class KeyPointFigure extends Triangle {
	public KeyPointFigure(JecnFigureData flowElementData) {
		super(flowElementData);
	}
}
