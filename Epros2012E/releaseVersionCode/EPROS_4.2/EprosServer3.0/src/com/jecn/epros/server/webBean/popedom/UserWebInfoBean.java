package com.jecn.epros.server.webBean.popedom;

import java.util.List;

/**
 * @author yxw 2013-3-6
 * @description：人员详情
 */
public class UserWebInfoBean {
	/**主键ID*/
	private Long userId;
	/**登录名*/
	private String loginName;
	/**真实姓名*/
	private String trueName;
	/**岗位*/
	private List<String> listPos;
	/**部门*/
	private List<String> listOrg;
	/**角色*/
	private List<String> listRole;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public List<String> getListPos() {
		return listPos;
	}
	public void setListPos(List<String> listPos) {
		this.listPos = listPos;
	}
	public List<String> getListOrg() {
		return listOrg;
	}
	public void setListOrg(List<String> listOrg) {
		this.listOrg = listOrg;
	}
	public List<String> getListRole() {
		return listRole;
	}
	public void setListRole(List<String> listRole) {
		this.listRole = listRole;
	}
}
