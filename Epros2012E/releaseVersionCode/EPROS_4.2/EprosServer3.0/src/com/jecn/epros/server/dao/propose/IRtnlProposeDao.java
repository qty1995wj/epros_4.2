package com.jecn.epros.server.dao.propose;

import java.util.List;

import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 合理化建议Dao 操作类 2013-09-02
 * 
 */
public interface IRtnlProposeDao extends IBaseDao<JecnRtnlPropose, String> {

	public int getProposeDetailNum(String peopleIds, String orgIds,
			String startTime, String endTime) throws Exception;
	
	public List<Object[]> getProposeDetailList(String peopleIds, String orgIds,
			String startTime, String endTime, int start, int limit) throws Exception;

	public List<Object[]> getProposeOrgCountList(String orgIds,
			String startTime, String endTime, int start, int limit) throws Exception;

	public List<Object[]> getProposeCreateCountList(String personIds,
			String startTime, String endTime, int start, int limit) throws Exception;

}
