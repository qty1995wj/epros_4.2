package com.jecn.epros.server.bean.task;

import java.util.Locale;

/**
 * 创建任务时，从任务配置审批配置中提取的各阶段
 * 
 * @author Administrator
 * 
 */
public class JecnTaskStage implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 主键Id */
	private Long id;
	/** 任务Id */
	private Long taskId;
	/**
	 * 任务审批阶段标识 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 6：各业务体系审批阶段 7：IT总监审批阶段
	 * 8:事业部经理 9：总经理 10:整理意见;11-14 自定义审批阶段
	 */
	private Integer stageMark;
	/** 任务各阶段名称 */
	private String stageName;
	/** 任务审批顺序 */
	private Integer sort;
	/** 是否显示 0：不显示 1:显示 */
	private Integer isShow;
	/** 是否必填 0：非必填 1:必填 */
	private Integer isEmpty;
	/** 是否已选择人 0:未选择人 1：已选择人 */
	private Integer isSelectedUser;

	private String enName;

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Integer getStageMark() {
		return stageMark;
	}

	public void setStageMark(Integer stageMark) {
		this.stageMark = stageMark;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public String getStageName(int  local) {
		return local == 0 ? stageName : enName;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public Integer getIsEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(Integer isEmpty) {
		this.isEmpty = isEmpty;
	}

	public Integer getIsSelectedUser() {
		return isSelectedUser;
	}

	public void setIsSelectedUser(Integer isSelectedUser) {
		this.isSelectedUser = isSelectedUser;
	}
}
