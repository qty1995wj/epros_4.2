package com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit;

public class SyncErrorInfo {
	/** 当前人员同步的项目和上次人员同步的项目不一致，请切换到上次人员同步的项目下后再执行人员同步 */
	public static final String PROJECT_ID_NOT_SAME_FAIL = "当前人员同步的项目和上次人员同步的项目不一致，请切换到上次人员同步的项目下后再执行人员同步";
	/** 部门编号为空 */
	public static final String DEPT_NUM_NULL = "部门编号为空";
	/** 部门名称为空 */
	public static final String DEPT_NAME_NULL = "部门名称为空";
	/** 上级部门编号为空 */
	public static final String P_DEPT_NUM_NULL = "上级部门编号为空";
	/** 上级部门编号为0的数据有且只能出现一次 */
	public static final String P_DEPT_NUM_0S = "上级部门编号为[0]的数据有且只能出现一次";
	/** 上级部门编号没有一条[0]的数据 */
	public static final String P_DEPT_NUM_0 = "上级部门编号没有一条为[0]的数据";
	/** 部门对应的父部门编号出现（A->B->C->A）形式的循环 */
	public static final String P_DEPT_NUM_DEAD_LOCK = "部门对应的父部门编号出现（A->B->C->A）形式的循环";
	/** 该部门的父部门编号无效，没有找到给定编号的部门 */
	public static final String P_DEPT_NUM_OTHER = "该部门的父部门编号无效，没有找到给定编号的部门";
	/** 部门编号不唯一 */
	public static final String DEPT_NUM_NOT_ONLY = "部门编号不唯一";
	/** 同一部门下部门名称不能重复 */
	public static final String DEPT_NAME_NOT_ONLY_BY_PDEPT = "同一部门下部门名称不能重复";

	/** 岗位编号为空 */
	public static final String POS_NUM_NULL = "岗位编号为空";
	/** 岗位名称为空 */
	public static final String POS_NAME_NULL = "岗位名称为空";
	/** 岗位所属部门编号为空 */
	public static final String POS_DEPT_NUM_NULL = "岗位所属部门编号为空";
	/** 岗位所属部门编号在部门集合中不存在 */
	public static final String POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST = "岗位所属部门编号在部门集合中不存在";
	/** 岗位编号不唯一 */
	public static final String POS_NUM_NOT_ONLY = "岗位编号不唯一";
	/** 同一部门下岗位名称不能重复 */
	public static final String POS_NAME_NOT_ONLY = "同一部门下岗位名称不能重复";
	/** 登录名称为空 */
	public static final String USER_NUM_NULL = " 登录名称为空";
	/** 登录名称不能为[admin] */
	public static final String USER_NUM_AMIN = "登录名称不能为[admin]";
	/** 真实姓名为空 */
	public static final String TRUE_NAME_NULL = " 真实姓名为空";
	/** 给定的人员对应的岗位编号在岗位集合中不存在 */
	public static final String USER_POS_NUM_NOT_EXISTS_POS_LIST = "给定的人员对应的岗位编号在岗位集合中不存在";
	/** 邮箱格式不正确 */
	public static final String EMAIL_FORMAT_FAIL = " 真邮箱格式不正确";
	/** 内外网邮件标识不正确(0、1) */
	public static final String EMAIL_TYPE_FAIL = " 内外网邮件标识不正确(0、1)";
	/** 出现多条登录名称和岗位编号相同的数据 */
	public static final String USER_DATA_NOT_ONLY = "出现多条登录名称和岗位编号相同的数据";
	/** 一人多岗情况，人员对应的真实姓名必须相同 */
	public static final String USER_TRUE_NAME_NOT_SAME = "一人多岗情况，人员对应的真实姓名必须相同";
	/** 一人多岗情况，人员对应的邮箱、邮箱类型必须相同 */
	public static final String USER_EMAIL_NOT_SAME = "一人多岗情况，人员对应的邮箱、邮箱类型必须相同";
	/** 一人多岗情况，人员对应的电话必须相同 */
	public static final String USER_PHONE_NOT_SAME = "一人多岗情况，人员对应的电话必须相同";
	/** 一人多岗情况，人员对应的岗位编号不能为空 */
	public static final String USER_ONE_POS_MUTIL_NULL = "一人多岗情况，人员对应的岗位编号不能为空";
	/** 一人多岗情况下：真实姓名、邮箱地址、邮箱内外网标识、电话都必须相同 */
	public static final String USER_ONE_USER_OTHER_SAME = "一人多岗情况下：真实姓名、邮箱地址、邮箱内外网标识、电话都必须相同";

	/** 同一个人员不同岗位情况，岗位编号不能为空 */
	public static final String USER_NUM_MUTIL_POS_NUM_NOT_NULL = "同一个人员不同岗位情况，岗位编号不能为空";

	/** 导入方式不是excel、xml、hessian中其中一个 */
	public static final String IMPORT_TYPE_ERROR = "导入方式不是excel、xml、hessian中其中一个";
	/** 岗位编号不为空时，岗位名称或岗位所属部门编号不能为空 */
	public static final String POS_NAME_NULL_POS_NUM_NOT_NULL = "岗位编号不为空时，岗位名称或岗位所属部门编号不能为空";
	/** 岗位编号为空时，岗位名称和岗位所属部门编号都为空 */
	public static final String POS_ATTRIBUTES_NULL = "岗位编号为空时，岗位名称和岗位所属部门编号都为空";
	/** 有两条登录名称、岗位编号都相同数据 */
	public static final String TWO_SANME_USER_DATA = "有两条登录名称、任职岗位编号都相同数据";
	/** 人员数据所有字段都为空 */
	public static final String USER_ALL_NULL = "人员数据所有字段都为空";
	/** **********************返回页面信息 开始*********************** */
	/** 获取配置文件数据失败 */
	public static final String GET_CONFIG_FILE_FAIL = "获取配置文件数据失败";

	/** 部门、岗位以及人员数据同步失败 */
	public static final String IMPORT_JSP_FAIL = "部门、岗位以及人员数据同步失败";
	/** 部门、岗位以及人员数据同步完成 */
	public static final String IMPORT_JSP_SUCCESS = "部门、岗位以及人员数据同步完成";
	/** 请选择标准的Excel文件! */
	public static final String IMPORT_JSP_EXCEL_NAME_NOT_XLS = "请选择标准的Excel文件!";
	/** 上传文件为空 */
	public static final String IMPORT_FILE_NULL = "上传文件为空";
	/** 上传文件成功保存到服务器本地 */
	public static final String IMPORT_FILE_SAVE_LOCAL_SUCCESS = "上传文件成功保存到服务器本地";
	/** 上传文件保存到服务器本地失败 */
	public static final String IMPORT_FILE_SAVE_LOCAL_FAIL = "上传文件保存到服务器本地失败";
	/** 文件保存到服务器本地失败 */
	public static final String FILE_SAVE_LOCAL_FAIL = "文件保存到服务器本地失败";
	/** 上传excel格式不正确，请参照模板! */
	public static final String IMPORT_FILE_FORMAT_VAIL = "上传excel格式不正确，请参照模板!";
	/** 读取本地Excel文件失败! */
	public static final String READ_LOCAL_EXCEL_FILE_FAIL = "读取本地Excel文件失败";
	/** 下载失败! */
	public static final String FILE_DOWN_FAIL = "下载失败!";
	/** 人员导入 变更数据不存在 */
	public static final String PERSONNEL_CHANGES_DOWN_FAIL = "人员导入 变更数据不存在!";
	/** 文件不存在 */
	public static final String FILE_NOT_EXISTS = "文件不存在";
	/** 没有人员错误信息Excel文件 */
	public static final String ERROR_FILE_NOT_EXISTS = "没有人员错误信息";
	/** 人员数据导出失败 */
	public static final String USER_FILE_DOWN_FAIL = "人员数据导出失败";
	/** 读取数据库失败 */
	public static final String SELECT_DB_FAIL = "读取数据库失败";

	/** 系统异常 */
	public static final String SYSTEM_ERRROR = "系统异常";

	/** 同步开始时间格式不正确 */
	public static final String START_TIME_FORMAT_FAIL = "同步开始时间格式不正确";
	/** 同步时间间最少为1天，且只能正整数 */
	public static final String INTERVAL_DAY_FORMAT_FAIL = "同步时间间最少为1天，且只能正整数";
	/** 同步方式不对 */
	public static final String AUTO_FORMAT_FAIL = "同步方式不对,请选择手动或自动";

	/** 保存配置数据出错 */
	public static final String SAVE_CONFIG_XML_FAIL = "保存配置数据出错";

	/** 部门名称由中文、英文、数字及“_”、“-”组成 */
	public static final String DEPT_NAME_SPECIAL_CHAR = "部门名称由中文、英文、数字及“_”、“-”组成";
	/** 岗位名称由中文、英文、数字及“_”、“-”组成 */
	public static final String POS_NAME_SPECIAL_CHAR = "岗位名称由中文、英文、数字及“_”、“-”组成";
	/** 真实姓名由中文、英文、数字及“_”、“-”组成 */
	public static final String TRUE_NUME_SPECIAL_CHAR = "真实姓名由中文、英文、数字及“_”、“-”组成";

	/** 部门编号不能超过122个字符 */
	public static final String DEPT_NUM_LENGTH_ERROR = "部门编号不能超过122个字符";
	/** 部门名称不能超过122个字符 */
	public static final String DEPT_NAME_LENGTH_ERROR = "部门名称不能超过122个字符";
	/** 上级部门编号不能超过122个字符 */
	public static final String P_DEPT_NUM_LENGTH_ERROR = "上级部门编号不能超过122个字符";

	/** 人员编号(登录名称)不能超过122个字符 */
	public static final String LOGIN_NAME_LENGTH_ERROR = "登录名称不能超过122个字符";
	/** 真实姓名不能超过122个字符 */
	public static final String TRUE_NAME_LENGTH_ERROR = "真实姓名不能超过122个字符";
	/** 任职岗位编号不能超过122个字符 */
	public static final String POS_NUM_LENGTH_ERROR = "任职岗位编号不能超过122个字符";
	/** 任职岗位名称不能超过122个字符 */
	public static final String POS_NAME_LENGTH_ERROR = "任职岗位名称不能超过255个字符";
	/** 岗位所属部门编号不能超过122个字符 */
	public static final String POS_DEPT_NUM_LENGTH_ERROR = "岗位所属部门编号不能超过122个字符";
	/** 邮箱地址不能超过122个字符 */
	public static final String EMAIL_LENGTH_ERROR = "邮箱地址不能超过122个字符";
	/** 联系电话不能超过122个字符 */
	public static final String PHONE_LENGTH_ERROR = "联系电话不能超过122个字符";
	/** "人员导入成功！" */
	public static final String IMPORT_SUCCESS = "人员导入成功！";
	/** "人员导入失败！" */
	public static final String IMPORT_ERROR = "人员导入失败！";
	/** 岗位编号相同，岗位名称必须相同 */
	public static final String POS_NAME_NOT_SAME = "岗位编号相同，岗位名称必须相同";
	/** 岗位编号相同，岗位所属部门必须相同 */
	public static final String POS_NUM_SAME_DEPT_SAME = "岗位编号相同，岗位所属部门必须相同";
	/** **********************返回页面信息 结束*********************** */
	
	/*****************************基准岗位同步**********************************/
	
	/**基准岗位编号不能为空***/
	public static final String BASE_POSNUM_NOTNULL = "基准岗位编号不能为空";
	/**基准岗位名称不能够为空****/
	public static final String BASE_POSNAME_NOTNULL = "基准岗位名称不能够为空";
	/**基准岗位编号不能重复******/
	public static final String BASE_POSNUM_NOTREPET = "基准岗位编号不能够重复";
	/**基准岗位下的所属岗位在实际岗位中不存在**/
	public static final String BASEPOS_OWN_NOTEXIT = "基准岗位下的所属岗位在实际岗位中不存在";
	/**岗位对应的基准岗位名称为空***/
	public static final String BASEPOS_NAME_NOTNULL = "岗位对应基准岗位名称不能为空";
	/**岗位对应的基准岗位编号不能为空*/
	public static final String BASEPOS_NUM_NOTNULL = "岗位对应基准岗位编号不能为空";
	
	/**********************************************************************/
	

	/** 人员同步人数超出上限！ */
	public static final String USER_IMPORT_CAPS = "人员超出上限，上限个数为";
}
