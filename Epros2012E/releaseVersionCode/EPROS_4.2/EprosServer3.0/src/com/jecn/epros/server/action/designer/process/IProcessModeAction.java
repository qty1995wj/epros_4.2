package com.jecn.epros.server.action.designer.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessData;
import com.jecn.epros.server.bean.process.ProcessMapData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

/*******************************************************************************
 * 流程模板管理
 * 
 * @author 2012-06-26
 * 
 */
public interface IProcessModeAction {

	// /**
	// * 获得所有的流程地图模板
	// * @return
	// * @throws Exception
	// */
	// public List<JecnTreeBean> getFlowMapModels() throws Exception;

	/**
	 * 获得所有的流程图模板(流程图，流程地图)
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllFlowMapModels() throws Exception;

	/**
	 * 根据父ID 查询所有子节点
	 * 
	 * @param flowMapModelId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getFlowModels(Long flowMapModelId) throws Exception;

	/**
	 * 新建流程模板管理
	 * 
	 * @param jecnFlowStructureMode
	 * @param isXorY
	 *            0横向图，1 纵向图
	 * @return
	 * @throws Exception
	 */
	public Long addFlowModel(JecnFlowStructureT jecnFlowStructureT, int isXorY) throws Exception;

	/**
	 * 重命名
	 * 
	 * @param newName
	 * @param id
	 * @param updatePersonId
	 * @throws Exception
	 */

	public void reName(String newName, Long id, Long updatePersonId) throws Exception;

	/**
	 * 搜索
	 * 
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String name, Long projectId, int type) throws Exception;

	/**
	 * 节点排序
	 * 
	 * @param list
	 * @param pId
	 * @param projectId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId) throws Exception;

	/**
	 * 节点移动
	 * 
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodes(List<Long> listIds, Long pId) throws Exception;

	/**
	 * 删除
	 * 
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteFlowModel(List<Long> listIds) throws Exception;

	/**
	 * 根据父Id获取该父节点对应的父节点
	 * 
	 * @param perId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowStructureT getFlowMapPerInfo(Long perId) throws Exception;

	/**
	 * @author yxw 2012-7-24
	 * @description:打开流程地图模板
	 * @param id
	 *            流程 地图模板ID
	 * @return
	 */
	public ProcessOpenMapData getProcessMapModeData(long id) throws Exception;

	/**
	 * @author yxw 2012-7-24
	 * @description:打开流程图模板
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public ProcessOpenData getProcessModeData(long id) throws Exception;

	/**
	 * @author yxw 2012-7-31
	 * @description:流程地图保存
	 * @param jecnFlowStructureT
	 * @param saveList
	 * @param updateList
	 * @param deleteListLine
	 * @param deleteList
	 * @param flowImgeBytes
	 *            生成流程图
	 * @throws Exception
	 */
	public void saveProcessMapMode(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-7-31
	 * @description:流程图保存
	 * @param processData
	 * @param flowImgeBytes
	 *            生成流程图
	 * @throws Exception
	 */
	public void saveProcessMode(ProcessData processData, byte[] flowImgeBytes, Long updatePersionId) throws Exception;

	ProcessOpenMapData getProcessMapRelatedData(long id, long projectId) throws Exception;

	void saveMapRelatedProcess(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePeopleId) throws Exception;
}
