package epros.designer.gui.recycle;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
/**
 * 文件回收站树展开监听事件类
 * @author user
 *
 */
public class FileRecycleTreeListener extends RecycleTreeListener {

	public FileRecycleTreeListener(JecnHighEfficiencyTree jTree) {
		super(jTree);
	}


	@Override
	protected List<JecnTreeBean> getTreeExpandedData(JecnTreeNode node)
			throws Exception {
		return ConnectionPool.getFileAction().getRecycleJecnTreeBeanList(
				JecnConstants.projectId, node.getJecnTreeBean().getId());
	}

}
