package com.jecn.epros.service.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.StringUtil;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.domain.*;
import com.jecn.epros.domain.org.OrgBaseInfoBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.ProcessBaseInfoBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.system.*;
import com.jecn.epros.mapper.SystemManageMapper;
import com.jecn.epros.mapper.UserMapper;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.security.DataAccessAuthorityBean;
import com.jecn.epros.service.IMixRPCService;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.system.SystemService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.service.web.JecnIflytekSyncUserEnum;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.IDUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Service
@Transactional
public class SystemServiceImpl implements SystemService {
    private static final Logger logger = LoggerFactory.getLogger(SystemServiceImpl.class);
    @Autowired
    private SystemManageMapper systemManageMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IMixRPCService mixRPCService;


    @Override
    public int getTotalUser(UserWebSearchBean userWebSearchBean) {

        return systemManageMapper.getUserManageUserCount(userWebSearchBean);
    }

    @Override
    public List<UserWebInfoBean> fetchUserInfos(UserWebSearchBean userWebSearchBean, Paging paging) {
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "ju.people_id");
        List<JecnUser> users = systemManageMapper.fetchUsers(userWebSearchBean);

        List<UserWebInfoBean> listUserWebInfoBean = new ArrayList<UserWebInfoBean>();
        if (users == null) {
            return listUserWebInfoBean;
        }
        Set<Long> setIds = new HashSet<Long>();
        IfytekCommon.ifytekCommonGetPosUserName(users);
        for (JecnUser user : users) {
            if (user == null) {
                continue;
            }
            setIds.add(user.getId());
            UserWebInfoBean userWebInfoBean = new UserWebInfoBean();
            userWebInfoBean.setUserId(user.getId());
            userWebInfoBean.setLoginName(user.getLoginName());
            //科大讯飞 需要用 登录名获取 真实姓名
            userWebInfoBean.setTrueName(user.getName());
            listUserWebInfoBean.add(userWebInfoBean);
        }
        if (setIds.size() == 0) {
            return listUserWebInfoBean;
        }

        // 部门岗位
        String ids = SqlCommon.getIdsSet(setIds);
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("ids", ids);
        // SELECT JFOI.FIGURE_ID , JFOI.FIGURE_TEXT ,
        // JFO.ORG_ID ,JFO.ORG_NAME ,JUPR.PEOPLE_ID
        List<Map<String, Object>> posList = systemManageMapper.fetchPeoplePos(paramMap);

        // 角色
        // select jri.role_id as roleId,jri.role_name as roleName,jur.people_id
        // as peopleId
        List<Map<String, Object>> roleList = systemManageMapper.fetchPeopleRole(paramMap);

        for (UserWebInfoBean userWebInfoBean : listUserWebInfoBean) {
            // 岗位
            List<String> listPosStr = new ArrayList<String>();
            // 部门
            List<String> listOrgStr = new ArrayList<String>();
            // 角色
            List<String> listRoleStr = new ArrayList<String>();
            for (Map<String, Object> posMap : posList) {
                if (posMap.get("FIGURE_ID") == null || posMap.get("ORG_ID") == null) {
                    continue;
                }
                if (userWebInfoBean.getUserId().toString().equals(posMap.get("PEOPLE_ID").toString())) {
                    listPosStr.add(posMap.get("FIGURE_TEXT").toString());
                    listOrgStr.add(posMap.get("ORG_NAME").toString());
                }
            }
            for (Map<String, Object> roleMap : roleList) {
                if (roleMap.get("PEOPLE_ID") == null || roleMap.get("ROLE_ID") == null) {
                    continue;
                }
                if (userWebInfoBean.getUserId().toString().equals(roleMap.get("PEOPLE_ID").toString())) {
                    listRoleStr.add(roleMap.get("ROLE_NAME").toString());
                }
            }
            userWebInfoBean.setListOrg(listOrgStr);
            userWebInfoBean.setListPos(listPosStr);
            userWebInfoBean.setListRole(listRoleStr);
        }

        return listUserWebInfoBean;
    }

    @Override
    public List<RoleWebInfoBean> fetchUserRoles(String roleName) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("roleName", roleName);
        List<Map<String, Object>> roleInfos = systemManageMapper.fetchUserRoles(paramMap);
        List<RoleWebInfoBean> infos = new ArrayList<RoleWebInfoBean>();
        for (Map<String, Object> infoMap : roleInfos) {
            RoleWebInfoBean infoWebBean = new RoleWebInfoBean();
            infoWebBean.setRoleId(Long.valueOf(infoMap.get("ROLE_ID").toString()));
            if (infoMap.get("ROLE_NAME") != null) {
                infoWebBean.setRoleName(infoMap.get("ROLE_NAME").toString());
            }
            if (infoMap.get("ROLE_REMARK") != null) {
                infoWebBean.setRoleRemark(infoMap.get("ROLE_REMARK").toString());
            }

            infos.add(infoWebBean);
        }
        return infos;
    }

    @Override
    public RoleManageBean fetchUserRoleInfos(Map<String, Object> paramMap, Paging paging) {
        //获取 角色关联人员ID
        RoleManageBean managerBean = new RoleManageBean();
        boolean isDownLoad = (boolean) paramMap.get("isDownLoad");
        if (!isDownLoad) {
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), " PEOPLE_ID");
            List<String> peopleIds = systemManageMapper.getUserRoleInfo(paramMap);
            paramMap.put("peopleIds", peopleIds);
        }
        // 查询 权限范围数据
        List<RoleInfoBean> roleInfos = systemManageMapper.fetchUserRoleInfos(paramMap);
        IfytekCommon.getIflytekhRoleUserName(roleInfos);
        //查询总条数
        int roleTotal = systemManageMapper.getTotalUserRoleInfo(paramMap);
        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setStartLevel(1);
        inventory.setTotalNum(roleTotal);
        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.getRoleUserInfoTitles()));
        inventory.setStyleMap(InventoryStyleManager.getUserRoleInventoryStyles(data.getTitileContents()));
        InventoryCell cell = null;

        for (RoleInfoBean roleBean : roleInfos) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();
            data.getContents().add(content);
            //名称
            cell = new InventoryCell();
            List<LinkResource> userList = new ArrayList<LinkResource>();
            userList.add(roleBean.getUserLink());
            cell.setLinks(userList);
            content.add(cell);
            //部门
            cell = new InventoryCell();
            List<LinkResource> posList = new ArrayList<LinkResource>();
            for (OrgBaseInfoBean orgBean : roleBean.getOrgBean()) {
                posList.add(orgBean.getOrgLink(TreeNodeUtils.NodeType.auth));
            }
            cell.setLinks(posList);
            content.add(cell);
            //人员默认系统角色
            StringBuilder strName = new StringBuilder();
            for (RoleWebInfoBean bean : roleBean.getRoleInfo()) {
                if (bean.getRoleType() != null) {
                    switch (bean.getRoleType()) {
                        case "admin":
                            strName.append(JecnProperties.getValue("manager"));
                            strName.append(isDownLoad ? "\n" : "<br>");
                            break;
                        case "design":
                            strName.append(JecnProperties.getValue("designManager"));
                            strName.append(isDownLoad ? "\n" : "<br>");
                            break;
                        case "secondAdmin":
                            strName.append(JecnProperties.getValue("secondAdmin"));
                            strName.append(isDownLoad ? "\n" : "<br>");
                            break;
                        case "isDesignFileAdmin":
                            strName.append(JecnProperties.getValue("fileManagers"));
                            strName.append(isDownLoad ? "\n" : "<br>");
                            break;
                        case "isDesignRuleAdmin":
                            strName.append(JecnProperties.getValue("ruleAdmins"));
                            strName.append(isDownLoad ? "\n" : "<br>");
                            break;
                        case "isDesignProcessAdmin":
                            strName.append(JecnProperties.getValue("traCoordinates"));
                            strName.append(isDownLoad ? "\n" : "<br>");
                            break;
                        case "viewAdmin":
                            strName.append(JecnProperties.getValue("browseAdmin"));
                            strName.append(isDownLoad ? "\n" : "<br>");
                            break;
                        default:
                            break;
                    }
                }
            }
            cell = new InventoryCell();
            cell.setText(strName.toString());
            content.add(cell);
            // 写出自定义角色
            if (roleBean.getRoleInfo() != null && roleBean.getRoleInfo().size() > 0) {
                addRoleRange(roleBean, content, isDownLoad);
            }
        }
        managerBean.setTotal(roleTotal);
        managerBean.setRoleData(inventory);
        return managerBean;
    }


    private void fillEmptyCell(int orgFillNum, List<InventoryCell> content, InventoryTable data) {
        InventoryCell cell = null;
        for (int i = 0; i < orgFillNum; i++) {
            cell = new InventoryCell();
            cell.setText("");
            content.add(cell);
        }

    }

    @Override
    public int getTotalRole(String roleName) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("roleName", roleName);
        int total = systemManageMapper.getTotalRole(paramMap);
        return total;
    }

    @Override
    public int getTotalViewRole(String roleName) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("roleName", roleName);
        int total = systemManageMapper.getTotalViewRole(paramMap);
        return total;
    }

    @Override
    public List<ViewRoleDetailBean> fetchViewRoles(String roleName, Paging paging) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("roleName", roleName);
        // select jri.role_id as roleId,jri.role_name as
        // roleName,jri.role_remark as roleRemark
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "rp.UPDATE_TIME desc");
        List<Map<String, Object>> roleInfos = systemManageMapper.fetchViewRoles(paramMap);
        List<ViewRoleDetailBean> infos = new ArrayList<ViewRoleDetailBean>();
        for (Map<String, Object> infoMap : roleInfos) {
            ViewRoleDetailBean infoWebBean = new ViewRoleDetailBean();
            infoWebBean.setId(infoMap.get("ID").toString());
            if (infoMap.get("NAME") != null) {
                infoWebBean.setRoleName(infoMap.get("NAME").toString());
            }
            if (infoMap.get("ROLETYPE") != null) {
                infoWebBean.setRoleType(infoMap.get("ROLETYPE").toString());
            }

            infos.add(infoWebBean);
        }
        return infos;
    }

    @Override
    public List<TreeNode> getViewRoleAuthList(String roleId) {
        List<Map<String, Object>> treelist = systemManageMapper.viewRoleAuthList(roleId);
        Map<String, Object> leaf = new HashMap<>();
        Map<String, Object> m = new HashMap<>();
        List<TreeNode> nodes = new ArrayList<>();
        TreeNode node;
        for (Map<String, Object> map : treelist) { //查找根节点
            if (map.get("ISLEAF").equals("false")) {
                node = new TreeNode(
                        map.get("ID").toString(),
                        (AuthenticatedUserUtil.isLanguageType() == 0 ? map.get("TEXT").toString() : map.get("ENTEXT").toString()),
                        map.get("ISLEAF").toString().equals("true"),
                        TreeNodeUtils.NodeType.auth,
                        0,
                        null,
                        null,
                        map.get("SELECTED").toString().equals("true"),
                        map.get("ISCHECKED").toString().equals("true")
                );
                findTree(node, nodes, treelist);
                nodes.add(node);
            } else {
                continue;
            }
        }
        return nodes;
    }

    private void findTree(TreeNode node, List<TreeNode> nodes, List<Map<String, Object>> treelist) {
        List<TreeNode> list = new ArrayList<TreeNode>();
        TreeNode nodeChildren;
        for (Map<String, Object> map : treelist) {
            if (map.get("PID") != null && node.getId().equals(map.get("PID").toString())) {
                list.add(nodeChildren = new TreeNode(
                        map.get("ID").toString(),
                        (AuthenticatedUserUtil.isLanguageType() == 0 ? map.get("TEXT").toString() : map.get("ENTEXT").toString()),
                        map.get("ISLEAF").toString().equals("true"),
                        TreeNodeUtils.NodeType.auth,
                        0,
                        node.getText(),
                        node.getId(),
                        map.get("SELECTED").toString().equals("true"),
                        map.get("ISCHECKED").toString().equals("true")
                ));
                findTree(nodeChildren, nodes, treelist);
            } else {
                continue;
            }
        }
        node.setChildren(list);
    }


    @Override
    public int saveViewRoleAuthList(ViewRoleAuth viewRoleAuth) {
        int rows = viewRoleAuth.getNodes().length;
        systemManageMapper.deleteViewRoleAuth(viewRoleAuth.getRoleId());
        boolean isWebRoler = false;
        for (int i = 0; i < rows; i++) {
            Map<String, Object> m = new HashMap<>();
            m.put("guid", Common.getUUID());
            m.put("roleId", viewRoleAuth.getRoleId());
            m.put("menuid", viewRoleAuth.getNodes()[i]);
            if ("1".equals(viewRoleAuth.getRoleId()) && 67 == viewRoleAuth.getNodes()[i]) {
                isWebRoler = true;
                continue;
            }
            if (!"1".equals(viewRoleAuth.getRoleId()) && 55 == viewRoleAuth.getNodes()[i]) {
                continue;
            }
            systemManageMapper.insertViewRoleAuth(m);
        }

        if (isWebRoler) {
            Map<String, Object> m = new HashMap<>();
            m.put("guid", Common.getUUID());
            m.put("roleId", "1");
            m.put("menuid", 67);
            systemManageMapper.insertViewRoleAuth(m);
        }
        return rows;
    }

    @Override
    public int getProcessCheckCount(Integer processCheckType, long projectId) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("processCheckType", String.valueOf(processCheckType));
        paramMap.put("projectId", String.valueOf(projectId));
        return systemManageMapper.getProcessCheckCount(paramMap);
    }

    @Override
    public List<ProcessBaseInfoBean> fetchProcessCheckList(Integer processCheckType, long projectId, Paging paging) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("processCheckType", String.valueOf(processCheckType));
        paramMap.put("projectId", String.valueOf(projectId));

        PageHelper.startPage(paging.getPageNum(), paging.getPageSize());
        // select distinct flow_id as flowId,flow_name as flowName,Flow_Id_Input
        // as flowIdInput from (
        List<Map<String, Object>> processInfos = systemManageMapper.fetchProcessCheckList(paramMap);
        // 流程检错结果集合
        List<ProcessBaseInfoBean> processCheckList = new ArrayList<ProcessBaseInfoBean>();

        for (Map<String, Object> map : processInfos) {
            if (map.get("FLOW_ID") == null || map.get("FLOW_NAME") == null) {
                continue;
            }
            ProcessBaseInfoBean processBaseInfoBean = new ProcessBaseInfoBean();
            processBaseInfoBean.setFlowId(Long.valueOf(map.get("FLOW_ID").toString()));
            processBaseInfoBean.setFlowName(map.get("FLOW_NAME").toString());
            if (map.get("FLOW_ID_INPUT") != null) {
                processBaseInfoBean.setFlowIdInput(map.get("FLOW_ID_INPUT").toString());
            }
            processCheckList.add(processBaseInfoBean);
        }

        return processCheckList;
    }

    @Override
    public MessageResult addRole(ViewRoleDetailBean role) {
        MessageResult result = new MessageResult();
        if (role == null) {
            result.setSuccess(false);
            result.setResultMessage("System error");
            return result;
        }
        try {
            // 不为默认角色 1 管理员 2浏览管理员 3 二级管理员 4 默认用户
            if (!"1".equals(role.getId()) && !"2".equals(role.getId()) && !"3".equals(role.getId()) && !"4".equals(role.getId())) {
                deleteRole(role);// 清空 角色下原有的关联关系
                role.setCreatePeople(AuthenticatedUserUtil.getPeopleId());//获取创建人
                role.setUpdatePeople(AuthenticatedUserUtil.getPeopleId());
                role.setId(StringUtils.isNotBlank(role.getId()) ? role.getId() : IDUtils.makeUUID());//getId 不为空的情况下 为编辑内容
                role.setUpdateTime(new java.util.Date());
                role.setCreateTime(new java.util.Date());
                role.setRoleType("2");//1:管理员 0 用户 2：自定义角色。
                //添加角色
                systemManageMapper.addRole(role);
                //如果人员不为空情况下 需要先删除人员下 关联的所有角色。此操作主要为了 一个人员只能关联一个角色
                if (role.getPeopleId().length > 0) {
                    systemManageMapper.delRolesUserToPeople(role);
                }
                //添加  人员  部门  岗位 对应的 角色关联。
                if (role.getPeopleId().length > 0 || role.getOrgId().length > 0 || role.getPosId().length > 0) {
                    systemManageMapper.addRolesUser(role);
                }
            }
            //角色对应的菜单项
            ViewRoleAuth viewRoleAuth = new ViewRoleAuth();
            viewRoleAuth.setRoleId(role.getId());
            viewRoleAuth.setNodes(role.getNodes());
            saveViewRoleAuthList(viewRoleAuth);
            result.setSuccess(true);
            result.setResultMessage(JecnProperties.getValue("SaveSuccess"));
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setResultMessage("System error");
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> findRolesInfo(ViewRoleDetailBean role) {
        List<Map<String, Object>> maps = systemManageMapper.findRolesUser(role);
        /**
         * 前台控件必须要求的 key 是小写，所以转换为小写。
         */
        Map<String, Object> doMap = new HashedMap();
        List<String> list = new ArrayList<>();
        for (int i = maps.size() - 1; i < maps.size(); i--) {
            if (i == -1) {
                break;
            }
            Map<String, Object> map = maps.get(i);
            if (list.contains(map.get("ID").toString())) {//去除 一人多岗情况下的重复数据
                maps.remove(map);
            }
            list.add(map.get("ID").toString());
            Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Object> entry = it.next();
                doMap.put(entry.getKey().toString().toLowerCase(), entry.getValue());
            }
            map.clear();
            map.putAll(doMap);
        }
        IfytekCommon.getIflytekUserName(maps, "name", "login_name");
        return maps;
    }


    /**
     * 删除 浏览角色
     *
     * @param role
     * @return
     */
    @Override
    public MessageResult deleteRole(ViewRoleDetailBean role) {
        MessageResult result = new MessageResult();
        if (StringUtil.isNotEmpty(role.getId())) {
            systemManageMapper.deleteRole(role);//删除角色
            systemManageMapper.deleteRoleAssociationMenu(role);//删除角色下关联菜单
            systemManageMapper.deleteRolesUserToRole(role);//清空角色下的所有关联人员 根据角色删除
            result.setSuccess(true);
            result.setResultMessage(JecnProperties.getValue("SaveSuccess"));
        } else {
            result.setSuccess(false);
            result.setResultMessage("System error : ROLE_ID IS NULL");
        }
        return result;
    }

    /**
     * 根据用户查询所属角色 添加到用户缓存中
     *
     * @param authorityBean
     */
    @Override
    public void addRoles(DataAccessAuthorityBean authorityBean, Long pepoleId) {
        List<String> list = new ArrayList<>();
        String roleId = "0";
        if (authorityBean.isAdmin()) { //管理员
            roleId = "1";
            list.add(roleId);
            authorityBean.setRowId(list);
            return;
        } else if (authorityBean.isViewAdmin()) {//浏览管理员
            roleId = "2";
            list.add(roleId);
            authorityBean.setRowId(list);
            return;
        } else if (authorityBean.isSecondAdmin()) {//二级管理员
            roleId = "3";
            list.add(roleId);
            authorityBean.setRowId(list);
            return;
        } else {
            /**
             *  根据人员查询用户对应角色 直接返回对应的角色ID
             */
            Map<String, Object> peopleMap = systemManageMapper.findRolesByUser(pepoleId);
            if (peopleMap != null && peopleMap.size() > 0) {
                list.add(peopleMap.get("RID").toString());
                authorityBean.setRowId(list);
                return;
            }
            /**
             *  根据人员查询岗位对应角色根据 人员id
             */
            List<String> posMap = systemManageMapper.findRolesThePosByUser(pepoleId);
            if (!posMap.isEmpty() && posMap.size() > 0) {
                authorityBean.setRowId(posMap);
                return;
            }
            /**
             *  根据人员查询部门对应角色根据 人员id
             */
            List<String> orgMap = systemManageMapper.findRolesTheOrgByUser(pepoleId);
            if (!orgMap.isEmpty() && orgMap.size() > 0) {
                authorityBean.setRowId(orgMap);
                return;
            }
            /**
             * 如果没有分配权限的情况下 分配默认用户
             */
            roleId = "4";
            list.add(roleId);
            authorityBean.setRowId(list);
            return;
        }
    }

    // 写出角色以及后续内容
    private void addRoleRange(RoleInfoBean roleWebInfoBean, List<InventoryCell> content, boolean isDownLoad) {
        StringBuilder str = new StringBuilder();
        List<StringBuilder> rangeList = new ArrayList<StringBuilder>();
        StringBuilder flowStr = new StringBuilder();
        StringBuilder fileStr = new StringBuilder();
        StringBuilder standStr = new StringBuilder();
        StringBuilder ruleStr = new StringBuilder();
        StringBuilder riskStr = new StringBuilder();
        StringBuilder termStr = new StringBuilder();
        StringBuilder orgStr = new StringBuilder();
        StringBuilder posGStr = new StringBuilder();
        //角色后内容填充
        for (RoleWebInfoBean role : roleWebInfoBean.getRoleInfo()) { //自定义角色名称 因后期业务可能变动 自定义角色中 暂时隐藏 系统默认角色
            if (!"admin".equals(role.getRoleType()) &&
                    !"design".equals(role.getRoleType()) && !"viewAdmin".equals(role.getRoleType()) && !"isDesignFileAdmin".equals(role.getRoleType()) && !"isDesignRuleAdmin".equals(role.getRoleType())
                    && !"isDesignProcessAdmin".equals(role.getRoleType())) {
                //&& !"secondAdmin".equals(role.getRoleType())
                str.append(isDownLoad ? "\n" : "<br>");
                //自定义角色名称
                str.append(role.getRoleName());
            }
            // 填充角色以及后续内容
            flowStr.append(fillRoleChildren(role.getFlowRes(), isDownLoad));
            fileStr.append(fillRoleChildren(role.getFileRes(), isDownLoad));
            standStr.append(fillRoleChildren(role.getStandRes(), isDownLoad));
            ruleStr.append(fillRoleChildren(role.getRuleRes(), isDownLoad));
            riskStr.append(fillRoleChildren(role.getRiskRes(), isDownLoad));
            termStr.append(fillRoleChildren(role.getTermRes(), isDownLoad));
            orgStr.append(fillRoleChildren(role.getOrgRes(), isDownLoad));
            posGStr.append(fillRoleChildren(role.getPosGroupkRes(), isDownLoad));
        }
        rangeList.add(flowStr);
        rangeList.add(fileStr);
        rangeList.add(standStr);
        rangeList.add(ruleStr);
        rangeList.add(riskStr);
        rangeList.add(termStr);
        rangeList.add(orgStr);
        rangeList.add(posGStr);

        InventoryCell cell = new InventoryCell();
        cell.setText(str.toString());
        content.add(cell);
        //拼接所有权限范围
        for (StringBuilder strs : rangeList) {
            cell = new InventoryCell();
            cell.setText(strs.toString());
            content.add(cell);
        }
    }

    private StringBuilder fillRoleChildren(List<ScopeAuthority> bean, boolean isDownLoad) {
        StringBuilder objName = new StringBuilder();
        for (ScopeAuthority obj : bean) {
            StringBuilder str = new StringBuilder();
            if (obj.getScopeAuthority() != null && !obj.getScopeAuthority().isEmpty()) {
                str.append("(");
                for (ScopeAuthority parent : obj.getScopeAuthority()) {
                    str.append(parent.getName());
                    str.append("/");
                }
                str.deleteCharAt(str.length() - 1);
                str.append(")");
            }
            objName.append(isDownLoad ? "\n" : "<br>");
            objName.append(obj.getName() + str.toString());
        }
        return objName;
    }

    @Override
    public MessageResult fetchLogNames(String version) {

        List<String> logs = new ArrayList<>();

        if ("3".equals(version)) {
            logs = mixRPCService.fetchLogNames();
        } else if ("4".equals(version)) {
            File file = new File("logs");
            if (file.exists() && file.isDirectory()) {
                File[] files = file.listFiles();
                for (File f : files) {
                    logs.add(f.getName());
                }
            }
        }
        return new ViewLogsResult(logs);
    }

    @Override
    public byte[] getLogByte(String version, String name) {
        byte[] bytes = new byte[0];
        if ("3".equals(version)) {
            bytes = mixRPCService.getLogByte(name);
        } else if ("4".equals(version)) {
            File file = new File("logs/" + name);
            if (file.exists() && file.isFile()) {
                InputStream input = null;
                try {
                    input = new FileInputStream(file);
                    bytes = IOUtils.toByteArray(input);
                } catch (Exception e) {
                    logger.error("", e);
                } finally {
                    if (input != null) {
                        try {
                            input.close();
                        } catch (IOException e) {

                        }
                    }
                }
            }
        }

        return bytes;
    }

}
