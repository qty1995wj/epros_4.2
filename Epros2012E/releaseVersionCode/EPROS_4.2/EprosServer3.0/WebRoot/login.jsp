<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
Cookie cookies [] = request.getCookies();
if(cookies!=null){
	for(Cookie c:cookies){
		if(c.getName().equals("keepLogin")){
			request.setAttribute("reqKeepLogin",c.getValue());
		}else if(c.getName().equals("loginName")){
			request.setAttribute("loginName",java.net.URLDecoder.decode(c.getValue(), "UTF-8"));
		}else if(c.getName().equals("password")){
			request.setAttribute("password",c.getValue());
		}
	}
}


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><s:if test="#application.versionType">
				<s:text name="eprosSytem" />
			</s:if> <s:else>
				<s:text name="epsSytem" />
			</s:else></title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/global.css" rel="stylesheet" type="text/css" />
		<link href="css/login.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
function loginOnLoad() {
	if ("${reqKeepLogin}" == "keepLogin") {
		document.getElementById("keepLogin").checked = true;
		document.getElementById("loginName").value = "${loginName}";
		document.getElementById("password").value = "${password}";
		document.form1.submit();
	}

}
function checkLoginStrLengthbyIdnull(id, maxLen) {
	var str = document.getElementById(id).value;
	var lengthstr = getLength(str);
	if (lengthstr > maxLen) {

		return false;
	}
	return true;
}function getLength(str){
				return str.replace(/[^\x00-\xFF]/g,'**').length;
			}
			function formSubmit(){ 
				var loginName = document.getElementById("loginName").value;//document.getElementById("loginName").value;
				if (!checkLoginStrLengthbyIdnull("loginName", 122)) {
					//用户名长度不能超过122个字符!
					alert("\u7528\u6237\u540d\u957f\u5ea6\u4e0d\u80fd\u8d85\u8fc7122\u4e2a\u5b57\u7b26!");
					return;
				}
				if (!checkLoginStrLengthbyIdnull("password", 19)) {
					//密码长度不能超过19个字符!
					alert("\u5bc6\u7801\u957f\u5ea6\u4e0d\u80fd\u8d85\u8fc719\u4e2a\u5b57\u7b26!");
					return;
				}
				document.form1.submit();
			}
			function formReset(){
				document.form1.reset();
			}
			function getFocus(){
				document.form1.loginName.focus();
			}
			function enterToPasswordEvent(event){
				if(event==null){
					event=window.event;//IE
				}
				if(event.keyCode==13){
					document.getElementById("password").focus();
				}
			}
			function enterEvent(event){
				if(event==null){
					event=window.event;//IE
				}
				if(event.keyCode==13){
					document.form1.submit();
				}
			}
			function langChange(){
				document.getElementById("langChange").value = document.getElementById("langSelector").value;
				document.langForm.submit();
			}
		</script>
	</head>

	<body bgcolor="#D3D3D3" onload="loginOnLoad()">
		<div class="login_bg01">
			<table width="100%" border="0" align="center" cellpadding="0"
				cellspacing="0">
				<tr>
					<td valign="top">
						<table width="100%" border="0" align="center" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="252">
									<img src="images/login_top01.jpg" width="252" height="108"
										border="0" align="middle" />
								</td>
								<td>
									<img src="images/login_top02.jpg" width="15" height="108"
										border="0" align="middle" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<table width="100%" border="0" align="center" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="252" valign="top">
									<img src="images/login_top03.jpg" width="252" height="260"
										border="0" align="middle" />
								</td>
								<td width="30%"></td>
								<td valign="top" class="login_border01" style="border: 0px;">

									<form name="form1" action="login.action" method="post">
										<table width="500" border="0" align="center" cellpadding="0"
											cellspacing="0" bgcolor="#FFFFFF">
											<tr>

												<td class="login_bg03" width="500">
													<s:if test="#application.versionType">
														<img src="images/login_text01.jpg" height="80" border="0"
															align="middle" />
													</s:if>
													<s:else>
														<img src="images/login-eps.jpg" height="80" border="0"
															align="middle" />
													</s:else>
												</td>

											</tr>
											<tr>
												<td height="178" valign="top">
													<table width="320" border="0" align="center"
														cellpadding="0" cellspacing="0">
														<tr>
															<td height="25">
																&nbsp;
															</td>
															<td>
																&nbsp;
															</td>
														</tr>
														<tr>
															<td height="12"></td>
															<td class="login_text04">
																<s:if test="#application.isShowTip">
																用户名为员工号，初始密码为123456
																</s:if>
															</td>
														</tr>
														<tr>
															<td width="70px" class="login_text02">
																<s:text name="login.Name" />
																:
															</td>
															<td>
																<input id="loginName" name="loginName" type="text"
																	class="login_input01"
																	onkeydown="enterToPasswordEvent(event);" />
															</td>
														</tr>
														<tr>
															<td height="12"></td>
															<td class="login_text04">
																<s:fielderror>
																	<s:param>loginMessage</s:param>
																</s:fielderror>
															</td>
														</tr>
														<tr>
															<td width="70px" class="login_text02">
																<s:text name="login.Password" />
																:
															</td>
															<td>
																<input id="password" name="password" type="password"
																	class="login_input01" onkeydown="enterEvent(event);" />
															</td>
														</tr>
														<tr>
															<td height="12"></td>
															<td height="12" class="login_text04">
															</td>
														</tr>
														<tr>
															<td height="21">
																<td height="21">
																	<img src="<s:text name='img.src' />/login_button01.jpg"
																		onclick="formSubmit();return false;"
																		style="cursor: pointer;" width="74" height="21"
																		hspace="8" border="0" align="middle" />
																	<img src="<s:text name='img.src' />/login_button02.jpg"
																		onclick="formReset();return false;"
																		style="cursor: pointer;" width="74" height="21"
																		hspace="8" border="0" align="middle" />
																</td>
														</tr>
														<tr>
															<td height="21">
																<td height="21" class="login_text02">
																	<input type="checkBox" name="keepLogin" id="keepLogin"
																		value="keepLogin" />
																	<s:text name="login.State" />
																</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</form>
								</td>
								<td width="30%"></td>
								<td width="252" valign="top">
									<img src="images/login_top04.jpg" width="252" height="260"
										border="0" align="middle" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" align="center" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="252" valign="top">
									<img src="images/login_top05.jpg" width="252" height="27"
										border="0" align="middle" />
								</td>
								<%--								<td width="100%" style="border: 0px">--%>
								<%--								<table style="border: 0px">--%>
								<%--									<tr  style="border: 0px">--%>


								<td width="30%"></td>
								<td valign="top" class="login_bg04" style="border: 0px;">
									<table width="503" style="border: 0px;" border="0"
										align="center" cellpadding="0" cellspacing="0">
										<tr>
											<td width="41" valign="top" style="border: 0px;">
												<img src="images/login_top06.jpg" width="41" height="80"
													border="0" align="middle" />
											</td>
											<td align="center" class="login_text03" width="462">
												Copyright &copy; jecn.com.cn
											</td>

										</tr>
									</table>
								</td>
								<td valign="top">
									<img src="images/login_top07.jpg" width="54" height="80"
										border="0" align="middle" />
								</td>

								<td width="30%"></td>

								<%--								</tr>--%>
								<%--								</table>--%>
								<%--								</td>--%>
								<td width="199">
									<table width="199"></table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>