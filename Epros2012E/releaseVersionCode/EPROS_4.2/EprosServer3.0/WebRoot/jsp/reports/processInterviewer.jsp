<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div" style="height: 99%;">
			<form action="" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="processInterviewerSearchText"
						style="overflow: hidden; padding-bottom: 5px;" class="minWidth">
						<table width="100%">
							<tr>
								<td width="10%" height="25px" align="right">
									<!-- 访问人： -->
									<s:text name="interviewerC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="userName_interview"
										onkeyup="search(userName_interview,userId_interview,search_interview_user,'person');"
										onblur="divNode(search_interview_user)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("person","userName_interview","userId_interview")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_interview_user"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td width="10%" align="right">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="processName_interview_id" maxLength="32" readonly="readonly"/>
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectProcessOrMaWindow("processName_interview_id","processId_interview_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
								</td>

								<td style="vertical-align: middle;" width="10%;">
									<a onClick="toggle(processInterviewerSearchText)"
										style="cursor: pointer;" class="packUp"> <img id="packUp"
											src="${basePath}images/iParticipateInProcess/on.gif"
											align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
												name="packUp" />
									</span> </a>
								</td>
							</tr>

							<tr>
								<td height="25px;" align="right">
									<!-- 时间段: -->
									<s:text name="timeC" />
								</td>
								<td align="left">
									<s:textfield readonly="true" cssClass="Wdate" id="startTime"
										cssStyle="width:36%;border-color: #7F9DB9;"
										onclick="WdatePicker();" theme="simple" />
									-
									<s:textfield readonly="true" cssClass="Wdate" id="endTime"
										cssStyle="width:36%;border-color: #7F9DB9;"
										onclick="WdatePicker();" theme="simple" />
								</td>


								<td align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_interview_id"
										onkeyup="search(orgDutyName_interview_id,orgDutyId_interview_id,search_interview_orgDutyName,'org');"
										onblur="divNode(search_interview_orgDutyName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_interview_id","orgDutyId_interview_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_interview_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td></td>
							</tr>

							<tr>
								<td align="right" height="25px">
									<!-- 部门名称: -->
									<s:text name="orgNameC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgLookName_interview_id"
										onkeyup="search(orgLookName_interview_id,orgLookId_interview_id,search_interview_orgLookName,'org');"
										onblur="divNode(search_interview_orgLookName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgLookName_interview_id","orgLookId_interview_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_interview_orgLookName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td align="right">
									<!-- 岗位名称: -->
									<s:text name="positionNameC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="participateInPostName_interview_id"
										onkeyup="search(participateInPostName_interview_id,participateInPostId_interview_id,search_interview_participateInPost,'pos');"
										onblur="divNode(search_interview_participateInPost)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("position","participateInPostName_interview_id","participateInPostId_interview_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_interview_participateInPost"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td></td>
							</tr>
						</table>
					</div>
					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;" onclick="processInterviewerGridSearch()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;"
							onclick="processInterviewerSearchReset()" />
					</div>
				</div>
				<!-- 访问人ID -->
				<input type="hidden" id="userId_interview" value="-1">
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_interview_id" value="-1" />
				<!-- 流程ID -->
				<input type="hidden" id="processId_interview_id" value="-1" />
				<!-- 部门名称ID -->
				<input type="hidden" id="orgLookId_interview_id" value="-1" />
				<!-- 岗位名称 -->
				<input type="hidden" id="participateInPostId_interview_id"
					value="-1" />
			</form>

			<div id="processInterviewerGrid_id">
			</div>
			<div align="center" style="padding-top: 10px;">
				<a onclick="processInterviewerDownloadExcel()"
					style="cursor: pointer; color: #005EA7;" class="table"><s:text
						name="downLoadExcel" /> </a>
			</div>
		</div>
	</body>
</html>