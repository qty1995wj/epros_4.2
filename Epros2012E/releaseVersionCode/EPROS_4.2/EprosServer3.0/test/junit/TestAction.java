package junit;

import com.jecn.epros.server.webBean.WebLoginBean;

/**
 * 
 * 参照样例
 * 
 * @author admin
 * 
 */
public class TestAction extends JUnitBaseAction {
	/**
	 * 重写父类初始化session
	 */
	protected void setupBeforeInitDispatcher() throws Exception {
		super.setupBeforeInitDispatcher();
		WebLoginBean webLoginBean = createWebLoginBean("admin", "a");
		this.webLoginBean = webLoginBean;
	}

	public void testLogin() {
		// 参数赋值
		request.setParameter("loginName", "admin");
		request.setParameter("password", "a");
		try {
			String result = this.executeActionProxy("/login");
			System.out.println("result:" + result);
			this.assertEquals("main", result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 查询KPI
	 */
	public void testSearchFlowKpi() {
		request.setParameter("searchKpiBean.kpiName", "kpione");
		request.setParameter("searchKpiBean.startTime", "2014-01-13");
		request.setParameter("searchKpiBean.endTime", "2015-01-13");
		request.setParameter("searchKpiBean.flowId", "1423");
		request.setParameter("searchKpiBean.kpiId", "301");
		request.setParameter("searchKpiBean.kpiHorType", "1");
		try {
			String result = this.executeActionProxy("/searchFlowKpi.action");
			System.out.println(result + ":" + response.getContentAsString());
			this.assertEquals(null, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 编辑KPI值 updateFlowKpiImage.action
	 */
	public void testEditKpiValue() {

	}

	/**
	 * 移除KPI 测试事物是否生效
	 */
	// var url = "deleteFlowKpiImage.action?jecnFlowKpi.kpiId=" + kpiId;
	public void testRemoveKpiValue() {

	}

}
