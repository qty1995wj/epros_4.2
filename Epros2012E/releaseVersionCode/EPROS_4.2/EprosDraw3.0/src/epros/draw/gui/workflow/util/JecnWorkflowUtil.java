package epros.draw.gui.workflow.util;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.event.JecnOftenActionProcess;
import epros.draw.event.flowElement.JecnFlowElementEventProcess;
import epros.draw.event.flowElement.JecnManLineResizeProcess;
import epros.draw.event.flowElement.JecnVHLineEventProcess;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.JecnLineTextPanel;
import epros.draw.gui.line.shape.JecnTempManLine;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnSelectAreaPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;

/**
 * 
 * 针对画图面板区域工具类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnWorkflowUtil {

	/** 画图面板标题面板的关闭按钮:未选中图片 */
	private static ImageIcon tabCloseBtnIcon = null;
	/** 画图面板标题面板的关闭按钮:选中图片 */
	private static ImageIcon tabSelectedCloseBtnIcon = null;

	/** 画图面板标题面板:流程图图片 */
	private static ImageIcon partMapIcon = null;
	/** 画图面板标题面板:流程地图图片 */
	private static ImageIcon totalMapIcon = null;
	/** 画图面板标题面板:组织图图片 */
	private static ImageIcon orgMapIcon = null;

	/**
	 * 
	 * 获取流程图图标
	 * 
	 * @return ImageIcon 流程图图标
	 */
	public static ImageIcon getPartMapIcon() {
		if (partMapIcon == null) {
			partMapIcon = JecnFileUtil.getWorkflowImageIcon(MapType.partMap.toString());
		}
		return partMapIcon;
	}

	/**
	 * 
	 * 获取流程地图图标
	 * 
	 * @return ImageIcon 流程地图图标
	 */
	public static ImageIcon getTotalMapIcon() {
		if (totalMapIcon == null) {
			totalMapIcon = JecnFileUtil.getWorkflowImageIcon(MapType.totalMap.toString());
		}
		return totalMapIcon;
	}

	/**
	 * 
	 * 获取组织图标
	 * 
	 * @return ImageIcon 流程地图图标
	 */
	public static ImageIcon getOrgMapIcon() {
		if (orgMapIcon == null) {
			orgMapIcon = JecnFileUtil.getWorkflowImageIcon(MapType.orgMap.toString());
		}
		return orgMapIcon;
	}

	/**
	 * 
	 * 未选中图片
	 * 
	 * @return ImageIcon
	 */
	public static ImageIcon getTabCloseBtnIcon() {
		if (tabCloseBtnIcon == null) {
			tabCloseBtnIcon = JecnFileUtil.getWorkflowImageIcon("tabCloseBtn");
		}
		return tabCloseBtnIcon;
	}

	/**
	 * 
	 * 选中图片
	 * 
	 * @return
	 */
	public static ImageIcon getTabSelectedCloseBtnIcon() {
		if (tabSelectedCloseBtnIcon == null) {
			tabSelectedCloseBtnIcon = JecnFileUtil.getWorkflowImageIcon("tabSelectedCloseBtn");
		}
		return tabSelectedCloseBtnIcon;
	}

	/**
	 * 
	 * 点击面板、图形处理
	 * 
	 * @param baseFlowElement
	 */
	public static void setCurrentSelect(JecnBaseFlowElementPanel baseFlowElement) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || baseFlowElement == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane.getCurrentSelectElement().contains(baseFlowElement)) {
			return;
		}
		// 添加元素到画板选中集合
		desktopPane.addSelectFlowElementList(baseFlowElement);
		// 显示图形元素选中点
		switch (baseFlowElement.getFlowElementData().getMapElemType()) {
		case ManhattanLine:// 单箭头连接线
		case CommonLine:// 不带箭头连接线
			JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) baseFlowElement;
			linePanel.showManhattanLineResizeHandles();
			break;
		case DividingLine:
		case HDividingLine:
		case VDividingLine:
			JecnBaseDividingLinePanel dividingLine = (JecnBaseDividingLinePanel) baseFlowElement;
			// 显示分割线选中点
			dividingLine.showLineResizeHandles();
			break;
		case ParallelLines:
		case VerticalLine:
			JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) baseFlowElement;
			// 显示横竖分割线选中点
			baseVHLinePanel.showParallelLinesResizeHandles();
			break;
		default:// 默认其他图形处理
			JecnPaintFigureUnit.showResizeHandles((JecnBaseFigurePanel) baseFlowElement);
			break;
		}

		// 键盘事件未执行完，切换选中元素，处理键盘释放事件
		desktopPane.getKeyHandler().keyReleased();
	}

	/**
	 * 清空选中的图形元素集合的所有显示点
	 * 
	 * @return boolean true：清除成功 false：清除失败
	 * 
	 */
	public static boolean hideAllResizeHandle() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return false;
		}

		// 移除编辑框，恢复流程元素显示（boolean true:移除成功；false:移除不成功）
		boolean ret = desktopPane.getAidCompCollection().removeFlowElemPanel();
		if (!ret) {
			return false;
		}

		// 清空面板所有选选中点、图形元素编辑点
		List<JecnBaseFlowElementPanel> listSelectAll = desktopPane.getCurrentSelectElement();

		hiddenSelectFigureResizeHandle(listSelectAll);
		// 当前选中的所有控件清空
		desktopPane.getCurrentSelectElement().clear();
		// 圈选框坐标清空
		JecnSelectAreaPanel areaPanel = desktopPane.getAidCompCollection().getSelecedAreaPanel();
		areaPanel.setEndPoint(null);

		// 隐藏 to或from 编号选择框
		JecnPaintFigureUnit.hiddenToOrFromLinkActive();

		JecnDesignerProcess.getDesignerProcess().hiddelElementPanel();
		return true;
	}

	public static void hiddenSelectFigureResizeHandle(List<JecnBaseFlowElementPanel> listSelectAll) {
		if (listSelectAll.size() == 1) {
			// 泳池特殊处理
			listSelectAll = JecnWorkflowUtil.selectModelFigureContainsEles(listSelectAll.get(0));

			JecnBaseFlowElementPanel elementPanel = listSelectAll.get(0);
			if (elementPanel instanceof JecnBaseFigurePanel) {
				// 元素属性特殊处理
				JecnDesignerProcess.getDesignerProcess().updateElementPanel((JecnBaseFigurePanel) elementPanel);
			}
		}

		for (JecnBaseFlowElementPanel baseFlowElement : listSelectAll) {
			switch (baseFlowElement.getFlowElementData().getMapElemType()) {
			case ManhattanLine:// 单箭头连接线
			case CommonLine:// 不带箭头连接线
				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) baseFlowElement;
				linePanel.hideManhattanLineResizeHandles();
				break;
			case DividingLine:
			case HDividingLine:
			case VDividingLine:
				JecnBaseDividingLinePanel dividingLine = (JecnBaseDividingLinePanel) baseFlowElement;
				dividingLine.hideLineResizeHandles();
				break;

			case ParallelLines:
			case VerticalLine:
				JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) baseFlowElement;
				// 显示横竖分割线选中点
				baseVHLinePanel.hideParallelLinesResizeHandles();
				break;
			default:// 默认其他图形处理
				if (baseFlowElement instanceof ModelFigure) {
					ModelFigure modelFigure = (ModelFigure) baseFlowElement;
					modelFigure.setChick(false);
				}
				JecnPaintFigureUnit.hideResizeHandles((JecnBaseFigurePanel) baseFlowElement);
				break;
			}
		}
	}

	/**
	 * 
	 * 获取有效的放大缩小倍数，如：1.0,2.0等
	 * 
	 * @param workflowSpace
	 *            double 给定的放大缩小倍数
	 * @return double 有效的放大缩小倍数
	 */
	public static double convertWorkflowSpace(double workflowSpace) {
		// 获取当前放大缩小倍数
		double tmpWorkflowScale;
		if (workflowSpace < JecnSystemStaticData.getMinWorkflowSpace()) {
			tmpWorkflowScale = JecnSystemStaticData.getMinWorkflowSpace();
		} else if (workflowSpace > JecnSystemStaticData.getMaxWorkflowSpace()) {
			tmpWorkflowScale = JecnSystemStaticData.getMaxWorkflowSpace();
		} else {
			tmpWorkflowScale = workflowSpace;
		}
		return tmpWorkflowScale;
	}

	/**
	 * 
	 * 获取有效的放大缩小倍数
	 * 
	 * @param workflowSpace
	 *            String 输入的放大缩小倍数字符串
	 * @return double 有效的放大缩小倍数
	 */
	public static double convertWorkflowSpace(String workflowSpace) {
		if (DrawCommon.isNullOrEmtryTrim(workflowSpace)) {
			return 1.0;
		}

		// 去掉%
		String text = String.valueOf(workflowSpace).replaceAll("%", "");

		// 判断是否是数字
		Pattern pat = Pattern.compile("[0-9]+");
		// 编辑控件内容不满足设置模式直接返回
		if (!pat.matcher(text).matches()) {
			return 1.0;
		}
		// 当前输入或选中的放大缩小倍数
		double newScale = Double.valueOf(text) / 100;

		// 获取当前放大缩小倍数
		return convertWorkflowSpace(newScale);
	}

	/**
	 * 获取制定集合图形元素在指定区域内的最大坐标和最小坐标
	 * 
	 * @param listPanel
	 *            List<JecnBaseFlowElementPanel> 制定的图形元素集合
	 * @return JecnSelectFigureXY 记录区域最大和最小坐标
	 */
	public static JecnSelectFigureXY getMaxSelectFigureXY(List<JecnBaseFlowElementPanel> listPanel) {
		int minX = 0;
		int minY = 0;
		int maxX = 0;
		int maxY = 0;

		int value = 10;
		JecnSelectFigureXY figureXY = new JecnSelectFigureXY();
		for (int i = 0; i < listPanel.size(); i++) {
			if (listPanel.get(i) instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel figure = (JecnBaseFigurePanel) listPanel.get(i);
				if (i == 0) {
					minX = figure.getLocation().x;
					minY = figure.getLocation().y;
				}
				if (maxX < figure.getLocation().x + figure.getWidth()) {
					maxX = figure.getLocation().x + figure.getWidth();
				}
				if (maxY < figure.getLocation().y + figure.getHeight()) {
					maxY = figure.getLocation().y + figure.getHeight();
				}
				if (minX > figure.getLocation().x) {
					minX = figure.getLocation().x;
				}
				if (minY > figure.getLocation().y) {
					minY = figure.getLocation().y;
				}
				if (figure instanceof CommentText) {
					CommentText commentText = (CommentText) figure;
					Point startP = commentText.commentLine.getStartPoint();
					if (startP.x > maxX) {
						maxX = startP.x;
					}
					if (startP.x < minX) {
						minX = startP.x;
					}
					if (startP.y < minY) {
						minY = startP.y;
					}
					if (startP.y < maxY) {
						maxY = startP.y;
					}
				}
			}
		}
		figureXY.setMaxX(maxX + value);
		figureXY.setMaxY(maxY + value);
		figureXY.setMinX(minX - value);
		figureXY.setMinY(minY - value);
		return figureXY;
	}

	/**
	 * 得到画板的最大值(加了10像素)
	 * 
	 * @param workflow
	 *            面板
	 * @param listPanel
	 *            面板流程元素集合
	 */
	public static JecnSelectFigureXY getMaxSize(JecnDrawDesktopPane workflow, List<JecnBaseFlowElementPanel> listPanel) {

		// 获取选中图形区域的最大坐标和最小坐标
		JecnSelectFigureXY figureXY = getSelectFigureXY(listPanel);
		// 获取坐标是否达到边界处理
		reSetSelectFigureXY(figureXY);
		return figureXY;
	}

	/**
	 * 得到画板的最大值
	 * 
	 * @param workflow
	 *            面板
	 * @param listPanel
	 *            面板流程元素集合
	 */
	public static JecnSelectFigureXY getWorkflowMaxSize(JecnDrawDesktopPane workflow,
			List<JecnBaseFlowElementPanel> listPanel) {
		// 获取选中图形区域的最大坐标和最小坐标
		JecnSelectFigureXY figureXY = getSelectFigureXY(listPanel);
		return figureXY;
	}

	/**
	 * 获取选中图形区域的最大坐标和最小坐标
	 * 
	 * @param listPanel
	 *            选中图形集合
	 * @return JecnSelectFigureXY 获取指定区域内坐标的最大值和最小值
	 */
	public static JecnSelectFigureXY getSelectFigureXY(List<JecnBaseFlowElementPanel> listPanel) {
		int minX = 0;
		int minY = 0;
		int maxX = 0;
		int maxY = 0;
		// 获取图形的最大XY坐标
		JecnSelectFigureXY figureXY = new JecnSelectFigureXY();
		// 非连接线和横竖分割线第一次给变量赋值的标识：true不用比较大小直接赋值；false：需要比较大小满足条件才能赋值
		boolean firstFlag = true;
		for (JecnBaseFlowElementPanel flowElem : listPanel) {
			if (flowElem instanceof JecnBaseFigurePanel) {// 图形元素
				JecnBaseFigurePanel figure = (JecnBaseFigurePanel) flowElem;
				if (firstFlag) {
					minX = figure.getLocation().x;
					minY = figure.getLocation().y;
					firstFlag = false;
				}
				if (minX > figure.getLocation().x) {
					minX = figure.getLocation().x;
				}
				if (minY > figure.getLocation().y) {
					minY = figure.getLocation().y;
				}

				if (maxX < figure.getLocation().x + figure.getWidth()) {
					maxX = figure.getLocation().x + figure.getWidth();
				}
				if (maxY < figure.getLocation().y + figure.getHeight()) {
					maxY = figure.getLocation().y + figure.getHeight();
				}
				if (figure instanceof CommentText) {
					CommentText commentText = (CommentText) figure;
					Point startP = commentText.commentLine.getStartPoint();
					if (startP.x > maxX) {
						maxX = startP.x;
					}
					if (startP.x < minX) {
						minX = startP.x;
					}
					if (startP.y < minY) {
						minY = startP.y;
					}
					if (startP.y > maxY) {
						maxY = startP.y;
					}
				}
			} else if (flowElem instanceof JecnBaseManhattanLinePanel) {// 连接线
				JecnBaseManhattanLinePanel manLine = (JecnBaseManhattanLinePanel) flowElem;
				for (LineSegment lineSegment : manLine.getLineSegments()) {
					if (firstFlag) {
						minX = lineSegment.getZoomEPoint().x;
						minY = lineSegment.getZoomEPoint().y;
						firstFlag = false;
					}
					if (lineSegment.getZoomEPoint().x < minX) {
						minX = lineSegment.getZoomEPoint().x;
					}
					if (lineSegment.getZoomSPoint().x < minX) {
						minX = lineSegment.getZoomSPoint().x;
					}
					if (lineSegment.getZoomEPoint().y < minY) {
						minY = lineSegment.getZoomEPoint().y;
					}
					if (lineSegment.getZoomSPoint().y < minY) {
						minY = lineSegment.getZoomSPoint().y;
					}
					if (lineSegment.getZoomSPoint().getX() > maxX) {
						maxX = lineSegment.getZoomSPoint().x;
					}
					if (lineSegment.getZoomEPoint().x > maxX) {
						maxX = lineSegment.getZoomEPoint().x;
					}
					if (lineSegment.getZoomSPoint().y > maxY) {
						maxY = lineSegment.getZoomSPoint().y;
					}
					if (lineSegment.getZoomEPoint().y > maxY) {
						maxY = lineSegment.getZoomEPoint().y;
					}
				}
				// 获取面板所有连接线编辑框
				JecnLineTextPanel textPanel = manLine.getTextPanel();
				if (textPanel == null) {
					continue;
				}
				if (minX > textPanel.getLocation().x) {
					minX = textPanel.getLocation().x;
				}
				if (minY > textPanel.getLocation().y) {
					minY = textPanel.getLocation().y;
				}

				if (maxX < textPanel.getLocation().x + textPanel.getWidth()) {
					maxX = textPanel.getLocation().x + textPanel.getWidth();
				}
				if (maxY < textPanel.getLocation().y + textPanel.getHeight()) {
					maxY = textPanel.getLocation().y + textPanel.getHeight();
				}
			} else if (flowElem instanceof JecnBaseDividingLinePanel) {// 分割线（斜线、横线竖线）
				JecnBaseDividingLinePanel fig = (JecnBaseDividingLinePanel) flowElem;
				if (firstFlag && minX == 0 && minY == 0) {
					if (fig.getStartPoint().x > fig.getEndPoint().x) {
						minX = fig.getEndPoint().x;
					} else {
						minX = fig.getStartPoint().x;
					}
					if (fig.getStartPoint().y > fig.getEndPoint().y) {
						minY = fig.getEndPoint().y;
					} else {
						minY = fig.getStartPoint().y;
					}
					firstFlag = false;
				}

				if (minX > fig.getStartPoint().x) {
					minX = fig.getStartPoint().x;
				}
				if (minY > fig.getStartPoint().getY()) {
					minY = fig.getStartPoint().y;
				}
				if (minX > fig.getEndPoint().x) {
					minX = fig.getEndPoint().x;
				}
				if (minY > fig.getEndPoint().y) {
					minY = fig.getEndPoint().y;
				}

				if (maxX < fig.getStartPoint().x) {
					maxX = fig.getStartPoint().x;
				}
				if (maxY < fig.getStartPoint().y) {
					maxY = fig.getStartPoint().y;
				}
				if (maxX < fig.getEndPoint().x) {
					maxX = fig.getEndPoint().x;
				}
				if (maxY < fig.getEndPoint().y) {
					maxY = fig.getEndPoint().y;
				}
			}
		}

		figureXY.setMaxX(maxX);
		figureXY.setMaxY(maxY);
		figureXY.setMinX(minX);
		figureXY.setMinY(minY);
		return figureXY;
	}

	/**
	 * 获取坐标是否达到边界处理
	 * 
	 * @param figureXY
	 */
	public static void reSetSelectFigureXY(JecnSelectFigureXY figureXY) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		int minX = figureXY.getMinX();
		int minY = figureXY.getMinY();
		int maxX = figureXY.getMaxX();
		int maxY = figureXY.getMaxY();
		// 增加边缘
		if (minX - 10 < 0) {
			minX = 0;
		} else {
			minX = minX - 10;
		}
		if (minY - 10 < 0) {
			minY = 0;
		} else {
			minY = minY - 10;
		}
		boolean falg = false;
		if (maxX > workflow.getWidth()) {
			// maxX = maxX + 10;
			falg = true;
		}
		maxX = maxX + 10;
		if (maxY > workflow.getHeight()) {
			// maxY = maxY + 10;
			falg = true;
		}
		maxY = maxY + 10;
		if (falg) {
			workflow.setPreferredSize(new Dimension(maxX, maxY));
			workflow.setSize(maxX, maxY);
			workflow.validate();
			workflow.repaint();
		}
		figureXY.setMaxX(maxX);
		figureXY.setMaxY(maxY);
		figureXY.setMinX(minX);
		figureXY.setMinY(minY);
	}

	/**
	 * 返回面板图形选中区域中心点（复制粘贴应用、片段应用）
	 * 
	 * @return Point
	 */
	public static Point getSelectCenterPoint(List<JecnBaseFlowElementPanel> panelList) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || panelList == null || panelList.size() == 0) {
			return null;
		}
		// 获取选中图形元素的最大XY坐标和最小XY坐标
		JecnSelectFigureXY figureXY = JecnWorkflowUtil.getSelectFigureXY(panelList);

		int centerX = (figureXY.getMaxX() - figureXY.getMinX()) / 2 + figureXY.getMinX();

		int centerY = (figureXY.getMaxY() - figureXY.getMinY()) / 2 + figureXY.getMinY();

		return new Point(centerX, centerY);

	}

	/**
	 * 键盘Ctrl+左键
	 * 
	 * @param e
	 * @return boolean true:点击Ctrl键，false：点击的不是Ctrl键
	 */
	public static boolean isPressCtril(MouseEvent e) {
		if (e.getModifiers() == 18) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * 判断是否有按下Ctrl键
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 * @return boolean 是按住Ctrl键；未按住Ctrl键
	 */
	public static boolean isCtrl(MouseEvent e) {
		if (e == null) {
			return false;
		}
		return ((e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0) ? true : false;
	}

	/**
	 * 添加图形到选中集合(选中集合不包括该图形 添加一个删除一个用)
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel 流程元素
	 * @param isCtrl
	 *            boolean 按住Ctrl键标识
	 */
	public static void clearListAndAddElement(JecnBaseFlowElementPanel flowElementPanel, boolean isCtrl) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}

		if (!workflow.isContainsSelectList(flowElementPanel)) {// 选中图形是否包含当前鼠标点击图形
			if (!isCtrl) {// 是否点击的ctrl键
				// 清空选中的图形元素集合的所有显示点
				// 清空面板所有选选中点、图形元素编辑点
				List<JecnBaseFlowElementPanel> listSelectAll = workflow.getCurrentSelectElement();
				JecnWorkflowUtil.hiddenSelectFigureResizeHandle(listSelectAll);
				workflow.getCurrentSelectElement().clear();
			}
			// 点击图形处理
			JecnWorkflowUtil.setCurrentSelect(flowElementPanel);
		}
	}

	/**
	 * 锁定泳池，拖动泳池后，泳池内包含 元素一起移动和复制
	 * 
	 * @param flowElementPanel
	 * @return
	 */
	public static List<JecnBaseFlowElementPanel> selectModelFigureContainsEles(JecnBaseFlowElementPanel flowElementPanel) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (!JecnSystemStaticData.isShowModel() || !(flowElementPanel instanceof ModelFigure)) {
			return workflow.getCurrentSelectElement();
		}
		List<JecnBaseFlowElementPanel> panelList = workflow.getPanelList();

		List<JecnBaseFlowElementPanel> selectModelContainsEleList = new ArrayList<JecnBaseFlowElementPanel>();
		int maxX = flowElementPanel.getX() + flowElementPanel.getWidth();
		int maxY = flowElementPanel.getY() + flowElementPanel.getHeight();
		for (JecnBaseFlowElementPanel ele : panelList) {
			if (flowElementPanel == ele || workflow.getCurrentSelectElement().contains(ele)) {
				selectModelContainsEleList.add(ele);
				continue;
			}
			if (flowElementPanel.getX() <= ele.getX() && flowElementPanel.getY() <= ele.getY()
					&& ele.getX() + ele.getWidth() <= maxX && ele.getY() + ele.getHeight() <= maxY) {
				selectModelContainsEleList.add(ele);
			}
		}
		return selectModelContainsEleList;
	}

	/**
	 * 根据跟定日期转换字符串日期格式yyyy-MM-dd
	 * 
	 * @param date
	 * @return String 字符串日期格式
	 */
	public static String getStringbyDate(Date date) {
		if (date == null) {
			return null;
		}
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	/**
	 * 鼠标圈选处理
	 * 
	 * @param drawDesktopPane
	 */
	public static boolean getSelectAreaPanels() {
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (drawDesktopPane == null) {
			return false;
		}

		// 获取鼠标全选selectPanel
		JecnSelectAreaPanel selectPanel = JecnDrawMainPanel.getMainPanel().getWorkflow().getAidCompCollection()
				.getSelecedAreaPanel();

		// 获取面板所有流程元素集合
		List<JecnBaseFlowElementPanel> allElementList = drawDesktopPane.getPanelList();

		// 圈选selectPanel 的开始点
		Point startPoint = selectPanel.getStartPoint();
		// 圈选selectPanel 的结束点
		Point endPoint = selectPanel.getEndPoint();
		if (startPoint == null || endPoint == null) {
			return false;
		}
		if ((startPoint.x != endPoint.x) && (startPoint.y != endPoint.y)) {
			for (int i = 0; i < allElementList.size(); i++) {// 面板所有流程元素
				JecnBaseFlowElementPanel flowElementPanel = allElementList.get(i);
				if (flowElementPanel instanceof JecnBaseFigurePanel) {// 面板图形元素
					JecnBaseFigurePanel figure = (JecnBaseFigurePanel) flowElementPanel;
					if (((Math.min(startPoint.x, endPoint.x) < figure.getLocation().x) && (figure.getLocation().x < Math
							.max(startPoint.x, endPoint.x)))
							&& ((Math.min(startPoint.y, endPoint.y) < figure.getLocation().y) && (figure.getLocation().y < Math
									.max(startPoint.y, endPoint.y)))) {
						JecnWorkflowUtil.setCurrentSelect(figure);
					}
				} else if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 连接线
					JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) flowElementPanel;
					// 获取连接线开始点和结束点 在当前面板放大缩小倍数下的坐标值
					int x1 = 0;
					int x2 = 0;
					int y1 = 0;
					int y2 = 0;
					// 开始点坐标值
					x1 = manhattanLine.getStartPoint().x;
					y1 = manhattanLine.getStartPoint().y;
					// 结束点坐标值
					x2 = manhattanLine.getEndPoint().x;
					y2 = manhattanLine.getEndPoint().y;
					if (Math.min(startPoint.x, endPoint.x) < Math.min(x1, x2)
							&& Math.max(startPoint.x, endPoint.x) > Math.max(x1, x2)
							&& Math.min(startPoint.y, endPoint.y) < Math.min(y1, y2)
							&& Math.max(startPoint.y, endPoint.y) > Math.max(y1, y2)) {
						JecnWorkflowUtil.setCurrentSelect(manhattanLine);
						// 面板获取
						drawDesktopPane.requestFocusInWindow();
					}
				} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {// 分割线
					JecnBaseDividingLinePanel divLine = (JecnBaseDividingLinePanel) flowElementPanel;
					if (Math.min(startPoint.x, endPoint.x) < Math.min(divLine.getStartPoint().x,
							divLine.getEndPoint().x)
							&& Math.max(startPoint.x, endPoint.x) > Math.max(divLine.getStartPoint().x, divLine
									.getEndPoint().x)
							&& Math.min(startPoint.y, endPoint.y) < Math.min(divLine.getStartPoint().y, divLine
									.getEndPoint().y)
							&& Math.max(startPoint.y, endPoint.y) > Math.max(divLine.getStartPoint().y, divLine
									.getEndPoint().y)) {
						JecnWorkflowUtil.setCurrentSelect(divLine);
					}
				}

			}
		}
		if (drawDesktopPane.getCurrentSelectElement().size() < 1) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 关闭面板是否弹出是否保存对话框
	 * 
	 * 
	 * @param flag
	 *            boolean 是-否-取消 true: OK-NO-CANCEL；false：OK-NO
	 * @return true:不执行当前操作，false 执行当前操作
	 * 
	 */
	public static boolean isSaveWorkFlow(JecnDrawDesktopPane desktopPane, boolean flag) {
		if (desktopPane == null) {
			return true;
		}

		int save = -10;
		if (desktopPane.getFlowMapData().isSave()) {// 存在撤销集合，需判断是否保存处理
			if (flag) {
				save = JecnOptionPane.showConfirmDialog(JecnDrawMainPanel.getMainPanel(), JecnDrawMainPanel
						.getMainPanel().getResourceManager().getValue("optionInfo"), null,
						JecnOptionPane.YES_NO_CANCEL_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			} else {
				save = JecnOptionPane.showConfirmDialog(JecnDrawMainPanel.getMainPanel(), JecnDrawMainPanel
						.getMainPanel().getResourceManager().getValue("optionInfo"), null,
						JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			}

			if (save == JecnOptionPane.CANCEL_OPTION) {// 是否保存点击取消
				return true;
			} else if (save == JecnOptionPane.OK_OPTION) {// 提示是否保存，点击是
				JecnDrawMainPanel.getMainPanel().getTabbedPane().setSelectedComponent(desktopPane.getScrollPanle());
				boolean isSave = JecnOftenActionProcess.saveWokfFlow(desktopPane);
				if (!isSave) {// 保存弹出框点击取消
					return true;
				} else {
					// 保存成功不处理
				}
			}
		}

		if (JecnSystemStaticData.isEprosDesigner()) {// 设计器
			boolean ret = JecnDesignerProcess.getDesignerProcess().workFlowClose(desktopPane);

			if (!ret) {// 操作失败
				// 提示内容
				String text = "[" + desktopPane.getFlowMapData().getName() + "]"
						+ JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("saveError");

				JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), text, null,
						JecnOptionPane.ERROR_MESSAGE);
				if (save == JecnOptionPane.OK_OPTION) {// 是否保存提示界面中是按钮
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * 给定图形是否添加需要置底，添加需要置底的图形：关键活动（PA,KSF,KCP）、协作框、泳池
	 * 
	 * @param figure
	 *            JecnBaseFlowElementPanel
	 * @return boolean 是返回true；不是返回false
	 * 
	 */
	public static boolean isAddLayoutBottom(JecnBaseFlowElementPanel figure) {
		if (figure == null) {
			return false;
		}
		MapElemType mapElemType = figure.getFlowElementData().getMapElemType();
		if (mapElemType == MapElemType.PAFigure || mapElemType == MapElemType.KSFFigure
				|| mapElemType == MapElemType.KCPFigure || mapElemType == MapElemType.KCPFigureComp
				|| mapElemType == MapElemType.DottedRect || mapElemType == MapElemType.ModelFigure) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 判断面板右键菜单、流程元素右键菜单其中一个显示
	 * 
	 * @return 显示：true ; 隐藏：false
	 */
	public static boolean isPopupMenuShowing() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// true：面板右键菜单、流程元素右键菜单其中一个显示
		boolean show = false;

		if (workflow == null) {
			return show;
		}

		// 面板右键菜单显示标识
		boolean workShow = false;
		// 流程元素右键菜单标识
		boolean flowShow = false;
		switch (workflow.getFlowMapData().getMapType()) {
		case partMap:// 流程图
			// 面板右键菜单
			workShow = JecnDrawMainPanel.getMainPanel().getPartMapWorkflowPopupMenu().getWorkflowPopupMenu()
					.isShowing();
			// 流程元素右键菜单
			flowShow = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu().getFlowElemPopupMenu()
					.isShowing();

			show = (workShow || flowShow) ? true : false;
			break;
		case totalMap:// 流程地图
			// 面板右键菜单
			workShow = JecnDrawMainPanel.getMainPanel().getTotalMapWorkflowPopupMenu().getWorkflowPopupMenu()
					.isShowing();
			// 流程元素右键菜单
			flowShow = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu().getFlowElemPopupMenu()
					.isShowing();

			show = (workShow || flowShow) ? true : false;
			break;
		case orgMap:// 组织图
			// 面板右键菜单
			workShow = JecnDrawMainPanel.getMainPanel().getOrgMapWorkflowPopupMenu().getWorkflowPopupMenu().isShowing();
			// 流程元素右键菜单
			flowShow = JecnDrawMainPanel.getMainPanel().getOrgMapFlowElemPopupMenu().getFlowElemPopupMenu().isShowing();

			show = (workShow || flowShow) ? true : false;
			break;
		}
		return show;
	}

	/**
	 * 
	 * 删除拖动连接线小线段生成的虚拟连接线
	 * 
	 */
	public static void disposeAllTempLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		for (int i = 0; i < desktopPane.getTempLines().size(); i++) {
			desktopPane.remove(desktopPane.getTempLines().get(i));
		}
		desktopPane.getTempLines().clear();
	}

	/**
	 * 清空拖动连接线端点生成的虚拟曼哈顿线段
	 * 
	 */
	public static void disposeLines() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		JecnTempManLine tempLine = desktopPane.getTempLine();
		JecnTempManLine jecnTempManLine = desktopPane.getJecnTempManLine();

		if (tempLine != null) {
			if (tempLine.getTextPanel() != null) {
				desktopPane.remove(tempLine.getTextPanel());
				tempLine.setTextPanel(null);
			}
			tempLine.disposeLines();
			desktopPane.setTempLine(null);
		}
		if (jecnTempManLine != null) {
			if (jecnTempManLine.getTextPanel() != null) {
				desktopPane.remove(jecnTempManLine.getTextPanel());
				jecnTempManLine.setTextPanel(null);
			}
			jecnTempManLine.disposeLines();
			desktopPane.setJecnTempManLine(null);
			JecnManLineResizeProcess.setDraggedResizePanel(null);
		}
	}

	/**
	 * 
	 * 拖动线端点时出现bug才添加方法
	 * 
	 * 清空拖动连接线端点生成的虚拟曼哈顿线段以及清除拖动线段端点记录临时端点
	 * 
	 */
	public static void disposeLinesAndEidtPoint() {
		disposeLines();
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		desktopPane.setTempEditPort(null);
	}

	/**
	 * 
	 * 清除 拖动添加图形等操作产生的临时数据，流程元素库选中指针状态
	 * 
	 * @return boolean true：右键菜单显示返回状态；false:其他操作
	 */
	public static boolean clearStates() {
		// 判断面板右键菜单、流程元素右键菜单其中一个显示
		if (JecnWorkflowUtil.isPopupMenuShowing()) {
			return true;
		}

		if (JecnFlowElementEventProcess.isDragged) {// 图形
			JecnFlowElementEventProcess.isDragged = false;
		}
		if (JecnVHLineEventProcess.isDragged()) {// 横竖分割线
			JecnVHLineEventProcess.setDragged(false);
		}
		if (JecnManLineResizeProcess.isDragged()) {// 拖动两端端点，点击连接线添加连接线
			JecnManLineResizeProcess.setDragged(false);
		}

		// 临时小线段集合，比如拖动过程中生成线段，拖动结束后清除该临时线段
		JecnWorkflowUtil.disposeAllTempLine();
		// 清空临时线段数据
		JecnWorkflowUtil.disposeLinesAndEidtPoint();
		// 隐藏编辑点
		JecnPaintFigureUnit.hideAllConnectPort();

		// 清除快捷添加图形显示小三角形
		JecnDrawMainPanel.getMainPanel().interruptLinkFigure();

		// 修改成指针
		JecnDrawMainPanel.getMainPanel().getBoxPanel().setSelectedBtnType(MapElemType.guide);

		return false;
	}

	/**
	 * 
	 * 清除 拖动添加图形等操作产生的临时数据，流程元素库选中指针状态
	 * 
	 */
	public static void clearStatesAndSelected() {
		if (JecnFlowElementEventProcess.isDragged) {// 图形
			JecnFlowElementEventProcess.isDragged = false;
		}
		if (JecnVHLineEventProcess.isDragged()) {// 横竖分割线
			JecnVHLineEventProcess.setDragged(false);
		}
		if (JecnManLineResizeProcess.isDragged()) {// 拖动两端端点，点击连接线添加连接线
			JecnManLineResizeProcess.setDragged(false);
		}

		// 临时小线段集合，比如拖动过程中生成线段，拖动结束后清除该临时线段
		JecnWorkflowUtil.disposeAllTempLine();
		// 清空临时线段数据
		JecnWorkflowUtil.disposeLinesAndEidtPoint();
		// 隐藏编辑点
		JecnPaintFigureUnit.hideAllConnectPort();
		// 清空选中的图形元素集合的所有显示点 并清空选中集合
		JecnWorkflowUtil.hideAllResizeHandle();

		// 清除快捷添加图形显示小三角形
		JecnDrawMainPanel.getMainPanel().interruptLinkFigure();

		// 修改成指针
		JecnDrawMainPanel.getMainPanel().getBoxPanel().setSelectedBtnType(MapElemType.guide);
	}

	/***
	 * 判断是否为流程图
	 * 
	 * @param desktopPane
	 * @return
	 */
	public static boolean isPartMap(JecnDrawDesktopPane desktopPane) {
		if (desktopPane == null) {
			JecnLogConstants.LOG_LINK_FIGURE_PANEL.error("desktopPane参数为空");
			return false;
		}
		MapType mapType = desktopPane.getFlowMapData().getMapType();
		if (mapType.equals(MapType.partMap)) {
			return true;
		}
		return false;
	}

	/**
	 * 情况格式刷
	 */
	public static void clearFormatPainter() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		workflow.setFormatPainterData(null);
		workflow.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public static List<JecnBaseFigurePanel> getEditPortList(Point mouseP) {
		int INSTANCE = 50;

		List<JecnBaseFigurePanel> panels = new ArrayList<JecnBaseFigurePanel>();
		List<JecnBaseFlowElementPanel> panelList = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();
		for (JecnBaseFlowElementPanel elementPanel : panelList) {
			if (!(elementPanel instanceof JecnBaseFigurePanel)) {
				continue;
			}
			if (elementPanel instanceof ModelFigure) {
				continue;
			}
			// 判断鼠标 坐标位置附件元素
			Rectangle newRect = new Rectangle(elementPanel.getX() - INSTANCE, elementPanel.getY() - INSTANCE,
					elementPanel.getWidth() + 2 * INSTANCE, elementPanel.getHeight() + 2 * INSTANCE);
			if (newRect.contains(mouseP)) {
				panels.add((JecnBaseFigurePanel) elementPanel);
			}
		}
		return panels;
	}
}
