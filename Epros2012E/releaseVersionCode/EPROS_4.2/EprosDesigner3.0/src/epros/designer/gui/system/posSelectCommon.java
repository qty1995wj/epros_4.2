package epros.designer.gui.system;

import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class posSelectCommon extends SingleSelectCommon {
	private static Logger log = Logger.getLogger(posSelectCommon.class);

	public posSelectCommon(int rows, JecnPanel infoPanel, Insets insets, JecnTreeBean jecnTreeBean,
			JecnDialog jecnDialog, String labName,boolean isRequest) {
		super(rows, infoPanel, insets, jecnTreeBean, jecnDialog, labName,isRequest);
	}

	@Override
	protected List<JecnTreeBean> searchName(String name, Long projectId) {
		try {
			return ConnectionPool.getPersonAction().searchUserByName(name, projectId);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this.getJecnDialog(), JecnProperties.getValue("serverConnException"));
			log.error(JecnProperties.getValue("searchPostionException"), e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	protected void selectButPerformed(List<JecnTreeBean> list, JSearchTextField jecnField, Long id) {
		JecnDesignerCommon.setSinglePositionPeoson(list, jecnField, this.getJecnDialog(), id);
	}

}
