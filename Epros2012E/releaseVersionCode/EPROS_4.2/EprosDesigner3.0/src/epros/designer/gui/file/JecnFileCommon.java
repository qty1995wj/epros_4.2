package epros.designer.gui.file;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;

/*******************************************************************************
 * 文件管理公用方法
 * 
 * @author 2012-05-28
 * 
 */
public class JecnFileCommon {
	private static Logger log = Logger.getLogger(JecnFileCommon.class);

	/***************************************************************************
	 * 获得所有文件
	 * 
	 * @param list
	 * @return
	 */
	public static JecnTreeModel getFileTreeModel(List<JecnTreeBean> list) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.fileRoot, JecnProperties
				.getValue("fileManageTree"));
		JecnTreeCommon.addNLevelNodes(list, rootNode);
		return new JecnTreeModel(rootNode);
	}

	public static JecnTreeModel getTreeMoveModel(List<JecnTreeBean> list, List<Long> listIds) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.fileRoot, JecnProperties
				.getValue("fileManageTree"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, listIds);
		return new JecnTreeModel(rootNode);
	}

	/***************************************************************************
	 * 点击树节点的操作
	 * 
	 * @param evt
	 * @param jTree
	 * @param jecnManageDialog
	 * 
	 */
	public static void treeMousePressed(MouseEvent evt, JecnTree jTree, FileManageDialog jecnManageDialog) {
		HighEfficiencyFileTree htree = (HighEfficiencyFileTree) jTree;
		/** 点击左键 */
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			// 双击左键
			if (evt.getClickCount() == 2) {
				TreePath[] treePath = jTree.getSelectionPaths();
				if (treePath != null && treePath.length == 1) {
					JecnTreeNode node = (JecnTreeNode) treePath[0].getLastPathComponent();

					// 如果双击的节点是文件目录则展开节点，如果是文件，则打开文件
					if (node.getJecnTreeBean().getTreeNodeType().toString().equals(TreeNodeType.file.toString())) {// file:文件

						if (htree.getFlag() == 0) {
							if (JecnTreeCommon.isAuthNode(node) == 0) {
								JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
										.getValue("noPopedomOp"));
								return;
							}
							// 打开文件
							FileOpenBean fileOpenBean;
							try {
								fileOpenBean = ConnectionPool.getFileAction().openFile(node.getJecnTreeBean().getId());
								if (fileOpenBean == null) {
									JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
											.getValue("fileHasDel"));
									return;
								}
								JecnUtil.openFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
							} catch (Exception e) {
								log.error("JecnFileCommon treeMousePressed is error", e);
							}
						}
					} else {
						JecnTreeCommon.autoExpandNode(jTree, node);
					}
				}
			}
		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = jTree.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean().getTreeNodeType();
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
						// if (nodeSelect.getJecnTreeBean().getTreeNodeType()
						// .equals(TreeNodeType.toolRoot)
						// || nodeSelect.getJecnTreeBean()
						// .getTreeNodeType()
						// .equals(TreeNodeType.tool)) {
						// continue;
						// }
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean().getTreeNodeType())) {
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}
				if (listSelectNode.size() > 0) {
					FileTreeMenu menu = null;
					if (htree.getIsFileMange() == 0) {
						menu = new FileTreeMenu(jTree, listSelectNode, jecnManageDialog);
					} else if (htree.getIsFileMange() == 1) {
						menu = new FileTreeMenu(jTree, listSelectNode, htree.getFileChooseDialog());
					}
					if (listSelectNode.size() == 1) {
						jTree.setSelectionPath(listTreePaths.get(0));
						menu.setListNode(listSelectNode);
						if (listSelectNode.size() == 1
								&& !"fileRoot".equals(listSelectNode.get(0).getJecnTreeBean().getTreeNodeType()
										.toString())) {
							if (JecnDesignerCommon.isNotAbolishOrNotDelete(listSelectNode.get(0).getJecnTreeBean()
									.getId(), 1)) {
								if (listSelectNode.get(0).getJecnTreeBean().getApproveType() != 2) {
									menu.show(evt.getComponent(), evt.getX(), evt.getY());
								}
							}
						} else {
							menu.show(evt.getComponent(), evt.getX(), evt.getY());
						}

						return;
					}
					// 去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(listSelectNode, jTree);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}

						if (treePaths.length == 1) {
							jTree.setSelectionPath(treePaths[0]);
						} else {
							jTree.setSelectionPaths(treePaths);
						}
						menu.setListNode(listRemoveChild);
						if (listRemoveChild.size() == 1
								&& !"fileRoot".equals(listRemoveChild.get(0).getJecnTreeBean().getTreeNodeType()
										.toString())) {
							if (JecnDesignerCommon.isNotAbolishOrNotDelete(listRemoveChild.get(0).getJecnTreeBean()
									.getId(), 1)) {
								if (listRemoveChild.get(0).getJecnTreeBean().getApproveType() != 2) {
									menu.show(evt.getComponent(), evt.getX(), evt.getY());
								}
							}
						} else {
							menu.show(evt.getComponent(), evt.getX(), evt.getY());
						}
					}
				}

			}
		}
	}

	public static void updateFileAction(JecnTreeNode node, JTree jTree) {
		if (node == null || jTree == null) {
			throw new NullPointerException("JecnFileCommon updateFileAction is error！");
		}

		boolean isDesign = JecnTreeCommon.isDesigner(node);

		if (isDesign && !JecnTool.isBeInTask(node)) {// 如果是设计者，并且处于任务中退出
			return;
		}
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileManageDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
		fileChoose.setEidtTextFiled(false);

		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("updateFile"));
		int i = fileChoose.showSaveDialog(null);
		if (i == JFileChooser.APPROVE_OPTION) {
			File file = fileChoose.getSelectedFile();
			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSaveFileManageDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			JecnTreeBean treeBean = node.getJecnTreeBean();
			try {
				// 判断是否在其他目录中存在
				List<String> names = new ArrayList<String>();
				names.add(JecnUtil.getFileName(file.getPath()));
				String[] ns = ConnectionPool.getFileAction().validateNamefullPath(names, treeBean.getPid(), 1);
				if (ns != null && ns.length > 0) {
					// 是否删除提示框
					int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), ns[0]
							+ JecnProperties.getValue("itExistsDirectories"), null, JecnOptionPane.YES_NO_OPTION,
							JecnOptionPane.INFORMATION_MESSAGE);
					if (option == JecnOptionPane.NO_OPTION) {
						return;
					}
				}
				if (ConnectionPool.getFileAction().validateUpdateName(JecnUtil.getFileName(file.getPath()),
						treeBean.getId(), treeBean.getPid())) {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileNameHaved"));
					return;
				} else {

					JecnFileBeanT fileBean = ConnectionPool.getFileAction().getFilBeanById(treeBean.getId());
					fileBean.setFileName(JecnUtil.getFileName(file.getPath()));
					fileBean.setFileStream(JecnUtil.changeFileToByte(file.getPath()));
					// 更新文件
					ConnectionPool.getFileAction().updateFile(fileBean,
							JecnConstants.loginBean.getJecnUser().getPeopleId(), node.getJecnTreeBean().isPub(),
							isDesign);
					if(isDesign){
						node.getJecnTreeBean().setUpdate(true);
						//JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), node);
					}
					JecnTreeCommon.reNameNode(jTree, node, JecnUtil.getFileName(file.getPath()));
					JecnTreeCommon.selectNode(jTree, node);
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("updateSuccess"));
				}
			} catch (Exception e) {
				log.error("JecnFileCommon updateFileAction is error！", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}
		}
	}

	/**
	 * 文件是否能被删除
	 * 
	 * @param listIds
	 * @return
	 * @throws Exception
	 */
	public static boolean fileIsDel(List<Long> listIds) throws Exception {
		// 验证节点审批状态
		boolean isInTask = ConnectionPool.getTaskRecordAction().idsIsInTask(listIds, TreeNodeType.file);
		if (isInTask) {
			// 存在节点处于审批中，请查阅！
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("exitsNodeInTask"));
			return false;
		}
		// 验证制度文件引用文件管理中的文件 节点审批状态

		boolean idsIsInRuleTask = ConnectionPool.getTaskRecordAction().idsIsInRuleTask(listIds, TreeNodeType.file);
		if (idsIsInRuleTask) {
			// 存在节点处制度审批中，请查阅！
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("exitsNodeInRuleTask"));
			return false;
		}
		// 验证发布状态。type：0：流程；1：文件；2：制度
		if (!JecnTreeCommon.nodeValidate(1)) {
			boolean fileIsPub = ConnectionPool.getTaskRecordAction().idsIsPub(listIds, TreeNodeType.file);
			if (fileIsPub) {
				// 存在发布状态的节点，请查阅！
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("exitNodeIsPub"));
				return false;
			}
			boolean ruleFileIsPub = ConnectionPool.getTaskRecordAction().idsIsRulePubByFile(listIds, TreeNodeType.file);
			if (ruleFileIsPub) {
				// 制度文件存在发布状态的节点，请查阅！
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("exitNodeIsRulePub"));
				return false;
			}
		}
		return true;
	}

}
