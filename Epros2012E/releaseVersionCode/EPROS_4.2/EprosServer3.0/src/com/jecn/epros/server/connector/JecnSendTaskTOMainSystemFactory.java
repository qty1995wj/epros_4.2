package com.jecn.epros.server.connector;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.system.NeedThrowException;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.connector.impl.CMSTaskInfo;
import com.jecn.epros.server.connector.impl.FiberHomeTaskInfo;
import com.jecn.epros.server.connector.impl.FuYaoTaskInfo;
import com.jecn.epros.server.connector.impl.HeErTaiTaskInfo;
import com.jecn.epros.server.connector.impl.IflytekTaskInfo;
import com.jecn.epros.server.connector.impl.JinkeTaskInfo;
import com.jecn.epros.server.connector.impl.KukaTaskInfo;
import com.jecn.epros.server.connector.impl.LiBangTaskInfo;
import com.jecn.epros.server.connector.impl.LingNanTaskInfo;
import com.jecn.epros.server.connector.impl.MengNiuTaskInfo;
import com.jecn.epros.server.connector.impl.MulinsenTaskInfo;
import com.jecn.epros.server.connector.impl.Nine999TaskInfo;
import com.jecn.epros.server.connector.impl.WanHuaTaskInfo;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;

/**
 * 任务代办工厂类
 * 
 * @author xiaohu
 * 
 */
public enum JecnSendTaskTOMainSystemFactory {
	INSTANCE;
	protected static final Logger log = Logger.getLogger(JecnSendTaskTOMainSystemFactory.class);

	public void sendTask(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds, Long curPeopleId)
			throws Exception {
		JecnBaseTaskSend taskSend = getTaskSend();
		if (taskSend == null) {
			log.warn("不能发送代办，当前登录类型为：" + JecnContants.otherLoginType);
			return;
		}
		try {
			taskSend.sendTaskInfoToMainSystem(beanNew, personDao, handlerPeopleIds, curPeopleId);
		} catch (NeedThrowException e) {
			log.error("", e);
			throw e;
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private JecnBaseTaskSend getTaskSend() {
		JecnBaseTaskSend taskSend = null;
		if (JecnContants.otherLoginType == 24) {// 十所
			taskSend = new Cetc10TaskInfo();
		} else if (JecnContants.otherLoginType == 37) {// 蒙牛
			taskSend = new MengNiuTaskInfo();
		} else if (JecnContants.otherLoginType == 41) {// 木林森
			taskSend = new MulinsenTaskInfo();
		} else if (JecnContants.otherLoginType == 42) {// 招商证券
			taskSend = new CMSTaskInfo();
		} else if (JecnContants.otherLoginType == 43) {// 和而泰
			taskSend = new HeErTaiTaskInfo();
		} else if (JecnContants.otherLoginType == 45) {// 和而泰
			taskSend = new FuYaoTaskInfo();
		} else if (JecnContants.otherLoginType == 46) {// 万华
			taskSend = new WanHuaTaskInfo();
		} else if (JecnContants.otherLoginType == 47) {// 九新
			taskSend = new Nine999TaskInfo();
		} else if (JecnContants.otherLoginType == 19) {// 烽火
			taskSend = new FiberHomeTaskInfo();
		} else if (JecnContants.otherLoginType == 40) {// 烽火
			taskSend = new IflytekTaskInfo();
		} else if (JecnContants.otherLoginType == 53) {// 顾家家居
			taskSend = new KukaTaskInfo();
		}else if (JecnContants.otherLoginType == 54){//岭南
			taskSend = new LingNanTaskInfo();
		}else if (JecnContants.otherLoginType == 28) {// 立邦
			taskSend = new LiBangTaskInfo();
		} else if (JecnConfigTool.isJinKe()) {// 金科
			taskSend = new JinkeTaskInfo();
		}
		return taskSend;
	}

	/**
	 * 取消任务
	 * 
	 * @param beanNew
	 * @param personDao
	 * @param handlerPeopleIds
	 */
	public void sendTaskCancel(Long curPeopleId, JecnTaskBeanNew beanNew, JecnUser jecnUser, JecnUser createUser) {
		JecnBaseTaskSend taskSend = getTaskSend();
		if (taskSend == null) {
			return;
		}
		try {
			taskSend.sendInfoCancel(curPeopleId, beanNew, jecnUser, createUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 批量删除任务代办
	 * 
	 * @param beanNew
	 * @param personDao
	 * @param handlerPeopleIds
	 *            新代办人员集合
	 * @param cancelPeoples
	 *            取消代办的人员ID集合
	 * @throws Exception
	 */
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		if (cancelPeoples == null) {// 不存在删除人员，走正常审批(非蒙牛和十所，代办处理)
			sendTask(beanNew, personDao, handlerPeopleIds, curPeopleId);
			return;
		}
		JecnBaseTaskSend taskSend = getTaskSend();
		if (taskSend == null) {
			return;
		}
		try {
			taskSend.sendInfoCancels(beanNew, personDao, handlerPeopleIds, cancelPeoples, curPeopleId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
