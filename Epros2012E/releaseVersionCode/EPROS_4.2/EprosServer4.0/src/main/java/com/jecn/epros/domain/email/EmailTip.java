package com.jecn.epros.domain.email;

public class EmailTip {

    /**
     * 服务地址
     **/
    private String address;
    /**
     * 地址提示
     **/
    private String addressTip;
    /**
     * 密码提示
     **/
    private String passwordTip;
    /**
     * 落款
     **/
    private String inscribe;

    public String getAddressTip() {
        return addressTip;
    }

    public void setAddressTip(String addressTip) {
        this.addressTip = addressTip;
    }

    public String getPasswordTip() {
        return passwordTip;
    }

    public void setPasswordTip(String passwordTip) {
        this.passwordTip = passwordTip;
    }

    public String getInscribe() {
        return inscribe;
    }

    public void setInscribe(String inscribe) {
        this.inscribe = inscribe;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
