package epros.draw.gui.swing;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 自定义JSplitPane类
 * 
 * 能实现隐藏其中一面组件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSplitPane extends JSplitPane {
	/** JecnSplitPane或leftPanelContainPanel容器 */
	private JPanel mainPanel = null;

	/** JecnSplitPane左面/上面容器 */
	private JComponent leftPanel = null;
	/** JecnSplitPane右面/下面容器 */
	private JComponent rightPanel = null;

	/**  */
	private boolean flag = false;
	/** 分隔条将要使用的位置 */
	private int dividerLocing = 0;

	public JecnSplitPane(int newOrientation, JPanel mainPanel, JComponent leftPanel, JComponent rightPanel) {
		super(newOrientation);
		this.mainPanel = mainPanel;
		this.leftPanel = leftPanel;
		this.rightPanel = rightPanel;
		iniComponent();
	}

	private void iniComponent() {
		// 左:右=7:3
		this.setDividerLocation(0.7);
		// 当窗体变大时，左边面板获取空间,右面板不变
		this.setResizeWeight(1.0);
		// 快速展开/折叠分隔条
		this.setOneTouchExpandable(false);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		this.setBorder(null);

		this.setLeftComponent(leftPanel);
		this.setRightComponent(rightPanel);

		if (this.getUI() instanceof BasicSplitPaneUI) {// 分割条
			BasicSplitPaneUI ui = (BasicSplitPaneUI) this.getUI();
			// 无边框
			ui.getDivider().setBorder(null);
			// 默认背景颜色
			ui.getDivider().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		}
	}

	/**
	 * 
	 * 只显示左面面板
	 * 
	 */
	public void showLeftPanel() {
		if (leftPanel != this.getLeftComponent()) {// 不存在
			this.setLeftComponent(leftPanel);
		}
		if (rightPanel == this.getRightComponent()) {// 存在
			this.remove(rightPanel);
		}
		setUIDivider(0);
	}

	/**
	 * 
	 * 只显示模型面板
	 * 
	 */
	public void showRightPanel() {
		if (rightPanel != this.getRightComponent()) {// 不存在
			this.setRightComponent(rightPanel);
		}
		if (leftPanel == this.getLeftComponent()) {// 存在
			this.remove(leftPanel);
		}
		setUIDivider(0);
	}

	/**
	 * 
	 * 流程元素和模型同时显示
	 * 
	 */
	public void showBothPanel() {
		if (leftPanel != this.getLeftComponent()) {// 不存在
			this.setLeftComponent(leftPanel);
		}
		if (rightPanel != this.getRightComponent()) {// 不存在
			this.setRightComponent(rightPanel);
		}
		setUIDivider(3);

		// 设置分割线位置点
		setJecnDividerLocation();
	}

	/**
	 * 
	 * 设置splitpane的ui分隔条的大小
	 * 
	 * @param newSize
	 */
	private void setUIDivider(int newSize) {
		if (this.getUI() instanceof BasicSplitPaneUI) {// 分割条
			BasicSplitPaneUI ui = (BasicSplitPaneUI) this.getUI();
			ui.getDivider().setDividerSize(newSize);
		}
	}

	public void initdividerLocing() {
		if (this.getLeftComponent() != leftPanel || this.getRightComponent() != rightPanel
				|| !(this.getUI() instanceof BasicSplitPaneUI)) {
			return;
		}

		BasicSplitPaneUI splitPaneUI = (BasicSplitPaneUI) this.getUI();
		int splitWidth = 0;
		// 组件边框距离
		int insetSpace = 0;
		// 组件分隔条宽/高
		int dividerWidth = 0;
		// 分隔条的当前位置
		int currentLoc = splitPaneUI.getDividerLocation(this);

		if (JSplitPane.HORIZONTAL_SPLIT == this.orientation) {
			splitWidth = this.getWidth();
			insetSpace = this.getInsets().right;
			dividerWidth = splitPaneUI.getDivider().getWidth();
		} else {
			splitWidth = this.getHeight();
			insetSpace = this.getInsets().top;
			dividerWidth = splitPaneUI.getDivider().getHeight();
		}

		// 去除边框空闲区域、分隔条的宽剩下的宽
		int contentWidth = splitWidth - insetSpace - dividerWidth;
		// 分隔条将要使用的位置
		dividerLocing = contentWidth - currentLoc;
	}

	private void setJecnDividerLocation() {
		if (this.dividerLocing == 0 || !(this.getUI() instanceof BasicSplitPaneUI)) {
			return;
		}
		BasicSplitPaneUI splitPaneUI = (BasicSplitPaneUI) this.getUI();
		int splitWidth = 0;
		// 组件边框距离
		int insetSpace = 0;
		// 组件分隔条宽/高
		int dividerWidth = 0;

		if (JSplitPane.HORIZONTAL_SPLIT == this.orientation) {
			splitWidth = this.getWidth();
			insetSpace = this.getInsets().right;
			dividerWidth = splitPaneUI.getDivider().getWidth();
		} else {
			splitWidth = this.getHeight();
			insetSpace = this.getInsets().top;
			dividerWidth = splitPaneUI.getDivider().getHeight();
		}

		// 去除边框空闲区域、分隔条的宽剩下的宽
		int contentWidth = splitWidth - insetSpace - dividerWidth - dividerLocing;

		// 设置分隔条位置点
		this.setDividerLocation(contentWidth);
		this.setLastDividerLocation(contentWidth);
	}
}
