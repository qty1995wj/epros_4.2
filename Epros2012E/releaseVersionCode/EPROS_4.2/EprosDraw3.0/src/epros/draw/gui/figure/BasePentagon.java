/*
 * Pentagon.java
 * 五边形
 * Created on 2008年11月4日, 下午2:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 
 * 五边形、制度的基类
 * 
 * @author zhangxh
 * @date： 日期：Mar 23, 2012 时间：9:54:45 AM
 */
public class BasePentagon extends JecnBaseFigurePanel {
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int x5;
	private int y5;
	private int doubleLineWidth = 0;

	/** Creates a new instance of Pentagon */
	public BasePentagon(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;

		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int count = DrawCommon.convertDoubleToInt(19 * scale);
		doubleLineWidth = JecnWorkflowConstant.DOUBLE_LINE_SPACE;
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth - count;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight / 2;
			x4 = userWidth - count;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight - count;
			x4 = userWidth / 2;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight - count;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = count;
			y2 = 0;
			x3 = userWidth;
			y3 = 0;
			x4 = userWidth;
			y4 = userHeight;
			x5 = count;
			y5 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth;
			y2 = count;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
			x5 = 0;
			y5 = count;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, 0, y1, y2, y3, y4, y5, 0,userWidth,userHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		// paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		JecnPaintEnum.INTANCE.paintHandTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4, x5,
				0, y1, y2, y3, y4, y5, 0, 0, 0, userWidth, userHeight);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		JecnPaintEnum.INTANCE.paintHandTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4, x5,
				0, y1, y2, y3, y4, y5, 0, 0, 0, userWidth, userHeight);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, 0, y1, y2, y3, y4, y5, 0, 0, 0, userWidth, userHeight);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, 0, y1, y2, y3, y4, y5, 0, 3, 5,userWidth,userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3,
				x4, x5, 0, y1, y2, y3, y4, y5, 0, 0, 0,userWidth,userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3,
				x4, x5, 0, y1, y2, y3, y4, y5, 0, 3, 5,userWidth,userHeight);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, 0, y1, y2, y3, y4, y5, 0, doubleLineWidth,userWidth,userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DAndShadowsPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2,
				x3, x4, x5, 0, y1, y2, y3, y4, y5, 0, doubleLineWidth,userWidth,userHeight);
	}
}
