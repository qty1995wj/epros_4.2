/**
 * BPMServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public interface BPMServiceSoap_PortType extends java.rmi.Remote {
    public com.jecn.webservice.jinke.taskPub.Item[] t(java.lang.String con) throws java.rmi.RemoteException;

    /**
     * 判断子表某列是否含有指定的值
     */
    public boolean containValue(java.lang.String instanceId, java.lang.String tableName, java.lang.String colName) throws java.rmi.RemoteException;
    public java.lang.String getAllUnFilishWorkItems(java.lang.String startTime, java.lang.String endTime, int startIndex, int endIndex) throws java.rmi.RemoteException;

    /**
     * 获取所有待阅流程
     */
    public java.lang.String getAllUnreadWorkItems(java.lang.String startTime, java.lang.String endTime, int startIndex, int endIndex) throws java.rmi.RemoteException;
    public com.jecn.webservice.jinke.taskPub.ServiceResult callback(java.lang.String instanceId, java.lang.String strStatusCode, java.lang.String childTableName, java.lang.String mapRelation, java.lang.String sheetCode) throws java.rmi.RemoteException;
    public java.lang.Object tx(java.lang.String instanceId, java.lang.String strStatusCode, java.lang.String childTableName, java.lang.String mapRelation, java.lang.String sheetCode) throws java.rmi.RemoteException;

    /**
     * 流程实例状态
     */
    public int getInstanceState(java.lang.String instanceId) throws java.rmi.RemoteException;
    public boolean editWorkFlowValues(java.lang.String userCode, java.lang.String instanceId, com.jecn.webservice.jinke.taskPub.DataItemParam[] val) throws java.rmi.RemoteException;

    /**
     * 获取2个日期之间的天数
     */
    public double getDays(java.util.Calendar startDate, java.util.Calendar endDate) throws java.rmi.RemoteException;

    /**
     * 获取发起流程模板
     */
    public java.lang.String getWorkfowNodeByUser(java.lang.String userCode, boolean showFavorite, boolean isMobile, java.lang.String parentCode, java.lang.String searchKey) throws java.rmi.RemoteException;

    /**
     * 获取用户未完成的任务总数
     */
    public int getUserUnfinishedWorkItemCount() throws java.rmi.RemoteException;

    /**
     * 获取用户已完成的任务总数
     */
    public int getUserFinishedWorkItemCount() throws java.rmi.RemoteException;

    /**
     * 获取用户待阅任务总数
     */
    public int getUserUnReadWorkItemCount() throws java.rmi.RemoteException;

    /**
     * 获取用户已阅任务总数
     */
    public int getUserReadedWorkItemCount() throws java.rmi.RemoteException;

    /**
     * 查询用户的已办
     */
    public com.jecn.webservice.jinke.taskPub.WorkItemViewModel[] getFinishWorkItems(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String workflowCode, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 查询用户的待办
     */
    public com.jecn.webservice.jinke.taskPub.WorkItemViewModel[] getUnFinishWorkItems(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String workflowCode, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 查询用户的已阅任务
     */
    public com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getReadWorkItems(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String workflowCode, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 查询用户的待阅任务
     */
    public com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getUnReadWorkItems(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String workflowCode, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 提交(已阅)工作任务
     */
    public boolean submitWorkItem(java.lang.String workItemId, java.lang.String commentText) throws java.rmi.RemoteException;

    /**
     * 驳回工作任务
     */
    public boolean returnWorkItem(java.lang.String workItemId, java.lang.String commentText) throws java.rmi.RemoteException;

    /**
     * 强制结束流程
     */
    public boolean finishInstance(java.lang.String instanceId) throws java.rmi.RemoteException;

    /**
     * 强制结束流程YS
     */
    public boolean finishInstanceYS(java.lang.String instanceId) throws java.rmi.RemoteException;

    /**
     * 激活流程
     */
    public boolean activeInstance(java.lang.String instanceId) throws java.rmi.RemoteException;

    /**
     * 激活指定的活动节点
     */
    public boolean activeToken(java.lang.String instanceId, java.lang.String activityCode, java.lang.String[] participants) throws java.rmi.RemoteException;

    /**
     * 取消指定的活动节点
     */
    public boolean cancelToken(java.lang.String instanceId, java.lang.String activityCode) throws java.rmi.RemoteException;

    /**
     * 取回工作任务
     */
    public boolean retrieveWorkItem(java.lang.String workitemId) throws java.rmi.RemoteException;

    /**
     * 启动H3流程实例，设置主键数据项的值
     */
    public com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflowWithKey(java.lang.String instanceName, java.lang.String workflowCode, java.lang.String userAlias, boolean finishStart, java.lang.String keyName, java.lang.String keyValue) throws java.rmi.RemoteException;

    /**
     * 启动H3运营管理平台流程实例
     */
    public com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflow_S(java.lang.String instanceName, java.lang.String workflowCode, java.lang.String userCode, java.lang.String finishStart, java.lang.String paramValues) throws java.rmi.RemoteException;

    /**
     * 启动H3流程实例
     */
    public com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflow(java.lang.String instanceName, java.lang.String workflowCode, java.lang.String userCode, boolean finishStart, com.jecn.webservice.jinke.taskPub.DataItemParam[] paramValues) throws java.rmi.RemoteException;

    /**
     * ERP流程更新预估金额，不验证
     */
    public java.lang.String setItemValue_Amount(java.lang.String userCode, java.lang.String instanceId, java.lang.String amount) throws java.rmi.RemoteException;

    /**
     * ERP流程更新附件，不验证
     */
    public java.lang.String setItemValue_noauth(java.lang.String userCode, java.lang.String instanceId, java.lang.String att_name, java.lang.String att_url) throws java.rmi.RemoteException;

    /**
     * 获取项目成本管理人员
     */
    public java.lang.String[] getQycbgly(java.lang.String project_Guid) throws java.rmi.RemoteException;

    /**
     * 设置单个流程数据项的值
     */
    public boolean setItemValue(java.lang.String userCode, java.lang.String bizObjectSchemaCode, java.lang.String bizObjectId, java.lang.String keyName, java.lang.Object keyValue) throws java.rmi.RemoteException;

    /**
     * 设置批量流程数据项的值
     */
    public boolean setItemValues(java.lang.String userCode, java.lang.String bizObjectSchemaCode, java.lang.String bizObjectId, com.jecn.webservice.jinke.taskPub.DataItemParam[] keyValues) throws java.rmi.RemoteException;

    /**
     * 输出日志至引擎服务器
     */
    public void writeLog(java.lang.String message) throws java.rmi.RemoteException;

    /**
     * 获取参与者(H3定制)
     */
    public java.lang.String[] getParticals(java.lang.String instanceId, java.lang.String colName, java.lang.String roleCode) throws java.rmi.RemoteException;
    public java.lang.String getColFilterContext(java.lang.String roleCode) throws java.rmi.RemoteException;
    public java.lang.String[] testSerial(java.lang.String xmlString) throws java.rmi.RemoteException;

    /**
     * 通过系统编码查询用户的已办
     */
    public java.lang.String getFinishWorkItemsBySystemID(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String sysGID, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 通过系统编码查询用户的待办
     */
    public java.lang.String getUnFinishWorkItemsBySystemID(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String sysGID, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 查询用户的已办
     */
    public java.lang.String getDmFinishWorkItems(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String workflowCode, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 获取用户未完成的任务总数
     */
    public com.jecn.webservice.jinke.taskPub.BpmRetrunValue getUnFinishCountBySystemID(java.lang.String userCode, java.lang.String sysGID) throws java.rmi.RemoteException;

    /**
     * 通过系统编码查询我的流程
     */
    public java.lang.String myInstanceBySystemID(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String sysGID, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 通过系统编码查询我的流程
     */
    public java.lang.String getMyInstance(java.lang.String userCode, java.util.Calendar startTime, java.util.Calendar endTime, int startIndex, int endIndex, java.lang.String instanceName) throws java.rmi.RemoteException;

    /**
     * 获取用户已完成的任务总数
     */
    public com.jecn.webservice.jinke.taskPub.BpmRetrunValue getFinishCountBySystemID(java.lang.String userCode, java.lang.String sysGID) throws java.rmi.RemoteException;

    /**
     * 获取所有已办流程
     */
    public java.lang.String getAllFinishedWorkItems(java.lang.String startTime, java.lang.String endTime, int startIndex, int endIndex) throws java.rmi.RemoteException;

    /**
     * 获取所有人员的发出流程
     */
    public java.lang.String getAllSendWorkItems(java.lang.String startTime, java.lang.String endTime, int startIndex, int endIndex) throws java.rmi.RemoteException;

    /**
     * 自动化运行流程
     */
    public java.lang.String automateResult(java.lang.String instanceId) throws java.rmi.RemoteException;
}
