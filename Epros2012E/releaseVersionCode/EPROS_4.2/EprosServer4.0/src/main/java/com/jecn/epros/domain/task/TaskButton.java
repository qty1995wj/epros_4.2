package com.jecn.epros.domain.task;

/**
 * 任务界面的按钮
 */
public class TaskButton {


    private String name;
    private int operation;

    public TaskButton(String name, int operation) {
        this.name = name;
        this.operation = operation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOperation() {
        return operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }


}
