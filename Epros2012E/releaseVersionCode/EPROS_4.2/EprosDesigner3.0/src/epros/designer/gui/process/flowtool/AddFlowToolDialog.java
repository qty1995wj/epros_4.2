package epros.designer.gui.process.flowtool;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;

/**
 * 新建支持工具
 * 
 * @author fuzhh Apr 9, 2013
 * 
 */
public class AddFlowToolDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddFlowToolDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddFlowToolDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("add");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("contentC");
	}

	@Override
	public void saveData() {
		// 向支持工具表中添加数据
		JecnFlowSustainTool jecnFlowSustainTool = new JecnFlowSustainTool();
		jecnFlowSustainTool.setFlowSustainToolShow(getName());
		jecnFlowSustainTool.setPreFlowSustainToolId(pNode.getJecnTreeBean().getId());
		jecnFlowSustainTool.setSortId(JecnTreeCommon.getMaxSort(pNode));
		try {
			// 执行数据库添加
			Long id = ConnectionPool.getFlowTool().addSustainTool(jecnFlowSustainTool, JecnUtil.getUserId());
			// 向树节点添加新建的支持工具节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.tool);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);

			this.dispose();
		} catch (Exception e) {
			log.error("AddFlowToolDialog saveData is error", e);
		}

	}

	@Override
	public boolean validateNodeRepeat(String name) {

		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.tool);
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
