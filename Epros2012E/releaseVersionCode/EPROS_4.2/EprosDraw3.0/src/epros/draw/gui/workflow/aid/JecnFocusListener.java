package epros.draw.gui.workflow.aid;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * 
 * 编辑框焦点事件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFocusListener implements FocusListener {

	@Override
	public void focusGained(FocusEvent e) {

	}

	/**
	 * 
	 * 失去焦点
	 * 
	 */
	@Override
	public void focusLost(FocusEvent e) {
		if (e.getSource() instanceof JecnTextArea) {
			// 编辑框
			JecnTextArea textArea = (JecnTextArea) e.getSource();
			textArea.isEditFail();
		}
	}
}
