package com.jecn.epros.server.webBean.popedom;

import java.util.HashMap;
import java.util.Map;

public class ProcessListBean {
	/** 流程名称 */
	private String flowName;
	/** 流程编号 */
	private String flowNumber;
	/** 流程IP */
	private Long flowId;
	/** 流程KPI */
	private Map<Long, String> flowKPIMap = new HashMap<Long, String>();
	/** 有效期 */
	private Integer expiry;
	/** 活动ID 和活动名称 */
	private Map<Long, String> processActiveMap = new HashMap<Long, String>();
	/** 流程责任人 **/
	private String resPeopleName;
	/** 流程监护人名称 */
	private String guardianName;
	/** 流程拟稿人 */
	private String draftPerson;
	/** 版本号 */
	private String versionId;
	/** 发布时间 */
	private String pubDate;
	/** 是否及时优化 0空 ,1是 ,2否 */
	private int optimization;
	/** 处于什么阶段 0是待建，1是审批，2是发布 */
	private int flowState;

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public Integer getExpiry() {
		return expiry;
	}

	public void setExpiry(Integer expiry) {
		this.expiry = expiry;
	}

	public Map<Long, String> getFlowKPIMap() {
		return flowKPIMap;
	}

	public void setFlowKPIMap(Map<Long, String> flowKPIMap) {
		this.flowKPIMap = flowKPIMap;
	}

	public Map<Long, String> getProcessActiveMap() {
		return processActiveMap;
	}

	public void setProcessActiveMap(Map<Long, String> processActiveMap) {
		this.processActiveMap = processActiveMap;
	}

	public String getFlowNumber() {
		return flowNumber;
	}

	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}

	public String getResPeopleName() {
		return resPeopleName;
	}

	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getDraftPerson() {
		return draftPerson;
	}

	public void setDraftPerson(String draftPerson) {
		this.draftPerson = draftPerson;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public int getOptimization() {
		return optimization;
	}

	public void setOptimization(int optimization) {
		this.optimization = optimization;
	}

	public int getFlowState() {
		return flowState;
	}

	public void setFlowState(int flowState) {
		this.flowState = flowState;
	}

}
