package com.jecn.epros.service.org;

import com.jecn.epros.domain.org.*;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;

import java.util.List;
import java.util.Map;

public interface OrgService {
    /**
     * 树节点展开
     *
     * @param map
     * @return
     */
    public List<TreeNode> findChildOrgs(Map<String, Object> map);


    /**
     * 部门概括信息
     *
     * @param map
     * @return
     */
    public OrgBaseInfoBean findOrgBaseInfoBean(Map<String, Object> map);

    /**
     * 岗位概括信息
     *
     * @param map
     * @return
     */
    public PosBaseInfoBean findPosBaseInfoBean(Map<String, Object> map);

    /**
     * 岗位角色信息
     *
     * @param posId
     * @return
     */
    public  List<PosRelatedRole>  getPosRelatedRole(Long posId);

    /**
     * 搜索组织
     *
     * @param map
     * @param searchBean
     * @return
     */
    public List<SearchOrgResultBean> searchOrg(Map<String, Object> map);

    /**
     * 搜索组织 总数
     *
     * @param map
     * @return
     * @
     */
    public int searchOrgTotal(Map<String, Object> map);

    /**
     * 搜索组织
     *
     * @param map
     * @return
     */
    public List<SearchPosResultBean> searchPos(Map<String, Object> map);

    /**
     * 搜索岗位统计数据
     *
     * @param map
     * @return
     */
    public Inventory searchPosStatistics(Map<String, Object> map);

    /**
     * 搜索岗位组统计数据
     *
     * @param map
     * @return
     */
    public Inventory searchPosGroupStatistics(Map<String, Object> map);

    /**
     * 搜索组织 总数
     *
     * @param map
     * @return
     * @
     */
    public int searchPosTotal(Map<String, Object> map);

    /**
     * 搜索岗位组 总数
     *
     * @param map
     * @return
     * @
     */
    public int searchPosGroupTotal(Map<String, Object> map);


    public Inventory getInventory(Map<String,Object> paramMap);


    public Inventory getPosInventory(Map<String,Object> paramMap);




}
