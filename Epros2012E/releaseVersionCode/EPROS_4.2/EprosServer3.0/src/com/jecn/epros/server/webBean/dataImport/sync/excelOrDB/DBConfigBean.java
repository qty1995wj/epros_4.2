package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

public class DBConfigBean extends AbstractConfigBean {

	/** 数据库方言 */
	private String dialect = null;
	/** 数据库驱动 */
	private String driver = null;
	/** 数据库访问地址 */
	private String url = null;
	/** 数据库登录名 */
	private String username = null;
	/** 数据库登录密码 */
	private String password = null;
	/** 部门查询sql */
	private String deptSql = null;
	/** 用户查询sql */
	private String userSql = null;
	/** 岗位查询sql */
	private String posSql = null;
	/** 查询存储过程 sql */
	private String procedureSql = null;
	/** 工号和与账号对应 查询sql */
	private String numOrAdNumSql = null;

	/*** 基准岗位sql ****/
	private String basePosSql = null;

	/** 岗位组集合 */
	private String posGroupSql = null;

	/** 数据库同步校验 */

	private String validateSql = null;

	private String updateSql = null;

	public String getBasePosSql() {
		return basePosSql;
	}

	public void setBasePosSql(String basePosSql) {
		this.basePosSql = basePosSql;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeptSql() {
		return deptSql;
	}

	public void setDeptSql(String deptSql) {
		this.deptSql = deptSql;
	}

	public String getUserSql() {
		return userSql;
	}

	public void setUserSql(String userSql) {
		this.userSql = userSql;
	}

	public String getPosSql() {
		return posSql;
	}

	public void setPosSql(String posSql) {
		this.posSql = posSql;
	}

	public String getNumOrAdNumSql() {
		return numOrAdNumSql;
	}

	public void setNumOrAdNumSql(String numOrAdNumSql) {
		this.numOrAdNumSql = numOrAdNumSql;
	}

	public String getProcedureSql() {
		return procedureSql;
	}

	public void setProcedureSql(String procedureSql) {
		this.procedureSql = procedureSql;
	}

	public String getPosGroupSql() {
		return posGroupSql;
	}

	public void setPosGroupSql(String posGroupSql) {
		this.posGroupSql = posGroupSql;
	}

	public String getValidateSql() {
		return validateSql;
	}

	public void setValidateSql(String validateSql) {
		this.validateSql = validateSql;
	}

	public String getUpdateSql() {
		return updateSql;
	}

	public void setUpdateSql(String updateSql) {
		this.updateSql = updateSql;
	}
}
