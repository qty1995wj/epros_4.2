package epros.draw.gui.operationConfig;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.draw.gui.operationConfig.ConfigFileInstructionsDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 相关标准 表
 * 
 * @author 2012-07-13
 * 
 */
public class OrderTable extends JTable {

	private Vector<Vector<String>> content;

	private String standardTitle;

	public OrderTable(String standardTitle) {
		this.standardTitle = standardTitle;
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	public void setTableModel() {
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		columnModel.getColumn(2).setMinWidth(100);
		columnModel.getColumn(2).setMaxWidth(150);
	}

	public Vector<Vector<String>> getContent() {
		return content;
	}

	public void setContent(Vector<Vector<String>> content) {
		this.content = content;
	}

	public OrderTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(standardTitle);
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("type"));
		return new OrderTableMode(getContent(), title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class OrderTableMode extends DefaultTableModel {
		public OrderTableMode(Vector<Vector<String>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}
}
