package com.jecn.epros.domain.system;

import com.jecn.epros.domain.process.Inventory;

import java.util.List;
import java.util.Map;

public class RoleManageBean {

    private int total = 0;

    private List<RoleWebInfoBean> data;

    private Inventory roleData;
    /*         ADMIN_NUM : 管理员数量 总数
     *         DESIGN_NUM : 流程设计专家 总数
     *         DESIGN_NUM_USE:流程设计专家 使用数
     *         KEY_TYPE : 秘钥类型 永久 or 临时
     *         KEY_EXPIRY_TIME : 秘钥到期时间

     * */
    private Map<String, String> roleMap;


    public Map<String, String> getRoleMap() {
        return roleMap;
    }

    public void setRoleMap(Map<String, String> roleMap) {
        this.roleMap = roleMap;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<RoleWebInfoBean> getData() {
        return data;
    }

    public void setData(List<RoleWebInfoBean> data) {
        this.data = data;
    }

    public Inventory getRoleData() {
        return roleData;
    }

    public void setRoleData(Inventory roleData) {
        this.roleData = roleData;
    }
}
