package com.jecn.epros.server.bean.rule;



/**
 * 制度内容表
 * 
 * @author lihongliang
 * 
 */
public class RuleContent implements java.io.Serializable {
	private Long id;//主键ID
	private Long ruleTitleId;//制度标题ID
	private String contentStr;//内容
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRuleTitleId() {
		return ruleTitleId;
	}
	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}
	public String getContentStr() {
		if(!isNullOrEmtryTrim(contentStr)){
			return contentStr;
		}else{
			return "";
		}
	}
	public void setContentStr(String contentStr) {
		this.contentStr = contentStr;
	}
	
	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	  boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}
}
