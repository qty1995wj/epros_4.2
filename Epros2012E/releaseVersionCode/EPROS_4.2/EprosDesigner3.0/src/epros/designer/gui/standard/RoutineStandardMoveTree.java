package epros.designer.gui.standard;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

public class RoutineStandardMoveTree extends JecnRoutineTree {
	private Logger log = Logger.getLogger(RoutineStandardMoveTree.class);

	public RoutineStandardMoveTree(List<Long> listIds) {
		super(listIds);
	}
	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
//			list = ConnectionPool.getStandardAction().getAllStandardDir(JecnConstants.projectId);
		} catch (Exception e) {
			log.error("RoutineStandardMoveTree getTreeModel is error", e);
		}
		
		// 根节点 "标准"
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.standardRoot,JecnProperties.getValue("standard"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
