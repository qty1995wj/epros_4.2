package com.jecn.epros.domain;

import java.util.List;

public class JecnFlowOrgImage implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private JecnFlowOrg orgnization;// 组织ID

    private Long figureId;// 主键ID
    private String figureType;// 元素类型
    private String figureText;// 元素名称
    private Long startFigure;// 连接线输出的图像元素的FigureId
    private Long endFigure;// 连接线输入的图像元素的FigureId
    private Long poLongx;// 图像元素开始点的X
    private Long poLongy;// 图像元素开始点的Y
    private Long width;// 图像元素的宽
    private Long height;// 图像元素的高
    private Long fontSize;// 线的粗细大小 或图形字体的大小
    private String BGColor;// 图像元素的填充色
    private String fontColor;// 字体颜色
    private Long linkOrgId;// 关联组织ID或岗位figureId
    private String lineColor;// 连接线的颜色
    private String fontPosition;// 字体位置
    private Long havaShadow;// 是否有阴影
    private String circumgyrate;// 元素的选择角度
    private String bodyLine;// 线的样式
    private String bodyColor;// 边框颜色
    private Long isErect;// 字体竖排
    private String figureImageId;// 临时的唯一标示
    private Long orderIndex;// 层级
    private Long fontBody;// 字体加粗
    private Long frameBody;// 边框加粗
    private String fontType;// 字体类型
    private Long sortId;// 岗位排序
    private String fileAccessPath;// 获得文件访问路径
    private Integer is3DEffect;// 是否有3D效果
    private Integer fillEffects;// 填充效果:0为未填充,1为单色效果,2未双色效果
    private Integer lineThickness;// 线的深度
    private String figureNumberId;// 崗位編號
    private String orgNumberId;// 组织编号
    private List<JecnLineSegmentDep> lineSegmentDepList = null;// 线段集合
    private String shadowColor; // 阴影颜色
    private String startFigureUUID;// 开始点元素UUID
    private String endFigureUUID;// 结束点元素UUID

    public String getStartFigureUUID() {
        return startFigureUUID;
    }

    public void setStartFigureUUID(String startFigureUUID) {
        this.startFigureUUID = startFigureUUID;
    }

    public String getEndFigureUUID() {
        return endFigureUUID;
    }

    public void setEndFigureUUID(String endFigureUUID) {
        this.endFigureUUID = endFigureUUID;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public JecnFlowOrg getOrgnization() {
        return orgnization;
    }

    public void setOrgnization(JecnFlowOrg orgnization) {
        this.orgnization = orgnization;
    }

    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public String getFigureType() {
        return figureType;
    }

    public void setFigureType(String figureType) {
        this.figureType = figureType;
    }

    public String getFigureText() {
        return figureText;
    }

    public void setFigureText(String figureText) {
        this.figureText = figureText;
    }

    public Long getStartFigure() {
        return startFigure;
    }

    public void setStartFigure(Long startFigure) {
        this.startFigure = startFigure;
    }

    public Long getEndFigure() {
        return endFigure;
    }

    public void setEndFigure(Long endFigure) {
        this.endFigure = endFigure;
    }

    public Long getPoLongx() {
        return poLongx;
    }

    public void setPoLongx(Long poLongx) {
        this.poLongx = poLongx;
    }

    public Long getPoLongy() {
        return poLongy;
    }

    public void setPoLongy(Long poLongy) {
        this.poLongy = poLongy;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getFontSize() {
        return fontSize;
    }

    public void setFontSize(Long fontSize) {
        this.fontSize = fontSize;
    }

    public String getBGColor() {
        return BGColor;
    }

    public void setBGColor(String color) {
        BGColor = color;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public Long getLinkOrgId() {
        return linkOrgId;
    }

    public void setLinkOrgId(Long linkOrgId) {
        this.linkOrgId = linkOrgId;
    }

    public String getLineColor() {
        return lineColor;
    }

    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }

    public String getFontPosition() {
        return fontPosition;
    }

    public void setFontPosition(String fontPosition) {
        this.fontPosition = fontPosition;
    }

    public Long getHavaShadow() {
        return havaShadow;
    }

    public void setHavaShadow(Long havaShadow) {
        this.havaShadow = havaShadow;
    }

    public String getCircumgyrate() {
        return circumgyrate;
    }

    public void setCircumgyrate(String circumgyrate) {
        this.circumgyrate = circumgyrate;
    }

    public String getBodyLine() {
        return bodyLine;
    }

    public void setBodyLine(String bodyLine) {
        this.bodyLine = bodyLine;
    }

    public String getBodyColor() {
        return bodyColor;
    }

    public void setBodyColor(String bodyColor) {
        this.bodyColor = bodyColor;
    }

    public Long getIsErect() {
        return isErect;
    }

    public void setIsErect(Long isErect) {
        this.isErect = isErect;
    }

    public String getFigureImageId() {
        return figureImageId;
    }

    public void setFigureImageId(String figureImageId) {
        this.figureImageId = figureImageId;
    }

    public Long getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Long orderIndex) {
        this.orderIndex = orderIndex;
    }

    public Long getFontBody() {
        return fontBody;
    }

    public void setFontBody(Long fontBody) {
        this.fontBody = fontBody;
    }

    public Long getFrameBody() {
        return frameBody;
    }

    public void setFrameBody(Long frameBody) {
        this.frameBody = frameBody;
    }

    public String getFontType() {
        return fontType;
    }

    public void setFontType(String fontType) {
        this.fontType = fontType;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public String getFileAccessPath() {
        return fileAccessPath;
    }

    public void setFileAccessPath(String fileAccessPath) {
        this.fileAccessPath = fileAccessPath;
    }

    public Integer getIs3DEffect() {
        return is3DEffect;
    }

    public void setIs3DEffect(Integer is3DEffect) {
        this.is3DEffect = is3DEffect;
    }

    public Integer getFillEffects() {
        return fillEffects;
    }

    public void setFillEffects(Integer fillEffects) {
        this.fillEffects = fillEffects;
    }

    public Integer getLineThickness() {
        return lineThickness;
    }

    public void setLineThickness(Integer lineThickness) {
        this.lineThickness = lineThickness;
    }

    public String getFigureNumberId() {
        return figureNumberId;
    }

    public void setFigureNumberId(String figureNumberId) {
        this.figureNumberId = figureNumberId;
    }

    public String getOrgNumberId() {
        return orgNumberId;
    }

    public void setOrgNumberId(String orgNumberId) {
        this.orgNumberId = orgNumberId;
    }

    public List<JecnLineSegmentDep> getLineSegmentDepList() {
        return lineSegmentDepList;
    }

    public void setLineSegmentDepList(List<JecnLineSegmentDep> lineSegmentDepList) {
        this.lineSegmentDepList = lineSegmentDepList;
    }

}
