package com.jecn.epros.server.dao.dataImport.match.impl;

import java.util.List;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.dataImport.match.IMatchUserDataCheckDao;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;

/**
 * 岗位匹配人员同步 验证DAO实现
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jun 3, 2013 时间：2:04:11 PM
 */
public class MatchUserDataCheckDaoImpl extends AbsBaseDao<String, String>
		implements IMatchUserDataCheckDao {
	/**
	 * 
	 * 人员数据行间校验，是否存在重复数据
	 */
	@Override
	public List<UserMatchBean> userSameDataCheck() {
		String sql = "with tmp_user as (select MU.Login_Name,"
				+ "                                            MU.true_name,"
				+ "                                            MU.Position_Num,"
				+ "                                            MU.Email,"
				+ "                                            MU.email_type,"
				+ "                                            MU.Phone"
				+ "                                       from JECN_MATCH_USER_BEAN MU"
				+ "                                      group by MU.Login_Name,"
				+ "                                               MU.true_name,"
				+ "                                               MU.Position_Num,"
				+ "                                               MU.Email,"
				+ "                                               MU.email_type,"
				+ "                                               MU.Phone"
				+ "                                     having count(MU.Login_Name) > 1)"
				+ " select MU.*" + "  from JECN_MATCH_USER_BEAN MU"
				+ " where MU.Login_Name in ( select Login_Name from tmp_user)";

		return getSession().createSQLQuery(sql).addEntity(UserMatchBean.class)
				.list();
	}

	/**
	 * 人员登录名称相同数据验证 登录名称相同，存在岗位编号为空
	 * 
	 * @return
	 */
	@Override
	public List<UserMatchBean> loginNameSameAndPosNullCheck() {
		String sql = "with tmp_user as ("
				+ "  select tu.Login_Name"
				+ "                        from JECN_MATCH_USER_BEAN tu"
				+ "                       group by tu.Login_Name"
				+ "                      having count(tu.Login_Name) > 1"
				+ "  )"
				+ " select *"
				+ "   from JECN_MATCH_USER_BEAN"
				+ ""
				+ "  where Login_Name in (select tu.Login_Name"
				+ "                         from JECN_MATCH_USER_BEAN tu, tmp_user t"
				+ "                        where tu.Login_Name = t.Login_Name"
				+ "                          and tu.Position_Num is null)";

		return getSession().createSQLQuery(sql).addEntity(UserMatchBean.class)
				.list();
	}

	/**
	 * 一人多岗位情况 ，真实姓名不同
	 * 
	 * @return
	 */
	@Override
	public List<UserMatchBean> trueNameNoSameCheck() {
		String sql = "with tmp_user as (select tu.Login_Name"
				+ "                                  from JECN_MATCH_USER_BEAN tu"
				+ "                                 group by tu.Login_Name"
				+ "                                having count(tu.Login_Name) > 1)"
				+ " select *"
				+ "   from JECN_MATCH_USER_BEAN"
				+ "  where Login_Name in"
				+ "        (select tu.Login_Name"
				+ "           from JECN_MATCH_USER_BEAN tu"
				+ "          inner join tmp_user t on tu.Login_Name = t.Login_Name"
				+ "                               and exists"
				+ "          (select 1"
				+ "                                      from JECN_MATCH_USER_BEAN tu1"
				+ "                                     where tu.Login_Name = tu1.Login_Name"
				+ "                                       and tu.Position_Num <>"
				+ "                                           tu1.Position_Num"
				+ "                                       and tu.true_name <> tu1.true_name))";

		return getSession().createSQLQuery(sql).addEntity(UserMatchBean.class)
				.list();
	}

	/**
	 * 一人多岗位情况 ，邮箱地址和邮箱类型不同
	 * 
	 * @return
	 */
	@Override
	public List<UserMatchBean> emailNoSameCheck() {
		String sql = "with tmp_user as (select tu.Login_Name"
				+ "                                  from JECN_MATCH_USER_BEAN tu"
				+ "                                 group by tu.Login_Name"
				+ "                                having count(tu.Login_Name) > 1)"
				+ ""
				+ "  select tu1.*"
				+ "    from JECN_MATCH_USER_BEAN tu1, JECN_MATCH_USER_BEAN tu2, tmp_user tu3"
				+ "   where tu1.Login_Name = tu3.Login_Name"
				+ "     and tu2.Login_Name = tu3.Login_Name"
				+ "     and tu1.Position_Num <> tu2.Position_Num"
				+ "     and tu1.email <> tu2.email"
				+ "     and tu1.email is not null"
				+ "     and tu2.email is not null"
				+ "  union"
				+ "  select tu1.*"
				+ "    from JECN_MATCH_USER_BEAN tu1, JECN_MATCH_USER_BEAN tu2, tmp_user tu3"
				+ "   where tu1.Login_Name = tu3.Login_Name"
				+ "     and tu2.Login_Name = tu3.Login_Name"
				+ "     and tu1.Position_Num <> tu2.Position_Num"
				+ "     and (tu1.email is null and tu2.email is not null)"
				+ "  union"
				+ "  select tu1.*"
				+ "    from JECN_MATCH_USER_BEAN tu1, JECN_MATCH_USER_BEAN tu2, tmp_user tu3"
				+ "   where tu1.Login_Name = tu3.Login_Name"
				+ "     and tu2.Login_Name = tu3.Login_Name"
				+ "     and tu1.Position_Num <> tu2.Position_Num"
				+ "     and (tu1.email is not null and tu2.email is null)"
				+ "  union"
				+ "  select tu1.*"
				+ "    from JECN_MATCH_USER_BEAN tu1, JECN_MATCH_USER_BEAN tu2, tmp_user tu3"
				+ "   where tu1.Login_Name = tu3.Login_Name"
				+ "     and tu2.Login_Name = tu3.Login_Name"
				+ "     and tu1.Position_Num <> tu2.Position_Num"
				+ "     and tu1.email_type <> tu2.email_type"
				+ "     and tu1.email_type is not null"
				+ "     and tu2.email_type is not null"
				+ "  union"
				+ "  select tu1.*"
				+ "    from JECN_MATCH_USER_BEAN tu1, JECN_MATCH_USER_BEAN tu2, tmp_user tu3"
				+ "   where tu1.Login_Name = tu3.Login_Name"
				+ "     and tu2.Login_Name = tu3.Login_Name"
				+ "     and tu1.Position_Num <> tu2.Position_Num"
				+ "     and tu1.email_type is null"
				+ "     and tu2.email_type is not null"
				+ "  union"
				+ "  select tu1.*"
				+ "    from JECN_MATCH_USER_BEAN tu1, JECN_MATCH_USER_BEAN tu2, tmp_user tu3"
				+ "   where tu1.Login_Name = tu3.Login_Name"
				+ "     and tu2.Login_Name = tu3.Login_Name"
				+ "     and tu1.Position_Num <> tu2.Position_Num"
				+ "     and tu1.email_type is not null"
				+ "     and tu2.email_type is null" + "" + "" + "" + "";

		return getSession().createSQLQuery(sql).addEntity(UserMatchBean.class)
				.list();
	}

	/**
	 * 一人多岗位情况 ，联系方式不同
	 * 
	 * @return
	 */
	@Override
	public List<UserMatchBean> phoneNoSameCheck() {
		String sql = "with tmp_pos as ("
				+ "    select tu1.login_name,tu1.phone"
				+ "    from JECN_MATCH_USER_BEAN tu1"
				+ "   inner join JECN_MATCH_USER_BEAN tu on tu.login_name = tu1.login_name"
				+ "                                  and tu.position_num <> tu1.position_num"
				+ "" + "  )" + "  select tu.*"
				+ "    from JECN_MATCH_USER_BEAN tu"
				+ "   inner join tmp_pos tp on tu.login_name = tp.login_name"
				+ "                        and tu.phone <> tp.phone"
				+ "  union" + "  select tu.*"
				+ "    from JECN_MATCH_USER_BEAN tu"
				+ "   inner join tmp_pos tp on tu.login_name = tp.login_name"
				+ "                        and tu.phone is null"
				+ "                        and tp.phone is not null"
				+ "                        union" + "  select tu.*"
				+ "    from JECN_MATCH_USER_BEAN tu"
				+ "   inner join tmp_pos tp on tu.login_name = tp.login_name"
				+ "                        and tu.phone is not null"
				+ "                        and tp.phone is  null";

		return getSession().createSQLQuery(sql).addEntity(UserMatchBean.class)
				.list();
	}

	/**
	 * 一人多岗位情况，岗位编号不能相同
	 * 
	 * @return List<UserMatchBean>
	 */
	@Override
	public List<UserMatchBean> sameLoginNameAndSamePos() {
		String sql = "with tmp_user as (select tu.Login_Name, tu.Position_Num"
				+ "                                   from JECN_MATCH_USER_BEAN tu"
				+ "                                  group by tu.Login_Name, tu.Position_Num"
				+ "                                 having count(tu.Login_Name) > 1)"
				+ "  select *"
				+ "    from JECN_MATCH_USER_BEAN"
				+ "   where Login_Name in (select tu.Login_Name"
				+ "                        from JECN_MATCH_USER_BEAN tu, tmp_user t"
				+ "                       where tu.Login_Name = t.Login_Name"
				+ "                         and tu.Position_Num = t.Position_Num)";

		return getSession().createSQLQuery(sql).addEntity(UserMatchBean.class)
				.list();

	}

	/**
	 * 部门行间校验，部门编号不唯一
	 * 
	 * @return List<DeptMatchBean>
	 */
	@Override
	public List<DeptMatchBean> hasSameDeptNum() {
		String sql = "with tmp_dept as (select MD.dept_num"
				+ "                                      from JECN_MATCH_DEPT_BEAN MD"
				+ "                                     group by MD.DEPT_NUM"
				+ "                                    having count(MD.DEPT_NUM) > 1)"
				+ "select JMD.*" + "  from JECN_MATCH_DEPT_BEAN JMD"
				+ " where JMD.dept_num in (select dept_num from tmp_dept)";

		return getSession().createSQLQuery(sql).addEntity(DeptMatchBean.class)
				.list();
	}

	/**
	 * 同一部门下部门名称不能重复
	 * 
	 * @return List<DeptMatchBean>
	 */
	@Override
	public List<DeptMatchBean> hasSameDeptName() {
		String sql = "with tmp_dept as ("
				+ "  select td1.p_dept_num, td1.dept_name"
				+ "    from JECN_MATCH_DEPT_BEAN td1"
				+ "   group by td1.p_dept_num, td1.dept_name"
				+ "  having count(td1.dept_name) > 1" + ")" + ""
				+ "select td.*" + "  from JECN_MATCH_DEPT_BEAN td"
				+ " where exists (select 1" + "          from tmp_dept"
				+ "         where td.dept_name = dept_name"
				+ "           and td.p_dept_num = p_dept_num)";

		return getSession().createSQLQuery(sql).addEntity(DeptMatchBean.class)
				.list();
	}

	/**
	 * 部门内不能出现重复岗位
	 * 
	 * @return List<UserMatchBean>
	 */
	@Override
	public List<PosMatchBean> deptHashSamePosName() {
		String sql = "select JP.*"
				+ "  from jecn_match_pos_bean JP"
				+ " inner join jecn_match_pos_bean JP1 on JP.DEPT_NUM = JP1.DEPT_NUM"
				+ "                                   and JP.ACT_POS_NUM <> JP1.ACT_POS_NUM"
				+ "                                   and JP.ACT_POS_NAME = JP1.ACT_POS_NAME";

		return getSession().createSQLQuery(sql).addEntity(PosMatchBean.class)
				.list();
	}

	/**
	 * 岗位编号不唯一
	 * 
	 * @return
	 * @return List<UserMatchBean>
	 */
	@Override
	public List<PosMatchBean> posNumNotOnly() {
		String sql = "with tmp_pos as ("
				+ " select JP.ACT_POS_NUM"
				+ "  from jecn_match_pos_bean JP GROUP BY JP.ACT_POS_NUM having count(JP.ACT_POS_NUM)>1"
				+ ")"
				+ "select jp1.*"
				+ "  from jecn_match_pos_bean jp1"
				+ " where jp1.act_pos_num in (select tp.ACT_POS_NUM from tmp_pos tp)";

		return getSession().createSQLQuery(sql).addEntity(PosMatchBean.class)
				.list();
	}

	/**
	 * 岗位所属部门编号在部门集合中不存在
	 * 
	 * @return
	 * @return List<UserMatchBean>
	 */
	@Override
	public List<PosMatchBean> posNumRefDeptNumNoExists() {
		String sql = "select jp.*"
				+ "  from jecn_match_pos_bean jp"
				+ " where not exists"
				+ " (select 1 from jecn_match_dept_bean jd where jp.dept_num = jd.dept_num)";
		return getSession().createSQLQuery(sql).addEntity(PosMatchBean.class)
				.list();
	}

	/**
	 * 岗位编号相同，岗位名称必须相同
	 * 
	 * @return
	 */
	@Override
	public List<PosMatchBean> posNameNotSameList() {
		String sql = "select jp.*"
				+ "  from jecn_match_pos_bean jp"
				+ " inner join jecn_match_pos_bean jp1 on jp.dept_num = jp1.dept_num"
				+ "                                 and jp.act_pos_num = jp1.act_pos_num"
				+ "                                 and jp.act_pos_name <> jp1.act_pos_name";
		return getSession().createSQLQuery(sql).addEntity(PosMatchBean.class)
				.list();
	}

	/**
	 * 岗位编号相同，岗位所属部门必须相同
	 * 
	 * @return
	 */
	@Override
	public List<PosMatchBean> posNumSameAndDeptSameList() {
		String sql = "with tmp_pos as ("
				+ "  select jp.act_pos_num,jp.dept_num"
				+ "    from jecn_match_pos_bean jp"
				+ "   group by jp.act_pos_num,jp.dept_num" + "" + ")"
				+ "select tu.*" + "  from jecn_match_pos_bean tu"
				+ " where tu.act_pos_num in (select act_pos_num"
				+ "                            from tmp_pos"
				+ "                           group by act_pos_num"
				+ "                          having count(act_pos_num) > 1)";

		return getSession().createSQLQuery(sql).addEntity(PosMatchBean.class)
				.list();
	}
}
