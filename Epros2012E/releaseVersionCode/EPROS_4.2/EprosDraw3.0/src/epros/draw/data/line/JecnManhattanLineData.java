package epros.draw.data.line;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.JecnXYData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.system.JecnSystemData;
import epros.draw.util.DrawCommon;

/**
 * 
 * 连接线：N个小线段的集合
 * 
 * 小线段集合是从开始点到结束点顺序存储的，即集合中第一个元素一定是开始点，最后一个元素一定是结束点
 * 
 * @author ZHOUXY
 * 
 */
public class JecnManhattanLineData extends JecnBaseLineData {

	/** 开始端对应图形ID */
	private long startId = -1;
	/** 结束端对应图形ID */
	private long endId = -1;

	/** 开始端对应图形的UUID */
	private String startFigureUUID = null;
	/** 结束端对应图形的UUID */
	private String endFigureUUID = null;

	/** 开始端对应图形的编辑点类型 */
	private EditPointType startEditPointType = null;
	/** 结束端对应图形的编辑点类型 */
	private EditPointType endEditPointType = null;
	/** true：显示双箭头 */
	private boolean isTwoArrow = false;

	/** 小线段数据集合 */
	private List<JecnLineSegmentData> lineSegmentDataList = new ArrayList<JecnLineSegmentData>();

	private Point textPoint;

	private JecnFigureData startFigureData;
	private JecnFigureData endFigureData;

	public JecnManhattanLineData(MapElemType mapElemType) {
		super(mapElemType);
		// 初始化图形属性
		JecnSystemData.initCurrFlowElementData(this);
	}

	/**
	 * 
	 * 获取连接线的开始点
	 * 
	 * @return JecnXYData
	 */
	public JecnXYData getOriginalStartFigurePoint() {
		if (lineSegmentDataList.size() == 0) {
			return new JecnXYData();
		}
		return lineSegmentDataList.get(0).getOriginalStartXYData();
	}

	/**
	 * 获取连接线下数据层下开始点
	 * 
	 * @return Point
	 */
	public Point getZoomStartPoint() {
		return new Point(DrawCommon.convertDoubleToInt(lineSegmentDataList.get(0).getOriginalStartXYData().getX()
				* this.getWorkflowScale()), DrawCommon.convertDoubleToInt(lineSegmentDataList.get(0)
				.getOriginalStartXYData().getY()
				* this.getWorkflowScale()));
	}

	/**
	 * 获取连接线数据层下结束点
	 * 
	 * @return
	 */
	public Point getZoomEndPoint() {
		return new Point(DrawCommon.convertDoubleToInt(lineSegmentDataList.get(lineSegmentDataList.size() - 1)
				.getOriginalEndXYData().getX()
				* this.getWorkflowScale()), DrawCommon.convertDoubleToInt(lineSegmentDataList.get(
				lineSegmentDataList.size() - 1).getOriginalEndXYData().getY()
				* this.getWorkflowScale()));
	}

	/**
	 * 
	 * 获取连接线的结束点
	 * 
	 * @return JecnXYData 结束点
	 */
	public JecnXYData getOriginalEndFigurePoint() {
		if (lineSegmentDataList.size() == 0) {
			return new JecnXYData();
		}
		return lineSegmentDataList.get(lineSegmentDataList.size() - 1).getOriginalEndXYData();
	}

	public long getStartId() {
		return startId;
	}

	public void setStartId(long startId) {
		this.startId = startId;
	}

	public long getEndId() {
		return endId;
	}

	public void setEndId(long endId) {
		this.endId = endId;
	}

	public EditPointType getStartEditPointType() {
		return startEditPointType;
	}

	public void setStartEditPointType(EditPointType startEditPointType) {
		this.startEditPointType = startEditPointType;
	}

	public EditPointType getEndEditPointType() {
		return endEditPointType;
	}

	public void setEndEditPointType(EditPointType endEditPointType) {
		this.endEditPointType = endEditPointType;
	}

	public List<JecnLineSegmentData> getLineSegmentDataList() {
		return lineSegmentDataList;
	}

	public void setLineSegmentDataList(List<JecnLineSegmentData> lineSegmentDataList) {
		this.lineSegmentDataList = lineSegmentDataList;
	}

	public String getStartFigureUUID() {
		return startFigureUUID;
	}

	public void setStartFigureUUID(String startFigureUUID) {
		this.startFigureUUID = startFigureUUID;
	}

	public String getEndFigureUUID() {
		return endFigureUUID;
	}

	public void setEndFigureUUID(String endFigureUUID) {
		this.endFigureUUID = endFigureUUID;
	}

	@Override
	public JecnManhattanLineData newInstance() {
		return new JecnManhattanLineData(mapElemType);
	}

	@Override
	public JecnManhattanLineData clone() {
		return (JecnManhattanLineData) this.currClone();
	}

	public boolean isTwoArrow() {
		return isTwoArrow;
	}

	public void setTwoArrow(boolean isTwoArrow) {
		this.isTwoArrow = isTwoArrow;
	}

	public Point getTextPoint() {
		return textPoint;
	}

	public void setTextPoint(Point textPoint) {
		this.textPoint = textPoint;
	}

	@Override
	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnManhattanLineData)
				|| !(destFlowElementData instanceof JecnManhattanLineData)) {
			return;
		}

		// 复制流程元素属性
		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnManhattanLineData src = (JecnManhattanLineData) srcFlowElementData;
		JecnManhattanLineData dest = (JecnManhattanLineData) destFlowElementData;

		// 开始端对应图形ID
		dest.setStartId(src.getStartId());
		// 结束端对应图形ID
		dest.setEndId(src.getEndId());

		// 开始端对应图形的UUID
		dest.setStartFigureUUID(src.getStartFigureUUID());
		// 结束端对应图形的UUID
		dest.setEndFigureUUID(src.getEndFigureUUID());

		// 开始端对应图形的编辑点类型
		dest.setStartEditPointType(src.getStartEditPointType());
		// 结束端对应图形的编辑点类型
		dest.setEndEditPointType(src.getEndEditPointType());
		// 小线段数据集合
		dest.getLineSegmentDataList().clear();
		dest.setTwoArrow(src.isTwoArrow());
		dest.setTextPoint(src.getTextPoint());
		for (JecnLineSegmentData lineSegmentData : src.getLineSegmentDataList()) {
			dest.getLineSegmentDataList().add(lineSegmentData.clone());
		}
	}

	public JecnFigureData getStartFigureData() {
		return startFigureData;
	}

	public void setStartFigureData(JecnFigureData startFigureData) {
		this.startFigureData = startFigureData;
	}

	public JecnFigureData getEndFigureData() {
		return endFigureData;
	}

	public void setEndFigureData(JecnFigureData endFigureData) {
		this.endFigureData = endFigureData;
	}
}
