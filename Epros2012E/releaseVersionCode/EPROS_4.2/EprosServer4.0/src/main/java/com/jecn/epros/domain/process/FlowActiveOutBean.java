package com.jecn.epros.domain.process;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.file.FileBaseBean;

import java.util.ArrayList;
import java.util.List;

public class FlowActiveOutBean {
    private FileBaseBean FileBaseBean;

    private List<FileBaseBean> listTemplet;
    //新版模板
    private List<LinkResource> mouldLinks;
    //新版样例
    private List<LinkResource> templetLinks;

    public FileBaseBean getFileBaseBean() {
        return FileBaseBean;
    }

    public void setFileBaseBean(FileBaseBean fileBaseBean) {
        FileBaseBean = fileBaseBean;
    }

    public List<FileBaseBean> getListTemplet() {
        return listTemplet;
    }

    public void setListTemplet(List<FileBaseBean> listTemplet) {
        this.listTemplet = listTemplet;
    }


    public List<LinkResource> getMouldLinks() {
        return mouldLinks;
    }

    public List<LinkResource> getLinks() {
        return mouldLinks;
    }

    public void setMouldLinks(List<LinkResource> mouldLinks) {
        this.mouldLinks = mouldLinks;
    }

    public List<LinkResource> getTempletLinks() {
        return templetLinks;
    }

    public void setTempletLinks(List<LinkResource> templetLinks) {
        this.templetLinks = templetLinks;
    }
}
