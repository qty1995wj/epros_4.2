package com.jecn.epros.domain.task;

/**
 * 任务业务处理，代码参数非法处理类
 */
public class TaskIllegalArgumentException extends IllegalArgumentException {

    public TaskIllegalArgumentException(String msg) {
        super(msg);
    }
}
