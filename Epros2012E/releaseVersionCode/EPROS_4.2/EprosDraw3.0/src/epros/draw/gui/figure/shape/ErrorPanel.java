package epros.draw.gui.figure.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 流程面板错误信息panel
 * 
 * @author Administrator
 * @date： 日期：Dec 15, 2012 时间：2:34:06 PM
 */
public class ErrorPanel extends JPanel {

	protected Color bodyColor = Color.RED;
	Color c = Color.BLACK;
	BasicStroke basicStroke = null;
	/** 自定义画笔使用样式：执行呈现和图像处理服务的类 */
	private RenderingHints renderingHints = null;
	private String pText = "";

	public ErrorPanel() {
		this.setSize(330, 40);
		// 自定义画笔使用样式：执行呈现和图像处理服务的类
		renderingHints = JecnUIUtil.getRenderingHints();
		this.setFont(new Font("宋体", 5, 12));
	}

	public void paintComponent(Graphics g) {

		int x1, y1, x2, y2; // 定义坐标属性
		x1 = 0;
		y1 = 0;
		x2 = this.getWidth() - 1;
		y2 = this.getHeight() - 1;
		Graphics2D g2d = (Graphics2D) g; // 定义画笔

		RenderingHints originalRenderingHints = g2d.getRenderingHints();
		// 获取原始绘画颜色
		Color originalColor = g2d.getColor();

		// 指定RenderingHints
		g2d.setRenderingHints(renderingHints);
		// 颜色
		g2d.setColor(Color.RED);
		// 绘制图形
		int textLength = g.getFontMetrics(this.getFont()).stringWidth(pText);
		int textHeight = g.getFontMetrics(this.getFont()).getHeight();
		String[] splitStrings = pText.split("\n");
		for (int i = 0; i < splitStrings.length; i++) {
			textLength = g.getFontMetrics(this.getFont()).stringWidth(splitStrings[i]);
			g.drawString(splitStrings[i], this.getWidth() / 2 - textLength / 2, this.getHeight() / 2 + 3 * textHeight
					/ 4 - ((splitStrings.length * textHeight) / 2) + textHeight * i);
		}
		g2d.setFont(this.getFont());
		float[] dash2 = { 5.0f };
		g2d.setPaint(bodyColor);
		g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, dash2, 0.0f));
		g2d.drawRect(x1, y1, x2, y2);
		// g2d.setColor(c);

		// 还原原始RenderingHints对象
		g2d.setRenderingHints(originalRenderingHints);
		// 还原原始绘画颜色
		g2d.setColor(originalColor);

	}

	public void setText(String pText) {
		this.pText = pText;
		if (pText == null) {
			this.setToolTipText("");
		} else {
			this.setToolTipText(pText);
		}
	}

	public String getText() {
		return pText;
	}

	public void repaintPanel() {
		this.repaint();
		JecnDrawMainPanel.getMainPanel().getWorkflow().getErrorPanelList().add(this);
		JecnDrawMainPanel.getMainPanel().getWorkflow().add(this);
		JecnDrawMainPanel.getMainPanel().getWorkflow().setLayer(this, 1000);
	}
}
