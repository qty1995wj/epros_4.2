package epros.draw.gui.top.toolbar;

import java.awt.GridBagConstraints;
import java.awt.event.ItemEvent;

import javax.swing.JCheckBox;
import javax.swing.SpinnerNumberModel;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.event.JecnFormatActionProcess;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.top.toolbar.JecnSpinnerPanel.AutoSizeType;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏大类：格式类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFormatToolBarItemPanel extends JecnAstractToolBarItemPanel {

	/** 模板维护 */
	private JecnToolbarContentButton attributeSetBtn = null;
	/** 自动编号 ：默认不自动编号 */
	private JecnToolbarContentButton antoNumBtn = null;
	/** 角色移动 ：显示、隐藏 默认隐藏 */
	private JCheckBox roleMoveCheckBox = null;
	/** 网格 ：显示、隐藏 默认显示 */
	private JCheckBox gridCheckBox = null;
	/** 横分割线: 显示、隐藏 默认显示 */
	private JCheckBox pLinesCheckBox = null;
	/** 竖分割线 ：显示、隐藏 默认隐藏 */
	private JCheckBox vLineCheckBox = null;
	/** 画图面板宽自动放大控件 */
	private JecnSpinnerPanel autoWidthSpinner = null;
	/** 画图面板高自动放大控件 */
	private JecnSpinnerPanel autoHeightSpinner = null;

	/** 格式面板处理对象 */
	private JecnFormatActionProcess formatActionProcess = null;

	/** 合并泳池 */
	private JCheckBox megerModelBox = null;

	public JecnFormatToolBarItemPanel(JecnToolBarPanel toolBarPanel) {
		super(toolBarPanel);
		initchildComponents();
	}

	@Override
	protected void initTitleType() {
		this.titleType = ToolBarElemType.formatTitle;
	}

	@Override
	protected void createChildComponents() {

		// 模板维护
		attributeSetBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.formatAttriSet), ToolBarElemType.formatAttriSet);
		// 自动编号
		antoNumBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.formatAntoNum), ToolBarElemType.formatAntoNum);

		// 角色移动 ：显示、隐藏 默认隐藏
		roleMoveCheckBox = new JCheckBox(JecnResourceUtil.getJecnResourceUtil()
				.getValue(ToolBarElemType.formatRoleMove));
		// 网格 ：显示、隐藏 默认显示
		gridCheckBox = new JCheckBox(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.formatGrid));
		// 横分割线: 显示、隐藏 默认显示
		pLinesCheckBox = new JCheckBox(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.formatPLine));
		// 竖分割线 ：显示、隐藏 默认隐藏
		vLineCheckBox = new JCheckBox(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.formatVLine));
		// 画图面板宽自动放大控件
		autoWidthSpinner = new JecnSpinnerPanel(new SpinnerNumberModel(100, 100, 10000, 100), JecnResourceUtil
				.getJecnResourceUtil().getValue(ToolBarElemType.formatAutoWidth), AutoSizeType.width);
		// 画图面板高自动放大控件
		autoHeightSpinner = new JecnSpinnerPanel(new SpinnerNumberModel(100, 100, 10000, 100), JecnResourceUtil
				.getJecnResourceUtil().getValue(ToolBarElemType.formatAutoHeigth), AutoSizeType.heigth);

		megerModelBox = new JCheckBox(JecnResourceUtil.getJecnResourceUtil().getValue("lockPool"));

		// 格式面板处理对象
		formatActionProcess = new JecnFormatActionProcess();

		// 背景
		roleMoveCheckBox.setBackground(this.getBackground());
		gridCheckBox.setBackground(this.getBackground());
		pLinesCheckBox.setBackground(this.getBackground());
		vLineCheckBox.setBackground(this.getBackground());
		megerModelBox.setBackground(this.getBackground());
		// 边框
		this.setBorder(JecnUIUtil.getTootBarBorder());

		// 默认选中
		this.gridCheckBox.setSelected(JecnSystemStaticData.isShowGrid());
		this.pLinesCheckBox.setSelected(JecnSystemStaticData.isShowParallelLines());
		this.vLineCheckBox.setSelected(JecnSystemStaticData.isShowVerticalLine());
		this.megerModelBox.setSelected(JecnSystemStaticData.isShowModel());
		// 动作命令
		this.gridCheckBox.setActionCommand(JecnActionCommandConstants.TOOBAR_FORMAT_GRID);
		this.pLinesCheckBox.setActionCommand(JecnActionCommandConstants.TOOBAR_FORMAT_PARALLELINES);
		this.vLineCheckBox.setActionCommand(JecnActionCommandConstants.TOOBAR_FORMAT_VERTICALINE);
		this.megerModelBox.setActionCommand(JecnActionCommandConstants.TOOBAR_FORMAT_MODEL);
		// 角色活动动作命令
		this.roleMoveCheckBox.setActionCommand("roleMobile");

		// 事件
		this.gridCheckBox.addItemListener(toolBarPanel);
		this.pLinesCheckBox.addItemListener(toolBarPanel);
		this.vLineCheckBox.addItemListener(toolBarPanel);
		this.roleMoveCheckBox.addItemListener(toolBarPanel);
		megerModelBox.addItemListener(toolBarPanel);
	}

	@Override
	protected void layoutChildComponents() {

		int index = 0;
		GridBagConstraints c = null;
		if (JecnDesignerProcess.getDesignerProcess().isAdmin()
				|| JecnDesignerProcess.getDesignerProcess().isSecondAdmin()) {
			// 模板维护
			c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0(), 0, 0);
			this.add(attributeSetBtn.getJToolBar(), c);
			index++;
		}
		// 角色移动 ：显示、隐藏 默认隐藏
		c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(roleMoveCheckBox, c);
		index++;
		// 网格 ：显示、隐藏 默认显示
		c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(gridCheckBox, c);

		index = 0;
		// 自动编号
		c = new GridBagConstraints(index, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(antoNumBtn.getJToolBar(), c);
		index++;
		// 横分割线: 显示、隐藏 默认显示
		c = new GridBagConstraints(index, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(pLinesCheckBox, c);
		index++;
		// 竖分割线 ：显示、隐藏 默认隐藏
		c = new GridBagConstraints(index, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(vLineCheckBox, c);
		index++;
		// 画图面板宽自动放大控件
		c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(autoWidthSpinner, c);
		// 画图面板高自动放大控件
		c = new GridBagConstraints(index, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(autoHeightSpinner, c);
		index++;
		c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(this.megerModelBox, c);
	}

	/**
	 * 
	 * 单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		this.formatActionProcess.itemStateChanged(e);
	}

	public JCheckBox getRoleMoveCheckBox() {
		return roleMoveCheckBox;
	}

	public void setRoleMoveCheckBox(JCheckBox roleMoveCheckBox) {
		this.roleMoveCheckBox = roleMoveCheckBox;
	}

	/**
	 * 
	 * 设置角色移动隐藏
	 * 
	 */
	public void setRoleMoveCheckBoxFalse() {
		if (roleMoveCheckBox.isSelected()) {
			roleMoveCheckBox.setSelected(false);
		}
	}
}
