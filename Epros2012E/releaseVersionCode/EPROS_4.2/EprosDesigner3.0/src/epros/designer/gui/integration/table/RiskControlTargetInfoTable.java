package epros.designer.gui.integration.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.integration.JecnControlTarget;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 *  选择控制目标====控制目标数据集合显示Table
 * 2013-11-08
 *
 */
public class RiskControlTargetInfoTable extends JecnTable  {
	private List<JecnControlTarget> controlTargetList = new ArrayList<JecnControlTarget>();
	
	public RiskControlTargetInfoTable(List<JecnControlTarget> controlTargetList){
		this.controlTargetList = controlTargetList;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		//设置第一列宽度为0
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}
	
	@Override
	public JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add("riskId"); //风险点ID
		title.add("riskNum");//风险点编号
		title.add(JecnProperties.getValue("controlTarget"));
		return new JecnTableModel(title,getContent(controlTargetList));
	}
	public Vector<Vector<String>> getContent(List<JecnControlTarget> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (int i = 0; i<list.size();i++) {
				JecnControlTarget jecnControlTarget = list.get(i);
				Vector<String> data = new Vector<String>();
				data.add(jecnControlTarget.getId().toString());
				data.add(String.valueOf(jecnControlTarget.getRiskId()));
				data.add(jecnControlTarget.getRiskNum());
				data.add(jecnControlTarget.getDescription());
				
				content.add(data);
			}
		}
		return content;
	}
	@Override
	public int[] gethiddenCols() {
		return new int[] { 0,1,2 };
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

}
