package epros.draw.gui.figure.shape;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.BaseFlowLevelFigure;

/**
 * 
 * 流程等级图形 二级
 * 
 * @author ZHANGXH
 *
 */
public class FlowLevelFigureTwo extends BaseFlowLevelFigure {

	public FlowLevelFigureTwo(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
		intLevel = 2;
	}
}
