package epros.tray;

import org.apache.log4j.Logger;

import epros.tray.JecnStartD2.ResultEnum;

/**
 * 
 * 对外入口
 * 
 * D2启动或停止处理类
 * 
 * @author zhouxy
 * 
 */
public class JecnProcessD2 {
	private static Logger log = Logger.getLogger(JecnProcessD2.class);
	public void processEprosD2(boolean isStart) {
		if (isStart) {// 启动
			try{
				JecnStartD2 d2 = new JecnStartD2();
				ResultEnum  res=d2.startEprosD2();
				if(res==null){
					return;
				}else{
					log.error("JecnProcessD2 is error:"+res);
				}
			}catch(Exception e){
				log.error("JecnProcessD2 is error:",e);
			}
			System.exit(0);
		} else {// 停止
			/*JecnStopD2 d2 = new JecnStopD2();
			d2.stopEprosD2();*/
		}
	}
}
