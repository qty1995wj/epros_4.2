package com.jecn.epros.server.action.designer.project.impl;

import com.jecn.epros.server.action.designer.project.ICurProjectAction;
import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.service.project.ICurProjectService;

/**
 * 设置主项目
 * @param projectId
 * @return
 * @throws Exception
 */

public class CurProjectActionImpl implements ICurProjectAction {

	private ICurProjectService curProjectService;
	
	public ICurProjectService getCurProjectService() {
		return curProjectService;
	}


	public void setCurProjectService(ICurProjectService curProjectService) {
		this.curProjectService = curProjectService;
	}


	@Override
	public boolean updateCurProject(Long projectId) throws Exception {
		return curProjectService.updateCurProject(projectId);
	}


	@Override
	public JecnCurProject getJecnCurProjectById()
			throws Exception {
		return curProjectService.getJecnCurProjectById();
	}

}
