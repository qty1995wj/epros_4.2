package com.jecn.epros.server.webBean.dataImport.match;

import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;



/***
 * 部门临时表
 * @author zhangjie
 * 20120208
 */
public class DeptMatchBean extends AbstractBaseBean {
	/**部门ID */
	private Long id;
	
	/**部门编码 */
	private String deptNum;
	
	/**部门名称 */
	private String deptName;
	
	/**上级部门编码 */
	private String perDeptNum;
	
	/**项目ID */
	private Long projectId;
	
//	/**标识位   0：添加，1：更新，2：删除*/
//	private int deptFlag;
//	
//	/**错误信息*/
//	private String error;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
    
	public String getPerDeptNum() {
		return perDeptNum;
	}

	public void setPerDeptNum(String perDeptNum) {
		this.perDeptNum = perDeptNum;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	@Override
	/**
	 * 
	 * 返回属性信息
	 * 
	 * @return
	 */
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 部门编号
		strBuf.append("部门编号:");
		strBuf.append(this.getDeptNum());
		strBuf.append(MatchConstant.EMTRY_SPACE);

		// 部门名称
		strBuf.append("部门名称:");
		strBuf.append(this.getDeptName());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 上级部门编号
		strBuf.append("上级部门编号:");
		strBuf.append(this.getPerDeptNum());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 错误信息
		strBuf.append("错误信息:");
		strBuf.append(this.getError());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}

//	public int getDeptFlag() {
//		return deptFlag;
//	}
//
//	public void setDeptFlag(int deptFlag) {
//		this.deptFlag = deptFlag;
//	}
//
//	public String getError() {
//		return error;
//	}
//
//	public void setError(String error) {
//		this.error = error;
//	}
	
}
