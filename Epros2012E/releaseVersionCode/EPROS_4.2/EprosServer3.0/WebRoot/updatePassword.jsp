<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>密码修改</title>
<link rel="stylesheet" type="text/css"
	href="ext/resources/css/ext-all.css" />
<script type="text/javascript" src="ext/adapter/ext/ext-base.js" charset="UTF-8"></script>
<script type="text/javascript" src="ext/ext-all.js" charset="UTF-8"></script>
<s:if test="#session.country=='US'">
</s:if><s:else>
<script type="text/javascript"
	src="js/epros_zh_CN.js" charset="UTF-8"></script>
	<script type="text/javascript"
	src="ext/ext-lang-zh_CN.js" charset="UTF-8"></script>
</s:else>
<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/global.css" rel="stylesheet" type="text/css" />
		<link href="css/login.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" language="javascript">
		function checkStrLength(str, minLen, maxLen) {
			var lengthstr = getLength(str);
			if (lengthstr > maxLen) {
				alert("\u8f93\u5165\u5b57\u7b26\u4e32\u7684\u957f\u5ea6\u4e0d\u80fd\u8d85\u8fc7" + maxLen + "\u4f4d");
				return false;
			} else {
				if (lengthstr < minLen) {
					alert("\u8f93\u5165\u5b57\u7b26\u4e32\u7684\u957f\u5ea6\u4e0d\u80fd\u4f4e\u4e8e" + minLen + "\u4f4d");
					return false;
				}
			}
			return true;
		}
		
		function getLength(str){
			return str.replace(/[^\x00-\xFF]/g,'**').length;
		}
				
		
		function formSubmit(){
			document.form1.submit();
		 }
		function closeWindow(){
			window.location="login.jsp";
		 }
	    </script>

</head>
<body bgcolor="#D3D3D3">
	    <input type="hidden" id="EnterANewPassword" value='<s:text name="plInNewPwd"/>'/>
	    <input type="hidden" id="TheNewPasswordTwiceInconsistent" value= '<s:text name="twopwdnosame"/>'/>
	    <input type="hidden" value= "<s:property value="oldPwd" />" id = "hidOldPwd" name="hidOldPwd"/>
			<table width="100%" border="0" align="center" cellpadding="0"
				cellspacing="0" >
				<tr height="125">
				</tr>
				<tr>
					<td valign="top">
						<table width="100%" border="0" align="center" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="35%" valign="top" height="200">
								</td>
								<td valign="top">
									<div style="text-align: center;margin: auto;padding-top:10px;">
										<div id="float" style="width:95%;">
											<div>
												<img src="images/icon08.jpg" hspace="10" border="0" align="absmiddle" />
												<span class="t4"><s:text name="updatePassword" />
												</span>
											</div>
											<div>
												<form name="form1" action="updateLoginPwd.action" method="post">
													<table width="95%"border="0" align="center"
														cellpadding="0" cellspacing="0">
														<tr>
															<td>
																&nbsp;
															</td>
															<td>
																&nbsp;
															</td>
														</tr>
														<tr align="center">
														 <s:text name="erropwdWar" />
														</tr>
														<tr>
															<td width="160" height="30" align="center"
																bgcolor="#E8E8E8" class="t5">
																<s:text name="originalPasswordC" />
															</td>
															<td bgcolor="#E8E8E8">
																<input id="oldPassword" name="oldPassword"
																	type="password" class="input01" />
																	<span style="color: red;">*</span>
															</td>
														</tr>
														<tr>
															<td bgcolor="#E8E8E8"></td>
															<td align="center"
																bgcolor="#E8E8E8"  class="t5">
<%--																<span id="oldPwdPerf" ></span>--%>
																<s:fielderror>
																	<s:param>oldPwdPerf</s:param>
																</s:fielderror>
															</td>
														</tr>
														<tr>
															<td width="160" height="30" align="center"
																bgcolor="#E8E8E8" class="t5">
																<s:text name="theNewPasswordC" />
															</td>
															<td bgcolor="#E8E8E8">
																<input id="newPassword" name="newPassword"
																	type="password" class="input01" />
																	<span style="color: red;">*</span>
															</td>
														</tr>
														<tr>
															<td bgcolor="#E8E8E8"></td>
															<td align="center"
																bgcolor="#E8E8E8" class="t5">
<%--																<span style="color: red;" id="newPwdPerf"></span>--%>
																<s:fielderror>
																	<s:param>newPwdPerf</s:param>
																</s:fielderror>
															</td>
														</tr>
														<tr>
															<td width="160" height="30" align="center"
																bgcolor="#E8E8E8" class="t5">
																<s:text name="confirmPasswordC" />
															</td>
															<td bgcolor="#E8E8E8">
																<input id="reNewPassword" name="reNewPassword"
																	type="password" class="input01" />
																	<span style="color: red;">*</span>
															</td>
														</tr>
														<tr>
															<td bgcolor="#E8E8E8"></td>
															<td align="center"
																bgcolor="#E8E8E8" class="t5">
<%--																<span style="color: red;" id=""></span>--%>
																<s:fielderror>
																	<s:param>reNewPwdPerf</s:param>
																</s:fielderror>
															</td>
														</tr>
														<tr>
															<td height="40" colspan="2" align="center">
																<img onclick="formSubmit();" style="cursor: pointer"
																	src="images/button-ok.jpg"
																	width="65" height="25" hspace="8" border="0"
																	align="absmiddle" />
																<img onclick="closeWindow();" style="cursor: pointer"
																	src="images/button_cancel.jpg"
																	width="65" height="25" hspace="8" border="0"
																	align="absmiddle" />
															</td>
														</tr>
													</table>
												</form>
											</div>
										</div>
									</div>
								</td>
								<td width="35%" valign="top" height="200">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="190">
				</tr>
			</table>
	</body>
</html>