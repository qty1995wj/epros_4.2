package epros.draw.gui.workflow.grid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;

import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 画图面板绘制网格类
 * 
 * 通过放大倍数以及画图面板的大小进行绘制
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShapeGridProcess {
	/** 画图面板的宽 */
	private double width = 0.0D;
	/** 画图面板的高 */
	private double height = 0.0D;

	public JecnShapeGridProcess() {

	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 绘制画图面板网格
	 * 
	 * @param paramGraphics
	 *            Graphics 画笔
	 * @param multiple
	 *            double 放大倍数
	 * @param width
	 *            double画图面板宽
	 */
	public void drawWorkflowGride(Graphics paramGraphics, double multiple,
			double width, double height) {
		if (paramGraphics == null || height <= 1.0D || width <= 1.0D) {// 宽、高小于等于1.0
			return;
		}

		this.width = width;
		this.height = height;

		// 画笔
		Graphics2D g2D = (Graphics2D) paramGraphics;
		// 备份
		Stroke stroke = g2D.getStroke();
		Color color = g2D.getColor();

		// 设置画笔宽
		g2D.setStroke(JecnUIUtil.getGirdLineBasicStroke());
		// 设置画笔颜色
		g2D.setColor(JecnUIUtil.getGridColor());

		// 绘制网格
		drawGride(g2D, multiple);

		// 还原备份
		g2D.setStroke(stroke);
		g2D.setColor(color);
	}

	/**
	 * 
	 * 绘制网格
	 * 
	 * @param paramGraphics2D
	 *            Graphics2D 画笔
	 * @param multiple
	 *            double 放大倍数
	 */
	private void drawGride(Graphics2D paramGraphics2D, double multiple) {

		// 创建画几何图形对象
		GeneralPath generalPath = new GeneralPath();

		// 获取放大的单元格值
		float cellSize = (float) (JecnWorkflowConstant.CELL_SIZE * multiple);
		if (cellSize <= 1.0F) {
			return;
		}

		float width = (float) this.width;
		float height = (float) this.height;
		// 行标题
		float widthSize = cellSize;
		while (widthSize <= height) {
			generalPath.moveTo(0.0F, widthSize - 1.0F);
			generalPath.lineTo(width, widthSize - 1.0F);
			widthSize += cellSize;
		}
		// 列标题
		float heightSize = cellSize;
		while (heightSize <= width) {
			generalPath.moveTo(heightSize - 1.0F, 0.0F);
			generalPath.lineTo(heightSize - 1.0F, height);
			heightSize += cellSize;
		}

		// 绘制
		paramGraphics2D.draw(generalPath);
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
}
