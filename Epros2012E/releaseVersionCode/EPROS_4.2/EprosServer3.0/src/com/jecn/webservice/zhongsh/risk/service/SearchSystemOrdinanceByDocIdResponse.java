
package com.jecn.webservice.zhongsh.risk.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�����ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchSystemOrdinanceByDocIdResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchSystemOrdinanceByDocIdResult"
})
@XmlRootElement(name = "SearchSystemOrdinanceByDocIdResponse")
public class SearchSystemOrdinanceByDocIdResponse {

    @XmlElement(name = "SearchSystemOrdinanceByDocIdResult")
    protected String searchSystemOrdinanceByDocIdResult;

    /**
     * ��ȡsearchSystemOrdinanceByDocIdResult���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchSystemOrdinanceByDocIdResult() {
        return searchSystemOrdinanceByDocIdResult;
    }

    /**
     * ����searchSystemOrdinanceByDocIdResult���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchSystemOrdinanceByDocIdResult(String value) {
        this.searchSystemOrdinanceByDocIdResult = value;
    }

}
