package com.jecn.epros.server.webBean.dataImport.sync;

import java.io.Serializable;

public class JecnOrgView implements Serializable {
	private String orgNum;
	private String orgName;
	private String perOrgNum;
	/** 0：添加，1：更新，2：删除 */
	private String orgStatus;
	
	private String UUID;

	public JecnOrgView(String orgNum, String orgName, String perOrgNum, String orgStatus) {
		this.orgNum = orgNum;
		this.orgName = orgName;
		this.perOrgNum = perOrgNum;
		this.orgStatus = orgStatus;
	}

	public JecnOrgView() {
	}

	public String getOrgNum() {
		return orgNum;
	}

	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPerOrgNum() {
		return perOrgNum;
	}

	public void setPerOrgNum(String perOrgNum) {
		this.perOrgNum = perOrgNum;
	}

	public String getOrgStatus() {
		return orgStatus;
	}

	public void setOrgStatus(String orgStatus) {
		this.orgStatus = orgStatus;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
}
