/**
 * UUVService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cms.designerSSO;

public interface UUVService extends javax.xml.rpc.Service {
    public java.lang.String getUUVServiceSoapAddress();

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getUUVServiceSoap12Address();

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap12() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
