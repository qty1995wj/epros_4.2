package epros.designer.gui.recycle;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 文件回收站
 * 
 * @author Administrator
 * @date 2015-03-10
 */
public class FileRecyclePanel extends RecyclePanel {

	private FileRecycleTree recycleTree;
	private Logger log = Logger.getLogger(FileRecyclePanel.class);

	public FileRecyclePanel() {
		super();
	}

	@Override
	public String getSearchName() {
		return JecnProperties.getValue("fileNameC");
	}

	@Override
	public RecycleTree initTree() {
		recycleTree = new FileRecycleTree();
		recycleTree.setRecyclePanel(this);
		return recycleTree;
	}


	@Override
	public List<JecnTreeBean> searchByName(String name) {

		try {
			return ConnectionPool.getFileAction().getAllFilesByName(name,
					JecnConstants.projectId,
					JecnConstants.loginBean.getSetFile(),
					JecnConstants.loginBean.isAdmin());
		} catch (Exception e) {
			log.error("FileRecyclePanel searchByName is error", e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	public void delDataAction(List<Long> listIds) {
		try {
			ConnectionPool.getFileAction().deleteFiles(listIds,
					JecnConstants.projectId, JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("FileRecyclePanel delDataAction is error", e);
		}
	}

	@Override
	public String getTreeBorderName() {
		return JecnProperties.getValue("fileManage");
	}
}
