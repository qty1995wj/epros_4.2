package com.jecn.epros.server.bean.rule;

import java.util.Locale;

/**
 *制度模板标题表 (JECN_TITLE)
 * 
 * @author lihongliang
 * 
 */
public class RuleModeTitleBean implements java.io.Serializable {
	private Long id;
	private Long modeId;// 模板(页面)ID
	private String titleName;// 名称
	private Integer sortId;// 序号
	private Integer type;// 类型(0是内容，1是文件表单，2是流程表单)
	
	private String enName;

	/** 0:非必填，1：必填 */
	private int requiredType;

	
	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getModeId() {
		return modeId;
	}

	public void setModeId(Long modeId) {
		this.modeId = modeId;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public Integer getSortId() {
		return sortId;
	}

	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public int getRequiredType() {
		return requiredType;
	}

	public void setRequiredType(int requiredType) {
		this.requiredType = requiredType;
	}

	public String getTitleName(Locale locale) {
		return locale == Locale.CHINESE ? titleName : enName;
	}
}
