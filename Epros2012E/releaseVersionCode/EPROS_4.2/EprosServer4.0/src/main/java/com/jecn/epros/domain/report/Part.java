package com.jecn.epros.domain.report;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 饼状图的一个区域
 *
 * @author hyl
 */
public class Part {

    public static Map<String, String> roleRange = new LinkedHashMap<>();
    public static Map<String, String> activityRange = new LinkedHashMap<>();
    public static List<String> roleRangeKeys = null;
    public static List<String> activityRangeKeys = null;

    static {
        roleRange.put("0-3", "少于3个");
        roleRange.put("4-8", "4个到8个");
        roleRange.put("9", "多于9个");

        activityRange.put("0-10", "少于10个");
        activityRange.put("11-20", "11个到20个");
        activityRange.put("21", "多于21个");

        roleRangeKeys = new ArrayList(roleRange.keySet());
        activityRangeKeys = new ArrayList(activityRange.keySet());
    }

    /**
     * 唯一标示或者是区域的编号或者是区域的id
     */
    private String part;
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
