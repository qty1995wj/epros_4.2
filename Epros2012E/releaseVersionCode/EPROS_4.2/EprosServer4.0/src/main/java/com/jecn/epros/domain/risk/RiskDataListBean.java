package com.jecn.epros.domain.risk;

import com.jecn.epros.domain.process.Path;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 风险清单
 */
public class RiskDataListBean implements Serializable {

    private List<Path> paths = new ArrayList<Path>();
    /**
     * 风险ID
     */
    private Long riskId = null;
    /**
     * 是否为目录
     */
    private Integer isDir;
    /**
     * 目录级别
     */
    private Integer level;
    private String path;
    /**
     * 父节点ID
     */
    private Long parentId = null;
    /**
     * 风险编号
     */
    private String riskCode = null;
    /**
     * 风险描述或者目录名
     */
    private String riskName = null;
    /**
     * 风险等级 （当前风险系数 高 中 低）1：高		2：中	3：低
     */
    private Integer riskGrade;
    /**
     * 标准化控制
     */
    private String standardControl = null;
    /**
     * 制度ID
     */
    private Long ruld = null;
    /**
     * 制度名称
     */
    private String ruleName = null;
    /**
     * 制度责任部门ID
     */
    private Long ruleOrgId = null;
    /**
     * 制度责任部门名称
     */
    private String ruleOrgName = null;
    /**
     * 控制目标ID
     */
    private Long controlId = null;
    /**
     * 控制点名称
     **/
    private String controlName;
    /**
     * 控制点描述
     */
    private String controlDescroption = null;
    /**
     * 控制点目标
     */
    private String controlTargetName = null;
    /**
     * 活动ID
     */
    private Long activeId = null;
    /**
     * 活动名称
     */
    private String activeName = null;
    /**
     * 控制点编号
     */
    private String controlNum = null;
    /**
     * 活动说明
     */
    private String activeShow = null;
    ;
    /**
     * 所属流程ID
     */
    private Long flowId = null;
    /**
     * 所属流程名称
     */
    private String flowName = null;
    /**
     * 流程责任人部门ID
     */
    private Long resOrgId = null;
    /**
     * 流程责任人名称
     */
    private String resOrgName = null;
    /**
     * 控制方法 0:IT 1:自动 2IT/自动
     */
    private Integer controlMethod;
    /**
     * 控制类型 0:预防性 1:发现性
     */
    private Integer controlType;
    /**
     * 是否为关键控制点 0:是 1:否
     */
    private Integer keyControl;
    /**
     * 控制频率 0:不定期 1:日 2: 周 3:月 4:季度 5:年度
     */
    private Integer controlFrequency;
    /**
     * 角色名称
     */
    private String roleName;

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    public Integer getIsDir() {
        return isDir;
    }

    public void setIsDir(Integer isDir) {
        this.isDir = isDir;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public Integer getRiskGrade() {
        return riskGrade;
    }

    public void setRiskGrade(Integer riskGrade) {
        this.riskGrade = riskGrade;
    }

    public String getStandardControl() {
        return standardControl;
    }

    public void setStandardControl(String standardControl) {
        this.standardControl = standardControl;
    }

    public Long getRuld() {
        return ruld;
    }

    public void setRuld(Long ruld) {
        this.ruld = ruld;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Long getRuleOrgId() {
        return ruleOrgId;
    }

    public void setRuleOrgId(Long ruleOrgId) {
        this.ruleOrgId = ruleOrgId;
    }

    public String getRuleOrgName() {
        return ruleOrgName;
    }

    public void setRuleOrgName(String ruleOrgName) {
        this.ruleOrgName = ruleOrgName;
    }

    public Long getControlId() {
        return controlId;
    }

    public void setControlId(Long controlId) {
        this.controlId = controlId;
    }

    public String getControlName() {
        return controlName;
    }

    public void setControlName(String controlName) {
        this.controlName = controlName;
    }

    public String getControlDescroption() {
        return controlDescroption;
    }

    public void setControlDescroption(String controlDescroption) {
        this.controlDescroption = controlDescroption;
    }

    public String getControlTargetName() {
        return controlTargetName;
    }

    public void setControlTargetName(String controlTargetName) {
        this.controlTargetName = controlTargetName;
    }

    public Long getActiveId() {
        return activeId;
    }

    public void setActiveId(Long activeId) {
        this.activeId = activeId;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public String getControlNum() {
        return controlNum;
    }

    public void setControlNum(String controlNum) {
        this.controlNum = controlNum;
    }

    public String getActiveShow() {
        return activeShow;
    }

    public void setActiveShow(String activeShow) {
        this.activeShow = activeShow;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getResOrgId() {
        return resOrgId;
    }

    public void setResOrgId(Long resOrgId) {
        this.resOrgId = resOrgId;
    }

    public String getResOrgName() {
        return resOrgName;
    }

    public void setResOrgName(String resOrgName) {
        this.resOrgName = resOrgName;
    }

    public Integer getControlMethod() {
        return controlMethod;
    }

    public void setControlMethod(Integer controlMethod) {
        this.controlMethod = controlMethod;
    }

    public Integer getControlType() {
        return controlType;
    }

    public void setControlType(Integer controlType) {
        this.controlType = controlType;
    }

    public Integer getKeyControl() {
        return keyControl;
    }

    public void setKeyControl(Integer keyControl) {
        this.keyControl = keyControl;
    }

    public Integer getControlFrequency() {
        return controlFrequency;
    }

    public void setControlFrequency(Integer controlFrequency) {
        this.controlFrequency = controlFrequency;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


}
