package com.jecn.epros.domain.security;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by admin on 2017/3/20.
 */
public class NodeIdsData {
    /**
     * 展开显得节点ID集合
     */
    private Set<Long> expandIds = new HashSet<>();
    /**
     * 能操作的节点ID集合
     */
    private Set<Long> operationIds = new HashSet<>();

    /**
     * 能下载的节点ID 集合
     * @return
     */
    private Set<Long> downLoadIds = new HashSet<>();

    public Set<Long> getExpandIds() {
        if (expandIds == null) {
            expandIds = new HashSet<>();
        }
        return expandIds;
    }

    public Set<Long> getDownLoadIds() {
        if (downLoadIds == null) {
            downLoadIds = new HashSet<>();
        }
        return downLoadIds;
    }

    public void setDownLoadIds(Set<Long> downLoadIds) {
        this.downLoadIds = downLoadIds;
    }

    public void setExpandIds(Set<Long> expandIds) {
        this.expandIds = expandIds;
    }

    public Set<Long> getOperationIds() {
        if (operationIds == null) {
            operationIds = new HashSet<>();
        }
        return operationIds;
    }

    public void setOperationIds(Set<Long> operationIds) {
        this.operationIds = operationIds;
    }
}
