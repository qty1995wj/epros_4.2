package junit;

/**
 * 测试主入口
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.jecn.epros.server.action.web.task.TestTaskAction;

@RunWith(Suite.class)
@SuiteClasses( { TestAction.class, TestTaskAction.class })
public class TestAll {

}
