package com.jecn.epros.server.download.word;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.HeaderDocBean;
import com.jecn.epros.server.bean.download.RuleContentBean;
import com.jecn.epros.server.bean.download.RuleDownloadBean;
import com.jecn.epros.server.bean.download.RuleFileBean;
import com.jecn.epros.server.bean.download.RuleFlowBean;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnRuleCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;
import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;

public class RuleDownload {
	private static final Logger log = Logger.getLogger(RuleDownload.class);

	public static String create(RuleDownloadBean ruleDownloadBean) throws MalformedURLException, IOException, Exception {
		String filePath = null;
		Document document = null;
		FileOutputStream fileOutputStream = null;
		try {
			filePath = JecnFinal.saveTempFilePath();

			document = new Document(PageSize.A4, 80, 80, 80, 100);
			fileOutputStream = new FileOutputStream(JecnFinal.getAllTempPath(filePath));
			RtfWriter2.getInstance(document, fileOutputStream);
			document.open();
			// 添加页眉
			// Image headerImg = Image.getInstance(".\\logo\\logo01.jpg");
			// 生成页眉
			HeaderDocBean headerDocBean = new HeaderDocBean();
			// 名称
			headerDocBean.setName(ruleDownloadBean.getRuleName());
			// 是否公开
			headerDocBean.setIsPublic(ruleDownloadBean.getRuleIsPublic());
			// 编号
			headerDocBean.setInputNum(ruleDownloadBean.getRuleInputNum());
			// 版本
			headerDocBean.setVersion(ruleDownloadBean.getRuleVersion());
			// 页眉图片
			Image loginImage = JecnFinal.getImage("logo01.jpg");
			DownloadUtil.getHeaderDoc(document, headerDocBean, loginImage);
			// 生成页脚
			DownloadUtil.getFooterDoc(document);

			String line = "\n\n\n\n";
			document.add(new Paragraph(line));
			// 公司名称
			document.add(DownloadUtil.ST_X3_MIDDLE(ruleDownloadBean.getCompanyName()));

			String newline = "\n\n\n\n\n\n\n";
			document.add(new Paragraph(newline));

			// 标题
			document.add(DownloadUtil.ST_X2_BOLD_MIDDLE(ruleDownloadBean.getRuleName()));

			DownloadUtil.getReturnTable(document, ruleDownloadBean.getPeopleList(), ruleDownloadBean.getRuleInputNum(),
					ruleDownloadBean.getRuleVersion());

			int count = 1;
			for (RuleTitleT ruleTitleT : ruleDownloadBean.getRuleTitleTList()) {
				// 段落标题
				String titleName = null;
				if (JecnConfigTool.isShowDownLoadWordIndex()) {
					titleName = count + "、" + ruleTitleT.getTitleName();
				} else {
					titleName = ruleTitleT.getTitleName();
				}
				Paragraph title = DownloadUtil.getParagraphTitle(titleName);
				document.add(title);
				if (ruleTitleT.getType() == 0) {
					for (RuleContentBean ruleContentBean : ruleDownloadBean.getRuleContentList()) {
						if (ruleTitleT.getId().equals(ruleContentBean.getRuleTitleId())) {
							String content = "";
							if (ruleContentBean.getRuleContent() != null
									&& !"".equals(ruleContentBean.getRuleContent().trim())) {
								content = ruleContentBean.getRuleContent();
							} else {
								content = "\t" + JecnUtil.getValue("none");
							}
							document.add(DownloadUtil.getContent(content));
							document.add(new Paragraph());
						}
					}
				} else if (ruleTitleT.getType() == 1) {
					// 标识是否有文件数据
					boolean isTable = false;
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20 };
					// 文件编号
					String fileNumbers = JecnUtil.getValue("fileNumber");
					// 文件名称
					String fileName = JecnUtil.getValue("fileName");
					// 标题数据
					List<String> fileTitleList = new ArrayList<String>();
					fileTitleList.add(fileNumbers);
					fileTitleList.add(fileName);
					// 内容数据
					List<Object[]> fileObjList = new ArrayList<Object[]>();
					for (RuleFileBean ruleFileBean : ruleDownloadBean.getRuleFileList()) {
						if (ruleTitleT.getId().equals(ruleFileBean.getRuleTitleId())) {
							isTable = true;
							// 文件编号
							Object fileNumObj = ruleFileBean.getFileInput();
							// 文件名称
							Object fileNameObj = ruleFileBean.getFileName();
							// List数据
							Object[] objArr = new Object[] { fileNumObj, fileNameObj };
							fileObjList.add(objArr);
						}
					}
					if (isTable) {
						Table table = DownloadUtil.getTable(tableWidth, tableInt, fileTitleList, fileObjList);
						document.add(table);
						document.add(new Paragraph());
					} else {
						document.add(DownloadUtil.getContent("\t" + JecnUtil.getValue("none")));
						document.add(new Paragraph());
					}
				} else if (ruleTitleT.getType() == 2) {
					// 标识是否有文件数据
					boolean isTable = false;
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20 };
					// 流程名称
					String processName = JecnUtil.getValue("processName");
					// 流程编号
					String processNumbers = JecnUtil.getValue("processNumber");
					// 标题数据
					List<String> flowTitleList = new ArrayList<String>();
					flowTitleList.add(processNumbers);
					flowTitleList.add(processName);
					// 内容数据
					List<Object[]> flowObjList = new ArrayList<Object[]>();
					for (RuleFlowBean ruleFlowBean : ruleDownloadBean.getRuleFlowList()) {
						if (ruleTitleT.getId().equals(ruleFlowBean.getRuleTitleId())) {
							isTable = true;
							// 流程编号
							Object flowNumObj = ruleFlowBean.getFlowInput();
							// 流程名称
							Object flowNameObj = ruleFlowBean.getFlowName();
							// List数据
							Object[] objArr = new Object[] { flowNumObj, flowNameObj };
							flowObjList.add(objArr);
						}
					}
					if (isTable) {
						Table table = DownloadUtil.getTable(tableWidth, tableInt, flowTitleList, flowObjList);
						document.add(table);
						document.add(new Paragraph());
					} else {
						document.add(DownloadUtil.getContent("\t" + JecnUtil.getValue("none")));
						document.add(new Paragraph());
					}
				} else if (ruleTitleT.getType() == 3) {
					// 标识是否有文件数据
					boolean isTable = false;
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20 };
					// 流程名称
					String processName = "架构名称";
					// 流程编号
					String processNumbers = "架构编号";
					// 标题数据
					List<String> flowTitleList = new ArrayList<String>();
					flowTitleList.add(processNumbers);
					flowTitleList.add(processName);
					// 内容数据
					List<Object[]> flowObjList = new ArrayList<Object[]>();
					for (RuleFlowBean ruleFlowBean : ruleDownloadBean.getRuleFlowMapList()) {
						if (ruleTitleT.getId().equals(ruleFlowBean.getRuleTitleId())) {
							isTable = true;
							// 流程地图编号
							Object flowNumObj = ruleFlowBean.getFlowInput();
							// 流程地图名称
							Object flowNameObj = ruleFlowBean.getFlowName();
							// List数据
							Object[] objArr = new Object[] { flowNumObj, flowNameObj };
							flowObjList.add(objArr);
						}
					}
					if (isTable) {
						Table table = DownloadUtil.getTable(tableWidth, tableInt, flowTitleList, flowObjList);
						document.add(table);
						document.add(new Paragraph());
					} else {
						document.add(DownloadUtil.getContent("\t" + JecnUtil.getValue("none")));
						document.add(new Paragraph());
					}
				} else if (ruleTitleT.getType() == 4) {// 标准
					// 标识是否有文件数据
					boolean isTable = false;
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20 };
					// 标准名称
					String standarName = JecnUtil.getValue("standardName");
					// 标准类型
					String standarType = JecnUtil.getValue("type");
					// 标题数据
					List<String> standardTitleList = new ArrayList<String>();
					standardTitleList.add(standarType);
					standardTitleList.add(standarName);
					// 内容数据
					List<Object[]> standardObjList = new ArrayList<Object[]>();
					for (JecnStandarCommon jecnStandarCommon : ruleDownloadBean.getRuleStandardList()) {
						if (ruleTitleT.getId().equals(jecnStandarCommon.getRuleTitleId())) {
							isTable = true;
							// 标准名称
							Object standardNameObj = jecnStandarCommon.getStandarName();
							// 标准类型
							Object standardTypeObj = null;
							if ("1".equals(jecnStandarCommon.getStandaType())) {
								standardTypeObj = JecnUtil.getValue("fileStandar");
							} else if ("2".equals(jecnStandarCommon.getStandaType())) {
								standardTypeObj = JecnUtil.getValue("flowStandar");
							} else if ("3".equals(jecnStandarCommon.getStandaType())) {
								standardTypeObj = JecnUtil.getValue("flowMapStandar");
							} else if ("4".equals(jecnStandarCommon.getStandaType())) {
								standardTypeObj = JecnUtil.getValue("standarClause");
							} else if ("5".equals(jecnStandarCommon.getStandaType())) {
								standardTypeObj = JecnUtil.getValue("clauseRequest");
							}
							// List数据
							Object[] objArr = new Object[] { standardTypeObj, standardNameObj };
							standardObjList.add(objArr);
						}
					}
					if (isTable) {
						Table table = DownloadUtil.getTable(tableWidth, tableInt, standardTitleList, standardObjList);
						document.add(table);
						document.add(new Paragraph());
					} else {
						document.add(DownloadUtil.getContent("\t" + JecnUtil.getValue("none")));
						document.add(new Paragraph());
					}
				} else if (ruleTitleT.getType() == 5) {// 风险
					// 标识是否有文件数据
					boolean isTable = false;
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20 };
					// 风险描述
					String riskName = JecnUtil.getValue("riskNameC");
					// 风险编号
					String riskNum = JecnUtil.getValue("riskNumberC");
					// 标题数据
					List<String> riskTitleList = new ArrayList<String>();
					riskTitleList.add(riskNum);
					riskTitleList.add(riskName);
					// 内容数据
					List<Object[]> riskObjList = new ArrayList<Object[]>();
					for (JecnRiskCommon jecnRiskCommon : ruleDownloadBean.getRuleRiskList()) {
						if (ruleTitleT.getId().equals(jecnRiskCommon.getRuleTitleId())) {
							isTable = true;
							// 风险编号
							Object riskNumObj = jecnRiskCommon.getRiskNumber();
							// 风险描述
							Object riskNameObj = jecnRiskCommon.getRiskName();
							// List数据
							Object[] objArr = new Object[] { riskNumObj, riskNameObj };
							riskObjList.add(objArr);
						}
					}
					if (isTable) {
						Table table = DownloadUtil.getTable(tableWidth, tableInt, riskTitleList, riskObjList);
						document.add(table);
						document.add(new Paragraph());
					} else {
						document.add(DownloadUtil.getContent("\t" + JecnUtil.getValue("none")));
						document.add(new Paragraph());
					}
				} else if (ruleTitleT.getType() == 6) {// 风险
					// 标识是否有文件数据
					boolean isTable = false;
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20 };
					// 制度名称
					String ruleName = "制度名称";
					// 制度编号
					String ruleNum = "制度编号";
					// 标题数据
					List<String> ruleTitleList = new ArrayList<String>();
					ruleTitleList.add(ruleNum);
					ruleTitleList.add(ruleName);
					// 内容数据
					List<Object[]> riskObjList = new ArrayList<Object[]>();
					for (JecnRuleCommon jecnRuleCommon : ruleDownloadBean.getRuleRuleList()) {
						if (ruleTitleT.getId().equals(jecnRuleCommon.getRuleTitleId())) {
							isTable = true;
							// 制度编号
							Object ruleNumObj = jecnRuleCommon.getRuleNumber();
							// 制度名称
							Object ruleNameObj = jecnRuleCommon.getRuleName();
							// List数据
							Object[] objArr = new Object[] { ruleNumObj, ruleNameObj };
							riskObjList.add(objArr);
						}
					}
					if (isTable) {
						Table table = DownloadUtil.getTable(tableWidth, tableInt, ruleTitleList, riskObjList);
						document.add(table);
						document.add(new Paragraph());
					} else {
						document.add(DownloadUtil.getContent("\t" + JecnUtil.getValue("none")));
						document.add(new Paragraph());
					}
				}
				count++;
			}
		} catch (Exception ex) {
			log.error("", ex);
			throw ex;
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (Exception ex) {
					log.error("关闭document", ex);
				}
			}

			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception ex) {
					log.error("关闭fileOutputStream", ex);
				}
			}
		}
		return filePath;
	}
}
