package epros.draw.visioxml.visioinput;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.SwingWorker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.ManhattanLine;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.top.toolbar.io.JecnIOUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.DrawCommon;
import epros.draw.visio.JecnFileNameFilter;

/**
 * visio导入
 * 
 * @author admin
 * 
 */
public class VisioImportAction {
	private String[] fileEX = new String[] { "vdx" };
	private static final Log log = LogFactory.getLog(VisioImportAction.class);

	// 导入
	public void input() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveVisioDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JFileChooser fileChooser = new JecnFileChooser(pathUrl);
		// 移除系统给定的文件过滤器
		fileChooser.setAcceptAllFileFilterUsed(false);
		// 标题
		fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("pleaseChooseTheFile"));
		// 过滤文件后缀
		fileChooser.setFileFilter(new JecnFileNameFilter(fileEX));

		// 设置按钮为打开
		fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);

		// 显示文件选择器
		int i = fileChooser.showOpenDialog(JecnDrawMainPanel.getMainPanel());

		// 选择确认（yes、ok）后返回该值。
		if (i == JFileChooser.APPROVE_OPTION) {
			// 获取输出文件的路径
			String filePath = fileChooser.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSaveVisioDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			// 判断文件后缀名是否为vdx结尾
			if (isEndEquals(filePath)) {
				File flowFile = fileChooser.getSelectedFile();
				// 获取文件名称
				String fileName = flowFile.getName();
				fileName = fileName.substring(0, fileName.lastIndexOf("."));
				// 获取输出文件的路径
				String xmlPath = fileChooser.getSelectedFile().getPath();
				// 弹出确认框，覆盖或取消
				boolean flag = importConfirm();
				if (flag) {
					recoveryFlowFile(xmlPath);
				}
			}
		}
	}

	/**
	 * 覆盖流程图/组织图/流程地图
	 * 
	 * @author weidp
	 * @date 2014-11-3 下午03:33:09
	 * @param flowFile
	 */
	private void recoveryFlowFile(String flowFile) {
		WorkTask task = new WorkTask();
		task.setFullAllPath(flowFile);
		String error = JecnLoading.start(task);
		if (!DrawCommon.isNullOrEmtryTrim(error)) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil.getJecnResourceUtil()
					.getValue("inPutVisioError"));
		}
	}

	/**
	 * @author yxw 2012-8-10
	 * @description：面板保存进度控制线程
	 */
	class WorkTask extends SwingWorker<String, Void> {
		private String fullAllPath;

		@Override
		protected String doInBackground() throws Exception {
			imporotFlow(fullAllPath);
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}

		public String getFullAllPath() {
			return fullAllPath;
		}

		public void setFullAllPath(String fullAllPath) {
			this.fullAllPath = fullAllPath;
		}
	}

	/**
	 * 刷新画图面板
	 * 
	 * @param workflow
	 * @param width
	 * @param height
	 */
	private void refrashPanelSize(JecnDrawDesktopPane workflow, int width, int height) {
		workflow.setSize(width, height);
		workflow.setPreferredSize(new Dimension(width, height));
		workflow.revalidate();
	}

	/**
	 * 
	 * 读文件
	 * 
	 * @param flowFile
	 */
	private void imporotFlow(String allFullPath) {
		try {
			VisioBasicDataConvert reader = new VisioBasicDataConvert(allFullPath);
			EPROSMiddleData data = reader.read();
			if (data == null) {
				log.error("解析visio文件数据为空,allFullPath=" + allFullPath);
				return;
			}
			// 移除面板图形
			JecnIOUtil.removeAllWorkflow();
			// 添加横线 、竖线、分割线 到面板
			addDivedingLineToWorkflow(data.getDivedingLines());
			// 此处为了设置box层级，拆分2个集合
			List<JecnFigureData> listBox = new ArrayList<JecnFigureData>();
			List<JecnFigureData> listOther = new ArrayList<JecnFigureData>();
			List<JecnFigureData> list = data.getFigureDataLists();

			if (list != null) {
				for (JecnFigureData date : list) {
					if (MapElemType.DottedRect == date.getMapElemType()) {
						date.setZOrderIndex(0);
						date.setFillType(FillType.none);
						listBox.add(date);
						continue;
					}
					listOther.add(date);
				}
			}
			// 添加协作框到图形面板
			addFigureToWorkflow(listBox);
			// 添加图形到画图面板
			addFigureToWorkflow(listOther);
			// 添加横竖分割线到面板
			addVHLineToWorkflow(data.getVHLines());
			// 处理曼哈顿连接线
			addManLineToWorkflow(data.getManLines());
			// 设置面板的大小
			refrashPanelSize(getWorkflow(), (int) data.getPanelWidth(), (int) data.getPanelHeight());
		} catch (Exception e) {
			log.error("导入visio异常", e);
			JecnLoading.setError("导入visio异常");
		}

	}

	/**
	 * 便利曼哈顿线
	 * 
	 * @param manLines
	 */
	private void addManLineToWorkflow(List<JecnManhattanLineData> manLines) {
		if (manLines == null) {
			return;
		}
		// 面板所有图形
		List<JecnBaseFlowElementPanel> panleList = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();
		// 曼哈顿线
		ManhattanLine manLine = null;
		for (JecnManhattanLineData manLineDate : manLines) {
			manLine = new ManhattanLine(manLineDate);
			// 设置连接线的开始图形和结束图形
			VisioInputTools.setManLineRefFigure(panleList, manLine);
			// 添加连接线到画图面板
			JecnAddFlowElementUnit.addManhattinLine(manLine);
			// 设置线层级
			manLine.reSetFlowElementZorder();
			manLine.insertTextToLine();
		}
	}

	/**
	 * 便利分割线
	 * 
	 * @param divedings
	 */
	private void addDivedingLineToWorkflow(List<JecnBaseDivedingLineData> divedings) {
		if (divedings == null) {
			return;
		}
		// 分割线对象
		DividingLine dividingLine = null;
		for (JecnBaseDivedingLineData diveingDate : divedings) {
			dividingLine = new DividingLine(diveingDate);
			JecnAddFlowElementUnit.addDividingLine(dividingLine);
			// 给图形设置层级
			dividingLine.reSetFlowElementZorder();
		}
	}

	/**
	 * 便利横竖分割线
	 * 
	 * @param baseVHLines
	 */
	private void addVHLineToWorkflow(List<JecnVHLineData> baseVHLines) {
		if (baseVHLines == null) {
			return;
		}
		// 横竖分割线
		JecnBaseVHLinePanel vhLine = null;
		for (JecnVHLineData vhLineData : baseVHLines) {
			if (LineDirectionEnum.horizontal.equals(vhLineData.getLineDirectionEnum())) {
				vhLine = new ParallelLines(vhLineData);
			} else if (LineDirectionEnum.vertical.equals(vhLineData.getLineDirectionEnum())) {
				vhLine = new VerticalLine(vhLineData);
			} else {
				throw new RuntimeException("解析分割线异常");
			}
			JecnAddFlowElementUnit.addVHLine(vhLine, new Point((int) vhLineData.getVhX(), (int) vhLineData.getVhY()));
			// 给图形设置层级
			vhLine.reSetFlowElementZorder();
		}
	}

	/**
	 * 便利图形
	 * 
	 * @param figureDataLists
	 */
	private void addFigureToWorkflow(List<JecnFigureData> figureDataLists) {
		if (figureDataLists == null) {
			return;
		}
		Point point = null;
		for (JecnFigureData figureData : figureDataLists) {
			point = new Point();
			double x = figureData.getFigureDataCloneable().getLeftUpXY().getX();
			double y = figureData.getFigureDataCloneable().getLeftUpXY().getY();
			point.setLocation(x, y);
			// 创建图形对象
			JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowFigure(figureData
					.getMapElemType(), figureData);
			// 设置图形对象100%状态下位置、大小、编辑点等
			JecnFigureDataCloneable currFigureDataCloneable = figurePanel.getFlowElementData().getFigureDataCloneable();
			currFigureDataCloneable.reSetAttributes(point, new Dimension(figurePanel.getFlowElementData()
					.getFigureSizeX(), figurePanel.getFlowElementData().getFigureSizeY()), 1.0);
			// 设置图形属性
			figurePanel.setBounds(currFigureDataCloneable.getX(), currFigureDataCloneable.getY(),
					currFigureDataCloneable.getWidth(), currFigureDataCloneable.getHeight());

			// 面板添加流程元素
			JecnAddFlowElementUnit.addFigure(figurePanel);
		}
	}

	/**
	 * 导入确认提示
	 * 
	 * @author weidp
	 * @date 2014-10-29 下午02:10:50
	 * @return
	 */
	private boolean importConfirm() {
		JecnDrawDesktopPane drawDesk = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 提示语句
		String msg = "";
		switch (drawDesk.getFlowMapData().getMapType()) {
		case totalMap:
			msg = JecnResourceUtil.getJecnResourceUtil().getValue("totalMapTipMsg");
			break;
		case partMap:
			msg = JecnResourceUtil.getJecnResourceUtil().getValue("partMapTipMsg");
			break;
		case orgMap:
			msg = JecnResourceUtil.getJecnResourceUtil().getValue("orgMapTipMsg");
			break;
		}
		// 弹出提示框
		int result = JecnOptionPane.showOptionDialog(null, msg, JecnResourceUtil.getJecnResourceUtil().getValue("tip"),
				JecnOptionPane.OK_CANCEL_OPTION, JecnOptionPane.INFORMATION_MESSAGE, null, new Object[] {
						JecnResourceUtil.getJecnResourceUtil().getValue("cover"),
						JecnResourceUtil.getJecnResourceUtil().getValue("cancel") }, 0);
		return result == JecnOptionPane.OK_OPTION ? true : false;
	}

	/**
	 * 判断文件是否以 数组中的数据 结尾
	 */
	private boolean isEndEquals(String filePath) {
		for (String endEqu : fileEX) {
			if (filePath.endsWith(endEqu)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取面板
	 * 
	 * @return 当前面板
	 */
	private JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}
}
