package epros.designer.gui.popedom.positiongroup;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

public class PosGroupCommon {
	/***
	 * 获得所有的支持工具
	 * 
	 * @param list
	 * @return
	 */
	public static JecnTreeModel getPosGroupTreeModel(List<JecnTreeBean> list) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.positionGroupRoot, JecnProperties
				.getValue("positionGroup"));
		JecnTreeCommon.addNLevelNodes(list, rootNode);
		return new JecnTreeModel(rootNode);
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:获得TreeModel
	 * @param listDefault
	 * @param list
	 * @return
	 */
	public static JecnTreeModel getTreeMoveModel(List<JecnTreeBean> list, List<Long> listIds) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.positionGroupRoot, JecnProperties
				.getValue("positionGroup"));
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, listIds);
		return new JecnTreeModel(rootNode);
	}

}
