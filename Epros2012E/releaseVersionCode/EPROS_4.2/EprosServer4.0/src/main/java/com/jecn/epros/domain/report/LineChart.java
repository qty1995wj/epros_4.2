package com.jecn.epros.domain.report;

import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 2017/1/12.
 */
public class LineChart {


    private List<String> names;

    private Map<String, List<String>> data;

    private List<ChartResource> resources; //架构或部门的数据集合

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public Map<String, List<String>> getData() {
        return data;
    }

    public void setData(Map<String, List<String>> data) {
        this.data = data;
    }

    public List<ChartResource> getResources() {
        return resources;
    }

    public void setResources(List<ChartResource> resources) {
        this.resources = resources;
    }

        public static class ChartResource {
        private Long id;
        private String name;
        private Integer type;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer  getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }
    }


}
