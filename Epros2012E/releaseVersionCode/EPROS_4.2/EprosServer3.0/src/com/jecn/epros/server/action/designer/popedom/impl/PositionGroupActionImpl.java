package com.jecn.epros.server.action.designer.popedom.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.action.designer.popedom.IPositionGroupAction;
import com.jecn.epros.server.bean.dataimport.AddOrDelBaseBean;
import com.jecn.epros.server.bean.dataimport.BasePosAndPostionBean;
import com.jecn.epros.server.bean.popedom.JecnPositionGroup;
import com.jecn.epros.server.bean.system.JecnTablePageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.popedom.IPositionGroupService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class PositionGroupActionImpl implements IPositionGroupAction {

	private IPositionGroupService positionGroupService;

	public IPositionGroupService getPositionGroupService() {
		return positionGroupService;
	}

	public void setPositionGroupService(IPositionGroupService positionGroupService) {
		this.positionGroupService = positionGroupService;
	}

	@Override
	public List<JecnTreeBean> getChildPositionGroup(Long pId, Long projectId) throws Exception {
		return positionGroupService.getChildPositionGroup(pId, projectId);
	}

	@Override
	public List<JecnTreeBean> getChildPositionGroup(Long pId, Long projectId, Long peopleId) throws Exception {
		return positionGroupService.getChildPositionGroup(pId, projectId, peopleId);
	}

	@Override
	public List<JecnTreeBean> getChildPositionGroupDirs(Long pId, Long projectId) throws Exception {
		return positionGroupService.getChildPositionGroupDirs(pId, projectId);
	}

	@Override
	public List<JecnTreeBean> getAllPositionGroup(Long projectId) throws Exception {
		return positionGroupService.getAllPositionGroup(projectId);
	}

	@Override
	public List<JecnTreeBean> getAllPositionGroupDir(Long projectId) throws Exception {
		return positionGroupService.getAllPositionGroupDir(projectId);
	}

	@Override
	public List<JecnTreeBean> searchPositionGroupsByName(Long projectId, String name, Long peopleId) throws Exception {
		if (peopleId == null) {
			return positionGroupService.searchPositionGroupsByName(projectId, name);
		}
		return positionGroupService.searchPositionGroupsByName(projectId, name, peopleId);
	}

	@Override
	public void updateSortPosGroup(List<JecnTreeDragBean> list, Long pId, Long projectId, Long peopleId)
			throws Exception {
		positionGroupService.updateSortPosGroup(list, pId, projectId, peopleId);
	}

	@Override
	public void movePositionGroups(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		positionGroupService.movePositionGroups(listIds, pId, updatePersonId, moveNodeType);
	}

	@Override
	public void deletePositionGroups(List<Long> listIds, Long projectId, Long peopleId) throws Exception {
		positionGroupService.deletePositionGroups(listIds, projectId, peopleId);
	}

	@Override
	public Long addPositionGroup(JecnPositionGroup positionGroup) throws Exception {
		return positionGroupService.addPositionGroup(positionGroup);
	}

	@Override
	public void updatePositionGroup(Long positionGroupId, List<Long> positionIds, Long updatePersonId) throws Exception {
		positionGroupService.updatePositionGroup(positionGroupId, positionIds, updatePersonId);
	}

	@Override
	public void rePositionGroupName(String newName, Long id, Long updatePersonId) throws Exception {
		positionGroupService.rePositionGroupName(newName, id, updatePersonId);
		// 同步流程图中 角色的文本信息（岗位组名称）

	}

	@Override
	public List<JecnTreeBean> getPositionGroupDirsByNameMove(String name, List<Long> listIds, Long projectId)
			throws Exception {
		return positionGroupService.getPositionGroupDirsByNameMove(name, listIds, projectId);
	}

	@Override
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid, Long projectId) throws Exception {
		return positionGroupService.validateRepeatNameEidt(newName, id, pid, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id, Long projectId) throws Exception {
		return positionGroupService.getPnodes(id, projectId);
	}

	@Override
	public List<JecnTreeBean> getPosByPositionGroupId(Long id) throws Exception {
		return positionGroupService.getPosByPositionGroupId(id);
	}

	/**
	 * 查询基准岗位
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public BasePosAndPostionBean getJecnBaseBean(Long projectId, String baseName, String posGroupId) {
		return positionGroupService.getJecnBaseBean(projectId, baseName, posGroupId);
	}

	/**
	 * 根据ID查询基准岗位对应的岗位
	 * 
	 * @author cheshaowei
	 * @throws Exception
	 * */
	public List<Object[]> getBaseRelPos(String baseId, String pageNum, String pageSize, String operationType)
			throws Exception {
		return positionGroupService.getBaseRelPos(baseId, pageNum, pageSize, operationType);
	}

	/**
	 * 
	 * 添加新关联的基准岗位，关联基准岗位
	 * 
	 * @author cheshaowei
	 * @param addOrDelBaseBean
	 *            待添加数据和待更新数据封装bean
	 * @throws Exception
	 * 
	 * */
	public void addOrUpdate(AddOrDelBaseBean addOrDelBaseBean) throws Exception {
		positionGroupService.addOrUpdate(addOrDelBaseBean);
	}

	/**
	 * 判断是否存在岗位组和基准岗位关联
	 * 
	 * @param @param groupId
	 * @param @return
	 * @param @throws Exception
	 * @author ZXH
	 * @date 2015-7-17 下午02:33:16
	 * @throws
	 */
	@Override
	public int posGroupRelCount(Long groupId) throws Exception {
		return positionGroupService.posGroupRelCount(groupId);
	}

	@Override
	public void AddSelectPoss(Set<Long> selectIds, Long groupId) throws Exception {
		positionGroupService.AddSelectPoss(selectIds, groupId);

	}

	@Override
	public void AddSearchResultPoss(Long orgId, String posName, Long groupId, Long projectId) throws Exception {
		positionGroupService.AddSearchResultPoss(orgId, posName, groupId, projectId);

	}

	@Override
	public JecnTablePageBean searchPos(Long orgId, String posName, int curPage, String operationType, int pageSize,
			Long groupId, Long projectId) throws Exception {
		return positionGroupService.searchPos(orgId, posName, curPage, operationType, pageSize, groupId, projectId);
	}

	@Override
	public void delResultSelectPoss(Set<Long> selectIds, Long groupId) throws Exception {
		positionGroupService.delResultSelectPoss(selectIds, groupId);
	}

	@Override
	public void cancelResultPoss(Long groupId) throws Exception {
		positionGroupService.cancelResultPoss(groupId);
	}

	@Override
	public JecnTablePageBean showPos(int curPage, String operationType, int pageSize, Long groupId) throws Exception {
		return positionGroupService.showPos(curPage, operationType, pageSize, groupId);
	}

	@Override
	public void AddSearchResultPoss(String orgName, String posName, Long groupId, Long projectId, boolean isSelect) {
		positionGroupService.AddSearchResultPoss(orgName, posName, groupId, projectId, isSelect);

	}

	@Override
	public void AddSearchResultPoss(List<Long> ids, String posName, Long groupId, Long projectId, boolean isSelect) {
		positionGroupService.AddSearchResultPoss(ids, posName, groupId, projectId, isSelect);

	}

	@Override
	public JecnTablePageBean searchPos(String orgName, String posName, int curPage, String operationType, int pageSize,
			Long groupId, Long projectId, boolean isSelect) {
		return positionGroupService.searchPos(orgName, posName, curPage, operationType, pageSize, groupId, projectId,
				isSelect);
	}

	@Override
	public JecnTablePageBean searchPos(List<Long> orgIds, String posName, int curPage, String operationType,
			int pageSize, Long groupId, Long projectId, boolean isSelect) {
		return positionGroupService.searchPos(orgIds, posName, curPage, operationType, pageSize, groupId, projectId,
				isSelect);
	}

	@Override
	public String getPosGroupExplain(Long id) throws SQLException, IOException {
		return positionGroupService.getPosGroupExplain(id);
	}

	@Override
	public boolean updatePosGroupExplain(Long id, String posGroupExplain, Long peopleId) throws Exception {
		return positionGroupService.getPosGroupExplain(id, posGroupExplain, peopleId);
	}

}
