package com.jecn.epros.sqlprovider.server3;

import com.jecn.epros.domain.EnumClass.TreeNodeType;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.file.FileBean;
import com.jecn.epros.domain.file.FileBeanT;
import com.jecn.epros.domain.file.FileContent;
import com.jecn.epros.domain.process.FlowBasicInfoT;
import com.jecn.epros.domain.process.FlowStructure;
import com.jecn.epros.domain.process.FlowStructureT;
import com.jecn.epros.domain.rule.G020RuleFileContent;
import com.jecn.epros.domain.rule.Rule;
import com.jecn.epros.domain.rule.RuleT;
import com.jecn.epros.domain.system.Message;
import com.jecn.epros.domain.task.TaskBean;
import com.jecn.epros.domain.task.TaskHistoryNew;
import com.jecn.epros.domain.task.TaskStage;
import com.jecn.epros.domain.task.TempAuditPeopleBean;
import com.jecn.epros.mapper.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.BaseSqlProvider;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;
import net.sourceforge.jtds.jdbc.BlobImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Blob;
import java.util.*;

/**
 * 任务审批通用类
 *
 * @author ZHANGXIAOHU @date： 日期：May 7, 2013 时间：11:10:15 AM
 */
public class TaskCommon {
    private static final Log log = LogFactory.getLog(TaskCommon.class);
    /**
     * 任务审批阶段个数
     */
    private static final int TASK_STATE_VALUE = 10;

    public static final String ALL = "全部";
    public static final String PROCESS_TASK = "流程任务";
    public static final String FILE_TASK = "文件任务";
    public static final String RULE_TASK = "制度任务";
    public static final String MAP_TASK = "流程架构任务";
    public static final String FINISH = "完成";
    public static final String FINISHING_VIEWS = "整理意见";

    /**
     * 整理意见状态
     **/
    private static final String finView = "views";
    /**
     * 人员离职或岗位变更
     **/
    public static final String STAFF_CHANGE = "人员离职";
    /**
     * 人员已离职或没有岗位，请联系管理员！
     **/
    public static final String STAFF_TURNOVER_OR_NO_JOB = "人员已离职或没有岗位，请联系管理员！";
    /**
     * TASK_NOTHIN
     **/
    public static final String TASK_NOTHIN = "无";
    public static final String DRAFT = "拟稿";

    /**
     * APPROVE_ERROR
     **/
    public static final String APPROVE_ERROR = "审批异常";
    /**
     * "密级由(秘密)更改为(公开)"
     **/
    public static final String CHANGE_SECRET_TO_PUBLIC = "密级由(秘密)更改为(公开)";
    /**
     * "密级由(公开)更改为(秘密)"
     **/
    public static final String CHANGE_PUBLIC_TO_SECRET = "密级由(公开)更改为(秘密)";
    /**
     * "类别由("
     **/
    public static final String TYPE_FROM = "类别由(";
    /**
     * ")更改为("
     **/
    public static final String TO_TYPE = ")更改为(";
    /**
     * "部门权限由("
     **/
    public static final String ROG_FROM = "部门权限由(";
    /**
     * "岗位权限由("
     **/
    public static final String POS_FROM = "岗位权限由(";
    /**
     * "岗位组权限由("
     **/
    public static final String GRO_FROM = "岗位组权限由(";

    /**
     * 审批成功！
     **/
    public static final String APPROVE_SUCESS = "审批成功！";
    /**
     * 任务已审批！
     **/
    public static final String APPROVE_IS_SUBMIT = "任务已审批！";
    /**
     * 访问出错！
     **/
    public static final String visitError = "访问出错！";
    /**
     * 当前审批阶段审批人不能为空！
     **/
    public static final String CUR_APP_PEOPLE_NOT_NULL = "当前审批阶段审批人不能为空！";
    /**
     * 编辑成功
     **/
    public static final String EDIT_TASK_SUCESS = "编辑成功";
    /**
     * 当前操作人和返回目标人相同！
     **/
    public static final String CUR_PEOPLE_IS_SAME_TO_PEOPLE = "当前操作人和返回目标人相同！";

    /**
     * （已离职）
     **/
    public static final String LEAVE_OFFICE = "（已离职）";
    /**
     * 任务已失效！
     **/
    public static final String TASK_IS_EXPIRE = "任务已失效！";
    /**
     * 会审人员已离职或没有岗位，请联系管理员！
     **/
    public static final String REVIEW_PEOPLE_NOT_NULL = "会审人员已离职或没有岗位，请联系管理员！";
    /**
     * 任务已审批或已失效！
     **/
    public static final String TASK_APPROVE_OR_PEOPLE_LIVE = "任务已审批或已失效！";

    /**
     * 字符串ID去空格拼装
     *
     * @param accString
     * @return
     */
    public static String getAccessByStr(String accString) {
        if (accString == null || "".equals(accString)) {
            return "";
        }
        String[] strArr = accString.split(",");
        String accessIds = "";
        for (int i = 0; i < strArr.length; i++) {
            String str = strArr[i];
            if (str == null || "".equals(str.trim())) {
                continue;
            }
            if (accessIds == "") {
                accessIds = str.trim();
            } else {
                accessIds = accessIds + "," + str.trim();
            }
        }
        return accessIds;
    }

    /**
     * 给定参数是否为null
     *
     * @param obj Object
     * @return
     */
    public static boolean isNullObj(Object obj) {
        return (obj == null) ? true : false;
    }

    /**
     * 通过审批标识符号获得阶段
     *
     * @param approveSign
     * @return
     */
    public static int getState(String approveSign) {
        if ("a1".equals(approveSign)) {
            return 1;
        } else if ("a2".equals(approveSign)) {
            return 2;
        } else if ("a3".equals(approveSign)) {
            return 3;
        } else if ("a4".equals(approveSign)) {
            return 4;
        } else if ("a5".equals(approveSign)) {
            return 6;
        } else if ("a6".equals(approveSign)) {
            return 7;
        } else if ("a7".equals(approveSign)) {
            return 8;
        } else if ("a8".equals(approveSign)) {
            return 9;
        }
        return -1;
    }

    /**
     * 根据获取的object对象返回intvalue
     *
     * @param object Object int值的对象
     * @return int
     */
    public static int getIntByObject(Object object) {
        if (object == null || "".equals(object.toString())) {
            return -1;
        }

        return Integer.valueOf(object.toString()).intValue();
    }

    /**
     * 根据获取的object对象返回intvalue
     *
     * @param object Object int值的对象
     * @return int
     */
    public static long getlongByObject(Object object) {
        if (object == null || "".equals(object.toString())) {
            return -1;
        }

        return Long.valueOf(object.toString()).longValue();
    }

    /**
     * 验证两个对象的值是否相同
     *
     * @param str1
     * @param str2
     * @return
     */
    public static boolean isSameStr(Object obj1, Object obj2) {
        if (isNullObj(obj1) && isNullObj(obj2)) {// 均为空
            return false;
        } else if (obj1.toString().trim().equals(obj2.toString().trim())) {
            return true;
        }
        return false;
    }

    /**
     * 字符串换行符替换
     *
     * @param str
     * @return
     */
    public static String replaceAll(String str) {
        if (Common.isNullOrEmtryTrim(str)) {
            return "";
        }
        return str.replaceAll("\n", "<br>");
    }

    /**
     * 通过人员id获得人员的姓名
     *
     * @param userMap
     * @param peopleId
     * @return String 人员真实姓名
     */
    public static String getTrueNameByPeopleId(Map<Long, JecnUser> userMap, Long peopleId) {

        JecnUser user = userMap.get(peopleId);
        if (user != null) {
            if (user.getIsLock() == 1) {
                return user.getName() + "（已离职）";// （已离职）
            } else {
                return user.getName();
            }
        }
        // 人员离职
        return "（人员离职）";
    }

    /**
     * 验证开始时间和结束时间是否在
     *
     * @param startTime
     * @param endTime
     * @return true 开始时间和结束时间都不为空
     */
    public static boolean isRightTime(String startTime, String endTime) {
        if (Common.isNullOrEmtryTrim(startTime) || Common.isNullOrEmtryTrim(endTime)) {// 开始时间和结束时间不能为空
            return false;
        }
        return true;
    }

    /**
     * 在jecnTaskStageList中按照state获取对应的Bean
     */
    public static TaskStage getCurTaskStage(List<TaskStage> jecnTaskStageList, Integer state) {
        for (TaskStage jecnTaskStage : jecnTaskStageList) {
            if (jecnTaskStage.getStageMark().intValue() == state) {
                return jecnTaskStage;
            }
        }
        return null;
    }

    /**
     * 根据任务类型获取对应的系统审批阶段配置项信息
     *
     * @param taskType 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
     * @param state    任务阶段 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
     *                 7：IT总监审批阶段 8:事业部经理 9：总经理
     * @return
     */
    public static List<TempAuditPeopleBean> getTempAuditPeopleBeanList(TaskBean taskBeanNew,
                                                                       List<TaskStage> taskStageList) {
        List<TempAuditPeopleBean> listPeopleBean = new ArrayList<TempAuditPeopleBean>();
        // 是否执行过 2:为当前任务 1：已审批，0 未审批
        int isExecute = 1;
        for (TaskStage taskStage : taskStageList) {
            // 当前任务阶段
            if (taskStage.getStageMark().intValue() == taskBeanNew.getTaskState()) {
                isExecute = 2;
                initTempAuditPeopleBean(listPeopleBean, isExecute, taskStage);
                isExecute = 0;
                continue;
            }
            initTempAuditPeopleBean(listPeopleBean, isExecute, taskStage);
        }
        return listPeopleBean;
    }

    private static void initTempAuditPeopleBean(List<TempAuditPeopleBean> listPeopleBean, int isExecute,
                                                TaskStage taskStage) {
        TempAuditPeopleBean auditPeopleBean = new TempAuditPeopleBean();
        //获取国际化状态
        int type = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getLanguage();
        // 审核阶段名称
        auditPeopleBean.setAuditLabel(taskStage.getStageName(type));
        // 记录当前审核阶段对应任务的状态
        auditPeopleBean.setState(taskStage.getStageMark());
        // 记录当前审核阶段信息主键
        auditPeopleBean.setStageId(taskStage.getId());
        // 执行过 是否审批 isExecute 2:为当前任务 1：已审批，0 未审批
        auditPeopleBean.setIsApproval(isExecute);
        // 添加是否显示
        auditPeopleBean.setIsShow(taskStage.getIsShow());
        // 添加是否为空
        auditPeopleBean.setIsEmpty(taskStage.getIsEmpty());
        // 添加是否选择人
        // 添加集合
        auditPeopleBean.setIsSelectedUser(taskStage.getIsSelectedUser());
        listPeopleBean.add(auditPeopleBean);
    }

    /**
     * 验证是否存在评审人阶段
     *
     * @param listAuditPeopleBean 各阶段审批人信息
     * @return true 存在评审人阶段 false 不存在评审人阶段
     */
    private static boolean isExitReviewPeople(List<TempAuditPeopleBean> listAuditPeopleBean) {
        if (listAuditPeopleBean == null) {
            throw new NullPointerException("验证是否存在评审人阶段异常！listAuditPeopleBean == null");
        }
        for (int i = 0; i < listAuditPeopleBean.size(); i++) {
            TempAuditPeopleBean auditPeopleBean = listAuditPeopleBean.get(i);
            if (auditPeopleBean.getState() == 3 && auditPeopleBean.getAuditId() != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * 打回整理验证 （审批人必须存在评审人会审阶段后）
     *
     * @param orderList 审批顺序
     * @param state     当前任务状态 当前阶段审批人必须在评审人会审阶段之后审批才可以存在打回整理按钮操作
     * @return
     * @throws BsException
     */
    public static boolean isCallBack(List<Integer> orderList, int state,
                                     List<TempAuditPeopleBean> listAuditPeopleBean) {
        if (listAuditPeopleBean == null || orderList == null) {
            throw new NullPointerException("打回整理状态验证异常！listAuditPeopleBean==null || orderList==null");
        }
        // 验证是否存在评审人阶段】
        if (!isExitReviewPeople(listAuditPeopleBean)) {// 不存在评审人阶段，责没有打回整理状态
            return false;
        }
        // // 当前任务状态标识
        // String stateType = JecnTaskCommon.getStateType(state);
        // 评审人对应数组中的位置
        int reviewCount = 0;
        // 当前阶段对应数组的位置
        int stateCount = 0;
        for (int i = 0; i < orderList.size(); i++) {
            if (state == orderList.get(i)) {
                stateCount = i;// 当前阶段位置
            } else if (3 == orderList.get(i)) {
                reviewCount = i;// 评审人所在位置
            }
        }
        if (stateCount > reviewCount) {// 当前阶段审批人必须在评审人会审阶段之后审批才可以存在打回整理按钮操作
            return true;
        }
        return false;
    }

    /**
     * 获取上个已选择人的阶段的阶段信息
     *
     * @param taskStageList
     * @param state
     * @return
     * @author weidp
     * @date 2014-7-4 下午07:52:35
     */
    public static TaskStage getPreTaskStage(List<TaskStage> taskStageList, int curState) {
        if (taskStageList == null || taskStageList.size() == 0) {
            return null;
        }
        // 记录前一个State在集合里的索引
        int preStateIndex = -1;
        // 如果前一个阶段未选择人，则激活此变量，继续向前一个状态搜索
        boolean beforeCurState = false;
        // 从集合的最后一个元素开始找
        for (int curStateIndex = taskStageList.size() - 1; curStateIndex >= 0; curStateIndex--) {
            // 找到当前元素所处的索引
            if (taskStageList.get(curStateIndex).getStageMark().intValue() == curState) {
                // 变更当前索引为--后的索引
                curStateIndex--;
                if (curStateIndex < 0) {
                    break;
                }
                // 如果前一个索引处的元素已选择人，则得到其所在位置的索引，结束循环
                if (taskStageList.get(curStateIndex).getIsSelectedUser().intValue() == 1) {
                    preStateIndex = curStateIndex;
                    break;
                }
                // 当前一个索引的元素未选择人时，激活此元素
                beforeCurState = true;
            }
            // 找到curState前的第一个已选择人的Stage
            if (beforeCurState && taskStageList.get(curStateIndex).getIsSelectedUser() == 1) {
                preStateIndex = curStateIndex;
                break;
            }
        }
        // 返回结果
        if (preStateIndex != -1 && taskStageList.get(preStateIndex) != null) {
            return taskStageList.get(preStateIndex);
        }
        return null;
    }

    /**
     * 获得下一阶段的JecnTaskStage
     *
     * @param curState
     * @param taskStageList
     * @return
     * @author weidp
     * @date 2014-7-4 下午07:53:39
     */
    public static TaskStage getNextTaskStage(List<TaskStage> taskStageList, int curState) {
        if (taskStageList == null || taskStageList.size() == 0) {
            return null;
        }
        // 下一个阶段的索引
        int nextStateIndex = -1;
        // 下一个阶段的是否选择人非已选择状态时，激活此元素
        boolean afterCurState = false;

        for (int curStateIndex = 0; curStateIndex < taskStageList.size(); curStateIndex++) {
            // 找到当前阶段时
            if (taskStageList.get(curStateIndex).getStageMark().intValue() == curState) {
                // 如果下个阶段为已选择人状态，保存下个阶段的索引，结束循环。否则激活afterCurState元素
                curStateIndex++;
                if (curStateIndex == taskStageList.size()) {
                    break;
                }
                if (taskStageList.get(curStateIndex).getIsSelectedUser().intValue() == 1) {
                    nextStateIndex = curStateIndex;
                    break;
                }
                afterCurState = true;

            }
            // 找到任务当前状态后isSelectedUser=1的第一个Stage元素
            if (afterCurState && taskStageList.get(curStateIndex).getIsSelectedUser() == 1) {
                nextStateIndex = curStateIndex;
                break;
            }
        }
        // 返回结果
        if (nextStateIndex != -1 && taskStageList.get(nextStateIndex) != null) {
            return taskStageList.get(nextStateIndex);
        }
        return null;
    }

    /**
     * 特殊操作处理
     *
     * @param taskElseType 最后一次提交的状态
     * @param upState      上一阶段状态
     * @return int页面对应操作 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回
     * 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审 拟稿人------>评审 7：拟稿人重新提交审批
     * 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交 11:编辑提交 12 编辑打回 13: 撤回 14:
     * 反回
     */
    public static int sepicalAssignReBack(int taskElseType, int upState) {
        int elseType = 1;
        switch (taskElseType) {
            case 2:// 交办
            case 3:// 转批
            case 4:// 打回
            case 5:// 提交意见（提交审批意见(评审-------->拟稿人)
            case 9:// 打回整理
                // 提交状态
                elseType = 1;
                break;

            case 10:// 交办人提交后点击撤销
                // 交办状态
                elseType = 2;
                break;
            case 6:// 二次评审
                // 撤回到整理意见阶段
                elseType = 5;
                break;
            case 7:// 重新提交
            case 0:// 重新提交
                // 撤回到打回
                elseType = 4;
                break;
        }
        if (upState == 10) {// 整理意见阶段点击撤回
            // 设为整理意见操作
            elseType = 9;
        }

        if (taskElseType == 10) {
            // 页面显示为交办人的页面
            taskElseType = 2;
        }
        return elseType;
    }

    /**
     * 根据任务审批状态获取任务配置对于的mark唯一标识
     *
     * @param state 任务审批状态
     * @return String MARK 任务审批环节唯一标识
     */
    public static String getItemMark(int state) {
        String mark = "";
        switch (state) {
            case 1:// 文控审核人
                mark = ConfigItemPartMapMark.taskAppDocCtrlUser.toString();
                break;
            case 2:// 部门审核人
                mark = ConfigItemPartMapMark.taskAppDeptUser.toString();
                break;
            case 3:// 评审人会审
                mark = ConfigItemPartMapMark.taskAppReviewer.toString();
                break;
            case 4:// 批准人审核
                mark = ConfigItemPartMapMark.taskAppPzhUser.toString();
                break;
            case 5:// 完成
                mark = "Complete";
                break;
            case 6: // 各业务体系负责人
                mark = ConfigItemPartMapMark.taskAppOprtUser.toString();
                break;
            case 7: // IT总监
                mark = ConfigItemPartMapMark.taskAppITUser.toString();
                break;
            case 8:// 事业部经理
                mark = ConfigItemPartMapMark.taskAppDivisionManager.toString();
                break;
            case 9:// 总经理
                mark = ConfigItemPartMapMark.taskAppGeneralManager.toString();
                break;
            case 10:// 整理意见
                mark = finView;
                break;
        }
        return mark;
    }

    /**
     * 根据mark 类型获取对应的系统配置信息
     *
     * @param list
     * @param mark
     * @return
     */
    public static ConfigItemBean getJecnConfigItemBena(List<ConfigItemBean> list, String mark) {
        if (list == null || Common.isNullOrEmtryTrim(mark)) {
            throw new NullPointerException("根据mark 类型获取对应的系统配置信息 异常！");
        }
        for (ConfigItemBean itemBean : list) {
            if (mark.equals(itemBean.getMark())) {
                return itemBean;
            }
        }
        return null;
    }

    /**
     * 发布任务，创建文控版本信息日志
     *
     * @param historyNew 文控信息
     * @throws Exception
     */
    public static void saveTaskHistoryNew(TaskHistoryNew historyNew, UserMapper userMapper, DocMapper docMapper,
                                          ProcessMapper processMapper, RuleMapper ruleMapper, FileMapper fileMapper) throws Exception {
        // 发布时间
        historyNew.setPublishDate(new Date());
        // 关联ID
        Long refId = historyNew.getRelateId();
        // 文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
        int type = historyNew.getType();
        if (refId == null) {
            return;
        }
        log.info("发布文件类型 = " + type);
        if (type == 0 || type == 4) {// 发布流程
            if (type == 0) {
                // 获取流程基本信息表中流程有效期添加到文控主表有效期
                FlowBasicInfoT flowBasicInfoT = processMapper.getProcessBasicInfoT(refId);
                Long expiry = flowBasicInfoT.getExpiry();
                if (expiry == null) {
                    // 有效期（永久）
                    historyNew.setExpiry(0L);
                    // 下一次审视时间
                    historyNew.setNewScanDate(new Date());
                } else {
                    // 有效期
                    historyNew.setExpiry(Long.valueOf(expiry));
                    // 下一次审视时间
                    historyNew.setNewScanDate(Common.plusMonthOnDateForDate(new Date(), expiry.intValue()));
                }
            }

            pubFlow(historyNew, userMapper, docMapper, processMapper, ruleMapper, fileMapper);
            // FIXME 华帝登录时写入数据至Domino平台
            // if (isVatti) {
            // DominoOperation.importDataToDomino(historyNew.getRelateId(),
            // "Flow");
            // }

        } else if (type == 1) { // 文件发布
            pubFile(historyNew, docMapper, userMapper, fileMapper);
        } else if (type == 2) {// 发布制度模板文件
            pubModeRuleFile(historyNew, docMapper, userMapper, ruleMapper, fileMapper);
            // FIXME
            // if (isVatti) {
            // DominoOperation.importDataToDomino(historyNew.getRelateId(),
            // "Rule");
            // }
        } else if (type == 3) {// 发布制度文件
            RuleT ruleT = ruleMapper.getRuleT(refId);
            if (ruleT.getIsFileLocal() == null) {
                throw new NullPointerException("制度文件的isFileLocal标示为空，制度id为" + ruleT.getId());
            }
            if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 0) {
                pubRuleFile(historyNew, docMapper, userMapper, ruleMapper, fileMapper);
            } else if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 1) {
                pubRuleLocalFile(historyNew, docMapper, userMapper, ruleMapper, fileMapper);
            }

        }

    }

    /**
     * 发布流程 and 流程地图
     *
     * @param historyNew 文控信息
     * @param docMapper
     * @param fileMapper
     * @throws Exception
     */
    private static void pubFlow(TaskHistoryNew historyNew, UserMapper userMapper, DocMapper docMapper,
                                ProcessMapper processMapper, RuleMapper ruleMapper, FileMapper fileMapper) throws Exception {
        long startTime = System.currentTimeMillis();

        log.info("发布的流程文件相关查阅权限成功！方法 getAccessPeopleIds  = " + (System.currentTimeMillis() - startTime));
        // 保存文控信息
        // FIXME 怎样获得保存之后的主键id
        if (BaseSqlProvider.isOracle()) {
            docMapper.saveJecnTaskHistoryNewOracle(historyNew);
        } else {
            docMapper.saveJecnTaskHistoryNewSqlServer(historyNew);
        }

        log.info("流程发布，保存文件成功！ = " + (System.currentTimeMillis() - startTime));
        FlowStructureT structureT = processMapper.getFlowStructureT(historyNew.getRelateId());
        String prfName = structureT.getFlowName();
        // 获取正式版
        String oldPath = "";
        FlowStructure structure = processMapper.getFlowStructure(historyNew.getRelateId());
        // fileMapper.getSession().evict(structure);
        if (structure != null) {// 删除历史图片
            oldPath = structure.getFilePath();
        }
        // 保存文件版本ID到流程主表
        structureT.setHistoryId(historyNew.getId());
        // 设置更新时间
        structureT.setUpdateDate(new Date());
        // fileMapper.getSession().update(structureT);
        // fileMapper.getSession().flush();
        processMapper.updateFlowStructureT(structureT);

        TreeNodeType nodeType = null;
        if (historyNew.getType() == 4) {// 流程地图发布
            releaseProcessMapData(processMapper, historyNew.getRelateId());
            log.info("流程地图发布，临时表数据插入真实表操作成功！");
            // 流程地图
            nodeType = TreeNodeType.processMap;
            // 发布流程地图相关文件
            pubFlowMapRelateFile(historyNew, docMapper, fileMapper);
        } else {// 流程发布
            // 发布时流程参与人 Object[] 0:内外网；1：邮件地址；2：人员主键ID
            startTime = System.currentTimeMillis();
            log.info("流程发布 数据处理开始 = " + (System.currentTimeMillis() - startTime));

            // 发布流程的数据
            releaseProcessData(historyNew.getRelateId(), processMapper);
            log.info("流程图发布，临时表数据插入真实表操作成功！" + (System.currentTimeMillis() - startTime));
            // 流程图
            nodeType = TreeNodeType.process;
            // 发布流程图相关文件信息
            pubFlowRelateFile(historyNew, docMapper, fileMapper);

            log.info("流程图发布，流程相关的文件，问发布的处理成功！" + (System.currentTimeMillis() - startTime));

            // List<JecnUser> users = getPartInPeopleIds(userMapper,
            // historyNew.getRelateId());
            // 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度、4流程地图
            // Set<Long> accessPeopleIdS = getAccessPeopleIds(userMapper, 0,
            // historyNew.getRelateId());
            // FIXME是否给参与人发送邮件
            // boolean isPartInEmailTrue = JecnTaskCommon.isTrueMark(
            // ConfigItemPartMapMark.emailPulishParticipants.toString(),
            // selectMessageAndEmailItemBean(configItemDao));
            // if (isPartInEmailTrue && !users.isEmpty()) {// 参与人发送邮件通知
            // BaseEmailBuilder emailBuilder = EmailBuilderFactory
            // .getEmailBuilder(EmailBuilderType.PROCESS_TASK_PUBLISH);
            // emailBuilder.setData(users, historyNew, prfName, 0);
            // List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
            // // TODO :EMAIL
            // JecnUtil.saveEmail(buildEmail);
            // }

            // FIXME 参与人是否发送消息
            // boolean isPartInMesTrue =
            // JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.mesPulishParticipants.toString(),
            // selectMessageAndEmailItemBean(configItemDao));
            // if (isPartInMesTrue) {// 参与人发送消息
            // Set<Long> setList = new HashSet<Long>();
            // for (JecnUser user : users) {
            // setList.add(user.getPeopleId());
            // }
            // createMessage(setList, historyNew.getType(), prfName,
            // docMapper.getSession());
            // log.info("流程图发布，参与人发送消息成功！");
            // }

            log.info("流程图发布， 相关邮件、文件处理结束 end！ " + (System.currentTimeMillis() - startTime));
        }

        // FIXME 操作说明
        // // 发布获取保存路径下的图片拷贝到发布图片路径下，并记录当前生成当前版本文件
        // startTime = System.currentTimeMillis();
        // log.info("流程图发布，记录文控版本 start: " + startTime);
        // String filePath = getPubImagePath(processMapper,
        // historyNew.getRelateId(), structureT.getFilePath(), oldPath,
        // docMapper, configItemDao, processBasicDao, processKPIDao,
        // processRecordDao, nodeType, true);
        //
        // historyNew.setFilePath(filePath);
        // log.info("流程图发布，记录文控版本 end: " + (System.currentTimeMillis() -
        // startTime));
        //
        // // 发布相关制度
        // pubRelateRule(historyNew, docMapper, processMapper, ruleMapper);
        // log.info("发布相关制度 end: " + (System.currentTimeMillis() - startTime));
        // // FIXME 查阅权限 消息，邮件
        // // setMessageByAccess(docMapper, configItemDao, accessPeopleIdS,
        // // historyNew.getRelateId(), prfName,
        // // historyNew.getType());
        // log.info("查阅权限 消息，邮件 end: " + (System.currentTimeMillis() -
        // startTime));
    }

    /**
     * 发布相关制度
     *
     * @param historyNew
     * @param docMapper
     * @param processMapper
     * @param ruleMapper
     * @throws Exception
     * @Deprecated
     */
    private static void pubRelateRule(TaskHistoryNew historyNew, DocMapper docMapper, ProcessMapper processMapper,
                                      RuleMapper ruleMapper) throws Exception {
        if (!validateRelateRule(historyNew.getType())) {// 验证流程、流程地图 相关制度是否关联发布
            return;
        }
        // }
        // 发布流程文控信息 --start
        // 发布流程是否存在相关制度为发布--start
        // 0:制度主键ID，1：是制度目录，制度模板文件，制度文件；2：制度文件对应的文件版本ID
        // key id isDir versionId
        List<Map<String, Object>> listRuleIdsNoRelease = null;
        if (historyNew.getType() == 4) {
            listRuleIdsNoRelease = processMapper.getRelateRuleByFlowMapId(historyNew.getRelateId());
            log.info("获取流程地图相关制度成功！");
        } else {
            listRuleIdsNoRelease = processMapper.getRelateRuleByFlowId(historyNew.getRelateId());
            log.info("获取流程图相关制度成功！");
        }
        if (listRuleIdsNoRelease != null && listRuleIdsNoRelease.size() > 0) {
            // 发布制度
            pubRefRule(listRuleIdsNoRelease, historyNew, ruleMapper, docMapper);
            log.info("流程发布，发布流程相关制度成功！");
        }
    }

    /**
     * 验证流程、流程地图 相关制度是否关联发布
     *
     * @param type 相关类型 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
     * @return true:关联发布
     */
    public static boolean validateRelateRule(int type) {
        // 验证配置是否发布关联的文件信息
        switch (type) {// 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
            case 0:
                if ("0".equals(Contants.pubItemMap.get(ConfigItemPartMapMark.pubFlowRelateRule.toString()))) {// 流程图相关附件
                    // ,0:不公开
                    // ，所以不关联发布
                    return false;
                }
            case 1:
            case 2:
            case 3:
                break;
            case 4:
                if ("0".equals(Contants.pubItemMap.get(ConfigItemPartMapMark.pubFlowMapRelateRule.toString()))) {// 流程地图相关附件
                    // ,0:不公开
                    // ，所以不关联发布
                    return false;
                }
        }
        return true;
    }

    /**
     * 发布流程对应的相关制度
     *
     * @param listRuleIdsNoRelease 流程对应的相关制度
     * @param historyNew
     * @throws Exception
     */
    private static void pubRefRule(List<Map<String, Object>> listRuleIdsNoRelease, TaskHistoryNew historyNew,
                                   RuleMapper ruleMapper, DocMapper docMapper) throws Exception {
        // 制度文件
        List<Long> listRuleFileIds = new ArrayList<Long>();
        // (key:制度主键ID，value：文件版本ID)
        Map<Long, Long> mapRuleFileVersion = new HashMap<Long, Long>();
        // 制度模板文件
        List<Long> listRuleModeIds = new ArrayList<Long>();
        for (Map<String, Object> obj : listRuleIdsNoRelease) {
            if (obj.get("ID") == null || obj.get("IS_DIR") == null || obj.get("VERSION_ID") == null) {
                continue;
            }
            if ("1".equals(obj.get("IS_DIR").toString())) {
                listRuleModeIds.add(Long.valueOf(obj.get("ID").toString()));
            } else {
                listRuleFileIds.add(Long.valueOf(obj.get("ID").toString()));
                mapRuleFileVersion.put(Long.valueOf(obj.get("ID").toString()),
                        Long.valueOf(obj.get("VERSION_ID").toString()));
            }
        }
        if (listRuleFileIds.size() > 0) {
            List<RuleT> listRuleT = ruleMapper.fetchRuleTByIds(SqlCommon.getIds(listRuleFileIds));
            // 文控信息
            for (Long ruleId : listRuleFileIds) {
                TaskHistoryNew jecnTaskHistoryNew = DocControlCommon.getTaskHistoryNew(historyNew);
                jecnTaskHistoryNew.setRelateId(ruleId);
                jecnTaskHistoryNew.setType(3);
                // 文控版本号
                jecnTaskHistoryNew.setVersionId(getRuleVersionValue());
                // 文件版本Id
                jecnTaskHistoryNew.setFileContentId(mapRuleFileVersion.get(ruleId));
                // 保存文控信息
                docMapper.saveJecnTaskHistoryNewOracle(jecnTaskHistoryNew);
                // 记录制度发布对应当前版本的ID
                updateRuleT(listRuleT, ruleId, jecnTaskHistoryNew.getId(), ruleMapper);
            }
            releaseRuleFileData(listRuleFileIds, ruleMapper);
        }
        if (listRuleModeIds.size() > 0) {
            String ids = SqlCommon.getIds(listRuleModeIds);
            List<RuleT> listRuleT = ruleMapper.fetchRuleTByIds(ids);
            // 文控信息
            for (Long id : listRuleModeIds) {
                TaskHistoryNew jecnTaskHistoryNew = DocControlCommon.getTaskHistoryNew(historyNew);
                jecnTaskHistoryNew.setRelateId(id);
                jecnTaskHistoryNew.setType(2);
                // 通过制度Id获得制度模板文件的程序文件
                ruleFilePath(id, jecnTaskHistoryNew, ruleMapper, docMapper);
                // 文控版本号
                jecnTaskHistoryNew.setVersionId(getRuleVersionValue());
                // 保存文控信息
                if (BaseSqlProvider.isOracle()) {
                    docMapper.saveJecnTaskHistoryNewOracle(jecnTaskHistoryNew);
                } else {
                    docMapper.saveJecnTaskHistoryNewSqlServer(jecnTaskHistoryNew);
                }

                // 记录制度发布对应当前版本的ID
                // FIXME 获得返回的主键id
                updateRuleT(listRuleT, id, jecnTaskHistoryNew.getId(), ruleMapper);
            }
            releaseRuleModeData(listRuleModeIds, ruleMapper);
        }
    }

    private static void releaseRuleFileData(List<Long> listIds, RuleMapper ruleMapper) throws Exception {

        if (listIds == null || listIds.size() == 0) {
            return;
        }

        // 删除老版本的数据
        revokeRuleFileData(listIds, ruleMapper);

        // 保存文件需要发布的数据
        saveRuleFileData(listIds, ruleMapper);

        // 每次发布，正式表记录当前版本数据
        releaseRuleFileContents(listIds, ruleMapper);

    }

    private static void releaseRuleFileContents(List<Long> listIds, RuleMapper ruleMapper) throws Exception {

        if (listIds == null || listIds.size() == 0) {
            return;
        }

        String ids = SqlCommon.getIds(listIds);
        // as id, FC.FILE_NAME as fileName, FC.UPDATE_PEOPLE_ID as
        // updatePeopleId, FC.FILE_STREAM fileStream
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", ids);

        List<Map<String, Object>> fileContentLists = ruleMapper.fetchRuleTRelatedContentByIds(paramMap);
        List<Rule> rules = ruleMapper.fetchRuleByIds(paramMap);
        // 发布时 正式文件记录当前版本
        G020RuleFileContent curContent = null;
        Date curDate = new Date();
        for (Map<String, Object> m : fileContentLists) {
            Long id = Long.valueOf(m.get("ID").toString());
            String fileName = m.get("FILE_NAME").toString();
            Long updatePeopleId = Long.valueOf(m.get("UPDATE_PEOPLE_ID").toString());
            Blob b = (Blob) m.get("FILE_STREAM");
            byte[] fileStream = b.getBytes(1, (int) b.length());
            curContent = new G020RuleFileContent();
            curContent.setRuleId(id);
            curContent.setFileName(fileName);
            curContent.setUpdatePeopleId(updatePeopleId);
            curContent.setUpdateTime(curDate);
            curContent.setIsVersionLocal(1);
            curContent.setFileStream(fileStream);
            // 保存真实表的文件版本
            saveCurContent(curContent, ruleMapper);
            // 更新真实表对应文件版本ID
            updateRuleBeanContent(ruleMapper, rules, curContent.getId(), curContent.getRuleId());
        }

    }

    private static void saveCurContent(G020RuleFileContent curContent, RuleMapper ruleMapper) {
        if (BaseSqlProvider.isOracle()) {
            ruleMapper.saveRuleFileContentOracle(curContent);
        } else {
            ruleMapper.saveRuleFileContentSqlServer(curContent);
        }

    }

    private static void saveRuleFileData(List<Long> listFileIds, RuleMapper ruleMapper) {

        if (listFileIds == null || listFileIds.size() == 0) {
            return;
        }

        int size = listFileIds.size();
        int batch = 100;
        // 余数
        int remainder = size % batch;
        // 整数
        int integral = size - remainder;
        List<Long> batchIds = null;
        if (integral > 0) {
            for (int i = 0; i < integral; i = i + batch) {
                batchIds = listFileIds.subList(i, i + batch);

                saveRuleFileDataBatch(batchIds, ruleMapper);
            }
        }
        if (remainder > 0) {
            int start = integral;
            int end = size;
            batchIds = listFileIds.subList(start, end);

            saveRuleFileDataBatch(batchIds, ruleMapper);
        }

    }

    private static void saveRuleFileDataBatch(List<Long> listFileIds, RuleMapper ruleMapper) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", SqlCommon.getIds(listFileIds));

        // 岗位查阅权限
        ruleMapper.insertAccessPermissions(paramMap);
        // 部门查阅权限
        ruleMapper.insertOrgAccessPermissions(paramMap);
        // 岗位组查阅权限
        ruleMapper.insertGroupPermissions(paramMap);
        // 添加制度相关风险
        ruleMapper.insertRuleRisk(paramMap);
        // 添加制度相关标准
        ruleMapper.insertRuleStandard(paramMap);
        // 添加制度对象的信息
        ruleMapper.insertRule(paramMap);
    }

    private static void revokeRuleFileData(List<Long> listFileIds, RuleMapper ruleMapper) {
        if (listFileIds == null || listFileIds.size() == 0) {
            return;
        }

        int size = listFileIds.size();
        int batch = 100;
        // 余数
        int remainder = size % batch;
        // 整数
        int integral = size - remainder;
        List<Long> batchIds = null;
        if (integral > 0) {
            for (int i = 0; i < integral; i = i + batch) {
                batchIds = listFileIds.subList(i, i + batch);

                revokeRuleFileDataBatch(listFileIds, ruleMapper);
            }
        }
        if (remainder > 0) {
            int start = integral;
            int end = size;
            batchIds = listFileIds.subList(start, end);

            revokeRuleFileDataBatch(batchIds, ruleMapper);
        }

    }

    private static void revokeRuleFileDataBatch(List<Long> listFileIds, RuleMapper ruleMapper) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", SqlCommon.getIds(listFileIds));
        // 删除岗位查阅权限
        ruleMapper.deleteAccessPermissions(paramMap);
        // 删除部门查阅权限
        ruleMapper.deleteOrgAccessPermissions(paramMap);
        // 删除岗位组查阅权限
        ruleMapper.deleteGroupPermissions(paramMap);
        // 删除制度相关风险
        ruleMapper.deleteRuleRisk(paramMap);
        // 删除制度相关标准
        ruleMapper.deleteRuleStandard(paramMap);
        // 删除制度对象的信息
        ruleMapper.deleteRule(paramMap);
    }

    /**
     * 记录制度发布对应当前版本的ID
     *
     * @param listRuleT 未发布文件
     * @param ruleId    制度
     * @param historyId 文控信息版本ID
     */
    private static void updateRuleT(List<RuleT> listRuleT, Long ruleId, Long historyId, RuleMapper ruleMapper) {
        for (RuleT ruleT : listRuleT) {
            if (ruleT.getId().equals(ruleId)) {
                ruleT.setHistoryId(historyId);
                ruleT.setUpdateDate(new Date());
                ruleMapper.updateRuleT(ruleT);
                return;
            }
        }
    }

    /**
     * 获取系统配置制度默认版本号
     *
     * @return
     */
    public static String getRuleVersionValue() {
        // 获取文件系统配置默认的版本号 暂用随机数，系统配置暂取消
        // return configItemDao.selectValue(2, "basicRuleAutoPubVersion");
        return UUID.randomUUID().toString();
    }

    /**
     * 发布获取保存路径下的图片拷贝到发布图片路径下，并记录当前生成当前版本文件
     *
     * @param flowStructureDao
     * @param refId
     * @param savePath
     * @param oldPath
     * @param docControlDao
     * @param configItemDao
     * @param processBasicDao
     * @param processKPIDao
     * @param processRecordDao
     * @param nodeType
     * @param isRecordVersion
     *            true：记录版本信息，false不记录版本信息
     * @return 生成的版本文件路径
     * @throws Exception
     */
    // FIXME
    // public static String getPubImagePath(IFlowStructureDao flowStructureDao,
    // Long refId, String savePath,
    // String oldPath, IJecnDocControlDao docControlDao, IJecnConfigItemDao
    // configItemDao,
    // IProcessBasicInfoDao processBasicDao, IProcessKPIDao processKPIDao,
    // IProcessRecordDao processRecordDao,
    // TreeNodeType nodeType, boolean isRecordVersion) throws Exception {
    // // 生成bute[] 根据保存时生成的图片路径;
    // byte[] bytes = JecnFinal.getBytesByFilePath(savePath);
    // String pubImagePath = "";
    // if (bytes != null) {
    // pubImagePath = JecnFinal.getImagePathByPub(bytes);
    // }
    // String filePath = "";
    // if (isRecordVersion) {// true记录版本信息
    // // 获得流程地图和流程发布时候所生成的流程文件
    // JecnCreateDoc createDoc = DownloadUtil.getFlowFile(refId, nodeType,
    // false, configItemDao, docControlDao,
    // flowStructureDao, processBasicDao, processKPIDao, processRecordDao);
    // if (createDoc.getBytes() != null) {
    // log.info("获取word操作说明文件信息成功！");
    // filePath = JecnFinal.saveFilePath(JecnFinal.FLOW_FILE_RELEASE, new
    // Date(), ".doc");
    // JecnFinal.createFile(filePath, createDoc.getBytes());
    // }
    // }
    // if (!JecnCommon.isNullOrEmtryTrim(pubImagePath)) {// 生成图片路径
    // // 删除历史存在的文件
    // if (!JecnCommon.isNullOrEmtryTrim(oldPath)) {
    // // 删除文件
    // JecnFinal.deleteFile(oldPath);
    // // 删除目录
    // JecnFinal.doDelete(JecnCommon.getSavePorcessImagePath(oldPath));
    // }
    //
    // JecnFlowStructureT structureT = flowStructureDao.get(refId);
    // if (structureT.getIsFlow() == 1 && structureT.getPagings() > 1) {//
    // 流程，切存在分页
    // copyFileDir(savePath, pubImagePath);
    // }
    // // 更新发布后新生成的图片路径
    // String hql = "update JecnFlowStructure set filePath=? where flowId=?";
    // flowStructureDao.execteHql(hql, pubImagePath, refId);
    // flowStructureDao.getSession().flush();
    // log.info("更新JecnFlowStructure （发布生成的图片路径）信息成功！");
    // }
    // return filePath;
    // }

    /**
     * 发布流程的信息
     *
     * @param flowId
     * @param processMapper
     */
    private static void releaseProcessData(Long flowId, ProcessMapper processMapper) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("type", "0");
        paramMap.put("flowId", flowId);
        paramMap.put("rid", flowId);

        // 清除老版本的数据
        revokeProcessRelease(flowId, processMapper);
        // 1发布活动的样例
        processMapper.insertTemplet(paramMap);

        // 添加活动输出的数据
        processMapper.insertModeFileT(paramMap);

        // 添加活动的输入和操作规范的数据
        processMapper.insertActivityFileT(paramMap);

        // 添加活动的指标的数据
        processMapper.insertRefIndicatorst(paramMap);

        // 添加角色的岗位的数据
        processMapper.insertProcessStationRelatedT(paramMap);
        // 添加线的线段的数据
        processMapper.insertLineSegmentT(paramMap);

        // 角色活动关系表
        processMapper.insertRoleActiveT(paramMap);
        // 添加图形的数据
        processMapper.insertFlowStructureImageT(paramMap);
        // 发布流程基本信息
        processMapper.insertFlowBasicInfoT(paramMap);

        // 添加相关标准的数据
        processMapper.insertFlowStandardT(paramMap);

        // 添加相关制度的数据
        processMapper.insertRelatedCriterionT(paramMap);

        // 发布流程责任部门
        processMapper.insertFlowRelatedOrgT(paramMap);

        // 发布流程支持工具
        processMapper.insertFlowToolRelatedT(paramMap);

        // 发布流程保持记录
        processMapper.insertFlowRecordT(paramMap);

        // 发布流程KPI则基本信息
        processMapper.insertFlowKpiName(paramMap);
        // 流程KPI对应的支持工具关系数据
        processMapper.insertSustainToolConn(paramMap);

        // 发布流程驱动规则基本信息
        processMapper.insertFlowDriverT(paramMap);

        // 岗位查阅权限
        processMapper.insertAccessPermissionsT(paramMap);

        // 部门查阅权限
        processMapper.insertOrgAccessPermT(paramMap);

        // 岗位组查阅权限
        processMapper.insertOrgGroupPermT(paramMap);
        /** ************ 【二期新增】 ********************** */
        // 活动线上信息
        processMapper.insertActiveOnline(paramMap);
        // 活动控制点
        processMapper.insertControlPoint(paramMap);
        // 活动相关标准
        processMapper.insertActiveStandard(paramMap);
        // 发布流程地图元素附件表
        processMapper.insertFigureFile(paramMap);
        // 发布流程对象
        processMapper.insertFlowStructureT(paramMap);
        // 发布to from关联关系表
        processMapper.insertToFromActive(paramMap);

    }

    private static void revokeProcessRelease(Long flowId, ProcessMapper processMapper) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("flowId", flowId);
        paramMap.put("rid", flowId);
        paramMap.put("type", "0");

        // 删除样例表的数据
        processMapper.deleteTemplet(paramMap);
        // 删除活动输出的数据
        processMapper.deleteModeFile(paramMap);
        // 删除活动的输入和操作规范的数据
        processMapper.deleteActivityFile(paramMap);
        // 删除活动的指标的数据
        processMapper.deleteRefIndicators(paramMap);
        // 删除角色的岗位的数据
        processMapper.deleteProcessStationRelated(paramMap);
        // 删除线的线段的数据
        processMapper.deleteLineSegment(paramMap);
        // 删除角色和活动的关系数据
        processMapper.deleteRoleActive(paramMap);
        processMapper.deleteTempletTwo(paramMap);
        /** *********** 【二期需求】 **************** */
        // 活动线上信息（活动与信息化关系临时表）
        processMapper.deleteActiveOnline(paramMap);

        // 活动相关控制点
        processMapper.deleteControlPoint(paramMap);

        // 活动相关标准
        processMapper.deleteActiveStandard(paramMap);
        // 删除流程图文档元素附件
        processMapper.deleteFigureFile(paramMap);
        // 删除图形的数据
        processMapper.deleteFlowStructureImage(paramMap);
        // 删除相关标准的数据
        processMapper.deleteFlowStandard(paramMap);
        // 删除相关制度的数据
        processMapper.deleteFlowRelatedCriterion(paramMap);
        // 删除责任部门
        processMapper.deleteFlowRelated(paramMap);
        // 删除流程图相关支持工具
        processMapper.deleteFlowToolRelated(paramMap);
        // 删除流程记录(流程图操作说明记录保存表)
        processMapper.deleteFlowRecord(paramMap);

        // 清除和KPI对应不上的数据
        processMapper.deleteFlowKpi(paramMap);
        // 流程KPI与支持工具关系数据
        processMapper.deleteSustainToolConn(paramMap);
        // 删除流程KPI
        processMapper.deleteFlowKpiName(paramMap);

        // 删除流程驱动规则(流程图时间驱动表)
        processMapper.deleteFlowDriver(paramMap);
        // 删除流程基本信息
        processMapper.deleteFlowBasicInfo(paramMap);
        // 删除岗位查阅权限
        processMapper.deleteAccessPermissions(paramMap);
        // 删除部门查阅权限
        processMapper.deleteOrgAccessPermission(paramMap);
        // 删除岗位组查阅权限
        processMapper.deleteGroupPermission(paramMap);

        // 删除发布时生成的临时图片
        FlowStructure flowStructure = processMapper.getFlowStructure(flowId);
        if (flowStructure != null) {
            Final.deleteFile(flowStructure.getFilePath());
        }
        // 删除流程对象
        processMapper.deleteFlowStructure(paramMap);

        // 删除to from 和活动的关联关系
        processMapper.deleteToFromActive(paramMap);

    }

    /**
     * 获取流程中角色参与的人员ID集合
     *
     * @param userMapper
     * @param flowId     流程ID
     * @return List<Long> 人员ID集合
     */
    private static List<JecnUser> getPartInPeopleIds(UserMapper userMapper, Long flowId) {
        if (userMapper == null || flowId == null) {
            throw new NullPointerException(
                    "JecnTaskCommon中方法getPartInPeopleIds（）docControlDao == null || flowId == null");
        }

        /** 获取流程中参与的人员ID */
        return userMapper.fetchRoleRelatedUser(flowId);
    }

    /**
     * 流程发布删除老数据，出入新数据
     *
     * @param processId
     */
    private static void releaseProcessMapData(ProcessMapper processMapper, Long processId) {
        // 删除流程地图老版本的数据
        revokeProcessMapRelease(processMapper, processId);
        // 插入数据
        insertProcessMapRelease(processMapper, processId);
    }

    private static void insertProcessMapRelease(ProcessMapper processMapper, Long processId) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("processId", processId);
        paramMap.put("type", "0");

        // 发布流程地图元素附件表
        processMapper.insertFigureFile(paramMap);

        // 添加线的线段的数据
        processMapper.insertLineSegment(paramMap);

        // 添加图形的数据
        processMapper.insertFlowStructureImage(paramMap);

        // 发布流程地图基本信息(信息表包括流程地图关联附件ID)
        processMapper.insertMainFlow(paramMap);

        // 岗位查阅权限
        processMapper.insertAccessPermissions(paramMap);

        // 部门查阅权限
        processMapper.insertOrgAccessPermissions(paramMap);

        // 岗位组查阅权限
        processMapper.insertGroupPermissions(paramMap);

        // 发布流程对象
        processMapper.insertFlowStructure(paramMap);

        // 添加相关制度的数据
        processMapper.insertFlowRelatedCriterion(paramMap);

    }

    private static void revokeProcessMapRelease(ProcessMapper processMapper, Long processId) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("flowId", processId);

        // 删除线的线段的数据
        processMapper.deleteLineSegment(paramMap);

        // 删除流程地图元素对应附件信息
        processMapper.deleteFigureFile(paramMap);

        // 删除流程地图相关制度表
        processMapper.deleteFlowRelatedCriterion(paramMap);

        // 删除图形元素的数据
        processMapper.deleteFlowStructureImage(paramMap);

        // 删除流程地图基本信息(信息表包括流程地图关联附件ID)
        processMapper.deleteMainFlow(paramMap);

        // 删除流程地图的岗位查阅权限
        processMapper.deleteAccessPermissions(paramMap);

        // 删除流程地图的部门查阅权限
        processMapper.deleteOrgAccessPermissions(paramMap);

        // 删除岗位组查阅权限
        processMapper.deleteGroupPermissions(paramMap);

        // 删除发布时生成的临时图片
        FlowStructure flowStructure = processMapper.getFlowStructure(processId);
        if (flowStructure != null) {
            Final.deleteFile(flowStructure.getFilePath());
        }
        // 删除流程地图对象
        processMapper.deleteFlowStructure(paramMap);

    }

    private static Set<Long> getAccessPeopleIds(UserMapper userMapper, Integer type, Long rid) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("type", type);
        paramMap.put("rid", rid);
        return userMapper.fetchPosAccessPermissApproveByType(paramMap);
    }

    private static void pubRuleLocalFile(TaskHistoryNew historyNew, DocMapper docMapper, UserMapper userMapper,
                                         RuleMapper ruleMapper, FileMapper fileMapper) throws Exception {

        Long refId = historyNew.getRelateId();

        // 制度文件
        List<Long> listRuleFileIds = new ArrayList<Long>();
        listRuleFileIds.add(refId);

        RuleT ruleT = ruleMapper.getRuleT(refId);
        historyNew.setFileContentId(ruleT.getFileId());

        // 保存文控信息
        if (BaseSqlProvider.isOracle()) {
            docMapper.saveJecnTaskHistoryNewOracle(historyNew);
        } else {
            docMapper.saveJecnTaskHistoryNewSqlServer(historyNew);
        }

        ruleT.setHistoryId(historyNew.getId());
        Date updateDate = new Date();
        // 设置更新时间
        ruleT.setUpdateDate(updateDate);
        // FIXME 更新人需不需要？
        ruleMapper.updateRuleT(ruleT);
        // 发布制度
        releaseRuleFileData(listRuleFileIds, ruleMapper);
        // // 更新发布后的制度版本信息
        // Rule rule = ruleMapper.getRule(refId);
        // rule.setHistoryId(historyNew.getId());
        // // FIXME 如下是我添加的
        // rule.setUpdateDate(updateDate);
        // ruleMapper.updateRule(rule);

        // FIXME 查阅权限 消息，邮件
        // Set<Long> accessPeopleIdS = new HashSet<Long>();
        // 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
        // accessPeopleIdS = getAccessPeopleIds(userMapper, 3, refId);
        // String prfName = ruleT.getRuleName();
        // setMessageByAccess(docMapper, configItemDao, accessPeopleIdS,
        // historyNew.getRelateId(), prfName,
        // historyNew.getType());

    }

    /**
     * 制度文件
     *
     * @param historyNew
     * @param docMapper
     * @param ruleMapper
     * @param configItemDao
     * @param fileMapper
     * @throws Exception
     */
    private static void pubRuleFile(TaskHistoryNew historyNew, DocMapper docMapper, UserMapper userMapper,
                                    RuleMapper ruleMapper, FileMapper fileMapper) throws Exception {

        Long refId = historyNew.getRelateId();


        // 制度文件
        List<Long> listRuleFileIds = new ArrayList<Long>();
        listRuleFileIds.add(refId);

        RuleT ruleT = ruleMapper.getRuleT(refId);

        Long fileId = ruleT.getFileId();
        if (fileId == null) {
            log.warn("制度id：" + ruleT.getId() + "关联的文件id为空");
            return;
        }
        FileBeanT jecnFileBean = fileMapper.getFileBeanT(fileId);
        if (jecnFileBean == null) {
            log.warn("制度id：" + ruleT.getId() + "关联的文件id：" + fileId + "的文件被删除");
            return;
        }
        historyNew.setFileContentId(jecnFileBean.getVersionId());
        // 保存文控信息
        if (BaseSqlProvider.isOracle()) {
            docMapper.saveJecnTaskHistoryNewOracle(historyNew);
        } else {
            docMapper.saveJecnTaskHistoryNewSqlServer(historyNew);
        }

        ruleT.setHistoryId(historyNew.getId());
        // 设置更新时间
        ruleT.setUpdateDate(new Date());
        ruleMapper.updateRuleT(ruleT);
        // 发布制度
        releaseRuleFileData(listRuleFileIds, ruleMapper);
        // // FIXME 发布的时候已经将临时表的数据同步到正式表 更新发布后的制度版本信息
        // Rule rule = ruleMapper.getRule(refId);
        // rule.setHistoryId(historyNew.getId());

        // 发布制度相关文件
        pubRuleRelateFile(historyNew, docMapper, fileMapper);
        // FIXME 查阅权限 消息，邮件
        // Set<Long> accessPeopleIdS = new HashSet<Long>();
        // String prfName = ruleT.getRuleName();
        // // 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
        // accessPeopleIdS = getAccessPeopleIds(userMapper, 3, refId);
        // setMessageByAccess(docMapper, configItemDao, accessPeopleIdS,
        // historyNew.getRelateId(), prfName,
        // historyNew.getType());
    }

    /**
     * 发布流程相关文件
     *
     * @param historyNew TaskHistoryNew
     * @param docMapper  DocMapper
     * @param fileMapper FileMapper
     * @throws Exception
     */
    private static void pubFlowRelateFile(TaskHistoryNew historyNew, DocMapper docMapper, FileMapper fileMapper)
            throws Exception {
        if (!validateRelateFile(historyNew.getType())) {
            return;
        }
        // 获取流程未发布的文件
        List<Map<String, Object>> listFileObjects = findProcessNoPubFiles(historyNew.getRelateId(), docMapper);
        // 发布流程、制度，处理其相关的未发布文件
        pubRefFiles(listFileObjects, historyNew, getFileVersionValue(), 1, fileMapper, docMapper);
    }

    private static List<Map<String, Object>> findProcessNoPubFiles(Long flowId, DocMapper docMapper) {
        return docMapper.fetchProcessNoPubFiles(flowId);
    }

    /**
     * 发布流程地图相关文件
     *
     * @param historyNew
     * @param docMapper
     * @param fileMapper
     * @throws Exception
     */
    private static void pubFlowMapRelateFile(TaskHistoryNew historyNew, DocMapper docMapper, FileMapper fileMapper)
            throws Exception {
        if (!validateRelateFile(historyNew.getType())) {
            return;
        }
        // 获取流程未发布的文件
        List<Map<String, Object>> fileList = findProcessMapNoPubFiles(historyNew.getRelateId(), docMapper);
        // 发布流程、制度，处理其相关的未发布文件
        pubRefFiles(fileList, historyNew, getFileVersionValue(), 1, fileMapper, docMapper);
    }

    /**
     * 获取系统配置文件默认版本号
     *
     * @return
     */
    public static String getFileVersionValue() {
        // 获取文件系统配置默认的版本号 暂用随机数，系统配置暂取消
        // return configItemDao.selectValue(3, "basicFileAutoPubVersion");
        return UUID.randomUUID().toString();
    }

    /**
     * 获得流程地图未发布/更新的文件
     * <p>
     * 对这个方法修改时，必须对JecnUtil类findProcessMapNoPubFiles方法进行修改
     *
     * @param flowId
     * @return
     * @throws Exception
     */
    public static List<Map<String, Object>> findProcessMapNoPubFiles(Long flowId, DocMapper docMapper)
            throws Exception {

        return docMapper.fetchProcessMapNoPubFiles(flowId);
    }

    /**
     * 验证流程、流程地图、制度模板文件 相关附件是否关联发布
     *
     * @param type 相关类型 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
     * @return true:关联发布
     */
    public static boolean validateRelateFile(int type) {
        // 验证配置是否发布关联的文件信息
        switch (type) {// 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
            case 0:
                if ("0".equals(Contants.pubItemMap.get(ConfigItemPartMapMark.pubFlowRelateFile.toString()))) {// 流程图相关附件0:不关联发布
                    return false;
                }
            case 1:
                break;
            case 2:
                if ("0".equals(Contants.pubItemMap.get(ConfigItemPartMapMark.pubRuleFile.toString()))) {// 制度相关附件0:不关联发布
                    return false;
                }
            case 3:
                break;
            case 4:
                if ("0".equals(Contants.pubItemMap.get(ConfigItemPartMapMark.pubFlowMapRelateFile.toString()))) {// 流程地图相关附件
                    // ,0:不关联发布
                    return false;
                }
        }
        return true;
    }

    /**
     * 发布制度模板相关文件
     *
     * @param historyNew
     * @param docMapper
     * @param fileMapper
     * @throws Exception
     */
    private static void pubRuleModelRelateFile(TaskHistoryNew historyNew, DocMapper docMapper, FileMapper fileMapper)
            throws Exception {
        if (!validateRelateFile(historyNew.getType())) {
            return;
        }
        // 未发布文件
        List<Map<String, Object>> listFileObjects = findRuleModeNoPubFiles(historyNew.getRelateId(), docMapper);
        if (listFileObjects.size() > 0) {
            // 发布流程、制度，处理其相关的未发布文件
            pubRefFiles(listFileObjects, historyNew, getFileVersionValue(), 1, fileMapper, docMapper);
        }
    }

    private static List<Map<String, Object>> findRuleModeNoPubFiles(Long relateId, DocMapper docMapper) {

        return docMapper.fetchRuleModeNoPubFiles(relateId);
    }

    /**
     * 发布制度相关文件
     *
     * @param historyNew
     * @param docMapper
     * @param fileMapper
     * @throws Exception
     */
    private static void pubRuleRelateFile(TaskHistoryNew historyNew, DocMapper docMapper, FileMapper fileMapper)
            throws Exception {
        // 未发布文件
        List<Map<String, Object>> listFileObjects = findRuleFileNoPubFiles(historyNew.getRelateId(), docMapper);
        if (listFileObjects.size() > 0) {
            // 发布流程、制度，处理其相关的未发布文件
            pubRefFiles(listFileObjects, historyNew, getFileVersionValue(), 1, fileMapper, docMapper);
        }
    }

    private static List<Map<String, Object>> findRuleFileNoPubFiles(Long relateId, DocMapper docMapper) {
        return docMapper.fetchRuleFileNoPubFiles(relateId);
    }

    /**
     * 发布流程、制度，处理其相关的未发布文件
     *
     * @param listFileObjects 文件信息集合
     * @param historyNew      版本信息
     * @param configVersion
     * @param type            相关类型 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
     * @param fileMapper      文件DAO接口
     * @param docMapper       文控信息处理接口
     * @throws Exception
     */
    public static void pubRefFiles(List<Map<String, Object>> listFileObjects, TaskHistoryNew historyNew,
                                   String configVersion, int type, FileMapper fileMapper, DocMapper docMapper) throws Exception {
        if (listFileObjects == null || listFileObjects.size() == 0) {
            return;
        }
        if (!validateRelateFile(type)) {// 相关文件是否关联发布
            return;
        }

        // 文件发布文件
        List<Long> listFileIds = new ArrayList<Long>();
        // 记录文件相关版本ID<key文件ID，value：文件对应的版本ID>
        Map<Long, Long> fileIdToVersionId = new HashMap<Long, Long>();
        for (Map<String, Object> map : listFileObjects) {
            if (map == null) {
                continue;
            }
            // 文件ID
            Long fileId = Long.valueOf(map.get("FILEID").toString());
            Object versionObj = map.get("VERSIONID");
            // 文件版本ID
            Long versionId = null;
            if (versionObj == null) {
                log.error("pubRefFiles方法的文件id为：" + fileId + "的versionId为空");
            } else {
                // 文件版本ID
                versionId = Long.valueOf(versionObj.toString());
            }

            // 记录未发布的文件Id
            listFileIds.add(fileId);
            fileIdToVersionId.put(fileId, versionId);
        }
        if (listFileIds.size() > 0) {
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("ids", SqlCommon.getIds(listFileIds));
            List<FileBeanT> listFileT = fileMapper.fetchFileBeanT(paramMap);
            // 文控信息
            for (Long fileId : listFileIds) {
                TaskHistoryNew jecnTaskHistoryNew = DocControlCommon.getTaskHistoryNew(historyNew);
                jecnTaskHistoryNew.setRelateId(fileId);
                jecnTaskHistoryNew.setType(type);
                // 文控版本号
                jecnTaskHistoryNew.setVersionId(configVersion);
                // 文件版本Id
                jecnTaskHistoryNew.setFileContentId(fileIdToVersionId.get(fileId));
                // 保存文控信息
                if (BaseSqlProvider.isOracle()) {
                    docMapper.saveJecnTaskHistoryNewOracle(jecnTaskHistoryNew);
                } else {
                    docMapper.saveJecnTaskHistoryNewSqlServer(jecnTaskHistoryNew);
                }

                // 更新文件对应的文控信息版本ID
                updateFileT(listFileT, fileId, jecnTaskHistoryNew.getId(), fileMapper);
            }
            // 发布相关文件
            releaseFiles(listFileIds, fileMapper);
        }
    }

    public static void releaseFiles(List<Long> listFileIds, FileMapper fileMapper) throws Exception {

        // 删除原先的数据
        revokeFiles(listFileIds, fileMapper);

        // 插入最新的数据
        insertAndUpdateFile(listFileIds, fileMapper);

        // 发布文件，记录正式文件内容记录
        releaseFileContents(listFileIds, fileMapper);
    }

    private static void insertAndUpdateFile(List<Long> listFileIds, FileMapper fileMapper) {

        if (listFileIds == null || listFileIds.size() == 0) {
            return;
        }

        int size = listFileIds.size();
        int batch = 100;
        // 余数
        int remainder = size % batch;
        // 整数
        int integral = size - remainder;
        List<Long> batchIds = null;
        if (integral > 0) {
            for (int i = 0; i < integral; i = i + batch) {
                batchIds = listFileIds.subList(i, i + batch);

                insertAndUpdateFileBatch(batchIds, fileMapper);
            }
        }
        if (remainder > 0) {
            int start = integral;
            int end = size;
            batchIds = listFileIds.subList(start, end);

            insertAndUpdateFileBatch(batchIds, fileMapper);
        }

    }

    private static void insertAndUpdateFileBatch(List<Long> listFileIds, FileMapper fileMapper) {

        if (listFileIds == null || listFileIds.size() == 0) {
            return;
        }

        String ids = SqlCommon.getIds(listFileIds);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", ids);
        fileMapper.updateFileContentTypeToOneByIds(paramMap);
        // 查阅岗位
        fileMapper.insertAccessPermissionByIds(paramMap);
        // 查阅部门
        fileMapper.insertOrgAccessPermissionByIds(paramMap);
        // 将岗位组查阅权限从临时表插入到正式表
        fileMapper.insertGroupPermissionByIds(paramMap);
        // 插入正式表
        fileMapper.insertFilesByIds(paramMap);

    }

    public static void revokeFiles(List<Long> listFileIds, FileMapper fileMapper) throws Exception {
        if (listFileIds == null || listFileIds.size() == 0) {
            return;
        }

        int size = listFileIds.size();
        int batch = 100;
        // 余数
        int remainder = size % batch;
        // 整数
        int integral = size - remainder;
        List<Long> batchIds = null;
        if (integral > 0) {
            for (int i = 0; i < integral; i = i + batch) {
                batchIds = listFileIds.subList(i, i + batch);

                deleteFiles(batchIds, fileMapper);
            }
        }
        if (remainder > 0) {
            int start = integral;
            int end = size;
            batchIds = listFileIds.subList(start, end);

            deleteFiles(batchIds, fileMapper);
        }

    }

    private static void deleteFiles(List<Long> listFileIds, FileMapper fileMapper) {
        String ids = SqlCommon.getIds(listFileIds);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", ids);
        // 删除正式表
        fileMapper.deleteFileByIds(paramMap);
        // 删除岗位查阅权限
        fileMapper.deleteAccessPermissionByIds(paramMap);
        // 删除部门查阅权限
        fileMapper.deleteOrgAccessPermissionByIds(paramMap);
        // 删除岗位组查阅权限
        fileMapper.deleteGroupPermissionByIds(paramMap);
    }

    private static void releaseFileContents(List<Long> listFileIds, FileMapper fileMapper) throws Exception {

        String ids = SqlCommon.getIds(listFileIds);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", ids);
        List<Map<String, Object>> fileContentLists = fileMapper.fetchFileRelatedContent(paramMap);

        List<FileBean> fileBeans = fileMapper.fetchFileBeanByIds(paramMap);
        // 发布时 正式文件尽量当前版本
        FileContent curContent = null;
        Date curDate = new Date();
        for (Map<String, Object> obj : fileContentLists) {
            if (obj.get("FILESTREAM") == null) {
                continue;
            }
            curContent = new FileContent();
            curContent.setFileId(Long.valueOf(obj.get("FILEID").toString()));
            curContent.setFileName(obj.get("FILENAME").toString());
            curContent.setUpdatePeopleId(Long.valueOf(obj.get("UPDATEPEOPLEID").toString()));
            curContent.setUpdateTime(curDate);
            curContent.setType(Integer.valueOf(obj.get("TYPE").toString()));
            curContent.setIsVersionLocal(1);
            BlobImpl blob = (BlobImpl) obj.get("FILESTREAM");
            byte[] b = blob.getBytes(1, (int) blob.length());
            curContent.setFileStream(b);
            saveFileContent(curContent, fileMapper);
            updateFileBeanContent(fileMapper, fileBeans, curContent.getId(), curContent.getFileId());
        }
    }

    private static void saveFileContent(FileContent curContent, FileMapper fileMapper) {
        if (BaseSqlProvider.isOracle()) {
            fileMapper.saveFileContentOracle(curContent);
        } else {
            fileMapper.saveFileContentSqlServer(curContent);
        }

    }

    private static void updateFileBeanContent(FileMapper fileMapper, List<FileBean> fileBeans, Long contentId,
                                              Long fileId) throws Exception {
        for (FileBean jecnFileBean : fileBeans) {
            if (jecnFileBean.getFileId().toString().equals(fileId.toString())) {
                jecnFileBean.setVersionId(contentId);
                fileMapper.updateFileBean(jecnFileBean);
            }
        }
    }

    private static void updateRuleBeanContent(RuleMapper ruleMapper, List<Rule> ruleTs, Long contentId, Long ruleId)
            throws Exception {

        for (Rule rule : ruleTs) {
            if (rule.getId().toString().equals(ruleId.toString())) {
                rule.setFileId(contentId);
                ruleMapper.updateRule(rule);
            }
        }

    }

    /**
     * 更新文件对应的文控信息版本ID
     *
     * @param listFileT 未发布文件
     * @param fileId    文件I D
     * @param historyId 文控信息版本ID
     */
    private static void updateFileT(List<FileBeanT> listFileT, Long fileId, Long historyId, FileMapper fileMapper) {
        Date updateTime = new Date();
        for (FileBeanT jecnFileBeanT : listFileT) {
            if (jecnFileBeanT.getFileId().equals(fileId)) {
                jecnFileBeanT.setHistoryId(historyId);
                jecnFileBeanT.setUpdateTime(updateTime);
                fileMapper.updateFileBeanT(jecnFileBeanT);
                return;
            }
        }
    }

    /**
     * 制度模板文件
     *
     * @param historyNew
     * @param docMapper
     * @param ruleMapper
     * @param configItemDao
     * @param fileMapper
     * @throws Exception
     */
    private static void pubModeRuleFile(TaskHistoryNew historyNew, DocMapper docMapper, UserMapper userMapper,
                                        RuleMapper ruleMapper, FileMapper fileMapper) throws Exception {

        // 制度模板文件
        List<Long> listRuleModeFileIds = new ArrayList<Long>();
        listRuleModeFileIds.add(historyNew.getRelateId());

        RuleT ruleT = ruleMapper.getRuleT(historyNew.getRelateId());

        if (BaseSqlProvider.isOracle()) {
            // 保存文控信息
            docMapper.saveJecnTaskHistoryNewOracle(historyNew);
        } else {
            // 保存文控信息
            docMapper.saveJecnTaskHistoryNewSqlServer(historyNew);
        }

        // 更新制度对应的版本信息ID
        ruleT.setHistoryId(historyNew.getId());
        // 设置更新时间
        ruleT.setUpdateDate(new Date());
        ruleMapper.updateRuleT(ruleT);
        // 发布制度模板文件
        releaseRuleModeData(listRuleModeFileIds, ruleMapper);
        // 更新发布后的制度版本信息
        // Rule rule = ruleMapper.getRule(historyNew.getRelateId());
        // rule.setHistoryId(historyNew.getId());
        // 通过制度Id获得制度模板文件的程序文件
        ruleFilePath(historyNew.getRelateId(), historyNew, ruleMapper, docMapper);
        // 发布制度模板相关文件
        pubRuleModelRelateFile(historyNew, docMapper, fileMapper);

        // FIXME
        // Set<Long> accessPeopleIdS = new HashSet<Long>();
        // // 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
        // accessPeopleIdS = getAccessPeopleIds(userMapper, 3,
        // historyNew.getRelateId());
        // String prfName = ruleT.getRuleName();
        // // 查阅权限 消息，邮件
        // setMessageByAccess(docMapper, configItemDao, accessPeopleIdS,
        // historyNew.getRelateId(), prfName,
        // historyNew.getType());
    }

    /**
     * 制度模板文件操作说明路径获取
     *
     * @param rId
     * @param historyNew
     * @throws Exception
     */
    // FIXME 操作
    private static void ruleFilePath(Long rId, TaskHistoryNew historyNew, RuleMapper ruleMapper, DocMapper docMapper)
            throws Exception {
        // 通过制度Id获得制度模板文件的程序文件
        // CreateDoc createDoc = DownloadUtil.getRuleFile(rId, false,
        // ruleMapper, docMapper);
        // if (createDoc.getBytes() == null) {
        // return;
        // }
        // String filePath = Final.saveFilePath(Final.RULE_FILE_RELEASE, new
        // Date(), ".doc");
        // Final.createFile(filePath, createDoc.getBytes());
        // historyNew.setFilePath(filePath);
    }

    private static void releaseRuleModeData(List<Long> listIds, RuleMapper ruleMapper) {

        revokeRuleModeData(listIds, ruleMapper);

        saveRuleModeData(listIds, ruleMapper);

    }

    private static void saveRuleModeData(List<Long> listIds, RuleMapper ruleMapper) {

        if (listIds == null || listIds.size() == 0) {
            return;
        }

        int size = listIds.size();
        int batch = 100;
        // 余数
        int remainder = size % batch;
        // 整数
        int integral = size - remainder;
        List<Long> batchIds = null;
        if (integral > 0) {
            for (int i = 0; i < integral; i = i + batch) {
                batchIds = listIds.subList(i, i + batch);

                saveRuleModeDataBatch(batchIds, ruleMapper);
            }
        }
        if (remainder > 0) {
            int start = integral;
            int end = size;
            batchIds = listIds.subList(start, end);

            saveRuleModeDataBatch(batchIds, ruleMapper);
        }

    }

    private static void saveRuleModeDataBatch(List<Long> listIds, RuleMapper ruleMapper) {

        if (listIds == null || listIds.size() == 0) {
            return;
        }

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", SqlCommon.getIds(listIds));
        paramMap.put("type", 3);

        // 岗位查阅权限
        ruleMapper.insertAccessPermissions(paramMap);
        // 部门查阅权限
        ruleMapper.insertOrgAccessPermissions(paramMap);
        // 将岗位组查阅权限从临时表插入到正式表
        ruleMapper.insertGroupPermissions(paramMap);
        // 添加制度文件
        ruleMapper.insertRuleFile(paramMap);
        // 添加制度内容
        ruleMapper.insertRuleContent(paramMap);
        // 添加制度的标题
        ruleMapper.insertRuleTitle(paramMap);
        // 添加制度相关风险
        ruleMapper.insertRuleRisk(paramMap);
        // 添加制度相关标准
        ruleMapper.insertRuleStandard(paramMap);
        // 添加制度对象的信息
        ruleMapper.insertRule(paramMap);

    }

    private static void revokeRuleModeData(List<Long> listFileIds, RuleMapper ruleMapper) {

        if (listFileIds == null || listFileIds.size() == 0) {
            return;
        }

        int size = listFileIds.size();
        int batch = 100;
        // 余数
        int remainder = size % batch;
        // 整数
        int integral = size - remainder;
        List<Long> batchIds = null;
        if (integral > 0) {
            for (int i = 0; i < integral; i = i + batch) {
                batchIds = listFileIds.subList(i, i + batch);

                revokeRuleModeDataBatch(batchIds, ruleMapper);
            }
        }
        if (remainder > 0) {
            int start = integral;
            int end = size;
            batchIds = listFileIds.subList(start, end);

            revokeRuleModeDataBatch(batchIds, ruleMapper);
        }

    }

    private static void revokeRuleModeDataBatch(List<Long> listFileIds, RuleMapper ruleMapper) {

        if (listFileIds == null || listFileIds.size() == 0) {
            return;
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", SqlCommon.getIds(listFileIds));
        paramMap.put("type", 3);

        // 删除岗位查阅权限
        ruleMapper.deleteAccessPermissions(paramMap);
        // 删除部门查阅权限
        ruleMapper.deleteOrgAccessPermissions(paramMap);
        // 删除岗位组查阅权限
        ruleMapper.deleteGroupPermissions(paramMap);

        // 删除制度文件
        ruleMapper.deleteRuleFile(paramMap);
        // 删除制度内容
        ruleMapper.deleteRuleContent(paramMap);
        // 删除制度的标题
        ruleMapper.deleteRuleTitle(paramMap);
        // 删除制度相关风险
        ruleMapper.deleteRuleRisk(paramMap);
        // 删除制度相关标准
        ruleMapper.deleteRuleStandard(paramMap);
        // 删除制度对象的信息
        ruleMapper.deleteRule(paramMap);

    }

    /**
     * 发布文件
     *
     * @param historyNew
     * @param docMapper
     * @param fileMapper
     * @throws Exception
     */
    private static void pubFile(TaskHistoryNew historyNew, DocMapper docMapper, UserMapper userMapper,
                                FileMapper fileMapper) throws Exception {

        FileBeanT fileBeanT = fileMapper.getFileBeanT(historyNew.getRelateId());
        if (fileBeanT == null) {
            throw new NullPointerException("文件发布，文件为空，文件id为：" + historyNew.getRelateId());
        }

        historyNew.setFileContentId(fileBeanT.getVersionId());
        // 保存文控信息
        if (BaseSqlProvider.isOracle()) {
            docMapper.saveJecnTaskHistoryNewOracle(historyNew);
        } else {
            docMapper.saveJecnTaskHistoryNewSqlServer(historyNew);
        }

        List<Long> listFileIds = new ArrayList<Long>();
        listFileIds.add(fileBeanT.getFileId());

        // 设置发布的版本id
        fileBeanT.setHistoryId(historyNew.getId());
        // 设置更新时间
        fileBeanT.setUpdateTime(new Date());
        fileMapper.updateFileBeanT(fileBeanT);

        // 发布文件
        releaseFiles(listFileIds, fileMapper);

        // FIXME 查阅权限 消息，邮件
        // String prfName = jecnFileBeanT.getFileName();
        // Set<Long> accessPeopleIdS = new HashSet<Long>();
        // // 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
        // accessPeopleIdS = getAccessPeopleIds(userMapper, 1,
        // historyNew.getRelateId());
        // setMessageByAccess(docMapper, configItemDao, accessPeopleIdS,
        // historyNew.getRelateId(), prfName,
        // historyNew.getType());
    }

    /**
     * 任务现阶段是否在文控所处阶段之前
     *
     * @param state
     * @param jecnTaskStageList
     * @return
     * @author weidp
     * @date 2014-7-3 下午03:18:20
     */
    public static boolean isStateBeforeDocControl(Integer state, List<TaskStage> jecnTaskStageList) {
        int curSort = -1;
        int docControlSort = -1;
        for (TaskStage taskStage : jecnTaskStageList) {
            if (state == 1 && taskStage.getStageMark().intValue() == state) {
                return true;
            }
            if (taskStage.getStageMark().intValue() == state) {
                curSort = taskStage.getSort();
            } else if (taskStage.getStageMark().intValue() == 1) {
                docControlSort = taskStage.getSort();
            }
        }
        if (docControlSort != -1 && curSort != -1) {
            return docControlSort > curSort;
        }
        return false;
    }

    /**
     * 发布给PRF查阅权限人员发送消息
     *
     * @param peopleIds 人员主键ID集合
     * @param prfName   PRF文件名称
     * @param mesType   1:审批；2：发布；3：创建
     */
    public static List<Message> createTaskMessages(Set<Long> peopleIds, int taskType, String prfName, int mesType) {
        if (peopleIds == null || Common.isNullOrEmtryTrim(prfName)) {
            return new ArrayList<Message>();
        }
        List<Message> messages = new ArrayList<Message>();
        for (Long peopleId : peopleIds) {
            if (peopleId != null) {
                Message message = new Message();
                message.setPeopleId(0L);
                // 消息类型：任务
                message.setMessageType(1);
                message.setMessageContent(prfName);
                String strName = null;
                if (mesType == 2) {// 发布任务
                    strName = getStrTitle(mesType) + prfName + getMesContants(mesType);
                } else {// 创建任务或审批任务
                    strName = getStrTitle(mesType) + getMesContants(mesType) + prfName;
                }
                message.setMessageTopic(strName);
                message.setNoRead(Long.valueOf(0));
                message.setInceptPeopleId(peopleId);
                message.setCreateTime(new Date());
                messages.add(message);
            }
        }
        return messages;
    }

    /**
     * 任务审批过程发送消息
     *
     * @param type
     * @return
     */
    private static String getStrTitle(int type) {
        String strTitle = null;
        if (type == 1) {// 任务审批
            strTitle = "审批任务：";
        } else if (type == 2) {
            strTitle = "发布任务：";
        } else if (type == 3) {
            strTitle = "创建任务：";
        }
        return strTitle;
    }

    /**
     * 根据关联类型获取中文注释
     *
     * @param type    0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
     * @param mesType : 1:任务审批；2：任务发布；3：创建任务
     * @return
     */
    private static String getMesContants(int mesType) {
        // 任务已审批!
        String strPub = null;
        if (mesType == 1) {// 任务审批
            strPub = "请审批";
        } else if (mesType == 2) {
            strPub = "已发布！";
        } else if (mesType == 3) {
            strPub = "请审批";
        }
        return strPub;
    }

}
