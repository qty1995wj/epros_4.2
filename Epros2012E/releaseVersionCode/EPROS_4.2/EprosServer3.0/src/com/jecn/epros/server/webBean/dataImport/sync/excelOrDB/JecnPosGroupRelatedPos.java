package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;

/**
 * 岗位组相关岗位 同步
 * 
 * @author ZXH
 * 
 */
public class JecnPosGroupRelatedPos extends AbstractBaseBean {
	private String posNum = null;
	/** 岗位组编号 */
	private String posGroupNum = null;

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public String getPosGroupNum() {
		return posGroupNum;
	}

	public void setPosGroupNum(String posGroupNum) {
		this.posGroupNum = posGroupNum;
	}

	@Override
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 部门编号
		strBuf.append("编号:");
		strBuf.append(posNum);
		strBuf.append(SyncConstant.EMTRY_SPACE);

		// 部门名称
		strBuf.append("名称:");
		strBuf.append(posGroupNum);
		strBuf.append(SyncConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}
}
