package epros.designer.gui.workflow;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程设计向导中箭头
 * 
 * @author ZHOUXY
 *
 */
public class JecnGuideArrawPanel extends JPanel {
	private Color fillColor = Color.WHITE;
	private Color changeColor = new Color(34, 160, 60);
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int x5;
	private int x6;
	private int x7;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int y5;
	private int y6;
	private int y7;

	public JecnGuideArrawPanel() {
		this.setPreferredSize(new Dimension(50, 20));
	}

	public void paintComponent(Graphics g) {
		int userWidth = this.getWidth() - 1;
		int userHeight = this.getHeight() - 1;
		x1 = userWidth * 3 / 5;
		y1 = 0;
		x2 = userWidth * 3 / 5;
		y2 = userHeight * 3 / 5;
		x3 = userWidth * 4 / 5;
		y3 = userHeight * 3 / 5;
		x4 = userWidth / 2;
		y4 = userHeight - 1;
		x5 = userWidth * 1 / 5;
		y5 = userHeight * 3 / 5;
		x6 = userWidth * 2 / 5;
		y6 = userHeight * 3 / 5;
		x7 = userWidth * 2 / 5;
		y7 = 0;
		Graphics2D g2d = (Graphics2D) g;

		// 背景渐变色 颜色渐变填充
		paintBackColor(g2d, FillColorChangType.horizontal);

		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 }, new int[] {
				y1, y2, y3, y4, y5, y6, y7 }, 7);
		g2d.setStroke(new BasicStroke(1));
		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 }, new int[] {
				y1, y2, y3, y4, y5, y6, y7 }, 7);
	}

	/**
	 * 背景渐变色 颜色渐变填充
	 * 
	 * @param g2d
	 * @param jPanelColorChange
	 */
	protected void paintBackColor(Graphics2D g2d,
			FillColorChangType fillColorChangType) {
		// topTilt, // 1：向上倾斜 bottom, // 2：向下倾斜 vertical, // 3：垂直horizontal
		// 渐变色 第一种颜色
		Color color1 = null;
		// 渐变色 第二种颜色
		Color color2 = null;

		int x = 0;
		int y = 0;
		switch (fillColorChangType) {
		case topTilt:
			color1 = fillColor;
			color2 = changeColor;
			x = this.getWidth();
			y = this.getHeight();
			break;
		case bottom:
			color2 = fillColor;
			color1 = changeColor;
			x = this.getWidth();
			y = 0;
			break;
		case vertical:
			color1 = fillColor;
			color2 = changeColor;
			x = this.getWidth();
			y = this.getHeight();
			break;
		case horizontal:
			color1 = fillColor;
			color2 = changeColor;
			x = 0;
			y = this.getHeight();
			break;
		default:
			break;
		}
		// 如果双色时存在颜色为空,取面板默认色
		if (color1 == null) {
			color1 = JecnUIUtil.getDefaultBackgroundColor();
		}
		if (color2 == null) {
			color2 = JecnUIUtil.getDefaultBackgroundColor();
		}
		g2d.setPaint(new GradientPaint(0, 0, color1, x, y, color2));
	}

	/**
	 * 
	 * 渐变方向类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum FillColorChangType {
		topTilt, // 1：向上倾斜 默认
		bottom, // 2：向下倾斜
		vertical, // 3：垂直
		horizontal
		// 4：水平
	}
}
