package com.jecn.epros.server.bean.process;

/**
 * <li>Title: FlowCriterion.java</li>
 * <li>Project: epros</li>
 * <li>Package: com.jecn.criterion.bean</li>
 * <li>Description: 
 * <li>
 * <li>Copyright: Copyright (c) 2008</li>
 * <li>Company: JECN Technologies </li>
 * <li>Created on Jun 29, 2008, 8:05:46 PM</li>
 * 
 * @author fulin_tang
 * @version Epros V1.0
 * 制度和流程相关（设计端显示）
 */
public class FlowCriterionT implements java.io.Serializable {

	private Long flowCriterionId;//主键ID
	private Long flowId;//流程ID
	private Long criterionClassId;//制度ID

	public FlowCriterionT() {
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}


	public Long getCriterionClassId() {
		return criterionClassId;
	}

	public void setCriterionClassId(Long criterionClassId) {
		this.criterionClassId = criterionClassId;
	}

	public Long getFlowCriterionId() {
		return flowCriterionId;
	}

	public void setFlowCriterionId(Long flowCriterionId) {
		this.flowCriterionId = flowCriterionId;
	}
}