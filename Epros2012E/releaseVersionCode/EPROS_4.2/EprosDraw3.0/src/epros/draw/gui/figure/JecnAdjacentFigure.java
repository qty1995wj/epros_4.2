package epros.draw.gui.figure;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import epros.draw.gui.line.JecnTempLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

/**
 * 
 * 
 * 获取拖动图形时，指向附近图形的小线段，便于提示用户拖动图形与附近图形中心点对齐功能
 * 
 * 设置先关联图形生成临时线段
 * 
 * @author ZHANGXH
 * @date： 日期：May 8, 2012 时间：2:17:13 PM
 */
public class JecnAdjacentFigure {
	/** 拖动的当前图形 上 相邻图形 */
	private static JecnBaseFigurePanel topFigure = null;
	/** 拖动的当前图形 左 相邻图形 */
	private static JecnBaseFigurePanel leftFigure = null;
	/** 拖动的当前图形 下 相邻图形 */
	private static JecnBaseFigurePanel rightFigure = null;
	/** 拖动的当前图形 右 相邻图形 */
	private static JecnBaseFigurePanel bottomFigure = null;
	/** 记录临时线段集合 */
	private static JecnTempLine tempLiftLine = null;
	private static JecnTempLine tempRightLine = null;
	private static JecnTempLine tempTopLine = null;
	private static JecnTempLine tempBottomLine = null;

	/**
	 * 拖动图形设置图形相邻图形中心点坐标是否存在横或纵相同的情况，出现横坐标相同 用虚拟横线标识
	 * 
	 * @param curFigure
	 */
	public static void getRefFigure(JecnBaseFigurePanel curFigure, Rectangle rectangle) {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 获取面板所有流程元素
		List<JecnBaseFlowElementPanel> allFlowElements = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();

		if (allFlowElements == null || allFlowElements.size() == 0) {
			return;
		}

		// 图形中心点坐标
		// 中心点X坐标
		int centerX = DrawCommon.convertDoubleToInt(rectangle.getCenterX());
		// 中心点Y坐标
		int centerY = DrawCommon.convertDoubleToInt(rectangle.getCenterY());

		// 获取中心点X坐标相同的图形
		List<JecnBaseFigurePanel> figureListX = new ArrayList<JecnBaseFigurePanel>();
		// 获取中心点Y坐标相同的图形
		List<JecnBaseFigurePanel> figureListY = new ArrayList<JecnBaseFigurePanel>();

		for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : allFlowElements) {
			if (jecnBaseFlowElementPanel instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel figure = (JecnBaseFigurePanel) jecnBaseFlowElementPanel;
				if (figure == curFigure) {
					// 如果为当前图形跳出当前循环
					continue;
				} else {
					if (Math.abs(figure.getCenterPointX() - centerX) < 10 && !figureListY.contains(figure)) {// 获取中心点X坐标相等的图形元素
						figureListY.add(figure);
					}
					if (Math.abs(figure.getCenterPointY() - centerY) < 10 && !figureListX.contains(figure)) {// 获取中心点Y坐标相等的图形元素
						figureListX.add(figure);
					}
				}
			}
		}

		// 根据中心点坐标相同的图形获取距离拖动图形四个编辑点最近的图形
		setAdjacentFigure(rectangle, figureListX, figureListY);

		// 根据坐标点生成虚拟连线
		drawTempLine(rectangle);
	}

	/**
	 * 相邻图形中心点在同一水平或垂直线上时 画虚拟线段
	 * 
	 * @param rectangle
	 *            拖动的虚拟图形边框
	 */
	public static void drawTempLine(Rectangle rectangle) {
		Point tempStartPoint = new Point();
		Point tempEndPoint = new Point();
		if (leftFigure != null && rightFigure != null) {// 左右两侧图形比较中心点Y坐标到标准图形的中心点Y坐标
			int left = Math.abs(DrawCommon.convertDoubleToInt(rectangle.getCenterY()
					- leftFigure.getEditLeftPortPanel().getZoomCenterPoint().getY()));
			int right = Math.abs(DrawCommon.convertDoubleToInt(rectangle.getCenterY()
					- rightFigure.getEditLeftPortPanel().getZoomCenterPoint().getY()));
			if (left > right) {// 左侧距离大于右侧距离
				drawRightTempLine(rectangle, tempStartPoint, tempEndPoint);
			} else if (left < right) {
				drawLiftTempLine(rectangle, tempStartPoint, tempEndPoint);
			} else if (left == right) {// 左右都有线段
				drawLiftTempLine(rectangle, tempStartPoint, tempEndPoint);
				drawRightTempLine(rectangle, tempStartPoint, tempEndPoint);
			}
		} else {
			if (leftFigure != null) {// 左侧图形 水平位置
				drawLiftTempLine(rectangle, tempStartPoint, tempEndPoint);
			}
			if (rightFigure != null) {// 右侧图形 水平位置
				drawRightTempLine(rectangle, tempStartPoint, tempEndPoint);
			}
		}

		if (topFigure != null && bottomFigure != null) {// 左右两侧
			double top = Math.abs(rectangle.getCenterX() - topFigure.getEditTopPortPanel().getZoomCenterPoint().x);
			double bottom = Math.abs(rectangle.getCenterX()
					- bottomFigure.getEditBottomPortPanel().getZoomCenterPoint().x);

			if (top > bottom) {// 左侧距离大于右侧距离
				drawBottomTempLine(rectangle, tempStartPoint, tempEndPoint);
			} else if (top < bottom) {
				drawTopTempLine(rectangle, tempStartPoint, tempEndPoint);
			} else if (top == bottom) {// 上下都有线段
				drawBottomTempLine(rectangle, tempStartPoint, tempEndPoint);
				drawTopTempLine(rectangle, tempStartPoint, tempEndPoint);
			}
		} else {
			if (topFigure != null) {// 上方图形 垂直位置
				drawTopTempLine(rectangle, tempStartPoint, tempEndPoint);
			}
			if (bottomFigure != null) {// 下方图形 垂直位置
				drawBottomTempLine(rectangle, tempStartPoint, tempEndPoint);
			}
		}
	}

	/**
	 * 画左侧图形和拖动边框中心点的线段
	 * 
	 * @param rectangle
	 * @param tempStartPoint
	 * @param tempEndPoint
	 */
	private static void drawLiftTempLine(Rectangle rectangle, Point tempStartPoint, Point tempEndPoint) {
		tempLiftLine = new JecnTempLine();
		Point editLeftPoint = leftFigure.getEditLeftPortPanel().getZoomCenterPoint();
		tempEndPoint.x = editLeftPoint.x - 10;
		tempEndPoint.y = editLeftPoint.y;

		// 开始点取虚拟图形位置点+ 宽度+ 10
		tempStartPoint.x = rectangle.x + rectangle.width + 10;
		tempStartPoint.y = editLeftPoint.y;

		// 设置新的位置点
		rectangle.setLocation(rectangle.getLocation().x, editLeftPoint.y
				- DrawCommon.convertDoubleToInt(rectangle.getHeight() / 2.0));

		// 根据坐标点生成虚拟线段
		tempLiftLine.drawTempLine(tempStartPoint.x, tempStartPoint.y, tempEndPoint.x, tempEndPoint.y);
		addTempLineToWork(tempLiftLine);
	}

	/**
	 * 
	 * 添加虚拟连接线到面板
	 * 
	 * @param tempLine
	 */
	public static void addTempLineToWork(JecnTempLine tempLine) {
		JecnDrawDesktopPane wDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		wDesktopPane.add(tempLine);
		wDesktopPane.setLayer(tempLine, wDesktopPane.getMaxZOrderIndex() + 1);
	}

	/**
	 * 画右侧图形和拖动边框中心点的线段
	 * 
	 * @param rectangle
	 * @param tempStartPoint
	 * @param tempEndPoint
	 */
	private static void drawRightTempLine(Rectangle rectangle, Point tempStartPoint, Point tempEndPoint) {
		tempRightLine = new JecnTempLine();
		Point editRightPoint = rightFigure.getEditRightPortPanel().getZoomCenterPoint();
		tempEndPoint.x = editRightPoint.x + 10;
		tempEndPoint.y = editRightPoint.y;

		// 开始点取虚拟图形位置点 - 10
		tempStartPoint.x = rectangle.x - 10;
		tempStartPoint.y = editRightPoint.y;
		// 设置新的位置点
		rectangle.setLocation(rectangle.getLocation().x, editRightPoint.y
				- DrawCommon.convertDoubleToInt(rectangle.getHeight() / 2.0));
		// 根据坐标点生成虚拟线段
		tempRightLine.drawTempLine(tempStartPoint.x, tempStartPoint.y, tempEndPoint.x, tempEndPoint.y);
		addTempLineToWork(tempRightLine);
	}

	/**
	 * 画上方图形和拖动边框中心点的线段
	 * 
	 * @param rectangle
	 * @param tempStartPoint
	 * @param tempEndPoint
	 */
	private static void drawTopTempLine(Rectangle rectangle, Point tempStartPoint, Point tempEndPoint) {
		tempTopLine = new JecnTempLine();
		Point editTopPoint = topFigure.getEditTopPortPanel().getZoomCenterPoint();
		tempEndPoint.x = editTopPoint.x;
		tempEndPoint.y = editTopPoint.y - 10;

		// 开始点取虚拟图形位置点 + 高度+ 10
		tempStartPoint.x = editTopPoint.x;
		tempStartPoint.y = rectangle.y + rectangle.height + 10;

		// 设置新的位置点
		rectangle.setLocation(editTopPoint.x - DrawCommon.convertDoubleToInt(rectangle.getWidth() / 2.0), rectangle
				.getLocation().y);
		// 根据坐标点生成虚拟线段
		tempTopLine.drawTempLine(tempStartPoint.x, tempStartPoint.y, tempEndPoint.x, tempEndPoint.y);
		addTempLineToWork(tempTopLine);
	}

	/**
	 * 画上方图形和拖动边框中心点的线段
	 * 
	 * @param rectangle
	 * @param tempStartPoint
	 * @param tempEndPoint
	 */
	private static void drawBottomTempLine(Rectangle rectangle, Point tempStartPoint, Point tempEndPoint) {
		tempBottomLine = new JecnTempLine();
		Point editBottomPoint = bottomFigure.getEditBottomPortPanel().getZoomCenterPoint();
		tempEndPoint.x = editBottomPoint.x;
		tempEndPoint.y = editBottomPoint.y + 10;

		// 开始点取虚拟图形位置点 - 10
		tempStartPoint.x = editBottomPoint.x;
		tempStartPoint.y = rectangle.y - 10;

		// 设置新的位置点
		rectangle.setLocation(editBottomPoint.x - DrawCommon.convertDoubleToInt(rectangle.getWidth() / 2.0), rectangle
				.getLocation().y);
		// 根据坐标点生成虚拟线段
		tempBottomLine.drawTempLine(tempStartPoint.x, tempStartPoint.y, tempEndPoint.x, tempEndPoint.y);
		addTempLineToWork(tempBottomLine);
	}

	/**
	 * 删除面板虚拟线段，清空线段对应的图形
	 * 
	 */
	public static void removeTempLine() {
		if (tempLiftLine != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(tempLiftLine);
			leftFigure = null;
		}
		if (tempRightLine != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(tempRightLine);
			rightFigure = null;
		}
		if (tempTopLine != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(tempTopLine);
			topFigure = null;
		}
		if (tempBottomLine != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(tempBottomLine);
			bottomFigure = null;
		}
	}

	/**
	 * 根据中心点坐标相同的图形获取距离拖动图形四个编辑点最近的图形
	 * 
	 * @param rectangle
	 *            拖动图形边框
	 * @param figureListX
	 *            中心点X坐标相同的图形集合
	 * @param figureListY
	 *            中心点Y坐标相同的图形集合
	 */
	public static void setAdjacentFigure(Rectangle rectangle, List<JecnBaseFigurePanel> figureListX,
			List<JecnBaseFigurePanel> figureListY) {
		// 拖动图形4个编辑点4个方向坐标
		// 坐标编辑点横向坐标
		int leftEdit = rectangle.x;
		// 坐标编辑点横向坐标
		int rightEdit = rectangle.x + rectangle.width;
		// 坐标编辑点横向坐标
		int topEdit = rectangle.y;
		// 坐标编辑点横向坐标
		int bottomEdit = rectangle.y + rectangle.height;
		if (figureListX != null && figureListX.size() > 0) {
			// 横向到左侧编辑点的距离
			int instanceLeft = 0;
			// 横向到右侧编辑点的距离
			int instanceRight = 0;
			for (JecnBaseFigurePanel jecnBaseFigurePanel : figureListX) {// 中心点Y坐标相同，获取横向编辑点到拖动图形距离最小的图形元素
				if (jecnBaseFigurePanel.getEditRightPortPanel().getZoomCenterPoint().x <= leftEdit) {// 和拖动图形左侧编辑点距离最短的图形
					// 应比较图形右侧编辑点到拖动图形左侧编辑点的横向距离
					int tempInstanceX = leftEdit - jecnBaseFigurePanel.getEditRightPortPanel().getZoomCenterPoint().x;
					if (instanceLeft == 0 || instanceLeft > tempInstanceX) {
						// 右侧编辑点到拖动图形左侧编辑点的距离
						instanceLeft = tempInstanceX;
						leftFigure = jecnBaseFigurePanel;
					}
				}
				if (jecnBaseFigurePanel.getEditLeftPortPanel().getZoomCenterPoint().x >= rightEdit) {
					// 应比较图形左侧编辑点到拖动图形右侧编辑点的横向距离
					int tempInstanceX = jecnBaseFigurePanel.getEditLeftPortPanel().getZoomCenterPoint().x - rightEdit;
					if (instanceRight == 0 || instanceRight > tempInstanceX) {// 右侧编辑点到拖动图形左侧编辑点的距离
						instanceRight = tempInstanceX;
						rightFigure = jecnBaseFigurePanel;
					}
				}
			}
		}
		if (figureListY != null && figureListY.size() > 0) {
			// 横向到左侧编辑点的距离
			int instanceTop = 0;
			// 横向到右侧编辑点的距离
			int instanceBottom = 0;
			for (JecnBaseFigurePanel jecnBaseFigurePanel : figureListY) {// 中心点Y坐标相同，获取横向编辑点到拖动图形距离最小的图形元素
				if (jecnBaseFigurePanel.getEditBottomPortPanel().getZoomCenterPoint().y <= topEdit) {// 和拖动图形上侧编辑点距离最短的图形
					// 应比较图形底端编辑点到拖动图形上侧编辑点的横向距离
					int tempInstanceY = topEdit - jecnBaseFigurePanel.getEditBottomPortPanel().getZoomCenterPoint().y;
					if (instanceTop == 0 || instanceTop > tempInstanceY) {
						// 右侧编辑点到拖动图形左侧编辑点的距离
						instanceTop = tempInstanceY;
						topFigure = jecnBaseFigurePanel;
					}
				}
				if (jecnBaseFigurePanel.getEditTopPortPanel().getZoomCenterPoint().y >= bottomEdit) {
					// 应比较图形左侧编辑点到拖动图形右侧编辑点的横向距离
					int tempInstanceY = jecnBaseFigurePanel.getEditLeftPortPanel().getZoomCenterPoint().y - bottomEdit;
					if (instanceBottom == 0 || instanceBottom > tempInstanceY) {// 右侧编辑点到拖动图形左侧编辑点的距离
						instanceBottom = tempInstanceY;
						bottomFigure = jecnBaseFigurePanel;
					}
				}
			}
		}
	}

}
