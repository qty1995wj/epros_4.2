/**
 * 我关注的流程
 */
function atentionProcessGrid() {
	atentionProcessGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize,
				secretLevel : 0
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "attenTionProcess.action?isFlow=1"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "flowId",
			mapping : "flowId"
		}, {
			name : "flowName",
			mapping : "flowName"
		}, {
			name : "flowIdInput",
			mapping : "flowIdInput"
		}, {
			name : "resPeopleName",
			mapping : "resPeopleName"
		}, {
			name : "orgName",
			mapping : "orgName"
		}, {
			name : "secretLevel",
			mapping : "isPublic"
		}, {
			name : "pubDate",
			mapping : "pubDate"
		} ])

	});
	//给请求添加预定义的参数
	atentionProcessGridStore.on("beforeload", function(thiz, options) {
		//debugger
			thiz.baseParams["secretLevel"] = $("#attentionProcess_intensive")
					.val();
		});

	atentionProcessGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	atentionProcessGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : atentionProcessGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});
	atentionProcessGridGridPanel = new Ext.grid.GridPanel( {
		id : 'atentionProcessGridGridPanel_id',
		renderTo : 'atentionProcessGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 160,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		bodyStyle : 'border-left: 0px;',
		store : atentionProcessGridStore,
		colModel : atentionProcessGridColumnModel,
		bbar : atentionProcessGridBbar
	})

	//	myhome_index_id = "myhome_atentionProcess_id";
}

/**
 * 我的流程架构
 */
function atentionMapProcessGrid() {
	atentionMapProcessGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize,
				secretLevel : 0
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "attenTionProcess.action?isFlow=0"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "flowId",
			mapping : "flowId"
		}, {
			name : "flowName",
			mapping : "flowName"
		}, {
			name : "flowIdInput",
			mapping : "flowIdInput"
		}, {
			name : "secretLevel",
			mapping : "isPublic"
		}, {
			name : "pubDate",
			mapping : "pubDate"
		} ])

	});

	//给请求添加预定义的参数
	atentionMapProcessGridStore.on("beforeload", function(thiz, options) {

		thiz.baseParams["secretLevel"] = $("#attentionMapProcess_intensive")
				.val();
	});

	var atentionMapProcessGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				//跳转流程地图
				renderer : processMapNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	var atentionMapProcessGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : atentionMapProcessGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	atentionProcessMapGridGridPanel = new Ext.grid.GridPanel( {
		id : 'atentionMapProcessGridGridPanel_id',
		renderTo : 'atentionMapProcessGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 160,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		bodyStyle : 'border-left: 0px;',
		store : atentionMapProcessGridStore,
		colModel : atentionMapProcessGridColumnModel,
		bbar : atentionMapProcessGridBbar
	})
}
/**
 * 我关注的制度
 */
function atentionRuleGrid() {

	atentionRuleGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize,
				secretLevel : 0
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "attenTionRule.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "ruleId",
			mapping : "ruleId"
		}, {
			name : "ruleName",
			mapping : "ruleName"
		}, {
			name : "ruleNum",
			mapping : "ruleNum"
		}, {
			name : "ruleTypeName",
			mapping : "ruleTypeName"
		}, {
			name : "dutyOrg",
			mapping : "dutyOrg"
		}, {
			name : "secretLevel",
			mapping : "secretLevel"
		}, {
			name : "pubDate",
			mapping : "pubDate"
		}, {
			name : "isDir",
			mapping : "isDir"
		}, {
			name : "fileId",
			mapping : "fileId"
		} ])

	});

	//给请求添加预定义的参数
	atentionRuleGridStore.on("beforeload", function(thiz, options) {

		thiz.baseParams["secretLevel"] = $("#attentionRule_intensive").val();
	});

	atentionRuleGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_ruleName,
				dataIndex : "ruleName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : ruleNameRenderer
			}, {
				header : i_ruleNum,
				dataIndex : "ruleNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_ruleType,
				dataIndex : "ruleTypeName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "dutyOrg",
				align : "center",
				menuDisabled : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	atentionRuleGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : atentionRuleGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	atentionRuleGridGridPanel = new Ext.grid.GridPanel( {
		id : "atentionRuleGridGridPanel_id",
		renderTo : 'atentionRuleGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 135,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		bodyStyle : 'border-left: 0px;',
		store : atentionRuleGridStore,
		colModel : atentionRuleGridColumnModel,
		bbar : atentionRuleGridBbar
	});
	myhome_index_id = "myhome_myRule_id";
}
/**
 * 我关注的标准
 */
function atentionStandardGrid() {

	atentionStandardGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "attenTionStandard.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "id",
			mapping : "id"
		}, {
			name : "name",
			mapping : "name"
		}, {
			name : "fileId",
			mapping : "fileId"
		}, {
			name : "fileName",
			mapping : "fileName"
		}, {
			name : "type",
			mapping : "type"
		}, {
			name : "pname",
			mapping : "pname"
		} ])

	});

	atentionStandardGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_standardName,
				dataIndex : "name",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : standardNameRenderer
			}, {
				header : i_pdirName,
				dataIndex : "pname",
				align : "center",
				menuDisabled : true,
				sortable : true
			}

	]);
	atentionStandardGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : atentionStandardGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	atentionStandardGridGridPanel = new Ext.grid.GridPanel( {
		id : "atentionStandardGridGridPanel_id",
		renderTo : 'atentionStandardGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 165,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		border : false,
		store : atentionStandardGridStore,
		colModel : atentionStandardGridColumnModel,
		bbar : atentionStandardGridBbar
	})

}
/**
 * 我关注的其他文件
 */
function atentionFileGrid() {

	atentionFileGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "attenTionFile.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "fileId",
			mapping : "fileId"
		}, {
			name : "fileName",
			mapping : "fileName"
		}, {
			name : "fileNum",
			mapping : "fileNum"
		}, {
			name : "orgName",
			mapping : "orgName"
		}, {
			name : "secretLevel",
			mapping : "secretLevel"
		} ])

	});

	atentionFileGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_fileName,
				dataIndex : "fileName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : fileNameRenderer
			}, {
				header : i_fileNum,
				dataIndex : "fileNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}

	]);
	atentionFileGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : atentionFileGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	atentionFileGridGridPanel = new Ext.grid.GridPanel( {
		id : "atentionFileGridGridPanel_id",
		renderTo : 'atentionFileGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 135,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		bodyStyle : 'border-left: 0px;',
		store : atentionFileGridStore,
		colModel : atentionFileGridColumnModel,
		bbar : atentionFileGridBbar
	})
	myhome_index_id = "myhome_myRepository_id";
}
/**
 * 我的工作模版及指导书grid
 */
function myWorkModelGrid() {

	myWorkModelGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "myWorkModel.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "fileId",
			mapping : "fileId"
		}, {
			name : "fileName",
			mapping : "fileName"
		}, {
			name : "fileNum",
			mapping : "fileNum"
		}, {
			name : "orgName",
			mapping : "orgName"
		}, {
			name : "secretLevel",
			mapping : "secretLevel"
		} ])

	});

	myWorkModelGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_fileName,
				dataIndex : "fileName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : fileNameRenderer
			}, {
				header : i_fileNum,
				dataIndex : "fileNum",
				align : "center",
				menuDisabled : true
			}

	]);
	myWorkModelGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : myWorkModelGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	myWorkModelGridGridPanel = new Ext.grid.GridPanel( {
		id : "myWorkModelGridGridPanel_id",
		renderTo : 'myWorkModelDirectBookGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 165,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		border : false,
		store : myWorkModelGridStore,
		colModel : myWorkModelGridColumnModel,
		bbar : myWorkModelGridBbar
	})

}
/**
 * 我的工作模版及指导书
 */
function myWorkModelSearch() {
	myWorkModelGridStore.setBaseParam("flowId", document
			.getElementById("myJoinProcessSelect_id").value);
	myWorkModelGridStore.setBaseParam("activeId", document
			.getElementById("activitySelect_id").value);
	myWorkModelGridStore.setBaseParam("fileSearchName", document
			.getElementById("myWorkFileName_id").value);
	myWorkModelGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});
}

/**
 * 重置我的工作模版及指导书
 */
function myWorkModelReset() {
	document.getElementById("myJoinProcessSelect_id").options[0].selected = true;
	document.getElementById("activitySelect_id").options[0].selected = true;
	document.getElementById("myWorkFileName_id").value = "";

	var joinProcess = document.getElementById('activitySelect_id');
	for ( var i = joinProcess.options.length - 1; i > 0; i--) {
		joinProcess.remove(i);
	}
}
/**
 * 我关注的流程搜索
 */
function attentionProcessSearch() {
	atentionProcessGridStore.setBaseParam("processName", document
			.getElementById("attentionProcessName_id").value);
	atentionProcessGridStore.setBaseParam("processNum", document
			.getElementById("attentionProcessNumber_id").value);
	atentionProcessGridStore.setBaseParam("orgId", document
			.getElementById("attentionOrg_id").value);
	atentionProcessGridStore.setBaseParam("secretLevel", document
			.getElementById("attentionProcess_intensive").value);
	atentionProcessGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});

}

/**
 * 我关注的流程地图搜索
 */
function attentionMapProcessSearch() {
	atentionMapProcessGridStore.setBaseParam("processName", document
			.getElementById("attentionMapProcessName_id").value);
	atentionMapProcessGridStore.setBaseParam("processNum", document
			.getElementById("attentionMapProcessNumber_id").value);
	atentionMapProcessGridStore.setBaseParam("secretLevel", document
			.getElementById("attentionMapProcess_intensive").value);
	atentionMapProcessGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});

}
/**
 * 我关注的制度搜索
 */
function attentionRuleSearch() {
	atentionRuleGridStore.setBaseParam("ruleName", document
			.getElementById("ruleName").value);
	atentionRuleGridStore.setBaseParam("ruleNum", document
			.getElementById("ruleNumber").value);
	var orgId = document.getElementById("orgDutyId_rule_id").value;
	if (orgId == null || orgId == '') {
		orgId = -1;
	}
	atentionRuleGridStore.setBaseParam("posLookId", orgId);
	atentionRuleGridStore.setBaseParam("secretLevel", document
			.getElementById("attentionRule_intensive").value);
	atentionRuleGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});
}
/**
 * 我关注的标准搜索
 */
function attentionStandardSearch() {
	atentionStandardGridStore.setBaseParam("standardName", document
			.getElementById("standardName").value);
	atentionStandardGridStore.setBaseParam("posLookId", document
			.getElementById("attentionStandard_positionName").value);
	atentionStandardGridStore.setBaseParam("secretLevel", document
			.getElementById("attentionStandard_intensive").value);
	atentionStandardGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});
}
/**
 * 我关注的文件搜索
 */
function attentionFileSearch() {
	atentionFileGridStore.setBaseParam("fileName", document
			.getElementById("fileName").value);
	atentionFileGridStore.setBaseParam("fileNum", document
			.getElementById("fileNumber").value);
	var orgId = document.getElementById("orgDutyId_rule_id").value;
	if (orgId == null || orgId == '') {
		orgId = -1;
	}
	atentionFileGridStore.setBaseParam("posLookId", orgId);
	atentionFileGridStore.setBaseParam("secretLevel", document
			.getElementById("attentionFile_intensive").value);
	atentionFileGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});
}

/**
 * 重置我关注的流程
 */
function attentionProcessReset() {
	document.getElementById("attentionProcessName_id").value = "";
	document.getElementById("attentionProcessNumber_id").value = "";
	document.getElementById("attentionOrgName_id").value = "";
	document.getElementById("attentionOrg_id").value = "-1";
	document.getElementById("attentionProcess_intensive").options[0].selected = true;
}
/**
 * 重置我关注的流程地图
 */
function attentionMapProcessReset() {
	document.getElementById("attentionMapProcessName_id").value = "";
	document.getElementById("attentionMapProcessNumber_id").value = "";
	document.getElementById("attentionMapProcess_intensive").options[0].selected = true;
}

/**
 * 重置我关注的文件
 */
function attentionFileReset() {
	document.getElementById("fileName").value = "";
	document.getElementById("fileNumber").value = "";
	document.getElementById("orgDutyId_rule_id").value = "-1";
	document.getElementById("orgDutyName_rule_id").value = "";
	document.getElementById("attentionFile_intensive").options[0].selected = true;
}

/**
 * 重置我关注的制度
 */
function attentionRuleReset() {
	document.getElementById("ruleName").value = "";
	document.getElementById("ruleNumber").value = "";
	document.getElementById("orgDutyId_rule_id").value = "-1";
	document.getElementById("orgDutyName_rule_id").value = "";
	document.getElementById("attentionRule_intensive").options[0].selected = true;
}

/**
 * 重置我关注的标准
 */
function attentionStandarReset() {
	document.getElementById("standardName").value = "";
	document.getElementById("attentionStandard_positionName").options[0].selected = true;
	document.getElementById("attentionStandard_intensive").options[0].selected = true;
}

/**
 * 重置我参与的流程
 */
function joinProcessReset() {
	document.getElementById("processName_id").value = "";
	document.getElementById("processNumber_id").value = "";
	document.getElementById("positionName_id").options[0].selected = true;
}

function posDownload() {
	myPosId = document.getElementById("posSelect_id").value;
	if (myPosId == "") {
		myPosId = -1;
	}
	window.open("downLoadPosition.action?posId=" + myPosId);
}

/**
 * 岗位知识库
 */
function positionRepository() {

	var positionRepositoryTabPanel = new Ext.TabPanel(
			{
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				id : 'positionRepositoryTabPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 30,
				renderTo : 'positionRepository_id',
				defaults : {
					border : false
				},
				items : [
						{
							title : i_myJoinProcess,// 我参与的流程
							id : "myJoinProcess_id",
							autoLoad : {
								url : basePath + "jsp/myhome/joinProcess.jsp",
								callback : function() {
									getMyJoinProcess();
								}
							}
						},
						{
							title : i_myAttentionProcess,// 我关注的流程
							id : "myAttentionProcess_id",
							autoLoad : {
								url : basePath
										+ "jsp/myhome/attentionProcess.jsp",
								callback : function() {
									atentionProcessGrid();
								}
							}
						},
						{
							title : i_orgLeadProcess,// 部门主导流程
							id : "orgLeadProcess_id",
							border : false,
							autoLoad : {
								url : basePath
										+ "jsp/myhome/departmentLeadingProcess.jsp",
								callback : function() {
									deptProcessGrid();
								}
							}
						}, {
							title : i_myProcessMap,// 我的流程地图
							border : false,
							id : "myMapProcess_id",
							autoLoad : {
								url : basePath + "jsp/myhome/myMapProcess.jsp",
								callback : function() {
									atentionMapProcessGrid();
								}
							}
						} ],
				listeners : {
					'bodyresize' : function(p, width, height) {
						if (p.getActiveTab() == null) {
							return;
						}
						if (p.getActiveTab().getId() == 'myJoinProcess_id') {
							Ext.getCmp('myJoinprocessGrid_id').setSize(
									width - 2, height - 110);
						} else if (p.getActiveTab().getId() == 'myAttentionProcess_id') {//我关注的流程
							Ext.getCmp('atentionProcessGridGridPanel_id')
									.setSize(width - 2, height - 110);
						} else if (p.getActiveTab().getId() == 'orgLeadProcess_id') {
							Ext.getCmp('deptProcessGridPanel_id').setSize(
									width - 2, height - 110);
						} else if (p.getActiveTab().getId() == 'myAttentionStandard_id') {
							Ext.getCmp('atentionStandardGridGridPanel_id')
									.setSize(width - 2, height - 110);
						} else if (p.getActiveTab().getId() == 'myAttentionOtherFile_id') {
							Ext.getCmp('atentionFileGridGridPanel_id').setSize(
									width - 2, height - 110);
						} else if (p.getActiveTab().getId() == 'myPositionDetail_id') {
							setMyhomeMaxjobDesWH('myJobDescription_id');
						} else if (p.getActiveTab().getId() == 'myMapProcess_id') {//我的流程地图 
							Ext.getCmp('atentionMapProcessGridGridPanel_id')
									.setSize(width - 2, height - 110);
						}
					},
					'tabchange' : function(tabPanel, panel) {
						if (panel.getId() == 'myJoinProcess_id') {
							if (typeof (myJoinprocessGridPanel) != "undefined") {
								myJoinprocessGridPanel.setSize(tabPanel
										.getWidth() - 2,
										tabPanel.getHeight() - 133);
							}
						} else if (panel.getId() == 'myAttentionProcess_id') {
							if (typeof (atentionProcessGridGridPanel) != "undefined") {
								atentionProcessGridGridPanel.setSize(tabPanel
										.getWidth() - 2,
										tabPanel.getHeight() - 133);
							}

						} else if (panel.getId() == 'orgLeadProcess_id') {
							if (typeof (deptProcessGridPanel) != "undefined") {
								deptProcessGridPanel.setSize(tabPanel
										.getWidth() - 2,
										tabPanel.getHeight() - 133);
							}
						} else if (panel.getId() == 'myAttentionStandard_id') {
							if (typeof (atentionStandardGridGridPanel) != "undefined") {
								atentionStandardGridGridPanel.setSize(tabPanel
										.getWidth() - 2,
										tabPanel.getHeight() - 133);
							}
						} else if (panel.getId() == 'myAttentionOtherFile_id') {
							if (typeof (atentionFileGridGridPanel) != "undefined") {
								atentionFileGridGridPanel.setSize(tabPanel
										.getWidth() - 2,
										tabPanel.getHeight() - 133);
							}
						} else if (panel.getId() == 'myMapProcess_id') {//我的流程地图 
							if (typeof (atentionProcessMapGridGridPanel) != "undefined") {
								atentionProcessMapGridGridPanel(tabPanel
										.getWidth() - 2,
										tabPanel.getHeight() - 133);
							}
						}

					}
				}

			});
	if (!versionType) {
		positionRepositoryTabPanel.remove('myAttentionRuleTab_id');
		positionRepositoryTabPanel.remove('myAttentionStandard_id');
	}
	if (isHiddenBDF) {// 巴德富，不显示我的流程架构
		positionRepositoryTabPanel.remove("myMapProcess_id");
	}
	myhome_index_id = "myhome_positionRepository_id";
	// Ext.getCmp("myhomeCenter_id").add(positionRepositoryTabPanel);
}

/**
 * 我参与的流程下拉列表
 */
function myJoinProcessSelect() {
	$.ajax( {
		type : 'post',
		url : 'myJoinProcessList.action',
		dataType : 'json',
		success : function(msg) {
			if (msg != null && msg.length > 0) {
				var joinProcess = document
						.getElementById('myJoinProcessSelect_id');
				for ( var i = joinProcess.options.length - 1; i > 0; i--) {
					joinProcess.remove(i);
				}
				for ( var i = 0; i < msg.length; i++) {
					joinProcess.add(new Option(msg[i].name, msg[i].id));
				}
			}
		}

	})
}
/**
 * 根据我参与的流程查询活动下拉列表
 */
function myJoinProcessSelectOnchange() {
	$.ajax( {
		type : 'post',
		url : 'activeListByProcessId.action',
		data : {
			flowId : $("#myJoinProcessSelect_id").val()
		},
		dataType : 'json',
		success : function(msg) {
			if (msg != null && msg.length > 0) {
				var joinProcess = document.getElementById('activitySelect_id');
				for ( var i = joinProcess.options.length - 1; i > 0; i--) {
					joinProcess.remove(i);
				}
				for ( var i = 0; i < msg.length; i++) {
					joinProcess.add(new Option(msg[i].name, msg[i].id));
				}
			}
		}

	})

}