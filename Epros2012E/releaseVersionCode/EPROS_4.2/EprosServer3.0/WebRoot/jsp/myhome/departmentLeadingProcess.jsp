<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="departmentSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td height="25px" width="10%" align="right">
								<!-- 流程名称： -->
								<s:text name="processNameC"></s:text>
							</td>
							<td align="left" width="35%">
								<input type="text" style="width: 80%;"
									id="dept_lead_processName_id" maxLength="32" />
							</td>


							<td width="10%" align="right">
								<!-- 流程编号： -->
								<s:text name="processNumberC"></s:text>
							</td>
							<td align="left" width="35%">
								<input type="text" style="width: 80%;"
									id="dept_lead_processNumber_id" maxLength="32" />
							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a
									onClick="toggleDiv(departmentSearchText,'dproOackUp','dproSearchVal')"
									style="cursor: pointer;" class="packUp"> <img
										id="dproOackUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="dproSearchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td height="25px;" align="right">
								<!-- 责任部门: -->
								<s:text name="responsibilityDepartmentC" />
							</td>
							<td align="left">
								<select style="width: 80%;" id="dept_lead_Select_id">
									<option value="-1">
										<s:text name="all" />
									</option>
									<s:iterator value="#session.webLoginBean.listOrgBean">
										<option value='<s:property value="orgId"/>'>
											<s:property value="orgName" />
										</option>
									</s:iterator>
								</select>
							</td>

							<td></td>
							<td></td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="deptProcessGridSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="departmentLeadingProcessReset()" />
				</div>
			</div>

			<div id="deptProcessGrid_id">

			</div>
		</div>
	</body>
</html>