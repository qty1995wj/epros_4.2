package com.jecn.epros.server.webBean.integration;

import java.util.HashMap;
import java.util.Map;

/**
 * 风险清单 控制点bean
 * 
 * @author fuzhh 2013-12-13
 * 
 */
public class ControlTargetListBean {
	/*** 控制主键ID */
	private Long controlId;
	
	/** 控制目标 */
	private String controlDescription;
	/** 控制点对应活动 */
	private Map<Long, ControlActiveBean> controlActiveMap = new HashMap<Long, ControlActiveBean>();

	public Long getControlId() {
		return controlId;
	}

	public void setControlId(Long controlId) {
		this.controlId = controlId;
	}

	public String getControlDescription() {
		return controlDescription;
	}

	public void setControlDescription(String controlDescription) {
		this.controlDescription = controlDescription;
	}

	public Map<Long, ControlActiveBean> getControlActiveMap() {
		return controlActiveMap;
	}

	public void setControlActiveMap(
			Map<Long, ControlActiveBean> controlActiveMap) {
		this.controlActiveMap = controlActiveMap;
	}

}
