package epros.designer.gui.rule;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class HighEfficiencyRuleMoveTree extends JecnHighEfficiencyTree {
	private Logger log = Logger.getLogger(HighEfficiencyRuleMoveTree.class);

	public HighEfficiencyRuleMoveTree(List<Long> listIds) {
		super(listIds);
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RuleMoveTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getRuleAction().getChildRuleDirs(
					Long.valueOf(0), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighEfficiencyRuleMoveTree getTreeModel is error", e);
			//增加提示
		}
		
		// 根节点 "制度"
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.ruleRoot,JecnProperties.getValue("rule"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
