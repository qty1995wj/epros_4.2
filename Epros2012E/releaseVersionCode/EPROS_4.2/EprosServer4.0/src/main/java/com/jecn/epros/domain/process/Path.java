package com.jecn.epros.domain.process;

import java.io.Serializable;

/**
 * 清单中的几级名称几级编号
 *
 * @author user
 */
public class Path implements Serializable {

    private Integer level;
    private String name = "";
    private String number = "";

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
