package epros.designer.gui.system.config.ui.property.table;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程文件滚动面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnThreeTableScrollPane extends JecnTableScrollPane {

	public JecnThreeTableScrollPane(JecnAbtractBaseConfigDialog dialog) {
		super(null, dialog);
	}

	/**
	 * 
	 * 初始化表
	 * 
	 * @param headerName
	 *            String
	 */
	protected void initTable(String headerName) {
		// 表头
		String[] header = { JecnProperties.getValue("task_TableHeaderName"), JecnProperties.getValue("enName"),
				JecnProperties.getValue("basicTableHeaderName"), "id" };
		cloumnNames = header;
		// 创建table模型
		tableModel = new JecnDefaultTableModel(cloumnNames);
		// 创建表
		table = new JTable(tableModel);
		// 设置用户是否可以拖动列头，以重新排序各列,设置为false
		table.getTableHeader().setReorderingAllowed(false);
		// 设置表头背景颜色
		table.getTableHeader().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置没有ID列的宽度
		int index = getBeanIndex();
		table.getColumnModel().getColumn(index).setPreferredWidth(0);
		table.getColumnModel().getColumn(index).setMinWidth(0);
		table.getColumnModel().getColumn(index).setMaxWidth(0);

		TableColumnModel cm = table.getColumnModel();
		// 必填项
		TableColumn tableColumn = cm.getColumn(2);
		tableColumn.setMinWidth(100);
		tableColumn.setMaxWidth(100);

		// 自定义表头UI
		table.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		table.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				tablemouseReleased(evt);
			}
		});
	}

	private void tablemouseReleased(MouseEvent evt) {
		if (evt.getClickCount() == 1) {
			int row = table.getSelectedRow();
			if (row != -1) {
				// 获取选中行集合
				List<JecnConfigItemBean> itemList = this.getSelectedItemBeanList();
				// if (itemList.size() == 1 &&
				// "a10".equals(itemList.get(0).getMark())) {// 选中活动说明
				// this.dialog.getRightPanel().getPropertySetButton().setEnabled(true);
				// } else if (itemList.size() == 1 &&
				// "a8".equals(itemList.get(0).getMark())) {// 角色/职责
				// this.dialog.getRightPanel().getPropertySetButton().setEnabled(true);
				// } else if (itemList.size() == 1
				// &&
				// ConfigItemPartMapMark.a16.toString().equals(itemList.get(0).getMark()))
				// {// 客户
				// this.dialog.getRightPanel().getPropertySetButton().setEnabled(true);
				// } else {
				// this.dialog.getRightPanel().getPropertySetButton().setEnabled(false);
				// }

				if (itemList.size() == 1) {// 选中活动说明
					this.dialog.getRightPanel().getPropertySetButton().setEnabled(true);
				}
			}
		}
	}

	/**
	 * 
	 * 获取表数据
	 * 
	 * @param itemList
	 *            List<JecnConfigItemBean>
	 * 
	 * @return Object[][]
	 */
	protected Object[][] getTableData(List<JecnConfigItemBean> itemList) {
		if (itemList == null || itemList.size() == 0) {
			return null;
		}
		// 创建数组
		Object[][] data = new Object[itemList.size()][cloumnNames.length];

		// “必填”
		String notEmptyText = JecnProperties.getValue("flowFileISEmpty");

		for (int i = 0; i < itemList.size(); i++) {
			JecnConfigItemBean itemBean = itemList.get(i);
			data[i][0] = itemBean.getName();
			data[i][1] = itemBean.getEnName();
			data[i][2] = getNotEmpty(itemBean.getIsEmpty(), notEmptyText);
			data[i][3] = itemBean;
		}
		return data;
	}
}
