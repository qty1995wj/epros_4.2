package com.jecn.epros.domain.process.kpi;

import java.util.List;

/**
 * Created by admin on 2017/10/13.
 */
public class KpiSearchResultData {
    private boolean operation;
    private List<KPIValuesBean> kpiValues;

    public boolean isOperation() {
        return operation;
    }

    public void setOperation(boolean operation) {
        this.operation = operation;
    }

    public List<KPIValuesBean> getKpiValues() {
        return kpiValues;
    }

    public void setKpiValues(List<KPIValuesBean> kpiValues) {
        this.kpiValues = kpiValues;
    }
}
