package epros.draw.gui.figure.shape.total;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnPaintEnum;

public class TrialReport extends JecnBaseFigurePanel {
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int x5;
	private int y5;
	private int doubleLineWidth = 0;

	public TrialReport(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;

		doubleLineWidth = JecnWorkflowConstant.DOUBLE_LINE_SPACE;
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = userWidth / 5;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight / 5;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth * 4 / 5;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight / 5;
			x4 = userWidth;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight * 4 / 5;
			x4 = userWidth * 4 / 5;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = userWidth / 5;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight * 4 / 5;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintHandTop(g2d);
	}

	private void paintHandTop(Graphics2D g2d) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
		}
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintHandTop(g2d);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DHandLow(g2d, 0, 0);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DHandLow(g2d, 3, 3);
	}

	private void paint3DHandLow(Graphics2D g2d, int indent3Dvaluemin, int indent3Dvaluemax) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemax, x4 + indent3Dvaluemin,
					x5 + indent3Dvaluemin }, new int[] { y1 + indent3Dvaluemin, y2 + indent3Dvaluemin,
					y3 - indent3Dvaluemin, y4 - indent3Dvaluemax, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvaluemin, x2, x3 - indent3Dvaluemax, x4 - indent3Dvaluemin,
					x5 + indent3Dvaluemin }, new int[] { y1 + indent3Dvaluemin, y2 + indent3Dvaluemin, y3,
					y4 - indent3Dvaluemax, y5 - indent3Dvaluemin }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvaluemin, x2 - indent3Dvaluemin, x3, x4, x5 + indent3Dvaluemin },
					new int[] { y1 + indent3Dvaluemin, y2 + indent3Dvaluemin, y3, y4 - indent3Dvaluemax,
							y5 - indent3Dvaluemax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvaluemin, x2 - indent3Dvaluemax, x3 - indent3Dvaluemax, x4, x5 },
					new int[] { y1 + indent3Dvaluemin, y2 + indent3Dvaluemin, y3 - indent3Dvaluemin,
							y4 - indent3Dvaluemin, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paintInputHandLine(g2d, 0, 0);
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paintInputHandLine(Graphics2D g2d, int indent3Dvaluemin, int indent3Dvaluemmax) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemmax, x4 - indent3Dvaluemin, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemmax, y5 - indent3Dvaluemmax }, 5);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemmax, x4 - indent3Dvaluemin, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemmax, y5 - indent3Dvaluemin }, 5);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawPolygon(
					new int[] { x1 + indent3Dvaluemin, x2, x3 - indent3Dvaluemmax, x4 - indent3Dvaluemmax, x5 },
					new int[] { y1 - indent3Dvaluemin, y2, y3, y4 - indent3Dvaluemmax, y5 - indent3Dvaluemmax }, 5);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawPolygon(
					new int[] { x1 - indent3Dvaluemin, x2 - indent3Dvaluemmax, x3 - indent3Dvaluemmax, x4, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemin, y5 }, 5);
		}
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DInputHandLine(g2d, 3, doubleLineWidth);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		paint3DInputHandLine(g2d, 3, doubleLineWidth);
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paint3DInputHandLine(Graphics2D g2d, int indent3Dvaluemin, int indent3Dvaluemax) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemax, x3 - indent3Dvaluemax, x4 + indent3Dvaluemax,
					x5 + indent3Dvaluemax }, new int[] { y1 + indent3Dvaluemax, y2 + indent3Dvaluemax,
					y3 - indent3Dvaluemax, y4 - indent3Dvaluemax, y5 + indent3Dvaluemin }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvaluemax, x2 - indent3Dvaluemax, x3 - indent3Dvaluemax,
					x4 - indent3Dvaluemax, x5 + indent3Dvaluemax }, new int[] { y1 + indent3Dvaluemax,
					y2 + indent3Dvaluemax, y3 + indent3Dvaluemax, y4 - indent3Dvaluemax, y5 - indent3Dvaluemax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvaluemax, x2 - indent3Dvaluemax, x3 - indent3Dvaluemax,
					x4 - indent3Dvaluemax, x5 + indent3Dvaluemax }, new int[] { y1 + indent3Dvaluemax,
					y2 + indent3Dvaluemax, y3 - indent3Dvaluemax, y4 - indent3Dvaluemax, y5 - indent3Dvaluemax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvaluemax, x2 - indent3Dvaluemax, x3 - indent3Dvaluemax,
					x4 + indent3Dvaluemax, x5 + indent3Dvaluemax }, new int[] { y1 + indent3Dvaluemax,
					y2 + indent3Dvaluemax, y3 - indent3Dvaluemax, y4 - indent3Dvaluemax, y5 - indent3Dvaluemax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DAndShadowsPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2,
				x3, x4, x5, 0, y1, y2, y3, y4, y5, 0, doubleLineWidth, userWidth, userHeight);
	}
}
