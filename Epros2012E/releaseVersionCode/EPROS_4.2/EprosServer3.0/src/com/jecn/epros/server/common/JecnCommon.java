package com.jecn.epros.server.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.sql.Clob;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.util.JSONUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

public class JecnCommon {
	private static final Logger log = Logger.getLogger(JecnCommon.class);

	/**
	 * @author yxw 2012-5-23
	 * @description:
	 * @param currentPage当前页
	 * @param pageSize每页显示行数
	 * @param pagerMethod
	 *            操作方式
	 * @param totalRows
	 *            总数
	 * @return
	 */
	public static Pager getPager(int currentPage, int pageSize, String pagerMethod, int totalRows) {

		Pager pager = new Pager(totalRows, pageSize);
		// 如果当前页号为空,表示为首次查询该页
		// 如果不为0,则刷新pager对象,输入当前页号等信息
		if (currentPage != 0) {
			pager.refresh(currentPage);
		}
		// 获取当前执行的方法,首页,前一页,后一页,尾页.
		if (pagerMethod != null) {
			if (pagerMethod.equals("home")) {
				pager.first();
			} else if (pagerMethod.equals("previous")) {
				pager.previous();
			} else if (pagerMethod.equals("next")) {
				pager.next();
			} else if (pagerMethod.equals("end")) {
				pager.last();
			}
		}
		return pager;

	}

	/**
	 * @author yxw 2012-5-9
	 * @description:通过字符串转化成Enum
	 * @param str
	 * @return
	 */
	public static TreeNodeType getTreeNodeType(String str) {

		return Enum.valueOf(TreeNodeType.class, str);
	}

	/**
	 * @author zhangchen May 23, 2012
	 * @description：去掉最后一个字符
	 * @param strBuffer
	 * @return
	 */
	public static String getStringByBuffer(StringBuffer strBuffer) {
		if (!"".equals(strBuffer.toString())) {
			return strBuffer.toString().substring(0, strBuffer.toString().length() - 1);
		}
		return "";
	}

	/**
	 * @author zhangchen Jul 3, 2012
	 * @description：通过所有节点得到所有子节点包括自己
	 * @param idsList
	 * @param listAll
	 * @return
	 */
	private static void getAllChilds(Long id, List<Object[]> listAll, Set<Long> set) {
		for (Object[] obj : listAll) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			if (obj[1].toString().equals(id.toString())) {
				set.add(Long.valueOf(obj[0].toString()));
				getAllChilds(Long.valueOf(obj[0].toString()), listAll, set);
			}
		}
	}

	/**
	 * @author zhangchen Jul 3, 2012
	 * @description：通过所有节点得到所有子节点包括自己
	 * @param idsList
	 * @param listAll
	 * @return
	 */
	public static Set<Long> getAllChilds(Long orgId, List<Object[]> listAll) {
		Set<Long> set = new HashSet<Long>();
		set.add(orgId);
		getAllChilds(orgId, listAll, set);
		return set;
	}

	/**
	 * @author zhangchen Jul 3, 2012
	 * @description：通过所有节点得到所有子节点包括自己
	 * @param idsList
	 * @param listAll
	 * @return
	 */
	public static Set<Long> getAllChilds(List<Long> listIds, List<Object[]> listAll) {
		Set<Long> set = new HashSet<Long>();
		for (Long id : listIds) {
			set.add(id);
			getAllChilds(id, listAll, set);
		}
		return set;
	}

	/**
	 * @author zhangchen Jul 3, 2012
	 * @description：通过所有节点得到所有子节点包括自己
	 * @param idsList
	 * @param listAll
	 * @return
	 */
	public static Set<Long> getAllChilds(Set<Long> setIds, List<Object[]> listAll) {
		Set<Long> set = new HashSet<Long>();
		Iterator<Long> it = setIds.iterator();
		while (it.hasNext()) {
			Long id = it.next();
			set.add(id);
			getAllChilds(id, listAll, set);
		}
		return set;
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：获得唯一ID
	 * @return
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 字符串转换日期类型
	 * 
	 * @param str
	 * @return
	 */
	public static Date getDateByString(String str) {
		if (isNullOrEmtryTrim(str)) {
			return null;
		}
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return dateformat.parse(str);
		} catch (ParseException e) {
			log.error("", e);
		}
		return null;
	}

	public static Date getDateByString(String str, String format) {
		if (isNullOrEmtryTrim(str)) {
			return null;
		}
		SimpleDateFormat dateformat = new SimpleDateFormat(format);
		try {
			return dateformat.parse(str);
		} catch (ParseException e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * 字符串转换时间类型
	 * 
	 * @param str
	 * @return
	 */
	public static Date getDateTimeByString(String str) {
		if (isNullOrEmtryTrim(str)) {
			return null;
		}
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return dateformat.parse(str);
		} catch (ParseException e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * 日期转换成字符串时间格式 到时分秒
	 * 
	 * @param date
	 *            Date日期
	 * @return String 日期格式
	 */
	public static String getStringbyDateHMS(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	/**
	 * 日期转换成字符串时间格式 到时分秒
	 * 
	 * @param date
	 *            Date日期
	 * @return String 日期格式
	 */
	public static String getStringbyDate(Date date) {
		if (date == null) {
			return null;
		}
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	public static String getStringbyDate(Date date, String fmt) {
		return new SimpleDateFormat(fmt).format(date);
	}

	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

	/**
	 * 
	 * 校验邮箱格式
	 * 
	 * @param mailAddr
	 *            String
	 * @return boolean 验证成功：true；验证失败：false
	 */
	public static boolean checkMailFormat(String mailAddr) {
		if (isNullOrEmtryTrim(mailAddr)) {
			return false;
		}
		// 邮箱格式不正确
		Pattern emailer = Pattern.compile(JecnContants.EMAIL_FORMAT);
		if (!emailer.matcher(mailAddr).matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 两个时间段相差月数
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getMonth(Date start, Date end) {
		if (start.after(end)) {
			Date t = start;
			start = end;
			end = t;
		}
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(start);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(end);
		Calendar temp = Calendar.getInstance();
		temp.setTime(end);
		temp.add(Calendar.DATE, 1);

		int year = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		int month = endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

		if ((startCalendar.get(Calendar.DATE) == 1) && (temp.get(Calendar.DATE) == 1)) {
			return year * 12 + month + 1;
		} else if ((startCalendar.get(Calendar.DATE) != 1) && (temp.get(Calendar.DATE) == 1)) {
			return year * 12 + month;
		} else if ((startCalendar.get(Calendar.DATE) == 1) && (temp.get(Calendar.DATE) != 1)) {
			return year * 12 + month;
		} else {
			return (year * 12 + month - 1) < 0 ? 0 : (year * 12 + month);
		}
	}

	/**
	 * 解密 以String密文输入,String明文输出
	 * 
	 * @param strMi
	 * @return
	 */
	public static String getDesString(String strMi) {
		if (isNullOrEmtryTrim(strMi)) {
			return null;
		}
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] decodedBytes = null;
		String a = "";
		try {
			decodedBytes = decoder.decodeBuffer(strMi);
			a = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			log.error("BASE64Decoder解密异常", e);
		}
		return a;
	}

	/**
	 * 根据浏览器返回不同的编码格式的 字符串
	 * 
	 * @author fuzhh Apr 3, 2013
	 * @param str
	 * @return
	 */
	// public static String getURLEncoder(HttpServletRequest request, String
	// str) {
	// try {
	// String Agent = request.getHeader("User-Agent");
	// if (null != Agent) {
	// Agent = Agent.toLowerCase();
	// if (Agent.indexOf("firefox") != -1) {
	// str = new String(str.getBytes(), "iso8859-1");
	// } else if (Agent.indexOf("msie") != -1) {
	// str = URLEncoder.encode(str, "UTF-8");
	// } else {
	// str = URLEncoder.encode(str, "UTF-8");
	// }
	// }
	// } catch (UnsupportedEncodingException e) {
	// e.printStackTrace();
	// }
	// return str;
	// }
	/**
	 * 根据获取的客户端的字符转换编码格式
	 * 
	 * @author fuzhh Apr 18, 2013
	 * @param response
	 * @param str
	 * @return
	 */
	public static String getDownFileNameByEncoding(HttpServletResponse response, String str, String isApp) {
		try {
			if ("true".equals(isApp)) {
				return new String(str.getBytes("UTF-8"), "ISO8859-1");
			}
			return new String(str.getBytes(), response.getCharacterEncoding());
		} catch (UnsupportedEncodingException e) {
			log.error("根据获取的客户端的字符转换编码格式异常！", e);
			return "";
		}
	}

	/**
	 * 判断是否为数字
	 * 
	 * @author fuzhh May 2, 2013
	 */
	public static boolean judgeObjIsNum(String str) {
		if (str.matches("[0-9]*")) {
			return true;
		}
		return false;
	}

	public static boolean isOracle() {
		return (JecnContants.dbType.equals(DBType.ORACLE)) ? true : false;
	}

	public static boolean isSQLServer() {
		return (JecnContants.dbType.equals(DBType.SQLSERVER)) ? true : false;
	}

	public static boolean isMySql() {
		return (JecnContants.dbType.equals(DBType.MYSQL)) ? true : false;
	}

	/**
	 * 流程地图可添加附件元素
	 * 
	 * @param figureType
	 * @return
	 */
	public static boolean isInfoFigure(String figureType) {
		if (figureType.equals("Oval") || figureType.equals("Triangle") || figureType.equals("FunctionField")
				|| figureType.equals("Pentagon") || figureType.equals("RightHexagon")
				|| figureType.equals("ModelFigure") || figureType.equals("InputHandFigure")
				|| figureType.equals("SystemFigure") || figureType.equals("IsoscelesTrapezoidFigure")
				|| figureType.equals("FileImage") || figureType.equals("MapRhombus")) {
			return true;
		}
		// 流程地图：圆形 9
		// 流程地图 三角形 10
		// 流程地图 矩形功能域 11
		// 五边形 12
		// 箭头六边形 13
		// 泳池 14
		// 手动输入 19
		// 制度 20
		// 等腰梯形
		// 文档
		return false;
	}

	/**
	 * 转换json
	 * 
	 * @author fuzhh 2013-11-21
	 * @param stores
	 * @param columns
	 * @param dataList
	 * @return 转换后的值
	 */
	public static String composeJson(String stores, String columns, String str, int noPubTotal, int approveTotal,
			int pubTotal, int allTotal) {
		if (isNullOrEmtryTrim(stores) || isNullOrEmtryTrim(columns) || isNullOrEmtryTrim(str)) {
			return null;
		}
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append("\"stores\":" + stores + ",");
		builder.append("\"columns\":" + columns + ",");
		builder.append("\"dataList\":" + str + ",");
		builder.append("\"noPubTotal\":" + noPubTotal + ",");
		builder.append("\"approveTotal\":" + approveTotal + ",");
		builder.append("\"pubTotal\":" + pubTotal + ",");
		builder.append("\"allTotal\":" + allTotal);
		builder.append("}");
		return builder.toString();
	}

	/**
	 * 列表表头对应数据 Json拼装
	 * 
	 * @author fuzhh 2013-12-2
	 * @param name
	 * @param mapping
	 * @return
	 */
	public static String storesJson(String name, String mapping) {
		return "{\"name\" : \"" + name + "\"," + "\"mapping\" : \"" + mapping + "\"}";
	}

	/**
	 * 表头Json拼装
	 * 
	 * @author fuzhh 2013-12-2
	 * @param header
	 * @param dataIndex
	 * @param linkName
	 * @return
	 */
	public static String columnsJson(String header, String dataIndex, String linkName) {
		String str = "{\"header\" :\"" + header + "\"," + "\"dataIndex\" : \"" + dataIndex + "\","
				+ "\"align\" : \"center\"";
		if (!isNullOrEmtryTrim(linkName)) {
			str = str + ",\"renderer\":" + linkName + "}";
		} else {
			str = str + "}";
		}
		return str;
	}

	/**
	 * 内容拼装
	 * 
	 * @author fuzhh 2013-12-2
	 * @param name
	 * @param value
	 * @return
	 */
	public static String contentJson(Object name, Object value) {
		String val = "\"\"";
		if (value != null) {
			val = JSONUtils.quote(value.toString());
		}
		return "\"" + name + "\":" + val + "";
	}

	/**
	 * 内容拼装
	 * 
	 * @author fuzhh 2013-12-2
	 * @param name
	 * @param value
	 * @return
	 */
	public static String contentBrJson(Object name, Object value) {
		String val = "";
		if (value != null) {
			val = value.toString().replaceAll("\n", "<br>");
		}
		return "\"" + name + "\":\"" + val + "\"";
	}

	/**
	 * 
	 * @author yxw 2013年8月20日
	 * @description:替换换行符
	 * @param s
	 * @return
	 */
	public static String quote(String s) {
		return s.replaceAll("\n", "");
	}

	/**
	 * Clob 转换成String方法
	 * 
	 * @param clob
	 * @return String
	 * @throws Exception
	 */
	public static String ClobToString(Clob clob) throws Exception {
		if (clob == null) {
			return "";
		}
		String reString = "";
		BufferedReader br = null;
		Reader is = null;
		try {
			is = clob.getCharacterStream();// 得到流
			br = new BufferedReader(is);
			String s = br.readLine();
			StringBuffer sb = new StringBuffer();
			while (s != null) {// 执行循环将字符串全部取出付值给StringBuffer由StringBuffer转成STRING
				sb.append(s);
				s = br.readLine();
			}
			reString = sb.toString();
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					log.error("", e);
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					log.error("", e);
				}
			}
		}

		return reString;
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param curStr
	 *            传入的参数
	 * @return
	 */
	public static boolean checkIsNumber(String curStr) {
		if (curStr.matches("[0-9]*")) {
			return true;
		}
		return false;
	}

	/**
	 * 制度类型
	 * 
	 * @author fuzhh 2013-12-27
	 * @param ruleType
	 * @return
	 */
	public static String ruleTypeVal(int ruleType) {
		String type = "";
		if (ruleType == 1) {
			// 制度模板
			type = JecnUtil.getValue("ruleModeFile");
		} else if (ruleType == 2) {
			// 制度文件
			type = JecnUtil.getValue("ruleFile");
		}
		return type;
	}

	/**
	 * 标准类型
	 * 
	 * @author fuzhh 2013-12-27
	 * @param standardType
	 * @return
	 */
	public static String standardTypeVal(int standardType) {
		// 标准类型
		String stanType = "";
		if (standardType == 1) {// 0是目录，1文件标准 ，2流程标准 3流程地图标准4标准条款5条款要求
			stanType = JecnUtil.getValue("fileStandar");
		} else if (standardType == 2) {// 流程标准
			stanType = JecnUtil.getValue("flowStandar");
		} else if (standardType == 3) {// 流程地图标准
			stanType = JecnUtil.getValue("flowMapStandar");
		} else if (standardType == 4) {// 标准条款
			stanType = JecnUtil.getValue("standarClause");
		} else if (standardType == 5) {// 条款要求
			stanType = JecnUtil.getValue("clauseRequest");
		}
		return stanType;
	}

	/**
	 * 返回流程图保存名称
	 * 
	 * @author fuzhh 2014-2-24
	 * @param flowImgPath
	 * @return
	 */
	public static String getPorcessImagePath(String flowImgPath) {
		int pathNum = flowImgPath.lastIndexOf("/");
		String path = flowImgPath.substring(pathNum, flowImgPath.length());
		return getSavePorcessImagePath(path);
	}

	/**
	 * 获取流程图保存路径
	 * 
	 * @author fuzhh 2014-2-24
	 * @param flowImgPath
	 * @return
	 */
	public static String getSavePorcessImagePath(String flowImgPath) {
		if (JecnCommon.isNullOrEmtryTrim(flowImgPath)) {
			return null;
		}
		int imgNum = flowImgPath.lastIndexOf(".");
		String imgName = flowImgPath.substring(0, imgNum);
		return imgName;
	}

	public static WebLoginBean getWebLoginBean() {
		Object object = ActionContext.getContext().getSession().get(JecnContants.SESSION_KEY);
		return object == null ? null : (WebLoginBean) object;
	}

	public static boolean isAdmin() {
		return getWebLoginBean().getIsAdmin();
	}

	/**
	 * TPath 转换 set集合
	 * 
	 * @param set
	 * @param tpath
	 */
	public static void getIdsByTpath(Set<Long> set, String tpath) {
		if (StringUtils.isEmpty(tpath) || set == null) {
			return;
		}
		String[] strIds = tpath.split("-");
		for (String string : strIds) {
			if (StringUtils.isEmpty(string)) {
				continue;
			}
			set.add(Long.valueOf(string));
		}
	}

	/**
	 * 定位数节点通用集合
	 * 
	 * @param t_path
	 * @param id
	 * @return
	 */
	public static Set<Long> getPositionTreeIds(String t_path, Long id) {
		Set<Long> setIds = new HashSet<Long>();
		setIds.add(0L);
		String[] strArr = t_path.split("-");
		for (String str : strArr) {
			if (!StringUtils.isEmpty(str)) {
				if (!str.equals(id.toString())) {
					setIds.add(Long.valueOf(str));
				}

			}
		}
		return setIds;
	}

	/**
	 * 递归更新移动节点
	 * 
	 * @param ObjArr
	 * @param pId
	 * @param updateTime
	 * @param updatePeopleId
	 * @param t_PathTO
	 * @param levelTo
	 * @param baseDao
	 * @param moveNodeType
	 */
	@SuppressWarnings("unchecked")
	private static void updateNodesChildTreeBeansChild(List<Object[]> ObjArr, Long pId, String pubPerNumber,
			Date updateTime, Long updatePeopleId, String t_PathTO, int levelTo, IBaseDao baseDao,
			TreeNodeType moveNodeType, int isHide, String pViewSort) {
		for (Object[] obj : ObjArr) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			if (obj[1].toString().equals(pId.toString())) {
				Long id = Long.valueOf(obj[0].toString());
				String t_path = t_PathTO + id.toString() + "-";
				int level = levelTo + 1;
				int sortId = 0;
				if (obj[3] != null) {
					sortId = Integer.valueOf(obj[3].toString());
				}
				String viewSort = JecnUtil.concatViewSort(pViewSort, sortId);
				List<String> strSqls = JecnCommonSql.getUpdateMoveNodeSql(pId, pubPerNumber, id, t_path, level,
						updateTime, updatePeopleId, moveNodeType, isHide, viewSort);
				for (String sql : strSqls) {
					baseDao.execteNative(sql);
				}
				String perNumber = "";
				if (obj[2] != null) {
					perNumber = obj[2].toString();
				}
				JecnCommon.updateNodesChildTreeBeansChild(ObjArr, id, perNumber, updateTime, updatePeopleId, t_path,
						level, baseDao, moveNodeType, isHide, viewSort);
			}
		}
	}

	/**
	 * 设置节点移动后，子节点tpath和tlevel
	 * 
	 * @param listIds
	 *            移动的节点ID集合
	 * @param pId
	 *            移动到的父节点ID
	 * @param isHide
	 *            :1显示，0隐藏
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void updateNodesChildTreeBeans(List<Long> listIds, Long pId, String pubPerNumber, Date updateTime,
			Long updatePeopleId, String t_PathTO, int levelTo, IBaseDao baseDao, TreeNodeType moveNodeType, int isHide)
			throws Exception {

		// 获得移动到的目录的view_sort
		String pViewSort = "";
		Set<TreeNodeType> escape = JecnUtil.getNoViewSortNodeType();
		if (!escape.contains(moveNodeType)) {// 岗位移动不查询pViewSort
			Map<String, Object> m = baseDao.getNativeResultToMap(getViewSort(pId, moveNodeType));
			if (m != null) {
				pViewSort = m.get("VIEW_SORT") != null ? m.get("VIEW_SORT").toString() : null;
			}
		}
		// String updateDateStr = JecnCommon.getStringbyDateHMS(updateTime);
		// 获取所有子节点;安装tpath 由大到小排序；
		List<Object[]> ObjArr = baseDao
				.listNativeSql(JecnCommonSql.getChildObjectsSqlmoveByType(listIds, moveNodeType));
		for (Long id : listIds) {
			for (Object[] obj : ObjArr) {
				if (obj == null || obj[0] == null || obj[1] == null) {
					continue;
				}
				if (id.toString().equals(obj[0].toString())) {
					String t_path = t_PathTO + id + "-";
					int level = levelTo + 1;
					// obj[3]为sortid
					int sortId = 0;
					if (obj[3] != null) {
						sortId = Integer.valueOf(obj[3].toString());
					}
					String viewSort = JecnUtil.concatViewSort(pViewSort, sortId);
					List<String> strSqls = JecnCommonSql.getUpdateMoveNodeSql(pId, pubPerNumber, id, t_path, level,
							updateTime, updatePeopleId, moveNodeType, isHide, viewSort);
					for (String sql : strSqls) {
						baseDao.execteNative(sql);
					}
					String perNumber = "";
					if (obj[2] != null) {
						perNumber = obj[2].toString();
					}
					updateNodesChildTreeBeansChild(ObjArr, id, perNumber, updateTime, updatePeopleId, t_path, level,
							baseDao, moveNodeType, isHide, viewSort);
				}
			}
		}
	}

	/**
	 * true 启用二级管理员
	 * 
	 * @return boolean
	 */
	public static boolean isShowSecondAdmin() {
		return JecnContants.SHOW_VALUE.equals(JecnContants.getValue(ConfigItemPartMapMark.showSecondAdminAccessConfig
				.toString())) ? true : false;
	}

	/**
	 * 
	 * @param id
	 * @param moveNodeType
	 * @return
	 */
	public static String getViewSort(Long id, TreeNodeType moveNodeType) {
		String sql = "SELECT VIEW_SORT FROM ";
		switch (moveNodeType) {
		case process:
		case processFile:
		case processMap:
		case processMapMode:
		case processMode:
			sql += "JECN_FLOW_STRUCTURE_T where flow_id";
			break;
		case standardDir:
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
		case standardClauseRequire:
			sql += "JECN_CRITERION_CLASSES WHERE CRITERION_CLASS_ID";
			break;
		case organization:
			sql += "JECN_FLOW_ORG where ORG_ID";
			break;
		case ruleDir:
		case ruleFile:
		case ruleModeFile:
			sql += "JECN_RULE_T WHERE ID";
			break;
		case riskDir:
		case riskPoint:
			sql += "JECN_RISK WHERE ID";
			break;
		case fileDir:
		case file:
			sql += "JECN_FILE_T WHERE FILE_ID";
			break;
		case positionGroupDir:
		case positionGroup:
			sql += "JECN_POSITION_GROUP WHERE ID";
			break;
		case termDefineDir:
		case termDefine:
			sql += "TERM_DEFINITION_LIBRARY WHERE ID";
			break;
		case ruleModeDir:
		case ruleModeRoot:
			sql = "";
			break;
		default:
			throw new IllegalArgumentException("getViewSort不识别的类型" + moveNodeType);
		}
		sql += "=" + id;
		return sql;
	}

	public static boolean isProcess(TreeNodeType type) {
		return type == TreeNodeType.process || type == TreeNodeType.processFile;
	}

	public static List<Long> getIdsByTreeBeans(List<JecnTreeBean> list) {
		if (list == null) {
			return new ArrayList<Long>();
		}
		List<Long> resultIds = new ArrayList<Long>();
		for (JecnTreeBean jecnTreeBean : list) {
			resultIds.add(jecnTreeBean.getId());
		}
		return resultIds;
	}

	/**
	 * 给树节点赋予任务状态
	 * 
	 * @param id
	 * @param listTaskState
	 * @param jecnTreeBean
	 */
	public static void setTaskState(Long id, List<Object[]> listTaskState, JecnTreeBean jecnTreeBean) {
		if (listTaskState == null) {
			return;
		}
		for (Object[] objs : listTaskState) {
			if (objs == null || objs[0] == null || objs[1] == null || objs[2] == null) {
				continue;
			}
			if (objs[0].toString().equals(id.toString())) {
				jecnTreeBean.setState(Integer.parseInt(objs[1].toString()));
				if ("0".equals(objs[2].toString()) || "2".equals(objs[2].toString())) {
					jecnTreeBean.setApproveType(Integer.parseInt(objs[2].toString()));
				}
				if (objs[3] != null) {
					jecnTreeBean.setEdit(Integer.valueOf(objs[3].toString()));
				}
				break;
			}
		}
	}

	/**
	 * 换行符替换
	 * 
	 * @param str
	 * @return
	 */
	public static String replaceWarp(String str) {
		return StringUtils.isBlank(str) ? "" : str.replaceAll("\n", "<br>");
	}

	/**
	 * 换行符替换
	 * 
	 * @param str
	 * @return
	 */
	public static StringBuffer replaceWarp(String string, String character) {
		StringBuffer stringBuffer = new StringBuffer();
		if (StringUtils.isBlank(string)) {
			return stringBuffer;
		}
		String[] str = string.split("\n");
		for (int i = 0; i < str.length; i++) {
			int s = 0;
			int stringLength = 50;
			if (str[i].length() == 0) {
				s = str[i].length() / stringLength;
			} else {
				s = str[i].length() / stringLength + 1;
			}
			for (int j = 0; j < s; j++) {
				if (str[i].length() < stringLength) {
					string = str[i] + "\n";
				} else {
					string = str[i].substring(0, stringLength);
					str[i] = str[i].substring(stringLength);
				}
				string = string.replaceAll("\n", "<br>" + character);
				stringBuffer.append(string);
			}
		}
		return stringBuffer;
	}

	/**
	 * 获得模板
	 * 
	 * @param id
	 *            资源
	 * @param type
	 *            资源类型 0 流程 1文件 2制度
	 * @param approveType
	 *            审批类型
	 * @return
	 */
	public static List<TaskConfigItem> getTaskConfigItems(IBaseDao baseDao, long id, int type, int approveType) {
		String templateId = getTemplateId(baseDao, id, type, approveType);
		List<TaskConfigItem> result = null;
		if (templateId != null) {
			String hql = "from TaskConfigItem where templetId=? order by sort asc";
			result = baseDao.listHql(hql, templateId);
		}
		return result;
	}

	public static String getTemplateId(IBaseDao baseDao, long id, int type, int approveType) {
		List<Long> ids = new ArrayList<Long>();
		String path = null;
		int resourceType = type;
		switch (type) {
		case 0:
		case 4:
			String hql = "from JecnFlowStructureT where flowId=?";
			JecnFlowStructureT f = (JecnFlowStructureT) baseDao.getObjectHql(hql, id);
			path = f.gettPath();
			resourceType = f.getIsFlow() == 0 ? 4 : 0;
			break;
		default:
			throw new IllegalArgumentException("getTaskConfigItems unsport type :" + type);
		}

		if (StringUtils.isNotBlank(path)) {
			String[] arr = path.split("-");
			for (int i = arr.length - 1; i >= 0; i--) {
				Long a = Long.valueOf(arr[i]);
				if (a != null) {
					ids.add(a);
				}
			}
		}
		if (ids.isEmpty()) {
			ids.add(id);
		}
		String sql = " select max(tt.id) from task_templet tt" + " inner join prf_related_templet prt "
				+ " on tt.id=prt.templet_id and prt.templet_type=? and prt.type=? and prt.related_id=?"
				+ " where exists(                                    "
				+ " select 1 from task_config_item tci where tt.id = tci.templet_id)";
		String templateId = null;
		for (Long a : ids) {
			templateId = (String) baseDao.getObjectNativeSql(sql, approveType, resourceType, a);
			if (templateId != null) {
				break;
			}
		}
		return templateId;
	}

	public static List<TaskConfigItem> getVisibleTaskConfigs(List<TaskConfigItem> taskConfigItems) {
		List<TaskConfigItem> result = new ArrayList<TaskConfigItem>();
		if (JecnUtil.isEmpty(taskConfigItems)) {
			return result;
		}
		for (TaskConfigItem c : taskConfigItems) {
			if ("1".equals(c.getValue())) {
				result.add(c);
			}
		}
		return result;
	}

}
