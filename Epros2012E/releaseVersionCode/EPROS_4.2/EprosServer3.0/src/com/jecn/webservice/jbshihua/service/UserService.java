package com.jecn.webservice.jbshihua.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;
import com.jecn.webservice.jbshihua.entity.PostInfo;
import com.jecn.webservice.jbshihua.entity.UserInfo;

@Service
public interface UserService extends IBaseService<JecnUserPosView, String> {
	public int receiveUser(List<JecnUserPosView> users) throws Exception;

	public int saveUsers(List<JecnUserPosView> users) throws Exception;

	public List<JecnUserPosView> getAllUsers() throws Exception;

	void receiveJob(List<PostInfo> postList) throws Exception;
}
