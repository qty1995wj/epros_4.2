package com.jecn.epros.server.common;

/**
 *
 * 常量类
 *
 * @author ZHOUXY
 *
 */
public class JecnConfigContents {
    /** 是否是表中数据 */
    public static final int TABLE_DATA_YES = 1;
    /** 是否是表之外数据 */
    public static final int TABLE_DATA_NO = 0;

    /** 显示 */
    public static final String ITEM_IS_SHOW = "1";
    /** 不显示 */
    public static final String ITEM_IS_NOT_SHOW = "0";

    /** 必填 */
    public static final int ITEM_NOT_EMPTY = 1;
    /** 非必填） */
    public static final int ITEM_EMPTY = 0;

    /** 流程责任人类型:人 */
    public static final String ITEM_FLOW_USER = "0";
    /** 流程责任人类型:岗位 */
    public static final String ITEM_FLOW_POS = "1";

    /** 启用；支持 */
    public static final String ITEM_AUTO_SAVE_YES = "1";
    /** 禁止；不支持 */
    public static final String ITEM_AUTO_SAVE_NO = "0";
    /** 1：文控审核主导审批发布 */
    public static final String ITEM_DOC_CONTROL = "1";
    /** 0：正常审批发布（拟稿人主导发布）（默认） */
    public static final String ITEM_USER_CONTROL = "0";

    /** 流程文件类型：段落 */
    public static final String ITEM_PARAGRAPH = "1";
    /** 流程文件类型：表格（默认） */
    public static final String ITEM_TABLE = "0";

    /** 全局权限(0：秘密 1：公开):秘密 */
    public static final String ITEM_PUB_SECRET = "0";
    /** 全局权限(0：秘密 1：公开)：公开 */
    public static final String ITEM_PUB_OPEN = "1";

    // 大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置//
    /** 0：流程地图 */
    public static final int TYPE_BIG_ITEM_TOTALMAP = 0;
    /** 1：流程图 */
    public static final int TYPE_BIG_ITEM_PARTMAP = 1;
    /** 2：制度 */
    public static final int TYPE_BIG_ITEM_ROLE = 2;
    /** 3：文件 */
    public static final int TYPE_BIG_ITEM_FILE = 3;
    /** 4：邮箱配置 */
    public static final int TYPE_BIG_ITEM_MAIL = 4;
    /** 5：权限配置 */
    public static final int TYPE_BIG_ITEM_PUB = 5;
    /** 6:其他设置 */
    public static final int TYPE_BIG_ITEM_OTHER = 6;
    // 大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置//

    // 小类：0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
    // 7:任务操作8：消息邮件配置//
    /** 0：审批环节配置 */
    public static final int TYPE_SMALL_ITEM_TASKAPP = 0;
    /** 1：任务界面显示项配置 */
    public static final int TYPE_SMALL_ITEM_TASKSHOW = 1;
    /** 2：基本配置 */
    public static final int TYPE_SMALL_ITEM_BASICINFO = 2;
    /** 3：流程图建设规范 */
    public static final int TYPE_SMALL_ITEM_STAND = 3;
    /** 4：流程文件 */
    public static final int TYPE_SMALL_ITEM_FLOWFILE = 4;
    /** 5：邮箱配置 */
    public static final int TYPE_SMALL_ITEM_MAIL = 5;
    /** 6：权限配置 */
    public static final int TYPE_SMALL_ITEM_PUB = 6;
    /** 7：任务操作配置 */
    public static final int TYPE_SMALL_TASK_OPERATE_ITEM_PUB = 7;
    /** 8：消息邮件配置 */
    public static final int TYPE_SMALL_MES_EMAIL_ITEM_PUB = 8;
    /** 9：合理化建议 */
    public static final int TYPE_SMALL_RATION_PRO_ITEM_PUB = 9;
    /** 10：流程属性配置 */
    public static final int TYPE_SMALL_FLOW_BASIC = 10;
    /** 11：发布时，相关的未发布文件是否同步发布 相关配置 */
    public static final int TYPE_SMALL_PUBLIC_RELATE_FILE = 11;
    /** 14: 文件下载次数配置 */
    public static final int TYPE_SMALL_DOWNLOAD_COUNT = 14;
    /** 15：文控信息配置 */
    public static final int TYPE_SMALL_DOCUMENT_CONTROL = 15;
    /** 16：流程KPI属性名称配置 */
    public static final int TYPE_SMALL_KPI_ATTRIBUTE = 16;
    /** 其它 */
    public static final int TYPE_SMALL_OTHOR = 17;
    /** 18：流程架构-其它配置 */
    public static final int TYPE_PROCESS_MAP_OTHER = 18;

    // 小类：0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
    // 7:任务操作8：消息邮件配置9：合理化建议//
    public static final String EN_COLON = ":";
    public static final String CH_COLON = "：";

    /**
     *
     * 流程设置有用的唯一标识
     *
     */
    public enum ConfigItemPartMapMark {
        // ********流程文件********//
        a1, // 目的
        a2, // 适用范围
        a3, // 术语定义
        a4, // 流程驱动规则
        a5, // 输入
        a6, // 输出
        a7, // 关键活动
        a8, // 角色/职责
        a9, // 流程图
        a10, // 活动说明
        a11, // 流程记录
        a12, // 操作规范
        a13, // 相关流程
        a14, // 相关制度
        a15, // 流程关键测评指标板
        a16, // 客户
        a17, // 不适用范围
        a18, // 概述
        a19, // 补充说明
        a20, // 记录保存
        a21, // 相关标准
        flowFileType, // 流程文件类型：1:段落；0：表格
        // ********流程文件********//

        // ********基本信息********//
        basicRoleMax, // 角色个数上限
        basicRoleMin, // 角色个数下限
        basicRoundMax, // 活动个数上限
        basicRoundMin, // 活动个数下限
        basicDutyOfficer, // 流程责任人类型：0: 选择人 1:选择岗位
        basicRuleCreateTypeFile, // 制度新建类型：文件（1：支持；0：不支持）
        basicRuleCreateTypeLocalFile, // 制度新建类型：文件（1：支持；0：不支持）
        basicRuleCreateTypeModel, // 制度新建类型：制度（1：支持；0：不支持）
        basicRuleAutoPubVersion, // 制度自动发布版本号
        basicFileAutoPubVersion, // 文件自动发布版本号
        basicFilePudedFileUpdateFlag, // 是否直接更新已发布文件
        basicFilePudedFileDeleteFlag, // 是否直接删除已发布文件

        basicIsAutoSave, // 是否启用自动存档:1：启动 0：不启动
        basicAutoSaveTime, // 自动存档时间：分钟数
        basicCmp, // 公司名称配置
        basicEprosURL, // Epros登录地址
        // ********基本信息********//

        // ********流程图建设规范 【start】********//
        standRoleNumberMax, // 角色上限
        standRoleNumberMin, // 角色下限
        standActivityNumberMax, // 活动上限
        standActivityNumberMin, // 活动下限
        standReworkNumber, // 正反返工符数量对应
        standExistRoleAndActivites, // 角色活动是否对应
        standActivityNumberNotEmpty, // 活动编号是否必填
        standActivityInfoNotEmpty, // 活动说明是否必填
        standKeyActivityInfoNotEmpty, // 关键活动说明是否必填
        standRoleInfoNotEmpty, // 角色职责是否必填
        standRoleExistPosition, // 角色是否存在岗位
        standMainPositionOnly, // 主责岗位唯一
        standCustomRoleNotEmpty, // 客户必须选择岗位或岗位组
        standActivitysExistsInAndOutLine, // 活动存在入和出的连接线
        standActivityNameNotEmpty, // 活动名称是否必填
        standActivityInputNotEmpty, // 活动输入是否必填
        standActivityOutPutNotEmpty, // 活动输出是否必填
        // ********流程图建设规范 【end】********//

        // ********任务审批环节********//
        taskAppDocCtrlUser, // 文控审核人
        taskAppDeptUser, // 部门审核人
        taskAppReviewer, // 评审人会审
        taskAppPzhUser, // 批准人审核
        taskAppOprtUser, // 各业务体系负责人
        taskAppITUser, // IT总监
        taskAppDivisionManager, // 事业部经理
        taskAppGeneralManager, // 总经理
        taskCustomApproval1, // 自定义1
        taskCustomApproval2, // 自定义2
        taskCustomApproval3, // 自定义3
        taskCustomApproval4, // 自定义4
        taskAppPubType, // 主导发布类型：1：文控审核主导审批发布；0：正常审批发布（拟稿人主导发布）（默认）

        // ********任务审批环节********//

        // ********任务界面显示项********//
        taskShowPublic, // 密级
        taskShowDefinition, // 术语定义
        taskShowPhone, // 联系方式
        taskShowNumber, // 流程编号
        taskShowFileType, // 文件类别
        taskShowViewPerm, // 查阅权限
        taskShowTestRun, // 试运行
        taskShowRunCycle, // 试运行周期(天数) 默认1；
        taskShowRunRealseCycle, // 发送试运行报告周期(天数) 默认1；
        // ********任务界面显示项********//

        // ********邮箱邮件********//
        mailBoxInnerServerAddr, // 邮箱内网服务器
        mailBoxInnerMailAddr, // 邮箱内网邮箱地址
        mailBoxInnerPwd, // 邮箱内网密码
        mailBoxInnerLoginName, // 内网邮箱登陆名
        mailBoxInnerPort, // 内网端口号
        mailBoxInnerIsUseSSL, // 内网是否启用ssl
        mailBoxInnerIsUseAnonymity, // 内网是否启用匿名发送
        mailBoxOuterServerAdd, // 邮箱外网服务器
        mailBoxOuterMailAddr, // 邮箱外网邮箱地址
        mailBoxOuterPwd, // 邮箱外网密码
        mailBoxOuterLoginName, // -外网邮箱登陆名
        mailBoxOuterPort, // 外网端口号
        mailBoxOuterIsUseSSL, // 外网是否启用ssl
        mailBoxOuterIsUseAnonymity, // 内网是否启用匿名发送
        mailAddrTip, // 邮件地址提示
        mailPwdTip, // 邮件密码提示
        mailEndContent, // 邮件落款
        // ********邮箱邮件********//

        // ********权限********//
        pubPartMap, // 流程 (0：秘密 1：公开)
        pubTotalMap, // 流程地图 (0：秘密 1：公开)
        pubOrg, // 组织 (0：秘密 1：公开)
        pubPos, // 岗位 (0：秘密 1：公开)
        pubRule, // 制度 (0：秘密 1：公开)
        pubStand, // 标准 (0：秘密 1：公开)
        pubFile, // 文件 (0：秘密 1：公开)

        /** 消息邮件配置 */
        mesApprover, // 审批任务：审批人--消息
        mesDraftPeople, // 审批任务：拟稿人--消息
        mesAddApprover, // 创建任务：审批人--消息
        mesPulishCompetence, // 发布任务：具有查阅权限的人--消息
        mesPulishApprover, // 发布任务：审批人--消息
        mesPulishDraftPeople, // 发布任务：拟稿人--消息
        mesPulishParticipants, // 发布任务：参与人--消息

        emailApprover, // 审批任务：审批人--邮件
        emailDraftPeople, // 审批任务：拟稿人--邮件
        mailTaskApproveTip, // 审批任务：审批超时提醒(启动)
        mailTaskApproveTipValue, // 审批任务：审批超时提醒时间（日）
        emailAddApprover, // 创建任务：审批人--邮件
        emailPulishCompetence, // 发布任务：具有查阅权限的人--邮件
        emailPulishApprover, // 发布任务：审批人--邮件
        emailPulishDraftPeople, // 发布任务：拟稿人--邮件
        emailPulishParticipants, // 发布任务：参与人--邮件

        /** 【消息邮件配置添加流程有效期人员配置】 **/
        mailFlowFictionPeople, // 流程拟制人
        mailFlowGuardianPeople, // 流程监护人
        mailFlowRefRationPeople, // 流程责任人
        mailSystemManager, // 系统管理员

        messageFlowFictionPeople, // 流程拟制人
        messageFlowGuardianPeople, // 流程监护人
        messageFlowRefRationPeople, // 流程责任人
        messageSystemManager, // 系统管理员

        /** 合理化建议配置 */
        proSystemRation, // 系统管理员
        proFlowRefRation, // 流程责任人
        proPubOrSecRation, // 公开

        /** 任务操作 */
        taskOperationReCall, // 撤回
        taskOperationReBack, // 返回
        taskOperationAssigned, // 交办
        taskOperationTransfer, // 转批
        taskOperationCollate, // 打回整理
        enableNewCollate, // 启用新版本的打回整理

        /** 【流程属性配置】 **/
        guardianPeople, // 流程监护人
        fictionPeople, // 流程拟制人
        processValidDefaultValue, // 流程有效期\
        isShowProcessValidity, // 是否可编辑流程有效期
        processValidForever, // 流程有效期是否永久 0：是。1：否
        // ********权限********//

        // ********其他配置********//
        systemLocalFileProfix, // 文件保存路径前缀
        systemLacalFileSaveType, // 文件保存方式
        otherUserSyncType, // 人员导入类型
        otherOperaType, // 操作说明模板类型
        otherLoginType, // 登录类型
        isShowHeadInfo, // 是否显示流程架构的操作说明配置 是否是裕同定制化
        otherVersionType, // 版本类型
        otherEditPort,
        // 流程元素编辑点是否左上进右下出

        /*************** 发布时，相关的未发布文件是否同步发布 相关配置 ************************/
        pubFlowMapRelateRule, // 地图发布：1、制度：发布
        pubFlowMapRelateFile, // 地图发布：2、文件：发布
        pubFlowRelateRule, // 流程发布：1、制度：发布
        pubFlowRelateFile, // 流程发布：1、文件：发布
        pubRuleFile, // 制度模板文件发布：文件：发布

        /**************** 针对能查看流程地图、流程图、制度权限的使用者：可查阅文件的相关文件是否有查看的权限配置 **********************************/
        permissionFlowMapRelateRule, // 流程地图相关文件：1、制度：公开、秘密；（右键菜单关联制度）
        permissionFlowMapRelateFile, // 流程地图相关文件：1、附件：公开、秘密；
        permissionFlowRelateStandard, // 流程图关联文件：标准：公开、秘密
        permissionFlowRelateRule, // 流程图关联文件：制度：公开、秘密
        permissionFlowRelateFile, // 流程图关联文件：文件：公开、秘密
        permissionRuleRelateStandard, // 制度相关文件：标准：1公开、0秘密；
        permissionRuleRelateFile,
        // 制度相关文件：文件：1公开、0秘密；
        a26, // 相关文件
        a27, // 自定义要素1
        a28, // 自定义要素2
        a29, // 自定义要素3
        a30, // 自定义要素4
        a31, // 自定义要素5
        downLoadCountIsLimited, // 下载次数现在复选框 0：无限制，1：有限制
        downLoadCountValue, // 下载次数
        showKPIProperty, // 显示指标来源 支撑的一级指标 相关度：选中启动；不选中不启动
        allowSupplierKPIData, // 允许数据录入者提供数据：选中启动；不选中不启动
        shouOrgPosName, // 流程图角色关联岗位时其显示部门名称与岗位名称：选中启动；不选中不启动
        saveRecovery, // 保存恢复数据间隔时间：选中启动；不选中不启动
        saveRecoveryValue, // 自动存档时间：分钟数
        allowSamePosName, // 同一部门下允许岗位名称相同：选中启动；不选中不启动
        proposalTaskTip, // 提醒时间间隔(日)：选中启用，不选中不启用
        proposalTaskTipValue, // 提醒时间间隔(日)
        basicRuleResPeople, // 制度责任人类型：0：人员；1：岗位
        showPosNameBox, // 显示岗位名称：选中启用，不选中不启用
        showActInOutBox, // 显示活动输入、输出
        a32, // 流程文件：输入和输出
        a33, // 流程范围
        a34, // 术语
        a35, // 流程相关文件
        a36, // 流程接口描述
        a37, // 流程标准化文件
        a38, // 流程-支撑IT系统
        a39, // 新版输入
        a40, // 新版输出
        mailPubToAdmin, // 发布给系统管理员发送邮件
        mailPubToFixed, // 发布给固定人员发送邮件
        documentControl, // 文控信息查阅权限
        mailProposeToHandle, // 提交合理化建议给处理人发邮件
        mailProposeToSubmit, // 处理人处理合理化建议给提交人发邮件采纳或者拒绝
        activityFileShow, // 活动输入输出文本显示
        kpiName, // KPI名称
        kpiType, // KPI类型
        kpiUtilName, // KPI单位名称
        kpiTargetValue, // KPI目标值
        kpiTimeAndFrequency, // 数据统计时间频率
        kpiDefined, // KPI定义
        kpiDesignFormulas, // 流程KPI计算公式
        kpiDataProvider, // 数据提供者
        kpiDataAccess, // 数据获取方式
        kpiIdSystem, // IT系统
        kpiSourceIndex, // 指标来源
        kpiRrelevancy, // 相关度
        kpiSupportLevelIndicator, // 支撑的一级指标
        kpiPurpose, // 设置目的
        kpiPoint, // 测量点
        kpiPeriod, // 统计周期
        kpiExplain, // 说明
        processShowNumber, // 流程、架构的树节点的流程编号是否显示
        isShowDateActivity, // 活动明细是否显示时间选项
        isCreateFlowStandard, // 是否允许创建流程标准
        isShowDateFlowFile, // 流程文件时间驱动是否显示日期选择项
        // ********其他配置********//

        processAccessConfiguration, // 流程体系权限配置（根据角色权限显示流程体系内容）
        isPublic, // 数据库公开名称：默认显示公开
        isSecret, // 数据库秘密名称：默认显示秘密
        projectManagement, // 显示项目管理
        isShowDirList, // 制度、风险、标准、文件：浏览端双击目录默认显示清单，默认显示
        riskSysName, // 风险提醒名称
        showProcessAccessConfig, // 是否启用流程图角色权限显示
        showSecondAdminAccessConfig, // 是否启用二级管理员权限
        mailBoxOuterIsUseTLS, // 外网是否启用tls
        mailBoxInnerIsUseTLS, // 内网是否启用tls
        flowFileDownMenuShow, // 流程文件下载菜单是否显示
        canOpenDesktopPane, // 是否可以打开画图面板,默认是不可以多个人访问同一面板

        activeCodeSelect, // 活动标号
        vhFlowSelect, // 横向、纵向流程选择
        sameNumberInDottedRect, // 协作框内的活动自动编号时是否保持一致

        /** 在线预览配置 */
        enableOnlineView, // 是否启用在线预览（0：启用 1：不启用）
        enableOnlineViewDownload, // 在线预览是否启用文件下载（0：启用 1：不启用）

        showLineHasNoFigure, // 连接线是否可不关联元素存在（：启用1：不启用）
        isUsedActiveFigureAR, // 石勘院新增元素：新建流程应用活动是否改用新增活动（：启用1：不启用）',0)
        isHiddenSystem, // 是否隐藏制度（：启用1：不启用）
        isHiddenStandard, // 是否隐藏制度（：启用1：不启用）
        isHiddenRisk, // 是否隐藏制度（：启用1：不启用）

        /** 自动编号 流程、文件、制度 */
        auto_companyCode, // 公司代码
        auto_productionLine, // 生产线或小部门
        auto_fileType, // 文件类型
        auto_smallClass, // 小类别
        process_level, // 流程-层级

        file_auto_companyCode, // 公司代码
        file_auto_productionLine, // 生产线或小部门
        file_auto_fileType, // 文件类型
        file_auto_smallClass, // 小类别

        rule_auto_companyCode, // 公司代码
        rule_auto_productionLine, // 生产线或小部门
        rule_auto_fileType, // 文件类型
        rule_auto_smallClass, // 小类别
        /** 自动编号 */

        supportTools, // 支持工具
        enclosure, // 附件
        validTill, // 有效期
        readerFlowCustomer, // 是否从流程图客户图符中读取
        ruleType, // 制度类别
        activityType, // 活动类别
        isShowProcessDownLoad, // 流程下载权限
        activityNumIsShow, // 活动编号类别是否隐藏
        isShowFileDownLoad, // 文件下载权限
        isShowRuleDownLoad, // 制度下载权限
        isShowAutoCode, // 是否显示自动编号
        isShowProcessMode, // 是否显示流程模板
        isShowDes, // 是否显示流程架构描述
        isAdminEditProcessMap, // 只允许系统管理员编辑流程架构
        mailFlowDriverTip, // 流程驱动规则提前给相关人员发送提醒
        mailFlowDriverProcessFileTip, // 流程驱动规则提前给流程文件中的提醒人员发送提醒
        mailFavoriteUpdate, // 文件更新给收藏人员发送邮件
        isAllowedPubReName, // 重命名后直接发布
        isArchitectureUploadFile, // 流程架构上传文件
        isRuleUploadFile, // 制度上传文件
        isEbableNewApp, // 是否启用新版服务
        // cookie地址
        securityLevel,
        // 流程保密级别是否显示
        // 制度业务范围
        ruleBusinessScope,
        // 制度其它范围
        ruleOtherScope,
        // 制度组织范围
        ruleOrgScope,
        // 制度监护人
        ruleGuardianPeople,
        // 制度有效期默认值
        ruleValidDefaultValue, isShowRuleValidity, // 制度有效期是否可以编辑
        // 有效期是否永久 0：是。1：否
        ruleValidForever, mailRuleGuardianPeople, // 制度监护人邮件
        mailRuleRefRationPeople, // 制度责任人邮件
        mailRuleSystemManager, messageRuleGuardianPeople, messageRuleRefRationPeople, messageRuleSystemManager,

        onlySyncUser,
        // 人员同步只同步人员
        rulePurpose, // 目的
        ruleApplicability, // 适用范围
        ruleRriteDept, // 文件编写部门
        ruleDraftMan, // 起草人
        ruleDraftingDept, // 起草单位
        ruleChangeVersionExplain, // 制度换版说明
        ruleTestRunFile, // 试行版文件
        ruleCode, // 制度编号
        ruleResPeople, // 制度责任人
        ruleResDept,
        // 制度责任部门
        ruleScurityLevel, // 制度保密级别
        fileScurityLevel, // 文件保密级别

        /********* 【制度任务显示项新增属性】 ********/
        ruleTaskPurpose, // 目的
        ruleTaskApplicability, // 适用范围
        ruleTaskType, // 类别
        ruleTaskSecurityLevel,
        /********* 【制度任务显示项新增属性】 ********/
        // 保密级别
        showProcessFile, // 是否显示流程（文件）
        showApplicabilitySeleteDept, // 流程适用范围-部门选择
        showHiddenImplDate, // 是否显示实施日期
        isOfficeTransformFile, // 是否启用office转换文件(xmlword转标准的office word)
        isAppRequest, // 手机app 请求
        isHiddenSelectTree, // 选择框隐藏tree面板
        isShowActiveTimeValue, // 流程文件-活动明细是否显示办理时限
        activeTimeLineUtil,
        // 活动办理时限单位
        isShowRiskType,
        // 角色、职责显示岗位的类型（岗位组、岗位）
        commissioner, // 合理化建议处理人（专员）
        personLiable, // 合理化建议处理人（责任人）

        /** 废止相关文件 **/
        ruleAbolish, processAbolish, processMapAbolish,

        syncPostionNotExistsDept // 人员同步，可直接同步岗位（不存在登录人和部门）

        , isShowProcessCommissioner// 显示流程专员
        , isShowProcessMapCommissioner// 显示架构专员
        , isShowFileCommissioner// 文件专员
        , isShowRuleCommissioner// 制度专员
        , isShowEffectiveDate// 生效日期
        , isShowFileKeyWord// 文件关键字
        , isShowRuleKeyWord// 制度关键字
        , isShowProcessKeyWord// 流程关键字
        , isShowProcessMapKeyWord
        // 架构关键字
        , isShowFileResPeople// 是否显示文件责任人
        , taskDefaultPeoplesOrder
        // 任务默认审批人显示顺序 (1、专员、责任人、部门领导;2、模板默认人员;3、本地文件存储)
        , isStandardizedAutoCode, // 标准通用自动编号 默认0 不启用
        processFileShowPositionType,
        // 角色、职责显示岗位的类型（岗位组、岗位）
        basicFileResPeople, // 文件责任人类型

        isShowProcessMapNum, // 流程架构编号
        isShowProcessMapDescription, // 流程架构描述
        isShowProcessMapPurpose, // 流程架构目的
        isShowProcessMapResPeople, // 流程架构责任人
        isShowProcessMapResponsibleDepartment, // 流程架构责任部门.
        isShowProcessMapLastFlow, // 流程架构上一层流程
        isShowProcessMapNextFlow, // 流程架构下一层流程
        isShowProcessMapFlowInputInfo, // 流程架构输入信息
        isShowProcessMapFlowOutputInfo, // 流程架构输出信息
        isShowProcessMapFlowStart, // 流程起始
        isShowProcessMapFlowEnd, // 流程终止
        isShowProcessMapFlowKPI, // 流程架构KPI
        isShowProcessMapOwnerType // 流程架构责任人类型
        , isShowProcessMapSupplier// 供应商
        , isShowProcessMapCustomer // 客户
        , isShowProcessMapRiskAndOpportunity// 风险与机会
        , newAerialMap // 新建鸟瞰图（1显示，0隐藏）
        , replaceAllElementAttribute// 全局替换元素属性
        , isUpdateRoleAlias// 是否更新角色别名
        , processFileActivityShowIT// 流程文件活动中是否显示IT系统
        , uploadRuleFileType
        // 上传的制度文件类型
        , processShowBussType
        // 流程业务类型
        , isShowActivtyTextInOutIcon// 活动文本输入输出图标显示（：隐藏：0;显示：1）
        , mailExpiryTip, // 启用有效期提醒
        mailExpiryAdvanceTip, // 有效期提前多少天
        mailExpiryFrequencyTip,
        // 有效期提醒频率
        isShowProcessCustomer, // 客户
        isShowAccessButton, // 是否显示 查阅权限按钮置灰
        useNewInout,
        // 使用新版本的输入输出
        isOpenSyncFileFromOrg1, isOpenSyncFileFromOrg,
        // 是否开启文件目录，按照组织结构同步
        cookieTime, // cookie有效时间
        permissionisParentNode, // 是否继承父目录权限
        isShowBeginningEndElement, // 是否显示开始结束元素
        autoVersionNumber, // 是否启用自动版本号
        initVersionNumber, // 初始化版本号
        enableSmallVersion, // 是否启用(自动版本号)小版本
        versionNumberId // 初始化版本号前缀
        , expiryRecursion,
        // 有效维护部门搜索是否递归搜索
        editAuthProcessRelease, // 流程编辑权限设置(发布)
        editAuthProcessNotRelease, // 流程编辑权限设置(未发布)
        editAuthProcessMapRelease, // 流程架构编辑权限设置(发布)
        editAuthProcessMapNotRelease, // 流程架构编辑权限设置(未发布)
        isShowProcessAccess, // 是否有流程图查看权限
        mailBoxInnerAlias, mailBoxOuterAlias, processMapModelUpperLimit, // 架构模型新建上限
        processModelUpperLimit, // 流程模型新建上限
        isShowFileNum, // 文件编号是否显示
        mailPlatName,
        // 邮件中显示的平台名称
        isShowGrid, // 默认是否显示流程图背景网格线
        elementLocation,
        // 元素链接显示位置
        ruleFictionPeople,
        isShowTimeLimit //浏览端是否显示办理时限时间轴
    }

    public enum ButtonCommendEnum {
        addOKBtn, // 加入按钮弹出对话框中确认按钮
        addCancelBtn, // 加入按钮弹出对话框中取消按钮
    }

    // 流程文件附件类型
    public enum EXCESS {
        FILE_ACT("活动附件"), FILE_RULE("制度附件"), FILE_STAND("标准附件"), FILE_STANDIZED("相关标准化文件");
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        private EXCESS(String value) {
            this.value = value;
        }
    }
}
