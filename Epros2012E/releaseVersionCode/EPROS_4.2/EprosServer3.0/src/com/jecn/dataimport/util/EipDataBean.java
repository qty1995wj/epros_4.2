package com.jecn.dataimport.util;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 聚光科技,待接办中心需要数据
 * 
 */
public class EipDataBean implements Serializable {
	/** 流程编号 */
	private Long flowId;// 
	/** 流程名称 */
	private String flowName;
	/** 流程发起人 */
	private String createPerson;
	/** 状态 */
	private String state;
	/** 任务名称 */
	private String taskName;
	/** 到达时间 */
	private Date startTime;
	/** 要求完成时间 */
	private Date endTime;
	/** 任务类型 0：流程任务，1文件任务，2：制度任务 */
	private String taskType;

	/**
	 * 
	 * @return 流程编号
	 */
	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	/**
	 * 
	 * @return 流程名称
	 */
	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	/**
	 * 
	 * @return 流程发起人
	 */
	public String getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}

	/**
	 * 
	 * @return 状态
	 */
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 
	 * @return 任务名称
	 */
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * 
	 * @return 到达时间
	 */
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * 
	 * @return 要求完成时间
	 */
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
}
