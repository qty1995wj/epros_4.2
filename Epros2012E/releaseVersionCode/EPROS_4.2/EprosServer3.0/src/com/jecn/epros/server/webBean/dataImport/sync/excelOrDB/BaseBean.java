package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.dataimport.BasePosBean;
import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

public class BaseBean {

	/** 部门 */
	private List<DeptBean> deptBeanList = null;
	/** 岗位 */
	private List<PosBean> posBeanList = new ArrayList<PosBean>();
	/** 人员 */
	private List<UserBean> userBeanList = new ArrayList<UserBean>();
	/**
	 * 视图中获取的基准岗位集合及基准岗位与岗位关系集合
	 * */
	private List<BasePosBean> basePosBean = new ArrayList<BasePosBean>();

	/** 拆分后的基准岗位集合 **/
	private List<JecnBasePosBean> jecnBasePosBeanList = new ArrayList<JecnBasePosBean>();

	/** 岗位组 */
	private List<JecnPosGroup> posGroups = new ArrayList<JecnPosGroup>();
	/** 岗位组关联岗位 */
	private List<JecnPosGroupRelatedPos> groupRelatedPos = new ArrayList<JecnPosGroupRelatedPos>();
	/**
	 * 工号和工号对应的域账号 由于当前Epros中的longinName用的是工号，部分公司需要将工号替换成域账号
	 * 所以需要此集合，存放工号和工号对应的域账号
	 * */
	// private List<UserDomainNameBean> userDomainNameBeans=new
	// ArrayList<UserDomainNameBean>();

	/** 不同步的人员登录名称集合(该集合记录相关人员不更新，相关岗位不更新) */
	public static List<String> syncNotUpdateUsers = new ArrayList<String>();
	/** 原始的人员+岗位在一起数据 */
	private List<UserBean> userPosBeanList = null;
	/** 手动或自动更新标识:手动：0；自动：1 */
	private String isAuto;

	public BaseBean(List<DeptBean> deptBeanList, List<UserBean> userPosBeanList) {

		// 部门
		if (deptBeanList == null) {
			this.deptBeanList = new ArrayList<DeptBean>();
		} else {
			this.deptBeanList = deptBeanList;
		}
		// 人员
		if (userPosBeanList == null) {
			this.userPosBeanList = new ArrayList<UserBean>();
		} else {
			this.userPosBeanList = userPosBeanList;
		}
	}

	public BaseBean(List<DeptBean> deptBeanList, List<UserBean> userPosBeanList, List<JecnPosGroup> posGroups,
			List<JecnPosGroupRelatedPos> groupRelatedPos) {
		// 部门
		if (deptBeanList == null) {
			this.deptBeanList = new ArrayList<DeptBean>();
		} else {
			this.deptBeanList = deptBeanList;
		}
		// 人员
		if (userPosBeanList == null) {
			this.userPosBeanList = new ArrayList<UserBean>();
		} else {
			this.userPosBeanList = userPosBeanList;
		}
		// 岗位组
		if (posGroups == null) {
			this.posGroups = new ArrayList<JecnPosGroup>();
		} else {
			this.posGroups = posGroups;
		}
		if (groupRelatedPos == null) {
			this.groupRelatedPos = new ArrayList<JecnPosGroupRelatedPos>();
		} else {
			this.groupRelatedPos = groupRelatedPos;
		}
	}

	public BaseBean(List<DeptBean> deptBeanList, List<UserBean> userPosBeanList, List<BasePosBean> basePosBeanList,
			BasePosBean basePosBean) {
		// 部门
		if (deptBeanList == null) {
			this.deptBeanList = new ArrayList<DeptBean>();
		} else {
			this.deptBeanList = deptBeanList;
		}
		// 人员
		if (userPosBeanList == null) {
			this.userPosBeanList = new ArrayList<UserBean>();
		} else {
			this.userPosBeanList = userPosBeanList;
		}
		// 基准岗位与岗位关系集合
		if (basePosBeanList == null) {
			this.basePosBean = new ArrayList<BasePosBean>();
		} else {
			this.basePosBean = basePosBeanList;
		}

	}

	public BaseBean(List<DeptBean> deptBeanList, List<UserBean> userPosBeanList,
			List<UserDomainNameBean> userDomainNameBeans) {

		// 部门
		if (deptBeanList == null) {
			this.deptBeanList = new ArrayList<DeptBean>();
		} else {
			this.deptBeanList = deptBeanList;
		}
		// 人员
		if (userPosBeanList == null) {
			this.userPosBeanList = new ArrayList<UserBean>();
		} else {
			this.userPosBeanList = userPosBeanList;
		}
		// //工号和工号所对应的域账号
		// if(userDomainNameBeans!=null){
		// this.
		// }
	}

	/**
	 * 
	 * 分解人员和岗位数据
	 * 
	 * @param userPosSheet
	 */
	public void resolveUserPos() {

		if (userPosBeanList == null) {
			return;
		}

		// 记录已经存储过的岗位
		Set<String> posNumSet = new HashSet<String>();

		for (int i = 0; i < userPosBeanList.size(); i++) {
			// 获取原始人员+岗位数据
			UserBean userPosBean = userPosBeanList.get(i);
			// 岗位
			PosBean posBean = new PosBean();
			// 人员
			UserBean userBean = new UserBean();

			// 任职岗位编号
			String posNum = userPosBean.getPosNum();
			// 登录名称
			String userNum = userPosBean.getUserNum();

			// 任职岗位编号
			posBean.setPosNum(SyncTool.emtryToNull(posNum));
			// 任职岗位名称
			posBean.setPosName(SyncTool.emtryToNull(userPosBean.getPosName()));
			// 岗位所属部门编号
			posBean.setDeptNum(SyncTool.emtryToNull(userPosBean.getDeptNum()));
			// 存储拆分前的数据，为了记录错误信息
			posBean.setUserPosBean(userPosBean);
			// 登录名称
			userBean.setUserNum(SyncTool.emtryToNull(userNum));
			// 真实姓名
			userBean.setTrueName(SyncTool.emtryToNull(userPosBean.getTrueName()));
			// 人员所在岗位编号
			userBean.setPosNum(SyncTool.emtryToNull(posNum));
			// 邮箱地址
			userBean.setEmail(SyncTool.emtryToNull(userPosBean.getEmail()));

			// 邮箱、邮箱类型不等于空且是“0”，“1”
			if (userPosBean.checkEmailType()) {
				userBean.setEmailType(Integer.valueOf(userPosBean.getEmailType()));
			}
			// 联系电话
			userBean.setPhone(SyncTool.emtryToNull(userPosBean.getPhone()));
			// 存储拆分前的数据，为了记录错误信息
			userBean.setUserPosBean(userPosBean);

			userBean.setDomainName(userPosBean.getDomainName());
			// 判断不用存储标识
			boolean flag = true;
			// 岗位
			// 不为空且已经存储过了就不用存储
			if (!SyncTool.isNullOrEmtryTrim(posNum)) {
				if (posNumSet.contains(posNum.trim())) {
					flag = false;
				}
			} else {
				flag = false;
			}

			if (flag) {
				posBeanList.add(posBean);
				posNumSet.add(posNum);
			}

			// 前提条件：不能出现两条以上岗位编号为空的数据
			// 人员编号相同，岗位也相同情况不用添加
			// if (isUserBeanAddDB(userBeanSet, userBean)) {
			userBeanList.add(userBean);
			// userBeanSet.add(userBean);
			// }
		}
	}

	/**
	 * 前提条件：人员编号不能为空，一人多岗时岗位编号不能为空
	 * 
	 * 判断人员编号相同，岗位也相同情况下返回false，其他情況返回true
	 * 
	 * @param userBeanSet
	 *            userBeanSet
	 * @param userPosBean
	 *            userPosBean
	 * @return
	 */
	private boolean isUserBeanAddDB(List<UserBean> userBeanSet, UserBean userPosBean) {

		for (UserBean userBean : userBeanSet) {
			// 人员编号相同，岗位也相同
			if (userBean.repeatAttributes(userPosBean)) {
				return false;
			}
		}

		return true;
	}

	public List<DeptBean> getDeptBeanList() {
		return deptBeanList;
	}

	public List<PosBean> getPosBeanList() {
		return posBeanList;
	}

	public List<UserBean> getUserBeanList() {
		return userBeanList;
	}

	public List<UserBean> getUserPosBeanList() {
		return userPosBeanList;
	}

	/**
	 * 
	 * 添加部门记录
	 * 
	 * @param deptBean
	 */
	public void addDeptBeanList(DeptBean deptBean) {
		if (deptBean != null) {
			this.deptBeanList.add(deptBean);
		}
	}

	/**
	 * 
	 * 添加岗位记录
	 * 
	 * @param posBean
	 */
	public void addPosBeanList(PosBean posBean) {
		if (posBean != null) {
			this.posBeanList.add(posBean);
		}
	}

	/**
	 * 
	 * 添加人员记录
	 * 
	 * @param userBean
	 */
	public void addUserBeanList(UserBean userBean) {
		if (userBean != null) {
			this.userBeanList.add(userBean);
		}
	}

	/**
	 * 
	 * @return 部门、岗位以及人员集合其中一个为空返回true，否则返回false
	 */
	public boolean isNull() {
		return (deptBeanList == null || posBeanList == null || userBeanList == null || userPosBeanList == null) ? true
				: false;
	}

	/**
	 * 
	 * 判断岗位的所属部门编号是否在部门集合中存在
	 * 
	 * @param posBean
	 * @return boolean 存在返回true，不存在返回false
	 * 
	 */
	public boolean existsPosByDept(PosBean posBean) {
		boolean ret = false;

		if (!isNull()) {
			for (DeptBean deptBean : deptBeanList) {
				if (posBean.getDeptNum().equals(deptBean.getDeptNum())) {
					ret = true;
					break;
				}
			}
		}
		return ret;
	}

	/**
	 * 
	 * 判断岗位的所属部门编号是否在部门集合中存在
	 * 
	 * @param deptNum
	 * @return boolean 存在返回true，不存在返回false
	 * 
	 */
	public boolean existsPosByDept(String deptNum) {
		boolean ret = false;
		if (SyncTool.isNullOrEmtryTrim(deptNum)) {
			return ret;
		}
		if (!isNull()) {
			for (DeptBean deptBean : deptBeanList) {
				if (deptNum.trim().equals(deptBean.getDeptNum())) {
					ret = true;
					break;
				}
			}
		}
		return ret;
	}

	/**
	 * 
	 * 判断人员的对应岗位编号是否在岗位集合中存在
	 * 
	 * @param userBean
	 * @return
	 */
	public boolean existsUserByPos(UserBean userBean) {
		boolean ret = false;
		if (!isNull()) {
			for (PosBean posBean : posBeanList) {
				if (userBean.getPosNum().equals(posBean.getPosNum())) {
					ret = true;
				}
			}
		}
		return ret;
	}

	public List<BasePosBean> getBasePosBean() {
		return basePosBean;
	}

	public void setBasePosBean(List<BasePosBean> basePosBean) {
		this.basePosBean = basePosBean;
	}

	public List<JecnBasePosBean> getJecnBasePosBeanList() {
		return jecnBasePosBeanList;
	}

	public void setJecnBasePosBeanList(List<JecnBasePosBean> jecnBasePosBeanList) {
		this.jecnBasePosBeanList = jecnBasePosBeanList;
	}

	public List<JecnPosGroup> getPosGroups() {
		return posGroups;
	}

	public void setPosGroups(List<JecnPosGroup> posGroups) {
		this.posGroups = posGroups;
	}

	public List<JecnPosGroupRelatedPos> getGroupRelatedPos() {
		return groupRelatedPos;
	}

	public void setGroupRelatedPos(List<JecnPosGroupRelatedPos> groupRelatedPos) {
		this.groupRelatedPos = groupRelatedPos;
	}

	public String getIsAuto() {
		return isAuto;
	}

	public void setIsAuto(String isAuto) {
		this.isAuto = isAuto;
	}
}
