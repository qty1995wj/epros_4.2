package com.jecn.epros.server.action.web.login.ad.cms;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import EIAC.EAC.SSO.PSOConfig;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnCommon;

/**
 * 招商证券
 * 
 * @author ZXH
 * @date 2018-03-26
 * 
 */
public class CMSLoginAction extends JecnAbstractADLoginAction {

	public CMSLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		HttpServletRequest request = this.loginAction.getRequest();
		HttpServletResponse response = this.loginAction.getResponse();
		// request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		// 发送到EAC验证
		String posturl = "";
		String defaulturl = request.getRequestURL().toString();
		EIAC.EAC.SSO.AppSSOBLL app = new EIAC.EAC.SSO.AppSSOBLL();
		String IASID = request.getParameter("IASID");
		String TimeStamp = request.getParameter("TimeStamp");
		// String ReturnURL = request.getParameter("ReturnURL");
		String UserAccount = request.getParameter("UserAccount");
		String ErrorDescription = request.getParameter("ErrorDescription");
		String Authenticator = request.getParameter("Authenticator");
		// String charectstr=request.getCharacterEncoding();

		try {
			// 可以读配置文件
			if (StringUtils.isBlank(UserAccount)) {// 首次登陆
				IASID = PSOConfig.GetIASID();
				// 取时间
				TimeStamp = JecnCommon.getStringbyDate(new Date(), "yyyy-MM-dd HH:mm:ss");
				posturl = app.PostString(IASID, TimeStamp, defaulturl, null);
				log.info(posturl);
				// 输出html到浏览器，自动执行请求
				loginAction.outString(posturl);
				return null;
			}

			// 接收从EAC返回的
			if (StringUtils.isNotBlank(UserAccount)) {
				String Result = request.getParameter("Result");
				if (!"0".equals(Result)) {
					log.error("Result验证不成功！");
					log.error(IASID + ":" + TimeStamp + ":" + UserAccount + ":" + Result + ":" + ErrorDescription + ":"
							+ Authenticator);
					return null;
				}

				if (app.ValidateFromEAC(IASID, TimeStamp, UserAccount, Result, ErrorDescription, Authenticator)) {
					return this.loginByLoginName(UserAccount);
				} else {
					log.info("验证错误");
					log.info(IASID + ":" + TimeStamp + ":" + UserAccount + ":" + Result + ":" + ErrorDescription + ":"
							+ Authenticator);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
