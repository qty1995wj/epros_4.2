package epros.draw.gui.workflow.dialog;

import java.awt.Dimension;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.toolbar.element.JecnFlowElementAttributesPanel;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 右键菜单点击流程元素菜单项弹出对话框
 * 
 * 设置流程元素的属性值
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementAttrDialog extends JecnDialog {

	/** 流程元素面板 */
	private JecnFlowElementAttributesPanel flowElementAttributesPanel = null;
	/** 流程元素对象 */
	private JecnBaseFlowElementPanel elementPanel = null;

	public JecnFlowElementAttrDialog(JecnBaseFlowElementPanel elementPanel) {

		this.elementPanel = elementPanel;

		intComponents();
	}

	private void intComponents() {
		// 元素属性面板
		flowElementAttributesPanel = new JecnFlowElementAttributesPanel(this, elementPanel, false);

		// 大小
		Dimension size = new Dimension(700, 545);
		this.setSize(size);
		this.setMinimumSize(size);
		this.setPreferredSize(size);
		this.setModal(true);
		// this.setResizable(false);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);

		// 居中
		this.setLocationRelativeTo(null);

		// 标题
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.flowElemAttr));

		this.getContentPane().add(flowElementAttributesPanel);
	}

	/**
	 * 
	 * 此方法必须有，保证JecnDialog释放资源是能成功释放
	 * 
	 */
	public void setVisible(boolean b) {
		super.setVisible(b);
		flowElementAttributesPanel = null;
		elementPanel = null;
	}
}
