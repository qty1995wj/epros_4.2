package com.jecn.epros.server.download.wordxml.sailun;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 赛轮操作说明下载
 * 
 * @author admin
 * 
 */
public class SLProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);

	public SLProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(17.5F);
		getDocProperty().setFlowChartScaleValue(4.5F);
		setDocStyle(" ", textTitleStyle());
	}

	/**
	 * 流程图横向
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0.1F, 0.1F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 活动说明多一列说动时间估算
	 */
	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		if (rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 添加标题
		rowData.add(0, titles);

		List<String[]> newRowData = new ArrayList<String[]>();
		String[] tempStr = null;
		int rowNum = 0;
		for (String[] str : rowData) {
			tempStr = new String[str.length + 1];
			for (int i = 0; i < str.length; i++) {
				tempStr[i] = str[i];
			}
			tempStr[str.length] = "";
			if (rowNum == 0) {
				tempStr[str.length] = "活动时间估算";
			}
			rowNum++;
			newRowData.add(tempStr);
		}

		float[] width = new float[] { 2.36F, 2.36F, 2.36F, 2.36F, 2.36F, 2.36F, 2.36F };
		Table tab = createTableItem(processFileItem, width, newRowData);
		tab.setRowStyle(0, tblTitleStyle());

	}

	@Override
	protected void initFirstSect(Sect sect) {
		SectBean sectBean = getSectBean();
		sect.setSectBean(sectBean);
		// 固定值23磅
		ParagraphLineRule fixVline = new ParagraphLineRule(23F, LineRuleType.EXACT);
		String flowName = processDownloadBean.getFlowName();
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		pBean.setSpace(2f, 2f, fixVline);
		sect.createParagraph(flowName, pBean);
		createCommhdr(sect);
		createCommftr(sect);

	}

	@Override
	protected void initFlowChartSect(Sect sect) {
		SectBean sectBean = getSectBean();
		sectBean.setSize(29.7F, 21F);
		sectBean.setPage(1F, 2.5F, 2F, 2F, 1F, 1F);
		sect.setSectBean(sectBean);
		createCommhdr(sect);
		createCommftr(sect);

	}

	@Override
	protected void initSecondSect(Sect sect) {
		SectBean sectBean = getSectBean();
		sect.setSectBean(sectBean);
		createCommhdr(sect);
		createCommftr(sect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(1F, 2.5F, 2F, 1.5F, 0F, 0F);
		titleSect.setSectBean(sectBean);
		addTitleSectContent(titleSect);

	}

	private void addTitleSectContent(Sect titleSect) {
		// 宋体 四号
		ParagraphBean stshBean = new ParagraphBean(Align.center, "宋体", Constants.sihao, false);
		// 宋体 五号
		ParagraphBean stwhBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		for (int i = 0; i < 3; i++) {
			titleSect.createParagraph("");
		}
		// Times New Roman 48 号
		ParagraphBean gcBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		titleSect.createParagraph("GC", gcBean);

		// 公司名称文件
		// 黑体 小一
		ParagraphBean htxyBean = new ParagraphBean(Align.distribute, "黑体", Constants.xiaoyi, false);
		titleSect.createParagraph(processDownloadBean.getCompanyName() + "文件", htxyBean);
		// 流程编号
		// Times New Roman 4 号
		ParagraphBean bhBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sihao, false);
		titleSect.createParagraph(processDownloadBean.getFlowInputNum(), bhBean);
		titleSect.createParagraph("");
		LineGraph lineGraph = new LineGraph(LineStyle.single, 0.1F);
		lineGraph.setFrom(-12.3F, 69.75F);
		lineGraph.setTo(479.9F, 69.75F);
		titleSect.createParagraph(lineGraph);

		for (int i = 0; i < 7; i++) {
			titleSect.createParagraph("");
		}
		// 流程名称
		// 黑体 一号
		ParagraphBean htyhBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		titleSect.createParagraph(processDownloadBean.getFlowName(), htyhBean);
		for (int i = 0; i < 3; i++) {
			titleSect.createParagraph("", htyhBean);
		}
		int num = 0;
		if (processDownloadBean.getDocumentList() != null) {
			num += processDownloadBean.getDocumentList().size();
		}
		String version = processDownloadBean.getFlowVersion() == "" ? "  " : processDownloadBean.getFlowVersion();
		// 版本号
		String flowVersion = "第" + version + "版" + "  修订" + num + "次";
		titleSect.createParagraph(flowVersion, stshBean);
		titleSect.createParagraph("分发号：01", stwhBean);

		for (int i = 0; i < 13; i++) {
			titleSect.createParagraph("", htyhBean);
		}

		// 发布时间
		String releaseDateStr = processDownloadBean.getReleaseDate();
		String effectiveStr = "";
		if (StringUtils.isNotEmpty(releaseDateStr)) {
			Date release = JecnUtil.StrToDate(releaseDateStr);
			// 实施时间 发布时间+15
			Date effective = JecnUtil.addDay(release, 15);
			releaseDateStr = JecnUtil.DateToStr(release, "yyyy-MM-dd") + "发布";
			effectiveStr = JecnUtil.DateToStr(effective, "yyyy-MM-dd") + "实施";
		} else {
			releaseDateStr = "未发布";
			effectiveStr = "未实施";
		}
		List<String[]> rowData = new ArrayList<String[]>();
		String[] str = new String[] { releaseDateStr, effectiveStr };
		rowData.add(str);
		// 黑体 四号
		ParagraphBean tabLPBean = new ParagraphBean("黑体", Constants.sihao, false);

		ParagraphBean tabRPBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		TableBean tblBean = new TableBean();

		Table tab = JecnWordUtil.createTab(titleSect, tblBean, new float[] { 8.5F, 8.5F }, rowData, tabLPBean);
		tab.getRow(0).getCell(1).getParagraph(0).setParagraphBean(tabRPBean);

		lineGraph = new LineGraph(LineStyle.single, 0.1F);
		lineGraph.setFrom(-12.3F, 69.75F);
		lineGraph.setTo(480.9F, 69.75F);
		titleSect.createParagraph(lineGraph);
		// 发布
		// 黑体 四号 居中
		ParagraphBean htshBean = new ParagraphBean(Align.center, "黑体", Constants.sihao, false);
		String comPubTitle = "  发  布";
		if (StringUtils.isNotEmpty(releaseDateStr)) {
			comPubTitle = "";
		}
		titleSect.createParagraph(processDownloadBean.getCompanyName() + comPubTitle, htshBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(DocGrid.getInstance());
		// 设置页边距
		sectBean.setPage(1F, 2.5F, 2F, 2F, 2.5F, 2F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.right, "黑体", Constants.wuhao, false);
		//pBean.setpStyle(PStyle.AF1);
		pBean.setSpace(0f, 0f, vLine1);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		header.createParagraph(processDownloadBean.getFlowInputNum(), pBean);
	}

	/**
	 * 创建通用页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.right, "宋体", Constants.xiaowu, false);
		pBean.setSpace(0f, 0f, vLine1);
		Footer footer = sect.createFooter(HeaderOrFooterType.odd);
		Paragraph p = footer.createParagraph("", pBean);
		p.appendCurPageRun();
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.01 厘米
		tblStyle.setTableLeftInd(0.01F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0F, 0, 0F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitleStyle.setSpace(0f, 0f, vLine1);
		return tblTitleStyle;
	}

	// 宋体 五号 首行2字符
	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		textContentStyle.setInd(0.1F, 0, 0F, 0.7F);
		return textContentStyle;
	}

	// 黑体 五号 单倍行距
	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("黑体", Constants.wuhao, false);
		textTitleStyle.setSpace(1F, 1F, vLine1);
		// 悬挂缩进默认0.74 会导致序号 超过10了增加两倍故设置0.9
		textTitleStyle.setInd(0F, 0, 0.9F, 0);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
