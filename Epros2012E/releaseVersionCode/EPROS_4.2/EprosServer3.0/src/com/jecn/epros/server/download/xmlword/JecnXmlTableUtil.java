package com.jecn.epros.server.download.xmlword;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

/**
 * 操作xml word table类
 * 
 * @author fuzhh 2013-6-3
 * 
 */
public class JecnXmlTableUtil {
	/**
	 * 获取table节点
	 * 
	 * @author fuzhh 2013-5-24
	 * @param sectionEle
	 *            内容正文节点
	 * @param logo
	 *            标识
	 * @return
	 */
	public static Element getTableNode(Element sectionEle, String logo) {
		// 存储竖向Table的List
		List<Element> tblEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(sectionEle, tblEleList, "tbl");
		// 存储竖向标题样式
		Element titleEle = XmlDownUtil.getSelectEle(tblEleList, logo);
		return titleEle;
	}

	/**
	 * 获取table节点中的tr节点
	 * 
	 * @author fuzhh 2013-5-24
	 * @param sectionEle内容正文节点
	 * @param tableEle
	 *            table节点
	 * @param logo
	 *            标识
	 * @return
	 */
	public static Element getTableTrNode(Element tableEle, String logo) {
		// 查找table 中的 tr节点
		List<Element> tblTrEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(tableEle, tblTrEleList, "tr");
		// 存储tr节点
		Element trEle = XmlDownUtil.getSelectEle(tblTrEleList, logo);
		return trEle;
	}

	/**
	 * 获取tr中的tc节点
	 * 
	 * @author fuzhh 2013-5-27
	 * @param trEle
	 *            tr节点
	 * @param logo
	 *            标识
	 * @return
	 */
	public static Element getTableTcNode(Element trEle, String logo) {
		// 查找tr 中的 tc节点
		List<Element> tablTcEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(trEle, tablTcEleList, "tc");
		// 存储样式的节点
		Element tcTitleEle = XmlDownUtil.getSelectEle(tablTcEleList, logo);
		return tcTitleEle;
	}

	/**
	 * 修改Tc节点下宽度
	 * 
	 * @author fuzhh 2013-5-27
	 * @param tcPrEle
	 *            tc节点
	 * @param widthNum
	 *            宽度
	 */
	public static void updateTcWidth(Element tcPrEle, int widthNum) {
		// 找到tcPr下的tcW 节点
		List<Element> tcWPrEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(tcPrEle, tcWPrEleList, "tcW");
		Element tcwNode = null;
		if (tcWPrEleList.size() == 0) {
			tcwNode = tcPrEle.addElement("w:tcW");
		} else if (tcWPrEleList.size() == 1) {
			tcwNode = tcWPrEleList.get(0);
		} else {
			for (int j = 0; j < tcWPrEleList.size(); j++) {
				Element node = tcWPrEleList.get(j);
				if (j == 0) {
					tcwNode = node;
				} else {
					XmlDownUtil.deleteNode(tcwNode);
				}
			}
		}
		XmlDownUtil.updateNodeAttribute(tcwNode, "w", String.valueOf(widthNum));
	}

	/**
	 * 生成横向table数据(合并行)
	 * 
	 * @author fuzhh 2013-5-27
	 * @param tcEle
	 *            tc节点
	 * @param strNum
	 *            内容集合
	 * @param columnNum
	 *            需要合并的列
	 * @param columnNum
	 *            是否为合并的第一行
	 * @param widthNum
	 *            单元格的宽度
	 * 
	 * 
	 */
	public static void createWideTable(Element tcEle, String[] strNum,
			int columnNum, boolean isOneCount, int widthNum) {
		for (int i = 0; i < strNum.length; i++) {
			// 复制tc 节点
			Element copeTcContentEle = XmlDownUtil.copyNodes(tcEle);
			String strVal = strNum[i];
			// 找到tc下的tcPr 节点
			List<Element> tablTcPrEleList = new ArrayList<Element>();
			XmlDownUtil.getAllChildNodeList(copeTcContentEle, tablTcPrEleList,
					"tcPr");
			if (tablTcPrEleList.size() > 0) {
				Element tcPrEle = tablTcPrEleList.get(0);
				updateTcWidth(tcPrEle, widthNum);
				if (columnNum == i) {
					Element vmerge = tcPrEle.addElement("w:vmerge");
					if (isOneCount) {
						XmlDownUtil.setEleAttrubute(vmerge, "w:val", "restart");
					}
				}
			}
			XmlDownUtil.updateNodeValueByNode(copeTcContentEle, strVal);
		}
	}

	/**
	 * 生成横向table数据
	 * 
	 * @author fuzhh 2013-5-27
	 * @param tcEle
	 *            tc节点
	 * @param strNum
	 *            内容集合
	 */
	public static void createWideTable(Element tcEle, String[] strNum,
			int widthNum) {
		for (String strVal : strNum) {
			// 复制tc节点
			Element copeTcEle = XmlDownUtil.copyNodes(tcEle);
			XmlDownUtil.updateNodeValueByNode(copeTcEle, strVal);
			// 找到tc下的tcPr 节点
			List<Element> tablTcPrEleList = new ArrayList<Element>();
			XmlDownUtil.getAllChildNodeList(copeTcEle, tablTcPrEleList, "tcPr");
			if (tablTcPrEleList.size() > 0) {
				Element tcPrEle = tablTcPrEleList.get(0);
				updateTcWidth(tcPrEle, widthNum);
			}
		}
	}

	/**
	 * 生成竖向table数据
	 * 
	 * @author fuzhh 2013-5-24
	 * @param highTrEle
	 *            tr节点
	 * @param title
	 *            标题
	 * @param content
	 *            内容
	 */
	public static void createHighTable(Element trEle, String title,
			String content) {
		// 查找竖向tr 中的 tc节点
		List<Element> highTblTcEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(trEle, highTblTcEleList, "tc");
		// 存储竖向标题样式的节点
		Element highTcTitleEle = XmlDownUtil.getSelectEle(highTblTcEleList,
				XmlDownUtil.highTabTitleStyle);
		XmlDownUtil.updateNodeValueByNode(highTcTitleEle, title);
		// 存储竖向内容样式的节点
		Element highTcContentEle = XmlDownUtil.getSelectEle(highTblTcEleList,
				XmlDownUtil.highTabContentStyle);
		XmlDownUtil.updateNodeValueByNode(highTcContentEle, content);
	}

	/**
	 * 生成竖向table数据
	 * 
	 * @author fuzhh 2013-5-24
	 * @param highTrEle
	 *            tr节点
	 * @param title
	 *            标题
	 * @param content
	 *            内容
	 */
	public static void createHighTable(Element trEle, String titleOne,
			String titleTwo, String contentOne, String contentTwo) {
		int widthNum = XmlDownUtil.pageWidth / 4;
		// 查找竖向tr 中的 tc节点
		List<Element> highTblTcEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(trEle, highTblTcEleList, "tc");
		// 存储竖向标题样式的节点
		Element highTcTitleEle = XmlDownUtil.getSelectEle(highTblTcEleList,
				XmlDownUtil.highTabTitleStyle);
		// 复制TC节点 驱动类型
		Element copeTcNodeOne = XmlDownUtil.copyNodes(highTcTitleEle);
		XmlDownUtil.updateNodeValueByNode(highTcTitleEle, titleOne);
		XmlDownUtil.updateNodeValueByNode(copeTcNodeOne, contentOne);
		updateTcWidth(highTcTitleEle, widthNum);
		updateTcWidth(copeTcNodeOne, widthNum);

		// 存储竖向内容样式的节点
		Element highTcContentEle = XmlDownUtil.getSelectEle(highTblTcEleList,
				XmlDownUtil.highTabContentStyle);
		// 复制TC节点 驱动类型
		Element copeTcNodeTwo = XmlDownUtil.copyNodes(highTcTitleEle);
		XmlDownUtil.updateNodeValueByNode(highTcContentEle, titleTwo);
		XmlDownUtil.updateNodeValueByNode(copeTcNodeTwo, contentTwo);
		updateTcWidth(highTcContentEle, widthNum);
		updateTcWidth(copeTcNodeTwo, widthNum);
	}

	/**
	 * 复制tr节点生成tr数据
	 * 
	 * @author fuzhh 2013-5-30
	 * @param trEle
	 *            tr节点
	 * @param title
	 *            标题名称
	 * @param content
	 *            内容
	 */
	public static void createHighTableDate(Element trEle, String title,
			String content) {
		// 复制tr节点 驱动类型
		Element copeDriverTypeTrNode = XmlDownUtil.copyNodes(trEle);
		// 设置tr数据
		createHighTable(copeDriverTypeTrNode, title, content);
	}

	/**
	 * 创建横向表格标题列
	 * 
	 * @author fuzhh 2013-5-28
	 * @param copeTableNode
	 *            复制的table节点
	 * @param titleStrNum
	 *            表格数组
	 * @param widthNum
	 *            宽度
	 */
	public static void createWideTableTitle(Element copeTableNode,
			String[] titleStrNum, int widthNum) {
		// 获取横向Table的tr标题节点
		Element trEle = getTableTrNode(copeTableNode,
				XmlDownUtil.wideTableTitle);
		if (trEle == null) {
			return;
		}
		// 复制tr节点
		Element copeTitleTrNode = XmlDownUtil.copyNodes(trEle);
		// 获取tc节点
		Element tcEle = getTableTcNode(copeTitleTrNode,
				XmlDownUtil.wideTableTitle);
		// 生成标题行
		createWideTable(tcEle, titleStrNum, widthNum);
		// 标题生成完删除TR节点
		XmlDownUtil.deleteNode(trEle);
		// 删除样式的tc节点
		XmlDownUtil.deleteNode(tcEle);
	}

	/**
	 * 创建横向内容列(合并)
	 * 
	 * @author fuzhh 2013-5-28
	 * @param copeTrContentNode
	 *            复制table中的tr节点
	 * @param strNum
	 *            数据
	 * @param widthNum
	 *            列宽
	 * @param isMergeOne
	 *            是否为合并的第一列
	 */
	public static void createWideTableMergeContent(Element copeTrContentNode,
			String[] strNum, int widthNum, boolean isMergeOne) {
		// 复制tr节点
		Element copeContentTrNode = XmlDownUtil.copyNodes(copeTrContentNode);
		// 获取tc节点
		Element tcContentEle = getTableTcNode(copeContentTrNode,
				XmlDownUtil.wideTableContent);
		createWideTable(tcContentEle, strNum, 0, isMergeOne, widthNum);
		XmlDownUtil.deleteNode(tcContentEle);
	}

	/**
	 * 创建横向内容列（不合并）
	 * 
	 * @author fuzhh 2013-5-28
	 * @param copeTrContentNode
	 *            复制table中的tr节点
	 * @param strNum
	 *            数据
	 * @param widthNum
	 *            列宽
	 */
	public static void createWideTableContent(Element copeTrContentNode,
			String[] strNum, int widthNum) {
		// 复制tr节点
		Element copeContentTrNode = XmlDownUtil.copyNodes(copeTrContentNode);
		// 获取tc节点
		Element tcContentEle = getTableTcNode(copeContentTrNode,
				XmlDownUtil.wideTableContent);
		createWideTable(tcContentEle, strNum, widthNum);
		XmlDownUtil.deleteNode(tcContentEle);
	}

	/**
	 * 创建横向内容列(根据传人的Tc节点创建并合并)
	 * 
	 * @author fuzhh 2013-5-28
	 * @param copeTrContentNode
	 *            复制table中的tc节点
	 * @param strNum
	 *            数据
	 * @param columnNum
	 *            合并的列
	 * @param widthNum
	 *            列宽
	 * @param isMergeOne
	 *            是否为合并的第一列
	 */
	public static void createWideTableByTc(Element tcContentNode,
			String[] strNum, int columnNum, int widthNum, boolean isMergeOne) {
		for (int i = 0; i < strNum.length; i++) {
			// 复制Tc
			Element copeTcEle = XmlDownUtil.copyNodes(tcContentNode);
			createWideRowDate(copeTcEle, strNum, columnNum, widthNum,
					isMergeOne, i);
		}
	}

	/**
	 * 创建横向内容列(根据传人的Tc节点创建并合并)
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tabTilNode
	 *            复制table中的tc节点
	 * @param copeTrContentNode
	 *            复制table中的tc节点
	 * @param strNum
	 *            数据
	 * @param columnNum
	 *            合并的列
	 * @param widthNum
	 *            列宽
	 * @param isMergeOne
	 *            是否为合并的第一列
	 */
	public static void createWideTableByTc(Element tabTilNode,
			Element tcContentNode, String[] strNum, int columnNum,
			int widthNum, boolean isMergeOne) {
		for (int i = 0; i < strNum.length; i++) {
			// 复制Tc
			Element copeTcEle = null;
			if (i == columnNum) {
				copeTcEle = XmlDownUtil.copyNodes(tabTilNode);
			} else {
				copeTcEle = XmlDownUtil.copyNodes(tcContentNode);
			}
			createWideRowDate(copeTcEle, strNum, columnNum, widthNum,
					isMergeOne, i);
		}
	}

	/**
	 * 设置table 中tc 的数据
	 * 
	 * @author fuzhh 2013-5-30
	 * @param copeTcEle
	 *            复制的tc节点
	 * @param strNum
	 *            tc数据数组
	 * @param columnNum
	 *            合并的行
	 * @param widthNum
	 *            tc的宽度
	 * @param isMergeOne
	 *            是否为合并第一行
	 * @param
	 */
	public static void createWideRowDate(Element copeTcEle, String[] strNum,
			int columnNum, int widthNum, boolean isMergeOne, int i) {
		String strVal = strNum[i];
		// 找到tc下的tcPr 节点
		List<Element> tablTcPrEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(copeTcEle, tablTcPrEleList, "tcPr");
		if (tablTcPrEleList.size() > 0) {
			Element tcPrEle = tablTcPrEleList.get(0);
			updateTcWidth(tcPrEle, widthNum);
			if (columnNum == i) {
				Element vmerge = tcPrEle.addElement("w:vmerge");
				if (isMergeOne) {
					XmlDownUtil.setEleAttrubute(vmerge, "w:val", "restart");
				}
			}
		}
		XmlDownUtil.updateNodeValueByNode(copeTcEle, strVal);
	}

	/**
	 * 创建横向表格列 和标题
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param objList
	 *            表格数据
	 * @param titleStrNum
	 *            标题列数组
	 * @param titleName
	 *            标题名称
	 */
	public static void creWideTabAndTitleConDate(Element tableEle,
			Element titleEle, Element contentEle, List<Object[]> objList,
			String[] titleStrNum, int widthNum, String titleName) {
		// 创建标题
		JecnXmlContentUtil.createTitle(titleName, titleEle);
		if (objList != null && objList.size() > 0) {
			createWideTabDate(tableEle, objList, titleStrNum, widthNum);
		} else {
			JecnXmlContentUtil.createContent("", contentEle);
		}
	}
	
	/**
	 * 创建表格
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table 节点数据
	 * @param objList
	 *            需要生成的数据
	 * @param titleStrNum
	 *            标题数组
	 * @param widthNum
	 *            宽度
	 */
	public static void createWideTabDate(Element tableEle,
			List<Object[]> objList, String[] titleStrNum, int widthNum) {
		// 复制table节点
		Element copeTableNode = XmlDownUtil.copyNodes(tableEle);
		// 创建横向标题
		createWideTableTitle(copeTableNode, titleStrNum, widthNum);
		// 获取横向Table的tr内容节点
		Element trContentEle = getTableTrNode(copeTableNode,
				XmlDownUtil.wideTableContent);
		// 创建内容
		for (Object[] obj : objList) {
			String[] strVals = JecnXmlContentUtil.strNumsByObj(obj,
					titleStrNum.length);
			createWideTableContent(trContentEle, strVals, widthNum);
		}
		XmlDownUtil.deleteNode(trContentEle);
	}
	
}
