package epros.draw.gui.workflow.tabbedPane;

import javax.swing.JButton;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTabbedTitleButton extends JButton {
	/** 分页标题面板 */
	private JecnTabbedTitlePanel tabbedTitlePanel = null;

	public JecnTabbedTitleButton(JecnTabbedTitlePanel tabbedTitlePanel) {
		if (tabbedTitlePanel == null) {
			JecnLogConstants.LOG_TABBED_TITLE_BUTTON
					.error("JecnTabbedTitleButton类构造函数：参数为null. tabbedTitlePanel="
							+ tabbedTitlePanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.tabbedTitlePanel = tabbedTitlePanel;

		initComponents();
	}

	private void initComponents() {

		// 不让绘制内容区域
		this.setContentAreaFilled(false);
		// 无边框
		this.setBorder(null);
		// 设置按钮边框和标签之间的空白
		this.setMargin(JecnUIUtil.getInsets0());
		this.setBorderPainted(false);
		// 隐藏选择焦点
		this.setFocusable(false);
		// 图片
		this.setIcon(JecnWorkflowUtil.getTabCloseBtnIcon());

		// 事件
		this.addActionListener(JecnDrawMainPanel.getMainPanel());
		this.addMouseListener(JecnDrawMainPanel.getMainPanel());
		this.addMouseMotionListener(JecnDrawMainPanel.getMainPanel());

	}

	public JecnTabbedTitlePanel getTabbedTitlePanel() {
		return tabbedTitlePanel;
	}

	/**
	 * 
	 * 获取当前按钮所在的选项卡的索引
	 * 
	 * @return int 当前按钮所在的选项卡的索引
	 */
	public int getTabbedIndex() {
		return getTabbedTitlePanel().getTabbedPane().indexOfComponent(
				getTabbedTitlePanel().getCurrDesktopPane().getScrollPanle());
	}

}