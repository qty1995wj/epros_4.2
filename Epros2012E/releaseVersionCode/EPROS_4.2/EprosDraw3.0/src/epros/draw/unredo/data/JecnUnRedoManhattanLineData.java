/*      Copyright (c) 2012 JECN Corporation                     */
/*      JECN CONFIDENTIAL AND PROPRIETARY                       */
/*      All rights reserved by JECN Corporation.                */
/*      This program must be used solely for the purpose for    */
/*      which it was furnished by JECN Corporation.   No part   */
/*      of this program may be reproduced  or  disclosed  to    */
/*      others,  in  any form,  without  the  prior  written    */
/*      permission of JECN Corporation. Use  of  copyright      */
/*      notice does not evidence publication of the program.    */
package epros.draw.unredo.data;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;

/**
 * 
 * 连接线属性备份类，只备份连接线重要的属性
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUnRedoManhattanLineData {
	/** 备份的连接线对象 */
	private JecnManhattanLineData manhattanLineDataClone = null;
	/** 连接线对象 */
	private JecnBaseManhattanLinePanel manhattanLine = null;

	/** 线的开始图形 */
	protected JecnBaseFigurePanel startFigure = null;
	/** 线的结束图形 */
	protected JecnBaseFigurePanel endFigure = null;

	public JecnUnRedoManhattanLineData(JecnBaseManhattanLinePanel manhattanLine) {
		if (manhattanLine == null) {
			JecnLogConstants.log_JecnUnRedoManhattanLineData
					.error("JecnUnRedoManhattanLineData(JecnBaseManhattanLinePanel manhattanLine)异常：参数为空");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.manhattanLine = manhattanLine;
		this.manhattanLineDataClone = manhattanLine.getFlowElementData()
				.clone();
		// TODO稳定后确认是否删除
		this.startFigure = manhattanLine.getStartFigure();
		this.endFigure = manhattanLine.getEndFigure();
	}

	public JecnManhattanLineData getManhattanLineDataClone() {
		return manhattanLineDataClone;
	}

	public JecnBaseManhattanLinePanel getManhattanLine() {
		return manhattanLine;
	}

	/**
	 * 
	 * 获取连接线数据对象
	 * 
	 * @return JecnManhattanLineData 连接线数据对象
	 */
	public JecnManhattanLineData getManhattanLineData() {
		return manhattanLine.getFlowElementData();
	}

	public JecnBaseFigurePanel getStartFigure() {
		return startFigure;
	}

	public void setStartFigure(JecnBaseFigurePanel startFigure) {
		this.startFigure = startFigure;
	}

	public JecnBaseFigurePanel getEndFigure() {
		return endFigure;
	}

	public void setEndFigure(JecnBaseFigurePanel endFigure) {
		this.endFigure = endFigure;
	}

}
