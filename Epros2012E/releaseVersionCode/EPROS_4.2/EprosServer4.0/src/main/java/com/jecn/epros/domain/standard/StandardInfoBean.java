package com.jecn.epros.domain.standard;

import com.jecn.epros.domain.process.Path;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 制度清单bean
 *
 * @author user
 */
public class StandardInfoBean implements Serializable {
    private List<Path> paths = new ArrayList<Path>();
    private Integer level;
    private String path;
    private Long standardId;
    private String standName;
    private Long preStandId;
    private Long relatedId;
    /**
     * 标准类型 标准类型(0是目录，1文件标准，2流程标准 3流程地图标准4标准条款5条款要求)
     */
    private int standType;
    /**
     * 条款对应要求
     */
    private String standContent;
    /**
     * 标准清单相关文件信息
     **/
    private List<StandardRelatePRFBean> listStandardRefBean = new ArrayList<StandardRelatePRFBean>();
    /**
     * 记录当前标准内添加的活动ID
     */
    private Set<Long> activeSet = new HashSet<Long>();
    /**
     * 记录当前标准添加的流程ID
     */
    private Set<Long> flowSet = new HashSet<Long>();
    /**
     * 记录当前标准添加的制度ID
     */
    private Set<Long> ruleSet = new HashSet<Long>();


    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getStandardId() {
        return standardId;
    }

    public void setStandardId(Long standardId) {
        this.standardId = standardId;
    }

    public String getStandName() {
        return standName;
    }

    public void setStandName(String standName) {
        this.standName = standName;
    }

    public Long getPreStandId() {
        return preStandId;
    }

    public void setPreStandId(Long preStandId) {
        this.preStandId = preStandId;
    }

    public int getStandType() {
        return standType;
    }

    public void setStandType(int standType) {
        this.standType = standType;
    }

    public String getStandContent() {
        return standContent;
    }

    public void setStandContent(String standContent) {
        this.standContent = standContent;
    }

    public List<StandardRelatePRFBean> getListStandardRefBean() {
        return listStandardRefBean;
    }

    public void setListStandardRefBean(List<StandardRelatePRFBean> listStandardRefBean) {
        this.listStandardRefBean = listStandardRefBean;
    }

    public Set<Long> getActiveSet() {
        return activeSet;
    }

    public void setActiveSet(Set<Long> activeSet) {
        this.activeSet = activeSet;
    }

    public Set<Long> getFlowSet() {
        return flowSet;
    }

    public void setFlowSet(Set<Long> flowSet) {
        this.flowSet = flowSet;
    }

    public Set<Long> getRuleSet() {
        return ruleSet;
    }

    public void setRuleSet(Set<Long> ruleSet) {
        this.ruleSet = ruleSet;
    }

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }
}
