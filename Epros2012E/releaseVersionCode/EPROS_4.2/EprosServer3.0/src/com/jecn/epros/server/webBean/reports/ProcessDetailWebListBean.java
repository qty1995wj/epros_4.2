package com.jecn.epros.server.webBean.reports;

import java.util.ArrayList;
import java.util.List;

public class ProcessDetailWebListBean {
	
	/** 流程架构或者责任部门的id*/
	private long id;
	/** 流程架构或者责任部门的name*/
	private String name;
	/** 流程架构或者责任部门在Excel中显示的名称*/
	private String sheetName;
	/** 流程架构或者责任部门下的所有的流程清单*/
	private List<ProcessDetailWebBean> allDetailWebBeans=new ArrayList<ProcessDetailWebBean>();
    /** 流程架构或者责任部门下流程清单的最大深度*/
	private int maxLevel=0;
	/** 责任部门、流程地图已发布流程数量*/
	private int pubCount=0;
	/** 责任部门、流程地图已发布流程数量*/
	private int notPubCount=0;
	/** 责任部门、流程地图总体流程数量*/
	private int allCount=0;
	/** 流程发布比例 已发布除以总数*/
	private float pubPercent=0;
	/** 0为责任部门 1为流程地图*/
	private int type;
	
	public int getPubCount() {
		return pubCount;
	}

	public void setPubCount(int pubCount) {
		this.pubCount = pubCount;
	}

	public int getNotPubCount() {
		return notPubCount;
	}

	public void setNotPubCount(int notPubCount) {
		this.notPubCount = notPubCount;
	}

	public int getAllCount() {
		return allCount;
	}

	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}

	public float getPubPercent() {
		return pubPercent;
	}

	public void setPubPercent(float pubPercent) {
		this.pubPercent = pubPercent;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public List<ProcessDetailWebBean> getAllDetailWebBeans() {
		return allDetailWebBeans;
	}

	public void setAllDetailWebBeans(List<ProcessDetailWebBean> allDetailWebBeans) {
		this.allDetailWebBeans = allDetailWebBeans;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	

}
