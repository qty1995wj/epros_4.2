package com.jecn.epros.server.service.standard;

import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.standard.StandardWebInfoBean;

/**
 * 标准清单接口
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-14 时间：下午01:34:11
 */
public interface IStandardListService extends IBaseService<String, String> {
	/**
	 * 返回清单数据List集合
	 * 
	 * @param standId
	 *            点击的标准阶段ID
	 * @param showAll
	 *            true 全部展示;false :展示10条
	 * @return List<StandardInfoListBean>
	 * @throws Exception
	 */
	StandardWebInfoBean getStandardInfoList(Long standId, Long projectId,
			boolean showAll) throws Exception;

}
