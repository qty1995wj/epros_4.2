package epros.draw.gui.swing;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 带JTable的滚动面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTableScrollPane extends JScrollPane {
	/** 表 */
	private JTable table = null;

	public JecnTableScrollPane() {
		initComponent();
	}

	private void initComponent() {
		//创建表对象
		table = new JTable();

		table.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 自定义表头UI
		table.getTableHeader().setUI(new JecnBasicTableHeaderUI());

		this.setBorder(JecnUIUtil.getTootBarBorder());
		this.getViewport()
				.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setViewportView(table);
	}

	public JTable getTable() {
		return table;
	}
}
