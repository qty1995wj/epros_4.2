package com.jecn.epros.server.service.dataImport.match;

import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 获取待导入数据
 */
public interface IMatchDataFactory {
	/**
	 * 获取待导入数据
	 * 
	 * @return
	 * @throws Exception
	 *             20120209
	 */
	public BaseBean getImportData(String importType) throws Exception;

	/**
	 * 
	 * 获取配置文件之值
	 * 
	 * @param importType
	 *            String
	 * @return
	 */
	public AbstractConfigBean getInitTime(String importType) throws Exception;
}
