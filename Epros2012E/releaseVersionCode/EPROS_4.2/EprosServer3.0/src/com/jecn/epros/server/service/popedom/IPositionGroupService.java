package com.jecn.epros.server.service.popedom;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.dataimport.AddOrDelBaseBean;
import com.jecn.epros.server.bean.dataimport.BasePosAndPostionBean;
import com.jecn.epros.server.bean.popedom.JecnPositionGroup;
import com.jecn.epros.server.bean.system.JecnTablePageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IPositionGroupService extends IBaseService<JecnPositionGroup, Long> {
	/**
	 * @author yxw 2012-5-15
	 * @description:树加载，通过父类Id和项目ID，获得子节点(目录和岗位组)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildPositionGroup(Long pId, Long projectId) throws Exception;

	
	/**
	 * @author yxw 2012-5-15
	 * @description:树加载，通过父类Id和项目ID 人员ID，获得子节点(目录和岗位组)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildPositionGroup(Long pId, Long projectId,Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description: 树加载，通过父类Id和项目ID，获得岗位组目录
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildPositionGroupDirs(Long pId, Long projectId) throws Exception;

	/**
	 * @author zhangchen May 15, 2012
	 * @description：获得岗位组下面的岗位
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPosByPositionGroupId(Long id) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:树加载，通过项目ID，获得Role所有的子节点(目录、岗位组、岗位)
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getAllPositionGroup(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:树加载，通过项目ID，获得所有岗位组目录
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getAllPositionGroupDir(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:table加载，通过项目Id，名称，搜索岗位组
	 * @param projectId
	 * @param name
	 * @return
	 */
	public List<JecnTreeBean> searchPositionGroupsByName(Long projectId, String name) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:table加载，通过项目Id，名称，权限 搜索岗位组 
	 * @param projectId
	 * @param name
	 * @return
	 */
	public List<JecnTreeBean> searchPositionGroupsByName(Long projectId, String name,Long peopleId) throws Exception;

	
	/**
	 * @author yxw 2012-5-15
	 * @description:排序节点的父节点ID
	 * @param list
	 * @param pId
	 * @param projectId
	 * @param peopleId 
	 */
	public void updateSortPosGroup(List<JecnTreeDragBean> list, Long pId, Long projectId, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:节点移动
	 * @param listIds
	 * @param pId
	 *            移动到新节点的Id
	 */
	public void movePositionGroups(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description: 删除节点
	 * @param listIds
	 * @param peopleId 
	 */
	public void deletePositionGroups(List<Long> listIds, Long projectId, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:
	 * @return
	 * @throws Exception
	 */
	public void updatePositionGroup(Long positionGroupId, List<Long> positionIds, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:重命名
	 * @param newName
	 * @param id
	 * @param updatePersionId
	 */
	public void rePositionGroupName(String newName, Long id, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:table加载，移动节点，搜索除了移动节点和移动节点子级目录外的其它目录
	 * @param name
	 * @param listIds
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getPositionGroupDirsByNameMove(String name, List<Long> listIds, Long projectId)
			throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description:更新时，判断是否与同一父ID下同一类是否重名
	 * @param newName
	 * @param id
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-15
	 * @description: 根据ID和项目ID递归上级节点，包括和一级的兄弟节点
	 * @param id
	 * @paramprojectId
	 * @return
	 */
	public List<JecnTreeBean> getPnodes(Long id, Long projectId) throws Exception;

	/**
	 * 查询基准岗位
	 * 
	 * @author cheshaowei
	 * 
	 * @param baseName
	 *            搜索的基准岗位名称 posGroupId 选中岗位组ID
	 * */
	public BasePosAndPostionBean getJecnBaseBean(Long projectId, String baseName, String posGroupId);

	/**
	 * 根据ID查询基准岗位对应岗位
	 * 
	 * @author cheshaowei
	 * @param baseId
	 *            基准岗位ID
	 * @throws Exception
	 * 
	 * */
	public List<Object[]> getBaseRelPos(String baseId, String pageNum, String pageSize, String operationType)
			throws Exception;

	/**
	 * 
	 * 添加新关联的基准岗位，关联基准岗位
	 * 
	 * @author cheshaowei
	 * @param addOrDelBaseBean
	 *            待添加数据和待更新数据封装bean
	 * @throws Exception
	 * 
	 * */
	public void addOrUpdate(AddOrDelBaseBean addOrDelBaseBean) throws Exception;

	/**
	 * 判断是否存在岗位组和基准岗位关联
	 * 
	 * @param @param groupId
	 * @param @return
	 * @param @throws Exception
	 * @author ZXH
	 * @date 2015-7-17 下午02:33:16
	 * @throws
	 */
	int posGroupRelCount(Long groupId) throws Exception;

	public void AddSelectPoss(Set<Long> selectIds, Long groupId) throws Exception;

	void AddSearchResultPoss(Long orgId, String posName, Long groupId, Long projectId) throws Exception;

	JecnTablePageBean searchPos(Long orgId, String posName, int curPage, String operationType, int pageSize,
			Long groupId, Long projectId) throws Exception;

	void delResultSelectPoss(Set<Long> selectIds, Long groupId) throws Exception;

	void cancelResultPoss(Long groupId) throws Exception;

	JecnTablePageBean showPos(int curPage, String operationType, int pageSize, Long groupId) throws Exception;
	
	public void AddSearchResultPoss(String orgName, String posName, Long groupId, Long projectId,boolean isSelect);
	
	public void AddSearchResultPoss(List<Long> ids, String posName,Long groupId, Long projectId,boolean isSelect);

	public JecnTablePageBean searchPos(String orgName, String posName, int curPage, String operationType, int pageSize,
			Long groupId, Long projectId,boolean isSelect);
	
	public JecnTablePageBean searchPos(List<Long> orgIds, String posName, int curPage, String operationType, int pageSize,
			Long groupId, Long projectId,boolean isSelect);


	public String getPosGroupExplain(Long id) throws SQLException, IOException;


	public boolean getPosGroupExplain(Long id, String posGroupExplain, Long peopleId) throws Exception;


	public Long addPositionGroup(JecnPositionGroup positionGroup) throws Exception;
}
