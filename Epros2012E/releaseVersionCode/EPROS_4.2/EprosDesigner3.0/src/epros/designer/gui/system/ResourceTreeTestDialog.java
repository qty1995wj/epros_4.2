package epros.designer.gui.system;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.draw.gui.top.dialog.JecnDialog;

public class ResourceTreeTestDialog extends JecnDialog {

	private JPanel mainPanel=new JPanel();
	private JScrollPane s=new JScrollPane();
	private ResourceHighEfficiencyTree  tree=new ResourceHighEfficiencyTree();
	
	private JPanel p=new JPanel();
	public ResourceTreeTestDialog(){
		this.setSize(300, 800);
		Dimension d=new Dimension(200,700);
		s.setPreferredSize(d);
		s.setMinimumSize(d);
		s.setViewportView(tree);
		mainPanel.add(s);
		this.getContentPane().add(mainPanel);
	}
	
	static String hexByte(byte b) {
	    // String s = "000000" + Integer.toHexString(b);
	    // return s.substring(s.length() - 2);
	    return String.format("%02x",b); // 网友的建议,可修改为这个代码，更简单通用一些
	  }
}
