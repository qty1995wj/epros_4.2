package com.jecn.epros.server.service.vatti.impl;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.service.vatti.VattiDominoDataService;
import com.jecn.epros.server.webBean.vatti.DominoDataBean;

/**
 * 华帝Domino数据操作Service
 * 
 * @author weidp
 * @date： 日期：2014-5-27 时间：上午10:02:01
 */
public class VattiDominoDataServiceImpl extends AbsBaseService<JecnUser, Long>
		implements VattiDominoDataService {

	private IFlowStructureDao flowStructureDao;
	private IRuleDao ruleDao;

	@Override
	public DominoDataBean getDominoFlowData(Long flowId) throws Exception {
		JecnFlowStructure flow = flowStructureDao
				.findJecnFlowStructureById(flowId);
		DominoDataBean dominoData = new DominoDataBean(flow);
		return dominoData;
	}

	@Override
	public DominoDataBean getDominoRuleData(Long ruleId) throws Exception {
		Rule rule = ruleDao.getRuleById(ruleId);
		DominoDataBean dominoData = new DominoDataBean(rule);
		return dominoData;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	public void setRuleDao(IRuleDao ruleDao) {
		this.ruleDao = ruleDao;
	}

}
