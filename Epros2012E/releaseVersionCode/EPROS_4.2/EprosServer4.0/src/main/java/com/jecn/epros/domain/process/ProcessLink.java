package com.jecn.epros.domain.process;

import com.jecn.epros.domain.LinkResource;

/**
 * Created by admin on 2018/3/15.
 */
public class ProcessLink {
    private LinkResource docLink;

    private LinkResource imageLink;

    public LinkResource getDocLink() {
        return docLink;
    }

    public void setDocLink(LinkResource docLink) {
        this.docLink = docLink;
    }

    public LinkResource getImageLink() {
        return imageLink;
    }

    public void setImageLink(LinkResource imageLink) {
        this.imageLink = imageLink;
    }
}
