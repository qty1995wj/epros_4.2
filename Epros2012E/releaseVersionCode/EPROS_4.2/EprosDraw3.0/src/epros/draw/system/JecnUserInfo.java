package epros.draw.system;

import epros.draw.util.JecnResourceUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 用户提示信息管理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUserInfo {
	/**
	 * 
	 * 获取用户信息
	 * 
	 * @param key
	 *            String 用户信息的KEY
	 * @return String 用户信息的Value
	 */
	public static String getValue(String key) {
		if (DrawCommon.isNullOrEmtryTrim(key)) {
			return "";
		}
		return JecnResourceUtil.getJecnResourceUtil().getValue(key);
	}

	/**
	 * 
	 * 当前文件已经存在是否覆盖？
	 * 
	 * @return String 返回内容“当前文件已经存在是否覆盖？”
	 */
	public static String getExistFileInfo() {
		return getValue("localFileExist");
	}

	/**
	 * 
	 * 保存失败，请确认文件是否编辑中或文件名称是否有\/:*?"<>|字符
	 * 
	 * @return String
	 */
	public static String getSaveFileFail() {
		return getValue("saveFileFail");
	}

	/**
	 * 
	 * 创建文件异常，请确定文件名是否有\/:*?"<>|字符
	 * 
	 * @return
	 */
	public static String getCreateFileError() {
		return getValue("createFileError");
	}
}
