package com.jecn.epros.server.dao.integration.impl;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.integration.IJecnControlTargetDao;
/****
 * 控制目标表操作类
 * 2013-10-30
 *
 */
public class JecnControlTargetDaoImpl extends AbsBaseDao<JecnControlTarget, Long> implements IJecnControlTargetDao {

	@Override
	public void delControlTargetByRiskIds(Set<Long> setIds) throws Exception {
		// 风险-设计器
		String hql = "delete from JecnControlTarget where riskId in ";
		List<String> listStr = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		
	}

}
