package epros.designer.gui.process.guide.guideTable;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.process.guide.DesignerProcessOperationIntrusDialog;
import epros.designer.util.JecnConfigTool;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 活动说明表格
 * 
 * @author 2012-07-05
 * 
 */
public class DesignerActiveDescTable extends JTable implements MouseListener {
	/** 界面初始化的数据 */
	private List<JecnActivityShowBean> listAllActivityShow;
	/** 活动说明 */
	private DesignerProcessOperationIntrusDialog processOperationIntrusDialog;

	/**
	 * 界面初始化
	 * 
	 * @param listAllActivityShow
	 *            界面初始化的数据
	 */
	public DesignerActiveDescTable(List<JecnActivityShowBean> listAllActivityShow,
			DesignerProcessOperationIntrusDialog processOperationIntrusDialog) {
		this.processOperationIntrusDialog = processOperationIntrusDialog;
		this.listAllActivityShow = listAllActivityShow;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
		this.addMouseListener(this);
	}

	private int[] gethiddenCols() {
		if (JecnConfigTool.showActInOutBox()) {
			return new int[] { 0, 7 };
		} else {
			return new int[] { 0, 5, 6, 7 };
		}
	}

	/**
	 * 初始化JTable数据
	 * 
	 * @author fuzhh Aug 28, 2012
	 * @return
	 */
	public ActiveDescTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activitiesNumbers"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("executiveRole"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityTitle"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityIndicatingThat"));
		if (JecnConstants.loginBean.getOtherLoginType() == 4) {
			title.add(JecnResourceUtil.getJecnResourceUtil().getValue("innerControlRisk"));
			title.add(JecnResourceUtil.getJecnResourceUtil().getValue("standardConditions"));
		} else {
			title.add(JecnResourceUtil.getJecnResourceUtil().getValue("input"));
			title.add(JecnResourceUtil.getJecnResourceUtil().getValue("output"));
		}
		title.add("roleUIID");
		// 获取操作说明数据
		Vector<Vector<Object>> rowData = new Vector<Vector<Object>>();
		for (JecnActivityShowBean activityShowBean : listAllActivityShow) {
			Vector<Object> row = new Vector<Object>();
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum());
			row.add(activityShowBean.getRoleName());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFigureText());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow());
			if (JecnConstants.loginBean.getOtherLoginType() == 4) {
				row.add(activityShowBean.getActiveFigure().getFlowElementData().getInnerControlRisk());
				row.add(activityShowBean.getActiveFigure().getFlowElementData().getStandardConditions());
			} else {
				String input = activityShowBean.getActiveFigure().getFlowElementData().getActivityInput();
				row.add(DrawCommon.isNullOrEmtryTrim(input) ? activityShowBean.getInput() : input);
				String output = activityShowBean.getActiveFigure().getFlowElementData().getActivityOutput();
				row.add(DrawCommon.isNullOrEmtryTrim(output) ? activityShowBean.getOutput() : output);
			}
			row.add(activityShowBean.getRoleUIID());
			rowData.add(row);
		}
		return new ActiveDescTableMode(rowData, title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	class ActiveDescTableMode extends DefaultTableModel {
		public ActiveDescTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// 判断为左键双击
		if (e.getClickCount() == 2 && e.getButton() == 1) {
			int selectRow = this.getSelectedRow();
			DesignerActivitiesDetailsDialog drawJecnActiveDetailsDialog = new DesignerActivitiesDetailsDialog(
					listAllActivityShow.get(selectRow), processOperationIntrusDialog);
			drawJecnActiveDetailsDialog.setVisible(true);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}
}
