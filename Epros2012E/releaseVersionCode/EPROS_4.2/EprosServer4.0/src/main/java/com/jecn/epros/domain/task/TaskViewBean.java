package com.jecn.epros.domain.task;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class TaskViewBean {


    @ApiModelProperty("总数")
    private Integer total = 0;

    @ApiModelProperty("任务列表")
    private List<MyTaskBean> data;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<MyTaskBean> getData() {
        return data;
    }

    public void setData(List<MyTaskBean> data) {
        this.data = data;
    }

}
