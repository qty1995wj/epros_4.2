package com.jecn.epros.server.dao.integration;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.common.IBaseDao;

/*******************************************************************************
 * 风险表操作类 2013-10-30
 * 
 */
public interface IJecnRiskDao extends IBaseDao<JecnRisk, Long> {
	/***************************************************************************
	 * 删除风险
	 * 
	 * @param setIds
	 * @throws Exception
	 */
	public void deleteRisks(Set<Long> setIds) throws Exception;

	/**
	 * 根据风险目录ID获取当前目录下所有风险控制清单
	 * @author fuzhh 2013-12-13
	 * @param id
	 * @param projectId
	 * @param isShowAll
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRiskInventoryALL(Long id, Long projectId,
			boolean isShowAll) throws Exception;

	/**
	 * 风险清单 节点处在的级别
	 * 
	 * @author fuzhh 2013-12-13
	 * @param id
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int findRiskListLevel(Long id) throws Exception;

	public List<Object[]> getRoleAuthChildsRisk(Long pid, Long projectId, Long peopleId);

	public List<Object[]> searchRoleAuthByName(String name, Long peopleId);

}
