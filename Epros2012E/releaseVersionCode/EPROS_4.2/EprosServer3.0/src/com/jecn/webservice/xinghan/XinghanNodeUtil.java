package com.jecn.webservice.xinghan;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.jecn.webservice.xinghan.bean.FlowInfo;
import com.jecn.webservice.xinghan.bean.FlowLink;
import com.jecn.webservice.xinghan.bean.FlowNode;
import com.jecn.webservice.xinghan.bean.Point;

/**
 * 操作星汉流程图xml节点工具类
 * 
 * @author chehuanbo
 * 
 */
public class XinghanNodeUtil {
	
	private static final Log log = LogFactory.getLog(XinghanNodeUtil.class);
	/**
	 * 把流程图对象转换为XMl
	 * 
	 * @param flowInfo
	 * @return
	 */
	public static String createXml(FlowInfo flowInfo) {
		if (flowInfo == null) {
			log.error("flowInfo  is null");
			return null;
		}
		Document doc = null;
		try {
			doc = DocumentHelper.createDocument();
			Element flowcat = doc.addElement("interface").addElement("flowcat");
			// 设置流程类别属性
			flowcat.addAttribute("flowcatid", flowInfo.getFlowCatId());
			flowcat.addAttribute("flowcatno", flowInfo.getFlowCatNo());
			flowcat.addAttribute("flowcatname", flowInfo.getFlowCatName());
			// 流程节点
			Element flow = flowcat.addElement("flow");
			flow.addAttribute("flowid", flowInfo.getFlowId());
			flow.addAttribute("flowno", flowInfo.getFlowNo());
			flow.addAttribute("flowname", flowInfo.getFlowName());
			// 节点
			if (flowInfo.getFlowNode() != null
					&& flowInfo.getFlowNode().size() > 0) {
				for (FlowNode node : flowInfo.getFlowNode()) {
					Element eNode = flow.addElement("node");
					eNode.addAttribute("nodeid", node.getNodeId());
					eNode.addAttribute("nodename", node.getNodeName());
					eNode.addAttribute("nodetype", node.getNodeType());
					eNode.addAttribute("xNodePoint", node.getxNodePoint());
					eNode.addAttribute("yNodePoint", node.getyNodePoint());
				}
			}
			// 连接线
			if (flowInfo.getFlowLink() != null
					&& flowInfo.getFlowLink().size() > 0) {
				Element eLink = null;
				Element ePoint = null;
				for (FlowLink link : flowInfo.getFlowLink()) {
					eLink = flow.addElement("link");
					eLink.addAttribute("linkid", link.getLinkId());
					eLink.addAttribute("linkname", link.getLinkName());
					eLink.addAttribute("linktype", link.getLinkType());
					eLink.addAttribute("fromnodeid", link.getFromNodeId());
					eLink.addAttribute("tonodeid", link.getToNodeId());
					if (link.getPoint() != null && link.getPoint().size() > 0) {
						for (Point point : link.getPoint()) {
							ePoint = eLink.addElement("point");
							ePoint.addAttribute("xLinkPoint", point
									.getxLinkPoint());
							ePoint.addAttribute("yLinkPoint", point
									.getyLinkPoint());
						}

					}
				}
			}
			return doc.asXML();
		} catch (Exception e) {
			log.error("构建dom文档异常", e);
			return null;
		}
	}

	/**
	 * 解析xml获取 返回同步状态
	 * 
	 * @param xml
	 * @return doc
	 */
	public static boolean parseXml(String xml) {
		if (xml != null && !"".equals(xml)) {
			try {
				Document doc = DocumentHelper.parseText(xml);
				Element root = doc.getRootElement();
				String value = root.element("interfacecode").element("result")
						.element("flag").getText();
				if ("1".equals(value)) {
					return true;
				} else {
					return false;
				}
			} catch (DocumentException e) {
				log.error("解析XML异常,xml=" + xml, e);
				return false;
			}
		}
		return false;
	}
}
