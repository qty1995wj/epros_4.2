package epros.designer.gui.process.flow.file;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.rule.edit.RelatedFilePanel;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;

/**
 * 流程(文件) 相关标准
 * 
 * @author ZXH
 * @date 2017-4-20下午04:40:46
 */
public class FlowRelatedStandardPanel extends RelatedFilePanel {

	public FlowRelatedStandardPanel(List<JecnTreeBean> treeBeanList) {
		super(treeBeanList, RelatedNodeType.standard);
	}

	protected Vector<Object> initTableRowData(JecnTreeBean treeBean) {
		Vector<Object> row = new Vector<Object>();
		row.add(treeBean.getId());
		// 关联ID
		row.add(treeBean.getRelationId());
		row.add(treeBean.getName());
		// 获取类型对应的名称
		row.add(JecnUtil.getTypeName(treeBean.getTreeNodeType()));
		// 详细
		row.add(this.newEditJLabel());
		// 隐藏类型
		row.add(JecnUtil.getRelaType(treeBean.getTreeNodeType()));
		return row;
	}


	/**
	 * 隐藏table列
	 * 
	 */
	@Override
	protected void hiddenColumns() {
		// 渲染表格
		relatedFileTable.getColumnModel().getColumn(4).setCellRenderer(this.newEditJLabel());
		TableColumnModel columnModel = relatedFileTable.getColumnModel();
		columnModel.getColumn(3).setMaxWidth(60);
		columnModel.getColumn(3).setMinWidth(60);
		columnModel.getColumn(4).setMaxWidth(100);
		columnModel.getColumn(4).setMinWidth(100);
		TableColumn tableColumn = columnModel.getColumn(0);
		// 默认隐藏
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		columnModel.getColumn(1).setMinWidth(0);
		columnModel.getColumn(1).setMaxWidth(0);
		// 关联类型
		columnModel.getColumn(5).setMinWidth(0);
		columnModel.getColumn(5).setMaxWidth(0);

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		columnModel.getColumn(3).setCellRenderer(render);
		// 行高 20
		relatedFileTable.setRowHeight(20);
	}


	/**
	 * 获取表头
	 * 
	 * @return
	 */
	@Override
	protected Vector<Object> getTitle() {
		Vector<Object> title = new Vector<Object>();
		title.add("id");
		title.add("fileId");
		// 名称
		title.add(JecnProperties.getValue("name"));
		// 类型
		title.add(JecnProperties.getValue("type"));
		// 操作
		title.add(JecnProperties.getValue("operational"));
		title.add("relateType");
		return title;
	}

	/**
	 * 
	 * 详情双击
	 * 
	 * @return
	 */
	// @Override
	public void isClickTableEdit() {
		if (relatedFileTable.getSelectedColumn() == 4) {
			int row = relatedFileTable.getSelectedRow();
			if (row != -1) {
				// 标准类型
				int relateType = Integer.valueOf(relatedFileTable.getValueAt(row, 5).toString());
				long standardId = Long.valueOf(relatedFileTable.getValueAt(row, 0).toString());
				JecnDesignerCommon.openStandard(standardId, relateType);
			}
		}
	}

}
