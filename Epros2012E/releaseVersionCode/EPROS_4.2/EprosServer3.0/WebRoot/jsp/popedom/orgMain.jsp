<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>组织详情</title>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript"
			src="${basePath}js/epros_${language}_${country}.js">
</script>
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js">
</script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js">
</script>
		<script type="text/javascript" src="${basePath}js/common.js">
</script>
		<script type="text/javascript" src="${basePath}js/firstPage.js"
			charset="UTF-8">
</script>
		<script type="text/javascript" src="${basePath}js/popedom/orgList.js">
</script>
<script type="text/javascript" src="${basePath}js/browserDetect.js" charset="UTF-8"></script>
		<script type="text/javascript">
Ext.onReady(function() {
	Ext.QuickTips.init();// 使得tip提示可用
		var orgMapMainPanel = new Ext.TabPanel({
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				region : 'center',
				items : [{
					title : i_orgMap,// 组织图
					id : "orgMap_id",
					autoLoad : "${basePath}"
							+ "jsp/processSys/"+processMapContainerJsp+"?mapType=organization&mapID="
							+ "${param.orgId}"+"&mapName="+"${param.name}"
				},{
					title : i_orgResponsibility,// 部门职责
					id : "orgResponsibility_id",
					autoLoad : "${basePath}"
							+ "findOrgDuties.action?orgId="
							+ "${param.orgId}"
				},{
					title : i_orgList,// 组织清单
					id : "i_orgList",
					autoLoad : {
					    url : "${basePath}"
							+  "jsp/popedom/relatedProcessList.jsp",
							 scripts : true,
							 callback : function() {
								orgListJson(${param.orgId},false,true);
							 }
					}
				}]
			});
	var northPanle=new Ext.Panel({
		height:20,
		region : 'north',
		contentEl:'homePage_id'		
	});
	new Ext.Viewport({
		layout : 'border',
		items : [northPanle,orgMapMainPanel]
	});
});

</script>
	</head>
	
	<body>
		<input type="hidden" id="_isPop" value="true"/>
		<div id="homePage_id" style="height: 25px; width: 100%; overflow: hidden;"
			align="right" class="gradient minWidth">
			<%@include file="/navigation.jsp"%>
		</div>
	</body>
</html>