package com.jecn.epros.server.action.web.project;

import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.popedom.OrganizationAction;
import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.project.ICurProjectService;
import com.jecn.epros.server.service.project.IProjectService;
import com.jecn.epros.server.service.system.IUnReadMessageService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

public class ProjectAction extends BaseAction {
	private static final Logger log = Logger.getLogger(ProjectAction.class);
	/** 项目service */
	private IProjectService projectService;
	/** 项目集合 */
	private List<JecnProject> listProject;
	/** 项目名称 */
	private String projectName;
	/** 项目ID */
	private String selprojectId;
	/** 未读消息 */
	private IUnReadMessageService unReadMesgService;
	/** 主项目操作 */
	private ICurProjectService curProjectService;
	/*** 主项目 */
	private JecnCurProject jecnCurProject;

	public ICurProjectService getCurProjectService() {
		return curProjectService;
	}

	public void setCurProjectService(ICurProjectService curProjectService) {
		this.curProjectService = curProjectService;
	}

	public IUnReadMessageService getUnReadMesgService() {
		return unReadMesgService;
	}

	public void setUnReadMesgService(IUnReadMessageService unReadMesgService) {
		this.unReadMesgService = unReadMesgService;
	}

	private OrganizationAction organizationAction;
	private IPersonService personService;

	public IPersonService getPersonService() {
		return personService;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	public OrganizationAction getOrganizationAction() {
		return organizationAction;
	}

	public void setOrganizationAction(OrganizationAction organizationAction) {
		this.organizationAction = organizationAction;
	}

	public String getSelprojectId() {
		return selprojectId;
	}

	public void setSelprojectId(String selprojectId) {
		this.selprojectId = selprojectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public IProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(IProjectService projectService) {
		this.projectService = projectService;
	}

	public List<JecnProject> getListProject() {
		return listProject;
	}

	public void setListProject(List<JecnProject> listProject) {
		this.listProject = listProject;
	}

	/***
	 * 获取所有项目
	 * 
	 * @return
	 */
	public String getAllProjects() {
		try {
			// 所有项目集合
			listProject = projectService.getListProject();
			// 主项目
			jecnCurProject = curProjectService.getJecnCurProjectById();
			// 如果是主项目，则在项目名称后边添加(主项目)
			for (JecnProject jecnProject : listProject) {
				if (jecnCurProject.getCurProjectId().equals(
						jecnProject.getProjectId())) {
					jecnProject.setProjectName(jecnProject.getProjectName()
							+ "(主项目)");
				}
				String projectName = jecnProject.getProjectName().replaceAll(
						" ", "&nbsp;").replaceAll("<", "&lt;").replaceAll(">",
						"&gt;");

				jecnProject.setProjectName(projectName);
			}
		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/***
	 * 打开项目
	 */
	public String openSelectedProject() {
		try {
			// String proName = new
			// String(projectName.getBytes("iso8859-1"),"utf-8");
			getRequest().setCharacterEncoding("utf-8");
			// 获取
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext
					.getContext().getSession().get("webLoginBean");
			webLoginBean.setProjectId(Long.valueOf(selprojectId.toString()));
			webLoginBean.setProjectName(projectName);
			// 未读消息数量
			int unReadMesgNum = unReadMesgService.getUnReadMesgNum(webLoginBean
					.getJecnUser().getPeopleId());
			webLoginBean.setMessageNum(unReadMesgNum);
		} catch (Exception e) {
			log.error(e);
			return ERROR;
		}
		return SUCCESS;
	}

}
