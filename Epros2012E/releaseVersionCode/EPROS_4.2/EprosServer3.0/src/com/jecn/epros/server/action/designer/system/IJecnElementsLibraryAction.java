package com.jecn.epros.server.action.designer.system;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnElementsLibrary;

public interface IJecnElementsLibraryAction {

	List<JecnElementsLibrary> getShowElementsLibraryByType(int type) throws Exception;

	void updateElementsLibary(List<String> list, int type) throws Exception;

	List<JecnElementsLibrary> getAllElementsLibraryByType(int type) throws Exception;

	List<JecnElementsLibrary> getAddElementsLibraryByType(int type) throws Exception;

}
