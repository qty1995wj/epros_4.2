package com.jecn.epros.server.action.web.login.ad.sailun;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

/**
 * 赛轮配置读取类 (cfgFile/sailun/saiLunDomainInfo.properties)
 * 
 */
public class SaiLunAfterItem {

	private static Logger log = Logger.getLogger(SaiLunAfterItem.class);
    
	static Map<String, String> map = new HashMap<String, String>();
	
	

	/**
	 * 读取赛轮配置文件
	 * 
	 */
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH
				+ "cfgFile/sailun/saiLunDomainInfo.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);
			//获取到所有的陪配置信息
			Set<String> domainSt = props.stringPropertyNames();
			for (String str : domainSt) {
				if(str.startsWith("domain")){
					String domStr = props.getProperty(str);
					if(StringUtils.isNotBlank(domStr)){
						String[] domainIp = domStr.split("-");
						if(domainIp.length==2){
							map.put(domainIp[0], domainIp[1]);
						}
					}
				}
			}
			if(map.size()==0){
				log.error("没有读取到合法的domain：SaiLunAfterItem start()");
			}
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}
}
