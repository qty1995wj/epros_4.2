package com.jecn.webservice.zhongsh.flowpush.bean;

public class WBActionExecutionCondition {
	private String conditionGUID;
	private String actionGUID;
	private String condition;
	private String conditionType;

	public String getConditionGUID() {
		return conditionGUID;
	}

	public void setConditionGUID(String conditionGUID) {
		this.conditionGUID = conditionGUID;
	}

	public String getActionGUID() {
		return actionGUID;
	}

	public void setActionGUID(String actionGUID) {
		this.actionGUID = actionGUID;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getConditionType() {
		return conditionType;
	}

	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}
}
