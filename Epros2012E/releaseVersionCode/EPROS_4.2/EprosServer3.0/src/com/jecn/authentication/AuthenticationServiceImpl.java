package com.jecn.authentication;

import com.jecn.epros.bean.authetication.AuthenticationRequest;
import com.jecn.epros.bean.authetication.TokenAnalysis;
import com.jecn.epros.service.AuthenticationService;

public class AuthenticationServiceImpl implements AuthenticationService {

	@Override
	public TokenAnalysis analyze(String token) {
		return AuthenticationTokenUtils.analyzeToken(token);
	}

	@Override
	public String authentication(AuthenticationRequest request) {
		return AuthenticationTokenUtils.generateToken(request);
	}

	@Override
	public void destroy(String token) {
		// TODO wait to decide to implement in database or memory
	}

}
