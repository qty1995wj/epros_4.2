package epros.designer.gui.standard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnLookSetDialog;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.process.ProcessChooseDialog;
import epros.designer.gui.system.Callback;
import epros.designer.gui.system.ResourceSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnDataImportUtil;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * @author yxw 2012-11-8
 * @description：标准树右键菜单
 */
public class StadandardResourceTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(StadandardResourceTreeMenu.class);
	/** 新建标准目录 */
	private JMenuItem newStandardItem = new JMenuItem(JecnProperties.getValue("newStandardDir"), new ImageIcon(
			"images/menuImage/newStandardDir.gif"));
	/** 导入数据 */
	private JMenuItem importItem = new JMenuItem(JecnProperties.getValue("importFile"), new ImageIcon(
			"images/menuImage/import.gif"));
	/** 新建条款 */
	private JMenuItem newStanClauseItem = new JMenuItem(JecnProperties.getValue("newStanClause"), new ImageIcon(
			"images/menuImage/newStanClause.gif"));
	/** 新建条款要求 */
	private JMenuItem newStanClauseRequireItem = new JMenuItem(JecnProperties.getValue("newStanClauseRequire"),
			new ImageIcon("images/menuImage/newStanClauseRequire.gif"));
	/** 编辑条款要求 */
	private JMenuItem editStanClauseRequireItem = new JMenuItem(JecnProperties.getValue("editStanClauseRequire"),
			new ImageIcon("images/menuImage/editStadandard.gif"));
	/** 编辑条款 */
	private JMenuItem editClauseItem = new JMenuItem(JecnProperties.getValue("editClause"), new ImageIcon(
			"images/menuImage/property.gif"));
	/** 节点排序 */
	private JMenuItem nodeSortItem = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon(
			"images/menuImage/nodeSort.gif"));
	/** 节点移动 */
	private JMenuItem nodeMoveItem = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon(
			"images/menuImage/nodeMove.gif"));
	/** 刷新 */
	private JMenuItem refreshItem = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon(
			"images/menuImage/refresh.gif"));
	/** 更新标准文件 */
	private JMenuItem updateStandardFileItem = new JMenuItem(JecnProperties.getValue("updateStandardFile"));
	/** 重命名 */
	private JMenuItem renameItem = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon(
			"images/menuImage/rename.gif"));
	/** 查阅权限 */
	private JMenuItem lookItem = new JMenuItem(JecnProperties.getValue("lookPermissions"), new ImageIcon(
			"images/menuImage/lookPermissions.gif"));
	/** 删除 */
	private JMenuItem deleteItem = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon(
			"images/menuImage/delete.gif"));
	/** 关联流程 */
	private JMenuItem setRalationFlowItem = new JMenuItem(JecnProperties.getValue("createProcessStandard"),
			new ImageIcon("images/menuImage/createProcessStandard.gif"));
	/** 关联文件 */
	private JMenuItem uploadStadandardFileItem = new JMenuItem(JecnProperties.getValue("ralationFile"), new ImageIcon(
			"images/menuImage/ralationFile.gif"));
	/** 标准文件属性 */
	private JMenuItem stadandardFileAttributeItem = new JMenuItem(JecnProperties.getValue("stadandardFileAttribute"));

	private Separator separatorOne = new Separator();
	private Separator separatorTwo = new Separator();
	/** 单选节点 */
	private JecnTreeNode selectNode = null;
	private JecnTree jTree = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> selectNodes = null;

	public StadandardResourceTreeMenu(JecnTree jTree, List<JecnTreeNode> selectNodes, int selectMutil) {
		this.jTree = jTree;
		this.selectNodes = selectNodes;
		if (0 == selectMutil) {
			selectNode = selectNodes.get(0);
		}
		if (selectNodes.size() == 1) {
			// 单选设置menu
			this.singleSelect();
		} else {
			// 多选设置menu
			this.mutilSelect();
		}

		this.add(importItem);// 导入
		this.add(newStandardItem);// 新建标准目录
		this.add(newStanClauseItem);// 新建条款
		this.add(newStanClauseRequireItem);// 新建条款要求
		this.add(uploadStadandardFileItem);// 关联文件
		this.add(setRalationFlowItem);// 关联流程
		this.add(updateStandardFileItem);// 更新标准文件
		this.add(editStanClauseRequireItem);// 编辑
		this.add(separatorOne);

		this.add(renameItem);// 重命名
		this.add(nodeSortItem);// 节点排序
		this.add(nodeMoveItem);// 节点移动
		this.add(refreshItem);// 刷新
		this.add(deleteItem);// 删除
		this.add(separatorTwo);

		this.add(lookItem);// 查阅权限
		this.add(editClauseItem);// 编辑条款
		this.add(stadandardFileAttributeItem);

		menuItemActionInit();
	}

	private void menuItemActionInit() {
		importItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				importItemAction();
			}
		});
		newStandardItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newStandardItemAction();
			}
		});
		newStanClauseItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AddStanClauseDialog s = new AddStanClauseDialog(selectNode, jTree);
				s.setVisible(true);
			}
		});
		newStanClauseRequireItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AddStanClauseRequireDialog s = new AddStanClauseRequireDialog(selectNode, jTree);
				s.setVisible(true);
			}
		});
		editStanClauseRequireItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EditStanClauseRequireDialog d = new EditStanClauseRequireDialog(selectNode, jTree);
				d.setVisible(true);
			}
		});
		editClauseItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EditStanClauseDialog d = new EditStanClauseDialog(selectNode, jTree);
				d.setVisible(true);
			}
		});
		updateStandardFileItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateStandardFileItemAction();
			}
		});

		nodeSortItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeSortItemAction();
			}
		});
		nodeMoveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeMoveItemAction();
			}
		});
		refreshItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshItemAction();
			}
		});

		renameItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renameItemAction();
			}
		});

		lookItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deptLookItemAction();
			}
		});

		deleteItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteItemAction();
			}
		});

		setRalationFlowItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setRalationFlowItemAction();
			}
		});

		uploadStadandardFileItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				uploadStadandardFileItemAction();
			}
		});
		stadandardFileAttributeItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				stadandardFileAttributeAction();
			}
		});
	}

	protected void importItemAction() {
		JecnDataImportUtil.importDataByType("standard", selectNode, new Callback<Void>() {
			@Override
			public Void call(Object obj) {
				refreshItemAction();
				return null;
			}
		});
	}

	private void stadandardFileAttributeAction() {
		StadandardFileAttributeDialog stadandardFileAttributeDialog = new StadandardFileAttributeDialog(selectNode);
		stadandardFileAttributeDialog.setVisible(true);
	}

	private void newStandardItemAction() {
		AddStandarDialog addStandarDialog = new AddStandarDialog(selectNode, jTree);
		addStandarDialog.setVisible(true);
	}

	private void nodeSortItemAction() {
		ResourceSortDialog rs = new ResourceSortDialog(selectNode, jTree);
		rs.setVisible(true);
	}

	private void updateStandardFileItemAction() {
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		case standard:
			FileChooseDialog fileChooseDialog = new FileChooseDialog(new ArrayList<JecnTreeBean>());
			fileChooseDialog.removeEmptyBtn();
			fileChooseDialog.setSelectMutil(false);
			fileChooseDialog.setVisible(true);
			if (fileChooseDialog.isOperation()) {
				int count = fileChooseDialog.getResultTable().getRowCount();
				if (count == 1) {
					long fileId = Long.valueOf((String) fileChooseDialog.getResultTable().getValueAt(0, 0));
					String fileName = (String) fileChooseDialog.getResultTable().getValueAt(0, 1);
					try {
						if (JecnTreeCommon.validateRepeatNameEidt(fileName, selectNode)) {
							JecnOptionPane.showMessageDialog(null, "\"" + fileName + "\""
									+ JecnProperties.getValue("isExist"));
							return;
						}
						ConnectionPool.getStandardAction().updateStandardFile(selectNode.getJecnTreeBean().getId(),
								fileId, fileName, JecnConstants.getUserId());
						selectNode.getJecnTreeBean().setRelationId(fileId);
						JecnTreeCommon.reNameNode(jTree, selectNode, fileName);
						JecnTreeCommon.selectNode(jTree, selectNode);
						JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("updateSuccess"));
					} catch (Exception e) {
						log.error("StadandardResourceTreeMenu updateStandardFileItemAction is error", e);
					}
				}

			}

			break;
		case standardProcess:// 标准流程
		case standardProcessMap:
			ProcessChooseDialog processChooseDialog = new ProcessChooseDialog(new ArrayList<JecnTreeBean>());
			processChooseDialog.setSelectMutil(false);
			processChooseDialog.removeEmptyBtn();
			processChooseDialog.setVisible(true);
			if (processChooseDialog.isOperation()) {
				int count = processChooseDialog.getResultTable().getRowCount();
				if (count == 1) {
					JecnTreeBean treeBean = processChooseDialog.getListJecnTreeBean().get(0);
					long fileId = Long.valueOf((String) processChooseDialog.getResultTable().getValueAt(0, 0));
					String fileName = treeBean.getName();
					try {
						if (JecnTreeCommon.validateRepeatNameEidt(fileName, selectNode)) {
							JecnOptionPane.showMessageDialog(null, "\"" + fileName + "\""
									+ JecnProperties.getValue("isExist"));
							return;
						}
						ConnectionPool.getStandardAction().updateStandardProcess(selectNode.getJecnTreeBean().getId(),
								fileId, fileName, treeBean.getTreeNodeType(), JecnConstants.getUserId());
						selectNode.getJecnTreeBean().setRelationId(treeBean.getId());
						if (treeBean.getTreeNodeType() == TreeNodeType.process) {
							selectNode.getJecnTreeBean().setTreeNodeType(TreeNodeType.standardProcess);
						} else if (treeBean.getTreeNodeType() == TreeNodeType.processMap) {
							selectNode.getJecnTreeBean().setTreeNodeType(TreeNodeType.standardProcessMap);
						}
						JecnTreeCommon.reNameNode(jTree, selectNode, fileName);
					} catch (Exception e) {
						log.error("StadandardResourceTreeMenu updateStandardFileItemAction is error", e);
					}
				}

			}

			break;

		default:
			break;
		}

	}

	private void nodeMoveItemAction() {
		if (!JecnDesignerCommon.validateRepeatNodesName(selectNodes)) {
			return;
		}
		StandardMoveChooseDialog smc = new StandardMoveChooseDialog(selectNodes, jTree);
		smc.setVisible(true);
	}

	private void refreshItemAction() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getStandardAction().getChildStandards(id, JecnConstants.projectId);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("StadandardResourceTreeMenu refreshItemAction is error", ex);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
	}

	private void renameItemAction() {
		EditStandarNameDialog editStandarNameDialog = new EditStandarNameDialog(selectNode, jTree);
		editStandarNameDialog.setVisible(true);
	}

	private void deptLookItemAction() {

		try {
			LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
					selectNode.getJecnTreeBean().getId(), 2);

			JecnLookSetDialog d = new JecnLookSetDialog(lookPopedomBean, selectNode, 2, false);
			d.setVisible(true);
		} catch (Exception e) {
			log.error("StadandardResourceTreeMenu deptLookItemAction is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
	}

	private void deleteItemAction() {
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode node : selectNodes) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		if (listIds.size() == 0) {
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			ConnectionPool.getStandardAction().deleteStandard(listIds, JecnConstants.projectId,
					JecnConstants.getUserId());
			/** 删除节点 */
			JecnTreeCommon.removeNodes(jTree, selectNodes);
		} catch (Exception e) {
			log.error("StadandardResourceTreeMenu deleteItemAction is error", e);
		}

	}

	private void setRalationFlowItemAction() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		ProcessChooseDialog d = new ProcessChooseDialog(list);
		d.setVisible(true);
		if (d.isOperation() && list.size() > 0) {
			List<StandardBean> listStandardBean = new ArrayList<StandardBean>();
			StandardBean standardBean = null;
			int i = 0;
			for (JecnTreeBean b : list) {

				// 关联流程重复判断
				TreeNodeType nodeType = b.getTreeNodeType() == TreeNodeType.process ? TreeNodeType.standardProcess
						: TreeNodeType.standardProcessMap;
				if (JecnTreeCommon.validateRepeatNameAdd(selectNode, b.getName(), nodeType)) {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("processHavedRelation"));
					return;
				}
				standardBean = new StandardBean();
				standardBean.setCriterionClassName(b.getName());
				standardBean.setPreCriterionClassId(selectNode.getJecnTreeBean().getId());
				standardBean.setProjectId(JecnConstants.projectId);
				standardBean.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(selectNode) + i));
				standardBean.setRelationId(b.getId());
				standardBean.setStanType(b.getTreeNodeType() == TreeNodeType.process ? 2 : 3);
				standardBean.setIsPublic(1L);
				standardBean.setCreatePeopleId(JecnConstants.getUserId());
				standardBean.setUpdatePeopleId(JecnConstants.getUserId());
				listStandardBean.add(standardBean);
				i++;

			}
			try {
				ConnectionPool.getStandardAction().addRelationFlow(listStandardBean, JecnConstants.getUserId());
				List<JecnTreeBean> listNode = ConnectionPool.getStandardAction().getChildStandards(
						selectNode.getJecnTreeBean().getId(), JecnConstants.projectId);
				JecnTreeCommon.refreshNode(selectNode, listNode, jTree);
				JecnTreeCommon.autoExpandNode(jTree, selectNode);
			} catch (Exception e1) {
				log.error("StadandardResourceTreeMenu setRalationFlowItemAction is error", e1);
			}
		}
	}

	private void uploadStadandardFileItemAction() {
		UploadStandardFileDialog uploadStandardFileDialog = new UploadStandardFileDialog(selectNode, jTree);
		uploadStandardFileDialog.setVisible(true);
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		if (JecnTreeCommon.isAuthNodes(selectNodes)) {
			// 删除
			deleteItem.setVisible(true);
			// 移动
			nodeMoveItem.setVisible(true);
		}

	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		int authType = JecnTreeCommon.isAuthNode(selectNode);
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case standardRoot:
			// 刷新
			refreshItem.setVisible(true);
			if (JecnDesignerCommon.isAdmin()) {
				importItem.setVisible(true);
				separatorOne.setVisible(true);
				// 新建标准
				newStandardItem.setVisible(true);
				// 搜索
				// searchItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
			}

			break;
		case standardDir:
			// authType = JecnTreeCommon.isAuthNode(selectNode);
			// 刷新
			refreshItem.setVisible(true);
			if (authType == 1 || authType == 2) {
				importItem.setVisible(true);
				separatorOne.setVisible(true);
				separatorTwo.setVisible(true);
				// 新建标准
				newStandardItem.setVisible(true);
				// 新建条款
				newStanClauseItem.setVisible(true);

				// 查阅权限
				lookItem.setVisible(true);
				// 设置关联流程
				setRalationFlowItem.setVisible(true);
				// 关联文件
				uploadStadandardFileItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
				if (authType == 2) {
					// 重命名
					renameItem.setVisible(true);
					// 节点移动
					nodeMoveItem.setVisible(true);
					// 删除
					deleteItem.setVisible(true);
				}
			}

			break;
		case standard:
			if (authType == 1 || authType == 2) {
				updateStandardFileItem.setVisible(true);
				// 节点移动
				nodeMoveItem.setVisible(true);
				if (authType == 2) {
					// 删除
					deleteItem.setVisible(true);
				}
				stadandardFileAttributeItem.setVisible(true);
			}
			break;
		case standardProcess:
		case standardProcessMap:
			if (authType == 1 || authType == 2) {
				updateStandardFileItem.setVisible(true);
				// 节点移动
				nodeMoveItem.setVisible(true);
				if (authType == 2) {
					// 删除
					deleteItem.setVisible(true);
				}
			}
			break;
		case standardClause:
			// 刷新
			refreshItem.setVisible(true);
			if (authType == 1 || authType == 2) {
				newStanClauseRequireItem.setVisible(true);
				// 节点移动
				nodeMoveItem.setVisible(true);
				if (authType == 2) {
					// 删除
					deleteItem.setVisible(true);
				}
				editClauseItem.setVisible(true);
			}
			break;
		case standardClauseRequire:
			if (authType == 1 || authType == 2) {
				// 编辑
				editStanClauseRequireItem.setVisible(true);
				// 节点移动
				nodeMoveItem.setVisible(true);
				if (authType == 2) {
					// 删除
					deleteItem.setVisible(true);
				}
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 初始化
	 */
	private void init() {
		importItem.setVisible(false);
		/** 新建标准 */
		newStandardItem.setVisible(false);
		newStanClauseItem.setVisible(false);// 新建条款
		newStanClauseRequireItem.setVisible(false);// 新建条款要求
		editStanClauseRequireItem.setVisible(false);// 编辑
		editClauseItem.setVisible(false);// 属性
		/** 节点排序 */
		nodeSortItem.setVisible(false);
		/** 节点移动 */
		nodeMoveItem.setVisible(false);
		/** 刷新 */
		refreshItem.setVisible(false);
		updateStandardFileItem.setVisible(false);
		/** 重命名 */
		renameItem.setVisible(false);
		/** 查阅权限(部门) */
		lookItem.setVisible(false);
		/** 查阅权限(岗位) */
		// positionLookItem.setVisible(false);
		/** 删除 */
		deleteItem.setVisible(false);
		/** 关联流程 */
		setRalationFlowItem.setVisible(false);
		/** 关联文件 */
		uploadStadandardFileItem.setVisible(false);
		// 标准文件属性
		stadandardFileAttributeItem.setVisible(false);
		separatorOne.setVisible(false);
		separatorTwo.setVisible(false);
	}

}
