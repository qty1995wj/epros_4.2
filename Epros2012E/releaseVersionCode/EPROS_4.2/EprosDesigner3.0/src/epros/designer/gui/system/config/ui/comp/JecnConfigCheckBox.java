package epros.designer.gui.system.config.ui.comp;

import javax.swing.JCheckBox;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

/**
 * 
 * 复选框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigCheckBox extends JCheckBox {
	/** 数据对象 */
	private JecnConfigItemBean itemBean = null;

	public JecnConfigCheckBox(String text) {
		super(text);
	}

	public JecnConfigItemBean getItemBean() {
		return itemBean;
	}

	public void setItemBean(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
	}

	/**
	 * 
	 * 设置数据对象并且获取Value字段值给此组件
	 * 
	 * @param itemBean
	 */
	public void setItemBeanAndValue(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
		initConfigCheckBoxData();
	}

	/**
	 * 
	 * value字段存储true/false值时
	 * 
	 * 设置设定值
	 * 
	 */
	public void initConfigCheckBoxData() {
		if (itemBean != null) {
			this.setSelected(JecnConfigContents.ITEM_AUTO_SAVE_YES
					.equals(itemBean.getValue()));
		}
	}

	/**
	 * 
	 * value字段存储true/false值时
	 * 
	 * 设置默认值
	 * 
	 */
	public void intConfigCheckBoxDefaultData() {
		if (itemBean != null) {
			this.setSelected(JecnConfigContents.ITEM_AUTO_SAVE_YES
					.equals(itemBean.getDefaultValue()));
		}
	}
}
