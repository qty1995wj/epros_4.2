package wordxml.element.dom.table;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.ShdBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.IBody;
import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.paragraph.Paragraph;

/**
 * 操作表格类
 */
public class Table implements IBodyElement {

	/*** 表格属性对象 *****/
	private TableBean tableBean;

	private List<TableRow> tableRows = new ArrayList<TableRow>();
	/**** 表格列宽数组 *****/
	private float[] cellWidths;
	/** 所属对象 */
	private IBody parent;

	public Table(float[] cellWidths) {
		this.cellWidths = cellWidths;
	}

	public Table(TableBean tableBean, float[] cellWidths) {
		this.tableBean = tableBean;
		this.cellWidths = cellWidths;
	}

	/**
	 * 创建表格行
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:17:12
	 * @return
	 */
	public TableRow createTableRow() {
		TableRow tr = new TableRow();
		tr.setTable(this);
		this.tableRows.add(tr);
		return tr;
	}

	/**
	 * 按照换行符截取字符串数据
	 * 
	 * @param content
	 * @return
	 */
	public String[] getTcContents(String content) {
		if (content == null) {
			return new String[] { "" };
		}
		String[] strs = content.split("\n");
		if (strs.length == 0) {
			return new String[] { "" };
		}
		return strs;
	}

	/**
	 * 创建多行数据
	 * 
	 * @param rowData
	 * @param pBean
	 */
	public void createTableRow(List<String[]> rowData, ParagraphBean pBean, float rowHeight) {
		// 创建行
		for (String[] tr : rowData) {
			TableRow tabRow = createTableRow();
			tabRow.setHeight(rowHeight);
			// 创建列
			for (String tc : tr) {
				TableCell cell = tabRow.createTableCell();
				// 创建文本
				for (String text : getTcContents(tc)) {
					cell.createParagraph(text, pBean);
				}
			}
		}
	}

	/**
	 * 创建多行数据
	 * 
	 * @param rowData
	 * @param pBean
	 */
	public void createTableRow(List<String[]> rowData, ParagraphBean pBean) {
		// 创建行
		for (String[] tr : rowData) {
			createTableRow(tr, pBean);
		}
	}

	/**
	 * 创建一行数据
	 * 
	 * @param rowData
	 * @param pBean
	 */
	public TableRow createTableRow(String[] rowData, ParagraphBean pBean) {
		// 创建行
		TableRow tabRow = createTableRow();
		// 创建列
		for (String tc : rowData) {
			TableCell cell = tabRow.createTableCell();
			// 创建文本
			for (String text : getTcContents(tc)) {
				cell.createParagraph(text, pBean);
			}
		}
		return tabRow;
	}

	/**
	 * 创建表格行 设置新的宽度
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:17:12
	 * @return
	 */
	public TableRow createTableRow(float height) {
		TableRow tr = createTableRow();
		tr.setHeight(height);
		return tr;
	}

	/**
	 * 创建表格行 设置新的宽度
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:17:12
	 * @return
	 */
	public TableRow createTableRow(float height, boolean isHeader) {
		TableRow tr = createTableRow();
		tr.setHeight(height);
		tr.setHeader(isHeader);
		return tr;
	}

	/**
	 * 获取指定行
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:17:39
	 * @param pos
	 *            位置
	 * @return
	 */
	public TableRow getRow(int pos) {
		return (pos < 0 || pos >= getRowCount()) ? null : tableRows.get(pos);
	}

	/**
	 * 获取行对象集合
	 * 
	 * @author weidp
	 * @date 2014-10-15 上午10:19:25
	 * @return
	 */
	public List<TableRow> getRows() {
		return Collections.unmodifiableList(tableRows);
	}

	/**
	 * 获取总行数
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:20:55
	 * @return
	 */
	public int getRowCount() {
		return tableRows.size();
	}

	/**
	 * 获取表格样式Bean
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:21:48
	 * @return
	 */
	public TableBean getTableBean() {
		return tableBean;
	}

	/**
	 * 设置表格样式Bean
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:22:07
	 * @param tableBean
	 */
	public void setTableBean(TableBean tableBean) {
		this.tableBean = tableBean;
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setCellStyle(int pos, ParagraphBean pBean) {
		for (TableRow tr : this.getRows()) {
			TableCell tc = tr.getCell(pos);
			if (tc != null) {
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 设置表格的某一列的文本添加外部文件连接，
	 * 
	 * @param tab
	 * @param pos
	 * @param prefix
	 *            前缀
	 */
	public void setCellExtFile(int rows, int pos, String prefix, ParagraphBean pBean) {
		if (prefix == null || "".equals(prefix)) {
			return;
		}
		TableCell tc = this.getRows().get(rows).getCell(pos);
		if (tc != null) {
			for (Paragraph p : tc.getParagraphs()) {
				p.setExtFilePath(prefix);
				p.setParagraphBean(pBean);
			}
		}
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setCellStyle(int pos, ParagraphBean pBean, Valign vAlign) {
		for (TableRow tr : this.getRows()) {
			TableCell tc = tr.getCell(pos);
			if (tc != null) {
				tc.setvAlign(vAlign);
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 仅用于联合电子的表格特殊 设置表格某一列的文本样式 排除第几行的
	 * 
	 * @param pos
	 * @param pBean
	 * @param vAlign
	 * @param expRow
	 */
	public void setCellStyle(int pos, ParagraphBean pBean, Valign vAlign, int expRow) {
		for (int i = 0; i < this.getRows().size(); i++) {
			if (i == expRow) {
				continue;
			}
			TableRow tr = this.getRow(i);
			TableCell tc = tr.getCell(pos);
			if (tc != null) {
				tc.setvAlign(vAlign);
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setCellStyle(int pos, Valign vAlign) {
		for (TableRow tr : this.getRows()) {
			TableCell tc = tr.getCell(pos);
			if (tc != null) {
				tc.setvAlign(vAlign);
			}
		}
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setCellStyle(int pos, ParagraphBean pBean, boolean hasBgColor) {
		for (TableRow tr : this.getRows()) {
			TableCell tc = tr.getCell(pos);
			if (hasBgColor) {
				tc.getCellBean().setShdBean(ShdBean.getInstance());
			}
			if (tc != null) {
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setRowStyle(int pos, ParagraphBean pBean) {
		TableRow tr = this.getRow(pos);
		if (tr != null) {
			for (TableCell tc : tr.getCells()) {
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setRowStyle(int pos, ParagraphBean pBean, Valign valign) {
		TableRow tr = this.getRow(pos);
		if (tr != null) {
			for (TableCell tc : tr.getCells()) {
				tc.setvAlign(valign);
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setRowStyle(int pos, Valign valign) {
		TableRow tr = this.getRow(pos);
		if (tr != null) {
			for (TableCell tc : tr.getCells()) {
				tc.setvAlign(valign);
			}
		}
	}

	/**
	 * 设置表格的某一列的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setRowStyle(int pos, ParagraphBean pBean, boolean hasBgColor) {
		TableRow tr = this.getRow(pos);
		if (tr != null) {
			for (TableCell tc : tr.getCells()) {
				if (hasBgColor) {
					tc.getCellBean().setShdBean(ShdBean.getInstance());
				}
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 设置表格的某一行的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setRowStyle(int pos, ParagraphBean pBean, boolean hasBgColor, boolean isHeader) {
		TableRow tr = this.getRow(pos);
		tr.setHeader(isHeader);
		if (tr != null) {
			for (TableCell tc : tr.getCells()) {
				if (hasBgColor) {
					tc.getCellBean().setShdBean(ShdBean.getInstance());
				}
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	/**
	 * 设置行的边框
	 * 
	 * @param pos
	 * @param border
	 * @param width
	 */
	public void setRowBorder(int pos, String border, float width) {
		if (!Arrays.asList(new String[] { "Left", "Top", "Right", "Bottom" }).contains(border)) {
			return;
		}
		TableRow tr = this.getRow(pos);
		Method md = null;
		if (tr != null) {
			for (TableCell tc : tr.getCells()) {
				try {
					md = tc.getCellBean().getClass().getDeclaredMethod("setBorder" + border, float.class);
					md.invoke(tc.getCellBean(), width);
				} catch (Exception e) {
					// 一般情况下不可能抛出异常
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 设置表格的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setParagraphStyle(ParagraphBean pBean) {
		for (TableRow tr : this.getRows()) {
			for (TableCell tc : tr.getCells()) {
				for (Paragraph p : tc.getParagraphs()) {
					p.setParagraphBean(pBean);
				}
			}
		}
	}

	public IBody getParent() {
		return parent;
	}

	public void setParent(IBody parent) {
		this.parent = parent;
	}

	public float[] getColWidths() {
		return cellWidths;
	}

	public void setColWidths(float[] colWidths) {
		this.cellWidths = colWidths;
	}

	public void setValign(Valign valign) {

		List<TableRow> rows = this.getRows();
		if (rows == null) {
			return;
		}
		for (TableRow row : rows) {
			row.setValign(valign);
		}

	}

	/**
	 * 设置单元格对齐方式,只能在Table已经填充完毕Cell的时候调用才有效果
	 * 
	 * @param valign
	 * @param height
	 */
	public void setCellValignAndHeight(Valign valign, float height) {
		for (TableRow row : this.getRows()) {
			row.setHeight(height);
			for (TableCell cell : row.getCells()) {
				cell.setvAlign(valign);
			}
		}
	}

	public void setCellValign(Valign valign) {
		setCellValignAndHeight(valign, 1F);
	}

	public void setColStyle(int colIndex, ParagraphBean paragraphBean, Valign valign) {
		for (TableRow row : this.getRows()) {
			TableCell cell = row.getCell(0);
			cell.setvAlign(valign);
			List<Paragraph> paragraphs = cell.getParagraphs();
			if (paragraphs != null && !paragraphs.isEmpty()) {
				for (Paragraph p : paragraphs) {
					p.setParagraphBean(paragraphBean);
				}
			}
		}
	}

	/**
	 * 设置表格的某一行的文本样式
	 * 
	 * @param tab
	 * @param pos
	 * @param pBean
	 */
	public void setColBackGround(int... cols) {
		List<TableRow> rows = this.getRows();
		for (TableRow row : rows) {
			for (int col : cols) {
				if (row.getCells().size() - 1 >= col) {
					row.getCell(col).getCellBean().setShdBean(ShdBean.getInstance());
				}
			}
		}
	}

	public void setRowHight(float h) {
		List<TableRow> rows = this.getRows();
		if (rows != null) {
			for (TableRow row : rows) {
				row.setHeight(h);
			}
		}

	}

}
