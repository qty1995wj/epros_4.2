/*
 * FileImage.java
 *文档
 * Created on 2008年11月12日, 下午1:52
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.util.JecnUIUtil;

/**
 * 文档
 * 
 * @author zhangxh
 * 
 * @date： 日期：Mar 22, 2012 时间：10:05:07 AM
 */
public class FileImage extends JecnBaseFigurePanel {
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int x5;
	private int x6;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int y5;
	private int y6;
	/** Double 的最大可能值 */
	private double min = Double.MAX_VALUE;
	private double max = Double.MIN_VALUE;
	/** 曲线横向坐标集合 */
	private List<Integer> xList = null;
	/** 曲线纵向坐标集合 */
	private List<Integer> yList = null;
	/** 图形X方向所有坐标集合 */
	private int[] xx = null;
	/** 图形Y方向所有坐标集合 */
	private int[] yy = null;

	public FileImage(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	/**
	 * 平滑曲线,多个点的变化
	 * 
	 * @param x
	 *            横向坐标集合
	 * @param y
	 *            纵向坐标集合
	 * @param value
	 * @return
	 */
	public double lagrange(double x[], double y[], double value) {
		double sum = 0;
		double L;
		for (int i = 0; i < x.length; i++) {
			L = 1;
			for (int j = 0; j < x.length; j++) {
				if (j != i) {
					L = L * (value - x[j]) / (x[i] - x[j]);
				}
			}
			sum = sum + L * y[i];
		}
		return sum;
	}

	/**
	 * 画图
	 * 
	 */
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		xList = new ArrayList<Integer>();
		yList = new ArrayList<Integer>();
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = userWidth;
			y1 = 0;

			x2 = 0;
			y2 = y1;

			x3 = x2;
			y3 = 7 * userHeight / 8;

			x4 = userWidth / 4;
			y4 = userHeight;

			x5 = 3 * userWidth / 4;
			y5 = 3 * userHeight / 4;

			x6 = userWidth;
			y6 = 7 * userHeight / 8;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = userWidth;
			y1 = 0;

			x2 = userWidth / 8;
			y2 = y1;

			x3 = 0;
			y3 = userHeight / 4;

			x4 = userWidth / 4;
			y4 = 3 * userHeight / 4;

			x5 = userWidth / 8;
			y5 = userHeight;

			x6 = userWidth;
			y6 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = userWidth;
			y1 = userHeight / 8;

			x2 = 3 * userWidth / 4;
			y2 = 0;

			x3 = userWidth / 4;
			y3 = userHeight / 4;

			x4 = 0;
			y4 = userHeight / 8;

			x5 = 0;
			y5 = userHeight;

			x6 = userWidth;
			y6 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = 7 * userWidth / 8;
			y1 = 0;

			x2 = 0;
			y2 = y1;

			x3 = x2;
			y3 = userHeight;

			x4 = 7 * userWidth / 8;
			y4 = userHeight;

			x5 = userWidth;
			y5 = 6 * userHeight / 8;

			x6 = 6 * userWidth / 8;
			y6 = 1 * userHeight / 4;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			double[] X = { x3, x4, x5, x6 };
			double[] Y = { y3, y4, y5, y6 };
			for (int i = 0; i < X.length; i++) {
				if (X[i] > max) {
					max = X[i];
				}
				if (X[i] < min) {
					min = X[i];
				}
			}
			for (double x = min; x <= max; x = x + 0.01) {
				xList.add((int) x);
				yList.add((int) lagrange(X, Y, x));
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = x1;
			yy[0] = y1;
			xx[1] = x2;
			yy[1] = y2;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}

			g2d.setPaint(flowElementData.getBodyColor());
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.drawPolygon(xx, yy, xx.length);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			double[] X = { x5, x4, x3, x2 };
			double[] Y = { y5, y4, y3, y2 };
			for (int i = 0; i < Y.length; i++) {
				if (Y[i] > max) {
					max = Y[i];
				}
				if (Y[i] < min) {
					min = Y[i];
				}
			}
			for (double y = min; y <= max; y = y + 0.01) {
				xList.add((int) lagrange(Y, X, y));
				yList.add((int) y);
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = x1;
			yy[0] = y1;
			xx[yList.size() + 1] = x6;
			yy[yList.size() + 1] = y6;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 1] = xList.get(i);
				yy[i + 1] = yList.get(i);
			}

			g2d.setPaint(flowElementData.getBodyColor());
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.drawPolygon(xx, yy, xx.length);

		} else if (flowElementData.getCurrentAngle() == 180.0) {

			double[] X = { x4, x3, x2, x1 };
			double[] Y = { y4, y3, y2, y1 };
			for (int i = 0; i < X.length; i++) {
				if (X[i] > max) {
					max = X[i];
				}
				if (X[i] < min) {
					min = X[i];
				}
			}
			for (double x = min; x <= max; x = x + 0.01) {
				xList.add((int) x);
				yList.add((int) lagrange(X, Y, x));
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[xList.size()] = x6;
			yy[xList.size()] = y6;
			xx[xList.size() + 1] = x5;
			yy[xList.size() + 1] = y5;

			for (int i = 0; i < xList.size(); i++) {
				xx[i] = xList.get(i);
				yy[i] = yList.get(i);
			}
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.drawPolygon(xx, yy, xx.length);

		} else if (flowElementData.getCurrentAngle() == 270.0) {
			double[] X = { x1, x6, x5, x4 };
			double[] Y = { y1, y6, y5, y4 };
			double min = Double.MAX_VALUE;
			double max = Double.MIN_VALUE;
			for (int i = 0; i < Y.length; i++) {
				if (Y[i] > max) {
					max = Y[i];
				}
				if (Y[i] < min) {
					min = Y[i];
				}
			}
			for (double y = min; y <= max; y = y + 0.01) {
				xList.add((int) lagrange(Y, X, y));
				yList.add((int) y);
			}
			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = x3;
			yy[0] = y3;
			xx[1] = x2;
			yy[1] = y2;
			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}

			g2d.setPaint(flowElementData.getBodyColor());
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.drawPolygon(xx, yy, xx.length);
		}
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintFileImageTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintFileImageTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 * @param g2d
	 * @param shadowCount
	 *            顶层缩进榜值
	 */
	private void paintFileImageTop(Graphics2D g2d, int shadowCount) {
		// 画图获取顶层画图组装数据层
		getListForTopPaint(g2d, shadowCount);
		// 顶层 原图，榜值均为零
		g2d.fillPolygon(xx, yy, xx.length);
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawPolygon(xx, yy, xx.length);
	}

	/**
	 * 画图获取顶层画图组装数据层
	 * 
	 * @param g2d
	 * @param shadowCount
	 */
	private void getListForTopPaint(Graphics2D g2d, int shadowCount) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			double[] X = new double[] { x3, x4 - shadowCount, x5 - shadowCount, x6 - shadowCount };
			double[] Y = new double[] { y3 - shadowCount, y4 - shadowCount, y5 - shadowCount, y6 - shadowCount };
			// 获取数组
			getXYList(0, X, Y);
			xx[0] = x1 - shadowCount;
			yy[0] = y1;
			xx[1] = x2;
			yy[1] = y2;
			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			double[] X = new double[] { x5 - shadowCount, x4 - shadowCount, x3, x2 };
			double[] Y = new double[] { y5 - shadowCount, y4 - shadowCount, y3 - shadowCount, y2 };
			// 获取数组
			getXYList(1, X, Y);

			xx[0] = x1 - shadowCount;
			yy[0] = y1;
			xx[yList.size() + 1] = x6 - shadowCount;
			yy[yList.size() + 1] = y6 - shadowCount;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 1] = xList.get(i);
				yy[i + 1] = yList.get(i);
			}
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			double[] X = new double[] { x4, x3 - shadowCount, x2 - shadowCount, x1 - shadowCount };
			double[] Y = new double[] { y4, y3, y2, y1 };
			// 获取数组
			getXYList(0, X, Y);

			xx[xList.size()] = x6 - shadowCount;
			yy[xList.size()] = y6 - shadowCount;
			xx[xList.size() + 1] = x5;
			yy[xList.size() + 1] = y5 - shadowCount;

			for (int i = 0; i < xList.size(); i++) {
				xx[i] = xList.get(i);
				yy[i] = yList.get(i);
			}
		} else if (flowElementData.getCurrentAngle() == 270.0) {// 阴影
			double[] X = new double[] { x1 - shadowCount, x6 - 4, x5 - shadowCount, x4 - shadowCount };
			double[] Y = new double[] { y1, y6 - shadowCount, y5 - shadowCount, y4 - shadowCount };
			// 获取数组
			getXYList(1, X, Y);
			xx[0] = x3;
			yy[0] = y3 - shadowCount;
			xx[1] = x2;
			yy[1] = y2;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}
		}
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DFigure(Graphics2D g2d) {
		paint3DEventLow(g2d, 0);
	}

	/**
	 * 3D和阴影顶层填充
	 */
	public void paintShadowAnd3DFigure(Graphics2D g2d) {
		// 阴影
//		paintShadows(g2d);
//		addShadowPanel();
		// 顶层
		paint3DEventLow(g2d, 0);
	}

	private void paint3DEventLow(Graphics2D g2d, int indent3Dvalue) {
		// 画图获取顶层画图组装数据层
		getListForTopPaint(g2d, indent3Dvalue);
		// 顶层
		paintBackColor(g2d);// 颜色填充,渐变色
		g2d.fillPolygon(xx, yy, xx.length);
		// 底层,3D效果
		int gw = 3 * 2;// 边缘渐变
		for (int i = gw; i >= 2; i -= 2) {
			float pct = (float) (gw - i) / (gw - 1);
			paintLineColor(g2d);// 颜色填充,渐变色
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, pct));
			g2d.setStroke(new BasicStroke(i));
			g2d.drawPolygon(xx, yy, xx.length);
		}
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawPolygon(xx, yy, xx.length);
	}

	/**
	 * 
	 * 
	 * @param type
	 *            0:循环数组X 水平，1;循环数组Y 垂直
	 * @param X
	 * @param Y
	 */
	private void getXYList(int type, double[] X, double[] Y) {
		// 清空数组重新实例化数组
		xList = new ArrayList<Integer>();
		yList = new ArrayList<Integer>();
		// 重新获取double的最大可能值和最小可能值
		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;
		// 获取循环类型 0:循环数组X 水平，1;循环数组Y 垂直

		if (type == 1) {
			for (int i = 0; i < Y.length; i++) {
				if (Y[i] > max) {
					max = Y[i];
				}
				if (Y[i] < min) {
					min = Y[i];
				}
			}
			for (double y = min; y <= max; y = y + 0.01) {
				xList.add((int) lagrange(Y, X, y));
				yList.add((int) y);
			}
		} else {
			for (int i = 0; i < X.length; i++) {
				if (X[i] > max) {
					max = X[i];
				}
				if (X[i] < min) {
					min = X[i];
				}
			}
			for (double x = min; x <= max; x = x + 0.01) {
				xList.add((int) x);
				yList.add((int) lagrange(X, Y, x));
			}
		}
		xx = new int[xList.size() + 2];
		yy = new int[yList.size() + 2];
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			double[] X = { x3, x4, x5, x6 };
			double[] Y = { y3, y4, y5, y6 };
			// 获取数组
			getXYList(0, X, Y);
			xx[0] = x1;
			yy[0] = y1;
			xx[1] = x2;
			yy[1] = y2;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}

		} else if (flowElementData.getCurrentAngle() == 90.0) {
			double[] X = { x5, x4, x3, x2 };
			double[] Y = { y5, y4, y3, y2 };
			// 获取数组
			getXYList(1, X, Y);

			xx[0] = x1;
			yy[0] = y1;
			xx[yList.size() + 1] = x6;
			yy[yList.size() + 1] = y6;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 1] = xList.get(i);
				yy[i + 1] = yList.get(i);
			}

		} else if (flowElementData.getCurrentAngle() == 180.0) {
			double[] X = { x4, x3, x2, x1 };
			double[] Y = { y4, y3, y2, y1 };
			// 获取数组
			getXYList(0, X, Y);

			xx[xList.size()] = x6;
			yy[xList.size()] = y6;
			xx[xList.size() + 1] = x5;
			yy[xList.size() + 1] = y5;

			for (int i = 0; i < xList.size(); i++) {
				xx[i] = xList.get(i);
				yy[i] = yList.get(i);
			}
		} else if (flowElementData.getCurrentAngle() == 270.0) {// 阴影
			double[] X = { x1, x6, x5, x4 };
			double[] Y = { y1, y6, y5, y4 };
			// 获取数组
			getXYList(1, X, Y);
			xx[0] = x3;
			yy[0] = y3;
			xx[1] = x2;
			yy[1] = y2;
			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}
		}
		g2d.setPaint(new GradientPaint(0.0f, userHeight * 0.25f, flowElementData.getShadowColor(), 0.0f, userHeight,
				JecnUIUtil.getShadowBodyColor()));
		g2d.fillPolygon(xx, yy, xx.length);
	}
}
