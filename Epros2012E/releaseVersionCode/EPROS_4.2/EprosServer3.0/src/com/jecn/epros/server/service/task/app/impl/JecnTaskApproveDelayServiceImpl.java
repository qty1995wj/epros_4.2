package com.jecn.epros.server.service.task.app.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.system.impl.TimerTaskAction;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.task.IJecnTaskApproveDelayDao;
import com.jecn.epros.server.email.buss.SimpleUserEmailBean;
import com.jecn.epros.server.email.buss.ToolEmail;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.task.app.IJecnTaskApproveDelayService;
import com.jecn.epros.server.util.JecnUtil;

/**
 * @author huoyl
 */
public class JecnTaskApproveDelayServiceImpl extends
		AbsBaseService<JecnUser, Long> implements IJecnTaskApproveDelayService {
	/** 搁置任务记时的时间单位为天 每晚加1 */
	private static int delayTaskDay = 0;

	/** 任务超时提醒数据接口 */
	private IJecnTaskApproveDelayDao jecnTaskApproveDelayDao;

	public IJecnTaskApproveDelayDao getJecnTaskApproveDelayDao() {
		return jecnTaskApproveDelayDao;
	}

	public void setJecnTaskApproveDelayDao(
			IJecnTaskApproveDelayDao jecnTaskApproveDelayDao) {
		this.jecnTaskApproveDelayDao = jecnTaskApproveDelayDao;
	}

	@Override
	public void sendDelayEmail() throws Exception {

		// 未起用就直接退出
		if (!"1".equals(JecnUtil.getConfigItemValue(
				JecnContants.flowEndDateList, "proposalTaskTip"))) {
			delayTaskDay = 0;
			return;
		} else {
			delayTaskDay++;
		}

		// 获取搁置任务间隔提醒天数
		String configItemValue = JecnUtil.getConfigItemValue(JecnContants.flowEndDateList, "proposalTaskTipValue");
		int tipDay =configItemValue==null?-1:Integer.valueOf(configItemValue);
		if (tipDay <= 0) {
			return;
		}
		if (delayTaskDay >= tipDay)// 如果搁置任务的计时时间大于等于搁置任务配置的天数则发送邮件
		{
			delayTaskDay = 0;
			int taskDelayCount = jecnTaskApproveDelayDao.getTaskDelayCount();
			if (taskDelayCount <= 0) {
				return;
			}
			List<JecnUser> adminUserList = jecnTaskApproveDelayDao
					.getAdminUserList();
			if (adminUserList == null || adminUserList.size() == 0) {
				return;
			}
			if(!adminUserList.isEmpty()){
				// 发送邮件
				BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_DELAY);
				emailBuilder.setData(adminUserList);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				//TODO :EMAIL
				JecnUtil.saveEmail(buildEmail);
			}
			
		}
	}

}
