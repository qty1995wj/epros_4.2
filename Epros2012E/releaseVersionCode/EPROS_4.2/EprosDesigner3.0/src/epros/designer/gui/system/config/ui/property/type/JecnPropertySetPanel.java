package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ButtonCommendEnum;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKCancelJButtonPanel;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextArea;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnPropertySetJDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 流程配置-操作说明-属性配置主面板
 * 
 * @Time 2014-10-15
 * 
 */
public class JecnPropertySetPanel extends JecnAbstractPropertyBasePanel implements ActionListener {
	/** 配置界面 */
	private JecnPropertySetJDialog propertySetJDialog = null;
	/** 详细信息显示面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 显示岗位名称 */
	private JecnConfigCheckBox showPosNameBox = null;
	/** 显示活动输入、输出 */
	private JecnConfigCheckBox showActInOutBox = null;
	/** 确认取消按钮面板 */
	private JecnOKCancelJButtonPanel okCancelBtnAddItemPanel = null;
	/** 活动说明显示类型 */
	private JLabel typeLabel = null;
	/** 段落 */
	private JecnConfigRadioButton paragraphRadioButton = null;
	/** 表格 */
	private JecnConfigRadioButton tableRadioButton = null;

	/** 文本显示 */
	private JLabel fileLabel = null;
	/** 文本显示复选框 */
	private JecnConfigCheckBox fileShowCheckBox = null;

	/** 客户是否从流程图符中读取 */
	private JecnConfigCheckBox readerFlowCustomerBox = null;

	/** true:显示岗位 */
	private boolean isShowPos;
	/** 读取流程客户 */
	private boolean isShowReadCustomer;
	/** 是否显示操作说明每一项的提示信息 **/
	private boolean isShowOperationTip = true;
	/** 活动信息 **/
	private boolean isShowActivity;

	/** 文本显示 */
	private JLabel activeTimeLabel = null;
	/** 文本显示复选框 */
	private JecnConfigCheckBox activeTimeCheckBox = null;

	/** 活动文本输入输出图标显示（：隐藏：0;显示：1） */
	private JLabel activePutLogoLabel = null;
	/** 活动文本输入输出图标显示（：隐藏：0;显示：1） */
	private JecnConfigCheckBox activePutLogoCheckBox = null;
	protected JLabel chOperationTipLabel;
	protected JLabel enOperationTipLabel;
	protected JecnConfigTextArea chOperationTip;
	protected JecnConfigTextArea enOperationTip;
	protected JScrollPane chOperationPanel;
	protected JScrollPane enOperationPanel;

	public JecnPropertySetPanel(JecnAbtractBaseConfigDialog dialog, JecnPropertySetJDialog propertySetJDialog) {
		super(dialog);
		this.propertySetJDialog = propertySetJDialog;
		// 初始化
		initComponents();
		// 初始化值
		initData(dialog.getSelectedPropContPanel().getConfigTypeDesgBean());
		initLayout();
	}

	private void initComponents() {
		// 确认取消按钮面板
		okCancelBtnAddItemPanel = new JecnOKCancelJButtonPanel(dialog);
		// 显示岗位名称
		showPosNameBox = new JecnConfigCheckBox(JecnProperties.getValue("showPosNameBox"));
		// 显示活动输入、输出
		showActInOutBox = new JecnConfigCheckBox(JecnProperties.getValue("showActInOutBox"));
		showPosNameBox.setOpaque(false);
		showActInOutBox.setOpaque(false);
		showActInOutBox.setActionCommand("showActInOutBox");
		showActInOutBox.addActionListener(this);

		// 是否从流程图客户图符中读取
		readerFlowCustomerBox = new JecnConfigCheckBox(JecnProperties.getValue("readerFlowCustomer"));
		readerFlowCustomerBox.setOpaque(false);
		readerFlowCustomerBox.addActionListener(this);

		// 事件
		okCancelBtnAddItemPanel.getOkJButton().addActionListener(this);
		okCancelBtnAddItemPanel.getCancelJButton().addActionListener(this);
		// 按钮命令
		okCancelBtnAddItemPanel.getOkJButton().setActionCommand(ButtonCommendEnum.addOKBtn.toString());
		okCancelBtnAddItemPanel.getCancelJButton().setActionCommand(ButtonCommendEnum.addCancelBtn.toString());
		// 活动说明显示类型
		typeLabel = new JLabel(JecnProperties.getValue("flowFileTypeLabel"));
		// 段落
		paragraphRadioButton = new JecnConfigRadioButton(JecnProperties.getValue("flowFileParagraph"));
		// 表格
		tableRadioButton = new JecnConfigRadioButton(JecnProperties.getValue("flowFileTable"));

		fileLabel = new JLabel(JecnProperties.getValue("fileShowText"));
		fileShowCheckBox = new JecnConfigCheckBox("");
		fileShowCheckBox.setOpaque(false);
		fileShowCheckBox.setActionCommand("fileShowCheckBox");
		fileShowCheckBox.addActionListener(this);

		// 添加按钮组
		ButtonGroup group = new ButtonGroup();
		group.add(paragraphRadioButton);
		group.add(tableRadioButton);

		// 事件
		paragraphRadioButton.addActionListener(this);
		tableRadioButton.addActionListener(this);
		// 命令按钮名称
		paragraphRadioButton.setActionCommand("paragraphRadioButton");
		tableRadioButton.setActionCommand("tableRadioButton");

		paragraphRadioButton.setOpaque(false);
		tableRadioButton.setOpaque(false);

		// 默认
		tableRadioButton.setSelected(true);

		activeTimeCheckBox = new JecnConfigCheckBox("");
		activeTimeLabel = new JLabel(JecnProperties.getValue("timeLimit"));
		activeTimeCheckBox.setOpaque(false);
		activeTimeCheckBox.addActionListener(this);

		activePutLogoCheckBox = new JecnConfigCheckBox("");
		activePutLogoLabel = new JLabel(JecnProperties.getValue("textShowLogoImage"));
		activePutLogoCheckBox.setOpaque(false);
		activePutLogoCheckBox.addActionListener(this);

		chOperationTipLabel = new JLabel(JecnProperties.getValue("tipCh"));
		chOperationTip = new JecnConfigTextArea();
		chOperationTip.setRows(4);
		chOperationPanel = new JScrollPane();
		chOperationPanel.setViewportView(chOperationTip);

		enOperationTipLabel = new JLabel(JecnProperties.getValue("tipEn"));
		enOperationTip = new JecnConfigTextArea();
		enOperationTip.setRows(4);
		enOperationPanel = new JScrollPane();
		enOperationPanel.setViewportView(enOperationTip);

	}

	private void initLayout() {
		this.add(infoPanel, BorderLayout.CENTER);
		this.add(okCancelBtnAddItemPanel, BorderLayout.SOUTH);

		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);

		int count = 0;
		infoPanel.setLayout(new GridBagLayout());
		if (isShowPos) {
			c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(showPosNameBox, c);
		} else if (isShowReadCustomer) {
			c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(readerFlowCustomerBox, c);
		} else if (isShowActivity) {
			count = addActivityAtty(count, insets);
		}

		if (isShowOperationTip) {
			count++;
			c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(chOperationTipLabel, c);
			c = new GridBagConstraints(1, count, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			infoPanel.add(chOperationPanel, c);

			count++;
			c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(enOperationTipLabel, c);
			c = new GridBagConstraints(1, count, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			infoPanel.add(enOperationPanel, c);
		}
		// count++;
		// c = new GridBagConstraints(0, count, 3, 1, 1.0, 1.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
		// insets, 0, 0);
		// infoPanel.add(new JLabel(), c);
	}

	private int addActivityAtty(int count, Insets insets) {
		// 显示活动输入输出 布局
		GridBagConstraints c = new GridBagConstraints(0, count, 3, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		infoPanel.add(showActInOutBox, c);

		count++;

		// 活动明细显示类型
		c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(typeLabel, c);
		// 段落
		c = new GridBagConstraints(1, count, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(paragraphRadioButton, c);
		// 表格
		c = new GridBagConstraints(2, count, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(tableRadioButton, c);

		count++;
		c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(fileLabel, c);

		c = new GridBagConstraints(1, count, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(fileShowCheckBox, c);

		count++;
		c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activeTimeLabel, c);

		c = new GridBagConstraints(1, count, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activeTimeCheckBox, c);

		count++;
		c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activePutLogoLabel, c);

		c = new GridBagConstraints(1, count, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activePutLogoCheckBox, c);
		return count;
	}

	@Override
	public boolean check() {
		return false;
	}

	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;
		// 当前属性值面板
		JecnAbstractPropertyBasePanel selectedPropContPanel = dialog.getSelectedPropContPanel();
		JecnThreeTableScrollPane tableScrollPane = (JecnThreeTableScrollPane) selectedPropContPanel
				.getTableScrollPane();
		// 获取选中行集合
		List<JecnConfigItemBean> itemList = tableScrollPane.getSelectedItemBeanList();
		if (itemList.size() == 1) {
			JecnConfigItemBean bean = itemList.get(0);
			if (ConfigItemPartMapMark.a8.toString().equals(bean.getMark())) {// 角色/职责
				isShowPos = true;
			} else if (ConfigItemPartMapMark.a16.toString().equals(bean.getMark())) {// 客户
				isShowReadCustomer = true;
			} else if (ConfigItemPartMapMark.a10.toString().equals(bean.getMark())) {
				isShowActivity = true;
			}
			// String mark = bean.getMark();
			// Pattern pattern = Pattern.compile("a[0-9]+");
			// Matcher matcher = pattern.matcher(mark);
			// isShowOperationTip = matcher.matches();
			chOperationTip.setItemBean(bean);
			chOperationTip.setText(bean.getV1());

			enOperationTip.setItemBean(bean);
			enOperationTip.setText(bean.getV2());
		}

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String value = itemBean.getValue();
			if (ConfigItemPartMapMark.showPosNameBox.toString().equals(mark)) {// 显示岗位名称
				showPosNameBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {
					showPosNameBox.setSelected(true);
				} else {
					showPosNameBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.showActInOutBox.toString().equals(mark)) {// 显示活动输入、输出
				showActInOutBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {
					showActInOutBox.setSelected(true);
				} else {
					showActInOutBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.flowFileType.toString().equals(mark)) {
				paragraphRadioButton.setItemBean(itemBean);
				tableRadioButton.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_PARAGRAPH.equals(value)) {// 流程文件类型(1:段落；0:表格（默认）)
					paragraphRadioButton.setSelected(true);
				} else {
					tableRadioButton.setSelected(true);
				}
			} else if (ConfigItemPartMapMark.activityFileShow.toString().equals(mark)) {// 活动输入输出文本显示
				if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {
					fileShowCheckBox.setSelected(true);
				} else {
					fileShowCheckBox.setSelected(false);
				}
				fileShowCheckBox.setItemBean(itemBean);
			} else if (ConfigItemPartMapMark.readerFlowCustomer.toString().equals(mark)) {// 是否从流程图客户图符中读取
				if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {
					readerFlowCustomerBox.setSelected(true);
				} else {
					readerFlowCustomerBox.setSelected(false);
				}
				readerFlowCustomerBox.setItemBean(itemBean);
			} else if (ConfigItemPartMapMark.isShowActiveTimeValue.toString().equals(mark)) {// 是否从流程图客户图符中读取
				if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {
					activeTimeCheckBox.setSelected(true);
				} else {
					activeTimeCheckBox.setSelected(false);
				}
				activeTimeCheckBox.setItemBean(itemBean);
			} else if (ConfigItemPartMapMark.isShowActivtyTextInOutIcon.toString().equals(mark)) {// 是否从流程图客户图符中读取
				if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {
					activePutLogoCheckBox.setSelected(true);
				} else {
					activePutLogoCheckBox.setSelected(false);
				}
				activePutLogoCheckBox.setItemBean(itemBean);
			}

		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (ButtonCommendEnum.addOKBtn.toString().equals(btn.getActionCommand())) {// 确认按钮
				okActionPerformed(e);
			} else if (ButtonCommendEnum.addCancelBtn.toString().equals(btn.getActionCommand())) {// 取消按钮
				cancelActionPerformed(e);
			}
		} else if (e.getSource() instanceof JRadioButton) {
			JecnConfigRadioButton btn = (JecnConfigRadioButton) e.getSource();
			if (btn.isSelected()) {// 是否选中
				// 流程文件类型：1:段落；0：表格(默认)
				if ("paragraphRadioButton".equals(btn.getActionCommand())) {// 段落
					btn.getItemBean().setValue(JecnConfigContents.ITEM_PARAGRAPH);
				} else if ("tableRadioButton".equals(btn.getActionCommand())) {// 表格(默认)
					btn.getItemBean().setValue(JecnConfigContents.ITEM_TABLE);
				}
			}
		} else if (e.getSource() instanceof JecnConfigCheckBox) {
			JecnConfigCheckBox btn = (JecnConfigCheckBox) e.getSource();
			if ("fileShowCheckBox".equals(btn.getActionCommand())) {
				if (fileShowCheckBox.isSelected()) {
					btn.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else {
					btn.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				}
			} else if ("showActInOutBox".equals(btn.getActionCommand())) {
				if (showActInOutBox.isSelected()) {
					btn.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else {
					btn.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				}
			}
		}
	}

	/**
	 * 
	 * 确认按钮处理方法
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void okActionPerformed(ActionEvent e) {
		// 检查长度
		JecnUserInfoTextArea infoLabel = okCancelBtnAddItemPanel.getInfoLabel();
		String tip = chOperationTip.getText();
		if (tip != null) {
			String check = JecnUserCheckUtil.checkNoteLength(tip.trim());
			if (check != null && !"".equals(check)) {
				infoLabel.setVisible(true);
				infoLabel.setText(check);
				return;
			}
		}

		tip = enOperationTip.getText();
		if (tip != null) {
			String check = JecnUserCheckUtil.checkNoteLength(tip.trim());
			if (check != null && !"".equals(check)) {
				infoLabel.setVisible(true);
				infoLabel.setText(check);
				return;
			}
		}

		infoLabel.setVisible(false);

		if (showPosNameBox.getItemBean() == null || showActInOutBox.getItemBean() == null) {
			return;
		}
		if (isShowReadCustomer) {// 读取流程客户
			readerFlowCustomerBox.getItemBean().setValue(
					readerFlowCustomerBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);
		} else if (isShowPos) {// 显示岗位名称
			showPosNameBox.getItemBean()
					.setValue(
							showPosNameBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
									: JecnConfigContents.ITEM_IS_NOT_SHOW);
		} else {// 显示活动输入、输出
			showActInOutBox.getItemBean().setValue(
					showActInOutBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);
			fileShowCheckBox.getItemBean().setValue(
					fileShowCheckBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);
			activeTimeCheckBox.getItemBean().setValue(
					activeTimeCheckBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);

			activePutLogoCheckBox.getItemBean().setValue(
					activePutLogoCheckBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);
		}
		if (isShowOperationTip) {
			chOperationTip.getItemBean().setV1(chOperationTip.getText());
			enOperationTip.getItemBean().setV2(enOperationTip.getText());
		}

		// 关闭显示框
		propertySetJDialog.setVisible(false);
	}

	/**
	 * 
	 * 取消按钮处理方法
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void cancelActionPerformed(ActionEvent e) {
		// 关闭显示框
		propertySetJDialog.setVisible(false);
	}
}
