package com.jecn.epros.service.selectFile;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.selectFile.SelectTableBean;

import java.util.List;
import java.util.Map;

public interface SelectFileService {

    /**
     * 树展开
     *
     * @param map
     * @return
     * @
     */
    public List<TreeNode> treeNodes(Map<String, Object> map);


    /**
     * 搜索
     *
     * @param map
     * @return
     * @
     */
    public List<TreeNode> searchName(Map<String, Object> map);

    /**
     * 获得当前节点
     * @param map
     * @return
     */
    TreeNode treeNode(Map<String, Object> map);

    /**
     * 节点定位，展开的是一个树结构
     * @param map
     * @return
     */
    TreeNode treeNodesOrientate(Map<String, Object> map);

    LinkResource getFileLink(Map<String,Object> map);

    List<TreeNode> inventedTreeNodes(Map<String, Object> map);
}
