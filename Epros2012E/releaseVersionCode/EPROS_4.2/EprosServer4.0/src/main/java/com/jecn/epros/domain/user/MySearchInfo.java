package com.jecn.epros.domain.user;

/**
 *
 * 我的主页-流程、制度、风险 搜索对象
 * Created by ZXH on 2017/2/15.
 */
public class MySearchInfo {
    /** 名称*/
    private String name;
    /** 编号*/
    private String number;

    public  String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
