package com.jecn.epros.server.bean.process.temp;

import java.util.ArrayList;
import java.util.List;

public class TempApproveTimeHeadersBean {
	
	/**表头*/
	private List<String> headers=new ArrayList<String>();
	/**表头顺序*/
	private List<String> order=new ArrayList<String>();
	public List<String> getHeaders() {
		return headers;
	}
	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}
	public List<String> getOrder() {
		return order;
	}
	public void setOrder(List<String> order) {
		this.order = order;
	}
	

}
