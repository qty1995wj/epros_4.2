package com.jecn.epros.server.bean.process;
/**
 * 活动和角色的关系（设计器）
 * @author Administrator
 *
 */
public class JecnRoleActiveT implements java.io.Serializable{
	// Fields
	private Long autoIncreaseId;//主键ID
    private String roleImageId;//设计器角色唯一标识
    private String activeFigureId;//设计器活动唯一标识
    private Long figureRoleId;//对应角色JecnFlowStructureImageT的figureId
    private Long figureActiveId;//对应活动JecnFlowStructureImageT的figureId
	public Long getAutoIncreaseId() {
		return autoIncreaseId;
	}
	public void setAutoIncreaseId(Long autoIncreaseId) {
		this.autoIncreaseId = autoIncreaseId;
	}
	public String getRoleImageId() {
		return roleImageId;
	}
	public void setRoleImageId(String roleImageId) {
		this.roleImageId = roleImageId;
	}
	public String getActiveFigureId() {
		return activeFigureId;
	}
	public void setActiveFigureId(String activeFigureId) {
		this.activeFigureId = activeFigureId;
	}
	public Long getFigureRoleId() {
		return figureRoleId;
	}
	public void setFigureRoleId(Long figureRoleId) {
		this.figureRoleId = figureRoleId;
	}
	public Long getFigureActiveId() {
		return figureActiveId;
	}
	public void setFigureActiveId(Long figureActiveId) {
		this.figureActiveId = figureActiveId;
	}
}
