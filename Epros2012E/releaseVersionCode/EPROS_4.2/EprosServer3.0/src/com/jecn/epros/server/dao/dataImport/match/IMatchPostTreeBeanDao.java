package com.jecn.epros.server.dao.dataImport.match;

import java.util.List;

import com.jecn.epros.server.common.IBaseDao;

/**
 * 岗位匹配DAO接口
 * 
 * @author Zhangxiaohu
 * @date： 日期：Feb 28, 2013 时间：2:01:02 PM
 */
public interface IMatchPostTreeBeanDao extends IBaseDao<String, String> {
	/**
	 * 
	 * @description:
	 * @param deptNum部门编号
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getPosAndPersonByOrgId(String deptNum, Long projectId)
			throws Exception;

	/**
	 * @description：获得部门下的岗位
	 * @param deptNum部门编号
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getPosByOrgId(String deptNum, Long projectId)
			throws Exception;

}
