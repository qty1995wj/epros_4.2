package epros.designer.gui.popedom.role;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreePath;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * @author yxw 2012-5-7
 * @description：角色公用方法
 */
public class JecnRoleCommon {

	/**
	 * @author yxw 2012-5-7
	 * @description:获得TreeModel
	 * @param listDefault
	 * @param list
	 * @return
	 */
	public static JecnTreeModel getTreeModel(List<JecnTreeBean> listDefault, List<JecnTreeBean> list) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.roleRoot, JecnProperties
				.getValue("roleManage"));// "角色管理"
		// 默认目录
		JecnTreeNode defaultDirNode = JecnTreeCommon.createTreeRoot(TreeNodeType.roleDefaultDir, JecnProperties
				.getValue("defaultDir"));// "系统默认目录"
		rootNode.add(defaultDirNode);

		if (JecnDesignerCommon.isAdmin() && JecnDesignerCommon.isShowSecondAdmin()) {
			// 流程管理员权限目录
			JecnTreeNode secondAdminDirNode = JecnTreeCommon.createTreeRoot(TreeNodeType.roleSecondAdminDefaultDir,
					JecnProperties.getValue("flowAdminJurisdictionCatalog"));// "流程管理员权限目录"
			rootNode.add(secondAdminDirNode);
			secondAdminDirNode.getJecnTreeBean().setId(-1L);
			addSecondAdminNode(secondAdminDirNode);
		}
		// 添加默认节点
		for (JecnTreeBean jecnTreeBean : listDefault) {
			jecnTreeBean.setName(JecnProperties.getValue(jecnTreeBean.getNumberId()));
			defaultDirNode.add(new JecnTreeNode(jecnTreeBean));
		}
		//
		JecnTreeCommon.addNLevelNodes(list, rootNode);
		return new JecnTreeModel(rootNode);
	}

	private static void addSecondAdminNode(JecnTreeNode secondAdminDirNode) {
		List<JecnTreeBean> listSecondAdmins = null;
		Long curSecondAdminRoleId = -1L;
		try {
			if (JecnDesignerCommon.isSecondAdmin()) {
				curSecondAdminRoleId = ConnectionPool.getJecnRole().getSecondAdminRoleId(JecnConstants.getUserId());
				if (curSecondAdminRoleId != 0) {
					secondAdminDirNode.getJecnTreeBean().setId(curSecondAdminRoleId);
				}
			}
			listSecondAdmins = ConnectionPool.getJecnRole().getSecondAdminTreeBean(curSecondAdminRoleId);
			if (listSecondAdmins.isEmpty()) {
				return;
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("flowRoleError"));
			return;
		}
		for (JecnTreeBean jecnTreeBean : listSecondAdmins) {
			if (jecnTreeBean.getId().equals(curSecondAdminRoleId)) {
				continue;
			}
			secondAdminDirNode.add(new JecnTreeNode(jecnTreeBean));
		}
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:获得TreeModel
	 * @param listDefault
	 * @param list
	 * @return
	 */
	public static JecnTreeModel getTreeMoveModel(List<JecnTreeBean> list, List<Long> listIds) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.roleRoot, JecnProperties
				.getValue("roleTree"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, listIds);
		return new JecnTreeModel(rootNode);
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:点击树节点操作
	 * @param evt
	 * @param jTree
	 */
	public static void treeMousePressed(MouseEvent evt, JecnTree jTree, RoleManageDialog jecnManageDialog) {
		/** 点击左键 */
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			if (evt.getClickCount() == 2) {
				TreePath[] treePath = jTree.getSelectionPaths();
				if (treePath != null && treePath.length == 1) {
					JecnTreeNode node = (JecnTreeNode) treePath[0].getLastPathComponent();
					JecnTreeCommon.autoExpandNode(jTree, node);
					if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.role
							|| node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.roleSecondAdmin) {
						RoleDialog roleDialog = null;
						if (TreeNodeType.roleSecondAdmin == node.getJecnTreeBean().getTreeNodeType()) {// 流程管理员
							roleDialog = new SecondAdminRoleDialog(node, jTree);
						} else {
							roleDialog = new EditRoleDialog(node.getJecnTreeBean(), jTree);
						}
						roleDialog.setVisible(true);
						if (roleDialog.isOperation()) {
							JecnTreeCommon.reNameNode(jTree, node, roleDialog.getRoleName());
							JecnTreeCommon.selectNode(jTree, node);
							// 提示更新成功
							JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
									.getValue("updateSuccess"));
						}
					}

				}
			}
		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = jTree.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean().getTreeNodeType();
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
						// 系统默认角色不能有右键菜单，（roleSecondAdminDefaultDir:流程管理员除外）
						if (nodeSelect.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.roleDefault)
								|| nodeSelect.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.roleDefaultDir)) {
							continue;
						}
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean().getTreeNodeType())) {
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}
				if (listSelectNode.size() > 0) {
					RoleTreeMenu menu = new RoleTreeMenu(jTree, jecnManageDialog);
					if (listSelectNode.size() == 1) {
						TreeNodeType nodeType = listSelectNode.get(0).getJecnTreeBean().getTreeNodeType();
						if (!JecnDesignerCommon.isAdmin() && nodeType == TreeNodeType.roleRoot) {
							return;
						}
						jTree.setSelectionPath(listTreePaths.get(0));
						menu.setListNode(listSelectNode);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
						return;
					}
					// 去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(listSelectNode, jTree);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							jTree.setSelectionPath(treePaths[0]);
						} else {
							jTree.setSelectionPaths(treePaths);
						}
						menu.setListNode(listRemoveChild);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}

			}
		}
	}

	public static List<JecnTreeBean> getRoleTreeBeans(Long pId) throws Exception {
		Long peopleId = null;
		if (JecnDesignerCommon.isSecondAdmin()) {
			peopleId = JecnConstants.getUserId();
		}
		return ConnectionPool.getJecnRole().getChildRoles(pId, JecnConstants.projectId, peopleId);
	}

	public static List<JecnTreeBean> getRoleTreeBeans(JecnTreeNode node) throws Exception {
		if (node == null) {
			return null;
		}
		Long peopleId = null;
		JecnTreeBean treeBean = node.getJecnTreeBean();
		if (JecnDesignerCommon.isSecondAdmin()) {
			peopleId = JecnConstants.getUserId();
		}
		if (treeBean.getTreeNodeType() == TreeNodeType.roleSecondAdminDefaultDir) {// 流程管理员目录
			return ConnectionPool.getJecnRole().getSecondAdminTreeBean(treeBean.getId());
		} else {
			return ConnectionPool.getJecnRole().getChildRoles(treeBean.getId(), JecnConstants.projectId, peopleId);
		}
	}

	public static List<JecnTreeBean> getDefaultRoleTreeBeans() throws Exception {
		return ConnectionPool.getJecnRole().getDefaultRoles(JecnDesignerCommon.isAdmin());
	}
}
