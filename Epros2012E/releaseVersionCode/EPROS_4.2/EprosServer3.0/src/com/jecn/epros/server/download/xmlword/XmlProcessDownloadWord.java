package com.jecn.epros.server.download.xmlword;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;

import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.word.DocModeCommon;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;

/**
 * xml 导出操作说明类
 * 
 * @author fuzhh 2013-6-3
 * 
 */
public class XmlProcessDownloadWord {
	/** 模板存储的文件目录 */
	private String DOC_MODE_FILE = "document/docMode/";
	/** 模板存储文件名称 */
	private String DOC_MODE_NAME = "wordMode.xml";

	/**
	 * 创建word Xml 转换为word
	 * 
	 * @author fuzhh 2013-6-3
	 * @param processDownloadBean
	 * @return
	 */
	public String createWord(ProcessDownloadBean processDownloadBean,
			String path) {
		Document document = createXmlModeDocument(processDownloadBean);
		if (document == null) {
			return "";
		}
		String filePath = "";

		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			filePath = path;
		} else {
			filePath = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
		}
		// 导出word
		boolean flag = DocModeCommon.docXmlFile(document, filePath);
		// word文件存储路径
		String docPath = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
		if (flag) {
			// 转换为word文件
			DocModeCommon.readXmlToDoc(filePath, docPath);
			// 文件路径不为空
			if (filePath != null) {
				// 创建文件
				File file = new File(filePath);
				if (file.exists()) {
					file.delete();
				}
			}
			return docPath;
		} else {
			return "";
		}
	}

	/**
	 * 创建word xml格式
	 * 
	 * @author fuzhh 2013-6-3
	 * @param processDownloadBean
	 * @return
	 */
	public String createXml(ProcessDownloadBean processDownloadBean, String path) {
		Document document = createXmlModeDocument(processDownloadBean);
		if (document == null) {
			return "";
		}

		String filePath = "";
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			filePath = path;
		} else {
			filePath = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
		}

		// 导出word
		boolean flag = DocModeCommon.docXmlFile(document, filePath);
		if (flag) {
			return filePath;
		} else {
			return "";
		}
	}

	/**
	 * 获取xml的 document 对象
	 * 
	 * @author fuzhh Apr 23, 2013
	 */
	public Document createXmlModeDocument(
			ProcessDownloadBean processDownloadBean) {
		// 获取项目文件路径
		String filePath = JecnPath.APP_PATH + DOC_MODE_FILE;
		// 模板存储路径
		String docPath = filePath + DOC_MODE_NAME;
		// 获取xml的 Document
		Document document = XmlDownUtil.getDocumentByXml(docPath);
		if (document == null) {
			return null;
		}
		// 格式化 Document
		document.normalize();
		// 创建word
		createWord(document, processDownloadBean);

		return document;
	}

	/**
	 * 创建word
	 * 
	 * @author fuzhh Apr 23, 2013
	 */
	private void createWord(Document document,
			ProcessDownloadBean processDownloadBean) {
		// 获取根节点
		Element rootEle = XmlDownUtil.getRootElement(document);
		// 获取body节点
		Element bodyEle = XmlDownUtil.getChildNodes(rootEle, "body");
		// 获取子部分节点
		Element sectionEle = XmlDownUtil.getChildNodes(bodyEle, "sub-section");
		// 存储所有节点的List
		List<Element> pNodeEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(sectionEle, pNodeEleList, "p");
		// 存储标题样式的节点
		Element titleEle = XmlDownUtil.getSelectEle(pNodeEleList,
				XmlDownUtil.titleStyle);
		// 存储内容的节点
		Element contentEle = XmlDownUtil.getSelectEle(pNodeEleList,
				XmlDownUtil.contentStyle);
		// 获取竖向的table节点
		Element highTableEle = JecnXmlTableUtil.getTableNode(sectionEle,
				XmlDownUtil.highTabTitleStyle);
		// 获取横向的table节点
		Element wideTableEle = JecnXmlTableUtil.getTableNode(sectionEle,
				XmlDownUtil.wideTableTitle);
		// 获取横向image节点
		Element sectPrImgEle = XmlDownUtil.getImageExistNode(pNodeEleList,
				new String[] { "binData", "sectPr" });
		// 获取普通image节点
		Element imgEle = XmlDownUtil.getImageExistNode(pNodeEleList, "binData",
				"sectPr");
		// 获取页眉页脚节点
		Element sectPrEle = XmlDownUtil.getExistNode(pNodeEleList, "sectPr");
		// 设置页眉数据
		JecnXmlContentUtil.setDefineData(sectionEle, processDownloadBean);
		// 生成word数据
		setUpWordData(processDownloadBean, sectPrEle, titleEle, contentEle,
				highTableEle, wideTableEle, sectPrImgEle, imgEle);

		// 删除样式节点
		XmlDownUtil.deleteNode(titleEle);
		XmlDownUtil.deleteNode(contentEle);
		XmlDownUtil.deleteNode(highTableEle);
		XmlDownUtil.deleteNode(wideTableEle);
		XmlDownUtil.deleteNode(sectPrImgEle);
		XmlDownUtil.deleteNode(sectPrEle);
		XmlDownUtil.deleteNode(imgEle);
	}

	private void setUpWordData(ProcessDownloadBean processDownloadBean,
			Element sectPrEle, Element titleEle, Element contentEle,
			Element highTableEle, Element wideTableEle, Element sectPrImgEle,
			Element imgEle) {
		if (JecnContants.otherOperaType != 1) {
			createHomePage(highTableEle, titleEle, contentEle,
					processDownloadBean.getPeopleList());
			// 复制页面页脚节点
			XmlDownUtil.copyNodes(sectPrEle);
		}
		int count = 1;
		for (JecnConfigItemBean JecnConfigItemBean : processDownloadBean
				.getJecnConfigItemBean()) {
			if ("0".equals(JecnConfigItemBean.getValue())) {// 是否显示
				continue;
			}
			// 标题名称
			String titleName = count + "、" + JecnConfigItemBean.getName();
			// 内容
			String contentVal = "";
			if ("a1".equals(JecnConfigItemBean.getMark())) { // 1、目的
				contentVal = processDownloadBean.getFlowPurpose();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a2".equals(JecnConfigItemBean.getMark())) { // 2、适用范围
				contentVal = processDownloadBean.getApplicability();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a3".equals(JecnConfigItemBean.getMark())) { // 3、术语定义
				contentVal = processDownloadBean.getNoutGlossary();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a4".equals(JecnConfigItemBean.getMark())) { // 4、 驱动规则
				createDrivingRules(highTableEle, titleEle, contentEle,
						processDownloadBean.getFlowDriver(), titleName);
				count++;
			} else if ("a5".equals(JecnConfigItemBean.getMark())) { // 5、流程输入
				contentVal = processDownloadBean.getFlowInput();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a6".equals(JecnConfigItemBean.getMark())) { // 6、流程输出
				contentVal = processDownloadBean.getFlowOutput();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a7".equals(JecnConfigItemBean.getMark())) { // 7、关键活动
				createKeyActivity(wideTableEle, titleEle, contentEle,
						titleName, processDownloadBean.getKeyActivityShowList());
				count++;
			} else if ("a8".equals(JecnConfigItemBean.getMark())) { // 8、角色职责
				createRoleResponsibility(wideTableEle, titleEle, contentEle,
						processDownloadBean.getRoleList(), titleName);
				count++;
			} else if ("a9".equals(JecnConfigItemBean.getMark())) { // 9、流程图
				if(StringUtils.isNotBlank(processDownloadBean.getImgPath())){
					createImg(
							sectPrEle,
							sectPrImgEle,
							imgEle,
							titleEle,
							titleName,
							JecnContants.jecn_path
									+ processDownloadBean.getImgPath());
				}else{
					
					JecnXmlContentUtil.createTitleAndContent(titleName, "",
							titleEle, contentEle);
				}
				count++;
			} else if ("a10".equals(JecnConfigItemBean.getMark())) { // 10、活动明细
				createActivityShow(wideTableEle, titleEle, contentEle,
						processDownloadBean.getAllActivityShowList(), titleName);
				count++;
			} else if ("a11".equals(JecnConfigItemBean.getMark())) { // 11、流程记录
				createProcessRecord(wideTableEle, titleEle, contentEle,
						processDownloadBean.getProcessRecordList(), titleName);
				count++;
			} else if ("a12".equals(JecnConfigItemBean.getMark())) { // 12、操作规范
				createOperationTemplate(wideTableEle, titleEle, contentEle,
						processDownloadBean.getOperationTemplateList(),
						titleName);
				count++;
			} else if ("a13".equals(JecnConfigItemBean.getMark())) { // 13、相关流程
				createRelatedProcess(wideTableEle, titleEle, contentEle,
						processDownloadBean.getRelatedProcessList(), titleName);
				count++;
			} else if ("a14".equals(JecnConfigItemBean.getMark())) { // 14、相关制度
				createRuleName(wideTableEle, titleEle, contentEle,
						processDownloadBean.getRuleNameList(), titleName);
				count++;
			} else if ("a15".equals(JecnConfigItemBean.getMark())) { // 15、流程关键测评指标板
				createFlowKpi(wideTableEle, titleEle, contentEle,
						processDownloadBean.getFlowKpiList(), titleName);
				count++;
			} else if ("a16".equals(JecnConfigItemBean.getMark())) { // 16、客户
				contentVal = processDownloadBean.getFlowCustom();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a17".equals(JecnConfigItemBean.getMark())) { // 17、不适用范围
				contentVal = processDownloadBean.getNoApplicability();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a18".equals(JecnConfigItemBean.getMark())) { // 18、概述信息
				contentVal = processDownloadBean.getFlowSummarize();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a19".equals(JecnConfigItemBean.getMark())) { // 19、补充说明
				contentVal = processDownloadBean.getFlowSupplement();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			} else if ("a20".equals(JecnConfigItemBean.getMark())) { // 20、记录保存
				createFlowRecord(wideTableEle, titleEle, contentEle,
						processDownloadBean.getFlowRecordList(), titleName);
				count++;
			} else if ("a21".equals(JecnConfigItemBean.getMark())) { // 21、相关标准
				createStandardName(wideTableEle, titleEle, contentEle,
						processDownloadBean.getStandardList(), titleName);
				count++;
			} else if ("a22".equals(JecnConfigItemBean.getMark())) { // 22、流程边界
				createStartEndActive(highTableEle, titleEle, contentEle,
						processDownloadBean.getStartEndActive(), titleName);
				count++;
			} else if ("a23".equals(JecnConfigItemBean.getMark())) { // 23、流程责任属性
				createResponFlow(highTableEle, titleEle, contentEle,
						processDownloadBean.getResponFlow(), titleName);
				count++;
			} else if ("a24".equals(JecnConfigItemBean.getMark())) { // 24、流程文控信息
				createDocument(wideTableEle, titleEle, contentEle,
						processDownloadBean.getDocumentList(), titleName);
				count++;
			} else if ("a25".equals(JecnConfigItemBean.getMark())) { // 25、相关风险
				createRisk(wideTableEle, titleEle, contentEle,
						processDownloadBean.getRilsList(), titleName);
				count++;
			}
			else if ("a26".equals(JecnConfigItemBean.getMark())) { // 26相关文件
				contentVal = processDownloadBean.getFlowRelatedFile();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			}
			else if ("a27".equals(JecnConfigItemBean.getMark())) { // 27、自定义要素1
				contentVal = processDownloadBean.getFlowCustomOne();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			}else if ("a28".equals(JecnConfigItemBean.getMark())) { // 28、自定义要素2
				contentVal = processDownloadBean.getFlowCustomTwo();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			}
			else if ("a29".equals(JecnConfigItemBean.getMark())) { // 29、自定义要素3
				contentVal = processDownloadBean.getFlowCustomThree();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			}
			else if ("a30".equals(JecnConfigItemBean.getMark())) { // 30、自定义要素4
				contentVal = processDownloadBean.getFlowCustomFour();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			}
			else if ("a31".equals(JecnConfigItemBean.getMark())) { // 31、自定义要素5
				contentVal = processDownloadBean.getFlowCustomFive();
				JecnXmlContentUtil.createTitleAndContent(titleName, contentVal,
						titleEle, contentEle);
				count++;
			}
		}
	}

	/**
	 * 创建首页显示
	 * 
	 * @author fuzhh Jul 23, 2013
	 * @param tableEle
	 * @param titleEle
	 * @param contentEle
	 */
	private void createHomePage(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> recordBeanList) {
		if (recordBeanList != null && recordBeanList.size() > 0) {
			// 复制table节点
			Element copeTableNode = XmlDownUtil.copyNodes(tableEle);
			// 获取竖向Table的tr节点
			Element trEle = JecnXmlTableUtil.getTableTrNode(copeTableNode,
					XmlDownUtil.highTabTitleStyle);
			for (Object[] obj : recordBeanList) {
				String peopleStr = "";
				if (obj[0] != null) {
					peopleStr = obj[0].toString();
				}
				String peopleName = "";
				if (obj[1] != null) {
					peopleName = obj[1].toString();
				}
				String dateStr = JecnUtil.getValue("theDateC");

				String dateVal = "";
				if (obj[2] != null) {
					dateVal = obj[2].toString();
				}
				// 复制tr节点 驱动类型
				Element copeDriverTypeTrNode = XmlDownUtil.copyNodes(trEle);
				JecnXmlTableUtil.createHighTable(copeDriverTypeTrNode,
						peopleStr, peopleName, dateStr, dateVal);
			}
			// 删除Tr节点
			XmlDownUtil.deleteNode(trEle);
		}
	}

	/**
	 * 创建图片
	 * 
	 * @author fuzhh 2013-5-29
	 * @param sectionEle
	 *            正文节点
	 * @param sectPrImgEle
	 *            横向显示的图片节点
	 * @param imgEle
	 *            图片节点
	 * @param titleEle
	 *            标题节点
	 * @param titleName
	 *            标题名称
	 * @param imgPath
	 *            图片路径
	 */
	private void createImg(Element sectionEle, Element sectPrImgEle,
			Element imgEle, Element titleEle, String titleName, String imgPath) {
		// 创建标题
		JecnXmlContentUtil.createTitle(titleName, titleEle);
		// 复制页眉页脚节点
		XmlDownUtil.copyNodes(sectionEle);
		if (sectPrImgEle == null) {
			return;
		}
		String fileDirPath = imgPath.substring(0, imgPath.lastIndexOf("."));
		File fileDir = new File(fileDirPath);
		if (JecnContants.otherOperaType == 2
				|| JecnContants.otherOperaType == 4) {// 根据图片大小来判断是否横向显示

			if (fileDir.exists()) {
				int len = fileDir.list().length;
				JecnXmlImageUtil.createHighImage(sectPrImgEle, imgEle,
						fileDirPath + "/" + 1 + ".jpg", XmlDownUtil.docWidth,
						XmlDownUtil.docHeight, XmlDownUtil.widthDocWidth,
						XmlDownUtil.widthDocHeight);
				for (int i = 2; i <= len; i++) {
					JecnXmlImageUtil.createHighImage(sectPrImgEle, imgEle,
							fileDirPath + "/" + i + ".jpg",
							XmlDownUtil.docWidth, XmlDownUtil.docHeight,
							XmlDownUtil.widthDocWidth,
							XmlDownUtil.widthDocHeight);
					
				}
			} else {
				JecnXmlImageUtil.createHighImage(sectPrImgEle, imgEle, imgPath,
						XmlDownUtil.docWidth, XmlDownUtil.docHeight,
						XmlDownUtil.widthDocWidth, XmlDownUtil.widthDocHeight);
			}

		} else if (JecnContants.otherOperaType == 3
				|| JecnContants.otherOperaType == 5) {// 始终纵向显示
			
			if (fileDir.exists()) {
				int len = fileDir.list().length;
				JecnXmlImageUtil.createHighImage(imgEle, fileDirPath + "/" + 1 + ".jpg",
						XmlDownUtil.docWidth, XmlDownUtil.docHeight);
				for (int i = 2; i <= len; i++) {
					JecnXmlImageUtil.createHighImage(imgEle, fileDirPath + "/" + i + ".jpg",
							XmlDownUtil.docWidth, XmlDownUtil.docHeight);
				}
			}else{
				JecnXmlImageUtil.createHighImage(imgEle, imgPath,
						XmlDownUtil.docWidth, XmlDownUtil.docHeight);
			}
			
		}
	}

	/**
	 * 生成驱动规则数据
	 * 
	 * @author fuzhh 2013-5-24
	 * @param sectionEle
	 *            正文节点
	 * @param tableEle
	 *            table 节点
	 * @param driverBean
	 *            驱动规则数据
	 * @param titleName
	 *            标题名称
	 */
	private void createDrivingRules(Element tableEle, Element titleEle,
			Element contentEle, FlowDriverBean driverBean, String titleName) {
		// 创建标题
		JecnXmlContentUtil.createTitle(titleName, titleEle);
		if (driverBean.getDriveType() != null) {
			// 复制table节点
			Element copeTableNode = XmlDownUtil.copyNodes(tableEle);
			// 获取竖向Table的tr节点
			Element trEle = JecnXmlTableUtil.getTableTrNode(copeTableNode,
					XmlDownUtil.highTabTitleStyle);
			// 驱动类型
			String driverType = JecnUtil.getValue("drivingType");
			// 驱动类型名称
			String driveType = "";
			if ("1".equals(driverBean.getDriveType().toString())) {
				driveType = JecnUtil.getValue("timeDrive");
			} else {
				driveType = JecnUtil.getValue("eventDriven");
			}
			// 设置tr数据
			JecnXmlTableUtil.createHighTableDate(trEle, driverType, driveType);

			// 驱动规则
			String driverRules = JecnUtil.getValue("drivingRules");
			// 驱动规则value
			String driverRulesVal = driverBean.getDriveRules();
			// 设置tr数据
			JecnXmlTableUtil.createHighTableDate(trEle, driverRules,
					driverRulesVal);

			if ("1".equals(driverBean.getDriveType().toString())) {
				// 频率
				String frequency = JecnUtil.getValue("frequency");
				// 频率value
				String frequencyVal = driverBean.getFrequency();
				// 设置tr数据
				JecnXmlTableUtil.createHighTableDate(trEle, frequency,
						frequencyVal);

				// 开始时间
				String startingTime = JecnUtil.getValue("startTime");
				// 开始时间value
				String startTimeVal = driverBean.getStartTime();
				// 设置tr数据
				JecnXmlTableUtil.createHighTableDate(trEle, startingTime,
						startTimeVal);

				// 结束时间
				String endingTime = JecnUtil.getValue("endTime");
				// 结束时间value
				String endTimeVal = driverBean.getEndTime();
				// 设置tr数据
				JecnXmlTableUtil.createHighTableDate(trEle, endingTime,
						endTimeVal);

				// 类型
				String type = JecnUtil.getValue("type");
				// 类型value
				String typeVal = getTypeVal(driverBean.getQuantity());
				// 设置tr数据
				JecnXmlTableUtil.createHighTableDate(trEle, type, typeVal);
			}
			// 删除Tr节点
			XmlDownUtil.deleteNode(trEle);
		} else {
			JecnXmlContentUtil.createContent("", contentEle);
		}

	}

	/**
	 * 创建相关风险
	 * 
	 * @author fuzhh 2013-5-27
	 * @param fileNameList
	 *            string集合
	 * @param titleName
	 *            标题名称
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 */
	public static void createRisk(Element tableEle, Element titleEle,
			Element contentEle, List<JecnRisk> rilsList, String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 2;
		// 风险编号
		String riskNum = JecnUtil.getValue("riskNum");
		// 风险名称
		String riskName = JecnUtil.getValue("riskName");
		// 标题数组
		String[] titleStrNum = new String[] { riskNum, riskName };
		// 数组
		List<Object[]> riskObjList = new ArrayList<Object[]>();
		for (JecnRisk jecnRisk : rilsList) {
			Object[] riskObjStr = new Object[2];
			riskObjStr[0] = jecnRisk.getRiskCode();
			riskObjStr[1] = jecnRisk.getRiskName();
			riskObjList.add(riskObjStr);
		}
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, riskObjList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 生成关键活动
	 * 
	 * @author fuzhh 2013-5-27
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param titleName
	 *            标题名称
	 * @param keyActivityShowList
	 *            关键活动数据
	 */
	private void createKeyActivity(Element tableEle, Element titleEle,
			Element contentEle, String titleName,
			List<KeyActivityBean> keyActivityShowList) {
		// 创建标题
		JecnXmlContentUtil.createTitle(titleName, titleEle);
		// 问题区域
		List<KeyActivityBean> paList = new ArrayList<KeyActivityBean>();
		// 关键控制点
		List<KeyActivityBean> ksfList = new ArrayList<KeyActivityBean>();
		// 关键成功因素
		List<KeyActivityBean> kcpList = new ArrayList<KeyActivityBean>();
		for (KeyActivityBean keyActivityBean : keyActivityShowList) {
			if ("1".equals(keyActivityBean.getActiveKey())) {
				paList.add(keyActivityBean);
			} else if ("2".equals(keyActivityBean.getActiveKey())) {
				ksfList.add(keyActivityBean);
			} else if ("3".equals(keyActivityBean.getActiveKey())) {
				kcpList.add(keyActivityBean);
			}
		}
		if (keyActivityShowList != null && keyActivityShowList.size() > 0) {
			if (JecnContants.flowFileType == 1) {
				createKeyParagraphActivity(titleEle, contentEle, paList,
						ksfList, kcpList);
			} else {
				createKeyTableActivity(tableEle, paList, ksfList, kcpList);
			}
		} else {
			JecnXmlContentUtil.createContent("", contentEle);
		}

	}

	/**
	 * 关键活动说明的 锻炼形式
	 * 
	 * @author fuzhh 2013-9-6
	 * @param contentEle
	 * @param titleName
	 * @param keyActivityShowList
	 */
	private void createKeyParagraphActivity(Element titleEle,
			Element contentEle, List<KeyActivityBean> paList,
			List<KeyActivityBean> ksfList, List<KeyActivityBean> kcpList) {
		if (paList != null && paList.size() > 0) {
			// 问题区域
			String problemArea = JecnUtil.getValue("problemAreas");
			// 创建标题
			JecnXmlContentUtil.createTitle(problemArea, titleEle);
			for (KeyActivityBean activityBean : paList) {
				createKeyShow(activityBean, contentEle);
			}
		}
		if (ksfList != null && ksfList.size() > 0) {
			// 关键成功因素
			String keySuccessFactor = JecnUtil.getValue("keySuccessFactors");
			// 创建标题
			JecnXmlContentUtil.createTitle(keySuccessFactor, titleEle);
			for (KeyActivityBean activityBean : ksfList) {
				createKeyShow(activityBean, contentEle);
			}
		}
		if (kcpList != null && kcpList.size() > 0) {
			// 关键控制点
			String keyControlPoint = JecnUtil.getValue("keyControlPoint");
			// 创建标题
			JecnXmlContentUtil.createTitle(keyControlPoint, titleEle);
			for (KeyActivityBean activityBean : kcpList) {
				createKeyShow(activityBean, contentEle);
			}
		}
	}

	/**
	 * 关键活动 段落形式显示
	 * 
	 * @author fuzhh 2013-9-6
	 * @param activityBean
	 * @param contentEle
	 */
	private void createKeyShow(KeyActivityBean activityBean, Element contentEle) {
		String nbsp = "     ";
		String colon = ":";

		String activityNum = "";
		if (activityBean.getActivityNumber() != null) {
			activityNum = activityBean.getActivityNumber();
		}
		String activityName = "";
		if (activityBean.getActivityName() != null) {
			activityName = activityBean.getActivityName();
		}
		if (!JecnCommon.isNullOrEmtryTrim(activityNum)
				&& !JecnCommon.isNullOrEmtryTrim(activityName)) {
			// 活动编号 名称
			JecnXmlContentUtil.createContent(" " + activityNum + "   "
					+ activityName, contentEle);
		} else {
			// 活动
			String activity = JecnUtil.getValue("activities");
			// 活动编号 名称
			JecnXmlContentUtil.createContent(" " + activity, contentEle);
		}

		// 活动说明
		String activityShow = JecnUtil.getValue("activityIndicatingThat");
		String acitvityExplain = "";
		if (activityBean.getActivityShow() != null) {
			acitvityExplain = activityBean.getActivityShow();
			acitvityExplain = toMultiLine(acitvityExplain, 70);
		}
		JecnXmlContentUtil.createContent(nbsp + activityShow + colon
				+ acitvityExplain, contentEle);

		// 关键说明
		String keyShow = JecnUtil.getValue("keyShow");
		String keyExplain = "";
		if (activityBean.getActivityShowControl() != null) {
			keyExplain = activityBean.getActivityShowControl();
			keyExplain = toMultiLine(keyExplain, 70);
		}
		JecnXmlContentUtil.createContent(nbsp + keyShow + colon + keyExplain,
				contentEle);
	}

	/**
	 * 关键活动说明的table形式
	 * 
	 * @author fuzhh 2013-9-6
	 * @param contentEle
	 * @param titleName
	 * @param keyActivityShowList
	 */
	private void createKeyTableActivity(Element tableEle,
			List<KeyActivityBean> paList, List<KeyActivityBean> ksfList,
			List<KeyActivityBean> kcpList) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 5;
		// 关键活动
		String keyActivities = JecnUtil.getValue("keyActivities");
		// 活动编号
		String activitynumbers = JecnUtil.getValue("activitynumbers");
		// 活动名称
		String activityTitle = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activityShow = JecnUtil.getValue("activityIndicatingThat");
		// 关键说明
		String keyShow = JecnUtil.getValue("keyShow");
		// 问题区域
		String problemArea = JecnUtil.getValue("problemAreas");
		// 关键成功因素
		String keySuccessFactor = JecnUtil.getValue("keySuccessFactors");
		// 关键控制点
		String keyControlPoint = JecnUtil.getValue("keyControlPoint");
		// 标题数组
		String[] titleStrNum = new String[] { keyActivities, activitynumbers,
				activityTitle, activityShow, keyShow };
		// 复制table节点
		Element copeTableNode = XmlDownUtil.copyNodes(tableEle);
		// 创建横向标题
		JecnXmlTableUtil.createWideTableTitle(copeTableNode, titleStrNum,
				widthNum);

		// 问题区域
		boolean isPaOne = true;
		// 关键成功因素
		boolean isKsfOne = true;
		// 关键控制点
		boolean isKcpOne = true;
		// 获取横向Table的tr内容节点
		Element trContentEle = JecnXmlTableUtil.getTableTrNode(copeTableNode,
				XmlDownUtil.wideTableContent);
		if (trContentEle == null) {
			return;
		}
		// 问题区域
		for (KeyActivityBean keyActivityBean : paList) {
			if (isPaOne) {
				String[] paOneNum = new String[] { problemArea,
						keyActivityBean.getActivityNumber(),
						keyActivityBean.getActivityName(),
						keyActivityBean.getActivityShow(),
						keyActivityBean.getActivityShowControl() };
				JecnXmlTableUtil.createWideTableMergeContent(trContentEle,
						paOneNum, widthNum, true);
				isPaOne = false;
			} else {
				String[] paNum = new String[] { "",
						keyActivityBean.getActivityNumber(),
						keyActivityBean.getActivityName(),
						keyActivityBean.getActivityShow(),
						keyActivityBean.getActivityShowControl() };
				JecnXmlTableUtil.createWideTableMergeContent(trContentEle,
						paNum, widthNum, false);
			}
		}
		// 关键控制点
		for (KeyActivityBean keyActivityBean : ksfList) {
			if (isKsfOne) {
				String[] ksfOneNum = new String[] { keySuccessFactor,
						keyActivityBean.getActivityNumber(),
						keyActivityBean.getActivityName(),
						keyActivityBean.getActivityShow(),
						keyActivityBean.getActivityShowControl() };
				JecnXmlTableUtil.createWideTableMergeContent(trContentEle,
						ksfOneNum, widthNum, true);
				isKsfOne = false;
			} else {
				String[] ksfNum = new String[] { "",
						keyActivityBean.getActivityNumber(),
						keyActivityBean.getActivityName(),
						keyActivityBean.getActivityShow(),
						keyActivityBean.getActivityShowControl() };
				JecnXmlTableUtil.createWideTableMergeContent(trContentEle,
						ksfNum, widthNum, false);
			}
		}
		// 关键控制点
		for (KeyActivityBean keyActivityBean : kcpList) {
			if (isKcpOne) {
				String[] kcpOneNum = new String[] { keyControlPoint,
						keyActivityBean.getActivityNumber(),
						keyActivityBean.getActivityName(),
						keyActivityBean.getActivityShow(),
						keyActivityBean.getActivityShowControl() };
				JecnXmlTableUtil.createWideTableMergeContent(trContentEle,
						kcpOneNum, widthNum, true);
				isKsfOne = false;
			} else {
				String[] kcpNum = new String[] { "",
						keyActivityBean.getActivityNumber(),
						keyActivityBean.getActivityName(),
						keyActivityBean.getActivityShow(),
						keyActivityBean.getActivityShowControl() };
				JecnXmlTableUtil.createWideTableMergeContent(trContentEle,
						kcpNum, widthNum, false);
			}
		}
		XmlDownUtil.deleteNode(trContentEle);
	}

	/**
	 * 创建角色职责表格
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param roleList
	 *            角色数据
	 * @param titleName
	 *            标题名称
	 */
	private void createRoleResponsibility(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> roleList, String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 3;
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = JecnUtil.getValue("positionName");
		// 角色职责
		String roleResponsibility = JecnUtil.getValue("responsibilities");
		// 标题数组
		String[] titleStrNum = new String[] { roleName, positionName,
				roleResponsibility };
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, roleList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 创建活动说明数据
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param allActivityShowList
	 *            活动数据
	 * @param titleName
	 *            标题名称
	 */
	private void createActivityShow(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> allActivityShowList,
			String titleName) {
		if (JecnContants.flowFileType == 1) {
			// 创建标题
			JecnXmlContentUtil.createTitle(titleName, titleEle);
			if (allActivityShowList != null && allActivityShowList.size() > 0) {
				String nbsp = "     ";
				String colon = ":";
				for (Object[] obj : allActivityShowList) {
					String activityNum = "";
					if (obj[0] != null) {
						activityNum = obj[0].toString();
					}
					String activityName = "";
					if (obj[2] != null) {
						activityName = obj[2].toString();
					} else {
						activityName = JecnUtil.getValue("activities");
					}
					// 活动编号 名称
					JecnXmlContentUtil.createContent(activityNum + "   "
							+ activityName, contentEle);

					// 执行角色
					String executiveRole = JecnUtil.getValue("executiveRole");
					String activityRole = "";
					if (obj[1] != null) {
						activityRole = obj[1].toString();
					}
					JecnXmlContentUtil.createContent(nbsp + executiveRole
							+ colon + activityRole, contentEle);
					// 输入
					String input = JecnUtil.getValue("input");
					String activityInput = "";
					if (obj[4] != null) {
						activityInput = obj[4].toString().replaceAll("\n", "、");
						activityInput = toMultiLine(activityInput, 70);
					}
					JecnXmlContentUtil.createContent(nbsp + input + colon
							+ activityInput, contentEle);
					// 输出
					String output = JecnUtil.getValue("output");
					String activityOutput = "";
					if (obj[5] != null) {
						activityOutput = obj[5].toString()
								.replaceAll("\n", "、");
						activityOutput = toMultiLine(activityOutput, 70);
					}
					JecnXmlContentUtil.createContent(nbsp + output + colon
							+ activityOutput, contentEle);
					// 活动说明
					String activityDescription = JecnUtil
							.getValue("activityIndicatingThat");
					String activityShow = "";
					if (obj[3] != null) {
						activityShow = obj[3].toString();
						activityShow = toMultiLine(activityShow, 70);
					}
					JecnXmlContentUtil.createContent(nbsp + activityDescription
							+ colon + activityShow, contentEle);
				}
			} else {
				JecnXmlContentUtil.createContent("", contentEle);
			}

		} else {
			// 单元格宽度
			int widthNum = XmlDownUtil.pageWidth / 6;
			// 活动编号
			String activityNumber = JecnUtil.getValue("activitynumbers");
			// 执行角色
			String executiveRole = JecnUtil.getValue("executiveRole");
			// 活动名称
			String eventTitle = JecnUtil.getValue("activityTitle");
			// 活动说明
			String activityDescription = JecnUtil
					.getValue("activityIndicatingThat");
			// 输入
			String input = JecnUtil.getValue("input");
			// 输出
			String output = JecnUtil.getValue("output");
			// 标题数组
			String[] titleStrNum = new String[] { activityNumber,
					executiveRole, eventTitle, activityDescription, input,
					output };
			// 生成数据
			JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
					contentEle, allActivityShowList, titleStrNum, widthNum,
					titleName);
		}

	}

	/**
	 * 在字符串指定位置加入换行符
	 * 
	 * @author fuzhh 2013-9-6
	 * @param str
	 * @param len
	 * @return
	 */
	private String toMultiLine(String str, int len) {
		char[] chs = str.toCharArray();
		StringBuffer sb = new StringBuffer();
		for (int i = 0, sum = 0; i < chs.length; i++) {
			sum += chs[i] < 0xff ? 1 : 2;
			sb.append(chs[i]);
			if (sum >= len) {
				sum = 0;
				sb.append("\n              ");
			}
		}
		return sb.toString();
	}

	/**
	 * 创建流程记录
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param processRecordList
	 *            流程记录数据
	 * @param titleName
	 *            标题名称
	 */
	private void createProcessRecord(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> processRecordList,
			String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 2;
		// 文件编号
		String fileNumber = JecnUtil.getValue("fileNumber");
		// 文件名称
		String fileName = JecnUtil.getValue("fileName");
		// 标题数组
		String[] titleStrNum = new String[] { fileNumber, fileName };
		// 生成数据
		JecnXmlTableUtil
				.creWideTabAndTitleConDate(tableEle, titleEle, contentEle,
						processRecordList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 创建操作规范
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param operationTemplateList
	 *            操作规范数据
	 * @param titleName
	 *            标题名称
	 */
	private void createOperationTemplate(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> operationTemplateList,
			String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 2;
		// 文件编号
		String fileNumber = JecnUtil.getValue("fileNumber");
		// 文件名称
		String fileName = JecnUtil.getValue("fileName");
		// 标题数组
		String[] titleStrNum = new String[] { fileNumber, fileName };
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, operationTemplateList, titleStrNum, widthNum,
				titleName);
	}

	/**
	 * 创建流程关键评测指标
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param flowKpiList
	 *            关键评测指标数据
	 * @param titleName
	 *            标题名称
	 */
	private void createFlowKpi(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> flowKpiList, String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 5;
		for (Object[] obj : flowKpiList) {
			Object objStr = obj[1];
			if (objStr != null && Integer.valueOf(objStr.toString()) == 1) {
				// 过程性指标
				obj[1] = JecnUtil.getValue("efficiencyIndex");
			} else if (objStr != null
					&& Integer.valueOf(objStr.toString()) == 0) {
				// 结果性指标
				obj[1] = JecnUtil.getValue("effectIndex");
			} else {
				// 无
				obj[1] = JecnUtil.getValue("none");
			}
		}
		// 名称
		String name = JecnUtil.getValue("name");
		// 类型
		String type = JecnUtil.getValue("type");
		// KPI定义
		String KpiDefinition = JecnUtil.getValue("KPIDefinition");
		// 统计方法
		String statisticsMethod = JecnUtil.getValue("statisticalMethod");
		// 目标值
		String targetValue = JecnUtil.getValue("targetValue");
		// 标题数组
		String[] titleStrNum = new String[] { name, type, KpiDefinition,
				statisticsMethod, targetValue };
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, flowKpiList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 创建相关制度
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param ruleList
	 *            制度数据
	 * @param titleName
	 *            标题名称
	 */
	private void createRuleName(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> ruleList, String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 2;
		// 制度名称
		String name = JecnUtil.getValue("ruleName");
		// 类型
		String type = JecnUtil.getValue("type");
		// 标题数组
		String[] titleStrNum = new String[] { name, type };
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, ruleList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 创建相关标准
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param standardList
	 *            相关标准
	 * @param titleName
	 *            标题名称
	 */
	private void createStandardName(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> standardList, String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 2;
		// 标准名称
		String name = JecnUtil.getValue("standardName");
		// 类型
		String type = JecnUtil.getValue("type");
		// 标题数组
		String[] titleStrNum = new String[] { name, type };
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, standardList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 创建记录保存
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param flowRecordList
	 *            记录保存数据
	 * @param titleName
	 *            标题名称
	 */
	private void createFlowRecord(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> flowRecordList, String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 6;
		// 记录名称
		String recordName = JecnUtil.getValue("recordName");
		// 保存责任人
		String saveResponsiblePersons = JecnUtil
				.getValue("saveResponsiblePersons");
		// 保存场所
		String savePlace = JecnUtil.getValue("savePlace");
		// 归档时间
		String filingTime = JecnUtil.getValue("filingTime");
		// 保存期限
		String expirationDate = JecnUtil.getValue("storageLife");
		// 到期处理方式
		String handlingDue = JecnUtil.getValue("treatmentDue");
		// 标题数组
		String[] titleStrNum = new String[] { recordName,
				saveResponsiblePersons, savePlace, filingTime, expirationDate,
				handlingDue };
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, flowRecordList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 创建起始活动和终止活动
	 * 
	 * @author fuzhh 2013-5-30
	 * @param tableEle
	 *            竖向table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param startEndActive
	 *            活动数据
	 * @param titleName
	 *            标题名称
	 */
	private void createStartEndActive(Element tableEle, Element titleEle,
			Element contentEle, Object[] startEndActive, String titleName) {
		// 创建标题
		JecnXmlContentUtil.createTitle(titleName, titleEle);
		// 复制table节点
		Element copeTableNode = XmlDownUtil.copyNodes(tableEle);
		// 获取竖向Table的tr节点
		Element trEle = JecnXmlTableUtil.getTableTrNode(copeTableNode,
				XmlDownUtil.highTabTitleStyle);
		// 起始活动
		String startActive = JecnUtil.getValue("initialActivity");
		Object startObj = startEndActive[0];
		String startActiveVal = "";
		if (startObj != null) {
			startActiveVal = startObj.toString();
		}
		// 设置tr数据
		JecnXmlTableUtil
				.createHighTableDate(trEle, startActive, startActiveVal);
		// 终止活动
		String endActive = JecnUtil.getValue("ceasingActivity");
		Object endObj = startEndActive[1];
		String endActiveVal = "";
		if (endObj != null) {
			endActiveVal = endObj.toString();
		}
		// 设置tr数据
		JecnXmlTableUtil.createHighTableDate(trEle, endActive, endActiveVal);
		// 删除Tr节点
		XmlDownUtil.deleteNode(trEle);

	}

	/**
	 * 创建流程责任属性
	 * 
	 * @author fuzhh 2013-5-30
	 * @param tableEle
	 *            竖向table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param responFlow
	 *            流程责任属性数据
	 * @param titleName
	 *            标题名称
	 */
	private void createResponFlow(Element tableEle, Element titleEle,
			Element contentEle, ProcessAttributeBean responFlow,
			String titleName) {
		// 创建标题
		JecnXmlContentUtil.createTitle(titleName, titleEle);
		// 复制table节点
		Element copeTableNode = XmlDownUtil.copyNodes(tableEle);
		// 获取竖向Table的tr节点
		Element trEle = JecnXmlTableUtil.getTableTrNode(copeTableNode,
				XmlDownUtil.highTabTitleStyle);
		// 流程监护人
		String processTheGuardian = JecnUtil.getValue("processTheGuardian");
		String processTheGuardianVal = responFlow.getGuardianName();
		// 设置tr数据
		JecnXmlTableUtil.createHighTableDate(trEle, processTheGuardian,
				processTheGuardianVal);
		// 流程责任人
		String responsiblePersons = JecnUtil
				.getValue("processResponsiblePersons");
		// String responsiblePersonsVal = "";
		// if (responFlow.getDutyUserType() == 0
		// && responFlow.getDutyUserName() != null) {
		// // 人员：
		// responsiblePersonsVal = JecnUtil.getValue("personnelC")
		// + responFlow.getDutyUserName();
		// } else if (responFlow.getDutyUserType() == 1
		// && responFlow.getDutyUserName() != null) {
		// // 岗位：
		// responsiblePersonsVal = JecnUtil.getValue("jobsC")
		// + responFlow.getDutyUserName();
		// }
		// 设置tr数据
		JecnXmlTableUtil.createHighTableDate(trEle, responsiblePersons,
				responFlow.getDutyUserName());

		// 流程拟制人
		String processArtificialPerson = JecnUtil
				.getValue("processArtificialPerson");
		String processArtificialPersonVal = responFlow.getFictionPeopleName();
		// 设置tr数据
		JecnXmlTableUtil.createHighTableDate(trEle, processArtificialPerson,
				processArtificialPersonVal);

		// 流程责任部门
		String processDepartment = JecnUtil
				.getValue("processResponsibilityDepartment");
		String processDepartmentVal = responFlow.getDutyOrgName();
		// 设置tr数据
		JecnXmlTableUtil.createHighTableDate(trEle, processDepartment,
				processDepartmentVal);
		// 删除Tr节点
		XmlDownUtil.deleteNode(trEle);
	}

	/**
	 * 流程文控信息
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param documentList
	 *            流程文控信息
	 * @param titleName
	 *            标题名称
	 */
	private void createDocument(Element tableEle, Element titleEle,
			Element contentEle, List<Object[]> documentList, String titleName) {
		// 单元格宽度
		int widthNum = XmlDownUtil.pageWidth / 5;
		// 版本号
		String version = JecnUtil.getValue("versionNumber");
		// 变更说明
		String changeDescription = JecnUtil.getValue("changeThat");
		// 发布日期
		String releaseDate = JecnUtil.getValue("releaseDate");
		// 流程拟制人
		String artificialPerson = JecnUtil.getValue("processArtificialPerson");
		// 审批人
		String theApprover = JecnUtil.getValue("theApprover");
		// 标题数组
		String[] titleStrNum = new String[] { version, changeDescription,
				releaseDate, artificialPerson, theApprover };
		// 生成数据
		JecnXmlTableUtil.creWideTabAndTitleConDate(tableEle, titleEle,
				contentEle, documentList, titleStrNum, widthNum, titleName);
	}

	/**
	 * 创建相关流程
	 * 
	 * @author fuzhh 2013-5-28
	 * @param tableEle
	 *            table节点
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 * @param relatedProcessList
	 *            相关流程数据
	 * @param titleName
	 *            标题名称
	 */
	private void createRelatedProcess(Element tableEle, Element titleEle,
			Element contentEle, List<RelatedProcessBean> relatedProcessList,
			String titleName) {
		// 创建标题
		JecnXmlContentUtil.createTitle(titleName, titleEle);
		if (relatedProcessList.size() > 0) {
			// 单元格宽度
			int widthNum = XmlDownUtil.pageWidth / 3;
			// 上游流程
			List<RelatedProcessBean> topList = new ArrayList<RelatedProcessBean>();
			// 下游流程
			List<RelatedProcessBean> downList = new ArrayList<RelatedProcessBean>();
			// 过程流程
			List<RelatedProcessBean> processList = new ArrayList<RelatedProcessBean>();
			// 子流程
			List<RelatedProcessBean> sonList = new ArrayList<RelatedProcessBean>();
			for (RelatedProcessBean relatedProcessBean : relatedProcessList) {
				if ("1".equals(relatedProcessBean.getLinecolor())) {
					topList.add(relatedProcessBean);
				} else if ("2".equals(relatedProcessBean.getLinecolor())) {
					downList.add(relatedProcessBean);
				} else if ("3".equals(relatedProcessBean.getLinecolor())) {
					processList.add(relatedProcessBean);
				} else if ("4".equals(relatedProcessBean.getLinecolor())) {
					sonList.add(relatedProcessBean);
				}
			}
			// 流程编号
			String ProcessId = JecnUtil.getValue("processNumber");
			// 流程名称
			String ProcessName = JecnUtil.getValue("processName");
			// 标题数组
			String[] titleStrNum = new String[] { "", ProcessId, ProcessName };
			// 复制table节点
			Element copeTableNode = XmlDownUtil.copyNodes(tableEle);
			// 获取横向Table的tr标题节点
			Element trEle = JecnXmlTableUtil.getTableTrNode(copeTableNode,
					XmlDownUtil.wideTableTitle);
			if (trEle == null) {
				return;
			}
			// 复制tr节点
			Element copeTitleTrNode = XmlDownUtil.copyNodes(trEle);
			// 获取tc节点
			Element tcEle = JecnXmlTableUtil.getTableTcNode(copeTitleTrNode,
					XmlDownUtil.wideTableTitle);
			// 生成标题行
			JecnXmlTableUtil.createWideTable(tcEle, titleStrNum, widthNum);
			// 标题生成完删除TR节点
			XmlDownUtil.deleteNode(trEle);
			// 获取横向Table的tr内容节点
			Element trContentEle = JecnXmlTableUtil.getTableTrNode(
					copeTableNode, XmlDownUtil.wideTableContent);
			// 上游流程
			boolean isTop = true;
			for (RelatedProcessBean relatedProcessBean : topList) {
				// 复制tr节点
				Element copeTrNode = XmlDownUtil.copyNodes(trContentEle);
				// 获取tc节点
				Element tableTcEle = JecnXmlTableUtil.getTableTcNode(
						copeTrNode, XmlDownUtil.wideTableContent);
				if (isTop) {
					// 复制tc节点 到指定节点
					Element copeTcNode = XmlDownUtil.copyNodes(copeTrNode,
							tcEle);
					// 上游流程
					String flowUpper = JecnUtil.getValue("upstreamProcess");
					String[] oneNum = new String[] { flowUpper,
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(copeTcNode,
							tableTcEle, oneNum, 0, widthNum, true);
					XmlDownUtil.deleteNode(copeTcNode);
					isTop = false;
				} else {
					String[] nums = new String[] { "",
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(tableTcEle, nums, 0,
							widthNum, false);
				}
				XmlDownUtil.deleteNode(tableTcEle);
			}
			// 下游流程
			boolean isDown = true;
			for (RelatedProcessBean relatedProcessBean : downList) {
				// 复制tr节点
				Element copeTrNode = XmlDownUtil.copyNodes(trContentEle);
				// 获取tc节点
				Element tableTcEle = JecnXmlTableUtil.getTableTcNode(
						copeTrNode, XmlDownUtil.wideTableContent);
				if (isDown) {
					// 复制tc节点 到指定节点
					Element copeTcNode = XmlDownUtil.copyNodes(copeTrNode,
							tcEle);
					// 下游流程
					String flowLower = JecnUtil.getValue("downstreamProcess");
					String[] oneNum = new String[] { flowLower,
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(copeTcNode,
							tableTcEle, oneNum, 0, widthNum, true);
					XmlDownUtil.deleteNode(copeTcNode);
					isDown = false;
				} else {
					String[] nums = new String[] { "",
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(tableTcEle, nums, 0,
							widthNum, false);
				}
				XmlDownUtil.deleteNode(tableTcEle);
			}

			// 过程流程
			boolean isProcess = true;
			for (RelatedProcessBean relatedProcessBean : processList) {
				// 复制tr节点
				Element copeTrNode = XmlDownUtil.copyNodes(trContentEle);
				// 获取tc节点
				Element tableTcEle = JecnXmlTableUtil.getTableTcNode(
						copeTrNode, XmlDownUtil.wideTableContent);
				if (isProcess) {
					// 复制tc节点 到指定节点
					Element copeTcNode = XmlDownUtil.copyNodes(copeTrNode,
							tcEle);
					// 过程流程
					String flowLower = JecnUtil.getValue("processFlow");
					String[] oneNum = new String[] { flowLower,
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(copeTcNode,
							tableTcEle, oneNum, 0, widthNum, true);
					XmlDownUtil.deleteNode(copeTcNode);
					isProcess = false;
				} else {

					String[] nums = new String[] { "",
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(tableTcEle, nums, 0,
							widthNum, false);
				}
				XmlDownUtil.deleteNode(tableTcEle);
			}

			// 子流程
			boolean isSon = true;
			for (RelatedProcessBean relatedProcessBean : sonList) {
				// 复制tr节点
				Element copeTrNode = XmlDownUtil.copyNodes(trContentEle);
				// 获取tc节点
				Element tableTcEle = JecnXmlTableUtil.getTableTcNode(
						copeTrNode, XmlDownUtil.wideTableContent);
				if (isSon) {
					// 复制tc节点 到指定节点
					Element copeTcNode = XmlDownUtil.copyNodes(copeTrNode,
							tcEle);
					// 子流程
					String flowLower = JecnUtil.getValue("childProcess");
					String[] oneNum = new String[] { flowLower,
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(copeTcNode,
							tableTcEle, oneNum, 0, widthNum, true);
					XmlDownUtil.deleteNode(copeTcNode);
					isSon = false;
				} else {
					String[] nums = new String[] { "",
							relatedProcessBean.getFlowNumber(),
							relatedProcessBean.getFlowName() };
					JecnXmlTableUtil.createWideTableByTc(tableTcEle, nums, 0,
							widthNum, false);
				}
				XmlDownUtil.deleteNode(tableTcEle);
			}
			// 删除样式的tc节点
			XmlDownUtil.deleteNode(tcEle);
			XmlDownUtil.deleteNode(trContentEle);
		} else {
			JecnXmlContentUtil.createContent("", contentEle);
		}
	}

	/**
	 * 获取时间驱动类型
	 * 
	 * @author fuzhh 2013-5-24
	 * @param qantity
	 * @return
	 */
	private String getTypeVal(String qantity) {
		String dateType = "";
		if ("1".equals(qantity)) { // 日
			dateType = JecnUtil.getValue("day");
		} else if ("2".equals(qantity)) { // 周
			dateType = JecnUtil.getValue("weeks");
		} else if ("3".equals(qantity)) { // 月
			dateType = JecnUtil.getValue("month");
		} else if ("4".equals(qantity)) { // 季
			dateType = JecnUtil.getValue("season");
		} else if ("5".equals(qantity)) { // 年
			dateType = JecnUtil.getValue("years");
		}
		return dateType;
	}
}
