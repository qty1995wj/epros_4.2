package com.jecn.epros.server.common;

import org.apache.commons.lang.builder.ToStringBuilder;

public abstract class AbsBaseBean implements java.io.Serializable{


	private static final long serialVersionUID = -563229263775383724L;
	/**
	 * 输出格式如:example.bean.UserBean@89ae9e[id=123,name=hello,password=123]
	 */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    
    

}
