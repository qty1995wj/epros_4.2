package epros.designer.gui.standard;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 新建标准
 * 
 * @author 2012-06-08
 * 
 */
public class AddStandarDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddStandarDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddStandarDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("newStandardDir");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		// 向StandardBean文件中添加数据
		StandardBean standardBean = new StandardBean();
		standardBean.setCriterionClassName(getName());
		standardBean.setPreCriterionClassId(pNode.getJecnTreeBean().getId());
		standardBean.setProjectId(JecnConstants.projectId);
		standardBean.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode)));
		standardBean.setStanType(0);
		standardBean.setIsPublic(JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubStand) ? 1L : 0L);
		standardBean.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		standardBean.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 向数据库中添加标准
		Long id = null;
		try {
			id = ConnectionPool.getStandardAction().addStandard(standardBean);
			// 向树节点添加标准节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardDir);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			// 关闭窗体
			this.dispose();
		} catch (Exception e) {
			log.error("AddStandarDialog saveData is error", e);
		}

	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.standardDir);
	}

	protected void validateEnglishName() {// 验证英文名称
	}

	protected void addEnNamePanel(Insets insets) {
	}

}
