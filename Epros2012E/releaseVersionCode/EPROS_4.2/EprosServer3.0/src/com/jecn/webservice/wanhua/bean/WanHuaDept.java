package com.jecn.webservice.wanhua.bean;

public class WanHuaDept {
	private String orgId;
	private String orgName;
	private String pOrgId;
	private String changeType;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPOrgId() {
		return pOrgId;
	}

	public void setPOrgId(String orgId) {
		pOrgId = orgId;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

}
