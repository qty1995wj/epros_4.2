package com.jecn.epros.domain.task;

import com.jecn.epros.domain.common.abolish.AbolishBean;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * Created by Administrator on 2017\12\11 0011.
 */
public class MyAbolishBean {

    @ApiModelProperty("总数")
    private Integer total = 0;

    @ApiModelProperty("任务列表")
    private List<AbolishBean> data;


    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<AbolishBean> getData() {
        return data;
    }

    public void setData(List<AbolishBean> data) {
        this.data = data;
    }
}
