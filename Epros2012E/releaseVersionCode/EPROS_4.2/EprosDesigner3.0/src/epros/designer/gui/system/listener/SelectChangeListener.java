package epros.designer.gui.system.listener;

public interface SelectChangeListener{
	public void actionPerformed(SelectChangeEvent e);
}
