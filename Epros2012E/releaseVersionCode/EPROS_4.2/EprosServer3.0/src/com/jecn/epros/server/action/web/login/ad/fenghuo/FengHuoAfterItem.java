package com.jecn.epros.server.action.web.login.ad.fenghuo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

/**
 * 烽火通信
 * 
 */
public class FengHuoAfterItem {

	private static Logger log = Logger.getLogger(FengHuoAfterItem.class);

	// domainIp
	public static String LDAP_URL = null;
	// 域后缀
	public static String DOMAIN = null;
	// 是否验证密码 (默认false)
	public static boolean VERIFY = false;

	public static String ADMIN_NAME = null;
	public static String ADMIN_PASSWORD = null;

	public static String FILTER = "";
	public static String BASE = "";

	/**
	 * 读取广机配置文件
	 * 
	 */
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/fengHuo/fengHuoConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			// AD服务器地址
			LDAP_URL = props.getProperty("ldapURL");

			// 域后缀
			FengHuoAfterItem.DOMAIN = props.getProperty("domain");

			ADMIN_NAME = props.getProperty("username");
			ADMIN_PASSWORD = props.getProperty("password");

			// 过滤条件
			FILTER = props.getProperty("filter");
			BASE = props.getProperty("base");

		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}
}
