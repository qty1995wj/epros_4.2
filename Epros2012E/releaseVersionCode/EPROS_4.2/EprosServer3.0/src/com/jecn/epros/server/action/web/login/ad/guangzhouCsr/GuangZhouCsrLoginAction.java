package com.jecn.epros.server.action.web.login.ad.guangzhouCsr;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class GuangZhouCsrLoginAction extends JecnAbstractADLoginAction {

	public GuangZhouCsrLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 广州机电单点登录入口
	 * 
	 * @desc 先验证是否存在用户名和密码，如果不存在走AD域验证
	 * 
	 * @author chehuanbo
	 * 
	 * @return String
	 */
	public String login() {
		try {
			// 单点登录名称
			String adLoginName = this.loginAction.getLoginName();
			// 单点登录密码
			String adPassWord = this.loginAction.getPassword();

			if ("admin".equals(adLoginName)) {
				return loginAction.userLoginGen(true);
			}
			String userName = loginAction.getRequest().getParameter("userName");
			if (StringUtils.isNotEmpty(userName)) {
				loginAction.setLoginName(userName);
				return loginAction.userLoginGen(false);
			}
			// 直接传参登陆
			if (!StringUtils.isBlank(adLoginName) && !StringUtils.isBlank(adPassWord)) {
				if (!doDomainAuth()) {
					log.error("域验证失败:" + adLoginName);
					return LoginAction.INPUT;
				}
				// 普通
				return loginAction.userLoginGen(false);
			} else {
				// 域登录
				return doDomainLogin();
			}

		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 域验证
	 * 
	 * @param userName
	 * @param pwd
	 * @return
	 */
	private boolean doDomainAuth() {
		DirContext ctx = null;
		String account = loginAction.getLoginName(); // 设置访问账号
		String password = loginAction.getPassword(); // 设置账号密码
		String host = JecnGuangZhouJiDianAfterItem.DOMAIN_IP; // AD服务器
		String root = "389"; // root
		String domain = JecnGuangZhouJiDianAfterItem.DOMAIN; // 邮箱的后缀名
		String user = account.indexOf(domain) > 0 ? account : account + domain;

		log.info("用户名==" + user);
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.PROVIDER_URL, "ldap://" + host + ":" + root);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user);
		env.put(Context.SECURITY_CREDENTIALS, password);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

		try {
			ctx = new InitialDirContext(env);
			log.info("AD域验证成功!");
		} catch (AuthenticationException e) {
			log.error("AD域验证失败！！！", e);
			return false;
		} catch (Exception e) {
			log.error("AD域验证异常！！！", e);
			return false;
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.error("InitialDirContext关闭异常...", e);
				}
			}
		}
		return true;
	}

	/**
	 * 域登录
	 * 
	 * @return
	 */
	private String doDomainLogin() {
		HttpServletRequest request = this.loginAction.getRequest();
		log.info("本地域用户：" + request.getRemoteUser());
		// 获取本地域信息
		String[] domainInfo = request.getRemoteUser() == null ? null : request.getRemoteUser().split("\\\\");
		// 获取域信息的情况下 进行域登录
		if (domainInfo != null) {
			log.info("domain:" + domainInfo[0]);
			if (!domainInfo[0].equals(JecnGuangZhouJiDianAfterItem.DOMAIN_INFO)) {
				log.info("用户不再指定的域：" + JecnGuangZhouJiDianAfterItem.DOMAIN_INFO + "中，用户所在域为：" + domainInfo[0]);
				return LoginAction.INPUT;
			}
			log.info("accountNum:" + domainInfo[1]);
			this.loginAction.setLoginName(domainInfo[1]);
			// 不验证密码登录（域账号存在于Epros中时登陆成功）
			return this.loginAction.userLoginGen(false);
		} else {
			// 返回登录页面
			return LoginAction.INPUT;
		}
	}
}
