package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.jecn.epros.server.action.web.login.ad.cetc29.JecnCetc29AfterItem;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DBConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroup;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroupRelatedPos;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;
import com.jecn.webservice.cetc29.intfsServiceWSXML;
import com.jecn.webservice.cetc29.intfsServiceWSXMLClient;

public class ImportUserByCetc29DB extends AbstractImportUser {

	private final Logger log = Logger.getLogger(ImportUserByCetc29DB.class);

	/** 部门 */
	private List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
	/** 岗位 */
	private List<UserBean> userBeanList = new ArrayList<UserBean>();
	/** 执行读取标识(true:表示执行过；false：标识没有执行过) */
	private boolean readedFlag = false;

	/** 岗位组 */
	private List<JecnPosGroup> posGroups = new ArrayList<JecnPosGroup>();
	/** 岗位组关联岗位 */
	private List<JecnPosGroupRelatedPos> groupRelatedPos = new ArrayList<JecnPosGroupRelatedPos>();

	/** 岗位类别 对应岗位组名称 */
	private Map<String, Set<String>> posTypeRelatedsGroupMaps = new HashMap<String, Set<String>>();
	/** 岗位组相关岗位 */
	private Map<String, Set<String>> groupRelatedPosMaps = new HashMap<String, Set<String>>();

	intfsServiceWSXML service = null;
	{
		intfsServiceWSXMLClient client = new intfsServiceWSXMLClient();
		// create a default service endpoint
		service = client.getintfsServiceWSXMLPort();
	}

	public ImportUserByCetc29DB(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 
	 * 获取部门数据集合
	 * 
	 * @return
	 */
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return deptBeanList;
	}

	/**
	 * 
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return userBeanList;
	}

	/**
	 * @throws BsException
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	private void readDB() throws Exception {
		// 执行过就不执行
		if (readedFlag) {
			return;
		}

		// Excel文件读取，部门、岗位、人员数据都全部取出来
		try {
			selectDB((DBConfigBean) configBean);
		} catch (Exception e) {
			throw new IOException(SyncErrorInfo.SELECT_DB_FAIL);

		}
	}

	/**
	 * 
	 * 查询数据库返回查询结果
	 * 
	 * @param configBean
	 */
	private void selectDB(DBConfigBean configBean) throws Exception {
		String resultDept = service.getModelDatasXML("Dept", JecnCetc29AfterItem.QUERY_STR_DEPT,
				JecnCetc29AfterItem.FIELD_STR_DEPT);
		readerXmlDept(resultDept);
		log.info("部门数据 : " + resultDept);

		// 人员岗位
		log.info("人员数据 FIELD_STR_USER: " + JecnCetc29AfterItem.FIELD_STR_USER + " JecnCetc29AfterItem.RC_CODE = "
				+ JecnCetc29AfterItem.RC_CODE);
		String resultUser = service.getModelDatasXML(JecnCetc29AfterItem.RC_CODE, JecnCetc29AfterItem.QUERY_STR_USER,
				JecnCetc29AfterItem.FIELD_STR_USER);
		log.info("人员数据 : " + resultUser);
		readerXmlUser(resultUser);
		readedFlag = true;
	}

	private String xmlString() {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<LIST>" + "<RY>" + "<ADCode>001</ADCode>"
				+ "       <USERNAME>部门1</USERNAME>" + "      <USERSTATION>SWIEE</USERSTATION>" + " </DEPT>" + "<RY>"
				+ "<ADCode>002</ADCode>" + "       <USERNAME>部门2</USERNAME>" + "      <USERSTATION>001</USERSTATION>"
				+ " </RY>" + "</LIST>";
	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resUser
	 */
	@SuppressWarnings("unchecked")
	private void readerXmlDept(String fileStr) throws Exception {
		Document document = getDocumentByXml(fileStr);
		if (document == null) {
			return;
		}
		// 根节点
		Element root = document.getRootElement();
		List<Element> rowsEle = root.elements(JecnCetc29AfterItem.DEPT_NODE_NAME);
		if (rowsEle.isEmpty()) {
			return;
		}
		String[] fieldNames = JecnCetc29AfterItem.XML_FIELD_DEPT.split(",");
		if (fieldNames.length != 3) {
			throw new IllegalArgumentException("获取xml人员字段异常！fieldNames = " + JecnCetc29AfterItem.XML_FIELD_DEPT);
		}
		DeptBean deptBean = null;
		// 0 有效
		String isUse = null;
		for (Element deptEle : rowsEle) {// 部门大节点
			List<Element> childEles = deptEle.elements();
			deptBean = new DeptBean();
			for (Element ele : childEles) {// 部门属性
				if ("ISSEAL_V".equals(ele.getName())) {
					isUse = ele.getStringValue();
				} else if (fieldNames[0].equals(ele.getName())) {
					deptBean.setDeptNum(ele.getStringValue());
				} else if (fieldNames[1].equals(ele.getName())) {
					deptBean.setDeptName(ele.getStringValue());
				} else if (fieldNames[2].equals(ele.getName())) {
					if (JecnCetc29AfterItem.DEPT_ROOT_NODE.equals(deptBean.getDeptNum())) {// 根节点-SWIEE
						deptBean.setPerDeptNum("0");
					} else {
						deptBean.setPerDeptNum(ele.getStringValue());
					}
				}
			}
			if (StringUtils.isBlank(deptBean.getDeptNum()) || "1".equals(isUse)) {
				continue;
			}
			deptBeanList.add(deptBean);
		}
	}

	@SuppressWarnings("unchecked")
	private void readerXmlUser(String fileStr) throws Exception {
		Document document = getDocumentByXml(fileStr);
		if (document == null) {
			return;
		}
		// 根节点
		Element root = document.getRootElement();
		List<Element> rowsEle = root.elements(JecnCetc29AfterItem.USER_NODE_NAME);
		if (rowsEle.isEmpty()) {
			return;
		}
		String[] fieldNames = JecnCetc29AfterItem.XML_FIELD_USER.split(",");
		if (fieldNames.length != 7) {
			throw new IllegalArgumentException("获取xml人员字段异常！fieldNames = " + JecnCetc29AfterItem.XML_FIELD_USER);
		}
		UserBean userBean = null;
		// 0 有效
		String isUse = null;
		for (Element userEle : rowsEle) {
			userBean = new UserBean();
			List<Element> childEles = userEle.elements();
			String posType = "";
			for (Element ele : childEles) {// 人员属性
				if ("ISSEAL_V".equals(ele.getName())) {// 1;为无效人员
					isUse = ele.getStringValue();
				} else if (fieldNames[0].equals(ele.getName())) {
					userBean.setUserNum(ele.getStringValue());
				} else if (fieldNames[2].equals(ele.getName())) {// 岗位类别
					if (StringUtils.isNotBlank(ele.getStringValue())
							&& !posTypeRelatedsGroupMaps.containsKey(ele.getStringValue())) {
						posTypeRelatedsGroupMaps.put(ele.getStringValue(), new HashSet<String>());
					}
					posType = ele.getStringValue();
				} else if (fieldNames[1].equals(ele.getName())) {
					userBean.setTrueName(ele.getStringValue());
				} else if (fieldNames[3].equals(ele.getName())) {
					userBean.setPosName(ele.getStringValue());
				} else if (fieldNames[4].equals(ele.getName())) {
					userBean.setDeptNum(ele.getStringValue());
				} else if (fieldNames[5].equals(ele.getName())) {
					userBean.setEmail(ele.getStringValue());
					if (StringUtils.isNotBlank(userBean.getEmail())) {
						// 内网:0 外网:1
						userBean.setEmailType(0);
					}
				} else if (fieldNames[6].equals(ele.getName())) {
					userBean.setPhone(ele.getStringValue());
				}
			}

			if (StringUtils.isBlank(userBean.getUserNum()) || "1".equals(isUse)) {
				continue;
			}

			// 添加岗位组，岗位对应关系
			addGroupPosInfo(userBean, posType);

			userBeanList.add(userBean);
		}

		// 创建岗位组目录及岗位组
		createGroupPos();
		// 创建岗位组岗位相关关系
		createGroupRelatedPos();
	}

	// 创建岗位组岗位相关关系
	private void createGroupRelatedPos() {
		if (groupRelatedPosMaps == null || groupRelatedPosMaps.values() == null) {
			return;
		}
		for (Entry<String, Set<String>> entry : groupRelatedPosMaps.entrySet()) {
			for (String string : entry.getValue()) {
				JecnPosGroupRelatedPos groupRelPos = new JecnPosGroupRelatedPos();
				groupRelPos.setPosGroupNum(entry.getKey());
				groupRelPos.setPosNum(string);
				groupRelatedPos.add(groupRelPos);
			}
		}
	}

	private void createGroupPos() {
		JecnPosGroup posGroup = null;
		for (Entry<String, Set<String>> entry : posTypeRelatedsGroupMaps.entrySet()) {
			posGroup = new JecnPosGroup();
			posGroup.setIsDir(1);
			posGroup.setName(entry.getKey());
			posGroup.setNum(entry.getKey());
			posGroup.setParentNum("0");

			posGroups.add(posGroup);
			// 添加岗位组
			addGroupPos(entry.getValue(), posGroup.getNum());
		}
	}

	private void addGroupPos(Set<String> groupNames, String parentNum) {
		if (groupNames == null) {
			return;
		}
		JecnPosGroup posGroup = null;
		for (String string : groupNames) {
			posGroup = new JecnPosGroup();
			posGroup.setIsDir(0);
			posGroup.setName(string);
			posGroup.setNum(string);
			posGroup.setParentNum(parentNum);
			posGroups.add(posGroup);
		}
	}

	private void addGroupPosInfo(UserBean userBean, String posType) {
		if (StringUtils.isNotBlank(userBean.getDeptNum()) && StringUtils.isNotBlank(userBean.getPosName())) {
			userBean.setPosNum(userBean.getPosName() + "-" + userBean.getDeptNum());

			if (!groupRelatedPosMaps.containsKey(userBean.getPosName())) {
				groupRelatedPosMaps.put(userBean.getPosName(), new HashSet<String>());
			}
			groupRelatedPosMaps.get(userBean.getPosName()).add(userBean.getPosNum());
			// 岗位类别相关岗位组名称
			if (StringUtils.isNotBlank(posType)) {
				posTypeRelatedsGroupMaps.get(posType).add(userBean.getPosName());
			}
		} else {
			userBean.setDeptNum(null);
			userBean.setPosName(null);
		}
	}

	private Document getDocumentByXml(String fileStr) {
		try {
			return DocumentHelper.parseText(fileStr);
		} catch (Exception e) {
			log.error("dom4J 获取Document异常！", e);
			return null;
		}
	}

	@Override
	public List<JecnPosGroupRelatedPos> getGroupRelatedPos() throws Exception {
		return groupRelatedPos;
	}

	@Override
	public List<JecnPosGroup> getPosGroups() throws Exception {
		return posGroups;
	}
}
