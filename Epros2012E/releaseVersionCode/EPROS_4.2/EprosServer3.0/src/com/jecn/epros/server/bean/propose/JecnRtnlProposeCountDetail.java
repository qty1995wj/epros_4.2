package com.jecn.epros.server.bean.propose;

/**
 * 合理化建议统计详细信息bean
 * @author hyl
 *
 */
public class JecnRtnlProposeCountDetail {
	
	/** 合理化建议id*/
	private String id;
	/** 非合理化建议name,而是显示的名称，例如，如果是创建人的统计那么name为创建人的姓名*/
	private String name;
	/** 由于在jfchart中如果横坐标的名称相同，则显示异常，而createName或者orgName为横坐标，所以需要一个不重复的名称**/
	private String sheetName;
	
	/** 合理化建议创建人*/
	private String createName;
	/** 部门名称*/
	private String orgName;
	/** 合理化建议内容*/
	private String rtnlContent;
	/** 日期*/
	private String strDate;
	/** 采纳率 显示带%**/
	private String acceptPercentStr;
	/** 采纳率**/
	private int acceptPercent=0;
	/** 提交的总数**/
	private int total=0;
	/** 采纳的总数**/
	private int acceptTotal=0;
	/** 流程/制度名称*/
	private String relateName;
	/** 流程/制度id*/
	private String relateId;
	/** 流程/制度创建时间*/
	private String createTime;
	
	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getRtnlContent() {
		return rtnlContent;
	}

	public void setRtnlContent(String rtnlContent) {
		this.rtnlContent = rtnlContent;
	}

	public String getStrDate() {
		return strDate;
	}

	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

	public String getAcceptPercentStr() {
		return acceptPercentStr;
	}

	public void setAcceptPercentStr(String acceptPercentStr) {
		this.acceptPercentStr = acceptPercentStr;
	}

	public int getAcceptPercent() {
		return acceptPercent;
	}

	public void setAcceptPercent(int acceptPercent) {
		this.acceptPercent = acceptPercent;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getAcceptTotal() {
		return acceptTotal;
	}

	public void setAcceptTotal(int acceptTotal) {
		this.acceptTotal = acceptTotal;
	}

	public String getRelateName() {
		return relateName;
	}

	public void setRelateName(String relateName) {
		this.relateName = relateName;
	}

	public String getRelateId() {
		return relateId;
	}

	public void setRelateId(String relateId) {
		this.relateId = relateId;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	
		

}
