package com.jecn.epros.controller;

import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.propose.*;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.IMixRPCService;
import com.jecn.epros.service.propose.ProposeService;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.AttachmentDownloadUtils;
import com.jecn.epros.util.ConfigUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("suggest")
@Api(value = "/suggest", description = "合理化建议", position = 8)
public class ProposeController {

    @Autowired
    private ProposeService proposeService;
    @Autowired
    private IMixRPCService mixRPCService;

    @ApiOperation("点击一条主题，然后打开该主题下的所有的合理化建议")
    @RequestMapping(value = "/suggests", method = RequestMethod.GET)
    public ResponseEntity<ProposeView> fetchPropose(@ModelAttribute ProposeSearch param) {

        if (param.getRelatedId() == null || param.getType() == null) {
            throw new IllegalArgumentException("ProposeController fetchPropose  is error");
        }

        Boolean isAdmin = AuthenticatedUserUtil.isAdmin();
        Long loginPeopleId = AuthenticatedUserUtil.getPeopleId();

        ProposeView view = new ProposeView();

        // 未读建议
        Integer unReadNum = proposeService.getProposeNum(param, 0, isAdmin, loginPeopleId);
        // 已读建议
        Integer readNum = proposeService.getProposeNum(param, 1, isAdmin, loginPeopleId);
        // 采纳建议
        Integer acceptNum = proposeService.getProposeNum(param, 2, isAdmin, loginPeopleId);
        // 拒绝建议
        Integer refuseNum = proposeService.getProposeNum(param, 3, isAdmin, loginPeopleId);

        // 根据页面所在的tab
        List<Propose> proposes = proposeService.fetchProposes(param, isAdmin, loginPeopleId);

        view.setAllNum(unReadNum + readNum + acceptNum + refuseNum);
        view.setUnReadNum(unReadNum);
        view.setReadNum(readNum);
        view.setAcceptNum(acceptNum);
        view.setRefuseNum(refuseNum);
        view.setProposes(transferProposesToShow(proposes));
        view.setCurState(param.getCurState());
        return ResponseEntity.ok(view);
    }

    private List<ProposePageView> transferProposesToShow(List<Propose> proposes) {
        List<ProposePageView> showProposes = new ArrayList<ProposePageView>();
        for (Propose propose : proposes) {
            showProposes.add(transferProposeToShow(propose));
        }
        return showProposes;
    }

    private ProposePageView transferProposeToShow(Propose propose) {
        ProposePageView showPropose = new ProposePageView();
        showPropose.setId(propose.getId());
        showPropose.setType(propose.getType());
        showPropose.setRelationId(propose.getRelationId());
        showPropose.setRelationName(propose.getRelationName());
        showPropose.setProposeContent(propose.getProposeContent());
        showPropose.setReplyContent(propose.getReplyContent());
        showPropose.setState(propose.getState());
        showPropose.setReplyPeopleId(propose.getReplyPersonId());
        showPropose.setReplyName(propose.getReplyName());
        showPropose.setReplyTime(propose.getReplyTime());
        showPropose.setCreatePeopleId(propose.getCreatePersonId());
        showPropose.setCreatePeopleName(propose.getCreatePeopoleName());
        showPropose.setCreateTime(propose.getCreateTime());
        showPropose.setProposeFile(propose.getProposeFile());
        showPropose.setButtonShowConfig(propose.getButtonShowConfig());
        showPropose.setShowType(propose.getShowType());
        return showPropose;
    }

    /**
     * 根据前台传回来的数据将单个的合理化建议更改为需要的值 如果是回复消息需要传递strContent，如果不是回复消息不能够传递这个字段
     *
     * @return
     * @author hyl
     */
    @ApiOperation("更改合理化建议的状态")
    @RequestMapping(value = "handle", method = RequestMethod.POST)
    public ResponseEntity<?> updateSingleState(@RequestBody ProposeUpdateState updateBean) {
        JecnUser jecnUser = new JecnUser();
        jecnUser.setId(AuthenticatedUserUtil.getPeopleId());

        Set<Integer> stateSet = new HashSet<Integer>();
        stateSet.add(Propose.ACCEPT);
        stateSet.add(Propose.READ);
        stateSet.add(Propose.REFUSE);
        // 校验
        if (updateBean == null || updateBean.getProposeId() == null || updateBean.getState() == null
                || !stateSet.contains(updateBean.getState())) {
            throw new IllegalArgumentException("ProposeController updateSingleState is error");
        }
        int isSuccess = proposeService.updateProposeState(updateBean.getProposeId(), updateBean.getState(), null,
                jecnUser);
        if (isSuccess == Propose.SUCCESS && (updateBean.getState() == Propose.ACCEPT || updateBean.getState() == Propose.REFUSE) && ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.mailProposeToSubmit)) {
            mixRPCService.handleProposeSendEmail(updateBean.getProposeId(), updateBean.getState());
        }
        return ResponseEntity.ok(isSuccess);
    }

    /**
     * 回复消息
     *
     * @return
     * @author hyl
     */
    @ApiOperation("回复消息")
    @RequestMapping(value = "reply", method = RequestMethod.POST)
    public ResponseEntity<?> reply(@RequestBody ProposeReply updateBean) {
        JecnUser jecnUser = new JecnUser();
        jecnUser.setId(AuthenticatedUserUtil.getPeopleId());
        // 校验
        if (updateBean == null || updateBean.getProposeId() == null) {
            throw new IllegalArgumentException("ProposeController reply is error");
        }
        int isSuccess = proposeService.updateProposeState(updateBean.getProposeId(), Propose.READ,
                updateBean.getStrContent(), jecnUser);
        return ResponseEntity.ok(isSuccess);
    }

    @ApiOperation("提交合理化建议")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    public ResponseEntity<?> createPropose(@ModelAttribute ProposeCreate param) {
        if (param.getId() == null || param.getType() == null || param.getShowType() == null) {
            throw new NullPointerException(
                    "ProposeController createPropose is error:id：" + param.getId() + "type：" + param.getType() + "showType" + param.getShowType());
        }
        Long peopleId = AuthenticatedUserUtil.getPeopleId();
        Propose propose = proposeService.savePropose(param, peopleId);
        // 提交合理化建议之后发送邮件
        submitSendEmail(propose);
        return ResponseEntity.ok().build();
    }

    private void submitSendEmail(Propose propose) {
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.mailProposeToHandle)) {
            mixRPCService.submitProposeSendEmail(propose.getId());
        }
    }


    @ApiOperation("编辑合理化建议")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResponseEntity<?> editPropose(@ModelAttribute ProposeEdit param) {

        if (param == null) {
            throw new NullPointerException("ProposeController editPropose is error");
        }

        if (param.getProposeId() == null) {
            throw new NullPointerException("ProposeController editPropose is error");
        }

        Long peopleId = AuthenticatedUserUtil.getPeopleId();
        int isSuccess = proposeService.editPropose(param, peopleId);
        return ResponseEntity.ok(isSuccess);
    }

    @ApiOperation("删除合理化建议的回复内容")
    @RequestMapping(value = "clearReply", method = RequestMethod.GET)
    public void clearReply(@RequestParam String proposeId) {
        if (proposeId == null) {
            throw new NullPointerException("ProposeController clearReply is error");
        }
        proposeService.clearReply(proposeId);
    }

    @ApiOperation("删除合理化建议")
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ResponseEntity<?> deletePropose(@RequestParam String proposeId) {

        if (proposeId == null) {
            throw new NullPointerException("ProposeController deletePropose is error");
        }
        // 登录人
        Long peopleId = AuthenticatedUserUtil.getPeopleId();
        boolean isAdmin = AuthenticatedUserUtil.isAdmin();

        int result = proposeService.deletePropose(proposeId, peopleId, isAdmin);
        return ResponseEntity.ok(result);
    }

    @ApiOperation("主题界面搜索")
    @RequestMapping(value = "topicSearch", method = RequestMethod.GET)
    public ResponseEntity<?> topicSearch(@ModelAttribute TopicSearch topicSearch, @ModelAttribute Paging paging) {

        Long peopleId = AuthenticatedUserUtil.getPeopleId();
        boolean isAdmin = AuthenticatedUserUtil.isAdmin() || AuthenticatedUserUtil.isViewAdmin();
        int total = proposeService.searchProposeTopicNum(topicSearch, isAdmin, peopleId);
        List<ProposeTopic> topics = new ArrayList<ProposeTopic>();
        if (total > 0) {
            topics = proposeService.searchProposeTopic(topicSearch, isAdmin, peopleId, paging);
        }
        Map<String, Object> view = new HashMap<String, Object>();
        view.put("data", topics);
        view.put("total", total);
        return ResponseEntity.ok(view);
    }

    @ApiOperation("我的主页的合理化建议")
    @RequestMapping(value = "my", method = RequestMethod.GET)
    public ResponseEntity<MyProposeTopic> myProposes(@RequestParam Boolean isPaging) {
        if (isPaging == null) {
            isPaging = true;
        }
        Long peopleId = AuthenticatedUserUtil.getPeopleId();
        MyProposeTopic myPropose = proposeService.getMyProposesTopic(peopleId, isPaging);
        return ResponseEntity.ok(myPropose);
    }

    /**
     * 显示需要分配处理人的主题和未分配处理人的主题(只有管理员可见)
     *
     * @param type 0需要分配处理人的主题 1已经分配处理人的主题
     * @return
     * @throws Exception
     */
    @ApiOperation("显示需要分配处理人的主题和未分配处理人的主题(只有管理员可见)")
    @RequestMapping(value = "topicShow", method = RequestMethod.GET)
    public ResponseEntity<?> topicShow(@RequestParam Integer type, @ModelAttribute Paging paging) {
        if (type == null || (type != 0 && type != 1)) {
            throw new IllegalArgumentException("ProposeController topicShow is error，type=" + type);
        }
        int total = proposeService.searchProposeTopicNum(type);
        List<ProposeTopic> topics = new ArrayList<ProposeTopic>();
        if (total > 0) {
            topics = proposeService.searchProposeTopic(type, paging);
        }
        Map<String, Object> view = new HashMap<String, Object>();
        view.put("total", total);
        view.put("topics", topics);
        return ResponseEntity.ok(view);
    }

    @ApiOperation("给合理化建议分配处理人")
    @RequestMapping(value = "distribute", method = RequestMethod.POST)
    public ResponseEntity<?> distributeHandlePeople(@RequestBody ProposeDistributeHandlePeople param) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("relatedId", param.getRelatedId());
        map.put("relatedType", param.getType());
        map.put("peopleId", param.getHandlePeople());
        proposeService.distributeHandlePeople(map);
        return ResponseEntity.ok().build();
    }

    @ApiOperation("合理化建议统计图表")
    @RequestMapping(value = "totalChartSearch", method = RequestMethod.GET)
    public ResponseEntity<ProposeTotalChart> findProposeTotalChart(
            @RequestBody ProposeTotalSearchBean proposeTotalSearchBean) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("proposeTotalSearchBean", proposeTotalSearchBean);
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        return ResponseEntity.ok(proposeService.findProposeTotalChart(map));
    }

    @ApiOperation("合理化建议统计")
    @RequestMapping(value = "totalSearch", method = RequestMethod.GET)
    public ResponseEntity<ProposeTotalView> proposeTotal(@ModelAttribute ProposeTotalSearchBean proposeTotalSearchBean,
                                                         @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("proposeTotalSearchBean", proposeTotalSearchBean);
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("paging", paging);
        ProposeTotalView proposeTotalView = new ProposeTotalView();
        int total = proposeService.findProposeTotal(map);
        proposeTotalView.setTotal(total);
        if (total > 0) {
            proposeTotalView.setListProposeTotalData(proposeService.findProposeTotalData(map));
        }
        return ResponseEntity.ok(proposeTotalView);
    }

    @ApiOperation("下载合理化建议附件")
    @RequestMapping(value = "downloadProposeFile", method = RequestMethod.GET)
    public ResponseEntity<?> downloadProposeFile(String proposeId, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("proposeId", proposeId);
        List<ProposeFile> files = proposeService.getProposeFileByProposeId(map);
        if (files.size() == 0) {
            throw new EprosBussinessException("ProposeController downloadProposeFile is error id:" + proposeId);
        }
        Attachment attachment = new Attachment();
        attachment.setContent(files.get(0).getFileStream());
        attachment.setName(files.get(0).getFileName());
        return AttachmentDownloadUtils.newResponseEntity(request, attachment);
    }

    @ApiOperation("获得主题的处理人")
    @RequestMapping(value = "topicHandler", method = RequestMethod.GET)
    public ResponseEntity<?> topicHandler(@RequestParam(value = "relatedId", required = true) String relatedId, @RequestParam(value = "relatedType", required = true) String relatedType) {
        Map<String, Object> params = new HashMap<>();
        params.put("relatedId", relatedId);
        params.put("relatedType", relatedType);
        JecnUser user = proposeService.getTopicHandler(params);
        if (user != null) {
            user.setPassword(null);
        }
        return ResponseEntity.ok(user);
    }
}
