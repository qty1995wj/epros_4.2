package com.jecn.epros.server.bean.task;

import java.util.Date;

public class JecnAbolishBean {
	/**
	 * 关联Id
	 */
	Long rId;
	/**
	 *  tType= 0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,3:制度文件ID，4：流程地图ID
	 */
	int type;
	/**
	 * 关联名称
	 */
	String name;
	/**
	 * 任务Id
	 */
	Long taskId;
	/**
	 * 废止时间 
	 */
	Date abolishDate;
	/**
	 * 文件T_PATH
	 */
	String path;
	/**
	 * 关联编号
	 */
	String fileNum;
	/**
	 * 关联关键字
	 */
	String keyWord;
	
	public Long getrId() {
		return rId;
	}
	public void setrId(Long rId) {
		this.rId = rId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public Date getAbolishDate() {
		return abolishDate;
	}
	public void setAbolishDate(Date abolishDate) {
		this.abolishDate = abolishDate;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFileNum() {
		return fileNum;
	}
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
	
	
	
	
	
	
}
