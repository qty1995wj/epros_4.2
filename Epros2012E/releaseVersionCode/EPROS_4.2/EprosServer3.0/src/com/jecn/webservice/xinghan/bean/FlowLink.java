package com.jecn.webservice.xinghan.bean;

import java.util.List;

/**
 * 流程线属性bean
 * 
 * @author admin
 * 
 */
public class FlowLink {
	/*** 线ID ****/
	private long linkId;
	/*** 线名称 ****/
	private String linkName;
	/*** 线条类型（1返工符，0否） *******/
	private int linkType;
	/*** From节点 ******/
	private long fromNodeId;
	/*** To节点 ******/
	private long toNodeId;
	/**** 连接点的集合 ***/
	private List<Point> point;

	public String getLinkId() {
		if (linkId == 0) {
			return "";
		}
		return String.valueOf(linkId);
	}

	public void setLinkId(long linkId) {
		this.linkId = linkId;
	}

	public String getLinkType() {
		return String.valueOf(linkType);
	}

	public void setLinkType(int linkType) {
		this.linkType = linkType;
	}

	public String getFromNodeId() {
		if (fromNodeId == 0) {
			return "";
		}
		return String.valueOf(fromNodeId);
	}

	public void setFromNodeId(long fromNodeId) {
		this.fromNodeId = fromNodeId;
	}

	public String getToNodeId() {
		if (toNodeId == 0) {
			return "";
		}
		return String.valueOf(toNodeId);
	}

	public void setToNodeId(long toNodeId) {
		this.toNodeId = toNodeId;
	}

	public String getLinkName() {
		if(linkName==null){
			return "";
		}
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public List<Point> getPoint() {
		return point;
	}

	public void setPoint(List<Point> point) {
		this.point = point;
	}

}
