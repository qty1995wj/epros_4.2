package com.jecn.epros.server.service.task;

/**
 * 任务数据处理sql
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jan 14, 2013 时间：4:48:35 PM
 */
public class TaskCommonSql {
	/** 发送试用行报告的sql SQLSERVER */
	public static String SEND_TEST_RUN_REPORT_SQL_SQLSERVER = "select t.*, "
			+ "      ju.people_id,ju.login_name,ju.true_name,ju.email ,ju.email_type"
			+ "      from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id"
			+ "      where datediff(day,t.create_time,getdate())=t.send_runtime_value"
			+ "      and t.flow_type=0";

	/** 发送试用行报告的sql ORACLE */
	public static String SEND_TEST_RUN_REPORT_SQL_ORACLE = "select t.*, "
			+ "      ju.people_id,ju.login_name,ju.true_name,ju.email ,ju.email_type"
			+ "      from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id"
			+ "	   where (to_date (to_char(sysdate, 'yyyy-mm-dd'),'yyyy-mm-dd') -"
			+ "      to_date(to_char(t.create_time,'yyyy-mm-dd'),'yyyy-mm-dd'))=t.send_runtime_value"
			+ "      and t.flow_type=0";

	/** 发送试用行结束报告的sql SQLSERVER */
	public static String SEND_TEST_RUN_END_SQL_SQLSERVER = "select t.*, "
			+ "      ju.people_id,ju.login_name,ju.true_name,ju.email ,ju.email_type"
			+ "      from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id"
			+ "      where datediff(day,t.TEST_RUN_TIME,getdate())=0"
			+ "      and t.flow_type=0";

	/** 发送试用行结束报告的sql ORACLE */
	public static String SEND_TEST_RUN_END_SQL_ORACLE = "select t.*, "
			+ "      ju.people_id,ju.login_name,ju.true_name,ju.email ,ju.email_type"
			+ "      from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id"
			+ "	   where to_char(t.TEST_RUN_TIME, 'yyyy-mm-dd')=to_char(sysdate,'yyyy-mm-dd')"
			+ "      and t.flow_type=0";
}
