package com.jecn.epros.server.connector.Libang;

import java.util.List;

public class LibangDeleteReceivers {
	private String AppCode;
	private String RefID;
	private List<String> Receivers;

	public String getAppCode() {
		return AppCode;
	}

	public void setAppCode(String appCode) {
		AppCode = appCode;
	}

	public String getRefID() {
		return RefID;
	}

	public void setRefID(String refID) {
		RefID = refID;
	}

	public List<String> getReceivers() {
		return Receivers;
	}

	public void setReceivers(List<String> receivers) {
		Receivers = receivers;
	}

}
