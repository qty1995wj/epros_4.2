package com.jecn.epros.server.service.file.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import oracle.sql.BLOB;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.file.FileAttributeBean;
import com.jecn.epros.server.bean.file.FileInfoBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileRelatedRiskT;
import com.jecn.epros.server.bean.file.FileRelatedStandardT;
import com.jecn.epros.server.bean.file.FileUseInfoBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.FlowStandardizedFileT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.rule.RuleStandardizedFileT;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.JecnSqlConstant;
import com.jecn.epros.server.dao.autoCode.IAutoCodeDao;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileContentDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IOrganizationDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.popedom.IPosGroupnAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.standard.IStandardDao;
import com.jecn.epros.server.download.excel.ExcelFont;
import com.jecn.epros.server.download.excel.SetExcelStyle;
import com.jecn.epros.server.download.word.SelectDownloadProcess;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.process.buss.FullPathManager;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.file.FileBean;
import com.jecn.epros.server.webBean.file.FileDetailListWebBean;
import com.jecn.epros.server.webBean.file.FileDetailRelatedWebBean;
import com.jecn.epros.server.webBean.file.FileDetailWebBean;
import com.jecn.epros.server.webBean.file.FileSearchBean;
import com.jecn.epros.server.webBean.file.FileUseBean;
import com.sun.xml.internal.txw2.IllegalAnnotationException;

/**
 * @author yxw 2013-1-22
 * @description：文件服务层
 */
@Transactional(rollbackFor = Exception.class)
public class FileServiceImpl extends AbsBaseService<JecnFileBeanT, Long> implements IFileService {

	private Logger log = Logger.getLogger(FileServiceImpl.class);
	private IFileDao fileDao;
	private IFileContentDao fileContentDao;
	private IOrgAccessPermissionsDao orgAccessPermissionsDao;
	private IPositionAccessPermissionsDao positionAccessPermissionsDao;
	private IOrganizationDao organizationDao;
	private IFlowStructureDao flowStructureDao;
	private IRuleDao ruleDao;
	private IStandardDao standardDao;
	private IPersonDao personDao;
	private IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao;
	private IJecnDocControlDao docControlDao;
	@Resource
	private IAutoCodeDao autoCodeDao;

	public IPosGroupnAccessPermissionsDao getPosGroupAccessPermissionsDao() {
		return posGroupAccessPermissionsDao;
	}

	public void setPosGroupAccessPermissionsDao(IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao) {
		this.posGroupAccessPermissionsDao = posGroupAccessPermissionsDao;
	}

	public IPersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(IPersonDao personDao) {
		this.personDao = personDao;
	}

	public void setStandardDao(IStandardDao standardDao) {
		this.standardDao = standardDao;
	}

	public void setRuleDao(IRuleDao ruleDao) {
		this.ruleDao = ruleDao;
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
		this.baseDao = fileDao;
	}

	public IFileContentDao getFileContentDao() {
		return fileContentDao;
	}

	public void setFileContentDao(IFileContentDao fileContentDao) {
		this.fileContentDao = fileContentDao;
	}

	public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
		this.orgAccessPermissionsDao = orgAccessPermissionsDao;
	}

	public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
		this.positionAccessPermissionsDao = positionAccessPermissionsDao;
	}

	public void setDocControlDao(IJecnDocControlDao docControlDao) {
		this.docControlDao = docControlDao;
	}

	@Override
	public void reFileDirName(String name, Long id, Long updatePersonId) throws Exception {
		try {
			// 更新文件临时表
			String hql = "update JecnFileBeanT set updateTime=?,updatePersonId=?,fileName=? where fileID=?";
			fileDao.execteHql(hql, new Date(), updatePersonId, name, id);
			// 更新文件表
			hql = "update JecnFileBean set updateTime=?,updatePersonId=?,fileName=? where fileID=?";
			fileDao.execteHql(hql, new Date(), updatePersonId, name, id);
			// 记录日志
			JecnUtil.saveJecnJournal(id, name, 2, 2, updatePersonId, fileDao, 4);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void addFiles(List<JecnFileBeanT> fileList, AccessId accId, boolean isPub, int codeTotal) throws Exception {
		try {
			Long ruleId = null;
			Long flowId = null;
			List<Long> fileIds = new ArrayList<Long>();
			for (JecnFileBeanT jecnFileBeanT : fileList) {
				// 保存文件到主类
				jecnFileBeanT.setCreateTime(new Date());
				jecnFileBeanT.setUpdateTime(new Date());
				/** 版本到本地 */
				if (JecnContants.fileSaveType == 0) {
					jecnFileBeanT.setSaveType(0);
				} else {
					jecnFileBeanT.setSaveType(1);
				}
				fileDao.save(jecnFileBeanT);
				fileIds.add(jecnFileBeanT.getFileID());
				if (jecnFileBeanT.getFlowId() != null) {
					flowId = jecnFileBeanT.getFlowId();
				} else if (jecnFileBeanT.getRuleId() != null) {
					ruleId = jecnFileBeanT.getRuleId();
				}
				if (isPub) {
					orgAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1, accId.getOrgAccessId(), 0, 0,
							jecnFileBeanT.getProjectId(), TreeNodeType.file);
					positionAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1, accId.getPosAccessId(), 0,
							0, jecnFileBeanT.getProjectId(), TreeNodeType.file);
					posGroupAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1,
							accId.getPosGroupAccessId(), 0, 0, jecnFileBeanT.getProjectId(), TreeNodeType.file);
				} else {
					// 保存岗位查阅权限
					positionAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1, accId.getPosAccessId(),
							true);

					// 保存部门查阅权限
					orgAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1, accId.getOrgAccessId(), true);
					posGroupAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1,
							accId.getPosGroupAccessId(), true);
				}
				// 记录文件内容
				JecnFileContent jecnFileContent = new JecnFileContent();
				jecnFileContent.setFileId(jecnFileBeanT.getFileID());
				jecnFileContent.setFileName(jecnFileBeanT.getFileName());
				jecnFileContent.setUpdatePeopleId(jecnFileBeanT.getUpdatePersonId());
				jecnFileContent.setUpdateTime(jecnFileBeanT.getUpdateTime());
				if (isPub) {
					jecnFileContent.setType(1);
				} else {
					jecnFileContent.setType(0);
				}
				// 本地
				if (JecnContants.fileSaveType == 0) {
					// 文件后缀名称
					String suffixName = JecnFinal.getSuffixName(jecnFileBeanT.getFileName());
					// 保存数据库路径
					String filePath = JecnFinal.saveFilePath(JecnFinal.FILE_LIBRARY, new Date(), suffixName);
					jecnFileContent.setFilePath(filePath);
					// 创建文件
					JecnFinal.createFile(filePath, jecnFileBeanT.getFileStream());
					jecnFileContent.setIsVersionLocal(0);
					fileDao.getSession().save(jecnFileContent);

					jecnFileBeanT.setVersionId(jecnFileContent.getId());
					fileDao.update(jecnFileBeanT);
				}/** 数据库 */
				else if (JecnContants.fileSaveType == 1) {
					// 保存数据库
					jecnFileContent.setIsVersionLocal(1);
					if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
						jecnFileContent.setFileStream(Hibernate.createBlob(jecnFileBeanT.getFileStream()));
						fileDao.getSession().save(jecnFileContent);
					} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
						jecnFileContent.setFileStream(Hibernate.createBlob(new byte[1]));
						fileDao.getSession().save(jecnFileContent);
						fileDao.getSession().flush();
						fileDao.getSession().refresh(jecnFileContent, LockMode.UPGRADE);
						SerializableBlob sb = (SerializableBlob) jecnFileContent.getFileStream();
						java.sql.Blob wrapBlob = sb.getWrappedBlob();
						BLOB blob = (BLOB) wrapBlob;
						try {
							OutputStream out = blob.getBinaryOutputStream();
							out.write(jecnFileBeanT.getFileStream());
							out.close();
						} catch (SQLException ex) {
							log.error("写blob大字段错误！", ex);
							throw ex;
						} catch (IOException ex) {
							log.error("写blob大字段错误！", ex);
							throw ex;
						}
					}
				}
				updateTpathAndTLevel(jecnFileBeanT);
				// 更新文件对应版本号
				jecnFileBeanT.setVersionId(jecnFileContent.getId());
				fileDao.update(jecnFileBeanT);
				if (isPub) {
					String sql = "insert into jecn_file select * from jecn_file_t where file_id=?";
					fileDao.execteNative(sql, jecnFileBeanT.getFileID());
				}
				JecnUtil.saveJecnJournal(jecnFileBeanT.getFileID(), jecnFileBeanT.getFileName(), 2, 1, jecnFileBeanT
						.getUpdatePersonId(), fileDao);
			}

			JecnTaskCommon.inheritTaskTemplate(flowStructureDao, fileIds, 1, fileList.get(0).getPeopleID());

			if (JecnConfigTool.isRuleArchitectureUploadFile() && ruleId != null) {
				// 制度相关标准化文件
				addRuleStandardFile(ruleId, fileIds);
			} else if (JecnConfigTool.isArchitectureUploadFile() && flowId != null) {
				addFlowStandardFile(flowId, fileIds);
			}

			if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowAutoCode) && JecnConfigTool.isLiBangLoginType()) {
				autoCodeDao.updateAutoCodeTableByRelatedId(fileList.get(0).getPerFileId(), 4, codeTotal);
			} else
			// 标准 自动编号
			if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {
				autoCodeDao.updateStandardizedAutoCode(fileList.get(0).getPerFileId(), 1, codeTotal);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private void addFlowStandardFile(Long flowId, List<Long> fileIds) {
		FlowStandardizedFileT standardizedFile = null;
		for (Long fileId : fileIds) {
			standardizedFile = new FlowStandardizedFileT();
			standardizedFile.setFlowId(flowId);
			standardizedFile.setFileId(fileId);
			standardizedFile.setId(JecnCommon.getUUID());
			flowStructureDao.getSession().save(standardizedFile);
		}
	}

	private void addRuleStandardFile(Long ruleId, List<Long> fileIds) {
		RuleStandardizedFileT standardizedFile = null;
		for (Long fileId : fileIds) {
			standardizedFile = new RuleStandardizedFileT();
			standardizedFile.setRelatedId(ruleId);
			standardizedFile.setFileId(fileId);
			standardizedFile.setId(JecnCommon.getUUID());
			ruleDao.getSession().save(standardizedFile);
		}
	}

	/**
	 * 文件对象转换成JecnTreeBean
	 * 
	 * @param obj
	 * @return
	 */
	private JecnTreeBean getJecnTreeBeanByObjectNoDel(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null
				|| obj[8] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		if ("0".equals(obj[3].toString())) {
			// 文件目录
			jecnTreeBean.setTreeNodeType(TreeNodeType.fileDir);
		} else {
			// 角色节点
			jecnTreeBean.setTreeNodeType(TreeNodeType.file);
		}
		// 文件节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 文件节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 文件节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 文件节点父类名称
		jecnTreeBean.setPname("");
		if (Integer.parseInt(obj[4].toString()) > 0) {
			// 文件节点是否有子节点
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}
		// 文件的创建时间
		if (obj[5] != null) {
			jecnTreeBean.setCreateTime((Date) obj[5]);
		}
		if (obj[6] != null) {
			// 保存数据的方式
			jecnTreeBean.setSaveType(Integer.valueOf(obj[6].toString()));
		}
		// 文件编号
		if (obj[7] != null) {
			jecnTreeBean.setNumberId(obj[7].toString());
		} else {
			jecnTreeBean.setNumberId("");
		}
		// 是否发布
		if ("0".equals(obj[8].toString())) {
			jecnTreeBean.setPub(false);
		} else {
			jecnTreeBean.setPub(true);
		}
		// 关联Id
		if (obj[9] != null) {
			jecnTreeBean.setRelationId(Long.valueOf(obj[9].toString()));
		}
		return jecnTreeBean;
	}

	/**
	 * 文件对象转换成JecnTreeBean
	 * 
	 * @param obj
	 * @return
	 */
	private JecnTreeBean getJecnTreeBeanByObjectNoDel(Object[] obj, List<Object[]> listTask) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null
				|| obj[8] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		if ("0".equals(obj[3].toString())) {
			// 文件目录
			jecnTreeBean.setTreeNodeType(TreeNodeType.fileDir);
		} else {
			// 角色节点
			jecnTreeBean.setTreeNodeType(TreeNodeType.file);
		}
		// 文件节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 文件节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 文件节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 文件节点父类名称
		jecnTreeBean.setPname("");
		if (Integer.parseInt(obj[4].toString()) > 0) {
			// 文件节点是否有子节点
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}
		// 文件的创建时间
		if (obj[5] != null) {
			jecnTreeBean.setCreateTime((Date) obj[5]);
		}
		if (obj[6] != null) {
			// 保存数据的方式
			jecnTreeBean.setSaveType(Integer.valueOf(obj[6].toString()));
		}
		// 文件编号
		if (obj[7] != null) {
			jecnTreeBean.setNumberId(obj[7].toString());
		} else {
			jecnTreeBean.setNumberId("");
		}
		// 是否发布
		if ("0".equals(obj[8].toString())) {
			jecnTreeBean.setPub(false);
		} else {
			jecnTreeBean.setPub(true);
		}
		if (obj.length > 11 && obj[11] != null) {
			jecnTreeBean.setUpdate(Integer.valueOf(obj[11].toString()) == 1 ? true : false);
		}
		// 关联Id
		if (obj[9] != null) {
			jecnTreeBean.setRelationId(Long.valueOf(obj[9].toString()));
		}
		if (obj.length > 12 && obj[12] != null) {
			jecnTreeBean.setNodeType(Integer.valueOf(obj[12].toString()));
		}
		JecnCommon.setTaskState(jecnTreeBean.getId(), listTask, jecnTreeBean);
		return jecnTreeBean;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：文件子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjectsNoDel(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = this.getJecnTreeBeanByObjectNoDel(obj);
			if (jecnTreeBean != null)
				listResult.add(jecnTreeBean);

		}
		return listResult;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：文件子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjectsNoDel(List<Object[]> list, List<Object[]> listTask) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = this.getJecnTreeBeanByObjectNoDel(obj, listTask);
			if (jecnTreeBean != null)
				listResult.add(jecnTreeBean);

		}
		return listResult;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getChildFiles(Long pId, Long projectId) throws Exception {
		try {
			// 文件夹没有是否公开的功能，所以利用isPub属性存储Hide的值。用于文件管理中的文件夹的显示与隐藏。
			String convertHide = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				convertHide = "convert(varchar(2),t.hide) ";
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				convertHide = "to_char(t.hide) ";
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				convertHide = "convert(t.hide,char(2)) ";
			}
			String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
					+ "case when jft.file_id is null then 0 else 1 end as count,"
					+ "t.CREATE_TIME,t.savetype,t.doc_id"
					+ ", CASE WHEN t.is_dir=0 THEN "
					+ convertHide
					+ " WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,"
					+ "case when t.FLOW_ID is null then t.RULE_ID ELSE t.FLOW_ID END RELATED_ID"
					+ ",t.sort_id ,case when t.pub_time <> t.update_time then   1 else 0 end as isUpdate,t.node_type "
					+ " from jecn_file_t t"
					+ " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id"
					+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0"
					+ " where t.per_file_id=? and t.project_id=? and t.del_state = 0 order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql, pId, projectId);
			sql = "select t.file_id, JECN_TASK.STATE, JECN_TASK.APPROVE_TYPE,JECN_TASK.EDIT" + "  from jecn_file_t t"
					+ " INNER JOIN JECN_TASK_BEAN_NEW JECN_TASK" + "    ON JECN_TASK.R_ID = T.file_id"
					+ "   AND JECN_TASK.IS_LOCK = 1" + "   AND JECN_TASK.STATE <> 5" + "   AND JECN_TASK.TASK_TYPE = 1"
					+ " where t.per_file_id = ?" + "   and t.project_id = ?" + "   and t.del_state = 0";
			List<Object[]> listTask = fileDao.listNativeSql(sql, pId, projectId);
			return findByListObjectsNoDel(list, listTask);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception {
		List<Object[]> list = fileDao.getRoleAuthChildFiles(pId, projectId, peopleId);
		return findByListObjectsNoDel(list);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getChildFileDirs(Long pId, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
					+ "case when jft.file_id is null then 0 else 1 end as count,"
					+ "t.CREATE_TIME,t.savetype,t.doc_id"
					+ ", CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.flow_id,t.sort_id"
					+ " from jecn_file_t t"
					+ " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id"
					+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0"
					+ " where t.per_file_id=? and t.project_id=? and t.del_state = 0 and t.is_dir=0 order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql, pId, projectId);
			return findByListObjectsNoDel(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getAllFiles(Long projectId) throws Exception {
		try {
			String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
					+ "case when jft.file_id is null then 0 else 1 end as count," + "t.CREATE_TIME,t.savetype,t.doc_id"
					+ ", CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.flow_id,t.sort_id"
					+ " from jecn_file_t t" + " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id "
					+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0"
					+ " where t.project_id=? and t.del_state = 0 order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql, projectId);
			return findByListObjectsNoDel(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getAllFileDirs(Long projectId) throws Exception {
		try {
			String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
					+ "case when jft.file_id is null then 0 else 1 end as count,"
					+ "t.CREATE_TIME,t.savetype,t.doc_id"
					+ ", CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.flow_id,t.sort_id"
					+ " from jecn_file_t t "
					+ " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id "
					+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0"
					+ " where t.is_dir=0 and t.del_state = 0 and t.project_id=? order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql, projectId);
			return findByListObjectsNoDel(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnFileBeanT> getFilesByName(String name, Long projectId) throws Exception {
		try {
			// String hql =
			// "from JecnFileBeanT where isDir=1 and projectId=? and fileName
			// like ? order by perFileId,sortId,fileID";
			// return fileDao.listHql(hql, 0, 20, projectId, "%" + name + "%");
			String sql = "select jf.FILE_ID," + " jf.FILE_NAME," + " jf.FILE_PATH," + " jf.PEOPLE_ID,"
					+ " jf.PER_FILE_ID," + " jf.CREATE_TIME," + " jf.UPDATE_TIME," + " jf.IS_DIR," + " jf.ORG_ID,"
					+ " jf.DOC_ID," + " jf.IS_PUBLIC," + " jf.FLOW_ID," + " jf.SORT_ID," + " jf.SAVETYPE,"
					+ " jf.UPDATE_PERSON_ID," + " jf.PROJECT_ID," + " jf.VERSION_ID," + " jf.HISTORY_ID,"
					+ " jfo.org_name, " + " jf.hide," + " jf.del_state" + " from jecn_file_t jf"
					+ " left join jecn_flow_org jfo on jfo.org_id = jf.org_id and jfo.del_state=0"
					+ " where jf.is_dir=1 and jf.project_id=? and jf.file_name like ? and jf.del_state = 0"
					+ " order by jf.per_file_id,jf.sort_id,jf.file_id";
			List<Object[]> listFileBeanT = fileDao.listNativeSql(sql, 0, 20, projectId, "%" + name + "%");
			return findByListFileBeanTObjects(listFileBeanT);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 按权限搜索文件
	 * 
	 * @param name
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnFileBeanT> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		List<Object[]> listFileBeanT = fileDao.searchRoleAuthByName(name, projectId, peopleId);
		return findByListFileBeanTObjects(listFileBeanT);
	}

	private List<JecnFileBeanT> findByListFileBeanTObjects(List<Object[]> listObj) {
		List<JecnFileBeanT> listFileBeanT = new ArrayList<JecnFileBeanT>();
		for (Object[] obj : listObj) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[7] == null || obj[15] == null) {
				continue;
			}
			JecnFileBeanT fileBeanT = new JecnFileBeanT();
			// 文件ID
			if (obj[0] != null && !"".equals(obj[0])) {
				fileBeanT.setFileID(Long.valueOf(obj[0].toString()));
			}
			// 文件名称
			if (obj[1] != null && !"".equals(obj[1])) {
				fileBeanT.setFileName(obj[1].toString());
			}
			// 创建人
			if (obj[3] != null && !"".equals(obj[3])) {
				fileBeanT.setPeopleID(Long.valueOf(obj[3].toString()));
			}
			// 创建时间
			if (obj[5] != null && !"".equals(obj[5])) {
				fileBeanT.setCreateTime((Date) obj[5]);
			}
			// 更新人
			if (obj[14] != null && !"".equals(obj[14])) {
				fileBeanT.setUpdatePersonId(Long.valueOf(obj[14].toString()));
			}
			// 更新时间
			if (obj[6] != null && !"".equals(obj[6])) {
				fileBeanT.setUpdateTime((Date) obj[6]);
			}
			// 文件流

			// 父ID
			if (obj[4] != null && !"".equals(obj[4])) {
				fileBeanT.setPerFileId(Long.valueOf(obj[4].toString()));
			}
			// 0是目录,1是文件
			if (obj[7] != null && !"".equals(obj[7])) {
				fileBeanT.setIsDir(Integer.parseInt(obj[7].toString()));
			}
			// 责任部门
			if (obj[8] != null && !"".equals(obj[8])) {
				fileBeanT.setOrgId(Long.valueOf(obj[8].toString()));
			}
			// 责任部门名称（不存在数据库中）
			if (obj[18] != null && !"".equals(obj[18])) {
				fileBeanT.setOrgName(obj[18].toString());
			}
			// 文件编号
			if (obj[9] != null && !"".equals(obj[9])) {
				fileBeanT.setDocId(obj[9].toString());
			}
			// 0是公开,1是秘密
			if (obj[10] != null && !"".equals(obj[10])) {
				fileBeanT.setIsPublic(Long.valueOf(obj[10].toString()));
			}
			// 所属流程Id
			if (obj[11] != null && !"".equals(obj[11])) {
				fileBeanT.setFlowId(Long.valueOf(obj[11].toString()));
			}
			// 所属流程名称
			fileBeanT.setFlowName(null);
			// 排序编号
			if (obj[12] != null && !"".equals(obj[12])) {
				fileBeanT.setSortId(Integer.parseInt(obj[12].toString()));
			}
			// 保存方式，0本地保存，1数据库大字段保存
			if (obj[13] != null && !"".equals(obj[13])) {
				fileBeanT.setSaveType(Integer.parseInt(obj[13].toString()));
			}
			// 项目ID
			if (obj[15] != null && !"".equals(obj[15])) {
				fileBeanT.setProjectId(Long.valueOf(obj[15].toString()));
			}
			// 当前对应版本号
			if (obj[16] != null && !"".equals(obj[16])) {
				fileBeanT.setVersionId(Long.valueOf(obj[16].toString()));
			}
			// 文控ID
			if (obj[17] != null && !"".equals(obj[17])) {
				fileBeanT.setHistoryId(Long.valueOf(obj[17].toString()));
			}
			// 隐藏：0：隐藏，1：显示
			if (obj[19] != null && !"".equals(obj[19])) {
				fileBeanT.setHide(Integer.parseInt(obj[19].toString()));
			}
			// 删除：0：没有删除，1：删除
			if (obj[20] != null && !"".equals(obj[20])) {
				fileBeanT.setDelState(Integer.parseInt(obj[20].toString()));
			}
			listFileBeanT.add(fileBeanT);
		}

		return listFileBeanT;
	}

	@Override
	public void updateSortFiles(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception {
		try {
			String hql = "from JecnFileBeanT where perFileId=? ";
			List<JecnFileBeanT> listJecnFileBean = fileDao.listHql(hql, pId);
			List<JecnFileBeanT> listUpdate = new ArrayList<JecnFileBeanT>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (JecnFileBeanT jecnFileBeanT : listJecnFileBean) {
					if (jecnTreeDragBean.getId().equals(jecnFileBeanT.getFileID())) {
						if (jecnFileBeanT.getSortId() == 0
								|| (jecnTreeDragBean.getSortId() != jecnFileBeanT.getSortId())) {
							jecnFileBeanT.setSortId(jecnTreeDragBean.getSortId());
							listUpdate.add(jecnFileBeanT);
							break;
						}
					}
				}
			}
			for (JecnFileBeanT jecnFileBeanT : listUpdate) {
				fileDao.update(jecnFileBeanT);
				// 更新文件表
				hql = "update JecnFileBean set sortId=? where fileID=?";
				fileDao.execteHql(hql, jecnFileBeanT.getSortId(), jecnFileBeanT.getFileID());
				JecnUtil.saveJecnJournal(jecnFileBeanT.getFileID(), jecnFileBeanT.getFileName(), 2, 5, updatePersonId,
						fileDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程、流程架构可维护支撑文件
	 * 
	 * @param pId
	 * @param relatedMap
	 * @param updatePersonId
	 * @throws Exception
	 */
	@Override
	public void updateSortFiles(Long pId, Map<Long, Integer> relatedMap, Long updatePersonId) throws Exception {
		List<JecnTreeDragBean> fileDrags = getRelatedFileDragBeans(pId, relatedMap);
		updateSortFiles(fileDrags, pId, updatePersonId);
	}

	/**
	 * 
	 * @param pId
	 * @param relatedMap
	 *            key:流程ID，value：流程对的sortId
	 * @return
	 */
	public List<JecnTreeDragBean> getRelatedFileDragBeans(Long pId, Map<Long, Integer> relatedMap) {
		List<JecnTreeDragBean> list = new ArrayList<JecnTreeDragBean>();
		JecnTreeDragBean dragBean = null;

		List<JecnFileBeanT> listFileDir = getDirectory(pId);
		Map<Long, Long> theFlowFileMap = new HashMap<Long, Long>();
		for (JecnFileBeanT jecnFileBeanT : listFileDir) {
			if (jecnFileBeanT.getFlowId() == null) {
				continue;
			}
			theFlowFileMap.put(jecnFileBeanT.getFlowId(), jecnFileBeanT.getFileID());
		}

		int maxSortId = 0;
		for (Entry<Long, Integer> entry : relatedMap.entrySet()) {
			dragBean = new JecnTreeDragBean();
			Long fileId = theFlowFileMap.get(entry.getKey());
			if (fileId == null) {
				continue;
			}
			dragBean.setId(fileId);
			dragBean.setSortId(entry.getValue());
			dragBean.setTreeNodeType(TreeNodeType.fileDir);
			if (dragBean.getSortId() > maxSortId) {
				maxSortId = dragBean.getSortId();
			}
			list.add(dragBean);
		}

		// 获取目录下文件
		List<JecnFileBeanT> files = this.getDirectoryFile(pId);

		for (JecnFileBeanT jecnFileBeanT : files) {// 重新排序目录下文件
			maxSortId++;
			dragBean = new JecnTreeDragBean();
			dragBean.setId(jecnFileBeanT.getFileID());
			dragBean.setSortId(maxSortId);
			dragBean.setTreeNodeType(TreeNodeType.file);
			list.add(dragBean);
		}
		return list;
	}

	@Override
	public void moveFiles(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		try {
			String t_path = "";
			int level = -1;
			int isHide = 1;
			if (pId.intValue() != 0) {
				JecnFileBeanT jecnFileBeanT = this.get(pId);
				t_path = jecnFileBeanT.gettPath();
				level = jecnFileBeanT.gettLevel();
				isHide = jecnFileBeanT.getHide();
			}
			JecnCommon.updateNodesChildTreeBeans(listIds, pId, "", new Date(), updatePersonId, t_path, level,
					fileContentDao, moveNodeType, isHide);
			// 添加日志
			for (Long id : listIds) {
				JecnUtil.saveJecnJournal(id, null, 2, 4, updatePersonId, fileDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public List<JecnFileContent> getJecnFileContents(Long id) throws Exception {
		// 版本信息
		String hql = "select id,fileId,updateTime,updatePeopleId,fileName from JecnFileContent where fileId=? ORDER BY updateTime DESC";
		List<Object[]> list = fileDao.listHql(hql, id);
		List<JecnFileContent> listJecnFileContent = new ArrayList<JecnFileContent>();
		JecnFileBeanT jecnFileBeanT = this.get(id);
		JecnFileBean jecnFileBean = this.findJecnFileBeanById(id);

		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[4] == null) {
				continue;
			}
			Long versionId = Long.valueOf(obj[0].toString());
			if (versionId.equals(jecnFileBeanT.getVersionId())) {
				continue;
			}
			if (jecnFileBean != null && versionId.equals(jecnFileBean.getVersionId())) {
				continue;
			}
			JecnFileContent jecnFileContent = new JecnFileContent();
			jecnFileContent.setId(versionId);
			jecnFileContent.setFileId((Long) obj[1]);
			if (obj[2] != null) {
				jecnFileContent.setUpdateTime((Date) obj[2]);
			}
			if (obj[3] != null)
				jecnFileContent.setUpdatePeopleId((Long) obj[3]);
			jecnFileContent.setFileName(obj[4].toString());
			listJecnFileContent.add(jecnFileContent);
		}
		return listJecnFileContent;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public FileInfoBean getFileBean(Long id, boolean isPub) throws Exception {
		String pub = isPub ? "" : "_T";
		try {
			JecnFileBeanT jecnFileBeanT = null;
			if (isPub) {
				JecnFileBean jecnFileBean = fileDao.findJecnFileBeanById(id);
				if (jecnFileBean != null) {
					jecnFileBeanT = new JecnFileBeanT();
					PropertyUtils.copyProperties(jecnFileBeanT, jecnFileBean);
				}
			} else {
				jecnFileBeanT = fileDao.get(id);
			}
			if (jecnFileBeanT == null) {
				return null;
			}
			// 责任部门
			Long orgId = jecnFileBeanT.getOrgId();
			if (orgId != null) {
				// 部门对象
				JecnFlowOrg jecnFlowOrg = this.organizationDao.get(orgId);
				if (jecnFlowOrg != null && jecnFlowOrg.getOrgName() != null) {
					jecnFileBeanT.setOrgName(jecnFlowOrg.getOrgName());
				} else {
					jecnFileBeanT.setOrgName("");
				}
			} else {
				jecnFileBeanT.setOrgName("");
			}
			// 所属流程
			Long flowId = jecnFileBeanT.getFlowId();
			if (flowId != null) {
				if (isPub) {
					JecnFlowStructure jecnFlowStructure = this.flowStructureDao.findJecnFlowStructureById(flowId);
					if (jecnFlowStructure != null && jecnFlowStructure.getFlowName() != null) {
						jecnFileBeanT.setFlowName(jecnFlowStructure.getFlowName());
					} else {
						jecnFileBeanT.setFlowName("");
					}
				} else {
					JecnFlowStructureT jecnFlowStructureT = this.flowStructureDao.get(flowId);
					if (jecnFlowStructureT != null && jecnFlowStructureT.getFlowName() != null) {
						jecnFileBeanT.setFlowName(jecnFlowStructureT.getFlowName());
					} else {
						jecnFileBeanT.setFlowName("");
					}
				}
			} else {
				jecnFileBeanT.setFlowName("");
			}
			// 创建人
			Long peopleId = jecnFileBeanT.getPeopleID();
			if (peopleId != null) {
				JecnUser jecnUser = this.personDao.get(peopleId);
				if (jecnUser != null) {
					jecnFileBeanT.setPeopleName(jecnUser.getTrueName());
				}
			}

			// 专员
			if (jecnFileBeanT.getCommissionerId() != null) {
				JecnUser jecnUser = this.personDao.get(jecnFileBeanT.getCommissionerId());
				if (jecnUser != null)
					jecnFileBeanT.setCommissionerName(jecnUser.getTrueName());
			}
			// 责任人
			if (jecnFileBeanT.getResPeopleId() != null) {
				JecnUser jecnUser = this.personDao.get(jecnFileBeanT.getResPeopleId());
				if (jecnUser != null)
					jecnFileBeanT.setResPeopleName(jecnUser.getTrueName());
			}
			FileInfoBean fileInfoBean = new FileInfoBean();
			fileInfoBean.setJecnFileBeanT(jecnFileBeanT);
			// 文件岗位查阅权限
			fileInfoBean.setListPos(this.positionAccessPermissionsDao.getPositionAccessPermissions(id, 1, isPub));

			// 部门查阅权限
			fileInfoBean.setLisOrg(this.orgAccessPermissionsDao.getOrgAccessPermissions(id, 1, isPub));
			// 岗位组查阅权限
			fileInfoBean.setListPosGroup(this.posGroupAccessPermissionsDao.getPosGroupAccessPermissions(id, 1, isPub));
			// 文件相关标准
			String sql = "SELECT B.CRITERION_CLASS_ID,B.CRITERION_CLASS_NAME,B.PRE_CRITERION_CLASS_ID,B.STAN_TYPE,B.RELATED_ID,"
					+ "             0 AS HASCHILD"
					+ "           FROM FILE_RELATED_STANDARD"
					+ pub
					+ "  A, JECN_CRITERION_CLASSES B,JECN_FILE"
					+ pub
					+ " JF"
					+ "           WHERE A.STANDARD_ID = B.CRITERION_CLASS_ID   AND A.FILE_ID = JF.FILE_ID AND JF.DEL_STATE=0 AND JF.FILE_ID=?";
			List<Object[]> listStandards = fileDao.listNativeSql(sql, id);
			List<JecnTreeBean> standardsRelateds = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listStandards) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectStandard(obj);
				if (jecnTreeBean == null) {
					continue;
				}
				standardsRelateds.add(jecnTreeBean);
			}
			fileInfoBean.setStandardsRelateds(standardsRelateds);
			// 文件相关制度
			sql = "SELECT RRT.RISK_ID, JR.NAME" + "  FROM FILE_RELATED_RISK" + pub + " RRT" + " INNER JOIN JECN_FILE"
					+ pub + " JF" + "    ON JF.FILE_ID = RRT.FILE_ID AND JF.DEL_STATE=0" + " INNER JOIN JECN_RISK JR"
					+ "    ON JR.ID = RRT.RISK_ID" + " WHERE JF.FILE_ID = ?";
			List<Object[]> listRisks = fileDao.listNativeSql(sql, id);
			fileInfoBean.setRiskRelateds(JecnUtil.getJecnTreeBeanByObjectRisk(listRisks));

			return fileInfoBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public List<FileUseInfoBean> getFileUsages(Long id) throws Exception {
		// 文件被调用的情况
		// 流程地图的附件
		List<Object[]> list = null;
		String sql = "select b.flow_id, b.flow_name" + "  from JECN_MAIN_FLOW_T a, JECN_FLOW_STRUCTURE_T b"
				+ " where b.flow_id = a.flow_id" + "   and b.del_state = 0" + "   and a.file_id = ?" + " union "
				+ "select e.flow_id, e.flow_name" + "  from JECN_FLOW_STRUCTURE_T       e,"
				+ "       JECN_FIGURE_FILE_T          c," + "       JECN_FLOW_STRUCTURE_IMAGE_T d"
				+ " where e.flow_id = d.flow_id" + "   and e.del_state = 0" + "   and c.figure_id = d.FIGURE_ID"
				+ "   and c.file_id = ?";
		list = fileDao.listNativeSql(sql, id, id);
		List<FileUseInfoBean> listFileUserInfoBean = new ArrayList<FileUseInfoBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.processMap);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 流程的附件
		sql = "select b.flowId,b.flowName " + "from JecnFlowBasicInfoT as a,JecnFlowStructureT as b "
				+ "where b.flowId = a.flowId and b.delState=0 and a.fileId=?";
		list = fileDao.listHql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.process);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}

		// 岗位的附件
		sql = "SELECT JFOI.FIGURE_ID,JFOI.FIGURE_TEXT FROM JECN_POSITION_FIG_INFO JPFI"
				+ " INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + " ON JFOI.FIGURE_ID=JPFI.FIGURE_ID"
				+ " INNER JOIN JECN_FLOW_ORG JFO" + " ON JFO.ORG_ID=JFOI.ORG_ID" + " WHERE JPFI.FILE_ID=?"
				+ " and JFO.DEL_STATE=0";
		list = fileDao.listNativeSql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.position);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}

		// 流程的活动操作规范与活动输入
		if (!JecnConfigTool.useNewInout()) {
			sql = "select distinct b.flowId,b.flowName from JecnFlowStructureImageT as a,"
					+ "JecnFlowStructureT as b,JecnActivityFileT as c where b.flowId = a.flowId and "
					+ "b.delState=0 and a.figureId = c.figureId and c.fileSId=?";
			list = fileDao.listHql(sql, id);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
				fileUserInfoBean.setType(TreeNodeType.process);
				fileUserInfoBean.setId(obj[0].toString());
				if (obj[1] != null) {
					fileUserInfoBean.setName(obj[1].toString());
				} else {
					fileUserInfoBean.setName("");
				}
				listFileUserInfoBean.add(fileUserInfoBean);
			}
			// 流程的输出
			sql = "select distinct b.flowId,b.flowName "
					+ "from JecnFlowStructureImageT as a,JecnFlowStructureT as b,JecnModeFileT as c "
					+ "where b.flowId = a.flowId and b.delState=0 and a.figureId = c.figureId and c.fileMId=?";
			list = fileDao.listHql(sql, id);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
				fileUserInfoBean.setType(TreeNodeType.process);
				fileUserInfoBean.setId(obj[0].toString());
				if (obj[1] != null) {
					fileUserInfoBean.setName(obj[1].toString());
				} else {
					fileUserInfoBean.setName("");
				}
				listFileUserInfoBean.add(fileUserInfoBean);
			}
			// 流程的样例
			sql = "select distinct b.flowId,b.flowName from " + "JecnFlowStructureImageT as a,JecnFlowStructureT as b,"
					+ "JecnModeFileT as c,JecnTempletT as d" + " where b.flowId = a.flowId and b.delState=0 "
					+ " and a.figureId = c.figureId and " + " c.modeFileId = d.modeFileId and d.fileId=?";
			list = fileDao.listHql(sql, id);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
				fileUserInfoBean.setType(TreeNodeType.process);
				fileUserInfoBean.setId(obj[0].toString());
				if (obj[1] != null) {
					fileUserInfoBean.setName(obj[1].toString());
				} else {
					fileUserInfoBean.setName("");
				}
				listFileUserInfoBean.add(fileUserInfoBean);
			}
		} else {
			// 流程的輸入输出 新版
			sql = "select distinct b.flow_id,b.flow_name" + "  from jecn_flow_structure_image_t  a,"
					+ "       jecn_flow_structure_t        b," + "       JECN_ACTIVITY_FILE_T         c"
					+ " where b.flow_id = a.flow_id" + "   and b.del_state = 0" + "   and a.figure_id = c.figure_id"
					+ "   and c.FILE_S_ID = ?" + "  and c.FILE_TYPE = 1"
					+ " union select distinct jf.flow_id, jf.flow_name" + "  from JECN_FIGURE_IN_OUT_T inout"
					+ " inner join jecn_flow_structure_image_t imf" + "    on inout.figure_id = imf.figure_id"
					+ " inner join jecn_flow_structure_t jf" + "    on jf.flow_id = imf.flow_id"
					+ "    and jf.del_state = 0" + "    and inout.file_id = ?";
			// 流程的样例
			sql += " union select distinct jf.flow_id, jf.flow_name" + "  from JECN_FIGURE_IN_OUT_T inout"
					+ " inner join JECN_FIGURE_IN_OUT_SAMPLE_T sam" + "    on inout.id = sam.in_out_id"
					+ " inner join jecn_flow_structure_image_t imf" + "    on inout.figure_id = imf.figure_id"
					+ " inner join jecn_flow_structure_t jf" + "    on jf.flow_id = imf.flow_id"
					+ "   and jf.del_state = 0 where sam.file_id = ?";
			list = fileDao.listNativeSql(sql, id, id, id);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
				fileUserInfoBean.setType(TreeNodeType.process);
				fileUserInfoBean.setId(obj[0].toString());
				if (obj[1] != null) {
					fileUserInfoBean.setName(obj[1].toString());
				} else {
					fileUserInfoBean.setName("");
				}
				listFileUserInfoBean.add(fileUserInfoBean);
			}

		}
		// 标准的附件
		sql = "select criterionClassId,criterionClassName,stanType from StandardBean "
				+ "where stanType=1 and relationId=?";
		list = fileDao.listHql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[2] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			if ("1".equals(obj[2].toString())) {
				fileUserInfoBean.setType(TreeNodeType.standard);
			} else if ("2".equals(obj[2].toString())) {
				fileUserInfoBean.setType(TreeNodeType.standardProcess);
			} else if ("3".equals(obj[2].toString())) {
				fileUserInfoBean.setType(TreeNodeType.standardProcessMap);
			}
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 制度的附件
		sql = "select a.id,b.fileName from RuleT as a,JecnFileBeanT as b  where b.delState=0 and a.isDir=2 and a.fileId=b.fileID and b.fileID =?";
		list = fileDao.listHql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.ruleFile);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 制度的表单附件
		sql = "select distinct a.id,a.ruleName " + "from RuleT as a,RuleFileT as b,RuleTitleT as c "
				+ "where a.id=c.ruleId and c.id=b.ruleTitleId and b.ruleFileId=?";
		list = fileDao.listHql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.ruleModeFile);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 流程的图标
		sql = "select b.flowId,b.flowName,b.isFlow from JecnFlowStructureImageT as a,JecnFlowStructureT as b where b.flowId = a.flowId and b.delState=0  and a.figureType="
				+ JecnCommonSql.getIconString() + " and a.linkFlowId=?";
		list = fileDao.listHql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[2] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			if ("0".equals(obj[2].toString())) {
				fileUserInfoBean.setType(TreeNodeType.processMap);
			} else {
				fileUserInfoBean.setType(TreeNodeType.process);
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 组织的图标
		sql = "select b.orgId,b.orgName from JecnFlowOrgImage as a,JecnFlowOrg as b where b.orgId = a.orgId and b.delState=0 and a.figureType="
				+ JecnCommonSql.getIconString() + " and a.linkOrgId=?";
		list = fileDao.listHql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.organization);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 相关流程或流程架构
		sql = "select t.flow_id,t.flow_name,t.isflow from" + " FLOW_STANDARDIZED_FILE_T fsf,Jecn_Flow_Structure_t t"
				+ " where t.flow_id = fsf.flow_id and t.del_state=0 and fsf.file_id=?";
		list = fileDao.listNativeSql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[2] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			if ("0".equals(obj[2].toString())) {
				fileUserInfoBean.setType(TreeNodeType.processMap);
			} else {
				fileUserInfoBean.setType(TreeNodeType.process);
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 相关风险
		sql = "select jr.id,jr.name from FILE_RELATED_RISK_T frr" + "	inner join jecn_risk jr on frr.risk_id=jr.id"
				+ "   where frr.file_id=?";
		list = fileDao.listNativeSql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.riskPoint);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}
		// 相关标准
		sql = " select jr.criterion_class_id,jr.criterion_class_name from FILE_RELATED_STANDARD_T frs"
				+ "	inner join JECN_CRITERION_CLASSES jr on frs.standard_id=jr.criterion_class_id"
				+ "  where frs.file_id=?";
		list = fileDao.listNativeSql(sql, id);
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			FileUseInfoBean fileUserInfoBean = new FileUseInfoBean();
			fileUserInfoBean.setType(TreeNodeType.standard);
			fileUserInfoBean.setId(obj[0].toString());
			if (obj[1] != null) {
				fileUserInfoBean.setName(obj[1].toString());
			} else {
				fileUserInfoBean.setName("");
			}
			listFileUserInfoBean.add(fileUserInfoBean);
		}

		LinkedHashSet<FileUseInfoBean> set = new LinkedHashSet<FileUseInfoBean>(list.size());
		set.addAll(listFileUserInfoBean);
		listFileUserInfoBean.clear();
		listFileUserInfoBean.addAll(set);

		// 不改动原有代码的情况下 逻辑去重
		List<FileUseInfoBean> listNew = new ArrayList<FileUseInfoBean>();
		for (FileUseInfoBean bean : listFileUserInfoBean) {
			if (listNew != null && !listNew.isEmpty()) {
				boolean isNew = true;
				for (FileUseInfoBean fileUseInfoBean : listNew) {
					if (fileUseInfoBean.getId().equals(bean.getId())) {
						isNew = false;
					}
				}
				if (isNew) {
					listNew.add(bean);
				}
			} else {
				listNew.add(bean);
			}
		}
		return listNew;
	}

	/**
	 * 
	 * @author yxw 2012-5-29
	 * @description:文件删除
	 * @param listIds
	 * @return int 0 删除异常，1删除成功；2：存在节点处于任务中
	 * @throws Exception
	 */
	@Override
	public void deleteFiles(List<Long> listIds, Long projectId, Long updatePersonId) throws Exception {
		try {
			// 待删除的文件
			Set<Long> setFileIds = new HashSet<Long>();
			// 待删除的目录
			Set<Long> setDirIds = new HashSet<Long>();
			String sql = JecnCommonSql.getChildObjectsSqlByTypeDelState(listIds, TreeNodeType.fileDir);
			List<Object[]> listAll = fileDao.listNativeSql(sql);
			for (Object[] obj : listAll) {
				if (obj == null || obj[0] == null || obj[1] == null) {
					continue;
				}
				if ("1".equals(obj[1].toString())) {
					setFileIds.add(Long.valueOf(obj[0].toString()));
				} else {
					setDirIds.add(Long.valueOf(obj[0].toString()));
				}
			}
			// 添加日志
			JecnUtil.saveJecnJournals(setFileIds, 2, 3, updatePersonId, fileDao);
			// 添加日志
			JecnUtil.saveJecnJournals(setDirIds, 2, 3, updatePersonId, fileDao);
			fileDao.deleteFiles(setFileIds, setDirIds);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateFile(JecnFileBeanT fileBean, Long updatePersonId, boolean isPub, boolean isDesign)
			throws Exception {
		try {
			JecnFileContent jecnFileContent = new JecnFileContent();
			jecnFileContent.setFileId(fileBean.getFileID());
			jecnFileContent.setFileName(fileBean.getFileName());
			jecnFileContent.setUpdatePeopleId(updatePersonId);
			jecnFileContent.setUpdateTime(new Date());
			// 发布并且是系统管理员或文件设计管理员
			if (isPub && !isDesign) {
				jecnFileContent.setType(1);
			} else {
				jecnFileContent.setType(0);
			}
			// 保存到本地
			if (fileBean.getSaveType() == 0) {
				jecnFileContent.setIsVersionLocal(0);
				// 保存文件至本地
				saveFileToLocal(fileBean, jecnFileContent);
				fileDao.getSession().save(jecnFileContent);
				fileBean.setVersionId(jecnFileContent.getId());
				fileBean.setUpdateTime(new Date());
				fileBean.setUpdatePersonId(updatePersonId);
				fileDao.update(fileBean);
			} else {
				jecnFileContent.setIsVersionLocal(1);
				// 保存到数据库
				updateFileToDataBase(jecnFileContent, fileBean, updatePersonId);
				// 保存版本文件至本地
				saveVersionFileToLocal(fileBean.getFileID(), jecnFileContent.getId(), updatePersonId);
			}
			if (isPub && !isDesign) {
				String hql = "update JecnFileBeanT set updateTime=?,pubTime=? where fileID=?";
				fileDao.execteHql(hql, fileBean.getPubTime(),fileBean.getPubTime(),fileBean.getFileID());
				
				hql = "update JecnFileBean set fileName=?,updatePersonId=?,versionId=? where fileID=?";
				fileDao.execteHql(hql, fileBean.getFileName(), fileBean.getUpdatePersonId(), 
						fileBean.getVersionId(), fileBean.getFileID());
			}
			ruleDao.reNameRuleByRelationId(fileBean.getFileName(), fileBean.getFileID());
			standardDao.reNameStandardByRelationId(fileBean.getFileName(), 0, fileBean.getFileID());
			// 添加日志
			JecnUtil.saveJecnJournal(fileBean.getFileID(), fileBean.getFileName(), 2, 2, updatePersonId, fileDao, 8);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private void saveFileToLocal(JecnFileBeanT fileBean, JecnFileContent fileContent) throws IOException {
		String fileName;
		byte[] bytes;
		if (fileBean.getSaveType() == 1 && fileContent.getIsVersionLocal() == 0) {// 文件保存方式：数据库，版本文件保存到本地
			fileName = fileContent.getFileName();
			bytes = fileContent.getBytes();
		} else {// 文件保存方式保存到本地，版本文件也保存到本地
			fileName = fileBean.getFileName();
			bytes = fileBean.getFileStream();
		}
		String filePath = JecnFinal.saveFileToLocal(bytes, fileName, JecnFinal.FILE_LIBRARY);
		if (filePath == null) {
			throw new IllegalAnnotationException("JecnFinal.saveFileToLocal获取文件路径非法!");
		}
		fileContent.setFilePath(filePath);
	}

	private void saveVersionFileToLocal(JecnFileBeanT fileBean, Long updatePersonId) throws Exception {
		// 1、获取版本文件
		JecnFileContent fileContent = (JecnFileContent) fileDao.getSession().get(JecnFileContent.class,
				fileBean.getVersionId());
		byte[] bytes = JecnDaoUtil.blobToBytes(fileContent.getFileStream());
		fileContent.setBytes(bytes);
		// 2、更新版本文件标识
		fileContent.setIsVersionLocal(0);
		fileContent.setUpdateTime(new Date());
		fileContent.setUpdatePeopleId(updatePersonId);
		fileContent.setFileStream(null);
		fileDao.getSession().update(fileContent);
		fileDao.getSession().flush();
		// 3、保存版本文件到本地
		saveFileToLocal(fileBean, fileContent);
	}

	private void saveVersionFileToLocal(Long fileId, Long contentId, Long updatePersonId) throws Exception {
		String hql = "from JecnFileContent where fileId = ? and isVersionLocal = 1 and id <> " + contentId;
		List<JecnFileContent> fileContents = fileDao.listHql(hql, fileId);

		if (fileContents.size() == 0) {
			return;
		}
		// 获取 正式表
		JecnFileBean fileBean = fileDao.findJecnFileBeanById(fileId);

		for (JecnFileContent fileContent : fileContents) {
			if (fileBean != null && fileBean.getVersionId() != null
					&& String.valueOf(fileContent.getId()).equals(fileBean.getVersionId().toString())) {
				// 过滤掉发布版本
				continue;
			}
			byte[] bytes = JecnDaoUtil.blobToBytes(fileContent.getFileStream());
			if (bytes == null) {
				continue;
			}
			// 2、更新版本文件标识
			fileContent.setIsVersionLocal(0);
			fileContent.setUpdateTime(new Date());
			fileContent.setUpdatePeopleId(updatePersonId);
			fileContent.setFileStream(null);
			// 3、保存版本文件到本地
			String filePath = JecnFinal.saveFileToLocal(bytes, fileContent.getFileName(), JecnFinal.FILE_LIBRARY);
			if (filePath == null) {
				throw new IllegalAnnotationException("JecnFinal.saveFileToLocal获取文件路径非法!");
			}
			fileContent.setFilePath(filePath);

			fileDao.getSession().update(fileContent);
			fileDao.getSession().flush();
		}
	}

	@SuppressWarnings("deprecation")
	private void updateFileToDataBase(JecnFileContent jecnFileContent, JecnFileBeanT fileBean, Long updatePersonId)
			throws Exception {
		if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
			jecnFileContent.setFileStream(Hibernate.createBlob(fileBean.getFileStream()));
			fileDao.getSession().save(jecnFileContent);
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			jecnFileContent.setFileStream(Hibernate.createBlob(new byte[1]));
			fileDao.getSession().save(jecnFileContent);
			fileDao.getSession().flush();
			fileDao.getSession().refresh(jecnFileContent, LockMode.UPGRADE);
			SerializableBlob sb = (SerializableBlob) jecnFileContent.getFileStream();
			java.sql.Blob wrapBlob = sb.getWrappedBlob();
			BLOB blob = (BLOB) wrapBlob;
			OutputStream out = blob.getBinaryOutputStream();
			out.write(fileBean.getFileStream());
			out.close();
		}
		fileBean.setVersionId(jecnFileContent.getId());
		fileBean.setUpdateTime(new Date());
		fileBean.setUpdatePersonId(updatePersonId);

		fileDao.update(fileBean);
	}

	@Override
	public FileOpenBean openFile(Long id, boolean isPub) throws Exception {
		return SelectDownloadProcess.openFile(id, isPub, fileDao);
	}

	@Override
	public void updateRecovery(Long fileId, Long versionId, Long updatePersonId, boolean isPub, boolean isDesign)
			throws Exception {
		try {
			JecnFileBeanT jecnFileBeanT = fileDao.get(fileId);
			if (jecnFileBeanT == null) {
				return;
			}
			JecnFileContent jecnFileContent = (JecnFileContent) fileDao.getSession().get(JecnFileContent.class,
					versionId);
			if (jecnFileContent == null) {
				return;
			}
			byte[] bytes = null;
			if (jecnFileBeanT.getSaveType() == 0 || jecnFileContent.getIsVersionLocal() == 0) {
				bytes = JecnFinal.getBytesByFilePath(jecnFileContent.getFilePath());
			} else {
				SerializableBlob sb = (SerializableBlob) jecnFileContent.getFileStream();
				Blob wrapBlob = sb.getWrappedBlob();
				bytes = new byte[(int) wrapBlob.length()];

				InputStream in = null;
				try {
					in = wrapBlob.getBinaryStream();
					in.read(bytes);
				} catch (Exception e) {
					log.error("", e);
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (Exception e) {
							log.error("", e);
						}
					}
				}
			}
			if (bytes == null) {
				return;
			}
			jecnFileBeanT.setFileStream(bytes);
			jecnFileBeanT.setFileName(jecnFileContent.getFileName());
			// 调用更新文件的方法
			this.updateFile(jecnFileBeanT, updatePersonId, isPub, isDesign);
			JecnUtil.saveJecnJournal(jecnFileBeanT.getFileID(), jecnFileBeanT.getFileName(), 2, 15, updatePersonId,
					fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public String[] validateAddName(List<String> names, Long pId, int fileType) throws Exception {
		boolean global = JecnConfigTool.fileValidateGlobal();
		try {
			if (global) {
				return getRepeatNames(names, fileType);
			} else {
				return getRepeatNames(names, pId, fileType);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public String[] validateNamefullPath(List<String> names, Long pId, int fileType) throws Exception {
		try {
			String condition = "";
			if (fileType != -1) {
				condition = "and isDir=" + fileType;
			}
			String hql = "select fileName from JecnFileBeanT where perFileId <> ? " + condition;
			List<String> listData = fileDao.listHql(hql, pId);
			List<String> listResult = new ArrayList<String>();
			for (String newName : names) {
				if (listData.contains(newName)) {
					listResult.add(newName);
				}
			}
			if (listResult.size() == 0) {
				return null;
			}
			String[] strArr = new String[listResult.size()];
			for (int i = 0; i < listResult.size(); i++) {
				strArr[i] = listResult.get(i);
			}
			return strArr;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private String[] getRepeatNames(List<String> names, int fileType) {
		String condition = "";
		if (fileType != -1) {
			condition = " and is_dir=" + fileType;
		}
		String nameConditon = JecnCommonSql.getStrs(names);
		String sql = " select file_name from JECN_FILE_T where del_state=0 and file_name in " + nameConditon
				+ condition;
		List<String> repeatNames = fileDao.listObjectNativeSql(sql, "file_name", Hibernate.STRING);
		Set<String> distinctNames = new HashSet<String>();
		for (String r : repeatNames) {
			distinctNames.add(r);
		}
		return distinctNames.toArray(new String[distinctNames.size()]);
	}

	private String[] getRepeatNames(List<String> names, Long pId, int fileType) {
		String condition = "";
		if (fileType != -1) {
			condition = "and isDir=" + fileType;
		}
		String hql = "select fileName from JecnFileBeanT where delState <> 2 and  perFileId=? " + condition;
		List<String> listData = fileDao.listHql(hql, pId);
		List<String> listResult = new ArrayList<String>();
		for (String newName : names) {
			if (listData.contains(newName)) {
				listResult.add(newName);
			}
		}
		if (listResult.size() == 0) {
			return null;
		}
		String[] strArr = new String[listResult.size()];
		for (int i = 0; i < listResult.size(); i++) {
			strArr[i] = listResult.get(i);
		}
		return strArr;
	}

	@Override
	public boolean validateUpdateName(String name, Long id, Long pId) throws Exception {
		try {
			String hql = "select count(*) from JecnFileBeanT where perFileId=? and delState=0  and fileName=? and isDir=1 and fileID<>?";
			int count = fileDao.countAllByParamsHql(hql, pId, name, id);
			if (count > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void deleteVersion(List<Long> ids, Long fileId, Long updatePersonId) throws Exception {
		try {
			JecnFileBeanT jecnFileBeanT = fileDao.get(fileId);
			jecnFileBeanT.setUpdatePersonId(updatePersonId);
			jecnFileBeanT.setUpdateTime(new Date());
			fileDao.update(jecnFileBeanT);
			// if (jecnFileBeanT.getSaveType().intValue() == 0) {
			/** 获得本地文件版本信息 */
			String hql = "select filePath from JecnFileContent where id in" + JecnCommonSql.getIds(ids);
			List<String> pathLists = this.fileDao.listHql(hql);
			for (String str : pathLists) {
				JecnFinal.deleteFile(str);
			}

			hql = "delete from JecnFileContent where id in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, ids);
			for (String str : listStr) {
				fileDao.execteHql(str);
			}
			// 记录日志
			JecnUtil.saveJecnJournal(jecnFileBeanT.getFileID(), jecnFileBeanT.getFileName(), 2, 16, updatePersonId,
					fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public FileOpenBean openVersion(Long fileId, Long versionId) throws Exception {
		try {
			return JecnDaoUtil.getFileOpenBean(fileDao, versionId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateFileProperty(JecnFileBeanT jecnFileBeanT, Set<Long> standardIds, Set<Long> riskIds,
			Long updatePersonId) throws Exception {
		try {
			jecnFileBeanT.setUpdatePersonId(updatePersonId);
			jecnFileBeanT.setUpdateTime(new Date());
			fileDao.update(jecnFileBeanT);
			// 更新相关标准
			String hql = "from FileRelatedStandardT where fileId=?";
			List<FileRelatedStandardT> listOldStandard = fileDao.listHql(hql, jecnFileBeanT.getFileID());
			for (Long standardId : standardIds) {
				boolean isExist = false;
				for (FileRelatedStandardT fileRelatedStandardT : listOldStandard) {
					if (standardId.equals(fileRelatedStandardT.getStandardId())) {
						isExist = true;
						listOldStandard.remove(fileRelatedStandardT);
						break;
					}
				}
				if (!isExist) {
					FileRelatedStandardT fileRelatedStandardT = new FileRelatedStandardT();
					fileRelatedStandardT.setId(JecnCommon.getUUID());
					fileRelatedStandardT.setFileId(jecnFileBeanT.getFileID());
					fileRelatedStandardT.setStandardId(standardId);
					fileDao.getSession().save(fileRelatedStandardT);
				}
			}
			for (FileRelatedStandardT fileRelatedStandardT : listOldStandard) {
				fileDao.getSession().delete(fileRelatedStandardT);
			}
			// 更新相关风险
			hql = "from FileRelatedRiskT where fileId=?";
			List<FileRelatedRiskT> listOldRisk = fileDao.listHql(hql, jecnFileBeanT.getFileID());
			for (Long riskId : riskIds) {
				boolean isExist = false;
				for (FileRelatedRiskT fileRelatedRiskT : listOldRisk) {
					if (riskId.equals(fileRelatedRiskT.getRiskId())) {
						isExist = true;
						listOldRisk.remove(fileRelatedRiskT);
						break;
					}
				}
				if (!isExist) {
					FileRelatedRiskT fileRelatedRiskT = new FileRelatedRiskT();
					fileRelatedRiskT.setId(JecnCommon.getUUID());
					fileRelatedRiskT.setFileId(jecnFileBeanT.getFileID());
					fileRelatedRiskT.setRiskId(riskId);
					fileDao.getSession().save(fileRelatedRiskT);
				}
			}
			for (FileRelatedRiskT fileRelatedRiskT : listOldRisk) {
				fileDao.getSession().delete(fileRelatedRiskT);
			}
			// 保存岗位查阅权限
			positionAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1, jecnFileBeanT.getAccessId()
					.getPosAccessId(), false);
			// 保存部门查阅权限
			orgAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1, jecnFileBeanT.getAccessId()
					.getOrgAccessId(), false);
			// 保存岗位组查阅权限
			posGroupAccessPermissionsDao.savaOrUpdate(jecnFileBeanT.getFileID(), 1, jecnFileBeanT.getAccessId()
					.getPosGroupAccessId(), false);
			// 添加日志
			JecnUtil.saveJecnJournal(jecnFileBeanT.getFileID(), jecnFileBeanT.getFileName(), 2, 2, updatePersonId,
					fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getJecnTreeBeanByName(String name, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
					+ "case when jft.file_id is null then 0 else 1 end as count,"
					+ "t.CREATE_TIME,t.savetype,t.doc_id"
					+ ", CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.flow_id,t.sort_id"
					+ " from jecn_file_t t "
					+ " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id "
					+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0"
					+ " where t.is_dir=1 and t.del_state=0 and t.file_name like ? and t.project_id=? order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql, 0, 20, "%" + name + "%", projectId);
			return findByListObjectsNoDel(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public JecnTreeBean getJecnTreeBeanByIds(Long listId, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
					+ "case when jft.file_id is null then 0 else 1 end as count," + "t.CREATE_TIME,t.savetype,t.doc_id"
					+ ", CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.flow_id,t.sort_id"
					+ " from jecn_file_t t " + " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id "
					+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0"
					+ " where t.is_dir=1 and t.del_state=0 and t.file_id = " + listId
					+ " and t.project_id=? order by t.per_file_id,t.sort_id,t.file_id";
			Object[] obj = fileDao.getObjectNativeSql(sql, projectId);
			return this.getJecnTreeBeanByObjectNoDel(obj);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getPnodes(Long fileId, Long projectId) throws Exception {
		try {
			JecnFileBeanT jecnFileBeanT = fileDao.get(fileId);
			if (jecnFileBeanT == null) {
				throw new IllegalArgumentException("文件不存在文件ID：" + fileId);
			}
			Set<Long> set = JecnCommon.getPositionTreeIds(jecnFileBeanT.gettPath(), fileId);
			String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.Is_Dir"
					+ "  ,case when jft.file_id is null then 0 else 1 end as count,t.CREATE_TIME,t.savetype,t.doc_id"
					+ ", CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.flow_id,T.SORT_ID"
					+ " from jecn_file_t t LEFT JOIN jecn_file jf ON t.file_id = jf.file_id  "
					+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0"
					+ " where t.per_file_id in " + JecnCommonSql.getIdsSet(set)
					+ " and t.project_id=? and t.del_state = 0 " + " ORDER BY T.PER_FILE_ID,T.SORT_ID,T.FILE_ID";
			List<Object[]> list = fileDao.listNativeSql(sql, projectId);
			sql = "select t.file_id, JECN_TASK.STATE, JECN_TASK.APPROVE_TYPE,JECN_TASK.EDIT" + "  from jecn_file_t t"
					+ " INNER JOIN JECN_TASK_BEAN_NEW JECN_TASK" + "    ON JECN_TASK.R_ID = T.file_id"
					+ "   AND JECN_TASK.IS_LOCK = 1" + "   AND JECN_TASK.STATE <> 5" + "   AND JECN_TASK.TASK_TYPE = 1"
					+ " where t.per_file_id in" + JecnCommonSql.getIdsSet(set) + "   and t.project_id = ?"
					+ "   and t.del_state = 0";
			List<Object[]> listTask = fileDao.listNativeSql(sql, projectId);
			return findByListObjectsNoDel(list, listTask);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public boolean isFilesAuth(List<Long> fileIds, Long projectId, Set<Long> setIds) throws Exception {
		for (Long fileId : fileIds) {
			if (setIds.contains(fileId)) {// 管理员分配节点不允许删除
				return false;
			}
			if (!isFileAuth(fileId, projectId, setIds, TreeNodeType.file)) {
				return false;
			}
		}
		return true;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public boolean isFileAuth(Long fileId, Long projectId, Set<Long> setIds, TreeNodeType treeNodeType)
			throws Exception {
		try {
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				switch (treeNodeType) {
				case file:
					sql = "WITH MY_FILE AS(SELECT * FROM JECN_FILE_T JF WHERE JF.File_Id in"
							+ JecnCommonSql.getIdsSet(setIds) + " UNION ALL"
							+ " SELECT JFF.* FROM MY_FILE INNER JOIN JECN_FILE_T JFF"
							+ " ON MY_FILE.FILE_ID=JFF.Per_File_Id)"
							+ " SELECT  count(*) FROM MY_FILE where project_id=? and FILE_ID =?";
					break;
				case processMap:// 流程地图
				case process: // 流程图
				case standardProcess: // 流程标准(以流程ID为判断标准)
				case standardProcessMap:// 流程地图标准(以流程地图ID为判断标准)
					sql = "WITH MY_FILE AS(SELECT * FROM Jecn_Flow_Structure_t JF WHERE JF.flow_Id in"
							+ JecnCommonSql.getIdsSet(setIds)
							+ "             UNION ALL SELECT JFF.* FROM MY_FILE INNER JOIN Jecn_Flow_Structure_t JFF  ON MY_FILE.flow_ID=JFF.Pre_Flow_Id)"
							+ "            SELECT  count(*) FROM MY_FILE where projectid=? and flow_Id =?";
					break;
				case ruleFile:// 制度文件
				case ruleModeFile:// 制度模板文件
					sql = "WITH MY_FILE AS(SELECT * FROM jecn_rule_t JF WHERE JF.Id in"
							+ JecnCommonSql.getIdsSet(setIds)
							+ "             UNION ALL SELECT JFF.* FROM MY_FILE INNER JOIN jecn_rule_t JFF  ON MY_FILE.ID=JFF.Per_Id)"
							+ "            SELECT  count(*) FROM MY_FILE where project_id=? and Id =?";
					break;
				case standard: // 文件标准
				case standardClause:// 标准条款
				case standardClauseRequire:// 条款要求
					sql = "WITH MY_FILE AS(SELECT * FROM jecn_criterion_classes JF WHERE JF.Criterion_Class_Id in"
							+ JecnCommonSql.getIdsSet(setIds)
							+ "             UNION ALL SELECT JFF.* FROM MY_FILE INNER JOIN jecn_criterion_classes JFF  ON MY_FILE.Criterion_Class_Id=JFF.Pre_Criterion_Class_Id)"
							+ "            SELECT  count(*) FROM MY_FILE where project_id=? and Criterion_Class_Id =?";
					break;
				case riskPoint:// 风险点
					sql = "WITH MY_FILE AS(SELECT * FROM jecn_risk JF WHERE JF.Id in"
							+ JecnCommonSql.getIdsSet(setIds)
							+ "             UNION ALL SELECT JFF.* FROM MY_FILE INNER JOIN jecn_risk JFF  ON MY_FILE.Id=JFF.Parent_Id)"
							+ "            SELECT  count(*) FROM MY_FILE where project_id=? and Id =?";
					break;
				default:
					sql = "";
					break;
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				switch (treeNodeType) {
				case file:
					sql = "SELECT count(*) FROM JECN_FILE_T JF" + " where JF.project_id=? and JF.File_Id=?"
							+ " CONNECT BY PRIOR JF.File_Id = JF.Per_File_Id" + " START WITH JF.File_Id in"
							+ JecnCommonSql.getIdsSet(setIds);
					break;
				case processMap:// 流程地图
				case process: // 流程图
				case standardProcess: // 流程标准(以流程ID为判断标准)
				case standardProcessMap:// 流程地图标准(以流程地图ID为判断标准)
					sql = " SELECT count(*) FROM Jecn_Flow_Structure_t JF where JF.Projectid=? and JF.Flow_Id=?"
							+ " CONNECT BY PRIOR JF.flow_id = JF.Pre_Flow_Id START WITH JF.Flow_Id in"
							+ JecnCommonSql.getIdsSet(setIds);
					break;
				case ruleFile:// 制度文件
				case ruleModeFile:// 制度模板文件
					sql = "SELECT count(*) FROM jecn_rule_t JF where JF.Project_Id=? and JF.id=?"
							+ " CONNECT BY PRIOR JF.Id = JF.Per_Id START WITH JF.id in"
							+ JecnCommonSql.getIdsSet(setIds);
					break;
				case standard: // 文件标准
				case standardClause:// 标准条款
				case standardClauseRequire:// 条款要求
					sql = " SELECT count(*) FROM jecn_criterion_classes JF where JF.Project_Id=? and JF.Criterion_Class_Id=?"
							+ " CONNECT BY PRIOR JF.Criterion_Class_Id = JF.Pre_Criterion_Class_Id START WITH JF.Criterion_Class_Id in"
							+ JecnCommonSql.getIdsSet(setIds);
					break;
				case riskPoint:// 风险点
					sql = " SELECT count(*) FROM jecn_risk JF where JF.Project_Id=? and JF.Id=?"
							+ " CONNECT BY PRIOR JF.Id = JF.Parent_Id START WITH JF.Id in"
							+ JecnCommonSql.getIdsSet(setIds);
					break;
				default:
					sql = "";
					break;
				}
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				return isAuthFile(fileId, setIds);
			}
			int count = fileDao.countAllByParamsNativeSql(sql, projectId, fileId);
			if (count > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author zhangchen Aug 17, 2012
	 * @description：是否有文件权限
	 * @param fileId
	 * @param setIds
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	private boolean isAuthFile(Long fileId, Set<Long> setIds) {
		try {
			JecnFileBeanT jecnFileBeanT = fileDao.get(fileId);
			if (setIds.contains(jecnFileBeanT.getPerFileId())) {
				return true;
			}
			if (jecnFileBeanT.getPerFileId() != 0) {
				return isAuthFile(jecnFileBeanT.getPerFileId(), setIds);
			}

		} catch (Exception e) {
			log.error("", e);
		}
		return false;
	}

	public IOrganizationDao getOrganizationDao() {
		return organizationDao;
	}

	public void setOrganizationDao(IOrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 * @param jecnFlowStructureT
	 * @throws Exception
	 */
	private void updateTpathAndTLevel(JecnFileBeanT fileDir) throws Exception {
		// 获取上级节点
		JecnFileBeanT preFile = fileDao.get(fileDir.getPerFileId());
		if (preFile == null) {
			fileDir.settLevel(0);
			fileDir.settPath(fileDir.getFileID() + "-");
			fileDir.setViewSort(JecnUtil.concatViewSort("", fileDir.getSortId()));
		} else {
			fileDir.settLevel(preFile.gettLevel() + 1);
			fileDir.settPath(preFile.gettPath() + fileDir.getFileID() + "-");
			fileDir.setViewSort(JecnUtil.concatViewSort(preFile.getViewSort(), fileDir.getSortId()));
		}
		// 更新
		fileDao.update(fileDir);
		fileDao.getSession().flush();
	}

	@Override
	public long addFileDir(JecnFileBeanT fileDir) throws Exception {
		try {
			Date now = new Date();
			fileDir.setUpdateTime(now);
			fileDir.setCreateTime(now);
			fileDir.setSaveType(JecnContants.fileSaveType);
			if (fileDir.getPerFileId() != null && fileDir.getPerFileId().intValue() != 0) {
				JecnFileBeanT pubBean = this.get(fileDir.getPerFileId());
				fileDir.setResPeopleId(pubBean.getResPeopleId());
				fileDir.setTypeResPeople(pubBean.getTypeResPeople());
				fileDir.setCommissionerId(pubBean.getCommissionerId());
			}
			fileDao.save(fileDir);
			// 更新tpath 和tlevel
			updateTpathAndTLevel(fileDir);
			fileDao.getSession().flush();
			orgAccessPermissionsDao.addPermissionisParentNode(fileDir.getPerFileId(), fileDir.getFileID());
			String sql = JecnSqlConstant.JECN_FILE + "=?";
			fileDao.execteNative(sql, fileDir.getFileID());
			JecnTaskCommon.inheritTaskTemplate(flowStructureDao, fileDir.getFileID(), 1, fileDir.getPeopleID());
			// 添加日志`
			JecnUtil.saveJecnJournal(fileDir.getFileID(), fileDir.getFileName(), 2, 1, fileDir.getUpdatePersonId(),
					fileDao);
			return fileDir.getFileID();
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void cancelRelease(Long id, Long peopleId) throws Exception {
		try {
			List<Long> listFileIds = new ArrayList<Long>();
			listFileIds.add(id);
			fileDao.revokeFiles(listFileIds);
			JecnUtil.saveJecnJournals(listFileIds, 2, 18, peopleId, fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * 不记录文控发布
	 */
	@Override
	public void directRelease(Long id, Long peopeId) throws Exception {
		try {
			Date pubTime = null;
			JecnFileBean jecnFile = fileDao.findJecnFileBeanById(id);
			if (jecnFile != null && jecnFile.getPubTime() != null) {
				pubTime = jecnFile.getPubTime();
				fileDao.getSession().evict(jecnFile);
			}
			List<Long> listFileIds = new ArrayList<Long>();
			listFileIds.add(id);
			// 获取文件对象
			fileDao.releaseFiles(listFileIds);
			// 如果正式表发布日期不为空那么还原发布日期
			if (pubTime != null) {
				String sql = "update jecn_file_t set update_time=?,pub_time=? where file_id=?";
				fileDao.execteNative(sql, pubTime, pubTime, id);
				fileDao.getSession().flush();
				sql = "update jecn_file set update_time=?,pub_time=? where file_id=?";
				fileDao.execteNative(sql, pubTime, pubTime, id);
				fileDao.getSession().flush();
			}
			JecnUtil.saveJecnJournals(listFileIds, 2, 17, peopeId, fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 
	 * @param pId
	 * @param projectId
	 * @param orgIds
	 * @param posIds
	 * @param isAdmin
	 * @return List<JecnTreeBean>
	 * @exception
	 */
	@Override
	public List<JecnTreeBean> getChildFilesWeb(Long pId, Long projectId, Long peopleId, boolean isAdmin)
			throws Exception {
		try {
			String sql = "";
			if (!isAdmin && !JecnContants.isFileAdmin) {
				sql = JecnCommonSqlTPath.INSTANCE.getTreeAppend(peopleId, projectId, TYPEAUTH.FILE);
			}
			sql = sql + "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
					+ "case when jft.file_id is null then 0 else 1 end as count,t.CREATE_TIME,t.savetype,t.doc_id"
					+ ",1,t.flow_id,t.sort_id" + " from jecn_file t"
					+ " LEFT JOIN jecn_file jft on t.file_id = jft.per_file_id and jft.del_state=0";
			if (!isAdmin && !JecnContants.isFileAdmin) {
				sql = sql + JecnCommonSqlTPath.INSTANCE.getSqlFuncSubStr();
			}
			sql = sql + " where t.del_state=0 and t.per_file_id= " + pId + " and t.project_id=" + projectId
					+ " and t.hide=1 order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql);

			return findByListObjectsNoDel(list);

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int getFileTotal(FileSearchBean fileSearchBean) throws Exception {
		try {
			String sql = " select count(distinct t.file_id) from jecn_file t " + getFileSearchCommon(fileSearchBean);
			return fileDao.countAllByParamsNativeSql(sql, fileSearchBean.getProjectId());
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 文件搜索
	 * 
	 * @param fileSearchBean
	 * @return
	 */
	private String getFileSearchCommon(FileSearchBean fileSearchBean) {
		String sql = "  LEFT JOIN (SELECT GPT.RELATE_ID, PGR.FIGURE_ID FROM JECN_GROUP_PERMISSIONS GPT "
				+ "  INNER JOIN JECN_POSITION_GROUP_R PGR ON GPT.POSTGROUP_ID =  PGR.GROUP_ID "
				+ "   AND GPT.TYPE = 1 " + "  UNION " + "  SELECT AP.RELATE_ID, AP.FIGURE_ID "
				+ "   FROM JECN_ACCESS_PERMISSIONS AP " + "   WHERE AP.TYPE = 1) jap ON " + "jap.relate_id=t.file_id"
				+ " left join jecn_org_access_permissions joap on joap.type=1 and joap.relate_id = t.file_id"
				+ " left join jecn_flow_org org on org.del_state=0 and org.org_id=t.org_id";
		if (!fileSearchBean.isAdmin() && !JecnContants.isFileAdmin) {
			sql = sql
					+ "  inner join "
					+ JecnCommonSqlTPath.INSTANCE.getFileSearch(fileSearchBean.getPeopleId(), fileSearchBean
							.getProjectId(), TYPEAUTH.FILE) + " on MY_AUTH_FILES.file_id=t.file_id";
		}
		sql = sql + " where t.del_state=0 and t.project_id=?" + " and t.is_dir=1 and t.hide = 1";
		// 文件名称
		if (StringUtils.isNotBlank(fileSearchBean.getFileName())) {
			String[] fileName = fileSearchBean.getFileName().split(" ");
			String lsql = "";
			int namecount = 0;
			for (int f = 0; f < fileName.length; f++) {
				// 文件名称
				if (fileName[f].trim() != null && !"".equals(fileName[f].trim())) {
					if (namecount == 0) {
						lsql = lsql + "select file_name from jecn_file where file_name like '%" + fileName[f].trim()
								+ "%' ";
					} else {
						lsql = lsql + " or file_name like '%" + fileName[f].trim() + "%'";
					}
					namecount++;
				}
			}
			sql = sql + " and t.file_name in (" + lsql + ") ";
		}
		// 文件编号
		if (StringUtils.isNotBlank(fileSearchBean.getFileNum())) {
			String[] fileNum = fileSearchBean.getFileNum().split(" ");
			String nsql = "";
			int numcount = 0;
			for (int f = 0; f < fileNum.length; f++) {
				if (fileNum[f].trim() != null && !"".equals(fileNum[f].trim())) {
					if (numcount == 0) {
						nsql = nsql + " select doc_id from jecn_file where doc_id like '%" + fileNum[f].trim() + "%'";
					} else {
						nsql = nsql + " or doc_id like '%" + fileNum[f].trim() + "%'";
					}
					numcount++;
				}
			}
			sql = sql + " and t.doc_id in (" + nsql + ") ";
		}
		// 责任部门
		if (StringUtils.isNotBlank(fileSearchBean.getOrgName())) {
			if (fileSearchBean.getOrgId() != -1) {
				sql = sql + " and org.org_id =" + fileSearchBean.getOrgId();
			} else {
				sql = sql + " and org.org_name like '%" + fileSearchBean.getOrgName() + "%'";
			}
		}
		// 查阅岗位权限
		if (StringUtils.isNotBlank(fileSearchBean.getPosLookName())) {
			if (fileSearchBean.getPosLookId() != -1) {
				sql = sql + " and jap.figure_id=" + fileSearchBean.getPosLookId();
			} else {
				sql = sql
						+ " and jap.figure_id in (select jfom.figure_id from jecn_flow_org_image jfom,jecn_flow_org jfo_t where jfom.org_id = jfo_t.org_id and jfo_t.projectid="
						+ fileSearchBean.getProjectId() + " and jfo_t.del_state=0 and jfom.figure_type = "
						+ JecnCommonSql.getPosString() + "  and  jfom.figure_text like '%"
						+ fileSearchBean.getPosLookName() + "%')";
			}
		}
		// 查阅部门权限
		if (StringUtils.isNotBlank(fileSearchBean.getOrgLookName())) {
			if (fileSearchBean.getOrgLookId() != -1) {
				sql = sql + " and joap.org_id=" + fileSearchBean.getOrgLookId();
			} else {
				sql = sql + " and joap.org_id in (select jfo_t.org_id from jecn_flow_org jfo_t where jfo_t.projectid="
						+ fileSearchBean.getProjectId() + " and jfo_t.del_state=0 and jfo_t.org_name like '%"
						+ fileSearchBean.getOrgLookName() + "%')";
			}
		}
		// 密级
		if (fileSearchBean.getSecretLevel() != -1) {
			sql = sql + " and t.is_public=" + fileSearchBean.getSecretLevel();
		}
		return sql;
	}

	@Override
	public List<FileWebBean> getFileList(FileSearchBean fileSearchBean, int start, int limit) throws Exception {
		try {
			String sql = " select distinct t.file_id,t.file_name,t.doc_id,t.org_id,org.org_name,t.is_public,t.per_file_id,t.sort_id from jecn_file t "
					+ getFileSearchCommon(fileSearchBean);
			sql += " order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql, start, limit, fileSearchBean.getProjectId());
			List<FileWebBean> listResult = new ArrayList<FileWebBean>();
			for (Object[] obj : list) {
				FileWebBean fileWebBean = JecnUtil.getFileWebBeanByObject(obj);
				if (fileWebBean != null) {
					listResult.add(fileWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 搜索通用查阅条件
	 * 
	 * @param fileSearchBean
	 * @param projectId
	 * @return
	 */
	private String getSql(PublicSearchBean publicSearchBean, Long projectId) {
		StringBuffer str = new StringBuffer();
		str
				.append(" from jecn_file t left join jecn_flow_org org on org.del_state=0 and org.org_id=t.org_id where t.del_state=0 and t.project_id="
						+ projectId + " and t.is_dir=1  ");

		// getSql 新添加密级条件(不能是默认 is_public =1)
		if (-1 != publicSearchBean.getSecretLevel()) { // -1:全部,0秘密,1公开
			// 不拼接is_public条件
			str.append("  and t.is_public= " + publicSearchBean.getSecretLevel());
		}
		// 文件名称
		if (StringUtils.isNotBlank(publicSearchBean.getName())) {
			str.append(" and t.file_name like '%" + publicSearchBean.getName() + "%'");
		}
		// 文件编号
		if (StringUtils.isNotBlank(publicSearchBean.getNumber())) {
			str.append(" and t.doc_id like '%" + publicSearchBean.getNumber() + "%'");
		}
		// 责任部门
		if (StringUtils.isNotBlank(publicSearchBean.getOrgName())) {
			if (publicSearchBean.getOrgId() != -1) {
				str.append(" and t.org_id =" + publicSearchBean.getOrgId());
			} else {
				str
						.append(" and t.org_id in (select jfo.org_id from jecn_flow_org jfo where jfo.del_state=0 and jfo.projectid="
								+ projectId + " and jfo.org_name like '%" + publicSearchBean.getOrgName() + "%')");
			}
		}
		return str.toString();

	}

	@Override
	public int getTotalPublicFile(PublicSearchBean publicSearchBean, Long projectId) throws Exception {
		try {
			String sql = "select count(*)" + this.getSql(publicSearchBean, projectId);
			return fileDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<PublicFileBean> getPublicFile(PublicSearchBean publicSearchBean, int start, int limit, Long projectId)
			throws Exception {
		try {
			String sql = "select t.file_id,t.file_name,t.doc_id,t.org_id,org.org_name,t.is_public"
					+ this.getSql(publicSearchBean, projectId);
			sql += " order by t.per_file_id,t.sort_id,t.file_id";
			List<Object[]> list = fileDao.listNativeSql(sql, start, limit);
			List<PublicFileBean> listResult = new ArrayList<PublicFileBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				PublicFileBean publicFileBean = new PublicFileBean();
				publicFileBean.setId(Long.parseLong(obj[0].toString()));
				if (obj[1] != null) {
					publicFileBean.setName(obj[1].toString());
				} else {
					publicFileBean.setName("");
				}
				if (obj[2] != null) {
					publicFileBean.setNumber(obj[2].toString());
				} else {
					publicFileBean.setNumber("");
				}
				if (obj[4] != null) {
					publicFileBean.setOrgName(obj[4].toString());
				} else {
					publicFileBean.setOrgName("");
				}
				if (obj[5] != null) {
					publicFileBean.setSecretLevel(Integer.parseInt(obj[5].toString()));
				}
				publicFileBean.setType(2);
				listResult.add(publicFileBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int getTotalAttenTionFile(AttenTionSearchBean attenTionSearchBean) throws Exception {
		try {
			String sql = this.attenTionFileSql(attenTionSearchBean, true);
			return fileDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<FileWebBean> getAttenTionFile(AttenTionSearchBean attenTionSearchBean, int start, int limit)
			throws Exception {
		try {
			String sql = this.attenTionFileSql(attenTionSearchBean, false);
			List<Object[]> list = fileDao.listNativeSql(sql, start, limit);
			List<FileWebBean> listResult = new ArrayList<FileWebBean>();
			for (Object[] obj : list) {
				FileWebBean fileWebBean = JecnUtil.getFileWebBeanByObject(obj);
				if (fileWebBean != null) {
					listResult.add(fileWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 搜索通用查阅条件
	 * 
	 * @param fileSearchBean
	 * @param projectId
	 * @return
	 */
	private String attenTionFileSql(AttenTionSearchBean attenTionSearchBean, boolean isCount) {
		String sql = "";
		if (isCount) {
			sql = sql + " select count(c_file.file_id)";
		} else {
			sql = sql + " select c_file.*";
		}
		sql = sql
				+ " from (select distinct t.file_id,t.file_name,t.doc_id,t.org_id,org.org_name,t.is_public,t.per_file_id,t.sort_id"
				+ " from jecn_file t " + " left join jecn_flow_org org on org.del_state=0 and org.org_id=t.org_id";
		if (!attenTionSearchBean.isAdmin() && !JecnContants.isFileAdmin) {
			sql = sql
					+ "  inner join "
					+ JecnCommonSqlTPath.INSTANCE.getFileSearch(attenTionSearchBean.getUserId(), attenTionSearchBean
							.getProjectId(), TYPEAUTH.FILE) + " on MY_AUTH_FILES.file_id=t.file_id";
		}
		sql = sql + " where t.hide=1 and t.del_state=0 and t.is_dir=1";

		// 文件名称
		if (StringUtils.isNotBlank(attenTionSearchBean.getName())) {
			sql = sql + " and t.file_name like '%" + attenTionSearchBean.getName() + "%'";
		}
		// 文件编号
		if (StringUtils.isNotBlank(attenTionSearchBean.getNumber())) {
			sql = sql + " and t.doc_id like '%" + attenTionSearchBean.getNumber() + "%'";
		}
		// 责任部门
		if (attenTionSearchBean.getOrgId() != -1) {
			sql = sql + "  and t.org_id=" + attenTionSearchBean.getOrgId();
		}
		// 密级
		if (attenTionSearchBean.getSecretLevel() != -1) {
			sql = sql + " and t.is_public=" + attenTionSearchBean.getSecretLevel();
		}
		sql = sql + " ) c_file";

		if (!isCount) {
			sql = sql + " order by c_file.per_file_id,c_file.sort_id,c_file.file_id";
		}
		return sql;
	}

	@Override
	public FileBean findFileBean(Long fileId, boolean isPub) throws Exception {
		try {
			FileBean fileBean = new FileBean();
			JecnFileBean jecnFileBean = null;
			if (isPub) {
				jecnFileBean = fileDao.findJecnFileBeanById(fileId);
			} else {
				JecnFileBeanT jecnFileBeanT = fileDao.findJecnFileBeanTById(fileId);
				if (jecnFileBeanT != null) {
					jecnFileBean = new JecnFileBean();
					PropertyUtils.copyProperties(jecnFileBean, jecnFileBeanT);

				}
			}
			if (jecnFileBean == null) {
				return null;
			}
			// 文件ID
			fileBean.setFileID(jecnFileBean.getFileID());
			// 文件名称
			fileBean.setFileName(jecnFileBean.getFileName());
			// 文件编号
			fileBean.setDocId(jecnFileBean.getDocId());
			// 创建人
			Long peopleId = jecnFileBean.getPeopleID();
			if (peopleId != null) {
				JecnUser jecnUser = this.personDao.get(peopleId);
				if (jecnUser != null) {
					fileBean.setCreatePeople(jecnUser.getTrueName());
					fileBean.setCreatePeopleId(peopleId);
				}
			}
			// 创建时间
			fileBean.setCreateTime(jecnFileBean.getCreateTime());
			// 密级
			fileBean.setIsPublic(jecnFileBean.getIsPublic());
			// 更新人
			if (jecnFileBean.getUpdatePersonId() != null) {
				JecnUser jecnUser = this.personDao.get(jecnFileBean.getUpdatePersonId());
				if (jecnUser != null) {
					fileBean.setUpdatePeople(jecnUser.getTrueName());
					fileBean.setUpdatePeopleId(peopleId);
				}
			}
			// 更新时间
			fileBean.setUpdateTime(jecnFileBean.getUpdateTime());
			// 责任部门
			Long orgId = jecnFileBean.getOrgId();
			if (orgId != null) {
				// 部门对象
				JecnFlowOrg jecnFlowOrg = this.organizationDao.get(orgId);
				if (jecnFlowOrg != null && jecnFlowOrg.getOrgName() != null) {
					fileBean.setOrgName(jecnFlowOrg.getOrgName());
					fileBean.setOrgId(orgId);
				} else {
					fileBean.setOrgName("");
				}
			} else {
				fileBean.setOrgName("");
			}
			// 所属流程
			Long flowId = jecnFileBean.getFlowId();
			if (flowId != null) {
				if (isPub) {
					JecnFlowStructure jecnFlowStructure = this.flowStructureDao.findJecnFlowStructureById(flowId);
					if (jecnFlowStructure != null && jecnFlowStructure.getFlowName() != null) {
						fileBean.setFlowName(jecnFlowStructure.getFlowName());
					} else {
						fileBean.setFlowName("");
					}
				} else {
					JecnFlowStructureT jecnFlowStructure = this.flowStructureDao.get(flowId);
					if (jecnFlowStructure != null && jecnFlowStructure.getFlowName() != null) {
						fileBean.setFlowName(jecnFlowStructure.getFlowName());
					} else {
						fileBean.setFlowName("");
					}
				}
			} else {
				fileBean.setFlowName("");
			}
			// 文件被调用的情况
			String hql = "";
			// 流程地图的附件
			if (isPub) {
				hql = "select b.flowId,b.flowName,b.isPublic from JecnMainFlow as a,JecnFlowStructure as b "
						+ "where b.flowId = a.flowId and b.delState=0 and a.fileId=?";
			} else {
				hql = "select b.flowId,b.flowName,b.isPublic from JecnMainFlowT as a,JecnFlowStructureT as b "
						+ "where b.flowId = a.flowId and b.delState=0 and a.fileId=?";
			}
			List<Object[]> list = fileDao.listHql(hql, fileId);
			List<FileUseBean> listFileUserBean = new ArrayList<FileUseBean>();
			// 文件使用情况关联流程架构(地图)Id集合
			List<Long> listFlowMapId = new ArrayList<Long>();
			// 文件使用情况关联流程Id集合
			List<Long> listFlowId = new ArrayList<Long>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.processMap.toString());
				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
				listFlowMapId.add(Long.valueOf(obj[0].toString()));
			}
			// 流程地图/流程图中元素的附件
			if (isPub) {
				hql = "SELECT jfs.flow_id, jfs.flow_name, jfs.is_public,jfs.isFlow FROM JECN_FIGURE_FILE JFF "
						+ " INNER JOIN jecn_flow_structure_image jfsi ON jff.figure_id = jfsi.figure_id "
						+ " INNER JOIN jecn_flow_structure jfs ON jfs.flow_id = jfsi.flow_id  WHERE jfs.DEL_STATE=0 and jff.FILE_ID =?";
			} else {
				hql = "SELECT jfs.flow_id, jfs.flow_name, jfs.is_public,jfs.isFlow FROM JECN_FIGURE_FILE_T JFF "
						+ " INNER JOIN jecn_flow_structure_image_T jfsi ON jff.figure_id = jfsi.figure_id "
						+ " INNER JOIN jecn_flow_structure_T jfs ON jfs.flow_id = jfsi.flow_id  WHERE jfs.DEL_STATE=0 and  jff.FILE_ID =?";
			}
			list = fileDao.listNativeSql(hql, fileId);
			// listFileUserBean = new ArrayList<FileUseBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null || obj[3] == null) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				if ("0".equals(obj[3].toString())) {// 流程地图元素附件
					if (listFlowMapId.contains(Long.valueOf(obj[0].toString()))) {
						continue;
					}
					fileUseBean.setTypeName(TreeNodeType.processMap.toString());
					listFlowMapId.add(Long.valueOf(obj[0].toString()));
				} else if ("1".equals(obj[3].toString())) {// 流程元素附件
					if (listFlowId.contains(Long.valueOf(obj[0].toString()))) {
						continue;
					}
					fileUseBean.setTypeName(TreeNodeType.process.toString());
					listFlowId.add(Long.valueOf(obj[0].toString()));
				}

				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
			}
			listFlowMapId.clear();

			// 流程的附件
			if (isPub) {
				hql = "select b.flowId,b.flowName,b.isPublic " + "from JecnFlowBasicInfo as a,JecnFlowStructure as b "
						+ "where b.flowId = a.flowId and b.delState=0 and a.fileId=?";
			} else {
				hql = "select b.flowId,b.flowName,b.isPublic "
						+ "from JecnFlowBasicInfoT as a,JecnFlowStructureT as b "
						+ "where b.flowId = a.flowId and b.delState=0 and a.fileId=?";
			}
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				if (listFlowId.contains(Long.valueOf(obj[0].toString()))) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.process.toString());
				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
				listFlowId.add(Long.valueOf(obj[0].toString()));
			}
			// 流程的活动操作规范与活动输入
			if (isPub) {
				hql = "select distinct b.flowId,b.flowName,b.isPublic from JecnFlowStructureImage as a,"
						+ "JecnFlowStructure as b,JecnActivityFile as c where b.flowId = a.flowId and "
						+ "b.delState=0 and a.figureId = c.figureId and c.fileSId=?";
			} else {
				hql = "select distinct b.flowId,b.flowName,b.isPublic from JecnFlowStructureImageT as a,"
						+ "JecnFlowStructureT as b,JecnActivityFileT as c where b.flowId = a.flowId and "
						+ "b.delState=0 and a.figureId = c.figureId and c.fileSId=?";
			}
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				if (listFlowId.contains(Long.valueOf(obj[0].toString()))) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.process.toString());
				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
				listFlowId.add(Long.valueOf(obj[0].toString()));
			}
			// 流程的输出
			if (isPub) {
				hql = "select distinct b.flowId,b.flowName,b.isPublic "
						+ "from JecnFlowStructureImage as a,JecnFlowStructure as b,JecnModeFile as c "
						+ "where b.flowId = a.flowId and b.delState=0 and a.figureId = c.figureId and c.fileMId=?";
			} else {
				hql = "select distinct b.flowId,b.flowName,b.isPublic "
						+ "from JecnFlowStructureImageT as a,JecnFlowStructureT as b,JecnModeFileT as c "
						+ "where b.flowId = a.flowId and b.delState=0 and a.figureId = c.figureId and c.fileMId=?";
			}
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				if (listFlowId.contains(Long.valueOf(obj[0].toString()))) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.process.toString());
				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
				listFlowId.add(Long.valueOf(obj[0].toString()));
			}
			// 流程的样例
			if (isPub) {
				hql = "select distinct b.flowId,b.flowName,b.isPublic from "
						+ "JecnFlowStructureImage as a,JecnFlowStructure as b," + "JecnModeFile as c,JecnTemplet as d"
						+ " where b.flowId = a.flowId and b.delState=0 " + " and a.figureId = c.figureId and "
						+ " c.modeFileId = d.modeFileId and d.fileId=?";
			} else {
				hql = "select distinct b.flowId,b.flowName,b.isPublic from "
						+ "JecnFlowStructureImageT as a,JecnFlowStructureT as b,"
						+ "JecnModeFileT as c,JecnTempletT as d" + " where b.flowId = a.flowId and b.delState=0 "
						+ " and a.figureId = c.figureId and " + " c.modeFileId = d.modeFileId and d.fileId=?";
			}
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				if (listFlowId.contains(Long.valueOf(obj[0].toString()))) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.process.toString());
				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
				listFlowId.add(Long.valueOf(obj[0].toString()));
			}
			listFlowId.clear();
			// 标准的附件
			hql = "select criterionClassId,criterionClassName,isPublic,stanType,relationId from StandardBean "
					+ "where stanType=1 and relationId=?";
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null || obj[3] == null) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				if ("1".equals(obj[3].toString())) {
					// 文件格式
					fileUseBean.setTypeName(TreeNodeType.standard.toString());
				} else if ("2".equals(obj[3].toString())) {
					// 文件格式
					fileUseBean.setTypeName(TreeNodeType.standardProcess.toString());
				} else if ("3".equals(obj[3].toString())) {
					// 文件格式
					fileUseBean.setTypeName(TreeNodeType.standardProcessMap.toString());
				}
				// 标准主键ID
				fileUseBean.setId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[4].toString()));
				// 标准类型
				fileUseBean.setStanType(Integer.valueOf(obj[3].toString()));

				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
			}
			// 制度的附件
			if (isPub) {
				hql = "select a.id,a.ruleName,a.isPublic from Rule as a where a.isDir=2 and a.fileId=? and isFileLocal = 0";
			} else {
				hql = "select a.id,a.ruleName,a.isPublic from RuleT as a where a.isDir=2 and a.fileId=? and isFileLocal = 0";
			}
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.ruleFile.toString());
				// 文件ID
				fileUseBean.setId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
			}

			// 制度的表单附件
			if (isPub) {
				hql = "select distinct a.id,a.ruleName,a.isPublic " + "from Rule as a,RuleFile as b,RuleTitle as c "
						+ "where a.id=c.ruleId and c.id=b.ruleTitleId and b.ruleFileId=?";
			} else {
				hql = "select distinct a.id,a.ruleName,a.isPublic " + "from RuleT as a,RuleFileT as b,RuleTitleT as c "
						+ "where a.id=c.ruleId and c.id=b.ruleTitleId and b.ruleFileId=?";
			}
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.ruleModeFile.toString());
				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
			}
			// 流程的图标
			if (isPub) {
				hql = "select b.flowId,b.flowName,b.isPublic,b.isFlow from JecnFlowStructureImage as a,JecnFlowStructure as b where b.flowId = a.flowId and b.delState=0  and a.figureType="
						+ JecnCommonSql.getIconString() + " and a.linkFlowId=?";
			} else {
				hql = "select b.flowId,b.flowName,b.isPublic,b.isFlow from JecnFlowStructureImageT as a,JecnFlowStructureT as b where b.flowId = a.flowId and b.delState=0  and a.figureType="
						+ JecnCommonSql.getIconString() + " and a.linkFlowId=?";
			}
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[2] == null || obj[3] == null) {
					continue;
				}
				FileUseBean fileUseBean = new FileUseBean();
				if ("0".equals(obj[3].toString())) {
					fileUseBean.setTypeName(TreeNodeType.processMap.toString());
				} else {
					// 文件格式
					fileUseBean.setTypeName(TreeNodeType.process.toString());
				}

				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				fileUseBean.setIsPublic(Long.parseLong(obj[2].toString()));
				listFileUserBean.add(fileUseBean);
			}

			// 组织的图标
			hql = "select b.orgId,b.orgName from JecnFlowOrgImage as a,JecnFlowOrg as b where b.orgId = a.orgId and b.delState=0 and a.figureType="
					+ JecnCommonSql.getIconString() + " and a.linkOrgId=?";
			list = fileDao.listHql(hql, fileId);
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null) {
					continue;
				}

				FileUseBean fileUseBean = new FileUseBean();
				// 文件格式
				fileUseBean.setTypeName(TreeNodeType.organization.toString());

				// 文件ID
				fileUseBean.setFileId(Long.valueOf(obj[0].toString()));
				// 文件名称
				if (obj[1] != null) {
					fileUseBean.setFileName(obj[1].toString());
				} else {
					fileUseBean.setFileName("");
				}
				// 组织没有密级
				fileUseBean.setIsPublic(-1L);
				listFileUserBean.add(fileUseBean);
			}
			fileBean.setFileUseList(listFileUserBean);
			return fileBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnFileBeanT> getFileDirsByName(String name, Long projectId) throws Exception {

		try {
			// String hql =
			// "from JecnFileBeanT where isDir=1 and projectId=? and fileName
			// like ? order by perFileId,sortId,fileID";
			// return fileDao.listHql(hql, 0, 20, projectId, "%" + name + "%");
			String sql = "select jf.FILE_ID,"
					+ " jf.FILE_NAME,"
					+ "jf.FILE_PATH,"
					+ "jf.PEOPLE_ID,"
					+ " jf.PER_FILE_ID,"
					+ "jf.CREATE_TIME,"
					+ "jf.UPDATE_TIME,"
					+ "jf.IS_DIR,"
					+ "jf.ORG_ID,"
					+ "jf.DOC_ID,"
					+ "jf.IS_PUBLIC,"
					+ "jf.FLOW_ID,"
					+ " jf.SORT_ID,"
					+ "jf.SAVETYPE,"
					+ "jf.UPDATE_PERSON_ID,"
					+ "jf.PROJECT_ID,"
					+ "jf.VERSION_ID,"
					+ "jf.HISTORY_ID,"
					+ "jfo.org_name,"
					+ "jf.hide,"
					+ "jf.del_state"
					+ " from jecn_file_t jf left join jecn_flow_org jfo on jfo.org_id = jf.org_id and jfo.del_state=0 where jf.del_state=0 and jf.is_dir=0 and jf.project_id=? and jf.file_name like ? "
					+ "order by jf.per_file_id,jf.sort_id,jf.file_id";
			List<Object[]> listFileBeanT = fileDao.listNativeSql(sql, 0, 20, projectId, "%" + name + "%");
			return findByListFileBeanTObjects(listFileBeanT);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 验证当前选中记录是否为发布文件
	 * 
	 * @param fileId
	 *            文件ID
	 * @return int==1 为发布文件
	 * @throws Exception
	 */
	@Override
	public int isPubFileCount(Long fileId) throws Exception {
		String sql = "select count(*) from Jecn_File where file_id=?";
		return fileDao.countAllByParamsNativeSql(sql, fileId);
	}

	/**
	 * 验证当前选中记录是否为发布文件
	 * 
	 * @param fileIds
	 *            文件ID集合
	 * @return 为发布文件总数
	 * @throws Exception
	 */
	@Override
	public int isPubFileCount(List<Long> fileIds) throws Exception {
		String sql = "select count(*) from Jecn_File where file_id in " + JecnCommonSql.getIds(fileIds);
		return fileDao.countAllByParamsNativeSql(sql);
	}

	@Override
	public int getFileTotalByPid(Long pId, Long projectId, Long peopleId, boolean isAdmin) throws Exception {

		try {
			String sql = " with click_Files AS" + " (SELECT C.file_id ,C.per_file_id FROM  JECN_FILE C"
					+ " INNER JOIN JECN_FILE P ON C.T_PATH LIKE " + JecnCommonSql.getJoinFunc("P")
					+ " WHERE P.file_id =? AND C.is_dir=1 AND C.del_state=0 )";

			sql = sql + " select count(click_Files.file_id) from click_Files";
			if (!isAdmin && !JecnContants.isFileAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.FILE)
						+ " on MY_AUTH_FILES.file_id=click_Files.file_id";
			}
			return ruleDao.countAllByParamsNativeSql(sql, pId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<FileWebBean> getFileListByPid(Long pId, Long projectId, Long peopleId, boolean isAdmin, int start,
			int limit) throws Exception {
		try {
			String sql = "";
			// 向下递归
			sql = sql + " with click_Files AS" + " (SELECT C.file_id ,C.per_file_id FROM  JECN_FILE C"
					+ " INNER JOIN JECN_FILE P ON C.T_PATH LIKE " + JecnCommonSql.getJoinFunc("P")
					+ " WHERE P.file_id =? AND C.is_dir=1 AND C.del_state=0 and C.hide=1)";
			sql = sql + " select t.file_id,t.file_name,t.doc_id,t.org_id,org.org_name,t.is_public from jecn_file t "
					+ " left join jecn_flow_org org on org.org_id=t.org_id and org.del_state=0"
					+ " inner join click_Files on click_Files.file_id= t.file_id ";
			if (!isAdmin && !JecnContants.isFileAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.FILE)
						+ " on MY_AUTH_FILES.file_id=t.file_id";
			}
			sql = sql + " order by t.per_file_id,t.sort_id,t.file_id";

			List<Object[]> list = fileDao.listNativeSql(sql, start, limit, pId);
			List<FileWebBean> listResult = new ArrayList<FileWebBean>();
			for (Object[] obj : list) {
				FileWebBean fileWebBean = JecnUtil.getFileWebBeanByObject(obj);
				if (fileWebBean != null) {
					listResult.add(fileWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateHideFiles(Long folderId, int hideFlag) throws Exception {
		try {
			List<Long> listIds = new ArrayList<Long>();
			listIds.add(folderId);
			String sql = "UPDATE JECN_FILE_T SET HIDE = " + hideFlag + " WHERE FILE_ID IN ("
					+ JecnCommonSql.getChildObjectsSqlGetSingleIdFile(listIds) + " )";
			// 更新临时表
			fileDao.execteNative(sql);
			sql = "UPDATE JECN_FILE SET HIDE = " + hideFlag + " WHERE FILE_ID IN ("
					+ JecnCommonSql.getChildObjectsSqlGetSingleIdFile(listIds) + " )";
			// 更新文件表
			fileDao.execteNative(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public FileDetailListWebBean getFileDetailList(long fileId, boolean showAll) throws Exception {
		try {
			// 获取当前级别
			int curLevel = getFileCurLevel(fileId);
			// 获取查询的文件清单的sql
			String sql = getFileDetailListSql(fileId, showAll);
			// 获取数据库查询的结果
			List<Object[]> dataList = fileDao.listNativeSql(sql);
			// 填充数据至fileDetailList
			return fillDataToFileDetailMap(fileId, dataList, curLevel);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author wdp
	 * @param fileId
	 * @description 为存储文件清单的Map集合填充数据
	 * @param dataList
	 *            原始数据
	 * @param fileDetailList
	 *            读取完毕的数据集合
	 * @param culLevel
	 *            当前点击的文件的级别
	 */
	private FileDetailListWebBean fillDataToFileDetailMap(Long fileId, List<Object[]> dataList, int curLevel) {
		FileDetailListWebBean fileDetailListWebBean = new FileDetailListWebBean();
		int maxLevel = 0;
		// 读取信息使用的Map
		Map<Long, FileDetailWebBean> fileMap = new HashMap<Long, FileDetailWebBean>();

		List<FileDetailWebBean> fileDetailList = new ArrayList<FileDetailWebBean>();

		for (Object[] objects : dataList) {
			Long tempFileId = Long.valueOf(objects[3].toString()); // 文件Id
			FileDetailWebBean fileDetailWebBean = fileMap.get(tempFileId); // 获取文件详情
			// 文件第一次存入
			if (fileDetailWebBean == null && objects[12].equals("baseInfo")) {
				fileDetailWebBean = new FileDetailWebBean();
				int level = JecnUtil.valueToInt(objects[0]);
				fileDetailWebBean.setFileId(tempFileId);
				fileDetailWebBean.setPerFileId(JecnUtil.valueToLong(objects[1])); // 父文件ID
				fileDetailWebBean.setFileName(JecnUtil.valueToString(objects[4]));
				fileDetailWebBean.setDocNumber(JecnUtil.valueToString(objects[5])); // 文件编号
				fileDetailWebBean.setLevel(level); // 文件级别
				fileDetailWebBean.setResponsibleDeptName(JecnUtil.valueToString(objects[6]));// 责任部门
				fileDetailWebBean.setCreator(JecnUtil.valueToString(objects[7]));// 创建人
				fileDetailWebBean.setCreateTime(JecnUtil.valueToDateStr(objects[8]));
				fileDetailWebBean.setUpdateTime(JecnUtil.valueToDateStr(objects[9]));
				fileDetailWebBean.setIsDir(JecnUtil.valueToInt(objects[13]));
				// 存入Map
				fileMap.put(tempFileId, fileDetailWebBean);
				// 存入数据集合
				fileDetailList.add(fileDetailWebBean);
				if (fileDetailWebBean.getIsDir() == 0 && maxLevel < level) {
					maxLevel = level;
				}
				continue;
			}
			// 获取该文件的关联信息集合
			List<FileDetailRelatedWebBean> relatedBeanList = fileDetailWebBean.getFileDetailRelatedList();
			FileDetailRelatedWebBean fileRelatedWebBean = new FileDetailRelatedWebBean();
			// 存入关联信息到关联信息集合中
			if (objects[12].equals(TreeNodeType.process.toString())) {
				setFileRelatedIdNameType(fileRelatedWebBean, TreeNodeType.process.toString(), objects);
			} else if (objects[12].equals(TreeNodeType.processMap.toString())) {
				setFileRelatedIdNameType(fileRelatedWebBean, TreeNodeType.processMap.toString(), objects);
			} else if (objects[12].equals(TreeNodeType.ruleFile.toString())) {
				setFileRelatedIdNameType(fileRelatedWebBean, TreeNodeType.ruleFile.toString(), objects);
			} else if (objects[12].equals(TreeNodeType.ruleModeFile.toString())) {
				setFileRelatedIdNameType(fileRelatedWebBean, TreeNodeType.ruleModeFile.toString(), objects);
			} else if (objects[12].equals(TreeNodeType.standard.toString())) {
				setFileRelatedIdNameType(fileRelatedWebBean, TreeNodeType.standard.toString(), objects);
			} else if (objects[12].equals(TreeNodeType.position.toString())) {
				setFileRelatedIdNameType(fileRelatedWebBean, TreeNodeType.position.toString(), objects);
			}

			relatedBeanList.add(fileRelatedWebBean);
		}
		fileMap.clear();
		// 排序后的集合
		List<FileDetailWebBean> sortList = new ArrayList<FileDetailWebBean>();
		Set<Long> fileIdSet = new HashSet<Long>();
		// 排序
		sortFileDataList(fileId, fileDetailList, sortList, fileIdSet);
		// 填充文件清单WebBean
		fileDetailListWebBean.setCurLevel(curLevel);
		fileDetailListWebBean.setMaxLevel(maxLevel + 1);
		fileDetailListWebBean.setFileDetailList(sortList);
		return fileDetailListWebBean;
	}

	/**
	 * 对文件清单进行排序
	 * 
	 * @author weidp
	 * @date 2014-8-28 上午11:32:54
	 * @param fileId
	 * @param listResult
	 * @param listSortResult
	 * @param fileIdSet
	 */
	private void sortFileDataList(Long fileId, List<FileDetailWebBean> listResult,
			List<FileDetailWebBean> listSortResult, Set<Long> fileIdSet) {
		for (FileDetailWebBean fileDetailWebBean : listResult) {
			if (fileId.equals(fileDetailWebBean.getFileId())) {
				listSortResult.add(fileDetailWebBean);
			} else if (fileId.equals(fileDetailWebBean.getPerFileId())) {
				boolean isQuery = false;
				for (Long id : fileIdSet) {
					if (id.equals(fileDetailWebBean.getFileId())) {
						isQuery = true;
					}
				}
				if (!isQuery) {
					fileIdSet.add(fileDetailWebBean.getFileId());
					sortFileDataList(fileDetailWebBean.getFileId(), listResult, listSortResult, fileIdSet);
				}
			}
		}
	}

	/**
	 * @author wdp
	 * @description 读取与文件相关的信息
	 * @param fileRelatedWebBean
	 * @param type
	 * @param objects
	 */
	private void setFileRelatedIdNameType(FileDetailRelatedWebBean fileRelatedWebBean, String type, Object[] objects) {
		fileRelatedWebBean.setRelatedId(JecnUtil.valueToLong(objects[10]));
		fileRelatedWebBean.setRelatedName(JecnUtil.valueToString(objects[11]));
		fileRelatedWebBean.setRelatedType(JecnUtil.valueToString(objects[12]));
	}

	/**
	 * @author weidp
	 * @description 获取文件的级别
	 * @param fileId
	 * @return
	 */
	private int getFileCurLevel(long fileId) {
		JecnFileBeanT bean = this.fileDao.get(fileId);
		return bean == null ? 0 : bean.gettLevel();
	}

	/**
	 * @author weidp
	 * @description 获取查询文件清单的Sql
	 * @param showAll
	 *            是否获取全部信息 true 获取全部 false 10条
	 * @return
	 */
	private String getFileDetailListSql(Long fileId, boolean showAll) {
		List<Long> listIds = new ArrayList<Long>();
		listIds.add(fileId);
		String sql = "WITH FILES AS ( SELECT JF.* FROM JECN_FILE JF  INNER JOIN ("
				+ JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.fileDir)
				+ ") jr on JF.file_id=jr.file_id WHERE JF.DEL_STATE = 0 AND JF.HIDE = 1)";
		return sql
				+ getSelectSqlOfFileDetailList()
				+ (JecnContants.dbType.equals(DBType.ORACLE) ? " order by T_LEVEL nulls last"
						: " ORDER BY CASE WHEN T_LEVEL IS NULL THEN -1 END");

	}

	/**
	 * @author weidp
	 * @description 文件清单的SelectSql
	 * @return
	 */
	private String getSelectSqlOfFileDetailList() {
		String selectSql = " SELECT * FROM (SELECT FILES.T_LEVEL,"// 基本信息查询（0级|文件名称
				// |文件编号|责任部门|创建人|创建时间|更新时间
				// ）
				+ " FILES.PER_FILE_ID,"
				+ " FILES.SORT_ID,"
				+ " FILES.FILE_ID,"
				+ " FILES.FILE_NAME,"
				+ " FILES.DOC_ID,"
				+ " JFO.ORG_NAME,"
				+ " JU.TRUE_NAME,"
				+ " FILES.CREATE_TIME,"
				+ " FILES.PUB_TIME,"
				+ " NULL AS RELATEDID,"
				+ " NULL AS RELATEDNAME,"
				+ " 'baseInfo' AS RELATEDTYPE"
				+ " ,FILES.IS_DIR "
				+ " FROM FILES"
				+ " LEFT JOIN JECN_USER JU ON JU.PEOPLE_ID = FILES.PEOPLE_ID"
				+ " LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = FILES.ORG_ID"
				// 关联的流程地图
				+ " UNION	"
				+ " SELECT "
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID, "
				+ " JFS.FLOW_NAME AS RELATEDNAME,"
				+ " 'processMap' AS RELATEDTYPE "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN JECN_MAIN_FLOW JMF ON JMF.FILE_ID = FILES.FILE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JMF.FLOW_ID = JFS.FLOW_ID "
				+ " WHERE JFS.DEL_STATE = 0"
				// 关联的流程
				+ " UNION 	"
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID, "
				+ " JFS.FLOW_NAME AS RELATEDNAME,"
				+ " 'process' AS RELATEDTYPE   "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN JECN_FLOW_BASIC_INFO JFBI ON JFBI.FILE_ID =FILES.FILE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JFS.FLOW_ID = JFBI.FLOW_ID"
				+ " WHERE JFS.DEL_STATE = 0"
				// 关联的活动输入和规范
				+ " UNION 	"
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID,   "
				+ " JFS.FLOW_NAME AS RELATEDNAME,"
				+ " 'process' AS RELATEDTYPE "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN JECN_ACTIVITY_FILE JAF ON JAF.FILE_S_ID = FILES.FILE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE JFSI ON JFSI.FIGURE_ID =JAF.FIGURE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JFS.FLOW_ID = JFSI.FLOW_ID"
				+ " WHERE JFS.DEL_STATE = 0"
				// 关联的活动输出
				+ " UNION 	"
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID,"
				+ " JFS.FLOW_NAME AS RELATEDNAME,"
				+ " 'process' AS RELATEDTYPE "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN JECN_MODE_FILE JMOF ON JMOF.FILE_M_ID = FILES.FILE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE JFSI ON JFSI.FIGURE_ID = JMOF.FIGURE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JFS.FLOW_ID = JFSI.FLOW_ID "
				+ " WHERE JFS.DEL_STATE = 0"
				// 关联的流程模板
				+ " UNION 	"
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID,    "
				+ " JFS.FLOW_NAME AS RELATEDNAME, "
				+ " 'process' AS RELATEDTYPE  "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN JECN_TEMPLET JT ON JT.FILE_ID = FILES.FILE_ID"
				+ " LEFT JOIN JECN_MODE_FILE JMOF ON JMOF.MODE_FILE_ID =JT.MODE_FILE_ID "
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE JFSI ON JFSI.FIGURE_ID = JMOF.FIGURE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JFS.FLOW_ID = JFSI.FLOW_ID "
				+ " WHERE JFS.DEL_STATE = 0"
				// 关联的标准
				+ " UNION 	"
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JCC.CRITERION_CLASS_ID AS RELATEDID,     "
				+ " JCC.CRITERION_CLASS_NAME AS RELATEDNAME,   "
				+ " 'standard' AS RELATEDTYPE                "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN JECN_CRITERION_CLASSES JCC ON JCC.RELATED_ID = FILES.FILE_ID"
				+ " WHERE JCC.STAN_TYPE=1"
				// 关联的制度
				+ " UNION	"
				+ " SELECT "
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JR.ID AS RELATEDID,   "
				+ " JR.RULE_NAME AS RELATEDNAME,"
				+ " 'ruleFile' AS RELATEDTYPE       "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN JECN_RULE JR ON JR.FILE_ID = FILES.FILE_ID"
				+ " WHERE JR.IS_DIR = 2"
				// 关联的制度模板文件
				+ " UNION 	"
				+ " SELECT "
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JR.ID AS RELATEDID,        "
				+ " JR.RULE_NAME AS RELATEDNAME,"
				+ " 'ruleModeFile' AS RELATEDTYPE     "
				+ " ,FILES.IS_DIR "
				+ " FROM FILES "
				+ " LEFT JOIN RULE_FILE  RF ON RF.RULE_FILE_ID = FILES.FILE_ID"
				+ " LEFT JOIN RULE_TITLE RT ON RT.ID = RF.RULE_TITLE_ID"
				+ " LEFT JOIN JECN_RULE  JR ON JR.ID = RT.RULE_ID "
				+ " WHERE JR.ID IS NOT NULL"
				// 关联的流程地图中使用的文档
				+ " UNION 	"
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID,         "
				+ " JFS.FLOW_NAME AS RELATEDNAME,     "
				+ " 'processMap' AS RELATEDTYPE  ,FILES.IS_DIR  FROM FILES "
				+ " LEFT JOIN JECN_FIGURE_FILE JFF ON JFF.FILE_ID=FILES.FILE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE JFSI ON JFSI.FIGURE_ID = JFF.FIGURE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JFS.FLOW_ID = JFSI.FLOW_ID "
				+ " WHERE JFS.DEL_STATE = 0 AND JFSI.FIGURE_TYPE = 'FileImage' AND JFS.ISFLOW=0"
				// 关联的流程地图中元素的附件
				+ " UNION "
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID,         "
				+ " JFS.FLOW_NAME AS RELATEDNAME,     "
				+ " 'processMap' AS RELATEDTYPE  ,FILES.IS_DIR  FROM FILES "
				+ "	inner join JECN_FIGURE_FILE  JFF on files.file_id=jff.file_id "
				+ "	inner join jecn_flow_structure_image jfsi on jff.figure_id=jfsi.figure_id "
				+ "	inner join jecn_flow_structure jfs on jfs.flow_id=jfsi.flow_id "
				+ " WHERE JFS.DEL_STATE = 0 AND JFS.ISFLOW=0"
				// 关联的流程中使用的文档
				+ " UNION  	 "
				+ " SELECT"
				+ " NULL,NULL,NULL,FILES.FILE_ID, NULL,NULL,  NULL, NULL, NULL,NULL,"
				+ " JFS.FLOW_ID AS RELATEDID,     "
				+ " JFS.FLOW_NAME AS RELATEDNAME,    "
				+ " 'process' AS RELATEDTYPE  ,FILES.IS_DIR  FROM FILES "
				+ " LEFT JOIN JECN_FIGURE_FILE JFF ON JFF.FILE_ID=FILES.FILE_ID"
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE JFSI ON JFSI.FIGURE_ID = JFF.FIGURE_ID "
				+ " LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JFS.FLOW_ID = JFSI.FLOW_ID "
				+ " WHERE JFS.DEL_STATE = 0 AND JFSI.FIGURE_TYPE = 'FileImage' AND JFS.ISFLOW=1"
				// 关联岗位中使用的附件
				+ " UNION "
				+ " SELECT NULL,NULL,NULL,FILES.FILE_ID,NULL,NULL,NULL,NULL,NULL,NULL,"
				+ " JPFI.FIGURE_ID AS RELATEDID,"
				+ " JFOI.FIGURE_TEXT AS RELATEDNAME,"
				+ " 'position' AS RELATEDTYPE,"
				+ " FILES.IS_DIR"
				+ " FROM FILES"
				+ " INNER JOIN JECN_POSITION_FIG_INFO JPFI"
				+ " ON JPFI.FILE_ID = FILES.FILE_ID"
				+ " INNER JOIN JECN_FLOW_ORG_IMAGE JFOI"
				+ " ON JFOI.FIGURE_ID = JPFI.FIGURE_ID"
				+ " INNER JOIN JECN_FLOW_ORG JFO"
				+ " ON JFO.ORG_ID=JFOI.ORG_ID" + " WHERE JFO.DEL_STATE = 0 ) TEMP ";
		return selectSql;
	}

	@Override
	public byte[] downLoadFileDetailListExcel(FileDetailListWebBean fileDetailList) {
		if (fileDetailList == null || fileDetailList.getFileDetailList() == null) {
			throw new NullPointerException();
		}
		WritableWorkbook wbookData = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path + JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			File file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// 文件清单Sheet
			WritableSheet wsheet = wbookData.createSheet(JecnUtil.getValue("fileDetailList"), 0);
			// 设置标题字体样式
			WritableFont fontTitle1 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK); // 字体加粗
			WritableCellFormat wformatTitle1 = SetExcelStyle
					.setExcelCellStyle(new ExcelFont(fontTitle1, Colour.YELLOW));
			// 设置字体标题背景特殊样式
			WritableCellFormat wformatTitle2 = SetExcelStyle.setExcelCellStyle(new ExcelFont(fontTitle1,
					Colour.LIGHT_ORANGE));
			// 设置字体样式（普通）
			WritableFont fontStyle3 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK);

			// 设置单元格内容居中
			WritableCellFormat wformatContecnt3 = SetExcelStyle.setExcelCellStyle(new ExcelFont(Colour.WHITE,
					VerticalAlignment.CENTRE, Alignment.CENTRE, fontStyle3));
			wformatContecnt3.setWrap(true);
			// 设置字体样式（普通）
			WritableFont fontStyle4 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
			// 设置单元格内容居中
			WritableCellFormat wformatContecnt4 = SetExcelStyle.setExcelCellStyle(new ExcelFont(Colour.WHITE,
					VerticalAlignment.CENTRE, Alignment.CENTRE, fontStyle4));
			wformatContecnt4.setWrap(true);
			// 设置字体样式（蓝色）
			WritableFont fontStyle5 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLUE_GREY);
			WritableCellFormat wformatContecnt5 = SetExcelStyle.setExcelCellStyle(new ExcelFont(Colour.WHITE,
					VerticalAlignment.CENTRE, Alignment.LEFT, fontStyle5));
			wformatContecnt5.setWrap(true);

			// 设置字体样式（蓝色）
			WritableFont fontStyle6 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLUE_GREY);
			// 设置单元格内容居左、垂直：居上
			WritableCellFormat wformatContecnt6 = SetExcelStyle.setExcelCellStyle(new ExcelFont(Colour.WHITE,
					VerticalAlignment.TOP, Alignment.LEFT, fontStyle6));
			wformatContecnt6.setWrap(true);

			List<WritableCellFormat> cellFormats = new ArrayList<WritableCellFormat>();
			cellFormats.add(wformatTitle1);
			cellFormats.add(wformatTitle2);
			cellFormats.add(wformatContecnt3);
			cellFormats.add(wformatContecnt4);
			cellFormats.add(wformatContecnt5);
			cellFormats.add(wformatContecnt6);
			// 初始化标题行（样式及文本）
			addSheetTitleCell(fileDetailList, cellFormats, wsheet);
			// 初始化数据行
			addSheetData(fileDetailList, cellFormats, wsheet);
			// 写入Exel工作表
			wbookData.write();
			// 关闭Excel工作薄对象
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			// 把一个文件转化为字节
			byte[] tempDate = JecnUtil.getByte(file);
			if (file.exists()) {
				file.delete();
			}
			return tempDate;
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("关闭WritableWorkbook异常", e);
				}
			}
		}

		return null;
	}

	/**
	 * @author weidp
	 * @description 添加标题行
	 * @param fileDetailList
	 * @param wsheet
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	private void addSheetTitleCell(FileDetailListWebBean fileDetailList, List<WritableCellFormat> cellFormats,
			WritableSheet wsheet) throws WriteException, RowsExceededException {

		// 总级别数
		int levelTotal = fileDetailList.getMaxLevel();
		int count = 1; // 行号
		wsheet.setRowView(1, 440);
		wsheet.addCell(new Label(0, 1, "", cellFormats.get(0)));
		for (int i = 0; i < levelTotal; i++) {
			// 级别
			if (i >= fileDetailList.getCurLevel()) {
				wsheet.setColumnView(count, 20);
				wsheet.addCell(new Label(count, 1, i + JecnUtil.getValue("level"), cellFormats.get(0)));
				count++;
			}
		}
		// 文件名称
		wsheet.setColumnView(count, 55);
		wsheet.addCell(new Label(count, 1, JecnUtil.getValue("fileName"), cellFormats.get(1)));
		// 文件编号
		wsheet.setColumnView(count + 1, 25);
		wsheet.addCell(new Label(count + 1, 1, JecnUtil.getValue("fileNumber"), cellFormats.get(1)));
		// 责任部门
		wsheet.setColumnView(count + 2, 25);
		wsheet.addCell(new Label(count + 2, 1, JecnUtil.getValue("responsibilityDepartment"), cellFormats.get(0)));
		// 创建人
		wsheet.setColumnView(count + 3, 12);
		wsheet.addCell(new Label(count + 3, 1, JecnUtil.getValue("fileCreator"), cellFormats.get(0)));
		// 创建时间
		wsheet.setColumnView(count + 4, 12);
		wsheet.addCell(new Label(count + 4, 1, JecnUtil.getValue("createTime"), cellFormats.get(0)));
		// 更新时间
		wsheet.setColumnView(count + 5, 12);
		wsheet.addCell(new Label(count + 5, 1, JecnUtil.getValue("updateTime"), cellFormats.get(0)));
		// 文件使用情况
		wsheet.setColumnView(count + 6, 30);
		wsheet.addCell(new Label(count + 6, 1, JecnUtil.getValue("fileUse"), cellFormats.get(0)));
	}

	/***************************************************************************
	 * 递归查找当前文件的所有父级目录，并且拼装到JSON中
	 * 
	 * @param fileDetail
	 *            文件清单封装 fileDetailList 文件清单集合类 bufer
	 * @author chehuanbo
	 * 
	 * @since V3.06
	 */
	private static void findDir(FileDetailWebBean fileDetail, FileDetailListWebBean fileDetailList,
			List<RoleDir> roleDirExcelList) {

		FileDetailWebBean fileDetail1 = fileDetail;

		boolean isExit = false; // 判断是否存在父节点 ，默认：false 不存在

		for (FileDetailWebBean perFileDetail : fileDetailList.getFileDetailList()) {
			if (fileDetail1.getPerFileId().longValue() == perFileDetail.getFileId()) {
				RoleDir roleDir = new RoleDir();
				roleDir.setRoleName(perFileDetail.getFileName());
				roleDirExcelList.add(roleDir);
				// 更改临时数据bean 主要用来下一次查找父级节点的查找
				fileDetail1 = perFileDetail;
				isExit = true;
				break;
			}
		}

		if (isExit) { // 如果存在父节点，继续递归查找
			findDir(fileDetail1, fileDetailList, roleDirExcelList);
		}
	}

	/**
	 * @author weidp
	 * @description 创建内容行
	 * @param fileDetailList
	 * @param wsheet
	 * @param wformatContent
	 * @throws WriteException
	 * @throws RowsExceededException
	 *             修改人：chehuanbo 描述：修改文件清单显示样式
	 * @since V3.05
	 */
	private void addSheetData(FileDetailListWebBean fileDetailList, List<WritableCellFormat> cellFormats,
			WritableSheet wsheet) throws RowsExceededException, WriteException {

		int curLevel = fileDetailList.getCurLevel(); // 选中的级别
		int maxLevel = fileDetailList.getMaxLevel(); // 最大级别数
		int rowNum = 2; // 行
		int num = 1; // 行号
		for (int i = 0; i < fileDetailList.getFileDetailList().size(); i++) {
			FileDetailWebBean fileDetail = fileDetailList.getFileDetailList().get(i);
			// 如果是目录跳过，因为是以文件为基础查找的上级目录
			if (fileDetail.getIsDir() == 0) {
				continue;
			}
			// 目录临时集合
			List<RoleDir> roleDirs = new ArrayList<RoleDir>();
			// 查找当前文件的所有父级目录
			findDir(fileDetail, fileDetailList, roleDirs);
			// 获取当前风险总共有几级目录
			int total = roleDirs.size() - 1;
			// 行号
			wsheet.addCell(new Label(0, rowNum, String.valueOf(num), cellFormats.get(2)));
			// ==添加目录到excel Start==
			for (int j = 1; j <= maxLevel; j++) {
				if (j <= roleDirs.size()) {
					String roleName = roleDirs.get(total).getRoleName();
					// 添加级别名称
					wsheet.addCell(new Label(j, rowNum, roleName, cellFormats.get(2)));
				} else {
					wsheet.addCell(new Label(j, rowNum, "", cellFormats.get(2)));
				}
				if (total >= 0) {
					total--;
				}
			}
			wsheet.addCell(new Label(maxLevel - curLevel + 1, rowNum, fileDetail.getFileName(), cellFormats.get(4)));
			wsheet.addCell(new Label(maxLevel - curLevel + 2, rowNum, fileDetail.getDocNumber(), cellFormats.get(2)));
			wsheet.addCell(new Label(maxLevel - curLevel + 3, rowNum, fileDetail.getResponsibleDeptName(), cellFormats
					.get(2)));
			wsheet.addCell(new Label(maxLevel - curLevel + 4, rowNum, fileDetail.getCreator(), cellFormats.get(2)));
			wsheet.addCell(new Label(maxLevel - curLevel + 5, rowNum, fileDetail.getCreateTime(), cellFormats.get(2)));
			wsheet.addCell(new Label(maxLevel - curLevel + 6, rowNum, fileDetail.getUpdateTime(), cellFormats.get(2)));
			wsheet.addCell(new Label(maxLevel - curLevel + 7, rowNum, getRelatedList(fileDetail
					.getFileDetailRelatedList()), cellFormats.get(5)));
			// 设置列宽
			wsheet.setColumnView(maxLevel - curLevel + 1, 25);
			wsheet.setColumnView(maxLevel - curLevel + 2, 25);
			wsheet.setColumnView(maxLevel - curLevel + 3, 25);
			wsheet.setColumnView(maxLevel - curLevel + 4, 25);
			wsheet.setColumnView(maxLevel - curLevel + 5, 25);
			wsheet.setColumnView(maxLevel - curLevel + 6, 25);
			wsheet.setColumnView(maxLevel - curLevel + 7, 55);
			rowNum++;
			num++;
		}
		// 总计
		wsheet.setRowView(0, 360);
		wsheet.addCell(new Label(1, 0, JecnUtil.getValue("total") + String.valueOf(num - 1), cellFormats.get(3)));
	}

	/**
	 * @author weidp
	 * @description 获取文件使用情况的String
	 * @param fileDetailRelatedList
	 * @return
	 */
	private String getRelatedList(List<FileDetailRelatedWebBean> fileDetailRelatedList) {
		StringBuffer relateBuf = new StringBuffer();
		for (int i = 0; i < fileDetailRelatedList.size(); i++) {
			FileDetailRelatedWebBean relateBean = fileDetailRelatedList.get(i);
			if (relateBean.getRelatedType().equals(TreeNodeType.process.toString())) {// 流程
				relateBuf.append(relateBean.getRelatedName()).append("[" + JecnUtil.getValue("flow") + "]");
			} else if (relateBean.getRelatedType().equals(// 流程地图
					TreeNodeType.processMap.toString())) {
				relateBuf.append(relateBean.getRelatedName()).append("[" + JecnUtil.getValue("flowMap") + "]");
			} else if (relateBean.getRelatedType().equals( // 制度文件
					TreeNodeType.ruleFile.toString())) {
				relateBuf.append(relateBean.getRelatedName()).append("[" + JecnUtil.getValue("ruleFile") + "]");
			} else if (relateBean.getRelatedType().equals( // 制度模板文件
					TreeNodeType.ruleModeFile.toString())) {
				relateBuf.append(relateBean.getRelatedName()).append("[" + JecnUtil.getValue("ruleModeFile") + "]");
			} else if (relateBean.getRelatedType().equals( // 标准
					TreeNodeType.standard.toString())) {
				relateBuf.append(relateBean.getRelatedName()).append("[" + JecnUtil.getValue("standard") + "]");
			} else if (relateBean.getRelatedType().equals( // 岗位
					TreeNodeType.position.toString())) {
				relateBuf.append(relateBean.getRelatedName()).append("[" + JecnUtil.getValue("pos") + "]");
			}

			if (i < fileDetailRelatedList.size() - 1) {
				relateBuf.append("\n");
			}
		}
		return relateBuf.toString();
	}

	/***************************************************************************
	 * 文件回收站 树显示
	 * 
	 * @param projectId
	 *            项目ID
	 * @param pId
	 *            父节点id
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pId) throws Exception {

		try {
			String sql = "";
			if (JecnContants.dbType.equals(DBType.MYSQL)) {
				sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
						+ " CASE WHEN jft.file_id is not null THEN '1' ELSE '0' END AS child_count,"
						+ " t.CREATE_TIME,t.savetype,t.doc_id,"
						+ " CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.del_state,t.sort_id"
						+ " from jecn_file_t t" + " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id"
						+ " LEFT JOIN jecn_file_t jft ON t.file_id = jft.per_file_id"
						+ " where t.del_state <> 2 and t.project_id =? order by t.per_file_id,t.sort_id,t.file_id";
				List<Object[]> list = fileDao.listNativeSql(sql, projectId);
				return findByListObjectsRecy(list);
			} else {
				sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
						+ " CASE WHEN jft.file_id is not null THEN '1' ELSE '0' END AS child_count,"
						+ " t.CREATE_TIME,t.savetype,t.doc_id,"
						+ " CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.del_state,t.sort_id"
						+ " from jecn_file_t t"
						+ " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id"
						+ " LEFT JOIN jecn_file_t jft ON t.file_id = jft.per_file_id"
						+ " where t.del_state <> 2 and t.per_file_id = ? and t.project_id =? order by t.per_file_id,t.sort_id,t.file_id";
				List<Object[]> list = fileDao.listNativeSql(sql, pId, projectId);
				return findByListObjectsRecy(list);
			}

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 文件回收站：恢复当前节点
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@Override
	public void updateRecycleFile(List<Long> ids) throws Exception {
		try {
			if (ids != null && ids.size() > 0) {
				String sql = "update jecn_file_t set del_state=0 where file_id in" + JecnCommonSql.getIds(ids);
				fileDao.execteNative(sql);
				sql = "update jecn_file set del_state=0 where file_id in" + JecnCommonSql.getIds(ids);
				fileDao.execteNative(sql);
			}
		} catch (Exception e) {
			log.error("恢复删除的文件出错", e);
			throw e;
		}
	}

	/**
	 * 文件回收站：恢复当前节点及子节点
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@Override
	public void updateRecycleFileOrDirIds(List<Long> listIds, Long projectId) throws Exception {
		if (listIds.isEmpty()) {
			return;
		}
		try {
			String sql = "";
			Set<Long> setFileTIds = new HashSet<Long>();
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = " WITH MY_FILE AS (" + "  SELECT * FROM JECN_FILE_T JF" + "  WHERE JF.File_Id in "
						+ JecnCommonSql.getIds(listIds) + "  UNION ALL" + "  SELECT JFF.*" + "  FROM MY_FILE"
						+ "  INNER JOIN JECN_FILE_T JFF ON MY_FILE.FILE_ID = JFF.Per_File_Id)"
						+ "  SELECT distinct FILE_ID FROM MY_FILE";
				List<Object> list = fileDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setFileTIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "  select distinct JF.File_Id" + "  from JECN_FILE_T JF"
						+ "  CONNECT BY PRIOR JF.FILE_ID = JF.PER_FILE_ID" + "  START WITH JF.FILE_ID in "
						+ JecnCommonSql.getIds(listIds);
				List<Object> list = fileDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setFileTIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				sql = "select tt.file_id,tt.per_file_id from jecn_file_t tt where tt.project_id=?";
				List<Object[]> listAll = fileDao.listNativeSql(sql, projectId);
				setFileTIds = JecnCommon.getAllChilds(listIds, listAll);
			}
			if (setFileTIds.size() > 0) {
				sql = "update jecn_file_t  set del_State=0 where del_state<>2 and   file_ID in";
				List<String> listSql = JecnCommonSql.getSetSqls(sql, setFileTIds);
				for (String sqlStr : listSql) {
					fileDao.execteNative(sqlStr);
				}
				sql = "update jecn_file set del_State=0 where  del_state<>2 and   file_ID in";
				listSql = JecnCommonSql.getSetSqls(sql, setFileTIds);
				for (String sqlStr : listSql) {
					fileDao.execteNative(sqlStr);
				}
			}
		} catch (Exception e) {
			log.error("恢复文件目录及子节点出错", e);
			throw e;
		}
	}

	/***************************************************************************
	 * 文件管理删除文件(假删：放入回收站)
	 * 
	 * @param listIds
	 *            文件ID集合
	 * @param projectId
	 *            项目ID
	 * @param updatePersonId
	 *            更新人ID
	 * @throws Exception
	 */
	@Override
	public void deleteFileManage(List<Long> listIds, Long projectId, Long updatePersonId, TreeNodeType treeNodeType)
			throws Exception {
		try {

			String sql = "update jecn_file_t set del_State=1 where del_State <> 2 and file_ID in "
					+ JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, treeNodeType);
			fileDao.execteNative(sql);
			sql = "update jecn_file  set del_State=1 where del_State <> 2 and file_ID in "
					+ JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, treeNodeType);
			fileDao.execteNative(sql);
			JecnUtil.saveJecnJournals(listIds, 2, 6, updatePersonId, baseDao);
		} catch (Exception e) {
			log.error("恢复文件目录及子节点出错", e);
			throw e;
		}
	}

	/**
	 * 文件回收站 搜索
	 */
	@Override
	public List<JecnTreeBean> getAllFilesByName(String name, Long projectId, Set<Long> fileIds, boolean isAdmin)
			throws Exception {
		String sql = "select jft.file_id, jft.file_name, jft.is_dir,jft.per_file_id" + "  from jecn_file_t jft ";
		if (!isAdmin && !JecnContants.dbType.equals(DBType.MYSQL)) {
			if (fileIds.size() > 0) {
				sql = sql + " INNER JOIN (" + JecnCommonSql.getChildObjectsSqlGetSingleIdFile(fileIds)
						+ ") DESIGN_AUTH ON DESIGN_AUTH.FILE_ID=jft.FILE_ID";
			} else {
				return new ArrayList<JecnTreeBean>();
			}
		}
		sql = sql + " where jft.file_name like '%" + name + "%' and jft.del_state=1 and jft.project_id=?";
		if (!isAdmin && !JecnContants.dbType.equals(DBType.MYSQL)) {
			if (fileIds.size() > 0) {
				sql = sql + " and jft.FILE_ID not in " + JecnCommonSql.getIdsSet(fileIds);
			}
		}
		List<Object[]> objs = fileDao.listNativeSql(sql, 0, 40, projectId);
		return getFileListByObjs(objs);
	}

	/**
	 * 文件回收站 搜索 封装BEAN
	 * 
	 * @param objs
	 *            0 id 1 name 2 isDir 3 pid
	 * @return
	 */
	private List<JecnTreeBean> getFileListByObjs(List<Object[]> objs) {
		List<JecnTreeBean> flieList = new ArrayList<JecnTreeBean>();
		if (objs.size() == 0) {
			return flieList;
		}
		for (Object[] obj : objs) {
			if (obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
				continue;
			}
			JecnTreeBean fileT = new JecnTreeBean();
			fileT.setId(Long.valueOf(obj[0].toString()));
			fileT.setName(obj[1].toString());
			if (Integer.parseInt(obj[2].toString()) == 1) {// 文件

				fileT.setTreeNodeType(TreeNodeType.file);
			} else {
				fileT.setTreeNodeType(TreeNodeType.fileDir);
			}

			fileT.setPid(Long.valueOf(obj[3].toString()));
			flieList.add(fileT);
		}
		return flieList;
	}

	/**
	 * 文件回收站 定位
	 * 
	 * @param id
	 *            文件id
	 * @param projectId
	 *            项目id
	 */
	@Override
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception {
		JecnFileBeanT jecnFileBeanT = fileDao.get(id);
		Set<Long> set = JecnCommon.getPositionTreeIds(jecnFileBeanT.gettPath(), id);
		String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.Is_Dir"
				+ ",case when jft.file_id is null then 0 else 1 end as count" + ",t.CREATE_TIME,t.savetype,t.doc_id"
				+ ",CASE WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.del_state,T.SORT_ID"
				+ " from jecn_file_t t LEFT JOIN jecn_file jf ON t.file_id = jf.file_id "
				+ " LEFT JOIN jecn_file_t jft ON t.file_id = jft.per_file_id" + " where t.per_file_id in "
				+ JecnCommonSql.getIdsSet(set) + " and t.project_id=? " + " ORDER BY T.PER_FILE_ID,T.SORT_ID,T.FILE_ID";
		List<Object[]> list = fileDao.listNativeSql(sql, projectId);
		return findByListObjectsRecy(list);
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description： 文件回收站 封装Bean
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjectsRecy(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = this.getJecnTreeBeanByObjectRecy(obj);
			if (jecnTreeBean != null)
				listResult.add(jecnTreeBean);

		}
		return listResult;
	}

	/**
	 * 文件对象转换成JecnTreeBean
	 * 
	 * @param obj
	 * @return
	 */
	private JecnTreeBean getJecnTreeBeanByObjectRecy(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null
				|| obj[8] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		if ("0".equals(obj[3].toString())) {
			// 文件目录
			jecnTreeBean.setTreeNodeType(TreeNodeType.fileDir);
		} else {
			// 角色节点
			jecnTreeBean.setTreeNodeType(TreeNodeType.file);
		}
		// 文件节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 文件节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 文件节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 文件节点父类名称
		jecnTreeBean.setPname("");
		if (Integer.parseInt(obj[4].toString()) > 0) {
			// 文件节点是否有子节点
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}
		// 文件的创建时间
		if (obj[5] != null) {
			jecnTreeBean.setCreateTime((Date) obj[5]);
		}
		if (obj[6] != null) {
			// 保存数据的方式
			jecnTreeBean.setSaveType(Integer.valueOf(obj[6].toString()));
		}
		// 文件编号
		if (obj[7] != null) {
			jecnTreeBean.setNumberId(obj[7].toString());
		} else {
			jecnTreeBean.setNumberId("");
		}
		// 是否发布
		if ("0".equals(obj[8].toString())) {
			jecnTreeBean.setPub(false);
		} else {
			jecnTreeBean.setPub(true);
		}

		// 是否删除
		if (obj.length > 9) {
			if (obj[9] != null && !"".equals(obj[9].toString())) {
				jecnTreeBean.setIsDelete(Integer.parseInt(obj[9].toString()));
			}
		}
		return jecnTreeBean;
	}

	@Override
	public int getMaxSortId(Long id) throws Exception {

		String sql = "SELECT MAX(JF.SORT_ID) FROM JECN_FILE_T JF WHERE JF.PER_FILE_ID=?";
		Object obj = fileDao.getObjectNativeSql(sql, id);
		int maxSort = 0;
		if (obj != null) {
			maxSort = Integer.valueOf(obj.toString());
		}
		return maxSort;
	}

	@Override
	public List<JecnTaskHistoryNew> getTaskHistoryNew(long fileId) throws Exception {
		try {
			return fileDao.getTaskHistoryNewListById(fileId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 文控信息版本记录
	 * 
	 * @param id
	 *            ID
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	@Override
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long id) throws Exception {
		try {
			return docControlDao.getJecnTaskHistoryNew(id);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public Map<String, String> getFileNameAndContentId(long fileId, boolean isPub) throws Exception {
		Map<String, String> content = new HashMap<String, String>();
		/* if (isHistory) { */
		FileOpenBean openBean = JecnDaoUtil.getFileOpenBean(fileDao, fileId);
		content.put("FileName", openBean.getName());
		content.put("ContentId", String.valueOf(fileId));
		return content;
		/* } */

		/*
		 * if (isPub) { JecnFileBean file =
		 * fileDao.findJecnFileBeanById(fileId); content.put("FileName",
		 * file.getFileName()); content.put("ContentId",
		 * file.getVersionId().toString()); } else { JecnFileBeanT fileT =
		 * fileDao.get(fileId); content.put("FileName", fileT.getFileName());
		 * content.put("ContentId", fileT.getVersionId().toString()); }
		 */
	}

	@Override
	public JecnFileBean findJecnFileBeanById(Long fileId) throws Exception {
		return this.fileDao.findJecnFileBeanById(fileId);
	}

	@Override
	public void batchReleaseNotRecord(List<Long> ids, Long peopleId) throws Exception {
		try {
			fileDao.releaseFiles(ids);
			JecnUtil.saveJecnJournals(ids, 2, 17, peopleId, fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void batchCancelReleaseNotRecord(List<Long> ids, Long peopleId) throws Exception {
		try {
			fileDao.revokeFiles(ids);
			JecnUtil.saveJecnJournals(ids, 2, 18, peopleId, fileDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获取目录下文件
	 * 
	 * @param pId
	 * @return
	 */
	@Override
	public List<JecnFileBeanT> getDirectoryFile(Long pId) {
		String hql = "from JecnFileBeanT where perFileId=? and isDir = 1 order by sortId";
		return fileDao.listHql(hql, pId);
	}

	public List<JecnFileBeanT> getDirectory(Long pId) {
		String hql = "from JecnFileBeanT where perFileId=? and isDir = 0";
		return fileDao.listHql(hql, pId);
	}

	/**
	 * 维护支撑文件-获取自动生成的文件节点
	 * 
	 * @param relatedsMap
	 *            key:1流程；2 制度
	 */
	@Override
	public List<Long> getFileAuthByFlowAutoCreateDir(Map<Integer, Set<Long>> relatedsMap) throws Exception {
		Set<Long> flowIds = relatedsMap.get(1);
		List<Long> resultFiles = new ArrayList<Long>();
		if (flowIds != null) {
			String sql = "SELECT distinct T.FILE_ID FROM JECN_FILE_T T where  t.is_dir=0 and t.flow_id in"
					+ JecnCommonSql.getIdsSet(flowIds);
			List<Object> list = this.fileDao.listNativeSql(sql);

			for (Object obj : list) {
				resultFiles.add(Long.valueOf(obj.toString()));
			}
		}

		// 制度 支撑文件-文件节点集合
		Set<Long> ruleIds = relatedsMap.get(2);
		if (ruleIds != null) {
			String sql = "SELECT distinct T.FILE_ID FROM JECN_FILE_T T where  t.is_dir=0 and t.rule_id in"
					+ JecnCommonSql.getIdsSet(ruleIds);
			List<Object> list = this.fileDao.listNativeSql(sql);

			for (Object obj : list) {
				resultFiles.add(Long.valueOf(obj.toString()));
			}
		}

		return resultFiles;
	}

	@Override
	public List<JecnTreeBean> getJecnTreeBeanByIds(Set<Long> ids) throws Exception {
		String sql = "SELECT DISTINCT T.FILE_ID," + "                T.FILE_NAME," + "                T.PER_FILE_ID,"
				+ "                T.IS_DIR," + "                CASE"
				+ "                  WHEN JFT.FILE_ID IS NULL THEN" + "                   0" + "                  ELSE"
				+ "                   1" + "                END AS COUNT," + "                T.CREATE_TIME,"
				+ "                T.SAVETYPE," + "                T.DOC_ID," + "                CASE"
				+ "                  WHEN JF.FILE_ID IS NULL THEN" + "                   '0'"
				+ "                  ELSE" + "                   '1'" + "                END AS IS_PUB,"
				+ "                T.FLOW_ID," + "                T.SORT_ID" + "  FROM JECN_FILE_T T"
				+ "  LEFT JOIN JECN_FILE JF ON T.FILE_ID = JF.FILE_ID"
				+ "  LEFT JOIN JECN_FILE_T JFT ON T.FILE_ID = JFT.PER_FILE_ID"
				+ "                           AND JFT.DEL_STATE = 0" + " WHERE T.FILE_ID IN"
				+ JecnCommonSql.getIdsSet(ids) + "   AND T.DEL_STATE = 0";
		List<Object[]> list = fileDao.listNativeSql(sql);
		return findByListObjectsNoDel(list);
	}

	@Override
	public FileAttributeBean getFileAttributeBeanById(Long id) throws Exception {
		JecnFileBeanT jecnFileBean = this.get(id);
		FileAttributeBean fileAttributeBean = new FileAttributeBean();
		// 责任人
		if (jecnFileBean.getResPeopleId() != null && jecnFileBean.getTypeResPeople() != null) {
			// 责任人类型
			fileAttributeBean.setResPeopleType(jecnFileBean.getTypeResPeople().intValue());
			// 责任人ID
			fileAttributeBean.setResPeopleId(jecnFileBean.getResPeopleId());
			if (jecnFileBean.getTypeResPeople().intValue() == 0) {
				JecnUser jecnUser = this.personDao.get(fileAttributeBean.getResPeopleId());
				if (jecnUser != null) {
					fileAttributeBean.setRePeopleName(jecnUser.getTrueName());
				}
			} else if (jecnFileBean.getTypeResPeople().intValue() == 1) {
				String hql = "from JecnFlowOrgImage where figureId = ?";
				JecnFlowOrgImage jecnFlowOrgImage = fileDao.getObjectHql(hql, jecnFileBean.getResPeopleId());
				if (jecnFlowOrgImage != null) {
					fileAttributeBean.setRePeopleName(jecnFlowOrgImage.getFigureText());
				}
			}
		}
		// 专员
		if (jecnFileBean.getCommissionerId() != null) {
			JecnUser jecnUser = this.personDao.get(jecnFileBean.getCommissionerId());
			if (jecnUser != null) {
				fileAttributeBean.setCommissionerId(jecnFileBean.getCommissionerId());
				fileAttributeBean.setCommissionerName(jecnUser.getTrueName());
			}
		}
		return fileAttributeBean;
	}

	@Override
	public void updateFileAttributeBeanById(FileAttributeBean fileAttributeBean, Long id, Long updatePersonId)
			throws Exception {
		try {
			List<Long> listIds = new ArrayList<Long>();
			listIds.add(id);
			TreeNodeType treeNodeType = TreeNodeType.fileDir;
			String sql = "update jecn_file_t set RES_PEOPLE_ID=" + fileAttributeBean.getResPeopleId()
					+ ", TYPE_RES_PEOPLE = " + fileAttributeBean.getResPeopleType() + ",COMMISSIONER_ID = "
					+ fileAttributeBean.getCommissionerId() + " where file_ID in "
					+ JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, treeNodeType);
			fileDao.execteNative(sql);
			sql = "update jecn_file set RES_PEOPLE_ID=" + fileAttributeBean.getResPeopleId() + ", TYPE_RES_PEOPLE = "
					+ fileAttributeBean.getResPeopleType() + ",COMMISSIONER_ID = "
					+ fileAttributeBean.getCommissionerId() + " where file_ID in "
					+ JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, treeNodeType);
			fileDao.execteNative(sql);
			JecnUtil.saveJecnJournals(listIds, 2, 2, updatePersonId, baseDao, 10);
		} catch (Exception e) {
			log.error("恢复文件目录及子节点出错", e);
			throw e;
		}

	}

	@Override
	public Long getFileRelatedFlowId(Long fileId) {
		JecnFileBeanT jecnFileBeanT = fileDao.get(fileId);
		return jecnFileBeanT.getFlowId();
	}

	@Override
	public boolean isNotAbolishOrNotDelete(Long id) {
		String sql = "select count(t.file_id) from jecn_file_t t where t.file_id = ? and t.del_state = 0";
		return fileDao.countAllByParamsNativeSql(sql, id) > 0 ? true : false;
	}

	@Override
	public List<JecnDictionary> getFileTypeData() throws Exception {
		String hql = "from JecnDictionary where name='FILE_TYPE' order by code";
		return fileDao.listHql(hql);
	}

	/**
	 * 根据文件ID获取维护支撑文件的流程编号
	 * 
	 * @date 2018-3-2
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	@Override
	public String getFlowNumByFileId(Long fileId) throws Exception {
		String sql = "SELECT FST.FLOW_ID_INPUT" + "  FROM JECN_FILE_T JFT" + " INNER JOIN JECN_FLOW_STRUCTURE_T FST"
				+ "    ON JFT.FLOW_ID = FST.FLOW_ID" + " WHERE JFT.FILE_ID = " + fileId;
		return fileDao.getObjectNativeSql(sql);
	}

	@Override
	public List<Long> getFileAuthByFlowAutoCreateDir(Set<Long> flowIds) throws Exception {
		String sql = "SELECT distinct T.FILE_ID FROM JECN_FILE_T T where  t.is_dir=0 and t.flow_id in"
				+ JecnCommonSql.getIdsSet(flowIds);
		List<Object> list = this.fileDao.listNativeSql(sql);
		List<Long> listIds = new ArrayList<Long>();
		for (Object obj : list) {
			listIds.add(Long.valueOf(obj.toString()));
		}
		return listIds;
	}

	@Override
	public void createFile(String filePath, byte[] changeFileToByte) throws IOException {
		// TODO Auto-generated method stub
		File file = new File(filePath);

		FileOutputStream out = null;
		try {
			file.createNewFile();
			out = new FileOutputStream(file, false);
			out.write(changeFileToByte);
			out.flush();
		} catch (IOException ex) {
			log.error("写出异常", ex);
			throw ex;
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				log.error("关闭输出流异常", ex);
			}
		}
	}

	@Override
	public String saveFilePath(String flowHeadinfo, Date date, String suffixName) {
		String path = JecnConfigTool.getItemValue(ConfigItemPartMapMark.systemLocalFileProfix.toString());
		File file = new File(path + "eprosFileLibrary/" + flowHeadinfo + getStringbyDate(date));
		if (!file.exists()) {
			file.mkdir();
		}
		return path + "eprosFileLibrary/" + flowHeadinfo + getStringbyDate(date) + "/" + JecnCommon.getUUID()
				+ suffixName;
	}

	private String getStringbyDate(Date date) {
		return new SimpleDateFormat("yyyyMM").format(date);
	}

	@Override
	public List<JecnFileBeanT> getMergeFilesByName(String name, Long projectId) {
		try {
			String sql = "select jf.FILE_ID," + " jf.FILE_NAME," + " jf.FILE_PATH," + " jf.PEOPLE_ID,"
					+ " jf.PER_FILE_ID," + " jf.CREATE_TIME," + " jf.UPDATE_TIME," + " jf.IS_DIR," + " jf.ORG_ID,"
					+ " jf.DOC_ID," + " jf.IS_PUBLIC," + " jf.FLOW_ID," + " jf.SORT_ID," + " jf.SAVETYPE,"
					+ " jf.UPDATE_PERSON_ID," + " jf.PROJECT_ID," + " jf.VERSION_ID," + " jf.HISTORY_ID,"
					+ " jfo.org_name, " + " jf.hide," + " jf.del_state" + " from jecn_file_t jf"
					+ " left join jecn_flow_org jfo on jfo.org_id = jf.org_id and jfo.del_state=0"
					+ " where jf.is_dir=1 and jf.project_id=? and jf.file_name like ? and jf.del_state = 0"
					+ " order by jf.per_file_id,jf.sort_id,jf.file_id";
			List<Object[]> listFileBeanT = fileDao.listNativeSql(sql, 0, 30, projectId, "%" + name + "%");
			List<JecnFileBeanT> files = findByListFileBeanTObjects(listFileBeanT);
			FullPathManager manager = new FullPathManager(this.baseDao, 1);
			for (JecnFileBeanT file : files) {
				manager.add(file.getFileID());
			}
			manager.generate();
			for (JecnFileBeanT file : files) {
				file.setFileName(manager.getFullPath(file.getFileID()));
			}
			return files;
		} catch (Exception e) {
			log.error("", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<JecnFileBeanT> getFilesFullPath(List<Long> addIds) {
		String hql = "from JecnFileBeanT where fileID in " + JecnCommonSql.getIds(addIds);
		List<JecnFileBeanT> files = fileDao.listHql(hql);
		FullPathManager manager = new FullPathManager(fileDao, 1);
		Session s = this.fileDao.getSession();
		for (JecnFileBeanT file : files) {
			s.evict(file);
			manager.add(file.getFileID());
		}
		manager.generate();
		for (JecnFileBeanT file : files) {
			file.setFileName(manager.getFullPath(file.getFileID()));
		}
		return files;
	}

	@Override
	public void merge(Long mainId, List<Long> mergeIds) {
		if (mainId == null || JecnUtil.isEmpty(mergeIds)) {
			return;
		}
		merge(mainId, mergeIds, "");
		merge(mainId, mergeIds, "_t");
	}

	/**
	 * @author hyl
	 * 
	 *         将多个文件合并到一个文件中
	 */
	private void merge(Long mainId, List<Long> mergeIds, String pub) {
		String ids = JecnCommonSql.getIds(mergeIds);
		JecnFileBeanT fileBeanT = fileDao.get(mainId);
		String fileName = fileBeanT.getFileName().trim();
		rename("JECN_RULE", "FILE_ID", "RULE_NAME", ids, fileName, pub);
		renameStandard(ids, fileName, pub);

		update("JECN_TEMPLET", "FILE_ID", ids, mainId, pub);
		update("JECN_MODE_FILE", "FILE_M_ID", ids, mainId, pub);
		update("JECN_ACTIVITY_FILE", "FILE_S_ID", ids, mainId, pub);
		update("JECN_FIGURE_FILE", "FILE_ID", ids, mainId, pub);
		update("FLOW_STANDARDIZED_FILE", "FILE_ID", ids, mainId, pub);
		update("JECN_FIGURE_IN_OUT_SAMPLE", "FILE_ID", ids, mainId, pub);
		update("FILE_RELATED_STANDARD", "FILE_ID", ids, mainId, pub);
		update("FILE_RELATED_RISK", "FILE_ID", ids, mainId, pub);
		update("RULE_STANDARDIZED_FILE", "FILE_ID", ids, mainId, pub);
		update("RULE_FILE", "RULE_FILE_ID", ids, mainId, pub);
		update("JECN_RULE", "FILE_ID", ids, mainId, pub);

		updateStandard(mainId, ids);

		updatePerm(mainId, ids, pub);
		updateTask(mainId, ids);

		delSurplus("JECN_TEMPLET", "FILE_ID", "id", "mode_file_id", mainId, pub);

		delSurplus("FLOW_STANDARDIZED_FILE", "FILE_ID", "id", "flow_id", mainId, pub);
		delSurplus("RULE_STANDARDIZED_FILE", "FILE_ID", "id", "related_id", mainId, pub);
		delSurplus("FILE_RELATED_STANDARD", "FILE_ID", "id", "standard_id", mainId, pub);
		delSurplus("FILE_RELATED_RISK", "FILE_ID", "id", "risk_id", mainId, pub);
		delSurplus("RULE_FILE", "RULE_FILE_ID", "id", "rule_title_id", mainId, pub);
		delSurplus("JECN_FIGURE_FILE", "FILE_ID", "id", "figure_id", mainId, pub);

		delSurplus("JECN_ACTIVITY_FILE", "FILE_S_ID", "file_id", "figure_id", mainId, pub, 0);
		delSurplus("JECN_ACTIVITY_FILE", "FILE_S_ID", "file_id", "figure_id", mainId, pub, 1);

		// 删除多余的模板
		delSurplusModelSample("JECN_FIGURE_IN_OUT_SAMPLE", "FILE_ID", "id", "in_out_id", mainId, pub, 0);
		// 删除多余的样例
		delSurplusModelSample("JECN_FIGURE_IN_OUT_SAMPLE", "FILE_ID", "id", "in_out_id", mainId, pub, 1);

		// 删除多余的权限
		delSurplusPerm("JECN_ACCESS_PERMISSIONS", "access_type", "id", "figure_id", mainId, pub);
		delSurplusPerm("JECN_ORG_ACCESS_PERMISSIONS", "access_type", "id", "org_id", mainId, pub);
		delSurplusPerm("JECN_GROUP_PERMISSIONS", "access_type", "id", "postgroup_id", mainId, pub);
		// 删除文件
		lockFile(ids);
	}

	private void renameStandard(String ids, String fileName, String pub) {
		String sql = "update jecn_criterion_classes set criterion_class_name='" + fileName
				+ "' where stan_type=1 and related_id in " + ids;
		this.baseDao.execteNative(sql);
	}

	private void rename(String tableName, String fileColumn, String fileNameCoumn, String ids, String fileName,
			String pub) {
		String sql = "update " + tableName + pub + " set " + fileNameCoumn + " = '" + fileName + "'  where "
				+ fileColumn + " in " + ids;
		this.baseDao.execteNative(sql);
	}

	private void updateStandard(Long mainId, String ids) {
		String sql = "update jecn_criterion_classes set related_id=" + mainId + " where stan_type=1 and related_id in "
				+ ids;
		this.baseDao.execteNative(sql);
	}

	private void delSurplus(String tableName, String fileColumn, String keyColumn, String groupColumn, Long mainId,
			String pub) {
		tableName += pub;
		String sql = "delete from " + tableName + " where " + keyColumn + " in" + "       (select " + keyColumn
				+ " from " + tableName + " where " + fileColumn + " = " + mainId + ")" + "   and " + keyColumn
				+ " not in (select id from (select " + groupColumn + ", min(" + keyColumn + ")as id from " + tableName
				+ "                 where " + fileColumn + " = " + mainId + "                 group by " + groupColumn
				+ ") a)";
		this.baseDao.execteNative(sql);
	}

	private void delSurplus(String tableName, String fileColumn, String keyColumn, String groupColumn, Long mainId,
			String pub, int type) {
		tableName += pub;
		String sql = "delete from " + tableName + " where " + keyColumn + " in" + "       (select " + keyColumn
				+ " from " + tableName + " where " + fileColumn + " = " + mainId + " and file_type=" + type + ")"
				+ "   and " + keyColumn + " not in (select id from (select " + groupColumn + ", min(" + keyColumn
				+ ")as id from " + tableName + "                 where " + fileColumn + " = " + mainId
				+ "  and file_type=" + type + "                group by " + groupColumn + ") a)";
		this.baseDao.execteNative(sql);
	}

	private void delSurplusModelSample(String tableName, String fileColumn, String keyColumn, String groupColumn,
			Long mainId, String pub, int type) {
		tableName += pub;
		String sql = "delete from " + tableName + " where " + keyColumn + " in" + "       (select " + keyColumn
				+ " from " + tableName + " where " + fileColumn + " = " + mainId + " and type=" + type + ")"
				+ "   and " + keyColumn + " not in (select id from (select " + groupColumn + ", min(" + keyColumn
				+ ")as id from " + tableName + "                 where " + fileColumn + " = " + mainId + "  and type="
				+ type + "                group by " + groupColumn + ") a)";
		this.baseDao.execteNative(sql);
	}

	private void updateTask(Long mainId, String ids) {
		// 任务中
		String sql = "update jecn_task_bean_new set r_id=" + mainId + " where task_type=1 and r_id in " + ids;
		this.baseDao.execteNative(sql);
	}

	private void lockFile(String ids) {
		lockFile(ids, "");
		lockFile(ids, "_t");
	}

	private void lockFile(String ids, String pub) {
		String sql = "update jecn_file" + pub + " set del_state=1 where file_id in " + ids;
		this.baseDao.execteNative(sql);
	}

	private void delSurplusPerm(String tableName, String fileColumn, String keyColumn, String groupColumn, Long mainId,
			String pub) {
		tableName = getPremTableName(tableName, pub);
		String sql = "delete from " + tableName + " where " + keyColumn + " in" + "       (select " + keyColumn
				+ " from " + tableName + " where " + fileColumn + " = " + mainId + " and type=1 )" + "   and "
				+ keyColumn + " not in (select id from (select " + groupColumn + ", min(" + keyColumn + ")as id from "
				+ tableName + "                 where " + fileColumn + " = " + mainId + " and type=1 group by "
				+ groupColumn + ") a)";
		this.baseDao.execteNative(sql);

	}

	private void updatePerm(Long mainId, String ids, String pub) {
		updatePerm("JECN_ACCESS_PERMISSIONS", ids, mainId, pub);
		updatePerm("JECN_ORG_ACCESS_PERMISSIONS", ids, mainId, pub);
		updatePerm("JECN_GROUP_PERMISSIONS", ids, mainId, pub);
	}

	private void updatePerm(String tableName, String ids, Long id, String pub) {
		tableName = getPremTableName(tableName, pub);
		String sql = "update " + tableName + " set RELATE_ID = " + id + " where type=1 and RELATE_ID in " + ids;
		this.baseDao.execteNative(sql);
	}

	private String getPremTableName(String tableName, String pub) {
		String result = tableName + pub;
		if ("JECN_ORG_ACCESS_PERMISSIONS_T".equalsIgnoreCase(result)) {
			return "JECN_ORG_ACCESS_PERM_T";
		}
		return result;
	}

	private void update(String tableName, String idName, String ids, Long id, String pub) {
		try {
			String sql = "update " + tableName + pub + " set " + idName + "=" + id + " where " + idName + " in " + ids;
			this.baseDao.execteNative(sql);
		} catch (Exception e) {
			log.error("表：" + tableName + pub, e);
			throw new IllegalArgumentException("");
		}
	}

	@Override
	public void updateViewSort(TreeNodeType treeNodeType, Long id) throws Exception {
		if (TreeNodeType.position.equals(treeNodeType)) {
			return;
		}
		// 获得移动到的目录的view_sort
		String pViewSort = "";
		if (id != 0L && !JecnUtil.noViewSortNodeType(treeNodeType)) {
			Map<String, Object> m = baseDao.getNativeResultToMap(JecnCommon.getViewSort(id, treeNodeType));
			if (m != null) {
				pViewSort = m.get("VIEW_SORT") != null ? m.get("VIEW_SORT").toString() : null;
			}
			if (StringUtils.isBlank(pViewSort)) {
				log.error("当前的view_sort为空");
				return;
			}
		}
		Map<Long, String> idToViewSort = new HashMap<Long, String>();
		idToViewSort.put(id, pViewSort);
		updateViewSort(idToViewSort, treeNodeType, id);
	}

	private void updateViewSort(Map<Long, String> idToViewSort, TreeNodeType treeNodeType, Long pId) {
		if (JecnUtil.noViewSortNodeType(treeNodeType)) {
			return;
		}
		// 获取所有子节点;安装tpath 由大到小排序；
		List<Long> listIds = new ArrayList<Long>();
		listIds.add(pId);
		List<Object[]> lists = null;
		if (pId == 0) {
			lists = baseDao.listNativeSql(JecnCommonSql.getAllObjectsByType(treeNodeType));
		} else {
			lists = baseDao.listNativeSql(JecnCommonSql.getChildObjectsSqlmoveByType(listIds, treeNodeType));
		}
		for (Object[] obj : lists) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			String pViewSort = idToViewSort.get(Long.valueOf(obj[1].toString()));
			if (pViewSort == null) {
				continue;
			}
			Long id = Long.valueOf(obj[0].toString());
			int sortId = 0;
			if (obj[3] != null) {
				sortId = Integer.valueOf(obj[3].toString());
			}
			String viewSort = JecnUtil.concatViewSort(pViewSort, sortId);
			idToViewSort.put(id, viewSort);
			updateViewSortDB(treeNodeType, id, viewSort);
		}

	}

	private void updateViewSortDB(TreeNodeType treeNodeType, Long id, String viewSort) {
		List<String> strSqls = JecnCommonSql.getUpdateViewSortSql(id, treeNodeType, viewSort);
		for (String sql : strSqls) {
			baseDao.execteNative(sql);
		}
	}
}

/**
 * 
 * 制度目录临时bean
 * 
 */
class RoleDir {

	/** 制度名称 */
	private String roleName = null;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
