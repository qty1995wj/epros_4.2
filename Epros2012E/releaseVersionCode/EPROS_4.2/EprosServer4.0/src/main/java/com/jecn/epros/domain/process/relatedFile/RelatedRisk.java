package com.jecn.epros.domain.process.relatedFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;

/**
 * 相关风险
 *
 * @author admin
 */
public class RelatedRisk {
    private Long id;
    /**
     * 编号
     */
    private String number;
    /**
     * 名称或描述
     */
    private String name;

    public LinkResource getLink() {
        return new LinkResource(id, number, ResourceType.RISK, TreeNodeUtils.NodeType.ruleModelFile);
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
