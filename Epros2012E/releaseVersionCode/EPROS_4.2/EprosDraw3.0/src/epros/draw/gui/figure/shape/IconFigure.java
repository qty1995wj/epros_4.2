/*
 * IconFigure.java
 *图标插入框
 * Created on 2009年3月23日, 上午11:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 图标插入框
 * 
 * @author Administrator
 */
public class IconFigure extends JecnBaseFigurePanel {

	private Long fileId;
	private int x1;
	private int x2;
	private int y1;
	private int y2;
	private Image img;

	/** Creates a new instance of IconFigure */
	public IconFigure(JecnFigureData figureData) {
		super(figureData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		x1 = 0;
		y1 = 0;
		x2 = userWidth;
		y2 = userHeight;
		paintFigure(g);
	}

	// public void getImg() {
	// if (iconFilePath != null) {
	// // figureData.setText("");
	// img = Toolkit.getDefaultToolkit().getImage(iconFilePath);
	// }
	// }

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigure(Graphics2D g2d) {
		if (img != null) {
			MediaTracker t = new MediaTracker(this);
			t.addImage(img, 0);
			try {
				t.waitForAll();
			} catch (Exception ex) {
				JecnLogConstants.log_JecnAbstarctFlowElementPanel.error(
						"IconFigure类paintGeneralFigure方法：", ex);
				return;
			}
			g2d.drawImage(img, x1, y1, x2, y2, null);
		}
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public void setImg(Image img) {
		this.img = img;
	}

	public Image getImg() {
		return img;
	}
	
	/**
	 * 
	 * 把源图形属性赋值给目标图形属性
	 * 
	 * @param srcFigure
	 *            源属性
	 * @param destFigure
	 *            目标属性
	 */
	@Override
	public void setFigureAttributes(JecnBaseFigurePanel srcFigure,
			JecnBaseFigurePanel destFigure) {
		super.setFigureAttributes(srcFigure, destFigure);
		if (srcFigure == null || destFigure == null) {
			return;
		}
		IconFigure srcIconFigure = (IconFigure)srcFigure;
		IconFigure destIconFigure = (IconFigure)destFigure;
		destIconFigure.setImg(srcIconFigure.getImg());
	}

}
