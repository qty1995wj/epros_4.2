package epros.designer.gui.popedom.positiongroup;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.dataimport.AddOrDelBaseBean;
import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public class BasePosResultTable extends JTable {

	/** 岗位组对应的基准岗位集合 */
	private List<JecnBasePosBean> basePosBeanList = null;

	public List<JecnBasePosBean> getBasePosBeanList() {
		return basePosBeanList;
	}

	AddBasePosDialog basePosDialog = null;

	public AddBasePosDialog getBasePosDialog() {
		return basePosDialog;
	}

	public void setBasePosDialog(AddBasePosDialog basePosDialog) {
		this.basePosDialog = basePosDialog;
	}

	public BasePosResultTable(AddBasePosDialog basePosDialog) {
		this.basePosDialog = basePosDialog;
		this.basePosBeanList = basePosDialog.getBasePosBeanList();
		this.setModel(getTableTitle());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 添加按钮
		this.getColumnModel().getColumn(2).setCellRenderer(new EditJLabel());
		// 选中行事件 true 选中
		// this.setRowSelectionAllowed(false);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {// 双击
					// 编辑处理
					isClickTableEdit();
				}
			}

			/**
			 * 鼠标移动到表格监听
			 */
			public void mouseExited(MouseEvent e) {
				mouseMovedProcess(e);
			}
		});
		/**
		 * 添加鼠标监听
		 * 
		 */
		this.addMouseMotionListener(new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				mouseMovedProcess(e);
			}
		});
	}

	/**
	 * 数据显示
	 * 
	 * @return
	 */
	public Vector<Vector<Object>> getTableData(
			List<JecnBasePosBean> jecnPosBeanList) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		// 数据转换
		Vector<Object> v = null;
		for (JecnBasePosBean jecnBasePosBean : jecnPosBeanList) {
			v = new Vector<Object>();
			v.add(jecnBasePosBean.getBasePosNum());
			v.add(jecnBasePosBean.getBasePosName());
			v.add(new EditJLabel());
			vector.add(v);
		}
		return vector;

	}

	/**
	 * 多选
	 * 
	 * @return
	 */
	public boolean isSelectMutil() {
		return true;
	}

	/**
	 * 隐藏列
	 * 
	 * @return
	 */
	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	/**
	 * 获得Table的表头
	 * 
	 * @return
	 */
	private BasePosTableMode getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("basePosName"));
		title.add(JecnProperties.getValue("relPos"));
		return new BasePosTableMode(getTableData(this.basePosBeanList), title);
	}

	class BasePosTableMode extends DefaultTableModel {
		public BasePosTableMode(Vector<Vector<Object>> data,
				Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	/** 选中table 当前行和列 */
	protected int myRow = -10;
	protected int myColumn = -10;

	/**
	 * 鼠标进入表格监听
	 * 
	 * @param e
	 */
	protected void mouseMovedProcess(MouseEvent e) {
		int row = this.rowAtPoint(e.getPoint());
		int column = this.columnAtPoint(e.getPoint());
		if (myRow == row && column == myColumn) {// 始终在指定的单元格移动，不执行重绘
			return;
		}
		myRow = this.rowAtPoint(e.getPoint());
		myColumn = this.columnAtPoint(e.getPoint());

		this.revalidate();
		this.repaint();
	}

	/**
	 * 
	 * 渲染
	 * 
	 * @author Administrator
	 * @date： 日期：2013-11-20 时间：上午10:51:08
	 */
	class EditJLabel extends JLabel implements TableCellRenderer {
		// true：需要加色渲染
		private boolean isDef = false;

		EditJLabel() {
			this.setHorizontalAlignment(JLabel.CENTER);
			this.setVerticalAlignment(JLabel.CENTER);
			// 详情
			this.setText(JecnProperties.getValue("linkDetail"));
		}

		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			this.isDef = (myRow == row && myColumn == column);
			return this;
		}

		/**
		 * 
		 * 绘制组件
		 * 
		 */
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;

			int w = this.getWidth();
			int h = this.getHeight();
			if (isDef) {
				// 渐变
				g2d.setPaint(new GradientPaint(0, 0,
						JecnUIUtil.getTopNoColor(), 0, h / 2, JecnUIUtil
								.getButtomNoColor()));
				g2d.fillRect(0, 0, w, h);
				this.setForeground(Color.red);
			} else {
				this.setForeground(Color.black);
			}

			super.paint(g);
		}
	}

	/**
	 * 
	 * 详情双击
	 * 
	 * @return
	 */
	public void isClickTableEdit() {
		if (this.getSelectedColumn() == 2) {
			String baseNum = this.getValueAt(this.getSelectedRow(), 0)
					.toString();
			BasePosRelatedPosDialog basePosRelatedPosDialog = new BasePosRelatedPosDialog(
					baseNum);
			basePosRelatedPosDialog.setVisible(true);
		} 
//		else if (this.getSelectedColumn() == 1) {
//			String baseName = this.getValueAt(this.getSelectedRow(), 1)
//					.toString();
//			String baseNum = this.getValueAt(this.getSelectedRow(), 0)
//					.toString();
//			Vector<Vector<Object>> vector = ((DefaultTableModel) basePosDialog
//					.getBasePosResultTable().getModel()).getDataVector();
//			for (Vector<Object> vc : vector) {
//				String resultBaseNum = vc.get(0).toString();
//				if ((resultBaseNum != null && baseNum != null)
//						&& resultBaseNum.equals(baseNum)) {
//					return;
//				}
//			}
//			// 数据转换
//			Vector<Object> v = new Vector<Object>();
//			v.add(baseNum);
//			v.add(baseName);
//			v.add(new EditJLabel());
//			((DefaultTableModel) basePosDialog.getBasePosResultTable()
//					.getModel()).addRow(v);
//		}

	}

	/**
	 * 
	 * 删除关联的基准岗位
	 * 
	 * */
	public void delBasePos() {

		int[] rows = this.getSelectedRows();
		// 判断是否选中行数
		if (rows.length == 0) {
			// 请选择一行!
			JecnOptionPane.showMessageDialog(null, JecnProperties
					.getValue("chooseOneRow"));
			return;
		}
		// 删除基准岗位
		for (int i = rows.length - 1; i >= 0; i--) {
			// for (int i =0;i<rows.length; i++) {

			String basePosNum = (String) ((DefaultTableModel) this.getModel())
					.getValueAt(rows[i], 0);
			((DefaultTableModel) this.getModel()).removeRow(rows[i]);
			removeBasePosList(basePosNum);
		}
		// // 目前只有一条
		// basePosBeanList.clear();
	}

	private void removeBasePosList(String basePosNum) {
		if (DrawCommon.isNullOrEmtry(basePosNum)) {
			return;
		}
		for (JecnBasePosBean bean : basePosBeanList) {
			if (basePosNum.equals(bean.getBasePosNum())) {
				basePosBeanList.remove(bean);
				return;
			}
		}
	}

	/**
	 * 清空关联的基准岗位
	 * 
	 * */
	public void clearBasePos() {
		// 清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) this.getModel()).removeRow(index);
		}
		// 目前只有一条
		basePosBeanList.clear();
	}

	/**
	 * 点击确定更新关联的基准岗位
	 * 
	 * @throws Exception
	 * 
	 * */
	public void updateBaseRelPos(String posGroupId) throws Exception {
		AddOrDelBaseBean addOrDelBaseBean = new AddOrDelBaseBean();
		addOrDelBaseBean.setPosGroupId(posGroupId);
		addOrDelBaseBean.setAddJecnBasePosBeanList(basePosBeanList);
		ConnectionPool.getPosGroup().addOrUpdate(addOrDelBaseBean);
	}
}
