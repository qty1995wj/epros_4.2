package epros.designer.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;

public class UnzipAFile {
	private static Logger log = Logger.getLogger(UnzipAFile.class);

	/**
	 * @author yxw 2013-4-3
	 * @description:
	 * @param baseDir
	 *            输出文件夹
	 * @param zipFileName
	 * @throws Exception
	 */
	public static void UnZip(String baseDir, String zipFileName) {
		// 输出文件夹
		ZipFile zFile = null;
		try {
			zFile = new ZipFile(zipFileName);
			Enumeration en = zFile.entries();
			ZipEntry entry = null;
			while (en.hasMoreElements()) {
				// 得到其中一项ZipEntry
				entry = (ZipEntry) en.nextElement();
				// 如果是文件夹则不处理
				if (entry.isDirectory()) {
					continue;
				} else {
					// 如果是文件则写到输出目录
					copyFile(zFile, baseDir, entry);
				}
			}

		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (zFile != null) {
				try {
					zFile.close();
				} catch (IOException e) {
					log.error("UnzipAFile zFile is error", e);
				}
			}
		}

	}

	private static void copyFile(ZipFile source, String baseDir, ZipEntry entry) {
		// 以ZipEntry为参数得到一个InputStream，并写到OutputStream中
		// 是否需要创建目录
		mkdirs(baseDir, entry.getName());
		// 建立输出流
		OutputStream os = null;
		InputStream is = null;
		try {
			os = new BufferedOutputStream(new FileOutputStream(new File(
					baseDir, entry.getName())));
			// 取得对应ZipEntry的输入流
			is = new BufferedInputStream(source.getInputStream(entry));
			int readLen = 0;
			byte[] buf = new byte[1024];
			// 复制文件
			// System.out.println("Extracting: " + entry.getName() + "\t"
			// + entry.getSize() + "\t" + entry.getCompressedSize());
			while ((readLen = is.read(buf, 0, 1024)) != -1) {
				os.write(buf, 0, readLen);
			}
			os.flush();
		} catch (Exception e) {
			log.error("", e);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				log.error("UnzipAFile InputStream is error", e);
			}
			try {
				if (os != null) {
					os.close();
				}
			} catch (IOException e) {
				log.error("UnzipAFile OutputStream is error。", e);
			}

		}

	}

	/**
	 * 给定根目录，返回一个相对路径所对应的实际文件名.
	 * 
	 * @param baseDir
	 *            指定根目录
	 * @param absFileName
	 *            相对路径名，来自于ZipEntry中的name
	 * @return java.io.File 实际的文件
	 */
	private static void mkdirs(String baseDir, String relativeFileName) {
		String[] dirs = relativeFileName.split("/");
		File ret = new File(baseDir);
		if (dirs.length > 1) {
			for (int i = 0; i < dirs.length - 1; i++) {
				ret = new File(ret, dirs[i]);
			}
		}
		if (!ret.exists()) {
			ret.mkdirs();
		}
	}
}
