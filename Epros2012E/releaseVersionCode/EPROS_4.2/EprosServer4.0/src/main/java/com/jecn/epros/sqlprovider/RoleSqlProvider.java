package com.jecn.epros.sqlprovider;

import com.jecn.epros.sqlprovider.server3.SqlCommon;

public class RoleSqlProvider {


    public String getSecondAdminRoleId(Long peopleId) {

        String sql = "SELECT DISTINCT RI.Role_Id" +
                "  FROM JECN_ROLE_INFO RI" +
                " INNER JOIN JECN_USER_ROLE UR" +
                "    ON RI.ROLE_ID = UR.ROLE_ID" +
                "   AND UR.PEOPLE_ID = " + peopleId +
                " WHERE RI.FILTER_ID = '" + SqlCommon.getStrSecondAdmin() + "'";
        return sql;
    }


}
