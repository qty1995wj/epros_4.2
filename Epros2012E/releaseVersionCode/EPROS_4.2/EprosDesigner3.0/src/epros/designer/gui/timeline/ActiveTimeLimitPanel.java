package epros.designer.gui.timeline;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 流程明细-办理时限面板
 * 
 * @author ZXH(修改)
 * 
 */
public class ActiveTimeLimitPanel extends JecnPanel implements CaretListener {
	/** 数据 */
	private JecnActiveData activeData = null;

	/** 目标值标签 */
	private JLabel targetLabel = null;
	/** 目标值输入框 */
	private JTextField targetField = null;
	/** 目标值提示信息框 */
	private JecnUserInfoTextArea targetInfoTextArea = null;

	/** 现状值标签 */
	private JLabel statusLabel = null;
	/** 现状值输入框 */
	private JTextField statusField = null;
	/** 现状值提示信息框 */
	private JecnUserInfoTextArea statusTextArea = null;

	/** 说明标签 */
	protected JLabel explainLabel = null;
	/** 说明输入框 */
	protected JTextArea explainTextArea = null;
	/** 说明提示信息框 */
	protected JecnUserInfoTextArea explainInfoTextArea = null;
	/** 说明输入框容器 */
	protected JScrollPane explainScrollPane = null;

	private JecnPanel mainPanel;

	public ActiveTimeLimitPanel(JecnActiveData activeData) {
		super();
		this.activeData = activeData;
		// 初始化
		initComs();
		initListener();
		initLayOut();
		initData();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.activeData = jecnActiveData;
		initData();
	}

	private void initListener() {
		// 目标值
		targetField.addCaretListener(this);
		statusField.addCaretListener(this);
		// 说明
		explainTextArea.addCaretListener(this);
	}

	/**
	 * 
	 * 输入框的插入字符事件
	 * 
	 */
	@Override
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	private void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == targetField) {// 编号输入框
			// 数字格式是否正确
			checkTextFieldNumberFormat(targetInfoTextArea, targetField);
		} else if (e.getSource() == statusField) {// 名称输入框
			// 名称长度超过122个字符或61个汉字
			checkTextFieldNumberFormat(statusTextArea, statusField);
		} else if (e.getSource() == explainTextArea) {// 名称输入框
			// 名称长度超过1200个字符或600个汉字
			checkTextAreaLength(explainInfoTextArea, explainTextArea);
		}
	}

	private void initComs() {
		explainTextArea = new JTextArea();
		if (JecnDesignerCommon.isShowEleAttribute()) {
			mainPanel = new JecnPanel();
			explainTextArea.setRows(2);
		} else {
			mainPanel = new JecnPanel(480, 404);
			Dimension size = new Dimension(480, 404);
			Dimension preferredSize = new Dimension(600, 500);
			this.setSize(preferredSize);
			this.setMinimumSize(size);
		}
		// 目标值
		targetLabel = new JLabel(JecnProperties.getValue("targetValue") + "(" + JecnConfigTool.getActiveTimeLineUtil()
				+ ")：");
		targetField = new JTextField();
		targetInfoTextArea = new JecnUserInfoTextArea();
		statusLabel = new JLabel(JecnProperties.getValue("situationValue") + "("
				+ JecnConfigTool.getActiveTimeLineUtil() + ")：");
		statusField = new JTextField();
		statusTextArea = new JecnUserInfoTextArea();

		explainLabel = new JLabel(JecnProperties.getValue("actBaseDes"));

		explainInfoTextArea = new JecnUserInfoTextArea();
		// 说明输入框容器
		explainScrollPane = new JScrollPane(explainTextArea);
		explainScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		explainScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		explainScrollPane.setBorder(null);
		// 说明输入区域
		explainTextArea.setLineWrap(true);
		explainTextArea.setWrapStyleWord(true);
		explainTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("timeLimit")));
	}

	private void initLayOut() {
		int h = 7;
		if (JecnDesignerCommon.isShowEleAttribute()) {
			h = 1;
		}
		/** 标签间距 */
		Insets insetsLabel = new Insets(h, 35, h, 3);
		/** 内容间距 */
		Insets insetsContent = new Insets(h, 3, h, 5);
		// 说明
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST,
				GridBagConstraints.NONE, insetsLabel, 0, 0);
		mainPanel.add(explainLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		mainPanel.add(explainScrollPane, c);
		// 提示框
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(explainInfoTextArea, c);

		// 现状值
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		mainPanel.add(statusLabel, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		mainPanel.add(statusField, c);
		// 提示框
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(statusTextArea, c);

		// 目标值label
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		mainPanel.add(targetLabel, c);
		// 目标值输入框EAST
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		mainPanel.add(targetField, c);
		// 目标值提示框
		c = new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(targetInfoTextArea, c);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(mainPanel, c);
	}

	protected void initData() {
		this.targetField.setText(activeData.getTargetValue() != null ? activeData.getTargetValue().toString() : "");
		this.statusField.setText(activeData.getStatusValue() != null ? activeData.getStatusValue().toString() : "");
		explainTextArea.setText(activeData.getExplain());
	}

	public boolean checkTextFieldNumberFormat(JecnUserInfoTextArea infoTextArea, JTextField textField) {
		// true:校验成功；false：校验失败
		String checkString = checkNumberFormat(textField.getText().trim());
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 校验数字是否合法 只能输入数字，并且整数位最长为9位，小数位为1位。
	 * 
	 * @author hyl
	 * @param num
	 * @return
	 */
	private String checkNumberFormat(String num) {

		String reg = "\\d{1,9}(\\.\\d{1,2})?";
		Pattern pattern = Pattern.compile(reg);
		// 创建匹配给定参数与此模式的匹配器
		Matcher matcher = pattern.matcher(num);
		// 开始匹配
		if (num != null && !"".equals(num) && !matcher.matches()) {
			return JecnProperties.getValue("limitTargetInfo");
		}

		return "";
	}

	/**
	 * 校验文本长度
	 * 
	 * @param infoTextArea
	 * @param textArea
	 * @return
	 */
	public boolean checkTextAreaLength(JecnUserInfoTextArea infoTextArea, JTextArea textArea) {
		String checkString = JecnUserCheckUtil.checkNoteLength(textArea.getText().trim());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	public void isOkBut() {
		activeData.setTargetValue(getStringValueToBigDecimal(targetField.getText()));
		activeData.setStatusValue(getStringValueToBigDecimal(statusField.getText()));
		activeData.setExplain(explainTextArea.getText());
	}

	public boolean isUpdate() {
		if (activeData.getTargetValue() == null) {
			activeData.setTargetValue(null);
		}
		if (activeData.getStatusValue() == null) {
			activeData.setStatusValue(null);
		}
		String target = targetField.getText();
		if (DrawCommon.isNullOrEmtryTrim(target)) {
			target = null;
		}

		String status = statusField.getText();
		if (DrawCommon.isNullOrEmtryTrim(status)) {
			status = null;
		}
		if (!DrawCommon
				.checkBigDecimalSame(activeData.getTargetValue(), target != null ? new BigDecimal(target) : null)) {
			return true;
		}

		if (!DrawCommon
				.checkBigDecimalSame(activeData.getStatusValue(), status != null ? new BigDecimal(status) : null)) {
			return true;
		}

		if (!DrawCommon.checkStringSame(activeData.getExplain(), explainTextArea.getText())) {
			return true;
		}
		return false;
	}

	public boolean okBtnCheck() {
		// 编号
		if (!checkTextAreaLength(explainInfoTextArea, explainTextArea)) {
			return false;
		}
		if (!checkTextFieldNumberFormat(targetInfoTextArea, targetField)) {
			return false;
		}
		if (!checkTextFieldNumberFormat(statusTextArea, statusField)) {
			return false;
		}
		return true;
	}

	private BigDecimal getStringValueToBigDecimal(String value) {
		return DrawCommon.isNullOrEmtryTrim(value) ? null : new BigDecimal(value);
	}
}
