package epros.draw.gui.top.toolbar.element;

import java.awt.GridBagConstraints;
import java.awt.event.ItemEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 
 * 流程元素面板的阴影面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementShadowPanel extends JecnAbstarctFlowElementPanel {
	/** 阴影按钮 */
	private JRadioButton shadowButton = null;
	/** 无阴影按钮 */
	private JRadioButton noShadowButton = null;
	/** 阴影颜色 */
	private JecnElementButtonPanel shadowColorPanel = null;

	public JecnFlowElementShadowPanel(JecnFlowElementPanel flowElementPanel) {
		super(flowElementPanel);
		init();
	}

	protected void initComponents() {

		ButtonGroup group = new ButtonGroup();

		// 无阴影按钮
		noShadowButton = new JRadioButton(flowElementPanel.getResourceManager().getValue("noShadow"));
		// 有阴影按钮
		shadowButton = new JRadioButton(flowElementPanel.getResourceManager().getValue("isShadow"));
		// 阴影颜色
		shadowColorPanel = new JecnElementButtonPanel(flowElementPanel, flowElementPanel.getSizeColor(),
				flowElementPanel.getBTN_SHADOW_COLOR());

		// 边框标题
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), flowElementPanel
				.getResourceManager().getValue(flowElementPanel.getBTN_SHADOW_COLOR())));

		// 透明
		// 阴影按钮
		shadowButton.setOpaque(false);
		// 无阴影按钮
		noShadowButton.setOpaque(false);

		group.add(noShadowButton);
		group.add(shadowButton);

		// 事件
		noShadowButton.addItemListener(flowElementPanel);
		shadowButton.addItemListener(flowElementPanel);

	}

	/**
	 * 
	 * 初始化阴影
	 * 
	 */
	protected void initLayout() {

		// 无阴影按钮
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(noShadowButton, c);

		// 空闲区域
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(new JLabel(), c);

		// 有阴影按钮
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(shadowButton, c);

		// 阴影颜色
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsContent(), 0, 0);
		this.add(shadowColorPanel, c);
	}

	@Override
	public void initData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null) {
			return;
		}
		if (!flowElementData.isShadowsFlag()) {// 无阴影
			this.getNoShadowButton().setSelected(true);
		} else {// 有阴影
			this.getShadowButton().setSelected(true);
			this.getShadowColorPanel().setBackground(flowElementData.getShadowColor());
		}
	}

	/**
	 * 
	 * 单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() != noShadowButton && e.getSource() != shadowButton) {// 选中时无阴影、阴影
			return;
		}
		itemStateChangedProcess(e);
	}

	private void itemStateChangedProcess(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			if (e.getSource() == noShadowButton) {// 选中时无阴影
				// 隐藏控件
				this.shadowColorPanel.setEnabled(false);

				if (flowElementPanel.getSelectedBasePanel() instanceof JecnBaseFigurePanel) {
					JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) flowElementPanel.getSelectedBasePanel();
					figurePanel.removeRelatedPanel();
				}
				// 数据层：阴影类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData().setShadowsFlag(false);
			} else if (e.getSource() == shadowButton) {// 有阴影
				// 显示控件
				this.shadowColorPanel.setEnabled(true);

				// 数据层：阴影类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData().setShadowsFlag(true);
				if (flowElementPanel.getSelectedBasePanel() instanceof JecnBaseFigurePanel) {
					JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) flowElementPanel.getSelectedBasePanel();
					figurePanel.addShadowPanel();
				}
			}
		}
	}

	public JRadioButton getShadowButton() {
		return shadowButton;
	}

	public JRadioButton getNoShadowButton() {
		return noShadowButton;
	}

	public JecnElementButtonPanel getShadowColorPanel() {
		return shadowColorPanel;
	}

}
