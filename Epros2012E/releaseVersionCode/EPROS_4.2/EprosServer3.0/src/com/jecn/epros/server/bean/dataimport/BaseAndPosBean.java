package com.jecn.epros.server.bean.dataimport;

import java.util.ArrayList;
import java.util.List;

/**
 * 基准岗位集合和基准岗位对应关系Bean
 * 
 * @author cheshaowei
 * 
 * 
 * */
public class BaseAndPosBean {

	private List<JecnBasePosBean> basePosBeanList = null;
	/** 已存在的重复基准岗位编号 */
	private List<String> existsBasePosNum = null;

	/** 基准岗位中对应的实际岗位在人员岗位对应关系中不存在的集合 */
	private List<BasePosBean> noExistsPosNum = null;

	public BaseAndPosBean(List<JecnBasePosBean> basePosBeanList,
			List<String> existsBasePosNum, List<BasePosBean> noExistsPosNum) {
		this.basePosBeanList = basePosBeanList;
		this.existsBasePosNum = existsBasePosNum;
		this.noExistsPosNum = noExistsPosNum;
	}

	public List<JecnBasePosBean> getBasePosBeanList() {
		if (basePosBeanList == null) {
			basePosBeanList = new ArrayList<JecnBasePosBean>();
		}
		return basePosBeanList;
	}

	public void setBasePosBeanList(List<JecnBasePosBean> basePosBeanList) {
		this.basePosBeanList = basePosBeanList;
	}

	public List<String> getExistsBasePosNum() {
		return existsBasePosNum;
	}

	public void setExistsBasePosNum(List<String> existsBasePosNum) {
		this.existsBasePosNum = existsBasePosNum;
	}

	public List<BasePosBean> getNoExistsPosNum() {
		return noExistsPosNum;
	}

	public void setNoExistsPosNum(List<BasePosBean> noExistsPosNum) {
		this.noExistsPosNum = noExistsPosNum;
	}
}
