package epros.designer.gui.autoNum;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TreeExpansionEvent;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

/**
 * 自动编号-过程代码tree结构
 * 
 * @author Administrator
 * 
 */
public class AutoNumJecnTree extends JecnHighEfficiencyTree {
	protected static Logger log = Logger.getLogger(AutoNumJecnTree.class);

	/** 树代码类型: 0 流程，1 文件 ， 2 制度 */
	protected int treeType;

	public AutoNumJecnTree(int treeType) {
		this.treeType = treeType;
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new JecnTreeListener() {

			@Override
			public void treeExpanded(TreeExpansionEvent event) {
				if (!AutoNumJecnTree.this.isAllowExpand()) {
					return;
				}
				JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
				if (node.getChildCount() == 0) {
					try {
						List<JecnTreeBean> list = ConnectionPool.getAutoCodeAction().getChildProcessCodes(
								node.getJecnTreeBean().getId(), treeType);
						JecnTreeCommon.expansionTreeNode(AutoNumJecnTree.this, list, node);
					} catch (Exception e) {
						log.error("getTreeExpansionListener is error", e);
					}
					JecnTreeCommon.selectNode(AutoNumJecnTree.this, node);
				}
			}

			@Override
			public void treeCollapsed(TreeExpansionEvent event) {
				if (!AutoNumJecnTree.this.isAllowExpand()) {
					return;
				}
				JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
				JecnTreeCommon.selectNode(AutoNumJecnTree.this, node);
			}
		};
	}

	@Override
	public JecnTreeModel getTreeModel() {
		return null;
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = this.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean().getTreeNodeType();
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean().getTreeNodeType())) {
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}

				if (listSelectNode.size() > 0) {
					AutoTreeMenu menu = new AutoTreeMenu(this, treeType);
					if (listSelectNode.size() == 1) {
						this.setSelectionPath(listTreePaths.get(0));
						menu.setListNode(listSelectNode);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
						return;
					}
					// 去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(listSelectNode, this);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							this.setSelectionPath(treePaths[0]);
						} else {
							this.setSelectionPaths(treePaths);
						}
						menu.setListNode(listRemoveChild);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}
			}
		}
	}

	/**
	 * 获取选中节点
	 * 
	 * @return
	 */
	public JecnTreeNode getSelectNode() {
		/** 选中节点路径集合 */
		TreePath[] treePathArr = this.getSelectionPaths();
		if (treePathArr != null && treePathArr.length > 0) {
			/** 以最后一个选中为例 */
			TreePath treePathClick = treePathArr[treePathArr.length - 1];
			return (JecnTreeNode) treePathClick.getLastPathComponent();
		}
		return null;
	}

	public void allowExpandByTreeBean(JecnTreeBean jecnTreeBean) {
		try {
			JecnTreeCommon.searchNode(jecnTreeBean.getId(), jecnTreeBean.getTreeNodeType(), this, treeType);
		} catch (Exception e) {
			log.error("getSelectNode is error", e);
		}
		this.setAllowExpand(true);
	}
}
