
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserByLoginNameResult" type="{http://tempuri.org/}User" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserByLoginNameResult"
})
@XmlRootElement(name = "GetUserByLoginNameResponse")
public class GetUserByLoginNameResponse {

    @XmlElement(name = "GetUserByLoginNameResult")
    protected User getUserByLoginNameResult;

    /**
     * 获取getUserByLoginNameResult属性的值。
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getGetUserByLoginNameResult() {
        return getUserByLoginNameResult;
    }

    /**
     * 设置getUserByLoginNameResult属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setGetUserByLoginNameResult(User value) {
        this.getUserByLoginNameResult = value;
    }

}
