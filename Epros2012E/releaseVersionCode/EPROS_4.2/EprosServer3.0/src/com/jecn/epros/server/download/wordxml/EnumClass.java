package com.jecn.epros.server.download.wordxml;

public class EnumClass {
	/** 流程驱动规则 **/
	public enum FLOW_DERVER_RULE {
		TIME_DRIVEN("事件驱动", 0), EVENT_DRIVEN("时间驱动", 1), TIME_EVENT_DRIVEN(
				"时间驱动/事件驱动", 2);
		// 成员变量
		private String name;
		private int type;

		// 构造方法
		private FLOW_DERVER_RULE(String name, int type) {
			this.name = name;
			this.type = type;
		}

		public String getName() {
			return name;
		}

		public int getType() {
			return type;
		}

		public static String getName(int type) {
			for (FLOW_DERVER_RULE c : FLOW_DERVER_RULE.values()) {
				if (c.getType() == type) {
					return c.name;
				}
			}
			return null;
		}

		public static int getType(String name) {
			for (FLOW_DERVER_RULE c : FLOW_DERVER_RULE.values()) {
				if (c.getName().equals(name)) {
					return c.getType();
				}
			}
			return 0;
		}
	}
}
