package epros.draw.gui.box;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.system.JecnSystemData;

/**
 * 
 * 流程元素面板中滚动面板监听事件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnScrollPaneComponentAdapter extends ComponentAdapter {
	/** 按钮间距 */
	public final int BUTTON_SPACE = 5;

	public JecnScrollPaneComponentAdapter() {
		super();
	}

	/**
	 * Invoked when the component's size changes.
	 */
	public void componentResized(ComponentEvent e) {
		componentActionPerformed(e);
	}

	private void componentActionPerformed(ComponentEvent e) {
		if (e.getSource() instanceof JecnToolBoxAbstractScrollPane) {// 流程元素面板
			// 滚动面板
			JecnToolBoxAbstractScrollPane toolBoxScrollPane = (JecnToolBoxAbstractScrollPane) e
					.getSource();
			setJecnToolBoxAbstractScrollPaneSize(toolBoxScrollPane);
		} else if (e.getSource() instanceof JecnTemplateScrollPane) {
			JecnTemplateScrollPane templateScrollPane = (JecnTemplateScrollPane) e
					.getSource();
			setTemplateScrollPaneSize(templateScrollPane);

		}
	}

	/**
	 * 设置JecnToolBoxAbstractScrollPane大小
	 * 
	 * @param toolBoxScrollPane
	 */
	public void setJecnToolBoxAbstractScrollPaneSize(
			JecnToolBoxAbstractScrollPane toolBoxScrollPane) {
		// 按钮个数
		int btnCount = toolBoxScrollPane.getToolBoxBean().getCount();
		if (btnCount == 0) {
			return;
		}
		// 流程元素面板
		JecnToolBoxFlowElemContainPanel flowElemContainPanel = toolBoxScrollPane
				.getToolBoxMainPanel().getFlowElemContainPanel();
		if (flowElemContainPanel != null) {
			// 按钮大小
			Dimension btnSize = flowElemContainPanel.getButtonShowTypeSize();
			// 滚动面板的大小
			Dimension scrollSize = toolBoxScrollPane.getViewport().getSize();

			// 设置面板最小大小
			toolBoxScrollPane.setMinimumSize(btnSize);

			// 当前按钮面板大小
			Dimension curPanelSize = getScrollSize(scrollSize.width, btnSize,
					btnCount);
			toolBoxScrollPane.getFlowPanel().setPreferredSize(curPanelSize);
			toolBoxScrollPane.getFlowPanel().setSize(curPanelSize);
		}
	}

	/**
	 * 设置JecnTemplateScrollPane 大小
	 * 
	 * @param templateScrollPane
	 */
	public void setTemplateScrollPaneSize(
			JecnTemplateScrollPane templateScrollPane) {
		// 按钮个数
		int count = 0;
		// 流程图个数
		if (templateScrollPane.getMapType() == MapType.partMap) {
			count = JecnSystemData.getPartMapCount();
		} else if (templateScrollPane.getMapType() == MapType.totalMap) { // 流程地图个数
			count = JecnSystemData.getTotalMapCount();
		}
		if (count == 0) {
			return;
		}
		// 按钮大小
		Dimension btnSize = templateScrollPane.getButtonShowTypeSize();
		// 滚动面板的大小
		Dimension scrollSize = templateScrollPane.getViewport().getSize();
		// 设置面板最小大小
		templateScrollPane.setMinimumSize(btnSize);

		// 当前按钮面板大小
		Dimension curPanelSize = getScrollSize(scrollSize.width, btnSize, count);
		// 修改panel的大小
		templateScrollPane.getTemplatePanel().setPreferredSize(curPanelSize);
		templateScrollPane.getTemplatePanel().setSize(curPanelSize);
	}

	/**
	 * 
	 * 动态计算滚动面板的大小
	 * 
	 * @param scrollWidth
	 *            滚动面板宽度
	 * @param btnSize
	 *            按钮大小
	 * @param btnsCount
	 *            按钮个数
	 * @return Dimension 滚动面板大小
	 * 
	 */
	private Dimension getScrollSize(int scrollWidth, Dimension btnSize,
			int btnsCount) {
		Dimension size = new Dimension();
		int btnWidth = btnSize.width + BUTTON_SPACE;
		int btnHeight = btnSize.height + BUTTON_SPACE;
		size.width = scrollWidth;
		int tmpRowCount = (scrollWidth - BUTTON_SPACE) / btnWidth;
		if (tmpRowCount == 0) {
			tmpRowCount = 1;
		}
		int tmpRowNum = btnsCount / tmpRowCount;
		int tmp = btnsCount % tmpRowCount;
		if (tmp > 0) {
			size.height = (tmpRowNum + 1) * btnHeight + BUTTON_SPACE;
		} else {
			size.height = tmpRowNum * btnHeight + BUTTON_SPACE;
		}
		return size;
	}
}
