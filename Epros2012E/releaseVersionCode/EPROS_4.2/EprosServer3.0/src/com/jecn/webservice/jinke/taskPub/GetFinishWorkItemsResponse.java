/**
 * GetFinishWorkItemsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetFinishWorkItemsResponse  implements java.io.Serializable {
    private com.jecn.webservice.jinke.taskPub.WorkItemViewModel[] getFinishWorkItemsResult;

    public GetFinishWorkItemsResponse() {
    }

    public GetFinishWorkItemsResponse(
           com.jecn.webservice.jinke.taskPub.WorkItemViewModel[] getFinishWorkItemsResult) {
           this.getFinishWorkItemsResult = getFinishWorkItemsResult;
    }


    /**
     * Gets the getFinishWorkItemsResult value for this GetFinishWorkItemsResponse.
     * 
     * @return getFinishWorkItemsResult
     */
    public com.jecn.webservice.jinke.taskPub.WorkItemViewModel[] getGetFinishWorkItemsResult() {
        return getFinishWorkItemsResult;
    }


    /**
     * Sets the getFinishWorkItemsResult value for this GetFinishWorkItemsResponse.
     * 
     * @param getFinishWorkItemsResult
     */
    public void setGetFinishWorkItemsResult(com.jecn.webservice.jinke.taskPub.WorkItemViewModel[] getFinishWorkItemsResult) {
        this.getFinishWorkItemsResult = getFinishWorkItemsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFinishWorkItemsResponse)) return false;
        GetFinishWorkItemsResponse other = (GetFinishWorkItemsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getFinishWorkItemsResult==null && other.getGetFinishWorkItemsResult()==null) || 
             (this.getFinishWorkItemsResult!=null &&
              java.util.Arrays.equals(this.getFinishWorkItemsResult, other.getGetFinishWorkItemsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetFinishWorkItemsResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetFinishWorkItemsResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetFinishWorkItemsResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFinishWorkItemsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetFinishWorkItemsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getFinishWorkItemsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetFinishWorkItemsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "WorkItemViewModel"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "WorkItemViewModel"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
