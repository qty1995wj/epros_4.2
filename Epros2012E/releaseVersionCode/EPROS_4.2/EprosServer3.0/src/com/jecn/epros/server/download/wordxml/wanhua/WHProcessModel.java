package com.jecn.epros.server.download.wordxml.wanhua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

/**
 * 万华
 * 
 * @author jecn 关于logo图片，根据需求客户的logo是不会变的，但是公司名称会变
 * 
 */
public class WHProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距 1.5倍行距 *****/
	private final ParagraphLineRule vLine1_5 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);

	/**
	 * 默认模版
	 * 
	 * @param processDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public WHProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(16.49F);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(false);
		getDocProperty().setFlowChartScaleValue(6.5F);
		setDocStyle("", textTitleStyle());
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getTitleSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2.54F, 2.85F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(2.54F, 2.5F, 1.75F, 2.5F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getCharSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.1F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		// 页眉
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		TableBean tableBean = new TableBean();
		tableBean.setBorderBottom(0.5F);
		tableBean.setTableAlign(Align.center);
		Table table = header.createTable(tableBean, new float[] { 8F, 8F });
		TableRow row = table.createTableRow(new String[] { processDownloadBean.getFlowName(),
				"编号：" + nullToEmpty(processDownloadBean.getFlowInputNum()) }, new ParagraphBean(Align.left, Fonts.SONG,
				Constants.xiaowu, false));
		setTableParagraph(table, new int[] { 0 }, new int[] { 1 }, new ParagraphBean(Align.right, Fonts.ROMAN,
				Constants.xiaowu, false));

		// 页脚
		Footer footer = sect.createFooter(HeaderOrFooterType.odd);
		Paragraph paragraph = footer.createParagraph("", new ParagraphBean(Align.center, "Times New Roman",
				Constants.xiaowu, false));
		paragraph.appendCurPageRun();

	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
		createContent(firstSect);
	}

	private void createContent(Sect firstSect) {
		alterPage(firstSect);

	}

	/**
	 * 修改页
	 * 
	 * @param firstSect
	 */
	private void alterPage(Sect sect) {
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "仿宋", Constants.xiaosi, false);
		paragraphBean.setSpace(0F, 0F, vLine1_5);
		sect.createParagraph("签发或修订记录", paragraphBean);

		TableBean tableBean = new TableBean();
		tableBean.setTableAlign(Align.center);
		tableBean.setBorder(0.5F);
		tableBean.setCellMarginBean(new CellMarginBean(0F, 0F, 0F, 0F));
		Table historyTable = sect.createTable(tableBean, new float[] { 1.22F, 3.5F, 1.53F, 1.53F, 1.53F, 1.53F, 1.53F,
				1.53F, 2.5F });
		historyTable.createTableRow(new String[] { "版本", "签发或修订说明", "Owner", "编写人", "审核人", "会签", "审批人", "发布日期" },
				new ParagraphBean(Align.center, "仿宋", Constants.xiaosi, false));

		List<String[]> tableData = new ArrayList<String[]>();
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (historys.size() == 0) {
			tableData.add(new String[] { "", "", "", "", "", "", "", "" });
		} else {
			Map<Long, String> historyToDuty = getHistoryToDuty();
			for (JecnTaskHistoryNew history : historys) {
				tableData.add(new String[] { history.getVersionId(), history.getModifyExplain(),
						nullToEmpty(historyToDuty.get(history.getId())), history.getDraftPerson(),
						getPeopleByState(history, 1), getPeopleByState(history, 3), getPeopleByState(history, 4),
						dateToStr(history.getPublishDate(), "yyyy.MM.dd") });
			}
		}
		historyTable.createTableRow(tableData, new ParagraphBean(Align.left, "仿宋", Constants.xiaosi, false));
		historyTable.setCellValignAndHeight(Valign.center, 0.7F);
		sect.breakPage();
	}

	private Map<Long, String> getHistoryToDuty() {
		Map<Long, String> m = new HashMap<Long, String>();
		String sql = "select h.history_id, ju.true_name" + "  from jecn_flow_basic_info_h h"
				+ " inner join jecn_user ju" + "    on h.res_people_id = ju.people_id" + "  where h.flow_id=?"
				+ " union " + "select b.history_id, ju.true_name" + "  from jecn_flow_basic_info h"
				+ " inner join jecn_flow_structure b" + "    on h.flow_id = b.flow_id" + " inner join jecn_user ju"
				+ "    on h.res_people_id = ju.people_id" + "  where h.flow_id=?";
		List<Object[]> objs = processDownloadBean.getBaseDao().listNativeSql(sql, processDownloadBean.getFlowId(),
				processDownloadBean.getFlowId());
		for (Object[] obj : objs) {
			m.put(Long.valueOf(obj[0].toString()), nullToEmpty(obj[1]));
		}
		return m;
	}

	private String getPeopleByState(JecnTaskHistoryNew history, int state) {
		StringBuilder b = new StringBuilder();
		List<JecnTaskHistoryFollow> approvePeoples = history.getListJecnTaskHistoryFollow();
		if (approvePeoples != null) {
			for (JecnTaskHistoryFollow approve : approvePeoples) {
				if (approve.getStageMark() == state) {
					if (StringUtils.isBlank(approve.getName())) {
						continue;
					}
					b.append("/");
					b.append(nullToEmpty(approve.getName()));
				}
			}
		}
		String result = b.toString().replaceFirst("/", "");
		return result;
	}

	@SuppressWarnings("unchecked")
	private String owner(JecnTaskHistoryNew history, String pub) {
		String condition = "h.flow_id=" + processDownloadBean.getFlowId();
		if ("_H".equalsIgnoreCase(pub)) {
			condition = "h.history_id=" + history.getId();
		}
		String sql = "select ju.true_name from jecn_flow_basic_info" + pub + " h"
				+ "  inner join jecn_user ju on h.res_people_id=ju.people_id" + "  where " + condition;
		IBaseDao baseDao = processDownloadBean.getBaseDao();
		List<String> results = baseDao.listObjectNativeSql(sql, "true_name", Hibernate.STRING);
		return results == null || results.size() == 0 ? "" : results.get(0);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.0F, 1.76F, 1.5F, 1.76F, 1.1F, 1.75F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getTitleSectBean());
		titleSect.createParagraph(super.getLogoImage(5.68F, 1.5F));
		titleSect.createMutilEmptyParagraph(8);
		titleSect.createParagraph(processDownloadBean.getFlowName(), new ParagraphBean(Align.center, Fonts.SONG,
				Constants.yihao, true));
		titleSect.createMutilEmptyParagraph(14);
		titleSect.createParagraph(nullToEmpty(processDownloadBean.getFlowInputNum()), new ParagraphBean(Align.center,
				Fonts.ROMAN, Constants.sanhao, true));
		titleSect.createMutilEmptyParagraph(6);
		titleSect.createParagraph(processDownloadBean.getCompanyName(), new ParagraphBean(Align.center, Fonts.SONG,
				Constants.sanhao, true));

	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, new FontBean("仿宋", Constants.xiaosi, false));
		tblContentStyle.setSpace(0.5F, 0.2F, vLine1); // 设置段落行间距
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableAlign(Align.center);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, new FontBean("仿宋", Constants.xiaosi, true));
		tblTitileStyle.setSpace(0.5F, 0.2F, vLine1); // 设置段落行间距
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, new FontBean("仿宋", Constants.xiaosi, false));
		textContentStyle.setInd(0F, 0, 0, 0.9F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(20F, LineRuleType.EXACT));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean("宋体 (中文正文)", Constants.sihao, true));
		textTitleStyle.setInd(0, 0, 0.95F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(0.5F, 0.2F, vLine1_5); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
