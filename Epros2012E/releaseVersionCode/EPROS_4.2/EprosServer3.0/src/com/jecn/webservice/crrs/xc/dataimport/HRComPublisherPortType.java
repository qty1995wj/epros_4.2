/**
 * HRComPublisherPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crrs.xc.dataimport;

public interface HRComPublisherPortType extends java.rmi.Remote {
    public com.jecn.webservice.crrs.xc.dataimport.SummaryPackage getOrganization_viewSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crrs.xc.dataimport.SummaryPackage getIam_empbaseinfoSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crrs.xc.dataimport.SummaryPackage getPosition_viewSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crrs.xc.dataimport.SummaryPackage getParttimejob_vSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crrs.xc.dataimport.Organization_viewDataPackage queryOrganization_viewData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crrs.xc.dataimport.Iam_empbaseinfoDataPackage queryIam_empbaseinfoData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crrs.xc.dataimport.Position_viewDataPackage queryPosition_viewData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crrs.xc.dataimport.Parttimejob_vDataPackage queryParttimejob_vData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
}
