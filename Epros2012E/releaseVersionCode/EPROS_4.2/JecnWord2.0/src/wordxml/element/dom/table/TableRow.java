package wordxml.element.dom.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wordxml.constant.Constants.Valign;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.table.CellBean;
import wordxml.element.dom.IBodyElement;

/**
 * 表格行数据
 */
public class TableRow implements IBodyElement {
	/** 所属表格 */
	private Table table;
	/** 行内所有列 */
	private List<TableCell> cells = new ArrayList<TableCell>();
	/**** 行高 单位 厘米 *****/
	private float height = 0.6F;
	/***** 指定是否是标题行 ***/
	private boolean header = false;

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public boolean isHeader() {
		return header;
	}

	public void setHeader(boolean header) {
		this.header = header;
	}

	/**
	 * 创建列
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:23:13
	 * @return
	 */
	public TableCell createTableCell() {
		TableCell tc = new TableCell();
		tc.setTableRow(this);
		// 设置列宽
		tc.setWidth(getCellWidth());
		this.cells.add(tc);
		return tc;
	}

	/**
	 * 创建列
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:23:13
	 * @return
	 */
	public TableCell createTableCell(int pos) {
		TableCell tc = new TableCell();
		tc.setTableRow(this);
		// 设置列宽
		tc.setWidth(getCellWidth());
		this.cells.add(pos, tc);
		return tc;
	}

	/**
	 * 创建列
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:23:13
	 * @return
	 */
	public TableCell createTableCell(Valign vAlign) {
		TableCell tc = createTableCell();
		tc.setvAlign(vAlign);
		return tc;
	}

	/**
	 * 创建列 带样式
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:23:13
	 * @return
	 */
	public TableCell createTableCell(CellBean cellBean) {
		TableCell tc = createTableCell();
		tc.setCellBean(cellBean);
		return tc;
	}

	/**
	 * 获取新列的列宽
	 * 
	 * @author weidp
	 * @date 2014-10-16 下午02:53:48
	 * @return
	 */
	private float getCellWidth() {
		int widthPos = 0;
		// 获取当前列的列宽索引（之前所有列的跨列值的和）
		for (TableCell cell : cells) {
			widthPos += cell.getColspan();
		}
		if (table.getColWidths().length > widthPos) {
			return table.getColWidths()[widthPos];
		}
		return 0;
	}

	/**
	 * 获取指定列
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:25:36
	 * @return
	 */
	public TableCell getCell(int pos) {
		return (pos < 0 || pos >= getCellCount()) ? null : cells.get(pos);
	}

	/**
	 * 获取行内的总列数
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:28:31
	 * @return
	 */
	public int getCellCount() {
		return cells.size();
	}

	/**
	 * 获取行所属列集合
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:24:37
	 * @return
	 */
	public List<TableCell> getCells() {
		return Collections.unmodifiableList(cells);
	}

	public void setCells(List<TableCell> cells) {
		this.cells = cells;
	}

	/**
	 * 获取所属表格
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:29:16
	 * @return
	 */
	public Table getTable() {
		return table;
	}

	/**
	 * 设置所属表格
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:29:30
	 * @param table
	 */
	public void setTable(Table table) {
		this.table = table;
	}

	/**
	 * 创建一行内的多个文本单元格
	 * 
	 * @param content
	 */
	public void createCells(String[] content) {
		for (String str : content) {
			createTableCell().createParagraph(str);
		}
	}

	/**
	 * 创建一行内的多个文本单元格 带样式
	 * 
	 * @param content
	 */
	public void createCells(String[] content, ParagraphBean pBean) {
		for (String str : content) {
			createTableCell().createParagraph(str, pBean);
		}
	}

	/**
	 * 创建一行内的多个文本单元格并设置Valgin
	 * 
	 * @param content
	 * @param cellBean
	 */
	public void createCells(String[] content, Valign vAlgin) {
		for (String str : content) {
			createTableCell(vAlgin).createParagraph(str);
		}
	}

	/**
	 * 创建一行内的多个文本单元格 带样式
	 * 
	 * @param content
	 * @param cellBean
	 */
	public void createCells(String[] content, Valign vAlgin, ParagraphBean pBean) {
		for (String str : content) {
			createTableCell(vAlgin).createParagraph(str, pBean);
		}
	}

	public void setValign(Valign valign) {
		
		List<TableCell> cells = this.getCells();
		if(cells==null){
			return;
		}
		
		for(TableCell cell:cells){
			cell.setvAlign(valign);
		}
	}
}
