package epros.designer.gui.popedom.role.choose;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 风险选择框获取显示数据 2013-11-11
 * 
 */
public class RoleManageHighEfficiencyRiskTree extends JecnHighEfficiencyTree {
	private Logger log = Logger.getLogger(RoleManageHighEfficiencyRiskTree.class);
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleManageRiskTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getJecnRiskAction().getRoleAuthChildsRisk(0L, JecnConstants.projectId,
					JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("RoleManageHighEfficiencyRiskTree getTreeModel is error", e);
			// 增加提示
		}

		// 根节点 风险
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.riskRoot, JecnProperties.getValue("risk"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {

	}

}
