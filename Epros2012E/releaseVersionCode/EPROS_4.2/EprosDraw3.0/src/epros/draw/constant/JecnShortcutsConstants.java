package epros.draw.constant;

/**
 * 
 * 快捷键
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShortcutsConstants {
	/** 打开流程图/流程地图/组织图 Ctrl+O */
	public final static String KEY_OPEN = "eprosDrawOpen";
	/** 保存 Ctrl+S */
	public final static String KEY_SAVE = "eprosDrawSave";
	/** 另存为 Ctrl+Shift+A */
	public final static String KEY_SAVE_AS = "eprosDrawSaveAs";
	/** 导出为图像 Ctrll+Shift+G */
	public final static String KEY_SAVE_IMAGE = "eprosDrawSaveImage";
	/** 撤销 Ctrl+Z */
	public final static String KEY_UNDO = "eprosDrawUndo";
	/** 恢复 Ctrl+Y */
	public final static String KEY_REDO = "eprosDrawRedo";
	/** 剪切 Ctrl+X */
	public final static String KEY_CUT = "eprosDrawCut";
	/** 复制 Ctrl+C */
	public final static String KEY_COPY = "eprosDrawCopy";
	/** 粘贴 Ctrl+V */
	public final static String KEY_PASTE = "eprosDrawPaste";
	/** 删除 Delete */
	public final static String KEY_DELETE = "eprosDrawDelete";
	/** 截图 Ctrl+G */
	public final static String KEY_ICON_SHOT = "eprosDrawIconshot";
	/** 全选 Ctrl+A */
	public final static String KEY_ALL_SELECT = "eprosDrawAllSelect";
	/** 放大视图 Ctrl+Shift+B */
	public final static String KEY_BIG_IMAGE = "eprosDrawBigImage";
	/** 缩小视图 Ctrl+Shift+C */
	public final static String KEY_SMALL_IMAGE = "eprosDrawSmallImage";
	
	/** 逆时针旋转 Ctrl+L */
	/** 顺时针旋转 Ctrl+R */
	/** 原始视图 Ctrl+Shift+I */
	/** 置于顶层 Alt+Shift+T */
	/** 置于底层 Alt+Shift+B */
	/** 上移一层 Alt+Shift+U */
	/** 下移一层 Alt+Shift+D */
	/** 自动编号 Alt+Shift+I */
	/** 编辑 F2 */
	/** 退出编辑 ESC */
	/** 新建流程图 Ctrl+Shift+N */
	/** 新建流程地图 Ctrl+Shift+S */
	/** 关于 Alt+Shift+A */
	/** 打印 Ctrlt+P */
	/** 关于 Alt+Shift+A */
}
