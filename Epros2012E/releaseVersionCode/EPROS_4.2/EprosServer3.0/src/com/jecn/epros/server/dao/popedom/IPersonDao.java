package com.jecn.epros.server.dao.popedom;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.IBaseDao;

public interface IPersonDao extends IBaseDao<JecnUser, Long> {

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：判断用户是否存在
	 * @param LoginName
	 * @return
	 * @throws Exception
	 */
	public boolean isUserExist(String loginName) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：通过用户名和密码获得用户对象
	 * @param LoginName
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public JecnUser getJecnUserByLoginNameAndPassword(String loginName, String passward) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：通过用户名得用户对象
	 * @param LoginName
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public JecnUser getJecnUserByLoginName(String loginName) throws Exception;

	/**
	 * @author hyl
	 * @description：通过用户id获得用户信息
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public JecnUser getJecnUserById(Long peopleId);

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除人员
	 * @param setIds
	 * @return 0正确删除 1不能删除所有管理员 至少留有一个
	 * @throws Exception
	 */
	public int deletePeoples(List<Long> listIds) throws Exception;

	/**
	 * @author 2012-12-24
	 * @description：恢复人员
	 * @param setIds
	 * @throws Exception
	 */
	public void restorationPeoples(List<Long> listIds) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除人员
	 * @param projectId
	 * @throws Exception
	 */
	public void deletePeoples(Long projectId) throws Exception;

	/**
	 * @author zhangchen Nov 2, 2012
	 * @description：获得当前设计器用户数
	 * @return
	 * @throws Exception
	 */
	public int getCurUserInt() throws Exception;

	/**
	 * @author zhangchen Nov 2, 2012
	 * @description：获得当前设计器用户数(不包括自己的用户)
	 * @return
	 * @throws Exception
	 */
	public int getCurUserInt(Long peopleId) throws Exception;

	/**
	 * 获得用户的组织和岗位信息
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getPosOrgInfo(Long peopleId) throws Exception;

	/**
	 * 根据主键IDs 获取人员信息集合
	 * 
	 * @param setPeopleIds
	 *            人员主键集合
	 * @return 人员邮件信息集合 List<Object[]> 0:内外网；1：邮件地址；2：人员主键ID
	 * @throws Exception
	 */
	public List<JecnUser> getJecnUserList(Set<Long> setPeopleIds) throws Exception;

	/**
	 * 获取所有用户
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnUser> findAllJecnUser() throws Exception;

	/**
	 * 获取人员用户总数
	 * 
	 * @return int
	 * @throws Exception
	 */
	public int userTotalCount() throws Exception;

	/**
	 * 根据任务类型获得指定收件人
	 * 
	 * @return List<Object[]>
	 * @throws Exception
	 */
	public List<JecnUser> getFixedEmail(int taskType) throws Exception;

	/**
	 * 判断人员及其岗位是否存在
	 * 
	 * @author weidp
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public boolean isPeopleAndPosAllExists(Long peopleId) throws Exception;

	/**
	 * 获得管理员人员信息集合
	 * 
	 * @return 人员邮件信息集合 List<Object[]> 0:内外网；1：邮件地址；2：人员主键ID
	 * @throws Exception
	 */
	public List<Object[]> getAdminEmail() throws Exception;

	/**
	 * 获得管理员的id
	 * 
	 * @return List<Long>
	 * @throws Exception
	 */
	public List<Object> getAdminIds() throws Exception;

	/**
	 * 根据获得固定收件人的id
	 * 
	 * @param int type 指定人员的类型
	 * @return List<Long>
	 * @throws Exception
	 */
	public List<Object> getFixedIds(int type) throws Exception;

	/**
	 * 获取所有admin用户
	 * 
	 * @return
	 */
	public List<JecnUser> getAllAdminUser() throws Exception;

	/**
	 * 角色权限获取人员选择结果集
	 * 
	 * @param name
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	List<Object[]> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;

	JecnUser getJecnUserByEmail(String email) throws Exception;
	JecnUser getJecnUserByPhone(String phone) throws Exception;

	public int findAdminUseCount();

	public int findDesignUseCount();
	
	/**
	 * 获得某个人当前到他处审批的任务记录最新ID，作为单一待办ID   同时可以用作他审批完成后，取消代办的唯一ID
	 * 
	 * @param taskId peopleId
	 * @return
	 * @throws Exception
	 */
	public Long getJecnTaskForTodoRecodeIdByTaskIdPeopleId(Long taskId,Long peopleId) throws Exception;
}
