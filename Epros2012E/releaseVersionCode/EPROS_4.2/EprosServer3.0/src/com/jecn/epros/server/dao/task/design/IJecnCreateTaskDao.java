package com.jecn.epros.server.dao.task.design;

import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 创建任务DAO接口类
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 7, 2012 时间：6:00:26 PM
 */
public interface IJecnCreateTaskDao extends IBaseDao<JecnTaskBeanNew, Long> {
	/**
	 * 获得处于上传文件任务中的文件
	 * 
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	public List<String> getUploadFileTaskFileNames(Long fileId)
			throws Exception;

	/**
	 * 是否处于任务中
	 * 
	 * @param id
	 * @param type
	 *            0为流程任务，1为文件任务，2为制度任务
	 * @return
	 * @throws Exception
	 */
	public boolean isBeInTask(Long id, int type) throws Exception;

	/**
	 * 是否可编辑任务
	 * 
	 * @param id
	 *            关联ID
	 * @param loginPeopleId
	 *            登录人peopleId
	 * 
	 * @param type
	 *            0为流程任务，1为文件任务，2为制度任务
	 * @return
	 * @throws Exception
	 */
	public boolean isExditTask(Long id, Long loginPeopleId, int type)
			throws Exception;

	/**
	 * 判断该文件是否存在
	 * 
	 * @param fileName
	 *            文件名称
	 * @param fileId
	 *            节点主键ID
	 * @return
	 * @throws Exception
	 */
	public int checkFileExists(String fileName, Long fileId) throws Exception;

}
