package epros.draw.gui.figure.shape.part;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.List;

import sun.swing.SwingUtilities2;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 返入 风险箭头》-返工
 * 
 * @author zhangxh
 * @date： 日期：Mar 23, 2012 时间：10:28:23 AM
 */
public class ReverseArrowhead extends JecnBaseFigurePanel {
	private int x1;
	private int y1;
	private int x2;
	private int x3;
	private int y3;
	private int y2;

	public ReverseArrowhead(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		if (flowElementData.getCurrentAngle() == 90.0) {// 顺时针旋转90度
			x1 = userWidth / 2;
			y1 = userHeight / 2;

			x3 = 0;
			y3 = 0;

			x2 = 0;
			y2 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 180.0) {// 顺时针旋转180度
			x1 = userWidth / 2;
			y1 = userHeight / 2;
			x3 = userWidth;
			y3 = 0;
			x2 = 0;
			y2 = 0;
		} else if (flowElementData.getCurrentAngle() == 270.0) {// 顺时针旋转270度
			x1 = userWidth / 2;
			y1 = userHeight / 2;
			x3 = userWidth;
			y3 = userHeight;
			x2 = userWidth;
			y2 = 0;
		} else if (flowElementData.getCurrentAngle() == 0.0 || flowElementData.getCurrentAngle() == 360.0) {// 默认
			x1 = userWidth / 2;
			y1 = userHeight / 2;

			x3 = 0;
			y3 = userHeight;

			x2 = userWidth;
			y2 = userHeight;
		}
		paintFigure(g);
	}

	/**
	 * 反出，线段端点坐标
	 * 
	 * @return
	 */
	public Point getLinePoint() {
		Point point = new Point();
		int x = 0, y = 0;
		if (flowElementData.getCurrentAngle() == 270.0) {
			x = this.getX();
			y = this.getY() + this.getHeight() / 2;
		} else if (flowElementData.getCurrentAngle() == 0.0 || flowElementData.getCurrentAngle() == 360.0) {
			x = this.getX() + this.getWidth() / 2;
			y = this.getY();
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x = this.getX() + this.getWidth();
			y = this.getY() + this.getHeight() / 2;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x = this.getX() + this.getWidth() / 2;
			y = this.getY() + this.getHeight();
		}
		point.setLocation(x, y);
		return point;
	}

	/**
	 * 返工符返入 设置字体位置
	 * 
	 */
	public void setFontPosition(Graphics2D g, int type) {
		// 获取字体内容
		String text = this.getFlowElementData().getFigureText();
		if (DrawCommon.isNullOrEmtryTrim(text) || g == null) {
			return;
		}
		int textHeight = g.getFontMetrics(this.getCurScaleFont()).getHeight();
		// 字体颜色
		g.setColor(flowElementData.getFontColor());

		// 字体对象Font
		g.setFont(this.getCurScaleFont());

		List<String> list = changeFontType(g, text, this.getCurScaleFont(), this.getWidth());
		// 三角图形高度
		int tempHeight = this.getHeight() / 2;
		// // 三角图形宽度
		int tempWidth = this.getWidth() / 2;
		if (flowElementData.getCurrentAngle() == 90.0) {// 顺时针旋转90度
			for (int i = 0; i < list.size(); i++) {
				SwingUtilities2.drawString(this, g, list.get(i), tempWidth / 4, this.getHeight() / 2 + textHeight / 3
						- (list.size() / 2) * textHeight + textHeight * i);
			}
		} else if (flowElementData.getCurrentAngle() == 180.0) {// 顺时针旋转180度
			for (int i = 0; i < list.size(); i++) {
				int textLength = g.getFontMetrics(this.getFont()).stringWidth(list.get(i));
				if (list.size() > 1 && i == 0) {// 垂直方向 箭头朝上
					tempHeight = tempHeight - (list.size() * textHeight) / 2;
				}
				SwingUtilities2.drawString(this, g, list.get(i), this.getWidth() / 2 - textLength / 2, tempHeight / 2
						+ (textHeight * i));
			}
		} else if (flowElementData.getCurrentAngle() == 270.0) {// 顺时针旋转270度
			for (int i = 0; i < list.size(); i++) {
				SwingUtilities2.drawString(this, g, list.get(i), this.getWidth() / 2 + tempWidth / 4, this.getHeight()
						/ 2 + textHeight / 3 - (list.size() / 2) * textHeight + textHeight * i);
			}
		} else if (flowElementData.getCurrentAngle() == 0.0 || flowElementData.getCurrentAngle() == 360.0) {// 默认
			for (int i = 0; i < list.size(); i++) {
				if (list.size() > 1 && i == 0) {// 垂直方向 箭头朝下
					tempHeight = tempHeight - (list.size() * textHeight) + textHeight;
				}
				int textLength = g.getFontMetrics(this.getFont()).stringWidth(list.get(i));
				SwingUtilities2.drawString(this, g, list.get(i), this.getWidth() / 2 - textLength / 2, this.getHeight()
						/ 2 + tempHeight * 3 / 4 + textHeight * i);
			}
		}
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {

		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x3, x2 }, new int[] { y1, y3, y2 }, 3);

		g2d.setPaint(Color.BLACK);
		if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawLine(userWidth / 2, userHeight / 2, userWidth, userHeight / 2);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawLine(userWidth / 2, userHeight / 2, userWidth / 2, userHeight);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawLine(0, userHeight / 2, userWidth / 2, userHeight / 2);
		} else if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawLine(userWidth / 2, 0, userWidth / 2, userHeight / 2);
		}
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintArrowHeadTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintArrowHeadTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintArrowHeadTop(Graphics2D g2d, int shadowCount) {
		if (flowElementData.getCurrentAngle() == 90.0) {// 水平方向
			g2d.fillPolygon(new int[] { x1 - shadowCount, x3, x2 }, new int[] { y1, y3, y2 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount, x3, x2 }, new int[] { y1, y3, y2 - shadowCount }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2 - shadowCount, userHeight / 2, userWidth, userHeight / 2);
		} else if (flowElementData.getCurrentAngle() == 180.0) {// 垂直方向
			g2d.fillPolygon(new int[] { x1, x3 - shadowCount, x2 }, new int[] { y1 - shadowCount, y3, y2 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x3 - shadowCount, x2 }, new int[] { y1 - shadowCount, y3, y2 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2, userHeight / 2 - shadowCount, userWidth / 2, userHeight);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x3 - shadowCount, x2 - shadowCount }, new int[] { y1 - shadowCount,
					y3 - shadowCount, y2 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x3 - shadowCount, x2 - shadowCount }, new int[] { y1 - shadowCount,
					y3 - shadowCount, y2 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(0, userHeight / 2 - shadowCount, userWidth / 2, userHeight / 2 - shadowCount);
		} else if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount, x3, x2 - shadowCount }, new int[] { y1, y3 - shadowCount,
					y2 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount, x3, x2 - shadowCount }, new int[] { y1, y3 - shadowCount,
					y2 - shadowCount }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2 - shadowCount, 0 + shadowCount, userWidth / 2 - shadowCount, userHeight / 2);
		}
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DArrowHeadLow(g2d, 0);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DArrowHeadLow(g2d, linevalue);
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	private void paint3DArrowHeadLow(Graphics2D g2d, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue, x3, x2 }, new int[] { y1, y3, y2 - indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x3 - indent3Dvalue, x2 }, new int[] { y1 - indent3Dvalue, y3, y2 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x3 - indent3Dvalue, x2 - indent3Dvalue }, new int[] { y1 - indent3Dvalue,
					y3 - indent3Dvalue, y2 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue, x3, x2 - indent3Dvalue }, new int[] { y1,
					y3 - indent3Dvalue, y2 - indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DArrowHeadLine(g2d, 0);
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paint3DArrowHeadLine(Graphics2D g2d, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue, x3, x2 }, new int[] { y1, y3, y2 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawPolygon(new int[] { x1, x3 - indent3Dvalue, x2 }, new int[] { y1 - indent3Dvalue, y3, y2 }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawPolygon(new int[] { x1, x3 - indent3Dvalue, x2 - indent3Dvalue }, new int[] { y1 - indent3Dvalue,
					y3 - indent3Dvalue, y2 }, 3);
		} else if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue, x3, x2 - indent3Dvalue }, new int[] { y1,
					y3 - indent3Dvalue, y2 - indent3Dvalue }, 3);
		}
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DArrowHeadLine(g2d, 4);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - 4, x3 + 4, x2 + 4 }, new int[] { y1, y3 + 4, y2 - 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2, userHeight / 2, userWidth, userHeight / 2);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x3 - 4, x2 + 4 }, new int[] { y1 - 4, y3 + 4, y2 + 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2, userHeight / 2, userWidth / 2, userHeight);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + 4, x3 - 4, x2 - 4 }, new int[] { y1, y3 - 4, y2 + 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(0, userHeight / 2, userWidth / 2, userHeight / 2);
		} else if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1, x3 + 4, x2 - 4 }, new int[] { y1 + 4, y3 - 4, y2 - 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2, 0, userWidth / 2, userHeight / 2);
		}
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - 4 - 4, x3 + 4, x2 + 4 }, new int[] { y1, y3 + 4, y2 - 4 - 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2 - 4, userHeight / 2, userWidth, userHeight / 2);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x3 - 4 - 4, x2 + 4 }, new int[] { y1 - 4 - 4, y3 + 4, y2 + 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2, userHeight / 2 - 4, userWidth / 2, userHeight);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + 4, x3 - 4 - 4, x2 - 4 - 4 }, new int[] { y1 - 4, y3 - 4 - 4, y2 + 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(0, userHeight / 2 - 4, userWidth / 2, userHeight / 2 - 4);
		} else if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - 4, x3 + 4, x2 - 4 - 4 }, new int[] { y1 + 4, y3 - 4 - 4, y2 - 4 - 4 }, 3);
			g2d.setPaint(new Color(0, 0, 0));
			g2d.drawLine(userWidth / 2 - 4, 0 + 4, userWidth / 2 - 4, userHeight / 2);
		}
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		if (flowElementData.getCurrentAngle() == 90.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1, x3 + 4, x2 + 4 }, new int[] { y1 + 4, y3 + 4, y2 }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 4, x3, x2 + 4 }, new int[] { y1, y3 + 4, y2 + 4 }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 4, x3, x2 }, new int[] { y1 + 4, y3, y2 + 4 }, 3);
		} else if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1, x3 + 4, x2 }, new int[] { y1 + 2, y3, y2 }, 3);
		}
	}

	/**
	 * 
	 * 边缘渐变 循环
	 * 
	 * @param gw
	 * @param g2d
	 * @param x
	 * @param y
	 */
	private void paintShadowsFor(int gw, Graphics2D g2d, int[] x, int[] y) {
		for (int i = gw; i >= 2; i -= 2) {
			float pct = (float) (gw - i) / (gw - 1);
			g2d.setPaint(new GradientPaint(0.0f, userHeight * 0.25f, flowElementData.getShadowColor(), 0.0f,
					userHeight, JecnUIUtil.getShadowBodyColor()));
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, pct));
			g2d.setStroke(new BasicStroke(i));
			g2d.drawPolygon(x, y, 3);
		}
	}
}
