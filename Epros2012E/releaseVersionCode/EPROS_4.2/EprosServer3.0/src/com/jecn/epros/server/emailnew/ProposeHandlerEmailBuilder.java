package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnUtil;

public class ProposeHandlerEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {
		JecnRtnlPropose rtnlPropose = (JecnRtnlPropose) getData()[0];
		// 邮件标题
		String subject = initSubject();
		String content = initContent(user, rtnlPropose);

		EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
		emailBasicInfo.setSubject(subject);
		emailBasicInfo.setContent(content);
		emailBasicInfo.addRecipients(user);
		return emailBasicInfo;
	}

	private String initContent(JecnUser user, JecnRtnlPropose rtnlPropose) {
		// 欢迎词
		StringBuffer strBuf = new StringBuffer();
		strBuf.append(JecnUtil.getValue("thisIsProposeEmail"));
		strBuf.append(EmailContentBuilder.strBr);

		// 提交时间
		strBuf.append(JecnUtil.getValue("submitTimeC"));
		strBuf.append(JecnCommon.getStringbyDate(rtnlPropose.getCreateTime()));
		strBuf.append(EmailContentBuilder.strBr);

		// 提交人
		strBuf.append(JecnUtil.getValue("proposeSendPerson"));
		strBuf.append(rtnlPropose.getCreateUpPeopoleName());
		strBuf.append(EmailContentBuilder.strBr);
		strBuf.append("合理化建议主题名称：" + rtnlPropose.getRelationName());
		strBuf.append(EmailContentBuilder.strBr);

		// 提交意见
		strBuf.append(JecnUtil.getValue("submitCommentsC"));
		strBuf.append(rtnlPropose.getProposeContent());
		strBuf.append(EmailContentBuilder.strBr);

		EmailContentBuilder builder = new EmailContentBuilder();
		builder.setContent(strBuf.toString());
		builder.setResourceUrl(getProposeAddress(user.getPeopleId(), rtnlPropose.getId()));
		return builder.buildContent();
	}

	/**
	 * 生成标题
	 * 
	 * @return
	 */
	private String initSubject() {
		return JecnUtil.getValue("rationalizationproposals") + "-" + JecnUtil.getValue("submitComments");
	}

	/**
	 * 处理链接
	 * 
	 * @param peopleId
	 *            　人员id
	 * @param rtnlId
	 *            合理化建议id
	 * @return
	 */
	private String getProposeAddress(Long peopleId, String rtnlId) {
		return "/loginMail.action?mailPeopleId=" + peopleId + "&accessType=mailOpenPropose" + "&rtnlProposeId="
				+ rtnlId;
	}
}
