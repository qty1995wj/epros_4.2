package com.jecn.epros.domain.inventory;

import java.util.List;

/**
 * 控制目标
 *
 * @author user
 */
public class RiskInventoryControlPointBean {
    //JecnControlTarget
    /**
     * 控制目标ID
     */
    private Long targetId;
    /**
     * 控制目标
     */
    private String description;
    /***活动*/
    private List<RiskInventoryRelatedActiveBean> listRiskInventoryRelatedActiveBean;

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RiskInventoryRelatedActiveBean> getListRiskInventoryRelatedActiveBean() {
        return listRiskInventoryRelatedActiveBean;
    }

    public void setListRiskInventoryRelatedActiveBean(
            List<RiskInventoryRelatedActiveBean> listRiskInventoryRelatedActiveBean) {
        this.listRiskInventoryRelatedActiveBean = listRiskInventoryRelatedActiveBean;
    }
}
