package com.jecn.svg.dataModel.epros;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.jecn.svg.dataModel.graphs.SvgTimeLine;

public class TimeLineDataHandler {

	private TimeLineDataTransfer transfer = new TimeLineDataTransfer();

	private Long startNodeId;
	private List<TimeLinePath> allTimeLinePaths;
	private Map<Long, TimeLineNode> allTimeLineNodes;

	/**
	 * 获取时间轴节点
	 * 
	 * @param sourceData
	 * 
	 * @return
	 */
	public void getTimeLineNodes(Map<Long, TimeLineBaseData> sourceData, SvgTimeLine timeLine) {
		transfer.translate(sourceData);
		startNodeId = transfer.getStartNodeId();
		if (startNodeId == 0) {
			return;
		}
		allTimeLineNodes = transfer.getAllNodes();
		allTimeLinePaths = transfer.getAllPaths();

		List<Long> maxSumValuePath = getMaxCostPath();
		List<TimeLineNode> nodes = new ArrayList<TimeLineNode>();
		double sumTargetValue = 0;
		double sumStatusValue = 0;
		int sort = 0;
		int prevSort = 0;
		for (Long id : maxSumValuePath) {
			sort++;
			TimeLineNode node = allTimeLineNodes.get(id);
			if (node.getValue() == null || node.getValue() == 0 || node.getStatusValue() == null
					|| node.getStatusValue() == 0) {
				continue;
			}
			sumTargetValue += node.getValue();
			sumStatusValue += node.getStatusValue();
			node.setValue(node.getValue());
			node.setStatusValue(node.getStatusValue());
			if (sort - prevSort > 1) {
				node.setContinuous(true);
			}
			prevSort = sort;
			nodes.add(node);
		}
		timeLine.setSumStatusValue(sumStatusValue);
		timeLine.setSumTargetValue(sumTargetValue);
		timeLine.setNodes(nodes);
	}

	/**
	 * 获取最大消耗路径
	 * 
	 * @return
	 */
	private List<Long> getMaxCostPath() {
		List<List<Long>> allPathList = getAllPath();
		// 获取每条路径的消耗
		List<Double> sumValueList = new ArrayList<Double>();
		for (List<Long> list : allPathList) {
			Double sumValue = 0d;
			for (Long id : list) {
				if (allTimeLineNodes.get(id).getStatusValue() != null) {
					sumValue += allTimeLineNodes.get(id).getStatusValue();
				}
			}
			sumValueList.add(sumValue);
		}
		// 获取最大消耗路径
		Double maxSumValue = 0d;
		if (sumValueList.isEmpty()) {
			return new ArrayList<Long>();
		}
		for (Double sumValue : sumValueList) {
			if (sumValue > maxSumValue) {
				maxSumValue = sumValue;
			}
		}
		return allPathList.get(sumValueList.indexOf(maxSumValue));
	}

	/**
	 * 获取所有路径
	 * 
	 * @return
	 */
	private List<List<Long>> getAllPath() {
		List<List<Long>> removedPathList = new ArrayList<List<Long>>();
		List<List<Long>> allPathList = new ArrayList<List<Long>>();
		findAllPath(new ArrayList<Long>(Arrays.asList(startNodeId)), removedPathList, startNodeId, allPathList);
		// 清除无效的临时路径
		allPathList.removeAll(removedPathList);
		return allPathList;
	}

	/**
	 * 递归获取所有路径
	 * 
	 * @param pathList
	 * @param removedList
	 * @param nodeId
	 * @param paths
	 */
	private void findAllPath(List<Long> pathList, List<List<Long>> removedList, long nodeId, List<List<Long>> paths) {
		for (TimeLinePath path : allTimeLinePaths) {
			// 避免闭环
			if (pathList.contains(path.getEndNodeId())) {
				continue;
			}
			if (path.getStartNodeId() == nodeId) {
				removedList.add(pathList);
				List<Long> newPathList = new ArrayList<Long>(pathList);
				newPathList.add(path.getEndNodeId());
				paths.add(newPathList);
				findAllPath(newPathList, removedList, path.getEndNodeId(), paths);
			}
		}
	}

}
