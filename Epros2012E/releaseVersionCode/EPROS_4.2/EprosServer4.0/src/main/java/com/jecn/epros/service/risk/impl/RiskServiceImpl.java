package com.jecn.epros.service.risk.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.InventoryCell;
import com.jecn.epros.domain.InventoryTable;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.file.FileDirCommonBean;
import com.jecn.epros.domain.inventory.*;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.report.InventoryBaseInfo;
import com.jecn.epros.domain.risk.*;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.mapper.InventoryMapper;
import com.jecn.epros.mapper.RiskMapper;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.TreeNodeUtils.TreeType;
import com.jecn.epros.service.risk.RiskService;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Utils;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RiskServiceImpl implements RiskService {
    @Autowired
    private RiskMapper riskMapper;
    @Autowired
    private InventoryMapper inventoryMapper;

    @Override
    public List<TreeNode> findChildRisks(Map<String, Object> map) {
        return TreeNodeUtils.setNodeType(riskMapper.findChildRisks(map), TreeType.risk);
    }

    @Override
    public RiskBaseInfoBean findBaseInfoBean(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        RiskBaseInfoBean riskBaseInfoBean = new RiskBaseInfoBean();
        Risk risk = riskMapper.getRisk(id);
        riskBaseInfoBean.setRiskName(risk.getRiskName());
        riskBaseInfoBean.setRiskCode(risk.getRiskCode());
        riskBaseInfoBean.setStandardControl(risk.getStandardControl());
        riskBaseInfoBean.setGrade(risk.getGrade());
        riskBaseInfoBean.setRiskType(risk.getRiskType());
        riskBaseInfoBean.setRiskPointGuides(getRiskPointGuides(riskMapper.findRiskGuide(id)));
        try {
            riskBaseInfoBean.setRelatedTargets(riskMapper.findRiskTargetDescriptions(id));
        } catch (Exception e) {
            throw new EprosBussinessException("RiskServiceImpl findBaseInfoBean is error id:" + id, e);
        }
        return riskBaseInfoBean;
    }


    private String getRiskPointGuides(List<RiskRelatedGuide> guides) {
        if (guides == null || guides.size() == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (RiskRelatedGuide relatedGuide : guides) {
            builder.append(relatedGuide.getName()).append("&nbsp;&nbsp;&nbsp;&nbsp;").append(relatedGuide.getDescription()).append("\n");
        }
        return builder.substring(0, builder.length() - 1);
    }

    @Override
    public RiskRelatedFileBean findRelatedFiles(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        RiskRelatedFileBean riskRelatedFileBean = new RiskRelatedFileBean();
        riskRelatedFileBean.setRelatedProcesses(riskMapper.findRelatedFlows(id));
        riskRelatedFileBean.setRelatedRules(riskMapper.findRelatedRules(id));
        return riskRelatedFileBean;
    }

    @Override
    public List<SearchRiskResultBean> searchRisk(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "risk_code");
        return riskMapper.searchRisk(map);
    }

    @Override
    public int searchRiskTotal(Map<String, Object> map) {
        return riskMapper.searchRiskTotal(map);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Inventory getInventory(Map<String, Object> paramMap) {

        Boolean isPaging = (Boolean) paramMap.get("isPaging");

        List<RiskInventoryRelatedControlPointBean> listResult = new ArrayList<>();
        if (isPaging) {
            // 查询分页的流程
            Paging paging = (Paging) paramMap.get("paging");
            // 通过view_sort分页查询流程
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "view_sort");
            List<String> paths = inventoryMapper.fetchPaths(paramMap);
            if (paths != null && paths.size() > 0) {
                // 将查询到的流程和他的所有父节点都集中处理为(1,2,3)类型
                String ids = Utils.convertPathCollectToSqlIn(paths);
                paramMap.put("ids", ids);
                // 从数据库获得清单项的详细信息并排序
                listResult = riskMapper.fetchInventory(paramMap);
            }
        } else {
            listResult.addAll(riskMapper.fetchInventoryParent(paramMap));
            // 获得清单数据,经过view_sort排序的数据
            listResult.addAll(riskMapper.fetchInventory(paramMap));
        }

        // 查询流程的最高的级别
        InventoryBaseInfo baseInfo = inventoryMapper.getInventoryBaseInfo(paramMap);
        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setMaxLevel(baseInfo.getMaxLevel());
        inventory.setTotalNum(baseInfo.getTotal());

        Map<Long, FileDirCommonBean> dirs = new HashMap<>();
        List<RiskInventoryRelatedControlPointBean> risks = new ArrayList<>();
        for (RiskInventoryRelatedControlPointBean bean : listResult) {
            if (bean.getIsDir() == 0) {
                dirs.put(bean.getId(), new FileDirCommonBean(bean.getId(), bean.getRiskName(), "", bean.gettLevel()));
            } else {
                risks.add(bean);
            }
        }

        InventoryTable data = new InventoryTable();
        // 添加标题行的数据
        data.addContentRow(ExcelUtils.getInventoryTitle(inventory.getStartLevel(), inventory.getMaxLevel(), InventoryStyleManager.getProcessTitles()));

        if (risks.size() > 0) {
            // 风险与内控知识库的关联
            List<RiskInventoryRelatedInfoBean> relatedInfo = inventoryMapper.fetchRiskRelatedInfo(paramMap);
            // 风险与制度关联
            List<RiskInventoryRelateRuleBean> relatedRule = inventoryMapper.fetchRiskRelatedRule(paramMap);
            initInventoryContent(risks, dirs, inventory, relatedInfo, relatedRule);
        }

        return inventory;

    }

    /**
     * 风险清单
     *
     * @throws Exception
     */
    private Inventory initInventoryContent(List<RiskInventoryRelatedControlPointBean> risks,
                                           Map<Long, FileDirCommonBean> dirMap, Inventory inventory,
                                           List<RiskInventoryRelatedInfoBean> relatedInfo, List<RiskInventoryRelateRuleBean> relatedRule) {

        // 将风险id作为key 关联的知识库为value
        Map<Long, List<RiskInventiryInfoBean>> riskRelatedInfoMap = new HashMap<Long, List<RiskInventiryInfoBean>>();
        if (relatedInfo != null && relatedInfo.size() > 0) {
            for (RiskInventoryRelatedInfoBean riskRelatedInfo : relatedInfo) {
                riskRelatedInfoMap.put(riskRelatedInfo.getRiskId(), riskRelatedInfo.getListRiskInventiryInfoBean());
            }
        }

        // 将风险id作为key 关联的知识库为value
        Map<Long, List<FileInventoryCommonBean>> riskRelatedRuleMap = new HashMap<Long, List<FileInventoryCommonBean>>();
        if (relatedRule != null && relatedRule.size() > 0) {
            for (RiskInventoryRelateRuleBean riskRelatedRule : relatedRule) {
                riskRelatedRuleMap.put(riskRelatedRule.getRiskId(), riskRelatedRule.getListRule());
            }
        }

        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.getRiskTitles()));
        inventory.setStyleMap(InventoryStyleManager.getRiskInventoryStyles(data.getTitileContents()));

        boolean isShowRiskType = ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.isShowRiskType);
        // 替换风险列的空格的数量
        int riskFillNum = inventory.getMaxLevel() - inventory.getStartLevel() + 1 + (isShowRiskType ? 7 : 6);
        // 替换风险和控制目标的空格的数量
        int pointFillNum = riskFillNum + 1;
        // 风险后没有控制目标替换的空格的数量
        int noPointFillNum = 11;
        // 控制目标没有活动替换的空格的数量
        int noActiveFillNum = 10;
        for (RiskInventoryRelatedControlPointBean risk : risks) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();
            data.getContents().add(content);
            fillLevel(dirMap, inventory, risk, content);
            // 风险描述 到 相关制度
            fillRiskAttribute(content, risk, riskRelatedInfoMap, riskRelatedRuleMap, isShowRiskType);

            // 风险是不是第一个
            boolean riskIsFirst = true;
            List<RiskInventoryControlPointBean> listRiskInventoryControlPointBean = risk
                    .getListRiskInventoryControlPointBean();
            // 单个部门下的岗位
            if (listRiskInventoryControlPointBean != null && listRiskInventoryControlPointBean.size() > 0) {
                for (RiskInventoryControlPointBean point : listRiskInventoryControlPointBean) {

                    if (!riskIsFirst) {// 控制点换行显示
                        content = new ArrayList<InventoryCell>();
                        data.getContents().add(content);
                        fillEmptyCell(riskFillNum, content, data);
                    }
                    // 控制目标
                    fillControlPoint(content, point);

                    boolean pointIsFirst = true;
                    List<RiskInventoryRelatedActiveBean> listRiskInventoryRelatedActiveBean = point
                            .getListRiskInventoryRelatedActiveBean();
                    // 写出活动
                    if (listRiskInventoryRelatedActiveBean != null && listRiskInventoryRelatedActiveBean.size() > 0) {
                        for (RiskInventoryRelatedActiveBean active : listRiskInventoryRelatedActiveBean) {
                            if (!pointIsFirst) {// 角色换行显示
                                content = new ArrayList<InventoryCell>();
                                data.getContents().add(content);
                                fillEmptyCell(pointFillNum, content, data);
                            }
                            // 填充控制点编号以及以后的内容
                            fillControlAttribute(content, active);

                            pointIsFirst = false;
                        }
                    } else {
                        fillEmptyCell(noActiveFillNum, content, data);
                    }

                    riskIsFirst = false;
                }
            } else {
                fillEmptyCell(noPointFillNum, content, data);
            }
        }
        return inventory;
    }

    private void fillControlPoint(final List<InventoryCell> content, RiskInventoryControlPointBean point) {
        InventoryCell cell;
        cell = new InventoryCell();
        cell.setText(point.getDescription());
        content.add(cell);
    }

    private void fillLevel(Map<Long, FileDirCommonBean> dirMap, Inventory inventory,
                           RiskInventoryRelatedControlPointBean risk, List<InventoryCell> content) {
        for (int i = inventory.getStartLevel(); i <= inventory.getMaxLevel(); i++) {
            boolean isExist = false;
            FileDirCommonBean fileDirCommonBean = null;
            for (String idStr : Common.getPaths(risk.gettPath())) {
                fileDirCommonBean = Common.getFileDirCommonBean(dirMap, idStr);
                if (fileDirCommonBean != null && i == fileDirCommonBean.getCurlevel()) {
                    isExist = true;
                    break;
                }
            }
            InventoryCell cell = new InventoryCell();
            if (isExist) {
                cell.setText(fileDirCommonBean.getName());
                content.add(cell);
            } else {
                cell.setText("");
                content.add(cell);
            }
        }
    }

    private void fillRiskAttribute(final List<InventoryCell> content, RiskInventoryRelatedControlPointBean risk,
                                   Map<Long, List<RiskInventiryInfoBean>> riskRelatedInfoMap,
                                   Map<Long, List<FileInventoryCommonBean>> riskRelatedRuleMap, boolean isShowRiskType) {
        InventoryCell cell = new InventoryCell();

        if (risk.getId() != null) {
            LinkResource link = new LinkResource(risk.getId(), risk.getRiskCode(), ResourceType.RISK);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
        } else {
            cell.setText(risk.getRiskCode());
        }
        content.add(cell);

        // 风险描述
        cell = new InventoryCell();
        cell.setText(risk.getRiskName());
        content.add(cell);

        // 风险等级
        cell = new InventoryCell();
        String riskLevel = "";
        Integer riskGrade = risk.getGrade();
        if (riskGrade != null) {
            if (riskGrade == 1) {
                riskLevel = JecnProperties.getValue("high");
            } else if (riskGrade == 2) {
                riskLevel = JecnProperties.getValue("in");
            } else if (riskGrade == 3) {
                riskLevel = JecnProperties.getValue("low");
            }
        }
        cell.setText(riskLevel);
        content.add(cell);

        if (isShowRiskType) {
            cell = new InventoryCell();
            cell.setText(risk.getRiskType());
            content.add(cell);
        }

        // 企业内部控制基本规范
        cell = new InventoryCell();
        StringBuffer buf = new StringBuffer();
        List<RiskInventiryInfoBean> relatedInfos = riskRelatedInfoMap.get(risk.getId());
        if (relatedInfos != null && relatedInfos.size() > 0) {
            for (RiskInventiryInfoBean info : relatedInfos) {
                if (info.getName() != null) {
                    buf.append("\n");
                    buf.append(info.getName());
                    if (info.getDescription() != null) {
                        buf.append("：");
                        buf.append(info.getDescription());
                    }
                }
            }
        }
        String guideStr = buf.toString();
        if (guideStr.startsWith("\n")) {
            guideStr = guideStr.replaceFirst("\n", "");
        }

        cell.setText(guideStr);
        content.add(cell);

        // 标准化控制
        cell = new InventoryCell();
        cell.setText(risk.getStandardControl());
        content.add(cell);

        // 相关制度
        cell = new InventoryCell();
        List<FileInventoryCommonBean> relatedRuleList = riskRelatedRuleMap.get(risk.getId());
        if (relatedRuleList != null && relatedRuleList.size() > 0) {
            List<LinkResource> listLinkResource = new ArrayList<LinkResource>();
            for (FileInventoryCommonBean ruleRelated : relatedRuleList) {
                listLinkResource.add(ruleRelated.getLink());
            }
            cell.setLinks(listLinkResource);
        }
        content.add(cell);

    }

    private void fillControlAttribute(List<InventoryCell> content, RiskInventoryRelatedActiveBean active) {

        // 控制编号
        InventoryCell cell = new InventoryCell();
        cell.setText(active.getControlCode());
        content.add(cell);

        // 控制活动简描述
        cell = new InventoryCell();
        cell.setText(active.getActiveShow());
        content.add(cell);

        // 角色名称
        cell = new InventoryCell();
        cell.setText(active.getRoleName());
        content.add(cell);
        // 活动名称
        cell = new InventoryCell();
//        cell.setText(active.getActiveName());
        if (active.getActiveId() != null && active.getActiveName() != null) {
            LinkResource link = new LinkResource(active.getActiveId(), active.getActiveName(), ResourceType.ACTIVITY);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
        }
        content.add(cell);

        // 流程名称
        cell = new InventoryCell();
//        cell.setText(active.getFlowName());
        if (active.getFlowId() != null && active.getFlowName() != null) {
            LinkResource link = new LinkResource(active.getFlowId(), active.getFlowName(), ResourceType.PROCESS);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
        }
        content.add(cell);

        // 责任部门
        cell = new InventoryCell();
        cell.setText(active.getOrgName());
        content.add(cell);

        // 控制方法
        cell = new InventoryCell();
        String controlMethodStr = "";
        Integer controlMethod = active.getMethod();
        if (controlMethod != null) {
            if (controlMethod == 0) {
                controlMethodStr = JecnProperties.getValue("artificial");
            } else if (controlMethod == 1) {
                controlMethodStr = JecnProperties.getValue("iT");
            } else if (controlMethod == 2) {
                controlMethodStr = JecnProperties.getValue("artificialOrIt");
            }
        }
        cell.setText(controlMethodStr);
        content.add(cell);

        // 控制类型
        cell = new InventoryCell();
        String controlTypeStr = "";
        Integer controlType = active.getType();
        if (controlType != null) {
            if (controlType == 0) {
                controlTypeStr = JecnProperties.getValue("preventive");
            } else if (controlType == 1) {
                controlTypeStr = JecnProperties.getValue("discovery");
            }
        }
        cell.setText(controlTypeStr);
        content.add(cell);

        // 是否为关键控制0:是 1:否
        cell = new InventoryCell();
        String keyControlStr = "";
        Integer keyControl = active.getKeyPoint();
        if (keyControl != null) {
            if (keyControl == 0) {
                keyControlStr = JecnProperties.getValue("yes");
            } else if (keyControl == 1) {
                keyControlStr = JecnProperties.getValue("no");
            }
        }
        cell.setText(keyControlStr);
        content.add(cell);

        // 控制频率0:随时 1:日 2: 周 3:月 4:季度 5:年度
        String controlFrequencyStr = "";
        Integer controlFrequency = active.getFrequency();
        if (controlFrequency != null) {
            if (controlFrequency == 0) {
                controlFrequencyStr = JecnProperties.getValue("Irregular");
            } else if (controlFrequency == 1) {
                controlFrequencyStr = JecnProperties.getValue("ri");
            } else if (controlFrequency == 2) {
                controlFrequencyStr = JecnProperties.getValue("week");
            } else if (controlFrequency == 3) {
                controlFrequencyStr = JecnProperties.getValue("month");
            } else if (controlFrequency == 4) {
                controlFrequencyStr = JecnProperties.getValue("quarter");
            } else if (controlFrequency == 5) {
                controlFrequencyStr = JecnProperties.getValue("yearD");
            }
        }
        cell = new InventoryCell();
        cell.setText(controlFrequencyStr);
        content.add(cell);
    }

    private void fillEmptyCell(int orgFillNum, final List<InventoryCell> content, InventoryTable data) {
        InventoryCell cell = null;
        for (int i = 0; i < orgFillNum; i++) {
            cell = new InventoryCell();
            cell.setText("");
            content.add(cell);
        }
    }

}
