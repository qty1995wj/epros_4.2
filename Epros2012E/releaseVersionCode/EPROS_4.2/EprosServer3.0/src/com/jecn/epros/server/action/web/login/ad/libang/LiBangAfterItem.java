package com.jecn.epros.server.action.web.login.ad.libang;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.lingnan.LingNanAfterItem;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.util.JecnPath;

public class LiBangAfterItem {
	private static Logger log = Logger.getLogger(LiBangAfterItem.class);
	// 需要特殊处理的OU
	public static String VERIFY_PATH = "c:\\verify";

	public static String AppCode = null;
	public static String SSOLoginUrl = null;
	public static String SSOServiceUrl = null;
	public static String CLIENT_ID = null;
	public static String CLIENT_SECRET = null;
	public static String AUTHS_URL = null;
	public static String TOKEN_URL = null;
	public static String USERINFO_URL = null;
	public static String LOGIN_URL = null;
	public static String MAIN_URL = null;
	public static String WX_LOGIN_RUL = null;
	private static ResourceBundle libangConfig;
	static {
		libangConfig = ResourceBundle.getBundle("cfgFile/libang/LiBangConfig");
	}

	public static String getValue(String key) {
		return libangConfig.getString(key);
	}

	public static void start() {
		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/libang/LiBangConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);
			VERIFY_PATH = props.getProperty("VERIFY_PATH");

			AppCode = props.getProperty("AppCode");
			SSOLoginUrl = props.getProperty("SSOLoginUrl");
			SSOServiceUrl = props.getProperty("SSOServiceUrl");
			// 域后缀
			JecnContants.DOMAIN_SUFFIX = props.getProperty("domainSuffix");
			CLIENT_ID = props.getProperty("CLIENT_ID");
			CLIENT_SECRET = props.getProperty("CLIENT_SECRET");
			AUTHS_URL = props.getProperty("AUTHS_URL");
			TOKEN_URL = props.getProperty("TOKEN_URL");
			USERINFO_URL = props.getProperty("USERINFO_URL");
			LOGIN_URL = props.getProperty("LOGIN_URL");
			MAIN_URL = props.getProperty("MAIN_URL");
			WX_LOGIN_RUL = props.getProperty("WX_LOGIN_RUL");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		}
	}
}