package com.jecn.epros.domain.journal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.service.CommonServiceUtil;

import java.util.Date;

/**
 * 用户访问日志-返回数据对象
 *
 * @author ZXH
 */
public class UserVisitsResult {
    /**
     * 文件ID
     */
    private Long relatedId;
    /**
     * 日志类型 1是流程，2是文件，3是角色,4是制度,5是标准,6是组织,9是风险,15 术语定义
     * */
    private int relatedType;
    /**
     * 文件类型
     */
    private String relatedName;
    /**
     * 操作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date operatorTime;
    /**
     * 日志类型 1是新增，2是修改，3是删除，4移动，5排序，6假删除放到回收站,10自动编号，13设置任务模板
     * 15记录恢复（本地上传的文件或者制度或者流程文件）16记录删除
     */
    private int operatorType;
    /**
     * 访问人
     */
    private String peopleName;

    private String strRelatedType;

    @JsonIgnore
    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public int getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(int relatedType) {
        this.relatedType = relatedType;
    }

    public Date getOperatorTime() {
        return operatorTime;
    }

    public void setOperatorTime(Date operatorTime) {
        this.operatorTime = operatorTime;
    }

    @JsonIgnore
    public int getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(int operatorType) {
        this.operatorType = operatorType;
    }

    public String getPeopleName() {
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public String getStrRelatedType() {
        return CommonServiceUtil.getStringTypeByRelatedType(relatedType);
    }

    public String getStrOperationType() {
        return CommonServiceUtil.getStringTypeByOperationType(operatorType);
    }
}
