package com.jecn.epros.bean.checkout;

import java.io.Serializable;
import java.util.List;

public class CheckoutResult implements Serializable {
    private String name;
    private List<CheckoutItem> ins;
    private List<CheckoutItem> outs;
    private List<CheckoutResult> sons;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CheckoutItem> getIns() {
        return ins;
    }

    public void setIns(List<CheckoutItem> ins) {
        this.ins = ins;
    }

    public List<CheckoutItem> getOuts() {
        return outs;
    }

    public void setOuts(List<CheckoutItem> outs) {
        this.outs = outs;
    }

    public List<CheckoutResult> getSons() {
        return sons;
    }

    public void setSons(List<CheckoutResult> sons) {
        this.sons = sons;
    }
}
