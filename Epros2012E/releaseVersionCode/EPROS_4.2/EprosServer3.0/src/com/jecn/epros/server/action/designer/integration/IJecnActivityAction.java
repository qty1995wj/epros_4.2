package com.jecn.epros.server.action.designer.integration;

import java.util.List;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;

/**
 * 活动类别Action接口
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-10-31 时间：上午10:01:34
 */
public interface IJecnActivityAction {
	/**
	 * 添加活动类别
	 * 
	 * @param activeTypeBean
	 * @throws Exception
	 */
	Long addActivityType(JecnActiveTypeBean activeTypeBean) throws Exception;

	/**
	 * 验证是否存在相同名称
	 * 
	 * @param name
	 *            输入的类别名称
	 * @return true:存在相同名称
	 * @throws Exception
	 */
	boolean isSameActivityType(String name) throws Exception;

	/**
	 * 获取活动类别集合
	 * 
	 * @return List<JecnActiveTypeBean>
	 * @throws Exception
	 */
	List<JecnActiveTypeBean> findJecnActiveTypeBeanList() throws Exception;

	/**
	 * 
	 * 删除活动类别
	 * 
	 * @param typeIds
	 * @throws Exception
	 */
	void delteFlowArributeType(List<Long> typeIds) throws Exception;

}
