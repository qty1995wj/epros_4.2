package com.jecn.epros.server.webBean.task;

import java.util.Set;

/**
 * 任务列表显示（我的任务、任务管理）
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Apr 25, 2013 时间：9:52:48 AM
 */
public class MyTaskBean {
	/** 任务ID */
	private long taskId;
	/** 任务名称 */
	private String taskName;
	/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
	private int taskType;
	
	/** 审批类型 0审批 1借阅 2废止 * */
	private Integer approveType;

	private Long createPeopleId;
	/** 创建名称 */
	private String createName;
	/**
	 * 任务阶段 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
	 * 8： 9 ： 11:自定义1 12 自定义2 13自定义3 14自定义4
	 * */
	private int taskStage;
	/** 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;3:拟稿人无审批状态;4:拟稿人为当前阶段审批人 */
	private int taskElseState = -1;
	/** 各阶段审核人标题名称 */
	private String stageName;
	/** 驳回状态 1：当前审批人存在驳回状态 */
	private int reJect;
	/** 更新时间 */
	private String updateTime;
	/** 任务开始时间 */
	private String startTime;
	/** 任务预估结束时间 */
	private String endTime;
	/** 任务发布时间 */
	private String publishTime;
	/** 责任人 */
	private String resPeopleName;
	/** 当前任务的操作 *******/
	private String operation;
	/** 江波龙 添加字段：返回json 客户解析显示表格 */
	private String taskTypeName;

	/********* [十所。任务代办，当前阶段审批人] **************/
	private Long taskApprovePeopleId;
	private Long taskCreatePeopleName;
	/** true:当前任务无效,false当前任务有效 */
	private String isProcessed;

	/** 任务说明 */
	private String taskDesc;

	private String rid;

	private String createPeopleLoginName;

	private int operationType;

	private Set<Long> handlerPeopleIds;

	private String createTime;

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public int getTaskStage() {
		return taskStage;
	}

	public void setTaskStage(int taskStage) {
		this.taskStage = taskStage;
	}

	public int getTaskElseState() {
		return taskElseState;
	}

	public void setTaskElseState(int taskElseState) {
		this.taskElseState = taskElseState;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public int getReJect() {
		return reJect;
	}

	public void setReJect(int reJect) {
		this.reJect = reJect;
	}

	public String getResPeopleName() {
		return resPeopleName;
	}

	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}

	public String getTaskTypeName() {
		return taskTypeName;
	}

	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Long getTaskApprovePeopleId() {
		return taskApprovePeopleId;
	}

	public void setTaskApprovePeopleId(Long taskApprovePeopleId) {
		this.taskApprovePeopleId = taskApprovePeopleId;
	}

	public String getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(String isProcessed) {
		this.isProcessed = isProcessed;
	}

	public String getTaskDesc() {
		return taskDesc;
	}

	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	public Long getTaskCreatePeopleName() {
		return taskCreatePeopleName;
	}

	public void setTaskCreatePeopleName(Long taskCreatePeopleName) {
		this.taskCreatePeopleName = taskCreatePeopleName;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getCreatePeopleLoginName() {
		return createPeopleLoginName;
	}

	public void setCreatePeopleLoginName(String createPeopleLoginName) {
		this.createPeopleLoginName = createPeopleLoginName;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public Set<Long> getHandlerPeopleIds() {
		return handlerPeopleIds;
	}

	public void setHandlerPeopleIds(Set<Long> handlerPeopleIds) {
		this.handlerPeopleIds = handlerPeopleIds;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getApproveType() {
		return approveType;
	}

	public void setApproveType(Integer approveType) {
		this.approveType = approveType;
	}
}
