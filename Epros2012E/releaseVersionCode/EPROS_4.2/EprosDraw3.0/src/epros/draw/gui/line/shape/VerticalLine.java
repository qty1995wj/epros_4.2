/*
 * VerticalLine.java
 *
 * Created on 2009年3月11日, 上午9:48
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.line.shape;

import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 竖向分割线
 * 
 * @author Administrator
 */
public class VerticalLine extends JecnBaseVHLinePanel {

	public VerticalLine(JecnVHLineData flowElementData) {
		super(flowElementData);
	}

	/**
	 * 
	 * 获取竖分割线显示/隐藏标识
	 * 
	 */
	protected boolean showVHLine() {
		return JecnSystemStaticData.isShowVerticalLine();
	}
}
