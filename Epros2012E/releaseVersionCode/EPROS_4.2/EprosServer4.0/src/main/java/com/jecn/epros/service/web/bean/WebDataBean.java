package com.jecn.epros.service.web.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/11/27.
 */
public class WebDataBean<T> {
    //返回信息 消息返回结果，true:成功;false:失败 提示信息
    private boolean result;
    //提示信息
    private String message;
    //总记录数
    private Integer total;
    //总页数
    private Integer pageCount;
    //返回的具体数据
    private List<T> content;


    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
