package epros.designer.gui.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;

import epros.designer.gui.task.buss.TaskApproveConstant;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;

public enum ConfigCommon {
	INSTANTCE;
	/** 设计器配置文件 */
	private final String DESIGNER_PROPERTIES_URL = "resources/fileProperties/CommonConfig.properties";
	private Logger log = Logger.getLogger(ConfigCommon.class);

	/** 自动编号配置 */
	private final String AUTO_CONFIG_URL = "resources/fileProperties/AutoCodeConfig.properties";

	/**
	 * 配置Bean
	 * 
	 * @param value
	 * @param key
	 * @return
	 */
	public boolean writeCommonConfigProperties(String value, ConfigCommonEnum commonEnum) {
		if (commonEnum == null) {
			return false;
		}
		Properties prop = redProperties(DESIGNER_PROPERTIES_URL);
		if (prop == null) {
			return false;
		}
		OutputStream fos = null;
		try {
			fos = writeProperties(DESIGNER_PROPERTIES_URL);
			prop.setProperty(commonEnum.toString(), value);
			prop.store(fos, null);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		} finally {
			if (fos == null) {
				return false;
			}
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
				log.error("ConfigCommon writeCommonConfigProperties is error", e);
			}
			fos = null;
		}
		return true;
	}

	/**
	 * 获取配置信息
	 * 
	 * @param commonEnum
	 * @return
	 */
	public String getConfigProperties(ConfigCommonEnum commonEnum) {
		if (commonEnum == null) {
			return null;
		}
		Properties properties = redProperties(DESIGNER_PROPERTIES_URL);
		if (properties == null) {
			return null;
		}
		return properties.getProperty(commonEnum.toString());
	}

	private Properties redProperties(String url) {
		Properties properties = new Properties();
		if (DrawCommon.isNullOrEmtryTrim(url)) {// 路径不存在
			log.error("ConfigCommon getConfigProperties is error");
			return properties;
		}
		try {
			File file = new File(JecnDesignerCommon.class.getResource("/").toURI().getPath() + url);
			// 文件、制度、流程
			InputStream in = new FileInputStream(file);
			properties.load(in);
			in.close();
		} catch (IOException e) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("ConfigCommon getConfigProperties is error", e);
			return null;
		} catch (URISyntaxException e) {
			log.error("ConfigCommon getConfigProperties is error", e);
		}
		return properties;
	}

	/**
	 * 自动编号默认显示
	 * 
	 * @param type
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @return
	 */
	public AutoCodeBean getAutoDataByConfig(int type) {
		Properties prop = redProperties(AUTO_CONFIG_URL);
		if (prop == null) {
			return null;
		}
		String typeString = getTypeMarkByType(type);
		AutoCodeBean codeBean = new AutoCodeBean();
		codeBean.setProcessCode(prop.getProperty(typeString + ConfigCommonEnum.ProcessCode.toString()));
		codeBean.setProductionLine(prop.getProperty(typeString + ConfigCommonEnum.ProductionLine.toString()));
		codeBean.setCompanyCode(prop.getProperty(typeString + ConfigCommonEnum.CompanyCode.toString()));
		codeBean.setFileType(prop.getProperty(typeString + ConfigCommonEnum.FileType.toString()));
		codeBean.setSmallClass(prop.getProperty(typeString + ConfigCommonEnum.SmallClass.toString()));
		codeBean.setProcessLevel(prop.getProperty(typeString + ConfigCommonEnum.ProcessLevel.toString()));
		return codeBean;
	}

	/**
	 * 
	 * 
	 * @param type
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @return
	 */
	private String getTypeMarkByType(int type) {
		String typeStr = "";
		switch (type) {
		case 0:
		case 1:
			typeStr = "flow";
			break;
		case 2:
		case 3:
			typeStr = "rule";
			break;
		case 4:
		case 5:
			typeStr = "file";
			break;
		default:
			break;
		}
		return typeStr;
	}

	public void setAutoDataByCodeBean(AutoCodeBean autoCodeBean, int type) {
		Properties prop = redProperties(AUTO_CONFIG_URL);
		if (prop == null) {
			return;
		}

		OutputStream fos = null;
		try {
			String typeString = getTypeMarkByType(type);
			fos = writeProperties(AUTO_CONFIG_URL);
			prop.setProperty(typeString + ConfigCommonEnum.CompanyCode.toString(), autoCodeBean.getCompanyCode());
			prop.setProperty(typeString + ConfigCommonEnum.ProcessCode.toString(),
					autoCodeBean.getProcessCode() == null ? "" : autoCodeBean.getProcessCode());
			prop.setProperty(typeString + ConfigCommonEnum.ProductionLine.toString(), autoCodeBean.getProductionLine());
			prop.setProperty(typeString + ConfigCommonEnum.FileType.toString(), autoCodeBean.getFileType());
			prop.setProperty(typeString + ConfigCommonEnum.SmallClass.toString(), autoCodeBean.getSmallClass());
			prop.setProperty(typeString + ConfigCommonEnum.ProcessLevel.toString(),
					autoCodeBean.getProcessLevel() == null ? "" : autoCodeBean.getProcessLevel());
			prop.store(fos, null);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		} finally {
			if (fos == null) {
				return;
			}
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
				log.error("ConfigCommon setAutoDataByCodeBean is error", e);
			}
			fos = null;
		}
	}

	public enum ConfigCommonEnum {
		designAccessMenu, // 1:只显示设计权限节点0:显示所有节点
		relatedFileDir, domainLogin, // 域登陆
		// 关联文件存储路径
		// 记录上一次的变化规则
		CompanyCode, ProcessCode, ProductionLine, FileType, SmallClass, ProcessLevel, remberPwdShow, // 记住密码是否显示
		typeId, // 类别
		securityLevel,
		// 级别
		// 是否显示流程图背景网格线
		showGrid,
		isIflytekLogin//是否是科大讯飞
	}

	private OutputStream writeProperties(String url) {
		OutputStream fos = null;
		if (DrawCommon.isNullOrEmtryTrim(url)) {// 路径不存在
			log.error("ConfigCommon writeProperties is error");
			return fos;
		}
		try {
			File file = new File(JecnDesignerCommon.class.getResource("/").toURI().getPath() + url);
			// 文件、制度、流程
			fos = new FileOutputStream(file);
			return fos;
		} catch (IOException ex) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("ConfigCommon writeProperties is error", ex);
			return null;
		} catch (URISyntaxException e) {
			log.error("ConfigCommon writeProperties is error", e);
		}
		return fos;
	}

	/**
	 * true：显示域登录
	 * 
	 * @return
	 */
	public boolean isDomainLogin() {
		return "1".equals(getConfigProperties(ConfigCommonEnum.domainLogin));
	}

	public boolean remberPwdShow() {
		String value = getConfigProperties(ConfigCommonEnum.remberPwdShow);
		if ("0".equals(value)) {
			return false;
		}
		return true;
	}
}
