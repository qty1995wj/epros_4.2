package com.jecn.webservice.puyuan.service;


/**
 * 
 * @ClassName: IBpsProSyncService 
 * @Description: 获取同步到Bps系统中的流程信息以及流程元素接口
 * @author cheshaowei
 * @date 2015-4-14 上午11:00:43
 * 
 */
public interface IBpsProSyncService {

	/**
	 * 
	 * @Title: SysncFlowToBps 
	 * @Description: 获取同步到Bps系统中的流程信息以及流程元素
	 * @param @param flowId 流程ID
	 * @param @param path 流程推送保存路径
	 * @return int 
	 * @author cheshaowei
	 * @date 2015-4-14 上午11:00:20
	 * @throws
	 * 
	 */
	public int SysncFlowToBps(Long flowId,String path);

	
}
