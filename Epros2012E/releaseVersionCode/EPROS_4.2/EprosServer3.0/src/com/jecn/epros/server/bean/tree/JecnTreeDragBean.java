package com.jecn.epros.server.bean.tree;

import java.io.Serializable;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
/**
 * @author yxw 2012-4-27
 * @description：节点排序bean
 */
public class JecnTreeDragBean implements Serializable{
	/**节点ID*/
	private Long id;
	/**节点排序号*/
	private int sortId;
	/**节点类型*/
	private TreeNodeType   treeNodeType;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getSortId() {
		return sortId;
	}
	public void setSortId(int sortId) {
		this.sortId = sortId;
	}
	public TreeNodeType getTreeNodeType() {
		return treeNodeType;
	}
	public void setTreeNodeType(TreeNodeType treeNodeType) {
		this.treeNodeType = treeNodeType;
	}
}
