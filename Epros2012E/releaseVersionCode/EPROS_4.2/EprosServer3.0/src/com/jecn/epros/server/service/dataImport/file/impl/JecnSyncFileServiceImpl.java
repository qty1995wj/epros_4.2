package com.jecn.epros.server.service.dataImport.file.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.JecnSqlConstant;
import com.jecn.epros.server.dao.dataImport.file.IJecnSyncFileDao;
import com.jecn.epros.server.service.dataImport.PathUpdater;
import com.jecn.epros.server.service.dataImport.file.IJecnSyncFileService;
import com.jecn.epros.server.service.file.IFileService;

@Transactional
public class JecnSyncFileServiceImpl extends AbsBaseService<String, String> implements IJecnSyncFileService {

	private final Log log = LogFactory.getLog(JecnSyncFileServiceImpl.class);

	private IJecnSyncFileDao syncFileDao;

	@Resource(name = "FileServiceImpl")
	private IFileService fileService;

	@Override
	public void syncFile(Long projectId) throws Exception {

		log.info(" 文件目录同步开始！");
		// 1、判断待删除目录是否存在，不存在创建，并返回带创建目录ID
		Long fileDirId = getToDeleteDirId(projectId);

		log.info(" 判断待删除目录是否存在，不存在创建！");
		// 2、更新节点名称和父节点
		syncFileDao.updateFileNameByOrg();
		log.info("更新节点名称和父节点！");
		// 3、新增节点
		String sql = "insert into jecn_file_T(FILE_ID, FILE_NAME, PEOPLE_ID, PER_FILE_ID,CREATE_TIME, UPDATE_PERSON_ID,UPDATE_TIME, IS_DIR,"
				+ " IS_PUBLIC, SORT_ID,"
				+ " SAVETYPE, "
				+ "PROJECT_ID,"
				+ "DEL_STATE,PUB_TIME ,HIDE,VIEW_SORT,RULE_ID,ORG_NUM,PER_ORG_NUM,NODE_TYPE)"
				+ "SELECT JECN_FILE_SEQUENCE.NEXTVAL, O.ORG_NAME,1,-1,SYSDATE,1,SYSDATE,0"
				+ " ,0,0,"
				+ JecnContants.fileSaveType
				+ ","
				+ projectId
				+ ",0,null,1,NULL,NULL,O.ORG_NUMBER,O.PREORG_NUMBER,1 FROM JECN_FLOW_ORG O"
				+ " WHERE NOT EXISTS (SELECT 1 FROM   JECN_FILE_T FT WHERE O.ORG_NUMBER = FT.ORG_NUM AND FT.DEL_STATE = 0 AND FT.IS_DIR = 0 AND FT.NODE_TYPE = 1)";
		syncFileDao.execteNative(sql);

		log.info("3、新增节点！");
		// 更新父节点

		sql = "update jecn_file_T T" + "   SET T.PER_FILE_ID =" + "       (SELECT PT.FILE_ID"
				+ "          FROM jecn_file_T PT" + "         WHERE PT.ORG_NUM = T.PER_ORG_NUM"
				+ "           and PT.DEL_STATE = 0" + "           and PT.ORG_NUM IS NOT NULL)"
				+ " where T.DEL_STATE = 0" + "   and T.ORG_NUM IS NOT NULL";
		syncFileDao.execteNative(sql);
		syncFileDao.getSession().flush();
		log.info("更新父节点");
		// 更新顶级节点父ID
		sql = "update jecn_file_T set PER_FILE_ID = 0 where PER_ORG_NUM = '0'";
		syncFileDao.execteNative(sql);
		syncFileDao.getSession().flush();

		log.info("更新顶级节点父ID");

		// 根据组织节点比较，待删除节点迁移到待迁移目录下
		updateFileDir(fileDirId);
		log.info("根据组织节点比较，待删除节点迁移到待迁移目录下");

		// 更新TPATH
		PathUpdater updater = new PathUpdater(syncFileDao);
		// 更新文件表
		updater.updateFileTLevelAndTPath();

		// 同步真实表
		syncTrueFile();
		log.info("同步真实表, 同步完成！");

	}

	private void updateFileDir(Long fileDirId) throws Exception {
		// 1、获取待迁移的目录
		String sql = getDeleteFileDirRecycleSql(fileDirId);
		List<Object[]> listObject = this.syncFileDao.listNativeSql(sql);
		if (listObject == null || listObject.isEmpty()) {
			return;
		}
		// key 是目录名称 value 是文件集合
		Map<Long, String> fileMaps = new HashMap<Long, String>();
		StringBuilder str = null;
		Set<Long> deleteIds = new HashSet<Long>();
		for (Object[] file : listObject) { // 文件
			Integer file_isDir = Integer.valueOf(file[3].toString());
			if (file_isDir == 0) {
				continue;
			}
			String filePath = file[1].toString();
			Long pId = 0L;
			str = new StringBuilder();
			// 拼接 迁移的目录名称
			for (Object[] catalogBean : listObject) {
				Integer isDir = Integer.valueOf(catalogBean[3].toString());
				if (isDir == 1) {
					continue;
				}
				String catalogPath = catalogBean[1].toString();
				String catalogName = catalogBean[2].toString();
				if (filePath.contains(catalogPath)) { // 获取文件的父级目录
					str.append(catalogName);
					str.append(".");
					pId = Long.valueOf(catalogBean[0].toString());
				} else {
					deleteIds.add(Long.valueOf(catalogBean[0].toString()));
				}
			}
			if (pId != 0) {
				if (StringUtils.isNotBlank(str.toString())) {
					str.deleteCharAt(str.length() - 1);
				}
				fileMaps.put(pId, str.toString());
			}
		}

		// 更新 文件目录到 待删除目录下 临时表
		performFileUpdates(fileMaps, fileDirId, "_T", deleteIds);
		// 更新 文件目录到 待删除目录下 正式表
		performFileUpdates(fileMaps, fileDirId, " ", deleteIds);

	}

	/**
	 * 拼接 文件同步更新sql
	 * 
	 * @return
	 */
	private void performFileUpdates(Map<Long, String> fileMaps, Long fileDirId, String pubStr, Set<Long> deleteIds) {
		if (fileMaps == null || fileMaps.isEmpty() || fileDirId == null) {
			return;
		}

		// 更新文件目录名称 返回更新目录的ID
		Set<Long> ids = updateFileDirs(fileMaps, pubStr, fileDirId);

		// 过滤 已经更新的文件ID 并删除
		Iterator<Long> it = ids.iterator();
		while (it.hasNext()) {
			deleteIds.remove(it.next());
		}
		delteFiles(deleteIds, pubStr);

	}

	private Set<Long> updateFileDirs(Map<Long, String> fileMaps, String pubStr, Long fileDirId) {

		Map<Long, String> doMap = new HashMap<Long, String>();
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE JECN_FILE" + pubStr + " SET PER_FILE_ID = ?,FILE_NAME = ( case ");
		Set<Long> ids = new HashSet<Long>();
		for (Map.Entry<Long, String> entry : fileMaps.entrySet()) {
			ids.add(entry.getKey());
			doMap.put(entry.getKey(), entry.getValue());
			// 100条执行
			if (doMap.size() == 100) {
				Set<Long> inIds = new HashSet<Long>();
				for (Map.Entry<Long, String> map : doMap.entrySet()) {
					inIds.add(map.getKey());
					sql.append("WHEN FILE_ID = " + map.getKey() + " THEN '" + map.getValue() + "'");
				}
				sql.append(" ELSE FILE_NAME END ) WHERE ORG_NUM IS NOT NULL  AND IS_DIR = 0 and file_id in"
						+ JecnCommonSql.getIdsSet(inIds));
				this.syncFileDao.execteNative(sql.toString(), fileDirId);
				doMap = new HashMap<Long, String>();
				sql = new StringBuilder();
				sql.append("UPDATE JECN_FILE" + pubStr + " SET PER_FILE_ID = ?,FILE_NAME = ( case ");
			}

		}

		// 不足100条执行
		if (doMap != null && !doMap.isEmpty()) {
			Set<Long> inIds = new HashSet<Long>();
			for (Map.Entry<Long, String> map : doMap.entrySet()) {
				inIds.add(map.getKey());
				sql.append("WHEN FILE_ID = " + map.getKey() + " THEN '" + map.getValue() + "'");
			}
			sql.append(" ELSE FILE_NAME END ) WHERE ORG_NUM IS NOT NULL  AND IS_DIR = 0 and file_id in"
					+ JecnCommonSql.getIdsSet(inIds));
			this.syncFileDao.execteNative(sql.toString(), fileDirId);
		}

		return ids;
	}

	private void delteFiles(Set<Long> deleteIds, String pubStr) {
		// 删除其他没有文件的
		String deleteSql = "update JECN_FILE" + pubStr + " set del_state = 1 where file_id  in";
		List<String> listStr = JecnCommonSql.getSetSqls(deleteSql, deleteIds);
		for (String str : listStr) {
			str += " and is_dir = 0 and  ORG_NUM IS NOT NULL";
			syncFileDao.execteNative(str);
		}
	}

	private String getDeleteFileDirRecycleSql(Long fileDirId) throws Exception {
		String sql = "SELECT FT.FILE_ID, FT.T_PATH,FT.FILE_NAME,FT.IS_DIR,FT.NODE_TYPE " + "  FROM JECN_FILE_T FT "
				+ "   WHERE  NOT EXISTS "
				+ " (SELECT 1 FROM JECN_FLOW_ORG O WHERE FT.ORG_NUM = O.ORG_NUMBER) AND FILE_ID !=" + fileDirId
				+ " AND FT.DEL_STATE = 0 AND FT.T_PATH IS NOT NULL  ORDER BY FT.T_PATH";
		return sql;
	}

	private void syncTrueFile() {
		String sql = "SELECT FILE_ID" + "  FROM JECN_FILE_T T"
				+ " WHERE NOT EXISTS (SELECT 1 FROM JECN_FILE JF WHERE JF.FILE_ID = T.FILE_ID)"
				+ "   AND T.NODE_TYPE = 1" + "   AND T.ORG_NUM IS NOT NULL" + "   AND T.Del_State = 0";
		List<Long> listFileIds = syncFileDao.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
		sql = JecnSqlConstant.JECN_FILE + " in";
		List<String> listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			syncFileDao.execteNative(str);
		}
	}

	private Long getToDeleteDirId(Long projectId) throws Exception {
		String fileName = "待迁移文件";
		String sql = "SELECT FILE_ID FROM JECN_FILE_T T WHERE T.FILE_NAME = ? AND T.NODE_TYPE = 1";
		Object obj = syncFileDao.getObjectNativeSql(sql, fileName);

		Long fileId = obj == null ? null : Long.valueOf(obj.toString());

		if (fileId == null) {
			JecnFileBeanT fileDir = new JecnFileBeanT();
			// 文件名称
			fileDir.setFileName(fileName);
			// 创建人
			fileDir.setPeopleID(1L);
			// 更新人
			fileDir.setUpdatePersonId(1L);
			// 父ID
			fileDir.setPerFileId(0L);
			// 是否是目录 0：目录，1：文件
			fileDir.setIsDir(0);
			fileDir.setNodeType(1);
			// 密级：0是秘密,1是公开
			fileDir.setIsPublic(0L);
			// 隐藏 0:隐藏 1：显示
			fileDir.setHide(1);
			// 创建时间
			fileDir.setCreateTime(new Date());
			// 项目ID
			fileDir.setProjectId(projectId);
			fileDir.setDelState(0);
			// 排序
			fileDir.setSortId(0);
			fileService.addFileDir(fileDir);

			fileId = fileDir.getFileID();
		}
		return fileId;
	}

	public IJecnSyncFileDao getSyncFileDao() {
		return syncFileDao;
	}

	public void setSyncFileDao(IJecnSyncFileDao syncFileDao) {
		this.syncFileDao = syncFileDao;
	}
}
