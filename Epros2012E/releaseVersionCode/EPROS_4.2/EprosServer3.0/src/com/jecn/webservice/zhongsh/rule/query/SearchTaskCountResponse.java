
package com.jecn.webservice.zhongsh.rule.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchTaskCountResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchTaskCountResult"
})
@XmlRootElement(name = "SearchTaskCountResponse")
public class SearchTaskCountResponse {

    @XmlElement(name = "SearchTaskCountResult")
    protected int searchTaskCountResult;

    /**
     * 获取searchTaskCountResult属性的值。
     * 
     */
    public int getSearchTaskCountResult() {
        return searchTaskCountResult;
    }

    /**
     * 设置searchTaskCountResult属性的值。
     * 
     */
    public void setSearchTaskCountResult(int value) {
        this.searchTaskCountResult = value;
    }

}
