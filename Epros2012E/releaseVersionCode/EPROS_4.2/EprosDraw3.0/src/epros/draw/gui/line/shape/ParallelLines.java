/*      Copyright (c) 2009 JECN Corporation                     */
/*      JECN CONFIDENTIAL AND PROPRIETARY                       */
/*      All rights reserved by JECN Corporation.                */
/*      This program must be used solely for the purpose for    */
/*      which it was furnished by JECN Corporation.   No part   */
/*      of this program may be reproduced  or  disclosed  to    */
/*      others,  in  any form,  without  the  prior  written    */
/*      permission of JECN Corporation. Use  of  copyright      */
/*      notice does not evidence publication of the program.    */
package epros.draw.gui.line.shape;

import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 横向分割线
 * 
 * @author Administrator
 */
public class ParallelLines extends JecnBaseVHLinePanel {

	public ParallelLines(JecnVHLineData flowElementData) {
		super(flowElementData);
	}

	/**
	 * 
	 * 获取横分割线显示/隐藏标识
	 * 
	 */
	protected boolean showVHLine() {
		return JecnSystemStaticData.isShowParallelLines();
	}
}
