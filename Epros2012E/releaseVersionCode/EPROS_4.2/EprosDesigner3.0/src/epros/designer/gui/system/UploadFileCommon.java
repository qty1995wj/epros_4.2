package epros.designer.gui.system;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.standard.StandardFileContent;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnUIUtil;

public class UploadFileCommon {
	/** 滚动面板，加入fileTable */
	private JScrollPane fileTableScrollPanel;
	private JTable fileTable;;
	private JecnPanel fileButtonPanel;;
	/** 选择按钮 */
	private JButton uploadBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 打开按钮 */
	private JButton openBut = new JButton(JecnProperties.getValue("openBtn"));

	/** 删除 */
	private JButton deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));
	// 验证提示
	private JLabel fileVerfyLab = new JLabel();
	private JecnPanel filePanel;

	/** 窗体 */
	private JecnDialog jecnDialog;

	public UploadFileCommon(JPanel mainPanel, int rows, int columns, Insets insets, int width, JecnDialog jecnDialog) {
		this.jecnDialog = jecnDialog;

		fileButtonPanel = new JecnPanel(width - 20, 30);
		fileTable = new UploadFileTable();
		fileTableScrollPanel = new JScrollPane();

		Dimension dimension = new Dimension(width - 20, 175);
		fileTableScrollPanel.setPreferredSize(dimension);
		fileTableScrollPanel.setMinimumSize(dimension);
		fileTableScrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		filePanel = new JecnPanel(width - 10, 240);
		// 添加文件
		filePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("addFile")));
		/** ==============end================================ */
		filePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("addFile")));// 添加文件
		// 验证提示滚动设置tipLabScroPane
		fileVerfyLab.setForeground(Color.red);

		fileTableScrollPanel.setViewportView(fileTable);
		fileButtonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		fileButtonPanel.add(uploadBut);
		fileButtonPanel.add(openBut);
		fileButtonPanel.add(deleteBut);
		fileButtonPanel.add(fileVerfyLab);

		filePanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets1 = new Insets(5, 5, 5, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets1,
				0, 0);
		filePanel.add(fileTableScrollPanel, c);

		// 确认按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets1,
				0, 0);
		filePanel.add(fileButtonPanel, c);

		c = new GridBagConstraints(columns, rows, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insets, 0, 0);
		mainPanel.add(filePanel, c);

		initActionListener();
	}

	public void initTableData(List<StandardFileContent> fileContents) {
		if (fileContents == null) {
			return;
		}
		// 已经选择过的文件
		Vector<Vector<String>> vs = ((DefaultTableModel) fileTable.getModel()).getDataVector();
		for (StandardFileContent fileContent : fileContents) {
			Vector<String> data = new Vector<String>();
			data.add(fileContent.getId().toString());
			data.add(fileContent.getFileName());
			data.add(null);
			((DefaultTableModel) fileTable.getModel()).addRow(data);
		}
	}

	private void initActionListener() {
		uploadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectFile();
			}
		});

		openBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openFile();
			}
		});

		deleteBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectRows = fileTable.getSelectedRows();
				if (selectRows.length == 0) {
					fileVerfyLab.setText(JecnProperties.getValue("chooseOneRow"));
					return;
				}
				for (int i = selectRows.length - 1; i >= 0; i--) {
					((DefaultTableModel) fileTable.getModel()).removeRow(selectRows[i]);
				}
				fileVerfyLab.setText("");
			}
		});

	}

	class UploadFileTable extends JTable {

		public UploadFileTable() {

			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}
			// this.addKeyListener(new CTableKeyAdapter(this));
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		public UploadTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("id"));
			title.add(JecnProperties.getValue("fileName"));
			title.add(JecnProperties.getValue("fileSrc"));
			return new UploadTableModel(null, title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {
			return new int[] { 0, 2 };
		}
	}

	class UploadTableModel extends DefaultTableModel {
		public UploadTableModel(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			if (colindex == 0) {
				return true;
			}
			return false;
		}

	}

	/**
	 * 选择要上传的制度文件
	 */
	protected void selectFile() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSavaRuleUpdateDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);

		fileChoose.setEidtTextFiled(false);

		// 可以同时上传多个文件
		fileChoose.setMultiSelectionEnabled(true);
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
		int i = fileChoose.showSaveDialog(jecnDialog);
		if (i == JFileChooser.APPROVE_OPTION) {
			// 选择的文件
			File[] files = fileChoose.getSelectedFiles();

			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSavaRuleUpdateDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			Vector<Vector<Object>> vs = ((DefaultTableModel) fileTable.getModel()).getDataVector();
			F: for (File f : files) {
				for (Vector t : vs) {// 判断是否已上传
					if (f.getPath().equals((String) t.get(2))) {
						continue F;
					}
				}
				Vector<Object> v = new Vector<Object>();
				v.add("");
				v.add(JecnUtil.getFileName(f.getPath()));
				v.add(f.getPath());
				((DefaultTableModel) fileTable.getModel()).addRow(v);
			}
		}
	}

	public List<StandardFileContent> getFileContents() {
		this.getFileVerfyLab().setText("");
		JTable fileTable = this.getFileTable();
		if (fileTable.isEditing()) {
			fileTable.getCellEditor().stopCellEditing();
		}
		int rows = fileTable.getRowCount();

		List<StandardFileContent> addFileContents = new ArrayList<StandardFileContent>();
		StandardFileContent fileContent = null;
		TableModel model = this.getFileTable().getModel();
		// 获取父节点的hide值
		for (int i = 0; i < rows; i++) {
			fileContent = new StandardFileContent();
			fileContent.setFileName((String) model.getValueAt(i, 1));
			fileContent.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			if (StringUtils.isNotBlank(model.getValueAt(i, 0).toString())) {
				fileContent.setId(Long.valueOf(model.getValueAt(i, 0).toString()));
			}
			if (fileContent.getId() == null) {
				fileContent.setBytes(JecnUtil.changeFileToByte((String) model.getValueAt(i, 2)));
			}
			addFileContents.add(fileContent);
		}
		return addFileContents;
	}

	/**
	 * 打开文件
	 */
	protected void openFile() {
		// 判断是否选中Table中的一行，没选中，提示选中一行
		int[] selectRows = fileTable.getSelectedRows();

		if (selectRows.length == 0) {
			fileVerfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		for (int i = 0; i < selectRows.length; i++) {
			if (fileTable.getValueAt(selectRows[0], 0) != null
					&& StringUtils.isNotBlank(fileTable.getValueAt(selectRows[0], 0).toString())) {
				Long id = Long.valueOf((String) fileTable.getValueAt(selectRows[0], 0));
				fileVerfyLab.setText("");
				JecnDesignerCommon.openStandardContentFile(id);
			} else {
				String filePath = (String) fileTable.getValueAt(selectRows[i], 2);
				JecnUtil.openLocalFile(filePath);
			}
		}
		fileVerfyLab.setText("");
	}

	public JTable getFileTable() {
		return fileTable;
	}

	public JLabel getFileVerfyLab() {
		return fileVerfyLab;
	}

	public void setFileVerfyLab(JLabel fileVerfyLab) {
		this.fileVerfyLab = fileVerfyLab;
	}

	/**
	 * 多选是否更新
	 * 
	 * @param oldStrIds
	 * @param list
	 * @return
	 */
	public boolean isUpdate(List<StandardFileContent> saveFileContents, List<StandardFileContent> initFileContents) {
		if (initFileContents != null && initFileContents.size() > 0) {
			if (saveFileContents.size() == 0) {
				return true;
			} else {
				if (initFileContents.size() != saveFileContents.size()) {
					return true;
				} else {
					for (StandardFileContent saveContentId : saveFileContents) {
						if (saveContentId.getId() == null) {
							return true;
						}
					}
				}
			}
		} else {
			if (saveFileContents.size() > 0) {
				return true;
			}
		}
		return false;
	}
}
