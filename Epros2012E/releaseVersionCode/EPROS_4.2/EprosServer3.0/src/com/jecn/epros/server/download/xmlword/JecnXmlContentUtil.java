package com.jecn.epros.server.download.xmlword;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnUtil;

/**
 * xml word 内容操作类
 * 
 * @author fuzhh 2013-6-3
 * 
 */
public class JecnXmlContentUtil {

	/**
	 * 创建标题和内容
	 * 
	 * @author fuzhh 2013-5-24
	 * @param titleName
	 * @param contentVal
	 * @param titleEle
	 * @param contentEle
	 */
	public static void createTitleAndContent(String titleName,
			String contentVal, Element titleEle, Element contentEle) {
		// 创建标题
		createTitle(titleName, titleEle);
		// 创建内容
		createContent(contentVal, contentEle);
	}

	/**
	 * 创建文件名称
	 * 
	 * @author fuzhh 2013-5-27
	 * @param fileNameList
	 *            string集合
	 * @param titleName
	 *            标题名称
	 * @param titleEle
	 *            标题节点
	 * @param contentEle
	 *            内容节点
	 */
	public static void createFileNameByStrList(List<String> fileNameList,
			String titleName, Element titleEle, Element contentEle) {
		String contentVal = "";
		if (fileNameList.size() > 0) {
			for (String standardName : fileNameList) {
				contentVal = standardName + "\n";
			}
			contentVal.substring(0, contentVal.length() - 2);
		} else {
			contentVal = JecnUtil.getValue("none");
		}
		createTitleAndContent(titleName, contentVal, titleEle, contentEle);
	}

	/**
	 * 创建标题
	 * 
	 * @author fuzhh 2013-5-24
	 * @param titleName
	 *            标题名称
	 * @param titleEle
	 *            标题模板
	 */
	public static void createTitle(String titleName, Element titleEle) {
		Element copeNode = XmlDownUtil.copyNodes(titleEle);
		XmlDownUtil.updateNodeValueByNode(copeNode, titleName);
	}

	/**
	 * 创建内容
	 * 
	 * @author fuzhh 2013-5-24
	 * @param contentVal
	 *            内容
	 * @param contentEle
	 *            内容模板
	 */
	public static void createContent(String contentVal, Element contentEle) {
		String[] strNum = formatContent(contentVal);
		for (String str : strNum) {
			Element copeConNode = XmlDownUtil.copyNodes(contentEle);
			XmlDownUtil.updateNodeValueByNode(copeConNode, str);
		}
	}

	/**
	 * obj数组转换为String 数组
	 * 
	 * @author fuzhh 2013-5-28
	 * @param obj
	 * @return
	 */
	public static String[] strNumsByObj(Object[] obj,int titleCount) {
		String[] strVals = new String[titleCount];
		for (int i = 0; i < titleCount; i++) {
			strVals[i] = (String) obj[i];
		}
		return strVals;
	}

	/**
	 * 内容分割
	 * 
	 * @author fuzhh 2013-5-24
	 * @param contentVal
	 * @return
	 */
	public static String[] formatContent(String contentVal) {
		contentVal = contentStr(contentVal);
		String[] strNum = contentVal.split("\n");
		return strNum;
	}

	/**
	 * 内容格式化
	 * 
	 * @author fuzhh 2013-5-24
	 * @param contentVal
	 * @return
	 */
	public static String contentStr(String contentVal) {
		if (JecnCommon.isNullOrEmtryTrim(contentVal)) {
			contentVal = JecnUtil.getValue("none");
		}
		return contentVal;
	}

	/**
	 * 设置数据
	 * 
	 * @author fuzhh 2013-5-30
	 * @param sectionEle
	 *            正文节点
	 * @param processDownloadBean
	 *            数据
	 */
	public static void setDefineData(Element sectionEle,
			ProcessDownloadBean processDownloadBean) {
		// 查询传入节点下所有的节点
		List<Element> allEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(sectionEle, allEleList);

		for (Element ele : allEleList) {
			if ("t".equals(ele.getName())) {
				// 是否需要修改
				boolean isUpdate = false;
				// 节点下的值
				String eleVal = XmlDownUtil.getNodeValue(ele);
				if (!JecnCommon.isNullOrEmtryTrim(eleVal)) {
					eleVal = eleVal.trim();
				}
				// 设置 的值
				String setVal = "";
				if ("JecnCompanyName".equals(eleVal)) { // 公司名称
					setVal = processDownloadBean.getCompanyName();
					isUpdate = true;
				} else if ("JecnProcessName".equals(eleVal)) { // 流程名称
					setVal = processDownloadBean.getFlowName();
					isUpdate = true;
				} else if ("JecnProcessNumber".equals(eleVal)) { // 流程编号
					setVal = processDownloadBean.getFlowInputNum();
					isUpdate = true;
				} else if ("JecnProcessVersion".equals(eleVal)) { // 流程版本
					setVal = processDownloadBean.getFlowVersion();
					isUpdate = true;
				} else if ("JecnUpdateDate".equals(eleVal)) { // 更新日期
					setVal = processDownloadBean.getUpdateDate();
					isUpdate = true;
				} else if ("JecnReleaseDate".equals(eleVal)) { // 发布日期
					setVal = processDownloadBean.getReleaseDate();
					isUpdate = true;
				} else if ("JecnEffectiveDate".equals(eleVal)) { // 生效日期
					setVal = processDownloadBean.getEffectiveDate();
					isUpdate = true;
				} else if ("JecnProcessCustom".equals(eleVal)) { // 流程客户
					setVal = processDownloadBean.getFlowCustom();
					isUpdate = true;
				} else if ("JecnProcessIsPublic".equals(eleVal)) { // 流程密集
					if ("0".equals(processDownloadBean.getFlowIsPublic())) {
						setVal = JecnUtil.getValue("secret");
					} else if ("1"
							.equals(processDownloadBean.getFlowIsPublic())) {
						setVal = JecnUtil.getValue("public");
					}
					isUpdate = true;
				}
				if (setVal == null) {
					setVal = "";
				}
				// 设置节点值
				if (isUpdate) {
					ele.setText(setVal);
				}
			}
		}
	}
}
