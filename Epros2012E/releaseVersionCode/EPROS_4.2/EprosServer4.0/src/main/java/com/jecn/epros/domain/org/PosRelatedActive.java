package com.jecn.epros.domain.org;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.process.kpi.RefIndicators;

import java.util.List;

/**
 * 岗位参与流程的活动
 *
 * @author lenovo
 */
public class PosRelatedActive {
    private Long activeId;
    private String activeName;
    private String acticeNum;
    private List<RefIndicators> listRefIndicators;

    public LinkResource getLink() {
        return new LinkResource(activeId, activeName, ResourceType.ACTIVITY);
    }

    public void setActiveId(Long activeId) {
        this.activeId = activeId;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public String getActiceNum() {
        return acticeNum;
    }

    public void setActiceNum(String acticeNum) {
        this.acticeNum = acticeNum;
    }

    public List<RefIndicators> getListRefIndicators() {
        return listRefIndicators;
    }

    public void setListRefIndicators(List<RefIndicators> listRefIndicators) {
        this.listRefIndicators = listRefIndicators;
    }
}
