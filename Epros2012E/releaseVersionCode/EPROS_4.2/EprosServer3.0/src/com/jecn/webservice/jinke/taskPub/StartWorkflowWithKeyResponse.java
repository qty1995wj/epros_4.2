/**
 * StartWorkflowWithKeyResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class StartWorkflowWithKeyResponse  implements java.io.Serializable {
    private com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflowWithKeyResult;

    public StartWorkflowWithKeyResponse() {
    }

    public StartWorkflowWithKeyResponse(
           com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflowWithKeyResult) {
           this.startWorkflowWithKeyResult = startWorkflowWithKeyResult;
    }


    /**
     * Gets the startWorkflowWithKeyResult value for this StartWorkflowWithKeyResponse.
     * 
     * @return startWorkflowWithKeyResult
     */
    public com.jecn.webservice.jinke.taskPub.BPMServiceResult getStartWorkflowWithKeyResult() {
        return startWorkflowWithKeyResult;
    }


    /**
     * Sets the startWorkflowWithKeyResult value for this StartWorkflowWithKeyResponse.
     * 
     * @param startWorkflowWithKeyResult
     */
    public void setStartWorkflowWithKeyResult(com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflowWithKeyResult) {
        this.startWorkflowWithKeyResult = startWorkflowWithKeyResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StartWorkflowWithKeyResponse)) return false;
        StartWorkflowWithKeyResponse other = (StartWorkflowWithKeyResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.startWorkflowWithKeyResult==null && other.getStartWorkflowWithKeyResult()==null) || 
             (this.startWorkflowWithKeyResult!=null &&
              this.startWorkflowWithKeyResult.equals(other.getStartWorkflowWithKeyResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStartWorkflowWithKeyResult() != null) {
            _hashCode += getStartWorkflowWithKeyResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StartWorkflowWithKeyResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">StartWorkflowWithKeyResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startWorkflowWithKeyResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "StartWorkflowWithKeyResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "BPMServiceResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
