package wordxml.element.bean.paragraph;

import wordxml.constant.Constants.LineRuleType;

/**
 * 行距
 * @author user
 *
 */
public class ParagraphLineRule {
	/** 行距 单位/磅 转换为缇 1*20=20 */
	private float line;
	/** （ line 设置为1 类型设置为AUTO）=单倍行距 1.5 1.5倍行距 2 2倍行距 非1、1.5、2时 均为多倍行距 */
	/** 行距类型 默认使用绝对值：exact */
	private LineRuleType ruleType = LineRuleType.EXACT;

	public ParagraphLineRule(float line) {
		this.line = line;
	}

	/**
	 * @param line 行距 单位为磅
	 * @param ruleType 行距类型
	 */
	public ParagraphLineRule(float line, LineRuleType ruleType) {
		this.line = line;
		this.setRuleType(ruleType);
	}

	public static ParagraphLineRule newInstance(float line) {
		return new ParagraphLineRule(line);
	}

	public float getLine() {
		return line;
	}

	public void setLine(float line) {
		this.line = line;
	}

	public LineRuleType getRuleType() {
		return ruleType;
	}

	public void setRuleType(LineRuleType ruleType) {
		if (ruleType == null) {
			return;
		}
		this.ruleType = ruleType;
	}

}
