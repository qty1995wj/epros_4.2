package com.jecn.epros.server.action.designer.system.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.system.IJecnFixedEmailAction;
import com.jecn.epros.server.bean.system.JecnFixedEmail;
import com.jecn.epros.server.service.system.IJecnFixedEmailService;

/***
 * 发布 固定人员处理
 * 2014-04-09
 *
 */
public class JecnFixedEmailActionImpl implements IJecnFixedEmailAction{
	
	private IJecnFixedEmailService fixedEmailService;

	public IJecnFixedEmailService getFixedEmailService() {
		return fixedEmailService;
	}

	public void setFixedEmailService(IJecnFixedEmailService fixedEmailService) {
		this.fixedEmailService = fixedEmailService;
	}

	@Override
	public void AddFixedEmail(List<JecnFixedEmail> fixedEmailList) throws Exception {
		fixedEmailService.AddFixedEmail(fixedEmailList);
	}

	@Override
	public void deleteFixedEmail(List<String> fixedEmailIds) throws Exception {
		fixedEmailService.deleteFixedEmail(fixedEmailIds);
	}

	@Override
	public List<JecnFixedEmail> getFixedEmailList() throws Exception {
		return fixedEmailService.getFixedEmailList();
	}
	

}
