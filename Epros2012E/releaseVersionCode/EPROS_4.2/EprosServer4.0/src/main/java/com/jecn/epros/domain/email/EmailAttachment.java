package com.jecn.epros.domain.email;

/**
 * 邮件内容表
 */

public class EmailAttachment implements java.io.Serializable {

    private String id;
    /**
     * 附件内容 和数据库关联
     */
    private byte[] attachment;
    /**
     * 附件名称
     */
    private String attachmentName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }


}