package epros.draw.event;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.shape.part.DataImage;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnFlowElementCopyAndPaste;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 工具栏编辑动作处理类
 * 
 * @author ZHOUXY
 */
public class JecnEditerActionProcess extends JecnAbstractActionProcess {

	@Override
	protected void actionPerformed(ActionEvent e, ToolBarElemType toolBarElemType) {
		process(toolBarElemType);
	}

	/**
	 * 
	 * 编辑工具栏中动作处理
	 * 
	 * @param toolBarElemType
	 *            ToolBarElemType 按钮类型
	 */
	private void process(ToolBarElemType toolBarElemType) {
		if (getWorkflow() == null) {
			return;
		}
		switch (toolBarElemType) {
		case editUpLayer: // 上移一层.
			fluctuationFigureLayer(true);
			break;
		case editDownLayer: // 下移一层
			fluctuationFigureLayer(false);
			break;
		case editFrontLayer: // 置于顶层
			topDownFigureLayer(true);
			break;
		case editBackLayer: // 置于底层
			topDownFigureLayer(false);
			break;

		case editCut: // 剪切
			JecnFlowElementCopyAndPaste.flowElementCut();
			break;

		case editCopy: // 复制
			JecnFlowElementCopyAndPaste.flowElementCopy();
			break;

		case editPaste: // 粘贴
			if (JecnFlowElementCopyAndPaste.getEditPastePoint() != null) {// 点击图形鼠标右键粘贴
				// 获取右键点击时相对面板的点
				getWorkflow().setStartPoint(JecnFlowElementCopyAndPaste.getEditPastePoint());
				JecnFlowElementCopyAndPaste.setEditPastePoint(null);
			}
			JecnFlowElementCopyAndPaste.flowElementPaste();
			break;

		case editDelete: // 删除
			JecnFlowElementCopyAndPaste.flowElementDelete();
			break;
		case formatPainter:// 格式刷
			JecnFlowElementCopyAndPaste.formatPainterFigureAttrs();
			break;
		case editIconshot: // 截图
			JecnFlowElementCopyAndPaste.iconShot();
			break;
		case editClockWize: // 顺时针旋转
			turnFigure(true);
			break;
		case editAntiClockWize: // 逆时针旋转
			turnFigure(false);
			break;
		case editFill: // 填充

			break;
		case editLine: // 线条

			break;
		case editShadow: // 阴影

			break;
		case edit3D: // 3D效果
			updateFigure3D();
			break;
		}
		getWorkflow().repaint();
	}

	/**
	 * 获取面板
	 * 
	 * @return 面板
	 */
	private JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 
	 * 获取画图面板上所有流程元素集合
	 * 
	 * @return getAllFigureList 流程元素集合
	 */
	private List<JecnBaseFlowElementPanel> getCurrentSelectElementList() {
		if (getWorkflow() == null) {
			return null;
		}
		return getWorkflow().getCurrentSelectElement();
	}

	/**
	 * 修改图形3D设置
	 */
	private void updateFigure3D() {
		if (getCurrentSelectElementList() == null || getCurrentSelectElementList().size() == 0) {
			return;
		}
		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		for (JecnBaseFlowElementPanel baseFlowElementPanel : getCurrentSelectElementList()) {
			if (baseFlowElementPanel instanceof DataImage
					|| FillType.none.equals(baseFlowElementPanel.getFlowElementData().getFillType())) {
				continue;
			}
			// 记录操作前数据
			undoData.recodeFlowElement(baseFlowElementPanel);
			if (baseFlowElementPanel.getFlowElementData().isFlag3D()) {
				JecnElementAttributesUtil.updateFlag3D(baseFlowElementPanel, false);
			} else {
				JecnElementAttributesUtil.updateFlag3D(baseFlowElementPanel, true);
			}
			// 记录操作后数据
			redoData.recodeFlowElement(baseFlowElementPanel);
		}
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
	}

	/**
	 * 流程元素对象是否可以旋转
	 * 
	 * @param elementPanel
	 *            JecnBaseFlowElementPanel流程元素对象
	 * @return True 不可以旋转
	 */
	private boolean isNoTurnFigure(JecnBaseFlowElementPanel elementPanel) {
		if (elementPanel == null) {
			return false;
		}
		switch (elementPanel.getFlowElementData().getMapElemType()) {
		case ModelFigure:
		case FlowLevelFigureOne:
		case FlowLevelFigureTwo:
		case FlowLevelFigureThree:
		case FlowLevelFigureFour:
		case ActiveFigureAR:
			return true;

		default:
			break;
		}
		return false;
	}

	/**
	 * 图形旋转
	 * 
	 * @param type
	 *            true表示顺时针 false表示逆时针
	 */
	private void turnFigure(boolean type) {
		// 判断当前面板是否为空
		if (getWorkflow() == null) {
			return;
		}
		// 旋转角度
		double angleDouble = 0.0;
		// 判断选择的图形如果不是一个就返回
		if (getCurrentSelectElementList().size() != 1) {
			return;
		}

		JecnBaseFlowElementPanel elementPanel = (JecnBaseFlowElementPanel) getCurrentSelectElementList().get(0);
		if (isNoTurnFigure(elementPanel)) {
			return;
		}
		if (!(elementPanel instanceof JecnBaseFigurePanel)) {
			return;
		}

		JecnBaseFigurePanel baseFlowElement = (JecnBaseFigurePanel) elementPanel;
		CommentText commentText = null;
		if (baseFlowElement instanceof CommentText) {// 注释框特殊处理
			commentText = (CommentText) baseFlowElement;
			// 旋转
			commentText.isCurrAlgle();
		}
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 记录操作前数据
		undoData.recodeFlowElement(baseFlowElement);

		// 获取图形的旋转角度
		angleDouble = baseFlowElement.getFlowElementData().getCurrentAngle();
		// 判断是否为双向箭头和单向箭头，如果是 则选择45°
		if (baseFlowElement.getFlowElementData().getMapElemType() == MapElemType.TwoArrowLine
				|| baseFlowElement.getFlowElementData().getMapElemType() == MapElemType.OneArrowLine) {
			if (type) {
				angleDouble += 45.0;
			} else {
				angleDouble += -45.0;
			}
		} else {
			if (type) {
				angleDouble += 90.0;
			} else {
				angleDouble += -90.0;
			}
		}
		// 获取判断后的旋转角度
		angleDouble = getCurrentAngle(angleDouble);
		// 新位置点
		Point newPoint = new Point();
		// 设置新的位置点
		newPoint.setLocation(baseFlowElement.getLocation().x + baseFlowElement.getWidth() / 2.0
				- baseFlowElement.getHeight() / 2.0, baseFlowElement.getLocation().y - baseFlowElement.getWidth() / 2.0
				+ baseFlowElement.getHeight() / 2.0);

		// 验证新的位置点是否在面板内（位置点横纵坐标值都必须大于0）
		if (newPoint.x <= 0 || newPoint.y <= 0) {
			// 清空对象
			undoData = null;
			redoData = null;
			newPoint = null;
			return;
		}

		// 获取图形的大小
		Dimension size = new Dimension();
		// 获取100%比例下的大小
		size.setSize(baseFlowElement.getHeight() / getWorkflow().getWorkflowScale(), baseFlowElement.getWidth()
				/ getWorkflow().getWorkflowScale());
		// 重置位置点数据
		baseFlowElement.getJecnFigureDataCloneable().reSetAttributes(newPoint, size, getWorkflow().getWorkflowScale());
		baseFlowElement.getFlowElementData().setCurrentAngle(angleDouble);

		JecnFigureDataCloneable figureDataCloneable = baseFlowElement.getZoomJecnFigureDataCloneable();

		// 重置图形的数据
		baseFlowElement.setBounds(figureDataCloneable.getX(), figureDataCloneable.getY(), figureDataCloneable
				.getWidth(), figureDataCloneable.getHeight());

		// 选转后,重新绘制阴影
		baseFlowElement.repaintShadowPanel();
		// 重画连接线
		JecnDraggedFiguresPaintLine.paintLineByFigures(baseFlowElement);

		// 记录操作后数据
		redoData.recodeFlowElement(baseFlowElement);
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);

		getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
		// 刷新面板
		getWorkflow().repaint();
		// 面板获取焦点
		getWorkflow().requestFocusInWindow();
	}

	/**
	 * 旋转角度判断
	 * 
	 * @param pCurrentAngle
	 *            选择角度
	 * @return
	 */
	public double getCurrentAngle(double angleDouble) {
		if (angleDouble >= 360.0) {
			angleDouble = angleDouble - 360.0;
		}
		if (angleDouble < 0.0) {
			angleDouble = angleDouble + 360.0;
		}
		return angleDouble;
	}

	/**
	 * 上下移动层级
	 * 
	 * @param flag
	 *            true为上移 false为下移
	 */
	private void fluctuationFigureLayer(boolean flag) {
		// 判断当前面板是否为空
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getGraphicsLevel().fluctuationFigureLayer(flag);
	}

	/**
	 * 置顶置底层级
	 * 
	 * @param flag
	 *            true为置顶 false为置底
	 */
	private void topDownFigureLayer(boolean flag) {
		// 判断当前面板是否为空
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getGraphicsLevel().topDownFigureLayer(flag);
	}
}
