package epros.draw.gui.workflow.tabbedPane;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.SwingUtilities;

import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.line.shape.ManhattanLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 图形层级
 * 
 * @author fuzhh
 * 
 */
public class JecnGraphicsLevel {
	/** 面板 */
	private JecnDrawDesktopPane workflow;

	/**
	 * 
	 * 构造函数
	 * 
	 * @param workflow
	 *            JecnDrawDesktopPane 画图面板
	 */
	public JecnGraphicsLevel(JecnDrawDesktopPane workflow) {
		this.workflow = workflow;
	}

	/**
	 * 两图形的层级互换
	 * 
	 * @param oneFigure
	 *            第一个图形
	 * @param twoFigure
	 *            第二个图形
	 */
	public void exchangeLevel(JecnBaseFlowElementPanel oneFigure, JecnBaseFlowElementPanel twoFigure) {
		// 第一个图形的层级
		int oneOptLayer = oneFigure.getFlowElementData().getZOrderIndex();
		// 第一个图形的层级
		int twoOptLayer = twoFigure.getFlowElementData().getZOrderIndex();
		// 第一个图形的层级换为第二个图形的层级
		oneFigure.getFlowElementData().setZOrderIndex(twoOptLayer);
		// 第二个图形的层级换为第一个层级
		twoFigure.getFlowElementData().setZOrderIndex(oneOptLayer);
	}

	/**
	 * 上下移动层级方法
	 * 
	 * @param flag
	 *            ture为上移 false下移
	 */
	public void fluctuationFigureLayer(boolean flag) {
		int value = 2;
		// 判断是否只选择一个图形
		if (currentSelectElement().size() == 1) {
			// 获取选择的图形
			JecnBaseFlowElementPanel flowElement = currentSelectElement().get(0);
			// 选中图形的层级
			int optLayer = flowElement.getFlowElementData().getZOrderIndex();
			// 接收图形的交集
			Set<JecnBaseFlowElementPanel> overlapFigureList = figureOverlapList(flowElement);
			if (overlapFigureList.size() > 0) {
				JecnBaseFlowElementPanel optElement = null;
				int index = -1;
				if (flag) {
					for (JecnBaseFlowElementPanel figureElement : overlapFigureList) {
						int layer = figureElement.getFlowElementData().getZOrderIndex();
						if (optLayer < layer) {
							if (index == -1) {
								index = layer;
								optElement = figureElement;
							} else if (index > layer) {
								index = layer;
								optElement = figureElement;
							}
						}
					}
				} else {
					for (JecnBaseFlowElementPanel figureElement : overlapFigureList) {
						int layer = figureElement.getFlowElementData().getZOrderIndex();
						if (optLayer > layer) {
							if (index == -1) {
								index = layer;
								optElement = figureElement;
							} else if (index < layer) {
								index = layer;
								optElement = figureElement;
							}
						}
					}
				}
				if (optElement != null && index != -1) {
					// // 创建记录撤销恢复的流程元素数据的对象
					JecnUndoRedoData undoData = new JecnUndoRedoData();
					// // 记录操作前数据
					undoData.recodeFlowElement(flowElement);
					undoData.recodeFlowElement(optElement);
					if (flag) {
						for (JecnBaseFlowElementPanel baseFlowElement : getAllFigureList()) {
							int baseLayer = baseFlowElement.getFlowElementData().getZOrderIndex();
							if (baseLayer > optLayer && baseFlowElement.getFlowElementData().getZOrderIndex() <= index) {
								baseFlowElement.getFlowElementData().setZOrderIndex(baseLayer - value);
								if (baseFlowElement instanceof JecnBaseManhattanLinePanel) {
									setLinePanelLayer(baseFlowElement, baseLayer - value);
								} else {
									// 设置当前流程元素层级
									baseFlowElement.reSetFlowElementZorder();
								}
							}
						}
						// 第一个图形的层级换为第二个图形的层级
						flowElement.getFlowElementData().setZOrderIndex(index);

						if (flowElement instanceof JecnBaseManhattanLinePanel) {
							setLinePanelLayer(flowElement, index);
						} else {
							flowElement.reSetFlowElementZorder();
						}
					} else {
						for (JecnBaseFlowElementPanel baseFlowElement : getAllFigureList()) {
							int baseLayer = baseFlowElement.getFlowElementData().getZOrderIndex();
							if (baseLayer >= index) {
								baseFlowElement.getFlowElementData().setZOrderIndex(baseLayer + value);
								if (baseFlowElement instanceof JecnBaseManhattanLinePanel) {
									setLinePanelLayer(baseFlowElement, baseLayer + value);
								} else {
									// 设置当前流程元素层级
									baseFlowElement.reSetFlowElementZorder();
								}
							}
						}
						// 选中图形的层级
						flowElement.getFlowElementData().setZOrderIndex(index);

						if (flowElement instanceof JecnBaseManhattanLinePanel) {
							setLinePanelLayer(flowElement, index);
						} else {
							flowElement.reSetFlowElementZorder();
						}
						// 面板最大层级加一
						workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + value);
					}
					// 创建记录撤销恢复的流程元素数据的对象
					JecnUndoRedoData redoData = new JecnUndoRedoData();
					redoData.recodeFlowElement(flowElement);
					redoData.recodeFlowElement(optElement);

					// 记录这次操作的数据集合
					JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);

					workflow.getUndoRedoManager().addEdit(unredoProcess);
					workflow.repaint();
				}
			}
		}
	}

	/**
	 * 设置连接线层级
	 * 
	 * @author fuzhh Aug 23, 2012
	 * @param baseFlowElement
	 *            连接线
	 * @param lineLayer
	 *            层级
	 */
	public void setLinePanelLayer(JecnBaseFlowElementPanel baseFlowElement, int lineLayer) {
		JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) baseFlowElement;
		if (linePanel instanceof ManhattanLine) {// 带箭头连接线
			if (linePanel == null) {
				return;
			}
			ManhattanLine manLine = (ManhattanLine) linePanel;

			workflow.setLayer(manLine.arrowPanel, lineLayer);
			if (manLine.startArrow != null) {
				workflow.setLayer(manLine.startArrow, lineLayer);
			}
		}
		if (linePanel.getTextPanel() != null) {// 如果连接线存在编辑框，设置编辑框层级
			workflow.setLayer(linePanel.getTextPanel(), lineLayer);
		}
		for (LineSegment lineSegment : linePanel.getLineSegments()) {
			workflow.setLayer(lineSegment, lineLayer);
		}
	}

	/**
	 * 置于顶层和底层方法
	 * 
	 * @param flag
	 *            true顶层 false底层
	 */
	public void topDownFigureLayer(boolean flag) {

		// 判断是否只选择一个图形
		if (currentSelectElement().size() == 1) {
			// 操作前数据
			JecnUndoRedoData undoData = null;
			// 操作后数据
			JecnUndoRedoData redoData = null;

			// 获取选择的图形
			JecnBaseFlowElementPanel flowElement = currentSelectElement().get(0);
			// 选中图形的层级
			int optLayer = flowElement.getFlowElementData().getZOrderIndex();
			if (flag) {// true顶层
				boolean layerFlag = false;
				// 接收图形的交集
				Set<JecnBaseFlowElementPanel> overlapFigureList = figureOverlapList(flowElement);

				// 如果面板元素小于 500
				List<JecnBaseFlowElementPanel> elementPanels = getAllFigureList();
				if (elementPanels.size() <= 500) {
					overlapFigureList = new HashSet<JecnBaseFlowElementPanel>();
					overlapFigureList.addAll(elementPanels);
				}
				for (JecnBaseFlowElementPanel baseFlowElement : overlapFigureList) {
					// 获取图形的层级
					int figureLayer = baseFlowElement.getFlowElementData().getZOrderIndex();
					if (figureLayer > optLayer) {
						layerFlag = true;
					}
				}
				if (layerFlag) {
					// 创建记录撤销恢复的流程元素数据的对象
					undoData = new JecnUndoRedoData();
					undoData.recodeFlowElement(flowElement);

					// 获取面板最大的层级
					flowElement.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
					if (flowElement instanceof JecnBaseManhattanLinePanel) {
						setLinePanelLayer(flowElement, workflow.getMaxZOrderIndex());
					} else {
						// 设置当前流程元素层级
						flowElement.reSetFlowElementZorder();
					}

					// 把面板最大层级加一
					workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
					// 创建记录撤销恢复的流程元素数据的对象
					redoData = new JecnUndoRedoData();
					redoData.recodeFlowElement(flowElement);
				}
				if (undoData == null || redoData == null) {
					return;
				}
				// 记录这次操作的数据集合
				JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);

				workflow.getUndoRedoManager().addEdit(unredoProcess);

			} else {// false底层
				setDownFigureLayer(flowElement);
			}
		}
	}

	/**
	 * 把传入的图形至于最底层,并且执行撤销恢复
	 * 
	 * @param flowElement
	 */
	public void setDownFigureLayer(JecnBaseFlowElementPanel flowElement) {
		if (flowElement == null) {
			return;
		}
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 执行置底处理
		setDownFigureLayerProcess(flowElement, undoData, redoData);

		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);

		workflow.getUndoRedoManager().addEdit(unredoProcess);
	}

	/**
	 * 把传入的图形至于最底层
	 * 
	 * @param flowElement
	 *            JecnBaseFlowElementPanel
	 */
	public void setDownFigureLayerProcess(JecnBaseFlowElementPanel flowElement, JecnUndoRedoData undoData,
			JecnUndoRedoData redoData) {
		if (flowElement == null || undoData == null || redoData == null) {
			return;
		}

		// 判断是否需要重置 传入图形的层级
		boolean layerFlag = false;
		// 获取传入图形的层级
		int optLayer = flowElement.getFlowElementData().getZOrderIndex();
		// 接收图形的交集
		Set<JecnBaseFlowElementPanel> overlapFigureList = figureOverlapList(flowElement);

		for (JecnBaseFlowElementPanel baseFlowElement : overlapFigureList) {
			// 获取图形的层级
			int figureLayer = baseFlowElement.getFlowElementData().getZOrderIndex();
			// 循环所有图形 如果当前图行的层级小于传入图形的层级 ， 当前图形的层级数加一
			if (figureLayer < optLayer) {
				undoData.recodeFlowElement(baseFlowElement);
				// 获取面板最大的层级
				baseFlowElement.getFlowElementData().setZOrderIndex(figureLayer + 2);
				if (baseFlowElement instanceof JecnBaseManhattanLinePanel) {
					setLinePanelLayer(baseFlowElement, figureLayer + 2);
				} else {
					// 设置当前流程元素层级
					baseFlowElement.reSetFlowElementZorder();
				}
				redoData.recodeFlowElement(baseFlowElement);
				layerFlag = true;
			}

		}
		if (layerFlag) {
			undoData.recodeFlowElement(flowElement);
			// 设置层级为0
			flowElement.getFlowElementData().setZOrderIndex(0);
			if (flowElement instanceof JecnBaseManhattanLinePanel) {
				setLinePanelLayer(flowElement, 0);
			} else {
				// 设置当前流程元素层级
				flowElement.reSetFlowElementZorder();
			}
			// 面板最大层级加一
			workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
			redoData.recodeFlowElement(flowElement);
		}
	}

	/**
	 * 
	 * 当前画图面板上获得与给定流程元素相交的流程元素集合
	 * 
	 * @param optElement
	 *            选中的图形
	 * @return 存在交集的List
	 */
	public Set<JecnBaseFlowElementPanel> figureOverlapList(JecnBaseFlowElementPanel optElement) {

		// 保存交集的List
		Set<JecnBaseFlowElementPanel> overlapFigureList = new HashSet<JecnBaseFlowElementPanel>();

		// 获取选中图形的Rectangle
		Rectangle optBounds = optElement.getBounds();
		// 当前点击线段小线段的List
		List<LineSegment> optLineList = null;
		// 判断选择的图形是否是线
		if (optElement instanceof JecnBaseManhattanLinePanel) {
			// 如果为线 获取小线段的 panel
			JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) optElement;
			optLineList = manhattanLine.getLineSegments();
		}
		for (JecnBaseFlowElementPanel figureElement : getAllFigureList()) {
			// 去掉点击的图形
			if (optElement == figureElement) {
				continue;
			}
			Rectangle tempRect = null;
			// 线段小线段的List
			List<LineSegment> lineList = null;
			// 判断是否为线
			if (figureElement instanceof JecnBaseManhattanLinePanel) {
				// 如果为线 获取小线段的 panel
				JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) figureElement;
				lineList = manhattanLine.getLineSegments();
			}
			if (optLineList != null) {
				for (LineSegment optLineSegment : optLineList) {
					Rectangle allBounds = figureElement.getBounds();
					Rectangle optLineBounds = optLineSegment.getBounds();
					tempRect = SwingUtilities.computeIntersection(optLineBounds.x, optLineBounds.y,
							optLineBounds.width, optLineBounds.height, allBounds);
					// 矩形边框是否存在
					if (!isExits(tempRect)) {
						overlapFigureList.add(figureElement);
						break;
					}
				}
			} else if (optLineList != null && lineList != null) {
				for (LineSegment optLineSegment : optLineList) {
					Rectangle optLineBounds = optLineSegment.getBounds();
					for (LineSegment lineSegment : lineList) {
						Rectangle lineBounds = lineSegment.getBounds();
						tempRect = SwingUtilities.computeIntersection(optLineBounds.x, optLineBounds.y,
								optLineBounds.width, optLineBounds.height, lineBounds);
						// 矩形边框是否存在
						if (!isExits(tempRect)) {
							overlapFigureList.add(figureElement);
						}
					}
				}
			} else if (lineList != null) {
				for (LineSegment lineSegment : lineList) {
					Rectangle lineBounds = lineSegment.getBounds();
					tempRect = SwingUtilities.computeIntersection(optBounds.x, optBounds.y, optBounds.width,
							optBounds.height, lineBounds);
					// 矩形边框是否存在
					if (!isExits(tempRect)) {
						overlapFigureList.add(figureElement);
					}
				}
			} else {
				Rectangle allBounds = figureElement.getBounds();
				tempRect = SwingUtilities.computeIntersection(optBounds.x, optBounds.y, optBounds.width,
						optBounds.height, allBounds);
				// 矩形边框是否存在
				if (!isExits(tempRect)) {
					overlapFigureList.add(figureElement);
				}
			}

		}

		// 添加当前元素
		overlapFigureList.add(optElement);
		return overlapFigureList;
	}

	private List<JecnBaseFlowElementPanel> gerResetOrderElementPanel(Set<JecnBaseFlowElementPanel> overlapFigureList) {
		List<JecnBaseFlowElementPanel> listFigure = new ArrayList<JecnBaseFlowElementPanel>();
		listFigure.addAll(overlapFigureList);
		// 面板图形层级重新排序
		JecnFlowTemplate.sortAllFlowElementCloneList(listFigure);

		return listFigure;
	}

	/**
	 * 保存时重置层级
	 */
	public void saveResetLayer() {
		// 面板图形层级重新排序
		JecnFlowTemplate.sortAllFlowElementCloneList(getAllFigureList());
		// 重置图形层级
		for (int i = 0; i < getAllFigureList().size(); i++) {
			JecnBaseFlowElementPanel flowElement = getAllFigureList().get(i);
			flowElement.getFlowElementData().setZOrderIndex(i * 2);
		}
		// 重置面板最大层级
		workflow.setMaxZOrderIndex(getAllFigureList().size() * 2);
	}

	/**
	 * 保存时重置层级
	 */
	public void saveLocalResetLayer() {
		// 面板图形层级重新排序
		JecnFlowTemplate.sortAllFlowElementCloneList(getAllFigureList());
		// 重置图形层级
		for (int i = 0; i < getAllFigureList().size(); i++) {
			JecnBaseFlowElementPanel flowElement = getAllFigureList().get(i);
			flowElement.getFlowElementData().setZOrderIndex(i * 2);
			flowElement.reSetFlowElementZorder();
		}
		// 重置面板最大层级
		workflow.setMaxZOrderIndex(getAllFigureList().size() * 2);
	}

	/**
	 * 判断返回的Rectangle是否为0
	 * 
	 * @param tempRect
	 *            传入的矩形边框
	 * @return 是否重叠 true不重叠 false重叠
	 */
	private boolean isExits(Rectangle tempRect) {
		if (tempRect.x == 0 && tempRect.y == 0 && tempRect.width == 0 && tempRect.height == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 获取画图面板上所有流程元素集合
	 * 
	 * @return getAllFigureList 流程元素集合
	 */
	private List<JecnBaseFlowElementPanel> getAllFigureList() {
		return this.workflow.getPanelList();
	}

	/**
	 * 
	 * 获取画图面板上选中流程元素集合
	 * 
	 * @return List<JecnBaseFlowElementPanel> 选中流程元素集合
	 */
	private List<JecnBaseFlowElementPanel> currentSelectElement() {
		return this.workflow.getCurrentSelectElement();
	}
}
