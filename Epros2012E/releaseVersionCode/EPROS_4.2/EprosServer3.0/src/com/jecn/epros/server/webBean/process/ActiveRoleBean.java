package com.jecn.epros.server.webBean.process;

import java.util.List;

import com.jecn.epros.server.webBean.RoleBean;

public class ActiveRoleBean {
	// 流程名称
	private String flowName;
	// 角色对应活动的List
	private List<RoleBean> roleList;
	// 图片存储路径
	private String imgPath;

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public List<RoleBean> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<RoleBean> roleList) {
		this.roleList = roleList;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

}
