package com.jecn.epros.service.process;

import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.common.relatedfiles.RelatedFileBean;
import com.jecn.epros.domain.process.*;
import com.jecn.epros.domain.process.kpi.*;
import com.jecn.epros.domain.process.processFile.OperationFile;
import com.jecn.epros.domain.process.processFile.ProcessMapFileExplain;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface ProcessService {
    /**
     * 流程树节点展开
     *
     * @param map
     * @return
     */
    List<TreeNode> findChildFlows(Map<String, Object> map);

    ProcessBaseAttribute findBaseInfoBean(Map<String, Object> map);

    List<FlowKPIBean> findKpiNames(Map<String, Object> map);

    KpiSearchResultData findKpiValuesByDate(Map<String, Object> map);

    List<KPIInfo> findKpiInfos(Map<String, Object> map);

    void saveKpiValues(Map<String, Object> map, List<KPIValuesBean> kpiValuesBeans);

    List<SearchResultBean> searchProcess(Map<String, Object> map, SearchBean searchBean);

    /**
     * 流程文件
     *
     * @param map
     * @return
     */
    public OperationFile getShowProcessFile(Map<String, Object> map);

    /**
     * 流程清单
     */
    public Inventory getInventory(Map<String, Object> params);

    /**
     * 流程架构 文件说明
     *
     * @param map
     * @return
     */
    ProcessMapFileExplain getProcessMapFileExplain(Map<String, Object> map);

    /**
     * 流程搜索
     *
     * @param map
     * @return
     */
    public List<FlowSearchResultBean> searchFlow(Map<String, Object> map);

    /**
     * 流程搜索 总数
     *
     * @param map
     * @return
     */
    public int searchFlowTotal(Map<String, Object> map);

    /**
     * 流程搜索
     *
     * @param map
     * @return
     */
    public List<FlowMapSearchResultBean> searchFlowMap(Map<String, Object> map);

    /**
     * 流程搜索 总数
     *
     * @param map
     * @return
     */
    public int searchFlowMapTotal(Map<String, Object> map);

    /**
     * 流程架构 相关文件
     *
     * @param map
     * @return
     */
    public List<RelatedFileBean> findFlowMapRelatedFile(Map<String, Object> map);

    /**
     * 活动明细
     *
     * @param map
     * @return
     */
    public FlowActiveDetainBean findFlowActiveDetainBean(Map<String, Object> map);

    /**
     * 流程操作模板
     *
     * @param map
     * @return
     */
    List<FlowOperationModel> getProcessActiveBeanList(Map<String, Object> map);

    List<Resource> fetchProcessesSupportResources(Map<String, Object> paramMap);

    ProcessArchitectureCard getProcessArchitectureCard(Map<String, Object> paramMap);

    void updateKpiValue(Map<String, Object> map, KPIValuesBean kpiValue);

    FlowKpiName getKpiNameByMap(Map<String, Object> map);

    Inventory showCheckout(Long flowId);

    Inventory showCheckoutRole(Long flowId);

    List<TreeNode> findInventedFlows(Map<String, Object> map);
}
