package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnSustainToolConnTBean;
import com.jecn.epros.server.common.IBaseDao;
/**
 * 与支持工具关联临时表
 * @Time 2014-10-24
 *
 */
public interface ISustainToolConnTDao extends IBaseDao<JecnSustainToolConnTBean, Long> {

	/***
	 * 根据关联ID、关联类型获取与支持工具关联临时表数据
	 * @param relatedId关联ID
	 * @param relatedType 关联类型
	 * @return
	 * @throws Exception
	 */
	public List<JecnSustainToolConnTBean> getSustainToolConnTList(Long relatedId,int relatedType) throws Exception;
	
}
