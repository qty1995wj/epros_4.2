package epros.draw.gui.workflow.tabbedPane;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.aid.JecnElemRelatedActivePanel;
import epros.draw.util.JecnUIUtil;

/**
 * 特殊工具栏，Button
 * 
 * @author Administrator
 * 
 */
public class LinkButton extends JButton {
	/** 装载按钮容器 */
	protected JToolBar toolBar = null;
	private JecnElemRelatedActivePanel pPanel;
	private JecnBaseFlowElementPanel flowElementPanel;
	private JecnActiveData active;

	public LinkButton(JecnElemRelatedActivePanel pPanel, JecnBaseFlowElementPanel flowElementPanel,
			JecnActiveData active) {
		this.setText(active.getActivityNum());
		this.pPanel = pPanel;
		this.flowElementPanel = flowElementPanel;
		this.active = active;
		initComponents();
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 图片
		// this.setIcon(null);
		// 不显示焦点状态
		this.setFocusPainted(false);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.add(this);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pPanel.actionPerformed(flowElementPanel, active);
			}
		});
	}
}
