//流程审视优化统计表


// 流程访问数统计下载
function processKPIFollowReportDownloadExcel(){
     //责任部门ID 最多20个拿逗号分隔
	 var orgId=document.getElementById("orgDutyId_access_id").value;
	 //流程地图ID 最多20个拿逗号分隔
	 var processMapId=document.getElementById("processMapId_access_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;

	 //开始时间与结束时间不填
	if(startTime==""||endTime==""){
		alert(i_startTimeAndEndTimeMustNotNull);
		return;
	}
	
	//结束时间小于开始时间
	if(startTime>=endTime)
	{
		alert(i_startTimeIsLessEndTime);
		return;
	}
	
	var m = spaceMonths(startTime,endTime);
    if(m>60)//多于五年
    {
    	alert(i_intervalTimeCannotBigThan+5+i_year);
    	return;
    }
	 
    //判断部门id与流程地图id是否为空
	 if(((orgId==-1||orgId=="")&&(processMapId==-1||processMapId=="")))
	{
		 //必须选择责任部门或流程地图！
		 alert(i_mustSelectOrgOrProcessMap);
		 return;
	}
	
	document.getElementById("endTimeSub").value=endTime;
	document.getElementById("startTimeSub").value=startTime;
	
	var processKPIFollowForm=document.getElementById("processKPIFollowForm");
	
	processKPIFollowForm.action="downloadProcessKPIFollow.action";
	processKPIFollowForm.submit();
	
}

