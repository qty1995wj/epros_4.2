package epros.designer.gui.rule.mode;

import java.util.Vector;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;

public class EditRuleModeTitleDialog extends RuleModeTitelDialog {
	private JecnTable titleTable = null;

	public EditRuleModeTitleDialog(JecnTable titleTable) {
		// 标题"加入标题"
		this.setTitle(JecnProperties.getValue("editTitle"));
		this.titleTable = titleTable;
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		Vector<Vector<String>> vs = ((JecnTableModel) this.titleTable
				.getModel()).getDataVector();
		int i = this.titleTable.getSelectedRow();
		String oldName = (String) ((JecnTableModel) this.titleTable.getModel())
				.getValueAt(i, 1);
		if (vs != null) {
			for (Vector<String> v : vs) {
				if (name.equals(oldName)) {
					continue;
				}
				if (name.equals(v.get(1))) {
					return true;
				}

			}
		}
		return false;
	}

	@Override
	public boolean validateTitleType(String titleType) {
		Vector<Vector<String>> vs = ((JecnTableModel) this.titleTable
				.getModel()).getDataVector();
		int i = this.titleTable.getSelectedRow();
		String oldTitleType = (String) ((JecnTableModel) this.titleTable
				.getModel()).getValueAt(i, 2);

		if (vs != null) {
			for (Vector<String> v : vs) {
				if (titleType.equals(v.get(2)) && !oldTitleType.equals(v.get(2))) {
					return true;
				}

			}
		}
		return false;
	}

}
