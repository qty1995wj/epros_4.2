package com.jecn.epros.server.action.web.login.ad.henansiwei;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * DES 解密
 * 
 * @ClassName: CryptoTools
 * @author ZXH
 */
public class SiWeiDesKey {
	// 设置密钥
	private static final byte[] DESkey = { (byte) 0x4A, (byte) 0x45, (byte) 0x43, (byte) 0x4E, (byte) 0xF3,
			(byte) 0xF1, (byte) 0xF2, (byte) 0xF5 };
	// 设置向量
	private static final byte[] DESIV = { (byte) 0xC2, (byte) 0xD3, (byte) 0xD5, (byte) 0xC7, (byte) 0xF1, (byte) 0xF8,
			(byte) 0xF2, (byte) 0xF0 };

	private static final String DES_ALGORITHM = "DES/CBC/PKCS5Padding";

	// 加密算法的参数接口，IvParameterSpec是它的一个实现
	static AlgorithmParameterSpec iv = null;

	private static Key key = null;

	public SiWeiDesKey() throws Exception {
		// 设置密钥参数
		DESKeySpec keySpec = new DESKeySpec(DESkey);
		// 设置向量
		iv = new IvParameterSpec(DESIV);
		// 获得密钥工厂
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		// 得到密钥对象
		key = keyFactory.generateSecret(keySpec);
	}

	/**
	 * 解密
	 * 
	 * @Title: decode
	 * @param data
	 * @return
	 * @throws Exception
	 * @return String
	 * @date 2015-8-19 下午03:26:18
	 * @throws
	 */
	public String decode(String data) throws Exception {
		Cipher deCipher = Cipher.getInstance(DES_ALGORITHM);
		deCipher.init(Cipher.DECRYPT_MODE, key, iv);
		BASE64Decoder base64Decoder = new BASE64Decoder();
		byte[] pasByte = deCipher.doFinal(base64Decoder.decodeBuffer(data));
		return new String(pasByte, "UTF-8");
	}

	/**
	 * 加密
	 * 
	 * @param data
	 * @throws
	 */
	public String encode(String data) throws Exception {
		Cipher enCipher = Cipher.getInstance(DES_ALGORITHM);// 得到加密对象Cipher
		enCipher.init(Cipher.ENCRYPT_MODE, key, iv);// 设置工作模式为加密模式，给出密钥和向量
		byte[] pasByte = enCipher.doFinal(data.getBytes("utf-8"));
		BASE64Encoder base64Encoder = new BASE64Encoder();
		return base64Encoder.encode(pasByte);
	}

	public static void main(String[] args) throws Exception {
		// MutV6UBQquA=
		SiWeiDesKey tools = new SiWeiDesKey();
		// admin:Z+5pK6FYrCw=
		String key = tools.encode("admin");
		System.out.println("加密:" + key);
		//miaQFqAFwi8=
		System.out.println("解密:" + tools.decode("sTIkBYthU2Q="));
	}
}