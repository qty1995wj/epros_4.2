package com.jecn.epros.server.service.integration.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.integration.IJecnActivityTypeDao;
import com.jecn.epros.server.service.integration.IJecnActivityService;

@Transactional
public class JecnActivityServiceImpl extends
		AbsBaseService<JecnActiveTypeBean, Long> implements
		IJecnActivityService {
	/** 活动类别DAO接口 */
	private IJecnActivityTypeDao activityTypeDao;

	/**
	 * 添加或更新活动类型
	 * 
	 * @param activeTypeBean
	 * @throws Exception
	 */
	@Override
	public Long addActivityType(JecnActiveTypeBean activeTypeBean)
			throws Exception {
		Long typeId = null;
		if (activeTypeBean == null) {
			throw new IllegalAccessException("添加活动类别参数非法！");
		}
		if (activeTypeBean.getTypeId() == null) {// 添加
			typeId = activityTypeDao.save(activeTypeBean);
		} else {// 更新
			activityTypeDao.getSession().update(activeTypeBean);
		}
		return typeId;
	}

	/**
	 * 
	 * 获取指定名称的的个数
	 * 
	 * @param name
	 * @return int名称个数
	 * @throws Exception
	 */
	@Override
	public int findCountByName(String name) throws Exception {
		String sql = "SELECT COUNT(AT.TYPE_NAME) from JECN_ACTIVE_TYPE AT WHERE AT.TYPE_NAME=?";
		return activityTypeDao.countAllByParamsNativeSql(sql, name);
	}

	/**
	 * 获取活动类别集合
	 * 
	 * @return List<JecnActiveTypeBean>
	 * @throws Exception
	 */
	@Override
	public List<JecnActiveTypeBean> findJecnActiveTypeBeanList()
			throws Exception {
		return activityTypeDao.listAll();
	}

	/**
	 * 根据活动类别ID 删除类别
	 * 
	 * @param typeIds
	 * @throws Exception
	 */
	@Override
	public void delteFlowArributeType(List<Long> typeIds) throws Exception {
		String sql = "DELETE FROM JECN_ACTIVE_TYPE WHERE TYPE_ID IN "
				+ JecnCommonSql.getIds(typeIds);
		this.activityTypeDao.execteNative(sql);
	}

	public IJecnActivityTypeDao getActivityTypeDao() {
		return activityTypeDao;
	}

	public void setActivityTypeDao(IJecnActivityTypeDao activityTypeDao) {
		this.activityTypeDao = activityTypeDao;
	}
}
