package epros.designer.gui.rule.mode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class RuleModeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(RuleModeMenu.class);
	/** 新建目录 */
	private JMenuItem dirMenu;
	/** 新建模板 */
	private JMenuItem addMenu;
	/** 更新模板 */
	private JMenuItem updateMenu;
	/** 重命名 */
	private JMenuItem reNameMenu;
	/** 删除 */
	private JMenuItem delMenu;
	/** 刷新 */
	private JMenuItem refurbishMenu;
	/** 节点移动 */
	private JMenuItem dragNode;
	/** 节点排序 */
	private JMenuItem nodeSort;

	/** 树 */
	private JecnTree jTree = null;

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;

	private RuleModeManageDialog jecnManageDialog = null;

	public RuleModeMenu(JecnTree jTree, RuleModeManageDialog jecnManageDialog) {
		this.jTree = jTree;
		this.jecnManageDialog = jecnManageDialog;
		initCompotents();
		initAction();

	}

	private void initCompotents() {
		// "新建目录"
		dirMenu = new JMenuItem(JecnProperties.getValue("addDir"), new ImageIcon("images/menuImage/newRuleDir.gif"));
		// 新建模板
		addMenu = new JMenuItem(JecnProperties.getValue("addMode"));
		// 重命名
		reNameMenu = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon("images/menuImage/rename.gif"));
		// 删除
		delMenu = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon("images/menuImage/delete.gif"));
		// 刷新
		refurbishMenu = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon("images/menuImage/refresh.gif"));
		// 节点排序
		nodeSort = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon("images/menuImage/nodeSort.gif"));// 排序
		// 节点移动
		dragNode = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon("images/menuImage/nodeMove.gif"));
		// 更新模板
		updateMenu = new JMenuItem(JecnProperties.getValue("edit"));

		this.add(dirMenu);
		this.add(addMenu);
		this.addSeparator();
		this.add(reNameMenu);
		this.add(updateMenu);
		this.add(delMenu);
		this.add(refurbishMenu);
		this.add(nodeSort);
		this.add(dragNode);
		// this.addSeparator();
	}

	private void initAction() {

		dirMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				dirMenuActionPerformed(evt);
			}
		});

		addMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				addMenuActionPerformed(evt);
			}
		});

		reNameMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				eidtMenuActionPerformed(evt);
			}
		});

		delMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delMenuActionPerformed(evt);
			}
		});

		updateMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateMenuActionPerformed(evt);
			}
		});

		refurbishMenu.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				refurbishMenuActionPerformed(evt);
			}
		});

		nodeSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				nodeSortActionPerformed(evt);
			}
		});
		dragNode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				dragNodeActionPerformed(evt);
			}
		});
	}

	private void initItemAbled() {
		dirMenu.setEnabled(false);
		addMenu.setEnabled(false);
		reNameMenu.setEnabled(false);
		delMenu.setEnabled(false);
		refurbishMenu.setEnabled(false);
		nodeSort.setEnabled(false);
		dragNode.setEnabled(false);
		updateMenu.setEnabled(false);
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		initItemAbled();
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case ruleModeRoot:
			// 新建目录
			dirMenu.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 刷新
			refurbishMenu.setEnabled(true);
			break;
		// 目录节点
		case ruleModeDir:
			// 新建目录
			dirMenu.setEnabled(true);
			// 新建模板
			addMenu.setEnabled(true);
			// 重命名
			reNameMenu.setEnabled(true);
			// 删除
			delMenu.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 移动
			dragNode.setEnabled(true);
			// 刷新
			refurbishMenu.setEnabled(true);
			break;
		// 模板节点
		case ruleMode:
			// 删除
			delMenu.setEnabled(true);
			// 移动
			dragNode.setEnabled(true);
			// 更新模板
			updateMenu.setEnabled(true);
			break;
		default:
			initItemAbled();
			break;
		}

	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		initItemAbled();
		// 删除
		delMenu.setEnabled(true);
		// 移动
		dragNode.setEnabled(true);
	}

	/**
	 * 刷新 按钮
	 */
	public void refurbishMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		try {
			List<JecnTreeBean> list = ConnectionPool.getRuleModeAction().getChildRuleMode(
					selectNode.getJecnTreeBean().getId());
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("RuleModeMenu refurbishMenuActionPerformed is error", ex);
		}
	}

	/**
	 * 节点移动
	 * 
	 * @param evt
	 */
	public void dragNodeActionPerformed(ActionEvent evt) {
		if (listNode.size() > 0) {
			if (!JecnDesignerCommon.validateRepeatNodesName(listNode)) {
				return;
			}
			RuleModeMoveChooseDialog moveDialog = new RuleModeMoveChooseDialog(listNode, jTree);
			moveDialog.setFocusableWindowState(true);
			moveDialog.setVisible(true);

		}
	}

	/**
	 * 节点排序
	 * 
	 * @param evt
	 */
	public void nodeSortActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		RuleModeSortDialog ruleModeSortDialog = new RuleModeSortDialog(selectNode, jTree);
		ruleModeSortDialog.setVisible(true);

	}

	/**
	 * 添加目录
	 * 
	 * @param evt
	 */
	public void dirMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		AddRuleModelDirNameDialog addDir = new AddRuleModelDirNameDialog(selectNode, jTree);
		addDir.setVisible(true);
	}

	/**
	 * 添加模板
	 * 
	 * @param evt
	 */
	public void addMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		AddRuleModeDialog addDialog = new AddRuleModeDialog(selectNode, jTree);
		addDialog.setVisible(true);
	}

	/**
	 * 重命名
	 * 
	 * @param evt
	 */
	public void eidtMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		EditRuleModelDirNameDialog editRuleModelDirNameDialog = new EditRuleModelDirNameDialog(selectNode, jTree);
		editRuleModelDirNameDialog.setVisible(true);
	}

	/**
	 * 删除
	 * 
	 * @param evt
	 */
	public void delMenuActionPerformed(ActionEvent evt) {
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		if (JecnConstants.isMysql) {
			listIds = JecnTreeCommon.getAllChildIds(listNode);
		} else {
			for (JecnTreeNode node : listNode) {
				listIds.add(node.getJecnTreeBean().getId());
			}
		}
		if (listIds.size() == 0) {
			return;
		}

		try {
			ConnectionPool.getRuleModeAction().deleteRuleMode(listIds, JecnUtil.getUserId());
			/** 删除节点 */
			JecnTreeCommon.removeNodes(jTree, listNode);
			// 要删除的数据名称
			Vector<Vector<Object>> vs = ((DefaultTableModel) jecnManageDialog.getJecnTable().getModel())
					.getDataVector();
			for (int i = vs.size() - 1; i >= 0; i--) {
				Vector<Object> vob = vs.get(i);
				for (Long longId : listIds) {
					if (vob.get(1).toString().equals(longId.toString())) {
						((DefaultTableModel) jecnManageDialog.getJecnTable().getModel()).removeRow(i);
					}
				}
			}
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("deleteSuccess"));
		} catch (Exception e) {
			log.error("RuleModeMenu delMenuActionPerformed is error", e);
		}

	}

	/**
	 * 更新模板
	 * 
	 * @param evt
	 */
	public void updateMenuActionPerformed(ActionEvent evt) {
		if (selectNode == null) {
			return;
		}
		EditRuleModeDialog editDialog = new EditRuleModeDialog(selectNode.getJecnTreeBean(), jTree);
		editDialog.setVisible(true);
		if (!editDialog.isOperation) {
			return;
		}
		JecnTreeCommon.reNameNode(jTree, selectNode, editDialog.getNameField().getText().trim());
		JecnTreeCommon.selectNode(jTree, selectNode);
	}

	public void setListNode(List<JecnTreeNode> listNode) {
		this.listNode = listNode;
		if (listNode.size() == 1) {
			this.selectNode = listNode.get(0);
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

}
