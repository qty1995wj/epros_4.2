package epros.designer.service.process;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnLineSegmentDep;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.util.DrawCommon;

/**
 * 保存组织图
 * 
 * @author fuzhh
 * 
 */
public class JecnSaveOrg {

	/**
	 * 图形数据转换为 JecnFlowOrgImage数据
	 * 
	 * @param figureData
	 *            图形数据
	 * @param orgId
	 *            组织ID
	 * @param JecnFlowOrgImage
	 *            存储的Bean
	 */
	public static void getFlowOrgImageData(JecnFigureData figureData,
			Long orgId, JecnFlowOrgImage flowOrgImage) throws Exception {
		// 流程ID
		flowOrgImage.setOrgId(orgId);
		// 图形唯一标示
		flowOrgImage.setFigureImageId(figureData.getFlowElementUUID());
		// 线主键ID
		flowOrgImage.setFigureId(figureData.getFlowElementId());
		// 图形的类型
		flowOrgImage.setFigureType(figureData.getMapElemType().toString());
		// 元素名称
		String figureText = figureData.getFigureText();
		if (figureText != null
				&& MapElemType.PositionFigure == figureData.getMapElemType()) {
			figureText = figureText.trim();
		}
		flowOrgImage.setFigureText(figureText);
		// 开始点
		flowOrgImage.setPoLongx(Long.valueOf(figureData
				.getFigureDataCloneable().getX()));
		// 结束点
		flowOrgImage.setPoLongy(Long.valueOf(figureData
				.getFigureDataCloneable().getY()));
		// 图形的宽度
		flowOrgImage.setWidth(Long.valueOf(figureData.getFigureSizeX()));
		// 图形的高度
		flowOrgImage.setHeight(Long.valueOf(figureData.getFigureSizeY()));
		// 字体大小
		flowOrgImage.setFontSize(Long.valueOf(figureData.getFontSize()));
		// 图形层级
		flowOrgImage.setOrderIndex(Long.valueOf(figureData.getZOrderIndex()));
		// 图形的链接id
		flowOrgImage.setLinkOrgId(figureData.getDesignerFigureData()
				.getLinkId());
		// 图形的填充颜色
		getColorStr(figureData, flowOrgImage);
		// 图形填充效果

		// 字体颜色
		if (figureData.getFontColor() != null) {
			// 字体颜色
			String fontColorStr = String.valueOf(figureData.getFontColor()
					.getRed()
					+ ","
					+ figureData.getFontColor().getGreen()
					+ ","
					+ figureData.getFontColor().getBlue());
			flowOrgImage.setFontColor(fontColorStr);
		}
		// 字体加粗
		flowOrgImage.setFontBody(Long.valueOf(figureData.getFontStyle()));
		// 字体类型
		flowOrgImage.setFontType(figureData.getFontName());
		// 字体位置
		flowOrgImage.setFontPosition(String.valueOf(figureData
				.getFontPosition()));
		// 是否存在阴影
		if (figureData.isShadowsFlag()) {
			flowOrgImage.setHavaShadow(1L);
		} else {
			flowOrgImage.setHavaShadow(0L);
		}
		// 阴影颜色
		if (figureData.getShadowColor() != null) {
			// 阴影颜色
			String fontColorStr = String.valueOf(figureData.getShadowColor()
					.getRed()
					+ ","
					+ figureData.getShadowColor().getGreen()
					+ ","
					+ figureData.getShadowColor().getBlue());
			flowOrgImage.setShadowColor(fontColorStr);
		}
		// 旋转角度
		flowOrgImage.setCircumgyrate(String.valueOf(figureData
				.getCurrentAngle()));
		// 边框宽度
		flowOrgImage.setFrameBody(Long.valueOf(figureData.getBodyWidth()));
		// 边框样式
		flowOrgImage.setBodyLine(String.valueOf(figureData.getBodyStyle()));
		// 边框颜色
		if (figureData.getBodyColor() != null) {
			flowOrgImage.setBodyColor(String.valueOf(figureData.getBodyColor()
					.getRed()
					+ ","
					+ figureData.getBodyColor().getGreen()
					+ ","
					+ figureData.getBodyColor().getBlue()));
		}
		// 是否显示3D
		int is3Dshwo = 0;
		if (figureData.isFlag3D()) {
			is3Dshwo = 1;
		}
		flowOrgImage.setIs3DEffect(is3Dshwo);

		if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			if (commentData.getLineData() == null) {
				return;
			}
			// 注释框线条开始X
			flowOrgImage.setStartFigure(Long.valueOf((int) commentData
					.getStartX()));
			// 注释框线条开始Y
			flowOrgImage.setEndFigure(Long.valueOf((int) commentData
					.getStartY()));
		} else if (MapElemType.PositionFigure == figureData.getMapElemType()) {// 岗位
			flowOrgImage.setFigureNumberId(figureData.getDesignerFigureData()
					.getPosNumber());
		}
	}

	/**
	 * 获取线的数据
	 * 
	 * @param lineData
	 *            线数据
	 * @param orgId
	 *            组织ID
	 * @param flowOrgImage
	 *            存储的Bean
	 * @return
	 */
	public static JecnFlowOrgImage getFlowOrgImagetoLineData(
			JecnBaseLineData lineData, Long orgId, JecnFlowOrgImage flowOrgImage)
			throws Exception {
		// 流程ID
		flowOrgImage.setOrgId(orgId);
		// 线主键ID
		flowOrgImage.setFigureId(lineData.getFlowElementId());
		// 线的类型
		flowOrgImage.setFigureType(lineData.getMapElemType().toString());
		// 线的UIID
		flowOrgImage.setFigureImageId(lineData.getFlowElementUUID());
		// 线条样式
		flowOrgImage.setBodyLine(String.valueOf(lineData.getBodyStyle()));
		if (lineData.getMapElemType() == MapElemType.CommonLine
				|| lineData.getMapElemType() == MapElemType.ManhattanLine) { // 连接线
			JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) lineData;
			// 线的开始图形的主键ID
			flowOrgImage.setStartFigure(manhattanLineData.getStartId());
			// 线的结束图形的主键ID
			flowOrgImage.setEndFigure(manhattanLineData.getEndId());
			// 开始图形的UIID
			flowOrgImage.setStartFigureUUID(manhattanLineData
					.getStartFigureUUID());
			// 结束图形的UIID
			flowOrgImage.setEndFigureUUID(manhattanLineData.getEndFigureUUID());
			// 开始点的边界点
			int startLineType = DrawCommon.getLineType(manhattanLineData
					.getStartEditPointType());
			flowOrgImage.setPoLongx(Long.valueOf(startLineType));

			// 结束点的边界点
			int endLineType = DrawCommon.getLineType(manhattanLineData
					.getEndEditPointType());
			flowOrgImage.setPoLongy(Long.valueOf(endLineType));

			// 线的颜色
			flowOrgImage.setLineColor(String.valueOf(manhattanLineData
					.getBodyColor().getRed()
					+ ","
					+ manhattanLineData.getBodyColor().getGreen()
					+ ","
					+ manhattanLineData.getBodyColor().getBlue()));
			// 线的层级
			flowOrgImage.setOrderIndex(Long.valueOf(manhattanLineData
					.getZOrderIndex()));
			// 线条宽度
			flowOrgImage.setLineThickness(Integer.valueOf(manhattanLineData
					.getBodyWidth()));
			// 字体颜色
			if (manhattanLineData.getFontColor() != null) {
				// 字体颜色
				String fontColorStr = String.valueOf(manhattanLineData
						.getFontColor().getRed()
						+ ","
						+ manhattanLineData.getFontColor().getGreen()
						+ "," + manhattanLineData.getFontColor().getBlue());
				flowOrgImage.setFontColor(fontColorStr);
			}
			// 字体加粗
			flowOrgImage.setFontBody(Long.valueOf(manhattanLineData
					.getFontStyle()));
			// 字体类型
			flowOrgImage.setFontType(manhattanLineData.getFontName());
			// 字体大小
			flowOrgImage.setFontSize(Long.valueOf(manhattanLineData
					.getFontSize()));

			// 线数据
			flowOrgImage.setFigureText(manhattanLineData.getFigureText());
			// 小线段的数据
			List<JecnLineSegmentDep> listLineSegmentDep = new ArrayList<JecnLineSegmentDep>();
			// 添加小线段开始
			for (JecnLineSegmentData lineSegmentData : manhattanLineData
					.getLineSegmentDataList()) {
				if (lineSegmentData.getFlowLineID() == -1) {
					JecnLineSegmentDep lineSegmentDep = new JecnLineSegmentDep();
					// 给小线段赋值
					lineSegmentDep.setFigureId(manhattanLineData
							.getFlowElementId());
					// 线条开始点X
					lineSegmentDep.setStartX(Long.valueOf((int) lineSegmentData
							.getLineSegmentData().getStartXY().getX()));
					// 线条开始点Y
					lineSegmentDep.setStartY(Long.valueOf((int) lineSegmentData
							.getLineSegmentData().getStartXY().getY()));
					// 线条结束点X
					lineSegmentDep.setEndX(Long.valueOf((int) lineSegmentData
							.getLineSegmentData().getEndXY().getX()));
					// 线条结束点Y
					lineSegmentDep.setEndY(Long.valueOf((int) lineSegmentData
							.getLineSegmentData().getEndXY().getY()));
					listLineSegmentDep.add(lineSegmentDep);
				}
			}
			// 添加小线段集合
			flowOrgImage.setLineSegmentDepList(listLineSegmentDep);
		}
		return flowOrgImage;
	}

	/**
	 * 判断线是否需要修改
	 * 
	 * @param lineData
	 *            线数据
	 * @param oldLineData
	 *            内存中线的数据
	 * @param orgId
	 *            组织ID
	 * @param flowOrgImage
	 *            存储的Bean
	 * @return
	 */
	public static boolean isUpdateFlowLineData(JecnBaseLineData lineData,
			JecnBaseLineData oldLineData, Long orgId,
			JecnFlowOrgImage flowOrgImage) throws Exception {
		boolean isUpdate = false;
		// 线的UIID
		if (oldLineData.getFlowElementUUID() != null
				&& !oldLineData.getFlowElementUUID().equals(
						lineData.getFlowElementUUID())) {
			isUpdate = true;
		}
		if ((lineData.getMapElemType() == MapElemType.CommonLine || lineData
				.getMapElemType() == MapElemType.ManhattanLine)
				&& (oldLineData.getMapElemType() == MapElemType.CommonLine || oldLineData
						.getMapElemType() == MapElemType.ManhattanLine)) { // 连接线
			JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) lineData;
			JecnManhattanLineData oldManhattanLineData = (JecnManhattanLineData) oldLineData;
			if (!manhattanLineData.getStartFigureUUID().equals(
					oldManhattanLineData.getStartFigureUUID())) {// 开始图形的UIID
				isUpdate = true;
			} else if (!manhattanLineData.getEndFigureUUID().equals(
					oldManhattanLineData.getEndFigureUUID())) {// 结束图形的UIID
				isUpdate = true;
			} else if (manhattanLineData.getStartEditPointType() != oldManhattanLineData
					.getStartEditPointType()) { // 开始点的边界点
				isUpdate = true;
			} else if (manhattanLineData.getEndEditPointType() != oldManhattanLineData
					.getEndEditPointType()) { // 结束点的边界点
				isUpdate = true;
			} else if ((manhattanLineData.getBodyColor() != null
					&& oldManhattanLineData.getBodyColor() != null && manhattanLineData
					.getBodyColor().getRGB() != oldManhattanLineData
					.getBodyColor().getRGB())
					|| (manhattanLineData.getBodyColor() != null && oldManhattanLineData
							.getBodyColor() == null)
					|| (manhattanLineData.getBodyColor() == null && oldManhattanLineData
							.getBodyColor() != null)) {// 线的颜色
				isUpdate = true;
			} else if (manhattanLineData.getZOrderIndex() != oldManhattanLineData
					.getZOrderIndex()) {// 线的层级
				isUpdate = true;
			} else if (manhattanLineData.getBodyWidth() != oldManhattanLineData
					.getBodyWidth()) {// 线条宽度
				isUpdate = true;
			} else if (manhattanLineData.getBodyStyle() != manhattanLineData
					.getBodyStyle()) {// 线条类型
				isUpdate = true;
			} else if ((manhattanLineData.getFontName() != null && !manhattanLineData
					.getFontName().equals(oldManhattanLineData.getFontName()))
					|| (oldManhattanLineData.getFontName() != null && !oldManhattanLineData
							.getFontName().equals(
									manhattanLineData.getFontName()))) {// 字体类型
				isUpdate = true;
			} else if (manhattanLineData.getFigureText() != null
					&& !manhattanLineData.getFigureText().equals(
							oldManhattanLineData.getFigureText())) {// 线数据
				isUpdate = true;
			} else if ((manhattanLineData.getFontColor() != null
					&& oldManhattanLineData.getFontColor() != null && manhattanLineData
					.getFontColor().getRGB() != oldManhattanLineData
					.getFontColor().getRGB())
					|| (manhattanLineData.getFontColor() == null && oldManhattanLineData
							.getFontColor() != null)
					|| (manhattanLineData.getFontColor() != null && oldManhattanLineData
							.getFontColor() == null)) { // 字体颜色
				isUpdate = true;
			} else if (manhattanLineData.getFontStyle() != oldManhattanLineData
					.getFontStyle()) {// 字体加粗
				isUpdate = true;
			} else if (manhattanLineData.getFontName().equals(
					oldManhattanLineData.getFontName())) {// 字体类型
				isUpdate = true;
			} else if (manhattanLineData.getFontSize() != oldManhattanLineData
					.getFontSize()) {// 字体大小
				isUpdate = true;
			}
			// 添加小线段开始
			for (JecnLineSegmentData lineSegmentData : manhattanLineData
					.getLineSegmentDataList()) {
				if (lineSegmentData.getFlowLineID() == -1) {
					isUpdate = true;
				}
			}
		}
		if (isUpdate) {
			getFlowOrgImagetoLineData(lineData, orgId, flowOrgImage);
		}
		return isUpdate;
	}

	/**
	 * 判断是否需要修改
	 * 
	 * @param figureData
	 *            图形的数据
	 * @param oldFigureData
	 *            内存的数据
	 * @param orgId
	 *            组织ID
	 * @param flowOrgImage
	 *            需要修改的bean
	 * @return
	 */
	public static boolean isUpdateOrgElementData(JecnFigureData figureData,
			JecnFigureData oldFigureData, Long orgId,
			JecnFlowOrgImage flowOrgImage) throws Exception {
		boolean isUpdate = false;
		if (figureData.getFillType() == oldFigureData.getFillType()) {
			if (figureData.getFillType() == FillType.one) {
				if ((figureData.getFillColor() != null
						&& oldFigureData.getFillColor() != null && figureData
						.getFillColor().getRGB() != oldFigureData
						.getFillColor().getRGB())
						|| (oldFigureData.getFillColor() != null && figureData
								.getFillColor() == null)
						|| (oldFigureData.getFillColor() == null && figureData
								.getFillColor() != null)) {
					isUpdate = true;
				}
			} else if (figureData.getFillType() == FillType.two) {
				if ((figureData.getFillColor() != null
						&& oldFigureData.getFillColor() != null && figureData
						.getFillColor().getRGB() != oldFigureData
						.getFillColor().getRGB())
						|| (oldFigureData.getFillColor() != null && figureData
								.getFillColor() == null)
						|| (oldFigureData.getFillColor() == null && figureData
								.getFillColor() != null)) {
					isUpdate = true;
				}
				if ((figureData.getChangeColor() != null
						&& oldFigureData.getChangeColor() != null && figureData
						.getChangeColor().getRGB() != oldFigureData
						.getChangeColor().getRGB())
						|| (oldFigureData.getChangeColor() != null && figureData
								.getChangeColor() == null)
						|| (oldFigureData.getChangeColor() == null && figureData
								.getChangeColor() != null)) {
					isUpdate = true;
				}
			}
		} else {
			isUpdate = true;
		}

		if ((figureData.getFigureText() != null && !figureData.getFigureText()
				.equals(oldFigureData.getFigureText()))
				|| (oldFigureData.getFigureText() != null && !oldFigureData
						.getFigureText().equals(figureData.getFigureText()))) {
			isUpdate = true;
		} else if (figureData.getFigureDataCloneable().getX() != oldFigureData
				.getFigureDataCloneable().getX()) {
			isUpdate = true;
		} else if (figureData.getFigureDataCloneable().getY() != oldFigureData
				.getFigureDataCloneable().getY()) {
			isUpdate = true;
		} else if (figureData.getFigureSizeX() != oldFigureData
				.getFigureSizeX()) {
			isUpdate = true;
		} else if (figureData.getFigureSizeY() != oldFigureData
				.getFigureSizeY()) {
			isUpdate = true;
		} else if (figureData.getFontSize() != oldFigureData.getFontSize()) {
			isUpdate = true;
		} else if (figureData.getZOrderIndex() != oldFigureData
				.getZOrderIndex()) {
			isUpdate = true;
		} else if (figureData.getFillType() != oldFigureData.getFillType()) {
			isUpdate = true;
		} else if (figureData.getFontStyle() != oldFigureData.getFontStyle()) {
			isUpdate = true;
		} else if ((figureData.getFontName() != null && !figureData
				.getFontName().equals(oldFigureData.getFontName()))
				|| (oldFigureData.getFontName() != null && !oldFigureData
						.getFontName().equals(figureData.getFontName()))) {
			isUpdate = true;
		} else if (figureData.getFontPosition() != oldFigureData
				.getFontPosition()) {
			isUpdate = true;
		} else if (figureData.isShadowsFlag() != oldFigureData.isShadowsFlag()) {
			isUpdate = true;
		} else if ((figureData.getShadowColor() != null
				&& oldFigureData.getShadowColor() != null && figureData
				.getShadowColor().getRGB() != oldFigureData.getShadowColor()
				.getRGB())
				|| (oldFigureData.getShadowColor() != null && figureData
						.getShadowColor() == null)
				|| (oldFigureData.getShadowColor() == null && figureData
						.getShadowColor() != null)) {
			isUpdate = true;
		} else if (figureData.getCurrentAngle() != oldFigureData
				.getCurrentAngle()) {
			isUpdate = true;
		} else if (figureData.getBodyWidth() != oldFigureData.getBodyWidth()) {
			isUpdate = true;
		} else if (figureData.getBodyStyle() != oldFigureData.getBodyStyle()) {
			isUpdate = true;
		} else if ((figureData.getFontColor() != null
				&& oldFigureData.getFontColor() != null && figureData
				.getFontColor().getRGB() != oldFigureData.getFontColor()
				.getRGB())
				|| (figureData.getFontColor() == null && oldFigureData
						.getFontColor() != null)
				|| (figureData.getFontColor() != null && oldFigureData
						.getFontColor() == null)) { // 字体颜色
			isUpdate = true;
		} else if ((figureData.getBodyColor() != null
				&& oldFigureData.getBodyColor() != null && figureData
				.getBodyColor().getRGB() != oldFigureData.getBodyColor()
				.getRGB())
				|| (oldFigureData.getBodyColor() != null && figureData
						.getBodyColor() == null)
				|| (oldFigureData.getBodyColor() == null && figureData
						.getBodyColor() != null)) {
			isUpdate = true;
		} else if (figureData.isFlag3D() != oldFigureData.isFlag3D()) {
			isUpdate = true;
		} else if ((figureData.getDesignerFigureData().getLinkId() != null && !figureData
				.getDesignerFigureData().getLinkId().equals(
						oldFigureData.getDesignerFigureData().getLinkId()))
				|| (oldFigureData.getDesignerFigureData().getLinkId() != null && !oldFigureData
						.getDesignerFigureData().getLinkId().equals(
								figureData.getDesignerFigureData().getLinkId()))) {
			isUpdate = true;
		}
		if (MapElemType.CommentText == figureData.getMapElemType()
				&& MapElemType.CommentText == oldFigureData.getMapElemType()) {// 判断是否为注释框
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			JecnCommentLineData oldCommentData = (JecnCommentLineData) oldFigureData;
			if (commentData.getStartX() != oldCommentData.getStartX()) {
				isUpdate = true;
			} else if (commentData.getStartY() != oldCommentData.getStartY()) {
				isUpdate = true;
			}
		}

		if (isUpdate) {
			getFlowOrgImageData(figureData, orgId, flowOrgImage);
		}
		return isUpdate;
	}

	/**
	 * 获取图形的填充颜色
	 * 
	 * @param figureData
	 * @return
	 */
	public static void getColorStr(JecnFigureData figureData,
			JecnFlowOrgImage flowOrgImage) throws Exception {
		String backColor = "";
		// 图行颜色的类型none为未填充,one为单色效果,two为双色效果 默认未填充单色效果
		if (figureData.getFillType() != null) {
			switch (figureData.getFillType()) {
			case none:
				// 图形填充色效果标识
				flowOrgImage.setFillEffects(0);
				break;
			case one:
				// 图形填充色效果标识
				if (figureData.getFillColor() != null) {
					backColor = figureData.getFillColor().getRed() + ","
							+ figureData.getFillColor().getGreen() + ","
							+ figureData.getFillColor().getBlue();
				}
				flowOrgImage.setFillEffects(1);
				break;
			case two:
				// 图形填充色效果标识
				int colorChangType = 0;
				switch (figureData.getFillColorChangType()) {
				case topTilt:
					colorChangType = 1;
					break;
				case bottom:
					colorChangType = 2;
					break;
				case vertical:
					colorChangType = 3;
					break;
				case horizontal:
					colorChangType = 4;
					break;
				}
				// 双色填充
				if (figureData.getFillColor() != null
						&& figureData.getChangeColor() != null) {
					backColor = +figureData.getFillColor().getRed() + ","
							+ figureData.getFillColor().getGreen() + ","
							+ figureData.getFillColor().getBlue() + ","
							+ figureData.getChangeColor().getRed() + ","
							+ figureData.getChangeColor().getGreen() + ","
							+ figureData.getChangeColor().getBlue() + ","
							+ colorChangType;
				} else if (figureData.getFillColor() != null) {
					figureData.setFillType(FillType.one);
					getColorStr(figureData, flowOrgImage);
				} else {
					figureData.setFillType(FillType.none);
					getColorStr(figureData, flowOrgImage);
				}
				flowOrgImage.setFillEffects(2);
				break;
			}
		}
		flowOrgImage.setBGColor(backColor);
	}
}
