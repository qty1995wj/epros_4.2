package epros.draw.data.line;

import java.awt.Point;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnSystemData;
import epros.draw.util.DrawCommon;

/**
 * 分割线数据处理类
 * 
 * @author ZHANGXH
 * 
 */
public class JecnVHLineData extends JecnBaseLineData {
	/** 原始状态下：X坐标 */
	private double vhX = 0.0D;
	/** 原始状态下：Y坐标 */
	private double vhY = 0.0D;
	/** 线段方向 */
	private LineDirectionEnum lineDirectionEnum = null;

	public JecnVHLineData(MapElemType mapElemType,
			LineDirectionEnum lineDirectionEnum) {
		super(mapElemType);
		if (lineDirectionEnum == null) {
			// TODO
		}
		this.lineDirectionEnum = lineDirectionEnum;

		// 初始化图形属性
		JecnSystemData.initCurrFlowElementData(this);
	}

	/**
	 * 
	 * 横分割线设置Y坐标，竖分割线设置X坐标
	 * 
	 * @param point
	 *            Point 鼠标点击面板坐标
	 */
	public void setVHXY(Point point) {
		switch (lineDirectionEnum) {
		case horizontal:// 水平
			this.vhY = point.getY()
					/ JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
			break;
		case vertical:// 垂直
			this.vhX = point.getX()
					/ JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
			break;
		}
	}

	public LineDirectionEnum getLineDirectionEnum() {
		return lineDirectionEnum;
	}

	public void setLineDirectionEnum(LineDirectionEnum lineDirectionEnum) {
		this.lineDirectionEnum = lineDirectionEnum;
	}

	/**
	 * 
	 * 获取放大缩小后的横分割线的Y坐标
	 * 
	 * @return int 放大缩小后的横分割线的Y坐标
	 */
	public int getHYInt() {
		return DrawCommon.convertDoubleToInt(this.vhY
				* JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
	}

	/**
	 * 
	 * 获取放大缩小后的竖分割线的X坐标
	 * 
	 * @return int 放大缩小后的竖分割线的X坐标
	 */
	public int getVXInt() {
		return DrawCommon.convertDoubleToInt(this.vhX
				* JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
	}

	public double getVhX() {
		return vhX;
	}

	public void setVhX(double vhX) {
		this.vhX = vhX;
	}

	public double getVhY() {
		return vhY;
	}

	public void setVhY(double vhY) {
		this.vhY = vhY;
	}

	@Override
	public JecnVHLineData newInstance() {
		return new JecnVHLineData(mapElemType, lineDirectionEnum);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnVHLineData clone() {
		return (JecnVHLineData) this.currClone();
	}

	@Override
	/**
	 * 
	 * 
	 * 复制流程元素属性,参数不能为空
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(
			JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnVHLineData)
				|| !(destFlowElementData instanceof JecnVHLineData)) {
			return;
		}

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnVHLineData src = (JecnVHLineData) srcFlowElementData;
		JecnVHLineData dest = (JecnVHLineData) destFlowElementData;

		// 原始状态下：X坐标
		dest.setVhX(src.getVhX());
		// 原始状态下：Y坐标
		dest.setVhY(src.getVhY());
		// 线段方向
		dest.setLineDirectionEnum(src.getLineDirectionEnum());
	}
}
