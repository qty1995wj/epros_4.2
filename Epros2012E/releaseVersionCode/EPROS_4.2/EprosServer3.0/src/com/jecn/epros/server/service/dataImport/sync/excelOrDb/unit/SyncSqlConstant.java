package com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit;

import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

public class SyncSqlConstant {

	// ---------------------------------------项目 开始
	/** 获取部门临时表中项目ID */
	public final static String SELECT_TMP_JECN_IO_DEPT_PROJECT_ID = "SELECT PROJECTID FROM TMP_JECN_IO_DEPT T GROUP BY PROJECTID";
	// ---------------------------------------项目 结束

	// ---------------------------------------部门 开始
	/** 通用部门临时表所有数据 */
	public final static String DELETE_TMP_JECN_IO_DEPT_SQL = "DELETE FROM TMP_JECN_IO_DEPT";

	/** 待导入部门中在各自真实库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_DEPT = "UPDATE TMP_JECN_IO_DEPT"
			+ "   SET PRO_FLAG = 1"
			+ " WHERE EXISTS (SELECT 1 FROM JECN_FLOW_ORG G WHERE G.ORG_NUMBER = DEPT_NUM AND G.PROJECTID=:PROJECTID)";

	// -------------------------------------插入部门临时表中标识是插入的部门start
	/** 插入部门临时表中标识是插入的部门 */
	// oracle
	public final static String INSERT_JECN_FLOW_ORG_DEPT_ORACLE = "INSERT INTO JECN_FLOW_ORG(ORG_ID,PROJECTID,ORG_NUMBER,PREORG_NUMBER,ORG_RESPON,PER_ORG_ID,ORG_NAME,ORG_WIDTH,ORG_HEIGHT,SORT_ID,DEL_STATE)"
			+ " SELECT JECN_FLOW_ORG_SQUENCE.NEXTVAL AS ORG_ID, "
			+ "       :PROJECTID                    AS PROJECTID,"
			+ "       DEPT_NUM                      AS ORG_NUMBER,"
			+ "       P_DEPT_NUM                    AS PREORG_NUMBER,"
			+ "       NULL                          AS ORG_RESPON,"
			+ "       NULL                          AS PER_ORG_ID,"
			+ "       DEPT_NAME                     AS ORG_NAME,"
			+ "       NULL                          AS ORG_WIDTH,"
			+ "       NULL                          AS ORG_HEIGHT,"
			+ "       NULL                          AS SORT_ID,"
			+ "       0                             AS DEL_STATE"
			+ "  FROM TMP_JECN_IO_DEPT G WHERE G.PRO_FLAG=0";
	// sqlserver
	public final static String INSERT_JECN_FLOW_ORG_DEPT_SQLSERVER = "INSERT INTO JECN_FLOW_ORG(PROJECTID,ORG_NUMBER,PREORG_NUMBER,ORG_RESPON,PER_ORG_ID,ORG_NAME,ORG_WIDTH,ORG_HEIGHT,SORT_ID,DEL_STATE)"
			+ " SELECT "
			+ "       :PROJECTID                    AS PROJECTID,"
			+ "       DEPT_NUM                      AS ORG_NUMBER,"
			+ "       P_DEPT_NUM                    AS PREORG_NUMBER,"
			+ "       NULL                          AS ORG_RESPON,"
			+ "       NULL                          AS PER_ORG_ID,"
			+ "       DEPT_NAME                     AS ORG_NAME,"
			+ "       NULL                          AS ORG_WIDTH,"
			+ "       NULL                          AS ORG_HEIGHT,"
			+ "       NULL                          AS SORT_ID,"
			+ "       0                             AS DEL_STATE"
			+ "  FROM TMP_JECN_IO_DEPT G WHERE G.PRO_FLAG=0";
	// -------------------------------------插入部门临时表中标识是插入的部门end
	/** 同一部门下部门名称不能重复 相同父节点下不能存在相同的部门名称 */
	public final static String SELECT_DEPT_NAME_SAME_PDEPT = "SELECT DISTINCT TD.*"
			+ "  FROM TMP_JECN_IO_DEPT TD"
			+ "  JOIN TMP_JECN_IO_DEPT TD2 ON TD.NUM_ID <> TD2.NUM_ID"
			+ "                           AND TD.DEPT_NAME = TD2.DEPT_NAME"
			+ "                           AND TD.P_DEPT_NUM = TD2.P_DEPT_NUM";

	// -----------------------------------------------------------------------删除部门以及相关start
	/** 删除部门临时表中没有的部门对于的 流程与部门关联表(JECN_VIEW_FLOW_ORG) */
	public final static String DELETE_VIEW_FLOW_ORG_DEPT = "DELETE FROM JECN_VIEW_FLOW_ORG"
			+ " WHERE ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";

	/** 流程与部门关联临时表(JECN_VIEW_FLOW_ORG_T) 删除 */
	public final static String DELETE_VIEW_FLOW_ORG_T_DEPT = "DELETE FROM JECN_VIEW_FLOW_ORG_T"
			+ " WHERE ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";

	/** 流程责任部门(JECN_FLOW_RELATED_ORG) 删除 */
	public final static String DELETE_JECN_FLOW_RELATED_ORG_DEPT = "DELETE FROM JECN_FLOW_RELATED_ORG"
			+ " WHERE ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";

	/** 流程责任部门临时表(JECN_FLOW_RELATED_ORG_T) 删除 */
	public final static String DELETE_JECN_FLOW_RELATED_ORG_T_DEPT = "DELETE FROM JECN_FLOW_RELATED_ORG_T"
			+ " WHERE ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";

	/** 部门查阅权限关联表（JECN_ORG_ACCESS_PERMISSIONS） 删除 */
	public final static String DELETE_JECN_ORG_ACCESS_PERMISSIONS = "DELETE FROM JECN_ORG_ACCESS_PERMISSIONS"
			+ " WHERE ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";
	/** 部门查阅权限关联表临时表（JECN_ORG_ACCESS_PERM_T） 删除 */
	public final static String DELETE_JECN_ORG_ACCESS_PERM_T = "DELETE FROM JECN_ORG_ACCESS_PERM_T"
			+ " WHERE ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";
	/** 删除 岗位查阅权限 JECN_ACCESS_PERMISSIONS */
	public final static String DELETE_JECN_POS_ACCESS_PERM = "DELETE FROM JECN_ACCESS_PERMISSIONS"
			+ " WHERE FIGURE_ID NOT IN"
			+ "       (SELECT P.FIGURE_ID"
			+ "          FROM TMP_JECN_IO_POS TP, JECN_FLOW_ORG_IMAGE P"
			+ "         WHERE TP.POS_NUM = P.FIGURE_NUMBER_ID)"
			+ "   and figure_id in"
			+ "       (select t.figure_id"
			+ "          from jecn_flow_org_image t"
			+ "         inner join jecn_flow_org o on t.org_id = o.org_id"
			+ "                                   and o.projectid = :PROJECTID)";

	/** 删除 岗位查阅权限临时表 JECN_ACCESS_PERMISSIONS_T */
	public final static String DELETE_JECN_POS_ACCESS_PERM_T = "DELETE FROM JECN_ACCESS_PERMISSIONS_T"
			+ " WHERE FIGURE_ID NOT IN"
			+ "       (SELECT D.FIGURE_ID"
			+ "          FROM TMP_JECN_IO_POS TP, JECN_FLOW_ORG_IMAGE D"
			+ "         WHERE TP.POS_NUM = D.FIGURE_NUMBER_ID)"
			+ "   and figure_id in"
			+ "       (select t.figure_id"
			+ "          from jecn_flow_org_image t"
			+ "         inner join jecn_flow_org o on t.org_id = o.org_id"
			+ "                                   and o.projectid = :PROJECTID)";

	/** 文件表(JECN_FILE) 更新：部门ID(ORG_ID)=NULL */
	public final static String UPDATE_JECN_FILE_DEPT = "UPDATE JECN_FILE"
			+ "   SET ORG_ID = NULL"
			+ " WHERE ORG_ID IS NOT NULL"
			+ "  AND ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";
	/** 文件表(JECN_FILE_T) 更新：部门ID(ORG_ID)=NULL */
	public final static String UPDATE_JECN_FILE_T_DEPT = "UPDATE JECN_FILE_T"
			+ "   SET ORG_ID = NULL"
			+ " WHERE ORG_ID IS NOT NULL"
			+ "  AND ORG_ID NOT IN (SELECT D.ORG_ID"
			+ "                        FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "                       WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "                         AND D.PROJECTID = :PROJECTID)"
			+ "   AND ORG_ID IN"
			+ "       (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.PROJECTID = :PROJECTID)";

	/** 部门表（JECN_FLOW_ORG） 删除 */
	public final static String DELETE_JECN_FLOW_ORG = "DELETE FROM JECN_FLOW_ORG"
			+ " WHERE PROJECTID=:PROJECTID AND "
			+ "NOT EXISTS (SELECT 1"
			+ "          FROM TMP_JECN_IO_DEPT TD"
			+ "         WHERE TD.DEPT_NUM = ORG_NUMBER)";

	// -----------------------------------------------------------------------删除部门以及相关end

	/** 更新部门名称、上级部门ID */
	public final static String UPDATE_DEPT_PID_NAME = "UPDATE JECN_FLOW_ORG"
			+ "   SET ORG_NAME   = (SELECT T.TD_DEPT_NAME"
			+ "                       FROM (SELECT D.ORG_ID     AS D_ORG_ID,"
			+ "                                    TD.DEPT_NAME AS TD_DEPT_NAME"
			+ "                               FROM JECN_FLOW_ORG D"
			+ "                               LEFT JOIN TMP_JECN_IO_DEPT TD ON D.ORG_NUMBER ="
			+ "                                                                TD.DEPT_NUM"
			+ "                                                            AND D.PROJECTID ="
			+ "                                                                :PROJECTID"
			+ "                               LEFT JOIN JECN_FLOW_ORG D2 ON TD.P_DEPT_NUM ="
			+ "                                                             D2.ORG_NUMBER"
			+ "                                                         AND D2.PROJECTID ="
			+ "                                                             :PROJECTID"
			+ "                              WHERE D.PROJECTID = :PROJECTID) T"
			+ "                      WHERE T.D_ORG_ID = ORG_ID),"
			+ "       PER_ORG_ID = (SELECT T.TD_PER_ORG_ID"
			+ "                       FROM (SELECT D.ORG_ID AS D_ORG_ID,"
			+ "                                    CASE"
			+ "                                      WHEN TD.P_DEPT_NUM = '0' THEN"
			+ "                                       0"
			+ "                                      ELSE"
			+ "                                       D2.ORG_ID"
			+ "                                    END AS TD_PER_ORG_ID"
			+ "                               FROM JECN_FLOW_ORG D"
			+ "                               LEFT JOIN TMP_JECN_IO_DEPT TD ON D.ORG_NUMBER ="
			+ "                                                                TD.DEPT_NUM"
			+ "                                                            AND D.PROJECTID ="
			+ "                                                                :PROJECTID"
			+ "                               LEFT JOIN JECN_FLOW_ORG D2 ON TD.P_DEPT_NUM ="
			+ "                                                             D2.ORG_NUMBER"
			+ "                                                         AND D2.PROJECTID ="
			+ "                                                             :PROJECTID"
			+ "                              WHERE D.PROJECTID = :PROJECTID) T"
			+ "                      WHERE T.D_ORG_ID = ORG_ID)"
			+ " WHERE ORG_ID IN"
			+ "       (SELECT T.D_ORG_ID"
			+ "          FROM (SELECT D.ORG_ID AS D_ORG_ID,"
			+ "                       D.PER_ORG_ID AS D_PER_ORG_ID,"
			+ "                       D.ORG_NAME AS D_ORG_NAME,"
			+ "                       TD.DEPT_NAME AS TD_DEPT_NAME,"
			+ "                       CASE"
			+ "                         WHEN TD.P_DEPT_NUM = '0' THEN"
			+ "                          0"
			+ "                         ELSE"
			+ "                          D2.ORG_ID"
			+ "                       END AS TD_PER_ORG_ID"
			+ "                  FROM JECN_FLOW_ORG D"
			+ "                  LEFT JOIN TMP_JECN_IO_DEPT TD ON D.ORG_NUMBER = TD.DEPT_NUM"
			+ "                                               AND D.PROJECTID = :PROJECTID"
			+ "                  LEFT JOIN JECN_FLOW_ORG D2 ON TD.P_DEPT_NUM = D2.ORG_NUMBER"
			+ "                                            AND D2.PROJECTID = :PROJECTID"
			+ "                 WHERE D.PROJECTID = :PROJECTID) T"
			+ "         WHERE T.D_ORG_NAME <> T.TD_DEPT_NAME"
			+ "            OR T.D_PER_ORG_ID <> TD_PER_ORG_ID"
			+ "            OR T.D_PER_ORG_ID IS NULL)"
			+ "   AND PROJECTID = :PROJECTID";

	// ---------------------------------------部门 结束

	// ---------------------------------------岗位 开始
	/** 通用岗位临时表所有数据 */
	public final static String DELETE_TMP_JECN_IO_POS_SQL = "DELETE FROM TMP_JECN_IO_POS";
	/** 待导入岗位以中在各自真实库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_POS = "UPDATE TMP_JECN_IO_POS"
			+ "   SET PRO_FLAG = 1" + " WHERE EXISTS (SELECT 1"
			+ "          FROM JECN_FLOW_ORG_IMAGE G, JECN_FLOW_ORG D"
			+ "         WHERE G.FIGURE_NUMBER_ID = POS_NUM"
			+ "           AND G.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)";

	// -------------------------------------插入岗位临时表中标识是插入的岗位start
	/** 插入岗位临时表中标识是插入的岗位 */
	// oracle
	public final static String INSERT_JECN_FLOW_ORG_IMAGE_ORACLE = "INSERT INTO JECN_FLOW_ORG_IMAGE"
			+ "  (FIGURE_ID,"
			+ "   ORG_ID,"
			+ "   FIGURE_TYPE,"
			+ "   FIGURE_TEXT,"
			+ "   START_FIGURE,"
			+ "   END_FIGURE,"
			+ "   X_POINT,"
			+ "   Y_POINT,"
			+ "   WIDTH,"
			+ "   HEIGHT,"
			+ "   FONT_SIZE,"
			+ "   BGCOLOR,"
			+ "   FONTCOLOR,"
			+ "   LINK_ORG_ID,"
			+ "   LINECOLOR,"
			+ "   FONTPOSITION,"
			+ "   HAVESHADOW,"
			+ "   CIRCUMGYRATE,"
			+ "   BODYLINE,"
			+ "   BODYCOLOR,"
			+ "   ISERECT,"
			+ "   FIGURE_IMAGE_ID,"
			+ "   Z_ORDER_INDEX,"
			+ "   FONT_BODY,"
			+ "   FRAME_BODY,"
			+ "   FONT_TYPE,"
			+ "   SORT_ID,"
			+ "   FILE_ACCESS_PATH,"
			+ "   IS_3D_EFFECT,"
			+ "   FILL_EFFECTS,"
			+ "   LINE_THICKNESS,"
			+ "   FIGURE_NUMBER_ID,"
			+ "   ORG_NUMBER_ID)"
			+ "  SELECT JECN_FLOW_ORG_IMAGE_SQUENCE.NEXTVAL,"
			+ "         (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.ORG_NUMBER=G.DEPT_NUM AND D.PROJECTID=:PROJECTID),"
			+ "         :FIGURE_TYPE,"
			+ "         POS_NAME,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         100,"
			+ "         60,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         POS_NUM,"
			+ "         DEPT_NUM"
			+ "    FROM TMP_JECN_IO_POS G WHERE G.PRO_FLAG = 0";
	// sqlserver
	public final static String INSERT_JECN_FLOW_ORG_IMAGE_SQLSERVER = "INSERT INTO JECN_FLOW_ORG_IMAGE"
			+ "  ("
			+ "   ORG_ID,"
			+ "   FIGURE_TYPE,"
			+ "   FIGURE_TEXT,"
			+ "   START_FIGURE,"
			+ "   END_FIGURE,"
			+ "   X_POINT,"
			+ "   Y_POINT,"
			+ "   WIDTH,"
			+ "   HEIGHT,"
			+ "   FONT_SIZE,"
			+ "   BGCOLOR,"
			+ "   FONTCOLOR,"
			+ "   LINK_ORG_ID,"
			+ "   LINECOLOR,"
			+ "   FONTPOSITION,"
			+ "   HAVESHADOW,"
			+ "   CIRCUMGYRATE,"
			+ "   BODYLINE,"
			+ "   BODYCOLOR,"
			+ "   ISERECT,"
			+ "   FIGURE_IMAGE_ID,"
			+ "   Z_ORDER_INDEX,"
			+ "   FONT_BODY,"
			+ "   FRAME_BODY,"
			+ "   FONT_TYPE,"
			+ "   SORT_ID,"
			+ "   FILE_ACCESS_PATH,"
			+ "   IS_3D_EFFECT,"
			+ "   FILL_EFFECTS,"
			+ "   LINE_THICKNESS,"
			+ "   FIGURE_NUMBER_ID,"
			+ "   ORG_NUMBER_ID)"
			+ "  SELECT "
			+ "         (SELECT D.ORG_ID FROM JECN_FLOW_ORG D WHERE D.ORG_NUMBER=G.DEPT_NUM AND D.PROJECTID=:PROJECTID),"
			+ "         :FIGURE_TYPE,"
			+ "         POS_NAME,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         100,"
			+ "         60,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         NULL,"
			+ "         POS_NUM,"
			+ "         DEPT_NUM"
			+ "    FROM TMP_JECN_IO_POS G WHERE G.PRO_FLAG = 0";
	// -------------------------------------插入岗位临时表中标识是插入的岗位end

	// 更新【变更岗位】对应的岗位数据
	public final static String UPDATE_POS_UP = "UPDATE JECN_FLOW_ORG_IMAGE"
			+ "   SET ORG_ID = (SELECT DU.ORG_ID"
			+ "                   FROM JECN_FLOW_ORG DU"
			+ "                  WHERE DU.PROJECTID = :PROJECTID"
			+ "                    AND DU.ORG_NUMBER = FIGURE_NUMBER_ID)"
			+ " WHERE FIGURE_ID IN"
			+ "       (SELECT DISTINCT T.FIGURE_ID"
			+ "          FROM (SELECT DISTINCT TP.DEPT_NUM, P.FIGURE_ID, P.ORG_ID"
			+ "                  FROM TMP_JECN_IO_POS     TP,"
			+ "                       JECN_FLOW_ORG_IMAGE P,"
			+ "                       JECN_FLOW_ORG       D"
			+ "                 WHERE D.ORG_ID = P.ORG_ID"
			+ "                   AND D.PROJECTID = :PROJECTID"
			+ "                   AND P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "                   AND TP.PRO_FLAG = 1) T,"
			+ "               JECN_FLOW_ORG DU"
			+ "         WHERE T.DEPT_NUM = DU.ORG_NUMBER"
			+ "           AND DU.PROJECTID = :PROJECTID"
			+ "           AND T.ORG_ID <> DU.ORG_ID)";

	// -----------------------------------------------------------------------删除岗位以及相关start

	// ------------------------------------------------待删除部门下的岗位 start
	/** 岗位基本属性（JECN_POSITION_FIG_INFO） 删除 */
	public final static String DELETE_JECN_POSITION_FIG_INFO_DEL_DEPT = "DELETE JECN_POSITION_FIG_INFO"
			+ " WHERE FIGURE_ID IN"
			+ "       (SELECT P.FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P"
			+ "         WHERE P.ORG_ID IN (SELECT ORG_ID"
			+ "                              FROM JECN_FLOW_ORG"
			+ "                             WHERE ORG_NUMBER NOT IN"
			+ "                                   (SELECT DEPT_NUM FROM TMP_JECN_IO_DEPT)"
			+ "                               AND PROJECTID = :PROJECTID))";

	/** 岗位与岗位组关系表（JECN_POSITION_GROUP_R） 删除 */
	public final static String DELETE_JECN_POSITION_GROUP_R_DEL_DEPT = "DELETE JECN_POSITION_GROUP_R"
			+ " WHERE FIGURE_ID IN"
			+ "       (SELECT P.FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P"
			+ "         WHERE P.ORG_ID IN (SELECT ORG_ID"
			+ "                              FROM JECN_FLOW_ORG"
			+ "                             WHERE ORG_NUMBER NOT IN"
			+ "                                   (SELECT DEPT_NUM FROM TMP_JECN_IO_DEPT)"
			+ "                               AND PROJECTID = :PROJECTID))";

	/** 流程基本信息（JECN_FLOW_BASIC_INFO） 责任人类型（TYPE_RES_PEOPLE）==1岗位；0 人员 更新 */
	public final static String UPDATE_JECN_FLOW_BASIC_INFO_DEL_DEPT = "UPDATE JECN_FLOW_BASIC_INFO"
			+ "   SET RES_PEOPLE_ID = NULL, TYPE_RES_PEOPLE = NULL"
			+ " WHERE TYPE_RES_PEOPLE = 0"
			+ "   and RES_PEOPLE_ID is not null"
			+ "   AND NOT EXISTS (SELECT 1"
			+ "          FROM JECN_USER U, TMP_JECN_IO_USER TU"
			+ "         WHERE U.LOGIN_NAME = TU.USER_NUM"
			+ "           AND Res_People_Id = U.PEOPLE_ID)";

	/** 流程基本信息临时表(JECN_FLOW_BASIC_INFO_T) 责任人类型（TYPE_RES_PEOPLE）==0 更新 */
	public final static String UPDATE_JECN_FLOW_BASIC_INFO_T_DEL_DEPT = "UPDATE JECN_FLOW_BASIC_INFO_T"
			+ "   SET RES_PEOPLE_ID = NULL, TYPE_RES_PEOPLE = NULL"
			+ " WHERE TYPE_RES_PEOPLE = 0"
			+ "   and RES_PEOPLE_ID is not null"
			+ "   AND NOT EXISTS (SELECT 1"
			+ "          FROM JECN_USER U, TMP_JECN_IO_USER TU"
			+ "         WHERE U.LOGIN_NAME = TU.USER_NUM"
			+ "           AND Res_People_Id = U.PEOPLE_ID)";

	/** 流程角色与岗位关联表（PROCESS_STATION_RELATED）0：岗位 删除 */
	public final static String DELETE_JECN_PROCESS_STATION_RELATED_DEL_DEPT = "DELETE PROCESS_STATION_RELATED"
			+ " WHERE TYPE = 0"
			+ "   AND FIGURE_POSITION_ID IN"
			+ "       (SELECT P.FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P"
			+ "         WHERE P.ORG_ID IN (SELECT ORG_ID"
			+ "                              FROM JECN_FLOW_ORG"
			+ "                             WHERE ORG_NUMBER NOT IN"
			+ "                                   (SELECT DEPT_NUM FROM TMP_JECN_IO_DEPT)"
			+ "                               AND PROJECTID = :PROJECTID))";

	/** 流程角色与岗位关联临时表（PROCESS_STATION_RELATED_T）0：岗位 删除 */
	public final static String DELETE_JECN_PROCESS_STATION_RELATED_T_DEL_DEPT = "DELETE PROCESS_STATION_RELATED_T"
			+ " WHERE TYPE = 0"
			+ "   AND FIGURE_POSITION_ID IN"
			+ "       (SELECT P.FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P"
			+ "         WHERE P.ORG_ID IN (SELECT ORG_ID"
			+ "                              FROM JECN_FLOW_ORG"
			+ "                             WHERE ORG_NUMBER NOT IN"
			+ "                                   (SELECT DEPT_NUM FROM TMP_JECN_IO_DEPT)"
			+ "                               AND PROJECTID = :PROJECTID))";

	/** 岗位与制度关联关系表（JECN_POSITION_REF_SYSTEM） 删除 */
	public final static String DELETE_JECN_POSITION_REF_SYSTEM_DEL_DEPT = "DELETE JECN_POSITION_REF_SYSTEM"
			+ " WHERE POSITION_ID IN"
			+ "       (SELECT P.FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P"
			+ "         WHERE P.ORG_ID IN (SELECT ORG_ID"
			+ "                              FROM JECN_FLOW_ORG"
			+ "                             WHERE ORG_NUMBER NOT IN"
			+ "                                   (SELECT DEPT_NUM FROM TMP_JECN_IO_DEPT)"
			+ "                               AND PROJECTID = :PROJECTID))";

	/** 岗位表（JECN_FLOW_ORG_IMAGE） 删除 */
	public final static String DELETE_JECN_FLOW_ORG_IMAGE_DEL_DEPT = "DELETE"
			+ "  FROM JECN_FLOW_ORG_IMAGE" + " WHERE NOT EXISTS (SELECT 1"
			+ "          FROM TMP_JECN_IO_DEPT TD"
			+ "         WHERE TD.DEPT_NUM = ORG_NUMBER_ID"
			+ "           AND TD.PROJECTID = :PROJECTID)"
			+ "   AND NOT EXISTS (SELECT 1"
			+ "          FROM TMP_JECN_IO_POS TP"
			+ "         WHERE TP.POS_NUM = FIGURE_NUMBER_ID)";

	// ------------------------------------------------待删除部门下的岗位 end

	/** 岗位基本属性（JECN_POSITION_FIG_INFO） 删除 */
	public final static String DELETE_JECN_POSITION_FIG_INFO = "DELETE JECN_POSITION_FIG_INFO"
			+ " WHERE FIGURE_ID NOT IN"
			+ "       (SELECT FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P, TMP_JECN_IO_POS TP, JECN_FLOW_ORG D"
			+ "         WHERE P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "           AND P.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)";

	/** 岗位与岗位组关系表（JECN_POSITION_GROUP_R） 删除 */
	public final static String DELETE_JECN_POSITION_GROUP_R = "DELETE JECN_POSITION_GROUP_R"
			+ " WHERE FIGURE_ID NOT IN"
			+ "       (SELECT FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P, TMP_JECN_IO_POS TP, JECN_FLOW_ORG D"
			+ "         WHERE P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "           AND P.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)"
			+ "   AND FIGURE_ID IN (SELECT P.FIGURE_ID"
			+ "                       FROM JECN_FLOW_ORG_IMAGE P, JECN_FLOW_ORG D"
			+ "                      WHERE P.ORG_ID = D.ORG_ID"
			+ "                        AND D.PROJECTID = :PROJECTID)";

	/** 流程基本信息（JECN_FLOW_BASIC_INFO） 责任人类型（TYPE_RES_PEOPLE）==1 更新 */
	public final static String UPDATE_JECN_FLOW_BASIC_INFO_1 = "UPDATE JECN_FLOW_BASIC_INFO"
			+ "   SET RES_PEOPLE_ID = NULL, TYPE_RES_PEOPLE = NULL"
			+ " WHERE (RES_PEOPLE_ID IS NOT NULL OR TYPE_RES_PEOPLE IS NOT NULL)"
			+ "   AND TYPE_RES_PEOPLE = 1"
			+ "   AND RES_PEOPLE_ID NOT IN"
			+ "       (SELECT FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P, TMP_JECN_IO_POS TP, JECN_FLOW_ORG D"
			+ "         WHERE P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "           AND P.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)"
			+ "   AND RES_PEOPLE_ID IN (SELECT P.FIGURE_ID"
			+ "                       FROM JECN_FLOW_ORG_IMAGE P, JECN_FLOW_ORG D"
			+ "                      WHERE P.ORG_ID = D.ORG_ID"
			+ "                        AND D.PROJECTID = :PROJECTID)";

	/** 流程基本信息临时表(JECN_FLOW_BASIC_INFO_T) 责任人类型（TYPE_RES_PEOPLE）==1 更新 */
	public final static String UPDATE_JECN_FLOW_BASIC_INFO_T_1 = "UPDATE JECN_FLOW_BASIC_INFO_T"
			+ "   SET RES_PEOPLE_ID = NULL, TYPE_RES_PEOPLE = NULL"
			+ " WHERE (RES_PEOPLE_ID IS NOT NULL OR TYPE_RES_PEOPLE IS NOT NULL)"
			+ "   AND TYPE_RES_PEOPLE = 1"
			+ "   AND RES_PEOPLE_ID NOT IN"
			+ "       (SELECT FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P, TMP_JECN_IO_POS TP, JECN_FLOW_ORG D"
			+ "         WHERE P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "           AND P.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)"
			+ "   AND RES_PEOPLE_ID IN (SELECT P.FIGURE_ID"
			+ "                       FROM JECN_FLOW_ORG_IMAGE P, JECN_FLOW_ORG D"
			+ "                      WHERE P.ORG_ID = D.ORG_ID"
			+ "                        AND D.PROJECTID = :PROJECTID)";

	/** 流程角色与岗位关联表（PROCESS_STATION_RELATED）0：岗位 删除 */
	public final static String DELETE_JECN_PROCESS_STATION_RELATED = "DELETE PROCESS_STATION_RELATED"
			+ " WHERE TYPE = 0"
			+ "   AND FIGURE_POSITION_ID NOT IN"
			+ "       (SELECT FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P, TMP_JECN_IO_POS TP, JECN_FLOW_ORG D"
			+ "         WHERE P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "           AND P.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)"
			+ "   AND FIGURE_POSITION_ID IN (SELECT P.FIGURE_ID"
			+ "                       FROM JECN_FLOW_ORG_IMAGE P, JECN_FLOW_ORG D"
			+ "                      WHERE P.ORG_ID = D.ORG_ID"
			+ "                        AND D.PROJECTID = :PROJECTID)";

	/** 流程角色与岗位关联临时表（PROCESS_STATION_RELATED_T）0：岗位 删除 */
	public final static String DELETE_JECN_PROCESS_STATION_RELATED_T = "DELETE PROCESS_STATION_RELATED_T"
			+ " WHERE TYPE = 0"
			+ "   AND FIGURE_POSITION_ID NOT IN"
			+ "       (SELECT FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P, TMP_JECN_IO_POS TP, JECN_FLOW_ORG D"
			+ "         WHERE P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "           AND P.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)"
			+ "   AND FIGURE_POSITION_ID IN (SELECT P.FIGURE_ID"
			+ "                       FROM JECN_FLOW_ORG_IMAGE P, JECN_FLOW_ORG D"
			+ "                      WHERE P.ORG_ID = D.ORG_ID"
			+ "                        AND D.PROJECTID = :PROJECTID)";

	/** 岗位与制度关联关系表（JECN_POSITION_REF_SYSTEM） 删除 */
	public final static String DELETE_JECN_POSITION_REF_SYSTEM = "DELETE JECN_POSITION_REF_SYSTEM"
			+ " WHERE POSITION_ID NOT IN"
			+ "       (SELECT FIGURE_ID"
			+ "          FROM JECN_FLOW_ORG_IMAGE P, TMP_JECN_IO_POS TP, JECN_FLOW_ORG D"
			+ "         WHERE P.FIGURE_NUMBER_ID = TP.POS_NUM"
			+ "           AND P.ORG_ID = D.ORG_ID"
			+ "           AND D.PROJECTID = :PROJECTID)"
			+ "   AND POSITION_ID IN (SELECT P.FIGURE_ID"
			+ "                       FROM JECN_FLOW_ORG_IMAGE P, JECN_FLOW_ORG D"
			+ "                      WHERE P.ORG_ID = D.ORG_ID"
			+ "                        AND D.PROJECTID = :PROJECTID)";

	/** 岗位表（JECN_FLOW_ORG_IMAGE） 删除 */
	public final static String DELETE_JECN_FLOW_ORG_IMAGE = "DELETE FROM JECN_FLOW_ORG_IMAGE "
			+ " WHERE NOT EXISTS (SELECT 1"
			+ "          FROM TMP_JECN_IO_DEPT TD"
			+ "         WHERE TD.DEPT_NUM = ORG_NUMBER_ID"
			+ "           AND TD.PROJECTID = :PROJECTID)"
			+ "   OR NOT EXISTS (SELECT 1"
			+ "          FROM TMP_JECN_IO_POS TP"
			+ "         WHERE TP.POS_NUM = FIGURE_NUMBER_ID)";

	// -----------------------------------------------------------------------删除岗位以及相关end
	/** 更新岗位名称、所属部门ID */
	public final static String UPDATE_POS_DEPTID_NAME = "UPDATE JECN_FLOW_ORG_IMAGE"
			+ "   SET FIGURE_TEXT = (SELECT T.TP_POS_NAME"
			+ "                        FROM (SELECT P.FIGURE_ID AS P_FIGURE_ID,"
			+ "                                     TP.POS_NAME AS TP_POS_NAME"
			+ "                                FROM (SELECT PL.*"
			+ "                                        FROM JECN_FLOW_ORG_IMAGE PL,"
			+ "                                             JECN_FLOW_ORG       D"
			+ "                                       WHERE PL.ORG_ID = D.ORG_ID"
			+ "                                         AND D.PROJECTID = :PROJECTID) P"
			+ "                                LEFT JOIN TMP_JECN_IO_POS TP ON P.FIGURE_NUMBER_ID ="
			+ "                                                                TP.POS_NUM"
			+ "                                LEFT JOIN (SELECT *"
			+ "                                            FROM JECN_FLOW_ORG D1"
			+ "                                           WHERE D1.PROJECTID = :PROJECTID) D ON TP.DEPT_NUM ="
			+ "                                                                                 D.ORG_NUMBER) T"
			+ "                       WHERE T.P_FIGURE_ID = FIGURE_ID),"
			+ "       ORG_ID      = (SELECT T.TP_ORG_ID"
			+ "                        FROM (SELECT P.FIGURE_ID AS P_FIGURE_ID,"
			+ "                                     D.ORG_ID    AS TP_ORG_ID"
			+ "                                FROM (SELECT PL.*"
			+ "                                        FROM JECN_FLOW_ORG_IMAGE PL,"
			+ "                                             JECN_FLOW_ORG       D"
			+ "                                       WHERE PL.ORG_ID = D.ORG_ID"
			+ "                                         AND D.PROJECTID = :PROJECTID) P"
			+ "                                LEFT JOIN TMP_JECN_IO_POS TP ON P.FIGURE_NUMBER_ID ="
			+ "                                                                TP.POS_NUM"
			+ "                                LEFT JOIN (SELECT *"
			+ "                                            FROM JECN_FLOW_ORG D1"
			+ "                                           WHERE D1.PROJECTID = :PROJECTID) D ON TP.DEPT_NUM ="
			+ "                                                                                 D.ORG_NUMBER) T"
			+ "                       WHERE T.P_FIGURE_ID = FIGURE_ID)"
			+ " WHERE FIGURE_ID IN"
			+ "       (SELECT T.P_FIGURE_ID"
			+ "          FROM (SELECT P.FIGURE_ID   AS P_FIGURE_ID,"
			+ "                       P.FIGURE_TEXT AS P_FIGURE_TEXT,"
			+ "                       P.ORG_ID      AS P_ORG_ID,"
			+ "                       TP.POS_NAME   AS TP_POS_NAME,"
			+ "                       D.ORG_ID      AS TP_ORG_ID"
			+ "                  FROM (SELECT PL.*"
			+ "                          FROM JECN_FLOW_ORG_IMAGE PL, JECN_FLOW_ORG D"
			+ "                         WHERE PL.ORG_ID = D.ORG_ID"
			+ "                           AND D.PROJECTID = :PROJECTID) P"
			+ "                  LEFT JOIN TMP_JECN_IO_POS TP ON P.FIGURE_NUMBER_ID ="
			+ "                                                  TP.POS_NUM"
			+ "                  LEFT JOIN (SELECT *"
			+ "                              FROM JECN_FLOW_ORG D1"
			+ "                             WHERE D1.PROJECTID = :PROJECTID) D ON TP.DEPT_NUM ="
			+ "                                                                   D.ORG_NUMBER) T"
			+ "         WHERE T.P_FIGURE_TEXT <> T.TP_POS_NAME"
			+ "            OR T.P_ORG_ID <> T.TP_ORG_ID"
			+ "            OR T.P_ORG_ID IS NULL)";

	// ---------------------------------------岗位 结束

	// ---------------------------------------人员 开始
	/** 通用人员临时表所有数据 */
	public final static String DELETE_TMP_JECN_IO_USER = "DELETE FROM TMP_JECN_IO_USER";
	/** 待导入人员中在各自真实库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_USER = "UPDATE TMP_JECN_IO_USER"
			+ "  SET PRO_FLAG = 1"
			+ "WHERE EXISTS (SELECT 1 FROM JECN_USER G WHERE G.LOGIN_NAME = USER_NUM)";

	// -------------------------------------插入人员临时表中标识是插入的人员start
	/** 插入人员临时表中标识是插入的人员 */
	// oracle
	public final static String INSERT_JECN_USER_ORACLE = "INSERT INTO JECN_USER"
			+ "(PEOPLE_ID,"
			+ "USER_NUMBER,"
			+ "LOGIN_NAME,"
			+ "PASSWORD,"
			+ "TRUE_NAME,"
			+ "EMAIL,"
			+ "ISLOCK,"
			+ "MAC_ADDRESS,"
			+ "EMAIL_TYPE,"
			+ "PHONE_STR)"
			+ "  SELECT JECN_USER_SQUENCE.NEXTVAL,"
			+ "         NULL,"
			+ "         USER_NUM,"// --登录名称
			+ "         :PASSWORD,"
			+ "         TRUE_NAME,"
			+ "         Email,"
			+ "         0,"
			+ "         NULL,"
			+ "         CASE WHEN EMAIL_TYPE=:EMAIL_INNER THEN 'inner' WHEN EMAIL_TYPE=:EMAIL_OUTER THEN 'outer' END,"
			+ "         PHONE"
			+ "    FROM (SELECT DISTINCT G.USER_NUM,G.TRUE_NAME,G.EMAIL,G.EMAIL_TYPE,G.PHONE FROM TMP_JECN_IO_USER G WHERE G.PRO_FLAG = 0)T";
	// sqlserver
	public final static String INSERT_JECN_USER_SQLSERVER = "INSERT INTO JECN_USER"
			+ "("
			+ "USER_NUMBER,"
			+ "LOGIN_NAME,"
			+ "PASSWORD,"
			+ "TRUE_NAME,"
			+ "EMAIL,"
			+ "ISLOCK,"
			+ "MAC_ADDRESS,"
			+ "EMAIL_TYPE,"
			+ "PHONE_STR)"
			+ "  SELECT "
			+ "         NULL,"
			+ "         USER_NUM,"// --登录名称
			+ "         :PASSWORD,"
			+ "         TRUE_NAME,"
			+ "         Email,"
			+ "         0,"
			+ "         NULL,"
			+ "         CASE WHEN EMAIL_TYPE=:EMAIL_INNER THEN 'inner' WHEN EMAIL_TYPE=:EMAIL_OUTER THEN 'outer' END,"
			+ "         PHONE"
			+ "    FROM (SELECT DISTINCT G.USER_NUM,G.TRUE_NAME,G.EMAIL,G.EMAIL_TYPE,G.PHONE FROM TMP_JECN_IO_USER G WHERE G.PRO_FLAG = 0)T";

	// -------------------------------------插入人员临时表中标识是插入的人员end

	// -----------------------------------------------------------------------删除人员以及相关start
	/** 人员角色关联表（JECN_USER_ROLE）:删除 PEOPLE_ID=人员表主键ID */
	public final static String DELETE_JECN_USER_ROLE = "DELETE FROM JECN_USER_ROLE"
			+ " WHERE PEOPLE_ID NOT IN"
			+ "       (SELECT U.PEOPLE_ID"
			+ "          FROM JECN_USER U"
			+ "         WHERE U.LOGIN_NAME IN (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU)"
			+ "            OR U.LOGIN_NAME ='admin')";

	/** 流程结构表（JECN_FLOW_STRUCTURE）:更新人员ID（PEOPLE_ID）字段为NULL */
	public final static String UPDATE_JECN_FLOW_STRUCTURE = "UPDATE JECN_FLOW_STRUCTURE"
			+ "   SET PEOPLE_ID = NULL"
			+ " WHERE PEOPLE_ID IS NOT NULL"
			+ " AND PEOPLE_ID NOT IN"
			+ "       (SELECT U.PEOPLE_ID"
			+ "          FROM JECN_USER U"
			+ "         WHERE U.LOGIN_NAME IN (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU)"
			+ "            OR U.LOGIN_NAME ='admin')";

	/** 流程结构临时表(JECN_FLOW_STRUCTURE_T):更新人员ID（PEOPLE_ID）字段为NULL */
	public final static String UPDATE_JECN_FLOW_STRUCTURE_T = "UPDATE JECN_FLOW_STRUCTURE_T"
			+ "   SET PEOPLE_ID = NULL"
			+ " WHERE PEOPLE_ID IS NOT NULL"
			+ " AND PEOPLE_ID NOT IN"
			+ "       (SELECT U.PEOPLE_ID"
			+ "          FROM JECN_USER U"
			+ "         WHERE U.LOGIN_NAME IN (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU)"
			+ "            OR U.LOGIN_NAME ='admin')";

	/** 文件表(JECN_FILE) 更新:人员ID(PEOPLE_ID)=NULL */
	public final static String UPDATE_JECN_FILE = "UPDATE JECN_FILE"
			+ "   SET PEOPLE_ID = NULL"
			+ " WHERE PEOPLE_ID IS NOT NULL"
			+ " AND PEOPLE_ID NOT IN"
			+ "       (SELECT U.PEOPLE_ID"
			+ "          FROM JECN_USER U"
			+ "         WHERE U.LOGIN_NAME IN (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU)"
			+ "            OR U.LOGIN_NAME ='admin')";
	/** 文件表(JECN_FILE_T) 更新:人员ID(PEOPLE_ID)=NULL */
	public final static String UPDATE_JECN_FILE_T = "UPDATE JECN_FILE_T"
			+ "   SET PEOPLE_ID = NULL"
			+ " WHERE PEOPLE_ID IS NOT NULL"
			+ " AND PEOPLE_ID NOT IN"
			+ "       (SELECT U.PEOPLE_ID"
			+ "          FROM JECN_USER U"
			+ "         WHERE U.LOGIN_NAME IN (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU)"
			+ "            OR U.LOGIN_NAME ='admin')";

	/** 信息表(JECN_MESSAGE) 更新:人员ID(PEOPLE_ID)=NULL */
	public final static String UPDATE_JECN_MESSAGE_USER = "UPDATE JECN_MESSAGE"
			+ "   SET PEOPLE_ID = NULL"
			+ " WHERE PEOPLE_ID IS NOT NULL"
			+ " AND PEOPLE_ID NOT IN"
			+ "       (SELECT U.PEOPLE_ID"
			+ "          FROM JECN_USER U"
			+ "         WHERE U.LOGIN_NAME IN (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU)"
			+ "            OR U.LOGIN_NAME ='admin')";

	/** （流程文件制度）任务表（JECN_FLOW_TASK_BEAN） 更新：创建人员ID（CREATE_PERSON_ID）=null */

	/** 人员表（JECN_USER）：删除 */
	public final static String DELETE_JECN_USER = "DELETE FROM JECN_USER WHERE  LOGIN_NAME NOT IN (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU) AND LOGIN_NAME <>'admin'";

	// -----------------------------------------------------------------------删除人员以及相关end\
	/** 更新人员真实姓名、邮箱地址、联系电话、内外网邮件标识(内网：0（inner）,外网：1（outer）) */
	public final static String UPDATE_USER_NAME_EMAIL_PHONE_ORACLE = 
			"UPDATE JECN_USER" +
			"   SET (TRUE_NAME, EMAIL, EMAIL_TYPE, PHONE_STR, DOMAIN_NAME) =" + 
			"       (SELECT T.TU_TRUE_NAME," + 
			"               T.TU_EMAIL," + 
			"               T.TU_EMAIL_TYPE," + 
			"               T.TU_PHONE," + 
			"               T.TU_DOMAIN_NAME" + 
			"          FROM (SELECT DISTINCT U.PEOPLE_ID AS U_PEOPLE_ID," + 
			"                                TU.TRUE_NAME AS TU_TRUE_NAME," + 
			"                                TU.EMAIL AS TU_EMAIL," + 
			"                                CASE" + 
			"                                  WHEN TU.EMAIL_TYPE = 0 THEN" + 
			"                                   'inner'" + 
			"                                  WHEN TU.EMAIL_TYPE = 1 THEN" + 
			"                                   'outer'" + 
			"                                  ELSE" + 
			"                                   NULL" + 
			"                                END TU_EMAIL_TYPE," + 
			"                                TU.PHONE AS TU_PHONE," + 
			"                                TU.DOMAIN_NAME AS TU_DOMAIN_NAME" + 
			"                  FROM JECN_USER U" + 
			"                  LEFT JOIN TMP_JECN_IO_USER TU" + 
			"                    ON U.LOGIN_NAME = TU.USER_NUM" + 
			"                 WHERE U.LOGIN_NAME <> 'admin') T" + 
			"         WHERE T.U_PEOPLE_ID = PEOPLE_ID)"
			+ " WHERE ISLOCK=0 AND " 
			+ "		LOGIN_NAME NOT IN "+JecnCommonSql.getStrs(BaseBean.syncNotUpdateUsers) ;
	
	
	public final static String UPDATE_USER_NAME_EMAIL_PHONE_SQLSERVER = 
			"UPDATE TU" +
			"   SET TU.TRUE_NAME   = T.TRUE_NAME," + 
			"       TU.EMAIL       = T.EMAIL," + 
			"       TU.EMAIL_TYPE  = T.EMAIL_TYPE," + 
			"       TU.PHONE_STR   = T.PHONE," + 
			"       TU.DOMAIN_NAME = T.DOMAIN_NAME FROM (SELECT DISTINCT U.PEOPLE_ID," + 
			"                                                            TU.TRUE_NAME," + 
			"                                                            TU.EMAIL," + 
			"                                                            CASE" + 
			"                                                              WHEN TU.EMAIL_TYPE = 0 THEN" + 
			"                                                               'inner'" + 
			"                                                              WHEN TU.EMAIL_TYPE = 1 THEN" + 
			"                                                               'outer'" + 
			"                                                              ELSE" + 
			"                                                               NULL" + 
			"                                                            END EMAIL_TYPE," + 
			"                                                            TU.PHONE," + 
			"                                                            TU.DOMAIN_NAME" + 
			"                                              FROM JECN_USER U" + 
			"                                              LEFT JOIN TMP_JECN_IO_USER TU" + 
			"                                                ON U.LOGIN_NAME =" + 
			"                                                   TU.USER_NUM" + 
			"                                             WHERE U.LOGIN_NAME <> 'admin') T, JECN_USER TU" + 
			" WHERE T.PEOPLE_ID = TU.PEOPLE_ID"
			+ " AND ISLOCK=0 AND " 
			+ "		LOGIN_NAME NOT IN "+JecnCommonSql.getStrs(BaseBean.syncNotUpdateUsers);

	// ---------------------------------------人员 结束

	// ---------------------------------------人员与岗位关系 开始
	/** 信息表(JECN_MESSAGE) 更新:收件人ID(INCEPT_PEOPLE_ID)=NULL 更新 */
	public final static String UPDATE_JECN_MESSAGE_REF = "UPDATE JECN_MESSAGE"
			+ "   SET INCEPT_PEOPLE_ID = NULL"
			+ " WHERE INCEPT_PEOPLE_ID IS NOT NULL"
			+ "   AND INCEPT_PEOPLE_ID NOT IN"
			+ "       (SELECT REF.USER_ID"
			+ "          FROM JECN_USER_POSITION_RELATED REF,"
			+ "               (SELECT U.PEOPLE_ID, P.FIGURE_ID "// --获取与临时表人员岗位一一对应的数据
			+ "                  FROM TMP_JECN_IO_USER TU"
			+ "                  LEFT JOIN JECN_USER U ON TU.USER_NUM = U.LOGIN_NAME"
			+ "                  LEFT JOIN JECN_FLOW_ORG_IMAGE P ON TU.POS_NUM ="
			+ "                                                     P.FIGURE_NUMBER_ID"
			+ "                 WHERE TU.POS_NUM IS NOT NULL) NEWREF"
			+ "         WHERE REF.PEOPLE_ID = NEWREF.PEOPLE_ID"
			+ "           AND REF.FIGURE_ID = NEWREF.FIGURE_ID)";

	// /** 人员岗位关联表（JECN_USER_POSITION_RELATED）删除 */
	// public final static String DELETE_JECN_USER_POSITION_RELATED_ORACLE =
	// "DELETE JECN_USER_POSITION_RELATED JU"
	// + " WHERE NOT EXISTS"
	// + " (SELECT 1 FROM JECN_USER U WHERE JU.PEOPLE_ID = U.PEOPLE_ID)"
	// + " AND NOT EXISTS (SELECT 1"
	// + " FROM JECN_FLOW_ORG_IMAGE FOI, JECN_FLOW_ORG FO"
	// + " WHERE FOI.ORG_ID = FO.ORG_ID"
	// + " AND JU.FIGURE_ID = FOI.FIGURE_ID"
	// + " AND FO.PROJECTID = :PROJECTID)";
	// /** 人员岗位关联表（JECN_USER_POSITION_RELATED）删除 */
	// public final static String DELETE_JECN_USER_POSITION_RELATED_SQLSERVER =
	// "DELETE JU from JECN_USER_POSITION_RELATED JU"
	// + " WHERE NOT EXISTS"
	// + " (SELECT 1 FROM JECN_USER U WHERE JU.PEOPLE_ID = U.PEOPLE_ID)"
	// + " AND NOT EXISTS (SELECT 1"
	// + " FROM JECN_FLOW_ORG_IMAGE FOI, JECN_FLOW_ORG FO"
	// + " WHERE FOI.ORG_ID = FO.ORG_ID"
	// + " AND JU.FIGURE_ID = FOI.FIGURE_ID"
	// + " AND FO.PROJECTID = :PROJECTID)";

	/** 人员岗位关联表（JECN_USER_POSITION_RELATED）删除 */
	public final static String DELETE_JECN_USER_POSITION_RELATED_ORACLE = "DELETE FROM JECN_USER_POSITION_RELATED JR"
			+ " WHERE NOT EXISTS(with NEWREF as (SELECT U.PEOPLE_ID, P.FIGURE_ID"
			+ "                              FROM TMP_JECN_IO_USER TU,"
			+ "                                   JECN_USER U,"
			+ "                                   (SELECT P1.FIGURE_ID,"
			+ "                                           P1.FIGURE_NUMBER_ID"
			+ "                                      FROM JECN_FLOW_ORG_IMAGE P1,"
			+ "                                           JECN_FLOW_ORG       D"
			+ "                                     WHERE P1.ORG_ID = D.ORG_ID"
			+ "                                       AND D.PROJECTID = :PROJECTID) P"
			+ "                             WHERE TU.USER_NUM = U.LOGIN_NAME"
			+ "                               AND TU.POS_NUM = P.FIGURE_NUMBER_ID"
			+ "                               AND TU.POS_NUM IS NOT NULL" +
					"						  AND TU.USER_NUM NOT IN "+JecnCommonSql.getStrs(BaseBean.syncNotUpdateUsers)+")"
			+ "   select 1"
			+ "     from NEWREF"
			+ "    where JR.PEOPLE_ID = NEWREF.PEOPLE_ID"
			+ "      AND JR.FIGURE_ID = NEWREF.FIGURE_ID)";
	/** 人员岗位关联表（JECN_USER_POSITION_RELATED）删除 */
	public final static String DELETE_JECN_USER_POSITION_RELATED_SQLSERVER = "WITH NEWREF as (SELECT U.PEOPLE_ID, P.FIGURE_ID"
			+ "                        FROM TMP_JECN_IO_USER TU,"
			+ "                       JECN_USER U,"
			+ "                       (SELECT P1.FIGURE_ID,"
			+ "                               P1.FIGURE_NUMBER_ID"
			+ "                          FROM JECN_FLOW_ORG_IMAGE P1,"
			+ "                               JECN_FLOW_ORG       D"
			+ "                         WHERE P1.ORG_ID = D.ORG_ID"
			+ "                           AND D.PROJECTID = :PROJECTID) P"
			+ "                 WHERE TU.USER_NUM = U.LOGIN_NAME"
			+ "                   AND TU.POS_NUM = P.FIGURE_NUMBER_ID"
			+ "                   AND TU.POS_NUM IS NOT NULL" 
			+ "					  AND TU.USER_NUM NOT IN "+JecnCommonSql.getStrs(BaseBean.syncNotUpdateUsers)+")"
			+ " DELETE PR FROM JECN_USER_POSITION_RELATED PR WHERE NOT EXISTS ( select 1"
			+ " from NEWREF EF"
			+ " where PR.PEOPLE_ID = EF.PEOPLE_ID"
			+ "  AND PR.FIGURE_ID = EF.FIGURE_ID)";

	// -----------------------------------------------------------------------删除人员与岗位关系以及相关end

	/** 插入人员与岗位关系数据（临时表中存在真实表中不存在） */
	public final static String INSERT_JECN_USER_POSITION_RELATED_ORACLE = "INSERT INTO JECN_USER_POSITION_RELATED"
			+ "  (USER_ID, FIGURE_ID, PEOPLE_ID) with tmp_user as"
			+ "  (SELECT U.PEOPLE_ID, P.FIGURE_ID"
			+ "     FROM TMP_JECN_IO_USER TU"
			+ "     LEFT JOIN JECN_USER U ON TU.USER_NUM = U.LOGIN_NAME"
			+ "     LEFT JOIN (SELECT DISTINCT PL.*"
			+ "                 FROM JECN_FLOW_ORG_IMAGE PL, JECN_FLOW_ORG D"
			+ "                WHERE PL.ORG_ID = D.ORG_ID"
			+ "                  AND D.PROJECTID = :PROJECTID) P ON TU.POS_NUM = P.FIGURE_NUMBER_ID"
			+ "    WHERE TU.POS_NUM IS NOT NULL)"
			+ "  SELECT JECN_USER_SQUENCE.NEXTVAL, TMP.FIGURE_ID, TMP.PEOPLE_ID"
			+ "    from tmp_user TMP"
			+ "   where not exists (select 1"
			+ "            from JECN_USER_POSITION_RELATED REF"
			+ "           where REF.PEOPLE_ID = TMP.PEOPLE_ID"
			+ "             AND REF.FIGURE_ID = TMP.FIGURE_ID)";

	public final static String INSERT_JECN_USER_POSITION_RELATED_SQLSERVER = "INSERT INTO JECN_USER_POSITION_RELATED"
			+ "  (FIGURE_ID, PEOPLE_ID)"
			+ "  SELECT TMP.FIGURE_ID, TMP.PEOPLE_ID"
			+ "    from (SELECT U.PEOPLE_ID, P.FIGURE_ID"
			+ "            FROM TMP_JECN_IO_USER TU"
			+ "            LEFT JOIN JECN_USER U ON TU.USER_NUM = U.LOGIN_NAME"
			+ "            LEFT JOIN (SELECT DISTINCT PL.*"
			+ "                        FROM JECN_FLOW_ORG_IMAGE PL, JECN_FLOW_ORG D"
			+ "                       WHERE PL.ORG_ID = D.ORG_ID"
			+ "                         AND D.PROJECTID = :PROJECTID) P ON TU.POS_NUM ="
			+ "                                                   P.FIGURE_NUMBER_ID"
			+ "           WHERE TU.POS_NUM IS NOT NULL) TMP"
			+ "   where not exists (select 1"
			+ "            from JECN_USER_POSITION_RELATED REF"
			+ "           where REF.PEOPLE_ID = TMP.PEOPLE_ID"
			+ "             AND REF.FIGURE_ID = TMP.FIGURE_ID)";

	// ---------------------------------------人员与岗位关系 结束

	// -------------------------------查询部门、岗位、人员 整体数据start--
	/** 获取部门所有数据（部门编号、部门名称、上级部门编号） */
	public final static String SELECT_DEPT_ALL_DATA = "SELECT T.ORG_NUMBER,"
			+ "       T.ORG_NAME,"
			+ "       CASE"
			+ "         WHEN T.PER_ORG_ID = 0 THEN"
			+ "          '0'"
			+ "         ELSE"
			+ "          T2.ORG_NUMBER"
			+ "       END AS PER_ORG_NUMBER"
			+ "  FROM (SELECT * FROM JECN_FLOW_ORG D1 WHERE D1.PROJECTID = :PROJECTID) T"
			+ "  LEFT JOIN (SELECT * FROM JECN_FLOW_ORG D1 WHERE D1.PROJECTID = :PROJECTID) T2 ON T.PER_ORG_ID = T2.ORG_ID";
	/**   */
	public final static String SELECT_USER_POS_ALL_DATA = "SELECT U.LOGIN_NAME,"
			+ "       U.TRUE_NAME,"
			+ "       REF.FIGURE_NUMBER_ID,"
			+ "       REF.FIGURE_TEXT,"
			+ "       REF.ORG_NUMBER,"
			+ "       U.EMAIL,"
			+ "       CASE"
			+ "         WHEN U.EMAIL_TYPE = 'inner' THEN"
			+ "          0"
			+ "         WHEN U.EMAIL_TYPE = 'outer' THEN"
			+ "          1"
			+ "         ELSE"
			+ "          NULL"
			+ "       END AS EMAIL_TYPE,"
			+ "       U.PHONE_STR"
			+ "  FROM JECN_USER U"
			+ "  LEFT JOIN (SELECT REF1.*,"
			+ "                    P1.FIGURE_NUMBER_ID,"
			+ "                    P1.FIGURE_TEXT,"
			+ "                    P1.ORG_NUMBER"
			+ "               FROM JECN_USER_POSITION_RELATED REF1,"
			+ "                    (SELECT PL.*, D.ORG_NUMBER"
			+ "                       FROM JECN_FLOW_ORG_IMAGE PL, JECN_FLOW_ORG D"
			+ "                      WHERE PL.ORG_ID = D.ORG_ID"
			+ "						   AND D.DEL_STATE=0"
			+ "                        AND D.PROJECTID = :PROJECTID) P1,"
			+ "                    JECN_USER U1"
			+ "              WHERE REF1.PEOPLE_ID = U1.PEOPLE_ID"
			+ "                AND REF1.FIGURE_ID = P1.FIGURE_ID) REF ON U.PEOPLE_ID ="
			+ "                                                          REF.PEOPLE_ID"
			+ " WHERE U.LOGIN_NAME<>'admin'  and U.ISLOCK <> 1";
	/** 更新制度临时表中临时部门为空 jecn_rule_t */
	public final static String UPDATE_RULE_T_REF_DEPT_ORACLE = "update jecn_rule_t t"
			+ "     set t.org_id = null"
			+ "   where not exists (SELECT 1"
			+ "            FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "           WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "             AND D.PROJECTID = :PROJECTID"
			+ "             and D.ORG_ID = t.org_id)";
	/** 更新制度临时表中部门为空 jecn_rule */
	public final static String UPDATE_RULE_REF_DEPT_ORACLE = "update jecn_rule t"
			+ "     set t.org_id = null"
			+ "   where not exists (SELECT 1"
			+ "            FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "           WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "             AND D.PROJECTID = :PROJECTID"
			+ "             and D.ORG_ID = t.org_id)";

	/** 更新制度临时表中临时部门为空 jecn_rule_t */
	public final static String UPDATE_RULE_T_REF_DEPT_SQLSERVER = "update t"
			+ "   set t.org_id = null from jecn_rule_t t"
			+ " where not exists (SELECT 1"
			+ "          FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "         WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "           AND D.PROJECTID = :PROJECTID"
			+ "           and D.ORG_ID = t.org_id)";
	/** 更新制度临时表中部门为空 jecn_rule */
	public final static String UPDATE_RULE_REF_DEPT_SQLSERVER = "update t"
			+ "   set t.org_id = null from jecn_rule t"
			+ " where not exists (SELECT 1"
			+ "          FROM TMP_JECN_IO_DEPT TD, JECN_FLOW_ORG D"
			+ "         WHERE TD.DEPT_NUM = D.ORG_NUMBER"
			+ "           AND D.PROJECTID = :PROJECTID"
			+ "           and D.ORG_ID = t.org_id)";

	/** 流程基本信息 流程拟制人 */
	public final static String UPDATE_JECN_FLOW_BASC_WARD_PEOPLE_ID = "UPDATE JECN_FLOW_BASIC_INFO"
			+ "   SET WARD_PEOPLE_ID = null"
			+ " where WARD_PEOPLE_ID is not null"
			+ "   and NOT EXISTS (SELECT 1"
			+ "          FROM JECN_USER U, TMP_JECN_IO_USER TU"
			+ "         WHERE U.LOGIN_NAME = TU.USER_NUM"
			+ "           AND WARD_PEOPLE_ID = U.PEOPLE_ID)";

	/** 流程基本信息 流程监护人 */
	public final static String UPDATE_JECN_FLOW_BASC_FICTION_PEOPLE_ID = "UPDATE JECN_FLOW_BASIC_INFO"
			+ "   SET FICTION_PEOPLE_ID = null"
			+ " where FICTION_PEOPLE_ID is not null"
			+ "   and NOT EXISTS (SELECT 1"
			+ "          FROM JECN_USER U, TMP_JECN_IO_USER TU"
			+ "         WHERE U.LOGIN_NAME = TU.USER_NUM"
			+ "           AND FICTION_PEOPLE_ID = U.PEOPLE_ID)";

	/** 流程基本信息 流程拟制人 */
	public final static String UPDATE_JECN_FLOW_BASC_T_WARD_PEOPLE_ID = "UPDATE JECN_FLOW_BASIC_INFO_T"
			+ "   SET WARD_PEOPLE_ID = null"
			+ " where WARD_PEOPLE_ID is not null"
			+ "   and NOT EXISTS (SELECT 1"
			+ "          FROM JECN_USER U, TMP_JECN_IO_USER TU"
			+ "         WHERE U.LOGIN_NAME = TU.USER_NUM"
			+ "           AND WARD_PEOPLE_ID = U.PEOPLE_ID)";

	/** 流程基本信息 流程监护人 */
	public final static String UPDATE_JECN_FLOW_BASC_T_FICTION_PEOPLE_ID = "UPDATE JECN_FLOW_BASIC_INFO_T"
			+ "   SET FICTION_PEOPLE_ID = null"
			+ " where FICTION_PEOPLE_ID is not null"
			+ "   and NOT EXISTS (SELECT 1"
			+ "          FROM JECN_USER U, TMP_JECN_IO_USER TU"
			+ "         WHERE U.LOGIN_NAME = TU.USER_NUM"
			+ "           AND FICTION_PEOPLE_ID = U.PEOPLE_ID)";

	/** 流程图信息(临时表) 角色文本 岗位 */
	public final static String UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_T_ORACLE = "UPDATE JECN_FLOW_STRUCTURE_IMAGE_T JFSI SET FIGURE_TEXT= "
			+ " (select T.FIGURE_TEXT from JECN_FLOW_ORG_IMAGE T,PROCESS_STATION_RELATED_T B,JECN_FLOW_ORG C "
			+ " 		where b.FIGURE_FLOW_ID=JFSI.FIGURE_ID  "
			+ " 		and t.FIGURE_ID=b.FIGURE_POSITION_ID "
			+ " 		and t.ORG_ID=c.ORG_ID 	and b.TYPE=0 ";

	/** 流程图信息(正式表) 角色文本 岗位 */
	public final static String UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_ORACLE = "UPDATE JECN_FLOW_STRUCTURE_IMAGE JFSI SET FIGURE_TEXT= "
			+ " (SELECT T.FIGURE_TEXT from JECN_FLOW_ORG_IMAGE T,PROCESS_STATION_RELATED B,JECN_FLOW_ORG C "
			+ " 		where b.FIGURE_FLOW_ID=JFSI.FIGURE_ID  "
			+ " 		and t.FIGURE_ID=b.FIGURE_POSITION_ID "
			+ " 		and t.ORG_ID=c.ORG_ID 	and b.TYPE=0 ";

	/** 流程图信息(临时表) 角色文本 岗位 */
	public final static String UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_T_SQLSERVER = "UPDATE JFSI SET JFSI.FIGURE_TEXT= "
			+ " (select T.FIGURE_TEXT from JECN_FLOW_ORG_IMAGE T,PROCESS_STATION_RELATED_T B,JECN_FLOW_ORG C "
			+ " 		where b.FIGURE_FLOW_ID=JFSI.FIGURE_ID  "
			+ " 		and t.FIGURE_ID=b.FIGURE_POSITION_ID "
			+ " 		and t.ORG_ID=c.ORG_ID 	and b.TYPE=0 ";

	/** 流程图信息(正式表) 角色文本 岗位 */
	public final static String UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_SQLSERVER = "UPDATE JFSI SET JFSI.FIGURE_TEXT= "
			+ " (SELECT T.FIGURE_TEXT from JECN_FLOW_ORG_IMAGE T,PROCESS_STATION_RELATED B,JECN_FLOW_ORG C "
			+ " 		where b.FIGURE_FLOW_ID=JFSI.FIGURE_ID  "
			+ " 		and t.FIGURE_ID=b.FIGURE_POSITION_ID "
			+ " 		and t.ORG_ID=c.ORG_ID 	and b.TYPE=0 ";
	
	/** 将岗位ID插入岗位基本信息表 */
	public static final String INSERT_JECN_POSITION_FIG_INFO = "INSERT INTO JECN_POSITION_FIG_INFO"
			+ " (FIGURE_ID)"
			+ " SELECT JFOI.FIGURE_ID"
			+ "   FROM JECN_FLOW_ORG_IMAGE JFOI,JECN_FLOW_ORG JFO"
			+ "  WHERE NOT EXISTS (SELECT 1"
			+ "          FROM JECN_POSITION_FIG_INFO JPFI"
			+ "        WHERE JFOI.FIGURE_ID = JPFI.FIGURE_ID) AND JFOI.FIGURE_TYPE='PositionFigure' "
			+ "  AND JFOI.ORG_ID=JFO.ORG_ID  AND JFO.PROJECTID = :PROJECTID ";
	
	
	/**插入基准岗位信息**/
	public static final String INSERT_BASE_POS="INSERT INTO JECN_BASE_POS(BASE_POS_ID,BASE_POS_NUMBER,BASE_POS_NAME) " +
			"VALUES(?,?,?) ";
	
	
	/**获取实际岗位信息**/
	public static final String SELECT_POS_INFO = "SELECT JFOI.FIGURE_ID,JFOI.FIGURE_NUMBER_ID " +
			                                     " FROM JECN_FLOW_ORG_IMAGE JFOI " +
			                                     " WHERE JFOI.FIGURE_TYPE = 'PositionFigure'";
	/**查询基准岗位信息*/
	public static final String SELECT_BASE_POS_INFO = "SELECT JBP.BASE_POS_ID,JBP.BASE_POS_NUMBER,JBP.BASE_POS_NAME  FROM JECN_BASE_POS JBP";
	
	/**删除基准岗位*/
	public static final String DEL_BASE_POS_INFO = "DELETE FROM JECN_BASE_POS JBP WHERE JBP.BASE_POS_NUMBER = ?";
	/**删除基准岗位sqlserver版*/
	public static final String DEL_BASE_POS_INFO_SQL = "DELETE JBP FROM JECN_BASE_POS JBP WHERE JBP.BASE_POS_NUMBER = ?";
	
	/**插入基准岗位与岗位关系*/
	public static final String INSERT_BASE_REL_POS_INFO = "INSERT INTO JECN_BASE_REL_POS(BASE_REL_ID,POS_ID,BASE_ID) VALUES(?,?,?)";
	
	/**删除基准岗位与岗位关系*/
	public static final String DEL_ALL_BASE_REL_POS_INFO = "DELETE FROM JECN_BASE_REL_POS";
	
	/** 岗位组同步 oracle */ 
	public final static String INSERT_POSGROUP_ORACLE =  "INSERT INTO JECN_POSITION_GROUP" +
		"  (ID," + 
		"   NAME," + 
		"   PROJECT_ID," + 
		"   PER_ID," + 
		"   IS_DIR," + 
		"   CREATE_PERSON_ID," + 
		"   CREATE_TIME," + 
		"   UPDATE_TIME," + 
		"   UPDATE_PERSON_ID," + 
		"   SORT_ID," + 
		"   T_PATH," + 
		"   T_LEVEL," + 
		"   CODE," + 
		"   PARENT_CODE," + 
		"   DATA_TYPE)" + 
		"  SELECT JECN_USER_SQUENCE.NEXTVAL ID," + 
		"         NAME," + 
		"         :PROJECTID                         PROJECT_ID," + 
		"         NULL                      PER_ID," + 
		"         IS_DIR," + 
		"         1                         CREATE_PERSON_ID," + 
		"         sysdate                   CREATE_TIME," + 
		"         sysdate                   UPDATE_TIME," + 
		"         1                         UPDATE_PERSON_ID," + 
		"         1                        SORT_ID," + 
		"         NULL                      T_PATH," + 
		"         NULL                      T_LEVEL," + 
		"         NUM                       CODE," + 
		"         PARENT_NUM                PARENT_CODE," + 
		"         1                         DATA_TYPE" + 
		"    FROM TMP_JECN_POS_GROUP WHERE PRO_FLAG = 0";
	
	/** 岗位组同步 sqlserver */ 
	public final static String INSERT_POSGROUP_SQLSERVER =  "INSERT INTO JECN_POSITION_GROUP" +
		"  (NAME," + 
		"   PROJECT_ID," + 
		"   PER_ID," + 
		"   IS_DIR," + 
		"   CREATE_PERSON_ID," + 
		"   CREATE_TIME," + 
		"   UPDATE_TIME," + 
		"   UPDATE_PERSON_ID," + 
		"   SORT_ID," + 
		"   T_PATH," + 
		"   T_LEVEL," + 
		"   CODE," + 
		"   PARENT_CODE," + 
		"   DATA_TYPE)" + 
		"  SELECT NAME," + 
		"         :PROJECTID                         PROJECT_ID," + 
		"         NULL                      PER_ID," + 
		"         IS_DIR," + 
		"         1                         CREATE_PERSON_ID," + 
		"         sysdate                   CREATE_TIME," + 
		"         sysdate                   UPDATE_TIME," + 
		"         1                         UPDATE_PERSON_ID," + 
		"         1                        SORT_ID," + 
		"         NULL                      T_PATH," + 
		"         NULL                      T_LEVEL," + 
		"         NUM                       CODE," + 
		"         PARENT_NUM                PARENT_CODE," + 
		"         1                         DATA_TYPE" + 
		"    FROM TMP_JECN_POS_GROUP WHERE PRO_FLAG = 0";
	
	/** 岗位组同步 oracle */ 
	public final static String INSERT_POSGROUP_RELATED_POS_ORACLE =  "INSERT INTO JECN_POSITION_GROUP_R" +
		"  (ID, GROUP_ID, FIGURE_ID, STATE, POS_NUM, POSGROUP_NUM, DATA_TYPE)" + 
		"  SELECT JECN_USER_SQUENCE.NEXTVAL ID," + 
		"         NULL                      GROUP_ID," + 
		"         NULL                      FIGURE_ID," + 
		"         0                         STATE," + 
		"         POS_NUM," + 
		"         POSGROUP_NUM," + 
		"         1                         DATA_TYPE" + 
		"    FROM TMP_JECN_POSGROUP_POS WHERE PRO_FLAG = 0";

	
	/** 岗位组同步 sqlserver */ 
	public final static String INSERT_POSGROUP_RELATED_POS_SQLSERVER =  "INSERT INTO JECN_POSITION_GROUP_R" +
	"  (GROUP_ID, FIGURE_ID, STATE, POS_NUM, POSGROUP_NUM, DATA_TYPE)" + 
	"  SELECT NULL                      GROUP_ID," + 
	"         NULL                      FIGURE_ID," + 
	"         0                         STATE," + 
	"         POS_NUM," + 
	"         POSGROUP_NUM," + 
	"         1                         DATA_TYPE" + 
	"    FROM TMP_JECN_POSGROUP_POS WHERE PRO_FLAG = 0";
	
	
	/** 流程角色与岗位关联表（PROCESS_STATION_RELATED）0：岗位 删除 */
	public final static String DELETE_JECN_PROCESS_STATION_RELATED_DEL_POSGROUP = "DELETE PROCESS_STATION_RELATED" +
		" WHERE TYPE = 1" + 
		"   AND NOT EXISTS (SELECT 1" + 
		"          FROM JECN_POSITION_GROUP PG" + 
		"         WHERE FIGURE_POSITION_ID = PG.ID)";

	/** 流程角色与岗位关联临时表（PROCESS_STATION_RELATED_T）0：岗位 删除 */
	public final static String DELETE_JECN_PROCESS_STATION_RELATED_T_DEL_POSGROUP= "DELETE PROCESS_STATION_RELATED_T" +
		" WHERE TYPE = 1" + 
		"   AND NOT EXISTS (SELECT 1" + 
		"          FROM JECN_POSITION_GROUP PG" + 
		"         WHERE FIGURE_POSITION_ID = PG.ID)";
	
	/**更新岗位组*/
	public final static String UPDATE_JECN_POSGROUP_ORACLE = "UPDATE JECN_POSITION_GROUP T" +
		"   SET T.NAME       =" + 
		"       (SELECT JPG.NAME FROM TMP_JECN_POS_GROUP JPG WHERE JPG.NUM = CODE)," + 
		"       T.PARENT_CODE =" + 
		"       (SELECT JPG.PARENT_NUM FROM TMP_JECN_POS_GROUP JPG WHERE JPG.NUM = CODE)," + 
		"       T.PER_ID     =" + 
		"       (SELECT PG.ID FROM JECN_POSITION_GROUP PG" + 
		"         WHERE PG.CODE = T.PARENT_CODE)" + 
		" WHERE DATA_TYPE = 1";
	
	/**更新岗位组*/
	public final static String UPDATE_JECN_POSGROUP_SQLSERVER = "UPDATE T" +
		"   SET T.NAME        = JPG.NUM," + 
		"       T.PARENT_CODE = JPG.PARENT_NUM," + 
		"       T.PER_ID = PG.ID FROM JECN_POSITION_GROUP T INNER JOIN TMP_JECN_POS_GROUP JPG" + 
		" WHERE JPG.NUM = T.CODE  INNER JOIN JECN_POSITION_GROUP PG ON PG.CODE = T.PARENT_CODE";
	
	/** 更新岗位组临时表*/
	public final static String UPDATE_TMP_JECN_POS_GROUP_ORACLE = "UPDATE TMP_JECN_POS_GROUP PG SET PG.PRO_FLAG = 1 "
		+ "WHERE EXISTS (SELECT 1 FROM JECN_POSITION_GROUP G WHERE G.CODE = PG.NUM)";
	
	public final static String UPDATE_TMP_JECN_POS_GROUP_SQLSERVER = "UPDATE PG   SET PG.PRO_FLAG = 1 FROM TMP_JECN_POS_GROUP PG "
		+ "WHERE EXISTS (SELECT 1 FROM JECN_POSITION_GROUP G WHERE G.CODE = PG.NUM)";
	
	/** 岗位组相关岗位 临时表*/
	public final static String UPDATE_TMP_JECN_POSGROUP_POS_ORACLE = "UPDATE TMP_JECN_POSGROUP_POS POS_G SET POS_G.PRO_FLAG = 1 WHERE EXISTS (SELECT 1 FROM JECN_POSITION_GROUP_R G  "
		+ "WHERE G.POS_NUM = POS_G.POS_NUM AND  G.POSGROUP_NUM = POS_G.POSGROUP_NUM)"; 
	public final static String UPDATE_TMP_JECN_POSGROUP_POS_SQLSERVER = "UPDATE POS_G SET POS_G.PRO_FLAG = 1 FROM TMP_JECN_POSGROUP_POS POS_G WHERE EXISTS (SELECT 1 FROM JECN_POSITION_GROUP_R G  "
		+ "WHERE G.POS_NUM = POS_G.POS_NUM AND  G.POSGROUP_NUM = POS_G.POSGROUP_NUM)"; 
	
	/** 删除岗位组和岗位关系表中记录在临时表中不存在的对应关系记录*/
	public final static String DELETE_JECN_POSITION_GROUP_R_BY_TMP_ORACLE = "DELETE FROM JECN_POSITION_GROUP_R GR" + " WHERE NOT EXISTS (SELECT 1"
		+ "          FROM TMP_JECN_POSGROUP_POS JPP" + "         WHERE JPP.POS_NUM = GR.POS_NUM"
		+ "           AND JPP.POSGROUP_NUM = GR.POSGROUP_NUM)" + " AND GR.DATA_TYPE = 1";
	public final static String DELETE_JECN_POSITION_GROUP_R_BY_TMP_SQLSERVER = "DELETE GR FROM JECN_POSITION_GROUP_R GR" + " WHERE NOT EXISTS (SELECT 1"
		+ "          FROM TMP_JECN_POSGROUP_POS JPP" + "         WHERE JPP.POS_NUM = GR.POS_NUM"
		+ "           AND JPP.POSGROUP_NUM = GR.POSGROUP_NUM)" + " AND GR.DATA_TYPE = 1";
	
	/** 更新流程人员表：在人员实际表中不存在的数据 给予锁定状态 0:解锁 1：锁定 */
	public final static String UPDATE_JECN_USER_ISLOCK = "UPDATE JECN_USER"
			+ "  SET ISLOCK = 1" + " WHERE LOGIN_NAME NOT IN "
			+ " (SELECT TU.USER_NUM FROM TMP_JECN_IO_USER TU) "
			+ " AND LOGIN_NAME <> 'admin'";
	
	/** 删除人员后，情况人员对应岗位关系表*/
	public final static String DELETE_USER_POSTION_SQLSERVER = "DELETE JUP FROM JECN_USER_POSITION_RELATED JUP WHERE NOT EXISTS (SELECT 1 FROM JECN_USER TU WHERE TU.PEOPLE_ID = JUP.PEOPLE_ID)";
	
	public final static String DELETE_USER_POSTION_ORACLE = "DELETE FROM JECN_USER_POSITION_RELATED JUP WHERE NOT EXISTS (SELECT 1 FROM JECN_USER TU WHERE TU.PEOPLE_ID = JUP.PEOPLE_ID)";
}


