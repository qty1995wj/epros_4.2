package epros.draw.visioxml.input;

import java.awt.Point;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;

import visioxml.bean.ConnectProperty;
import visioxml.constant.Constants.Align;
import visioxml.constant.Constants.CellType;
import visioxml.constant.Constants.FillPattern;
import visioxml.constant.Constants.FontStyle;
import visioxml.constant.Constants.LinePattern;
import visioxml.constant.Constants.LineRoute;
import visioxml.constant.Constants.Valign;
import visioxml.util.Tools;
import visioxml.util.Unit;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 解析帮助类
 * 
 * @author admin
 * 
 */
class VisioModelReaderHelper {

	private static final Log log = LogFactory.getLog(VisioModelReaderHelper.class);
	// 横线、竖线、分割线
	static final List<String> EPROS_LINE = Arrays.asList("EPROS_LINE_DIVIDINGLINE", "EPROS_LINE_HDIVIDINGLINE",
			"EPROS_LINE_VDIVIDINGLINE");
	// 横分割线、竖分割线
	static final List<String> EPROS_DVICE_LINES = Arrays.asList("EPROS_LINE_PARALLELLINES", "EPROS_LINE_VERTICALLINE");

	// 动态连接线
	static final String EPROS_DYN_LINE = "Dynamic connector";
	// 泳池==》角色
	static final String EPROS_VISIO_SWIMLANE = "Swimlane";
	// CFF容器==》名称文本框
	static final String EPROS_VISIO_CFF_Container = "CFF Container";
	// 阶段列表==》垂直分割线
	static final String EPROS_VISIO_PHASE_LIST = "Phase List";
	// 处理层级
	static final List<String> specialShaape = Arrays.asList("小组框架");

	// 联合使用 存在泳道 除了 角色外的图形X偏移80；
	static boolean SWIMLANE;
	static final int pinX = 100;

	// 活动对应的nameU集合，图形上面有编号的
	static final List<String> activityNameU = Arrays.asList("EPROS_ROUNDRECT", "EPROS_DECISION", "EPROS_DECISION_IT",
			"JECN_DATABASE", "活动", "Decision");

	// 导入三角形 特殊处理一下(向上偏移一点)
	static final List<MapElemType> mapElementType = Arrays.asList(MapElemType.Triangle, MapElemType.KeyPointFigure,
			MapElemType.EndFigure);
	// 保存字体集合<key=id,value=name>
	static Map<String, String> faceName = new HashMap<String, String>();
	// 保存颜色集合<key=id,value=#000000>
	static Map<String, String> colorName = new HashMap<String, String>();
	// 保存用到的图形<key=id,value=shapeElement>
	static Map<String, Element> mapShape = new HashMap<String, Element>();

	/*** visio图形master集合 key=ID,value=nameU */
	private Map<String, String> masterMap = null;

	/*** visio图形 key=MasterNameU和value=MasterShape **/
	private Map<String, Element> masterShapeMap = null;

	public VisioModelReaderHelper(Document doc) {
		masterShapeMap = new HashMap<String, Element>();
		masterMap = readMaster(doc);

	}

	/**
	 * 获取master集合
	 * 
	 * @param doc
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String> readMaster(final Document doc) {
		Map<String, String> hashMap = new HashMap<String, String>();
		List<Element> masters = doc.getRootElement().element("Masters").elements();
		String masterNameU = "";
		for (Element master : masters) {
			masterNameU = master.attributeValue("NameU").replaceAll("\\.\\d+", "").trim();
			hashMap.put(master.attributeValue("ID"), masterNameU);
			masterShapeMap.put(masterNameU, master.element("Shapes").element("Shape"));
		}
		// 初始化字体
		List<Element> faceNames = doc.getRootElement().element("FaceNames").elements();
		for (Element face : faceNames) {
			faceName.put(face.attributeValue("ID"), face.attributeValue("Name"));
		}
		// 初始化颜色
		if (doc.getRootElement().element("Colors") != null) {
			List<Element> colorEntity = doc.getRootElement().element("Colors").elements();
			for (Element color : colorEntity) {
				colorName.put(color.attributeValue("IX"), color.attributeValue("RGB"));
			}
		}

		return hashMap;
	}

	/**
	 * 设置通用图形位置点
	 * 
	 * @param figureData
	 * @param ele
	 * @param panelHeight
	 */
	protected void setGeneralFiguleLocation(JecnFigureData figureData, Element ele, double panelHeight) {
		// Xfrom
		Element xFrom = ele.element("XForm");
		double visioWidth = Double.valueOf(xFrom.elementText("Width"));
		double visioHeight = Double.valueOf(xFrom.elementText("Height"));
		double visioPinX = Double.valueOf(xFrom.elementText("PinX"));
		double visioPinY = Double.valueOf(xFrom.elementText("PinY"));
		double visisAngle = Double.valueOf(xFrom.elementText("Angle"));
		int visioAngle = VisioInputTools.DoubleToInt(Unit.toDeg(Double.valueOf(visisAngle)));
		// 图形旋转
		double angle = Tools.visioAngleToEprosAngle(visioAngle);
		// 计算图形的位置点
		if (angle == 90 || angle == 270) {
			double temp = visioWidth;
			visioWidth = visioHeight;
			visioHeight = temp;
			visioPinX = visioPinX - visioHeight / 8; // 暂时加的偏移量
			visioPinY = visioPinY - visioWidth / 28;// 暂时加的偏移量
		}
		// 图形宽度
		int width = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(visioWidth + ""));
		// 图形高度
		int height = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(visioHeight + ""));
		// 图形的X
		double pinX = VisioInputTools.changeX(visioPinX + "", width);
		// 图形的Y
		double pinY = VisioInputTools.changeY(visioPinY + "", panelHeight, height);
		if (mapElementType.contains(figureData.getMapElemType())) {
			pinY = pinY - (Unit.MMToPx(2));
		}
		figureData.getFigureDataCloneable().getLeftUpXY().setXY(pinX, pinY);
		// 设置图形旋转
		figureData.setCurrentAngle(angle);

		String nameU = getMasterMap().get(String.valueOf(ele.attributeValue("Master")));
		nameU = nameU.replaceAll("\\.\\d+", "").trim();

		// 泳池角色 不设置大小
		if (VisioModelReaderHelper.EPROS_VISIO_SWIMLANE.equals(nameU)) {
			pinY = pinY + height / 4;
			figureData.getFigureDataCloneable().getLeftUpXY().setXY(pinX, pinY);
			return;
		}
		// CFF容器 对应的是 名称文本框
		if (VisioModelReaderHelper.EPROS_VISIO_CFF_Container.equals(nameU)) {
			figureData.getFigureDataCloneable().getLeftUpXY().setXY(width / 2, 0);
			return;
		}
		// 图形的宽度
		figureData.setFigureSizeX(width);
		// 图型的高度
		figureData.setFigureSizeY(height);
	}

	/**
	 * 设置特殊的图形
	 * 
	 * @param figureData
	 * @param ele
	 * @param figureType
	 * @param panelHeight
	 */
	protected void setSpecialFiguleLocation(JecnFigureData figureData, Element ele, String figureType,
			double panelHeight) {
		if ("CommentText".equals(figureType)) {// 如果为注释框
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			// 获取连接线数据对象
			JecnBaseDivedingLineData lineData = new JecnBaseDivedingLineData(MapElemType.CommentLine);
			Element xFrom1D = ele.element("XForm1D");
			// 开始
			int startX = VisioInputTools.DoubleToInt(VisioInputTools.changeX1D(xFrom1D.elementText("BeginX")));
			// 结束
			int startY = VisioInputTools.DoubleToInt(VisioInputTools.changeY1D(xFrom1D.elementText("BeginY"),
					panelHeight));
			// 图形的X
			int endX = VisioInputTools.DoubleToInt(VisioInputTools.changeX1D(xFrom1D.elementText("EndX")));
			// 图形的Y
			int endY = VisioInputTools.DoubleToInt(VisioInputTools.changeY1D(xFrom1D.elementText("EndY"), panelHeight));

			// 开始点
			Point startPoint = new Point(startX, startY);
			// 结束点
			Point endPoint = new Point(endX, endY);
			lineData.getDiviLineCloneable().initOriginalArributs(startPoint, endPoint, figureData.getBodyWidth(),
					JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowScale());

			// 设置小线段
			commentData.setLineData(lineData);
			// 注释框当作0度处理(visio里面默认是90度不能旋转)
			commentData.setCurrentAngle(0);
			// 设置为左上
			commentData.setFontPosition(0);
			// 图形的宽度（visio中获取不到设置默认值110）
			// commentData.setFigureSizeX(110);
			// 图型的高度（visio中获取不到设置默认值40）
			// commentData.setFigureSizeY(40);
			// 图形的位置点
			commentData.getFigureDataCloneable().getLeftUpXY().setXY(endX, endY - commentData.getFigureSizeY() / 2);

		} else if ("OneArrowLine".equals(figureType) || "TwoArrowLine".equals(figureType)
				|| "ReverseArrowhead".equals(figureType) || "RightArrowhead".equals(figureType)) {
			Element xFrom = ele.element("XForm");
			// 图形宽度
			int width = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(xFrom.elementText("Width")));
			// 图形高度
			int height = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(xFrom.elementText("Height")));
			Element xFrom1D = ele.element("XForm1D");
			double begX = Double.valueOf(xFrom1D.elementText("BeginX"));
			// double endX = Double.valueOf(xFrom1D.elementText("EndX"));
			double begY = Double.valueOf(xFrom1D.elementText("BeginY"));
			// double endY = Double.valueOf(xFrom1D.elementText("EndY"));
			// 获取角度
			double angle = figureData.getCurrentAngle();
			double pinX = 0;
			double pinY = 0;
			if (angle == 0 || angle == 360) {
				pinX = VisioInputTools.changeX1D(begX + "");
				pinY = VisioInputTools.changeY(begY + "", panelHeight, height);
			}
			// 不支持旋转（暂时不处理）
			else if (angle == 90 || angle == 270) {
			}
			// 图形的宽度
			// figureData.setFigureSizeX(width);
			// 图型的高度
			// figureData.setFigureSizeY(height);
			// 图形的位置点
			figureData.getFigureDataCloneable().getLeftUpXY().setXY(pinX, pinY);
		}

	}

	/**
	 * 设置图形显示的文本
	 * 
	 * @param figureData
	 * @param ele
	 * @param figureType
	 */
	protected void setJecnFiguleText(JecnAbstractFlowElementData figureData, Element ele, String figureType) {
		// 活动编号
		String activityNum = getActivityNum(ele);
		// 图形上添加的字符
		String figureText = getFigureText(ele);
		// 没有文本获取masterShape文本属性
		if ("".equals(figureText)) {
			String masterNameU = masterMap.get(ele.attributeValue("Master"));
			figureText = getFigureText(masterShapeMap.get(masterNameU));
		}
		// 设置图形文本
		figureData.setFigureText(figureText);
		if (DrawCommon.isActive(MapElemType.valueOf(figureType))) {
			JecnActiveData activeData = (JecnActiveData) figureData;
			if ("".equals(activityNum)) {
				activityNum = VisioInputTools.getActivityNum(figureText);
				figureText = figureText.replaceAll(activityNum, "").trim();
				figureData.setFigureText(figureText);
			}
			activeData.setActivityNum(activityNum);
		}
	}

	/**
	 * 获取活动编号
	 * 
	 * @param ele
	 */
	private String getActivityNum(Element ele) {
		String activityNum = "";
		if ("Group".equals(ele.attributeValue("Type"))) {
			List<Element> shapeList = ele.element("Shapes").elements("Shape");
			for (Element shape : shapeList) {
				if (shape.elementTextTrim("Text") != null && !"".equals(shape.elementTextTrim("Text"))) {
					String text = shape.elementTextTrim("Text").trim();
					if (VisioInputTools.isactivityNum(text)) {
						activityNum = text;
						break;
					}
				}
			}
		}
		return activityNum;
	}

	/**
	 * 获取图形上的文字
	 * 
	 * @param ele
	 */
	private String getFigureText(Element ele) {
		String figureText = "";
		if (ele.elementTextTrim("Text") != null && !"".equals(ele.elementTextTrim("Text"))) {
			figureText = ele.elementTextTrim("Text");
			return figureText;
		}
		// 组合图形
		if ("Group".equals(ele.attributeValue("Type"))) {
			String masterID = ele.attributeValue("Master");
			String nameU = getMasterMap().get(masterID);
			List<Element> shapeList = ele.element("Shapes").elements("Shape");
			for (Element shape : shapeList) {
				if (shape.elementTextTrim("Text") != null && !"".equals(shape.elementTextTrim("Text"))) {
					String text = shape.elementTextTrim("Text").trim();
					// 组合图形不是活动，直接当作文本内容返回
					if (!activityNameU.contains(nameU)) {
						figureText = text;
						break;
					} else {
						if (!VisioInputTools.isactivityNum(text)) {
							figureText = text;
							break;
						}
					}

				}
			}
		}
		return figureText;
	}

	/**
	 * 设置图形通用属性
	 * 
	 * @param figureData
	 * @param ele
	 * @param zIndex
	 */
	protected void setGeneralFiguleAttr(JecnAbstractFlowElementData figureData, Element ele, int zIndex) {
		// 图形的层级
		figureData.setZOrderIndex(zIndex);
		String nameU = getMasterMap().get(String.valueOf(ele.attributeValue("Master")));
		nameU = nameU.replaceAll("\\.\\d+", "").trim();
		// 泳池
		if (VisioModelReaderHelper.EPROS_VISIO_SWIMLANE.equals(nameU)
				|| VisioModelReaderHelper.EPROS_VISIO_CFF_Container.equals(nameU)) {
			return;
		}
		// 字体属性
		setFontAttr(figureData, ele);
		// 填充属性
		setFillAttr(figureData, ele);
		// 边框属性
		setLineAttr(figureData, ele);
	}

	/**
	 * 设置边框属性
	 * 
	 * @param figureData
	 * @param shape
	 */
	private void setLineAttr(JecnAbstractFlowElementData figureData, Element shape) {
		Element line = shape.element("Line");
		// visio 模具拖入的，没有line节点 获取主控图形line
		if (line == null) {
			String masterNameU = masterMap.get(shape.attributeValue("Master"));
			line = masterShapeMap.get(masterNameU).element("Line");
		}
		if (line == null || "1".equals(line.attributeValue("Del"))) {
			return;
		}
		// 导入线条粗细 默认写死 1磅
		String lineWeight = "0.01388888888888889";
		if (MapElemType.DividingLine == figureData.getMapElemType()) {
			lineWeight = line.elementTextTrim("LineWeight");
		}
		String lineColor = line.elementTextTrim("LineColor");
		if (colorName.get(lineColor) != null) {
			lineColor = colorName.get(lineColor);
		}
		if ("#FFFFFF".equals(lineColor)) {
			return;
		}
		LinePattern linePattern = LinePattern.getLinePattern(line.elementTextTrim("LinePattern"));
		// 线条颜色
		figureData.setBodyColor(Unit.RGBToColor(lineColor));
		// 线条大小
		figureData.setBodyWidth(PointsToPt(lineWeight));
		// 线条类型
		figureData.setBodyStyle(VisioInputTools.converLinePattern(linePattern));
	}

	/**
	 * 等级填充属性节点
	 * 
	 * @param figureData
	 * 
	 * @param mapElemType
	 * @param shape
	 */
	@SuppressWarnings("unchecked")
	private void setLevelFillAttr(JecnAbstractFlowElementData figureData, MapElemType mapElemType, Element shape) {
		List<Element> listShape = shape.element("Shapes").elements("Shape");
		// 等级颜色 对应的坐标不能改变
		Element fill = null;
		Element otherFill = null;
		switch (mapElemType) {
		case FlowLevelFigureOne:
			fill = listShape.get(0).element("Fill");
			otherFill = listShape.get(1).element("Fill");
			break;
		case FlowLevelFigureTwo:
			fill = listShape.get(3).element("Fill");
			otherFill = listShape.get(1).element("Fill");
			break;
		case FlowLevelFigureThree:
			fill = listShape.get(2).element("Fill");
			otherFill = listShape.get(1).element("Fill");
			break;
		case FlowLevelFigureFour:
			fill = listShape.get(1).element("Fill");
			otherFill = listShape.get(0).element("Fill");
			break;
		}
		// 针对 visio面板拖入的等级图形
		if (fill == null || otherFill == null) {
			log.error("visoi拖入的等级，没有手动设置颜色，就没有fill节点，跳过");
			return;
		}
		// 颜色1
		String fillColor = fill.elementTextTrim("FillForegnd");
		// 颜色2
		String changeColor = otherFill.elementTextTrim("FillForegnd");
		figureData.setFillColor(Unit.RGBToColor(changeColor));
		figureData.setChangeColor(Unit.RGBToColor(fillColor));
		figureData.setFillType(FillType.two);
	}

	/**
	 * 设置填充属性
	 * 
	 * @param figureData
	 * @param shape
	 */
	private void setFillAttr(JecnAbstractFlowElementData figureData, Element shape) {
		Element fill = shape.element("Fill");
		// 等级图形走另外的分支
		if (MapElemType.FlowLevelFigureOne == figureData.getMapElemType()
				|| MapElemType.FlowLevelFigureTwo == figureData.getMapElemType()
				|| MapElemType.FlowLevelFigureThree == figureData.getMapElemType()
				|| MapElemType.FlowLevelFigureFour == figureData.getMapElemType()) {
			setLevelFillAttr(figureData, figureData.getMapElemType(), shape);
			return;
		}
		// visio模具拖入的没有fill节点获取主控图形fill节点
		if (fill == null) {
			String masterNameU = masterMap.get(shape.attributeValue("Master"));
			fill = masterShapeMap.get(masterNameU).element("Fill");
		}
		if (fill == null || "1".equals(fill.attributeValue("Del"))) {
			return;
		}
		// 背景填充方式
		FillPattern fillPattern = FillPattern.getFillPattern(fill.elementTextTrim("FillPattern"));
		// 颜色1
		String fillColor = fill.elementTextTrim("FillForegnd");
		// 颜色2
		String changeColor = fill.elementTextTrim("FillBkgnd");
		if (colorName.get(fillColor) != null) {
			fillColor = colorName.get(fillColor);
		}
		if (colorName.get(changeColor) != null) {
			changeColor = colorName.get(changeColor);
		}

		if ("#FFFFFF".equals(fillColor)) {
			return;
		}
		if ("#FFFFFF".equals(changeColor)) {
			return;
		}
		// 阴影填充方式
		FillPattern shdwPattern = FillPattern.getFillPattern(fill.elementTextTrim("ShdwPattern"));
		// 阴影填充颜色
		String shadowColor = fill.elementTextTrim("ShdwForegnd");
		if (colorName.get(shadowColor) != null) {
			shadowColor = colorName.get(shadowColor);
		}
		if ("#FFFFFF".equals(shadowColor)) {
			return;
		}
		FillType fillType = VisioInputTools.coverFillPattern(fillPattern);
		// 渐变方式
		FillColorChangType fillColorChangType = VisioInputTools.coverFillColorChangType(fillPattern);

		// 填充类型 单双色
		figureData.setFillType(fillType);
		// 颜色1
		figureData.setFillColor(Unit.RGBToColor(fillColor));
		// 颜色2
		if (FillType.two == fillType) {
			figureData.setFillColor(Unit.RGBToColor(changeColor));
			figureData.setChangeColor(Unit.RGBToColor(fillColor));
		}
		// 颜色渐变方向
		figureData.setFillColorChangType(fillColorChangType);
		// 是否存在阴影
		if (FillPattern._00 != shdwPattern) {
			figureData.setShadowsFlag(true);
			// 阴影颜色
			figureData.setShadowColor(Unit.RGBToColor(shadowColor));
		} else {
			figureData.setShadowsFlag(false);
		}

	}

	/**
	 * 设置字体属性
	 * 
	 * @param figureData
	 * @param shape
	 */
	private void setFontAttr(JecnAbstractFlowElementData figureData, Element shape) {
		Element chart = shape.element("Char");
		// visio 模具拖入的，没有char 节点，获取主控图形 char节点
		if (chart == null) {
			String masterNameU = masterMap.get(shape.attributeValue("Master"));
			chart = masterShapeMap.get(masterNameU).element("Char");
		}
		if (chart == null || "1".equals(chart.attributeValue("Del"))) {
			return;
		}
		// 字体颜色
		String fontColor = chart.elementTextTrim("Color");
		if (colorName.get(fontColor) != null) {
			fontColor = colorName.get(fontColor);
		}
		if ("#FFFFFF".equals(fontColor)) {
			return;
		}
		// 字体大小，活字典
		String size = chart.elementTextTrim("Size"); // PT 活字典
		// 是否加粗
		FontStyle fontStyle = FontStyle.getFontStyle(chart.elementTextTrim("Style"));
		// 文字垂直或者水平0水平1 垂直
		String useVertical = chart.elementTextTrim("UseVertical");
		// 水平对齐方式
		Align align = Align.CENTER;
		if (shape.element("Para") != null) {
			align = Align.getAlign(shape.element("Para").elementTextTrim("HorzAlign"));
		}
		// 垂直对齐方式
		Valign valign = Valign.MIDDLE;
		if (shape.element("TextBlock") != null) {
			valign = Valign.getValign(shape.element("TextBlock").elementTextTrim("VerticalAlign"));
		}
		// 字体颜色
		figureData.setFontColor(Unit.RGBToColor(fontColor));
		// 字体大小
		figureData.setFontSize(PointsToPt(size) + 3);
		// 字体
		figureData.setFontName(faceName.get(chart.elementText("Font")));
		// 获取字符
		String figuretext = getFigureText(shape);
		// 没有文本获取masterShape文本属性
		if ("".equals(figuretext)) {
			String masterNameU = masterMap.get(shape.attributeValue("Master"));
			figuretext = getFigureText(masterShapeMap.get(masterNameU));
		}
		if (VisioInputTools.isChineseChar(figuretext)) {
			figureData.setFontName("宋体");
		}
		// 字体加粗
		figureData.setFontStyle(VisioInputTools.converFontStyle(fontStyle));
		// 设置文字竖排
		if ("1".equals(useVertical)) {
			figureData.setFontUpRight(true);
		}
		// 字体位置
		figureData.setFontPosition(VisioInputTools.converFontPosition(align, valign));
	}

	/**
	 * 解析动态连接线到Map集合
	 * 
	 * @param listConnect
	 * @param beginX
	 * @param endX
	 */
	protected void initConnects(List<Element> listConnect, Map<String, ConnectProperty> beginX,
			Map<String, ConnectProperty> endX) {
		for (Element conn : listConnect) {
			int fromSheet = Integer.valueOf(conn.attributeValue("FromSheet"));
			CellType cellType = CellType.getCellType(conn.attributeValue("FromCell"));
			int toSheet = Integer.valueOf(conn.attributeValue("ToSheet"));
			LineRoute lineRoute = LineRoute.getVisioRoute(conn.attributeValue("ToCell"));
			ConnectProperty connect = new ConnectProperty(fromSheet, cellType, toSheet, lineRoute);
			if (CellType.BEGINX == cellType) {
				beginX.put(fromSheet + "", connect);
			} else if (CellType.ENDX == cellType) {
				endX.put(fromSheet + "", connect);
			} else {
				throw new RuntimeException("解析visio连接线关系异常,cellType=" + cellType.toString());
			}
		}

	}

	/**
	 * 活字典 转为磅值
	 * 
	 * @param point
	 * @return
	 */
	private int PointsToPt(String point) {
		return VisioInputTools.DoubleToInt(Unit.PointsToPt(Double.valueOf(point)));
	}

	public Map<String, String> getMasterMap() {
		return masterMap;
	}

	public void setMasterMap(Map<String, String> masterMap) {
		this.masterMap = masterMap;
	}
}
