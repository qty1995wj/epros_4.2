package epros.designer.gui.common.treeCheckbox;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import epros.designer.tree.JecnTreeNode;

public class CheckBoxTreeNodeSelectionListener extends MouseAdapter{
	private List<JecnTreeNode> listNodes;
	public CheckBoxTreeNodeSelectionListener(List<JecnTreeNode> listNodes){
		this.listNodes = listNodes;
		String a ="";
	}
	
	 @Override                                                                              
	  public void mouseClicked(MouseEvent event)                                            
	  {                                                                                     
	      JTree tree = (JTree)event.getSource();                                            
	      int x = event.getX();                                                             
	      int y = event.getY();                                                             
	      int row = tree.getRowForLocation(x, y);                                           
	      TreePath path = tree.getPathForRow(row);                                          
	      if(path != null)                                                                  
	      {                                                                                 
	          JecnTreeNode node = (JecnTreeNode)path.getLastPathComponent();        
	          if(node != null)                                                              
	          {    
	        	  if(node.getJecnTreeBean().getIsDelete() == 1){
	        		  boolean isSelected = !node.isSelected();                                  
		              node.setSelected(isSelected);                                             
		              ((DefaultTreeModel)tree.getModel()).nodeStructureChanged(node);   
	        	  }
	          }
	          if( node.isSelected()){
	        	  listNodes.add(node);
	          }
	      }                                                                                 
	  }

	public List<JecnTreeNode> getListNodes() {
		return listNodes;
	}

	public void setListNodes(List<JecnTreeNode> listNodes) {
		this.listNodes = listNodes;
	}   
	 

}
