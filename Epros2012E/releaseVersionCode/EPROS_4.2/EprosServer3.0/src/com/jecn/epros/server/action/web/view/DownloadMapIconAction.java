package com.jecn.epros.server.action.web.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.process.IFlowStructureService;

/**
 * 
 * 流程图/流程地图/组织图图片
 * 
 * @author ZHOUXY
 * 
 */
public class DownloadMapIconAction extends BaseAction {
	public final Log log = LogFactory.getLog(DownloadMapIconAction.class);
	/** 流程图/流程地图/组织图图片路径 */
	private String mapIconPath = null;
	/** 流程图/流程地图/组织图名称 */
	private String mapName = null;

	private Long flowId;

	private String isPub;

	private String isApp;

	@Resource(name = "FlowStructureServiceImpl")
	private IFlowStructureService structureService;

	/**
	 * 
	 * 对外接口，导出人员数据（excel方式）
	 * 
	 * @return
	 */
	public String downAllMapIcon() {
		if (isNull(mapIconPath) || isNull(JecnContants.jecn_path)) {
			log.info("mapIconPath=" + mapIconPath + ";JecnContants.jecn_path=" + JecnContants.jecn_path);
			this.setErrorInfo(getText("downFileNoFind"));
			return ERRORINFO;
		}
		File file = new File(JecnContants.jecn_path + mapIconPath);
		if (!file.exists() || !file.isFile()) {// 判断文件是否是标准文件，是否存在
			log.info("不是文件或文件不存在。path=" + JecnContants.jecn_path + mapIconPath);
			this.setErrorInfo(getText("downFileNoFind"));
			return ERRORINFO;
		}
		return SUCCESS;
	}

	/**
	 * 
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() {
		return getInputStreamLocal();
	}

	public String getMapIconPath() {
		return mapIconPath;
	}

	public void setMapIconPath(String mapIconPath) {
		this.mapIconPath = mapIconPath;
	}

	/**
	 * 
	 * 对于配置中的 ${mapName}, 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	public String getMapName() {
		if (isNull(mapIconPath) || flowId == null) {
			log.error("下载图片为空! mapIconPath = " + mapIconPath + " flowId = " + flowId);
			return "";
		}
		int index = mapIconPath.lastIndexOf(".");
		try {
			isApp = this.getRequest().getParameter("isApp");
			// 暂时未处理任务查阅图片情况
			if ("true".equals(isPub)) {
				JecnFlowStructure flowStructure = structureService.findJecnFlowStructureById(flowId);
				mapName = flowStructure.getFlowName();
			} else {
				JecnFlowStructureT flowStructure = structureService.get(flowId);
				mapName = flowStructure.getFlowName();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 保存图片名称
		String name = null;
		if (isNull(mapName)) {
			name = "new Icon";
		} else {
			name = mapName;
		}
		if (index >= 0) {
			name = name + mapIconPath.substring(index);
		}
		// 通过浏览器转码
		name = JecnCommon.getDownFileNameByEncoding(getResponse(), name, isApp);
		return name;
	}

	// public void setMapName(String mapName) {
	// this.mapName = mapName;
	// }

	/**
	 * 
	 * 获取图片
	 * 
	 * @return InputStream
	 */
	private InputStream getInputStreamLocal() {
		try {
			return new FileInputStream(new File(JecnContants.jecn_path + mapIconPath));
		} catch (Exception e) {
			log.error("DownloadMapIconAction类getInputStreamLocal方法文件找不到异常：", e);
			return null;
		}
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	private boolean isNull(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

	public String getIsPub() {
		return isPub;
	}

	public void setIsPub(String isPub) {
		this.isPub = isPub;
	}
}
