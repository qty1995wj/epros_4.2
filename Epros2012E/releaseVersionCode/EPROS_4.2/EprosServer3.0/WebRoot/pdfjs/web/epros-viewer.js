'use strict';
$(document).keydown(function(e){
	   if( e.ctrlKey  == true && (e.keyCode == 83) ){
	      return false; // 截取返回false就不会保存网页了
	   }
	});
$(document).ready(function() {
	var args = window.location.search.split("?"), name = "", downloadURL = "";
	args[1].split("&").forEach(function(t) {
		var param = t.split("=");
		switch (param[0]) {
		case "name":
			name = decodeURI(param[1]);
			break;
		case "downloadURL":
			downloadURL = param[1];
			break;
		default:
			break;
		}
	});
	document.title = name;
	if (downloadURL !== "none") {
		$("#downloadForm").attr("action", downloadURL + "?" + args[2]);
		$("#epsDownload").bind("click", function() {
			$("#downloadForm").submit();
		})

	} else {
		$("#epsDownload").css("display", "none");
		$("#print").css("display", "none");
	}
	var isDownLoad = $('#session').val();
		if (isDownLoad !== "true") {
			$("#epsDownload").css("display", "none");
			$("#secondaryDownload").css("display", "none");
			//
			$("#print").css("display", "none");
			$("#secondaryPrint").css("display", "none");
			addCssRule("@media print", " body{display:none}");
		}
	});

/*
 * 动态添加 CSS 样式
 * @param selector {string} 选择器
 * @param rules    {string} CSS样式规则
 * @param index    {number} 插入规则的位置, 靠后的规则会覆盖靠前的，默认在后面插入
 */
var addCssRule = function() {
	// 创建一个 style， 返回其 stylesheet 对象
	function createStyleSheet() {
		var style = document.createElement('style');
		style.type = 'text/css';
		document.head.appendChild(style);
		return style.sheet;
	}

	// 创建 stylesheet 对象
	var sheet = createStyleSheet();

	// 返回接口函数
	return function(selector, rules, index) {
		index = index || 0;
		sheet.insertRule(selector + "{" + rules + "}", index);
	}
}();