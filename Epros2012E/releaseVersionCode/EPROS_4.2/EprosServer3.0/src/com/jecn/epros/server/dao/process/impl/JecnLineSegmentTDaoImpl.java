package com.jecn.epros.server.dao.process.impl;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.epros.server.bean.process.JecnMapLineSegmentT;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.process.IJecnLineSegmentTDao;

public class JecnLineSegmentTDaoImpl extends AbsBaseDao<JecnLineSegmentT, Long> implements IJecnLineSegmentTDao {

	@Override
	public List<JecnLineSegmentT> findAllByFlowId(Long flowId) throws Exception {
		String hql = "select a from JecnLineSegmentT as a,JecnFlowStructureImageT as b where a.figureId = b.figureId and b.flowId=?";
		return this.listHql(hql, flowId);
	}

	/**
	 * 流程架构- 集成关系图 连接线线段集合
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnMapLineSegmentT> findMapRelatedLineById(Long flowId) throws Exception {
		String hql = "select a from JecnMapLineSegmentT as a,JecnMapRelatedStructureImageT as b where a.figureId = b.figureId and b.flowId=?";
		return this.listHql(hql, flowId);
	}
}
