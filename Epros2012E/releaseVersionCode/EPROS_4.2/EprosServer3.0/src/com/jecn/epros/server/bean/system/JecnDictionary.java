package com.jecn.epros.server.bean.system;

import java.io.Serializable;

/**
 * Created by user on 2017/5/24. 字典项
 */
public class JecnDictionary implements Serializable {
	private String id;
	private String name;
	private String transName;
	private String enTransName;
	private Integer code;
	private String value;
	private String company;
	private String validable;
	private String parameter;
	private String enValue;

	public String getEnValue() {
		return enValue;
	}

	public void setEnValue(String enValue) {
		this.enValue = enValue;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTransName() {
		return transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public String getValue(int type) {
		return type == 0 ? value : enValue;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getValidable() {
		return validable;
	}

	public void setValidable(String validable) {
		this.validable = validable;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getEnTransName() {
		return enTransName;
	}

	public void setEnTransName(String enTransName) {
		this.enTransName = enTransName;
	}

}
