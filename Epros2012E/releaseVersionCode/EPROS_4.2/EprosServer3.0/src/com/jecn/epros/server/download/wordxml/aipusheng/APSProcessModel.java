package com.jecn.epros.server.download.wordxml.aipusheng;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

public class APSProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距 1.5倍行距 *****/
	private ParagraphLineRule vLine1_5 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	/**** 段落行距最小值16磅值 ********/
	private ParagraphLineRule vLine_atLeast16 = new ParagraphLineRule(16F, LineRuleType.AT_LEAST);
	/****** 段落行距固定值20磅 *********/
	private ParagraphLineRule lineRule = new ParagraphLineRule(20, LineRuleType.EXACT);

	/**
	 * 默认模版
	 * 
	 * @param processDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public APSProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(17.49F);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle(".", textTitleStyle());
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		String flowName = processDownloadBean.getFlowName();
		Header hdr = sect.createHeader(type);
		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(2.86F, 1.1F);
		logo.setPositionBean(new PositionBean(PositionType.relative, -20F, 0F, 1.76F, 0F));
		Paragraph logoP = new Paragraph(flowName);

		logoP.appendGraphRun(logo);
		hdr.addParagraph(logoP);
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		paragraphBean.setpStyle(PStyle.AF1);
		logoP.setParagraphBean(paragraphBean);

	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "Arial", Constants.xiaowu, false);
		Footer ftr = sect.createFooter(type);
		Paragraph p = ftr.createParagraph("", pBean);
		p.appendCurPageRun();
	}

	// /**
	// * 活动说明
	// *
	// * @param _count
	// * @param name
	// * @param allActivityShowList
	// */
	// protected void a10(ProcessFileItem processFileItem, String[] titles,
	// List<String[]> rowData) {
	// createTitle(processFileItem);
	// // 活动编号
	// String activityNumber = JecnUtil.getValue("activitynumbers");
	// // 执行角色s
	// String executiveRole = JecnUtil.getValue("executiveRole");
	// // 活动名称
	// String eventTitle = JecnUtil.getValue("activityTitle");
	// // 活动说明
	// String timeLimit = "时间（现状－目标）天";
	// float[] width = new float[] { 2F, 2F, 2F, 5, 4F };
	// List<Object[]> newRowData = new ArrayList<Object[]>();
	// // 标题数组
	// newRowData.add(new Object[] { activityNumber, executiveRole, eventTitle,
	// "活动说明", timeLimit });
	// // 添加数据 0:活动编号 1执行角色 2活动名称 3活动说明
	// for (Object[] activeData : processDownloadBean.getAllActivityShowList())
	// {
	// newRowData.add(new Object[] { JecnUtil.objToStr(activeData[0]),
	// JecnUtil.objToStr(activeData[1]),
	// JecnUtil.objToStr(activeData[2]), JecnUtil.objToStr(activeData[3]),
	// JecnUtil.objToStr(activeData[6]) + "-" + JecnUtil.objToStr(activeData[7])
	// });
	// }
	// Table tbl = createTableItem(processFileItem, width,
	// JecnWordUtil.obj2String(newRowData));
	// tbl.getRow(0).setHeader(true);
	// tbl.setRowStyle(0, docProperty.getTblTitleStyle(),
	// docProperty.isTblTitleShadow(), docProperty
	// .isTblTitleCrossPageBreaks());
	// }

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.0F, 1.76F, 1.5F, 1.76F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		createCommhdr(titleSect, HeaderOrFooterType.odd);
		// 流程名称
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		// 编制部门
		String orgName = nullToEmpty(processDownloadBean.getOrgName());
		String dutyName = nullToEmpty(processDownloadBean.getResponFlow().getDutyUserName());
		// 文件编号
		String flowInputNum = nullToEmpty(processDownloadBean.getFlowInputNum());
		String flowVersion = nullToEmpty(processDownloadBean.getFlowVersion());
		ParagraphBean pBean = null;
		for (int i = 0; i < 8; i++) {
			titleSect.createParagraph("");
		}
		// 宋体 小一 加粗 居中 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoyi, true);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph(flowName, pBean);
		for (int i = 0; i < 5; i++) {
			titleSect.createParagraph("");
		}
		// 宋体 四号 左对齐 1.5倍行距
		pBean = new ParagraphBean("宋体", Constants.sihao, false);
		pBean.setSpace(0f, 0f, vLine1_5);
		pBean.setInd(3.7F, 0, 0, 0.25F);
		titleSect.createParagraph("编制部门：" + orgName, pBean);
		titleSect.createParagraph("  所有者：" + dutyName, pBean);
		titleSect.createParagraph("文件编号：" + flowInputNum, pBean);
		titleSect.createParagraph("文件版号：" + flowVersion, pBean);
		for (int i = 0; i < 5; i++) {
			titleSect.createParagraph("");
		}

		// 宋体 五号 左对齐 最小值16磅值
		pBean = new ParagraphBean("宋体", Constants.wuhao, false);
		pBean.setSpace(0f, 0f, vLine_atLeast16);
		TableBean tabBean = new TableBean();
		tabBean.setBorder(1.5F);
		tabBean.setBorderInsideH(1);
		tabBean.setBorderInsideV(1);
		tabBean.setTableLeftInd(0.35F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		List<String[]> rowData = new ArrayList<String[]>();
		// 评审人集合
		/** 评审人集合 0 评审人类型 1名称 2日期 */
		List<Object[]> peopleList = processDownloadBean.getPeopleList();
		String[] str = null;
		rowData.add(new String[] { "审批栏", "姓名", "日期" });
		Object[] audit = new Object[3];
		Object[] control = new Object[3];
		Object[] access = new Object[3];
		for (Object[] obj : peopleList) {
			if (obj[3] != null && obj[3] instanceof Integer) {
				Integer mark = (Integer) obj[3];
				if (mark == 0) {
					audit = obj;
				} else if (mark == 1) {
					control = obj;
				} else if (mark == 4) {
					access = obj;
				}
			}
		}

		str = new String[] { "拟稿人", JecnUtil.objToStr(audit[1]), JecnUtil.objToStr(audit[2]) };
		rowData.add(str);
		str = new String[] { "文控审核人", JecnUtil.objToStr(control[1]), JecnUtil.objToStr(control[2]) };
		rowData.add(str);
		str = new String[] { "批准人", JecnUtil.objToStr(access[1]), JecnUtil.objToStr(access[2]) };
		rowData.add(str);

		Table tab = JecnWordUtil.createTab(titleSect, tabBean, new float[] { 6.36F, 5.36F, 5.36F }, rowData, pBean);
		/******* 设置第一行样式 微软雅黑五号 *********/
		pBean = new ParagraphBean(Align.center, "微软雅黑", Constants.wuhao, true);
		// 设置行样式
		tab.setRowStyle(0, pBean);
		pBean = new ParagraphBean("微软雅黑", Constants.wuhao, true);
		// 设置第一列样式
		tab.setCellStyle(0, pBean);
		tab.getRow(0).setHeader(true);
		/**** 设置行高和 垂直居中 ******/
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.getRow(i).setHeight(0.6F);
			tab.setRowStyle(i, Valign.center);
		}
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.wuhao, false));
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		return null;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, new FontBean("宋体", Constants.wuhao, true));
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.xiaosi, false));
		textContentStyle.setInd(0.52F, 0, 0, 0);
		textContentStyle.setSpace(0f, 0f, lineRule);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.xiaosi, true));
		textTitleStyle.setInd(0, 0, 0.95F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(0.5F, 0.2F, lineRule); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
