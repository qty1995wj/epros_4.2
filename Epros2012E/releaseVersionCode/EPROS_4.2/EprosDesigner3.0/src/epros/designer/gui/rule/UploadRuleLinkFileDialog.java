package epros.designer.gui.rule;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.CommonJTextField;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;

public class UploadRuleLinkFileDialog extends G020UploadRuleFileDialog {
	private static Logger log = Logger.getLogger(UploadRuleLinkFileDialog.class);

	private CommonJTextField ruleNameText;
	private CommonJTextField ruleLinkText;

	/** 制度名称必填提示符 */
	private JLabel ruleNameRequired;
	/** 制度链接必填提示符 */
	private JLabel ruleLinkTextRequired;

	private JButton button;

	public UploadRuleLinkFileDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree);
	}

	public int initRuleLinkCompotents(int rows, JecnPanel infoPanel, Insets insets) {
		this.setSize(getWidthMax(), 600);
		this.setTitle(JecnProperties.getValue("newRuleLink"));
		ruleNameRequired = new JLabel("*");
		ruleLinkTextRequired = new JLabel("*");
		ruleNameRequired.setForeground(Color.red);
		ruleLinkTextRequired.setForeground(Color.red);
		proPanel.setPreferredSize(new Dimension(getWidthMax() - 10, 500));
		ruleNameText = new CommonJTextField(rows, infoPanel, insets, JecnProperties.getValue("ruleNameC"));
		GridBagConstraints c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(5, 3, 5, 5), 0, 0);
		infoPanel.add(ruleNameRequired, c);
		rows++;
		ruleLinkText = new CommonJTextField(rows, infoPanel, insets, JecnProperties.getValue("ruleLinkC"));
		ruleLinkText.getTextField().addFocusListener(
				new JTextFieldHintListener(JecnProperties.getValue("systemLinkSpecification"), ruleLinkText
						.getTextField()));
		button = new JButton(JecnProperties.getValue("openBtn"));
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 3, 5, 5), 0, 0);
		infoPanel.add(button, c);
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// 调用浏览器打开链接
				JecnUtil.openUrl(ruleLinkText.getTextValue());
			}
		});
		c = new GridBagConstraints(4, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 3, 5, 5), 0, 0);
		infoPanel.add(ruleLinkTextRequired, c);
		rows++;
		// initData();
		return rows;
	}

	protected boolean initAndCheckRuleList() {
		verfyLab.setText("");
		RuleT ruleT = setRuleAttrs();
		ruleT.setRuleName(ruleNameText.getTextValue());
		ruleT.setIsFileLocal(2);
		ruleT.setRuleUrl(ruleLinkText.getTextValue());
		return true;
	}

	/**
	 * 编辑时，初始化
	 */
	private void initData() {
		ruleNameText.getTextField().setText(pNode.getJecnTreeBean().getName());

	}

	protected void addRuleFileTable(GridBagConstraints c, Insets insets, int rows) {

	}

	protected void addButtonPanel() {
	}

	protected void addFileSubmit() {
		/*
		 * // 责任人 if
		 * (!personliableSelect.getSingleSelectCommon().validateRequired
		 * (verfyLab)) { return; } // 责任部门 if
		 * (!resDepartSelectCommon.validateRequired(verfyLab)) { return; } ;
		 */
		if (ruleNameText.getTextValue().isEmpty()) {
			verfyLab.setText(JecnProperties.getValue("ruleName") + JecnProperties.getValue("isNotEmpty"));
			return;
		}
		if (ruleLinkText.getTextValue().isEmpty()) {
			verfyLab.setText(JecnProperties.getValue("ruleLink") + JecnProperties.getValue("isNotEmpty"));
			return;
		}
		// 验证制度名称长度
		if (DrawCommon.checkNameMaxLength(ruleNameText.getTextValue())) {
			verfyLab.setText(JecnProperties.getValue("ruleName") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		Pattern pattern = Pattern
				.compile("^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$");
		if (!pattern.matcher(ruleLinkText.getTextValue()).matches()) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("linkFormatIsIncorrect"));
			return;
		}
		list.clear();
		try {
			List<String> strName = new ArrayList<String>();
			strName.add(ruleNameText.getTextValue());
			if (validateNamefullPath(strName)) {
				return;
			}
			// 验证文件是否重复
			String[] ns = ConnectionPool.getRuleAction().validateAddNames(strName, pNode.getJecnTreeBean().getId());
			if (ns != null && ns.length > 0) {
				StringBuffer tip = new StringBuffer();
				for (String n : ns) {
					if (tip.length() == 0) {
						tip.append(n).append(",");
					} else {
						// tip.append("<br>");
						tip.append(n).append(",");
					}

				}
				String tipText = tip.toString().substring(0, tip.toString().length() - 1) + " "
						+ JecnProperties.getValue("haved");
				verfyLab.setText(tipText);
				return;
			}

		} catch (Exception e1) {
			log.error("AddRuleInfoDialog okButPerformed is error", e1);
		}
		// 初始化和校验制度文件列表
		if (!initAndCheckRuleList()) {
			return;
		}
		// 创建制度
		try {
			ConnectionPool.getRuleAction().addRuleFile(getRueFileData());
			List<JecnTreeBean> listNode = ConnectionPool.getRuleAction().getChildRules(pNode.getJecnTreeBean().getId(),
					JecnConstants.projectId);
			// 刷新制度树节点
			JecnTreeCommon.refreshNode(pNode, listNode, jTree);
			JecnTreeCommon.autoExpandNode(jTree, pNode);
			this.dispose();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class JTextFieldHintListener implements FocusListener {
		private String mHintText;
		private JTextField mTextField;

		public JTextFieldHintListener(String hintText, JTextField textField) {
			this.mHintText = hintText;
			this.mTextField = textField;
			textField.setForeground(Color.GRAY);
		}

		@Override
		public void focusGained(FocusEvent e) {
			String temp = mTextField.getText();
			if (temp.equals(mHintText)) {
				mTextField.setText("");
				mTextField.setForeground(Color.BLACK);
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			String temp = mTextField.getText();
			if (temp.equals("")) {
				mTextField.setForeground(Color.GRAY);
				mTextField.setText(mHintText);
			}
		}
	}
}
