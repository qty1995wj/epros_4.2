//流程访问数统计
function processAccessGrid() {
	processAccessGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "processAccess.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "flowId",
								mapping : "processWebBean.flowId"
							}, {
								name : "flowName",
								mapping : "processWebBean.flowName"
							}, {
								name : "flowIdInput",
								mapping : "processWebBean.flowIdInput"
							}, {
								name : "resPeopleName",
								mapping : "processWebBean.resPeopleName"
							}, {
								name : "orgName",
								mapping : "processWebBean.orgName"
							},{
								name : "accessCount",
								mapping : "accessCount"
							}])

			});

	processAccessGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			},{
				header : i_visits,
				dataIndex : "accessCount",
				align : "center",
				menuDisabled : true
			}

	]);
	processAccessGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processAccessGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processAccessGridPanel = new Ext.grid.GridPanel({
				id : "processAccessGridPanel_id",
				renderTo : 'processAccessGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-172,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processAccessGridStore,
				colModel : processAccessGridColumnModel,
				bbar : processAccessGridBbar
			})
	reports_index_id='reports_processAccess_id'; 		
}

/**
 * 流程访问数统计查询
 */
function processAccessGridSearch(){
	 //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_access_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_access_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
	 
	 if((endTime != null&&endTime != "")&&(startTime == null || startTime == "")){
		 // "提示" + "结束时间不能为空！"
		Ext.Msg.alert(i_tip, i_startTimeIsNotNull);
		return;
	 }
	  // 结束时间大于开始时间
	 if (startTime > endTime) {
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return;
	 }
	 if(resOrgId&&resOrgId!=""){
     	processAccessGridStore.setBaseParam("resOrgId", resOrgId);
     }
     if(processId&&processId!=""){
     	processAccessGridStore.setBaseParam("processId", processId);
     }
     processAccessGridStore.setBaseParam("startTime", startTime);
     processAccessGridStore.setBaseParam("endTime", endTime);

	 processAccessGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
				
}

/**
 * 流程访问数统计重置
 **/
function processAccessSearchReset(){
        document.getElementById("orgDutyName_access_id").value = "";
		document.getElementById("processName_access_id").value = "";
		document.getElementById("orgDutyId_access_id").value = "-1";
		document.getElementById("processId_access_id").value = "-1";
		document.getElementById("startTime").value = "";
		document.getElementById("endTime").value = "";
}

// 流程访问数统计下载
function processAccessDownloadExcel(){
     //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_access_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_access_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
     window.location.href="downloadProcessAccess.action?resOrgId="+resOrgId+"&processId="+processId+"&startTime="+startTime+"&endTime="+endTime;
}