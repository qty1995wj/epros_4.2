package com.jecn.epros.server.action.web.login.ad.neusoft;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.SSOLoginMailValidate;
import com.jecn.epros.server.util.JecnPath;

public class NeusoftAfterItem {
	private static Logger log = Logger.getLogger(NeusoftAfterItem.class);

	/** 收件箱地址 */
	private static String PORT = "587";
	private static String SMTP = null;

	public static void start() {
		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/neusoft/NeusoftConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);
			PORT = props.getProperty("port");
			SMTP = props.getProperty("smtp");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		}
	}

	public static boolean loginValidateByEmail(String name, String password) throws Exception {
		return SSOLoginMailValidate.loginValidateByEmail(name, password, PORT, SMTP);
	}

}
