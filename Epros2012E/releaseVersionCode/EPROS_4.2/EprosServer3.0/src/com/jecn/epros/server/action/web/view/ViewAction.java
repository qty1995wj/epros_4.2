package com.jecn.epros.server.action.web.view;

import java.sql.Clob;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.process.IViewAppletService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * 浏览端：流程图显示对应Action
 * 
 * @author yxw 2012-9-25
 * @description：图形显示Action
 */
public class ViewAction extends BaseAction {
	public final Log log = LogFactory.getLog(ViewAction.class);

	private IViewAppletService viewAppletService;

	/** 流程图主键ID */
	private long mapID = -1;
	/** 图形类型: totalMap:流程地图;partMap:流程图;orgMap:组织图 */
	private String mapType = null;
	/** 是否查询发布数据："true"或空查询正式表，"false"查询临时表 */
	private String isPub = null;
	/** 岗位组Id */
	private long posGroupId = -1;

	/**
	 * @author weidp
	 * @description 通过岗位组Id获得所属的岗位信息
	 * @return
	 */
	public String obtainPositionsByGroupId() {
		try {
			if (posGroupId == -1) {
				return null;
			}
			List<Object[]> objList = null;
			objList = viewAppletService.getPositionsByPosGroupId(posGroupId);
			getPositionList(objList);
			Gson gson = new Gson();
			String resultList = gson.toJson(objList);
			outJsonString(resultList);
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * 转换获取的岗位信息为字符串类型（Gson读取数据时，会将int,long读取成Double类型，所以提前进行转换）
	 *
	 * @author weidp 
	 * @date： 日期：2014-4-24 时间：上午10:37:52
	 * @param objList
	 */
	private void getPositionList(List<Object[]> objList) {
		if (objList == null || objList.size() == 0) {
			return;
		}
		for (Object[] objects : objList) {
			if (objects == null || objects.length == 0) {
				continue;
			}
			for (Object object : objects) {
				object = valueOf(object);
			}
		}
	}

	/**
	 * 
	 * 获取流程图/流程地图/组织图
	 * 
	 * @return String 流程图/流程地图/组织图数据JSON字符串
	 */
	public String getMap() {
		try {
			// 执行DB查询
			List<List<Object[]>> objList = null;

			if ("totalMap".equals(mapType)) {// 流程地图
				objList = getTotalMap();
			} else if ("partMap".equals(mapType)) {// 流程图
				objList = getPartMap();
			} else if ("orgMap".equals(mapType)) {// 组织图
				objList = getOrgMap();
			} else {
				return null;
			}

			log.info("查询DB结束，开始转换JSON格式字符串......");
			if (objList == null) {
				return null;
			}

			getList(objList);

			// String result = JSONArray.fromObject(objList).toString();
			Gson gson = new Gson();
			String result = gson.toJson(objList);

			log.info("结束转换JSON格式字符串......");
			log.info(result);
			// 传输数据
			this.getResponse().setCharacterEncoding("utf-8");
			this.getResponse().getWriter().print(result);
			return null;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	/**
	 * 
	 * 获取流程地图数据
	 * 
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	private List<List<Object[]>> getTotalMap() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
				.getSession().get("webLoginBean");
		if ("false".equals(isPub)) {// 查临时表
			return viewAppletService.getTotalMapT(mapID, webLoginBean
					.getJecnUser().getPeopleId(), webLoginBean.getProjectId());
		}
		// 执行DB查询
		return viewAppletService.getTotalMap(mapID, webLoginBean.getJecnUser()
				.getPeopleId(), webLoginBean.getProjectId());
	}

	/**
	 * 
	 * 获取流程图数据
	 * 
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	private List<List<Object[]>> getPartMap() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
				.getSession().get("webLoginBean");
		if ("false".equals(isPub)) {// 查临时表
			return viewAppletService.getPartMapT(mapID, webLoginBean
					.getJecnUser().getPeopleId(), webLoginBean.getProjectId());
		}
		// 执行DB查询
		return viewAppletService.getPartMap(mapID, webLoginBean.getJecnUser()
				.getPeopleId(), webLoginBean.getProjectId());
	}

	/**
	 * 
	 * 获取组织图数据
	 * 
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	private List<List<Object[]>> getOrgMap() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
				.getSession().get("webLoginBean");
		// 执行DB查询
		return viewAppletService.getOrgMap(mapID, webLoginBean.getJecnUser()
				.getPeopleId(), webLoginBean.getProjectId());
	}

	/**
	 * 
	 * 转换成字符串格式的集合
	 * 
	 * @param objList
	 *            List<List<Object[]>> DB值
	 */
	private void getList(List<List<Object[]>> objList) {
		if (objList == null || objList.size() == 0) {
			return;
		}

		for (List<Object[]> sedList : objList) {
			if (objList == null || sedList.size() == 0) {
				continue;
			}
			for (Object[] objArr : sedList) {
				if (objList == null) {
					continue;
				}
				for (int i = 0; i < objArr.length; i++) {
					Object tmm = objArr[i];
					if (tmm != null && tmm instanceof Clob) {
						try {
							Clob clob = (Clob) tmm;
							objArr[i] = clob.getSubString(1L, (int) clob
									.length());

						} catch (Exception e) {
							log.error("clob转换成字符串出错！", e);
							objArr[i] = "";
						}
					} else {
						objArr[i] = valueOf(tmm);
					}
				}
			}
		}
	}

	/**
	 * 
	 * 对象转换成字符串,如果为空返回“”
	 * 
	 * @param obj
	 *            Object
	 * @return String 字符串值或“”
	 */
	private String valueOf(Object obj) {
		return (obj == null) ? "" : obj.toString();
	}

	public void setViewAppletService(IViewAppletService viewAppletService) {
		this.viewAppletService = viewAppletService;
	}

	public long getMapID() {
		return mapID;
	}

	public void setMapID(long mapID) {
		this.mapID = mapID;
	}

	public String getMapType() {
		return mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}

	public String getIsPub() {
		return isPub;
	}

	public void setIsPub(String isPub) {
		this.isPub = isPub;
	}

	public Long getPosGroupId() {
		return posGroupId;
	}

	public void setPosGroupId(Long posGroupId) {
		this.posGroupId = posGroupId;
	}

}
