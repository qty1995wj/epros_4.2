package com.jecn.epros.server.service.task.app.impl;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.external.TaskParam;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.SimpleTaskBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;

/**
 * 文件任务审批，业务处理类
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Nov 27, 2012 时间：10:50:54 AM
 */
@Transactional(rollbackFor = Exception.class)
public class JecnFileTaskServiceImpl extends JecnCommonTaskService implements IJecnBaseTaskService {
	/**
	 * 获取文件信息
	 * 
	 * @param fileId
	 *            文件主键ID （目前文件任务只存在提交审批）
	 * @param simpleTaskBean
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public void getFileInfomation(SimpleTaskBean simpleTaskBean) throws Exception {
		JecnTaskBeanNew taskBeanNew = simpleTaskBean.getJecnTaskBeanNew();
		if (taskBeanNew == null) {//
			log.error("JecnFileTaskServiceImpl类getFileInfomation方法中参数为空!");
			throw new IllegalArgumentException("参数不能为空!");
		}
		Long fileId = taskBeanNew.getRid();
		// 文件信息
		JecnFileBeanT jecnFileBeanT = fileDao.get(fileId);
		if (jecnFileBeanT == null) {
			log.error("JecnFileTaskServiceImpl类getFlowInfomation方法中文件不存在!");
			return;
		}
		if (jecnFileBeanT.getPerFileId() != null) {// 是否存在父节点。存在获取父节点名称
			JecnFileBeanT preFileBeanT = fileDao.get(jecnFileBeanT.getPerFileId());
			// PRF父节点名称
			simpleTaskBean.setPrfPreName(preFileBeanT.getFileName());
		}
		// 当前节点名称
		simpleTaskBean.setPrfName(jecnFileBeanT.getFileName());
		// 设置密级
		if (jecnFileBeanT.getIsPublic() != null) {
			simpleTaskBean.setStrPublic(jecnFileBeanT.getIsPublic().toString());
		}
		// 设置编号
		simpleTaskBean.setPrfNumber(jecnFileBeanT.getDocId());
	}

	/**
	 * 文件审批变动
	 * 
	 * @param submitMessage
	 *            提交任务信息
	 * @param taskBeanNew
	 *            任务主表
	 * @throws Exception
	 */

	public String updateAssembledFileTaskFixed(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew)
			throws Exception {
		Long rid = taskBeanNew.getRid();
		JecnFileBeanT fileBeanT = fileDao.get(rid);
		// 任务的显示项
		JecnTaskApplicationNew jecnTaskApplicationNew = abstractTaskDao.getJecnTaskApplicationNew(taskBeanNew.getId());
		// 密级 0公开：1 ； 秘密
		Long isPublic = fileBeanT.getIsPublic();
		String taskFixedDesc = JecnTaskCommon.TASK_NOTHIN;
		// 文控审核和部门审核
		if (taskBeanNew.getUpState() == 1 || taskBeanNew.getUpState() == 2) {
			// 获取审批变动
			taskFixedDesc = assembledPrfTaskFixed(submitMessage, taskBeanNew, jecnTaskApplicationNew, null, isPublic);
		}
		if (taskFixedDesc.equals(JecnTaskCommon.TASK_NOTHIN)) {// 无变动
			return taskFixedDesc;
		}
		if (!JecnCommon.isNullOrEmtryTrim(submitMessage.getStrPublic())) {
			fileBeanT.setIsPublic(Long.valueOf(submitMessage.getStrPublic().trim()));
			abstractTaskDao.getSession().update(fileBeanT);
			abstractTaskDao.getSession().flush();
		}
		return taskFixedDesc;
	}

	@Override
	public void createPRFTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * 打开试用行报告
	 * 
	 * @param id
	 *            任务主键ID
	 * @return JecnTaskFileTemporary 试运行文件
	 * @throws Exception
	 */
	@Override
	public JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception {
		return super.downLoadTaskRunFile(id);
	}

	/**
	 * 获取任务信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param taskOperation
	 *            我的任务点击操作 0审批;1重新提交;2整理意见;3任务管理，编辑任务
	 * @return SimpleTaskBean 页面获取的任务相关信息
	 * @throws Exception
	 */
	@Override
	// @Transactional(isolation = Isolation.SERIALIZABLE)
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public SimpleTaskBean getSimpleTaskBeanById(Long taskId, int taskOperation) throws Exception {
		return super.getSimpleTaskBeanById(taskId, taskOperation);
	}

	/**
	 * 验证当前登录人是否为任务审批人
	 * 
	 * @param peopleId
	 *            登录人人员主键ID
	 * @param taskId
	 *            任务主键ID
	 * @return True 是当前任务审批人；false：不是当前任务审批人
	 * @throws Exception
	 */

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public boolean isApprovePeopleChecked(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception {
		return super.isApprovePeopleChecked(peopleId, taskId, taskElseState, approveCounts);
	}

	/**
	 * 审批通过
	 * 
	 * @param submitMessage
	 *            审核人提交信息
	 * 
	 */

	@Override
	public void taskOperation(SimpleSubmitMessage submitMessage) throws Exception {
		super.taskOperation(submitMessage);
	}

	/**
	 * 当前任务，当前操作人是否已审批任务
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param curPeopleId
	 *            当前操作人主键ID
	 * @param taskOperation
	 *            我的任务操作人的操作状态 0审批;1重新提交;2整理意见;3任务管理，编辑任务
	 * @return true 当前操作人已审批，或无审批权限
	 * @throws Exception
	 */
	@Override
	public boolean hasApproveTask(Long taskId, Long curPeopleId, int taskOperation) throws Exception {
		return super.hasApproveTask(taskId, curPeopleId, taskOperation);
	}

	/**
	 * 检测任务是否搁置
	 * 
	 * @param simpleTaskBean
	 *            任务信息
	 * @return
	 * @throws Exception
	 */
	@Override
	public SimpleTaskBean checkTaskIsDelay(SimpleTaskBean simpleTaskBean) throws Exception {
		return super.checkTaskIsDelay(simpleTaskBean);
	}

	@Override
	public void releaseTaskByWebService(Long taskId, List<JecnTaskHistoryFollow> taskHistorys) throws Exception {
		super.releaseTaskByWebService(taskId, taskHistorys);
	}
	
	@Override
	public MessageResult handleTaskExternal(TaskParam param) {
		return super.handleTaskExternal(param);
	}

}
