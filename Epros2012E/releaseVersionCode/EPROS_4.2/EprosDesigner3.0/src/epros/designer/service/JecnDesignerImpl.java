package epros.designer.service;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.tree.TreePath;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.process.ActivitySaveData;
import com.jecn.epros.server.bean.process.ActivityUpdateData;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowFigure;
import com.jecn.epros.server.bean.process.JecnFlowFigureNow;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnRoleActiveT;
import com.jecn.epros.server.bean.process.JecnToFromActiveT;
import com.jecn.epros.server.bean.process.ProcessData;
import com.jecn.epros.server.bean.process.ProcessFileImageFigureData;
import com.jecn.epros.server.bean.process.ProcessMapData;
import com.jecn.epros.server.bean.process.ProcessMapFigureData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.RoleFigureData;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.JecnDesignerFrame;
import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.ConfigCommon.ConfigCommonEnum;
import epros.designer.gui.error.ErrorCommon;
import epros.designer.gui.header.JecnDesignerModuleShow;
import epros.designer.gui.pagingImage.PageUtil;
import epros.designer.gui.pagingImage.PagingImageUtil;
import epros.designer.gui.pagingImage.SplitWorkflowDialog;
import epros.designer.gui.process.activitiesProperty.ActivitiesDetailsDialog;
import epros.designer.gui.process.activitiesProperty.DesignerActivityCodeBuildDialog;
import epros.designer.gui.process.flow.AddFlowDialog;
import epros.designer.gui.process.flowmap.AddFlowMapDialog;
import epros.designer.gui.system.ResourceHighEfficiencyTree;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.gui.workflow.JecnWorkflowMenuItem;
import epros.designer.gui.workflow.WorkFlowBakTimer;
import epros.designer.gui.workflow.WorkFlowTimer;
import epros.designer.service.process.JecnOrganization;
import epros.designer.service.process.JecnProcess;
import epros.designer.service.process.JecnSaveOrg;
import epros.designer.service.process.JecnSaveProcess;
import epros.designer.service.process.JecnTemplate;
import epros.designer.service.system.JecnElementAttrsbuteCommon;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnImplData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.designer.JecnActiveStandardBeanT;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnControlPointT;
import epros.draw.designer.JecnFigureFileTBean;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.designer.JecnRefIndicatorsT;
import epros.draw.gui.designer.JecnDesigner;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.designer.JecnIDesigner;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.operationConfig.ConfigFileConPanyPanel;
import epros.draw.gui.operationConfig.DrawOperationDescriptionConfigBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.JecnFlowExport;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.top.toolbar.io.JecnIOUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnImageUtil;
import epros.draw.util.JecnResourceUtil;
import epros.tray.JecnProcessD2;

/**
 * 
 * 和画图工具交互类
 * 
 * 设计器功能但是需要在画图工具上添加代码，从而出现此类
 * 
 * @author Administrator
 * 
 */
public class JecnDesignerImpl implements JecnIDesigner {
	private static Logger log = Logger.getLogger(JecnDesignerImpl.class);
	private SplitWorkflowDialog splitWorkflow;

	@Override
	public String saveMap() {
		try {
			// 进行备份操作 v3.06
			WorkFlowBakTimer.getInstance().runningBackUp();

			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

			if (workflow.getFlowMapData().isAuthSave() != 1) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("ON_PANEL_JUR"));
				return null;
			}
			JecnFlowMapData flowMapData = workflow.getFlowMapData();
			// 获取所有图形
			List<JecnBaseFlowElementPanel> allFigureList = workflow.getPanelList();
			if (flowMapData.getMapType() == MapType.orgMap) {
				// 存储所有岗位名称的List
				List<String> posNameList = new ArrayList<String>();
				boolean isSaveUpdate = false;
				boolean isPosNameNull = false;
				for (JecnBaseFlowElementPanel baseFlowElementPanel : allFigureList) {
					boolean isAdd = true;
					if (baseFlowElementPanel.getFlowElementData().getMapElemType() == MapElemType.PositionFigure) {
						if (baseFlowElementPanel.getFlowElementData().getFigureText() == null
								|| "".equals(baseFlowElementPanel.getFlowElementData().getFigureText().trim())) {
							isPosNameNull = true;
						}
						if (!isPosNameNull) {
							for (String posName : posNameList) {
								if (posName.equals(baseFlowElementPanel.getFlowElementData().getFigureText())) {
									isAdd = false;
									isSaveUpdate = true;
								}
							}
							if (isAdd) {
								posNameList.add(baseFlowElementPanel.getFlowElementData().getFigureText());
							}
						}
					}
				}
				if (isPosNameNull) {
					JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties
							.getValue("postNameCannotBeEmpty"), null, JecnOptionPane.ERROR_MESSAGE);
					return JecnProperties.getValue("postNameCannotBeEmpty");
				}

				// 读取配置是否允许岗位名称相同 0:不允许 1：允许
				Long allowSamePosName = null;
				try {
					allowSamePosName = Long
							.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "allowSamePosName"));
				} catch (Exception e) {
					log.error("JecnDesignerImpl rows：180 is error", e);
				}
				if (allowSamePosName == 0) {
					if (isSaveUpdate) {
						JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties
								.getValue("positionNameRepeat"), null, JecnOptionPane.ERROR_MESSAGE);
						return JecnProperties.getValue("positionNameRepeat");
					}
				}
			}
			MapType mapType = flowMapData.getMapType();
			// 流程是否被占用
			switch (mapType) {
			case partMap:// 流程图
				if (!JecnDesignerCommon.canOpen(flowMapData.getFlowId(), TreeNodeType.process, null)) {
					return null;
				}
				break;
			case totalMap:// 流程地图
				if (!JecnDesignerCommon.canOpen(flowMapData.getFlowId(), TreeNodeType.processMap, null)) {
					return null;
				}
				break;
			case orgMap:// 组织图
				if (!JecnDesignerCommon.canOpen(flowMapData.getFlowId(), TreeNodeType.organization, null)) {
					return null;
				}
				break;
			case totalMapRelation:// 集成关系图
				if (!JecnDesignerCommon.canOpen(flowMapData.getFlowId(), TreeNodeType.processMapRelation, null)) {
					return null;
				}
				break;
			default:
				break;
			}

			// 回到原始视图
			// 2019-3-13注释 ：需求原因 保存时候 不返回到 原始视图大小
			// workflow.getWorkflowZoomProcess().zoomByNewWorkflowScale(1.0);

			// 设置鼠标状态为连接线
			JecnDrawMainPanel.getMainPanel().getBoxPanel().setSelectedBtnType(MapElemType.guide);
			int h = workflow.getScrollPanle().getHorizontalScrollBar().getValue();
			int v = workflow.getScrollPanle().getVerticalScrollBar().getValue();
			// 移除角色移动
			workflow.removeJecnRoleMobile();
			// 面板大小
			JecnSelectFigureXY figureXY = JecnWorkflowUtil.getMaxSize(workflow, allFigureList);
			// 面板存在错误数据时删除面板错误状态的panel
			if (workflow.getErrorPanelList().size() > 0) {
				ErrorCommon.INSTANCE.clearErrorPanel();
			}

			// 清空面板分页符连接线
			PageUtil.removePageSetLine();
			if (workflow.getTipPanel() != null) {
				workflow.getTipPanel().setVisible(false);
			}
			// 检查面板
			workflow.validate();
			// 收集图片数据
			// 转换为byte
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				// 获取image缓冲区
				BufferedImage image = JecnImageUtil.saveWorkflowDrawImage(workflow, figureXY.getMaxX(), figureXY
						.getMaxY(), 1.0, 1.0);
				if (image != null) {
					ImageIO.write(image, "jpg", out);
				} else {
					log.error("image = null 文件写入异常");
				}
			} catch (Exception e) {
				log.error("文件写入异常", e);
			} finally {
				try {
					out.close();
				} catch (IOException e1) {
					if (out != null) {
						try {
							out.close();
						} catch (IOException e) {
							log.error("", e);
						}
					}
					log.error("JecnDesignerImpl file is error！", e1);
				}
			}
			byte[] data = out.toByteArray();
			String s = null;
			switch (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType()) {
			case partMap:// 流程图
				s = savePartMap(data);
				// checkRole(flowMapData.getFlowId());
				break;
			case totalMap:// 流程地图
				s = saveTotalMap(data);
				break;
			case orgMap:// 组织图
				s = saveOrgMap();
				break;
			case totalMapRelation:// 集成关系图
				s = saveRelatedMap(data);
				break;
			}

			// 回到原始倍数
			// workflow.getWorkflowZoomProcess().zoomByNewWorkflowScale(
			// workflowScale);
			// 如果是流程图且显示角色移动时: 删除之前角色移动添加新的角色移动
			workflow.removeAddRoleMove();
			workflow.getScrollPanle().getHorizontalScrollBar().setValue(h);
			workflow.getScrollPanle().getVerticalScrollBar().setValue(v);
			Point point = null;
			if (workflow.getFlowMapData().isHFlag()) {
				// 设置完成后滚动条位置
				int rh = workflow.getScrollPanle().getHorizontalScrollBar().getValue();
				point = new Point(rh, 0);

			} else {
				int rv = workflow.getScrollPanle().getVerticalScrollBar().getValue();
				point = new Point(0, rv);
			}
			// 设置角色移动位置
			if (workflow.getRoleMobile() != null) {
				workflow.getRoleMobile().getRoleMove().setLocation(point);
			}
			workflow.repaint();
			return s;
		} catch (Exception e) {
			log.error("JecnDesignerImpl  is error!", e);
			return "1";
		}
	}

	// private void checkRole(Long flowId) {
	// if (!JecnConfigTool.checkRoleEnable() || flowId == null) {
	// return;
	// }
	// RoleCheckDialog dialog = null;
	// try {
	// dialog = new RoleCheckDialog(flowId);
	// if (dialog.hasUnRelatedRole()) {
	// dialog.setVisible(true);
	// } else {
	// dialog.dispose();
	// }
	// } catch (Exception e) {
	// log.error("", e);
	// }
	// }

	public String savePartMap(byte[] data) {
		Long flowId = null;

		// 发布时提示信息
		boolean isError = ErrorCommon.INSTANCE.getErrorFigure(true);
		// 流程文件错误信息
		List<String> strList = null;
		try {
			strList = ErrorCommon.INSTANCE.getStrList();
		} catch (Exception e1) {
			e1.printStackTrace();
			log.error("JecnDesignerImpl savePartMap is error！", e1);
			return "1";
		}
		if (isError) {// 存在错误信息 更新流程错误信息表
			ErrorCommon.INSTANCE.clearErrorPanel();
		}
		try {
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			/** ***********执行保存前验证流程图属性规范*************** */
			if (desktopPane == null) {
				return "1";
			} else if (desktopPane.getErrorPanelList().size() > 0) {
				ErrorCommon.INSTANCE.clearErrorPanel();
			}
			JecnFlowMapData flowMapData = desktopPane.getFlowMapData();
			// 流程图原始图数据
			List<JecnFigureData> oldFigureDataList = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
					.getDesignerData().getOldFigureList();
			// 流程图原始线数据
			List<JecnBaseLineData> oldLineDataList = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
					.getDesignerData().getOldLineList();
			// 重置层级
			JecnDrawMainPanel.getMainPanel().getWorkflow().getGraphicsLevel().saveResetLayer();
			// 流程主表
			JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(
					flowMapData.getFlowId());
			flowId = flowStructureT.getFlowId();
			// 分页数
			flowStructureT.setPagings(desktopPane.getFlowMapData().getPageSetingBean().getPageSets());
			// 最后一页坐标
			flowStructureT.setXpoint(desktopPane.getFlowMapData().getPageSetingBean().getPageX());
			// 横纵向
			flowStructureT.setImagvh(desktopPane.getFlowMapData().getPageSetingBean().isPagingVh() == true ? 0 : 1);
			// 是否存在错误信息
			flowStructureT.setErrorState(isError ? 1 : 0);

			// 流程基本信息表
			JecnFlowBasicInfoT jecnFlowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(
					flowMapData.getFlowId());
			if (!desktopPane.getFlowMapData().isHFlag()) {
				jecnFlowBasicInfoT.setIsXorY(1L);
			} else {
				jecnFlowBasicInfoT.setIsXorY(0L);
			}
			// 获取面板数据
			JecnFlowMapData flowMap = desktopPane.getFlowMapData();
			// 目的
			jecnFlowBasicInfoT.setFlowPurpose(flowMap.getFlowOperationBean().getFlowPurpose());
			// 适用范围
			jecnFlowBasicInfoT.setApplicability(flowMap.getFlowOperationBean().getApplicability());
			// 术语定义
			jecnFlowBasicInfoT.setNoutGlossary(flowMap.getFlowOperationBean().getNoutGlossary());
			// 输入
			jecnFlowBasicInfoT.setInput(flowMap.getFlowOperationBean().getFlowInput());
			// 输出
			jecnFlowBasicInfoT.setOuput(flowMap.getFlowOperationBean().getFlowOutput());
			// 起始活动
			jecnFlowBasicInfoT.setFlowStartpoint(flowMap.getFlowOperationBean().getFlowStartActive());
			// 终止活动
			jecnFlowBasicInfoT.setFlowEndpoint(flowMap.getFlowOperationBean().getFlowEndActive());
			// 不适用范围
			jecnFlowBasicInfoT.setNoApplicability(flowMap.getFlowOperationBean().getNoApplicability());
			// 概况信息
			jecnFlowBasicInfoT.setFlowSummarize(flowMap.getFlowOperationBean().getFlowSummarize());
			// 补充说明
			jecnFlowBasicInfoT.setFlowSupplement(flowMap.getFlowOperationBean().getFlowSupplement());
			// 客户
			if (JecnConfigTool.processCustomShowType() == 1) {// 数据来源于流程图中流程客户图标的名称
				jecnFlowBasicInfoT.setFlowCustom(JecnDesignerCommon.getCustomValue());
			} else {
				jecnFlowBasicInfoT.setFlowCustom(flowMap.getFlowOperationBean().getFlowCustom());
			}
			if (isError || (strList != null && strList.size() > 0)) {// 0:错误
				// 1:正确
				flowStructureT.setErrorState(0);
			} else {
				flowStructureT.setErrorState(1);
			}
			// 需要添加的数据
			List<JecnFlowStructureImageT> insertFlowElementList = new ArrayList<JecnFlowStructureImageT>();
			// 需要修改的数据
			List<JecnFlowStructureImageT> updateFlowElementList = new ArrayList<JecnFlowStructureImageT>();
			// 需要删除的数据
			List<String> deleteFigureDataList = new ArrayList<String>();
			// 需要删除的线数据
			List<String> deleteLineDataList = new ArrayList<String>();
			// 保存活动数据
			List<JecnFigureData> activeFigureDataList = new ArrayList<JecnFigureData>();
			// 保存角色数据
			List<JecnFigureData> roleFigureDataList = new ArrayList<JecnFigureData>();
			// 保存分割线数据
			List<JecnBaseLineData> vHLineDataDataList = new ArrayList<JecnBaseLineData>();

			// 保存的活动
			List<ActivitySaveData> activitySaveList = new ArrayList<ActivitySaveData>();
			// 更新的活动
			List<ActivityUpdateData> activityUpdateList = new ArrayList<ActivityUpdateData>();
			// 删除的活动
			List<String> activiteDeleteList = new ArrayList<String>();

			// 保存的角色
			List<RoleFigureData> roleSaveList = new ArrayList<RoleFigureData>();
			// 更新的角色
			List<RoleFigureData> roleUpdateList = new ArrayList<RoleFigureData>();
			// 删除的角色
			List<String> roleDeleteList = new ArrayList<String>();

			// 活动和to、from关联关系
			List<JecnFigureData> toFromFigureDataList = new ArrayList<JecnFigureData>();

			/** **************** 【关联文件链接 集合】 **************************** */
			/** 添加元素集合 */
			List<ProcessFileImageFigureData> saveFileImageDataList = new ArrayList<ProcessFileImageFigureData>();
			/** 修改元素集合 */
			List<ProcessFileImageFigureData> updateFileImageDataList = new ArrayList<ProcessFileImageFigureData>();

			// 图形的数据
			if (flowMapData.getFigureList() != null) {
				for (JecnFigureData figureData : flowMapData.getFigureList()) {
					JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();
					// 判断是否为活动
					if (DrawCommon.isActive(figureData.getMapElemType())) {
						// 用了判断角色和活动关联
						activeFigureDataList.add(figureData);
					} else if (DrawCommon.isRole(figureData.getMapElemType())) { // 判断是否为角色,虚拟角色，客户
						// 用了判断角色和活动关联
						roleFigureDataList.add(figureData);
					} else if (figureData.getMapElemType() == MapElemType.ToFigure
							|| figureData.getMapElemType() == MapElemType.FromFigure) {
						toFromFigureDataList.add(figureData);
					}

					// -1 代表为图形主键ID的默认值
					if (figureData.getUUID() == null) {
						JecnSaveProcess.getFlowStructureImageTtoFigureData(figureData, flowId, flowStructureImageT);
						if (DrawCommon.isActive(figureData.getMapElemType())) { // 判断是否为活动
							// 转换为活动的Data
							JecnActiveData activeData = (JecnActiveData) figureData;
							// 创建活动数据Bean
							ActivitySaveData activitySaveData = new ActivitySaveData();
							// 活动保存的图形数据
							activitySaveData.setJecnFlowStructureImageT(flowStructureImageT);
							// 时间轴
							flowStructureImageT.setTargetValue(activeData.getTargetValue());
							flowStructureImageT.setStatusValue(activeData.getStatusValue());
							flowStructureImageT.setExplain(activeData.getExplain());

							// 活动输出
							activitySaveData.setListModeFileT(JecnSaveProcess.switchJecnModeFileT(activeData
									.getListModeFileT(), activeData.getUUID()));
							// 活动输入
							activitySaveData.setListJecnActivityFileT(JecnSaveProcess.switchActivityFileT(activeData
									.getListJecnActivityFileT(), activeData.getUUID()));
							// 活动指标
							activitySaveData.setListRefIndicatorsT(JecnSaveProcess.switchJecnRefIndicatorsT(activeData
									.getListRefIndicatorsT(), activeData.getUUID()));
							// 活动线上信息
							activitySaveData.setListJecnActiveOnLineTBean(JecnSaveProcess
									.switchLocalJecnActiveOnLineTBean(activeData.getListJecnActiveOnLineTBean(),
											activeData.getUUID()));
							// 活动相关标准
							activitySaveData.setListJecnActiveStandardBeanT(JecnSaveProcess
									.switchLocalJecnActiveStandardBeanT(activeData.getListJecnActiveStandardBeanT(),
											activeData.getUUID()));
							// 活动相关控制点C 修改过
							activitySaveData.setListControlPoint(JecnSaveProcess.swithLocalJecnActiveControlPoint(
									activeData.getJecnControlPointTList(), activeData.getUUID()));

							// 新版 活动输入输出
							activitySaveData.setListFigureInTs(JecnSaveProcess.switchJecnFigureInOutT(activeData
									.getListFigureInTs(), activeData.getUUID()));

							activitySaveData.setListFigureOutTs(JecnSaveProcess.switchJecnFigureInOutT(activeData
									.getListFigureOutTs(), activeData.getUUID()));

							activitySaveList.add(activitySaveData);
						} else if (DrawCommon.isRole(figureData.getMapElemType())) { // 判断是否为角色
							// 转换为角色的Data
							JecnRoleData roleData = (JecnRoleData) figureData;
							RoleFigureData roleFigureData = new RoleFigureData();
							// 角色保存的图形数据
							roleFigureData.setJecnFlowStructureImageT(flowStructureImageT);
							// 角色与岗位关联
							roleFigureData.setListRolePositionInsert(JecnSaveProcess.switchJecnFlowStationT(roleData
									.getFlowStationList(), roleData.getUUID()));
							roleSaveList.add(roleFigureData);
						} else if (figureData.getMapElemType() == MapElemType.FileImage) {// 文档，可关联文件
							// 保存文件相关附件数据对象
							saveFileImageDataList.add(getProcessFileImageFigureData(figureData, flowStructureImageT));
						} else if (DrawCommon.isStandardImpl(figureData.getMapElemType())) {// 接口
							JecnImplData implData = (JecnImplData) figureData;
							flowStructureImageT.setImplName(implData.getImplName());
							flowStructureImageT.setImplNote(implData.getImplNote());
							flowStructureImageT.setProcessRequirements(implData.getProcessRequirements());
							insertFlowElementList.add(flowStructureImageT);
						} else {
							insertFlowElementList.add(flowStructureImageT);
						}
						continue;
					}
				}
				if (oldFigureDataList != null) {
					for (JecnFigureData oldFigureData : oldFigureDataList) {
						JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();
						// 是否需要删除标识
						boolean isDelete = true;
						// 循环内存中存放的图形数据集合
						for (JecnFigureData figureData : flowMapData.getFigureList()) {
							if (figureData.getUUID().equals(oldFigureData.getUUID())) {
								if (figureData.getUUID() == null) {// 新增元素
									continue;
								}
								if (DrawCommon.isActive(figureData.getMapElemType())) { // 判断是否为活动
									ActivityUpdateData activityUpdateData = new ActivityUpdateData();
									// 判断图形是否更新（不判断 输入输出 指标类的数据）
									boolean isUpdate = JecnSaveProcess.isUpdateFlowElementData(figureData,
											oldFigureData, flowId, flowStructureImageT);
									if (isUpdate) {
										activityUpdateData.setJecnFlowStructureImageT(flowStructureImageT);
										JecnActiveData activeData = (JecnActiveData) figureData;
										// 时间轴
										flowStructureImageT.setTargetValue(activeData.getTargetValue());
										flowStructureImageT.setStatusValue(activeData.getStatusValue());
										flowStructureImageT.setExplain(activeData.getExplain());
										JecnSaveProcess.getFlowStructureImageTtoFigureData(figureData, flowId,
												activityUpdateData.getJecnFlowStructureImageT());
									}
									activityUpdateData = JecnSaveProcess.isActiveUpdate(figureData, oldFigureData,
											flowId, activityUpdateData);
									activityUpdateList.add(activityUpdateData);

								} else if (DrawCommon.isRole(figureData.getMapElemType())) { // 判断是否为角色
									RoleFigureData roleFigureData = new RoleFigureData();
									roleFigureData.setJecnFlowStructureImageT(flowStructureImageT);
									boolean isRoleUpdate = JecnSaveProcess.isRoleUpdate(figureData, oldFigureData,
											flowId, roleFigureData);
									if (isRoleUpdate) {
										roleUpdateList.add(roleFigureData);
									}
								} else if (figureData.getMapElemType() == MapElemType.FileImage) {// 文档
									boolean isUpdate = JecnSaveProcess.isUpdateFlowElementData(figureData,
											oldFigureData, flowId, flowStructureImageT);
									ProcessFileImageFigureData imageFigureData = JecnSaveProcess
											.isFileImageUpdate(figureData);
									if (isUpdate) {
										imageFigureData.setJecnFlowStructureImageT(flowStructureImageT);
									}
									updateFileImageDataList.add(imageFigureData);
								} else if (DrawCommon.isImplFigure(figureData.getMapElemType())) {// 接口
									boolean isUpdate = JecnSaveProcess.isUpdateFlowElementData(figureData,
											oldFigureData, flowId, flowStructureImageT);
									if (isUpdate) {
										JecnImplData implData = (JecnImplData) figureData;
										flowStructureImageT.setImplName(implData.getImplName());
										flowStructureImageT.setImplNote(implData.getImplNote());
										flowStructureImageT.setProcessRequirements(implData.getProcessRequirements());
										updateFlowElementList.add(flowStructureImageT);
									}
								} else {
									boolean isUpdate = JecnSaveProcess.isUpdateFlowElementData(figureData,
											oldFigureData, flowId, flowStructureImageT);
									if (isUpdate) {
										updateFlowElementList.add(flowStructureImageT);
									}
								}
								isDelete = false;
								break;
							}
						}
						// 需要删除的数据
						if (isDelete) {
							if (DrawCommon.isActive(oldFigureData.getMapElemType())) { // 判断是否为活动
								activiteDeleteList.add(oldFigureData.getUUID());
							} else if (DrawCommon.isRole(oldFigureData.getMapElemType())) { // 判断是否为角色
								roleDeleteList.add(oldFigureData.getUUID());
							} else {
								deleteFigureDataList.add(oldFigureData.getUUID());
							}
						}

					}
				}
			}

			// 线的数据
			if (flowMapData.getLineList() != null) {
				for (JecnBaseLineData lineData : flowMapData.getLineList()) {
					JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();
					if (lineData.getMapElemType() == MapElemType.ParallelLines
							|| lineData.getMapElemType() == MapElemType.VerticalLine) { // 判断是否为横竖分割线
						vHLineDataDataList.add(lineData);
					}
					// 情况错误线段
					if (clearErrorLine(lineData, deleteFigureDataList, deleteLineDataList)) {
						continue;
					}
					// -1 代表为线主键ID的默认值
					if (lineData.getUUID() == null) {
						JecnSaveProcess.getFlowStructureImageTtoLineData(lineData, flowId, flowStructureImageT);
						insertFlowElementList.add(flowStructureImageT);
						continue;
					}

				}
				if (oldLineDataList != null) {
					for (JecnBaseLineData oldLineData : oldLineDataList) {
						JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();
						// 是否需要删除标识
						boolean isDelete = true;
						if (oldLineData.getMapElemType() == MapElemType.CommonLine
								|| oldLineData.getMapElemType() == MapElemType.ManhattanLine) {
							JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) oldLineData;
							if (manhattanLineData.getLineSegmentDataList().size() == 0) {
								deleteFigureDataList.add(oldLineData.getUUID());
								deleteLineDataList.add(oldLineData.getUUID());
								continue;
							}
						}

						// 循环内存中存放的线数据集合
						for (JecnBaseLineData lineData : flowMapData.getLineList()) {
							if (lineData.getUUID() == null) {
								continue;
							}
							if (lineData.getUUID().equals(oldLineData.getUUID())) {
								boolean isUpdate = JecnSaveProcess.isUpdateFlowLineData(lineData, oldLineData, flowId,
										flowStructureImageT);
								if (isUpdate) {
									updateFlowElementList.add(flowStructureImageT);
								}
								isDelete = false;
								break;
							}
						}

						// 需要删除的数据
						if (isDelete) {
							deleteFigureDataList.add(oldLineData.getUUID());
							deleteLineDataList.add(oldLineData.getUUID());
						}
					}
				}
			}

			// 活动和角色的关联关系
			List<JecnRoleActiveT> roleActiveTList = JecnSaveProcess.setActiveIdForRoleFigure(activeFigureDataList,
					roleFigureDataList, vHLineDataDataList);

			// to from和活动的关联关系
			List<JecnToFromActiveT> toFromActiveTList = JecnSaveProcess.setActiveIdForToFromFigure(flowId,
					activeFigureDataList, toFromFigureDataList);

			ProcessData processData = new ProcessData();
			// 流程主表
			processData.setJecnFlowStructureT(flowStructureT);
			// 流程基本信息
			processData.setJecnFlowBasicInfoT(jecnFlowBasicInfoT);
			// 角色与活动关联集合
			processData.setRoleActiveTList(roleActiveTList);
			// 需要删除的图形数据集合
			processData.setDeleteListLine(deleteLineDataList);
			// 需要时删除线的集合
			processData.setDeleteList(deleteFigureDataList);
			// 需要保存的元素
			processData.setSaveList(insertFlowElementList);
			// 需要修改的元素
			processData.setUpdateList(updateFlowElementList);
			// 保存的活动
			processData.setActivitySaveList(activitySaveList);
			// 更新的活动
			processData.setActivityUpdateList(activityUpdateList);
			// 删除的活动
			processData.setActiviteDeleteList(activiteDeleteList);
			// 保存的角色
			processData.setRoleSaveList(roleSaveList);
			// 更新的角色
			processData.setRoleUpdateList(roleUpdateList);
			// 删除的角色
			processData.setRoleDeleteList(roleDeleteList);
			// to和活动的关联关系
			processData.setToFromActiveTList(toFromActiveTList);

			/** ********** 【文档相关链接数据对象】 ***************** */
			processData.setSaveFileImageDataList(saveFileImageDataList);
			processData.setUpdateFileImageDataList(updateFileImageDataList);

			// 是否有分页符
			if (flowMapData.getPageSetingBean().getPageSets() > 1) {
				splitWorkflow = new SplitWorkflowDialog();
				splitWorkflow.addWindowListener(new WindowAdapter() {// 为了打开后隐藏
							@Override
							public void windowOpened(WindowEvent e) {
								splitWorkflow.setVisible(false);
							}
						});
				splitWorkflow.setSize(new Dimension(0, 0));// 不让用户感觉到存在
				splitWorkflow.setVisible(true);

				processData.setPagingImageList(PagingImageUtil.getPagingImageByte(splitWorkflow.getPanelList()));

			}

			processData.setSave(desktopPane.getFlowMapData().isSave());
			ConnectionPool.getProcessAction().saveProcess(processData, data, JecnConstants.getUserId());
			// 重新给面板赋值
			refreshFlowMapData();
		} catch (Exception e) {
			log.error("JecnDesignerImpl is error！", e);
			return "1";
		}
		return null;
	}

	private void refreshFlowMapData() throws Exception {
		JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();

		List<JecnFigureData> oldFigureList = flowMapData.getDesignerData().getOldFigureList();
		/** 线对象集合 （设计器专用） */
		List<JecnBaseLineData> oldLineList = flowMapData.getDesignerData().getOldLineList();
		// 刷新缓存
		oldFigureList.clear();
		oldLineList.clear();

		for (JecnFigureData figureData : flowMapData.getFigureList()) {
			oldFigureList.add(getCloneData(figureData));
		}
		for (JecnBaseLineData lineData : flowMapData.getLineList()) {
			JecnBaseLineData clone = (JecnBaseLineData) lineData.clone();
			clone.setUUID(lineData.getUUID());
			oldLineList.add(clone);
		}

		JecnTreeCommon.flushCurrentTreeNodeToUpdate();

		// 更新面板显示状态（编辑权限）
		JecnPaintFigureUnit.addOpenTipPanel();

	}

	public static JecnFigureData getCloneData(JecnFigureData figureData) {
		JecnFigureData clone = figureData.clone();
		clone.setFlowElementId(figureData.getFlowElementId());
		clone.setUUID(figureData.getUUID());
		if (DrawCommon.isActive(figureData.getMapElemType())) {
			refreshActivieData((JecnActiveData) clone, (JecnActiveData) figureData);
		}
		if (DrawCommon.isRole(figureData.getMapElemType())) {
			refreshRoleData((JecnRoleData) clone, (JecnRoleData) figureData);
		}
		if (figureData.getListFigureFileTBean() != null) {
			refReshFigureFile(clone, figureData);
		}
		return clone;
	}

	private static void refReshFigureFile(JecnFigureData cloneData, JecnFigureData figureData) {
		List<JecnFigureFileTBean> figureFileTBeans = figureData.getListFigureFileTBean();
		JecnFigureFileTBean cloneFigureFile = null;
		for (JecnFigureFileTBean jecnFigureFileTBean : figureFileTBeans) {
			cloneFigureFile = jecnFigureFileTBean.clone();
			cloneFigureFile.setUUID(jecnFigureFileTBean.getUUID());
			cloneData.getListFigureFileTBean().add(cloneFigureFile);
		}
	}

	private static void refreshRoleData(JecnRoleData cloneData, JecnRoleData curData) {
		List<JecnFlowStationT> flowStationList = curData.getFlowStationList();
		for (JecnFlowStationT flowStationT : flowStationList) {
			JecnFlowStationT clone = (JecnFlowStationT) flowStationT.clone();
			clone.setUUID(flowStationT.getUUID());
			cloneData.getFlowStationList().add(clone);
		}
	}

	private static void refreshActivieData(JecnActiveData cloneData, JecnActiveData curData) {
		// 活动输出
		for (JecnModeFileT modeFileT : curData.getListModeFileT()) {
			JecnModeFileT clone = (JecnModeFileT) modeFileT.clone();
			clone.setUUID(modeFileT.getUUID());
			cloneData.getListModeFileT().add(clone);
		}
		// 活动输入
		for (JecnActivityFileT activityFileT : curData.getListJecnActivityFileT()) {
			JecnActivityFileT clone = (JecnActivityFileT) activityFileT.clone();
			clone.setUUID(activityFileT.getUUID());
			cloneData.getListJecnActivityFileT().add(clone);
		}
		// 活动指标
		for (JecnRefIndicatorsT refIndicatorsT : curData.getListRefIndicatorsT()) {
			JecnRefIndicatorsT clone = (JecnRefIndicatorsT) refIndicatorsT.clone();
			clone.setUUID(refIndicatorsT.getUUID());
			cloneData.getListRefIndicatorsT().add(clone);
		}
		// 活动线上信息
		for (JecnActiveOnLineTBean activeOnLineTBean : curData.getListJecnActiveOnLineTBean()) {
			JecnActiveOnLineTBean clone = (JecnActiveOnLineTBean) activeOnLineTBean.clone();
			clone.setId(activeOnLineTBean.getId());
			cloneData.getListJecnActiveOnLineTBean().add(clone);
		}
		// 活动相关标准
		for (JecnActiveStandardBeanT activeStandardBeanT : curData.getListJecnActiveStandardBeanT()) {
			JecnActiveStandardBeanT clone = (JecnActiveStandardBeanT) activeStandardBeanT.clone();
			clone.setId(activeStandardBeanT.getId());
			cloneData.getListJecnActiveStandardBeanT().add(clone);
		}
		// 活动相关控制点C 修改过
		for (JecnControlPointT controlPointT : curData.getJecnControlPointTList()) {
			JecnControlPointT clone = (JecnControlPointT) controlPointT.clone();
			clone.setId(clone.getId());
			cloneData.getJecnControlPointTList().add(clone);
		}

		// 新版输入输出
		if (curData.getListFigureInTs() != null) {
			List<JecnFigureInoutT> cloneListFigureInTs = new ArrayList<JecnFigureInoutT>();
			for (JecnFigureInoutT conleInoutT : curData.getListFigureInTs()) {
				cloneListFigureInTs.add(conleInoutT.clone());
			}
			cloneData.setListFigureInTs(cloneListFigureInTs);
		}
		if (curData.getListFigureOutTs() != null) {
			List<JecnFigureInoutT> cloneListFigureInTs = new ArrayList<JecnFigureInoutT>();
			for (JecnFigureInoutT conleInoutT : curData.getListFigureOutTs()) {
				cloneListFigureInTs.add(conleInoutT.clone());
			}
			cloneData.setListFigureOutTs(cloneListFigureInTs);
		}
	}

	private boolean clearErrorLine(JecnBaseLineData lineData, List<String> deleteFigureDataList,
			List<String> deleteLineDataList) {
		if (lineData instanceof JecnManhattanLineData) {
			JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) lineData;
			if (manhattanLineData.getLineSegmentDataList().size() == 0) {
				deleteFigureDataList.add(lineData.getUUID());
				deleteLineDataList.add(lineData.getUUID());
				return true;
			}
		}
		return false;
	}

	public static void reloadProcessPanel(Long flowId) throws Exception {
		JecnTreeCommon.flushCurrentTreeNodeToUpdate();
		// 清空面板
		JecnIOUtil.resetWorkflow();
		// 重新给面板赋值
		ProcessOpenData processOpenData = ConnectionPool.getProcessAction().getProcessData(flowId,
				JecnConstants.projectId);
		JecnPaintFigureUnit.addOpenTipPanel();
		JecnProcess.setProcessMapData(processOpenData, false);
	}

	/**
	 * 文件相关附件数据对象
	 * 
	 * @param figureData
	 * @param flowStructureImageT
	 * @return
	 */
	private ProcessFileImageFigureData getProcessFileImageFigureData(JecnFigureData figureData,
			JecnFlowStructureImageT flowStructureImageT) {
		ProcessFileImageFigureData fileImageFigureData = new ProcessFileImageFigureData();
		fileImageFigureData.setJecnFlowStructureImageT(flowStructureImageT);
		fileImageFigureData.setListJecnFigureFileTBean(JecnSaveProcess.switchLocalFigureFileTBean(figureData
				.getListFigureFileTBean(), figureData.getUUID()));
		return fileImageFigureData;
	}

	/**
	 * 保存流程地图
	 * 
	 * @param data
	 *            生成图片字节数组
	 * @return
	 */
	public String saveTotalMap(byte[] data) {
		boolean isRefresh = true;
		try {
			ProcessMapData mapData = getProcessMapData();

			JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
			mapData.setSave(flowMapData.isSave());
			// 保存面板数据
			ConnectionPool.getProcessAction().saveProcessMap(mapData, data, JecnConstants.getUserId());
		} catch (Exception e) {
			isRefresh = false;
			log.error("JecnDesignerImpl saveTotalMap is error！", e);
			return "1";
		} finally {
			if (isRefresh) {
				try {
					// 重新给面板赋值
					refreshFlowMapData();
				} catch (Exception e) {
					log.error("JecnDesignerImpl saveTotalMap is error", e);
					return "2";
				}
			}
		}
		return null;
	}

	public String saveOrgMap() {
		boolean isRefresh = true;
		Long orgId = null;
		// 当前面板的放大倍数
		double workflowScale = 0;
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		try {
			JecnFlowMapData flowMapData = workflow.getFlowMapData();
			// 组织图原始图数据
			List<JecnFigureData> oldFigureDataList = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
					.getDesignerData().getOldFigureList();
			// 组织图原始线数据
			List<JecnBaseLineData> oldLineDataList = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
					.getDesignerData().getOldLineList();
			// 重置层级
			JecnDrawMainPanel.getMainPanel().getWorkflow().getGraphicsLevel().saveResetLayer();
			// 组织主表
			JecnFlowOrg flowOrg = ConnectionPool.getOrganizationAction().getOrgInfo(flowMapData.getFlowId());
			orgId = flowOrg.getOrgId();
			// 需要添加的数据
			List<JecnFlowOrgImage> insertFlowElementList = new ArrayList<JecnFlowOrgImage>();
			// 需要修改的数据
			List<JecnFlowOrgImage> updateFlowElementList = new ArrayList<JecnFlowOrgImage>();
			// 需要删除的数据
			List<Long> deleteFigureDataList = new ArrayList<Long>();
			// 需要删除的线数据
			List<Long> deleteLineDataList = new ArrayList<Long>();

			// 图形的数据
			if (flowMapData.getFigureList() != null) {
				for (JecnFigureData figureData : flowMapData.getFigureList()) {
					JecnFlowOrgImage flowOrgImage = new JecnFlowOrgImage();
					// -1 代表为图形主键ID的默认值
					if (-1 == figureData.getFlowElementId()) {
						JecnSaveOrg.getFlowOrgImageData(figureData, orgId, flowOrgImage);
						insertFlowElementList.add(flowOrgImage);
						continue;
					}
				}
				if (oldFigureDataList != null) {
					// 判断是否需要修改和删除
					for (JecnFigureData oldFigureData : oldFigureDataList) {
						JecnFlowOrgImage flowOrgImage = new JecnFlowOrgImage();
						// 是否需要删除标识
						boolean isDelete = true;
						for (JecnFigureData figureData : flowMapData.getFigureList()) {
							if (-1 == figureData.getFlowElementId()) {
								continue;
							}
							if (figureData.getFlowElementId() == oldFigureData.getFlowElementId()) {
								boolean isUpdate = JecnSaveOrg.isUpdateOrgElementData(figureData, oldFigureData, orgId,
										flowOrgImage);
								if (isUpdate) {
									updateFlowElementList.add(flowOrgImage);
								}
								isDelete = false;
								break;
							}
						}
						// 需要删除的数据
						if (isDelete) {
							deleteFigureDataList.add(oldFigureData.getFlowElementId());
						}
					}
				}
			}

			// 线的数据
			if (flowMapData.getLineList() != null) {
				for (JecnBaseLineData lineData : flowMapData.getLineList()) {
					JecnFlowOrgImage flowOrgImage = new JecnFlowOrgImage();
					// TODO 情况错误线段
					// if (clearErrorLine(lineData, deleteFigureDataList,
					// deleteLineDataList)) {
					// continue;
					// }
					// -1 代表为线主键ID的默认值
					if (-1 == lineData.getFlowElementId()) {
						JecnSaveOrg.getFlowOrgImagetoLineData(lineData, orgId, flowOrgImage);
						insertFlowElementList.add(flowOrgImage);
						continue;
					}
				}
				if (oldLineDataList != null) {
					// 判断是否需要修改和删除
					for (JecnBaseLineData oldLineData : oldLineDataList) {
						JecnFlowOrgImage flowOrgImage = new JecnFlowOrgImage();
						// 是否需要删除标识
						boolean isDelete = true;

						if (oldLineData.getMapElemType() == MapElemType.CommonLine
								|| oldLineData.getMapElemType() == MapElemType.ManhattanLine) {
							JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) oldLineData;
							if (manhattanLineData.getLineSegmentDataList().size() == 0) {
								deleteFigureDataList.add(oldLineData.getFlowElementId());
								deleteLineDataList.add(oldLineData.getFlowElementId());
								continue;
							}
						}

						for (JecnBaseLineData lineData : flowMapData.getLineList()) {
							if (-1 == lineData.getFlowElementId()) {
								continue;
							}
							if (lineData.getFlowElementId() == oldLineData.getFlowElementId()) {
								boolean isUpdate = JecnSaveOrg.isUpdateFlowLineData(lineData, oldLineData, orgId,
										flowOrgImage);
								if (isUpdate) {
									updateFlowElementList.add(flowOrgImage);
								}
								isDelete = false;
								break;
							}
						}
						// 需要删除的数据
						if (isDelete) {
							deleteFigureDataList.add(oldLineData.getFlowElementId());
							deleteLineDataList.add(oldLineData.getFlowElementId());
						}
					}
				}
			}
			workflowScale = workflow.getWorkflowScale();
			// 保存面板数据
			ConnectionPool.getOrganizationAction().saveOrgMap(flowOrg, insertFlowElementList, updateFlowElementList,
					deleteLineDataList, deleteFigureDataList);
		} catch (Exception e) {
			log.error("JecnDesignerImpl saveOrgMap is error！", e);
			isRefresh = false;
			return "1";
		} finally {
			if (isRefresh) {
				try {
					// 清空面板
					JecnIOUtil.resetWorkflow();
					// 重新给面板赋值
					JecnOpenOrgBean orgOpenData = ConnectionPool.getOrganizationAction().openOrgMap(orgId);
					JecnOrganization.setOrgMapData(orgOpenData);
					// 回到原始倍数
					workflow.getWorkflowZoomProcess().zoomByNewWorkflowScale(workflowScale);
					// 刷新树节点
					List<JecnTreeBean> list = ConnectionPool.getOrganizationAction().getChildOrgsAndPositon(orgId,
							JecnConstants.projectId, false);
					JecnTreeCommon.refreshNode(JecnTreeCommon.getTreeNode(JecnTreeCommon.getNodeUniqueIdentifier(orgId,
							TreeNodeType.organization), JecnDesignerMainPanel.getDesignerMainPanel().getTree()), list,
							JecnDesignerMainPanel.getDesignerMainPanel().getTree());
				} catch (Exception e) {
					log.error("JecnDesignerImpl saveOrgMap is error!", e);
					return "2";
				}
			}
		}
		return null;
	}

	/**
	 * 保存集成关系图
	 * 
	 * @param data
	 * @return
	 */
	public String saveRelatedMap(byte[] data) {
		boolean isRefresh = true;
		try {
			ProcessMapData mapData = getProcessMapData();
			// 保存面板数据
			ConnectionPool.getProcessModeAction().saveMapRelatedProcess(mapData, data, JecnConstants.getUserId());
		} catch (Exception e) {
			isRefresh = false;
			log.error("JecnDesignerImpl saveRelatedMap is error！", e);
			return "1";
		} finally {
			if (isRefresh) {
				try {
					// // 清空面板
					// JecnIOUtil.resetWorkflow();
					// // 重新给面板赋值
					// ProcessOpenMapData processMapData =
					// ConnectionPool.getProcessModeAction().getProcessMapRelatedData(
					// flowId, JecnConstants.projectId);
					// JecnProcess.setPorcessTotalMapData(processMapData,
					// false);
					// 重新给面板赋值
					refreshFlowMapData();
				} catch (Exception e) {
					log.error("JecnDesignerImpl saveRelatedMap is error", e);
					return "2";
				}
			}
		}
		return null;
	}

	private ProcessMapData getProcessMapData() throws Exception {
		JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		// 流程图原始图数据
		List<JecnFigureData> oldFigureDataList = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
				.getDesignerData().getOldFigureList();
		// 流程图原始线数据
		List<JecnBaseLineData> oldLineDataList = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
				.getDesignerData().getOldLineList();
		// 重置层级
		JecnDrawMainPanel.getMainPanel().getWorkflow().getGraphicsLevel().saveResetLayer();

		// 流程主表
		JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(
				flowMapData.getFlowId());
		Long flowId = flowStructureT.getFlowId();
		// 需要添加的数据
		List<ProcessMapFigureData> insertFigureDataList = new ArrayList<ProcessMapFigureData>();
		// 需要修改的数据
		List<ProcessMapFigureData> updateFigureDataList = new ArrayList<ProcessMapFigureData>();
		// 需要删除的数据
		List<String> deleteFigureDataList = new ArrayList<String>();
		// 需要删除的线数据
		List<String> deleteLineDataList = new ArrayList<String>();

		ProcessMapFigureData mapFigureData = null;

		// 图形的数据
		if (flowMapData.getFigureList() != null) {
			for (JecnFigureData figureData : flowMapData.getFigureList()) {

				JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();

				// -1 代表为图形主键ID的默认值
				if (figureData.getUUID() == null) {
					mapFigureData = new ProcessMapFigureData();
					JecnSaveProcess.getFlowStructureImageTtoFigureData(figureData, flowId, flowStructureImageT);
					// 元素对象
					mapFigureData.setJecnFlowStructureImageT(flowStructureImageT);
					// 元素附件
					mapFigureData.setListJecnFigureFileTBean(JecnSaveProcess.switchLocalFigureFileTBean(figureData
							.getListFigureFileTBean(), figureData.getUUID()));
					insertFigureDataList.add(mapFigureData);
					continue;
				}
			}
			if (oldFigureDataList != null) {
				// 判断是否需要修改和删除
				for (JecnFigureData oldFigureData : oldFigureDataList) {
					JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();
					// 是否需要删除标识
					boolean isDelete = true;
					for (JecnFigureData figureData : flowMapData.getFigureList()) {
						if (figureData.getUUID() == null) {
							continue;
						}
						if (figureData.getUUID().equals(oldFigureData.getUUID())) {
							if (figureData.getUUID() == null) {// 新增元素
								continue;
							}
							boolean isUpdate = JecnSaveProcess.isUpdateFlowElementData(figureData, oldFigureData,
									flowId, flowStructureImageT);
							mapFigureData = new ProcessMapFigureData();
							// 元素附件
							mapFigureData.setListJecnFigureFileTBean(JecnSaveProcess.switchLocalFigureFileTBean(
									figureData.getListFigureFileTBean(), figureData.getUUID()));
							if (isUpdate) {
								// 元素对象
								mapFigureData.setJecnFlowStructureImageT(flowStructureImageT);
							}
							// 更新元素集合
							updateFigureDataList.add(mapFigureData);
							isDelete = false;
							break;
						}
					}
					// 需要删除的数据
					if (isDelete) {
						deleteFigureDataList.add(oldFigureData.getUUID());
					}
				}
			}
		}

		// 线的数据
		if (flowMapData.getLineList() != null) {
			for (JecnBaseLineData lineData : flowMapData.getLineList()) {
				JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();
				// 情况错误线段
				if (clearErrorLine(lineData, deleteFigureDataList, deleteLineDataList)) {
					continue;
				}
				// -1 代表为线主键ID的默认值
				if (lineData.getUUID() == null) {
					JecnSaveProcess.getFlowStructureImageTtoLineData(lineData, flowId, flowStructureImageT);
					mapFigureData = new ProcessMapFigureData();
					// 元素对象
					mapFigureData.setJecnFlowStructureImageT(flowStructureImageT);
					// 更新元素集合
					insertFigureDataList.add(mapFigureData);
					continue;
				}

			}
			if (oldLineDataList != null) {
				// 判断是否需要修改和删除
				for (JecnBaseLineData oldLineData : oldLineDataList) {
					JecnFlowStructureImageT flowStructureImageT = new JecnFlowStructureImageT();
					// 是否需要删除标识
					boolean isDelete = true;
					for (JecnBaseLineData lineData : flowMapData.getLineList()) {
						if (lineData.getUUID() == null) {
							continue;
						}
						if (lineData.getUUID().equals(oldLineData.getUUID())) {
							boolean isUpdate = JecnSaveProcess.isUpdateFlowLineData(lineData, oldLineData, flowId,
									flowStructureImageT);
							if (isUpdate) {
								mapFigureData = new ProcessMapFigureData();
								// 元素对象
								mapFigureData.setJecnFlowStructureImageT(flowStructureImageT);
								// 更新元素集合
								updateFigureDataList.add(mapFigureData);
							}
							isDelete = false;
							break;
						}
					}
					// 需要删除的数据
					if (isDelete && StringUtils.isNotBlank(oldLineData.getUUID())) {
						deleteFigureDataList.add(oldLineData.getUUID());
						deleteLineDataList.add(oldLineData.getUUID());
					}
				}
			}
		}
		ProcessMapData mapData = new ProcessMapData();
		// 保存流程地图
		mapData.setJecnFlowStructureT(flowStructureT);
		// 新增元素
		mapData.setSaveFigureDataList(insertFigureDataList);
		// 更新元素
		mapData.setUpdateFigureDataList(updateFigureDataList);
		// 删除连接线ID集合
		mapData.setDeleteList(deleteFigureDataList);
		// 删除元素ID集合
		mapData.setDeleteListLine(deleteLineDataList);
		return mapData;
	}

	@Override
	public String newPartMap() {
		// 资源管理树
		ResourceHighEfficiencyTree tree = JecnDesignerMainPanel.getDesignerMainPanel().getTree();
		if (tree == null) {// 没有树直接返回
			return null;
		}

		TreePath[] treePath = tree.getSelectionPaths();
		if (treePath != null && treePath.length == 1) {
			JecnTreeNode node = (JecnTreeNode) treePath[0].getLastPathComponent();
			if (node.getJecnTreeBean().getApproveType() == 2) {
				/*
				 * 文件处于假删或废止中
				 */
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("fileDelOrAbolish"));
				return "";
			}
			TreeNodeType treeNodeType = node.getJecnTreeBean().getTreeNodeType();
			if (treeNodeType == TreeNodeType.processRoot || treeNodeType == TreeNodeType.processMap
					|| treeNodeType == TreeNodeType.process) {
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType == 0) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("aimNodeNoPopedom"));
					return "";
				}
				// if (node.getJecnTreeBean().getPid() == 0) {
				// JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(),
				// JecnProperties
				// .getValue("level0CannotProcess"));
				// return "";
				// }
				AddFlowDialog addFlowDialog = new AddFlowDialog(node, JecnDesignerMainPanel.getDesignerMainPanel()
						.getTree());
				addFlowDialog.setVisible(true);
				return "";
			} else {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("selectUpFlow"));
			}

		} else {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("selectUpFlow"));
		}
		return null;
	}

	@Override
	public String newTotalMap() {
		// 资源管理树
		ResourceHighEfficiencyTree tree = JecnDesignerMainPanel.getDesignerMainPanel().getTree();
		if (tree == null) {// 没有树直接返回
			return null;
		}

		// 只允许管理员编辑流程架构时，设计者菜单控制
		if (!JecnDesignerCommon.isAdmin() && JecnConfigTool.isAdminEditFlowMapNode()) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("aimNodeNoPopedom"));
			return "";
		}

		TreePath[] treePath = tree.getSelectionPaths();
		if (treePath != null && treePath.length == 1) {
			JecnTreeNode node = (JecnTreeNode) treePath[0].getLastPathComponent();
			if (node.getJecnTreeBean().getApproveType() == 2) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("fileDelOrAbolish"));
				return "";
			}
			TreeNodeType treeNodeType = node.getJecnTreeBean().getTreeNodeType();
			if (treeNodeType == TreeNodeType.processRoot || treeNodeType == TreeNodeType.processRoot
					|| treeNodeType == TreeNodeType.processMap) {
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType == 0) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("aimNodeNoPopedom"));
					return "";
				}
				// 流程显示配置
				AddFlowMapDialog addFlowMapDiaolog = new AddFlowMapDialog(node, JecnDesignerMainPanel
						.getDesignerMainPanel().getTree());
				addFlowMapDiaolog.setVisible(true);
				return "";
			} else {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("selectUpFlow"));
			}

		} else {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("selectUpFlow"));
		}
		return null;
	}

	@Override
	public String addGenFlowElem() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JecnDefaultFlowElementData> readCerrFlowElementDataList() {

		List<JecnDefaultFlowElementData> jecnDefaultFlowElementDataList = new ArrayList<JecnDefaultFlowElementData>();
		try {
			List<JecnFlowFigureNow> jecnFlowFigureNowList = ConnectionPool.getProcessAction().getAllFigureAttribute();

			for (JecnFlowFigureNow jecnFlowFigureNow : jecnFlowFigureNowList) {
				JecnDefaultFlowElementData defaultFigureData = new JecnDefaultFlowElementData();
				// 主键
				defaultFigureData.setFlowElementId(jecnFlowFigureNow.getId());
				// 添加图形类型
				defaultFigureData.setMapElemType(Enum.valueOf(MapElemType.class, jecnFlowFigureNow.getFigureName()));
				// 边框颜色:bodyColor
				defaultFigureData.setBodyColor(DrawCommon.covertStringToColor(jecnFlowFigureNow.getBorderColor()));
				// 边框类型:bodyStyles
				defaultFigureData.setBodyStyle(Integer.valueOf(jecnFlowFigureNow.getBorderStyle().trim()));
				// 图形宽:figureSizeX
				defaultFigureData.setFigureSizeX(jecnFlowFigureNow.getFigureWidth().intValue());
				// 图形高:figureSizeY
				defaultFigureData.setFigureSizeY(jecnFlowFigureNow.getFigureHeight().intValue());
				// 填充类型:fillType
				defaultFigureData.setFillType(Enum.valueOf(FillType.class, jecnFlowFigureNow.getFillType()));

				// 填充颜色:fillColor
				defaultFigureData.setFillColor(DrawCommon.covertStringToColor(jecnFlowFigureNow.getBgColor()));

				// 渐变方向类型:fillColorChangType
				defaultFigureData.setChangeColor(DrawCommon.covertStringToColor(jecnFlowFigureNow.getChangeColor()));

				defaultFigureData.setFillColorChangType(Enum.valueOf(FillColorChangType.class, jecnFlowFigureNow
						.getFillColorChangType()));
				// 字体颜色:fontColor
				defaultFigureData.setFontColor(DrawCommon.covertStringToColor(jecnFlowFigureNow.getFontColor()));

				// 字体位置:fontPosition
				defaultFigureData.setFontPosition(DrawCommon.coverStringToInt(jecnFlowFigureNow.getFontLocation()));

				// 字体大小:fontSize
				defaultFigureData.setFontSize(jecnFlowFigureNow.getFontSize().intValue());

				// 阴影:shadow
				defaultFigureData.setShadow(jecnFlowFigureNow.getIsShadow());

				// 阴影颜色:shadowColor
				defaultFigureData.setShadowColor(DrawCommon.covertStringToColor(jecnFlowFigureNow.getShadowColor()));

				// 字体竖排:upRight
				defaultFigureData.setUpRight(jecnFlowFigureNow.getIsVertical());

				// 字形样式:fontStyle
				defaultFigureData.setFontStyle(jecnFlowFigureNow.getFontStyle());

				// 字体样式:fontName
				defaultFigureData.setFontName(jecnFlowFigureNow.getFontName());

				// 线条粗细:bodyWidth
				defaultFigureData.setBodyWidth(jecnFlowFigureNow.getBodyWidth());

				// 3D效果:flag3D
				defaultFigureData.setFlag3D(jecnFlowFigureNow.getIs3D() == 0 ? true : false);
				// 流程元素文字内容:figureText
				defaultFigureData.setFigureText(jecnFlowFigureNow
						.getFigureText(JecnResourceUtil.getLocale() == Locale.ENGLISH ? 1 : 0));

				// 是否显示编辑四个点:eidtPoint
				defaultFigureData.setEidtPoint(jecnFlowFigureNow.getEidtPoint() == 0 ? false : true);

				// 是否支持双击：isSortAddFigure
				defaultFigureData.setDoubleClick(jecnFlowFigureNow.getIsDoubleClick() == 0 ? false : true);

				// 是否支持快速添加图形：isSortAddFigure
				defaultFigureData.setSortAddFigure(jecnFlowFigureNow.getIsSortAddFigure() == 0 ? false : true);
				jecnDefaultFlowElementDataList.add(defaultFigureData);
			}
		} catch (Exception e) {
			log.error("JecnDesignerImpl is error", e);

		}
		return jecnDefaultFlowElementDataList;
	}

	@Override
	public List<JecnDefaultFlowElementData> readSystemDefaultFlowElementDataList() {
		List<JecnDefaultFlowElementData> jecnDefaultFlowElementDataList = new ArrayList<JecnDefaultFlowElementData>();
		try {
			List<JecnFlowFigure> jecnFlowFigureList = ConnectionPool.getProcessAction().getAllFigureAttributeDefault();
			for (JecnFlowFigure jecnFlowFigure : jecnFlowFigureList) {
				JecnDefaultFlowElementData defaultFigureData = new JecnDefaultFlowElementData();
				// 主键
				defaultFigureData.setFlowElementId(jecnFlowFigure.getId());
				// 添加图形类型
				defaultFigureData.setMapElemType(Enum.valueOf(MapElemType.class, jecnFlowFigure.getFigureName()));
				// 边框颜色:bodyColor
				defaultFigureData.setBodyColor(DrawCommon.covertStringToColor(jecnFlowFigure.getBorderColor()));
				// 边框类型:bodyStyles
				defaultFigureData.setBodyStyle(Integer.valueOf(jecnFlowFigure.getBorderStyle().trim()));
				// 图形宽:figureSizeX
				defaultFigureData.setFigureSizeX(jecnFlowFigure.getFigureWidth().intValue());
				// 图形高:figureSizeY
				defaultFigureData.setFigureSizeY(jecnFlowFigure.getFigureHeight().intValue());
				// 填充类型:fillType
				defaultFigureData.setFillType(Enum.valueOf(FillType.class, jecnFlowFigure.getFillType()));

				// 填充颜色:fillColor
				defaultFigureData.setFillColor(DrawCommon.covertStringToColor(jecnFlowFigure.getBgColor()));

				// 渐变方向类型:fillColorChangType
				defaultFigureData.setChangeColor(DrawCommon.covertStringToColor(jecnFlowFigure.getChangeColor()));

				defaultFigureData.setFillColorChangType(Enum.valueOf(FillColorChangType.class, jecnFlowFigure
						.getFillColorChangType()));
				// 字体颜色:fontColor
				defaultFigureData.setFontColor(DrawCommon.covertStringToColor(jecnFlowFigure.getFontColor()));

				// 字体位置:fontPosition
				defaultFigureData.setFontPosition(DrawCommon.coverStringToInt(jecnFlowFigure.getFontLocation()));

				// 字体大小:fontSize
				defaultFigureData.setFontSize(jecnFlowFigure.getFontSize().intValue());

				// 阴影:shadow
				defaultFigureData.setShadow(jecnFlowFigure.getIsShadow());

				// 阴影颜色:shadowColor
				defaultFigureData.setShadowColor(DrawCommon.covertStringToColor(jecnFlowFigure.getShadowColor()));

				// 字体竖排:upRight
				defaultFigureData.setUpRight(jecnFlowFigure.getIsVertical());

				// 字形样式:fontStyle
				defaultFigureData.setFontStyle(jecnFlowFigure.getFontStyle());

				// 字体样式:fontName
				defaultFigureData.setFontName(jecnFlowFigure.getFontName());

				// 线条粗细:bodyWidth
				defaultFigureData.setBodyWidth(jecnFlowFigure.getBodyWidth());

				// 3D效果:flag3D
				defaultFigureData.setFlag3D(jecnFlowFigure.getIs3D() == 0 ? true : false);
				// 流程元素文字内容:figureText
				defaultFigureData.setFigureText(jecnFlowFigure.getFigureText());

				// 是否显示编辑四个点:eidtPoint
				defaultFigureData.setEidtPoint(jecnFlowFigure.getEidtPoint() == 0 ? false : true);

				// 是否支持双击：isSortAddFigure
				defaultFigureData.setDoubleClick(jecnFlowFigure.getIsDoubleClick() == 0 ? false : true);

				// 是否支持快速添加图形：isSortAddFigure
				defaultFigureData.setSortAddFigure(jecnFlowFigure.getIsSortAddFigure() == 0 ? false : true);
				jecnDefaultFlowElementDataList.add(defaultFigureData);
			}
		} catch (Exception e) {
			log.error("JecnDesignerImpl is error", e);

		}
		return jecnDefaultFlowElementDataList;
	}

	@Override
	public boolean writeCurrFlowElementDataList(List<JecnDefaultFlowElementData> dataList) {
		List<JecnFlowFigureNow> list = new ArrayList<JecnFlowFigureNow>();
		if (dataList == null) {
			return false;
		}

		for (JecnDefaultFlowElementData defaultFlowElementData : dataList) {
			if (!defaultFlowElementData.isUpdateFlag()) {// 没有更新的
				continue;
			}

			JecnFlowFigureNow flowFigureNow = new JecnFlowFigureNow();
			// 主键
			flowFigureNow.setId(defaultFlowElementData.getFlowElementId());
			// 元素名称 元素类型
			flowFigureNow.setFigureName(defaultFlowElementData.getMapElemType().toString());
			// 元素宽度
			flowFigureNow.setFigureWidth(new Long(defaultFlowElementData.getFigureSizeX()));
			// 元素高度
			flowFigureNow.setFigureHeight(new Long(defaultFlowElementData.getFigureSizeY()));
			// 是否为阴影效果:0是;1否
			flowFigureNow.setIsShadow(defaultFlowElementData.getShadow());
			// 是否为3D效果:0是;1否
			flowFigureNow.setIs3D(defaultFlowElementData.getFlag3D() ? 0 : 1);
			// 是否为竖排文字:0是;1否
			flowFigureNow.setIsVertical(defaultFlowElementData.getUpRight());
			// 填充(背景)颜色
			flowFigureNow.setBgColor(DrawCommon.coverColorToString(defaultFlowElementData.getFillColor()));
			// 边框颜色
			flowFigureNow.setBorderColor(DrawCommon.coverColorToString(defaultFlowElementData.getBodyColor()));
			// 字体颜色
			flowFigureNow.setFontColor(DrawCommon.coverColorToString(defaultFlowElementData.getFontColor()));
			// 字体大小
			flowFigureNow.setFontSize(new Long(defaultFlowElementData.getFontSize()));
			// 字体位置
			flowFigureNow.setFontLocation(String.valueOf(defaultFlowElementData.getFontPosition()));
			// 边框样式
			flowFigureNow.setBorderStyle(String.valueOf(defaultFlowElementData.getBodyStyle()));
			flowFigureNow.setFillType(defaultFlowElementData.getFillType().toString());
			flowFigureNow.setChangeColor(DrawCommon.coverColorToString(defaultFlowElementData.getChangeColor()));
			/** 渐变方向类型——new */
			flowFigureNow.setFillColorChangType(defaultFlowElementData.getFillColorChangType().toString());
			/** 字形样式——new */
			flowFigureNow.setFontStyle(defaultFlowElementData.getFontStyle());
			/** 字体名称——new */
			flowFigureNow.setFontName(defaultFlowElementData.getFontName());
			/** 阴影颜色——new */
			flowFigureNow.setShadowColor(DrawCommon.coverColorToString(defaultFlowElementData.getShadowColor()));
			/** 线条粗细———new */
			flowFigureNow.setBodyWidth(defaultFlowElementData.getBodyWidth());
			flowFigureNow.setEidtPoint(defaultFlowElementData.getEidtPoint() ? 1 : 0);
			// 是否支持双击：isDoubleClick
			flowFigureNow.setIsDoubleClick(defaultFlowElementData.getDoubleClick() ? 1 : 0);
			// 是否支持快速添加图形：isSortAddFigure
			flowFigureNow.setIsSortAddFigure(defaultFlowElementData.getSortAddFigure() ? 1 : 0);
			// 流程元素文字内容:figureText
			flowFigureNow.setFigureText(defaultFlowElementData.getFigureText());

			list.add(flowFigureNow);
		}

		if (list.size() == 0) {
			return true;
		}

		try {
			ConnectionPool.getProcessAction().updateFigureAttribute(list);

			if (JecnConfigTool.replaceAllElementAttribute()) {// 是否启用元素属性更新提醒
				// 是否更新流程图中元素相关属性！
				int result = JecnOptionPane.showConfirmDialog(null, JecnProperties
						.getValue("yNUpdateProcessElementAttrs"), JecnProperties.getValue("pointLab"),
						JecnOptionPane.YES_NO_OPTION);
				if (result == 0) {
					ConnectionPool.getProcessAction().replaceAlllElementAttrs(list);
				}
			}
			return true;
		} catch (Exception e) {
			log.error("JecnDesignerImpl is error", e);

		}

		return false;
	}

	@Override
	public void activeDetailsDialog(JecnBaseActiveFigure activeFigure) {

	}

	@Override
	public void beforeFlowElemPopupShow(JecnBaseFlowElementPanel flowElementPanel) {
		// 图形
		if (flowElementPanel instanceof JecnBaseFlowElementPanel) {
			// 先把所以MenuItem置灰
			JecnWorkflowMenuItem.getWorkflowMenuItem().setMenuItemDisabled();
			// 激活相应的右键菜单
			JecnWorkflowMenuItem.getWorkflowMenuItem().setElemMenuItemEnabled(flowElementPanel);
			// 线段
		} else {
			// 先把所以MenuItem置灰
			JecnWorkflowMenuItem.getWorkflowMenuItem().setMenuItemDisabled();
		}

		// 如果 流程图 为只读模式下 禁用操作数据 菜单
		if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().isAuthSave() == 3) {
			JecnWorkflowMenuItem.getWorkflowMenuItem().setMenuItemDisabled();
		}
	}

	@Override
	public void beforeFlowElemMutilsPopupShow() {
		JecnWorkflowMenuItem.getWorkflowMenuItem().setMenuItemDisabled();
		JecnWorkflowMenuItem.getWorkflowMenuItem().setElemMenusItemEnabled();
	}

	@Override
	public void beforeWorkflowPopupShow() {
		// 先把所以MenuItem置灰
		JecnWorkflowMenuItem.getWorkflowMenuItem().setMenuItemDisabled();
		// 激活相应的右键菜单
		JecnWorkflowMenuItem.getWorkflowMenuItem().setWorkflowMenuItemEnabled();

	}

	/**
	 * 模型的删除
	 */
	public void deleteTemplate() {
		JecnTemplate.deleteTemplate();
	}

	/**
	 * 获取模型数据
	 */
	public void getJecnProcessTemplateBeanList() {
		JecnTemplate.readTemplateDataToWorkflow();
	}

	/**
	 * 保存模型数据
	 */
	public void saveTemplate() {
		JecnTemplate.saveTemplateData();
	}

	/**
	 * 修改模型数据
	 */
	public void updateTemplate() {
		JecnTemplate.updateTemplate();

	}

	@Override
	public void loginOut() {
		// 读取配置文件输入流
		InputStream in = null;
		FileInputStream fileInput = null;
		FileOutputStream out = null;
		Properties props = null;
		try {
			ConnectionPool.getPersonAction().loginOut(JecnConstants.getUserId());
			// 保存上传文件最后一次选择的路径
			props = new Properties();
			// 配置文件地址
			String path = JecnDesignerImpl.class.getResource("/").toURI().getPath() + "resources/JecnConfig.properties";

			fileInput = new FileInputStream(path);
			in = new BufferedInputStream(fileInput);
			props.load(in);
			out = new FileOutputStream(path);

			props.store(out, "");

			if (!JecnConstants.isPubShow()) {// D2，D3版本
				JecnProcessD2 d2 = new JecnProcessD2();
				d2.processEprosD2(false);
			}
		} catch (Exception e) {
			log.error("JecnDesignerImpl loginOut is error", e);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
				if (fileInput != null) {
					fileInput.close();
				}

			} catch (IOException e) {

				log.error("JecnDesignerImpl loginOut is error", e);
			}

		}
	}

	/**
	 * 
	 * 关闭占用
	 * 
	 * @param workflow
	 *            JecnDrawDesktopPane
	 * 
	 * @return boolean true：成功；false：失败
	 * 
	 */
	@Override
	public boolean workFlowClose(JecnDrawDesktopPane workflow) {
		if (workflow == null) {
			return false;
		}
		JecnFlowMapData flowMapData = workflow.getFlowMapData();
		if (flowMapData.isAuthSave() != 1) {
			return true;
		}
		try {
			switch (flowMapData.getMapType()) {
			case totalMap:// 流程地图
			case partMap: // 流程图
			case totalMapRelation:
				ConnectionPool.getProcessAction().moveEdit(flowMapData.getFlowId(), JecnConstants.getUserId());
				break;
			case orgMap: // 组织图
				ConnectionPool.getOrganizationAction().moveEdit(flowMapData.getFlowId());
				break;
			default:
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error("JecnDesignerImpl workFlowClose is error", e);
			return false;
		}
	}

	@Override
	public void activeDetail() {
		JecnBaseActiveFigure baseActiveFigure = (JecnBaseActiveFigure) JecnDrawMainPanel.getMainPanel()
				.getPartMapFlowElemPopupMenu().getElementPanel();
		JecnActiveData jecnActiveData = baseActiveFigure.getFlowElementData();
		ActivitiesDetailsDialog d = new ActivitiesDetailsDialog(jecnActiveData, baseActiveFigure);
		d.setVisible(true);

	}

	@Override
	public void swicthWorkFlow(long id, MapType mapType, ModeType modeType) {
		switch (mapType) {
		case totalMap:// 流程地图
		case totalMapRelation:
			try {
				if (modeType == ModeType.totalMapMode) {
					JecnTreeCommon.searchNode(id, TreeNodeType.processMapMode, JecnDesignerMainPanel
							.getDesignerMainPanel().getModelPanel().getTree());
				} else {
					JecnTreeCommon.searchNode(id, TreeNodeType.processMap, JecnDesignerMainPanel.getDesignerMainPanel()
							.getTree());
				}

			} catch (Exception e) {
				log.error("JecnDesignerImpl swicthWorkFlow is error", e);
			}
			break;
		case partMap:// 流程图
			try {
				if (modeType == ModeType.partMapMode) {
					JecnTreeCommon.searchNode(id, TreeNodeType.processMode, JecnDesignerMainPanel
							.getDesignerMainPanel().getModelPanel().getTree());
				} else {
					JecnTreeCommon.searchNode(id, TreeNodeType.process, JecnDesignerMainPanel.getDesignerMainPanel()
							.getTree());
				}

			} catch (Exception e) {
				log.error("JecnDesignerImpl swicthWorkFlow is error", e);
			}
			break;
		case orgMap:// 组织图
			try {
				JecnTreeCommon.searchNode(id, TreeNodeType.organization, JecnDesignerMainPanel.getDesignerMainPanel()
						.getTree());
			} catch (Exception e) {
				log.error("JecnDesignerImpl swicthWorkFlow is error", e);
			}
			break;

		default:
			break;
		}
	}

	public void isEditFlowDesignGuide(MapType mapType) {
		JecnDesignerMainPanel.getDesignerMainPanel().getGuidePanel().isEditFlowDesignGuide(mapType);
	}

	/**
	 * @author yxw 2012-10-22
	 * @description:定时器重新启动 （面变需要保存时、切换流程图标签时）
	 */
	@Override
	public void timerRestart() {
		WorkFlowTimer.restart();
		WorkFlowBakTimer.getInstance().restart();
	}

	@Override
	public List<DrawOperationDescriptionConfigBean> operConfigList() {
		List<DrawOperationDescriptionConfigBean> operationDescriptionConfigList = new ArrayList<DrawOperationDescriptionConfigBean>();
		List<JecnConfigItemBean> configItemBeanList = ConnectionPool.getConfigAciton().selectShowFlowFile();
		for (JecnConfigItemBean configItemBean : configItemBeanList) {
			DrawOperationDescriptionConfigBean operationDescriptionConfigBean = new DrawOperationDescriptionConfigBean();
			operationDescriptionConfigBean.setName(configItemBean.getName(JecnResourceUtil.getLocale()));
			operationDescriptionConfigBean.setMark(configItemBean.getMark());
			operationDescriptionConfigBean.setIsShow(Integer.valueOf(configItemBean.getValue()));
			operationDescriptionConfigBean.setIsEmpty(configItemBean.getIsEmpty());
			operationDescriptionConfigBean.setV1(configItemBean.getV1());
			operationDescriptionConfigBean.setV2(configItemBean.getV2());
			operationDescriptionConfigList.add(operationDescriptionConfigBean);
		}
		return operationDescriptionConfigList;
	}

	/**
	 * 
	 * 添加连接到角色移动上的图形
	 * 
	 * @param figure
	 *            JecnBaseRoleFigure
	 */
	@Override
	public void addLinkToroleMobile(JecnBaseRoleFigure figure) {
		if (figure == null) {
			return;
		}
		JecnDesignerCommon.addElemLink(figure, MapType.partMap);
	}

	/**
	 * 
	 * 获取版本类型
	 * 
	 * @return boolean true： 完整版 false：标准版
	 */
	@Override
	public boolean isVersionType() {
		return JecnConstants.versionType == 99 || JecnConstants.versionType == 100;
	}

	/**
	 * 获取图标插入框 图片
	 * 
	 * @author fuzhh 2013-7-25
	 * @param id
	 *            图片ID
	 * @return
	 */
	public void openFile(long id, IconFigure iconFigure) {
		if (iconFigure == null) {
			return;
		}
		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getFileAction().openFile(id);
		} catch (Exception e) {
			log.error("JecnDesignerImpl openFile is error！", e);
		}
		if (fileOpenBean == null) {
			return;
		}
		String path = JecnUtil.downFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
		if (path != null) {
			java.awt.Image img = Toolkit.getDefaultToolkit().getImage(path);
			iconFigure.setImg(img);
			iconFigure.repaint();
		}
	}

	/**
	 * 隐藏流程设计向导和资源管理器
	 * 
	 */
	@Override
	public void doubleCheckTabbedPanel(MouseEvent e) {
		JecnDesignerModuleShow designModuleShow = JecnDesignerFrame.getDesignModuleShow();

		JecnSplitPane allSplitPane = designModuleShow.getAllSplitPane();
		JecnSplitPane partSplitPane = designModuleShow.getPartSplitPane();
		if (designModuleShow == null || e == null || allSplitPane == null || partSplitPane == null) {
			log.error("");
			throw new IllegalArgumentException("JecnDesignerImpl designModuleShow =" + designModuleShow + " e=:" + e);
		}
		if (e.getClickCount() != 2) {
			return;
		}
		// 设置双击为true
		designModuleShow.setChecked(true);

		// 树面板显示复选框
		JCheckBox treePaneCheckBox = designModuleShow.getTreePaneCheckBox();
		// 流程导向显示复选框
		JCheckBox flowGuideCheckBox = designModuleShow.getFlowGuideCheckBox();

		if (treePaneCheckBox.isSelected() || flowGuideCheckBox.isSelected()) {
			// 只显示画图面板
			allSplitPane.showRightPanel();
			// 复选框不为勾选
			treePaneCheckBox.setSelected(false);
			flowGuideCheckBox.setSelected(false);
		} else {
			// 显示树面板
			allSplitPane.showBothPanel();
			// 显示流程设计向导面板
			partSplitPane.showBothPanel();
			// 复选框为勾选
			treePaneCheckBox.setSelected(true);
			flowGuideCheckBox.setSelected(true);
		}
		// 设置双击为false
		designModuleShow.setChecked(false);
		JecnDrawMainPanel.getMainPanel().revalidate();
		JecnDrawMainPanel.getMainPanel().repaint();
	}

	@Override
	public boolean isPubShow() {

		return JecnConstants.isPubShow();
	}

	@Override
	public JPanel getConfigFileConPanyPanel() {
		try {
			if (!JecnConstants.isPubShow()) {// D2版本
				// 获取路径
				URL url = JecnDesignerImpl.class.getResource("");
				String path = null;
				try {
					path = url.toURI().getPath();
				} catch (URISyntaxException e) {
					// 这儿一般是不进入此分支的
					path = url.getPath().replaceAll("%20", " ");
				}
				if (!DrawCommon.isNullOrEmtry(path)) {
					path = path.substring(0, path.lastIndexOf("epros/designer/service"))
							+ "server/jecn/EprosServer3.0/images/";
					log.info("保存公司logo图片路径=" + path);
					return new ConfigFileConPanyPanel(path);
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	@Override
	public void saveWorkFlowAsBak(JecnDrawDesktopPane workflow) {
		if (workflow == null || workflow.getFlowMapData() == null) {
			log.info("JecnDesignerImpl saveWorkFlowAsBak is error！");
			return;
		}
		// 重置层级
		JecnFlowMapData flowMap = workflow.getFlowMapData();
		// 流程图导出类
		JecnFlowExport flowExport = new JecnFlowExport();
		// 获取备份路径
		String bakPath = getFinalBakPath(flowMap, JecnUtil.getFlowBakPath(flowMap));
		// 保存
		flowExport.saveFlow(flowMap, bakPath, true);
	}

	/**
	 * 获取备份文件的路径
	 * 
	 * @author weidp
	 * @date 2014-10-9 上午09:53:06
	 * @param flowMap
	 * @return
	 */
	private String getFinalBakPath(JecnFlowMapData flowMap, String bakPath) {
		// 如果文件夹为空则创建
		if (!bakPath.isEmpty()) {
			// 文件夹不存在时，创建文件夹。存在时删除文件夹内的所有文件。
			File file = new File(bakPath);
			if (!file.exists() && !file.isDirectory()) {
				file.mkdirs();
			} else {
				// 文件夹下的所有子文件
				File[] files = file.listFiles();
				if (files.length != 0) {
					// 删除所有子文件
					for (File f : files) {
						f.delete();
					}
				}
			}
		}
		// 获取时间戳，返回epr文件路径
		long time = new Date().getTime();
		return bakPath + time + ".epr";
	}

	@Override
	public void recoveryFlowFile(File flowFile) {
		JecnFlowImport flowImport = new JecnFlowImport();
		flowImport.recoveryFlowFile(flowFile);
	}

	/**
	 * 
	 * 检查权限，是否让操作
	 * 
	 * @return boolean true：有权限操作；false：没有权限操作
	 */
	public boolean checkAuth() {
		if (!JecnDesigner.isAdmin) {// 设计器且不是管理员
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noPopedomOp"));
			return false;
		}
		return true;
	}

	@Override
	public List<MapElemType> getShowElementsType(int type) {
		try {
			List<JecnElementsLibrary> elementsLibraries = ConnectionPool.getElementsLibraryAction()
					.getShowElementsLibraryByType(type);
			if (elementsLibraries.size() == 0) {
				return null;
			}
			List<MapElemType> showResult = new ArrayList<MapElemType>();
			for (JecnElementsLibrary elementsLibrary : elementsLibraries) {
				showResult.add(MapElemType.valueOf(elementsLibrary.getMark()));
			}
			return showResult;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("JecnDesignerImpl ！getAllElementsType" + " type = " + type);
		}
		return null;
	}

	@Override
	public void updateElementsType(List<MapElemType> mapElemTypes, int type) {
		if (mapElemTypes == null) {
			return;
		}
		List<String> elemTypes = new ArrayList<String>();
		for (MapElemType mapElemType : mapElemTypes) {
			elemTypes.add(mapElemType.toString());
		}
		try {
			ConnectionPool.getElementsLibraryAction().updateElementsLibary(elemTypes, type);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("JecnDesignerImpl ！updateElementsType" + " type = " + type);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("updateElementLibraryException"));
			return;
		}
	}

	@Override
	public List<MapElemType> getAllElementsType(int type) {
		try {
			List<JecnElementsLibrary> showElems = ConnectionPool.getElementsLibraryAction()
					.getShowElementsLibraryByType(type);

			List<JecnElementsLibrary> addElems = ConnectionPool.getElementsLibraryAction().getAddElementsLibraryByType(
					type);

			List<MapElemType> allResult = new ArrayList<MapElemType>();
			for (JecnElementsLibrary elementsLibrary : showElems) {
				allResult.add(MapElemType.valueOf(elementsLibrary.getMark()));
			}

			for (JecnElementsLibrary elementsLibrary : addElems) {
				allResult.add(MapElemType.valueOf(elementsLibrary.getMark()));
			}
			return allResult;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("JecnDesignerImpl！getAllElementsType" + " type = " + type);
		}
		return null;
	}

	@Override
	public boolean isAdmin() {
		return JecnDesignerCommon.isAdmin();
	}

	@Override
	public boolean isNumShow() {
		boolean isNumShow = false;
		try {
			String value = ConnectionPool.getConfigAciton().selectValueFromCache(
					ConfigItemPartMapMark.processShowNumber);
			if ("1".equals(value)) {
				isNumShow = true;
			}
		} catch (Exception e) {
			log.error("JecnDesignerImpl isNumShow is error", e);
		}

		return isNumShow;
	}

	@Override
	public String getFlowIdInput(Long flowId) {
		String flowIdInput = "";
		try {
			JecnFlowStructureT jecnFlowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);
			flowIdInput = jecnFlowStructureT.getFlowIdInput();
		} catch (Exception e) {
			log.error("JecnDesignerImpl getFlowIdInput is error", e);
		}

		return flowIdInput;
	}

	@Override
	public void activeAutoNums() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (JecnDesignerProcess.getDesignerProcess().isHiddenActiveType()) {
			DesignerActivityCodeBuildDialog activityNumber = new DesignerActivityCodeBuildDialog(desktopPane);
			activityNumber.setVisible(true);
		} else {
			DesignerActivityCodeBuildDialog activityNumber = new DesignerActivityCodeBuildDialog(desktopPane);
			// true 为横向 false 为纵向
			boolean flag = activityNumber.getWorkflow().getFlowMapData().isHFlag();
			int index = activityNumber.getNumberTypeJcomboBox().getSelectedIndex();
			activityNumber.buildArabia(index, flag);
		}
	}

	@Override
	public boolean isSameNumberInDottedRect() {
		boolean sameNumber = false;
		try {
			String value = ConnectionPool.getConfigAciton().selectValueFromCache(
					ConfigItemPartMapMark.sameNumberInDottedRect);
			if ("1".equals(value)) {
				sameNumber = true;
			}
		} catch (Exception e) {
			log.error("JecnDesignerImpl isSameNumberInDottedRect  is error", e);
		}

		return sameNumber;
	}

	/**
	 * 流程架构图，连线是否可不关联元素存在
	 * 
	 * @return true :连接线可不关联元素存在
	 */
	@Override
	public boolean isShowLineHasNoFigure() {
		String value = ConnectionPool.getConfigAciton().selectValue(6,
				ConfigItemPartMapMark.showLineHasNoFigure.toString());
		return "1".equals(value) ? true : false;
	}

	/**
	 * 是否启用石化配置活动
	 * 
	 * @return
	 */
	@Override
	public boolean isUserActiveByItem() {
		String value = JecnDesignerCommon.getUsedActiveFigureValue();
		return "1".equals(value) ? true : false;
	}

	@Override
	public boolean isSecondAdmin() {
		return JecnDesignerCommon.isAdmin();
	}

	/**
	 * 是否有编辑流程地图的权限
	 * 
	 * @return
	 */
	@Override
	public boolean isEditFlowMap() {
		return JecnConfigTool.isAdminEditFlowMapNode();
	}

	@Override
	public void addElemLink(JecnBaseFigurePanel elementPanel, MapType mapType) {
		JecnDesignerCommon.addElemLink(elementPanel, mapType);
	}

	/**
	 * 科大讯飞- 隐藏修改元素属性操作
	 * 
	 * @return
	 */
	@Override
	public boolean isHiddenEditToolBarAttrs() {
		return JecnConfigTool.isIflytekLogin();
	}

	/**
	 * 隐藏客户元素
	 * 
	 * @return
	 */
	@Override
	public boolean isShowProcessCustomer() {
		return JecnConfigTool.isShowProcessCustomer();
	}

	/**
	 * 隐藏开始元素
	 * 
	 * @return
	 */
	@Override
	public boolean isShowBeginningEndElement() {
		return JecnConfigTool.isShowBeginningEndElement();
	}

	@Override
	public boolean isActivityNumTypeShow() {
		// TODO Auto-generated method stub
		return JecnConfigTool.isActivityNumTypeShow();
	}

	@Override
	public boolean useNewInout() {
		return JecnConfigTool.useNewInout();
	}

	@Override
	public void editButAction(ActionEvent e) {
		JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		// 1、获取数据库流程节点站位
		Long flowId = flowMapData.getFlowId();
		try {
			JecnDesignerCommon.setSaveAuthValue(flowMapData,
					flowMapData.getMapType() == MapType.partMap ? TreeNodeType.process : TreeNodeType.processMap,
					flowId);
		} catch (Exception e1) {
			e1.printStackTrace();
			log.error("JecnDesignerImpl editButAction is error!", e1);
		}
	}

	@Override
	public boolean isShowWorkflowEdit() {
		JecnTreeNode selectNode = JecnTreeCommon.getWorkNode();
		if (selectNode == null) {
			return false;
		}
		if (!JecnConfigTool.isCetc10Login()) { // 编辑模式 只有十所显示
			return false;
		}
		if (selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.organization) {
			// true:发布
			boolean isPub = selectNode.getJecnTreeBean().isPub();
			// true:架构
			boolean isFlow = selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap;
			if (isFlow) {
				if (isPub && JecnConfigTool.editAuthProcessMapRelease()) {
					return true;
				} else if (!isPub && JecnConfigTool.editAuthProcessMapNotRelease()) {
					return true;
				}
			} else {
				if (isPub && JecnConfigTool.editAuthProcessRelease()) {
					return true;
				} else if (!isPub && JecnConfigTool.editAuthProcessNotRelease()) {
					return true;
				}
			}
		}
		return false;
	}

	// JecnPanel panel = JecnDrawMainPanel.getMainPanel().getEleFigurePanel();
	// panel.removeAll();
	// Component elementPanel =
	// JecnDesignerProcess.getDesignerProcess().initEleFigurePanel();
	// if (elementPanel != null) {
	// panel.add(elementPanel);
	// panel.updateUI();
	// }

	@Override
	public Component initEleFigurePanel() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return null;
		}
		List<JecnBaseFlowElementPanel> selectPanles = JecnDrawMainPanel.getMainPanel().getWorkflow()
				.getCurrentSelectElement();
		if (selectPanles.size() != 1) {
			return null;
		}
		JecnBaseFlowElementPanel curFigurePanel = selectPanles.get(0);
		MapElemType elemType = curFigurePanel.getFlowElementData().getMapElemType();
		if (!DrawCommon.isActive(elemType) && !DrawCommon.isRole(elemType)) {
			return null;
		}
		return JecnElementAttrsbuteCommon.getElementAttributePanel((JecnBaseFigurePanel) curFigurePanel);
	}

	@Override
	public void showElementAttributePanel() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		if (!JecnDesignerCommon.isShowEleAttribute() || desktopPane.getFlowMapData().getMapType() != MapType.partMap) {
			return;
		}
		List<JecnBaseFlowElementPanel> selectPanles = JecnDrawMainPanel.getMainPanel().getWorkflow()
				.getCurrentSelectElement();
		if (selectPanles.size() != 1) {
			return;
		}
		JecnBaseFlowElementPanel elementPanel = selectPanles.get(0);
		MapElemType elemType = elementPanel.getFlowElementData().getMapElemType();
		if (!DrawCommon.isActive(elemType) && !DrawCommon.isRole(elemType)) {
			JecnElementAttrsbuteCommon.cleanElementAttrsbute();
			return;
		}
		JecnBaseFigurePanel curFigurePanel = (JecnBaseFigurePanel) elementPanel;

		JComponent component = null;
		if (JecnElementAttrsbuteCommon.isSameTypeElement((JecnBaseFigurePanel) curFigurePanel)) {
			JecnElementAttrsbuteCommon.updateElementArrtibute(curFigurePanel);
		} else {
			component = JecnElementAttrsbuteCommon.createElementAttributePanel(curFigurePanel);
		}
		JecnPanel panel = JecnDrawMainPanel.getMainPanel().getEleFigurePanel();
		if (component != null) {// 替换
			panel.removeAll();
			panel.add(component);
		}
		panel.updateUI();
		JecnDesignerFrame.getDesignModuleShow().showEleFigure();
	}

	@Override
	public void updateElementPanel(JecnBaseFigurePanel curFigurePanel) {
		JecnElementAttrsbuteCommon.updateElementArrtibute(curFigurePanel);
	}

	@Override
	public void hiddelElementPanel() {
		JecnElementAttrsbuteCommon.cleanElementAttrsbute();
	}

	@Override
	public int getModelUpperLimit(int type) {
		// type 0 架构 1 流程
		if (type == 0) {
			return JecnConfigTool.getProcessMapModelUpperLimit();
		} else {
			return JecnConfigTool.getProcessModelUpperLimit();
		}

	}

	@Override
	public boolean isShowGrid() {
		String configProperties = ConfigCommon.INSTANTCE.getConfigProperties(ConfigCommonEnum.showGrid);
		if (StringUtils.isNotBlank(configProperties)) {
			return Boolean.valueOf(configProperties);
		} else {
			// 判断不存在此 数据库配置 写入配置文件 只有第一次初始化时候配置 并且返回 初始化值
			if (ConfigCommon.INSTANTCE.writeCommonConfigProperties(String.valueOf(JecnConfigTool.isShowGrid()),
					ConfigCommonEnum.showGrid)) {
				return JecnConfigTool.isShowGrid();
			}
			return false;
		}
	}

	@Override
	public void setGridState(boolean showGrid) {
		ConfigCommon.INSTANTCE.writeCommonConfigProperties(String.valueOf(showGrid), ConfigCommonEnum.showGrid);
	}

}
