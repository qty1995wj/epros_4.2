package com.jecn.epros.server.dao.process.impl;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.process.IFlowToolDao;

public class FlowToolDaoImpl extends AbsBaseDao<JecnFlowSustainTool, Long> implements IFlowToolDao {

	@Override
	public List<Object[]> getFlowTools(Long flowId, String pub) throws Exception {
		String sql =  "SELECT DISTINCT T.KPI_AND_ID, ST.FLOW_SUSTAIN_TOOL_DESCRIBE" +
			"  FROM JECN_FLOW_KPI_NAME"+ pub +" T" + 
			" INNER JOIN JECN_FLOW_STRUCTURE"+ pub +" FST" + 
			"    ON T.FLOW_ID = FST.FLOW_ID" + 
			" INNER JOIN JECN_SUSTAIN_TOOL_CONN"+ pub + " TOOL" + 
			"    ON T.KPI_AND_ID = TOOL.RELATED_ID" + 
			"   AND TOOL.RELATED_TYPE = 0" + 
			" INNER JOIN JECN_FLOW_SUSTAIN_TOOL ST" + 
			"    ON TOOL.SUSTAIN_TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID" +
			" WHERE FST.FLOW_ID = ?";
		return this.listNativeSql(sql, flowId);
	}
}
