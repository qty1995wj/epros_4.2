package com.jecn.epros.domain.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.service.TreeNodeUtils;

import java.util.Date;
import java.util.List;

/**
 * 我负责的流程
 * Created by zhr on 2017/11/3.
 */
public class MyResponsibilityProcess {
    @JsonIgnore
    private String trueName;
    @JsonIgnore
    private String flowName;
    @JsonIgnore
    private Long flowId;
    @JsonIgnore
    /** 0:流程/架构；2：流程（文件）*/
    private int nodeType;
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date pubTime;


    /**
     * 所属流程架构
     */
    private Long architectureId;
    private String architectureName;

    /**
     * 相关文件
     */
    List<Resource> relatedFiles;



    public LinkResource getLink() {
        return new LinkResource(flowId, flowName,LinkResource.ResourceType.PROCESS ,TreeNodeUtils.NodeType.main);
    }

    public int getNodeType() {
        return nodeType;
    }

    public LinkResource getArchitectureLink() {
        return new LinkResource(architectureId, architectureName, LinkResource.ResourceType.PROCESS_MAP,TreeNodeUtils.NodeType.main);
    }

    public void setNodeType(int nodeType) {
        this.nodeType = nodeType;
    }

    public List<Resource> getRelatedFiles() {
        return relatedFiles;
    }

    public void setRelatedFiles(List<Resource> relatedFiles) {
        this.relatedFiles = relatedFiles;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }
}
