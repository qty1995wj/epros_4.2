package com.jecn.epros.server.service.autoCode;

import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;

public interface IStandardizedAutoCodeService {
	public AutoCodeResultData getStandardizedAutoCode(AutoCodeNodeData codeNodeData) throws Exception;

	int updateStandardizedAutoCode(Long id, int relatedType, int codeTotal) throws Exception;

	Integer getAutoCodeTotal(Long relatedId, int relatedType) throws Exception;

	public boolean reNumItemItemAction(AutoCodeNodeData nodeData, long userId);
}
