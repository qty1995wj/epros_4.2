package epros.designer.gui.system;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class ResourceTreeListener extends JecnTreeListener {
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(ResourceTreeListener.class);

	public ResourceTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		// //删除该节点下的数据，重新赋值
		// node.removeAllChildren();
		if (node.getChildCount() == 0) {
			List<JecnTreeBean> list = null;
			try {
				JecnTreeBean treeBean = node.getJecnTreeBean();
				switch (treeBean.getTreeNodeType()) {
				case processRoot:
				case processMap:
				case process:
					// 加载流程
					list = JecnDesignerCommon.getDesignerProcessTreeBeanList(treeBean.getId(), JecnDesignerCommon
							.isOnlyViewDesignAuthor());
					break;
				case organizationRoot:
					list = ConnectionPool.getOrganizationAction().getChildOrgsAndPositon(treeBean.getId(),
							JecnConstants.projectId, true);
					// 加载岗位组根节点
					list.add(0, JecnTreeCommon.createTreeRoot(TreeNodeType.positionGroupRoot,
							JecnProperties.getValue("positionGroup")).getJecnTreeBean());
					break;
				case organization:
					// 加载组织与岗位
					list = ConnectionPool.getOrganizationAction().getChildOrgsAndPositon(treeBean.getId(),
							JecnConstants.projectId, true);
					break;
				case position:
					list = ConnectionPool.getPersonAction().getPersonByPosition(node.getJecnTreeBean().getId());
					break;
				case positionGroupRoot:
				case positionGroupDir:
					// 加载岗位组和岗位组目录
					list = ConnectionPool.getPosGroup()
							.getChildPositionGroup(treeBean.getId(), JecnConstants.projectId);
					break;
				case positionGroup:
					// 加载岗位组下的岗位
					list = ConnectionPool.getPosGroup().getPosByPositionGroupId(treeBean.getId());
					break;
				case standardRoot:
				case standardDir: // 标准目录
				case standard: // 标准
				case standardClause:
					list = ConnectionPool.getStandardAction().getChildStandards(treeBean.getId(),
							JecnConstants.projectId);
					break;
				case ruleRoot:
				case ruleDir: // 制度目录
				case ruleFile: // 制度文件
				case ruleModeFile:// 制度模板文件
					list = ConnectionPool.getRuleAction().getChildRules(treeBean.getId(), JecnConstants.projectId);
					break;
				case riskRoot:
				case riskDir: // 风险目录
				case riskPoint: // 风险点
					list = ConnectionPool.getJecnRiskAction().getChildsRisk(treeBean.getId(), JecnConstants.projectId);
					break;
				case termDefineRoot: 
				case termDefineDir: 
					list = ConnectionPool.getTermDefinitionAction().getChilds(treeBean.getId());
					break;
				}
				if (treeBean.getTreeNodeType() == TreeNodeType.processRoot) {

				}
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("ResourceTreeListener treeExpanded is error", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

}
