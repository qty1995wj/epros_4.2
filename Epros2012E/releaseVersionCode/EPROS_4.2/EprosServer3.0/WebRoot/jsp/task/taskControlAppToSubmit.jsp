<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}css/global.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}css/search.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript"
			src="${basePath}js/epros_${language}_${country}.js">
</script>
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js">
</script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js">
</script>
		<script type="text/javascript" src="${basePath}ext/ext-lang-zh_CN.js"
			charset="UTF-8">
</script>
		<script type="text/javascript" src="${basePath}js/common.js">
</script>
		<script type="text/javascript"
			src="${basePath}js/My97DatePicker/WdatePicker.js">
</script>
		<script type="text/javascript" src="${basePath}js/search/search.js">
</script>
		<script type="text/javascript" src="${basePath}jquery/jquery-1.6.2.js">
</script>
		<script type="text/javascript" src="${basePath}js/task/taskCommon.js">
</script>
		<script type="text/javascript" src="${basePath}js/firstPage.js"
			charset="UTF-8">
</script>
		<script type="text/javascript">
var basePath = "${basePath}";
var emailType = ${emailType};
              window.onresize=function(){
			    setShowTaskPageWH('opinion');
			 }
        </script>
		<!-- 文控审核人主导审批 -->
		<title><s:text name="appControlSubmit"></s:text></title>
		<script type="text/javascript">
var basePath = "${basePath}";

/**
 *人员选择
 */
function selectPeople(auditName, auditPeopleId, state) {
	var stateValue = document.getElementById(state).value;
	if (taskUserMultipleSelect(stateValue)) {//评审人\自定义阶段多选
		orgPosPerMultipleSelect("person", auditName, auditPeopleId);
	} else {
		selectOrgPosWindow("person", auditName, auditPeopleId);
	}
}
/**
 *点击取消
 */

function cleanText() {
	//转批的人员选择框质空
	document.getElementById("subPeopleName").value = '';
	//转批的人员选择框质空
	document.getElementById("subPeopleId").value = '';

	//转批的人员选择框质空
	document.getElementById("assignedPeopleName").value = '';
	//转批的人员选择框质空
	document.getElementById("assignedPeopleId").value = '';
}
</script>
	</head>
	<body onload="setShowTaskPageWH('opinion');">
		<div style="height: 25px; width: 100%; overflow: hidden;"
			align="right" class="gradient minWidth">
			<%@include file="/navigation.jsp"%>
		</div>
		<div id="mainDiv" class="div" style="width: 1024px; margin: auto;">
			<s:form name="form1" action="" method="post" theme="simple">
				<div id="information" class="divBorder">
					<table width="100%">
						<tr>
							<td width="10%" height="25px" align="center">
								<!-- 任务名称： -->
								<s:text name="taskNameC"></s:text>
							</td>
							<td width="37%">
								${simpleTaskBean.jecnTaskBeanNew.taskName }
							</td>
							<td width="50" rowspan="3" align="center" class="taskSimpleBlock">
								<!-- 待提交的审批意见： -->
								<s:text name="opinionC"></s:text>
							</td>
							<td rowspan="3" align="center">
								<textarea id="opinion" name="submitMessage.opinion"
									style="width: 90%; height: 80px;"></textarea>
								<span class="mustInput">*</span>
							</td>
							
						</tr>
						<tr>
							<td align="center">
								<!-- 任务状态: -->
								<s:text name="taskStateC" />
							</td>
							<td>
								${simpleTaskBean.stageName }
							</td>
						</tr>
						<tr>
							<td height="25px" align="center">
								<!-- 变更说明: -->
								<s:text name="changeThatC" />
							</td>
							<td>
								${simpleTaskBean.taskDescStr}
							</td>
						</tr>
						<!-- 文控主导，各阶段审批人选择 -->
						<s:iterator value="simpleTaskBean.listAuditPeopleBean"
							id="listAudit" status="st1">
							<s:if test="#listAudit.isShow==1">
								<s:if
									test="#listAudit.isSelectedUser==1||#listAudit.isApproval==0">
									<tr>
										<td height="25px" align="center"
											id="auditLable<s:property value='#st1.count'/>">
											<!--<s:if test="#listAudit.state!=0"></s:if>-->
											${listAudit.auditLable }
											<s:text name="chinaColon"></s:text>

										</td>
										<td>
											<s:if test="#listAudit.state==3||#listAudit.state==11||#listAudit.state==12||#listAudit.state==13||#listAudit.state==14">
												<!-- 评审人会审阶段,自定义阶段 人员选择框为文本域 -->
											<textarea id="areaAuditName<s:property value='#st1.count'/>" 
												style="width: 72%;height:80px;font-Size:13px;"
												readonly="readonly"
											    name="simpleTaskBean.listAuditPeopleBean[<s:property value='#st1.index'/>].auditName"
										    >${listAudit.auditName}</textarea>
												<s:if test='#listAudit.isApproval==0'>
													<img style="cursor: pointer; vertical-align: top;"
														onclick='selectPeople("areaAuditName<s:property value='#st1.count'/>","auditPeopleId<s:property value='#st1.count'/>","state<s:property value='#st1.count'/>")'
														src="${basePath}images/common/select.gif" hspace="5"
														border="0" />
												</s:if>
												<s:if test="#listAudit.isEmpty==1">
													<font color="red" style="vertical-align: top;"><s:text name="required"></s:text>
													</font>
												</s:if>
											</s:if>
											<s:else>
												<input id="auditName<s:property value='#st1.count'/>"
													name="simpleTaskBean.listAuditPeopleBean[<s:property value="#st1.index"/>].auditName"
													value="${listAudit.auditName}"
													style="width: 72%; vertical-align: middle;" readonly="readonly" />
												<!--<s:if test="#listAudit.state==0">type="hidden"</s:if>/>-->
												<!-- 选择 -->
												<s:if test='#listAudit.isApproval==0'>
													<img style="cursor: pointer; vertical-align: middle;"
														onclick='selectPeople("auditName<s:property value='#st1.count'/>","auditPeopleId<s:property value='#st1.count'/>","state<s:property value='#st1.count'/>")'
														src="${basePath}images/common/select.gif" hspace="5"
														border="0" />
													<div id="search_suggest<s:property value='#st1.count'/>"
														style="display: none; vertical-align: middle; z-index: 1; position: absolute;"></div>
												</s:if>
												<s:if test="#listAudit.isEmpty==1">
														<font color="red"><s:text name="required"></s:text>
														</font>
												</s:if>
											</s:else>

											<input id="auditPeopleId<s:property value='#st1.count'/>"
												name="simpleTaskBean.listAuditPeopleBean[<s:property value="#st1.index"/>].auditId"
												value="${listAudit.auditId}" style="display: none" />
											<input id="state<s:property value='#st1.count'/>"
												name="simpleTaskBean.listAuditPeopleBean[<s:property value="#st1.index"/>].state"
												value="${listAudit.state}" style="display: none" />
											<input id="stageId<s:property value='#st1.count'/>"
												name="simpleTaskBean.listAuditPeopleBean[<s:property value="#st1.index"/>].stageId"
												value="${listAudit.stageId}" style="display: none" />
											<input id="isEmpty<s:property value='#st1.count'/>"
												name="simpleTaskBean.listAuditPeopleBean[<s:property value="#st1.index"/>].isEmpty"
												value="${listAudit.isEmpty}" style="display: none" />
										</td>
										<td>
										</td>
									</tr>
								</s:if>
							</s:if>
						</s:iterator>
						<tr>
							<td height="25px" align="left" colspan="2" class="highlight">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<s:if test="simpleTaskBean.jecnTaskBeanNew.taskType==0">
									<!-- 查看流程详细信息 -->
									<a class="table"
										href="process.action?reqType=public&isPub=false&type=processT&flowId=<s:property value='simpleTaskBean.jecnTaskBeanNew.rid' />"
										style="cursor: pointer;" target='_blank'><img
											src="${basePath}images/myTask/arrow.gif" /> <s:text
											name="checkProcessDetailedInformation" /> </a>
								</s:if>
								<s:elseif test="simpleTaskBean.jecnTaskBeanNew.taskType==4">
									<!-- 查看地图详细信息 -->
									<a class="table"
										href="process.action?reqType=public&isPub=false&type=processMapT&flowId=<s:property value='simpleTaskBean.jecnTaskBeanNew.rid' />"
										style="cursor: pointer;" target='_blank'><img
											src="${basePath}images/myTask/arrow.gif" /> <s:text
											name="checkMapProcessDetailedInformation" /> </a>
								</s:elseif>
								<s:elseif test="simpleTaskBean.jecnTaskBeanNew.taskType==2">
									<!-- 制度模板文件 -->
									<a class="table"
										href="rule.action?reqType=public&isPub=false&type=ruleModeFileT&ruleId=<s:property value='simpleTaskBean.jecnTaskBeanNew.rid' />"
										style="cursor: pointer;" target='_blank'><img
											src="${basePath}images/myTask/arrow.gif" /> <s:text
											name="checkRuleDetailedInformation" /> </a>
								</s:elseif>
								<s:elseif test="simpleTaskBean.jecnTaskBeanNew.taskType==3">
									<!-- 制度文件 -->
									<a class="table"
										href="rule.action?reqType=public&isPub=false&type=ruleFileT&ruleId=<s:property value='simpleTaskBean.jecnTaskBeanNew.rid' />"
										style="cursor: pointer;" target='_blank'><img
											src="${basePath}images/myTask/arrow.gif" /> <s:text
											name="checkRuleDetailedInformation" /> </a>
								</s:elseif>
								<s:elseif test="simpleTaskBean.jecnTaskBeanNew.taskType==1">
									<!-- 文件 -->
									<a class="table"
										href="getFileInformation.action?reqType=public&isPub=false&fileId=<s:property value='simpleTaskBean.jecnTaskBeanNew.rid' />"
										style="cursor: pointer;" target='_blank'><img
											src="${basePath}images/myTask/arrow.gif" /> <s:text
											name="checkFileDetailedInformation" /> </a>
								</s:elseif>
							</td>
							<td>
							</td>
							<!-- 正常审批显示界面 -->
							<td colspan="3" align="right">
								<div
									style="padding-bottom:10;display: <s:if test="simpleTaskBean.jecnTaskBeanNew.state==3||simpleTaskBean.jecnTaskBeanNew.taskElseState==2">none</s:if>;"
									align="right" id="submitDisplay">
									<s:if test="simpleTaskBean.callBack==1">
										<!-- 打回整理9 -->
										<img style="cursor: pointer;"
											onclick="taskSubmit(9,'loadingImage9','submitLoading1')"
											src="${basePath}images/myTask/reBackColl.gif"
											id="loadingImage9" />
									</s:if>
									<!-- 返回 -->
									<s:if test="simpleTaskBean.taskOperationBean.reBack==1">
										<img style="cursor: pointer;"
											onclick="taskSubmit(14,'loadingImage14','submitLoading1')"
											src="${basePath}images/myTask/reCallT.gif"
											id="loadingImage14" />
									</s:if>
									<!-- 打回4 -->
									<img style="cursor: pointer;"
										onclick="taskSubmit(4,'loadingImage4','submitLoading1')"
										src="${basePath}images/myTask/backTo.gif" id="loadingImage4" />
									<!-- 转批3 -->
									<s:if test="simpleTaskBean.showOperationTransfer==1">
										<span style="cursor: pointer;"><img
												src="${basePath}images/myTask/turnBatch.gif"
												onclick="subconcessions(true)" id="loadingImage13" /> </span>
									</s:if>
									<!-- 交办2 -->
									<s:if test="simpleTaskBean.showOperationAssigned==1">
										<span style="cursor: pointer;"><img
												src="${basePath}images/myTask/assignedBy.gif"
												onclick="assigned(true)" id="loadingImage12" /> </span>
									</s:if>
									<!-- 通过1 -->
									<img onclick="taskSubmit(1,'loadingImage1','submitLoading1')"
										src="${basePath}images/myTask/throughThe.gif"
										style="cursor: pointer;" id="loadingImage1" />
									<!-- 取消 -->
									<span style="cursor: pointer;"><img
											onclick="windowClose();"
											src="${basePath}images/myTask/cancel.gif" id="loadingImage0" />
									</span>
									<!-- 提交中。。。 隐藏属性 -->
									<span style="display: none;" id="submitLoading1"><s:text
											name="sunmit_Loading" /> </span>
								</div>
								<!-- 转批提交按钮 -->
								<div style="display: none;" align="right"
									id="subconcessionsDisplay">
									<table width="95%">
										<tr>
											<td height="25px">
												<!-- 人员选择： -->
												<s:text name="peopleSelect" />
												<s:text name="chinaColon" />
											</td>
											<td align="left">
												<input type="text" id="subPeopleName"
													style="vertical-align: middle; width: 150px;"
													readonly="readonly"
													  />
												<br />
												<div id="subconcess_suggest"
													style="display: none; z-index: 1; position: absolute;"
													align="left"></div>
											</td>
											<td align="left">
												<!-- 转批 3-->
												<span style="cursor: pointer;"
													onclick='selectOrgPosWindow("person","subPeopleName","subPeopleId")'><img
														style="vertical-align: middle;"
														src="${basePath}images/common/select.gif" /> </span>
											</td>
											<td align="left">
												<img
													onclick="taskSubmit(3,'loadingImage3','submitLoading3')"
													src="${basePath}images/myTask/turnBatch.gif"
													style="vertical-align: middle; cursor: pointer;"
													id="loadingImage3" />
												<span style="display: none;" id="submitLoading3"><s:text
														name="sunmit_Loading" /> </span>
											</td>
											<td align="left">
												<span style="cursor: pointer;"><img
														src="${basePath}images/myTask/cancel.gif"
														onclick="subconcessions(false)"
														style="vertical-align: middle;" id="loadingImage11" /> </span>
											</td>
										</tr>
									</table>
									<input type="text" id="subPeopleId"
										style="vertical-align: middle; display: none;" />
								</div>

								<!-- 交办提交按钮 -->
								<div style="padding-bottom: 10px; display: none;" align="right"
									id="assignedDisplay">
									<table width="95%">
										<tr>
											<td height="25px">
												<!-- 人员选择： -->
												<s:text name="peopleSelect" />
												<s:text name="chinaColon" />
											</td>
											<td align="left">
												<input type="text" id="assignedPeopleName"
													style="vertical-align: middle; width: 150px;"
													readonly="readonly"
													/>
												<br />
												<div id="ass_search_suggest"
													style="display: none; z-index: 1; position: absolute;"
													align="left"></div>
											</td>
											<td>
												<span style="cursor: pointer;"
													onclick='selectOrgPosWindow("person","assignedPeopleName","assignedPeopleId")'><img
														style="vertical-align: middle;"
														src="${basePath}images/common/select.gif" /> </span>
											</td>
											<td>
												<!-- 交办 2-->
												<img
													onclick="taskSubmit(2,'loadingImage2','submitLoading2')"
													src="${basePath}images/myTask/assignedBy.gif"
													style="vertical-align: middle; cursor: pointer;"
													id="loadingImage2" />
												<span style="display: none;" id="submitLoading2"><s:text
														name="sunmit_Loading" /> </span>
											</td>
											<td>
												<span style="cursor: pointer;"><img
														src="${basePath}images/myTask/cancel.gif"
														onclick="assigned(false)" style="vertical-align: middle;"
														id="loadingImage15" /> </span>
											</td>
										</tr>
									</table>
									<input type="text" id="assignedPeopleId"
										style="vertical-align: middle; display: none;" />
								</div>

								<!-- 评审人提交按钮 -->
								<div
									style="padding-bottom: 10px;display: <s:if test="simpleTaskBean.jecnTaskBeanNew.state!=3">none</s:if>;"
									align="right" id="reviewDisplay">
									<!-- 提交5 -->
									<span style="cursor: pointer;"><img
											onclick="taskSubmit(5,'loadingImage5','submitLoading5')"
											src="${basePath}images/myTask/submitOption.gif"
											id="loadingImage5" /> </span>
									<span style="display: none;" id="submitLoading5"><s:text
											name="sunmit_Loading" /> </span>
									<!-- 取消 -->
									<span style="cursor: pointer;"><img
											onclick="windowClose();"
											src="${basePath}images/myTask/cancel.gif" id="loadingImage6" />
									</span>
								</div>

								<!-- 交办人提交按钮 -->
								<div
									style="padding-bottom: 10px;display: <s:if test="simpleTaskBean.jecnTaskBeanNew.taskElseState!=2">none</s:if>;"
									align="right" id="reviewDisplay">
									<!-- 提交5 -->
									<span style="cursor: pointer;"><img
											onclick="taskSubmit(10,'loadingImage10','submitLoading10')"
											src="${basePath}images/myTask/submit.gif" id="loadingImage10" />
									</span>
									<span style="display: none;" id="submitLoading10"><s:text
											name="sunmit_Loading" /> </span>
									<!-- 取消 -->
									<span style="cursor: pointer;"><img
											onclick="windowClose();"
											src="${basePath}images/myTask/cancel.gif" id="loadingImage11" />
									</span>
								</div>
							</td>
						</tr>
					</table>
				</div>

				<div align="center">
					<table align="center">
						<tr>
							<s:iterator value="simpleTaskBean.listTaskViewAudit"
								id="auditList" status="st1">
								<s:if test="#auditList.isSelectedUser==1">
									<!-- 当前审核阶段 -->
									<s:if
										test="#auditList.isApproval==2&&#auditList.auditName!=null">
										<td>
											<!-- 圆点 -->
											<img src="${basePath}images/myTask/taskComplete.gif" />
										</td>
										<!-- 审批中半实线半虚线 -->
										<td>
											<img src="${basePath}images/myTask/tasking.gif" width="75px;" />
										</td>
									</s:if>
									<s:elseif test="#auditList.isApproval==1">
										<td>
											<!-- 圆点 -->
											<img src="${basePath}images/myTask/taskComplete.gif" />
										</td>
										<td>
											<!-- 实线 -->
											<img src="${basePath}images/myTask/taskForExamination.gif"
												width="75px;" />
										</td>
									</s:elseif>
									<s:elseif
										test="#auditList.isApproval==0&&#auditList.auditName!=null">
										<!-- 虚点 -->
										<td>
											<img src="${basePath}images/myTask/notTask.gif" />
										</td>
										<!-- 虚线 -->
										<td>
											<img src="${basePath}images/myTask/notTaskint.gif"
												width="75px;" />
										</td>
									</s:elseif>
								</s:if>
							</s:iterator>

							<s:if
								test="simpleTaskBean.jecnTaskBeanNew.state==5&&#auditList.auditName!=null">
								<td>
									<img src="${basePath}images/myTask/taskComplete.gif" />
								</td>
							</s:if>
							<s:else>
								<td>
									<img src="${basePath}images/myTask/notTask.gif" />
								</td>
							</s:else>
						</tr>

						<tr>
							<s:iterator value="simpleTaskBean.listTaskViewAudit"
								id="auditList" status="st1">
								<s:if test="#auditList.isSelectedUser==1">
									<s:if
										test="#auditList.isApproval==1||#auditList.auditName!=null">
										<td></td>
										<td align="center">
											${auditList.auditLable }
										</td>
									</s:if>
								</s:if>
							</s:iterator>
							<td>
								<s:text name="publish"></s:text>
							</td>
						</tr>
					</table>
				</div>


				<div class="tabDiv">
					<div class="tabTitle" id="secTable">
						<ul>
							<li id="liBasicInformation" onclick="secBoard(0)"
								class="basicInfo1"></li>
							<li id="liExaminationAndApprovalRecords" class="taskRecord2"
								onclick="secBoard(1)"></li>
							<li class="liBorder"></li>
						</ul>
					</div>

					<!-- 任务基本信息 -->
					<div id="basicInformation">
						<div style="width: 100%"></div>
						<!-- IE6,7显示错位，加一个标签 -->
						<table>
							<tr height="5px">
							</tr>
							<tr>
								<td style="font: bold 13px ;">
									<!-- 任务基本信息 -->
									<s:text name="taskBasicInformation" />
								</td>
								<td>
								</td>
							</tr>
							<tr height="5px">
							</tr>
							<tr>
								<td>
									<!-- 任务创建人：-->
									<s:text name="taskCreatePeopleC" />
								</td>
								<td>
									<a class="table" target="_black"
										href="${basePath}approvePeopleInfo.action?peopleId=${simpleTaskBean.jecnTaskBeanNew.createPersonId}">${simpleTaskBean.jecnTaskBeanNew.createPersonTemporaryName
										}</a>
								</td>
							</tr>
							<tr>
								<td>
									<!-- 开始时间：-->
									<s:text name="startTimeC" />
								</td>
								<td>
									${simpleTaskBean.jecnTaskBeanNew.startTimeStr }
								</td>
							</tr>
							<tr>
								<td>
									<!-- 预估结束时间：-->
									<s:text name="predictEndTimeC" />
								</td>
								<td>
									${simpleTaskBean.jecnTaskBeanNew.endTimeStr }
								</td>
							</tr>
							<tr>
								<td>
									<!-- 联系方式： -->
									<s:text name="contactC" />
								</td>
								<td>
									${simpleTaskBean.phone }
								</td>
							</tr>
						</table>


						<!-- 相关流程信息 -->
						<hr style="border: 1px solid #E8E8E8;">

						<table>
							<tr>
								<td style="font: bold 13px ;" width="100px">
									<s:if
										test="simpleTaskBean.jecnTaskBeanNew.taskType==0 || simpleTaskBean.jecnTaskBeanNew.taskType==4">
										<!-- 相关流程信息 -->
										<s:text name="relatedProcessInformation" />
									</s:if>
									<s:elseif
										test="simpleTaskBean.jecnTaskBeanNew.taskType==2||simpleTaskBean.jecnTaskBeanNew.taskType==3">
										<!-- 制度模板文件 -->
										<s:text name="relatedRuleInformation" />
									</s:elseif>
									<s:elseif test="simpleTaskBean.jecnTaskBeanNew.taskType==1">
										<!-- 文件 -->
										<s:text name="relatedFlieInformation" />
									</s:elseif>
								</td>
								<td></td>
							</tr>
							<tr height="5px">
							</tr>
							<tr>
								<td>
									<s:if
										test="simpleTaskBean.jecnTaskBeanNew.taskType==0 || simpleTaskBean.jecnTaskBeanNew.taskType==4">
										<!-- 流程文件名称： -->
										<s:text name="processFileNameC" />
									</s:if>
									<s:elseif
										test="simpleTaskBean.jecnTaskBeanNew.taskType==2||simpleTaskBean.jecnTaskBeanNew.taskType==3">
										<!-- 制度模板文件 -->
										<s:text name="ruleFileNameC" />
									</s:elseif>
									<s:elseif test="simpleTaskBean.jecnTaskBeanNew.taskType==1">
										<!-- 文件 -->
										<s:text name="fileNameC" />
									</s:elseif>
								</td>
								<td>
									${simpleTaskBean.prfName }
								</td>
							</tr>
							<tr>
								<td>
									<s:if
										test="simpleTaskBean.jecnTaskBeanNew.taskType==0 || simpleTaskBean.jecnTaskBeanNew.taskType==4">
										<!-- 所属业务域： -->
										<s:text name="subordinateProcessMapC" />
									</s:if>
									<s:elseif
										test="simpleTaskBean.jecnTaskBeanNew.taskType==2||simpleTaskBean.jecnTaskBeanNew.taskType==3">
										<!-- 制度模板文件 -->
										<s:text name="fileDirC" />
									</s:elseif>
									<s:elseif test="simpleTaskBean.jecnTaskBeanNew.taskType==1">
										<!-- 文件 -->
										<s:text name="fileDirC" />
									</s:elseif>
								</td>
								<td>
									${simpleTaskBean.prfPreName }
								</td>
							</tr>
							<!-- 编号-->
							<tr
								style="display: <s:if test='simpleTaskBean.jecnTaskApplicationNew.isFlowNumber!=1 || #session.serverFlowInput=="1"'>none</s:if>;">
								<td align="left">
									<!-- 编号 -->
									${simpleTaskBean.typeNumber }
								</td>
								<td class="t5">
									${simpleTaskBean.prfNumber }
								</td>
							</tr>
							<!-- 术语定义-->
							<tr
								style="display: <s:if test='simpleTaskBean.jecnTaskApplicationNew.isDefinition!=1'>none</s:if>;">
								<td align="left">
									<!-- 术语定义 -->
									<s:text name="isDefinition" />
									<s:text name="chinaColon"></s:text>
								</td>
								<td class="t5" style="word-break:break-all">
									${simpleTaskBean.definitionStr }
								</td>
							</tr>
							<!-- 密级 -->
							<tr
								style="display: <s:if test="simpleTaskBean.jecnTaskApplicationNew.isPublic!=1">none</s:if>;">
								<td align="left">
									<s:text name="intensiveC" />
								</td>
								<td class="t5">
									<!-- 如果是文控审核和部门审核可以修改密级权限 -->
									<s:if
										test="((simpleTaskBean.jecnTaskBeanNew.state==1||simpleTaskBean.jecnTaskBeanNew.state==2)&&(simpleTaskBean.jecnTaskBeanNew.taskElseState!=2))">
										<select id="prfPublic" class="text10">
											<!-- 秘密 -->
											<option value="0"
												<s:if test="simpleTaskBean.strPublic==0">selected</s:if>>
												${secret}
											</option>
											<!-- 公开 -->
											<option value="1"
												<s:if test="simpleTaskBean.strPublic==1">selected</s:if>>
												<s:property value="#application.public"/>
											</option>
										</select>
									</s:if>
									<s:else>
										<!-- 无条件限制 -->
										<s:if test="simpleTaskBean.strPublic==0">
											${secret}
										</s:if>
										<s:else>
										    <s:property value="#application.public"/>
										</s:else>
									</s:else>
								</td>
							</tr>
							<!-- 部门查阅权限： -->
							<tr
								style="display: <s:if test="simpleTaskBean.jecnTaskApplicationNew.isAccess!=1">none</s:if>;">
								<td align="left">
									<!-- 部门查阅权限： -->
									<s:text name="departmentAccessPermissionsC" />
								</td>
								<td class="t5">
									<!-- 验证查阅权限是否可编辑 -->
									<!-- 当部门阶段和文控阶段时 ，审核人不是交办人 可以编辑查阅权限 -->
									<s:if
										test="((simpleTaskBean.jecnTaskBeanNew.state==1||simpleTaskBean.jecnTaskBeanNew.state==2)&&(simpleTaskBean.jecnTaskBeanNew.taskElseState!=2))">
										<s:textarea id="orgNames" rows="4" cols="30"
											cssStyle="vertical-align: middle;"
											name="simpleTaskBean.orgNames" theme="simple" readonly="true"></s:textarea>
										<a><img
												onclick='orgPosPerMultipleSelect("organization","orgNames","orgIds")'
												src="${basePath}images/common/select.gif"
												style="vertical-align: top; cursor: pointer;" /> </a>
										<input type="hidden" id="orgIds" name="submitMessage.orgIds"
											value="${simpleTaskBean.orgIds}" />
									</s:if>
									<s:else>
								   ${submitMessage.tableOrgNames }
								</s:else>
								</td>
							</tr>
							<tr
								style="display: <s:if test="simpleTaskBean.jecnTaskApplicationNew.isAccess!=1">none</s:if>;">
								<td align="left">
									<!-- 岗位查阅权限 -->
									<s:text name="postAccessPermissionsC" />
								</td>
								<td class="t5">
									<!-- 当部门阶段和文控阶段时 ，审核人不是交办人 可以编辑查阅权限 -->
									<s:if
										test="((simpleTaskBean.jecnTaskBeanNew.state==1||simpleTaskBean.jecnTaskBeanNew.state==2)&&(simpleTaskBean.jecnTaskBeanNew.taskElseState!=2))">
										<a style="cursor: pointer;"> <!-- 验证查阅权限是否可编辑 --> <s:textarea
												id="posNames" rows="4" cols="30"
												cssStyle="vertical-align: middle;"
												name="simpleTaskBean.posNames" theme="simple"
												readonly="true"></s:textarea> <img
												onclick='orgPosPerMultipleSelect("position","posNames","posIds")'
												src="${basePath}images/common/select.gif"
												style="vertical-align: top; cursor: pointer;" /> </a>
										<input type="hidden" id="posIds" name="submitMessage.posIds"
											value="${simpleTaskBean.posIds}" />
									</s:if>
									<s:else>
								   ${submitMessage.tablePosNames}
								</s:else>
								</td>
							</tr>
							<tr
								style="display: <s:if test="simpleTaskBean.jecnTaskApplicationNew.isAccess!=1">none</s:if>;">
								<td align="left">
									<!-- 岗位组查阅权限 -->
									<s:text name="postGroupAccessPermissionsC" />
								</td>
								<td class="t5">
									<!-- 当部门阶段和文控阶段时 ，审核人不是交办人 可以编辑查阅权限 -->
									<s:if
										test="((simpleTaskBean.jecnTaskBeanNew.state==1||simpleTaskBean.jecnTaskBeanNew.state==2)&&(simpleTaskBean.jecnTaskBeanNew.taskElseState!=2))">
										<a style="cursor: pointer;"> <!-- 验证查阅权限是否可编辑 --> <s:textarea
												id="positionGroup" rows="4" cols="30"
												cssStyle="vertical-align: middle;"
												name="simpleTaskBean.groupNames" theme="simple"
												readonly="true"></s:textarea> <img
												onclick='orgPosPerMultipleSelect("positionGroup","positionGroup","groupIds")'
												src="${basePath}images/common/select.gif"
												style="vertical-align: top; cursor: pointer;" />
										</a>
										<input type="hidden" id="groupIds"
											name="submitMessage.groupIds"
											value="${simpleTaskBean.groupIds}" />
									</s:if>
									<s:else>
								   ${simpleTaskBean.tableGroupNames}
								</s:else>
								</td>
							</tr>
						
							<!-- 流程类别 -->
							<tr
								style='display: <s:if test="simpleTaskBean.jecnTaskApplicationNew.fileType!=1">none</s:if>;'>
								<td align="left">
									<s:text name="processType" />
									<s:text name="chinaColon" />
								</td>
								<td class="t5">
									<s:if
										test="((simpleTaskBean.jecnTaskBeanNew.state==1||simpleTaskBean.jecnTaskBeanNew.state==2)&&(simpleTaskBean.jecnTaskBeanNew.taskElseState!=2))">
										<s:if test="simpleTaskBean.listTypeBean!=null">
											<s:select list="simpleTaskBean.listTypeBean"
												listValue="typeName" listKey="typeId"
												value="simpleTaskBean.prfType"
												name="simpleTaskBean.prfTypeName" id="typeListId"
												theme="simple">
											</s:select>
										</s:if>
									</s:if>
									<s:else>
								  ${simpleTaskBean.prfTypeName}
								</s:else>
									<input type="hidden" id="prfType" name="submitMessage.prfType" />
									<input type="hidden" id="prfTypeName"
										name="submitMessage.prfTypeName" />
								</td>
							</tr>
						</table>

						<hr style="border: 1px solid #E8E8E8;">

						<table>
							<tr>
								<td style="font: bold 13px ;">
									<!-- 审核信息 -->
									<s:text name="auditInformation" />
								</td>
								<td></td>
							</tr>
							<tr height="5px">
							</tr>
							<s:iterator value="simpleTaskBean.listAudit" id="auditList"
								status="st1">

								<tr>
									<s:if
										test="#auditList.auditName!=null&&#auditList.auditName!=''">
										<td>
											<!-- 各任务阶段审核名称 -->
											${auditList.auditLable }
											<s:text name="chinaColon"></s:text>
										</td>
										<td>
											<!-- 各任务阶段审核人姓名 -->
											<s:iterator value="#auditList.tableAuditIdNames" id="idAndName" status="st" >
									            <a class="table" target="_black"
											        href="${basePath}approvePeopleInfo.action?peopleId=${idAndName[0]}">${idAndName[1]}</a><br/>
								           </s:iterator>
										</td>
									</s:if>
								</tr>
							</s:iterator>
						</table>
						<!-- 试运行报告 -->
						<div
							style="<s:if test='simpleTaskBean.taskRunFileId==null'>display:none;</s:if>">
							<table>
								<tr>
									<td style="font: bold 13px ;">
										<!-- 试运行报告 标题-->
										<s:text name="runtestReport" />
									</td>
								</tr>
								<tr>
									<td>
										<table>
											<tr>
												<td>
													<!-- 试运行报告 名称-->
													<s:if test="simpleTaskBean.taskRunFileId!=null">
														<a class="nav2"
															href="downloadAction.action?fileId=${simpleTaskBean.taskRunFileId}&fileName=${simpleTaskBean.taskRunFileName}&fileType=1">${simpleTaskBean.taskRunFileName}</a>
													</s:if>
												</td>
												<td>
													&nbsp;
												</td>
												<td>
													&nbsp;
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>


					<!-- 任务审批流转记录 -->
					<div id="examinationAndApprovalRecords" style="display: none;">
						<div class="scroll01" style="width: 100%">
							<table width="100%" border="0" cellpadding="0" cellspacing="1"
								align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
								<tr class="FixedTitleRow" align="center">
									<th width="2%" class="gradient">
									</th>
									<th bgcolor="#DDDDDD" width="8%" height="35" class="gradient"
										style="word-break: break-all;">
										<!--    审核人 -->
										<s:text name="approvePerson" />
									</th>
									<th bgcolor="#DDDDDD" width="30%" height="35" class="gradient"
										style="word-break: break-all;">
										<!-- 审批意见 -->
										<s:text name="appIdea" />
									</th>
									<th bgcolor="#DDDDDD" width="15%" height="35" class="gradient"
										style="word-break: break-all;">
										<!--    审批变动 -->
										<s:text name="appChange" />
									</th>
									<th bgcolor="#DDDDDD" width="10%" height="35" class="gradient"
										style="word-break: break-all;">
										<!--   阶段-->
										<s:text name="stage" />
									</th>
									<th bgcolor="#DDDDDD" width="12%" height="35" class="gradient"
										style="word-break: break-all;">
										<!--  操作-->
										<s:text name="operation" />
									</th>
									<th bgcolor="#DDDDDD" width="8%" height="35" class="gradient"
										style="word-break: break-all;">
										<!--  目标人-->
										<s:text name="targetPerson" />
									</th>
									<th bgcolor="#DDDDDD" width="13%" height="35" class="gradient"
										style="word-break: break-all;">
										<!--  操作日期-->
										<s:text name="operationDate" />
									</th>
									<th width="2%" class="gradient"></th>
								</tr>
								<!-- 日志集合 -->
								<s:iterator value="simpleTaskBean.listJecnTaskForRecodeNew"
									status="st">
									<tr id="tr0" bgcolor="#ffffff"
										onmouseover="style.backgroundColor='#EFEFEF';"
										onmouseout="style.backgroundColor='#ffffff';">
										<td class="gradient" align="center">
											<s:property value="#st.index+1" />
										</td>
										<!-- 审核人 -->
										<td class="t5" style="word-break: break-all;">
											<s:property value="fromPeopleTemporaryName" />
										</td>
										<!-- 审批意见 -->
										<td class="t5">
											<s:property value="opinion" escape="false" />
										</td>
										<!-- 审批变动  -->
										<td class="t5" style="word-break: break-all;">
											<s:property value="taskFixedDesc" />
										</td>
										<!-- 阶段  -->
										<td class="t5" style="word-break: break-all;">
											<s:if test='upState=="0"'>
												<s:text name="draft" />
											</s:if>
											<s:else>${upStateName }</s:else>
										</td>
										<!-- 操作 -->
										<td class="t5" style="word-break: break-all;">
											<!-- 拟稿人提交审批 -->
											<s:if test='taskElseState=="0"'>
												<s:text name="draftsubmitted" />
											</s:if>
											<!-- 通过 -->
											<s:elseif test='taskElseState=="1"'>
												<s:text name="isPass" />
											</s:elseif>
											<!-- 交办 -->
											<s:elseif test='taskElseState=="2"'>
												<s:text name="assigned" />
											</s:elseif>
											<!-- 转批 -->
											<s:elseif test='taskElseState=="3"'>
												<s:text name="subconcessions" />
											</s:elseif>
											<!-- 打回 -->
											<s:elseif test='taskElseState=="4"'>
												<s:text name="fightBack" />
											</s:elseif>
											<!-- 提交意见 -->
											<s:elseif test='taskElseState=="5"'>
												<s:text name="submitComments" />
											</s:elseif>
											<!-- 二次评审 -->
											<s:elseif test='taskElseState=="6"'>
												<s:text name="secondReview" />
											</s:elseif>
											<!-- 拟稿人重新提交审批 -->
											<s:elseif test='taskElseState=="7"'>
												<s:text name="draftRe_submitted" />
											</s:elseif>
											<!-- 完成 -->
											<s:elseif test='taskElseState=="8"'>
												<s:text name="complete" />
											</s:elseif>
											<!-- 打回整理意见 -->
											<s:elseif test='taskElseState=="9"'>
												<s:text name="finishingfightback" />
											</s:elseif>
											<!-- 交办人提交 -->
											<s:elseif test='taskElseState=="10"'>
												<s:text name="assignedbysubmitted" />
											</s:elseif>
											<!-- 编辑 -->
											<s:elseif test='taskElseState=="11"'>
												<s:text name="editor" />
											</s:elseif>
											<!-- 撤回 -->
											<s:elseif test='taskElseState=="13"'>
												<s:text name="reBackTask" />
											</s:elseif>
											<!-- 返回 -->
											<s:elseif test='taskElseState=="14"'>
												<s:text name="reCallToTask" />
											</s:elseif>
											<!-- 无任何操作 -->
											<s:else>
												<s:text name="none" />
											</s:else>
										</td>
										<!-- 目标人  -->
										<td class="t5" style="word-break: break-all;">
											<s:property value="toPeopleTemporaryName" />
										</td>
										<!-- 操作日期 -->
										<td class="t5" style="word-break: break-all;">
											<s:date name="updateTime" format="yyyy-MM-dd HH:mm:ss" />
										</td>
										<td></td>
									</tr>
								</s:iterator>
							</table>
						</div>
					</div>
				</div>
			</s:form>
		</div>

		<input type="hidden" id="taskType"
			value="${simpleTaskBean.jecnTaskBeanNew.taskType }" />
		<input type="hidden" id="taskId"
			value="${simpleTaskBean.jecnTaskBeanNew.id }" />
		<input type="hidden" id="isControlauditLead"
			value="${simpleTaskBean.jecnTaskBeanNew.isControlauditLead }" />
		<input type="hidden" id="state"
			value="${simpleTaskBean.jecnTaskBeanNew.state }" />
		<!-- 登录人人员主键 -->
		<input type="hidden" id="curPeopleId"
			value="<s:property value='#session.webLoginBean.jecnUser.peopleId'/>">
	</body>
</html>