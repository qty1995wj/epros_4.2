<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title></title>
	</head>
	<body>
		<div id="ruleRelateFileId"  style="overflow-y: scroll;">
			<div id="ruleRelateFile"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; float: left; width: 98%;" >
				
				<table align="left" width="98%">
			 			<s:iterator value="listRuleRelateFile" id="str">
					<!-- 流程表单 -->
					<tr>
						<td style="padding-left: 10px;" align="left">
<%--							<s:if test="listJecnFlowCommon.size > 0">--%>
								<table width="98%" style="margin-top: 12px;">
									<tr style="width: 100%;"><!-- background-color: #F3F1F1; -->
										<td
											style="font-weight: bold; font-size: 13px;">
											<s:text name="relatedProcess" />
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellpadding="0" cellspacing="1"
									align="center" bgcolor="#BFBFBF" style="table-layout: fixed;" >
									<tr class="FixedTitleRow" align="center">
										<td width="2%" class="gradient">
										</td>
										<td bgcolor="#DDDDDD" width="25%" height="25" 
											style="word-break: break-all;"  class="gradient">
											<s:text name="processNumber" />
										</td>
										<td bgcolor="#DDDDDD" class="gradient"
											style="word-break: break-all;">
											<s:text name="processName" />
										</td>
										<td width="2%" class="gradient">
										</td>
									</tr>
									<s:iterator value="listJecnFlowCommon" status = "st">
										<tr align="center" bgcolor="#ffffff"
											onmouseover="style.backgroundColor='#EFEFEF';"
											onmouseout="style.backgroundColor='#ffffff';" height="20">
											<td align="right" class="gradient">
												<s:property value="#st.index+1" />
											</td>
											<td width="25%" class="t5" style="word-break: break-all;">
												<s:property value="flowNumber" />
											</td>
											<td class="t5" style="word-break: break-all;">
												<a
													href="process.action?type=process&flowId=<s:property value='flowId'/>"
													target='_blank' style="cursor: pointer;" class="table">
													<s:property value="flowName" /> </a>
											</td>
											<td></td>
										</tr>
									</s:iterator>
								</table>
<%--							</s:if>--%>
<%--							<s:else>--%>
<%--								<s:text name="none" />--%>
<%--							</s:else>--%>
						</td>
					</tr>

					<!-- 流程地图表单 -->
					<tr>
						<td style="padding-left: 10px;" align="left">
<%--							<s:if test="listJecnFlowMapCommon.size > 0">--%>
								<table width="98%" style="margin-top: 12px;">
									<tr style="width: 100%;"><!-- background-color: #F3F1F1; -->
										<td
											style="font-weight: bold; font-size: 13px;">
											<s:text name="ruleRelateFlowMap" />
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellpadding="0" cellspacing="1"
									align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
									<tr class="FixedTitleRow" align="center">
										<td width="2%" class="gradient">
										</td>
										<td bgcolor="#DDDDDD" width="25%" height="25" class="gradient"
											style="word-break: break-all;">
											<s:text name="processMapNumbersC" />
										</td>
										<td bgcolor="#DDDDDD" class="gradient"
											style="word-break: break-all;">
											<s:text name="processMapName" />
										</td>
										<td width="2%" class="gradient">
										</td>
									</tr>
									<s:iterator value="listJecnFlowMapCommon" status ="st">
										<tr align="center" bgcolor="#ffffff"
											onmouseover="style.backgroundColor='#EFEFEF';"
											onmouseout="style.backgroundColor='#ffffff';" height="20">
											<td width="2%" align="right" class="gradient">
												<s:property value="#st.index+1" />
											</td>
											<td width="25%" class="t5" style="word-break: break-all;">
												<s:property value="flowNumber" />
											</td>
											<td class="t5" style="word-break: break-all;">
												<a
													href="process.action?type=processMap&flowId=<s:property value='flowId'/>"
													target='_blank' style="cursor: pointer;" class="table">
													<s:property value="flowName" /> </a>
											</td>
											<td></td>
										</tr>
									</s:iterator>
								</table>
<%--							</s:if>--%>
<%--							<s:else>--%>
<%--								<s:text name="none" />--%>
<%--							</s:else>--%>
						</td>
					</tr>
					<!-- 标准表单 -->
					<tr>
						<td style="padding-left: 10px;" align="left">
<%--							<s:if test="listJecnStandarCommon.size > 0">--%>
								<table width="98%" style="margin-top: 12px;">
									<tr style=" width: 100%;"> <!-- background-color: #F3F1F1; -->
										<td
											style="font-weight: bold; font-size: 13px;">
											<s:text name="relatedStandard" />
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellpadding="0" cellspacing="1"
									align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
									<tr class="FixedTitleRow" align="center">
										<td width="2%" class="gradient">
										</td>
										<td width="25%"  height="25" bgcolor="#DDDDDD" class="gradient"
											style="word-break: break-all;">
											<s:text name="type" />
										</td>
										<td bgcolor="#DDDDDD"  class="gradient"
											style="word-break: break-all;">
											<s:text name="standardName" />
										</td>
										<td width="2%" class="gradient">
										</td>
									</tr>
									<s:iterator value="listJecnStandarCommon" status = "st">
										<tr align="center" bgcolor="#ffffff"
											onmouseover="style.backgroundColor='#EFEFEF';"
											onmouseout="style.backgroundColor='#ffffff';" height="20">
											<td width="2%" align="right" class="gradient">
												<s:property value="#st.index+1" />
											</td>
<%--											<td class="t5" style="word-break: break-all;">--%>
												<s:if test="standaType==1">
													<td class="t5" style="word-break: break-all;">
													<s:text name="fileStandar" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?from=rule&reqType=public&standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:if>
												<s:elseif test="standaType== 2">
													<td class="t5" style="word-break: break-all;">
													<s:text name="flowStandar" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?from=rule&reqType=public&standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
												<s:elseif test="standaType== 3">
													<td class="t5" style="word-break: break-all;">
													<s:text name="flowMapStandar" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?from=rule&reqType=public&standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
												<s:elseif test="standaType == 4">
													<td class="t5" style="word-break: break-all;">
													<s:text name="standarClause" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
												<s:elseif test="standaType == 5">
													<td class="t5" style="word-break: break-all;">
													<s:text name="clauseRequest" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?from=rule&reqType=public&standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
<%--											</td>--%>
<%--											<td width="25%" class="t5" style="word-break: break-all;">--%>
<%--												<a--%>
<%--													href="standard.action?standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"--%>
<%--													target='_blank' style="cursor: pointer;" class="table">--%>
<%--													<s:property value="standarName" /> </a>--%>
<%--											</td>--%>
											<td></td>
										</tr>
									</s:iterator>
								</table>
<%--							</s:if>--%>
<%--							<s:else>--%>
<%--								<s:text name="none" />--%>
<%--							</s:else>--%>
						</td>
					</tr>
					<!-- 风险表单 -->
					<tr>
						<td style="padding-left: 10px;" align="left">
<%--							<s:if test="listJecnRiskCommon.size > 0">--%>
								<table width="98%" style="margin-top: 12px;">
									<tr style="width: 100%;"><!-- background-color: #F3F1F1; -->
										<td
											style="font-weight: bold; font-size: 13px;">
											<s:text name="ruleRelateRisk" />
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellpadding="0" cellspacing="1"
									align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
									<tr class="FixedTitleRow" align="center">
										<td width="2%" class="gradient">
										</td>
										<td bgcolor="#DDDDDD" width="25%" height="25" class="gradient"
											style="word-break: break-all;">
											<s:text name="riskNumberC" />
										</td>
										<td bgcolor="#DDDDDD" class="gradient"
											style="word-break: break-all;">
											<s:text name="riskNameC" />
										</td>
										<td width="2%" class="gradient">
										</td>
									</tr>
									<s:iterator value="listJecnRiskCommon" status = "st">
										<tr align="center" bgcolor="#ffffff"
											onmouseover="style.backgroundColor='#EFEFEF';"
											onmouseout="style.backgroundColor='#ffffff';" height="20">
											<td width="2%" align="right" class="gradient">
												<s:property value="#st.index+1" />
											</td>
											<td width="25%" class="t5" style="word-break: break-all;">
												<s:property value="riskNumber" />
											</td>
											<td class="t5" style="word-break: break-all;">
												<a
													href="getRiskById.action?riskDetailId=<s:property value='riskId'/>"
													target='_blank' style="cursor: pointer;" class="table">
													<s:property value="riskName" /> </a>
											</td>
											<td></td>
										</tr>
									</s:iterator>
								</table>
<%--							</s:if>--%>
<%--							<s:else>--%>
<%--								<s:text name="none" />--%>
<%--							</s:else>--%>
						</td>
					</tr>
					</s:iterator>
				</table>

			</div>
		</div>
	</body>
</html>