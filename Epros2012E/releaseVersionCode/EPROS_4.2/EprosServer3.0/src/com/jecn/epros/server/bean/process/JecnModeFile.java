package com.jecn.epros.server.bean.process;

/**
 * 
 * 活动的输出bean(浏览端)
 * 
 * @author Administrator
 * 
 */
public class JecnModeFile implements java.io.Serializable {
	private Long modeFileId;// 主键
	private Long figureId;// 活动元素ID
	private String figureUUID;// 活动元素索引
	private String modeName;// 文件名称
	private Long fileMId;// 输出的表单 文件ID
	private String UUID;// 主键索引

	public Long getModeFileId() {
		return modeFileId;
	}

	public void setModeFileId(Long modeFileId) {
		this.modeFileId = modeFileId;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public Long getFileMId() {
		return fileMId;
	}

	public void setFileMId(Long fileMId) {
		this.fileMId = fileMId;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}

}
