package epros.designer.gui.process.guide.explain;

import java.util.Map;

import epros.designer.gui.process.activitiesProperty.ActivitiesDetailsDialog;
import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 点击活动说明弹出的dialog
 * 
 * @author user
 * 
 */
public class JecnActiveDescDialog extends ActivitiesDetailsDialog {
	/** 点击table的行数 */

	private Map<String, JecnFileDescriptionComponent> listComponent;

	private ProcessOperationDialog processOperationDialog;

	public JecnActiveDescDialog(JecnActivityShowBean activityShowBean, ProcessOperationDialog processOperationDialog) {
		super(activityShowBean.getActiveFigure().getFlowElementData(), activityShowBean.getActiveFigure());
		this.processOperationDialog = processOperationDialog;
		this.listComponent = processOperationDialog.getListComponent();
	}

	/**
	 * 确定事件
	 * 
	 * @author fuzhh Oct 11, 2012
	 */
	public void okButPerformed() {
		super.okButPerformed();
		// 关键活动
		processOperationDialog.setFlowOperationBean();
		JecnFileDescriptionComponent jecnCompont = listComponent.get("a7");
		if (jecnCompont != null && jecnCompont instanceof JecnKeyActive) {
			JecnKeyActive keyActive = (JecnKeyActive) jecnCompont;
			keyActive.refreshTable();
		}
		// 活动说明
		jecnCompont = listComponent.get("a10");
		if (jecnCompont != null && jecnCompont instanceof JecnActiveDesc) {
			JecnActiveDesc jecnActiveDesc = (JecnActiveDesc) jecnCompont;
			jecnActiveDesc.getTable().refluseTable();
		}
		// 流程记录
		jecnCompont = listComponent.get("a11");
		if (jecnCompont != null && jecnCompont instanceof JecnProcessActiveFile) {
			JecnProcessActiveFile activeFile = (JecnProcessActiveFile) jecnCompont;
			activeFile.getTable().refluseTable();
		}
		// 操作规范
		jecnCompont = listComponent.get("a12");
		if (jecnCompont != null && jecnCompont instanceof JecnProcessActiveFile) {
			JecnProcessActiveFile activeFile = (JecnProcessActiveFile) jecnCompont;
			activeFile.getTable().refluseTable();
		}

		// 面板保存
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		this.dispose();
	}

	/***************************************************************************
	 * 取消
	 */
	protected void cancelPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int tipAction = dialogCloseBeforeMsgTipAction();
			if (tipAction == 2) {// 信息提示：true：需要执行关闭 false：不要执行关闭
				// this.setVisible(false);
				this.okButPerformed();
			} else if (tipAction == 1) {
				this.setVisible(false);
			}
		}
	}

}
