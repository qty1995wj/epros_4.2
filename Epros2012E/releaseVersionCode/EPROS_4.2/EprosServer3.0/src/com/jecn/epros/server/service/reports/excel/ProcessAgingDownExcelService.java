package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;

import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.AgingProcessBean;

/**
 * 
 * 流程时效性统计
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessAgingDownExcelService extends BaseDownExcelService {
	/** 数据对象 */
	private AgingProcessBean agingProcessBean = null;

	public ProcessAgingDownExcelService() {
		this.mouldPath = getPrefixPath() + "mould/processAgingReport.xls";
		this.startDataRow = 3;
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (agingProcessBean == null) {
			return true;
		}
		
		try {
			// 日期(
			String prefixFirst = JecnUtil.getValue("prefixFirst");
			// )统计：间隔时间(月)：
			String suffixFirst = JecnUtil.getValue("suffixFirst2");
			// 未更新
			String update = JecnUtil.getValue("failedToUpdate");
			// 已更新
			String updated = JecnUtil.getValue("hasBeenUpdated");
			// 个
			String geUnit = JecnUtil.getValue("geUnit");

			// 第一行
			// 字体样式
			WritableCellFormat wcfFC = getWritableCellFormat();
			// 当前时间（yyyy-MM-dd）
			String date = getDate();
			String content = prefixFirst + date + suffixFirst
					+ agingProcessBean.getTime();
			dataSheet.addCell(new Label(0, 0, content, wcfFC));

			// 第二行
			// 字体样式
			wcfFC = getWritableCellFormat();
			// 居中
			wcfFC.setAlignment(getAlignment());
			content = update + agingProcessBean.getFailedToUpdate() + geUnit
					+ "  " + updated + agingProcessBean.getHasBeenUpdated()
					+ geUnit;
			dataSheet.addCell(new Label(0, 1, content, wcfFC));

			// 第四行开始
			List<ProcessWebBean> processWebList = agingProcessBean
					.getProcessWebList();
			if (processWebList != null) {
				for (int i = 0; i < processWebList.size(); i++) {
					ProcessWebBean bean = processWebList.get(i);
					int row = startDataRow + i;
					// 流程名称
					dataSheet.addCell(new Label(0, row, bean.getFlowName()));
					// 流程编号
					dataSheet.addCell(new Label(1, row, bean.getFlowIdInput()));
					// 流程责任人
					dataSheet
							.addCell(new Label(2, row, bean.getResPeopleName()));
					// 责任部门
					dataSheet.addCell(new Label(3, row, bean.getOrgName()));
					// 发布时间
					dataSheet.addCell(new Label(4, row, bean.getPubDate()));
					// 状态 0未更新，1已更新状态
					String state = (bean.getUpdateType() == 0) ? update
							: updated;
					dataSheet.addCell(new Label(5, row, state));
				}
			}

			return true;
		} catch (Exception e) {
			log.error("ProcessAgingDownExcelService类editExcle方法", e);
			return false;
		}
	}

	public AgingProcessBean getAgingProcessBean() {
		return agingProcessBean;
	}

	public void setAgingProcessBean(AgingProcessBean agingProcessBean) {
		this.agingProcessBean = agingProcessBean;
	}
}
