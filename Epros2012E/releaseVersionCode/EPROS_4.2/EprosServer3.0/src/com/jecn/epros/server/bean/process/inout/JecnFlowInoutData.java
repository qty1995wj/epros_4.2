package com.jecn.epros.server.bean.process.inout;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class JecnFlowInoutData implements Serializable {
	private JecnFlowInoutBase inout;
	private List<JecnTreeBean> poses;
	private List<JecnTreeBean> posGroups;

	public JecnFlowInoutBase getInout() {
		return inout;
	}

	public void setInout(JecnFlowInoutBase inout) {
		this.inout = inout;
	}

	public List<JecnTreeBean> getPoses() {
		return poses;
	}

	public void setPoses(List<JecnTreeBean> poses) {
		this.poses = poses;
	}

	public List<JecnTreeBean> getPosGroups() {
		return posGroups;
	}

	public void setPosGroups(List<JecnTreeBean> posGroups) {
		this.posGroups = posGroups;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JecnFlowInoutData) {
			boolean isUpdate = true;
			JecnFlowInoutData bean = (JecnFlowInoutData) obj;
			if ((this.posGroups != null) && (bean.getPosGroups() != null)) {
				if (this.posGroups.size() != bean.getPosGroups().size()) {
					return false;
				}
				if (!this.posGroups.containsAll(bean.getPosGroups())) {
					return false;
				}
			}
			if ((this.poses != null) && (bean.getPoses() != null)) {
				if (this.poses.size() != bean.getPoses().size()) {
					return false;
				}
				if (!this.poses.containsAll(bean.getPoses())) {
					return false;
				}
			}
			if (this.inout != null && bean.getInout() != null) {
				if (!this.inout.equals(bean.getInout())) {
					return false;
				}
			}
			return isUpdate;
		}
		return super.equals(obj);
	}
}
