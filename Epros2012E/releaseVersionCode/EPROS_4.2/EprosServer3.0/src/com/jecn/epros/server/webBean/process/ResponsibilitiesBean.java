package com.jecn.epros.server.webBean.process;

public class ResponsibilitiesBean {
	/**流程ID*/
	private String processId;
	/**流程名称*/
	private String processName;
	/**流程编号*/
	private String flowInputId;
	/**角色职责*/
	private String roleRes;
	/**角色名称*/
	private String roleName;
	/**总个数*/
	private String sizeTotal;
	/**活动编号*/
	private String activityId;
	/**活动名称*/
	private String activityName;
	/**活动主键*/
	private String figureId;
	/**活动对应指标个数*/
	private String sizeIndacations;
	/**指标*/
	private String indicatorName;
	/**指标值*/
	private String indicatorValue;
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getFlowInputId() {
		return flowInputId;
	}
	public void setFlowInputId(String flowInputId) {
		this.flowInputId = flowInputId;
	}
	public String getRoleRes() {
		return roleRes;
	}
	public void setRoleRes(String roleRes) {
		this.roleRes = roleRes;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getSizeTotal() {
		return sizeTotal;
	}
	public void setSizeTotal(String sizeTotal) {
		this.sizeTotal = sizeTotal;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getFigureId() {
		return figureId;
	}
	public void setFigureId(String figureId) {
		this.figureId = figureId;
	}
	public String getSizeIndacations() {
		return sizeIndacations;
	}
	public void setSizeIndacations(String sizeIndacations) {
		this.sizeIndacations = sizeIndacations;
	}
	public String getIndicatorName() {
		return indicatorName;
	}
	public void setIndicatorName(String indicatorName) {
		this.indicatorName = indicatorName;
	}
	public String getIndicatorValue() {
		return indicatorValue;
	}
	public void setIndicatorValue(String indicatorValue) {
		this.indicatorValue = indicatorValue;
	}
}
