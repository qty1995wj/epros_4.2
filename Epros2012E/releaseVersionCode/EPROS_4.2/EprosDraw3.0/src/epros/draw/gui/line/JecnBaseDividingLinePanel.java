package epros.draw.gui.line;

import java.awt.Graphics;
import java.awt.Point;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnXYData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.gui.line.resize.JecnDividingLineResizePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 横线、竖线、斜线（老版分割线）
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnBaseDividingLinePanel extends JecnBaseLinePanel {

	// ******************** 线段上对应的三个点 start ********************//
	/** 选中点：开始点 */
	protected JecnDividingLineResizePanel lineResizePanelStart = null;
	/** 选中点：中间点 */
	protected JecnDividingLineResizePanel lineResizePanelCenter = null;
	/** 选中点：结束点 */
	protected JecnDividingLineResizePanel lineResizePanelEnd = null;
	/** 添加图形生成虚拟图形边框 */
	protected TempDividingLine tempDividingLine = null;
	/** 分割线为横线或竖线标识 horizontal, // 水平vertical, // 垂直 error、 不正确类型 */
	protected LineDirectionEnum directionEnum = LineDirectionEnum.error;

	/** 是否显示分割线显示点 true:显示分割线显示点 */
	protected boolean isShowLineResize = false;

	// ******************** 线段上对应的三个点 end ********************//
	public JecnBaseDividingLinePanel(JecnBaseDivedingLineData flowElementData) {
		super(flowElementData);

		initComponents();
	}

	public JecnBaseDivedingLineData getFlowElementData() {
		return (JecnBaseDivedingLineData) flowElementData;
	}

	protected void initComponents() {
		// 无边框
		this.setBorder(null);
		// 透明
		this.setOpaque(false);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 选中点：开始点
		setLineResizePanelStart(JecnDividingLineResizePanel
				.createStartLineResizePanel(this));
		// 选中点：中间点
		lineResizePanelCenter = JecnDividingLineResizePanel
				.createCenterLineResizePanel(this);
		// 选中点：结束点
		lineResizePanelEnd = JecnDividingLineResizePanel
				.createEndLineResizePanel(this);
	}

	/**
	 * 
	 * 分割线显示点
	 * 
	 * @param desktopPane
	 */
	public void showLineResizeHandles() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (desktopPane == null) {
			return;
		}
		// 隐藏分割线选中点
		hideLineResizeHandles();
		isShowLineResize = true;
		int resizeWidth = this.lineResizePanelStart.getWidth();
		int resizeHeight = this.lineResizePanelStart.getHeight();
		Point startPos = this.getStartPoint();
		Point endPos = this.getEndPoint();
		if (this.lineResizePanelEnd != null) {
			desktopPane.add(this.lineResizePanelEnd);
			desktopPane.setLayer(this.lineResizePanelEnd, this
					.getFlowElementData().getZOrderIndex() + 1);
			this.lineResizePanelEnd.setLocation(DrawCommon.convertDoubleToInt(endPos
					.getX()
					- resizeWidth / 2.0), DrawCommon.convertDoubleToInt(endPos.getY()
					- resizeHeight / 2.0));
		}
		if (this.lineResizePanelCenter != null) {
			desktopPane.add(this.lineResizePanelCenter);
			desktopPane.setLayer(this.lineResizePanelCenter, this
					.getFlowElementData().getZOrderIndex() + 1);
			this.lineResizePanelCenter.setLocation(DrawCommon.convertDoubleToInt(Math
					.abs(endPos.getX() + startPos.getX())
					/ 2.0 - resizeWidth / 2.0), DrawCommon.convertDoubleToInt(Math
					.abs(endPos.getY() + startPos.getY())
					/ 2.0 - resizeHeight / 2.0));
		}
		desktopPane.add(this.lineResizePanelStart);
		desktopPane.setLayer(this.lineResizePanelStart, this
				.getFlowElementData().getZOrderIndex() + 1);
		this.lineResizePanelStart.setLocation(DrawCommon.convertDoubleToInt(startPos
				.getX()
				- resizeWidth / 2.0), DrawCommon.convertDoubleToInt(startPos.getY()
				- resizeHeight / 2.0));
	}

	/**
	 * 
	 * 
	 * 隐藏分割线选中点
	 * 
	 * @param this
	 */
	public void hideLineResizeHandles() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (desktopPane == null) {
			return;
		}
		isShowLineResize = false;
		if (this.lineResizePanelStart != null) {
			desktopPane.remove(this.lineResizePanelStart);
		}
		if (this.lineResizePanelEnd != null) {
			desktopPane.remove(this.lineResizePanelEnd);
		}
		if (this.lineResizePanelCenter != null) {
			desktopPane.remove(this.lineResizePanelCenter);
		}
	}

	/**
	 * 选中线的判断是否线在选中区域
	 * 
	 * @param px
	 * @param py
	 * @return
	 */
	public boolean lineContainsPoint(int px, int py) {
		// 判断是否在区域内
		if (this.getStartPoint().getX() == this.getEndPoint().getX()
				&& py <= Math.max(this.getStartPoint().getY(), this
						.getEndPoint().getY())
				&& py >= Math.min(this.getStartPoint().getY(), this
						.getEndPoint().getY())) {
			if (Math.abs(this.getStartPoint().getX() - px) <= 5) {
				return true;
			}
		}
		if (this.getStartPoint().getY() == this.getEndPoint().getY()
				&& py <= Math.max(this.getStartPoint().getX(), this
						.getEndPoint().getX())
				&& px >= Math.min(this.getStartPoint().getX(), this
						.getEndPoint().getX())) {
			if (Math.abs(this.getStartPoint().getY() - py) <= 5) {
				return true;
			}
		}
		if (px <= Math.max(this.getStartPoint().getX(), this.getEndPoint()
				.getX())
				&& px >= Math.min(this.getStartPoint().getX(), this
						.getEndPoint().getX())
				&& py <= Math.max(this.getStartPoint().getY(), this
						.getEndPoint().getY())
				&& py >= Math.min(this.getStartPoint().getY(), this
						.getEndPoint().getY())) {
			double a, b, x, y;
			a = (double) (this.getStartPoint().getY() - this.getEndPoint()
					.getY())
					/ (double) (this.getStartPoint().getX() - this
							.getEndPoint().getX());
			b = (double) this.getStartPoint().getY() - a
					* (double) this.getStartPoint().getX();
			x = (py - b) / a;
			y = a * px + b;
			if (Math.min(Math.abs(x - px), Math.abs(y - py)) <= 5) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取当前放大缩小倍数下分割线开始点
	 * 
	 * @return Point
	 */
	public Point getStartPoint() {
		JecnXYData jecnXYData = getFlowElementData().getDiviLineCloneable()
				.getStartXY();
		// 当前面板放大缩小倍数
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		// 创建需要获取的point 点
		Point point = new Point();
		// 设置Point的位置
		point.setLocation(jecnXYData.getX() * scale, jecnXYData.getY() * scale);
		return point;
	}

	/**
	 * 获取当前放大缩小倍数下分割线结束点
	 * 
	 * @return Point
	 */
	public Point getEndPoint() {
		JecnXYData jecnXYData = getFlowElementData().getDiviLineCloneable()
				.getEndXY();
		// 当前面板放大缩小倍数
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		// 创建需要获取的point 点
		Point point = new Point();
		// 设置Point的位置
		point.setLocation(jecnXYData.getX() * scale, jecnXYData.getY() * scale);
		return point;
	}

	//
	// /**
	// * 获取当前线段100%状态下开始点
	// *
	// * @return Point开始点
	// */
	// public Point getOriStartPoint() {
	// JecnXYData jecnXYData = getFlowElementData().getDiviLineCloneable()
	// .getStartXY();
	// return jecnXYData.getPoint();
	// }
	//
	// /**
	// * 获取当前线段100%状态下结束点
	// *
	// * @return Point结束点
	// */
	// public Point getOriEndPoint() {
	// JecnXYData jecnXYData = getFlowElementData().getDiviLineCloneable()
	// .getEndXY();
	// return jecnXYData.getPoint();
	// }

	/**
	 * 
	 * 绘制组件
	 * 
	 */
	public void paintComponent(Graphics g) {
		// 创建line2D对象
		JecnLine2D line2D = new JecnLine2D(this.getStartPoint(), this
				.getEndPoint(), this.getFlowElementData()
				.getDiviLineCloneable().getLineDirectionEnum());
		line2D.setBodyColor(this.getFlowElementData().getBodyColor());
		line2D.setStroke(this.getFlowElementData().getBodyBasicStroke());
		line2D.paint(g);
	}

	/**
	 * 获取线段原始状态下 位置点和坐标点数据对象
	 * 
	 * @param startPoint
	 * @param endPoint
	 */
	public void reSetAttributes(Point startPoint, Point endPoint) {
		this.getFlowElementData().getDiviLineCloneable().reSetAttributes(
				startPoint, endPoint, this.getFlowElementData().getBodyWidth(),
				this.getFlowElementData().getWorkflowScale());
	}

	protected void reSetLocation(Point startPoint, Point endPoint) {

	}

	/**
	 * 设置横向分隔符位置点
	 * 
	 * @param startPoint
	 * @param endPoint
	 */
	public void setBounds(Point startPoint, Point endPoint) {
		if (startPoint == null || endPoint == null) {
			return;
		}

		// 当前放大缩小下的倍数
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		// 线条宽度
		int lineSizeWidth = DrawCommon.convertDoubleToInt(this.getFlowElementData()
				.getBodyWidth()
				* scale);
		if (lineSizeWidth < 1) {
			lineSizeWidth = 1;
		}
		// 纵向分隔符 开始点坐标
		int startX = startPoint.x;
		int startY = startPoint.y;
		// 纵向分隔符 结束点坐标
		int endX = endPoint.x;
		int endY = endPoint.y;
		switch (this.getFlowElementData().getDiviLineCloneable()
				.getLineDirectionEnum()) {
		case horizontal:
			// 设置位置
			this.setBounds(DrawCommon.convertDoubleToInt(Math.min(startX, endX)
					- lineSizeWidth / 2.0), DrawCommon.convertDoubleToInt(endY
					- lineSizeWidth / 2.0), Math.abs(startX - endX)
					+ lineSizeWidth, lineSizeWidth);
			break;
		case vertical:
			this.setBounds(DrawCommon
					.convertDoubleToInt(startX - lineSizeWidth / 2.0), Math
					.min(startY, endY), lineSizeWidth, Math.abs(startY - endY));
			break;
		default:
			break;
		}

	}

	/**
	 * 根据位置点设置分割线位置及大小
	 * 
	 * @param location
	 *            Point图形位置点
	 */
	public void setVHDiviLineBounds(Point location) {
		Point endPoint = null;

		// 当前放大缩小下的倍数
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();

		int width = DrawCommon.convertDoubleToInt(this.getFlowElementData()
				.getFigureSizeX()
				* scale);
		int height = DrawCommon.convertDoubleToInt(this.getFlowElementData()
				.getFigureSizeY()
				* scale);

		if (width > height) {// 横线
			endPoint = new Point(location.x + width, location.y);
		} else {// 纵线
			endPoint = new Point(location.x, location.y + height);
		}
		if (endPoint == null) {
			return;
		}
		// 横向分隔符
		this.reSetAttributes(location, endPoint);
		// 设置线段位置点和大小
		this.setBounds(location, endPoint);
	}

	public void setLineResizePanelStart(
			JecnDividingLineResizePanel lineResizePanelStart) {
		this.lineResizePanelStart = lineResizePanelStart;
	}

	public void setLineResizePanelCenter(
			JecnDividingLineResizePanel lineResizePanelCenter) {
		this.lineResizePanelCenter = lineResizePanelCenter;
	}

	public void setLineResizePanelEnd(
			JecnDividingLineResizePanel lineResizePanelEnd) {
		this.lineResizePanelEnd = lineResizePanelEnd;
	}

	public JecnDividingLineResizePanel getLineResizePanelStart() {
		return lineResizePanelStart;
	}

	public JecnDividingLineResizePanel getLineResizePanelCenter() {
		return lineResizePanelCenter;
	}

	public JecnDividingLineResizePanel getLineResizePanelEnd() {
		return lineResizePanelEnd;
	}

	/**
	 * 创建虚拟分割线对象
	 * 
	 * @return
	 */

	public TempDividingLine createTempLinePanel() {
		if (tempDividingLine == null) {
			tempDividingLine = new TempDividingLine();
		}
		// 拖动时虚拟分割线段
		return tempDividingLine;
	}

	/**
	 * 拖动分割线重置分割线开始点和结束点，记录分割线原始大小
	 * 
	 */
	public void moveLineResetPoint() {
		if (this instanceof DividingLine) {
			if (tempDividingLine.getStartPoint() == null
					|| tempDividingLine.getEndPoint() == null) {
				return;
			}
			// 设置拖动点位置及分割线原始位置
			initOriginalArributs(tempDividingLine.getStartPoint(),
					tempDividingLine.getEndPoint(), this, this
							.getLineResizePanelCenter());
		} else {
			setVHDiviLineBounds(this.getTempFigureBean().getLocalPoint());
		}
		// 分割线显示点
		this.showLineResizeHandles();
	}

	/**
	 * 设置拖动点位置及分割线原始位置
	 * 
	 * @param curStartPoint
	 *            Point 拖动分割线的当前端点
	 * @param curLine
	 *            JecnBaseDividingLinePanel 拖动的分割线
	 * @param lineResizePanel
	 *            JecnDividingLineResizePanel 拖动的当前端点对应的显示点
	 */
	public void initOriginalArributs(Point curStartPoint, Point curEndPoint,
			JecnBaseDividingLinePanel curLine,
			JecnDividingLineResizePanel lineResizePanel) {
		// 记录当前位置的原始点
		curLine
				.getFlowElementData()
				.getDiviLineCloneable()
				.initOriginalArributs(curStartPoint, curEndPoint,
						curLine.getFlowElementData().getBodyWidth(),
						JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());

		int width = this.getFlowElementData().getDiviLineCloneable().getWidth();

		int height = this.getFlowElementData().getDiviLineCloneable()
				.getHeight();

		curLine.getFlowElementData().setFigureSizeX(width);
		curLine.getFlowElementData().setFigureSizeY(height);

		// 重设分割线显示点的位置
		if (lineResizePanel != null) {
			lineResizePanel.setLocation(curStartPoint);
		}
		// 设置分隔符位置点
		reSetLocation(curStartPoint, curEndPoint);
	}

	public void setTempDividingLine(TempDividingLine tempDividingLine) {
		this.tempDividingLine = tempDividingLine;
	}

	/**
	 * 记录分割线100%状态下的克隆值
	 * 
	 * @param startPoint
	 *            Point分割线开始点
	 * @param endPoint
	 *            Point分割线结束点
	 */
	public void originalCloneData(Point startPoint, Point endPoint) {
		// 横竖分割线的数据对象
		JecnBaseDivedingLineData lineData = (JecnBaseDivedingLineData) this
				.getFlowElementData();
		// 设置X、Y坐标 设置位置点和大小
		lineData.getDiviLineCloneable().initOriginalArributs(startPoint,
				endPoint, lineData.getBodyWidth(), lineData.getWorkflowScale());
		// 设置分隔符位置点
		reSetLocation(startPoint, endPoint);
	}

	/**
	 * 获取克隆数据对象
	 */
	public JecnBaseDividingLinePanel clone() {
		return (JecnBaseDividingLinePanel) this.currClone();
	}

	public LineDirectionEnum getDirectionEnum() {
		return directionEnum;
	}

	public boolean isShowLineResize() {
		return isShowLineResize;
	}
}
