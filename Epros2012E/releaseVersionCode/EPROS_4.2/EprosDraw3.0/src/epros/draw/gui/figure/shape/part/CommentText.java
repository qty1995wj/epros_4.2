package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.SnapToGrid;
import epros.draw.gui.line.shape.CommentLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 注释框
 * 
 * @author Administrator
 * @date： 日期：Mar 20, 2012 时间：3:06:16 PM
 */
public class CommentText extends JecnBaseFigurePanel {

	/** 注释框线条 */
	public CommentLine commentLine = null;
	/** 注释框是否执行移动位移算法 True：拖动 */
	private boolean isMove = false;
	/** 是否执行旋转 true:执行旋转 */
	private boolean isCurrAlgle = false;
	/** （首次添加 右侧元素库添加时）true:首次添加 */
	private boolean isFirstAdd = false;

	public CommentText(JecnFigureData flowElementData) {
		super(flowElementData);
		initComponents();
	}

	public void initComponents() {
		// 获取连接线数据对象
		JecnBaseDivedingLineData lineData = null;
		if (this.getFlowElementData().getLineData() == null) {
			// 获取连接线数据对象
			lineData = new JecnBaseDivedingLineData(MapElemType.CommentLine);
		} else {
			lineData = this.getFlowElementData().getLineData();
		}
		// 记录小线段数据层
		this.getFlowElementData().setLineData(lineData);

		this.commentLine = new CommentLine(lineData);
		// 首次添加
		isFirstAdd();
		// 设置图形连接线关联
		commentLine.setCommentText(this);
	}

	public JecnCommentLineData getFlowElementData() {
		return (JecnCommentLineData) flowElementData;
	}

	@Override
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		// int widthLine = this.getWidth() - 1;
		// int heightLine = this.getHeight() - 10;
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawLine(0, 0, 0, userHeight);
			g2d.drawLine(0, 0, 10, 0);
			g2d.drawLine(0, userHeight, 10, userHeight);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawLine(0, 0, 0, 10);
			g2d.drawLine(0, 0, userWidth, 0);
			g2d.drawLine(userWidth, 0, userWidth, 10);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawLine(userWidth, 0, userWidth, userHeight);
			g2d.drawLine(userWidth - 10, 0, userWidth, 0);
			g2d.drawLine(userWidth - 10, userHeight, userWidth, userHeight);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawLine(0, userHeight - 10, 0, userHeight);
			g2d.drawLine(0, userHeight, userWidth, userHeight);
			g2d.drawLine(userWidth, userHeight - 10, userWidth, userHeight);
		}
	}

	/**
	 * 注释框普通算法 不存在填充色 和未填充一样
	 * 
	 * 
	 */
	public void paintGeneralFigure(Graphics2D g2d) {
		paintNoneFigure(g2d);
	}

	public Point getLineStartPoint() {
		return new Point((int) commentLine.getStartPoint().getX(),
				(int) commentLine.getStartPoint().getY());
	}

	/**
	 * 重新父类 添加、移动组件并调整其大小。由 x 和 y 指定左上角的新位置，由 width 和 height 指定新的大小
	 * 
	 * @param int
	 *            x
	 * @param int
	 *            y
	 * @param int
	 *            width
	 * @param int
	 *            height
	 */
	public void setBounds(int x, int y, int width, int height) {
		// 设置图形位置点、大小
		super.setBounds(x, y, width, height);
		if (isCurrAlgle) {// True 执行旋转 或首次添加连接线（流程元素库添加）
			isCurrAlgle = false;
			// 执行旋转
			trunCommentText();
		} else if (isFirstAdd) {// 首次添加
			isFirstAdd = false;
			addCommonLine();
		} else if (isMove) {// 拖动
			isMove = false;
			// 图形和小线段相对位移
			int diffx = 0;
			int diffy = 0;
			// 上一次小线段结束点
			Point curEndP = commentLine.getEndPoint();
			// 线对象当前状态下结束点
			Point endPoint = reCommonLineEPoint();
			// 打开流程面板没有位移， 拖动计算位移
			diffx = endPoint.x - curEndP.x;
			diffy = endPoint.y - curEndP.y;
			Point startPoint = reCommonLineSPoint(diffx, diffy);
			// 设置线段大小 记录线段原始状态下数值
			commentLine.getFlowElementData().getDiviLineCloneable()
					.initOriginalArributs(startPoint, endPoint,
							this.getFlowElementData().getBodyWidth(),
							getCurWorkScale());
		} else {
			// 线对象当前状态下结束点
			Point endPoint = reCommonLineEPoint();
			Point startPoint = commentLine.getStartPoint();
			// 设置线段大小 记录线段原始状态下数值
			commentLine.getFlowElementData().getDiviLineCloneable()
					.initOriginalArributs(startPoint, endPoint,
							this.getFlowElementData().getBodyWidth(),
							getCurWorkScale());
		}
		if (commentLine.isShowLineResize()) {// 是否显示分割线显示点
			commentLine.showLineResizeHandles();
		}
	}

	/**
	 * 注释框旋转操作
	 * 
	 */
	private void trunCommentText() {
		// 上一次小线段结束点
		Point startPoint = commentLine.getStartPoint();
		// 线对象当前状态下结束点
		Point endPoint = reCommonLineEPoint();
		// 打开流程面板没有位移， 拖动计算位移
		// 设置线段大小 记录线段原始状态下数值
		commentLine.getFlowElementData().getDiviLineCloneable()
				.initOriginalArributs(startPoint, endPoint,
						this.getFlowElementData().getBodyWidth(),
						getCurWorkScale());
	}

	/**
	 * 重画CommonLine
	 * 
	 * @param x
	 * @param y
	 */
	public void reDrawCommonLine(int x, int y) {
		Point newPoint = new Point(x, y);
		int diffx = newPoint.x - this.getLocation().x;
		int diffy = newPoint.y - this.getLocation().y;
		if (commentLine.getStartPoint() != null) {
			double startX = commentLine.getStartPoint().getX();
			double startY = commentLine.getStartPoint().getY();
			double endX = commentLine.getEndPoint().getX();
			double endY = commentLine.getEndPoint().getY();
			Point startPoint = new Point((int) startX + diffx, (int) startY
					+ diffy);
			Point endPoint = new Point((int) endX + diffx, (int) endY + diffy);

			commentLine.getFlowElementData().getDiviLineCloneable()
					.reSetAttributes(startPoint, endPoint,
							this.getFlowElementData().getBodyWidth(),
							getCurWorkScale());
		}
	}

	/**
	 * 获取小线段结束点
	 * 
	 * @return
	 */
	private Point reCommonLineEPoint() {
		// 换算结束点位置
		Point endPoint = new Point();
		if (flowElementData.getCurrentAngle() == 0.0) {
			endPoint = new Point(this.getLocation().x, this.getLocation().y
					+ this.getHeight() / 2);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			endPoint = new Point(this.getLocation().x + this.getWidth() / 2,
					this.getLocation().y);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			endPoint = new Point(this.getLocation().x + this.getWidth(), this
					.getLocation().y
					+ this.getHeight() / 2);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			endPoint = new Point(this.getLocation().x + this.getWidth() / 2,
					this.getLocation().y + this.getHeight());
		}
		// 设置分割线结束点
		return endPoint;
	}

	/**
	 * 根据移动位移获取注释框小线段开始点坐标
	 * 
	 * @param diffx
	 *            相对X方向位移
	 * @param diffy
	 *            相对Y方向位移
	 * @return Point小线段开始点
	 */
	public Point reCommonLineSPoint(int diffx, int diffy) {
		// 开始点位置
		Point newPoint = null;
		newPoint = new Point((int) commentLine.getStartPoint().getX() + diffx,
				(int) commentLine.getStartPoint().getY() + diffy);
		return newPoint;
	}

	/**
	 * 添加commonLine
	 * 
	 * 小线段闪烁处理开始点
	 * 
	 */
	public void addCommonLine() {
		Point startPoint = null;
		Point endPoint = null;
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 线条开始点（远离图形点）
			startPoint = new Point(this.getLocation().x - this.getHeight() / 2,
					this.getLocation().y + this.getHeight());
			// 线条结束点(靠近图形点)
			endPoint = new Point(this.getLocation().x, this.getLocation().y
					+ this.getHeight() / 2);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			// 线条开始点（远离图形点）
			startPoint = new Point(this.getLocation().x,
					this.getLocation().y - 20);
			// 线条结束点(靠近图形点)
			endPoint = new Point(this.getLocation().x + this.getWidth() / 2,
					this.getLocation().y);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			// 线条开始点（远离图形点）
			startPoint = new Point(this.getLocation().x + this.getWidth() + 20,
					this.getLocation().y);
			// 线条结束点(靠近图形点)
			endPoint = new Point(this.getLocation().x + this.getWidth(), this
					.getLocation().y
					+ this.getHeight() / 2);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			startPoint = new Point(this.getLocation().x + this.getWidth(), this
					.getLocation().y
					+ this.getHeight() + 20);
			// 线条结束点(靠近图形点)
			endPoint = new Point(this.getLocation().x + this.getWidth() / 2,
					this.getLocation().y + this.getHeight());
		}

		if (startPoint == null || endPoint == null) {
			return;
		}
		// 设置线段大小 记录线段原始状态下数值
		commentLine.getFlowElementData().getDiviLineCloneable()
				.initOriginalArributs(startPoint, endPoint,
						this.getFlowElementData().getBodyWidth(),
						getCurWorkScale());
		if (commentLine.isShowLineResize()) {
			commentLine.showLineResizeHandles();
		}
	}

	/**
	 * **********************************修改图形 添加 方法 start
	 * ***********************************************************
	 */
	/**
	 * 
	 * 如果存在网格，按网格设置大小
	 * 
	 * @param figure
	 * @param x
	 * @param y
	 */
	public Point checkIsGrid(int x, int y) {
		return SnapToGrid.checkIsGrid(x, y);
	}

	public double getCurWorkScale() {
		return JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
	}

	/**
	 * 
	 * 把源图形属性赋值给目标图形属性
	 * 
	 * @param srcFigure
	 *            源属性
	 * @param destFigure
	 *            目标属性
	 */
	public void setFigureAttributes(JecnBaseFigurePanel srcFigure,
			JecnBaseFigurePanel destFigure) {
		if (srcFigure == null || destFigure == null) {
			return;
		}
		if (srcFigure.getFlowElementData().getFigureDataCloneable() == null) {
			srcFigure.getFlowElementData().setFigureDataCloneable(
					new JecnFigureDataCloneable());
		}
		// 备份克隆对象JecnFigureDataCloneable
		destFigure.getFlowElementData()
				.setFigureDataCloneable(
						srcFigure.getFlowElementData().getFigureDataCloneable()
								.clone());

		CommentText elementPanel = (CommentText) destFigure;
		// 把源图形属性赋值给目标图形属性
		elementPanel.commentLine = this.commentLine.clone();
		// 设置图形连接线关联
		elementPanel.commentLine.setCommentText(elementPanel);

		elementPanel.getFlowElementData().setLineData(
				elementPanel.commentLine.getFlowElementData());
	}

	/**
	 * 执行拖动位移算法
	 * 
	 */
	public void isMove() {
		isMove = true;
		isCurrAlgle = false;
		isFirstAdd = false;
	}

	/**
	 * 执行旋转算法
	 * 
	 */
	public void isCurrAlgle() {
		isMove = false;
		isCurrAlgle = true;
		isFirstAdd = false;
	}

	/**
	 * 
	 * 执行首次添加（右侧流程元素库添加）
	 */
	public void isFirstAdd() {
		isMove = false;
		isCurrAlgle = false;
		isFirstAdd = true;
	}

	/**
	 * 
	 * 执行默认算法（取数据层）
	 */
	public void isDefault() {
		isMove = false;
		isCurrAlgle = false;
		isFirstAdd = false;
	}

	/**
	 * 
	 * 设置给定注释框三个状态值
	 * 
	 * @param src
	 *            CommentText
	 */
	public void setBooleanFlags(CommentText src) {
		if (src == null) {
			return;
		}
		isMove = src.isMove;
		isCurrAlgle = src.isCurrAlgle;
		isFirstAdd = src.isFirstAdd;
	}
}