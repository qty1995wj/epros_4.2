package com.jecn.epros.server.bean.task.temp;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;

public class JecnTaskResubmitDesignerBean implements Serializable {

	private JecnTaskBeanNew taskBeanNew = null;

	private List<JecnTaskStateBean> taskStateBeans = null;

	public JecnTaskBeanNew getTaskBeanNew() {
		return taskBeanNew;
	}

	public void setTaskBeanNew(JecnTaskBeanNew taskBeanNew) {
		this.taskBeanNew = taskBeanNew;
	}

	public List<JecnTaskStateBean> getTaskStateBeans() {
		return taskStateBeans;
	}

	public void setTaskStateBeans(List<JecnTaskStateBean> taskStateBeans) {
		this.taskStateBeans = taskStateBeans;
	}

}
