package wordxml.util;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import sun.misc.BASE64Encoder;

public class Tools {
	private static final Log log = LogFactory.getLog(Tools.class);
	public static final String SPECIAL_CHAR_REG = "&([0-9]|[0-2]\\d|30|31);";
	public static final String NUM_REG = "&(\\d+);";
	public static final ThreadLocal<Pattern> SPECIAL_CHAR_T = new ThreadLocal<Pattern>() {
		protected Pattern initialValue() {
			return Pattern.compile(SPECIAL_CHAR_REG);
		}
	};
	public static final ThreadLocal<Pattern> NUM_T = new ThreadLocal<Pattern>() {
		protected Pattern initialValue() {
			return Pattern.compile(NUM_REG);
		}
	};

	/**
	 * 转换Color为RGB字符串
	 * 
	 * @author weidp
	 * @date 2014-9-19 下午03:13:08
	 * @param color
	 * @return
	 */
	public static String convertColorToRGBStr(Color color) {
		if (color == null) {
			return "auto";
		}
		String R = Integer.toHexString(color.getRed());
		R = R.length() < 2 ? ('0' + R) : R;
		String G = Integer.toHexString(color.getGreen());
		G = G.length() < 2 ? ('0' + G) : G;
		String B = Integer.toHexString(color.getBlue());
		B = B.length() < 2 ? ('0' + B) : B;
		return R + G + B;
	}

	/**
	 * 
	 * 判断给定参数是否为空
	 * 
	 * @param str
	 *            String
	 * @return boolean str为空返回ture，不为空返回false
	 */
	public static boolean isBlank(String str) {
		int length;
		if (str == null || (length = str.length()) == 0) {
			return true;
		}
		for (int i = 0; i < length; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 获取图片的Base64编码
	 * 
	 * @param imgFilePath
	 *            图片路径
	 * @return
	 */
	public static String getBase64Img(String filePath) {
		// 文件输入流
		FileInputStream fis = null;
		byte[] buffer = null;
		try {
			File file = new File(filePath);
			// 文件输入流
			fis = new FileInputStream(file);
			// 转换为字节流
			buffer = new byte[fis.available()];
			// 读取数据字节
			fis.read(buffer);
			// 转换图片
			if (buffer != null) {
				// base64 对象
				BASE64Encoder encoder = new BASE64Encoder();
				// 转换为base64字符串
				return encoder.encode(buffer);
			}
		} catch (FileNotFoundException e) {
			log.error("图片文件不存在！", e);
		} catch (IOException e) {
			log.error("转换图片出现异常！", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error("关闭图片输入流异常", e);
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * XML特殊字符( &、"、'、<、>)转换方法
	 * 
	 * @param text
	 *            待存储到xml文件的字符串
	 * @return String 当参数为null或空字符串时不做转换返回参数字符串，其他情况返回转换后字符串
	 */
	public static String xmlEntities(String text) {
		if (text == null || "".equals(text)) {
			return text;
		}
		return text.replaceAll("&", "&amp;").replaceAll("\"", "&quot;").replaceAll("'", "&apos;").replaceAll("<",
				"&lt;").replaceAll(">", "&gt;");
	}

	/**
	 * 获取给定文件全路径
	 * 
	 * @return String 文件全路径 和NULL
	 */
	public static String getSyncPath(String fileName) {
		// 路径
		String path = null;
		if (fileName == null || "".equals(fileName.trim())) {
			log.info("Tools类getSyncPath方法：找不到指定的文件。fileName=" + fileName);
			return path;
		}
		URL url = Tools.class.getResource(fileName);
		if (url == null) {
			log.info("Tools类getSyncPath方法：找不到指定的文件。fileName=" + fileName);
			return path;
		}
		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			log.error("Tools类getSyncPath方法：path=" + path, e);
			path = url.getPath().replaceAll("%20", " ");
		}
		// 打印文件路径
		log.info("Tools类getSyncPath方法：全路径path=" + path);
		return path;
	}

	/**
	 * 格式化输出XMl文档
	 * 
	 * @param document
	 * @return
	 */
	public static String formateXml(Document document) {
		if (document == null) {
			log.info("Tools类formateXml方法：找不到指定的文件。document is  null");
			return null;
		}
		try {
			OutputFormat format = OutputFormat.createPrettyPrint();
			StringWriter sw = new StringWriter();
			XMLWriter writer = new XMLWriter(sw, format);
			writer.write(document);
			writer.close();
			return sw.toString();
		} catch (IOException e) {
			log.error("格式化xml文档异常,方法名称：formateXml", e);
			return null;
		}
	}

	/**
	 * 去除ASCII中的控制字符9 10 13 除外
	 * 
	 * @param str
	 *            需要处理的字符串
	 * @return
	 */
	public static String handleSpecialChar(String str) {
		// 如果传过来的字符串为空或者为空格直接返回
		if (str == null || str.trim().equals("")) {
			return str;
		}
		StringBuilder buf = new StringBuilder();
		char[] charArray = str.toCharArray();
		for (char c : charArray) {
			int i = (int) c;
			buf.append("&" + i + ";");
		}
		String temp = buf.toString();

		// 将特殊字符去除
		Matcher m = SPECIAL_CHAR_T.get().matcher(temp);
		// 发现特殊字符替换为空字符
		if (m.find()) {
			temp = m.replaceAll("");
		}

		// 将数字转换为字符
		buf.setLength(0);
		Matcher mm = NUM_T.get().matcher(temp);
		while (mm.find()) {
			buf.append((char) Integer.valueOf(mm.group(1)).intValue());
		}

		return buf.toString();

	}
}
