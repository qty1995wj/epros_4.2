package com.jecn.epros.server.action.web.login.ad.fenghuo;

import java.util.ResourceBundle;

/**
 * 
 * 烽火待办配置信息
 * 
 */
public class FiberhomeTaskAfterItem {

	private static ResourceBundle fiberhomeConfig;

	static {
		fiberhomeConfig = ResourceBundle.getBundle("cfgFile/fiberhome/fiberhomeConfig");
	}

	public static String getValue(String key) {
		return fiberhomeConfig.getString(key);
	}
}
