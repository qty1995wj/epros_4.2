package com.jecn.epros.bean.external;

import io.swagger.annotations.ApiParam;

import java.io.Serializable;
import java.util.List;

public class TaskState implements Serializable {
    @ApiParam(value = "阶段唯一标识", required = false)
    private String stateMark;
    @ApiParam(value = "阶段名称", required = true)
    private String stateName;
    @ApiParam(value = "阶段名称,英文", required = false)
    private String stateNameEn;
    @ApiParam(value = "当前阶段审批人", required = true)
    private List<TaskPeople> peoples;

    public String getStateMark() {
        return stateMark;
    }

    public void setStateMark(String stateMark) {
        this.stateMark = stateMark;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public List<TaskPeople> getPeoples() {
        return peoples;
    }

    public void setPeoples(List<TaskPeople> peoples) {
        this.peoples = peoples;
    }

    public String getStateNameEn() {
        return stateNameEn;
    }

    public void setStateNameEn(String stateNameEn) {
        this.stateNameEn = stateNameEn;
    }
}
