package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程KPI
 * 
 * @author ZXH
 * 
 */
public class JecnFlowKPIPanel extends JecnAbstractPropertyBasePanel implements ActionListener {

	/** 流程KPIPanel **/
	private JecnPanel flowKpiPanel = null;
	/** 允许数据录入者提供数据 **/
	private JecnConfigCheckBox dataProviderEntryBox;

	public JecnFlowKPIPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		iniComponents();

		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 表面板
		tableScrollPane = new JecnThreeTableScrollPane(dialog);
		this.add(tableScrollPane, BorderLayout.CENTER);

		flowKpiPanel = new JecnPanel();
		flowKpiPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		dataProviderEntryBox = new JecnConfigCheckBox(JecnProperties.getValue("dataProviderEntry"));
		dataProviderEntryBox.addActionListener(this);
		dataProviderEntryBox.setOpaque(false);
		dataProviderEntryBox.setSelected(false);
	}

	private void initLayout() {
		// 是否存在浏览端，如果存在浏览端则隐藏表格
		if (JecnConstants.isPubShow()) {
//			this.add(tableScrollPane, BorderLayout.CENTER);
			this.add(flowKpiPanel, BorderLayout.SOUTH);
		}
		flowKpiPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(0, 5, 0, 0);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		flowKpiPanel.add(new JLabel(), c);
		// 允许数据录入者提供数据
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		flowKpiPanel.add(dataProviderEntryBox, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		flowKpiPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();
			if (ConfigItemPartMapMark.allowSupplierKPIData.toString().equals(mark)) {// 允许数据录入者提供数据
				dataProviderEntryBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					dataProviderEntryBox.setSelected(true);
				} else {
					dataProviderEntryBox.setSelected(false);
				}
			}
		}

		// 过滤隐藏项
		tableScrollPane.initData(tableScrollPane.getShowItem(configTypeDesgBean.getTableItemList(),
				JecnConfigContents.TYPE_SMALL_ITEM_STAND));
	}

	@Override
	public boolean check() {
		return true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == dataProviderEntryBox) {// 允许数据录入者提供数据1：选中；0不选中
				if (dataProviderEntryBox.getItemBean() == null) {
					return;
				}
				if (dataProviderEntryBox.isSelected()) {// 选中
					dataProviderEntryBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					dataProviderEntryBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
	}
}
