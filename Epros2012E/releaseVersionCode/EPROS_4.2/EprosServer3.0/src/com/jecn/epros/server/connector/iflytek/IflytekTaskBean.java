package com.jecn.epros.server.connector.iflytek;

public class IflytekTaskBean<T> {
	/** 发送系统ID,通过消息中心注册获取 */
	private String senderSystem;
	/** 授权码 */
	private String authCode;
	/** 发送用户(传OA系统) */
	private String senderUser;
	/** 接收系统ID(传notice_213b29bddedd498399c4735dc30f7f47) */
	private String receiverSystem;
	/** 接收用户(传集中待办) */
	private String receiverUser;
	/** 私信：0 通知：1短信：2 推送：3 邮件：4（传1） */
	private String msgType;
	/** 标题(传OA已办流程接口) */
	private String title;

	private T content;

	public String getSenderSystem() {
		return senderSystem;
	}

	public void setSenderSystem(String senderSystem) {
		this.senderSystem = senderSystem;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getSenderUser() {
		return senderUser;
	}

	public void setSenderUser(String senderUser) {
		this.senderUser = senderUser;
	}

	public String getReceiverSystem() {
		return receiverSystem;
	}

	public void setReceiverSystem(String receiverSystem) {
		this.receiverSystem = receiverSystem;
	}

	public String getReceiverUser() {
		return receiverUser;
	}

	public void setReceiverUser(String receiverUser) {
		this.receiverUser = receiverUser;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "IflytenTaskBean [senderSystem=" + senderSystem + ", senderUser=" + senderUser + ", receiverSystem="
				+ receiverSystem + ", receiverUser=" + receiverUser + ", authCode=" + authCode + ", title=" + title
				+ ", content=" + content + ", msgType=" + msgType + "]";
	}
}
