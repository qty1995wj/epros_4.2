package com.jecn.epros.server.email.buss;

import javax.mail.PasswordAuthentication;

/**
 * 邮件用户验证
 * 
 * @author xiaohu
 * 
 * @date： 日期：Dec 12, 2012 时间：11:13:29 AM
 */
public class SmtpAuth extends javax.mail.Authenticator {
	/** 用户名 */
	private String username;
	/** 用户密码 */
	private String password;

	public SmtpAuth(String username, String password) {
		this.username = username;
		this.password = password;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
}
