package com.jecn.epros.server.bean.popedom;

/**
 * UserRole entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class UserRole implements java.io.Serializable {

	// Fields

	private Long userRoleId;
	private Long roleId;
	private Long peopleId;

	public UserRole() {
	}

	public UserRole(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public UserRole(Long userRoleId, Long roleId, Long peopleId) {
		this.userRoleId = userRoleId;
		this.roleId = roleId;
		this.peopleId = peopleId;
	}

	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
}