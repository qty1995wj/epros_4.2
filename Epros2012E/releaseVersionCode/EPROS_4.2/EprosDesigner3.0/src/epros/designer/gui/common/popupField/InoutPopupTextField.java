package epros.designer.gui.common.popupField;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.util.ConnectionPool;

/**
 * 输入输出的文本搜索弹出框
 * 
 * @author hyl
 * 
 */
public class InoutPopupTextField extends AbsPopupTextField {

	@Override
	protected List<JecnTreeBean> searchName(String name, Long projectId) {
		List<JecnTreeBean> result = ConnectionPool.getProcessAction().fetchEntryLimit(name, 20);
		return result;
	}

}
