package com.jecn.epros.server.bean.task;
/**
 * 记录当前阶段操作人
 * 
 * @author wanglikai
 *
 */
public class JecnTaskPeopleNew implements java.io.Serializable {
	private Long id;
	/** 任务主键Id */
	private Long taskId;
	/** 临时存储审核人peopleID */
	private Long approvePid;
	/** 日志该条记录的id**/
	private Long recordId;

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Long getApprovePid() {
		return approvePid;
	}

	public void setApprovePid(Long approvePid) {
		this.approvePid = approvePid;
	}
}
