package epros.designer.gui.process.guide.explain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.guide.KPIShowDialog;
import epros.designer.gui.process.kpi.TmpKpiShowValues;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnResourceUtil;

/**
 * 流程关键测评指标板 也就是kpi
 * 
 * @author user
 * 
 */
public class JecnProcessEvaluation extends JecnFileDescriptionTable {

	private Long flowId;
	private TmpKpiShowValues tmpKpiShowValues;
	private JButton changeBut = new JButton(JecnProperties.getValue("change"));

	public JecnProcessEvaluation(int index, String name, boolean isRequest, JecnPanel contentPanel, Long flowId,
			String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		initData();
		titlePanel.add(changeBut);
		changeBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				change();
			}
		});

		super.initTable();
	}

	protected void change() {
		KPIShowDialog designerKPIShowDialog = new KPIShowDialog(flowId);
		designerKPIShowDialog.setVisible(true);
		initData();
		this.getTable().refluseTable();

	}

	private void initData() {
		try {
			tmpKpiShowValues = JecnDesignerCommon.getShowValues(flowId);
		} catch (Exception e) {

		}
	}

	@Override
	protected void dbClickMethod() {

	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		try {
			List<JecnConfigItemBean> kpiTitle = tmpKpiShowValues.getKpiTitles();
			List<List<String>> kpi = tmpKpiShowValues.getKpiRowValues();
			for (JecnConfigItemBean itemBean : kpiTitle) {
				if (JecnResourceUtil.getLocale() == Locale.ENGLISH) {// English
					title.add(itemBean.getEnName());
				} else {
					title.add(itemBean.getName());
				}
			}
			Vector<String> row = null;
			for (List<String> rowConTent : kpi) {
				row = new Vector<String>();
				for (String tdStr : rowConTent) {
					row.add(tdStr);
				}
				content.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
			new RuntimeException("JecnProcessEvaluation getTableModel is error");
		}
		return new JecnTableModel(title, content);
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] {};
	}

	@Override
	protected boolean isSelectMutil() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isUpdate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		// TODO Auto-generated method stub
		return false;
	}

}
