package com.jecn.dataimport.util;

import java.io.Serializable;
import java.util.List;

public class FlowRalatedBean implements Serializable {
	private String processId;// 流程ID
	private String processName;// 流程名称
	private List<ActivityShowBean> activityList;// 活动
	private String flowInputId;// 流程编号
	private String positionName;// 岗位名称
	private String sizeIndex;
	private String roleRes;// 角色职责
	private String roleName;// 角色名称
	private String flowActiveSize;

	public String getFlowActiveSize() {
		return flowActiveSize;
	}

	public void setFlowActiveSize(String flowActiveSize) {
		this.flowActiveSize = flowActiveSize;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public List<ActivityShowBean> getActivityList() {
		return activityList;
	}

	public void setActivityList(List<ActivityShowBean> activityList) {
		this.activityList = activityList;
	}

	public String getFlowInputId() {
		return flowInputId;
	}

	public void setFlowInputId(String flowInputId) {
		this.flowInputId = flowInputId;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getSizeIndex() {
		return sizeIndex;
	}

	public void setSizeIndex(String sizeIndex) {
		this.sizeIndex = sizeIndex;
	}

	public String getRoleRes() {
		return roleRes;
	}

	public void setRoleRes(String roleRes) {
		this.roleRes = roleRes;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
