package epros.designer.gui.task.jComponent.reSubmit;

import epros.designer.gui.task.jComponent.JecnFileTaskJDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;

/**
 * 文件任务审批
 * 
 * @author Administrator
 * @date： 日期：Nov 20, 2012 时间：9:55:00 AM
 */
public class JecnFileReSubmitTaskJDialog extends JecnFileTaskJDialog {

	public JecnFileReSubmitTaskJDialog(JecnTreeNode treeNode, JecnTree jTree, int approveType) {
		super(treeNode, jTree, approveType);
	}

}
