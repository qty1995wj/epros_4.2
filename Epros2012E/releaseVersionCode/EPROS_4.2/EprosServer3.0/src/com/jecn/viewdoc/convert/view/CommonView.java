package com.jecn.viewdoc.convert.view;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.viewdoc.bean.ViewDocContentIdCache;
import com.jecn.viewdoc.convert.ViewDocsPathManager;

/**
 * 之前的预览，本人未改逻辑（通过转换为pdf方式）
 * 
 * @author hyl
 * 
 */
public class CommonView extends AbsView {
	public static final String[] DEFAULT_ALLOW = new String[] { "doc", "docx", "ppt", "pptx", "pdf", "jpg", "png",
			"gif" };

	public CommonView(ViewInfo viewInfo) {
		super(viewInfo);
	}

	@Override
	public String getPath() {
		ViewDocContentIdCache currentCache = null;
		boolean isSuccess = false;
		String viewDocPath = null;
		try {
			if (!checkFileViewable(fileInfo.get("FileName"))) {
				return null;
			}
			currentCache = viewDocService.getCache(relatedId, type);
			if (currentCache == null) {
				currentCache = createCache();
				viewDocService.save(currentCache);
			} else {
				if (!("true".equals(isApp))
						&& isNewestCache(Long.valueOf(fileInfo.get("ContentId")), currentCache.getContentId())
						&& isExists(currentCache)) {
					currentCache.getCachePath();
				} else {
					FileUtils.deleteQuietly(new File(getAbsoluteCachePath(currentCache.getCachePath())));
					FileOpenBean fileOpenBean = getFileOpenBean();
					currentCache.setCachePath(getViewDocPath(fileOpenBean));
					currentCache.setContentId(fileOpenBean.getId());
					viewDocService.update(currentCache);
				}
			}
			isSuccess = true;

			if (isSuccess) {
				String params = assembleFileNameAndDownloadURL(currentCache.getCachePath());
				viewDocPath = ViewDocsPathManager.VIEWER_JS + params;
			} else {
				viewDocPath = ViewUtils.getDownloadFileURL(viewInfo);
			}
		} catch (Exception e) {
			log.error("获取文件：[" + fileInfo.get("FileName") + "]时，出现异常", e);
		}
		return viewDocPath;
	}

	@Override
	public boolean allowView() {
		return true;
	}

	@Override
	public Set<String> allowViewExtensions() {
		return new HashSet<String>(Arrays.asList(DEFAULT_ALLOW));
	}

}
