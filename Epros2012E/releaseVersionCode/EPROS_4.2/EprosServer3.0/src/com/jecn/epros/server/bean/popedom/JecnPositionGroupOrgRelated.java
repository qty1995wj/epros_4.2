package com.jecn.epros.server.bean.popedom;

/**
 * 岗位组与岗位关系表
 * @author Administrator
 *
 */
public class JecnPositionGroupOrgRelated implements java.io.Serializable {
	private Long id;//主键ID
	private Long groupId;// 岗位组Id
	private Long figureId;// 岗位Id
    private Long state = 0L; //状态 0：岗位组直接关联 1：基准岗位关联
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}
	
	
}
