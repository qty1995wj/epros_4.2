Ext.onReady(function() {
	Ext.QuickTips.init();// 使得tip提示可用
		Ext.useShims = true;
		Ext.MessageBox.minWidth = 240;
		if (screenHeight < 650) {
			pageSize = 10;
		}
		var viewport = new Ext.Viewport( {
			id : 'jecnEpros_id',
			layout : 'border',
			items : [ {
				border : false,
				region : 'center',
				id : 'main-panel',
				listeners : {
					'bodyresize' : function(p, width, height) {
						if (p.isVisible()) {
							if (mian_index_id != ""
									&& Ext.getCmp(mian_index_id)) {
								Ext.getCmp(mian_index_id)
										.setSize(width, height);
							}
						}
					}
				}
			} ]
		});
		loadContent();
	});

function loadContent() {
	var externalJspType = $("#externalResource").val();
	if (externalJspType === "processSys") {
		swicthProcessSys();
	} else if (externalJspType === "fileSys") {
		swicthFileSystem();
	} else if (externalJspType === "riskSys") {
		swicthRiskSystem();
	} else if (externalJspType === "reportSys") {
		swicthReportsSystem();
	} else if (externalJspType === "ruleSys") {
		swicthRuleSystem();
	} else if (externalJspType === "standardSys") {
		swicthStandardSys();
	} else if (externalJspType === "orgSys") {
		swicthOrgPosition();
	} 
}