package com.jecn.epros.server.action.web.login.ad.guangzhouCsr;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

/**
 * 广州机电配置读取类 (cfgFile/guangzhoujidian/guangZhouJiDianDomainInfo.properties)
 * 
 */
public class JecnGuangZhouJiDianAfterItem {

	private static Logger log = Logger.getLogger(JecnGuangZhouJiDianAfterItem.class);

	// domain名
	public static String DOMAIN_INFO = null;
	// domainIp
	public static String DOMAIN_IP = null;
	// 域后缀
	public static String DOMAIN = null;
	// 是否验证密码 (默认false)
	public static boolean VERIFY = false;

	/**
	 * 读取广机配置文件
	 * 
	 */
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH
				+ "cfgFile/guangzhoujidian/guangZhouJiDianDomainInfo.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			// 域名
			String domainInfo = props.getProperty("domainInfo");
			if (!JecnCommon.isNullOrEmtryTrim(domainInfo)) {
				JecnGuangZhouJiDianAfterItem.DOMAIN_INFO = domainInfo;
			}

			// AD服务器地址
			String domainIp = props.getProperty("domainIp");
			if (!JecnCommon.isNullOrEmtryTrim(domainIp)) {
				JecnGuangZhouJiDianAfterItem.DOMAIN_IP = domainIp;
			}

			// 域后缀
			String domain = props.getProperty("domain");
			if (!JecnCommon.isNullOrEmtryTrim(domainIp)) {
				JecnGuangZhouJiDianAfterItem.DOMAIN = domain;
			}

			// 是否验证密码
			String verify = props.getProperty("verify");
			if (!JecnCommon.isNullOrEmtryTrim(verify)) {
				JecnGuangZhouJiDianAfterItem.VERIFY = Boolean.valueOf(verify);
			}

		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}
}
