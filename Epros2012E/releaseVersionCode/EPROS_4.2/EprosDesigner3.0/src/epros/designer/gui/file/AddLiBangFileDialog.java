package epros.designer.gui.file;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;

import epros.designer.gui.autoNum.AutoNumSelectDialog;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class AddLiBangFileDialog extends AddFileDialog {
	/** 编号选择按钮 */
	protected JButton selectCodeBut;
	protected JTextField viewText;
	protected JCheckBox jBox;

	protected JCheckBox sameNumBox;

	public AddLiBangFileDialog(JecnTreeNode node, JecnTree jTree) {
		super(node, jTree);
		initData();
	}

	@Override
	protected void initChildComponents() {
		selectCodeBut = new JButton(JecnProperties.getValue("selectNum"));
		viewText = new JTextField();
		jBox = new JCheckBox("是否自动编号", true);
		sameNumBox = new JCheckBox("编号是否相同", false);
		initChildListener();
	}

	public void customPanel(int rows, JecnPanel mainPanel, Insets insets) {
		JecnPanel filePanel = new JecnPanel();
		// 自动编号预览
		GridBagConstraints c = new GridBagConstraints(0, rows, 4, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(filePanel, c);
		initFilePanel(filePanel, insets);
	}

	private void initData() {
		try {
			// 制度目录-编号规则获取
			AutoCodeBean codeBean = ConnectionPool.getAutoCodeAction().getAutoCodeBean(pNode.getJecnTreeBean().getId(),
					4);
			if (codeBean == null) {
				return;
			}
			showViewCode(codeBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initFilePanel(JecnPanel filePanel, Insets insets) {
		filePanel.setLayout(new GridBagLayout());

		TitledBorder border = BorderFactory.createTitledBorder("请先选择编号，再上传文件！");
		border.setTitleColor(Color.red);
		border.setTitleFont(new Font("宋体", Font.PLAIN, 14));
		filePanel.setBorder(border);

		// 编号
		GridBagConstraints c = new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		filePanel.add(viewText, c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(selectCodeBut, c);

		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(new CommonJlabelMustWrite(), c);

		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(jBox, c);

		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(sameNumBox, c);
	}

	private void initChildListener() {
		selectCodeBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AutoNumSelectDialog selectDialog = new AutoNumSelectDialog(pNode, AddLiBangFileDialog.this);
				selectDialog.setVisible(true);
				if (StringUtils.isNotBlank(selectDialog.getCodeBean().getResultCode())) {
					showViewCode(selectDialog.getCodeBean());
				}
			}
		});

		sameNumBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				sameNumBoxAction();

			}
		});
	}

	protected void sameNumBoxAction() {
		int rows = addFileTable.getRowCount();
		if (rows == 0) {
			return;
		}

		if (sameNumBox.isSelected()) {
			for (int i = 0; i < rows; i++) {
				addFileTable.getModel().setValueAt(viewText.getText(), i, 0);
			}
		} else {
			String code = viewText.getText();
			int codeTotal = Integer.valueOf(code.substring(code.lastIndexOf("-")+1, code.length()));
			for (int i = 0; i < rows; i++) {
				addFileTable.getModel().setValueAt(resultCode + "-" + getStringCodeByIndex(codeTotal), i, 0);
				codeTotal++;
			}
		}
	}

	String resultCode = "";

	/**
	 * 编号预览
	 * 
	 * @param codeBean
	 */
	private void showViewCode(AutoCodeBean codeBean) {
		resultCode = codeBean.getResultCode().substring(0, codeBean.getResultCode().lastIndexOf("-"));
		codeTotal = Integer.valueOf(codeBean.getResultCode().substring(codeBean.getResultCode().lastIndexOf("-") + 1,
				codeBean.getResultCode().length()));
		viewText.setText(resultCode + "-" + getStringCodeByIndex(codeTotal));
	}

	/**
	 * 添加文件编号
	 */
	protected void addFileCode(Vector<Object> v) {
		if (jBox.isSelected()) {
			String code = null;
			if (sameNumBox.isSelected()) {
				code = viewText.getText();
			} else {
				code = resultCode + "-" + getStringCodeByIndex(codeTotal);
				codeTotal++;
			}
			v.add(code);
		} else {
			v.add("");
		}

	}

	/**
	 * 上传文件，先验证编号是否存在
	 */
	protected boolean isUploadFileValidate() {
		if (jBox.isSelected()) {
			if (StringUtils.isBlank(viewText.getText())) {
				JecnOptionPane.showMessageDialog(null, "请先选择编号，在上传文件！");
				return true;
			}
		}
		return false;
	}

	protected void upload() throws Exception {
		if (sameNumBox.isSelected()) {
			String code = null;
			code = viewText.getText();
			codeTotal = Integer.valueOf(code.substring(code.lastIndexOf("-") + 1, code.length()));
		}
		super.upload();
	}

	private String getStringCodeByIndex(int index) {
		if (index < 10) {
			return "00" + index;
		} else if (index < 100) {
			return "0" + index;
		} else {
			return "" + index;
		}
	}
}
