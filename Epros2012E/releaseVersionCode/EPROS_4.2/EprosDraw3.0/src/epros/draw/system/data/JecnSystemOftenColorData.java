package epros.draw.system.data;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 存放常用颜色的类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSystemOftenColorData {
	/** 填充常用颜色List */
	private List<Color> fillColorList = new ArrayList<Color>();
	/** 线条常用颜色List */
	private List<Color> lineColorList = new ArrayList<Color>();
	/** 字体常用颜色List */
	private List<Color> fontColorList = new ArrayList<Color>();
	/** 阴影常用颜色List */
	private List<Color> shadowColorList = new ArrayList<Color>();

	/** 是否需要更新到本地标识 */
	private boolean updateFlag = false;

	public boolean isUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(boolean updateFlag) {
		this.updateFlag = updateFlag;
	}

	public List<Color> getFillColorList() {
		return fillColorList;
	}

	public void setFillColorList(List<Color> fillColorList) {
		this.fillColorList = fillColorList;
	}

	public List<Color> getLineColorList() {
		return lineColorList;
	}

	public void setLineColorList(List<Color> lineColorList) {
		this.lineColorList = lineColorList;
	}

	public List<Color> getFontColorList() {
		return fontColorList;
	}

	public void setFontColorList(List<Color> fontColorList) {
		this.fontColorList = fontColorList;
	}

	public List<Color> getShadowColorList() {
		return shadowColorList;
	}

	public void setShadowColorList(List<Color> shadowColorList) {
		this.shadowColorList = shadowColorList;
	}

}
