package com.jecn.epros.server.download.wordxml.jiangbolong;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.StyledEditorKit.FontSizeAction;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

/**
 * 江波龙操作说明下载
 * 
 * @author admin
 * 
 */
public class JBLProcessFileMode extends ProcessFileModel {
	/****** 段落行距固定值20磅 *********/
	private ParagraphLineRule lineRuleFixt = new ParagraphLineRule(20, LineRuleType.EXACT);
	/****** 单倍行距 *********/
	private ParagraphLineRule lineRuleAuto = new ParagraphLineRule(1, LineRuleType.AUTO);
	/** 评审人集合 0 评审人类型 1名称 2日期3评审阶段标识 */
	List<Object[]> peopleList = processDownloadBean.getPeopleList();
	/** 评审人集合 NEW 0 阶段标识 1评审人类型 2评审人名称 3部门名称 4更新时间 5 排序 6用户ID */
	List<Object[]> peopleListNew = processDownloadBean.getPeopleListNew();

	public JBLProcessFileMode(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(17.49F);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle("、", textTitleStyle());
	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0.01F, 0.01F, new ParagraphLineRule(18F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 添加首页内容
	 * 
	 * @param sect
	 */
	private void addTitleSectContent(Sect sect) {
		
		// 表格统一宽度
		float _newTblWidth = 17.21F;
		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		/**** 表格默认段落样式 宋体小居中 单倍行距自动 ************/
		ParagraphBean defaultTabPBean = new ParagraphBean("宋体", Constants.wuhao, false);
		defaultTabPBean.setSpace(0f, 0f, lineRuleAuto);

		String fileName = "文件名称：" + processDownloadBean.getFlowName();
		String fileNumber = "文件编号：" + processDownloadBean.getFlowInputNum();
		String version = "版本号：" + processDownloadBean.getFlowVersion();
		String pubOrg = "发文部门：" + processDownloadBean.getOrgName();
		String pubDate = "发文日期："
				+ processDownloadBean.getReleaseDate().replaceAll("\\w+\\d+\\:\\d+.+", "").replaceAll("\\-", "/");
		// 密集
		String isPub = getPubOrSec();
		String afterReadArchive = "阅后存档：" + isPub;
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { fileName, fileNumber, version });
		rowData.add(new String[] { pubOrg, pubDate, afterReadArchive });

		float[] width = new float[] { 5.73F, 5.13F, 3.75F };
		sect.createParagraph("");
		JecnWordUtil.resizeTblWidth(width, _newTblWidth);
		
		// 表格
		Table tab = JecnWordUtil.createTab(sect, tabBean, width, rowData, defaultTabPBean);
		setTblValign(tab);

		/** 部门权限表格 **/
		TableBean tabBeanDept = new TableBean();
		tabBeanDept.setBorder(0.5F);
		tabBeanDept.setTableAlign(Align.center);
		tabBeanDept.setBorderInsideV(null);
		tabBeanDept.setBorderInsideH(null);
		tabBeanDept.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		rowData = new ArrayList<String[]>();

		if ("1".equals(processDownloadBean.getFlowIsPublic())) {
			rowData.add(new String[] { "收文：", "江波龙公司全体员工","","","" });
		} else {
			// 部门查阅权限部门为4列
			List<JecnFlowOrg> permOrgs = processDownloadBean.getPermOrgs();
			int permOrgNum = permOrgs.size();
			int lastNum = permOrgNum % 4;
			String first = "";
			if (lastNum == 0) {
				if (permOrgNum == 0) {
					first = "收文：";
					rowData.add(new String[] { first, "", "", "", "" });
				} else {
					// 写出整数行
					for (int i = 0; i < permOrgNum; i = i + 4) {
						if (i == 0) {
							first = "收文：";
						} else {
							first = "";
						}
						rowData.add(new String[] { first,
								permOrgs.get(i).getOrgName(),
								permOrgs.get(i + 1).getOrgName(),
								permOrgs.get(i + 2).getOrgName(),
								permOrgs.get(i + 3).getOrgName() });
					}
				}
			} else {
				int loop = permOrgNum - lastNum;
				// 写出整数行
				for (int i = 0; i < loop; i = i + 4) {
					if (i == 0) {
						first = "收文：";
					} else {
						first = "";
					}
					rowData.add(new String[] { first,
							permOrgs.get(i).getOrgName(),
							permOrgs.get(i + 1).getOrgName(),
							permOrgs.get(i + 2).getOrgName(),
							permOrgs.get(i + 3).getOrgName() });
				}
				// 写出最后一行
				List<String> orgNames = new ArrayList<String>();
				if (loop <= 0) {
					first = "收文：";
				}else{
					first="";
				}
				orgNames.add(first);
				for (int i = permOrgNum - lastNum; i < permOrgNum; i++) {
					orgNames.add(permOrgs.get(i).getOrgName());
				}
				String[] array = orgNames.toArray(new String[5]);
				rowData.add(array);

			}
			int needRowSpan = lastNum == 0 ? permOrgNum / 4
					: permOrgNum / 4 + 1;
			if (permOrgNum > 0) {
				tab.getRow(0).getCell(0).setRowspan(needRowSpan);
			}
		}

		width = new float[] { 1.37F, 3.5F, 3.5F, 3.5F, 3.5F };
		JecnWordUtil.resizeTblWidth(width, _newTblWidth);
		tab = JecnWordUtil.createTab(sect, tabBeanDept, width, rowData, defaultTabPBean);
	
		

		/** 拟制表格 **/
		rowData = new ArrayList<String[]>();
		// 拟制 ==》流程属性—流程拟制人
		String nizhi = processDownloadBean.getResponFlow().getFictionPeopleName();
		// 审核==>部门审批人 ==部门审核阶段 2
		String shenhe = "";
		// 批准==>批准审批人==》流程批准阶段4
		String pizhun = "";
		JecnTaskHistoryNew latestHistoryNew = processDownloadBean.getLatestHistoryNew();
		if(latestHistoryNew!=null){
			List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow = latestHistoryNew.getListJecnTaskHistoryFollow();
			for(JecnTaskHistoryFollow follow:listJecnTaskHistoryFollow){
				//任务审批阶段标识 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 6：各业务体系审批阶段 7：IT总监审批阶段
				if(follow.getStageMark()==2){
					shenhe=follow.getName();
				}else if(follow.getStageMark()==4){
					pizhun =follow.getName();
				}
			}
			
		}
		
		/** 评审人集合 0 评审人类型 1名称 2日期3评审阶段标识 */
		if (peopleList != null && peopleListNew == null) {
			for (Object[] obj : peopleList) {
				if ("2".equals(obj[3].toString())) {
					shenhe = obj[1].toString();
				}
				if ("4".equals(obj[3].toString())) {
					pizhun = obj[1].toString();
				}
			}
		}
		/** 评审人集合 NEW 0 阶段标识 1评审人类型 2评审人名称 3部门名称 4更新时间 5 排序 6用户ID */
		if (peopleListNew != null && peopleList == null) {
			for (Object[] obj : peopleListNew) {
				if ("2".equals(obj[0].toString())) {
					shenhe = obj[2].toString();
				}
				if ("4".equals(obj[0].toString())) {
					pizhun = obj[2].toString();
				}
			}
		}
		rowData.add(new String[] { "拟制", nizhi, "审核", shenhe, "批准", pizhun });
		width = new float[] { 1.3F, 4.44F, 1.3F, 4.44F, 1.3F, 4.43F };
		JecnWordUtil.resizeTblWidth(width, _newTblWidth);
		tab = JecnWordUtil.createTab(sect, tabBean, width, rowData, defaultTabPBean);
		setTblValign(tab);
		sect.createParagraph("");
		// /////////////////////////////////////////////////////////////////////////////////
		/** 发布版本表格 **/
		ParagraphBean defaultTabPBean_B = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		defaultTabPBean_B.setSpace(0f, 0f, lineRuleAuto);
		width = new float[] { 0.59F, 2.3F, 0.74F, 0.74F, 0.74F, 8F, 1.7F, 1.8F };
		JecnWordUtil.resizeTblWidth(width, _newTblWidth);
		rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "序号", "修订日期", "修订类别", "修订内容说明", "版本", "修订者" });
		rowData.add(new String[] { "", "", "制定", "修改", "废止", "", "", "" });
		int rowNumber = 1;
		List<Object[]> documentList = processDownloadBean.getDocumentList();
		if (documentList != null && documentList.size() != 0) {
			for (int i = documentList.size() - 1; i >= 0; i--) {
				Object[] obj = documentList.get(i);
				String xuhao = rowNumber + "";
				String reviseData = String.valueOf(obj[2]).replaceAll("\\w+\\d+\\:\\d+.+", "").replaceAll("\\-", "/");
				String countent = obj[1] + "";
				String versionStr = obj[0] + "";
				String people = obj[3] + "";
				String zhiding = "";
				String xiugai = "";
				String feizhi = "";
				if (i == documentList.size() - 1) {
					zhiding = "Y";
				} else {
					xiugai = "Y";
				}
				rowData.add(new String[] { xuhao, reviseData, zhiding, xiugai, feizhi, countent, versionStr, people });
				rowNumber++;
			}
		}
		tab = JecnWordUtil.createTab(sect, tabBean, width, rowData, defaultTabPBean);
		setTblValign(tab);
		// 设置表格加粗
		for (int i = 0; i < 2; i++) {
			TableRow row = tab.getRow(i);
			for (TableCell cell : row.getCells()) {
				cell.getParagraph(0).setParagraphBean(defaultTabPBean_B);
			}
		}
		tab.getRow(0).getCell(2).setColspan(3);
		// 注意：合并了单元格 之后，并且是第一行，需要重新设置第一行的单元格宽度
		tab.getRow(0).getCell(0).setWidth(0.8F);
		tab.getRow(0).getCell(1).setWidth(2.25F);
		tab.getRow(0).getCell(2).setWidth(2.25F);
		tab.getRow(0).getCell(3).setWidth(8.2F);
		tab.getRow(0).getCell(4).setWidth(1.17F);
		tab.getRow(0).getCell(5).setWidth(1.86F);

		tab.getRow(0).getCell(0).setRowspan(2);
		tab.getRow(0).getCell(1).setRowspan(2);
		// 这里设置跨行开始和结束标记
		tab.getRow(0).getCell(3).setVmergeBeg(true);
		tab.getRow(1).getCell(5).setVmerge(true);

		tab.getRow(0).getCell(4).setVmergeBeg(true);
		tab.getRow(1).getCell(6).setVmerge(true);
		tab.getRow(0).getCell(5).setVmergeBeg(true);
		tab.getRow(1).getCell(7).setVmerge(true);

		
		List<TableRow> rows = tab.getRows();
		List<TableCell> cells=null;
		for(int num=2;num<rows.size();num++){
			cells = rows.get(num).getCells();
			if(cells.size()<4){
				continue;
			}
			for(int c=2;c<5;c++){
				cells.get(c).getParagraph(0).setParagraphBean(new ParagraphBean(Align.center,"宋体", Constants.wuhao, false));
			    
			}
		}
		
		//添加分页符
		Paragraph lastParagraph = sect.createParagraph("");
		lastParagraph.setWarp(true);
	}

	/**
	 * 设置表格垂直居中
	 * 
	 * @param tab
	 */
	private void setTblValign(Table tab) {
		for (TableRow row : tab.getRows()) {
			row.setHeight(0.98F);
			for (TableCell cell : row.getCells()) {
				cell.setvAlign(Valign.center);
			}
		}
	}

	@Override
	protected void initTitleSect(Sect sect) {
//		SectBean sectBean = getSectBean();
//		sectBean.setStartNum(1);
//		sect.setSectBean(sectBean);
//		addTitleSectContent(sect);
//		createHdrAndFtr(sect, false);
	}

	@Override
	protected void initFirstSect(Sect sect) {
		SectBean sectBean = getSectBean();
		sect.setSectBean(sectBean);
		addTitleSectContent(sect);
		createHdrAndFtr(sect, false);
	}

	@Override
	protected void initFlowChartSect(Sect sect) {
		SectBean sectBean = getSectBean();
		sectBean.setHorizontal(true);
		sectBean.setSize(29.7F, 21);
		sect.setSectBean(sectBean);
		createHdrAndFtr(sect, true);
	}

	@Override
	protected void initSecondSect(Sect sect) {
		SectBean sectBean = getSectBean();
		sect.setSectBean(sectBean);
		createHdrAndFtr(sect, false);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createHdrAndFtr(Sect sect, boolean isChart) {
		createHdr(sect);
		createFtr(sect, isChart);
	}

	private void createHdr(Sect sect) {
		// logo图标
		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(9.28F, 1.76F);
		Paragraph p = sect.createHeader(HeaderOrFooterType.odd).createParagraph("", new ParagraphBean(PStyle.AF1));
		p.appendGraphRun(logo);
	}

	private void createFtr(Sect sect, boolean isChart) {
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.xiaowu, false);
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 表格左缩进0.34 厘米
		tblStyle.setTableAlign(Align.center);
		// 表格内边距 厘米
		tblStyle.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		float[] cellWidths = { 15F, 1.5F };
		if (isChart) {
			cellWidths = new float[] { 24F, 1.5F };
		}
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "说明（受控文件）：*未经本公司同意，严禁以任何形式复制", "" });
		Table tab = new Table(tblStyle, cellWidths);
		tab.createTableRow(rowData, pBean);
		Paragraph p = tab.getRows().get(0).getCell(1).getParagraph(0);
		FontBean fontCalibri = new FontBean("Calibri", Constants.xiaowu, false);
		p.appendCurPageRun();
		p.appendTextRun("/", fontCalibri);
		p.appendTotalPagesRun();
		sect.createFooter(HeaderOrFooterType.odd).addTable(tab);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(new FontBean("宋体", Constants.wuhao, false));
		tblTitleStyle.setSpace(0F, 0F, lineRuleAuto); // 设置段落行间距
		return tblTitleStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 1 磅
		tblStyle.setBorder(1F);
		// 表格左缩进0.34 厘米
		tblStyle.setTableLeftInd(0.34F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.2F, 0, 0.2F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(Align.center, new FontBean("宋体", Constants.wuhao, true));
		tblTitleStyle.setSpace(0F, 0F, lineRuleAuto); // 设置段落行间距
		return tblTitleStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(new FontBean("宋体", Constants.xiaosi, false));
		textContentStyle.setInd(0.52F, 0, 0, 0); // 段落缩进 厘米
		textContentStyle.setSpace(0.5F, 0.2F, lineRuleFixt); // 设置段落行间距
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(new FontBean("宋体", Constants.xiaosi, true));
		textTitleStyle.setInd(0, 0, 0.95F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(0.5F, 0.2F, lineRuleFixt); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
