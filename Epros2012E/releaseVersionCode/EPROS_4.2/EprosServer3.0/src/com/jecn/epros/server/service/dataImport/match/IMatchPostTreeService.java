package com.jecn.epros.server.service.dataImport.match;

import java.util.List;

import com.jecn.epros.server.service.dataImport.match.constant.TmpMatchTreePostBean;
import com.jecn.epros.server.webBean.WebTreeBean;

public interface IMatchPostTreeService {
	/**
	 * 获取当前节点下子节点
	 * 
	 * @param strNumber
	 *            编号
	 * @param projectId
	 *            项目ID
	 * @param isLeaf
	 *            true 含子节点
	 * @return List<WebTreeBean>
	 * @throws Exception
	 */
	List<WebTreeBean> getChildMatchOrgsAndPositon(String strNumber,
			Long projectId) throws Exception;

	/**
	 * 
	 * 根据岗位名称模糊查询
	 * 
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	List<TmpMatchTreePostBean> searchPositionByName(String name, Long projectId)
			throws Exception;

	List<WebTreeBean> getChildMatchBasePos() throws Exception;

	/**
	 * 
	 * 根据基准岗位名称模糊查询
	 * 
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	List<TmpMatchTreePostBean> searchBaseNameByName(String baseName)
			throws Exception;

}
