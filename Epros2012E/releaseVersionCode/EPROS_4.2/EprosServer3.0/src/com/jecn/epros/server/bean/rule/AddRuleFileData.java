package com.jecn.epros.server.bean.rule;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.popedom.AccessId;

public class AddRuleFileData implements Serializable {
	private List<RuleT> list;
	private String posIds;
	private String orgIds;
	private String posGroupIds;
	private Long updatePersonId;
	private int totalCode;
	
	private AccessId accIds;

	private List<RuleStandardizedFileT> standardizedFileTs;

	public List<RuleStandardizedFileT> getStandardizedFileTs() {
		return standardizedFileTs;
	}

	public void setStandardizedFileTs(List<RuleStandardizedFileT> standardizedFileTs) {
		this.standardizedFileTs = standardizedFileTs;
	}

	public List<RuleT> getList() {
		return list;
	}

	public void setList(List<RuleT> list) {
		this.list = list;
	}

	
	public AccessId getAccIds() {
		return accIds;
	}

	public void setAccIds(AccessId accIds) {
		this.accIds = accIds;
	}

	public String getPosIds() {
		return posIds;
	}

	public void setPosIds(String posIds) {
		this.posIds = posIds;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public String getPosGroupIds() {
		return posGroupIds;
	}

	public void setPosGroupIds(String posGroupIds) {
		this.posGroupIds = posGroupIds;
	}

	public Long getUpdatePersonId() {
		return updatePersonId;
	}

	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}

	public int getTotalCode() {
		return totalCode;
	}

	public void setTotalCode(int totalCode) {
		this.totalCode = totalCode;
	}

}
