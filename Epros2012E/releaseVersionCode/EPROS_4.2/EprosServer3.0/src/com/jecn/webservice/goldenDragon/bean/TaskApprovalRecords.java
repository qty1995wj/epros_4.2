package com.jecn.webservice.goldenDragon.bean;

/**
 * 任务审批记录
 * 
 * @author admin
 * 
 */
public class TaskApprovalRecords {
	private String appellation;
	private String name;
	private String approvalTime;
	/** 3:会签 */
	private String stage;

	public String getAppellation() {
		return appellation;
	}

	public void setAppellation(String appellation) {
		this.appellation = appellation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApprovalTime() {
		return approvalTime;
	}

	public void setApprovalTime(String approvalTime) {
		this.approvalTime = approvalTime;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}
}
