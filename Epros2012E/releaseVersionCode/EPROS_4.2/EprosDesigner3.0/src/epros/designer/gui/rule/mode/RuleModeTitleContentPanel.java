package epros.designer.gui.rule.mode;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class RuleModeTitleContentPanel extends JPanel {

	private JTextArea  textArea=null;
	public RuleModeTitleContentPanel(String title){

		this.setLayout(new BorderLayout());
//		this.setBorder(BorderFactory.createTitledBorder(
//				BorderFactory.createEmptyBorder(1, 1, 1, 1), title,
//				TitledBorder.DEFAULT_JUSTIFICATION,
//				TitledBorder.DEFAULT_POSITION,
//				new java.awt.Font(JecnProperties.getValue("songTi"), 1, 12), Color.BLACK));
		this.textArea=new JTextArea();
		this.textArea.setBorder(BorderFactory.createLineBorder(Color.gray));
		this.textArea.setRows(5);
		this.textArea.setEditable(false);
		this.add(this.textArea,BorderLayout.CENTER);
	}
	

}
