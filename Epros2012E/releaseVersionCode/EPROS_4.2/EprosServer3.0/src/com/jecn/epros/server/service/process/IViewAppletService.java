package com.jecn.epros.server.service.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.svg.dataModel.SvgPanel;

/**
 * 
 * 浏览器applet对应的处理层接口
 * 
 * @author ZHOUXY
 * 
 */
public interface IViewAppletService extends
		IBaseService<JecnFlowStructureT, Long> {

	/**
	 * 
	 * 查询流程地图数据
	 * 
	 * @param processId
	 *            流程地图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	public List<List<Object[]>> getTotalMap(Long processId, Long peopleId,
			Long projectId) throws Exception;

	/**
	 * 
	 * 查询流程图数据
	 * 
	 * @param processId
	 *            流程图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	public List<List<Object[]>> getPartMap(Long processId, Long peopleId,
			Long projectId) throws Exception;

	/**
	 * 
	 * 查询流程地图数据
	 * 
	 * @param processId
	 *            流程地图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	public List<List<Object[]>> getTotalMapT(Long processId, Long peopleId,
			Long projectId) throws Exception;

	/**
	 * 
	 * 查询流程图数据
	 * 
	 * @param processId
	 *            流程图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	public List<List<Object[]>> getPartMapT(Long processId, Long peopleId,
			Long projectId) throws Exception;
	
	public SvgPanel getSvgBeanT(Long processId, Long peopleId, Long projectId,Long historyId, int languageType)
			throws Exception;

	/**
	 * 
	 * 查询组织图数据
	 * 
	 * @param processId
	 *            组织图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	public List<List<Object[]>> getOrgMap(Long processId, Long peopleId,
			Long projectId) throws Exception;

	/**
	 * 根据岗位组Id获取岗位信息
	 * 
	 * @author weidp
	 * @date： 日期：2014-4-23 时间：上午10:21:51
	 * @param posGroupId
	 * @return
	 */
	public List<Object[]> getPositionsByPosGroupId(long posGroupId)
			throws Exception;

	SvgPanel getSvgBean(Long processId, Long peopleId, Long projectId, int languageType) throws Exception;

	SvgPanel getSvgMapBean(Long processId, Long peopleId, Long projectId, int languageType) throws Exception;

	SvgPanel getSvgMapBeanT(Long processId, Long peopleId, Long projectId,Long historyId, int languageType) throws Exception;

	public SvgPanel getSvgOrg(Long flowId, Long peopleId, long projectId, int languageType);

	SvgPanel getSvgIntegrationChartBeanT(Long processId, Long peopleId, Long projectId,Long historyId, int languageType) throws Exception;

	SvgPanel getSvgIntegrationChartBean(Long processId, Long peopleId, Long projectId, int languageType) throws Exception;

}
