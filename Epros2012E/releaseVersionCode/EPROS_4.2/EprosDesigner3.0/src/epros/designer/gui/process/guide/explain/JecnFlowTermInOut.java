package epros.designer.gui.process.guide.explain;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.inout.JecnFlowInoutBase;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigTool;

import epros.designer.gui.process.guide.flowTermInoutDialog.FlowTermInoutDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnResourceUtil;

/**
 * 新版输入输出
 * 
 * @author zhr
 * 
 */
public class JecnFlowTermInOut extends JecnFileDescriptionTable {
	private Logger log = Logger.getLogger(this.getClass());
	private List<JecnFlowInoutData> flowInoutTs = new ArrayList<JecnFlowInoutData>();

	private List<JecnFlowInoutData> oldInoutTs = new ArrayList<JecnFlowInoutData>();

	// 文件选择按钮初始化
	protected JButton selectBut = new JButton(JecnProperties.getValue("add"));

	protected JButton editBut = new JButton(JecnProperties.getValue("edit"));

	protected JButton deleteBut = new JButton(JecnProperties.getValue("delete"));

	/** 上移按钮 */
	private JButton upButton = null;
	/** 下移按钮 */
	private JButton downButton = null;

	private int type;

	private Long flowId;

	public JecnFlowTermInOut(int index, String name, boolean isRequest, JecnPanel contentPanel, Long flowId, int type,
			String tip) {
		super(index, name, isRequest, contentPanel, tip);
		titlePanel.add(selectBut);
		titlePanel.add(editBut);
		titlePanel.add(deleteBut);
		this.flowId = flowId;
		this.type = type;
		initData();
		addComponent();
		initEvent();
	}

	private void addComponent() {
		initTable();
		insertSort();
		this.upButton = new JButton(JecnProperties.getValue("upBtn"));
		this.downButton = new JButton(JecnProperties.getValue("downBtn"));

		Insets insets = new Insets(0, 1, 0, 1);
		GridBagConstraints c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(getSortPanel(), c);
		this.getTable().setDefaultRenderer(Object.class, new TableViewRenderer());// 红色标记部分是用来渲染JTable的自定义绘制器

		/*
		 * table.getColumnModel().getColumn(3).setCellEditor(new MyRender());
		 * table.getColumnModel().getColumn(3).setCellRenderer(newMyRender());
		 */

		/*
		 * table.getColumnModel().getColumn(3).setMaxWidth(70);
		 * table.getColumnModel().getColumn(3).setMinWidth(70);
		 */
		/*
		 * table.getColumnModel().getColumn(0).setMaxWidth(100);
		 * table.getColumnModel().getColumn(0).setMinWidth(100);
		 */

	}

	private JecnPanel getSortPanel() {
		JecnPanel jan = new JecnPanel();
		Dimension dimension = null;
		if (JecnResourceUtil.getLocale() == Locale.CHINESE) {
			dimension = new Dimension(60, 70);
		} else {
			dimension = new Dimension(85, 70);
		}
		jan.setPreferredSize(dimension);
		jan.setMinimumSize(dimension);
		jan.setLayout(new FlowLayout(FlowLayout.CENTER));
		jan.add(upButton);
		jan.add(downButton);
		return jan;
	}

	private void initEvent() {
		selectBut.addActionListener(new ActionListener() { // 新建
					@Override
					public void actionPerformed(ActionEvent e) {
						JecnFlowInoutData createResourceData = createResourceData();
						List<String> names = new ArrayList<String>();
						for (int i = 0; i < flowInoutTs.size(); i++) {
							names.add(flowInoutTs.get(i).getInout().getName()); // 获取名称验证重名
						}
						FlowTermInoutDialog dialog = new FlowTermInoutDialog(createResourceData, names, getIsInout(),
								flowId);
						dialog.setVisible(true);
						if (dialog.isOk() && dialog.isOpaque()) {
							flowInoutTs.add(createResourceData);
							refreshTable();
						}
					}
				});
		deleteBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				if (selectedRow != -1) {
					JecnFlowTermInOut.this.flowInoutTs.remove(selectedRow);
					JecnFlowTermInOut.this.refreshTable();
				}
			}
		});
		editBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				if (selectedRow != -1) {
					List<String> names = new ArrayList<String>();
					for (int i = 0; i < flowInoutTs.size(); i++) {
						if (i != selectedRow) {
							names.add(flowInoutTs.get(i).getInout().getName()); // 获取名称验证重名
						}
					}
					FlowTermInoutDialog dialog = new FlowTermInoutDialog(flowInoutTs.get(selectedRow), names,
							getIsInout(), flowId);
					dialog.setVisible(true);
					if (dialog.isOk() && dialog.isOpaque()) {
						refreshTable();
					}
				}
			}
		});
		// 上移
		upButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				upButtonAction();
			}
		});
		// 下移
		downButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downButtonAction();
			}
		});

	}

	// 上移
	public void upButtonAction() {
		int selectedRow = table.getSelectedRow();
		if (selectedRow > 0) {
			Collections.swap(flowInoutTs, selectedRow, selectedRow - 1);
			this.getTable().moveUpRows();
			insertSort();
		}

	}

	// 下移
	public void downButtonAction() {
		int selectedRow = table.getSelectedRow();
		if (selectedRow >= 0 && (selectedRow + 1 < flowInoutTs.size())) {
			Collections.swap(flowInoutTs, selectedRow, selectedRow + 1);
			this.getTable().moveDownRows();
			insertSort();
		}
	}

	// 新建初始数据
	private JecnFlowInoutData createResourceData() {
		JecnFlowInoutData data = new JecnFlowInoutData();
		JecnFlowInoutBase bean = new JecnFlowInoutBase();
		data.setInout(bean);
		data.setPoses(new ArrayList<JecnTreeBean>());
		data.setPosGroups(new ArrayList<JecnTreeBean>());
		return data;
	}

	private void initData() {
		try {
			// 由于页面可以取到相关文件所以没有从数据库取值，加快显示速度
			if (getIsInout()) {
				flowInoutTs = ConnectionPool.getProcessAction().getFlowInoutsByType(0, flowId);
				oldInoutTs = ConnectionPool.getProcessAction().getFlowInoutsByType(0, flowId);
			} else {
				flowInoutTs = ConnectionPool.getProcessAction().getFlowInoutsByType(1, flowId);
				oldInoutTs = ConnectionPool.getProcessAction().getFlowInoutsByType(1, flowId);
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/*
	 * private void cloneData() throws IllegalAccessException,
	 * InvocationTargetException, NoSuchMethodException { if (flowInoutTs !=
	 * null && !flowInoutTs.isEmpty()) { for (JecnFlowInoutData
	 * jecnFlowInoutData : flowInoutTs) { JecnFlowInoutData clone = new
	 * JecnFlowInoutData(); PropertyUtils.copyProperties(clone,
	 * jecnFlowInoutData); oldInoutTs.add(clone); } } }
	 */

	// *双击编辑*/
	@Override
	protected void dbClickMethod() {
		int selectedRow = table.getSelectedRow();
		if (selectedRow != -1) {
			List<String> names = new ArrayList<String>();
			for (int i = 0; i < flowInoutTs.size(); i++) {
				if (i != selectedRow) {
					names.add(flowInoutTs.get(i).getInout().getName()); // 获取名称验证重名
				}
			}
			FlowTermInoutDialog dialog = new FlowTermInoutDialog(flowInoutTs.get(selectedRow), names, getIsInout(),
					flowId);
			dialog.setVisible(true);
			if (dialog.isOk() && dialog.isOpaque()) {
				refreshTable();
			}
		}
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("name"));
		if (this.type == 0) {
			title.add(JecnProperties.getValue("offer"));
		} else {
			title.add(JecnProperties.getValue("recipient"));
		}
		title.add(JecnProperties.getValue("actBase"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vectors = new Vector<Vector<String>>();
		if (flowInoutTs != null && !flowInoutTs.isEmpty()) {
			for (JecnFlowInoutData bean : flowInoutTs) {
				Vector<String> content = new Vector<String>();
				content.add(bean.getInout().getName());
				content.add(splicingStr(bean.getPoses(), bean.getPosGroups(), bean.getInout().getPos()));
				content.add(bean.getInout().getExplain());
				vectors.add(content);
			}
		}
		return vectors;

	}

	private boolean getIsInout() {
		return type == 0 ? true : false;
	}

	/**
	 * 重新添加数据
	 * 
	 * @param resourceMap
	 */
	private void refreshTable() {
		remoeAll();
		if (flowInoutTs != null && !flowInoutTs.isEmpty()) {
			for (JecnFlowInoutData bean : flowInoutTs) {
				Vector<String> content = new Vector<String>();
				content.add(bean.getInout().getName());
				content.add(splicingStr(bean.getPoses(), bean.getPosGroups(), bean.getInout().getPos()));
				content.add(bean.getInout().getExplain());
				((DefaultTableModel) this.getTable().getModel()).addRow(content);
			}
		}
		insertSort();
	}

	public void remoeAll() {
		// 清空
		for (int index = this.getTable().getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) this.getTable().getModel()).removeRow(index);
		}
	}

	private String splicingStr(List<JecnTreeBean> posBean, List<JecnTreeBean> posGBean, String posText) {
		StringBuilder str = new StringBuilder();
		str.append(StringUtils.isBlank(posText) ? "" : posText + "\n");
		// 0岗位 1岗位组
		if (posBean != null && !posBean.isEmpty()) {
			for (JecnTreeBean pos : posBean) {
				str.append(pos.getName());
				if (StringUtils.isNotBlank(str.toString())) {
					str.append("\n");
				}
			}
		}
		if (posGBean != null && !posGBean.isEmpty()) {
			for (JecnTreeBean posG : posGBean) {
				str.append(posG.getName());
				if (StringUtils.isNotBlank(str.toString())) {
					str.append("\n");
				}
			}
		}
		String s = str.toString();
		if (s.endsWith("\n")) {
			s = s.substring(0, s.length() - 1);
		}
		return s;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] {};
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		if (flowInoutTs.size() != oldInoutTs.size()) {
			return true;
		}
		if (!flowInoutTs.containsAll(oldInoutTs)) {
			return true;
		}
		return false;
	}

	/**
	 * 获取结果集
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getData() {
		List<Map<String, Object>> doMap = new ArrayList<Map<String, Object>>();
		insertSort();
		for (JecnFlowInoutData bean : flowInoutTs) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("inout", bean.getInout());
			Set<Long> posIds = new HashSet<Long>();
			for (JecnTreeBean pos : bean.getPoses()) {
				posIds.add(pos.getId());
			}
			map.put("pos", posIds);
			Set<Long> posGIds = new HashSet<Long>();
			for (JecnTreeBean pos : bean.getPosGroups()) {
				posGIds.add(pos.getId());
			}
			map.put("posGroup", posGIds);
			doMap.add(map);
		}
		return doMap;
	}

	/**
	 * 节点排序
	 */
	private void insertSort() {
		int rowCount = table.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			JecnFlowInoutData jecnFlowInoutData = flowInoutTs.get(i);
			jecnFlowInoutData.getInout().setSortId(Long.valueOf(i));
		}
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	class MyRender extends AbstractCellEditor implements TableCellRenderer, ActionListener, TableCellEditor {
		private static final long serialVersionUID = 1L;
		protected JButton deleteBut = null;

		public MyRender() {
			deleteBut = new JButton(JecnProperties.getValue("delete"));
			deleteBut.addActionListener(this);
		}

		@Override
		public Object getCellEditorValue() {
			return null;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			return deleteBut;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			table.setRowSelectionAllowed(false);
			int selectedRow = table.getSelectedRow();
			JecnFlowTermInOut.this.flowInoutTs.remove(selectedRow);
			JecnFlowTermInOut.this.refreshTable();
			table.selectAll();
		}

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			return deleteBut;
		}
	}

	// 自定义的表格绘制器
	class TableViewRenderer extends JTextArea implements TableCellRenderer {
		public TableViewRenderer() {
			// 将表格设为自动换行
			setLineWrap(true); // 利用JTextArea的自动换行方法
		}

		public Component getTableCellRendererComponent(JTable jtable, Object obj, // obj指的是单元格内容
				boolean isSelected, boolean hasFocus, int row, int column) {
			setText(obj == null ? "" : obj.toString()); // 利用JTextArea的setText设置文本方法
			// 计算当下行的最佳高度

			// 计算了该行所有列的内容所对应的高度，挑选最高的那个
			int maxPreferredHeight = 18;
			for (int i = 0; i < jtable.getColumnCount(); i++) {
				setText("" + jtable.getValueAt(row, i));
				setSize(jtable.getColumnModel().getColumn(column).getWidth(), 0);
				maxPreferredHeight = Math.max(maxPreferredHeight, getPreferredSize().height);
			}

			if (jtable.getRowHeight(row) != maxPreferredHeight)

			{
				jtable.setRowHeight(row, maxPreferredHeight);
			}

			if (isSelected) {
				this.setBackground(jtable.getSelectionBackground());
			} else {
				this.setBackground(jtable.getBackground());
			}

			setText(obj == null ? "" : obj.toString());
			return this;
		}

	}

}
