package com.jecn.viewdoc.convert.view;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.jecn.epros.server.bean.file.FileOpenBean;

/**
 * pageoffice预览
 * 
 * @author hyl
 * 
 */
public class PageofficeView extends AbsView {

	public static String[] DEFAULT_ALLOW = new String[] { "doc", "docx", "ppt", "pptx", "pdf", "xls", "xlsx" };

	public PageofficeView(ViewInfo viewInfo) {
		super(viewInfo);
	}

	@Override
	public boolean allowView() {
		return true;
	}

	@Override
	public Set<String> allowViewExtensions() {
		return new HashSet<String>(Arrays.asList(DEFAULT_ALLOW));
	}

	@Override
	public String getPath() {
		String filePath = null;
		try {
			if (this.checkFileViewable(getFileName())) {
				FileOpenBean fileOpenBean = getFileOpenBean();
				// 获取存储临时文件的目录
				filePath = ViewUtils.getNewTempPath(getFileName()).replace("/", "\\\\");
				FileUtils.writeByteArrayToFile(new File(filePath), fileOpenBean.getFileByte());
			} else {
				throw new IllegalArgumentException("pageoffice不支持预览的类型");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		String extension = FilenameUtils.getExtension(getFileName());
		request.setAttribute("poCtrl", getData(extension, filePath));
		request.setAttribute("viewPage", ViewUtils.getPageofficePage(extension));

		return ViewUtils.getPageofficePage(extension);
	}

	private Object getData(String extension, String filePath) {
		Object result = null;
		if (extension.startsWith("ppt")) {
			result = getPPT(request, extension, filePath);
		} else if (extension.startsWith("doc")) {
			result = getDOC(request, extension, filePath);
		} else if (extension.startsWith("xls")) {
			result = getXLS(request, extension, filePath);
		} else if (extension.startsWith("pdf")) {
			result = getPDF(request, extension, filePath);
		} else {
			throw new IllegalArgumentException("不支持的预览类型：" + extension);
		}
		return result;
	}

	private Object getPDF(HttpServletRequest request, String extension, String filePath) {
		return ViewUtils.getPageOfficeBaseBean(request, extension, filePath);
	}

	private Object getXLS(HttpServletRequest request, String extension, String filePath) {
		return ViewUtils.getPageOfficeBaseBean(request, extension, filePath);
	}

	private Object getDOC(HttpServletRequest request, String extension, String filePath) {
		return ViewUtils.getPageOfficeBaseBean(request, extension, filePath);
	}

	private Object getPPT(HttpServletRequest request, String extension, String filePath) {
		return ViewUtils.getPageOfficeBaseBean(request, extension, filePath);
	}
}
