package com.jecn.epros.server.action.web.login.ad.powerlong;

import java.util.ResourceBundle;

/**
 * 
 * 中睿通信单点登录配置信息
 * 
 */
public class JecnPowerLongAfterItem {

	private static ResourceBundle powerLong;
	public static String TASK_URL;

	static {
		powerLong = ResourceBundle.getBundle("cfgFile/powerlong/powerLongServerConfig");
	}

	public static String getValue(String key) {
		return powerLong.getString(key);
	}
}
