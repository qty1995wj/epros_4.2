package com.jecn.epros.server.download.wordxml.chengdu29;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnPath;

/***
 * 成都29所操作说明
 * 
 * @author admin
 * 
 */
public class CD29ProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1.5F,
			LineRuleType.AUTO);
	/*** 段落行距固定值20磅 *****/
	private ParagraphLineRule vLineFix20 = new ParagraphLineRule(20,
			LineRuleType.EXACT);

	public CD29ProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		super.setDefAscii("黑体");
		getDocProperty().setNewRowHeight(0.83F);
		getDocProperty().setNewTblWidth(16.4F);
		getDocProperty().setFlowChartScaleValue(5F);
		setDocStyle("", textTitleStyle());
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(2.5F, 2.5F, 2.5F, 2.5F, 1.4F, 1.4F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0.2F, 0.2F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 创建通用的页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommftrhdr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		// 宋体 五号 右对齐
		ParagraphBean pBean = new ParagraphBean(Align.right, "宋体",
				Constants.wuhao, false);
		Header hdr = null;
		hdr = sect.createHeader(type);
		hdr.createParagraph(processDownloadBean.getFlowInputNum()
				+ processDownloadBean.getFlowName(), pBean);
		pBean = new ParagraphBean("宋体", Constants.wuhao, false);
		pBean.setpStyle(PStyle.AF1);
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 宋体 小四居中对齐
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体",
				Constants.xiaosi, false);
		Footer ftr = null;
		Paragraph p = null;
		ftr = sect.createFooter(type);
		p = ftr.createParagraph("", pBean);
		p.appendCurPageRun();
	}

	/**
	 * 添加封面节点封皮内容
	 * 
	 * @param titleSect
	 */
	private void addTitleSectContent(Sect titleSect) {
		ParagraphBean pBean = null;
		pBean = new ParagraphBean(Align.left, "宋体", Constants.xiaosi, false);
		pBean.setInd(0, 0, 0, 0.85F);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph("", pBean);
		// logo图标
		Image logo = new Image(getDocProperty().getLogoImage());
		PositionBean positionBean = new PositionBean(PositionType.absolute,
				-11.6F, 0, -10.5F, 0);
		logo.setSize(4.87F, 1.24F);
		logo.setPositionBean(positionBean);
		Paragraph logpP = titleSect.createParagraph("", pBean);
		logpP.appendGraphRun(logo);
		for (int i = 0; i < 5; i++) {
			titleSect.createParagraph("", pBean);
		}
		// 黑体 小一 居中
		pBean = new ParagraphBean(Align.center, "黑体", Constants.xiaoyi, true);
		titleSect.createParagraph(processDownloadBean.getFlowInputNum()
				+ processDownloadBean.getFlowName(), pBean);
		pBean = new ParagraphBean(Align.left, "宋体", Constants.xiaosi, false);
		pBean.setInd(0, 0, 0.85F, 0);
		pBean.setSpace(0f, 0f, vLine1);
		for (int i = 0; i < 17; i++) {
			titleSect.createParagraph("", pBean);
		}
		String imagePath = JecnPath.APP_PATH + "images/logo/chengdu29.png";
		Image image = new Image(imagePath);
		image.setSize(13.56F, 0.61F);
		Paragraph imageP = titleSect.createParagraph(" ", pBean);
		imageP.appendGraphRun(image);

		pBean = new ParagraphBean(Align.center, "黑体", Constants.erhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph("二〇     年   月   日 ", pBean);
	}

	/**
	 * 添加封面节点表格内容
	 * 
	 * @param titleSect
	 */
	private void addTitleSect_table(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, false);
		pBean.setSpace(0f, 0f, vLineFix20);
		TableBean tabBean = new TableBean();
		tabBean.setBorder(0.5F);
		tabBean.setTableLeftInd(0);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0, 0, 0));
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "文件编号", "", "签署页     版本" });
		rowData.add(new String[] { "" });
		rowData.add(new String[] { "签  署", "角      色", "签  名", "日  期" });
		rowData.add(new String[] { "拟  制", "", "", "" });
		rowData.add(new String[] { "审  核", "", "", "" });
		for (int i = 0; i < 4; i++) {
			rowData.add(new String[] { "", "", "", "" });
		}
		rowData.add(new String[] { "修  订  记  录" });
		rowData.add(new String[] { "版本", "页次", "数量", "更改单号", "签名", "日期", "版本",
				"页次", "数量", "更改单号", "签名", "日期" });
		for (int i = 0; i < 7; i++) {
			rowData.add(new String[] { "", "", "", "", "", "", "", "", "", "",
					"", "" });
		}
		// 创建表格
		Table tab = JecnWordUtil.createTab(titleSect, tabBean, new float[] {
				1.18F, 1.18F, 1.18F, 1.87F, 1.18F, 1.06F, 1.18F, 1.18F, 1.18F,
				1.8F, 1.31F, 1.31F }, rowData, pBean);
		tab.getRow(0).getCell(0).setColspan(3);
		tab.getRow(0).getCell(1).setColspan(5);
		tab.getRow(0).getCell(2).setColspan(4);
		tab.getRow(1).setHeight(5.95F);
		tab.getRow(1).getCell(0).setColspan(12);
		/****** 设置2-9 行各个单元格跨列 *******/
		for (int i = 2; i < 9; i++) {
			tab.getRow(i).getCell(0).setColspan(3);
			tab.getRow(i).getCell(1).setColspan(3);
			tab.getRow(i).getCell(2).setColspan(4);
			tab.getRow(i).getCell(3).setColspan(2);
		}
		tab.getRow(9).setHeight(1.54F);
		tab.getRow(9).getCell(0).setvAlign(Valign.center);
		tab.getRow(9).getCell(0).setColspan(12);

		/**** 添加第二行数据 ***/
		TableCell cell = tab.getRow(1).getCell(0);
		cell.createParagraph("");
		cell.createParagraph("");
		cell.createParagraph("");
		cell.createParagraph("");
		// 宋体 小四 居右 最小值20磅值
		pBean = new ParagraphBean(Align.left, "黑体", Constants.erhao, false);
		pBean.setSpace(0f, 0f, vLineFix20);
		pBean.setInd(3.39F, 0, 0, 0);
		cell.createParagraph("批  准：", pBean);
		cell.createParagraph("");
		cell.createParagraph("");
		// 宋体 小四 居右 最小值20磅值
		pBean = new ParagraphBean(Align.right, "宋体", Constants.xiaosi, false);
		pBean.setSpace(0f, 0f, vLineFix20);
		pBean.setInd(3.39F, 0, 0, 0);
		cell.createParagraph("年     月    日", pBean);
		cell.createParagraph("");

	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.7F, 2.7F, 2.7F, 2.7F, 1.4F, 1.4F);
		titleSect.setSectBean(sectBean);
		addTitleSectContent(titleSect);
		addTitleSect_table(titleSect);
		titleSect.createHeader(HeaderOrFooterType.odd).createParagraph("   ",
				new ParagraphBean(PStyle.AF1));
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommftrhdr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(1.7F, 1.7F, 1.7F, 1.7F, 1F, 1F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommftrhdr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommftrhdr(secondSect);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体",
				Constants.wuhao, false);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0.19F, 0, 0.19F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, true);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体",
				Constants.xiaosi, false);
		textContentStyle.setInd(0, 0, 0, 0.85F);
		textContentStyle.setSpace(0f, 0f, vLine1);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "黑体",
				Constants.xiaosi, false);
		textTitleStyle.setInd(0, 0, 0, 0.2F);
		textTitleStyle.setSpace(0.5F, 0.5F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
	
	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles,
			List<String[]> rowData) {
		
		List<String[]> newRowData=new ArrayList<String[]>();
		for(String[] strArr:rowData){
			String[] arr=new String[6];
			newRowData.add(arr);
			arr[0]=strArr[0];
			arr[1]=strArr[1];
			arr[2]=strArr[2];
			arr[3]=strArr[3];
			arr[4]=handle(strArr[4]);
			arr[5]=handle(strArr[5]);
		}
		
		super.a10(processFileItem, titles, newRowData);
		
	}

	private String handle(String str) {
		StringBuffer buf = new StringBuffer();
		if (StringUtils.isNotBlank(str)) {
			String[] split = str.split("\n");
			int num=0;
			for (String fileName : split) {
				if(StringUtils.isBlank(fileName)){
					continue;
				}
				num++;
				int index = fileName.lastIndexOf(".");
				buf.append(num+".");
				if (index > 0) {
					buf.append(fileName.substring(0, index));
				} else {
					buf.append(fileName);
				}
				buf.append("\n");
			}
			return buf.toString();
		}
		return "";
	}
}
