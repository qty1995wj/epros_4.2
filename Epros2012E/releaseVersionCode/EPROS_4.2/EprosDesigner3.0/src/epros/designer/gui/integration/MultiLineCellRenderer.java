package epros.designer.gui.integration;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;

public class MultiLineCellRenderer extends JTextArea implements
		TableCellRenderer {
	private Color BackColor;
	private Color defaultBackColor;
	private Color defaultForeColor;
	private Color[][] changedColor;
	private int flag;
	/**自动撑开表格 false：不撑开；true：撑开*/
	private boolean isAutoHeight = false;
	public MultiLineCellRenderer(int intRow, int intCol,int flag) {
		setLineWrap(true);
		setWrapStyleWord(true);
		setOpaque(true);
		defaultBackColor = Color.WHITE;
		defaultForeColor = Color.BLACK;
		this.flag = flag;

//		changedColor = new Color[intRow][intCol];
//		for (int i = 0; i < intRow; i++) {
//			for (int j = 0; j < intCol; j++) {
//				changedColor[i][j] = defaultBackColor;
//			}
//		}
	}
	public MultiLineCellRenderer(boolean isAutoHeight,int flag) {
//		setOpaque(true);
		setLineWrap(true);
		setWrapStyleWord(true);
		this.isAutoHeight = isAutoHeight;
		this.flag = flag;
	}
//	public void setCellBackColor(int setRow, int setCol, Color setColor) {

//		changedColor[setRow][setCol] = setColor;
//	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		
		
//		if (hasFocus) {
//			setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
//		} else {
//			setBorder(new EmptyBorder(1, 2, 1, 2));
//		}
		if(isAutoHeight){
			setOpaque(true);
			setForeground(defaultForeColor);
			setBackground(Color.WHITE);
			if(table.isRowSelected(row)){
				setBackground(Color.lightGray);
			}
			// 计算当下行的最佳高度
			int maxPreferredHeight = 0;
			for (int i = 0; i < table.getColumnCount(); i++) {
				setText("" + table.getValueAt(row, i));
				setSize(table.getColumnModel().getColumn(column).getWidth(), 0);
				maxPreferredHeight = Math.max(maxPreferredHeight,
						getPreferredSize().height);
			}
			//选中后高度设置
			if (table.getRowHeight(row) != maxPreferredHeight) { 
				table.setRowHeight(row, maxPreferredHeight);
			}
		}else{
			setForeground(defaultForeColor);
			setBackground(Color.WHITE);
			if(table.isRowSelected(row)){
				setBackground(Color.LIGHT_GRAY);
			}
			setFont(table.getFont());
			
			if(table.isRowSelected(row)){
				if(flag == 0){
					// 计算当下行的最佳高度
					int maxPreferredHeight = 0;
					for (int i = 0; i < table.getColumnCount(); i++) {
						setText("" + table.getValueAt(row, i));
						setSize(table.getColumnModel().getColumn(column).getWidth(), 0);
						maxPreferredHeight = Math.max(maxPreferredHeight,
								getPreferredSize().height);
					}
					//选中后高度设置
					if (table.getRowHeight(row) != maxPreferredHeight) { 
						table.setRowHeight(row, maxPreferredHeight);
					}
				}else{
					//选中后高度设置
	//				if (table.getRowHeight(row) != controlGuideTable.getRowHeight(row)) { 
						table.setRowHeight(row, table.getRowHeight());
	//				}
				}
				
			}
		}
		setText((value == null) ? "" : value.toString());
		return this;
	}
}