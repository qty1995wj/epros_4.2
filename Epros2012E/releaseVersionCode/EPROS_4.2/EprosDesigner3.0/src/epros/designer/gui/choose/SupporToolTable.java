package epros.designer.gui.choose;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.util.JecnProperties;
import epros.draw.util.JecnUIUtil;

public class SupporToolTable extends JTable  {
	private List<JecnTreeBean> suppotoollist = new ArrayList<JecnTreeBean>();
	public SupporToolTable(List<JecnTreeBean> suppotoollist) {
		this.suppotoollist = suppotoollist;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public SupporToolMode getTableModel() {
		Vector<String> title=new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return new SupporToolMode(getContent(suppotoollist),
				title);
	}
	class SupporToolMode extends DefaultTableModel {
		public SupporToolMode(Vector<Vector<String>> data,
				Vector<String> title) {
			super(data, title);

		}
		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}
	}
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}
	public Vector<String> getTableTitle() {
		Vector<String> title=new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return title;
	}
	public  void remoeAll(){
		//清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel)this.getModel()).removeRow(index);
        } 
	}
	public int[] gethiddenCols() {
		return new int[] { 0 };
	}

	public boolean isSelectMutil() {
		return true;
	}
}
