package com.jecn.epros.server.bean.process;

import java.math.BigDecimal;

/**
 * 
 * 流程元素正式表
 * 
 * @author Administrator
 * 
 */
public class JecnFlowStructureImage implements java.io.Serializable {
	private Long flowId;// 流程ID
	private Long figureId;// 主键ID
	private String UUID;// 索引
	private String figureType;// 元素类型
	private String figureText;// 元素名称
	private Long startFigure;// 连接线输出的图像元素的FigureId
	private Long endFigure;// 连接线输入的图像元素的FigureId
	private Long poLongx;// 图像元素开始点的X
	private Long poLongy;// 图像元素开始点的Y
	private Long width;// 图像元素的宽
	private Long height;// 图像元素的高
	private Long fontSize;// 线的粗细大小 或图形字体的大小
	private String BGColor;// 图像元素的填充色
	private String fontColor;// 字体颜色
	private Long linkFlowId;// 连接流程Id或者制度ID
	private String lineColor;// 关键活动
	// :1为PA,2为KSF,3为KCP
	private String fontPosition;// 字体位置
	private Long havaShadow;// 是否有阴影
	private String circumgyrate;// 元素的选择角度
	private String bodyLine;// 线的样式
	private String bodyColor;// 边框颜色
	private Long isErect;// 字体竖排

	private String figureImageId;// 临时的唯一标示
	private Long orderIndex;// 层级
	private String activityShow;// 活动说明或者图片的临时名称
	private String activityId;// 活动编号
	private Long fontBody;// 字体加粗
	private Long frameBody;// 边框加粗
	private String fontType;// 字体类型
	private String roleRes;// 角色职责或者关键活动的控制点
	private Integer is3DEffect;// 是否有3D效果
	private Integer fillEffects;// 填充效果
	private Integer lineThickness;// 线的深
	private String startTime;// 活动开始时间
	private String stopTime;// 活动结束时间
	private Integer timeType;// 活动时间类型
	private String startFigureUUID;// 开始点元素UUID
	private String endFigureUUID;// 结束点元素UUID
	private String shadowColor;// 阴影颜色
	private String innerControlRisk;// 对应内控矩阵风险点
	private String standardConditions;// 对应标准条款
	/** 活动类型ID */
	private Long activeTypeId;
	/** 是否线上 0：线下 ;1：线上 */
	private int isOnLine;
	private String activityInput;// 输入说明
	private String activityOutput;// 输出说明
	/** 记录泳池分割点X坐标(目前为横向图) */
	private int dividingX = 30;

	/************** 时间轴 ***************************/
	/** 目标值 */
	private BigDecimal targetValue;
	/** 现状值 */
	private BigDecimal statusValue;
	/** 说明 */
	private String explain;

	/** 接口类型 1是上游流程,2是下游流程,3是过程流程,4是子流程 */
	private int implType;
	/** 接口名称 **/
	private String implName;
	/** 备注 **/
	private String implNote;
	/** 流程要求 **/
	private String processRequirements;
	/** 自定义 （爱普生 活动担当） */
	private String customOne;

	/** 元素形狀 */
	private String eleShape;

	public int getIsOnLine() {
		return isOnLine;
	}

	public void setIsOnLine(int isOnLine) {
		this.isOnLine = isOnLine;
	}

	public Long getActiveTypeId() {
		return activeTypeId;
	}

	public void setActiveTypeId(Long activeTypeId) {
		this.activeTypeId = activeTypeId;
	}

	public String getInnerControlRisk() {
		return innerControlRisk;
	}

	public void setInnerControlRisk(String innerControlRisk) {
		this.innerControlRisk = innerControlRisk;
	}

	public String getStandardConditions() {
		return standardConditions;
	}

	public void setStandardConditions(String standardConditions) {
		this.standardConditions = standardConditions;
	}

	public String getStopTime() {
		return stopTime;
	}

	public void setStopTime(String stopTime) {
		this.stopTime = stopTime;
	}

	public Integer getTimeType() {
		return timeType;
	}

	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getFigureType() {
		return figureType;
	}

	public void setFigureType(String figureType) {
		this.figureType = figureType;
	}

	public String getFigureText() {
		return figureText;
	}

	public void setFigureText(String figureText) {
		this.figureText = figureText;
	}

	public Long getStartFigure() {
		return startFigure;
	}

	public void setStartFigure(Long startFigure) {
		this.startFigure = startFigure;
	}

	public Long getEndFigure() {
		return endFigure;
	}

	public void setEndFigure(Long endFigure) {
		this.endFigure = endFigure;
	}

	public Long getPoLongx() {
		return poLongx;
	}

	public void setPoLongx(Long poLongx) {
		this.poLongx = poLongx;
	}

	public Long getPoLongy() {
		return poLongy;
	}

	public void setPoLongy(Long poLongy) {
		this.poLongy = poLongy;
	}

	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}

	public Long getFontSize() {
		return fontSize;
	}

	public void setFontSize(Long fontSize) {
		this.fontSize = fontSize;
	}

	public String getBGColor() {
		return BGColor;
	}

	public void setBGColor(String color) {
		BGColor = color;
	}

	public String getStartFigureUUID() {
		return startFigureUUID;
	}

	public void setStartFigureUUID(String startFigureUUID) {
		this.startFigureUUID = startFigureUUID;
	}

	public String getEndFigureUUID() {
		return endFigureUUID;
	}

	public void setEndFigureUUID(String endFigureUUID) {
		this.endFigureUUID = endFigureUUID;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public Long getLinkFlowId() {
		return linkFlowId;
	}

	public void setLinkFlowId(Long linkFlowId) {
		this.linkFlowId = linkFlowId;
	}

	public String getLineColor() {
		return lineColor;
	}

	public void setLineColor(String lineColor) {
		this.lineColor = lineColor;
	}

	public String getFontPosition() {
		return fontPosition;
	}

	public void setFontPosition(String fontPosition) {
		this.fontPosition = fontPosition;
	}

	public Long getHavaShadow() {
		return havaShadow;
	}

	public void setHavaShadow(Long havaShadow) {
		this.havaShadow = havaShadow;
	}

	public String getCircumgyrate() {
		return circumgyrate;
	}

	public void setCircumgyrate(String circumgyrate) {
		this.circumgyrate = circumgyrate;
	}

	public String getBodyLine() {
		return bodyLine;
	}

	public void setBodyLine(String bodyLine) {
		this.bodyLine = bodyLine;
	}

	public String getBodyColor() {
		return bodyColor;
	}

	public void setBodyColor(String bodyColor) {
		this.bodyColor = bodyColor;
	}

	public Long getIsErect() {
		return isErect;
	}

	public void setIsErect(Long isErect) {
		this.isErect = isErect;
	}

	public String getFigureImageId() {
		return figureImageId;
	}

	public void setFigureImageId(String figureImageId) {
		this.figureImageId = figureImageId;
	}

	public Long getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(Long orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getActivityShow() {
		if (!isNullOrEmtryTrim(activityShow)) {
			return activityShow;
		} else {
			return "";
		}
	}

	public void setActivityShow(String activityShow) {
		this.activityShow = activityShow;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public Long getFontBody() {
		return fontBody;
	}

	public void setFontBody(Long fontBody) {
		this.fontBody = fontBody;
	}

	public Long getFrameBody() {
		return frameBody;
	}

	public void setFrameBody(Long frameBody) {
		this.frameBody = frameBody;
	}

	public String getFontType() {
		return fontType;
	}

	public void setFontType(String fontType) {
		this.fontType = fontType;
	}

	public String getRoleRes() {
		return roleRes;
	}

	public void setRoleRes(String roleRes) {
		this.roleRes = roleRes;
	}

	public Integer getIs3DEffect() {
		return is3DEffect;
	}

	public void setIs3DEffect(Integer is3DEffect) {
		this.is3DEffect = is3DEffect;
	}

	public Integer getFillEffects() {
		return fillEffects;
	}

	public void setFillEffects(Integer fillEffects) {
		this.fillEffects = fillEffects;
	}

	public Integer getLineThickness() {
		return lineThickness;
	}

	public void setLineThickness(Integer lineThickness) {
		this.lineThickness = lineThickness;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getShadowColor() {
		return shadowColor;
	}

	public void setShadowColor(String shadowColor) {
		this.shadowColor = shadowColor;
	}

	public int getDividingX() {
		return dividingX;
	}

	public void setDividingX(int dividingX) {
		this.dividingX = dividingX;
	}

	public String getActivityInput() {
		return activityInput;
	}

	public void setActivityInput(String activityInput) {
		this.activityInput = activityInput;
	}

	public String getActivityOutput() {
		return activityOutput;
	}

	public void setActivityOutput(String activityOutput) {
		this.activityOutput = activityOutput;
	}

	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	private boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

	public BigDecimal getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(BigDecimal targetValue) {
		this.targetValue = targetValue;
	}

	public BigDecimal getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(BigDecimal statusValue) {
		this.statusValue = statusValue;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public int getImplType() {
		return implType;
	}

	public void setImplType(int implType) {
		this.implType = implType;
	}

	public String getImplName() {
		return implName;
	}

	public void setImplName(String implName) {
		this.implName = implName;
	}

	public String getImplNote() {
		return implNote;
	}

	public void setImplNote(String implNote) {
		this.implNote = implNote;
	}

	public String getProcessRequirements() {
		return processRequirements;
	}

	public void setProcessRequirements(String processRequirements) {
		this.processRequirements = processRequirements;
	}

	public String getCustomOne() {
		return customOne;
	}

	public void setCustomOne(String customOne) {
		this.customOne = customOne;
	}

	public String getEleShape() {
		return eleShape;
	}

	public void setEleShape(String eleShape) {
		this.eleShape = eleShape;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

}
