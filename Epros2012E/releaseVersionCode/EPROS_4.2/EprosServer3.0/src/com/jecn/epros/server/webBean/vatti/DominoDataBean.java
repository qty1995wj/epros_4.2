package com.jecn.epros.server.webBean.vatti;

import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.email.buss.EmailConfigItemBean;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 华帝Domino平台 所需的数据Bean 存储流程和制度信息
 * 
 * @author weidp
 * @date： 日期：2014-5-6 时间：上午10:32:51
 */
public class DominoDataBean {
	/** 主键Id */
	private String id;
	/** 名称 */
	private String name;
	/** 类型 Flow--流程 Rule--制度 */
	private String type;
	/** 访问路径 */
	private String url;
	/** 发布时间 */
	private String publishTime;

	public DominoDataBean() {

	}

	public DominoDataBean(Rule rule) {
		this.id = JecnUtil.valueToString(rule.getId());
		this.name = rule.getRuleName();
		String type = "Rule";
		this.setUrl(type);
		this.setType(type);
		this.publishTime = JecnUtil.valueToDateStr(rule.getUpdateDate());
	}

	public DominoDataBean(JecnFlowStructure flowStructure) {
		this.id = JecnUtil.valueToString(flowStructure.getFlowId());
		this.name = flowStructure.getFlowName();
		String type = flowStructure.getIsFlow() == 0 ? "processMap" : "process";
		this.setUrl(type);
		this.setType(type);
		this.publishTime = JecnUtil.valueToDateStr(flowStructure
				.getUpdateDate());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	/**
	 * 更改类型为Domino平台需要的类型标志
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-9 时间：上午11:32:31
	 * @param type
	 */
	private void setType(String type) {
		if (type.equals("process") || type.equals("processMap")) {
			this.type = "Flow";
		} else {
			this.type = "Rule";
		}
	}

	public String getUrl() {
		return url;
	}

	/**
	 * 按照类型来给出所需的Url
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-9 时间：上午11:32:50
	 * @param type
	 */
	private void setUrl(String type) {
		if (type.equals("process")) {
			this.url = getJecnServerHttp()
					+ "/loginMail.action?accessType=mailOpenDominoMap&mailFlowType=process&mailFlowId="
					+ this.id;
		} else if (type.equals("processMap")) {
			this.url = getJecnServerHttp()
					+ "/loginMail.action?accessType=mailOpenDominoMap&mailFlowType=processMap&mailFlowId="
					+ this.id;
		} else {
			this.url = getJecnServerHttp()
					+ "/loginMail.action?accessType=mailOpenDominoRuleModelFile&mailRuleType=ruleModeFile&mailRuleId="
					+ this.id;
		}
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	/**
	 * 获取浏览端地址
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-8 时间：上午09:46:57
	 * @return
	 * @throws Exception
	 */
	private String getJecnServerHttp() {
		return EmailConfigItemBean.getConfigItemBean().getHttpString();
	}
	
	/**
	 * 获取Key（用于从Domino视图中获取某一条记录）
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-27 时间：下午02:53:34
	 * @return
	 */
	public Object getKey() {
		return this.id + this.type;
	}

}
