package com.jecn.epros.server.download.wordxml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ActiveItSystem;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;

public class WordPlex {

	private ProcessDownloadBean processDownloadBean;
	private ProcessFileModel model;
	private ProcessFileProperty docProperty;

	public WordPlex(ProcessFileModel model, ProcessDownloadBean processDownloadBean) {
		this.processDownloadBean = processDownloadBean;
		this.model = model;
		this.docProperty = model.getDocProperty();
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	public void paragraphStyleKpi(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		// KPI配置bean
		List<JecnConfigItemBean> configItemBeanList = processDownloadBean.getConfigKpiItemBean();
		createTitle(processFileItem);
		// 是否创建表格风格段落
		boolean flagTab = oldRowData.size() > 1 ? true : false;
		ParagraphBean pBean = model.textContentStyle();
		// 表格内容
		int count = 0;
		for (String[] row : oldRowData) { // 对应一条配置数据
			List<String[]> tableData = new ArrayList<String[]>();// 一个表格数据
			String[] rowContent = null;// 表格一行数据
			// boolean isExitKpiName = false;
			if (flagTab) {
				String number = ++count + ".";
				processFileItem.getBelongTo().createParagraph(number, pBean);
			}
			for (JecnConfigItemBean config : configItemBeanList) {
				if ("0".equals(config.getValue())) {
					continue;
				}
				rowContent = new String[2];
				rowContent[0] = config.getName();
				rowContent[1] = getKpiContent(row, config.getMark());
				tableData.add(rowContent);
			}
			Table tbl = createTableItem(processFileItem, new float[] { 4.25F, 11.15F }, tableData);
			tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());
		}
	}

	private Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		return model.createTableItem(processFileItem, width, rowData);
	}

	private ProcessFileProperty getDocProperty() {
		return model.getDocProperty();
	}

	private void createTitle(ProcessFileItem processFileItem) {
		model.createTitle(processFileItem);
	}

	/**
	 * * 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6 KIP值单位名称 7 数据获取方式 8
	 * 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12 目的 13 测量点 14 统计周期 15 说明
	 */
	private String getKpiContent(String[] row, String mark) {
		if ("kpiName".equals(mark)) {// KPI名称
			return row[0];
		} else if ("kpiType".equals(mark)) {// KPI类型
			return row[1];
		} else if ("kpiUtilName".equals(mark)) {// KPI单位名称
			return JecnResourceUtil.getKpiVertical(Integer.valueOf(row[6]));
		} else if ("kpiTargetValue".equals(mark)) {// KPI目标值
			return row[2];
		} else if ("kpiTimeAndFrequency".equals(mark)) {// 数据统计时间频率
			return JecnResourceUtil.getKpiHorizontal(Integer.valueOf(row[5]));
		} else if ("kpiDefined".equals(mark)) {// KPI定义
			return row[3];
		} else if ("kpiDesignFormulas".equals(mark)) {// 流程KPI计算方式
			return row[4];
		} else if ("kpiDataProvider".equals(mark)) {// 数据提供者
			return row[10];
		} else if ("kpiDataAccess".equals(mark)) {// 数据获取方式
			return JecnResourceUtil.getKpiDataMethod(Integer.valueOf(row[7]));
		} else if ("kpiIdSystem".equals(mark)) {// IT 系统
			return row[16];
		} else if ("kpiSourceIndex".equals(mark)) {// 指标来源
			return JecnResourceUtil.getKpiTargetType(Integer.valueOf(row[9]));
		} else if ("kpiRrelevancy".equals(mark)) {// 相关度
			return JecnResourceUtil.getKpiRelevance(Integer.valueOf(row[8]));
		} else if ("kpiSupportLevelIndicator".equals(mark)) {// 支持的一级指标
			return row[11];
		} else if ("kpiPurpose".equals(mark)) {// 设置目的
			return row[12];
		} else if ("kpiPoint".equals(mark)) {// 测量点
			return row[13];
		} else if ("kpiPeriod".equals(mark)) {// 统计周期
			return row[14];
		} else if ("kpiExplain".equals(mark)) {// 说明
			return row[15];
		}
		return "";
	}

	public void createApplyScope(ProcessFileItem processFileItem) {
		Sect sect = processFileItem.getBelongTo();
		createTitle(processFileItem);
		sect.createParagraph("1（组织范围）：" + JecnUtil.nullToEmpty(model.concat(processDownloadBean.getApplyOrgs())) + ";",
				model.textContentStyle());
		sect.createParagraph("2（业务范围 / 产品范围）：" + JecnUtil.nullToEmpty(processDownloadBean.getApplicability()), model
				.textContentStyle());
	}

	public void createActivityTable(ProcessFileItem processFileItem) {
		// 标题
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		boolean showTimeLine = JecnConfigTool.showActTimeLine();
		boolean showInOut = JecnContants.showActInOutBox;
		boolean showIT = JecnConfigTool.processFileActivityShowIT();

		// ---------标题
		List<String> titles = new ArrayList<String>();
		List<List<String>> datas = new ArrayList<List<String>>();
		// 活动编号
		titles.add(JecnUtil.getValue("activitynumbers"));
		// 执行角色
		titles.add(JecnUtil.getValue("executiveRole"));
		// 活动名称
		titles.add(JecnUtil.getValue("activityTitle"));
		// 活动说明
		titles.add(JecnUtil.getValue("activityIndicatingThat"));
		if (showInOut) {
			// 输入
			titles.add(JecnUtil.getValue("input"));
			// 输出
			titles.add(JecnUtil.getValue("output"));
		}
		if (showTimeLine) {
			// 现状目标
			titles.add(JecnUtil.getValue("timeLimit") + "\n(现状/目标" + JecnConfigTool.getActiveTimeLineUtil() + ")");
		}
		if (showIT) {
			// 信息化
			titles.add("信息化");
		}

		// 宽度
		List<Float> widthList = new ArrayList<Float>();
		widthList.add(2F);
		widthList.add(2F);
		widthList.add(2F);
		if (showInOut) {
			widthList.add(2F);
			widthList.add(2F);
		}
		if (showTimeLine) {
			widthList.add(2F);
		}
		if (showIT) {
			widthList.add(2F);
		}
		// 其余空间被活动说明占据
		float total = 0;
		for (float f : widthList) {
			total += f;
		}
		float max = docProperty.getNewTblWidth();
		// 活动说明的宽度
		float last = max - total > 2 ? max - total : 2;
		widthList.add(3, last);

		// --------内容
		Map<Long, String> idToKey = new HashMap<Long, String>();
		if (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent()) {
			List<KeyActivityBean> keyActivityShowList = processFileItem.getDataBean().getKeyActivityShowList();
			for (KeyActivityBean k : keyActivityShowList) {
				idToKey.put(k.getId(), k.getActivityShowControl());
			}
		}
		List<String[]> dataRows = JecnWordUtil.obj2String(processFileItem.getDataBean().getAllActivityShowList());
		Map<String, List<String>> activeIdToITMap = getActiveIdToITMap();
		for (String[] data : dataRows) {
			String content = JecnUtil.nullToEmpty(data[3]);
			if (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent()) {
				String keyContent = JecnUtil.nullToEmpty(idToKey.get(Long.valueOf(data[8].toString())));
				if (StringUtils.isNotBlank(content) && StringUtils.isNotBlank(content)) {
					content = content + "\n" + keyContent;
				} else {
					content = content + keyContent;
				}
			}
			List<String> row = new ArrayList<String>(Arrays.asList(data[0], data[1], data[2], content));
			if (showInOut) {
				// 输入
				row.add(data[4]);
				// 输出
				row.add(data[5]);
			}
			if (showTimeLine) {
				// 现状目标
				String targetValue = StringUtils.isBlank(JecnUtil.objToStr(data[6])) ? "" : JecnUtil.objToStr(data[6]);
				String statusValue = StringUtils.isBlank(JecnUtil.objToStr(data[7])) ? "" : JecnUtil.objToStr(data[7]);
				String timeLineValue = ProcessFileModel.concatTimeLineValue(targetValue, statusValue);
				row.add(timeLineValue);
			}
			if (showIT) {
				// 信息化
				List<String> its = activeIdToITMap.get(data[8]);
				if (its != null && its.size() > 0) {
					row.add(JecnUtil.concat(its, "/"));
				} else {
					row.add("");
				}
			}

			datas.add(row);
		}

		// 添加标题
		int columnSize = titles.size();
		rowData.add(titles.toArray(new String[columnSize]));
		for (List<String> data : datas) {
			rowData.add(data.toArray(new String[columnSize]));
		}
		float[] widths = new float[columnSize];
		for (int i = 0; i < columnSize; i++) {
			widths[i] = widthList.get(i);
		}
		Table tbl = createTableItem(processFileItem, widths, rowData);
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	private Map<String, List<String>> getActiveIdToITMap() {
		List<ActiveItSystem> activeItSystems = processDownloadBean.getActiveItSystems();
		Map<String, List<String>> m = new HashMap<String, List<String>>();
		for (ActiveItSystem s : activeItSystems) {
			List<String> ls = JecnUtil.getElseAddList(m, s.getActiveId().toString());
			ls.add(s.getSystemName());
		}
		return m;
	}
}
