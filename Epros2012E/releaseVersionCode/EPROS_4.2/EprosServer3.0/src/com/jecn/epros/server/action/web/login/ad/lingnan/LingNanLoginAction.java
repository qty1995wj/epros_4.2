package com.jecn.epros.server.action.web.login.ad.lingnan;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 岭南单点登录 运行流程 1、
 */
public class LingNanLoginAction extends JecnAbstractADLoginAction {
    
	private static final String privateKey = LingNanAfterItem.getValue("privateKey");//获取解析token 的私钥

	public LingNanLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	public String login() {
		String loginName = null;
		String token = "";
		// kk或别的系统产生的LRToken
		Cookie[] cookies = loginAction.getRequest().getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("LRToken")) {
				token = cookie.getValue();
			}
			log.info("cookie : " + cookie.getName() + " --- " + cookie.getValue());
		}
		    log.info("token -- " + token);
		try {
			loginName = TokenUtils.parseLRToken(token, null, privateKey);//解析token
			log.info("loginName -- " + loginName);
			System.out.println("loginName -- " + loginName);
			if (StringUtils.isNotBlank(loginName)) {
				return loginByLoginName(loginName);
			}
		} catch (Exception e) {
			log.error("token解析异常：" + token);
			e.printStackTrace();
		}
		return LoginAction.INPUT;
	}
}
