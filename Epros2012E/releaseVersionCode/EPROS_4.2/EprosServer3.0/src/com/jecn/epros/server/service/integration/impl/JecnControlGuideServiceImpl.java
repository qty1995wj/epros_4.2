package com.jecn.epros.server.service.integration.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.integration.IJecnControlGuideDao;
import com.jecn.epros.server.service.integration.IJecnControlGuideService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * 内控指引知识库Service实现类
 * 
 * @author Administrator
 * 
 */
@Transactional(rollbackFor = Exception.class)
public class JecnControlGuideServiceImpl extends AbsBaseService<JecnControlGuide, Long> implements
		IJecnControlGuideService {

	private Logger log = Logger.getLogger(JecnControlGuideServiceImpl.class);

	/** 内控指引知识库Dao接口 */
	private IJecnControlGuideDao jecnControlGuideDao;

	public IJecnControlGuideDao getJecnControlGuideDao() {
		return jecnControlGuideDao;
	}

	public void setJecnControlGuideDao(IJecnControlGuideDao jecnControlGuideDao) {
		this.jecnControlGuideDao = jecnControlGuideDao;
	}

	/**
	 * 添加内控指引目录
	 * 
	 * @param jecnControlGuide
	 *            内控指引知识库Bean
	 * @return
	 * @throws Exception
	 */
	@Override
	public long addJecnControlGuideDir(JecnControlGuide jecnControlGuide) throws Exception {
		try {
			jecnControlGuideDao.save(jecnControlGuide);
			JecnUtil.saveJecnJournal(jecnControlGuide.getId(), jecnControlGuide.getName(), 18, 1, jecnControlGuide
					.getCreatePerson(), jecnControlGuideDao);
			return jecnControlGuide.getId();
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl添加内控指引目录出错！", e);
			throw e;
		}
	}

	/**
	 * 根据文件名称查询文件目录
	 * 
	 * @param name
	 *            所选文件名称
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnControlGuide> getJecnControlGuideDirsByName(String name) throws Exception {
		return null;
	}

	/**
	 * 根据父ID查询内控指引目录子节点
	 * 
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getChildJecnControlGuideDirs(Long pId) throws Exception {
		String sql = "select cg.id,cg.parent_id,cg.type,cg.name,cg.description,"
				+ "cg.create_person,cg.create_time,cg.update_person,cg.update_time,"
				+ "cg.note,(select count(*) from jecn_control_guide where parent_id = "
				+ "cg.parent_id) from JECN_CONTROL_GUIDE cg where cg.parent_id = " + pId + " "
				+ "and type = 0 order by cg.sort";
		List<Object[]> objList = jecnControlGuideDao.listNativeSql(sql);

		List<JecnTreeBean> jecnTreeBeans = new ArrayList<JecnTreeBean>();
		for (Object[] obj : objList) {
			if (obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
					|| obj[6] == null || obj[7] == null || obj[8] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			jecnTreeBean.setPid(Long.valueOf(obj[1].toString()));
			jecnTreeBean.setTreeNodeType(TreeNodeType.innerControlDir);
			jecnTreeBean.setName(obj[3].toString());
			// 是否有子节点
			if (Integer.parseInt(obj[10].toString()) > 0) {
				// 内控指引知识库节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			jecnTreeBeans.add(jecnTreeBean);
		}
		return jecnTreeBeans;
	}

	/**
	 * 移动节点
	 * 
	 * @param listIds
	 *            要移动的节点ID集合
	 * @param pId
	 *            当前节点父ID
	 * @param updatePersonId
	 *            更新人
	 * @throws Exception
	 */
	@Override
	public void moveSortControlGuide(List<Long> listIds, Long pId, Long updatePersonId) throws Exception {
		try {
			String hql = "update JecnControlGuide set parentId = ?, updateTime = ?, " + "updatePerson = ? where id in "
					+ JecnCommonSql.getIds(listIds) + "order by sort";
			jecnControlGuideDao.execteHql(hql, pId, new Date(), updatePersonId);
			JecnUtil.saveJecnJournals(listIds, 18, 1, updatePersonId, jecnControlGuideDao);
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl内控指引节点移动出错：", e);
			throw e;
		}
	}

	/**
	 * 根据父ID查询内控指引子节点
	 * 
	 * @param pId
	 *            当前节点父ID
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getChildJecnControlGuides(Long pId, Long projectId) throws Exception {
		String sql = "select cg.id,cg.parent_id,cg.type,cg.name,cg.description,"
				+ "cg.create_person,cg.create_time,cg.update_person,cg.update_time,"
				+ "cg.note,(select count(*) from jecn_control_guide where parent_id = "
				+ "cg.id) from JECN_CONTROL_GUIDE cg where cg.parent_id = " + pId + "and cg.project_id = " + projectId
				+ "order by cg.sort";
		List<Object[]> objList = jecnControlGuideDao.listNativeSql(sql);

		List<JecnTreeBean> jecnTreeBeans = new ArrayList<JecnTreeBean>();
		for (Object[] obj : objList) {
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			jecnTreeBean.setPid(Long.valueOf(obj[1].toString()));
			if ("0".equals(obj[2].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.innerControlDir);
			} else if ("1".equals(obj[2].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.innerControlClause);
			}
			jecnTreeBean.setName(obj[3].toString());
			// 内控指引知识库详细条款
			if (obj[4] != null) {
				jecnTreeBean.setContent(obj[4].toString());
			}
			// 是否有子节点
			if (Integer.parseInt(obj[10].toString()) > 0) {
				// 内控指引知识库节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			jecnTreeBeans.add(jecnTreeBean);
		}
		return jecnTreeBeans;
	}

	/**
	 * 内控指引目录重命名
	 * 
	 * @param id
	 *            当前目录ID
	 * @param name
	 *            新名称
	 * @param updatePersonId
	 *            更改人
	 * @throws Exception
	 */
	@Override
	public void reControlGuideDirName(Long id, String name, Long updatePersonId) throws Exception {
		try {
			String hql = "update JecnControlGuide set name = ?, updateTime = ?, "
					+ "updatePerson = ? where id = ? order by sort";
			jecnControlGuideDao.execteHql(hql, name, new Date(), updatePersonId, id);
			JecnUtil.saveJecnJournal(id, name, 18, 2, updatePersonId, jecnControlGuideDao, 4);
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl内控指引目录重命名出错：", e);
			throw e;
		}
	}

	/**
	 * 根据所选ID集合删除
	 * 
	 * @param listIds
	 *            所选ID集合
	 * @throws Exception
	 */
	@Override
	public void deleteControlGuides(List<Long> listIds, Long peopleId) throws Exception {
		try {
			// 待删除的ID
			Set<Long> setIds = new HashSet<Long>();
			String sql = "";
			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select distinct jm.id from jecn_control_guide jm "
						+ "connect by prior jm.id = jm.parent_id START WITH " + "jm.id in "
						+ JecnCommonSql.getIds(listIds);
				List<Object> list = jecnControlGuideDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			}
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "with my_guide as (select * from jecn_control_guide jm " + "where jm.id in "
						+ JecnCommonSql.getIds(listIds) + " union "
						+ "all select jmm.* from my_guide inner join jecn_control_guide "
						+ "jmm on my_guide.id = jmm.parent_id) select distinct id " + "from my_guide";
				List<Object> list = jecnControlGuideDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			}
			if (setIds.size() > 0) {
				jecnControlGuideDao.deleteControlGuide(setIds);
				JecnUtil.saveJecnJournals(setIds, 18, 3, peopleId, jecnControlGuideDao);
			}
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl内控指引知识库删除出错", e);
			throw e;
		}
	}

	/**
	 * 内控指引节点排序
	 * 
	 * @param list
	 *            节点集合
	 * @param pId
	 *            当前节点父ID
	 * @param updatePersonId
	 *            更改人
	 * @throws Exception
	 */
	@Override
	public void updateSortControlGuide(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception {
		try {
			String hql = "from JecnControlGuide where parentId = ? order by sort";
			List<JecnControlGuide> listJecnControlGuides = jecnControlGuideDao.listHql(hql, pId);
			List<JecnControlGuide> listUpdateJecnControlGuides = new ArrayList<JecnControlGuide>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (JecnControlGuide jecnControlGuide : listJecnControlGuides) {
					if (jecnTreeDragBean.getId().equals(jecnControlGuide.getId())) {
						if (jecnControlGuide.getSort() == 0
								|| jecnTreeDragBean.getSortId() != jecnControlGuide.getSort()) {
							jecnControlGuide.setSort(jecnTreeDragBean.getSortId());
							listUpdateJecnControlGuides.add(jecnControlGuide);
							break;
						}
					}
				}
			}
			for (JecnControlGuide jecnControlGuide : listUpdateJecnControlGuides) {
				jecnControlGuide.setUpdateTime(new Date());
				jecnControlGuide.setUpdatePerson(updatePersonId);
				jecnControlGuideDao.update(jecnControlGuide);
				hql = "update JecnControlGuide set sort = ? where id = ? order by sort";
				jecnControlGuideDao.execteHql(hql, jecnControlGuide.getSort(), jecnControlGuide.getId());
				JecnUtil.saveJecnJournal(jecnControlGuide.getId(), jecnControlGuide.getName(), 18, 5, updatePersonId,
						jecnControlGuideDao);
			}
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl内控指引节点排序出错", e);
			throw e;
		}
	}

	/**
	 * 根据名称查询内控指引知识库条款
	 * 
	 * @param name
	 *            内控指引条款名称
	 * @return JecnControlGuide集合
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getControlGuideByName(String name) throws Exception {
		try {
			String sql = "select jcg.id,jcg.parent_id,jcg.name,jcg.DESCRIPTION from JECN_CONTROL_GUIDE jcg "
					+ "where jcg.name like ? " + "and jcg.type = 1 order by jcg.ID, jcg.PARENT_ID";
			List<Object[]> list = jecnControlGuideDao.listNativeSql(sql, "%" + name + "%");
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : list) {
				if (obj[0] == null || obj[1] == null || obj[2] == null) {
					continue;
				}
				JecnTreeBean jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
				jecnTreeBean.setPid(Long.valueOf(obj[1].toString()));
				jecnTreeBean.setName(obj[2].toString());
				// 内控指引知识库内容
				if (obj[3] != null) {
					jecnTreeBean.setContent(obj[3].toString());
				}
				listResult.add(jecnTreeBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl根据名称查询内控指引知识库条款出错", e);
		}
		return null;
	}

	/**
	 * 根据选中节点查询具体条款内容
	 */
	@Override
	public JecnControlGuide getJecnControlGuideById(Long id) throws Exception {
		try {
			String hql = "from JecnControlGuide where id = ?";
			JecnControlGuide jecnControlGuide = jecnControlGuideDao.getObjectHql(hql, id);
			return jecnControlGuide;
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl根据选中节点查询具体条款内容出错：", e);
		}
		return null;
	}

	/**
	 * 编辑知识库条款
	 */
	@Override
	public boolean updateJecnControlGuide(Long id, String name, String description, Long updatePerson, Date updateTime)
			throws Exception {
		try {
			String hql = "update JecnControlGuide set name = ?, description = ?, "
					+ "updatePerson = ?, updateTime = ? where id = ?";
			jecnControlGuideDao.execteHql(hql, name, description, updatePerson, updateTime, id);
			JecnUtil.saveJecnJournal(id, name, 18, 2, updatePerson, jecnControlGuideDao);
			return true;
		} catch (Exception e) {
			log.error("JecnControlGuideServiceImpl编辑知识库条款出错：", e);
		}
		return false;
	}

}
