package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class JecnAuthBean implements Serializable{
	/** 0是秘密，1是公开*/
	private int isPublic;
	/**岗位 权限*/
	private List<JecnTreeBean> listJecnTreeBeanPos;
	/**部门  权限*/
	private List<JecnTreeBean> listJecnTreeBeanOrg;
	public int getIsPublic() {
		return isPublic;
	}
	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}
	public List<JecnTreeBean> getListJecnTreeBeanPos() {
		return listJecnTreeBeanPos;
	}
	public void setListJecnTreeBeanPos(List<JecnTreeBean> listJecnTreeBeanPos) {
		this.listJecnTreeBeanPos = listJecnTreeBeanPos;
	}
	public List<JecnTreeBean> getListJecnTreeBeanOrg() {
		return listJecnTreeBeanOrg;
	}
	public void setListJecnTreeBeanOrg(List<JecnTreeBean> listJecnTreeBeanOrg) {
		this.listJecnTreeBeanOrg = listJecnTreeBeanOrg;
	}
}
