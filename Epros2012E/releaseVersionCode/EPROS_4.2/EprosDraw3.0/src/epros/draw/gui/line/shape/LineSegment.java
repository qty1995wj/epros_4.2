package epros.draw.gui.line.shape;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.gui.line.JecnBaseLineSegmentPanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnLine2D;
import epros.draw.gui.line.resize.JecnLineResizePanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

/**
 * 
 * 线条的小线段
 * 
 * @author ZHOUXY
 */
public class LineSegment extends JecnBaseLineSegmentPanel {
	/** 带箭头连接线的箭头 */
	private int arrowW = 0;
	/** 带箭头连接线的箭头 */
	private int arrowH = 0;
	/** 圆弧集合 */
	private List<DrawArc> drawArcList = new ArrayList<DrawArc>();
	/** 相交线中横线 */
	private List<LineSegment> lineSegmentX = new ArrayList<LineSegment>();
	/** 相交线中竖线 */
	private List<LineSegment> lineSegmentY = new ArrayList<LineSegment>();
	/** 记录相交线断的焦点 */
	private List<Point> linePointY = new ArrayList<Point>();

	/**
	 * 
	 * 创建小线段构造函数
	 * 
	 * @param manhattanLine
	 *            ManhattanLine 连接线
	 * @param lineSegmentData
	 *            JecnLineSegmentDataCloneable 小线段数据对象
	 */
	public LineSegment(JecnBaseManhattanLinePanel baseManhattanLine, JecnLineSegmentData lineSegmentData) {
		super(baseManhattanLine, lineSegmentData);
		initComponents();
	}

	private void initComponents() {
		// 选中点：开始点
		lineResizePanelStart = JecnLineResizePanel.createStartLineResizePanel(this);
		// 选中点：中间点
		lineResizePanelCenter = JecnLineResizePanel.createCenterLineResizePanel(this);
		// 鼠标移动到线段显示点，鼠标状态设置
		lineResizePanelCenter.setMouseCursor(lineSegmentData.getLineDirectionEnum());
		// 选中点：结束点
		lineResizePanelEnd = JecnLineResizePanel.createEndLineResizePanel(this);

		// 设置小线段位置点
		drawLineSegment(getStartX(), getStartY(), getEndX(), getEndY());
		
		setMouseCursor();
	}

	public void setMouseCursor() {
		this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
	}

	/**
	 * 小线段开始点X坐标
	 * 
	 * @return
	 */
	public int getStartX() {
		return DrawCommon.convertDoubleToInt(lineSegmentData.getOriginalStartXYData().getX());
	}

	public int getStartY() {
		return DrawCommon.convertDoubleToInt(lineSegmentData.getOriginalStartXYData().getY());
	}

	public int getEndX() {
		return DrawCommon.convertDoubleToInt(lineSegmentData.getOriginalEndXYData().getX());
	}

	public int getEndY() {
		return DrawCommon.convertDoubleToInt(lineSegmentData.getOriginalEndXYData().getY());
	}

	/**
	 * 
	 * @param newLine
	 *            小线段
	 * @param startX
	 *            开始点的横坐标
	 * @param startY
	 *            开始点的纵坐标
	 * @param endX
	 *            结束点的横坐标
	 * @param endY
	 *            结束点的纵坐标
	 */
	public void drawLineSegment(int startX, int startY, int endX, int endY) {
		// 如果开始点和结束点的横坐标相等，则线段为竖线
		int lineSizeWidth = this.getBaseManhattanLine().getFlowElementData().getBodyWidth();
		if (startX == endX) {// 纵线
			// 设置位置
			this.setBounds(startX - lineSizeWidth / 4, Math.min(startY, endY) - lineSizeWidth / 4, lineSizeWidth, Math
					.abs(startY - endY)
					+ lineSizeWidth / 2 + 1);
		}
		// 如果开始点和结束点的纵坐标相等，则线段为横向
		else if (startY == endY) {// 横线
			// 设置位置
			this.setBounds(Math.min(startX, endX), endY - lineSizeWidth / 4, Math.abs(startX - endX) + lineSizeWidth
					/ 4, lineSizeWidth);
		}
		// 设置线段背景颜色
	}

	@Override
	public void paintComponent(Graphics g) {
		// 创建line2D对象
		JecnLine2D line2D = new JecnLine2D(this.getZoomSPoint(), this.getZoomEPoint(), this.getLineSegmentData()
				.getLineDirectionEnum());
		line2D.setBodyColor(this.getFillColor());
		line2D.setStroke(this.getBasicStroke());
		line2D.paint(g);
	}

	/**
	 * 获取小线段原始状态开始点
	 * 
	 * @return
	 */
	public Point getStartPoint() {
		return this.lineSegmentData.getOriginalStartXYData().getPoint();
	}

	/**
	 * 当前面板放大缩小倍数下
	 * 
	 * @return
	 */
	public Point getZoomSPoint() {
		// 获取面板当前放大缩小倍数
		double scale = baseManhattanLine.getFlowElementData().getWorkflowScale();
		return new Point(DrawCommon.convertDoubleToInt(getStartPoint().getX() * scale), DrawCommon
				.convertDoubleToInt(getStartPoint().getY() * scale));
	}

	/**
	 * 当前面板放大缩小倍数下
	 * 
	 * @return
	 */
	public Point getZoomEPoint() {
		// 获取面板当前放大缩小倍数
		double scale = baseManhattanLine.getFlowElementData().getWorkflowScale();
		return new Point(DrawCommon.convertDoubleToInt(getEndPoint().getX() * scale), DrawCommon
				.convertDoubleToInt(getEndPoint().getY() * scale));
	}

	/**
	 * 画弧
	 * 
	 * 
	 * @param line
	 */
	public void drawArc(LineSegment line) {
		if (line == null || JecnDrawMainPanel.getMainPanel() == null
				|| JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 当前面板
		JecnDrawDesktopPane curWorkflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 记录面板所有小线段的集合
		List<LineSegment> allLineList = new ArrayList<LineSegment>();
		// 获取面板所有的连接线
		List<JecnBaseManhattanLinePanel> manhattanLineList = curWorkflow.getManLineList();
		for (int i = 0; i < manhattanLineList.size(); i++) {// 得到所有小线段
			allLineList.addAll(manhattanLineList.get(i).getLineSegments());
		}
		switch (line.getLineSegmentData().getLineDirectionEnum()) {
		case horizontal:// 当前线段为横线，获取相交线为竖线的交点 并画弧
			drawArcByHorLine(curWorkflow, allLineList, line);
			break;
		case vertical:// 当前线段为竖线，获取相交线为横线的交点 并画弧
			drawArcByVerLine(curWorkflow, allLineList, line);
			break;
		}
		curWorkflow.updateUI();
	}

	/**
	 * 当前线段为竖线，获取相交线为横线的交点 并画弧
	 * 
	 * @param curWorkflow
	 *            面板
	 * @param allLineList
	 *            面板所有的小线段
	 * @param verLine
	 *            当前线为竖线
	 */
	public void drawArcByVerLine(JecnDrawDesktopPane curWorkflow, List<LineSegment> allLineList, LineSegment verLine) {
		for (int i = 0; i < allLineList.size(); i++) {
			LineSegment horLine = allLineList.get(i);
			// 横线
			if (LineDirectionEnum.horizontal.equals(horLine.lineSegmentData.getLineDirectionEnum())) {
				// 根据横线和竖线获取弧的交点并画弧
				drawArcByVHLine(curWorkflow, horLine, verLine);
			}
		}
	}

	/**
	 * 当前线段为横线，获取相交线为竖线的交点 并画弧
	 * 
	 * @param curWorkflow
	 * @param allLineList
	 *            面板所有的小线段集合
	 * @param horLine
	 *            当前线段
	 */
	public void drawArcByHorLine(JecnDrawDesktopPane curWorkflow, List<LineSegment> allLineList, LineSegment horLine) {
		// 横线
		for (int i = 0; i < allLineList.size(); i++) {
			LineSegment verLine = allLineList.get(i);
			// 竖线
			if (LineDirectionEnum.vertical.equals(verLine.lineSegmentData.getLineDirectionEnum())) {
				// 根据横线和竖线获取弧的交点并画弧
				drawArcByVHLine(curWorkflow, horLine, verLine);
			}
		}
	}

	/**
	 * 根据横线和竖线获取弧的交点并画弧
	 * 
	 * @param curWorkflow
	 *            面板
	 * @param horLine
	 *            横线
	 * @param verLine
	 *            纵线
	 */
	private void drawArcByVHLine(JecnDrawDesktopPane curWorkflow, LineSegment horLine, LineSegment verLine) {
		// 相交线焦点
		Point point = getPointOfIntersection(horLine, verLine);
		if (point == null) {
			return;
		}
		if (verLine.baseManhattanLine.getFlowElementData().getBodyWidth() == 3) {
			point = new Point(point.x + 1, point.y);
		}
		// 获取圆弧
		DrawArc drawArc = new DrawArc(horLine, verLine);
		// 设置圆弧中心点
		drawArc.setCenterPoint(point);
		// 设置圆弧大小，位置点
		getDrawArc(drawArc, point, horLine.baseManhattanLine.getFlowElementData().getBodyWidth(),
				verLine.baseManhattanLine.getFlowElementData().getBodyWidth());

		if (!horLine.getDrawArcList().contains(drawArc)) {
			// 横线记录圆弧集合
			horLine.getDrawArcList().add(drawArc);
			// 竖线添加相交的横线
			horLine.getLineSegmentY().add(verLine);
			// 竖线记录圆弧集合
			verLine.getDrawArcList().add(drawArc);
			// 竖线记录和横线相交的中心点
			verLine.linePointY.add(point);
		}
		// 竖线记录相交的横线集合
		verLine.getLineSegmentX().add(horLine);
		// 设置线段和弧线的层级关系

		// 添加圆弧到面板
		curWorkflow.add(drawArc);
		int layerX = horLine.getBaseManhattanLine().getFlowElementData().getZOrderIndex();
		int layerY = verLine.getBaseManhattanLine().getFlowElementData().getZOrderIndex();
		if (layerX >= layerY) {
			curWorkflow.setLayer(drawArc, layerX + 1);
		} else if (layerX <= layerY) {
			curWorkflow.setLayer(drawArc, layerY + 1);
		}
		curWorkflow.updateUI();
	}

	/**
	 * 根据线段的线条大小设置弧的位置点 大小
	 * 
	 * @param drawArc
	 *            待添加的圆弧
	 * @param point
	 *            圆弧中心点
	 * @param horizontalLine
	 *            横线
	 * @param verticalLine
	 *            纵线
	 */
	public void getDrawArc(DrawArc drawArc, Point point, int horLineWidth, int verLineWidth) {
		if (horLineWidth == 1) {
			drawArc.setSize(new Dimension(12, 7));
			if (horLineWidth % 2 == 0) {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2 + 1, point.y - drawArc.getHeight() + 1);
			} else {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2, point.y - drawArc.getHeight() + 1);
			}
		}
		if (horLineWidth == 2) {
			drawArc.setSize(new Dimension(12, 7));
			if (verLineWidth == 3) {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2, point.y - drawArc.getHeight() + 2);
			}
			if (verLineWidth == 1) {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2, point.y - drawArc.getHeight() + 2);
			} else {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2 + 1, point.y - drawArc.getHeight() + 2);
			}
		}
		if (horLineWidth == 3) {
			drawArc.setSize(new Dimension(14, 9));
			if (verLineWidth == 2) {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2, point.y - drawArc.getHeight() + 3);
			}
			if (verLineWidth == 1) {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2, point.y - drawArc.getHeight() + 3);
			} else {
				drawArc.setLocation(point.x - drawArc.getWidth() / 2 + 1, point.y - drawArc.getHeight() + 3);
			}
		} else {
			// drawArc.setSize(new Dimension(12 + horLineWidth, 7 +
			// horLineWidth));
			// drawArc.setLocation(point.x - drawArc.getWidth() / 2 + 1, point.y
			// - drawArc.getHeight() + 3);
		}
	}

	/**
	 * 
	 * 得到两条互相垂直线段的交点
	 * 
	 * @param horizontalLine
	 *            横线
	 * @param verticalLine
	 *            竖线
	 * @return 横线和竖线的交点
	 */
	private Point getPointOfIntersection(LineSegment horizontalLine, LineSegment verticalLine) {
		if (horizontalLine.baseManhattanLine == verticalLine.baseManhattanLine) {// 若相交线为同一条曼哈顿线
			return null;
		}
		Point point = null;

		// 横线和竖线相交 交点坐标为竖线的X坐标和横线的Y坐标
		point = new Point(verticalLine.getZoomSPoint().x, horizontalLine.getZoomSPoint().y);// 交点
		// 判断交点是否在水平线上
		if (horizontalLine.getZoomSPoint().x > horizontalLine.getZoomEPoint().x) {
			if (horizontalLine.getZoomEPoint().x < point.x && point.x < horizontalLine.getZoomSPoint().x) {// 交点坐标必须在横线上，X坐标在横线长度范围内
				if (verticalLine.getZoomSPoint().y < verticalLine.getZoomEPoint().y) {
					if (verticalLine.getZoomSPoint().y < point.y && point.y < verticalLine.getZoomEPoint().y) {
						// 满足条件 交点存在
						return point;
					}
				}
				if (verticalLine.getZoomSPoint().y > verticalLine.getZoomEPoint().y) {
					if (verticalLine.getZoomEPoint().y < point.y && point.y < verticalLine.getZoomSPoint().y) {
						// 满足条件 交点存在
						return point;
					}
				}
			}
		} else if (horizontalLine.getZoomSPoint().x < horizontalLine.getZoomEPoint().x) {
			if (horizontalLine.getZoomSPoint().x < point.x && point.x < horizontalLine.getZoomEPoint().x) {
				if (verticalLine.getZoomSPoint().y < verticalLine.getZoomEPoint().y) {
					if (verticalLine.getZoomSPoint().y < point.y && point.y < verticalLine.getZoomEPoint().y) {
						// 满足条件 交点存在
						return point;
					}
				}
				if (verticalLine.getZoomSPoint().y > verticalLine.getZoomEPoint().y) {
					if (verticalLine.getZoomEPoint().y < point.y && point.y < verticalLine.getZoomSPoint().y) {
						// 满足条件 交点存在
						return point;
					}
				}
			}
		}
		// 满足条件 交点存在 返回空值
		return null;
	}

	/**
	 * 
	 * 判断是否存在圆弧,存在则删除
	 * 
	 * @param curWorkflow
	 *            面板
	 * @param lineSegment
	 *            当前线段
	 */

	public void removeDrawArc(LineSegment lineSegment) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || lineSegment == null) {
			return;
		}
		JecnDrawDesktopPane curWorkflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 记录线段对应的圆弧
		List<DrawArc> tmpVHList = new ArrayList<DrawArc>();
		// 和当前线段相交的线段
		List<LineSegment> refListLine = null;

		for (int k = 0; k < lineSegment.getDrawArcList().size(); k++) {
			// 画板删除弧形
			curWorkflow.remove(lineSegment.getDrawArcList().get(k));
			tmpVHList.add(lineSegment.getDrawArcList().get(k));
		}
		if (tmpVHList.size() == 0) {// 当前线段不存在圆弧
			return;
		}

		// 线段对应的弧
		lineSegment.getDrawArcList().clear();
		switch (lineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:// 横线
			refListLine = lineSegment.getLineSegmentY();
			break;
		case vertical:// 竖线
			// 获取竖线线交的横线
			refListLine = lineSegment.getLineSegmentX();
			break;
		}
		if (refListLine == null || refListLine.size() == 0) {
			return;
		}
		for (int i = 0; i < refListLine.size(); i++) {
			LineSegment vhLine = refListLine.get(i);
			if (vhLine.getDrawArcList().size() == 0) {
				continue;
			}
			// 删除关联线段中存在要删除的圆弧
			vhLine.getDrawArcList().removeAll(tmpVHList);
		}
		lineSegment.getDrawArcList().clear();
	}

	/**
	 * 获取小线段结束点
	 * 
	 * @return
	 */
	public Point getEndPoint() {
		return this.lineSegmentData.getOriginalEndXYData().getPoint();
	}

	public void setArrowW(int arrowW) {
		this.arrowW = arrowW;
	}

	public void setArrowH(int arrowH) {
		this.arrowH = arrowH;
	}

	/**
	 * 线段是否包含传入坐标
	 * 
	 * @param px
	 * @param py
	 * @return
	 */
	public boolean isContainsPort(int px, int py) {
		if ((this.getStartPoint().x == px && this.getStartPoint().y == py)
				|| (this.getEndPoint().x == px && this.getEndPoint().y == py)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 备份对象的部分属性
	 * 
	 */
	public LineSegment clone() {
		LineSegment lineSegment = new LineSegment(baseManhattanLine, lineSegmentData.clone());
		return lineSegment;
	}

	public List<DrawArc> getDrawArcList() {
		return drawArcList;
	}

	public List<LineSegment> getLineSegmentX() {
		return lineSegmentX;
	}

	public List<LineSegment> getLineSegmentY() {
		return lineSegmentY;
	}

	public List<Point> getLinePointY() {
		return linePointY;
	}

	public int getArrowW() {
		return arrowW;
	}

	public int getArrowH() {
		return arrowH;
	}
}
