package com.jecn.epros.server.action.web.login.ad.yuexiu;

import java.util.ResourceBundle;

/**
 * 烽火通信
 * 
 */
public class YueXiuAfterItem {

	private static ResourceBundle yuexiuConfig;

	static {
		yuexiuConfig = ResourceBundle.getBundle("cfgFile/yuexiu/yuexiuConfig");
	}

	public static String getValue(String key) {
		return yuexiuConfig.getString(key);
	}

}
