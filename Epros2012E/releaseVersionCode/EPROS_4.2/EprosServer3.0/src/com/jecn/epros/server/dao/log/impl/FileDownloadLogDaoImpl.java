package com.jecn.epros.server.dao.log.impl;

import com.jecn.epros.server.bean.log.FileDownloadLog;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.log.IFileDownloadLogDao;

public class FileDownloadLogDaoImpl extends AbsBaseDao<FileDownloadLog, Long>
		implements IFileDownloadLogDao {

}
