
package com.jecn.webservice.zhongsh.risk.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�����ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchTaskListResult" type="{http://tempuri.org/}ArrayOfTask" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchTaskListResult"
})
@XmlRootElement(name = "SearchTaskListResponse")
public class SearchTaskListResponse {

    @XmlElement(name = "SearchTaskListResult")
    protected ArrayOfTask searchTaskListResult;

    /**
     * ��ȡsearchTaskListResult���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTask }
     *     
     */
    public ArrayOfTask getSearchTaskListResult() {
        return searchTaskListResult;
    }

    /**
     * ����searchTaskListResult���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTask }
     *     
     */
    public void setSearchTaskListResult(ArrayOfTask value) {
        this.searchTaskListResult = value;
    }

}
