package epros.draw.gui.swing.textfield;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretListener;

import epros.draw.gui.swing.JecnToolbarButton;
import epros.draw.gui.swing.calendar.JecnCalendarDialog;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 带有时间按钮的输入框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTextFieldAndCalendarPanel extends JPanel implements ActionListener, MouseListener {
	/** 时间格式 */
	protected String dateFormat = "yyyy-MM-dd";

	/** 输入框 */
	protected JTextField textFlied = null;
	/** 时间按钮 */
	protected JecnToolbarButton toolbarButton = null;

	/** 输入框 */
	protected String oldText = "";

	public JecnTextFieldAndCalendarPanel() {
		initComponents();
	}

	public JecnTextFieldAndCalendarPanel(Date date) {
		this();
		setTextByDate(date);
	}

	private void initComponents() {
		// 输入框
		textFlied = new JTextField();
		// 时间按钮
		toolbarButton = new JecnToolbarButton();

		this.setBorder(JecnUIUtil.getTootBarBorder());
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new BorderLayout(2, 2));

		textFlied.setBorder(null);
		textFlied.setOpaque(false);
		textFlied.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		textFlied.setEditable(false);

		toolbarButton.setIcon(JecnFileUtil.getImageIconforPath("/epros/draw/images/swing/textfield/calendar.gif"));

		toolbarButton.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("dialogTitle"));
		textFlied.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("dialogTitle"));

		toolbarButton.addActionListener(this);
		toolbarButton.addMouseListener(this);
		textFlied.addMouseListener(this);

		this.add(textFlied, BorderLayout.CENTER);
		this.add(toolbarButton.getJToolBar(), BorderLayout.EAST);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (e.getSource() instanceof JecnToolbarButton) {
			toolbarButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		} else if (e.getSource() instanceof JTextField) {
			textFlied.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (e.getSource() instanceof JecnToolbarButton) {
			toolbarButton.setCursor(Cursor.getDefaultCursor());
		} else if (e.getSource() instanceof JTextField) {
			textFlied.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getSource() instanceof JTextField) {
			eventProcess();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnToolbarButton) {
			eventProcess();
		}
	}

	/**
	 * 
	 * 点击日期按钮或输入框弹出日历组件,选中一个日期确定处理
	 * 
	 */
	protected void eventProcess() {
		JecnCalendarDialog calendarDialog = null;

		String text = textFlied.getText();
		if (!DrawCommon.isNullOrEmtryTrim(text)) {
			try {
				SimpleDateFormat dateformat = new SimpleDateFormat(dateFormat);
				Date date = dateformat.parse(text);
				calendarDialog = new JecnCalendarDialog(date);
			} catch (Exception ex) {
				calendarDialog = new JecnCalendarDialog();
			}
		} else {
			calendarDialog = new JecnCalendarDialog();
		}

		calendarDialog.setVisible(true);

		boolean isOk = calendarDialog.isOk();

		if (isOk) {
			textFlied.setText(calendarDialog.getCurrSelectedData());
		}
	}

	public String getText() {
		return textFlied.getText();
	}

	public void setText(String t) {
		this.textFlied.setText(t);
	}

	public String getOldText() {
		return oldText;
	}

	public void setOldText(String oldText) {
		this.oldText = oldText;
	}

	public boolean isUpdate() {
		if (!oldText.equals(textFlied.getText())) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 给定日期赋值给输入框
	 * 
	 * @param date
	 */
	protected void setTextByDate(Date date) {
		if (date != null) {
			try {
				SimpleDateFormat dateformat = new SimpleDateFormat(dateFormat);
				this.textFlied.setText(dateformat.format(date));
			} catch (Exception ex) {
			}
		}
	}

	public void addCaretListener(CaretListener listener) {
		this.textFlied.addCaretListener(listener);
	}

	public void setEditable(boolean b) {
		this.textFlied.setEditable(b);
	}
}
