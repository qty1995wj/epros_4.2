package com.jecn.epros.mapper;

import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.sqlprovider.CommonSqlProvider;
import com.jecn.epros.sqlprovider.ConfigItemSqlProvider;
import com.jecn.epros.sqlprovider.FileSqlProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

public interface DictionaryMapper {


    @Select("SELECT * FROM JECN_DICTIONARY WHERE VALIDABLE = 'y' ")
    @ResultMap("mapper.Config.dictionary")
    List<Dictionary> fetchAllDictionary();

    @SelectProvider(type = CommonSqlProvider.class, method = "fetchDictionary")
    @ResultMap("mapper.Config.dictionary")
    List<Dictionary> fetchDictionary();

    @SelectProvider(type = CommonSqlProvider.class, method = "getDictionaryItem")
    List<String> getDictionaryItem();

    @SelectProvider(type = CommonSqlProvider.class, method = "getDictionaryItemByType")
    Dictionary getDictionaryItemByType(Map<String,Object> map);

    @SelectProvider(type = CommonSqlProvider.class, method = "getDictionaryItemByName")
    List<Dictionary> getDictionaryItemByName(Map<String,Object> map);

    @Update("UPDATE JECN_DICTIONARY SET VALUE = #{value,jdbcType=VARCHAR},EN_VALUE = #{enValue,jdbcType=VARCHAR} ,VALIDABLE = #{validable,jdbcType=VARCHAR}, " +
            "parameter = #{parameter},parameterType = #{parameterType} WHERE NAME = #{name} AND CODE = #{code}")
    int updateDictionaryItem(Map<String, Object> paramMap);

    @Insert("INSERT INTO jecn_dictionary(ID,NAME,TRANS_NAME,CODE,VALUE,company,validable,EN_VALUE,parameter,parametertype,EN_TRANS_NAME) " +
            "VALUES(#{id},#{name},#{transName,jdbcType=VARCHAR},#{code,jdbcType=VARCHAR},#{value,jdbcType=VARCHAR},#{company,jdbcType=VARCHAR},#{validable,jdbcType=VARCHAR},#{enValue,jdbcType=VARCHAR},#{parameter,jdbcType=VARCHAR},#{parameterType,jdbcType=VARCHAR},#{enTransName,jdbcType=VARCHAR})")
    int insertDictionaryItem(Map<String, Object> paramMap);
}
