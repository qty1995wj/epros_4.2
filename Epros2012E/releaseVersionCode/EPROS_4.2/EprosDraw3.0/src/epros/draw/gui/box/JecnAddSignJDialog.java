package epros.draw.gui.box;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 添加图形元素
 * 
 * @author fuzhh
 * 
 */
public class JecnAddSignJDialog extends JecnDialog implements ActionListener {
	/** 资源文件管理对象 */
	private JecnResourceUtil resourceManager = null;
	/** 未添加的按钮集合：流程元素库不需要显示按钮 */
	private List<JecnButton> noAddList = new ArrayList<JecnButton>();
	/** 已经添加的按钮集合:流程元素库需要显示的按钮 */
	private List<JecnButton> addedList = new ArrayList<JecnButton>();

	/** 按钮所在面板 */
	private JecnToolBoxAbstractScrollPane toolBoxScrollPane = null;

	/** 主面板 */
	private JPanel mainJpanel = null;
	/** 还未添加符号 */
	private JPanel noAddPanel = null;

	/** 已添加符号 */
	private JPanel addedPanel = null;
	private JScrollPane addedScrollPanel = null;

	/** 确认取消面板 */
	private JecnBoxOKCancelPanel okCancelPanel = null;

	/** JecnButton按钮大小 */
	private Dimension sizeButton = new Dimension(70, 60);
	/** 本地Properties文件显示流程元素 */
	private List<MapElemType> showTypeList = null;

	public JecnAddSignJDialog(JecnToolBoxAbstractScrollPane toolBoxScrollPane, List<MapElemType> showTypeList) {
		if (toolBoxScrollPane == null || showTypeList == null) {
			JecnLogConstants.LOG_TOOLBOX_PANEL.error("JecnAddSignJDialog类构造函数：参数为null或“”.toolBoxScrollPane="
					+ toolBoxScrollPane + ";showTypeList=" + showTypeList);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolBoxScrollPane = toolBoxScrollPane;
		this.showTypeList = showTypeList;
		// 初始化组件
		initComponents();
		// 布局
		iniLayout();
		// 初始化数据
		initData();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 实例化资源文件管理对象
		resourceManager = JecnResourceUtil.getJecnResourceUtil();

		// 初始化主Panel
		mainJpanel = new JPanel();

		// 还未添加符号面板
		noAddPanel = new JPanel();

		Dimension addPpreferredSize = new Dimension(500, 450);

		// 已添加符号
		addedPanel = new JPanel();
		addedScrollPanel = new JScrollPane();
		addedScrollPanel.setViewportView(addedPanel);
		addedScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		addedScrollPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		addedScrollPanel.setPreferredSize(addPpreferredSize);
		addedPanel.setPreferredSize(addPpreferredSize);
		addedScrollPanel.setBorder(null);
		// 鼠标滚动大小
		addedScrollPanel.getHorizontalScrollBar().setUnitIncrement(30);
		addedScrollPanel.getVerticalScrollBar().setUnitIncrement(30);

		// 确认取消面板
		okCancelPanel = new JecnBoxOKCancelPanel();

		// 设置不可调整大小
		// this.setResizable(false);
		// 模态
		this.setModal(true);
		// 标题
		this.setTitle(getResourceManager().getValue("addingSymbols"));

		// ******************主面板******************//
		// 布局管理器
		mainJpanel.setLayout(new BorderLayout());
		// 边框
		mainJpanel.setBorder(null);
		// 默认背景色
		mainJpanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// ******************主面板******************//

		// ******************添加符号******************//
		// 边框
		noAddPanel.setBorder(BorderFactory.createTitledBorder(getResourceManager().getValue("availableSymbo")));

		// 布局管理器
		noAddPanel.setLayout(new FlowLayout());
		// 设置背景色
		noAddPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// ******************添加符号******************//

		// ******************已添加符号******************//
		// 布局管理器
		addedPanel.setLayout(new FlowLayout());
		// 边框
		addedPanel.setBorder(BorderFactory.createTitledBorder(getResourceManager().getValue("alreadyAddingSymbols")));
		// 设置背景色
		addedPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// ******************已添加符号******************//

		// 事件
		okCancelPanel.getOkBtn().addActionListener(this);
		okCancelPanel.getCancelBtn().addActionListener(this);
	}

	/**
	 * 
	 * 布局
	 * 
	 */
	private void iniLayout() {
		// 主面板
		this.getContentPane().add(mainJpanel);

		JPanel contPanel = new JPanel();
		contPanel.setLayout(new GridLayout(2, 1));
		contPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加符号
		contPanel.add(noAddPanel);
		// 已添加符号
		contPanel.add(addedScrollPanel);

		mainJpanel.add(contPanel, BorderLayout.CENTER);
		// 确定取消面板
		mainJpanel.add(okCancelPanel, BorderLayout.SOUTH);
	}

	/**
	 * 
	 * 初始化数据
	 * 
	 */
	private void initData() {
		if (JecnDesignerProcess.getDesignerProcess().getDesigner() == null) {
			// 不常用线
			addButtons(toolBoxScrollPane.getGenLineTypeArray());
			// 不常用图形
			addButtons(toolBoxScrollPane.getGenFigureTypeArray());
		} else {
			addDataBaseButtons();
		}
	}

	/**
	 * 
	 * 添加不常用流程元素
	 * 
	 * @param genTypeArray
	 *            MapElemType[]不常用流程元素
	 */
	private void addButtons(MapElemType[] genTypeArray) {
		if (genTypeArray == null) {
			return;
		}

		for (MapElemType mapElemType : genTypeArray) {// 不常用流程元素
			// 按钮图标
			JecnButton btn = new JecnButton(mapElemType);
			if (showTypeList.contains(mapElemType)) {// 已经添加到流程元素库中显示了
				// 添加到集合
				addedList.add(btn);
				// 添加到面板
				this.addedPanel.add(btn);
			} else {
				// 添加到集合
				noAddList.add(btn);
				// 添加到面板
				this.noAddPanel.add(btn);
			}
		}
	}

	private void addDataBaseButtons() {
		List<MapElemType> listAll = JecnDesignerProcess.getDesignerProcess().getAllElementsType(getWorkType());
		for (MapElemType mapElemType : listAll) {// 不常用流程元素
			// 按钮图标
			JecnButton btn = new JecnButton(mapElemType);
			if (showTypeList.contains(mapElemType)) {// 已经添加到流程元素库中显示了
				// 添加到集合
				addedList.add(btn);
				// 添加到面板
				this.addedPanel.add(btn);
			} else {
				// 添加到集合
				noAddList.add(btn);
				// 添加到面板
				this.noAddPanel.add(btn);
			}
		}
	}

	private JecnResourceUtil getResourceManager() {
		return resourceManager;
	}

	private JecnResourceUtil getBoxPsanelResourceManager() {
		return this.toolBoxScrollPane.getToolBoxMainPanel().getResourceManager();
	}

	/**
	 * 
	 * 按钮事件处理
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnButton) {// 图标按钮
			JecnButton flowElemBtn = (JecnButton) e.getSource();
			reShowByAction(flowElemBtn);
		} else if (e.getSource() instanceof JButton) {
			// 按钮
			JButton button = (JButton) e.getSource();
			if (JecnActionCommandConstants.OK_BUTTON.equals(button.getActionCommand())) {// 确认按钮
				boolean flag = okActionPerformed();
				if (!flag) {
					return;
				}
				// 重新布局流程元素库
				JecnScrollPaneComponentAdapter scrollPaneComponentAdapter = new JecnScrollPaneComponentAdapter();
				scrollPaneComponentAdapter.setJecnToolBoxAbstractScrollPaneSize(toolBoxScrollPane);
				// 隐藏对话框
				this.setVisible(false);
			} else if (JecnActionCommandConstants.CANCEL_BUTTON.equals(button.getActionCommand())) {// 取消按钮
				this.setVisible(false);
			}
		}

	}

	/**
	 * 
	 * 确认按钮处理动作
	 * 
	 * @return boolean 处理成功：true；失败：false
	 */
	private boolean okActionPerformed() {
		// 1. 清除未添加的集合中在流程元素库已经添加的按钮
		// 2. 添加已经添加的集合添加到流程元素库，但要排除已经添加了流程元素
		// 3.更新缓存，写入本地Properties文件

		if (JecnDesignerProcess.getDesignerProcess().isDraw()) {// 单机版
			for (JecnButton btn : noAddList) {// 清除未添加的集合中在流程元素库已经添加的按钮
				toolBoxScrollPane.removeGenBtnToBtsPanel(btn.mapElemType);
			}
			// 流程元素库中没有且需要添加到流程元素库中的按钮添加到流程元素库以及其数据等
			for (JecnButton btn : addedList) { // 添加已经添加的集合添加到流程元素库，但要排除已经添加了流程元素
				this.toolBoxScrollPane.addGenBtnToAll(btn.mapElemType);
			}
			isLocalElementsTypeUpdate();
		} else {
			isDataBaseElemTypeUpdate();
		}
		toolBoxScrollPane.revalidate();
		toolBoxScrollPane.repaint();
		return true;
	}

	private void isDataBaseElemTypeUpdate() {
		int type = getWorkType();
		List<MapElemType> allElements = JecnDesignerProcess.getDesignerProcess().getAllElementsType(type);
		for (MapElemType elemType : allElements) {// 清除未添加的集合中在流程元素库已经添加的按钮
			toolBoxScrollPane.removeGenBtnToBtsPanel(elemType);
		}
		List<MapElemType> elemTypes = new ArrayList<MapElemType>();
		for (JecnButton btn : addedList) { // 添加已经添加的集合添加到流程元素库，但要排除已经添加了流程元素
			elemTypes.add(btn.mapElemType);
			this.toolBoxScrollPane.addGenBtnToAll(btn.mapElemType);
		}
		JecnDesignerProcess.getDesignerProcess().updateElementsType(elemTypes, type);
	}

	private int getWorkType() {
		int type = 0;
		if (toolBoxScrollPane instanceof JecnToolBoxPartScrollPane) {// 流程图
			type = 0;
		} else if (toolBoxScrollPane instanceof JecnToolBoxTotalScrollPane) {// 流程地图
			type = 1;
		} else if (toolBoxScrollPane instanceof JecnToolBoxOrgScrollPane) {// 组织图
			type = 2;
		}
		return type;
	}

	private boolean isLocalElementsTypeUpdate() {

		// 待写入本地Properties文件的集合
		List<MapElemType> sysList = new ArrayList<MapElemType>();
		// 获取已经添加到流程元素库中的不常用流程元素，目的写入本地文件
		List<JecnToolBoxButton> genflowElemList = this.toolBoxScrollPane.getToolBoxBean().getAllShowList();
		for (JecnToolBoxButton btn : genflowElemList) {
			if (btn != null) {
				sysList.add(btn.getBtnType());
			}
		}

		// toolBoxScrollPane.revalidate();
		// toolBoxScrollPane.repaint();

		showTypeList.clear();
		showTypeList.addAll(sysList);

		// 是否更新成功，true：成功；false：失败
		boolean updateFlag = false;
		if (toolBoxScrollPane instanceof JecnToolBoxPartScrollPane) {// 流程图
			updateFlag = JecnSystemData.writePartMapByPropertyFile();
		} else if (toolBoxScrollPane instanceof JecnToolBoxTotalScrollPane) {// 流程地图
			updateFlag = JecnSystemData.writeTotalMapToPropertyFile();
		} else if (toolBoxScrollPane instanceof JecnToolBoxOrgScrollPane) {// 组织图
			updateFlag = JecnSystemData.writeOrgMapToPropertyFile();
		} else {
			this.okCancelPanel.getFreeLabel().setText(ErrorInfoConstant.SYSTEM_PROCES_ERROR);
			return false;
		}
		if (!updateFlag) {// 失败
			this.okCancelPanel.getFreeLabel().setText(ErrorInfoConstant.UPDATE_ERROR);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 点击图标按钮，切换显示所在面板
	 * 
	 * @param flowElemBtn
	 *            JecnButton 图标按钮
	 */
	private void reShowByAction(JecnButton flowElemBtn) {
		if (noAddList.contains(flowElemBtn)) {// 在未添加面板中
			addedList.add(flowElemBtn);
			noAddList.remove(flowElemBtn);

			noAddPanel.remove(flowElemBtn);
			addedPanel.add(flowElemBtn);
		} else if (addedList.contains(flowElemBtn)) {// 在已经添加面板中
			noAddList.add(flowElemBtn);
			addedList.remove(flowElemBtn);

			addedPanel.remove(flowElemBtn);
			noAddPanel.add(flowElemBtn);
		}
		this.mainJpanel.revalidate();
		this.mainJpanel.repaint();

	}

	/**
	 * 
	 * 按钮
	 * 
	 * @author ZHOUXY
	 * 
	 */
	class JecnButton extends JButton {
		private MapElemType mapElemType = null;

		JecnButton(MapElemType mapElemType) {
			this.mapElemType = mapElemType;

			topButton();
		}

		/**
		 * 
		 * 图上内容下布局
		 * 
		 */
		private void topButton() {
			// 布局管理器
			this.setLayout(null);
			// 图片
			this.setIcon(JecnFileUtil.getToolBoxImageIcon(mapElemType.toString()));

			// 内容
			String text = getBoxPsanelResourceManager().getValue(mapElemType);
			this.setText(text);
			// 边框
			this.setBorder(null);
			// 提示信息
			this.setToolTipText(text);

			// 大小
			this.setPreferredSize(sizeButton);
			// 最小大小
			this.setMinimumSize(sizeButton);

			// 图片内容显示方式：居中
			this.setHorizontalAlignment(SwingConstants.CENTER);
			// 上下分布
			this.setHorizontalTextPosition(SwingConstants.CENTER);
			this.setVerticalTextPosition(SwingConstants.BOTTOM);

			// 事件
			this.addActionListener(JecnAddSignJDialog.this);
		}
	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		showTypeList = null;
		noAddList = null;
		addedList = null;
		toolBoxScrollPane = null;
	}
}
