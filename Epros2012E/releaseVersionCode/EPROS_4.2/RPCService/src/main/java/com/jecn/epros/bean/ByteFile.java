package com.jecn.epros.bean;

import java.io.Serializable;

public class ByteFile implements Serializable {
	// 文件流
	private byte[] bytes;
	// 文件名称
	private String fileName;

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
