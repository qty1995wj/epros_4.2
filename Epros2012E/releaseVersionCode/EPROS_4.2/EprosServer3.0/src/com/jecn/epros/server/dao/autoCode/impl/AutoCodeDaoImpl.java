package com.jecn.epros.server.dao.autoCode.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.autoCode.ProcessCodeTree;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.autoCode.IAutoCodeDao;

public class AutoCodeDaoImpl extends AbsBaseDao<AutoCodeBean, String> implements IAutoCodeDao {

	@Override
	public Long addPorcessCode(ProcessCodeTree codeTree) throws Exception {
		if (codeTree == null) {
			throw new IllegalArgumentException("创建过程代码，参数非法！codeTree = null");
		}
		Date curDate = new Date();
		codeTree.setCreateTime(curDate);
		codeTree.setUpdateTime(curDate);
		this.getSession().save(codeTree);
		if (StringUtils.isBlank(codeTree.gettPath())) {
			codeTree.settPath(codeTree.getId() + "-");
		} else {
			codeTree.settPath(codeTree.gettPath() + codeTree.getId() + "-");
		}
		codeTree.settLevel(codeTree.gettPath().split("-").length - 1);
		this.getSession().update(codeTree);
		return codeTree.getId();
	}

	/**
	 * 删除节点
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@Override
	public void deleteProcessCode(List<Long> ids) throws Exception {
		// T_PATH获取删除的ID集合
		String sql = JecnCommonSql.getChildObjectsSql("PROCESS_CODE_TREE", "ID", "PARENT_ID", null, ids, null);
		List<Object[]> listAll = this.listNativeSql(sql);
		List<Long> deleteIds = new ArrayList<Long>();
		for (Object[] obj : listAll) {
			if (obj[0] == null || obj[1] == null) {
				continue;
			}
			deleteIds.add(Long.valueOf(obj[0].toString()));
		}
		sql = "DELETE FROM PROCESS_CODE_TREE WHERE ID IN";
		List<String> listSql = JecnCommonSql.getListSqls(sql, deleteIds);
		for (String str : listSql) {
			this.execteNative(str);
		}
	}

	/**
	 * 重命名
	 * 
	 * @param id
	 * @param newName
	 * @param updatePersonId
	 * @throws Exception
	 */
	@Override
	public void updateName(Long id, String newName, Long updatePersonId) throws Exception {
		String sql = "UPDATE PROCESS_CODE_TREE SET NAME = ?,UPDATE_PERSON_ID = ?,UPDATE_TIME = "
				+ JecnCommonSql.getDateTimeByDB() + " WHERE ID = ?";
		this.execteNative(sql, newName, updatePersonId, id);
	}

	@Override
	public List<Object[]> getChildProcessCodes(Long pId, int treeType) throws Exception {
		String sql = "SELECT DISTINCT PC.Id," + "                PC.NAME," + "                PC.PARENT_ID,"
				+ "                PC.IS_DIR," + "                PC.SORT_ID," + "                PC.T_PATH,"
				+ "                PC.T_LEVEL," + "                CASE" + "                  WHEN CH.ID IS NULL THEN"
				+ "                   0" + "                  ELSE" + "                   1"
				+ "                END AS COUNT" + "  FROM PROCESS_CODE_TREE PC" + "  LEFT JOIN PROCESS_CODE_TREE CH"
				+ "    ON PC.ID = CH.PARENT_ID" + " WHERE PC.PARENT_ID = ? AND PC.TYPE = ?" + " ORDER BY PC.SORT_ID";
		return listNativeSql(sql, pId, treeType);
	}

	@Override
	public List<Object[]> getPnodes(Set<Long> set, int treeType) throws Exception {
		String sql = "SELECT DISTINCT PC.Id," + "                PC.NAME," + "                PC.PARENT_ID,"
				+ "                PC.IS_DIR," + "                PC.SORT_ID," + "                PC.T_PATH,"
				+ "                PC.T_LEVEL," + "                CASE" + "                  WHEN CH.ID IS NULL THEN"
				+ "                   0" + "                  ELSE" + "                   1"
				+ "                END AS COUNT" + "  FROM PROCESS_CODE_TREE PC" + "  LEFT JOIN PROCESS_CODE_TREE CH"
				+ "    ON PC.ID = CH.PARENT_ID" + " WHERE PC.PARENT_ID IN" + JecnCommonSql.getIdsSet(set)
				+ " AND PC.TYPE = ?" + " ORDER BY PC.SORT_ID";
		return listNativeSql(sql, treeType);
	}

	@Override
	public String getCodePath(Long id) throws Exception {
		String sql = "SELECT T.T_PATH FROM PROCESS_CODE_TREE T WHERE T.ID = " + id;
		List<String> list = this.listNativeSql(sql);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<Object[]> getTreeBeanByName(String name) throws Exception {
		String sql = "SELECT DISTINCT PC.ID," + "                PC.NAME," + "                PC.PARENT_ID,"
				+ "                PC.IS_DIR," + "                PC.SORT_ID," + "                PC.T_PATH,"
				+ "                PC.T_LEVEL," + "                CASE" + "                  WHEN CH.ID IS NULL THEN"
				+ "                   0" + "                  ELSE" + "                   1"
				+ "                END AS COUNT" + "  FROM PROCESS_CODE_TREE PC" + "  LEFT JOIN PROCESS_CODE_TREE CH"
				+ "    ON PC.ID = CH.PARENT_ID" + " WHERE PC.NAME LIKE'" + name + " %'";
		return listNativeSql(sql);
	}

	/**
	 * 根据文件类型、ID 更新对应编号规则下的最大流水号
	 * 
	 * @param relatedId
	 * @param relatedType
	 * @param codeTotal
	 * @throws Exception
	 */
	@Override
	public void updateAutoCodeTableByRelatedId(Long relatedId, int relatedType, int codeTotal) throws Exception {
		String sql = "UPDATE AUTO_CODE_TABLE" + "   SET CODE_TOTAL = ?" + " WHERE GUID = (SELECT AUTO_CODE_ID"
				+ "                 FROM AUTO_CODE_RELATED_TABLE" + "                WHERE RELATED_ID = ?"
				+ "                  AND RELATED_TYPE = ?)";
		this.execteNative(sql, codeTotal, relatedId, relatedType);
	}

	/***
	 * 标准自动编号 更新目录对应流水号
	 * 
	 * @param relatedId
	 * @param relatedType
	 * @param codeTotal
	 * @return
	 * @throws Exception
	 */
	@Override
	public int updateStandardizedAutoCode(Long relatedId, int relatedType, int codeTotal) throws Exception {
		relatedId = JecnCommonSql.specialAutoCodeNodeHandle(relatedId, relatedType);
		String sql = "UPDATE JECN_STANDARDIZED_AUTO_COD SET CODE_TOTAL = ? WHERE RELATED_TYPE = ? AND RELATED_ID = ?";
		return this.execteNative(sql, codeTotal, relatedType, relatedId);
	}
}
