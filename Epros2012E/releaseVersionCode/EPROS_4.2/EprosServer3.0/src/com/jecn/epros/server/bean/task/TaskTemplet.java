package com.jecn.epros.server.bean.task;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务模板
 * 
 * @author user
 * 
 */
public class TaskTemplet implements Serializable {

	private String id;
	/** 模板名称 **/
	private String name;
	private String eName;
	/** 任务类型 **/
	private int type;
	private Date createTime;
	private Long createPeople;
	private Date updateTime;
	private Long updatePeople;

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getName(int type) {
		return type == 0 ? this.name : this.eName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePeople() {
		return createPeople;
	}

	public void setCreatePeople(Long createPeople) {
		this.createPeople = createPeople;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getUpdatePeople() {
		return updatePeople;
	}

	public void setUpdatePeople(Long updatePeople) {
		this.updatePeople = updatePeople;
	}

}
