package com.jecn.webservice.crs.dataimport;

public class HrPublisherPortTypeProxy implements com.jecn.webservice.crs.dataimport.HrPublisherPortType {
  private String _endpoint = null;
  private com.jecn.webservice.crs.dataimport.HrPublisherPortType hrPublisherPortType = null;
  
  public HrPublisherPortTypeProxy() {
    _initHrPublisherPortTypeProxy();
  }
  
  public HrPublisherPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initHrPublisherPortTypeProxy();
  }
  
  private void _initHrPublisherPortTypeProxy() {
    try {
      hrPublisherPortType = (new com.jecn.webservice.crs.dataimport.HrPublisherLocator()).getHrPublisherHttpSoap11Endpoint();
      if (hrPublisherPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)hrPublisherPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)hrPublisherPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (hrPublisherPortType != null)
      ((javax.xml.rpc.Stub)hrPublisherPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherPortType() {
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType;
  }
  
  public com.jecn.webservice.crs.dataimport.BasePackage login(java.lang.String userName, java.lang.String password) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.login(userName, password);
  }
  
  public com.jecn.webservice.crs.dataimport.SummaryPackage getPOSITION_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getPOSITION_VIEWSummary(userName, password, versionId);
  }
  
  public com.jecn.webservice.crs.dataimport.SummaryPackage getORGANIZATION_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getORGANIZATION_VIEWSummary(userName, password, versionId);
  }
  
  public com.jecn.webservice.crs.dataimport.SummaryPackage getTB_INF_EMPLOYEESummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getTB_INF_EMPLOYEESummary(userName, password, versionId);
  }
  
  public com.jecn.webservice.crs.dataimport.SummaryPackage getTB_INF_EMPCONTACTSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getTB_INF_EMPCONTACTSummary(userName, password, versionId);
  }
  
  public com.jecn.webservice.crs.dataimport.SummaryPackage getTB_PER_EMPLOYEEJOBSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getTB_PER_EMPLOYEEJOBSummary(userName, password, versionId);
  }
  
  public com.jecn.webservice.crs.dataimport.SummaryPackage getUSER_POS_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getUSER_POS_VIEWSummary(userName, password, versionId);
  }
  
  public com.jecn.webservice.crs.dataimport.SummaryPackage getORG_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getORG_VIEWSummary(userName, password, versionId);
  }
  
  public com.jecn.webservice.crs.dataimport.POSITION_VIEWDataPackage getPOSITION_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getPOSITION_VIEWPackage(userName, password, versionId, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.ORGANIZATION_VIEWDataPackage getORGANIZATION_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getORGANIZATION_VIEWPackage(userName, password, versionId, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.TB_INF_EMPLOYEEDataPackage getTB_INF_EMPLOYEEPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getTB_INF_EMPLOYEEPackage(userName, password, versionId, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.TB_INF_EMPCONTACTDataPackage getTB_INF_EMPCONTACTPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getTB_INF_EMPCONTACTPackage(userName, password, versionId, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.TB_PER_EMPLOYEEJOBDataPackage getTB_PER_EMPLOYEEJOBPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getTB_PER_EMPLOYEEJOBPackage(userName, password, versionId, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.USER_POS_VIEWDataPackage getUSER_POS_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getUSER_POS_VIEWPackage(userName, password, versionId, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.ORG_VIEWDataPackage getORG_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.getORG_VIEWPackage(userName, password, versionId, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.BasePackage logout() throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.logout();
  }
  
  public com.jecn.webservice.crs.dataimport.POSITION_VIEWDataPackage queryPOSITION_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.queryPOSITION_VIEWData(userName, password, filter, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.ORGANIZATION_VIEWDataPackage queryORGANIZATION_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.queryORGANIZATION_VIEWData(userName, password, filter, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.TB_INF_EMPLOYEEDataPackage queryTB_INF_EMPLOYEEData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.queryTB_INF_EMPLOYEEData(userName, password, filter, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.TB_INF_EMPCONTACTDataPackage queryTB_INF_EMPCONTACTData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.queryTB_INF_EMPCONTACTData(userName, password, filter, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.TB_PER_EMPLOYEEJOBDataPackage queryTB_PER_EMPLOYEEJOBData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.queryTB_PER_EMPLOYEEJOBData(userName, password, filter, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.USER_POS_VIEWDataPackage queryUSER_POS_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.queryUSER_POS_VIEWData(userName, password, filter, pageNo);
  }
  
  public com.jecn.webservice.crs.dataimport.ORG_VIEWDataPackage queryORG_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException{
    if (hrPublisherPortType == null)
      _initHrPublisherPortTypeProxy();
    return hrPublisherPortType.queryORG_VIEWData(userName, password, filter, pageNo);
  }
  
  
}