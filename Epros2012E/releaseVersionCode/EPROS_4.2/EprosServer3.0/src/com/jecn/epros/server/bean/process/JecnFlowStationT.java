package com.jecn.epros.server.bean.process;

public class JecnFlowStationT implements java.io.Serializable {
	private Long flowStationId;// 主键ID
	private Long figureFlowId;// 对应角色（JecnFlowStructureImageT）的figureId
	private Long figurePositionId;// 对应岗位（JecnFlowOrgImage）的figureId或者对于岗位组的Id
	private String type;// 岗位为0,岗位组为1
	private String stationName;// 岗位或岗位组名称
	private String UUID;
	private String figureUUID;

	public Long getFlowStationId() {
		return flowStationId;
	}

	public void setFlowStationId(Long flowStationId) {
		this.flowStationId = flowStationId;
	}

	public Long getFigureFlowId() {
		return figureFlowId;
	}

	public void setFigureFlowId(Long figureFlowId) {
		this.figureFlowId = figureFlowId;
	}

	public Long getFigurePositionId() {
		return figurePositionId;
	}

	public void setFigurePositionId(Long figurePositionId) {
		this.figurePositionId = figurePositionId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
}
