package epros.designer.gui.system.config.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;

import epros.designer.gui.system.config.ui.property.type.JecnPropertySetPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/***
 * 属性配置按钮点击打开的显示框
 * @Time 2014-10-15
 *
 */
public class JecnPropertySetJDialog extends JecnDialog {
	/** 配置界面 */
	private JecnAbtractBaseConfigDialog dialog;
	/** 主面板 */
	private JecnPropertySetPanel propertySetPanel = null;
	public JecnPropertySetJDialog(JecnAbtractBaseConfigDialog dialog){
		super(dialog);
		this.dialog = dialog;
		//初始化
		initComponents();
	}

	protected void initComponents() {
		// 主面板
		propertySetPanel = new JecnPropertySetPanel(dialog, this);

		// 大小
		Dimension size = new Dimension(590, 460);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		// 模态
		this.setModal(true);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 关闭处理模式
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//
		this.setLocationRelativeTo(dialog);

		this.getContentPane().add(propertySetPanel, BorderLayout.CENTER);
	}

	public JecnPropertySetPanel getPropertySetPanel() {
		return propertySetPanel;
	}
	
}
