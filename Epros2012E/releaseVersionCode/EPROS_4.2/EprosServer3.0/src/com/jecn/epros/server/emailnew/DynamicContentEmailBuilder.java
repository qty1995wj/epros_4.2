package com.jecn.epros.server.emailnew;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;

public abstract class DynamicContentEmailBuilder extends BaseEmailBuilder {

	@Override
	public List<EmailBasicInfo> buildEmail() {
		List<EmailBasicInfo> emails = new ArrayList<EmailBasicInfo>();
		for (JecnUser user : getUsers()) {
			emails.add(getEmailBasicInfo(user));
		}
		return emails;
	}

	protected abstract EmailBasicInfo getEmailBasicInfo(JecnUser user);
}
