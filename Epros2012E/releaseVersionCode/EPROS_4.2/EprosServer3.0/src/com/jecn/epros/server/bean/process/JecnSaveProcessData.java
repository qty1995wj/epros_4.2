package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 创建流程-数据对象
 * 
 * @author xiaohu
 * 
 */
public class JecnSaveProcessData implements Serializable {
	private JecnFlowStructureT jecnFlowStructureT;
	private JecnFlowBasicInfoT jecnFlowBasicInfoT;
	private String posIds;
	private String orgIds;
	private String sptIds;
	private Long updatePersionId;
	private String posGroupIds;

	/** 自动编号 总数 */
	private int totalCode;

	public JecnFlowStructureT getJecnFlowStructureT() {
		return jecnFlowStructureT;
	}

	public void setJecnFlowStructureT(JecnFlowStructureT jecnFlowStructureT) {
		this.jecnFlowStructureT = jecnFlowStructureT;
	}

	public JecnFlowBasicInfoT getJecnFlowBasicInfoT() {
		return jecnFlowBasicInfoT;
	}

	public void setJecnFlowBasicInfoT(JecnFlowBasicInfoT jecnFlowBasicInfoT) {
		this.jecnFlowBasicInfoT = jecnFlowBasicInfoT;
	}

	public String getPosIds() {
		return posIds;
	}

	public void setPosIds(String posIds) {
		this.posIds = posIds;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public String getSptIds() {
		return sptIds;
	}

	public void setSptIds(String sptIds) {
		this.sptIds = sptIds;
	}

	public Long getUpdatePersionId() {
		return updatePersionId;
	}

	public void setUpdatePersionId(Long updatePersionId) {
		this.updatePersionId = updatePersionId;
	}

	public String getPosGroupIds() {
		return posGroupIds;
	}

	public void setPosGroupIds(String posGroupIds) {
		this.posGroupIds = posGroupIds;
	}

	public int getTotalCode() {
		return totalCode;
	}

	public void setTotalCode(int totalCode) {
		this.totalCode = totalCode;
	}
}
