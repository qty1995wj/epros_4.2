package epros.draw.data.line;

import java.awt.Point;

/**
 * 
 * 马哈顿算法生成小线段坐标记录类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnManhattanRouterResultData {
	/** 开始点 */
	private Point startPoint;
	/** 结束点 */
	private Point endPint;
	/** 是否为结束线段数据 */
	private boolean isEndLine;
	private String UUID;

	public JecnManhattanRouterResultData() {

	}

	public Point getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}

	public Point getEndPint() {
		return endPint;
	}

	public void setEndPint(Point endPint) {
		this.endPint = endPint;
	}

	public boolean isEndLine() {
		return isEndLine;
	}

	public void setEndLine(boolean isEndLine) {
		this.isEndLine = isEndLine;
	}
	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}
}
