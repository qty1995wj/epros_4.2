package com.jecn.epros.server.bean.task.temp;

import java.util.List;

/**
 * 查询任务搜索条件
 * 
 * @author zhangxh
 * 
 */
public class SimpleSeachTaskBean {
	/** 任务名称 */
	private String taskName;
	/***/
	private Long taskId;
	/** 任务当前状态 */
	private String state;
	/** 审批人 userId */
	private Long userId;
	/** 开始时间 */
	private String startDate;
	/** 结束时间 */
	private String endDate;
	/** 1是不需要时间作为条件，0是需要时间作为条件 */
	private String isDate;
	/** 0流程任务，1上传文件任务，2制度 */
	private String taskType;
	/**
	 * ************************* 查询获取数据
	 * ********************************************
	 */
	/** 查询获取任务数 */
	private int rowsCount;
	/** 流程名称 */
	private String flowName;
	/** 关联ID */
	private String rId;
	/** 获取任务信息集合 */
	private List<ShowSimpleTaskAllBean> listShowFlowTaskAllBean;

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getIsDate() {
		return isDate;
	}

	public void setIsDate(String isDate) {
		this.isDate = isDate;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public int getRowsCount() {
		return rowsCount;
	}

	public void setRowsCount(int rowsCount) {
		this.rowsCount = rowsCount;
	}

	public List<ShowSimpleTaskAllBean> getListShowFlowTaskAllBean() {
		return listShowFlowTaskAllBean;
	}

	public void setListShowFlowTaskAllBean(
			List<ShowSimpleTaskAllBean> listShowFlowTaskAllBean) {
		this.listShowFlowTaskAllBean = listShowFlowTaskAllBean;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getRId() {
		return rId;
	}

	public void setRId(String id) {
		rId = id;
	}
}
