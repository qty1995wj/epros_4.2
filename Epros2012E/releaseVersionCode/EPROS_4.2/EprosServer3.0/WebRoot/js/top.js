function nTabs(tabObj, obj) {
	var ulTitles = document.getElementById("ulTitle")
			.getElementsByTagName("td");
	var isOpened=false;
	$("#ulTitle td").each(function(i,item){
		item=$(item);
		if(item.css("backgroundImage")!="none" &&item.attr("id")==obj.id){
			isOpened=true;
			return;
		}
	});
	if(isOpened){
		return;
	}
	for (i = 0; i < ulTitles.length; i++) {
		if (ulTitles[i].id == obj.id) {
			var image = basePath + "images/top/background.jpg";
			ulTitles[i].style.backgroundImage = "url(" + image + ")";
			ulTitles[i].style.color = "black";
		} else {
			ulTitles[i].style.backgroundImage = "";
			ulTitles[i].style.color = "white";
		}
	}      

}

function switchSearchEverything(){
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
		url : basePath + 'searchResource.jsp?timeStamp='
				+ getTimeStamp(),
		scripts : true,
		callback : function(el, success, response) {
			if (response.responseText.indexOf('login.jsp') > 0) {
				return;
			}
		}
	});
}

var mian_index_id = "";
function swicthMyhome() {
	mian_index_id = "myhome_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext
			.getCmp("main-panel")
			.load(
					{
						url : basePath + 'getMyTaskTotal.action?timsStamp='
								+ getTimeStamp(),
						scripts : true,
						callback : function(el, success, response) {
							if (response.responseText.indexOf('login.jsp') > 0) {
								return;
							}
							myhome();
							if (taskTotal > 0) {
								document.getElementById(
										"initSearch.action;myTask").click();
							} else {
								document
										.getElementById(
												basePath
														+ "jsp/myhome/positionRepository.jsp;positionRepository")
										.click();
							}
						}
					});
}
var processCliean = true;
function swicthProcessSys() {
	mian_index_id = "process_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load(
			{
				url : basePath + 'jsp/processSys/processSys.jsp?timsStamp='
						+ getTimeStamp(),
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					processCliean = true;
					processSys();
				}
			});

}

function swicthRiskSystem() {
	mian_index_id = "riskSystem_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
		url : basePath + 'jsp/riskSystem/riskSystem.jsp?timsStamp=' + getTimeStamp(),
		scripts : true,
		callback : function(el, success, response) {
			if (response.responseText.indexOf('login.jsp') > 0) {
				return;
			}
			riskSystem();
		}
	});
}

function swicthStandardSys() {
	mian_index_id = "standardPanel_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load(
			{
				url : basePath + 'jsp/standardSys/standardSys.jsp?timsStamp='
						+ getTimeStamp(),
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					standardSys();
				}
			});
}

var orgPositionCliean = true;
function swicthOrgPosition() {
	mian_index_id="orgPositionPanel_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
				url : basePath + 'jsp/popedom/orgPosition.jsp?timsStamp='+getTimeStamp(),
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					orgPositionCliean = true;
					orgPosition();
				}
			});
}
//点击任务管理执行的方法
function taskManagement() {
	//点击任务管理按钮初始化tab页
	mian_index_id = "taskManageGridPanel_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load(
			{
				url : basePath + 'jsp/task/taskManagement.jsp?timsStamp='
						+ getTimeStamp(),
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					processCliean = true;
					taskManagementInit();

				}
			});
	
}

/*
 * 初始化任务管理tab页
 * @return {TypeName} 
 */
function taskManagementInit(){
	var taskManagementPanel =new Ext.TabPanel(
			{  
				border : true,
				//resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				id : 'taskManagementTabPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight(),
				renderTo : 'taskManagement_id',
				defaults : {
					border : false
				},
				items : [
						{
							title : i_search,// 搜索
							id : "taskManagementSearch_id",
							autoLoad : {
								url : 'initManagementSearch.action?timsStamp='+getTimeStamp(),
								callback : function() {
									taskManage();
								}
							}
						},
						{
							title : i_taskManagementPeopleReplace,// 任务审批人替换
							id : "taskManagementPeopleReplace_id",
							autoLoad : {
								url : basePath
										+ "jsp/task/taskManagementPeopleReplace.jsp"
								
							}
						}
					]
	
		});
	//如果非管理员删除人员替换tab页
	if(!isAdmin)
	{
		taskManagementPanel.remove(Ext
							.getCmp("taskManagementPeopleReplace_id"));
		
	}
	
}


/***
 * 合理化建议
 * @return {TypeName} 
 */
function navigationRtnlPropose() {
	mian_index_id="mainProposeDivId_Id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load(
			{
				url : basePath + 'jsp/propose/navigationRtnlPropose.jsp?timsStamp='
						+ getTimeStamp(),
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					processCliean = true;
					navigationRtnlProposeTabInit();

				}
			});
}

/**
 * 导航栏合理化建议tab页初始化
 */
function navigationRtnlProposeTabInit(){
	//TODO
		var proposeTabPanel =new Ext.TabPanel(
			{  
				border : false,
				//resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				id : 'proposeTabPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight(),
				renderTo : 'mainProposeDivId',
				items : [
						{
							title : i_rationalization,// 合理化建议
							id : "proposeTotal_id",
							autoLoad : {
								url : 'searchProposeJsp.action?timsStamp='+getTimeStamp(),
								scripts : true,
								callback : function(el, success, response) {
								
							         tabNavigationProposeBoard(0,'1');
				
								}
							}
						},
						{
							title : "统计",// 
							id : "rtnlProposeCount_id_id",
							layout: 'fit',
							scripts : true,
							autoLoad : {
								url : basePath
										+ "jsp/propose/rtnlProposeCount.jsp?timsStamp="+getTimeStamp(),
								
							scripts : true,
							callback : function(el, success, response) {
							
						         rtnlProposeCountInit();
			
							}
						}
						}
					]
	
		});
	//如果非管理员删除一些tab页
	if(!isAdmin)
	{
		proposeTabPanel.remove(Ext
							.getCmp("rtnlProposeCount_id_id"));
		
	}
	
	
}




function rationProposal() {
	$("#rtnlProposeShow_Grid").children().remove();
	$("#rtnlProposeShow_Grid").load("searchProposeShow.action?pagerMethod=first", 
	{
	  
	  "_totalRows":$("#allProId").text()
	  
	},
    function() {	
        setRtnlPropseWH('rtnlProposeShowContent');
        onloadPage(document.getElementById("totalPages").value);
	});
}


function swicthRuleSystem() {
	mian_index_id="rulePanel_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
				url : basePath + 'jsp/rule/ruleSys.jsp?timsStamp='+getTimeStamp(),
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					ruleSys();
				}
			});
}

function swicthFileSystem() {
	mian_index_id="filePanel_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
				url : basePath + 'jsp/file/fileSys.jsp?timsStamp='+getTimeStamp(),
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					fileSys();
				}
			});
}

// 报表
function swicthReportsSystem() {
	mian_index_id="reportsPanel_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
		url : basePath + 'jsp/reports/reports.jsp?timsStamp='+getTimeStamp(),
		scripts : true,
		callback : function(el, success, response) {
			reports();
			document.getElementById(basePath
					+ "jsp/reports/processDetailListReport.jsp").click();
		}
	});
}
// 系统管理
function swicthSysManageSystem() {
	var path="jsp/systemManager/personManageSearch.jsp;personManageGrid";
	if(!isShowUserManager&&isShowUserManager){
		path="jsp/systemManager/rolepersonManageSearch.jsp;roleManageGrid";
	}else{
		path="jsp/systemManager/import/importMain.jsp;personImport";
	}
	
	mian_index_id="sysManagerPanel_index_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
		url : basePath + 'jsp/systemManager/systemMain.jsp?timsStamp='+getTimeStamp(),
		scripts : true,
		callback : function(el, success, response) {
			if (response.responseText.indexOf('login.jsp') > 0) {
				return;
			}
			sysManager();
			document
					.getElementById(basePath+ path)
					.click();
		}
	});

}
// 切换项目
function tabProject() {
	var iWidth = 250; // 弹出窗口的宽度;
	var iHeight = 250; // 弹出窗口的高度;
	var iTop = (window.screen.availHeight - 30 - iHeight) / 2; // 获得窗口的垂直位置;
	var iLeft = (window.screen.availWidth - 10 - iWidth) / 2; // 获得窗口的水平位置;
	// 弹出页面，居中显示，可改变窗体大小
	var pro = window
			.open(
					'allProjects.action',
					i_projectList,
					'height='
							+ iHeight
							+ ',innerHeight='
							+ iHeight
							+ ',width='
							+ iWidth
							+ ',innerWidth='
							+ iWidth
							+ ',top='
							+ iTop
							+ ',left='
							+ iLeft
							+ ',resizable=yes,toolbar=no,menubar=no,location=no,status=no,scrollbars=no,border:5 ridge #ff0000');
	pro.focus();
}

function swicthMessage() {
	// ===============消息==================================================================================
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
				url : basePath + 'jsp/message/message.jsp',
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					myMessage();
								document
					.getElementById(basePath
							+ "jsp/message/unRead.jsp;unMessageSysGrid")
					.click();
				}
			});
}

var myMessage_index_id='';
function myMessage() {
	mian_index_id="message_index_id";
	var myhomePanel = new Ext.Panel({
		renderTo : "message_id",
		border : false,
		id:'message_index_id',
		layout : 'border',
		height : Ext.getCmp("main-panel").getInnerHeight(),
		items : [{
			region : 'west',
			border : 0,
			title : '<div id="left_title_id" style="text-align:center;font-weight:bold">'
					+ i_message + '</div>',
			contentEl : 'messageLeft_id',
			split : true,
			collapseMode : 'mini',
			width : 180

		}, {
			region : 'center',
			border : 0,
			id : 'messageCenter_id',
			title : i_unReadMesg,

			listeners:{
				'bodyresize':function(p,width,height){
					if(myMessage_index_id!=""){
						if(myMessage_index_id=='myMessage_read_id'&&Ext.getCmp('messageGridGridPanel_id')!=null){
							Ext.getCmp('messageGridGridPanel_id').setSize(width,height-5);
						}else if(myMessage_index_id=='myMessage_unread_id'&&Ext.getCmp('unMssageGridGridPanel_id')!=null){
							Ext.getCmp('unMssageGridGridPanel_id').setSize(width,height-5);
						}
						
					}
					
				}
			}
		}]
	});
	var selectedLi;
	$("#messageUl_id").children().bind("click", function(e) {
		e.target.style.background = "url(" + basePath
				+ "images/common/libackgroundClicked.jpg)";
		$(selectedLi).css("background",
				"url(" + basePath + "images/common/libackground.jpg)");
		selectedLi = e.target;
		Ext.getCmp("messageCenter_id").setTitle(selectedLi.innerHTML);
		Ext.getCmp("messageCenter_id").removeAll(true);
		Ext.getCmp("messageCenter_id").load({
					url : selectedLi.attributes.id.value.split(';')[0]+'?timsStamp='+getTimeStamp(),
					scripts : true,
					callback : function(el, success, response) {
						if (selectedLi.attributes.id.value.split(';').length == 2) {
							eval(selectedLi.attributes.id.value.split(';')[1])();
						}
					}
		});

		Ext.Ajax.request({   //TODO 点击未读消息、已读消息时更新 未读消息总数,并刷新数据表格
			url: basePath+'getUnReadMessageList.action',
			success:function(response){
				Ext.getDom("messageNum").innerHTML = "("+Ext.decode(response.responseText).totalProperty+")";
			}
		});
	});
}
// 已读消息
function messageSysGrid() {

	messageGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "getReadMessageList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "messageId",
									mapping : "messageId"
								}, {
									name : "title",
									mapping : "messageTopic"
								}, {
									name : "peopeldate",
									mapping : "peopleTime"
								}, {
									name : "ids",
									mapping : "messageId"
								}, {
									name : "peopleName",
									mapping : "peopleName"
								}])

			});
	var sm = new Ext.grid.CheckboxSelectionModel({ 
		//添加全選監聽
    listeners:{     
      'selectionchange':function(sm){  
        var hd_checker = messageGridGridPanel.getEl().select('div.x-grid3-hd-checker');    
   		 var hd = hd_checker.first();
        if(messageGridGridPanel.getSelectionModel().getSelections().length != messageGridGridPanel.getStore().getCount()){
        	 hd.removeClass('x-grid3-hd-checker-on'); 	 
        };
        if(messageGridGridPanel.getSelectionModel().getSelections().length == messageGridGridPanel.getStore().getCount()){		
        		 hd.addClass('x-grid3-hd-checker-on')}
      }
      }}  );
	messageGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), sm, {
				header : "ID",
				dataIndex : "messageId",
				align : "center",
				menuDisabled : true,
				hidden : true
				// sortable : true,
		}	, {
				header : i_mainTile,
				dataIndex : "title",
				align : "center",
				menuDisabled : true
			}, {
				header : i_sendPeopleDate,
				dataIndex : "peopeldate",
				align : "center",
				menuDisabled : true
			}, {
				header : "peopleName",
				dataIndex : "peopleName",
				hidden : true
			}

	]);
	messageGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : messageGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});
	messageGridGridPanel = new Ext.grid.GridPanel({
				id : "messageGridGridPanel_id",
				renderTo : 'readMessageGrid_id',
				tbar : [{
							borer : true,
							text : i_Delete,    
							iconCls : 'remove', 
							handler : function() { 
								delMesg()			
							}
						}],
				height : Ext.getCmp("main-panel").getInnerHeight() - 30,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				selModel : sm,
				store : messageGridStore,
				colModel : messageGridColumnModel,
				bbar : messageGridBbar
			})
			
	myMessage_index_id='myMessage_read_id';
	
}

function messageDetailRenderer(value, cellmeta, record) {
	// 制度类型
	return "<div><a target='_blank' class='a-operation' href='" + basePath
			+ "searchReadMessageBean.action?messageId="
			+ record.data['messageId'] + "&peopleName="
			+ record.data['peopleName'] + "'>" + i_lookUp + "</a>";

}
// 执行删除deleteReadMessage
function delMesg() {
	var mymesg = new String();
	var records = messageGridGridPanel.getSelectionModel().getSelections();
	if (records.length == 0) {
		Ext.MessageBox.alert(i_tip, i_noDelReadMesg);
		return;
	}
	Ext.MessageBox.confirm(i_confirm, i_HAS_DELETE_TASK,
			function(button, text) {
				if (button == 'yes') {

					for (var i = 0; i < records.length; i++) {
						mymesg += records[i].data.messageId + ",";
					}
					Ext.Ajax.request({
								url : basePath
										+ 'deleteReadMessage.action?mesgIds='
										+ mymesg,
								dataType : 'Json',
								params : {
									values : mymesg
								},
								success : function(req) { 
									for (var i = records.length; i >= 0; i--) {  
										messageGridStore
												.remove(messageGridGridPanel
														.getSelectionModel()
														.getSelected());
									}
									messageGridStore.reload();
								}
							});
				} else {
					return;
				}
			});
}

// /未读消息
function unMessageSysGrid() {
	unMssageGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "getUnReadMessageList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "messageId",
									mapping : "messageId"
								}, {
									name : "title",
									mapping : "messageTopic"
								}, {
									name : "peopeldate",
									mapping : "peopleTime"
								}, {
									name : "ids",
									mapping : "messageId"
								}, {
									name : "peopleName",
									mapping : "peopleName"
								}])

			});
	var smu = new Ext.grid.CheckboxSelectionModel({ 
		//添加全選監聽
    listeners:{     
      'selectionchange':function(smu){  		
        var d_checker = unMssageGridGridPanel.getEl().select('div.x-grid3-hd-checker');    
   		 var d = d_checker.first();
        if(unMssageGridGridPanel.getSelectionModel().getSelections().length != unMssageGridGridPanel.getStore().getCount()){
        	 d.removeClass('x-grid3-hd-checker-on'); 	 
        };
        if(unMssageGridGridPanel.getSelectionModel().getSelections().length == unMssageGridGridPanel.getStore().getCount()){	        	
        		 d.addClass('x-grid3-hd-checker-on')}
      }
      }} );
	unMssageGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), smu, {
				header : "ID",
				dataIndex : "messageId",
				align : "center",
				menuDisabled : true,
				hidden : true
				// sortable : true,
		}	, {
				header : i_mainTile,
				dataIndex : "title",
				align : "center",
				menuDisabled : true
			}, {
				header : i_sendPeopleDate,
				dataIndex : "peopeldate",
				align : "center",
				menuDisabled : true
			}, {
				header : "peopleName",
				dataIndex : "peopleName",
				hidden : true
			}

	]);
	unMssageGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : unMssageGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});
	unMssageGridGridPanel = new Ext.grid.GridPanel({
				id : "unMssageGridGridPanel_id",
				renderTo : 'unReadMssageGrid_id',
				tbar : [{  
							borer : true,
							text : is_read,
							iconCls : 'update',
							handler : function() {
								tagHasBeenRead()
							}
						},'-',{
							borer : true,
							text : i_Delete,
							iconCls : 'remove', 
							handler : function() {    
								delUnMsg()   
							}
						}],
				height : Ext.getCmp("main-panel").getInnerHeight() - 30,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				selModel : smu,
				store : unMssageGridStore,
				colModel : unMssageGridColumnModel,
				bbar : unMssageGridBbar
			})
		myMessage_index_id='myMessage_unread_id';	
}
function unReadmessageDetailRenderer(value, cellmeta, record) {

	return "<div><a target='_blank' class='a-operation' href='" + basePath
			+ "searchUnReadMessageBean.action?messageId="
			+ record.data['messageId'] + "&peopleName="
			+ record.data['peopleName'] + "' onclick='delUnReadMessage()'>"
			+ i_lookUp + "</a>";

}
function delUnReadMessage() {
	unMssageGridStore.remove(unMssageGridGridPanel.getSelectionModel()
			.getSelected());
}
// function delUnReadMessage(){
// unMssageGridStore.remove(unMssageGridGridPanel.getSelectionModel().getSelected());
// var viewport = new Ext.Viewport({
// id : 'jecnEpros_id',
// autoScroll : true,
// layout : 'border',
// items : [{
// id : "north_nav_id",
// region : 'north',
// height : 83,
// split : false,
// autoLoad :{url: basePath + 'top.jsp'
// }
// }]
// });
// }
// 执行删除deleteReadMessage
function delUnMsg() {
	var mymesg = new String();
	var records = unMssageGridGridPanel.getSelectionModel().getSelections();
	if (records.length == 0) {
		Ext.MessageBox.alert(i_tip, i_noDelUnReadMesg);
		return;
	}
	Ext.MessageBox.confirm(i_confirm, i_HAS_DELETE_TASK,
			function(button, text) {
				if (button == 'yes') {
					for (var i = 0; i < records.length; i++) {
						mymesg += records[i].data.messageId + ",";
					}
					Ext.Ajax.request({
								url : basePath
										+ 'deleteUnReadMessage.action?mesgIds='
										+ mymesg,
								dataType : 'Json',
								params : {
									values : mymesg
								},
								success : function(response) { //删除未读消息，修改未读消息数量
									for(var i = records.length; i>=0; i--){
										unMssageGridStore.remove(records[i]);
									}
									unMssageGridStore.reload();
									Ext.getDom("messageNum").innerHTML = "("+response.responseText+")";
								}
							});
				} else {
					return;
				}
			});
}
//设置标记已读
function tagHasBeenRead (){ 
	var msg = new String();
	var records = unMssageGridGridPanel.getSelectionModel().getSelections();
	if(records.length == 0){
		Ext.Msg.alert(i_tip,i_noTagUnReadMesg);
		return;
	}
	Ext.Msg.confirm(i_confirm,i_HAS_TAG_TASK,function(button,text){
		if(button == 'yes'){
			for(var i = 0; i< records.length; i++){
				msg += records[i].data.messageId + ",";
			}
			Ext.Ajax.request({
				url:basePath + 'tagUnReadMessage.action?mesgIds='+msg,
				dataType:'Json',
				params:{
					values:msg
				},
				success:function(response){
					for(var i = records.length; i>=0; i--){
						unMssageGridStore.remove(records[i]);
					}
					unMssageGridStore.reload();
					Ext.getDom("messageNum").innerHTML = "("+response.responseText+")";
				}
			});
		} else {
			return;
		} 
	});
}
function downloadJre() {
	var regedit_shell
	try {
		regedit_shell = new ActiveXObject("Wscript.Shell");
	} catch (e) {
	}
	if (regedit_shell) {
		var sysValue = regedit_shell
				.RegRead("HKLM\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment\\PROCESSOR_ARCHITECTURE");
		if (sysValue) {
			sysValue = sysValue.toLowerCase();
			if (sysValue.indexOf("64") >= 0) {
				window.open("downloadJre64.action");
			} else if (sysValue.indexOf("86") >= 0) {
				window.open("downloadJre32.action");
			}
		}
	} else if (window.navigator.platform == "Win32") {
		window.open("downloadJre32.action");
	} else {
		window.open("downloadJre64.action");
	}

}

function helpMouseOver()
{
	var contentId = document.getElementById("contentId");
	var heplULId = document.getElementById("heplUlId");
	contentId.style.display ="block";
	heplULId.style.display ="block";
	var menuid = document.getElementById("menu1");
	menuid.num = 1;
	var menuh2 = menuid.getElementsByTagName("span");
	var menuul = menuid.getElementsByTagName("ul");
	var menuli = menuul[0].getElementsByTagName("li");
	menuh2[0].onmouseover = show;
	menuh2[0].onmouseout = unshow;
	menuul[0].onmouseover = show;
	menuul[0].onmouseout = unshow;
	function show()
	{
		menuul[0].className = "clearfix typeul block";
		contentId.style.display ="block";
		heplULId.style.display ="block";
	}
	function unshow()
	{
		menuul[0].className = "typeul";
		contentId.style.display ="none";
		heplULId.style.display ="none";
	}
	for(var i = 0; i < menuli.length; i++)
	 {
		 menuli[i].num = i;
		 var liul = menuli[i].getElementsByTagName("ul")[0];
		  if(liul)
		  {
			typeshow();
		  }
	 }
	function typeshow()
	{
		menuli[i].onmouseover = showul;
		menuli[i].onmouseout = unshowul;
	}
	function showul()
	{
		menuli[this.num].getElementsByTagName("ul")[0].className = "block";
	}
	function unshowul()
	{
		menuli[this.num].getElementsByTagName("ul")[0].className = "";
	}
}
function openAboutWindow()
{
	var iTop = (window.screen.availHeight - 30 - 330) / 2; // 获得窗口的垂直位置;
	var iLeft = (window.screen.availWidth - 10 - 465) / 2; // 获得窗口的水平位置;
	window.open("aboutDaliog.jsp", "newAbout", "height=330, width=465,toolbar=no,scrollbars="+scroll+",menubar=no,top="+iTop+",left="+iLeft);
}

function downloadHelp()
{
 	window.open("downloadHelp.action");
}

// 关闭ie不兼容提示信息
function compatibleTipClose(){
	
	$("#compatible_tip").hide();
    $.ajax({
		url : "closeCompatibleTip.action"
	});
	
}