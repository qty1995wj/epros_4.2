package com.jecn.epros.server.bean.process;

/**
 * 责任部门（设计器）
 * <li>Title: JecnFlowOrg.java</li>
 * <li>Project: epros</li>
 * <li>Package: com.jecn.organization.bean</li>
 * <li>Description: JecnFlowOrg bean
 * <li>
 * <li>Copyright: Copyright (c) 2008</li>
 * <li>Company: JECN Technologies </li>
 * <li>Created on Jun 30, 2008, 9:54:32 AM</li>
 * 
 * @author guangsen_tan
 * @version Epros V1.0
 */
public class FlowOrgT implements java.io.Serializable {
	private Long floworgStructid;//主键ID
	private Long flowId;//流程ID
	private Long orgId;//部门ID

	public FlowOrgT() {

	}

	public Long getFloworgStructid() {
		return floworgStructid;
	}

	public void setFloworgStructid(Long floworgStructid) {
		this.floworgStructid = floworgStructid;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
}