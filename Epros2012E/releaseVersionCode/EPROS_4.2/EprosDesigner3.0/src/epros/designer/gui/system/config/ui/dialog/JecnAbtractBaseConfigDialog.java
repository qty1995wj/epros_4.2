package epros.designer.gui.system.config.ui.dialog;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKCancelJButtonPanel;
import epros.designer.gui.system.config.ui.main.JecnConfigMainPanel;
import epros.designer.gui.system.config.ui.main.JecnConfigTopJpanel;
import epros.designer.gui.system.config.ui.main.JecnPropertyPanel;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.right.JecnRightButtonPanel;
import epros.designer.gui.system.config.ui.tree.JecnTreePanel;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 配置对话框
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAbtractBaseConfigDialog extends JecnDialog {
	/** 主面板 */
	protected JPanel mainPanel = null;
	/** 上面panel */
	protected JecnConfigTopJpanel topPanel = null;
	/** 中间分隔面板(树面板、内容面板的容器) */
	protected JecnSplitPane splitPane = null;
	/** 中间树面板 */
	protected JecnTreePanel treePanel = null;
	/** 中间内容面板 */
	protected JecnConfigMainPanel contentPanel = null;
	/** 下面按钮panel */
	protected JecnOKCancelJButtonPanel okCancelJButtonPanel = null;

	/** 配置文件所需要的bean对象 */
	protected JecnConfigDesgBean configBean = null;

	public JecnAbtractBaseConfigDialog(JecnConfigDesgBean configBean) {
		if (configBean == null) {
			throw new IllegalArgumentException("JecnAbtractBaseConfigDialog is error");
		}
		this.configBean = configBean;

		initComponents();
		initLayout();
	}

	protected void initComponents() {
		// 主面板
		mainPanel = new JPanel();
		// 上面panel
		topPanel = new JecnConfigTopJpanel(this);
		// 中间树面板
		treePanel = new JecnTreePanel(this);
		// 中间内容面板
		contentPanel = new JecnConfigMainPanel(this);
		// 中间分隔面板
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, null,
				treePanel, contentPanel);

		// 下面按钮panel
		okCancelJButtonPanel = new JecnOKCancelJButtonPanel(this);

		// 大小
		Dimension size = new Dimension(1000, 600);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		// 设置Model
		this.setModal(true);
		this.setLocationRelativeTo(null);

		mainPanel.setOpaque(false);
		mainPanel.setBorder(null);
		mainPanel.setLayout(new GridBagLayout());
		this.getContentPane().add(mainPanel);

		// 左:右=3:7
		splitPane.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPane.setResizeWeight(0.0);
		splitPane.showBothPanel();

		// 点击按钮事件
		okCancelJButtonPanel.getOkJButton().addActionListener(
				okCancelJButtonPanel.getOkJButton());
		okCancelJButtonPanel.getCancelJButton().addActionListener(
				okCancelJButtonPanel.getCancelJButton());
		okCancelJButtonPanel.getOkJButton().setEnabled(false);
	}


	
	/**
	 * 
	 * 初始化布局
	 * 
	 */
	protected void initLayout() {
		// 上面面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(topPanel, c);

		// 中间分隔面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, JecnUIUtil
						.getInsets0(), 0, 0);
		mainPanel.add(splitPane, c);

		// 下面按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(okCancelJButtonPanel, c);
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public JecnConfigTopJpanel getTopPanel() {
		return topPanel;
	}

	public JecnTreePanel getTreePanel() {
		return treePanel;
	}

	public JecnConfigMainPanel getContentPanel() {
		return contentPanel;
	}

	public JecnOKCancelJButtonPanel getOkCancelJButtonPanel() {
		return okCancelJButtonPanel;
	}

	public JecnConfigDesgBean getConfigBean() {
		return configBean;
	}

	/**
	 * 
	 * 获取属性面板
	 * 
	 * @return JecnPropertyPanel
	 */
	public JecnPropertyPanel getPropertyPanel() {
		return contentPanel.getPropertyPanel();
	}

	/**
	 * 
	 * 获取加入、删除等按钮面板
	 * 
	 * @return JecnRightButtonPanel
	 */
	public JecnRightButtonPanel getRightPanel() {
		return contentPanel.getRightPanel();
	}

	/**
	 * 
	 * 获取当前选中的属性面板，选中根节点或者没有选中时返回值NULL
	 * 
	 * @return JecnAbstractPropertyBasePanel 当前选中的属性面板或NULL
	 */
	public JecnAbstractPropertyBasePanel getSelectedPropContPanel() {
		return getPropertyPanel().getSelectedPropContPanel();
	}

	/**
	 * 
	 * 设置提示信息以及确认按钮
	 * 
	 * @param key
	 *            String 提示信息
	 */
	public void updateBtnAndInfo(String text) {
		getOkCancelJButtonPanel().updateBtnAndInfoText(text);
	}
}
