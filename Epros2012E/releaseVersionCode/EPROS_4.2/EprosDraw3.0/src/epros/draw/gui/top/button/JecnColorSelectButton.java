package epros.draw.gui.top.button;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemOftenColorData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnColorSelectButton extends JButton implements MouseListener,
		ActionListener {
	/** 装载按钮容器 */
	protected JToolBar toolBar = null;
	/** 按钮类型 */
	protected ToolBarElemType toolBarElemType = null;

	/** 常用颜色 */
	private JecnSystemOftenColorData systemOftenColorData = null;

	/** 左边Panel */
	private JPanel leftPanel = null;
	/** 右边 */
	private JButton rightButton = null;

	/** 图片的Button */
	private JButton imageButton = null;
	/** 颜色的Button的容器 */
	protected JPanel colorButonPanel = null;
	/** 颜色的Button */
	private JButton colorbutton = null;

	public JecnColorSelectButton(String text, ToolBarElemType toolBarElemType) {

		systemOftenColorData = JecnSystemData.readOftenColorData();

		this.toolBarElemType = toolBarElemType;

		initComponents(text);
		initData();
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	private void initComponents(String text) {
		Dimension thisSize = new Dimension(60, 20);
		Dimension colorButonPanelSize = new Dimension(10, 5);

		// 左边Panel
		leftPanel = new JPanel();
		// 右边
		rightButton = new JButton();

		leftPanel.setLayout(new BorderLayout());
		leftPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		leftPanel.setOpaque(false);
		leftPanel.setBorder(null);

		// 设置标签显示文字
		rightButton.setText(text);
		// 不显示选中框
		rightButton.setFocusable(false);
		// 背景颜色
		rightButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 命令字符串
		rightButton.setActionCommand(toolBarElemType.toString());
		// 不让绘制内容区域
		rightButton.setContentAreaFilled(false);
		// 无边框
		rightButton.setBorder(null);

		// 图片的Button
		imageButton = new JButton(JecnFileUtil
				.getToolBarImageIcon(toolBarElemType.toString()));
		// 颜色的Button的容器
		colorButonPanel = new JPanel();
		// 颜色的Button
		colorbutton = new JButton();

		// 命令字符串
		imageButton.setActionCommand(toolBarElemType.toString());
		imageButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 不显示选中框
		imageButton.setFocusable(false);
		// 不让绘制内容区域
		imageButton.setContentAreaFilled(false);
		// 边框为空
		imageButton.setBorder(null);
		// 透明
		imageButton.setOpaque(false);

		// 命令字符串
		colorbutton.setActionCommand(toolBarElemType.toString());
		// 透明
		colorbutton.setOpaque(false);
		// 不让绘制内容区域
		colorbutton.setContentAreaFilled(false);
		// 不显示焦点状态
		colorbutton.setFocusPainted(false);
		// 无边框
		colorbutton.setBorder(null);

		// 大小
		colorButonPanel.setPreferredSize(colorButonPanelSize);
		colorButonPanel.setMinimumSize(colorButonPanelSize);
		colorButonPanel.setMaximumSize(colorButonPanelSize);
		// 设置布局样式
		colorButonPanel.setLayout(new BorderLayout());
		// 向panel添加panel
		colorButonPanel.add(colorbutton);

		// 布局管理器
		this.setLayout(new BorderLayout());
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 不显示焦点状态
		this.setFocusPainted(false);
		// 大小
		this.setPreferredSize(thisSize);
		this.setMaximumSize(thisSize);
		this.setMinimumSize(thisSize);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());

		toolBar.add(this);

		// 布局
		leftPanel.add(imageButton, BorderLayout.CENTER);
		leftPanel.add(colorButonPanel, BorderLayout.SOUTH);

		this.add(leftPanel, BorderLayout.WEST);
		this.add(rightButton, BorderLayout.CENTER);

		// 添加点击监听
		this.addActionListener(this);
		imageButton.addActionListener(this);
		rightButton.addActionListener(this);
		colorbutton.addActionListener(this);

		imageButton.addMouseListener(this);
		colorbutton.addMouseListener(this);
		rightButton.addMouseListener(this);
	}

	private void initData() {
		// 初始显示颜色
		Color colorPanel = null;
		switch (toolBarElemType) {
		case editFill: // 填充选择颜色
			if (systemOftenColorData.getFillColorList() != null
					&& systemOftenColorData.getFillColorList().size() == 8) {
				colorPanel = systemOftenColorData.getFillColorList().get(0);
			}
			break;
		case editShadow: // 阴影选择颜色
			if (systemOftenColorData.getShadowColorList() != null
					&& systemOftenColorData.getShadowColorList().size() == 8) {
				colorPanel = systemOftenColorData.getShadowColorList().get(0);
			}
			break;
		case editLine: // 线条选择颜色
			if (systemOftenColorData.getLineColorList() != null
					&& systemOftenColorData.getLineColorList().size() == 8) {
				colorPanel = systemOftenColorData.getLineColorList().get(0);
			}
			break;
		case editfontOftenColor: // 字体选择颜色
			if (systemOftenColorData.getFontColorList() != null
					&& systemOftenColorData.getFontColorList().size() == 8) {
				colorPanel = systemOftenColorData.getFontColorList().get(0);
			}

			break;
		}

		// 设置panel背景色
		if (colorPanel != null) {
			colorButonPanel.setBackground(colorPanel);
		} else {
			colorButonPanel.setBackground(Color.red);
		}
	}

	public JPanel getColorButonPanel() {
		return colorButonPanel;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		mouseTransmit(e);
	}

	public void mouseExited(MouseEvent e) {
		mouseTransmit(e);
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) { // 点击为button
			JButton colorButton = (JButton) e.getSource();
			String colorComm = colorButton.getActionCommand();
			// 修改元素颜色
			JecnElementAttributesUtil.updateElementColor(colorComm);
		}
	}
	
	/**
	 * 
	 * 鼠标事件传递
	 * 
	 * @param e
	 */
	private void mouseTransmit(MouseEvent e) {
		// 源按钮
		JButton button = (JButton) e.getSource();
		// 目标按钮
		MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(button, e,
				JecnColorSelectButton.this);
		// 触发给定事件
		JecnColorSelectButton.this.dispatchEvent(desMouseEvent);
	}
}