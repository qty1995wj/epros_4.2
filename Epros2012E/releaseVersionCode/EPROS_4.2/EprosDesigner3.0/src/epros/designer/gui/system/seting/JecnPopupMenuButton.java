package epros.designer.gui.system.seting;

import java.awt.GridLayout;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import epros.designer.gui.system.JecnToolbarButton;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 选择具体配置项的弹出菜单
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPopupMenuButton extends JPopupMenu   {
	/** 主panel */
	private JPanel mainPanel = null;
	/** 按钮列表 */
	private List<JecnToolbarButton> btnList = null;
	
	public JecnPopupMenuButton(List<JecnToolbarButton> btnList) {
		this.btnList = btnList;
		initComponents();
	}

	private void initComponents() {
		// 设置背景色为默认色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 初始化 主Panel
		mainPanel = new JPanel();
		mainPanel.setOpaque(false);

		// 设置背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		if (btnList != null && btnList.size() >= 1) {
			mainPanel.setLayout(new GridLayout(btnList.size(), 1, 3, 3));
			for (JecnToolbarButton btn : btnList) {
				mainPanel.add(btn.getJToolBar());
			}
		}
		this.add(mainPanel);
	}
}
