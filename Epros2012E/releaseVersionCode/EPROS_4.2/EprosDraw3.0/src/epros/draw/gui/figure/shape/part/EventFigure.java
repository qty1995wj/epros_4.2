package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 事件
 * 
 * @author Administrator
 * 
 * @date： 日期：Mar 22, 2012 时间：9:52:45 AM
 */
public class EventFigure extends JecnBaseFigurePanel {

	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int x5;
	private int x6;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int y5;
	private int y6;

	public EventFigure(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	@Override
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		x1 = 0;
		y1 = userHeight / 2;
		x2 = 20;
		y2 = 0;
		x3 = userWidth - 20;
		y3 = 0;
		x4 = userWidth;
		y4 = userHeight / 2;
		x5 = userWidth - 20;
		y5 = userHeight;
		x6 = 20;
		y6 = userHeight;
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1,
				y2, y3, y4, y5, y6 }, 6);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintEventTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintEventTop(g2d, shadowCount);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintEventTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		g2d.fillPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount,
				x4 - shadowCount, x5 - shadowCount, x6 - shadowCount },
				new int[] { y1 - shadowCount / 2, y2, y3, y4 - shadowCount / 2,
						y5 - shadowCount, y6 - shadowCount }, 6);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount,
				x4 - shadowCount, x5 - shadowCount, x6 - shadowCount },
				new int[] { y1 - shadowCount / 2, y2, y3, y4 - shadowCount / 2,
						y5 - shadowCount, y6 - shadowCount }, 6);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DEventLow(g2d, 0);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DEventLow(g2d, indent3Dvalue);
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	private void paint3DEventLow(Graphics2D g2d, int indent3Dvalue) {
		g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue,
				x4 - indent3Dvalue, x5 - indent3Dvalue, x6 - indent3Dvalue },
				new int[] { y1 - indent3Dvalue, y2, y3, y4 - indent3Dvalue,
						y5 - indent3Dvalue, y6 - indent3Dvalue }, 6);
		g2d.setStroke(flowElementData.getBasicStroke());
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DEventLine(g2d, 0);
	}

	private void paint3DEventLine(Graphics2D g2d, int indent3Dvalue) {
		g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue,
				x4 - indent3Dvalue, x5 - indent3Dvalue, x6 - indent3Dvalue },
				new int[] { y1 - indent3Dvalue, y2, y3, y4 - indent3Dvalue,
						y5 - indent3Dvalue, y6 - indent3Dvalue }, 6);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DEventLine(g2d, 5);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 - 5, x4 - 5, x5 - 5,
				x6 + 5 }, new int[] { y1, y2 + 5, y3 + 5, y4, y5 - 5, y6 - 5 },
				6);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		g2d.fillPolygon(new int[] { x1 + 5, x2 - 5 + 5, x3 - 5 - 5, x4 - 5 - 5,
				x5 - 5 - 5, x6 - 5 + 5 }, new int[] { y1 - 5, y2 + 5, y3 + 5,
				y4 - 5, y5 - 5 - 5, y6 - 5 - 5 }, 6);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillPolygon(new int[] { x1 + shadowCount, x2, x3 + shadowCount, x4,
				x5, x6 }, new int[] { y1, y2 + shadowCount, y3 + shadowCount,
				y4, y5, y6 }, 6);
		// int gw = 2 * 2;// 边缘渐变
		// for (int i = gw; i >= 2; i -= 2) {
		// float pct = (float) (gw - i) / (gw - 1);
		// g2d.setPaint(new GradientPaint(0.0f, userHeight * 0.25f,
		// flowElementData.getShadowColor(), 0.0f, userHeight,
		// JecnUIUtil.getShadowBodyColor()));
		// g2d.setComposite(AlphaComposite.getInstance(
		// AlphaComposite.SRC_ATOP, pct));
		// g2d.setStroke(new BasicStroke(i));
		// g2d.drawPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4, x5 + 5,
		// x6 + 5 }, new int[] { y1 + 5, y2 + 5, y3 + 5, y4 + 5, y5,
		// y6 }, 6);
		// }
	}
}
