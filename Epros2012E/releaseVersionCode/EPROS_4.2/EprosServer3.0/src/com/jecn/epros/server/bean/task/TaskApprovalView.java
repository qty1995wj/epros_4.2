package com.jecn.epros.server.bean.task;

import java.io.Serializable;
import java.util.Date;

public class TaskApprovalView implements Serializable {
	private Long taskId;
	private String taskName;
	private Long relatedId;
	private int taskType;
	/** 申请人登录名称 */
	private String applyName;
	private Date createDate;
	/** 0：未发布，1:发布 */
	private int releaseState;
	/** 相关文件URL */
	private String relatedDataUrl;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getApplyName() {
		return applyName;
	}

	public void setApplyName(String applyName) {
		this.applyName = applyName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getReleaseState() {
		return releaseState;
	}

	public void setReleaseState(int releaseState) {
		this.releaseState = releaseState;
	}

	public String getRelatedDataUrl() {
		return relatedDataUrl;
	}

	public void setRelatedDataUrl(String relatedDataUrl) {
		this.relatedDataUrl = relatedDataUrl;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}
}
