package wordxml.element.bean.page;

import wordxml.constant.Constants.DocGridType;

/***
 * //行和字符 <w:docGrid w:type="lines-and-chars" w:line-pitch="312"
 * w:char-space="40960"/>
 * 
 * //指定行 <w:docGrid w:type="lines" w:line-pitch="312" w:char-space="40960"/>
 * 
 * //文字对齐方式 <w:docGrid w:type="snap-to-chars" w:line-pitch="312"
 * w:char-space="42106"/>
 * 
 * //无 <w:docGrid w:line-pitch="312" w:char-space="42106"/>
 */
public class DocGrid {
	// 网格类型 行网格、行和字符网格、文字对齐字符网格
	private DocGridType gridType;
	// 行与行之间的距离 磅
	private float linePitch;
	// 文件每行字符数 磅
	private float charSpace;

	/**
	 * 设置文档网格以及跨度
	 * @param gridType 网格
	 * @param linePitch 跨度
	 */
	public DocGrid(DocGridType gridType, float linePitch) {
		this.gridType = gridType;
		this.linePitch = linePitch;
	}

	/**
	 * 
	 * @param gridType
	 * @param linePitch
	 * @param charSpace
	 */
	public DocGrid(DocGridType gridType, float linePitch, float charSpace) {
		this.gridType = gridType;
		this.linePitch = linePitch;
		this.charSpace = charSpace;
	}

	public DocGridType getGridType() {
		return gridType;
	}

	public void setGridType(DocGridType gridType) {
		if (gridType == null) {
			return;
		}
		this.gridType = gridType;
	}

	public float getLinePitch() {
		return linePitch;
	}

	public void setLinePitch(float linePitch) {
		this.linePitch = linePitch;
	}

	public float getCharSpace() {
		return charSpace;
	}

	public void setCharSpace(float charSpace) {
		this.charSpace = charSpace;
	}

	/**
	 * 返回默认实例
	 * 
	 * @author weidp
	 * @date 2014-10-25 下午01:47:28
	 * @return
	 */
	public static DocGrid getInstance() {
		return new DocGrid(DocGridType.LINES, 15.6F);
	}
}
