package com.jecn.epros.server.emailnew;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.email.buss.EmailConfigItemBean;

public class EmailContentBuilder {

	/** 邮件信息换行 */
	public static final String strBr = "<br>";

	/** 内容 */
	private String content;
	/** 外部链接地址 */
	private String resourceUrl;

	public String buildContent() {
		StringBuilder sb = new StringBuilder();
		sb.append(content);
		sb.append(strBr);
		sb.append(getTip());
		sb.append(strBr + "此邮件为系统自动发送，请不要直接回复，谢谢！");
		return sb.toString();
	}

	private String getTip() {
		// 存在需要打开的Epros资源链接时，写入固定的提示信息
		if (StringUtils.isNotBlank(resourceUrl)) {
			EmailConfigItemBean configItemBean = EmailConfigItemBean.getConfigItemBean();
			configItemBean.initEmailItem();
			StringBuilder sb = new StringBuilder();

			String mailAddrTip = configItemBean.getMailAddrTip();// 邮件地址提示
			String eprosResourceUrl = initEprosResourceUrl(configItemBean.getHttpString());// Epros资源链接地址

			if (StringUtils.isNotBlank(mailAddrTip)) {
				sb.append(mailAddrTip);
			}

			sb.append(eprosResourceUrl);
			sb.append(strBr);

			if (StringUtils.isNotBlank(configItemBean.getMailPwdTip())) {
				String mailPwdTip = configItemBean.getMailPwdTip();// 邮件密码提示
				sb.append(mailPwdTip);
				sb.append(strBr);
			}

			if (StringUtils.isNotBlank(configItemBean.getMailEndContent())) {
				String mailEndContent = configItemBean.getMailEndContent();// 邮件落款
				sb.append(mailEndContent);
				sb.append(strBr);
			}

			return sb.toString();
		} else {
			return "";
		}
	}

	/**
	 * 初始化Epros资源链接地址
	 * 
	 * @param eprosServerUrl
	 * @return
	 */
	private String initEprosResourceUrl(String eprosServerUrl) {
		return "<a href = '" + eprosServerUrl + resourceUrl + "'>" + eprosServerUrl + "</a>" + strBr;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

}
