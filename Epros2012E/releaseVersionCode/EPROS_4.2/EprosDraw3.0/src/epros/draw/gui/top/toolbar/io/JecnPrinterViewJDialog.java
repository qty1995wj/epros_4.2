package epros.draw.gui.top.toolbar.io;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 打印预览
 * 
 * @author fuzhh
 * 
 */
public class JecnPrinterViewJDialog extends JecnDialog implements ActionListener, ChangeListener {
	private static final Log log = LogFactory.getLog(JecnPrinterViewJDialog.class);
	/** 主panel */
	private JPanel mainPanel;
	/** 滚动面板 */
	private JScrollPane imageJScrollPanel;
	/** 图标存放panel */
	private JPanel signPanel;
	/** 取消Panel */
	private JPanel cancelPanel;
	/** 图片显示Panel */
	private JPanel showImagePanel;
	/** 横排的Label */
	private JLabel hLabel;
	/** 横排 */
	private JRadioButton hJradioButton;
	/** 竖排的label */
	private JLabel vLabel;
	/** 竖排 */
	private JRadioButton vJradioButton;
	/** 缩放范围Label */
	private JLabel zoomLabel;
	/** 缩放范围下拉框 */
	private JSpinner zoomJspinner;
	/** 互斥组 */
	private ButtonGroup btnGroup;
	/** 放大的倍数 */
	private double scaleRatio = 1.0;
	/** 符号的集合 */
	List<String> signList = new ArrayList<String>();
	/** 页面放大缩小图标 */
	List<String> mrList = new ArrayList<String>();
	/** 打印机 */
	private PrinterJob printerJob = PrinterJob.getPrinterJob();
	/** 打印机配置 */
	private PageFormat pageFormat = printerJob.defaultPage();
	/** 每页纸宽度 */
	private double pagePreviewWidth;
	/** 每页纸高度 */
	private double pagePreviewHeight;
	/** 每页内容宽度 */
	private double pageContentWidth;
	/** 每页内容高度 */
	private double pageContentHeight;
	/** 每页左边距 */
	private double pageBorderLeft;
	/** 每页右边距 */
	private double pageBorderRight;
	/** 每页顶边距 */
	private double pageBorderTop;
	/** 每页底边距 */
	private double pageBorderBottom;
	/** 打印横向显示个数 */
	private int countX = 0;
	/** 打印竖向显示个数 */
	private int countY = 0;
	/** 最终打印集合 */
	private List<JecnPrinterPreviewJPanel> printerlList;
	/** 每页显示区域panel */
	private PrinterPageShowJPanel printerPagePreviewJPanel;
	/** 打印纸张页边距panel */
	private PrinterPageBorderJPanel printerPageBorderPreviewJPanel;
	/** 打印内容显示面板panel */
	private JecnPrinterPreviewJPanel printerPageContentPreviewJPanel;
	/** 面板 */
	private JecnDrawDesktopPane workflow;
	/** 从Workflow获得显示图像区域 */
	private JecnPrinterWorkflowJPanel printerWorkflowJPanel;

	public JecnPrinterViewJDialog() {
		// 设置打印机方向
		pageFormat.setOrientation(PageFormat.REVERSE_LANDSCAPE);
		workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		initComponents();
		initView();
	}

	/**
	 * 载入当前面板到打印区域
	 */
	public void initView() {
		// 初始化存放面板数据的panel
		printerWorkflowJPanel = new JecnPrinterWorkflowJPanel(workflow, scaleRatio);
		// 每页纸宽度
		pagePreviewWidth = DrawCommon.convertDouble(pageFormat.getWidth());
		// 每页纸高度
		pagePreviewHeight = DrawCommon.convertDouble(pageFormat.getHeight());
		// 每页内容宽度
		pageContentWidth = pageFormat.getImageableWidth();
		// 每页内容高度
		pageContentHeight = pageFormat.getImageableHeight();
		// 每页左边距
		pageBorderLeft = pageFormat.getImageableX();
		// 每页右边距
		pageBorderRight = pageFormat.getWidth() - pageContentWidth - pageBorderLeft;
		// 每页顶边距
		pageBorderTop = pageFormat.getImageableY();
		// 每页底边距
		pageBorderBottom = pageFormat.getHeight() - pageContentHeight - pageBorderTop;

		// 打印横向显示个数
		countX = printerWorkflowJPanel.getWidth() / (int) pageContentWidth;
		if (printerWorkflowJPanel.getWidth() % (int) pageContentWidth != 0) {
			countX = countX + 1;
		}
		// 打印竖向显示个数
		countY = printerWorkflowJPanel.getHeight() / (int) pageContentHeight;
		if (printerWorkflowJPanel.getHeight() % (int) pageContentHeight != 0) {
			countY = countY + 1;
		}
		// 打印图像显示边距
		int previewContainerBorder = 15;
		// 打印图像显示边距
		showImagePanel = new JPanel();

		showImagePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		showImagePanel.setPreferredSize(new Dimension(((int) DrawCommon.convertDouble(pagePreviewWidth)) * countX
				+ previewContainerBorder * (countX + 1), ((int) DrawCommon.convertDouble(pagePreviewHeight)) * countY
				+ previewContainerBorder * (countY + 1)));

		imageJScrollPanel.setViewportView(showImagePanel);
		printerlList = new ArrayList<JecnPrinterPreviewJPanel>();
		// 显示总图像区域横位移
		int movX = 0;
		// 显示总图像区域竖位移
		int movY = 0;
		for (int i = 0; i < countY; i++) {
			for (int j = 0; j < countX; j++) {
				// 每页显示区域
				printerPagePreviewJPanel = new PrinterPageShowJPanel((int) pagePreviewWidth, (int) pagePreviewHeight);
				printerPagePreviewJPanel.setLayout(new BorderLayout());
				showImagePanel.add(printerPagePreviewJPanel);

				// 每页边距,目的是为了显示阴影的效果
				printerPageBorderPreviewJPanel = new PrinterPageBorderJPanel((int) pagePreviewWidth,
						(int) pagePreviewHeight, (int) pageBorderLeft, (int) pageBorderRight, (int) pageBorderTop,
						(int) pageBorderBottom);
				printerPageBorderPreviewJPanel.setLayout(new BorderLayout());
				printerPagePreviewJPanel.add(printerPageBorderPreviewJPanel, BorderLayout.CENTER);

				// 每页图像显示区域
				movX = (int) (j * pageContentWidth);
				movY = (int) (i * pageContentHeight);
				printerPageContentPreviewJPanel = new JecnPrinterPreviewJPanel((int) pageContentWidth,
						(int) pageContentHeight, movX, movY, printerWorkflowJPanel);
				printerPageBorderPreviewJPanel.add(printerPageContentPreviewJPanel, BorderLayout.CENTER);

				printerlList.add(printerPageContentPreviewJPanel);
			}
		}
	}

	/** 初始化操作 */
	private void initComponents() {
		// 设置为模态窗口
		this.setModal(true);
		// 设置标题
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("printPreview"));
		// 设置初始大小
		this.setPreferredSize(new Dimension(990, 750));
		this.setSize(990, 750);
		// 设置居中 ， 在size 之后
		setLocationRelativeTo(null);
		// 打印
		signList.add("print");
		// 页面设置
		signList.add("pageSetup");
		// 显示一页
		signList.add("onePage");
		// 放大
		mrList.add("magnify");
		// 缩小
		mrList.add("redues");

		// 初始化 主Panel
		mainPanel = new JPanel();
		// 主panel的布局方式
		mainPanel.setLayout(new BorderLayout());
		// 把符号panel放入主panel
		getSignPanel();
		mainPanel.add(signPanel, BorderLayout.NORTH);
		// 把滚动面板放入主panel
		getImageJScrollPanel();
		mainPanel.add(imageJScrollPanel, BorderLayout.CENTER);
		// 把取消面板放入主panel
		getCancelPanel();
		mainPanel.add(cancelPanel, BorderLayout.SOUTH);
		// 把主panel放入自定义的panel
		this.getContentPane().add(mainPanel);
	}

	/** 符号栏初始化 */
	private void getSignPanel() {
		JPanel sPanel = new JPanel();
		// 设置布局方式
		sPanel.setLayout(new GridBagLayout());
		// 设置背景色
		sPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 初始化互斥
		btnGroup = new ButtonGroup();
		// 初始化符号panel
		signPanel = new JPanel();
		signPanel.setPreferredSize(new Dimension(600, 30));
		// 设置背景色
		signPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置布局方式
		signPanel.setLayout(new BorderLayout());
		// 初始化横排Label
		hLabel = new JLabel();
		hLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("crosswise"));
		// 初始化 横排
		hJradioButton = new JRadioButton();
		// 初始化选中
		hJradioButton.setSelected(true);
		// 设置此按钮的动作事件名称
		hJradioButton.setActionCommand("horizontal");
		// 添加此按钮的动作侦听器
		hJradioButton.addActionListener(this);
		// 设置透明
		hJradioButton.setOpaque(false);

		// 初始化竖排的Label
		vLabel = new JLabel();
		vLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("lengthways"));
		// 初始化竖排
		vJradioButton = new JRadioButton();
		// 设置此按钮的动作事件名称
		vJradioButton.setActionCommand("verticalForms");
		// 添加此按钮的动作侦听器
		vJradioButton.addActionListener(this);
		// 设置透明
		vJradioButton.setOpaque(false);

		// 初始化缩放label
		zoomLabel = new JLabel();
		zoomLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("zoomLimits"));
		// 初始化缩放下拉框
		SpinnerModel model = new SpinnerNumberModel(100, 20, 200, 1);
		zoomJspinner = new JSpinner(model);
		zoomJspinner.setValue(100);
		// 设置下拉框事件
		zoomJspinner.addChangeListener(this);
		// 设置下拉框动作事件名称
		zoomJspinner.setName("zoom");
		// 把横向 纵向添加到互斥组
		btnGroup.add(hJradioButton);
		btnGroup.add(vJradioButton);

		// 设置布局样式
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.WEST, new Insets(0, 0, 0, 0), 0, 0);
		int count = 0;
		// 加入符号
		for (String sign : signList) {
			c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
					new Insets(0, 0, 0, 0), 0, 0);
			JButton btn = new JButton();
			// 不显示选中框
			btn.setFocusable(false);
			// 不让绘制内容区域
			// btn.setContentAreaFilled(false);
			// 边框为空
			btn.setBorder(null);
			// 透明
			btn.setOpaque(false);
			if ("print".equals(sign)) {
				btn.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("print"));
				btn.setText(JecnResourceUtil.getJecnResourceUtil().getValue("print"));
				// 设置button大小
				btn.setPreferredSize(new Dimension(80, 20));
			} else if ("pageSetup".equals(sign)) {
				btn.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("pageSetup"));
				btn.setText(JecnResourceUtil.getJecnResourceUtil().getValue("pageSetup"));
				// 设置button大小
				btn.setPreferredSize(new Dimension(100, 20));
			} else if ("onePage".equals(sign)) {
				btn.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("showA"));
				btn.setText(JecnResourceUtil.getJecnResourceUtil().getValue("showA"));
				// 设置button大小
				btn.setPreferredSize(new Dimension(100, 20));
			}
			// 设置此按钮的动作事件名称
			btn.setActionCommand(sign);
			// 添加此按钮的动作侦听器
			btn.addActionListener(this);

			btn.setIcon(JecnFileUtil.getToolBoxImageIcon(sign));

			JecnToolBar toolBar = new JecnToolBar();
			toolBar.add(btn);

			sPanel.add(toolBar, c);
			count++;
		}
		c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
				new Insets(0, 10, 0, 0), 0, 0);
		// 下拉框
		sPanel.add(zoomLabel, c);
		count++;
		c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
				new Insets(0, 0, 0, 10), 0, 0);
		sPanel.add(zoomJspinner, c);
		count++;

		// 加入符号
		for (String sign : mrList) {
			c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
					new Insets(0, 0, 0, 0), 0, 0);
			JButton btn = new JButton();
			// 不显示选中框
			btn.setFocusable(false);
			// 不让绘制内容区域
			// btn.setContentAreaFilled(false);
			// 边框为空
			btn.setBorder(null);
			btn.setBackground(JecnUIUtil.getDefaultBackgroundColor());

			if ("magnify".equals(sign)) {
				btn.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("magnify"));
			} else if ("redues".equals(sign)) {
				btn.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("redues"));
			}
			// 设置button大小
			btn.setPreferredSize(new Dimension(25, 20));
			// 设置此按钮的动作事件名称
			btn.setActionCommand(sign);
			// 添加此按钮的动作侦听器
			btn.addActionListener(this);

			btn.setIcon(JecnFileUtil.getToolBoxImageIcon(sign));

			JecnToolBar toolbar = new JecnToolBar();
			toolbar.add(btn);
			sPanel.add(toolbar, c);
			count++;
		}
		c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
				new Insets(0, 10, 0, 0), 0, 0);
		// 横向
		sPanel.add(hLabel, c);
		count++;
		c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
				new Insets(0, 0, 0, 0), 0, 0);
		sPanel.add(hJradioButton, c);
		count++;
		c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
				new Insets(0, 0, 0, 0), 0, 0);
		// 纵向
		sPanel.add(vLabel, c);
		count++;
		c = new GridBagConstraints(count, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
				new Insets(0, 0, 0, 0), 0, 0);
		sPanel.add(vJradioButton, c);

		signPanel.add(sPanel, BorderLayout.WEST);
	}

	/** 初始化滚动面板 */
	private void getImageJScrollPanel() {
		// 初始化滚动面板
		imageJScrollPanel = new JScrollPane();
		// 设置滚动面板大小
		imageJScrollPanel.setPreferredSize(new Dimension(600, 640));
	}

	/**
	 * 打印图像区域只显示一页
	 */
	private void onePagePerformed() {
		double imgChangeCountTemp = 1;
		while (true) {
			JecnPrinterWorkflowJPanel printerWorkflowJPanelTemp = new JecnPrinterWorkflowJPanel(workflow,
					imgChangeCountTemp);
			if (printerWorkflowJPanelTemp.getWidth() <= pageContentWidth
					&& printerWorkflowJPanelTemp.getHeight() <= pageContentHeight) {
				scaleRatio = imgChangeCountTemp;
				zoomJspinner.setValue(imgChangeCountTemp * 100);
				break;
			}
			imgChangeCountTemp = imgChangeCountTemp - 0.01;
		}
	}

	/** 取消panel初始化 */
	private void getCancelPanel() {
		// 初始化取消按钮
		cancelPanel = new JPanel();
		// 设置大小
		cancelPanel.setPreferredSize(new Dimension(580, 30));
		// 设置取消按钮布局方式
		cancelPanel.setLayout(new BorderLayout());
		// 设置背景色
		cancelPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 初始化取消
		JButton cancelButton = new JButton();
		// 设置此按钮的动作事件名称
		cancelButton.setActionCommand(JecnActionCommandConstants.CANCEL_BUTTON);
		// 添加此按钮的动作侦听器
		cancelButton.addActionListener(this);
		cancelButton.setText(JecnResourceUtil.getJecnResourceUtil().getValue("canceled"));
		// 把取消按钮加入到cancelPanel
		cancelPanel.add(cancelButton, BorderLayout.EAST);
	}

	/**
	 * 打印
	 */
	private void printActionPerformed() {
		// 创建文档
		Book book = new Book();
		// 获得打印横竖位置
		pageFormat.getOrientation();
		// 设置页面属性
		PageFormat pf = new PageFormat();
		Paper p = new Paper();
		if (vJradioButton.isSelected()) {
			pf.setOrientation(PageFormat.PORTRAIT);
			p.setSize(pagePreviewWidth, pagePreviewHeight);
			p.setImageableArea(this.pageBorderLeft, this.pageBorderTop, pageContentWidth, pageContentHeight);
		} else if (hJradioButton.isSelected()) {
			pf.setOrientation(PageFormat.REVERSE_LANDSCAPE);
			p.setSize(pagePreviewHeight, pagePreviewWidth);
			p.setImageableArea(this.pageBorderLeft, this.pageBorderTop, pageContentHeight, pageContentWidth);
		}
		pf.setPaper(p);

		for (int i = 0; i < printerlList.size(); i++) {
			JecnPrinterImagesJPanel printerImages = new JecnPrinterImagesJPanel(printerlList.get(i));
			// 添加到书中,组成一个页面
			book.append(printerImages, pf);
		}
		// 获取打印服务对象
		PrinterJob pj = PrinterJob.getPrinterJob();
		// 设置打印类
		pj.setPageable(book);

		try {
			// 是否显示打印对话框,在用户确认后打印,也可以直接打印
			boolean printerJobFlag = pj.printDialog();
			if (printerJobFlag) {
				pj.print();
			}
		} catch (PrinterException e) {
			log.error("JecnPrinterViewJDialog类中的printActionPerformed()方法异常", e);
		}
	}

	/**
	 * 打印设置
	 */
	private void pageSetupPerformed() {
		pageFormat = printerJob.pageDialog(pageFormat);
		initView();
		// 横竖位置选择显示
		if (pageFormat.getOrientation() == PageFormat.PORTRAIT) {
			hJradioButton.setSelected(false);
			vJradioButton.setSelected(true);
			pageFormat.setOrientation(PageFormat.PORTRAIT);
		} else {
			hJradioButton.setSelected(true);
			vJradioButton.setSelected(false);
			pageFormat.setOrientation(PageFormat.REVERSE_LANDSCAPE);
		}
	}

	/**
	 * 横排的事件处理
	 */
	private void horizontalPerformed() {
		vJradioButton.setSelected(false);
		hJradioButton.setSelected(true);
		pageFormat.setOrientation(PageFormat.REVERSE_LANDSCAPE);
		initView();
	}

	/**
	 * 竖排的事件处理
	 */
	private void verticalFormsPerformed() {
		vJradioButton.setSelected(true);
		hJradioButton.setSelected(false);
		pageFormat.setOrientation(PageFormat.PORTRAIT);
		initView();
	}

	/**
	 * button的点击事件
	 */
	public void actionPerformed(ActionEvent e) {
		// 判断点击是否为button
		if (e.getSource() instanceof JButton) {
			JButton but = (JButton) e.getSource();
			String butName = but.getActionCommand();
			// 放大
			if ("magnify".equals(butName)) {
				double zoomValue = Double.valueOf(zoomJspinner.getValue().toString());
				int magnifyValue = (int) (zoomValue + 10);
				if (magnifyValue > 200) {
					return;
				}
				zoomJspinner.setValue(magnifyValue);
			} else if ("redues".equals(butName)) { // 缩小
				double zoomValue = Double.valueOf(zoomJspinner.getValue().toString());
				int magnifyValue = (int) zoomValue - 10;
				if (magnifyValue < 20) {
					return;
				}
				zoomJspinner.setValue(magnifyValue);
			} else if ("print".equals(butName)) { // 打印
				printActionPerformed();
			} else if ("pageSetup".equals(butName)) { // 页面设置
				pageSetupPerformed();
			} else if ("onePage".equals(butName)) { // 显示一页
				onePagePerformed();
			} else if (JecnActionCommandConstants.CANCEL_BUTTON.equals(butName)) { // 取消
				this.dispose();
			}
		} else if (e.getSource() instanceof JRadioButton) {
			JRadioButton but = (JRadioButton) e.getSource();
			String butName = but.getActionCommand();
			if ("horizontal".equals(butName)) { // 横排
				horizontalPerformed();
			} else if ("verticalForms".equals(butName)) { // 竖排
				verticalFormsPerformed();
			}
		}
	}

	/**
	 * JComponent的点击事件
	 */
	public void stateChanged(ChangeEvent e) {
		// 判断点击是否为JSpinner
		if (e.getSource() instanceof JSpinner) {
			// 获取点击的JSpinner
			JSpinner jspinner = (JSpinner) e.getSource();
			// 获取点击JSpinner的名称
			String pinnerName = jspinner.getName();
			// 判断如果点击的是 定义的JSpinner
			if ("zoom".equals(pinnerName)) {
				double zoomValue = Double.valueOf(zoomJspinner.getValue().toString());
				scaleRatio = zoomValue / 100;
				workflow.getShapeProcess().setWidth(workflow.getWidth() * scaleRatio);
				workflow.getShapeProcess().setHeight(workflow.getHeight() * scaleRatio);
				initView();
			}
		}

	}
}
