package epros.designer.gui.file;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 文件重命名
 * @author fuzhh Apr 9, 2013
 *
 */
public class EditNameDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditNameDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditNameDialog(JecnTreeNode pNode, JTree jTree) {
		this.selectNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		try {
			this.setName(selectNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("EditNameDialog EditNameDialog is error", e);
		}
		this.setName(selectNode.getJecnTreeBean().getName());
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		try {
			ConnectionPool.getFileAction().reFileDirName(this.getName(),
					selectNode.getJecnTreeBean().getId(),
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			// 重命名
			JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		} catch (Exception e) {
			log.error("EditNameDialog saveData is error", e);
		}
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name,
				selectNode);
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
