package epros.designer.gui.system.config.ui.tree;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.border.BevelBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.JecnAbstractJPanel;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.main.JecnPropertyPanel;
import epros.designer.util.JecnProperties;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 树面板
 * 
 * @author Administrator
 * 
 */
public class JecnTreePanel extends JecnAbstractJPanel {

	private JecnTree jecnTree = null;

	public JecnTreePanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
	}

	protected void initComponents() {
		// 大小
		Dimension size = new Dimension(270, 342);
		this.setPreferredSize(size);
		this.setMinimumSize(size);

		// 背景颜色
		this.setBackground(Color.WHITE);
		// 边框
		this.setBorder(new BevelBorder(BevelBorder.LOWERED, JecnUIUtil
				.getDefaultBackgroundColor(), Color.GRAY));

		// 初始化树
		initTreeData();

	}

	/**
	 * 
	 * 初始化树
	 * 
	 */
	private void initTreeData() {
		// 创建树
		jecnTree = new JecnTree(JecnProperties.getValue("task_treeRootName"),
				dialog);

		jecnTree.addTreeSelectionListener(new JecnTreeSelectionListener());
		// 不显示根节点
		jecnTree.setRootVisible(false);

		// 布局
		GridBagConstraints c = null;
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 1), 0, 0);
		this.add(this.jecnTree, c);
	}

	public JecnTree getJecnTree() {
		return jecnTree;
	}

	public void setJecnTree(JecnTree jecnTree) {
		this.jecnTree = jecnTree;
	};
}

class JecnTreeSelectionListener implements TreeSelectionListener {

	public void valueChanged(TreeSelectionEvent e) {
		Object obj = e.getSource();
		if (obj == null || !(obj instanceof JecnTree)) {
			return;
		}
		JecnTree jecnTree = (JecnTree) obj;

		// 返回离 x,y 最近的节点的路径
		TreePath path = e.getNewLeadSelectionPath();

		jecnTree.setSelectionPath(path);

		// 获取选中节点
		JecnTreeNode selectedNode = (JecnTreeNode) path.getLastPathComponent();
		
		// 节点对应的数据对象
		JecnConfigTypeDesgBean configTypeDesgBean = null;
		
		// 获取属性面板
		JecnPropertyPanel propertyPanel = jecnTree.getDialog()
				.getPropertyPanel();
		//是否存在标准版，标准版的“流程制度类别配置”名称显示为 “流程类别配置”
		if(JecnConstants.isEpros()){//完整版
			if(JecnProperties.getValue("flowRuleTypeName").equals(selectedNode.getNodeName())){
				configTypeDesgBean = new JecnConfigTypeDesgBean(12);
			}else if(JecnProperties.getValue("pointReceivePeople").equals(selectedNode.getNodeName())){
				configTypeDesgBean = new JecnConfigTypeDesgBean(13);
			}else{
				configTypeDesgBean = selectedNode
						.getConfigTypeDesgBean();
			}
		}else{
			if(JecnProperties.getValue("flowTypeName").equals(selectedNode.getNodeName())){
				configTypeDesgBean = new JecnConfigTypeDesgBean(12);
			}else if(JecnConstants.versionType != 1 && JecnProperties.getValue("pointReceivePeople").equals(selectedNode.getNodeName())){
				configTypeDesgBean = new JecnConfigTypeDesgBean(13);
			}else{
				configTypeDesgBean = selectedNode
						.getConfigTypeDesgBean();
	
			}
		}
		// 根据给定的节点数据，显示内容
		propertyPanel.showProperty(configTypeDesgBean);
		
	}
}
