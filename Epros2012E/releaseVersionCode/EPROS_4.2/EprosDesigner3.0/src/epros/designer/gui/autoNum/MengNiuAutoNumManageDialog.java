package epros.designer.gui.autoNum;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 流程、制度、文件自动编号管理页面
 * 
 * @author ZXH
 * 
 */
public class MengNiuAutoNumManageDialog extends JecnDialog {
	protected static Logger log = Logger.getLogger(AutoNumManageDialog.class);
	/** 主面板 */
	protected JPanel mainPanel = null;

	protected JecnPanel buttonPanel = null;

	/** 部门代码 auto_productionLine */
	protected AutoTreeScroolPanel unitCodeScroolPanel = null;
	/** 模块代码 auto_fileType */
	protected AutoTreeScroolPanel moduleCodeScroolPanel = null;

	/** 类别代码 auto_typeCode */
	protected AreaScrollPanel typeCodeScrollPanel = null;

	/** 年 auto_smallClass */
	// protected AreaScrollPanel yearCodeScrollPanel = null;
	/** 版本 process_level */
	protected AreaScrollPanel versionCodeScrollPanel = null;

	protected JButton okButton = null;
	protected JButton calcelButton = null;

	protected JecnConfigItemBean typeItem;
	protected JecnConfigItemBean unitCodeItem;
	protected JecnConfigItemBean moduleCodeItem;
	// protected JecnConfigItemBean yearCodeItem;
	protected JecnConfigItemBean versionCodeItem;

	/** 树面板 */
	protected JScrollPane treePanel = null;

	public MengNiuAutoNumManageDialog() {
		initData();
		initCompotents();
		initLayout();
		initCompotentsValue();
	}

	protected void initData() {
		typeItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_companyCode);
		unitCodeItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_productionLine);
		moduleCodeItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_fileType);
		// yearCodeItem =
		// JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_smallClass);
		versionCodeItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.process_level);

		if (typeItem == null || unitCodeItem == null || moduleCodeItem == null || versionCodeItem == null) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("accessServerError"));
			log.error("MengNiuAutoNumManageDialog initData is error!" + versionCodeItem);
			throw new IllegalArgumentException("MengNiuAutoNumManageDialog initData is error!");
		}
	}

	protected void initCompotentsValue() {
		typeCodeScrollPanel.getTextArea().setText(typeItem.getValue());
		// yearCodeScrollPanel.getTextArea().setText(yearCodeItem.getValue());
		versionCodeScrollPanel.getTextArea().setText(versionCodeItem.getValue());
	}

	protected void initCompotents() {
		mainPanel = new JPanel();
		// 设置Dialog大小
		this.setSize(755, 430);
		this.setResizable(false);
		this.setTitle(JecnProperties.getValue("numberManagement"));
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		buttonPanel = new JecnPanel();
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		typeCodeScrollPanel = new AreaScrollPanel();

		unitCodeScroolPanel = new AutoTreeScroolPanel(unitCodeItem.getName(JecnResourceUtil.getLocale()), 3);
		moduleCodeScroolPanel = new AutoTreeScroolPanel(moduleCodeItem.getName(JecnResourceUtil.getLocale()), 4);

		// yearCodeScrollPanel = new AreaScrollPanel();
		versionCodeScrollPanel = new AreaScrollPanel();

		okButton = new JButton(JecnProperties.getValue("okBtn"));
		calcelButton = new JButton(JecnProperties.getValue("cancelBtn"));

		typeCodeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), typeItem
				.getName(JecnResourceUtil.getLocale())));
		// yearCodeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
		// yearCodeItem
		// .getName()));

		versionCodeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				versionCodeItem.getName()));

		this.getContentPane().add(mainPanel);

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		calcelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MengNiuAutoNumManageDialog.this.dispose();
			}
		});
	}

	protected void okButtonAction() {
		typeItem.setValue(typeCodeScrollPanel.getTextArea().getText());

		// yearCodeItem.setValue(yearCodeScrollPanel.getTextArea().getText());
		versionCodeItem.setValue(versionCodeScrollPanel.getTextArea().getText());

		List<JecnConfigItemBean> itemBeans = new ArrayList<JecnConfigItemBean>();
		getOkButtonItems(itemBeans);
		JecnConfigTool.updateItemBeans(itemBeans);
		this.dispose();
	}

	protected void getOkButtonItems(List<JecnConfigItemBean> itemBeans) {
		itemBeans.add(typeItem);
		itemBeans.add(unitCodeItem);
		itemBeans.add(moduleCodeItem);
		// itemBeans.add(yearCodeItem);
		itemBeans.add(versionCodeItem);
	}

	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 0, 5);
		// 中心控件显示面板
		// c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
		// GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		// mainPanel.add(typeCodeScrollPanel, c);

		// other
		addOtherPanel(c, insets);

		c = new GridBagConstraints(0, 1, 6, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		mainPanel.add(buttonPanel, c);

		// 添加按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okButton);
		buttonPanel.add(calcelButton);
	}

	protected void addOtherPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, insets,
				0, 0);
		mainPanel.add(typeCodeScrollPanel, c);

		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(unitCodeScroolPanel, c);

		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(moduleCodeScroolPanel, c);

		c = new GridBagConstraints(5, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, insets,
				0, 0);
		mainPanel.add(versionCodeScrollPanel, c);
	}

	class AreaScrollPanel extends JScrollPane {
		private JTextArea textArea = null;

		public AreaScrollPanel() {
			textArea = new JTextArea();
			// 名称输入框
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			textArea.setBorder(BorderFactory.createLineBorder(Color.gray));
			textArea.setBorder(null);

			Dimension dimension = new Dimension(100, 370);
			this.setMinimumSize(dimension);
			this.setViewportView(textArea);
			// 名称输入区域容器
			this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		}

		public JTextArea getTextArea() {
			return textArea;
		}
	}

	class AutoTreeScroolPanel extends JScrollPane {
		/** 自动编号tree面板 */
		protected AutoNumJecnTree jecnTree = null;

		public AutoTreeScroolPanel(String title, int treeType) {
			// Dimension dimension = new Dimension(200, 395);
			// treePanel.setPreferredSize(dimension);
			// treePanel.setMinimumSize(dimension);
			jecnTree = getTree(treeType);
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 边框
			// 树滚动面板
			this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
			this.setViewportView(jecnTree);
		}

		public AutoNumJecnTree getJecnTree() {
			return jecnTree;
		}
	}

	/**
	 * 
	 * @param treeType
	 *            3:单位代码，4：模块代码
	 * @return
	 */
	private AutoNumJecnTree getTree(int treeType) {
		if (treeType == 3) {
			return new MengNiuUnitCodeAutoNumJecnTree(treeType);
		} else {
			return new MengNiuModuleCodeAutoNumJecnTree(treeType);
		}
	}

}
