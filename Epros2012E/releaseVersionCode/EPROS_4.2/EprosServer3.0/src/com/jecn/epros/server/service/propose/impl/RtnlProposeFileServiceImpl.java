package com.jecn.epros.server.service.propose.impl;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.dao.propose.IRtnlProposeFileDao;
import com.jecn.epros.server.service.propose.IRtnlProposeFileService;

/**
 * 合理化建议附件表Service 操作类
 * 2013-09-02
 *
 */
@Transactional
public class RtnlProposeFileServiceImpl extends AbsBaseService<JecnRtnlProposeFile, String>
implements IRtnlProposeFileService{
	
	private IRtnlProposeFileDao rtnlProposeFileDao;

	public IRtnlProposeFileDao getRtnlProposeFileDao() {
		return rtnlProposeFileDao;
	}

	public void setRtnlProposeFileDao(IRtnlProposeFileDao rtnlProposeFileDao) {
		this.rtnlProposeFileDao = rtnlProposeFileDao;
	}
	

}
