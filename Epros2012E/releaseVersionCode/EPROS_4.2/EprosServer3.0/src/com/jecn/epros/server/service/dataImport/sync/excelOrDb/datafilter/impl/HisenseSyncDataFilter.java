package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import java.util.regex.Pattern;

import com.jecn.epros.server.action.web.login.ad.hisense.JecnHisenseAfterItem;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncSpecialDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class HisenseSyncDataFilter extends AbstractSyncSpecialDataFilter {

	public HisenseSyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	@Override
	protected void initSpecialDataPath() {
		this.specialDataPath = JecnPath.CLASS_PATH + "cfgFile/hisense/hisenseSyncSpecialData.xls";
	}

	/**
	 * 
	 * 过滤部门数据
	 * 
	 * @param deptBeanList
	 *            List<DeptBean> 待导入的部门数据
	 * 
	 */
	@Override
	public void filterDeptDataBeforeCheck() {
		log.info("海信人员同步-filterDeptDataBeforeCheck");
		// 修改海信集团总部的上级部门编号为0
		for (DeptBean deptBean : syncData.getDeptBeanList()) {
			if ("10000000".equals(deptBean.getDeptNum())) {
				deptBean.setPerDeptNum("0");
				log.info("海信人员同步-存在顶级节点");
				break;
			}
		}
	}

	/**
	 * 
	 * 过滤岗位人员数据
	 * 
	 * @param userPosBeanList
	 *            List<UserBean> 待导入的岗位人员数据
	 * 
	 */
	@Override
	public void filterUserPosDataBeforeCheck() {
		// 邮箱格式不正确
		Pattern emailer = Pattern.compile(SyncConstant.EMAIL_FORMAT);
		for (UserBean userBean : syncData.getUserPosBeanList()) {
			// 海信人员同步时邮箱格式不正确时置空
			if (!JecnCommon.isNullOrEmtryTrim(userBean.getEmail()) && !emailer.matcher(userBean.getEmail()).matches()) {
				userBean.setEmail(null);
				userBean.setEmailType(null);
			}
			// 岗位编号为00000000时设置为空
			if ("00000000".equals(userBean.getPosNum())) {
				userBean.setPosNum(null);
				userBean.setDeptNum(null);
			}
		}
	}

	/**
	 * 
	 * 目前海信实现方案：第一次同步过滤掉所有错误数据只导入正确数据，
	 * 
	 * 第二次以后的同步还是按照以前规则所有数据都要正常才入库
	 * 
	 * 所有过滤校验规则后执行
	 * 
	 * @param baseBean
	 *            BaseBean待导入数据
	 * 
	 */
	@Override
	public void removeUnusefulDataAfterCheck() {
		// 开启允许错误数据时，对错误数据进行过滤
		if (JecnHisenseAfterItem.ALLOW_ERROR) {
			// 只导入正确数据
			for (int i = syncData.getDeptBeanList().size() - 1; i >= 0; i--) {
				if (!JecnCommon.isNullOrEmtryTrim(syncData.getDeptBeanList().get(i).getError())) {
					syncData.getDeptBeanList().remove(i);
				}
			}
			// 插入岗位
			for (int i = syncData.getPosBeanList().size() - 1; i >= 0; i--) {
				if (!JecnCommon.isNullOrEmtryTrim(syncData.getPosBeanList().get(i).getUserPosBean().getError())) {
					syncData.getPosBeanList().remove(i);
				}
			}
			// 插入人员
			for (int i = syncData.getUserBeanList().size() - 1; i >= 0; i--) {
				if (!JecnCommon.isNullOrEmtryTrim(syncData.getUserBeanList().get(i).getUserPosBean().getError())) {
					syncData.getUserBeanList().remove(i);
				}
			}
		}
	}

}
