package com.jecn.epros.server.dao.email.impl;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.email.IEmailDao;
import com.jecn.epros.server.email.bean.JecnEmail;
import com.jecn.epros.server.util.SqlUtil;

public class EmailDaoImpl extends AbsBaseDao<JecnEmail,String> implements IEmailDao{

	@Override
	public void saveObject(Object obj) throws Exception {
		
		this.getSession().save(obj);
		this.getSession().flush();
		
	}

	@Override
	public List<Object[]> getNeedSendEmails() throws Exception {
		// 未发送的邮件只处理今天未发送的
		String conditon = SqlUtil.getEqualAccuracyDay("E.CREATE_DATE", new Date());
		String sql = "select"
				+ " c.subject,"
				+ " c.content,"
				+ " u.true_name,"
				+ " u.email,"
				+ " u.email_type,"
				+ " at.attachment,"
				+ " at.attachment_name,"
				+ " e.id,"
				+ " u.people_id,"
				+ " u.phone_str"
				+ " from jecn_email e"
				+ " inner join jecn_email_content c on e.content_id=c.id"
				+ " inner join jecn_user u on e.people_id=u.people_id and u.islock=0"
				+ " left join jecn_email_related_attachment a on e.id=a.email_id"
				+ " left join jecn_email_attachment at on a.attachment_id=at.id"
				+ " where e.send_state=0 and " + conditon;

		return this.listNativeSql(sql);
	}

}
