package com.jecn.epros.server.webBean.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 流程KPI跟踪表
 * 
 * 每一个责任部门下kpi等信息的集合bean
 * 
 * @author huoyl
 * 
 */
public class ProcessKPIWebListBean implements Serializable {
	
	/** 责任部门ID/流程架构的id */
	private Long id;
	/** 责任部门名称 /流程架构的名称*/
	private String name;
	/** 在excel sheet中显示的名称*/
	private String sheetName;
	/** 该责任部门下的所有的kpi的集合*/
	private List<ProcessKPIWebBean> allKPIWebBeans=new ArrayList<ProcessKPIWebBean>();
	/** 0为责任部门 1为流程地图*/
	private int type;
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public List<ProcessKPIWebBean> getAllKPIWebBeans() {
		return allKPIWebBeans;
	}
	public void setAllKPIWebBeans(List<ProcessKPIWebBean> allKPIWebBeans) {
		this.allKPIWebBeans = allKPIWebBeans;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public void addProcessKPIWebBean(ProcessKPIWebBean processKPIWebBean) {
		if (processKPIWebBean == null) {
			return;
		}
		this.allKPIWebBeans.add(processKPIWebBean);
	}
	
	
	
}
