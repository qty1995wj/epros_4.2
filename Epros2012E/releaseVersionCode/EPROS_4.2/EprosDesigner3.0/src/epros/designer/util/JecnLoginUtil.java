package epros.designer.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

public class JecnLoginUtil {
	private static Logger log = Logger.getLogger(JecnLoginUtil.class);

	public static String getMacAddress() {
		try {
			return getLocalMac(InetAddress.getLocalHost());
		} catch (SocketException e) {
			log.error("getMacAddress error", e);
			e.printStackTrace();
		} catch (UnknownHostException e) {
			log.error("getMacAddress error", e);
			e.printStackTrace();
		}
		return null;
	}

	private static String getLocalMac(InetAddress ia) throws SocketException {
		// 获取网卡，获取地址
		byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mac.length; i++) {
			if (i != 0) {
				sb.append("-");
			}
			// 字节转换为整数
			int temp = mac[i] & 0xff;
			String str = Integer.toHexString(temp);
			if (str.length() == 1) {
				sb.append("0" + str);
			} else {
				sb.append(str);
			}
		}
		return sb.toString().toUpperCase();
	}
}
