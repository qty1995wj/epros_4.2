package com.jecn.epros.server.dao.popedom.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnPosGroupPermissionsT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.popedom.IPosGroupnAccessPermissionsDao;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class PosGroupAccessPermissionsDaoImpl extends AbsBaseDao<JecnPosGroupPermissionsT, Long> implements
		IPosGroupnAccessPermissionsDao {

	@Override
	public List<JecnTreeBean> getPosGroupAccessPermissions(Long relateId, int type, boolean isPub) throws Exception {
		String sql = "";
		if (isPub) {
			sql = "select jpg.id,jpg.name,jpg.per_id ,jpp.access_type" + " from JECN_POSITION_GROUP jpg ,JECN_GROUP_PERMISSIONS jpp"
					+ " where jpg.id = jpp.postgroup_id and jpp.relate_id =? and jpp.type = ?"
					+ " order by jpg.per_id,jpg.sort_id,jpg.id";
		} else {
			sql = " select jpg.id,jpg.name,jpg.per_id, jpp.access_type" + " from JECN_POSITION_GROUP jpg ,JECN_GROUP_PERMISSIONS_T jpp"
					+ " where jpg.id = jpp.postgroup_id and jpp.relate_id =? and jpp.type = ?"
					+ " order by jpg.per_id,jpg.sort_id,jpg.id";
		}
		List<Object[]> listPos = listNativeSql(sql, relateId, type);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listPos) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPosGroup(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, String posIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posGroupType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.setNodeAuths(listIds, type, posIds, isModifyPub);
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, List<AccessId> posIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posGroupType, projectId, nodeType);
		//是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.setNodeAuths(listIds, type, posIds, isModifyPub);
	}
	
	/**
	 * 设置单一节点部门查阅权限（oracle数据库）
	 * 
	 * @param relateId
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 * @param isModifyPub
	 *            true 是连同发布一起修改
	 */
	private void setNodeAuths(List<Long> list, int type, String posIds, boolean isModifyPub) {
		if (isModifyPub) {
			String hql = "delete from JecnPosGroupPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
		}
		if (posIds == null || "".equals(posIds)) {
			String hql = "delete from JecnPosGroupPermissionsT where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
			return;
		}

		List<JecnPosGroupPermissionsT> listOldT = new ArrayList<JecnPosGroupPermissionsT>();
		String hql = "from JecnPosGroupPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		for (String str : listStr) {
			List<JecnPosGroupPermissionsT> listJecnPosGroupAccessPermissionsT = this.listHql(str, type);
			for (JecnPosGroupPermissionsT jecnAccessPermissionsT : listJecnPosGroupAccessPermissionsT) {
				listOldT.add(jecnAccessPermissionsT);
			}
		}
		String[] idsArr = posIds.split(",");
		for (Long relateId : list) {
			for (String str : idsArr) {
				if (str == null || "".equals(str)) {
					continue;
				}
				Long posId = Long.valueOf(str);
				boolean isExist = false;
				for (JecnPosGroupPermissionsT jecnAccessPermissionsT : listOldT) {
					if (relateId.equals(jecnAccessPermissionsT.getRelateId())
							&& posId.equals(jecnAccessPermissionsT.getPosGrouId())
							&& jecnAccessPermissionsT.getType().intValue() == type) {
						isExist = true;
						listOldT.remove(jecnAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnPosGroupPermissionsT jecnAccessPermissionsT = new JecnPosGroupPermissionsT();
					jecnAccessPermissionsT.setRelateId(relateId);
					jecnAccessPermissionsT.setType(type);
					// jecnAccessPermissionsT.setId(posId);
					jecnAccessPermissionsT.setPosGrouId(posId);
					this.save(jecnAccessPermissionsT);
				}
			}
		}
		for (JecnPosGroupPermissionsT jecnAccessPermissionsT : listOldT) {
			this.deleteObject(jecnAccessPermissionsT);
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "insert into JECN_GROUP_PERMISSIONS (id, POSTGROUP_ID, relate_id, type) select id, POSTGROUP_ID, relate_id, type from JECN_GROUP_PERMISSIONS_T where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, list);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}

	}
	/**
	 * 设置单一节点部门查阅权限（oracle数据库）
	 * 
	 * @param relateId
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 * @param isModifyPub
	 *            true 是连同发布一起修改
	 */
	private void setNodeAuths(List<Long> list, int type, List<AccessId> posIds, boolean isModifyPub) {
		if (isModifyPub) {
			String hql = "delete from JecnPosGroupPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
		}
		if (posIds == null || posIds.isEmpty()) {
			String hql = "delete from JecnPosGroupPermissionsT where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
			return;
		}

		List<JecnPosGroupPermissionsT> listOldT = new ArrayList<JecnPosGroupPermissionsT>();
		String hql = "from JecnPosGroupPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		for (String str : listStr) {
			List<JecnPosGroupPermissionsT> listJecnPosGroupAccessPermissionsT = this.listHql(str, type);
			for (JecnPosGroupPermissionsT jecnAccessPermissionsT : listJecnPosGroupAccessPermissionsT) {
				listOldT.add(jecnAccessPermissionsT);
			}
		}
		for (Long relateId : list) {
			for (AccessId ids : posIds) {
				if (ids == null || ids.getAccessId() == null) {
					continue;
				}
				boolean isExist = false;
				for (JecnPosGroupPermissionsT jecnAccessPermissionsT : listOldT) {
					if (relateId.equals(jecnAccessPermissionsT.getRelateId())
							&& ids.getAccessId().equals(jecnAccessPermissionsT.getPosGrouId())
							&& jecnAccessPermissionsT.getType().intValue() == type && jecnAccessPermissionsT.getAccessType().intValue() == (ids.isDownLoad() ? 1 : 0)) {
						isExist = true;
						listOldT.remove(jecnAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnPosGroupPermissionsT jecnAccessPermissionsT = new JecnPosGroupPermissionsT();
					jecnAccessPermissionsT.setRelateId(relateId);
					jecnAccessPermissionsT.setType(type);
					// jecnAccessPermissionsT.setId(posId);
					jecnAccessPermissionsT.setPosGrouId(ids.getAccessId());
					jecnAccessPermissionsT.setAccessType(ids.isDownLoad() ? 1:0);
					this.save(jecnAccessPermissionsT);
				}
			}
		}
		for (JecnPosGroupPermissionsT jecnAccessPermissionsT : listOldT) {
			this.deleteObject(jecnAccessPermissionsT);
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "insert into JECN_GROUP_PERMISSIONS (id, POSTGROUP_ID, relate_id, type,ACCESS_TYPE) select id, POSTGROUP_ID, relate_id, type,ACCESS_TYPE from JECN_GROUP_PERMISSIONS_T where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, list);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}

	}
	@Override
	public void savaOrUpdate(Long relateId, int type, String posGroupIds, boolean isSave) throws Exception {

		if (isSave) {
			if (posGroupIds != null && !"".equals(posGroupIds)) {
				String[] idsArr = posGroupIds.split(",");
				for (String str : idsArr) {
					JecnPosGroupPermissionsT jecnPosGroupPermissionsT = new JecnPosGroupPermissionsT();
					jecnPosGroupPermissionsT.setType(type);
					jecnPosGroupPermissionsT.setRelateId(relateId);
					jecnPosGroupPermissionsT.setPosGrouId(Long.valueOf(str));
					this.save(jecnPosGroupPermissionsT);
				}
			}
		} else {
			String hql = "from JecnPosGroupPermissionsT where type=? and relateId=?";
			List<JecnPosGroupPermissionsT> listposGroupPermissions = this.listHql(hql, type, relateId);
			// 新添加对象集合
			if (posGroupIds != null && !"".equals(posGroupIds)) {

				String[] idsArr = posGroupIds.split(",");
				for (String str : idsArr) {
					if (str == null || "".equals(str)) {
						continue;
					}
					boolean isExist = false;
					for (JecnPosGroupPermissionsT jecnPosGroupPermissionsT : listposGroupPermissions) {
						if (str.equals(jecnPosGroupPermissionsT.getPosGrouId().toString())) {
							listposGroupPermissions.remove(jecnPosGroupPermissionsT);
							isExist = true;
							break;
						}
					}
					if (!isExist) {
						JecnPosGroupPermissionsT jecnPosGroupPermissionsT = new JecnPosGroupPermissionsT();
						jecnPosGroupPermissionsT.setType(type);
						jecnPosGroupPermissionsT.setRelateId(relateId);
						jecnPosGroupPermissionsT.setPosGrouId(Long.valueOf(str));
						this.save(jecnPosGroupPermissionsT);
					}
				}
			}
			for (JecnPosGroupPermissionsT jecnPosGroupPermissionsT : listposGroupPermissions) {
				this.deleteObject(jecnPosGroupPermissionsT);
			}
		}
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, List<AccessId> Ids, boolean isSave) throws Exception {

		if (isSave) {
			if (Ids != null && !Ids.isEmpty()) {
				for (AccessId accessId : Ids) {
					JecnPosGroupPermissionsT jecnPosGroupPermissionsT = new JecnPosGroupPermissionsT();
					jecnPosGroupPermissionsT.setType(type);
					jecnPosGroupPermissionsT.setRelateId(relateId);
					jecnPosGroupPermissionsT.setPosGrouId(accessId.getAccessId());
					jecnPosGroupPermissionsT.setAccessType(accessId.isDownLoad() ? 1 : 0);
					this.save(jecnPosGroupPermissionsT);
				}
			}
		} else {
			String hql = "from JecnPosGroupPermissionsT where type=? and relateId=?";
			List<JecnPosGroupPermissionsT> listposGroupPermissions = this.listHql(hql, type, relateId);
			// 新添加对象集合
			if (Ids != null && !Ids.isEmpty()) {
				for (AccessId accessId : Ids) {
					if (accessId.getAccessId() == null) {
						continue;
					}
					boolean isExist = false;
					for (JecnPosGroupPermissionsT jecnPosGroupPermissionsT : listposGroupPermissions) {
						if (accessId.getAccessId().equals(jecnPosGroupPermissionsT.getPosGrouId().toString())) {
							listposGroupPermissions.remove(jecnPosGroupPermissionsT);
							isExist = true;
							break;
						}
					}
					if (!isExist) {
						JecnPosGroupPermissionsT jecnPosGroupPermissionsT = new JecnPosGroupPermissionsT();
						jecnPosGroupPermissionsT.setType(type);
						jecnPosGroupPermissionsT.setRelateId(relateId);
						jecnPosGroupPermissionsT.setPosGrouId(accessId.getAccessId());
						jecnPosGroupPermissionsT.setAccessType(accessId.isDownLoad() ? 1 : 0);
						this.save(jecnPosGroupPermissionsT);
					}
				}
			}
			for (JecnPosGroupPermissionsT jecnPosGroupPermissionsT : listposGroupPermissions) {
				this.deleteObject(jecnPosGroupPermissionsT);
			}
		}
	}

	@Override
	public void del(Long relateId, int type, String posGroupIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (StringUtils.isBlank(posGroupIds)) {
			return;
		}
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posGroupType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, posGroupIds, isModifyPub);
	}

	@Override
	public void del(Long relateId, int type, List<AccessId> posGroupIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (posGroupIds == null ||posGroupIds.isEmpty()) {
			return;
		}
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posGroupType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, posGroupIds, isModifyPub);
	}
	private void delNodeAuths(List<Long> list, int type, String orgGroupStr, boolean isModifyPub) throws Exception {

		if (StringUtils.isBlank(orgGroupStr) || list == null || list.size() == 0) {
			return;
		}
		List<String> groupIds = Arrays.asList(orgGroupStr.split(","));
		if (isModifyPub) {
			String lastHql = "";
			String hql = "delete from JecnPosGroupPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String subHql : listStr) {
				for (String groupId : groupIds) {
					lastHql = subHql + " and posGrouId =?";
					this.execteHql(lastHql, type, Long.valueOf(groupId));
				}
			}

		}

		String hql = "delete from JecnPosGroupPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		String lastHql = "";
		for (String subHql : listStr) {
			for (String groupId : groupIds) {
				lastHql = subHql + " and posGrouId =?";
				this.execteHql(lastHql, type, Long.valueOf(groupId));
			}
		}

	}

	private void delNodeAuths(List<Long> list, int type, List<AccessId> posGroupIds, boolean isModifyPub) throws Exception {

		if (posGroupIds == null || list == null || list.size() == 0) {
			return;
		}
		if (isModifyPub) {
			String lastHql = "";
			String hql = "delete from JecnPosGroupPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String subHql : listStr) {
				for (AccessId ids : posGroupIds) {
					lastHql = subHql + " and posGrouId =?";
					this.execteHql(lastHql, type,ids.getAccessId());
				}
			}

		}

		String hql = "delete from JecnPosGroupPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		String lastHql = "";
		for (String subHql : listStr) {
			for (AccessId ids : posGroupIds) {
				lastHql = subHql + " and posGrouId =?";
				this.execteHql(lastHql, type,ids.getAccessId());
			}
		}

	}

	@Override
	public void add(Long relateId, int type, String posGroupIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (StringUtils.isBlank(posGroupIds)) {
			return;
		}
		// 获得需要赋予权限的节点集合
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posGroupType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		// 先删除需要添加的节点
		this.delNodeAuths(listIds, type, posGroupIds, isModifyPub);

		// 添加
		String[] idsArr = posGroupIds.split(",");
		for (Long nodeId : listIds) {
			for (String str : idsArr) {
				Long orgId = Long.valueOf(str);
				JecnPosGroupPermissionsT jecnPosGroupPermissionsT = new JecnPosGroupPermissionsT();
				jecnPosGroupPermissionsT.setType(type);
				jecnPosGroupPermissionsT.setRelateId(nodeId);
				jecnPosGroupPermissionsT.setPosGrouId(orgId);
				this.save(jecnPosGroupPermissionsT);
			}
		}
		if (isModifyPub) {
			getSession().flush();

			String sql = "delete from JECN_GROUP_PERMISSIONS where type=? and relate_id in";
			List<String> listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}

			sql = "insert into JECN_GROUP_PERMISSIONS (id, POSTGROUP_ID, relate_id, type) select id, POSTGROUP_ID, relate_id, type from JECN_GROUP_PERMISSIONS_T where type=? and relate_id in";
			listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}
		}

	}
	@Override
	public void add(Long relateId, int type, List<AccessId> posGroupIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (posGroupIds == null || posGroupIds.isEmpty()) {
			return;
		}
		// 获得需要赋予权限的节点集合
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, posGroupType, projectId, nodeType);
		// 是否要发布权限
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		// 先删除需要添加的节点
		this.delNodeAuths(listIds, type, posGroupIds, isModifyPub);

		// 添加
		for (Long nodeId : listIds) {
			for (AccessId ids : posGroupIds) {
				Long posGroupId = ids.getAccessId();
				JecnPosGroupPermissionsT jecnPosGroupPermissionsT = new JecnPosGroupPermissionsT();
				jecnPosGroupPermissionsT.setType(type);
				jecnPosGroupPermissionsT.setRelateId(nodeId);
				jecnPosGroupPermissionsT.setPosGrouId(posGroupId);
				jecnPosGroupPermissionsT.setAccessType(ids.isDownLoad() ? 1 : 0);
				this.save(jecnPosGroupPermissionsT);
			}
		}
		if (isModifyPub) {
			getSession().flush();

			String sql = "delete from JECN_GROUP_PERMISSIONS where type=? and relate_id in";
			List<String> listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}

			sql = "insert into JECN_GROUP_PERMISSIONS (id, POSTGROUP_ID, relate_id, type,ACCESS_TYPE) select id, POSTGROUP_ID, relate_id, type,ACCESS_TYPE from JECN_GROUP_PERMISSIONS_T where type=? and relate_id in";
			listSqls = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listSqls) {
				this.execteNative(str, type);
			}
		}

	}
	private List<Long> getNeedChangeAuthNodes(Long relateId, int type, int posGroupType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = new ArrayList<Long>();
		if (posGroupType == 0) {
			listIds = JecnUtil.getNeedSetAuthNodesSingle(relateId, type, nodeType, this);
		} else if (posGroupType == 1) {
			listIds = JecnUtil.getNeedSetAuthNodesContainChildNode(relateId, type, projectId, this);
		}
		return listIds;
	}

}
