package com.jecn.epros.server.action.web.task;

/**
 * 任务业务处理，代码参数非法处理类
 * 
 * @author Administrator
 * @date： 日期：Apr 19, 2013 时间：11:11:49 AM
 */
public class TaskIllegalArgumentException extends IllegalArgumentException {
	/** 0：人员离职，1：返回操作异常  6人员离职或者没有岗位 7转批或者交办操作的时候，最新审批人已经存在了审批人 8任务已审批 9不能够撤回*/
	private int expType;

	public TaskIllegalArgumentException(String smg, int expType) {
		super(smg);
		this.expType = expType;
	}

	public TaskIllegalArgumentException() {

	}

	public int getExpType() {
		return expType;
	}

	public void setExpType(int expType) {
		this.expType = expType;
	}
}
