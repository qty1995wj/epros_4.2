package com.jecn.epros.mapper;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.security.CurProject;
import com.jecn.epros.domain.security.NodeData;
import com.jecn.epros.domain.security.SecurityBaseBean;
import com.jecn.epros.sqlprovider.SecuritySqlProvider;
import com.jecn.epros.sqlprovider.UserSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SecurityMapper {

    @SelectProvider(type = UserSqlProvider.class, method = "findByUsernameSql")
    @ResultMap("mapper.Security.JecuUserWithAuthorities")
    JecnUser findByUsername(String username);


    /**
     * 获得当今项目
     *
     * @return
     */
    @Select("SELECT JP.PROJECTID,JP.PROJECTNAME FROM JECN_CURPROJECT CUR_P" +
            " INNER JOIN JECN_PROJECT JP ON CUR_P.CUR_PROJECT_ID = JP.PROJECTID")
    @ResultMap("mapper.Security.curProject")
    CurProject findCurProject();

    /**
     * 获得岗位集合
     *
     * @param map
     * @return
     */
    @Select("SELECT POS.FIGURE_ID,POS.ORG_ID FROM JECN_FLOW_ORG_IMAGE POS" +
            "       INNER JOIN JECN_USER_POSITION_RELATED JUPR ON POS.FIGURE_ID=JUPR.FIGURE_ID AND JUPR.PEOPLE_ID=#{peopleId}" +
            "       INNER JOIN JECN_FLOW_ORG ORG ON POS.ORG_ID=ORG.ORG_ID AND ORG.DEL_STATE=0 AND ORG.PROJECTID=#{projectId}")
    List<Map<String, Object>> findPosIds(Map<String, Object> map);

    /**
     * 部门所有父部门集合（包括自己）
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "findOrgIds")
    Set<Long> findOrgIds(Map<String, Object> map);

    /**
     * 是不是能查阅岗位
     *
     * @param map
     * @return
     */
    @Select("SELECT COUNT(POS.FIGURE_ID) FROM JECN_FLOW_ORG_IMAGE POS" +
            "       INNER JOIN JECN_USER_POSITION_RELATED JUPR ON POS.FIGURE_ID=JUPR.FIGURE_ID AND JUPR.PEOPLE_ID=#{peopleId}" +
            "       INNER JOIN JECN_FLOW_ORG ORG ON POS.ORG_ID=ORG.ORG_ID AND ORG.DEL_STATE=0 AND ORG.PROJECTID=#{projectId}" +
            "       WHERE POS.FIGURE_ID=#{posId}")
    int isPosAuth(Map<String, Object> map);

    /**
     * 是不是能查阅部门
     *
     * @param map
     * @return
     */
    @Select("SELECT COUNT(ORG.ORG_ID) FROM JECN_FLOW_ORG_IMAGE POS" +
            "       INNER JOIN JECN_USER_POSITION_RELATED JUPR ON POS.FIGURE_ID=JUPR.FIGURE_ID AND JUPR.PEOPLE_ID=#{peopleId}" +
            "       INNER JOIN JECN_FLOW_ORG ORG ON POS.ORG_ID=ORG.ORG_ID AND ORG.DEL_STATE=0 AND ORG.PROJECTID=#{projectId}" +
            "       WHERE ORG.ORG_ID=#{orgId}")
    int isOrgAuth(Map<String, Object> map);

    /**
     * 获得用户默认权限的关键字
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "findDefaultRoleFilter")
    List<String> findDefaultRoleFilter(Map<String, Object> map);


    /**
     * 判断是不是参与流程
     *
     * @param posIds
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "isParticipationProcess")
    @ResultType(Integer.class)
    int isParticipationProcess(Set<Long> posIds);

    /**
     * 是否有岗位查阅权限
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "isAccessPosPermissions")
    @ResultType(Integer.class)
    int isAccessPosPermissions(Map<String, Object> map);

    /**
     * 是否有查阅权限(部门)
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "isAccessOrgPermissions")
    @ResultType(Integer.class)
    int isAccessOrgPermissions(Map<String, Object> map);


    /**
     * 获得流程基础信息
     *
     * @param id
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "getFlowBese")
    @ResultMap("mapper.Security.securityBaseBean")
    SecurityBaseBean getFlowBese(Long id);

    /**
     * 获得制度基础信息
     *
     * @param id
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "getRuleBese")
    @ResultMap("mapper.Security.securityBaseBean")
    SecurityBaseBean getRuleBese(Long id);

    /**
     * 获得标准基础信息
     *
     * @param id
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "getStandardBese")
    @ResultMap("mapper.Security.securityBaseBean")
    SecurityBaseBean getStandardBese(Long id);


    /**
     * 获得文件基础信息
     *
     * @param id
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "getFileBese")
    @ResultMap("mapper.Security.securityBaseBean")
    SecurityBaseBean getFileBese(Long id);


    /**
     * 获取有数据权限的节点ID集合
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SecuritySqlProvider.class, method = "getInitAuthorityNodesSql")
    @ResultMap("mapper.Security.nodeData")
    List<NodeData> getAuthorityNodes(Map<String, Object> map);

    @SelectProvider(type = SecuritySqlProvider.class, method = "getOrgScope")
    List<Map<String,Object>> getOrgScope(Map<String, Object> param);
}
