package com.jecn.webservice.zhongsh.flowpush.service.buss;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.process.FlowFileInputi3GUIDBean;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.webservice.zhongsh.flowpush.bean.FlowElem;
import com.jecn.webservice.zhongsh.flowpush.bean.ParallelState;
import com.jecn.webservice.zhongsh.flowpush.bean.WBAction;
import com.jecn.webservice.zhongsh.flowpush.bean.WBDecision;
import com.jecn.webservice.zhongsh.flowpush.bean.WBDecisionAction;
import com.jecn.webservice.zhongsh.flowpush.bean.WBRole;
import com.jecn.webservice.zhongsh.flowpush.bean.WBRoleStateAssignment;
import com.jecn.webservice.zhongsh.flowpush.bean.WBState;
import com.jecn.webservice.zhongsh.flowpush.bean.WBWorkflow;
import com.jecn.webservice.zhongsh.flowpush.dao.IShiHuaDao;
import com.jecn.webservice.zhongsh.flowpush.service.buss.XmlEnumBean.StateType;
import com.jecn.webservice.zhongsh.flowpush.service.impl.ICreateXmlServiceImpl;
import com.jecn.webservice.zhongsh.flowpush.service.impl.ShiHuaProSyncServiceImpl;
import com.jecn.webservice.zhongsh.i3.tempuri.OuterService_WSHttpBindingOuterService_Client;

public class ShiHuaXmlHandle {

	private Log log = LogFactory.getLog(ShiHuaProSyncServiceImpl.class);

	// 0:失败 1：成功 2：不存在流程开始元素
	private final int FAIL = 0;
	private final int SUCCESS = 1;
	private final int NO_START_ELEM = 2;
	private final int NO_END_ELEM = 3;
	private final int RECT_IN_NUM_BIG_THAN_ONE = 4;
	private final int RECT_OUT_NUM_BIG_THAN_ONE = 5;

	/** key为元素的id,value为元素 **/
	private Map<Long, FlowElem> elemMap = null;
	/** 所有的元素 **/
	private List<FlowElem> elems = null;

	/** 线的list **/
	private List<FlowElem> lineList = null;
	/** 活动list **/
	private List<FlowElem> activeList = null;
	/** TO标签的list **/
	private List<FlowElem> toList = null;
	/** 返入的list **/
	private List<FlowElem> fanruList = null;
	/** 返出的list **/
	private List<FlowElem> fanchuList = null;
	/** 并行框的list **/
	private List<FlowElem> recList = null;

	/** 角色关联的人员的map **/
	private Map<Long, List<String>> roleWithUserMap = null;

	/**
	 * 节点开始的id(创建的活动是没有id的)
	 */
	private Long startId = null;

	/** 结束 **/
	private FlowElem endElem = null;
	private FlowElem startElem = null;

	private Map<Long, FlowElem> joinMap = null;
	private Map<Long, FlowElem> splitMap = null;

	/**
	 * 最终的结果
	 */
	/** 活动的list **/
	private List<FlowElem> resultActivityList = null;
	/** 角色的list **/
	private List<FlowElem> resultRoleList = null;
	/** 线的list **/
	private List<FlowElem> resultLineList = null;

	/** 活动的Map **/
	private Map<Long, FlowElem> resultActivityMap = null;
	/** 角色的Map **/
	private Map<Long, FlowElem> resultRoleMap = null;
	/** 线的Map **/
	private Map<Long, FlowElem> resultLineMap = null;

	/** 客户的bean **/
	private WBWorkflow flow = null;

	/** 记录X轴分支指标添加多少单位 */
	private long splitX = 0;

	public int sysncFlowToBps(Long flowId, IShiHuaDao shiHuaDao) {
		if (flowId == null) {
			log.error("待推送的flowId为空，同步失败！");
			return FAIL;
		}
		// ======================当前流程的所有流程元素=======
		log.info("流程推送开始！");
		try {
			createObj();

			// 获取流程基本信息流程名称，流程GUID
			findFlowInfoById(flowId, shiHuaDao);

			elems = shiHuaDao.findAllByFlowImageId(flowId);
			// 初始化角色对应的人员map
			roleWithUserMap = shiHuaDao.findRoleRelatedPeopleId(flowId);
			// 初始化给元素生成guid以及需要的各种初始化数据
			initGuidAndInitCollection(elems);

			// 校验
			int result = validateFlowElem();

			if (result != SUCCESS) {
				log.error("校验流程图元素出错，出错类型：" + result);
				return result;
			}

			// 3、创建数据bean
			initAll();
			// 4、转换为石化的bean
			jecnToShiHua();

			log.info("生成XML开始！");

			// 获取生成的xml字符串
			String _saveWorkflow_def = ICreateXmlServiceImpl.INSTANCE.createXml(flow);

			// System.out.println(_saveWorkflow_def);

			// webservice请求,
			String resultType = OuterService_WSHttpBindingOuterService_Client.INSTANCE.workFlowResult(flow
					.getWorkflowCode(), flow.getWorkflowName(), _saveWorkflow_def, flow.getWorkflowDescription());
			if ("SUCCESS".endsWith(resultType)) {
				return SUCCESS;
			} else {
				return FAIL;
			}

			// return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("流程出错：" + e);
			return FAIL;
		}
	}

	private void findFlowInfoById(Long flowId, IShiHuaDao shiHuaDao) throws Exception {
		Object[] flowInfo = shiHuaDao.getFlowInfoObjects(flowId);
		if (flowInfo[0] != null) {
			flow.setWorkflowName(flowInfo[0].toString());
		}
		if (flowInfo[1] == null) {// 不存在流程唯一标识GUID
			FlowFileInputi3GUIDBean inputi3guidBean = new FlowFileInputi3GUIDBean();
			inputi3guidBean.setGuid(UUID.randomUUID().toString());
			inputi3guidBean.setCreateDate(new Date());
			inputi3guidBean.setFlowId(flowId);
			flow.setWorkflowCode(inputi3guidBean.getGuid());
			shiHuaDao.saveFlowFileInputi3GUIDBean(inputi3guidBean);
		} else {
			flow.setWorkflowCode(flowInfo[1].toString());
		}
	}

	private void createObj() {

		/** key为元素的id,value为元素 **/
		elemMap = new HashMap<Long, FlowElem>();
		/** 所有的元素 **/
		elems = new ArrayList<FlowElem>();

		/** 线的list **/
		lineList = new ArrayList<FlowElem>();
		/** 活动list **/
		activeList = new ArrayList<FlowElem>();
		/** TO标签的list **/
		toList = new ArrayList<FlowElem>();
		/** 返入的list **/
		fanruList = new ArrayList<FlowElem>();
		/** 返出的list **/
		fanchuList = new ArrayList<FlowElem>();

		/** 角色关联的人员的map **/
		roleWithUserMap = new HashMap<Long, List<String>>();

		startId = Long.MAX_VALUE;

		joinMap = new HashMap<Long, FlowElem>();
		splitMap = new HashMap<Long, FlowElem>();

		/**
		 * 最终的结果
		 */
		/** 活动的list **/
		resultActivityList = new ArrayList<FlowElem>();
		/** 角色的list **/
		resultRoleList = new ArrayList<FlowElem>();
		/** 线的list **/
		resultLineList = new ArrayList<FlowElem>();

		recList = new ArrayList<FlowElem>();

		/** 活动的Map **/
		resultActivityMap = new HashMap<Long, FlowElem>();
		/** 角色的Map **/
		resultRoleMap = new HashMap<Long, FlowElem>();
		/** 线的Map **/
		resultLineMap = new HashMap<Long, FlowElem>();

		/** 客户的bean **/
		flow = new WBWorkflow();

	}

	private int validateFlowElem() {

		if (this.startElem == null) {
			FlowElem minNumberElem = getMinNumberElem();
			if (minNumberElem == null) {
				log.error("没找到开始节点以及开始编号最小的节点");
				return NO_START_ELEM;
			}
		}

		if (this.endElem == null) {
			log.error("没找到结束节点");
			return NO_END_ELEM;
		}

		int result = validateRecInAndOutNum();
		if (result != SUCCESS) {
			log.error("协作框进入和出去的线异常");
			return result;
		}

		return SUCCESS;
	}

	private int validateRecInAndOutNum() {

		for (FlowElem endElem : recList) {
			int numOfInLine = 0;
			int numOfOutLine = 0;
			// 协作框进入的线
			int rectIn = findElemInLinesNum(endElem.getFigureId());
			numOfInLine += rectIn;
			if (rectIn > 1) {
				log.error("协作框id为：" + endElem.getFigureId() + " ，自身进入的线条数为" + rectIn);
				return RECT_IN_NUM_BIG_THAN_ONE;
			}

			// 协作框出去的线
			int rectOut = findElemOutLinesNum(endElem.getFigureId());
			numOfOutLine += rectOut;
			if (rectOut > 1) {
				log.error("协作框id为：" + endElem.getFigureId() + " ，自身出去的线条数为" + rectOut);
				return RECT_OUT_NUM_BIG_THAN_ONE;
			}

			MyRectangle rect = new MyRectangle(endElem.getPoLongx(), endElem.getPoLongy(), endElem.getWidth(), endElem
					.getHeight());
			for (FlowElem needActive : activeList) {

				// objActive 代表协作框内的元素（例如：活动）
				boolean flag = rect.contains(needActive.getPoLongx(), needActive.getPoLongy(), needActive.getWidth(),
						needActive.getHeight());
				if (flag) { // true 属于当前协作框

					int activeIn = findElemInLinesNum(needActive.getFigureId());
					numOfInLine += activeIn;
					if (numOfInLine > 1) {
						log.error("协作框id为：" + endElem.getFigureId() + " ，协作框和其中的活动进入的线条数和为" + numOfInLine);
						return RECT_IN_NUM_BIG_THAN_ONE;
					}

					int activeOut = findElemOutLinesNum(needActive.getFigureId());
					numOfOutLine += activeOut;
					if (numOfOutLine > 1) {
						log.error("协作框id为：" + endElem.getFigureId() + " ，协作框和其中的活动出去的线条数和为" + numOfOutLine);
						return RECT_OUT_NUM_BIG_THAN_ONE;
					}
				}
			}

		}

		return SUCCESS;
	}

	private void jecnToShiHua() {

		WBDecision createWBDecision = null;
		WBRoleStateAssignment createWBRoleStateAssignment = null;

		// 生成活动和线的关联关系
		for (FlowElem elem : resultActivityList) {
			// 活动
			flow.getwBStateCollection().add(createWBState(elem));

			// 活动和线的关系
			createWBDecision = createWBDecision(elem);
			if (createWBDecision != null) {
				flow.getwBDecisionCollection().add(createWBDecision);
			}

			// 角色和活动的关系
			createWBRoleStateAssignment = createWBRoleStateAssignment(elem);
			if (createWBRoleStateAssignment != null) {
				flow.getwBRoleStateAssignmentCollection().add(createWBRoleStateAssignment);
			}

		}

		// 生成所有用户
		WBRole allUserRole = new WBRole();
		allUserRole.setGuid("60aac1ab-0a4c-4f8c-a887-5b2de66ea379");
		allUserRole.setRoleName("所有用户");
		allUserRole.setRoleIconType("Default");
		allUserRole.setAllowedUsers("*");
		flow.getwBRoleCollection().add(allUserRole);
		// 生成发起者
		WBRole startRole = new WBRole();
		startRole.setGuid("7d7d7a66-f751-470e-807b-84c1a2a33d20");
		startRole.setRoleName("发起者");
		startRole.setRoleIconType("A_Gold");
		startRole.setAllowedUsers("#WBRoleSpecialUserAuthor");
		flow.getwBRoleCollection().add(startRole);

		// 生成角色
		for (FlowElem elem : resultRoleList) {
			flow.getwBRoleCollection().add(createWBRole(elem));

		}

		// 生成线
		for (FlowElem elem : resultLineList) {
			flow.getwBActionCollection().add(createWBAction(elem));
		}

	}

	private WBRoleStateAssignment createWBRoleStateAssignment(FlowElem elem) {
		if (elem.getRelatedRoleId() == null) {
			return null;
		}
		WBRoleStateAssignment roleStateAssignmen = new WBRoleStateAssignment();
		roleStateAssignmen.setStateGUID(elem.getGuid());
		if (resultRoleMap.containsKey(elem.getRelatedRoleId()) && resultRoleMap.get(elem.getRelatedRoleId()) != null) {
			roleStateAssignmen.setRoleGUID(resultRoleMap.get(elem.getRelatedRoleId()).getGuid());
		} else {
			log.error("活动：" + elem.getFigureId() + "对应的角色" + elem.getRelatedRoleId() + "在初始数据中未找到");
			return null;
		}
		return roleStateAssignmen;
	}

	private WBDecision createWBDecision(FlowElem elem) {
		WBDecision decision = new WBDecision();
		decision.setGuid(UUID.randomUUID().toString());
		decision.setStateGUID(elem.getGuid());
		WBDecisionAction decisionAction = null;
		for (FlowElem line : resultLineList) {
			if (elem.getFigureId().equals(line.getStartFigure())) {
				decisionAction = new WBDecisionAction();
				decisionAction.setGuid(UUID.randomUUID().toString());
				decisionAction.setActionGUID(line.getGuid());
				decision.getwBDecisionActionCollection().add(decisionAction);
			}
		}
		return decision.getwBDecisionActionCollection().size() > 1 ? decision : null;
	}

	/**
	 * 创建线
	 * 
	 * @author hyl
	 * @param elem
	 * @return
	 */
	private WBAction createWBAction(FlowElem elem) {
		WBAction action = new WBAction();
		action.setGuid(elem.getGuid());
		action.setActionName(elem.getFigureText());
		action.setDescription(elem.getActivityShow());
		action.setFirstStateGUID(elem.getStartGuid());
		action.setSecondStateGUID(elem.getEndGuid());
		if (elem.getPoLongx() == null || elem.getPoLongy() == null) {
			return action;
		}
		action.setFirstStateActionHandlerID(elem.getPoLongx().toString());
		action.setSecondStateActionHandlerID(elem.getPoLongy().toString());
		return action;
	}

	/**
	 * 创建角色
	 * 
	 * @author hyl
	 * @param elem
	 * @return
	 */
	private WBRole createWBRole(FlowElem elem) {
		WBRole role = new WBRole();
		role.setGuid(elem.getGuid());
		role.setRoleName(elem.getFigureText());
		role.setRoleIconType("A");

		List<String> list = roleWithUserMap.get(elem.getFigureId());
		if (list != null) {
			StringBuffer userAppend = new StringBuffer();
			for (int i = 0; i < list.size(); i++) {
				if (i == 0) {
					userAppend.append(list.get(i));
				} else {
					userAppend.append(";");
					userAppend.append(list.get(i));
				}
			}
			role.setAllowedUsers(userAppend.toString());
		}

		return role;
	}

	private WBState createWBState(FlowElem elem) {
		WBState state = new WBState();
		state.setGuid(elem.getGuid());
		state.setStateName(elem.getFigureText());
		state.setDescription(elem.getActivityShow());
		state.setType(elem.getType());
		if (isW2H2State(elem.getType())) {
			state.setStateWidth("2");
			state.setStateHeight("2");
		} else {
			state.setStateWidth("4");
			state.setStateHeight("2");
		}
		state.setPositionX(getPositonXY(elem.getShowPoLongx() == null ? elem.getPoLongx() : elem.getShowPoLongx()));
		state.setPositionY(getPositonXY(elem.getShowPoLongy() == null ? elem.getPoLongy() : elem.getShowPoLongy()));
		return state;
	}

	/**
	 * 高和宽都是2的state
	 * 
	 * @param type
	 */
	private boolean isW2H2State(String type) {
		return StateType.Start.toString().equals(type) || StateType.End.toString().equals(type)
				|| StateType.Decision.toString().equals(type) || StateType.ParallelJoin.toString().equals(type)
				|| StateType.ParallelSplit.toString().equals(type);
	}

	private String getPositonXY(Long pXY) {
		int value = convertDoubleToInt(pXY / 30);
		return pXY == null ? "0" : String.valueOf(pXY % 30 == 0 ? value : value + 1);
	}

	public int convertDoubleToInt(double doubleObj) {
		return (int) Math.floor(doubleObj + 0.5);
	}

	/**
	 * 
	 * @param elem
	 *            start节点
	 */
	private void initAll() {

		FlowElem needSearchElem = null;
		FlowElem startActiveElem = null;
		if (this.startElem == null) {

			// 创建开始活动
			startElem = createStartEle();
			// 添加开始和结束元素
			addStartAndEndEle();

			// 获得流程编号最小的节点
			FlowElem minElem = getMinNumberElem();

			// 编号最小的活动是否在协作框中
			FlowElem recElem = getElemInRecElem(minElem);
			if (recElem != null) {// 在协作框中

				FlowElem startToRecLine = createLineElem(startElem, recElem, null);
				lineList.add(startToRecLine);

				startActiveElem = startElem;
				needSearchElem = startElem;
			} else {// 不在协作框中
				resultActivityList.add(minElem);

				// 创建开始节点连接编号最小的活动的线
				FlowElem startToMinLine = createLineElem(startElem, minElem, null);
				resultLineList.add(startToMinLine);

				startActiveElem = minElem;
				needSearchElem = minElem;
			}

		} else {
			needSearchElem = this.startElem;
			startActiveElem = this.startElem;
			// 添加开始和结束元素
			addStartAndEndEle();
		}

		// 迭代生成数据
		initLineAndActiveElemAndAddToList(needSearchElem, startActiveElem, null);

		// 1、 返入/返出处理
		revHandle();
		// 2、TO/from标签处理
		toHandle();

		// 处理结束元素坐标值
		endElem.setShowPoLongx(endElem.getPoLongx() + splitX);

		for (FlowElem active : resultActivityList) {
			resultActivityMap.put(active.getFigureId(), active);
		}
		for (FlowElem line : resultLineList) {
			resultLineMap.put(line.getFigureId(), line);
		}
		for (FlowElem role : resultRoleList) {
			resultRoleMap.put(role.getFigureId(), role);
		}

		log.info("生成的活动数为：" + resultActivityList.size());
		log.info("生成的线段数为：" + resultLineList.size());
		log.info("生成的角色数为：" + resultRoleList.size());

	}

	private void addStartAndEndEle() {
		// 添加开始和结束元素
		resultActivityList.add(startElem);
		resultActivityList.add(endElem);
	}

	private FlowElem createStartEle() {
		FlowElem startElem = new FlowElem();
		startElem.setType(StateType.Start.toString());
		startElem.setGuid(UUID.randomUUID().toString());
		startElem.setFigureId(createId());
		startElem.setFigureText("开始");
		startElem.setPoLongx(0L);
		startElem.setPoLongy(0L);
		return startElem;
	}

	/**
	 * 获得元素所在的协作框
	 * 
	 * @param elem
	 * @return
	 */
	private FlowElem getElemInRecElem(FlowElem elem) {

		MyRectangle rect = null;
		for (FlowElem endElem : recList) {
			rect = new MyRectangle(endElem.getPoLongx(), endElem.getPoLongy(), endElem.getWidth(), endElem.getHeight());

			boolean flag = rect.contains(elem.getPoLongx(), elem.getPoLongy(), elem.getWidth(), elem.getHeight());
			if (flag) { // true 属于当前协作框
				return endElem;
			}
		}
		return null;
	}

	/**
	 * 获得活动编号最小的流程元素
	 * 
	 * @return
	 */
	private FlowElem getMinNumberElem() {
		FlowElem startNode = null;
		int curActivityNum = 0;
		for (FlowElem elem : activeList) {
			if (StringUtils.isBlank(elem.getActivityId()) || !NumberUtils.isNumber(elem.getActivityId())) {
				continue;
			}
			if (startNode == null || Integer.parseInt(elem.getActivityId()) < curActivityNum) {
				startNode = elem;
				curActivityNum = Integer.parseInt(startNode.getActivityId());
			}
		}
		return startNode;
	}

	private void toHandle() {
		for (FlowElem toElem : toList) {
			String toFigureString = toElem.getFigureText();
			if (StringUtils.isBlank(toFigureString)) {
				continue;
			}
			toFigureString = toFigureString.trim();
			// 截取To 对应的活动编号
			if (toFigureString.indexOf("To") != -1) {
				toFigureString = toFigureString.substring(2, toFigureString.length()).trim();
			} else {
				toFigureString = toFigureString.substring(0, toFigureString.length()).trim();
			}

			if (StringUtils.isBlank(toFigureString)) {
				continue;
			}

			for (FlowElem activeElem : activeList) {
				if (StringUtils.isBlank(activeElem.getActivityId())) {
					continue;
				}
				if (toFigureString.equals(activeElem.getActivityId().trim())) {
					// 创建TO到活动的线
					FlowElem lineElem = createLineElem(toElem, activeElem, null);
					resultLineList.add(lineElem);
				}

			}
		}

	}

	/**
	 * 返入返出处理
	 */
	private void revHandle() {
		// 返出找返入
		for (FlowElem fanchu : fanchuList) {
			// 找到对应的返入
			FlowElem fanru = findFanRuByFanChu(fanchu.getFigureText());
			if (fanru == null) {
				continue;
			}

			// 找到离返出符最近的元素
			FlowElem fanchuActive = rArrowhead(fanchu);
			if (fanchuActive == null) {
				continue;
			}

			// 找到离返入符最近的元素
			FlowElem fanruActive = rArrowhead(fanru);
			// 未找到
			if (fanruActive == null) {
				continue;
			}

			FlowElem startElem = fanchuActive;
			FlowElem endElem = fanruActive;

			if (isDottedRect(fanchuActive)) {
				startElem = getParallelJoinByRec(fanchuActive);
			} else {
				FlowElem fanchuRec = getElemInRecElem(fanchuActive);
				if (fanchuRec != null) {
					startElem = getParallelJoinByRec(fanchuRec);
				}
			}

			if (isDottedRect(fanruActive)) {
				endElem = getParallelSplitByRec(fanruActive);
			} else {
				// 获得元素所在的协作框
				FlowElem fanruRec = getElemInRecElem(fanruActive);
				if (fanruRec != null) {
					endElem = getParallelSplitByRec(fanruRec);
				}
			}

			if (startElem != null && endElem != null) {
				// 创建到返出的连接线
				FlowElem lineElem = createLineElem(startElem, endElem, null);
				log.info("返出到返入生成的线的guid为：" + lineElem.getGuid());
				resultLineList.add(lineElem);
			}

		}

	}

	private FlowElem getParallelJoinByRec(FlowElem recElem) {
		return joinMap.containsKey(recElem.getFigureId()) == true ? joinMap.get(recElem.getFigureId()) : null;
	}

	private FlowElem getParallelSplitByRec(FlowElem recElem) {
		return splitMap.containsKey(recElem.getFigureId()) == true ? splitMap.get(recElem.getFigureId()) : null;
	}

	private FlowElem createLineElem(FlowElem startElem, FlowElem endElem, FlowElem linePropertyElem) {
		FlowElem lineElem = new FlowElem();
		lineElem.setStartGuid(startElem.getGuid());
		lineElem.setEndGuid(endElem.getGuid());
		lineElem.setStartFigure(startElem.getFigureId());
		lineElem.setEndFigure(endElem.getFigureId());
		lineElem.setFigureId(createId());
		lineElem.setGuid(UUID.randomUUID().toString());
		if (linePropertyElem != null) {
			lineElem.setFigureText(linePropertyElem.getFigureText());
			lineElem.setPoLongx(linePropertyElem.getPoLongx());
			lineElem.setPoLongy(linePropertyElem.getPoLongy());
		} else {
			// 默认 右侧出，左侧进
			lineElem.setPoLongx(2L);
			lineElem.setPoLongy(6L);
		}
		// 转换后开始元素连接线位置为右侧出：8 为盈科连接线方向点
		if (StateType.Start.toString().equals(startElem.getType())) {
			lineElem.setPoLongx(8L);
		} else if (StateType.End.toString().equals(endElem.getType())) {
			lineElem.setPoLongy(8L);
		}

		return lineElem;
	}

	private FlowElem findFanRuByFanChu(String figureText) {
		if (StringUtils.isBlank(figureText)) {
			return null;
		}
		// 通过返出找返入
		for (FlowElem elem : fanruList) {
			if (figureText.equals(elem.getFigureText() == null ? "" : elem.getFigureText().trim())) {
				return elem;
			}
		}
		return null;
	}

	/**
	 * 查找与返入或者返出最近的元素
	 * 
	 * @param lineAngle
	 *            旋转角度 figureList 流程元素
	 * 
	 **/
	public FlowElem rArrowhead(FlowElem elem) {

		int fx = elem.getPoLongx().intValue();
		int fy = elem.getPoLongy().intValue();

		int lineW = elem.getWidth().intValue();
		int lineH = elem.getHeight().intValue();

		// 返出小线段坐标
		// 返出小线段坐标
		Point linePoint = null;
		if (elem.getRevolve() == 90.0) {
			linePoint = new Point(lineW + fx, lineH / 2 + fy);
		} else if (elem.getRevolve() == 180.0) {
			linePoint = new Point(lineW / 2 + fx, lineH + fy);
		} else if (elem.getRevolve() == 270.0) {
			linePoint = new Point(fx, lineH / 2 + fy);
		} else {// 0.0 360.0
			linePoint = new Point(lineW / 2 + fx, fy);
		}

		// 比较过的最小距离值
		int lastValue = Integer.MAX_VALUE;
		// 比较过的最小距离对应图形数据
		FlowElem nearst = null;
		// 当前图形对象
		Rectangle rect = new Rectangle();

		for (FlowElem nearElem : elems) {
			if (!(isActivity(nearElem) || isDottedRect(nearElem))) {
				continue;
			}

			// 修改属性
			rect.setBounds(nearElem.getPoLongx().intValue(), nearElem.getPoLongy().intValue(), nearElem.getWidth()
					.intValue(), nearElem.getHeight().intValue());
			if (iscontains(rect, linePoint)) {// 线段点在图形内时，返回此图形数据 1
				return nearElem;
			}

			// 返出X,Y坐标
			int lineX = linePoint.x;
			int lineY = linePoint.y;

			// 图形原点坐标以及其对角线对应点坐标
			int rectX = rect.x;
			int rectY = rect.y;
			int rectW = rect.x + rect.width;
			int rectH = rect.y + rect.height;

			int value = Integer.MAX_VALUE;
			// 开始查找最小值
			if (lineY < rectY || lineY > rectH) {// 返出在图形南北面 2
				if (lineX < rectX || lineX > rectW) {// 四个对角面
					int tmpX = Math.abs(lineX - rectX);
					int tmpY = Math.abs(lineY - rectY);
					if (tmpX <= tmpY) {
						value = tmpX;
					} else {
						value = tmpY;
					}
				} else {// 返出在图形南北面方情况 3
					value = Math.abs(lineY - rectY);
				}
			} else { // 返出在图形东西面上方情况 4
				value = Math.abs(lineX - rectX);
			}
			if (lastValue > value) {// 上一次最小距离大于当前最小距离，替换成当前对象
				lastValue = value;
				nearst = nearElem;
			}
		}
		return nearst;
	}

	/**
	 * 
	 * @param rect
	 *            比对元素外框
	 * @param linePoint
	 *            反出/返入对应线的坐标
	 * @return
	 */
	private boolean iscontains(Rectangle rect, Point linePoint) {
		int rectX = rect.x;
		int rectY = rect.y;
		// 右下
		int rectXW = rect.x + rect.width;
		int rectYH = rect.y + rect.height;
		return (rectX <= linePoint.x && linePoint.x <= rectXW) && (rectY <= linePoint.y && linePoint.y <= rectYH);
	}

	/**
	 * 生成活动、线 设计思路，从开始活动找到连接它的线，如果线连接到的目标节点是活动，生成线 如果先连接到的节点是非活动，那么查找直到找到活动，生成线
	 * 
	 * 
	 * @param needSearchElem
	 *            需要搜索的节点 非线段
	 * @param startActiveElem
	 *            线段最先连接的活动（意思就是如果需要生成一条线，那么线开始的活动就是这个）
	 */
	private void initLineAndActiveElemAndAddToList(FlowElem needSearchElem, FlowElem startActiveElem, FlowElem startLine) {

		for (FlowElem line : lineList) {
			// 找到第一个连接的线
			if (needSearchElem.getFigureId().equals(line.getStartFigure())) {

				// 找到线的结束节点
				FlowElem lineEndElem = elemMap.get(line.getEndFigure());

				if (lineEndElem == null) {
					log.error("------");
					log.error("map中未找到figureId为" + line.getEndFigure() + "的元素");
					continue;
				}

				FlowElem recElem = null;
				if (isActivity(lineEndElem)) {
					recElem = getActiveAroundRec(lineEndElem);
				}

				if (isDottedRect(lineEndElem) || recElem != null) {// 活动框或者连接的活动是在这个活动框内的
					if (recElem != null) {
						lineEndElem = recElem;
					}
					// 连接活动框出去的线
					List<FlowElem> outLines = findElemOutLines(lineEndElem.getFigureId());

					// 协作框内活动临时集合
					List<FlowElem> activeTempList = new ArrayList<FlowElem>();
					MyRectangle rect = new MyRectangle(lineEndElem.getPoLongx(), lineEndElem.getPoLongy(), lineEndElem
							.getWidth(), lineEndElem.getHeight());
					// 根据流程协作框查找协作框内所有的活动
					for (FlowElem needActive : activeList) {
						if (!isDottedRect(needActive)) {
							// objActive 代表协作框内的元素（例如：活动）
							boolean flag = rect.contains(needActive.getPoLongx(), needActive.getPoLongy(), needActive
									.getWidth(), needActive.getHeight());
							if (flag) { // true 属于当前协作框
								// 将协作框内的元素添加到临时集合中
								activeTempList.add(needActive);
								// 活动出去的线
								outLines.addAll(findElemOutLines(needActive.getFigureId()));
							}
						}
					}

					if (outLines.size() == 0 || outLines.size() > 1) {
						log.error("协作框出去的线为：" + outLines + " 所以错误,必须只有一条");
						continue;
					}

					// 出去的线有一根
					if (outLines.size() == 1) {
						// 分支和会聚 节点处理
						addSplitJoinEle(startActiveElem, lineEndElem, startLine, outLines, activeTempList);
					}
				} else if (isSubFlow(lineEndElem) || isInterface(lineEndElem)) {// 如果是子流程或者接口直接连到结束

					if (isLastElem(lineEndElem)) {
						// 创建到结束的线
						FlowElem lineElem = createLineElem(startActiveElem, this.endElem, null);
						resultLineList.add(lineElem);
					} else {
						// 获得线连接的下个元素
						initLineAndActiveElemAndAddToList(lineEndElem, startActiveElem, line);
					}

				} else if (isEnd(lineEndElem)) {// 如果是结束节点

					// 创建到结束节点的线
					FlowElem lineElem = createLineElem(startActiveElem, this.endElem, null);
					resultLineList.add(lineElem);

				} else if (isActivity(lineEndElem)) {
					activeHandle(lineEndElem, startActiveElem, line);
				} else {// 如果非活动直接向下递归
					initLineAndActiveElemAndAddToList(lineEndElem, startActiveElem, line);
				}
			}
		}

	}

	private void activeHandle(FlowElem lineEndElem, FlowElem startActiveElem, FlowElem line) {

		// 是否已经生成该活动
		FlowElem endActiveElem = getResultActive(lineEndElem.getFigureId());

		// 不为空说明这个活动已经存在，已经到达
		if (endActiveElem != null) {
			// 创建新的线
			FlowElem lineElem = createLineElem(startActiveElem, endActiveElem, line);
			resultLineList.add(lineElem);
		} else {
			// 创建新的活动
			endActiveElem = lineEndElem;
			setActiveType(endActiveElem);
			endActiveElem.setShowPoLongx(endActiveElem.getPoLongx() + splitX);
			endActiveElem.setShowPoLongy(endActiveElem.getPoLongy());
			resultActivityList.add(endActiveElem);

			// 创建新的线
			FlowElem lineElem = createLineElem(startActiveElem, endActiveElem, line);
			resultLineList.add(lineElem);

			// 当前结束的节点重新开始查找新的节点
			initLineAndActiveElemAndAddToList(lineEndElem, endActiveElem, null);
		}
	}

	private boolean isLastElem(FlowElem elem) {
		for (FlowElem e : lineList) {
			if (e.getStartFigure().equals(elem.getFigureId())) {
				return false;
			}
		}
		return true;
	}

	private long getShowPoLongX(FlowElem startActiveElem) {
		return startActiveElem.getShowPoLongx() == null ? startActiveElem.getPoLongx() : startActiveElem
				.getShowPoLongx();
	}

	private void addSplitJoinEle(FlowElem startActiveElem, FlowElem lineEndElem, FlowElem startLine,
			List<FlowElem> outLines, List<FlowElem> activeTempList) {
		// 分支
		FlowElem parallelSplit = new FlowElem();
		parallelSplit.setFigureText("分支");
		parallelSplit.setGuid(UUID.randomUUID().toString());
		parallelSplit.setType("ParallelSplit");
		parallelSplit.setFigureId(createId());

		parallelSplit.setShowPoLongx(getShowPoLongX(startActiveElem) + startActiveElem.getWidth() + 120);
		resultActivityList.add(parallelSplit);

		splitX = splitX + 120;

		// 会聚(最终该节点的figureId为框内的出去的连接线的startFigureId也就是开始连接的元素的id)
		FlowElem parallelJoin = new FlowElem();
		parallelJoin.setFigureText("会聚");
		parallelJoin.setGuid(UUID.randomUUID().toString());
		parallelJoin.setType("ParallelJoin");

		joinMap.put(lineEndElem.getFigureId(), parallelJoin);
		splitMap.put(lineEndElem.getFigureId(), parallelSplit);

		FlowElem outLine = outLines.get(0);
		// 获得以会聚出去的线为起点的元素
		parallelJoin.setFigureId(elemMap.get(outLine.getFigureId()).getStartFigure());

		resultActivityList.add(parallelJoin);

		// 汇聚和分支配对
		flow.getParallelStateCollection().add(createParallelState(parallelSplit, parallelJoin));

		// 创建连接到分支的线
		FlowElem toLine = createLineElem(startActiveElem, parallelSplit, startLine);
		toLine.setType("Normal");
		resultLineList.add(toLine);

		long maxY = 0;
		long minY = 0;

		long maxX = 0;
		// 框内的活动
		for (FlowElem active : activeTempList) {
			if (maxY == 0 && maxY == 0) {
				maxY = active.getPoLongy();
				minY = active.getPoLongy();
				maxX = active.getPoLongx();
			}
			if (maxY <= active.getPoLongy()) {
				maxY = active.getPoLongy();
			}
			if (minY >= active.getPoLongy()) {
				minY = active.getPoLongy();
			}
			active.setShowPoLongy(active.getPoLongy());
			active.setShowPoLongx(active.getPoLongx() + splitX);

			if (maxX <= active.getShowPoLongx() + active.getWidth()) {
				maxX = active.getShowPoLongx() + active.getWidth();
			}
			resultActivityList.add(active);
			// 分支到活动的线
			FlowElem parallelJoinToActiveLine = createLineElem(parallelSplit, active, null);
			resultLineList.add(parallelJoinToActiveLine);

			// 活动到会聚的线
			FlowElem activeLineToParallelJoin = createLineElem(active, parallelJoin, null);
			resultLineList.add(activeLineToParallelJoin);
		}
		// 设置会聚和分支的Y坐标
		long y = (maxY - minY) / 2 + minY;
		parallelSplit.setShowPoLongy(y);
		parallelJoin.setShowPoLongy(y);
		// 会聚元素的位置X位置
		parallelJoin.setShowPoLongx(maxX + 120);
		splitX = splitX + 120;
		// 从会聚开始查找
		initLineAndActiveElemAndAddToList(parallelJoin, parallelJoin, null);
	}

	private boolean isInterface(FlowElem endElem) {
		if (endElem.getFigureType().equals("ImplFigure")) {
			return true;
		}
		return false;
	}

	private boolean isSubFlow(FlowElem endElem) {
		if (endElem.getFigureType().equals("OvalSubFlowFigure")) {
			return true;
		}
		return false;
	}

	/**
	 * ParallelState
	 * 
	 * @param parallelSplit
	 *            分支
	 * @param parallelJoin
	 *            会聚
	 * @return
	 */
	private ParallelState createParallelState(FlowElem parallelSplit, FlowElem parallelJoin) {
		ParallelState parallelState = new ParallelState();
		parallelState.setGuid(UUID.randomUUID().toString());
		parallelState.setSplitStateGUID(parallelSplit.getGuid());
		parallelState.setJoinStateGUID(parallelJoin.getGuid());
		return parallelState;
	}

	/**
	 * 获得包围着活动的并行框
	 * 
	 * @param needActive
	 * @return
	 */
	private FlowElem getActiveAroundRec(FlowElem needActive) {

		MyRectangle rect = null;
		// 根据流程协作框查找协作框内所有的活动
		for (FlowElem recElem : recList) {

			rect = new MyRectangle(recElem.getPoLongx(), recElem.getPoLongy(), recElem.getWidth(), recElem.getHeight());

			// objActive 代表协作框内的元素（例如：活动）
			boolean flag = rect.contains(needActive.getPoLongx(), needActive.getPoLongy(), needActive.getWidth(),
					needActive.getHeight());
			if (flag) { // true 属于当前协作框

				return recElem;
			}
		}

		return null;
	}

	/**
	 * 是否为结束节点
	 * 
	 * @param endElem
	 * @return
	 */
	private boolean isEnd(FlowElem endElem) {

		if (endElem.getFigureType().equals("EndFigure") || endElem.getFigureType().equals("FlowFigureEnd")) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为开始节点
	 * 
	 * @param elem
	 * @return
	 */
	private boolean isStart(FlowElem elem) {

		if (elem.getFigureType().equals("FlowFigureStart")) {
			return true;
		}
		return false;
	}

	/**
	 * 从最终的活动list中根据活动id获得活动
	 * 
	 * @param figureId
	 * @return
	 */
	private FlowElem getResultActive(Long figureId) {
		for (FlowElem elem : resultActivityList) {
			if (elem.getFigureId().equals(figureId)) {
				return elem;
			}
		}
		return null;
	}

	private Long createId() {
		return startId--;
	}

	/**
	 * 设置活动的类型
	 * 
	 * @param elem
	 */
	private void setActiveType(FlowElem elem) {
		String figureType = elem.getFigureType();
		// if (figureType.equals("Rhombus") || figureType.equals("ITRhombus")) {
		// elem.setType("Decision");
		// } else
		if (figureType.equals("EndFigure")) {
			elem.setType("End");
		} else {
			elem.setType("Normal");
		}
	}

	class MyRectangle {
		private long x;
		private long y;
		private long width;
		private long height;

		MyRectangle(long X, long Y, long W, long H) {
			this.x = X;
			this.y = Y;
			this.width = W;
			this.height = H;
		}

		boolean contains(long X, long Y, long W, long H) {
			long w = this.width;
			long h = this.height;
			if ((w | h | W | H) < 0) {
				return false;
			}
			long x = this.x;
			long y = this.y;
			if (X < x || Y < y) {
				return false;
			}

			w += x;
			W += X;
			if (W <= X) {
				if (w >= x || W > w)
					return false;
			} else {
				if (w >= x && W > w)
					return false;
			}
			h += y;
			H += Y;
			if (H <= Y) {
				if (h >= y || H > h)
					return false;
			} else {
				if (h >= y && H > h)
					return false;
			}
			return true;
		}
	}

	/**
	 * to跳转
	 * 
	 * @param endElem
	 * @return
	 */
	private boolean isToFigure(FlowElem endElem) {
		if (endElem.getFigureType().equals("ToFigure")) {
			return true;
		}
		return false;
	}

	/**
	 * 通过元素id获得以该元素为开始的线的集合
	 * 
	 * @param figureId
	 * @return
	 */
	private List<FlowElem> findElemOutLines(Long startFigureId) {

		List<FlowElem> elems = new ArrayList<FlowElem>();

		for (FlowElem elem : lineList) {
			if (elem.getStartFigure().equals(startFigureId)) {
				elems.add(elem);
			}
		}
		return elems;
	}

	/**
	 * 通过元素id获得以该元素为结束的线的集合
	 * 
	 * @param figureId
	 * @return
	 */
	private List<FlowElem> findElemInLines(Long endFigureId) {

		List<FlowElem> elems = new ArrayList<FlowElem>();

		for (FlowElem elem : lineList) {
			if (elem.getEndFigure().equals(endFigureId)) {
				elems.add(elem);
			}
		}
		return elems;
	}

	private int findElemOutLinesNum(Long startFigureId) {

		return findElemOutLines(startFigureId).size();
	}

	private int findElemInLinesNum(Long endFigureId) {

		return findElemInLines(endFigureId).size();
	}

	/**
	 * 是否为返入
	 * 
	 * @author hyl
	 * @param endElem
	 * @return
	 */
	private boolean isReverseArrowhead(FlowElem endElem) {
		if (endElem.getFigureText().equals("ReverseArrowhead")) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为协作框
	 * 
	 * @author hyl
	 * @param endElem
	 * @return
	 */
	private boolean isDottedRect(FlowElem endElem) {
		if (endElem.getFigureType().equals("DottedRect")) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为活动 将epros中的活动、决策、it决策、信息系统都视为活动
	 * 
	 * @author hyl
	 * @param endElem
	 * @return
	 */
	private boolean isActivity(FlowElem endElem) {
		return JecnUtil.isActive(endElem.getFigureType());
	}

	/**
	 * 给元素创建guid以及初始化需要的集合
	 * 
	 * @author hyl
	 * @param elems
	 */
	private void initGuidAndInitCollection(List<FlowElem> elems) throws Exception {
		for (FlowElem elem : elems) {
			String guid = UUID.randomUUID().toString();
			elem.setGuid(guid);
			elemMap.put(elem.getFigureId(), elem);

			if ("CommonLine".equals(elem.getFigureType()) || "ManhattanLine".equals(elem.getFigureType())) { // 连接线
				lineList.add(elem);
			} else if (JecnUtil.isRole(elem.getFigureType())) {
				resultRoleList.add(elem);
			} else if (isActivity(elem)) {
				activeList.add(elem);
			} else if (isToFigure(elem)) {
				toList.add(elem);
			} else if (elem.getFigureType().equals("ReverseArrowhead")) {
				fanruList.add(elem);
			} else if (elem.getFigureType().equals("RightArrowhead")) {
				fanchuList.add(elem);
			} else if (elem.getFigureType().equals("DottedRect")) {
				recList.add(elem);
			} else if (isEnd(elem)) {
				this.endElem = elem;
				this.endElem.setType(StateType.End.toString());
				this.endElem.setFigureText("结束");
			} else if (isStart(elem)) {
				this.startElem = elem;
				this.startElem.setType(StateType.Start.toString());
				this.startElem.setFigureText("开始");
			}
		}

		log.info("-------------------------");
		log.info("原始活动的总数为：" + activeList.size());
		log.info("原始线段的总数为：" + lineList.size());
		log.info("原始角色的总数为：" + resultRoleList.size());
		log.info("原始TO元素的总数为：" + toList.size());
		log.info("原始返入的总数为：" + fanruList.size());
		log.info("原始返出的总数为：" + fanchuList.size());
		log.info("并行框的总数为：" + recList.size());
	}

}
