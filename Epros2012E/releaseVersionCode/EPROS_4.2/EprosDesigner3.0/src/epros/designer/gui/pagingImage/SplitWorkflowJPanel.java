package epros.designer.gui.pagingImage;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;

import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 从workflow中获得显示图像区域面板
 */
public class SplitWorkflowJPanel extends JPanel {
	/** 面板 */
	private JecnDrawDesktopPane workflow;
	/** 放大倍数 */
	private double scaleRatio = 1.0;

	public SplitWorkflowJPanel(JecnDrawDesktopPane workflow, double scaleRatio) {
		this.workflow = workflow;
		this.scaleRatio = scaleRatio;
		// 设置panel的大小
		this.setSize(new Dimension((int) (workflow.getWidth() * scaleRatio),
				(int) (workflow.getHeight() * scaleRatio)));
	}

	@Override
	public void paintComponent(Graphics g) {
		// 获取G2d 画笔
		Graphics2D g2d = (Graphics2D) g;
		// 获取面板设置
		RenderingHints rhO = g2d.getRenderingHints();
		// 设置抗锯齿 和 抗锯齿提示值
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHints(rh);
		// 将调用面板重画到新面板上,只画图像
		AffineTransform af = g2d.getTransform();
		// 面板的缩放
		g2d.transform(AffineTransform.getScaleInstance(scaleRatio, scaleRatio));
		// 新建组件集合
		ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
		// 获取组件 修改缓冲区为false ， 实时刷新
		updateDoubleBuffered(workflow, dbcomponents);
		workflow.paint(g);
		// 设置使用缓冲区为true最后生成图形
		resetDoubleBuffered(dbcomponents);
		// 还原面板设置
		g2d.setRenderingHints(rhO);
		// 重写面板上的 AffineTransform
		g2d.setTransform(af);
		// 销毁画笔
		g2d.dispose();
	}

	/**
	 * 获取组件
	 * 
	 * @author fuzhh 2013-11-7
	 * 
	 */
	private void updateDoubleBuffered(JComponent component,
			ArrayList<JComponent> dbcomponents) {
		if (component.isDoubleBuffered()) {
			// 添加到结合
			dbcomponents.add(component);
			// 设置不使用 缓冲区
			component.setDoubleBuffered(false);
		}
		// 循环所有的组件
		for (int i = 0; i < component.getComponentCount(); i++) {
			Component c = component.getComponent(i);
			if (c instanceof JComponent) {
				updateDoubleBuffered((JComponent) c, dbcomponents);
			}
		}
	}

	/**
	 * 设置使用缓冲区
	 * 
	 * @author fuzhh 2013-11-7
	 * 
	 */
	private void resetDoubleBuffered(ArrayList<JComponent> dbcomponents) {
		for (JComponent component : dbcomponents) {
			component.setDoubleBuffered(true);
		}
	}
}
