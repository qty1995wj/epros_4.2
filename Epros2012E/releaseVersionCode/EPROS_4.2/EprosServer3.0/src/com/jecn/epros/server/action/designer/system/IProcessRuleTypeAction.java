package com.jecn.epros.server.action.designer.system;

import java.util.List;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;

public interface IProcessRuleTypeAction {
	/**
	 * 获得流程类别
	 * @throws Exception
	 */
	public List<ProceeRuleTypeBean> getFlowAttributeType() throws Exception;
	
	/**
	 * 删除 流程类别
	 * @param typeId
	 * @throws Exception
	 */
	public void delteFlowArributeType(List<Long> typeIds) throws Exception;
	
	/**
	 * 更新流程类别
	 * @param proceeRuleTypeBean
	 * @throws Exception
	 */
	public void updateFlowType(ProceeRuleTypeBean proceeRuleTypeBean) throws Exception;
	
	/***
	 * 添加流程类别
	 * @param proceeRuleTypeBean
	 * @throws Exception
	 */
	public Long addFlowType(ProceeRuleTypeBean proceeRuleTypeBean) throws Exception;
}
