package epros.designer.gui.process.activitiesProperty;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import epros.designer.gui.process.termsInOutDPopup.TremsInOrOutDiDialog;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnFigureInoutSampleT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 活动明细： 输入
 * 
 * @author 2018-8-27
 * 
 */
public class ActivitiesTermsInOutPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(ActivitiesInPanel.class);

	private static final long serialVersionUID = 1L;
	// 内容表格
	private JecnTable jecnTable = null;

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(550, 30);
	// 添加
	private JButton addBut = new JButton(JecnProperties.getValue("add"));
	// 编辑
	private JButton editBut = new JButton(JecnProperties.getValue("edit"));
	// 删除
	private JButton delBut = new JButton(JecnProperties.getValue("delete"));

	/** 上移按钮 */
	private JButton upButton = new JButton(JecnProperties.getValue("upBtn"));

	private JButton downButton = new JButton(JecnProperties.getValue("downBtn"));
	// 内容面板
	private JecnPanel contentPanel = new JecnPanel();
	/** 所在的滚动面板 */
	private JScrollPane activeScrollPane = new JScrollPane();

	private List<JecnFigureInoutT> inOutBean = new ArrayList<JecnFigureInoutT>();

	private List<JecnFigureInoutT> oldOutBean = new ArrayList<JecnFigureInoutT>();
	// 0 输入 1 输出
	private int type;

	/**
	 * 输入/输出
	 * 
	 * @param type
	 *            调用类型 0 输入 1 输出
	 */
	public ActivitiesTermsInOutPanel(List<JecnFigureInoutT> inOutBean, int type) {
		this.inOutBean = clone(inOutBean);
		this.oldOutBean = inOutBean;
		this.type = type;
		initLayout();
		initEvent();
		initData();
	}

	private void initData() {
		// 防止sortId为空 初始化时 自动根据数据顺序加载排序
		insertSort();
	}

	/**
	 * 元素属性 - 活动输入输出 数据初始化
	 * 
	 * @param inOutBean
	 * @param type
	 */
	public void initInfoPanelData(List<JecnFigureInoutT> inOutBean, int type) {
		this.inOutBean = clone(inOutBean);
		structureOldBean();
		this.type = type;

		removeAllTable();
		Vector<Vector<String>> fileData = getContent();
		for (Vector<String> v : fileData) {
			((DefaultTableModel) jecnTable.getModel()).addRow(v);
		}
		initData();
	}

	private void removeAllTable() {
		for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(index);
		}
	}

	private List<JecnFigureInoutT> clone(List<JecnFigureInoutT> inOutBean) {
		List<JecnFigureInoutT> inOutBeanClone = new ArrayList<JecnFigureInoutT>();
		for (JecnFigureInoutT jecnFigureInoutT : inOutBean) {
			JecnFigureInoutT clone = new JecnFigureInoutT();
			try {
				PropertyUtils.copyProperties(clone, jecnFigureInoutT);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			inOutBeanClone.add(clone);
		}
		return inOutBeanClone;
	}

	private void structureOldBean() {
		for (JecnFigureInoutT jecnFigure : inOutBean) {
			oldOutBean.add(jecnFigure.clone());
		}
	}

	private void initLayout() {
		contentPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		contentPanel.setBorder(BorderFactory.createTitledBorder(getType() ? JecnProperties.getValue("actOut")
				: JecnProperties.getValue("actIn")));

		activeScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(550, 150);
		activeScrollPane.setPreferredSize(dimension);
		activeScrollPane.setMaximumSize(dimension);
		activeScrollPane.setMinimumSize(dimension);

		jecnTable = new JecnTable() {

			@Override
			public boolean isSelectMutil() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public int[] gethiddenCols() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public JecnTableModel getTableModel() {
				return new JecnTableModel(getTitle(), getContent());
			}
		};
		jecnTable.setDefaultRenderer(Object.class, new TableViewRenderer());// 红色标记部分是用来渲染JTable的自定义绘制器
		// 设置默认背景色
		jecnTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		jecnTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = jecnTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(0);
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		activeScrollPane.setViewportView(jecnTable);

		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(contentPanel, c);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(buttonPanel, c);
		// 表格
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		contentPanel.add(activeScrollPane, c);

		// 移动
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, insets, 0, 0);
		contentPanel.add(getSortPanel(), c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(addBut);
		buttonPanel.add(editBut);
		buttonPanel.add(delBut);
	}

	private JecnPanel getSortPanel() {
		JecnPanel jan = new JecnPanel();
		Dimension dimension = null;
		if (JecnResourceUtil.getLocale() == Locale.CHINESE) {
			dimension = new Dimension(70, 35);
		} else {
			dimension = new Dimension(85, 70);
		}

		jan.setPreferredSize(dimension);
		jan.setMinimumSize(dimension);
		jan.setLayout(new FlowLayout(FlowLayout.CENTER));
		jan.add(upButton);
		jan.add(downButton);
		return jan;
	}

	private boolean getType() {
		return type == 0 ? false : true;
	}

	private void initEvent() {
		// 添加数据
		addBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<String> names = new ArrayList<String>();
				for (JecnFigureInoutT beans : inOutBean) {
					names.add(beans.getName());
				}
				TremsInOrOutDiDialog dialog = new TremsInOrOutDiDialog(getType(), null, names);
				dialog.setVisible(true);
				if (dialog.isOpaque() && dialog.isOk()) {
					JecnFigureInoutT inOut = dialog.getInOutBean();
					addTableRow(inOut);
				}
			}
		});
		// 编辑
		editBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnFigureInoutT oldBean = new JecnFigureInoutT();
				int selectedRow = jecnTable.getSelectedRow();
				if (inOutBean != null && selectedRow >= 0) {
					oldBean = inOutBean.get(selectedRow);
					List<String> names = new ArrayList<String>();
					for (int i = 0; i < inOutBean.size(); i++) {
						if (i != selectedRow) {
							names.add(inOutBean.get(i).getName());
						}
					}
					TremsInOrOutDiDialog dialog = new TremsInOrOutDiDialog(getType(), oldBean, names);
					dialog.setVisible(true);
					if (dialog.isOpaque() && dialog.isOk()) {
						editTableRow();
					}
				}
			}
		});
		delBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				delete();
			}
		});
		jecnTable.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// 判断为左键双击
				if (e.getClickCount() == 2 && e.getButton() == 1) {
					JecnFigureInoutT oldBean = new JecnFigureInoutT();
					int selectedRow = jecnTable.getSelectedRow();
					if (inOutBean != null && selectedRow >= 0) {
						oldBean = inOutBean.get(selectedRow);
						List<String> names = new ArrayList<String>();
						for (int i = 0; i < inOutBean.size(); i++) {
							if (i != selectedRow) {
								names.add(inOutBean.get(i).getName());
							}
						}
						TremsInOrOutDiDialog dialog = new TremsInOrOutDiDialog(getType(), oldBean, names);
						dialog.setVisible(true);
						if (dialog.isOpaque() && dialog.isOk()) {
							editTableRow();
						}
					}
				}

			}
		});
		// 上移
		upButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				upButtonAction();
			}
		});
		// 下移
		downButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downButtonAction();
			}
		});
	}

	// 上移
	public void upButtonAction() {
		int selectedRow = jecnTable.getSelectedRow();
		if (selectedRow > 0) {
			Collections.swap(inOutBean, selectedRow, selectedRow - 1);
			jecnTable.moveUpRows();
			insertSort();
		}

	}

	// 下移
	public void downButtonAction() {
		int selectedRow = jecnTable.getSelectedRow();
		if (selectedRow >= 0 && (selectedRow + 1 < inOutBean.size())) {
			Collections.swap(inOutBean, selectedRow, selectedRow + 1);
			jecnTable.moveDownRows();
			insertSort();
		}
	}

	/**
	 * 节点排序
	 */
	private void insertSort() {
		int rowCount = jecnTable.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			JecnFigureInoutT jecnFlowInoutData = inOutBean.get(i);
			jecnFlowInoutData.setSortId(Long.valueOf(i));
		}
	}

	private void delete() {
		JecnFigureInoutT oldBean = new JecnFigureInoutT();
		int selectedRow = jecnTable.getSelectedRow();
		if (inOutBean != null && selectedRow >= 0) {
			oldBean = inOutBean.get(selectedRow);
			inOutBean.remove(oldBean);
			editTableRow();
		}
	}

	/**
	 * 重新添加数据
	 * 
	 * @param resourceMap
	 */
	private void editTableRow() {
		remoeAll();
		if (inOutBean != null && !inOutBean.isEmpty()) {
			for (JecnFigureInoutT bean : inOutBean) {
				Vector<String> row = new Vector<String>();
				// ID
				row.add(bean.getId());
				// 名称
				row.add(bean.getName());
				// 模板
				row.add(getFileName(bean.getListSampleT(), 0));
				if (getType()) {
					// 样例
					row.add(getFileName(bean.getListSampleT(), 1));
				}
				// 说明
				row.add(bean.getExplain());
				((DefaultTableModel) jecnTable.getModel()).addRow(row);
			}
		}

	}

	public void remoeAll() {
		// 清空
		for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(index);
		}
	}

	/**
	 * 添加一行
	 */
	private void addTableRow(JecnFigureInoutT inOut) {
		if (inOut != null) {
			this.inOutBean.add(inOut);
			Vector<String> row = new Vector<String>();
			// ID
			row.add(inOut.getId());
			// 名称
			row.add(inOut.getName());
			// 模板
			row.add(getFileName(inOut.getListSampleT(), 0));
			if (getType()) {
				// 样例
				row.add(getFileName(inOut.getListSampleT(), 1));
			}
			// 说明
			row.add(inOut.getExplain());
			((DefaultTableModel) jecnTable.getModel()).addRow(row);
			insertSort();
		}
	}

	/**
	 * 生成table的Model
	 * 
	 * @return
	 */
	private Vector<String> getTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		title.add(JecnProperties.getValue("flowMapMdel"));
		if (getType()) {
			title.add(JecnProperties.getValue("actSample"));
		}
		title.add(JecnProperties.getValue("actBase"));
		return title;
	}

	/***************************************************************************
	 * 初始化内容
	 * 
	 * @return
	 */
	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		try {
			if (inOutBean != null && !inOutBean.isEmpty()) {
				for (JecnFigureInoutT bean : inOutBean) {
					Vector<String> row = new Vector<String>();
					// ID
					row.add(bean.getId());
					// 名称
					row.add(bean.getName());
					// 模板
					row.add(getFileName(bean.getListSampleT(), 0));
					if (getType()) {
						// 样例
						row.add(getFileName(bean.getListSampleT(), 1));
					}
					// 说明
					row.add(bean.getExplain());
					vs.add(row);
				}
			}

		} catch (Exception e) {
			log.error("ActivitiesInPanel getContent is error", e);
		}
		return vs;
	}

	protected List<JecnFigureInoutT> getData() {
		return inOutBean;
	}

	/**
	 * 
	 * @param sampleBeans
	 * @param fileType
	 *            文件类型 0 是模板文件 1 是样例文件
	 * @return
	 */
	private String getFileName(List<JecnFigureInoutSampleT> sampleBeans, int fileType) {
		StringBuilder str = new StringBuilder();
		if (sampleBeans != null) {
			for (JecnFigureInoutSampleT sampleBean : sampleBeans) {
				if (sampleBean.getType() == fileType) {
					str.append(sampleBean.getFileName());
					if (StringUtils.isNotBlank(str.toString())) {
						str.append("\n");
					}
				}
			}
		}
		if (StringUtils.isNotBlank(str.toString())) {
			str.deleteCharAt(str.length() - 1);
		}
		return str.toString();
	}

	protected boolean isUpdate() {
		if (inOutBean.size() != oldOutBean.size()) {
			return true;
		}
		if (!inOutBean.containsAll(oldOutBean)) {
			return true;
		}
		return false;
	}

	// 自定义的表格绘制器
	class TableViewRenderer extends JTextArea implements TableCellRenderer {
		public TableViewRenderer() {
			// 将表格设为自动换行
			setLineWrap(true); // 利用JTextArea的自动换行方法
		}

		public Component getTableCellRendererComponent(JTable jtable, Object obj, // obj指的是单元格内容
				boolean isSelected, boolean hasFocus, int row, int column) {
			setText(obj == null ? "" : obj.toString()); // 利用JTextArea的setText设置文本方法
			// 计算当下行的最佳高度

			// 计算了该行所有列的内容所对应的高度，挑选最高的那个
			int maxPreferredHeight = 18;
			for (int i = 1; i < jtable.getColumnCount(); i++) {
				setText("" + jtable.getValueAt(row, i));
				setSize(jtable.getColumnModel().getColumn(column).getWidth(), 0);
				maxPreferredHeight = Math.max(maxPreferredHeight, getPreferredSize().height);
			}

			if (jtable.getRowHeight(row) != maxPreferredHeight)

			{
				jtable.setRowHeight(row, maxPreferredHeight);
			}

			if (isSelected) {
				this.setBackground(jtable.getSelectionBackground());
			} else {
				this.setBackground(jtable.getBackground());
			}

			setText(obj == null ? "" : obj.toString());
			return this;
		}

	}
}
