package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.system.sysUtil.SysContents;

public class ServerStopEmailBuilder extends FixedContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo() {

		int addDays = Integer.valueOf(getData()[0].toString());

		return createEmail(addDays);
	}

	private EmailBasicInfo createEmail(int addDays) {
		EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
		// "服务器到期提醒！"
		emailBasicInfo.setSubject(SysContents.SUB_JECT);
		emailBasicInfo.setContent(getMailContent(addDays));

		return emailBasicInfo;
	}

	/**
	 * 获取正文
	 * 
	 * @param addDays
	 * @return
	 */
	private String getMailContent(int addDays) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(getContent1()).append(addDays);
		buffer.append(SysContents.MAIL_CONTENT_2);
		buffer.append(SysContents.MAIL_CONTENT_3);
		buffer.append(SysContents.MAIL_CONTENT_4);
		buffer.append(SysContents.MAIL_CONTENT_5);
		buffer.append(SysContents.MAIL_CONTENT_6);
		buffer.append(JecnConfigTool.getEmailPlatName());
		return buffer.toString();

	}

	private Object getContent1() {
		String result = "您好！<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;" + JecnConfigTool.getEmailPlatName() + "密钥";
		return result;
	}

}
