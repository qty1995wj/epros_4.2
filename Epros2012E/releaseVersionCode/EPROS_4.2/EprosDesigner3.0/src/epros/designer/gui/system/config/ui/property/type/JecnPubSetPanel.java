package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.designer.util.JecnProperties;

/**
 * 
 * 权限面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPubSetPanel extends JecnAbstractPropertyBasePanel {

	public JecnPubSetPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 表面板
		tableScrollPane = new JecnTableScrollPane(JecnProperties
				.getValue("task_TableHeaderName"),dialog);
		this.add(tableScrollPane, BorderLayout.CENTER);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 显示表数据
		tableScrollPane.initData(configTypeDesgBean.getTableItemList());
	}

	@Override
	public boolean check() {
		return true;
	}
}
