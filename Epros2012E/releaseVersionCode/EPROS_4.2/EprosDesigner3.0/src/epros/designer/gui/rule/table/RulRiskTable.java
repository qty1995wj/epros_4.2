package epros.designer.gui.rule.table;

import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.rule.JecnRiskCommon;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;
/**
 * 制度操作说明===风险表单表格
 * 2013-11-22
 *
 */
public class RulRiskTable extends JTable {
	private List<JecnRiskCommon> listJecnRiskCommon;
	private  Long titleId = null;
	public RulRiskTable(List<JecnRiskCommon> listJecnRiskCommon,Long titleId){
		this.listJecnRiskCommon = listJecnRiskCommon;
		this.titleId = titleId;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}
	public RuleRiskTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("riskNum"));
		title.add(JecnProperties.getValue("riskDesc"));
		return new RuleRiskTableMode(updateTableContent(listJecnRiskCommon),
				title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class RuleRiskTableMode extends DefaultTableModel {
		public RuleRiskTableMode(Vector<Vector<Object>> data,
				Vector<String> title) {
			super(data, title);

		}

		@Override
		public Class getColumnClass(int columnIndex) {
			return getValueAt(0, columnIndex).getClass();
		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public Vector<Vector<Object>> updateTableContent(List<JecnRiskCommon> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				JecnRiskCommon jecnRiskCommon = list.get(i);

				Vector<Object> data = new Vector<Object>();
				if (jecnRiskCommon != null) {
					data.add(jecnRiskCommon.getRiskId());
					// 风险编号
					data.add(jecnRiskCommon.getRiskNumber());
					// 风险名称
					data.add(jecnRiskCommon.getRiskName());
					vector.add(data);
				}
			}
		}
		return vector;
	}
}
