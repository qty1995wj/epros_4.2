package com.jecn.epros.server.bean.process.entry;

import java.io.Serializable;

public class JecnEntryFigureBase implements Serializable {

	private String id;
	private Long flowId;
	private Long figureId;
	private String inoutId;
	private Long entryId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getInoutId() {
		return inoutId;
	}

	public void setInoutId(String inoutId) {
		this.inoutId = inoutId;
	}

	public Long getEntryId() {
		return entryId;
	}

	public void setEntryId(Long entryId) {
		this.entryId = entryId;
	}

}
