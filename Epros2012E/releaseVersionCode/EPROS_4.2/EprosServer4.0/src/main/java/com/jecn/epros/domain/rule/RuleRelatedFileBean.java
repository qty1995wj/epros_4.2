package com.jecn.epros.domain.rule;

import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.process.relatedFile.RelatedFlowMap;
import com.jecn.epros.domain.process.relatedFile.RelatedRisk;
import com.jecn.epros.domain.process.relatedFile.RelatedStandard;

import java.util.ArrayList;
import java.util.List;

public class RuleRelatedFileBean {
    /**
     * 相关流程地图
     */
    private List<RelatedFlowMap> relatedFlowMaps = new ArrayList<RelatedFlowMap>();
    /**
     * 相关流程
     */
    private List<RelatedFlow> relatedProcesses = new ArrayList<RelatedFlow>();
    /**
     * 相关标准
     */
    private List<RelatedStandard> relatedStandards = new ArrayList<RelatedStandard>();
    /**
     * 相关风险
     */
    private List<RelatedRisk> relatedRisks = new ArrayList<RelatedRisk>();

    public List<RelatedFlowMap> getRelatedFlowMaps() {
        return relatedFlowMaps;
    }

    public void setRelatedFlowMaps(List<RelatedFlowMap> relatedFlowMaps) {
        this.relatedFlowMaps = relatedFlowMaps;
    }

    public List<RelatedFlow> getRelatedProcesses() {
        return relatedProcesses;
    }

    public void setRelatedProcesses(List<RelatedFlow> relatedProcesses) {
        this.relatedProcesses = relatedProcesses;
    }

    public List<RelatedStandard> getRelatedStandards() {
        return relatedStandards;
    }

    public void setRelatedStandards(List<RelatedStandard> relatedStandards) {
        this.relatedStandards = relatedStandards;
    }

    public List<RelatedRisk> getRelatedRisks() {
        return relatedRisks;
    }

    public void setRelatedRisks(List<RelatedRisk> relatedRisks) {
        this.relatedRisks = relatedRisks;
    }
}
