package epros.designer.gui.system.config.ui.property.type;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKJButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUIUtil;

/***
 * 文件下载次数配置 2014-08-26
 * 
 * @author Administrator
 * 
 */
public class JecnDownLoadCountPanel extends JecnAbstractPropertyBasePanel
		implements ActionListener {
	/** 下载次数标签 */
	private JLabel downLoadCountLable = new JLabel(JecnProperties.getValue("downLoadCountC"));
	/** 下载次数复选框 */
	private JecnConfigCheckBox downLoadCountBox = new JecnConfigCheckBox(JecnProperties.getValue("task_isAutoSave"));
	/** 下载次数文本框 */
	private JecnConfigTextField downLoadCountField = new JecnConfigTextField();
	/** 下载次数提醒提示信息 */
	private JecnUserInfoTextArea downLoadCountInfoArea = new JecnUserInfoTextArea();

	public JecnDownLoadCountPanel(JecnAbtractBaseConfigDialog dialog) {

		super(dialog);
		initComponents();
		initLayout();
	}

	/***
	 * 初始化
	 */
	private void initComponents() {
		downLoadCountField.setEditable(false);
		downLoadCountInfoArea.setVisible(false);
		downLoadCountField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, downLoadCountField,
						downLoadCountInfoArea));

		downLoadCountBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
//		downLoadCountField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new GridBagLayout());
		downLoadCountBox.addActionListener(this);
		downLoadCountField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, downLoadCountField,
						downLoadCountInfoArea));
	}

	/***
	 * 布局
	 */
	private void initLayout() {
		Insets insets = new Insets(5, 5, 5, 5);
		GridBagConstraints c = null;

		// 下载次数标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(downLoadCountLable, c);
		// 下载次数复选框
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(downLoadCountBox, c);
		// 下载次数文本框
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(downLoadCountField, c);

		// 下载次数提醒提示信息
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0);
		this.add(downLoadCountInfoArea, c);

		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, insets,
				0, 0);
		this.add(new JLabel(), c);
	}

	@Override
	public boolean check() {
		// 存在错误标识：true，无错：false
		boolean exsitsError = false;
		String info = getNumCheckInfo(downLoadCountField.getText());
		exsitsError = this.setErrorInfo(downLoadCountInfoArea, info);
		JecnOKJButton btn = dialog.getOkCancelJButtonPanel().getOkJButton();
		
		if(exsitsError){
			downLoadCountInfoArea.setVisible(true);
			if (btn.isEnabled()) {
				btn.setEnabled(false);
			}
			return exsitsError;
		}else{
			downLoadCountInfoArea.setVisible(false);
		}
		if (!btn.isEnabled()) {// 验证成功
			btn.setEnabled(true);
		}
		return exsitsError;
	}
	private String getNumCheckInfo(String strValidity){
		// 获取下载次数
		String reg = "^[0-9]*$";
		String tipStr = "";
		//下载次数 值
		if (downLoadCountBox.isSelected()) {
			if (strValidity == null || "".equals(strValidity)) {
				tipStr = JecnProperties.getValue("downLoadCountNotNull");
			} else if (!strValidity.matches(reg)) {
				tipStr = JecnProperties.getValue("downLoadMustNum");
			}  else if (strValidity.length() > 9 || Integer.parseInt(strValidity) > 99999999) {// 如果输入的下载次数大于2147483647则提示重新输入
				// “strValidity.length() > 9”判断是为了避免Integer.parseInt(strValidity)报错， 不能超过99999999
				tipStr = JecnProperties.getValue("downLoadMax");
			} else if (Integer.parseInt(strValidity) <= 0) {
				tipStr = JecnProperties.getValue("downLoadSuperfluousZeroC");
			} else {
				tipStr ="";
			}
		}
		return tipStr;
	}
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean
				.getOtherItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();

			if (ConfigItemPartMapMark.downLoadCountIsLimited.toString().equals(// 下载次数复选框
					mark)) {
				downLoadCountBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {// 1：启用
					downLoadCountBox.setSelected(true);
					downLoadCountField.setEditable(true);
				} else {
					downLoadCountBox.setSelected(false);
					downLoadCountField.setEditable(false);
				}
			} else if (ConfigItemPartMapMark.downLoadCountValue.toString()
					.equals(mark)) {// 下载次数输入限制次数
				downLoadCountField.setItemBean(itemBean);
				downLoadCountField.setText((itemBean.getValue() == null) ? ""
						: itemBean.getValue());
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == downLoadCountBox) { // 1：选中；0不选中
				if (downLoadCountBox.isSelected()) {// 选中
					downLoadCountBox.getItemBean().setValue(
							JecnConfigContents.ITEM_AUTO_SAVE_YES);
					downLoadCountField.setEditable(true);
					check();
				} else {
					downLoadCountBox.getItemBean().setValue(
							JecnConfigContents.ITEM_AUTO_SAVE_NO);
					downLoadCountField.setEditable(false);
					downLoadCountField.setText("");
					check();
				}
			}
		}
	}

}
