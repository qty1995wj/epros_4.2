package com.jecn.epros.server.service.dataImport.match.buss;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.jecn.epros.server.action.web.login.ad.datangyidong.DaTangYiDongAfterItem;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.XMLConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 
 * 获取远程Xml文件，读取xml中数据
 * 
 * @author zhouxy
 * 
 */
public class ImportUserByXML extends AbstractImportUser {
	private final Logger log = Logger.getLogger(ImportUserByXML.class);

	/** 部门数据集合 */
	private List<DeptMatchBean> deptList = new ArrayList<DeptMatchBean>();
	/** 岗位人员数据集合 */
	private List<UserMatchBean> userList = new ArrayList<UserMatchBean>();
	/** 岗位数据集合 */
	private List<PosMatchBean> posList = new ArrayList<PosMatchBean>();

	public ImportUserByXML(AbstractConfigBean cnfigBean) {
		super(cnfigBean);

		// 下载待导入xml文件的到本地，再读取此本地xml文件中数据
		downAndReadXmlData();
	}

	@Override
	public List<DeptMatchBean> getDeptBeanList() throws Exception {
		return deptList;
	}

	@Override
	public List<UserMatchBean> getUserBeanList() throws Exception {
		return userList;
	}

	@Override
	public List<PosMatchBean> getPosBeanList() throws Exception {
		return this.posList;
	}

	/**
	 * 
	 * 下载待导入xml文件的到本地，再读取此本地xml文件中数据
	 * 
	 */
	private void downAndReadXmlData() {

		// 获取待导入的xml数据路径
		XMLConfigBean xmlConfigBean = (XMLConfigBean) configBean;
		String xmlFilePath = xmlConfigBean.getSrcXmlFilePath();
		if (StringUtils.isBlank(xmlFilePath)) {
			return;
		}

		// 获取本地文件路径
		String locatPath = "./sameNamesExcel.xls";

		// 从指定地址下载文件到本地
		boolean ret = readFileToLocal(xmlFilePath, locatPath);
		if (!ret) {
			return;
		}

		try {// 读取xml文件中数据
			ret = readXmlData(locatPath);
		} catch (Exception e) {
			throw new RuntimeException("其他错误。locatPath=" + locatPath, e);
		}
	}

	/**
	 * 获取给定文件全路径
	 * 
	 * @return String 文件全路径 和NULL
	 */
	// public static String getSyncPath(String fileName) {
	// // 路径
	// String path = null;
	//
	// // 打印文件路径
	// log.info("JecnFileUtil类getSyncPath方法：参数文件名称fileName=" + fileName);
	// if (Tool.isNullOrEmtryTrim(fileName)) {
	// log.info("JecnFileUtil类getSyncPath方法：文件名称为null或空。fileName="
	// + fileName);
	// return path;
	// }
	// // 获取路径
	// URL url = Tool.class.getResource(fileName);
	// if (url == null) {
	// log
	// .info("JecnFileUtil类getSyncPath方法：找不到指定的文件。fileName="
	// + fileName);
	// return path;
	// }
	//
	// try {
	// path = url.toURI().getPath();
	// } catch (URISyntaxException e) {
	// // 这儿一般是不进入此分支的
	// log.error("JecnFileUtil类getSyncPath方法：path=" + path, e);
	// path = url.getPath().replaceAll("%20", " ");
	// }
	// // 打印文件路径
	// log.info("JecnFileUtil类getSyncPath方法：全路径path=" + path);
	// return path;
	// }

	/**
	 * 
	 * <?xml version="1.0" encoding="utf-8"?> <company id="0" name="大唐移动"> <Dept
	 * ID="10000" PID="0" Name="1大唐移动" DeptName="大唐移动通信设备有限公司"> <Member
	 * Staff_num="20368" Staff_name="周兴2" Job_name="公司知识产权总监1" /> <Member
	 * Staff_num="20365" Staff_name="周兴裕" Job_name="公司知识产权总监1" /> <Member
	 * Staff_num="20365" Staff_name="周兴裕" Job_name="公司知识产权总监2" /> <Member
	 * Staff_num="20366" Staff_name="小虎" Job_name="公司知识产权总监" /> <Member
	 * Staff_num="21366" Staff_name="测试1" Job_name="公司知识产权总监2" /> <Member
	 * Staff_num="21367" Staff_name="测试2" Job_name="公司知识产权总监2" />
	 * 
	 * <Dept ID="10001" PID="10000" Name="2技术决策" DeptName="技术决策委员会"> <Member
	 * Staff_num="20365" Staff_name="周兴裕" Job_name="公司知识产权总监4" /> <Member
	 * Staff_num="20000" Staff_name="李世鹤" Job_name="高级技术顾问" /> <Member
	 * Staff_num="20001" Staff_name="陈洪涛" Job_name="质量部副总经理" /> </Dept> </Dept>
	 * <Dept ID="30000" PID="0" Name="1上海大唐移动" DeptName="上海大唐移动通信设备有限公司">
	 * <Member Staff_num="20365" Staff_name="周兴裕" Job_name="公司知识产权总监3" />
	 * <Member Staff_num="20367" Staff_name="小王" Job_name="会计专员" /> </Dept>
	 * </company>
	 * 
	 * 
	 * 读取xml文件数据
	 * 
	 * @param locatPath
	 *            String 本地xml文件路径
	 * 
	 * @return boolean true:读取成功；false：读取失败
	 * 
	 */
	private boolean readXmlData(String locatPath) {
		// 读取xml文件数据
		// 读取数据
		
		SAXReader reader = new SAXReader();
		Document doc = null;
		try {
			doc = reader.read(locatPath);
		} catch (DocumentException e) {
			log.error("解析xml报错。locatPath=" + locatPath, e);
		} catch (Exception e) {
			log.error("其他错误。locatPath=" + locatPath, e);
		}
		if (doc == null) {
			return false;
		}

		// 根节点
		Element root = doc.getRootElement();
		// 对应公司节点
		DeptMatchBean companyBean = new DeptMatchBean();
		// 编号
		// String id = getTextTrim(root.attributeValue("id"));
		// 名称
		String name = getTextTrim(root.attributeValue("name"));

		// 编号 (大唐顶级部门编号为0 与Epros顶级部门编号冲突 。修改为-1) 3.0以后做出的数据调整
		companyBean.setDeptNum("-1");
		// 名称
		companyBean.setDeptName(name);
		// 设置父节点ID是0
		companyBean.setPerDeptNum("0");

		deptList.add(companyBean);

		// 递归实现读取部门、岗位以及人员数据
		readEleData(root, deptList, userList, companyBean);

		return true;
	}

	/**
	 * 
	 * 读取xml文件数据
	 * 
	 * List对象存放N个部门； 一个部门对象中存放N个岗位；一个岗位对象存放一个人员对象
	 * 
	 * @param deptList
	 *            List<DeptBean> 部门数据集合
	 * @param userList
	 *            List<UserBean> 岗位人员数据集合
	 * @param PDptBean
	 *            DeptBean 父部门对象
	 * 
	 */
	private void readEleData(Element e, List<DeptMatchBean> deptList,
			List<UserMatchBean> userList, DeptMatchBean PDptBean) {

		// 获取子节点
		List<Element> eList = e.elements();
		Set<String> posSet = new HashSet<String>();
		for (Element e1 : eList) {

			if ("Dept".equals(e1.getName())) {// 部门节点

				// <Dept ID="10001" PID="10000" Name="决策" DeptName="技术决策委员会">

				DeptMatchBean deptBean = new DeptMatchBean();

				// 部门编号
				String deptNum = getTextTrim(e1.attributeValue("ID"));
				// 部门名称
				String deptName = getTextTrim(e1.attributeValue("DeptName"));
				// // 父编号
				String perDeptNum = getTextTrim(e1.attributeValue("PID"));
				// 校验PID是否和父节点的DeptNum一致
				if (e.isRootElement()) {// 父节点为根节点
					if (!"0".equals(perDeptNum)) {
						throw new RuntimeException("XML数据异常:根节点下的子节点PID不为 0");
					}
				} else if (!PDptBean.getDeptNum().equals(perDeptNum)) {
					throw new RuntimeException("XML数据异常：父节点ID为"
							+ PDptBean.getDeptNum() + "，子节点PID为：" + perDeptNum);
				}
				if (Integer.valueOf(deptNum) < 0) {
					throw new RuntimeException("XML数据异常：部门ID为负数：" + deptNum);
				}
				// 部门编号
				deptBean.setDeptNum(deptNum);
				// 部门名称
				deptBean.setDeptName(deptName);
				// 父编号
				deptBean.setPerDeptNum(PDptBean.getDeptNum());

				deptList.add(deptBean);

				readEleData(e1, deptList, userList, deptBean);

			} else if ("Member".equals(e1.getName())) {// 岗位节点

				// Staff_num:人员登录名称；Staff_name：真实姓名；Job_name：岗位名称
				// <Member Staff_num="20"Staff_name="张三"Job_name="总监"/>

				UserMatchBean userBean = new UserMatchBean();

				// 岗位名称
				String posName = getTextTrim(e1.attributeValue("Job_name"));
				// 人员登录名称
				String loginName = getTextTrim(e1.attributeValue("UserName"));
				// 真实姓名
				String trueName = getTextTrim(e1.attributeValue("Staff_name"));
				// email前缀
				String email = getTextTrim(e1.attributeValue("UserName"));
				
				// 人员登录名称
				userBean.setLoginName(loginName);
				// 真实姓名
				userBean.setTrueName(trueName);
				// 岗位编号:部门编号_岗位名称
				userBean.setPosNum(PDptBean.getDeptNum() + "-" + posName);
				// Email
				if (StringUtils.isNotBlank(email)&&StringUtils.isNotBlank(DaTangYiDongAfterItem.EMAIL)) {
					userBean.setEmail(email +DaTangYiDongAfterItem.EMAIL);
					userBean.setEmailType(0);
				}
				userList.add(userBean);

				if (!posSet.contains(userBean.getPosNum())) {
					// 设置岗位
					PosMatchBean posBean = new PosMatchBean();
					// 基准岗位编号
					posBean.setBasePosNum(MatchTool.emtryToNull(userBean
							.getPosNum()));
					// 基准岗位名称
					posBean.setBasePosName(MatchTool.emtryToNull(posName));
					// 实际岗位编号
					posBean.setActPosName(MatchTool.emtryToNull(posName));
					// 实际岗位名称
					posBean.setActPosNum(MatchTool.emtryToNull(userBean
							.getPosNum()));
					// 岗位所属部门编号
					posBean.setDeptNum(MatchTool.emtryToNull(PDptBean
							.getDeptNum()));
					posList.add(posBean);
					posSet.add(userBean.getPosNum());
				}
			}
		}
	}

	/**
	 * 
	 * 去空
	 * 
	 * @param text
	 *            String
	 * 
	 * @return String ""或去空值
	 * 
	 */
	private String getTextTrim(String text) {
		return (StringUtils.isBlank(text)) ? "" : text.trim();
	}

	/**
	 * 
	 * 从给定的url下载文件
	 * 
	 * @param destUrl
	 *            String 源文件路径
	 * @param localPath
	 *            String 本地文件路径
	 * 
	 * @return boolean true:下载成功；false：下载失败
	 * 
	 */
	private boolean readFileToLocal(String destUrl, String localPath) {

		HttpURLConnection httpUrl = null;

		InputStream inputStream = null;
		BufferedInputStream bis = null;

		FileOutputStream fos = null;

		try {
			// 建立链接
			URL url = new URL(destUrl);
			httpUrl = (HttpURLConnection) url.openConnection();
			// 连接指定的资源
			httpUrl.connect();
			inputStream = httpUrl.getInputStream();

			// 获取网络输入流
			bis = new BufferedInputStream(inputStream);

			// 建立文件
			fos = new FileOutputStream(localPath);
			// 写文件
			byte[] buf = new byte[1024];
			int size = 0;
			while ((size = bis.read(buf)) != -1) {
				fos.write(buf, 0, size);
			}
			fos.flush();
			return true;
		} catch (Exception e) {
			log.error("", e);
			return false;
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				log.error("关闭流FileOutputStream失败", e);
			}
			try {
				bis.close();
			} catch (IOException e) {
				log.error("关闭流BufferedInputStream失败", e);
			}
			try {
				inputStream.close();
			} catch (IOException e) {
				log.error("关闭流InputStream失败", e);
			}
			try {
				httpUrl.disconnect();
			} catch (Exception e) {
				log.error("关闭httpUrl失败", e);
			}

		}
	}
}
