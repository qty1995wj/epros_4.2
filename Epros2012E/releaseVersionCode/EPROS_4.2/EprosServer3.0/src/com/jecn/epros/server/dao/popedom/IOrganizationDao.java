package com.jecn.epros.server.dao.popedom;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IOrganizationDao extends IBaseDao<JecnFlowOrg, Long> {
	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：获得项目下所有岗位
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getAllPosByProjectId(Long projectId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：获得项目下所有部门
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getAllOrgByProjectId(Long projectId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：获得部门下的岗位
	 * @param orgId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getPosByOrgId(Long orgId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-17
	 * @description:
	 * @param orgId组织ID
	 * @param projectId项目ID
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getPosAndPersonByOrgId(Long orgId, Long projectId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：搜索部门
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getSearchOrgIds(Long orgId, Long projectId) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除部门
	 * @param setOrgIds
	 * @throws Exception
	 */
	public void deleteOrgs(Set<Long> setOrgIds) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除部门
	 * @param projectId
	 * @throws Exception
	 */
	public void deleteOrgs(Long projectId) throws Exception;

	/**
	 * 获得项目下，部门下的所有主导流程
	 * 
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findResFlowsByOrgIdAndProjectId(Set<Long> setOrgIds, Long projectId) throws Exception;

	/**
	 * @author zhangchen Aug 14, 2012
	 * @description：获得关键活动(部门下和项目)
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws DaoException
	 */
	public List<Object[]> findKeyActivesByOrgIdAndProjectId(Set<Long> setOrgIds, Long projectId) throws Exception;

	/**
	 * @author zhangchen Aug 14, 2012
	 * @description：获得KPI(部门下和项目)
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws DaoException
	 */
	public List<Object[]> findKPIByOrgIdAndProjectId(Set<Long> setOrgIds, Long projectId) throws Exception;

	public List<JecnTreeBean> listFlowFileDownloadPosPerms(Long id) throws Exception;

	public List<Object[]> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception;

	public List<Object[]> getRoleAuthChildPersonOrgs(Long pId, Long projectId, Long peopleId);

	List<Object[]> getRoleAuthPosAndPersonByOrgId(Long orgId, Long projectId, Long peopleId) throws Exception;

	public List<Object[]> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;
}
