package epros.designer.gui.rule;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;

/***
 * 新建制度目录
 * 
 * @author 2012-06-08
 * 
 */
public class AddRuleDirDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(AddRuleDirDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddRuleDirDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("newRuleDir");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		// 向Rule文件中添加数据
		RuleT rulet = new RuleT();
		rulet.setRuleName(getName());
		rulet.setPerId(pNode.getJecnTreeBean().getId());
		rulet.setProjectId(JecnConstants.projectId);
		rulet.setSortId(JecnTreeCommon.getMaxSort(pNode));
		// 0是目录，1是制度,2是制度文件
		rulet.setIsDir(0);
		// Date createDatime = new Date();
		// rulet.setCreateDate(createDatime);
		rulet.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// rulet.setUpdateDate(createDatime);
		rulet.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		rulet.setIsPublic(JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubRule) ? 1L : 0L);
		// 向数据库中添加制度目录
		Long id = null;
		try {
			id = ConnectionPool.getRuleAction().addRule(rulet);
			// 向树节点添加制度目录节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.ruleDir);
			jecnTreeBean.setPub(true);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
		} catch (Exception e) {
			log.error("AddRuleDirDialog saveData is error", e);
		}
		// 关闭窗体
		this.dispose();
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		try {
			return ConnectionPool.getRuleAction().validateAddNameDirRepeat(pNode.getJecnTreeBean().getId(), name);
		} catch (Exception e) {
			log.error("", e);
		}
		return true;
	}

}
