package com.jecn.epros.server.action.designer.popedom.impl;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.action.designer.popedom.IPersonAction;
import com.jecn.epros.server.bean.log.JecnPersonalPermRecord;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.LoginBean;
import com.jecn.epros.server.bean.popedom.UserAddReturnBean;
import com.jecn.epros.server.bean.popedom.UserEditBean;
import com.jecn.epros.server.bean.popedom.UserPageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.util.JecnDesignerPassward;
import com.jecn.epros.server.util.JecnUserSecurityKey;

public class PersonActionImpl implements IPersonAction {

	private IPersonService personService;

	public IPersonService getPersonService() {
		return personService;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	@Override
	public UserAddReturnBean savePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception {
		return personService.savePerson(jecnUser, figureIds, roleIds);
	}

	@Override
	public UserAddReturnBean updatePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception {
		return personService.updatePerson(jecnUser, figureIds, roleIds);
	}

	// @Override
	// public UserPageBean searchUser(List<Long> listOrgIds, String roleType,
	// String loginName, String trueName, int curPage, String operationType,
	// int pageSize,Long projectId) throws Exception{
	// return personService.searchUser(listOrgIds, roleType, loginName,
	// trueName, curPage, operationType, pageSize,projectId);
	// }

	@Override
	public UserPageBean searchUser(Long orgId, String roleType, String loginName, String trueName, int curPage,
			String operationType, int pageSize, Long projectId, Long isDelete, boolean isLogin) throws Exception {
		return personService.searchUser(orgId, roleType, loginName, trueName, curPage, operationType, pageSize,
				projectId, isDelete, isLogin);
	}

	@Override
	public List<JecnTreeBean> searchUserByName(String name, Long projectId) throws Exception {
		return personService.searchUserByName(name, projectId);
	}

	@Override
	public UserEditBean getUserEidtBeanByPeopleId(Long peopleId) throws Exception {
		return personService.getUserEditBeanByPeopleId(peopleId);
	}

	@Override
	public void dischargeLogin(Long peopleId) throws Exception {
		personService.updateLogin(peopleId);

	}

	@Override
	public int delUsers(List<Long> listPeopleIds) throws Exception {
		return personService.delUsers(listPeopleIds);

	}

	@Override
	public boolean validateAddLoginName(String loginName) throws Exception {
		List<JecnUser> list = personService.getUserByLoginName(loginName);
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean validateUpdateLoginName(String loginName, Long id) throws Exception {
		List<JecnUser> list = personService.getUserByLoginName(loginName, id);
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<JecnTreeBean> getPersonByPosition(Long positionId) throws Exception {
		return personService.getChildPoss(positionId);
	}

	/**
	 * @param loginName
	 * @param password
	 * @param macAddressClient
	 * @param loginType
	 *            : 0输入密码登录； 1 记住密码登录 在密码框有输入时成0; 2：域用户登录,3:刷新
	 * @return LoginBean
	 */
	@Override
	public LoginBean loginDesign(String loginName, String password, String macAddressClient, int loginType)
			throws Exception {
		String pass = "";
		if (loginType == 1) {
			pass = JecnUserSecurityKey.getsecret(JecnDesignerPassward.getDesignerDecrypt(password));
		} else if (loginType == 0) {
			pass = JecnUserSecurityKey.getsecret(password);
			password = JecnDesignerPassward.getDesignerEncrypt(password);
		}
		LoginBean loginBean = personService.loginDesign(loginName, pass, macAddressClient, loginType);
		JecnUser user = loginBean.getJecnUser();
		if (user != null) {
			user.setPassword(password);
		}
		return loginBean;
	}

	@Override
	public void loginOut(Long userId) throws Exception {
		personService.moveEditAllByLoginName(userId);
	}

	@Override
	public void restorationUsers(List<Long> listPeopleIds) throws Exception {
		personService.restorationUsers(listPeopleIds);
	}

	@Override
	public String userPasswordUpdate(Long userId, String newPassword) throws Exception {
		personService.userPasswordUpdate(userId, JecnUserSecurityKey.getsecret(newPassword));
		return JecnDesignerPassward.getDesignerEncrypt(newPassword);

	}

	@Override
	public List<Long> getAllAdminList() throws Exception {
		return personService.getAllAdminList();
	}

	/**
	 * 添加人员 验证最大用户
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public int addPeopleValidate() throws Exception {
		return personService.addPeopleValidate();
	}

	/**
	 * 角色权限获取人员选择结果集
	 * 
	 * @param name
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		if (peopleId == null) {
			return this.searchUserByName(name, projectId);
		}
		return personService.searchRoleAuthByName(name, projectId, peopleId);
	}

	@Override
	public JecnUser getJecnUserByPeopleId(Long peopleId) {
		return personService.getJecnUserByPeopleId(peopleId);
	}

	@Override
	public JecnPersonalPermRecord getPersionalPermRecord(Long id, int type, Long peopleId) throws Exception {
		return personService.getPersionalPermRecord(id, type, peopleId);
	}

	@Override
	public Map<String, String> getJecnIftekSyncUser(List<String> name){
		return personService.getJecnIftekSyncUser(name);
	}

	@Override
	public List<JecnTreeBean> searchUserByIftekSyncLoginName(List<String> names, Long projectId) {
		return personService.searchUserByIftekSyncLoginName(names,projectId);
	}

	@Override
	public String getListUserSearchBean(String loginName) {
		return personService.getListUserSearchBean(loginName);
	}
	
	
	
}
