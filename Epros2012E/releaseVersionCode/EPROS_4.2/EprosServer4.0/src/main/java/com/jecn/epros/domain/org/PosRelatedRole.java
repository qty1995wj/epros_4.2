package com.jecn.epros.domain.org;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;

import java.util.ArrayList;
import java.util.List;

/**
 * 岗位参与的角色
 *
 * @author lenovo
 */
public class PosRelatedRole {
    private String roleName;
    private String roleRespon;
    private Long flowId;
    private String flowName;
    private List<PosRelatedActive> activities;

    public LinkResource getProcessLink() {
        return new LinkResource(flowId, flowName, ResourceType.PROCESS);
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleRespon() {
        return roleRespon;
    }

    public void setRoleRespon(String roleRespon) {
        this.roleRespon = roleRespon;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    @JsonIgnore
    public List<PosRelatedActive> getActivities() {
        return activities;
    }

    public void setActivities(List<PosRelatedActive> activities) {
        this.activities = activities;
    }

    public List<LinkResource> getActivityLinks() {
        List<LinkResource> links = new ArrayList<>();
        for (PosRelatedActive active : activities) {
            links.add(active.getLink());
        }
        return links;
    }
}
