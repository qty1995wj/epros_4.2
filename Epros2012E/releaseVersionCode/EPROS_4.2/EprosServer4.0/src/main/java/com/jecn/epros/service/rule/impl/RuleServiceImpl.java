package com.jecn.epros.service.rule.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.*;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.file.FileDirCommonBean;
import com.jecn.epros.domain.inventory.FileInventoryRelateBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.processFile.OperationFile;
import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.process.relatedFile.RelatedFlowMap;
import com.jecn.epros.domain.process.relatedFile.RelatedRisk;
import com.jecn.epros.domain.process.relatedFile.RelatedStandard;
import com.jecn.epros.domain.report.InventoryBaseInfo;
import com.jecn.epros.domain.rule.*;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.mapper.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.CommonServiceUtil;
import com.jecn.epros.service.ServiceExpandTreeData;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.TreeNodeUtils.TreeType;
import com.jecn.epros.service.common.CommonService;
import com.jecn.epros.service.rule.RuleOperationEnum;
import com.jecn.epros.service.rule.RuleService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Params;
import com.jecn.epros.util.Utils;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class RuleServiceImpl implements RuleService {

    @Autowired
    private RuleMapper ruleMapper;

    @Autowired
    private OrgMapper orgMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private InventoryMapper inventoryMapper;

    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private CommonService commonService;

    @Autowired
    private StandardMapper standardMapper;

    /**
     * 树展开
     */
    @Override
    public List<TreeNode> findChildRules(Map<String, Object> map) {
        map.put("typeAuth", TYPEAUTH.RULE);
        boolean isAdmin = (boolean) map.get("isAdmin");
        List<TreeNode> treeBeans = ruleMapper.findChildFlowsAdmin(map);

        treeBeans = ServiceExpandTreeData.findExpandNodesByDataList(treeBeans, TreeType.rule);
        return TreeNodeUtils.setNodeType(treeBeans, TreeType.rule);
    }

    /**
     * historyId为空 查询制度
     *
     * @param map
     * @return
     */
    public Rule findRule(Map<String, Object> map) {
        boolean isPub = Common.isPub(map.get("isPub"));
        // 制度对象
        Rule rule = null;
        if (isPub) {
            rule = ruleMapper.getRule(Long.valueOf(map.get("id").toString()));
        } else {
            rule = ruleMapper.getRuleByTRuleId(Long.valueOf(map.get("id").toString()));
        }
        return rule;
    }

    /**
     * 存在historyId 查询制度文控表信息
     *
     * @param map
     * @return
     */
    public Rule findRuleHistory(Map<String, Object> map) {
        // 制度对象
        Rule rule = null;
        rule = ruleMapper.getRuleHistory(Long.valueOf(map.get("historyId").toString()));
        return rule;
    }

    /**
     * 制度基本信息
     */
    @Override
    public List<RuleAttribute> findBaseInfoBean(Map<String, Object> map) {
        RuleBaseInfoBean ruleInfoBean = new RuleBaseInfoBean();
        List<RuleAttribute> attributes = new ArrayList<>();
        boolean isPub = Common.isPub(map.get("isPub"));
        long historyId = (long) map.get("historyId");
        String isAp = map.get("isApp").toString();
        Rule rule = null;
        RuleBaseInfo baseInfo = null;
        if (historyId == 0) {//判断historyId为空执行
            rule = findRule(map);
            baseInfo = ruleMapper.findRuleBaseInfo(map);
        } else {
            rule = findRuleHistory(map);//判断historyId不为空 查询 制度文控表表数据
            baseInfo = ruleMapper.findRuleBaseHistoryInfo(map);//查询制度基本信息文控
        }
        if (rule == null) {
            return attributes;
        }
        // 1、制度名称
        addNameAttribute(attributes, rule, isPub, isAp);
        // 2、编号
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.ruleCode)) {
            attributes.add(new RuleAttribute(getConfigName("ruleCode"), rule.getRuleNumber(), null, null));
        }
        // 11、保密级别
        String isPubStr = JecnProperties.getValue("scopeOfAuthorityC");
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.ruleScurityLevel)) {
            attributes.add(new RuleAttribute(getConfigName("ruleScurityLevel"), rule.getConfidentialityLevelStr(), null, null));
            isPubStr = JecnProperties.getValue("levelC");
            ;
        }
        // 3、密级
        attributes.add(new RuleAttribute(isPubStr, ConfigUtils.getSecretByType(rule.getIsPublic() == null ? "0" : rule.getIsPublic()), null, null));
        // 4、类别
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleType.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleType"), orgMapper.findTypeNameById(rule.getTypeId()), null, null));
        }
        // 5、责任人
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleResPeople.toString())) {
            addResPeopleAttribute(attributes, rule);
        }
        //拟制人
        if ("1".equals(ConfigUtils.getValue("ruleFictionPeople"))) {
            addFictionAttribute(attributes, rule);
        }
        //专员
        if ("1".equals(ConfigUtils.getValue("isShowRuleCommissioner"))) {
            addCommissionerAttribute(attributes, rule);
        }

        // 4、关键字
        if ("1".equals(ConfigUtils.getValue("isShowRuleKeyWord"))) {
            attributes.add(new RuleAttribute(getConfigName("isShowRuleKeyWord"), rule.getKeyWord(), null, null));
        }

        // 6、监护人
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleGuardianPeople.toString())) {
            addGuardianUser(attributes, rule);
        }

        // 7、责任部门
        // 6、监护人
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleResDept.toString())) {
            if (rule.getOrgId() != null) {
                attributes.add(new RuleAttribute(getConfigName("ruleResDept"), null, new LinkResource(rule.getOrgId(), orgMapper.findOrgNameById(rule.getOrgId()), ResourceType.ORGANIZATION), null));
            } else {
                attributes.add(new RuleAttribute(getConfigName("ruleResDept"), null, null, null));
            }
        }
        // 8、有效期
        attributes.add(new RuleAttribute(JecnProperties.getValue("termOfValiditC"), rule.getExpiry() == null || 0 == rule.getExpiry().intValue() ? JecnProperties.getValue("permanent") : rule.getExpiry().toString(), null, null));

        // 生效日期
        if (ConfigUtils.isMengNiuLogin()) {
            if (rule.getPubTime() != null)
                attributes.add(new RuleAttribute(JecnProperties.getValue("dateOfEntryIntoForceC"), Common.getStringbyDate(Common.getDateAddNumDay(rule.getPubTime(), 3)), null, null));
        } else {
            // 9、生效日期
            if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.isShowEffectiveDate.toString())) {
                // 生效日期
                attributes.add(new RuleAttribute(getConfigName("isShowEffectiveDate"), rule.getEffectiveTime() != null ? Common.getStringbyDate(rule.getEffectiveTime()) : "", null, null));
                //有效期
                int expiry = Integer.valueOf(rule.getExpiry() != null ? rule.getExpiry().toString() : "0");
                Date trueEEffectiveTime = rule.getEffectiveTime();
                if (trueEEffectiveTime == null) {
                    trueEEffectiveTime = rule.getPubTime();
                }
                if (expiry > 0) {
                    attributes.add(new RuleAttribute(JecnProperties.getValue("expirationDate"), trueEEffectiveTime != null ? Common.getStringbyDate(Common.getDateAddNumMonth(trueEEffectiveTime, expiry)) : "", null, null));
                } else {
                    attributes.add(new RuleAttribute(JecnProperties.getValue("expirationDate"), JecnProperties.getValue("permanent"), null, null));
                }
            }

        }
        // 9、业务范围
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleBusinessScope.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleBusinessScope"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getBusinessScope()), null, null));
        }
        // 10、其它范围
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleOtherScope.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleOtherScope"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getOtherScope()), null, null));
        }

        //  if (rule.getIsDir() == 1) {
        //制度模板文件-添加制度属性
        addMengNiuAttrs(map, baseInfo, attributes);
        //  }
        return attributes;
    }

    private void addMengNiuAttrs(Map<String, Object> map, RuleBaseInfo baseInfo, List<RuleAttribute> attributes) {
        // 目的
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.rulePurpose.toString())) {
            attributes.add(new RuleAttribute(getConfigName("rulePurpose"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getPurpose()), null, null));
        }
        // 适用范围
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleApplicability.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleApplicability"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getApplicability()), null, null));
        }

        // 文件换版说明
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleChangeVersionExplain.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleChangeVersionExplain"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getChangeVersionExplain()), null, null));
        }
        // 文件编写部门、归口部门
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleRriteDept.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleRriteDept"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getWriteDept()), null, null));
        }
        // 起草人
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleDraftMan.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleDraftMan"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getDraftman()), null, null));
        }
        // 起草单位
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleDraftingDept.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleDraftingDept"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getDraftingUnit()), null, null));
        }
        // 试行版文件
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleTestRunFile.toString())) {
            attributes.add(new RuleAttribute(getConfigName("ruleTestRunFile"), baseInfo == null ? "" : Common.replaceWarp(baseInfo.getTestRunFile()), null, null));
        }
        // 11、组织范围
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleOrgScope.toString())) {
            addOrgApplyAttribute(map, attributes);
        }
    }

    private void addOrgApplyAttribute(Map<String, Object> map, List<RuleAttribute> attributes) {
        List<BaseBean> applyOrgList = ruleMapper.findRuleApplyOrg(map);
        if (applyOrgList != null) {
            List<LinkResource> applyOrgs = new ArrayList<>();
            LinkResource link = null;
            for (BaseBean baseBean : applyOrgList) {
                link = new LinkResource(baseBean.getId(), baseBean.getName(), ResourceType.ORGANIZATION);
                applyOrgs.add(link);
            }
            attributes.add(new RuleAttribute(getConfigName("ruleOrgScope"), null, null, applyOrgs));
        } else {
            attributes.add(new RuleAttribute(getConfigName("ruleOrgScope"), null, null, null));
        }
    }

    private void addResPeopleAttribute(List<RuleAttribute> attributes, Rule rule) {
        if (rule.getAttchMentId() != null && rule.getTypeResPeople() != null) {
            LinkResource link = null;
            if (rule.getTypeResPeople().intValue() == 0) { // 人员
                JecnUser jecnUser = this.userMapper.getUser(rule.getAttchMentId());
                if (jecnUser != null) {
                    link = new LinkResource(jecnUser.getId(), IfytekCommon.getIflytekUserName(jecnUser.getName(), 0), ResourceType.PERSON);
                }
            } else { // 岗位
                link = new LinkResource(rule.getAttchMentId(), this.orgMapper.findPostNameById(rule.getAttchMentId()), ResourceType.POSITION);
            }
            attributes.add(new RuleAttribute(getConfigName("ruleResPeople"), null, link, null));
        } else {
            attributes.add(new RuleAttribute(getConfigName("ruleResPeople"), null, null, null));
        }
    }

    //专员
    private void addCommissionerAttribute(List<RuleAttribute> attributes, Rule rule) {
        LinkResource link = null;
        if (rule.getCommissionerId() != null) {
            JecnUser jecnUser = this.userMapper.getUser(rule.getCommissionerId());
            if (jecnUser != null) {
                link = new LinkResource(jecnUser.getId(), IfytekCommon.getIflytekUserName(jecnUser.getName(), 0), ResourceType.PERSON);
            }
            attributes.add(new RuleAttribute(getConfigName("isShowRuleCommissioner"), null, link, null));
        } else {
            attributes.add(new RuleAttribute(getConfigName("isShowRuleCommissioner"), null, null, null));
        }
    }

    private void addFictionAttribute(List<RuleAttribute> attributes, Rule rule) {
        LinkResource link = null;
        if (rule.getFictionPeopleId() != null) {
            JecnUser jecnUser = this.userMapper.getUser(rule.getFictionPeopleId());
            if (jecnUser != null) {
                link = new LinkResource(jecnUser.getId(), IfytekCommon.getIflytekUserName(jecnUser.getName(), 0), ResourceType.PERSON);
            }
            attributes.add(new RuleAttribute(getConfigName("ruleFictionPeople"), null, link, null));
        } else {
            attributes.add(new RuleAttribute(getConfigName("ruleFictionPeople"), null, null, null));
        }
    }

    private String getConfigName(String mark) {
        return ConfigUtils.getName(mark) + (AuthenticatedUserUtil.isLanguageType() == 0 ? "：" : ":");

    }

    private void addNameAttribute(List<RuleAttribute> attributes, Rule rule, boolean isPub, String isApp) {
        RuleAttribute nameAttribute = new RuleAttribute();
        attributes.add(nameAttribute);
        nameAttribute.setTitle(JecnProperties.getValue("ruleNameC"));

        if (rule.getIsDir() == 2) {
            Long relatedId = rule.getFileId();
            LinkResource link = null;
            if (rule.getIsFileLocal() == 1) {
                link = new LinkResource(relatedId, rule.getRuleName(), ResourceType.RULE);
                nameAttribute.setLink(link);
            } else if (rule.getIsFileLocal() == 0) {
                Params params = new Params("fileId", relatedId);
                params.add("isPub", isPub);
                String pub = fileMapper.judgmentFileExists(params.toMap());//判断文件是否存在
                if (pub != null) {
                    switch (pub) {
                        case "1":
                            nameAttribute.setText(rule.getRuleName() + JecnProperties.getValue("falseDeletion"));
                            break;
                        case "0":
                            link = new LinkResource(relatedId, rule.getRuleName(), ResourceType.FILE);
                            nameAttribute.setLink(link);
                            break;
                        case "2":
                            nameAttribute.setText(rule.getRuleName() + JecnProperties.getValue("repealed"));
                            break;
                        default:
                            break;
                    }
                }
            } else {
                link = new LinkResource(null, rule.getRuleName(), null);
                nameAttribute.setHrefURL(rule.getRuleUrl());
                nameAttribute.setLink(link);
            }
        } else {
            nameAttribute.setText(rule.getRuleName());
        }

        if ("true".equals(isApp)) {// 手机端 连接
            nameAttribute.setLink(CommonServiceUtil.getRuleNameLink(rule, isApp));
        }
    }

    private void addGuardianUser(List<RuleAttribute> attributes, Rule rule) {
        if (rule.getGuardianId() != null) {
            JecnUser guardianUser = userMapper.getUser(rule.getGuardianId());
            if (guardianUser != null) {
                LinkResource link = new LinkResource(guardianUser.getId(), IfytekCommon.getIflytekUserName(guardianUser.getName(), 0), ResourceType.PERSON);
                attributes.add(new RuleAttribute(getConfigName("ruleGuardianPeople"), null, link, null));
            }
        }
    }


    @Override
    public RuleRelatedFileBean findReleateFiles(Map<String, Object> map) {
        RuleRelatedFileBean ruleRelatedFileBean = new RuleRelatedFileBean();
        List<RelatedFlowMap> relatedFlowMaps = ruleMapper.findRelatedFlowMap(map);
        List<RelatedFlow> relatedProcesses = ruleMapper.findRelatedFlow(map);
        List<RelatedStandard> relatedStandards = ruleMapper.findRelatedStandard(map);
        if (relatedStandards != null) {
            for (RelatedStandard standard : relatedStandards) {
                standard.setFrom(TreeNodeUtils.NodeType.ruleFile);
            }
        }
        List<RelatedRisk> relatedRisks = ruleMapper.findRelatedRisk(map);
        ruleRelatedFileBean.setRelatedFlowMaps(relatedFlowMaps);
        ruleRelatedFileBean.setRelatedProcesses(relatedProcesses);
        ruleRelatedFileBean.setRelatedStandards(relatedStandards);
        ruleRelatedFileBean.setRelatedRisks(relatedRisks);
        return ruleRelatedFileBean;
    }

    @Override
    public List<SearchRuleResultBean> searchRule(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "pub_time desc");
        return ruleMapper.searchRule(map);
    }

    @Override
    public int searchRuleTotal(Map<String, Object> map) {
        return ruleMapper.searchRuleTotal(map);
    }

    @Override
    public Inventory getInventory(Map<String, Object> map) {
        Boolean isPaging = (Boolean) map.get("isPaging");
        List<RuleInfoBean> ruleAndDirs = new ArrayList<>();
        if (isPaging) {
            // 查询分页的流程
            Paging paging = (Paging) map.get("paging");
            // 通过view_sort分页查询流程
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "view_sort");
            List<String> paths = inventoryMapper.fetchPaths(map);
            if (paths != null && paths.size() > 0) {
                // 将查询到的流程和他的所有父节点都集中处理为(1,2,3)类型
                String ids = Utils.convertPathCollectToSqlIn(paths);
                map.put("ids", ids);
                // 从数据库获得清单项的详细信息并排序
                ruleAndDirs = ruleMapper.fetchInventory(map);
            }
        } else {
            List<RuleInfoBean> parents = ruleMapper.fetchInventoryParent(map);
            ruleAndDirs.addAll(parents);
            // 获得制度数据,经过view_sort排序的数据，包含分页的数据以及他到level0的所有父节点
            ruleAndDirs.addAll(ruleMapper.fetchInventory(map));
        }
        //根据制度的部门ID  查询部门的父类部门
        for (RuleInfoBean ruleBean : ruleAndDirs) {
            if (ruleBean.getDutyOrgId() != null) {
                ruleBean.setDutyOrgList(orgMapper.findOrgPaterStrByOrgId(ruleBean.getDutyOrgId()));
            }
        }
        InventoryBaseInfo baseInfo = inventoryMapper.getInventoryBaseInfo(map);
        // 初始化清单
        Inventory inventory = new Inventory(baseInfo);

        Map<Long, FileDirCommonBean> dirs = new HashMap<>();
        List<RuleInfoBean> rules = new ArrayList<>();
        for (RuleInfoBean bean : ruleAndDirs) {
            if (bean.getIsDir() == 0) {
                dirs.put(bean.getRuleId(), new FileDirCommonBean(bean.getRuleId(), bean.getRuleName(), bean.getRuleNum(), bean.getLevel()));
            } else {
                rules.add(bean);
            }
        }
        IfytekCommon.ifytekCommonGetRuleUserName(rules);
        // 获取清单数据实体
        InventoryTable inventoryTable = getInventoryTable(isPaging, dirs, rules,
                inventory.getStartLevel(), inventory.getMaxLevel(), map);
        inventory.setData(inventoryTable);
        inventory.setStyleMap(InventoryStyleManager.getRuleInventoryStyles(inventoryTable.getTitileContents()));

        return inventory;
    }


    private InventoryTable getInventoryTable(boolean isPaging, Map<Long, FileDirCommonBean> dirs,
                                             List<RuleInfoBean> rules, int minLv, int maxLv, Map<String, Object> paramMap) {
        // 制度相关流程架构
        List<FileInventoryRelateBean> listRelateFlowMaps = new ArrayList<>();
        // 制度相关流程
        List<FileInventoryRelateBean> listRelateFlows = new ArrayList<>();
        // 制度相关标准
        List<FileInventoryRelateBean> listRelateStandards = new ArrayList<>();
        // 制度相关风险
        List<FileInventoryRelateBean> listRelateRisks = new ArrayList<>();
        // 制度相关制度
        List<FileInventoryRelateBean> listRelateRules = new ArrayList<>();
        // 制度相关标准化文件
        List<FileInventoryRelateBean> listRelateStandardized = new ArrayList<>();

        InventoryTable data = new InventoryTable();
        // 添加标题行的数据
        if (ConfigUtils.inventoryShowDirEnd()) {
            data.addContentRow(ExcelUtils.getMengNiuInventoryTitleNoNum(minLv, maxLv, InventoryStyleManager.getRuleMengNiuTitles()));
        } else {
            data.addContentRow(ExcelUtils.getInventoryTitleNoNum(minLv, maxLv, InventoryStyleManager.getRuleTitles()));
        }

        if (rules.size() > 0) {
            // 获得制度相关流程架构
            listRelateFlowMaps = inventoryMapper.findRuleChildFlowMaps(paramMap);
            // 获得制度相关流程
            listRelateFlows = inventoryMapper.findRuleChildFlows(paramMap);
            // 获得制度相关制度
            listRelateRules = inventoryMapper.findRuleChildRules(paramMap);
            // 制度相关标准
            listRelateStandards = inventoryMapper.findRuleRelatedStandard(paramMap);
            // 制度相关风险
            listRelateRisks = inventoryMapper.findRuleRelatedRisk(paramMap);
            //制度相关标准化文件
            listRelateStandardized = inventoryMapper.findRuleRelatedStandardized(paramMap);
        }

        for (RuleInfoBean rule : rules) {
            List<InventoryCell> contentRow = new ArrayList<InventoryCell>();

            if (ConfigUtils.inventoryShowDirEnd()) {// 目录显示在后面，内容列显示在前
                // 添加流程的相关数据
                contentRow.addAll(getContentCells(rule, listRelateFlowMaps, listRelateFlows, listRelateStandards, listRelateRisks, listRelateStandardized, listRelateRules));
                // 添加自动生成的级别数据
                contentRow.addAll(ExcelUtils.getLevelsNoNum(dirs, minLv, maxLv, rule.getPath()));
            } else {
                // 添加自动生成的级别数据
                contentRow.addAll(ExcelUtils.getLevelsNoNum(dirs, minLv, maxLv, rule.getPath()));
                // 添加流程的相关数据
                contentRow.addAll(getContentCells(rule, listRelateFlowMaps, listRelateFlows, listRelateStandards, listRelateRisks, listRelateStandardized, listRelateRules));
            }
            data.addContentRow(contentRow);
        }
        return data;
    }


    private List<InventoryCell> getContentCells(RuleInfoBean rule,
                                                List<FileInventoryRelateBean> listRelateFlowMaps, List<FileInventoryRelateBean> listRelateFlows,
                                                List<FileInventoryRelateBean> listRelateStandards, List<FileInventoryRelateBean> listRelateRisks,
                                                List<FileInventoryRelateBean> listRelateStandardized, List<FileInventoryRelateBean> listRelateRules) {

        List<InventoryCell> contentCells = new ArrayList<InventoryCell>();
        String ruleName = rule.getRuleName();
/*        if (rule.getRuleName() != null && rule.getRuleName().lastIndexOf(".") > 0 && rule.getIsDir() == 2) {
             ruleName = rule.getRuleName().substring(0, rule.getRuleName().lastIndexOf("."));
        }*/
        InventoryCell cell = new InventoryCell();
        cell.setText(ruleName);
        if (rule.getTypeByData() == 2) {// 发布的有链接，未发布的没有
            LinkResource link = new LinkResource(rule.getRuleId(), ruleName, ResourceType.RULE, TreeNodeUtils.NodeType.inventory);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
        }
        cell.setStatus(Utils.getNodeStatusByType(rule.getTypeByData()));
        contentCells.add(cell);

        cell = new InventoryCell();
        cell.setText(rule.getRuleNum());
        contentCells.add(cell);

        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.ruleScurityLevel)) {
            cell = new InventoryCell();
            cell.setText(ConfigUtils.getConfidentialityLevel(rule.getConfidentialityLevel()));
            contentCells.add(cell);
        }
        if (ConfigUtils.inventoryShowDirEnd()) {
            cell = new InventoryCell();
            int countType = commonService.findMengNiuPubType("JECN_RULE", rule.getRuleId(), "ID");
            String pubState = JecnProperties.getValue("newRelease");//新建发布
            if (rule.getTypeByData() == 0) {
                pubState = JecnProperties.getValue("unpublished");//未发布
            } else if (countType > 1) {
                pubState = JecnProperties.getValue("newRevision");//新建换版
            }
            cell.setText(pubState);
            contentCells.add(cell);
        }
        // 相关文件
        List<LinkResource> relatedLinks = new ArrayList<>();
        // 相关流程架构
        relatedLinks.addAll(Common.getFileInventoryCommonBeans(rule.getRuleId(), listRelateFlowMaps));
        // 相关流程
        relatedLinks.addAll(Common.getFileInventoryCommonBeans(rule.getRuleId(), listRelateFlows));
        //相关制度
        relatedLinks.addAll(Common.getFileInventoryCommonBeans(rule.getRuleId(), listRelateRules));
        // 相关标准
        relatedLinks.addAll(Common.getFileInventoryCommonBeans(rule.getRuleId(), listRelateStandards));
        // 相关风险
        relatedLinks.addAll(Common.getFileInventoryCommonBeans(rule.getRuleId(), listRelateRisks));
        // 标准化文件
        relatedLinks.addAll(Common.getFileInventoryCommonBeans(rule.getRuleId(), listRelateStandardized));

        cell = new InventoryCell();
        cell.setLinks(relatedLinks);
        contentCells.add(cell);

        // 责任部门
        String orgName = Common.getOrgFullPathStr(rule.getDutyOrg(), rule.getDutyOrgList());
        cell = new InventoryCell();
        cell.setText(orgName);
        contentCells.add(cell);


        if (!ConfigUtils.inventoryShowDirEnd()) {
            // 责任人
            LinkResource link = new LinkResource(rule.getResponsibleId(), rule.getResponsibleName(), rule.getResponsibleType() == 0 ? ResourceType.PERSON : ResourceType.POSITION, TreeNodeUtils.NodeType.inventory);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell = new InventoryCell();
            cell.setLinks(links);
            contentCells.add(cell);
            // 监护人
            cell = new InventoryCell();
            cell.setText(rule.getDraftPeople());
            contentCells.add(cell);
        }

        // 版本号
        cell = new InventoryCell();
        cell.setText(rule.getVersionId());
        contentCells.add(cell);

        // 发布日期
        cell = new InventoryCell();
        cell.setText(rule.getPubDate() != null ? Common.getStringbyDateHM(rule.getPubDate()) : "");
        contentCells.add(cell);

        // 有效期
        cell = new InventoryCell();
        String periodOfValidity = "";
        if (rule.getTypeByData() != 0) {
            if (rule.getExpiry() == null || rule.getExpiry() == 0) {
                periodOfValidity = JecnProperties.getValue("permanent");
            } else if (rule.getExpiry() != null && rule.getExpiry() > 0) {
                periodOfValidity = rule.getExpiry() + "";
            }
        }
        cell.setText(periodOfValidity);
        contentCells.add(cell);

        return contentCells;
    }

    /**
     * 制度操作说明
     *
     * @param map
     * @return
     * @throws Exception
     */
    @Override
    public OperationFile findRuleOperationFile(Map<String, Object> map) {
        OperationFile operationFile = new OperationFile();
        if (map.get("id") == null) {
            return operationFile;
        }
        boolean isPub = StringUtils.isBlank(map.get("isPub").toString()) ? true : false;
        long historyId = (long) map.get("historyId");
        Rule rule = null;
        if (historyId == 0) {//判断historyId为空执行
            rule = findRule(map);
        } else {
            rule = findRuleHistory(map);//判断historyId不为空 查询 制度文控表表数据
        }
        operationFile.setId(rule.getId());
        operationFile.setType("rule");
        operationFile.setName(rule.getRuleName());
        try {
            operationFile.setFileBeans(RuleOperationEnum.INSTANCE.getRuleOperationContents(map, ruleMapper, standardMapper));
        } catch (Exception e) {
            throw new EprosBussinessException("查询制度操作说明异常，制度id：" + rule.getId(), e);
        }
        // operationFile.setUrl(HttpUtil.getDownLoadHistoryFileHttp(rule.getFileId(), true, "", true));
        return operationFile;
    }

    @Override
    public RuleT getRuleInfo(Long ruleId, boolean isPub) {
        RuleT rule;
        if (isPub) {
            rule = ruleMapper.getRule(ruleId);
        } else {
            rule = ruleMapper.getRuleT(ruleId);
        }
        return rule;
    }

}
