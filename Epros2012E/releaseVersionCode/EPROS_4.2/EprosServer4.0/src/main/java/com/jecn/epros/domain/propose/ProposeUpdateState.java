package com.jecn.epros.domain.propose;

import io.swagger.annotations.ApiModelProperty;

/**
 * 采纳拒绝
 *
 * @author user
 */
public class ProposeUpdateState {
    @ApiModelProperty(value = "合理化建议id")
    private String proposeId;
    @ApiModelProperty(value = "")
    private Integer state;

    public String getProposeId() {
        return proposeId;
    }

    public void setProposeId(String proposeId) {
        this.proposeId = proposeId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
