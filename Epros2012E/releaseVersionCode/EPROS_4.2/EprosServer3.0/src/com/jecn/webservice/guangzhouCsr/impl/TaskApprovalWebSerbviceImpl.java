package com.jecn.webservice.guangzhouCsr.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.guangzhouCsr.JecnGuangZhouJiDianAfterItem;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.task.search.IJecnTaskSearchService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.guangzhouCsr.CrsTaskBean;
import com.jecn.webservice.guangzhouCsr.ITaskApprovalWebService;

/**
 * 
 * 广州电力动车任务获取webService实现
 * 
 * @author chehuanbo
 * @date 2014-11-28
 * 
 * **/
@WebService
public class TaskApprovalWebSerbviceImpl implements ITaskApprovalWebService {

	private static final Logger log = Logger.getLogger(TaskApprovalWebSerbviceImpl.class);

	@WebMethod
	public List<CrsTaskBean> getALLTaskApproval(String loginName, String loginPassWord) {
		// 判定参数是否非法
		if (isParamsIllegal(loginName, loginPassWord)) {
			return null;
		}
		IPersonService personService = (IPersonService) ApplicationContextUtil.getContext()
				.getBean("PersonServiceImpl");
		IJecnTaskSearchService iJecnTaskSearchService = (IJecnTaskSearchService) ApplicationContextUtil.getContext()
				.getBean("searchServiceImpl");
		try {
			// 获取用户信息
			WebLoginBean loginBean = personService.getWebLoginBean(loginName, loginPassWord,
					JecnGuangZhouJiDianAfterItem.VERIFY);
			if (loginBean.getLoginState() == 0) {
				log.info("获取用户任务数据时，用户登录验证失败！原因：用户名或密码错误。");
				return null;
			}
			if (loginBean.getJecnUser() == null) {
				log.error("用户" + loginName + "不存在！");
				return null;
			}
			log.info("用户登录名 user = " + loginBean.getJecnUser().getLoginName());
			// 根据查询条件获取登录人相关任务
			List<MyTaskBean> myTaskBeans = iJecnTaskSearchService.getMyTaskBySearch(null, loginBean.getJecnUser()
					.getPeopleId(), 0, -1);
			if (myTaskBeans != null && myTaskBeans.size() > 0) {
				// 当前用户任务集合
				List<CrsTaskBean> crsTaskBeans = new ArrayList<CrsTaskBean>();
				for (MyTaskBean myTaskBean : myTaskBeans) {
					if (isInvisibleTask(myTaskBean)) {
						continue;
					}
					CrsTaskBean crsTaskBean = new CrsTaskBean();
					// 任务ID
					crsTaskBean.setTaskId(myTaskBean.getTaskId());
					// 任务名称
					crsTaskBean.setTaskName(myTaskBean.getTaskName());
					// 任务类型
					crsTaskBean.setTaskType(myTaskBean.getTaskType());
					// 创建时间
					crsTaskBean.setCreateTime(myTaskBean.getUpdateTime());
					crsTaskBeans.add(crsTaskBean);
				}
				return crsTaskBeans;
			}
		} catch (Exception e) {
			log.error("调用webService接口查询任务数据出现出现异常", e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 任务是否不可以审批，条件判定依照myTask.js选写
	 * 
	 * @param myTaskBean
	 */
	private boolean isInvisibleTask(MyTaskBean myTaskBean) {
		int taskElseState = myTaskBean.getTaskElseState();
		int taskStage = myTaskBean.getTaskStage();
		if (taskElseState == 3 || (taskElseState == 5 && (taskStage == 10 || taskStage == 0))
				|| (taskStage != 5 && myTaskBean.getReJect() == 1)) {
			return true;
		}
		return false;
	}

	/**
	 * 判定参数是否非法
	 * 
	 * @param loginName
	 * @param loginPassWord
	 * @return true 非法的 false 合法的
	 */
	private boolean isParamsIllegal(String loginName, String loginPassWord) {
		// loginName 登录名称
		if (StringUtils.isBlank(loginName)) {
			return true;
		}
		// 开启校验密码的情况下判断密码是否为空
		// 广机那边负责开发的员工告知无法传递用户的密码
		if (JecnGuangZhouJiDianAfterItem.VERIFY && StringUtils.isBlank(loginPassWord)) {
			return true;
		}
		return false;
	}
}