package epros.draw.gui.top.dialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 执行对话框标题区拖动的面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDialogDropTitlePanel extends JecnPanel implements
		MouseMotionListener, MouseListener, ActionListener {
	/** 对话框 */
	protected JecnDialog dialog = null;
	/** 用于拖动的虚图 */
	protected JDialog virtualDialog = null;

	/** 按下时坐标 */
	protected Point pressedPoint = null;
	/** 释放时基于屏幕坐标 */
	protected Point destPoint = null;

	/** 按下标识： true：按下 false：未按下 */
	protected boolean isPressed = false;
	/** 拖动标识 true：拖动 false：未拖动 */
	protected boolean isDragged = false;
	/** 对话框是否最大 */
	protected boolean isMax = false;

	/** 和全屏互换的Rectangle对象 */
	protected Rectangle resetRectangle = null;

	/** 是否可由用户调整大小（最大化按钮是否显示，拖动对话框边框是否让拖动）：true可以调整 false不可以调整 */
	protected boolean resizable = true;

	JecnDialogDropTitlePanel(JecnDialog dialog) {
		if (dialog == null) {
			JecnLogConstants.LOG_DIALOG_DROP_TITLE_PANEL
					.error("JecnDialogDropTitlePanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.dialog = dialog;

		initComponent();
	}

	private void initComponent() {
		// 透明
		this.setOpaque(false);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		virtualDialog = new JDialog(dialog, false);
		virtualDialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		// 禁用此对话框的装饰
		virtualDialog.setUndecorated(true);
		// 设置此对话框是否可由用户调整大小
		virtualDialog.setResizable(true);
		// 布局
		virtualDialog.getContentPane().setLayout(new GridBagLayout());
		virtualDialog.getContentPane().setBackground(
				JecnUIUtil.getDialogDragColor());
		//
		JPanel label = new JPanel();
		label.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0);
		virtualDialog.getContentPane().add(label, c);
	}

	/**
	 * 
	 * 按下
	 * 
	 */
	public void mousePressed(MouseEvent event) {
		// 按下
		isPressed = true;

		// 获取按下坐标
		pressedPoint = event.getPoint();
		virtualDialog.setBounds(dialog.getBounds());
	}

	/**
	 * 
	 * 释放
	 * 
	 */
	public void mouseReleased(MouseEvent event) {
		if (isDragged && isPressed && destPoint != null) {// 有按下且拖动才执行
			// 设置对话框位置点
			dialog.setLocation(destPoint);

			// 设置按下标识
			isPressed = false;
			// 设置拖动标识
			isDragged = false;
		}
		setVirtualDialogFlase();
	}

	void setVirtualDialogFlase() {
		if (virtualDialog != null && virtualDialog.isVisible()) {
			virtualDialog.setSize(0, 0);
			virtualDialog.setVisible(false);
		}
	}

	/**
	 * 
	 * 拖动
	 * 
	 * 
	 */
	public void mouseDragged(MouseEvent event) {

		// 界面最大化时，不用拖动
		if (!isPressed || dialog == null || isMax) {
			isDragged = false;
			return;
		}

		if (!virtualDialog.isVisible()) {
			virtualDialog.setBounds(dialog.getBounds());
			virtualDialog.setVisible(true);
		}
		destPoint = new Point(dialog.getLocation().x + event.getX()
				- pressedPoint.x, dialog.getLocation().y + event.getY()
				- pressedPoint.y);
		virtualDialog.setLocation(destPoint);

		isDragged = true;
	}

	public void mouseClicked(MouseEvent paramMouseEvent) {
		if (paramMouseEvent.getClickCount() == 2
				&& SwingUtilities.isLeftMouseButton(paramMouseEvent)) {// 左键双击
			if (paramMouseEvent.getSource() instanceof JecnDialogDropTitlePanel) {
				// 设置按下标识
				isPressed = false;
				// 设置拖动标识
				isDragged = false;
				// 虚拟对话框隐藏
				setVirtualDialogFlase();

				if (!resizable) {
					return;
				}
				resetDialog();
			}
		}
	}

	public void mouseEntered(MouseEvent paramMouseEvent) {

	}

	public void mouseExited(MouseEvent paramMouseEvent) {

	}

	public void mouseMoved(MouseEvent paramMouseEvent) {

	}

	/**
	 * 
	 * 
	 * 对话框最大化、关闭按钮事件
	 * 
	 */
	public void actionPerformed(ActionEvent paramActionEvent) {
		if (paramActionEvent.getSource() instanceof AbstractButton) {
			// 事件源
			AbstractButton btn = (AbstractButton) paramActionEvent.getSource();
			if ("maxBtn".equals(btn.getName())) {// 最大化按钮
				resetDialog();
			} else if ("exitBtn".equals(btn.getName())) {// 关闭
				// 执行右上角关闭按钮之前动作
				boolean isClose = dialogExitBefore(dialog
						.getDialogListenerList());
				if (isClose) {// 关闭
					dialog.setVisible(false);
				}
			}
		}
	}

	/**
	 * 
	 * 关闭前判断监听
	 * 
	 * @param dialogListenerList
	 *            Map<JecnDialogListener, Object>监听记录对象
	 * 
	 * @return boolean true：关闭；false：不关闭
	 */
	private boolean dialogExitBefore(
			Map<JecnDialogListener, Object> dialogListenerList) {
		// 关闭
		if (dialogListenerList == null || dialogListenerList.size() == 0) { // 没有监听
			return true;
		}
		// 有监听
		for (JecnDialogListener dialogListener : dialogListenerList.keySet()) {
			// value值
			Object obj = dialogListenerList.get(dialogListener);
			// 执行关闭前方法是否关闭: true：需要执行关闭 false：不要执行关闭
			boolean isClose = dialogListener
					.dialogCloseBefore(new JecnDialogEvent(obj));
			if (isClose) {// 关闭
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * 
	 * 设置对话框显示最大化
	 * 
	 */
	private void setMaxDialog() {
		// 存储对话框全屏前的位置点和大小
		setResetRectangle(dialog.getBounds());

		// 屏幕大小
		Rectangle dialogRect = dialog.getGraphicsConfiguration().getBounds();
		Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(
				dialog.getGraphicsConfiguration());
		dialogRect.x += insets.left;
		dialogRect.y += insets.top;
		dialogRect.width -= insets.left + insets.right;
		dialogRect.height -= insets.top + insets.bottom;
		// 设置位置点和大小
		dialog.setBounds(dialogRect);
		
		// 对话框是全屏显示
		this.isMax = true;
	}

	/**
	 * 
	 * 全屏显示还原成全屏之前的非全屏状态
	 * 
	 */
	private void resetDialog() {
		if (!isMax) {// 不是最大化
			// 最大化处理
			setMaxDialog();

			// 设置图片
			dialog.getToolTitlePanel().getResizeBtn().setIcon(
					JecnDialogIcon.resizeMaxIcon);
			return;
		}
		// 对话框非全屏显示
		this.isMax = false;

		// 设置位置点和大小
		Rectangle rect = getResetRectangle();
		dialog.setBounds(rect);

		// 设置图片
		dialog.getToolTitlePanel().getResizeBtn().setIcon(
				JecnDialogIcon.resizeIcon);
	}

	public Rectangle getResetRectangle() {
		if (resetRectangle == null) {
			resetRectangle = this.dialog.getBounds();
		}
		return resetRectangle;
	}

	public void setResetRectangle(Rectangle resetRectangle) {
		if (resetRectangle == null) {
			resetRectangle = this.dialog.getBounds();
		}
		this.resetRectangle = resetRectangle;
	}

	boolean isMax() {
		return isMax;
	}
}
