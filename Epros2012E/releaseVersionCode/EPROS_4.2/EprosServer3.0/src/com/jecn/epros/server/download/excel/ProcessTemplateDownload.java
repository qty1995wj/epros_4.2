package com.jecn.epros.server.download.excel;

import java.io.File;
import java.util.List;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.webBean.process.ActiveModeBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;

/**
 * 流程操作面板下载
 * 
 * @author ZHANGXH
 */
public class ProcessTemplateDownload {
	/** 流程操作面板下载日志 */
	private Log log = LogFactory.getLog(ProcessTemplateDownload.class);
	/** 模板文件路径 */
	private String excelModeFilePath;
	/** 导出文件路径 */
	private String excelOutPutFilePath;

	public boolean processOptionExcel(
			List<ProcessActiveWebBean> processActiveTemplateList) {
		log.info("获取模板文件路径：" + excelModeFilePath);

		Workbook wbookMould = null;
		WritableWorkbook wbookData = null;
		try {
			// 判断模板文件路径是否存在
			if (JecnCommon.isNullOrEmtryTrim(excelModeFilePath)) {
				return false;

			}
			// 人员导出模板文件 不存在返回
			File fileMould = new File(excelModeFilePath);
			if (!fileMould.exists()) {
				return false;
			}
			log.info("获取模板文件路径验证成功");

			// 创建导出的模板文件
			File fileData = new File(excelOutPutFilePath);

			// 生成模板数据
			wbookMould = Workbook.getWorkbook(fileMould);
			// 读取模板再向模板写入数据然后生成Excel
			wbookData = Workbook.createWorkbook(fileData, wbookMould);
			
			try{//提前释放资源
				wbookMould.close();
				wbookMould=null;
			}catch (Exception e) {
				log.error("", e);
			}

			// 获取sheet工作簿
			WritableSheet[] sheetArry = wbookData.getSheets();

			// 部门
			WritableSheet activeSheet = sheetArry[0];

			outPutExcelBuss(activeSheet, processActiveTemplateList);
			// 写入本地
			wbookData.write();
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			log.info("错误信息存储结束，错误信息存放路径：" + excelOutPutFilePath);
		} catch (Exception ex) {
			log.error("人员数据导出失败：" + this.getClass().getName() + "类editExcel方法 "
					+ ex.getMessage());

			return false;

		} finally {

			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("人员数据导出失败：" + this.getClass().getName()
							+ "类editExcel方法 " + e.getMessage());
				}
			}
			if (wbookMould != null) {
				try {
					wbookMould.close();
				} catch (Exception e) {
					log.error("",e);
				}
				
			}
		}

		return true;
	}

	/***************************************************************************
	 * 编辑岗位sheet，把DB中岗位数据写入给定sheet中
	 * 
	 * @param activeSheet
	 * 
	 * @param posBeanList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void outPutExcelBuss(WritableSheet activeSheet,
			List<ProcessActiveWebBean> processActiveTemplateList)
			throws RowsExceededException, WriteException {

		// 行数 3 是第几行开始
		int rowNum = 0;
		WritableCellFormat wformat = new WritableCellFormat();// 表样式
		wformat.setBorder(Border.ALL, BorderLineStyle.THIN);
		//设置自动换行
		wformat.setWrap(true);
		// 添加岗位数据
		for (int i = 0; i < processActiveTemplateList.size(); i++) {
			// 获取一条岗位数据
			ProcessActiveWebBean activeWebBean = processActiveTemplateList
					.get(i);
			if (i == 0) {
				rowNum = 2;
			} else {
				rowNum++;
			}
			// 输出（表单）
			int modeSize = getOutFileName(activeSheet, rowNum, activeWebBean
					.getListMode(), wformat);
			if (modeSize != rowNum) {
				activeSheet.mergeCells(0, rowNum, 0, modeSize - 1);
				activeSheet.mergeCells(1, rowNum, 1, modeSize - 1);
				activeSheet.mergeCells(2, rowNum, 2, modeSize - 1);
				activeSheet.mergeCells(5, rowNum, 5, modeSize - 1);
			}
			// 活动名称
			activeSheet.addCell(new Label(0, rowNum, activeWebBean
					.getActiveName(), wformat));
			// 活动编号
			activeSheet.addCell(new Label(1, rowNum, activeWebBean
					.getActiveId(), wformat));
			// 输入
			activeSheet.addCell(new Label(2, rowNum, getFileNames(activeWebBean
					.getListInput()), wformat));
			// 操作规范
			activeSheet.addCell(new Label(5, rowNum, getFileNames(activeWebBean
					.getListOperation()), wformat));
			if (modeSize != rowNum && modeSize != 0) {
				rowNum = modeSize - 1;
			}
		}
	}

	/**
	 * 输出（表单和样例）
	 * 
	 * @param activeSheet
	 * @param rowNum
	 * @param listMode
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private int getOutFileName(WritableSheet activeSheet, int rowNum,
			List<ActiveModeBean> listMode, WritableCellFormat wformat)
			throws RowsExceededException, WriteException {
		
		/********** 没有输出***********/
		if (listMode == null || listMode.size() == 0) {// 没有输出
			// 输出（表单）
			activeSheet.addCell(new Label(3, rowNum, "", wformat));
			// 输出（样例）
			activeSheet.addCell(new Label(4, rowNum, "", wformat));
			return rowNum;
		}
		
		/********** 有输出***********/
		// 输出表单名称
		for (ActiveModeBean activeModeBean : listMode) {
			int roleRowSpan = activeModeBean.getListTemplet().size();
			for (int i = 0; i < roleRowSpan; i++) {
				FileWebBean fileWebBean = activeModeBean.getListTemplet()
						.get(i);
				// 输出（样例）
				activeSheet.addCell(new Label(4, rowNum + i, fileWebBean
						.getFileName(), wformat));
			}
			if (roleRowSpan != 0) {
				// 合并行 第rowNum行 开始 rowNum + roleRowSpan - 1行结束
				activeSheet.mergeCells(3, rowNum, 3, rowNum + roleRowSpan - 1);
			} else {
				// 输出（样例）
				activeSheet.addCell(new Label(4, rowNum, "", wformat));
			}
			// 输出（表单）
			activeSheet.addCell(new Label(3, rowNum, activeModeBean
					.getModeFile().getFileName(), wformat));
			if (roleRowSpan == 0) {
				rowNum++;
			}
			rowNum = roleRowSpan + rowNum;
		}
		return rowNum;
	}

	/**
	 * 获取输入或操作规范文件名称
	 * 
	 * @param listOperation
	 * @return
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	private String getFileNames(List<FileWebBean> listOperation)
			throws RowsExceededException, WriteException {
		String operationNames = "";
		if (listOperation == null) {
			return operationNames;
		}
		for (FileWebBean fileWebBean : listOperation) {
			// 输出（样例）
			operationNames = operationNames + "\n" + fileWebBean.getFileName();
		}
		return operationNames;
	}

	public static ProcessTemplateDownload getProcessTemplateDownload() {
		return new ProcessTemplateDownload();
	}

	public String getExcelModeFilePath() {
		return excelModeFilePath;
	}

	public void setExcelModeFilePath(String excelModeFilePath) {
		this.excelModeFilePath = excelModeFilePath;
	}

	public String getExcelOutPutFilePath() {
		return excelOutPutFilePath;
	}

	public void setExcelOutPutFilePath(String excelOutPutFilePath) {
		this.excelOutPutFilePath = excelOutPutFilePath;
	}
}
