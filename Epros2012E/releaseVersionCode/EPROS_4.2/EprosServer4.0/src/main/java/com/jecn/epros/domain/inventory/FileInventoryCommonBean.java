package com.jecn.epros.domain.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;

public class FileInventoryCommonBean {
    private Long fileId;
    private String fileName;
    // 0是流程架构、1是流程、2是制度、3是标准、4风险、5是活动编号、6是流程KPI
    private int fileType;

    //目标值
    private String targetValue;
    //现状值
    private String statusValue;

    public LinkResource getLink() {
        return new LinkResource(fileId, fileName, getResourceType(fileType), TreeNodeUtils.NodeType.inventory);
    }

    private ResourceType getResourceType(int type) {
        ResourceType resourceType = null;
        switch (type) {
            case 0:
                resourceType = ResourceType.PROCESS_MAP;
                break;
            case 1:
                resourceType = ResourceType.PROCESS;
                break;
            case 2:
                resourceType = ResourceType.RULE;
                break;
            case 3:
                resourceType = ResourceType.STANDARD;
                break;
            case 4:
                resourceType = ResourceType.RISK;
                break;
            case 5:
                resourceType = ResourceType.ACTIVITY;
                break;
            case 6:
                resourceType = ResourceType.KPI;
                break;
            case 7:
                resourceType = ResourceType.FILE;
                break;
        }

        return resourceType;
    }

    @JsonIgnore
    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    @JsonIgnore
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @JsonIgnore
    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    @JsonIgnore
    public String getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

    @JsonIgnore
    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }
}
