package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 基本信息面板
 * 
 * @author ZXH
 * 
 */
public class JecnFlowMapOtherConfigPanel extends JecnAbstractPropertyBasePanel implements ActionListener {

	/** 其他项面板 */
	private JecnPanel otherPanel = null;

	/** 只允许管理员编辑流程架构 */
	private JecnConfigCheckBox checkBox = new JecnConfigCheckBox(JecnProperties.getValue("allowAdminEdit"));

	public JecnFlowMapOtherConfigPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		checkBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		checkBox.setSelected(false);
		checkBox.addActionListener(this);

		otherPanel = new JecnPanel();
		otherPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		otherPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("editFlowMap")));
	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {

		this.add(otherPanel, BorderLayout.CENTER);
		// otherPanel.setLayout(new GridBagLayout());

		Insets insets = new Insets(5, 5, 5, 5);
		GridBagConstraints c = null;
		int index = 0;
		c = new GridBagConstraints(0, index, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		otherPanel.add(checkBox, c);
		index++;

		// 空闲区域
		c = new GridBagConstraints(0, index, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, insets,
				0, 0);
		otherPanel.add(new JLabel(), c);
		index++;
		// **************流程责任人 end **************//

	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String value = itemBean.getValue();
			if (ConfigItemPartMapMark.isAdminEditProcessMap.toString().equals(mark)) {// 流程有效期
				checkBox.setItemBean(itemBean);
				if ("0".equals(value)) {
					checkBox.setSelected(false);
				} else {
					checkBox.setSelected(true);
				}
			}
		}
	}

	/**
	 * 
	 * 校验数据
	 * 
	 * @return boolean 校验成功：true ；校验失败：false
	 * 
	 */
	@Override
	public boolean check() {
		return true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (checkBox.isSelected()) {
			checkBox.getItemBean().setValue("1");
		} else {
			checkBox.getItemBean().setValue("0");
		}
	}
}
