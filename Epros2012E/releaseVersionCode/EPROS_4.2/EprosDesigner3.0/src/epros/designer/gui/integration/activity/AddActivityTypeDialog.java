package epros.designer.gui.integration.activity;

import java.awt.Insets;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 类别名称 添加
 * 
 * @author 2012-07-05
 * 
 */
public class AddActivityTypeDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddActivityTypeDialog.class);
	/** true：添加成功 */
	protected boolean isOperation = false;
	private Long typeId;

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public AddActivityTypeDialog() {
		// 置于屏幕的中央
		this.setLocationRelativeTo(null);
	}

	protected void addEnNamePanel(Insets insets) {

	}

	protected void validateEnglishName() {

	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addBtn");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	/**
	 * 保存活动类别
	 * 
	 */
	@Override
	public void saveData() {
		JecnActiveTypeBean activeTypeBean = new JecnActiveTypeBean();
		activeTypeBean.setTypeName(this.getName());
		try {
			// 添加类别数据库
			typeId = ConnectionPool.getActivityAction().addActivityType(activeTypeBean);
			isOperation = true;
			this.dispose();
		} catch (Exception e) {
			isOperation = false;
			log.error("AddActivityTypeDialog saveData is error", e);
		}

	}

	/**
	 * 验证是否重名
	 */
	@Override
	public boolean validateNodeRepeat(String name) {
		try {
			boolean isSame = ConnectionPool.getActivityAction().isSameActivityType(name);
			if (isSame) {// True:存在相同名称
				return true;
			}
		} catch (Exception e) {
			log.error("AddActivityTypeDialog validateNodeRepeat is error", e);
		}
		return false;
	}

}
