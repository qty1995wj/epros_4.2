package com.jecn.epros.server.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import jxl.write.WritableImage;
import jxl.write.WritableSheet;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jecn.epros.server.action.designer.system.impl.TimerEmailSendAction;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.temp.TempSearchKpiBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnJournal;
import com.jecn.epros.server.bean.task.PrfRelatedTemplet;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.JecnSqlConstant;
import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.service.email.IEmailService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.integration.RiskDetailBean;
import com.jecn.epros.server.webBean.integration.RiskWebBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.standard.StandardWebBean;

public class JecnUtil {
	private final static Log log = LogFactory.getLog(JecnUtil.class);
	/** 资源文件库--国际化 */
	public static PropertyResourceBundle resourceBundle;

	/** 数据库动态加载 配置 */
	public static Map<String, String> custOmResourceMap;

	/**
	 * 语言文本缓存 中文
	 */
	private static Map<String, String> languageTextCh = new HashMap<String, String>();
	/**
	 * 语言文本缓存英文
	 */
	private static Map<String, String> languageTextEn = new HashMap<String, String>();

	public static final Map<String, String> markToDic = new HashMap<String, String>();

	static {
		resourceBundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("epros_zh_CN");
		custOmResourceMap = new HashMap<String, String>();
		settingLanguageType();
		initMarkToDic();
	}

	/**
	 * 加载活动明细文本
	 * 
	 * @param type
	 */
	private static void settingLanguageType() {

		languageTextEn.put("inputC", "inPut:");
		languageTextEn.put("outPutC", "outPut:");
		languageTextEn.put("operation", "SOP(Standard of Operation):");
		languageTextEn.put("activityDescriptionC", "Activity Description：");
		languageTextEn.put("keyExplanationC", "Key Description：");
		languageTextEn.put("refStand", "Linked standard:");
		languageTextEn.put("rescriptionC", "Description：");
		languageTextEn.put("roleResponsibilitiesC", "Role Responsibity：");
		languageTextEn.put("nameC", "Name：");
		languageTextEn.put("definitionC", "Definition：");
		languageTextEn.put("Template", "  [template]");
		languageTextEn.put("positionNameC", "  job name:");
		languageTextEn.put("sample", "  [Sample]");
		languageTextEn.put("positionGroupC", "  job group name:");

		languageTextCh.put("inputC", "输入：");
		languageTextCh.put("outPutC", "输出：");
		languageTextCh.put("operation", "操作规范：");
		languageTextCh.put("activityDescriptionC", "活动说明：");
		languageTextCh.put("refStand", "关联标准：");
		languageTextCh.put("keyExplanationC", "关键说明：");
		languageTextCh.put("rescriptionC", "描述：");
		languageTextCh.put("roleResponsibilitiesC", "角色职责：");
		languageTextCh.put("nameC", "名称：");
		languageTextCh.put("definitionC", "定义：");
		languageTextCh.put("Template", "  [模板]");
		languageTextCh.put("sample", "  [样例]");
		languageTextCh.put("positionGroupC", "  岗位组名称：");
		languageTextCh.put("positionNameC", "  岗位名称：");

	}

	/**
	 * 将key对应的name en_name改为value对应的
	 */
	private static void initMarkToDic() {
		// ruleScurityLevel, // 制度保密级别
		// fileScurityLevel, // 文件保密级别
		markToDic.put(ConfigItemPartMapMark.ruleScurityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL.toString());
		markToDic.put(ConfigItemPartMapMark.fileScurityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL.toString());
		markToDic.put(ConfigItemPartMapMark.ruleTaskSecurityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL
				.toString());
		markToDic.put(ConfigItemPartMapMark.securityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL.toString());

	}

	public static String getValue(String key, int type) {
		if (type == 1) {
			if (languageTextEn.containsKey(key)) {
				return languageTextEn.get(key);
			} else {
				return "null";
			}
		} else {
			if (languageTextCh.containsKey(key)) {
				return languageTextCh.get(key);
			} else {
				return "未定义";
			}
		}
	}

	public static String getBaseName() {
		return "epros_zh_CN";
	}

	public static String getValue(String key) {
		try {
			if (custOmResourceMap.containsKey(key)) {
				return custOmResourceMap.get(key);
			}
			return resourceBundle.getString(key.trim());
		} catch (Exception e) {
			log.error("获取文件值异常", e);
			return "";
		}
	}

	/**
	 * 文件任务模板保存
	 * 
	 * @param baseDao
	 * @param relateId
	 * @param relateType
	 *            0流程、1文件、2制度主键
	 */
	public static void createFileAddPrfRelatedTemplets(IBaseDao baseDao, Long relateId, String relateName,
			int relateType, String approveId, String borrowId, String desuetudeId, Long updatePeopleId)
			throws Exception {
		boolean isExist = false;
		if (StringUtils.isNotBlank(approveId)) {
			JecnUtil.savePrfRelatedTemplet(baseDao, relateId, relateType, approveId, 0);
			isExist = true;
		}

		if (StringUtils.isNotBlank(borrowId)) {
			JecnUtil.savePrfRelatedTemplet(baseDao, relateId, relateType, borrowId, 1);
			isExist = true;
		}

		if (StringUtils.isNotBlank(desuetudeId)) {
			JecnUtil.savePrfRelatedTemplet(baseDao, relateId, relateType, desuetudeId, 2);
			isExist = true;
		}
		if (isExist) {
			int type = 0;
			if (relateType == 0 || relateType == 4) {
				type = 1;
			} else if (relateType == 2) {
				type = 4;
			} else if (relateType == 1) {
				type = 2;
			}
			saveJecnJournal(relateId, relateName, type, 13, updatePeopleId, baseDao);
		}
	}

	/**
	 * 文件任务模板保存
	 * 
	 * @param baseDao
	 * @param relateId
	 * @param relateType
	 *            0流程、1文件、2制度主键
	 * @param taskTempletId
	 * @param taskTempletType
	 *            0审批 1借阅 2废止
	 */
	public static void savePrfRelatedTemplet(IBaseDao baseDao, Long relateId, int relateType, String taskTempletId,
			int taskTempletType) {
		PrfRelatedTemplet approveTemplet = new PrfRelatedTemplet();
		approveTemplet.setRelatedId(relateId);
		approveTemplet.setTempletId(taskTempletId);
		approveTemplet.setType(relateType);
		approveTemplet.setTempletType(taskTempletType);
		baseDao.getSession().save(approveTemplet);
	}

	/**
	 * 获取数据库动态配置 名称
	 * 
	 * @return
	 * 
	 */
	public static void setConfigItemValues(ServletContext servletContext) {
		// 风险
		servletContext.setAttribute("riskSys", JecnUtil.getValue("riskSys"));
		// 密级
		servletContext.setAttribute("publicShow", JecnUtil.getValue("public"));
		servletContext.setAttribute("secretShow", JecnUtil.getValue("secret"));
		// 版本信息
		servletContext.setAttribute("versionType", JecnUtil.isVersionType());
		// 制度、风险、标准、文件：浏览端双击目录默认显示清单
		servletContext.setAttribute("isShowDirList", JecnContants.isShowDirList);
		// 具有查阅权限的人能否查看文控信息显示 0是不显示，1 是显示
		servletContext.setAttribute("isDocControlPermission", JecnContants.docControlPermission == 1 ? true : false);

		// 隐藏标准
		servletContext.setAttribute("isHiddenStandard", JecnContants.getValue(ConfigItemPartMapMark.isHiddenStandard
				.toString()));
		// 隐藏制度
		servletContext.setAttribute("isHiddenSystem", JecnContants.getValue(ConfigItemPartMapMark.isHiddenSystem
				.toString()));
		// 隐藏风险
		servletContext.setAttribute("isHiddenRisk", JecnContants
				.getValue(ConfigItemPartMapMark.isHiddenRisk.toString()));
		// 操作说明编号
		servletContext.setAttribute("otherOperaType", JecnContants.configMaps.get(
				ConfigItemPartMapMark.otherOperaType.toString()).getValue());

		// 隐藏 巴德富隐藏项 (巴德富)
		servletContext.setAttribute("isHiddenBDF", JecnContants.otherOperaType == 16 ? true : false);

		// 中车显示提示信息 21
		servletContext.setAttribute("isShowTip", JecnContants.otherOperaType == 21 ? true : false);

		// 立邦不显示人员管理
		servletContext.setAttribute("isShowUserManager", JecnContants.otherLoginType == 28 ? false : true);
		// 立邦不显示角色管理
		servletContext.setAttribute("isShowRoleManager", JecnContants.otherLoginType == 28 ? false : true);

		// 写出动态名称
		for (Map.Entry<String, String> entry : JecnUtil.custOmResourceMap.entrySet()) {
			servletContext.setAttribute(entry.getKey(), entry.getValue());
		}

	}

	/**
	 * 
	 * 获取是否是完整版
	 * 
	 * @return 完整版true；非完整版false
	 */
	public static boolean isVersionType() {
		return (JecnContants.versionType >= 99) ? true : false;
	}

	/**
	 * @author yxw 2013-7-22
	 * @description:0：有浏览端标准版（默认）；99：有浏览端完整版；1：无浏览端标准版；100：无浏览端完整版
	 * @return
	 */
	public static boolean isPubShow() {
		if (JecnContants.versionType == 1 || JecnContants.versionType == 100) {
			return false;
		}
		return true;
	}

	/**
	 * 把一个文件转化为字节
	 * 
	 * @param file
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] getByte(File file) {
		byte[] bytes = null;
		if (file != null) {
			InputStream is = null;
			try {
				is = new FileInputStream(file);
				int length = (int) file.length();
				if (length > Integer.MAX_VALUE) // 当文件的长度超过了int的最大值
				{
					return null;
				}
				bytes = new byte[length];
				int offset = 0;
				int numRead = 0;
				while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
					offset += numRead;
				}
				// 如果得到的字节长度和file实际的长度不一致就可能出错了
				if (offset < bytes.length) {
					return null;
				}
			} catch (Exception e) {
				log.error("文件转换为字节流异常！", e);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						log.error("关闭输入流异常！", e);
					}
				}
			}
		}
		return bytes;
	}

	/**
	 * @author zhangchen May 31, 2012
	 * @description：去掉文件后缀
	 * @param name
	 * @return
	 */
	public static String getFileNameNoSuffix(String name) {
		return name.substring(0, name.lastIndexOf("."));
	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：组织对象转换成树对象
	 * @param obj
	 * @return
	 */
	public static JecnTreeBean getJecnTreeBeanByObjectOrg(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();

		// 组织节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 组织节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 组织节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 组织节点父类名称
		jecnTreeBean.setPname("");
		jecnTreeBean.setTreeNodeType(TreeNodeType.organization);
		if (obj.length > 4) {
			if (Integer.parseInt(obj[3].toString()) > 0 || Integer.parseInt(obj[4].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
		} else {

			if (Integer.parseInt(obj[3].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
		}
		if (obj.length > 6) {
			if (obj[6] != null && !"".equals(obj[6])) {
				jecnTreeBean.setNumberId(obj[6].toString());
			}
		}

		if (obj.length > 7) {
			jecnTreeBean.setT_Path(obj[7].toString());
		}

		return jecnTreeBean;
	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：组织对象转换成树对象
	 * @param obj
	 * @return
	 */
	public static JecnTreeBean getJecnTreeBeanByObjectOrgAccess(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();

		// 组织节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 组织节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 组织节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 组织节点父类名称
		jecnTreeBean.setPname("");
		jecnTreeBean.setTreeNodeType(TreeNodeType.organization);
		if (obj.length > 4) {
			if (Integer.parseInt(obj[3].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
		} else {

			if (Integer.parseInt(obj[3].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
		}
		if (obj[4] != null) {
			jecnTreeBean.setDownLoad(Integer.valueOf(obj[4].toString()) == 1 ? true : false);
		}
		if (obj.length > 6) {
			if (obj[6] != null && !"".equals(obj[6])) {
				jecnTreeBean.setNumberId(obj[6].toString());
			}
		}

		if (obj.length > 7) {
			jecnTreeBean.setT_Path(obj[7].toString());
		}

		return jecnTreeBean;
	}

	public static JecnTreeBean getJecnTreeBeanFromObjectOrg(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();

		// 组织节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 组织节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 组织节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 组织节点父类名称
		jecnTreeBean.setPname("");
		jecnTreeBean.setTreeNodeType(TreeNodeType.organization);
		if (obj.length > 4) {
			if (Integer.parseInt(obj[3].toString()) > 0 || Integer.parseInt(obj[4].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
		} else {

			if (Integer.parseInt(obj[3].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
		}

		jecnTreeBean.setT_Path(obj[6].toString());

		return jecnTreeBean;
	}

	/**
	 * 岗位组对象转换成树对象
	 */
	public static JecnTreeBean getJecnTreeBeanByObjectGroup(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
			return null;
		}
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		// 岗位组节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 岗位组节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 岗位节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 岗位节点的父名称
		jecnTreeBean.setPname("dddd");
		// 岗位接节点类型
		if (Long.valueOf(obj[3].toString()) == 0) {
			jecnTreeBean.setTreeNodeType(TreeNodeType.positionGroup);
		} else {
			jecnTreeBean.setTreeNodeType(TreeNodeType.fileDir);
		}
		// 判断是否存在子节点
		if (Long.valueOf(obj[2].toString()) == 0 && Long.valueOf(obj[3].toString()) == 1) {
			// 当前岗位组中存在子节点
			jecnTreeBean.setChildNode(true);
		} else if (Long.valueOf(obj[2].toString()) != 0 && Long.valueOf(obj[3].toString()) == 1) {

			// 当前岗位组中存在子节点
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}
		return jecnTreeBean;

	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：岗位对象转换成树对象
	 * @param obj
	 * @return
	 */
	public static JecnTreeBean getJecnTreeBeanByObjectPos(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
			return null;
		}
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		// 岗位节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 岗位节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 岗位节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		jecnTreeBean.setTreeNodeType(TreeNodeType.position);
		// 岗位节点父类名称
		jecnTreeBean.setPname(obj[3].toString());
		jecnTreeBean.setChildNode(false);
		// 部门名称
		jecnTreeBean.setOrgName(obj[3].toString());
		if (obj.length > 4 && obj[4] != null) {
			jecnTreeBean.setNumberId(obj[4].toString());
		}
		return jecnTreeBean;
	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：岗位对象转换成树对象
	 * @param obj
	 * @return
	 */
	public static JecnTreeBean getJecnTreeBeanByObjectPosAcccess(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
			return null;
		}
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		// 岗位节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 岗位节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 岗位节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		jecnTreeBean.setTreeNodeType(TreeNodeType.position);
		// 岗位节点父类名称
		jecnTreeBean.setPname(obj[3].toString());
		jecnTreeBean.setChildNode(false);
		// 部门名称
		jecnTreeBean.setOrgName(obj[3].toString());
		if (obj.length > 4 && obj[4] != null) {
			jecnTreeBean.setDownLoad("1".equals(obj[4].toString()) ? true : false);
		}
		return jecnTreeBean;
	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：岗位组对象转换成树对象
	 * @param obj
	 * @return
	 */
	public static JecnTreeBean getJecnTreeBeanByObjectPosGroup(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
			return null;
		}
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		// 岗位组节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 岗位组节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 岗位组节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		if (obj[3] != null) {
			jecnTreeBean.setDownLoad("1".equals(obj[3].toString()) ? true : false);
		}
		jecnTreeBean.setTreeNodeType(TreeNodeType.positionGroup);
		jecnTreeBean.setChildNode(false);
		return jecnTreeBean;
	}

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：判断是不是连接线
	 * @param figureType
	 * @return
	 */
	public static boolean isRelateLine(String figureType) {
		if ("CommonLine".equals(figureType) || "ManhattanLine".equals(figureType)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 判断是否是活动
	 * 
	 * @param figure
	 * @return
	 */
	public static boolean isActive(String figureType) {
		if ("RoundRectWithLine".equals(figureType) || "Rhombus".equals(figureType) || "ITRhombus".equals(figureType)
				|| "DataImage".equals(figureType) || figureType.equals("ActiveFigureAR")
				|| figureType.equals("ExpertRhombusAR")) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为活动中的接口或子流程
	 * 
	 * @param figureType
	 * @return
	 */
	public static boolean isActivityFlow(String figureType) {
		if ("ActivityImplFigure".equals(figureType) || "ActivityOvalSubFlowFigure".equals(figureType)) {
			return true;
		}
		return false;
	}

	/**
	 * 返回活动字符串集合
	 * 
	 * @return
	 */
	public static List<String> getActiveLists() {
		return Arrays.asList("RoundRectWithLine", "DataImage", "Rhombus", "ITRhombus", "ActiveFigureAR",
				"ExpertRhombusAR");
	}

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：判断是不是角色
	 * @param figureType
	 * @return
	 */
	public static boolean isRole(String figureType) {
		if ("RoleFigure".equals(figureType) || "CustomFigure".equals(figureType)
				|| "VirtualRoleFigure".equals(figureType) || "RoleFigureAR".equals(figureType)) {
			return true;
		}
		return false;
	}

	/**
	 * 术语元素
	 * 
	 * @param figureType
	 * @return
	 */
	public static boolean isTermFigure(String figureType) {
		return "TermFigureAR".equals(figureType);
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：是否是流程管理员
	 * @param roleStr
	 * @return
	 */
	public static boolean isAdmin(String roleStr) {
		if ("admin".equals(roleStr)) {
			return true;
		}
		return false;
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：是否是流程设计专家
	 * @param roleStr
	 * @return
	 */
	public static boolean isDesign(String roleStr) {
		if ("design".equals(roleStr)) {
			return true;
		}
		return false;
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：是否是浏览管理员
	 * @param roleStr
	 * @return
	 */
	public static boolean isViewAdmin(String roleStr) {
		if ("viewAdmin".equals(roleStr)) {
			return true;
		}
		return false;
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：是否是设计管理员-文件
	 * @param roleStr
	 * @return
	 */
	public static boolean isDesignFileAdmin(String roleStr) {
		if ("isDesignFileAdmin".equals(roleStr)) {
			return true;
		}
		return false;
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：是否是设计管理员-流程
	 * @param roleStr
	 * @return
	 */
	public static boolean isDesignProcessAdmin(String roleStr) {
		if ("isDesignProcessAdmin".equals(roleStr)) {
			return true;
		}
		return false;
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：是否是设计管理员-制度
	 * @param roleStr
	 * @return
	 */
	public static boolean isDesignRuleAdmin(String roleStr) {
		if ("isDesignRuleAdmin".equals(roleStr)) {
			return true;
		}
		return false;
	}

	public static boolean isSecondAdmin(String roleStr) {
		if ("secondAdmin".equals(roleStr)) {
			return true;
		}
		return false;
	}

	/**
	 * 设置单个节点时候 需要设置节点的集合
	 * 
	 * @param relateId
	 * @param type
	 * @param nodeType
	 * @return
	 */
	public static List<Long> getNeedSetAuthNodesSingle(Long relateId, int type, TreeNodeType nodeType, IBaseDao baseDao)
			throws Exception {
		List<Long> listIds = new ArrayList<Long>();
		// 流程架构只设当前节点
		listIds.add(relateId);
		String hql = null;
		if (nodeType == TreeNodeType.fileDir) {
			hql = "select fileID from JecnFileBeanT where perFileId=? and isDir=1";
		} else if (nodeType == TreeNodeType.ruleDir) {
			hql = "select id from RuleT where perId=? and isDir <>0";
		} else if (nodeType == TreeNodeType.standardDir) {
			hql = "select criterionClassId from StandardBean where preCriterionClassId=? and stanType=1";
		}
		if (hql != null) {
			listIds.addAll(baseDao.listHql(hql, relateId));
		}
		return listIds;

	}

	/**
	 * 设置某个节点以及子节点的时候 需要设置节点的集合
	 * 
	 * @param relateId
	 * @param type
	 * @param nodeType
	 * @return
	 */
	public static List<Long> getNeedSetAuthNodesContainChildNode(Long relateId, int type, Long projectId,
			IBaseDao baseDao) throws Exception {
		String sql = getAllChilds(type);
		List<Object> selectList = baseDao.listNativeSql(sql, relateId);
		List<Long> listResult = new ArrayList<Long>();
		for (Object o : selectList) {
			listResult.add(Long.valueOf(o.toString()));
		}

		return listResult;
	}

	public static String getAllChilds(int type) {
		String tableName = "";
		String idName = "";

		switch (type) {
		case 0:
			tableName = "JECN_FLOW_STRUCTURE_T";
			idName = "FLOW_ID";

			break;
		case 1:
			tableName = "JECN_FIlE_T";
			idName = "FILE_ID";

			break;
		case 2:
			tableName = "JECN_CRITERION_CLASSES";
			idName = "CRITERION_CLASS_ID";

			break;
		case 3:
			tableName = "JECN_RULE_T";
			idName = "ID";
			break;
		case 4:
			tableName = "JECN_FLOW_ORG";
			idName = "ORG_ID";
			break;
		default:
			break;
		}

		String sql = "select c." + idName + " from " + tableName + " c inner join " + tableName
				+ " p on c.t_path like p.t_path" + JecnCommonSql.getConcatChar() + "'%' where p." + idName + "=?";
		return sql;
	}

	/**
	 * @author zhangchen Oct 4, 2013
	 * @description：获得所有子节点
	 * @param relateId
	 * @param listAll
	 * @param set
	 */
	private static void getAllChildNode(Long relateId, List<Object[]> listAll, List<Long> list) {
		for (Object[] obj : listAll) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			if (relateId.toString().equals(obj[1].toString())) {
				list.add(Long.valueOf(obj[0].toString()));
				getAllChildNode(Long.valueOf(obj[0].toString()), listAll, list);
			}
		}
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：保存日志
	 * @param relateId
	 *            日志对象
	 * @param relateName
	 *            日志对象名称
	 * @param type
	 *            1是流程，2是文件，3是角色,4是制度,5是标准，6组织,9是风险
	 * @param operatorType
	 *            1是新增，2是修改，3是删除，4移动，5排序，6假删除放到回收站，10自动编号，13设置模板
	 * @param updatePeopleId
	 * @param s
	 * @throws Exception
	 */
	public static void saveJecnJournal(Long relateId, String relateName, int type, int operatorType,
			Long updatePeopleId, IBaseDao baseDao) throws Exception {
		saveJecnJournal(relateId, relateName, type, operatorType, updatePeopleId, baseDao, 0);
	}

	public static void saveJecnJournal(Long relateId, String relateName, int type, int operatorType,
			Long updatePeopleId, IBaseDao baseDao, int secondOperatorType) throws Exception {
		JecnJournal jecnJournal = new JecnJournal();
		jecnJournal.setOperatorType(operatorType);
		jecnJournal.setRelateId(relateId);
		jecnJournal.setRelateName(relateName);
		jecnJournal.setType(type);
		jecnJournal.setUpdatePeopleId(updatePeopleId);
		jecnJournal.setUpdateTime(new Date());
		jecnJournal.setSecondOperatorType(secondOperatorType);
		baseDao.save(jecnJournal);
	}

	public static void saveJecnJournals(Collection<Long> listIds, int type, int operatorType, Long updatePeopleId,
			IBaseDao baseDao, int sencondOperatorType) throws Exception {
		if (listIds.size() == 0) {
			return;
		}
		String sql = JecnUtil.getJecnJournalsSql(type);
		if (!"".equals(sql)) {
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				baseDao.execteNative(str, type, operatorType, updatePeopleId, new Date(), sencondOperatorType);
			}
		}
	}

	/***************************************************************************
	 * @param relateId
	 * @param relateName
	 * @param type
	 *            1是流程，2是文件，3是角色,4是制度,5是标准，6组织
	 * @param operatorType
	 *            1是新增，2是修改，3是删除，4移动，5排序，6假删除放到回收站，10自动编号
	 * @param updatePeopleId
	 * @param s
	 * @throws Exception
	 */
	public static void saveJecnJournals(List<Long> listIds, int type, int operatorType, Long updatePeopleId,
			IBaseDao baseDao) throws Exception {
		saveJecnJournals(listIds, type, operatorType, updatePeopleId, baseDao, 0);
	}

	/***************************************************************************
	 * @param relateId
	 * @param relateName
	 * @param type
	 *            1是流程，2是文件，3是角色,4是制度,5是标准,6是组织9,风险，15术语定义
	 * @param operatorType
	 *            1是新增，2是修改，3是删除，4移动，5排序，6假删除放到回收站，10自动编号
	 * @param updatePeopleId
	 * @param s
	 * @throws Exception
	 */
	public static void saveJecnJournals(Set<Long> setIds, int type, int operatorType, Long updatePeopleId,
			IBaseDao baseDao) throws Exception {
		saveJecnJournals(setIds, type, operatorType, updatePeopleId, baseDao, 0);
	}

	/***************************************************************************
	 * @param type
	 *            1是流程，2是文件，3是角色,4是制度,5是标准,6是组织9,风险，15术语定义
	 * @throws Exception
	 */
	private static String getJecnJournalsSql(int type) {
		String sql = "";
		if (JecnContants.dbType == DBType.ORACLE) {
			sql = "Insert into JECN_JOURNAL (id,RELATE_ID,RELATE_NAME,type,OPERATOR_TYPE,UPDATE_PEOPLE_ID,UPDATE_TIME,SECOND_OPERATOR_TYPE)";
		} else {
			sql = "Insert into JECN_JOURNAL (RELATE_ID,RELATE_NAME,type,OPERATOR_TYPE,UPDATE_PEOPLE_ID,UPDATE_TIME,SECOND_OPERATOR_TYPE)";
		}

		switch (type) {
		case 1:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.flow_id,t.flow_name,?,?,?,?,? from jecn_flow_structure_t t where t.flow_id in";
			} else {
				sql = sql + " select t.flow_id,t.flow_name,?,?,?,?,? from jecn_flow_structure_t t where t.flow_id in";
			}
			break;
		case 2:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.file_id,t.file_name,?,?,?,?,? from jecn_file_t t where t.file_id in";
			} else {
				sql = sql + " select t.file_id,t.file_name,?,?,?,?,? from jecn_file_t t where t.file_id in";
			}
			break;
		case 3:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.role_id,t.role_name,?,?,?,?,? from jecn_role_info t where t.role_id in";
			} else {
				sql = sql + " select t.role_id,t.role_name,?,?,?,?,? from jecn_role_info t where t.role_id in";
			}
			break;
		case 4:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.id,t.rule_name,?,?,?,?,? from jecn_rule_t t where t.id in";
			} else {
				sql = sql + " select t.id,t.rule_name,?,?,?,?,? from jecn_rule_t t where t.id in";
			}
			break;
		case 5:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.CRITERION_CLASS_ID,t.CRITERION_CLASS_NAME,?,?,?,?,? from JECN_CRITERION_CLASSES t where t.CRITERION_CLASS_ID in";
			} else {
				sql = sql
						+ " select t.CRITERION_CLASS_ID,t.CRITERION_CLASS_NAME,?,?,?,?,? from JECN_CRITERION_CLASSES t where t.CRITERION_CLASS_ID in";
			}
			break;
		case 6:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.ORG_ID,t.ORG_NAME,?,?,?,?,? from JECN_FLOW_ORG t where t.ORG_ID in";
			} else {
				sql = sql + " select t.ORG_ID,t.ORG_NAME,?,?,?,?,? from JECN_FLOW_ORG t where t.ORG_ID in";
			}
			break;
		case 9:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql + " select JECN_JOURNAL_SQUENCE.Nextval,t.ID,t.NAME,?,?,?,?,? from JECN_RISK t where t.ID in";
			} else {
				sql = sql + " select t.ID,t.NAME,?,?,?,?,? from JECN_RISK t where t.ID in";
			}
			break;
		case 15:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.ID,t.NAME,?,?,?,?,? from TERM_DEFINITION_LIBRARY t where t.ID in";
			} else {
				sql = sql + " select t.ID,t.NAME,?,?,?,?,? from TERM_DEFINITION_LIBRARY t where t.ID in";
			}
			break;
		case 16:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.ID,t.NAME,?,?,?,?,? from jecn_position_group t where t.ID in";
			} else {
				sql = sql + " select t.ID,t.NAME,?,?,?,?,? from jecn_position_group t where t.ID in";
			}
			break;
		case 17:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.flow_sustain_tool_id,t.FLOW_SUSTAIN_TOOL_DESCRIBE,?,?,?,?,? from JECN_FLOW_SUSTAIN_TOOL t where t.flow_sustain_tool_id in";
			} else {
				sql = sql
						+ " select t.flow_sustain_tool_id,t.FLOW_SUSTAIN_TOOL_DESCRIBE,?,?,?,?,? from JECN_FLOW_SUSTAIN_TOOL t where t.flow_sustain_tool_id in";
			}
			break;
		case 18:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,t.flow_sustain_tool_id,t.FLOW_SUSTAIN_TOOL_DESCRIBE,?,?,?,?,? from JECN_FLOW_SUSTAIN_TOOL t where t.flow_sustain_tool_id in";
			} else {
				sql = sql + " select t.id,t.name,?,?,?,?,? from JECN_CONTROL_GUIDE t where t.id in";
			}
			break;
		case 19:
			if (JecnContants.dbType == DBType.ORACLE) {
				sql = sql
						+ " select JECN_JOURNAL_SQUENCE.Nextval,id,t.mode_name,?,?,?,?,? from JECN_MODE t where t.id in";
			} else {
				sql = sql + " select t.id,t.mode_name,?,?,?,?,? from JECN_MODE t where t.id in";
			}
			break;
		default:
			throw new IllegalArgumentException("getJecnJournalsSql 不支持的类型：" + type);
		}
		return sql;
	}

	/**
	 * 删除查阅权限
	 * 
	 * @param set
	 * @param type
	 *            0是流程、1是文件、2是标准、3制度
	 * @param baseDao
	 */
	public static void deleteViewAuth(Set<Long> set, int type, IBaseDao baseDao) throws Exception {
		// 岗位 查阅权限
		String hql = "delete from JecnAccessPermissionsT where type=? and relateId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, set);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		hql = "delete from JecnAccessPermissions where type=? and relateId in";
		list = JecnCommonSql.getSetSqls(hql, set);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 部门查阅权限
		hql = "delete from JecnOrgAccessPermissionsT where type=? and relateId in";
		list = JecnCommonSql.getSetSqls(hql, set);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		hql = "delete from JecnOrgAccessPermissions where type=? and relateId in";
		list = JecnCommonSql.getSetSqls(hql, set);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 删除查阅权限
	 * 
	 * @param set
	 * @param type
	 *            0是流程、1是文件、2是标准、3制度
	 * @param baseDao
	 */
	public static void deleteViewAuth(List<Long> listIds, int type, IBaseDao baseDao) throws Exception {
		// 岗位 查阅权限
		String hql = "delete from JecnAccessPermissionsT where type=? and relateId in";
		List<String> list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		hql = "delete from JecnAccessPermissions where type=? and relateId in";
		list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 部门查阅权限
		hql = "delete from JecnOrgAccessPermissionsT where type=? and relateId in";
		list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		hql = "delete from JecnOrgAccessPermissions where type=? and relateId in";
		list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 设计权限
	 * 
	 * @param listIds
	 * @param type
	 *            0是流程、1是文件、2是标准、3是制度
	 * @param baseDao
	 */
	public static void deleteDesignAuth(List<Long> listIds, int type, IBaseDao baseDao) throws Exception {
		String hql = "delete from JecnRoleContent where type=? and relateId in";
		List<String> list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 设计权限
	 * 
	 * @param listIds
	 * @param type
	 *            0是流程、1是文件、2是标准、3是制度
	 * @param baseDao
	 */
	public static void deleteDesignAuth(Set<Long> setIds, int type, IBaseDao baseDao) throws Exception {
		String hql = "delete from JecnRoleContent where type=? and relateId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 删除任务
	 * 
	 * @param setIds
	 * @param type
	 *            0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
	 * @param baseDao
	 */
	public static void deleteTask(Set<Long> setIds, int type, IBaseDao baseDao) throws Exception {
		List<String> list = null;
		String hql = "";

		// 删除任务各阶段审批人记录表
		hql = "delete from JecnTaskApprovePeopleConn where stageId in (select id from JecnTaskStage where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str + ")", type);
		}

		// 删除任务各阶段信息
		hql = "delete from JecnTaskStage where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}

		// 删除任务目标人表
		hql = "delete from JecnTaskPeopleNew where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务流传记录表
		hql = "delete from JecnTaskForRecodeNew where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务试运行文件表
		hql = "delete from JecnTaskTestRunFile where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务任务显示项表
		hql = "delete from JecnTaskApplicationNew where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务主表
		hql = "delete from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 删除任务
	 * 
	 * @param setIds
	 * @param type
	 *            0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
	 * @param baseDao
	 */
	public static void deleteTask(List<Long> listIds, int type, IBaseDao baseDao) throws Exception {
		List<String> list = null;
		String hql = "";

		// 删除任务各阶段审批人记录表
		hql = "delete from JecnTaskApprovePeopleConn where stageId in (select id from JecnTaskStage where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getListSqlAddBracket(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str + ")", type);
		}
		// 删除任务各阶段信息
		hql = "delete from JecnTaskStage where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getListSqlAddBracket(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务目标人表
		hql = "delete from JecnTaskPeopleNew where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getListSqlAddBracket(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务流传记录表
		hql = "delete from JecnTaskForRecodeNew where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getListSqlAddBracket(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务试运行文件表
		hql = "delete from JecnTaskTestRunFile where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getListSqlAddBracket(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务任务显示项表
		hql = "delete from JecnTaskApplicationNew where taskId in (select id from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getListSqlAddBracket(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		// 删除任务主表
		hql = "delete from JecnTaskBeanNew where taskType=? and rid in";
		list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 删除文控信息
	 * 
	 * @param listIds
	 * @param type
	 *            0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @param baseDao
	 */
	public static void deleteRecord(List<Long> listIds, int type, IBaseDao baseDao) throws Exception {
		List<String> list = null;
		String hql = "";
		hql = "delete JecnTaskHistoryFollow where historyId in (select id from JecnTaskHistoryNew where type=? and relateId in";
		list = JecnCommonSql.getListSqlAddBracket(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		hql = "delete from JecnTaskHistoryNew where type=? and relateId in";
		list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 删除文控信息
	 * 
	 * @param listIds
	 * @param type
	 *            0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @param baseDao
	 */
	public static void deleteRecord(Set<Long> setIds, int type, IBaseDao baseDao) throws Exception {
		List<String> list = null;
		String hql = "";
		hql = "delete JecnTaskHistoryFollow where historyId in (select id from JecnTaskHistoryNew where type=? and relateId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
		hql = "delete from JecnTaskHistoryNew where type=? and relateId in";
		list = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : list) {
			baseDao.execteHql(str, type);
		}
	}

	/**
	 * 获得流程未发布的文件
	 * 
	 * 对这个方法修改时，必须对JecnTaskCommon类findProcessNoPubFiles方法进行修改
	 * 
	 * @param flowId
	 * @return
	 */
	public static List<Long> findProcessNoPubFiles(Long flowId, IBaseDao baseDao) throws Exception {
		List<Long> result = new ArrayList<Long>();
		List<Object[]> objs = JecnTaskCommon.findProcessNoPubFiles(flowId, baseDao);
		for (Object[] obj : objs) {
			if (obj[0] == null) {
				continue;
			}
			result.add(Long.valueOf(obj[0].toString()));
		}
		return result;
	}

	/**
	 * 获得流程地图未发布的文件
	 * 
	 * 对这个方法修改时，必须对JecnTaskCommon类findProcessMapNoPubFiles方法进行修改
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public static List<Long> findProcessMapNoPubFiles(Long flowId, IBaseDao baseDao) throws Exception {
		// 流程地图对应附件、图片插入框对应图片文件、制度元素关联制度文件、制度元素关联制度中含有的文件项、图形元素关联的附件
		List<Long> result = new ArrayList<Long>();
		List<Object[]> objs = JecnTaskCommon.findProcessMapNoPubFiles(flowId, baseDao);
		for (Object[] obj : objs) {
			if (obj[0] == null) {
				continue;
			}
			result.add(Long.valueOf(obj[0].toString()));
		}
		return result;
	}

	/**
	 * 获得制度（文件）未发布的文件
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static List<Long> findRuleFileNoPubFiles(Long ruleId, IBaseDao baseDao) throws Exception {
		// String sql = "select distinct rule_file.file_id from ("
		// + " select t.file_id,jf.file_id as file_pub_id from jecn_file_t t"
		// +
		// "        LEFT JOIN jecn_file jf ON t.file_id = jf.file_id and jf.del_state=0"
		// +
		// "       ,jecn_rule_t jr where t.del_state=0 and t.file_id = jr.file_id and jr.is_dir=2 "
		// +
		// "and jr.is_file_local=0 and jr.is_file_local=0 and jr.id=?) rule_file"
		// + "       where rule_file.file_pub_id is null";
		String sql = "SELECT T.FILE_ID" + "  FROM JECN_FILE_T T" + " INNER JOIN (SELECT JR.FILE_ID"
				+ "               FROM JECN_RULE JR" + "              WHERE JR.IS_DIR = 2"
				+ "                AND JR.IS_FILE_LOCAL = 0" + "                AND JR.ID = ?" + "             UNION"
				+ "             SELECT RT.FILE_ID" + "               FROM RULE_STANDARDIZED_FILE_T RT"
				+ "              WHERE RT.RELATED_ID = ?) FILE_REF_TMP" + "    ON T.FILE_ID = FILE_REF_TMP.FILE_ID"
				+ "  LEFT JOIN JECN_FILE JF" + "    ON T.FILE_ID = JF.FILE_ID"
				+ "   AND (T.UPDATE_TIME > JF.UPDATE_TIME or JF.UPDATE_TIME is null)";
		return getLongList(baseDao.listNativeSql(sql, ruleId, ruleId));
	}

	public static List<Long> getLongList(List<Object> list) {
		List<Long> longList = null;
		if (list != null) {
			longList = new ArrayList<Long>();
			for (Object o : list) {
				longList.add(Long.valueOf(o.toString()));
			}
		}
		return longList;

	}

	/**
	 * 获得制度（模版）未发布的文件
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static List<Long> findRuleModeNoPubFiles(Long ruleId, IBaseDao baseDao) throws Exception {
		String sql = "select t.file_id" + "                  from jecn_file_t t"
				+ "                  LEFT JOIN jecn_file jf ON t.file_id = jf.file_id"
				+ "                ,jecn_rule_t jr,rule_title_t rt,rule_file_t rf where t.file_id=rf.rule_file_id"
				+ "                and rf.rule_title_id=rt.id and rt.rule_id=jr.id and jr.is_dir=1 and jr.id=?"
				+ "                 AND (T.UPDATE_TIME > JF.UPDATE_TIME or JF.UPDATE_TIME is null)";
		return baseDao.listObjectNativeSql(sql, "file_id", Hibernate.LONG, ruleId);
	}

	/**
	 * 获得流程地图中未发布的制度
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public static List<Long> getRelateRuleByFlowMapId(Long flowId, IBaseDao baseDao) throws Exception {
		String sql = "select distinct t.id from jecn_rule_t t left join jecn_rule jr on t.id=jr.id where jr.id is null"
				+ "      and t.id in (select jr.id from jecn_rule_t jr,jecn_flow_structure_image_t jfsi where jr.id=jfsi.link_flow_id and jfsi.figure_type="
				+ JecnCommonSql.getSystemString() + " and jfsi.flow_id=?)";
		return getLongList(baseDao.listNativeSql(sql, flowId));
	}

	/**
	 * 获得流程中未发布的制度
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public static List<Long> getRelateRuleByFlowId(Long flowId, IBaseDao baseDao) throws Exception {
		// c.isDir 0是目录，1是制度,2是制度文件
		String sql = "select distinct t.id from jecn_rule_t t left join jecn_rule jr on t.id=jr.id where jr.id is null"
				+ "      and t.id in (select jr.id from jecn_rule_t jr,flow_related_criterion_t frc where jr.id=frc.criterion_class_id and frc.flow_id=?)";
		return getLongList(baseDao.listNativeSql(sql, flowId));
	}

	/**
	 * 排序活动
	 * 
	 * @param list
	 * @return
	 */
	public static void getActivitySort(List<ProcessActiveWebBean> list) {
		// 外循环控制比较的次数
		for (int i = 0; i < list.size(); i++) {
			// 内循环控制比较后移位
			for (int j = list.size() - 1; j > i; j--) {
				ProcessActiveWebBean processActiveWebBeanOne = list.get(j - 1);
				ProcessActiveWebBean processActiveWebBeanTwo = list.get(j);
				String oneActivesNumber = processActiveWebBeanOne.getActiveId();
				String twoActivesNumber = processActiveWebBeanTwo.getActiveId();
				if (oneActivesNumber != null && twoActivesNumber != null) {
					if (oneActivesNumber.indexOf("-") > 0) {
						oneActivesNumber = oneActivesNumber.substring(oneActivesNumber.lastIndexOf("-") + 1,
								oneActivesNumber.length());
					}
					if (!oneActivesNumber.matches("[0-9]+")) {
						list.set(j - 1, processActiveWebBeanTwo);
						list.set(j, processActiveWebBeanOne);
						continue;
					}

					if (twoActivesNumber.indexOf("-") > 0) {
						twoActivesNumber = twoActivesNumber.substring(twoActivesNumber.lastIndexOf("-") + 1,
								twoActivesNumber.length());
					}
					if (!twoActivesNumber.matches("[0-9]+")) {
						continue;
					}

					if (Long.valueOf(oneActivesNumber) > Long.valueOf(twoActivesNumber)) {
						list.set(j - 1, processActiveWebBeanTwo);
						list.set(j, processActiveWebBeanOne);
					}
				} else if (oneActivesNumber == null) {
					list.set(j - 1, processActiveWebBeanTwo);
					list.set(j, processActiveWebBeanOne);
				}
			}
		}
	}

	/**
	 * 排序活动
	 * 
	 * @param list
	 * @return
	 */
	public static void getActivityObjSort(List<Object[]> list) {
		// 外循环控制比较的次数
		for (int i = 0; i < list.size(); i++) {
			// 内循环控制比较后移位
			for (int j = list.size() - 1; j > i; j--) {
				Object[] processActiveBeanOne = list.get(j - 1);
				Object[] processActiveBeanTwo = list.get(j);
				Object oneActivesNumber = processActiveBeanOne[1];
				Object twoActivesNumber = processActiveBeanTwo[1];
				if (oneActivesNumber != null && twoActivesNumber != null) {
					if (oneActivesNumber.toString().indexOf("-") > 0) {
						oneActivesNumber = oneActivesNumber.toString().substring(
								oneActivesNumber.toString().lastIndexOf("-") + 1, oneActivesNumber.toString().length());
					}
					if (!oneActivesNumber.toString().matches("[0-9]+")) {
						list.set(j - 1, processActiveBeanTwo);
						list.set(j, processActiveBeanOne);
						continue;
					}

					if (twoActivesNumber.toString().indexOf("-") > 0) {
						twoActivesNumber = twoActivesNumber.toString().substring(
								twoActivesNumber.toString().lastIndexOf("-") + 1, twoActivesNumber.toString().length());
					}
					if (!twoActivesNumber.toString().matches("[0-9]+")) {
						continue;
					}

					if (Long.valueOf(oneActivesNumber.toString()) > Long.valueOf(twoActivesNumber.toString())) {
						list.set(j - 1, processActiveBeanTwo);
						list.set(j, processActiveBeanOne);
					}
				} else if (oneActivesNumber == null) {
					list.set(j - 1, processActiveBeanTwo);
					list.set(j, processActiveBeanOne);
				}
			}
		}
	}

	/**
	 * 获得制度查阅bean
	 * 
	 * @param obj
	 * @return
	 */
	public static RuleInfoBean getRuleInfoBeanByObject(Object[] obj) {
		if (obj == null || obj[0] == null || obj[4] == null) {
			return null;
		}
		RuleInfoBean ruleInfoBean = new RuleInfoBean();
		// 制度主键Id
		ruleInfoBean.setRuleId(Long.valueOf(obj[0].toString()));
		// 制度名称
		if (obj[1] != null)
			ruleInfoBean.setRuleName(obj[1].toString());
		// 制度编号
		if (obj[2] != null)
			ruleInfoBean.setRuleNum(obj[2].toString());
		// 制度文件Id
		if (obj[3] != null)
			ruleInfoBean.setFileId(Long.valueOf(obj[3].toString()));
		// 制度类型
		ruleInfoBean.setIsDir(Integer.valueOf(obj[4].toString()));
		// 制度类别
		if (obj[5] != null && obj[6] != null) {
			ruleInfoBean.setRuleTypeNameId(Long.valueOf(obj[5].toString()));
			ruleInfoBean.setRuleTypeName(obj[6].toString());
		}
		// 责任部门
		if (obj[7] != null && obj[8] != null) {
			ruleInfoBean.setDutyOrgId(Long.valueOf(obj[7].toString()));
			ruleInfoBean.setDutyOrg(obj[8].toString());
		}
		// 密级
		if (obj[9] != null) {
			ruleInfoBean.setSecretLevel(Integer.parseInt(obj[9].toString()));
		}
		// 发布日期
		if (obj[10] != null) {
			ruleInfoBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[10].toString())));
		}
		return ruleInfoBean;
	}

	/**
	 * 标准对象
	 * 
	 * @param obj
	 * @return
	 */
	public static StandardWebBean getStandardWebBeanByObject(Object[] obj) {
		if (obj == null || obj[0] == null || obj[2] == null || obj[4] == null || obj[6] == null) {
			return null;
		}

		StandardWebBean standardWebBean = new StandardWebBean();
		// 主键ID
		standardWebBean.setId(Long.valueOf(obj[0].toString()));
		// 标准名称
		if (obj[1] != null) {
			standardWebBean.setName(obj[1].toString());
		} else {
			standardWebBean.setName("");
		}
		// 父目录的名称
		if (obj[3] != null) {
			standardWebBean.setPname(obj[3].toString());
		} else {
			standardWebBean.setPname("");
		}
		// 标准类型
		standardWebBean.setType(obj[4].toString());
		// 关联ID
		if (obj[5] != null) {
			standardWebBean.setFileId(Long.parseLong(obj[5].toString()));
			if (obj[7] != null) {
				standardWebBean.setFileName(obj[7].toString());
			}
		} else {
			standardWebBean.setFileId(-1);
			standardWebBean.setFileName("");
		}
		// 密级
		if (obj[6] != null) {
			standardWebBean.setSecretLevel(Long.parseLong(obj[6].toString()));

		}
		return standardWebBean;
	}

	/**
	 * 文件对象
	 * 
	 * @param obj
	 * @return
	 */
	// t.file_id,t.file_name,t.doc_id,t.org_id,org.org_name,t.is_public
	public static FileWebBean getFileWebBeanByObject(Object[] obj) {
		if (obj == null || obj[0] == null) {
			return null;
		}
		FileWebBean fileWebBean = new FileWebBean();
		// 文件ID
		fileWebBean.setFileId(Long.valueOf(obj[0].toString()));
		// 文件名称
		if (obj[1] != null) {
			fileWebBean.setFileName(obj[1].toString());
		} else {
			fileWebBean.setFileName("");
		}
		// 文件编号
		if (obj[2] != null) {
			fileWebBean.setFileNum(obj[2].toString());
		} else {
			fileWebBean.setFileNum("");
		}
		// 文件责任部门
		if (obj[4] != null) {
			fileWebBean.setOrgName(obj[4].toString());
		} else {
			fileWebBean.setOrgName("");
		}
		// 文件密级
		if (obj[5] != null) {
			fileWebBean.setSecretLevel(Integer.parseInt(obj[5].toString()));
		} else {
			fileWebBean.setSecretLevel(0);
		}
		return fileWebBean;
	}

	/**
	 * 风险对象
	 * 
	 * @param object
	 * @return jr.id,jr.risk_code,jr.name,jr.grade
	 */
	public static RiskWebBean getRiskWebBeanByObject(Object[] object) {
		if (object == null || object[0] == null) {
			return null;
		}
		RiskWebBean riskWebBean = new RiskWebBean();
		riskWebBean.setId(Long.valueOf(object[0].toString()));
		if (object[1] != null) {
			riskWebBean.setRiskCode(object[1].toString());
		} else {
			riskWebBean.setRiskCode("");
		}
		if (object[2] != null) {
			riskWebBean.setName(object[2].toString());
		} else {
			riskWebBean.setName("");
		}
		if (object[3] != null) {
			riskWebBean.setGrade(Integer.parseInt(object[3].toString()));
			if (Integer.parseInt(object[3].toString()) == 1) {
				riskWebBean.setGradeStr("高");
			}
			if (Integer.parseInt(object[3].toString()) == 2) {
				riskWebBean.setGradeStr("中");
			}
			if (Integer.parseInt(object[3].toString()) == 3) {
				riskWebBean.setGradeStr("低");
			}
		} else {
			riskWebBean.setGrade(0);
		}
		return riskWebBean;
	}

	/**
	 * 查询风险点详细信息
	 * 
	 * @param jr
	 *            .id, jr.risk_code, jr.name, jr.grade, jr.standardcontrol,
	 *            jc.id, jc.description
	 * @return
	 */
	public static RiskDetailBean getRiskDetailBeanByObject(Object[] object) {
		if (object == null || object[0] == null) {
			return null;
		}
		RiskDetailBean riskDetailBean = new RiskDetailBean();
		riskDetailBean.setRiskId(Long.valueOf(object[0].toString()));
		if (object[1] != null) {
			riskDetailBean.setRiskCode(object[1].toString());
		}
		if (object[2] != null) {
			riskDetailBean.setName(object[2].toString());
		}
		if (object[3] != null) {
			riskDetailBean.setGrade(Long.valueOf(object[3].toString()));
		}
		if (object[4] != null) {
			riskDetailBean.setStandardControl(object[4].toString());
		}
		if (object[5] != null) {
			riskDetailBean.setControlId(object[5].toString());
		}
		if (object[6] != null) {
			riskDetailBean.setControlDescription(object[6].toString());
		}
		return riskDetailBean;
	}

	/**
	 * 获得流程查阅bean
	 * 
	 * @param obj
	 * @return
	 */
	public static ProcessWebBean getProcessWebBeanByObject(Object[] obj) {
		ProcessWebBean processWebBean = new ProcessWebBean();
		if (obj[0] != null) {
			// 流程ID
			processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
		}
		// 流程名称
		if (obj[1] != null) {
			processWebBean.setFlowName(obj[1].toString());
		}
		// 流程编号
		if (obj[2] != null) {
			processWebBean.setFlowIdInput(obj[2].toString());
		}
		// 责任部门
		if (obj[3] != null && obj[4] != null) {
			processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
			processWebBean.setOrgName(obj[4].toString());
		}
		// 责任人
		if (obj[5] != null && obj[6] != null && obj[7] != null) {
			processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
			processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
			processWebBean.setResPeopleName(obj[7].toString());
		}
		// 更新时间
		if (obj[8] != null && obj[10] != null) {
			processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[8].toString())));
			processWebBean.setNoUpdateDate(JecnCommon.getMonth(JecnCommon.getDateByString(obj[10].toString()),
					JecnCommon.getDateByString(obj[8].toString())));
		}
		// 密级
		if (obj[9] != null) {
			processWebBean.setIsPublic(Integer.parseInt(obj[9].toString()));
		}
		return processWebBean;
	}

	/**
	 * 获得流程查阅bean
	 * 
	 * @param obj
	 * @return
	 */
	public static ProcessWebBean getProcessWebBeanByObjectCount(Object[] obj) {
		if (obj == null || obj[0] == null) {
			return null;
		}
		ProcessWebBean processWebBean = new ProcessWebBean();
		// 流程ID
		processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
		// 流程名称
		if (obj[1] != null) {
			processWebBean.setFlowName(obj[1].toString());
		}
		// 流程编号
		if (obj[2] != null) {
			processWebBean.setFlowIdInput(obj[2].toString());
		}
		// 责任部门
		if (obj[3] != null && obj[4] != null) {
			processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
			processWebBean.setOrgName(obj[4].toString());
		}
		// 责任人
		if (obj[5] != null && obj[6] != null && obj[7] != null) {
			processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
			processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
			processWebBean.setResPeopleName(obj[7].toString());
		}
		return processWebBean;
	}

	/**
	 * 两个时间相差的月数
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * @throws ParseException
	 */
	public static int getMonthSpace(Date date1, Date date2) throws ParseException {
		int iMonth = 0;
		int flag = 0;
		try {
			Calendar objCalendarDate1 = Calendar.getInstance();
			objCalendarDate1.setTime(date1);

			Calendar objCalendarDate2 = Calendar.getInstance();
			objCalendarDate2.setTime(date2);

			if (objCalendarDate2.equals(objCalendarDate1))
				return 0;
			if (objCalendarDate1.after(objCalendarDate2)) {
				Calendar temp = objCalendarDate1;
				objCalendarDate1 = objCalendarDate2;
				objCalendarDate2 = temp;
			}
			if (objCalendarDate2.get(Calendar.DAY_OF_MONTH) < objCalendarDate1.get(Calendar.DAY_OF_MONTH))
				flag = 1;

			if (objCalendarDate2.get(Calendar.YEAR) > objCalendarDate1.get(Calendar.YEAR))
				iMonth = ((objCalendarDate2.get(Calendar.YEAR) - objCalendarDate1.get(Calendar.YEAR)) * 12
						+ objCalendarDate2.get(Calendar.MONTH) - flag)
						- objCalendarDate1.get(Calendar.MONTH);
			else
				iMonth = objCalendarDate2.get(Calendar.MONTH) - objCalendarDate1.get(Calendar.MONTH) - flag;

		} catch (Exception e) {
			log.error("", e);
		}
		return iMonth;
	}

	/**
	 * 
	 * 获取给定字符串的字节长度，汉字为2个字节，字母数字等为一个字节
	 * 
	 * 字符串为null或""时返回0
	 * 
	 * @param text
	 *            字符串
	 * @return int 返回字符串的字节长度
	 */
	public static int getTextLength(String text) {
		try {
			return (text != null) ? text.getBytes("GBK").length : 0;
		} catch (UnsupportedEncodingException e) {
			return (text != null) ? text.getBytes().length : 0;
		}
	}

	/**
	 * Object转换为Integer
	 * 
	 * @author weidp
	 * @param object
	 * @return
	 */
	@Deprecated
	public static Integer valueToInt(Object object) {
		// 此方法有缺陷 为null时不应当返回0
		return object == null ? 0 : Integer.valueOf(object.toString());
	}

	/**
	 * Object转换为Long
	 * 
	 * @author weidp
	 * @param object
	 * @return
	 */
	@Deprecated
	public static Long valueToLong(Object object) {
		// 此方法有缺陷 为null时不应当返回0
		return object == null ? 0 : Long.valueOf(object.toString());
	}

	/**
	 * @author weidp
	 * @description 对象转换
	 * @param object
	 * @return
	 */
	public static String valueToString(Object object) {
		return object == null ? "" : object.toString();
	}

	/**
	 * Date转换为 'yyyy-MM-dd' 类型的字符串
	 * 
	 * @author weidp
	 * @param object
	 * @return
	 */
	public static String valueToDateStr(Object object) {
		if (object == null) {
			return "";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// 先将要格式化的字符串转为Date类型
		Date date = null;
		try {
			date = dateFormat.parse(object.toString());
		} catch (ParseException e) {
			log.error("传入的object非Data类型：", e);
		}
		return dateFormat.format(date);
	}

	/**
	 * 日期转换成字符串
	 * 
	 * @param date
	 * @return str
	 */
	public static String DateToStr(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str = format.format(date);
		return str;
	}

	/**
	 * 日期转换成字符串
	 * 
	 * @param date
	 * @return str
	 */
	public static String DateToStr(Date date, String formatStr) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(formatStr);
		String str = format.format(date);
		return str;
	}

	/**
	 * 字符串转换成日期
	 * 
	 * @param str
	 * @return date
	 */
	public static Date StrToDate(String str) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			log.error("转换为日期报错：str=" + str + e);
		}
		return date;
	}

	/**
	 * 两个时间差
	 * 
	 * @param endTimeStr
	 * @param startTimeStr
	 * @return
	 */
	public static int getMonthDiff(String endTimeStr, String startTimeStr) {
		Date startTime = StrToDate(startTimeStr);
		Calendar c = Calendar.getInstance();
		c.setTime(startTime);
		int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		Date endTime = StrToDate(endTimeStr);
		c.setTime(endTime);
		int year2 = c.get(Calendar.YEAR);
		int month2 = c.get(Calendar.MONTH);
		int result;
		if (year1 == year2) {
			result = month1 - month2;
		} else {
			result = 12 * (year1 - year2) + month1 - month2;
		}
		return result;
	}

	/**
	 * 是否为华帝登录
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-27 时间：上午10:33:03
	 * @return
	 */
	public static boolean isVattiLogin() {
		return JecnContants.otherLoginType == 8 ? true : false;
	}

	/**
	 * 插入图片到EXCEL
	 * 
	 * @author huoyl
	 * @param picSheet
	 *            sheet
	 * @param pictureFile
	 *            图片file对象
	 * @param cellRow
	 *            行数
	 * @param cellCol
	 *            列数
	 * @throws Exception
	 * 
	 */
	public static void addPictureToExcel(WritableSheet picSheet, File pictureFile, int picBeginRow, int picBeginCol)
			throws Exception {
		// 开始位置

		// 图片间的高度，宽度
		double picCellWidth = 0.0;
		double picCellHeight = 0.0;
		// 读入图片
		BufferedImage picImage = ImageIO.read(pictureFile);
		// 取得图片的像素高度，宽度
		int picWidth = picImage.getWidth();
		int picHeight = picImage.getHeight();

		// 计算图片的实际宽度
		int picWidth_t = picWidth * 32; // 具体的实验值，原理不清楚。
		for (int x = 0; x < 500; x++) {
			int bc = (int) Math.floor(picBeginCol + x);
			// 得到单元格的宽度
			int v = picSheet.getColumnView(bc).getSize();
			double offset0_t = 0.0;
			if (0 == x)
				offset0_t = (picBeginCol - bc) * v;
			if (0.0 + offset0_t + picWidth_t > v) {
				// 剩余宽度超过一个单元格的宽度
				double ratio_t = 1.0;
				if (0 == x) {
					ratio_t = (0.0 + v - offset0_t) / v;
				}
				picCellWidth += ratio_t;
				picWidth_t -= (int) (0.0 + v - offset0_t);
			} else { // 剩余宽度不足一个单元格的宽度
				double ratio_r = 0.0;
				if (v != 0)
					ratio_r = (0.0 + picWidth_t) / v;
				picCellWidth += ratio_r;
				break;
			}
		}
		// 计算图片的实际高度
		int picHeight_t = picHeight * 15;
		for (int x = 0; x < 500; x++) {
			int bc = (int) Math.floor(picBeginRow + x);
			// 得到单元格的高度
			int v = picSheet.getRowView(bc).getSize();
			double offset0_r = 0.0;
			if (0 == x)
				offset0_r = (picBeginRow - bc) * v;
			if (0.0 + offset0_r + picHeight_t > v) {
				// 剩余高度超过一个单元格的高度
				double ratio_q = 1.0;
				if (0 == x)
					ratio_q = (0.0 + v - offset0_r) / v;
				picCellHeight += ratio_q;
				picHeight_t -= (int) (0.0 + v - offset0_r);
			} else {// 剩余高度不足一个单元格的高度
				double ratio_m = 0.0;
				if (v != 0)
					ratio_m = (0.0 + picHeight_t) / v;
				picCellHeight += ratio_m;
				break;
			}
		}
		// 生成一个图片对象。
		WritableImage image = new WritableImage(picBeginCol, picBeginRow, picCellWidth, picCellHeight, pictureFile);
		// 把图片插入到sheet
		picSheet.addImage(image);

	}

	/**
	 * 在日期上增加月份
	 * 
	 * @author weidp
	 * @date 2014-10-30 下午05:55:57
	 * @param pubDate
	 * @param expiry
	 * @return
	 */
	public static String plusMonthOnDate(String dateStr, int month) {
		if (StringUtils.isBlank(dateStr)) {
			return "";
		}
		// 字符串转换日期格式
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// 得到日期格式对象
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			log.error("转换日期错误:需要的格式 yyyy-MM-dd", e);
		}
		return plusMonthOnDate(date, month);
	}

	/**
	 * 在日期上增加月份
	 * 
	 * @author weidp
	 * @date 2014-10-30 下午05:55:57
	 * @param pubDate
	 * @param expiry
	 * @return
	 */
	public static String plusMonthOnDate(Date date, int month) {
		// 字符串转换日期格式
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date result = plusMonthOnDateForDate(date, month);
		return sdf.format(result);
	}

	/**
	 * 在日期上增加月份
	 * 
	 * @author weidp
	 * @date 2014-10-30 下午05:55:57
	 * @param pubDate
	 * @param expiry
	 * @return
	 */
	public static Date plusMonthOnDateForDate(Date date, int month) {
		// 字符串转换日期格式
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// 获取日历实例
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		// 增加月份
		c.add(Calendar.MONTH, month);
		// Date result = c.getTime();
		return c.getTime();
	}

	/**
	 * 增加日期
	 * 
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date addDay(Date date, int day) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, day);
		return c.getTime();
	}

	/**
	 * 获取对应的配置项
	 * 
	 * @param configItemList
	 * @param mark
	 * @return
	 */
	public static String getConfigItemValue(List<JecnConfigItemBean> configItemList, String mark) {
		if (configItemList == null || configItemList.size() == 0) {
			return "";
		}
		for (JecnConfigItemBean itemBean : configItemList) {
			if (itemBean.getMark().equals(mark)) {
				return itemBean.getValue();
			}
		}
		return "";
	}

	/**
	 * 保存邮件信息
	 */
	public static void saveEmail(List<EmailBasicInfo> emails) {

		try {

			IEmailService service = (IEmailService) ApplicationContextUtil.getContext().getBean("emailServiceImpl");
			List<EmailMessageBean> emailBeanList = service.saveEmail(emails);
			TimerEmailSendAction timerEmailSendAction = (TimerEmailSendAction) ApplicationContextUtil.getContext()
					.getBean("timerEmailSendAction");
			if (!emailBeanList.isEmpty()) {
				timerEmailSendAction.putAll(emailBeanList);
			}
		} catch (Exception e) {
			log.error("保存邮件异常", e);
		}

	}

	public static void main(String[] args) {
		JecnUtil.StrToDate("2015-12-28 15:46");
	}

	public static boolean isToOrFrom(String figureType) {
		if ("ToFigure".equals(figureType) || "FromFigure".equals(figureType)) {
			return true;
		}
		return false;
	}

	public static String getShowName(String newName, String flowNum) {
		String showName = newName;
		if (JecnContants.processShowNumber) {
			if (StringUtils.isNotBlank(flowNum)) {
				showName = flowNum.trim() + " " + newName;
			}
		}
		return showName;
	}

	/**
	 * 根据mark从配置项中取值如果配置项的值为1返回true 否则返回false
	 * 
	 * @param mark
	 *            配置项的mark值
	 * @return
	 */
	public static boolean ifValueIsOneReturnTrue(String mark) {
		if (mark == null) {
			return false;
		}
		if ("1".equals(JecnContants.getValue(mark).trim())) {
			return true;
		}
		return false;
	}

	/**
	 * 根据输入时间拼接显示的数据模版
	 * 
	 * @param tmpListFlowKpi
	 *            数据库原始数据
	 * @param kpiType
	 * @param StringStartDate
	 * @param StringEndDate
	 * @return
	 */
	public static List<JecnFlowKpi> kpiModelAddCount(List<JecnFlowKpi> tmpListFlowKpi, String kpiType,
			String StringStartDate, String StringEndDate, TempSearchKpiBean searchKpiBean) {
		/** *** 输出前台的json数据结果 ****** */
		List<JecnFlowKpi> listFlowKpi = new ArrayList<JecnFlowKpi>();
		Calendar calendarbeg = getCalendar(StringStartDate, kpiType);
		Calendar calendarEnd = getCalendar(StringEndDate, kpiType);

		int num = 1;
		if ("1".equals(kpiType)) {// 季度*******
			// KPi按照季度查询*yyyy03、yyyy06、yyyy09、yyyy12
			num = 3;
			while (true) {
				JecnFlowKpi jecnFlowKpi = new JecnFlowKpi();
				jecnFlowKpi.setKpiHorVlaue(calendarbeg.getTime());
				// 流程ID
				jecnFlowKpi.setFlowId(searchKpiBean.getFlowId());
				// KPI ANdID
				jecnFlowKpi.setKpiAndId(searchKpiBean.getKpiId());
				String tempTime = JecnCommon.getStringbyDate(calendarbeg.getTime(), "yyyy-MM");
				for (int j = 0; j < tmpListFlowKpi.size(); j++) {
					JecnFlowKpi jecnFlowKpiT = tmpListFlowKpi.get(j);
					String oldTime = JecnCommon.getStringbyDate(jecnFlowKpiT.getKpiHorVlaue(), "yyyy-MM");
					if (tempTime.equals(oldTime)) {// 数据库存在值
						jecnFlowKpi.setKpiValue(jecnFlowKpiT.getKpiValue());
						jecnFlowKpi.setKpiId(jecnFlowKpiT.getKpiId());
						break;
					}
				}
				calendarbeg.add(Calendar.MONTH, num);
				if (calendarbeg.compareTo(calendarEnd) > 0) {
					break;
				}
				listFlowKpi.add(jecnFlowKpi);
			}
		} else if ("0".equals(kpiType)) {// 月份******* KPi按照月份查询*
			// 月份yyyy01、yyyy02...yyyy12
			num = 1;
			while (true) {
				if (calendarbeg.compareTo(calendarEnd) > 0) {
					break;
				}
				JecnFlowKpi jecnFlowKpi = new JecnFlowKpi();
				jecnFlowKpi.setKpiHorVlaue(calendarbeg.getTime());
				// 流程ID
				jecnFlowKpi.setFlowId(searchKpiBean.getFlowId());
				// KPI ANdID
				jecnFlowKpi.setKpiAndId(searchKpiBean.getKpiId());
				String tempTime = JecnCommon.getStringbyDate(calendarbeg.getTime(), "yyyy-MM");
				for (int j = 0; j < tmpListFlowKpi.size(); j++) {
					JecnFlowKpi jecnFlowKpiT = tmpListFlowKpi.get(j);
					String oldTime = JecnCommon.getStringbyDate(jecnFlowKpiT.getKpiHorVlaue(), "yyyy-MM");
					if (tempTime.equals(oldTime)) {// 数据库存在值
						jecnFlowKpi.setKpiValue(jecnFlowKpiT.getKpiValue());
						jecnFlowKpi.setKpiId(jecnFlowKpiT.getKpiId());
						break;
					}
				}
				calendarbeg.add(Calendar.MONTH, num);

				listFlowKpi.add(jecnFlowKpi);
			}
		}
		return listFlowKpi;
	}

	/**
	 * 根据输入的日期返回一个日历对象
	 * 
	 * @param dateStr
	 *            String yyyy-MM-dd
	 * @return
	 */
	private static Calendar getCalendar(String dateStr, String kpiType) {
		String[] dateArray = dateStr.split("-");

		int year = Integer.valueOf(dateArray[0]).intValue();
		int month = Integer.valueOf(dateArray[1]).intValue();
		int day = 1;
		Calendar c = Calendar.getInstance();
		// 0为月 1为季度
		if ("1".equals(kpiType)) {
			month = getMonth(month);
		}
		c.set(year, month - 1, day);
		return c;
	}

	/**
	 * 根据输入的日期返回一个日历对象(忽略天)
	 * 
	 * @param dateStr
	 *            String yyyy-MM-dd
	 * @return
	 */
	private static Calendar getCalendar(String dateStr) {
		String[] dateArray = dateStr.split("-");

		int year = Integer.valueOf(dateArray[0]).intValue();
		int month = Integer.valueOf(dateArray[1]).intValue();
		int day = 1;
		Calendar c = Calendar.getInstance();
		c.set(year, month - 1, day);
		return c;
	}

	private static int getMonth(int number) {
		int month = 3;
		if (1 <= number && number < 4) {
			month = 3;
		} else if (4 <= number && number < 7) {
			month = 6;
		} else if (7 <= number && number < 10) {
			month = 9;
		} else if (10 <= number) {
			month = 12;
		}
		return month;
	}

	public static String objToStr(Object obj) {
		if (obj == null) {
			return "";
		}
		return obj.toString();
	}

	public static String objToStrWithNull(Object obj) {
		if (obj == null) {
			return null;
		}
		return obj.toString();
	}

	/**
	 * 拼接viewSort
	 * 
	 * @param pViewSort
	 *            父节点的viewSort
	 * @param sortId
	 *            当前节点的sortId
	 * @return
	 */
	public static String concatViewSort(String pViewSort, int sortId) {
		if (sortId < 0) {
			sortId = 0;
		}
		if (StringUtils.isBlank(pViewSort)) {
			return completeFourDigit(sortId);
		}
		return pViewSort + completeFourDigit(sortId);
	}

	public static String completeFourDigit(int numSortId) {
		String sortId = String.valueOf(numSortId);
		if (sortId.length() == 0) {
			return "0000";
		} else if (sortId.length() == 1) {
			return "000" + sortId;
		} else if (sortId.length() == 2) {
			return "00" + sortId;
		} else if (sortId.length() == 3) {
			return "0" + sortId;
		} else if (sortId.length() == 4) {
			return sortId;
		}
		return "0000";
	}

	/**
	 * 将target的值重置为source
	 * 
	 * @param source
	 * @param target
	 */
	public static void calendarReset(Calendar source, Calendar target) {
		target.set(source.get(Calendar.YEAR), source.get(Calendar.MONTH), source.get(Calendar.DAY_OF_MONTH), source
				.get(Calendar.HOUR_OF_DAY), source.get(Calendar.MINUTE), source.get(Calendar.SECOND));
		target.set(Calendar.MILLISECOND, source.get(Calendar.MILLISECOND));
	}

	/**
	 * 获得和传入日期间隔的日期
	 * 
	 * @param date
	 * @param intervalDay
	 *            单位是日，如果是正数返回值比传入日期大，如果是负数返回值比传入日期小
	 * @return
	 */
	public static Date getDateByInterval(Date date, int intervalDay) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, intervalDay);
		return cal.getTime();
	}

	public static String DateToStr(Date date, SimpleDateFormat format) {
		return format.format(date);
	}

	/**
	 * object 转化成 JecnTreeBean
	 * 
	 * @param obj
	 * @return
	 */
	public static List<JecnTreeBean> getJecnTreeBeanByObjectRisk(List<Object[]> objs) {
		List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();
		JecnTreeBean treeBean = null;
		for (Object[] objects : objs) {
			treeBean = new JecnTreeBean();
			treeBean.setId(Long.valueOf(objects[0].toString()));
			treeBean.setName(objects[1].toString());
			if (objects.length > 2) {
				treeBean.setNumberId(nullToEmpty(objects[2]));
			}
			treeBeans.add(treeBean);
		}
		return treeBeans;
	}

	/**
	 * object 转化成 JecnTreeBean
	 * 
	 * @param obj
	 * @return
	 */
	public static JecnTreeBean getJecnTreeBeanByObjectStandard(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		// 标准节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 标准名称
		if (obj[1] != null) {
			jecnTreeBean.setName(obj[1].toString());
		} else {
			jecnTreeBean.setName("");
		}
		// 标准节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 标准类型
		if (Integer.valueOf(obj[3].toString()) == 0) {
			// 标准目录
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardDir);
		} else if (Integer.valueOf(obj[3].toString()) == 1) {
			// 文件标准
			jecnTreeBean.setTreeNodeType(TreeNodeType.standard);
		} else if (Integer.valueOf(obj[3].toString()) == 2) {
			// 流程标准
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardProcess);
		} else if (Integer.valueOf(obj[3].toString()) == 3) {
			// 流程地图标准
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardProcessMap);
		} else if (Integer.valueOf(obj[3].toString()) == 4) {
			// 标准条款
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardClause);
		} else if (Integer.valueOf(obj[3].toString()) == 5) {
			// 条款要求
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardClauseRequire);
		}
		if (obj[4] != null) {
			jecnTreeBean.setRelationId(Long.valueOf(obj[4].toString()));
		}
		// 标准是否有子节点
		if (Integer.parseInt(obj[5].toString()) > 0) {
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}
		return jecnTreeBean;
	}

	public static Integer getAccessTypeByTaskType(int taskType) {
		// 查阅权限类型 0是流程1是文件2是标准3是制度
		Integer accessType = null;
		switch (taskType) {
		case 0:
		case 4:
			accessType = 0;
			break;
		case 1:
			accessType = 1;
			break;
		case 2:
		case 3:
			accessType = 3;
			break;
		default:
			throw new IllegalArgumentException("getAccessTypeByFileType不支持的文件类型");
		}
		return accessType;
	}

	public static Integer getAccessTypeByFileType(int fileType) {
		// 查阅权限类型 0是流程1是文件2是标准3是制度
		Integer accessType = null;
		switch (fileType) {
		case 0:
			accessType = 0;
			break;
		case 1:
			accessType = 1;
			break;
		case 2:
			accessType = 3;
			break;
		default:
			throw new IllegalArgumentException("getAccessTypeByFileType不支持的文件类型");
		}
		return accessType;
	}

	/**
	 * 
	 * 获取岗位查阅权限和部门和刚位组查阅权限对应的人员ID集合
	 * 
	 * @return Set<Long> 查阅权限对应的人员ID集合
	 */
	@SuppressWarnings("unchecked")
	public static Set<Long> getPermPeoples(IBaseDao baseDao, Long relatedId, int permType) {
		Set<Long> accessPeopleIds = new HashSet<Long>();
		// 获取岗位查阅权限对应的人员
		accessPeopleIds.addAll(baseDao.listObjectNativeSql(JecnSqlConstant.POS_ACCESS_PERMISS, "people_id",
				Hibernate.LONG, permType, relatedId));
		accessPeopleIds.addAll(baseDao.listObjectNativeSql(JecnSqlConstant.ORG_ACCESS_PERMISS, "people_id",
				Hibernate.LONG, permType, relatedId));
		accessPeopleIds.addAll(baseDao.listObjectNativeSql(JecnSqlConstant.GROUP_ACCESS_PERMISS, "people_id",
				Hibernate.LONG, permType, relatedId));
		return accessPeopleIds;
	}

	public static String concat(Collection<? extends Object> cols, CharSequence delimiter) {
		if (cols == null || cols.isEmpty()) {
			return "";
		}
		StringBuilder buf = new StringBuilder();
		int size = cols.size();
		int index = 0;
		for (Object o : cols) {
			index++;
			if (o == null) {
				continue;
			}
			buf.append(o);
			if (index != size) {
				buf.append(delimiter);
			}
		}
		return buf.toString();
	}

	public static String nullToEmpty(Object text) {
		if (text == null) {
			return "";
		}
		return text.toString().trim();
	}

	public static boolean isNotEmpty(Collection<?> col) {
		return col != null && !col.isEmpty();
	}

	public static boolean isEmpty(Collection<?> col) {
		return !isNotEmpty(col);
	}

	public static String getKpiFrom(int index) {
		switch (index) {
		case 0:
			return "二级指标";
		case 1:
			return "个人PBC指标";
		case 2:
			return "其他重要指标";
		default:
			break;
		}
		return "";
	}

	public static String objToNumberStr(Object obj) {
		if (obj == null) {
			return null;
		}
		if (obj instanceof BigDecimal) {
			BigDecimal big = (BigDecimal) obj;
			double v = big.doubleValue();
			if (v % 1 == 0) {
				return String.valueOf((long) v);
			} else {
				return String.valueOf(v);
			}
		}
		return obj.toString();
	}

	public static List<JecnTreeBean> fillPosOrgPath(List<JecnTreeBean> listResult, IBaseDao baseDao) {
		if (listResult == null || !JecnConfigTool.isShowPosFullPath()) {
			return listResult;
		}
		Set<Long> ids = new HashSet<Long>();
		for (JecnTreeBean bean : listResult) {
			if (bean.getPid() == null) {
				continue;
			}
			ids.add(bean.getPid());
		}
		if (ids.size() == 0) {
			return listResult;
		}
		String sql = JecnCommonSql.getParentObjectsSqlOrg(ids);
		List<Object[]> orgs = baseDao.listNativeSql(sql);
		Map<Long, Object[]> idNameMap = new HashMap<Long, Object[]>();
		for (Object[] obj : orgs) {
			if (obj == null || obj[0] == null || obj[1] == null || "".equals(obj[1].toString()) || obj[2] == null) {
				continue;
			}
			idNameMap.put(Long.valueOf(obj[0].toString()), obj);
		}
		for (JecnTreeBean bean : listResult) {
			bean.setPname(fillOrgPath(idNameMap, bean.getPid()));
		}
		return listResult;
	}

	private static String fillOrgPath(Map<Long, Object[]> idNameMap, Long startId) {
		if (startId == 0 || idNameMap.get(startId) == null) {
			return "";
		}
		List<String> names = new ArrayList<String>();
		while (startId != null && startId.intValue() != 0) {
			Object[] obj = idNameMap.get(startId);
			names.add(obj[1].toString());
			startId = Long.valueOf(obj[2].toString());
		}
		StringBuffer buf = new StringBuffer();
		for (int i = names.size() - 1; i >= 0; i--) {
			if (i == 0) {
				buf.append(names.get(i));
			} else {
				buf.append(names.get(i));
				buf.append("/");
			}
		}
		return buf.toString();
	}

	/**
	 * 返回MD5 32为字符串
	 * 
	 * @param sourceStr
	 * @return
	 */
	public static String MD5(byte[] bytes) {
		String result = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(bytes);
			byte b[] = md.digest();
			int i;
			StringBuilder builder = new StringBuilder();
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					builder.append("0");
				builder.append(Integer.toHexString(i));
			}
			result = builder.toString();
			return result;
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
		}
		return result;
	}

	public static String concatIds(Collection<? extends Object> cols) {
		if (cols == null || cols.isEmpty()) {
			throw new IllegalArgumentException("集合为空concatIds");
		}
		return "(" + concat(cols, ",") + ")";
	}

	public static <T> void swapColumn(final List<T[]> rowData, int i, int j) {
		if (rowData == null || rowData.size() == 0) {
			return;
		}
		T temp = null;
		for (T[] row : rowData) {
			temp = row[i];
			row[i] = row[j];
			row[j] = temp;
		}
	}

	/**
	 * 从map中获得一个key对应的value，value类型为List,如果存在则返回，如果不存在则创建一个List 并添加到map中，并返回
	 * 
	 * @param <T>
	 * @param m
	 * @param key
	 * @return
	 */
	public static <K, V> List<V> getElseAddList(Map<K, List<V>> m, K key) {
		if (m.get(key) == null) {
			m.put(key, new ArrayList<V>());
		}
		return m.get(key);
	}

	public static boolean lenThan(String name, int chLen) {
		return lenThanEn(name, chLen * 2);
	}

	public static boolean lenThanEn(String name, int enLen) {
		return getTextLength(name) > enLen;
	}

	public static boolean collectIsEmpty(Collection<?> c) {
		if (c == null || c.size() == 0) {
			return true;
		}
		return false;
	}

	public static boolean collectIsNotEmpty(Collection<?> c) {
		return !collectIsEmpty(c);
	}

	public static <T> T getOrElse(T obj1, T obj2) {
		if (obj1 == null) {
			return obj2;
		}
		return null;
	}

	public static List<JecnConfigItemBean> alterName(List<JecnConfigItemBean> configs) {
		if (configs == null) {
			return configs;
		}
		try {
			Map<String, String> markTodic = JecnUtil.markToDic;
			Set<String> keys = markTodic.keySet();
			for (JecnConfigItemBean config : configs) {
				if (keys.contains(config.getMark())) {
					List<JecnDictionary> dics = JecnConfigTool.getDictionaryList(DictionaryEnum.valueOf(markTodic
							.get(config.getMark())));
					if (!JecnUtil.isEmpty(dics)) {
						JecnDictionary dictionary = dics.get(0);
						if (StringUtils.isNotBlank(dictionary.getTransName())) {
							config.setName(dictionary.getTransName());
							config.setDefaultName(dictionary.getTransName());
						}
						if (StringUtils.isNotBlank(dictionary.getEnTransName())) {
							config.setEnName(dictionary.getEnTransName());
							config.setEnDefaultName(dictionary.getEnTransName());
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return configs;
	}

	public static String trimRemain(Object text, int remainLen) {
		String s = JecnUtil.nullToEmpty(text);
		if (s.length() >= remainLen) {
			return s.substring(0, remainLen);
		}
		return s;
	}

	/**
	 * 获得配置项的名称
	 * 
	 * @param mark
	 * @return
	 */
	public static String getName(String mark) {
		return JecnContants.LANGUAGES.get(mark).getName();
	}

	public static Set<TreeNodeType> getNoViewSortNodeType() {
		Set<TreeNodeType> escape = new HashSet<TreeNodeType>();
		escape.add(TreeNodeType.position);
		escape.add(TreeNodeType.ruleModeDir);
		escape.add(TreeNodeType.ruleModeRoot);
		return escape;
	}

	public static boolean noViewSortNodeType(TreeNodeType treeNodeType) {
		return getNoViewSortNodeType().contains(treeNodeType);
	}

	public static String ifNotEndWithAppend(String str, String endWith) {
		if (StringUtils.isBlank(str)) {
			return endWith;
		}
		return str.endsWith(endWith) ? str : str + endWith;
	}
	public static String ifNotEndWithAppendOrBlank(String str, String endWith) {
		if (StringUtils.isBlank(str)) {
			return "";
		}
		return str.endsWith(endWith) ? str : str + endWith;
	}
	
	public static String getJson(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
            log.error("转换 JSON 异常", e);
            return "";
        }
    }

	public static String decodeUrl(String url) throws Exception {
		String result = URLDecoder.decode(url, "UTF-8");
		return result;
	}
	
	public static String encodeUrl(String url) throws Exception{
		return URLEncoder.encode(url,"UTF-8");
	}
}
