package com.jecn.epros.server.dao.process;

import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.common.IBaseDao;

public interface IFlowDriverT extends IBaseDao<JecnFlowDriverT, Long> {

}
