package epros.draw.gui.operationConfig;

import java.io.Serializable;

public class DrawOperationDescriptionConfigBean implements Serializable {
	private Long id;
	private Integer turn;// 排序
	private String mark;// 标识
	private String name;// 名称
	private Integer isShow;// 是否显示0不显示1显示
	private Integer isDefault;// 是否为默认项0非默认1默认
	private Integer defaultSort;// 默认排序
	private String defaultName; // 默认名称
	private int isEmpty = -1; // 是否必填 1:必填 0:不必填
	private String v1;
	private String v2;

	/**
	 * 
	 * 判断是否是活动说明项
	 * 
	 * @return boolean true：是活动说明；false：不是活动说明
	 */
	public boolean isActiveInfo() {
		// 活动说明
		return ("a10".equals(mark)) ? true : false;
	}

	public int getIsEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(int isEmpty) {
		this.isEmpty = isEmpty;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTurn() {
		return turn;
	}

	public void setTurn(Integer turn) {
		this.turn = turn;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getDefaultSort() {
		return defaultSort;
	}

	public void setDefaultSort(Integer defaultSort) {
		this.defaultSort = defaultSort;
	}

	public String getV1() {
		return v1;
	}

	public void setV1(String v1) {
		this.v1 = v1;
	}

	public String getV2() {
		return v2;
	}

	public void setV2(String v2) {
		this.v2 = v2;
	}
	

}
