package epros.designer.gui.rule.table;

import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.rule.RuleFileT;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

public class RuleFileTable extends JTable {
	private List<RuleFileT> listFileBean = null;
	// 标题id
	Long titleId = null;

	public List<RuleFileT> getListFileBean() {
		return listFileBean;
	}

	public void setListFileBean(List<RuleFileT> listFileBean) {
		this.listFileBean = listFileBean;
	}

	public RuleFileTable(List<RuleFileT> listFileBean, Long titleId) {
		this.listFileBean = listFileBean;
		this.titleId = titleId;

		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	public RuleFileTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("fileNum"));
		title.add(JecnProperties.getValue("fileName"));
		title.add(JecnProperties.getValue("ruleTitleID"));
		return new RuleFileTableMode(updateTableContent(listFileBean), title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0, 3 };
	}

	class RuleFileTableMode extends DefaultTableModel {
		public RuleFileTableMode(Vector<Vector<Object>> data,
				Vector<String> title) {
			super(data, title);

		}

//		@Override
//		public Class getColumnClass(int columnIndex) {
//			return getValueAt(0, columnIndex).getClass();
//		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public Vector<Vector<Object>> updateTableContent(List<RuleFileT> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				RuleFileT ruleFileT = list.get(i);
				Vector<Object> data = new Vector<Object>();
				if (ruleFileT != null) {
					data.add(ruleFileT.getRuleFileId());
					// 文件编号
					data.add(ruleFileT.getFileInput());
					// 文件名称
					data.add(ruleFileT.getFileName());
					// // 制度操作说明标题ID
					data.add(ruleFileT.getRuleTitleId());
					vector.add(data);
				}
			}
		}
		return vector;
	}
}
