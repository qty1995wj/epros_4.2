package epros.draw.gui.figure.unit;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.JecnTempManLine;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.zoom.JecnWorkflowZoomProcess;

public class JecnDraggTmpLine {

	public static void otherCasesToDrawLine(JecnTempManLine tempLine) {
		// 新的ManhattanLine线 100%状态下的开始、结束点
		Point newStartPt = tempLine.getOriginalStartPoint();
		Point newEndPt = tempLine.getOriginalEndPoint();

		// 原来的ManhattanLine线开始、结束点
		Point oldStartPt = tempLine.getLineSegments().get(0).getStartPoint();
		Point oldEndPt = tempLine.getLineSegments().get(tempLine.getLineSegments().size() - 1).getEndPoint();

		// 开始点和结束点都未变化
		if (newStartPt.equals(oldStartPt) && newEndPt.equals(oldEndPt)) {
			return;
		} // 初始化小线段备份数据集
		List<LineSegment> lineSegmentsClone = tempLine.getLineSegmentsClone(tempLine);

		if (lineSegmentsClone.size() == 0 && tempLine.curLineSegmentsClone.size() == 0) {
			tempLine.paintLineByRouter();
		}
		drawAllLine(newStartPt, newEndPt, lineSegmentsClone);
		// 是否执行放大缩小算法
		if (JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale() != 1.0) {
			new JecnWorkflowZoomProcess(JecnDrawMainPanel.getMainPanel().getWorkflow()).zoomManhattanLine(tempLine);
		}
	}

	private static void drawAllLine(Point newStartPt, Point newEndPt, List<LineSegment> lineSegmentsClone) {
		if (lineSegmentsClone == null || lineSegmentsClone.size() < 1) {
			return;
		}
		// 记录线段端点集合
		List<Point> listTempPoint = new ArrayList<Point>();
		Point prePoint = new Point(newStartPt.x, newStartPt.y);
		Point newPoint = null;
		// 记录开始点
		listTempPoint.add(newStartPt);
		int lineLength = 0;// 线的长度

		for (int i = 0; i < lineSegmentsClone.size() - 1; i++) {
			LineSegment theLineSegment = lineSegmentsClone.get(i);
			switch (theLineSegment.getLineSegmentData().getLineDirectionEnum()) {
			case horizontal:
				lineLength = theLineSegment.getEndPoint().x - theLineSegment.getStartPoint().x;
				newPoint = new Point(prePoint.x + lineLength, prePoint.y);
				if (i == lineSegmentsClone.size() - 2) {// 尾线
					if (newPoint.x != newEndPt.x) {
						newPoint.x = newEndPt.x;
					}
				}
				break;
			case vertical:
				lineLength = theLineSegment.getEndPoint().y - theLineSegment.getStartPoint().y;
				newPoint = new Point(prePoint.x, prePoint.y + lineLength);
				if (i == lineSegmentsClone.size() - 2) {// 尾线
					if (newPoint.y != newEndPt.y) {
						newPoint.y = newEndPt.y;
					}
				}
				break;
			}
			listTempPoint.add(newPoint);
			// 更新端点集合
			prePoint.x = newPoint.x;
			prePoint.y = newPoint.y;
		}
		// 清空临时端点
		prePoint = null;
		listTempPoint.add(newEndPt);
		// 画线
		paintLineByListPoint(listTempPoint, null, lineSegmentsClone.get(0).getBaseManhattanLine());
		// 清空备份的小线段集合
		lineSegmentsClone = null;
	}

	public static void paintLineByListPoint(List<Point> listTempPoint, List<LineSegment> lineSegmentsClone,
			JecnBaseManhattanLinePanel manhattanLinePanel) {
		// 获取当前线段下小线段集合
		List<JecnManhattanRouterResultData> routerResultList = manhattanLinePanel.getRouterResultData(listTempPoint);

		if (routerResultList == null) {
			return;
		}
		if (lineSegmentsClone != null && lineSegmentsClone.size() > 0) {
			for (LineSegment lineSegment : lineSegmentsClone) {
				JecnManhattanRouterResultData data = new JecnManhattanRouterResultData();
				data.setStartPoint(lineSegment.getStartPoint());
				data.setEndPint(lineSegment.getEndPoint());
				routerResultList.add(data);
			}
		}
		// 排序
		routerResultList = manhattanLinePanel.reOrderLinePoint(routerResultList);
		// 添加连接线
		manhattanLinePanel.drawLineSegmentByListData(routerResultList);

		// 刷新面板
		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}
}
