package com.jecn.epros.server.dao.rule.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.sql.BLOB;

import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.lob.SerializableBlob;

import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.JecnFlowCommon;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleContent;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleFile;
import com.jecn.epros.server.bean.rule.RuleFileT;
import com.jecn.epros.server.bean.rule.RuleHistory;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.rule.RuleHistoryDao;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebLoginBean;

public class RuleHistoryDaoImpl extends AbsBaseDao<RuleHistory, Long> implements RuleHistoryDao {

}
