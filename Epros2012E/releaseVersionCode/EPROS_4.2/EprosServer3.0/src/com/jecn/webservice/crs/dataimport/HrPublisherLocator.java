/**
 * HrPublisherLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public class HrPublisherLocator extends org.apache.axis.client.Service implements com.jecn.webservice.crs.dataimport.HrPublisher {

	public HrPublisherLocator() {
	}

	public HrPublisherLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public HrPublisherLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
			throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for HrPublisherHttpSoap11Endpoint
	private java.lang.String HrPublisherHttpSoap11Endpoint_address = "http://10.96.11.207:80/msm/service/HrPublisher.HrPublisherHttpSoap11Endpoint";

	public java.lang.String getHrPublisherHttpSoap11EndpointAddress() {
		return HrPublisherHttpSoap11Endpoint_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String HrPublisherHttpSoap11EndpointWSDDServiceName = "HrPublisherHttpSoap11Endpoint";

	public java.lang.String getHrPublisherHttpSoap11EndpointWSDDServiceName() {
		return HrPublisherHttpSoap11EndpointWSDDServiceName;
	}

	public void setHrPublisherHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
		HrPublisherHttpSoap11EndpointWSDDServiceName = name;
	}

	public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap11Endpoint()
			throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(HrPublisherHttpSoap11Endpoint_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getHrPublisherHttpSoap11Endpoint(endpoint);
	}

	public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap11Endpoint(java.net.URL portAddress)
			throws javax.xml.rpc.ServiceException {
		try {
			com.jecn.webservice.crs.dataimport.HrPublisherSoap11BindingStub _stub = new com.jecn.webservice.crs.dataimport.HrPublisherSoap11BindingStub(
					portAddress, this);
			_stub.setPortName(getHrPublisherHttpSoap11EndpointWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	public void setHrPublisherHttpSoap11EndpointEndpointAddress(java.lang.String address) {
		HrPublisherHttpSoap11Endpoint_address = address;
	}

	// Use to get a proxy class for HrPublisherHttpSoap12Endpoint
	private java.lang.String HrPublisherHttpSoap12Endpoint_address = "http://10.96.11.207:80/msm/service/HrPublisher.HrPublisherHttpSoap12Endpoint";

	public java.lang.String getHrPublisherHttpSoap12EndpointAddress() {
		return HrPublisherHttpSoap12Endpoint_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String HrPublisherHttpSoap12EndpointWSDDServiceName = "HrPublisherHttpSoap12Endpoint";

	public java.lang.String getHrPublisherHttpSoap12EndpointWSDDServiceName() {
		return HrPublisherHttpSoap12EndpointWSDDServiceName;
	}

	public void setHrPublisherHttpSoap12EndpointWSDDServiceName(java.lang.String name) {
		HrPublisherHttpSoap12EndpointWSDDServiceName = name;
	}

	public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap12Endpoint()
			throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(HrPublisherHttpSoap12Endpoint_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getHrPublisherHttpSoap12Endpoint(endpoint);
	}

	public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap12Endpoint(java.net.URL portAddress)
			throws javax.xml.rpc.ServiceException {
		try {
			com.jecn.webservice.crs.dataimport.HrPublisherSoap12BindingStub _stub = new com.jecn.webservice.crs.dataimport.HrPublisherSoap12BindingStub(
					portAddress, this);
			_stub.setPortName(getHrPublisherHttpSoap12EndpointWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	public void setHrPublisherHttpSoap12EndpointEndpointAddress(java.lang.String address) {
		HrPublisherHttpSoap12Endpoint_address = address;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown. This
	 * service has multiple ports for a given interface; the proxy
	 * implementation returned may be indeterminate.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (com.jecn.webservice.crs.dataimport.HrPublisherPortType.class.isAssignableFrom(serviceEndpointInterface)) {
				com.jecn.webservice.crs.dataimport.HrPublisherSoap11BindingStub _stub = new com.jecn.webservice.crs.dataimport.HrPublisherSoap11BindingStub(
						new java.net.URL(HrPublisherHttpSoap11Endpoint_address), this);
				_stub.setPortName(getHrPublisherHttpSoap11EndpointWSDDServiceName());
				return _stub;
			}
			if (com.jecn.webservice.crs.dataimport.HrPublisherPortType.class.isAssignableFrom(serviceEndpointInterface)) {
				com.jecn.webservice.crs.dataimport.HrPublisherSoap12BindingStub _stub = new com.jecn.webservice.crs.dataimport.HrPublisherSoap12BindingStub(
						new java.net.URL(HrPublisherHttpSoap12Endpoint_address), this);
				_stub.setPortName(getHrPublisherHttpSoap12EndpointWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
				+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("HrPublisherHttpSoap11Endpoint".equals(inputPortName)) {
			return getHrPublisherHttpSoap11Endpoint();
		} else if ("HrPublisherHttpSoap12Endpoint".equals(inputPortName)) {
			return getHrPublisherHttpSoap12Endpoint();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://hr.publisher.mdm", "HrPublisher");
	}

	private java.util.HashSet ports = null;

	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://hr.publisher.mdm", "HrPublisherHttpSoap11Endpoint"));
			ports.add(new javax.xml.namespace.QName("http://hr.publisher.mdm", "HrPublisherHttpSoap12Endpoint"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {

		if ("HrPublisherHttpSoap11Endpoint".equals(portName)) {
			setHrPublisherHttpSoap11EndpointEndpointAddress(address);
		} else if ("HrPublisherHttpSoap12Endpoint".equals(portName)) {
			setHrPublisherHttpSoap12EndpointEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
