package epros.designer.gui.process.flow.file;

import java.util.List;

import javax.swing.JTree;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileContent;

import epros.designer.gui.file.FileVersionPanel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/***
 * 流程文件版本信息
 * 
 * @author 2012-05-30
 * 
 */
public class FlowFileVersionPanel extends FileVersionPanel {

	public FlowFileVersionPanel(JTree jTree, JecnTreeNode selectNode) {
		super(jTree, selectNode);
		rebackBut.setVisible(false);
		delBut.setVisible(false);
	}

	protected List<JecnFileContent> getVersions(Long id) throws Exception {
		return ConnectionPool.getProcessAction().getJecnFlowFileContents(id);
	}

	/***
	 * 删除
	 */
	protected void delButPerformed() {
		// 所有的版本都显示出来了，所以不能删除
	}

	/***
	 * 恢复
	 */
	protected void rebackButPerformed() {

	}

	/***
	 * 查看,双击表格查看版本信息
	 */
	@Override
	protected void searchButPerformed() {
		int[] selectRows = fileVersionTable.getSelectedRows();
		if (selectRows.length != 1) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		try {
			FileOpenBean openBean = ConnectionPool.getProcessAction().openFileContent(
					Long.valueOf(fileVersionTable.getModel().getValueAt(selectRows[0], 0).toString()));
			if (openBean.getFileByte() == null) {
				JecnOptionPane.showMessageDialog(this, "文件已丢失");
			} else {
				JecnUtil.openFile(openBean.getName(), openBean.getFileByte());
			}
		} catch (Exception e) {
			log.error("查看流程文件版本信息异常！", e);
		}

	}
}
