package com.jecn.epros.server.dao.dataImport.match.impl;

import java.util.List;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.dataImport.match.ISearchTableMatchDao;

public class SearchTableMatchDaoImpl extends AbsBaseDao<String, String>
		implements ISearchTableMatchDao {
	public List<Object[]> getSqlResut(String sql, String arg) throws Exception {
		return listNativeSql(sql, arg);
	}
}
