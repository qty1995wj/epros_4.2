package com.jecn.epros.bean;

/***
 * 部门审批,文控审批
 * 
 * @author user
 * 
 */
public class TaskChangeInfoApproveParam extends BaseTaskParam {

	/** 密级 0秘密，1 公开 */
	private String strPublic;
	/** 查阅权限组织ID集合 ','分隔 */
	private String orgIds;
	/** 查阅权限岗位ID集合 ','分隔 */
	private String posIds;
	/** 查阅权限相关部门名称‘/’分离 */
	private String orgNames;
	/** 查阅权限相关岗位名称‘/’分离 */
	private String posNames;
	/** 查阅岗位组ID集合 ','分隔 */
	private String groupIds;
	/** 查阅岗位组名称集合 ','分隔 */
	private String groupNames;
	/** PRF当前类别 */
	private String prfType;
	/** PRF当前类别 */
	private String prfTypeName;

	public String getStrPublic() {
		return strPublic;
	}

	public void setStrPublic(String strPublic) {
		this.strPublic = strPublic;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public String getPosIds() {
		return posIds;
	}

	public void setPosIds(String posIds) {
		this.posIds = posIds;
	}

	public String getOrgNames() {
		return orgNames;
	}

	public void setOrgNames(String orgNames) {
		this.orgNames = orgNames;
	}

	public String getPosNames() {
		return posNames;
	}

	public void setPosNames(String posNames) {
		this.posNames = posNames;
	}

	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	public String getGroupNames() {
		return groupNames;
	}

	public void setGroupNames(String groupNames) {
		this.groupNames = groupNames;
	}

	public String getPrfType() {
		return prfType;
	}

	public void setPrfType(String prfType) {
		this.prfType = prfType;
	}

	public String getPrfTypeName() {
		return prfTypeName;
	}

	public void setPrfTypeName(String prfTypeName) {
		this.prfTypeName = prfTypeName;
	}

}
