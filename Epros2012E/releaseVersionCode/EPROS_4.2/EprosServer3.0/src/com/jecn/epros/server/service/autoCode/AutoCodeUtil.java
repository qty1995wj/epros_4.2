package com.jecn.epros.server.service.autoCode;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;

public class AutoCodeUtil {
	/** 右斜杠 */
	private static final String BACK_SLANT = "\\";

	/**
	 * 蒙牛自动编号生成规则
	 * 
	 * @param codeBean
	 * @return
	 */
	public static String getMengNiuAutoNum(AutoCodeBean codeBean) {
		StringBuilder builder = new StringBuilder();
		// 类别代码\单位代码-模块代码-序号-年份-版本
		String typeCode = codeBean.getCompanyCode();
		String unitCode = codeBean.getProductionLine();
		String moduleCode = codeBean.getFileType();
		String yearCode = codeBean.getSmallClass();
		builder.append(typeCode);
		String SEPARATOR_H = "-";

		// 单位代码
		if (StringUtils.isNotBlank(unitCode) && !"-1".equals(moduleCode)) {
			builder.append(BACK_SLANT).append(unitCode);
		} else {
			builder.append(SEPARATOR_H);
		}
		// 模块代码
		if (StringUtils.isBlank(unitCode) && StringUtils.isNotBlank(moduleCode) && !"-1".equals(moduleCode)) {
			builder.append(moduleCode);
		} else if (StringUtils.isNotBlank(moduleCode) && !"-1".equals(moduleCode)) {
			builder.append(SEPARATOR_H).append(moduleCode);
		}
		// 序号
		builder.append(SEPARATOR_H).append(codeBean.getCodeTotal());
		// 年份
		if (StringUtils.isNotBlank(yearCode) && !"-1".equals(yearCode)) {
			builder.append(SEPARATOR_H).append(yearCode);
		}
		// 版本
		String versionCode = codeBean.getProcessLevel();
		if (StringUtils.isNotBlank(versionCode) && !"-1".equals(versionCode)) {
			builder.append(SEPARATOR_H).append(versionCode);
		}
		return builder.toString();
	}

	private final static String LiBang_SPLIT = "-";

	public static String getLibangAutoNum(AutoCodeBean codeBean, int relateType) {
		StringBuilder builder = new StringBuilder();
		if (relateType == 1 || relateType == 0) {
			getProcessAutoNum(codeBean, builder);
		} else {
			getOtherAutoNum(codeBean, builder);
		}
		String strNum = String.valueOf(codeBean.getCodeTotal());
		if (strNum.length() == 1) {
			builder.append(LiBang_SPLIT).append("00").append(strNum);
		} else if (strNum.length() == 2) {
			builder.append(LiBang_SPLIT).append("0").append(strNum);
		} else if (strNum.length() == 3) {
			builder.append(LiBang_SPLIT).append(strNum);
		}
		return builder.toString();
	}

	private static void getProcessAutoNum(AutoCodeBean codeBean, StringBuilder builder) {
		String companyCode = codeBean.getCompanyCode();
		String processCode = codeBean.getProcessCode();
		String processLevel = codeBean.getProcessLevel();
		builder.append(companyCode);
		if (!"-1".equals(processLevel)) {
			builder.append(LiBang_SPLIT).append(processLevel);
		}
		builder.append(LiBang_SPLIT).append(processCode);
	}

	private static void getOtherAutoNum(AutoCodeBean codeBean, StringBuilder builder) {
		String companyCode = codeBean.getCompanyCode();
		String processCode = codeBean.getProcessCode();
		String productionLine = codeBean.getProductionLine();
		String fileType = codeBean.getFileType();
		String smallClass = codeBean.getSmallClass();
		builder.append(companyCode).append(LiBang_SPLIT).append(processCode);
		if (!"-1".equals(productionLine)) {
			builder.append(LiBang_SPLIT).append(productionLine);
		}
		if (!"-1".equals(fileType)) {
			builder.append(LiBang_SPLIT).append(fileType);
		}
		if (!"-1".equals(smallClass)) {
			builder.append(LiBang_SPLIT).append(smallClass);
		}
	}

	/**
	 * 木林森自动编号
	 * 
	 * @param codeBean
	 * @param relateType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @return
	 */
	public static String getMulinsenAutoNum(AutoCodeBean codeBean, int relateType) {
		StringBuilder builder = new StringBuilder();
		String companyCode = codeBean.getCompanyCode();
		String codeL2 = codeBean.getProductionLine();
		String fileType = codeBean.getFileType();
		String codeL3 = codeBean.getSmallClass();
		String SEPARATOR_H = "-";

		if (relateType == 5 && StringUtils.isNotBlank(codeBean.getProcessCode())) {// 文件
			builder.append(codeBean.getProcessCode()).append(SEPARATOR_H).append(fileType);
		} else {
			if (StringUtils.isNotBlank(codeL2)) {
				builder.append(codeL2).append(SEPARATOR_H);
			}
			if (StringUtils.isNotBlank(codeL3)) {
				builder.append(codeL3).append(SEPARATOR_H);
			}
			builder.append(fileType);
		}
		builder.append(codeBean.getCodeTotal() < 10 ? "0" + codeBean.getCodeTotal() : codeBean.getCodeTotal());
		// 公司代码
		if (StringUtils.isNotBlank(companyCode) && !"-1".equals(companyCode)) {
			builder.append(SEPARATOR_H).append(companyCode);
		}
		return builder.toString();
	}
}
