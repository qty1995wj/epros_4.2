package epros.designer.gui.process.activitiesProperty;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.JecnTextFieldAndCalendarPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 信息化 面板
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-11-14 时间：上午11:35:17
 */
public class ActiveOnLineInfoPanel extends JecnPanel {
	/** 主面板 */
	private JecnPanel mainPanel;
	/*	*//** 线上: */
	/*
	 * private JCheckBox onLineBox;
	 */
	/** 表单 */
	protected JTable jecnTable = null;
	/** 表单滚动面板 */
	private JScrollPane activeScrollPane;

	/** 添加按钮 */
	private JButton addButton;
	/** 编辑按钮 */
	private JButton editButton;
	/** 删除按钮 */
	private JButton deleteBut;
	/** 底层button按钮 */
	private JecnPanel buttonPanel;

	/** 活动信息 */
	private JecnActiveData activeData;

	public ActiveOnLineInfoPanel(JecnActiveData activeData) {
		super();
		this.activeData = activeData;

		// 初始化
		initComs();
		initLayOut();
		initData();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.activeData = jecnActiveData;
		initData();
		removeAllTable();
		Vector<Vector<Object>> fileData = getContent();
		for (Vector<Object> v : fileData) {
			((DefaultTableModel) jecnTable.getModel()).addRow(v);
		}
	}

	private void removeAllTable() {
		for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(index);
		}
	}

	/**
	 * 初始化控件
	 * 
	 */
	private void initComs() {
		initTable();

		mainPanel = new JecnPanel();
		/*
		 * // 线上: onLineBox = new JCheckBox(JecnProperties.getValue("onLine"));
		 */
		// 按钮添加
		addButton = new JButton(JecnProperties.getValue("addBtn"));
		// 编辑按钮
		editButton = new JButton(JecnProperties.getValue("task_editBtn"));
		// 删除
		deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));
		buttonPanel = new JecnPanel();

		addButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		editButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		deleteBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		/* onLineBox.setBackground(JecnUIUtil.getDefaultBackgroundColor()); */
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		/**
		 * 添加按钮监听
		 */
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addButtonListener();
			}
		});

		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editButtonListener();
			}
		});
		/**
		 * 删除按钮监听
		 */
		deleteBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButListener();
			}
		});
		/*		*//**
		 * 线上监听
		 * 
		 */
		/*
		 * onLineBox.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) {
		 * onLineBoxLinstener(); } });
		 */
	}

	protected void addButtonListener() {
		ActiveOnLineJDialog activeOnLineJDialog = new ActiveOnLineJDialog(JecnProperties.getValue("addActiveLine"), -1,
				this);
		activeOnLineJDialog.setVisible(true);
	}

	protected void editButtonListener() {
		int selectRow = jecnTable.getSelectedRow();
		if (selectRow == -1) {// 没有数据
			return;
		}
		ActiveOnLineJDialog activeOnLineJDialog = new ActiveOnLineJDialog(JecnProperties.getValue("editActiveLine"),
				selectRow, this);
		activeOnLineJDialog.setVisible(true);
	}

	/**
	 * 布局
	 * 
	 */
	private void initLayOut() {
		Insets insets = new Insets(1, 1, 1, 1);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		// 线上
		/* mainPanel.add(onLineBox, c); */
		mainPanel.add(activeScrollPane, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		// 线上
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(addButton);
		buttonPanel.add(editButton);
		buttonPanel.add(deleteBut);

		// 添加主面板
		Insets insets1 = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets1,
				0, 0);
		this.add(mainPanel, c);
	}

	/**
	 * 数据初始化
	 * 
	 */
	private void initData() {
		// hiddenBut();
		/*
		 * if (activeData.getIsOnLine() == 1) {// 线上
		 * onLineBox.setSelected(true); }
		 */
		showBut();
		// // 初始化支持工具
		// initToolFile();
	}

	/**
	 * 初始化table
	 * 
	 */
	private void initTable() {
		jecnTable = new JTable(getTableModel());
		activeScrollPane = new JScrollPane();
		Dimension dimension = null;
		if (JecnDesignerCommon.isShowEleAttribute()) {
			dimension = new Dimension(550, 180);
		} else {
			dimension = new Dimension(550, 400);
		}
		activeScrollPane.setPreferredSize(dimension);
		activeScrollPane.setMaximumSize(dimension);
		activeScrollPane.setMinimumSize(dimension);
		activeScrollPane.setViewportView(jecnTable);
		activeScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置默认背景色
		jecnTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		jecnTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = jecnTable.getColumnModel();

		columnModel.getColumn(3).setCellRenderer(new PanelCellEdit());
		// 列不可拖动
		jecnTable.getTableHeader().setReorderingAllowed(false);
		// 设置行高
		jecnTable.setRowHeight(20);

		columnModel.getColumn(3).setMaxWidth(150);
		columnModel.getColumn(3).setMinWidth(150);

		columnModel.getColumn(1).setMaxWidth(150);
		columnModel.getColumn(1).setMinWidth(150);
		// 默认隐藏
		columnModel.getColumn(0).setMinWidth(0);
		columnModel.getColumn(0).setMaxWidth(0);
	}

	/*	*//**
	 * 
	 * 按钮监听
	 */
	/*
	 * private void onLineBoxLinstener() { if (onLineBox.isSelected()) {// 勾选
	 * showBut(); } else { hiddenBut(); } }
	 */

	private void showBut() {
		addButton.setEnabled(true);
		editButton.setEnabled(true);
		deleteBut.setEnabled(true);
	}

	private void hiddenBut() {
		addButton.setEnabled(false);
		editButton.setEnabled(false);
		deleteBut.setEnabled(false);
	}

	/*	*//**
	 * 是否线上验证
	 * 
	 * @return
	 */
	/*
	 * public boolean isOnLineCheck() { if (onLineBox.isSelected()) {// 选中：线上
	 * int rows = jecnTable.getRowCount(); if (rows == 0) {// 没有数据 return true;
	 * } for (int i = 0; i < rows; i++) {// 行数循环 // 获取时间对象 Object objectTime =
	 * jecnTable.getValueAt(i, 3); // 获取名称 Object name = jecnTable.getValueAt(i,
	 * 1); if (objectTime == null) { JecnOptionPane.showMessageDialog(this,
	 * JecnProperties.getValue("onlineTimeCannotBeEmpty")); return false; } else
	 * { PanelCellEdit panelCellEdit = (PanelCellEdit) objectTime; if
	 * (DrawCommon.isNullOrEmtryTrim(panelCellEdit.getText())) {
	 * JecnOptionPane.showMessageDialog(this,
	 * JecnProperties.getValue("onlineTimeCannotBeEmpty")); return false; } } if
	 * (name == null || DrawCommon.isNullOrEmtryTrim(name.toString())) {
	 * JecnOptionPane.showMessageDialog(this,
	 * JecnProperties.getValue("sysNameNotNull")); return false; } } } return
	 * true; }
	 */

	/**
	 * 生成table的Model
	 * 
	 * @return
	 */
	private DefaultTableModel getTableModel() {
		Vector<Object> title = new Vector<Object>();
		title.add("id");
		// 系统名称
		title.add(JecnProperties.getValue("sysName"));
		// 事务代码
		title.add(JecnConfigTool.getItDesc());
		// 上线时间
		title.add(JecnProperties.getValue("onLineTime"));
		return new ActiveCompetecnMode(getContent(), title);
	}

	/**
	 * table 默认
	 * 
	 * @author Administrator
	 * @date： 日期：2013-11-14 时间：上午09:51:05
	 */
	class ActiveCompetecnMode extends DefaultTableModel {
		public ActiveCompetecnMode(Vector<Vector<Object>> data, Vector<Object> title) {
			super(data, title);

		}

		/**
		 * 编辑单元格
		 * 
		 */
		public boolean isCellEditable(int rowindex, int colindex) {
			if (colindex == 3) {// 线上为勾选
				Object o = jecnTable.getValueAt(rowindex, colindex);

				// 主键ID
				Object toolId = jecnTable.getValueAt(rowindex, 0);
				if (toolId == null) {
					return false;
				}
				if (o instanceof PanelCellEdit) {
					PanelCellEdit fieldAndCalendarPanel = (PanelCellEdit) o;
					fieldAndCalendarPanel.eventProcess();
				}
				return true;
			}
			return false;
		}

		public void remoeAll() {
			// 清空
			for (int index = this.getRowCount() - 1; index >= 0; index--) {
				this.removeRow(index);
			}
		}
	}

	/***************************************************************************
	 * 初始化内容
	 * 
	 * @return
	 */
	private Vector<Vector<Object>> getContent() {
		if (activeData == null) {
			return null;
		}
		Vector<Vector<Object>> vs = new Vector<Vector<Object>>();
		try {
			// 关联类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
			for (JecnActiveOnLineTBean activeOnLineTBean : activeData.getListJecnActiveOnLineTBean()) {
				vs.add(getRowData(activeOnLineTBean));
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			return null;
		}
		return vs;
	}

	private Vector<Object> getRowData(JecnActiveOnLineTBean activeOnLineTBean) {
		Vector<Object> row = new Vector<Object>();
		row.add(activeOnLineTBean.getToolId());
		// 文件名称
		row.add(activeOnLineTBean.getSysName());
		row.add(activeOnLineTBean.getInformationDescription());
		PanelCellEdit panelCellEdit = new PanelCellEdit();
		panelCellEdit.setText(JecnTool.getStringbyDate(activeOnLineTBean.getOnLineTime()));
		row.add(panelCellEdit);
		return row;
	}

	/**
	 * 删除按钮监听
	 * 
	 */
	public void deleteButListener() {
		int[] selectRows = jecnTable.getSelectedRows();
		if (selectRows.length == 0) {
			// 请选择一行!
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(selectRows[i]);
		}
	}

	/**
	 * 编辑
	 * 
	 * @param row
	 * @param activeOnLineTBean
	 */
	public void updateRow(int row, JecnActiveOnLineTBean activeOnLineTBean) {
		jecnTable.setValueAt(activeOnLineTBean.getToolId().toString(), row, 0);
		jecnTable.setValueAt(activeOnLineTBean.getSysName(), row, 1);
		jecnTable.setValueAt(activeOnLineTBean.getInformationDescription(), row, 2);
		((PanelCellEdit) jecnTable.getValueAt(row, 3)).setText(JecnTool.getStringbyDate(activeOnLineTBean
				.getOnLineTime()));
		jecnTable.updateUI();
	}

	/**
	 * 添加
	 * 
	 * @param activeOnLineTBean
	 */
	public void addRow(JecnActiveOnLineTBean activeOnLineTBean) {
		((DefaultTableModel) jecnTable.getModel()).addRow(this.getRowData(activeOnLineTBean));
		jecnTable.updateUI();
	}

	/**
	 * 通过Table 获得结果集
	 * 
	 * @return
	 */
	public List<JecnActiveOnLineTBean> getResultList() {
		List<JecnActiveOnLineTBean> resultList = new ArrayList<JecnActiveOnLineTBean>();
		int rows = jecnTable.getRowCount();
		PanelCellEdit cellEdit = null;
		String toolId = null;
		String name = "";
		String informationDescription = "";
		boolean isUpdate = false;

		JecnActiveOnLineTBean newActiveOnLineTBean = null;
		for (int i = 0; i < rows; i++) {
			toolId = (jecnTable.getValueAt(i, 0)).toString();
			if (jecnTable.getValueAt(i, 1) != null && !"".equals(jecnTable.getValueAt(i, 1).toString())) {
				name = jecnTable.getValueAt(i, 1).toString();
			} else {
				name = "";
			}
			if (jecnTable.getValueAt(i, 2) != null && !"".equals(jecnTable.getValueAt(i, 2).toString())) {
				informationDescription = jecnTable.getValueAt(i, 2).toString();
			} else {
				informationDescription = "";
			}
			cellEdit = (PanelCellEdit) jecnTable.getValueAt(i, 3);
			for (JecnActiveOnLineTBean activeOnLineTBean : activeData.getListJecnActiveOnLineTBean()) {
				if (activeOnLineTBean.getToolId().toString().equals(toolId)) {// 工具ID相同
					newActiveOnLineTBean = new JecnActiveOnLineTBean();
					newActiveOnLineTBean.setActiveId(activeData.getFlowElementId());
					newActiveOnLineTBean.setToolId(activeOnLineTBean.getToolId());
					newActiveOnLineTBean.setSysName(name);
					newActiveOnLineTBean.setInformationDescription(informationDescription);
					newActiveOnLineTBean.setProcessId(activeOnLineTBean.getProcessId());
					newActiveOnLineTBean.setId(activeOnLineTBean.getId());
					newActiveOnLineTBean.setFigureUUID(activeOnLineTBean.getFigureUUID());
					if (!"".equals(cellEdit.getText())) {
						newActiveOnLineTBean.setOnLineTime(JecnTool.getDateByString(cellEdit.getText()));
					} else {
						newActiveOnLineTBean.setOnLineTime(null);
					}
					resultList.add(newActiveOnLineTBean);
					isUpdate = true;
					break;
				}
			}
			if (!isUpdate) {
				// 活动ID
				newActiveOnLineTBean = new JecnActiveOnLineTBean();
				newActiveOnLineTBean.setActiveId(activeData.getFlowElementId());
				newActiveOnLineTBean.setToolId(Long.valueOf(toolId));
				newActiveOnLineTBean.setSysName(name);
				newActiveOnLineTBean.setFigureUUID(activeData.getUUID());
				newActiveOnLineTBean.setInformationDescription(informationDescription);
				newActiveOnLineTBean.setOnLineTime(JecnTool.getDateByString(cellEdit.getText()));
				// 获取流程ID
				newActiveOnLineTBean.setProcessId(JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
						.getFlowId());
				resultList.add(newActiveOnLineTBean);
			}
			isUpdate = false;
		}
		return resultList;
	}

	/**
	 * 主面板点击确认执行
	 * 
	 */
	public void okButListener() {
		// 更新的集合
		List<JecnActiveOnLineTBean> resultList = getResultList();
		/*
		 * if (onLineBox.isSelected()) {// 显示选中 0：线下 ;1：线上
		 * activeData.setIsOnLine(1); } else { activeData.setIsOnLine(0); }
		 */
		activeData.setIsOnLine(1);// 默认线上
		activeData.getListJecnActiveOnLineTBean().clear();
		activeData.getListJecnActiveOnLineTBean().addAll(resultList);
	}

	public boolean isUpdate() {
		List<JecnActiveOnLineTBean> resultList = getResultList();
		if (resultList.size() != activeData.getListJecnActiveOnLineTBean().size()) {
			return true;
		}
		for (JecnActiveOnLineTBean newActiveOnLineTBean : resultList) {
			boolean isExist = false;
			for (JecnActiveOnLineTBean oldActiveOnLineTBean : activeData.getListJecnActiveOnLineTBean()) {
				if (DrawCommon.checkStringSame(newActiveOnLineTBean.getId(), oldActiveOnLineTBean.getId())
						&& DrawCommon.checkStringSame(newActiveOnLineTBean.getInformationDescription(),
								oldActiveOnLineTBean.getInformationDescription())
						&& DrawCommon.checkLongSame(newActiveOnLineTBean.getToolId(), oldActiveOnLineTBean.getToolId())
						&& DrawCommon.checkDateSame(newActiveOnLineTBean.getOnLineTime(), oldActiveOnLineTBean
								.getOnLineTime())
						&& DrawCommon.checkStringSame(newActiveOnLineTBean.getInformationDescription(),
								oldActiveOnLineTBean.getInformationDescription())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 表格单元格渲染
	 * 
	 * @author ZHAGNXIAOHU
	 * @date： 日期：2013-11-13 时间：下午05:53:16
	 */
	class PanelCellEdit extends JecnTextFieldAndCalendarPanel implements TableCellRenderer {

		public PanelCellEdit() {
			super();
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			JecnTextFieldAndCalendarPanel panel = null;
			if (value instanceof JecnTextFieldAndCalendarPanel) {
				panel = (JecnTextFieldAndCalendarPanel) value;
			}
			return panel == null ? new JecnTextFieldAndCalendarPanel() : panel;
		}

		public void eventProcess() {
			super.eventProcess();
		}
	}
}
