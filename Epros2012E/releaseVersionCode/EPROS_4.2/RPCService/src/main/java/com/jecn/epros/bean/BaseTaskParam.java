package com.jecn.epros.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 审批项最基本的父类bean
 * @author user
 *
 */
public class BaseTaskParam implements Serializable{
	
	/** 任务主键ID */
	private String taskId;
	/**
	 * 任务审批人的操作状态 
	 * 0：拟稿人提交审批 
	 * 1：通过 
	 * 2：交办
	 * 3：转批
	 * 4：打回
	 * 5：提交意见（提交审批意见(评审-------->拟稿人)
	 * 6：二次评审 拟稿人------>评审 
	 * 7：拟稿人重新提交审批 
	 * 8：完成 
	 * 9：打回整理意见(批准------>拟稿人)）
	 * 10：交办人提交
	 * 11、12：编辑 (系统管理员编辑任务) 
	 * 13：撤回 
	 * 14：返回
	 **/
	private String taskOperation;
	/** 任务阶段**/
	private int taskStage;
	/** 提交审核信息 */
	private String opinion;
	/** 执行日期 */
	private Date implementDate;

	public Date getImplementDate() {
		return implementDate;
	}
	public void setImplementDate(Date implementDate) {
		this.implementDate = implementDate;
	}
	public String getOpinion() {
		return opinion;
	}
	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}
	public int getTaskStage() {
		return taskStage;
	}
	public void setTaskStage(int taskStage) {
		this.taskStage = taskStage;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskOperation() {
		return taskOperation;
	}
	public void setTaskOperation(String taskOperation) {
		this.taskOperation = taskOperation;
	}
}
