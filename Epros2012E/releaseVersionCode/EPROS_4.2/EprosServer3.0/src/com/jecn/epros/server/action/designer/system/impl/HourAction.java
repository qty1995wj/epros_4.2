package com.jecn.epros.server.action.designer.system.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.service.process.IFlowStructureService;

/**
 * 
 * 每小时触发的程序
 * 
 * @author hyl
 * 
 */
public class HourAction {

	private final static Log log = LogFactory.getLog(HourAction.class);
	private IFlowStructureService flowStructureService;
	private IFlowStructureDao flowStructureDao;

	public void timerExecte() {
		driver();
	}

	private void driver() {
		if (!JecnConfigTool.isUseNewDriver()) {
			return;
		}
		if (!JecnConfigTool.isShowDriverTime()) {
			return;
		}
		if (!JecnConfigTool.isSendFlowDriverEmail() && !JecnConfigTool.isSendFlowDriverEmailToTip()) {
			return;
		}
		try {
			flowStructureService.sendFlowDriverEmailNew();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	public IFlowStructureService getFlowStructureService() {
		return flowStructureService;
	}

	public void setFlowStructureService(IFlowStructureService flowStructureService) {
		this.flowStructureService = flowStructureService;
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

}
