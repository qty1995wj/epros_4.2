package com.jecn.epros.controller;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.org.PosBaseInfoBean;
import com.jecn.epros.domain.org.PosViewBean;
import com.jecn.epros.domain.org.SearchPosResultBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.IDocRPCService;
import com.jecn.epros.service.org.OrgService;
import com.jecn.epros.util.AttachmentDownloadUtils;
import com.jecn.epros.util.excel.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "岗位")
public class PositionController {

    @Autowired
    private OrgService orgService;

    @ApiOperation(value = "岗位-搜索")
    @RequestMapping(value = "/positions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PosViewBean> search(
            @RequestParam(defaultValue = "") String name, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("paging", paging);
        map.put("name", name);
        PosViewBean posViewBean = new PosViewBean();
        int total = orgService.searchPosTotal(map);
        List<SearchPosResultBean> listSearchPosResultBean = orgService.searchPos(map);
        posViewBean.setTotal(total);
        posViewBean.setData(listSearchPosResultBean);
        return ResponseEntity.ok(posViewBean);
    }

        @PreAuthorize("hasAuthority('posBaseInfo')")
    @ApiOperation(value = "岗位-详情")
    @RequestMapping(value = "positions/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PosBaseInfoBean> findPosBaseInfo(@PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        return ResponseEntity.ok(orgService.findPosBaseInfoBean(map));
    }

}