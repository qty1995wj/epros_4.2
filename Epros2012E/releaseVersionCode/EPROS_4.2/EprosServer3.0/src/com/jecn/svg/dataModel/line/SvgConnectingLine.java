package com.jecn.svg.dataModel.line;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jecn.svg.dataModel.SvgBaseGraph;
import com.jecn.svg.dataModel.epros.SvgEprosGraphData;

/**
 * 连接线数据对象
 * 
 * @date 2016-8-2下午03:53:54
 */
public class SvgConnectingLine extends SvgBaseGraph {

	private List<SvgLineData> lines = new ArrayList<SvgLineData>();

	private Long textX;
	private Long textY;

	/** 1：显示双箭头 */
	private int isTwoArrowLine;

	/** 开始图形元素ID */
	private Long startFigureId;

	/** 连接线起始位置点 1左 2上 3右 4下 */
	private int poLongx;

	private Point textPoint;

	public List<SvgLineData> getLines() {
		return lines;
	}

	public void addLineData(Long startX, Long startY, Long endX, Long endY, List<int[]> xyPoint) {
		SvgLineData lineData = new SvgLineData();
		lineData.setStartX(startX);
		lineData.setStartY(startY);
		lineData.setEndX(endX);
		lineData.setEndY(endY);
		lineData.addCrossOverPoints(xyPoint);
		this.lines.add(lineData);
	}

	/**
	 * 小线段数据集排序
	 * 
	 * @param lineSegmentDataList
	 */
	public void reOrderLinePoint(List<SvgEprosGraphData> eprosGraphResource) {
		SvgEprosGraphData startFigure = getStartFigure(startFigureId, eprosGraphResource);
		if (startFigure == null) {
			return;
		}
		// 开始点位置方向
		Point mStartP = this.getLineStartPoint(startFigure, poLongx);

		List<SvgLineData> list = new ArrayList<SvgLineData>();
		if (mStartP != null) {
			reformSegmentsChild(list, mStartP, lines);
		}
		lines = list;
	}

	/**
	 * 递归算法，根据开始点和线的集合递归，当前开始点为下个线段的结束点
	 * 
	 * @param list
	 * @param pStartP
	 *            输入开始点
	 * @param lines
	 */
	private void reformSegmentsChild(List<SvgLineData> list, Point pStartP, List<SvgLineData> lines) {
		Point newStartP = null;
		for (int i = 0; i < lines.size(); i++) {
			SvgLineData data = lines.get(i);
			Long startX = data.getStartX();
			Long startY = data.getStartY();
			Long endX = data.getEndX();
			Long endY = data.getEndY();
			if (startX == pStartP.x && startY == pStartP.y) {
				// 线段开始点和线的开始位置点相等
				list.add(data);
				newStartP = new Point(data.getEndX().intValue(), data.getEndY().intValue());
				lines.remove(i);
				break;
			}

			if (endX == pStartP.x && endY == pStartP.y) {
				// 线段结束点和线的开始位置点相等
				// 开始变结束
				data.setEndY(data.getStartY());
				data.setEndX(data.getStartX());

				data.setStartX((long) pStartP.x);
				data.setStartY((long) pStartP.y);
				list.add(data);
				newStartP = new Point(data.getEndX().intValue(), data.getEndY().intValue());
				lines.remove(i);
				break;
			}
		}
		if (newStartP != null) {
			reformSegmentsChild(list, newStartP, lines);
		}
	}

	@JsonProperty("x")
	public Long getTextX() {
		return textX;
	}

	@JsonProperty("y")
	public Long getTextY() {
		return textY;
	}

	public void setTextPosition() {
		if (getText() == null || lines.size() == 0) {
			return;
		}
		if (textPoint != null) {
			textX = Long.valueOf(textPoint.x);
			textY = Long.valueOf(textPoint.y);
			return;
		}
		SvgLineData lineWithText = null;
		if (lines.size() % 2 == 0) {
			lineWithText = lines.get(lines.size() / 2 - 1);
			textX = lineWithText.getEndX();
			textY = lineWithText.getEndY();
		} else {
			lineWithText = lines.get(lines.size() / 2);
			if (lineWithText.getStartX().intValue() == lineWithText.getEndX()) {
				textX = lineWithText.getStartX();
				textY = lineWithText.getStartY() + (lineWithText.getEndY() - lineWithText.getStartY()) / 2;
			} else {
				textX = lineWithText.getStartX() + (lineWithText.getEndX() - lineWithText.getStartX()) / 2;
				textY = lineWithText.getStartY();
			}
		}
	}

	/**
	 * 获取开始图形
	 * 
	 * @param startFigureId
	 * @param eprosGraphResource
	 * @return
	 */
	private SvgEprosGraphData getStartFigure(Long startFigureId, List<SvgEprosGraphData> eprosGraphResource) {
		for (SvgEprosGraphData graphData : eprosGraphResource) {
			if (startFigureId.equals(graphData.getFigureId())) {
				return graphData;
			}
		}
		return null;
	}

	/**
	 * 获取开始点
	 * 
	 * @param startFigure
	 * @param LongX
	 * @return
	 */
	private Point getLineStartPoint(SvgEprosGraphData startFigure, int LongX) {
		// 宽度
		Long width = startFigure.getWidth();
		// 高度
		Long height = startFigure.getHeight();

		Long x = startFigure.getPoLongx();
		Long y = startFigure.getPoLongy();

		Point point = new Point();
		// 1左 2上 3右 4下
		switch (LongX) {
		case 1:
			// 左中心点
			point.setLocation(x, y + height / 2.0);
			break;
		case 2:
			// 上中心点
			point.setLocation(x + width / 2.0, y);
			break;
		case 3:
			// 右中心点
			point.setLocation(x + width, y + height / 2.0);
			break;
		case 4:
			point.setLocation(x + width / 2.0, y + height);
			break;
		}

		return point;
	}

	public int getIsTwoArrowLine() {
		return isTwoArrowLine;
	}

	public void setIsTwoArrowLine(int isTwoArrowLine) {
		this.isTwoArrowLine = isTwoArrowLine;
	}

	public void setStartFigureId(Long startFigureId) {
		this.startFigureId = startFigureId;
	}

	public void setPoLongx(int poLongx) {
		this.poLongx = poLongx;
	}

	public void setTextPoint(Point textPoint) {
		this.textPoint = textPoint;
	}
}
