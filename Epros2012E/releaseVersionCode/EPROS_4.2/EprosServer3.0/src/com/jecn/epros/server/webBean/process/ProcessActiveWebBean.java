package com.jecn.epros.server.webBean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.process.JecnRefIndicators;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 活动相关信息Bean
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-12-9 时间：下午05:22:07
 */
public class ProcessActiveWebBean implements Serializable {
	/** 活动ID */
	private Long activeFigureId;
	/** 活动编号 */
	private String activeId;
	/** 活动名称 */
	private String activeName;
	/** 活动说明 */
	private String activeShow;
	/** 执行角色ID */
	private Long roleFigureId;
	/** 角色 */
	private String roleName;
	/** 角色职责 */
	private String roleShow;
	/** 活动输入 */
	private List<FileWebBean> listInput;
	/** 活动操作规范 */
	private List<FileWebBean> listOperation;
	/** 活动输出 */
	private List<ActiveModeBean> listMode;
	/** 关键活动 类型 1为PA,2为KSF,3为KCP */
	private int keyActiveState;
	/** 关键活动 类型 */
	private String strKeyActive;
	/** 关键活动说明 */
	private String keyActiveShow;
	/** 活动指标集合 */
	private List<JecnRefIndicators> indicatorsList;
	/** 活动指标的个数 */
	private String sizeRefIndIcators;
	/** 流程bean */
	private ProcessWebBean processWebBean;
	/** 活动开始时间 */
	private String startTime;
	/** 活动结束时间 */
	private String stopTime;
	/** 活动时间类型 */
	private Integer timeType;
	private String strTimeType;

	/** 对应内控矩阵风险点 */
	private String innerControlRisk;
	/** 对应标准条款 */
	private String standardConditions;

	/** 活动类别ID */
	private Long relateId;
	/** 活动类别名称 */
	private String activeTypeName;

	/** 控制点 */
	private JecnControlPointT controlPoint;

	/**
	 * 控制点集合 C
	 * */
	private List<JecnControlPointT> controlPointTs;

	/** 活动标准关联表 */
	private List<JecnActiveStandardBeanT> listJecnActiveStandardBean;
	/** 线上信息 */
	private List<JecnActiveOnLineTBean> listJecnActiveOnLineBean;

	/** 是否是关键控制点：0：是，1：否 */
	private String strKeyPoint;
	/** 控制频率 0:随时 1:日 2: 周 3:月 4:季度 5:年度 */
	private String strFrequency;

	/** 控制方法 0:人工1:自动 */
	private String strMethod;
	/** 控制类型 0:预防性1:发现性 */
	private String controlType;
	private String activityInput;// 输入说明
	private String activityOutput;// 输出说明
	// 活动输入输出说明配置
	private String activityNoteShow;

	// 活动明细时间配置
	private String activityTimeConfig;

	/** 存在值说明是接口（活动）/子流程（活动） */
	private Long linkFlowId;

	/** 是否是活动（子流程（活动）/接口（活动） 在页面不属于活动;） */
	private int activityFlowType;

	public String getActivityNoteShow() {
		return activityNoteShow;
	}

	public void setActivityNoteShow(String activityNoteShow) {
		this.activityNoteShow = activityNoteShow;
	}

	public List<JecnControlPointT> getControlPointTs() {
		return controlPointTs;
	}

	public void setControlPointTs(List<JecnControlPointT> controlPointTs) {
		this.controlPointTs = controlPointTs;
	}

	public JecnControlPointT getControlPoint() {
		return controlPoint;
	}

	public void setControlPoint(JecnControlPointT controlPoint) {
		this.controlPoint = controlPoint;
	}

	public List<JecnActiveStandardBeanT> getListJecnActiveStandardBean() {
		return listJecnActiveStandardBean;
	}

	public void setListJecnActiveStandardBean(List<JecnActiveStandardBeanT> listJecnActiveStandardBean) {
		this.listJecnActiveStandardBean = listJecnActiveStandardBean;
	}

	public List<JecnActiveOnLineTBean> getListJecnActiveOnLineBean() {
		return listJecnActiveOnLineBean;
	}

	public void setListJecnActiveOnLineBean(List<JecnActiveOnLineTBean> listJecnActiveOnLineBean) {
		this.listJecnActiveOnLineBean = listJecnActiveOnLineBean;
	}

	public String getStartTime() {
		this.startTime = getChangeValue(startTime);
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStopTime() {
		this.stopTime = getChangeValue(stopTime);
		return stopTime;
	}

	public void setStopTime(String stopTime) {
		this.stopTime = stopTime;
	}

	public Integer getTimeType() {
		return timeType;
	}

	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}

	public Long getActiveFigureId() {
		return activeFigureId;
	}

	public void setActiveFigureId(Long activeFigureId) {
		this.activeFigureId = activeFigureId;
	}

	public String getActiveId() {
		return activeId;
	}

	public void setActiveId(String activeId) {
		this.activeId = activeId;
	}

	public String getActiveName() {
		if (!JecnCommon.isNullOrEmtryTrim(activeName)) {
			return activeName;
		} else {
			return "";
		}
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}

	public Long getRoleFigureId() {
		return roleFigureId;
	}

	public void setRoleFigureId(Long roleFigureId) {
		this.roleFigureId = roleFigureId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<FileWebBean> getListInput() {
		return listInput;
	}

	public void setListInput(List<FileWebBean> listInput) {
		this.listInput = listInput;
	}

	public List<FileWebBean> getListOperation() {
		return listOperation;
	}

	public void setListOperation(List<FileWebBean> listOperation) {
		this.listOperation = listOperation;
	}

	public List<ActiveModeBean> getListMode() {
		return listMode;
	}

	public void setListMode(List<ActiveModeBean> listMode) {
		this.listMode = listMode;
	}

	public String getActiveShow() {
		if (!JecnCommon.isNullOrEmtryTrim(activeShow)) {
			return activeShow;
		} else {
			return "";
		}
	}

	public void setActiveShow(String activeShow) {
		this.activeShow = activeShow;
	}

	public int getKeyActiveState() {
		return keyActiveState;
	}

	public void setKeyActiveState(int keyActiveState) {
		this.keyActiveState = keyActiveState;
	}

	public String getKeyActiveShow() {
		return keyActiveShow;
	}

	public void setKeyActiveShow(String keyActiveShow) {
		this.keyActiveShow = keyActiveShow;
	}

	public List<JecnRefIndicators> getIndicatorsList() {
		return indicatorsList;
	}

	public void setIndicatorsList(List<JecnRefIndicators> indicatorsList) {
		this.indicatorsList = indicatorsList;
	}

	public String getSizeRefIndIcators() {
		return sizeRefIndIcators;
	}

	public void setSizeRefIndIcators(String sizeRefIndIcators) {
		this.sizeRefIndIcators = sizeRefIndIcators;
	}

	public ProcessWebBean getProcessWebBean() {
		return processWebBean;
	}

	public void setProcessWebBean(ProcessWebBean processWebBean) {
		this.processWebBean = processWebBean;
	}

	public String getRoleShow() {
		return roleShow;
	}

	public void setRoleShow(String roleShow) {
		this.roleShow = roleShow;
	}

	public String getInnerControlRisk() {
		return innerControlRisk;
	}

	public void setInnerControlRisk(String innerControlRisk) {
		this.innerControlRisk = innerControlRisk;
	}

	public String getStandardConditions() {
		return standardConditions;
	}

	public void setStandardConditions(String standardConditions) {
		this.standardConditions = standardConditions;
	}

	public Long getRelateId() {
		return relateId;
	}

	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}

	public String getActiveTypeName() {
		return activeTypeName;
	}

	public void setActiveTypeName(String activeTypeName) {
		this.activeTypeName = activeTypeName;
	}

	/**
	 * 获取活动时间类型
	 * 
	 * @return
	 */
	public String getStrTimeType() {
		if (timeType == null) {
			return strTimeType;
		}
		switch (timeType) {
		case 1:// 日
			strTimeType = JecnUtil.getValue("day");
			break;
		case 2:// 周
			strTimeType = JecnUtil.getValue("weeks");
			break;
		case 3:// 月
			strTimeType = JecnUtil.getValue("month");
			break;
		case 4:// 季度
			strTimeType = JecnUtil.getValue("season");
			break;
		case 5:// 年
			strTimeType = JecnUtil.getValue("years");
			break;
		}
		return strTimeType;
	}

	final String onMonday = JecnUtil.getValue("onMonday");
	final String onTuesday = JecnUtil.getValue("onTuesday");
	final String onWednesda = JecnUtil.getValue("onWednesda");
	final String onThursday = JecnUtil.getValue("onThursday");
	final String onFriday = JecnUtil.getValue("onFriday");
	final String onSaturday = JecnUtil.getValue("onSaturday");
	final String onSunday = JecnUtil.getValue("onSunday");

	/**
	 * 时间类型为周
	 * 
	 * @param strType
	 *            2 周
	 * @return strType对应的日期
	 */
	private String getChangeValue(String strType) {
		if (this.timeType == null) {
			return "";
		}
		String chahgeValue = "";
		switch (this.timeType) {
		case 1:// 日
			chahgeValue = dayValue(strType);
			break;
		case 2:// 周
			chahgeValue = weekValue(strType);
			break;
		case 3:// 月
			chahgeValue = monthValue(strType);
			break;
		case 4:// 季度
			chahgeValue = seasonValue(strType);
			break;
		case 5:// 年
			chahgeValue = yearValue(strType);
			break;
		}
		return chahgeValue;
	}

	private String yearValue(String yearValue) {
		if (JecnCommon.isNullOrEmtryTrim(yearValue)) {
			return "";
		}
		String[] str = yearValue.split(",");
		if (str.length != 2) {
			return "";
		}
		StringBuffer newStr = new StringBuffer();
		newStr.append(Integer.valueOf(str[0]) + 1);
		// 月
		newStr.append(JecnUtil.getValue("month"));
		newStr.append(Integer.valueOf(str[1]) + 1);
		// 日
		newStr.append(JecnUtil.getValue("day"));
		return newStr.toString();
	}

	private String seasonValue(String seasonValue) {
		if (JecnCommon.isNullOrEmtryTrim(seasonValue)) {
			return "";
		}
		return Integer.valueOf(seasonValue) + 1 + JecnUtil.getValue("day");
	}

	private String monthValue(String monthValue) {
		if (JecnCommon.isNullOrEmtryTrim(monthValue)) {
			return "";
		}
		return Integer.valueOf(monthValue) + 1 + JecnUtil.getValue("day");
	}

	private String dayValue(String dayValue) {
		if (JecnCommon.isNullOrEmtryTrim(dayValue)) {
			return "";
		}
		String[] str = dayValue.split(",");
		if (str.length != 3) {
			return "";
		}
		StringBuffer newStr = new StringBuffer();
		newStr.append(str[0]);
		// 时
		newStr.append(JecnUtil.getValue("when"));
		newStr.append(str[1]);
		// 分
		newStr.append(JecnUtil.getValue("points"));
		newStr.append(str[2]);
		// 秒
		newStr.append(JecnUtil.getValue("seconds"));
		return newStr.toString();
	}

	private String weekValue(String strType) {
		if (JecnCommon.isNullOrEmtryTrim(strType)) {
			return "";
		}
		int type = Integer.valueOf(strType);
		switch (type) {
		case 0:
			return onMonday;
		case 1:

			return onTuesday;
		case 2:

			return onWednesda;
		case 3:

			return onThursday;
		case 4:

			return onFriday;
		case 5:

			return onSaturday;
		case 6:

			return onSunday;
		}
		return "";
	}

	/**
	 * 1为PA,2为KSF,3为KCP
	 * 
	 * @return
	 */
	public String getStrKeyActive() {
		if (keyActiveState == 1) {//
			strKeyActive = "PA";
		} else if (keyActiveState == 2) {
			strKeyActive = "KSF";
		} else if (keyActiveState == 3) {
			strKeyActive = "KCP";
		} else {
			strKeyActive = "无";
		}
		return strKeyActive;
	}

	public void setStrTimeType(String strTimeType) {
		this.strTimeType = strTimeType;
	}

	/**
	 * 控制类型转换
	 * 
	 * @return
	 */
	public String getControlType() {
		if (this.controlPoint.getType() == 1) {// 发现性
			controlType = JecnUtil.getValue("discover");
		} else {// 预防性
			controlType = JecnUtil.getValue("preventive");
		}
		return controlType;
	}

	/**
	 * 控制方法转换
	 * 
	 * @return
	 */
	public String getStrMethod() {
		if (this.controlPoint.getMethod() == 0) {// 人工
			strMethod = JecnUtil.getValue("artificial");
		} else {// 自动
			strMethod = JecnUtil.getValue("autoSys");
		}
		return strMethod;
	}

	/**
	 * 控制频率转换
	 * 
	 * @return
	 */
	public String getStrFrequency() {
		switch (this.controlPoint.getFrequency()) {
		case 0:// 随时
			strFrequency = JecnUtil.getValue("anyTime");
			break;
		case 1:// 日
			strFrequency = JecnUtil.getValue("day");
			break;
		case 2:// 周
			strFrequency = JecnUtil.getValue("weeks");
			break;
		case 3:// 月
			strFrequency = JecnUtil.getValue("month");
			break;
		case 4:// 季度
			strFrequency = JecnUtil.getValue("seasons");
			break;
		case 5:// 年度
			strFrequency = JecnUtil.getValue("years");
			break;
		}
		return strFrequency;
	}

	public String getActivityInput() {
		return activityInput;
	}

	public void setActivityInput(String activityInput) {
		this.activityInput = activityInput;
	}

	public String getActivityOutput() {
		return activityOutput;
	}

	public void setActivityOutput(String activityOutput) {
		this.activityOutput = activityOutput;
	}

	public String getActivityTimeConfig() {
		return activityTimeConfig;
	}

	public void setActivityTimeConfig(String activityTimeConfig) {
		this.activityTimeConfig = activityTimeConfig;
	}

	/**
	 * 返回 是否为关键控制点
	 * 
	 * @return
	 */
	public String getStrKeyPoint() {
		if (this.controlPoint.getKeyPoint() == 0) {
			strKeyPoint = JecnUtil.getValue("is");
		} else {
			strKeyPoint = JecnUtil.getValue("no");
		}
		return strKeyPoint;
	}

	public Long getLinkFlowId() {
		return linkFlowId;
	}

	public void setLinkFlowId(Long linkFlowId) {
		this.linkFlowId = linkFlowId;
	}

	public int getActivityFlowType() {
		return activityFlowType;
	}

	public void setActivityFlowType(int activityFlowType) {
		this.activityFlowType = activityFlowType;
	}

}
