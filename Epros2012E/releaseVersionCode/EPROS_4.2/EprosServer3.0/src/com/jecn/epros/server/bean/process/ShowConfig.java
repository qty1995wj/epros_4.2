package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 页面显示的配置项
 * @author user
 *
 */
public class ShowConfig implements Serializable{
	
	private int width;
	private boolean readOnly=true;
	private String name;
	private String cls;
	private String fieldLabel;
	private boolean hidden=false;
	private int height;
//	private boolean grow=false;
	
	private String xtype;
	
	
	
	public String getXtype() {
		return xtype;
	}
	public void setXtype(String xtype) {
		this.xtype = xtype;
	}
//	public boolean isGrow() {
//		return grow;
//	}
//	public void setGrow(boolean grow) {
//		this.grow = grow;
//	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public boolean isReadOnly() {
		return readOnly;
	}
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCls() {
		return cls;
	}
	public void setCls(String cls) {
		this.cls = cls;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	

}
