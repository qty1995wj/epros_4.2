package epros.designer.gui.rule;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.JecnRuleData;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 新建制度(制度基本信息，制度说明，制度文控信息)
 * 
 * @author Administrator 2012-10-24
 */
public class AddRuleInfoDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddRuleInfoDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 制度操作说明面板Operating Instructions */
	private JScrollPane operationPanel = new JScrollPane();

	/** 制度基本信息面板 */
	private JScrollPane baseInfoPanel = new JScrollPane();

	/** JTabbedPane */
	private JTabbedPane tabPanel = new JTabbedPane();

	/** 按钮面板 */
	private JPanel buttonPanel = new JecnPanel();

	/*** 验证提示 */
	private JLabel verfyLab = new JLabel();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	// 制度操作说明
	private RuleOperatePanel ruleOperatePanel = null;
	// 制度基本信息
	private RuleBaseInfoPanel ruleBaseInfoPanel = null;

	public AddRuleInfoDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		// 设置窗体大小
		this.setSize(950, 670);
		this.setTitle(JecnProperties.getValue("newRule"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);

		// 设置面板背景颜色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		operationPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		baseInfoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加ui
		tabPanel.setUI(new JecnWindowsTabbedPaneUI());
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

		verfyLab.setForeground(Color.red);
		this.setModal(true);
		initLayout();
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		Container contentPane = this.getContentPane();
		tabPanel.add(baseInfoPanel, 0);
		tabPanel.setTitleAt(0, JecnProperties.getValue("ruleBaseInfo"));
		tabPanel.add(operationPanel, 1);
		tabPanel.setTitleAt(1, JecnProperties.getValue("ruleOperation"));
		// tabPanel.add(controlPanel, 2);
		// tabPanel.setTitleAt(2, "制度文控信息");

		// 制度操作说明
		ruleOperatePanel = new RuleOperatePanel(selectNode);
		operationPanel.setViewportView(ruleOperatePanel);

		// 制度基本信息
		ruleBaseInfoPanel = new RuleBaseInfoPanel(AddRuleInfoDialog.this, selectNode.getJecnTreeBean().getId());
		baseInfoPanel.setViewportView(ruleBaseInfoPanel);
		ruleBaseInfoPanel.getRuleNumField().setColumns(20);
		ruleBaseInfoPanel.getRuleNameField().setColumns(20);
		
		// 按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		contentPane.add(buttonPanel, BorderLayout.SOUTH);
		contentPane.add(tabPanel, BorderLayout.CENTER);
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	/***
	 * 取消
	 */
	private void cancelPerformed() {
		cancelButPerformed();
	}

	private boolean isUpdate() {
		// 制度模板是否更新
		if (ruleOperatePanel != null) {
			if (ruleOperatePanel.isUpdate()) {
				return true;
			}
		}
		// 基本信息
		if (ruleBaseInfoPanel.isUpdate()) {
			return true;
		}
		return false;
	}

	/***
	 * 确定
	 */
	private void okButPerformed() {
		verfyLab.setText("");
		String ruleName = ruleBaseInfoPanel.ruleNameField.getText().trim();
		String ss = JecnUserCheckUtil.checkName(ruleName);
		if (!DrawCommon.isNullOrEmtryTrim(ss)) {
			verfyLab.setText(JecnProperties.getValue("ruleName") + ss);
			return;
		}
		/*
		 * if (!checkIsRequest()) { // 验证制度必填项 return; }
		 */
		// 关键字 长度验证
		if (ruleBaseInfoPanel.keyWordField != null
				&& DrawCommon.checkNameMaxLength(ruleBaseInfoPanel.keyWordField.getResultStr())) {
			verfyLab.setText(JecnProperties.getValue("keyWord") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 验证制度编号长度
		if (DrawCommon.checkNameMaxLength(ruleBaseInfoPanel.ruleNumField.getText())) {
			verfyLab.setText(JecnProperties.getValue("ruleNum") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}

		// 有效期
		if (ruleBaseInfoPanel.expiryCommon != null) {
			if (ruleBaseInfoPanel.expiryCommon.isValidateNotPass(verfyLab)) {
				return;
			}
		}
		// 验证自定义输入项长度
		if (ruleBaseInfoPanel.validationLength()) {
			return;
		}
		try {
			if (ConnectionPool.getRuleAction().validateAddName(ruleName, selectNode.getJecnTreeBean().getId())) {
				verfyLab.setText(JecnProperties.getValue("nameHaved"));
				return;
			}
			if (validateNamefullPath(ruleName, 1)) {
				return;
			}
		} catch (Exception e1) {
			log.error("AddRuleInfoDialog okButPerformed is error", e1);
		}

		// =====================获取制度操作说明===============
		// 要保存的制度信息
		List<JecnRuleOperationCommon> ruleList = ruleOperatePanel.getResultData();
		try {
			JecnRuleData ruleData = ruleBaseInfoPanel.getJecnRuleData();
			ruleData.getRuelBean().setPerId(selectNode.getJecnTreeBean().getId());
			ruleData.getRuelBean().setSortId(JecnTreeCommon.getMaxSort(selectNode));
			ruleData.getRuelBean().setProjectId(JecnConstants.projectId);
			ruleData.getRuelBean().setIsDir(1);
			ruleData.getRuelBean().setCreatePeopleId(JecnConstants.getUserId());
			ruleData.getRuelBean().setCreateDate(new Date());
			ruleData.setListJecnRuleOperationCommon(ruleList);
			ruleData.setRuleRiskTList(ruleOperatePanel.getRiskIds());
			ruleData.setStandardTList(ruleOperatePanel.getStandardIds());
			ruleData.setRuleInstitutionList(ruleOperatePanel.getRuleIds());
			Long id = ConnectionPool.getRuleAction().updateRule(ruleData); // modelId
			// 向树节点添加制度目录节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(ruleName);
			jecnTreeBean.setPid(selectNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(selectNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeFile);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, selectNode);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("exceptionOfNewSystem"));
			log.error(JecnProperties.getValue("exceptionOfNewSystem"), e);
		}
		this.setVisible(false);
	}

	/**
	 * 提示 文件库中是否有重名文件
	 * 
	 * @param names
	 *            type 1 模板 2文件
	 * @return
	 */
	private boolean validateNamefullPath(String ruleName, int type) {
		try {
			String ns = ConnectionPool.getRuleAction().validateNamefullPath(ruleName,
					selectNode.getJecnTreeBean().getId(), type);
			if (!"".equals(ns)) {
				String tipText = ns + " " + JecnProperties.getValue("itExistsDirectories");
				// 是否删除提示框
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), tipText, null,
						JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 验证必填项
	 * 
	 * @return
	 */
	private boolean checkIsRequest() {

		// 制度编号
		if (ruleBaseInfoPanel.ruleNumField != null && ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleCode)) {// 流程编号是否必填
			if ("".equals(ruleBaseInfoPanel.ruleNumField.getText().trim())) {
				verfyLab.setText(ruleBaseInfoPanel.ruleNumLab.getText().substring(0,
						ruleBaseInfoPanel.ruleNumLab.getText().length() - 1)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 制度类别
		if (ruleBaseInfoPanel.categoryCommon != null && ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleType)) {
			if (ruleBaseInfoPanel.categoryCommon.getTypeResultId() == null
					|| ruleBaseInfoPanel.categoryCommon.getTypeResultId() == 0) {
				verfyLab.setText(JecnProperties.getValue("categoriesC") + JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}

		// 生效日期
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.isShowEffectiveDate)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.effectiveTime.getText())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.isShowEffectiveDate)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 责任人
		if (ruleBaseInfoPanel.personliableSelect != null) {
			if (!ruleBaseInfoPanel.personliableSelect.getSingleSelectCommon().validateRequired(verfyLab)) {
				return false;
			}
		}
		// 监护人
		if (ruleBaseInfoPanel.guardianPeopleSelectCommon != null) {
			if (!ruleBaseInfoPanel.guardianPeopleSelectCommon.validateRequired(verfyLab)) {
				return false;
			}
		}
		// 责任部门
		if (ruleBaseInfoPanel.resDepartSelectCommon != null) {
			if (!ruleBaseInfoPanel.resDepartSelectCommon.validateRequired(verfyLab)) {
				return false;
			}
		} // 专员
		if (ruleBaseInfoPanel.commissionerPeopleSelectCommon != null) {
			if (!ruleBaseInfoPanel.commissionerPeopleSelectCommon.validateRequired(verfyLab)) {
				return false;
			}
		}
		// 关键字
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.isShowRuleKeyWord)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.keyWordField.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.isShowRuleKeyWord)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 业务范围
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleBusinessScope)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.businessScope.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleBusinessScope)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 其他范围
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleOtherScope)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.otherScope.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleOtherScope)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}

		// 制度目的
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.rulePurpose)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.rulePurpose.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.rulePurpose)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 适用范围
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleApplicability)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.ruleApplicability.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleApplicability)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 文件换版说明
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleChangeVersionExplain)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.changeVersionExplain.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleChangeVersionExplain)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 文件编写部门
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleRriteDept)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.ruleRriteDept.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleRriteDept)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		// 起草人
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleDraftMan)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.ruleDraftMan.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleDraftMan)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}

		// 起草单位
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleDraftingDept)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.ruleDraftingDept.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleDraftingDept)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}

		// 试行版文件
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleTestRunFile)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.ruleTestRunFile.getResultStr())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleTestRunFile)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}

		// 部门范围
		if (ruleBaseInfoPanel.isRequest(ConfigItemPartMapMark.ruleOrgScope)) {// 
			if (StringUtils.isBlank(ruleBaseInfoPanel.orgScope.getResultIds())) {
				verfyLab.setText(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleOrgScope)
						+ JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		return true;
	}
}
