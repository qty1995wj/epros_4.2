package com.jecn.epros.server.dao.system.impl;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnUserLoginLog;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.dao.system.IJecnUserLoginLogDao;

/**
 * 登录日志(JECN_USER_LOGIN_LOG)操作类
 * @author 201305
 *
 */
public class JecnUserLoginLogDaoImpl extends
AbsBaseDao<JecnUserLoginLog, String> implements IJecnUserLoginLogDao{

	@Override
	public int addUserLoginLog(Long userId, String sessionId, int loginType) {
		String sql = "insert into JECN_USER_LOGIN_LOG (ID,USER_ID,SESSION_ID,LOGIN_TIME,LOGIN_OUT_TYPE,LOGIN_TYPE) values(?,?,?,?,-1,?)";
		return this.execteNative(sql, JecnCommon.getUUID(),userId,sessionId,new Date(),loginType);
	}

	@Override
	public List<String> getUserLoginLogSessionID(int loginType) {
		String sql = "select session_id from jecn_user_login_log where LOGIN_TYPE = ?";
		return this.listNativeSql(sql,loginType);
	}

	@Override
	public int updateLogiOutTime() {
		String sql = "update jecn_user_login_log set login_out_time = ? , login_out_type = 2 where login_out_time is null";
		return this.execteNative(sql, new Date());
	}

	@Override
	public void updateUserLoginLog(String sessionId, int loginOutType,
			int loginType) {
		Date date = new Date();
		String sql = "";
		if(loginType == 0){
			/**loginType :0：浏览端登录，1：设计器登录。
			 *  浏览端登录时，sessionId 为 JECN_USER_LOGIN_LOG中的SESSION_ID
			 *  设计器登录时：sessionId 为JECN_USER_LOGIN_LOG中的user_id
			 */
			sql = "update JECN_USER_LOGIN_LOG set LOGIN_OUT_TIME = ?, LOGIN_OUT_TYPE = ? where SESSION_ID = ? and LOGIN_TYPE = ?";
		}else if(loginType == 1){
			sql = "update JECN_USER_LOGIN_LOG set LOGIN_OUT_TIME = ?, LOGIN_OUT_TYPE = ? where user_id = ?  and login_out_time is null  and LOGIN_TYPE = ?";
		}
		this.execteNative(sql,date,loginOutType,sessionId,loginType);
	}

}
