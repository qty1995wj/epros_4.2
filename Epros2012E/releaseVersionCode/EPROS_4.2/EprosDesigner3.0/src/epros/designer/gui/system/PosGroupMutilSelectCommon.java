package epros.designer.gui.system;

import java.awt.Insets;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.popedom.positiongroup.choose.PosGropChooseDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

public class PosGroupMutilSelectCommon extends MutilSelectCommon {

	public PosGroupMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest) {
		super(rows, infoPanel, insets, list, JecnProperties.getValue("posCroupCompetenceC"), jecnDialog, isRequest,
				true);
	}

	public PosGroupMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest, boolean isDownLoad) {
		super(rows, infoPanel, insets, list, JecnProperties.getValue("posCroupCompetenceC"), jecnDialog, isRequest,
				isDownLoad);
	}

	public PosGroupMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			String labName, JecnDialog jecnDialog, boolean isRequest) {
		super(rows, infoPanel, insets, list, labName, jecnDialog, isRequest);
	}

	public PosGroupMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest, boolean isDownLoad, String name) {
		super(rows, infoPanel, insets, list, name, jecnDialog, isRequest, isDownLoad);
	}

	@Override
	protected JecnSelectChooseDialog getJecnSelectChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		return new PosGropChooseDialog(list, jecnDialog, true);
	}

}
