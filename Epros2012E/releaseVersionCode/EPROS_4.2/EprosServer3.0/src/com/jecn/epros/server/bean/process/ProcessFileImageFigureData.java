package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程元素数据对象
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-12-3 时间：下午02:25:24
 */
public class ProcessFileImageFigureData implements Serializable {
	/** 元素 */
	private JecnFlowStructureImageT jecnFlowStructureImageT;
	/** 相关链接集合 */
	private List<JecnFigureFileTBean> listJecnFigureFileTBean;

	public JecnFlowStructureImageT getJecnFlowStructureImageT() {
		return jecnFlowStructureImageT;
	}

	public void setJecnFlowStructureImageT(
			JecnFlowStructureImageT jecnFlowStructureImageT) {
		this.jecnFlowStructureImageT = jecnFlowStructureImageT;
	}

	public List<JecnFigureFileTBean> getListJecnFigureFileTBean() {
		if (listJecnFigureFileTBean == null) {
			listJecnFigureFileTBean = new ArrayList<JecnFigureFileTBean>();
		}
		return listJecnFigureFileTBean;
	}

	public void setListJecnFigureFileTBean(
			List<JecnFigureFileTBean> listJecnFigureFileTBean) {
		this.listJecnFigureFileTBean = listJecnFigureFileTBean;
	}
}
