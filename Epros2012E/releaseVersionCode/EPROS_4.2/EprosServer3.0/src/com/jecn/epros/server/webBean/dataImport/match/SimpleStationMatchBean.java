package com.jecn.epros.server.webBean.dataImport.match;

public class SimpleStationMatchBean {
	/** 关联表主键ID */
	private Long refId;
	/** 流程中岗位名称 */
	private String flowPositionName;
	/** HR系统中岗位名称 */
	private String hRPositionName;
	/** 流程中岗位ID */
	private Long flowPositionId;
	/** HR系统中岗位ID */
	private Long hRPositionId;

	public Long getRefId() {
		return refId;
	}

	public void setRefId(Long refId) {
		this.refId = refId;
	}

	public String getFlowPositionName() {
		return flowPositionName;
	}

	public void setFlowPositionName(String flowPositionName) {
		this.flowPositionName = flowPositionName;
	}

	public String getHRPositionName() {
		return hRPositionName;
	}

	public void setHRPositionName(String positionName) {
		hRPositionName = positionName;
	}

	public Long getFlowPositionId() {
		return flowPositionId;
	}

	public void setFlowPositionId(Long flowPositionId) {
		this.flowPositionId = flowPositionId;
	}

	public Long getHRPositionId() {
		return hRPositionId;
	}

	public void setHRPositionId(Long positionId) {
		hRPositionId = positionId;
	}
}
