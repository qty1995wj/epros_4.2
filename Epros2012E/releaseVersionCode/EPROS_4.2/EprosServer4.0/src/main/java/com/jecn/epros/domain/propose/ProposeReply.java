package com.jecn.epros.domain.propose;

import io.swagger.annotations.ApiModelProperty;

/**
 * 采纳拒绝
 *
 * @author user
 */
public class ProposeReply {
    @ApiModelProperty(value = "合理化建议id")
    private String proposeId;
    @ApiModelProperty(value = "回复的消息")
    private String strContent;

    public String getProposeId() {
        return proposeId;
    }

    public void setProposeId(String proposeId) {
        this.proposeId = proposeId;
    }

    public String getStrContent() {
        return strContent;
    }

    public void setStrContent(String strContent) {
        this.strContent = strContent;
    }

}
