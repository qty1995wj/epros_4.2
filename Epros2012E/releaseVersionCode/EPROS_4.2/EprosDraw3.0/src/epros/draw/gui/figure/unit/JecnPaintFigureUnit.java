package epros.draw.gui.figure.unit;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Locale;

import javax.swing.JPanel;

import epros.draw.constant.JecnToolBoxConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.AuthTipPanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.CommonLine;
import epros.draw.gui.line.shape.ManhattanLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

public class JecnPaintFigureUnit {

	/**
	 * 活动说明自动换行
	 * 
	 * @param string
	 * @return
	 */
	public static StringBuffer getsubstring(String string) {
		String[] str = string.split("\n");
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < str.length; i++) {
			int s = 0;
			int stringLength = 50;
			if (str[i].length() == 0) {
				s = str[i].length() / stringLength;
			} else {
				s = str[i].length() / stringLength + 1;
			}
			for (int j = 0; j < s; j++) {
				if (str[i].length() < stringLength) {
					string = str[i] + "\n";
				} else {
					string = str[i].substring(0, stringLength) + "\n";
					str[i] = str[i].substring(stringLength);
				}
				string = string.replaceAll("\n", "<br>");
				stringBuffer.append(string);
			}
		}
		return stringBuffer;
	}

	/**
	 * 
	 * 添加四个编辑点
	 * 
	 * @param curFigure
	 */
	public static void addConnectPort(JecnFigureDataCloneable figureCloneble, JecnBaseFigurePanel curFigure) {
		if (curFigure.getEditLeftPortPanel() == null) {
			return;
		}
		// 获取编辑点panel大小
		int resizePortWidth = curFigure.getEditLeftPortPanel().getWidth();
		int resizePortHeight = curFigure.getEditLeftPortPanel().getHeight();
		// 设置连接线相关的四个连接点getEditLeftPortPanel()：左，getEditTopPortPanel()：上，getEditRightPortPanel()：右，getEditBottomPortPanel()：下
		curFigure.getEditLeftPortPanel().setLocation(
				(int) (figureCloneble.getEditLeftPoint().getX() - resizePortWidth / 2),
				(int) (figureCloneble.getEditLeftPoint().getY() - resizePortHeight / 2));// 左
		curFigure.getEditTopPortPanel().setLocation(
				(int) (figureCloneble.getEditTopPoint().getX() - resizePortWidth / 2),
				(int) (figureCloneble.getEditTopPoint().getY() - resizePortHeight / 2));// 中
		curFigure.getEditRightPortPanel().setLocation(
				(int) (figureCloneble.getEditRightPoint().getX() - resizePortWidth / 2),
				(int) (figureCloneble.getEditRightPoint().getY() - resizePortHeight / 2));
		curFigure.getEditBottomPortPanel().setLocation(
				(int) (figureCloneble.getEditBottomPoint().getX() - resizePortWidth / 2),
				(int) (figureCloneble.getEditBottomPoint().getY() - resizePortHeight / 2));
	}

	/**
	 * 添加图形的8个显示点
	 * 
	 * @param curFigure
	 */
	public static void addResetPort(JecnFigureDataCloneable figureCloneble, JecnBaseFigurePanel curFigure) {
		if (JecnDrawMainPanel.getMainPanel() == null || curFigure.getResizeTopLeftPanel() == null) {
			return;
		}
		// 获取当前放大缩小倍数下图形位置点和坐标点、编辑点
		figureCloneble.getZoomFigureDataProcess(JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
		// 设置显示图形时的8个连接点
		// 显示点图形的宽度
		int resizeWidth = curFigure.getResizeTopLeftPanel().getWidth();
		// 显示点图形的高度
		int resizeHeight = curFigure.getResizeTopLeftPanel().getHeight();
		// 图形的位置点X坐标
		int figurePosX = figureCloneble.getX();
		// 图形的位置点Y坐标
		int figurePosY = figureCloneble.getY();
		// 图形当前面板放大倍数下的宽度
		int figureWidth = figureCloneble.getWidth();
		// 图形当前面板放大倍数下的高度
		int figureHeight = figureCloneble.getHeight();

		if (curFigure instanceof ModelFigure) {
			ModelFigure modelFigure = (ModelFigure) curFigure;
			if (modelFigure.isChick()) {// 鼠标是否点击编辑区域
				figureWidth = modelFigure.getZoomDividingX();
			}
		}
		// 上左
		curFigure.getResizeTopLeftPanel().setLocation(figurePosX - resizeWidth, figurePosY - resizeHeight);
		// 上中
		curFigure.getResizeTopCenterPanel().setLocation(figurePosX + figureWidth / 2 - resizeWidth / 2,
				figurePosY - resizeHeight);
		// 上右
		curFigure.getResizeTopRightPanel().setLocation(figurePosX + figureWidth, figurePosY - resizeHeight);
		// 右中
		curFigure.getResizeRightCenterPanel().setLocation(figurePosX + figureWidth,
				figurePosY + figureHeight / 2 - resizeHeight / 2);
		// 下右
		curFigure.getResizeBottomRightPanel().setLocation(figurePosX + figureWidth, figurePosY + figureHeight);
		// 下中
		curFigure.getResizeBottomCenterPanel().setLocation(figurePosX + figureWidth / 2 - resizeWidth / 2,
				figurePosY + figureHeight);
		// 下左
		curFigure.getResizeBottomLeftPanel().setLocation(figurePosX - resizeWidth, figurePosY + figureHeight);
		// 左中
		curFigure.getResizeLeftCenterPanel().setLocation(figurePosX - resizeWidth,
				figurePosY + figureHeight / 2 - resizeHeight / 2);
	}

	/**
	 * 添加图形的8个显示点
	 * 
	 * @param curFigure
	 */
	public static void reSetCurFigure(JecnBaseFigurePanel curFigure) {
		if (JecnDrawMainPanel.getMainPanel() == null || curFigure.getResizeTopLeftPanel() == null) {
			return;
		}
		// 设置显示图形时的8个连接点
		// 显示点图形的宽度
		int resizeWidth = curFigure.getResizeTopLeftPanel().getWidth();
		// 显示点图形的高度
		int resizeHeight = curFigure.getResizeTopLeftPanel().getHeight();
		// 图形的位置点X坐标
		int figurePosX = curFigure.getX();
		// 图形的位置点Y坐标
		int figurePosY = curFigure.getY();
		// 图形当前面板放大倍数下的宽度
		int figureWidth = curFigure.getWidth();
		// 图形当前面板放大倍数下的高度
		int figureHeight = curFigure.getHeight();

		if (curFigure instanceof ModelFigure) {
			ModelFigure modelFigure = (ModelFigure) curFigure;
			if (modelFigure.isChick()) {// 鼠标是否点击编辑区域
				figureWidth = modelFigure.getZoomDividingX();
			}
		}
		// 上左
		curFigure.getResizeTopLeftPanel().setLocation(figurePosX - resizeWidth, figurePosY - resizeHeight);
		// 上中
		curFigure.getResizeTopCenterPanel().setLocation(figurePosX + figureWidth / 2 - resizeWidth / 2,
				figurePosY - resizeHeight);
		// 上右
		curFigure.getResizeTopRightPanel().setLocation(figurePosX + figureWidth, figurePosY - resizeHeight);
		// 右中
		curFigure.getResizeRightCenterPanel().setLocation(figurePosX + figureWidth,
				figurePosY + figureHeight / 2 - resizeHeight / 2);
		// 下右
		curFigure.getResizeBottomRightPanel().setLocation(figurePosX + figureWidth, figurePosY + figureHeight);
		// 下中
		curFigure.getResizeBottomCenterPanel().setLocation(figurePosX + figureWidth / 2 - resizeWidth / 2,
				figurePosY + figureHeight);
		// 下左
		curFigure.getResizeBottomLeftPanel().setLocation(figurePosX - resizeWidth, figurePosY + figureHeight);
		// 左中
		curFigure.getResizeLeftCenterPanel().setLocation(figurePosX - resizeWidth,
				figurePosY + figureHeight / 2 - resizeHeight / 2);
	}

	/**
	 * 隐藏图形的4个编辑点
	 * 
	 * @param curFigure
	 */
	public static void hideConnectPort(JecnBaseFigurePanel curFigure) {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null
				|| curFigure.getResizeTopLeftPanel() == null) {
			return;
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().remove(curFigure.getEditLeftPortPanel());
		JecnDrawMainPanel.getMainPanel().getWorkflow().remove(curFigure.getEditTopPortPanel());
		JecnDrawMainPanel.getMainPanel().getWorkflow().remove(curFigure.getEditRightPortPanel());
		JecnDrawMainPanel.getMainPanel().getWorkflow().remove(curFigure.getEditBottomPortPanel());
	}

	/**
	 * 显示所有图形编辑点
	 * 
	 */
	public static void showAllConnectPort() {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane.getFlowMapData().isHiddenLineEditPoint()) {
			return;
		}
		List<JecnBaseFlowElementPanel> listFlowElement = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();
		for (int i = 0; i < listFlowElement.size(); i++) {
			JecnBaseFlowElementPanel temcurFigure = listFlowElement.get(i);
			// 判断元素是否为图形
			if (!getFigurePanel(temcurFigure.getFlowElementData().getMapElemType())) {
				// 不是图形跳出执行下次循环
				continue;
			}

			// 角色，注释框，自由文本框、图标框等不需要添加编辑点
			if (temcurFigure.getFlowElementData().getEditPoint() && temcurFigure instanceof JecnBaseFigurePanel) {// 是否显示图形编辑点
				showConnectPort((JecnBaseFigurePanel) temcurFigure);
			}
		}
	}

	/**
	 * 显示指定元素 编辑点
	 * 
	 * @param showEditPortList
	 */
	public static void showAllConnectPort(List<JecnBaseFigurePanel> showEditPortList) {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (!desktopPane.getFlowMapData().isHiddenLineEditPoint()) {
			return;
		}
		for (int i = 0; i < showEditPortList.size(); i++) {
			JecnBaseFigurePanel temcurFigure = showEditPortList.get(i);

			if (temcurFigure instanceof ModelFigure) {
				continue;
			}
			// 判断元素是否为图形
			if (!getFigurePanel(temcurFigure.getFlowElementData().getMapElemType())) {
				// 不是图形跳出执行下次循环
				continue;
			}

			// 角色，注释框，自由文本框、图标框等不需要添加编辑点
			if (temcurFigure.getFlowElementData().getEditPoint()) {// 是否显示图形编辑点
				showConnectPort(temcurFigure);
			}
		}
	}

	/**
	 * 隐藏所有图形的4个编辑点
	 * 
	 */
	public static void hideEditPortByList(List<JecnBaseFigurePanel> showEditPortList) {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		for (int i = 0; i < showEditPortList.size(); i++) {
			JecnBaseFigurePanel temcurFigure = showEditPortList.get(i);
			// 判断元素是否为图形
			if (!(temcurFigure instanceof JecnBaseFigurePanel)
					|| !getFigurePanel(temcurFigure.getFlowElementData().getMapElemType())) {
				// 不是图形跳出执行下次循环
				continue;
			}
			if (temcurFigure.getFlowElementData().getEditPoint()) {// 是否显示图形编辑点
				hideConnectPort(temcurFigure);
			}
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getShowEditPointFigures().clear();
		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}

	/**
	 * 隐藏所有图形的4个编辑点
	 * 
	 */
	public static void hideAllConnectPort() {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		List<JecnBaseFlowElementPanel> listFlowElement = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();
		for (int i = 0; i < listFlowElement.size(); i++) {
			JecnBaseFlowElementPanel temcurFigure = listFlowElement.get(i);
			// 判断元素是否为图形
			if (!(temcurFigure instanceof JecnBaseFigurePanel)
					|| !getFigurePanel(temcurFigure.getFlowElementData().getMapElemType())) {
				// 不是图形跳出执行下次循环
				continue;
			}
			if (temcurFigure.getFlowElementData().getEditPoint()) {// 是否显示图形编辑点
				hideConnectPort((JecnBaseFigurePanel) temcurFigure);
			}
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}

	/**
	 * 根据给定标识判断该标识是否为图形
	 * 
	 * @param mapElemType
	 * @return boolean True:图形；false：不是图形
	 */
	public static boolean getFigurePanel(MapElemType mapElemType) {
		// 所有能添加到面板的图形类型
		MapElemType[] figrueType = JecnToolBoxConstant.figrueType;
		for (MapElemType elemType : figrueType) {
			if (elemType.equals(mapElemType)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 显示图形的4个编辑点
	 * 
	 * @param curFigure
	 */
	public static void showConnectPort(JecnBaseFigurePanel curFigure) {
		// 设置面板显示4个编辑点
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null
				|| curFigure.getEditLeftPortPanel() == null) {
			return;
		}

		JecnDrawMainPanel.getMainPanel().getWorkflow().add(curFigure.getEditLeftPortPanel());
		JecnDrawMainPanel.getMainPanel().getWorkflow().add(curFigure.getEditTopPortPanel());
		JecnDrawMainPanel.getMainPanel().getWorkflow().add(curFigure.getEditRightPortPanel());
		JecnDrawMainPanel.getMainPanel().getWorkflow().add(curFigure.getEditBottomPortPanel());
		// 设置图形编辑点层级
		addEditPortLayer(JecnDrawMainPanel.getMainPanel().getWorkflow(), curFigure);

		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}

	/**
	 * 设置图形编辑点层级
	 * 
	 * @param workflow
	 * @param curFigure
	 */
	public static void addEditPortLayer(JecnDrawDesktopPane workflow, JecnBaseFigurePanel curFigure) {
		if (curFigure.getEditLeftPortPanel() == null) {
			return;
		}
		int curZorder = curFigure.getFlowElementData().getZOrderIndex() + 1;
		// 设置编辑点层级
		workflow.setLayer(curFigure.getEditLeftPortPanel(), curZorder);
		workflow.setLayer(curFigure.getEditTopPortPanel(), curZorder);
		workflow.setLayer(curFigure.getEditRightPortPanel(), curZorder);
		workflow.setLayer(curFigure.getEditBottomPortPanel(), curZorder);
	}

	/**
	 * 
	 * 
	 * 显示选中点
	 * 
	 * @param baseFigurePanel
	 */
	public static void showResizeHandles(JecnBaseFigurePanel baseFigurePanel) {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null
				|| baseFigurePanel.getResizeLeftCenterPanel() == null) {
			return;
		}
		hideResizeHandles(baseFigurePanel);
		// 角色移动 TODO
		// 获取当前面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (baseFigurePanel instanceof ModelFigure) {
			// 鼠标是否点击编辑区域
			addResetPort(baseFigurePanel.getZoomJecnFigureDataCloneable(), baseFigurePanel);
		}
		// 8个方向的点
		workflow.add(baseFigurePanel.getResizeLeftCenterPanel());
		workflow.add(baseFigurePanel.getResizeTopCenterPanel());
		workflow.add(baseFigurePanel.getResizeTopRightPanel());
		workflow.add(baseFigurePanel.getResizeTopLeftPanel());
		workflow.add(baseFigurePanel.getResizeRightCenterPanel());
		workflow.add(baseFigurePanel.getResizeBottomCenterPanel());
		workflow.add(baseFigurePanel.getResizeBottomLeftPanel());
		workflow.add(baseFigurePanel.getResizeBottomRightPanel());

		if (baseFigurePanel instanceof CommentText) {
			CommentText commentText = (CommentText) baseFigurePanel;
			commentText.commentLine.showLineResizeHandles();
		}
		// 设置图形8个方向点的层级
		addResizeToWorkFlow(JecnDrawMainPanel.getMainPanel().getWorkflow(), baseFigurePanel);
		workflow.updateUI();
	}

	/**
	 * 设置图形8个方向点的层级
	 * 
	 * @param workflow
	 * @param baseFigurePanel
	 */
	public static void addResizeToWorkFlow(JecnDrawDesktopPane workflow, JecnBaseFigurePanel baseFigurePanel) {
		if (workflow == null || baseFigurePanel.getResizeLeftCenterPanel() == null) {
			return;
		}
		int curZorder = workflow.getMaxZOrderIndex() + 2;
		// 设置显示点相对图形的层级
		workflow.setLayer(baseFigurePanel.getResizeLeftCenterPanel(), curZorder);
		workflow.setLayer(baseFigurePanel.getResizeTopLeftPanel(), curZorder);
		workflow.setLayer(baseFigurePanel.getResizeTopCenterPanel(), curZorder);
		workflow.setLayer(baseFigurePanel.getResizeTopRightPanel(), curZorder);
		workflow.setLayer(baseFigurePanel.getResizeRightCenterPanel(), curZorder);
		workflow.setLayer(baseFigurePanel.getResizeBottomRightPanel(), curZorder);
		workflow.setLayer(baseFigurePanel.getResizeBottomCenterPanel(), curZorder);
		workflow.setLayer(baseFigurePanel.getResizeBottomLeftPanel(), curZorder);
	}

	/**
	 * 
	 * 隐藏选中点
	 * 
	 * @param baseFigurePanel
	 */
	public static void hideResizeHandles(JecnBaseFigurePanel baseFigurePanel) {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null
				|| baseFigurePanel.getResizeLeftCenterPanel() == null) {
			return;
		}
		// 获取当前面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		workflow.remove(baseFigurePanel.getResizeLeftCenterPanel());
		workflow.remove(baseFigurePanel.getResizeTopLeftPanel());
		workflow.remove(baseFigurePanel.getResizeTopCenterPanel());
		workflow.remove(baseFigurePanel.getResizeTopRightPanel());
		workflow.remove(baseFigurePanel.getResizeRightCenterPanel());
		workflow.remove(baseFigurePanel.getResizeBottomRightPanel());
		workflow.remove(baseFigurePanel.getResizeBottomCenterPanel());
		workflow.remove(baseFigurePanel.getResizeBottomLeftPanel());
		if (baseFigurePanel instanceof CommentText) {
			CommentText commentText = (CommentText) baseFigurePanel;
			commentText.commentLine.hideLineResizeHandles();
		}
		workflow.updateUI();
	}

	/**
	 * 背景渐变色 颜色渐变填充
	 * 
	 * @param panel
	 *            需要填充的组件
	 * @param g2d
	 *            画笔
	 * @param fillColorChangType
	 *            FillColorChangType渐变方向
	 * @param fillColor填充色
	 * @param changeColor
	 *            改变色
	 */
	public static void paintBackColor(JPanel panel, Graphics2D g2d, FillColorChangType fillColorChangType,
			Color fillColor, Color changeColor) {
		// topTilt, // 1：向上倾斜 bottom, // 2：向下倾斜 vertical, // 3：垂直horizontal
		// 渐变色 第一种颜色
		Color color1 = null;
		// 渐变色 第二种颜色
		Color color2 = null;
		int x = 0;
		int y = 0;
		switch (fillColorChangType) {
		case topTilt:// 向上倾斜
			color1 = fillColor;
			color2 = changeColor;
			x = panel.getWidth();
			y = panel.getHeight();
			break;
		case bottom:// 向下倾斜
			color2 = fillColor;
			color1 = changeColor;
			x = panel.getWidth();
			y = panel.getHeight();
			break;
		case vertical:// 垂直
			color1 = fillColor;
			color2 = changeColor;
			x = panel.getWidth();
			y = 0;

			break;
		case horizontal:// 水平
			color1 = fillColor;
			color2 = changeColor;
			x = 0;
			y = panel.getHeight();
			break;
		default:
			break;
		}
		// 如果双色时存在颜色为空,取面板默认色
		if (color1 == null) {
			color1 = JecnUIUtil.getDefaultBackgroundColor();
		}
		if (color2 == null) {
			color2 = JecnUIUtil.getDefaultBackgroundColor();
		}
		g2d.setPaint(new GradientPaint(0, 0, color1, x, y, color2));
	}

	public static void showToOrFromLinkActive(JecnBaseFlowElementPanel flowElementPanel) {
		// 显示TO元素的单击面板
		JecnDrawMainPanel.getMainPanel().getWorkflow().getAidCompCollection().getRelatedActivePanel().update(
				flowElementPanel);
	}

	/**
	 * 清除to和from快捷设置活动编号面板
	 */
	public static void hiddenToOrFromLinkActive() {
		JecnDrawMainPanel.getMainPanel().getWorkflow().getAidCompCollection().getRelatedActivePanel()
				.hiddenReleateActivePanel();
	}

	/**
	 * 流程图，没有编辑流程图权限时，提示信息
	 */
	public static void addOpenTipPanel() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		String titleTip = null;

		if (desktopPane.getFlowMapData().isAuthSave() == 5) {
			titleTip = JecnResourceUtil.getJecnResourceUtil().getValue("thisProess") + " "
					+ desktopPane.getFlowMapData().getOccupierName() + " "
					+ JecnResourceUtil.getJecnResourceUtil().getValue("thisProcessEdit");
		} else if (desktopPane.getFlowMapData().isAuthSave() == 2) {
			// 本流程处于任务中，以只读方式打开！
			titleTip = JecnResourceUtil.getJecnResourceUtil().getValue("thisProcessTask");
		} else if (desktopPane.getFlowMapData().isAuthSave() == 4) {
			// 只读模式
			titleTip = JecnResourceUtil.getJecnResourceUtil().getValue("readOnlyOpen");
		} else if (JecnDesignerProcess.getDesignerProcess().isEditFlowMap()
				&& (desktopPane.getFlowMapData() != null && (desktopPane.getFlowMapData().getMapType() == MapType.totalMap || desktopPane
						.getFlowMapData().getMapType() == MapType.totalMapRelation))) {
			// 您没有编辑权限，以只读方式打开！
			titleTip = JecnResourceUtil.getJecnResourceUtil().getValue("thisEdit");
			desktopPane.getFlowMapData().setAuthSave(3);
		} else if (desktopPane.getFlowMapData().isAuthSave() == 1) {
			if (JecnDesignerProcess.getDesignerProcess().isShowWorkflowEdit()) {
				titleTip = JecnResourceUtil.getJecnResourceUtil().getValue("editMode");
			} else {
				return;
			}
		} else if (desktopPane.getFlowMapData().isAuthSave() == 3) {
			// 您没有编辑权限，以只读方式打开！
			titleTip = JecnResourceUtil.getJecnResourceUtil().getValue("thisEdit");
		}
		// 添加面板显示
		AuthTipPanel tipPanel = new AuthTipPanel();
		if (desktopPane.getFlowMapData().isAuthSave() == 4) {
			tipPanel.initEditButton();
		}
		String pText = titleTip;
		tipPanel.setText(pText);
		if (desktopPane.getFlowMapData().isAuthSave() == 1 && !JecnDesignerProcess.getDesignerProcess().isEditFlowMap()) {
			if (JecnResourceUtil.getLocale() == Locale.CHINESE) {
				tipPanel.setSize(100, 35);
			} else {
				tipPanel.setSize(150, 35);
			}
		} else {
			if (JecnResourceUtil.getLocale() == Locale.CHINESE) {
				tipPanel.setSize(400, 35);
			} else {
				tipPanel.setSize(600, 35);
			}
		}
		tipPanel.setBackground(Color.red);
		tipPanel.setFont(new Font("宋体", 1, 16));
		tipPanel.setLocation(2, 2);
		tipPanel.repaint();
		desktopPane.add(tipPanel);
		desktopPane.setTipPanel(tipPanel);
	}

	public static JecnBaseManhattanLinePanel getJecnBaseManhattanLinePanel(String lineFlag,
			JecnManhattanLineData manhattanLineData) {
		if ("ManhattanLine".equals(lineFlag)) {
			return new ManhattanLine(manhattanLineData);
		} else {
			return new CommonLine(manhattanLineData);
		}
	}

	/**
	 * 拖动连接线 - 是否显示编辑，业务处理
	 * 
	 * @param e
	 *            鼠标事件 ，相对面板的坐标事件
	 * @param figurePanel
	 *            当前连接线开始图形
	 */
	public static void selectMoveInDesk(MouseEvent e, JecnBaseFigurePanel figurePanel) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (!desktopPane.getFlowMapData().isHiddenLineEditPoint()) {
			return;
		}

		if (figurePanel == null) {
			JecnPaintFigureUnit.hideEditPortByList(desktopPane.getShowEditPointFigures());
		}

		// 隐藏历史选中点
		List<JecnBaseFigurePanel> showEditPortList = JecnWorkflowUtil.getEditPortList(e.getPoint());
		if (showEditPortList.size() > 0) {
			JecnPaintFigureUnit.showAllConnectPort(showEditPortList);
			desktopPane.getShowEditPointFigures().addAll(showEditPortList);
		}
	}
}
