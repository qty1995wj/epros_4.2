package com.jecn.epros.server.download.wordxml.libang;

import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 立邦操作模版下载
 * 
 * @author admin
 * 
 */
public class LBProcessModel extends ProcessFileModel {
	/*** 段落行距单倍倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);

	/*** 段落行距固定值16磅值 *****/
	private ParagraphLineRule vLine2 = new ParagraphLineRule(16F, LineRuleType.EXACT);

	public LBProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setFlowChartScaleValue(6.0F);
		setDocStyle("、", textTitleStyle());
		getDocProperty().setNewTblWidth(17.49F);
		
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		// sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		String version = "";
		String pubDate = "";
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (historys != null && historys.size() > 0) {
			version = historys.get(0).getVersionId();
			pubDate = JecnUtil.DateToStr(historys.get(0).getPublishDate(), "yyyy-MM-dd");
		}

		ParagraphBean pBean = new ParagraphBean("宋体", Constants.xiaoer, true);
		pBean.setAlign(Align.left);
		Header hdr = sect.createHeader(type);
		// 公司 logo 图标
		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(1.77F, 1.11F);
		Paragraph p = new Paragraph(logo,pBean);
		p.appendTextRun("立邦中国集团");
		hdr.addParagraph(p);
		pBean = new ParagraphBean("宋体", Constants.shihao, false);
		pBean.setpStyle(PStyle.AF1);
		pBean.setInd(0f, 0f, 0f, 10.3f);
		hdr.addParagraph(new Paragraph("文件编号：" + nullToEmpty(flowInputNum), pBean));
		hdr.addParagraph(new Paragraph("版本信息：" + nullToEmpty(version), pBean));
		hdr.addParagraph(new Paragraph("实施日期：" + nullToEmpty(pubDate), pBean));
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {

		ParagraphBean pBean = new ParagraphBean("宋体", Constants.shihao, false);
		pBean.setAlign(Align.left);
		Footer ftr = sect.createFooter(type);
		// 一体化体系文件 第1页，共5页
		Paragraph p = ftr.createParagraph("一体化体系文件                                                   第", pBean);
		p.appendCurPageRun();
		p.appendTextRun("页，共");
		p.appendTotalPagesRun();
		p.appendTextRun("页");
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoyi, true);
		firstSect.createParagraph(" ", pBean);
		firstSect.createParagraph(nullToEmpty(processDownloadBean.getFlowName()), pBean);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(1.2F, 1.76F, 1.2F, 1.76F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {

	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, false);
		tblContentStyle.setSpace(0f, 0f, vLine2);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0, 0, 0);
		// 表格左缩进0.01厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		/*** 初始化标题内容段落属性 ****/
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, "宋体", Constants.xiaosi, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "宋体", Constants.xiaosi, true);
		textTitleStyle.setSpace(0F, 0F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
