package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnControlPointT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 活动控制点
 * 
 * @author CHEHUANBO
 */
public class ActiveControlPointTablPanel extends JecnPanel {
	private Logger log = Logger.getLogger(ActiveControlPointTablPanel.class);
	/** 元素数据 */
	private JecnActiveData activeData;

	/** 所在的滚动面板 */
	private JScrollPane activeScrollPane = new JScrollPane();

	/** 表单 */
	private JTable jecnTable = null;

	/** 主面板 */
	private JecnPanel mainPanel = null;

	/** 按钮面板 */
	private JecnPanel buttonPanel = null;

	/** 添加按钮 */
	private JButton addButton = null;

	/** 编辑按钮 */
	private JButton editButton = null;

	/** 删除按钮 */
	private JButton deleteBuffon = null;
	private List<JecnControlPointT> jecnControlPointTList = new ArrayList<JecnControlPointT>();

	public ActiveControlPointTablPanel(JecnActiveData activeData) {
		this.activeData = activeData;
		initComs();
		initLayout();
	}

	public void initInfoPanelData(JecnActiveData activeData) {
		this.activeData = activeData;
		removeAllTable();
		Vector<Vector<Object>> fileData = getContent();
		for (Vector<Object> v : fileData) {
			((DefaultTableModel) jecnTable.getModel()).addRow(v);
		}
	}

	private void removeAllTable() {
		jecnControlPointTList = new ArrayList<JecnControlPointT>();
		for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(index);
		}
	}

	/**
	 * 组件初始化
	 * 
	 */
	private void initComs() {
		Dimension dimension = null;
		if (JecnDesignerCommon.isShowEleAttribute()) {
			mainPanel = new JecnPanel();
			// 设置表格大小
			dimension = new Dimension(550, 180);
		} else {
			mainPanel = new JecnPanel(280, 400);
			// 设置表格大小
			dimension = new Dimension(550, 380);
		}

		buttonPanel = new JecnPanel();
		// 添加
		addButton = new JButton(JecnProperties.getValue("addBtn"));
		// 关联
		editButton = new JButton(JecnProperties.getValue("editBtn"));
		// 删除
		deleteBuffon = new JButton(JecnProperties.getValue("deleteBtn"));
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		addButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		editButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		deleteBuffon.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		jecnTable = new ActiveTable();
		// 设置默认背景色
		jecnTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		jecnTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// 列不可拖动
		jecnTable.getTableHeader().setReorderingAllowed(false);
		// 设置行高
		jecnTable.setRowHeight(20);
		// 隐藏RiskId列
		TableColumnModel columnModel = jecnTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(8);
		// 默认隐藏
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);

		activeScrollPane.setPreferredSize(dimension);
		activeScrollPane.setMaximumSize(dimension);
		activeScrollPane.setMinimumSize(dimension);
		activeScrollPane.setViewportView(jecnTable);
		activeScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 背景色
		activeScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加按钮
		jecnTable.getColumnModel().getColumn(7).setCellRenderer(new EditJLabel());

	}

	/***
	 * 初始化布局
	 * 
	 * @author CHEHUANBO
	 */
	private void initLayout() {
		// 标签间距
		Insets insetsTable = new Insets(1, 1, 0, 1);
		// 添加组件，设置组件布局
		Insets insetsPanel = new Insets(0, 1, 3, 1);
		// 新建控制点
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insetsTable, 0, 0);
		mainPanel.add(activeScrollPane, c);
		// 控制点面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsPanel, 0, 0);
		mainPanel.add(buttonPanel, c);
		// 按钮面板
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(addButton); // 添加按钮
		buttonPanel.add(editButton); // 编辑按钮
		buttonPanel.add(deleteBuffon);// 删除按钮

		// 添加主面板
		Insets insets1 = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets1,
				0, 0);
		this.add(mainPanel, c);

		/**
		 * 添加事件
		 * 
		 */
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addButtonListener();
			}
		});

		/**
		 * 编辑事件
		 * 
		 */
		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editButtonListener();
			}
		});

		/**
		 * 删除事件
		 * 
		 */
		deleteBuffon.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// 获取选中的行数
				int[] selectRows = jecnTable.getSelectedRows();
				// 判断是否选中行数
				if (selectRows.length == 0) {
					// 请选择一行!
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
					return;
				}
				// 删除控制点
				for (int i = selectRows.length - 1; i >= 0; i--) {
					((DefaultTableModel) jecnTable.getModel()).removeRow(selectRows[i]);
					jecnControlPointTList.remove(selectRows[i]);
				}
			}

		});

	}

	/**
	 * 添加控制点
	 * 
	 */
	private void addButtonListener() {
		// 调用添加对话框 参数0代表添加控制点
		AddOrEditPointDialog addOrEditPointDialog = new AddOrEditPointDialog(new JecnControlPointT(), 0, this, -1);
		addOrEditPointDialog.setVisible(true);
	}

	/**
	 * 编辑控制点
	 * 
	 */
	private void editButtonListener() {
		// 获取选中的行
		int[] selectRows = jecnTable.getSelectedRows();
		if (selectRows.length == 0) {
			// 请选择一行!
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
			return;
		} else if (selectRows.length > 1) {// 判断是否选择了多行
			// 请不要选择多行!
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("onlyOne"));
			return;
		}
		// 将修改后的控制点添加到活动数据集合中
		// 调用添加(编辑)对话框 参数1代表编辑
		AddOrEditPointDialog addOrEditPointDialog = new AddOrEditPointDialog(jecnControlPointTList.get(selectRows[0]),
				1, this, selectRows[0]);
		addOrEditPointDialog.setVisible(true);
	}

	/**
	 * 编辑后刷新
	 * 
	 * @author CHEHUANBO
	 */
	public void editRefushTable(int selectRow, JecnControlPointT controlPointT) {
		// 控制点编号
		jecnTable.setValueAt(controlPointConvert(0, controlPointT), selectRow, 0);
		// 风险点编号
		jecnTable.setValueAt(controlPointConvert(1, controlPointT), selectRow, 1);
		// 控制目标
		jecnTable.setValueAt(controlPointConvert(2, controlPointT), selectRow, 2);
		// 控制方法
		jecnTable.setValueAt(controlPointConvert(3, controlPointT), selectRow, 3);
		// 控制类型
		jecnTable.setValueAt(controlPointConvert(4, controlPointT), selectRow, 4);
		// 控制频率
		jecnTable.setValueAt(controlPointConvert(5, controlPointT), selectRow, 5);
		// 关键控制点
		jecnTable.setValueAt(controlPointConvert(6, controlPointT), selectRow, 6);
		// 风险ID
		jecnTable.setValueAt(controlPointT.getRiskId().toString(), selectRow, 8);
	}

	/**
	 * 
	 * 表格获取数据
	 */
	public void addTableData(JecnControlPointT controlPointT) {
		Vector<Object> row = new Vector<Object>();
		row.add(controlPointConvert(0, controlPointT)); // 控制点编号
		row.add(controlPointConvert(1, controlPointT)); // 风险点编号
		row.add(controlPointConvert(2, controlPointT)); // 控制目标
		row.add(controlPointConvert(3, controlPointT)); // 控制方法
		row.add(controlPointConvert(4, controlPointT));// 控制类型
		row.add(controlPointConvert(5, controlPointT)); // 控制频率
		row.add(controlPointConvert(6, controlPointT)); // 关键控制点
		row.add("");
		row.add(controlPointT.getRiskId()); // 风险ID
		controlPointT.setActiveId(activeData.getFlowElementId());
		jecnControlPointTList.add(controlPointT);
		((ActiveRefTableModel) jecnTable.getModel()).addRow(row);
	}

	/** 选中table 当前行和列 */
	protected int myRow = -10;

	protected int myColumn = -10;

	class ActiveTable extends JTable {
		public ActiveTable() {
			super();
			this.setModel(getTableModel());

			// 选中行事件 true 选中
			// this.setRowSelectionAllowed(false);
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {// 双击
						// 编辑处理
						isClickTableEdit();
					}
				}

				/**
				 * 鼠标移动到表格监听
				 */
				public void mouseExited(MouseEvent e) {
					mouseMovedProcess(e);
				}
			});
			/**
			 * 添加鼠标监听
			 * 
			 */
			this.addMouseMotionListener(new MouseAdapter() {
				public void mouseMoved(MouseEvent e) {
					mouseMovedProcess(e);
				}
			});
		}

		/**
		 * 鼠标进入表格监听
		 * 
		 * @param e
		 */
		protected void mouseMovedProcess(MouseEvent e) {
			int row = this.rowAtPoint(e.getPoint());
			int column = this.columnAtPoint(e.getPoint());
			if (myRow == row && column == myColumn) {// 始终在指定的单元格移动，不执行重绘
				return;
			}
			myRow = this.rowAtPoint(e.getPoint());
			myColumn = this.columnAtPoint(e.getPoint());

			this.revalidate();
			this.repaint();
		}
	}

	/**
	 * 生成table的Model
	 * 
	 * @return
	 */
	private ActiveRefTableModel getTableModel() {
		Vector<Object> title = new Vector<Object>();
		title.add(JecnProperties.getValue("controlPointNum"));// 控制点编号
		title.add(JecnProperties.getValue("riskNum"));// 风险编号
		title.add(JecnProperties.getValue("controlTargetC2"));// 控制目标
		title.add(JecnProperties.getValue("controlPointMethod"));// 控制方法
		title.add(JecnProperties.getValue("controlType"));// 控制类型
		title.add(JecnProperties.getValue("controlFre")); // 控制频率
		title.add(JecnProperties.getValue("activityKeyControlPoint")); // 关键控制点
		title.add(JecnProperties.getValue("operational")); // 风险详细
		title.add(JecnProperties.getValue("riskId")); // 风险ID
		return new ActiveRefTableModel(title, getContent());
	}

	/**
	 * 初始化表格内容
	 * 
	 * @return
	 */
	private Vector<Vector<Object>> getContent() {
		if (activeData == null) {
			return null;
		}
		Vector<Vector<Object>> vs = new Vector<Vector<Object>>();
		try {
			for (JecnControlPointT jecnControlPointT : activeData.getJecnControlPointTList()) {
				Vector<Object> row = new Vector<Object>();
				row.add(controlPointConvert(0, jecnControlPointT)); // 控制点编号
				row.add(controlPointConvert(1, jecnControlPointT)); // 风险点编号
				row.add(controlPointConvert(2, jecnControlPointT)); // 控制目标
				row.add(controlPointConvert(3, jecnControlPointT)); // 控制方法
				row.add(controlPointConvert(4, jecnControlPointT));// 控制类型
				row.add(controlPointConvert(5, jecnControlPointT)); // 控制频率
				row.add(controlPointConvert(6, jecnControlPointT)); // 关键控制点
				row.add(new EditJLabel());
				row.add(jecnControlPointT.getRiskId()); // 关键控制点
				vs.add(row);
				jecnControlPointTList.add(getControlPointT(jecnControlPointT));
			}
		} catch (Exception e) {
			log.error("getContent is error", e);
		}
		return vs;
	}

	private JecnControlPointT getControlPointT(JecnControlPointT jecnControlPointT) {
		JecnControlPointT controlPointT = new JecnControlPointT();
		controlPointT.setId(jecnControlPointT.getId());
		controlPointT.setActiveId(jecnControlPointT.getActiveId());
		controlPointT.setControlCode(jecnControlPointT.getControlCode());
		controlPointT.setCreatePersonId(jecnControlPointT.getCreatePersonId());
		controlPointT.setCreateTime(jecnControlPointT.getCreateTime());
		controlPointT.setFrequency(jecnControlPointT.getFrequency());
		controlPointT.setKeyPoint(jecnControlPointT.getKeyPoint());
		controlPointT.setMethod(jecnControlPointT.getMethod());
		controlPointT.setNote(jecnControlPointT.getNote());
		controlPointT.setRiskId(jecnControlPointT.getRiskId());
		controlPointT.setTargetId(jecnControlPointT.getTargetId());
		controlPointT.setType(jecnControlPointT.getType());
		controlPointT.setUpdatePersonId(jecnControlPointT.getUpdatePersonId());
		controlPointT.setUpdateTime(jecnControlPointT.getUpdateTime());
		controlPointT.setControlTarget(jecnControlPointT.getControlTarget());
		controlPointT.setRiskNum(jecnControlPointT.getRiskNum());
		controlPointT.setIsDelete(jecnControlPointT.getIsDelete());
		return controlPointT;
	}

	class ActiveRefTableModel extends DefaultTableModel {
		public ActiveRefTableModel(Vector<Object> title, Vector<Vector<Object>> content) {
			this.setDataVector(content, title);
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

		public void remoeAll() {
			// 清空
			for (int index = this.getRowCount() - 1; index >= 0; index--) {
				this.removeRow(index);
			}
		}
	}

	/**
	 * 
	 * 渲染
	 * 
	 * @author Administrator
	 * @date： 日期：2013-11-20 时间：上午10:51:08
	 */
	class EditJLabel extends JLabel implements TableCellRenderer {
		// true：需要加色渲染
		private boolean isDef = false;

		EditJLabel() {
			this.setHorizontalAlignment(JLabel.CENTER);
			this.setVerticalAlignment(JLabel.CENTER);
			// 详情
			this.setText(JecnProperties.getValue("linkDetail"));
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			this.isDef = (myRow == row && myColumn == column);
			return this;
		}

		/**
		 * 
		 * 绘制组件
		 * 
		 */
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;

			int w = this.getWidth();
			int h = this.getHeight();
			if (isDef) {
				// 渐变
				g2d.setPaint(new GradientPaint(0, 0, JecnUIUtil.getTopNoColor(), 0, h / 2, JecnUIUtil
						.getButtomNoColor()));
				g2d.fillRect(0, 0, w, h);
				this.setForeground(Color.red);
			} else {
				this.setForeground(Color.black);
			}

			super.paint(g);
		}
	}

	/**
	 * 
	 * 详情双击
	 * 
	 * @return
	 */
	public void isClickTableEdit() {
		// 风险第七列可编辑
		if (jecnTable.getSelectedColumn() == 7) {

			Long riskId = Long.valueOf(jecnTable.getValueAt(jecnTable.getSelectedRow(), 8).toString());
			JecnDesignerCommon.openRisk(riskId);
		}
	}

	/***
	 * 转换控制点显示方式
	 * 
	 * @author CHEHUANBO
	 * @param controlPointT
	 *            : 控制点所有内容 num:Bean的属性顺序代表序号
	 * @return String 返回转换后的显示内容
	 */
	private String controlPointConvert(int num, JecnControlPointT controlPointT) {
		String showContent = null; // 显示的内容
		switch (num) {
		case 0: // 控制点编号
			showContent = controlPointT.getControlCode();
			break;
		case 1: // 风险点编号
			showContent = controlPointT.getRiskNum();
			break;
		case 2: // 控制目标
			showContent = controlPointT.getControlTarget();
			break;
		case 3: // 控制方法
			if (controlPointT.getMethod() == 0) {
				showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conManual");
			} else if (controlPointT.getMethod() == 1) {
				showContent = JecnResourceUtil.getJecnResourceUtil().getValue("it");
			} else {
				showContent = JecnResourceUtil.getJecnResourceUtil().getValue("renOrIt");
			}
			break;
		case 4: // 控制类型
			if (controlPointT.getType() == 0) {
				showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conPreventive");
			} else {
				showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conFound");
			}
			break;
		case 5:// 控制频率
			showContent = controlFrequencyConvert(controlPointT.getFrequency());
			break;
		case 6: // 关键控制点
			if (controlPointT.getKeyPoint() == 0) {
				showContent = JecnResourceUtil.getJecnResourceUtil().getValue("yes");
			} else {
				showContent = JecnResourceUtil.getJecnResourceUtil().getValue("no");
			}
			break;
		default:
			break;
		}
		return showContent;
	}

	/**
	 * 控制频率显示转换
	 * 
	 * @author CHEHUANBO
	 * @param frequency
	 *            :控制频率
	 * @return String: 转换后显示的内容
	 * 
	 * */
	private String controlFrequencyConvert(int frequency) {

		String showContent = null;

		switch (frequency) {
		case 0:
			showContent = JecnResourceUtil.getJecnResourceUtil().getValue("noScheduled");
			break;
		case 1:
			showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conDay");
			break;
		case 2:
			showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conWeek");
			break;
		case 3:
			showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conMonth");
			break;
		case 4:
			showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conQuarter");
			break;
		case 5:
			showContent = JecnResourceUtil.getJecnResourceUtil().getValue("conYear");
			break;
		default:
			break;
		}
		return showContent;
	}

	public boolean isUpdate() {
		if (jecnControlPointTList.size() != activeData.getJecnControlPointTList().size()) {
			return true;
		}
		for (JecnControlPointT newControlPointT : jecnControlPointTList) {
			boolean isExist = false;
			for (JecnControlPointT oldControlPointT : activeData.getJecnControlPointTList()) {
				if (DrawCommon.checkStringSame(newControlPointT.getId(), oldControlPointT.getId())
						&& DrawCommon.checkStringSame(newControlPointT.getControlCode(), oldControlPointT
								.getControlCode()) && newControlPointT.getKeyPoint() == oldControlPointT.getKeyPoint()
						&& newControlPointT.getFrequency() == oldControlPointT.getFrequency()
						&& newControlPointT.getMethod() == oldControlPointT.getMethod()
						&& newControlPointT.getType() == oldControlPointT.getType()
						&& DrawCommon.checkLongSame(newControlPointT.getTargetId(), oldControlPointT.getTargetId())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}

	public List<JecnControlPointT> getJecnControlPointTList() {
		return jecnControlPointTList;
	}
}
