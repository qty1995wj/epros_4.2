package epros.designer.gui.process.guide.guideTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 * 相关标准 表
 * 
 * @author 2012-07-13
 * 
 */
public class OrderTable extends JTable {
	private static Logger log = Logger.getLogger(OrderTable.class);
	/** 获得流程相关标准数据 */
	private List<StandardBean> listStandardBean = new ArrayList<StandardBean>();
	/** 流程ID */
	private Long flowId = null;

	public OrderTable(Long flowId) {
		this.flowId = flowId;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		TableColumnModel columnModel = this.getColumnModel();
		TableColumn tableColumn4 = columnModel.getColumn(4);
		tableColumn4.setMinWidth(100);
		tableColumn4.setMaxWidth(100);
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 获取流程记录保存数据
		try {
			listStandardBean = ConnectionPool.getProcessAction()
					.getStandardBeanByFlowId(flowId);
			for (int i = 0; i < listStandardBean.size(); i++) {
				StandardBean standardBean = listStandardBean.get(i);
				Vector<String> row = new Vector<String>();
				row.add(String.valueOf(standardBean.getCriterionClassId()));
				// 标准名称
				row.add(standardBean.getCriterionClassName());
				// 标准关联ID
				row.add(String.valueOf(standardBean.getRelationId()));
				// 标准类别 标准类型(0是目录，1文件标准，2流程标准 3流程地图标准4标准条款5条款要求)
				row.add(String.valueOf(standardBean.getStanType()));
				// 类型名称
				row.add(JecnDesignerCommon
						.standardTypeVal(standardBean.getStanType()));
				vs.add(row);
			}

		} catch (Exception e) {
			log.error("OrderTable getContent is error", e);
		}

		return vs;
	}

	public OrderTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("standardName"));
		title.add("RelationId");
		title.add("standarType");
		// 类型
		title.add(JecnProperties.getValue("type"));
		return new OrderTableMode(getContent(), title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0, 2, 3 };
	}

	class OrderTableMode extends DefaultTableModel {
		public OrderTableMode(Vector<Vector<String>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}
}
