/**
 * 
 */
package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

/**
 * 基准岗位校验 主要校验一下几种情况： 1、基准岗位编号为空 2、基准岗位名称为空 3、基准岗位重复 4、基准岗位下存在的岗位在实际岗位中不存在
 * 
 * @author cheshaowei
 * 
 */
public class CheckBasePos {

	private static final Logger log = Logger.getLogger(CheckBasePos.class);

	/**
	 * 基准岗位校验
	 * 
	 * @author cheshaowei
	 * @param basePosBeanList
	 *            基准岗位集合
	 * @return true : 校验失败 false: 校验成功
	 * 
	 * */
	public boolean checkBasePos(List<JecnBasePosBean> basePosBeanList,
			List<String> existsBasePosNum) {
		if (basePosBeanList == null) {
			log.info("验证的基准岗位集合不存在！");
			return false;
		}
		log.info("基准岗位行内校验开始！");
		boolean isError = false;

		// 所有重复的基准岗位

		// 获取基准岗位
		for (int i = 0; i < basePosBeanList.size(); i++) {
			// 基准岗位bean
			JecnBasePosBean basePosBean = basePosBeanList.get(i);
			// 基准岗位编号
			String basePosNum = basePosBean.getBasePosNum();
			if (SyncTool.isNullOrEmtryTrim(basePosNum)) { // 去空判断
				log.error("基准岗位校验发现存在基准岗位编号为空的数据！");
				// 添加错误信息：基准岗位编号不能够为空
				basePosBean.addError(SyncErrorInfo.BASE_POSNUM_NOTNULL);
				isError = true;
			}
			// 基准岗位名称
			String baseposName = basePosBean.getBasePosName();
			if (SyncTool.isNullOrEmtryTrim(baseposName)) {
				log.error("基准岗位校验发现存在基准岗位名称为空的数据！");
				// 添加错误信息，基准岗位名称不能够为空
				basePosBean.addError(SyncErrorInfo.BASE_POSNAME_NOTNULL);
				isError = true;
			}
			if (SyncTool.isNullOrEmtryTrim(basePosNum)
					|| existsBasePosNum == null) {
				continue;
			}
			// 判断基准岗位是否重复
//			for (String bPosNum : existsBasePosNum) {
//				if (bPosNum.equals(basePosNum)) {
//					log.error("基准岗位校验发现存在基准岗位编号存在重复的数据！");
//					// 添加错误信息：基准岗位编号不能够重复
//					basePosBean.addError(SyncErrorInfo.BASE_POSNUM_NOTREPET);
//					isError = true;
//				}
//			}
		}
		return isError;
	}

	/**
	 * 基准岗位与岗位的关系校验 主要校验：当前基准岗位中的所属岗位是否在实际岗位中存在
	 * 
	 * @param basePosRelBeanList
	 *            基准岗位与岗位关系集合 posBeanList 实际岗位集合
	 * @return true 验证失败 false 验证成功
	 * @author cheshaowei
	 * @since v3.08
	 * */
	// public boolean checkBaseRelPos(List<JecnPoseBean> relPosBeans,
	// Map<String, PosBean> mapPosBeans) {
	// log.info("基准岗位与岗位的关系校验开始！");
	// if (mapPosBeans == null || relPosBeans == null) {
	// return false;
	// }
	// boolean isCheckSuccess = false;
	// for (JecnPoseBean poseBean : relPosBeans) {
	// // 如果基准岗位下所属岗位在实际岗位集合中存在
	// PosBean bean = mapPosBeans.get(poseBean.getPosNum());
	//			
	// }
	// log.info("基准岗位与岗位关系校验结束" + isCheckSuccess);
	// return isCheckSuccess;
	// }
}
