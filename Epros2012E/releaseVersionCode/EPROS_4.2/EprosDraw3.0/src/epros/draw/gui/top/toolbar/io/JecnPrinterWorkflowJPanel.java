package epros.draw.gui.top.toolbar.io;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;

import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 从workflow中获得显示图像区域面板
 */
public class JecnPrinterWorkflowJPanel extends JPanel {

	private JecnDrawDesktopPane workflow;
	private double scaleRatio = 1.0;

	public JecnPrinterWorkflowJPanel(JecnDrawDesktopPane workflow,
			double scaleRatio) {
		this.workflow = workflow;
		this.scaleRatio = scaleRatio;
		this.setPreferredSize(new Dimension(
				(int) (workflow.getWidth() * scaleRatio), (int) (workflow
						.getHeight() * scaleRatio)));
		this.setSize(new Dimension((int) (workflow.getWidth() * scaleRatio),
				(int) (workflow.getHeight() * scaleRatio)));
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		RenderingHints rhO = g2d.getRenderingHints();
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHints(rh);
		// 将调用面板重画到新面板上,只画图像
		AffineTransform af = g2d.getTransform();
		JComponent jcomponent = (JComponent) workflow;
		g2d.transform(AffineTransform.getScaleInstance(scaleRatio, scaleRatio));
		ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
		updateDoubleBuffered(jcomponent, dbcomponents);
		jcomponent.paint(g);
		resetDoubleBuffered(dbcomponents);
		g2d.setRenderingHints(rhO);
		g2d.setTransform(af);
		g2d.dispose();
	}

	private void updateDoubleBuffered(JComponent component,
			ArrayList<JComponent> dbcomponents) {
		if (component.isDoubleBuffered()) {
			dbcomponents.add(component);
			component.setDoubleBuffered(false);
		}
		for (int i = 0; i < component.getComponentCount(); i++) {
			Component c = component.getComponent(i);
			if (c instanceof JComponent) {
				updateDoubleBuffered((JComponent) c, dbcomponents);
			}
		}
	}

	private void resetDoubleBuffered(ArrayList<JComponent> dbcomponents) {
		for (JComponent component : dbcomponents) {
			component.setDoubleBuffered(true);
		}
	}
}
