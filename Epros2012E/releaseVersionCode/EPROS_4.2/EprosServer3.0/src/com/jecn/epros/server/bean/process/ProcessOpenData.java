package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.process.inout.JecnFlowInoutT;

public class ProcessOpenData implements Serializable {
	/** 流程主表 */
	private JecnFlowStructureT jecnFlowStructureT;
	private List<ProcessFigureData> processFigureDataList;
	/** 流程属性表 */
	private JecnFlowBasicInfoT jecnFlowBasicInfoT;

	private JecnMainFlowT mainFlowT;

	private List<JecnFlowInoutT> flowInTs;

	private List<JecnFlowInoutT> flowOutTs;

	/** 文件操作状态 1：可编辑，2：任务中，3 ：只读模式 ； 4：只读模式，可编辑 ；5：占用 */
	private int isAuthSave = 1;
	/** 面板占用人 */
	private String occupierName;

	public String getOccupierName() {
		return occupierName;
	}

	public void setOccupierName(String occupierName) {
		this.occupierName = occupierName;
	}

	public int getIsAuthSave() {
		return isAuthSave;
	}

	public void setIsAuthSave(int isAuthSave) {
		this.isAuthSave = isAuthSave;
	}

	public JecnFlowBasicInfoT getJecnFlowBasicInfoT() {
		return jecnFlowBasicInfoT;
	}

	public void setJecnFlowBasicInfoT(JecnFlowBasicInfoT jecnFlowBasicInfoT) {
		this.jecnFlowBasicInfoT = jecnFlowBasicInfoT;
	}

	public JecnFlowStructureT getJecnFlowStructureT() {
		return jecnFlowStructureT;
	}

	public void setJecnFlowStructureT(JecnFlowStructureT jecnFlowStructureT) {
		this.jecnFlowStructureT = jecnFlowStructureT;
	}

	public List<ProcessFigureData> getProcessFigureDataList() {
		return processFigureDataList;
	}

	public void setProcessFigureDataList(List<ProcessFigureData> processFigureDataList) {
		this.processFigureDataList = processFigureDataList;
	}

	public JecnMainFlowT getMainFlowT() {
		return mainFlowT;
	}

	public void setMainFlowT(JecnMainFlowT mainFlowT) {
		this.mainFlowT = mainFlowT;
	}

	public List<JecnFlowInoutT> getFlowInTs() {
		return flowInTs;
	}

	public void setFlowInTs(List<JecnFlowInoutT> flowInTs) {
		this.flowInTs = flowInTs;
	}

	public List<JecnFlowInoutT> getFlowOutTs() {
		return flowOutTs;
	}

	public void setFlowOutTs(List<JecnFlowInoutT> flowOutTs) {
		this.flowOutTs = flowOutTs;
	}

}
