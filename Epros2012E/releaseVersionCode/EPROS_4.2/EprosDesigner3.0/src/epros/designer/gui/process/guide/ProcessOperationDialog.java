package epros.designer.gui.process.guide;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.guide.explain.JecnActiveDesc;
import epros.designer.gui.process.guide.explain.JecnFlowScope;
import epros.designer.gui.process.guide.explain.JecnFlowTermInOut;
import epros.designer.gui.process.guide.explain.JecnITSystem;
import epros.designer.gui.process.guide.explain.JecnInAndOut;
import epros.designer.gui.process.guide.explain.JecnKeyActive;
import epros.designer.gui.process.guide.explain.JecnProcessActiveFile;
import epros.designer.gui.process.guide.explain.JecnProcessBorder;
import epros.designer.gui.process.guide.explain.JecnProcessDriveRule;
import epros.designer.gui.process.guide.explain.JecnProcessDriveRuleNew;
import epros.designer.gui.process.guide.explain.JecnProcessDutyProperty;
import epros.designer.gui.process.guide.explain.JecnProcessEvaluation;
import epros.designer.gui.process.guide.explain.JecnProcessHistory;
import epros.designer.gui.process.guide.explain.JecnProcessInterfaces;
import epros.designer.gui.process.guide.explain.JecnProcessRecord;
import epros.designer.gui.process.guide.explain.JecnRelatedFile;
import epros.designer.gui.process.guide.explain.JecnRelatedFlow;
import epros.designer.gui.process.guide.explain.JecnRelatedRisk;
import epros.designer.gui.process.guide.explain.JecnRelatedRule;
import epros.designer.gui.process.guide.explain.JecnRelatedStandard;
import epros.designer.gui.process.guide.explain.JecnRelatedStandardFile;
import epros.designer.gui.process.guide.explain.JecnRoleAndDuty;
import epros.designer.gui.process.guide.explain.JecnTermDefinition;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionArea;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionField;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.service.process.JecnSelectProcessDownload;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.event.JecnOftenActionProcess;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.shape.part.TermFigureAR;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.operationConfig.DrawOperationDescriptionConfigBean;
import epros.draw.gui.operationConfig.bean.JecnFlowOperationBean;
import epros.draw.gui.operationConfig.data.JecnFlowOperationDataUnit;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程操作说明
 * 
 * @author hyl
 * 
 */
public class ProcessOperationDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(ProcessOperationDialog.class);
	/** 流程ID */
	private Long flowId;
	/** 流程基本信息临时表 */
	private JecnFlowBasicInfoT flowBasicInfoT = null;
	/** 当前面板的节点 **/
	private JecnTreeNode curWorkNode;

	private Map<String, JecnFileDescriptionComponent> listComponent = new HashMap<String, JecnFileDescriptionComponent>();
	// 操作说明的一份数据拷贝，一些数据改变会重新刷新该数据bean
	private JecnFlowOperationBean flowOperationBean;

	protected JecnPanel mainPanel = new JecnPanel(800, 700);
	/** 滚动面板内信息面板 */
	protected JecnPanel contentPanel = new JecnPanel();
	/** 结果滚动面板 */
	protected JScrollPane resultScrollPane = new JScrollPane(contentPanel);
	/** 按钮面板 */
	protected JecnPanel buttonPanel = new JecnPanel(590, 40);
	/** 下载 */
	protected JButton downLoad = new JButton(JecnProperties.getValue("downloadOpDes"));
	/** 确定 */
	protected JButton okBut = new JButton(JecnProperties.getValue("confirm"));
	/** 取消 */
	protected JButton cancelBut = new JButton(JecnProperties.getValue("cancel"));
	/** 错误提示 */
	protected JLabel errorLab = new JLabel();

	protected List<DrawOperationDescriptionConfigBean> operConfigList;

	public ProcessOperationDialog(Long flowId, JecnTreeNode selectNode) {
		this.setTitle(JecnProperties.getValue("processOperatingInstructions"));
		this.setSize(700, 720);
		this.setModal(true);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.curWorkNode = selectNode;
		this.flowId = flowId;
		this.operConfigList = JecnDesignerProcess.getDesignerProcess().getOperConfigList();
		setFlowOperationBean();
		resultScrollPane.getVerticalScrollBar().setValue(0);
		initFlowBasicInfoFromWorkflow();
		initComponentAndLayout();

		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		downLoad.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				downLoadFile();
			}
		});
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;

			}
		});

	}

	private Dimension getSizeDimension() {
		// 650, 700
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = Math.min(650, (int) screenSize.getWidth());
		int height = Math.min(700, (int) screenSize.getHeight() - 50);
		return new Dimension(width, height);
	}

	public void setFlowOperationBean() {
		this.flowOperationBean = JecnFlowOperationDataUnit.getJecnFlowOperationBean();
	}

	private void initComponentAndLayout() {

		errorLab.setForeground(Color.RED);

		Insets insets = new Insets(5, 5, 5, 5);
		GridBagConstraints c = null;

		int row = 0;
		String mark = null;
		JecnFileDescriptionComponent component = null;
		String name = null;
		boolean isRequest = false;
		String tip = "";
		for (DrawOperationDescriptionConfigBean confiBean : operConfigList) {
			if (confiBean.getIsShow() == 1) {
				row++;
				mark = confiBean.getMark().toString();
				name = confiBean.getName();
				tip = JecnResourceUtil.CH ? confiBean.getV1() : confiBean.getV2();
				// isRequest = confiBean.getIsEmpty() == 1 ? true : false;
				// 需求是:如果不填的话流程说明也可以保存，也可以关闭，所以必填项也不显示必填的*号，也暂时没有必要从数据库取
				isRequest = confiBean.getIsEmpty() == 1 ? true : false;

				if (confiBean.getMark().equals("a1")) {// 目的
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, true, flowBasicInfoT
							.getFlowPurpose(), tip);
				} else if (confiBean.getMark().equals("a2")) {// 适用范围
					component = new JecnProcessApplyArea(row, name, isRequest, contentPanel, flowBasicInfoT
							.getApplicability(), this, flowBasicInfoT.getFlowId(), tip);
				} else if (confiBean.getMark().equals("a3")) {// 术语定义
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, false, flowBasicInfoT
							.getNoutGlossary(), tip);
				} else if (confiBean.getMark().equals("a4")) {// 流程驱动规则
					if (JecnConfigTool.isUseNewDrive()) {
						component = new JecnProcessDriveRuleNew(row, name, contentPanel, isRequest, flowBasicInfoT,
								this, tip);
					} else {
						component = new JecnProcessDriveRule(row, name, contentPanel, isRequest, flowBasicInfoT, this,
								tip);
					}
				} else if (confiBean.getMark().equals("a5")) {// 输入
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, true, flowBasicInfoT
							.getInput(), tip);
				} else if (confiBean.getMark().equals("a6")) {// 输出
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, true, flowBasicInfoT
							.getOuput(), tip);
				} else if (confiBean.getMark().equals("a7")) {// 关键活动
					component = new JecnKeyActive(row, name, isRequest, contentPanel, this, tip);
				} else if (confiBean.getMark().equals("a8")) {// 角色职责
					component = new JecnRoleAndDuty(row, name, isRequest, contentPanel, this, tip);
				} else if (confiBean.getMark().equals("a9")) {// 流程图
					component = new JecnFileDescriptionField(row, name, contentPanel, isRequest, false, curWorkNode
							.getJecnTreeBean().getName(), tip);
				} else if (confiBean.getMark().equals("a10")) {// 活动说明
					component = new JecnActiveDesc(row, name, isRequest, contentPanel, this, tip);
				} else if (confiBean.getMark().equals("a11")) {// 流程记录
					component = new JecnProcessActiveFile(row, name, isRequest, contentPanel, this, 0, tip,
							flowBasicInfoT);
				} else if (confiBean.getMark().equals("a12")) {// 操作规范
					component = new JecnProcessActiveFile(row, name, isRequest, contentPanel, this, 1, tip,
							flowBasicInfoT);
				} else if (confiBean.getMark().equals("a13")) {// 相关流程
					component = new JecnRelatedFlow(row, name, isRequest, contentPanel, flowId, tip);
				} else if (confiBean.getMark().equals("a14")) {// 相关制度
					component = new JecnRelatedRule(row, name, isRequest, contentPanel, flowId, tip);
				} else if (confiBean.getMark().equals("a15")) {// 流程关键测评指标板
					component = new JecnProcessEvaluation(row, name, isRequest, contentPanel, flowId, tip);
				} else if (confiBean.getMark().equals("a16")) {// 客户
					boolean editAble = false;
					String text = "";
					if (JecnConfigTool.processCustomShowType() == 1) {
						editAble = false;
						// 获取面板上客户
						text = JecnDesignerCommon.getCustomValue();
					} else {
						editAble = true;
						text = flowBasicInfoT.getFlowCustom();
					}
					component = new JecnFileDescriptionField(row, name, contentPanel, isRequest, editAble, text, tip);
				} else if (confiBean.getMark().equals("a17")) {// 不适用范围
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, true, flowBasicInfoT
							.getNoApplicability(), tip);
				} else if (confiBean.getMark().equals("a18")) {// 概述
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, true, flowBasicInfoT
							.getFlowSummarize(), tip);
				} else if (confiBean.getMark().equals("a19")) {// 补充说明
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, true, flowBasicInfoT
							.getFlowSupplement(), tip);
				} else if (confiBean.getMark().equals("a20")) {// 记录保存
					component = new JecnProcessRecord(row, name, isRequest, contentPanel, flowId, tip);
				} else if (confiBean.getMark().equals("a21")) {// 相关标准
					component = new JecnRelatedStandard(row, name, isRequest, contentPanel, flowId,
							TreeNodeType.process, tip);
				} else if (confiBean.getMark().equals("a25")) {// 相关风险
					component = new JecnRelatedRisk(row, name, isRequest, contentPanel, flowId, this, tip);
				} else if (confiBean.getMark().equals("a27")) {// 自定义要素1
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, false, flowBasicInfoT
							.getFlowCustomOne(), tip);
				} else if (confiBean.getMark().equals("a28")) {// 自定义要素2
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, false, flowBasicInfoT
							.getFlowCustomTwo(), tip);
				} else if (confiBean.getMark().equals("a29")) {// 自定义要素3
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, false, flowBasicInfoT
							.getFlowCustomThree(), tip);
				} else if (confiBean.getMark().equals("a30")) {// 自定义要素4
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, false, flowBasicInfoT
							.getFlowCustomFour(), tip);
				} else if (confiBean.getMark().equals("a31")) {// 自定义要素5
					component = new JecnFileDescriptionArea(row, name, contentPanel, isRequest, false, flowBasicInfoT
							.getFlowCustomFive(), tip);
				} else if (confiBean.getMark().equals("a32")) {// 输入和输出
					component = new JecnInAndOut(row, name, contentPanel, isRequest, flowBasicInfoT, tip);
				} else if (confiBean.getMark().equals("a22")) {// 流程边界
					component = new JecnProcessBorder(row, name, contentPanel, isRequest, flowBasicInfoT, tip);
				} else if (confiBean.getMark().equals("a23")) {// 流程责任属性
					component = new JecnProcessDutyProperty(row, name, contentPanel, isRequest, curWorkNode, flowId,
							tip);
				} else if (confiBean.getMark().equals("a24")) {// 流程文控信息
					component = new JecnProcessHistory(row, name, contentPanel, isRequest, flowId, tip);
				} else if (confiBean.getMark().equals("a33")) {// 流程范围
					// 包含起始结束输入输出
					component = new JecnFlowScope(row, name, contentPanel, isRequest, flowBasicInfoT, tip);
				} else if (confiBean.getMark().equals("a34")) {// 术语
					component = new JecnTermDefinition(row, name, isRequest, contentPanel, tip, flowBasicInfoT);
				} else if (confiBean.getMark().equals("a35")) {// 相关文件
					component = new JecnRelatedFile(row, name, isRequest, contentPanel, this, tip, flowBasicInfoT);
				} else if (confiBean.getMark().equals("a36")) {// 流程接口描述
					component = new JecnProcessInterfaces(row, name, isRequest, contentPanel, this, tip);
				} else if (confiBean.getMark().equals("a37")) {// 流程标准文件
					component = new JecnRelatedStandardFile(row, name, isRequest, contentPanel, flowId, tip);
				} else if (confiBean.getMark().equals("a38")) {// 信息化
					component = new JecnITSystem(row, name, isRequest, contentPanel, this, tip);
				} else if (confiBean.getMark().equals("a39")) {// 新版输入
					component = new JecnFlowTermInOut(row, name, isRequest, contentPanel, flowBasicInfoT.getFlowId(),
							0, tip);
				} else if (confiBean.getMark().equals("a40")) {// 新版输出
					component = new JecnFlowTermInOut(row, name, isRequest, contentPanel, flowBasicInfoT.getFlowId(),
							1, tip);
				}
				listComponent.put(mark, component);
			}
		}

		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(580, 600);
		resultScrollPane.setPreferredSize(dimension);

		// *************************end*****************************************************
		resultScrollPane.setViewportView(contentPanel);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(resultScrollPane, c);

		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);

		if (JecnConfigTool.isDongRuanYiLiaoLoginType()) {
			downLoad.setText(JecnProperties.getValue("previewBtn"));
		}
		// 添加按钮面板
		addButtonPanel();
		this.getContentPane().add(mainPanel);
	}

	// TODO
	private void initFlowBasicInfoFromWorkflow() {
		try {
			flowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(flowId);
		} catch (Exception e) {
			log.error("ProcessOperationDialog initFlowBasicInfoFromWorkflow is error", e);
		}
		// 获取面板数据
		// 从面板取值重新填充基本信息bean（因为导入流程图需要覆盖这些值）
		JecnFlowOperationBean workflow = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
				.getFlowOperationBean();
		// 目的
		flowBasicInfoT.setFlowPurpose(workflow.getFlowPurpose());
		// 适用范围
		flowBasicInfoT.setApplicability(workflow.getApplicability());
		// 术语定义
		flowBasicInfoT.setNoutGlossary(workflow.getNoutGlossary());
		// 输入
		flowBasicInfoT.setInput(workflow.getFlowInput());
		// 输出
		flowBasicInfoT.setOuput(workflow.getFlowOutput());
		// 起始活动
		flowBasicInfoT.setFlowStartpoint(workflow.getFlowStartActive());
		// 终止活动
		flowBasicInfoT.setFlowEndpoint(workflow.getFlowEndActive());
		// 不适用范围
		flowBasicInfoT.setNoApplicability(workflow.getNoApplicability());
		// 概况信息
		flowBasicInfoT.setFlowSummarize(workflow.getFlowSummarize());
		// 补充说明
		flowBasicInfoT.setFlowSupplement(workflow.getFlowSupplement());
		// 客户
		if (JecnConfigTool.processCustomShowType() == 1) {// 数据来源于流程图中流程客户图标的名称
			flowBasicInfoT.setFlowCustom(JecnDesignerCommon.getCustomValue());
		} else {
			flowBasicInfoT.setFlowCustom(workflow.getFlowCustom());
		}
		// 自定义
		flowBasicInfoT.setFlowCustomOne(workflow.getCustomOne());
		flowBasicInfoT.setFlowCustomTwo(workflow.getCustomTwo());
		flowBasicInfoT.setFlowCustomThree(workflow.getCustomThree());
		flowBasicInfoT.setFlowCustomFour(workflow.getCustomFour());
		flowBasicInfoT.setFlowCustomFive(workflow.getCustomFive());
	}

	public void flushFlowBasicInfoToWorkflow(Map<String, Object> resultMap) {
		JecnFlowOperationBean workflow = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
				.getFlowOperationBean();
		// 目的
		workflow.setFlowPurpose(flowBasicInfoT.getFlowPurpose());
		// 适用范围
		workflow.setApplicability(flowBasicInfoT.getApplicability());
		// 术语定义
		workflow.setNoutGlossary(flowBasicInfoT.getNoutGlossary());
		// 输入
		workflow.setFlowInput(flowBasicInfoT.getInput());
		// 输出
		workflow.setFlowOutput(flowBasicInfoT.getOuput());
		// 起始活动
		workflow.setFlowStartActive(flowBasicInfoT.getFlowStartpoint());
		// 终止活动
		workflow.setFlowEndActive(flowBasicInfoT.getFlowEndpoint());
		// 不适用范围
		workflow.setNoApplicability(flowBasicInfoT.getNoApplicability());
		// 概况信息
		workflow.setFlowSummarize(flowBasicInfoT.getFlowSummarize());
		// 补充说明
		workflow.setFlowSupplement(flowBasicInfoT.getFlowSupplement());
		// 客户
		if (JecnConfigTool.processCustomShowType() == 1) {// 数据来源于流程图中流程客户图标的名称
			workflow.setFlowCustom(JecnDesignerCommon.getCustomValue());
		} else {
			workflow.setFlowCustom(flowBasicInfoT.getFlowCustom());
		}
		// 自定义
		workflow.setCustomOne(flowBasicInfoT.getFlowCustomOne());
		workflow.setCustomTwo(flowBasicInfoT.getFlowCustomTwo());
		workflow.setCustomThree(flowBasicInfoT.getFlowCustomThree());
		workflow.setCustomFour(flowBasicInfoT.getFlowCustomFour());
		workflow.setCustomFive(flowBasicInfoT.getFlowCustomFive());

		// TODO 在图上生成
		List<JecnTreeBean> terms = (List<JecnTreeBean>) resultMap.get("a34");
		if (terms != null && terms.size() > 0) {
			// 获得最下面的点
			Point point = getBottomSuitablePoint();
			int suitableX = (int) point.getX();
			for (JecnTreeBean treeBean : terms) {
				TermFigureAR termFigureAR = (TermFigureAR) JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.TermFigureAR);
				termFigureAR.getFlowElementData().setTermDefine(treeBean.getContent());
				termFigureAR.getFlowElementData().setFigureText(treeBean.getName());
				termFigureAR.getFlowElementData().getDesignerFigureData().setLinkId(treeBean.getId());
				JecnAddFlowElementUnit.addFlowElement(new Point(suitableX, (int) point.getY()), termFigureAR);
				suitableX += 120;
			}
			// 调用保存
			JecnOftenActionProcess.saveWokfFlow(JecnDrawMainPanel.getMainPanel().getWorkflow());
		}
	}

	private Point getBottomSuitablePoint() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		List<JecnBaseFlowElementPanel> els = workflow.getPanelList();
		List<JecnBaseVHLinePanel> lines = workflow.getVhLinePanelList();
		int x = 200, y = 20;
		for (JecnBaseFlowElementPanel e : els) {
			if (!(e instanceof JecnBaseVHLinePanel) && e.getLocation().y > y) {
				y = e.getLocation().y;
			}
		}

		for (JecnBaseVHLinePanel l : lines) {
			if (l.getLineDirectionEnum() == LineDirectionEnum.vertical && (int) l.getLocation().x < x) {
				x = l.getLocation().x;
			}
		}
		x = x + 20;
		y = y + 100;
		return new Point(x, y);
	}

	/**
	 * 按钮面板
	 */
	protected void addButtonPanel() {

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		if (JecnDesignerCommon.isAuthSave()) {// 有保存权限
			buttonPanel.add(errorLab);
			buttonPanel.add(downLoad);
			buttonPanel.add(okBut);
		}
		buttonPanel.add(cancelBut);

	}

	/**
	 * 流程文件下载
	 */
	public void downLoadFile() {
		if (isValidate()) {
			if (isUpdate()) {
				// 更新
				updateOperation();
			}
			// 下载
			List<JecnTreeNode> curWorkNodes = new ArrayList<JecnTreeNode>();
			curWorkNodes.add(curWorkNode);
			JecnSelectProcessDownload.processDownload(false, curWorkNode, curWorkNodes);
		}
	}

	/**
	 * 确定
	 */
	public void okButPerformed() {
		if (isValidate()) {
			if (isUpdate()) {
				updateOperation();
			}
			this.setVisible(false);
		}
	}

	private void updateOperation() {
		// 确认错误提示
		errorLab.setText("");
		String key;
		JecnFileDescriptionComponent value;
		Map<String, Object> resultMap = new HashMap<String, Object>();

		for (Map.Entry<String, JecnFileDescriptionComponent> entry : listComponent.entrySet()) {

			key = entry.getKey();
			value = entry.getValue();

			if (key.equals("a1")) {// 目的
				flowBasicInfoT.setFlowPurpose(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a2")) {// 适用范围
				flowBasicInfoT.setApplicability(((JecnProcessApplyArea) value).getResultStr());
				// 适用部门
				String applyOrgId = ((JecnProcessApplyArea) value).getOrgIds();
				resultMap.put(key, applyOrgId);
			} else if (key.equals("a3")) {// 术语定义
				flowBasicInfoT.setNoutGlossary(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a4")) {// 流程驱动规则
				if (JecnConfigTool.isUseNewDrive()) {
					JecnProcessDriveRuleNew driver = (JecnProcessDriveRuleNew) value;
					// 流程驱动类型
					flowBasicInfoT.setDriveRules(driver.getDriveRules());
					// 流程驱动规则
					flowBasicInfoT.setDriveType(driver.getDriveType());
					if (JecnConfigTool.isShowTimeDriverText()) {
						// 驱动规则bean
						resultMap.put(key, driver.getJecnFlowDriver());
					}
				} else {
					JecnProcessDriveRule driver = (JecnProcessDriveRule) value;
					// 流程驱动类型
					flowBasicInfoT.setDriveRules(driver.getDriveRules());
					// 流程驱动规则
					flowBasicInfoT.setDriveType(driver.getDriveType());
					if (JecnConfigTool.isShowTimeDriverText()) {
						// 驱动规则bean
						resultMap.put(key, driver.getJecnFlowDriver());
					}
				}
			} else if (key.equals("a5")) {// 输入
				flowBasicInfoT.setInput(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a6")) {// 输出
				flowBasicInfoT.setOuput(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a14")) {// 相关制度
				resultMap.put(key, ((JecnRelatedRule) value).getResultListLong());
			} else if (key.equals("a16")) {// 客户
				flowBasicInfoT.setFlowCustom(((JecnFileDescriptionField) value).getResultStr());
			} else if (key.equals("a17")) {// 不适用范围
				flowBasicInfoT.setNoApplicability(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a18")) {// 概述
				flowBasicInfoT.setFlowSummarize(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a19")) {// 补充说明
				flowBasicInfoT.setFlowSupplement(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a20")) {// 记录保存
				resultMap.put(key, ((JecnProcessRecord) value).getResultListJecnFlowRecordT());
			} else if (key.equals("a21")) {// 相关标准
				resultMap.put(key, ((JecnRelatedStandard) value).getResultListLong());
			} else if (key.equals("a22")) {// 流程边界
				flowBasicInfoT.setFlowStartpoint(((JecnProcessBorder) value).getFlowStart());
				flowBasicInfoT.setFlowEndpoint(((JecnProcessBorder) value).getFlowEnd());
			} else if (key.equals("a27")) {// 自定义要素1
				flowBasicInfoT.setFlowCustomOne(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a28")) {// 自定义要素2
				flowBasicInfoT.setFlowCustomTwo(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a29")) {// 自定义要素3
				flowBasicInfoT.setFlowCustomThree(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a30")) {// 自定义要素4
				flowBasicInfoT.setFlowCustomFour(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a31")) {// 自定义要素5
				flowBasicInfoT.setFlowCustomFive(((JecnFileDescriptionArea) value).getResultStr());
			} else if (key.equals("a32")) {// 输入和输出
				JecnInAndOut inAndOut = ((JecnInAndOut) value);
				flowBasicInfoT.setInput(inAndOut.getIn());
				flowBasicInfoT.setOuput(inAndOut.getOut());
			} else if (key.equals("a33")) {// 流程范围
				flowBasicInfoT.setFlowStartpoint(((JecnFlowScope) value).getFlowStart());
				flowBasicInfoT.setFlowEndpoint(((JecnFlowScope) value).getFlowEnd());
				flowBasicInfoT.setInput(((JecnFlowScope) value).getFlowIn());
				flowBasicInfoT.setOuput(((JecnFlowScope) value).getFlowOut());
			} else if (key.equals("a37")) {// 流程标准文件
				resultMap.put(key, ((JecnRelatedStandardFile) value).getResultListLong());
			} else if (key.equals("a39")) {// 新版输入
				resultMap.put(key, ((JecnFlowTermInOut) value).getData());
			} else if (key.equals("a40")) {// 新版输出
				resultMap.put(key, ((JecnFlowTermInOut) value).getData());
			} else if (key.equals("a34")) {
				resultMap.put(key, ((JecnTermDefinition) value).getAdd());
			}
		}

		try {
			ConnectionPool.getProcessAction().updateFlowOperation(flowId, flowBasicInfoT, resultMap,
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			curWorkNode.getJecnTreeBean().setUpdate(true);
			JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), curWorkNode);
			// 刷新操作中的流程基本数据到面板
			flushFlowBasicInfoToWorkflow(resultMap);
			// 获取面板
			JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("saveException"));
			log.error("ProcessOperationDialog updateOperation is error", e);
		}
	}

	private boolean isValidate() {
		for (DrawOperationDescriptionConfigBean confiBean : operConfigList) {
			if (confiBean.getIsShow() == 1) {
				if (listComponent.get(confiBean.getMark().toString()).isValidateNotPass(errorLab)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (JecnDesignerCommon.isAuthSave()) {// 有保存权限
			if (!isUpdate()) {// 没有更新
				this.setVisible(false);
			} else {
				int optionTig = this.dialogCloseBeforeMsgTipAction();
				if (optionTig == 2) {
					okButPerformed();
				} else if (optionTig == 1) {
					this.setVisible(false);
				}
			}
		} else {
			this.setVisible(false);
		}
	}

	/**
	 * 
	 * 是否有更新
	 * 
	 * @return boolean true：有更新；false：没有更新
	 */
	protected boolean isUpdate() {
		if (!JecnConfigTool.isCetc10Login()) {
			if (!JecnDesignerCommon.isEditByTaskNode(this.curWorkNode.getJecnTreeBean(), JecnDesignerMainPanel
					.getDesignerMainPanel().getTree(), JecnConstants.loginBean.isDesignProcessAdmin())) {
				return false;
			}
		}
		for (DrawOperationDescriptionConfigBean confiBean : operConfigList) {
			if (confiBean.getIsShow() == 1) {
				if (listComponent.get(confiBean.getMark().toString()).isUpdate()) {
					return true;
				}
			}
		}
		return false;
	}

	public JecnFlowBasicInfoT getFlowBasicInfoT() {
		return flowBasicInfoT;
	}

	public void setFlowBasicInfoT(JecnFlowBasicInfoT flowBasicInfoT) {
		this.flowBasicInfoT = flowBasicInfoT;
	}

	public JecnTreeNode getCurWorkNode() {
		return curWorkNode;
	}

	public void setCurWorkNode(JecnTreeNode curWorkNode) {
		this.curWorkNode = curWorkNode;
	}

	public Map<String, JecnFileDescriptionComponent> getListComponent() {
		return listComponent;
	}

	public void setListComponent(Map<String, JecnFileDescriptionComponent> listComponent) {
		this.listComponent = listComponent;
	}

	public JecnFlowOperationBean getFlowOperationBean() {
		return flowOperationBean;
	}

}
