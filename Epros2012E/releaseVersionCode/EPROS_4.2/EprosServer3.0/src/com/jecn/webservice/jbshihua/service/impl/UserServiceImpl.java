package com.jecn.webservice.jbshihua.service.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;
import com.jecn.webservice.jbshihua.entity.PostInfo;
import com.jecn.webservice.jbshihua.entity.UserViewDao;
import com.jecn.webservice.jbshihua.service.UserService;

@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl extends AbsBaseService<JecnUserPosView, String> implements UserService {

	private UserViewDao userViewDao;

	public UserViewDao getUserViewDao() {
		return userViewDao;
	}

	public void setUserViewDao(UserViewDao userViewDao) {
		this.userViewDao = userViewDao;
	}

	@Override
	public int receiveUser(List<JecnUserPosView> users) throws Exception {
		List<String> userUUIDs = getUserUUID();
		for (JecnUserPosView user : users) {
			if (userUUIDs.contains(user.getUserNum())) {// 原来没有 新增的
				userViewDao.updateUser(user);
			} else {
				userViewDao.insertUser(user);
				userUUIDs.add(user.getUserNum());
			}
		}
		return 0;
	}
	
	private List<String> getUserUUID(){
		String sql = "SELECT user_num FROM jecn_user_pos_info";
		return userViewDao.listObjectNativeSql(sql, "user_num", Hibernate.STRING);
	}

	@Override
	public void receiveJob(List<PostInfo> postList) throws Exception {
		List<String> existsUUIDS = getPostUUIDS();
		for (PostInfo post : postList) {
			if(existsUUIDS.contains(post.getUuid())){
				userViewDao.updatePost(post);
			}else{
				userViewDao.insertPost(post);
			}
		}
	}

	@Override
	public int saveUsers(List<JecnUserPosView> users) throws Exception {
		for (JecnUserPosView user : users) {
			if ("0".equals(user.getStatus())) {
				userViewDao.insertUser(user);
			} else if ("1".equals(user.getStatus()) || "2".equals(user.getStatus())) {
				userViewDao.updateUserAndPost(user);
			}
		}
		return 0;
	}
	
	private List<String> getPostUUIDS(){
		String sql = "SELECT UUID FROM JECN_POST_INFO";
		return userViewDao.listObjectNativeSql(sql, "UUID", Hibernate.STRING);
	}

	@Override
	public List<JecnUserPosView> getAllUsers() throws Exception {
		String hql = "from JecnUserPosView";
		return userViewDao.listHql(hql);
	}
}