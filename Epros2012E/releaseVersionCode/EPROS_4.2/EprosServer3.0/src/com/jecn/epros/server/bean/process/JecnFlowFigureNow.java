package com.jecn.epros.server.bean.process;

import java.io.Serializable;

import com.jecn.epros.server.util.JecnResourceUtil;

public class JecnFlowFigureNow implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4004577069022354060L;
	private Long id;//主键
    private String figureName;//元素名称
    private Long figureWidth;//元素宽度
    private Long figureHeight;//元素高度
    private Integer isShadow;//是否为阴影效果:0是;1否
    private Integer is3D;//是否为3D效果:0是;1否
    private Integer isVertical;//是否为竖排文字:0是;1否
    private String bgColor;//填充(背景)颜色
    private String borderColor;//边框颜色
    private String fontColor;//字体颜色
    private Long fontSize;//字体大小
    private String fontLocation;//字体位置
    private String borderStyle;//边框样式
    /**填充类型——new*/
    private String fillType;
    /**填充颜色2——new*/
    private String changeColor;
    /**渐变方向类型——new*/
    private String fillColorChangType;
    /**字形样式——new*/
    private int fontStyle;
    /**字体名称——new*/
    private String fontName;
    /**阴影颜色——new*/
    private String shadowColor;
    /**线条粗细———new*/
    private int bodyWidth;
    /**是否显示编辑四个点——new*/
    private int eidtPoint;
    /**是否支持双击——new*/
    private int isDoubleClick;
    /**是否支持快速添加图形——new*/
    private int isSortAddFigure;
    /**流程元素文字内容——new——new*/
    private String figureText;
    /**流程元素文字内容 英文——new——new*/
    private String figureEnText;
    
    
    public String getFigureEnText() {
		return figureEnText;
	}

	public void setFigureEnText(String figureEnText) {
		this.figureEnText = figureEnText;
	}

	/**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the figureName
     */
    public String getFigureName() {
        return figureName;
    }

    /**
     * @param figureName the figureName to set
     */
    public void setFigureName(String figureName) {
        this.figureName = figureName;
    }

    /**
     * @return the figureWidth
     */
    public Long getFigureWidth() {
        return figureWidth;
    }

    /**
     * @param figureWidth the figureWidth to set
     */
    public void setFigureWidth(Long figureWidth) {
        this.figureWidth = figureWidth;
    }

    /**
     * @return the figureHeight
     */
    public Long getFigureHeight() {
        return figureHeight;
    }

    /**
     * @param figureHeight the figureHeight to set
     */
    public void setFigureHeight(Long figureHeight) {
        this.figureHeight = figureHeight;
    }

    /**
     * @return the isShadow
     */
    public int getIsShadow() {
        return isShadow;
    }

    /**
     * @param isShadow the isShadow to set
     */
    public void setIsShadow(int isShadow) {
        this.isShadow = isShadow;
    }

    /**
     * @return the is3D
     */
    public int getIs3D() {
        return is3D;
    }

    /**
     * @param is3D the is3D to set
     */
    public void setIs3D(int is3D) {
        this.is3D = is3D;
    }

    /**
     * @return the isVertical
     */
    public int getIsVertical() {
        return isVertical;
    }

    /**
     * @param isVertical the isVertical to set
     */
    public void setIsVertical(int isVertical) {
        this.isVertical = isVertical;
    }

    /**
     * @return the bgColor
     */
    public String getBgColor() {
        return bgColor;
    }

    /**
     * @param bgColor the bgColor to set
     */
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    /**
     * @return the borderColor
     */
    public String getBorderColor() {
        return borderColor;
    }

    /**
     * @param borderColor the borderColor to set
     */
    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * @return the fontColor
     */
    public String getFontColor() {
        return fontColor;
    }

    /**
     * @param fontColor the fontColor to set
     */
    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    /**
     * @return the fontSize
     */
    public Long getFontSize() {
        return fontSize;
    }

    /**
     * @param fontSize the fontSize to set
     */
    public void setFontSize(Long fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * @return the fontLocation
     */
    public String getFontLocation() {
        return fontLocation;
    }

    /**
     * @param fontLocation the fontLocation to set
     */
    public void setFontLocation(String fontLocation) {
        this.fontLocation = fontLocation;
    }

    /**
     * @return the borderStyle
     */
    public String getBorderStyle() {
        return borderStyle;
    }

    /**
     * @param borderStyle the borderStyle to set
     */
    public void setBorderStyle(String borderStyle) {
        this.borderStyle = borderStyle;
    }

	public void setIsShadow(Integer isShadow) {
		this.isShadow = isShadow;
	}

	public void setIs3D(Integer is3D) {
		this.is3D = is3D;
	}

	public void setIsVertical(Integer isVertical) {
		this.isVertical = isVertical;
	}

	public String getFillType() {
		return fillType;
	}

	public void setFillType(String fillType) {
		this.fillType = fillType;
	}

	public String getChangeColor() {
		return changeColor;
	}

	public void setChangeColor(String changeColor) {
		this.changeColor = changeColor;
	}

	public String getFillColorChangType() {
		return fillColorChangType;
	}

	public void setFillColorChangType(String fillColorChangType) {
		this.fillColorChangType = fillColorChangType;
	}

	public int getFontStyle() {
		return fontStyle;
	}

	public void setFontStyle(int fontStyle) {
		this.fontStyle = fontStyle;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public String getShadowColor() {
		return shadowColor;
	}

	public void setShadowColor(String shadowColor) {
		this.shadowColor = shadowColor;
	}

	public int getBodyWidth() {
		return bodyWidth;
	}

	public void setBodyWidth(int bodyWidth) {
		this.bodyWidth = bodyWidth;
	}

	public int getEidtPoint() {
		return eidtPoint;
	}

	public void setEidtPoint(int eidtPoint) {
		this.eidtPoint = eidtPoint;
	}

	public int getIsDoubleClick() {
		return isDoubleClick;
	}

	public void setIsDoubleClick(int isDoubleClick) {
		this.isDoubleClick = isDoubleClick;
	}

	public int getIsSortAddFigure() {
		return isSortAddFigure;
	}

	public void setIsSortAddFigure(int isSortAddFigure) {
		this.isSortAddFigure = isSortAddFigure;
	}

	public String getFigureText() {
		return  figureText;
	}
	public String getFigureText(int type) {
		return  type == 0 ?this.figureText : this.figureEnText;
	}
	
	public void setFigureText(String figureText) {
		this.figureText = figureText;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

    
}
