package com.jecn.epros.server.service.rule;

import java.util.List;

import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.bean.rule.RuleModeTitleBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;

public interface IRuleModeService extends IBaseService<RuleModeBean, Long> {

	/**
	 * @author yxw 2012-4-27
	 * @description:树加载，通过父Id制度模板目录和模板
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildRuleMode(Long pId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:排序节点的父节点ID
	 * @param list
	 * @param pId
	 */
	public void updateSortRuleMode(List<JecnTreeDragBean> list, Long pId) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description:树加载，获取所有的节点
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllNode() throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:树加载，通过父Id制度模板目录
	 * @param pId
	 * @return
	 */
	public List<JecnTreeBean> getChildRuleModeDir(Long pId) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description:树加载，获取所有模板目录
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllRuleModeDir() throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:table加载，移动节点，搜索除了移动节点所在目录下其他关键字name的角色目录
	 * @param name
	 * @param listIds
	 * @return
	 */
	public List<JecnTreeBean> getRuleModeDirsByNameMove(String name, List<Long> listIds) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:移动模板
	 * @param listIds
	 * @param pId
	 *            移动到新节点的Id
	 */
	public void moveRuleModes(List<Long> listIds, Long pId, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description:增加制度模板
	 * @param ruleModeBean制度模板
	 * @param ruleModeTitleBean制度模板标题集合
	 * @return
	 */
	public Long addRuleMode(RuleModeBean ruleModeBean, List<RuleModeTitleBean> ruleModeTitleBean) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description:重命名
	 * @param newName
	 *            新名称
	 * @param id
	 * @param updatePersonId
	 *            修改人
	 * @throws Exception
	 */
	public void updateName(String newName, Long id, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description:更新模板操作
	 * @param ruleModeBean
	 * @param ruleModeTitleBeanList
	 * @throws Exception
	 */
	public void updateRuleMode(RuleModeBean ruleModeBean, List<RuleModeTitleBean> ruleModeTitleBeanList)
			throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description:删除目录或模板
	 * @param ids
	 *            要删除的数据ID
	 * @throws Exception
	 */
	public void deleteRuleMode(List<Long> ids, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description:根据名称搜索制度模板
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> findRuleModeByName(String name) throws Exception;

	/**
	 * @author yxw 2012-5-10
	 * @description:更新时，判断是否与同一父ID下的模板是否重名
	 * @param newName
	 * @param id
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid) throws Exception;

	/**
	 * @author yxw 2012-5-16
	 * @description: 根据模板id查询出此模板所有的标题
	 * @param modeId
	 * @return
	 * @throws Exception
	 */
	public List<RuleModeTitleBean> getRuleModeTitles(Long modeId) throws Exception;

	public Long addRuleModeDir(RuleModeBean ruleModeBean) throws Exception;
}
