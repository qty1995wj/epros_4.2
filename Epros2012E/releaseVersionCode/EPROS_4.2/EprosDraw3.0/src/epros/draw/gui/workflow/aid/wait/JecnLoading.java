package epros.draw.gui.workflow.aid.wait;

import javax.swing.SwingWorker;

import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

/**
 * 
 * 显示等待对话框类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnLoading {
	/** 等待按钮对话框 */
	private static JecnLoadingJDialog dialog = new JecnLoadingJDialog();
	/** 当前正在执行线程 */
	private static SwingWorker currSwingWorker = null;
	/** JecnLoadingJDialog是否显示 true：显示；false：隐藏 */
	private static boolean isDialogShow = false;
	/** SwingWorker中是否执行异常：null或“”正常；非（null或“”）异常 */
	private static String error = null;

	private JecnLoading() {
	}

	/**
	 * 
	 * 开始等待,执行异步操作数据
	 * 
	 * @param paramSwingWorker
	 *            SwingWorker
	 * @return String null:执行成功；非空执行失败
	 */
	public static String start(SwingWorker paramSwingWorker) {
		if (paramSwingWorker == null || dialog.isVisible()) {
			return "JecnLoading:paramSwingWorker null";
		}
		// 注册SwingWorker（true:注册成功；false：注册失败），注册成功才执行SwingWorker中代码
		if (!registerComponent(paramSwingWorker)) {
			return "JecnLoading:currSwingWorker not null";
		}

		try {
			// 初始化状态
			error = null;
			isDialogShow = true;

			// 执行后台处理
			currSwingWorker.execute();
			// 显示等待对话框
			dialog.setVisible(true);

			// SwingWorker中是否执行异常：null或“”正常；非（null或“”）异常
			if (!DrawCommon.isNullOrEmtry(error)) {
				return error;
			}
			return null;
		} catch (Exception e) {
			return "JecnLoading:isError";
		} finally {
			if (dialog.isVisible()) {// 等待对话框显示情况下隐藏对话框
				dialog.setVisible(false);
			}
			isDialogShow = false;
			// 初始化数据
			currSwingWorker = null;

			if (JecnDesignerProcess.getDesignerProcess().getDesigner() != null) {
				// 主要对应横竖分割线自动设置大小问题
				JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
				if (workflow != null) {
					workflow.revalidate();
					workflow.repaint();
				}
			}
		}
	}

	/**
	 * 
	 * 另外一个线程执行方法
	 * 
	 * 停止等待
	 * 
	 */
	public static void stop() {
		if (dialog.isVisible()) {// 显示
			dialog.setVisible(false);
		}
	}

	/**
	 * 
	 * 等待对话框是否显示
	 * 
	 * @return boolean true：显示；false：隐藏
	 */
	public static boolean isDialogShow() {
		return isDialogShow;
	}

	/**
	 * 
	 * swingworker中方法调用代码，其他地方调用注意并发错误
	 * 
	 * @param error
	 *            String
	 */
	public static void setError(String error) {
		JecnLoading.error = error;
	}

	/**
	 * 
	 * 设置显示内容
	 * 
	 * @param text
	 *            String
	 */
	public static void setShowText(String text) {
		if (dialog != null) {
			dialog.setShowText(text);
		}
	}

	/**
	 * 
	 * 设置默认显示内容
	 * 
	 */
	public static void initShowDefaultText() {
		if (dialog != null) {
			dialog.initShowDefaultText();
		}
	}

	/**
	 * 
	 * 注册SwingWorker，注册成功才执行SwingWorker中代码
	 * 
	 * @param paramSwingWorker
	 *            SwingWorker
	 * @return boolean true:注册成功；false：注册失败
	 */
	private static boolean registerComponent(SwingWorker paramSwingWorker) {
		if (currSwingWorker == null) {
			currSwingWorker = paramSwingWorker;
			return true;
		}
		return false;
	}
}
