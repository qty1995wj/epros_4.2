package epros.designer.gui.define;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.gui.file.EditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class EditTermDefineDirDailog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(EditNameDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditTermDefineDirDailog(JecnTreeNode pNode, JTree jTree) {
		this.selectNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		this.setName(selectNode.getJecnTreeBean().getName());
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		try {
			ConnectionPool.getTermDefinitionAction().reName(this.getName(), selectNode.getJecnTreeBean().getId(),
					JecnConstants.getUserId());
			// 重命名
			JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		} catch (Exception e) {
			log.error(JecnProperties.getValue("editTermDefineDirError"), e);
		}
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return ConnectionPool.getTermDefinitionAction().isExistEdit(name, selectNode.getJecnTreeBean().getId(), 0,
				selectNode.getJecnTreeBean().getPid());
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}
}
