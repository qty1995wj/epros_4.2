package epros.draw.gui.figure.shape;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.BaseFlowLevelFigure;

/**
 * 
 * 流程等级图形 四级
 * 
 * @author ZHANGXH
 *
 */
public class FlowLevelFigureFour extends BaseFlowLevelFigure {

	public FlowLevelFigureFour(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
		intLevel = 4;
	}

}
