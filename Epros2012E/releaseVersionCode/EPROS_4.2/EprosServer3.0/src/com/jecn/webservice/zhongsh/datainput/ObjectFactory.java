
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jecn.webservice.zhongsh.datainput package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jecn.webservice.zhongsh.datainput
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetUsersByRoleCode }
     * 
     */
    public GetUsersByRoleCode createGetUsersByRoleCode() {
        return new GetUsersByRoleCode();
    }

    /**
     * Create an instance of {@link GetAllUser2Response }
     * 
     */
    public GetAllUser2Response createGetAllUser2Response() {
        return new GetAllUser2Response();
    }

    /**
     * Create an instance of {@link ArrayOfUser }
     * 
     */
    public ArrayOfUser createArrayOfUser() {
        return new ArrayOfUser();
    }

    /**
     * Create an instance of {@link ClearCache }
     * 
     */
    public ClearCache createClearCache() {
        return new ClearCache();
    }

    /**
     * Create an instance of {@link ClearCacheResponse }
     * 
     */
    public ClearCacheResponse createClearCacheResponse() {
        return new ClearCacheResponse();
    }

    /**
     * Create an instance of {@link GetUserByLoginNameResponse }
     * 
     */
    public GetUserByLoginNameResponse createGetUserByLoginNameResponse() {
        return new GetUserByLoginNameResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link GetUserByLoginName1Response }
     * 
     */
    public GetUserByLoginName1Response createGetUserByLoginName1Response() {
        return new GetUserByLoginName1Response();
    }

    /**
     * Create an instance of {@link GetAllUserResponse }
     * 
     */
    public GetAllUserResponse createGetAllUserResponse() {
        return new GetAllUserResponse();
    }

    /**
     * Create an instance of {@link ClearCachePrefixResponse }
     * 
     */
    public ClearCachePrefixResponse createClearCachePrefixResponse() {
        return new ClearCachePrefixResponse();
    }

    /**
     * Create an instance of {@link SearchUsers1Response }
     * 
     */
    public SearchUsers1Response createSearchUsers1Response() {
        return new SearchUsers1Response();
    }

    /**
     * Create an instance of {@link GetUsersAndRoles }
     * 
     */
    public GetUsersAndRoles createGetUsersAndRoles() {
        return new GetUsersAndRoles();
    }

    /**
     * Create an instance of {@link GetUserTotalCount }
     * 
     */
    public GetUserTotalCount createGetUserTotalCount() {
        return new GetUserTotalCount();
    }

    /**
     * Create an instance of {@link GetUserTotalCountResponse }
     * 
     */
    public GetUserTotalCountResponse createGetUserTotalCountResponse() {
        return new GetUserTotalCountResponse();
    }

    /**
     * Create an instance of {@link GetUsersAndRolesResponse }
     * 
     */
    public GetUsersAndRolesResponse createGetUsersAndRolesResponse() {
        return new GetUsersAndRolesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRole }
     * 
     */
    public ArrayOfRole createArrayOfRole() {
        return new ArrayOfRole();
    }

    /**
     * Create an instance of {@link GetRoleList }
     * 
     */
    public GetRoleList createGetRoleList() {
        return new GetRoleList();
    }

    /**
     * Create an instance of {@link SearchUsers }
     * 
     */
    public SearchUsers createSearchUsers() {
        return new SearchUsers();
    }

    /**
     * Create an instance of {@link GetAllUser2 }
     * 
     */
    public GetAllUser2 createGetAllUser2() {
        return new GetAllUser2();
    }

    /**
     * Create an instance of {@link GetUsersByRoleCodeResponse }
     * 
     */
    public GetUsersByRoleCodeResponse createGetUsersByRoleCodeResponse() {
        return new GetUsersByRoleCodeResponse();
    }

    /**
     * Create an instance of {@link SearchUsers1 }
     * 
     */
    public SearchUsers1 createSearchUsers1() {
        return new SearchUsers1();
    }

    /**
     * Create an instance of {@link HelloWorldResponse }
     * 
     */
    public HelloWorldResponse createHelloWorldResponse() {
        return new HelloWorldResponse();
    }

    /**
     * Create an instance of {@link SearchUsersResponse }
     * 
     */
    public SearchUsersResponse createSearchUsersResponse() {
        return new SearchUsersResponse();
    }

    /**
     * Create an instance of {@link GetDeptListResponse }
     * 
     */
    public GetDeptListResponse createGetDeptListResponse() {
        return new GetDeptListResponse();
    }

    /**
     * Create an instance of {@link GetUserStateResponse }
     * 
     */
    public GetUserStateResponse createGetUserStateResponse() {
        return new GetUserStateResponse();
    }

    /**
     * Create an instance of {@link GetUserByLoginName }
     * 
     */
    public GetUserByLoginName createGetUserByLoginName() {
        return new GetUserByLoginName();
    }

    /**
     * Create an instance of {@link GetUserByLoginName1 }
     * 
     */
    public GetUserByLoginName1 createGetUserByLoginName1() {
        return new GetUserByLoginName1();
    }

    /**
     * Create an instance of {@link GetRoleListResponse }
     * 
     */
    public GetRoleListResponse createGetRoleListResponse() {
        return new GetRoleListResponse();
    }

    /**
     * Create an instance of {@link ClearCachePrefix }
     * 
     */
    public ClearCachePrefix createClearCachePrefix() {
        return new ClearCachePrefix();
    }

    /**
     * Create an instance of {@link GetDeptList }
     * 
     */
    public GetDeptList createGetDeptList() {
        return new GetDeptList();
    }

    /**
     * Create an instance of {@link HelloWorld }
     * 
     */
    public HelloWorld createHelloWorld() {
        return new HelloWorld();
    }

    /**
     * Create an instance of {@link GetAllUser }
     * 
     */
    public GetAllUser createGetAllUser() {
        return new GetAllUser();
    }

    /**
     * Create an instance of {@link GetUserState }
     * 
     */
    public GetUserState createGetUserState() {
        return new GetUserState();
    }

    /**
     * Create an instance of {@link Role }
     * 
     */
    public Role createRole() {
        return new Role();
    }

}
