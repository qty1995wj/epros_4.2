package com.jecn.epros.server.webBean.process;

public class ProcessMapWebBean {
	/**流程ID*/
	private long flowId;
	/**流程名称*/
	private String flowName;
	/**所属流程地图ID*/
	private long pid;
	/**所属流程地图名称*/
	private String pName;
	/**流程编号*/
	private String flowIdInput;// 流程编号
	/**发布时间*/
	private String pubDate;
	/**密级 0是公开，1是密码*/
	private int isPublic;
	public long getFlowId() {
		return flowId;
	}
	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public long getPid() {
		return pid;
	}
	public void setPid(long pid) {
		this.pid = pid;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getFlowIdInput() {
		return flowIdInput;
	}
	public void setFlowIdInput(String flowIdInput) {
		this.flowIdInput = flowIdInput;
	}
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public int getIsPublic() {
		return isPublic;
	}
	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}
	
}
