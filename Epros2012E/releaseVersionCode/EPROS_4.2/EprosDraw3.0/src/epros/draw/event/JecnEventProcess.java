package epros.draw.event;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;

import javax.swing.JComboBox;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.constant.JecnToolbarConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.event.flowElement.JecnDividingResizeEventProcess;
import epros.draw.event.flowElement.JecnEditPortEventProcess;
import epros.draw.event.flowElement.JecnFlowElemPopupMenuProcess;
import epros.draw.event.flowElement.JecnFlowElementEventProcess;
import epros.draw.event.flowElement.JecnFlowResizeActionProcess;
import epros.draw.event.flowElement.JecnLineSegmentResizeProcess;
import epros.draw.event.flowElement.JecnManLineResizeProcess;
import epros.draw.event.flowElement.JecnTextLineResizeProcess;
import epros.draw.event.flowElement.JecnVHLineEventProcess;
import epros.draw.gui.box.JecnNewUpdateTemplateDialog;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.figure.shape.JecnFigureResizePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.resize.JecnDividingLineResizePanel;
import epros.draw.gui.line.resize.JecnLineResizePanel;
import epros.draw.gui.line.resize.JecnLineTextResizePanel;
import epros.draw.gui.line.resize.JecnManhattanLineResizePanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnDialogSelectColor;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.popup.JecnMenuItem;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitleButton;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.visioxml.VisioAction;
import epros.draw.visioxml.visioinput.VisioImportAction;

/**
 * 
 * 按钮处理事件处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEventProcess extends JecnAbstractActionProcess {

	/** 常规 */
	private JecnOftenActionProcess oftenProcess = null;
	/** 文件 */
	private JecnFileActionProcess fileProcess = null;
	/** 编辑 */
	private JecnEditerActionProcess editerProcess = null;
	/** 格式 */
	private JecnFormatActionProcess formatProcess = null;
	/** 流程操作说明 */
	private JecnOperActionProcess operProcess = null;
	/** 帮助 */
	private JecnHelpActionProcess helpProcess = null;
	/** 工具栏：常规 */
	private List<ToolBarElemType> oftenList = null;
	/** 工具栏：文件 */
	private List<ToolBarElemType> fileList = null;
	/** 工具栏：编辑 */
	private List<ToolBarElemType> editList = null;
	/** 工具栏:格式 */
	private List<ToolBarElemType> formatList = null;
	/** 工具栏:帮助 */
	private List<ToolBarElemType> helpList = null;
	/** 工具栏:颜色选择 */
	private List<ToolBarElemType> selectColorList = null;

	/** 画图面板区域的事件处理对象 */
	private JecnTabbedEventProcess tabbedEventProcess = null;
	/** 流程元素鼠标事件处理 */
	private JecnFlowElementEventProcess flowElementEventProcess = null;
	/** 面板图形编辑点鼠标事件处理 (点击连接线，图形显示编辑点，鼠标移动到编辑点，点击、拖拽、释放事件) */
	private JecnEditPortEventProcess jecnEditPortActionProcess = null;

	/** 连接线拖动事件处理类 */
	private JecnLineSegmentResizeProcess jecnLineSegmentActionProcess = null;
	/** 拖动图形8个方向点事件处理 */
	private JecnFlowResizeActionProcess jecnFlowResizeActionProcess = null;

	/** 曼哈顿端点 */
	private JecnManLineResizeProcess jecnManLineResizeProcess = null;

	/** 流程元素右键菜单处理 */
	private JecnFlowElemPopupMenuProcess flowElemPopupMenuProcess = null;

	/** 分割线拖动事件处理 */
	private JecnDividingResizeEventProcess dividingResizeEventProcess = null;

	/** 横竖分割线拖动事件处理 */
	private JecnVHLineEventProcess vhLineEventProcess = null;

	/** 横竖分割线拖动事件处理 */
	private JecnTextLineResizeProcess textLineResizeProcess = null;

	public JecnEventProcess() {
		init();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void init() {
		// 常规
		oftenProcess = new JecnOftenActionProcess();
		// 文件
		fileProcess = new JecnFileActionProcess();
		// 编辑
		editerProcess = new JecnEditerActionProcess();
		// 格式
		formatProcess = new JecnFormatActionProcess();
		// 流程操作说明
		operProcess = new JecnOperActionProcess();
		// 帮助
		helpProcess = new JecnHelpActionProcess();

		// 画图面板区域的事件处理对象
		tabbedEventProcess = new JecnTabbedEventProcess();

		// 文件工具栏
		oftenList = Arrays.asList(JecnToolbarConstant.ToolBarOftenype);
		// 文件工具栏
		fileList = Arrays.asList(JecnToolbarConstant.ToolBarFlieType);
		// 编辑工具栏
		editList = Arrays.asList(JecnToolbarConstant.ToolBarEditType);
		// 格式工具栏
		formatList = Arrays.asList(JecnToolbarConstant.ToolBarFormatType);
		// 帮助工具栏
		helpList = Arrays.asList(JecnToolbarConstant.ToolBarHelpType);
		// 颜色选择工具栏
		selectColorList = Arrays.asList(JecnToolbarConstant.ToolSelectColorType);
		// 流程元素鼠标事件处理
		flowElementEventProcess = new JecnFlowElementEventProcess();
		// 面板图形编辑点鼠标事件处理 (点击连接线，图形显示编辑点，鼠标移动到编辑点，点击、拖拽、释放事件)
		jecnEditPortActionProcess = new JecnEditPortEventProcess();
		// 连接线拖动事件处理类
		jecnLineSegmentActionProcess = new JecnLineSegmentResizeProcess();
		// 拖动图形8个方向点事件处理
		jecnFlowResizeActionProcess = new JecnFlowResizeActionProcess();
		// 曼哈顿端点
		jecnManLineResizeProcess = new JecnManLineResizeProcess();
		// 流程元素右键菜单处理
		flowElemPopupMenuProcess = new JecnFlowElemPopupMenuProcess();
		// 分割线拖动事件处理
		dividingResizeEventProcess = new JecnDividingResizeEventProcess();
		// 横竖分割线事件处理
		vhLineEventProcess = new JecnVHLineEventProcess();

		textLineResizeProcess = new JecnTextLineResizeProcess();
	}

	/**
	 * 
	 * 所有按钮执行入口
	 * 
	 * @param e
	 *            ActionEvent
	 */
	public void actionPerformed(ActionEvent e) {
		processActionPerformed(e);
	}

	/**
	 * 
	 * 所有鼠标事件执行入口
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseEvent(MouseEvent e) {
		mouseEventProcess(e);
	}

	/**
	 * 
	 * 所有鼠标事件执行动作
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 */
	private void mouseEventProcess(MouseEvent e) {
		if (e.getSource() instanceof JecnDrawDesktopPane) {// 画图面板鼠标事件
			drawDesktopPaneProcess(e);
		} else if (e.getSource() instanceof JecnBaseFlowElementPanel || e.getSource() instanceof LineSegment) {// 流程元素鼠标监听事件
			if (e.getSource() instanceof JecnBaseVHLinePanel) {
				draggedVHLineEventProcess(e);
				return;
			}
			drawFlowElementProcess(e);
		} else if (e.getSource() instanceof JecnEditPortPanel) {// 选中工具栏连接线，图形显示编辑点，鼠标移动到编辑点，点击、拖拽、释放事件
			drawEditPortActionProcess(e);
		} else if (e.getSource() instanceof JecnLineResizePanel) {// 连接线拖动事件处理
			draggedLineActionProcess(e);
		} else if (e.getSource() instanceof JecnFigureResizePanel) {// 连接线拖动事件处理
			draggedFigureResizeActionProcess(e);
		} else if (e.getSource() instanceof JecnManhattanLineResizePanel) {// 连接线的首尾选中点，其具有改变连接线所关联的图形
			draggedManLineResizeProcess(e);
		} else if (e.getSource() instanceof JecnDividingLineResizePanel) {// 分割线拖动事件处理
			draggedDividingResizeProcess(e);
		} else if (e.getSource() instanceof JecnTabbedTitleButton) {// 画图面板的标题面板的关闭按钮
			tabbedTitleButtonProcess(e);
		} else if (e.getSource() instanceof JecnLineTextResizePanel) {// 连接线文字区域
			textLineActionProcess(e);
		}
	}

	/**
	 * 
	 * JecnDrawDesktopPane对象所有鼠标事件执行动作
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 */
	private void drawDesktopPaneProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// 按下
			tabbedEventProcess.drawDesktopPaneMousePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// 释放
			JecnTabbedEventProcess.drawDesktopPaneMouseReleased(e);
			break;
		case MouseEvent.MOUSE_MOVED:// 移动
			JecnTabbedEventProcess.mouseMoved(e);
			break;
		case MouseEvent.MOUSE_EXITED:// 移除
			tabbedEventProcess.workflowMouseExit(e);
			break;
		case MouseEvent.MOUSE_ENTERED:// 进入
			JecnTabbedEventProcess.mouseeEntered(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// 拖拽
			JecnTabbedEventProcess.buttonMouseDragged(e);
			break;
		}
	}

	/**
	 * 
	 * JecnTabbedTitleButton对象所有鼠标事件执行动作
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 */
	private void tabbedTitleButtonProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_MOVED:// 移动
			// 画图面板的标题面板的关闭按钮鼠标移动事件
			tabbedEventProcess.tabbedTitleButtonMouseMove(e);
			break;
		case MouseEvent.MOUSE_EXITED:// 移除
			// 画图面板的标题面板的关闭按钮鼠标退出事件
			tabbedEventProcess.tabbedTitleButtonMouseExit(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// 释放
			tabbedEventProcess.tabbedTitleButtonMouseReleased(e);
			break;
		}

	}

	/**
	 * 
	 * 通过命令名称执行相应的动作
	 * 
	 * @param paramString
	 *            命令名称
	 */
	private void processActionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnToolbarContentButton) {// 工具栏内容按钮
			toobarAction(e);
		} else if (e.getSource() instanceof JecnTabbedTitleButton) {// 画图面板选中页标题的按钮
			JecnTabbedTitleButton tabbedTitleButton = ((JecnTabbedTitleButton) e.getSource());
			JecnDrawMainPanel.getMainPanel().removeWolkflow(tabbedTitleButton);
		} else if (e.getSource() instanceof JComboBox) {
			if (JecnActionCommandConstants.ZOOM_COMBO_BOX.equals(e.getActionCommand())) {// 放大缩小下拉框
				oftenProcess.actionPerformed(e, ToolBarElemType.oftenZoomLabel);
			}
		} else if (e.getSource() instanceof JecnMenuItem) {// 弹出菜单项
			popupMenuAction(e);
		}
	}

	/**
	 * 
	 * 弹出菜单动作处理
	 * 
	 * @param e
	 *            ActionEvent 动作事件
	 */
	private void popupMenuAction(ActionEvent e) {
		// 弹出菜单项
		JecnMenuItem menuItem = (JecnMenuItem) e.getSource();

		// 按钮类型
		ToolBarElemType toolBarElemType = menuItem.getToolBarElemType();

		if (toolBarElemType == null) {
			return;
		}

		switch (toolBarElemType) {
		case flowElemAttr: // 元素属性
			flowElemPopupMenuProcess.menuItemActionPerformed(e, toolBarElemType);
			break;
		case activeDetails: // 活动明细
			if (JecnSystemStaticData.isEprosDesigner()) {// 如果是设计器
				JecnDesignerProcess.getDesignerProcess().activeDetail();
			} else {
				flowElemPopupMenuProcess.menuItemActionPerformed(e, toolBarElemType);
			}
			break;
		case roleDetails: // 角色明细
			if (EprosType.eprosDraw == JecnSystemStaticData.getEprosType()) {// 画图面板
				flowElemPopupMenuProcess.menuItemActionPerformed(e, toolBarElemType);
			}
			break;
		case flowElemAlign:// 元素自动对齐
			break;
		case editUpLayer: // 上移一层.
		case editDownLayer: // 下移一层
		case editFrontLayer: // 置于顶层
		case editBackLayer: // 置于底层
		case editCut: // 剪切
		case editCopy: // 复制
		case editPaste: // 粘贴
		case editDelete: // 删除
		case editIconshot: // 截图
			editerProcess.actionPerformed(e, toolBarElemType);
			break;
		case inputVisio: // 导入visio
			VisioImportAction visioImportAction = new VisioImportAction();
			visioImportAction.input();
			break;
		case outputVisio:// 导出visio
			VisioAction visioAction = new VisioAction();
			visioAction.outPut();
			break;
		case formatAntoNum: // 自动编号
			formatProcess.actionPerformed(e, toolBarElemType);
			break;
		case formatRoleMove: // 角色移动
			// 设置新的选中状态
			JecnDrawMainPanel.getMainPanel().getToolbar().getFormatToolBarItemPanel().getRoleMoveCheckBox()
					.setSelected(!JecnSystemStaticData.isShowRoleMove());
			break;
		case formatPLine: // 横分割线
			// 创建分割线对象
			JecnBaseFlowElementPanel pLinePanel = JecnCreateFlowElement.createFlowElementObj(MapElemType.ParallelLines);
			if (pLinePanel == null) {
				return;
			}
			// 添加分割线对象到面板处理事件中
			JecnTabbedEventProcess.tempFlowElementPanel = pLinePanel;
			// 面板处理事件调用添加流程元素方法
			JecnTabbedEventProcess.addFlowElement(new Point(0, JecnDrawMainPanel.getMainPanel().getWorkflow()
					.getStartPoint().y));
			break;
		case formatVLine: // 竖分割线
			// 创建分割线对象
			JecnBaseFlowElementPanel vLinePanel = JecnCreateFlowElement.createFlowElementObj(MapElemType.VerticalLine);
			if (vLinePanel == null) {
				return;
			}
			// 添加分割线对象到面板处理事件中
			JecnTabbedEventProcess.tempFlowElementPanel = vLinePanel;
			// 面板处理事件调用添加流程元素方法
			JecnTabbedEventProcess.addFlowElement(new Point(JecnDrawMainPanel.getMainPanel().getWorkflow()
					.getStartPoint().x, 0));
			break;
		case newTemplate: // 模板片段
			JecnNewUpdateTemplateDialog newTemplate = new JecnNewUpdateTemplateDialog(null);
			newTemplate.setVisible(true);
			break;
		case allSelected:// 全选
			JecnAddFlowElementUnit.selectAllFigure();
			break;
		case modelLine:// 永道
			JecnAddFlowElementUnit.addModelLineAction();
			break;
		}

	}

	/**
	 * 
	 * 工具栏动作处理
	 * 
	 * @param e
	 *            ActionEvent 动作事件
	 */
	private void toobarAction(ActionEvent e) {
		// 源按钮
		JecnToolbarContentButton toolbarContBtn = (JecnToolbarContentButton) e.getSource();
		// 按钮类型
		ToolBarElemType toolBarElemType = toolbarContBtn.getToolBarElemType();

		if (toolBarElemType == null) {
			return;
		}
		if (oftenList.contains(toolBarElemType)) {// 常规
			oftenProcess.actionPerformed(e, toolBarElemType);
		} else if (fileList.contains(toolBarElemType)) {// 文件
			fileProcess.actionPerformed(e, toolBarElemType);
		} else if (editList.contains(toolBarElemType)) {// 编辑
			editerProcess.actionPerformed(e, toolBarElemType);
		} else if (formatList.contains(toolBarElemType)) {// 格式
			formatProcess.actionPerformed(e, toolBarElemType);
		} else if (helpList.contains(toolBarElemType)) {// 帮助
			helpProcess.actionPerformed(e, toolBarElemType);
		} else if (selectColorList.contains(toolBarElemType)) {
			JecnDialogSelectColor dialogSelectColor = new JecnDialogSelectColor(toolBarElemType);
			dialogSelectColor.show((JecnToolbarContentButton) e.getSource(), 0, 25);
		} else {
			switch (toolBarElemType) {// 流程操作说明
			case operSetItem:// 配置流程操作说明
			case operDownload:// 下载流程操作说明
			case operEdit: // 编辑流程操作说明
				operProcess.actionPerformed(e, toolBarElemType);
				break;
			}
		}
	}

	/**
	 * 
	 * JecnBaseFlowBasePane对象所有鼠标事件执行动作
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 */
	private void drawFlowElementProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// 按下
			flowElementEventProcess.flowFigurePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// 释放
			flowElementEventProcess.flowFigureReleased(e);
			break;
		case MouseEvent.MOUSE_MOVED:// 移动
			flowElementEventProcess.mouseMoved(e);
			break;
		case MouseEvent.MOUSE_EXITED:// 退出
			flowElementEventProcess.flowFigureExit(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// 拖动
			JecnFlowElementEventProcess.flowFigureDragged(e);
			break;
		case MouseEvent.MOUSE_CLICKED:// 点击
			break;
		case MouseEvent.MOUSE_ENTERED:// 进入
			flowElementEventProcess.flowFigureEnter(e);
			break;
		}
	}

	/**
	 * 
	 * jecnEditPortActionProcess 点击连接线，图形显示编辑点，鼠标移动到编辑点，点击、拖拽、释放事件
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 */
	private void drawEditPortActionProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// “鼠标按下”事件
			jecnEditPortActionProcess.drawLineMousePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// “鼠标释放”事件
			jecnEditPortActionProcess.drawLineMouseReleased(e);
			break;
		case MouseEvent.MOUSE_MOVED:// “鼠标移动”事件。

			break;
		case MouseEvent.MOUSE_EXITED:// “鼠标离开”事件。
			jecnEditPortActionProcess.drawLineMouseExited(e);
			break;
		case MouseEvent.MOUSE_CLICKED:// “鼠标单击”事件。
			break;
		case MouseEvent.MOUSE_DRAGGED:// “鼠标拖动”事件。
			jecnEditPortActionProcess.drawLineMouseDragged(e);
			break;
		case MouseEvent.MOUSE_ENTERED:// “鼠标进入”事件。
			jecnEditPortActionProcess.drawLineMouseEntered(e);
			break;
		}
	}

	/**
	 * 鼠标点击连接线，拖动连接线事件
	 * 
	 * @param e
	 *            MouseEvent鼠标事件
	 */
	private void draggedLineActionProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// “鼠标按下”事件
			jecnLineSegmentActionProcess.drawLineMousePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// “鼠标释放”事件
			jecnLineSegmentActionProcess.drawLineMouseReleased(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// “鼠标拖动”事件。
			jecnLineSegmentActionProcess.drawLineMouseDragged(e);
			break;
		}
	}

	private void textLineActionProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// “鼠标按下”事件
			textLineResizeProcess.mouseResizePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// “鼠标释放”事件
			textLineResizeProcess.mouseResizeRelease(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// “鼠标拖动”事件。
			textLineResizeProcess.mouseResizeDragged(e);
			break;
		}
	}

	/**
	 * 鼠标拖动图形8个方向点事件处理
	 * 
	 * @param e
	 *            MouseEvent鼠标事件
	 */
	private void draggedFigureResizeActionProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// “鼠标按下”事件
			jecnFlowResizeActionProcess.mouseResizePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// “鼠标释放”事件
			jecnFlowResizeActionProcess.mouseResizeReleased(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// “鼠标拖动”事件。
			jecnFlowResizeActionProcess.mouseResizeDragged(e);
			break;
		}
	}

	/**
	 * 鼠标拖动图形8个方向点事件处理
	 * 
	 * @param e
	 *            MouseEvent鼠标事件
	 */
	private void draggedManLineResizeProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// “鼠标按下”事件
			jecnManLineResizeProcess.mouseResizePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// “鼠标释放”事件
			jecnManLineResizeProcess.mouseResizeRelease(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// “鼠标拖动”事件。
			jecnManLineResizeProcess.mouseResizeDragged(e);
			break;
		}
	}

	/**
	 * 鼠标拖动分割线事件处理
	 * 
	 * @param e
	 *            MouseEvent鼠标事件
	 */
	private void draggedDividingResizeProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// “鼠标按下”事件
			dividingResizeEventProcess.mouseResizePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// “鼠标释放”事件
			dividingResizeEventProcess.mouseResizeReleased(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// “鼠标拖动”事件。
			dividingResizeEventProcess.mouseResizeDragged(e);
			break;
		}
	}

	/**
	 * 鼠标拖动横竖分割线事件处理
	 * 
	 * @param e
	 *            MouseEvent鼠标事件
	 */
	private void draggedVHLineEventProcess(MouseEvent e) {
		if (e == null) {
			return;
		}
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:// “鼠标按下”事件
			vhLineEventProcess.mouseVHLinePressed(e);
			break;
		case MouseEvent.MOUSE_RELEASED:// “鼠标释放”事件
			vhLineEventProcess.mouseVHLineReleased(e);
			break;
		case MouseEvent.MOUSE_DRAGGED:// “鼠标拖动”事件。
			vhLineEventProcess.mouseVHLineDragged(e);
			break;
		case MouseEvent.MOUSE_MOVED:// “鼠标移动”事件。
			vhLineEventProcess.mouseVHLineMove(e);
		}
	}

	protected void actionPerformed(ActionEvent paramActionEvent, ToolBarElemType toolBarElemType) {
	}
}
