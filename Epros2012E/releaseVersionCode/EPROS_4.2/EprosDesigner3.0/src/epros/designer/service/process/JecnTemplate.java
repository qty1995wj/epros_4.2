package epros.designer.service.process;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.jecn.epros.server.bean.process.JecnProcessTemplateBean;
import com.jecn.epros.server.bean.process.JecnProcessTemplateFollowBean;

import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnUtil;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.JecnElement;
import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnXmlUtil;

/**
 * 模板操作表
 * 
 * @author fuzhh
 * 
 */
public class JecnTemplate {
	private static Logger log = Logger.getLogger(JecnTemplate.class);

	/**
	 * 保存模板到数据库
	 * 
	 * @author fuzhh Aug 10, 2012
	 * @param mapType
	 *            类型
	 * @param name
	 *            名称
	 */
	public static void saveTemplateData() {
		if (JecnFlowTemplate.getSaveTemplateData() != null) {
			// 模板保存的主表
			JecnProcessTemplateBean processTemplateBean = null;
			// 模板从表
			JecnProcessTemplateFollowBean processTemplateFollowBean = null;
			// 模板类型
			String mapType = JecnFlowTemplate.getSaveTemplateData().getMapType();
			mapType = replaceType(mapType);
			// 模型名称
			String templateName = JecnFlowTemplate.getSaveTemplateData().getTemplateName();
			// 获取流程主表的bean
			if (JecnDesignerMainPanel.getProcessTemplateMap() != null) {
				processTemplateBean = JecnDesignerMainPanel.getProcessTemplateMap().get(mapType);
			}
			if (processTemplateBean == null) {
				processTemplateBean = new JecnProcessTemplateBean();
			}
			// 模板图片名称
			String imageName = JecnFlowTemplate.getSaveTemplateData().getImagerName();
			// 获取模板从表的bean
			if (JecnDesignerMainPanel.getTemplateFollowMap() != null) {
				processTemplateFollowBean = JecnDesignerMainPanel.getTemplateFollowMap().get(imageName);
			}
			if (processTemplateFollowBean == null) {
				processTemplateFollowBean = new JecnProcessTemplateFollowBean();
			}
			// 获取xml的路径
			String xmlPath = JecnFlowTemplate.getXmlPath(MapType.valueOf(mapType));
			// 读取文件
			File file = new File(xmlPath);
			if (file != null) {
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);
					if (fis != null) {
						// 读取为 byte[]
						int len = fis.available();
						byte[] xmlByte = new byte[len];
						fis.read(xmlByte);
						// 把byte[]数据放入bean中
						processTemplateBean.setBytes(xmlByte);
						processTemplateBean.setType(mapType);
					}
					fis.close();
				} catch (FileNotFoundException e) {
					if (fis != null) {
						try {
							fis.close();
						} catch (IOException e1) {

							log.error("JecnTemplate saveTemplateData is error！" + e1);
						}
					}

					log.error("JecnTemplate saveTemplateData is error！" + e);
				} catch (IOException e) {
					if (fis != null) {
						try {
							fis.close();
						} catch (IOException e1) {

							log.error("JecnTemplate saveTemplateData is error！" + e1);
						}
					}

					log.error("JecnTemplate saveTemplateData is error！" + e);
				}

			} else {
				return;
			}
			String imagerPath = JecnFileUtil.getSyncPath("/epros/draw/configFile/model/images/") + imageName;
			// 读取文件
			File imageFile = new File(imagerPath);
			if (file != null) {
				FileInputStream imageFis = null;
				try {
					imageFis = new FileInputStream(imageFile);
					if (imageFis != null) {
						// 读取为 byte[]
						int imageLen = imageFis.available();
						byte[] imageByte = new byte[imageLen];
						imageFis.read(imageByte);
						// 把byte[]数据放入bean中
						processTemplateFollowBean.setBytes(imageByte);
						// 存放图片名称
						processTemplateFollowBean.setImageName(imageName);
						// 存放片段名称
						processTemplateFollowBean.setName(templateName);
					}
					imageFis.close();
				} catch (FileNotFoundException e) {
					if (imageFis != null) {
						try {
							imageFis.close();
						} catch (IOException e1) {

							log.error("JecnTemplate saveTemplateData is error！" + e1);
						}
					}

					log.error("JecnTemplate saveTemplateData is error！" + e);
				} catch (IOException e) {
					if (imageFis != null) {
						try {
							imageFis.close();
						} catch (IOException e1) {

							log.error("JecnTemplate saveTemplateData is error！" + e1);
						}
					}

					log.error("JecnTemplate saveTemplateData is error！" + e);
				}

			} else {
				return;
			}

			JecnFlowTemplate.setSaveTemplateData(new JecnTemplateData());
			try {
				processTemplateFollowBean.setCreatePeopleId(JecnUtil.getUserId());
				processTemplateFollowBean.setUpdatePeopleId(JecnUtil.getUserId());
				JecnProcessTemplateBean jecnProcessTemplateBean = ConnectionPool.getProcessTemplateAction()
						.saveTemplate(processTemplateBean, processTemplateFollowBean);
				// 获取模型从表
				JecnProcessTemplateFollowBean jecnProcessTemplateFollowBean = jecnProcessTemplateBean
						.getListJecnProcessTemplateFollowBean().get(0);

				// 把模型从表存入设计器
				JecnDesignerMainPanel.getTemplateFollowMap().put(processTemplateFollowBean.getImageName(),
						jecnProcessTemplateFollowBean);
				// 把名称存入设计器
				JecnFlowTemplate.getTemplateNameMap().put(jecnProcessTemplateFollowBean.getImageName(),
						jecnProcessTemplateFollowBean.getName());
				// 把模型主表存入设计器
				JecnDesignerMainPanel.getProcessTemplateMap().put(processTemplateBean.getType(),
						jecnProcessTemplateBean);
			} catch (Exception e) {
				log.error("JecnTemplate saveTemplateData is error!", e);

			}
		}
	}

	/**
	 * 查询模板数据到本地
	 * 
	 * @author fuzhh Aug 10, 2012
	 * @param processTemplateBean
	 *            模板的bean
	 */
	public static void readTemplateDataToWorkflow() {
		JecnDesignerMainPanel.getTemplateFollowMap().clear();
		JecnFlowTemplate.getTemplateNameMap().clear();
		JecnDesignerMainPanel.getProcessTemplateMap().clear();
		JecnSystemData.getTemplateDataList().clear();
		JecnSystemData.setTemplateData(new JecnTemplateData());
		JecnSystemData.getTemplateMap().clear();
		JecnSystemData.setTotalMapCount(0);
		JecnSystemData.setPartMapCount(0);

		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		List<JecnProcessTemplateBean> processTemplateBeanList = null;
		List<MapType> mapTypeList = new ArrayList<MapType>();
		mapTypeList.add(MapType.partMap);
		mapTypeList.add(MapType.totalMap);
		// 调用后台查询的方法
		try {
			processTemplateBeanList = ConnectionPool.getProcessTemplateAction().getJecnProcessTemplateBeanList();
		} catch (Exception e) {
			log.error("JecnTemplate readTemplateDataToWorkflow is error！", e);

		}
		// 图片存储的完整路径名
		String imagerPath = JecnFileUtil.getSyncPath("/epros/draw/configFile/model/images/");
		if (processTemplateBeanList != null) {
			for (JecnProcessTemplateBean processTemplateBean : processTemplateBeanList) {
				// 获取xml的路径
				String xmlPath = JecnFlowTemplate.getXmlPath(MapType.valueOf(processTemplateBean.getType()));
				for (int i = 0; i < mapTypeList.size(); i++) {
					MapType mapType = mapTypeList.get(i);
					if (mapType == MapType.valueOf(processTemplateBean.getType())) {
						mapTypeList.remove(i);
					}
				}
				try {
					File dir = new File(xmlPath);
					if (dir != null) {
						// 删除xml
						dir.delete();
					}
					fos = new FileOutputStream(dir);
					bos = new BufferedOutputStream(fos);
					bos.write(processTemplateBean.getBytes());
				} catch (Exception e) {
					log.error("JecnTemplate readTemplateDataToWorkflow is error！", e);

				} finally {
					if (bos != null) {
						try {
							bos.close();
						} catch (IOException e1) {
							log.error("JecnTemplate readTemplateDataToWorkflow is error！", e1);

						}
					}
					if (fos != null) {
						try {
							fos.close();
						} catch (IOException e1) {
							log.error("JecnTemplate readTemplateDataToWorkflow is error！", e1);

						}
					}
				}
				// 把模型主表存入设计器
				JecnDesignerMainPanel.getProcessTemplateMap().put(processTemplateBean.getType(), processTemplateBean);

				// 循环图片
				for (JecnProcessTemplateFollowBean processTemplateFollowBean : processTemplateBean
						.getListJecnProcessTemplateFollowBean()) {
					String path = imagerPath + processTemplateFollowBean.getImageName();
					try {
						File imageDir = new File(path);
						if (!imageDir.exists()) {// 判断文件目录是否存在
							fos = new FileOutputStream(imageDir);
							bos = new BufferedOutputStream(fos);
							bos.write(processTemplateFollowBean.getBytes());
						}
					} catch (Exception e) {
						log.error("JecnTemplate readTemplateDataToWorkflow is error！", e);

					} finally {
						if (bos != null) {
							try {
								bos.close();
							} catch (IOException e1) {
								log.error("JecnTemplate readTemplateDataToWorkflow is error！", e1);

							}
						}
						if (fos != null) {
							try {
								fos.close();
							} catch (IOException e1) {
								log.error("JecnTemplate readTemplateDataToWorkflow is error！", e1);

							}
						}
					}
					// 把模型从表存入设计器
					JecnDesignerMainPanel.getTemplateFollowMap().put(processTemplateFollowBean.getImageName(),
							processTemplateFollowBean);
					// 把名称存入设计器
					JecnFlowTemplate.getTemplateNameMap().put(processTemplateFollowBean.getImageName(),
							processTemplateFollowBean.getName());
				}
			}
		}

		File file = new File(imagerPath);
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files) {
				boolean isDelete = true;
				for (Map.Entry<String, String> templateNameMap : JecnFlowTemplate.getTemplateNameMap().entrySet()) {
					if (f.getName().equals(templateNameMap.getKey())) {
						isDelete = false;
					}
				}
				if (isDelete) {
					String path = imagerPath + f.getName();
					File dir = new File(path);
					if (dir.exists()) {
						// 删除xml
						dir.delete();
					}
				}
			}
		}

		for (MapType mapType : mapTypeList) {
			// 获取xml的路径
			String partXmlPath = JecnFlowTemplate.getXmlPath(mapType);
			File dir = new File(partXmlPath);
			if (dir.exists()) {
				// 删除xml
				dir.delete();
			}
			Document doc = JecnXmlUtil.getDocument();
			JecnElement info = new JecnElement(doc.createElement("mainPart"));
			doc.appendChild(info.getElement());
			JecnXmlUtil.writeXmlByDocumentAndAllPath(partXmlPath, doc);
		}
	}

	/**
	 * 修改的模型
	 * 
	 * @author fuzhh Aug 10, 2012
	 */
	public static void updateTemplate() {
		if (JecnFlowTemplate.getUpdateTemplateMap().size() > 0) {
			// 需要修改的ID
			long imageId = -1L;
			// 需要修改的名称
			String updateName = null;
			// 循环Map
			Iterator<String> iterator = JecnFlowTemplate.getUpdateTemplateMap().keySet().iterator();
			while (iterator.hasNext()) {
				// 获取图片名称
				String imageName = iterator.next();
				// 获取从表的bean
				JecnProcessTemplateFollowBean processTemplateFollowBean = JecnDesignerMainPanel.getTemplateFollowMap()
						.get(imageName);
				// 获取ID
				imageId = processTemplateFollowBean.getId();
				// 需要修改的名称
				updateName = JecnFlowTemplate.getUpdateTemplateMap().get(imageName);
			}
			// 重置Map
			JecnFlowTemplate.setUpdateTemplateMap(new HashMap<String, String>());
			// 调用后台修改的方法
			try {
				ConnectionPool.getProcessTemplateAction().updateTemplate(imageId, updateName, JecnUtil.getUserId());
			} catch (Exception e) {
				log.error("JecnTemplate readTemplateDataToWorkflow is error！！", e);

			}
		}
	}

	/**
	 * 删除的模型
	 * 
	 * @author fuzhh Aug 10, 2012
	 */
	public static void deleteTemplate() {
		if (JecnFlowTemplate.getDeleteTemplateMap().size() > 0) {
			// 循环Map
			Iterator<String> iterator = JecnFlowTemplate.getDeleteTemplateMap().keySet().iterator();
			// 主表的bean
			JecnProcessTemplateBean processTemplateBean = null;
			// 需要删除从表的主键ID
			long imagerId = -1L;
			while (iterator.hasNext()) {
				// 获取类型
				String mapType = iterator.next();
				// 通过类型获取主表bean
				processTemplateBean = JecnDesignerMainPanel.getProcessTemplateMap().get(mapType);
				// 获取xml的路径
				String xmlPath = JecnFlowTemplate.getXmlPath(MapType.valueOf(mapType));
				// 读取文件
				File file = new File(xmlPath);
				if (file != null) {
					try {
						FileInputStream fis = new FileInputStream(file);
						if (fis != null) {
							// 读取为 byte[]
							int len = fis.available();
							byte[] xmlByte = new byte[len];
							fis.read(xmlByte);
							// 把byte[]数据放入bean中
							processTemplateBean.setBytes(xmlByte);
						}
					} catch (FileNotFoundException e) {

						log.error("JecnTemplate deleteTemplate is error ！" + e);
					} catch (IOException e) {

						log.error("JecnTemplate deleteTemplate is error ！" + e);
					}
				} else {
					return;
				}
				// 获取图片名称
				String imagerName = JecnFlowTemplate.getDeleteTemplateMap().get(mapType);
				// 通过名称获取从表的bean
				JecnProcessTemplateFollowBean processTemplateFollowBean = JecnDesignerMainPanel.getTemplateFollowMap()
						.get(imagerName);
				// 给从表主键ID赋值
				imagerId = processTemplateFollowBean.getId();

				// 把模型从表存入设计器
				JecnDesignerMainPanel.getTemplateFollowMap().remove(imagerName);

				// 把名称存入设计器
				JecnFlowTemplate.getTemplateNameMap().remove(imagerName);
			}
			// 重置集合
			JecnFlowTemplate.setDeleteTemplateMap(new HashMap<String, String>());
			// 调用后台删除的方法
			try {
				processTemplateBean.setUpdatePeopleId(JecnUtil.getUserId());
				ConnectionPool.getProcessTemplateAction().deleteTemplate(processTemplateBean, imagerId);
			} catch (Exception e) {
				log.error("JecnTemplate deleteTemplate is error！", e);

			}
		}
	}

	private static String replaceType(String mapType) {
		return mapType.equals(MapType.totalMapRelation.toString()) ? MapType.totalMap.toString() : mapType;
	}
}
