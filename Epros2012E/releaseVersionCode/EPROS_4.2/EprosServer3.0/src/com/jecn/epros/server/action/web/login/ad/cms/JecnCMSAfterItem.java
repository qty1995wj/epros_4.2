package com.jecn.epros.server.action.web.login.ad.cms;

import java.util.ResourceBundle;

/**
 * 招商证券
 * 
 * @author ZXH
 * @date 2018-03-26
 */
public class JecnCMSAfterItem {

	private static ResourceBundle cmsConfig;

	static {
		cmsConfig = ResourceBundle.getBundle("cfgFile/cms/cmsServerConfig");
	}

	public static String getValue(String key) {
		return cmsConfig.getString(key);
	}
}
