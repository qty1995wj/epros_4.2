package epros.designer.gui.process.flow;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.ProcessFileNodeData;
import com.jecn.epros.server.bean.process.ProcessInfoBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.process.flow.file.FlowFilePropertyPanel;
import epros.designer.gui.process.flow.file.FlowRelatedRiskPanel;
import epros.designer.gui.process.flow.file.FlowRelatedRulePanel;
import epros.designer.gui.process.flow.file.FlowRelatedStandardPanel;
import epros.designer.gui.process.flow.file.FlowRelatedStandardizedFilePanel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程文件
 * 
 * @author 2012-07-04
 * 
 */
public class BaseFlowFileDialog extends JecnDialog {
	protected static Logger log = Logger.getLogger(BaseFlowFileDialog.class);

	protected JecnTreeNode selectNode = null;

	protected JTabbedPane tabPanel = null;
	protected FlowFilePropertyPanel filePropertyPanel = null;
	/** 相关标准 */
	protected FlowRelatedStandardPanel relatedStandard = null;
	/** 相关制度 */
	protected FlowRelatedRulePanel relatedRule = null;
	protected FlowRelatedRiskPanel relatedRisk = null;
	/** 相关标准化文件 */
	protected FlowRelatedStandardizedFilePanel relatedStandardizedFile = null;

	protected JScrollPane filePropertyScrollPanel;
	protected JScrollPane standardScrollPanel;
	protected JScrollPane ruleScrollPanel;
	protected JScrollPane riskScrollPanel;
	protected JScrollPane standardizedFileScrollPanel;

	protected JecnPanel buttonPanel = null;
	/** 确认按钮 */
	protected JButton okBut;
	/** 取消 */
	protected JButton cancelBut;

	protected Long id = null;
	/** 验证提示 */
	protected JLabel verfyLab = new JLabel();
	List<JecnTreeBean> treeBeanRelateStandardList = null;
	List<JecnTreeBean> treeBeanRelateRuleList = null;
	List<JecnTreeBean> treeBeanRelateRiskList = null;
	List<JecnTreeBean> treeBeanRelateFileList = null;
	boolean isAdd;

	public BaseFlowFileDialog(JecnTreeNode selectNode, boolean isAdd) {
		this.selectNode = selectNode;
		this.isAdd = isAdd;
		initData();
		initComponent();
		initLayout();
		initListener();
	}

	private void initData() {
		if (!isAdd && selectNode != null && selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processFile) {
			id = selectNode.getJecnTreeBean().getId();
		} else {
			treeBeanRelateStandardList = new ArrayList<JecnTreeBean>();
			treeBeanRelateRuleList = new ArrayList<JecnTreeBean>();
			treeBeanRelateRiskList = new ArrayList<JecnTreeBean>();
			treeBeanRelateFileList = new ArrayList<JecnTreeBean>();
			return;
		}
		try {
			treeBeanRelateStandardList = ConnectionPool.getStandardAction().getStandardByFlowId(id);
			treeBeanRelateRuleList = ConnectionPool.getRuleAction().getRuleTreeBeanByFlowId(id);
			treeBeanRelateRiskList = ConnectionPool.getProcessAction().getRelatedRiskList(id);
			treeBeanRelateFileList = ConnectionPool.getProcessAction().getStandardizedFiles(id);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			log.error("BaseFlowFileDialog initData is error！", e);
			return;
		}
	}

	private void initComponent() {
		// 设置窗体大小
		this.setSize(630, 700);
		if (isAdd) {
			this.setTitle(JecnProperties.getValue("addProcessFile"));
		} else {
			this.setTitle(JecnProperties.getValue("editProcessFile"));
		}
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);

		tabPanel = new JTabbedPane();
		// 流程属性
		filePropertyPanel = new FlowFilePropertyPanel(selectNode, this, isAdd);

		relatedStandard = new FlowRelatedStandardPanel(treeBeanRelateStandardList);
		relatedRule = new FlowRelatedRulePanel(treeBeanRelateRuleList);

		relatedRisk = new FlowRelatedRiskPanel(treeBeanRelateRiskList);

		relatedStandardizedFile = new FlowRelatedStandardizedFilePanel(treeBeanRelateFileList);

		filePropertyScrollPanel = new JScrollPane();
		filePropertyScrollPanel.setViewportView(filePropertyPanel);

		standardScrollPanel = new JScrollPane();
		standardScrollPanel.setViewportView(relatedStandard);

		ruleScrollPanel = new JScrollPane();
		ruleScrollPanel.setViewportView(relatedRule);

		riskScrollPanel = new JScrollPane();
		riskScrollPanel.setViewportView(relatedRisk);
		// 标准化文件
		standardizedFileScrollPanel = new JScrollPane();
		standardizedFileScrollPanel.setViewportView(relatedStandardizedFile);

		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		filePropertyScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		standardScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ruleScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		riskScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		standardizedFileScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 添加ui
		tabPanel.setUI(new JecnWindowsTabbedPaneUI());

		verfyLab.setForeground(Color.red);

		buttonPanel = new JecnPanel();
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	}

	private void initLayout() {
		this.getContentPane().add(tabPanel, BorderLayout.CENTER);

		int count = 0;
		tabPanel.add(filePropertyScrollPanel, count);
		tabPanel.setTitleAt(count, JecnProperties.getValue("flowAttrBtn"));
		count++;
		tabPanel.add(standardizedFileScrollPanel, count);
		tabPanel.setTitleAt(count, JecnProperties.getValue("standardFile"));
		count++;
		tabPanel.add(standardScrollPanel, count);
		tabPanel.setTitleAt(count, JecnProperties.getValue("relatedOrder"));
		count++;
		tabPanel.add(ruleScrollPanel, count);
		tabPanel.setTitleAt(count, JecnProperties.getValue("relatedRule"));
		count++;
		tabPanel.add(riskScrollPanel, count);
		tabPanel.setTitleAt(count, JecnProperties.getValue("relatedRisk"));
		count++;

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
	}

	private void initListener() {
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButton();
			}
		});
		cancelBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	protected void okButton() {

	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	protected boolean isUpdate() {
		if (relatedStandard.isUpdate() || filePropertyPanel.isUpdate() || relatedRule.isUpdate()
				|| relatedRisk.isUpdate() || relatedStandardizedFile.isUpdate()) {
			return true;
		}
		return false;
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButton();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	protected String isValidate() {
		return filePropertyPanel.isValidate();
	}

	protected void commonProcessFileData(ProcessFileNodeData processFileData) {
		ProcessInfoBean processInfoBean = filePropertyPanel.okBut();
		processFileData.setInfoBean(processInfoBean);
		processFileData.setRelatedStandardizeds(relatedStandardizedFile.getTreeBeanList());
		processFileData.setRelatedStandards(relatedStandard.getTreeBeanList());
		processFileData.setRelatedRisks(relatedRisk.getTreeBeanList());
		processFileData.setRelatedRules(relatedRule.getTreeBeanList());
		// 流程（文件）
		processFileData.setFileContent(filePropertyPanel.getFileContent());
		processFileData.setCreatePeopleId(JecnConstants.getUserId());
	}

	/***************************************************************************
	 * 确定
	 */
	private void okButPerformed() {
		this.setVisible(false);
	}
}
