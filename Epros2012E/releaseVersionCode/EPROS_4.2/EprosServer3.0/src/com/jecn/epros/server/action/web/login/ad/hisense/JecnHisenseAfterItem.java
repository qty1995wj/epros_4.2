package com.jecn.epros.server.action.web.login.ad.hisense;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 海信配置文件读取类（EprosServer3.0/src/cfgFile/hisense/ldapServerConfig.properties）
 * 
 * @author zhouxy
 * 
 */
public class JecnHisenseAfterItem {

	private static Logger log = Logger.getLogger(JecnHisenseAfterItem.class);

	// Ldap上下文工厂
	public static String LDAP_CONTEXT_FACTORY = null;
	// Ldap 服务器地址
	public static String LDAP_URL = null;
	// Ldap 权限验证类型
	public static String AUTH_TYPE = null;
	// Ldap epros用户组基准DN
	public static String EPROS_GROUP_DN = null;
	// Ldap 用户组基准DN
	public static String PEOPLE_DN = null;
	// 人员同步--》是否允许错误数据
	public static boolean ALLOW_ERROR = false;

	/**
	 * 海信单点登录 读取本地文件数据
	 * 
	 */
	public static void start() {

		log.info("开始读取系统配置表中相关数据......");

		/** 获取单点登录配置文件信息 */
		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH
				+ "cfgFile/hisense/ldapServerConfig.properties";

		log.info("配置文件路径：" + propsURL);

		FileInputStream in = null;

		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			// Ldap上下文工厂
			String ldapCtxFactory = props.getProperty("contextFactory");
			if (!JecnCommon.isNullOrEmtryTrim(ldapCtxFactory)) {
				JecnHisenseAfterItem.LDAP_CONTEXT_FACTORY = ldapCtxFactory;
			}

			// Ldap 服务器地址
			String ldapURL = props.getProperty("ldapURL");
			if (!JecnCommon.isNullOrEmtryTrim(ldapURL)) {
				JecnHisenseAfterItem.LDAP_URL = ldapURL;
			}

			// Ldap 权限验证类型
			String authType = props.getProperty("authType");
			if (!JecnCommon.isNullOrEmtryTrim(authType)) {
				JecnHisenseAfterItem.AUTH_TYPE = authType;
			}

			// Ldap epros用户组基准DN
			String eprosGroupDN = props.getProperty("eprosGroupDN");
			if (!JecnCommon.isNullOrEmtryTrim(eprosGroupDN)) {
				JecnHisenseAfterItem.EPROS_GROUP_DN = eprosGroupDN;
			}
			// Ldap 用户组基准DN
			String pepleDN = props.getProperty("peopleDN");
			if (!JecnCommon.isNullOrEmtryTrim(pepleDN)) {
				JecnHisenseAfterItem.PEOPLE_DN = pepleDN;
			}
			// 人员同步--》是否允许错误数据
			String allowError = props.getProperty("allowError");
			if (!JecnCommon.isNullOrEmtryTrim(allowError)) {
				JecnHisenseAfterItem.ALLOW_ERROR = Boolean.valueOf(allowError);
			}

		} catch (Exception e) {
			log.error("读取单点登录配置文件信息IO流错误！", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("关闭流错误！", e1);
				}
			}
		}
	}

}
