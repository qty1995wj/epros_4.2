package com.jecn.epros.server.dao.popedom.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.popedom.IPositionFigInfoDao;

public class PositionFigInfoDaoImpl extends
		AbsBaseDao<JecnPositionFigInfo, Long> implements IPositionFigInfoDao {
	/**
	 * 
	 * 获得岗位在项目下参与的角色(没有岗位组)
	 * 
	 * @author zhangchen
	 * @param peopleId
	 * @param projectId
	 * @return
	 * @throws DaoException
	 */
	@Override
	public List<Object[]> getRoleNOGroupByPosId(Long posId, Long projectId)
			throws Exception {
		StringBuffer str = new StringBuffer();
		str.append("select b.figureId,b.figureText,b.roleRes,a.flowId,a.flowName,a.flowIdInput,c.figurePositionId from");
		str.append(" JecnFlowStructureImage as b,JecnFlowStructure as a,JecnFlowStation as c where");
		str.append(" c.figureFlowId = b.figureId and c.type = '0'");
		str.append("  and b.flowId = a.flowId and a.delState=0 and a.projectId =?");
		str.append("  and c.figurePositionId =?");
		str.append("  order by a.perFlowId,a.sortId,a.flowId");
		return this.listHql(str.toString(), projectId, posId);
	}

	@Override
	public List<Object[]> getRoleGroupByPosId(Long posId, Long projectId)
			throws Exception {
		StringBuffer str = new StringBuffer();
		str.append("select b.figureId,b.figureText,b.roleRes,a.flowId,a.flowName,a.flowIdInput,d.figureId from");
		str.append(" JecnFlowStructureImage as b,JecnFlowStructure as a,JecnFlowStation as c,JecnPositionGroupOrgRelated as d where");
		str.append(" c.figureFlowId = b.figureId and c.type='1'");
		str.append(" and b.flowId = a.flowId and a.delState=0 and a.projectId =?");
		str.append(" and c.figurePositionId = d.groupId and d.figureId =?");
		str.append(" order by a.perFlowId,a.sortId,a.flowId");
		return this.listHql(str.toString(), projectId, posId);
	}

	@Override
	public List<Object[]> getActiveByRoleListIds(Set<Long> figureIdsList)
			throws Exception {
		StringBuffer str = new StringBuffer();
		str.append("select a.figureId,a.activityId,a.figureText,b.figureRoleId");
		str.append(" from JecnFlowStructureImage as a,JecnRoleActive as b where");
		str.append(" a.figureType in" + JecnCommonSql.getActiveString()
				+ " and a.figureId=b.figureActiveId and b.figureRoleId in");
		List<String> listStr = JecnCommonSql.getSetSqlAddBracket(
				str.toString(), figureIdsList);
		List<Object[]> listResult = new ArrayList<Object[]>();
		List<Object> checkRepeat = new ArrayList<Object>();
		for (String hql : listStr) {
			List<Object[]> listObj = this.listHql(hql);
			for (Object[] obj : listObj) {
				if(obj==null||obj[0]==null){
					continue;
				}
				if(!checkRepeat.contains(obj[0])){
					listResult.add(obj);
					checkRepeat.add(obj[0]);
				}
			}
		}
		return listResult;
	}
}
