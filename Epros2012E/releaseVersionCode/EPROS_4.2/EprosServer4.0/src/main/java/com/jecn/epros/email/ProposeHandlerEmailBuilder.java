package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.propose.Propose;
import com.jecn.epros.sqlprovider.server3.Common;

public class ProposeHandlerEmailBuilder extends DynamicContentEmailBuilder {

    @Override
    protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {
        Propose rtnlPropose = (Propose) getData()[0];
        // 邮件标题
        String subject = initSubject();
        String content = initContent(user, rtnlPropose);

        EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
        emailBasicInfo.setSubject(subject);
        emailBasicInfo.setContent(content);
        emailBasicInfo.addRecipients(user);
        return emailBasicInfo;
    }

    private String initContent(JecnUser user, Propose rtnlPropose) {
        // 欢迎词
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("您好!此邮件为合理化建议邮件");
        strBuf.append(EmailContentBuilder.strBr);

        // 提交时间
        strBuf.append("提交时间：");
        strBuf.append(Common.getStringbyDate(rtnlPropose.getCreateTime()));
        strBuf.append(EmailContentBuilder.strBr);

        // 提交人
        strBuf.append("提交人：");
        strBuf.append(rtnlPropose.getCreatePeopoleName());
        strBuf.append(EmailContentBuilder.strBr);

        // 关联文件的名称 合理化建议类型：0：流程；1：制度
        if (rtnlPropose.getType() == 0) {
            strBuf.append("流程名称：");
        } else if (rtnlPropose.getType() == 1) {
            strBuf.append("制度名称：");
        }
        strBuf.append(rtnlPropose.getRelationName());
        strBuf.append(EmailContentBuilder.strBr);

        // 提交意见
        strBuf.append("提交意见：");
        strBuf.append(rtnlPropose.getProposeContent());
        strBuf.append(EmailContentBuilder.strBr);

        EmailContentBuilder builder = new EmailContentBuilder(getTip());
        builder.setContent(strBuf.toString());
        builder.setResourceUrl(getProposeAddress(user.getId(), rtnlPropose.getId()));
        return builder.buildContent();
    }

    /**
     * 生成标题
     *
     * @return
     */
    private String initSubject() {
        return "合理化建议" + "-" + "提交意见";
    }

    /**
     * 处理链接
     *
     * @param peopleId 　人员id
     * @param rtnlId   合理化建议id
     * @return
     */
    private String getProposeAddress(Long peopleId, String rtnlId) {
        return "/loginMail.action?mailPeopleId=" + peopleId + "&accessType=mailOpenPropose" + "&rtnlProposeId="
                + rtnlId;
    }
}
