package epros.designer.gui.rule;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

public class EditRuleByViewCodeDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(EditRuleByViewCodeDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 控件显示面板 */
	private JPanel topPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = null;

	/** 编号Lab */
	private JLabel curViewCodeLabel = null;

	/** 编号填写框 */
	private JTextField curViewCodeTextField = null;

	/** 编号Lab */
	private JLabel viewCodeLabel = null;

	/** 编号填写框 */
	private JTextField viewCodeTextField = null;

	/** 更新文件 */
	private JLabel updateFileLabel = null;

	/** 更新文件 */
	private JTextField updateFileTextField = null;

	private JButton selectBut = null;

	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBut = null;

	/** 判断是否点击的确定按钮 */
	private boolean isOk = false;

	/** 设置面板控件大小 */
	Dimension dimension = null;

	private JComboBox comboBox = null;

	private JecnTreeNode selectNode;

	private JTree jTree;

	private File selectFile = null;

	private JLabel errorLable = null;

	public EditRuleByViewCodeDialog(JTree jTree, JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		initCompotents();
		initLayout();
		initData();
		this.setModal(true);
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
	}

	private void initData() {
		curViewCodeTextField.setText(selectNode.getJecnTreeBean().getNumberId());
		viewCodeTextField.setText(initViewCodeByCurCode(curViewCodeTextField.getText()));
	}

	private String name = "";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.curViewCodeTextField.setText(name);
	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();
		// 控件显示面板
		topPanel = new JPanel();
		// 按钮面板
		buttonPanel = new JPanel();
		// 编号Lab
		curViewCodeLabel = new JLabel();
		// 编号填写框
		curViewCodeTextField = new JTextField();
		curViewCodeTextField.setEditable(false);

		viewCodeLabel = new JLabel(JecnProperties.getValue("changeVerNum"));
		viewCodeTextField = new JTextField();
		viewCodeTextField.setEditable(false);
		/** 更新文件 */
		updateFileLabel = new JLabel(JecnProperties.getValue("updateFileC"));
		updateFileTextField = new JTextField();
		updateFileTextField.setEditable(false);
		selectBut = new JButton(JecnProperties.getValue("selectBtn"));

		errorLable = new JLabel();
		errorLable.setForeground(Color.RED);

		// 设置Dialog大小
		this.setSize(getWidthMin(), 160);
		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

		String version = JecnConfigTool.getItemBean(ConfigItemPartMapMark.process_level).getValue();
		comboBox = new JComboBox(StringUtils.isBlank(version) ? null : version.split("\n"));
		comboBox.setPreferredSize(new Dimension(56, 23));
		comboBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置控件显示面板的默认背景色
		topPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置按钮面板的默认背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 为确认按钮增加回车事件
		this.getRootPane().setDefaultButton(okBut);

	}

	/**
	 * 布局
	 */
	private void initLayout() {
		// 主面板布局
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(3, 5, 3, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(topPanel, c);
		// 按钮面板 布局
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);

		// 控件显示面板 布局
		topPanel.setLayout(new GridBagLayout());

		int topIndex = 0;
		// 编号Lab
		c = new GridBagConstraints(0, topIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		topPanel.add(curViewCodeLabel, c);
		// 编号填写框
		c = new GridBagConstraints(1, topIndex, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		topPanel.add(curViewCodeTextField, c);
		topIndex++;

		// 编号预览
		c = new GridBagConstraints(0, topIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		topPanel.add(viewCodeLabel, c);
		// 编号填写框
		c = new GridBagConstraints(1, topIndex, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		topPanel.add(viewCodeTextField, c);
		// 版本
		c = new GridBagConstraints(2, topIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		topPanel.add(comboBox, c);

		topIndex++;

		// 更新文件选择
		c = new GridBagConstraints(0, topIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		topPanel.add(updateFileLabel, c);
		c = new GridBagConstraints(1, topIndex, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		topPanel.add(updateFileTextField, c);
		c = new GridBagConstraints(2, topIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		topPanel.add(selectBut, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		buttonPanel.add(errorLable);
		// 确定
		buttonPanel.add(okBut);
		// 取消
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

		// 确定按钮事件监听
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消按钮事件监听
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
		selectBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateLocalRuleFile();
			};
		});

		comboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					comboBoxItemAction();
				}
			}
		});
		// Dialog标题
		this.setTitle(JecnProperties.getValue("updateRuleFile"));

		// 设置编号Lab的值
		this.curViewCodeLabel.setText(JecnProperties.getValue("actBaseNumC"));

		// 设置验证提示Label的大小
		dimension = new Dimension(240, 13);

		// 设置编号显示框JTextField的大小
		dimension = new Dimension(240, 20);
		this.curViewCodeTextField.setPreferredSize(dimension);
		curViewCodeTextField.setMinimumSize(dimension);
		curViewCodeTextField.setMaximumSize(dimension);
		// 设置Dialog的大小不被改变
		this.setResizable(false);
	}

	/**
	 * 下拉框 切换时间 处理
	 */
	private void comboBoxItemAction() {
		reSetViewCodeByCurCode(comboBox.getSelectedItem().toString());
	}

	private void updateLocalRuleFile() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSavaRuleUpdateDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
		fileChoose.setEidtTextFiled(false);

		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("updateFile"));

		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("updateFile"));
		int i = fileChoose.showSaveDialog(this);
		if (i == JFileChooser.APPROVE_OPTION) {
			selectFile = fileChoose.getSelectedFile();
			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSavaRuleUpdateDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			// 判断同一父节点下该文件ID是否存在
			if (JecnTreeCommon.validateRepeatNameEidt(selectFile.getName(), selectNode)) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("fileNameHaved"));
				return;
			}
			updateFileTextField.setText(selectFile.getName());
		}
	}

	// 确定事件
	public void okButtonAction() {
		name = curViewCodeTextField.getText().trim();
		curViewCodeTextField.setText(name);
		if (selectFile == null) {
			errorLable.setText(JecnProperties.getValue("changeVerFileNotNull"));
			return;
		}
		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		try {
			// 需要更新的信息
			RuleT ruleT = new RuleT();
			ruleT.setRuleName(selectFile.getName());
			ruleT.setFileBytes(JecnUtil.changeFileToByte(selectFile.getPath()));
			ruleT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			ruleT.setUpdatePeopleName(JecnConstants.loginBean.getJecnUser().getTrueName());
			ruleT.setProjectId(JecnConstants.projectId);
			ruleT.setRuleNumber(viewCodeTextField.getText().trim());
			ruleT.setId(treeBean.getId());
			Long relationId = ConnectionPool.getRuleAction().updateLocalRuleFile(ruleT);
			if (relationId == null) {
				return;
			}
			treeBean.setRelationId(relationId);
			JecnTreeCommon.reNameNode(jTree, selectNode, JecnUtil.getFileName(selectFile.getPath()));
			JecnTreeCommon.selectNode(jTree, selectNode);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("updateSuccess"));
		} catch (Exception e) {
			log.error("EditRuleByViewCodeDialog okButtonAction is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("localUpdateError"));
			return;
		}

		this.dispose();
		isOk = true;
	}

	private String initViewCodeByCurCode(String curCode) {
		if (StringUtils.isBlank(curCode)) {
			return "";
		}
		String[] str = curCode.split("-");
		if (str.length < 3) {
			return "";
		}

		Calendar cal = Calendar.getInstance();

		String year = cal.get(Calendar.YEAR) + "";
		str[str.length - 2] = year;
		// 初始化版本号下拉框
		String version = str[str.length - 1];
		comboBox.setSelectedItem(version);
		String viewCode = null;
		for (String string : str) {
			if (viewCode == null) {
				viewCode = string;
			} else {
				viewCode += "-" + string;
			}
		}
		return viewCode;
	}

	private void reSetViewCodeByCurCode(String selectVersion) {
		String curCode = viewCodeTextField.getText();
		if (StringUtils.isBlank(curCode)) {
			return;
		}
		String[] str = curCode.split("-");
		if (str.length < 3) {
			return;
		}
		str[str.length - 1] = selectVersion;
		String viewCode = null;
		for (String string : str) {
			if (viewCode == null) {
				viewCode = string;
			} else {
				viewCode += "-" + string;
			}
		}
		viewCodeTextField.setText(viewCode);
	}

	// 取消事件
	public void cancelButtonAction() {
		isOk = false;
		this.dispose();
	}

	public JTextField getNameTextField() {
		return curViewCodeTextField;
	}

	public void setNameTextField(JTextField nameTextField) {
		this.curViewCodeTextField = nameTextField;
	}

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}
}
