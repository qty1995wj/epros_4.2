package com.jecn.epros.domain.inventory;

import com.jecn.epros.domain.JecnUser;

import java.util.List;

public class PosInventoryPosBean {
    /**岗位Id**/
    private Long posId;
    /**岗位名称**/
    private String posName;
    /**岗位关联人员集合**/
    private List<JecnUser> posUser;

    public List<JecnUser> getPosUser() {
        return posUser;
    }

    public void setPosUser(List<JecnUser> posUser) {
        this.posUser = posUser;
    }

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }
}
