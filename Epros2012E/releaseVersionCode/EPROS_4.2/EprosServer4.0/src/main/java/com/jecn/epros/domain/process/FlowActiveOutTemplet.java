package com.jecn.epros.domain.process;

public class FlowActiveOutTemplet {
    private Long fileId;
    private String fileName;
    private String modeFileId;
    private Long fileTempletId;
    private String fileTempletName;
    private String explain;
    private String name;
    //新版输入输出 文件类型 0 模板 1 样例
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getModeFileId() {
        return modeFileId;
    }

    public void setModeFileId(String modeFileId) {
        this.modeFileId = modeFileId;
    }

    public Long getFileTempletId() {
        return fileTempletId;
    }

    public void setFileTempletId(Long fileTempletId) {
        this.fileTempletId = fileTempletId;
    }

    public String getFileTempletName() {
        return fileTempletName;
    }

    public void setFileTempletName(String fileTempletName) {
        this.fileTempletName = fileTempletName;
    }
}
