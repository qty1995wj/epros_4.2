package wordxml.element.bean.paragraph;

/**
 * 段落行距段落缩进bean
 * 
 * @author admin
 * 
 */
public class ParagraphRowIndBean {

	/*** 左侧缩进 厘米 *******/
	private float left;
	/*** 右侧缩进 厘米 ********/
	private float right;
	/*** 悬挂缩进 厘米 *******/
	private float hanging;
	/*** 指定第一行缩进 厘米 **********/
	private float firstLine;

	public ParagraphRowIndBean() {

	}

	public float getLeft() {
		return left;
	}

	public void setLeft(float left) {
		this.left = left;
	}

	public float getRight() {
		return right;
	}

	public void setRight(float right) {
		this.right = right;
	}

	public float getHanging() {
		return hanging;
	}

	public void setHanging(float hanging) {
		this.hanging = hanging;
	}

	public float getFirstLine() {
		return firstLine;
	}

	public void setFirstLine(float firstLine) {
		this.firstLine = firstLine;
	}

}
