package epros.draw.visio;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnModeFigureData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.top.toolbar.io.JecnIOUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;

/**
 * visio 获取图形数据生成 系统中 的图形
 * 
 * @author fuzhh 2013-7-2
 * 
 */
public class JecnVisioCreateFigure {
	private static Logger log = Logger.getLogger(JecnVisioCreateFigure.class);
	/** 面板 */
	private JecnDrawDesktopPane workflow = null;
	/** 文件路径 */
	private String filePath;
	/** 文件名称 */
	private String fileName;
	/** 文件后缀名 */
	private String[] fileEX = new String[] { "vdx", "vsx", "vtx" };

	public JecnVisioCreateFigure() {
		// 获取visio 对应图形的数据
		if (JecnVisioModeXmlData.visioMap.size() <= 0) {
			JecnVisioModeXmlData.getVisioFigureData();
		}
		// 面板初始化
		workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
	}

	public void initialize() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JFileChooser fileChooser = new JecnFileChooser(pathUrl);
		// 移除系统给定的文件过滤器
		fileChooser.setAcceptAllFileFilterUsed(false);
		// 标题
		fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("pleaseChooseTheFile"));
		// 过滤文件后缀
		fileChooser.setFileFilter(new JecnFileNameFilter(fileEX));
		// 设置按钮为打开
		fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);

		// 显示文件选择器
		int i = fileChooser.showOpenDialog(JecnDrawMainPanel.getMainPanel());

		// 选择确认（yes、ok）后返回该值。
		if (i == JFileChooser.APPROVE_OPTION) {
			// 获取输出文件的路径
			filePath = fileChooser.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSaveFileDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			// 判断文件后缀名是否为epros结尾
			if (isEndFile(filePath)) {
				File flowFile = fileChooser.getSelectedFile();
				// 获取文件名称
				fileName = flowFile.getName();
				fileName = fileName.substring(0, fileName.length() - 4);
				// 获取输出文件的路径
				String xmlPath = fileChooser.getSelectedFile().getPath();
				JecnReadVisio readVisio = new JecnReadVisio();
				List<JecnVisioPageDataBean> visioPageDataBeanList = readVisio.readVisioData(xmlPath);
				// 获取文件名称
				if (EprosType.eprosDraw == JecnSystemStaticData.getEprosType()) {
					boolean isOpen = JecnFlowImport.isOpen(xmlPath);
					if (isOpen) {
						return;
					}
				}
				WorkTask task = new WorkTask();
				if (visioPageDataBeanList == null) {
					return;
				}
				task.setVisioPageDataBeanList(visioPageDataBeanList);
				String error = JecnLoading.start(task);
				if (!DrawCommon.isNullOrEmtryTrim(error)) {
					// 打开异常
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
							.getJecnResourceUtil().getValue("openTheAbnormal"));
				}
			}
		}
	}

	/**
	 * 判定选择文件的后缀名是否正确
	 * 
	 * @author fuzhh 2013-7-16
	 * @param filePath
	 *            文件路径
	 * @return
	 */
	public boolean isEndFile(String filePath) {
		boolean isFlag = false;
		for (String str : fileEX) {
			if (filePath.endsWith(str)) {
				isFlag = true;
				break;
			}
		}
		return isFlag;
	}

	/**
	 * * 设置图形数据
	 * 
	 * @param flowStructureImage
	 *            服务图形数据的bean
	 * @param orderIndex
	 *            图形的层级
	 */
	public void getFigureImagerData(JecnFlowStructureImage flowStructureImage, List<JecnFlowStructureImage> figureList,
			List<JecnFlowStructureImage> lineList) throws Exception {
		Long figureId = flowStructureImage.getFigureId();
		if (figureId == null) {
			return;
		}
		// 图形标识
		String figureType = flowStructureImage.getFigureType();
		if (figureType == null) {
			return;
		}
		// 生成 连接线
		if ("ManhattanLine".equals(figureType) || "CommonLine".equals(figureType)) {
			lineList.add(flowStructureImage);
		} else {
			figureList.add(flowStructureImage);
		}
	}

	/**
	 * 创建连接线
	 * 
	 * @param flowStructureImage
	 *            数据bean
	 * @param figureType
	 *            图形标识
	 * @param figureId
	 *            图形ID
	 * @author fuzhh 2013-7-4
	 * @return
	 */
	public void createLine(JecnFlowStructureImage flowStructureImage,
			List<JecnBaseFlowElementPanel> baseFlowElementList, String figureType, Long figureId, int orderIndex)
			throws Exception {
		// System.out.println(figureType);
		// 获取连接线的数据对象
		JecnManhattanLineData manhattanLineData = new JecnManhattanLineData(MapElemType.valueOf(figureType));
		// 线的主键
		// if (flowStructureImage.getFigureId() != null) {
		// manhattanLineData
		// .setFlowElementId(flowStructureImage.getFigureId());
		// }

		// 线的宽度
		if (flowStructureImage.getLineThickness() != null) {
			manhattanLineData.setBodyWidth(flowStructureImage.getLineThickness());
		}
		// 线的类型
		manhattanLineData.setBodyStyle(Integer.valueOf(flowStructureImage.getBodyLine() == null ? "0"
				: flowStructureImage.getBodyLine()));
		// 字体颜色
		String fontColor = flowStructureImage.getFontColor();
		if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
			String[] backColors = fontColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			manhattanLineData.setFontColor(newBackColor);
		}

		// 字体加粗样式
		if (flowStructureImage.getFontBody() != null) {
			manhattanLineData.setFontStyle(flowStructureImage.getFontBody().intValue());
		}
		// 字体类型
		if (flowStructureImage.getFontType() != null) {
			manhattanLineData.setFontName(flowStructureImage.getFontType());
		}
		// 字体大小
		if (flowStructureImage.getFontSize() != null) {
			manhattanLineData.setFontSize(flowStructureImage.getFontSize().intValue());
		}
		// 图形的开始UIID
		if (flowStructureImage.getStartFigure() != null) {
			manhattanLineData.setStartFigureUUID(flowStructureImage.getStartFigure().toString());
			// 图形的开始ID
			manhattanLineData.setStartId(flowStructureImage.getStartFigure());
		}
		// 图形的结束UIID
		if (flowStructureImage.getEndFigure() != null) {
			manhattanLineData.setEndFigureUUID(flowStructureImage.getEndFigure().toString());
			// 图形的结束ID
			manhattanLineData.setEndId(flowStructureImage.getEndFigure());
		}

		// 获取开始点的编辑点
		int poLongx = 1;
		if (flowStructureImage.getPoLongx() != null) {
			poLongx = flowStructureImage.getPoLongx().intValue();
		}
		EditPointType startEditPointType = DrawCommon.getLineType(poLongx);
		manhattanLineData.setStartEditPointType(startEditPointType);
		// 获取结束点的编辑点
		int poLongy = 1;
		if (flowStructureImage.getPoLongy() != null) {
			poLongy = flowStructureImage.getPoLongy().intValue();
		}
		EditPointType editPointType = DrawCommon.getLineType(poLongy);
		manhattanLineData.setEndEditPointType(editPointType);

		// 线段颜色
		String bodyColor = flowStructureImage.getLineColor();
		if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
			String[] backColors = bodyColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			manhattanLineData.setBodyColor(newBackColor);
		}

		// 给线段赋值
		manhattanLineData.setFigureText(flowStructureImage.getFigureText());
		// 获取层级
		manhattanLineData.setZOrderIndex(orderIndex);
		// 根据标识位获取连接线对象
		JecnBaseManhattanLinePanel baseManhattanLine = JecnPaintFigureUnit.getJecnBaseManhattanLinePanel(figureType,
				manhattanLineData);
		// 设置连接线的开始图形和结束图形
		JecnFlowImport flowImport = new JecnFlowImport();
		flowImport.setManLineRefFigure(baseFlowElementList, baseManhattanLine);
		JecnAddFlowElementUnit.addManhattinLine(baseManhattanLine);
	}

	/**
	 * 创建图形数据
	 * 
	 * @author fuzhh 2013-7-4
	 * @param flowStructureImage
	 *            数据bean
	 * @param figureType
	 *            图形标识
	 * @param figureId
	 *            图形ID
	 * @throws Exception
	 */
	public void createFigure(List<JecnBaseFlowElementPanel> baseFlowElementPanel,
			JecnFlowStructureImage flowStructureImage, String figureType, Long figureId, int orderIndex)
			throws Exception {
		// 创建图形数据对象
		JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.valueOf(figureType));

		// 图形主键
		// figureData.setFlowElementId(figureId);
		// 图形UIID唯一标识，放入的是主键ID （画图面板上的唯一标识，不代表数据库）
		figureData.setFlowElementUUID(flowStructureImage.getFigureId().toString());
		// 图形上添加的字符
		figureData.setFigureText(flowStructureImage.getFigureText());
		// 图形的X点
		int pointX = flowStructureImage.getPoLongx().intValue();
		// 图形的Y点
		int pointY = flowStructureImage.getPoLongy().intValue();

		// 图形的连接ID
		if (flowStructureImage.getLinkFlowId() != null) {
			figureData.getDesignerFigureData().setLinkId(flowStructureImage.getLinkFlowId());
		}
		// 图形连接类型
		if (flowStructureImage.getLineColor() != null && !DrawCommon.isActive(MapElemType.valueOf(figureType))) {
			figureData.getDesignerFigureData().setLineType(flowStructureImage.getLineColor());
		}

		// 图形的位置点
		Point point = new Point(pointX, pointY);
		// 图形的宽度
		figureData.setFigureSizeX(flowStructureImage.getWidth().intValue());
		// 图形的高度
		figureData.setFigureSizeY(flowStructureImage.getHeight().intValue());
		// 字体的大小
		if (flowStructureImage.getFontSize() != null) {
			figureData.setFontSize(flowStructureImage.getFontSize().intValue());
		}
		// 图形的层级
		figureData.setZOrderIndex(orderIndex);
		// 图形填充效果
		int figureFillEffects = flowStructureImage.getFillEffects() == null ? 1 : flowStructureImage.getFillEffects();
		if (0 == figureFillEffects) {
			figureData.setFillType(FillType.none);
		} else if (1 == figureFillEffects) {
			figureData.setFillType(FillType.one);
		} else {
			figureData.setFillType(FillType.two);
		}
		// 图形的填充颜色
		String figureFillCololr = flowStructureImage.getBGColor();
		if (!DrawCommon.isNullOrEmtryTrim(figureFillCololr)) {
			String[] backColors = figureFillCololr.split(",");
			if (backColors.length == 3) {// 单色效果
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				figureData.setFillColor(newBackColor);
			} else if (backColors.length == 4) {// 双色效果+渐变方向
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				String colorChangType = backColors[3];
				if ("1".equals(colorChangType)) {
					// 向上倾斜
					figureData.setFillColorChangType(FillColorChangType.topTilt);
				} else if ("2".equals(colorChangType)) {
					// 向下倾斜
					figureData.setFillColorChangType(FillColorChangType.bottom);
				} else if ("3".equals(colorChangType)) {
					// 垂直
					figureData.setFillColorChangType(FillColorChangType.vertical);
				} else {
					// 水平
					figureData.setFillColorChangType(FillColorChangType.horizontal);
				}
				figureData.setFillColor(newBackColor);
			} else if (backColors.length == 7) {// 双色效果+渐变方向
				Color oneBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				Color twoBackColor = new Color(Integer.parseInt(backColors[3]), Integer.parseInt(backColors[4]),
						Integer.parseInt(backColors[5]));
				figureData.setFillColor(oneBackColor);
				figureData.setChangeColor(twoBackColor);

				String colorChangType = backColors[6];
				if ("1".equals(colorChangType)) {
					// 向上倾斜
					figureData.setFillColorChangType(FillColorChangType.topTilt);
				} else if ("2".equals(colorChangType)) {
					// 向下倾斜
					figureData.setFillColorChangType(FillColorChangType.bottom);
				} else if ("3".equals(colorChangType)) {
					// 垂直
					figureData.setFillColorChangType(FillColorChangType.vertical);
				} else {
					// 水平
					figureData.setFillColorChangType(FillColorChangType.horizontal);
				}
			}
		}
		// 字体颜色
		String fontColor = flowStructureImage.getFontColor();
		if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
			String[] backColors = fontColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setFontColor(newBackColor);
		}

		// 字体是否加粗0为正常，1为加粗，2为斜体
		if (flowStructureImage.getFontBody() != null) {
			int fontBody = flowStructureImage.getFontBody().intValue();
			figureData.setFontStyle(fontBody);
		}
		// 字体类型:默认为（宋体）
		String fontType = flowStructureImage.getFontType();
		if (DrawCommon.isNullOrEmtry(fontType)) {
			fontType = "宋体";
		}
		figureData.setFontName(fontType);
		// 字体位置
		if (flowStructureImage.getFontPosition() != null) {
			figureData.setFontPosition(Integer.valueOf(flowStructureImage.getFontPosition()));
		}
		// 是否存在阴影
		Long favaShadow = flowStructureImage.getHavaShadow();
		if (favaShadow != null) {
			if (favaShadow.intValue() == 1) {
				figureData.setShadowsFlag(true);
			} else {
				figureData.setShadowsFlag(false);
			}
		}
		// 阴影颜色
		String shadowColor = flowStructureImage.getShadowColor();
		if (!DrawCommon.isNullOrEmtryTrim(shadowColor)) {
			String[] backColors = shadowColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setShadowColor(newBackColor);
		}

		// 旋转角度值
		figureData.setCurrentAngle(Double.valueOf(flowStructureImage.getCircumgyrate()));
		// 边框样式
		if (flowStructureImage.getBodyLine() != null) {
			figureData.setBodyStyle(Integer.valueOf(flowStructureImage.getBodyLine()));
		}
		// 边框宽度
		if (flowStructureImage.getLineThickness() != null) {
			figureData.setBodyWidth(flowStructureImage.getLineThickness());
		}
		// 边框颜色
		String bodyColor = flowStructureImage.getBodyColor();
		if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
			String[] backColors = bodyColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setBodyColor(newBackColor);
		}
		// 是否显示3D
		Integer figure3D = flowStructureImage.getIs3DEffect();
		figureData.setFlag3D(figure3D != null && figure3D == 1 ? true : false);
		if (MapElemType.valueOf(figureType) == MapElemType.IconFigure) { // 图标插入框
			figureData.getDesignerFigureData().setLinkId(flowStructureImage.getLinkFlowId());
		} else if (MapElemType.valueOf(figureType) == MapElemType.ModelFigure) { // 泳池
			JecnModeFigureData modeFigureData = (JecnModeFigureData) figureData;
			modeFigureData.setDividingX(flowStructureImage.getDividingX());
		}
		// 生成图形
		JecnBaseFigurePanel baseFigureElement = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowFigure(MapElemType
				.valueOf(figureType), figureData);
		// 设置图形的位置
		baseFigureElement.setSizeAndLocation(point);
		// 添加图形到面板
		JecnAddFlowElementUnit.addFigure(baseFigureElement);
		baseFlowElementPanel.add(baseFigureElement);
	}

	/**
	 * 获取面板最大层级，并把层级数加一
	 * 
	 * @return 面板最大层级
	 */
	public int getOrderIndex() {
		if (workflow == null) {
			workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		}
		// 获取最大层级
		int orderIndex = workflow.getMaxZOrderIndex();
		// 最大层级加一
		workflow.setMaxZOrderIndex(orderIndex + 2);
		return orderIndex;
	}

	/**
	 * 创建流程图
	 * 
	 * @param mapName
	 *            流程图名称
	 * @param isHFlag
	 *            横向纵向标识
	 */
	private void createPartMap(String mapName, boolean isHFlag) {
		JecnFlowMapData partMapData = new JecnFlowMapData(MapType.partMap);
		// 名称
		partMapData.setName(mapName);
		// 横竖标识
		partMapData.setHFlag(isHFlag);

		JecnDrawMainPanel.getMainPanel().addWolkflow(partMapData);
	}

	/**
	 * @author yxw 2012-8-10
	 * @description：面板保存进度控制线程
	 */
	class WorkTask extends SwingWorker<Boolean, Void> {
		private List<JecnVisioPageDataBean> visioPageDataBeanList;

		@Override
		protected Boolean doInBackground() throws Exception {
			try {
				boolean isQuery = true;
				for (JecnVisioPageDataBean visioPageDataBean : visioPageDataBeanList) {
					if (visioPageDataBean.getPageName() == null) {
						visioPageDataBean.setPageName(fileName);
					}
					// 存储连接线数据
					List<JecnFlowStructureImage> lineList = new ArrayList<JecnFlowStructureImage>();
					// 存储图形的集合
					List<JecnFlowStructureImage> figureList = new ArrayList<JecnFlowStructureImage>();
					// 存储图形数据
					List<JecnBaseFlowElementPanel> baseFlowElementList = new ArrayList<JecnBaseFlowElementPanel>();
					// 判断是否为设计器
					if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
						// 清空面板
						JecnIOUtil.removeAllWorkflow();
					} else {
						// 创建流程图
						createPartMap(visioPageDataBean.getPageName(), true);
					}

					// 循环图形数据
					for (JecnFlowStructureImage flowStructureImage : visioPageDataBean.getFlowStructureImageList()) {
						getFigureImagerData(flowStructureImage, figureList, lineList);
					}
					// 重算 图形的位置点 Y
					int maxY = 1;
					// 获取 图形最大的Y点
					for (JecnFlowStructureImage flowStructureImage : figureList) {
						if (flowStructureImage.getPoLongy() != null && flowStructureImage.getPoLongy() > maxY) {
							maxY = flowStructureImage.getPoLongy().intValue();
						}
					}
					int centreInt = (maxY + 100) / 2;
					// 生成 图形
					for (JecnFlowStructureImage flowStructureImage : figureList) {
						int poy = 1;
						if (flowStructureImage.getPoLongy() != null && flowStructureImage.getPoLongy() > centreInt) {
							poy = centreInt - (flowStructureImage.getPoLongy().intValue() - centreInt);
						} else if (flowStructureImage.getPoLongy() != null
								&& flowStructureImage.getPoLongy() < centreInt) {
							poy = centreInt + (centreInt - flowStructureImage.getPoLongy().intValue());
						}
						if (poy < 1) {
							poy = 1;
						}
						flowStructureImage.setPoLongy(Long.valueOf(poy));

						Long figureId = flowStructureImage.getFigureId();
						// 图形标识
						String figureType = flowStructureImage.getFigureType();
						createFigure(baseFlowElementList, flowStructureImage, figureType, figureId, getOrderIndex());
					}
					if (baseFlowElementList != null) {
						for (JecnFlowStructureImage flowStructureImage : lineList) {
							Long figureId = flowStructureImage.getFigureId();
							if (figureId == null) {
								return false;
							}
							// 图形标识
							String figureType = flowStructureImage.getFigureType();
							if (figureType == null) {
								return false;
							}
							createLine(flowStructureImage, baseFlowElementList, figureType, figureId, getOrderIndex());
						}
					}
				}
				return isQuery;
			} catch (Exception e) {
				return false;
			}
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}

		public List<JecnVisioPageDataBean> getVisioPageDataBeanList() {
			return visioPageDataBeanList;
		}

		public void setVisioPageDataBeanList(List<JecnVisioPageDataBean> visioPageDataBeanList) {
			this.visioPageDataBeanList = visioPageDataBeanList;
		}

	}
}
