package epros.designer.gui.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileInfoBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.AccessId;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.flow.file.FlowRelatedRiskPanel;
import epros.designer.gui.process.flow.file.FlowRelatedStandardPanel;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * 文件属性窗体
 * 
 * @author 2012-05-29
 * 
 */
public class FilePropertyDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(FilePropertyDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	// 文件基本信息
	private FileBaseInfoDialog fileBaseDia = null;

	/** 主面板 */
	private JPanel mainPanel = null;

	/** 文件基本信息面板 */
	private JScrollPane fileBaseInfoPanel = null;

	protected JScrollPane standardScrollPanel;
	protected JScrollPane riskScrollPanel;

	/** JTabbedPane */
	private JTabbedPane tabPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = null;

	/** 确定 */
	private JButton okBut = null;

	/** 取消 */
	private JButton cancelBut = null;

	/** 设置大小 */
	Dimension dimension = null;
	/*** 验证信息提示Lab **/
	private JLabel verfyLab = new JLabel();
	private FileInfoBean fileBean = null;

	// 文件相关联标准
	private FlowRelatedStandardPanel flowRelatedStandardPanel = null;

	// 文件相关联风险
	protected FlowRelatedRiskPanel relatedRiskPanel = null;

	public FilePropertyDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;

		try {
			fileBean = ConnectionPool.getFileAction().getFileBean(selectNode.getJecnTreeBean().getId());
		} catch (Exception e) {
			log.error("FilePropertyDialog FilePropertyDialog is error！", e);
			return;
		}

		initCompotents();
		initLayout();
		// 是窗体始终显示在最前端
		this.setModal(true);
	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 文件基本信息面板
		fileBaseInfoPanel = new JScrollPane();

		// JTabbedPane
		tabPanel = new JTabbedPane();

		// 按钮面板
		buttonPanel = new JPanel();

		// 确定
		okBut = new JButton(JecnProperties.getValue("okBtn"));

		// 取消
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

		// 设置窗体大小
		this.setSize(650, 600);
		this.setMinimumSize(new Dimension(650, 505));
		this.setTitle(JecnProperties.getValue("fileProperty"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);

		// 设置面板背景颜色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		fileBaseInfoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		standardScrollPanel = new JScrollPane();
		standardScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		flowRelatedStandardPanel = new FlowRelatedStandardPanel(fileBean.getStandardsRelateds());
		standardScrollPanel.setViewportView(flowRelatedStandardPanel);

		riskScrollPanel = new JScrollPane();
		riskScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		relatedRiskPanel = new FlowRelatedRiskPanel(fileBean.getRiskRelateds());
		riskScrollPanel.setViewportView(relatedRiskPanel);

		verfyLab.setForeground(Color.red);

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

	}

	/***
	 * 布局
	 */
	private void initLayout() {

		Container contentPane = this.getContentPane();
		tabPanel.add(fileBaseInfoPanel, 0);
		tabPanel.setTitleAt(0, JecnProperties.getValue("fileBaseInfoTab"));// 文件基本信息
		tabPanel.add(this.standardScrollPanel, 1);
		tabPanel.setTitleAt(1, JecnProperties.getValue("relatedOrder"));// 相关标准
		tabPanel.add(this.riskScrollPanel, 2);
		tabPanel.setTitleAt(2, JecnProperties.getValue("relatedRisk"));// 相关风险

		// 文件基本信息
		fileBaseDia = new FileBaseInfoDialog(selectNode, this, fileBean);
		fileBaseDia.setVisible(true);
		fileBaseInfoPanel.setViewportView(fileBaseDia);

		// 按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		contentPane.add(buttonPanel, BorderLayout.SOUTH);
		contentPane.add(tabPanel, BorderLayout.CENTER);

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private boolean isUpdate() {
		if (fileBaseDia.isUpdate()) {
			return true;
		}
		if (flowRelatedStandardPanel.isUpdate()) {
			return true;
		}
		if (relatedRiskPanel.isUpdate()) {
			return true;
		}
		return false;
	}

	/***
	 * 确定 保存信息
	 */
	private void okButPerformed() {
		verfyLab.setText("");

		// 流程编号 长度验证
		if (fileBaseDia.fileNumField != null) {
			if (DrawCommon.checkNameMaxLength(fileBaseDia.fileNumField.getText().toString())) {
				verfyLab.setText(JecnProperties.getValue("fileNum") + JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}
		}
		// 关键字 长度验证
		if (fileBaseDia.keyWordField != null) {
			if (fileBaseDia.keyWordField.isValidateNotPass(verfyLab)) {
				return;
			}
		}
		if (isUpdate()) {
			JecnFileBeanT jecnFileBean = fileBean.getJecnFileBeanT();
			// 文件编号
			if (fileBaseDia.fileNumField != null && fileBaseDia.fileNumField.getText() != null
					&& !"".equals(fileBaseDia.fileNumField.getText())) {
				jecnFileBean.setDocId(fileBaseDia.fileNumField.getText());
				// 流程编号
				if (DrawCommon.checkNameMaxLength(fileBaseDia.fileNumField.getText().toString())) {
					verfyLab.setText(JecnProperties.getValue("fileNum") + JecnProperties.getValue("lengthNotBaiOut"));
					return;
				}
			} else {
				jecnFileBean.setDocId(null);
			}
			// // 密级
			int isPublic = this.fileBaseDia.accessAuthorityCommon.getPublicStatic();
			jecnFileBean.setIsPublic(isPublic == 0 ? 0L : 1L);
			// 保密级别
			jecnFileBean.setConfidentialityLevel(fileBaseDia.accessAuthorityCommon.getConfidentialityLevelStatic());
			// // 所属流程
			jecnFileBean.setFlowId(this.fileBaseDia.resProcessSelectCommon.getIdResult());
			// // 责任部门
			jecnFileBean.setOrgId(fileBaseDia.resDepartSelectCommon.getIdResult());
			AccessId accessId = new AccessId();
			// 部门查阅
			accessId.setOrgAccessId(fileBaseDia.accessAuthorityCommon.getOrgAccessIds());
			// 岗位查阅
			accessId.setPosAccessId(fileBaseDia.accessAuthorityCommon.getPosAccessIds());
			// 岗位组查阅
			accessId.setPosGroupAccessId(fileBaseDia.accessAuthorityCommon.getPosGroupAcessIds());
			jecnFileBean.setAccessId(accessId);
			// 关键字
			if (this.fileBaseDia.keyWordField != null) {
				if (fileBaseDia.keyWordField.isValidateNotPass(verfyLab)) {
					return;
				}
				jecnFileBean.setKeyword(fileBaseDia.keyWordField.getResultStr());
			}
			// 专员
			if (fileBaseDia.commissionerPeopleSelectCommon != null) {
				jecnFileBean.setCommissionerId(fileBaseDia.commissionerPeopleSelectCommon.getIdResult());
			}
			if (fileBaseDia.personliableSelect != null) {
				jecnFileBean.setResPeopleId(fileBaseDia.personliableSelect.getSingleSelectCommon().getIdResult());
				jecnFileBean.setTypeResPeople(fileBaseDia.personliableSelect.getPeopleResType());
			}

			try {
				ConnectionPool.getFileAction().updateFileProperty(jecnFileBean,
						this.flowRelatedStandardPanel.getResultIds(), this.relatedRiskPanel.getResultIds(),
						JecnConstants.getUserId());
				selectNode.getJecnTreeBean().setUpdate(true);
				JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), selectNode);
			} catch (Exception e) {
				log.error("okButPerformed is error", e);
				return;
			}
		}
		// 关闭文件属性面板
		this.dispose();
	}
}
