package com.jecn.epros.server.service.dataImport.match;

import java.util.List;

import com.jecn.epros.server.service.dataImport.match.util.MatchTool.DataErrorType;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.PosEprosRelBean;

/**
 * 
 * 人员数据导入处理类
 * 
 * @author zhagnxiaohu
 * @date： 日期：Apr 9, 2013 时间：3:02:40 PM
 */
public interface IUserMatchService {
	/**
	 * 获得匹配数据总数
	 * 
	 * @return
	 * @throws Exception
	 */
	int getPosEpsCount() throws Exception;

	/**
	 * 获取当前页岗位匹配集合
	 * 
	 * @return
	 * @throws BsException
	 * @throws Exception
	 */
	List<PosEprosRelBean> getStatcionData(int pageSize, int startRow)
			throws Exception;

	/**
	 * 根据流程岗位名称获取匹配结果
	 * 
	 * @param psoName
	 * @return
	 * @throws Exception
	 */
	int getSearchPosCount(String psoName) throws Exception;

	/**
	 * @param pageSize
	 * @param startRow
	 * @param posName
	 * @return
	 * @throws Exception
	 */
	List<PosEprosRelBean> getStatcionDataBySearch(int pageSize, int startRow,
			String posName) throws Exception;

	/**
	 * 根据匹配岗位的编码、ID查找对应的岗位名称
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Object[]> selectActEpsNames() throws Exception;

	/**
	 * 
	 * 添加岗位匹配关系
	 * 
	 * @param stationId
	 * @param hrPosNums
	 * @param type
	 * @throws Exception
	 */
	boolean addPosEpsRel(String stationId, String hrPosNums, int type)
			throws Exception;

	/***************************************************************************
	 * 更新岗位匹配关系
	 * 
	 * @param stationId
	 * @param hrPosNums
	 * @param basePosNums
	 * @param type
	 * @return
	 * @throws Exception
	 */
	boolean updatePosEpsRel(String stationId, String hrPosNums, int type)
			throws Exception;

	/***************************************************************************
	 * 删除 流程岗位与实际岗位关联表数据 JECN_POSITION_EPROS_REL 删除 流程人员与岗位管理表
	 * JECN_USER_POSITION_RELATED
	 * 
	 * @param posEprosRelBean
	 * @return
	 * @throws Exception
	 */
	void deletePosEpsData(Long flowPosId) throws Exception;

	/**
	 * 
	 * 获取上次导入的项目ID，与当前项目ID做比较
	 * 
	 * 查询部门临时表（TMP_JECN_IO_DEPT）的项目ID：
	 * 
	 * 如果查询出来结果没有值，直接导入；
	 * 
	 * 如果查询结果有数据有且只有一条数据，判断项目ID是否等于当前主项目ID：
	 * 
	 * 如果是执行导入，如果不是不导入提示不能导入
	 * 
	 * 如果查询结果有数据且有多条数据，返回提示不正确
	 * 
	 * @return
	 * @throws Exception
	 */
	boolean checkProjectId(Long projectId) throws Exception;

	/**
	 * 
	 * 把BaseBean对象中数据生成excel文件存储到本地
	 * 
	 * @param baseBean
	 * @return
	 */
	boolean saveErrorExcelToLocal(BaseBean baseBean);

	/**
	 * 
	 * 
	 * 删除给定路径的文件 15010165914
	 * 
	 * @param filePath
	 */
	void deleteErrorLocalExcel(String filePath);

	/**
	 * 获取流程岗位ID在匹配关系中的个数
	 * 
	 * @param flowPosId
	 *            流程岗位ID
	 * @return int 流程岗位个数
	 */
	int onlyMatchPost(Long flowPosId);

	/**
	 * 获取人员同步原始数据
	 * 
	 * @return
	 */
	boolean exportExcelOriData();

	/**
	 * 获取主项目ID
	 * 
	 * @return
	 * @throws Exception
	 */
	Long getCurJecnProject() throws Exception;

	/**
	 * 人员同步行内校验
	 * 
	 */
	DataErrorType checkData(BaseBean baseBean);

	/**
	 * 清除临时库、入库临时库
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	void deleteAndInsertTmpJecnIOTable(BaseBean baseBean) throws Exception;

	/**
	 * 人员同步行间校验
	 * 
	 * @return DataErrorType
	 */
	DataErrorType checkDataColumns();

	/**
	 * 如果 实际人员表、实际部门表、实际岗位表中有数据，则用临时表中的数据与实际表中的数据进行比较
	 * 
	 * @param curProjectId
	 * @throws Exception
	 */
	void updateProFlag(Long curProjectId) throws Exception;

	/**
	 * 
	 * 与真实库同步同步数据
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	void syncImortDataAndDB(List<Object[]> posChangeList) throws Exception;

	/**
	 * 操作人员表与临时表比较获取人员岗位变岗的集合
	 * 
	 * @return
	 */
	List<Object[]> findJecnUserObject();

	/**
	 * 删除实际表(部门，岗位，人员)中的原有的数据
	 * 
	 * @throws Exception
	 */
	void deleteActualMatchTableData() throws Exception;

	/**
	 * 
	 * 将临时表(部门，岗位，人员)中的数据添加到实际表(部门，岗位，人员)
	 * 
	 * @throws Exception
	 */
	void insertActualMatchTableData() throws Exception;

	/**
	 * 删除人员岗位关系表 （删除 流程岗位ID在岗位匹配关系表中不存在的 记录）
	 * 
	 * @throws Exception
	 */
	void deletePosRelUser() throws Exception;

}
