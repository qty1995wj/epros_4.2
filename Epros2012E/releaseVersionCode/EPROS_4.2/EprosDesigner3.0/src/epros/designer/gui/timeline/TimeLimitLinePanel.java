package epros.designer.gui.timeline;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnCloneablePanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public class TimeLimitLinePanel extends JecnPanel {

	private static final long serialVersionUID = 1L;
	/** 自定义画笔使用样式：执行呈现和图像处理服务的类 */
	private RenderingHints renderingHints = null;
	private TimeLimitPanelData panelData = new TimeLimitPanelData();

	private SumLimitPanel limitPanel;

	public TimeLimitLinePanel() {
		// 自定义画笔使用样式：执行呈现和图像处理服务的类
		renderingHints = JecnUIUtil.getRenderingHints();
		this.setFont(new Font("宋体", 5, 12));
		this.setBounds(panelData.getPanelX(), panelData.getPanelY(), panelData.getPanelWidth(), panelData
				.getPanelHeight());
		limitPanel = new SumLimitPanel();
		// limitPanel.repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; // 定义画笔

		RenderingHints originalRenderingHints = g2d.getRenderingHints();
		// 获取原始绘画颜色
		Color originalColor = g2d.getColor();

		// 指定RenderingHints
		g2d.setRenderingHints(renderingHints);

		// 绘制图形
		drawPanel(g2d);

		// 还原原始RenderingHints对象
		g2d.setRenderingHints(originalRenderingHints);
		// 还原原始绘画颜色
		g2d.setColor(originalColor);
	}

	/**
	 * 画图
	 * 
	 * @param g2d
	 */
	private void drawPanel(Graphics2D g2d) {
		// 画图时获取与当前面板的缩放比例对应的字体大小
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		int fontSize = (int) (this.getFont().getSize() * workFlow.getWorkflowScale());
		g2d.setFont(new Font(this.getFont().getFontName(), this.getFont().getStyle(), fontSize));
		g2d.setStroke(new BasicStroke(1.0f));
		// 数据同比例缩放
		panelData.zoomData();
		// 绘制横线
		drawHLine(g2d);

		int timeLineSort = 1;
		// 绘制竖线和文本
		for (int i = 0; i < panelData.getDataSize(); i++) {

			// 绘制竖线
			int vLineX = panelData.getVLineX(i);
			g2d.drawLine(vLineX, panelData.getVlineStartY(), vLineX, panelData.getVlineEndY());

			// 绘制编号
			String code = panelData.getActiveDataSource(i).getActivityNum();
			// int codeLength =
			// g2d.getFontMetrics(g2d.getFont()).stringWidth(code);

			// g2d.drawString(code, vLineX - codeLength / 2,
			// panelData.getVlineStartY() - 5);

			g2d.drawLine(vLineX, panelData.getVlineStartY(), vLineX, panelData.getVlineEndY());

			// 箭头
			g2d.drawLine(vLineX - 8, panelData.getVlineEndY() - 4, vLineX - 1, panelData.getVlineEndY());
			// 箭头
			g2d.drawLine(vLineX - 8, panelData.getVlineEndY() + 4, vLineX - 1, panelData.getVlineEndY());

			// 目标值
			targetText(g2d, i, vLineX, code, timeLineSort);
			// 现状值
			statusText(g2d, i, vLineX, code);

			timeLineSort = panelData.getActiveDataSource(i).getTimeLineSort();

		}
		// 汇总
		resetPanel();
	}

	/**
	 * 目标值显示文本
	 * 
	 * @param g2d
	 * @param index
	 * @param vLineX
	 */
	private void targetText(Graphics2D g2d, int index, int vLineX, String code, int timeLineSort) {
		int textHeight = g2d.getFontMetrics(this.getFont()).getHeight();
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		String title = JecnProperties.getValue("targetValue")+"(" + JecnConfigTool.getActiveTimeLineUtil() + ")：";
		String text = null;
		code = "(" + code + ")";
		if (index == 0) {
			text = title + code + panelData.getTimeCost(index);
		} else {
			text = code + panelData.getTimeCost(index);
		}
		int textLength = g2d.getFontMetrics(g2d.getFont()).stringWidth(text);

		int stringX = vLineX - textLength - 2;

		if (panelData.getActiveDataSource(index).getTimeLineSort() - timeLineSort > 1) {// 上线两个活动都存在时间轴
			g2d.drawLine(vLineX - panelData.getActiveDataSource(index).getFigureSizeX(), panelData.getVlineStartY(),
					vLineX - panelData.getActiveDataSource(index).getFigureSizeX(), panelData.getVlineEndY());
		}

		int stringY = panelData.getVlineEndY()
				+ DrawCommon.convertDoubleToInt(textHeight * workFlow.getWorkflowScale());
		g2d.drawString(text, stringX, stringY);
	}

	/**
	 * 现状值：显示文本
	 * 
	 * @param g2d
	 * @param index
	 * @param vLineX
	 */
	private void statusText(Graphics2D g2d, int index, int vLineX, String code) {
		int textHeight = g2d.getFontMetrics(this.getFont()).getHeight();
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		String title = JecnProperties.getValue("situationValue")+"(" + JecnConfigTool.getActiveTimeLineUtil() + ")：";
		String text = null;
		code = "(" + code + ")";
		if (index == 0) {
			text = title + code + panelData.getStatusTimeCost(index);
		} else {
			text = code + panelData.getStatusTimeCost(index);
		}
		int textLength = g2d.getFontMetrics(g2d.getFont()).stringWidth(text);
		int stringX = vLineX - textLength - 2;
		int stringY = panelData.getVlineStartY()
				+ DrawCommon.convertDoubleToInt(textHeight * workFlow.getWorkflowScale()) - 4;
		g2d.drawString(text, stringX, stringY);
	}

	/**
	 * 绘制横线（起点偏移量，
	 * 
	 * @param g2d
	 */
	private void drawHLine(Graphics2D g2d) {
		g2d.drawLine(TimeLimitPanelData.HLINE_OFFSET_START_X, panelData.getVlineEndY(), panelData.getPanelWidth(),
				panelData.getVlineEndY());
	}

	/**
	 * 按照缩放比例重新设置Panel
	 */
	private void resetPanel() {
		double zoomScale = JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowScale();
		int x = DrawCommon.convertDoubleToInt(panelData.getPanelX() * zoomScale);
		int y = DrawCommon.convertDoubleToInt(panelData.getPanelY() * zoomScale);
		int width = DrawCommon.convertDoubleToInt(panelData.getPanelWidth());
		int height = DrawCommon.convertDoubleToInt(panelData.getPanelHeight() * zoomScale);
		this.setBounds(x, y, width, height);
	}

	public SumLimitPanel getLimitPanel() {
		return limitPanel;
	}

	@SuppressWarnings("serial")
	class SumLimitPanel extends JecnCloneablePanel {
		public SumLimitPanel() {
			setSumPanelSize();
		}

		protected void paintComponent(Graphics g) {
			Graphics2D g2d = (Graphics2D) g; // 定义画笔

			RenderingHints originalRenderingHints = g2d.getRenderingHints();
			// 获取原始绘画颜色
			Color originalColor = g2d.getColor();

			// 指定RenderingHints
			g2d.setRenderingHints(renderingHints);

			// 绘制图形
			drawSumPanel(g2d);

			// 还原原始RenderingHints对象
			g2d.setRenderingHints(originalRenderingHints);
			// 还原原始绘画颜色
			g2d.setColor(originalColor);
		}

		private void drawSumPanel(Graphics2D g2d) {
			if (panelData.getSumStatusValues() == null || panelData.getSumTargetValues() == null) {
				return;
			}
			// g2d.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
			g2d.drawString(JecnProperties.getValue("situation_Z")+panelData.getSumStatusValues() + JecnConfigTool.getActiveTimeLineUtil(), 0, 15);
			g2d.drawString(JecnProperties.getValue("target_Z") + panelData.getSumTargetValues() + JecnConfigTool.getActiveTimeLineUtil(), 0, 35);
		}

		public void setSumPanelSize() {
			this.setBounds(20, 20, 170, 40);
		}
	}

}