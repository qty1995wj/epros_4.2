package com.jecn.epros.domain.org;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.inventory.PosInventoryRoleBean;
import com.sun.org.apache.regexp.internal.RE;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SearchPosStatisticsResultBean {

    private Long figureId;

    private String figureName;

    private Long orgId;

    private String orgName;
    /**
     * 责任部门包含父部门集合
     */
    private List<Map<String,Object>> dutyOrgList;

    @JsonIgnore
    private List<PosInventoryRoleBean> roles;

    public LinkResource getLink() {
        return new LinkResource(figureId, figureName, ResourceType.POSITION);
    }

    public LinkResource getOrgLink() {
        return new LinkResource(orgId, orgName, ResourceType.ORGANIZATION);
    }

    public List<PosInventoryRoleBean> getRoles() {
        return roles;
    }

    public void setRoles(List<PosInventoryRoleBean> roles) {
        this.roles = roles;
    }

    public List<Map<String, Object>> getDutyOrgList() {
        return dutyOrgList;
    }

    public void setDutyOrgList(List<Map<String, Object>> dutyOrgList) {
        this.dutyOrgList = dutyOrgList;
    }

    @JsonIgnore
    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    @JsonIgnore
    public String getFigureName() {
        return figureName;
    }

    public void setFigureName(String figureName) {
        this.figureName = figureName;
    }

    @JsonIgnore
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @JsonIgnore
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
