/** 屏幕可用分辨率宽度 */
var screenWidth = window.screen.availWidth;
/** 屏幕可用分辨率宽度 */
var screenHeight = window.screen.availHeight;
/** grid(Table)列表每页显示的行数 */
var pageSize = 20;
/** 搜索DIV收起与展开 */
var mh = 30; // 最小高度
var step = 5; // 每次变化的px量
var ms = 0.1; // 每隔多久循环一次
// 折叠速度的设置方法
function toggle(div) {
	if (!div.tid) {
		div.tid = "_" + Math.random() * 100;
	}
	if (!window.toggler) {
		window.toggler = {};
	}
	if (!window.toggler[div.tid]) {
		window.toggler[div.tid] = {
			obj : div,
			maxHeight : div.offsetHeight,
			minHeight : mh,
			timer : null,
			action : 1
		};
	}
	div.style.height = div.offsetHeight + "px";
	if (window.toggler[div.tid].timer) {
		clearTimeout(window.toggler[div.tid].timer);
	}
	window.toggler[div.tid].action *= -1;
	window.toggler[div.tid].timer = setTimeout("anim('" + div.tid + "')", ms);// 计时器的用法
}
// 通过对象的最小高度和最大高度，判断折叠是否停止
function anim(id) {
	var t = window.toggler[id];
	var o = window.toggler[id].obj;
	if (t.action < 0) {
		if (o.offsetHeight <= t.minHeight) {
			clearTimeout(t.timer);
			document.getElementById("packUp").src = basePath
					+ "images/common/packUp.gif";
			document.getElementById("searchVal").innerHTML = i_unfold;
			return;
		}
	} else {
		if (o.offsetHeight >= t.maxHeight) {
			clearTimeout(t.timer);
			document.getElementById("packUp").src = basePath
					+ "images/common/on.gif";
			document.getElementById("searchVal").innerHTML = i_packUp;
			return;
		}
	}
	o.style.height = (parseInt(o.style.height, 10) + t.action * step) + "px";
	window.toggler[id].timer = setTimeout("anim('" + id + "')", ms);
}

function toggleDiv(div, imgId, valId) {
	if (!div.tid) {
		div.tid = "_" + Math.random() * 100;
	}
	if (!window.toggler) {
		window.toggler = {};
	}
	if (!window.toggler[div.tid]) {
		window.toggler[div.tid] = {
			obj : div,
			maxHeight : div.offsetHeight,
			minHeight : mh,
			timer : null,
			action : 1
		};
	}
	div.style.height = div.offsetHeight + "px";
	if (window.toggler[div.tid].timer) {
		clearTimeout(window.toggler[div.tid].timer);
	}
	window.toggler[div.tid].action *= -1;
	window.toggler[div.tid].timer = setTimeout("animDiv('" +div.tid+"','"+imgId+"','"+valId +"')", ms);// 计时器的用法
}
// 通过对象的最小高度和最大高度，判断折叠是否停止
function animDiv(id, imgId, valId) {
	var t = window.toggler[id];
	var o = window.toggler[id].obj;
	if (t.action < 0) {
		if (o.offsetHeight <= t.minHeight) {
			clearTimeout(t.timer);
			document.getElementById(imgId).src = basePath
					+ "images/common/packUp.gif";
			document.getElementById(valId).innerHTML = i_unfold;
			return;
		}
	} else {
		if (o.offsetHeight >= t.maxHeight) {
			clearTimeout(t.timer);
			document.getElementById(imgId).src = basePath
					+ "images/common/on.gif";
			document.getElementById(valId).innerHTML = i_packUp;
			return;
		}
	}
	o.style.height = (parseInt(o.style.height, 10) + t.action * step) + "px";
	window.toggler[id].timer = setTimeout("animDiv('" + id + "','" + imgId + "','"+ valId + "')", ms);
}

/**
 * 任务类型转换
 * 
 * @param {}
 *            value
 */
function taskTypeRenderer(value) {
	var taskTypeName;
	switch (value) {
	case 0:
		taskTypeName = i_processTask;// 流程任务
		break;
	case 4:
		taskTypeName = i_processMapTask;// 地图任务
		break;
	case 1:
		taskTypeName = i_fileTask;// 文件任务
		break;
	case 2:// 制度模板文件
	case 3:// 制度文件
		taskTypeName = i_ruleTask;// 制度任务
		break;

	}
	return taskTypeName;
}

/**
 * 任务阶段转换
 * 
 * @param {}
 *            value
 */
function taskStageRenderer(value) {
	return "<img src='" + basePath + "images/myTask/task" + value + ".gif'/>";
}

function secretLevelRenderer(value) {
	//密级  0:秘密  1:公开
	if(value==0){
		return i_secret;	
	} else if (value ==1){
		return i_public;
	}
}

/**
 * 人员 岗位 部门单选
 * 
 * @param {}
 *            type 类型organization:组织position:岗位person:人员
 * @param {}
 *            inputDomId 输入框domID
 * @param {}
 *            hiddenDomId 隐藏domID
 */
function selectOrgPosWindow(type, inputDomId, hiddenDomId) {
	
		if (!type || type == "") {
		return;
	}
	var selectOrgPosTitle;
	var dataUrl;
	var gridUrl;
	var searchModel;
	if (type == "organization") {
		selectOrgPosTitle = i_orgSelect;
		dataUrl = "getChildOrg.action";
		gridUrl = "orgSearch.action";
	} else if (type == "position") {
		selectOrgPosTitle = i_posSelect;
		dataUrl = "getChildsOrgAndPos.action";
		gridUrl = "posSearch.action";
	} else if (type == "person") {
		selectOrgPosTitle = i_personSelect;
		dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
		gridUrl = "peopleSearch.action";
	}

	var selectOrgPosRoot = new Ext.tree.AsyncTreeNode({
				text : selectOrgPosTitle,
				expanded : true,
				id : "0"
			})

	var selectOrgPosLoader = new Ext.tree.TreeLoader({
				dataUrl : dataUrl
			})

	var orgPosPerTree = new Ext.tree.TreePanel({
		root : selectOrgPosRoot,
		loader : selectOrgPosLoader,
		height : 495,
		width : 180,
		region : 'west',
		autoScroll : true,
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			'beforeload' : function(node) {
				if (type == "person") {
					if (node.attributes.type == "position") {
						selectOrgPosLoader.dataUrl = "getChildPerson.action";
					} else {
						selectOrgPosLoader.dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
					}
				}
			},
			'dblclick' : function(node, e) {
				if (node.attributes.type == type) {
					var flag = false;
					 resultStore.removeAll();   //清除sesultStore中的数据
					 var sr = new Ext.data.Record();
					 sr.set("id",node.id.split("_")[0]);
					 sr.set("name", node.attributes.text);
				     resultStore.add(sr);
				}
			}
		}
	})

	var searchStore = new Ext.data.JsonStore({
				autoLoad : false,
				proxy : new Ext.data.HttpProxy({
							url : gridUrl
						}),
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						},{
							name : "orgName",
							mapping : "orgName"
						}]
			});
	 if (type == "position" || type == "person") {//查询岗位/人员时，显示所属部门
		 searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						},{
							header : i_orgName,
							dataIndex : "orgName"
						}]
			});
	 }else{
		 searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	 }
	
	var searchGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				region : 'center',
				height : 230,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				tbar : [i_nameC, new Ext.form.TextField({
							id : 'searchNameId',
							enableKeyEvents : true,
							width : 500,
							listeners : {
								'keyup' : function(o) {
									if ($.trim(o.getValue()) != "") {
										searchStore.setBaseParam("searchName",
												o.getValue());
										searchStore.load();
									}
								}
							}
						})],
				store : searchStore,
				colModel : searchModel,
				listeners : {
					'rowdblclick' : function(grid, i, e) {   //双击处理
					  var sr = grid.getStore().getAt(i);
					  resultStore.removeAll();  //移除所有数据
				      resultStore.add(sr);                   //加入选择的节点
					}
				}
			});

	/** ****************************************************************** */
	var resultStore = new Ext.data.JsonStore({
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						},{
							name : "orgName",
							mapping : "orgName"
						}]
			});
	var resultModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var resultGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				height : 205,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				store : resultStore,
				colModel : resultModel
			});

	var selectMainPanel = new Ext.Panel({
				height : 440,
				width : 690,
				layout : 'border',
				border : 0,
				frame : false,
				items : [orgPosPerTree, {
							region : 'center',
							items : [searchGridPanel, resultGridPanel]
						}]

			})
	var win = new Ext.Window({
				title : selectOrgPosTitle,
				height : 500,
				width : 700,
				minHeight : 500,
				minWidth : 700,
				modal : true,
				constrainHeader : true,//证窗口顶部不超出浏览器 
				closeAction : "close",
				resizable : false,// 四边和四角改变window的大小
				items : selectMainPanel,
				buttons : [{
					text : i_Delete,// 删除
					handler : function() {
						Ext.each(resultGridPanel.getSelectionModel()
										.getSelections(), function(r) {
									resultStore.remove(r);
								});
					},
					scope : this
				}, {
					text : i_empty,// 清空
					handler : function() {
						Ext.Msg.confirm('提示', '是否清空所有数据!', function(fc) {
									if (fc == "yes") {
										resultStore.removeAll();
									}
								});
					},
					scope : this
				}, {
					text : i_ok,// 确定
					handler : function() {
						var s = "";
						var ids = "";
						resultStore.each(function(r) {
							if(s==""){
								s = r.get("name");
							}else{
								s = s + "\n"+r.get("name") ;
							}
							
							if(ids==""){
								ids = r.get("id");
							}else{
								ids = ids + ","+r.get("id") ;
							}
						});
						document.getElementById(inputDomId).value = s;
						document.getElementById(hiddenDomId).value = ids;
						win.close();// 窗口关闭
					},
					scope : this
				}, {
					text : i_close,// 关闭
					handler : function() {
						win.close();// 窗口关闭
					}
				}]
			});
	        //同步文本框与点击选择按钮后弹出的窗口中的已选择的人员数据
			if (document.getElementById(inputDomId).value !=''&&document.getElementById(hiddenDomId).value.trim != '') {
				var name = document.getElementById(inputDomId).value;
				var id = document.getElementById(hiddenDomId).value;
				var sr = new Ext.data.Record();
					sr.set("id", id);
					sr.set("name", name);
					resultStore.add(sr);
				
			}
	win.show();

}

function activitiesShow(activeId) {
	window
			.open(
					'processActive.action?activeId=' + activeId,
					'height=200,width=400,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
}
/**
 * 部门、岗位、或人员多选
 * 
 * @param {}
 *            type 类型（organization：部门；position：岗位；person：人员;positionGroup：岗位组）
 * @param {}
 *            inputDomId 显示的名称
 * @param {}
 *            hiddenDomId 隐藏的ID
 * 
 */
function orgPosPerMultipleSelect(type, inputDomId, hiddenDomId) {
	if (!type || type == "") {
		return;
	}
	var selectOrgPosTitle;
	var dataUrl;
	var gridUrl;
	var searchModel;
	if (type == "organization") {
		selectOrgPosTitle = i_orgSelect;
		dataUrl = "getChildOrg.action";
		gridUrl = "orgSearch.action";
	} else if (type == "position") {
		selectOrgPosTitle = i_posSelect;
		dataUrl = "getChildsOrgAndPos.action";
		gridUrl = "posSearch.action";
	}else if(type=="positionGroup"){
        selectOrgPosTitle=proGroup;
        dataUrl="getChildsGroupandOrg.action";
        gridUrl="groupSearch.action";
	}else if (type=="person") {
		selectOrgPosTitle = i_personSelect;
		dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
		gridUrl = "peopleSearch.action";
	}

	var orgPosPerMultipleRoot = new Ext.tree.AsyncTreeNode({
				text : selectOrgPosTitle,
				expanded : true,
				id : "0"
			})
    
	//加载tree节点，返回json
	var orgPosPerMultipleLoader = new Ext.tree.TreeLoader({
				dataUrl : dataUrl
	})

	var orgPosPerMultipleTree = new Ext.tree.TreePanel({
		root : orgPosPerMultipleRoot,
		loader : orgPosPerMultipleLoader,
		height : 495,
		width : 180,
		region : 'west',
		autoScroll : true,
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
		'beforeload' : function(node) {
			if (type == "person") {
					if (node.attributes.type == "position") {
						orgPosPerMultipleLoader.dataUrl = "getChildPerson.action";
					}else {
					    orgPosPerMultipleLoader.dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
					}
			 }
			},
			'dblclick' : function(node, e) {
				if (node.attributes.type == type) {
					var flag = false;
					resultStore.each(function(r) {
								if (node.id.split("_")[0] == r.get("id")) {
									resultGridPanel.getSelectionModel()
											.selectRecords([r]);
									flag = true;
									return false;
								}
							});
					if (!flag) {
						var sr = new Ext.data.Record();
						sr.set("id", node.id.split("_")[0]);
						sr.set("name", node.attributes.text);
						resultStore.add(sr);
					}
				}
			}
		}
	})

	var searchStore = new Ext.data.JsonStore({
				autoLoad : false,
				proxy : new Ext.data.HttpProxy({
							url : gridUrl
						}),
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						}, {
							name : "orgName",
							mapping : "orgName"
						}]
			});
	if(type=="position" || type=="person"){//查询岗位/人员时，显示所属部门
		searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						},{
							header : i_orgName,
							dataIndex : "orgName"
						}]
			});
	}else{
		searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	}
	
	var searchGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				region : 'center',
				height : 230,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				tbar : [i_nameC, new Ext.form.TextField({
							id : 'searchNameId',
							enableKeyEvents : true,
							width : 500,
							listeners : {
								'keyup' : function(o) {
									if ($.trim(o.getValue()) != "") {
										searchStore.setBaseParam("searchName",
												o.getValue());
										searchStore.load();

									}

								}
							}
						})],
				store : searchStore,
				colModel : searchModel,
				listeners : {
					'rowdblclick' : function(grid, i, e) {
						var sr = grid.getStore().getAt(i);
						var flag = false;
						resultStore.each(function(r) {
									if (sr.get("id") == r.get("id")) {
										resultGridPanel.getSelectionModel()
												.selectRecords([r]);
										flag = true;
										return false;
									}
								});
						if (!flag) {
							resultStore.add(sr);
						}

					}
				}
			});

	/** ****************************************************************** */
	var resultStore = new Ext.data.JsonStore({
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						},{
							name : "orgName",
							mapping : "orgName"
						}]
			});
	var resultModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var resultGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				height : 205,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				store : resultStore,
				colModel : resultModel
			});

	var selectMainPanel = new Ext.Panel({
				height : 440,
				width : 690,
				layout : 'border',
				border : 0,
				frame : false,
				items : [orgPosPerMultipleTree, {
							region : 'center',
							items : [searchGridPanel, resultGridPanel]
						}]

			})
	var multipleSelectWin = new Ext.Window({
				title : selectOrgPosTitle,
				height : 500,
				width : 700,
				minHeight : 500,
				minWidth : 700,
				modal : true,
				constrainHeader : true,//证窗口顶部不超出浏览器 
				closeAction : "close",
				resizable : false,// 四边和四角改变window的大小
				items : selectMainPanel,
				buttons : [{
					text : i_Delete,// 删除
					handler : function() {
						Ext.each(resultGridPanel.getSelectionModel()
										.getSelections(), function(r) {
									resultStore.remove(r);
								});
					},
					scope : this
				}, {
					text : i_empty,// 清空
					handler : function() {
						Ext.Msg.confirm('提示', '是否清空所有数据!', function(fc) {
									if (fc == "yes") {
										resultStore.removeAll();
									}
								});
					},
					scope : this
				}, {
					text : i_ok,// 确定
					handler : function() {
					    var s = "";
						var ids = "";
						var item ="";
						resultStore.each(function(r) {
							if(s==''){
								s = r.get("name");
							}else{
								s = s + "\n"+r.get("name") ;
							}
							
							if(ids==''){
								ids = r.get("id");
							}else{
								ids = ids + ","+r.get("id") ;
							}
								});
						document.getElementById(inputDomId).value = s;
						document.getElementById(hiddenDomId).value = ids;
						multipleSelectWin.close();// 窗口关闭
					},
					scope : this
				}, {
					text : i_close,// 关闭
					handler : function() {
						multipleSelectWin.close();// 窗口关闭
					}
				}]
			});
	if (document.getElementById(inputDomId).value != '') {
		var names = document.getElementById(inputDomId).value.split('\n');
		var ids = document.getElementById(hiddenDomId).value.split(',');
		for (i = 0; i < ids.length; i++) {
			var sr = new Ext.data.Record();
			sr.set("id", ids[i]);
			sr.set("name", names[i]);
			resultStore.add(sr);
		}
	}

	multipleSelectWin.show();
}



function selectProcessOrMaWindow(inputDomId, hiddenDomId) {

	var selectProcessOrMapRoot = new Ext.tree.AsyncTreeNode({
				text : i_processOrMapSelect,
				expanded : true,
				id : "0"
			});

	var selectProcessOrMapLoader = new Ext.tree.TreeLoader({
				dataUrl : 'getChildFlows.action'
			});

	var isDblclick = false;
	var selectProcessOrMapTree = new Ext.tree.TreePanel({
		root : selectProcessOrMapRoot,
		loader : selectProcessOrMapLoader,
		height : 435,
		useArrows : true,
		autoScroll : true,
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			'beforedblclick' : function(node, e) {
				if (node.attributes.type == "process"
						|| node.attributes.type == "processMap") {
					isDblclick = true;
					document.getElementById(inputDomId).value = node.attributes.trueText;
					document.getElementById(hiddenDomId).value = node.attributes.id;
					win.close();
					return false;
				}
			}

		}

	});
	var win = new Ext.Window({
				title : i_processOrMapSelect,
				height : 500,
				width : 400,
				minHeight : 500,
				minWidth : 400,
				modal : true,
				constrainHeader : true,//保证窗口顶部不超出浏览器 
				resizable : false,// 四边和四角改变window的大小
				closeAction : "close",
				items : selectProcessOrMapTree,
				buttons : [{
							text : i_empty,
							handler : function() {
								document.getElementById(inputDomId).value = "";
								document.getElementById(hiddenDomId).value = -1;
								win.close();// 窗口关闭
							}

						}, {
							text : i_close,// 关闭
							handler : function() {
								win.close();// 窗口关闭
							}
						}]
			});

	win.show();
}

/***
 * 流程、制度 单选
 * @param type :类型：ruleChoose：制度；flowChoose：流程
 * @param {Object} inputDomId
 * @param {Object} hiddenDomId
 * @return {TypeName} 
 */
function selectProcessOrRuleWindow(type,inputDomId, hiddenDomId) {
//	var type = document.getElementById("chooseRtnlType_id").value;
	var textName;
	var dataUrlStr;
	if(type == "0"){//流程
		textName = i_processOrMapSelect;
		dataUrlStr = 'getChildFlows.action';
		document.getElementById("isFlowRtnl").value = type;
	}else if(type =="1"){//制度
		textName = i_ruleSelect;
		dataUrlStr = 'getChildRules.action';
		document.getElementById("isFlowRtnl").value = type;
	}
	var selectProcessOrRuleRoot = new Ext.tree.AsyncTreeNode({
				text : textName,
				expanded : true,
				id : "0"
			});
	var selectProcessOrRuleLoader = new Ext.tree.TreeLoader({
				dataUrl : dataUrlStr
			});
	var isDblclick = false;
	var selectProcessOrRuleTree = new Ext.tree.TreePanel({
		root : selectProcessOrRuleRoot,
		loader : selectProcessOrRuleLoader,
		height : 435,
		useArrows : true,
		autoScroll : true,
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			'beforedblclick' : function(node, e) {
				if (node.attributes.type == "process"//流程
						|| node.attributes.type == "ruleFile"//制度文件
							|| node.attributes.type == "ruleModeFile") {//制度模板文件
					isDblclick = true;
					document.getElementById(inputDomId).value = node.attributes.text;
					document.getElementById(hiddenDomId).value = node.attributes.id;
					win.close();
					return false;
				}
			}
		}
	});
	var win = new Ext.Window({
				title : textName,
				height : 500,
				width : 400,
				minHeight : 500,
				minWidth : 400,
				modal : true,
				constrainHeader : true,//保证窗口顶部不超出浏览器 
				resizable : false,// 四边和四角改变window的大小
				closeAction : "close",
				items : selectProcessOrRuleTree,
				buttons : [{
							text : i_empty,
							handler : function() {
								document.getElementById(inputDomId).value = "";
								document.getElementById(hiddenDomId).value = -1;
								win.close();// 窗口关闭
							}
						}, {
							text : i_close,// 关闭
							handler : function() {
								win.close();// 窗口关闭
							}
						}]
			});

	win.show();
}
/**
 * 系统退出
 */
function exit() {
	if (window.confirm(i_sureExit)) {
		window.location.href = "exit.action";
	}

}

// 流程应用人数统计
function processAnalysisWH(div) {
	document.getElementById(div).style.height = checkWidthHeight(40);
}

// 报表
function setReportsDivWidthHeight(divId) {
	document.getElementById(divId).style.height = checkWidthHeight(235);
}

// 流程清单
function setDivWidthHeight(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(215);
	}

}
// 弹出流程清单
function setShowDivWidthHeight(divId) {
	document.getElementById(divId).style.height = checkWidthHeight(140);
}

// 岗位说明书 最大部分
function setMyhomeMaxjobDesWH(divId) {
	document.getElementById(divId).style.height = checkWidthHeight(210);
}

// 岗位说明书 最大部分
function setMaxjobDesWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(150);
	}

}

// 岗位说明书 最大部分
function setShowMaxjobDesWH(divId) {
	document.getElementById(divId).style.height = checkWidthHeight(70);
}

// 操作说明
function setProcessFileDivWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(220);
	}

}

// 弹出操作说明
function setShowProcessFileDivWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(140);
	}

}

// 制度操作说明
function setRuleFileDivWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(280);
	}

}

// 弹出制度操作说明
function setShowRuleFileDivWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(80);
	}

}

// 我参与的流程
function setMyProcessDivWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(255);
	}

}

// 我的工作活动
function setMyActivitiesDivWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(255);
	}

}

// 流程操作模板
function setProcessTemplate(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(180);
	}

}

// 文控信息
function setDocumentTemplate(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(120);
	}

}

// 文控信息
function setShowDocumentTemplate(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(40);
	}

}

// 弹出流程操作模板
function setShowProcessTemplate(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(100);
	}

}

// 弹出文控信息
function setShowDocument(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(60);
	}

}

// 任务弹出流程操作模板
function setShowTaskProcessTemplate(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(50);
	}

}

// 文件使用情况
function setShowFile(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(300);
	}

}

// 风险体系使用情况
function setShowRiskSystem(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(300);
	}
}

// 任务文件使用情况
function setTaskShowFile(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(300);
	}

}

// 标准文件详情
function setShowStandardDetailFile(divId) {
	divId.style.height = checkWidthHeight(350);
}

// 相关流程
function setRelatedProcessWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(160);
	}

}

// 弹出相关流程
function setShowRelatedProcessWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(100);
	}

}

// 切换项目
function setProjectWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(0);
	}

}

// 制度内容
function setRuleRelatedProcessWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(220);
	}

}

// 制度内容
function setShowRuleRelatedProcessWH(divId) {
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(150);
	}

}

function setProcessAnalysisWH(divId){
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(260);
	}
}
// 合理化建议高度
function setRtnlPropseWH(divId){
	if (document.getElementById(divId)) {
		document.getElementById(divId).style.height = checkWidthHeight(368);
	}
}
//流程kpi图片与数据录入区的高度
function setKpiShowWH(divId){
	if (document.getElementById(divId)) {
		var clientHeight = document.documentElement.clientHeight;
		//kpi显示区上边栏的高度为226	
		if(clientHeight-156>440)
		{
			document.getElementById(divId).style.height = checkWidthHeight(156);
		}else{
			document.getElementById(divId).style.height='440px';
	    }
	}
}

function checkWidthHeight(num) {
	// 屏幕可用区域
	var clientHeight = document.documentElement.clientHeight;
	if (clientHeight - num < 0) {
		return "100px";
	} else {
		return clientHeight - num + "px";
	}
}

function getTimeStamp() {
	return new Date().getTime();
}

var taskTimer = {
	run : function() {
		var jecnProcess = document.getElementById("jecnProcessId");
		try {
			if (jecnProcess != null && !jecnProcess.isLoaded()) {
			} else {
				Ext.MessageBox.hide();
				Ext.TaskMgr.stop(taskTimer);
			}
		} catch (e) {
			Ext.MessageBox.hide();
			Ext.TaskMgr.stop(taskTimer);
		}
	},
	interval : 500
};
/*
 * 鼠标移动上以后显示全部内容
 */
function commonNameRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'>"+value+"</div>";
}


//部门选择框 有个数限制
/**
 * 部门、岗位、或人员多选 暂时只实现了部门，人员的多选 如果需要岗位可以新加
 * 
 * @param {}  
 *            type 类型（organization：部门；position：岗位；person：人员;positionGroup：岗位组）
 * @param {}
 *            inputDomId 显示的名称
 * @param {}
 *            hiddenDomId 隐藏的ID
 * @param {}
 *            showId 显示的位置的id
 * @param {}
 *            limit 部门、岗位、人员的个数
 * 
 */
function orgPosPerMultipleSelectHasLimit(type, inputDomId, hiddenDomId,showId,limit) {
	if (!type || type == "") {
		return;
	}
	var selectOrgPosTitle;
	var dataUrl;
	var gridUrl;
	var searchModel;
	if (type == "organization") {
		selectOrgPosTitle = i_orgSelect;
		dataUrl = "getChildOrg.action";
		gridUrl = "orgSearch.action";
	} else if (type == "position") {
		selectOrgPosTitle = i_posSelect;
		 
	}else if(type=="positionGroup"){
        selectOrgPosTitle=proGroup;
         
	}else if (type=="person") {
		
		//TODO
		selectOrgPosTitle = i_personSelect;
		dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
		gridUrl = "peopleSearch.action";
	}

	var orgPosPerMultipleRoot = new Ext.tree.AsyncTreeNode({
				text : selectOrgPosTitle,
				expanded : true,
				id : "0"
			})
    
	//加载tree节点，返回json
	var orgPosPerMultipleLoader = new Ext.tree.TreeLoader({
				dataUrl : dataUrl
	})

	var orgPosPerMultipleTree = new Ext.tree.TreePanel({
		root : orgPosPerMultipleRoot,
		loader : orgPosPerMultipleLoader,
		height : 495,
		width : 180,
		region : 'west',
		autoScroll : true,
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
		'beforeload' : function(node) {
			if (type == "person") {
					if (node.attributes.type == "position") {
						orgPosPerMultipleLoader.dataUrl = "getChildPerson.action";
					}else {
					    orgPosPerMultipleLoader.dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
					}
			 }
			},
			'dblclick' : function(node, e) {
				if (node.attributes.type == type) {
					var flag = false;
					resultStore.each(function(r) {
								if (node.id.split("_")[0] == r.get("id")) {
									resultGridPanel.getSelectionModel()
											.selectRecords([r]);
									flag = true;
									return false;
								}
							});
					if (!flag) {
						//TODO
						//判断limit个数
						var dataCount=0;
						resultStore.each(function(r) {
								
							dataCount++;
							
							});
						if(dataCount>=limit)
						{
							alert(i_cannotBigThan+limit);
							return;
						}
						var sr = new Ext.data.Record();
						sr.set("id", node.id.split("_")[0]);
						sr.set("name", node.attributes.text);
						resultStore.add(sr);
					}
				}
			}
		}
	})

	var searchStore = new Ext.data.JsonStore({
				autoLoad : false,
				proxy : new Ext.data.HttpProxy({
							url : gridUrl
						}),
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						}, {
							name : "orgName",
							mapping : "orgName"
						}]
			});
	if(type=="position" || type=="person"){//查询岗位/人员时，显示所属部门
		searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						},{
							header : i_orgName,
							dataIndex : "orgName"
						}]
			});
	}else{
		searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	}
	
	var searchGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				region : 'center',
				height : 230,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				tbar : [i_nameC, new Ext.form.TextField({
							id : 'searchNameId',
							enableKeyEvents : true,
							width : 500,
							listeners : {
								'keyup' : function(o) {
									if ($.trim(o.getValue()) != "") {
										searchStore.setBaseParam("searchName",
												o.getValue());
										searchStore.load();

									}

								}
							}
						})],
				store : searchStore,
				colModel : searchModel,
				listeners : {
					'rowdblclick' : function(grid, i, e) {
						var sr = grid.getStore().getAt(i);
						var flag = false;
						resultStore.each(function(r) {
									if (sr.get("id") == r.get("id")) {
										resultGridPanel.getSelectionModel()
												.selectRecords([r]);
										flag = true;
										return false;
									}
								});
						if (!flag) {
							resultStore.add(sr);
						}

					}
				}
			});

	/** ****************************************************************** */
	var resultStore = new Ext.data.JsonStore({
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						},{
							name : "orgName",
							mapping : "orgName"
						}]
			});
	var resultModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var resultGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				height : 205,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				store : resultStore,
				colModel : resultModel
			});

	var selectMainPanel = new Ext.Panel({
				height : 440,
				width : 690,
				layout : 'border',
				border : 0,
				frame : false,
				items : [orgPosPerMultipleTree, {
							region : 'center',
							items : [searchGridPanel, resultGridPanel]
						}]

			});
	var multipleSelectWin = new Ext.Window({
				title : selectOrgPosTitle,
				height : 500,
				width : 700,
				minHeight : 500,
				minWidth : 700,
				modal : true,
				constrainHeader : true,//证窗口顶部不超出浏览器 
				closeAction : "close",
				resizable : false,// 四边和四角改变window的大小
				items : selectMainPanel,
				buttons : [{
					text : i_Delete,// 删除
					handler : function() {
						Ext.each(resultGridPanel.getSelectionModel()
										.getSelections(), function(r) {
									resultStore.remove(r);
								});
					},
					scope : this
				}, {
					text : i_empty,// 清空
					handler : function() {
						Ext.Msg.confirm(i_tip,i_isClearAllData, function(fc) {
									if (fc == "yes") {
										resultStore.removeAll();
									}
								});
					},
					scope : this
				}, {
					text : i_ok,// 确定
					handler : function() {
						var s = "";
						var ids = "";
						var item ="";
						resultStore.each(function(r) {
							if(s==""){
								s = r.get("name");
							}else{
								s = s + "\n"+r.get("name") ;
							}
							
							if(ids==""){
								ids = r.get("id");
							}else{
								ids = ids + ","+r.get("id") ;
							}
								});
						document.getElementById(inputDomId).value = s;
						document.getElementById(hiddenDomId).value = ids;
						//显示 id: select_access_id
						if (type == "organization") {//部门
							
							item="<ul class='f-list' >";
							resultStore.each(function(r) {
								
							var oId="org"+r.get("id"); //id
							var oName=r.get("name"); //名称
							
						    item=item+"<li>"
							+"<a title='"+i_close+"' rel='nofollow' href='#'" 
							+"id='"+oId+"'" 
							+"onclick=closeOneOrganization('"+inputDomId+"','"+hiddenDomId+"','"+showId+"','"+oId+"')>" 
							+"<strong>"
							+oName
							+"</strong>"
							+"<img src='images/close_item.gif' />"
							+"</a>" 
						    +"</li>";
									 
							});
							item=item+" </ul>";
							
							
						
						} else if (type == "position") {
							
						}else if(type=="positionGroup"){
					        
						}else if (type=="person") {
							
							item="<ul class='f-list' >";
							resultStore.each(function(r) {
								
							var oId="person"+r.get("id"); //id
							var oName=r.get("name"); //名称
							
						    item=item+"<li>"
							+"<a title='"+i_close+"' rel='nofollow' href='#'" 
							+"id='"+oId+"'" 
							+"onclick=closeOnePerson('"+inputDomId+"','"+hiddenDomId+"','"+showId+"','"+oId+"')>" 
							+"<strong>"
							+oName
							+"</strong>"
							+"<img src='images/close_item.gif' />"
							+"</a>" 
						    +"</li>";
									 
							});
							item=item+" </ul>";
						}
						
						document.getElementById(showId).innerHTML=item;
						
						multipleSelectWin.close();// 窗口关闭
					},
					scope : this
				}, {
					text : i_close,// 关闭
					handler : function() {
						multipleSelectWin.close();// 窗口关闭
					}
				}]
			});
	if (document.getElementById(inputDomId).value != '') {
		var names = document.getElementById(inputDomId).value.split('\n');
		var ids = document.getElementById(hiddenDomId).value.split(',');
		for (i = 0; i < ids.length; i++) {
			var sr = new Ext.data.Record();
			sr.set("id", ids[i]);
			sr.set("name", names[i]);
			resultStore.add(sr);
		}
	}

	multipleSelectWin.show();
}

//--------------------------------------------------------------------------
//流程地图选择框有个数限制

/**
 * 流程，流程地图多选
 *          暂时只实现了流程地图的多选 如果需要可以自己添加流程图多选
 * 
 * @param 
 *            type 类型（process：流程；processMap：流程地图)
 * @param 
 *            inputDomId 显示的名称
 * @param 
 *            hiddenDomId 隐藏的ID
 * @param 
 *            showId 显示的位置的id   
 * @param 
 *            limit 流程流程地图的个数
 * 
 */
function processAndProcessMapSelectHasLimit(type, inputDomId, hiddenDomId,showId,limit) {
	if (!type || type == "") {
		return;
	}
	
	var selectProcessAndProcessMapTitle;
	var dataUrl;
	var gridUrl;
	var searchModel;
	if (type == "process") {
		selectProcessAndProcessMapTitle = i_orgSelect;
		
	} else if (type == "processMap") {
		selectProcessAndProcessMapTitle = i_posSelect;
		dataUrl = "getChildFlows.action";
		gridUrl = "searchFlowByName.action";
	} else if (type == "all") {
		selectProcessAndProcessMapTitle = "流程选择";
		dataUrl = "getChildFlows.action";
		gridUrl = "searchFlowByName.action";
	}

	var processMapPerMultipleRoot = new Ext.tree.AsyncTreeNode({
				text : selectProcessMapTitle,
				expanded : true,
				id : "0"
			})
    
	//加载tree节点，返回json
	var processMapPerMultipleLoader = new Ext.tree.TreeLoader({
				dataUrl : dataUrl
	})

	var processAndProcessMapMultipleTree = new Ext.tree.TreePanel({
		root : processMapPerMultipleRoot,
		loader : processMapPerMultipleLoader,
		height : 495,
		width : 180,
		region : 'west',
		autoScroll : true,
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			'dblclick' : function(node, e) {
		
		if(type=="all"){//可选流程与流程架构
		    	 	var flag = false;
					resultStore.each(function(r) {
								if (node.id.split("_")[0] == r.get("id")) {
									resultGridPanel.getSelectionModel()
											.selectRecords([r]);
									flag = true;
									return false;
								}
							});
					if (!flag) {

						//判断limit个数
						var dataCount=0;
						resultStore.each(function(r) {
								
							dataCount++;
							
							});
						if(dataCount>=limit)
						{
							alert(i_cannotBigThan+limit);
							return;
						}
						var sr = new Ext.data.Record();
						sr.set("id", node.id.split("_")[0]);
						sr.set("name", node.attributes.trueText);
						
						resultStore.add(sr);
					}
					
		       }else if (node.attributes.type == type) {
					var flag = false;
					resultStore.each(function(r) {
								if (node.id.split("_")[0] == r.get("id")) {
									resultGridPanel.getSelectionModel()
											.selectRecords([r]);
									flag = true;
									return false;
								}
							});
					if (!flag) {
						//TODO
						//判断limit个数
						var dataCount=0;
						resultStore.each(function(r) {
								
							dataCount++;
							
							});
						if(dataCount>=limit)
						{
							alert(i_cannotBigThan+limit);
							return;
						}
						var sr = new Ext.data.Record();
						sr.set("id", node.id.split("_")[0]);
						sr.set("name", node.attributes.trueText);
						resultStore.add(sr);
					}
				}
			}
		}
	})

	var searchStore = new Ext.data.JsonStore({
				autoLoad : false,
				proxy : new Ext.data.HttpProxy({
							url : gridUrl
						}),
				 fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						}]
			});

		searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						   }
				]
			});
	
	
	var searchGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				region : 'center',
				height : 230,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				tbar : [i_nameC, new Ext.form.TextField({
							id : 'searchNameId',
							enableKeyEvents : true,
							width : 500,
							listeners : {
								'keyup' : function(o) {
									if ($.trim(o.getValue()) != "") {
										searchStore.setBaseParam("searchName",
												o.getValue());
										
										searchStore.setBaseParam("type",type);
										
										searchStore.load();

									}

								}
							}
						})],
				store : searchStore,
				colModel : searchModel,
				listeners : {
					'rowdblclick' : function(grid, i, e) {
						var sr = grid.getStore().getAt(i);
						var flag = false;
						resultStore.each(function(r) {
									if (sr.get("id") == r.get("id")) {
										resultGridPanel.getSelectionModel()
												.selectRecords([r]);
										flag = true;
										return false;
									}
								});
						if (!flag) {
							resultStore.add(sr);
						}

					}
				}
			});

	/** ****************************************************************** */
	var resultStore = new Ext.data.JsonStore({
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						}]
			});
	var resultModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var resultGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				height : 205,
				border : 0,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				store : resultStore,
				colModel : resultModel
			});

	var selectMainPanel = new Ext.Panel({
				height : 440,
				width : 690,
				layout : 'border',
				border : 0,
				frame : false,
				items : [processAndProcessMapMultipleTree, {
							region : 'center',
							items : [searchGridPanel, resultGridPanel]
						}]

			})
	var multipleSelectWin = new Ext.Window({
				title : selectProcessMapTitle,
				height : 500,
				width : 700,
				minHeight : 500,
				minWidth : 700,
				modal : true,
				constrainHeader : true,//证窗口顶部不超出浏览器 
				closeAction : "close",
				resizable : false,// 四边和四角改变window的大小
				items : selectMainPanel,
				buttons : [{
					text : i_Delete,// 删除
					handler : function() {
						Ext.each(resultGridPanel.getSelectionModel()
										.getSelections(), function(r) {
									resultStore.remove(r);
								});
					},
					scope : this
				}, {
					text : i_empty,// 清空
					handler : function() {
						Ext.Msg.confirm(i_tip,i_isClearAllData, function(fc) {
									if (fc == "yes") {
										resultStore.removeAll();
									}
								});
					},
					scope : this
				}, {
					text : i_ok,// 确定
					handler : function() {
					
						var s = "";
						var ids = "";
						var item ="";
						resultStore.each(function(r) {
							if(s==""){
								s = r.get("name");
							}else{
								s = s + "\n"+r.get("name") ;
							}
							
							if(ids==""){
								ids = r.get("id");
							}else{
								ids = ids + ","+r.get("id") ;
							}
								});
						document.getElementById(inputDomId).value = s;
						document.getElementById(hiddenDomId).value = ids
						
						//显示 id: select_access_id
						if (type == "process") {//流程
							
							return;//由于暂时没有用所以return
						
						} else if (type == "processMap") {
							item=item+"<ul class='f-list' >";
							resultStore.each(function(r) {
								
							var oId="map"+r.get("id"); //id
							var oName=r.get("name"); //名称
							
						    item=item+"<li>"
							+"<a title='"+i_close+"' rel='nofollow' href='#'" 
							+"id='"+oId+"'" 
							+"onclick=closeOneProcessMap('"+inputDomId+"','"+hiddenDomId+"','"+showId+"','"+oId+"')>" 
							+"<strong>"
							+oName
							+"</strong>"
							+"<img src='images/close_item.gif' />"
							+"</a>" 
						    +"</li>";
									 
							});
							item=item+" </ul>";
							
							document.getElementById(showId).innerHTML=item;
							
							
						} else{//all
							item=item+"<ul class='f-list' >";
							resultStore.each(function(r) {
								
							var oId="map"+r.get("id"); //id
							var oName=r.get("name"); //名称
							
						    item=item+"<li>"
							+"<a title='"+i_close+"' rel='nofollow' href='#'" 
							+"id='"+oId+"'" 
							+"onclick=closeOneProcessMap('"+inputDomId+"','"+hiddenDomId+"','"+showId+"','"+oId+"')>" 
							+"<strong>"
							+oName
							+"</strong>"
							+"<img src='images/close_item.gif' />"
							+"</a>" 
						    +"</li>";
									 
							});
							item=item+" </ul>";
							
							document.getElementById(showId).innerHTML=item;
							
						}
						
						
						multipleSelectWin.close();// 窗口关闭
					},
					scope : this
				}, {
					text : i_close,// 关闭
					handler : function() {
						multipleSelectWin.close();// 窗口关闭
					}
				}]
			});
	if (document.getElementById(inputDomId).value != '') {
		var names = document.getElementById(inputDomId).value.split('\n');
		var ids = document.getElementById(hiddenDomId).value.split(',');
		for (i = 0; i < ids.length; i++) {
			var sr = new Ext.data.Record();
			sr.set("id", ids[i]);
			sr.set("name", names[i]);
			resultStore.add(sr);
		}
	}
	multipleSelectWin.show();
}

//关闭部门选择框中的所有选择的item
function closeAllOrganization(orgDutyNameId,orgDutyIdId,showId)
{
	//select_organization_id
	document.getElementById(showId).innerHTML="";
	//将隐藏域中的id和名称清除
	document.getElementById(orgDutyNameId).value = "";
    document.getElementById(orgDutyIdId).value = "-1";
}

//关闭一个部门中选择的item
function closeOneOrganization(orgNamesId,orgDutyIdId,showId,id)
{
	if(id==null&&id=="")
	{
		return;
	}
	
	//删除隐藏域的id
	var resOrgId=document.getElementById(orgDutyIdId).value;
	
	var resOrgIdArr=resOrgId.split(",");
	var orgIndex=-1;
	resOrgId="";
	for(var i=0;i<resOrgIdArr.length;i++)
	{
		//由于为了标示区别每一个item的id之前会标示类型
		var tempId="org"+resOrgIdArr[i];
		if(tempId==id)
		{
			orgIndex=i;
		}
		else if(tempId!=id)//把不删除的拼装
		{
			resOrgId=resOrgId+resOrgIdArr[i]+",";
		}
		
	}
	resOrgId=resOrgId.substring(0,resOrgId.length-1);
	document.getElementById(orgDutyIdId).value=resOrgId;
	
	//删除隐藏域中的部门名称
	var resOrgName=document.getElementById(orgNamesId).value;
	//orgDutyName_access_id
	var resOrgNameArr=resOrgName.split("\n");
	var resOrgName="";
	for(var i=0;i<resOrgIdArr.length;i++)
	{
		if(orgIndex!=i)
		{
			resOrgName=resOrgName+resOrgNameArr[i]+"\n";
		}
		
	}
	resOrgName=resOrgName.substring(0,resOrgName.length-1);
	document.getElementById(orgNamesId).value=resOrgName;
	
	
	//删除在页面显示的列表
	var parentNode=document.getElementById(id).parentNode;
	document.getElementById(id).parentNode.parentNode.removeChild(parentNode);
	if(resOrgId.length==0)
	{
		document.getElementById(showId).innerHTML="";
		document.getElementById(orgDutyIdId).value = "-1";
		document.getElementById(orgNamesId).value = "";
	}
	
	
	
}

//关闭所有流程地图中所有选择的item
function closeAllProcessMap(processNameId,processIdId,showId)
{
	//select_processMap_id
	document.getElementById(showId).innerHTML="";
	//将隐藏域中的id和名称清除
	document.getElementById(processNameId).value = "";
    document.getElementById(processIdId).value = "-1";
}

//关闭一个流程地图中选择的item
function closeOneProcessMap(processNamesId,processIdsId,showId,id)
{
	if(id==null&&id=="")
	{
		return;
	}
	
	//删除隐藏域的id
	var resProcessMapId=document.getElementById(processIdsId).value;
	
	var resProcessMapIdArr=resProcessMapId.split(",");
	var processMapIndex=-1;
	resProcessMapId="";
	for(var i=0;i<resProcessMapIdArr.length;i++)
	{   
		//由于为了标示区别每一个item的id之前会标示类型
		var tempId="map"+resProcessMapIdArr[i];
		if(tempId==id)
		{
			processMapIndex=i;
		}
		else if(tempId!=id)//把不删除的拼装
		{
			resProcessMapId=resProcessMapId+resProcessMapIdArr[i]+",";
		}
		
	}
	resProcessMapId=resProcessMapId.substring(0,resProcessMapId.length-1);
	document.getElementById(processIdsId).value=resProcessMapId;
	
	//删除隐藏域中的地图
	var resProcessMapName=document.getElementById(processNamesId).value;
	//processMapName_access_id
	var resProcessMapNameArr=resProcessMapName.split("\n");
	var resProcessMapName="";
	for(var i=0;i<resProcessMapIdArr.length;i++)
	{
		if(processMapIndex!=i)
		{
			resProcessMapName=resProcessMapName+resProcessMapNameArr[i]+"\n";
		}
		
	}
	resProcessMapName=resProcessMapName.substring(0,resProcessMapName.length-1);
	document.getElementById(processNamesId).value=resProcessMapName;
	
	
	//删除在页面显示的列表
	var parentNode=document.getElementById(id).parentNode;
	document.getElementById(id).parentNode.parentNode.removeChild(parentNode);
	if(resProcessMapId.length==0)
	{
		document.getElementById(showId).innerHTML="";
		document.getElementById(processIdsId).value = "-1";
		document.getElementById(processNamesId).value="";
	}
	
}


/**
 * 关闭一个人员选择的item
 * @param {Object} namesId 隐藏域中的名称的id
 * @param {Object} idsId 隐藏域中的id的id
 * @param {Object} showId 显示的位置的id
 * @param {Object} id 要删除的item的id
 * @return {TypeName} 
 */
function closeOnePerson(namesId,idsId,showId,id)
{
	closeOneItem(namesId,idsId,showId,id,"person");
	
}

function closeOneItem(namesId,idsId,showId,id,type){
	
	if(id==null&&id=="")
	{
		return;
	}
	
	
	var resId=document.getElementById(idsId).value;
	
	var resIdArr=resId.split(",");
	var resIndex=-1;
	resId="";
	for(var i=0;i<resIdArr.length;i++)
	{   
		//由于为了标示区别每一个item的id之前会标示类型
		var tempId=type+resIdArr[i];
		if(tempId==id)
		{
			resIndex=i;
		}
		else if(tempId!=id)//把不删除的拼装
		{
			resId=resId+resIdArr[i]+",";
		}
		
	}
	resId=resId.substring(0,resId.length-1);
	document.getElementById(idsId).value=resId;
	
	//删除隐藏域中的地图
	var resName=document.getElementById(namesId).value;

	var resNameArr=resName.split("\n");
	var resName="";
	for(var i=0;i<resIdArr.length;i++)
	{
		if(resIndex!=i)
		{
			resName=resName+resNameArr[i]+"\n";
		}
		
	}
	resName=resName.substring(0,resName.length-1);
	document.getElementById(namesId).value=resName;
	
	//删除在页面显示的列表
	var parentNode=document.getElementById(id).parentNode;
	document.getElementById(id).parentNode.parentNode.removeChild(parentNode);
	if(resId.length==0)
	{
		document.getElementById(showId).innerHTML="";
		document.getElementById(idsId).value = "-1";
		document.getElementById(namesId).value="";
	}
	
	
}

//时间相隔的月份 不准确不计算天数 如果返回的值为0不一定说明天数相等 日期格式为yyyy-mm-dd 
function spaceMonths(startTime,endTime)
{
	var date1 = endTime;
	var date2 = startTime;
	if (date2 > date1) {// 结束时间大于开始时间
		return -1;
	}else{
		// 拆分年月日
		date1 = date1.split('-');
		// 得到月数
		date1 = parseInt(date1[0]) * 12 + parseInt(date1[1]);
		// 拆分年月日
		date2 = date2.split('-');
		// 得到月数
		date2 = parseInt(date2[0]) * 12 + parseInt(date2[1]);
		var space = Math.abs(date1 - date2);
	    return space;
    }
}

/**
 * 去除input中的值
 * @param {Object} formId 表单的id
 */
function clearInputValue(formId){
	
	$("#"+formId).children("input").each(function(){
		
		$(this).attr("value",'');
		
	});
	
}
