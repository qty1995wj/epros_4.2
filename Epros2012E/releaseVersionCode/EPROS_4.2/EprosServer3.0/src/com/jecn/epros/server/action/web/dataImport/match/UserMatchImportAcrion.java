package com.jecn.epros.server.action.web.dataImport.match;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.jecn.epros.server.action.web.timer.TimerAction;
import com.jecn.epros.server.action.web.timer.UserAutoBuss;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.match.IMatchDataFactory;
import com.jecn.epros.server.service.dataImport.match.IUserMatchService;
import com.jecn.epros.server.service.dataImport.match.buss.ReadUserConfigXml;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool.DataErrorType;
import com.jecn.epros.server.service.popedom.IOrganizationService;
import com.jecn.epros.server.util.JecnKey;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 
 * 带有岗位匹配的人员同步
 * 
 */
public class UserMatchImportAcrion extends TimerAction {

	private final Log log = LogFactory.getLog(UserMatchImportAcrion.class);
	/** 执行待导入数据导入的业务处理对象 */
	private IUserMatchService matchService = null;
	/** 获取待导入数据接口 */
	private IMatchDataFactory matchDataFactory = null;
	/** 定时器处理 */
	private UserAutoBuss userAutoBuss = null;
	/** 导入数据方式 */
	private String importType = MatchConstant.IMPORT_TYPE_DB;
	/** *****************配置文件:开始时间、间隔天数、自动还是手动 start****************** */
	protected AbstractConfigBean abstractConfigBean = null;
	/** 返回前台值 */
	private String stringResult = null;

	@Autowired
	@Qualifier("OrganizationServiceImpl")
	private IOrganizationService organizationService;
	/**
	 * 
	 * 初始化界面获取页面所需值
	 * 
	 */
	public String matchInitParams() {
		return initParams(getImportTypeByData());
	}
	
	/**
	 * 根据DB标识获取人员同步类型
	 * 
	 * @return
	 */
	private String getImportTypeByData() {
		int tmpValue = JecnContants.otherUserSyncType;
		switch (tmpValue) {
		case 3:// DB+岗位匹配
			importType = MatchConstant.IMPORT_TYPE_DB;
			break;
		case 6:// XML+岗位匹配 【大唐】
			importType=MatchConstant.IMPORT_TYPE_XML;
			break;
		default :// db
			importType = MatchConstant.IMPORT_TYPE_DB;
		}
		return importType;
	}

	/**
	 * 
	 * 保存参数
	 * 
	 */
	public void matchSaveParams() {
		log.info("保存定时参数开始...");

		// 保存参数
		String ret = saveParams(abstractConfigBean);

		if (JecnCommon.isNullOrEmtryTrim(ret)) {
			// 拼装返回页面
			super.outJsonString("false");
			return;
		}

		// 保存参数没有出错且当给定是自动同步时
		if (MatchConstant.RESULT_SUCCESS.equals(ret)
				&& MatchConstant.AUTO_SYNC_USER_DATA_FLAG
						.equals(abstractConfigBean.getIaAuto())) {

			// 重新开始定时监听
			reStartTime();
		}
		log.info("保存定时参数结束...");
		// 拼装返回页面
		super.outJsonString("true");
	}

	/**
	 * 
	 * 重新开始定时监听
	 * 
	 */
	public void reStartTime() {
		if (userAutoBuss == null) {
			return;
		}
		// 停止定时
		userAutoBuss.timerStop();

		// 开始监听
		initParams(MatchConstant.IMPORT_TYPE_DB);
		userAutoBuss.setTimerAction(this);
		userAutoBuss.setAutoBean(abstractConfigBean);
		userAutoBuss.timerStart();
		// 执行定时器
		isTimer = true;
	}

	/**
	 * 人员数据导入
	 */
	public void matchImportData() {
		long startTime = System.currentTimeMillis();
		log.info("同步人员数据开始" + System.currentTimeMillis());
		try {
			// 1. 清除错误文件
			deleteErrorLocalExcel(MatchTool.getOutPutExcelErrorDataPath());

			// 2. 判断主项目ID:
			// 如果查询出来结果没有值，直接导入；
			// 如果查询结果有数据有且只有一条数据，判断项目ID是否等于当前主项目ID：
			// 如果是执行导入，如果不是不导入提示不能导入
			// 如果查询结果有数据且有多条数据，返回提示不正确
			// 执行定时器获取项目ID
			Long curProjectId = matchService.getCurJecnProject();
			if (curProjectId == null) {// 项目ID不存在
				log.error("项目ID不存在!");
				return;
			}
			// 获取上次导入的项目ID，与当前项目ID做比较 false 项目ID不一致，不执行导入
			boolean projectB = matchService.checkProjectId(curProjectId);
			if (!projectB) {
				// 拼装返回页面
				outJsonString("false");
				log.info("导入的项目ID不等于上次同步的项目ID");
				return;
			}

			log.info("获取待同步数据开始：" + System.currentTimeMillis());
			// 4. 获取外来的待导入数据
			BaseBean baseBean = matchDataFactory.getImportData(importType);
			log.info("获取待同步数据结束：" + System.currentTimeMillis());

			if (baseBean == null) {
				// 拼装返回页面
				outJsonString("false");
				return;
			}
			// 5. 执行数据导入
			DataErrorType errorType = importData(baseBean);
			if (errorType == DataErrorType.rowsType) {// 行间校验出错，直接导出本地Excel
				log.info("错误信息保存到服务器本地开始：" + System.currentTimeMillis());

				// 把错误信息保存到服务器本地
				saveErrorExcelToLocal(baseBean);
				log.info("错误信息保存到服务器本地结束：" + System.currentTimeMillis());

				// 拼装返回页面
				outJsonString("false");
				return;
			} else if (errorType == DataErrorType.columnsType) {// 行内校验出错
				log.info("错误信息保存到服务器本地开始：" + System.currentTimeMillis());

				// 把错误信息保存到服务器本地
				matchService.exportExcelOriData();
				log.info("错误信息保存到服务器本地结束：" + System.currentTimeMillis());
				// 拼装返回页面
				outJsonString("false");
				return;
			} else if (errorType == DataErrorType.serverCount) {// 人数超出限制
				int serverCount = JecnKey.getServerPeopleCounts();
				log.info("人员同步导入人员超出服务最大限制最大用户数为：" + serverCount);
				// 拼装返回页面
				outJsonString("serverCountError");
				return;
			} else {
				// 拼装返回页面
				outJsonString("true");
				log.info("同步人员数据结束,同步人员数据所用时间"
						+ (System.currentTimeMillis() - startTime));
				return;
			}
		} catch (Exception ex) {
			log.error("UserMatchImportAcrion类matchImportData方法：", ex);
			outJsonString("false");
			return;
		}
	}

	/**
	 * 
	 */
	public void outJsonString(String str) {
		if (isTimer) {
			return;
		}
		super.outJsonString(str);
	}

	/**
	 * 数据同步
	 * 
	 * @param baseBean
	 * @return
	 * @throws Exception
	 */
	public DataErrorType importData(BaseBean baseBean) throws Exception {
		// 默认无验证错误
		DataErrorType errorType = DataErrorType.none;
		// 获取主项目ID
		Long curProjectId = matchService.getCurJecnProject();

		// 1.读取数据：通用接口
		if (baseBean == null || baseBean.isNull()) {
			return errorType;
		}

		// 2.验证数据：行内验证
		errorType = matchService.checkData(baseBean);
		if (errorType.equals(DataErrorType.rowsType)
				|| errorType.equals(DataErrorType.serverCount)) {// 行内校验异常
			log.error("行内校验异常！ errorType = " + errorType.toString());
			return errorType;
		}

		// 3.添加当前项目到DeptBean中
		for (DeptMatchBean deptBean : baseBean.getDeptBeanList()) {
			deptBean.setProjectId(curProjectId);
		}
		log.info("3.添加当前项目到DeptBean中");
		// ----------------------验证数据 end

		// 4.清除临时库、入库临时库
		matchService.deleteAndInsertTmpJecnIOTable(baseBean);
		log.info("4.清除临时库、入库临时库");

		// 5 .人员，部门行间校验
		errorType = matchService.checkDataColumns();
		if (errorType.equals(DataErrorType.columnsType)) {// 行内校验异常
			log.error("行内校验异常！");
			return errorType;
		}
		log.info("5 .人员，部门行间校验");

		// 6.与实际库数据做验证
		// 待导入部门、岗位以及人员中在各自真实库中存在的数据， 更新其操作标识设置为1

		/** 如果 实际人员表、实际部门表、实际岗位表中有数据，则用临时表中的数据与实际表中的数据进行比较 */
		log.info("6.与实际库数据做验证");
		matchService.updateProFlag(curProjectId);

		// 7.同步实际库 ***************下面的操作就需要事务处理
		syncImortDataAndDB();
		log.info("7.同步实际库 ***************下面的操作就需要事务处理");
		
		return errorType;

	}

	/**
	 * 
	 * 同步实际库
	 * 
	 * @throws Exception
	 */
	private void syncImortDataAndDB() throws Exception {
		// 操作人员表与临时表比较获取人员岗位变岗的集合 (一人多岗也视为岗位变更即人员编号相同，岗位编号不同视为岗位变更)
		
		//取到岗位变更的集合
		List<Object[]> posChangeList = matchService.findJecnUserObject();

		// 删除实际表(部门，岗位，人员)中的原有的数据
		matchService.deleteActualMatchTableData();
		log.info("删除实际表(部门，岗位，人员)中的原有的数据");

		// 将临时表(部门，岗位，人员)中的数据添加到实际表(部门，岗位，人员)
		matchService.insertActualMatchTableData();
		log.info("将临时表(部门，岗位，人员)中的数据添加到实际表(部门，岗位，人员)");

		// 删除人员岗位关系表 （删除 流程岗位ID在岗位匹配关系表中不存在的 记录）
		matchService.deletePosRelUser();
		log.info(" 删除人员岗位关系表 （删除 流程岗位ID在岗位匹配关系表中不存在的 记录）");

		// 同步岗位匹配关系表数据
		matchService.syncImortDataAndDB(posChangeList);
		log.info(" 同步岗位匹配关系表数据");
	}

	/**
	 * 定时器同步数据
	 * 
	 */
	@Override
	public void synData() throws Exception {
		matchImportData();
	}

	/**
	 * 
	 * 把BaseBean对象中数据生成excel文件存储到本地
	 * 
	 * @param baseBean
	 * @return
	 */
	private boolean saveErrorExcelToLocal(BaseBean baseBean) {

		return matchService.saveErrorExcelToLocal(baseBean);
	}

	/**
	 * 
	 * 初始化界面获取页面所需值
	 * 
	 * @param importType
	 *            前台必须传值：导入方式（excel、xml、hessian,db），默认是excel导入
	 */
	public String initParams(String importType) {
		// 获取外来的待导入数据
		try {
			abstractConfigBean = matchDataFactory.getInitTime(importType);
			return MatchConstant.RESULT_SUCCESS;
		} catch (Exception e) {
			log.error("UserSyncAction类initParams方法：", e);

			// 拼装返回页面
			outJsonString("false");
			return null;
		}
	}

	/**
	 * 
	 * 保存参数
	 * 
	 * @param configBean
	 */
	private String saveParams(AbstractConfigBean configBean) {

		if (configBean == null) {
			return null;
		}
		// 开始时间
		String startTime = configBean.getStartTime();
		// 间隔时间
		String intervalDay = configBean.getIntervalDay();
		// 是否手动或自动标识：0：手动；1：自动
		String iaAuto = configBean.getIaAuto();

		// hh:mm
		if (JecnCommon.isNullOrEmtryTrim(startTime)
				|| !startTime
						.matches("^(0\\d{1}|1\\d{1}|2[0-3]):([0-5]\\d{1})$")) {
			return null;
		}

		if (JecnCommon.isNullOrEmtryTrim(intervalDay)
				|| !intervalDay.matches("^[1-9]\\d*$")) {
			return null;
		}

		if (JecnCommon.isNullOrEmtryTrim(iaAuto) || !iaAuto.matches("^[01]$")) {
			return null;
		}

		// 保存到配置文件
		boolean ret = ReadUserConfigXml.writerConfigXml(configBean);
		if (!ret) {
			return null;
		}
		return MatchConstant.RESULT_SUCCESS;
	}

	/**
	 * 
	 * 
	 * 删除给定路径的文件
	 * 
	 * @param filePath
	 */
	private void deleteErrorLocalExcel(String filePath) {
		matchService.deleteErrorLocalExcel(filePath);
	}

	public String getStringResult() {
		return stringResult;
	}

	public void setStringResult(String stringResult) {
		this.stringResult = stringResult;
	}

	public String getImportType() {
		return importType;
	}

	public void setImportType(String importType) {
		this.importType = importType;
	}

	public UserAutoBuss getUserAutoBuss() {
		return userAutoBuss;
	}

	public void setUserAutoBuss(UserAutoBuss userAutoBuss) {
		this.userAutoBuss = userAutoBuss;
	}

	public IUserMatchService getMatchService() {
		return matchService;
	}

	public void setMatchService(IUserMatchService matchService) {
		this.matchService = matchService;
	}

	@Override
	public AbstractConfigBean getAbstractConfigBean() {
		return abstractConfigBean;
	}

	public void setAbstractConfigBean(AbstractConfigBean abstractConfigBean) {
		this.abstractConfigBean = abstractConfigBean;
	}

	public IMatchDataFactory getMatchDataFactory() {
		return matchDataFactory;
	}

	public void setMatchDataFactory(IMatchDataFactory matchDataFactory) {
		this.matchDataFactory = matchDataFactory;
	}
}
