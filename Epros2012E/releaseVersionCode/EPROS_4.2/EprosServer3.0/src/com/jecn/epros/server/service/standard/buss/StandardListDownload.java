package com.jecn.epros.server.service.standard.buss;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.excel.ExcelFont;
import com.jecn.epros.server.download.excel.SetExcelStyle;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.standard.StandardInfoListBean;
import com.jecn.epros.server.webBean.standard.StandardRelatePRFBean;
import com.jecn.epros.server.webBean.standard.StandardWebInfoBean;

/**
 * 标准清单下载
 * 
 * @author ZHAGNXIAOHU 2013-12-5
 * 
 * 修改人：chehuanbo
 * 修改内容：修改标准清单显示样式，以及excel下载后的样式
 * @since V3.06
 */
public class StandardListDownload {
	private static final Logger log = Logger.getLogger(StandardListDownload.class);
	/**
	 * 创建岗位说明书
	 * 
	 * @param standardWebInfoBean
	 *            StandardWebInfoBean 标准数据
	 * @throws Exception
	 * 
	 */
	public static byte[] createStandListDesc(
			StandardWebInfoBean standardWebInfoBean) throws Exception {
		if (standardWebInfoBean == null
				|| standardWebInfoBean.getListInfoBean() == null) {
			throw new NullPointerException();
		}
		WritableWorkbook wbookData = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			File file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// 组织清单
			WritableSheet wsheet = wbookData.createSheet(JecnUtil
					.getValue("standardList"), 0);
			
			//设置标题字体样式
			WritableFont fontTitle1 = new WritableFont(WritableFont.TIMES, 11,
					WritableFont.BOLD); //字体加粗
			WritableCellFormat wformatTitle1=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontTitle1,Colour.YELLOW));
			//设置字体标题背景特殊样式
			WritableCellFormat wformatTitle2=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontTitle1,Colour.LIGHT_ORANGE));
			//设置字体样式（蓝色）
			WritableFont fontStyle3 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLUE_GREY);
			WritableCellFormat wformatContecnt3=SetExcelStyle.setExcelCellStyle(new ExcelFont(Colour.WHITE,VerticalAlignment.CENTRE,Alignment.LEFT, fontStyle3));
			wformatContecnt3.setWrap(true);
			//设置字体样式（普通）
			WritableFont fontStyle4 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
			WritableCellFormat wformatContecnt4=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontStyle4,Colour.WHITE));
			//设置字体样式（普通）
			WritableFont fontStyle5 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
			//设置单元格内容居左
			WritableCellFormat wformatContecnt5=SetExcelStyle.setExcelCellStyle(new ExcelFont(Colour.WHITE,VerticalAlignment.CENTRE,Alignment.LEFT, fontStyle5));
			wformatContecnt5.setWrap(true);
			List<WritableCellFormat> cellFormats=new ArrayList<WritableCellFormat>();
			cellFormats.add(wformatContecnt4);
			cellFormats.add(wformatContecnt5);
			cellFormats.add(wformatContecnt3);
			
			// 获取总的级别个数
			int levelTotal = standardWebInfoBean.getMaxLevel();
			int count = 0;
			// 行号C
			wsheet.addCell(new Label(count, 1, "", wformatTitle1));
			wsheet.setColumnView(0,5);
			//行高22
			wsheet.setRowView(1,440);
			for (int i = 0; i < levelTotal; i++) {// 创建级别标题
				if (i >= standardWebInfoBean.getCurLevel()) {
					// 级
					wsheet.addCell(new Label(count+1, 1, i
							+ JecnUtil.getValue("level"), wformatTitle1));
					wsheet.setColumnView(count+1, 20);
					count++;
				}
			}
			// 标准名称
			wsheet.addCell(new Label(count+1, 1, JecnUtil
					.getValue("standardName"), wformatTitle2));
			// 条款对应要求
			wsheet.addCell(new Label(count + 2, 1, JecnUtil
					.getValue("clauseToRequest"), wformatTitle2));
			// 相关文件
			wsheet.addCell(new Label(count + 3, 1, JecnUtil
					.getValue("activeRelateFile"), wformatTitle1));

			// 设置列宽
			wsheet.setColumnView(count+1, 30);
			wsheet.setColumnView(count + 2, 30);
			wsheet.setColumnView(count + 3, 30);

			WritableFont fontContent = new WritableFont(WritableFont.TIMES, 12);
			WritableCellFormat wformatContent = new WritableCellFormat(
					fontContent);
			// 设置自动换行
			wformatContent.setWrap(true);
			// 表标题样式
			wformatContent.setBorder(Border.ALL, BorderLineStyle.THIN);
			
			// 添加数据
			addSheetData(standardWebInfoBean, wsheet, cellFormats);
			// 写入Exel工作表
			wbookData.write();
			// 关闭Excel工作薄对象
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			// 把一个文件转化为字节
			byte[] tempDate = JecnUtil.getByte(file);
			if (file.exists()) {
				file.delete();
			}
			return tempDate;
		} catch (Exception e) {
			log.error("", e);
		}finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("关闭WritableWorkbook异常", e);
				}
			}
		}
		
		return null;
	}
    
	
	
	
	/**
	 * 添加数据
	 * 
	 * @param processSystemListBean
	 * @param wsheet
	 * @param wformatTitle
	 * @throws RowsExceededException
	 * @throws WriteException
	 * 修改人：chehuanbo 
	 * 描述：修改了清单显示样式
	 * @since V3.06 
	 */
	private static void addSheetData(StandardWebInfoBean standardWebInfoBean,
			WritableSheet wsheet, List<WritableCellFormat> cellFormats)
			throws RowsExceededException, WriteException {
		
		int count = 2;  //第几行
		int rowNum = 1; // 行号的初始值
		int depth = standardWebInfoBean.getCurLevel();
		int totalLevel = standardWebInfoBean.getMaxLevel();
		for (int i = 0; i < standardWebInfoBean.getListInfoBean().size(); i++) {
			// 添加行号
			wsheet.addCell(new Label(0, count, String.valueOf(rowNum),
					cellFormats.get(0)));
			// 获取标准条款或者条款要求
			StandardInfoListBean standardInfoListBean = standardWebInfoBean
					.getListInfoBean().get(i);
			// 当前标准的所有父级目录集合
			List<StandarDirBean> standarDirList = new ArrayList<StandarDirBean>();
			// 如果是目录不执行查找
			if (standardInfoListBean.getStandType() == 0) {
				continue;
			}
			// 递归查找标准条款或者条款要求的父目录
			findStandarDir(standardInfoListBean, standardWebInfoBean,
					standarDirList);
			// 活动当前标准条款或者条款要求的父级目录数
			int f = standarDirList.size() - 1;
			// 首先添加标准条款或者条款要求的父级目录到excel中
			for (int j = 1; j <= standardWebInfoBean.getMaxLevel(); j++) {

				if (j <= standarDirList.size()) {
					StandarDirBean dirBean = standarDirList.get(f);
					// 添加级别名称（从第二列开始，第一列用来放行号）
					wsheet.addCell(new Label(j, count,
							dirBean.getStandarName(), cellFormats.get(0)));
				} else {
					// 添加级别名称
					wsheet.addCell(new Label(j, count, "", cellFormats.get(0)));
				}

				if (f >= 0) {
					f--;
				}

			}
			if (standardInfoListBean.getStandType() == 5) { // 条款要求
				// 查找条款要求所属的条款
				for (StandardInfoListBean standardInfoListBean2 : standardWebInfoBean
						.getListInfoBean()) {
					if (standardInfoListBean.getPreStandId().longValue() == standardInfoListBean2
							.getStandardId()) {
						// 标准名称
						String standName = standardInfoListBean2.getStandName();
						wsheet.addCell(new Label(totalLevel - depth + 1, count,
								standName, cellFormats.get(2)));
					}
				}
				// =======end
				// 条款对应要求
				wsheet.addCell(new Label(totalLevel - depth + 2, count,
						standardInfoListBean.getStandContent(), cellFormats
								.get(2)));
			} else {
				// 标准名称
				String standName = standardInfoListBean.getStandName();
				wsheet.addCell(new Label(totalLevel - depth + 1, count,
						standName, cellFormats.get(2)));
				// 条款对应要求
				wsheet.addCell(new Label(totalLevel - depth + 2, count, "",
						cellFormats.get(2)));
			}
			// 相关文件信息
			wsheet.addCell(new Label(totalLevel - depth + 3, count,
					getRefFile(standardInfoListBean), cellFormats.get(1)));
			count++;
			rowNum++;
		}
		//总计
		wsheet.addCell(new Label(1, 0, JecnUtil.getValue("totalNum")+ String.valueOf(rowNum-1),
				cellFormats.get(0)));
		wsheet.setRowView(0, 360);
		
	}
	
	/**
	 * 
	 * 递归查找当前标或者标准条款的所有父目录
	 * @author chehuanbo
	 * @param  standardInfoListBean 标准基本信息bean 
	 *         StandardWebInfoBean 标准清单封装bean
	 *         standarDirList 父级目录集合
	 * @since V3.06
	 */
	private static void findStandarDir(StandardInfoListBean standardInfoListBean,StandardWebInfoBean standardWebInfoBean,List<StandarDirBean> standarDirList){
		try {
			//查找当前标准的父目录
			StandardInfoListBean standardInfoListBean1=standardInfoListBean;
			boolean isExit=false;//标记是否存在父级目录 true:存在 false:不存在
			for (StandardInfoListBean standardWebInfoBean2: standardWebInfoBean.getListInfoBean()) {
				if(standardInfoListBean.getPreStandId().longValue()== standardWebInfoBean2.getStandardId()){
				    if(standardWebInfoBean2.getStandType()!=4){
				    	//条款父级目录封装类
				    	StandarDirBean standarDirBean=new StandarDirBean();
				    	//目录级别
						standarDirBean.setLevel(standardWebInfoBean2.getLevel());
						//目录名称
						standarDirBean.setStandarName(standardWebInfoBean2.getStandName());
						
						standarDirList.add(standarDirBean);
				    }
				    //暂时赋值，方便下一次查找
					standardInfoListBean1=standardWebInfoBean2;
					isExit=true;  
					break;
				}
			}
			
			 //判断是否存在父级目录 如果存在执行递归
             //standardInfoListBean1.getLevel()!=standardWebInfoBean.getCurLevel()&&
			if(isExit){
				findStandarDir(standardInfoListBean1,standardWebInfoBean,standarDirList);
			}
		} catch (Exception e) {
           log.error("递归查找条款或者条款要求的父级目录时出现异常 StandardListDownLoad findStandarDir()中", e);
		}
	}
	

	/**
	 * 获取相关文件信息
	 * 
	 * @param standardInfoListBean
	 * @return
	 */
	private static String getRefFile(StandardInfoListBean standardInfoListBean) {
		StringBuffer strBuffer = new StringBuffer();
		for (int j = 0; j < standardInfoListBean.getListStandardRefBean()
				.size(); j++) {// 标准相关文件
			StandardRelatePRFBean relatePRFBean = standardInfoListBean
					.getListStandardRefBean().get(j);
			if (relatePRFBean.getRefType() == 1) {
				strBuffer.append(relatePRFBean.getRefName()).append(
						"[" + JecnUtil.getValue("flow") + "]");
			} else if (relatePRFBean.getRefType() == 2
					|| relatePRFBean.getRefType() == 4) {
				strBuffer.append(relatePRFBean.getRefName()).append(
						"[" + JecnUtil.getValue("rule") + "]");
			} else if (relatePRFBean.getRefType() == 3) {
				strBuffer.append(relatePRFBean.getRefName()).append(" ");
				strBuffer.append(relatePRFBean.getActiveName()).append(
						"[" + JecnUtil.getValue("activities") + "]");
			}
			strBuffer.append("\n");
		}
		return strBuffer.toString();
	}
}

/***
 * 
 * 标准目录封装类
 * 为了方便将标准目录添加到集合中，由此产生此类。此类的作用仅仅是用来生成标准目录
 * 
 * @author chehuanbo
 * @since V3.06
 */
class StandarDirBean{
	
	private int level;  //目录级别
	private String standarName=null; //目录名称
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getStandarName() {
		return standarName;
	}
	public void setStandarName(String standarName) {
		this.standarName = standarName;
	}
}