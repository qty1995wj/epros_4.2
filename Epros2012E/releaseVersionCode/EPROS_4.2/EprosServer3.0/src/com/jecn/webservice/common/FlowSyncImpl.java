/**
 * 
 */
package com.jecn.webservice.common;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.jecn.webservice.puyuan.service.IBpsProSyncService;
import com.jecn.webservice.xinghan.service.IXinghanProSyncService;
import com.jecn.webservice.zhongsh.flowpush.service.IShiHuaProSyncService;

/**
 * 
 * 流程同步调用总类
 * 
 * @author xiaobo
 * 
 */
@Component("FlowSyncImpl")
public class FlowSyncImpl implements IFLowSync {

	IXinghanProSyncService ixinghanProSyncService;
	IBpsProSyncService bpsProSyncService;

	/** 石勘院：盈科i3系统流程文件对接 */
	@Resource
	IShiHuaProSyncService shiHuaSyncService;

	/**
	 * 流程推送总方法
	 * 
	 * @param flowId
	 *            : 流程编号 syNctype：公司类型,path 选择的路径
	 * @return 0:失败 1：成功 2：不存在流程开始元素（普元专用）
	 * 
	 * */
	@Override
	public int sysncMethd(Long flowId, int syNctype, String path) {
		int flag = 0;
		if (0 == syNctype) { // 星汉
			flag = ixinghanProSyncService.SysncFlowToBps(flowId);
		} else if (1 == syNctype) { // 普元
			flag = bpsProSyncService.SysncFlowToBps(flowId, path);
		} else if (2 == syNctype) {// 中石化
			flag = shiHuaSyncService.SysncFlowToBps(flowId);
		}
		return flag;
	}

	public IXinghanProSyncService getIxinghanProSyncService() {
		return ixinghanProSyncService;
	}

	@Resource
	public void setIxinghanProSyncService(IXinghanProSyncService ixinghanProSyncService) {
		this.ixinghanProSyncService = ixinghanProSyncService;
	}

	public IBpsProSyncService getBpsProSyncService() {
		return bpsProSyncService;
	}

	@Resource
	public void setBpsProSyncService(IBpsProSyncService bpsProSyncService) {
		this.bpsProSyncService = bpsProSyncService;
	}

}
