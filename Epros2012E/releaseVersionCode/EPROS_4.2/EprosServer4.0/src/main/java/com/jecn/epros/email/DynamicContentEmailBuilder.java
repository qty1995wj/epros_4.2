package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;

import java.util.ArrayList;
import java.util.List;

public abstract class DynamicContentEmailBuilder extends BaseEmailBuilder {

    @Override
    public List<EmailBasicInfo> buildEmail() {
        List<EmailBasicInfo> emails = new ArrayList<EmailBasicInfo>();
        for (JecnUser user : getUsers()) {
            emails.add(getEmailBasicInfo(user));
        }
        return emails;
    }

    protected abstract EmailBasicInfo getEmailBasicInfo(JecnUser user);
}
