package com.jecn.epros.domain.system;

/**
 * Created by user on 2017/5/24.
 * 下拉框数据项
 */
public class SelectItem {

    private String code;
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
