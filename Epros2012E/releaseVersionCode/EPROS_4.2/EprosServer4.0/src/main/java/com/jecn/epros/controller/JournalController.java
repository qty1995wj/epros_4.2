package com.jecn.epros.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.jecn.epros.domain.journal.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jecn.epros.domain.Paging;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.journal.JournalService;
import com.jecn.epros.util.IDUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/journal", description = "日志", position = 10)
public class JournalController {

    @Autowired
    private JournalService journalService;

    @ApiOperation(value = "系统管理-用户访问日志")
    @RequestMapping(value = "/journals/user/visits", method = RequestMethod.GET)
    public ResponseEntity<LogUserVisitsView> searchFile(@ModelAttribute SearchUserVisits searchLogDetailBean,
                                                        @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("searchLogDetailBean", searchLogDetailBean);
        map.put("paging", paging);
        LogUserVisitsView logDetailViewBean = new LogUserVisitsView();
        logDetailViewBean.setTotal(journalService.getSearchLogDetailResultBeanTotal(map));
        logDetailViewBean.setData(journalService.getSearchLogDetailResultBeans(map));
        return ResponseEntity.ok(logDetailViewBean);
    }

    @ApiOperation(value = "系统管理-资源访问日志(点击详情跳转至日志管理)")
    @RequestMapping(value = "/journals/resource/visits", method = RequestMethod.GET)
    public ResponseEntity<LogResourceVisitsView> searchFile(@ModelAttribute SearchResourceVisits searchLogStatisticsBean,
                                                            @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("searchLogStatisticsBean", searchLogStatisticsBean);
        map.put("paging", paging);
        LogResourceVisitsView logStatisticsViewBean = new LogResourceVisitsView();
        logStatisticsViewBean.setTotal(journalService.getSearchLogStatisticsResultBeanTotal(map));
        logStatisticsViewBean.setData(journalService.getSearchLogStatisticsResultBeans(map));
        return ResponseEntity.ok(logStatisticsViewBean);
    }

    @ApiOperation(value = "系统管理-用户操作日志")
    @RequestMapping(value = "/journals/user/operates", method = RequestMethod.GET)
    public ResponseEntity<LogUserOperatesView> searchFile(@ModelAttribute SearchUserOperates searchLogDetailBean,
                                                          @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("searchLogDetailBean", searchLogDetailBean);
        map.put("paging", paging);
        LogUserOperatesView logDetailViewBean = new LogUserOperatesView();
        int total = journalService.getSearchLogOperateResultBeanTotal(map);
        logDetailViewBean.setTotal(total);
        if (total > 0) {
            logDetailViewBean.setData(journalService.getSearchLogOperateResultBeans(map));
        }
        return ResponseEntity.ok(logDetailViewBean);
    }

}
