package com.jecn.epros.server.dao.task.impl;

import java.io.InputStream;
import java.sql.Blob;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.sql.BLOB;

import org.hibernate.Session;
import org.hibernate.lob.SerializableBlob;

import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskApprovePeopleConn;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskForRecodeNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.JecnTaskTestRunFile;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.task.IJecnAbstractTaskDao;

/**
 * 任务处理公共方法处理类
 * 
 * @author Administrator
 * @date： 日期：Nov 7, 2012 时间：6:00:09 PM
 */
public class JecnAbstractTaskDaoImpl extends AbsBaseDao<JecnTaskBeanNew, Long> implements IJecnAbstractTaskDao {

	/**
	 * 获得任务的审批记录
	 * 
	 * @param taskId
	 * @return
	 * @throws DaoException
	 */
	public List<JecnTaskForRecodeNew> getJecnTaskForRecodeNewListByTaskId(Long taskId) throws Exception {
		String sql = "from JecnTaskForRecodeNew where taskId =" + taskId + " order by sortId";
		try {
			// 获取当前任务下所有日志记录
			List<JecnTaskForRecodeNew> list = getSession().createQuery(sql).list();
			// 记录所有日志下所有人员的人员ID集合
			Set<Long> setPeopleId = new HashSet<Long>();
			for (JecnTaskForRecodeNew jecnTaskForRecodeNew : list) {
				if (jecnTaskForRecodeNew.getCreatePersonId() != null) {
					setPeopleId.add(jecnTaskForRecodeNew.getCreatePersonId());
				}
				if (jecnTaskForRecodeNew.getFromPeopleId() != null) {
					setPeopleId.add(jecnTaskForRecodeNew.getFromPeopleId());
				}
				if (jecnTaskForRecodeNew.getToPeopleId() != null) {
					setPeopleId.add(jecnTaskForRecodeNew.getToPeopleId());
				}
				if (jecnTaskForRecodeNew.getOpinion() != null && !"".equals(jecnTaskForRecodeNew.getOpinion())) {
					jecnTaskForRecodeNew.setOpinion(jecnTaskForRecodeNew.getOpinion().replaceAll("\n", "<br>"));
				}
			}
			if (setPeopleId.size() > 0) {
				sql = "select peopleId,trueName from JecnUser where peopleId in" + JecnCommonSql.getIdsSet(setPeopleId);
				// Object[] 0:jecnUser主键ID；1：人员真实姓名
				List<Object[]> listObj = getSession().createQuery(sql).list();
				for (JecnTaskForRecodeNew jecnTaskForRecodeNew : list) {
					if (jecnTaskForRecodeNew.getCreatePersonId() != null) {
						jecnTaskForRecodeNew.setCreatePersonTemporaryName(getTrueNameByPeopleId(listObj,
								jecnTaskForRecodeNew.getCreatePersonId()));
					}
					if (jecnTaskForRecodeNew.getFromPeopleId() != null) {
						jecnTaskForRecodeNew.setFromPeopleTemporaryName(getTrueNameByPeopleId(listObj,
								jecnTaskForRecodeNew.getFromPeopleId()));
					}
					if (jecnTaskForRecodeNew.getToPeopleId() != null) {
						jecnTaskForRecodeNew.setToPeopleTemporaryName(getTrueNameByPeopleId(listObj,
								jecnTaskForRecodeNew.getToPeopleId()));
					}
				}
			}
			return list;
		} catch (Exception ex) {
			LOGGER.error("JecnAbstractTaskDao类中getJecnTaskForRecodeNewListByTaskId方法发生异常!" + ex);
		}
		return null;
	}

	/**
	 * 通过人员id获得人员的姓名
	 * 
	 * @param listObj
	 * @param peopleId
	 * @return
	 */
	private String getTrueNameByPeopleId(List<Object[]> listObj, Long peopleId) {
		for (Object[] obj : listObj) {
			// 0:peopleId；1：真实姓名
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			if (peopleId.toString().equals(obj[0].toString())) {
				return obj[1].toString();
			}
		}
		// 人员离职
		return "";
	}

	/**
	 * 获得当前任务的审批记录的条数
	 * 
	 * @param taskId
	 *            当前任务ID
	 * @return int 当前任务的审批记录总数
	 * @throws Exception
	 */
	public int getCurTaskRecordCount(Long taskId) throws Exception {
		try {
			String hql = "select count(id) from JecnTaskForRecodeNew where taskId =?";
			return this.countAllByParamsHql(hql, taskId);
		} catch (Exception ex) {
			LOGGER.error("JecnAbstractTaskDao类中getSortRecord方法出现异常,数据库保存失败!", ex);
		}
		return 0;
	}

	@Override
	public boolean checkFileExists(String fileName, Long fileId) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object[]> getPeopleIdsByUserIds(Set<Long> set) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getUploadFileTaskFileNames(Long fileId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 验证任务当前节点审批人是否存在
	 * 
	 * @param taskId
	 *            任务主键Id
	 * @return
	 * @throws DaoException
	 */
	@Override
	public int findTaskStatePeople(Long taskId) throws Exception {
		String sql = "select count(*) from JECN_TASK_PEOPLE_CONN_NEW p_c,jecn_task_stage jts where "
				+ " p_c.stage_id=jts.id"
				+ "and p_c.APPROVE_PID in (select people_id from jecn_user where islock <> 1) and jts.task_id=?";
		return this.countAllByParamsNativeSql(sql, taskId);
	}

	/**
	 * 获得任务审批操作人
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param state
	 *            任务阶段
	 * @return 当前任务阶段的审批人
	 * @throws Exception
	 */
	@Override
	public List<Long> getJecnTaskApprovePeopleConnListByTaskIdAndState(long stageId) throws Exception {
		String hql = "select p.approvePid from JecnTaskApprovePeopleConn as p,JecnUser as u  "
				+ "where p.approvePid =u.peopleId  and p.stageId=?";
		return this.listHql(hql, stageId);
	}

	/**
	 * 获得任务的记录条数
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return int 任务的记录条数
	 * @throws Exception
	 */
	@Override
	public int getSortRecord(Long taskId) throws Exception {
		String hql = "select count(*) from JecnTaskForRecodeNew where taskId =?";
		return this.countAllByParamsHql(hql, taskId);
	}

	/**
	 * 验证当前登录人是否为任务审批人
	 * 
	 * @param peopleId
	 *            登录人人员主键ID
	 * @param taskId
	 *            任务主键ID
	 * @return int >1 是当前任务审批人；0：不是当前任务审批人
	 * @throws DaoException
	 */
	@Override
	public int getPeopleCountByLoginPeople(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception {
		String sql = "select count(*) from jecn_task_people_new t_p,JECN_USER J_U where t_p.task_id =?  "
				+ "and t_p.task_id in " + "(select t_n.id from jecn_task_bean_new t_n where " + "t_n.TASK_ELSE_STATE=?"
				+ " and t_n.APPROVE_NO_COUNTS=?" + ")" + " AND t_p.approve_pid=J_U.PEOPLE_ID"
				+ " and t_p.approve_pid =?";
		return this.countAllByParamsNativeSql(sql, taskId, taskElseState, approveCounts, peopleId);
	}

	/**
	 * 验证指定人员ID是否存在
	 * 
	 * @param peopleId
	 *            人员主键ID
	 * @return 1：存在，0 ：不存在
	 * @throws Exception
	 */
	@Override
	public int isExitPeople(Long peopleId) throws Exception {
		String sql = "select count(u.people_id) from jecn_user u where u.isLock=0 and u.people_id=?";
		return this.countAllByParamsNativeSql(sql, peopleId);
	}

	/**
	 * 打开试用行报告
	 * 
	 * @param id
	 * @return
	 * @throws DaoException
	 */
	@Override
	public JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception {
		Session s = this.getSession();
		String sql = "from JecnTaskTestRunFile where id=" + id;
		List<JecnTaskTestRunFile> list = s.createQuery(sql).list();
		if (list.size() == 0 || list.get(0) == null) {
			return null;
		}
		JecnTaskTestRunFile jecnTaskTestRunFile = list.get(0);
		SerializableBlob sb = (SerializableBlob) jecnTaskTestRunFile.getFileStream();
		Blob wrapBlob = sb.getWrappedBlob();
		byte[] content = new byte[(int) wrapBlob.length()];

		// TODO 数据库判断
		InputStream in = null;
		try{
		if ("SQLSERVER".equals(JecnCommonSql.DBType.SQLSERVER.toString())) {
			in = wrapBlob.getBinaryStream();
		} else if ("ORACLE".equals(JecnCommonSql.DBType.ORACLE.toString())) {
			BLOB blob = (BLOB) wrapBlob;
			in = blob.getBinaryStream();
		}
		in.read(content);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
			in.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		JecnTaskFileTemporary jecnTaskFileTemporary = new JecnTaskFileTemporary();
		jecnTaskFileTemporary.setFileContent(content);
		jecnTaskFileTemporary.setFileName(jecnTaskTestRunFile.getFileName());
		jecnTaskFileTemporary.setId(id);
		jecnTaskFileTemporary.setTaskId(jecnTaskTestRunFile.getTaskId());
		return jecnTaskFileTemporary;
	}

	/**
	 * 验证指定人员ID是否存在
	 * 
	 * @param peopleId
	 *            人员主键ID
	 * @return int：根据人员ID集合获取存在的人员(JECN_USER)数
	 * @throws Exception
	 */
	@Override
	public int isExitPeoples(Set<Long> peopleIds) throws Exception {
		String sql = "select count(*) count(u.people_id) from jecn_user u inner join Jecn_User_Position_Related j_r on u.people_id = j_r.people_id where u.people_id in "
				+ JecnCommonSql.getIdsSet(peopleIds);
		return Integer.parseInt(this.getSession().createSQLQuery(sql).uniqueResult().toString());
	}

	/**
	 * 获取任务显示项
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return JecnTaskApplicationNew
	 * @throws Exception
	 */
	@Override
	public JecnTaskApplicationNew getJecnTaskApplicationNew(Long taskId) throws Exception {
		String sql = "select * from JECN_TASK_APPLIACTION_NEW where task_Id=" + taskId;
		List<JecnTaskApplicationNew> list = this.getSession().createSQLQuery(sql).addEntity(
				JecnTaskApplicationNew.class).list();
		if (list.size() == 0 || list.get(0) == null) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * 根据任务ID获取任务各阶段审批人信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return 各阶段审批人集合 List<Object[]> 0：任务阶段；1：人员真实姓名2：人员主键ID;3:是否删除1:删除
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getListJecnTaskApprovePeopleConn(Long taskId) throws Exception {
		String sql = "select T_P.stage_Id, u.true_name, u.people_id, u.ISLOCK  from JECN_TASK_PEOPLE_CONN_NEW T_P "
				+ "LEFT JOIN Jecn_User u ON T_P.APPROVE_PID = u.people_id  and Exists  (select p_u.people_id "
				+ "from jecn_user_position_related p_u  where u.people_id = p_u.people_id),JECN_TASK_STAGE J_S where T_P.stage_Id=J_S.id and J_S.task_Id = ?";
		return this.listNativeSql(sql, taskId);
	}

	@Override
	public List<JecnTaskStage> getJecnTaskStageList(Long taskId) {
		String hql = "from JecnTaskStage where taskId=? order by sort";
		return this.listHql(hql, taskId);
	}

	@Override
	public void deleteJecnTaskPeopleConnByTaskId(Long taskId) {
		String hql = "delete from JecnTaskApprovePeopleConn where stageId in (select id from JecnTaskStage where taskId = ?)";
		this.execteHql(hql, taskId);
	}

	@Override
	public void deleteJecnTaskPeopleConnByTaskIdAndStageId(Long stageId) {
		String hql = "delete from JecnTaskApprovePeopleConn where stageId=?";
		this.execteHql(hql, stageId);
	}

	@Override
	public JecnTaskApprovePeopleConn getTaskPeopleConnByStageId(long stageId) {
		String hql = "from JecnTaskApprovePeopleConn where stageId=?";
		return this.getObjectHql(hql, stageId);
	}

	@Override
	public JecnTaskForRecodeNew getJecnTaskForRecodeNew(Long recordId) {
		String hql = "from JecnTaskForRecodeNew where id=?";
		return this.getObjectHql(hql, recordId);
	}

}
