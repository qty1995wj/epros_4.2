package epros.designer.gui.system.config.ui.dialog;

import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.util.JecnProperties;

/**
 * 
 * 文件配置对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFileConfigJDialog extends JecnAbtractBaseConfigDialog {

	public JecnFileConfigJDialog(JecnConfigDesgBean configBean) {
		super(configBean);
		this.setTitle(JecnProperties.getValue("setTitleFileName"));
	}

}
