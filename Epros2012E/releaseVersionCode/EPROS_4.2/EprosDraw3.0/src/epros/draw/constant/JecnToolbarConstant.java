package epros.draw.constant;

/**
 * 
 * 工具栏常量类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolbarConstant {

	/**
	 * 
	 * 工具栏标题个数枚举
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum TooBarTitleNumEnum {
		four, // 4个
		five, // 5个
		six
		// 6个
	}

	public enum ToolBarElemType {
		fileTitle, // 文件标题
		editTitle, // 编辑标题
		formatTitle, // 格式标题
		operTitle, // 流程文件
		eprosMagTitle, // EPROS流程管理平台
		helpTitle, // 帮助标题

		// ///////常规/////////
		oftenSave, // 保存
		oftenSaveAS, // 另存为
		oftenSaveImage, // 导出为图片
		oftenUndo, // 撤销
		oftenRedo, // 恢复
		oftenZoomLabel, // 当前比例
		oftenOriginal, // 原始视图
		oftenZoomBig, // 放大
		oftenZoomSmall, // 缩小
		// ///////常规/////////

		// ///////文件/////////
		fileOpen, // 打开
		visioOpen, // 打开Visio
		fileCreateTotalMap, // 新建流程地图
		fileCreatePartMap, // 新建流程图
		fileViewPrint, // 预览打印
		fileExit, // 退出
		// ///////文件/////////

		// ///////编辑/////////
		editCut, // 剪切
		editCopy, // 复制
		editPaste, // 粘贴
		inputVisio, // 导入visio
		outputVisio, // 导出visio
		editDelete, // 删除
		formatPainter, // 格式刷
		editFill, // 填充
		editLine, // 线条
		editShadow, // 阴影
		edit3D, // 3D效果
		editLineType, // 线条类型
		editLineForm, // 线条粗细
		editUpLayer, // 上移一层
		editDownLayer, // 下移一层
		editFrontLayer, // 置于顶层
		editBackLayer, // 置于底层
		editClockWize, // 顺时针旋转
		editAntiClockWize, // 逆时针旋转
		editAlign, // 排列
		editIconshot, // 截图
		modelLine, // 添加泳道

		// ///////只有图片没有文字说明的 start/////////
		editFontSytle, // 字体样式
		editFontSize, // 字体大小
		editFontSizeBig, // 字体：放大
		editfontSizeSmall, // 字体：缩小
		// 说明：editfontColor包含：editfontOftenColor、editfontSelectColor
		editfontColor, // 字体颜色容器
		editfontOftenColor, // 字体颜色
		editfontSelectColor, // 字体选择颜色
		editfontForm, // 字体粗细
		editfontAglign, // 字体位置
		editFontAlign, // 文件排列方式

		editFillSelectColor, // 填充选择颜色
		editLineSelectColor, // 线条选择颜色
		editShadowSelectColor, // 阴影选择颜色

		// ///////只有图片没有文字说明的 end/////////

		// ///////编辑/////////

		// ///////格式/////////
		formatAttriSet, // 流程元素维护（模板维护）
		formatAntoNum, // 自动编号
		formatRoleMove, // 角色移动
		formatGrid, // 网格
		formatPLine, // 横分割线
		formatVLine, // 竖分割线
		formatAutoWidth, // 右边距
		formatAutoHeigth, // 下边距
		// ///////格式/////////

		// ///////流程文件/////////
		operSetItem, // 配置流程操作说明
		operDownload, // 下载流程操作说明
		operEdit, // 编辑流程操作说明

		// ///////流程文件/////////

		// ///////EPROS流程管理平台/////////
		eprosMangConductFile, // EPROS流程管理平台宣传资料
		// ///////EPROS流程管理平台/////////

		// ///////帮助/////////
		helpAbout, // 关于
		helpUserManual, // 用户手册
		helpFindJecn, // 联系我们
		// ///////帮助/////////

		// ///////其他/////////
		activeDetails, // 活动明细
		roleDetails, // 角色明细
		flowElemAttr, // 元素属性
		flowElemAlign, // 元素自动对齐
		newTemplate, // 新建模板 片段
		allSelected, mapFigureDesc
		// 描述
		// 全选
		// ///////其他/////////
	}

	/**
	 * 
	 * 常规
	 * 
	 */
	public final static ToolBarElemType[] ToolBarOftenype = { ToolBarElemType.oftenSave, // 保存
			ToolBarElemType.oftenSaveAS, // 另存为
			ToolBarElemType.oftenSaveImage, // 导出为图片
			ToolBarElemType.oftenUndo, // 撤销
			ToolBarElemType.oftenRedo, // 恢复
			ToolBarElemType.oftenZoomLabel, // 当前比例
			ToolBarElemType.oftenOriginal, // 原始视图
			ToolBarElemType.oftenZoomBig, // 放大
			ToolBarElemType.oftenZoomSmall // 缩小
	};

	/**
	 * 
	 * 文件
	 * 
	 */
	public final static ToolBarElemType[] ToolBarFlieType = { ToolBarElemType.fileOpen, // 打开
			ToolBarElemType.visioOpen, // visio打开
			ToolBarElemType.fileCreateTotalMap, // 新建流程地图
			ToolBarElemType.fileCreatePartMap, // 新建流程图
			ToolBarElemType.fileViewPrint, // 预览打印
			ToolBarElemType.fileExit // 退出
	};

	/**
	 * 
	 * 编辑
	 * 
	 */
	public final static ToolBarElemType[] ToolBarEditType = { ToolBarElemType.editCut, // 剪切
			ToolBarElemType.editCopy, // 复制
			ToolBarElemType.editPaste, // 粘贴
			ToolBarElemType.editDelete, // 删除
			ToolBarElemType.formatPainter, // 格式刷
			ToolBarElemType.editFill, // 填充
			ToolBarElemType.editLine, // 线条
			ToolBarElemType.editShadow, // 阴影
			ToolBarElemType.edit3D, // 3D效果
			ToolBarElemType.editLineType, // 线条类型
			ToolBarElemType.editLineForm, // 线条粗细
			ToolBarElemType.editUpLayer, // 上移一层
			ToolBarElemType.editDownLayer, // 下移一层
			ToolBarElemType.editFrontLayer, // 置于顶层
			ToolBarElemType.editBackLayer, // 置于底层
			ToolBarElemType.editClockWize, // 顺时针旋转
			ToolBarElemType.editAntiClockWize // 逆时针旋转
	};

	/**
	 * 
	 * 格式
	 * 
	 */
	public final static ToolBarElemType[] ToolBarFormatType = { ToolBarElemType.formatAttriSet, // 模板维护
			ToolBarElemType.formatAntoNum, // 自动编号
			ToolBarElemType.formatRoleMove, // 角色移动
			ToolBarElemType.formatGrid, // 网格
			ToolBarElemType.formatPLine, // 横分割线
			ToolBarElemType.formatVLine // 竖分割线
	};

	/**
	 * 
	 * 选择颜色
	 * 
	 */
	public final static ToolBarElemType[] ToolSelectColorType = { ToolBarElemType.editFillSelectColor, // 填充选择颜色
			ToolBarElemType.editShadowSelectColor, // 阴影选择颜色
			ToolBarElemType.editLineSelectColor, // 线条选择颜色
			ToolBarElemType.editfontSelectColor, // 字体选择颜色
	};
	/**
	 * 
	 * 帮助
	 * 
	 */
	public final static ToolBarElemType[] ToolBarHelpType = { ToolBarElemType.helpAbout, // 关于
			ToolBarElemType.helpUserManual, // 用户手册
			ToolBarElemType.helpFindJecn // 联系我们
	};

}
