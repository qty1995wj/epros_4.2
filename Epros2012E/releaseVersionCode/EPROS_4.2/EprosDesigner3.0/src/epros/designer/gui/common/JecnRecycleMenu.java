package epros.designer.gui.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.recycle.RecycleTree;
import epros.designer.gui.system.ResourceHighEfficiencyTree;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/***
 * 回收站树节点右键菜单
 * 
 * @author Administrator
 * @Date 2015-03-19
 */
public abstract class JecnRecycleMenu extends JPopupMenu {
	/** 选中节点集合 */
	private List<JecnTreeNode> selectedNodes = null;
	/** 要恢复的节点的集合 */
	private List<JecnTreeNode> recycleNodeList = new ArrayList<JecnTreeNode>();
	/** 选中的节点Id集合 */
	private List<Long> recycleNodesIdList = null;
	/** 恢复 (恢复当前节点及子节点) */
	private JMenuItem recycleNodesMenu = new JMenuItem(JecnProperties.getValue("recyleFileNodes"));
	/** 恢复(只恢复当前节点) */
	private JMenuItem recycleSingleNodeMenu = new JMenuItem(JecnProperties.getValue("recyleOnlyNode"));
	/** 删除 */
	private JMenuItem deleteMenu = new JMenuItem(JecnProperties.getValue("delete"));
	/** 数据tree */
	protected RecycleTree jTree;
	/** 要删除的节点Id集合 */
	private List<Long> trueDelNodesIdList = null;

	public JecnRecycleMenu(RecycleTree jTree) {
		this.jTree = jTree;
		initCompotents();
	}

	private void initCompotents() {
		this.add(recycleNodesMenu);
		this.add(recycleSingleNodeMenu);
		this.add(deleteMenu);
		initMenuAction();
	}

	/**
	 * 恢复目录及子文件
	 */
	protected abstract boolean doRecycleFullData();

	/**
	 * 恢复单个目录或文件
	 */
	protected abstract boolean doRecycleData();

	/**
	 * 数据真删
	 */
	protected abstract boolean trueDelete();

	/**
	 * 初始化右键菜单按钮事件
	 */
	private void initMenuAction() {
		// 恢复 (恢复当前节点及子节点)
		recycleNodesMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				recycleCurAndChild();
			}
		});
		// 恢复(只恢复当前节点)
		recycleSingleNodeMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				recycleCurNode();
			}
		});
		// 删除
		deleteMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteMenuPerformed();
			}
		});
	}

	/***
	 * 恢复(恢复当前节点及子节点)
	 */
	public void recycleCurAndChild() {
		if (!recyclePreparedSuccess()) {
			return;
		}
		// 获取已经恢复的节点集合
		initRecycleNodeList();

		// 子类实现恢复数据
		if (!doRecycleFullData()) {
			return;
		}
		// 清空table
		this.jTree.getRecyclePanel().getJecnRecyleTable().refrehTable(new ArrayList<JecnTreeBean>());

		// 将已经恢复的节点恢复为正常图标（修改删除状态）
		recycleState(recycleNodeList);
		// 移除节点
		// removeNodes();
		// 提示节点恢复成功
		recycleSuccess();
	}

	/**
	 * 恢复成功的提示语句
	 */
	private void recycleSuccess() {
		JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("recySuccess"));
	}

	/**
	 * 记录所有子节点至被恢复的文件List中
	 * 
	 * @param node
	 */
	private void recordChildren(JecnTreeNode node) {
		// 添加节点
		recycleNodeList.add(node);
		if (node.getChildCount() > 0) {
			for (Enumeration e = node.children(); e.hasMoreElements();) {
				JecnTreeNode treeNode = (JecnTreeNode) e.nextElement();
				recordChildren(treeNode);
			}
		}
	}

	/**
	 * 恢复完节点后，将节点移除
	 * 
	 * @param jTree
	 * @param list
	 */
	private void removeNodes() {
		for (JecnTreeNode node : recycleNodeList) {
			JecnTreeNode pNode = (JecnTreeNode) node.getParent();
			JecnTreeCommon.removeNode(jTree, pNode, node);
			// 如果删除的不再有子节点，则把数据层改为false;
			if (pNode.getChildCount() == 0) {
				pNode.getJecnTreeBean().setChildNode(false);
			}
		}
	}

	/**
	 * 恢复(只恢复当前节点)
	 */
	public void recycleCurNode() {
		if (!recyclePreparedSuccess()) {
			return;
		}

		// 恢复当前节点
		if (!doRecycleData()) {
			return;
		}
		// 清空table
		this.jTree.getRecyclePanel().getJecnRecyleTable().refrehTable(new ArrayList<JecnTreeBean>());
		// 获取已经恢复的节点集合
		// initRecycleNodeList();
		// 将已经恢复的节点恢复为正常图标
		recycleState(selectedNodes);

		// 删除已恢复的节点
		// removeNodes();
		recycleSuccess();
	}

	/**
	 * 初始化选中的node的id的集合
	 */
	private void initRecycleNodeIdList() {
		recycleNodesIdList = new ArrayList<Long>();
		for (JecnTreeNode node : selectedNodes) {
			recycleNodesIdList.add(node.getJecnTreeBean().getId());

		}
	}

	/**
	 * 将恢复的节点的删除状态改为正常状态
	 * 
	 * @param selectedNodes
	 *            List<JecnTreeNode> 需要恢复的节点
	 */
	private void recycleState(List<JecnTreeNode> selectedNodes) {
		for (JecnTreeNode node : selectedNodes) {
			node.getJecnTreeBean().setIsDelete(0);
		}
		// 刷新选中的节点
		for (JecnTreeNode node : selectedNodes) {
			JecnDesignerMainPanel designerMainPanel = JecnDesignerMainPanel.getDesignerMainPanel(); // 恢复资源管理树上的节点
			ResourceHighEfficiencyTree tree = designerMainPanel.getTree();
			JecnTreeNode jecnTreeNode = node;
			try {
				JecnTreeCommon.searchNode(jecnTreeNode.getJecnTreeBean().getId(), jecnTreeNode.getJecnTreeBean()
						.getTreeNodeType(), tree);
			} catch (Exception e) {
				e.printStackTrace();
			}
			((JecnTreeModel) jTree.getModel()).reload(node);
		}

	}

	/**
	 * 获取已经恢复的节点集合
	 */
	private void initRecycleNodeList() {

		for (JecnTreeNode treeNode : selectedNodes) {
			recordChildren(treeNode);
		}

	}

	/**
	 * 恢复数据前的准备工作
	 * 
	 * @return boolean true确定 false取消
	 */
	private boolean recyclePreparedSuccess() {
		// 选中节点不为空
		if (selectedNodes == null || selectedNodes.size() == 0) {
			return false;
		}
		// 是否恢复
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("ynRestoration"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return false;
		}
		// 获取选中的节点的id的集合
		initRecycleNodeIdList();
		// 有数据的情况下
		return true;
	}

	/**
	 * 真删数据前提示信息，以及数据准备
	 * 
	 *@return boolean true确定 false取消
	 */
	private boolean trueDelPreparedSuccess() {
		// 选中节点不为空
		if (selectedNodes == null) {
			return false;
		}
		// 是否恢复
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("delNoRecovery"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return false;
		}
		trueDelNodesIdList = new ArrayList<Long>();
		for (JecnTreeNode jecnTreeNode : selectedNodes) {
			trueDelNodesIdList.add(jecnTreeNode.getJecnTreeBean().getId());
		}

		// 获取选中的节点的id的集合
		initRecycleNodeIdList();
		// 有数据的情况下
		return true;
	}

	/***
	 * 删除
	 */
	public void deleteMenuPerformed() {
		if (!trueDelPreparedSuccess()) {
			return;
		}

		// 之类实现与数据库交互
		if (!trueDelete()) {
			return;
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jTree, selectedNodes);
		// 清空table
		this.jTree.getRecyclePanel().getJecnRecyleTable().refrehTable(new ArrayList<JecnTreeBean>());
		JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("deleteSuccess"));
	}

	/**
	 * 判断父节点有没有删除状态
	 * 
	 * @param node
	 * @return
	 */
	private boolean isPubNodeExistDelete(JecnTreeNode node) {
		JecnTreeNode pNode = (JecnTreeNode) node.getParent();
		if (pNode != null && pNode.getJecnTreeBean() != null) {
			if (pNode.getJecnTreeBean().getIsDelete() == 1) {
				return true;
			}
			return isPubNodeExistDelete(pNode);
		} else {
			return false;
		}
	}

	/**
	 * 根据选中的节点类型显示对应的菜单项
	 */
	public void displayMenus() {
		// 是否显示恢复 (恢复当前节点及子节点)
		boolean isRecycleNodesMenu = true;
		// 是否恢复当前节点
		boolean isRecycleSingleNodeMenu = true;
		// 是否能删除
		boolean isDeleteMenu = true;
		setMenuVisible();
		// 查看选中的文件的状态（有删除状态文件的，则
		for (JecnTreeNode node : selectedNodes) {
			// 父节点为删除状态时，子节点不允许恢复
			if (isPubNodeExistDelete(node)) {
				isRecycleNodesMenu = false;
				isRecycleSingleNodeMenu = false;
			}
			// 本节点没有删除状态
			if (node.getJecnTreeBean().getIsDelete() == 0) {
				isRecycleNodesMenu = false;
				isRecycleSingleNodeMenu = false;
				isDeleteMenu = false;
			}
			// 文件节点没有多选恢复
			if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.file) {
				isRecycleNodesMenu = false;
			}
			// 节点是管理员赋予角色权限 并且 发布的 不能删除
			// 有节点的 操作权限 并且 不是管理员  节点已经发布的情况下不允许删除
			if ((JecnTreeCommon.isAuthNode(node) == 2 &&!JecnConstants.loginBean.isAdmin()) && node.getJecnTreeBean().isPub()) {
				isDeleteMenu = false;
			}
		}

		if (isRecycleNodesMenu) {
			this.recycleNodesMenu.setVisible(true);
		}

		if (isRecycleSingleNodeMenu) {
			this.recycleSingleNodeMenu.setVisible(true);
		}

		if (isDeleteMenu) {
			this.deleteMenu.setVisible(true);
		}

	}

	/**
	 * 设置Menu的显示状态
	 */
	private void setMenuVisible() {
		recycleNodesMenu.setVisible(false);
		recycleSingleNodeMenu.setVisible(false);
		deleteMenu.setVisible(false);
	}

	public List<JecnTreeNode> getSelectedNodes() {
		return selectedNodes;
	}

	public void setSelectedNodes(List<JecnTreeNode> selectedNodes) {
		this.selectedNodes = selectedNodes;
	}

	public List<JecnTreeNode> getListRecycleNode() {
		return recycleNodeList;
	}

	public void setListRecycleNode(List<JecnTreeNode> listRecycleNode) {
		this.recycleNodeList = listRecycleNode;
	}

	public List<Long> getRecycleNodesIdList() {
		return recycleNodesIdList;
	}

	public void setRecycleNodesIdList(List<Long> recycleNodesIdList) {
		this.recycleNodesIdList = recycleNodesIdList;
	}

	public void setTrueDelNodesIdList(List<Long> trueDelNodesIdList) {
		this.trueDelNodesIdList = trueDelNodesIdList;
	}

}
