package com.jecn.epros.server.bean.download;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.lowagie.text.Image;

public class ProcessMapDownloadBean implements Serializable {
	/** 流程文件配置文件*/
	private List<JecnConfigItemBean> jecnConfigItemBean = new ArrayList<JecnConfigItemBean>();
	
	/** 流程名称 */
	private String flowName;
	/** 流程地图ID*/
	private Long flowId;
	/** 流程密集 */
	private String flowIsPublic;
	/** 流程编号 */
	private String flowInputNum;
	/** 版本 */
	private String flowVersion;
	/** 公司名称*/
	private String companyName;

	/** 目的 */
	private String flowAim;
	/** 范围 */
	private String flowArea;
	/** 术语定义 */
	private String flowNounDefine;
	/** 输入 */
	private String flowInput;
	/** 输出 */
	private String flowOutput;
	/** 流程地图图片 */
	private Image img;
	/** 步骤 */
	private String flowShowStep;
	/** 附件（不存放在数据库中） */
	private String fileName;
	/** 文件ID*/
	private Long fileId;
	/** 评审人集合 0 评审人类型 1名称 2日期 */
	private List<Object[]> peopleList = new ArrayList<Object[]>();

	
	public List<JecnConfigItemBean> getJecnConfigItemBean() {
		return jecnConfigItemBean;
	}

	public void setJecnConfigItemBean(List<JecnConfigItemBean> jecnConfigItemBean) {
		this.jecnConfigItemBean = jecnConfigItemBean;
	}

	public List<Object[]> getPeopleList() {
		return peopleList;
	}

	public void setPeopleList(List<Object[]> peopleList) {
		this.peopleList = peopleList;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowIsPublic() {
		return flowIsPublic;
	}

	public void setFlowIsPublic(String flowIsPublic) {
		this.flowIsPublic = flowIsPublic;
	}

	public String getFlowInputNum() {
		return flowInputNum;
	}

	public void setFlowInputNum(String flowInputNum) {
		this.flowInputNum = flowInputNum;
	}

	public String getFlowVersion() {
		return flowVersion;
	}

	public void setFlowVersion(String flowVersion) {
		this.flowVersion = flowVersion;
	}

	public String getFlowAim() {
		return flowAim;
	}

	public void setFlowAim(String flowAim) {
		this.flowAim = flowAim;
	}

	public String getFlowArea() {
		return flowArea;
	}

	public void setFlowArea(String flowArea) {
		this.flowArea = flowArea;
	}

	public String getFlowInput() {
		return flowInput;
	}

	public void setFlowInput(String flowInput) {
		this.flowInput = flowInput;
	}

	public String getFlowOutput() {
		return flowOutput;
	}

	public void setFlowOutput(String flowOutput) {
		this.flowOutput = flowOutput;
	}

	public String getFlowShowStep() {
		return flowShowStep;
	}

	public void setFlowShowStep(String flowShowStep) {
		this.flowShowStep = flowShowStep;
	}

	public String getFlowNounDefine() {
		return flowNounDefine;
	}

	public void setFlowNounDefine(String flowNounDefine) {
		this.flowNounDefine = flowNounDefine;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
}
