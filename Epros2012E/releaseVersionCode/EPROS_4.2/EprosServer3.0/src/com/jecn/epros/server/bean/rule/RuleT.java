package com.jecn.epros.server.bean.rule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

/**
 * 制度表T
 * 
 * @author lihongliang
 * 
 */
public class RuleT implements java.io.Serializable {

	private Long id;
	private Long projectId;// 工程Id
	private Long perId;// 父节点
	private String ruleNumber;// 制度编号
	private String ruleName;// 制度名称
	private Long isPublic;// 密级(0秘密,1公开)
	private Long typeId;// 制度类别：制度文件的类别划分，类别可增删改查对应JecnFlowType主健
	private String typeName;// 制度类别名称
	private Integer isDir;// (0是目录，1是制度,2是制度文件)
	private Date createDate;// 创建时间
	private Date updateDate;// 更新时间
	private Long createPeopleId;// 创建人ID
	private String createPeopleName;// JecnUserInfoDao中findByIdJecnUser
	private Long updatePeopleId;// 更新人ID
	private String updatePeopleName;// JecnUserInfoDao中findByIdJecnUser
	private Long templateId;// 模板Id
	private Long orgId;// 责任部门
	private String orgName;// 责任名称
	private Integer sortId;// 排序
	/** isFileLocal=0时，为文件库文件ID，isFileLocal=1时为本地上传表主键ID */
	private Long fileId;// 制度文件Id
	private boolean isPub = false;
	/** 数据同步 */
	private Long beforeId;
	private Long beforePId;
	/** 运行状态类型 */
	private Long testRunNumber;
	private Long historyId;// 文控ID
	private Long attchMentId;// 责任人ID
	private String attchMentName;// 责任人名称
	private Integer typeResPeople; // 责任人类型
	/** 文件bytes 不存在数据库中，只是做文件内容传递 */
	private byte[] fileBytes;
	private Integer isFileLocal;// g020 0是从文件库关联的1是从本地上传的
	private String tPath;// tpath 节点层级关系
	private int tLevel;// level 节点级别，默认0开始
	private Date pubTime;
	private String viewSort;
	/** 保密级别 **/
	private Integer confidentialityLevel;
	private Long expiry;// 有效期
	private Long guardianId;// 监护人ID
	private String guardianName;
	private String businessScope;// 业务范围
	private String otherScope;// 其他范围
	private Integer delState = 0;// 删除状态：0：未删除，1：已删除
	/** 支持文件关联的文件ID */
	private Long relatedFileId;

	/** 制度文件内容 存放数据库 */
	private byte[] fileStream;
	/** 制度文件名称 */
	private String fileName;
	JecnRuleBaiscInfoT jecnRuleBaiscInfoT;// 前言

	/** 关键字 **/
	private String keyword;
	/** 专员 **/
	/** 制度链接 **/
	private String ruleUrl;
	private Long commissionerId;

	private String commissionerName;

	private Date effectiveTime;
	// 拟制人 ID
	private Long fictionPeopleId;
	private String fictionPeopleName;

	private List<JecnTreeBean> orgScopes = new ArrayList<JecnTreeBean>();// 组织范围

	private Object syncExtra = null;

	public Object getSyncExtra() {
		return syncExtra;
	}

	public void setSyncExtra(Object syncExtra) {
		this.syncExtra = syncExtra;
	}

	public String getRuleUrl() {
		return ruleUrl;
	}

	public Date getEffectiveTime() {
		return effectiveTime;
	}

	public void setEffectiveTime(Date effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	public void setRuleUrl(String ruleUrl) {
		this.ruleUrl = ruleUrl;
	}

	public byte[] getFileStream() {
		return fileStream;
	}

	public void setFileStream(byte[] bytes) {
		this.fileStream = bytes;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBusinessScope() {
		return businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	public String getOtherScope() {
		return otherScope;
	}

	public void setOtherScope(String otherScope) {
		this.otherScope = otherScope;
	}

	public JecnRuleBaiscInfoT getJecnRuleBaiscInfoT() {
		return jecnRuleBaiscInfoT;
	}

	public void setJecnRuleBaiscInfoT(JecnRuleBaiscInfoT jecnRuleBaiscInfoT) {
		this.jecnRuleBaiscInfoT = jecnRuleBaiscInfoT;
	}

	public List<JecnTreeBean> getOrgScopes() {
		return orgScopes;
	}

	public void setOrgScopes(List<JecnTreeBean> orgScopes) {
		this.orgScopes = orgScopes;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public Long getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}

	public Long getExpiry() {
		return expiry;
	}

	public void setExpiry(Long expiry) {
		this.expiry = expiry;
	}

	public Integer getConfidentialityLevel() {
		return confidentialityLevel;
	}

	public void setConfidentialityLevel(Integer confidentialityLevel) {
		this.confidentialityLevel = confidentialityLevel;
	}

	public String getViewSort() {
		return viewSort;
	}

	public void setViewSort(String viewSort) {
		this.viewSort = viewSort;
	}

	public Date getPubTime() {
		return pubTime;
	}

	public void setPubTime(Date pubTime) {
		this.pubTime = pubTime;
	}

	public String gettPath() {
		return tPath;
	}

	public void settPath(String tPath) {
		this.tPath = tPath;
	}

	public int gettLevel() {
		return tLevel;
	}

	public void settLevel(int tLevel) {
		this.tLevel = tLevel;
	}

	public Integer getIsFileLocal() {
		return isFileLocal;
	}

	public void setIsFileLocal(Integer isFileLocal) {
		this.isFileLocal = isFileLocal;
	}

	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}

	public Long getAttchMentId() {
		return attchMentId;
	}

	public void setAttchMentId(Long attchMentId) {
		this.attchMentId = attchMentId;
	}

	public String getAttchMentName() {
		return attchMentName;
	}

	public void setAttchMentName(String attchMentName) {
		this.attchMentName = attchMentName;
	}

	public void setPub(boolean isPub) {
		this.isPub = isPub;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getRuleNumber() {
		return ruleNumber;
	}

	public void setRuleNumber(String ruleNumber) {
		this.ruleNumber = ruleNumber;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public Long getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Long isPublic) {
		this.isPublic = isPublic;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getIsDir() {
		return isDir;
	}

	public void setIsDir(Integer isDir) {
		this.isDir = isDir;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public String getCreatePeopleName() {
		return createPeopleName;
	}

	public void setCreatePeopleName(String createPeopleName) {
		this.createPeopleName = createPeopleName;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public String getUpdatePeopleName() {
		return updatePeopleName;
	}

	public void setUpdatePeopleName(String updatePeopleName) {
		this.updatePeopleName = updatePeopleName;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the sortId
	 */
	public Integer getSortId() {
		return sortId;
	}

	/**
	 * @param sortId
	 *            the sortId to set
	 */
	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}

	/**
	 * @return the fileId
	 */
	public Long getFileId() {
		return fileId;
	}

	/**
	 * @param fileId
	 *            the fileId to set
	 */
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	/**
	 * @return the orgId
	 */
	public Long getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId
	 *            the orgId to set
	 */
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName
	 *            the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getPerId() {
		return perId;
	}

	public void setPerId(Long perId) {
		this.perId = perId;
	}

	/**
	 * @return the isPub
	 */
	public boolean isPub() {
		return isPub;
	}

	/**
	 * @param isPub
	 *            the isPub to set
	 */
	public void setIsPub(boolean isPub) {
		this.isPub = isPub;
	}

	public Long getBeforeId() {
		return beforeId;
	}

	public void setBeforeId(Long beforeId) {
		this.beforeId = beforeId;
	}

	public Long getBeforePId() {
		return beforePId;
	}

	public void setBeforePId(Long beforePId) {
		this.beforePId = beforePId;
	}

	public Long getTestRunNumber() {
		return testRunNumber;
	}

	public void setTestRunNumber(Long testRunNumber) {
		this.testRunNumber = testRunNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Long historyId) {
		this.historyId = historyId;
	}

	public Integer getTypeResPeople() {
		return typeResPeople;
	}

	public void setTypeResPeople(Integer typeResPeople) {
		this.typeResPeople = typeResPeople;
	}

	public Integer getDelState() {
		return delState;
	}

	public void setDelState(Integer delState) {
		this.delState = delState;
	}

	public Long getRelatedFileId() {
		return relatedFileId;
	}

	public void setRelatedFileId(Long relatedFileId) {
		this.relatedFileId = relatedFileId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long getCommissionerId() {
		return commissionerId;
	}

	public void setCommissionerId(Long commissionerId) {
		this.commissionerId = commissionerId;
	}

	public String getCommissionerName() {
		return commissionerName;
	}

	public void setCommissionerName(String commissionerName) {
		this.commissionerName = commissionerName;
	}

	public Long getFictionPeopleId() {
		return fictionPeopleId;
	}

	public void setFictionPeopleId(Long fictionPeopleId) {
		this.fictionPeopleId = fictionPeopleId;
	}

	public String getFictionPeopleName() {
		return fictionPeopleName;
	}

	public void setFictionPeopleName(String fictionPeopleName) {
		this.fictionPeopleName = fictionPeopleName;
	}

}
