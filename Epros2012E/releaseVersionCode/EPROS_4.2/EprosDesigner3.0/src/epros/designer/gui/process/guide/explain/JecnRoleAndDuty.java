package epros.designer.gui.process.guide.explain;

import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.swing.JecnPanel;

/**
 * 角色/职责
 * 
 * @author user
 * 
 */
public class JecnRoleAndDuty extends JecnFileDescriptionTable {

	ProcessOperationDialog processOperationDialog;

	public JecnRoleAndDuty(int index, String name, boolean isRequest, JecnPanel contentPanel,
			ProcessOperationDialog processOperationDialog, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.processOperationDialog = processOperationDialog;
		initTable();
		table.setTableColumn(1, 120);
	}

	@Override
	protected void dbClickMethod() {
		int selectRow = table.getSelectedRow();
		List<JecnBaseRoleFigure> listRoleData = processOperationDialog.getFlowOperationBean().getBaseRoleFigureList();
		JecnRoleAndDutyDialog roleAndDutyDialog = new JecnRoleAndDutyDialog(listRoleData.get(selectRow),
				processOperationDialog);
		roleAndDutyDialog.setVisible(true);
		table.setRowSelectionInterval(selectRow, selectRow);
		table.updateUI();
	}

	public JecnTableModel getTableModel() {
		boolean showPosNameBox = JecnConfigTool.isShowPosNameInRole();
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("roleName"));
		// 岗位名称
		if (showPosNameBox) {
			title.add(JecnProperties.getValue("positionName"));
		}
		title.add(JecnProperties.getValue("responsibilities"));

		// 获取操作说明数据
		Vector<Vector<String>> rowData = new Vector<Vector<String>>();
		List<JecnBaseRoleFigure> listRoleData = processOperationDialog.getFlowOperationBean().getBaseRoleFigureList();
		if (listRoleData != null && listRoleData.size() > 0) {
			for (JecnBaseRoleFigure baseRoleFigure : listRoleData) {
				Vector<String> row = new Vector<String>();
				// 岗位名称
				String roleName = "";
				if (baseRoleFigure.getFlowElementData().getFlowStationList() != null) {
					for (JecnFlowStationT flowStationT : baseRoleFigure.getFlowElementData().getFlowStationList()) {
						if ("0".equals(flowStationT.getType())) {
							roleName += flowStationT.getStationName() + "/";
						}
					}
					for (JecnFlowStationT flowStationT : baseRoleFigure.getFlowElementData().getFlowStationList()) {
						if ("1".equals(flowStationT.getType())) {
							roleName += flowStationT.getStationName() + "/";
						}
					}
					if (!"".equals(roleName)) {
						roleName = roleName.substring(0, roleName.length() - 1);
					}
				}
				row.add(baseRoleFigure.getFlowElementData().getFlowElementUUID());
				row.add(baseRoleFigure.getFlowElementData().getFigureText());
				if (showPosNameBox) {
					row.add(roleName);
				}
				row.add(baseRoleFigure.getFlowElementData().getRoleRes());
				rowData.add(row);
			}
		}
		return new JecnTableModel(title, rowData);
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
