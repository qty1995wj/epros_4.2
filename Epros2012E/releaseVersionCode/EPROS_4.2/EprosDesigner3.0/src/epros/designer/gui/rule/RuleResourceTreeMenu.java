package epros.designer.gui.rule;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingWorker;
import javax.swing.tree.DefaultTreeModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnLookSetDialog;
import epros.designer.gui.error.ErrorCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.file.RuleVersionFileDialog;
import epros.designer.gui.publish.RuleFilePublishDialog;
import epros.designer.gui.publish.RuleModeFilePublishDialog;
import epros.designer.gui.rule.mode.RuleModeChooseDialog;
import epros.designer.gui.system.ResourceSortDialog;
import epros.designer.gui.task.config.TempletChooseDialog;
import epros.designer.gui.task.jComponent.JecnRuleTaskJDialog;
import epros.designer.service.rule.RuleFilesDownLoad;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.visio.JecnFileNameFilter;

/**
 * @author yxw 2012-11-8
 * @description：制度树右键菜单
 */
public class RuleResourceTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(RuleResourceTreeMenu.class);
	/** 新建制度目录 */
	private JMenuItem newRuleDirItem = new JMenuItem(JecnProperties.getValue("newRuleDir"), new ImageIcon(
			"images/menuImage/newRuleDir.gif"));
	/** 新建制度（模板） */
	private JMenuItem newRuleItem = new JMenuItem(JecnProperties.getValue("newRuleModel"), new ImageIcon(
			"images/menuImage/newRule.gif"));
	/** 新建制度模板(链接) */
	private JMenuItem newRuleLink = new JMenuItem(JecnProperties.getValue("newRuleLink"), new ImageIcon(
			"images/menuImage/uploadRuleFile.gif"));
	/** 搜索 */
	private JMenuItem searchItem = new JMenuItem(JecnProperties.getValue("search"), new ImageIcon(
			"images/menuImage/search.gif"));
	/** 节点排序 */
	private JMenuItem nodeSortItem = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon(
			"images/menuImage/nodeSort.gif"));
	/** 节点移动 */
	private JMenuItem nodeMoveItem = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon(
			"images/menuImage/nodeMove.gif"));
	/** 刷新 */
	private JMenuItem refreshItem = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon(
			"images/menuImage/refresh.gif"));
	/** 发布 */
	private JMenuItem releaseItem = new JMenuItem(JecnProperties.getValue("release"), new ImageIcon(
			"images/menuImage/release.gif"));
	/** 撤销发布 */
	private JMenuItem cancelReleaseItem = new JMenuItem(JecnProperties.getValue("cancelRelease"), new ImageIcon(
			"images/menuImage/cancelRelease.gif"));
	/** 直接发布-不记录版本 */
	private JMenuItem directReleaseItem = new JMenuItem(JecnProperties.getValue("directRelease"), new ImageIcon(
			"images/menuImage/directRelease.gif"));
	/** 提交审批 */
	private JMenuItem submitAppItem = new JMenuItem(JecnProperties.getValue("submitApp"), new ImageIcon(
			"images/menuImage/submitApp.gif"));
	/** 重命名 */
	private JMenuItem renameItem = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon(
			"images/menuImage/rename.gif"));
	/** 查阅权限 */
	private JMenuItem lookItem = new JMenuItem(JecnProperties.getValue("lookPermissions"), new ImageIcon(
			"images/menuImage/lookPermissions.gif"));
	/** 制度文件属性 */
	private JMenuItem editRuleInfoItem = new JMenuItem(JecnProperties.getValue("ruleFileAttribute"), new ImageIcon(
			"images/menuImage/editRuleInfo.gif"));
	/** 关联文件 */
	private JMenuItem uploadRuleFileItem = new JMenuItem(JecnProperties.getValue("uploadRuleFile"), new ImageIcon(
			"images/menuImage/uploadRuleFile.gif"));
	/** 更新制度文件 */
	private JMenuItem updateRuleFileItem = new JMenuItem(JecnProperties.getValue("updateRuleFile"), new ImageIcon(
			"images/menuImage/updateRuleFile.gif"));
	/** 文控信息 */
	private JMenuItem historyItem = new JMenuItem(JecnProperties.getValue("historyRecord"), new ImageIcon(
			"images/menuImage/historyRecord.gif"));
	/** 制度文件下载 */
	private JMenuItem downloadOpDesItem = new JMenuItem(JecnProperties.getValue("ruleFileDownload"), new ImageIcon(
			"images/menuImage/downloadOpDes.gif"));
	/** 设置制度模板 */
	private JMenuItem setRuleModeItem = new JMenuItem(JecnProperties.getValue("setRuleMode"), new ImageIcon(
			"images/menuImage/setRuleMode.gif"));
	/** 重置编号 */
	private JMenuItem reNumItem = new JMenuItem(JecnProperties.getValue("renumber"), new ImageIcon(
			"images/menuImage/renumber.gif"));
	/** 删除 */
	private JMenuItem deleteItem = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon(
			"images/menuImage/delete.gif"));
	/** 制度文件-本地上传-版本信息 */
	private JMenuItem ruleVersionMenu = new JMenuItem(JecnProperties.getValue("VersionRule"), new ImageIcon(
			"images/menuImage/search.gif"));
	/** 本地上传方式 - 新建制度（文件） */
	private JMenuItem uploadLocalRuleFileItem = new JMenuItem(JecnConfigTool.isMengNiuOperaType() ? JecnProperties
			.getValue("newRuleFile") : JecnProperties.getValue("localUpload"), new ImageIcon(
			"images/menuImage/localUpload.gif"));
	/** 自动编号 */
	private JMenuItem autoCodeMenu = new JMenuItem(JecnProperties.getValue("autoNum"));
	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 判断系统制度配置中，制度模板文件 */
	private String ruleModelFileValue = null;
	/** 判断系统制度配置中，制度文件关联文件库方式是否显示 */
	private String ruleFileValue = null;
	/** 判断系统制度配置中，制度文件本地上传方式是否显示 */
	private String ruleLocalFileValue = null;
	private JecnTree jTree = null;
	private Separator separatorOne = new Separator();
	private Separator separatorTwo = new Separator();
	private Separator separatorThree = new Separator();
	private Separator separatorFour = new Separator();
	/** 选中节点集合 */
	private List<JecnTreeNode> selectNodes = null;
	/** 任务配置 */
	/*
	 * 制度任务设置
	 */
	private JMenuItem taskEdit = new JMenuItem(JecnProperties.getValue("rule_Task_Seeting"));

	/** 批量导出制度 */
	private JMenuItem ruleFilesDownloadMenu = new JMenuItem(JecnProperties.getValue("batchDownload"));

	// 提交废止任务
	private JMenuItem desuetudeItem = new JMenuItem(JecnProperties.getValue("submitAbolish"));

	/** 制度 维护支持文件 */
	private JMenuItem ruleUploadFileMenu = new JMenuItem(JecnProperties.getValue("maintenanceSupportFile"));

	/**
	 * 批量发布不记录文控
	 */
	private JMenuItem batchReleaseNotRecord = new JMenuItem(JecnProperties.getValue("batchReleaseNoRecord"));
	/**
	 * 批量撤销发布
	 */
	private JMenuItem batchCancelRelease = new JMenuItem(JecnProperties.getValue("cancelRelease"));

	private JMenuItem pubNoteItem = new JMenuItem();

	public RuleResourceTreeMenu(JecnTree jTree, List<JecnTreeNode> selectNodes, int selectMutil) {
		this.jTree = jTree;
		this.selectNodes = selectNodes;
		if (0 == selectMutil) {
			selectNode = selectNodes.get(0);
			pubNoteItem = JecnUtil.createPubNoteMenu(selectNode);
			pubNoteItem.setVisible(false);
		}
		if (selectNodes.size() == 1) {
			// 单选设置menu
			this.singleSelect();
		} else {
			// 多选设置menu
			this.mutilSelect();
		}
		this.add(newRuleDirItem);
		this.add(newRuleItem);
		// this.add(relStanItem);// 关联标准
		// this.add(relRiskItem);// 关联风险
		this.add(uploadRuleFileItem);// 关联文件
		if (JecnConfigTool.isNewOneType()) { // 招商证券 加载 制度文件（链接） 按钮
			this.add(newRuleLink);
		}
		this.add(uploadLocalRuleFileItem);// 本地上传制度文件
		this.add(updateRuleFileItem);// 更新制度文件
		this.add(separatorOne);

		this.add(batchReleaseNotRecord);
		this.add(batchCancelRelease);

		this.add(editRuleInfoItem);// 编辑制度信息(制度文件属性)
		if (JecnConfigTool.isShowAutoNumber()) {// 是否允许自动编号
			this.add(autoCodeMenu);
		}

		if (JecnConstants.isPubShow()) {
			this.add(pubNoteItem);
			this.add(submitAppItem);// 提交审批
			this.add(desuetudeItem);// 提交废止任务
			this.add(releaseItem);// 发布
			this.add(directReleaseItem);// 发布-不记录文控
			this.add(cancelReleaseItem);// 撤销发布
			if (selectNode != null && selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.ruleRoot) {
				this.add(taskEdit);
			}
			this.add(separatorThree);
		}

		this.add(renameItem);// 重命名
		this.add(nodeSortItem);// 节点排序
		this.add(nodeMoveItem);// 节点移动
		this.add(refreshItem);// 刷新
		this.add(deleteItem);// 删除
		this.add(ruleVersionMenu);// 版本信息
		this.add(separatorTwo);

		this.add(lookItem);// 查阅权限
		// this.add(reNumItem);// 重置编号
		if (JecnConstants.isPubShow()) {
			this.add(historyItem);// 文控信息
		}

		this.add(downloadOpDesItem);// 操作说明下载
		this.add(setRuleModeItem);// 设置制度模板
		if (JecnDesignerCommon.isAdmin() && JecnConfigTool.isWanHuaLogin()) {
			this.add(ruleFilesDownloadMenu);
		}

		if (JecnConfigTool.isArchitectureRuleUploadFile()) {// 是否允许维护上传文件
			this.add(ruleUploadFileMenu);
		}
		menuItemActionInit();
	}

	private void menuItemActionInit() {
		newRuleDirItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newRuleDirItemAction();
			}
		});
		newRuleItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newRuleItemAction();
			}
		});
		/**
		 * // * 相关标准 // * //
		 */
		// relStanItem.addActionListener(new ActionListener() {
		//
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// // 权限验证
		// if (JecnTreeCommon.isDesigner(selectNode) &&
		// !JecnTool.isBeInTask(selectNode)) {// 如果是设计者，并且处于任务中退出
		// return;
		// }
		// // RuleStandardDialog standardDialog = new RuleStandardDialog(
		// // selectNode);
		// // standardDialog.setVisible(true);
		// EditRuleInfoDialog editRuleInfoDialog = new
		// EditRuleInfoDialog(selectNode, jTree, null);
		// if (selectNode.getJecnTreeBean().getTreeNodeType() ==
		// TreeNodeType.ruleFile) {
		// editRuleInfoDialog.gettabPanel().setSelectedIndex(1);
		// } else if (selectNode.getJecnTreeBean().getTreeNodeType() ==
		// TreeNodeType.ruleModeFile) {
		// editRuleInfoDialog.gettabPanel().setSelectedIndex(2);
		// }
		//
		// editRuleInfoDialog.setVisible(true);
		//
		// if (editRuleInfoDialog.isOpaque()) {
		// JecnTreeCommon.reNameNode(jTree, selectNode,
		// editRuleInfoDialog.getRuleBaseInfoPanel().ruleNameField.getText());
		// }
		// }
		// });
		// /**
		// * 相关风险
		// */
		// relRiskItem.addActionListener(new ActionListener() {
		//
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// // 权限验证
		// if (JecnTreeCommon.isDesigner(selectNode) &&
		// !JecnTool.isBeInTask(selectNode)) {// 如果是设计者，并且处于任务中退出
		// return;
		// }
		// // RuleRiskDialog riskDialog = new RuleRiskDialog(selectNode);
		// // riskDialog.setVisible(true);
		// EditRuleInfoDialog editRuleInfoDialog = new
		// EditRuleInfoDialog(selectNode, jTree, null);
		// if (selectNode.getJecnTreeBean().getTreeNodeType() ==
		// TreeNodeType.ruleFile) {
		// editRuleInfoDialog.gettabPanel().setSelectedIndex(2);
		// } else if (selectNode.getJecnTreeBean().getTreeNodeType() ==
		// TreeNodeType.ruleModeFile) {
		// editRuleInfoDialog.gettabPanel().setSelectedIndex(3);
		// }
		// editRuleInfoDialog.setVisible(true);
		//
		// if (editRuleInfoDialog.isOpaque()) {
		// JecnTreeCommon.reNameNode(jTree, selectNode,
		// editRuleInfoDialog.getRuleBaseInfoPanel().ruleNameField.getText());
		// }
		// }
		// });
		historyItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				historyItemAction();
			}
		});
		nodeSortItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeSortItemAction();
			}
		});
		nodeMoveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeMoveItemAction();
			}
		});
		reNumItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reNumItemItemAction();
			}
		});
		cancelReleaseItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// 提示是否撤销发布
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("isCancelRelease"), null, JecnOptionPane.YES_NO_OPTION,
						JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return;
				}
				try {
					ConnectionPool.getRuleAction().cancelRelease(selectNode.getJecnTreeBean().getId(),
							selectNode.getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
					selectNode.getJecnTreeBean().setPub(false);
					((DefaultTreeModel) jTree.getModel()).reload(selectNode);
				} catch (Exception e1) {
					log.error("cancelReleaseItem  is error ", e1);
				}
			}
		});
		directReleaseItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnTreeBean treeBean = selectNode.getJecnTreeBean();
				try {
					// 检查错误信息
					if (ErrorCommon.INSTANCE.checkRuleBaseInfo(treeBean.getId())) {
						return;
					}
					boolean isInTask = JecnTool.isInTask(treeBean);
					if (isInTask) {
						return;
					}
				} catch (Exception ee) {
					log.error("releaseItemAction is error! " + "treeBean.getTreeNodeType() = "
							+ treeBean.getTreeNodeType().toString(), ee);
				}
				// 提示是否不记录版本发布
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("isnotRemberPublish"), null, JecnOptionPane.YES_NO_OPTION,
						JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return;
				}
				try {
					ConnectionPool.getRuleAction().directRelease(selectNode.getJecnTreeBean().getId(),
							selectNode.getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
					selectNode.getJecnTreeBean().setPub(true);
					selectNode.getJecnTreeBean().setUpdate(false);
					((DefaultTreeModel) jTree.getModel()).reload(selectNode);
					// 发布成功
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
				} catch (Exception e1) {
					log.error("directReleaseItem is error", e1);
				}

			}
		});
		refreshItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshItemAction();
			}
		});
		uploadRuleFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				uploadRuleFileItemAction();
			}
		});

		// TODO
		uploadLocalRuleFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				uploadLocalRuleFileItemAction();
			}

		});

		updateRuleFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateRuleFileItemAction();
			}
		});
		submitAppItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				submitAppItemAction(0);
			}
		});
		desuetudeItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				submitAppItemAction(2);
			}
		});
		releaseItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				releaseItemAction();
			}
		});
		renameItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renameItemAction();
			}
		});

		lookItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deptLookItemAction();
			}
		});

		downloadOpDesItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downloadOpDesItemAction();
			}
		});
		// positionLookItem.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// positionLookItemAction();
		// }
		// });
		deleteItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteItemAction();
			}
		});
		// 显示版本记录
		ruleVersionMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				versionFilePerformed();
			}
		});

		// 编辑制度信息
		editRuleInfoItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editRuleInfoItemAction();
			}
		});
		// setRuleModeItem 设置制度模板
		setRuleModeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setRuleModeItemAction();
			}
		});

		taskEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				taskEditAction();

			}
		});
		autoCodeMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				autoCodeAction();
			}
		});
		ruleFilesDownloadMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ruleFilesDownload();
			}
		});
		newRuleLink.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				UploadRuleLinkFileDialog linkFileDialog = new UploadRuleLinkFileDialog(selectNode, jTree);
				linkFileDialog.setVisible(true);
			}
		});

		ruleUploadFileMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ruleUploadFileMenuAction();
			}
		});

		batchReleaseNotRecord.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BatchPubTask t = new BatchPubTask();
				JecnLoading.start(t);
			}
		});
		batchCancelRelease.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingWorker<Void, Void> t = new SwingWorker<Void, Void>() {

					@Override
					protected Void doInBackground() throws Exception {
						batchCancelReleaseAction();
						return null;
					}

					@Override
					public void done() {
						JecnLoading.stop();
					}

				};
				JecnLoading.start(t);
			}
		});

	}

	class BatchPubTask extends SwingWorker<String, Void> {
		@Override
		protected String doInBackground() throws Exception {
			String s = null;
			batchReleaseNotRecordAction();
			return s;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	private void batchReleaseNotRecordAction() {
		// // 是否处于任务审批中
		for (JecnTreeNode selectNode : selectNodes) {
			if (JecnTool.isBeInTaskWithTipName(selectNode)) {
				return;
			}
		}
		// 提示是否发布
		int option = JecnOptionPane
				.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("isnotRemberPublish"),
						null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			List<Long> ids = new ArrayList<Long>();
			for (JecnTreeNode selectNode : selectNodes) {
				ids.add(selectNode.getJecnTreeBean().getId());
			}
			ConnectionPool.getRuleAction().batchReleaseNotRecord(ids,
					selectNodes.get(0).getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
			for (JecnTreeNode selectNode : selectNodes) {
				selectNode.getJecnTreeBean().setPub(true);
				selectNode.getJecnTreeBean().setUpdate(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			}

			// 发布成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
		} catch (Exception e1) {
			log.error("batchReleaseNotRecordAction is error", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	private void batchCancelReleaseAction() {
		// 是否处于任务审批中
		for (JecnTreeNode selectNode : selectNodes) {
			if (JecnTool.isBeInTaskWithTipName(selectNode)) {
				return;
			}
		}

		// 提示是否撤销发布
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("whetherRevokeRelease"), null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			List<Long> ids = new ArrayList<Long>();
			for (JecnTreeNode selectNode : selectNodes) {
				ids.add(selectNode.getJecnTreeBean().getId());
			}
			ConnectionPool.getRuleAction().batchCancelRelease(ids,
					selectNodes.get(0).getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
			for (JecnTreeNode selectNode : selectNodes) {
				selectNode.getJecnTreeBean().setPub(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			}

			// 发布成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("whetherReleaseSucess"));
		} catch (Exception e1) {
			log.error("batchCancelReleaseAction is error", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	protected void ruleUploadFileMenuAction() {
		try {
			// 获取 可维护的文件目录ID
			Long relatedFileId = JecnDesignerCommon.getArchitectureFileDirId(selectNode);
			// 3、 定位到文档管理
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(relatedFileId);
			jecnTreeBean.setName(selectNode.getJecnTreeBean().getName());
			// 将输入表格上的数据传到文件选择框中显示
			FileChooseDialog fileChooseDialog = new FileChooseDialog(new ArrayList<JecnTreeBean>(), relatedFileId);
			// 只能选择一个文件
			fileChooseDialog.setSelectMutil(true);
			fileChooseDialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 重置编号
	 */
	private void reNumItemItemAction() {
		if (StringUtils.isNotBlank(selectNode.getJecnTreeBean().getNumberId())) {
			AutoCodeNodeData nodeData = new AutoCodeNodeData();
			nodeData.setNodeId(selectNode.getJecnTreeBean().getId());// 流程节点ID
			nodeData.setNodeCode(selectNode.getJecnTreeBean().getNumberId());// 制度目录编号
			nodeData.setNodeType(0);// 节点类型
			ConnectionPool.getAutoCodeAction().reNumItemItemAction(nodeData, JecnConstants.getUserId());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("RenumberingDone"));
		} else {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("PleaseAddNumberForNode"));
		}
	}

	protected void taskEditAction() {
		/*
		 * String curSelectTempletId =
		 * JecnTreeCommon.getTaskTempletIdBySelf(selectNode, 2); // 弹出一个面板
		 * TaskConfigSelectDialog taskEditDialog = new
		 * TaskConfigSelectDialog(selectNode, curSelectTempletId, 2);
		 * taskEditDialog.setVisible(true);
		 */
		TempletChooseDialog taskEditDialog = new TempletChooseDialog(selectNode.getJecnTreeBean().getId(), 2);
		taskEditDialog.setVisible(true);
	}

	private void newRuleDirItemAction() {
		AddRuleDirDialog addRuleDirDialog = new AddRuleDirDialog(selectNode, jTree);
		addRuleDirDialog.setVisible(true);

	}

	private void newRuleItemAction() {
		Long modeId = JecnTreeCommon.getRuleModeId(selectNode);
		if (modeId == null) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noModeNoCreateRule"));
			return;
		}
		AddRuleInfoDialog addRuleDialog = new AddRuleInfoDialog(selectNode, jTree);
		addRuleDialog.setVisible(true);
	}

	private void historyItemAction() {
		RuleDoControlPanel ruleDoControlPanel = new RuleDoControlPanel(selectNode, selectNode.getJecnTreeBean().getId());
		ruleDoControlPanel.setVisible(true);
	}

	private void nodeSortItemAction() {
		ResourceSortDialog rs = new ResourceSortDialog(selectNode, jTree);
		rs.setVisible(true);
	}

	private void nodeMoveItemAction() {
		if (!JecnDesignerCommon.validateRepeatNodesName(selectNodes)) {
			return;
		}
		RuleMoveChooseDialog rmc = new RuleMoveChooseDialog(selectNodes, jTree);
		rmc.setVisible(true);
	}

	/**
	 * 文件版本信息
	 */
	protected void versionFilePerformed() {
		if (selectNode == null) {
			return;
		}
		RuleVersionFileDialog ruleFileInj = new RuleVersionFileDialog(selectNode, jTree);
		ruleFileInj.setVisible(true);
	}

	/***************************************************************************
	 * 设置制度模板
	 */
	private void setRuleModeItemAction() {
		List<JecnTreeBean> listBean = new ArrayList<JecnTreeBean>();

		try {
			// 根据制度信息中的模板ID查询制度模板信息
			Long modeId = selectNode.getJecnTreeBean().getModeId();
			if (modeId != null) {
				RuleModeBean ruleModeBean = ConnectionPool.getRuleModeAction().getRuleModeById(modeId);
				if (ruleModeBean != null) {
					JecnTreeBean jecnTreeBean = new JecnTreeBean();
					jecnTreeBean.setId(ruleModeBean.getId());
					jecnTreeBean.setName(ruleModeBean.getModeName());
					listBean.add(jecnTreeBean);
				}
			}

			RuleModeChooseDialog ruleModeChooseDialog = new RuleModeChooseDialog(listBean);
			ruleModeChooseDialog.setSelectMutil(false);
			ruleModeChooseDialog.setVisible(true);
			if (ruleModeChooseDialog.isOperation()) {
				Long modeID = null;
				Long longFlag = 0L; // 为0的时候 listBean没有数据，为1L的时候listBean 不为空
				if (listBean.size() > 0) {
					modeID = Long.valueOf(ruleModeChooseDialog.getContent(listBean).get(0).get(0));
					longFlag = 1L;
					// 更新树节点上的模板ID
					selectNode.getJecnTreeBean().setModeId(
							Long.valueOf(ruleModeChooseDialog.getContent(listBean).get(0).get(0)));
				} else {
					modeID = 0L;
					longFlag = 0L;
					// 更新树节点上的模板ID
					selectNode.getJecnTreeBean().setModeId(null);
				}
				// 更新制度中的模板ID
				ConnectionPool.getRuleAction().updateRuleMode(selectNode.getJecnTreeBean().getId(), modeID,
						JecnConstants.getUserId(), longFlag);

			}
		} catch (Exception e) {
			log.error("setRuleModeItemAction is error", e);
		}
	}

	private void refreshItemAction() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getRuleAction().getChildRules(id, JecnConstants.projectId);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("refreshItemAction is error", ex);
		}
	}

	private void releaseItemAction() {
		if (selectNode == null) {
			return;
		}
		// 检查错误信息
		if (ErrorCommon.INSTANCE.checkRuleBaseInfo(selectNode.getJecnTreeBean().getId())) {
			return;
		}
		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		try {
			boolean isInTask = JecnTool.isInTask(treeBean);
			if (isInTask) {
				return;
			}
		} catch (Exception e) {
			log.error("releaseItemAction is error! " + "treeBean.getTreeNodeType() = "
					+ treeBean.getTreeNodeType().toString(), e);
		}
		if (!JecnTreeCommon.canPub(selectNode)) {
			return;
		}
		switch (treeBean.getTreeNodeType()) {
		case ruleFile:// 制度文件
			RuleFilePublishDialog filePublishDialog = new RuleFilePublishDialog(selectNode);
			filePublishDialog.setVisible(true);
			if (filePublishDialog.isOkBut()) {
				JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean().getTreeNodeType()
						.toString());
				selectNode.getJecnTreeBean().setPub(true);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
				// 发布成功
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
			} else {
				return;
			}
			break;
		case ruleModeFile:// 制度模板文件
			RuleModeFilePublishDialog modeFilePublishDialog = new RuleModeFilePublishDialog(selectNode);
			modeFilePublishDialog.setVisible(true);
			if (modeFilePublishDialog.isOkBut()) {
				JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean().getTreeNodeType()
						.toString());
				selectNode.getJecnTreeBean().setPub(true);
				selectNode.getJecnTreeBean().setUpdate(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
				// 发布成功
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
			} else {
				return;
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 提交制度任务
	 */
	private void submitAppItemAction(int approveType) {
		if (selectNode == null) {
			// TODO 请打开流程图
			return;
		}

		// 检查错误信息
		if (ErrorCommon.INSTANCE.checkRuleBaseInfo(selectNode.getJecnTreeBean().getId())) {
			return;
		}

		// 判断拟稿人是否有岗位
		// if (!JecnUtil.createPeopleIsExistPos()) {
		// return;
		// }
		// 是否处于任务审批中
		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		try {
			boolean isInTask = JecnTool.submitToTask(treeBean);
			if (isInTask) {
				return;
			}
		} catch (Exception e) {
			log.error("submitAppItemAction is error!" + "treeBean.getTreeNodeType() = "
					+ treeBean.getTreeNodeType().toString(), e);
		}
		if (!JecnTreeCommon.canApprove(selectNode, approveType)) {
			return;
		}
		if (approveType == 0) {
			if (JecnDesignerCommon.isOpenWorkFlow(selectNode)) {
				return;
			}
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (desktopPane != null && desktopPane.getFlowMapData().isSave()) {// 面板是否保存
				if (!JecnValidateCommon.isSaveMap(selectNode))
					return;
			}
			if (treeBean.getTreeNodeType().equals(TreeNodeType.process)) {// 流程节点验证图形是否包含错误信息
				// 点击树节点，切换主面板
				JecnDesignerCommon.checkFlowTreeTabPanel(selectNode.getJecnTreeBean().getId());
				// 检查错误信息
				if (ErrorCommon.INSTANCE.checkControlError()) {
					return;
				}
			}

		} else if (approveType == 2) {
			boolean isPub = treeBean.isPub(); // 是否发布
			if (!isPub) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("ruleUnpublished"));
				return;
			}
			int childCount = selectNode.getChildCount();
			if (childCount != 0) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("pleaseDeleteOrAbolishNodes"));
				return;
			}
		}
		JecnRuleTaskJDialog ruleTaskJDialog = new JecnRuleTaskJDialog(jTree, selectNode, approveType);
		ruleTaskJDialog.setVisible(true);

	}

	private void uploadLocalRuleFileItemAction() {
		if (JecnConfigTool.isViewFileCode()) {
			MengNiuUploadRuleFileDialog autoCodeDialog = new MengNiuUploadRuleFileDialog(selectNode, jTree);
			autoCodeDialog.setVisible(true);
		} else {
			UploadRuleFileDialog uploadRuleFileDialog = new G020UploadRuleFileDialog(selectNode, jTree);
			uploadRuleFileDialog.setVisible(true);
		}
	}

	private void uploadRuleFileItemAction() {
		UploadRuleFileDialog uploadRuleFileDialog = new UploadRuleFileDialog(selectNode, jTree);
		uploadRuleFileDialog.setVisible(true);
	}

	private void updateRuleFileItemAction() {
		if (selectNode == null) {
			throw new NullPointerException("updateRuleFileItemAction异常！");
		}
		// // 是否处于任务审批中
		// if (!JecnTreeCommon.isNodeAdmin(selectNode, 2)
		// && !JecnTool.isBeInTask(selectNode)) {// 不是管理员，切处于审批中
		// return;
		// }
		// // 是否有权限执行
		// if (!JecnTreeCommon.singleTreeNode(selectNode, 2)) {
		// return;
		// }

		// true：有操作权限
		boolean result = JecnTreeCommon.isAuth(selectNode.getJecnTreeBean().getId(), selectNode.getJecnTreeBean()
				.getTreeNodeType(), this);
		if (!result) {
			return;
		}

		try {

			RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(selectNode.getJecnTreeBean().getId());
			if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 1) {// 制度文件本地上传方式
				localRuleFileUpdate();
			} else {
				relatedRuleFileUpdate();
			}
		} catch (Exception e) {
			log.error("updateRuleFileItemAction is error", e);
		}
		// 查询数据库

	}

	private void relatedRuleFileUpdate() {

		List<JecnTreeBean> jecnTreeBean = new ArrayList<JecnTreeBean>();
		FileChooseDialog fileChooseDialog = new FileChooseDialog(jecnTreeBean, 0);
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			int count = fileChooseDialog.getResultTable().getRowCount();
			if (count == 1) {
				long fileId = Long.valueOf((String) fileChooseDialog.getResultTable().getValueAt(0, 0));
				String fileName = (String) fileChooseDialog.getResultTable().getValueAt(0, 1);
				if (JecnTreeCommon.validateRepeatNameEidt(fileName, selectNode)) {
					JecnOptionPane.showMessageDialog(null, "\"" + fileName + "\"" + JecnProperties.getValue("isExist"));
					return;
				}
				try {
					ConnectionPool.getRuleAction().updateRuleFile(selectNode.getJecnTreeBean().getId(), fileId,
							fileName, JecnConstants.getUserId());
					selectNode.getJecnTreeBean().setRelationId(fileId);
					selectNode.getJecnTreeBean().setUpdate(true);
					JecnTreeCommon.reNameNode(jTree, selectNode, fileName);
					JecnTreeCommon.selectNode(jTree, selectNode);
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("updateSuccess"));
				} catch (Exception e) {
					log.error("relatedRuleFileUpdate is error", e);
				}
			}

		}

	}

	private void localRuleFileUpdate() {
		if (JecnConfigTool.isViewFileCode()) {// 蒙牛预览编号，更新文件方式
			EditRuleByViewCodeDialog byViewCodeDialog = new EditRuleByViewCodeDialog(jTree, selectNode);
			byViewCodeDialog.setVisible(true);
		} else {// 标准的上传文件
			updateLocalRuleFile();
		}
	}

	private String[] getFileTypeByConfig() {
		String value = JecnConfigTool.getUploadFileType();
		return StringUtils.isBlank(value) ? null : value.split(",");
	}

	private void updateLocalRuleFile() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSavaRuleUpdateDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
		fileChoose.setEidtTextFiled(false);

		String[] arrs = getFileTypeByConfig();
		if (arrs != null) {
			// 设置过滤条件
			fileChoose.setFileFilter(new JecnFileNameFilter(arrs));
			// 移除系统给定的文件过滤器
			fileChoose.setAcceptAllFileFilterUsed(false);
		}
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("updateFile"));

		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("updateFile"));
		int i = fileChoose.showSaveDialog(this);
		if (i == JFileChooser.APPROVE_OPTION) {
			File file = fileChoose.getSelectedFile();

			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSavaRuleUpdateDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			// 判断同一父节点下该文件ID是否存在
			if (JecnTreeCommon.validateRepeatNameEidt(file.getName(), selectNode)) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("fileNameHaved"));
				return;
			}

			JecnTreeBean treeBean = selectNode.getJecnTreeBean();
			try {
				// 需要更新的信息
				RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(treeBean.getId());
				ruleT.setRuleName(file.getName());
				ruleT.setFileBytes(JecnUtil.changeFileToByte(file.getPath()));
				ruleT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
				ruleT.setUpdatePeopleName(JecnConstants.loginBean.getJecnUser().getTrueName());
				Long relationId = ConnectionPool.getRuleAction().updateLocalRuleFile(ruleT);
				if (relationId == null) {
					return;
				}
				treeBean.setRelationId(relationId);
				treeBean.setUpdate(true);
				JecnTreeCommon.reNameNode(jTree, selectNode, JecnUtil.getFileName(file.getPath()));
				JecnTreeCommon.selectNode(jTree, selectNode);
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("updateSuccess"));
			} catch (Exception e) {
				log.error("updateLocalRuleFile is error", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileIsOverSize"));
			}
		}
	}

	private void renameItemAction() {
		EditRuleNameDialog editRuleNameDialog = new EditRuleNameDialog(selectNode, jTree);
		editRuleNameDialog.setVisible(true);

	}

	private void deptLookItemAction() {
		try {
			LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
					selectNode.getJecnTreeBean().getId(), 3);

			JecnLookSetDialog d = new JecnLookSetDialog(lookPopedomBean, selectNode, 3, true);
			d.setVisible(true);

		} catch (Exception e) {
			log.error("deptLookItemAction is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
	}

	private void downloadOpDesItemAction() {
		if (selectNode != null) {
			BufferedOutputStream bos = null;
			FileOutputStream fos = null;
			try {
				// 获取默认路径
				String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveRuleOpDirectory();
				// 获取截取后正确的路径
				if (pathUrl != null && !"".equals(pathUrl)) {
					pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
				}
				JecnFileChooser fileChooser = new JecnFileChooser(pathUrl);
				// 移除系统给定的文件过滤器
				fileChooser.setAcceptAllFileFilterUsed(false);
				// 设置标题
				fileChooser.setDialogTitle(JecnProperties.getValue("downloadRuleInstruts"));
				// 过滤文件
				fileChooser.setFileFilter(new ImportFlowFilter("doc"));
				JecnCreateDoc jecnCreateDoc = null;
				if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.ruleModeFile) {
					jecnCreateDoc = ConnectionPool.getRuleAction().getRuleFile(selectNode.getJecnTreeBean().getId(),
							false);
				}
				if (jecnCreateDoc == null) {
					return;
				}
				// 文件名称获取当前面板名称
				fileChooser.setSelectedFile(new File(jecnCreateDoc.getFileName()));
				int i = fileChooser.showSaveDialog(null);
				// 选择确认（yes、ok）后返回该值。
				if (i == JFileChooser.APPROVE_OPTION) {
					fileChooser.getTextFiled().getText();
					// 获取输出文件的路径
					String fileDirectory = fileChooser.getSelectedFile().getPath();
					// 修改配置文件中的路径
					JecnSystemData.readCurrSystemFileIOData().setSaveRuleOpDirectory(fileDirectory);
					JecnSystemData.writeCurrSystemFileIOData();
					String pathName = fileChooser.getSelectedFile().getName();
					String error = JecnUserCheckUtil.checkFileName(pathName);
					if (!"".equals(error)) {
						JecnOptionPane.showMessageDialog(null, error, null, JecnOptionPane.ERROR_MESSAGE);
						return;
					}
					String xmlPath = null;
					// 判断文件后缀名是否为epros结尾
					if (fileDirectory.endsWith(".doc")) {
						xmlPath = fileDirectory;
					} else {
						xmlPath = fileDirectory + ".doc";
					}
					if (xmlPath != null) {
						// 判断是否重名
						xmlPath = fileDirectory + ".doc";
						File isExists = new File(xmlPath);
						if (isExists.exists()) {
							// 0 为是 1为否
							int selectYorN = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(),
									JecnProperties.getValue("theFileNameAlreadyExistsWhetherToCover"), "",
									JecnOptionPane.YES_NO_OPTION);
							if (selectYorN == 1) {
								return;
							}
						}
					}
					File file = null;
					File dir = new File(xmlPath);
					if (!dir.exists() && dir.isDirectory()) {// 判断文件目录是否存在
						dir.mkdirs();
					}
					file = new File(xmlPath);
					try {
						fos = new FileOutputStream(file);
					} catch (FileNotFoundException e) {
						JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), e.getLocalizedMessage(), "",
								JecnOptionPane.YES_NO_OPTION);
						log.error("", e);
						return;
					}
					bos = new BufferedOutputStream(fos);
					bos.write(jecnCreateDoc.getBytes());
				}
			} catch (Exception e) {
				log.error("", e);
			} finally {
				if (bos != null) {
					try {
						bos.close();
					} catch (IOException e) {
						log.error("", e);
					}
				}
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e1) {
						log.error("", e1);
					}
				}
			}
		}
	}

	private void deleteItemAction() {
		if (selectNodes == null) {
			// TODO 请打开流程图
			return;
		}
		List<Long> listIds = new ArrayList<Long>();

		Set<Long> setIds = new HashSet<Long>();
		for (JecnTreeNode node : selectNodes) {
			listIds.add(node.getJecnTreeBean().getId());
			setIds.add(node.getJecnTreeBean().getId());
		}
		if (listIds.size() == 0) {
			return;
		}
		try {
			if (!canDelOrAbolish(listIds, JecnProperties.getValue("ruleContinsFileCointinueDel"))) {
				return;
			}
			// 验证发布状态。type：0：流程；1：文件；2：制度
			if (!JecnTreeCommon.nodeValidate(2)) {
				boolean flowIsPub = ConnectionPool.getTaskRecordAction().idsIsPub(listIds, TreeNodeType.ruleModeFile);
				if (flowIsPub) {
					// 存在发布状态的节点，请查阅！
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("exitNodeIsPub"));
					return;
				}
			}
			// 删除节点
			ConnectionPool.getRuleAction().deleteFalseRule(setIds, JecnConstants.getUserId(), JecnConstants.projectId);
			/** 删除节点 */
			JecnTreeCommon.removeNodes(jTree, selectNodes);
		} catch (Exception e) {
			log.error("deleteItemAction is error", e);
		}
	}

	private boolean canDelOrAbolish(List<Long> listIds, String tip) {
		try {
			boolean hasFile = ConnectionPool.getProcessAction().belowRuleExistFile(listIds);
			String delTip = hasFile ? JecnProperties.getValue("ruleContinsFileCointinueDel") : JecnProperties
					.getValue("isDelete");
			// 提示是否删除
			int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), delTip, null,
					JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return false;
			}
			// 验证节点审批状态
			boolean isInTask = ConnectionPool.getTaskRecordAction().idsIsInTask(listIds, TreeNodeType.ruleModeFile);
			if (isInTask) {
				// 存在节点处于审批中，请查阅！
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("exitsNodeInTask"));
				return false;
			}
			if (hasFile) {
				List<Long> dirIds = ConnectionPool.getProcessAction().getRelatedFileIds(listIds);
				if (dirIds != null && dirIds.size() > 0) {
					boolean isFileInTask = ConnectionPool.getTaskRecordAction().idsIsInTask(dirIds, TreeNodeType.file);
					if (isFileInTask) {
						// 节点下的文件处于审批中
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("fileUnderTheNodeIsApproving"));
						return false;
					}
				}
			}
		} catch (Exception e1) {
			log.error("", e1);
		}
		return true;
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		desuetudeItem.setVisible(false);
		if (JecnTreeCommon.isAuthNodes(selectNodes)) {
			// 移动
			TreeNodeType type = selectNodes.get(0).getJecnTreeBean().getTreeNodeType();
			if (JecnDesignerCommon.isAuthorAdmin() || JecnDesignerCommon.isRuleAdmin()) {
				switch (type) {
				case ruleFile:
				case ruleModeFile:
					batchReleaseNotRecord.setVisible(true);
					batchCancelRelease.setVisible(true);
					boolean batchReleaseNotRecordEnable = true;
					boolean batchCancelReleaseEnable = true;
					// 批量发布
					for (JecnTreeNode node : selectNodes) {
						JecnTreeNode p = (JecnTreeNode) node.getParent();
						if (p != null && p.getJecnTreeBean().getId() != 0 && !p.getJecnTreeBean().isPub()) {
							batchReleaseNotRecordEnable = false;
							break;
						}
					}
					// 撤销发布
					for (JecnTreeNode node : selectNodes) {
						if (!node.getJecnTreeBean().isPub()) {
							batchCancelReleaseEnable = false;
							break;
						}
					}
					batchReleaseNotRecord.setEnabled(batchReleaseNotRecordEnable);
					batchCancelRelease.setEnabled(batchCancelReleaseEnable);
					break;
				default:
					break;
				}
			}

			// 删除
			deleteItem.setVisible(true);

			nodeMoveItem.setVisible(true);
		}

	}

	private void editRuleInfoItemAction() {
		// 权限验证
		if (JecnTreeCommon.isDesigner(selectNode) && !JecnTool.isBeInTask(selectNode)) {// 如果是设计者，并且处于任务中退出
			return;
		}
		EditRuleInfoDialog editRuleInfoDialog = new EditRuleInfoDialog(selectNode.getJecnTreeBean().getId(),
				selectNode, false, jTree);
		editRuleInfoDialog.setVisible(true);
	}

	private void autoCodeAction() {
		JecnDesignerCommon.autoCodeAction(selectNode, null, false);
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		// JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		int authType;
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case ruleRoot:
			// 刷新
			refreshItem.setVisible(true);
			ruleVersionMenu.setVisible(false);
			if (JecnDesignerCommon.isAdmin()) {
				separatorOne.setVisible(true);
				// 新建制度目录
				newRuleDirItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);

			}
			break;
		// 制度目录
		case ruleDir:
			authType = JecnTreeCommon.isAuthNode(selectNode);
			// 刷新
			reNumItem.setVisible(true);
			refreshItem.setVisible(true);
			if (authType == 1 || authType == 2) {
				separatorOne.setVisible(true);
				separatorTwo.setVisible(true);
				// 新建制度目录
				newRuleDirItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
				newRuleLink.setVisible(true);
				isRuleModeFile();
				// 判断是否为制度模板文件，true：制度模板文件显示，false：制度文件显示
				if ("1".equals(ruleModelFileValue)) {
					// 新建制度
					newRuleItem.setVisible(true);
					// 设置制度模板
					setRuleModeItem.setVisible(true);
				} else if ("0".equals(ruleModelFileValue)) {
					// 新建制度
					newRuleItem.setVisible(false);
					// 设置制度模板
					setRuleModeItem.setVisible(false);
				}
				if ("1".equals(ruleFileValue)) {
					uploadRuleFileItem.setVisible(true);
				} else if ("0".equals(ruleFileValue)) {
					uploadRuleFileItem.setVisible(false);
				}
				if ("1".equals(ruleLocalFileValue)) {
					// 上传制度文件
					uploadLocalRuleFileItem.setVisible(true);
				} else if ("0".equals(ruleLocalFileValue)) {
					// 上传制度文件
					uploadLocalRuleFileItem.setVisible(false);
				}
				// 查阅权限(部门)
				lookItem.setVisible(true);
				if (authType == 2) {
					// 重命名
					renameItem.setVisible(true);
					// 节点移动
					nodeMoveItem.setVisible(true);
					// 删除
					deleteItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignRuleAdmin()));
					ruleVersionMenu.setVisible(false);
				}
				if (JecnConfigTool.isShowRuleMenuTaskEdit()) {
					taskEdit.setVisible(true);
				}
				ruleFilesDownloadMenu.setVisible(true);
				JecnUtil.setMenuDisableThenInTaskExceptAuthorAdmin(selectNode, nodeSortItem, nodeMoveItem, renameItem,
						uploadRuleFileItem, updateRuleFileItem, deleteItem, autoCodeMenu, ruleUploadFileMenu);
			}

			break;
		// 制度文件
		case ruleFile:
			authType = JecnTreeCommon.isAuthNode(selectNode);
			// 刷新
			refreshItem.setVisible(true);
			if (authType == 1 || authType == 2) {

				if (selectNode.getJecnTreeBean().getDataType() == 1) {
					autoCodeMenu.setVisible(true);
					ruleVersionMenu.setVisible(true);
				}
				// relStanItem.setVisible(true);
				// relRiskItem.setVisible(true);
				separatorOne.setVisible(true);
				separatorThree.setVisible(true);
				// 更新制度文件
				updateRuleFileItem.setVisible(true);
				// 编辑制度信息
				editRuleInfoItem.setVisible(true);
				// // 查阅权限(部门)
				// lookItem.setVisible(true);
				// 提交审批
				submitAppItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignRuleAdmin()));
				desuetudeItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignRuleAdmin()));
				historyItem.setVisible(true);
				taskEdit.setVisible(true);
				if (JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignRuleAdmin()) {
					// 发布
					releaseItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignRuleAdmin()));
					// 撤销发布
					cancelReleaseItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode,
							JecnConstants.loginBean.isDesignRuleAdmin()));
					if (selectNode.getJecnTreeBean().isPub()) {
						cancelReleaseItem.setEnabled(true);
					} else {
						cancelReleaseItem.setEnabled(false);
					}
					directReleaseItem.setVisible(true);
				}
				if (authType == 2) {
					separatorTwo.setVisible(true);
					// 节点移动
					nodeMoveItem.setVisible(true);
					// 删除
					deleteItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignRuleAdmin()));
				}
				if (!deleteMenuVisible()) {
					deleteItem.setVisible(false);
				}
				ruleUploadFileMenu.setVisible(true);
				if (selectNode.getJecnTreeBean().isPub() && selectNode.getJecnTreeBean().getApproveType() == -1) {
					pubNoteItem.setVisible(true);
				}
				JecnUtil.setMenuDisableThenInTaskExceptAuthorAdmin(selectNode, nodeSortItem, nodeMoveItem, renameItem,
						uploadRuleFileItem, updateRuleFileItem, deleteItem, autoCodeMenu, ruleUploadFileMenu);
			}
			break;
		// 制度模板文件
		case ruleModeFile:
			authType = JecnTreeCommon.isAuthNode(selectNode);
			if (authType == 1 || authType == 2) {
				autoCodeMenu.setVisible(true);
				// relStanItem.setVisible(true);
				// relRiskItem.setVisible(true);
				separatorThree.setVisible(true);
				// // 查阅权限(部门)
				// lookItem.setVisible(true);
				// 编辑制度信息
				editRuleInfoItem.setVisible(true);
				// 下载操作说明
				downloadOpDesItem.setVisible(true);
				// 提交审批
				submitAppItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignRuleAdmin()));
				desuetudeItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignRuleAdmin()));
				// 文控记录
				historyItem.setVisible(true);
				taskEdit.setVisible(true);
				if (JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignRuleAdmin()) {
					// 发布
					releaseItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignRuleAdmin()));
					// 撤销发布
					cancelReleaseItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode,
							JecnConstants.loginBean.isDesignRuleAdmin()));
					if (selectNode.getJecnTreeBean().isPub()) {
						cancelReleaseItem.setEnabled(true);
					} else {
						cancelReleaseItem.setEnabled(false);
					}
					directReleaseItem.setVisible(true);
				}
				if (authType == 2) {
					separatorTwo.setVisible(true);
					// 重命名
					renameItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignRuleAdmin()));
					// 节点移动
					nodeMoveItem.setVisible(true);
					// 删除
					deleteItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignRuleAdmin()));

					ruleVersionMenu.setVisible(false);
				}
				if (!deleteMenuVisible()) {
					deleteItem.setVisible(false);
				}
				ruleUploadFileMenu.setVisible(false);
				if (selectNode.getJecnTreeBean().isPub() && selectNode.getJecnTreeBean().getApproveType() == -1) {
					pubNoteItem.setVisible(true);
				}
				JecnUtil.setMenuDisableThenInTaskExceptAuthorAdmin(selectNode, nodeSortItem, nodeMoveItem, renameItem,
						uploadRuleFileItem, updateRuleFileItem, deleteItem, autoCodeMenu, ruleUploadFileMenu);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 是否可以执行删除操作
	 * 
	 * @return
	 */
	private boolean deleteMenuVisible() {
		if (!(JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignRuleAdmin())
				&& selectNode.getJecnTreeBean().isPub()) {// 非管理员且非发布的文件，可有删除权限
			return false;
		}
		return true;
	}

	/**
	 * 初始化
	 */
	private void init() {

		/** 新建制度目录 */
		newRuleDirItem.setVisible(false);
		newRuleLink.setVisible(false);
		/** 新建制度 */
		newRuleItem.setVisible(false);
		// relStanItem.setVisible(false);
		// relRiskItem.setVisible(false);
		/** 搜索 */
		searchItem.setVisible(false);
		/** 节点排序 */
		nodeSortItem.setVisible(false);
		/** 节点移动 */
		nodeMoveItem.setVisible(false);
		/** 刷新 */
		refreshItem.setVisible(false);
		/** 发布 */
		releaseItem.setVisible(false);
		cancelReleaseItem.setVisible(false);
		directReleaseItem.setVisible(false);
		/** 提交审批 */
		submitAppItem.setVisible(false);
		desuetudeItem.setVisible(false);
		/** 重命名 */
		renameItem.setVisible(false);
		/** 查阅权限(部门) */
		lookItem.setVisible(false);
		/** 查阅权限(岗位) */
		// positionLookItem.setVisible(false);
		/** 编辑制度信息 */
		editRuleInfoItem.setVisible(false);
		/** 关联文件 */
		uploadRuleFileItem.setVisible(false);
		uploadLocalRuleFileItem.setVisible(false);
		historyItem.setVisible(false);
		updateRuleFileItem.setVisible(false);
		/** 操作说明下载 */
		downloadOpDesItem.setVisible(false);
		/** 设置制度模板 */
		setRuleModeItem.setVisible(false);
		/** 重置编号 */
		reNumItem.setVisible(false);
		/** 删除 */
		deleteItem.setVisible(false);

		separatorOne.setVisible(false);
		separatorTwo.setVisible(false);
		separatorThree.setVisible(false);
		separatorFour.setVisible(false);
		autoCodeMenu.setVisible(false);
		taskEdit.setVisible(false);
		ruleVersionMenu.setVisible(false);
		ruleUploadFileMenu.setVisible(false);
		pubNoteItem.setVisible(false);
		// 批量发布不记录文控
		batchReleaseNotRecord.setVisible(false);
		// 批量撤销发布
		batchCancelRelease.setVisible(false);
	}

	/**
	 * 验证节点是否为制度模版文件
	 * 
	 * @return true 制度模版文件；false：制度文件
	 */
	private void isRuleModeFile() {
		try {
			List<JecnConfigItemBean> list = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(2, 2);
			for (JecnConfigItemBean itemBean : list) {
				if ("basicRuleCreateTypeFile".equals(itemBean.getMark())) {// 文件类型
					if ("1".equals(itemBean.getValue())) {// 1：显示，0：不显示
						ruleFileValue = "1";
					} else {
						ruleFileValue = "0";
					}
				} else if ("basicRuleCreateTypeLocalFile".equals(itemBean.getMark())) {// 文件类型
					if ("1".equals(itemBean.getValue())) {// 1：显示，0：不显示
						ruleLocalFileValue = "1";
					} else {
						ruleLocalFileValue = "0";
					}
				} else if ("basicRuleCreateTypeModel".equals(itemBean.getMark())) {// 制度新建类型：模板
					if ("1".equals(itemBean.getValue())) {// 1：显示，0：不显示
						ruleModelFileValue = "1";
					} else {
						ruleModelFileValue = "0";
					}
				}
			}
		} catch (Exception e) {
			log.error("isRuleModeFile is error", e);
		}
	}

	protected void ruleFilesDownload() {
		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		try {
			RuleFilesDownLoad.ruleFilesDown(treeBean.getId(), treeBean.getName());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ruleFilesDownload is error！", e);
		}
	}
}
