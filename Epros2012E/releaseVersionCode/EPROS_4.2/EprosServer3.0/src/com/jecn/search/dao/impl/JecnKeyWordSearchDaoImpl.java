package com.jecn.search.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.process.util.MyFlowSqlUtil;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.search.dao.IJecnKeyWordSearchDao;

@Repository
public class JecnKeyWordSearchDaoImpl extends AbsBaseDao<String, String> implements IJecnKeyWordSearchDao {
	@Override
	public List<Object[]> keyWordSearch(String searchName, int relatedType, int start, int pageSize) throws Exception {
		WebLoginBean webBean = JecnCommon.getWebLoginBean();
		String sql = getSearchSqlByType(relatedType, webBean.getJecnUser().getPeopleId(), webBean.getProjectId(),
				searchName);
		List<Object[]> lists = null;
		if (JecnCommon.isOracle()) {
			lists = this.listNativeSql(sql, start, pageSize);
		} else if (JecnCommon.isSQLServer()) {
			lists = this.listSQLServerSelect(sql, "RELATEDID", start, pageSize);
		}
		return lists;
	}

	@Override
	public List<Map<String, Object>> keyWordSearchTotal(String searchName, int relatedType) throws Exception {
		WebLoginBean webBean = JecnCommon.getWebLoginBean();
		String sql = getSearchSqlByType(relatedType, webBean.getJecnUser().getPeopleId(), webBean.getProjectId(),
				searchName);
		sql = "SELECT COUNT(AllFileTable.RELATEDID) RELATEDID,AllFileTable.RELATETYPE FROM (" + sql
				+ ") AllFileTable GROUP BY AllFileTable.RELATETYPE";
		List<Map<String, Object>> list = this.getNativeResultToListMap(sql);
		return list;
	}

	/**
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 *            0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
	 * @param searchName
	 * @return
	 */
	private String getSearchSqlByType(int relatedType, Long peopleId, Long projectId, String searchName) {
		switch (relatedType) {
		case 0:
			return getAllAuthSearchSql(peopleId, projectId, searchName);
		case 1:
			return getSearchFlowMapSql(peopleId, projectId, searchName);
		case 2:
			return getSearchFlowSql(peopleId, projectId, searchName);
		case 3:
			return getSearchFileSql(peopleId, projectId, searchName);
		case 4:
			return getSearchSystemFileSql(peopleId, projectId, searchName);
		case 5:
			return getSearchStandardSql(peopleId, projectId, searchName);
		case 6:
			return getSearchRiskSql(peopleId, projectId, searchName);
		}
		return "";
	}

	/**
	 * 根据查阅权限获取需要查询的sql
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 * @param searchName
	 * @return
	 */
	private String getAllAuthSearchSql(Long peopleId, Long projectId, String searchName) {
		StringBuilder builder = new StringBuilder();
		builder.append(getSearchFlowSql(peopleId, projectId, searchName));
		builder.append(" UNION ALL ");
		builder.append(getSearchFlowMapSql(peopleId, projectId, searchName));
		builder.append(" UNION ALL ");
		builder.append(getSearchFileSql(peopleId, projectId, searchName));
		builder.append(" UNION ALL ");
		builder.append(getSearchSystemFileSql(peopleId, projectId, searchName));
		builder.append(" UNION ALL ");
		builder.append(getSearchStandardSql(peopleId, projectId, searchName));
		builder.append(" UNION ALL ");
		builder.append(getSearchRiskSql(peopleId, projectId, searchName));
		return builder.toString();
	}

	/**
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 *            0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
	 * @param searchName
	 *            搜索名称
	 * @return
	 */
	private String getSearchFlowSql(Long peopleId, Long projectId, String searchName) {
		StringBuilder builder = new StringBuilder();

		builder.append("SELECT DISTINCT T.FLOW_ID RELATEDID,");
		builder.append("                T.FLOW_NAME REALTEDNAME,");
		builder.append("                T.pub_time UPDATEDATE, 2 AS RELATETYPE");
		builder.append("  FROM JECN_FLOW_STRUCTURE T");
		if (!JecnContants.isFlowAdmin()) {
			builder.append(" INNER JOIN ").append(MyFlowSqlUtil.getFlowSearch(peopleId, projectId));
			builder.append("    ON MY_AUTH_FILES.FLOW_ID = T.FLOW_ID");
		}
		builder.append(" WHERE T.DEL_STATE = 0");
		builder.append("    AND T.ISFLOW=1 AND T.PROJECTID = " + projectId);
		if (StringUtils.isNotEmpty(searchName)) {
			builder.append("    AND T.FLOW_NAME LIKE '%" + searchName + "%'");
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 *            0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
	 * @param searchName
	 * @return
	 */
	private String getSearchFlowMapSql(Long peopleId, Long projectId, String searchName) {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT DISTINCT T.FLOW_ID RELATEDID,");
		builder.append("                T.FLOW_NAME REALTEDNAME,");
		builder.append("                T.pub_time UPDATEDATE, 1 AS RELATETYPE");
		builder.append("  FROM JECN_FLOW_STRUCTURE T");
		if (!JecnContants.isMainFlowAdmin()) {
			builder.append(" INNER JOIN ").append(MyFlowSqlUtil.getFlowSearch(peopleId, projectId));
			builder.append("    ON MY_AUTH_FILES.FLOW_ID = T.FLOW_ID");
		}
		builder.append(" WHERE T.DEL_STATE = 0");
		builder.append("    AND T.ISFLOW=0 AND T.PROJECTID = " + projectId);
		if (StringUtils.isNotEmpty(searchName)) {
			builder.append("    AND T.FLOW_NAME LIKE '%" + searchName + "%'");
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 *            0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
	 * @param searchName
	 * @return
	 */
	private String getSearchFileSql(Long peopleId, Long projectId, String searchName) {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT DISTINCT T.FILE_ID RELATEDID,");
		builder.append("                T.FILE_NAME REALTEDNAME,");
		builder.append("                T.PUB_TIME UPDATEDATE, 3 AS RELATETYPE");
		builder.append("  FROM JECN_FILE T");
		if (!JecnContants.isFileAdmin()) {
			builder.append(" INNER JOIN ").append(
					JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.FILE));
			builder.append("    ON MY_AUTH_FILES.FILE_ID = T.FILE_ID");
		}
		builder.append(" WHERE T.DEL_STATE = 0");
		builder.append("    AND T.PROJECT_ID = " + projectId + "  AND T.IS_DIR = 1 ");
		if (StringUtils.isNotEmpty(searchName)) {
			builder.append("  AND T.FILE_NAME LIKE '%" + searchName + "%'");
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 *            0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
	 * @param searchName
	 * @return
	 */
	private String getSearchSystemFileSql(Long peopleId, Long projectId, String searchName) {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT DISTINCT T.ID RELATEDID,");
		builder.append("                T.RULE_NAME REALTEDNAME,");
		builder.append("                T.pub_time UPDATEDATE, 4 AS RELATETYPE");
		builder.append("  FROM JECN_RULE T");
		if (!JecnContants.isSystemAdmin()) {
			builder.append(" INNER JOIN ").append(
					JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.RULE));
			builder.append("    ON MY_AUTH_FILES.ID = T.ID");
		}
		builder.append(" WHERE T.PROJECT_ID = " + projectId + "  AND T.IS_DIR <> 0");
		if (StringUtils.isNotEmpty(searchName)) {
			builder.append("  AND T.RULE_NAME LIKE '%" + searchName + "%'");
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 *            0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
	 * @param searchName
	 * @return
	 */
	private String getSearchStandardSql(Long peopleId, Long projectId, String searchName) {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT DISTINCT T.CRITERION_CLASS_ID RELATEDID,");
		builder.append("                T.CRITERION_CLASS_NAME REALTEDNAME,");
		builder.append("                T.UPDATE_DATE UPDATEDATE, 5 AS RELATETYPE");
		builder.append("  FROM JECN_CRITERION_CLASSES T");
		if (!JecnContants.isCriterionAdmin()) {
			builder.append(" INNER JOIN ").append(
					JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.STANDARD));
			builder.append("    ON MY_AUTH_FILES.CRITERION_CLASS_ID = T.CRITERION_CLASS_ID");
		}
		builder.append(" WHERE T.PROJECT_ID = " + projectId + " AND T.STAN_TYPE <> 0 ");
		if (StringUtils.isNotEmpty(searchName)) {
			builder.append("  AND T.CRITERION_CLASS_NAME LIKE '%" + searchName + "%'");
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param RELATETYPE
	 *            0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
	 * @param searchName
	 * @return
	 */
	private String getSearchRiskSql(Long peopleId, Long projectId, String searchName) {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT DISTINCT T.ID RELATEDID, T.NAME REALTEDNAME, T.UPDATE_TIME UPDATEDATE, 6 AS RELATETYPE");
		builder.append("  FROM JECN_RISK T");
		builder.append(" WHERE T.PROJECT_ID = " + projectId + "AND T.IS_DIR = 1 ");
		if (StringUtils.isNotEmpty(searchName)) {
			builder.append("  AND T.NAME LIKE '%" + searchName + "%'");
		}
		return builder.toString();
	}
}
