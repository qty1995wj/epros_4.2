package com.jecn.epros.mapper;

import com.jecn.epros.domain.system.Message;
import com.jecn.epros.sqlprovider.MessageSqlProvider;
import org.apache.ibatis.annotations.SelectProvider;

public interface MessageMapper {

    @SelectProvider(type = MessageSqlProvider.class, method = "saveMessage")
    public void saveMessage(Message message);

}
