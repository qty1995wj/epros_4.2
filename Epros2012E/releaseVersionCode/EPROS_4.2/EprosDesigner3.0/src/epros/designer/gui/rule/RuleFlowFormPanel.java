package epros.designer.gui.rule;

import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.rule.JecnFlowCommon;

import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

public class RuleFlowFormPanel extends JecnFileDescriptionTable {

	private List<JecnFlowCommon> listJecnFlowCommon;
	private int type;

	public RuleFlowFormPanel(int index, String name, JecnPanel contentPanel, List<JecnFlowCommon> listJecnFlowCommon,
			boolean isRequest, int type) {
		super(index, name, isRequest, contentPanel);
		this.type = type;
		this.listJecnFlowCommon = listJecnFlowCommon;
		initTable();
	}

	@Override
	protected void dbClickMethod() {

	}

	@Override
	protected JecnTableModel getTableModel() {
		String name = this.type == 0 ? JecnProperties.getValue("mapName") : JecnProperties.getValue("flowName");
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("mapNum"));
		title.add(name);
		return new JecnTableModel(title, getContent());// updateTableContent(ruleModelList)
	}

	private Vector<Vector<String>> getContent() {
		// 相关标准
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		if (listJecnFlowCommon != null && listJecnFlowCommon.size() > 0) {
			for (int i = 0; i < listJecnFlowCommon.size(); i++) {
				JecnFlowCommon jecnFlowCommon = listJecnFlowCommon.get(i);
				if (jecnFlowCommon != null) {
					Vector<String> data = new Vector<String>();
					data.add(jecnFlowCommon.getFlowId().toString());
					// 流程编号
					data.add(jecnFlowCommon.getFlowNumber());
					// 流程名称
					data.add(jecnFlowCommon.getFlowName());
					vector.add(data);
				}
			}
		}
		return vector;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}
}
