package epros.designer.gui.file;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileUseInfoBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 * 文件使用情况信息
 * 
 * @author 2012-05-30
 * 
 */
public class FileUseInfoDialog extends JPanel {

	private static Logger log = Logger.getLogger(FileUseInfoDialog.class);
	private JecnTreeNode selectNode = null;
	/** 文件名称 */
	private JLabel fileNameLab = null;

	/** 文件名称Field */
	private JTextField fileNameField = null;

	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 信息显示Table */
	private FileUseInfoTable fileUseinfo = null;

	/** 设置大小 */
	private Dimension dimension = null;

	private List<FileUseInfoBean> fileUseInfoList = new ArrayList<FileUseInfoBean>();

	public List<FileUseInfoBean> getFileUseInfoList() {
		return fileUseInfoList;
	}

	public void setFileUseInfoList(List<FileUseInfoBean> fileUseInfoList) {
		this.fileUseInfoList = fileUseInfoList;
	}

	public FileUseInfoTable getFileUseinfo() {
		return fileUseinfo;
	}

	public void setFileUseinfo(FileUseInfoTable fileUseinfo) {
		this.fileUseinfo = fileUseinfo;
	}

	public FileUseInfoDialog(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		initCompotents();
		initLayout();
		// 获取文件ID
		Long fileId = selectNode.getJecnTreeBean().getId();
		// 获取文件使用情况信息
		try {
			fileUseInfoList = ConnectionPool.getFileAction().getFileUsages(fileId);
			// 文件名称Field显示值
			this.fileNameField.setText(selectNode.getJecnTreeBean().getName());

		} catch (Exception e) {
			log.error("FileUseInfoDialog FileUseInfoDialog is error", e);
		}
		fileUseinfo = new FileUseInfoTable();
		TableColumnModel columnModel = fileUseinfo.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(2);
		tableColumn.setMinWidth(60);
		tableColumn.setMaxWidth(60);
		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tableColumn.setCellRenderer(render);
		resultScrollPane.setViewportView(fileUseinfo);
	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {

		// 文件名称
		fileNameLab = new JLabel(JecnProperties.getValue("fileNameC"));

		// 文件名称Field
		fileNameField = new JTextField();
		fileNameField.setEditable(false);
		// 结果滚动面板
		resultScrollPane = new JScrollPane();

		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		fileNameField.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setSize(600, 400);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dimension = new Dimension(600, 310);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.setMaximumSize(dimension);
		resultScrollPane.setMinimumSize(dimension);

		dimension = new Dimension(530, 25);
		fileNameField.setPreferredSize(dimension);
		fileNameField.setMaximumSize(dimension);
		fileNameField.setMinimumSize(dimension);

	}

	/***
	 * 布局
	 */
	private void initLayout() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);

		// 文件名称
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(fileNameLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(fileNameField, c);
		// 文件使用情况表格
		c = new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(resultScrollPane, c);
	}

	public Vector<Vector<Object>> updateTableContent(List<FileUseInfoBean> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		for (FileUseInfoBean fileUseInfoBean : list) {
			Vector<Object> data = new Vector<Object>();
			data.add(fileUseInfoBean.getId());
			// 文件m名称
			data.add(fileUseInfoBean.getName());
			// 文件类型
			if (fileUseInfoBean.getType().toString().equals(TreeNodeType.processMap.toString())) {
				data.add(JecnProperties.getValue("processMap"));
			} else if (fileUseInfoBean.getType().toString().equals(TreeNodeType.process.toString())) {
				data.add(JecnProperties.getValue("process"));
			} else if (fileUseInfoBean.getType().toString().equals(TreeNodeType.organization.toString())) {
				data.add(JecnProperties.getValue("organization"));
			} else if (fileUseInfoBean.getType().toString().equals(TreeNodeType.standard.toString())) {
				data.add(JecnProperties.getValue("standard"));
			} else if (fileUseInfoBean.getType().toString().equals(TreeNodeType.ruleFile.toString())
					|| fileUseInfoBean.getType().toString().equals(TreeNodeType.ruleModeFile.toString())) {
				data.add(JecnProperties.getValue("rule"));
			} else if (fileUseInfoBean.getType().toString().equals(TreeNodeType.position.toString())) {
				data.add(JecnProperties.getValue("position"));
			}else if (fileUseInfoBean.getType().toString().equals(TreeNodeType.riskPoint.toString())) {
				data.add(JecnProperties.getValue("risk_type"));
			}
			vector.add(data);
		}
		return vector;
	}

	class FileUseInfoTable extends JTable {

		FileUseInfoTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		public FileUseInfoTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("name"));
			title.add(JecnProperties.getValue("keyActiveType"));
			return new FileUseInfoTableMode(updateTableContent(fileUseInfoList), title);
		}

		public boolean isSelectMutil() {
			return false;
		}

		public int[] gethiddenCols() {

			return new int[] { 0 };
		}

		class FileUseInfoTableMode extends DefaultTableModel {
			public FileUseInfoTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);

			}

			@Override
			public Class getColumnClass(int columnIndex) {
				if (columnIndex == 4) {
					return ImageIcon.class;
				}
				return super.getColumnClass(columnIndex);
			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}

		}

	}
}
