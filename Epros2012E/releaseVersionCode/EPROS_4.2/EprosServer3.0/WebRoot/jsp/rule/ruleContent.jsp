<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
window.onresize = function() {
	setShowRuleRelatedProcessWH('ruleRelatedProcessShowId');
}
</script>
	</head>

	<body>
	<div id="div" class="div">
			<div id="ruleAttribute"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; float: left; width: 100%">
				<table align="left" width="100%">
					<tr>
						<s:if test="ruleFileWebBean.fileCommonBean!=null">
							<td width="120px;" class="td_dashed">
								<!-- 文件名称： -->
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<s:text name="ruleFileNameC" />
							</td>
							<!-- 如果是本地方式上传的文件 -->
							<s:if test="ruleFileWebBean.fileCommonBean.isFileLocal==1">
								<td style="word-wrap: break-word; word-break: break-all;"
									class="td_dashed">
									&nbsp;
							<a target='_blank'
										href="downloadFile.action?fileId=<s:property value='ruleFileWebBean.ruleInfoBean.ruleId' />&isPub=${isPub}&isPerm=false&downFileType=ruleLocal"
										class="table"> <s:property
											value="ruleFileWebBean.fileCommonBean.fileName" /> </a>
								</td>
							</s:if>
							<s:else>
								<td style="word-wrap: break-word; word-break: break-all;"
									class="td_dashed">
									&nbsp;
									<a target='_blank'
										href="downloadFile.action?fileId=<s:property
									value='ruleFileWebBean.fileCommonBean.fileId' />&isPub=${isPub}&isPerm=false"
										class="table"> <s:property
											value="ruleFileWebBean.fileCommonBean.fileName" /> </a>
								</td>
							</s:else>
						</s:if>
						<s:else>
							<td width="120px;" class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 制度名称： -->
								<s:text name="ruleNameC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								&nbsp; ${ruleFileWebBean.ruleInfoBean.ruleName}
							</td>
						</s:else>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 制度编号： -->
							<s:text name="ruleNumberC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<s:if test="ruleInfoBean.ruleNum==null||ruleInfoBean.ruleNum==''">
							     <s:text name="none"></s:text>
							</s:if>
							<s:else>
							  ${ruleFileWebBean.ruleInfoBean.ruleNum}
							</s:else>
						</td>
					</tr>
					<s:if test="#request.ruleTypeIsShow">
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 制度类别： -->
							<s:text name="ruleTypeC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<s:property value="ruleFileWebBean.ruleInfoBean.ruleTypeName" />
						</td>
					</tr>
					</s:if>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 责任人： -->
							<s:text name="responsiblePersonsC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
						   <s:property value="ruleFileWebBean.ruleInfoBean.responsibleName" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 责任部门： -->
							<s:text name="responsibilityDepartmentC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<a
								href="organization.action?orgId=<s:property value='ruleFileWebBean.ruleInfoBean.dutyOrgId'/>"
								target='_blank' style="cursor: pointer;" class="table"> <s:property
									value="ruleFileWebBean.ruleInfoBean.dutyOrg" /> </a>
						</td>
					</tr>
                   <tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 密级： -->
							<s:text name="intensiveC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<s:if test="ruleFileWebBean.ruleInfoBean.secretLevel== 1">
							    <s:property value="#application.public"/>
							</s:if>
							<s:elseif test="ruleFileWebBean.ruleInfoBean.secretLevel == 0">
								${secret}
							</s:elseif>
						</td>
					</tr>
				</table>
			</div>
		</div>
<%--		<div id="relatedProcess"--%>
<%--			style="margin-top: 10px; margin: 10px 15px 15px 15px;" align="center">--%>
<%--			<table width="98%" style="margin-bottom: 12px;">--%>
<%--				<tr style="background-color: #F3F1F1; width: 100%;">--%>
<%--					<td--%>
<%--						style="font-weight: bold; font-size: 13px; padding: 8px 12px 5px 12px;">--%>
<%--						<s:text name="relatedProcess" />--%>
<%--					</td>--%>
<%--				</tr>--%>
<%--			</table>--%>
<%--			<div id="ruleRelatedProcessShowId" style="overflow-y: scroll;">--%>
<%--				<table width="97%" border="0" cellpadding="0" cellspacing="1"--%>
<%--					align="center"--%>
<%--					style="table-layout: fixed; background-color: #BFBFBF">--%>
<%--					<tr class="FixedTitleRow" align="center">--%>
<%--						<td width="2%" class="gradient">--%>
<%--						</td>--%>
<%--						<td width="25%" height="25" class="gradient"--%>
<%--							style="word-break: break-all;">--%>
<%--							<!-- 流程名称 -->--%>
<%--							<s:text name="processName" />--%>
<%--						</td>--%>
<%--						<td width="21%" height="25" class="gradient"--%>
<%--							style="word-break: break-all;">--%>
<%--							<!-- 流程编号 -->--%>
<%--							<s:text name="processNumber" />--%>
<%--						</td>--%>
<%----%>
<%--						<td width="25%" height="25" class="gradient"--%>
<%--							style="word-break: break-all;">--%>
<%--							<!-- 责任部门-->--%>
<%--							<s:text name="responsibilityDepartment" />--%>
<%--						</td>--%>
<%--						<td width="25%" height="25" class="gradient"--%>
<%--							style="word-break: break-all;">--%>
<%--							<!-- 责任人 -->--%>
<%--							<s:text name="responsiblePersons" />--%>
<%--						</td>--%>
<%--						<td width="2%" class="gradient">--%>
<%--						</td>--%>
<%--					</tr>--%>
<%----%>
<%--					<s:iterator value="ruleFileWebBean.listProcessBean" status="st">--%>
<%--						<tr align="center" style="background-color: #ffffff"--%>
<%--							onmouseover="style.backgroundColor='#EFEFEF';"--%>
<%--							onmouseout="style.backgroundColor='#ffffff';" height="20">--%>
<%--							<td align="right" class="gradient">--%>
<%--								<s:property value="#st.index+1" />--%>
<%--							</td>--%>
<%--							<td class="t5" style="word-break: break-all;">--%>
<%--								<a--%>
<%--									href="process.action?type=process&flowId=<s:property value='flowId'/>"--%>
<%--									target='_blank' style="cursor: pointer;" class="table"> <s:property--%>
<%--										value="flowName" /> </a>--%>
<%--							</td>--%>
<%--							<td class="t5" style="word-break: break-all;">--%>
<%--								<s:property value="flowIdInput" />--%>
<%--							</td>--%>
<%--							<td class="t5" style="word-break: break-all;">--%>
<%--								<a href="organization.action?orgId=<s:property value='orgId'/>"--%>
<%--									target='_blank' style="cursor: pointer;" class="table"> <s:property--%>
<%--										value="orgName" /> </a>--%>
<%--							</td>--%>
<%--							<td class="t5" style="word-break: break-all;">--%>
<%--								<s:property value="resPeopleName" />--%>
<%--							</td>--%>
<%--							<td></td>--%>
<%--						</tr>--%>
<%--					</s:iterator>--%>
<%--				</table>--%>
<%--			</div>--%>
<%--		</div>--%>
	</body>
</html>