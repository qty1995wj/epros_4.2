package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.shape.PaintOvalFigure;

/**
 * 十所：子流程
 * 
 *@author ZXH
 * @date 2016-11-24上午11:12:16
 */
public class ActivityOvalSubFlowFigure extends JecnBaseActiveFigure {
	private int x1;
	private int y1;

	private PaintOvalFigure figurePaint;

	public ActivityOvalSubFlowFigure(JecnFigureData activeData) {
		super(activeData);
		figurePaint = new PaintOvalFigure(this);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;

		super.paintComponent(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		figurePaint.paintNoneFigure(g2d, x1, y1, userWidth, userHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintInputHandTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintInputHandTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintInputHandTop(Graphics2D g2d, int shadowCount) {
		figurePaint.paintInputHandTop(g2d, shadowCount, x1, y1, userWidth, userHeight);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DHandLow(g2d, 0);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d, int indent3Dvalue) {
		paint3DHandLow(g2d, indent3Dvalue);
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	private void paint3DHandLow(Graphics2D g2d, int indent3Dvalue) {
		figurePaint.paint3DHandLow(g2d, indent3Dvalue, x1, y1, userWidth, userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DOvalLine(g2d, 0);
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paint3DOvalLine(Graphics2D g2d, int indent3Dvalue) {
		figurePaint.paint3DOvalLine(g2d, indent3Dvalue, x1, y1, userWidth, userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DOvalLine(g2d, linevalue);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		figurePaint.paint3DPartTop(g2d, x1, y1, userWidth, userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		figurePaint.paint3DAndShadowsPartTop(g2d, x1, y1, userWidth, userHeight);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		// 阴影
		g2d.setPaint(flowElementData.getShadowColor());
		figurePaint.paintShadows(g2d, shadowCount, x1, y1, userWidth, userHeight);
	}

}
