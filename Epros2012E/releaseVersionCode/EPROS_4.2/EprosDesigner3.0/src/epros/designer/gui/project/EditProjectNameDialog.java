package epros.designer.gui.project;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.project.JecnProject;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 重命名 项目名称
 * 
 * @author 2012-05-24
 * 
 */
public class EditProjectNameDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(EditProjectNameDialog.class);
	private JecnProject jecnProject = null;
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditProjectNameDialog(JecnProject jecnProject) {
		this.setLocationRelativeTo(null);
		if (jecnProject == null) {
			return;
		}
		this.jecnProject = jecnProject;
		this.setName(jecnProject.getProjectName());
		this.setLocationRelativeTo(null);
	}

	public EditProjectNameDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		this.setName(selectNode.getJecnTreeBean().getName());

	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		Long id = null;
		if (selectNode == null) {
			jecnProject.setProjectName(getName());
			id = jecnProject.getProjectId();
		} else {
			// 节点重命名
			JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
			id = JecnConstants.projectId;
		}
		try {
			ConnectionPool.getProject().reProjectName(getName(), id);
			this.dispose();
		} catch (Exception e) {
			log.error("EditProjectNameDialog saveData is error", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		if (selectNode != null) {
			return false;
		}
		return ConnectionPool.getProject().validateUpdateName(name, jecnProject.getProjectId());
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
