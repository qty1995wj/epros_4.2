package com.jecn.epros.server.service.system;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 
 * 全局变量与数据库数据同步
 * 
 */
public class JecnCofigItemSyscDB {

	/**
	 * 
	 * 系统启动初始化审批数据
	 * 
	 */
	public static void startInitTaskAppData(
	// 流程图
			List<JecnConfigItemBean> destTaskAppList, List<JecnConfigItemBean> srcTaskAppList) {
		if (destTaskAppList == null || srcTaskAppList == null) {
			return;
		}
		destTaskAppList.clear();
		destTaskAppList.addAll(srcTaskAppList);
	}

	/**
	 * 
	 * 系统启动初始化普通数据
	 * 
	 * @param itemBean
	 */
	public static void startInitConfigItemData(JecnConfigItemBean itemBean) {
		if (itemBean == null) {
			return;
		}

		updateOtherItemData(itemBean);
	}

	/**
	 * 
	 * 
	 * 更新缓存数据
	 * 
	 * @param itemBean
	 *            JecnConfigItemBean
	 */
	public static void updateConfigItemData(JecnConfigItemBean itemBean) {
		if (itemBean == null) {
			return;
		}
		// 更新普通项
		updateOtherItemData(itemBean);

		// 审批环节,除去文控类型（文控类型不在设计器表jtable中显示）
		if (JecnConfigContents.TYPE_SMALL_ITEM_TASKAPP == itemBean.getTypeSmallModel()
				&& JecnConfigContents.TABLE_DATA_YES == itemBean.getIsTableData()) {// 审批环节
			if (JecnConfigContents.TYPE_BIG_ITEM_PARTMAP == itemBean.getTypeBigModel()) {// 流程图
				updateTaskAppData(JecnContants.partMapList, itemBean);
			} else if (JecnConfigContents.TYPE_BIG_ITEM_TOTALMAP == itemBean.getTypeBigModel()) {// 流程地图
				updateTaskAppData(JecnContants.totalMapList, itemBean);
			} else if (JecnConfigContents.TYPE_BIG_ITEM_FILE == itemBean.getTypeBigModel()) {// 文件
				updateTaskAppData(JecnContants.fileList, itemBean);
			} else if (JecnConfigContents.TYPE_BIG_ITEM_ROLE == itemBean.getTypeBigModel()) {// 制度
				updateTaskAppData(JecnContants.roleList, itemBean);
			}
		} else if (JecnConfigContents.TYPE_BIG_ITEM_MAIL == itemBean.getTypeBigModel()) {// 邮件
			updateTaskAppData(JecnContants.emailList, itemBean);
		} else if (JecnConfigContents.TYPE_BIG_ITEM_PUB == itemBean.getTypeBigModel()
				&& JecnConfigContents.TYPE_SMALL_MES_EMAIL_ITEM_PUB == itemBean.getTypeSmallModel()) {// 流程有效期
			updateTaskAppData(JecnContants.flowEndDateList, itemBean);
		} else if (JecnConfigContents.TYPE_BIG_ITEM_PARTMAP == itemBean.getTypeBigModel()
				&& JecnConfigContents.TYPE_SMALL_FLOW_BASIC == itemBean.getTypeSmallModel()) {// 流程属性
			updateTaskAppData(JecnContants.peopleItemList, itemBean);

		} else if (JecnConfigContents.TYPE_SMALL_ITEM_PUB == itemBean.getTypeSmallModel()) {// 权限，
			// 相关文件配置
			updateMapList(JecnContants.sysPubItemMap, itemBean);
		} else if (JecnConfigContents.TYPE_SMALL_PUBLIC_RELATE_FILE == itemBean.getTypeSmallModel()) {// 发布配置
			updateMapList(JecnContants.pubItemMap, itemBean);
		}
	}

	/**
	 * 更新key对应的value
	 * 
	 * @param map
	 * @param itemBean
	 */
	private static void updateMapList(Map<String, String> map, JecnConfigItemBean itemBean) {
		if (map == null || itemBean == null) {
			return;
		}
		if (map.containsKey(itemBean.getMark())) {
			map.put(itemBean.getMark(), itemBean.getValue());
		}
	}

	/**
	 * 
	 * 更新数据
	 * 
	 * @param itemBean
	 *            JecnConfigItemBean
	 */
	private static void updateOtherItemData(JecnConfigItemBean itemBean) {
		String value = itemBean.getValue();
		// 唯一标识
		String mark = itemBean.getMark();

		if (value == null || "".equals(value.trim())) {
			return;
		}
		if (JecnConfigContents.TYPE_BIG_ITEM_PUB == itemBean.getTypeBigModel()
				&& JecnConfigContents.TYPE_SMALL_ITEM_PUB == itemBean.getTypeSmallModel()) {// 权限设置中全局权限项
			// (0：秘密;1：公开)(true是公开，false是密码)
			int valueInt = Integer.valueOf(value).intValue();
			boolean valueBln = (valueInt == 1) ? true : false;
			// 赋值
			setPubValue(mark, valueBln);
		} else if (ConfigItemPartMapMark.systemLocalFileProfix.toString().equals(mark)) {// 文件保存路径前缀
			JecnContants.jecn_path = value;
		} else if (ConfigItemPartMapMark.systemLacalFileSaveType.toString().equals(mark)) {// 文件保存方式
			// 保存方式，0本地保存，1数据库大字段保存
			int valueInt = Integer.valueOf(value).intValue();
			JecnContants.fileSaveType = (valueInt == 1) ? 1 : 0;
		} else if (ConfigItemPartMapMark.basicRuleAutoPubVersion.toString().equals(mark)) {// //
			// 制度自动发布版本号
			JecnContants.theDefaultVersionNumber = value;
		} else if (ConfigItemPartMapMark.otherUserSyncType.toString().equals(mark)) {// 人员同步类型（1:Excel
			// 2:DB;3:岗位匹配）
			JecnContants.otherUserSyncType = Integer.valueOf(value).intValue();
		} else if (ConfigItemPartMapMark.otherOperaType.toString().equals(mark)) {// 操作说明模板类型（0：通用模板（默认））
			JecnContants.otherOperaType = Integer.valueOf(value).intValue();
		} else if (ConfigItemPartMapMark.otherLoginType.toString().equals(mark)) {// 登录类型（0：普通登录（默认））
			JecnContants.otherLoginType = Integer.valueOf(value).intValue();
		} else if (ConfigItemPartMapMark.otherVersionType.toString().equals(mark)) {// 版本类型（0：标准版（默认））
			JecnContants.versionType = Integer.valueOf(value).intValue();
		} else if (ConfigItemPartMapMark.otherEditPort.toString().equals(mark)) {// 1：左上进右下出（默认）；0：四个编辑点都能进出
			JecnContants.editPortType = Integer.valueOf(value).intValue();
		} else if (ConfigItemPartMapMark.downLoadCountIsLimited.toString().equals(mark)) {// 下载次数配置
			// true:启用，false：不启用
			JecnContants.downLoadCountIsLimited = ("1".equals(value)) ? true : false;
		} else if (ConfigItemPartMapMark.downLoadCountValue.toString().equals(mark)) {// 下载次数配置值
			JecnContants.downLoadCountValue = Integer.valueOf(value).intValue();
		} else if (ConfigItemPartMapMark.showPosNameBox.toString().equals(// 流程配置-流程文件定义-角色/职责属性配置：显示岗位名称
				mark)) {
			JecnContants.showPosNameBox = ("1".equals(value)) ? true : false;
		} else if (ConfigItemPartMapMark.showActInOutBox.toString().equals(// 流程配置-流程文件定义-活动说明属性配置：显示活动输入、输出
				mark)) {
			JecnContants.showActInOutBox = ("1".equals(value)) ? true : false;
		} else if (ConfigItemPartMapMark.documentControl.toString().equals(mark)) {// 文控信息查阅权限配置：0：不具有；1：具有流程、流程架构、文件、制度查阅权限的人
			JecnContants.docControlPermission = Integer.valueOf(value).intValue();
		} else if (ConfigItemPartMapMark.proPubOrSecRation.toString().equals(mark)) {
			// 合理化建议 建议是否公开
			JecnContants.proPubOrSecRation = ("1".equals(value)) ? true : false;
		} else if (ConfigItemPartMapMark.proSystemRation.toString().equals(mark)) {
			// 合理化建议 系统管理员是否可以处理意见
			JecnContants.proSystemRation = ("1".equals(value)) ? true : false;
		} else if (ConfigItemPartMapMark.proFlowRefRation.toString().equals(mark)) {
			// 合理化建议 责任人是否可以处理意见
			JecnContants.proFlowRefRation = ("1".equals(value)) ? true : false;
		} else if (ConfigItemPartMapMark.isShowDirList.toString().equals(mark)) {
			JecnContants.isShowDirList = ("1".equals(value)) ? true : false;
		} else if (ConfigItemPartMapMark.riskSysName.toString().equals(mark) && ("1".equals(value)) ? true : false) {
			// 根据配置显示风险体系名称，未配置取国际化默认值
			JecnUtil.custOmResourceMap.put("riskSys", itemBean.getName());
		} else if (ConfigItemPartMapMark.isPublic.toString().equals(itemBean.getMark())) {
			JecnUtil.custOmResourceMap.put("public", itemBean.getName());
		} else if (ConfigItemPartMapMark.isSecret.toString().equals(itemBean.getMark())) {
			JecnUtil.custOmResourceMap.put("secret", itemBean.getName());
		}
	}

	/**
	 * 
	 * 使用场合必须保证taskAppList中已经存在数据(除文控主动类型)
	 * 
	 * 更新对应ID的审批环节数据,没有找到对应IP就不做更新
	 * 
	 * @param taskAppList
	 *            (List<JecnConfigItemBean> 缓存中数据
	 * @param dbItemBean
	 *            JecnConfigItemBean 待更新数据
	 */
	private static void updateTaskAppData(List<JecnConfigItemBean> taskAppList, JecnConfigItemBean dbItemBean) {
		for (JecnConfigItemBean oldItem : taskAppList) {
			int oldID = oldItem.getId();
			int newID = dbItemBean.getId();
			if (oldID != 0 && oldID == newID) {// 同一条数据
				// 名称
				oldItem.setName(dbItemBean.getName());
				// 排序
				oldItem.setSort(dbItemBean.getSort());
				// 必填项
				oldItem.setIsEmpty(dbItemBean.getIsEmpty());
				// 值
				oldItem.setValue(dbItemBean.getValue());
				
				oldItem.setEnName(dbItemBean.getEnName());

				JecnContants.configMaps.put(dbItemBean.getMark(), dbItemBean);
				return;
			}
		}
	}

	/**
	 * 
	 * 全局权限
	 * 
	 * @param mark
	 *            String 权限下唯一标识
	 * @param valueBln
	 *            boolean (0：秘密;1：公开)(true是公开，false是密码)
	 */
	private static void setPubValue(String mark, boolean valueBln) {
		if (ConfigItemPartMapMark.pubPartMap.toString().equals(mark)) {// 流程
			// 浏览权限配置 流程
			JecnContants.isFlowAdmin = valueBln;
		} else if (ConfigItemPartMapMark.pubTotalMap.toString().equals(mark)) {// 流程地图
			// 浏览权限配置 流程地图 true是公开，false是密码
			JecnContants.isMainFlowAdmin = valueBln;
		} else if (ConfigItemPartMapMark.pubOrg.toString().equals(mark)) {// 组织
			// 浏览权限配置 组织 true是公开，false是密码
			JecnContants.isOrgAdmin = valueBln;
		} else if (ConfigItemPartMapMark.pubPos.toString().equals(mark)) {// 岗位
			// 浏览权限配置 岗位 true是公开，false是密码
			JecnContants.isStationAdmin = valueBln;
		} else if (ConfigItemPartMapMark.pubRule.toString().equals(mark)) {// 制度
			// 浏览权限配置 制度 true是公开，false是密码
			JecnContants.isSystemAdmin = valueBln;
		} else if (ConfigItemPartMapMark.pubStand.toString().equals(mark)) {// 标准
			// 浏览权限配置 标准 true是公开，false是密码
			JecnContants.isCriterionAdmin = valueBln;
		} else if (ConfigItemPartMapMark.pubFile.toString().equals(mark)) {// 文件
			// 浏览权限配置 文件 true是公开，false是密码
			JecnContants.isFileAdmin = valueBln;
		}
	}
}
