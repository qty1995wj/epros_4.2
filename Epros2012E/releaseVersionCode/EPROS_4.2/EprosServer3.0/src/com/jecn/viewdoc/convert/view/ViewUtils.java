package com.jecn.viewdoc.convert.view;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;

import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.viewdoc.convert.ViewDocsManager;
import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PDFCtrl;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.wordwriter.WordDocument;

public class ViewUtils {

	public static String getNewTempPath(String fileName) {
		return JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.TEMP_DIR + UUID.randomUUID().toString() + "."
				+ FilenameUtils.getExtension(fileName);
	}

	/**
	 * 文件预览URL：history 情况下，不支持文件下载
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String getDownloadFileURL(ViewInfo viewInfo) throws Exception {
		if (!ViewDocsManager.enabledOnlineViewDownload() || viewInfo == null) {
			return "none";
		}
		Long relatedId = viewInfo.getRelatedId();
		Integer type = viewInfo.getType();

		StringBuffer buffer = new StringBuffer();
		/**
		 * 在线预览类型：文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 ;
		 * 4、流程（文件）已发布，5：流程（文件）未发布;6 :标准附件; 7:流程图/架构图图片 已发布; 8:流程图/架构图图片 未发布
		 * 9：流程文件word 已发布;10：流程文件word 未发布;11:制度模板 已发布 ；12：制度模板未发布
		 **/
		switch (type) {
		case 0:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("isPub=false");
			break;
		case 1:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("isPub=true");
			break;
		case 2:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=ruleLocal");
			buffer.append("&");
			buffer.append("isPub=true");
			break;
		case 3:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=ruleLocal");
			buffer.append("&");
			buffer.append("isPub=false");
			break;
		case 4:
		case 5:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=processFile");
			buffer.append("&");
			buffer.append("isPub=false");
			break;
		case 6:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=standardFile");
			break;
		case 7:// 流程图片
			JecnFlowStructure flowStructure = viewInfo.getFlowStructureService().findJecnFlowStructureById(relatedId);
			if (flowStructure == null) {
				break;
			}
			buffer.append("downloadMapIconAction?");
			buffer.append("flowId=" + relatedId);
			buffer.append("&isPub=false&mapIconPath=" + flowStructure.getFilePath());
			break;
		case 8:
			JecnFlowStructureT flowStructureT = viewInfo.getFlowStructureService().get(relatedId);
			if (flowStructureT == null) {
				break;
			}
			buffer.append("downloadMapIconAction?");
			buffer.append("flowId=" + relatedId);
			buffer.append("&isPub=false&mapIconPath=" + flowStructureT.getFilePath());
			break;
		case 9:// 流程文件 - 发布
			buffer.append("printFlowDoc.action?");
			buffer.append("flowId=" + relatedId);
			buffer.append("&isPub=true");
			break;
		case 10:// 流程文件 - 未发布
			buffer.append("printFlowDoc.action?");
			buffer.append("flowId=" + relatedId);
			buffer.append("&isPub=true");
			break;
		case 11:// 制度模板文件 - 发布
			buffer.append("downloadRuleFile.action?");
			buffer.append("ruleId=" + relatedId);
			buffer.append("&isPub=false");
			break;
		case 12:// 制度模板文件 - 未发布
			buffer.append("downloadRuleFile.action?");
			buffer.append("ruleId=" + relatedId);
			buffer.append("&isPub=true");
			break;
		default:
			break;
		}
		return buffer.toString();
	}

	public static <T> T getPageOfficeBaseBean(HttpServletRequest request, String extension, String filePath) {
		Object result = null;
		if (extension.startsWith("ppt")) {
			result = getPPT(request, extension, filePath);
		} else if (extension.startsWith("doc")) {
			result = getDOC(request, extension, filePath);
		} else if (extension.startsWith("xls")) {
			result = getXLS(request, extension, filePath);
		} else if (extension.startsWith("pdf")) {
			result = getPDF(request, extension, filePath);
		} else {
			throw new IllegalArgumentException("不支持的预览类型：" + extension);
		}
		return (T) result;
	}

	private static PDFCtrl getPDF(HttpServletRequest request, String extension, String filePath) {
		PDFCtrl poCtrl = new PDFCtrl(request);
		poCtrl.setServerPage(request.getContextPath() + "/poserver.zz"); // 此行必须

		// Create custom toolbar
		// poCtrl.addCustomToolButton("打印", "Print()", 6);
		// poCtrl.addCustomToolButton("隐藏/显示书签", "SetBookmarks()", 0);
		// poCtrl.addCustomToolButton("-", "", 0);
		poCtrl.addCustomToolButton("实际大小", "SetPageReal()", 16);
		poCtrl.addCustomToolButton("适合页面", "SetPageFit()", 17);
		poCtrl.addCustomToolButton("适合宽度", "SetPageWidth()", 18);
		poCtrl.addCustomToolButton("-", "", 0);
		poCtrl.addCustomToolButton("首页", "FirstPage()", 8);
		poCtrl.addCustomToolButton("上一页", "PreviousPage()", 9);
		poCtrl.addCustomToolButton("下一页", "NextPage()", 10);
		poCtrl.addCustomToolButton("尾页", "LastPage()", 11);
		poCtrl.addCustomToolButton("-", "", 0);

		poCtrl.setMenubar(false);
		poCtrl.setAllowCopy(false);
		poCtrl.setTitlebar(false);

		poCtrl.webOpen(filePath);
		poCtrl.setTagId("A");

		return poCtrl;
	}

	private static PageOfficeCtrl getPPT(HttpServletRequest request, String extension, String filePath) {
		PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
		request.setAttribute("poCtrl", poCtrl);
		// 设置服务页面
		poCtrl.setServerPage(request.getContextPath() + "/poserver.zz");
		setBaseDocumentAttr(poCtrl);
		setBarDisable(poCtrl);
		// 打开excel
		poCtrl.webOpen(filePath, OpenModeType.pptReadOnly, "");
		poCtrl.setTagId("A");
		return poCtrl;
	}

	private static PageOfficeCtrl getXLS(HttpServletRequest request, String extension, String filePath) {
		PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
		// 设置服务页面
		poCtrl.setServerPage(request.getContextPath() + "/poserver.zz");
		setBarDisable(poCtrl);
		setBaseDocumentAttr(poCtrl);
		// 打开excel
		poCtrl.webOpen(filePath, OpenModeType.xlsReadOnly, "");
		poCtrl.setTagId("A");

		return poCtrl;
	}

	private static PageOfficeCtrl getDOC(HttpServletRequest request, String extension, String filePath) {
		PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
		// 设置服务器页面
		poCtrl.setServerPage(request.getContextPath() + "/poserver.zz");
		// 添加自定义按钮
		// poCtrl.addCustomToolButton("保存", "Save", 1);
		// poCtrl.addCustomToolButton("打印设置", "PrintSet", 0);
		// poCtrl.addCustomToolButton("打印", "PrintFile", 6);
		// poCtrl.addCustomToolButton("全屏/还原", "IsFullScreen", 4);
		// poCtrl.addCustomToolButton("-", "", 0);
		// poCtrl.addCustomToolButton("关闭", "Close", 21);
		// 打开Word文档
		setBaseDocumentAttr(poCtrl);
		setBarDisable(poCtrl);
		poCtrl.webOpen(filePath, OpenModeType.docReadOnly, "");

		poCtrl.setTagId("A");

		return poCtrl;
	}

	private static void setBarDisable(PageOfficeCtrl poCtrl) {
		poCtrl.setMenubar(false);
		poCtrl.setAllowCopy(false);
		poCtrl.setTitlebar(false);
		poCtrl.setCustomToolbar(false);
		poCtrl.setOfficeToolbars(false);
	}

	private static void setBaseDocumentAttr(PageOfficeCtrl poCtrl) {
		WordDocument doc = new WordDocument();
		// 添加水印 ，设置水印的内容
		// doc.getWaterMark().setText("PageOffice开发平台");
		doc.setDisableWindowRightClick(true);// 禁止word鼠标右键
		doc.setDisableWindowDoubleClick(true);// 禁止word鼠标双击
		doc.setDisableWindowSelection(true);// 禁止在word中选择文件内容
		// poCtrl.setJsFunction_AfterDocumentOpened("AfterDocumentOpened");
		poCtrl.setWriter(doc);
	}

	public static String getPageofficePage(String extension) {
		String page = extension;
		if (extension.startsWith("ppt")) {
			page = "ppt";
		} else if (extension.startsWith("doc")) {
			page = "doc";
		} else if (extension.startsWith("xls")) {
			page = "xls";
		}
		return "viewOnline/pageoffice/" + page + ".jsp";
	}

}
