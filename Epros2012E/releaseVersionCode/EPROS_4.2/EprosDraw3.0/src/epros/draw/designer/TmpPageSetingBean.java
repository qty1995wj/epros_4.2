package epros.draw.designer;

public class TmpPageSetingBean {
	/** 每页宽度 */
	private int pageWidth;
	/** 分页符个数 */
	private int pageSets = 1;
	/** 最后一条分页符X坐标 */
	private int pageX;
	/** 是否显示分页符 true:显示分页符 */
	private boolean isShowPageSet = false;
	/** 流程图分页 横竖向显示 true：横向 */
	private boolean pagingVh = true;

	public int getPageWidth() {
		return pageWidth;
	}

	public void setPageWidth(int pageWidth) {
		this.pageWidth = pageWidth;
	}

	public int getPageSets() {
		return pageSets;
	}

	public void setPageSets(int pageSets) {
		this.pageSets = pageSets;
	}

	public int getPageX() {
		return pageX;
	}

	public void setPageX(int pageX) {
		this.pageX = pageX;
	}

	public boolean isShowPageSet() {
		return isShowPageSet;
	}

	public void setShowPageSet(boolean isShowPageSet) {
		this.isShowPageSet = isShowPageSet;
	}

	public boolean isPagingVh() {
		return pagingVh;
	}

	public void setPagingVh(boolean pagingVh) {
		this.pagingVh = pagingVh;
	}

}
