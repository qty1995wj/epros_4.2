package epros.designer.gui.process;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.bean.checkout.CheckoutRoleItem;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;

import epros.designer.gui.rule.RuleDoControlPanel;
import epros.designer.service.JecnDesignerImpl;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnUIUtil;

/**
 * 角色匹配检查
 * 
 * @author hyl
 * 
 */
public class RoleCheckDialog extends JecnDialog {

	private Long flowId;
	private List<CheckoutRoleResult> results = null;
	private static Logger log = Logger.getLogger(RuleDoControlPanel.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = new JScrollPane();

	/** 信息显示Table */
	private RuleDocControlTable table = null;

	/** 设置大小 */
	private Dimension dimension = null;

	/** 查阅 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("closed"));

	public RoleCheckDialog(Long flowId) {
		this.flowId = flowId;
		try {
			results = ConnectionPool.getProcessAction().checkoutRole(flowId);
		} catch (Exception e) {
			log.error("", e);
		}
		init();
	}

	public RoleCheckDialog(Long flowId, boolean saveable) {
		this(flowId);
		this.okBut.setEnabled(saveable);
	}

	public boolean hasUnRelatedRole() {
		if (!JecnUtil.isEmpty(this.results)) {
			for (CheckoutRoleResult result : this.results) {
				if (JecnUtil.isEmpty(result.getData())) {
					continue;
				}
				for (CheckoutRoleItem item : result.getData()) {
					if (item.isFpEqual() && !item.isRelated()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void init() {
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setSize(670, 500);
		// 设置窗体大小
		this.setSize(670, 500);
		this.setTitle(JecnProperties.getValue("roleCheckout"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);
		this.setModal(true);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dimension = new Dimension(620, 360);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.setMaximumSize(dimension);
		resultScrollPane.setMinimumSize(dimension);

		table = new RuleDocControlTable();
		// 设置默认背景色
		table.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		table.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		resultScrollPane.setViewportView(table);

		initLayout();
		/***********************************************************************
		 * 查阅
		 */
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}

		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});

		// table.addMouseListener(new MouseAdapter() {
		// public void mousePressed(MouseEvent evt) {
		// searchTablemousePressed(evt);
		// }
		// });
	}

	// /**
	// * @author yxw 2012-5-9
	// * @description:双击Table
	// */
	// protected void searchTablemousePressed(MouseEvent evt) {
	// if (evt.getClickCount() == 1) {
	// int row = table.getSelectedRow();
	// int oldRowHeight = table.getRowHeight();
	// int newRowHeight = table.getRowHeight(row);
	// System.out.println(oldRowHeight);
	// System.out.println(newRowHeight);
	// if (row != -1) {
	// if (oldRowHeight >= newRowHeight) {
	// MultiLineCellRenderer multiLineCellRenderer = new
	// MultiLineCellRenderer(row, 3, 0);
	// multiLineCellRenderer.setToolTipText(table.getValueAt(row,
	// 3).toString());
	// table.setDefaultRenderer(Object.class, multiLineCellRenderer);
	// } else if (oldRowHeight < newRowHeight) {
	// // 选中后高度设置
	// table.setRowHeight(row, oldRowHeight);
	// MultiLineCellRenderer multiLineCellRenderer = new
	// MultiLineCellRenderer(row, 3, 1);
	// multiLineCellRenderer.setToolTipText(table.getValueAt(row,
	// 3).toString());
	// table.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 3,
	// 1));
	// }
	// }
	// }
	// }

	@SuppressWarnings("unchecked")
	private void okButPerformed() {
		if (isUpdate()) {
			List<CheckoutRoleItem> result = getDataFromTable();
			ConnectionPool.getProcessAction().updateActivityPosRelation(this.flowId, result);
			try {
				JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
				desktopPane.getFlowMapData().setSave(true);
				JecnDesignerImpl.reloadProcessPanel(flowId);
			} catch (Exception e) {
				log.error("刷新面板错误", e);
			}
		}
		this.dispose();
	}

	private List<CheckoutRoleItem> getDataFromTable() {
		TableModel tm = table.getModel();
		List<CheckoutRoleItem> result = new ArrayList<CheckoutRoleItem>();
		CheckoutRoleItem item = null;
		for (int i = 0; i < tm.getRowCount(); i++) {
			item = new CheckoutRoleItem();
			result.add(item);
			item.setFigureId((Long) tm.getValueAt(i, 0));
			item.setPosId((Long) tm.getValueAt(i, 1));
			item.setType((Integer) (tm.getValueAt(i, 2)));
			item.setRelated(((JCheckBox) tm.getValueAt(i, 6)).isSelected());
		}
		return result;
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);

		// 制度文控信息表格
		c = new GridBagConstraints(0, 0, 5, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(resultScrollPane, c);

		c = new GridBagConstraints(0, 1, 3, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(new JLabel(""), c);
		// 查阅
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(okBut, c);
		// 取消 cancelBut
		c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(cancelBut, c);

		this.getContentPane().add(mainPanel);
	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelPerformed() {
		this.dispose();
	}

	public boolean isUpdate() {
		List<CheckoutRoleItem> oldData = this.results.get(0).getData();
		List<CheckoutRoleItem> newData = getDataFromTable();
		Iterator<CheckoutRoleItem> oldIt = oldData.iterator();
		Iterator<CheckoutRoleItem> newIt = newData.iterator();
		while (oldIt.hasNext()) {
			if (oldIt.next().isRelated() != newIt.next().isRelated()) {
				return true;
			}
		}
		return false;
	}

	private List<Integer> cloumns = new ArrayList<Integer>();

	/**
	 * 制度文控表格显示信息
	 * 
	 * @param list
	 * @return
	 */
	public Vector<Vector<Object>> updateTableContent() {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		if (this.results != null && results.size() > 0) {
			String posPrefix = "（" + JecnProperties.getValue("position") + "）";// 岗位
			String groupPrefix = "（" + JecnProperties.getValue("positionGroup") + "）";// 岗位组
			String posPrefixNone = posPrefix + JecnProperties.getValue("none");// 无
			String groupPrefixNone = groupPrefix + JecnProperties.getValue("none");

			for (int i = 0; i < results.size(); i++) {
				CheckoutRoleResult checkout = results.get(i);
				Vector<Object> data = new Vector<Object>();
				if (JecnUtil.isNotEmpty(checkout.getData())) {
					int index = 0;
					for (CheckoutRoleItem item : checkout.getData()) {
						data = new Vector<Object>();
						data.add(item.getFigureId());
						data.add(item.getPosId());
						data.add(item.getType());
						data.add(item.getFigureText());

						String prefix = item.getType() == 0 ? posPrefix : groupPrefix;
						if (StringUtils.isBlank(item.getPosName())) {
							prefix = item.getType() == 0 ? posPrefixNone : groupPrefixNone;
						}
						if (item.isFpEqual() || item.isRelated()) {
							if (item.isFpEqual() && item.isRelated()) {
								data.add("√" + JecnProperties.getValue("related"));// 关联
							} else if (item.isFpEqual()) {
								data.add("◎" + JecnProperties.getValue("sameNameNotRealted"));// 同名未关联
								cloumns.add(index);
							} else if (item.isRelated()) {
								data.add("△" + JecnProperties.getValue("relatedNotSame"));// 关联不同名
							}
							data.add(prefix + item.getPosName());
							data.add(new JCheckBox("", item.isRelated()));
						} else {
							data.add("╳" + JecnProperties.getValue("Not related"));// 未关联
							cloumns.add(index);
							data.add("");
							JCheckBox box = new JCheckBox("", false);
							box.setEnabled(false);
							data.add(box);
						}
						index++;
						vector.add(data);
					}
				}
			}
		}
		return vector;
	}

	class RuleDocControlTable extends JTable {
		private static final long serialVersionUID = 1L;

		RuleDocControlTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// this.getTableHeader().setVisible(false);
			JCheckBox box = new JCheckBox();
			this.getColumnModel().getColumn(6).setCellEditor(new CheckButtonEditor(box));
			this.getColumnModel().getColumn(6).setCellRenderer(new CheckBoxRenderer());
			this.getColumnModel().getColumn(6).setMaxWidth(80);
			this.getColumnModel().getColumn(4).setMaxWidth(80);
			this.getColumnModel().getColumn(3).setPreferredWidth(160);
			this.getColumnModel().getColumn(3).setMaxWidth(200);

		}

		public RuleDocControlTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("");
			title.add("");
			title.add("");
			title.add(JecnProperties.getValue("role"));// "角色"
			title.add(JecnProperties.getValue("isRelated"));// "是否关联 "
			title.add(JecnProperties.getValue("belongToRoleStoreOrPos"));// "角色库名称"
			title.add(JecnProperties.getValue("relatedRole"));// "关联标准角色"
			return new RuleDocControlTableMode(updateTableContent(), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0, 1, 2 };
		}

		@Override
		public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
			Component component = super.prepareRenderer(renderer, row, column);
//			component.setBackground(JecnUIUtil.getDefaultBackgroundColor());
//			component.setFont(new Font("宋体", Font.PLAIN, 12));
			component.setForeground(Color.BLACK);
			if (column == 4 && cloumns.contains(row)) {
//				component.setBackground(Color.red);
				component.setForeground(Color.red);
			}
			return component;
		}

		class RuleDocControlTableMode extends DefaultTableModel {
			public RuleDocControlTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);

			}

			public boolean isCellEditable(int rowindex, int colindex) {
				if (colindex == 6) {
					return true;
				}
				return false;
			}

		}

		class CheckButtonEditor extends DefaultCellEditor implements ItemListener {
			private JCheckBox button;

			public CheckButtonEditor(JCheckBox checkBox) {
				super(checkBox);
			}

			public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
					int column) {
				if (value == null)
					return null;
				button = (JCheckBox) value;
				button.addItemListener(this);
				return (Component) value;
			}

			public Object getCellEditorValue() {
				button.removeItemListener(this);
				return button;
			}

			public void itemStateChanged(ItemEvent e) {
				super.fireEditingStopped();
			}
		}

		class CheckBoxRenderer implements TableCellRenderer {

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				if (value == null) {
					return null;
				}
				return (Component) value;
			}
		}
	}

}
