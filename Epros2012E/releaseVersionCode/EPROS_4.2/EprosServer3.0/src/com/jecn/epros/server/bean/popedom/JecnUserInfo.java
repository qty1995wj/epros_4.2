package com.jecn.epros.server.bean.popedom;

/**
 * JecnUserInfo entity.
 * 人员和岗位关联表
 * @author MyEclipse Persistence Tools
 */

public class JecnUserInfo implements java.io.Serializable {

	// Fields

	private Long userId;//主键ID
	private Long figureId;//岗位FigureID
	private Long peopleId;//人员ID

	// Constructors

	/** default constructor */
	public JecnUserInfo() {
	}

	/** minimal constructor */
	public JecnUserInfo(Long userId) {
		this.userId = userId;
	}

	/** full constructor */
	public JecnUserInfo(Long userId, Long figureId,Long peopleId) {
		this.userId = userId;
		this.figureId = figureId;
		this.peopleId = peopleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
}