package com.jecn.epros.server.action.web.login.ad.guangzhouCsr;

import java.util.List;

import com.jecn.epros.server.email.buss.EmailConfigItemBean;
import com.jecn.epros.server.webBean.task.MyTaskBean;

public class GuangZhouCrsTaskXmlBuilder {

	private List<MyTaskBean> taskBeanList;
	private Long loginId;

	public String build(List<MyTaskBean> taskBeanList, Long loginId) {
		this.taskBeanList = taskBeanList;
		this.loginId = loginId;
		StringBuilder builder = new StringBuilder();
		builder.append("<?xml version='1.0'  encoding='utf-8'?>");
		builder.append("<todolist>");
		builder.append(getAllToDo());
		builder.append("</todolist>");
		return builder.toString();
	}

	private StringBuilder getAllToDo() {
		StringBuilder builder = new StringBuilder();
		for (MyTaskBean myTaskBean : taskBeanList) {
			builder.append("<todo>");
			builder.append("<senduser>" + myTaskBean.getCreateName() + "</senduser>");
			builder.append("<sendtime>" + myTaskBean.getUpdateTime() + "</sendtime>");
			builder.append("<subject>" + myTaskBean.getTaskName() + "</subject>");
			builder.append("<link>" + getTaskApproveLink(myTaskBean.getTaskId()) + "</link>");
			builder.append("<category></category>");
			builder.append("</todo>");
		}
		return builder;
	}

	private String getTaskApproveLink(long taskId) {
		// 
		String epsServer = EmailConfigItemBean.getConfigItemBean().getHttpString();
		return epsServer + "/loginMail.action?accessType=mailApprove&amp;mailTaskId=" + taskId + "&amp;mailPeopleId="
				+ loginId;
	}

}
