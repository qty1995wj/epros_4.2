package epros.designer.gui.define;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class AddTermDefineDialog extends TermDefineDailog {
	private static Logger log = Logger.getLogger(AddTermDefineDialog.class);

	public AddTermDefineDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree, JecnProperties.getValue("addTermDefine"));
		this.initLayout("", "");
	}

	@Override
	protected boolean isExist(String name, JecnTreeNode selectNode) throws Exception {
		return ConnectionPool.getTermDefinitionAction().isExistAdd(name, selectNode.getJecnTreeBean().getId(), 1);
	}

	@Override
	protected void save(String defineName, String defineContent, JecnTreeNode selectNode, JecnTree jTree)
			throws Exception {
		TermDefinitionLibrary termDefinitionLibrary = JecnTermDefineCommon.getTermDefinitionLibrary(defineName,
				selectNode, 1, defineContent);
		termDefinitionLibrary = ConnectionPool.getTermDefinitionAction().createTermDefine(termDefinitionLibrary);
		// 向树节点添加文件 目录
		JecnTreeBean jecnTreeBean = JecnTermDefineCommon.getJecnTreeBean(termDefinitionLibrary);
		JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, selectNode);
	}
}
