package com.jecn.epros.bean;

import java.io.Serializable;

/**
 * 审批阶段的审批人对应3.0项目的TempAuditPeopleBean
 * 两者的属性名需要一致
 * 
 */
public class AuditPeopleBean implements Serializable{
	/** 审核人人员主键ID */
	private String auditId;
	/** 审核人真实姓名 */
	private String auditName;
	/** 审批阶段*/
	private int state;
	/** 审批阶段Id */
	private long stageId;
	
	public String getAuditId() {
		return auditId;
	}
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}
	public String getAuditName() {
		return auditName;
	}
	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public long getStageId() {
		return stageId;
	}
	public void setStageId(long stageId) {
		this.stageId = stageId;
	}
	
	
}
