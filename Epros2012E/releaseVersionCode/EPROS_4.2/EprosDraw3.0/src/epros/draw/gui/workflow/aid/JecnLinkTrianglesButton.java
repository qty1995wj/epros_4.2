package epros.draw.gui.workflow.aid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JButton;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 快速连接图形的三角图标
 * 
 * 当鼠标悬浮在图形A时，在图形上下左右分别显示四个小三角图标
 * 
 * 点击小三角图标能快速添加一个和图形A相关联的图形B
 * 
 * @author ZHOUXY
 * 
 */
public class JecnLinkTrianglesButton extends JButton {
	/** 小三角图标类型 */
	private EditPointType editPointType = null;
	/** 小三角宽 */
	private final int userWidth = 12;
	/** 小三角高 */
	private final int userHeight = 19;
	/** 三角形定义坐标属性:两个底边点、顶点 */
	private int xL, yL, xR, yR, xH, yH;

	/** X坐标集合 */
	private int[] xArray = null;
	/** X坐标集合 */
	private int[] yArray = null;

	/** 快速添加流程元素的面板 */
	private JecnLinkFigureMainProcess linkFigurePanel = null;

	public JecnLinkTrianglesButton(JecnLinkFigureMainProcess linkFigurePanel, EditPointType editPointType) {
		if (linkFigurePanel == null || editPointType == null) {
			JecnLogConstants.LOG_LINK_TRIANGLES_BUTTON.error("JecnLinkTrianglesButton类构造函数：参数为null. linkFigurePanel="
					+ linkFigurePanel + ";editPointType=" + editPointType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.editPointType = editPointType;
		this.linkFigurePanel = linkFigurePanel;

		// 初始化
		initComponents();
		// 添加鼠标事件
		addJecnMouseListener();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {
		switch (editPointType) {
		case left:// 左
			xL = userWidth;
			yL = userHeight;
			xR = userWidth;
			yR = 0;
			xH = 0;
			yH = userHeight / 2;
			this.setSize(userWidth, userHeight);
			break;
		case right:// 右
			xL = 0;
			yL = 0;
			xR = 0;
			yR = userHeight;
			xH = userWidth;
			yH = userHeight / 2;
			this.setSize(userWidth, userHeight);
			break;
		case top:// 上
			xL = 0;
			yL = userWidth;
			xR = userHeight;
			yR = userWidth;
			xH = userHeight / 2;
			yH = 0;
			this.setSize(userHeight, userWidth);
			break;
		case bottom:// 下
			xL = 0;
			yL = 0;
			xR = userHeight;
			yR = 0;
			xH = userHeight / 2;
			yH = userWidth;
			this.setSize(userHeight, userWidth);
			break;
		default:// 不匹配时 选择左面
			JecnLogConstants.LOG_LINK_TRIANGLES_BUTTON
					.error("JecnLinkTrianglesButton类initComponents方法：editPointType值不正确。editPointType=" + editPointType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);

		}
		// 三角形顶点坐标
		xArray = new int[] { xL, xR, xH };
		yArray = new int[] { yL, yR, yH };

		// 无边框
		this.setBorder(null);
		this.setOpaque(false);
	}

	/**
	 * 
	 * 添加鼠标事件
	 * 
	 */
	private void addJecnMouseListener() {
		this.addMouseListener(linkFigurePanel);
	}

	public void paintComponent(Graphics g) {

		// 定义画笔
		Graphics2D g2d = (Graphics2D) g;
		// 备份默认的呈现和图像处理服务管理对象
		RenderingHints rhO = g2d.getRenderingHints();
		// 呈现和图像处理服务管理对象
		RenderingHints renderingHints = JecnUIUtil.getRenderingHints();
		g2d.setRenderingHints(renderingHints);

		// 图形图元轮廓
		g2d.setStroke(JecnUIUtil.getBasicStrokeOne());
		// 三角颜色
		g2d.setPaint(Color.BLUE);
		// 填充
		g2d.fillPolygon(xArray, yArray, 3);

		g2d.setRenderingHints(rhO);

	}

	public void setVisible(boolean aFlag) {
		// 获取当前打开面板类型
		if (JecnWorkflowUtil.isPartMap(linkFigurePanel.getWorkflow())) {
			// 流程元素编辑点:true-左上进右下出（默认）;false-四个编辑点都能进出
			if (JecnSystemStaticData.isEditPortType() == 1
					&& (EditPointType.left == editPointType || EditPointType.top == editPointType)) {
				super.setVisible(false);
				return;
			} else if (JecnSystemStaticData.isEditPortType() == 2 && (EditPointType.right != editPointType)) {
				super.setVisible(false);
				return;
			}
		}
		super.setVisible(aFlag);
	}

	public EditPointType getEditPointType() {
		return editPointType;
	}

}
