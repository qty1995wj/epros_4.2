package epros.designer.gui.system.config.ui.main;

import java.awt.BorderLayout;
import java.awt.Dimension;

import epros.designer.gui.system.config.ui.JecnAbstractJPanel;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.right.JecnRightButtonPanel;

/**
 * 
 * 右面按钮面板、属性面板的容器
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigCenterPanel extends JecnAbstractJPanel {
	/** 属性面板 */
	private JecnPropertyPanel propertyPanel = null;
	/** 右面按钮面板 */
	private JecnRightButtonPanel rightPanel = null;

	public JecnConfigCenterPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		initComponents();
	}

	private void initComponents() {
		// 属性面板
		propertyPanel = new JecnPropertyPanel(dialog);
		// 右面按钮panel
		rightPanel = new JecnRightButtonPanel(dialog);

		this.setBorder(null);
		this.setPreferredSize(new Dimension(100, 340));
		this.setLayout(new BorderLayout());

		this.add(propertyPanel, BorderLayout.CENTER);
		this.add(rightPanel, BorderLayout.EAST);

	}

	public JecnPropertyPanel getPropertyPanel() {
		return propertyPanel;
	}

	public JecnRightButtonPanel getRightPanel() {
		return rightPanel;
	}

}
