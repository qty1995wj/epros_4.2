package wordxml.constant;

public class Constants {

	/********** 常用中文字号定义 单位 磅值 **************/
	public static final int chuhao = 84;// 初号
	public static final int xiaochu = 72;// 小初
	public static final int yihao = 52;// 一号
	public static final int xiaoyi = 48;// 小一
	public static final int erhao = 44;// 二号
	public static final int xiaoer = 36;// 小二
	public static final int sanhao = 32;// 三号
	public static final int xiaosan = 30;// 小三
	public static final int sihao = 28;// 四号
	public static final int xiaosi = 24;// 小四
	public static final int wuhao = 21;// 五号
	public static final int xiaowu = 18;// 小五
	public static final int liuhao = 15;// 六号
	public static final int xiaoliu = 13;// 小六
	public static final int qihao = 11;// 七号
	public static final int bahao = 10;// 八号

	/***************************/
	public static final int jiuhao = 18;// 九号
	public static final int shihao = 20;// 十号
	public static final int shidianwuhao = 21;// 10.5号
	public static final int shiyihao = 22;// 十一号
	public static final int shierhao = 24;// 十二号
	public static final int shisanhao = 26;// 十三号
	public static final int shisihao = 28;// 十四号
	public static final int shiwuhao = 30;// 十五号
	public static final int ershihao = 40;// 二十号
	public static final int sanshihao = 60;// 三十号
	public static final int sishiba = 96;// 四十八号

	// 段落风格 模版中已经定义好的风格
	public enum PStyle {
		// 页眉使用的下划线
		AF1("af1"),
		// 一级标题
		LVL1("1"),
		// 二级标题
		LVL2("2");
		private String value;

		private PStyle(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

	// 文档网格
	public enum DocGridType {
		// 行网格、行和字符网格、文字对齐字符网格
		LINES("lines"), LINESANDCHARTS("lines-and-chars"), SNAPTOCHARTS("snap-to-chars");
		private String value;

		private DocGridType(String name) {
			this.value = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

	// 定位方式
	public enum PositionType {
		absolute, // 绝对
		relative
		// 相对
	}

	// 线的类型
	public enum LineStyle {
		single, thinThin, thinThick, thickThin, thickBetweenThin
	}

	// 下划线类型
	public enum LineType {
		// 单线 双线 点线 虚线
		// single, thick, dotted, dashed
		SINGLE("single"), THINK("thick"), DOTTED("dotted"), DASHED("dashed");
		private String value;

		private LineType(String name) {
			this.value = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

	// 背景底纹类型
	public enum ShdType {
		// solid pct-5 pct-10 pct-15 pct-20 pct-25 pct-30
		SOLID("solid"), PCT5("pct-5"), PCT10("pct-10"), PCT15("pct-15"), PCT20("pct-20"), PCT25("pct-25"), PCT30(
				"pct-30");
		private String value;

		private ShdType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

	// 水平对齐方式
	public enum Align {
		left, center, right, both, distribute
	}

	// 垂直对齐方式
	public enum Valign {
		top, center, bottom
	}

	// 页眉，页脚类型 首页 奇数页 偶数页
	public enum HeaderOrFooterType {
		first, even, odd;
	}

	/** 段落间距类型 */
	public enum LineRuleType {
		/** 固定值 **/
		EXACT("exact"),
		/** 单倍、1.5倍、2倍、多倍行距 **/
		AUTO("auto"),
		/** 最小值 **/
		AT_LEAST("at-least");

		// 枚举对应的值
		private String value;

		public String getValue() {
			return value;
		}

		private LineRuleType(String value) {
			this.value = value;
		}
	}

	// NFC
	public enum NFC {
		NFC_39("39");// 一

		private String value;

		public String getValue() {
			return value;
		}

		private NFC(String value) {
			this.value = value;
		}
	}
}
