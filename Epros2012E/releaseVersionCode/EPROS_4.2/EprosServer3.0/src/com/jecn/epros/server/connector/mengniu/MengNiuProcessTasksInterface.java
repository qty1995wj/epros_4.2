package com.jecn.epros.server.connector.mengniu;

import java.util.List;

/**
 * 处理流程
 * 
 * @author xiaohu
 * 
 */
public class MengNiuProcessTasksInterface {
	/** 1参数A模式 2参数B模式 */
	private int ParamMode = 3;

	private List<MengNiuProcessTaskBean> TasksC;

	public int getParamMode() {
		return ParamMode;
	}

	public void setParamMode(int paramMode) {
		ParamMode = paramMode;
	}

	public List<MengNiuProcessTaskBean> getTasksC() {
		return TasksC;
	}

	public void setTasksC(List<MengNiuProcessTaskBean> tasksC) {
		TasksC = tasksC;
	}

}
