package epros.designer.gui.process.activitiesProperty;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 活动明细： 输入
 * 
 * @author 2012-07-27
 * 
 */
public class ActivitiesInPanel extends JecnPanel implements CaretListener {
	private static Logger log = Logger.getLogger(ActivitiesInPanel.class);
	/** 按钮面板 */
	private JecnPanel buttonPanel;

	/** * 输入按钮 */
	private JButton inBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** * 删除按钮 */
	private JButton deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** * 打开文件按钮 */
	private JButton openBut = new JButton(JecnProperties.getValue("openFile"));

	/** 所在的滚动面板 */
	private JScrollPane activeInScrollPane = new JScrollPane();
	private JTable jecnTable = null;
	// 说明输入
	private JecnPanel notePanel;
	private JecnTextArea noteTextArea;
	private JScrollPane noteTablePane;
	/** 输入说明提示信息框 */
	protected JecnUserInfoTextArea noteInfoTextArea;;
	/** 输入文件 */
	private List<JecnTreeBean> listFile = null;

	public JTable getJecnTable() {
		return jecnTable;
	}

	public void setJecnTable(JTable jecnTable) {
		this.jecnTable = jecnTable;
	}

	public JecnTextArea getNoteTextArea() {
		return noteTextArea;
	}

	public void setNoteTextArea(JecnTextArea noteTextArea) {
		this.noteTextArea = noteTextArea;
	}

	/** 设置大小 */
	private Dimension dimension = null;
	/** 活动的数据 */
	private JecnActiveData jecnActiveData;

	/** 删除文件id集合 */
	private List<Long> actInIds = null;

	/** 框的拥有者 */
	private JecnDialog jecnDialog;

	public List<Long> getActInIds() {
		return actInIds;
	}

	public ActivitiesInPanel(JecnActiveData jecnActiveData, JecnDialog jecnDialog) {
		this.jecnActiveData = jecnActiveData;
		this.jecnDialog = jecnDialog;
		boolean isShowEleAttrs = JecnDesignerCommon.isShowEleAttribute();
		if (isShowEleAttrs) {
			dimension = new Dimension(550, 80);
		}else{
			this.setSize(550, 450);
			dimension = new Dimension(550, 300);
		}
		
		activeInScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		activeInScrollPane.setPreferredSize(dimension);
		activeInScrollPane.setMaximumSize(dimension);
		activeInScrollPane.setMinimumSize(dimension);
		
		buttonPanel = new JecnPanel(550, 30);
		// 删除输入文件

		deleteBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButFormed();
			}
		});
		// 选择输入文件
		inBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				inButFormed();
			}
		});
		// 打开文件
		openBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 判断是否选中Table中的一行，没选中，提示选中一行
				int[] selectRows = jecnTable.getSelectedRows();
				if (selectRows.length == 1) {
					Long id = Long.valueOf((String) jecnTable.getValueAt(selectRows[0], 0));
					JecnDesignerCommon.openFile(id);
				} else {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
				}
			}
		});

		intComponents();
		initLayout();
		initData();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.jecnActiveData = jecnActiveData;
		initData();
		removeAllTable();
		Vector<Vector<String>> fileData = getContent();
		for (Vector<String> v : fileData) {
			((DefaultTableModel) jecnTable.getModel()).addRow(v);
		}
	}

	private void initData() {
		if (isShowInputText()) {
			// 初始化输入说明
			this.noteTextArea.setText(jecnActiveData.getActivityInput());
		}
	}

	/**
	 * true:显示文本
	 * 
	 * @return
	 */
	private boolean isShowInputText() {
		return JecnConfigTool.isShowInputText();
	}

	private void intComponents() {
		if (isShowInputText()) {
			notePanel = new JecnPanel();
			notePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			notePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("actBase")));
			noteInfoTextArea = new JecnUserInfoTextArea();
			noteTextArea = new JecnTextArea();
			noteTextArea.setRows(2);
			noteTablePane = new JScrollPane();
			noteTextArea.addCaretListener(this);
		}
	}

	private void initLayout() {

		jecnTable = new JTable(getTableModel());
		// 设置默认背景色
		jecnTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		jecnTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = jecnTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(0);
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		activeInScrollPane.setViewportView(jecnTable);

		GridBagConstraints c = null;
		Insets insets = new Insets(1, 1, 1, 1);

		if (notePanel != null) {
			// 添加说明
			noteTablePane.setViewportView(noteTextArea);
			notePanel.setLayout(new GridBagLayout());
			c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			notePanel.add(noteTablePane, c);
			c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			this.add(notePanel, c);
			if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standActivityInputNotEmpty)) {
				c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				notePanel.add(new CommonJlabelMustWrite(), c);
			}
			// 输入说明信息提示
			c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					JecnUIUtil.getInsets0(), 0, 0);
			this.add(noteInfoTextArea, c);
		}

		// 表格
		// Insets insets2 = new Insets(2, 5, 2, 2);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(activeInScrollPane, c);

		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(inBut);
		buttonPanel.add(deleteBut);
		buttonPanel.add(openBut);
	}

	/**
	 * 
	 * 输入框的插入字符事件
	 * 
	 */
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	protected void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == noteTextArea) {// 编号输入框
			// 编号长度不能超过1200个字符或600个汉字
			checkTextFieldLength(noteInfoTextArea, noteTextArea);
		}
	}

	/**
	 * 
	 * 校验JTextField的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textField
	 *            JTextField
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JecnTextArea jecnTextArea) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkAreaNote(jecnTextArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 点击确认按钮提交前校验
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	protected boolean okBtnCheck() {
		boolean ret = true;
		if (noteTextArea != null) {
			// 编号
			ret = checkTextFieldLength(noteInfoTextArea, noteTextArea);
		}

		return ret;
	}

	/***************************************************************************
	 * 输入文件选择
	 */
	@SuppressWarnings("unchecked")
	private void inButFormed() {
		listFile = new ArrayList<JecnTreeBean>();
		// 获取输入表格上的数据
		Vector<Vector<String>> listActiveInFile = ((JecnTableModel) jecnTable.getModel()).getDataVector();
		if (listActiveInFile != null) {
			for (Vector<String> vafs : listActiveInFile) {
				JecnTreeBean jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(Long.valueOf(vafs.get(0)));
				jecnTreeBean.setName(vafs.get(1));
				listFile.add(jecnTreeBean);
			}
		}
		// 将输入表格上的数据传到文件选择框中显示
		FileChooseDialog fileChooseDialog = new FileChooseDialog(listFile, jecnDialog);
		// 只能选择一个文件
		fileChooseDialog.setSelectMutil(true);
		fileChooseDialog.setVisible(true);

		if (fileChooseDialog.isOperation()) {
			Vector<Vector<String>> fileData = ((JecnTableModel) fileChooseDialog.getResultTable().getModel())
					.getDataVector();
			removeAllTable();
			for (Vector<String> v : fileData) {
				((DefaultTableModel) jecnTable.getModel()).addRow(v);
			}
		}
	}

	private void removeAllTable() {
		for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(index);
		}
	}

	/***************************************************************************
	 * 删除
	 */
	@SuppressWarnings("unchecked")
	private void deleteButFormed() {
		int[] selectRows = jecnTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseSelectDeleteTheInputFile"));
			return;
		}
		int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isDeleteC"), null,
				JecnOptionPane.YES_NO_OPTION);
		if (dialog == JecnOptionPane.YES_OPTION) {
			Vector<Vector<String>> vectors = ((DefaultTableModel) jecnTable.getModel()).getDataVector();
			actInIds = new ArrayList<Long>();
			for (int i = 0; i < selectRows.length; i++) {
				for (int j = 0; j < vectors.size(); j++) {
					Vector<String> vector = vectors.get(i);
					actInIds.add(Long.valueOf(vector.get(0)));
				}
			}

			for (int i = selectRows.length - 1; i >= 0; i--) {
				((DefaultTableModel) jecnTable.getModel()).removeRow(selectRows[i]);
			}
		}
	}

	/***************************************************************************
	 * 初始化内容
	 * 
	 * @return
	 */
	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		try {

			if (jecnActiveData != null && jecnActiveData.getListJecnActivityFileT() != null) {
				for (JecnActivityFileT jecnActivityFileT : jecnActiveData.getListJecnActivityFileT()) {
					if (jecnActivityFileT.getFileType().intValue() == 0) {
						Vector<String> row = new Vector<String>();
						row.add(String.valueOf(jecnActivityFileT.getFileSId()));
						// 文件名称
						row.add(jecnActivityFileT.getFileName());
						vs.add(row);
					}
				}
			}

		} catch (Exception e) {
			log.error("ActivitiesInPanel getContent is error", e);
		}
		return vs;
	}

	/**
	 * 生成table的Model
	 * 
	 * @return
	 */
	private JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getContent());
	}

	/**
	 * @author zhangchen Aug 28, 2012
	 * @description：获得活动输入和操作规范的结果
	 * @return
	 */
	public void setInputResult() {
		// 获得行数
		int rowCount = jecnTable.getRowCount();
		JecnTableModel jecnTableModel = (JecnTableModel) jecnTable.getModel();
		// 记录活动输入文件ID
		List<Long> listIds = new ArrayList<Long>();
		// 添加和更新输入
		for (int i = 0; i < rowCount; i++) {
			// 如果ID为空 返回
			if (jecnTableModel.getValueAt(i, 0) == null || "".equals(jecnTableModel.getValueAt(i, 0).toString())) {
				return;
			}
			boolean isNewAdd = true;
			for (JecnActivityFileT jecnActivityFileT : jecnActiveData.getListJecnActivityFileT()) {
				if (jecnActivityFileT.getFileType().intValue() == 0) {
					if (jecnTableModel.getValueAt(i, 0).toString().equals(jecnActivityFileT.getFileSId().toString())) {
						jecnActivityFileT.setFileName(jecnTableModel.getValueAt(i, 1).toString());
						listIds.add(jecnActivityFileT.getFileSId());
						isNewAdd = false;
						break;
					}
				}
			}
			if (isNewAdd) {
				JecnActivityFileT jecnActivityFileT = new JecnActivityFileT();
				jecnActivityFileT.setFigureId(jecnActiveData.getFlowElementId());
				jecnActivityFileT.setFileSId(Long.valueOf(jecnTableModel.getValueAt(i, 0).toString()));
				jecnActivityFileT.setFileName(jecnTableModel.getValueAt(i, 1).toString());
				jecnActivityFileT.setFileType(Long.valueOf(0));
				jecnActiveData.getListJecnActivityFileT().add(jecnActivityFileT);
				listIds.add(jecnActivityFileT.getFileSId());
			}
		}
		// 清除删除
		for (int i = jecnActiveData.getListJecnActivityFileT().size() - 1; i >= 0; i--) {
			JecnActivityFileT jecnActivityFileT = jecnActiveData.getListJecnActivityFileT().get(i);
			if (jecnActivityFileT.getFileType().intValue() == 0) {
				if (!listIds.contains(jecnActivityFileT.getFileSId())) {
					jecnActiveData.getListJecnActivityFileT().remove(i);
				}
			}
		}

		if (this.getNoteTextArea() != null) {
			jecnActiveData.setActivityInput(this.getNoteTextArea().getText());
		}
	}

	public boolean isUpdate() {
		if (isShowInputText()
				&& !DrawCommon.checkStringSame(this.getNoteTextArea().getText(), jecnActiveData.getActivityInput())) {
			return true;
		}
		// 获得行数
		int rowCount = jecnTable.getRowCount();
		JecnTableModel jecnTableModel = (JecnTableModel) jecnTable.getModel();
		// 记录活动输入文件ID
		List<Long> listIds = new ArrayList<Long>();
		// 记录已存在的输入ID集合
		for (int i = jecnActiveData.getListJecnActivityFileT().size() - 1; i >= 0; i--) {
			JecnActivityFileT jecnActivityFileT = jecnActiveData.getListJecnActivityFileT().get(i);
			if (jecnActivityFileT.getFileType().intValue() == 0) {
				listIds.add(jecnActivityFileT.getFileSId());
			}
		}

		for (int i = 0; i < rowCount; i++) {
			// 如果ID为空 返回
			if (jecnTableModel.getValueAt(i, 0) == null || "".equals(jecnTableModel.getValueAt(i, 0).toString())) {
				continue;
			}

			Long newFileId = Long.valueOf(jecnTableModel.getValueAt(i, 0).toString());
			boolean isExist = false;
			if (listIds.contains(newFileId)) {
				isExist = true;
				// 删除存在的Id，剩余的ID集合为待删除的文件ID集合
				listIds.remove(newFileId);
			}
			if (!isExist) {
				// 不存在，当前ID为新增ID
				return true;
			}
		}
		if (listIds.size() > 0) {
			return true;
		}
		return false;
	}
}
