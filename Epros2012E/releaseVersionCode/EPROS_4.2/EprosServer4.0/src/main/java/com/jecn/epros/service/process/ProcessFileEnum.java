package com.jecn.epros.service.process;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.common.Tuple;
import com.jecn.epros.domain.process.FlowBasicInfo;
import com.jecn.epros.domain.process.FlowStructure;
import com.jecn.epros.domain.process.inout.JecnFigureInoutBase;
import com.jecn.epros.domain.process.inout.JecnFigureInoutSampleBase;
import com.jecn.epros.domain.process.inout.JecnFlowInoutBean;
import com.jecn.epros.domain.process.inout.JecnFlowInoutPosBean;
import com.jecn.epros.domain.process.kpi.FlowKpiName;
import com.jecn.epros.domain.process.processFile.*;
import com.jecn.epros.domain.process.processFile.BaseProcessFileBean.ProcessFileType;
import com.jecn.epros.domain.process.processFile.ProcessFileTable.FileTableCell;
import com.jecn.epros.domain.process.relatedFile.RelatedProcess;
import com.jecn.epros.domain.process.relatedFile.RelatedRisk;
import com.jecn.epros.domain.process.relatedFile.RelatedStandard;
import com.jecn.epros.domain.process.relatedFile.StandardizedFile;
import com.jecn.epros.mapper.*;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.HttpUtil;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Utils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import java.sql.Clob;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * 流程文件 页面显示，业务封装
 *
 * @author ZXH
 */
public enum ProcessFileEnum {
    INSTANCE;

    private int isPub;
    private long historyId;

    public int getIsPub() {
        return isPub;
    }

    public void setIsPub(int isPub) {
        this.isPub = isPub;
    }


    @SuppressWarnings("unchecked")
    public BaseProcessFileBean createProcessFile(String mark, String title, Map<String, Object> paramMaps, int isPub) {
        this.isPub = isPub;

        FlowBasicInfo baseInfo = (FlowBasicInfo) paramMaps.get("baseInfo");
        ProcessMapper processMapper = (ProcessMapper) paramMaps.get("processMapper");
        final Map<String, Object> map = (Map<String, Object>) paramMaps.get("controlMap");
        this.historyId = Long.valueOf(map.get("historyId").toString());
        List<Map<String, Object>> activityMaps = (List<Map<String, Object>>) paramMaps.get("activityMaps");
        List<Map<String, Object>> roleMaps = (List<Map<String, Object>>) paramMaps.get("roleMaps");
        List<Map<String, Object>> roleRelatedActiveMaps = (List<Map<String, Object>>) paramMaps
                .get("roleRelatedActiveMaps");
        List<Map<String, Object>> activityInputMaps = (List<Map<String, Object>>) paramMaps.get("activityInputMaps");
        List<Map<String, Object>> activityOutputMaps = (List<Map<String, Object>>) paramMaps.get("activityOutputMaps");
        List<Map<String, Object>> activityOperateMaps = (List<Map<String, Object>>) paramMaps.get("activityOperateMaps");

        UserMapper userMapper = (UserMapper) paramMaps.get("userMapper");
        OrgMapper orgMapper = (OrgMapper) paramMaps.get("orgMapper");
        ConfigItemMapper configItemMapper = (ConfigItemMapper) paramMaps.get("configItemMapper");
        ConfigItemPartMapMark mapMark = ConfigItemPartMapMark.valueOf(mark);
        BaseProcessFileBean processFileBean = null;
        switch (mapMark) {
            case a1:// 目的
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowPurpose()));
                break;
            case a2: // 适用范围
                processFileBean = createFileText();
                // 获取 适用部门
                List<String> applyOrgNames = processMapper.findFlowApplyOrgNames(map);
                String orgNames = getApplyOrgNames(applyOrgNames);
                ((ProcessFileText) processFileBean).setContent(orgNames + Common.replaceWarp(baseInfo.getApplicability()));
                break;
            case a3:// 术语定义
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getNoutGlossary()));
                break;
            case a5:// 输入
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getInput()));
                break;
            case a6:
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getOuput()));
                break;
            case a9: // 流程图
                processFileBean = new ProcessFileTable();
                processFileBean.setFileType(ProcessFileType.paragraph);
                break;
            case a16: // 客户
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowCustom()));
                break;
            case a17: // 不适用范围
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getNoApplicability()));
                break;
            case a18: // 概述
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowSummarize()));
                break;
            case a19: // 补充说明
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowSupplement()));
                break;

            case a27: // 自定义要素1
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowCustomOne()));
                break;
            case a28: // 自定义要素2
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowCustomTwo()));
                break;
            case a29: // 自定义要素3
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowCustomThree()));
                break;
            case a30: // 自定义要素4
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowCustomFour()));
                break;
            case a31:// 自定义要素5
                processFileBean = createFileText();
                ((ProcessFileText) processFileBean).setContent(Common.replaceWarp(baseInfo.getFlowCustomFive()));
                break;

            /******************* 【表格开始】 ************************/
            case a4:// 流程驱动规则(纵表)
                // 流程驱动规则时间驱动配置 ConfigItemPartMapMark.isShowDateFlowFile
                ConfigItemBean itemBean = ConfigUtils.getConfig(ConfigItemPartMapMark.isShowDateFlowFile.toString());
                processFileBean = getFlowDriverRules(processMapper, map, baseInfo, "1".equals(itemBean.getValue()));
                break;
            case a8: // 角色/职责(有判断是否存在岗位)
                boolean isShowPost = ConfigUtils.isShowItem(ConfigItemPartMapMark.showPosNameBox);
                if (isShowPost) {
                    List<Map<String, Object>> roleRelatedPostGroups = processMapper.getRoleRelatedPostGroup(map);
                    List<Map<String, Object>> roleRelatedPosts = processMapper.getRoleRelatedPost(map);
                    processFileBean = getRoleInfo(roleMaps, roleRelatedPosts, roleRelatedPostGroups);
                } else {
                    processFileBean = getRoleInfo(roleMaps);
                }
                break;
            case a11: // 流程记录
                processFileBean = getRecordFileShow(activityInputMaps, activityOutputMaps);
                break;
            case a12: // 操作规范
                processFileBean = getOperationFileShow(activityOperateMaps);
                break;
            case a13: // 相关流程
                List<RelatedProcess> relatedProcesses = processMapper.findRelatedProcess(map);
                processFileBean = getRelatedFlows(relatedProcesses);
                break;
            case a14: // 相关制度
                List<Map<String, Object>> relatedRuleMaps = processMapper.getPorcessRelatedRule(map);
                processFileBean = getRelatedRules(relatedRuleMaps);
                break;
            case a15: // 流程关键测评指标板
                List<FlowKpiName> flowKpiNames = findFlowKpiNames(map, processMapper);
                processFileBean = getFlowKpis(flowKpiNames, configItemMapper);
                break;
            case a20: // 记录保存
                List<Map<String, Object>> recordMaps = processMapper.findFlowRecord(map);
                processFileBean = getFlowRecord(recordMaps);
                break;
            case a21: // 相关标准
                List<RelatedStandard> relatedStandards = processMapper.findRelatedStandard(map);
                StandardMapper standardMapper = (StandardMapper) paramMaps.get("standardMapper");
                List<BaseBean> list = getStandardParentNames(standardMapper, relatedStandards);
                processFileBean = getRelatedStandard(relatedStandards, list);
                break;
            case a22: // 流程边界
                processFileBean = getSetStartEndActive(baseInfo);
                break;
            case a23: // 流程责任属性
                processFileBean = getDutyAttr(processMapper, userMapper, orgMapper, map, baseInfo);
                break;
            case a24: // 流程文控信息
                List<Map<String, Object>> maps = processMapper.findDoucumentControl(map);
                processFileBean = getDocumentControl(maps);
                break;
            case a25: // 相关风险
                List<RelatedRisk> relatedRisks = processMapper.findRelatedRisk(map);
                processFileBean = getRelatedRisk(relatedRisks);
                break;
            case a26: // 相关文件
                processFileBean = new ProcessFileTable();
                processFileBean.setFileType(ProcessFileType.tableH);
                break;
            case a32:
                break;/******************* 【特殊节点，默认表格】 ************************/
            case a33:
                processFileBean = getFlowRange(baseInfo);
                break;
            case a34: //术语
                List<TermDefinition> termDefinitions = processMapper.getTermDefines(map);
                processFileBean = getTermDefineTable(termDefinitions);
                break;
            case a35: //流程相关文件
                processFileBean = getRelatedFiles(activityInputMaps, activityOutputMaps);
                break;
            case a36: //流程接口描述
                // 流程接口描述
                List<InterfaceDescription> implDescriptions = processMapper.getInterfaceDescription(map);
                processFileBean = getParentBaseInfo(processMapper, map, implDescriptions);
                break;
            case a10: // 活动说明(默认横表)
                // 获取活动输入输出说明配置 ConfigItemPartMapMark.activityFileShow
                String activityNote = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.showActInOutBox.toString());
                // value = 6:南京石化:添加对应内控矩阵风险点 7对应标准条款
                String loginValue = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.otherLoginType.toString());
                // 活动明细是否段落显示 flowFileType
                String flowFileType = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.flowFileType.toString());
                // IT系统
                List<JecnActiveOnLineBean> activeOnLine = processMapper.findItSystemByActiveId(map);
                Map<Long, List<JecnFigureInoutBase>> inoutMaps = getInoutMap(map, processMapper);
                if ("1".equals(flowFileType)) {// 活动明细段落显示
                    processFileBean = getActiveShowParagraph(activityMaps, roleRelatedActiveMaps, activityInputMaps, activityOutputMaps, "1".equals(activityNote), "6".equals(loginValue), activeOnLine, inoutMaps);
                } else {
                    processFileBean = getActiveShow(activityMaps, roleRelatedActiveMaps, activityInputMaps, activityOutputMaps, "1".equals(activityNote), "6".equals(loginValue), activeOnLine, inoutMaps);
                }
                break;
            case a7: // 关键活动
                // 活动明细是否段落显示 flowFileType
                flowFileType = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.flowFileType.toString());
                if ("1".equals(flowFileType)) {//
                    processFileBean = getKeyActiveParagraphShow(activityMaps);
                } else {
                    processFileBean = getKeyActiveShow(activityMaps);
                }
                break;
            case a37:// 流程标准化文件
                List<StandardizedFile> relatedStandardFiles = processMapper.findRelatedStandardFiles(map);
                processFileBean = getRelatedStandardFiles(relatedStandardFiles);
                break;
            case a38:// IT系统
                List<JecnActiveOnLineBean> jecnActiveOnLine = processMapper.findItSystemByActiveId(map);
                processFileBean = getJecnActiveOnLine(jecnActiveOnLine, activityMaps);
                break;
            case a39:// 新版输入
                Map<String, Object> param = new HashMap<>(map);
                param.put("inoutType", 0);
                List<JecnFlowInoutBean> ins = processMapper.findNewInout(param);
                processFileBean = getInout(ins, 0);
                break;
            case a40:// 新版输出
                param = new HashMap<>(map);
                param.put("inoutType", 1);
                List<JecnFlowInoutBean> outs = processMapper.findNewInout(param);
                processFileBean = getInout(outs, 1);
                break;
            default:
                break;
        }
        if (processFileBean == null) {
            return null;
        }
        processFileBean.setTitle(title);
        return processFileBean;
    }

    private Map<Long, List<JecnFigureInoutBase>> getInoutMap(Map<String, Object> map, ProcessMapper processMapper) {
        if (!ConfigUtils.isUseNewInout()) {
            return new HashMap<>();
        }
        List<JecnFigureInoutBase> list = processMapper.fetchInouts(map);
        return list.stream().collect(Collectors.groupingBy(JecnFigureInoutBase::getFigureId));
    }

    private BaseProcessFileBean getInout(List<JecnFlowInoutBean> inouts, int type) {
        ProcessFileTable processFileTable = getTableH();
        if (inouts.isEmpty()) {
            return processFileTable;
        }
        List<String> contents = new ArrayList<String>();
        contents.add(JecnProperties.getValue("name"));
        if (type == 0) {
            contents.add(JecnProperties.getValue("provider"));
        } else {
            contents.add(JecnProperties.getValue("recipient"));
        }
        contents.add(JecnProperties.getValue("describe"));
        addCellRowTable(processFileTable, contents);
        for (JecnFlowInoutBean inout : inouts) {
            addFourCellRowTable(processFileTable, valueOfString(inout.getName()),
                    getPostGroup(Utils.concat(inout.getPos(), "", JecnFlowInoutPosBean::getName), Utils.concat(inout.getGroups(), "", JecnFlowInoutPosBean::getName), inout.getPosText()),
                    valueOfString(inout.getExplain()), "", "");
        }
        return processFileTable;
    }

    private String getPostGroup(String posName, String postGroupName, String posText) {
        List<String> temp = new ArrayList<>();
        if (StringUtils.isNotBlank(posText)) {
            temp.add(posText);
        }
        if (StringUtils.isNotBlank(posName)) {
            temp.add(posName);
        }
        if (StringUtils.isNotBlank(postGroupName)) {
            temp.add(postGroupName);
        }
        String result = Utils.concat(temp, "<br>").replace("<br><br>", "<br>");
        if (result.endsWith("<br>")) {
            result = result.substring(0, result.length() - "<br>".length());
        }
        return result;
    }

    private BaseProcessFileBean getJecnActiveOnLine(List<JecnActiveOnLineBean> jecnActiveOnLine, List<Map<String, Object>> activityMaps) {
        ProcessFileTable processFileTable = getTableH();
        if (jecnActiveOnLine.isEmpty()) {
            return processFileTable;
        }
        List<String> contents = new ArrayList<String>();
        contents.add(JecnProperties.getValue("activityNumber"));
        contents.add(JecnProperties.getValue("activityNam"));
        contents.add(JecnProperties.getValue("systemName"));
        addCellRowTable(processFileTable, contents);
        for (Map<String, Object> map : activityMaps) {
            String sysName = "";
            Long figId = Long.valueOf(map.get("FIGURE_ID").toString());
            sysName = findsysName(figId, jecnActiveOnLine);
            if (sysName == null) {
                continue;
            }
            addTwoCellRowTable(processFileTable, valueOfString(map.get("ACTIVITY_ID")), valueOfString(map.get("FIGURE_TEXT")), sysName);
        }
        return processFileTable;
    }

    public static String findsysName(Long figId, List<JecnActiveOnLineBean> jecnActiveOnLine) {
        String sysName = null;
        for (JecnActiveOnLineBean ActiveOnLine : jecnActiveOnLine) {
            Long activeId = ActiveOnLine.getActiveId();
            if (!figId.equals(activeId)) {
                continue;
            } else {
                if (sysName == null) {
                    sysName = ActiveOnLine.getSysName();
                } else {
                    sysName += " / " + ActiveOnLine.getSysName();
                }
            }
        }
        return sysName;
    }


    private ProcessFileActivityKeyTable getParentBaseInfo(ProcessMapper processMapper, Map<String, Object> map, List<InterfaceDescription> implDescriptions) {
        FlowStructure flowStructure = null;
        if (historyId != 0) {
            flowStructure = processMapper.getFlowStructureHistory(map);
        } else {
            flowStructure = processMapper.findFlowStructureByMap(map);
        }
        FlowStructure parentFlowStructure = processMapper.getFlowStructure(flowStructure.getPerFlowId());
        return getInterfaceDescription(implDescriptions, parentFlowStructure);
    }


    public ProcessFileText createFileText() {
        ProcessFileText fileText = new ProcessFileText();
        fileText.setFileType(ProcessFileType.text);
        return fileText;
    }

    private BaseProcessFileBean getRelatedStandardFiles(List<StandardizedFile> relatedStandardFiles) {
        ProcessFileTable processFileTable = getTableH();
        if (relatedStandardFiles.isEmpty()) {
            return processFileTable;
        }
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("documentNumber"), JecnProperties.getValue("documentName"), "width:25%", "width:75%");

        List<LinkResource> linkResources = null;
        for (StandardizedFile file : relatedStandardFiles) {
            linkResources = new ArrayList<>();
            linkResources.add(file.getLink(isPub == 1, historyId));
            addTwoCellRowTable(processFileTable, file.getDocId(), linkResources);
        }
        return processFileTable;
    }

    /**
     * 流程KPI数据集
     *
     * @param map
     * @return
     */
    private List<FlowKpiName> findFlowKpiNames(Map<String, Object> map, ProcessMapper processMapper) {
        List<Map<String, Object>> kpiMaps = processMapper.findFlowKpi(map);
        List<Map<String, Object>> toolMaps = processMapper.findFlowTools(map);
        List<FlowKpiName> kpiList = new ArrayList<>();
        FlowKpiName flowKpi = null;
        for (Map<String, Object> obj : kpiMaps) {
            flowKpi = new FlowKpiName();
            flowKpi.setKpiAndId(valueOfLong(obj.get("KPI_AND_ID")));// 主键ID0
            flowKpi.setFlowId(valueOfLong(obj.get("FLOW_ID")));// 流程Id1
            flowKpi.setKpiName(valueOfString(obj.get("KPI_NAME")));// kpi名称2
            flowKpi.setKpiType(valueOfInteger(obj.get("KPI_TYPE")));// kpi类别3
            flowKpi.setKpiDefinition(valueOfString(obj.get("KPI_DEFINITION")));// kpi定义4
            flowKpi.setKpiStatisticalMethods(valueOfString(obj.get("KPI_STATISTICAL_METHODS")));// kpi计算公式5
            flowKpi.setKpiTarget(valueOfString(obj.get("KPI_TARGET")));// 目标值6
            flowKpi.setKpiHorizontal(valueOfString(obj.get("KPI_HORIZONTAL")));// 数据统计时间\频率7
            flowKpi.setKpiVertical(valueOfString(obj.get("KPI_VERTICAL")));// kpi值单位名称8
            flowKpi.setCreatTime(Common.getDateByString(valueOfString(obj.get("CREAT_TIME"))));// 创建时间9
            flowKpi.setKpiTargetOperator(valueOfInteger(obj.get("KPI_TARGET_OPERATOR")));// kpi目标值比较符号10
            flowKpi.setKpiDataMethod(valueOfInteger(obj.get("KPI_DATA_METHOD")));// kpi数据获取方式11
            flowKpi.setKpiDataPeopleId(valueOfLong(obj.get("KPI_DATA_PEOPEL_ID")));// 数据提供者ID
            flowKpi.setKpiRelevance(valueOfInteger(obj.get("KPI_RELEVANCE")));// 相关度14
            flowKpi.setKpiTargetType(valueOfInteger(obj.get("KPI_TARGET_TYPE")));// 指标来源15
            flowKpi.setFirstTargetId(valueOfString(obj.get("FIRST_TARGER_ID")));// 支撑的一级指标ID16
            flowKpi.setUpdateTime(Common.getDateByString(valueOfString(obj.get("UPDATE_TIME"))));// 更新时间17
            flowKpi.setCreatePeopleId(valueOfLong(obj.get("CREATE_PEOPLE_ID")));// 创建人ID
            flowKpi.setUpdatePeopleId(valueOfLong(obj.get("UPDATE_PEOPLE_ID")));// 更新人Id
            flowKpi.setKpiDataPeopleName(valueOfString(obj.get("TRUE_NAME")));// 数据提供者名称
            flowKpi.setFirstTargetContent(valueOfString(obj.get("TARGER_CONTENT")));// 支撑的一级指标名称21
            flowKpi.setKpiPurpose(valueOfString(obj.get("KPI_PURPOSE")));
            flowKpi.setKpiPoint(valueOfString(obj.get("KPI_POINT")));
            flowKpi.setKpiExplain(valueOfString(obj.get("KPI_EXPLAIN")));
            flowKpi.setKpiPeriod(valueOfString(obj.get("KPI_PERIOD")));
            // IT系统名称
            flowKpi.setKpiITSystemNames(getToolNamesByID(toolMaps, flowKpi.getKpiAndId()));
            kpiList.add(flowKpi);
        }
        return kpiList;
    }

    private String getToolNamesByID(List<Map<String, Object>> toolMaps, Long kpiId) {
        if (toolMaps == null || kpiId == null) {
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        // KPI_AND_ID, ST.FLOW_SUSTAIN_TOOL_DESCRIBE
        for (Map<String, Object> map : toolMaps) {
            if (kpiId.toString().equals(map.get("KPI_AND_ID").toString())) {
                if (buffer.length() == 0) {
                    buffer.append(map.get("FLOW_SUSTAIN_TOOL_DESCRIBE"));
                } else {
                    buffer.append("/").append(map.get("FLOW_SUSTAIN_TOOL_DESCRIBE"));
                }
            }
        }
        return buffer.toString();
    }

    /**
     * 流程驱动规则
     *
     * @param map
     * @param baseInfo
     */
    private ProcessFileTable getFlowDriverRules(ProcessMapper processMapper, Map<String, Object> map,
                                                FlowBasicInfo baseInfo, boolean isShowDateFlowFile) {
        ProcessFileTable processFileTable = getTableV();
        if (baseInfo.getDriveType() == null) {
            return processFileTable;
        }
        // 添加第一行
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("driveType"), getDriverTypeString(baseInfo.getDriveType().intValue()));
        // 第二行
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("drivingRules"), Common.replaceWarp(baseInfo.getDriveRules()));

        if (isShowDateFlowFile) {// 时间驱动
            FlowDriver flowDriver = getFlowDriver(map, processMapper.getFlowDriver(map));
            if (ConfigUtils.isUseNewDriver()) {
                StringBuilder b = new StringBuilder();
                concatDriverTime(flowDriver.getTimes(), b, flowDriver.getQuantity());
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("frequency"), flowDriver.getFrequency());
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("time"), b.toString());
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("type"), getTimeDriverType(flowDriver.getQuantity()));
            } else {
                // 第三行
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("frequency"), flowDriver.getFrequency());
                // 第四行
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("startTime"), flowDriver.getStartTime());
                // 第五行
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("endTime"), flowDriver.getEndTime());
                // 第六行
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("type"), getTimeDriverType(flowDriver.getQuantity()));
            }

            //查询提醒人
            List<String> trName = processMapper.findTrname(map);
            String name = "";
            if (ConfigUtils.isIflytekLogin()) {
                name = IfytekCommon.iflytekCommonLisStrByLoginName(trName).toString();
            } else {
                name = trName.toString();
            }
            addTwoCellRowTable(processFileTable, JecnProperties.getValue("remindingPeople"), name.substring(1, name.length() - 1));
        }
        return processFileTable;
    }

    private void concatDriverTime(List<? extends JecnFlowDriverTimeBase> times, StringBuilder b, String q) {
        if (Utils.isEmpty(times)) {
            return;
        }
        String[] week = new String[]{"", JecnProperties.getValue("monday"), JecnProperties.getValue("tuesday"),
                JecnProperties.getValue("wednesday"), JecnProperties.getValue("thursday"),
                JecnProperties.getValue("friday"), JecnProperties.getValue("saturday"), JecnProperties.getValue("sunday")};
        /** 时间类型：1:日，2:周，3:月，4:季，5:年 */
        for (JecnFlowDriverTimeBase time : times) {
            if ("1".equals(q)) {
                b.append(time.getHour());
                b.append(JecnProperties.getValue("shi"));
            } else if ("2".equals(q)) {
                b.append(week[time.getWeek()]);
                b.append(time.getHour());
                b.append(JecnProperties.getValue("shi"));
            } else if ("3".equals(q)) {
                b.append(time.getDay());
                b.append(JecnProperties.getValue("ri"));
                b.append(time.getHour());
                b.append(JecnProperties.getValue("shi"));
            } else if ("4".equals(q)) {
                b.append(JecnProperties.getValue("next"));
                b.append(time.getMonth());
                b.append(JecnProperties.getValue("month"));
                b.append(time.getDay());
                b.append(JecnProperties.getValue("ri"));
                b.append(time.getHour());
                b.append(JecnProperties.getValue("shi"));
            } else if ("5".equals(q)) {
                b.append(time.getMonth());
                b.append(JecnProperties.getValue("month"));
                b.append(time.getDay());
                b.append(JecnProperties.getValue("ri"));
                b.append(time.getHour());
                b.append(JecnProperties.getValue("shi"));
            }
            b.append("<br>");
        }
    }

    /**
     * 流程范围
     *
     * @param baseInfo
     */
    private ProcessFileTable getFlowRange(FlowBasicInfo baseInfo) {
        ProcessFileTable processFileTable = getTableV();
        if (baseInfo == null) {
            return processFileTable;
        }
        // 添加第一行
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("startingActivity"), Common.replaceWarp(baseInfo.getFlowStartpoint()));
        // 第二行
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("endActivity"), Common.replaceWarp(baseInfo.getFlowEndpoint()));
        // 第二行
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("input"), Common.replaceWarp(baseInfo.getInput()));
        // 第二行
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("out"), Common.replaceWarp(baseInfo.getOuput()));

        return processFileTable;
    }

    private ProcessFileTable getTermDefineTable(List<TermDefinition> termDefinitions) {
        ProcessFileTable processFileTable = getTableH();
        if (termDefinitions == null) {
            return processFileTable;
        }
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("name"), JecnProperties.getValue("definition"), "width:25%", "width:75%");

        for (TermDefinition termDefinition : termDefinitions) {
            addTwoCellRowTable(processFileTable, termDefinition.getName(), Common.replaceWarp(termDefinition.getInstructions()));
        }
        return processFileTable;
    }

    /**
     * 流程边界
     *
     * @param baseInfo
     * @return
     */
    private ProcessFileTable getSetStartEndActive(FlowBasicInfo baseInfo) {
        ProcessFileTable processFileTable = getTableV();
        if (baseInfo == null) {
            return processFileTable;
        }
        // 起始活动
        String startActivity = baseInfo.getFlowStartpoint();
        // 终止活动
        String endActivity = baseInfo.getFlowEndpoint();
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("startingActivity"), startActivity);
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("endActivity"), endActivity);
        return processFileTable;
    }

    /**
     * 流程属性
     *
     * @param processMapper
     * @param userMapper
     * @param orgMapper
     * @param map
     * @param baseInfo
     * @return
     */
    private ProcessFileTable getDutyAttr(ProcessMapper processMapper, UserMapper userMapper, OrgMapper orgMapper,
                                         Map<String, Object> map, FlowBasicInfo baseInfo) {
        ProcessFileTable processFileTable = getTableV();
        if (baseInfo == null) {
            return processFileTable;
        }
        // 1、 监护人
        if (ConfigUtils.valueIsOneThenTrue(ConfigItemPartMapMark.guardianPeople.toString())) { // 流程监护人
            addTwoCellRowTable(processFileTable, JecnProperties.getValue("processGuardian"), getPersonName(baseInfo.getGuardianId(), userMapper));
        }
        // 2、流程责任人
        if (baseInfo.getTypeResPeople() != null) {
            if (baseInfo.getTypeResPeople() == 0) {
                // 责任人
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("processResponsiblePerson"), getPersonName(baseInfo.getResPeopleId(), userMapper));
            } else {
                // 责任岗位
                addTwoCellRowTable(processFileTable, JecnProperties.getValue("processResponsiblePerson"), getPostName(baseInfo.getResPeopleId(), orgMapper));
            }
        }

        // 3、流程拟制人
        if (ConfigUtils.valueIsOneThenTrue(ConfigItemPartMapMark.fictionPeople.toString())) {
            addTwoCellRowTable(processFileTable, JecnProperties.getValue("processDrafting"), getPersonName(baseInfo.getFictionPeopleId(), userMapper));
        }
        // 4、 key:ORG_ID, ORG_NAME
        Map<String, Object> dutyOrgMap = processMapper.getProcessDutyOrg(map);
        if (dutyOrgMap != null) {
            addTwoCellRowTable(processFileTable, JecnProperties.getValue("processResponsibilityDepartment"),
                    dutyOrgMap.get("ORG_NAME") == null ? null : dutyOrgMap.get("ORG_NAME").toString());
        }
        return processFileTable;
    }

    /**
     * 活动说明
     *
     * @param listActiveShow        活动信息集合
     * @param roleRelatedActiveMaps 活动，角色关联关系集合
     * @param listInputFiles
     * @param listOutFiles
     * @param isShowInOutFile       :true  显示输入输出
     * @param addNJSHColumn         南京石化 添加(对应内控矩阵风险点 ;对应标准条款)
     * @param jecnActiveOnLine
     * @param inoutMaps
     * @return
     */
    private ProcessFileTable getActiveShow(List<Map<String, Object>> listActiveShow,
                                           List<Map<String, Object>> roleRelatedActiveMaps, List<Map<String, Object>> listInputFiles,
                                           List<Map<String, Object>> listOutFiles, boolean isShowInOutFile, boolean addNJSHColumn, List<JecnActiveOnLineBean> jecnActiveOnLine, Map<Long, List<JecnFigureInoutBase>> inoutMaps) {
        ProcessFileTable processFileTable = getTableH();
        if (listActiveShow.isEmpty()) {
            return processFileTable;
        }
        List<String> contents = new ArrayList<>();
        contents.add(JecnProperties.getValue("activityNumber"));
        contents.add(JecnProperties.getValue("executionRole"));
        contents.add(JecnProperties.getValue("activityNam"));
        contents.add(JecnProperties.getValue("activityDescription"));

        if (isShowInOutFile) {
            contents.add(JecnProperties.getValue("input"));
            contents.add(JecnProperties.getValue("out"));
            if (addNJSHColumn) {
                addNJSHColumn(contents);
            }
        } else {
            if (addNJSHColumn) {
                addNJSHColumn(contents);
            }
        }
        if (ConfigUtils.showActTimeLine()) {
            contents.add(JecnProperties.getValue("timeLimit") + "<br>(" + JecnProperties.getValue("statusTanget") + ConfigUtils.getValue(ConfigItemPartMapMark.activeTimeLineUtil) + ")");
        }
        if (ConfigUtils.processFileActivityShowIT()) {
            contents.add(JecnProperties.getValue("itSystem"));
        }
        addCellRowTable(processFileTable, contents);
        // 活动排序
        getActivityObjSort(listActiveShow);
        // 活动明细key: FIGURE_ID,
        // T.ACTIVITY_ID,T.FIGURE_TEXT,T.ACTIVITY_SHOW,T.LINECOLOR,
        // T.ROLE_RES,T.INNER_CONTROL_RISK,T.STANDARD_CONDITIONS,ACTIVITY_INPUT,ACTIVITY_OUTPUT

        Map<Long, String> activityToItMap = createActivityToItMap(jecnActiveOnLine);

        for (Map<String, Object> objActives : listActiveShow) {
            if (objActives.get("FIGURE_ID") == null) {
                continue;
            }
            Long figureId = valueOfLong(objActives.get("FIGURE_ID"));
            // 1、活动编号
            // 2、 角色职责
            String roleName = getActivityRelatedRoleName(roleRelatedActiveMaps, figureId);
            // 3、活动名称
            // 4、活动说明
            // 5、输入
            // 6、输出
            // 7、信息化（IT系统）
            List<JecnFigureInoutBase> inouts = inoutMaps.get(figureId);
            addActivityInfoRowTableH(processFileTable, objActives, roleName, listInputFiles, listOutFiles,
                    addNJSHColumn, isShowInOutFile, activityToItMap, inouts);

        }
        return processFileTable;
    }

    private Map<Long, String> createActivityToItMap(List<JecnActiveOnLineBean> jecnActiveOnLine) {
        Map<Long, String> result = new HashMap<>();
        if (jecnActiveOnLine == null || jecnActiveOnLine.size() == 0) {
            return result;
        }

        Map<Long, List<JecnActiveOnLineBean>> m1 = jecnActiveOnLine.stream().collect(Collectors.groupingBy(JecnActiveOnLineBean::getActiveId));
        for (Map.Entry<Long, List<JecnActiveOnLineBean>> entry : m1.entrySet()) {
            result.put(entry.getKey(), entry.getValue().stream().map(JecnActiveOnLineBean::getSysName).collect(Collectors.joining("/")));
        }
        return result;
    }

    /**
     * 活动说明（段落）
     *
     * @param listActiveShow
     * @param roleRelatedActiveMaps
     * @param listInputFiles
     * @param listOutFiles
     * @param isShowInOutFile
     * @param addNJSHColumn
     * @param jecnActiveOnLine
     * @param inoutMaps
     * @return
     */
    private ProcessFileActivityKeyTable getActiveShowParagraph(List<Map<String, Object>> listActiveShow,
                                                               List<Map<String, Object>> roleRelatedActiveMaps, List<Map<String, Object>> listInputFiles,
                                                               List<Map<String, Object>> listOutFiles, boolean isShowInOutFile, boolean addNJSHColumn, List<JecnActiveOnLineBean> jecnActiveOnLine, Map<Long, List<JecnFigureInoutBase>> inoutMaps) {
        ProcessFileActivityKeyTable processFileTable = new ProcessFileActivityKeyTable();
        if (listActiveShow.isEmpty()) {
            return processFileTable;
        }

        List<ProcessFileTable> fileTables = new ArrayList<ProcessFileTable>();
        // 活动排序
        getActivityObjSort(listActiveShow);
        // 活动明细key: FIGURE_ID,
        // T.ACTIVITY_ID,T.FIGURE_TEXT,T.ACTIVITY_SHOW,T.LINECOLOR,
        // T.ROLE_RES,T.INNER_CONTROL_RISK,T.STANDARD_CONDITIONS,ACTIVITY_INPUT,ACTIVITY_OUTPUT

        ProcessFileTable fileTable = null;
        for (Map<String, Object> objActives : listActiveShow) {
            if (objActives.get("FIGURE_ID") == null) {
                continue;
            }
            Long figureId = valueOfLong(objActives.get("FIGURE_ID"));
            // 1、活动编号
            // 2、 角色职责
            String roleName = getActivityRelatedRoleName(roleRelatedActiveMaps, figureId);
            // 3、活动名称
            // 4、活动说明

            if (isShowInOutFile) {// 显示输入输出
                // 5、输入
                // 6、输出
                fileTable = getActivityParagraphInOutput(objActives, listInputFiles, listOutFiles, roleName,
                        addNJSHColumn);
            } else {
                fileTable = getActivityParagraphInOutput(objActives, null, null, roleName, addNJSHColumn);
            }
            fileTables.add(fileTable);
        }
        processFileTable.setContents(fileTables);
        return processFileTable;
    }

    /**
     * 活动明细，单个活动段落数据处理
     *
     * @param objActives
     * @param roleName
     * @return
     */
    private ProcessFileTable getActivityParagraph(Map<String, Object> objActives, String roleName) {
        ProcessFileTable processFileTable = new ProcessFileTable();
        processFileTable.setFileType(ProcessFileType.paragraph);
        // 1、编号和活动
        addTwoCellRowTable(processFileTable, valueOfString(objActives.get("ACTIVITY_ID")),
                getResourceLinks(valueOfString(objActives.get("FIGURE_ID")), valueOfString(objActives.get("FIGURE_TEXT")), ResourceType.ACTIVITY));
        // 2、执行角色
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("executionRoleC"), roleName);
        // 3、活动说明
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("activityDescriptionC"), getStringByClob((Clob) objActives.get("ACTIVITY_SHOW")));
        return processFileTable;
    }

    public String getStringByClob(Clob clob) {
        return replaceWarp(Common.valueStringByClob(clob));
    }

    private ProcessFileTable getActivityParagraphInOutput(Map<String, Object> objActives,
                                                          List<Map<String, Object>> listInputFiles, List<Map<String, Object>> listOutFiles, String roleName,
                                                          boolean addNJSHCloumn) {
        ProcessFileTable processFileTable = getActivityParagraph(objActives, roleName);

        if (listInputFiles != null && listOutFiles != null) {
            // 4、输入
            Map<Long, String> inputMap = getActivityInputMaps(valueOfLong(objActives.get("FIGURE_ID")), listInputFiles);
            addTwoCellRowActivityInoutPutTable(processFileTable, JecnProperties.getValue("inputC"), valueOfString(inputMap.get("ACTIVITY_INPUT")),
                    getFileLinkResoucres(inputMap));
            // 5、输出
            Map<Long, String> outputMap = getActivityOutputMaps(valueOfLong(objActives.get("FIGURE_ID")), listOutFiles);
            addTwoCellRowActivityInoutPutTable(processFileTable, JecnProperties.getValue("outC"), valueOfString(inputMap.get("ACTIVITY_OUTPUT")),
                    getFileLinkResoucres(outputMap));
        }

        if (addNJSHCloumn) {// 南京石化
            addTwoCellRowTable(processFileTable, JecnProperties.getValue("correspondingInternalControlC"), valueOfString(objActives.get("INNER_CONTROL_RISK")));
            addTwoCellRowTable(processFileTable, JecnProperties.getValue("correspondingStandardTermsC"), valueOfString(objActives.get("STANDARD_CONDITIONS")));
        }
        return processFileTable;
    }

    private void addTwoCellRowActivityInoutPutTable(ProcessFileTable processFileTable, String content1, String content2,
                                                    List<LinkResource> linkResources2) {
        List<FileTableCell> cells = new ArrayList<FileTableCell>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content2);
        tableCell.setLinks(linkResources2);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    private void addNJSHColumn(List<String> contents) {
        contents.add(JecnProperties.getValue("correspondingInternalControl"));
        contents.add(JecnProperties.getValue("correspondingStandardTerms"));
    }

    /**
     * 关键活动表格
     *
     * @param listActiveShow
     * @return
     */
    private ProcessFileActivityKeyTable getKeyActiveShow(List<Map<String, Object>> listActiveShow) {
        ProcessFileActivityKeyTable activityKeyTable = new ProcessFileActivityKeyTable();
        if (listActiveShow.isEmpty()) {
            return activityKeyTable;
        }
        // 活动排序
        getActivityObjSort(listActiveShow);
        //// 活动明细key: FIGURE_ID,
        // T.ACTIVITY_ID,T.FIGURE_TEXT,T.ACTIVITY_SHOW,T.LINECOLOR,
        // T.ROLE_RES,T.INNER_CONTROL_RISK,T.STANDARD_CONDITIONS

        List<Map<String, Object>> paMaps = new ArrayList<>();
        List<Map<String, Object>> ksfMaps = new ArrayList<>();
        List<Map<String, Object>> kcpMaps = new ArrayList<>();
        List<Map<String, Object>> kcpAllowMaps = new ArrayList<>();
        boolean kcpAllowEnabel = ConfigUtils.elementEnable("KCPFigureComp");
        for (Map<String, Object> map : listActiveShow) {
            if (map.get("FIGURE_ID") == null) {
                continue;
            }
            // 关键活动类型
            if (map.get("LINECOLOR") != null) {
                String keyValue = map.get("LINECOLOR").toString();
                // 关键活动类型：1为PA,2为KSF,3为KCP
                if ("1".equals(keyValue)) {
                    paMaps.add(map);
                } else if ("2".equals(keyValue)) {
                    ksfMaps.add(map);
                } else if ("3".equals(keyValue)) {
                    kcpMaps.add(map);
                } else if ("4".equals(keyValue)) {
                    kcpAllowMaps.add(map);
                }
            }
        }
        List<ProcessFileTable> fileTables = new ArrayList<>();
        ProcessFileTable paTable = getActivityKeyTable(paMaps, 1);
        ProcessFileTable ksfTable = getActivityKeyTable(ksfMaps, 2);
        ProcessFileTable kcpTable = getActivityKeyTable(kcpMaps, 3);
        ProcessFileTable kcpAllowTable = getActivityKeyTable(kcpAllowMaps, 4);
        if (paTable != null) {
            paTable.setTitle(JecnProperties.getValue("problemArea"));
            fileTables.add(paTable);
        }
        if (ksfTable != null) {
            ksfTable.setTitle(JecnProperties.getValue("keySuccessFactors"));
            fileTables.add(ksfTable);
        }
        if (kcpTable != null) {
            kcpTable.setTitle(JecnProperties.getValue("criticalControlPoint"));
            fileTables.add(kcpTable);
        }
        if (kcpAllowTable != null && kcpAllowEnabel) {
            kcpAllowTable.setTitle(JecnProperties.getValue("keyControlPointAllow"));
            fileTables.add(kcpAllowTable);
        }
        activityKeyTable.setContents(fileTables);
        activityKeyTable.setFileType(ProcessFileType.listTables);
        return activityKeyTable;
    }

    /**
     * 关键活动表格
     *
     * @param implDescriptions
     * @param parentFlow
     * @return
     */
    private ProcessFileActivityKeyTable getInterfaceDescription(List<InterfaceDescription> implDescriptions, FlowStructure parentFlow) {
        ProcessFileActivityKeyTable implTables = new ProcessFileActivityKeyTable();
        if (implDescriptions.isEmpty()) {
            return implTables;
        }

        List<InterfaceDescription> inputFlows = new ArrayList<>();
        List<InterfaceDescription> outputFlows = new ArrayList<>();
        // 过程接口流程
        List<InterfaceDescription> proceduralFlows = new ArrayList<>();
        List<InterfaceDescription> childFlows = new ArrayList<>();

        //1是上游流程,2是下游流程,3是过程流程,4是子流程
        for (InterfaceDescription impl : implDescriptions) {
            if (impl.getImplType() == 1) {
                inputFlows.add(impl);
            } else if (impl.getImplType() == 2) {
                outputFlows.add(impl);
            } else if (impl.getImplType() == 3) {
                proceduralFlows.add(impl);
            } else if (impl.getImplType() == 4) {
                childFlows.add(impl);
            }
        }

        List<ProcessFileTable> fileTables = new ArrayList<>();
        ProcessFileTable parentFlowTable = getParentFlowTable(parentFlow);
        ProcessFileTable inputTable = getInterfaceDescriptionTable(inputFlows, 1);
        ProcessFileTable outputTable = getInterfaceDescriptionTable(outputFlows, 2);
        ProcessFileTable proceduralTable = getInterfaceDescriptionTable(proceduralFlows, 3);
        ProcessFileTable childTable = getInterfaceDescriptionTable(childFlows, 4);

        if (parentFlowTable != null) {
            parentFlowTable.setTitle(JecnProperties.getValue("theCorrespondingUpperProcess"));
            fileTables.add(parentFlowTable);
        }
        if (inputTable != null) {
            inputTable.setTitle(JecnProperties.getValue("inputProcessInterfaceDescription"));
            fileTables.add(inputTable);
        }
        if (outputTable != null) {
            outputTable.setTitle(JecnProperties.getValue("outputProcessInterfaceDescription"));
            fileTables.add(outputTable);
        }
        if (proceduralTable != null) {
            proceduralTable.setTitle(JecnProperties.getValue("processInterfaceProcess"));
            fileTables.add(proceduralTable);
        }

        if (childTable != null) {
            childTable.setTitle(JecnProperties.getValue("subProcess"));
            fileTables.add(childTable);
        }
        implTables.setContents(fileTables);
        implTables.setFileType(ProcessFileType.listTables);
        return implTables;
    }


    /**
     * 接口描述-上级流程
     *
     * @param parentFlow
     * @return
     */
    private ProcessFileTable getParentFlowTable(FlowStructure parentFlow) {
        if (parentFlow == null) {
            return null;
        }
        ProcessFileTable processFileTable = getTableH();
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("processFileName"), JecnProperties.getValue("fileEncoding"), "width:75%", "width:25%");

        List<LinkResource> linkResources = new ArrayList<>();
        LinkResource linkResource = new LinkResource(parentFlow.getFlowId().toString(), parentFlow.getFlowName(), ResourceType.PROCESS);
        linkResource.setIsPublic(isPub);
        linkResources.add(linkResource);

        addTwoCellRowTable(processFileTable, linkResources, parentFlow.getFlowIdInput());
        return processFileTable;
    }


    /**
     * 接口描述表格
     *
     * @param implList
     * @return
     */
    private ProcessFileTable getInterfaceDescriptionTable(List<InterfaceDescription> implList, int type) {
        if (implList.isEmpty()) {
            return null;
        }
        ProcessFileTable processFileTable = getTableH();
        addInterfaceDescriptionTitle(processFileTable, type);

        // 添加标题
        List<FileTableCell> cells = null;
        for (InterfaceDescription impl : implList) {
            cells = new ArrayList<FileTableCell>();
            // 接口名称
            FileTableCell tableCell = processFileTable.getTableCell();
            tableCell.setText(impl.getImplName());
            cells.add(tableCell);

            // 关联流程名称
            tableCell = processFileTable.getTableCell();
            tableCell.setText(null);
            LinkResource linkResource = new LinkResource(impl.getRid().toString(), impl.getRname(),
                    ResourceType.PROCESS);
            linkResource.setIsPublic(isPub);
            List<LinkResource> linkResources = new ArrayList<>();
            linkResources.add(linkResource);
            tableCell.setLinks(linkResources);
            cells.add(tableCell);

            // 流程要求
            tableCell = processFileTable.getTableCell();
            tableCell.setText(Common.replaceWarp(impl.getProcessRequirements()));
            cells.add(tableCell);

            // 备注
            tableCell = processFileTable.getTableCell();
            tableCell.setText(Common.replaceWarp(impl.getImplNote()));
            cells.add(tableCell);

            processFileTable.getContents().add(cells);
        }
        return processFileTable;
    }

    private void addInterfaceDescriptionTitle(ProcessFileTable processFileTable, int type) {
        // 添加标题
        List<FileTableCell> cells = new ArrayList<>();

        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(JecnProperties.getValue("processInterface"));
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(getImplDescTitle(type));
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(JecnProperties.getValue("processRequirements"));
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(JecnProperties.getValue("remarks"));
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    private String getImplDescTitle(int type) {
        String title = "";
        switch (type) {
            case 1:
                title = JecnProperties.getValue("upstreamProcess");
                break;
            case 2:
                title = JecnProperties.getValue("downstreamProcess");
                break;
            case 3:
                title = JecnProperties.getValue("processInterfaceProcess");
                break;
            case 4:
                title = JecnProperties.getValue("subProcess");
                break;
        }
        return title;
    }

    /**
     * 活动说明段落显示
     *
     * @param listActiveShow
     * @return
     */
    private ProcessFileActivityKeyParagraph getKeyActiveParagraphShow(List<Map<String, Object>> listActiveShow) {
        ProcessFileActivityKeyParagraph activityKeyTable = new ProcessFileActivityKeyParagraph();
        if (listActiveShow.isEmpty()) {
            return activityKeyTable;
        }
        // 活动排序
        getActivityObjSort(listActiveShow);
        //// 活动明细key: FIGURE_ID,
        // T.ACTIVITY_ID,T.FIGURE_TEXT,T.ACTIVITY_SHOW,T.LINECOLOR,
        // T.ROLE_RES,T.INNER_CONTROL_RISK,T.STANDARD_CONDITIONS

        List<Map<String, Object>> paMaps = new ArrayList<>();
        List<Map<String, Object>> ksfMaps = new ArrayList<>();
        List<Map<String, Object>> kcpMaps = new ArrayList<>();

        for (Map<String, Object> map : listActiveShow) {
            if (map.get("FIGURE_ID") == null) {
                continue;
            }
            // 关键活动类型
            if (map.get("LINECOLOR") != null) {
                String keyValue = map.get("LINECOLOR").toString();
                // 关键活动类型：1为PA,2为KSF,3为KCP
                if ("1".equals(keyValue)) {
                    paMaps.add(map);
                } else if ("2".equals(keyValue)) {
                    ksfMaps.add(map);
                } else if ("3".equals(keyValue)) {
                    kcpMaps.add(map);
                }
            }
        }

        // activityKeyTable.setContents(new ArrayList<ProcessFileTable>());
        ProcessFileActivityKeyTable paKeyTable = new ProcessFileActivityKeyTable();
        ProcessFileActivityKeyTable ksfKeyTable = new ProcessFileActivityKeyTable();
        ProcessFileActivityKeyTable kcpKeyTable = new ProcessFileActivityKeyTable();
        List<ProcessFileTable> paTables = getActivityKeyParagraph(paMaps, 1);
        List<ProcessFileTable> ksfTables = getActivityKeyParagraph(ksfMaps, 2);
        List<ProcessFileTable> kcpTables = getActivityKeyParagraph(kcpMaps, 3);
        if (paTables != null) {
            paKeyTable.setContents(paTables);
            paKeyTable.setFileType(ProcessFileType.paragraph);
            paKeyTable.setTitle(JecnProperties.getValue("problemArea"));
            activityKeyTable.getContents().add(paKeyTable);
        }
        if (ksfTables != null) {
            ksfKeyTable.setContents(ksfTables);
            ksfKeyTable.setFileType(ProcessFileType.paragraph);
            paKeyTable.setTitle(JecnProperties.getValue("keySuccessFactors"));
            activityKeyTable.getContents().add(ksfKeyTable);
        }
        if (kcpTables != null) {
            kcpKeyTable.setContents(kcpTables);
            kcpKeyTable.setFileType(ProcessFileType.paragraph);
            paKeyTable.setTitle(JecnProperties.getValue("criticalControlPoint"));
            activityKeyTable.getContents().add(kcpKeyTable);
        }
        activityKeyTable.setFileType(ProcessFileType.listParagraphs);

        return activityKeyTable;
    }

    /**
     * 相关角色不包含岗位
     *
     * @param roleMaps
     * @return
     */
    private ProcessFileTable getRoleInfo(List<Map<String, Object>> roleMaps) {
        ProcessFileTable processFileTable = getTableH();
        if (roleMaps.isEmpty()) {
            return processFileTable;
        }
        // 添加标题
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("roleName"), JecnProperties.getValue("roleResponsibility"), "width:25%", "width:75%");
        for (Map<String, Object> map : roleMaps) {
            addTwoCellRowTable(processFileTable, valueOfString(map.get("FIGURE_TEXT")),
                    valueOfString(map.get("ROLE_RES")));
        }
        return processFileTable;
    }

    /**
     * 角色职责
     *
     * @param roleMaps
     * @param roleRelatedPosts
     * @param roleRelatedPostGroups
     * @return
     */
    private ProcessFileTable getRoleInfo(List<Map<String, Object>> roleMaps, List<Map<String, Object>> roleRelatedPosts,
                                         List<Map<String, Object>> roleRelatedPostGroups) {
        ProcessFileTable processFileTable = getTableH();
        if (roleMaps.isEmpty()) {
            return processFileTable;
        }
        // 添加标题
        addRoleAdnContainsPostableTitle(processFileTable, JecnProperties.getValue("roleName"), JecnProperties.getValue("postName"),
                JecnProperties.getValue("roleResponsibility"), "width:20%", "width:20%", "width:60%");
        for (Map<String, Object> map : roleMaps) {
            addRoleAdnContainsPostableRows(processFileTable,
                    valueOfString(map.get("FIGURE_TEXT")),
                    getRoleRelatedPostMap(roleRelatedPosts, roleRelatedPostGroups, valueOfLong(map.get("FIGURE_ID"))),
                    Common.replaceWarp(valueOfString(map.get("ROLE_RES")))
            );
        }
        return processFileTable;
    }

    /**
     * 相关流程
     *
     * @param relatedProcesses
     * @return
     */
    private ProcessFileTable getRelatedFlows(List<RelatedProcess> relatedProcesses) {
        ProcessFileTable processFileTable = getTableVH();
        if (relatedProcesses.isEmpty()) {
            return processFileTable;
        }
        // 按流程类别分组
        Map<String, List<RelatedProcess>> relatedProcessMap = new LinkedHashMap<>();
        for (RelatedProcess relatedProcess : relatedProcesses) {
            if (!relatedProcessMap.containsKey(relatedProcess.getTypeName())) {
                relatedProcessMap.put(relatedProcess.getTypeName(), new ArrayList<>());
            }
            relatedProcessMap.get(relatedProcess.getTypeName()).add(relatedProcess);
        }
        addTitleCellRelatedFlowTable(processFileTable, JecnProperties.getValue("type"), JecnProperties.getValue("processNum"), JecnProperties.getValue("processName"));

        LinkResource linkResource = null;
        List<LinkResource> links = null;
        List<String> texts = null;
        for (Entry<String, List<RelatedProcess>> entry : relatedProcessMap.entrySet()) {
            links = new ArrayList<>();
            texts = new ArrayList<>();
            for (RelatedProcess relatedProcess : entry.getValue()) {
                linkResource = new LinkResource(relatedProcess.getId().toString(), relatedProcess.getName(),
                        ResourceType.PROCESS, TreeNodeUtils.NodeType.process);
                linkResource.setIsPublic(isPub);
                links.add(linkResource);
                texts.add(relatedProcess.getFlowCode());
            }
            addRowsCellRelatedFlowTable(processFileTable, entry.getKey(), texts, links);
        }
        return processFileTable;
    }

    /**
     * 流程相关制度
     *
     * @param relatedRuleMaps key:JR.ID, JR.RULE_NAME, JR.RULE_NUMBER,IS_DIR
     * @return
     */
    private ProcessFileTable getRelatedRules(List<Map<String, Object>> relatedRuleMaps) {
        ProcessFileTable processFileTable = getTableH();
        if (relatedRuleMaps.isEmpty()) {
            return processFileTable;
        }
        List<Map<String, String>> contents = new ArrayList<>();
//        contents.add("制度编号");
//        contents.add("制度名称");
//        contents.add("类型");
        Map m = new HashMap();
        m.put("text", JecnProperties.getValue("ruleNum"));
        m.put("style", "width:25%");
        contents.add(m);
        m = new HashMap();
        m.put("text", JecnProperties.getValue("ruleName"));
        m.put("style", "width:75%");
        contents.add(m);

        addCellRowTableStyle(processFileTable, contents);
        List<LinkResource> linkResources = null;
        LinkResource linkResource = null;
        for (Map<String, Object> map : relatedRuleMaps) {
            linkResources = new ArrayList<>();
            linkResource = new LinkResource(valueOfString(map.get("ID")), valueOfString(map.get("RULE_NAME")), ResourceType.RULE, TreeNodeUtils.NodeType.process);
            linkResource.setIsPublic(isPub);
            linkResources.add(linkResource);
            addTwoCellRowTable(processFileTable, valueOfString(map.get("RULE_NUMBER")), linkResources);
        }
        return processFileTable;
    }

    /**
     * 流程相关标准
     *
     * @param relatedStandards
     * @return
     */
    private ProcessFileTable getRelatedStandard(List<RelatedStandard> relatedStandards, List<BaseBean> list) {
        ProcessFileTable processFileTable = getTableH();
        if (relatedStandards.isEmpty()) {
            return processFileTable;
        }
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("standardName"), JecnProperties.getValue("dir"), "width:25%;", "width:75%;");
        List<LinkResource> linkResources = null;
        LinkResource linkResource = null;

        for (RelatedStandard relatedStandard : relatedStandards) {
            linkResources = new ArrayList<>();
            linkResource = new LinkResource(relatedStandard.getId().toString(), relatedStandard.getName(),
                    ResourceType.STANDARD, TreeNodeUtils.NodeType.process);
            linkResource.setIsPublic(isPub);
            linkResources.add(linkResource);
            addTwoCellRowTable(processFileTable, linkResources, getStandardParentNames(list, relatedStandard), "width:40%;", "width:60%;");
        }
        return processFileTable;
    }

    private String getStandardParentNames(List<BaseBean> list, RelatedStandard relatedStandard) {
        String parentPathName = "";
        for (BaseBean file : list) {
            if (relatedStandard.getId().intValue() == file.getId().intValue()) {
                if (StringUtils.isBlank(parentPathName)) {
                    parentPathName = file.getName();
                } else {
                    parentPathName += "/" + file.getName();
                }
            }
        }
        return parentPathName;
    }

    private List<BaseBean> getStandardParentNames(StandardMapper standardMapper, List<RelatedStandard> relatedStandards) {
        // 根据标准获取标准父节点
        Set<Long> ids = getIdsByStandardList(relatedStandards);
        if (ids.isEmpty()) {
            return new ArrayList<>();
        }
        Map<String, Object> paramMap = new HashedMap();
        paramMap.put("ids", ids);
        return standardMapper.getRelatedParentNameByIds(paramMap);
    }

    private Set<Long> getIdsByStandardList(List<RelatedStandard> list) {
        Set<Long> ids = new HashSet<>();
        for (RelatedStandard resultBean : list) {
            ids.add(resultBean.getId());
        }
        return ids;
    }

    /**
     * 相关风险
     *
     * @param relatedRisks
     * @return
     */
    private ProcessFileTable getRelatedRisk(List<RelatedRisk> relatedRisks) {
        ProcessFileTable processFileTable = getTableH();
        if (relatedRisks.isEmpty()) {
            return processFileTable;
        }
        addTwoCellRowTable(processFileTable, JecnProperties.getValue("riskNum"), JecnProperties.getValue("riskText"), "width:25%", "width:75%");
        List<LinkResource> linkResources = null;
        LinkResource linkResource = null;
        for (RelatedRisk relatedRisk : relatedRisks) {
            linkResources = new ArrayList<>();
            linkResource = new LinkResource(relatedRisk.getId().toString(), relatedRisk.getNumber(), ResourceType.RISK);
            linkResource.setIsPublic(isPub);
            linkResources.add(linkResource);
            addTwoCellRowTable(processFileTable, linkResources, relatedRisk.getName());
        }
        return processFileTable;
    }

    /**
     * 文控信息
     *
     * @param doucumentMaps KEY:HN.VERSION_ID, HN.MODIFY_EXPLAIN, HN.DRAFT_PERSON,
     *                      HN.PUBLISH_DATE, HF.NAME, HF.APPELLATION
     * @return
     */
    private ProcessFileTable getDocumentControl(List<Map<String, Object>> doucumentMaps) {
        ProcessFileTable processFileTable = getTableH();
        if (doucumentMaps.isEmpty()) {
            return processFileTable;
        }
        List<String> contents = new ArrayList<String>();
        contents.add(JecnProperties.getValue("versionNum"));
        contents.add(JecnProperties.getValue("changeInstructionss"));
        contents.add(JecnProperties.getValue("ReleaseDate"));
        contents.add(JecnProperties.getValue("DrafterC"));
        contents.add(JecnProperties.getValue("approverP"));

        addCellRowTable(processFileTable, contents);
        // 1、按文件主键分组

        Map<String, Map<String, Object>> maps = getGroupDocMaps(doucumentMaps);
        for (Entry<String, Map<String, Object>> entry : maps.entrySet()) {
            Map<String, Object> map = entry.getValue();
            contents = new ArrayList<String>();
            contents.add(valueOfString(map.get("VERSION_ID")));
            contents.add(valueOfString(map.get("MODIFY_EXPLAIN")));
            contents.add(valueOfString(map.get("PUBLISH_DATE")));
            contents.add(valueOfString(map.get("DRAFT_PERSON")));
            contents.add(valueOfString(map.get("APPELLATION")));
            addCellRowTable(processFileTable, contents);
        }
        return processFileTable;
    }

    /**
     * 流程KPI
     *
     * @param kpiNames
     * @param configItemMapper
     * @return
     */
    private ProcessFileTable getFlowKpis(List<FlowKpiName> kpiNames, ConfigItemMapper configItemMapper) {
        ProcessFileTable processFileTable = getTableH();
        if (kpiNames.isEmpty()) {
            return processFileTable;
        }
        Map<String, Object> configMap = new HashMap<String, Object>();
        configMap.put("typeBigModel", 1);
        configMap.put("typeSmallModel", 16);

        List<ConfigItemBean> kpiItems = configItemMapper.findConfigItems(configMap);
        TmpKpiShowValues showValues = FlowKpiTitleAndValuesEnum.INSTANCE.getConfigKpiNameAndValues(kpiNames, kpiItems);
        /** KPI 动态标题 */
        List<String> kpiTitles = showValues.getKpiTitles();
        /** KPI 动态行数据 */
        List<List<String>> kpiRowValues = showValues.getKpiRowValues();

        // 添加标题
        addCellRowTable(processFileTable, kpiTitles);
        // 添加内容
        for (List<String> list : kpiRowValues) {
            addCellRowTable(processFileTable, list);
        }
        return processFileTable;
    }

    private Map<String, Map<String, Object>> getGroupDocMaps(List<Map<String, Object>> doucumentMaps) {
        Map<String, Map<String, Object>> maps = new HashMap<String, Map<String, Object>>();
        Map<String, Object> resultMap = null;
        for (Map<String, Object> map : doucumentMaps) {
            if (!maps.containsKey(map.get("VERSION_ID"))) {
                maps.put(map.get("VERSION_ID").toString(), new HashMap<String, Object>());
            }
            resultMap = maps.get(map.get("VERSION_ID").toString());
            if (resultMap.get("VERSION_ID") == null) {
                resultMap.put("VERSION_ID", map.get("VERSION_ID"));
                resultMap.put("MODIFY_EXPLAIN", map.get("MODIFY_EXPLAIN"));
                resultMap.put("DRAFT_PERSON", map.get("DRAFT_PERSON"));
                resultMap.put("PUBLISH_DATE", map.get("PUBLISH_DATE"));
                if (map.get("NAME") == null || StringUtils.isBlank(map.get("NAME").toString())) {
                    continue;
                }
                resultMap.put("APPELLATION", map.get("APPELLATION") + "：" + getHistoryAppvoewName(map.get("NAME")));
            } else {
                if (map.get("NAME") == null || StringUtils.isBlank(map.get("NAME").toString())) {
                    continue;
                }
                if (resultMap.get("APPELLATION") == null) {
                    continue;
                }
                resultMap.put("APPELLATION", resultMap.get("APPELLATION").toString() + "<BR>" + map.get("APPELLATION") + "：" + getHistoryAppvoewName(map.get("NAME")));
            }
        }
        return maps;
    }

    private String getHistoryAppvoewName(Object name) {
        if (name == null) {
            return "";
        }
        return Common.replaceSlash(name.toString());
    }

    private String getStringByRuleDir(String isDir) {
        return "1".equals(isDir) ? JecnProperties.getValue("ruleFile") : JecnProperties.getValue("ruleTmpFile");
    }

    private void addTwoCellRowTable(ProcessFileTable processFileTable, List<LinkResource> linkResources,
                                    String content, String style1, String style2) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setLinks(linkResources);
        tableCell.setStyle(style1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content);
        tableCell.setStyle(style2);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    private void addThreeCellRowTable(ProcessFileTable processFileTable, String content1,
                                      List<LinkResource> linkResources, String content3) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setLinks(linkResources);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content3);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    /**
     * 流程操作规范
     *
     * @param listInputFiles
     * @return
     */
    private ProcessFileTable getOperationFileShow(List<Map<String, Object>> listInputFiles) {
        List<Map<String, Object>> operationMaps = getOperationStandardMaps(listInputFiles);
        ProcessFileTable fileTable = getTableH();
        if (operationMaps.isEmpty()) {
            return fileTable;
        }

        addTwoCellRowTable(fileTable, JecnProperties.getValue("documentNumber"), JecnProperties.getValue("documentName"), "width:25%", "width:75%");
        List<LinkResource> linkResources = null;
        LinkResource linkResource = null;
        List<String> fileIds = new ArrayList<>();
        for (Map<String, Object> map : operationMaps) {
            if (map.get("FILE_ID") == null) {
                continue;
            }
            String fileId = valueOfString(map.get("FILE_S_ID"));
            if (StringUtils.isBlank(fileId) || fileIds.contains(fileId)) {
                continue;
            }
            fileIds.add(fileId);
            linkResources = new ArrayList<>();
            linkResource = new LinkResource(fileId, valueOfString(map.get("FILE_NAME")),
                    ResourceType.FILE, TreeNodeUtils.NodeType.process);
            linkResource.setIsPublic(isPub);
            // 设置访问文件URL
            setLinkResourceURL(linkResource, linkResource.getId());
            linkResources.add(linkResource);
            addTwoCellRowTable(fileTable, valueOfString(map.get("DOC_ID")), linkResources);
        }
        return fileTable;
    }

    /**
     * 流程记录
     *
     * @param listInputFiles
     * @param listOutputFiles
     * @return
     */
    private ProcessFileTable getRecordFileShow(List<Map<String, Object>> listInputFiles,
                                               List<Map<String, Object>> listOutputFiles) {
        List<Map<String, Object>> operationMaps = getRelatedFileMaps(listInputFiles, listOutputFiles, false);
        ProcessFileTable fileTable = getTableH();
        if (operationMaps.isEmpty()) {
            return fileTable;
        }
        addTwoCellRowTable(fileTable, JecnProperties.getValue("documentNumber"), JecnProperties.getValue("documentName"), "width:25%", "width:75%");
        List<LinkResource> linkResources = null;
        LinkResource linkResource = null;
        for (Map<String, Object> map : operationMaps) {
            if (map.get("FILE_ID") == null) {
                continue;
            }
            linkResources = new ArrayList<>();
            String fileId = valueOfString(map.get("FILE_ID"));
            if (StringUtils.isBlank(fileId)) {
                continue;
            }

            linkResource = new LinkResource(fileId, valueOfString(map.get("FILE_NAME")),
                    ResourceType.FILE, TreeNodeUtils.NodeType.process);
            linkResource.setIsPublic(isPub);
            setLinkResourceURL(linkResource, linkResource.getId());
            linkResources.add(linkResource);
            addTwoCellRowTable(fileTable, valueOfString(map.get("FILE_NUM")), linkResources);
        }
        return fileTable;
    }

    private void setLinkResourceURL(LinkResource linkResource, String id) {
        if (historyId != 0) {
            linkResource.setUrl(HttpUtil.getDownLoadHistoryFileHttp(valueOfLong(id), false, "", true));
        } else {
            linkResource.setUrl(HttpUtil.getDownLoadHistoryFileHttp(valueOfLong(id), isPub == 1 ? true : false, "", false));
        }
    }

    /**
     * 相关文件
     *
     * @param listInputFiles
     * @param listOutputFiles
     * @return
     */
    private ProcessFileTable getRelatedFiles(List<Map<String, Object>> listInputFiles,
                                             List<Map<String, Object>> listOutputFiles) {
        List<Map<String, Object>> operationMaps = getRelatedFileMaps(listInputFiles, listOutputFiles, true);
        ProcessFileTable fileTable = getTableH();
        if (operationMaps.isEmpty()) {
            return fileTable;
        }
        addTwoCellRowTable(fileTable, JecnProperties.getValue("documentNumber"), JecnProperties.getValue("documentName"), "width:25%", "width:75%");

        List<LinkResource> linkResources = null;
        LinkResource linkResource = null;
        for (Map<String, Object> map : operationMaps) {
            String fileId = valueOfString(map.get("FILE_ID"));
            if (StringUtils.isBlank(fileId)) {
                continue;
            }
            linkResources = new ArrayList<>();
            linkResource = new LinkResource(fileId, valueOfString(map.get("FILE_NAME")),
                    ResourceType.FILE, TreeNodeUtils.NodeType.process);
            linkResource.setIsPublic(isPub);
            // 设置访问文件URL
            setLinkResourceURL(linkResource, linkResource.getId());
            linkResources.add(linkResource);
            addTwoCellRowTable(fileTable, valueOfString(map.get("FILE_NUM")), linkResources);
        }
        return fileTable;
    }

    /**
     * 记录保存
     *
     * @param recordMaps KEY:RECORD_NAME,T.RECORD_SAVE_PEOPLE,T.SAVE_LOCATION,T.
     *                   FILE_TIME,T.SAVE_TIME,T.RECORD_APPROACH
     * @return
     */
    private ProcessFileTable getFlowRecord(List<Map<String, Object>> recordMaps) {
        ProcessFileTable fileTable = getTableH();
        if (recordMaps.isEmpty()) {
            return fileTable;
        }
        List<String> titles = new ArrayList<String>();
        titles.add(JecnProperties.getValue("recordName"));
        titles.add(JecnProperties.getValue("preservationOfTheResponsiblePerson"));
        titles.add(JecnProperties.getValue("placeOfPreservation"));
        titles.add(JecnProperties.getValue("archivingTime"));
        titles.add(JecnProperties.getValue("timeLimitForPreservation"));
        titles.add(JecnProperties.getValue("expiryProcessing"));
        // 添加标题
        addCellRowTable(fileTable, titles);
        // 添加内容
        for (Map<String, Object> map : recordMaps) {
            titles = new ArrayList<String>();
            titles.add(valueOfString(map.get("RECORD_NAME")));
            titles.add(valueOfString(map.get("RECORD_SAVE_PEOPLE")));
            titles.add(valueOfString(map.get("SAVE_LOCATION")));
            titles.add(valueOfString(map.get("FILE_TIME")));
            titles.add(valueOfString(map.get("SAVE_TIME")));
            titles.add(valueOfString(map.get("RECORD_APPROACH")));
            addCellRowTable(fileTable, titles);
        }
        return fileTable;
    }

    private List<Map<String, Object>> getRelatedFileMaps(List<Map<String, Object>> listInputFiles,
                                                         List<Map<String, Object>> listOutputFiles, boolean isRelatedFile) {
        List<Map<String, Object>> operationMaps = new ArrayList<>();
        List<Long> fileIds = new ArrayList<>();
        Long curFileId = null;
        Map<String, Object> result = null;
        for (Map<String, Object> map : listInputFiles) {
            curFileId = valueOfLong(map.get("FILE_S_ID"));
            if (!isRelatedFile && valueOfInt(map.get("FILE_TYPE")) == 1) {// 0:活动输入，1 (流程相关文件：输入、输出、操作规范)
                continue;
            }
            if (fileIds.contains(curFileId)) {
                continue;
            }
            result = new HashMap<String, Object>();
            result.put("FILE_ID", map.get("FILE_S_ID"));
            result.put("FILE_NAME", map.get("FILE_NAME"));
            result.put("FILE_NUM", map.get("DOC_ID"));
            fileIds.add(curFileId);
            operationMaps.add(result);
        }

        for (Map<String, Object> map : listOutputFiles) {
            curFileId = valueOfLong(map.get("FILE_M_ID"));
            if (fileIds.contains(curFileId)) {
                continue;
            }
            result = new HashMap<String, Object>();
            result.put("FILE_ID", map.get("FILE_M_ID"));
            result.put("FILE_NAME", map.get("FILE_NAME"));
            result.put("FILE_NUM", map.get("DOC_ID"));
            fileIds.add(curFileId);
            operationMaps.add(result);
        }
        fileIds = null;
        return operationMaps;
    }

    /**
     * 相关流程
     */
    private void addTitleCellRelatedFlowTable(ProcessFileTable processFileTable, String content1, String content2,
                                              String content3) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        tableCell.setStyle("width:10%");
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content2);
        tableCell.setStyle("width:25%");
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content3);
        tableCell.setStyle("width:60%");
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    private void addRowsCellRelatedFlowTable(ProcessFileTable processFileTable, String content1, List<String> texts,
                                             List<LinkResource> postLinks) {
        List<FileTableCell> cells = new ArrayList<FileTableCell>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setTexts(texts);
        cells.add(tableCell);

        // 相关流程
        tableCell = processFileTable.getTableCell();
        tableCell.setLinks(postLinks);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    private String getRoleRelatedPostMap(List<Map<String, Object>> roleRelatedPosts,
                                         List<Map<String, Object>> roleRelatedPostGroups, Long roleId) {
        StringBuffer str = new StringBuffer();
        // key : ROLE_ID, POS_ID, JF.FIGURE_TEXT
        for (Map<String, Object> map : roleRelatedPosts) {
            if (roleId.toString().equals(valueOfString(map.get("ROLE_ID")))) {
                str.append(valueOfString(map.get("FIGURE_TEXT"))).append("/");
            }
        }
        for (Map<String, Object> map : roleRelatedPostGroups) {// 岗位组
            if (roleId.toString().equals(valueOfString(map.get("ROLE_ID")))) {
                str.append(valueOfString(map.get("FIGURE_TEXT"))).append("/");
            }
        }
        if (str.length() > 0) {
            return str.substring(0, str.toString().length() - 1);
        }
        return str.toString();
    }

    /**
     * 添加title
     *
     * @param processFileTable
     * @param content1
     * @param content2
     * @param content3
     */
    private void addRoleAdnContainsPostableTitle(ProcessFileTable processFileTable, String content1, String content2,
                                                 String content3, String style1, String style2,
                                                 String style3) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        tableCell.setStyle(style1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content2);
        tableCell.setStyle(style2);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content3);
        tableCell.setStyle(style3);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    private void addRoleAdnContainsPostableRows(ProcessFileTable processFileTable, String content1,
                                                String content2, String content3) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content2);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content3);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    /**
     * 关键活动表格
     *
     * @param maps
     * @return
     */
    private ProcessFileTable getActivityKeyTable(List<Map<String, Object>> maps, int type) {
        if (maps.isEmpty()) {
            return null;
        }
        ProcessFileTable processFileTable = getTableH();
        addActivityKeyTitle(processFileTable, type);

        // 添加标题
        List<FileTableCell> cells = null;
        for (Map<String, Object> map : maps) {
            Long figureId = valueOfLong(map.get("FIGURE_ID"));
            cells = new ArrayList<FileTableCell>();
            FileTableCell tableCell = processFileTable.getTableCell();
            tableCell.setText(valueOfString(map.get("ACTIVITY_ID")));
            tableCell.setStyle("width:10%");
            cells.add(tableCell);

            // 活动名称
            tableCell = processFileTable.getTableCell();
            tableCell.setText(null);
            LinkResource linkResource = new LinkResource(figureId.toString(), map.get("FIGURE_TEXT") == null ? "" : map.get("FIGURE_TEXT").toString(),
                    ResourceType.ACTIVITY);
            List<LinkResource> linkResources = new ArrayList<LinkResource>();
            linkResources.add(linkResource);
            linkResource.setIsPublic(isPub);
            tableCell.setLinks(linkResources);
            tableCell.setStyle("width:20%");
            cells.add(tableCell);

            // 活动说明
            tableCell = processFileTable.getTableCell();
            tableCell.setText(getStringByClob((Clob) map.get("ACTIVITY_SHOW")));
            tableCell.setStyle("width:40%");
            cells.add(tableCell);

            // 活动关键说明
            tableCell = processFileTable.getTableCell();
            tableCell.setText(replaceWarp(valueOfString(map.get("ROLE_RES"))));
            tableCell.setStyle("width:25%");
            cells.add(tableCell);

            processFileTable.getContents().add(cells);
        }
        return processFileTable;
    }

    /**
     * 关键活动表格
     *
     * @param maps
     * @return
     */
    private List<ProcessFileTable> getActivityKeyParagraph(List<Map<String, Object>> maps, int type) {
        if (maps.isEmpty()) {
            return null;
        }
        List<ProcessFileTable> fileTables = new ArrayList<>();
        ProcessFileTable processFileTable = null;
        for (Map<String, Object> map : maps) {
            processFileTable = new ProcessFileTable();
            processFileTable.setFileType(ProcessFileType.paragraph);
            addTwoCellRowTable(processFileTable, valueOfString(map.get("ACTIVITY_ID")), getResourceLinks(
                    valueOfString(map.get("FIGURE_ID")), valueOfString(map.get("FIGURE_TEXT")), ResourceType.ACTIVITY));
            addTwoCellRowTable(processFileTable, JecnProperties.getValue("activityDescriptionC"), getStringByClob((Clob) map.get("ACTIVITY_SHOW")));
            addTwoCellRowTable(processFileTable, getActivityKeyTitleByType(type) + "：",
                    valueOfString(map.get("ROLE_RES")));
            fileTables.add(processFileTable);
        }
        return fileTables;
    }

    private String getActivityKeyTitleByType(int type) {
        String title = "";
        if (type == 1) {
            title = JecnProperties.getValue("mattersNeedingAttention");
        } else if (type == 2) {
            title = JecnProperties.getValue("keySuccessFactors");
        } else if (type == 3) {
            title = JecnProperties.getValue("criticalControlPoint");
        } else if (type == 4) {
            title = JecnProperties.getValue("keyControlPointAllow");
        }
        return title;
    }

    private void addActivityKeyTitle(ProcessFileTable processFileTable, int type) {
        // 添加标题
        List<FileTableCell> cells = new ArrayList<>();

        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(JecnProperties.getValue("activityNumber"));
        cells.add(tableCell);

        // 活动名称
        tableCell = processFileTable.getTableCell();
        tableCell.setText(JecnProperties.getValue("activityNam"));
        cells.add(tableCell);

        // 活动说明
        tableCell = processFileTable.getTableCell();
        tableCell.setText(JecnProperties.getValue("activityDescription"));
        cells.add(tableCell);

        // 活动关键说明
        tableCell = processFileTable.getTableCell();
        tableCell.setText(getActivityKeyTitleByType(type));
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    private String getPersonName(Long peopleId, UserMapper userMapper) {
        if (peopleId == null || userMapper == null) {
            return null;
        }
        JecnUser user = userMapper.getUser(peopleId);
        return user == null ? null : user.getName();
    }

    private String getPostName(Long postId, OrgMapper orgMapper) {
        if (postId == null || orgMapper == null) {
            return null;
        }
        return orgMapper.findPostNameById(postId);
    }

    private ProcessFileTable getTableH() {
        ProcessFileTable processFileTable = new ProcessFileTable();
        processFileTable.setFileType(ProcessFileType.tableH);
        return processFileTable;
    }

    private ProcessFileTable getTableV() {
        ProcessFileTable processFileTable = new ProcessFileTable();
        processFileTable.setFileType(ProcessFileType.tableV);
        return processFileTable;
    }

    private ProcessFileTable getTableVH() {
        ProcessFileTable processFileTable = new ProcessFileTable();
        processFileTable.setFileType(ProcessFileType.tableVH);
        return processFileTable;
    }

    /**
     * 时间类型：1:日，2:周，3:月，4:季，5:年
     *
     * @param quantity
     * @return
     */
    private String getTimeDriverType(String quantity) {
        String quantityTime = "";
        if ("1".equals(quantity)) {
            quantityTime = JecnProperties.getValue("ri");
        } else if ("2".equals(quantity)) {
            quantityTime = JecnProperties.getValue("week");
        } else if ("3".equals(quantity)) {
            quantityTime = JecnProperties.getValue("month");
        } else if ("4".equals(quantity)) {
            quantityTime = JecnProperties.getValue("ji");
        } else if ("5".equals(quantity)) {
            quantityTime = JecnProperties.getValue("year");
        }
        return quantityTime;
    }

    /**
     * 添加行数据
     *
     * @param processFileTable
     * @param contents
     */
    private void addCellRowTable(ProcessFileTable processFileTable, List<String> contents) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = null;
        for (String content : contents) {
            tableCell = processFileTable.getTableCell();
            tableCell.setText(content);
            cells.add(tableCell);
        }
        processFileTable.getContents().add(cells);
    }

    /**
     * 添加行数据
     *
     * @param processFileTable
     * @param contents
     */
    private void addCellRowTableStyle(ProcessFileTable processFileTable, List<Map<String, String>> contents) {
        List<FileTableCell> cells = new ArrayList<FileTableCell>();
        FileTableCell tableCell = null;
        for (Map<String, String> content : contents) {
            tableCell = processFileTable.getTableCell();
            tableCell.setText(content.get("text"));
            tableCell.setStyle(content.get("style"));
            cells.add(tableCell);
        }
        processFileTable.getContents().add(cells);
    }

    /**
     * @param processFileTable
     * @param content1         内容
     * @param content2         内容
     */
    public void addTwoCellRowTable(ProcessFileTable processFileTable, String content1, String content2) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content2);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    public void addTwoCellRowTable(ProcessFileTable processFileTable, String content1, String content2, String style1, String style2) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        tableCell.setStyle(style1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content2);
        tableCell.setStyle(style2);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    public void addFourCellRowTable(ProcessFileTable processFileTable, String content1, String content2, String content3, String style1, String style2) {
        List<FileTableCell> cells = new ArrayList<>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        tableCell.setStyle(style1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content2);
        tableCell.setStyle(style2);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content3);
        tableCell.setStyle(style1);
        cells.add(tableCell);

        processFileTable.getContents().add(cells);
    }

    /**
     * @param figureId
     * @param inputMaps KEY: FILE_ID,T.FIGURE_ID,
     *                  T.FILE_S_ID(关联的文件主键ID),JF.FILE_NAME,T.FILE_TYPE(0:输入；1：操作规范),
     *                  JF.DOC_ID, JFO.ORG_NAME, JSI.ACTIVITY_INPUT
     */
    private Map<Long, String> getActivityInputMaps(Long figureId, List<Map<String, Object>> inputMaps) {
        Map<Long, String> maps = new HashMap<>();
        for (Map<String, Object> map : inputMaps) {
            if (!figureId.toString().equals(map.get("FIGURE_ID").toString()) || valueOfInt(map.get("FILE_TYPE")) == 1) {
                continue;
            }
            maps.put(valueOfLong(map.get("FILE_S_ID")), valueOfString(map.get("FILE_NAME")));
        }
        return maps;
    }

    private Map<Long, String> getActivityOutputMaps(Long figureId, List<Map<String, Object>> outputMaps) {
        Map<Long, String> maps = new HashMap<>();
        for (Map<String, Object> map : outputMaps) {
            if (!figureId.toString().equals(map.get("FIGURE_ID").toString())) {
                continue;
            }
            maps.put(valueOfLong(map.get("FILE_M_ID")), valueOfString(map.get("FILE_NAME")));
        }
        return maps;
    }

    /**
     * 流程操作规范
     *
     * @param inputMaps
     * @return
     */
    private List<Map<String, Object>> getOperationStandardMaps(List<Map<String, Object>> inputMaps) {
        List<Map<String, Object>> operationMaps = new ArrayList<>();
        for (Map<String, Object> map : inputMaps) {
            if (valueOfInt(map.get("FILE_TYPE")) == 0) {// 0:活动输入，1 ：操作规范
                continue;
            }
            operationMaps.add(map);
        }
        return operationMaps;
    }

    public void addTwoCellRowTable(ProcessFileTable processFileTable, String content1,
                                   List<LinkResource> linkResources) {
        List<FileTableCell> cells = new ArrayList<FileTableCell>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setLinks(linkResources);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    public void addTwoCellRowTable(ProcessFileTable processFileTable,
                                   List<LinkResource> linkResources, String content1) {
        List<FileTableCell> cells = new ArrayList<FileTableCell>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setLinks(linkResources);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(content1);
        cells.add(tableCell);
        processFileTable.getContents().add(cells);
    }

    public void addTwoCellRowTable(ProcessFileTable processFileTable, String fName, String fId, String itId) {
        List<FileTableCell> cells = new ArrayList<FileTableCell>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(fName);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(fId);
        cells.add(tableCell);

        FileTableCell tableCelll = processFileTable.getTableCell();
        tableCelll.setText(itId);
        cells.add(tableCelll);
        processFileTable.getContents().add(cells);
    }


    private List<FileTableCell> addActivityInfoRowTableH(ProcessFileTable processFileTable, String roleName,
                                                         Map<String, Object> objActives) {
        String activityId = valueOfString(objActives.get("ACTIVITY_ID"));
        String activityName = valueOfString(objActives.get("FIGURE_TEXT"));
        String activityShow = getStringByClob((Clob) objActives.get("ACTIVITY_SHOW"));
        Long figureId = valueOfLong(objActives.get("FIGURE_ID"));

        List<FileTableCell> cells = new ArrayList<FileTableCell>();
        FileTableCell tableCell = processFileTable.getTableCell();
        tableCell.setText(activityId);
        cells.add(tableCell);

        tableCell = processFileTable.getTableCell();
        tableCell.setText(roleName);
        cells.add(tableCell);

        // 活动名称
        tableCell = processFileTable.getTableCell();
        tableCell.setLinks(getResourceLinks(figureId.toString(), activityName, ResourceType.ACTIVITY));
        cells.add(tableCell);

        // 活动说明
        tableCell = processFileTable.getTableCell();
        tableCell.setText(activityShow);
        cells.add(tableCell);
        return cells;
    }

    public List<LinkResource> getResourceLinks(String id, String name, ResourceType resourceType) {
        LinkResource linkResource = new LinkResource(id, name, resourceType, TreeNodeUtils.NodeType.ruleModelFile);
        linkResource.setIsPublic(isPub);//是否发布
        if (!ResourceType.RULE.equals(resourceType)) {
            linkResource.setUrl(HttpUtil.getDownLoadHistoryFileHttp(valueOfLong(id), isPub == 1, "", true));
        }
        List<LinkResource> linkResources = new ArrayList<LinkResource>();
        linkResources.add(linkResource);
        return linkResources;
    }

    public void setHistoryId(long historyId) {
        this.historyId = historyId;
    }

    private void addActivityInfoRowTableH(ProcessFileTable processFileTable, Map<String, Object> objActives,
                                          String roleName, List<Map<String, Object>> input, List<Map<String, Object>> output, boolean addNJSHColumn,
                                          boolean isShowInoutPut, Map<Long, String> activityToItMap, List<JecnFigureInoutBase> inouts) {
        // 活动编号、执行角色，活动名称
        List<FileTableCell> cells = addActivityInfoRowTableH(processFileTable, roleName, objActives);
        Long figureId = valueOfLong(objActives.get("FIGURE_ID"));
        if (isShowInoutPut) {
            if (ConfigUtils.useNewInout()) {
                // 添加输入和输出
                cells.addAll(createInoutCells(processFileTable, inouts));
            } else {
                Map<Long, String> inputMap = getActivityInputMaps(figureId, input);
                // 输入
                FileTableCell tableCell = processFileTable.getTableCell();
                tableCell.setText(replaceWarp(getActivityInputText(figureId, input)));
                tableCell.setLinks(getFileLinkResoucres(inputMap));
                cells.add(tableCell);
                // 输出
                Map<Long, String> outputMap = getActivityOutputMaps(figureId, output);
                tableCell = processFileTable.getTableCell();
                tableCell.setText(replaceWarp(getActivityOutputText(figureId, output)));
                tableCell.setLinks(getFileLinkResoucres(outputMap));
                cells.add(tableCell);
            }
        }

        if (addNJSHColumn) {// 南京石化添加(INNER_CONTROL_RISK,T.STANDARD_CONDITIONS)
            FileTableCell tableCell = processFileTable.getTableCell();
            tableCell.setText(valueOfString(objActives.get("INNER_CONTROL_RISK")));
            cells.add(tableCell);
            tableCell = processFileTable.getTableCell();
            tableCell.setText(valueOfString(objActives.get("STANDARD_CONDITIONS")));
            cells.add(tableCell);
        }

        if (ConfigUtils.showActTimeLine()) {
            // 办理时限
            FileTableCell tableCell = processFileTable.getTableCell();
            if (objActives.get("STATUS_VALUE") != null && objActives.get("TARGET_VALUE") != null) {
                tableCell.setText(getTimeLineValue(objActives));
            }
            cells.add(tableCell);
        }

        if (ConfigUtils.processFileActivityShowIT()) {// 信息化
            FileTableCell tableCell = processFileTable.getTableCell();
            tableCell.setText(valueOfString(activityToItMap.get(figureId)));
            cells.add(tableCell);
        }

        processFileTable.getContents().add(cells);
    }

    /**
     * 0输入 1输出
     *
     * @param processFileTable
     * @param inouts
     * @return
     */
    private List<FileTableCell> createInoutCells(ProcessFileTable processFileTable, List<JecnFigureInoutBase> inouts) {
        FileTableCell in = processFileTable.getTableCell();
        FileTableCell out = processFileTable.getTableCell();
        List<FileTableCell> result = new ArrayList<>();
        result.add(in);
        result.add(out);
        if (Utils.isEmpty(inouts)) {
            return result;
        }
        Map<Integer, List<JecnFigureInoutBase>> map = inouts.stream().collect(Collectors.groupingBy(JecnFigureInoutBase::getType));
        List<JecnFigureInoutBase> ins = map.get(0);
        List<JecnFigureInoutBase> outs = map.get(1);

        in.setText(Utils.concat(ins, "<br>", JecnFigureInoutBase::getName));
        in.setLinks(getFileLinkResoucres(modelIdName(ins)));

        out.setText(Utils.concat(outs, "<br>", JecnFigureInoutBase::getName));
        List<LinkResource> outLinks = new ArrayList<>();
        outLinks.addAll(getFileLinkResoucres(modelIdName(outs)));
        outLinks.addAll(getFileLinkResoucres(sampleIdName(outs)));
        out.setLinks(outLinks);

        return result;
    }

    private List<Tuple<Long, String>> modelIdName(List<JecnFigureInoutBase> ins) {
        if (Utils.isEmpty(ins)) {
            return new ArrayList<>();
        }
        List<Tuple<Long, String>> result = new ArrayList<>();
        for (JecnFigureInoutBase a : ins) {
            if (!Utils.isEmpty(a.getSamples())) {
                for (JecnFigureInoutSampleBase b : a.getSamples()) {
                    if (b.getFileId() != null && b.getType() == 0) {
                        result.add(Tuple.instace(b.getFileId(), "[" + JecnProperties.getValue("model") + "]" + b.getFileName()));// 模板
                    }
                }
            }
        }
        return result;
    }

    private List<Tuple<Long, String>> sampleIdName(List<JecnFigureInoutBase> ins) {
        if (Utils.isEmpty(ins)) {
            return new ArrayList<>();
        }
        List<Tuple<Long, String>> result = new ArrayList<>();
        for (JecnFigureInoutBase a : ins) {
            if (!Utils.isEmpty(a.getSamples())) {
                for (JecnFigureInoutSampleBase b : a.getSamples()) {
                    if (b.getFileId() != null && b.getType() == 1) {
                        result.add(Tuple.instace(b.getFileId(), "[" + JecnProperties.getValue("sample") + "]" + b.getFileName()));// 样例
                    }
                }
            }
        }
        return result;
    }

    private String getTimeLineValue(Map<String, Object> objActives) {
        if (objActives.get("STATUS_VALUE") != null && objActives.get("TARGET_VALUE") != null) {
            String statusValue = objActives.get("STATUS_VALUE").toString();
            String targetValue = objActives.get("TARGET_VALUE").toString();
            // 去“.0”
            statusValue = statusValue.endsWith(".0") ? statusValue.substring(0, statusValue.indexOf(".0")) : statusValue;
            targetValue = targetValue.endsWith(".0") ? targetValue.substring(0, targetValue.indexOf(".0")) : targetValue;
            return statusValue + "/" + targetValue;
        }
        return "";
    }

    private String getActivityInputText(Long figureId, List<Map<String, Object>> input) {
        for (Map<String, Object> map : input) {
            if (figureId.toString().equals(map.get("FIGURE_ID").toString())) {
                return valueOfString(map.get("ACTIVITY_INPUT"));
            }
        }
        return null;
    }

    private String getActivityOutputText(Long figureId, List<Map<String, Object>> output) {
        for (Map<String, Object> map : output) {
            if (figureId.toString().equals(map.get("FIGURE_ID").toString())) {
                return valueOfString(map.get("ACTIVITY_OUTPUT"));
            }
        }
        return null;
    }

    /**
     * 文件链接resource
     *
     * @return
     */
    private List<LinkResource> getFileLinkResoucres(List<Tuple<Long, String>> list) {
        List<LinkResource> linkResources = new ArrayList<LinkResource>();
        LinkResource resource = null;
        for (Tuple<Long, String> entry : list) {
            if (entry.getA() == null) {
                continue;
            }
            resource = new LinkResource(entry.getA().toString(), entry.getB(), ResourceType.FILE, TreeNodeUtils.NodeType.process);
            // 设置访问文件URL
            setLinkResourceURL(resource, resource.getId());
            resource.setIsPublic(isPub);
            linkResources.add(resource);
        }
        if (list == null) {
            return new ArrayList<>();
        }
        return linkResources;
    }

    /**
     * 文件链接resource
     *
     * @param map
     * @return
     */
    private List<LinkResource> getFileLinkResoucres(Map<Long, String> map) {
        List<LinkResource> linkResources = new ArrayList<LinkResource>();
        LinkResource resource = null;
        for (Entry<Long, String> entry : map.entrySet()) {
            if (entry.getKey() == null) {
                continue;
            }
            resource = new LinkResource(entry.getKey().toString(), entry.getValue(), ResourceType.FILE, TreeNodeUtils.NodeType.process);
            // 设置访问文件URL
            setLinkResourceURL(resource, resource.getId());
            resource.setIsPublic(isPub);
            linkResources.add(resource);
        }
        return linkResources;
    }

    /**
     * @param roleRelatedActiveMaps 角色活动对应关系
     *                              key:FIGURE_ROLE_ID,RA.FIGURE_ACTIVE_ID,FSI.FIGURE_TEXT,FSI.
     *                              ROLE_RES
     * @param figureId
     * @return
     */
    private String getActivityRelatedRoleName(List<Map<String, Object>> roleRelatedActiveMaps, Long figureId) {
        // 执行角色
        for (Map<String, Object> map : roleRelatedActiveMaps) {
            if (map.get("FIGURE_ROLE_ID") == null) {
                continue;
            }
            if (figureId.toString().equals(map.get("FIGURE_ACTIVE_ID").toString())) {// 活动
                return valueOfString(map.get("FIGURE_TEXT"));
            }
        }
        return null;
    }

    private String valueOfString(Object obj) {
        return obj == null ? null : obj.toString();
    }

    /**
     * 字符串，替换换行符
     *
     * @param obj
     * @return
     */
    public String valueOfWarpString(Object obj) {
        return obj == null ? null : replaceWarp(obj.toString());
    }

    private Long valueOfLong(Object obj) {
        return obj == null ? null : Long.valueOf(obj.toString());
    }

    private int valueOfInt(Object obj) {
        return obj == null ? -1 : Integer.valueOf(obj.toString());
    }

    private int valueOfInteger(Object obj) {
        return obj == null ? -1 : Integer.parseInt(obj.toString());
    }

    /**
     * 排序活动
     *
     * @param list
     * @return
     */
    private void getActivityObjSort(List<Map<String, Object>> list) {
        // 外循环控制比较的次数
        for (int i = 0; i < list.size(); i++) {
            // 内循环控制比较后移位
            for (int j = list.size() - 1; j > i; j--) {
                Map<String, Object> processActiveBeanOne = list.get(j - 1);
                Map<String, Object> processActiveBeanTwo = list.get(j);
                Object oneActivesNumber = processActiveBeanOne.get("ACTIVITY_ID");
                Object twoActivesNumber = processActiveBeanTwo.get("ACTIVITY_ID");
                if (oneActivesNumber != null && twoActivesNumber != null) {
                    if (oneActivesNumber.toString().indexOf("-") > 0) {
                        oneActivesNumber = oneActivesNumber.toString().substring(
                                oneActivesNumber.toString().lastIndexOf("-") + 1, oneActivesNumber.toString().length());
                    }
                    if (!oneActivesNumber.toString().matches("[0-9]+")) {
                        list.set(j - 1, processActiveBeanTwo);
                        list.set(j, processActiveBeanOne);
                        continue;
                    }

                    if (twoActivesNumber.toString().indexOf("-") > 0) {
                        twoActivesNumber = twoActivesNumber.toString().substring(
                                twoActivesNumber.toString().lastIndexOf("-") + 1, twoActivesNumber.toString().length());
                    }
                    if (!twoActivesNumber.toString().matches("[0-9]+")) {
                        continue;
                    }

                    if (Long.valueOf(oneActivesNumber.toString()) > Long.valueOf(twoActivesNumber.toString())) {
                        list.set(j - 1, processActiveBeanTwo);
                        list.set(j, processActiveBeanOne);
                    }
                } else if (oneActivesNumber == null) {
                    list.set(j - 1, processActiveBeanTwo);
                    list.set(j, processActiveBeanOne);
                }
            }
        }
    }

    private String getDriverTypeString(int type) {
        String result = null;
        // 0事件驱动,1是时间驱动
        if (type == 0) {
            result = JecnProperties.getValue("eventDriven");
        } else if (type == 1) {
            result = JecnProperties.getValue("timeDriven");
        } else {
            result = JecnProperties.getValue("eventOrtimeDriven");
        }
        return result;
    }

    /**
     * 时间驱动
     *
     * @return
     */
    private FlowDriver getFlowDriver(Map<String, Object> map, FlowDriver flowDriver) {
        if (flowDriver == null) {
            return new FlowDriver();
        }
        // 数量
        if (flowDriver.getQuantity() != null) {
            if ("1".equals(flowDriver.getQuantity())) { // 日
                // 开始时间
                flowDriver.setStartTime(getTime(flowDriver.getStartTime()));
                // 结束时间
                flowDriver.setEndTime(getTime(flowDriver.getEndTime()));
            } else if ("2".equals(flowDriver.getQuantity())) { // 周
                // 开始时间
                flowDriver.setStartTime(getTimeByDayTime(flowDriver.getStartTime()));
                // 结束时间
                flowDriver.setEndTime(getTimeByDayTime(flowDriver.getEndTime()));
            } else if ("3".equals(flowDriver.getQuantity())) { // 月
                // 开始时间
                String startTime = "";
                if (flowDriver.getStartTime() != null) {
                    startTime = (Integer.valueOf(flowDriver.getStartTime()) + 1) + JecnProperties.getValue("day");
                }
                flowDriver.setStartTime(startTime);
                // 结束时间
                String endTime = "";
                if (flowDriver.getEndTime() != null) {
                    endTime = (Integer.valueOf(flowDriver.getEndTime()) + 1) + JecnProperties.getValue("day");
                }
                flowDriver.setEndTime(endTime);
            } else if ("4".equals(flowDriver.getQuantity())) { // 季
                // 开始时间
                String startTime = "";
                if (flowDriver.getStartTime() != null) {
                    startTime = (Integer.valueOf(flowDriver.getStartTime()) + 1) + JecnProperties.getValue("day");
                }
                flowDriver.setStartTime(startTime);
                // 结束时间
                String endTime = "";
                if (flowDriver.getEndTime() != null) {
                    endTime = (Integer.valueOf(flowDriver.getEndTime()) + 1) + JecnProperties.getValue("day");
                }
                flowDriver.setEndTime(endTime);
            } else if ("5".equals(flowDriver.getQuantity())) { // 年
                // 开始时间
                if (flowDriver.getStartTime() != null) {
                    flowDriver.setStartTime(getTimeByYear(flowDriver.getStartTime()));
                } else {
                    flowDriver.setStartTime("");
                }
                if (flowDriver.getEndTime() != null) {
                    flowDriver.setEndTime(getTimeByYear(flowDriver.getEndTime()));
                } else {
                    flowDriver.setEndTime("");
                }
            }
        } else {
            flowDriver.setQuantity("");
        }
        return flowDriver;
    }

    private String getTime(String time) {
        if (StringUtils.isBlank(time)) {
            return "";
        }
        StringBuffer result = new StringBuffer();
        String[] startStr = time.split(",");
        for (int i = 0; i < startStr.length; i++) {
            String str = startStr[i];
            if (i == 0) {
                result.append(str + JecnProperties.getValue("shi"));
            } else if (i == 1) {
                result.append(str + JecnProperties.getValue("fen"));
            } else if (i == 2) {
                result.append(str + JecnProperties.getValue("miao"));
            }
        }
        return result.toString();
    }

    private String getTimeByDayTime(String dayTime) {
        if (StringUtils.isBlank(dayTime)) {
            return "";
        }
        String result = "";
        if ("0".equals(dayTime)) {
            result = JecnProperties.getValue("monday");
        } else if ("1".equals(dayTime)) {
            result = JecnProperties.getValue("tuesday");
        } else if ("2".equals(dayTime)) {
            result = JecnProperties.getValue("wednesday");
        } else if ("3".equals(dayTime)) {
            result = JecnProperties.getValue("thursday");
        } else if ("4".equals(dayTime)) {
            result = JecnProperties.getValue("friday");
        } else if ("5".equals(dayTime)) {
            result = JecnProperties.getValue("saturday");
        } else if ("6".equals(dayTime)) {
            result = JecnProperties.getValue("sunday");
        }
        return result;
    }

    /**
     * 时间驱动，根据年获取年度天和月
     *
     * @param yearTime
     * @return
     */
    private String getTimeByYear(String yearTime) {
        StringBuffer result = new StringBuffer();
        String[] strings = yearTime.split(",");
        for (int i = 0; i < strings.length; i++) {
            String str = strings[i];
            if (i == 0) {
                result.append((Integer.valueOf(str) + 1) + JecnProperties.getValue("month"));
            } else if (i == 1) {
                result.append((Integer.valueOf(str) + 1) + JecnProperties.getValue("day"));
            }
        }
        return result.toString();
    }

    /**
     * 换行符替换
     *
     * @param str
     * @return
     */
    private String replaceWarp(String str) {
        return Common.replaceWarp(str);
    }

    private String getApplyOrgNames(List<String> orgNames) {
        if (orgNames == null || orgNames.size() == 0) {
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        buffer.append(JecnProperties.getValue("applicableDepartmentC"));
        for (String name : orgNames) {
            buffer.append(name).append("/");
        }
        String result = buffer.substring(0, buffer.length() - 1);
        return result + "<br><br>";
    }
}
