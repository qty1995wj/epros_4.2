package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableSheet;

import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.ProcessActivitiesBean;

/**
 * 
 * 流程活动数统计
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessActiveDownExcelService extends BaseDownExcelService {
	private List<ProcessActivitiesBean> processActivitiesList = null;

	public ProcessActiveDownExcelService() {
		this.mouldPath = getPrefixPath() + "mould/processActiveReport.xls";
		this.startDataRow = 2;
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (processActivitiesList == null) {
			return true;
		}

		try {
			// 第一行
			this.addFirstRowData(dataSheet);

			// 数据行号
			int i = 0;
			// 第三行开始
			for (ProcessActivitiesBean activitiesBean : processActivitiesList) {// 0~99,100以上
				// 如果数量为0跳出循环
				if(activitiesBean.getAllCount()==0){
					continue;
				}
				// 一种活动数对应的数据集合
				List<ProcessWebBean> processWebList = activitiesBean
						.getProcessWebList();
         
				// 活动数
				String activeCount = null;
				// 0~20
				int activeInt = activitiesBean.getActivitiesCount();
				if (activeInt >= 20) {
					activeCount = String.valueOf(">99");
				} else {
					int start = activeInt * 5;
					int end = activeInt * 5 + 4;
					activeCount = String.valueOf(start + "～" + end);
				}

				if (processWebList == null || processWebList.size() == 0) {
					int row = startDataRow + i;
					// 活动数
					dataSheet.addCell(new Label(0, row, activeCount));
					// 合计
					dataSheet.addCell(new Label(5, row, "0"));
					i++;
					continue;
				}

				// 合计
				String allCount = String.valueOf(activitiesBean.getAllCount());
				// 待合并开始行号
				int startFirstRow = startDataRow + i;

				for (ProcessWebBean bean : processWebList) {
					int row = startDataRow + i;
					// 活动数
					dataSheet.addCell(new Label(0, row, activeCount));
					// 流程名称
					dataSheet.addCell(new Label(1, row, bean.getFlowName()));
					// 流程编号
					dataSheet.addCell(new Label(2, row, bean.getFlowIdInput()));
					// 流程责任人
					dataSheet
							.addCell(new Label(3, row, bean.getResPeopleName()));
					// 责任部门
					dataSheet.addCell(new Label(4, row, bean.getOrgName()));
					// 合计
					dataSheet.addCell(new Label(5, row, allCount));
					i++;
				}
				// 合并单元格，参数格式（开始列，开始行，结束列，结束行）
				// 活动数
				dataSheet.mergeCells(0, startFirstRow, 0, startDataRow + i - 1);
				// 合计
				dataSheet.mergeCells(5, startFirstRow, 5, startDataRow + i - 1);
			}
			return true;
		} catch (Exception e) {
			log.error("ProcessActiveDownExcelService类editExcle方法", e);
			return false;
		}
	}

	public List<ProcessActivitiesBean> getProcessActivitiesList() {
		return processActivitiesList;
	}

	public void setProcessActivitiesList(
			List<ProcessActivitiesBean> processActivitiesList) {
		this.processActivitiesList = processActivitiesList;
	}
}
