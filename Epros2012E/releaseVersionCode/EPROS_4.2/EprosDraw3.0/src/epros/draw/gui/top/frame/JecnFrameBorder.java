package epros.draw.gui.top.frame;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.border.Border;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 窗体边框
 * 
 * @author Administrator
 * 
 */
public class JecnFrameBorder implements Border {

	public JecnFrameBorder() {

	}

	public Insets getBorderInsets(Component c) {
		return new Insets(2, 3, 4, 3);
	}

	public boolean isBorderOpaque() {
		return false;
	}

	public void paintBorder(Component c, Graphics g, int x, int y, int width,
			int height) {
		g.setColor(JecnUIUtil.getFrameBorderColor());
		g.fill3DRect(0, 0, width, height, true);
	}

}