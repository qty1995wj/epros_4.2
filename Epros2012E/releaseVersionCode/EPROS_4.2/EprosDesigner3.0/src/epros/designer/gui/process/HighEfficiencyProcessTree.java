package epros.designer.gui.process;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
/**
 * @author yxw 2012-6-14
 * @description：
 */
public class HighEfficiencyProcessTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(HighEfficiencyProcessTree.class);
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new ProcessTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.processRoot,JecnProperties.getValue("process"));
		try {
			list = JecnDesignerCommon.getDesignerProcessTreeBeanList(0L,false);
			JecnTreeCommon.addNLevelNodes(list, rootNode);
		} catch (Exception e) {
			log.error("HighEfficiencyProcessTree getTreeModel is error",e);
		}
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
