package epros.designer.gui.system.config.ui.tree;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeSelectionModel;

import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.util.JecnProperties;

/**
 * 
 * 任务配置树
 * 
 * @author Administrator
 * 
 */
public class JecnTree extends JTree {
	/** 根节点名称 */
	private String jecnRootName;
	/** 树模型 */
	private JecnTreeModel jecnTreeModel = null;
	/** 树根节点 */
	private JecnTreeNode jecnRootNode = null;
	/** 配置界面 */
	protected JecnAbtractBaseConfigDialog dialog = null;

	public JecnTree(String rootName, JecnAbtractBaseConfigDialog dialog) {
		if (rootName == null || "".equals(rootName) || "".equals(rootName.trim()) || dialog == null) {
			throw new IllegalArgumentException("JecnTree is error");
		}
		this.jecnRootName = rootName;
		this.dialog = dialog;

		init();
	}

	private void init() {

		initTreeData();

		// 一次只能选择一个路径
		DefaultTreeSelectionModel selModel = new DefaultTreeSelectionModel();
		selModel.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		setSelectionModel(selModel);
	}

	private void initTreeData() {
		// 创建树模型
		JecnTreeModel tempModel = createModel();
		jecnTreeModel = tempModel;
		// 设置模型
		jecnRootNode = (JecnTreeNode) tempModel.getRoot();
		setModel(tempModel);

		// // 选中根节点
		// TreePath path = new TreePath(jecnRootNode.getPath());
		// setSelectionPath(path);
	}

	/**
	 * 
	 * 初始配置项的树节点
	 * 
	 * @param rootNode
	 * @param configBean
	 */
	private void flowTreeInit(JecnTreeNode rootNode, JecnConfigDesgBean configBean) {
		if (configBean != null) {
			// 审批环节配置
			JecnConfigTypeDesgBean taskApprTypeDesgBean = configBean.getTaskApprTypeDesgBean();
			// 任务页面显示配置项
			JecnConfigTypeDesgBean taskShowTypeDesgBean = configBean.getTaskShowTypeDesgBean();
			// 基本信息设置
			JecnConfigTypeDesgBean basicTypeDesgBean = configBean.getBasicTypeDesgBean();
			// 流程图建设规范
			JecnConfigTypeDesgBean standedTypeDesgBean = configBean.getStandedTypeDesgBean();
			// 流程文件（流程操作说明）
			JecnConfigTypeDesgBean flowFileTypeDesgBean = configBean.getFlowFileTypeDesgBean();
			// 邮箱配置
			JecnConfigTypeDesgBean mailTypeDesgBean = configBean.getMailTypeDesgBean();
			// 权限配置
			JecnConfigTypeDesgBean pubSetTypeDesgBean = configBean.getPubSetTypeDesgBean();
			// 任务操作配置
			JecnConfigTypeDesgBean taskOperatDesgBean = configBean.getTaskOperatDesgBean();
			// 消息邮件配置
			JecnConfigTypeDesgBean messageEmailDesgBean = configBean.getMessageEmailDesgBean();
			// 9:合理化建议配置议
			JecnConfigTypeDesgBean rationProDesgBean = configBean.getRationProDesgBean();
			// 10:流程属性配置（流程其它配置）
			JecnConfigTypeDesgBean flowBasicBean = configBean.getFlowBasicBean();
			
			/** 11发布流程、流程地图、制度；相关文件是否发布相关配置 */
			JecnConfigTypeDesgBean pubRelateFileBean = configBean.getPubRelateFileBean();
			// 14:下载次数配置 */
			JecnConfigTypeDesgBean downLoadCountBean = configBean.getDownLoadCountBean();
			// 15: 文控信息配置
			JecnConfigTypeDesgBean documentControlBean = configBean.getDocumentControlBean();

			// 16: 流程KPI属性名称配置
			JecnConfigTypeDesgBean flowKPIAttributeBean = configBean.getFlowKPIAttributeBean();

			// 18: 流程架构其它配置
			JecnConfigTypeDesgBean otherFlowMapBean = configBean.getFlowMapOtherBean();

			// type: 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置
			// 6：权限配置7：任务操作8：消息邮件配置9:合理化建议配置
			JecnTreeNode node = null;

			if (basicTypeDesgBean.existsItemListData()) {// 基本信息设置
				if (JecnConfigContents.TYPE_BIG_ITEM_PUB == configBean.getBigType()) {// TODO
					// 此判断就是为了屏蔽文件配置中自动版本号问题而添加,将来修改好版本号问题再去掉
					node = new JecnTreeNode(JecnProperties.getValue("setNodeBasicName"), basicTypeDesgBean);
					rootNode.add(node);
				}
			}
			// 是否存在浏览端，没有浏览端隐藏:流程图建设规范
			if (standedTypeDesgBean.existsItemListData()) {// 流程图建设规范
				node = new JecnTreeNode(JecnProperties.getValue("setNodePartMapStandedName"), standedTypeDesgBean);
				rootNode.add(node);
			}
			if (basicTypeDesgBean.existsItemListData()) {// 制度属性
				if (configBean.getBigType() == JecnConfigContents.TYPE_BIG_ITEM_ROLE) {
					// 此判断就是为了屏蔽文件配置中自动版本号问题而添加,将来修改好版本号问题再去掉
					node = new JecnTreeNode(JecnProperties.getValue("ruleAttrBut"), basicTypeDesgBean);
					rootNode.add(node);
				}
			}
			if (basicTypeDesgBean.existsItemListData()) {// 文件属性
				if (configBean.getBigType() == JecnConfigContents.TYPE_BIG_ITEM_FILE) {
					// 此判断就是为了屏蔽文件配置中自动版本号问题而添加,将来修改好版本号问题再去掉
					node = new JecnTreeNode(JecnProperties.getValue("fileProperty"), basicTypeDesgBean);
					rootNode.add(node);
				}
			}
			if (configBean.getBigType() == 1 && flowFileTypeDesgBean.existsItemListData()) { // 流程文件（流程操作说明）
				node = new JecnTreeNode(JecnProperties.getValue("setNodeFlowFileName"), flowFileTypeDesgBean);
				rootNode.add(node);
			}
			if (basicTypeDesgBean.existsItemListData()) {// 流程属性
				if (configBean.getBigType() == JecnConfigContents.TYPE_BIG_ITEM_PARTMAP) {
					// 此判断就是为了屏蔽文件配置中自动版本号问题而添加,将来修改好版本号问题再去掉
					node = new JecnTreeNode(JecnProperties.getValue("flowAttrBtn"), basicTypeDesgBean);
					rootNode.add(node);
				}
			}
			if (basicTypeDesgBean.existsItemListData()) {// 流程架构属性
				if (configBean.getBigType() == JecnConfigContents.TYPE_BIG_ITEM_TOTALMAP) {
					// 此判断就是为了屏蔽文件配置中自动版本号问题而添加,将来修改好版本号问题再去掉
					node = new JecnTreeNode(JecnProperties.getValue("processArchitectureProperties"), basicTypeDesgBean);
					rootNode.add(node);
				}
			}

			if (flowKPIAttributeBean.existsItemListData()) {// 流程KPI
				node = new JecnTreeNode(JecnProperties.getValue("FlowKPISetting"), flowKPIAttributeBean);
				rootNode.add(node);
			}
			// 是否存在浏览端，没有浏览端隐藏:配置任务审批环节,任务页面显示配置项,权限配置
			if (JecnConstants.isPubShow()) {
				// if (taskApprTypeDesgBean.existsItemListData()) {// 配置任务审批环节
				// node = new
				// JecnTreeNode(JecnProperties.getValue("setNodeTaskAppName"),
				// taskApprTypeDesgBean);
				// rootNode.add(node);
				// }
				if (taskShowTypeDesgBean.existsItemListData()) { // 任务页面显示配置项
					node = new JecnTreeNode(JecnProperties.getValue("setNodeTaskShowName"), taskShowTypeDesgBean);
					rootNode.add(node);
				}
				if (pubSetTypeDesgBean.existsItemListData()) { // 权限配置
					node = new JecnTreeNode(JecnProperties.getValue("setNodepubName"), pubSetTypeDesgBean);
					rootNode.add(node);
				}
			}
			if (mailTypeDesgBean.existsItemListData()) { // 邮箱配置
				node = new JecnTreeNode(JecnProperties.getValue("setNodemailName"), mailTypeDesgBean);
				rootNode.add(node);
			}

			// 是否存在浏览端，没有浏览端隐藏:任务操作配置,消息邮件配置,合理化建议配置,流程属性, 发布配置
			if (JecnConstants.isPubShow()) {
				if (taskOperatDesgBean.existsItemListData()) {// 任务操作配置
					node = new JecnTreeNode(JecnProperties.getValue("taskOperation"), taskOperatDesgBean);
					rootNode.add(node);
				}

				if (messageEmailDesgBean.existsItemListData()) {// 消息邮件配置
					node = new JecnTreeNode(JecnProperties.getValue("mailRemindSetting"), messageEmailDesgBean);
					rootNode.add(node);
				}
				if (rationProDesgBean.existsItemListData()) {// 合理化建议配置
					node = new JecnTreeNode(JecnProperties.getValue("rationProName"), rationProDesgBean);
					rootNode.add(node);
				}

				if (flowBasicBean.existsItemListData()) {// 流程属性
					node = new JecnTreeNode(JecnProperties.getValue("otherSet"), flowBasicBean);
					rootNode.add(node);
				}
				if (pubRelateFileBean.existsItemListData()) {// 发布配置
					node = new JecnTreeNode(JecnProperties.getValue("pubSystem"), pubRelateFileBean);
					rootNode.add(node);
				}
				if (downLoadCountBean.existsItemListData()) {// 文件下载次数配置
					node = new JecnTreeNode(JecnProperties.getValue("downLoadCountSet"), downLoadCountBean);
					rootNode.add(node);
				}

				if (otherFlowMapBean.existsItemListData()) {// 流程架构-其它配置
					node = new JecnTreeNode(JecnProperties.getValue("otherSet"), otherFlowMapBean);
					rootNode.add(node);
				}
			}
			/*
			 * if (documentControlBean.existsItemListData()) {// 文控信息配置 node =
			 * new JecnTreeNode(JecnProperties.getValue("documentControlNode"),
			 * documentControlBean); rootNode.add(node); }
			 */
		}
	}

	/**
	 * 
	 * 创建树模型
	 * 
	 * @return
	 */
	private JecnTreeModel createModel() {
		// 根节点
		JecnTreeNode rootNode = new JecnTreeNode(jecnRootName);
		if (dialog != null) {
			// 获取后台数据
			JecnConfigDesgBean configBean = dialog.getConfigBean();

			this.flowTreeInit(rootNode, configBean);
		}
		JecnTreeModel jecnTreeModel = new JecnTreeModel(rootNode);
		return jecnTreeModel;
	}

	public String getJecnRootName() {
		return jecnRootName;
	}

	public void setJecnRootName(String jecnRootName) {
		this.jecnRootName = jecnRootName;
	}

	public JecnTreeModel getJecnTreeModel() {
		return jecnTreeModel;
	}

	public void setJecnTreeModel(JecnTreeModel jecnTreeModel) {
		this.jecnTreeModel = jecnTreeModel;
	}

	public JecnTreeNode getJecnRootNode() {
		return jecnRootNode;
	}

	public void setJecnRootNode(JecnTreeNode jecnRootNode) {
		this.jecnRootNode = jecnRootNode;
	}

	public JecnAbtractBaseConfigDialog getDialog() {
		return dialog;
	}

}
