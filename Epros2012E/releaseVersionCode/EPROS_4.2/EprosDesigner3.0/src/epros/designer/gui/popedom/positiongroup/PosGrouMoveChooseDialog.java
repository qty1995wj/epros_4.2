package epros.designer.gui.popedom.positiongroup;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/***
 * 岗位组移动 选择岗位组的对话框
 * 
 * @author 2012-05-17
 * 
 */
public class PosGrouMoveChooseDialog extends JecnMoveChooseDialog {

	public PosGrouMoveChooseDialog(List<JecnTreeNode> listMoveNodes, JecnTree moveTree) {
		super(listMoveNodes, moveTree);
	}

	private static Logger log = Logger.getLogger(PosGrouMoveChooseDialog.class);

	@Override
	public boolean savaData(List<Long> ids, Long pid) {

		try {
			ConnectionPool.getPosGroup()
					.movePositionGroups(ids, pid, JecnConstants.getUserId(), this.getMoveNodeType());
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("PosGrouMoveChooseDialog savaData is error！", e);
			return false;
		}

	}

	@Override
	public JecnTree getJecnTree() {

		return new HighEfficiencyPosGroupMoveTree(getListIds());
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return title;
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = ConnectionPool.getPosGroup().getChildPositionGroup(pid, JecnConstants.projectId);
			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}

				}
			}
		} catch (Exception e) {
			log.error("PosGrouMoveChooseDialog validateName is error", e);
			return false;

		}
		return false;
	}

}
