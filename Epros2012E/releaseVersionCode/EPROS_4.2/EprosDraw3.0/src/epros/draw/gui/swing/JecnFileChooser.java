package epros.draw.gui.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Window;
import java.io.File;
import java.util.Locale;

import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.FileChooserUI;

import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 文件选择处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFileChooser extends JFileChooser {

	public JecnFileChooser() {
	}

	public JecnFileChooser(String currentDirectoryPath) {
		super(currentDirectoryPath);
	}

	public JecnFileChooser(File file) {
		super(file);
	}

	/**
	 * 
	 * 选中文件名称编辑框不可编辑
	 * 
	 */
	public void setEidtTextFiled(boolean b) {
		Component comp = getTextFild();
		if (comp != null && comp instanceof JTextField) {
			JTextField field = ((JTextField) comp);
			field.setEditable(b);
			field.setBackground(Color.WHITE);
		}
	}

	/**
	 * 上传文件大小不能超过50M
	 */
	public void approveSelection() {
		File[] selectedFile2 = null;
		if (this.isMultiSelectionEnabled()) { // true 多选 false 是单选
			selectedFile2 = this.getSelectedFiles();
		} else {
			selectedFile2 = new File[] { this.getSelectedFile() };
		}
		if (selectedFile2 != null) {
			for (File file : selectedFile2) {
				if (!checkFileSize(file, 50, "M")) {
					JecnOptionPane.showMessageDialog(null, file.getName() + " "
							+ JecnResourceUtil.getJecnResourceUtil().getValue("noMoreThan"));
					return;
				}
			}
		}
		super.approveSelection();
	}

	/**
	 * 判断文件大小
	 * 
	 * @param :multipartFile:上传的文件
	 * @param size
	 *            : 限制大小
	 * @param unit
	 *            :限制单位（B,K,M,G)
	 * @return boolean:是否大于
	 */
	public static boolean checkFileSize(File file, int size, String unit) {
		long len = file.length();// 上传文件的大小, 单位为字节.
		// 准备接收换算后文件大小的容器
		double fileSize = 0;
		if ("B".equals(unit.toUpperCase())) {
			fileSize = (double) len;
		} else if ("K".equals(unit.toUpperCase())) {
			fileSize = (double) len / 1024;
		} else if ("M".equals(unit.toUpperCase())) {
			fileSize = (double) len / 1048576;
		} else if ("G".equals(unit.toUpperCase())) {
			fileSize = (double) len / 1073741824;
		}
		// 如果上传文件大于限定的容量
		if (fileSize > size) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 获取文件选择器的文件名称编辑框
	 * 
	 * @return
	 */
	public JTextField getTextFiled() {
		Component comp = getTextFild();
		if (comp != null && comp instanceof JTextField) {
			return (JTextField) comp;
		}
		return null;
	}

	protected JDialog createDialog(Component parent) throws HeadlessException {
		String title = getUI().getDialogTitle(this);
		putClientProperty(AccessibleContext.ACCESSIBLE_DESCRIPTION_PROPERTY, title);

		JDialog dialog;
		Window window = JecnOptionPane.getWindowForComponent(parent);
		if (window instanceof Frame) {
			dialog = new JecnDialog((Frame) window, title, true);
		} else {
			dialog = new JecnDialog((Dialog) window, title, true);
		}
		dialog.setComponentOrientation(this.getComponentOrientation());

		// 颜色选择器和按钮面板的容器
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(this, BorderLayout.CENTER);
		// 修改颜色选择器背景颜色
		setColorToSubComp(mainPanel);

		// 添加组件
		dialog.getContentPane().add(mainPanel);

		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		return dialog;
	}

	/**
	 * 
	 * 改变给定组件的所有子组件的背景颜色
	 * 
	 * 
	 * @param com
	 *            Component 组件
	 */
	private void setColorToSubComp(Component com) {
		com.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		if (com instanceof Container) {
			Container cont = (Container) com;
			for (Component sucom : cont.getComponents()) {
				setColorToSubComp(sucom);
			}
		}
	}

	/**
	 * 
	 * 获取文件选择器的文件名称编辑框
	 * 
	 * @return
	 */
	private Component getTextFild() {
		// 获取内容
		Locale local = this.getLocale();
		String fileNameLabelText = UIManager.getString("FileChooser.fileNameLabelText", local);

		FileChooserUI ui = this.getUI();
		int count = ui.getAccessibleChildrenCount(this);
		for (int i = 0; i < count; i++) {
			Accessible accessible = ui.getAccessibleChild(this, i);
			JLabel label = getLabel((JComponent) accessible, fileNameLabelText);
			if (label != null) {
				return label.getLabelFor();
			}
		}
		return null;
	}

	/**
	 * 
	 * 查找文件名称编辑框组件
	 * 
	 * @param comp
	 * @param fileNameLabelText
	 * @return
	 */
	private JLabel getLabel(JComponent comp, String fileNameLabelText) {
		JLabel label = null;
		if (comp instanceof JLabel) {
			if (((JLabel) comp).getText().equals(fileNameLabelText)) {
				label = (JLabel) comp;
			}
		} else if (comp instanceof JComponent) {
			Component[] comps = comp.getComponents();
			for (int i = 0; i < comps.length; i++) {
				if (comps[i] instanceof JComponent) {
					label = getLabel((JComponent) comps[i], fileNameLabelText);
					if (label != null) {
						break;
					}
				}
			}
		}
		return label;
	}

}
