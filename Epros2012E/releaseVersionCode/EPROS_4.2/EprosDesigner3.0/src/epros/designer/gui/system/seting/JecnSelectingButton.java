package epros.designer.gui.system.seting;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 选择列表按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSelectingButton extends JButton {
	/** 装载按钮容器 */
	protected JToolBar toolBar = null;

	public JecnSelectingButton() {
		initComponents();
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	private void initComponents() {
		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 图片内容显示方式：靠左
		this.setHorizontalAlignment(SwingConstants.LEFT);
			// 设置图片
			this.setIcon(JecnFileUtil
					.getImageIcon("toolbar/editShadowSelectColor.gif"));

		// 不显示焦点状态
		this.setFocusPainted(false);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.add(this);
	}
}