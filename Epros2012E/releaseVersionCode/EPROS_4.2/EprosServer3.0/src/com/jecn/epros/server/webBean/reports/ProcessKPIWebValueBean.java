package com.jecn.epros.server.webBean.reports;

/**
 * 
 * kpi值bean
 * 
 * 
 */
public class ProcessKPIWebValueBean {
	/** 流程KPI值主键id */
	private long kpiValueId;
	/** KPI值 */
	private double kpiValue;// 值
	/** 数据统计时间\频率对应（横坐标）对应日期 yyyy-MM */
	private String kpiHorVlaue;

	public long getKpiValueId() {
		return kpiValueId;
	}

	public void setKpiValueId(long kpiValueId) {
		this.kpiValueId = kpiValueId;
	}

	public double getKpiValue() {
		return kpiValue;
	}

	public void setKpiValue(double kpiValue) {
		this.kpiValue = kpiValue;
	}

	public String getKpiHorVlaue() {
		return kpiHorVlaue;
	}

	public void setKpiHorVlaue(String kpiHorVlaue) {
		this.kpiHorVlaue = kpiHorVlaue;
	}
}
