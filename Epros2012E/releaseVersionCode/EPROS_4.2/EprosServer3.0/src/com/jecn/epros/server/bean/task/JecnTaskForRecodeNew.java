package com.jecn.epros.server.bean.task;

import java.util.Date;

/**
 * 任务记录
 * 
 * @author zhangxh
 * 
 */
public class JecnTaskForRecodeNew implements java.io.Serializable {
	private Long id;
	/** 任务 ID */
	private Long taskId;
	/** 开始时间 */
	private Date startTime;
	/** 结束时间 */
	private Date endTime;
	/**
	 * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段 8:事业部经理 9：总经理 10:整理意见
	 */
	private Integer state;
	/** 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段 8:事业部经理 9：总经理 10:整理意见 */
	private Integer upState;
	/** 显示更新的审批阶段 */
	private String upStateName;
	/**
	 * 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
	 * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交 11:编辑
	 */
	private Integer taskElseState;
	/** 任务锁定：所有流程动作都不能做，只有解锁后才能操作 1：未锁定（默认） 0：锁定 */
	private Integer isLock;
	/** 评审意见 */
	private String opinion;
	/** 变更说明 */
	private String taskFixedDesc;
	/** 排序 */
	private int sortId;
	/** 创建人 */
	private Long createPersonId;
	/** 创建人 */
	private String createPersonTemporaryName;
	/** 创建时间 */
	private Date createTime;
	/** 更新时间 */
	private Date updateTime;
	/** 源人 */
	private Long fromPeopleId;
	/** 源人 临时 */
	private String fromPeopleTemporaryName;
	/** 目标人 */
	private Long toPeopleId;
	/** 目标人 临时 */
	private String toPeopleTemporaryName;
	/** 评审次数 */
	private Integer revirewCounts;
	/** 驳回次数 */
	private Integer approveNoCounts;
	/** 目标人ID集合 */
	private String tempToPeopleIds;
	/** 目标人名称集合 */
	private String tempToPeopleNames;

	public String getTempToPeopleIds() {
		return tempToPeopleIds;
	}

	public void setTempToPeopleIds(String tempToPeopleIds) {
		this.tempToPeopleIds = tempToPeopleIds;
	}

	public String getTempToPeopleNames() {
		return tempToPeopleNames;
	}

	public void setTempToPeopleNames(String tempToPeopleNames) {
		this.tempToPeopleNames = tempToPeopleNames;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getUpState() {
		return upState;
	}

	public void setUpState(Integer upState) {
		this.upState = upState;
	}

	public Integer getTaskElseState() {
		return taskElseState;
	}

	public void setTaskElseState(Integer taskElseState) {
		this.taskElseState = taskElseState;
	}

	public Integer getIsLock() {
		return isLock;
	}

	public void setIsLock(Integer isLock) {
		this.isLock = isLock;
	}

	public String getOpinion() {
		return opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

	public String getTaskFixedDesc() {
		return taskFixedDesc;
	}

	public void setTaskFixedDesc(String taskFixedDesc) {
		this.taskFixedDesc = taskFixedDesc;
	}

	public int getSortId() {
		return sortId;
	}

	public void setSortId(int sortId) {
		this.sortId = sortId;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getFromPeopleId() {
		return fromPeopleId;
	}

	public void setFromPeopleId(Long fromPeopleId) {
		this.fromPeopleId = fromPeopleId;
	}

	public Long getToPeopleId() {
		return toPeopleId;
	}

	public void setToPeopleId(Long toPeopleId) {
		this.toPeopleId = toPeopleId;
	}

	public Integer getRevirewCounts() {
		return revirewCounts;
	}

	public void setRevirewCounts(Integer revirewCounts) {
		this.revirewCounts = revirewCounts;
	}

	public Integer getApproveNoCounts() {
		return approveNoCounts;
	}

	public void setApproveNoCounts(Integer approveNoCounts) {
		this.approveNoCounts = approveNoCounts;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCreatePersonTemporaryName() {
		return createPersonTemporaryName;
	}

	public void setCreatePersonTemporaryName(String createPersonTemporaryName) {
		this.createPersonTemporaryName = createPersonTemporaryName;
	}

	public String getFromPeopleTemporaryName() {
		return fromPeopleTemporaryName;
	}

	public void setFromPeopleTemporaryName(String fromPeopleTemporaryName) {
		this.fromPeopleTemporaryName = fromPeopleTemporaryName;
	}

	public String getToPeopleTemporaryName() {
		return toPeopleTemporaryName;
	}

	public void setToPeopleTemporaryName(String toPeopleTemporaryName) {
		this.toPeopleTemporaryName = toPeopleTemporaryName;
	}

	public String getUpStateName() {
		return upStateName;
	}

	public void setUpStateName(String upStateName) {
		this.upStateName = upStateName;
	}
}
