package epros.designer.gui.system.date;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;

import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.JecnTextFieldAndCalendarPanel;

public class DateCommon {
	/** 结束时间 */
	protected JecnTextFieldAndCalendarPanel jTextFieldTime = null;
	/** 结束时间 */
	protected JLabel jLabelTime = null;
	/** 时间 */
	private JLabel timeRequiredLab = new JLabel(JecnProperties.getValue("required"));

	private Date date;

	public DateCommon(Date date, JecnPanel timePanel, int rows, Insets insets, String labName) {
		if (date == null) {
			date = new Date();
		}
		this.date = date;
		jTextFieldTime = new JecnTextFieldAndCalendarPanel();
		jTextFieldTime.setText(JecnTool.getStringbyDate(date));
		timeRequiredLab.setForeground(Color.red);
		// "结束时间:改为预估结束时间:"
		jLabelTime = new JLabel(labName);
		// 结束时间
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		timePanel.add(jLabelTime, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		timePanel.add(jTextFieldTime, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		timePanel.add(timeRequiredLab, c);
	}

	public boolean isUpdate() {
		if (date == null) {
			if (!"".equals(jTextFieldTime.getText())) {
				return true;
			}
		} else {
			if ("".equals(jTextFieldTime.getText())) {
				return true;
			} else {
				if (!JecnTool.getStringbyDate(date).equals(jTextFieldTime.getText())) {
					return true;
				}
			}
		}
		return false;
	}

	public Date getResult() {
		if ("".equals(jTextFieldTime.getText())) {
			return null;
		} else {
			return JecnTool.getDateByString(jTextFieldTime.getText());
		}
	}
}
