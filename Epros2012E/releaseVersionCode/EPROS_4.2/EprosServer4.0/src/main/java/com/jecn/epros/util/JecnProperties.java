package com.jecn.epros.util;

import com.jecn.epros.domain.ViewResultWithMsg;
import com.jecn.epros.domain.system.MaintainNodeBean;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.config.ConfigService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 服务启动成功后，初始化EPROS常用配置项数据源
 */
@Component
@Order(1)
public class JecnProperties implements CommandLineRunner {

    private final static Log log = LogFactory.getLog(JecnProperties.class);
    /**
     * 资源文件库--国际化  中文
     */
    public static ResourceBundle resourceBundleZN;

    /**
     * 资源文件库--国际化 英文
     */
    public static ResourceBundle resourceBundleEN;

    /**
     * 缓存 配置  中文
     */
    public static Map<String, String> custOmResourceChineseMap;

    /**
     * 缓存 配置  英文
     */
    public static Map<String, String> custOmResourceEnglishMap;
    /**
     *
     */
    @Autowired
    ConfigService configService;

    @Override
    public void run(String... args) throws Exception {
        // 英文资源库 : en_CN   中文资源库 : zh_US
        resourceBundleEN = PropertyResourceBundle.getBundle("EprosServer", new Locale("en", "US"));
        resourceBundleZN = PropertyResourceBundle.getBundle("EprosServer", new Locale("zh", "CN"));
        custOmResourceChineseMap = new HashMap<String, String>();
        custOmResourceEnglishMap = new HashMap<String, String>();
        settingUserLanguage();
        initDataBase();
    }

    private void initDataBase() {
        List<MaintainNodeBean> maintainNodeBean = configService.fetchMaintainNode();
        if (maintainNodeBean != null && !maintainNodeBean.isEmpty()) {
            for (MaintainNodeBean bean : maintainNodeBean) {
                custOmResourceChineseMap.put(bean.getMark(), bean.getName());
                custOmResourceEnglishMap.put(bean.getMark(), bean.getEnName());
            }
        }
    }

    public static void settingUserLanguage() {
        ResourceBundle bundle_ZH = resourceBundleZN;
        ResourceBundle bundle_EN = resourceBundleEN;
        for (String key : bundle_ZH.keySet()) {
            custOmResourceChineseMap.put(key, bundle_ZH.getString(key));
        }
        for (String key : bundle_EN.keySet()) {
            custOmResourceEnglishMap.put(key, bundle_EN.getString(key));
        }
    }

    /**
     * @param type 加载的语言类型 1 英语 0 中文
     * @return 返回成功 状态 0 成功  1 失败
     */
    public static ViewResultWithMsg settingUserLanguage(int type) {
        try {
            AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().setLanguage(type);
        } catch (Exception e) {
            e.printStackTrace();
            return new ViewResultWithMsg(false, "fail");
        }
        return new ViewResultWithMsg(true, "Success");
    }

    /**
     * 获取国际化 配置
     *
     * @param type
     * @return
     */
    public static ResourceBundle getResourceBundle(int type) {
        if (type == 1) {
            return resourceBundleEN;
        }
        return resourceBundleZN;
    }

    public static String getValue(String key) {
        try {
            int type = 0;
            if (AuthenticatedUserUtil.getPrincipal() != null) {
                type = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getLanguage();
            }
            if (type == 1) {
                return custOmResourceEnglishMap.get(key.trim());
            }
            return custOmResourceChineseMap.get(key.trim());
        } catch (Exception e) {
            log.error("JecnProperties getValue is error", e);
            return "";
        }
    }

    /**
     * 根据 传入类型 获取重用问 0 是中文 1 是英文
     *
     * @param key
     * @return
     */
    public static String getValueByType(String key, int type) {
        try {
            if (type == 1) {
                return custOmResourceEnglishMap.get(key.trim());
            }
            return custOmResourceChineseMap.get(key.trim());
        } catch (Exception e) {
            log.error("JecnProperties getValue is error", e);
            return "";
        }
    }

}
