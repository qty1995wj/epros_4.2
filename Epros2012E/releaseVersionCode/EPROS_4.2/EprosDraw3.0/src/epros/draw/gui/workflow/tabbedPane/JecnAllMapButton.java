package epros.draw.gui.workflow.tabbedPane;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnFileUtil;

/**
 * 
 * 浏览画图面板全景按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAllMapButton extends JToggleButton implements ActionListener {
	/** 装载按钮容器 */
	private JToolBar toolBar = null;
	/** 隐藏图片 */
	private ImageIcon allMapHiddIcon = null;
	/** 显示图片 */
	private ImageIcon allMapShowIcon = null;
	/** 弹出菜单 */
	private JecnAllShowPopupMenu popupMenu = null;
	/** 全景面板 */
	private JecnAllMapShowPanel allMapShowPanel = null;

	public JecnAllMapButton() {
		initComponents();
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	private void initComponents() {
		// 隐藏图片
		allMapHiddIcon = JecnFileUtil
				.getImageIconforPath("/epros/draw/images/workflow/allMapHidd.gif");
		// 显示图片
		allMapShowIcon = JecnFileUtil
				.getImageIconforPath("/epros/draw/images/workflow/allMapShow.gif");

		// 弹出菜单
		popupMenu = new JecnAllShowPopupMenu();

		// 全景面板
		allMapShowPanel = new JecnAllMapShowPanel();

		// 弹出菜单大小
		Dimension popupSize = new Dimension(allMapShowPanel.getWidth(),
				allMapShowPanel.getHeight());
		popupMenu.setSize(popupSize);
		popupMenu.setPreferredSize(popupSize);
		popupMenu.add(allMapShowPanel);

		// 透明
		this.setOpaque(false);
		// 无边框
		this.setBorder(null);
		// 不显示焦点状态
		this.setFocusPainted(false);
		// 事件
		this.addActionListener(this);
		// 图片
		this.setIcon(allMapHiddIcon);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.add(this);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnAllMapButton) {
			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
					.getWorkflow();
			if (workflow == null) {
				return;
			}

			// 触发弹出菜单按钮
			JecnAllMapButton btn = (JecnAllMapButton) e.getSource();

			if (popupMenu.isVisible()) {// 弹出框显示
				return;
			}

			// 清除角色移动
			JecnDrawMainPanel.getMainPanel().getToolbar()
					.getFormatToolBarItemPanel().setRoleMoveCheckBoxFalse();

			// 清除选中
			// JecnWorkflowUtil.hideAllResizeHandle();

			// 弹出菜单显示
			popupMenu.show(btn, btn.getX() + btn.getWidth(), btn.getHeight());

			// 设置画图面板位置点框的位置点和大小
			allMapShowPanel.setPointPanelBounds();
		}
	}

	class JecnAllShowPopupMenu extends JPopupMenu {

		public void setVisible(boolean b) {
			super.setVisible(b);
			if (!b) {
				// 图片
				JecnAllMapButton.this.setIcon(allMapHiddIcon);
			} else {
				// 图片
				JecnAllMapButton.this.setIcon(allMapShowIcon);
			}
		}
	}
}
