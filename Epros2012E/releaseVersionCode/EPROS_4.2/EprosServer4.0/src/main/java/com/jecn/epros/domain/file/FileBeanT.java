package com.jecn.epros.domain.file;

import com.jecn.epros.util.ConfigUtils;

import java.util.Date;

public class FileBeanT {

    private Long fileId; // 文件ID
    private String fileName; // 文件名称
    private Long peopleID;// 创建人
    private Date createTime;// 创建时间
    private Long updatePersonId;// 更新人
    private Date updateTime;// 更新时间
    private byte[] fileStream;// 文件转化成流的形式传递
    private Long perFileId;// 父ID
    private Integer isDir;// 0是目录,1是文件
    private Long orgId;// 责任部门
    private String orgName;// 责任部门名称（不存在数据库中）
    private String docId;// 文件编号
    private Long isPublic;// 0是秘密,1是公开
    private Long flowId;// 所属流程Id
    private String flowName;// 所属流程名称
    private Integer sortId;// 排序编号
    private Integer saveType;// 保存方式，0本地保存，1数据库大字段保存
    private Long projectId;// 项目ID
    private Long versionId;// 当前对应版本号
    private Long historyId;// 文控ID
    private Integer hide;// 0-隐藏，1-显示
    // 创建人
    private String peopleName;
    private Integer delState;// 删除状态：0：未删除，1：已删除
    private String tPath;// tpath 节点层级关系
    private int tLevel;// level 节点级别，默认0开始
    /**
     * 保密级别
     **/
    private Integer confidentialityLevel;

    private Date pubTime;
    private String viewSort;

    public String getConfidentialityLevelStr() {
        return ConfigUtils.getConfidentialityLevel(confidentialityLevel);
    }

    //------------------
    public Integer getConfidentialityLevel() {
        return confidentialityLevel;
    }

    public void setConfidentialityLevel(Integer confidentialityLevel) {
        this.confidentialityLevel = confidentialityLevel;
    }

    public String gettPath() {
        return tPath;
    }

    public void settPath(String tPath) {
        this.tPath = tPath;
    }

    public int gettLevel() {
        return tLevel;
    }

    public void settLevel(int tLevel) {
        this.tLevel = tLevel;
    }

    public Integer getDelState() {
        return delState;
    }

    public void setDelState(Integer delState) {
        this.delState = delState;
    }

    public String getPeopleName() {
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }

    public Integer getSaveType() {
        return saveType;
    }

    public void setSaveType(Integer saveType) {
        this.saveType = saveType;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileID) {
        this.fileId = fileID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getPeopleID() {
        return peopleID;
    }

    public void setPeopleID(Long peopleID) {
        this.peopleID = peopleID;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Long getUpdatePersonId() {
        return updatePersonId;
    }

    public void setUpdatePersonId(Long updatePersonId) {
        this.updatePersonId = updatePersonId;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public byte[] getFileStream() {
        return fileStream;
    }

    public void setFileStream(byte[] fileStream) {
        this.fileStream = fileStream;
    }

    public Long getPerFileId() {
        return perFileId;
    }

    public void setPerFileId(Long perFileId) {
        this.perFileId = perFileId;
    }

    public Integer getIsDir() {
        return isDir;
    }

    public void setIsDir(Integer isDir) {
        this.isDir = isDir;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Long isPublic) {
        this.isPublic = isPublic;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Integer getHide() {
        return hide;
    }

    public void setHide(Integer hide) {
        this.hide = hide;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    public String getViewSort() {
        return viewSort;
    }

    public void setViewSort(String viewSort) {
        this.viewSort = viewSort;
    }
}
