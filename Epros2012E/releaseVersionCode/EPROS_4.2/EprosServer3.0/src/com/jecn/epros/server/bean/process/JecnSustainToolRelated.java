package com.jecn.epros.server.bean.process;
/**
 * 支持工具和流程的关联表（浏览端）
 * @author Administrator
 */
public class JecnSustainToolRelated implements java.io.Serializable{
	private Long flowToolId;//主键ID
	private Long flowId;//流程ID
	private Long flowSustainToolId;//支持工具ID
	
	public JecnSustainToolRelated(){}
	
	public JecnSustainToolRelated(Long flowToolId){
		this.flowToolId = flowToolId;
	}
	
	public JecnSustainToolRelated(Long flowToolId,Long flowId,Long flowSustainToolId){
		this.flowToolId = flowToolId;
		this.flowId = flowId;
		this.flowSustainToolId = flowSustainToolId;
	}

	public Long getFlowToolId() {
		return flowToolId;
	}

	public void setFlowToolId(Long flowToolId) {
		this.flowToolId = flowToolId;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getFlowSustainToolId() {
		return flowSustainToolId;
	}

	public void setFlowSustainToolId(Long flowSustainToolId) {
		this.flowSustainToolId = flowSustainToolId;
	}
}
