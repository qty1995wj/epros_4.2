package epros.draw.gui.line.shape;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.gui.line.JecnBaseDividingLinePanel;

/**
 * 
 * 分割线
 * 
 * @author ZHOUXY
 */
public class DividingLine extends JecnBaseDividingLinePanel {

	public DividingLine(JecnBaseDivedingLineData flowElementData) {
		super(flowElementData);
	}

	/**
	 * 画线
	 * 
	 * @param g
	 */
	public void paintDiviLine(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; // 定义画笔
		// 记录操作前填充色
		Color oldColor = g2d.getColor();
		// 记录操作前画笔
		Stroke oldStroke = g2d.getStroke();

		g2d.setStroke(this.getFlowElementData().getBodyBasicStroke());// fulin
		// 加
		// 设置虚线用
		g2d.setColor(this.getFlowElementData().getBodyColor());
		g2d.drawLine(this.getStartPoint().x, this.getStartPoint().y, this
				.getEndPoint().x, this.getEndPoint().y);

		// 还原记录画笔和颜色
		g2d.setStroke(oldStroke);
		g2d.setColor(oldColor);
	}

	/**
	 * 获取克隆数据对象
	 */
	public DividingLine clone() {
		return (DividingLine) this.currClone();
	}
}
