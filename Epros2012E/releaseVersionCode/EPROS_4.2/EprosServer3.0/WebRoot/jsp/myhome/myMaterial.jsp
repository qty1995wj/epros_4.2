<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />

	</head>

	<body>
		<div style="height: 97%;">
			<div id="div" class="divBorder" align="center" style="height: 100%;">
				<div id="material" style="margin: 10px 15px 15px 10px; width: 95%;"
					align="left">
					<table width="100%">
						<tr>
							<td style="font: bold 13px; width: 10%">
								<!-- 个人资料 -->
								<s:text name="personalData" />
							</td>
							<td align="right" style="color: red;">
								<!-- 修改密码 -->
								<span style="cursor: pointer;" onclick="updatePassword()"><s:text
										name="updatePassword" /> </span>
							</td>
						</tr>
						<tr>
							<td>
								<!-- 登录名：-->
								<s:text name="loginNameC" />
							</td>
							<td>
								<s:property value="#session.webLoginBean.jecnUser.loginName" />
							</td>
						</tr>
						<tr>
							<td>
								<!-- 真实姓名：-->
								<s:text name="realNameC" />
							</td>
							<td>
								<s:property value="#session.webLoginBean.jecnUser.trueName" />
							</td>
						</tr>
						<tr>
							<td>
								<!-- 系统角色： -->
								<s:text name="systemRole" />
							</td>
							<td>
								<s:iterator value="#session.webLoginBean.roleList">
									<s:property value="name" />&nbsp;
								</s:iterator>
							</td>
						</tr>
					</table>
				</div>

				<div id="orgList" align="center">
					<table width="95%" border="0" cellpadding="0" cellspacing="1"
						align="center" style="background-color: #D0D0D0"
						style="table-layout: fixed;">
						<tr class="FixedTitleRow" align="center">
							<td width="2%" class="gradient">
							</td>
							<td width="48%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 岗位名称 -->
								<s:text name="positionName" />
							</td>
							<td width="48%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 部门名称 -->
								<s:text name="orgName" />
							</td>
							<td width="2%" class="gradient">
							</td>
						</tr>
						<s:iterator value="#session.webLoginBean.listPosBean" status="st">
							<tr align="center" style="background-color: #ffffff"
								onmouseover="style.backgroundColor=='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';" height="20">
								<td class="gradient" style="word-break: break-all;">
									<s:property value="#st.index+1" />
								</td>
								<td class="t5" style="word-break: break-all;">
									<a
										href="orgSpecification.action?posId=<s:property value='posId'/>"
										target='_blank' style="cursor: pointer;" class="table"> <s:property
											value="posName" /> </a>
								</td>
								<td class="t5" style="word-break: break-all;">
									<a href="organization.action?orgId=<s:property value='orgId'/>"
										target='_blank' style="cursor: pointer;" class="table"> <s:property
											value="orgName" /> </a>
									<br>
								</td>
								<td class="t5" style="word-break: break-all;">
								</td>
							</tr>
						</s:iterator>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>