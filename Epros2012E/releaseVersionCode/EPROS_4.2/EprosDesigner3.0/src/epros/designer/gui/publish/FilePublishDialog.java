package epros.designer.gui.publish;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 知识库文件发布
 * 
 * @author ZHANGXIAOHU 2012-10-23
 */
public class FilePublishDialog extends BasePublishDialog {

	public FilePublishDialog(JecnTreeNode treeNode) {
		super(treeNode);
		if (TreeNodeType.file != treeBean.getTreeNodeType()) {// 不是知识库文件发布
			log.error("FilePublishDialog ！　treeBean.getTreeNodeType()　＝"
					+ treeBean.getTreeNodeType());
			return;
		}
	}

	/**
	 * 初始化变量
	 * 
	 */
	public void initialize() {
		type = 1;
		prfName = JecnProperties.getValue("fileNameC");
		strTitle = JecnProperties.getValue("filePubI");
	}

}
