package epros.designer.gui.system.seting;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.JecnToolbarButton;
import epros.designer.gui.system.config.process.JecnConfigProcess;
import epros.designer.gui.system.config.ui.dialog.JecnFileConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFlowConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnMailConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnPubSetConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnRuleConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnTotalMapConfigJDialog;
import epros.designer.util.JecnProperties;

/**
 * 
 * 配置按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSet implements ActionListener {
	/** 权限设置按钮标识 */
	private final String PUR_SET_CMD = "purSet";
	/** 制度配置按钮标识 */
	private final String RULE_SET_CMD = "ruleSet";
	/** 流程配置按钮标识 */
	private final String PARTMAP_SET_CMD = "partMapSet";
	/** 流程地图配置按钮标识 */
	private final String TOTALMAP_SET_CMD = "totalMapSet";
	/** 文件配置按钮标识 */
	private final String FILE_SET_CMD = "fileSet";
	/** 邮箱配置按钮标识 */
	private final String MAIL_SET_CMD = "mailSet";
	/** 选择按钮标识 */
	private final String SELECTEDING_CMD = "selectedingRecover";

	/** 配置容器 */
	private JecnGroupButton groupRecoverBtn = null;
	/** 当前选中配置 */
	private JecnSelectedButton currSelectedBtn = null;
	/** 选择配置按钮 */
	private JecnSelectingButton selectingBtn = null;

	/** 权限设置 */
	private JecnToolbarButton pubSetButton = null;
	/** 制度配置 */
	private JecnToolbarButton ruleTaskButton = null;
	/** 流程配置 */
	private JecnToolbarButton processTaskButton = null;
	/** 流程地图配置 */
	private JecnToolbarButton totalMapTaskButton = null;
	/** 文件配置 */
	private JecnToolbarButton fileTaskButton = null;
	/** 邮箱配置 */
	private JecnToolbarButton mailSetButton = null;

	private JecnPopupMenuButton popupMenu = null;

	public JecnSet() {
		// 初始化组件
		initComponent();
	}

	/**
	 * 
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {
		// 当前选中项
		currSelectedBtn = new JecnSelectedButton(this);
		// 选择列表按钮
		selectingBtn = new JecnSelectingButton();
		// 容器
		groupRecoverBtn = new JecnGroupButton(currSelectedBtn, selectingBtn);

		// 权限设置
		pubSetButton = new JecnToolbarButton(JecnProperties.getValue("setTitlePubName"), "eprosSet");
		// 邮箱配置
		mailSetButton = new JecnToolbarButton(JecnProperties.getValue("setTitleMailName"), "processOperation");
		// 流程配置
		processTaskButton = new JecnToolbarButton(JecnProperties.getValue("setTitlePartMapName"), "processTaskConfig");
		// 流程地图配置
		totalMapTaskButton = new JecnToolbarButton(JecnProperties.getValue("setTitleTotalName"), "totalMapTaskConfig");
		// 制度配置
		ruleTaskButton = new JecnToolbarButton(JecnProperties.getValue("setTitleRuleName"), "ruleTaskConfig");
		// 文件配置
		fileTaskButton = new JecnToolbarButton(JecnProperties.getValue("setTitleFileName"), "fileTaskConifg");

		List<JecnToolbarButton> btnList = new ArrayList<JecnToolbarButton>();
		btnList.add(processTaskButton);// 流程配置

		if (JecnConstants.versionType != 1) {// 除去“无浏览端标准版”
			btnList.add(totalMapTaskButton);// 流程地图配置
		}
		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {// true：
																					// 完整版
																					// false：标准版
			btnList.add(ruleTaskButton);// 制度配置
		}
		if (JecnConstants.versionType != 1) {// 除去“无浏览端标准版”
			btnList.add(fileTaskButton);// 文件配置
			btnList.add(mailSetButton);// 邮箱配置
		}

		btnList.add(pubSetButton);// 权限设置

		popupMenu = new JecnPopupMenuButton(btnList);

		selectingBtn.setActionCommand(SELECTEDING_CMD);
		processTaskButton.setActionCommand(PARTMAP_SET_CMD);// 流程配置
		totalMapTaskButton.setActionCommand(TOTALMAP_SET_CMD);// 流程地图配置
		ruleTaskButton.setActionCommand(RULE_SET_CMD);// 制度配置
		fileTaskButton.setActionCommand(FILE_SET_CMD);// 文件配置
		pubSetButton.setActionCommand(PUR_SET_CMD);// 权限设置
		mailSetButton.setActionCommand(MAIL_SET_CMD);// 邮箱配置

		Dimension thisSize = new Dimension(70, 20);
		// 大小
		currSelectedBtn.setPreferredSize(thisSize);
		currSelectedBtn.setMaximumSize(thisSize);
		currSelectedBtn.setMinimumSize(thisSize);
		currSelectedBtn.setSize(thisSize);

		// 选择按钮
		selectingBtn.addActionListener(this);
		// 权限设置
		pubSetButton.addActionListener(this);
		// 邮箱配置
		mailSetButton.addActionListener(this);
		// 流程配置
		processTaskButton.addActionListener(this);
		// 流程地图配置
		totalMapTaskButton.addActionListener(this);
		// 制度配置
		ruleTaskButton.addActionListener(this);
		// 文件配置
		fileTaskButton.addActionListener(this);

		currSelectedBtn.setTextIcon(processTaskButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!JecnDesignerCommon.isAdminTip()) {
			return;
		}
		String cmdStr = e.getActionCommand();

		if (SELECTEDING_CMD.equals(cmdStr)) {// 选择按钮
			popupMenu.show(currSelectedBtn, 30, 25);
			popupMenu.setPopupSize(new Dimension(currSelectedBtn.getWidth() + 35, popupMenu.getHeight()));
			popupMenu.repaint();
			return;
		}

		if (TOTALMAP_SET_CMD.equals(cmdStr)) {// 流程地图配置
			Dimension thisSize = new Dimension(90, 20);
			// 大小
			currSelectedBtn.setPreferredSize(thisSize);
			currSelectedBtn.setMaximumSize(thisSize);
			currSelectedBtn.setMinimumSize(thisSize);
			currSelectedBtn.setSize(thisSize);
		} else {
			if (currSelectedBtn.getWidth() != 70) {
				Dimension thisSize = new Dimension(70, 20);
				// 大小
				currSelectedBtn.setPreferredSize(thisSize);
				currSelectedBtn.setMaximumSize(thisSize);
				currSelectedBtn.setMinimumSize(thisSize);
				currSelectedBtn.setSize(thisSize);
			}
		}

		if (popupMenu.isVisible()) {
			popupMenu.setVisible(false);
		}
		if (PARTMAP_SET_CMD.equals(cmdStr)) {// 流程配置
			JecnFlowConfigJDialog dialog = new JecnFlowConfigJDialog(JecnConfigProcess.getPartMapConfigData());
			dialog.setVisible(true);
			currSelectedBtn.setTextIcon(processTaskButton);
		} else if (TOTALMAP_SET_CMD.equals(cmdStr)) {// 流程地图配置
			JecnTotalMapConfigJDialog dialogMap = new JecnTotalMapConfigJDialog(JecnConfigProcess
					.getTotalMapConfigData());
			dialogMap.setVisible(true);
			currSelectedBtn.setTextIcon(totalMapTaskButton);
		} else if (RULE_SET_CMD.equals(cmdStr)) {// 制度配置
			JecnRuleConfigJDialog dialogRule = new JecnRuleConfigJDialog(JecnConfigProcess.getRuleConfigData());
			dialogRule.setVisible(true);
			currSelectedBtn.setTextIcon(ruleTaskButton);
		} else if (FILE_SET_CMD.equals(cmdStr)) {// 文件配置
			JecnFileConfigJDialog dialogFile = new JecnFileConfigJDialog(JecnConfigProcess.getFileConfigData());
			dialogFile.setVisible(true);
			currSelectedBtn.setTextIcon(fileTaskButton);
		} else if (PUR_SET_CMD.equals(cmdStr)) {// 权限设置
			JecnPubSetConfigJDialog pubDialog = new JecnPubSetConfigJDialog(JecnConfigProcess.getPubConfigData());
			pubDialog.setVisible(true);
			currSelectedBtn.setTextIcon(pubSetButton);
		} else if (MAIL_SET_CMD.equals(cmdStr)) {// 邮箱配置
			JecnMailConfigJDialog mailConfigJDialog = new JecnMailConfigJDialog(JecnConfigProcess.getMailConfigData());
			mailConfigJDialog.setVisible(true);
			currSelectedBtn.setTextIcon(mailSetButton);
		}
	}

	public JecnGroupButton getGroupRecoverBtn() {
		return groupRecoverBtn;
	}
}
