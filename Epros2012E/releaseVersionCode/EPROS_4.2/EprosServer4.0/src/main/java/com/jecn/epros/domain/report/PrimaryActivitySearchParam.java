package com.jecn.epros.domain.report;

import com.jecn.epros.domain.EnumClass;

import java.util.List;

/**
 * 关键合规点搜索
 */
public class PrimaryActivitySearchParam extends ReportSearchParam {

    private String processName;
    private String figureText;
    private String orgName;
    /**
     * 业务类型
     */
    private List<String> bussTypes;
    /**
     * 是否是下载
     */
    private boolean download;

    public String getFigureText() {
        return figureText;
    }

    public void setFigureText(String figureText) {
        this.figureText = figureText;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public List<String> getBussTypes() {
        return bussTypes;
    }

    public void setBussTypes(List<String> bussTypes) {
        this.bussTypes = bussTypes;
    }

    public boolean isDownload() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download = download;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }
}
