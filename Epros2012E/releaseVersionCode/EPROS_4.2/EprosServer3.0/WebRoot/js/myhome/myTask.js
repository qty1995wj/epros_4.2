// Ext.onReady(function(){
function myTask() {

	myTaskStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "getMyTask.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "taskId",
			mapping : "taskId"
		}, {
			name : "taskName",
			mapping : "taskName"
		}, {
			name : "taskType",
			mapping : "taskType"
		}, {
			name : "createName",
			mapping : "createName"
		}, {
			name : "resPeopleName",
			mapping : "resPeopleName"
		}, {
			name : "taskStage",
			mapping : "taskStage"
		}, {
			name : "stageName",
			mapping : "stageName"
		}, 
//		{   
//			name : "startTime",
//			mapping : "startTime"
//		}, {
//			name : "endTime",
//			mapping : "endTime"
//		}, 
		{
			name : "taskElseState",
			mapping : "taskElseState"
		}, {
			name : "reJect",
			mapping : "reJect"
		} ])

	});

	myTaskColumnModel = new Ext.grid.ColumnModel( [ new Ext.grid.RowNumberer(),
			{
				header : "taskId",
				dataIndex : "taskId",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_taskName,
				dataIndex : "taskName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_taskType,
				dataIndex : "taskType",
				align : "center",
				menuDisabled : true,
				renderer : taskTypeRenderer
			}, {
				header : i_creatName,
				dataIndex : "createName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : '责任人',
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : "taskStage",
				dataIndex : "taskStage",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_stage,
				dataIndex : "stageName",
				align : "center",
				menuDisabled : true
			}, {
				header : "taskElseState",
				dataIndex : "taskElseState",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : "reJect",
				dataIndex : "reJect",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, 
//			{
//				header : i_startTime,
//				dataIndex : "startTime",
//				align : "center",
//				menuDisabled : true,
//				sortable : true
//			    
//			}, { 
//				header : i_endTime,
//				dataIndex : "endTime",
//				align : "center",
//				menuDisabled : true,
//				sortable : true
//			}, 
			{
				header : i_operation,// 操作
				align : "center",
				menuDisabled : true,
				renderer : myTaskRenderer
			} ]);
	myTaskBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : myTaskStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	myTaskGridPanel = new Ext.grid.GridPanel( {
		id : "myTaskGridPanel_id",
		renderTo : 'myTask_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 162,

		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样。
		},
		frame : false,
		store : myTaskStore,
		colModel : myTaskColumnModel,
		bbar : myTaskBbar
	})
	myhome_index_id = "myhome_mytask_id";
}

/**
 * 任务列表操作
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 */
function myTaskRenderer(value, cellmeta, record) {
	// 任务ID
	var taskId = record.data["taskId"];
	// 任务阶段
	var taskStage = record.data["taskStage"];
	// 拟稿人操作，0是驳回重新提交状态,1是评审意见整理状态
	var taskElseState = record.data["taskElseState"];
	// 任务类型
	var taskType = record.data["taskType"];
	// 任务类型
	var reJect = record.data["reJect"];

	// 审批操作连接
	var varApp;
	// 删除操作连接
	var varDelete;
	// 拟稿人操作
	var varState;
	// div
	var varDiv;

	if (taskElseState == null) {
		return;
	}

	if (taskId == null || taskStage == null) {// 任务主键ID或任务阶段不存在
		return;
	}
	var appUrl = "taskId=" + taskId + "&taskType=" + taskType + "&taskStage="
			+ taskStage;

	var varReject;
	if (reJect != null && reJect == 1) {// 撤回
		varReject = "<a style='cursor: pointer;' onclick='reBackMyTask("
				+ taskId + "," + taskType + ",4);' class='a-operation'>"
				+ i_reCall + "</a>";
	}
	// 查阅
	var varView = "<a target='_blank' href='taskLookUp.action?" + appUrl
			+ "' class='a-operation'>" + i_lookUp + "</a>";
	// 删除操作 "<a href='myTaskDelete.action?" +appUrl+ "'
	// class='a-operation'>"+i_Delete+"</a>";
	varDelete = "<a style='cursor: pointer;' onclick='deleteMyTask(" + taskId
			+ "," + taskType + ");' class='a-operation'>" + i_Delete + "</a>";

	// taskElseState : 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;3:拟稿人无审批状态;4:拟稿人为当前阶段审批人
	if (taskStage == 0 || taskStage == 10 || taskElseState == 3
			|| taskElseState == 4) {// 拟稿人阶段或整理意见阶段
		if (taskElseState == 0) {// 重新提交状态
			varState = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "&taskOperation=" + 1 + "' class='a-operation'>"
					+ i_reSubmit + "</a>";
		} else if (taskElseState == 1) {// 评审意见整理
			varState = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "&taskOperation=" + 2 + "' class='a-operation'>"
					+ i_opinion + "</a>";
		}
		if (taskElseState == 3) {// 拟稿人不是审批人，只有查阅和删除权限
			varDiv = "<div >";
			if (varReject != null) {
				varDiv = varDiv + varReject + " | " + varView + " | "
						+ varDelete;
			} else {
				varDiv = varDiv + varView + " | " + varDelete;
			}
			varDiv = varDiv + "</a><a></a></div>";
		} else if (taskElseState == 4) {// 拟稿人是审批人
			if (varReject != null) {// 存在驳回权限
				alert(varReject);
			}
			varApp = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "' class='a-operation'>" + i_approve + "</a>";
			varDiv = "<div >" + varApp + " | " + varView + " | " + varDelete
					+ "</a><a></a></div>";
		} else if (taskElseState == 5 && (taskStage == 10 || taskStage == 0)) {// 打回整理撤回操作，或打回操作
			// 我的任务显示撤回按钮，无拟稿人情况
			varDiv = reCallNoDraft(varReject, varView);
		} else {
			varDiv = "<div >" + varState + " | " + varView + " | " + varDelete
					+ "</a><a></a></div>";
		}
	} else if (taskStage != 5) {// 其他审核阶段按钮为审批 (当前审批阶段)
		if (reJect == 1) {// 如果存在驳回状态
			if (taskElseState == 3) {// 拟稿人，只有查阅和删除权限
				// 我的任务显示撤回按钮，拟稿人操作
				varDiv = reCallIsDraft(varReject, varView);
			} else {
				// 我的任务显示撤回按钮，无拟稿人情况
				varDiv = reCallNoDraft(varReject, varView);
			}
		} else {

			varApp = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "' class='a-operation'>" + i_approve + "</a>";
			// 组织显示的按钮
			varDiv = "<div >" + varApp + " | " + varView + "</a><a></a></div>";
		}
	}
	return varDiv;
}
/**
 * 我的任务显示撤回按钮，拟稿人操作
 * 
 * @param {}
 *            varReject
 * @param {}
 *            varView
 * @return {}
 */
function reCallIsDraft(varReject, varView) {
	return "<div >" + varReject + " | " + varDelete + " | " + varView
			+ "</a><a></a></div>";
}
/**
 * 我的任务显示撤回按钮，无拟稿人情况
 * 
 * @return {}
 */
function reCallNoDraft(varReject, varView) {
	return "<div >" + varReject + " | " + varView + "</a><a></a></div>";
}
/**
 * 我的任务删除任务
 */
function deleteMyTask(taskId, taskType) {
	// "是否删除！"
	Ext.MessageBox.confirm(i_tip, i_HAS_DELETE_TASK, function(btn) {
		if (btn == 'yes') {// 点击的是‘是’
				Ext.Ajax.request( {
					url : basePath + "myTaskDelete.action?",
					params : "taskId=" + taskId + "&taskType=" + taskType,
					success : function() {
						myTaskStore.remove(myTaskGridPanel.getSelectionModel()
								.getSelected())
					}
				})
			}

		});
}

/**
 * 我的任务撤回任务
 */
function reBackMyTask(taskId, taskType, taskOperation) {
	// "是否撤回！"
	Ext.MessageBox.confirm(i_tip, i_whetherReCall, function(btn) {
		if (btn == 'yes') {// 点击的是‘是’
				Ext.Ajax.request( {
					url : basePath + "taskReCall.action?",
					params : "taskId=" + taskId + "&taskType=" + taskType
							+ "&taskOperation=" + taskOperation,
					success : function() {
//						// 撤回成功！
//					Ext.MessageBox.alert(i_tip, i_reCallSuccess);
					//跳转到我的主页
					reportsPersonallyFunction();
				}
				})
			}

		});
}

function taskCheckbox() {
	if (!document.getElementById("checkbox").checked) {
		document.getElementById("startTime").value = "";
		document.getElementById("endTime").value = "";
	}
}

function secBoard(type) {
	if (type == 0) {
		document.getElementById("basicInformation").style.display = "block";
		document.getElementById("examinationAndApprovalRecords").style.display = "none";
		document.getElementById("tdBasicInformation").style.background = "#FFFFFF";
		document.getElementById("tdExaminationAndApprovalRecords").style.background = "#E3E3E3";
		document.getElementById("tdBasicInformation").style.color = "#000000";
		document.getElementById("tdExaminationAndApprovalRecords").style.color = "#B6B6B6";
	} else if (type == 1) {
		document.getElementById("examinationAndApprovalRecords").style.display = "block";
		document.getElementById("basicInformation").style.display = "none";
		document.getElementById("tdExaminationAndApprovalRecords").style.background = "#FFFFFF";
		document.getElementById("tdBasicInformation").style.background = "#E3E3E3";
		document.getElementById("tdExaminationAndApprovalRecords").style.color = "#000000";
		document.getElementById("tdBasicInformation").style.color = "#B6B6B6";
	}
}
/**
 * 我的任务搜索按钮
 * 
 * 
 */
function myTaskSearch() {
	// 任务名称
	var taskName = document.getElementById("taskName").value;
	// 任务创建人主键ID
	var createUserId = document.getElementById("createUserId_myTask").value;
	// 任务类型
	var taskType = document.getElementById("searchTaskType").value;
	var taskStage;
	if (taskType == -1) {// 查询所有任务
		// 审批阶段
		taskStage = -1;
	} else if (taskType == 0) {// 流程任务
		taskStage = document.getElementById("flowState").value;
	} else if (taskType == 1) {// 文件任务
		taskStage = document.getElementById("fileState").value;
	} else if (taskType == 2) {// 制度任务
		taskStage = document.getElementById("ruleState").value;
	} else if (taskType == 4) {// 流程地图任务
		taskStage = document.getElementById("flowMapState").value;
	}
	// 开始时间
	var startTime = document.getElementById("startTime").value;
	// 结束时间
	var endTime = document.getElementById("endTime").value;
	if((endTime!=null&&endTime!="")&&(startTime==null||startTime==""))
	{
		Ext.Msg.alert(i_tip,i_startTimeIsNotNull);
		return;
	}
	// 结束时间大于开始时间
	if (startTime > endTime) {
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip, i_startTimeIsLessEndTime);
		return;
	}

	myTaskStore.setBaseParam("taskName", taskName);
	if (createUserId && createUserId != "") {
		myTaskStore.setBaseParam("createUserId", createUserId);
	} else {
		myTaskStore.setBaseParam("createUserId", 0);
	}

	if (taskType && taskType != "") {
		myTaskStore.setBaseParam("taskType", taskType);
	}
	if (taskStage && taskStage != "") {
		myTaskStore.setBaseParam("taskStage", taskStage);
	}
	myTaskStore.setBaseParam("taskName", taskName);
	myTaskStore.setBaseParam("startTime", startTime);
	myTaskStore.setBaseParam("endTime", endTime);
	myTaskStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});

}

function searchTaskType() {
	var state = document.getElementById("searchTaskType").value;
	if (state == 0) {// 流程图任务
		document.getElementById("taskType").style.display = 'none';
		document.getElementById("flowState").style.display = '';
		document.getElementById("flowMapState").style.display = 'none';
		document.getElementById("fileState").style.display = 'none';
		document.getElementById("ruleState").style.display = 'none';
		$("#flowState option[value='-1']").attr("selected","selected");
	} else if (state == 1) {// 文件任务
		document.getElementById("taskType").style.display = 'none';
		document.getElementById("flowState").style.display = 'none';
		document.getElementById("flowMapState").style.display = 'none';
		document.getElementById("fileState").style.display = '';
		document.getElementById("ruleState").style.display = 'none';
		$("#fileState option[value='-1']").attr("selected","selected");
	} else if (state == 2) {// 制度任务
		document.getElementById("taskType").style.display = 'none';
		document.getElementById("flowState").style.display = 'none';
		document.getElementById("flowMapState").style.display = 'none';
		document.getElementById("fileState").style.display = 'none';
		document.getElementById("ruleState").style.display = '';
		$("#ruleState option[value='-1']").attr("selected","selected");
	} else if (state == 4) {// 流程地图任务
		document.getElementById("taskType").style.display = 'none';
		document.getElementById("flowState").style.display = 'none';
		document.getElementById("flowMapState").style.display = '';
		document.getElementById("fileState").style.display = 'none';
		document.getElementById("ruleState").style.display = 'none';
		$("#flowMapState option[value='-1']").attr("selected","selected");
	} else if (state == -1) {
		document.getElementById("taskType").style.display = '';
		document.getElementById("flowState").style.display = 'none';
		document.getElementById("flowMapState").style.display = 'none';
		document.getElementById("fileState").style.display = 'none';
		document.getElementById("ruleState").style.display = 'none';
	}
}