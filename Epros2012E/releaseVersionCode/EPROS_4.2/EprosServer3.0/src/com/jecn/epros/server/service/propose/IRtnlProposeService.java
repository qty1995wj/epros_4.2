package com.jecn.epros.server.service.propose;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.propose.JecnFlowRtnlPropose;
import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCountDetail;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.common.IBaseService;

/**
 * 合理化建议Service 操作类 2013-09-02
 * 
 */

public interface IRtnlProposeService extends IBaseService<JecnRtnlPropose, String> {

	/***
	 * 针对单个流程的合理化建议 页面Tab切换 获取数据
	 * 
	 * @param proposeState
	 *            建议状态(0:未读1:已读2:采纳3:全部)
	 * @param isResponsible
	 *            是否流程责任人：true ：是， false ：不是
	 * @param peopleId
	 *            当选择我的建议时，根据当前用户ID获取数据
	 * @param flowId
	 *            流程ID
	 * @param start
	 * @param limit
	 * @param rtnlType
	 *            合理化建议类型：0：流程；1：制度
	 * @return
	 * @throws Exception
	 */
	public List<JecnRtnlPropose> getListFlowRtnlPropose(int proposeState, Long peopleId, Long flowId,
			int isCheckedMyPropose, int rtnlType, boolean isAdmin, int start, int limit) throws Exception;

	/***
	 * 针对单个流程的合理化建议 删除未读建议,只能删除未读状态下的建议
	 * 
	 * @param proposeId
	 *            建议ID
	 * @param 流程ID
	 * @param isEdit
	 *            是否处于编辑状态，编辑中建议不能删除：0:否 1:是
	 * @fileIds 附件建议表中主键Id集合
	 * @throws Exception
	 */
	public boolean deleteFlowPropose(String proposeId, Long flowId, int isEdit, int isCreator) throws Exception;

	/****
	 * 针对单个流程的合理化建议 获取不同状态下合理化建议 总数
	 * 
	 * @param flowId
	 *            流程ID
	 * @param peopleId
	 *            登录用户ID
	 * @param proposeState
	 *            建议的状态：0:未读1:已读2:采纳
	 * @param isCheckedMyPropose
	 *            是否选中我的建议/我处理的建议的复选框 0:没选中，1：选中
	 * @return
	 * @throws Exception
	 */
	public int getFlowProposeTotal(Long flowId, Long loginpeopleId, int proposeState, int isCheckedMyPropose,
			int rtnlType) throws Exception;

	/***
	 * 导航===获取不同状态下合理化建议 ====总数
	 * 
	 * @param flowRtnlPropose
	 *            导航搜索条件Bean
	 * @param flowId流程ID
	 * @param isResponsible是否是流程责任人
	 * @param peopleId登录人Id
	 * @param proposeState当前页显示的状态
	 * @param submitPeopleId合理化建议提交人ID
	 * @param startTime创建时间
	 * @param endTime创建时间
	 * @param isSelectedHandle界面我处理的建议是否选中
	 *            ： 0：否，1：是
	 * @param isSelectedSubmit界面我提交的建议是否选中
	 *            ： 0：否，1：是
	 * @param proFlowRefRation设计器流程责任人是否配置处理建议
	 *            ：0：默认都不选中，1：选中
	 * @param proPubOrSecRation密级
	 *            ：0：秘密，1：公开
	 * @param isAdmin
	 * @param listPosIds登录人员所在岗位集合
	 * @return
	 * @throws Exception
	 */
	public int getNavigationProposeTotal(JecnFlowRtnlPropose flowRtnlPropose, int proposeState) throws Exception;

	/**
	 * 获取流程主表数据
	 * 
	 * @param flowId
	 * @return
	 */
	public JecnFlowStructure getFlowStructure(Long flowId) throws Exception;

	/**
	 * 获取流程基本表数据
	 * 
	 * @param flowId
	 * @return
	 */
	public JecnFlowBasicInfo getJecnFlowBasicInfo(Long flowId) throws Exception;

	/**
	 * 获取流程基本表数据集合
	 * 
	 * @param flowId
	 * @return
	 */
	public List<JecnFlowBasicInfo> getJecnFlowBasicInfoList() throws Exception;

	/**
	 * 获取对应人员ID的人员名称
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public List<JecnUser> getJecnUserByResId(List<Long> peopleId) throws Exception;

	/***
	 * 更新合理化建议状态
	 * 
	 * @param proposeId
	 *            合理化建议主键ID
	 * @param proposeState
	 *            状态
	 * @param loginUserId
	 *            登录用户ID
	 * @param strContent
	 *            回复/采纳内容
	 * @param intflag
	 *            判断是回复内容(0)还是采纳内容（1）标识
	 * @param dateString
	 *            当前时间
	 * @throws Exception
	 */
	public String updateProposeState(String proposeId, int proposeState, Long loginUserId, String strContent,
			String dateString) throws Exception;

	/***
	 * 将当前未读状态下的合理化建议更新为已读状态
	 * 
	 * @param proposeIds
	 *            合理化建议主键ID
	 * @throws Exception
	 */
	public String updateAllUnReadToRead(Long relatedId, int rtnlType) throws Exception;

	/***
	 * 获取建议附件表单条数据
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnRtnlProposeFile getJecnRtnlProposeFile(String id) throws Exception;

	/***
	 * 获取建议表单条数据
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnRtnlPropose getJecnRtnlPropose(String id) throws Exception;

	/***
	 * 合理化建议搜索
	 * 
	 * @param rtnlPropose
	 *            建议表实体类
	 * @param start
	 * @param limit
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowRtnlPropose> searchListRtnlPropose(List<JecnRtnlPropose> listAllStatePropose,
			JecnFlowRtnlPropose flowRtnlPropose) throws Exception;

	/***
	 * 添加合理化建议数据
	 * 
	 * @param rtnlPropose
	 *            合理化建议
	 * @param jecnRtnlProposeFile
	 *            合理化建议附件表
	 * @param flowId
	 *            流程ID
	 * @throws Exception
	 */
	public String addRtnlPropose(JecnRtnlPropose rtnlPropose, JecnRtnlProposeFile jecnRtnlProposeFile, int inflag)
			throws Exception;

	/***
	 * 根据主键ID获取合理化建议附件表Bean
	 * 
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	public JecnRtnlProposeFile getPropoFileById(String fileId) throws Exception;

	/**
	 * 根据建议表ID把回复内容，回复人，回复时间设为空
	 * 
	 * @param id
	 *            建议表ID
	 * @throws Exception
	 */
	public String updateJecnRtnlPropose(String id) throws Exception;

	/***
	 * 获取导航栏合理化建议数据集合
	 * 
	 * @param proposeState
	 * @param isResponsible
	 * @param peopleId
	 * @param submitPeopleId
	 * @param startTime
	 * @param endTime
	 * @param isSelectedHandle
	 * @param isSelectedSubmit
	 * @param rtnlTypeId
	 *            合理化建议类型
	 * @return
	 * @throws Exception
	 */
	public List<JecnRtnlPropose> getListNavigationRtnlPropose(JecnFlowRtnlPropose flowRtnlPropose, int proposeState,
			int start, int limit) throws Exception;

	/***
	 * 删除合理化建议附件
	 * 
	 * @param fileId
	 *            附件主键ID
	 * @throws Exception
	 */
	public void deleteProposeFileById(String fileId) throws Exception;

	/***
	 * 编辑建议：是否符合显示DIV条件
	 * 
	 * @param proposeId
	 *            合理化建议ID
	 * @param flowId
	 *            合理化建议所属流程Id
	 * @param state
	 *            当前建议状态
	 * @return
	 * @throws Exception
	 */

	public boolean eidtIsShowDiv(String proposeId, Long flowId, int state) throws Exception;

	/***
	 * 获取制度基本信息
	 * 
	 * @param ruleId制度基本信息表主键ID
	 * @return
	 * @throws Exception
	 */
	public Rule getRuleInfo(Long ruleId) throws Exception;

	/**
	 * 获取制度基本信息表数据集合
	 * 
	 * @return
	 */
	public List<Rule> getRuleInfoList() throws Exception;

	/**
	 * 获得流程名称
	 * 
	 * @param flowRuleName输入的流程
	 *            、制度名称
	 * @param type
	 *            0：流程，1：制度
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findJecnFlowStructureName(String flowRuleName, int type) throws Exception;

	/**
	 * 是否是责任人
	 * 
	 * @param loginUserId
	 * @param fileId
	 * @param rtnlType
	 * @return
	 * @throws Exception
	 */
	public boolean isRes(Long loginUserId, Long relatedId, int rtnlType) throws Exception;

	/**
	 * 获得单个的合理化建议，并将显示按钮的配置项初始化
	 * 
	 * @param rtnlProposeId
	 * @param jecnUser
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public JecnRtnlPropose getJecnRtnlPropose(String rtnlProposeId, JecnUser jecnUser, boolean isAdmin)
			throws Exception;

	/**
	 * 合理化建议提交人统计
	 * 
	 * @param personIds
	 *            人员id使用逗号拼接
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<JecnRtnlProposeCountDetail> getProposeCreateCount(String personIds, String startTime, String endTime,
			int start, int limit) throws Exception;

	/**
	 * 合理化建议详细信息查询数量
	 * 
	 * @param personIds
	 * @param orgIds
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public int getProposeDetailCountNum(String personIds, String orgIds, String startTime, String endTime)
			throws Exception;

	/**
	 * 合理化建议详细信息分页查询
	 * 
	 * @param personIds
	 * @param orgIds
	 * @param startTime
	 * @param endTime
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<JecnRtnlProposeCountDetail> getProposeDetailCountList(String personIds, String orgIds,
			String startTime, String endTime, int start, int limit) throws Exception;

	/**
	 * 合理化建议部门查询
	 * 
	 * @param orgIds
	 * @param startTime
	 * @param endTime
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<JecnRtnlProposeCountDetail> getProposeOrgCount(String orgIds, String startTime, String endTime,
			int start, int limit) throws Exception;

	public void handleProposeSendEmail(String proposeId, int flag) throws Exception;

	public void submitProposeSendEmail(String proposeId) throws Exception;

}
