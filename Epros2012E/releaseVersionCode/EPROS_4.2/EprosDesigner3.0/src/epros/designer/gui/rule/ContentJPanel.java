package epros.designer.gui.rule;

import epros.designer.gui.system.fileDescription.JecnFileDescriptionArea;
import epros.draw.gui.swing.JecnPanel;

/***
 * 制度内容显示Panel
 * 
 * @author 2012-06-15
 * 
 */
public class ContentJPanel extends JecnFileDescriptionArea {

	public ContentJPanel(int index, String paragrapHeadingName, JecnPanel mainPanel, String ruleContentStr,
			boolean isRequest) {
		super(index, paragrapHeadingName, mainPanel, isRequest, false, ruleContentStr);
	}

	public void setAreaEditable(boolean isEidt) {
		this.getJecnArea().setEditable(isEidt);
	}

}
