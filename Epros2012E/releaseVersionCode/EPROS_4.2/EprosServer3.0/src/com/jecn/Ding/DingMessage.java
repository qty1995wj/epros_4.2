package com.jecn.Ding;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 
 * @author Angus
 * @date 2019年1月26日
 * 钉钉发送消息的类，需要重新封装  代替邮件功能
 */
public class DingMessage {
	private static Logger LOGGER = Logger.getLogger(DingMessage.class);
	String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
	
	/**
	 * @author Angus
	 * @param messageBean
	 * @return
	 * @throws UnsupportedEncodingException
	 * 
	 * Ding MSG:  https://open-doc.dingtalk.com/microapp/serverapi2/pgoxpy
	 * 
	 * 
	 */
	public JSONObject sendMail(EmailMessageBean messageBean) throws UnsupportedEncodingException {

		String phoneStr = messageBean.getUser().getPhoneStr();
		
//		mailToOA.setContent("<HTML>" + messageBean.getContent() + "</HTML>");
//		mailToOA.setSubject(messageBean.getSubject());
//		mailToOA.setTo_employee_id(messageBean.getUser().getLoginName());

		Map<String, String> jsonMap = new LinkedHashMap<String, String>();

		String content = messageBean.getContent();
		String oldUrl = "";
		if(content.indexOf("<a")>-1){
			oldUrl = content.substring(content.indexOf("<a"));
		}
				
		content = content.substring(0,content.indexOf("此邮件"));
		content = content.replace("<br>", "  \n");
		Pattern pattern = Pattern.compile("<a[^<]*?>");
		Matcher matcher = pattern.matcher(oldUrl);
		LOGGER.info("oldUrl="+oldUrl);
		String alabel = "";
		if (matcher.find()) {
			oldUrl = matcher.group(0);
			if (StringUtils.isNotBlank(oldUrl)) {
				alabel = oldUrl.substring(11, oldUrl.length() - 2);
			}
		}
		if(!"".equals(oldUrl) && "".equals(alabel)){
			LOGGER.error("邮件链接处理错误"+messageBean.getEmailId()+messageBean.getSubject());
			return null;
		}
		String markUrl = alabel;   //替换任务url
		LOGGER.info("alabel="+alabel);
		int taskIdPosition =alabel.indexOf("mailTaskId=");
		String taskId = "";
		if(taskIdPosition > -1){
			taskId = alabel.substring(taskIdPosition+11);
			taskId = taskId.substring(0,taskId.indexOf('&'));
			if(alabel.indexOf("isView=true")>-1){//预览链接
				markUrl = basicEprosURL + "ddTask.html?t="+taskId+"&p="+messageBean.getUser().getPeopleId()+"&v=true";
			}else{
				markUrl = basicEprosURL + "ddTask.html?t="+taskId+"&p="+messageBean.getUser().getPeopleId()+"&v=false";
			}
		}
		if(!"".equals(markUrl)){
			content = content +"[进入]("+markUrl+")";
		}
		
		JSONObject markdown = new JSONObject();
		markdown.put("title",messageBean.getSubject());
		markdown.put("text",content);
		JSONObject msg = new JSONObject();
		msg.put("msgtype", "markdown");
		msg.put("markdown", markdown);


//		LOGGER.info("builder = " + sb.toString());
		JSONObject msgJson = new JSONObject();
		msgJson.put("agent_id", DingAfterItem.getValue("AGENT_ID"));
		msgJson.put("userid_list", phoneStr);
		msgJson.put("msg", msg);
		
//		JSONObject jsonObject = JSONObject.fromObject(mailToOA);
//		LOGGER.info("JSON = " + jsonObject.toString());
//		Map<String, String> headers = new HashMap<String, String>();
//		headers.put("Content-type", "application/json;charset=utf-8");
//		LOGGER.info("URL=" + MulinsenAfterItem.APP_URL);
		LOGGER.info("钉钉发送通知="+msgJson.toJSONString());
		try {
			return Dingtools.sendMessage(msgJson.toJSONString());
		} catch (Exception e) {
			LOGGER.error("sendMail error!", e);
			e.printStackTrace();
		}
		return null;
	}
}
