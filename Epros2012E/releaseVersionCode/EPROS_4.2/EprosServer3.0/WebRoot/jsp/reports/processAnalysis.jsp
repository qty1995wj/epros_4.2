<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript"
			src="${basePath}js/reports/processAnalysis.js">
</script>
		<script type="text/javascript">
document.body.onresize = function() {
	setProcessAnalysisWH('sysGrid_id');
}
</script>
	</head>

	<body>
		<div id="div" class="div" style="height: 99%;">
			<form action="" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="processAnalysisSearchText" style="overflow: hidden;"
						class="minWidth">
						<table width="100%">
							<tr>
								<td width="10%" height="25px" align="right">
									<!-- 时间类型: -->
									<s:text name="timeTypeC" />
								</td>
								<td width="35%" align="left">
									<select style="width: 80%" id="timeType_id">
										<s:iterator>
											<!-- 所有类型 -->
											<option value="-1">
												<s:text name="allTypes" />
											</option>
											<!-- 年 -->
											<option value="5">
												<s:text name="years" />
											</option>
											<!-- 季 -->
											<option value="4">
												<s:text name="season" />
											</option>
											<!-- 月 -->
											<option value="3">
												<s:text name="month" />
											</option>
											<!-- 周 -->
											<option value="2">
												<s:text name="weeks" />
											</option>
											<!-- 日 -->
											<option value="1">
												<s:text name="day" />
											</option>
										</s:iterator>
									</select>
								</td>

								<td width="10%" align="right">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="processName_analy_id" maxLength="32" readonly="readonly"/>
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectProcessOrMaWindow("processName_analy_id","processId_analy_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
								</td>

								<td style="vertical-align: middle; width: 10%;" align="center">
									<a onClick="toggle(processAnalysisSearchText)"
										style="cursor: pointer;" class="packUp"> <img id="packUp"
											src="${basePath}images/iParticipateInProcess/on.gif"
											align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
												name="packUp" />
									</span> </a>
								</td>

							</tr>

							<tr>
								<td height="25px" align="right">
									<!-- 岗位: -->
									<s:text name="posC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="participateInPostName_analy_id"
										onkeyup="search(participateInPostName_analy_id,participateInPostId_analy_id,search_analy_participateInPost,'pos');"
										onblur="divNode(search_analy_participateInPost)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("position","participateInPostName_analy_id","participateInPostId_analy_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_analy_participateInPost"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>


								<td align="right">
									<!-- 流程类别： -->
									<s:text name="processTypeC"></s:text>
								</td>
								<td align="left">
									<select style="width: 80%" id="processType_Id">
										<option value="-1">
											<!-- 全部 -->
											<s:text name="all" />
										</option>
										<s:iterator value="#request.processTypeList">
											<option value="<s:property value="typeId" />">
												<s:property value="typeName" />
											</option>
										</s:iterator>
									</select>
								</td>

								<td></td>
							</tr>
						</table>
					</div>

					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;" onclick="processAnalysisPieCharts()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;" onclick="processAnalysisReset()" />
					</div>
				</div>
				<!-- 流程ID -->
				<input type="hidden" id="processId_analy_id" value="-1" />
				<!-- 岗位 -->
				<input type="hidden" id="participateInPostId_analy_id" value="-1" />
			</form>

			<div id="sysGrid_id" class="divBorder"
				style="overflow-y: scroll; overflow-x: hidden;">

			</div>
			<div align="center">
				<a onclick="processAnalysisDownloadExcel()"
					style="cursor: pointer; color: #005EA7;" class="table"><s:text
						name="downLoadExcel" /> </a>
			</div>
		</div>
	</body>
</html>