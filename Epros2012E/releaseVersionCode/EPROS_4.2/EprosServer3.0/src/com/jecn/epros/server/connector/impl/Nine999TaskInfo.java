package com.jecn.epros.server.connector.impl;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.nine999.ISysNotifyTodoWebService;
import com.jecn.webservice.nine999.ISysNotifyTodoWebServiceServiceLocator;
import com.jecn.webservice.nine999.NotifyTodoAppResult;
import com.jecn.webservice.nine999.NotifyTodoRemoveContext;
import com.jecn.webservice.nine999.NotifyTodoSendContext;

/**
 * 九新待办
 * 
 * @author Angus
 * @date 2018-09-27
 * 
 */
public class Nine999TaskInfo extends JecnBaseTaskSend {
	private IPersonDao personDao;

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
			// 创建任务
			createTasks(myTaskBean);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 审批完成，删除待办
			deleteToDoTask(myTaskBean, curPeopleId, this.personDao);
		} else {// 一条from的数据,一条to 的数据
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());
			// 上一个阶段，处理任务，代办待办删除
			deleteToDoTask(myTaskBean, curPeopleId, this.personDao);
			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean);
			}
		}
	}

	/**
	 * 删除待办
	 * 
	 * @param myTaskBean
	 * @param handlerPeopleId
	 * @throws Exception
	 */
	private void deleteToDoTask(MyTaskBean myTaskBean, Long handlerPeopleId, IPersonDao personDap) throws Exception {
		JecnUser user = personDao.get(handlerPeopleId);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String time = format1.format(System.currentTimeMillis());
		NotifyTodoRemoveContext sendContext = new NotifyTodoRemoveContext();
		sendContext.setAppName("BPM");
		sendContext.setOptType(1);
		sendContext.setTargets("{\"LoginName\":\"" + user.getLoginName() + "\"}");
		sendContext.setModelId(myTaskBean.getTaskId() + "_" + user.getPeopleId());
		sendContext.setModelName("BPM");

		log.error("删除任务待办:"+time);
		// Nine999TaskWebServiceClient.sendTodo(map);
		try{
			ISysNotifyTodoWebServiceServiceLocator webServiceServiceLocator = new ISysNotifyTodoWebServiceServiceLocator();
			ISysNotifyTodoWebService webService = webServiceServiceLocator.getISysNotifyTodoWebServicePort();
			NotifyTodoAppResult appResult = webService.deleteTodo(sendContext);
			time = format1.format(System.currentTimeMillis());
			log.info("删除待办结果:"+time+"_____"+appResult.getReturnState() + "-----");
		}catch(Exception e){
			e.printStackTrace();
			log.info("删除待办错误");
		}
		
		
	}

	private void createTasks(MyTaskBean myTaskBean) throws Exception {
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String time = format1.format(System.currentTimeMillis());
		for (Long handlerPeopleId : myTaskBean.getHandlerPeopleIds()) {
			Map<String, String> map = new HashMap<String, String>();
			JecnUser user = personDao.get(handlerPeopleId);
			NotifyTodoSendContext sendContext = new NotifyTodoSendContext();
			sendContext.setAppName("BPM");
			sendContext.setCreateTime(time);
			sendContext.setType(1);
			sendContext.setLink(getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
			sendContext.setSubject(myTaskBean.getTaskName());
			sendContext.setTargets("{\"LoginName\":\"" + user.getLoginName() + "\"}");
			sendContext.setModelId(myTaskBean.getTaskId() + "_" + user.getPeopleId());
			sendContext.setModelName("BPM");

			// Nine999TaskWebServiceClient.sendTodo(map);
			log.error("发送任务待办:"+time);
			try{
				ISysNotifyTodoWebServiceServiceLocator webServiceServiceLocator = new ISysNotifyTodoWebServiceServiceLocator();
				ISysNotifyTodoWebService webService = webServiceServiceLocator.getISysNotifyTodoWebServicePort();
				NotifyTodoAppResult notifyTodoAppResult = webService.sendTodo(sendContext);

				time = format1.format(System.currentTimeMillis());
				log.info("任务待办发送结果:"+time+"_______"+notifyTodoAppResult.getReturnState() + "-----"+sendContext.getModelId() + "---"+myTaskBean.getTaskName());
			}catch(Exception e){
				e.printStackTrace();
				log.error("任务待办发送失败");
			}
		}
	}

	protected String getMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("getCetc10ToDOTasks获取系统配置URL为空!");
		}
		return basicEprosURL + "loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
	}

	protected String getMibileMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("getCetc10ToDOTasks获取系统配置URL为空!");
		}
		return basicEprosURL + "/loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
	}

	private String getInstanceId(MyTaskBean myTaskBean, Long curPeopleId) {
		return myTaskBean.getTaskId() + "_" + myTaskBean.getRid() + "_" + curPeopleId;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		JecnUser createUser = personDao.get(beanNew.getCreatePersonId());
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setCreateTime(JecnCommon.getStringbyDateHMS(beanNew.getCreateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(beanNew.getTaskName());
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setRid(beanNew.getRid().toString());
		myTaskBean.setCreatePeopleLoginName(createUser.getLoginName());
		myTaskBean.setOperationType(beanNew.getTaskElseState());
		myTaskBean.setStageName(beanNew.getStateMark());
		myTaskBean.setCreatePeopleId(beanNew.getCreatePersonId());
		return myTaskBean;
	}

	private String getTaskName(String name) {
		return name.indexOf(".") != -1 ? name.substring(0, name.lastIndexOf(".")) : name;
	}

	/**
	 * 取消任务代办
	 */
	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew beanNew, JecnUser jecnUser, JecnUser createUser) {
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());
		try {
			deleteToDoTask(myTaskBean, jecnUser.getPeopleId(), this.personDao);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());

		// 批量删除待办
		deleteTasks(myTaskBean, cancelPeoples);

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean);
	}

	private void deleteTasks(MyTaskBean myTaskBean, List<Long> handlerPeopleIds) throws Exception {
		for (Long handlerPeopleId : handlerPeopleIds) {
			deleteToDoTask(myTaskBean, handlerPeopleId, this.personDao);
		}
	}

}
