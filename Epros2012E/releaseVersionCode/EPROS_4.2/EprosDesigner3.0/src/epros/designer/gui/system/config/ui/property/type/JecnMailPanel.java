package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;

/**
 * 
 * 邮箱面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnMailPanel extends JecnAbstractPropertyBasePanel {
	/** 主滚动面板 */
	private JScrollPane mainScrollPane = null;
	/** 主面板 */
	private JecnPanel mainPanel = null;
	/** 邮件配置面板 */
	private JecnMailSetPanel emailSetingPanel = null;
	/** 邮箱配置面板 */
	private JecnMailBoxSetPanel mailBoxSetPanel = null;

	public JecnMailPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 主滚动面板主滚动面板
		mainScrollPane = new JScrollPane();
		// 主面板
		mainPanel = new JecnPanel();
		// 邮箱配置面板
		mailBoxSetPanel = new JecnMailBoxSetPanel(this);
		// 邮件配置面板
		emailSetingPanel = new JecnMailSetPanel(this);

		this.setOpaque(false);
		this.setBorder(null);

		mainScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mainScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		mainPanel.setOpaque(false);
		mainPanel.setBorder(null);
		mainPanel.setLayout(new BorderLayout());

		this.add(mainScrollPane);
		mainScrollPane.setViewportView(mainPanel);
		// 邮箱配置面板
		mainPanel.add(mailBoxSetPanel, BorderLayout.NORTH);
		// 邮件配置面板
		mainPanel.add(emailSetingPanel, BorderLayout.CENTER);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		// 邮箱配置面板
		mailBoxSetPanel.initData(configTypeDesgBean);
		// 邮件配置面板
		emailSetingPanel.initData(configTypeDesgBean);
	}

	/**
	 * 
	 * 校验
	 * 
	 * @return boolean true：成功；false：失败
	 */
	public boolean check() {
		// 存在错误标识：true，无错：false
		boolean exsitsError = false;

		exsitsError = mailBoxSetPanel.check();

		boolean ret = emailSetingPanel.check();
		if (exsitsError || ret) {
			exsitsError = true;
		}

		JecnOKJButton btn = dialog.getOkCancelJButtonPanel().getOkJButton();
		if (exsitsError) {// 验证失败
			if (btn.isEnabled()) {
				btn.setEnabled(false);
			}
			return false;
		}

		if (!btn.isEnabled()) {// 验证成功
			btn.setEnabled(true);
		}
		return true;
	}

	/**
	 * 
	 * 设置错误信息
	 * 
	 * @param addrInfoTextArea
	 *            JecnUserInfoTextArea
	 * @param info
	 * @return boolean true:有错；false：无错
	 */
	@Override
	public boolean setErrorInfo(JecnUserInfoTextArea addrInfoTextArea,
			String info) {
		if (!DrawCommon.isNullOrEmtryTrim(info)) {// 有错
			if (!addrInfoTextArea.isVisible()) {
				addrInfoTextArea.setVisible(true);
			}
			addrInfoTextArea.setText(info);
			return true;
		} else {// 无错
			if (addrInfoTextArea.isVisible()) {
				addrInfoTextArea.setVisible(false);
			}
			addrInfoTextArea.setText("");
			return false;
		}
	}
	
	/**
	 * 验证邮箱的IP地址格式
	 * */
	public  String cheackMailIp(String emailIp){
		/** IP验证码 */
	   // String test = "([1-9]|[1-9]\\d|1\\d{2}|2[0-1]\\d|22[0-3])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
		String test="^([a-z0-9A-Z]+[-_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
	    //判断IP地址格式是否正确
         if(!emailIp.matches(test)){
            return JecnProperties.getValue("emailCheck");
         };
	    return "";
	}
	
}
