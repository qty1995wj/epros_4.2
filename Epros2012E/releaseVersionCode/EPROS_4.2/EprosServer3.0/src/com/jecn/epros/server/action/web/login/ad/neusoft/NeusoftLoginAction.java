package com.jecn.epros.server.action.web.login.ad.neusoft;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class NeusoftLoginAction extends JecnAbstractADLoginAction {
	private static Logger log = Logger.getLogger(NeusoftLoginAction.class);

	public NeusoftLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 东软医疗暂时去掉单点
	 */
	@Override
	public String login() {
		return loginAction.loginGen();
//		// 登录名称
//		String loginName = this.loginAction.getLoginName();
//		// 登录密码
//		String password = this.loginAction.getPassword();
//
//		if (StringUtils.isBlank(loginName) && StringUtils.isBlank(password)) {
//			return LoginAction.INPUT;
//		}
//
//		try {
//			if (NeusoftAfterItem.loginValidateByEmail(loginName, password)) {
//				log.info("邮箱验证通过 ！email  = " + loginName);
//				return loginByLoginName(loginName);
//			}
//		} catch (Exception e) {
//			log.info("邮箱验证通过 ！email  = " + loginName);
//			loginAction.setPassword("password");
//			return loginAction.loginGen();
//		}
//		return LoginAction.INPUT;
	}
}
