package com.jecn.epros.server.webBean.process;

import java.io.Serializable;

public class ProcessWebBean implements Serializable {
	/** 流程ID */
	private long flowId;
	/** 流程父节点ID */
	private Long parentFlowId;
	/** 流程名称 */
	private String flowName;
	/** 流程编号 */
	private String flowIdInput;// 流程编号
	/** 责任部门Id */
	private Long orgId;
	/** 责任部门 */
	private String orgName;
	/** 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人) */
	private int typeResPeople;// 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
	/** 流程责任人Id */
	private Long resPeopleId;// 流程责任人Id
	/** 流程责任人 */
	private String resPeopleName;// 流程责任人
	/** 发布时间 */
	private String pubDate;
	/** 未更新时间 单位月 */
	private int noUpdateDate;
	/** 类型 1是上游流程,2是下游流程,3是过程流程,4是子流程 */
	private int flowType;
	/** 密级 0是公开，1是密码 */
	private int isPublic;
	/** 级别 */
	private int level;
	/** 版本号 */
	private String versionId;
	/** 流程监护人ID */
	private Long guardianId;
	/** 流程监护人名称 */
	private String guardianName;
	/** 流程拟稿人 */
	private String draftPerson;
	/** 流程有效期 */
	private int expiry;
	/**下次审视需完成的时间*/
	private String nextScanDate;
	public String getNextScanDate() {
		return nextScanDate;
	}

	public void setNextScanDate(String nextScanDate) {
		this.nextScanDate = nextScanDate;
	}

	/** 是否及时优化 0空 ,1是 ,2否 */
	private int optimization;

	/** 处于什么阶段 0是待建，1是审批，2是发布 */
	private int typeByData;
	/** 0是流程地图，1是流程 */
	private int isflow;
	/** 0未更新，1已更新状态 */
	private int updateType;

	public Long getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getDraftPerson() {
		return draftPerson;
	}

	public void setDraftPerson(String draftPerson) {
		this.draftPerson = draftPerson;
	}

	public int getExpiry() {
		return expiry;
	}

	public void setExpiry(int expiry) {
		this.expiry = expiry;
	}

	public int getOptimization() {
		return optimization;
	}

	public void setOptimization(int optimization) {
		this.optimization = optimization;
	}

	public int getIsflow() {
		return isflow;
	}

	public void setIsflow(int isflow) {
		this.isflow = isflow;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getVersionId() {
		if (versionId == null) {
			return "";
		}
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public int getFlowType() {
		return flowType;
	}

	public void setFlowType(int flowType) {
		this.flowType = flowType;
	}

	public long getFlowId() {
		return flowId;
	}

	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowIdInput() {
		if (flowIdInput == null) {
			return "";
		}
		return flowIdInput;
	}

	public void setFlowIdInput(String flowIdInput) {
		this.flowIdInput = flowIdInput;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		if (orgName == null) {
			return "";
		}
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public int getTypeResPeople() {
		return typeResPeople;
	}

	public void setTypeResPeople(int typeResPeople) {
		this.typeResPeople = typeResPeople;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}

	public String getResPeopleName() {
		if (resPeopleName == null) {
			return "";
		}
		return resPeopleName;
	}

	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}

	public String getPubDate() {
		if (pubDate == null) {
			return "";
		}
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public int getNoUpdateDate() {
		return noUpdateDate;
	}

	public void setNoUpdateDate(int noUpdateDate) {
		this.noUpdateDate = noUpdateDate;
	}

	public int getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}

	public int getTypeByData() {
		return typeByData;
	}

	public void setTypeByData(int typeByData) {
		this.typeByData = typeByData;
	}

	public int getUpdateType() {
		return updateType;
	}

	public void setUpdateType(int updateType) {
		this.updateType = updateType;
	}

	public Long getParentFlowId() {
		return parentFlowId;
	}

	public void setParentFlowId(Long parentFlowId) {
		this.parentFlowId = parentFlowId;
	}

}
