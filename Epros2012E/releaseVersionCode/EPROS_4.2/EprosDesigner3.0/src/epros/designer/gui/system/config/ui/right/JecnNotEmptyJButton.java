package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;

/**
 * 
 * 必填按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnNotEmptyJButton extends JecnAbstractBaseJButton {

	public JecnNotEmptyJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		this.setToolTipText(JecnProperties.getValue("flowSetOptNotEmtryInfo"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// 获取当前选中的属性面板
		JecnAbstractPropertyBasePanel selectedPropContPanel = dialog
				.getSelectedPropContPanel();
		if (selectedPropContPanel == null
				|| selectedPropContPanel.getTableScrollPane() == null) {
			return;
		}

		// 获取选中序号集合
		int[] selectedRows = selectedPropContPanel.getTableScrollPane()
				.getSelectedRows();
		// 获取选中项
		List<JecnConfigItemBean> selectedItemBeanList = selectedPropContPanel
				.getTableScrollPane().getSelectedItemBeanList();

		if (selectedItemBeanList == null || selectedItemBeanList.size() == 0) {
			return;
		}
		//'关键活动', '角色/职责', '流程图', '活动说明', '流程记录','操作规范', '相关流程', '相关标准',
		//'相关制度', '流程关键测评指标板','记录保存', '相关风险', 
		//宝隆专有：'流程边界', '流程责任属性', '流程文控信息'
		String[] array={"a21","a25","a7","a8","a9","a10","a11","a12","a13","a14","a15","a20","a22","a23","a24","fileScurityLevel","securityLevel","ruleScurityLevel","isShowProcessMapLastFlow","isShowProcessMapNextFlow"};
		
		// 查找到非必填选中数据，修改成必填
		for (JecnConfigItemBean itemBean : selectedItemBeanList) {
			boolean ret=false;
			for(String mm:array){
				if(mm.equals(itemBean.getMark())){
					ret=true;
					break;
				}
			}
			if(ret){
				continue;
			}
			
			// 非必填改成必填
			itemBean.setIsEmpty(JecnConfigContents.ITEM_NOT_EMPTY);
		}

		// 重新加载
		dialog.getPropertyPanel().reShowProperty();

		// 根据给定的序号添加选中行,如果选中行超过界限选中边界行
		selectedPropContPanel.getTableScrollPane().setSelectedRowToTable(
				selectedRows);

	}
}
