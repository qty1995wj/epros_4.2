package com.jecn.epros.server.service.popedom;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.popedom.RoleWebInfoBean;

public interface IJecnRoleService extends IBaseService<JecnRoleInfo, Long> {
	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：树加载，获得默认角色
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getDefaultRoles(boolean isAdmin) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:树加载，通过父类Id和项目ID，获得role子节点(角色节点和角色目录)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildRoles(Long pId, Long projectId, Long secondAdminPeopleId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description: 树加载，通过父类Id和项目ID，获得role子节点(角色目录)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildRoleDirs(Long pId, Long projectId, Long secondAdminPeopleId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:树加载，通过项目ID，获得Role所有的子节点(角色节点和角色目录)
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getAllRoles(Long projectId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:树加载，通过项目ID，获得Role所有的子节点(角色目录)
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getAllRoleDirs(Long projectId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:table加载，通过项目Id，名称，搜索角色（不包括默认角色）
	 * @param projectId
	 * @param name
	 * @return
	 */
	public List<JecnTreeBean> getRolesByName(Long projectId, String name) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description: 排序节点的父节点ID
	 * @param list
	 * @param pId
	 * @param projectId
	 * 
	 */
	public void updateSortRoles(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersionId)
			throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:移动角色
	 * @param listIds
	 * @param pId
	 *            移动到新节点的Id
	 */
	public void moveRoles(List<Long> listIds, Long pId, Long updatePersionId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description: 删除节点
	 * @param listIds
	 */
	public void deleteRoles(List<Long> listIds, Long projectId, Long updatePersionId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:添加角色
	 * @param jecnRoleInfo
	 * @param mapIds
	 * @param updatePersionId
	 * @return
	 */
	public Long addRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:更新角色
	 * @param jecnRoleInfo
	 * @param mapIds
	 * @param updatePersionId
	 * @return
	 */
	public Long updateRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:角色重命名
	 * @param newName
	 * @param id
	 * @param updatePersionId
	 */
	public void updateName(String newName, Long id, Long updatePersonId) throws Exception;

	/**
	 * @author zhangchen 2012-4-27
	 * @description:table加载，移动节点，搜索除了移动节点所在目录下其他关键字name的角色目录
	 * @param projectId
	 * @param name
	 * @param listIds
	 * @return
	 */
	public List<JecnTreeBean> getRoleDirsByNameMove(Long projectId, String name, List<Long> listIds) throws Exception;

	/**
	 * @author yxw 2012-5-8
	 * @description:主键Id获得角色对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnRoleInfo getJecnRoleInfoById(Long id) throws Exception;

	/**
	 * @author yxw 2012-5-9
	 * @description:获得节点下的子节点的个数
	 * @param id
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int getChildCount(Long id, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-10
	 * @description:角色更新时，判断是否与同一父ID下的角色是否重名
	 * @param newName
	 * @param id
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean getRepeatNameEidt(String newName, Long id, Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description: 根据ID和项目ID递归上级节点，包括和一级的兄弟节点
	 * @param id
	 * @paramprojectId
	 * @return
	 */
	public List<JecnTreeBean> getPnodes(Long id, Long projectId) throws Exception;

	/**
	 * 判断请求节点是否有权限
	 * 
	 * @param webLoginBean
	 * @return
	 * @throws Exception
	 */
	public boolean isViewAuth(WebLoginBean webLoginBean, String requestName, HttpServletRequest request)
			throws Exception;

	/**
	 * @author yxw 2013-3-7
	 * @description:查询角色总数
	 * @param r0leName
	 *            角色名称
	 * @param roleType
	 *            角色类型 admin design viewAdmin
	 * @return
	 * @throws Exception
	 */
	public int getTotalRole(String roleName, String roleType) throws Exception;

	/**
	 * @author yxw 2013-3-7
	 * @description:分页查询角色
	 * @param ruleName角色名称
	 * @param roleType角色类型
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页显示行数
	 * @return
	 * @throws Exception
	 */
	public List<RoleWebInfoBean> getRoleList(String roleName, String roleType, int start, int limit) throws Exception;

	List<JecnTreeBean> getSecondAdminTreeBean(Long preId) throws Exception;

	public Long updateRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception;

	public Long addRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception;

	List<Object[]> selectUserRoleByRoleId(Long roleId) throws Exception;

	void saveUserRoleLog() throws Exception;

	/**
	 * 启用二级管理员，角色管理
	 * 
	 * @param projectId
	 * @param name
	 * @param preId
	 * @return
	 * @throws Exception
	 */
	List<JecnTreeBean> getSecondAdminRolesByName(Long projectId, String name, Long peopleId) throws Exception;

	Long getSecondAdminRoleId(Long peopleId) throws Exception;

	/**
	 * 用户是否存在登录设计器的角色权限：true 存在
	 * 
	 * @param peopleId
	 * @return boolean
	 * @throws Exception
	 */
	boolean isRoleAuthDesigner(Long peopleId) throws Exception;
}
