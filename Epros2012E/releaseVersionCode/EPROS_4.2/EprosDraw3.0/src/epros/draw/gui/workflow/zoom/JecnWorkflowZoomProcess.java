package epros.draw.gui.workflow.zoom;

import java.awt.Dimension;

import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.line.JecnDiviLineDataCloneable;
import epros.draw.event.JecnTabbedEventProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;

/**
 * 
 * 放大缩小处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnWorkflowZoomProcess {

	/** 画图面板 */
	private JecnDrawDesktopPane workflow = null;

	public JecnWorkflowZoomProcess(JecnDrawDesktopPane workflow) {
		if (workflow == null) {
			// TODO
		}
		this.workflow = workflow;
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 放大、缩小功能按钮执行放大缩小
	 * 
	 * @param multiple
	 *            放大缩小递增或递减比例值
	 */
	public void zoomBtnActionProcess(double multiple) {
		// 画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 删除虚框
		JecnTabbedEventProcess.removeTempFigure();

		// 键盘事件未执行完，切换选中元素，处理键盘释放事件
		workflow.getKeyHandler().keyReleased();

		// 上次操作的放大缩小倍数
		double oldWorkflowScale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();

		// 获取执行放大所后小所需要的倍数
		double tmpMultiple = oldWorkflowScale + multiple;
		double newWorkflowScale = JecnWorkflowUtil.convertWorkflowSpace(tmpMultiple);

		// 执行放大缩小
		zoomByMultiple(oldWorkflowScale, newWorkflowScale);

		// 如果是流程图且显示角色移动时: 删除之前角色移动添加新的角色移动
		workflow.removeAddRoleMove();

		workflow.requestFocusInWindow();

	}

	/**
	 * 
	 * 对外接口
	 * 
	 * newWorkflowScale具体的放大缩小倍数
	 * 
	 * @param newWorkflowScale
	 *            double 放大缩小新的放大缩小倍数
	 */
	public void zoomByNewWorkflowScale(double newWorkflowScale) {
		// 画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 键盘事件未执行完，切换选中元素，处理键盘释放事件
		workflow.getKeyHandler().keyReleased();
		// 上次操作的放大缩小倍数
		double oldWorkflowScale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();

		// 执行放大缩小
		zoomByMultiple(oldWorkflowScale, newWorkflowScale);

		// 如果是流程图且显示角色移动时: 删除之前角色移动添加新的角色移动
		workflow.removeAddRoleMove();
		// 面板获取光标
		workflow.requestFocusInWindow();
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 仅供放大缩小下拉框触发事件使用
	 * 
	 * 通过放大缩小下拉框选择项值来放大缩小画图面板
	 * 
	 * @param itemValue
	 *            String 当前放大缩小选择下拉框选中的放大倍数
	 */
	public void zoomByNewWorkflowScale(String itemValue) {
		String item = itemValue.replaceAll("%", "");
		zoomByNewWorkflowScale(Double.valueOf(item).doubleValue() / 100);
	}

	/**
	 * 
	 * 执行放大缩小
	 * 
	 * @param oldWorkflowScale
	 *            double 放大缩小前的放大缩小倍数
	 * @param newWorkflowScale
	 *            double 执行放大缩小后的放大缩小倍数
	 */
	private void zoomByMultiple(double oldWorkflowScale, double newWorkflowScale) {
		if (oldWorkflowScale != newWorkflowScale) {
			JecnDrawMainPanel.getMainPanel().setCurrWorkflowScale(newWorkflowScale);
			// 执行放大
			this.zoom();
		}

		// 同步工具栏放大缩小倍数
		JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().setZoomComboBoxVaule();

		workflow.getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 
	 * 所有放大缩小入口: 放大缩小画图面板
	 * 
	 * @param workflow
	 *            JecnDrawDesktopPane 画图面板
	 */
	private void zoom() {

		for (JecnBaseFlowElementPanel flowElementPanel : workflow.getPanelList()) {
			if (flowElementPanel instanceof JecnBaseFigurePanel) {// 图形
				JecnBaseFigurePanel figure = (JecnBaseFigurePanel) flowElementPanel;

				zoomFigure(figure);

			} else if (flowElementPanel instanceof JecnBaseVHLinePanel) {// 横竖分割线
				JecnBaseVHLinePanel vhLinePanel = (JecnBaseVHLinePanel) flowElementPanel;
				// 设置位置点和大小
				vhLinePanel.setVHLineBounds();
			} else if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 连接线
				JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) flowElementPanel;
				// 设置位置点和大小
				zoomManhattanLine(manhattanLine);
			} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {
				JecnBaseDividingLinePanel dividingLinePanel = (JecnBaseDividingLinePanel) flowElementPanel;
				// 设置位置点和大小
				zoomDiviLine(dividingLinePanel);
			}
		}

		this.workflow.revalidate();
		this.workflow.repaint();
	}

	/**
	 * 
	 * 放大缩小图形
	 * 
	 * @param figure
	 *            JecnBaseFigurePanel 图形
	 */
	public void zoomFigure(JecnBaseFigurePanel figure) {
		// 放大缩小后的图形大小和位置点
		JecnFigureDataCloneable currFigureDataCloneable = figure.getFlowElementData().getFigureDataCloneable()
				.getZoomFigureDataProcess(workflow.getWorkflowScale());
		// 设置图形大小和位置点
		figure.setBounds(currFigureDataCloneable.getX(), currFigureDataCloneable.getY(), currFigureDataCloneable
				.getWidth(), currFigureDataCloneable.getHeight());
	}

	/**
	 * 
	 * 放大缩小连接线
	 * 
	 * @param manhattanLine
	 *            ManhattanLine 连接线
	 */
	public void zoomManhattanLine(JecnBaseManhattanLinePanel manhattanLine) {
		if (manhattanLine == null) {
			return;
		}
		// 当前面板下执行画线（放大缩小、改变线属性算法） 小线段数据层不变
		manhattanLine.paintZoomLine();
	}

	/**
	 * 
	 * 放大缩小分割线
	 * 
	 * @param dividingLinePanel
	 *            JecnBaseDividingLinePanel 分割线
	 */
	public void zoomDiviLine(JecnBaseDividingLinePanel dividingLinePanel) {
		if (dividingLinePanel == null) {
			return;
		}
		// 获取线段对应数据类克隆放大缩小对象,如果开始点和结束点的坐标中x或者y至少有一个不相等返回null
		JecnDiviLineDataCloneable cloneable = dividingLinePanel.getFlowElementData().getZoomDiviLineCloneable();

		if (dividingLinePanel instanceof HDividingLine || dividingLinePanel instanceof VDividingLine) {

			// 设置横线，竖线位置及大小
			dividingLinePanel.setLocation(cloneable.getLocation());
			// 位置
			Dimension dimension = new Dimension();
			dimension.setSize(dividingLinePanel.getFlowElementData().getFigureSizeX()
					* this.workflow.getWorkflowScale(), dividingLinePanel.getFlowElementData().getFigureSizeY()
					* this.workflow.getWorkflowScale());
			dividingLinePanel.setSize(dimension);
		}

		if (dividingLinePanel.isShowLineResize()) {// 显示分割线显示点
			dividingLinePanel.showLineResizeHandles();
		}

		if (dividingLinePanel instanceof DividingLine) {// 斜线
			// 重新计算画图面板的大小
			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (workflow != null) {
				workflow.getCurrDrawPanelSize(dividingLinePanel);
			}
		}
	}
}
