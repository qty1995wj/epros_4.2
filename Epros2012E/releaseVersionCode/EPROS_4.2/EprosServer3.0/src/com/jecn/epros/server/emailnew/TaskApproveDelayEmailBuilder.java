package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.email.buss.ToolEmail;

public class TaskApproveDelayEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

		Object[] data = getData();
		JecnTaskBeanNew taskBean = (JecnTaskBeanNew) data[0];
		return createEmail(user, taskBean);
	}

	private EmailBasicInfo createEmail(JecnUser user, JecnTaskBeanNew taskBean) {
		String httpUrl = ToolEmail.getTaskHttpUrl(taskBean.getId(), user.getPeopleId());
		String subject = "催办：请审批《" + taskBean.getTaskName() + "》";
		/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
		int taskType = taskBean.getTaskType();
		String type = "";
		if (taskType == 0 || taskType == 4) {
			type = "流程";
		} else if (taskType == 1) {
			type = "文件";
		} else if (taskType == 2 || taskType == 3) {
			type = "制度";
		}
		String content = "请及时审批EPROS" + type + "：";

		EmailContentBuilder builder = new EmailContentBuilder();
		builder.setContent(content);
		builder.setResourceUrl(httpUrl);

		EmailBasicInfo basicInfo = new EmailBasicInfo();
		basicInfo.setContent(builder.buildContent());
		basicInfo.setSubject(subject);
		basicInfo.addRecipients(user);
		return basicInfo;
	}
}
