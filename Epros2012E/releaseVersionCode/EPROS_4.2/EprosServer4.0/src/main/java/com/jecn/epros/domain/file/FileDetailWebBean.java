package com.jecn.epros.domain.file;

import com.jecn.epros.domain.process.Path;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author weidp
 * @description 文件清单
 * @date： 日期：2014-4-14 时间：上午11:38:34
 */
public class FileDetailWebBean {

    List<Path> paths = new ArrayList<Path>();

    /**
     * 文件Id
     **/
    private Long fileId;
    /**
     * 父文件Id
     **/
    private Long perFileId;
    /**
     * 级别
     **/
    private Integer level;
    /**
     * path
     **/
    private String path;
    /**
     * 文件名称
     **/
    private String fileName;
    /**
     * 文件编号
     **/
    private String docNumber;
    /**
     * 责任部门
     **/
    private String responsibleDeptName;
    /**
     * 责任部门
     **/
    private String responsibleDeptId;

    /**
     * 责任人ID
     */
    private Long responsibleId;
    /**
     * 责任人名称
     */
    private String responsibleName;
    /**
     * 责任人类型 0是人员 1 是岗位
     */
    private Integer responsibleType = 0;
    /**
     * 责任部门包含父部门集合
     */
    private List<Map<String,Object>> dutyOrgList;
    /**
     * 创建人
     **/
    private String creator;
    /**
     * 创建时间
     **/
    private Date createTime;
    /**
     * 更新时间
     **/
    private Date updateTime;
    /**
     * 是否文件夹 0是目录,1是文件
     **/
    private Integer isDir;

    /**
     * 0未发布 ，1发布，2审批过程中
     **/
    private int pubState;

    public Long getResponsibleId() {
        return responsibleId;
    }

    public void setResponsibleId(Long responsibleId) {
        this.responsibleId = responsibleId;
    }

    public String getResponsibleName() {
        return responsibleName;
    }

    public void setResponsibleName(String responsibleName) {
        this.responsibleName = responsibleName;
    }

    public Integer getResponsibleType() {
        return responsibleType;
    }

    public void setResponsibleType(Integer responsibleType) {
        this.responsibleType = responsibleType;
    }

    public String getResponsibleDeptId() {
        return responsibleDeptId;
    }

    public void setResponsibleDeptId(String responsibleDeptId) {
        this.responsibleDeptId = responsibleDeptId;
    }

    public List<Map<String, Object>> getDutyOrgList() {
        return dutyOrgList;
    }

    public void setDutyOrgList(List<Map<String, Object>> dutyOrgList) {
        this.dutyOrgList = dutyOrgList;
    }

    /**
     * 文件使用情况
     **/
    private List<FileDetailRelatedWebBean> relatedList = new ArrayList<FileDetailRelatedWebBean>();

    public List<FileDetailRelatedWebBean> getRelatedList() {
        return relatedList;
    }

    public void setRelatedList(List<FileDetailRelatedWebBean> relatedList) {
        this.relatedList = relatedList;
    }

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getPerFileId() {
        return perFileId;
    }

    public void setPerFileId(Long perFileId) {
        this.perFileId = perFileId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public String getResponsibleDeptName() {
        return responsibleDeptName;
    }

    public void setResponsibleDeptName(String responsibleDeptName) {
        this.responsibleDeptName = responsibleDeptName;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDir() {
        return isDir;
    }

    public void setIsDir(Integer isDir) {
        this.isDir = isDir;
    }

    public int getPubState() {
        return pubState;
    }

    public void setPubState(int pubState) {
        this.pubState = pubState;
    }
}
