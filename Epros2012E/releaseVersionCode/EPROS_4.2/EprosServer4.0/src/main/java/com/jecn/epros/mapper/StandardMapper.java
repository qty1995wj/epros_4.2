package com.jecn.epros.mapper;

import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.relatedFile.RelatedFile;
import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.process.relatedFile.RelatedRule;
import com.jecn.epros.domain.standard.SearchStandardResultBean;
import com.jecn.epros.domain.standard.StandardBean;
import com.jecn.epros.domain.standard.StandardFileContent;
import com.jecn.epros.domain.standard.StandardInfoBean;
import com.jecn.epros.sqlprovider.StandardSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

public interface StandardMapper {
    /**
     * 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = StandardSqlProvider.class, method = "findChildStandards")
    @ResultMap("mapper.Standard.standardTreeBeans")
    public List<TreeNode> findChildStandards(Map<String, Object> map);

    @Select("select cr.CRITERION_CLASS_ID," +
            "       cr.PRE_CRITERION_CLASS_ID," +
            "       cr.CRITERION_CLASS_NAME," +
            "       cr.PROJECT_ID," +
            "      case when  cr.STAN_TYPE = 1  then jf.version_id else cr.RELATED_ID end RELATED_ID," +
            "       cr.SORT_ID," +
            "       cr.IS_PUBLIC," +
            "       cr.CREATE_PEOPLE_ID," +
            "       cr.CREATE_DATE," +
            "       cr.UPDATE_PEOPLE_ID," +
            "       cr.UPDATE_DATE," +
            "       cr.STAN_TYPE," +
            "       cr.ISPROFILE," +
            "       cr.NOTE," +
            "       cr.T_PATH," +
            "       cr.T_LEVEL," +
            "       cr.STAN_CONTENT," +
            "       cr.CONFIDENTIALITY_LEVEL," +
            "       cr.VIEW_SORT," +
            "  case when cr.stan_type = 1 and cr.related_id is not null  then jf.doc_id else '- ' end doc_id"+
            "  from JECN_CRITERION_CLASSES cr" +
            "  left join jecn_file jf on cr.related_id = jf.file_id where cr.criterion_Class_Id=#{standardId}")
    @ResultMap("mapper.Standard.standardBean")
    public StandardBean getStandardBean(Long standardId);

    /**
     * 相关流程
     *
     * @param map
     * @return
     */
    @SelectProvider(type = StandardSqlProvider.class, method = "findRelatedFlows")
    @ResultMap("mapper.Process.relatedFlow")
    public List<RelatedFlow> findRelatedFlows(Long standardId);

    /**
     * 相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = StandardSqlProvider.class, method = "findRelatedRules")
    @ResultMap("mapper.Process.relatedRule")
    public List<RelatedRule> findRelatedRules(Long standardId);

    @SelectProvider(type = StandardSqlProvider.class, method = "findRelatedFiles")
    @ResultMap("mapper.Process.relatedFile")
    List<RelatedFile> findRelatedFiles(long id);

    /**
     * 制度搜索 总数
     */
    @SelectProvider(type = StandardSqlProvider.class, method = "searchStandardTotal")
    @ResultType(Integer.class)
    public int searchStandardTotal(Map<String, Object> map);

    /**
     * 制度搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = StandardSqlProvider.class, method = "searchStandard")
    @ResultMap("mapper.Standard.searchStandardResultBean")
    public List<SearchStandardResultBean> searchStandard(Map<String, Object> map);

    @SelectProvider(type = StandardSqlProvider.class, method = "fetchInventory")
    @ResultMap("mapper.Standard.fetchInventory")
    public List<StandardInfoBean> fetchInventory(Map<String, Object> paramMap);

    @SelectProvider(type = StandardSqlProvider.class, method = "fetchRelatedFlow")
    public List<Map<String, Object>> fetchRelatedFlow(Map<String, Object> paramMap);

    @SelectProvider(type = StandardSqlProvider.class, method = "fetchRelatedActivity")
    public List<Map<String, Object>> fetchRelatedActivity(Map<String, Object> paramMap);

    @SelectProvider(type = StandardSqlProvider.class, method = "fetchRelatedRule")
    public List<Map<String, Object>> fetchRelatedRule(Map<String, Object> paramMap);

    @SelectProvider(type = StandardSqlProvider.class, method = "fetchInventoryParent")
    @ResultMap("mapper.Standard.fetchInventory")
    public List<StandardInfoBean> fetchInventoryParent(Map<String, Object> paramMap);

    @SelectProvider(type = StandardSqlProvider.class, method = "getPathNameByIdsSql")
    @ResultMap("mapper.Standard.listParentName")
    public List<BaseBean> getRelatedParentNameByIds(Map<String, Object> paramMap);


    @SelectProvider(type = StandardSqlProvider.class, method = "getFileContentsSql")
    public List<StandardFileContent> getFileContents(Long standardId);
}
