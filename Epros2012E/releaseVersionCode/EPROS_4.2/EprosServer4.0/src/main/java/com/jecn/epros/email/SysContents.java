package com.jecn.epros.email;

public class SysContents {
    /**
     * 服务器到期提醒！
     */
    public final static String SUB_JECT = "服务器到期提醒！";

    public final static String MAIL_CONTENT_1 = "您好！<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;Epros流程体系管理平台密钥";

    public final static String MAIL_CONTENT_2 = "天后到期，密钥到期后平台会自动停止，为了不影响贵公司正常工作，请及时联系杰成公司更新密钥。";

    public final static String MAIL_CONTENT_3 = "<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;杰成公司联系方式：<BR>";

    public final static String MAIL_CONTENT_4 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联系电话：400-619-4800<BR>";

    public final static String MAIL_CONTENT_5 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司网址：http://www.jecn.com.cn<BR><BR>";

    public final static String MAIL_CONTENT_6 = "&nbsp;&nbsp;&nbsp;&nbsp;给您带来的不便请您谅解！！！<BR><BR>";

    public final static String MAIL_CONTENT_7 = "Epros流程体系管理平台";


}
