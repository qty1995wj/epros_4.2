package com.jecn.epros.server.bean.tree.data;

import java.util.List;

public interface Node<T> {

	public boolean hasChilds();

	public List<Node<T>> getChilds();

	public Node<T> getParent();

	public T getData();
	
	public void setData(T data);

	public void setParent(Node<T> parent);
	
	public void add(Node<T> c);

	/**
	 * 数据库id
	 */
	public Long getDbId();

	public String getViewSort();

	public Long getSort();

	public int getLevel();

	public String getPath();

	public void setDbId(Long dbId);

	public void setSort(Long sort);

	public void setLevel(int level);

	public void setPath(String path);

	public void setViewSort(String viewSort);

}
