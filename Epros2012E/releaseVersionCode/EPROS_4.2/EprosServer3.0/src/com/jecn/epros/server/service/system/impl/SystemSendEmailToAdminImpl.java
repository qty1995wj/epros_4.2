package com.jecn.epros.server.service.system.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.system.ISystemSendEmailToAdmin;
import com.jecn.epros.server.service.system.sysUtil.SysContents;
import com.jecn.epros.server.util.JecnKey;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 邮件发送给系统管理员通知系统到期
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-25 时间：下午02:47:27
 */
public class SystemSendEmailToAdminImpl extends AbsBaseService<Long, Long> implements ISystemSendEmailToAdmin {

	/** Dao层对象 */
	private IJecnConfigItemDao configDao = null;

	/**
	 * 服务监听，到期发送邮件提醒
	 * 
	 * @throws Exception
	 */
	@Override
	public void systemServerSendEmail() throws Exception {
		String value = JecnKey.value;
		if (JecnKey.isTestValidate()) {// 试用
			// 获取到期时间
			String endDate = value.substring(0, 8);
			// yyyy-MM-dd
			endDate = endDate.substring(0, 4) + "-" + endDate.substring(4, 6) + "-" + endDate.substring(6, 8);
			// 到期结束服务
			endTimeExitServer(endDate);
			// 提前一个月提醒
			sendEmailBeforeOneMonth(endDate, 30);
			// 7天发送邮件提醒
			sendEmailBeforeOneMonth(endDate, 7);
		}
	}

	/**
	 * 到期结束服务
	 * 
	 * @param strDate
	 * @param days
	 */
	private void endTimeExitServer(String strDate) {
		// 到期时间
		Date endDate = JecnCommon.getDateByString(strDate);

		// 到期时间
		long endTime = endDate.getTime();
		// 当前
		long newTime = JecnCommon.getDateByString(JecnCommon.getStringbyDate(new Date())).getTime();

		// 执行任务前的延迟时间，单位是毫秒
		if (endTime < newTime) {// 结束时间小于当前时间，停止服务
			System.out.println("daoqile");
			System.exit(0);
		}
	}

	/**
	 * 提前一个月发送邮件
	 * 
	 * @param addDays
	 *            提前多少天发送邮件
	 * @throws Exception
	 */
	private void sendEmailBeforeOneMonth(String strEndDate, int addDays) throws Exception {
		// 到期时间
		Date endDate = JecnCommon.getDateByString(strEndDate);

		// 到期时间
		long endTime = endDate.getTime();

		// 新建一个日历对象
		Calendar calen = Calendar.getInstance();
		// 当前日期格式的对象 yyyy-MM-dd
		Date curDate = JecnCommon.getDateByString(JecnCommon.getStringbyDate(new Date()));
		calen.setTime(curDate);
		// 将日历的"天"增加
		calen.add(Calendar.DAY_OF_YEAR, addDays);

		// 当前时间
		long curTime = calen.getTime().getTime();

		if (curTime == endTime) {// 相等，发送邮件和消息
			List<JecnUser> users = findAdmins();
			// 发邮件
			BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.SERVER_STOP);
			emailBuilder.setData(users, addDays);
			List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
			JecnUtil.saveEmail(buildEmail);
			// TODO:email
			// 发消息
			sendMessage(addDays, users);
		}
	}

	/**
	 * 发送消息给管理员
	 * 
	 * @param addDays
	 * @param objects
	 * @throws Exception
	 */
	private void sendMessage(int addDays, List<JecnUser> users) throws Exception {
		if (users == null) {
			return;
		}
		for (JecnUser user : users) {
			if (user.getPeopleId() == null) {
				return;
			}
			createMessage(user.getPeopleId(), addDays);
		}
	}

	/**
	 * 
	 * 发布给PRF查阅权限人员发送消息
	 * 
	 * @param peopleIds
	 *            人员主键ID集合
	 * @param prfName
	 *            PRF文件名称
	 */
	private void createMessage(Long peopleId, int addDays) {
		Session s = this.configDao.getSession();
		// 已发布！
		if (peopleId != null) {
			JecnMessage jecnMessage = new JecnMessage();
			jecnMessage.setPeopleId(0L);
			// 消息类型：发布
			jecnMessage.setMessageType(0);
			jecnMessage.setMessageContent(getMailContent(addDays));
			// 服务器到期提醒！
			jecnMessage.setMessageTopic(SysContents.SUB_JECT);
			jecnMessage.setNoRead(Long.valueOf(0));
			jecnMessage.setInceptPeopleId(peopleId);
			jecnMessage.setCreateTime(new Date());
			s.save(jecnMessage);
			s.flush();
		}
	}

	/**
	 * 获取正文
	 * 
	 * @param addDays
	 * @return
	 */
	private String getMailContent(int addDays) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(getContent1()).append(addDays);
		buffer.append(SysContents.MAIL_CONTENT_2);
		buffer.append(SysContents.MAIL_CONTENT_3);
		buffer.append(SysContents.MAIL_CONTENT_4);
		buffer.append(SysContents.MAIL_CONTENT_5);
		buffer.append(SysContents.MAIL_CONTENT_6);
		buffer.append(JecnConfigTool.getEmailPlatName());
		return buffer.toString();

	}

	private Object getContent1() {
		String result = "您好！<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;" + JecnConfigTool.getEmailPlatName() + "密钥";
		return result;
	}

	/**
	 * 获取系统管理员邮件信息
	 * 
	 * @return 邮件信息
	 * @throws Exception
	 */
	private List<JecnUser> findAdmins() throws Exception {

		return configDao.getSession().createSQLQuery(JecnCommonSql.getAdminEmailSql()).addEntity(JecnUser.class).list();
	}

	public IJecnConfigItemDao getConfigDao() {
		return configDao;
	}

	public void setConfigDao(IJecnConfigItemDao configDao) {
		this.configDao = configDao;
	}
}
