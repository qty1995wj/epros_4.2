package epros.designer.tree;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.TreeSelectionModel;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.draw.util.JecnUIUtil;

public abstract class JecnTree extends JTree {
	
	private List<Long> listIds = new ArrayList<Long>();
	private TreeNodeType nodeType=null;
	protected Long startId=0L;
	public TreeNodeType getNodeType() {
		return nodeType;
	}

	public List<Long> getListIds() {
		return listIds;
	}

	public void setListIds(List<Long> listIds) {
		this.listIds = listIds;
	}

	public JecnTree(List<Long> listIds){
		this.listIds = listIds;
		this.setModel(getTreeModel());
		// 设置tree节点的图片显示
		this.setCellRenderer(new JecnTreeRenderer());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
		}
		
		/**添加鼠标事件*/
		this.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});
		
		//this.setCellRenderer(new JecnTreeRenderer());
		//设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}
	
	public JecnTree(){
		this.setModel(getTreeModel());
		// 设置tree节点的图片显示
		this.setCellRenderer(new JecnTreeRenderer());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
		}
		
		/**添加鼠标事件*/
		this.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});
		
		
		//设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}
	
	public JecnTree(Long startId){
		this.startId=startId;
		this.setModel(getTreeModel());
		// 设置tree节点的图片显示
		this.setCellRenderer(new JecnTreeRenderer());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
		}
		
		/**添加鼠标事件*/
		this.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});
		
		
		//设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}
	public JecnTree(List<Long> listIds,TreeNodeType nodeType){
		this.listIds = listIds;
		this.nodeType=nodeType;
		this.setModel(getTreeModel());
		// 设置tree节点的图片显示
		this.setCellRenderer(new JecnTreeRenderer());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
		}
		
		/**添加鼠标事件*/
		this.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});
		
		//this.setCellRenderer(new JecnTreeRenderer());
		//设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}	
	/**
	 * @author zhangchen May 2, 2012
	 * @description：获得TreeModel
	 * @return
	 */
	public abstract JecnTreeModel getTreeModel();

	/**
	 * 
	 * @author zhangchen May 2, 2012
	 * @description：设置Tree是否多选
	 * @return
	 */
	public abstract boolean isSelectMutil();
	
	/**
	 * @author yxw 2012-5-7
	 * @description:添加鼠标事件
	 * @return
	 */
	public abstract void jTreeMousePressed(MouseEvent evt);
}
