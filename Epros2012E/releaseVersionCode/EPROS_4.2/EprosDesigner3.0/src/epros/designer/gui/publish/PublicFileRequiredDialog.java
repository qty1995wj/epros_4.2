package epros.designer.gui.publish;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

/**
 * 发布任务必填项错误信息显示窗体
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Nov 14, 2012 时间：10:56:05 AM
 */
public class PublicFileRequiredDialog extends JecnDialog {
	/** 主面板 */
	protected JecnPanel mainPanel = new JecnPanel();

	/** 文件基本信息是否必填验证 接口获取错误信息集合 */
	private List<String> strList = null;
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	// /** 显示流程文件错误信息表格 */
	// private JTable jtable = new JTable();

	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = new JScrollPane();

	private JButton closeBut = new JButton(JecnProperties.getValue("closeBtn"));

	public PublicFileRequiredDialog(List<String> strList) {
		this.strList = strList;
		initComponents();
	}

	/**
	 * 初始化控件
	 * 
	 */
	private void initComponents() {
		this.setSize(480, 600);
		this.setTitle(JecnProperties.getValue("expInfo"));
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);
		mainPanel.setBorder(BorderFactory.createTitledBorder(""));
		// Dimension dimension = new Dimension(480, 600);
		// infoPanel.setPreferredSize(dimension);

		// textArea.setPreferredSize(dimension);
		// textArea.setSize(dimension);

		// infoPanel.add(textArea);
		// String str = getString();
		// textArea.setText(str);
		// mainPanel.add(infoPanel);
		closeBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeButPerformed();
			}

		});

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(
				0, 0, 0, 0), 0, 0);
		mainPanel.add(resultScrollPane, c);
		infoPanel.setLayout(new GridBagLayout());
		// infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		for (int i = 0; i < strList.size(); i++) {
			JLabel strField = new JLabel();
			strField.setText(i + 1 + "." + strList.get(i));

			c = new GridBagConstraints(0, i, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			infoPanel.add(strField, c);
			if (i == strList.size() - 1) {
				c = new GridBagConstraints(0, i + 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.BOTH, insets, 0, 0);
				infoPanel.add(new JLabel(), c);
			}
		}

		/*
		 * c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
		 * GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets, 0, 0);
		 * mainPanel.add(new JLabel(), c);
		 */
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(closeBut, c);
		resultScrollPane.setViewportView(infoPanel);
		this.getContentPane().add(mainPanel);

	}

	/***
	 * 关闭
	 */
	private void closeButPerformed() {
		this.dispose();
	}

	public String getString() {
		StringBuffer strBuffer = new StringBuffer();
		for (String strString : strList) {
			strBuffer.append(strString).append("\n");
		}
		return strBuffer.toString();
	}

}
