package epros.designer.gui.integration.risk;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlTarget;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.integration.table.RiskControlTargetTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * 新建风险点=====控制目标 2013-10-31
 * 
 */
public class AddRiskControlTargetPanel extends JecnPanel {
	private static Logger log = Logger
			.getLogger(AddRiskControlTargetPanel.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(450, 330);
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 按钮Panel */
	private JecnPanel buttonPanel = new JecnPanel();
	/** 新建控制目标面板 */
	private JecnPanel newControlTargetPanel = new JecnPanel();
	/** 新建控制目标内容显示面板 */
	private JecnPanel contentPanel = new JecnPanel();
	/** 新建控制目标====按钮面板 **/
	private JecnPanel contrTargetButPanel = new JecnPanel();

	/** 控制目标Table */
	protected RiskControlTargetTable controlTargetTable = null;

	/** 控制目标滚动面板 */
	private JScrollPane controlTargetScrollPane = null;
	/** 控制目标ids */
	protected String controlTargetIds = "";
	/** 提示Lab */
	private JLabel verfyLab = new JLabel("");
	/** 上移Button */
	private JButton moveUpBut = new JButton(JecnProperties.getValue("upBtn"));
	/** 下移Button */
	private JButton moveDownBut = new JButton(JecnProperties
			.getValue("downBtn"));
	/** 添加Button */
	private JButton addBut = new JButton(JecnProperties.getValue("addBtn"));
	/** 编辑Button */
	private JButton editBut = new JButton(JecnProperties.getValue("editBtn"));
	/** 删除Button */
	private JButton deleteBut = new JButton(JecnProperties
			.getValue("deleteBtn"));

	/** 控制目标Lab */
	private JLabel controlTargetLab = new JLabel(JecnProperties
			.getValue("controlTargetC"));

	/** 控制目标Area */
	protected JecnTextArea controlTargetArea = new JecnTextArea();// 400,180
	/** 控制目标滚动面板 */
	private JScrollPane addControlTargetScrollPane = new JScrollPane(
			controlTargetArea);
	private JLabel controlTargetRequired = new JLabel("*");
	/** 控制目标验证提示信息 */
	protected JLabel controlTargetVerfy = new JLabel();

	/** 确定 */
	private JButton saveBut = new JButton(JecnProperties.getValue("saveBut"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties
			.getValue("cancelBtn"));
	/** 判断是添加还是编辑控制目标 0：添加，1：编辑 **/
	protected int isAddOrUpdate = 0;

	/** 控制目标数据集合 */
	private List<JecnControlTarget> controlTargetList = new ArrayList<JecnControlTarget>();
	/** 设置大小 */
	Dimension dimension = null;

	/**
	 * 老的控制目标，为了判断是不是会出现更新
	 */
	private String targetDesOld = "";

	public AddRiskControlTargetPanel(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setSize(650, 500);

		controlTargetTable = new RiskControlTargetTable(controlTargetList);
		controlTargetScrollPane = new JScrollPane();
		controlTargetScrollPane.setViewportView(controlTargetTable);
		controlTargetScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		newControlTargetPanel.setBackground(JecnUIUtil
				.getDefaultBackgroundColor());
		contrTargetButPanel.setBackground(JecnUIUtil
				.getDefaultBackgroundColor());
		contentPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 控制目标
		controlTargetArea.setBorder(null);
		addControlTargetScrollPane.setBorder(BorderFactory
				.createLineBorder(Color.gray));
		addControlTargetScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		addControlTargetScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 换行
		controlTargetArea.setLineWrap(true);
		controlTargetVerfy.setForeground(Color.red);
		controlTargetRequired.setForeground(Color.red);

		newControlTargetPanel.setVisible(false);

		verfyLab.setForeground(Color.red);

		moveUpBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				moveUpButPerformed();
			}
		});
		moveDownBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				moveDownButPerformed();
			}
		});
		addBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}
		});
		editBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}
		});
		deleteBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}
		});
		saveBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				saveButPerformed();
			}
		});
		cancelBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		controlTargetTable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				controlTargetTablemouseReleased(evt);
			}
		});
		initLayout();

	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(mainPanel, c);
		// 信息面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());

		// 控制目标Table集合
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(controlTargetScrollPane, c);

		// 新建控制目标面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 5), 0, 0);
		mainPanel.add(newControlTargetPanel, c);
		newControlTargetPanel.setLayout(new GridBagLayout());
		// 新建控制目标内容显示
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 5), 0, 0);
		newControlTargetPanel.add(contentPanel, c);

		contentPanel.setLayout(new GridBagLayout());

		// 控制目标名称

		// c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
		// 0);
		// contentPanel.add(controlTargetLab, c);
		// 控制目标显示框
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		contentPanel.add(addControlTargetScrollPane, c);
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 0), 0, 0);
		contentPanel.add(controlTargetRequired, c);

		// 新建控制目标按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		newControlTargetPanel.add(contrTargetButPanel, c);
		contrTargetButPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		contrTargetButPanel.add(controlTargetVerfy);
		contrTargetButPanel.add(saveBut);
		contrTargetButPanel.add(cancelBut);

		// 按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		buttonPanel.add(moveUpBut);
		buttonPanel.add(moveDownBut);
		buttonPanel.add(addBut);
		buttonPanel.add(editBut);
		buttonPanel.add(deleteBut);
		buttonPanel.add(verfyLab);
	}

	/***
	 * 上移
	 */
	private void moveUpButPerformed() {
		controlTargetTable.moveUpRows();
		JecnTableModel model = (JecnTableModel) controlTargetTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(String.valueOf(i + 1), i, 1);
		}
	}

	/***
	 * 下移
	 */
	private void moveDownButPerformed() {
		controlTargetTable.moveDownRows();
		JecnTableModel model = (JecnTableModel) controlTargetTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(String.valueOf(i + 1), i, 1);
		}
	}

	/***
	 * 添加
	 */
	private void addButPerformed() {

		isAddOrUpdate = 0;
		newControlTargetPanel.setBorder(BorderFactory
				.createTitledBorder(JecnProperties.getValue("addBtn")));
		newControlTargetPanel.setVisible(true);
		verfyLab.setText("");
		controlTargetArea.setText("");
	}

	/***
	 * 编辑
	 */
	private void editButPerformed() {
		isAddOrUpdate = 1;
		verfyLab.setText("");
		newControlTargetPanel.setBorder(BorderFactory
				.createTitledBorder(JecnProperties.getValue("editBtn")));
		int[] selectRows = controlTargetTable.getSelectedRows();
		if (selectRows.length != 1) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		String targetDes = (String) controlTargetTable.getModel().getValueAt(
				controlTargetTable.getSelectedRow(), 2);
		if (targetDes != null) {
			this.controlTargetArea.setText(targetDes);
		}
		targetDesOld = targetDes;
		newControlTargetPanel.setVisible(true);
	}

	/**
	 * 删除
	 */
	private void deleteButPerformed() {
		verfyLab.setText("");
		// 提示请选中行
		int[] selectRows = controlTargetTable.getSelectedRows();
		if (selectRows.length <= 0) {
			verfyLab.setText(JecnProperties.getValue("task_selectedRowsInfo"));
			return;
		}
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		// 删除table选中的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) controlTargetTable.getModel())
					.removeRow(selectRows[i]);
		}
		JecnTableModel model = (JecnTableModel) this.controlTargetTable
				.getModel();
		int rows = model.getRowCount();

		// 重设序号
		for (int i = 0; i < rows; i++) {
			model.setValueAt(String.valueOf(i + 1), i, 1);
		}
		newControlTargetPanel.setVisible(false);
	}

	/***
	 * 保存新建控制目标
	 */
	private void saveButPerformed() {
		controlTargetVerfy.setText("");
		String controlTargetDesc = this.controlTargetArea.getText();
		// 控制目标长度验证
		if (DrawCommon.isNullOrEmtryTrim(controlTargetDesc)) {
			controlTargetVerfy.setText(JecnProperties.getValue("controlTarget")
					+ JecnProperties.getValue("isNotEmpty"));
			return;
		}
		if (controlTargetDesc != null && !"".equals(controlTargetDesc)) {
			if (DrawCommon.checkNoteLength(controlTargetDesc)) {
				controlTargetVerfy.setText(JecnProperties
						.getValue("controlTarget")
						+ JecnProperties.getValue("lengthNotOut"));
				return;
			}
		}
		if (isAddOrUpdate == 0) {
			Vector<String> v = new Vector<String>();
			v.add("");
			v.add(String
					.valueOf(controlTargetTable.getModel().getRowCount() + 1));
			v.add(controlTargetDesc);
			controlTargetTable.addRow(v);
			controlTargetArea.setText("");
			this.targetDesOld="";
		} else if (isAddOrUpdate == 1) {
			// 编辑控制目标点击保存操作
			if (controlTargetTable.getSelectedRow() == -1) {
				verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
				return;
			}
			controlTargetTable.getModel().setValueAt(
					this.controlTargetArea.getText(),
					controlTargetTable.getSelectedRow(), 2);
			// controlTargetArea.setText("");
			targetDesOld=controlTargetArea.getText();
		}
	}

	private void cancelButPerformed() {
		if (targetDesOld == null) {
			targetDesOld = "";
		}
		//更新变化
		if (!controlTargetArea.getText().equals(targetDesOld)) {
			// true已经显示 false没有显示
			int option = JecnOptionPane.showConfirmDialog(this,
					JecnDrawMainPanel.getMainPanel().getResourceManager()
							.getValue("optionInfo"), null,
					JecnOptionPane.YES_NO_CANCEL_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.YES_OPTION) {
				saveButPerformed();
			} else if (option == JecnOptionPane.NO_OPTION) {
				controlTargetVerfy.setText("");
				newControlTargetPanel.setVisible(false);
				targetDesOld="";
			}
		}else{
			controlTargetVerfy.setText("");
			newControlTargetPanel.setVisible(false);
			targetDesOld="";
		}
	}

	/**
	 * @description:单击Table行
	 */
	private void controlTargetTablemouseReleased(MouseEvent evt) {
		if (isAddOrUpdate == 1) {
			if (evt.getClickCount() == 1) {
				int row = controlTargetTable.getSelectedRow();
				if (row != -1) {
					// 显示选中行将数据显示到下方的controlTargetDescArea中
					String desc = this.controlTargetTable.getModel()
							.getValueAt(row, 2).toString();
					this.controlTargetArea.setText(desc);
				}
			}
		}
	}
}
