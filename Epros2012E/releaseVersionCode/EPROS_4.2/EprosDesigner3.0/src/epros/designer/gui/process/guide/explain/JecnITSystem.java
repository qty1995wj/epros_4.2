package epros.designer.gui.process.guide.explain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JLabel;

import epros.designer.gui.popedom.person.JecnPersonInfoPanel;
import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.swing.JecnPanel;

/**
 * 支撑IT系统
 * 
 * @author zxh
 * 
 */
public class JecnITSystem extends JecnFileDescriptionTable {

	private ProcessOperationDialog processOperationDialog;

	public JecnITSystem(int index, String name, boolean isRequest, JecnPanel contentPanel,
			ProcessOperationDialog processOperationDialog, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.processOperationDialog = processOperationDialog;

		initTable();
	}

	@Override
	protected void dbClickMethod() {

	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("activitiesNumbers"));
		title.add(JecnProperties.getValue("activityTitle"));
		title.add(JecnProperties.getValue("sysName"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		// 由于页面可以取到相关文件所以没有从数据库取值，加快显示速度
		List<JecnActivityShowBean> activitys = processOperationDialog.getFlowOperationBean().getListAllActivityShow();
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		for (JecnActivityShowBean activity : activitys) {
			JecnActiveData activeData = activity.getActiveFigure().getFlowElementData();
			List<JecnActiveOnLineTBean> activeOnLineTBeans = activeData.getListJecnActiveOnLineTBean();
			String sysNames = null;
			if (activeOnLineTBeans == null || activeOnLineTBeans.size() == 0) {
				continue;
			}
			for (JecnActiveOnLineTBean activeOnLineTBean : activeOnLineTBeans) {
				if (sysNames == null) {
					sysNames = activeOnLineTBean.getSysName();
				} else {
					sysNames += "/" + activeOnLineTBean.getSysName();
				}
			}
			content.add(createLineData(activeData.getActivityNum(), activeData.getFigureText(), sysNames));
		}

		return content;
	}

	private Vector<String> createLineData(String activeName, String activeNum, String fileName) {
		Vector<String> content = new Vector<String>();
		content.add(activeName);
		content.add(activeNum);
		// 名称
		content.add(fileName);
		return content;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] {};
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
