////////////////////////////////////////////////////////
// Copyright (c) 2012 JECN Corporation 
// JECN CONFIDENTIAL AND PROPRIETARY             
// All rights reserved by JECN Corporation.    
// This program must be used solely for the purpose for    
// which it was furnished by JECN Corporation. No part  
// of this program may be reproduced  or  disclosed  to 
// others,  in  any form,  without  the  prior  written    
// permission of JECN Corporation. Use  of  copyright    
// notice does not evidence publication of the program.    
////////////////////////////////////////////////////////

package epros.draw.util;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 国际化资源管理类
 * 
 * 界面上控件标题、布局大小、颜色、线条、说明等内容
 * 
 * @author 周兴裕
 * 
 */
public class JecnResourceUtil {
	/** 资源文件路径前缀 */
	private final String RESOURCE_PATH_PREFIX = "epros.draw.resources";
	/** 资源文件名称 */
	private String fileName = null;
	/** 资源包对象 */
	private PropertyResourceBundle resBundle = null;
	/** 语言环境对象 默认中文 */
	private static Locale locale = Locale.CHINESE;
	public static boolean CH = true;
	public static boolean EN = !CH;

	private static JecnResourceUtil resourceUtil = null;

	public JecnResourceUtil() {
		// 获取资源包对象
		getResourceBundle("JecnDrawMain");
	}

	public static JecnResourceUtil getJecnResourceUtil() {
		if (resourceUtil == null) {
			resourceUtil = new JecnResourceUtil();
		}
		return resourceUtil;
	}

	/**
	 * 
	 * 根据指定的键值，获取指定的value值
	 * 
	 * @param key
	 *            ToolBarElemType 待获取的键值
	 * @return String 给定键值对应的值
	 */
	public String getValue(ToolBarElemType key) {
		if (key == null) {
			return null;
		}
		return getValue(key.toString());
	}

	/**
	 * 
	 * 流程元素库，获取指定的value值
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return String 给定键值对应的值
	 */
	public String getValue(MapElemType mapElemType) {
		if (mapElemType == null) {
			return null;
		}
		return getValue(mapElemType.toString());
	}

	/**
	 * 
	 * 根据指定的键值，获取指定的value值
	 * 
	 * @param key
	 *            String 待获取的键值
	 * @return String 给定键值对应的值
	 */
	public String getValue(String key) {
		if (DrawCommon.isNullOrEmtryTrim(key)) {
			JecnLogConstants.LOG_RESOURCE_UTIL.error("JecnResourceUtil类setResourceBundle方法：参数为null或“”");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		if (resBundle == null) {
			JecnLogConstants.LOG_RESOURCE_UTIL
					.error("JecnResourceUtil类getValue方法：资源文件对象(PropertyResourceBundle)为null ：if (resBundle == null) { ...");
			return null;
		}

		try {
			return resBundle.getString(key);
		} catch (MissingResourceException mrex) {
			JecnLogConstants.LOG_RESOURCE_UTIL.error("JecnResourceUtil类getValue方法：没有找到给定key对应的值，key=：" + key);
		}

		return null;
	}

	/**
	 * 
	 * 通过资源文件对象和语言环境对象获取资源包对象.
	 * 
	 * 如果给定的语言环境对象不能获取资源包对象，
	 * 
	 * 那采用中文语言环境下获取资源包对象，如果此种情况获取失败那报错.
	 * 
	 * 
	 * @param clazz
	 *            Class 资源文件对象
	 */
	private void getResourceBundle(String propertiesName) {

		// 获取对象全名称
		// String name = propertiesName;

		fileName = RESOURCE_PATH_PREFIX + "." + propertiesName;

		try {
			// 使用指定的文件名称、语言环境获取资源对象
			resBundle = (PropertyResourceBundle) ResourceBundle.getBundle(fileName, locale);
		} catch (MissingResourceException e) {

			JecnLogConstants.LOG_RESOURCE_UTIL.error("JecnResourceUtil类setResourceBundle方法：通过本地获取语言环境找不到资源文件 "
					+ fileName, e);

			try {
				// 未找到指定基本名称的资源包,以中文语言环境获取
				resBundle = (PropertyResourceBundle) ResourceBundle.getBundle(fileName, Locale.CHINESE);

			} catch (java.util.MissingResourceException ev) {
				// 如果执行此分支，说明系统逻辑错误
				JecnLogConstants.LOG_RESOURCE_UTIL.error("JecnResourceUtil类setResourceBundle方法：找不到此资源文件 " + fileName,
						ev);
				throw ev;
			}
		}
	}

	public static Locale getLocale() {
		return locale;
	}

	public static void setLocale(Locale locale) {
		JecnResourceUtil.locale = locale;
		CH = JecnResourceUtil.locale == Locale.CHINESE;
		EN = !CH;
	}
}
