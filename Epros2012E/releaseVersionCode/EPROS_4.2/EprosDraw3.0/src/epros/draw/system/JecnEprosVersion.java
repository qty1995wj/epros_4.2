package epros.draw.system;

import java.util.Locale;

import epros.draw.constant.JecnToolbarConstant.TooBarTitleNumEnum;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 获取窗体标题、工具栏标题背景
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEprosVersion {
	/** 窗体标题背景图片名称前缀 */
	private final String preIconBefore = "toolbarTitleBack";
	/** 窗体标题背景图片名称后缀 */
	private final String preTitleBefore = "frameTitleName";

	private static JecnEprosVersion eprosVersion = null;
	/** 窗体标题背景图片 */
	private String toolbarTitleBackIcon = null;
	/** 窗体标题选中图片 */
	private String toolbarTitleBackSelectedIcon = null;
	/** 窗体标题 */
	private String frameTitleName = "";
	/** 关于图片 */
	private String helpAboutIcon = null;

	private JecnEprosVersion() {
		initComponents();
	}

	public static JecnEprosVersion getEprosVersion() {
		if (eprosVersion == null) {
			eprosVersion = new JecnEprosVersion();
		}
		return eprosVersion;
	}

	private void initComponents() {
		// 标题
		frameTitleName = JecnResourceUtil.getJecnResourceUtil().getValue(
				preTitleBefore + JecnSystemStaticData.getTooBarTitleNumEnum().toString());
		// 图片
		toolbarTitleBackIcon = preIconBefore + JecnSystemStaticData.getTooBarTitleNumEnum().toString();
		// 选中图片
		if (TooBarTitleNumEnum.six == JecnSystemStaticData.getTooBarTitleNumEnum()) {
			toolbarTitleBackSelectedIcon = "toolbarTitleSelectedsix";
		} else {
			toolbarTitleBackSelectedIcon = "toolbarTitleSelected";
		}

		// 关于图片
		switch (JecnSystemStaticData.getEasyProcessType()) {
		case easyProcessDrawer:// 画图工具（不带操作说明等）
			helpAboutIcon = "/epros/draw/images/epros/helpEasyprocessDrawer.jpg";
			break;
		case easyProcessDesigner:// 画图工具（带操作说明等）
			helpAboutIcon = "/epros/draw/images/epros/helpEasyprocessDesigner.jpg";
			break;
		case eprosDesigner:// 设计器(所有功能都在)
			if (JecnDesignerProcess.getDesignerProcess().isVersionType()) {
				helpAboutIcon = JecnResourceUtil.getLocale() == Locale.ENGLISH ? "/epros/draw/images/epros/helpEprosDesignerEN.jpg"
						: "/epros/draw/images/epros/helpEprosDesigner.jpg";
			} else {
				helpAboutIcon = "/epros/draw/images/epros/helpEPSDesigner.jpg";
			}

			break;
		default:
			helpAboutIcon = "/epros/draw/images/epros/helpEasyprocessDrawer.jpg";
			break;
		}
	}

	public String getFrameTitleName() {
		return frameTitleName;
	}

	public String getToolbarTitleBackIcon() {
		return toolbarTitleBackIcon;
	}

	public String getToolbarTitleBackSelectedIcon() {
		return toolbarTitleBackSelectedIcon;
	}

	public String getHelpAboutIcon() {
		return helpAboutIcon;
	}
}
