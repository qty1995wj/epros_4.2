//流程有效期维护
/**
 * 下载
 */
function processValidityReportDownloadExcel() {

	//责任部门ID 最多20个拿逗号分隔
	var orgId = document.getElementById("orgDutyId_validity_id").value;
	//流程地图ID/流程图id 最多20个拿逗号分隔
	var processMapId = document.getElementById("processMapId_validity_id").value;

	var processValidityForm = document.getElementById("processValidityForm");

	processValidityForm.action = "downloadExpiryMaintenance.action";
	processValidityForm.submit();

}
/**
 * 上传
 */
function processValidityImportExcel() {
	
	var file=document.getElementById("validityFile");
	//未选择文件
	if(file.value.length==0){
		alert(i_needImpordExcelFile);
		return;
	}
	//后缀名不是xls
	if (!/\.xls$/.test(file.value)) { // 请选择正确的EXCEL文件(后缀为xls)！
		alert(i_selectRightExcelFile);
		return;
	}
	$.ajaxFileUpload({  
    url:basePath+"importValidityData.action",            //需要链接到服务器地址  
    secureuri:true,  
    dataType : 'text',
    fileElementId:'validityFile',                        //文件选择框的id属性  
    success: function(data, status){     
      alert(data);
    },error: function (data, status, e){  
      alert(e);
    }  
});  

}
/**
 * 导出错误数据
 */
function processValidityErrorDateExcel() {

	window.open("downloadExpiryMaintenanceErrorData.action");
}