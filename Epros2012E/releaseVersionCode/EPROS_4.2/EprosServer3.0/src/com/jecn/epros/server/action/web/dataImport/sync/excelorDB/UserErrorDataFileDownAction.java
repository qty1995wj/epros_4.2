package com.jecn.epros.server.action.web.dataImport.sync.excelorDB;

import java.io.File;
import java.io.InputStream;

import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserErrorDataFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;

/**
 * 人员同步标准版导入异常错误信息下载处理
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Mar 1, 2013 时间：11:05:03 AM
 */
public class UserErrorDataFileDownAction extends BaseAction {
	/** 对应的业务处理类 */
	private UserErrorDataFileDownBuss userErrorDataFileDownBuss = null;

	/**
	 * 
	 * 执行下载
	 * 
	 * @return
	 * @throws Exception
	 */
	public String execute() {
		return checkFile();
	}

	/**
	 * 
	 * 判读文件是否存在
	 * 
	 * @return
	 */
	private String checkFile() {
		File file = new File(userErrorDataFileDownBuss.getOutFilepath());
		if (file.exists()) {
			return SyncConstant.RESULT_SUCCESS;
		} else {
			// 没有人员错误信息
			outResultHtml(SyncErrorInfo.ERROR_FILE_NOT_EXISTS, null);
			return null;
		}
	}

	/**
	 * 
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() {
		return userErrorDataFileDownBuss.getInputStream();
	}

	/**
	 * 
	 * 对于配置中的 ${fileName}, 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	public String getFileName() {
		return userErrorDataFileDownBuss.getFileName();
	}

	public UserErrorDataFileDownBuss getUserErrorDataFileDownBuss() {
		return userErrorDataFileDownBuss;
	}

	public void setUserErrorDataFileDownBuss(
			UserErrorDataFileDownBuss userErrorDataFileDownBuss) {
		this.userErrorDataFileDownBuss = userErrorDataFileDownBuss;
	}

}
