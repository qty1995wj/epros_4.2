package com.jecn.epros.domain.org;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;

public class SearchOrgResultBean {
    private Long orgId;
    private String orgName;
    private Long pOrgId;
    private String pOrgName;

    public LinkResource getLink() {
        return new LinkResource(orgId, orgName, ResourceType.ORGANIZATION);
    }

    public LinkResource getParentLink() {
        return new LinkResource(pOrgId, pOrgName, ResourceType.ORGANIZATION);
    }

    @JsonIgnore
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @JsonIgnore
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @JsonIgnore
    public Long getpOrgId() {
        return pOrgId;
    }

    public void setpOrgId(Long pOrgId) {
        this.pOrgId = pOrgId;
    }

    @JsonIgnore
    public String getpOrgName() {
        return pOrgName;
    }

    public void setpOrgName(String pOrgName) {
        this.pOrgName = pOrgName;
    }
}
