package com.jecn.epros.server.service.process;

import java.util.List;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;
import com.jecn.epros.server.bean.process.temp.TempProcessKpiBean;
import com.jecn.epros.server.bean.process.temp.TempSearchKpiBean;
import com.jecn.epros.server.common.IBaseService;

public interface IFlowPKIService extends IBaseService<JecnFlowKpiName, Long> {
	/**
	 * 根据流程ID获取KPI集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiName> getListJecnFlowKpiName(Long flowId)
			throws Exception;

	/**
	 * 获取流程对应的KPI信息
	 * 
	 * @param flowId
	 *            流程ID
	 * @return TempProcessKpiBean流程KPI信息
	 * @throws Exception
	 */
	public TempProcessKpiBean geTempProcessKpiBean(Long flowId)
			throws Exception;

	/**
	 * 创建流程KPI曲线图
	 * 
	 * @param searchKpiBean
	 *            KPI曲线图生成条件
	 * @throws Exception
	 * 
	 */
	public String createFlowKPIDiagram(TempSearchKpiBean searchKpiBean)
			throws Exception;

	/**
	 *查询某段时间内的KPI值
	 * 
	 * @param searchKpiBean
	 * 
	 * @throws Exception
	 * 
	 */

	public List<JecnFlowKpi> getFlowKPIValue(long kpiId,
			String StringStartDate, String StringEndDate) throws Exception;

	/***
	 * 获取单个KPI的值
	 * 
	 * @param kpiId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowKpi getFlowKPIValue(long kpiId) throws Exception;

	/***
	 * 获取单个KPI的值 主要是验证数据是否重复
	 * 
	 * @param kpiId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowKpi getFlowKPIValue(long kpiAndId, String kpiVlaue)
			throws Exception;

	/***
	 * 获取单个KPI
	 * 
	 * @param kpiAndId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowKpiName getFlowKPI(long kpiAndId) throws Exception;

	/***
	 * 添加一级指标
	 * 
	 * @throws Exception
	 */
	public String addFirstTarget(List<JecnKPIFirstTargetBean> list)
			throws Exception;

	/**
	 * 获取所有 一级指标数据集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnKPIFirstTargetBean> getAllFirstTarget() throws Exception;
	
	/**
	 * 获取所有 一级指标数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public JecnKPIFirstTargetBean  getFirstTarget(String id) throws Exception;
	
	/**
	 * 定时任务删除所有的流程下的kpi的图片
	 * @throws Exception
	 */
	public void deleteKpiTempImage() throws Exception;

	public JecnCreateDoc exportFlowKpi(TempSearchKpiBean searchKpiBean)throws Exception;
}
