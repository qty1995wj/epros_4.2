package com.jecn.epros.server.download.xmlword;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.dom4j.Element;
import org.jfree.util.Log;

import sun.misc.BASE64Encoder;

/**
 * 操作 xml word 图片类
 * 
 * @author fuzhh 2013-6-3
 * 
 */
public class JecnXmlImageUtil {

	/**
	 * 图片进行放大缩小处理
	 * 
	 * @author fuzhh 2013-5-29
	 * @param buffimg
	 *            图片对象
	 * @param docWidth
	 *            页面宽度
	 * @param docHeight
	 *            页面高度
	 */
	public static BufferedImage imageScale(BufferedImage buffimg, int docWidth,
			int docHeight) {
		if (buffimg != null) {
			// 得到源图宽
			int width = buffimg.getWidth();
			// 得到源图高
			int height = buffimg.getHeight();
			// 放大倍数
			double scale = 1;
			if (docWidth < width || docHeight < height) {
				// 宽度的放大缩小比例
				double widthScale = (double) width / (double) docWidth;
				// 高度的放大缩小比例
				double heightScale = (double) height / (double) docHeight;
				if (heightScale > widthScale) {
					scale = heightScale;
				} else {
					scale = widthScale;
				}
			} else {
				// 宽度的放大缩小比例
				double widthScale = (double) docWidth / (double) width;
				// 高度的放大缩小比例
				double heightScale = (double) docHeight / (double) height;
				if (widthScale < heightScale) {
					scale = heightScale;
				} else {
					scale = widthScale;
				}
			}
			if (scale < 1) {
				buffimg = zoomInImage(buffimg, scale + 1);
			} else {
				buffimg = zoomOutImage(buffimg, scale);
			}
		}
		return buffimg;
	}

	/**
	 * 对图片进行放大
	 * 
	 * @author fuzhh 2013-5-29
	 * @param originalImage
	 *            原始图片
	 * @param scale
	 *            放大倍数
	 * @return
	 */
	public static BufferedImage zoomInImage(BufferedImage originalImage,
			double scale) {
		int width = (int) (originalImage.getWidth() * scale);
		int height = (int) (originalImage.getHeight() * scale);
		BufferedImage newImage = new BufferedImage(width, height, originalImage
				.getType());
		Graphics g = newImage.getGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		return newImage;
	}

	/**
	 * 对图片进行缩小
	 * 
	 * @author fuzhh 2013-5-29
	 * @param originalImage
	 *            原始图片
	 * @param scale
	 *            缩小倍数
	 * @return 缩小后的Image
	 */
	public static BufferedImage zoomOutImage(BufferedImage originalImage,
			double scale) {
		int width = (int) (originalImage.getWidth() / scale);
		int height = (int) (originalImage.getHeight() / scale);
		BufferedImage newImage = new BufferedImage(width, height, originalImage
				.getType());
		Graphics g = newImage.getGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		return newImage;
	}

	/**
	 * 创建图片
	 * 
	 * @author fuzhh 2013-5-29
	 * @param imgEle
	 *            图片节点
	 * @param imgPath
	 *            图片路径
	 */
	public static void createHighImage(Element sectPrImgEle, Element imgEle,
			String imgPath, int width, int height, int wideDocWidth,
			int wideDocHeight) {
		if (imgEle == null || imgPath == null) {
			return;
		}
		// 图片文件路径
		File file = new File(imgPath);
		// 图片转换
		String baseImg = getBaseImgByImgPath(file);
		// 复制的节点
		Element copeImgNode = null;
		// 获取图片
		BufferedImage sourceImg;
		FileInputStream input=null;
		try {
			input=new FileInputStream(file);
			sourceImg = ImageIO.read(input);
			// 图片宽度
			int imgWidth = sourceImg.getWidth();
			// 图片高度
			int imgHeight = sourceImg.getHeight();
			if (imgWidth > imgHeight) {
				copeImgNode = XmlDownUtil.copyNodes(sectPrImgEle);
				sourceImg = JecnXmlImageUtil.imageScale(sourceImg,
						wideDocWidth, wideDocHeight);
			} else {
				copeImgNode = XmlDownUtil.copyNodes(imgEle);
				sourceImg = JecnXmlImageUtil.imageScale(sourceImg, width,
						height);
			}
			// 图片宽度
			imgWidth = sourceImg.getWidth();
			// 图片高度
			imgHeight = sourceImg.getHeight();
			updateImgSize(copeImgNode, imgWidth, imgHeight);
		} catch (Exception e) {
			Log.error("图片转换异常createHighImage方法", e);
		}finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e) {
					Log.error("", e);
				}
			}
		}
		if (copeImgNode != null) {
			XmlDownUtil.updateImageNode(copeImgNode, "binData",
					baseImg);
		}

	}

	/**
	 * 创建图片 (根据传入的节点显示)
	 * 
	 * @author fuzhh 2013-5-30
	 * @param imgEle
	 *            img 节点
	 * @param imgPath
	 *            图片路径
	 * @param width
	 *            图片宽度
	 * @param height
	 *            图片高度
	 */
	public static void createHighImage(Element imgEle, String imgPath,
			int width, int height) {
		if (imgEle == null || imgPath == null) {
			return;
		}
		// 图片文件路径
		File file = new File(imgPath);
		// 图片转换
		String baseImg = getBaseImgByImgPath(file);
		// 复制的节点
		Element copeImgNode = null;
		// 获取图片
		BufferedImage sourceImg;
		try {
			sourceImg = ImageIO.read(new FileInputStream(file));
			copeImgNode = XmlDownUtil.copyNodes(imgEle);
			sourceImg = JecnXmlImageUtil.imageScale(sourceImg, width, height);
			// 图片宽度
			int imgWidth = sourceImg.getWidth();
			// 图片高度
			int imgHeight = sourceImg.getHeight();
			updateImgSize(copeImgNode, imgWidth, imgHeight);
		} catch (Exception e) {
			Log.error("图片转换异常createHighImage(根据传入的节点显示)方法", e);
		}
		if (copeImgNode != null) {
			XmlDownUtil.updateImageNode(copeImgNode, "binData",
					baseImg);
		}

	}

	/**
	 * 把文件转换为base64
	 * 
	 * @author fuzhh 2013-5-29
	 * @param file
	 * @return
	 */
	public static String getBaseImgByImgPath(File file) {
		// base64 对象
		BASE64Encoder encoder = new BASE64Encoder();
		// 文件输入流
		FileInputStream fis = null;
		try {
			// 文件输入流
			fis = new FileInputStream(file);
			// 转换为字节流
			byte[] buffer = new byte[(int) file.length()];
			// 读取数据字节
			fis.read(buffer);
			// 转换为base64字符串
			String s_imageData = encoder.encode(buffer);

			return s_imageData;
		} catch (Exception e) {
			Log.error("图片转换异常！", e);
			return null;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					Log.error("关闭图片输入流异常", e);
				}
			}
		}

	}

	/**
	 * 修改图片的大小
	 * 
	 * @author fuzhh 2013-5-29
	 * @param ele
	 *            节点
	 * @param width
	 *            宽度
	 * @param height
	 *            高度
	 */
	public static void updateImgSize(Element ele, int width, int height) {
		// 找到节点下的shape 节点
		List<Element> shapeEleList = new ArrayList<Element>();
		XmlDownUtil.getAllChildNodeList(ele, shapeEleList, "shape");
		Element shapeNode = null;
		if (shapeEleList.size() > 0) {
			shapeNode = shapeEleList.get(0);
		}
		String imgSize = "width:" + width + "pt;height:" + height + "pt";
		XmlDownUtil.updateNodeAttribute(shapeNode, "style", imgSize);
	}
}
