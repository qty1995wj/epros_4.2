package com.jecn.epros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@ServletComponentScan
@SpringBootApplication
public class EprosServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(EprosServerApplication.class, args);
    }
}