package com.jecn.epros.server.webBean.file;

import java.util.List;

/**
 * @author weidp
 * @description  文件清单集合类
 * @date： 日期：2014-4-18 时间：下午05:24:36
 */
public class FileDetailListWebBean {
	/** 数据集合 */
	private List<FileDetailWebBean> fileDetailList;
	/** 最大级别 */
	private int maxLevel;
	/** 当前节点级别 */
	private int curLevel;

	public List<FileDetailWebBean> getFileDetailList() {
		return fileDetailList;
	}

	public void setFileDetailList(List<FileDetailWebBean> fileDetailList) {
		this.fileDetailList = fileDetailList;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public int getCurLevel() {
		return curLevel;
	}

	public void setCurLevel(int curLevel) {
		this.curLevel = curLevel;
	}

}
