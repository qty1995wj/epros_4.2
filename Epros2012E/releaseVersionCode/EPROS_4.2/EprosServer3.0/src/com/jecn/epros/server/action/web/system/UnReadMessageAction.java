package com.jecn.epros.server.action.web.system;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.bean.system.JecnMessageReaded;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.system.IReadMessageService;
import com.jecn.epros.server.service.system.IUnReadMessageService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

/***
 * 未读消息
 * @author Administrator
 * 2013-03-05
 */
public class UnReadMessageAction extends BaseAction {
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	/**消息主键ID*/
	private Long messageId;
	/**发送人名称*/
	private String peopleName;
	/**消息主键ID集合*/
	private String mesgIds;
	/**收件人ID*/
	private Long inceptPeopleId;

	private IUnReadMessageService unReadMessageService;
	private JecnMessageReaded jecnMessage;
	private IReadMessageService readMessageService;

	private static final Logger log = Logger.getLogger(UnReadMessageAction.class);
	
	/***
	 * 获取所有的未读消息
	 * @return
	 */
	public String getUnReadMessageList(){
		try {
			//获取收件人ID
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
			.getSession().get("webLoginBean");
			inceptPeopleId = webLoginBean.getJecnUser().getPeopleId();
			//已读数据条数
			int total = unReadMessageService.getTotalUnMessageList(inceptPeopleId);
			if(total>0){
				List<JecnMessage> readUnMessageList = unReadMessageService.getAllUnReadMesgList(start, limit,inceptPeopleId); 
				JSONArray jsonArray = JSONArray.fromObject(readUnMessageList);
				outJsonPage(jsonArray.toString(), total);
			}else{
				outJsonPage("[]", total);
			}
			webLoginBean.setMessageNum(total); //设置session中用户的未读消息数
		} catch (Exception e) {
			log.error("",e);
		}
		return null;
	}
	/***
	 * 根据未读消息主键ID获取消息详细信息
	 * @return
	 */
	public String searchUnReadMessageBean(){
		try {
			//获取收件人ID
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
			.getSession().get("webLoginBean");
			
			jecnMessage = unReadMessageService.searchUnReadMesgById(messageId,peopleName);
			webLoginBean.setMessageNum(webLoginBean.getMessageNum()-1);
			
		} catch (Exception e) {
			log.error("",e);
			return null;
		}
		return SUCCESS;
	}
	/***
	 * @author zhangfatao 2013-7-29
	 * @description  删除未读消息
	 * @return String:result 未读消息数量
	 */
	public void deleteUnReadMessage(){
		//获取收件人ID
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
				.getSession().get("webLoginBean");
		try {
			List<Long> mesgIdList = new ArrayList<Long>();
			String[] splitStr = mesgIds.split(",");
			for(String mesgId : splitStr){
				mesgIdList.add(Long.valueOf(mesgId));
			}
			unReadMessageService.deleteUnReadMesgs(mesgIdList);
			webLoginBean.setMessageNum(webLoginBean.getMessageNum()-splitStr.length);
			
		} catch (Exception e) {
			log.error("删除未读消息失败",e);
		}
		String result = String.valueOf(webLoginBean.getMessageNum());
		outString(result);
	}
	
	/**
	 * @author zhangft 2013-7-25
	 * @description 标记未读消息
	 * 
	 * @return String:result未读消息数量
	 */
	public void tagUnReadMessage(){ 
		//获取收件人ID
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
				.getSession().get("webLoginBean");
		try {
			List<Long> msgIdList = new ArrayList<Long>();
			String[] splitStr = mesgIds.split(",");
			for(String msgId:splitStr){
				msgIdList.add(Long.valueOf(msgId));
			}
			unReadMessageService.tagUnReadMesgs(msgIdList);
			//更改session中当前用户的未读消息数
			webLoginBean.setMessageNum(webLoginBean.getMessageNum()-splitStr.length);
		} catch (Exception e) {
			log.error("标记未读消息失败",e);
		}
		String result = String.valueOf(webLoginBean.getMessageNum());
		outString(result); 
	}
	
	public IUnReadMessageService getUnReadMessageService() {
		return unReadMessageService;
	}

	public void setUnReadMessageService(IUnReadMessageService unReadMessageService) {
		this.unReadMessageService = unReadMessageService;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getPeopleName() {
		return peopleName;
	}

	public void setPeopleName(String peopleName) {
		this.peopleName = peopleName;
	}

	public String getMesgIds() {
		return mesgIds;
	}

	public void setMesgIds(String mesgIds) {
		this.mesgIds = mesgIds;
	}

	public JecnMessageReaded getJecnMessage() {
		return jecnMessage;
	}
	public void setJecnMessage(JecnMessageReaded jecnMessage) {
		this.jecnMessage = jecnMessage;
	}
	public IReadMessageService getReadMessageService() {
		return readMessageService;
	}
	public void setReadMessageService(IReadMessageService readMessageService) {
		this.readMessageService = readMessageService;
	}
	public Long getInceptPeopleId() {
		return inceptPeopleId;
	}
	public void setInceptPeopleId(Long inceptPeopleId) {
		this.inceptPeopleId = inceptPeopleId;
	}
	

}
