package epros.designer.gui.process.flow.file;

import java.util.List;
import java.util.Vector;

import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.rule.edit.RelatedFilePanel;
import epros.designer.util.JecnProperties;

/**
 * 相关风险
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-11-20 时间：上午10:18:04
 */
public class FlowRelatedRiskPanel extends RelatedFilePanel {

	public FlowRelatedRiskPanel(List<JecnTreeBean> treeBeanList) {
		super(treeBeanList, RelatedNodeType.risk);
	}

	

	/**
	 * 隐藏table列
	 * 
	 */
	@Override
	public void hiddenColumns() {
		// 渲染表格
		relatedFileTable.getColumnModel().getColumn(2).setCellRenderer(this.newEditJLabel());
		TableColumnModel columnModel = relatedFileTable.getColumnModel();
		columnModel.getColumn(2).setMaxWidth(100);
		columnModel.getColumn(2).setMinWidth(100);
		TableColumn tableColumn = columnModel.getColumn(0);
		// 默认隐藏
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		relatedFileTable.setRowHeight(20);
	}

	/**
	 * 获取表头
	 * 
	 * @return
	 */
	@Override
	protected Vector<Object> getTitle() {
		Vector<Object> title = new Vector<Object>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		title.add(JecnProperties.getValue("operational"));
		return title;
	}

	/**
	 * 
	 * 单元格是否可编辑
	 * 
	 * @return
	 */
	@Override
	public void isClickTableEdit() {
		// 风险第二列可编辑
		if (relatedFileTable.getSelectedColumn() == 2) {
			Long riskId = Long.valueOf(relatedFileTable.getValueAt(relatedFileTable.getSelectedRow(), 0).toString());
			// 风险属性对话框
			JecnDesignerCommon.openRisk(riskId);
		}
	}

	protected Vector<Object> initTableRowData(JecnTreeBean treeBean) {
		Vector<Object> row = new Vector<Object>();
		row = new Vector<Object>();
		row.add(treeBean.getId());
		// 文件名称
		row.add(treeBean.getName());
		row.add(this.newEditJLabel());
		return row;
	}
}
