package epros.draw.gui.line;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 画线 line2D
 * 
 * @author ZHANGXH
 * @date： 日期：Aug 16, 2012 时间：10:30:44 AM
 */
public class JecnLine2D extends Line2D {
	private Point2D startPos;
	/** 线条结束点(靠近图形点) */
	private Point2D endPos;
	/** 线条类型，横线，竖线、斜线 horizontal, // 水平 vertical, // 垂直 error */
	private LineDirectionEnum directionEnum = null;
	/** 画笔 */
	private BasicStroke stroke = null;
	/** 线条颜色 */
	private Color bodyColor = null;

	/**
	 * 创建line2D对象
	 * 
	 * @param startPos
	 *            线段开始点
	 * @param endPos
	 *            线段结束点
	 * @param directionEnum
	 *            线段为横向线段或竖向线段标识 horizontal: 水平 vertical: 垂直 error: 不正确类型
	 */
	public JecnLine2D(Point2D startPos, Point2D endPos,
			LineDirectionEnum directionEnum) {
		this.startPos = startPos;
		this.endPos = endPos;
		this.directionEnum = directionEnum;
	}

	/**
	 * 画线
	 * 
	 * @param g
	 */
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; // 定义画笔
		RenderingHints originalRenderingHints = g2d.getRenderingHints();
		// 指定RenderingHints
		g2d.setRenderingHints(JecnUIUtil.getRenderingHints());

		g2d.setColor(Color.WHITE);
		g2d.setStroke(stroke);
		g2d.setPaint(bodyColor);
		g2d.draw(this);

		// 还原原始RenderingHints对象
		g2d.setRenderingHints(originalRenderingHints);
	}

	@Override
	public Point2D getP1() {
		switch (directionEnum) {
		case horizontal:// 横线
		case vertical:// 纵线
			return new Point();
		default:
			return startPos;
		}
	}

	@Override
	public Point2D getP2() {
		Point point = null;
		int lineLength = 0;
		switch (directionEnum) {
		case horizontal:// 横线
			lineLength = Math.abs(DrawCommon.convertDoubleToInt(startPos.getX()
					- endPos.getX()));
			point = new Point(lineLength, 0);
			break;
		case vertical:// 纵线
			lineLength = Math.abs(DrawCommon.convertDoubleToInt(startPos.getY()
					- endPos.getY()));
			point = new Point(0, lineLength);
			break;
		default:
			return startPos;
		}
		return point;
	}

	@Override
	public double getX1() {
		return this.getP1().getX();
	}

	@Override
	public double getX2() {
		return this.getP2().getX();
	}

	@Override
	public double getY1() {
		return this.getP1().getY();
	}

	@Override
	public double getY2() {
		return this.getP2().getY();
	}

	@Override
	public void setLine(double x1, double y1, double x2, double y2) {

	}

	public Rectangle2D getBounds2D() {
		return null;
	}

	public Point2D getStartPos() {
		return startPos;
	}

	public void setStartPos(Point2D startPos) {
		this.startPos = startPos;
	}

	public Point2D getEndPos() {
		return endPos;
	}

	public void setEndPos(Point2D endPos) {
		this.endPos = endPos;
	}

	public LineDirectionEnum getDirectionEnum() {
		return directionEnum;
	}

	public void setDirectionEnum(LineDirectionEnum directionEnum) {
		this.directionEnum = directionEnum;
	}

	public BasicStroke getStroke() {
		return stroke;
	}

	public void setStroke(BasicStroke stroke) {
		this.stroke = stroke;
	}

	public Color getBodyColor() {
		return bodyColor;
	}

	public void setBodyColor(Color bodyColor) {
		this.bodyColor = bodyColor;
	}

}
