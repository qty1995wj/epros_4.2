
package com.jecn.webservice.zhongsh.i3.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.jecn.webservice.zhongsh.i3.core.EResult;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SaveWorkflowName_QNAME = new QName("http://tempuri.org/", "name");
    private final static QName _SaveWorkflowRemark_QNAME = new QName("http://tempuri.org/", "remark");
    private final static QName _SaveWorkflowDef_QNAME = new QName("http://tempuri.org/", "def");
    private final static QName _SaveWorkflowCode_QNAME = new QName("http://tempuri.org/", "code");
    private final static QName _SaveWorkflowResponseSaveWorkflowResult_QNAME = new QName("http://tempuri.org/", "SaveWorkflowResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SaveWorkflow }
     * 
     */
    public SaveWorkflow createSaveWorkflow() {
        return new SaveWorkflow();
    }

    /**
     * Create an instance of {@link SaveWorkflowResponse }
     * 
     */
    public SaveWorkflowResponse createSaveWorkflowResponse() {
        return new SaveWorkflowResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "name", scope = SaveWorkflow.class)
    public JAXBElement<String> createSaveWorkflowName(String value) {
        return new JAXBElement<String>(_SaveWorkflowName_QNAME, String.class, SaveWorkflow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "remark", scope = SaveWorkflow.class)
    public JAXBElement<String> createSaveWorkflowRemark(String value) {
        return new JAXBElement<String>(_SaveWorkflowRemark_QNAME, String.class, SaveWorkflow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "def", scope = SaveWorkflow.class)
    public JAXBElement<String> createSaveWorkflowDef(String value) {
        return new JAXBElement<String>(_SaveWorkflowDef_QNAME, String.class, SaveWorkflow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "code", scope = SaveWorkflow.class)
    public JAXBElement<String> createSaveWorkflowCode(String value) {
        return new JAXBElement<String>(_SaveWorkflowCode_QNAME, String.class, SaveWorkflow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SaveWorkflowResult", scope = SaveWorkflowResponse.class)
    public JAXBElement<EResult> createSaveWorkflowResponseSaveWorkflowResult(EResult value) {
        return new JAXBElement<EResult>(_SaveWorkflowResponseSaveWorkflowResult_QNAME, EResult.class, SaveWorkflowResponse.class, value);
    }

}
