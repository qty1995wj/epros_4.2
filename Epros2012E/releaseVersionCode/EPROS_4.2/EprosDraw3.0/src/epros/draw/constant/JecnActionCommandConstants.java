package epros.draw.constant;

/**
 * 
 * 动作命令名称记录类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnActionCommandConstants {
	/** 工具栏->格式->网格 */
	public static final String TOOBAR_FORMAT_GRID = "toobar_format_grid";
	/** 工具栏->格式->横分割线 */
	public static final String TOOBAR_FORMAT_PARALLELINES = "toobar_format_ParallelLines";
	/** 工具栏->格式->竖分割线 */
	public static final String TOOBAR_FORMAT_VERTICALINE = "toobar_format_VerticalLine";
	/** 工具栏->格式->泳池 */
	public static final String TOOBAR_FORMAT_MODEL = "toobar_format_Model";

	/** 确认按钮动作命令标识 */
	public static final String OK_BUTTON = "okBtn";
	/** 取消动作命令标识 */
	public static final String CANCEL_BUTTON = "cancelBtn";

	/** 放大缩小下拉框 */
	public static final String ZOOM_COMBO_BOX = "zoomComboBox";
}
