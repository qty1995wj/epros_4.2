package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.standard.StandardChooseDialog;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnActiveStandardBeanT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 活动相关标准
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-11 时间：下午03:04:15
 */
public class ActiveRefStandardsPanel extends JecnPanel {
	private Logger log = Logger.getLogger(ActiveRefStandardsPanel.class);
	private JecnActiveData activeData;
	/** 所在的滚动面板 */
	private JScrollPane activeScrollPane = new JScrollPane();
	/** 表单 */
	private JTable jecnTable = null;

	/** 主面板 */
	private JecnPanel mainPanel = null;
	/** 按钮面板 */
	private JecnPanel buttonPanel = null;
	/** 关联按钮 */
	private JButton refButton = null;
	/** 删除按钮 */
	private JButton deleteBuffon = null;

	/** 标准数据 */
	private List<JecnTreeBean> listStander = null;

	public ActiveRefStandardsPanel(JecnActiveData activeData) {
		this.activeData = activeData;
		initComs();
		initLayout();
		initData();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.activeData = jecnActiveData;
		initData();
		((ActiveRefTableModel) jecnTable.getModel()).remoeAll();
		Vector<Vector<Object>> fileData = getContent();
		for (Vector<Object> v : fileData) {
			((DefaultTableModel) jecnTable.getModel()).addRow(v);
		}
	}

	/**
	 * 组件初始化
	 * 
	 */
	private void initComs() {
		// 设置表格大小
		Dimension dimension = null;
		if (JecnDesignerCommon.isShowEleAttribute()) {
			mainPanel = new JecnPanel(580, 180);
			dimension = new Dimension(550, 180);
		} else {
			mainPanel = new JecnPanel(580, 400);
			dimension = new Dimension(550, 400);
		}

		buttonPanel = new JecnPanel();
		// 关联
		refButton = new JButton(JecnProperties.getValue("relevance"));
		// 删除
		deleteBuffon = new JButton(JecnProperties.getValue("deleteBtn"));

		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		refButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		deleteBuffon.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		jecnTable = new ActiveTable();
		// 设置默认背景色
		jecnTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		jecnTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// 列不可拖动
		jecnTable.getTableHeader().setReorderingAllowed(false);
		TableColumnModel columnModel = jecnTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(0);
		// 默认隐藏
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		// 操作最小100，最大150
		columnModel.getColumn(1).setMinWidth(0);
		columnModel.getColumn(1).setMaxWidth(0);
		// 操作最小100，最大150
		columnModel.getColumn(4).setMinWidth(100);
		columnModel.getColumn(4).setMaxWidth(150);
		// 类型最小100，最大150
		columnModel.getColumn(3).setMinWidth(100);
		columnModel.getColumn(3).setMaxWidth(150);

		// 设置行高
		jecnTable.setRowHeight(20);

		activeScrollPane.setPreferredSize(dimension);
		activeScrollPane.setMaximumSize(dimension);
		activeScrollPane.setMinimumSize(dimension);
		activeScrollPane.setViewportView(jecnTable);
		activeScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 背景色
		activeScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 添加按钮
		jecnTable.getColumnModel().getColumn(4).setCellRenderer(new EditJLabel());

	}

	private void initLayout() {
		/** 标签间距 */
		Insets insetsTable = new Insets(1, 1, 0, 1);
		// 添加组件，设置组件布局
		Insets insetsPanel = new Insets(0, 1, 3, 1);
		// 新建控制点
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insetsTable, 0, 0);
		mainPanel.add(activeScrollPane, c);
		// 控制点面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsPanel, 0, 0);
		mainPanel.add(buttonPanel, c);

		// 按钮面板
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(refButton);
		buttonPanel.add(deleteBuffon);

		// 添加主面板
		Insets insets1 = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets1,
				0, 0);
		this.add(mainPanel, c);

		/**
		 * 关联
		 * 
		 */
		refButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				refButtonListener();
			}
		});
		/**
		 * 删除
		 * 
		 */
		deleteBuffon.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectRows = jecnTable.getSelectedRows();
				if (selectRows.length == 0) {
					// 请选择一行!
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
					return;
				}
				List<JecnTreeBean> deleteList = new ArrayList<JecnTreeBean>();
				for (int i = selectRows.length - 1; i >= 0; i--) {
					((DefaultTableModel) jecnTable.getModel()).removeRow(selectRows[i]);
					deleteList.add(listStander.get(selectRows[i]));
				}
				// 删除集合中存在
				listStander.removeAll(deleteList);
			}
		});

	}

	private void refButtonListener() {
		if (listStander == null) {
			listStander = new ArrayList<JecnTreeBean>();
		}
		StandardChooseDialog chooseDialog = new StandardChooseDialog(listStander, 0);
		chooseDialog.setVisible(true);
		if (chooseDialog.isOperation()) {// 操作
			initTableData();
		}
	}

	private void initData() {

	}

	/**
	 * 
	 * 表格获取数据
	 */
	private void initTableData() {
		if (listStander.size() > 0) {
			// 清除表格数据
			((ActiveRefTableModel) jecnTable.getModel()).remoeAll();
			Vector<Object> row = null;
			for (JecnTreeBean treeBean : listStander) {
				row = new Vector<Object>();
				row.add(treeBean.getId());
				row.add(treeBean.getPid());
				row.add(treeBean.getName());
				// 获取类型对应的名称
				row.add(JecnUtil.getTypeName(treeBean.getTreeNodeType()));
				// 详细
				row.add(new EditJLabel());
				((ActiveRefTableModel) jecnTable.getModel()).addRow(row);
			}
		} else {
			// 清除表格数据
			((ActiveRefTableModel) jecnTable.getModel()).remoeAll();
		}
	}

	/**
	 * 确定按钮
	 * 
	 */
	public void okButton() {
		activeData.getListJecnActiveStandardBeanT().clear();
		activeData.getListJecnActiveStandardBeanT().addAll(getResultList());
	}

	private List<JecnActiveStandardBeanT> getResultList() {
		int rows = jecnTable.getRowCount();
		String standardId = null;
		String pId = null;
		String name = null;
		int relaType;

		boolean isUpdate = false;
		// 添加的集合
		List<JecnActiveStandardBeanT> resultList = new ArrayList<JecnActiveStandardBeanT>();
		for (int i = 0; i < rows; i++) {
			standardId = (jecnTable.getValueAt(i, 0)).toString();
			if ((jecnTable.getValueAt(i, 1)) != null) {
				pId = (jecnTable.getValueAt(i, 1)).toString();
			}
			name = (jecnTable.getValueAt(i, 2)).toString();
			relaType = JecnUtil.getTypeByName((jecnTable.getValueAt(i, 3)).toString());
			for (JecnActiveStandardBeanT activeStandardBeanT : activeData.getListJecnActiveStandardBeanT()) {
				if (activeStandardBeanT.getStandardId().equals(standardId)) {
					resultList.add(activeStandardBeanT);
					isUpdate = true;
					break;
				}
			}
			if (!isUpdate) {// 没有更新，执行添加
				JecnActiveStandardBeanT activeStandardBeanT = new JecnActiveStandardBeanT();
				activeStandardBeanT.setActiveId(activeData.getFlowElementId());
				if (!DrawCommon.isNullOrEmtryTrim(pId)) {
					activeStandardBeanT.setClauseId(Long.valueOf(pId));
				}
				activeStandardBeanT.setRelaName(name);
				activeStandardBeanT.setRelaType(relaType);
				activeStandardBeanT.setStandardId(Long.valueOf(standardId));
				activeStandardBeanT.setFigureUUID(activeData.getUUID());
				resultList.add(activeStandardBeanT);
			}
			isUpdate = false;
		}
		return resultList;
	}

	/** 选中table 当前行和列 */
	protected int myRow = -10;

	protected int myColumn = -10;

	class ActiveTable extends JTable {
		public ActiveTable() {
			super();
			this.setModel(getTableModel());

			// 选中行事件 true 选中
			// this.setRowSelectionAllowed(false);
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {// 双击
						// 编辑处理
						isClickTableEdit();
					}
				}

				/**
				 * 鼠标移动到表格监听
				 */
				public void mouseExited(MouseEvent e) {
					mouseMovedProcess(e);
				}
			});
			/**
			 * 添加鼠标监听
			 * 
			 */
			this.addMouseMotionListener(new MouseAdapter() {
				public void mouseMoved(MouseEvent e) {
					mouseMovedProcess(e);
				}
			});
		}

		/**
		 * 鼠标进入表格监听
		 * 
		 * @param e
		 */
		protected void mouseMovedProcess(MouseEvent e) {
			int row = this.rowAtPoint(e.getPoint());
			int column = this.columnAtPoint(e.getPoint());
			if (myRow == row && column == myColumn) {// 始终在指定的单元格移动，不执行重绘
				return;
			}
			myRow = this.rowAtPoint(e.getPoint());
			myColumn = this.columnAtPoint(e.getPoint());

			this.revalidate();
			this.repaint();
		}
	}

	/***************************************************************************
	 * 初始化内容
	 * 
	 * @return
	 */
	private Vector<Vector<Object>> getContent() {
		if (activeData == null) {
			return null;
		}
		Vector<Vector<Object>> vs = new Vector<Vector<Object>>();
		if (listStander == null) {
			listStander = new ArrayList<JecnTreeBean>();
		} else {
			listStander.clear();
		}
		JecnTreeBean treeBean = null;
		try {
			// 关联类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
			for (JecnActiveStandardBeanT activeStandardBeanT : activeData.getListJecnActiveStandardBeanT()) {
				treeBean = new JecnTreeBean();
				Vector<Object> row = new Vector<Object>();
				row.add(activeStandardBeanT.getStandardId());
				// 所属条款ID
				row.add(activeStandardBeanT.getClauseId());
				// 文件名称
				row.add(activeStandardBeanT.getRelaName());
				// 关联类型
				row.add(JecnUtil.getStandardTypeName(activeStandardBeanT.getRelaType()));
				// 明细操作
				row.add(new EditJLabel());
				vs.add(row);

				// 初始化树结构
				treeBean.setId(activeStandardBeanT.getStandardId());
				treeBean.setName(activeStandardBeanT.getRelaName());
				treeBean.setTreeNodeType(JecnUtil.getNodeTypeByRelaType(activeStandardBeanT.getRelaType()));
				treeBean.setPid(activeStandardBeanT.getClauseId());
				listStander.add(treeBean);
			}

		} catch (Exception e) {
			log.error("ActiveRefStandardsPanel getContent is error", e);
		}
		return vs;
	}

	/**
	 * 生成table的Model
	 * 
	 * @return
	 */
	private ActiveRefTableModel getTableModel() {
		Vector<Object> title = new Vector<Object>();
		title.add("id");
		title.add("条款ID");
		title.add(JecnProperties.getValue("fileName"));
		// 类型
		title.add(JecnProperties.getValue("type"));
		// 详情
		title.add(JecnProperties.getValue("operational"));
		return new ActiveRefTableModel(title, getContent());
	}

	class ActiveRefTableModel extends DefaultTableModel {
		public ActiveRefTableModel(Vector<Object> title, Vector<Vector<Object>> content) {
			this.setDataVector(content, title);
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

		public void remoeAll() {
			// 清空
			for (int index = this.getRowCount() - 1; index >= 0; index--) {
				this.removeRow(index);
			}
		}
	}

	/**
	 * 
	 * 渲染
	 * 
	 * @author Administrator
	 * @date： 日期：2013-11-20 时间：上午10:51:08
	 */
	class EditJLabel extends JLabel implements TableCellRenderer {
		// true：需要加色渲染
		private boolean isDef = false;

		EditJLabel() {
			this.setHorizontalAlignment(JLabel.CENTER);
			this.setVerticalAlignment(JLabel.CENTER);
			// 详情
			this.setText(JecnProperties.getValue("linkDetail"));
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			this.isDef = (myRow == row && myColumn == column);
			return this;
		}

		/**
		 * 
		 * 绘制组件
		 * 
		 */
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;

			int w = this.getWidth();
			int h = this.getHeight();
			if (isDef) {
				// 渐变
				g2d.setPaint(new GradientPaint(0, 0, JecnUIUtil.getTopNoColor(), 0, h / 2, JecnUIUtil
						.getButtomNoColor()));
				g2d.fillRect(0, 0, w, h);
				this.setForeground(Color.red);
			} else {
				this.setForeground(Color.black);
			}

			super.paint(g);
		}
	}

	/**
	 * 
	 * 详情双击
	 * 
	 * @return
	 */
	public void isClickTableEdit() {
		// 风险第二列可编辑
		if (jecnTable.getSelectedColumn() == 4) {
			int row = jecnTable.getSelectedRow();
			// 标准类型
			int relateType = JecnUtil.getTypeByName(jecnTable.getValueAt(row, 3).toString());
			Long standardId = Long.valueOf(jecnTable.getValueAt(row, 0).toString());
			// 打开标准
			JecnDesignerCommon.openStandard(standardId, relateType);
		}
	}

	public boolean isUpdate() {
		List<JecnActiveStandardBeanT> standardBeanTs = getResultList();

		if (standardBeanTs.size() != activeData.getListJecnActiveStandardBeanT().size()) {
			return true;
		}
		for (JecnActiveStandardBeanT newStandardBeanT : standardBeanTs) {
			boolean isExist = false;
			for (JecnActiveStandardBeanT oldStandardBeanT : activeData.getListJecnActiveStandardBeanT()) {
				if (newStandardBeanT.getStandardId().equals(oldStandardBeanT.getStandardId())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}
}
