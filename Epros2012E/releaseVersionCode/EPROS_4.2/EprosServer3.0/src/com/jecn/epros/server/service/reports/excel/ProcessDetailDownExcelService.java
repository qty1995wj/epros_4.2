package com.jecn.epros.server.service.reports.excel;

import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.CompositeTitle;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleEdge;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.reports.ProcessDetailBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailLevel;
import com.jecn.epros.server.webBean.reports.ProcessDetailWebBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailWebListBean;

/**
 * 
 * 流程清单统计
 * 
 * @author huoyl
 * 
 */
public class ProcessDetailDownExcelService {
	private Log log = LogFactory.getLog(this.getClass());
	/** 数据对象 */
	private ProcessDetailBean processDetailBean;
	/** 发布的流程的总数的和 */
	private int pubCountSum = 0;
	/** 未发布的流程的总数的和 */
	private int notPubCountSum = 0;
	/** 流程的总数的和 */
	private int allCountSum = 0;
	/** 流程sheet到达的数量 */
	private int curSheet = 0;

	public ProcessDetailBean getProcessDetailBean() {
		return processDetailBean;
	}

	public void setProcessDetailBean(ProcessDetailBean processDetailBean) {
		this.processDetailBean = processDetailBean;
	}

	public byte[] getExcelFile() throws Exception {
		int type = processDetailBean.getType();
		byte[] tempDate = null;
		if (processDetailBean == null) {
			throw new Exception("ProcessDetailDownExcelService类processDetailBean数据为空");
		}

		if (processDetailBean.getAllDetailWebListBeans().size() == 0) {
			throw new Exception("ProcessDetailDownExcelService类processDetailBean.getAllDetailWebListBeans().size()为0");
		}
		// 设置sheet的名称
		setProcessDetailSheetName(processDetailBean.getAllDetailWebListBeans());

		WritableWorkbook wbookData = null;
		File file = null;
		File pngFile = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path + JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);

			// 开始写入第一页
			WritableSheet dataSheet = wbookData.createSheet(JecnUtil.getValue("dataStatistical"), curSheet);
			// 流程监护人是否显示
			boolean guardianPeopleIsShow = processDetailBean.isGuardianPeopleIsShow();
			// 合并第一行单元格
			dataSheet.mergeCells(0, 0, 4, 0);
			// 设置行高 需要乘以20
			dataSheet.setRowView(0, 360);
			dataSheet.addCell(new Label(0, 0, getFirstRowContent(), getFirstSheetHeaderStyle()));
			// 写出第二行 表头
			// 设置列宽
			dataSheet.setColumnView(0, 19);
			dataSheet.setColumnView(1, 14);
			dataSheet.setColumnView(2, 14);
			dataSheet.setColumnView(3, 14);
			dataSheet.setColumnView(4, 19);
			// 部门/流程地图
			// 已发布流程
			// 未发布流程
			// 流程清单总数
			// 流程实际覆盖率
			String orgOrMap = getName(JecnUtil.getValue("orgOrMap"), JecnUtil.getValue("orgOrRule"));
			String hasPubS = getName(JecnUtil.getValue("hasPubProcess"), JecnUtil.getValue("hasPubRule"));
			String allCount = getName(JecnUtil.getValue("pubAndNotPubProcess"), JecnUtil.getValue("pubAndNotPubRule"));
			String notPubS = getName(JecnUtil.getValue("hasNotPubProcess"), JecnUtil.getValue("hasNotPubRule"));
			String percent = getName(JecnUtil.getValue("processPubPercent"), JecnUtil.getValue("rulePubPercent"));
			// 总计
			String totalStr = JecnUtil.getValue("totalStr");

			dataSheet.addCell(new Label(0, 1, orgOrMap, getFirstSheetSecondStyle(Colour.BLACK)));
			dataSheet.addCell(new Label(1, 1, hasPubS, getFirstSheetSecondStyle(Colour.GREEN)));
			dataSheet.addCell(new Label(2, 1, notPubS, getFirstSheetSecondStyle(Colour.BLACK)));
			dataSheet.addCell(new Label(3, 1, allCount, getFirstSheetSecondStyle(Colour.DARK_RED)));
			dataSheet.addCell(new Label(4, 1, percent, getFirstSheetSecondStyle(Colour.BLACK)));

			WritableCellFormat firstSheetStyle = getFirstSheetStyle();
			// 实际覆盖率颜色为黄色
			WritableCellFormat percentStyle = getFirstSheetStyle();
			percentStyle.setBackground(Colour.YELLOW);

			List<ProcessDetailWebListBean> allDetailWebListBeans = processDetailBean.getAllDetailWebListBeans();
			// 第一页 第三行开始
			int row = 1;
			for (int i = 0; i < allDetailWebListBeans.size(); i++) {
				row = row + 1;
				ProcessDetailWebListBean bean = allDetailWebListBeans.get(i);

				pubCountSum += bean.getPubCount();
				notPubCountSum += bean.getNotPubCount();
				allCountSum += bean.getAllCount();
				// 责任部门/流程地图
				dataSheet.addCell(new Label(0, row, bean.getSheetName(), firstSheetStyle));
				dataSheet.addCell(new Label(1, row, String.valueOf(bean.getPubCount()), firstSheetStyle));
				dataSheet.addCell(new Label(2, row, String.valueOf(bean.getNotPubCount()), firstSheetStyle));
				dataSheet.addCell(new Label(3, row, String.valueOf(bean.getAllCount()), firstSheetStyle));
				// 覆盖率从零点几转换为百分比
				String pubPercent = (int) (bean.getPubPercent() * 100) + "%";
				dataSheet.addCell(new Label(4, row, pubPercent, percentStyle));

			}
			row++;
			// 写出总计行
			dataSheet.addCell(new Label(0, row, totalStr, firstSheetStyle));
			dataSheet.addCell(new Label(1, row, String.valueOf(pubCountSum), firstSheetStyle));
			dataSheet.addCell(new Label(2, row, String.valueOf(notPubCountSum), firstSheetStyle));
			dataSheet.addCell(new Label(3, row, String.valueOf(allCountSum), firstSheetStyle));
			String pubPercent;
			if (0 == allCountSum) {

				pubPercent = "0%";
			} else {

				pubPercent = (pubCountSum * 100 / allCountSum) + "%";
			}

			dataSheet.addCell(new Label(4, row, pubPercent, percentStyle));
			row++;
			// 写出图片 每一个部门显示的柱状图的宽度是80,然后总计显示的宽度再加80
			int imageWidth = allDetailWebListBeans.size() * 80 + 200 + 80;
			if (imageWidth < 500) {
				imageWidth = 500;
			}
			int imageHeight = 400;
			String imageAbsPath = this.createDetailImage(dataSheet, imageWidth, imageHeight);

			pngFile = new File(imageAbsPath);
			// 将图片文件插入到excel中
			JecnUtil.addPictureToExcel(dataSheet, pngFile, row + 2, 0);

			// 从第二个sheet页开始写出的是详细信息-----------------------------
			// 获得每一个部门或者流程地图的bean

			// 级编号
			String levelNumber = JecnUtil.getValue("levelNumber");
			// 级名称
			String levelName = JecnUtil.getValue("levelName");
			// 流程名称
			String processName = getName(JecnUtil.getValue("processName"), JecnUtil.getValue("ruleName"));
			// 流程编号
			String processNumber = getName(JecnUtil.getValue("processNumber"), JecnUtil.getValue("ruleNumber"));
			// 发布状态
			String pubState = JecnUtil.getValue("pubState");
			// 责任部门
			String responsibilityDepartment = JecnUtil.getValue("responsibilityDepartment");
			// 流程责任人
			String processResponsiblePersons = getName(JecnUtil.getValue("processResponsiblePersons"), JecnUtil
					.getValue("ruleResponsiblePersons"));
			// 流程监护人
			String processTheGuardian = getName(JecnUtil.getValue("processTheGuardian"), JecnUtil
					.getValue("ruleTheGuardian"));
			// 拟稿人
			String peopleMake = JecnUtil.getValue("peopleMake");
			// 版本号
			String versionNumber = JecnUtil.getValue("versionNumber");
			// 发布日期
			String releaseDate = JecnUtil.getValue("releaseDate");
			// 有效期
			String validity = JecnUtil.getValue("validity");
			// 是否及时优化
			String theTimelyOptimization = JecnUtil.getValue("theTimelyOptimization");
			// 下次审视需完成时间
			String nextViewNeedFinishTime = JecnUtil.getValue("nextViewNeedFinishTime");
			// 是
			String yes = JecnUtil.getValue("is");
			// 否
			String no = JecnUtil.getValue("no");
			// 已发布
			String hasPub = JecnUtil.getValue("hasPub");
			// 未发布
			String hasNotPub = JecnUtil.getValue("hasNotPub");
			// 永久
			String permanent = JecnUtil.getValue("permanent");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			// 表头的样式
			WritableCellFormat headStyle = getHeaderCellStyle();
			// 流程名称与编号的样式
			WritableCellFormat nameAndInputStyle = getHeaderCellStyle();
			nameAndInputStyle.setBackground(Colour.LIGHT_ORANGE);
			// 内容
			WritableCellFormat contentCellStyle = getContentCellStyle();

			for (ProcessDetailWebListBean detailListBean : allDetailWebListBeans) {
				int maxLevel = detailListBean.getMaxLevel();
				int contentStart = 2 * maxLevel + 1;
				List<ProcessDetailWebBean> allDetailWebBean = detailListBean.getAllDetailWebBeans();

				curSheet++;
				// 写出sheet
				WritableSheet sheet = wbookData.createSheet(detailListBean.getSheetName(), curSheet);

				// 写出表头从第一行开始
				// 第一行有个空格
				sheet.addCell(new Label(0, 0, ""));
				sheet.setColumnView(0, 3); // 设置下次审视需完成时间的宽度
				sheet.setRowView(0, 360); // 设置行的高度
				// 0级名称
				// 0级编号
				// 循环写出表头编号和名称
				int col = 1;
				for (int i = 0; i < maxLevel; i++) {
					sheet.setColumnView(col, 17); // 设置流程名称的宽度
					sheet.setColumnView(col + 1, 17); // 设置流程编号的宽度
					sheet.addCell(new Label(col++, 0, i + levelName, headStyle));
					sheet.addCell(new Label(col++, 0, i + levelNumber, headStyle));

				}
				// 从第contentStart开始写出剩余表头

				sheet.setColumnView(contentStart, 25); // 设置流程名称的宽度
				sheet.addCell(new Label(contentStart++, 0, processName, nameAndInputStyle));

				sheet.setColumnView(contentStart, 16); // 设置流程编号的宽度
				sheet.addCell(new Label(contentStart++, 0, processNumber, nameAndInputStyle));

				sheet.setColumnView(contentStart, 18); // 设置发布日期的宽度
				sheet.addCell(new Label(contentStart++, 0, pubState, headStyle));

				sheet.setColumnView(contentStart, 25); // 设置责任部门的宽度
				sheet.addCell(new Label(contentStart++, 0, responsibilityDepartment, headStyle));

				sheet.setColumnView(contentStart, 14); // 设置流程责任人的宽度
				sheet.addCell(new Label(contentStart++, 0, processResponsiblePersons, headStyle));
				if (guardianPeopleIsShow) {
					sheet.setColumnView(contentStart, 14); // 设置流程监护人的宽度
					sheet.addCell(new Label(contentStart++, 0, processTheGuardian, headStyle));

				}

				sheet.setColumnView(contentStart, 14); // 设置拟稿人的宽度
				sheet.addCell(new Label(contentStart++, 0, peopleMake, headStyle));

				sheet.setColumnView(contentStart, 14); // 设置版本信息的宽度
				sheet.addCell(new Label(contentStart++, 0, versionNumber, headStyle));

				sheet.setColumnView(contentStart, 16); // 设置发布日期的宽度
				sheet.addCell(new Label(contentStart++, 0, releaseDate, headStyle));

				sheet.setColumnView(contentStart, 14); // 设置流程有效期的宽度
				sheet.addCell(new Label(contentStart++, 0, validity, headStyle));

				sheet.setColumnView(contentStart, 14); // 设置是否及时优化的宽度
				sheet.addCell(new Label(contentStart++, 0, theTimelyOptimization, headStyle));

				sheet.setColumnView(contentStart, 22); // 设置下次审视需完成时间的宽度
				sheet.addCell(new Label(contentStart++, 0, nextViewNeedFinishTime, headStyle));

				/** 从第二行开始写出数据 */
				// 行号
				row = 1;
				// 获得流程清单的集合 数据不得超过15000行
				for (ProcessDetailWebBean webBean : allDetailWebBean) {

					List<ProcessDetailLevel> levelList = webBean.getProcessDetailLevelList();

					// 第一格有编号
					sheet.addCell(new Label(0, row, row + "", contentCellStyle));
					// 设置行高
					sheet.setRowView(row, 360);

					int levelSize = levelList.size();
					// 写出几级名称和编号行的数据
					col = 1;
					for (int i = levelSize - 1; i >= 0; i--) {

						ProcessDetailLevel processDetailLevel = levelList.get(i);
						sheet.addCell(new Label(col++, row, processDetailLevel.getLevelName(), contentCellStyle));
						sheet.addCell(new Label(col++, row, processDetailLevel.getLevelNum(), contentCellStyle));

					}

					contentStart = 2 * maxLevel + 1;
					// 写出内容为空的单元格
					for (int i = 2 * levelSize + 1; i < contentStart; i++) {
						sheet.addCell(new Label(i, row, "", contentCellStyle));
					}

					sheet.addCell(new Label(contentStart++, row, webBean.getFlowName(), contentCellStyle));
					sheet.addCell(new Label(contentStart++, row, webBean.getFlowIdInput(), contentCellStyle));

					// 是否发布 处于什么阶段 0是待建，1是审批，2是发布
					if (webBean.getTypeByData() == 2)// 发布
					{
						sheet.addCell(new Label(contentStart++, row, hasPub, contentCellStyle));
					} else {
						sheet.addCell(new Label(contentStart++, row, hasNotPub, contentCellStyle));
					}

					sheet.addCell(new Label(contentStart++, row, webBean.getTrueOrgName(), contentCellStyle));
					sheet.addCell(new Label(contentStart++, row, webBean.getResPeopleName(), contentCellStyle));
					if (guardianPeopleIsShow) {
						sheet.addCell(new Label(contentStart++, row, webBean.getGuardianName(), contentCellStyle));
					}

					sheet.addCell(new Label(contentStart++, row, webBean.getDraftPerson(), contentCellStyle));
					sheet.addCell(new Label(contentStart++, row, webBean.getVersionId(), contentCellStyle));
					sheet.addCell(new Label(contentStart++, row, webBean.getPubDate(), contentCellStyle));
					if (webBean.getTypeByData() == 2) {
						// 有效期
						if (webBean.getExpiry() == 0) {
							// 永久
							sheet.addCell(new Label(contentStart++, row, permanent, contentCellStyle));
						} else {
							sheet.addCell(new Label(contentStart++, row, String.valueOf(webBean.getExpiry()),
									contentCellStyle));
						}
					} else {
						sheet.addCell(new Label(contentStart++, row, "", contentCellStyle));
					}

					// //是否优化 1.为是 0.为否 .2 为空
					if (1 == webBean.getOptimization()) {
						// 是
						sheet.addCell(new Label(contentStart++, row, yes, contentCellStyle));
					} else if (0 == webBean.getOptimization()) {
						// 否
						sheet.addCell(new Label(contentStart++, row, no, contentCellStyle));
					} else {
						sheet.addCell(new Label(contentStart++, row, "", contentCellStyle));
					}

					// 下次审视需完成时间
					String nextScanData = webBean.getNextScanData();
					// 下次需审视日期
					if (nextScanData == null) {
						nextScanData = "";
					}
					// 下次发布日期
					sheet.addCell(new Label(contentStart++, row, nextScanData, contentCellStyle));
					// 行号加一
					row++;
				}

			}

			// 后续处理--------------------------------------
			wbookData.write();
			// 工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;
			// 把一个文件转化为字节
			tempDate = JecnUtil.getByte(file);

		} catch (Exception e) {
			throw new Exception("ProcessDetailDownExcelService类getExcelFile方法异常", e);
		} finally {

			// 删除临时文件临时图片
			if (file != null && file.exists()) {
				file.delete();
			}
			// 删除生成的图片
			if (pngFile != null && pngFile.exists()) {
				pngFile.delete();
			}
			try {
				if (wbookData != null) {
					wbookData.close();
				}
			} catch (Exception e) {
				throw new Exception("ProcessDetailDownExcelService关闭excel工作表异常", e);
			}
		}

		return tempDate;
	}

	private String getName(String a, String b) {
		if (processDetailBean.getType() == 0) {
			return a;
		}
		return b;
	}

	/**
	 * 使用KeySet去重，并获取唯一的sheetName
	 * 
	 * @param allDetailWebListBeans
	 */
	private void setProcessDetailSheetName(List<ProcessDetailWebListBean> allDetailWebListBeans) {

		Map<String, Integer> sheetNames = new HashMap<String, Integer>();
		for (ProcessDetailWebListBean bean : allDetailWebListBeans) {
			String sheetName = bean.getName();
			if (!sheetNames.keySet().contains(sheetName)) {
				sheetNames.put(bean.getName(), 0);
			} else {// 如果sheet的名称重复那么累加1
				sheetNames.put(sheetName, sheetNames.get(sheetName) + 1);
				sheetName = sheetName + "(" + sheetNames.get(sheetName) + ")";
			}
			bean.setSheetName(sheetName);
		}

	}

	// 第一页第一行标题栏样式
	private WritableCellFormat getFirstSheetHeaderStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.WHITE);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.RED);
		return tempCellFormat;
	}

	// 第一页第二行
	private WritableCellFormat getFirstSheetSecondStyle(Colour colour) throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, colour);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	// 设置表头
	/**
	 * 表头单元格样式的设定
	 */
	private WritableCellFormat getHeaderCellStyle() throws Exception {

		/*
		 * WritableFont.createFont("宋体")：设置字体为宋体 10：设置字体大小
		 * WritableFont.BOLD:设置字体加粗（BOLD：加粗 NO_BOLD：不加粗） false：设置非斜体
		 * UnderlineStyle.NO_UNDERLINE：没有下划线
		 */
		WritableFont font = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE);
		// NumberFormats.TEXT
		WritableCellFormat headerFormat = new WritableCellFormat();

		// 添加字体设置
		headerFormat.setFont(font);
		// 设置单元格背景色：表头为黄色
		headerFormat.setBackground(Colour.YELLOW);
		// 设置表头表格边框样式
		// 整个表格线为粗线、黑色
		headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
		// 表头内容水平居中显示
		headerFormat.setAlignment(Alignment.CENTRE);
		headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

		return headerFormat;
	}

	/**
	 * 设置表格内容的样式
	 * 
	 * @return
	 * @throws Exception
	 */
	private WritableCellFormat getContentCellStyle() throws Exception {

		WritableFont font = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.NO_BOLD, false,
				UnderlineStyle.NO_UNDERLINE);

		WritableCellFormat headerFormat = new WritableCellFormat();
		// 添加字体设置
		headerFormat.setFont(font);
		// 设置表头表格边框样式
		// 整个表格线为粗线、黑色
		headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
		// 表头内容水平居中显示
		headerFormat.setAlignment(Alignment.CENTRE);
		headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

		return headerFormat;
	}

	/**
	 *表头标题的样式
	 */
	private WritableCellFormat getFirstSheetStyle() throws WriteException {
		WritableCellFormat headerFormat = new WritableCellFormat();
		WritableFont font = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.NO_BOLD, false,
				UnderlineStyle.NO_UNDERLINE);
		// 添加字体设置
		headerFormat.setFont(font);
		// 设置表头表格边框样式
		// 整个表格线为粗线、黑色
		headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
		// 表头内容水平居中显示
		headerFormat.setAlignment(Alignment.CENTRE);
		headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		return headerFormat;

	}

	/**
	 * 获得第一行的数据
	 * 
	 * @author huoyl
	 * @return
	 */
	private String getFirstRowContent() throws ParseException, RowsExceededException, WriteException {

		String endTime = processDetailBean.getEndTime();

		endTime = endTime.replaceFirst("-", JecnUtil.getValue("years"));
		endTime = endTime.replaceFirst("-", JecnUtil.getValue("month"));
		endTime = endTime + JecnUtil.getValue("day");
		String content = JecnUtil.getValue("endAs") + endTime
				+ getName(JecnUtil.getValue("processPubCount"), JecnUtil.getValue("rulePubCount"));
		return content;
	}

	/**
	 * 生成流程清单的图片
	 * 
	 * @author huoyl
	 * @param row
	 * @param col
	 * @param dataSheet
	 * @param imageHeight
	 * @param imageWidth
	 * @throws Exception
	 * @throws Exception
	 */
	private String createDetailImage(WritableSheet dataSheet, int imageWidth, int imageHeight) throws Exception {
		// processDetailList
		JFreeChart localJFreeChart = createChart();

		String imageAbsPath = jfreeChartToPng(localJFreeChart, imageWidth, imageHeight);

		return imageAbsPath;
	}

	/**
	 * 柱状图数值源
	 * 
	 * @return
	 * @throws Exception
	 */
	private CategoryDataset createDataset1() throws Exception {

		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();

		String pub = getName(JecnUtil.getValue("hasPubProcess"), JecnUtil.getValue("hasPubRule"));
		String all = getName(JecnUtil.getValue("pubAndNotPubProcess"), JecnUtil.getValue("pubAndNotPubRule"));
		List<ProcessDetailWebListBean> allDetailWebListBeans = processDetailBean.getAllDetailWebListBeans();
		for (ProcessDetailWebListBean processDetail : allDetailWebListBeans) {
			// 已发布
			localDefaultCategoryDataset.addValue(processDetail.getPubCount(), pub, processDetail.getSheetName());
			// 流程清单总数
			localDefaultCategoryDataset.addValue(processDetail.getAllCount(), all, processDetail.getSheetName());
		}
		String totalStr = JecnUtil.getValue("totalStr");
		// 已发布流程的总计
		localDefaultCategoryDataset.addValue(pubCountSum, pub, totalStr);
		// 所有流程的总计
		localDefaultCategoryDataset.addValue(allCountSum, all, totalStr);

		return localDefaultCategoryDataset;
	}

	/*
	 * 折线数值源
	 */
	private CategoryDataset createDataset2() {

		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();

		String processPubPercent = getName(JecnUtil.getValue("processPubPercent"), JecnUtil.getValue("rulePubPercent"));
		List<ProcessDetailWebListBean> allDetailWebListBeans = processDetailBean.getAllDetailWebListBeans();
		for (ProcessDetailWebListBean processDetail : allDetailWebListBeans) {

			// 处理float为百分比
			int percent = (int) (processDetail.getPubPercent() * 100);
			localDefaultCategoryDataset.addValue(percent, processPubPercent, processDetail.getSheetName());
		}
		if (0 == allCountSum) {
			localDefaultCategoryDataset.addValue(0, processPubPercent, JecnUtil.getValue("totalStr"));
		} else {
			localDefaultCategoryDataset.addValue(pubCountSum * 100 / allCountSum, processPubPercent, JecnUtil
					.getValue("totalStr"));
		}

		return localDefaultCategoryDataset;
	}

	private JFreeChart createChart() throws Exception {

		// 标题
		JFreeChart localJFreeChart = ChartFactory.createBarChart("", "", "", createDataset1(),
				PlotOrientation.VERTICAL, false, false, false);

		Font font = new Font("宋体", Font.BOLD, 18);
		// 图片标题
		localJFreeChart.setTitle(new TextTitle(getFirstRowContent(), font));

		CategoryPlot localCategoryPlot = (CategoryPlot) localJFreeChart.getPlot();

		CategoryDataset localCategoryDataset = createDataset2();
		localCategoryPlot.setDataset(1, localCategoryDataset);
		localCategoryPlot.mapDatasetToRangeAxis(1, 1);

		// 水平
		CategoryAxis localCategoryAxis = localCategoryPlot.getDomainAxis();
		localCategoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

		// 竖直 我是左侧的单位
		NumberAxis leftNumberAxis = new NumberAxis(JecnUtil.getValue("unitOne"));
		// 如果总计小于10
		int maxCount = allCountSum;
		if (maxCount < 10) {
			maxCount = 10;
		}
		// 设置左侧显示坐标的最大值
		leftNumberAxis.setUpperBound(maxCount + maxCount / 10);
		leftNumberAxis.setLabelFont(new Font("宋体", Font.BOLD, 12));

		// 设置左侧竖直刻度长度
		leftNumberAxis.setAutoTickUnitSelection(false);
		double leftUnit = maxCount / 10;// 刻度的长度
		NumberTickUnit leftNtu = new NumberTickUnit(leftUnit);
		leftNumberAxis.setTickUnit(leftNtu);

		localCategoryPlot.setRangeAxis(0, leftNumberAxis);

		// 竖直 我是%比 右侧单位
		NumberAxis localNumberAxis = new NumberAxis(JecnUtil.getValue("unitPercent"));
		// 设置百分百的最大值
		localNumberAxis.setUpperBound(110);

		localNumberAxis.setLabelFont(new Font("宋体", Font.BOLD, 12));

		localCategoryPlot.setRangeAxis(1, localNumberAxis);

		// 柱状图数字显示
		BarRenderer localBarRenderer = (BarRenderer) localCategoryPlot.getRenderer();
		// localBarRenderer.setBaseItemLabelFont(new Font("宋体", Font.BOLD, 12));
		// 柱状图标签距离柱状图的距离
		localBarRenderer.setItemLabelAnchorOffset(0.0D);
		// 标签是否显示
		localBarRenderer.setBaseItemLabelsVisible(true);
		localBarRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		localBarRenderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());

		// 柱状图的宽度
		localBarRenderer.setMaximumBarWidth(0.1);
		localBarRenderer.setMinimumBarLength(0.1);
		localBarRenderer.setItemMargin(0.1);
		// 折线图显示标签
		LineAndShapeRenderer localLineAndShapeRenderer = new LineAndShapeRenderer();
		localLineAndShapeRenderer.setBaseShapesVisible(true);
		localLineAndShapeRenderer.setBaseItemLabelsVisible(true);
		// 折线图的线是否显示
		localLineAndShapeRenderer.setSeriesLinesVisible(0, true);
		localLineAndShapeRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());

		localLineAndShapeRenderer.setItemLabelAnchorOffset(2.0D);

		// 柱状图的标签的单位
		DecimalFormat localDecimalFormat2 = new DecimalFormat("##.#'%'");
		localDecimalFormat2.setNegativePrefix("(");
		localDecimalFormat2.setNegativeSuffix(")");
		localLineAndShapeRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}",
				localDecimalFormat2));
		localLineAndShapeRenderer.setBaseItemLabelsVisible(true);

		localCategoryPlot.setRenderer(1, localLineAndShapeRenderer);
		// 折线是在前段还是在后端
		localCategoryPlot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		localCategoryAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 12));
		// 描述的显示
		// 折线的描述（就是描述各种类型的线或者图）
		LegendTitle legend = new LegendTitle(localCategoryPlot);

		legend.setPosition(RectangleEdge.LEFT);
		BlockContainer blockcontainer = new BlockContainer(new BorderArrangement());
		blockcontainer.add(legend, RectangleEdge.LEFT);// 这行代码可以控制位置
		CompositeTitle compositetitle = new CompositeTitle(blockcontainer);
		compositetitle.setPosition(RectangleEdge.RIGHT);// 放置图形代表区域位置的代码

		localJFreeChart.addSubtitle(compositetitle);

		return localJFreeChart;
	}

	private String jfreeChartToPng(JFreeChart localJFreeChart, int width, int height) {

		String path = JecnPath.APP_PATH;
		// 获取存储临时文件的目录
		String tempFileDir = path + "images/tempImage/" + "PROCESS_DETAIL/";
		// 创建临时文件目录
		File fileDir = new File(tempFileDir);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		String timefile = UUID.randomUUID().toString();
		String fileName = tempFileDir + timefile + ".png";
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileName);
			ChartUtilities.writeChartAsPNG(fos, localJFreeChart, width, height);
			fos.flush();
		} catch (Exception e) {
			log.error("ProcessDetailDownExcelService生成图片异常", e);
		} finally {

			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("ProcessDetailDownExcelService.java关闭图片流异常", e);
				}

			}

		}

		return fileName;
	}

}
