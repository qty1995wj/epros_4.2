
package com.jecn.webservice.zhongsh.rule.download;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="smisFileSystem" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="documentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fileType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="generatePdf" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="generateOrdinance" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="docSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "username",
    "password",
    "systemId",
    "smisFileSystem",
    "documentName",
    "documentSuffix",
    "fileType",
    "generatePdf",
    "generateOrdinance",
    "docSource"
})
@XmlRootElement(name = "UploadSystemToPdfQuick")
public class UploadSystemToPdfQuick {

    protected String username;
    protected String password;
    protected String systemId;
    protected byte[] smisFileSystem;
    protected String documentName;
    protected String documentSuffix;
    protected int fileType;
    protected boolean generatePdf;
    protected boolean generateOrdinance;
    protected String docSource;

    /**
     * 获取username属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置username属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * 获取password属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置password属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * 获取systemId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * 设置systemId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemId(String value) {
        this.systemId = value;
    }

    /**
     * 获取smisFileSystem属性的值。
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSmisFileSystem() {
        return smisFileSystem;
    }

    /**
     * 设置smisFileSystem属性的值。
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSmisFileSystem(byte[] value) {
        this.smisFileSystem = value;
    }

    /**
     * 获取documentName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * 设置documentName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentName(String value) {
        this.documentName = value;
    }

    /**
     * 获取documentSuffix属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentSuffix() {
        return documentSuffix;
    }

    /**
     * 设置documentSuffix属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentSuffix(String value) {
        this.documentSuffix = value;
    }

    /**
     * 获取fileType属性的值。
     * 
     */
    public int getFileType() {
        return fileType;
    }

    /**
     * 设置fileType属性的值。
     * 
     */
    public void setFileType(int value) {
        this.fileType = value;
    }

    /**
     * 获取generatePdf属性的值。
     * 
     */
    public boolean isGeneratePdf() {
        return generatePdf;
    }

    /**
     * 设置generatePdf属性的值。
     * 
     */
    public void setGeneratePdf(boolean value) {
        this.generatePdf = value;
    }

    /**
     * 获取generateOrdinance属性的值。
     * 
     */
    public boolean isGenerateOrdinance() {
        return generateOrdinance;
    }

    /**
     * 设置generateOrdinance属性的值。
     * 
     */
    public void setGenerateOrdinance(boolean value) {
        this.generateOrdinance = value;
    }

    /**
     * 获取docSource属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocSource() {
        return docSource;
    }

    /**
     * 设置docSource属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocSource(String value) {
        this.docSource = value;
    }

}
