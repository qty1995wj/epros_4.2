package epros.designer.gui.rule.mode;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class HighEfficiencyRuleModeMoveTree extends JecnHighEfficiencyTree {
	
	private Logger log = Logger.getLogger(HighEfficiencyRuleModeMoveTree.class);
	/** 当前项目根节点下的角色目录 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();


	public HighEfficiencyRuleModeMoveTree(List<Long> listIds){
		super(listIds);
		
	}
	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new RuleModeMoveTreeListener(getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.ruleModeRoot,JecnProperties.getValue("ruleMode"));//"制度模板"
		
		try {
			list=ConnectionPool.getRuleModeAction().getChildRuleModeDir(0L);
			JecnTreeCommon.addNLevelMoveNodes(list, rootNode,this.getListIds());
		} catch (Exception e) {
			log.error("HighEfficiencyRuleModeMoveTree getTreeModel is error",e);
		}
		return new JecnTreeModel(rootNode) ;
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
