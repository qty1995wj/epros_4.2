package com.jecn.epros.domain.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.lang3.AnnotationUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 任务主表-1
 */
public class TaskBean implements java.io.Serializable {
    /**
     * 主键ID
     */
    private Long taskId;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 0：拟稿人主导主导审批（默认） 1：文控审核人主导审批;
     */
    private Integer isControlauditLead;
    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date startTime;
    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date endTime;
    /**
     * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
     * 8:事业部经理 9：总经理 10:整理意见
     */
    private Integer taskState;


    /**
     * State对应的阶段名称 用于邮件发送 ：不存入数据库
     */
    private String stateMark;
    /**
     * 上一个状态 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
     * 8:事业部经理 9：总经理 10:整理意见
     */
    private Integer upState;
    /**
     * 子操作 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
     * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)） 10：交办人提交 11:编辑提交
     * 12:编辑打回 13:撤回 14:反回
     */
    private Integer taskElseState;
    /**
     * 任务说明
     */
    private String taskDesc;
    /**
     * 任务锁定：所有流程动作都不能做，只有解锁后才能操作 1：未锁定（默认） 0：锁定 也就是假删除状态
     */
    private Integer isLock;
    /**
     * 试运行周期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date testRunTime;
    /**
     * 版本号
     */
    private String historyVistion;
    /**
     * 0：试运行 1：是正式 2：是升级
     */
    private Integer flowType;
    /**
     * 间隔多少天发送试运行报告
     */
    private Integer sendRunTimeValue;
    /**
     * taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,3:制度文件ID，4：
     * 流程地图ID
     */
    private Long rid;
    /**
     * 文件名称 不存数据库
     */
    private String rname;
    /**
     * 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
     */
    private int taskType;
    /**
     * 创建人
     */
    private Long createPersonId;
    /**
     * 创建人 名称
     */
    private String createName;
    /**
     * 创建人名称 不存数据库
     */
    private String createPeopleTemporaryName;
    /**
     * 电话号码
     */
    private String phone;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date createTime;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateTime;
    /**
     * 源人
     */
    private Long fromPeopleId;
    /**
     * 源人 名称 不存数据库
     */
    private String fromPeopleTemporaryName;
    /**
     * 评审次数
     */
    private int revirewCounts;
    /**
     * 驳回次数
     */
    private int approveNoCounts;
    /**
     * 格式化后的结束时间:不存数据库
     */
    private String endTimeStr;
    /**
     * 格式化后开始时间
     */
    private String startTimeStr;
    /**
     * 责任人
     */
    private String resPeopleName;
    /**
     * 是否有撤回操作
     **/
    private Integer reJect;
    /**
     * 发布日期
     **/
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date publishTime;

    private Long historyId;

    /**
     * 审批类型 0审批 1借阅 2废止
     **/
    private Integer approveType;

    /**
     * 英文  State对应的阶段名称 用于邮件发送 ：不存入数据库
     */
    private String stateEnMark;

    /**
     * 自定义输入项1
     **/
    private String customInputItemOne;

    /**
     * 自定义输入项2
     **/
    private String customInputItemTwo;

    /**
     * 自定义输入项名称1
     **/
    private String customInputItemThree;


    public String getCustomInputItemThree() {
        return customInputItemThree;
    }

    public void setCustomInputItemThree(String customInputItemThree) {
        this.customInputItemThree = customInputItemThree;
    }

    public String getCustomInputItemOne() {
        return customInputItemOne;
    }

    public void setCustomInputItemOne(String customInputItemOne) {
        this.customInputItemOne = customInputItemOne;
    }

    public String getCustomInputItemTwo() {
        return customInputItemTwo;
    }

    public void setCustomInputItemTwo(String customInputItemTwo) {
        this.customInputItemTwo = customInputItemTwo;
    }



    public String getStateEnMark() {
        return stateEnMark;
    }

    public void setStateEnMark(String stateEnMark) {
        this.stateEnMark = stateEnMark;
    }

    public LinkResource getRelatedLink() {
        LinkResource link = new LinkResource(rid, "", null);
        switch (taskType) {
            case 0:
                link.setName(JecnProperties.getValue("processDetails"));//流程详情
                link.setType(ResourceType.PROCESS);
                link.setFrom(TreeNodeUtils.NodeType.task);
                break;
            case 4:
                link.setName(JecnProperties.getValue("processMapDetails"));//架构详情
                link.setType(ResourceType.PROCESS_MAP);
                link.setFrom(TreeNodeUtils.NodeType.task);
                break;
            case 1:
                link.setName(JecnProperties.getValue("fileDetails"));//文件详情
                link.setType(ResourceType.FILE);
                link.setFrom(TreeNodeUtils.NodeType.task);
                break;
            case 2:
            case 3:
                link.setName(JecnProperties.getValue("ruleDetails"));
                link.setType(ResourceType.RULE);
                link.setFrom(TreeNodeUtils.NodeType.task);
                break;
        }
        if (taskState == 5) {
            link.setIsPublic(1);
        } else {
            link.setIsPublic(0);
        }
        link.setHistoryId(historyId);
        link.setApproveType(approveType);
        return link;
    }

    public String getTaskDescWithBr() {
        return taskDesc;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Integer getReJect() {
        return reJect;
    }

    public void setReJect(Integer reJect) {
        this.reJect = reJect;
    }

    public String getResPeopleName() {
        return resPeopleName;
    }

    public void setResPeopleName(String resPeopleName) {
        this.resPeopleName = resPeopleName;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getIsControlauditLead() {
        return isControlauditLead;
    }

    public void setIsControlauditLead(Integer isControlauditLead) {
        this.isControlauditLead = isControlauditLead;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTaskState() {
        return taskState;
    }

    public void setTaskState(Integer taskState) {
        this.taskState = taskState;
    }

    public String getStateMark() {
        int type = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getLanguage();
        return type == 0 ? stateMark : stateEnMark;
    }

    public void setStateMark(String stateMark) {
        this.stateMark = stateMark;
    }

    public Integer getUpState() {
        return upState;
    }

    public void setUpState(Integer upState) {
        this.upState = upState;
    }

    public Integer getTaskElseState() {
        return taskElseState;
    }

    public void setTaskElseState(Integer taskElseState) {
        this.taskElseState = taskElseState;
    }

    public String getTaskDesc() {
        return Common.replaceWarp(taskDesc);
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public Integer getIsLock() {
        return isLock;
    }

    public void setIsLock(Integer isLock) {
        this.isLock = isLock;
    }

    public Date getTestRunTime() {
        return testRunTime;
    }

    public void setTestRunTime(Date testRunTime) {
        this.testRunTime = testRunTime;
    }

    public String getHistoryVistion() {
        return historyVistion;
    }

    public void setHistoryVistion(String historyVistion) {
        this.historyVistion = historyVistion;
    }

    public Integer getFlowType() {
        return flowType;
    }

    public void setFlowType(Integer flowType) {
        this.flowType = flowType;
    }

    public Integer getSendRunTimeValue() {
        return sendRunTimeValue;
    }

    public void setSendRunTimeValue(Integer sendRunTimeValue) {
        this.sendRunTimeValue = sendRunTimeValue;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public Long getCreatePersonId() {
        return createPersonId;
    }

    public void setCreatePersonId(Long createPersonId) {
        this.createPersonId = createPersonId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getFromPeopleId() {
        return fromPeopleId;
    }

    public void setFromPeopleId(Long fromPeopleId) {
        this.fromPeopleId = fromPeopleId;
    }

    public String getFromPeopleTemporaryName() {
        return fromPeopleTemporaryName;
    }

    public void setFromPeopleTemporaryName(String fromPeopleTemporaryName) {
        this.fromPeopleTemporaryName = fromPeopleTemporaryName;
    }

    public int getRevirewCounts() {
        return revirewCounts;
    }

    public void setRevirewCounts(int revirewCounts) {
        this.revirewCounts = revirewCounts;
    }

    public int getApproveNoCounts() {
        return approveNoCounts;
    }

    public void setApproveNoCounts(int approveNoCounts) {
        this.approveNoCounts = approveNoCounts;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreatePeopleTemporaryName() {
        return createPeopleTemporaryName;
    }

    public void setCreatePeopleTemporaryName(String createPeopleTemporaryName) {
        this.createPeopleTemporaryName = createPeopleTemporaryName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }


    public Integer getApproveType() {
        return approveType;
    }

    public void setApproveType(Integer approveType) {
        this.approveType = approveType;
    }
}
