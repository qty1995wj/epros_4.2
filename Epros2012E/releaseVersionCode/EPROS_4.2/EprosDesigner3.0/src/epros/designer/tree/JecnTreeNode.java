package epros.designer.tree;

import javax.swing.tree.DefaultMutableTreeNode;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class JecnTreeNode extends DefaultMutableTreeNode {

	private JecnTreeBean jecnTreeBean;
	private static final long serialVersionUID = 1L;
	
	private boolean selected =false;

	public JecnTreeNode(JecnTreeBean jecnTreeBean) {
		super(JecnTreeCommon.getShowName(jecnTreeBean));
		this.jecnTreeBean = jecnTreeBean;
	}	
	public JecnTreeBean getJecnTreeBean() {
		return jecnTreeBean;
	}

	public void setJecnTreeBean(JecnTreeBean jecnTreeBean) {
		this.jecnTreeBean = jecnTreeBean;
	}

	public boolean isLeaf() {
		return !jecnTreeBean.isChildNode();
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
		
		if (selected) {
//			// 如果选中，则将其所有的子结点都选中
//			if (children != null) {
//				for (Object obj : children) {
//					JecnTreeNode node = (JecnTreeNode) obj;
//					if (selected != node.isSelected())
//						node.setSelected(selected);
//				}
//			}
			// 向上检查，如果父结点中存在子结点被选中，那么将父结点也选中
			JecnTreeNode pNode = (JecnTreeNode) parent;
			// 开始检查pNode的所有子节点是否都被选中
			if (pNode != null) {
				int index = 0;
				for (; index < pNode.children.size(); ++index) {
//					JecnTreeNode pChildNode = (JecnTreeNode) pNode.children
//							.get(index);
//					//如果存在没选中的子节点，则不执行将父节点选中
//					if (!pChildNode.isSelected())
//						break;
				}
				/*
				 * 如果pNode子结点存在已经选中，则选中父结点， 该方法是一个递归方法，因此在此不需要进行迭代，因为
				 * 当选中父结点后，父结点本身会向上检查的。
				 */
				if (index == pNode.children.size()) {
					if (pNode.isSelected() != selected)
						//只要状态为删除时，子节点选中时，向上父节点也被选中
						if(pNode.getJecnTreeBean().getIsDelete() == 1){
							pNode.setSelected(selected);
						}
				}
			}
		} else {
			/*
			 * 如果是取消父结点则所有子结点取消
			 * 
			 */
			if (children != null) {
				int index = 0;
				for (; index < children.size(); ++index) {
//					JecnTreeNode childNode = (JecnTreeNode) children
//							.get(index);
//					//如果存在没选中的子节点，则不取消所有节点选中效果
//					if (!childNode.isSelected())
//						break;
				}
				// 从上向下取消的时候,取消所有子节点
				if (index == children.size()) {
					for (int i = 0; i < children.size(); ++i) {
						JecnTreeNode node = (JecnTreeNode) children
								.get(i);
						if (node.isSelected() != selected)
							node.setSelected(selected);
					}
				}
			}

//			// 向上取消，只要存在一个子节点不是选上的，那么父节点就不应该被选上。
//			JecnTreeNode pNode = (JecnTreeNode) parent;
//			if (pNode != null && pNode.isSelected() != selected)
//				pNode.setSelected(selected);
		}
	}
	
}
