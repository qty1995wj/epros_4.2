package com.jecn.epros.server.dao.dataImport.excelorDB;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.bean.dataimport.JecnBaseRelPosBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

/**
 * 标准版人员同步DAO
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Feb 26, 2013 时间：3:04:11 PM
 */
public interface IUserImportDao extends IBaseDao<DeptBean, Long> {
	/**
	 * 
	 * 获取通用部门临时表(TMP_JECN_IO_DEPT)项目ID集合
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Long> selectProjectId() throws Exception;

	/**
	 * 
	 * 清除临时库、入库临时库
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	void deleteAndInsertTmpJecnIOTable(BaseBean baseBean) throws Exception;

	/**
	 * 
	 * 通用部门临时表(TMP_JECN_IO_DEPT)：同一部门下部门名称不能重复
	 * 
	 * @return
	 * @throws Exception
	 */
	List<DeptBean> checkDeptNameSame() throws Exception;

	/**
	 * 
	 * 待导入部门、岗位以及人员中在各自真实库中存在的数据， 更新其操作标识设置为1
	 * 
	 * @throws Exception
	 */
	void UpdateProFlag(Long projectId, boolean isExistsDept) throws Exception;

	/**
	 * 
	 * 插入部门临时表中标识是插入的部门
	 * 
	 * @param s
	 * @throws Exception
	 */
	void insertDept(Long projectId) throws Exception;

	/**
	 * 
	 * 插入部门临时表中标识是插入的岗位
	 * 
	 * @param s
	 *            StatelessSession
	 * @throws Exception
	 */
	void insertPos(Long projectId) throws Exception;

	/**
	 * 
	 * 插入通用人员临时表中标识是插入的人员
	 * 
	 * @param s
	 *            StatelessSession
	 * @throws Exception
	 */
	void insertUser() throws Exception;

	/**
	 * 
	 * 更新岗位变更，查询需要删除的部门为A，删除A下的岗位数据以及更新或删除岗位相关联的表中字段值
	 * 
	 * @param s
	 */
	void updatePosUpAndDeletePos(Long projectId) throws Exception;

	/**
	 * 
	 * 删除部门以及相关
	 * 
	 */
	void deleteDept(Long projectId) throws Exception;

	/**
	 * 
	 * 删除岗位以及相关 除了人员与岗位关联关系
	 * 
	 */
	void deletePos(Long projectId) throws Exception;

	/**
	 * 
	 * 删除人员以及相关 除了人员与岗位关联关系
	 * 
	 */
	void deleteUser(Long projectId) throws Exception;

	/**
	 * 
	 * 删除人员与岗位关联关系以及相关表
	 * 
	 */
	void deleteUserPosRef(Long projectId) throws Exception;

	/**
	 * 
	 * 更新部门名称、PID
	 * 
	 */
	void updateNameAndPidByDept(Long projectId) throws Exception;

	/**
	 * 
	 * 更新岗位名称、所属部门ID
	 * 
	 */
	void updateNameAndDeptIdByPos(Long projectId) throws Exception;

	/**
	 * 
	 * 更新人员真实姓名、邮箱地址、联系电话、内外网邮件标识(内网：0（inner）,外网：1（outer）)
	 * 
	 */
	void updateAttributesByUser(Long projectId) throws Exception;

	/**
	 * 
	 * 插入通用人员临时表中标识是插入的人员
	 * 
	 * @param s
	 *            StatelessSession
	 * @throws Exception
	 */
	void insertUserPosRef(Long projectId) throws Exception;

	/**
	 * 
	 * 清除原始数据临时卡，并添加新数据
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	void deleteAndInsertTmpOriJecnIOTable(BaseBean baseBean) throws Exception;

	/**
	 * 原始数据部门集合
	 * 
	 * @return
	 */
	List<JecnTmpOriDept> findAllDeptOri() throws Exception;

	/**
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	List<JecnTmpPosAndUserBean> findAllUserAndPosOri() throws Exception;

	/**
	 * 更新流程中的角色的文本信息（岗位名称）
	 * 
	 * @author weidp
	 * @date： 日期：2014-4-28 时间：上午10:46:02
	 * @param roleMap
	 *            正式与临时表中，在人员同步之前角色名称与所关联的岗位/岗位组名称相同 的角色ID
	 * @param curProjectID
	 *            项目ID
	 */
	void updateFlowRoleFigureText(Map<String, List<Long>> roleMap, Long curProjectID) throws Exception;

	/**
	 * 将岗位信息插入到岗位基本信息表数据（实际上是向岗位基本信息表内插入没有岗位基本信息的岗位数据）
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-23 时间：下午05:10:37
	 * @param currProjectID
	 */
	void insertPositionFigInfo(Long currProjectID) throws Exception;

	/**
	 * 获取需要改变文本的角色图标
	 * 
	 * @author weidp
	 * @date 2014-9-26 上午10:34:18
	 * @return
	 */
	Map<String, List<Long>> getChangeRoles() throws Exception;

	/**
	 * 插入基准岗位
	 * 
	 * @author cheshaowei
	 * @throws Exception
	 * @date 15-03-16
	 * 
	 * */
	void insertBasePosInfo(JecnBasePosBean basePosBean) throws Exception;

	/**
	 * 获取所有的实际岗位信息（主键ID，岗位编号）
	 * 
	 * @author cheshaowei
	 * @date 15-03-17
	 * */
	List<JecnFlowOrgImage> getFlowOrgImageList() throws Exception;

	/**
	 * 查询所有的基准岗位信息
	 * 
	 * @return List<JecnBasePosBean> 查询到的所有基准岗位集合
	 * @author cheshaowei
	 * @date 15-03-19
	 * 
	 * **/
	List<JecnBasePosBean> getJecnBasePosBeanList() throws Exception;

	/**
	 * 根据基准岗位ID删除单个基准岗位
	 * 
	 * @param baseId
	 *            基准岗位主键ID
	 * @author cheshaowei
	 * 
	 * */

	void delBasePosInfo(String baseId) throws Exception;

	/**
	 * 删除基准岗位与岗位的关系
	 * 
	 * @author cheshaowei
	 * **/
	public void delBasePosGroup(String baseId) throws Exception;

	/**
	 * 添加岗位与岗位组的关系
	 * 
	 * @author cheshaowei
	 * 
	 * **/
	public void addGroupRelPos() throws Exception;

	/**
	 * 删除岗位与岗位组关系
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public void delGroupRelPos(String baseId) throws Exception;

	/**
	 * 删除岗位组与岗位关系
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public void delJecnBasePosGroupR(String baseId) throws Exception;

	/**
	 * 删除所有基准岗位
	 * 
	 * @author cheshaowei
	 * 
	 * 
	 * */
	void deleteBaseRelPosInfo() throws Exception;

	/**
	 * 插入基准岗位与岗位关系
	 * 
	 * @author cheshaowei
	 * 
	 * @param jecnBaseRelPosBean
	 *            基准岗位与岗位关系封装bean
	 * @throws Exception
	 * */
	void insertBaseRelPosInfo(JecnBaseRelPosBean jecnBaseRelPosBean) throws Exception;

	/**
	 * 根据基准岗位编号删除基准岗位与岗位的关系
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public void delBaseRelPosParmBaseNum(String baseNum) throws Exception;

	void insertPosGroup(Long projectId) throws Exception;

	void insertPosGroupRelatedPos() throws Exception;

	void deletePosChangeBasePos() throws Exception;

	List<Object[]> getSyncDeptNums(Long projectId) throws Exception;

	List<Object[]> getSyncUserNumsCount() throws Exception;

	List<Object[]> getSyncPosNumsCount(Long projectId) throws Exception;

	void updatePerOrgNum();

}
