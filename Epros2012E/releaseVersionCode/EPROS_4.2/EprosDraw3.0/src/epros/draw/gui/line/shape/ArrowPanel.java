package epros.draw.gui.line.shape;

/*
 * ArrowPanel.java
 *绘制箭头
 * Created on 2008年12月23日, 下午3:44
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.gui.graphics.JecnGraphics;

/**
 * 箭头
 * 
 * @author Administrator
 */
public class ArrowPanel extends JPanel {

	private Color color;
	private EditPointType pointType;

	/** 箭头高度 */
	public double arrowH = 8;
	/** 底边的一半 */
	public double arrowW = 10;

	public ArrowPanel(Color color, EditPointType pointType) {
		if (color == null || pointType == null) {
			throw new IllegalArgumentException("创建连接线箭头参数非法！");
		}
		this.pointType = pointType;
		this.color = color;
	}

	public void paintComponent(Graphics g) {
		g.setColor(color);
		// 绘制箭头
		JecnGraphics.paintTriangleShape((Graphics2D) g, pointType, this.getWidth(), this.getHeight());
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
