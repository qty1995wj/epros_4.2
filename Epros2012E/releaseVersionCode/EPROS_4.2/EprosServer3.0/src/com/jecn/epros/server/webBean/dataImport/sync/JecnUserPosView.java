package com.jecn.epros.server.webBean.dataImport.sync;

import java.io.Serializable;

public class JecnUserPosView implements Serializable {
	private String userNum;
	private String loginName;
	private String trueName;
	private String orgNum;
	private String posNum;
	private String posName;
	private String phoneStr;
	private String email;
	private String emailType;
	/** 0：添加，1：更新，2：删除 */
	private String status;
	private String domain_Name;

	public JecnUserPosView() {

	}

	public JecnUserPosView(String userNum, String loginName, String trueName, String orgNum, String posNum,
			String posName, String phoneStr, String email, String emailType, String domain_Name, String status) {
		this.userNum = userNum;
		this.loginName = loginName;
		this.trueName = trueName;
		this.orgNum = orgNum;
		this.posNum = posNum;
		this.posName = posName;
		this.phoneStr = phoneStr;
		this.email = email;
		this.status = status;
		this.emailType = emailType;
		this.domain_Name = domain_Name;
	}

	public JecnUserPosView(String loginName, String posNum, String posName, String status) {
		this.loginName = loginName;
		this.posNum = posNum;
		this.posName = posName;
		this.status = status;
	}

	public String getUserNum() {
		return userNum;
	}

	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getOrgNum() {
		return orgNum;
	}

	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getPhoneStr() {
		return phoneStr;
	}

	public void setPhoneStr(String phoneStr) {
		this.phoneStr = phoneStr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDomain_Name() {
		return domain_Name;
	}

	public void setDomain_Name(String domain_Name) {
		this.domain_Name = domain_Name;
	}

	public String toString() {
		return "loginName = " + loginName + " trueName = " + trueName + " posNum = " + posNum + " posName = " + posName
				+ " orgNum = " + orgNum;
	}

}
