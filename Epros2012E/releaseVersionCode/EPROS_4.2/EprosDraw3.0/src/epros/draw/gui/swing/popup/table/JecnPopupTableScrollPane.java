package epros.draw.gui.swing.popup.table;

import javax.swing.JScrollPane;

/**
 * 
 * 滚动面板
 * 
 * JecnPopupTable的容器
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPopupTableScrollPane extends JScrollPane {
	private JecnPopupTable popupTable = null;

	public JecnPopupTableScrollPane(JecnPopupTable popupTable) {
		this.popupTable = popupTable;
	}

	public JecnPopupTable getPopupTable() {
		return popupTable;
	}

}
