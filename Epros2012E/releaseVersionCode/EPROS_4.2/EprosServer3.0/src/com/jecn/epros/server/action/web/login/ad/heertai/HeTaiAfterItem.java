package com.jecn.epros.server.action.web.login.ad.heertai;

import java.util.ResourceBundle;

/**
 * 烽火通信
 * 
 */
public class HeTaiAfterItem {

	private static ResourceBundle heTConfig;

	static {
		heTConfig = ResourceBundle.getBundle("cfgFile/heertai/heTServerConfig");
	}

	public static String getValue(String key) {
		return heTConfig.getString(key);
	}
}
