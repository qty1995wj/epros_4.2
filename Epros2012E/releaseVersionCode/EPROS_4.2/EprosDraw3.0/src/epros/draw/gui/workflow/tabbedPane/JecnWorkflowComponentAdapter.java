package epros.draw.gui.workflow.tabbedPane;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JPanel;

/**
 * 
 * 流程元素面板中滚动面板监听事件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnWorkflowComponentAdapter extends ComponentAdapter {

	public JecnWorkflowComponentAdapter() {
		super();
	}

	/**
	 * Invoked when the component's size changes.
	 */
	public void componentResized(ComponentEvent e) {
		componentActionPerformed(e);
	}

	private void componentActionPerformed(ComponentEvent e) {
		if (e.getSource() instanceof JecnDrawDesktopPane) {// 画图面板
			// 画图面板
			JecnDrawDesktopPane workflow = (JecnDrawDesktopPane) e.getSource();

			// 画图面板宽
			int workflowWidth = workflow.getWidth();
			// 画图面板高
			int workflowHeight = workflow.getHeight();

			boolean isSizeSame = workflow.getFlowMapData().isSizeSame(
					workflowWidth, workflowHeight);

			if (!isSizeSame) {
				// 设置数据层大小
				workflow.getFlowMapData()
						.setSize(workflowWidth, workflowHeight);
			}

			// 绘制刻度
			workflow.getScrollPanle().getShapeScaleRowPanel().setCurrSize(
					workflowWidth, workflowHeight);
			workflow.getScrollPanle().getShapeScaleColumnPanel().setCurrSize(
					workflowWidth, workflowHeight);

			// 画图面板大小变化，角色移动面板也变化
			reSizeRoleMove(workflow);
		}
	}

	private void reSizeRoleMove(JecnDrawDesktopPane workflow) {
		if (workflow.getRoleMobile() != null) {
			// 画图面板大小
			Dimension size = workflow.getSize();
			// 角色移动面板
			JPanel roleMove = workflow.getRoleMobile().getRoleMove();

			if (workflow.getFlowMapData().isHFlag()) {// 横向
				roleMove.setSize(roleMove.getWidth(), size.height);
			} else {// 纵向
				roleMove.setSize(size.width, roleMove.getHeight());
			}
		}
	}
}
