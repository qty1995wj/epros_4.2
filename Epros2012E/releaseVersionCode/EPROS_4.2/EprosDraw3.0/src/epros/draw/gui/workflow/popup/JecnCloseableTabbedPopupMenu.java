package epros.draw.gui.workflow.popup;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnDrawScrollPane;
import epros.draw.gui.workflow.tabbedPane.JecnDrawTabbedPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitlePanel;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 面板标签处理类
 * 
 * @author FUZHENHUA
 * @date： 日期：Jul 30, 2012 时间：11:29:32 AM
 */
public class JecnCloseableTabbedPopupMenu extends JPopupMenu implements ActionListener {
	/** 关闭所有标签 */
	private JMenuItem closeAllMenu;
	/** 关闭其他标签 */
	private JMenuItem closeAllExceptThisMenu;

	public JecnCloseableTabbedPopupMenu() {
		// 关闭所有标签页
		closeAllMenu = new JMenuItem();
		closeAllMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("closeAllTab"));
		// 添加事件监听
		closeAllMenu.addActionListener(this);
		closeAllMenu.setName("closeAllMenu");
		// 关闭其他标签页
		closeAllExceptThisMenu = new JMenuItem();
		closeAllExceptThisMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("closeOthersTab"));
		// 添加事件监听
		closeAllExceptThisMenu.addActionListener(this);
		closeAllExceptThisMenu.setName("closeAllExceptThisMenu");

		this.add(closeAllMenu);
		this.add(closeAllExceptThisMenu);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			// 获取JMenuItem
			JMenuItem menuItem = (JMenuItem) e.getSource();
			String menuName = menuItem.getName();
			// 获取选项卡个数
			int tabCount = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
			if ("closeAllMenu".equals(menuName)) {
				// 删除所有话题面板
				removeAllPanel(true);
			} else if ("closeAllExceptThisMenu".equals(menuName)) {
				// 当前面板
				JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
				for (int i = 0; i < tabCount; i++) {
					Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
					if (component instanceof JecnTabbedTitlePanel) {
						JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
						JecnDrawDesktopPane drawDesktopPane = tabbedTitlePanel.getCurrDesktopPane();
						if (drawDesktopPane.equals(workflow)) {
							continue;
						} else {
							// 判断当前画图面板是否需要保存
							if (curIsSavePanel(i, true)) {
								return;
							}
							JecnDrawMainPanel.getMainPanel().getTabbedPane().removeTabAt(i);
							// 重置选项卡个数
							tabCount = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
							i--;
						}
					}
				}
			}
		}
	}

	/**
	 * 删除所有画图面板
	 * 
	 * @param flag
	 *            boolean 是-否-取消 true: OK-NO-CANCEL；false：OK-NO
	 * @return boolean true:关闭所有画图面板成功 false:失败或没有关闭
	 * 
	 */
	public static boolean removeAllPanel(boolean flag) {
		return removeAllPanelProcess(flag);
	}

	/**
	 * 删除所有画图面板
	 * 
	 * @param flag
	 *            boolean 是-否-取消 true: OK-NO-CANCEL；false：OK-NO
	 * @return boolean true:关闭所有画图面板成功 false:失败或没有关闭
	 * 
	 */
	private static boolean removeAllPanelProcess(boolean flag) {
		// 获取选项卡个数
		int tabCount = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();

		if (tabCount == 0) {
			return true;
		}

		for (int i = 0; i < tabCount; i++) {
			// 获取所有的画图面板
			if (curIsSavePanel(i, flag)) {
				return false;
			}
			JecnDrawMainPanel.getMainPanel().getTabbedPane().removeTabAt(i);
			// 重置选项卡个数
			tabCount = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
			i--;
		}
		if (tabCount == 0) {
			JecnDrawMainPanel.getMainPanel().getTabbedPane().removeAll();
			// 移除页面面板
			JecnDrawMainPanel.getMainPanel().getLeftPanel().remove(JecnDrawMainPanel.getMainPanel().getTabbedPane());
			// 没有新建或打开的流程图或流程地图
			JecnDrawMainPanel.getMainPanel().setWorkflow(null);
			JecnDrawMainPanel.getMainPanel().getLeftPanel().repaint();
			return true;
		}
		return false;
	}

	/**
	 * 当前画图面板是否需要保存处理
	 * 
	 * @param selectCount
	 *            tabbed面板选中的标签数组位置
	 * @param flag
	 *            boolean 是-否-取消 true: OK-NO-CANCEL；false：OK-NO
	 * 
	 */
	public static boolean curIsSavePanel(int selectCount, boolean flag) {
		// 主界面分页面板
		JecnDrawTabbedPane tabbedPane = JecnDrawMainPanel.getMainPanel().getTabbedPane();
		// 获取所有的画图面板
		Component component = tabbedPane.getComponentAt(selectCount);
		if (component instanceof JecnDrawScrollPane) {
			// 如果是滚动面板的实例
			JecnDrawScrollPane drawScrollPane = (JecnDrawScrollPane) component;
			// 获取画图面板
			JecnDrawDesktopPane desktopPane = drawScrollPane.getDesktopPane();
			if (desktopPane == null) {
				return false;
			} else {
				// 关闭面板是否弹出是否保存对话框
				// 是-否-取消 true: OK-NO-CANCEL；false：OK-NO
				return JecnWorkflowUtil.isSaveWorkFlow(desktopPane, flag);
			}
		}
		return false;
	}
}
