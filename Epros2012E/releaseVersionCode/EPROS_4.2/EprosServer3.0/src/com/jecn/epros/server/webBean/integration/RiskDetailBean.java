package com.jecn.epros.server.webBean.integration;

/**
 * 查询风险点详细信息
 * @author zhouhongfu
 *
 */
public class RiskDetailBean {
	
	/** 风险点ID */
	private Long riskId;
	/** 风险点编号 */
	private String riskCode;
	/** 风险点描述 */
	private String name;
	/** 风险点等级 */
	private Long grade;
	/** 风险点等级 */
	private String gradeStr;
	/** 内控指引知识库 */
	private String controlGuide;
	/** 风险点标准化控制 */
	private String standardControl;
	/** 控制目标编号ID */
	private String controlId;
	/** 控制目标  */
	private String controlDescription;
	/** 控制活动简描述 */
	private String activityShow;
	/** 活动名称 */
	private String figureText;
	/** 流程名称 */
	private String flowName;
	/** 责任部门 */
	private String orgName;
	/** 控制类型 0:预防性1:发现性 */
	private long type;
	private String strType;
	/** 控制方法0:人工1:自动 */
	private long method;
	private String strMethod;
	/** 是否是关键控制点0:是1:否 */
	private long keyPoint;
	private String strKeyPoint;
	/** 控制频率0:随时 1:日 2: 周 3:月 4:季度 5:年度 */
	private long frequency;
	private String strFrequency;
	/** 制度名称 */
	private String ruleName;
	/** 制度编号 */
	private String ruleNumber;
	/** 制度类别 */
	private String ruleType;
	
	public Long getRiskId() {
		return riskId;
	}
	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getGrade() {
		return grade;
	}
	public void setGrade(Long grade) {
		this.grade = grade;
	}
	public String getGradeStr() {
		return gradeStr;
	}
	public void setGradeStr(String gradeStr) {
		this.gradeStr = gradeStr;
	}
	public String getStandardControl() {
		return standardControl;
	}
	public void setStandardControl(String standardControl) {
		this.standardControl = standardControl;
	}
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public String getControlDescription() {
		return controlDescription;
	}
	public void setControlDescription(String controlDescription) {
		this.controlDescription = controlDescription;
	}
	public String getControlGuide() {
		return controlGuide;
	}
	public void setControlGuide(String controlGuide) {
		this.controlGuide = controlGuide;
	}
	public String getActivityShow() {
		return activityShow;
	}
	public void setActivityShow(String activityShow) {
		this.activityShow = activityShow;
	}
	public String getFigureText() {
		return figureText;
	}
	public void setFigureText(String figureText) {
		this.figureText = figureText;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getType() {
		return type;
	}
	public void setType(long type) {
		this.type = type;
	}
	public long getMethod() {
		return method;
	}
	public void setMethod(long method) {
		this.method = method;
	}
	public long getKeyPoint() {
		return keyPoint;
	}
	public void setKeyPoint(long keyPoint) {
		this.keyPoint = keyPoint;
	}
	public long getFrequency() {
		return frequency;
	}
	public void setFrequency(long frequency) {
		this.frequency = frequency;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getRuleNumber() {
		return ruleNumber;
	}
	public void setRuleNumber(String ruleNumber) {
		this.ruleNumber = ruleNumber;
	}
	public String getRuleType() {
		return ruleType;
	}
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	public String getStrType() {
		return strType;
	}
	public void setStrType(String strType) {
		this.strType = strType;
	}
	public String getStrMethod() {
		return strMethod;
	}
	public void setStrMethod(String strMethod) {
		this.strMethod = strMethod;
	}
	public String getStrKeyPoint() {
		return strKeyPoint;
	}
	public void setStrKeyPoint(String strKeyPoint) {
		this.strKeyPoint = strKeyPoint;
	}
	public String getStrFrequency() {
		return strFrequency;
	}
	public void setStrFrequency(String strFrequency) {
		this.strFrequency = strFrequency;
	}
	
}
