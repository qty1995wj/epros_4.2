package epros.designer.service.process;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUserCheckUtil;

public class JecnSelectProcessDownload {
	private static Logger log = Logger.getLogger(JecnSelectProcessDownload.class);

	private static String filesName = "process files";

	/**
	 * 下载单个流程文件
	 * 
	 * @author fuzhh 2013-9-4
	 * @param isDownFile
	 * @param selectNode
	 */
	private void downloadOneProcess(boolean isDownFile, JecnTreeNode selectNode) throws Exception {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		try {
			// 获取默认路径
			String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFlowDirectory();
			// 获取截取后正确的路径
			if (pathUrl != null && !"".equals(pathUrl)) {
				pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
			}
			JecnFileChooser fileChooser = new JecnFileChooser(pathUrl);
			// 移除系统给定的文件过滤器
			fileChooser.setAcceptAllFileFilterUsed(false);
			// 过滤文件
			fileChooser.setFileFilter(new ImportFlowFilter("doc"));
			JecnCreateDoc jecnCreateDoc = null;
			// 文件名称获取当前面板名称
			fileChooser.setSelectedFile(new File(selectNode.getJecnTreeBean().getName()));
			int i = fileChooser.showSaveDialog(null);
			// 选择确认（yes、ok）后返回该值。
			if (i == JFileChooser.APPROVE_OPTION) {
				fileChooser.getTextFiled().getText();
				// 获取输出文件的路径
				String fileDirectory = fileChooser.getSelectedFile().getPath();
				String pathName = fileChooser.getSelectedFile().getName();
				String error = JecnUserCheckUtil.checkFileName(pathName);
				// 设置上传访问路径
				// 修改配置文件中的路径
				JecnSystemData.readCurrSystemFileIOData().setSaveFlowDirectory(fileDirectory);
				JecnSystemData.writeCurrSystemFileIOData();
				if (!"".equals(error)) {
					JecnOptionPane.showMessageDialog(null, error, "", JecnOptionPane.ERROR_MESSAGE);
					return;
				}
				// String fileName = selectNode.getJecnTreeBean().getName();
				//			
				// // JecnFileChooser fileChooser = new JecnFileChooser();
				// //保存路径
				// String fileDirectory = JecnFileChooser.setDefaultPath("path",
				// "doc",fileName);
				// if (selectNode.getJecnTreeBean().getTreeNodeType() ==
				// TreeNodeType.process) {
				// // 设置标题：下载流程图操作说明
				// fileChooser.setDialogTitle(JecnProperties
				// .getValue("downloadFlowInstruts"));
				// } else if (selectNode.getJecnTreeBean().getTreeNodeType() ==
				// TreeNodeType.processMap) {
				// // 设置标题“：下载流程地图操作说明
				// fileChooser.setDialogTitle(JecnProperties
				// .getValue("downloadFlowMapInstruts"));
				// }
				//			
				// JecnCreateDoc jecnCreateDoc = null;

				String xmlPath = fileDirectory;
				if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process) {
					// 流程ID
					long flowId = selectNode.getJecnTreeBean().getId();
					// 流程名称
					String flowName = selectNode.getJecnTreeBean().getName();
					// 写入流程文件
					processFiles(fileDirectory, flowId, flowName, isDownFile, false);

				} else if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap) {
					// 判断文件后缀名是否为epros结尾
					if (!fileDirectory.endsWith(".doc")) {
						xmlPath = fileDirectory + ".doc";
					}
					if (xmlPath != null) {
						// 判断是否重复
						boolean isFlag = isFileName(xmlPath);
						if (!isFlag) {
							return;
						}
					}
					jecnCreateDoc = ConnectionPool.getProcessAction().getFlowFile(selectNode.getJecnTreeBean().getId(),
							TreeNodeType.processMap, false);
					if (jecnCreateDoc == null) {
						return;
					}
					try {
						fos = new FileOutputStream(xmlPath);
					} catch (FileNotFoundException e) {
						log.error("JecnSelectProcessDownload downloadOneProcess is error", e);
						if (e.getLocalizedMessage().contains("ShellFolder")) {
							JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
									.getValue("saveFilePathError"));
						} else {
							JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), e.getLocalizedMessage());
						}
						return;
					}
					bos = new BufferedOutputStream(fos);
					bos.write(jecnCreateDoc.getBytes());
				}
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("downloadTheAbnormalProcessInstructions"));
			log.error("JecnSelectProcessDownload downloadOneProcess is error ！！！", e);
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					log.error("JecnSelectProcessDownload downloadOneProcess is error！", e);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("JecnSelectProcessDownload downloadOneProcess is error！", e);
				}
			}
		}
	}

	/**
	 * 下载选中的流程文件
	 * 
	 * @author fuzhh 2013-9-4
	 * @param isDownFile
	 * @param selectNodes
	 */
	private void downloadSelectProcess(boolean isDownFile, List<JecnTreeNode> selectNodes) throws Exception {
		// 是否有权限执行
		if (!JecnTreeCommon.isAuthNodes(selectNodes)) {
			return;
		}
		if (selectNodes.size() > 20) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("aMaximumOf20ProcessChoice"));
			return;
		}
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFlowDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}
		JecnFileChooser fileChooser = new JecnFileChooser(pathUrl);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		// 设置标题：下载流程图操作说明
		fileChooser.setDialogTitle(JecnProperties.getValue("downloadFlowInstruts"));
		// 文件名称获取当前面板名称
		// 文件过滤类型只显示文件夹
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setSelectedFile(new File(filesName));
		int i = fileChooser.showSaveDialog(null);
		// 选择确认（yes、ok）后返回该值。
		if (i == JFileChooser.APPROVE_OPTION) {
			// 获取输出文件的路径
			String path = fileChooser.getSelectedFile().getPath();
			path = path + File.separator + filesName + File.separator;
			String pathName = fileChooser.getSelectedFile().getName();
			String error = JecnUserCheckUtil.checkFileName(pathName);
			// 修改配置文件中的路径
			String writePath = path.substring(0, path.lastIndexOf("\\"));
			writePath = path.substring(0, path.lastIndexOf("\\"));
			JecnSystemData.readCurrSystemFileIOData().setSaveFlowDirectory(writePath);
			JecnSystemData.writeCurrSystemFileIOData();
			if (!"".equals(error)) {
				JecnOptionPane.showMessageDialog(null, error, "", JecnOptionPane.ERROR_MESSAGE);
				return;
			}
			if (path != null) {
				// 删除文件
				// deleteDirectory(path);
				// 流程操作说明输出流
				try {
					for (JecnTreeNode node : selectNodes) {
						// 流程ID
						long flowId = node.getJecnTreeBean().getId();
						// 流程名称
						String flowName = node.getJecnTreeBean().getName();
						if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process) {
							getFilePath(path);
							String filePath = path + flowName;
							// 写入流程文件
							processFiles(filePath, flowId, flowName, isDownFile, true);
						} else if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap) {
							// 写入流程地图文件
							processMapFiles(path, flowId);
						}
					}
				} catch (Exception e) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("downloadTheAbnormalProcessInstructions"));
					log.error("JecnSelectProcessDownload downloadSelectProcess is error!", e);
				}
			}
		}
	}

	/**
	 * 获取单个流程地图数据(批量调用)
	 * 
	 * @param path
	 *            文件目录
	 * @param flowId
	 *            流程地图ID
	 */
	private static void processMapFiles(String path, Long flowId) {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		JecnCreateDoc jecnCreateDoc = null;
		// 判断目录是否存在，不存在则创建
		getFilePath(path);
		try {
			jecnCreateDoc = ConnectionPool.getProcessAction().getFlowFile(flowId, TreeNodeType.processMap, false);
		} catch (Exception e) {
			log.error("JecnSelectProcessDownload processMapFiles is error", e);
			return;
		}
		if (jecnCreateDoc == null) {
			return;
		}
		String filePath = path + "\\"
				+ JecnTreeCommon.getShowName(jecnCreateDoc.getFileName(), jecnCreateDoc.getIdInput());
		if (!filePath.endsWith(".doc")) {
			filePath = filePath + ".doc";
		}
		if (filePath != null) {
			// 判断是否重复
			boolean isFlag = isFileName(filePath);
			if (!isFlag) {
				return;
			}
		}
		try {
			fos = new FileOutputStream(filePath);
			bos = new BufferedOutputStream(fos);
			bos.write(jecnCreateDoc.getBytes());
		} catch (FileNotFoundException e) {
			log.error("JecnSelectProcessDownload processMapFiles is error", e);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("saveFilePathError"));
			return;
		} catch (IOException e) {
			log.error("JecnSelectProcessDownload processMapFiles is error！", e);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("downloadTheAbnormalProcessInstructions"));
			return;
		} catch (Exception e) {
			log.error("JecnSelectProcessDownload processMapFiles is error！", e);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("downloadTheAbnormalProcessInstructions"));
			return;
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					log.error("JecnSelectProcessDownload processMapFiles is error！", e);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("JecnSelectProcessDownload processMapFiles is error！", e);
				}
			}
		}
	}

	/**
	 * 获取单个流程数据
	 * 
	 * @author fuzhh 2013-9-5
	 * @param flowId
	 * @param isDownFile
	 *            true:带附件下载（流程中相关连文件）
	 * @throws Exception
	 */
	private static void processFiles(String path, Long flowId, String flowName, boolean isDownFile, boolean isFile) {
		FileOutputStream docFos = null;
		// 获取操作说明字节流，和附件文件
		try {
			long peopleId = JecnConstants.loginBean.getJecnUser().getPeopleId();
			boolean isAdmin = JecnConstants.loginBean.isAdmin();
			long projectId = JecnConstants.projectId;
			// 记录附件下载标识
			boolean extFile = false;
			if (isDownFile) {
				Map<EXCESS, List<FileOpenBean>> downloadMap = ConnectionPool.getProcessAction().getProcessExcessFile(
						flowId, false, peopleId, projectId, isAdmin);
				for (EXCESS key : downloadMap.keySet()) {
					String tempPath = path + File.separator + key.getValue();
					List<FileOpenBean> fileList = downloadMap.get(key);
					if (fileList == null || fileList.size() == 0) {
						continue;
					}
					if (!extFile) {
						extFile = true;
					}
					// 判断目录是否存在
					getFilePath(tempPath);
					// 存储文件名称集合
					Map<String, Integer> fileMap = new HashMap<String, Integer>();
					// 获取文件信息
					for (FileOpenBean fileOpenBean : fileList) {
						// 获取文件路径
						String filePaths = tempPath + File.separator + fileOpenBean.getName();
						// 文件输出流
						FileOutputStream fileFos = null;
						try {
							// 判断是否重名
							File isExists = new File(filePaths);
							String fileName = isExists.getName();
							if (isExists.exists()) {
								if (fileMap.containsKey(fileName)) {
									int fileVal = fileMap.get(fileName);
									StringBuffer fileStr = new StringBuffer(fileName);
									fileStr.insert(fileName.lastIndexOf("."), "(" + (fileVal + 1) + ")");
									fileMap.put(fileName, fileVal + 1);
									filePaths = tempPath + File.separator + fileStr.toString();
								} else {
									fileMap.put(fileName, 1);
								}
							} else {
								fileMap.put(fileName, 1);
							}

							fileFos = new FileOutputStream(filePaths);
							fileFos.write(fileOpenBean.getFileByte());
						} catch (Exception e1) {
							log.error("JecnSelectProcessDownload processFiles is error ！", e1);
							// return;
						} finally {
							if (fileFos != null) {
								try {
									fileFos.close();
								} catch (Exception e2) {
									log.error("JecnSelectProcessDownload processFiles is error ！", e2);
								}
							}
						}
					}
				}

			}
			// 获取操作说明数据
			byte[] bytes = ConnectionPool.getProcessAction()
					.getProcessFile(flowId, false, peopleId, projectId, isAdmin);

			String filePath = path;
			if (extFile) {
				filePath = filePath + File.separator + flowName;
			}
			filePath = filePath + ".doc";
			// 判断是否重复
			boolean isFlag = isFileName(filePath);
			if (!isFlag) {
				return;
			}
			// 获取文件输出流
			docFos = new FileOutputStream(filePath);
			// 写入字符流
			docFos.write(bytes);
		} catch (FileNotFoundException e) {
			if (e.getLocalizedMessage().contains("ShellFolder")) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("saveFilePathError"));
			} else {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), e.getLocalizedMessage());
			}
			log.error("JecnSelectProcessDownload processFiles is error ！", e);
			return;
		} catch (IOException e) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("downloadTheAbnormalProcessInstructions"));
			log.error("JecnSelectProcessDownload processFiles is error ！", e);
			return;
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("downloadTheAbnormalProcessInstructions"));
			log.error("JecnSelectProcessDownload processFiles is error ！", e);
			return;
		} finally {
			if (docFos != null) {
				try {
					docFos.close();
				} catch (IOException e) {
					log.error("JecnSelectProcessDownload processFiles is error ！", e);
				}
			}
		}
	}

	/**
	 * 判断文件目录是存在不存在创建
	 * 
	 * @author fuzhh 2013-9-5
	 * @param path
	 */
	private static void getFilePath(String path) {
		// 判断文件目录是否存在不存在创建
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	/**
	 * 判断文件是否重名，重名提示是否覆盖
	 * 
	 * @author fuzhh 2013-9-5
	 * @param path
	 * @return
	 */
	private static boolean isFileName(String path) {
		// 判断是否重名
		File isExists = new File(path);
		if (isExists.exists()) {
			// 0 为是 1为否
			int selectYorN = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("fileName")
					+ path + JecnProperties.getValue("isExistOverwrite"), "", JecnOptionPane.YES_NO_OPTION);
			if (selectYorN == 1) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 删除目录（文件夹）以及目录下的文件
	 * 
	 * @author fuzhh 2013-9-2
	 * @param sPath
	 *            被删除目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static boolean deleteDirectory(String sPath) {
		// 如果sPath不以文件分隔符结尾，自动添加文件分隔符
		if (!sPath.endsWith(File.separator)) {
			sPath = sPath + File.separator;
		}
		File dirFile = new File(sPath);
		// 如果dir对应的文件不存在，或者不是一个目录，则退出
		if (!dirFile.exists() || !dirFile.isDirectory()) {
			return false;
		}
		boolean flag = true;
		// 删除文件夹下的所有文件(包括子目录)
		File[] files = dirFile.listFiles();
		for (int i = 0; i < files.length; i++) {
			// 删除子文件
			if (files[i].isFile()) {
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag)
					break;
			} // 删除子目录
			else {
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag)
					break;
			}
		}
		if (!flag)
			return false;
		// 删除当前目录
		if (dirFile.delete()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 删除单个文件
	 * 
	 * @author fuzhh 2013-9-2
	 * @param sPath
	 *            被删除文件的文件名
	 * @return 单个文件删除成功返回true，否则返回false
	 */
	public static boolean deleteFile(String sPath) {
		boolean flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	/**
	 * 下载操作说明
	 * 
	 * @author fuzhh 2013-9-3
	 * @param isFile
	 *            是否带附件
	 */
	public String downloadProcess(boolean isDownFile, JecnTreeNode selectNode, List<JecnTreeNode> selectNodes) {
		String isQuery = "true";
		if (selectNodes != null && selectNodes.size() > 1) {
			// 下载选中的文件
			try {
				downloadSelectProcess(isDownFile, selectNodes);
			} catch (Exception e) {
				isQuery = "false";
			}
		}
		if (selectNode != null) {
			// 下载单个文件
			try {
				downloadOneProcess(isDownFile, selectNode);
			} catch (Exception e) {
				log.error("", e);
				isQuery = "false";
			}
		}
		return isQuery;
	}

	/**
	 * 导出操作说明
	 * 
	 * @author fuzhh 2013-9-4
	 * @return
	 */
	public static boolean processDownload(boolean isDownFile, JecnTreeNode selectNode, List<JecnTreeNode> selectNodes) {
		if (JecnConfigTool.isShowHeadInfo()) {
			boolean flag = isEmptyHeadData(selectNodes);
			if (flag) {
				return false;
			}
		}
		String isQuery = null;
		ProcessDownloadWorker processDownloadWorker = new ProcessDownloadWorker();
		processDownloadWorker.setDownFile(isDownFile);
		processDownloadWorker.setSelectNode(selectNode);
		processDownloadWorker.setSelectNodes(selectNodes);

		String error = JecnLoading.start(processDownloadWorker);

		if (error != null) {
			return false;
		}
		try {
			isQuery = processDownloadWorker.get();
			if ("false".equals(isQuery)) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("downloadTheAbnormalProcessInstructions"));
				return false;
			}
		} catch (Exception e) {
			log.error("JecnSelectProcessDownload processDownload is error ！", e);
			return false;
		}
		return true;
	}

	/**
	 *判断 有没有设置操作说明头信息
	 * 
	 * @return
	 */
	private static boolean isEmptyHeadData(List<JecnTreeNode> selectNodes) {
		if (selectNodes == null || selectNodes.isEmpty()) {
			return true;
		}
		for (JecnTreeNode selectNode : selectNodes) {
			String[] parentStr = selectNode.getJecnTreeBean().getT_Path().split("-");// 根据选中流程节点获取父级的ID
			if (parentStr.length > 1) {
				List<FlowFileHeadData> flowFileHeadData = ConnectionPool.getProcessAction()
						.findFlowFileHeadInfoVerification(selectNode.getJecnTreeBean().getId());
				if (flowFileHeadData.isEmpty() || flowFileHeadData.get(0) == null) {
					if (JecnDesignerCommon.isAdmin() || JecnDesignerCommon.isSecondAdmin() || JecnDesignerCommon
									.isProcessAdmin()) {
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("noOperation"));
						return true;
					} else {
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("noOperationInstructions"));
						return true;
					}

				}
			}
		}
		return false;
	}

}

/**
 * 下载操作说明等待框
 * 
 * @author fuzhh 2013-9-4
 * 
 */
class ProcessDownloadWorker extends SwingWorker<String, Void> {
	private boolean isDownFile;
	private List<JecnTreeNode> selectNodes;
	private JecnTreeNode selectNode;

	@Override
	protected String doInBackground() throws Exception {
		String isQuery = new JecnSelectProcessDownload().downloadProcess(isDownFile, selectNode, selectNodes);
		return isQuery;
	}

	@Override
	public void done() {
		JecnLoading.stop();
	}

	public void setDownFile(boolean isDownFile) {
		this.isDownFile = isDownFile;
	}

	public void setSelectNodes(List<JecnTreeNode> selectNodes) {
		this.selectNodes = selectNodes;
	}

	public void setSelectNode(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
	}

}
