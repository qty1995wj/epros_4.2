package com.jecn.epros.domain.rule;

import java.util.List;

public class RuleViewBean {
    private int total;
    private List<SearchRuleResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SearchRuleResultBean> getData() {
        return data;
    }

    public void setData(List<SearchRuleResultBean> data) {
        this.data = data;
    }

}
