package com.jecn.epros.server.dao.log;

import com.jecn.epros.server.bean.log.FileDownloadLog;
import com.jecn.epros.server.common.IBaseDao;

public interface IFileDownloadLogDao extends IBaseDao<FileDownloadLog, Long> {

}
