package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.util.ConfigUtils;

import java.util.Date;

public class FlowStructureT {

    private Long projectId;// 项目ID
    private Long flowId;// 主键ID（流程ID）
    private String flowName;// 流程名称
    private Long isFlow;// 0是流程地图，1是流程
    private Long perFlowId;// 父ID
    private String strWidth;// 画图面板的宽
    private String strHeight;// 画图面板的高
    private String flowIdInput;// 流程编号
    private Long isPublic;// 0是密码，1是公开
    private Long sortId;// 排序
    private Long delState;// 1是删除，0是没有删除
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date createDate;// 创建时间
    private Long peopleId;// 创建人peopleId
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateDate;// 更新时间
    private Long updatePeopleId;// 更新人ID
    private String filePath;// 文件路径
    private Long historyId;// 文控ID
    private int errorState;// 0:错误 1:正确
    private int dataType;// 0:流程 1：模板
    private Long occupier;// 占用人
    private int pagings;// 分页数
    private int xpoint;// 最后一条线的坐标
    private int imagvh;// 分页横纵向显示
    private String tPath;//tpath 节点层级关系
    private int tLevel;// level 节点级别，默认0开始
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date pubTime;

    /**
     * 0:流程/架构图； 1：流程（文件）
     */
    private int nodeType;
    /**
     * 流程（文件）-关联的内容ID
     */
    private Long fileContentId;
    /**
     * 节点显示的顺序
     */
    private String viewSort;
    /**
     * 保密级别
     **/
    private Integer confidentialityLevel;


    private Long commissionerId;//专员

    private String keyWord;//专员


    private String bussType;//业务类型

    public String getBussType() {
        return bussType;
    }

    public void setBussType(String bussType) {
        this.bussType = bussType;
    }

    public String getConfidentialityLevelStr(){
        return ConfigUtils.getConfidentialityLevel(confidentialityLevel);
    }

    public Integer getConfidentialityLevel() {
        return confidentialityLevel;
    }

    public void setConfidentialityLevel(Integer confidentialityLevel) {
        this.confidentialityLevel = confidentialityLevel;
    }

    public Long getCommissionerId() {
        return commissionerId;
    }

    public void setCommissionerId(Long commissionerId) {
        this.commissionerId = commissionerId;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getIsFlow() {
        return isFlow;
    }

    public void setIsFlow(Long isFlow) {
        this.isFlow = isFlow;
    }

    public Long getPerFlowId() {
        return perFlowId;
    }

    public void setPerFlowId(Long perFlowId) {
        this.perFlowId = perFlowId;
    }

    public String getStrWidth() {
        return strWidth;
    }

    public void setStrWidth(String strWidth) {
        this.strWidth = strWidth;
    }

    public String getStrHeight() {
        return strHeight;
    }

    public void setStrHeight(String strHeight) {
        this.strHeight = strHeight;
    }

    public String getFlowIdInput() {
        return flowIdInput;
    }

    public void setFlowIdInput(String flowIdInput) {
        this.flowIdInput = flowIdInput;
    }

    public Long getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Long isPublic) {
        this.isPublic = isPublic;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public Long getDelState() {
        return delState;
    }

    public void setDelState(Long delState) {
        this.delState = delState;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Long peopleId) {
        this.peopleId = peopleId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdatePeopleId() {
        return updatePeopleId;
    }

    public void setUpdatePeopleId(Long updatePeopleId) {
        this.updatePeopleId = updatePeopleId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public int getErrorState() {
        return errorState;
    }

    public void setErrorState(int errorState) {
        this.errorState = errorState;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public Long getOccupier() {
        return occupier;
    }

    public void setOccupier(Long occupier) {
        this.occupier = occupier;
    }

    public int getPagings() {
        return pagings;
    }

    public void setPagings(int pagings) {
        this.pagings = pagings;
    }

    public int getXpoint() {
        return xpoint;
    }

    public void setXpoint(int xpoint) {
        this.xpoint = xpoint;
    }

    public int getImagvh() {
        return imagvh;
    }

    public void setImagvh(int imagvh) {
        this.imagvh = imagvh;
    }

    public String gettPath() {
        return tPath;
    }

    public void settPath(String tPath) {
        this.tPath = tPath;
    }

    public int gettLevel() {
        return tLevel;
    }

    public void settLevel(int tLevel) {
        this.tLevel = tLevel;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    public int getNodeType() {
        return nodeType;
    }

    public void setNodeType(int nodeType) {
        this.nodeType = nodeType;
    }

    public Long getFileContentId() {
        return fileContentId;
    }

    public void setFileContentId(Long fileContentId) {
        this.fileContentId = fileContentId;
    }

    public String getViewSort() {
        return viewSort;
    }

    public void setViewSort(String viewSort) {
        this.viewSort = viewSort;
    }
}
