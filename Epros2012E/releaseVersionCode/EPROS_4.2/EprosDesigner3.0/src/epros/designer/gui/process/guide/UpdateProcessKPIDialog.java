package epros.designer.gui.process.guide;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/*******************************************************************************
 * 编辑流程KPI
 * 
 * @author 2012-07-12
 * 
 */
public class UpdateProcessKPIDialog extends EditProcessKPIDialog {
	private static Logger log = Logger.getLogger(UpdateProcessKPIDialog.class);
	private JecnFlowKpiNameT flowKpiNameT = null;
	public boolean isOperation = false;
	private Date creatime = null;
	private Long createPeopleId = null;
	// 流程ID
	private Long flowId = null;
	/** 选中的KPI表格中的kpi名称 **/
	private String selecKPIName = null;
	/** 面板可输入控件 是否修改标识 返回true表示修改，false表示未修改 */
	private boolean isUpdate = false;

	private List<String> targetTypeList;
	private KPIShowDialog showDialog;

	public UpdateProcessKPIDialog(JecnFlowKpiNameT flowKpiNameT, Long flowId, KPIShowDialog showDialog) {
		this.flowId = flowId;
		// 获取要编辑的KPI数据
		this.flowKpiNameT = flowKpiNameT;
		this.showDialog = showDialog;
		selecKPIName = flowKpiNameT.getKpiName();
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				if (check()) {// true：需要执行关闭 false：不要执行关闭
					return true;
				}
				return false;
			}
		});
		// 将KPI属性显示到编辑面板上
		initLayout(flowKpiNameT);
		// 数据提供者框监听
		this.dataProviderListener();
	}

	private boolean check() {
		// kpi名称
		if (flowKpiNameT.getKpiName() == null || "".equals(flowKpiNameT.getKpiName())) {
			flowKpiNameT.setKpiName("");
			if (!flowKpiNameT.getKpiName().equals(this.kpiNameField.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiName().equals(this.kpiNameField.getText())) {
				isUpdate = true;
			}
		}
		// kpi定义
		if (flowKpiNameT.getKpiDefinition() == null || "".equals(flowKpiNameT.getKpiDefinition())) {
			flowKpiNameT.setKpiDefinition("");
			if (!flowKpiNameT.getKpiDefinition().equals(this.kpiDefinedArea.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiDefinition().equals(this.kpiDefinedArea.getText())) {
				isUpdate = true;
			}
		}
		// kpi统计方法
		if (flowKpiNameT.getKpiStatisticalMethods() == null || "".equals(flowKpiNameT.getKpiStatisticalMethods())) {
			flowKpiNameT.setKpiStatisticalMethods("");
			if (!flowKpiNameT.getKpiStatisticalMethods().equals(this.kpiMethodsArea.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiStatisticalMethods().equals(this.kpiMethodsArea.getText())) {
				isUpdate = true;
			}
		}
		// kpi目标值
		if (flowKpiNameT.getKpiTarget() == null || "".equals(flowKpiNameT.getKpiTarget())) {
			flowKpiNameT.setKpiTarget("");
			if (!flowKpiNameT.getKpiTarget().equals(this.targetField.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiTarget().equals(this.targetField.getText())) {
				isUpdate = true;
			}
		}

		if (flowKpiNameT.getKpiPeriod() == null || "".equals(flowKpiNameT.getKpiPeriod())) {
			flowKpiNameT.setKpiPeriod("");
			if (!flowKpiNameT.getKpiPeriod().equals(this.periodArea.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiPeriod().equals(this.periodArea.getText())) {
				isUpdate = true;
			}
		}

		if (flowKpiNameT.getKpiPurpose() == null || "".equals(flowKpiNameT.getKpiPurpose())) {
			flowKpiNameT.setKpiPurpose("");
			if (!flowKpiNameT.getKpiPurpose().equals(this.purposeArea.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiPurpose().equals(this.purposeArea.getText())) {
				isUpdate = true;
			}
		}

		if (flowKpiNameT.getKpiPoint() == null || "".equals(flowKpiNameT.getKpiPoint())) {
			flowKpiNameT.setKpiPoint("");
			if (!flowKpiNameT.getKpiPoint().equals(this.pointArea.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiPoint().equals(this.pointArea.getText())) {
				isUpdate = true;
			}
		}

		if (flowKpiNameT.getKpiExplain() == null || "".equals(flowKpiNameT.getKpiExplain())) {
			flowKpiNameT.setKpiExplain("");
			if (!flowKpiNameT.getKpiExplain().equals(this.explainArea.getText())) {
				isUpdate = true;
			}
		} else {
			if (!flowKpiNameT.getKpiExplain().equals(this.explainArea.getText())) {
				isUpdate = true;
			}
		}

		if (isUpdate) {
			int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("yncancel"), null,
					JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return false;
			}
			return true;
		}
		return true;
	}

	public UpdateProcessKPIDialog(JecnFlowKpiNameT flowKpiNameT) {
		// 获取要编辑的KPI数据
		this.flowKpiNameT = flowKpiNameT;
		// 将KPI属性显示到编辑面板上
		initLayout(flowKpiNameT);
	}

	/**
	 * 将KPI属性显示到编辑面板上
	 * 
	 * @author fuzhh Nov 1, 2012
	 * @param flowKpiNameT
	 */
	public void initLayout(JecnFlowKpiNameT flowKpiNameT) {
		// 名称
		this.kpiNameField.setText(flowKpiNameT.getKpiName());
		// 类型
		targetTypeList = showDialog.getTargetTypeList();
		if (flowKpiNameT.getKpiType() == null || "".equals(flowKpiNameT.getKpiType())) {
			this.kpiTypeCombox.setSelectedIndex(0);
		} else {
			int targetType = flowKpiNameT.getKpiType();
			for (int i = 0; i < targetTypeList.size(); i++) {
				if (targetType == i) {
					this.kpiTypeCombox.setSelectedIndex(i + 1);
					break;
				}
			}
		}
		// kpi值单位名称(纵向单位名称)
		this.longitudinalCombox.setSelectedIndex(Integer.parseInt(flowKpiNameT.getKpiVertical()));
		// 数据统计时间\频率(kpi横向单位名称)
		this.transverseCombox.setSelectedIndex(Integer.parseInt(flowKpiNameT.getKpiHorizontal()));
		// 定义
		this.kpiDefinedArea.setText(flowKpiNameT.getKpiDefinition());
		// 统计方法
		this.kpiMethodsArea.setText(flowKpiNameT.getKpiStatisticalMethods());
		// 目标值
		this.targetField.setText(flowKpiNameT.getKpiTarget());
		creatime = flowKpiNameT.getCreatTime();

		// kpi目标值比较符号
		this.targetCombox.setSelectedIndex(flowKpiNameT.getKpiTargetOperator());
		// 数据获取方式
		this.dataSourceMethodCombox.setSelectedIndex(flowKpiNameT.getKpiDataMethod());
		// IT系统
		// 根据流程ID获得支持工具ID集合
		try {
			listToolIds = ConnectionPool.getFlowTool().getSustainToolConnTList(flowKpiNameT.getKpiAndId(), 0);
			// 获得支持工具树对象
			listFlowTool = ConnectionPool.getFlowTool().getSustainToolsByIDs(listToolIds);
			itSystemIds = JecnDesignerCommon.getLook(listFlowTool, itSystemArea);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// ********* 数据提供者
		String dataProviderName = null;
		dataProviderField.setText(flowKpiNameT.getKpiDataPeopleName());
		dataProviderHideField.setText(flowKpiNameT.getKpiDataPeopleName());
		dataProviderId = flowKpiNameT.getKpiDataPeopleId();
		if (dataProviderId != null && !"".equals(dataProviderId) && !"null".equals(dataProviderId)
				&& !dataProviderId.equals("-1")) {
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(dataProviderId);
			jecnTreeBean.setName(dataProviderName);
			dataProviderList.add(jecnTreeBean);
		}
		// 相关度
		this.kpiRelevanceCombox.setSelectedIndex(flowKpiNameT.getKpiRelevance());
		// 指标来源
		this.targetSourceCombox.setSelectedIndex(flowKpiNameT.getKpiTargetType());
		// 支撑的一级指标
		this.kpiFirstTargetArea.setText(flowKpiNameT.getFirstTargetContent());
		this.kpiFirstTargetId.setText(flowKpiNameT.getFirstTargetId());
		// 创建人
		createPeopleId = flowKpiNameT.getCreatePeopleId();

		this.periodArea.setText(flowKpiNameT.getKpiPeriod());
		this.pointArea.setText(flowKpiNameT.getKpiPoint());
		this.explainArea.setText(flowKpiNameT.getKpiExplain());
		this.purposeArea.setText(flowKpiNameT.getKpiPurpose());
	}

	@Override
	public String getKPITile() {
		return JecnProperties.getValue("editFlowKPI");
	}

	@Override
	public void saveData() {

		// KPI名称
		String kpiName = this.kpiNameField.getText().trim();
		// KPI 类型
		String kpiType = (String) this.kpiTypeCombox.getSelectedItem();
		// KPI 纵向单位名称
		String kpiLongitudinal = String.valueOf(this.longitudinalCombox.getSelectedIndex());
		// kpi横向单位名称
		String kpiTransverse = String.valueOf(this.transverseCombox.getSelectedIndex());
		// kpi定义
		String kpiDefined = this.kpiDefinedArea.getText();
		// kpi统计方法
		String kpiMethods = this.kpiMethodsArea.getText();
		// kpi目标值
		String kpiTarget = this.targetField.getText();

		// kpi目标值比较符号
		int kpiTargetOperator = this.targetCombox.getSelectedIndex();
		// 数据获取方式
		int dataSourceMethod = this.dataSourceMethodCombox.getSelectedIndex();
		// 数据提供者
		Long kpiDataPeopleId = this.dataProviderId;
		// 相关度
		int kpiRelevance = this.kpiRelevanceCombox.getSelectedIndex();
		// 指标来源
		int kpiTargetType = this.targetSourceCombox.getSelectedIndex();
		// 支撑的一级指标ID
		String firstTargetId = this.kpiFirstTargetId.getText();
		// 数据提供者名称
		String kpiDataPeopleName = this.dataProviderField.getText();
		// 数据提供者隐藏名称
		String kpiDataPeopleHideName = this.dataProviderHideField.getText();
		// 支撑的一级指标内容
		String firstTargetContent = this.kpiFirstTargetArea.getText();
		// 选中人员ID后，输入不同的流程名称，最后还是保存与ID对应的名称
		if (kpiDataPeopleName != null && !"".equals(kpiDataPeopleName)
				&& !kpiDataPeopleHideName.equals(kpiDataPeopleName)) {
			kpiDataPeopleName = kpiDataPeopleHideName;
		}
		String kpiPurpose = this.purposeArea.getText();// 设置目的
		String kpiPoint = this.pointArea.getText();// 测量点
		String kpiPeriod = this.periodArea.getText();// 统计周期
		String kpiExplain = this.explainArea.getText();// 说明

		// KPI数据Bean
		flowKpiNameT.setFlowId(flowId);
		flowKpiNameT.setKpiName(kpiName);
		if (kpiType != null && !"".equals(kpiType)) {
			for (int i = 0; i < targetTypeList.size(); i++) {
				if (targetTypeList.get(i).equals(kpiType)) {
					flowKpiNameT.setKpiType(i);
					break;
				}
			}
		}
		
		String itStytemNames=itSystemArea.getText();
		
		flowKpiNameT.setKpiHorizontal(kpiTransverse);
		flowKpiNameT.setKpiVertical(kpiLongitudinal);
		flowKpiNameT.setKpiDefinition(kpiDefined);
		flowKpiNameT.setKpiStatisticalMethods(kpiMethods);
		flowKpiNameT.setKpiTarget(kpiTarget);
		flowKpiNameT.setCreatTime(creatime);
		flowKpiNameT.setCreatePeopleId(createPeopleId);
		flowKpiNameT.setKpiTargetOperator(kpiTargetOperator);
		flowKpiNameT.setKpiDataMethod(dataSourceMethod);
		flowKpiNameT.setKpiDataPeopleId(kpiDataPeopleId);
		flowKpiNameT.setKpiRelevance(kpiRelevance);
		flowKpiNameT.setKpiTargetType(kpiTargetType);
		flowKpiNameT.setFirstTargetId(firstTargetId);
		flowKpiNameT.setUpdateTime(new Date());
		flowKpiNameT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		flowKpiNameT.setKpiDataPeopleName(kpiDataPeopleName);// 数据提供者名称
		flowKpiNameT.setFirstTargetContent(firstTargetContent);// 支撑的一级指标
		flowKpiNameT.setKpiITSystemIds(itSystemIds);
		flowKpiNameT.setKpiITSystemNames(itStytemNames);
		flowKpiNameT.setKpiPurpose(kpiPurpose);
		flowKpiNameT.setKpiPoint(kpiPoint);
		flowKpiNameT.setKpiExplain(kpiExplain);
		flowKpiNameT.setKpiPeriod(kpiPeriod);

		// 更新流程KPI
		try {
			ConnectionPool.getProcessAction().updateKPI(flowKpiNameT,
					JecnConstants.loginBean.getJecnUser().getPeopleId());

			// 刷新设计向导，流程KPI表格选中行前几列
			reRreshRowKpiNameTable(flowKpiNameT);
			isOperation = true;
			this.dispose();
		} catch (Exception e) {
			log.error("UpdateProcessKPIDialog saveData is error", e);
		}
	}

	private void reRreshRowKpiNameTable(JecnFlowKpiNameT flowKpiNameT) {
		JecnFlowKpiName flowKpiName = new JecnFlowKpiName();
		BeanUtils.copyProperties(flowKpiNameT, flowKpiName);
		List<String> rowValues = ConnectionPool.getProcessAction().getConfigKpiRowValues(flowKpiName,
				ConnectionPool.getConfigAciton().selectTableConfigItemBeanByType(1, 16));
		if (rowValues == null) {
			return;
		}
		int selectRow = showDialog.getProcessKPIPropertyTable().getSelectedRow();
		Vector<Vector<String>> vs = ((DefaultTableModel) showDialog.getProcessKPIPropertyTable().getModel()).getDataVector();
		Vector<String> rows = vs.get(selectRow);
		for (int i = 0; i < rowValues.size(); i++) {
			showDialog.getProcessKPIPropertyTable().setValueAt(rowValues.get(i), selectRow, i + 1);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		if (showDialog != null) {
			List<JecnFlowKpiNameT> list = showDialog.getKpiShowValues().getKpiNameList();
			for (JecnFlowKpiNameT jecnFlowKpiNameT : list) {
				// 获取选中的KPI表格中的kpi名称 ,若与选中的名称相同则返回false
				if (String.valueOf(name).equals(selecKPIName.toString())) {
					return false;
				} else if (name.equals(jecnFlowKpiNameT.getKpiName().trim())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void cancelButPerformed() {
		// 判断可输入控件是否取消
		if (check()) {
			this.dispose();
		}
	}

}
