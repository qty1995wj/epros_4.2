package com.jecn.epros.domain.home;

/**
 * Created by ZXH on 2017/3/23.
 */
public class JoinProcessRelatedFile {
    private Long id;
    private String name;

    private Long flowId;

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
