package com.jecn.webservice.cms.sync.dataimport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.jecn.epros.server.action.web.login.ad.cms.JecnCMSAfterItem;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 信息通达webService 接口数据获取
 * 
 *@author ZXH
 * @date 2018-3-26 上午10:06:07
 */
public enum CMSWebServiceClient {
	INSTANCE;
	private static final Logger log = Logger.getLogger(CMSWebServiceClient.class);

	private static final String USERNAME = JecnCMSAfterItem.getValue("IASName");
	private static final String PASSWORD = JecnCMSAfterItem.getValue("userAcccount");

	/**
	 * 人员同步获取 webservice 组织 接口获取的数据集合
	 */
	public BaseBean syncDataViews() throws Exception {
		UUVServiceLocator serviceLocator = new UUVServiceLocator();
		String data = serviceLocator.getUUVServiceSoap().getAllOrganizations(USERNAME, PASSWORD);

		if (StringUtils.isBlank(data)) {
			return null;
		}
		String filePath = JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.TEMP_DIR + File.separator
				+ "Organizations.xml";
		File file = new File(filePath);
		if (file.exists()) {
			file.delete();
		}
		FileUtils.writeStringToFile(file, data, "UTF-8");

		// 创建XML文件
		// JecnFinal.createXmlFile(JecnFinal.TEMP_DIR, "Organizations.xml",
		// data);
		// 解析XML
		Document document = getDocumentByXml(file);
		// document.get
		Element root = document.getRootElement();
		List<Element> deptNodes = root.elements();

		List<DeptBean> depts = new ArrayList<DeptBean>();

		List<UserBean> users = new ArrayList<UserBean>();

		DeptBean deptBean = null;
		for (Element element : deptNodes) {
			deptBean = getDept(element);
			if (StringUtils.isBlank(deptBean.getDeptNum())) {
				continue;
			}

			if ("OR1000000000".equals(deptBean.getDeptNum())) {
				deptBean.setPerDeptNum("0");
			}

			if (StringUtils.isBlank(deptBean.getPerDeptNum())) {
				continue;
			}
			// 获取部门内人员集合
			users.addAll(syncUserAndPostViews(deptBean.getDeptNum()));
			// if (deptBean.getPerDeptNum().equals("OR1000000000")) {
			// System.out.println(deptBean.getDeptName() + " ----" +
			// deptBean.getzOrder());
			// }
			depts.add(deptBean);
		}

		return new BaseBean(depts, users);
	}

	/**
	 * 人员同步获取 webservice 组织 接口获取的数据集合
	 */
	public List<UserBean> syncUserAndPostViews(String deptNum) throws Exception {
		UUVServiceLocator serviceLocator = new UUVServiceLocator();
		// 查询当前部门下的用户
		String data = serviceLocator.getUUVServiceSoap().getCompactUserListByOrgID(USERNAME, PASSWORD, deptNum);

		if (StringUtils.isBlank(data) || "ERROR:该组织下无用户".equals(data)) {
			return new ArrayList<UserBean>();
		}
		String filePath = JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.TEMP_DIR + File.separator
				+ JecnCommon.getUUID() + ".xml";
		File file = new File(filePath);
		if (file.exists()) {
			file.delete();
		}
		FileUtils.writeStringToFile(file, data, "UTF-8");

		// 解析XML
		Document document = getDocumentByXml(file);
		Element root = document.getRootElement();
		List<Element> userNodes = root.elements();

		List<UserBean> users = new ArrayList<UserBean>();
		UserBean userBean = null;
		for (Element element : userNodes) {
			userBean = getUser(element, deptNum);
			if (StringUtils.isBlank(userBean.getUserNum())) {
				continue;
			}
			if (StringUtils.isNotBlank(userBean.getPosNum()) && StringUtils.isNotBlank(userBean.getPosName())) {
				userBean.setDeptNum(deptNum);
			}
			users.add(userBean);
		}
		return users;
	}

	private UserBean getUser(Element element, String deptNum) {
		List<Element> attrs = element.elements();
		UserBean userBean = new UserBean();
		for (Element ele : attrs) {
			if ("IsUserOrg".equals(ele.getName())) {

			}
			if ("AccountName".equals(ele.getName())) {
				userBean.setUserNum(ele.getStringValue());
			} else if ("ChsName".equals(ele.getName())) {
				userBean.setTrueName(ele.getStringValue());
			} else if ("EmailAddress".equals(ele.getName())) {
				userBean.setEmail(ele.getStringValue());
				userBean.setEmailType(1);
			} else if ("EmailAddress".equals(ele.getName())) {
				userBean.setEmail(ele.getStringValue());
				userBean.setEmailType(1);
			} else if ("OfficePhone".equals(ele.getName())) {
				userBean.setPhone(ele.getStringValue());
			} else if ("OfficePhone".equals(ele.getName())) {
				userBean.setPhone(ele.getStringValue());
			} else if ("Position".equals(ele.getName())) {
				userBean.setPosName(ele.getStringValue());
				if (StringUtils.isNotBlank(userBean.getPosName())) {
					userBean.setPosNum(deptNum + "_" + userBean.getPosName());
				}
			}
		}
		return userBean;
	}

	private DeptBean getDept(Element element) {
		List<Element> attrs = element.elements();
		DeptBean deptBean = new DeptBean();
		for (Element ele : attrs) {
			if ("IsUserOrg".equals(ele.getName())) {

			}
			if ("OrgID".equals(ele.getName())) {
				deptBean.setDeptNum(ele.getStringValue());
			} else if ("OrgName".equals(ele.getName())) {
				deptBean.setDeptName(ele.getStringValue());
			} else if ("parentId".equals(ele.getName())) {
				deptBean.setPerDeptNum(ele.getStringValue());
			}
		}
		return deptBean;
	}

	private static Document getDocumentByXml(File file) {
		try {
			SAXReader reader = new SAXReader();
			Document doc = null;
			if (file != null) {
				doc = reader.read(file);
			}
			return doc;
		} catch (Exception e) {
			log.error("dom4J 获取Document异常！", e);
			return null;
		}
	}

}
