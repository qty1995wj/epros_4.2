package com.jecn.epros.domain.journal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.service.CommonServiceUtil;

/**
 * 资源访问日志，搜索返回结果集对象
 *
 * @author lenovo
 */
public class ResourceVisitsResult {
    /**
     * 文件ID
     */
    private Long relatedId;
    /**
     * 文件类型 0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7是流程架构
     **/
    private int relatedType;
    /**
     * 文件类型
     */
    private String relatedName;
    /**
     * 操作次数
     */
    private int operatorTotal;

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    @JsonIgnore
    public int getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(int relatedType) {
        this.relatedType = relatedType;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public int getOperatorTotal() {
        return operatorTotal;
    }

    public void setOperatorTotal(int operatorTotal) {
        this.operatorTotal = operatorTotal;
    }

    public String getStrRelatedType() {
        return CommonServiceUtil.getStringTypeByRelatedType(relatedType);
    }
}
