package epros.designer.gui.task.buss;

import epros.designer.util.JecnProperties;

public class TaskApproveConstant {
	// 提交审批
	public static final String submit_for_app = JecnProperties
			.getValue("submitForApp");
	// 试运行版
	public static final String testRunVersion = JecnProperties
			.getValue("testRunVersionC")+JecnProperties
			.getValue("chinaColon");
	// #试运行结束时间：
	public static final String testRunEnd = JecnProperties
			.getValue("testRunEndC");
	// #试运行时间不能为空
	public static final String testRunTimeIsNotNull = JecnProperties
			.getValue("testRunTimeIsNotNull");
	// 正式版
	public static final String official_version = JecnProperties
			.getValue("officialVersion");
	// 升级版
	public static final String upgreaded_version = JecnProperties
			.getValue("upgreadedVersion");

	/** 找不到所要的配置文件,请联系管理员 */
	public static final String not_find_config_xml_error = JecnProperties
			.getValue("notFindConfigXmlError");
	// #访问服务器异常！
	public static final String access_server_error = JecnProperties
			.getValue("accessServerError");
	// 文件选择
	public static final String file_select = JecnProperties
			.getValue("fileChoose");
	// 版本号已存在，请修改！
	public static final String num_is_exit = JecnProperties
			.getValue("numIsExit");
	// 待上传的审批文件
	public static final String be_uploaded_approval_file = JecnProperties
			.getValue("beUploadedApprovalFile");
	// 文件选择
	public static final String choose_file = JecnProperties
			.getValue("fileChoose");
	// 是否退出？
	public static final String y_n_exit = JecnProperties.getValue("ynexit");
	// 提示！
	public static final String prompt = JecnProperties.getValue("prompt");
	// 审批人不能为空！
	public static final String apperover_not_empty = JecnProperties
			.getValue("apperoverNotEmpty");
	// 创建人不存在！
	public static final String create_people_no_exist = JecnProperties
			.getValue("createPeopleNoExist");
	// 文控审核人
	public static final String audit_approve_people = JecnProperties
			.getValue("auditApprovePeople");
	// 部门审核人
	public static final String dept_approve_people = JecnProperties
			.getValue("deptApprovePeople");
	// 评审人
	public static final String review_approve_people = JecnProperties
			.getValue("reviewApprovePeople");
	// 批准审核人
	public static final String approve_people = JecnProperties
			.getValue("approvePeople");
	// 各业务体系负责人
	public static final String business_system_people = JecnProperties
			.getValue("businessSystemPeople");
	// IT总监
	public static final String it_approve_people = JecnProperties
			.getValue("itApprovePeople");
	// 事业部经理
	public static final String division_man_people = JecnProperties
			.getValue("divisionManPeople");
	// 总经理
	public static final String general_man_people = JecnProperties
			.getValue("generalManPeople");

	// 不能为空！
	public static final String is_not_empty = JecnProperties
			.getValue("isNotEmpty");
	// 创建审批任务成功！
	public static final String create_task_success = JecnProperties
			.getValue("createTaskSuccess");
	// 创建审批任务失败！
	public static final String create_task_false = JecnProperties
			.getValue("createTaskFalse");
	// 开始时间不能为空！
	public static final String startTimeIsNotNull = JecnProperties
			.getValue("startTimeIsNotNull");
	// 结束时间不能为空！
	public static final String endTimeIsNotNull = JecnProperties
			.getValue("endTimeIsNotNull");
	// 开始时间不能大于结束时间！
	public static final String sTimeNotGreaterETime = JecnProperties
			.getValue("sTimeNotGreaterETime");
	// 试运行时间最少为
	public static final String testRunTimeLess = JecnProperties
			.getValue("testRunTimeLess");
	// 天
	public static final String day = JecnProperties.getValue("day");

	/** *******************文件审批****************************** */
	// 文件编号
	public static final String fileNumber = JecnProperties.getValue("fileNum");
	// 文件名称
	public static final String fileName = JecnProperties.getValue("fileName");
	// 文件路径
	public static final String filePath = JecnProperties.getValue("fileSrc");
	// 文件上传,已成功！
	public static final String fileUplodeSuc = JecnProperties
			.getValue("fileUplodeSuc");
	// 文件ID
	public static final String fileId = JecnProperties.getValue("fileId");
	// 版本号已经存在,请修改！
	public static final String docIdExist = JecnProperties
			.getValue("docIdExist");
	// 错误提示！
	public static final String error_Message = JecnProperties
			.getValue("errorMessage");
	// 创建审批任务成功！
	public static final String create_app_sucess = JecnProperties
			.getValue("createTaskSuccess");
	// 创建审批任务失败！
	public static final String create_app_error = JecnProperties
			.getValue("createTaskFalse");
	// 上传文件为空,请选择！
	public static final String uplode_file_is_null = JecnProperties
			.getValue("uplodeFileIsNull");
	// 重复存在
	public static final String repeat_xist = JecnProperties
			.getValue("repeatXist");
	// 已经在
	public static final String is_exist = JecnProperties.getValue("isExist");
	// 在目录下已经存在！
	public static final String this_dir_has_exist = JecnProperties
			.getValue("thisDirHasExist");
	// 评审人不能存在相同人员！
	public static final String reviewerCanNotSamePeople = JecnProperties
			.getValue("reviewerCanNotSamePeople");

	/** *******************文件审批****************************** */

	/** ***** q**************特殊字符处理****************************** */

	// 您输入了特殊字符！
	public static final String input_special_char = JecnProperties
			.getValue("inputSpecialChar");
}
