package com.jecn.epros.server.bean.project;

/**
 * 项目表
 * @author Administrator
 */
public class JecnProject implements java.io.Serializable{
    // Fields
    private Long projectId;//主键ID
    private String projectName;//项目名称
    private Long delSate;//1删除
    public JecnProject(){}
    public JecnProject(Long projectId){
        this.projectId = projectId;
    }
    public JecnProject(Long projectId,String projectName){
        this.projectId = projectId;
        this.projectName = projectName;
    }
    public Long getProjectId() {
        return projectId;
    }
    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }
    public String getProjectName() {
        return projectName;
    }
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    public Long getDelSate() {
        return delSate;
    }
    public void setDelSate(Long delSate) {
        this.delSate = delSate;
    }
}