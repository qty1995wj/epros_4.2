package com.jecn.epros.server.download.wordxml.jinlvkeche;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 金旅客车操作说明模版
 * 
 * @author hyl
 * 
 */
public class JLKCProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍行距 *****/
	private final ParagraphLineRule vLine2 = new ParagraphLineRule(1.5F,
			LineRuleType.AUTO);
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private final ParagraphLineRule leastVline12 = new ParagraphLineRule(12F,
			LineRuleType.AT_LEAST);
	private final float tableWidth = 15.03F;

	public JLKCProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(false);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(tableWidth);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(7.5F);
		setDocStyle(".", textTitleStyle());
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem,
			float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean
				.setSpace(0F, 0F,
						new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		float[] width = null;
		if (JecnContants.showPosNameBox) {
			width = new float[] { 3.5F, 3.5F, 7.5F };
		} else if (!JecnContants.showPosNameBox) {
			width = new float[] { 5.0F, 9.5F };
		}
		List<String[]> rowData2 = new ArrayList<String[]>();
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = "对应职位/岗位";
		// 角色职责
		String roleResponsibility = "应负责任";
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility,
					positionName });
		} else {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility });
		}
		for (String[] str : rowData) {
			if (JecnContants.showPosNameBox) {
				rowData2.add(new String[] { str[0], str[2], str[1] });
			} else {
				rowData2.add(new String[] { str[0], str[2] });
			}
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(),
				getDocProperty().isTblTitleShadow(), getDocProperty()
						.isTblTitleCrossPageBreaks());
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	protected void a10(ProcessFileItem processFileItem, String[] titles,
			List<String[]> rowData) {
		createTitle(processFileItem);
		// 活动编号
		String activityNumber = JecnUtil.getValue("activitynumbers");
		// 执行角色s
		String executiveRole = JecnUtil.getValue("executiveRole");
		// 活动名称
		String eventTitle = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activityDescription = "活动描述";
		String input = "输入";
		String output = "输出";
		float[] width = new float[] { 1.19F, 2.5F, 2.5F, 5.25F, 2F, 2.04F };
		List<Object[]> newRowData = new ArrayList<Object[]>();
		// 标题数组
		newRowData.add(new Object[] { activityNumber, eventTitle,
				executiveRole, activityDescription, input, output });
		// 添加数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
		for (Object[] activeData : processDownloadBean.getAllActivityShowList()) {
			newRowData
					.add(new Object[] { activeData[0], activeData[2],
							activeData[1], activeData[3], activeData[4],
							activeData[5] });
		}

		Table tbl = createTableItem(processFileItem, width, JecnWordUtil
				.obj2String(newRowData));
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty
				.isTblTitleShadow(), docProperty.isTblTitleCrossPageBreaks());

	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	@Override
	protected void a15(ProcessFileItem processFileItem,
			List<String[]> oldRowData) {

		// KPI配置bean
		List<JecnConfigItemBean> configItemBeanList = processDownloadBean
				.getConfigKpiItemBean();
		createTitle(processFileItem);

		List<String[]> data = new ArrayList<String[]>();
		List<String> showItems = new ArrayList<String>();
		List<String> titles = new ArrayList<String>();
		for (JecnConfigItemBean config : configItemBeanList) {
			if ("0".equals(config.getValue())) {
				continue;
			}
			showItems.add(config.getMark());
			titles.add(config.getName());
		}
		data.add(asArray(titles));

		List<String> temp = new ArrayList<String>();
		for (String[] row : oldRowData) { // 对应一条配置数据
			for (String mark : showItems) {
				temp.add(nullToEmpty(getKpiContent(row, mark)));
			}
			data.add(asArray(temp));
			temp.clear();
		}

		// 均分宽度
		int size = showItems.size();
		float eachLen = tableWidth / size;
		float[] sizes = new float[size];
		for (int i = 0; i <= size - 1; i++) {
			sizes[i] = eachLen;
		}
		Table tbl = createTableItem(processFileItem, sizes, data);
		tbl.setTableBean(docProperty.getTblStyle());
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty
				.isTblTitleShadow());

	}

	private String[] asArray(List<String> titles) {
		String[] arr = new String[titles.size()];
		titles.toArray(arr);
		return arr;
	}

	/**
	 * * 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6 KIP值单位名称 7 数据获取方式 8
	 * 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12 目的 13 测量点 14 统计周期 15 说明
	 */
	private String getKpiContent(String[] row, String mark) {
		if ("kpiName".equals(mark)) {// KPI名称
			return row[0];
		} else if ("kpiType".equals(mark)) {// KPI类型
			return row[1];
		} else if ("kpiUtilName".equals(mark)) {// KPI单位名称
			return JecnResourceUtil.getKpiVertical(Integer.valueOf(row[6]));
		} else if ("kpiTargetValue".equals(mark)) {// KPI目标值
			return row[2];
		} else if ("kpiTimeAndFrequency".equals(mark)) {// 数据统计时间频率
			return JecnResourceUtil.getKpiHorizontal(Integer.valueOf(row[5]));
		} else if ("kpiDefined".equals(mark)) {// KPI定义
			return row[3];
		} else if ("kpiDesignFormulas".equals(mark)) {// 流程KPI计算方式
			return row[4];
		} else if ("kpiDataProvider".equals(mark)) {// 数据提供者
			return row[10];
		} else if ("kpiDataAccess".equals(mark)) {// 数据获取方式
			return JecnResourceUtil.getKpiDataMethod(Integer.valueOf(row[7]));
		} else if ("kpiIdSystem".equals(mark)) {// IT 系统
			return row[16];
		} else if ("kpiSourceIndex".equals(mark)) {// 指标来源
			return JecnResourceUtil.getKpiTargetType(Integer.valueOf(row[9]));
		} else if ("kpiRrelevancy".equals(mark)) {// 相关度
			return JecnResourceUtil.getKpiRelevance(Integer.valueOf(row[8]));
		} else if ("kpiSupportLevelIndicator".equals(mark)) {// 支持的一级指标
			return row[11];
		} else if ("kpiPurpose".equals(mark)) {// 设置目的
			return row[12];
		} else if ("kpiPoint".equals(mark)) {// 测量点
			return row[13];
		} else if ("kpiPeriod".equals(mark)) {// 统计周期
			return row[14];
		} else if ("kpiExplain".equals(mark)) {// 说明
			return row[15];
		}
		return "";
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.5F, 3.17F, 2.5F, 3.17F, 1.25F, 1.03F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	protected void a35(ProcessFileItem processFileItem,
			List<FileWebBean> relatedFiles) {
		createTitle(processFileItem);
		String name = JecnUtil.getValue("fileName");
		String fileCode = "文件编号";

		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { name, fileCode });
		for (FileWebBean file : relatedFiles) {
			rowData.add(new String[] { file.getFileName(), file.getFileNum() });
		}
		float[] width = new float[] { 7.15F, 7.8F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, tblTitleStyle());

	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect sect) {
		SectBean sectBean = getSectBean();
		sect.setSectBean(sectBean);
		addTitleContent(sect);
	}

	private Paragraph createEmptyParagraph() {
		Paragraph paragraph = new Paragraph("", new ParagraphBean(Align.center,
				"宋体", Constants.wuhao, false));
		return paragraph;
	}

	/**
	 * 添加首页内容
	 * 
	 * @param sect
	 */
	private void addTitleContent(Sect sect) {

		/***** 默认表格样式 **********/
		TableBean tableBean = tblStyle();
		// tabBean.setTableAlign(Align.center);
		// tabBean.setBorder(0.5F);
		// tabBean.setCellMarginBean(new CellMarginBean(0F, 0F, 0F, 0F));

		Table componyTable = sect
				.createTable(tableBean, new float[] { 15.03F });
		TableRow createTableRow = componyTable.createTableRow(12.95F);
		TableCell componyCell = createTableRow.createTableCell();

		Image logo = getLogoImage();
		PositionBean positionBean = new PositionBean(PositionType.relative, 1F,
				0F, 1F, 0F);
		logo.setPositionBean(positionBean);
		Paragraph logpP = new Paragraph("");
		logpP.appendGraphRun(logo);
		componyCell.addParagraph(logpP);
		Paragraph emptyP = new Paragraph("");
		componyCell.addParagraph(emptyP);
		componyCell.addParagraph(emptyP);
		componyCell.addParagraph(emptyP);
		Paragraph componyP = new Paragraph("金龙旅行车有限公司", new ParagraphBean(
				Align.center, "宋体", Constants.yihao, true));
		componyCell.addParagraph(componyP);
		componyCell.addParagraph(emptyP);
		componyCell.addParagraph(emptyP);
		componyCell.addParagraph(emptyP);
		Paragraph flowNameP = new Paragraph(processDownloadBean.getFlowName(),
				new ParagraphBean(Align.center, "宋体", Constants.sanhao, true));
		componyCell.addParagraph(flowNameP);

		Table nameTable = sect.createTable(tableBean, new float[] { 2.69F,
				4.83F, 3.43F, 4.09F });
		nameTable.setCellValignAndHeight(Valign.center, 0.7F);

		// 级别中应该是不显示t_level=0的以及自己本身
		String L1 = "";
		String L2 = "";
		String L3 = "";
		String L4 = "";
		List<Object[]> parents = processDownloadBean.getParents();
		int parentLen = parents.size();
		if (parentLen > 2) {
			L1 = parents.get(1)[1].toString();
		}
		if (parentLen > 3) {
			L2 = parents.get(2)[1].toString();
		}
		if (parentLen > 4) {
			L3 = parents.get(3)[1].toString();
		}
		if (parentLen > 5) {
			L4 = parents.get(4)[1].toString();
		}

		String pOwner = getPOwner();

		String createTime = "";
		String pubTime = "";
		String version = "";
		String draftPeople = "";
		String joinPeople = "";
		if (processDownloadBean.getHistorys() != null
				&& processDownloadBean.getHistorys().size() > 0) {
			JecnTaskHistoryNew lastHistory = processDownloadBean.getHistorys()
					.get(0);
			createTime = lastHistory.getStartTime();
			pubTime = JecnUtil.DateToStr(lastHistory.getPublishDate(),
					"yyyy-MM-dd");
			version = lastHistory.getVersionId();
			draftPeople = lastHistory.getDraftPerson();
			if (lastHistory.getListJecnTaskHistoryFollow() != null
					&& lastHistory.getListJecnTaskHistoryFollow().size() != 0) {
				for (JecnTaskHistoryFollow follow : lastHistory
						.getListJecnTaskHistoryFollow()) {
					if (follow.getStageMark() != null && follow.getStageMark() == 3) {
						joinPeople += follow.getName() + "/";
					}
					if (joinPeople.endsWith("/")) {
						joinPeople = joinPeople.substring(0, joinPeople
								.length() - 1);
					}
				}
			}
		}

		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "一级流程", nullToEmpty(L1), "上级流程Owner", pOwner });
		data.add(new String[] {
				"二级流程",
				nullToEmpty(L2),
				"流程Owner",
				nullToEmpty(processDownloadBean.getResponFlow()
						.getDutyUserName()) });
		data.add(new String[] { "文件编号 ",
				nullToEmpty(processDownloadBean.getFlowInputNum()), "版本",
				nullToEmpty(version) });
		data.add(new String[] { "制订日期", nullToEmpty(createTime), "生效日期 ",
				nullToEmpty(pubTime) });
		data.add(new String[] { "核准", "会签 ", "审查", "制订 " });
		data.add(new String[] { "", nullToEmpty(joinPeople), "",
				nullToEmpty(draftPeople) });

		nameTable.createTableRow(data, tblContentStyle());

		// ----
		Table changeTable = sect.createTable(tableBean, new float[] { 15.03F });
		TableRow changeRow = changeTable.createTableRow(0.7F);
		changeRow.createCells(new String[] { "文件修订记录" }, Valign.center,
				new ParagraphBean(Align.center, Constants.wuhao, false));

		Table historyTable = sect.createTable(tableBean, new float[] { 1.19F,
				2F, 2F, 5F, 2.34F, 2.5F });
		TableRow headerRow = historyTable.createTableRow(0.7F);
		headerRow.createCells(new String[] { "序号", "版本变更 ", "修订页次",
				"所修订内容的摘要 ", "修订日期 ", "修订者 " }, Valign.center);
		List<String[]> tableData = new ArrayList<String[]>();
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (historys.size() == 0) {
			tableData.add(new String[] { "", "", "", "", "", ""});
		} else {
			int index = 0;
			for (JecnTaskHistoryNew history : historys) {
				index++;
				tableData.add(new String[] {
						String.valueOf(index),
						history.getVersionId(),
						"",
						history.getModifyExplain(),
						JecnUtil.DateToStr(history.getPublishDate(),
								"yyyy-MM-dd"), history.getDraftPerson() });
			}
		}
		historyTable.createTableRow(tableData, tblContentStyle());

		// ---

	}

	private String getPOwner() {
		IBaseDao baseDao = processDownloadBean.getBaseDao();
		Long pid = null;
		boolean pub = processDownloadBean.isPub();
		// 查询上级架构责任人
		if (pub) {
			JecnFlowStructure flowStructure = processDownloadBean
					.getFlowStructure();
			if (flowStructure.getPerFlowId() != 0) {//
				pid = flowStructure.getPerFlowId();
			}
		} else {
			JecnFlowStructureT flowStructureT = processDownloadBean
					.getFlowStructureT();
			if (flowStructureT.getPerFlowId() != 0) {
				pid = flowStructureT.getPerFlowId();
			}
		}
		if (pid == null) {
			return "";
		}

		boolean isFlow = true;
		String hql = "";
		if (pub) {
			hql = "from JecnFlowStructure where flowId=?";
			JecnFlowStructure flow = (JecnFlowStructure) baseDao.getObjectHql(
					hql, pid);
			if (flow.getIsFlow() == 0) {
				isFlow = false;
			}
		} else {
			hql = "from JecnFlowStructureT where flowId=?";
			JecnFlowStructureT flow = (JecnFlowStructureT) baseDao
					.getObjectHql(hql, pid);
			if (flow.getIsFlow() == 0) {
				isFlow = false;
			}
		}
		String pubChar = "";
		if (!pub) {
			pubChar = "_T";
		}
		String infoTable = "JECN_FLOW_BASIC_INFO" + pubChar;
		if (!isFlow) {
			infoTable = "JECN_MAIN_FLOW" + pubChar;
		}

		String sql = "SELECT b.TYPE_RES_PEOPLE,b.RES_PEOPLE_ID from JECN_FLOW_STRUCTURE"
				+ pubChar
				+ " a"
				+ " inner join "
				+ infoTable
				+ " b on a.FLOW_ID=b.FLOW_ID and a.flow_id=?";
		List<Object[]> objs = baseDao.listNativeSql(sql, pid);
		if (objs == null || objs.size() == 0) {
			return "";
		}

		Object[] obj = objs.get(0);
		if (obj[0] == null || obj[1] == null) {
			return "";
		}
		Long resPeopleType = Long.valueOf(obj[0].toString());
		Long resPeopleId = Long.valueOf(obj[1].toString());
		if (resPeopleType == null || resPeopleId == null) {
			return "";
		}
		// 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
		if (resPeopleType == 0) {
			hql = "from JecnUser where peopleId=?";
			JecnUser user = (JecnUser) baseDao.getObjectHql(hql, resPeopleId);
			return user.getTrueName();
		} else {
			hql = "from JecnFlowOrgImage where figureId=?";
			JecnFlowOrgImage image = (JecnFlowOrgImage) baseDao.getObjectHql(
					hql, resPeopleId);
			return image.getFigureText();
		}
	}

	private Image getLogoImage() {
		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(2.54F, 0.87F);
		return logo;
	}

	protected void setParagraphBean(Paragraph p, ParagraphBean pBean) {
		p.setParagraphBean(pBean);
	}

	protected void setParagraphBean(List<Paragraph> ps, ParagraphBean pBean) {
		for (Paragraph p : ps) {
			setParagraphBean(p, pBean);
		}
	}

	/**
	 * 记录保存
	 */
	@Override
	protected void a20(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 记录名称
		String recordName = "表单名称";
		// 保存责任人
		String saveResponsiblePersons = JecnUtil
				.getValue("saveResponsiblePersons");
		// 保存场所
		String savePlace = "保存单位";
		// 归档时间
		String filingTime = JecnUtil.getValue("filingTime");
		// 保存期限
		String expirationDate = "保存年限";
		// 到期处理方式
		String handlingDue = JecnUtil.getValue("treatmentDue");
		float[] width = new float[] { 4.75F, 4.75F, 1.9F, 3.5F };
		List<String[]> rowData2 = new ArrayList<String[]>();
		rowData2.add(0, new String[] { recordName, "表单编号", expirationDate,
				savePlace });
		for (String[] row : rowData) {
			rowData2.add(new String[] { row[0], row[6], row[4], row[2] });
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty
				.isTblTitleShadow(), docProperty.isTblTitleCrossPageBreaks());
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(firstSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		flowChartSect.setSectBean(sectBean);
		createCommfhdr(flowChartSect);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, true);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		Table table = header.createTable(tblStyle(), new float[] { left,
				center, right });
		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "", processDownloadBean.getFlowName(),
				getPubOrSec() });
		data.add(new String[] { "", "",
				nullToEmpty(processDownloadBean.getFlowInputNum()) });
		table.createTableRow(data, pBean);

		table.getRow(0).getCell(0).setVmergeBeg(true);
		table.getRow(1).getCell(0).setVmerge(true);
		table.getRow(0).getCell(0).setRowspan(2);

		table.getRow(0).getCell(1).setVmergeBeg(true);
		table.getRow(1).getCell(1).setVmerge(true);
		table.getRow(0).getCell(1).setRowspan(2);

		Paragraph logoP = table.getRow(0).getCell(0).getParagraph(0);
		Image logo = getLogoImage();
		logoP.appendGraphRun(logo);

		List<TableRow> rows = table.getRows();
		for (TableRow row : rows) {
			row.setHeight(0.77F);
		}
		table.setValign(Valign.center);
		List<Paragraph> paragraphs = table.getRow(0).getCell(1).getParagraphs();
		setParagraphBean(paragraphs, new ParagraphBean(Align.center,
				Constants.xiaosi, true));
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommfdr(Sect sect, float left, float center, float right) {

		Footer header = sect.createFooter(HeaderOrFooterType.odd);
		Paragraph pageParagraph = new Paragraph("", new ParagraphBean(
				Align.center, Constants.wuhao, false));
		pageParagraph.appendCurPageRun();
		pageParagraph.appendTextRun("/");
		pageParagraph.appendTotalPagesRun();
		header.addParagraph(pageParagraph);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 3.69F, 7.25F, 4.09F);
		createCommfdr(sect, 3.69F, 7.25F, 4.09F);
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCommhdr(sect, 3F, 19.7F, 3F);
		createCommfdr(sect, 3F, 19.7F, 3F);
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体",
				Constants.wuhao, false);
		textContentStyle.setInd(0F, 0F, 0F, 1F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(12F,
				LineRuleType.AT_LEAST));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("黑体",
				Constants.xiaosi, true);
		textTitleStyle.setInd(0F, 0F, 0.76F, 0F);
		textTitleStyle.setSpace(1F, 1F, leastVline12);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体",
				Constants.shihao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, "宋体",
				Constants.shihao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(1.5F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableLeftInd(0F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

}
