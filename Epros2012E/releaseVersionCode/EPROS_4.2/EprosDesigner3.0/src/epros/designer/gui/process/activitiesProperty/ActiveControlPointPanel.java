package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.integration.risk.ChooseControlTargetDialog;
import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnControlPointT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.gui.workflow.dialog.JecnActiveDetailsDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 控制点详细信息面板
 * 
 * @author CHEHUANBO
 */
public class ActiveControlPointPanel extends JecnPanel implements CaretListener {
	/** 日志 */
	private static Logger log = Logger.getLogger(ActiveControlPointPanel.class);
	/** 控制点panel */
	private JecnPanel newConPointPanel;

	/** 控制点编号： */
	private JLabel conPointNumLabel;
	/** 控制点编号输入框 */
	private JTextField conPointNumField;
	/** 必填项红星 * */
	private JLabel conPointJLabel = null;
	/** 编号提示信息框 */
	protected JecnUserInfoTextArea activeNumInfoTextArea = null;
	/** 风险编号： */
	private JLabel riskNumLabel;
	/** 风险编号输入框 */
	private JTextField riskNumField;

	/** 控制目标： */
	private JLabel conObjectLabel;
	/** 控制目文本域 */
	private JTextArea conObjectArea;
	/** 控制目文本域滚动面板 */
	private JScrollPane areaScroll;
	/** 控制目标选择按钮 */
	private JButton conObjectButton;
	/** 控制方法 ： */
	private JLabel conMethodLabel;
	/** 控制方法下拉框 */
	private JComboBox conMethodBox;
	/** 控制类型： */
	private JLabel conTypeLabel;
	/** 控制类型下拉框 */
	private JComboBox conTypeBox;
	/** 控制频率： */
	private JLabel conFrequencyLabel;
	/** 控制频率下拉框 */
	private JComboBox conFrequencyBox;
	/** 关键控制点 */
	private JCheckBox criticalConPointBox;
	/** 控制点面板 */
	private JecnPanel conPanel;
	/** 主面板 */
	private JecnPanel jecnMainPanel;
	/** 风险点 */
	private JecnControlPointT controlPoint = null;

	/** 判断是添加还是编辑 0:添加 1：编辑 默认是0 */
	private int addOrEditPoint = 0;

	public ActiveControlPointPanel(JecnControlPointT controlPoint, int addOrEditPoint) {
		this.addOrEditPoint = addOrEditPoint;
		this.controlPoint = controlPoint;
		// 初始化数据
		intComponents();
		// 布局
		initLayout();
		// 初始化数据
		initData();
	}

	private void intComponents() {
		// 初始化组件
		jecnMainPanel = new JecnPanel();
		conPanel = new JecnPanel();
		newConPointPanel = new JecnPanel();
		conPanel.setBorder(BorderFactory.createEtchedBorder());
		newConPointPanel.setBorder(BorderFactory.createEtchedBorder());
		// 控制点编号：
		conPointNumLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("conPointNum"));
		conPointNumField = new JTextField();
		// 编号提示信息框
		activeNumInfoTextArea = new JecnUserInfoTextArea();
		// 风险编号：
		riskNumLabel = new JLabel(JecnProperties.getValue("riskNumC"));
		riskNumField = new JTextField();
		riskNumField.setEditable(false);
		conPointJLabel = new JLabel(JecnProperties.getValue("required"));
		// 红色背景
		conPointJLabel.setForeground(Color.red);
		// 控制目标：
		conObjectLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("conObject"));
		conObjectArea = new JTextArea();
		conObjectArea.setEditable(false);
		conObjectArea.setBorder(null);
		conObjectArea.setLineWrap(true);
		// 初始化滚动面板
		areaScroll = new JScrollPane(conObjectArea);
		areaScroll.setBorder(BorderFactory.createLineBorder(Color.gray));
		areaScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		areaScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 选择
		conObjectButton = new JButton(JecnProperties.getValue("selectBtn"));
		// 控制方法 ： 0:手动 1:自动(IT)
		conMethodLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("conMethod"));
		conMethodBox = new JComboBox();
		// 手工
		conMethodBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conManual"));
		// 系统(IT)
		conMethodBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("it"));
		// 人工与系统(人工/IT)
		conMethodBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("renOrIt"));
		// 控制类型： 0:预防性 1:发现性
		conTypeLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("conType"));
		conTypeBox = new JComboBox();
		// 预防性
		conTypeBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conPreventive"));
		// 发现性
		conTypeBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conFound"));

		// 控制频率： 0:随时 1:日 2: 周 3:月 4:季度 5:年度
		conFrequencyLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("conFrequency"));
		conFrequencyBox = new JComboBox();
		// 0:随时（不定期）
		conFrequencyBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("noScheduled"));
		// 1:日
		conFrequencyBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conDay"));
		// 2: 周
		conFrequencyBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conWeek"));
		// 3:月
		conFrequencyBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conMonth"));
		// 4:季度
		conFrequencyBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conQuarter"));
		// 5:年度
		conFrequencyBox.addItem(JecnResourceUtil.getJecnResourceUtil().getValue("conYear"));

		// 关键控制点 0:是 1:否
		criticalConPointBox = new JCheckBox();
		// 关键控制点
		criticalConPointBox.setText(JecnResourceUtil.getJecnResourceUtil().getValue("criticalConPoint"));

		// 设置背景色
		criticalConPointBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		conFrequencyBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		conTypeBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		conMethodBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		conObjectButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		conObjectArea.setBackground(JecnUIUtil.getNoEditColor());
		riskNumField.setBackground(JecnUIUtil.getNoEditColor());
		conPointNumField.setBackground(Color.WHITE);
		newConPointPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		conPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnMainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		/**
		 * 控制目标选择按钮监听
		 */
		conObjectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				conObjectButtonListener();
			}
		});

		// 编号
		conPointNumField.addCaretListener(this);
	}

	/**
	 * 
	 *控制目标选择按钮监听
	 */
	private void conObjectButtonListener() {

		ChooseControlTargetDialog controlTargetDialog = new ChooseControlTargetDialog(controlPoint.getRiskId());
		controlTargetDialog.setVisible(true);
		if (controlTargetDialog.isOkButton()) {// 点击确定
			// 选中控制目标ID
			controlPoint.setTargetId(controlTargetDialog.getControlTargetId());
			// 风险ID
			controlPoint.setRiskId(controlTargetDialog.getRiskId());
			// 控制目标
			conObjectArea.setText(controlTargetDialog.getControlTargetDesc());

			riskNumField.setText(controlTargetDialog.getRiskNum());

		}
	}

	private void initLayout() {
		/** 标签间距 */
		Insets insetsLabel = new Insets(7, 11, 7, 3);
		/** 内容间距 */
		Insets insetsContent = new Insets(7, 0, 7, 5);
		// 添加组件，设置组件布局
		Insets insetsPanel = new Insets(3, 7, 3, 7);
		// 控制点面板
		GridBagConstraints c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insetsPanel, 0, 0);
		jecnMainPanel.add(conPanel, c);

		// 添加主面板
		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(jecnMainPanel, c);

		/*************** 控制点信息 面板 */

		// 控制目标：面板
		JecnPanel conPointPanel = new JecnPanel();

		JecnPanel conMethodPanel = new JecnPanel();

		// 控制点：带文本域
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insetsPanel,
				0, 0);
		conPanel.add(conPointPanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsPanel, 0, 0);
		conPanel.add(conMethodPanel, c);

		/*******************/
		// 控制点编号：
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		conPointPanel.add(conPointNumLabel, c);

		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		conPointPanel.add(conPointNumField, c);

		// 必填项，*
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		conPointPanel.add(conPointJLabel, c);

		// 编号信息显示框
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 5), 0, 0);
		conPointPanel.add(activeNumInfoTextArea, c);

		// 风险编号
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		conPointPanel.add(riskNumLabel, c);

		c = new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		conPointPanel.add(riskNumField, c);

		/********************/
		// 控制目标：
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		conPointPanel.add(conObjectLabel, c);

		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		conPointPanel.add(areaScroll, c);
		// 选择
		c = new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		conPointPanel.add(conObjectButton, c);

		/*********************/
		// 控制方法 ：
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				7, 23, 7, 3), 0, 0);
		conMethodPanel.add(conMethodLabel, c);

		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		conMethodPanel.add(conMethodBox, c);

		/******************/
		// 控制类型：
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		conMethodPanel.add(conTypeLabel, c);

		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		conMethodPanel.add(conTypeBox, c);

		/********************/
		// 控制频率：
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				7, 23, 7, 3), 0, 0);
		conMethodPanel.add(conFrequencyLabel, c);

		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		conMethodPanel.add(conFrequencyBox, c);

		// 关键控制点
		c = new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				7, 8, 7, 3), 0, 0);
		conMethodPanel.add(criticalConPointBox, c);

	}

	/**
	 * 初始化数控
	 * 
	 */
	private void initData() {
		if (addOrEditPoint == 0) {// 添加 C
			controlPoint = new JecnControlPointT();
			return;
		} else if (addOrEditPoint == 1 && controlPoint != null) {// 编辑 C
			// 控制点编号
			conPointNumField.setText(controlPoint.getControlCode());
			if (controlPoint.getKeyPoint() == 1) {
				// 关键控制点
				criticalConPointBox.setSelected(false);
			} else {// 选中
				criticalConPointBox.setSelected(true);
			}
			// 控制方法
			conMethodBox.setSelectedIndex(controlPoint.getMethod());
			// 控制类型
			conTypeBox.setSelectedIndex(controlPoint.getType());
			// 控制频率
			conFrequencyBox.setSelectedIndex(controlPoint.getFrequency());
			// 风险编号
			riskNumField.setText(controlPoint.getRiskNum());
			// 控制目标
			conObjectArea.setText(controlPoint.getControlTarget());
		}
	}

	/**
	 * 编号长度验证
	 * 
	 * @return true 成功
	 */
	public boolean isCheckButt(JLabel jLabel) {

		if (controlPoint == null) {
			return true;
		}
		if (DrawCommon.isNullOrEmtryTrim(conPointNumField.getText())) {// 控制点编号不能为空！
			jLabel.setText(JecnProperties.getValue("conPointNumNotNull"));
			return false;
		} else if (DrawCommon.isNullOrEmtryTrim(conObjectArea.getText())) { // 控制目标
			jLabel.setText(JecnProperties.getValue("conTargetNotNull"));
			return false;
		} else if (DrawCommon.isNullOrEmtryTrim(riskNumField.getText())) { // 风险编号
			jLabel.setText(JecnProperties.getValue("riskNumNotNull"));
			return false;
		}
		// 编号
		boolean ret = checkTextFieldLength(activeNumInfoTextArea, conPointNumField);
		return ret;

	}

	/**
	 * 点击确定
	 * 
	 * @author chehuanbo
	 * 
	 */
	public JecnControlPointT getControlPoint() {
		// 控制点编号
		controlPoint.setControlCode(conPointNumField.getText());
		// 创建人ID
		controlPoint.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		controlPoint.setCreateTime(new Date());
		// 更新人
		controlPoint.setUpdatePersonId(controlPoint.getCreatePersonId());
		// 更新时间
		controlPoint.setUpdateTime(new Date());

		if (criticalConPointBox.isSelected()) {// 选中关键控制点
			// 是否是关键控制点：0：是，1：否
			controlPoint.setKeyPoint(0);
		} else {
			controlPoint.setKeyPoint(1);
		}
		// 频率
		controlPoint.setFrequency(conFrequencyBox.getSelectedIndex());
		// 控制方法 0:手动1:自动
		controlPoint.setMethod(conMethodBox.getSelectedIndex());
		controlPoint.setNote("");
		// 控制类型 0:预防性1:发现性
		controlPoint.setType(conTypeBox.getSelectedIndex());
		// 控制目标
		controlPoint.setControlTarget(conObjectArea.getText());
		// 风险编号
		controlPoint.setRiskNum(riskNumField.getText());
		return controlPoint;
	}

	/**
	 * 
	 * 输入框的插入字符事件
	 * 
	 */
	@Override
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	private void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == conPointNumField) {// 编号输入框
			// 编号长度超过122个字符或61个汉字
			checkTextFieldLength(activeNumInfoTextArea, conPointNumField);
		}
	}

	/**
	 * 
	 * 校验JTextField的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textField
	 *            JTextField
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	private boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JTextField textField) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNameLength(textField.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	public int getAddOrEditPoint() {
		return addOrEditPoint;
	}

	public void setAddOrEditPoint(int addOrEditPoint) {
		this.addOrEditPoint = addOrEditPoint;
	}

}
