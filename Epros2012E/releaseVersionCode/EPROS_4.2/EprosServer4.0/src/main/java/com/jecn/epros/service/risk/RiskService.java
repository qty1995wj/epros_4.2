package com.jecn.epros.service.risk;

import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.risk.RiskBaseInfoBean;
import com.jecn.epros.domain.risk.RiskRelatedFileBean;
import com.jecn.epros.domain.risk.SearchRiskResultBean;

import java.util.List;
import java.util.Map;

public interface RiskService {
    /**
     * 树节点展开
     *
     * @param map
     * @return
     */
    public List<TreeNode> findChildRisks(Map<String, Object> map);


    /**
     * 概括信息
     *
     * @param map
     * @return
     */
    public RiskBaseInfoBean findBaseInfoBean(Map<String, Object> map);


    /**
     * 相关文件
     *
     * @param map
     * @return
     */
    public RiskRelatedFileBean findRelatedFiles(Map<String, Object> map);


    /**
     * 搜索
     *
     * @param map
     * @return
     */
    public List<SearchRiskResultBean> searchRisk(Map<String, Object> map);

    /**
     * 搜索 总数
     *
     * @param map
     * @return
     */
    public int searchRiskTotal(Map<String, Object> map);


    public Inventory getInventory(Map<String,Object> paramMap);
}
