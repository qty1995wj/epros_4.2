package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

/**
 * 
 * 任务审批环节面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTaskApprPanel extends JecnAbstractPropertyBasePanel implements
		ActionListener {

	/** 其他项面板 */
	private JecnPanel otherPanel = null;

	/** 主导发布类型:task_whoConntrolLead */
	private JLabel docPubTypeLabel = null;
	/** 文控主导审核发布 */
	private JecnConfigRadioButton docControlLeadRadioButton = null;
	/** 拟稿人主导发布 */
	private JecnConfigRadioButton docNotControlLeadRadioButton = null;

	public JecnTaskApprPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 表面板
		tableScrollPane = new JecnThreeTableScrollPane(dialog);
		this.add(tableScrollPane, BorderLayout.CENTER);

		// 其他项面板
		otherPanel = new JecnPanel();

		// 主导发布类型:
		docPubTypeLabel = new JLabel(JecnProperties
				.getValue("task_whoConntrolLead"));
		// 文控主导审核发布
		docControlLeadRadioButton = new JecnConfigRadioButton(JecnProperties
				.getValue("task_docControllead"));
		// 非文控主导审核发布
		docNotControlLeadRadioButton = new JecnConfigRadioButton(JecnProperties
				.getValue("task_notDocControlLead"));

		// 其他项面板
		otherPanel.setOpaque(false);
		otherPanel.setBorder(null);

		// 添加按钮组
		ButtonGroup group = new ButtonGroup();
		group.add(docControlLeadRadioButton);
		group.add(docNotControlLeadRadioButton);

		// 事件
		docControlLeadRadioButton.addActionListener(this);
		docNotControlLeadRadioButton.addActionListener(this);
		// 命令按钮名称
		docControlLeadRadioButton.setActionCommand("docControlLeadRadioButton");
		docNotControlLeadRadioButton
				.setActionCommand("docNotControlLeadRadioButton");

		docControlLeadRadioButton.setOpaque(false);
		docNotControlLeadRadioButton.setOpaque(false);
		// 默认
		docNotControlLeadRadioButton.setSelected(true);

	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		this.add(tableScrollPane, BorderLayout.CENTER);
		this.add(otherPanel, BorderLayout.SOUTH);

		Insets insets = new Insets(5, 5, 5, 5);

		// 流程责任人:标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docPubTypeLabel, c);
		// 流程责任人:人员
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docControlLeadRadioButton, c);
		// 流程责任人: 岗位
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docNotControlLeadRadioButton, c);
		// 空闲区域
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		otherPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean
				.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String value = itemBean.getValue();

			if (ConfigItemPartMapMark.taskAppPubType.toString().equals(mark)) {// 主导发布类型：1：文控审核主导审批发布；0：正常审批发布（拟稿人主导发布）（默认）
				docNotControlLeadRadioButton.setItemBean(itemBean);
				docControlLeadRadioButton.setItemBean(itemBean);

				if (JecnConfigContents.ITEM_USER_CONTROL.equals(value)) {
					docNotControlLeadRadioButton.setSelected(true);
				} else {
					docControlLeadRadioButton.setSelected(true);
				}
			}
		}

		// 过滤隐藏项排序
		List<JecnConfigItemBean> showTableItemList = tableScrollPane
				.getShowItem(configTypeDesgBean.getTableItemList(),
						JecnConfigContents.TYPE_SMALL_ITEM_TASKAPP);
		// 加载数据
		tableScrollPane.initData(showTableItemList);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton) {
			JecnConfigRadioButton btn = (JecnConfigRadioButton) e.getSource();
			if (btn.isSelected()) {// 是否选中
				// 主导发布类型：1：文控审核主导审批发布；0：正常审批发布（拟稿人主导发布）（默认）
				if ("docControlLeadRadioButton".equals(btn.getActionCommand())) {// 文控主导审核发布
					btn.getItemBean().setValue(
							JecnConfigContents.ITEM_DOC_CONTROL);

					// 校验
					check();
				} else if ("docNotControlLeadRadioButton".equals(btn
						.getActionCommand())) {// 拟稿人主导发布
					btn.getItemBean().setValue(
							JecnConfigContents.ITEM_USER_CONTROL);

					// 校验
					check();
				}
			}
		}
	}

	/**
	 * 
	 * 审批环节不能为空；
	 * 
	 * 文控主导审核发布为文控主导，审批环节必须有文控审核人且必填
	 * 
	 * 审批环节存在评审人会审时，评审人会审后必须存在其他审核人，当评审人必填，其后其他审核人至少有一个必填
	 * 
	 * @return boolean 校验成功返回true；校验失败返回false
	 */
	@Override
	public boolean check() {
		List<JecnConfigItemBean> showITemBeanList = tableScrollPane
				.getShowItem(configTypeDesgBean.getTableItemList(),
						configTypeDesgBean.getType());
		if (showITemBeanList == null || showITemBeanList.size() == 0) { // 3.审批环节不能为空
			dialog.updateBtnAndInfo(JecnProperties.getValue("taskApprRowNull"));
			return false;
		}

		if (docControlLeadRadioButton.isSelected()) {// 1.文控主导审核发布为文控主导，审批环节必须有文控审核人
			// 文控审核人
			String taskAppDocCtrlUser = ConfigItemPartMapMark.taskAppDocCtrlUser
					.toString();

			// 存在文控审核人且必填
			JecnConfigItemBean docCtrlItemBean = configTypeDesgBean
					.getConfigItemBeanNotEmptyByMark(showITemBeanList,
							taskAppDocCtrlUser);
			if (docCtrlItemBean == null) {
				dialog.updateBtnAndInfo(JecnProperties
						.getValue("taskApprExistsNotEmptyApprUser"));
				return false;
			}
		}

		// 评审人会审
		String taskAppReviewer = ConfigItemPartMapMark.taskAppReviewer
				.toString();

		// 是否存在评审人且其之后存在审核人
		boolean ret = configTypeDesgBean.getNextItemBeanByMark(
				showITemBeanList, taskAppReviewer);

		if (!ret) {// 不存在/存在且其后不存在审核人
			dialog.updateBtnAndInfo(JecnProperties
					.getValue("taskApprOtherUser"));
			return false;
		}

		// 评审人必填，其后其他审核人至少有一个必填
		ret = configTypeDesgBean.getNextItemBeanNotEmptyByMark(
				showITemBeanList, taskAppReviewer);
		if (!ret) {// 不存在/存在且其后不存在审核人
			dialog.updateBtnAndInfo(JecnProperties
					.getValue("taskApprOtherNotEmptyUser"));
			return false;
		}

		dialog.updateBtnAndInfo(null);
		return true;
	}
}
