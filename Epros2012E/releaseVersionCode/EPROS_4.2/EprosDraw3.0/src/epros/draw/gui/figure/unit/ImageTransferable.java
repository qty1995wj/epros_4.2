/*
 * ImageTransferable.java
 *
 * Created on 2009��7��15��, ����11:13
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package epros.draw.gui.figure.unit;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import javax.swing.ImageIcon;

/**
 * 
 * @author Administrator
 */
public class ImageTransferable extends ImageIcon implements Transferable,
		ClipboardOwner {
	DataFlavor[] flavors;
	ImageIcon copyIcon;

	public void lostOwnership(Clipboard clipboard, Transferable contents) {
	}

	public ImageTransferable(ImageIcon pcopyIcon) {
		flavors = new DataFlavor[1];
		flavors[0] = DataFlavor.imageFlavor;
		copyIcon = pcopyIcon;
	}

	public DataFlavor[] getTransferDataFlavors() {
		return flavors;
	}

	public Object getTransferData(DataFlavor flavor) {
		if (flavor.equals(flavors[0])) {
			return copyIcon.getImage();
		}
		return null;
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		if (flavor.equals(flavors[0]))
			return true;
		return false;
	}
}