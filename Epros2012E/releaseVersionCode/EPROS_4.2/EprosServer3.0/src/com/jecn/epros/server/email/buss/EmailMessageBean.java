package com.jecn.epros.server.email.buss;

import java.io.File;
import java.util.Date;

import com.jecn.epros.server.bean.popedom.JecnUser;
/**
 * 获取邮件信息
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Dec 12, 2012 时间：3:37:46 PM
 */
public class EmailMessageBean {
	/**邮件主题*/
	private String subject = null;// 
	/**邮件发送人地址*/
	private String from = null;// 
	/**邮件发送时间*/
	private Date sendDate = null;// 
	/**邮件正文*/
	private String content = null;// 
	/** 邮件是否有附件*/
	private boolean isAttach = false; //
//	/**邮件收件人地址集合*/
//	private ArrayList<String> recipients = new ArrayList<String>();// 
//	/**邮件抄送人地址集合*/
//	private ArrayList<String> addRrecipients = new ArrayList<String>();// 
//	/** 邮件附件名字集合*/
//	private ArrayList<String> attachNames = new ArrayList<String>(); //
//	/** 邮件附件集合*/
//	private ArrayList<InputStream> attachs = new ArrayList<InputStream>();//
	
	/** 邮件附件 */
	private File runTestFile = null;
	/**附件名称*/
	private String fileName;
	
	/**邮件主表id*/
	private String emailId;
	
	/** 收件人*/
	private JecnUser user;
	
	public JecnUser getUser() {
		return user;
	}
	public void setUser(JecnUser user) {
		this.user = user;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isAttach() {
		return isAttach;
	}

	public void setAttach(boolean isAttach) {
		this.isAttach = isAttach;
	}

//	public ArrayList<String> getRecipients() {
//		return recipients;
//	}
//
//	public ArrayList<String> getAttachNames() {
//		return attachNames;
//	}
//
//	public void setAttachNames(ArrayList<String> attachNames) {
//		this.attachNames = attachNames;
//	}
//
//	public ArrayList<InputStream> getAttachs() {
//		return attachs;
//	}
//
//	public void setAttachs(ArrayList<InputStream> attachs) {
//		this.attachs = attachs;
//	}
//
//	/** 添加邮件收件人地址集合 */
//	public void addRecipients(String recipient) {
//		if(recipient==null || recipient.length()==0){
//			return ;
//		}
//		this.recipients.add(recipient);
//	}
//
//	/** 添加邮件附件名字集合 */
//	public void addAttachNames(String attachName) {
//		this.attachNames.add(attachName);
//	}
//
//	/** 添加邮件附件集合 */
//	public void addAttachs(InputStream attachs) {
//		this.attachs.add(attachs);
//	}
//
//	public ArrayList<String> getAddRrecipients() {
//		return addRrecipients;
//	}

	public File getRunTestFile() {
		return runTestFile;
	}

	public void setRunTestFile(File runTestFile) {
		this.runTestFile = runTestFile;
	}

	/*
	 * public String getFilename() { return filename; }
	 * 
	 * public void setFilename(String filename) { this.filename = filename; }
	 * 
	 * public void addAttachfile(String fname) { file.addElement(fname); }
	 * 
	 * public Vector getFile() { return file; }
	 * 
	 * public void setFile(Vector file) { this.file = file; }
	 */

}
