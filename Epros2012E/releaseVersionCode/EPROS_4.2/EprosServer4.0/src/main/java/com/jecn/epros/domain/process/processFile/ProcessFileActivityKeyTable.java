package com.jecn.epros.domain.process.processFile;

import java.util.List;

/**
 * 关键活动表格
 *
 * @author ZXH
 */
public class ProcessFileActivityKeyTable extends BaseProcessFileBean {
    private List<ProcessFileTable> contents = null;

    public List<ProcessFileTable> getContents() {
        return contents;
    }

    public void setContents(List<ProcessFileTable> contents) {
        this.contents = contents;
    }

}
