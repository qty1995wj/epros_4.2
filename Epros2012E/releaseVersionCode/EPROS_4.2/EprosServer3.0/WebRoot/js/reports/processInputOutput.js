function processInputOutputGrid() {
	processInputOutputGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "processInputOutput.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "processId",
								mapping : "processId"
							}, {
								name : "processName",
								mapping : "processName"
							}, {
								name : "upstreamProcessName",
								mapping : "upstreamProcessName"
							}, {
								name : "upstreamProcessCount",
								mapping : "upstreamProcessCount"
							}, {
								name : "downstreamProcessName",
								mapping : "downstreamProcessName"
							}, {
								name : "downstreamProcessCount",
								mapping : "downstreamProcessCount"
							}])

			});

	processInputOutputGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "processName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processInputOutputNameRenderer
			}, {
				header : i_upstreamProcess,
				dataIndex : "upstreamProcessName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_count,
				dataIndex : "upstreamProcessCount",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_downstreamProcess,
				dataIndex : "downstreamProcessName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_count,
				dataIndex : "downstreamProcessCount",
				align : "center",
				sortable : true,
				menuDisabled : true
			}
	]);
	processInputOutputGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processInputOutputGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processInputOutputGridPanel = new Ext.grid.GridPanel({
				id : "processInputOutputGridPanel_id",
				renderTo : 'processInputOutputGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-145,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processInputOutputGridStore,
				colModel : processInputOutputGridColumnModel,
				bbar : processInputOutputGridBbar
			})
	reports_index_id='reports_processInputOutput_id';
}


/**
 * 流程输入输出
 */
function processInputOutputGridSearch(){
	 //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_input_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_input_id").value;
	 if(resOrgId&&resOrgId!=""){
     	processInputOutputGridStore.setBaseParam("resOrgId", resOrgId);
     }
     if(processId&&processId!=""){
     	processInputOutputGridStore.setBaseParam("processId", processId);
     }
	 processInputOutputGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
}

/**
 * 流程输入输出重置
 **/
function processInputOutputSearchReset(){
        document.getElementById("orgDutyName_input_id").value = "";
		document.getElementById("processName_input_id").value = "";
		document.getElementById("orgDutyId_input_id").value = "-1";
		document.getElementById("processId_input_id").value = "-1";
}


/**
 * 流程名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processInputOutputNameRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['processId']+"' >"+value+"</a>";
}

// 流程输入输出下载
function processInputOutputDownloadExcel(){
    //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_input_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_input_id").value;
	 window.location.href = "downloadProcessInputOutput.action?resOrgId="+resOrgId+"&processId="+processId;
}
