package com.jecn.epros.email;

import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.util.JecnProperties;

public class ProcessExpiryDateEmailBuilder extends FixedContentEmailBuilder {

    @Override
    protected EmailBasicInfo getEmailBasicInfo() {
        EmailBasicInfo basicInfo = new EmailBasicInfo();
        Object[] data = getData();
        String flowName = data[0].toString();
        String endScanTime = data[1].toString();
        String subject = JecnProperties.getValue("ProcessValidityPrompt");//流程有效期提示！
        String content =
        "贵部的《" + flowName + "》流程计划于" + endScanTime + "之前完成审视优化，请在截止日期前完成《" + flowName + "》流程的审视并发布！";
        basicInfo.setSubject(subject);
        basicInfo.setContent(content);
        return basicInfo;
    }

}
