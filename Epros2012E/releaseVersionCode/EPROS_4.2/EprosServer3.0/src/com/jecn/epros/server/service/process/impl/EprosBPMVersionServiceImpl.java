package com.jecn.epros.server.service.process.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.EprosBPMVersion;
import com.jecn.epros.server.dao.process.IEprosBPMVersionDao;
import com.jecn.epros.server.service.process.IEprosBPMVersionService;

@Transactional(rollbackFor = Exception.class)
public class EprosBPMVersionServiceImpl implements IEprosBPMVersionService {

	private IEprosBPMVersionDao bpmVersionDao;

	@Override
	public List<EprosBPMVersion> findEprosBpmVersions(String flowId) throws Exception {
		return bpmVersionDao.findEprosBpmVersions(flowId);
	}

	public IEprosBPMVersionDao getBpmVersionDao() {
		return bpmVersionDao;
	}

	public void setBpmVersionDao(IEprosBPMVersionDao bpmVersionDao) {
		this.bpmVersionDao = bpmVersionDao;
	}

}
