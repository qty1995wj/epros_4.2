package epros.draw.gui.box;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 
 * 新建模板和标题
 * 
 * @author ZHOUXY
 * 
 */
public class JecnNewUpdateTemplateDialog extends JecnDialog implements ActionListener, CaretListener {

	/** 资源文件管理对象 */
	private JecnResourceUtil resourceManager = null;
	/** 内容面板 */
	private JecnPanel mainPanel = null;
	/** 名称标签 */
	private JLabel label = null;
	/** 流程地图名称 */
	private JTextField nameTextField = null;
	/** 提示信息框 */
	private JLabel infoLabel = null;
	/** 确认取消按钮面板 */
	private JecnOKCancelPanel okCancelPanel = null;
	/** 名称内容 */
	private String name = null;
	/** 模板数据 */
	private JecnTemplateData templateData = null;
	/** 必填提示 */
	private JLabel lab = null;

	public JecnNewUpdateTemplateDialog(JecnTemplateData templateData) {
		this.templateData = templateData;
		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 实例化资源文件管理对象
		resourceManager = JecnResourceUtil.getJecnResourceUtil();

		// 内容面板
		mainPanel = new JecnPanel();
		// 名称标签
		if (templateData != null && templateData.getTemplateName() != name) {
			this.setTitle(resourceManager.getValue("edit"));
		} else {
			this.setTitle(resourceManager.getValue("template"));
		}
		label = new JLabel(resourceManager.getValue("templateNameC"));
		// 流程地图名称
		nameTextField = new JTextField();
		// 提示信息框
		infoLabel = new JLabel(" ");
		// 确认取消按钮面板
		okCancelPanel = new JecnOKCancelPanel();

		lab = new JLabel();
		lab.setForeground(Color.red);
		lab.setText("*");
		lab.setHorizontalAlignment(SwingConstants.LEFT);

		this.getContentPane().add(mainPanel);

		// 大小
		Dimension size = new Dimension(375, 125);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setSize(size);
		// 位置
		this.setLocationRelativeTo(null);
		// 模态
		this.setModal(true);
		// 不可调整大小
		// this.setResizable(false);

		// 大小
		Dimension contentSize = new Dimension(175, 20);
		// 流程地图名称
		nameTextField.setPreferredSize(contentSize);
		nameTextField.setMinimumSize(contentSize);

		if (templateData != null) {
			nameTextField.setText(templateData.getTemplateName());
		}

		// 提示信息框背景颜色和前景颜色
		infoLabel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoLabel.setForeground(Color.red);

		// 事件
		nameTextField.addCaretListener(this);
		okCancelPanel.getOkBtn().addActionListener(this);
		okCancelPanel.getCancelBtn().addActionListener(this);
	}

	/**
	 * 
	 * 
	 * 
	 */
	private void initLayout() {
		// 内容面板new Insets(20, 25, 0, 30)
		Insets labelInsets = new Insets(20, 25, 0, 0);
		Insets nameInsets = new Insets(20, 3, 0, 2);
		Insets okCancelPanelInsets = new Insets(5, 0, 0, 30);
		// 名称标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, labelInsets, 0, 0);
		mainPanel.add(label, c);
		// 片段名称
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				nameInsets, 0, 0);
		mainPanel.add(nameTextField, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				15, 0, 0, 10), 0, 0);
		mainPanel.add(lab, c);

		// 提示信息框
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(15, 0, 0, 10), 0, 0);
		mainPanel.add(infoLabel, c);
		// 空闲区域
		c = new GridBagConstraints(0, 2, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				nameInsets, 0, 0);
		mainPanel.add(new JLabel(), c);

		// 确认取消按钮面板
		c = new GridBagConstraints(0, 3, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				okCancelPanelInsets, 0, 0);
		mainPanel.add(okCancelPanel, c);

	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	/**
	 * 
	 * 
	 * 按钮动作事件
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	private void actionPerformedProcess(ActionEvent e) {
		if (okCancelPanel.getOkBtn() == e.getSource()) {// 确认按钮
			boolean ret = checkName();
			if (ret) {
				name = nameTextField.getText();
				if (name != null) {
					name = name.trim();
				}
				// 如果传入的名称不为空 证明此操作为修改
				if (templateData != null) {
					if (!templateData.getTemplateName().equals(name)) {
						// 修改xml 是否成功的标识
						boolean updateFlag = true;
						// 修改XML 中的数据
						updateFlag = JecnFlowTemplate.updateXmlNode(getWorkflow().getFlowMapData().getMapType(),
								templateData.getTemplateId(), name);
						// 修改xml成功后执行下面的修改
						if (updateFlag) {
							// 循环所有数据 找到需要修改的数据 修改名称
							templateData.setTemplateName(name);
						}
						// 获取数据对应的按钮
						JecnToolBar toolBar = JecnFlowTemplate.getToolBarToTemplateData(
								JecnSystemData.getTemplateMap(), templateData.getTemplateId());
						if (toolBar.getComponents().length > 0) {
							JecnTemplateBoxButton templateBoxButton = (JecnTemplateBoxButton) toolBar.getComponents()[0];
							// 修改toggleButton显示内容
							templateBoxButton.setText(name);
							// 刷新toggleButton
							templateBoxButton.updateUI();
						}
					}
					// 修改完成后 重置数据为空
					this.templateData = null;
				} else {
					if (getWorkflow().getFlowMapData().getMapType() == MapType.partMap) {
						int flow = JecnDesignerProcess.getDesignerProcess().getModelUpperLimit(1);
						if (JecnSystemData.getPartMapCount() >= flow) {
							infoLabel.setText(resourceManager.getValue("partMap") + flow
									+ resourceManager.getValue("mostModels"));
							return;
						}
					} else if (getWorkflow().getFlowMapData().getMapType() == MapType.totalMap) {
						int map = JecnDesignerProcess.getDesignerProcess().getModelUpperLimit(0);
						if (JecnSystemData.getTotalMapCount() >= map) {
							infoLabel.setText(resourceManager.getValue("totalMap") + map
									+ resourceManager.getValue("mostModels"));
							return;
						}
					}
					// 调用导出的方法
					JecnTemplateData templateData = JecnFlowTemplate.exportPartData(name, getWorkflow()
							.getFlowMapData().getMapType());

					if (templateData != null) {
						JecnScrollPaneComponentAdapter scrollPaneComponentAdapter = new JecnScrollPaneComponentAdapter();
						MapType mapType = getWorkflow().getFlowMapData().getMapType();
						if (mapType == MapType.partMap) {
							// 调用添加方法
							getJecnTemplateScrollPane().getTemplateButton(
									getJecnTemplateScrollPane().getTemplatePanel(), templateData);
							// 设置面板大小
							scrollPaneComponentAdapter.setTemplateScrollPaneSize(getJecnTemplateScrollPane());
							getJecnTemplateScrollPane().getTemplatePanel().updateUI();

						} else if (mapType == MapType.totalMap || mapType == MapType.totalMapRelation) {
							// 调用添加方法
							getTotalMapModelScrollPane().getTemplateButton(
									getTotalMapModelScrollPane().getTemplatePanel(), templateData);
							// 设置面板大小
							scrollPaneComponentAdapter.setTemplateScrollPaneSize(getTotalMapModelScrollPane());
							getTotalMapModelScrollPane().getTemplatePanel().updateUI();
						}
					} else {
						this.dispose();
					}
				}
				this.setVisible(false);
			}
		} else if (okCancelPanel.getCancelBtn() == e.getSource()) {// 取消按钮
			name = null;
			this.setVisible(false);
		}
	}

	/**
	 * 获取面板
	 * 
	 * @return
	 */
	private JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 获取JecnTemplateScrollPane
	 * 
	 * @return
	 */
	private JecnTemplateScrollPane getJecnTemplateScrollPane() {
		return JecnDrawMainPanel.getMainPanel().getBoxPanel().getPartMapModelScrollPane();
	}

	private JecnTemplateScrollPane getTotalMapModelScrollPane() {
		return JecnDrawMainPanel.getMainPanel().getBoxPanel().getTotalMapModelScrollPane();
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	private void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == nameTextField) {// 流程地图名称
			checkName();
		}
	}

	/**
	 * 
	 * 在名称输入框中输入字符时，验证内容是否正确
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	private boolean checkName() {

		String text = nameTextField.getText();

		// 验证数据
		String checkString = JecnUserCheckUtil.checkNullName(text);

		if (!DrawCommon.isNullOrEmtryTrim(checkString)) {
			// resourceManager.getValue("name")
			infoLabel.setText(checkString);

			return false;
		}
		// 修改
		if (templateData != null) {
			if (this.templateData.getTemplateName().equals(text)) {
				infoLabel.setText(" ");
				return true;
			}
		}
		for (JecnTemplateData templateData : JecnSystemData.getTemplateDataList()) {
			if (getWorkflow().getFlowMapData().getMapType().toString().equals(templateData.getMapType())) {
				if (text != null && text.trim().equals(templateData.getTemplateName())) {
					infoLabel.setText(resourceManager.getValue("nameRepeat"));
					return false;

				}
			}
		}
		infoLabel.setText(" ");
		return true;
	}
}
