package epros.designer.gui.integration.activity;

import java.awt.Insets;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-10-31 时间：上午11:13:58
 */
public class EditActivityTypeDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditActivityTypeDialog.class);
	private JecnActiveTypeBean activeTypeBean = null;
	/** 编辑成功：True成功 */
	protected boolean isOperation = false;
	private Long typeId;

	public EditActivityTypeDialog(Long typeId, JecnActiveTypeBean activeTypeBean) {
		this.typeId = typeId;
		this.activeTypeBean = activeTypeBean;
		this.setLocationRelativeTo(null);
		this.setName(activeTypeBean.getTypeName());
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("editBtn");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("contentC");
	}

	@Override
	public void saveData() {
		activeTypeBean.setTypeName(this.getName());
		try {
			ConnectionPool.getActivityAction().addActivityType(activeTypeBean);
			isOperation = true;
			this.dispose();
		} catch (Exception e) {
			log.error("EditActivityTypeDialog saveData is error", e);
		}

	}

	protected void addEnNamePanel(Insets insets) {

	}

	protected void validateEnglishName() {

	}
	/**
	 * 验证是否重名
	 * 
	 */
	@Override
	public boolean validateNodeRepeat(String name) {
		try {
			boolean isSame = ConnectionPool.getActivityAction()
					.isSameActivityType(name);
			if (isSame) {// True:存在相同名称
				return true;
			}
		} catch (Exception e) {
			log.error("EditActivityTypeDialog validateNodeRepeat is error", e);
		}
		return false;
	}

}
