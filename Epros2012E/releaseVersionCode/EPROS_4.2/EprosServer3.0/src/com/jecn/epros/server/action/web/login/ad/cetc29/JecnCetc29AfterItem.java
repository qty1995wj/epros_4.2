package com.jecn.epros.server.action.web.login.ad.cetc29;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

/**
 * 29所配置文件读取类（EprosServer3.0/src/cfgFile/cetc29/cetc20LdapServerConfig.
 * properties）
 * 
 */
public class JecnCetc29AfterItem {

	private static Logger log = Logger.getLogger(JecnCetc29AfterItem.class);

	// Ldap上下文工厂
	public static String LDAP_CONTEXT_FACTORY = null;
	// Ldap 服务器地址
	public static String LDAP_URL = null;
	// Ldap 权限验证类型
	public static String AUTH_TYPE = null;
	// Ldap epros用户组基准DN
	public static String EPROS_GROUP_DN = null;
	// Ldap 用户组基准DN
	public static String PEOPLE_DN = null;
	// 单点登录时代表登录名称的参数名
	public static String LOGIN_NAME = null;

	/****** 组织同步数据初始化 ****************/
	public static String USER_NAME = null;
	public static String PWD = null;
	public static String RC_CODE = null;
	/** 部门字段 */
	public static String FIELD_STR_DEPT = null;
	public static String QUERY_STR_DEPT = null;
	/** 人员字段 */
	public static String FIELD_STR_USER = null;
	public static String QUERY_STR_USER = null;
	public static String WSDL_URL = null;

	/** XML 节点类型名称 */
	public static String DEPT_NODE_NAME = null;
	public static String USER_NODE_NAME = null;
	/** xml字段 */
	public static String XML_FIELD_USER = null;
	public static String XML_FIELD_DEPT = null;
	public static String DEPT_ROOT_NODE = null;

	/**
	 * 读取29所配置文件
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午02:38:20
	 */
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/cetc29/cetc20LdapServerConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			// Ldap上下文工厂
			String ldapCtxFactory = props.getProperty("contextFactory");
			if (!JecnCommon.isNullOrEmtryTrim(ldapCtxFactory)) {
				JecnCetc29AfterItem.LDAP_CONTEXT_FACTORY = ldapCtxFactory;
			}

			// Ldap 服务器地址
			String ldapURL = props.getProperty("ldapURL");
			if (!JecnCommon.isNullOrEmtryTrim(ldapURL)) {
				JecnCetc29AfterItem.LDAP_URL = ldapURL;
			}

			// Ldap 权限验证类型
			String authType = props.getProperty("authType");
			if (!JecnCommon.isNullOrEmtryTrim(authType)) {
				JecnCetc29AfterItem.AUTH_TYPE = authType;
			}

			// Ldap epros用户组基准DN
			String eprosGroupDN = props.getProperty("eprosGroupDN");
			if (!JecnCommon.isNullOrEmtryTrim(eprosGroupDN)) {
				JecnCetc29AfterItem.EPROS_GROUP_DN = eprosGroupDN;
			}
			// Ldap 用户基准DN
			String pepleDN = props.getProperty("peopleDN");
			if (!JecnCommon.isNullOrEmtryTrim(pepleDN)) {
				JecnCetc29AfterItem.PEOPLE_DN = pepleDN;
			}
			// 单点登录时代表登录名称的参数名
			String loginName = props.getProperty("loginName");
			if (!JecnCommon.isNullOrEmtryTrim(loginName)) {
				JecnCetc29AfterItem.LOGIN_NAME = loginName;
			}

			// 组织同步
			USER_NAME = props.getProperty("username");
			PWD = props.getProperty("pwd");
			RC_CODE = props.getProperty("rcCode");
			// 部门字段
			FIELD_STR_DEPT = props.getProperty("fieldStrDept");
			QUERY_STR_DEPT = props.getProperty("queryStrDept");
			// 人员字段
			FIELD_STR_USER = props.getProperty("fieldStrUser");
			QUERY_STR_USER = props.getProperty("queryStrUser");
			WSDL_URL = props.getProperty("wsdlURL");
			DEPT_NODE_NAME = props.getProperty("deptNodeName");
			USER_NODE_NAME = props.getProperty("userNodeName");

			// xml字段
			XML_FIELD_USER = props.getProperty("xmlFieldUser");
			XML_FIELD_DEPT = props.getProperty("xmlFieldDept");
			DEPT_ROOT_NODE = props.getProperty("deptRootNode");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}
}
