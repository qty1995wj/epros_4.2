package com.jecn.epros.domain.process;

/**
 * 流程搜索bean
 *
 * @author ZXH
 */
public class SearchBean {

    private String name; //
    private String number;
    private Long orgId;
    /**
     * 0是秘密、1是公开
     */
    private int secret = -1;

    /***
     *
     * -1是全部，1是流程，0是流程架构，2文件，3制度
     */
    private int fileType;

    public int getSecret() {
        return secret;
    }

    public void setSecret(int secret) {
        this.secret = secret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }
}
