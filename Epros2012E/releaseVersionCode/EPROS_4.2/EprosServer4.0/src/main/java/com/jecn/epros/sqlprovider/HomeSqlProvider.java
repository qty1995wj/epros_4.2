package com.jecn.epros.sqlprovider;

import com.jecn.epros.service.ParamUtils;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.SqlCommon;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 我的主页 sql
 *
 * @author ZXH
 */
public class HomeSqlProvider extends BaseSqlProvider {

    public String findRoleRelateFlowByPostIdsSql(Map<String, Object> maps) {
        Long posId = Long.valueOf(maps.get("posId").toString());
        Long projectId = (Long) maps.get("projectId");
        return "SELECT * FROM ( SELECT ROLE_FSI.FIGURE_ID   roleId," +
                "       ROLE_FSI.FIGURE_TEXT roleName," +
                "       ROLE_FSI.ROLE_RES roleRes," +
                "       FS.FLOW_ID           flowId," +
                "       FS.FLOW_NAME         flowName," +
                "       POST_OI.FIGURE_ID    postId," +
                "       PARENT_FS.FLOW_ID    architectureId," +
                "       PARENT_FS.FLOW_NAME  architectureName," +
                "       PARENT_FS.ISFLOW     architectureType" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE ROLE_FSI" +
                "  LEFT JOIN PROCESS_STATION_RELATED PRS" +
                "    ON PRS.FIGURE_FLOW_ID = ROLE_FSI.FIGURE_ID" +
                "   AND PRS.TYPE = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE POST_OI" +
                "    ON POST_OI.FIGURE_ID = PRS.FIGURE_POSITION_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JUP" +
                "    ON POST_OI.FIGURE_ID = JUP.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON ROLE_FSI.FLOW_ID = FS.FLOW_ID" +
                "   AND FS.PROJECTID = " + projectId +
                "  LEFT JOIN JECN_FLOW_STRUCTURE PARENT_FS" +
                "    ON PARENT_FS.FLOW_ID = FS.PRE_FLOW_ID" +
                " WHERE JUP.FIGURE_ID = " + posId +
                "   AND POST_OI.FIGURE_ID IS NOT NULL   AND FS.DEL_STATE = 0 " +
                " UNION ALL " +
                "SELECT ROLE_FSI.FIGURE_ID   roleId," +
                "       ROLE_FSI.FIGURE_TEXT roleName," +
                "       ROLE_FSI.ROLE_RES roleRes," +
                "       FS.FLOW_ID           flowId," +
                "       FS.FLOW_NAME         flowName," +
                "       POST_OI.FIGURE_ID    postId," +
                "       PARENT_FS.FLOW_ID    architectureId," +
                "       PARENT_FS.FLOW_NAME  architectureName," +
                "       PARENT_FS.ISFLOW     architectureType" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE ROLE_FSI" +
                "  LEFT JOIN PROCESS_STATION_RELATED PRS" +
                "    ON PRS.FIGURE_FLOW_ID = ROLE_FSI.FIGURE_ID" +
                "   AND PRS.TYPE = 1" +
                "  LEFT JOIN JECN_POSITION_GROUP_R PGR" +
                "    ON PGR.GROUP_ID = PRS.FIGURE_POSITION_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE POST_OI" +
                "    ON POST_OI.FIGURE_ID = PGR.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JUP" +
                "    ON POST_OI.FIGURE_ID = JUP.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON ROLE_FSI.FLOW_ID = FS.FLOW_ID" +
                "   AND FS.PROJECTID = " + projectId +
                "   AND FS.DEL_STATE = 0" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE PARENT_FS" +
                "    ON PARENT_FS.FLOW_ID = FS.PRE_FLOW_ID" +
                " WHERE JUP.FIGURE_ID = " + posId +
                "   AND POST_OI.FIGURE_ID IS NOT NULL    AND FS.DEL_STATE = 0 ) T ";
    }


    public String findRoleRelateFlowByPostIdsSqlCount(Map<String, Object> maps) {
        Long posId = Long.valueOf(maps.get("posId").toString());
        Long projectId = (Long) maps.get("projectId");
        return "SELECT COUNT(*) FROM(SELECT ROLE_FSI.FIGURE_ID   roleId," +
                "       ROLE_FSI.FIGURE_TEXT roleName," +
                "       ROLE_FSI.ROLE_RES roleRes," +
                "       FS.FLOW_ID           flowId," +
                "       FS.FLOW_NAME         flowName," +
                "       POST_OI.FIGURE_ID    postId," +
                "       PARENT_FS.FLOW_ID    architectureId," +
                "       PARENT_FS.FLOW_NAME  architectureName," +
                "       PARENT_FS.ISFLOW     architectureType" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE ROLE_FSI" +
                "  LEFT JOIN PROCESS_STATION_RELATED PRS" +
                "    ON PRS.FIGURE_FLOW_ID = ROLE_FSI.FIGURE_ID" +
                "   AND PRS.TYPE = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE POST_OI" +
                "    ON POST_OI.FIGURE_ID = PRS.FIGURE_POSITION_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JUP" +
                "    ON POST_OI.FIGURE_ID = JUP.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON ROLE_FSI.FLOW_ID = FS.FLOW_ID" +
                "   AND FS.PROJECTID = " + projectId +
                "  LEFT JOIN JECN_FLOW_STRUCTURE PARENT_FS" +
                "    ON PARENT_FS.FLOW_ID = FS.PRE_FLOW_ID" +
                " WHERE JUP.FIGURE_ID = " + posId +
                "   AND POST_OI.FIGURE_ID IS NOT NULL   AND FS.DEL_STATE = 0 " +
                " UNION ALL " +
                "SELECT ROLE_FSI.FIGURE_ID   roleId," +
                "       ROLE_FSI.FIGURE_TEXT roleName," +
                "       ROLE_FSI.ROLE_RES roleRes," +
                "       FS.FLOW_ID           flowId," +
                "       FS.FLOW_NAME         flowName," +
                "       POST_OI.FIGURE_ID    postId," +
                "       PARENT_FS.FLOW_ID    architectureId," +
                "       PARENT_FS.FLOW_NAME  architectureName," +
                "       PARENT_FS.ISFLOW     architectureType" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE ROLE_FSI" +
                "  LEFT JOIN PROCESS_STATION_RELATED PRS" +
                "    ON PRS.FIGURE_FLOW_ID = ROLE_FSI.FIGURE_ID" +
                "   AND PRS.TYPE = 1" +
                "  LEFT JOIN JECN_POSITION_GROUP_R PGR" +
                "    ON PGR.GROUP_ID = PRS.FIGURE_POSITION_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE POST_OI" +
                "    ON POST_OI.FIGURE_ID = PGR.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JUP" +
                "    ON POST_OI.FIGURE_ID = JUP.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON ROLE_FSI.FLOW_ID = FS.FLOW_ID" +
                "   AND FS.PROJECTID = " + projectId +
                "   AND FS.DEL_STATE = 0" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE PARENT_FS" +
                "    ON PARENT_FS.FLOW_ID = FS.PRE_FLOW_ID" +
                " WHERE JUP.FIGURE_ID = " + posId +
                "   AND POST_OI.FIGURE_ID IS NOT NULL    AND FS.DEL_STATE = 0 )T";
    }

    /**
     * 角色相关活动
     *
     * @param maps
     * @return
     */
    public String findActiveByRoleListIdsSql(Map<String, Object> maps) {
        List<Long> roleIds = (List<Long>) maps.get(ParamUtils.DEFAULT_COLLECTION_KEY);
        return "SELECT FSI.FIGURE_ID, FSI.ACTIVITY_ID ACTIVITY_NUM, FSI.FIGURE_TEXT, RA.FIGURE_ROLE_ID"
                + "  FROM JECN_ROLE_ACTIVE RA" + " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE FSI"
                + "    ON FSI.FIGURE_ID = RA.FIGURE_ACTIVE_ID" + " INNER JOIN JECN_FLOW_STRUCTURE FS"
                + "    ON FS.FLOW_ID = FSI.FLOW_ID" + " WHERE RA.FIGURE_ROLE_ID IN " + SqlCommon.getIds(roleIds)
                + "  ORDER BY RA.FIGURE_ROLE_ID, FSI.ACTIVITY_ID";
    }

    /**
     * 流程相关制度
     *
     * @param maps
     * @return
     */
    public String findFlowRelatedRulesSql(Map<String, Object> maps) {
        @SuppressWarnings("unchecked")
        Set<Long> flowIds = (Set<Long>) maps.get(ParamUtils.DEFAULT_COLLECTION_KEY);
        return "SELECT FRC.FLOW_ID, R.ID, R.RULE_NAME NAME" + "  FROM FLOW_RELATED_CRITERION FRC, JECN_RULE R"
                + " WHERE FRC.CRITERION_CLASS_ID = R.ID AND R.DEL_STATE=0 AND FRC.FLOW_ID IN "
                + SqlCommon.getIdsSet(flowIds);
    }

    /**
     * 流程相关标准
     *
     * @param maps
     * @return
     */
    public String findFlowRelatedStandardsSql(Map<String, Object> maps) {
        @SuppressWarnings("unchecked")
        Set<Long> flowIds = (Set<Long>) maps.get(ParamUtils.DEFAULT_COLLECTION_KEY);
        return "SELECT FRC.FLOW_ID,C.CRITERION_CLASS_ID ID,C.CRITERION_CLASS_NAME NAME"
                + "  FROM JECN_FLOW_STANDARD FRC, JECN_CRITERION_CLASSES C"
                + " WHERE FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID" + "   AND FRC.FLOW_ID IN "
                + SqlCommon.getIdsSet(flowIds);
    }

    /**
     * 活动相关标准
     *
     * @param maps
     * @return
     */
    public String findActivityRelatedStandardsSql(Map<String, Object> maps) {
        @SuppressWarnings("unchecked")
        Set<Long> flowIds = (Set<Long>) maps.get(ParamUtils.DEFAULT_COLLECTION_KEY);
        return "SELECT SI.FLOW_ID," + "       C.CRITERION_CLASS_ID ID," + "       C.CRITERION_CLASS_NAME NAME"
                + "  FROM JECN_FLOW_STRUCTURE_IMAGE SI," + "       JECN_ACTIVE_STANDARD      JAS,"
                + "       JECN_CRITERION_CLASSES    C" + " WHERE JAS.ACTIVE_ID = SI.FIGURE_ID"
                + "   AND SI.FIGURE_TYPE IN" + SqlCommon.getActiveString()
                + "   AND JAS.STANDARD_ID = C.CRITERION_CLASS_ID" + "   AND SI.FLOW_ID IN "
                + SqlCommon.getIdsSet(flowIds);
    }

    /**
     * 流程相关风险
     *
     * @param maps
     * @return
     */
    public String findFlowRelatedRisksSql(Map<String, Object> maps) {
        @SuppressWarnings("unchecked")
        Set<Long> flowIds = (Set<Long>) maps.get(ParamUtils.DEFAULT_COLLECTION_KEY);
        return "SELECT F.FLOW_ID, R.ID, R.NAME" + "  FROM JECN_FLOW_BASIC_INFO      F,"
                + "       JECN_FLOW_STRUCTURE_IMAGE I," + "       JECN_CONTROL_POINT        P,"
                + "       JECN_RISK                 R" + " WHERE F.FLOW_ID = I.FLOW_ID"
                + "   AND P.ACTIVE_ID = I.FIGURE_ID" + "   AND P.RISK_ID = R.ID" + "   AND F.FLOW_ID IN"
                + SqlCommon.getIdsSet(flowIds);
    }

    /**
     * 获取人员岗位名称sql
     *
     * @param peopleId
     * @return
     */
    public String findPostNameByPeopleIdSql(Long peopleId) {
        return "SELECT JFOI.FIGURE_ID, JFOI.FIGURE_TEXT" + "  FROM JECN_USER_POSITION_RELATED JUPR"
                + " INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + "    ON JUPR.FIGURE_ID = JFOI.FIGURE_ID"
                + " WHERE JUPR.PEOPLE_ID = " + peopleId;
    }

    /**
     * 流程访问Top10
     *
     * @param map
     * @return
     */
    public String findFlowVisitTopTen(Map<String, Object> map) {
        return "select jfs.flow_id as file_id,jfs.flow_name as file_name,0 as file_type,count(joi.id) as total_count"
                + "     from jecn_opr_log joi,jecn_flow_structure jfs"
                + "     Inner join "
                + CommonSqlTPath.INSTANCE.getFileSearch(map) + " on MY_AUTH_FILES.Flow_id=jfs.flow_id"
                + "       where jfs.flow_id=joi.related_id and joi.related_type=0"
                + "       group by jfs.flow_id,jfs.flow_name";
    }


    /**
     * 流程合理化建议Top10
     *
     * @param map
     * @return
     */
    public String findFlowProposeTopTen(Map<String, Object> map) {
        return "SELECT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,0 AS FILE_TYPE,COUNT(JPR.ID) AS TOTAL_COUNT" +
                "     FROM JECN_RTNL_PROPOSE JPR,JECN_FLOW_STRUCTURE JFS" +
                "     Inner join " +
                CommonSqlTPath.INSTANCE.getFileSearch(map) + " on MY_AUTH_FILES.Flow_id=jfs.flow_id" +
                "       WHERE JFS.FLOW_ID=JPR.RELATION_ID AND JPR.RELATION_TYPE=0" +
                "       GROUP BY JFS.FLOW_ID,JFS.FLOW_NAME";
    }
}
