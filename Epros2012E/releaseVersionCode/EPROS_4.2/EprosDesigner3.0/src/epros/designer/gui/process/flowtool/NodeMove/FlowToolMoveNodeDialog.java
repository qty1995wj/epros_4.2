package epros.designer.gui.process.flowtool.NodeMove;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/***
 * 支持工具节点移动
 * 
 * @author 2012-06-19
 * 
 */
public class FlowToolMoveNodeDialog extends JecnMoveChooseDialog {
	private static Logger log = Logger.getLogger(FlowToolMoveNodeDialog.class);

	public FlowToolMoveNodeDialog(List<JecnTreeNode> listMoveNodes, JecnTree jTree) {
		super(listMoveNodes, jTree);
	}

	@Override
	public JecnTree getJecnTree() {
		// if (JecnConstants.isMysql) {
		// return new RoutineFlowToolMoveTree(this.getListIds());
		// } else {
		return new HighEfficiencyFlowToolMoveTree(getListIds());
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("flowTool"));
		return title;
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		try {
			ConnectionPool.getFlowTool().moveSustainTools(ids, pid, JecnUtil.getUserId());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("FlowToolMoveNodeDialog savaData is error", e);
		}
		return false;
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = ConnectionPool.getFlowTool().getChildSustainTools(pid);
			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}

				}
			}
		} catch (Exception e) {
			log.error("FlowToolMoveNodeDialog validateName is error", e);
			return false;

		}
		return false;
	}
}
