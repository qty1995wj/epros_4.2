package epros.draw.visio;

/**
 * 
 * visio字体样式bean
 * 
 * @author Administrator
 *
 */
public class JecnMasterDataBean {
	/** master ID */
	private String masterId;
	/** master NameU */
	private String nameU;
	/** 节点下 默认字体的样式 */
	private JecnVisioStyleDateBean visioStyleDateBean;

	public String getMasterId() {
		return masterId;
	}

	public void setMasterId(String masterId) {
		this.masterId = masterId;
	}

	public String getNameU() {
		return nameU;
	}

	public void setNameU(String nameU) {
		this.nameU = nameU;
	}

	public JecnVisioStyleDateBean getVisioStyleDateBean() {
		return visioStyleDateBean;
	}

	public void setVisioStyleDateBean(JecnVisioStyleDateBean visioStyleDateBean) {
		this.visioStyleDateBean = visioStyleDateBean;
	}
}
