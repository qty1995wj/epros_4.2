package epros.designer.gui.rule.mode;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.OldJecnMoveChooseDialog;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class RoutineRuleModeMoveTree extends JecnRoutineTree {
	private Logger log = Logger.getLogger(RoutineRuleModeMoveTree.class);
	/***/
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();

	private OldJecnMoveChooseDialog jecnChooseDialog;

	public RoutineRuleModeMoveTree(List<Long> listIds,OldJecnMoveChooseDialog jecnChooseDialog){
		super(listIds);
		this.listIds = listIds;
		this.jecnChooseDialog = jecnChooseDialog;
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.ruleModeRoot,JecnProperties.getValue("ruleMode"));//"制度模板"
		try {
			list=ConnectionPool.getRuleModeAction().getAllRuleModeDir();
			JecnTreeCommon.addNLevelMoveNodes(list, rootNode, listIds);
		} catch (Exception e) {
			log.error("RoutineRuleModeMoveTree getTreeModel is error",e);
		}
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		jecnChooseDialog.jTreeMousePressed(evt);

	}

}
