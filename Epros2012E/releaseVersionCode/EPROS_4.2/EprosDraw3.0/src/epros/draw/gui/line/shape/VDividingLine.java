package epros.draw.gui.line.shape;

import java.awt.Point;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.gui.line.JecnBaseDividingLinePanel;

/**
 * 
 * 竖线
 * 
 * @author ZHANGXH
 *
 */
public class VDividingLine extends JecnBaseDividingLinePanel {
	public VDividingLine(JecnBaseDivedingLineData flowElementData) {
		super(flowElementData);
		this.directionEnum = LineDirectionEnum.vertical;
	}

	/**
	 * 获取克隆数据对象
	 */
	public VDividingLine clone() {
		VDividingLine dividingLine = (VDividingLine) this.currClone();
		dividingLine.directionEnum = this.directionEnum;
		return dividingLine;
	}

	/**
	 * 重设位置点
	 * 
	 */
	public void reSetLocation(Point startPoint, Point endPoint) {
		this.setBounds(startPoint, endPoint);
	}
}
