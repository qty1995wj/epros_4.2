package epros.draw.gui.workflow.aid;

import java.awt.Dimension;

import javax.swing.JPopupMenu;
import javax.swing.event.DocumentEvent;
import javax.swing.event.EventListenerList;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnLineTextPanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnTipPopupMenu;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 编辑输入框文档类，主要实现实时校验内容是否满足要求
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPlainDocument extends PlainDocument {
	/** 被编辑的流程元素 */
	private JecnBaseFlowElementPanel flowElementPanel = null;
	/** 编辑框 */
	private JecnTextArea textArea = null;
	/** 提示信息弹出框 */
	private JecnTipPopupMenu tTipPopupMenu = null;

	public JecnPlainDocument(JecnTextArea textArea) {
		this.textArea = textArea;
		initComponent();
	}

	private void initComponent() {
		tTipPopupMenu = new JecnTipPopupMenu();
	}

	public void insertString(int offs, String str, AttributeSet a)
			throws BadLocationException {

		// 执行插入
		super.insertString(offs, str, a);

		// 获取输入前内容长度
		int oldLength = getLength();

		// 插入前原来内容
		String oldText = "";
		// 原来内容+当前插入内容
		String text = null;

		if (oldLength <= 0) {
			text = str;
		} else {
			oldText = this.getText(0, oldLength);
			text = oldText;
		}

		// 判断输入框内容是否正确，不正确是弹出菜单提示
		showPopupMenu(text, true);
	}

	protected void removeUpdate(AbstractDocument.DefaultDocumentEvent chng) {
		super.removeUpdate(chng);
		if (flowElementPanel == null) {
			return;
		}
		if (flowElementPanel instanceof ModelFigure) {// 泳池
			ModelFigure modelFigure = (ModelFigure) flowElementPanel;
			textArea.setSize(new Dimension(modelFigure.getZoomDividingX(),
					flowElementPanel.getHeight()));
		} else if (flowElementPanel instanceof JecnBaseFigurePanel) {// 泳池外图形
			textArea.setSize(flowElementPanel.getSize());
		} else if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 连接线
			textArea.setSize(JecnUIUtil.getEditTextAreaSize());
		}
	}

	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type. The event instance is lazily created using the
	 * parameters passed into the fire method.
	 * 
	 * @param e
	 *            the event
	 * @see EventListenerList
	 */
	protected void fireRemoveUpdate(DocumentEvent e) {
		super.fireRemoveUpdate(e);
		// 判断输入框内容是否正确，不正确是弹出菜单提示
		showPopupMenu(textArea.getText(), true);
	}

	/**
	 * 
	 * 
	 * 判断输入框内容是否正确，不正确是弹出菜单提示
	 * 
	 * @param text
	 *            String 内容
	 * @param isTipShow
	 *            boolean 是否显示提示信息：true显示；false不显示
	 * @return boolean true：验证成功 false：验证失败
	 */
	boolean showPopupMenu(String text, boolean isTipShow) {
		// 验证失败弹出提示信息
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null || flowElementPanel == null) {
			return true;
		}

		// 执行验证
		String info = JecnUserCheckUtil.checkNameMaxLength(flowElementPanel,
				text);

		if (DrawCommon.isNullOrEmtryTrim(info)) {// 验证成功
			tTipPopupMenu.getPopupMenu().setVisible(false);
			this.textArea.setBackground(JecnUIUtil.getWorkflowColor());
			return true;
		}

		// **********************验证失败**********************//
		// 设置提示信息提示信息
		tTipPopupMenu.setText(info);

		if (isTipShow && !tTipPopupMenu.getPopupMenu().isShowing()) {// 弹出菜单没有显示
			if (flowElementPanel instanceof JecnBaseFigurePanel) {
				// 显示弹出菜单
				tTipPopupMenu.getPopupMenu().show(
						JecnDrawMainPanel.getMainPanel().getWorkflow(),
						flowElementPanel.getX() + flowElementPanel.getWidth(),
						flowElementPanel.getY());
			} else if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 连接线
				JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) flowElementPanel;
				JecnLineTextPanel textPanel = manhattanLine.getTextPanel();
				// 显示弹出菜单
				tTipPopupMenu.getPopupMenu().show(
						JecnDrawMainPanel.getMainPanel().getWorkflow(),
						textPanel.getX() + textPanel.getWidth(),
						textPanel.getY());
			}

			// 设置流程元素编辑框内容不正确时的背景颜色
			textArea.setBackground(JecnUIUtil.getAidEditTextAreaBackColor());
			// 编辑框获取焦点
			textArea.requestFocusInWindow();
		}
		// **********************验证失败**********************//

		return false;
	}

	JecnBaseFlowElementPanel getFlowElementPanel() {
		return flowElementPanel;
	}

	void setFlowElementPanel(JecnBaseFlowElementPanel flowElementPanel) {
		this.flowElementPanel = flowElementPanel;
	}

	JPopupMenu getPopupMenu() {
		return this.tTipPopupMenu.getPopupMenu();
	}
}
