package com.jecn.epros.mapper;

import com.jecn.epros.domain.common.abolish.AbolishBean;
import com.jecn.epros.domain.task.*;
import com.jecn.epros.service.web.bean.iflytekTask.IflytekTaskDataBean;
import com.jecn.epros.service.web.parameter.IflytekParameter;
import com.jecn.epros.sqlprovider.CommonSqlProvider;
import com.jecn.epros.sqlprovider.TaskSqlProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface TaskMapper {

    /**
     * 任务管理根据查询条件获得任务
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = TaskSqlProvider.class, method = "fetchTasks")
    @ResultMap("mapper.Task.taskResult")
    public List<MyTaskBean> fetchTasks(Map<String, Object> paramMap);

    /**
     * 任务管理根据查询条件获得任务的数量
     */
    @SelectProvider(type = TaskSqlProvider.class, method = "getTaskManageCountBySearch")
    @ResultType(Integer.class)
    public int getTaskManageCountBySearch(Map<String, Object> paramMap);

    /**
     * 更换各阶段审批人
     *
     * @param parmMap
     */
    @UpdateProvider(type = TaskSqlProvider.class, method = "updateTaskPeopleConn")
    public void updateTaskPeopleConn(Map<String, Object> parmMap);

    /**
     * 更换当前阶段审批人
     *
     * @param parmMap
     */
    @UpdateProvider(type = TaskSqlProvider.class, method = "updateTaskPeople")
    public void updateTaskPeople(Map<String, Object> parmMap);

    /**
     * 更换创建人
     *
     * @param parmMap
     */
    @UpdateProvider(type = TaskSqlProvider.class, method = "updateTaskCreatePeople")
    public void updateTaskCreatePeople(Map<String, Object> parmMap);

    /**
     * 更换来源人
     *
     * @param parmMap
     */
    @UpdateProvider(type = TaskSqlProvider.class, method = "updateTaskFromPeople")
    public void updateTaskFromPeople(Map<String, Object> parmMap);

    @SelectProvider(type = TaskSqlProvider.class, method = "getMyTaskCount")
    public int getMyTaskCount(Map<String, Object> parmMap);

    @SelectProvider(type = TaskSqlProvider.class, method = "fetchMyTasks")
    @ResultMap("mapper.Task.myTaskBean")
    public List<TaskBean> fetchMyTasks(Map<String, Object> paramMap);

    @Select("SELECT T.TASK_ID FROM JECN_TASK_PEOPLE_NEW T WHERE T.APPROVE_PID = #{loginPeopleId}")
    public List<Long> fetchMyCurrentTasks(Long loginPeopleId);

    @Select("select jtbn.*,jts.stage_name,jts.en_name from jecn_task_bean_new jtbn left join jecn_task_stage jts on jtbn.state=jts.stage_mark and jtbn.id=jts.task_id where jtbn.id=#{taskId}")
    @ResultMap("mapper.Task.taskBean")
    public TaskBean getTaskBean(Long taskId);

    @Select("select * from Jecn_Task_Stage where task_Id=#{taskId} order by sort")
    @ResultMap("mapper.Task.taskStageWithTable")
    public List<TaskStage> fetchTaskStagesByTaskId(Long taskId);

    @Select("select * from JECN_TASK_APPLIACTION_NEW where TASK_ID=#{tasId}")
    @ResultMap("mapper.Task.taskApplicationNewWithTable")
    public TaskApplicationNew getTaskApplicationNew(Long taskId);

    @Select("SELECT ID AS ID,TASK_ID AS TASKID,FILE_NAME AS FILENAME FROM JECN_TASK_TEST_RUN_FILE WHERE TASK_ID=#{taskId}")
    public Map<String, Object> getTaskRunFileName(Long taskId);

    // 获取任务// 各阶段审批人集合 List<Object[]> 0：任务阶段；1：人员真实姓名2：人员主键ID;3:是否删除1:删除
    @Select("SELECT T_P.STAGE_ID AS STAGEID, U.TRUE_NAME AS TRUENAME, U.PEOPLE_ID AS PEOPLEID, U.ISLOCK AS ISLOCK, U.LOGIN_NAME AS LOGINNAME FROM JECN_TASK_PEOPLE_CONN_NEW T_P "
            + "LEFT JOIN Jecn_User u ON T_P.APPROVE_PID = u.people_id  ,JECN_TASK_STAGE J_S where T_P.stage_Id=J_S.id  AND U.ISLOCK = 0 and J_S.task_Id = #{taskId}")
    public List<Map<String, Object>> fetchApproceStateAndPeople(Long taskId);

    @Select("select * from JECN_TASK_FOR_RECODE_NEW where TASK_ID=#{taskId} order by SORT_ID asc")
    @ResultMap("mapper.Task.taskForRecodeNewWithTable")
    public List<TaskForRecodeNew> fetchTaskForRecords(Long taskId);

    // 阶段名 审批人姓名 岗位名 是否锁定
    @Select("SELECT TS.STAGE_NAME,U.TRUE_NAME,UPR.FIGURE_ID,U.ISLOCK" + " FROM JECN_TASK_STAGE  TS"
            + " LEFT JOIN JECN_TASK_PEOPLE_CONN_NEW TOCN ON TS.ID=TOCN.STAGE_ID"
            + " LEFT JOIN JECN_USER U ON TOCN.APPROVE_PID=U.PEOPLE_ID"
            + " LEFT JOIN JECN_USER_POSITION_RELATED UPR ON U.PEOPLE_ID=UPR.PEOPLE_ID"
            + " WHERE TS.IS_SHOW=1 AND TS.IS_SELECTED_USER=1 AND TS.TASK_ID=#{taskId} ORDER BY TS.SORT")
    public List<Map<String, Object>> fetchTaskRecordAndApproveName(String taskId);

    @Select("SELECT JTPN.ID,U.TRUE_NAME,U.PEOPLE_ID FROM JECN_TASK_PEOPLE_NEW JTPN" + "  INNER JOIN JECN_USER U"
            + "  ON JTPN.APPROVE_PID= U.PEOPLE_ID WHERE JTPN.TASK_ID=#{taskId}")
    public List<Map<String, Object>> getTaskCurApprovePeople(Long taskId);

    @Update(
            "update JECN_TASK_BEAN_NEW" +
                    " set" +
                    " update_time=#{updateTime}," +
                    " from_person_id=#{fromPeopleId}," +
                    " up_state=#{upState}," +
                    " task_else_state=#{taskElseState}," +
                    " state=#{taskState}" +
                    " where id=#{taskId}")
    public void updateTaskBean(TaskBean taskBean);

    @Delete("delete from JECN_TASK_PEOPLE_NEW where TASK_ID=#{taskId} AND APPROVE_PID=#{curPeopleId}")
    public void deletePeopleTaskPeopleNew(Map<String, Object> paramMap);

    @Select("select count(1) from jecn_task_for_recode_new where task_id=#{taskId}")
    public int getCurTaskRecordCount(Long taskId);

    @SelectKey(keyProperty = "id", before = true, resultType = Long.class, statement = {"select TASK_RECODE_NEW_SQUENCE.nextval from dual"})
    @InsertProvider(type = TaskSqlProvider.class, method = "saveTaskForRecodeNew")
    public void saveTaskForRecodeNewOracle(TaskForRecodeNew jecnTaskForRecodeNew);

    @SelectKey(keyProperty = "id", before = false, resultType = Long.class, statement = {"SELECT @@IDENTITY AS id"})
    @InsertProvider(type = TaskSqlProvider.class, method = "saveTaskForRecodeNew")
    public void saveTaskForRecodeNewSqlServer(TaskForRecodeNew jecnTaskForRecodeNew);

    @SelectKey(keyProperty = "id", before = true, resultType = Long.class, statement = {"select TASK_NUM_NEW_SQUENCE.nextval  from dual"})
    @InsertProvider(type = TaskSqlProvider.class, method = "saveTaskPeopleNew")
    public void saveTaskPeopleNewOracle(TaskPeopleNew jecnTaskPeopleNew);

    @SelectKey(keyProperty = "id", before = false, resultType = Long.class, statement = {"SELECT @@IDENTITY AS id"})
    @InsertProvider(type = TaskSqlProvider.class, method = "saveTaskPeopleNew")
    public void saveTaskPeopleNewSqlServer(TaskPeopleNew jecnTaskPeopleNew);

    @Select("select * from JECN_TASK_PEOPLE_CONN_NEW where stage_id=#{stageId}")
    @ResultMap("mapper.Task.taskApprovePeopleConnWithTable")
    public List<TaskApprovePeopleConn> fetchTaskApprovePeopleConnByStageId(Long stageId);

    @Update("update jecn_task_stage set is_selected_user=1 where task_Id=#{taskId} and stage_mark in ${stateStr}")
    public void updateTaskStageSelectUserTrue(Map<String, Object> paramMap);

    @Update("update jecn_task_stage set is_selected_user=0 where task_Id=#{taskId} and stage_mark not in ${stateStr}")
    public void updateTaskStageSelectUserFalse(Map<String, Object> paramMap);

    @Delete("delete from JECN_TASK_PEOPLE_CONN_NEW where stage_Id in (select id from Jecn_Task_Stage where task_Id = #{taskId})")
    public void deleteJecnTaskPeopleConnByTaskId(Long taskId);

    @SelectKey(keyProperty = "id", before = true, resultType = Long.class, statement = {"  select TASK_NUM_NEW_SQUENCE.nextval from dual"})
    @InsertProvider(type = TaskSqlProvider.class, method = "saveTaskPeopleConn")
    public void saveTaskPeopleConnOracle(TaskApprovePeopleConn approvePeopleConn);

    @SelectKey(keyProperty = "id", before = false, resultType = Long.class, statement = {"SELECT @@IDENTITY AS id"})
    @InsertProvider(type = TaskSqlProvider.class, method = "saveTaskPeopleConn")
    public void saveTaskPeopleConnSqlServer(TaskApprovePeopleConn approvePeopleConn);

    @Delete("delete from JECN_TASK_PEOPLE_NEW where TASK_ID=#{taskId}")
    public void deletePeopleTaskPeopleNewById(Long taskId);

    @Delete("delete from JECN_TASK_PEOPLE_CONN_NEW where stage_Id=#{stageId}")
    public void deleteJecnTaskPeopleConnByStageId(Long stageId);

    @Delete("delete from JECN_TASK_PEOPLE_NEW where TASK_ID=#{taskId} and approve_pid=#{peopleId}")
    public void deleteTaskPeopleNewByTaskAndPeopleId(Map<String, Object> paramMap);

    @Select("select * from JECN_TASK_PEOPLE_NEW where taskId=#{taskId}")
    @ResultMap("mapper.Task.taskPeopleWithTable")
    public List<TaskPeopleNew> fetchArrpovePeople(Long taskId);

    @Update("update jecn_task_bean_new set is_lock=0,update_time=#{updateTime},from_person_id=#{peopleId} where id=#{taskId}")
    public void markTaskToDelete(Map<String, Object> param);

    @Select("select count(t.id) from " + "jecn_task_bean_new t, jecn_task_people_new t_p"
            + " where t.id = #{taskId} and t_p.task_id = t.id and t.state=#{taskState} and t_p.approve_pid =#{curPeopleId}")
    public int taskApproveCount(Map<String, Object> paramMap);

    @Select("select id,task_id,file_name,file_stream from JECN_TASK_TEST_RUN_FILE where id=#{id}")
    @ResultMap("mapper.Task.TaskTestRunFile")
    TaskTestRunFile getTaskRunFile(Long id);

    @Select("  select jtfrn.* from JECN_TASK_FOR_RECODE_NEW jtfrn" +
            "  inner join JECN_TASK_PEOPLE_NEW jtpn on jtpn.RECORD_ID=jtfrn.ID and jtpn.TASK_ID=#{taskId} and jtpn.APPROVE_PID=#{peopleId}")
    @ResultMap("mapper.Task.taskForRecodeNewWithTable")
    TaskForRecodeNew getTaskForRecordNewByTaskIdAndPeopleId(Map<String, Object> params);

    @SelectProvider(type = TaskSqlProvider.class, method = "findAbolish")
    public List<AbolishBean> findAbolish(Map<String, Object> map);

    @SelectProvider(type = TaskSqlProvider.class, method = "findAbolish")
    @ResultType(Integer.class)
    public int findCount(Map<String, Object> map);

    @SelectProvider(type = TaskSqlProvider.class, method = "findTaskIflytekByUserCode")
    @ResultMap("mapper.Common.iflytekTask")
    public List<IflytekTaskDataBean> findTaskIflytekByUserCode(IflytekParameter parameter);
}
