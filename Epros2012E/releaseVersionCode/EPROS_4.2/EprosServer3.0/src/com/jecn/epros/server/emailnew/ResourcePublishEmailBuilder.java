package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.email.buss.ToolEmail;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 流程、文件等发布提醒
 * 
 * @author jecn
 * 
 */
public class ResourcePublishEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

		Object[] data = getData();
		EmailResource resource = (EmailResource) data[0];
		return createEmail(user, resource);
	}

	public EmailBasicInfo createEmail(JecnUser user, EmailResource emailResource) {

		StringBuffer buf = new StringBuffer();
		buf.append(JecnUtil.getValue("fileNameC") + emailResource.getName());
		buf.append(EmailContentBuilder.strBr);
		// 生成邮件的正文
		EmailContentBuilder contentBuilder = new EmailContentBuilder();
		contentBuilder.setContent(buf.toString());
		contentBuilder.setResourceUrl(getHttpUrl(emailResource, user));
		// 邮件标题 已完成并发布, 请查阅!
		String subject = '"' + emailResource.getName() + '"' + "," + ToolEmail.IS_FINISH_PUB;
		// 每个流程参与者都要接收邮件，点击邮件连接地址直接查看流程
		EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
		emailBasicInfo.setSubject(subject);
		emailBasicInfo.setContent(contentBuilder.buildContent());
		emailBasicInfo.addRecipients(user);

		return emailBasicInfo;

	}

	private String getHttpUrl(EmailResource emailResource, JecnUser user) {
		return ToolEmail.getUrlBySubFileType(emailResource.getSubType(), String.valueOf(emailResource.getId()), String
				.valueOf(user.getPeopleId()));
	}

}
