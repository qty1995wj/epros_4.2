package com.jecn.epros.server.dao.task.design.impl;

import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.task.design.IJecnCreateTaskDao;

/**
 * 创建任务DAO实现类
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Nov 7, 2012 时间：6:00:50 PM
 */
public class JecnCreateTaskDaoImpl extends AbsBaseDao<JecnTaskBeanNew, Long>
		implements IJecnCreateTaskDao {
	/**
	 * 判断该文件是否存在
	 * 
	 * @param fileName
	 * @return int >0 文件已存在
	 * @throws Exception
	 */
	@Override
	public int checkFileExists(String fileName, Long fileId) throws Exception {
		String sql = "select count(*) from Jecn_File_Task_Bean_New "
				+ " where file_Name =? "
				+ " and task_Id in ( select b.id  from Jecn_File a,Jecn_Task_Bean_New  b where a.del_state=0 and a.per_File_Id =?"
				+ " and b.task_Type = 1 and b.state <> 5 and (b.R_ID = a.file_ID or b.R_ID = a.per_File_Id))";
		return this.countAllByParamsNativeSql(sql, fileName, fileId);
	}

	/**
	 * 获得处于上传文件任务中的文件
	 * 
	 * @param fileId
	 */
	@Override
	public List<String> getUploadFileTaskFileNames(Long fileId)
			throws Exception {
		String hql = "select a.fileName from JecnFileTaskBeanNew as a,JecnTaskBeanNew as b where a.taskId=b.id and b.state<>5 and b.rid=?";
		try {
			return this.listHql(hql, fileId);
		} catch (Exception ex) {
			LOGGER.error(
					"JecnCreateTaskDaoImpl类中getUploadFileTaskFileNames方法发生异常!",
					ex);
		}
		return null;
	}

	@Override
	public boolean isBeInTask(Long id, int type) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isExditTask(Long id, Long loginPeopleId, int type)
			throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
}
