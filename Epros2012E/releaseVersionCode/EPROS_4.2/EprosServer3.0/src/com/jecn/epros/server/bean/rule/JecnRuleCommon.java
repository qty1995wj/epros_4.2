package com.jecn.epros.server.bean.rule;

/***
 * 制度表单
 * 2013-11-22
 *
 */
public class JecnRuleCommon  implements java.io.Serializable {
	private Long ruleId;//风险ID
	private String ruleNumber;//风险编号
	private String ruleName;//风险名称
	private Long ruleTitleId;//制度标题ID
	
	
	
	
	public Long getRuleId() {
		return ruleId;
	}
	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}
	public String getRuleNumber() {
		return ruleNumber;
	}
	public void setRuleNumber(String ruleNumber) {
		this.ruleNumber = ruleNumber;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public Long getRuleTitleId() {
		return ruleTitleId;
	}
	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}

	
}
