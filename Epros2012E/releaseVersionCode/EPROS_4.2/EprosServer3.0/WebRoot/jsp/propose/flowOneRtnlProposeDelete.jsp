<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
		<link rel="stylesheet" type="text/css"
			href="ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="ext/resources/css/xtheme-gray.css" />
		<link rel="stylesheet" type="text/css" href="css/info.css" />
		<script type="text/javascript" src="ext/adapter/ext/ext-base.js"
			charset="UTF-8">
</script>
		<script type="text/javascript" src="ext/ext-all.js" charset="UTF-8">
</script>
		<script type="text/javascript" src="${basePath}jquery/jquery-1.6.2.js"
			charset="UTF-8">
</script>
		<script type="text/javascript"
			src="${basePath}jquery/ajaxfileupload.js" charset="UTF-8">
</script>
		<script type="text/javascript" src="${basePath}js/epros_zh_CN.js"
			charset="UTF-8">
</script>
		<script type="text/javascript" src="${basePath}js/rtnlPropose.js">
</script>
		<script type="text/javascript"
			src="${basePath}js/rule/singleRuleRtnlPropose.js">
</script>
		<script type="text/javascript" src="${basePath}js/rule/ruleSys.js">
</script>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<script type="text/javascript">
var basePath = "${basePath}";
</script>
	</head>
	<body>
		<div style="font-size: 20px;" align="center">
			合理化建议已删除
		</div>
	</body>
</html>
