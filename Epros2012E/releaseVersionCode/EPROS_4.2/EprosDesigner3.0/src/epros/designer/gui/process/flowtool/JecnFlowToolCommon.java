package epros.designer.gui.process.flowtool;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreePath;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
/**
 * 流程支持工具公用方法
 * @author 2012-05-15
 *
 */
public class JecnFlowToolCommon {
	/***
	 * 获得所有的支持工具
	 * @param list
	 * @return
	 */
	public static JecnTreeModel getFlowToolTreeModel(List<JecnTreeBean> list){
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.toolRoot,JecnProperties.getValue("flowToolTree"));
		JecnTreeCommon.addNLevelNodes(list, rootNode);
		return new JecnTreeModel(rootNode);
	}
	/**
	 * 节点移动   获取支持工具
	 * @param list
	 * @param listIds
	 * @return
	 */
	public static JecnTreeModel getFlowToolMoveTreeModel(List<JecnTreeBean> list,
			List<Long> listIds){
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.toolRoot, JecnProperties.getValue("flowToolTree"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, listIds);
		return new JecnTreeModel(rootNode);
	}
	
	/***
	 * 点击树节点的操作
	 * @param evt
	 * @param jTree
	 * @param jecnManageDialog
	 */
	public static void treeMousePressed(MouseEvent evt, JecnTree jTree,FlowToolManageDialog flowToolManageDialog) {
		/** 点击左键 */
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			//双击左键
			if (evt.getClickCount() == 2) {
				TreePath[] treePath = jTree.getSelectionPaths();
				if (treePath != null && treePath.length == 1) {
					JecnTreeNode node = (JecnTreeNode) treePath[0]
							.getLastPathComponent();
					JecnTreeCommon.autoExpandNode(jTree, node);
				}
			}
		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = jTree.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick
						.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean()
						.getTreeNodeType();
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath
								.getLastPathComponent();
//						if (nodeSelect.getJecnTreeBean().getTreeNodeType()
//								.equals(TreeNodeType.toolRoot)
//								|| nodeSelect.getJecnTreeBean()
//										.getTreeNodeType()
//										.equals(TreeNodeType.tool)) {
//							continue;
//						}
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean()
								.getTreeNodeType())) {
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}
				if (listSelectNode.size() > 0) {
					FlowToolTreeMenu menu = new FlowToolTreeMenu(jTree,flowToolManageDialog);

					if (listSelectNode.size() == 1) {
						jTree.setSelectionPath(listTreePaths.get(0));
						menu.setListNode(listSelectNode);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
						return;
					}
					// 去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon
							.repeatChildNodes(listSelectNode, jTree);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild
								.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath
										.getLastPathComponent();
								if (node.getJecnTreeBean()
										.getId()
										.equals(nodeSelect.getJecnTreeBean()
												.getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							jTree.setSelectionPath(treePaths[0]);
						} else {
							jTree.setSelectionPaths(treePaths);
						}
						menu.setListNode(listRemoveChild);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}

			}
		}
		
		
		
	}

}
