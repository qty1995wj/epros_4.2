package com.jecn.epros.server.action.web.login.ad.lianhedianzi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

public class LianHeDianZiAfterItem {
	private static Logger log = Logger.getLogger(LianHeDianZiAfterItem.class);
	// Ldap 服务器地址
	public static String LDAP_URL = null;
	// 域名
	public static String DOMAIN = null;
	// AD 域基准目录后缀 dc=jecn,dc=com,dc=cn
	public static String ADDOMAIN = null;
	// 岗位匹配时使用的Email后缀
	public static String EMAIL = null;
	// 管理员用户名
	public static String USERNAME = null;
	// 密码
	public static String PASSWORD = null;
	// 映射的列
	public static String DATACOLUMNS = null;
	// 映射列域数据Bean的对应关系
	public static String DATAMAPPINGS = null;
	// 基准目录
	public static String BASE = null;
	// 过滤
	public static String FILTER = null;
	// 不读取数据的部门
	public static String UNREADORG = null;
	// 不读取数据的用户
	public static String UNREADUSER = null;
	// 需要特殊处理的OU
	public static String SPECIALOU = null;

	public static void start() {
		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/lianhedianzi/lianHeDianZiLdapServerConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			String ldapURL = props.getProperty("ldapURL");
			if (!JecnCommon.isNullOrEmtryTrim(ldapURL)) {
				LDAP_URL = ldapURL;
			}

			DOMAIN = props.getProperty("domain");
			USERNAME = props.getProperty("username");
			PASSWORD = props.getProperty("password");
			DATACOLUMNS = props.getProperty("dataColumns");
			DATAMAPPINGS = props.getProperty("dataMappings");
			BASE = props.getProperty("base");
			FILTER = props.getProperty("filter");
			UNREADORG = props.getProperty("unreadOrg");
			UNREADUSER = props.getProperty("unreadUser");
			ADDOMAIN = props.getProperty("adDomain");
			SPECIALOU = props.getProperty("specialOU");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		}
	}
}