package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;

import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.ProcessConstructionBean;

/**
 * 
 * 流程建设覆盖度
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessBuildCoverDownExcelService extends BaseDownExcelService {

	private ProcessConstructionBean buildCoverageBean = null;

	public ProcessBuildCoverDownExcelService() {
		this.mouldPath = getPrefixPath()
				+ "mould/processBuildCoverageReport.xls";
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (buildCoverageBean == null) {
			return true;
		}
		startDataRow = 3;
		try {
			// 第一行
			this.addFirstRowData(dataSheet);

			// 待建
			String toBeBuilt = JecnUtil.getValue("toBeBuilt");
			// 审批
			String approval = JecnUtil.getValue("examinationAndApprovalOf");
			// 发布
			String released = JecnUtil.getValue("released");
			// 个
			String geUnit = JecnUtil.getValue("geUnit");
			String emtly = "  ";

			// 第二行
			// 字体样式
			WritableCellFormat wcfFC = getWritableCellFormat();
			// 居中
			wcfFC.setAlignment(getAlignment());
			String content = approval + buildCoverageBean.getEaaCount()
					+ geUnit + emtly + released
					+ buildCoverageBean.getReleasedCount() + geUnit + emtly
					+ toBeBuilt + buildCoverageBean.getTbbCount() + geUnit;
			dataSheet.addCell(new Label(0, 1, content, wcfFC));

			// 第四行开始
			List<ProcessWebBean> processWebList = buildCoverageBean
					.getProcessWebList();
			if (processWebList != null) {
				for (int i = 0; i < processWebList.size(); i++) {
					ProcessWebBean bean = processWebList.get(i);
					int row = startDataRow + i;
					// 流程名称
					dataSheet.addCell(new Label(0, row, bean.getFlowName()));
					// 流程编号
					dataSheet.addCell(new Label(1, row, bean.getFlowIdInput()));
					// 流程责任人
					dataSheet
							.addCell(new Label(2, row, bean.getResPeopleName()));
					// 责任部门
					dataSheet.addCell(new Label(3, row, bean.getOrgName()));
					// 发布时间
					dataSheet.addCell(new Label(4, row, bean.getPubDate()));
					// 处于什么阶段 0是待建，1是审批，2是发布
					String state = "";
					switch (bean.getTypeByData()) {
					case 0:
						state = toBeBuilt;
						break;
					case 1:
						state = approval;
						break;
					case 2:
						state = released;
						break;
					}
					dataSheet.addCell(new Label(5, row, state));
				}
			}
			return true;
		} catch (Exception e) {
			log.error("ProcessAgingDownExcelService类editExcle方法", e);
			return false;
		}
	}

	public ProcessConstructionBean getBuildCoverageBean() {
		return buildCoverageBean;
	}

	public void setBuildCoverageBean(ProcessConstructionBean buildCoverageBean) {
		this.buildCoverageBean = buildCoverageBean;
	}
}
