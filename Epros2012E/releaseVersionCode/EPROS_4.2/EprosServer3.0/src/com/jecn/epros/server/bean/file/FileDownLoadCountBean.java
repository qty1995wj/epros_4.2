package com.jecn.epros.server.bean.file;

import java.io.Serializable;
import java.util.Date;

/***
 * 
 * 下载次数bean
 * @author xiaobo
 * 
 * */
public class FileDownLoadCountBean implements Serializable{
	
	
	
    private String id;   //主键ID
    private Long peopleId;  //人员ID
    private Long downLoadCount;//下载次数
    private Date createTime;  //创建时间
    private Date updateTime;  //更行时间
    
    
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
	public Long getDownLoadCount() {
		return downLoadCount;
	}
	public void setDownLoadCount(Long downLoadCount) {
		this.downLoadCount = downLoadCount;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
    
    
    
}
