package com.jecn.epros.server.download.wordxml.datangyidong;

import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.element.dom.page.Sect;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.base.OldGeneralProcessModel;

public class DTProcessModel extends OldGeneralProcessModel{

	public DTProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path);
	}
	
	
	/***
	 * 大唐移动不要页脚
	 * 
	 * @param sect
	 */
	@Override
	protected void createCommhdrftr(Sect sect) {
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

}
