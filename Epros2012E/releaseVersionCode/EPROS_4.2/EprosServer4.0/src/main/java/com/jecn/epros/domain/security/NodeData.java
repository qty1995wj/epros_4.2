package com.jecn.epros.domain.security;

/**
 * 数据权限对象
 * Created by admin on 2017/3/18.
 */
public class NodeData {
    private String tPath;
    private Long id;
    private Long accessType;

    public Long getAccessType() {
        return accessType;
    }

    public void setAccessType(Long accessType) {
        this.accessType = accessType;
    }

    public Long getId() {
        return id;
    }

    public String gettPath() {
        return tPath;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void settPath(String tPath) {
        this.tPath = tPath;
    }
}

