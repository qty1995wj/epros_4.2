/**
 * Iam_empbaseinfoLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crrs.xc.dataimport;

public class Iam_empbaseinfoLine  implements java.io.Serializable {
    private java.lang.String c_idcardflag;

    private java.math.BigDecimal c_age;

    private java.lang.String c_parttime;

    private java.math.BigDecimal c_sljdsj;

    private java.lang.String c_orgcode;

    private java.util.Date c_rsdate;

    private java.util.Date c_beginworkdate;

    private java.lang.String c_nationality;

    private java.math.BigDecimal c_jobid;

    private java.lang.String c_middlename;

    private java.math.BigDecimal c_gljdsj;

    private java.math.BigDecimal c_oid;

    private java.math.BigDecimal versionid;

    private java.math.BigDecimal c_operator;

    private java.math.BigDecimal c_sortnumber;

    private java.lang.String c_nccode;

    private java.lang.String c_careerlevel;

    private java.lang.String hkdjjg;

    private java.math.BigDecimal c_employeeid;

    private java.lang.String c_degree;

    private java.util.Date c_birthday;

    private java.lang.String c_specialduty;

    private java.util.Date c_joinjobdate;

    private java.lang.String c_usedname;

    private java.lang.String c_origin;

    private java.lang.String c_nativeplace;

    private java.math.BigDecimal c_jobgrade;

    private java.lang.String c_sourcetype;

    private java.math.BigDecimal c_orgid;

    private java.lang.String c_occupation;

    private java.lang.String c_firstname;

    private java.lang.String c_jobcode;

    private java.math.BigDecimal c_jobclass;

    private java.lang.String c_jobname;

    private java.lang.String c_gender;

    private java.util.Date c_hiredate;

    private java.lang.String c_marital;

    private java.lang.String c_idcard;

    private java.lang.String c_vstatus;

    private java.lang.String c_name;

    private java.lang.String c_regresidence;

    private java.lang.String c_birthplace;

    private java.lang.String c_zyjndj;

    private java.math.BigDecimal c_photo;

    private java.lang.String c_pcaccount;

    private java.lang.String c_dispatch;

    private java.lang.String c_unitname;

    private java.lang.String c_lastname;

    private java.lang.String c_employeetype;

    private java.lang.String c_status;

    private java.math.BigDecimal c_rsshr;

    private java.lang.String c_group;

    private java.math.BigDecimal versioncode;

    private java.lang.String c_positionfamily;

    private java.math.BigDecimal c_companyage;

    private java.lang.String c_passport;

    private java.util.Date c_joinunitdate;

    private java.lang.String c_unitcode;

    private java.util.Date versiontime;

    private java.lang.String hkxx;

    private java.math.BigDecimal c_jobseniority;

    private java.lang.String c_basestatus;

    private java.lang.String c_orgname;

    private java.lang.String c_transfer;

    private java.lang.String c_code;

    private java.lang.String c_careerplan;

    private java.lang.String c_13762586955440;

    private java.lang.String c_specialty;

    private java.math.BigDecimal c_joblevel;

    private java.util.Date c_lastupdattime;

    private java.lang.String c_department;

    private java.lang.String c_politicbusiness;

    private java.math.BigDecimal c_unitid;

    private java.lang.String c_remark;

    private java.util.Date c_operatetime;

    private java.lang.String c_nation;

    private java.lang.String c_employeestatus;

    private java.lang.String versiondelete;

    private java.math.BigDecimal c_hiretotallength;

    private java.lang.String c_hireinfo;

    private java.lang.String c_polity;

    private java.lang.String c_weavetype;

    private java.lang.String c_knowledge;

    private java.lang.String c_companymail;

    private java.lang.String c_mobile;

    private java.util.Date c_lut;

    public Iam_empbaseinfoLine() {
    }

    public Iam_empbaseinfoLine(
           java.lang.String c_idcardflag,
           java.math.BigDecimal c_age,
           java.lang.String c_parttime,
           java.math.BigDecimal c_sljdsj,
           java.lang.String c_orgcode,
           java.util.Date c_rsdate,
           java.util.Date c_beginworkdate,
           java.lang.String c_nationality,
           java.math.BigDecimal c_jobid,
           java.lang.String c_middlename,
           java.math.BigDecimal c_gljdsj,
           java.math.BigDecimal c_oid,
           java.math.BigDecimal versionid,
           java.math.BigDecimal c_operator,
           java.math.BigDecimal c_sortnumber,
           java.lang.String c_nccode,
           java.lang.String c_careerlevel,
           java.lang.String hkdjjg,
           java.math.BigDecimal c_employeeid,
           java.lang.String c_degree,
           java.util.Date c_birthday,
           java.lang.String c_specialduty,
           java.util.Date c_joinjobdate,
           java.lang.String c_usedname,
           java.lang.String c_origin,
           java.lang.String c_nativeplace,
           java.math.BigDecimal c_jobgrade,
           java.lang.String c_sourcetype,
           java.math.BigDecimal c_orgid,
           java.lang.String c_occupation,
           java.lang.String c_firstname,
           java.lang.String c_jobcode,
           java.math.BigDecimal c_jobclass,
           java.lang.String c_jobname,
           java.lang.String c_gender,
           java.util.Date c_hiredate,
           java.lang.String c_marital,
           java.lang.String c_idcard,
           java.lang.String c_vstatus,
           java.lang.String c_name,
           java.lang.String c_regresidence,
           java.lang.String c_birthplace,
           java.lang.String c_zyjndj,
           java.math.BigDecimal c_photo,
           java.lang.String c_pcaccount,
           java.lang.String c_dispatch,
           java.lang.String c_unitname,
           java.lang.String c_lastname,
           java.lang.String c_employeetype,
           java.lang.String c_status,
           java.math.BigDecimal c_rsshr,
           java.lang.String c_group,
           java.math.BigDecimal versioncode,
           java.lang.String c_positionfamily,
           java.math.BigDecimal c_companyage,
           java.lang.String c_passport,
           java.util.Date c_joinunitdate,
           java.lang.String c_unitcode,
           java.util.Date versiontime,
           java.lang.String hkxx,
           java.math.BigDecimal c_jobseniority,
           java.lang.String c_basestatus,
           java.lang.String c_orgname,
           java.lang.String c_transfer,
           java.lang.String c_code,
           java.lang.String c_careerplan,
           java.lang.String c_13762586955440,
           java.lang.String c_specialty,
           java.math.BigDecimal c_joblevel,
           java.util.Date c_lastupdattime,
           java.lang.String c_department,
           java.lang.String c_politicbusiness,
           java.math.BigDecimal c_unitid,
           java.lang.String c_remark,
           java.util.Date c_operatetime,
           java.lang.String c_nation,
           java.lang.String c_employeestatus,
           java.lang.String versiondelete,
           java.math.BigDecimal c_hiretotallength,
           java.lang.String c_hireinfo,
           java.lang.String c_polity,
           java.lang.String c_weavetype,
           java.lang.String c_knowledge,
           java.lang.String c_companymail,
           java.lang.String c_mobile,
           java.util.Date c_lut) {
           this.c_idcardflag = c_idcardflag;
           this.c_age = c_age;
           this.c_parttime = c_parttime;
           this.c_sljdsj = c_sljdsj;
           this.c_orgcode = c_orgcode;
           this.c_rsdate = c_rsdate;
           this.c_beginworkdate = c_beginworkdate;
           this.c_nationality = c_nationality;
           this.c_jobid = c_jobid;
           this.c_middlename = c_middlename;
           this.c_gljdsj = c_gljdsj;
           this.c_oid = c_oid;
           this.versionid = versionid;
           this.c_operator = c_operator;
           this.c_sortnumber = c_sortnumber;
           this.c_nccode = c_nccode;
           this.c_careerlevel = c_careerlevel;
           this.hkdjjg = hkdjjg;
           this.c_employeeid = c_employeeid;
           this.c_degree = c_degree;
           this.c_birthday = c_birthday;
           this.c_specialduty = c_specialduty;
           this.c_joinjobdate = c_joinjobdate;
           this.c_usedname = c_usedname;
           this.c_origin = c_origin;
           this.c_nativeplace = c_nativeplace;
           this.c_jobgrade = c_jobgrade;
           this.c_sourcetype = c_sourcetype;
           this.c_orgid = c_orgid;
           this.c_occupation = c_occupation;
           this.c_firstname = c_firstname;
           this.c_jobcode = c_jobcode;
           this.c_jobclass = c_jobclass;
           this.c_jobname = c_jobname;
           this.c_gender = c_gender;
           this.c_hiredate = c_hiredate;
           this.c_marital = c_marital;
           this.c_idcard = c_idcard;
           this.c_vstatus = c_vstatus;
           this.c_name = c_name;
           this.c_regresidence = c_regresidence;
           this.c_birthplace = c_birthplace;
           this.c_zyjndj = c_zyjndj;
           this.c_photo = c_photo;
           this.c_pcaccount = c_pcaccount;
           this.c_dispatch = c_dispatch;
           this.c_unitname = c_unitname;
           this.c_lastname = c_lastname;
           this.c_employeetype = c_employeetype;
           this.c_status = c_status;
           this.c_rsshr = c_rsshr;
           this.c_group = c_group;
           this.versioncode = versioncode;
           this.c_positionfamily = c_positionfamily;
           this.c_companyage = c_companyage;
           this.c_passport = c_passport;
           this.c_joinunitdate = c_joinunitdate;
           this.c_unitcode = c_unitcode;
           this.versiontime = versiontime;
           this.hkxx = hkxx;
           this.c_jobseniority = c_jobseniority;
           this.c_basestatus = c_basestatus;
           this.c_orgname = c_orgname;
           this.c_transfer = c_transfer;
           this.c_code = c_code;
           this.c_careerplan = c_careerplan;
           this.c_13762586955440 = c_13762586955440;
           this.c_specialty = c_specialty;
           this.c_joblevel = c_joblevel;
           this.c_lastupdattime = c_lastupdattime;
           this.c_department = c_department;
           this.c_politicbusiness = c_politicbusiness;
           this.c_unitid = c_unitid;
           this.c_remark = c_remark;
           this.c_operatetime = c_operatetime;
           this.c_nation = c_nation;
           this.c_employeestatus = c_employeestatus;
           this.versiondelete = versiondelete;
           this.c_hiretotallength = c_hiretotallength;
           this.c_hireinfo = c_hireinfo;
           this.c_polity = c_polity;
           this.c_weavetype = c_weavetype;
           this.c_knowledge = c_knowledge;
           this.c_companymail = c_companymail;
           this.c_mobile = c_mobile;
           this.c_lut = c_lut;
    }


    /**
     * Gets the c_idcardflag value for this Iam_empbaseinfoLine.
     * 
     * @return c_idcardflag
     */
    public java.lang.String getC_idcardflag() {
        return c_idcardflag;
    }


    /**
     * Sets the c_idcardflag value for this Iam_empbaseinfoLine.
     * 
     * @param c_idcardflag
     */
    public void setC_idcardflag(java.lang.String c_idcardflag) {
        this.c_idcardflag = c_idcardflag;
    }


    /**
     * Gets the c_age value for this Iam_empbaseinfoLine.
     * 
     * @return c_age
     */
    public java.math.BigDecimal getC_age() {
        return c_age;
    }


    /**
     * Sets the c_age value for this Iam_empbaseinfoLine.
     * 
     * @param c_age
     */
    public void setC_age(java.math.BigDecimal c_age) {
        this.c_age = c_age;
    }


    /**
     * Gets the c_parttime value for this Iam_empbaseinfoLine.
     * 
     * @return c_parttime
     */
    public java.lang.String getC_parttime() {
        return c_parttime;
    }


    /**
     * Sets the c_parttime value for this Iam_empbaseinfoLine.
     * 
     * @param c_parttime
     */
    public void setC_parttime(java.lang.String c_parttime) {
        this.c_parttime = c_parttime;
    }


    /**
     * Gets the c_sljdsj value for this Iam_empbaseinfoLine.
     * 
     * @return c_sljdsj
     */
    public java.math.BigDecimal getC_sljdsj() {
        return c_sljdsj;
    }


    /**
     * Sets the c_sljdsj value for this Iam_empbaseinfoLine.
     * 
     * @param c_sljdsj
     */
    public void setC_sljdsj(java.math.BigDecimal c_sljdsj) {
        this.c_sljdsj = c_sljdsj;
    }


    /**
     * Gets the c_orgcode value for this Iam_empbaseinfoLine.
     * 
     * @return c_orgcode
     */
    public java.lang.String getC_orgcode() {
        return c_orgcode;
    }


    /**
     * Sets the c_orgcode value for this Iam_empbaseinfoLine.
     * 
     * @param c_orgcode
     */
    public void setC_orgcode(java.lang.String c_orgcode) {
        this.c_orgcode = c_orgcode;
    }


    /**
     * Gets the c_rsdate value for this Iam_empbaseinfoLine.
     * 
     * @return c_rsdate
     */
    public java.util.Date getC_rsdate() {
        return c_rsdate;
    }


    /**
     * Sets the c_rsdate value for this Iam_empbaseinfoLine.
     * 
     * @param c_rsdate
     */
    public void setC_rsdate(java.util.Date c_rsdate) {
        this.c_rsdate = c_rsdate;
    }


    /**
     * Gets the c_beginworkdate value for this Iam_empbaseinfoLine.
     * 
     * @return c_beginworkdate
     */
    public java.util.Date getC_beginworkdate() {
        return c_beginworkdate;
    }


    /**
     * Sets the c_beginworkdate value for this Iam_empbaseinfoLine.
     * 
     * @param c_beginworkdate
     */
    public void setC_beginworkdate(java.util.Date c_beginworkdate) {
        this.c_beginworkdate = c_beginworkdate;
    }


    /**
     * Gets the c_nationality value for this Iam_empbaseinfoLine.
     * 
     * @return c_nationality
     */
    public java.lang.String getC_nationality() {
        return c_nationality;
    }


    /**
     * Sets the c_nationality value for this Iam_empbaseinfoLine.
     * 
     * @param c_nationality
     */
    public void setC_nationality(java.lang.String c_nationality) {
        this.c_nationality = c_nationality;
    }


    /**
     * Gets the c_jobid value for this Iam_empbaseinfoLine.
     * 
     * @return c_jobid
     */
    public java.math.BigDecimal getC_jobid() {
        return c_jobid;
    }


    /**
     * Sets the c_jobid value for this Iam_empbaseinfoLine.
     * 
     * @param c_jobid
     */
    public void setC_jobid(java.math.BigDecimal c_jobid) {
        this.c_jobid = c_jobid;
    }


    /**
     * Gets the c_middlename value for this Iam_empbaseinfoLine.
     * 
     * @return c_middlename
     */
    public java.lang.String getC_middlename() {
        return c_middlename;
    }


    /**
     * Sets the c_middlename value for this Iam_empbaseinfoLine.
     * 
     * @param c_middlename
     */
    public void setC_middlename(java.lang.String c_middlename) {
        this.c_middlename = c_middlename;
    }


    /**
     * Gets the c_gljdsj value for this Iam_empbaseinfoLine.
     * 
     * @return c_gljdsj
     */
    public java.math.BigDecimal getC_gljdsj() {
        return c_gljdsj;
    }


    /**
     * Sets the c_gljdsj value for this Iam_empbaseinfoLine.
     * 
     * @param c_gljdsj
     */
    public void setC_gljdsj(java.math.BigDecimal c_gljdsj) {
        this.c_gljdsj = c_gljdsj;
    }


    /**
     * Gets the c_oid value for this Iam_empbaseinfoLine.
     * 
     * @return c_oid
     */
    public java.math.BigDecimal getC_oid() {
        return c_oid;
    }


    /**
     * Sets the c_oid value for this Iam_empbaseinfoLine.
     * 
     * @param c_oid
     */
    public void setC_oid(java.math.BigDecimal c_oid) {
        this.c_oid = c_oid;
    }


    /**
     * Gets the versionid value for this Iam_empbaseinfoLine.
     * 
     * @return versionid
     */
    public java.math.BigDecimal getVersionid() {
        return versionid;
    }


    /**
     * Sets the versionid value for this Iam_empbaseinfoLine.
     * 
     * @param versionid
     */
    public void setVersionid(java.math.BigDecimal versionid) {
        this.versionid = versionid;
    }


    /**
     * Gets the c_operator value for this Iam_empbaseinfoLine.
     * 
     * @return c_operator
     */
    public java.math.BigDecimal getC_operator() {
        return c_operator;
    }


    /**
     * Sets the c_operator value for this Iam_empbaseinfoLine.
     * 
     * @param c_operator
     */
    public void setC_operator(java.math.BigDecimal c_operator) {
        this.c_operator = c_operator;
    }


    /**
     * Gets the c_sortnumber value for this Iam_empbaseinfoLine.
     * 
     * @return c_sortnumber
     */
    public java.math.BigDecimal getC_sortnumber() {
        return c_sortnumber;
    }


    /**
     * Sets the c_sortnumber value for this Iam_empbaseinfoLine.
     * 
     * @param c_sortnumber
     */
    public void setC_sortnumber(java.math.BigDecimal c_sortnumber) {
        this.c_sortnumber = c_sortnumber;
    }


    /**
     * Gets the c_nccode value for this Iam_empbaseinfoLine.
     * 
     * @return c_nccode
     */
    public java.lang.String getC_nccode() {
        return c_nccode;
    }


    /**
     * Sets the c_nccode value for this Iam_empbaseinfoLine.
     * 
     * @param c_nccode
     */
    public void setC_nccode(java.lang.String c_nccode) {
        this.c_nccode = c_nccode;
    }


    /**
     * Gets the c_careerlevel value for this Iam_empbaseinfoLine.
     * 
     * @return c_careerlevel
     */
    public java.lang.String getC_careerlevel() {
        return c_careerlevel;
    }


    /**
     * Sets the c_careerlevel value for this Iam_empbaseinfoLine.
     * 
     * @param c_careerlevel
     */
    public void setC_careerlevel(java.lang.String c_careerlevel) {
        this.c_careerlevel = c_careerlevel;
    }


    /**
     * Gets the hkdjjg value for this Iam_empbaseinfoLine.
     * 
     * @return hkdjjg
     */
    public java.lang.String getHkdjjg() {
        return hkdjjg;
    }


    /**
     * Sets the hkdjjg value for this Iam_empbaseinfoLine.
     * 
     * @param hkdjjg
     */
    public void setHkdjjg(java.lang.String hkdjjg) {
        this.hkdjjg = hkdjjg;
    }


    /**
     * Gets the c_employeeid value for this Iam_empbaseinfoLine.
     * 
     * @return c_employeeid
     */
    public java.math.BigDecimal getC_employeeid() {
        return c_employeeid;
    }


    /**
     * Sets the c_employeeid value for this Iam_empbaseinfoLine.
     * 
     * @param c_employeeid
     */
    public void setC_employeeid(java.math.BigDecimal c_employeeid) {
        this.c_employeeid = c_employeeid;
    }


    /**
     * Gets the c_degree value for this Iam_empbaseinfoLine.
     * 
     * @return c_degree
     */
    public java.lang.String getC_degree() {
        return c_degree;
    }


    /**
     * Sets the c_degree value for this Iam_empbaseinfoLine.
     * 
     * @param c_degree
     */
    public void setC_degree(java.lang.String c_degree) {
        this.c_degree = c_degree;
    }


    /**
     * Gets the c_birthday value for this Iam_empbaseinfoLine.
     * 
     * @return c_birthday
     */
    public java.util.Date getC_birthday() {
        return c_birthday;
    }


    /**
     * Sets the c_birthday value for this Iam_empbaseinfoLine.
     * 
     * @param c_birthday
     */
    public void setC_birthday(java.util.Date c_birthday) {
        this.c_birthday = c_birthday;
    }


    /**
     * Gets the c_specialduty value for this Iam_empbaseinfoLine.
     * 
     * @return c_specialduty
     */
    public java.lang.String getC_specialduty() {
        return c_specialduty;
    }


    /**
     * Sets the c_specialduty value for this Iam_empbaseinfoLine.
     * 
     * @param c_specialduty
     */
    public void setC_specialduty(java.lang.String c_specialduty) {
        this.c_specialduty = c_specialduty;
    }


    /**
     * Gets the c_joinjobdate value for this Iam_empbaseinfoLine.
     * 
     * @return c_joinjobdate
     */
    public java.util.Date getC_joinjobdate() {
        return c_joinjobdate;
    }


    /**
     * Sets the c_joinjobdate value for this Iam_empbaseinfoLine.
     * 
     * @param c_joinjobdate
     */
    public void setC_joinjobdate(java.util.Date c_joinjobdate) {
        this.c_joinjobdate = c_joinjobdate;
    }


    /**
     * Gets the c_usedname value for this Iam_empbaseinfoLine.
     * 
     * @return c_usedname
     */
    public java.lang.String getC_usedname() {
        return c_usedname;
    }


    /**
     * Sets the c_usedname value for this Iam_empbaseinfoLine.
     * 
     * @param c_usedname
     */
    public void setC_usedname(java.lang.String c_usedname) {
        this.c_usedname = c_usedname;
    }


    /**
     * Gets the c_origin value for this Iam_empbaseinfoLine.
     * 
     * @return c_origin
     */
    public java.lang.String getC_origin() {
        return c_origin;
    }


    /**
     * Sets the c_origin value for this Iam_empbaseinfoLine.
     * 
     * @param c_origin
     */
    public void setC_origin(java.lang.String c_origin) {
        this.c_origin = c_origin;
    }


    /**
     * Gets the c_nativeplace value for this Iam_empbaseinfoLine.
     * 
     * @return c_nativeplace
     */
    public java.lang.String getC_nativeplace() {
        return c_nativeplace;
    }


    /**
     * Sets the c_nativeplace value for this Iam_empbaseinfoLine.
     * 
     * @param c_nativeplace
     */
    public void setC_nativeplace(java.lang.String c_nativeplace) {
        this.c_nativeplace = c_nativeplace;
    }


    /**
     * Gets the c_jobgrade value for this Iam_empbaseinfoLine.
     * 
     * @return c_jobgrade
     */
    public java.math.BigDecimal getC_jobgrade() {
        return c_jobgrade;
    }


    /**
     * Sets the c_jobgrade value for this Iam_empbaseinfoLine.
     * 
     * @param c_jobgrade
     */
    public void setC_jobgrade(java.math.BigDecimal c_jobgrade) {
        this.c_jobgrade = c_jobgrade;
    }


    /**
     * Gets the c_sourcetype value for this Iam_empbaseinfoLine.
     * 
     * @return c_sourcetype
     */
    public java.lang.String getC_sourcetype() {
        return c_sourcetype;
    }


    /**
     * Sets the c_sourcetype value for this Iam_empbaseinfoLine.
     * 
     * @param c_sourcetype
     */
    public void setC_sourcetype(java.lang.String c_sourcetype) {
        this.c_sourcetype = c_sourcetype;
    }


    /**
     * Gets the c_orgid value for this Iam_empbaseinfoLine.
     * 
     * @return c_orgid
     */
    public java.math.BigDecimal getC_orgid() {
        return c_orgid;
    }


    /**
     * Sets the c_orgid value for this Iam_empbaseinfoLine.
     * 
     * @param c_orgid
     */
    public void setC_orgid(java.math.BigDecimal c_orgid) {
        this.c_orgid = c_orgid;
    }


    /**
     * Gets the c_occupation value for this Iam_empbaseinfoLine.
     * 
     * @return c_occupation
     */
    public java.lang.String getC_occupation() {
        return c_occupation;
    }


    /**
     * Sets the c_occupation value for this Iam_empbaseinfoLine.
     * 
     * @param c_occupation
     */
    public void setC_occupation(java.lang.String c_occupation) {
        this.c_occupation = c_occupation;
    }


    /**
     * Gets the c_firstname value for this Iam_empbaseinfoLine.
     * 
     * @return c_firstname
     */
    public java.lang.String getC_firstname() {
        return c_firstname;
    }


    /**
     * Sets the c_firstname value for this Iam_empbaseinfoLine.
     * 
     * @param c_firstname
     */
    public void setC_firstname(java.lang.String c_firstname) {
        this.c_firstname = c_firstname;
    }


    /**
     * Gets the c_jobcode value for this Iam_empbaseinfoLine.
     * 
     * @return c_jobcode
     */
    public java.lang.String getC_jobcode() {
        return c_jobcode;
    }


    /**
     * Sets the c_jobcode value for this Iam_empbaseinfoLine.
     * 
     * @param c_jobcode
     */
    public void setC_jobcode(java.lang.String c_jobcode) {
        this.c_jobcode = c_jobcode;
    }


    /**
     * Gets the c_jobclass value for this Iam_empbaseinfoLine.
     * 
     * @return c_jobclass
     */
    public java.math.BigDecimal getC_jobclass() {
        return c_jobclass;
    }


    /**
     * Sets the c_jobclass value for this Iam_empbaseinfoLine.
     * 
     * @param c_jobclass
     */
    public void setC_jobclass(java.math.BigDecimal c_jobclass) {
        this.c_jobclass = c_jobclass;
    }


    /**
     * Gets the c_jobname value for this Iam_empbaseinfoLine.
     * 
     * @return c_jobname
     */
    public java.lang.String getC_jobname() {
        return c_jobname;
    }


    /**
     * Sets the c_jobname value for this Iam_empbaseinfoLine.
     * 
     * @param c_jobname
     */
    public void setC_jobname(java.lang.String c_jobname) {
        this.c_jobname = c_jobname;
    }


    /**
     * Gets the c_gender value for this Iam_empbaseinfoLine.
     * 
     * @return c_gender
     */
    public java.lang.String getC_gender() {
        return c_gender;
    }


    /**
     * Sets the c_gender value for this Iam_empbaseinfoLine.
     * 
     * @param c_gender
     */
    public void setC_gender(java.lang.String c_gender) {
        this.c_gender = c_gender;
    }


    /**
     * Gets the c_hiredate value for this Iam_empbaseinfoLine.
     * 
     * @return c_hiredate
     */
    public java.util.Date getC_hiredate() {
        return c_hiredate;
    }


    /**
     * Sets the c_hiredate value for this Iam_empbaseinfoLine.
     * 
     * @param c_hiredate
     */
    public void setC_hiredate(java.util.Date c_hiredate) {
        this.c_hiredate = c_hiredate;
    }


    /**
     * Gets the c_marital value for this Iam_empbaseinfoLine.
     * 
     * @return c_marital
     */
    public java.lang.String getC_marital() {
        return c_marital;
    }


    /**
     * Sets the c_marital value for this Iam_empbaseinfoLine.
     * 
     * @param c_marital
     */
    public void setC_marital(java.lang.String c_marital) {
        this.c_marital = c_marital;
    }


    /**
     * Gets the c_idcard value for this Iam_empbaseinfoLine.
     * 
     * @return c_idcard
     */
    public java.lang.String getC_idcard() {
        return c_idcard;
    }


    /**
     * Sets the c_idcard value for this Iam_empbaseinfoLine.
     * 
     * @param c_idcard
     */
    public void setC_idcard(java.lang.String c_idcard) {
        this.c_idcard = c_idcard;
    }


    /**
     * Gets the c_vstatus value for this Iam_empbaseinfoLine.
     * 
     * @return c_vstatus
     */
    public java.lang.String getC_vstatus() {
        return c_vstatus;
    }


    /**
     * Sets the c_vstatus value for this Iam_empbaseinfoLine.
     * 
     * @param c_vstatus
     */
    public void setC_vstatus(java.lang.String c_vstatus) {
        this.c_vstatus = c_vstatus;
    }


    /**
     * Gets the c_name value for this Iam_empbaseinfoLine.
     * 
     * @return c_name
     */
    public java.lang.String getC_name() {
        return c_name;
    }


    /**
     * Sets the c_name value for this Iam_empbaseinfoLine.
     * 
     * @param c_name
     */
    public void setC_name(java.lang.String c_name) {
        this.c_name = c_name;
    }


    /**
     * Gets the c_regresidence value for this Iam_empbaseinfoLine.
     * 
     * @return c_regresidence
     */
    public java.lang.String getC_regresidence() {
        return c_regresidence;
    }


    /**
     * Sets the c_regresidence value for this Iam_empbaseinfoLine.
     * 
     * @param c_regresidence
     */
    public void setC_regresidence(java.lang.String c_regresidence) {
        this.c_regresidence = c_regresidence;
    }


    /**
     * Gets the c_birthplace value for this Iam_empbaseinfoLine.
     * 
     * @return c_birthplace
     */
    public java.lang.String getC_birthplace() {
        return c_birthplace;
    }


    /**
     * Sets the c_birthplace value for this Iam_empbaseinfoLine.
     * 
     * @param c_birthplace
     */
    public void setC_birthplace(java.lang.String c_birthplace) {
        this.c_birthplace = c_birthplace;
    }


    /**
     * Gets the c_zyjndj value for this Iam_empbaseinfoLine.
     * 
     * @return c_zyjndj
     */
    public java.lang.String getC_zyjndj() {
        return c_zyjndj;
    }


    /**
     * Sets the c_zyjndj value for this Iam_empbaseinfoLine.
     * 
     * @param c_zyjndj
     */
    public void setC_zyjndj(java.lang.String c_zyjndj) {
        this.c_zyjndj = c_zyjndj;
    }


    /**
     * Gets the c_photo value for this Iam_empbaseinfoLine.
     * 
     * @return c_photo
     */
    public java.math.BigDecimal getC_photo() {
        return c_photo;
    }


    /**
     * Sets the c_photo value for this Iam_empbaseinfoLine.
     * 
     * @param c_photo
     */
    public void setC_photo(java.math.BigDecimal c_photo) {
        this.c_photo = c_photo;
    }


    /**
     * Gets the c_pcaccount value for this Iam_empbaseinfoLine.
     * 
     * @return c_pcaccount
     */
    public java.lang.String getC_pcaccount() {
        return c_pcaccount;
    }


    /**
     * Sets the c_pcaccount value for this Iam_empbaseinfoLine.
     * 
     * @param c_pcaccount
     */
    public void setC_pcaccount(java.lang.String c_pcaccount) {
        this.c_pcaccount = c_pcaccount;
    }


    /**
     * Gets the c_dispatch value for this Iam_empbaseinfoLine.
     * 
     * @return c_dispatch
     */
    public java.lang.String getC_dispatch() {
        return c_dispatch;
    }


    /**
     * Sets the c_dispatch value for this Iam_empbaseinfoLine.
     * 
     * @param c_dispatch
     */
    public void setC_dispatch(java.lang.String c_dispatch) {
        this.c_dispatch = c_dispatch;
    }


    /**
     * Gets the c_unitname value for this Iam_empbaseinfoLine.
     * 
     * @return c_unitname
     */
    public java.lang.String getC_unitname() {
        return c_unitname;
    }


    /**
     * Sets the c_unitname value for this Iam_empbaseinfoLine.
     * 
     * @param c_unitname
     */
    public void setC_unitname(java.lang.String c_unitname) {
        this.c_unitname = c_unitname;
    }


    /**
     * Gets the c_lastname value for this Iam_empbaseinfoLine.
     * 
     * @return c_lastname
     */
    public java.lang.String getC_lastname() {
        return c_lastname;
    }


    /**
     * Sets the c_lastname value for this Iam_empbaseinfoLine.
     * 
     * @param c_lastname
     */
    public void setC_lastname(java.lang.String c_lastname) {
        this.c_lastname = c_lastname;
    }


    /**
     * Gets the c_employeetype value for this Iam_empbaseinfoLine.
     * 
     * @return c_employeetype
     */
    public java.lang.String getC_employeetype() {
        return c_employeetype;
    }


    /**
     * Sets the c_employeetype value for this Iam_empbaseinfoLine.
     * 
     * @param c_employeetype
     */
    public void setC_employeetype(java.lang.String c_employeetype) {
        this.c_employeetype = c_employeetype;
    }


    /**
     * Gets the c_status value for this Iam_empbaseinfoLine.
     * 
     * @return c_status
     */
    public java.lang.String getC_status() {
        return c_status;
    }


    /**
     * Sets the c_status value for this Iam_empbaseinfoLine.
     * 
     * @param c_status
     */
    public void setC_status(java.lang.String c_status) {
        this.c_status = c_status;
    }


    /**
     * Gets the c_rsshr value for this Iam_empbaseinfoLine.
     * 
     * @return c_rsshr
     */
    public java.math.BigDecimal getC_rsshr() {
        return c_rsshr;
    }


    /**
     * Sets the c_rsshr value for this Iam_empbaseinfoLine.
     * 
     * @param c_rsshr
     */
    public void setC_rsshr(java.math.BigDecimal c_rsshr) {
        this.c_rsshr = c_rsshr;
    }


    /**
     * Gets the c_group value for this Iam_empbaseinfoLine.
     * 
     * @return c_group
     */
    public java.lang.String getC_group() {
        return c_group;
    }


    /**
     * Sets the c_group value for this Iam_empbaseinfoLine.
     * 
     * @param c_group
     */
    public void setC_group(java.lang.String c_group) {
        this.c_group = c_group;
    }


    /**
     * Gets the versioncode value for this Iam_empbaseinfoLine.
     * 
     * @return versioncode
     */
    public java.math.BigDecimal getVersioncode() {
        return versioncode;
    }


    /**
     * Sets the versioncode value for this Iam_empbaseinfoLine.
     * 
     * @param versioncode
     */
    public void setVersioncode(java.math.BigDecimal versioncode) {
        this.versioncode = versioncode;
    }


    /**
     * Gets the c_positionfamily value for this Iam_empbaseinfoLine.
     * 
     * @return c_positionfamily
     */
    public java.lang.String getC_positionfamily() {
        return c_positionfamily;
    }


    /**
     * Sets the c_positionfamily value for this Iam_empbaseinfoLine.
     * 
     * @param c_positionfamily
     */
    public void setC_positionfamily(java.lang.String c_positionfamily) {
        this.c_positionfamily = c_positionfamily;
    }


    /**
     * Gets the c_companyage value for this Iam_empbaseinfoLine.
     * 
     * @return c_companyage
     */
    public java.math.BigDecimal getC_companyage() {
        return c_companyage;
    }


    /**
     * Sets the c_companyage value for this Iam_empbaseinfoLine.
     * 
     * @param c_companyage
     */
    public void setC_companyage(java.math.BigDecimal c_companyage) {
        this.c_companyage = c_companyage;
    }


    /**
     * Gets the c_passport value for this Iam_empbaseinfoLine.
     * 
     * @return c_passport
     */
    public java.lang.String getC_passport() {
        return c_passport;
    }


    /**
     * Sets the c_passport value for this Iam_empbaseinfoLine.
     * 
     * @param c_passport
     */
    public void setC_passport(java.lang.String c_passport) {
        this.c_passport = c_passport;
    }


    /**
     * Gets the c_joinunitdate value for this Iam_empbaseinfoLine.
     * 
     * @return c_joinunitdate
     */
    public java.util.Date getC_joinunitdate() {
        return c_joinunitdate;
    }


    /**
     * Sets the c_joinunitdate value for this Iam_empbaseinfoLine.
     * 
     * @param c_joinunitdate
     */
    public void setC_joinunitdate(java.util.Date c_joinunitdate) {
        this.c_joinunitdate = c_joinunitdate;
    }


    /**
     * Gets the c_unitcode value for this Iam_empbaseinfoLine.
     * 
     * @return c_unitcode
     */
    public java.lang.String getC_unitcode() {
        return c_unitcode;
    }


    /**
     * Sets the c_unitcode value for this Iam_empbaseinfoLine.
     * 
     * @param c_unitcode
     */
    public void setC_unitcode(java.lang.String c_unitcode) {
        this.c_unitcode = c_unitcode;
    }


    /**
     * Gets the versiontime value for this Iam_empbaseinfoLine.
     * 
     * @return versiontime
     */
    public java.util.Date getVersiontime() {
        return versiontime;
    }


    /**
     * Sets the versiontime value for this Iam_empbaseinfoLine.
     * 
     * @param versiontime
     */
    public void setVersiontime(java.util.Date versiontime) {
        this.versiontime = versiontime;
    }


    /**
     * Gets the hkxx value for this Iam_empbaseinfoLine.
     * 
     * @return hkxx
     */
    public java.lang.String getHkxx() {
        return hkxx;
    }


    /**
     * Sets the hkxx value for this Iam_empbaseinfoLine.
     * 
     * @param hkxx
     */
    public void setHkxx(java.lang.String hkxx) {
        this.hkxx = hkxx;
    }


    /**
     * Gets the c_jobseniority value for this Iam_empbaseinfoLine.
     * 
     * @return c_jobseniority
     */
    public java.math.BigDecimal getC_jobseniority() {
        return c_jobseniority;
    }


    /**
     * Sets the c_jobseniority value for this Iam_empbaseinfoLine.
     * 
     * @param c_jobseniority
     */
    public void setC_jobseniority(java.math.BigDecimal c_jobseniority) {
        this.c_jobseniority = c_jobseniority;
    }


    /**
     * Gets the c_basestatus value for this Iam_empbaseinfoLine.
     * 
     * @return c_basestatus
     */
    public java.lang.String getC_basestatus() {
        return c_basestatus;
    }


    /**
     * Sets the c_basestatus value for this Iam_empbaseinfoLine.
     * 
     * @param c_basestatus
     */
    public void setC_basestatus(java.lang.String c_basestatus) {
        this.c_basestatus = c_basestatus;
    }


    /**
     * Gets the c_orgname value for this Iam_empbaseinfoLine.
     * 
     * @return c_orgname
     */
    public java.lang.String getC_orgname() {
        return c_orgname;
    }


    /**
     * Sets the c_orgname value for this Iam_empbaseinfoLine.
     * 
     * @param c_orgname
     */
    public void setC_orgname(java.lang.String c_orgname) {
        this.c_orgname = c_orgname;
    }


    /**
     * Gets the c_transfer value for this Iam_empbaseinfoLine.
     * 
     * @return c_transfer
     */
    public java.lang.String getC_transfer() {
        return c_transfer;
    }


    /**
     * Sets the c_transfer value for this Iam_empbaseinfoLine.
     * 
     * @param c_transfer
     */
    public void setC_transfer(java.lang.String c_transfer) {
        this.c_transfer = c_transfer;
    }


    /**
     * Gets the c_code value for this Iam_empbaseinfoLine.
     * 
     * @return c_code
     */
    public java.lang.String getC_code() {
        return c_code;
    }


    /**
     * Sets the c_code value for this Iam_empbaseinfoLine.
     * 
     * @param c_code
     */
    public void setC_code(java.lang.String c_code) {
        this.c_code = c_code;
    }


    /**
     * Gets the c_careerplan value for this Iam_empbaseinfoLine.
     * 
     * @return c_careerplan
     */
    public java.lang.String getC_careerplan() {
        return c_careerplan;
    }


    /**
     * Sets the c_careerplan value for this Iam_empbaseinfoLine.
     * 
     * @param c_careerplan
     */
    public void setC_careerplan(java.lang.String c_careerplan) {
        this.c_careerplan = c_careerplan;
    }


    /**
     * Gets the c_13762586955440 value for this Iam_empbaseinfoLine.
     * 
     * @return c_13762586955440
     */
    public java.lang.String getC_13762586955440() {
        return c_13762586955440;
    }


    /**
     * Sets the c_13762586955440 value for this Iam_empbaseinfoLine.
     * 
     * @param c_13762586955440
     */
    public void setC_13762586955440(java.lang.String c_13762586955440) {
        this.c_13762586955440 = c_13762586955440;
    }


    /**
     * Gets the c_specialty value for this Iam_empbaseinfoLine.
     * 
     * @return c_specialty
     */
    public java.lang.String getC_specialty() {
        return c_specialty;
    }


    /**
     * Sets the c_specialty value for this Iam_empbaseinfoLine.
     * 
     * @param c_specialty
     */
    public void setC_specialty(java.lang.String c_specialty) {
        this.c_specialty = c_specialty;
    }


    /**
     * Gets the c_joblevel value for this Iam_empbaseinfoLine.
     * 
     * @return c_joblevel
     */
    public java.math.BigDecimal getC_joblevel() {
        return c_joblevel;
    }


    /**
     * Sets the c_joblevel value for this Iam_empbaseinfoLine.
     * 
     * @param c_joblevel
     */
    public void setC_joblevel(java.math.BigDecimal c_joblevel) {
        this.c_joblevel = c_joblevel;
    }


    /**
     * Gets the c_lastupdattime value for this Iam_empbaseinfoLine.
     * 
     * @return c_lastupdattime
     */
    public java.util.Date getC_lastupdattime() {
        return c_lastupdattime;
    }


    /**
     * Sets the c_lastupdattime value for this Iam_empbaseinfoLine.
     * 
     * @param c_lastupdattime
     */
    public void setC_lastupdattime(java.util.Date c_lastupdattime) {
        this.c_lastupdattime = c_lastupdattime;
    }


    /**
     * Gets the c_department value for this Iam_empbaseinfoLine.
     * 
     * @return c_department
     */
    public java.lang.String getC_department() {
        return c_department;
    }


    /**
     * Sets the c_department value for this Iam_empbaseinfoLine.
     * 
     * @param c_department
     */
    public void setC_department(java.lang.String c_department) {
        this.c_department = c_department;
    }


    /**
     * Gets the c_politicbusiness value for this Iam_empbaseinfoLine.
     * 
     * @return c_politicbusiness
     */
    public java.lang.String getC_politicbusiness() {
        return c_politicbusiness;
    }


    /**
     * Sets the c_politicbusiness value for this Iam_empbaseinfoLine.
     * 
     * @param c_politicbusiness
     */
    public void setC_politicbusiness(java.lang.String c_politicbusiness) {
        this.c_politicbusiness = c_politicbusiness;
    }


    /**
     * Gets the c_unitid value for this Iam_empbaseinfoLine.
     * 
     * @return c_unitid
     */
    public java.math.BigDecimal getC_unitid() {
        return c_unitid;
    }


    /**
     * Sets the c_unitid value for this Iam_empbaseinfoLine.
     * 
     * @param c_unitid
     */
    public void setC_unitid(java.math.BigDecimal c_unitid) {
        this.c_unitid = c_unitid;
    }


    /**
     * Gets the c_remark value for this Iam_empbaseinfoLine.
     * 
     * @return c_remark
     */
    public java.lang.String getC_remark() {
        return c_remark;
    }


    /**
     * Sets the c_remark value for this Iam_empbaseinfoLine.
     * 
     * @param c_remark
     */
    public void setC_remark(java.lang.String c_remark) {
        this.c_remark = c_remark;
    }


    /**
     * Gets the c_operatetime value for this Iam_empbaseinfoLine.
     * 
     * @return c_operatetime
     */
    public java.util.Date getC_operatetime() {
        return c_operatetime;
    }


    /**
     * Sets the c_operatetime value for this Iam_empbaseinfoLine.
     * 
     * @param c_operatetime
     */
    public void setC_operatetime(java.util.Date c_operatetime) {
        this.c_operatetime = c_operatetime;
    }


    /**
     * Gets the c_nation value for this Iam_empbaseinfoLine.
     * 
     * @return c_nation
     */
    public java.lang.String getC_nation() {
        return c_nation;
    }


    /**
     * Sets the c_nation value for this Iam_empbaseinfoLine.
     * 
     * @param c_nation
     */
    public void setC_nation(java.lang.String c_nation) {
        this.c_nation = c_nation;
    }


    /**
     * Gets the c_employeestatus value for this Iam_empbaseinfoLine.
     * 
     * @return c_employeestatus
     */
    public java.lang.String getC_employeestatus() {
        return c_employeestatus;
    }


    /**
     * Sets the c_employeestatus value for this Iam_empbaseinfoLine.
     * 
     * @param c_employeestatus
     */
    public void setC_employeestatus(java.lang.String c_employeestatus) {
        this.c_employeestatus = c_employeestatus;
    }


    /**
     * Gets the versiondelete value for this Iam_empbaseinfoLine.
     * 
     * @return versiondelete
     */
    public java.lang.String getVersiondelete() {
        return versiondelete;
    }


    /**
     * Sets the versiondelete value for this Iam_empbaseinfoLine.
     * 
     * @param versiondelete
     */
    public void setVersiondelete(java.lang.String versiondelete) {
        this.versiondelete = versiondelete;
    }


    /**
     * Gets the c_hiretotallength value for this Iam_empbaseinfoLine.
     * 
     * @return c_hiretotallength
     */
    public java.math.BigDecimal getC_hiretotallength() {
        return c_hiretotallength;
    }


    /**
     * Sets the c_hiretotallength value for this Iam_empbaseinfoLine.
     * 
     * @param c_hiretotallength
     */
    public void setC_hiretotallength(java.math.BigDecimal c_hiretotallength) {
        this.c_hiretotallength = c_hiretotallength;
    }


    /**
     * Gets the c_hireinfo value for this Iam_empbaseinfoLine.
     * 
     * @return c_hireinfo
     */
    public java.lang.String getC_hireinfo() {
        return c_hireinfo;
    }


    /**
     * Sets the c_hireinfo value for this Iam_empbaseinfoLine.
     * 
     * @param c_hireinfo
     */
    public void setC_hireinfo(java.lang.String c_hireinfo) {
        this.c_hireinfo = c_hireinfo;
    }


    /**
     * Gets the c_polity value for this Iam_empbaseinfoLine.
     * 
     * @return c_polity
     */
    public java.lang.String getC_polity() {
        return c_polity;
    }


    /**
     * Sets the c_polity value for this Iam_empbaseinfoLine.
     * 
     * @param c_polity
     */
    public void setC_polity(java.lang.String c_polity) {
        this.c_polity = c_polity;
    }


    /**
     * Gets the c_weavetype value for this Iam_empbaseinfoLine.
     * 
     * @return c_weavetype
     */
    public java.lang.String getC_weavetype() {
        return c_weavetype;
    }


    /**
     * Sets the c_weavetype value for this Iam_empbaseinfoLine.
     * 
     * @param c_weavetype
     */
    public void setC_weavetype(java.lang.String c_weavetype) {
        this.c_weavetype = c_weavetype;
    }


    /**
     * Gets the c_knowledge value for this Iam_empbaseinfoLine.
     * 
     * @return c_knowledge
     */
    public java.lang.String getC_knowledge() {
        return c_knowledge;
    }


    /**
     * Sets the c_knowledge value for this Iam_empbaseinfoLine.
     * 
     * @param c_knowledge
     */
    public void setC_knowledge(java.lang.String c_knowledge) {
        this.c_knowledge = c_knowledge;
    }


    /**
     * Gets the c_companymail value for this Iam_empbaseinfoLine.
     * 
     * @return c_companymail
     */
    public java.lang.String getC_companymail() {
        return c_companymail;
    }


    /**
     * Sets the c_companymail value for this Iam_empbaseinfoLine.
     * 
     * @param c_companymail
     */
    public void setC_companymail(java.lang.String c_companymail) {
        this.c_companymail = c_companymail;
    }


    /**
     * Gets the c_mobile value for this Iam_empbaseinfoLine.
     * 
     * @return c_mobile
     */
    public java.lang.String getC_mobile() {
        return c_mobile;
    }


    /**
     * Sets the c_mobile value for this Iam_empbaseinfoLine.
     * 
     * @param c_mobile
     */
    public void setC_mobile(java.lang.String c_mobile) {
        this.c_mobile = c_mobile;
    }


    /**
     * Gets the c_lut value for this Iam_empbaseinfoLine.
     * 
     * @return c_lut
     */
    public java.util.Date getC_lut() {
        return c_lut;
    }


    /**
     * Sets the c_lut value for this Iam_empbaseinfoLine.
     * 
     * @param c_lut
     */
    public void setC_lut(java.util.Date c_lut) {
        this.c_lut = c_lut;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Iam_empbaseinfoLine)) return false;
        Iam_empbaseinfoLine other = (Iam_empbaseinfoLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.c_idcardflag==null && other.getC_idcardflag()==null) || 
             (this.c_idcardflag!=null &&
              this.c_idcardflag.equals(other.getC_idcardflag()))) &&
            ((this.c_age==null && other.getC_age()==null) || 
             (this.c_age!=null &&
              this.c_age.equals(other.getC_age()))) &&
            ((this.c_parttime==null && other.getC_parttime()==null) || 
             (this.c_parttime!=null &&
              this.c_parttime.equals(other.getC_parttime()))) &&
            ((this.c_sljdsj==null && other.getC_sljdsj()==null) || 
             (this.c_sljdsj!=null &&
              this.c_sljdsj.equals(other.getC_sljdsj()))) &&
            ((this.c_orgcode==null && other.getC_orgcode()==null) || 
             (this.c_orgcode!=null &&
              this.c_orgcode.equals(other.getC_orgcode()))) &&
            ((this.c_rsdate==null && other.getC_rsdate()==null) || 
             (this.c_rsdate!=null &&
              this.c_rsdate.equals(other.getC_rsdate()))) &&
            ((this.c_beginworkdate==null && other.getC_beginworkdate()==null) || 
             (this.c_beginworkdate!=null &&
              this.c_beginworkdate.equals(other.getC_beginworkdate()))) &&
            ((this.c_nationality==null && other.getC_nationality()==null) || 
             (this.c_nationality!=null &&
              this.c_nationality.equals(other.getC_nationality()))) &&
            ((this.c_jobid==null && other.getC_jobid()==null) || 
             (this.c_jobid!=null &&
              this.c_jobid.equals(other.getC_jobid()))) &&
            ((this.c_middlename==null && other.getC_middlename()==null) || 
             (this.c_middlename!=null &&
              this.c_middlename.equals(other.getC_middlename()))) &&
            ((this.c_gljdsj==null && other.getC_gljdsj()==null) || 
             (this.c_gljdsj!=null &&
              this.c_gljdsj.equals(other.getC_gljdsj()))) &&
            ((this.c_oid==null && other.getC_oid()==null) || 
             (this.c_oid!=null &&
              this.c_oid.equals(other.getC_oid()))) &&
            ((this.versionid==null && other.getVersionid()==null) || 
             (this.versionid!=null &&
              this.versionid.equals(other.getVersionid()))) &&
            ((this.c_operator==null && other.getC_operator()==null) || 
             (this.c_operator!=null &&
              this.c_operator.equals(other.getC_operator()))) &&
            ((this.c_sortnumber==null && other.getC_sortnumber()==null) || 
             (this.c_sortnumber!=null &&
              this.c_sortnumber.equals(other.getC_sortnumber()))) &&
            ((this.c_nccode==null && other.getC_nccode()==null) || 
             (this.c_nccode!=null &&
              this.c_nccode.equals(other.getC_nccode()))) &&
            ((this.c_careerlevel==null && other.getC_careerlevel()==null) || 
             (this.c_careerlevel!=null &&
              this.c_careerlevel.equals(other.getC_careerlevel()))) &&
            ((this.hkdjjg==null && other.getHkdjjg()==null) || 
             (this.hkdjjg!=null &&
              this.hkdjjg.equals(other.getHkdjjg()))) &&
            ((this.c_employeeid==null && other.getC_employeeid()==null) || 
             (this.c_employeeid!=null &&
              this.c_employeeid.equals(other.getC_employeeid()))) &&
            ((this.c_degree==null && other.getC_degree()==null) || 
             (this.c_degree!=null &&
              this.c_degree.equals(other.getC_degree()))) &&
            ((this.c_birthday==null && other.getC_birthday()==null) || 
             (this.c_birthday!=null &&
              this.c_birthday.equals(other.getC_birthday()))) &&
            ((this.c_specialduty==null && other.getC_specialduty()==null) || 
             (this.c_specialduty!=null &&
              this.c_specialduty.equals(other.getC_specialduty()))) &&
            ((this.c_joinjobdate==null && other.getC_joinjobdate()==null) || 
             (this.c_joinjobdate!=null &&
              this.c_joinjobdate.equals(other.getC_joinjobdate()))) &&
            ((this.c_usedname==null && other.getC_usedname()==null) || 
             (this.c_usedname!=null &&
              this.c_usedname.equals(other.getC_usedname()))) &&
            ((this.c_origin==null && other.getC_origin()==null) || 
             (this.c_origin!=null &&
              this.c_origin.equals(other.getC_origin()))) &&
            ((this.c_nativeplace==null && other.getC_nativeplace()==null) || 
             (this.c_nativeplace!=null &&
              this.c_nativeplace.equals(other.getC_nativeplace()))) &&
            ((this.c_jobgrade==null && other.getC_jobgrade()==null) || 
             (this.c_jobgrade!=null &&
              this.c_jobgrade.equals(other.getC_jobgrade()))) &&
            ((this.c_sourcetype==null && other.getC_sourcetype()==null) || 
             (this.c_sourcetype!=null &&
              this.c_sourcetype.equals(other.getC_sourcetype()))) &&
            ((this.c_orgid==null && other.getC_orgid()==null) || 
             (this.c_orgid!=null &&
              this.c_orgid.equals(other.getC_orgid()))) &&
            ((this.c_occupation==null && other.getC_occupation()==null) || 
             (this.c_occupation!=null &&
              this.c_occupation.equals(other.getC_occupation()))) &&
            ((this.c_firstname==null && other.getC_firstname()==null) || 
             (this.c_firstname!=null &&
              this.c_firstname.equals(other.getC_firstname()))) &&
            ((this.c_jobcode==null && other.getC_jobcode()==null) || 
             (this.c_jobcode!=null &&
              this.c_jobcode.equals(other.getC_jobcode()))) &&
            ((this.c_jobclass==null && other.getC_jobclass()==null) || 
             (this.c_jobclass!=null &&
              this.c_jobclass.equals(other.getC_jobclass()))) &&
            ((this.c_jobname==null && other.getC_jobname()==null) || 
             (this.c_jobname!=null &&
              this.c_jobname.equals(other.getC_jobname()))) &&
            ((this.c_gender==null && other.getC_gender()==null) || 
             (this.c_gender!=null &&
              this.c_gender.equals(other.getC_gender()))) &&
            ((this.c_hiredate==null && other.getC_hiredate()==null) || 
             (this.c_hiredate!=null &&
              this.c_hiredate.equals(other.getC_hiredate()))) &&
            ((this.c_marital==null && other.getC_marital()==null) || 
             (this.c_marital!=null &&
              this.c_marital.equals(other.getC_marital()))) &&
            ((this.c_idcard==null && other.getC_idcard()==null) || 
             (this.c_idcard!=null &&
              this.c_idcard.equals(other.getC_idcard()))) &&
            ((this.c_vstatus==null && other.getC_vstatus()==null) || 
             (this.c_vstatus!=null &&
              this.c_vstatus.equals(other.getC_vstatus()))) &&
            ((this.c_name==null && other.getC_name()==null) || 
             (this.c_name!=null &&
              this.c_name.equals(other.getC_name()))) &&
            ((this.c_regresidence==null && other.getC_regresidence()==null) || 
             (this.c_regresidence!=null &&
              this.c_regresidence.equals(other.getC_regresidence()))) &&
            ((this.c_birthplace==null && other.getC_birthplace()==null) || 
             (this.c_birthplace!=null &&
              this.c_birthplace.equals(other.getC_birthplace()))) &&
            ((this.c_zyjndj==null && other.getC_zyjndj()==null) || 
             (this.c_zyjndj!=null &&
              this.c_zyjndj.equals(other.getC_zyjndj()))) &&
            ((this.c_photo==null && other.getC_photo()==null) || 
             (this.c_photo!=null &&
              this.c_photo.equals(other.getC_photo()))) &&
            ((this.c_pcaccount==null && other.getC_pcaccount()==null) || 
             (this.c_pcaccount!=null &&
              this.c_pcaccount.equals(other.getC_pcaccount()))) &&
            ((this.c_dispatch==null && other.getC_dispatch()==null) || 
             (this.c_dispatch!=null &&
              this.c_dispatch.equals(other.getC_dispatch()))) &&
            ((this.c_unitname==null && other.getC_unitname()==null) || 
             (this.c_unitname!=null &&
              this.c_unitname.equals(other.getC_unitname()))) &&
            ((this.c_lastname==null && other.getC_lastname()==null) || 
             (this.c_lastname!=null &&
              this.c_lastname.equals(other.getC_lastname()))) &&
            ((this.c_employeetype==null && other.getC_employeetype()==null) || 
             (this.c_employeetype!=null &&
              this.c_employeetype.equals(other.getC_employeetype()))) &&
            ((this.c_status==null && other.getC_status()==null) || 
             (this.c_status!=null &&
              this.c_status.equals(other.getC_status()))) &&
            ((this.c_rsshr==null && other.getC_rsshr()==null) || 
             (this.c_rsshr!=null &&
              this.c_rsshr.equals(other.getC_rsshr()))) &&
            ((this.c_group==null && other.getC_group()==null) || 
             (this.c_group!=null &&
              this.c_group.equals(other.getC_group()))) &&
            ((this.versioncode==null && other.getVersioncode()==null) || 
             (this.versioncode!=null &&
              this.versioncode.equals(other.getVersioncode()))) &&
            ((this.c_positionfamily==null && other.getC_positionfamily()==null) || 
             (this.c_positionfamily!=null &&
              this.c_positionfamily.equals(other.getC_positionfamily()))) &&
            ((this.c_companyage==null && other.getC_companyage()==null) || 
             (this.c_companyage!=null &&
              this.c_companyage.equals(other.getC_companyage()))) &&
            ((this.c_passport==null && other.getC_passport()==null) || 
             (this.c_passport!=null &&
              this.c_passport.equals(other.getC_passport()))) &&
            ((this.c_joinunitdate==null && other.getC_joinunitdate()==null) || 
             (this.c_joinunitdate!=null &&
              this.c_joinunitdate.equals(other.getC_joinunitdate()))) &&
            ((this.c_unitcode==null && other.getC_unitcode()==null) || 
             (this.c_unitcode!=null &&
              this.c_unitcode.equals(other.getC_unitcode()))) &&
            ((this.versiontime==null && other.getVersiontime()==null) || 
             (this.versiontime!=null &&
              this.versiontime.equals(other.getVersiontime()))) &&
            ((this.hkxx==null && other.getHkxx()==null) || 
             (this.hkxx!=null &&
              this.hkxx.equals(other.getHkxx()))) &&
            ((this.c_jobseniority==null && other.getC_jobseniority()==null) || 
             (this.c_jobseniority!=null &&
              this.c_jobseniority.equals(other.getC_jobseniority()))) &&
            ((this.c_basestatus==null && other.getC_basestatus()==null) || 
             (this.c_basestatus!=null &&
              this.c_basestatus.equals(other.getC_basestatus()))) &&
            ((this.c_orgname==null && other.getC_orgname()==null) || 
             (this.c_orgname!=null &&
              this.c_orgname.equals(other.getC_orgname()))) &&
            ((this.c_transfer==null && other.getC_transfer()==null) || 
             (this.c_transfer!=null &&
              this.c_transfer.equals(other.getC_transfer()))) &&
            ((this.c_code==null && other.getC_code()==null) || 
             (this.c_code!=null &&
              this.c_code.equals(other.getC_code()))) &&
            ((this.c_careerplan==null && other.getC_careerplan()==null) || 
             (this.c_careerplan!=null &&
              this.c_careerplan.equals(other.getC_careerplan()))) &&
            ((this.c_13762586955440==null && other.getC_13762586955440()==null) || 
             (this.c_13762586955440!=null &&
              this.c_13762586955440.equals(other.getC_13762586955440()))) &&
            ((this.c_specialty==null && other.getC_specialty()==null) || 
             (this.c_specialty!=null &&
              this.c_specialty.equals(other.getC_specialty()))) &&
            ((this.c_joblevel==null && other.getC_joblevel()==null) || 
             (this.c_joblevel!=null &&
              this.c_joblevel.equals(other.getC_joblevel()))) &&
            ((this.c_lastupdattime==null && other.getC_lastupdattime()==null) || 
             (this.c_lastupdattime!=null &&
              this.c_lastupdattime.equals(other.getC_lastupdattime()))) &&
            ((this.c_department==null && other.getC_department()==null) || 
             (this.c_department!=null &&
              this.c_department.equals(other.getC_department()))) &&
            ((this.c_politicbusiness==null && other.getC_politicbusiness()==null) || 
             (this.c_politicbusiness!=null &&
              this.c_politicbusiness.equals(other.getC_politicbusiness()))) &&
            ((this.c_unitid==null && other.getC_unitid()==null) || 
             (this.c_unitid!=null &&
              this.c_unitid.equals(other.getC_unitid()))) &&
            ((this.c_remark==null && other.getC_remark()==null) || 
             (this.c_remark!=null &&
              this.c_remark.equals(other.getC_remark()))) &&
            ((this.c_operatetime==null && other.getC_operatetime()==null) || 
             (this.c_operatetime!=null &&
              this.c_operatetime.equals(other.getC_operatetime()))) &&
            ((this.c_nation==null && other.getC_nation()==null) || 
             (this.c_nation!=null &&
              this.c_nation.equals(other.getC_nation()))) &&
            ((this.c_employeestatus==null && other.getC_employeestatus()==null) || 
             (this.c_employeestatus!=null &&
              this.c_employeestatus.equals(other.getC_employeestatus()))) &&
            ((this.versiondelete==null && other.getVersiondelete()==null) || 
             (this.versiondelete!=null &&
              this.versiondelete.equals(other.getVersiondelete()))) &&
            ((this.c_hiretotallength==null && other.getC_hiretotallength()==null) || 
             (this.c_hiretotallength!=null &&
              this.c_hiretotallength.equals(other.getC_hiretotallength()))) &&
            ((this.c_hireinfo==null && other.getC_hireinfo()==null) || 
             (this.c_hireinfo!=null &&
              this.c_hireinfo.equals(other.getC_hireinfo()))) &&
            ((this.c_polity==null && other.getC_polity()==null) || 
             (this.c_polity!=null &&
              this.c_polity.equals(other.getC_polity()))) &&
            ((this.c_weavetype==null && other.getC_weavetype()==null) || 
             (this.c_weavetype!=null &&
              this.c_weavetype.equals(other.getC_weavetype()))) &&
            ((this.c_knowledge==null && other.getC_knowledge()==null) || 
             (this.c_knowledge!=null &&
              this.c_knowledge.equals(other.getC_knowledge()))) &&
            ((this.c_companymail==null && other.getC_companymail()==null) || 
             (this.c_companymail!=null &&
              this.c_companymail.equals(other.getC_companymail()))) &&
            ((this.c_mobile==null && other.getC_mobile()==null) || 
             (this.c_mobile!=null &&
              this.c_mobile.equals(other.getC_mobile()))) &&
            ((this.c_lut==null && other.getC_lut()==null) || 
             (this.c_lut!=null &&
              this.c_lut.equals(other.getC_lut())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getC_idcardflag() != null) {
            _hashCode += getC_idcardflag().hashCode();
        }
        if (getC_age() != null) {
            _hashCode += getC_age().hashCode();
        }
        if (getC_parttime() != null) {
            _hashCode += getC_parttime().hashCode();
        }
        if (getC_sljdsj() != null) {
            _hashCode += getC_sljdsj().hashCode();
        }
        if (getC_orgcode() != null) {
            _hashCode += getC_orgcode().hashCode();
        }
        if (getC_rsdate() != null) {
            _hashCode += getC_rsdate().hashCode();
        }
        if (getC_beginworkdate() != null) {
            _hashCode += getC_beginworkdate().hashCode();
        }
        if (getC_nationality() != null) {
            _hashCode += getC_nationality().hashCode();
        }
        if (getC_jobid() != null) {
            _hashCode += getC_jobid().hashCode();
        }
        if (getC_middlename() != null) {
            _hashCode += getC_middlename().hashCode();
        }
        if (getC_gljdsj() != null) {
            _hashCode += getC_gljdsj().hashCode();
        }
        if (getC_oid() != null) {
            _hashCode += getC_oid().hashCode();
        }
        if (getVersionid() != null) {
            _hashCode += getVersionid().hashCode();
        }
        if (getC_operator() != null) {
            _hashCode += getC_operator().hashCode();
        }
        if (getC_sortnumber() != null) {
            _hashCode += getC_sortnumber().hashCode();
        }
        if (getC_nccode() != null) {
            _hashCode += getC_nccode().hashCode();
        }
        if (getC_careerlevel() != null) {
            _hashCode += getC_careerlevel().hashCode();
        }
        if (getHkdjjg() != null) {
            _hashCode += getHkdjjg().hashCode();
        }
        if (getC_employeeid() != null) {
            _hashCode += getC_employeeid().hashCode();
        }
        if (getC_degree() != null) {
            _hashCode += getC_degree().hashCode();
        }
        if (getC_birthday() != null) {
            _hashCode += getC_birthday().hashCode();
        }
        if (getC_specialduty() != null) {
            _hashCode += getC_specialduty().hashCode();
        }
        if (getC_joinjobdate() != null) {
            _hashCode += getC_joinjobdate().hashCode();
        }
        if (getC_usedname() != null) {
            _hashCode += getC_usedname().hashCode();
        }
        if (getC_origin() != null) {
            _hashCode += getC_origin().hashCode();
        }
        if (getC_nativeplace() != null) {
            _hashCode += getC_nativeplace().hashCode();
        }
        if (getC_jobgrade() != null) {
            _hashCode += getC_jobgrade().hashCode();
        }
        if (getC_sourcetype() != null) {
            _hashCode += getC_sourcetype().hashCode();
        }
        if (getC_orgid() != null) {
            _hashCode += getC_orgid().hashCode();
        }
        if (getC_occupation() != null) {
            _hashCode += getC_occupation().hashCode();
        }
        if (getC_firstname() != null) {
            _hashCode += getC_firstname().hashCode();
        }
        if (getC_jobcode() != null) {
            _hashCode += getC_jobcode().hashCode();
        }
        if (getC_jobclass() != null) {
            _hashCode += getC_jobclass().hashCode();
        }
        if (getC_jobname() != null) {
            _hashCode += getC_jobname().hashCode();
        }
        if (getC_gender() != null) {
            _hashCode += getC_gender().hashCode();
        }
        if (getC_hiredate() != null) {
            _hashCode += getC_hiredate().hashCode();
        }
        if (getC_marital() != null) {
            _hashCode += getC_marital().hashCode();
        }
        if (getC_idcard() != null) {
            _hashCode += getC_idcard().hashCode();
        }
        if (getC_vstatus() != null) {
            _hashCode += getC_vstatus().hashCode();
        }
        if (getC_name() != null) {
            _hashCode += getC_name().hashCode();
        }
        if (getC_regresidence() != null) {
            _hashCode += getC_regresidence().hashCode();
        }
        if (getC_birthplace() != null) {
            _hashCode += getC_birthplace().hashCode();
        }
        if (getC_zyjndj() != null) {
            _hashCode += getC_zyjndj().hashCode();
        }
        if (getC_photo() != null) {
            _hashCode += getC_photo().hashCode();
        }
        if (getC_pcaccount() != null) {
            _hashCode += getC_pcaccount().hashCode();
        }
        if (getC_dispatch() != null) {
            _hashCode += getC_dispatch().hashCode();
        }
        if (getC_unitname() != null) {
            _hashCode += getC_unitname().hashCode();
        }
        if (getC_lastname() != null) {
            _hashCode += getC_lastname().hashCode();
        }
        if (getC_employeetype() != null) {
            _hashCode += getC_employeetype().hashCode();
        }
        if (getC_status() != null) {
            _hashCode += getC_status().hashCode();
        }
        if (getC_rsshr() != null) {
            _hashCode += getC_rsshr().hashCode();
        }
        if (getC_group() != null) {
            _hashCode += getC_group().hashCode();
        }
        if (getVersioncode() != null) {
            _hashCode += getVersioncode().hashCode();
        }
        if (getC_positionfamily() != null) {
            _hashCode += getC_positionfamily().hashCode();
        }
        if (getC_companyage() != null) {
            _hashCode += getC_companyage().hashCode();
        }
        if (getC_passport() != null) {
            _hashCode += getC_passport().hashCode();
        }
        if (getC_joinunitdate() != null) {
            _hashCode += getC_joinunitdate().hashCode();
        }
        if (getC_unitcode() != null) {
            _hashCode += getC_unitcode().hashCode();
        }
        if (getVersiontime() != null) {
            _hashCode += getVersiontime().hashCode();
        }
        if (getHkxx() != null) {
            _hashCode += getHkxx().hashCode();
        }
        if (getC_jobseniority() != null) {
            _hashCode += getC_jobseniority().hashCode();
        }
        if (getC_basestatus() != null) {
            _hashCode += getC_basestatus().hashCode();
        }
        if (getC_orgname() != null) {
            _hashCode += getC_orgname().hashCode();
        }
        if (getC_transfer() != null) {
            _hashCode += getC_transfer().hashCode();
        }
        if (getC_code() != null) {
            _hashCode += getC_code().hashCode();
        }
        if (getC_careerplan() != null) {
            _hashCode += getC_careerplan().hashCode();
        }
        if (getC_13762586955440() != null) {
            _hashCode += getC_13762586955440().hashCode();
        }
        if (getC_specialty() != null) {
            _hashCode += getC_specialty().hashCode();
        }
        if (getC_joblevel() != null) {
            _hashCode += getC_joblevel().hashCode();
        }
        if (getC_lastupdattime() != null) {
            _hashCode += getC_lastupdattime().hashCode();
        }
        if (getC_department() != null) {
            _hashCode += getC_department().hashCode();
        }
        if (getC_politicbusiness() != null) {
            _hashCode += getC_politicbusiness().hashCode();
        }
        if (getC_unitid() != null) {
            _hashCode += getC_unitid().hashCode();
        }
        if (getC_remark() != null) {
            _hashCode += getC_remark().hashCode();
        }
        if (getC_operatetime() != null) {
            _hashCode += getC_operatetime().hashCode();
        }
        if (getC_nation() != null) {
            _hashCode += getC_nation().hashCode();
        }
        if (getC_employeestatus() != null) {
            _hashCode += getC_employeestatus().hashCode();
        }
        if (getVersiondelete() != null) {
            _hashCode += getVersiondelete().hashCode();
        }
        if (getC_hiretotallength() != null) {
            _hashCode += getC_hiretotallength().hashCode();
        }
        if (getC_hireinfo() != null) {
            _hashCode += getC_hireinfo().hashCode();
        }
        if (getC_polity() != null) {
            _hashCode += getC_polity().hashCode();
        }
        if (getC_weavetype() != null) {
            _hashCode += getC_weavetype().hashCode();
        }
        if (getC_knowledge() != null) {
            _hashCode += getC_knowledge().hashCode();
        }
        if (getC_companymail() != null) {
            _hashCode += getC_companymail().hashCode();
        }
        if (getC_mobile() != null) {
            _hashCode += getC_mobile().hashCode();
        }
        if (getC_lut() != null) {
            _hashCode += getC_lut().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Iam_empbaseinfoLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "iam_empbaseinfoLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_idcardflag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_idcardflag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_age");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_age"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_parttime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_parttime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_sljdsj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_sljdsj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_orgcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_orgcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_rsdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_rsdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_beginworkdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_beginworkdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_nationality");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_nationality"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_jobid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_middlename");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_middlename"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_gljdsj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_gljdsj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_oid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_oid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "versionid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_operator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_operator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_sortnumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_sortnumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_nccode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_nccode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_careerlevel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_careerlevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hkdjjg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "hkdjjg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_employeeid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_employeeid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_degree");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_degree"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_birthday");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_birthday"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_specialduty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_specialduty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_joinjobdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_joinjobdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_usedname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_usedname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_origin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_origin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_nativeplace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_nativeplace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobgrade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_jobgrade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_sourcetype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_sourcetype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_orgid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_orgid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_occupation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_occupation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_firstname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_firstname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_jobcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobclass");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_jobclass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_jobname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_gender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_hiredate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_hiredate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_marital");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_marital"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_idcard");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_idcard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_vstatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_vstatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_regresidence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_regresidence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_birthplace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_birthplace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_zyjndj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_zyjndj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_photo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_photo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_pcaccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_pcaccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_dispatch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_dispatch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_unitname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_unitname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_lastname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_lastname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_employeetype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_employeetype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_rsshr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_rsshr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_group");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_group"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versioncode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "versioncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_positionfamily");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_positionfamily"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_companyage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_companyage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_passport");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_passport"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_joinunitdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_joinunitdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_unitcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_unitcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versiontime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "versiontime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hkxx");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "hkxx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobseniority");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_jobseniority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_basestatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_basestatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_orgname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_orgname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_transfer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_transfer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_careerplan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_careerplan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_13762586955440");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_13762586955440"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_specialty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_specialty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_joblevel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_joblevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_lastupdattime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_lastupdattime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_department");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_department"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_politicbusiness");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_politicbusiness"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_unitid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_unitid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_remark");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_remark"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_operatetime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_operatetime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_nation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_nation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_employeestatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_employeestatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versiondelete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "versiondelete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_hiretotallength");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_hiretotallength"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_hireinfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_hireinfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_polity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_polity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_weavetype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_weavetype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_knowledge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_knowledge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_companymail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_companymail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_mobile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_mobile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_lut");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_lut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
