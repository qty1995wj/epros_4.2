/*
 * Rect.java
 *岗位元素
 * Created on 2008年11月3日, 上午11:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseRect;

/**
 * 岗位图标
 * 
 * @author Administrator
 */
public class PositionFigure extends BaseRect {

	public String jTextAreaSchoolRecordStation; // 学历
	public String jTextAreaKnowladgeStation;// 知识
	public String jTextAreaSkillStation; // 技能
	public String jTextAreaCoreValueStation; // 核心价值
	public String jTextAreaDiathesisModel;// 素质模型

	public PositionFigure(JecnFigureData figureData) {
		super(figureData);
	}
}
