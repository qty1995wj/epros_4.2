package epros.designer.gui.recycle;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnRecycleMenu;
import epros.designer.util.ConnectionPool;

public class RuleRecycleMenu extends JecnRecycleMenu {
	private static Logger log = Logger.getLogger(RuleRecycleMenu.class);

	public RuleRecycleMenu(RecycleTree jTree) {
		super(jTree);
	}

	@Override
	public boolean doRecycleFullData() {
		boolean flag = true;
		try {
			// 恢复当前节点以及子节点
			ConnectionPool.getRuleAction().getRecycleFileIds(super.getRecycleNodesIdList(), JecnConstants.projectId);
		} catch (Exception e) {
			flag = false;
			log.error("RuleRecycleMenu method doRecycleFullData error", e);
		}

		return flag;
	}

	@Override
	public boolean doRecycleData() {
		boolean flag = true;
		try {
			// 恢复当前节点
			ConnectionPool.getRuleAction().updateRecycleFile(super.getRecycleNodesIdList(), JecnConstants.projectId);
		} catch (Exception e) {
			flag = false;
			log.error("RuleRecycleMenu method doRecycleData error", e);
		}
		return flag;
	}

	@Override
	protected boolean trueDelete() {
		boolean flag = true;
		try {
			ConnectionPool.getRuleAction().deleteRule(super.getRecycleNodesIdList(), JecnConstants.getUserId());
		} catch (Exception e) {
			flag = false;
			log.error("RuleRecycleMenu method trueDelete error", e);
		}
		return flag;
	}
}
