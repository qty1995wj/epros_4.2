package epros.draw.gui.top.toolbar.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.data.workflow.JecnPartMapCreateData;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 创建流程图时弹出的对话框
 * 
 * @author ZHOUX
 * 
 */
public class JecnCreatePartMapDialog extends JecnDialog implements ActionListener, CaretListener, ItemListener {
	/** 内容面板 */
	private JecnPanel mainPanel = null;

	/** 流程图名称标签 */
	private JLabel nameLabel = null;
	/** 流程图名称 */
	private JTextField nameTextField = null;
	/** 提示信息框 */
	private JecnUserInfoTextArea infoTextArea = null;

	/** 角色、活动 总面板 */
	private JecnPanel roleRoundRectPanel = null;
	/** 预估角色 */
	private JLabel roleLabel = null;
	private JSpinner roleSpinner = null;
	/** 预估活动 */
	private JLabel roundRectLabel = null;
	private JSpinner roundRectSpinner = null;

	/** 横向纵向总面板 */
	private JecnPanel vhPanel = null;
	/** 横向面板 */
	private JPanel hPanel = null;
	/** 纵向面板 */
	private JPanel vPanel = null;
	/** 横向图片标签 */
	private JLabel hLabel = null;
	/** 横向单选框 */
	private JRadioButton hRadioBtn = null;
	/** 纵向图片标签 */
	private JLabel vLabel = null;
	/** 纵向单选框 */
	private JRadioButton vRadioBtn = null;

	/** 确认取消按钮面板 */
	private JecnOKCancelPanel okCancelPanel = null;

	/** 创建流程图时前提条件对应的数据对象 */
	private JecnPartMapCreateData partMapCreateData = null;
	/** 点击确认按钮标识 */
	private boolean okFlag = false;

	public JecnCreatePartMapDialog() {
		initComponents();
		initLayout();
	}

	private void initComponents() {
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("newFlowMap"));

		// 创建流程图时前提条件对应的数据对象
		partMapCreateData = new JecnPartMapCreateData();

		// 内容面板
		mainPanel = new JecnPanel();
		// 名称标签
		nameLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("partMapName"));
		// 流程图名称
		nameTextField = new JTextField();
		// 提示信息框
		infoTextArea = new JecnUserInfoTextArea();

		// 角色、活动 总面板
		roleRoundRectPanel = new JecnPanel();
		// 横向纵向总面板
		vhPanel = new JecnPanel();
		// 横向面板
		hPanel = new JPanel();
		// 纵向面板
		vPanel = new JPanel();

		// 预估角色
		roleLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("roleLabel"));
		roleSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));
		// 预估活动
		roundRectLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("roundRectLabel"));
		roundRectSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));

		// 横向图片标签
		hLabel = new JLabel(JecnFileUtil.getCreatePartMapIcon("h.gif"));
		// 横向选中框
		hRadioBtn = new JRadioButton(JecnResourceUtil.getJecnResourceUtil().getValue("hRadioButton"));
		// 纵向图片标签
		vLabel = new JLabel(JecnFileUtil.getCreatePartMapIcon("v.gif"));
		// 纵向选中框
		vRadioBtn = new JRadioButton(JecnResourceUtil.getJecnResourceUtil().getValue("vRadioButton"));

		// 确认取消按钮面板
		okCancelPanel = new JecnOKCancelPanel();

		Dimension size = new Dimension(375, 315);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setSize(size);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		// 布局管理器
		hPanel.setLayout(new BorderLayout());
		vPanel.setLayout(new BorderLayout());

		// 标题
		// 角色、活动 总面板
		roleRoundRectPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				JecnResourceUtil.getJecnResourceUtil().getValue("roleRoundRectBorderTitle")));
		// 横向纵向总面板
		vhPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("vhBorderTitle")));

		// 大小
		// 流程图名称
		Dimension contentSize = new Dimension(175, 20);
		nameTextField.setPreferredSize(contentSize);
		nameTextField.setMinimumSize(contentSize);

		Dimension spinnerSize = new Dimension(77, 20);
		// 角色
		roleSpinner.setPreferredSize(spinnerSize);
		// 活动
		roundRectSpinner.setPreferredSize(spinnerSize);

		// 背景颜色和前景颜色
		// 角色、活动 总面板
		roleRoundRectPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 横向纵向总面板
		vhPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 横向单选框
		hRadioBtn.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 纵向单选框
		vRadioBtn.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 事件
		nameTextField.addCaretListener(this);
		okCancelPanel.getOkBtn().addActionListener(this);
		okCancelPanel.getCancelBtn().addActionListener(this);
		hRadioBtn.addItemListener(this);
		vRadioBtn.addItemListener(this);

		ButtonGroup group = new ButtonGroup();
		group.add(hRadioBtn);
		group.add(vRadioBtn);
		hRadioBtn.setSelected(true);

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				okFlag = false;
				return true;
			}
		});
	}

	/**
	 * 
	 * 
	 * 
	 */
	private void initLayout() {
		initOneLayout();
		// 布局角色、活动 总面板
		initRoleRectPanelLayout();
		// 布局横向、纵向总面板
		vhPanelLayout();
	}

	private void initOneLayout() {
		this.getContentPane().add(mainPanel);

		// 内容面板new Insets(20, 25, 0, 30)
		Insets labelInsets = new Insets(20, 25, 0, 0);
		Insets nameInsets = new Insets(20, 3, 0, 30);
		Insets infoTextAreaInsets = new Insets(0, 30, 0, 10);
		Insets okCancelPanelInsets = new Insets(5, 0, 0, 30);

		Insets roleRoundRectInsets = new Insets(20, 25, 0, 30);
		Insets vhInsets = new Insets(0, 25, 0, 30);

		// **********第一行**********//
		// 流程图名称标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, labelInsets, 0, 0);
		mainPanel.add(nameLabel, c);
		// 流程图名称
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				nameInsets, 0, 0);
		mainPanel.add(nameTextField, c);
		// **********第一行**********//

		// 提示信息框
		c = new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				infoTextAreaInsets, 0, 0);
		mainPanel.add(infoTextArea, c);

		// 角色、活动 总面板
		c = new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				roleRoundRectInsets, 0, 0);
		mainPanel.add(roleRoundRectPanel, c);
		// 横向纵向总面板
		c = new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				vhInsets, 0, 0);
		mainPanel.add(vhPanel, c);

		// 空闲区域
		c = new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				infoTextAreaInsets, 0, 0);
		mainPanel.add(new JLabel(), c);

		// 确认取消按钮面板
		c = new GridBagConstraints(0, 5, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				okCancelPanelInsets, 0, 0);
		mainPanel.add(okCancelPanel, c);
	}

	/**
	 * 
	 * 布局角色、活动总面板
	 * 
	 */
	private void initRoleRectPanelLayout() {
		// 角色、活动 总面板
		// 最内部面板的第一个组件的间距
		Insets insetsFirst = new Insets(9, 3, 9, 3);
		// 标签间距
		Insets insetsLabel = new Insets(9, 11, 9, 3);
		// 内容间距
		Insets insetsContent = new Insets(9, 0, 9, 7);

		// 左面元素:预估角色
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsFirst, 0, 0);
		roleRoundRectPanel.add(roleLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		roleRoundRectPanel.add(roleSpinner, c);

		// 空闲区域
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsLabel, 0, 0);
		roleRoundRectPanel.add(new JLabel(), c);

		// 右面元素:预估活动
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		roleRoundRectPanel.add(roundRectLabel, c);
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		roleRoundRectPanel.add(roundRectSpinner, c);
	}

	/**
	 * 
	 * 布局横向、纵向总面板
	 * 
	 */
	private void vhPanelLayout() {
		// 横向纵向总面板
		Insets hInsets = new Insets(9, 85, 0, 40);
		Insets vInsets = new Insets(9, 0, 0, 72);
		// 横向面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, hInsets, 0, 0);
		vhPanel.add(hPanel, c);

		// 纵向面板
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				vInsets, 0, 0);
		vhPanel.add(vPanel, c);

		// 横向图片标签
		hPanel.add(this.hLabel, BorderLayout.NORTH);
		// 横向单选框
		hPanel.add(this.hRadioBtn, BorderLayout.CENTER);

		// 纵向图片标签
		vPanel.add(this.vLabel, BorderLayout.NORTH);
		// 纵向单选框
		vPanel.add(this.vRadioBtn, BorderLayout.CENTER);

	}

	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	/**
	 * 
	 * 横纵向单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		itemStateChangedProcess(e);
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	private void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == nameTextField) {// 流程图名称
			checkName();
		}
	}

	private void itemStateChangedProcess(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {// 选中
			if (e.getSource() == hRadioBtn) { // 横向单选框
				partMapCreateData.setHFlag(true);
			} else if (e.getSource() == vRadioBtn) {// 纵向单选框
				partMapCreateData.setHFlag(false);
			}
		}
	}

	private void actionPerformedProcess(ActionEvent e) {
		if (okCancelPanel.getOkBtn() == e.getSource()) {// 确认按钮
			// 校验数据
			boolean ret = checkName();
			if (ret) {// 赋值给数据对象
				// 名称
				this.partMapCreateData.setName(nameTextField.getText());
				// 角色个数
				this.partMapCreateData.setRoleCount(Integer.valueOf(this.roleSpinner.getValue().toString()));
				// 活动个数
				this.partMapCreateData.setRoundRectCount(Integer.valueOf(this.roundRectSpinner.getValue().toString()));
				okFlag = true;
			} else {
				okFlag = true;
				return;
			}
		} else if (okCancelPanel.getCancelBtn() == e.getSource()) {// 取消按钮
			okFlag = false;
		}

		this.setVisible(false);
	}

	/**
	 * 
	 * 在名称输入框中输入字符时，验证内容是否正确
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	private boolean checkName() {
		String text = nameTextField.getText();
		// 验证名称是否合法
		String checkString = JecnUserCheckUtil.checkName(text);
		return infoTextArea.checkInfo(checkString);
	}

	public JecnPartMapCreateData getPartMapCreateData() {
		return partMapCreateData;
	}

	public boolean isOkFlag() {
		return okFlag;
	}
}
