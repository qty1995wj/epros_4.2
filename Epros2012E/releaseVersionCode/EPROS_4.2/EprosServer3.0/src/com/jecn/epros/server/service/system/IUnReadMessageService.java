package com.jecn.epros.server.service.system;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.bean.system.JecnMessageReaded;

/***
 * 未读消息接口
 * @author 
 * 2013-03-05
 *
 */
public interface IUnReadMessageService {
	
	/****
	 * 获取所有未读消息
	 * @param startNum
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<JecnMessage> getAllUnReadMesgList(int startNum,int limit,Long inceptPeopleId) throws Exception;
	/***
	 *  获取未读数据的条数
	 *  @param inceptPeopleId 收件人ID(登录人员ID)
	 * @return
	 */
	public int getTotalUnMessageList(Long inceptPeopleId) throws Exception;
	/***
	 * 根据消息主键ID 查询消息详细信息
	 * @param messageId  主键ID
	 */
	public JecnMessageReaded searchUnReadMesgById(Long messageId,String peopleName) throws Exception;
	
	/***
	 * 删除未读消息
	 * @param mesgId  消息主键ID
	 * @throws Exception
	 */
	public void deleteUnReadMesgs(List<Long> mesgId) throws Exception;
	/***
	 * 未读消息数量
	 */
	
	public int getUnReadMesgNum(Long inceptPeopleId) throws Exception;
	
	/**
	 * @author zhangft 2013-7-25
	 * @description 标记未读消息
	 * @param msgIdList 消息主键id
	 * @return
	 */
	public void tagUnReadMesgs(List<Long> msgIdList) throws Exception;
}
