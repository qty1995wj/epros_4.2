package com.jecn.epros.server.action.web.login.ad.zhongdiantou;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;

import sun.misc.BASE64Decoder;

public class ZhongDTAES {
	static Cipher cipher;
	static final String KEY_ALGORITHM = "AES";
	static final String CIPHER_ALGORITHM_ECB = "AES/ECB/PKCS5Padding";
	/** 公司key 86 */
	static final int EPROS_KEY = 86;

	/**
	 * KEY解密
	 * 
	 * @param n
	 *            待解密的key
	 * @param key
	 *            公司key 86
	 * @return 解密后的随机数;
	 * @throws Exception
	 */
	public static String getKey(String n, int key) throws Exception {
		long l = Long.valueOf(n).longValue();
		l = (l - 51) / key;
		String decryptKey = String.valueOf(l);
		return decryptKey;
	}

	/**
	 * base 64 decode
	 * 
	 * @param base64Code
	 *            待解码的base 64 code
	 * @return 解码后的byte[]
	 * @throws Exception
	 */
	public static byte[] base64Decode(String base64Code) throws Exception {
		return new BASE64Decoder().decodeBuffer(base64Code);
	}

	/**
	 * AES解密
	 * 
	 * @param encryptStrBase64
	 *            待解密的Base64
	 * @param decryptKey
	 *            解密密钥
	 * @return 解密后的userID;
	 * @throws Exception
	 */

	public static String aesDecryptByBytes(String u, String decryptKey) throws Exception {
		byte[] encryptBytes = base64Decode(u);
		cipher = Cipher.getInstance(CIPHER_ALGORITHM_ECB);
		// KeyGenerator 生成aes算法密钥
		KeyGenerator secretKey1 = KeyGenerator.getInstance(KEY_ALGORITHM);
		secretKey1.init(128, new SecureRandom(decryptKey.getBytes()));
		// 使用解密模式初始化 密钥
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(secretKey1.generateKey().getEncoded(), KEY_ALGORITHM));
		byte[] decrypt = cipher.doFinal(encryptBytes);
		String userID = new String(decrypt, "utf-8");
		return userID;
	}

	/**
	 * 拆分字符串
	 * 
	 * @param userID
	 *            用户ｉｄ带随机字母
	 * @return　用户ｉｄ
	 */
	public static String splitReal(String userID) {
		int i = 0;
		int j = 0;
		char newChar[] = userID.toCharArray();
		char[] tmp = new char[newChar.length / 2 + 1];
		for (char ch : newChar) { // 遍历List集合
			if (i == newChar.length / 2 + 1) {
				break;
			}
			if (j > 0 && j % 2 == 1) {
				j++;
				continue;
			} else {
				tmp[i] = ch;
				i++;
				j++;
			}
		}
		String real = new String(tmp);
		return real;
	}

	public static String decrypt(String key, String loginName) throws Exception {
		if (StringUtils.isEmpty(key) || StringUtils.isEmpty(loginName)) {
			throw new IllegalArgumentException("参数非法！ key=" + key + " loginName = " + loginName);
		}
		// 解密Key
		String decryptKey = getKey(key, EPROS_KEY);
		String aesDecryptStr = aesDecryptByBytes(loginName, decryptKey);
		return splitReal(aesDecryptStr);
	}

	public static void main(String[] args) {
		// “u” :”UBvASljtuxILemeX23snHQ==”, //加密后的用户名
		// “n” : “94183042724117583”//经过处理后的随机数
		String key = "100827680183466325";
		String loginName = "+zYgxksgykgm6X4PPylB7Q==";
		try {
			String decryptKey = ZhongDTAES.decrypt(key, loginName);
			System.out.println(decryptKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}