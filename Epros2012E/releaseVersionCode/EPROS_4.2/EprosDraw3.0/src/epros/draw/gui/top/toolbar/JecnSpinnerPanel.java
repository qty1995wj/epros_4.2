package epros.draw.gui.top.toolbar;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import epros.draw.system.JecnUserCheckInfoProcess;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 右下边距面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSpinnerPanel extends JPanel implements ChangeListener {
	/** 选择控件 */
	private JSpinner spinner = null;
	/** 标题 */
	private JLabel label = null;

	private AutoSizeType autoSizeType = null;

	public JecnSpinnerPanel(SpinnerModel model, String text,
			AutoSizeType autoSizeType) {
		this.autoSizeType = autoSizeType;

		initComponent(model, text);
		initData();
	}

	private void initComponent(SpinnerModel model, String text) {
		// 选择控件
		spinner = new JSpinner(model);
		// 标题
		label = new JLabel();

		this.setOpaque(false);
		this.setBorder(null);

		// 标题
		label.setText((text == null) ? "" : text);

		this.setLayout(new BorderLayout());
		this.add(label, BorderLayout.WEST);
		this.add(spinner, BorderLayout.CENTER);

		spinner.addChangeListener(this);
		spinner.setToolTipText("100~10000");
	}

	private void initData() {
		int size = 100;
		if (AutoSizeType.width.toString().equals(autoSizeType.toString())) {
			size = JecnSystemStaticData.getWorkflowResizeWidth();
		} else {
			size = JecnSystemStaticData.getWorkflowResizeHeight();
		}
		this.spinner.setValue(size);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		Object obj = spinner.getValue();
		if (obj instanceof Number) {
			int size = Integer.parseInt(obj.toString());

			if (AutoSizeType.width.toString().equals(autoSizeType.toString())) {
				if (size == JecnSystemStaticData.getWorkflowResizeWidth()) {
					return;
				}
				JecnSystemStaticData.setWorkflowResizeWidth(size);
			} else {
				if (size == JecnSystemStaticData.getWorkflowResizeHeight()) {
					return;
				}
				JecnSystemStaticData.setWorkflowResizeHeight(size);
			}
			// 本地保存
			JecnUserCheckInfoProcess.writeSetFile();
		}
	}

	enum AutoSizeType {
		width, heigth
	}
}
