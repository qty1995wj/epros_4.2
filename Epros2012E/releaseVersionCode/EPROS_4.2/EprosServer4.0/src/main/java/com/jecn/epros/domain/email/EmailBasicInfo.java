package com.jecn.epros.domain.email;

import com.jecn.epros.domain.JecnUser;

import java.util.ArrayList;
import java.util.List;

public class EmailBasicInfo {

    /**
     * 邮件主题
     */
    private String subject;
    /**
     * 邮件正文
     */
    private String content;
    /**
     * 收件人集合
     */
    private List<JecnUser> recipients = new ArrayList<JecnUser>();
    /**
     * 附件
     */
    private List<AttachmentFile> attachments = new ArrayList<AttachmentFile>();

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void addRecipients(JecnUser user) {
        this.recipients.add(user);
    }

    public void addRecipients(List<JecnUser> users) {
        this.recipients.addAll(users);
    }

    public List<AttachmentFile> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentFile> attachments) {
        this.attachments = attachments;
    }

    public List<JecnUser> getRecipients() {
        return recipients;
    }


}
