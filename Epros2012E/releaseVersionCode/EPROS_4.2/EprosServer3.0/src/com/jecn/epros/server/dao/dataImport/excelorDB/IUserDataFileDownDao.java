package com.jecn.epros.server.dao.dataImport.excelorDB;

import java.util.List;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public interface IUserDataFileDownDao extends IBaseDao<DeptBean, Long> {
	/**
	 * 
	 * 获取部门所有数据（部门编号、部门名称、上级部门编号）
	 * 
	 */
	List<DeptBean> selectDeptList() throws Exception;

	/**
	 * 
	 * 获取人员岗位以及他们之间关联关系数据
	 * 
	 */
	List<UserBean> selectUserPosList() throws Exception;

}
