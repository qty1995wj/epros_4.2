package epros.draw.gui.top.toolbar.io;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import epros.draw.util.JecnResourceUtil;

/**
 * 接收传入的文件后缀名，过滤文件
 * 
 * @author Administrator
 * 
 */
public class ImportFlowFilter extends FileFilter {
	private String endName = "";

	public ImportFlowFilter(String name) {
		endName = name;
	}

	/**
	 * 
	 * 确认哪些显示在确认文件对话框中
	 * 
	 * 判断返回的是目录，还是文件（文件必须为传入的后缀名称）
	 * 
	 */
	public boolean accept(File f) {
		return f.isDirectory() || (f.isFile() && f.getName() != null && f.getName().endsWith("." + endName));
	}

	public String getDescription() {
		return endName + JecnResourceUtil.getJecnResourceUtil().getValue("fileChooseTypeName") + "(*." + endName + ")";
	}
}
