package epros.designer.service.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowFigureNow;

import epros.designer.util.ConnectionPool;
import epros.draw.system.JecnFlowElementForXML;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.util.DrawCommon;

public class ProcessFigureAttributeService {
	private static Logger log = Logger.getLogger(ProcessFigureAttributeService.class);
	public  static void fun(){
		List<JecnFlowFigureNow> listf=new ArrayList<JecnFlowFigureNow>();
		List<JecnDefaultFlowElementData> list=  JecnFlowElementForXML.readSystemInitModelByXML();
		for(JecnDefaultFlowElementData defaultFlowElementData:list){
			JecnFlowFigureNow flowFigureNow=new JecnFlowFigureNow();
			//主键
			//flowFigureNow.setId(defaultFlowElementData.getFlowElementId());
			flowFigureNow.setFigureText(defaultFlowElementData.getFigureText());
			//元素名称  元素类型
			flowFigureNow.setFigureName(defaultFlowElementData.getMapElemType().toString());
			//元素宽度
			flowFigureNow.setFigureWidth(new Long(defaultFlowElementData.getFigureSizeX()));
			//元素高度
			flowFigureNow.setFigureHeight(new Long(defaultFlowElementData.getFigureSizeY()));
			//是否为阴影效果:0是;1否
			flowFigureNow.setIsShadow(defaultFlowElementData.getShadow());
			//是否为3D效果:0是;1否
			flowFigureNow.setIs3D(defaultFlowElementData.getFlag3D()?0:1);
			//是否为竖排文字:0是;1否
			flowFigureNow.setIsVertical(defaultFlowElementData.getUpRight());
			//填充(背景)颜色
			flowFigureNow.setBgColor(DrawCommon.coverColorToString(defaultFlowElementData.getFillColor()));
			//边框颜色
			flowFigureNow.setBorderColor(DrawCommon.coverColorToString(defaultFlowElementData.getBodyColor()));
			//字体颜色
			flowFigureNow.setFontColor(DrawCommon.coverColorToString(defaultFlowElementData.getFontColor()));
			//字体大小
			flowFigureNow.setFontSize(new Long(defaultFlowElementData.getFontSize()));
			//字体位置
			flowFigureNow.setFontLocation(String.valueOf(defaultFlowElementData.getFontPosition()));
			//边框样式
			flowFigureNow.setBorderStyle(String.valueOf(defaultFlowElementData.getBodyStyle()));
			flowFigureNow.setFillType(defaultFlowElementData.getFillType().toString());
			flowFigureNow.setChangeColor(DrawCommon.coverColorToString(defaultFlowElementData.getChangeColor()));
			 /**渐变方向类型——new*/
			flowFigureNow.setFillColorChangType(defaultFlowElementData.getFillColorChangType().toString());
			 /**字形样式——new*/
			flowFigureNow.setFontStyle(defaultFlowElementData.getFontStyle());
			 /**字体名称——new*/
			flowFigureNow.setFontName(defaultFlowElementData.getFontName());
			 /**阴影颜色——new*/
			flowFigureNow.setShadowColor(DrawCommon.coverColorToString(defaultFlowElementData.getShadowColor()));
			  /**线条粗细———new*/
			flowFigureNow.setBodyWidth(defaultFlowElementData.getBodyWidth());
			flowFigureNow.setEidtPoint(defaultFlowElementData.getEidtPoint()?1:0);
			// 是否支持双击：isDoubleClick
			flowFigureNow.setIsDoubleClick(defaultFlowElementData.getDoubleClick()?1:0);
			// 是否支持快速添加图形：isSortAddFigure
			flowFigureNow.setIsSortAddFigure(defaultFlowElementData.getSortAddFigure()?1:0);
			listf.add(flowFigureNow);
		}
		
		
		try {
			ConnectionPool.getProcessAction().updateFigureAttribute(listf);
		} catch (Exception e1) {
			log.error("ProcessFigureAttributeService is error",e1);
		}
	}
}
