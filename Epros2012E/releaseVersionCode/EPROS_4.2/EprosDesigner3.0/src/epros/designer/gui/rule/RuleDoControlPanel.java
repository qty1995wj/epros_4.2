package epros.designer.gui.rule;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.publish.EditPublishDialog;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 制度文控信息
 * 
 * @author 2012-06-07
 * 
 */
public class RuleDoControlPanel extends JecnDialog {

	private static Logger log = Logger.getLogger(RuleDoControlPanel.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = new JScrollPane();

	/** 信息显示Table */
	private RuleDocControlTable ruleDocControlTable = null;

	/** 设置大小 */
	private Dimension dimension = null;

	/** 编辑 */
	private JButton editBut = new JButton(JecnProperties.getValue("editBtn"));

	/** 删除 */
	private JButton deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));
	/** 查阅 */
	private JButton searchBut = new JButton(JecnProperties.getValue("inspection"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 操作选择提示框 */
	private JLabel chooseLab = new JLabel();

	private Long docId = null;

	private List<JecnTaskHistoryNew> listHistory = new ArrayList<JecnTaskHistoryNew>();
	private String versionId = null;

	/** 编辑的文控信息Bean */
	private JecnTaskHistoryNew taskHisNew = null;

	private Long treeId = null;
	/** 选中的PRF节点 */
	private JecnTreeNode selectNode = null;
	/** 文控信息类型： 0：流程图，1：文件，2：制度模版文件，3：制度文件，4：流程地图 */
	private int editType = 0;
	/** 文控显示内容类型： 0：流程，1：文件，2：制度 */
	private int contType = 0;
	/** 编辑，删除按钮是否能使用 */
	boolean isEdit = false;

	private void init() {
		initEdit();
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setSize(670, 500);
		// 设置窗体大小
		this.setSize(670, 500);
		this.setTitle(JecnProperties.getValue("historyRecord"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);
		this.setModal(true);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dimension = new Dimension(620, 360);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.setMaximumSize(dimension);
		resultScrollPane.setMinimumSize(dimension);

		chooseLab.setForeground(Color.red);

		ruleDocControlTable = new RuleDocControlTable();
		// 设置默认背景色
		ruleDocControlTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ruleDocControlTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		resultScrollPane.setViewportView(ruleDocControlTable);

		initLayout();
		/***********************************************************************
		 * 编辑
		 */
		editBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}

		});
		/***********************************************************************
		 * 删除
		 */
		deleteBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}

		});
		/***********************************************************************
		 * 查阅
		 */
		searchBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				searchButPerformed();

			}

		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});
		// 选中一条文控信息 ，双击表格，打开文控编辑框
		ruleDocControlTable.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					ruleDocControlTableMousePressed();
				}
			}
		});
	}

	private void initEdit() {

		switch (editType) {
		case 0:
		case 4:
			if (JecnConstants.loginBean.isDesignProcessAdmin()) {
				isEdit = true;
			} else if (JecnDesignerCommon.isAuthorAdmin()) {
				isEdit = true;
			}
			break;
		case 2:
		case 3:
			if (JecnConstants.loginBean.isDesignRuleAdmin()) {
				isEdit = true;
			} else if (JecnDesignerCommon.isAuthorAdmin()) {
				isEdit = true;
			}
			break;
		case 1:
			if (JecnConstants.loginBean.isDesignFileAdmin()) {
				isEdit = true;
			} else if (JecnDesignerCommon.isAuthorAdmin()) {
				isEdit = true;
			}
			break;

		}
		if (isEdit) {
			deleteBut.setEnabled(true);
			editBut.setEnabled(true);
		} else {
			deleteBut.setEnabled(false);
			editBut.setEnabled(false);
		}
	}

	/**
	 * 流程文控信息 type ==0 文件文控信息 type==1
	 * 
	 * @param flowId
	 * @param type
	 */
	public RuleDoControlPanel(JecnTreeNode selectNode, Long flowId, int type) {
		this.selectNode = selectNode;
		this.treeId = flowId;
		this.editType = type;
		if (type == 0 || type == 4) {
			// 流程ID
			// this.treeId = flowId;
			// this.editType = type;
			this.contType = 0;
			try {
				listHistory = ConnectionPool.getDocControlAction().getJecnTaskHistoryNewList(flowId, type);
			} catch (Exception e) {
				log.error("RuleDoControlPanel RuleDoControlPanel is error", e);
			}
		} else if (type == 1) {
			this.contType = 1;
			try {
				listHistory = ConnectionPool.getDocControlAction().getJecnTaskHistoryNewList(treeId, type);
			} catch (Exception e) {
				log.error("RuleDoControlPanel RuleDoControlPanel is error", e);
			}
		}
		init();
	}

	/**
	 * 编辑制度，新建制度
	 * 
	 * @param selectNode
	 * @param treeId
	 */
	public RuleDoControlPanel(JecnTreeNode selectNode, Long treeId) {
		// 获取制度ID
		this.treeId = treeId;
		this.selectNode = selectNode;
		this.editType = selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.ruleFile ? 3 : 2;
		this.contType = 2;
		// 根据制度ID获取制度文控信息 JECN_TASK_HISTORY_NEW
		try {
			listHistory = ConnectionPool.getDocControlAction().getJecnTaskHistoryNewList(treeId,
					selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.ruleFile ? 3 : 2);
		} catch (Exception e1) {
			log.error("RuleDoControlPanel is error", e1);
		}
		init();
	}

	/***************************************************************************
	 * 双击文控表格事件
	 * 
	 */
	private void ruleDocControlTableMousePressed() {
		chooseLab.setText("");
		// 系统管理员验证,如果不是管理员登录则return
		if (!JecnDesignerCommon.isAdmin()) {
			if (contType == 0 && !JecnConstants.loginBean.isDesignProcessAdmin()) {// 流程
				return;
			} else if (contType == 1 && !JecnConstants.loginBean.isDesignFileAdmin()) {// 文件
				return;
			} else if (contType == 2 && !JecnConstants.loginBean.isDesignRuleAdmin()) {// 制度
				return;
			}
		}
		int selectRow = ruleDocControlTable.getSelectedRow();
		if (selectRow == -1) {
			chooseLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		if (isEdit) {
			openpublishPerformed(selectRow);
		}
	}

	/***************************************************************************
	 * 文控编辑面板
	 * 
	 */
	private void openpublishPerformed(int selectRow) {
		Vector<Vector<Object>> vectors = ((DefaultTableModel) ruleDocControlTable.getModel()).getDataVector();
		docId = Long.valueOf(vectors.get(selectRow).get(0).toString());
		EditPublishDialog editPublishDialog = new EditPublishDialog(docId, listHistory.get(selectRow), treeId,
				editType, selectNode);
		editPublishDialog.setVisible(true);
		if (editPublishDialog.isOperation()) {
			versionId = editPublishDialog.getVersonNumField().getText().trim();
			ruleDocControlTable.getModel().setValueAt(versionId, selectRow, 1);
			ruleDocControlTable.getModel().setValueAt(listHistory.get(selectRow).getDraftPerson(), selectRow, 2);
			taskHisNew = listHistory.get(selectRow);
			taskHisNew.setModifyExplain(editPublishDialog.getDescofChangeField().getText());
			taskHisNew.setVersionId(versionId);
			taskHisNew.setListJecnTaskHistoryFollow(editPublishDialog.getListHisFollow());
		}
		// 保存编辑的文控信息
		try {
			// 更新文控信息 updateJecnTaskHistoryNew
			ConnectionPool.getDocControlAction().updateJecnTaskHistoryNew(taskHisNew, versionId);
		} catch (Exception e) {
			log.error("RuleDoControlPanel openpublishPerformed is error", e);
		}
	}

	/***************************************************************************
	 * 编辑
	 */
	private void editButPerformed() {
		chooseLab.setText("");
		// 系统管理员验证,如果不是管理员登录则return
		if (!JecnDesignerCommon.isAuthorAdmin()) {
			if (contType == 0 && !JecnConstants.loginBean.isDesignProcessAdmin()) {// 流程
				return;
			} else if (contType == 1 && !JecnConstants.loginBean.isDesignFileAdmin()) {// 文件
				return;
			} else if (contType == 2 && !JecnConstants.loginBean.isDesignRuleAdmin()) {// 制度
				return;
			}
		}
		int selectRow = ruleDocControlTable.getSelectedRow();
		// 没有选中或者选中多行的情况都需要renturn
		if (selectRow == -1 || ruleDocControlTable.getSelectedRows().length > 1) {
			chooseLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		openpublishPerformed(selectRow);
	}

	/**
	 * 删除
	 */
	private void deleteButPerformed() {
		// 系统管理员验证,如果不是管理员登录则return
		if (!JecnDesignerCommon.isAuthorAdmin()) {
			if (contType == 0 && !JecnConstants.loginBean.isDesignProcessAdmin()) {// 流程
				return;
			} else if (contType == 1 && !JecnConstants.loginBean.isDesignFileAdmin()) {// 文件
				return;
			} else if (contType == 2 && !JecnConstants.loginBean.isDesignRuleAdmin()) {// 制度
				return;
			}
		}
		chooseLab.setText("");
		int[] selectRows = ruleDocControlTable.getSelectedRows();
		if (selectRows.length == 0) {
			chooseLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 增加是否要清空
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {// 否 退出
			return;
		}

		Vector<Vector<Object>> vectors = ((DefaultTableModel) ruleDocControlTable.getModel()).getDataVector();
		// 记录选中行的文控ID集合
		List<Long> listDocId = new ArrayList<Long>();
		for (int i = 0; i < selectRows.length; i++) {
			// 记录选中行的文控ID
			listDocId.add(Long.valueOf(vectors.get(selectRows[i]).get(0).toString()));
		}
		// 删除制度文控信息/删除Word文档
		try {
			if (listDocId.size() > 0) {
				ConnectionPool.getDocControlAction().deleteJecnTaskHistoryNew(listDocId);
			}
			// 删除表格上的数据
			for (int i = selectRows.length - 1; i >= 0; i--) {
				((DefaultTableModel) ruleDocControlTable.getModel()).removeRow(selectRows[i]);
				// 删除选中的tab所在list中对应的数据
				listHistory.remove(selectRows[i]);
			}
		} catch (Exception e) {
			log.error("RuleDoControlPanel deleteButPerformed is error", e);
		}
	}

	/***************************************************************************
	 * 查阅
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 */
	@SuppressWarnings("unchecked")
	private void searchButPerformed() {
		// 查阅 查看流畅版本记录 默认打开的是流程该版本下的文件说明

		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 1:获取文件名称 默认为打开文件的节点名称
		if (selectNode == null && desktopPane == null) {
			log.error("RuleDoControlPanel searchButPerformed is error！");
			return;
		}
		String fileName = "";
		if (TreeNodeType.ruleFile == selectNode.getJecnTreeBean().getTreeNodeType()
				|| TreeNodeType.file == selectNode.getJecnTreeBean().getTreeNodeType()) {
			fileName = selectNode.getJecnTreeBean().getName();
		} else if (desktopPane == null || selectNode != null) {
			fileName = selectNode.getJecnTreeBean().getName() + ".doc";
		} else {
			fileName = desktopPane.getFlowMapData().getName() + ".doc";
		}
		// 2:获取自己数组byte[]

		// TODO 获取选中table行的文控主键ID
		chooseLab.setText("");
		int[] selectRows = ruleDocControlTable.getSelectedRows();
		if (selectRows.length == 0) {
			chooseLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		} else if (selectRows.length >= 2) {// 判断是否选择多行
			chooseLab.setText(JecnProperties.getValue("onlyOne"));
			return;
		}

		Vector<Vector<Object>> vectors = ((DefaultTableModel) ruleDocControlTable.getModel()).getDataVector();
		Long docId = Long.valueOf(vectors.get(selectRows[0]).get(0).toString());

		FileOpenBean openBean = null;
		try {
			openBean = ConnectionPool.getDocControlAction().viewJecnTaskHistoryNew(docId,
					selectNode.getJecnTreeBean().getId());
		} catch (Exception e) {
			log.error("RuleDoControlPanel searchButPerformed  is error", e);
			e.printStackTrace();
		}
		if (openBean == null) {
			// log异常
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("docInfoIsLose"));
			return;
		}

		if (TreeNodeType.ruleFile == selectNode.getJecnTreeBean().getTreeNodeType()
				|| TreeNodeType.file == selectNode.getJecnTreeBean().getTreeNodeType()) {
			fileName = openBean.getName();
		} else if (TreeNodeType.processFile == selectNode.getJecnTreeBean().getTreeNodeType()) {
			fileName = selectNode.getJecnTreeBean().getName() + JecnDesignerCommon.getSuffixName(openBean.getName());
		}

		String fileVersionExtension = JecnDesignerCommon.getSuffixName(openBean.getName());
		String fileExtension = JecnDesignerCommon.getSuffixName(fileName);

		// Extension
		if (TreeNodeType.process == selectNode.getJecnTreeBean().getTreeNodeType()
				&& !fileVersionExtension.equals(fileExtension)) {// 流程（文件）转流程后，查阅文控版本（流程文件）
			fileName = selectNode.getJecnTreeBean().getName() + JecnDesignerCommon.getSuffixName(openBean.getName());
		}
		// 打开文件
		epros.designer.util.JecnUtil.openFile(fileName, openBean.getFileByte());
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);

		// 制度文控信息表格
		c = new GridBagConstraints(0, 0, 5, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(resultScrollPane, c);
		// 操作提示chooseLab
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(chooseLab, c);
		// 查阅
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(searchBut, c);
		// 编辑
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(editBut, c);
		// 删除
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(deleteBut, c);
		// 取消 cancelBut
		c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(cancelBut, c);

		this.getContentPane().add(mainPanel);
	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelPerformed() {
		this.dispose();
	}

	/**
	 * 制度文控表格显示信息
	 * 
	 * @param list
	 * @return
	 */
	public Vector<Vector<Object>> updateTableContent(List<JecnTaskHistoryNew> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		if (list != null && list.size() > 0) {
			SimpleDateFormat newDate = new SimpleDateFormat("yyyy-MM-dd");
			for (int i = 0; i < list.size(); i++) {
				JecnTaskHistoryNew jecnTaskHistoryNew = list.get(i);
				// if (jecnTaskHistoryNew.getListJecnTaskHistoryFollow().size()
				// == 0) {
				// continue;
				// }
				Vector<Object> data = new Vector<Object>();
				data.add(jecnTaskHistoryNew.getId());
				// 版本号
				data.add(jecnTaskHistoryNew.getVersionId());
				// 拟稿人
				data.add(jecnTaskHistoryNew.getDraftPerson());
				// 开始时间
				data.add(jecnTaskHistoryNew.getStartTime());
				// 结束时间
				data.add(jecnTaskHistoryNew.getEndTime());
				// 返工次数
				data.add(jecnTaskHistoryNew.getApproveCount());
				// 发布日期
				data.add(newDate.format(jecnTaskHistoryNew.getPublishDate()));

				vector.add(data);
			}
		}
		return vector;
	}

	class RuleDocControlTable extends JTable {

		RuleDocControlTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
		}

		public RuleDocControlTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("versionNum"));
			title.add(JecnProperties.getValue("draftPeople"));
			title.add(JecnProperties.getValue("startTimePub"));
			title.add(JecnProperties.getValue("endTimePub"));
			title.add(JecnProperties.getValue("reworkNum"));
			title.add(JecnProperties.getValue("publishDate")); // /
			return new RuleDocControlTableMode(updateTableContent(listHistory), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0 };
		}

		class RuleDocControlTableMode extends DefaultTableModel {
			public RuleDocControlTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);

			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}

		}

	}

	public JecnTreeNode getSelectNode() {
		return selectNode;
	}

	public void setSelectNode(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
	}

}
