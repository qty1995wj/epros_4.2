package com.jecn.epros.server.bean.file;

import java.io.Serializable;

public class FileAttributeBean implements Serializable {
	private Long resPeopleId;
	private String rePeopleName;
	private int resPeopleType;
	private Long commissionerId;
	private String commissionerName;
	private String keyWord;

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}

	public String getRePeopleName() {
		return rePeopleName;
	}

	public void setRePeopleName(String rePeopleName) {
		this.rePeopleName = rePeopleName;
	}

	public int getResPeopleType() {
		return resPeopleType;
	}

	public void setResPeopleType(int resPeopleType) {
		this.resPeopleType = resPeopleType;
	}

	public Long getCommissionerId() {
		return commissionerId;
	}

	public void setCommissionerId(Long commissionerId) {
		this.commissionerId = commissionerId;
	}

	public String getCommissionerName() {
		return commissionerName;
	}

	public void setCommissionerName(String commissionerName) {
		this.commissionerName = commissionerName;
	}
}
