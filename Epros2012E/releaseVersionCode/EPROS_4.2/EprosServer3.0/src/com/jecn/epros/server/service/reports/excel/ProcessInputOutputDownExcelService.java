package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableSheet;

import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.ProcessInputOutputBean;

/**
 * 
 * 流程输入输出统计
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessInputOutputDownExcelService extends BaseDownExcelService {
	private List<ProcessInputOutputBean> processInputOutputList = null;

	public ProcessInputOutputDownExcelService() {
		this.mouldPath = getPrefixPath() + "mould/processInputOutputReport.xls";
		this.startDataRow = 2;
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (processInputOutputList == null) {
			return true;
		}

		try {
			// 第一行
			this.addFirstRowData(dataSheet);

			// 数据行号
			int i = 0;
			// 第三行开始
			for (ProcessInputOutputBean ioBean : processInputOutputList) {
				// 流程名称
				String processName = ioBean.getProcessName();
				if (processName == null) {
					continue;
				}

				// 上游流程集合
				List<ProcessWebBean> upstreamProcess = ioBean
						.getUpstreamProcess();
				// 下游流程集合
				List<ProcessWebBean> downstreamProcess = ioBean
						.getDownstreamProcess();
				// 上游流程个数
				String upstreamProcessCount = String.valueOf(ioBean
						.getUpstreamProcessCount());
				// 下游流程个数
				String downstreamProcessCount = String.valueOf(ioBean
						.getDownstreamProcessCount());

				if ((upstreamProcess == null || upstreamProcess.size() == 0)
						&& (downstreamProcess == null || downstreamProcess
								.size() == 0)) {
					int row = startDataRow + i;
					// 流程名称
					dataSheet.addCell(new Label(0, row, processName));
					// 上游流程个数
					dataSheet.addCell(new Label(2, row, upstreamProcessCount));
					// 下游流程个数
					dataSheet
							.addCell(new Label(4, row, downstreamProcessCount));
					i++;
					continue;
				}

				// 待合并开始行号
				int startFirstRow = startDataRow + i;
				// 当前最大集合大小 0：上游；1：下游
				int maxSize = 0;
				if (upstreamProcess == null) {
					maxSize = downstreamProcess.size();
				} else if (downstreamProcess == null) {
					maxSize = upstreamProcess.size();
				} else if (upstreamProcess.size() >= downstreamProcess.size()) {// 上游流程>=下游流程
					maxSize = upstreamProcess.size();
				} else {
					maxSize = downstreamProcess.size();
				}

				for (int k = 0; k < maxSize; k++) {
					int row = startDataRow + i;
					// 上游集合
					ProcessWebBean upBean = null;
					if (upstreamProcess != null && k < upstreamProcess.size()) {
						upBean = upstreamProcess.get(k);
					}
					// 下游集合
					ProcessWebBean downBean = null;
					if (downstreamProcess != null
							&& k < downstreamProcess.size()) {
						downBean = downstreamProcess.get(k);
					}

					// 上游流程
					String upFlowName = (upBean != null) ? upBean.getFlowName()
							: null;
					// 下游流程
					String downFlowName = (downBean != null) ? downBean
							.getFlowName() : null;

					// 流程名称
					dataSheet.addCell(new Label(0, row, processName));
					// 上游流程
					dataSheet.addCell(new Label(1, row, upFlowName));
					// 上游流程个数
					dataSheet.addCell(new Label(2, row, upstreamProcessCount));
					// 下游流程
					dataSheet.addCell(new Label(3, row, downFlowName));
					// 下游流程个数
					dataSheet
							.addCell(new Label(4, row, downstreamProcessCount));
					i++;
				}
				// 合并单元格，参数格式（开始列，开始行，结束列，结束行）
				// 流程名称
				dataSheet.mergeCells(0, startFirstRow, 0, startDataRow + i - 1);
				// 上游流程个数
				dataSheet.mergeCells(2, startFirstRow, 2, startDataRow + i - 1);
				// 下游流程个数
				dataSheet.mergeCells(4, startFirstRow, 4, startDataRow + i - 1);
			}
			return true;
		} catch (Exception e) {
			log.error("ProcessRuleDownExcelService类editExcle方法", e);
			return false;
		}
	}

	public List<ProcessInputOutputBean> getProcessInputOutputList() {
		return processInputOutputList;
	}

	public void setProcessInputOutputList(
			List<ProcessInputOutputBean> processInputOutputList) {
		this.processInputOutputList = processInputOutputList;
	}
}
