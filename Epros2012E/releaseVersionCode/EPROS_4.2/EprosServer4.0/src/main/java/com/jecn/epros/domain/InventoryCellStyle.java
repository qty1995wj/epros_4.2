package com.jecn.epros.domain;

import com.jecn.epros.domain.InventoryTable.Align;

public class InventoryCellStyle {

    /**
     * 取值范围为1,2,4
     **/
    private int type = 1;
    /**
     * 文本方向
     **/
    private Align align = Align.center;

    public InventoryCellStyle() {

    }

    public InventoryCellStyle(int type) {
        this.type = type;
    }

    public InventoryCellStyle(Align align) {
        this.align = align;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Align getAlign() {
        return align;
    }

    public void setAlign(Align align) {
        this.align = align;
    }
}
