package epros.draw.gui.top.toolbar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏大类：Epros流程管理平台类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEprosMangToolBarItemPanel extends JecnAstractToolBarItemPanel implements ActionListener {

	/** EPROS流程管理平台宣传资料 */
	private JecnToolbarContentButton eprosConductBtn = null;
	/** 说明标签 */
	private JLabel infoLabel = null;

	public JecnEprosMangToolBarItemPanel(JecnToolBarPanel toolBarPanel) {
		super(toolBarPanel);

		initchildComponents();
	}

	@Override
	protected void initTitleType() {
		this.titleType = ToolBarElemType.eprosMagTitle;
	}

	@Override
	protected void createChildComponents() {
		// EPROS流程管理平台宣传资料
		eprosConductBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.eprosMangConductFile), ToolBarElemType.eprosMangConductFile);

		// 说明标签
		infoLabel = new JLabel();

		// EPROS流程管理平台宣传资料
		eprosConductBtn.setHorizontalAlignment(SwingConstants.CENTER);
		eprosConductBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		eprosConductBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		eprosConductBtn.setPreferredSize(new Dimension(60, 10));
		eprosConductBtn.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.eprosMangConductFile)
				+ JecnResourceUtil.getJecnResourceUtil().getValue("eprosMagTitleDetail"));

		infoLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("eprosInfo"));
		infoLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
		infoLabel.setForeground(Color.GRAY);

		// 事件
		eprosConductBtn.removeActionListener(JecnDrawMainPanel.getMainPanel());
		eprosConductBtn.addActionListener(this);

		// 边框
		this.setBorder(JecnUIUtil.getTootBarBorder());
	}

	@Override
	protected void layoutChildComponents() {
		// EPROS流程管理平台宣传资料
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets0(), 0, 0);
		this.add(eprosConductBtn.getJToolBar(), c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 3, 0, 10), 0, 0);
		this.add(infoLabel, c);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		eprosMangActionPerformed(e);
	}

	/**
	 * 
	 * 用户手册处理
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void eprosMangActionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnToolbarContentButton) {
			JecnToolbarContentButton helpUserManualBtn = (JecnToolbarContentButton) e.getSource();

			String rar = "rar";
			String dRar = ".rar";

			// 文件路径
			String path = JecnFileUtil.getSyncPath("/epros/draw/configFile/toolbar/eprosMang/eprosConduct.rar");
			if (DrawCommon.isNullOrEmtryTrim(path)) {
				return;
			}
			File file = new File(path);
			// 判断文件是否存在
			if (!file.exists() || !file.isFile()) {
				return;
			}

			// 文件选择器
			JecnFileChooser fileChooser = new JecnFileChooser();
			// 移除系统给定的文件过滤器
			fileChooser.setAcceptAllFileFilterUsed(false);
			// 标题
			fileChooser.setDialogTitle(helpUserManualBtn.getText());
			// 过滤文件后缀
			fileChooser.setFileFilter(new ImportFlowFilter(rar));
			// 设置按钮为打开
			fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
			// 选中模式：只能选中目录
			fileChooser.setSelectedFile(new File(JecnResourceUtil.getJecnResourceUtil().getValue("eprosMagTitleDetail")
					+ dRar));

			// 显示文件选择器
			int isOk = fileChooser.showSaveDialog(JecnDrawMainPanel.getMainPanel());

			// 选择确认（yes、ok）后返回该值。
			if (isOk == JFileChooser.APPROVE_OPTION) {
				File saveFile = fileChooser.getSelectedFile();
				String newPath = saveFile.getPath();
				if (DrawCommon.isNullOrEmtryTrim(newPath)) {
					return;
				}
				if (!saveFile.isFile() && !newPath.endsWith("rar")) {// 如果不是文件添加后缀名称
					newPath += dRar;
				}

				// 成功:true，失败:false
				JecnFileUtil.writeFileToOPLocal(file, newPath);
			}
		}
	}

}
