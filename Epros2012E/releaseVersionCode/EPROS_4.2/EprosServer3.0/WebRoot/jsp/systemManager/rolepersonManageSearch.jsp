<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%">
			<div id="search" class="divBorder" align="center">
				<div id="searchText" style="overflow: hidden;" class="minWidth"
					align="center">
					<table width="60%">
						<tr>
							<td width="60%" height="25px" align="center">
								<!-- 角色名称： -->
								<s:text name="roleNameC"></s:text>
								<input type="text" style="width: 60%" id="roleName_id"
									maxLength="32" />
							</td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img src="${basePath}images/iParticipateInProcess/search.gif"
						style="cursor: pointer;" onclick="roleManageGridSearch()" />
					<img src="${basePath}images/iParticipateInProcess/reset.gif"
						style="cursor: pointer;" onclick="roleManageReset()" />
				</div>

			</div>

			<div id="roleManageGrid_id">

			</div>
		</div>
	</body>
</html>