package com.jecn.webservice.crs.task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ICrsTaskWebService")
public interface ICrsTaskWebService {

	/**
	 * 根据用户登录名获取用户待办
	 * 
	 * @param loginName
	 * @return
	 */
	@WebMethod(operationName = "GetPList")
	public String GetPList(@WebParam(name = "PSCODE") String PSCODE, @WebParam(name = "PPRINCIPAL") String PPRINCIPAL,
			@WebParam(name = "PTYPE") String PTYPE, @WebParam(name = "PNUM") int PNUM);

	/**
	 * 我的制度-接口
	 * 
	 * @param loginName
	 * @param PNUM
	 * @return
	 */
	@WebMethod(operationName = "getRuleList")
	public String getRuleList(@WebParam(name = "loginName") String loginName, @WebParam(name = "PNUM") int PNUM);
}
