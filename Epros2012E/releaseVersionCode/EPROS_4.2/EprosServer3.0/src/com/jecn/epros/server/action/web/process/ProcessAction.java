package com.jecn.epros.server.action.web.process;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.CompositeTitle;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleEdge;

import com.google.gson.Gson;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.download.ProcessMapDownloadBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;
import com.jecn.epros.server.bean.process.ShowConfig;
import com.jecn.epros.server.bean.process.temp.TempProcessKpiBean;
import com.jecn.epros.server.bean.process.temp.TempSearchKpiBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.download.customized.mengNiu.ActiveRoleDownloadExcel;
import com.jecn.epros.server.download.excel.ProcessMapTemplateDownload;
import com.jecn.epros.server.download.excel.ProcessTemplateDownload;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.service.popedom.IOrgAccessPermissionsService;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.process.IFlowPKIService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IWebProcessActiveService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.system.IJecnConfigItemService;
import com.jecn.epros.server.service.system.IProcessRuleTypeService;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.KeyValueBean;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.process.ActiveRoleBean;
import com.jecn.epros.server.webBean.process.JecnRoleProcessBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;
import com.jecn.epros.server.webBean.process.ProcessAttributeWebBean;
import com.jecn.epros.server.webBean.process.ProcessBaseInfoBean;
import com.jecn.epros.server.webBean.process.ProcessMapBaseInfoBean;
import com.jecn.epros.server.webBean.process.ProcessMapWebBean;
import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;
import com.jecn.epros.server.webBean.process.ProcessSearchBean;
import com.jecn.epros.server.webBean.process.ProcessShowFile;
import com.jecn.epros.server.webBean.process.ProcessSystemListBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.process.RelatedProcessRuleStandardBean;
import com.opensymphony.xwork2.ActionContext;

public class ProcessAction extends BaseAction {
	private static final Logger log = Logger.getLogger(ProcessAction.class);
	private long node;
	private int start;
	private int limit;
	/** 流程名称 */
	private String processName;
	/** 流程编号 */
	private String processNum;
	/** 责任部门 */
	private long orgId = -1;
	/** 责任部门名称 */
	private String orgName;
	/** 查阅部门 */
	private long orgLookId = -1;
	/** 查阅部门名称 */
	private String orgLookName;
	/** 查阅岗位 */
	private long posLookId = -1;
	/** 查阅岗位名称 */
	private String posLookName;
	/** 流程类别 */
	private long processType = -1;
	/** 密级 0 秘密 1公开 */
	private long secretLevel = -1;
	/** 岗位ID */
	private long posId = -1;
	/** 岗位名称 */
	private String posName;
	/** 开始日期 */
	private String startDate;
	/** 结束日期 */
	private String endDate;
	/** 活动名称 */
	private String activeName;
	/** 请求来源 applet:浏览端图形连接来源 */
	private String source;
	/** 我关注的流程或流程地图 传入标识 */
	private int isFlow;

	/** 是否显示流程监护人 0:隐藏 1：显示 */
	private String artificialPersonValue;
	/** 是否显示拟制人 0:隐藏 1：显示 */
	private String responsibilityPeopleValue;
	/** * 流程操作模板类型 默认通用模板 0 */
	private int otherOperaType = 0;

	/** 页面跳转类型 */
	private String type;// 流程process processT流程地图processMap processMapT
	private IWebProcessService webProcessService;
	private IFlowStructureService flowStructureService;
	/** 类别service */
	private IProcessRuleTypeService processRuleTypeService;
	/** 活动明细service接口 */
	private IWebProcessActiveService activeService;
	/** 流程清单.xls */
	private String downProcessName = "流程清单.xls";

	public void setProcessRuleTypeService(IProcessRuleTypeService processRuleTypeService) {
		this.processRuleTypeService = processRuleTypeService;
	}

	/** 流程PKI */
	private IFlowPKIService flowPKIService;
	/** 记录KPI数据 */
	private TempProcessKpiBean tempProcessKpiBean;
	/** 流程ID */
	private long flowId = -1;

	/** 我参与的流程Bean */
	private List<JecnRoleProcessBean> roleProcessList;
	/** 流程属性 */
	private ProcessAttributeWebBean processAttribute;
	/** 流程操作说明数据 */
	private ProcessShowFile processShowFile;
	/** 流程地图操作说明数据 */
	private ProcessMapDownloadBean processMapDownloadBean;
	/** 流程图概况信息 */
	private ProcessBaseInfoBean processBaseInfoBean;
	/** 操作模板数据 */
	private List<ProcessActiveWebBean> processActiveTemplateList;
	/** 文控信息 */
	private List<JecnTaskHistoryNew> taskHistoryNewList;
	/** 相关流程、制度、标准 */
	private RelatedProcessRuleStandardBean relatedProcessRuleStandard;
	/** 流程清单 */
	private ProcessSystemListBean processSystemList;
	/** 流程地图概况信息 */
	private ProcessMapBaseInfoBean processMapBaseInfo;
	/** 流程KPI曲线图图片路径 */
	private String filePath;
	/** *** 流程KPI查询数据集合 ******* */
	private List<JecnFlowKpi> listFlowKpi;
	/** **** 单个流程KPI数据 ********* */
	private JecnFlowKpi jecnFlowKpi;
	/** 流程KPI搜索 */
	private TempSearchKpiBean searchKpiBean;
	/** 文件名称 */
	private String fileName = "ProcessOperatingInstructions.doc";
	/** 文件流Bean */
	private JecnCreateDoc createDoc;
	/** 是否为流程地图 */
	private String isMap;
	/** 活动ID */
	private long activeId = -1;
	/** 活动数据Bean */
	private ProcessActiveWebBean processActiveWebBean;
	/** 我的工作活动 */
	private List<ProcessActiveWebBean> listProcessActiveWebBean;

	/** 文控详细信息 */
	private JecnTaskHistoryNew taskHistoryNew;
	/** 文控ID */
	private long historyId;
	/** 是否为临时表 "ture"为正式表 "false"为临时表 */
	private String isPub = "true";
	/** 文件搜索名称 */
	private String fileSearchName;
	/** 登录类型 */
	private int otherLoginType;

	/** 流程排错结果集 */
	private List<ProcessBaseInfoBean> processCheckList;
	/** 搜索流程检错类型(0全部 1流程责任人 2流程责任部门 3角色对应岗位) */
	private long processCheckType = -1;

	/** 是否显示全部 */
	private boolean showAll = true;
	/** 拼装表头 */
	private String columns = "";
	/** 拼装数据源 */
	private String stores = "";
	/** 流程地图相关制度显示数据 */
	private List<Object[]> ruleObj;

	/** 流程或者流程地图的名称 */
	private String searchName;
	private long kpiAndId;
	private long kpiId;
	private String kpiHorVlaue;
	private String kpiValue;
	/** KPI 纵向名称 */
	protected String verName = null;
	/** KPI 横向名称 */
	protected String honName = null;
	/** *****************excel****************** */
	/** 这两个属性是必须有的,有框架要用 */
	private String uploadContentType = null;
	/** 这两个属性是必须有的,有框架要用 */
	private String uploadFileName = null;
	/** excel文件名称 */
	private File upload = null;
	/***/
	private String arrKpi;
	/** *** 人员services **** */
	private IPersonService personService;
	/** *** 系统配置services **** */
	private IJecnConfigItemService configService;
	/** kpi统计类型 0：月 1：季度 */
	private String kpiHorType;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/** _self 流程图在本页面内刷新 为空时默认效果为_blank 在新页面内打开 */
	private String target;
	/** 流程文件下载是否显示 **/
	private boolean processFileDownloadIsShow = true;

	private IOrgAccessPermissionsService orgAccessPermissionsService;
	/**
	 * 是否显示活动类别
	 */
	private boolean activityTypeIsShow = false;
	/** 流程类别 **/
	private boolean processTypeIsShow = false;
	/** 支持工具 **/
	private boolean supportToolsIsShow = false;
	/** 附件 **/
	private boolean accessoriesIsShow = false;
	/** 制度类型是否显示 **/
	private boolean ruleTypeIsShow = false;

	private String isApp = null;

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	/**
	 * 获取我参与的流程信息
	 * 
	 * @author fuzhh Dec 4, 2012
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getJoinFlow() {
		try {
			roleProcessList = webProcessService.getMyJoinProcess(getPeopleId(), processName, processNum, posId,
					getProjectId());
			StringBuffer s = new StringBuffer("[");
			for (JecnRoleProcessBean jecnRoleProcessBean : roleProcessList) {
				for (ProcessRoleWebBean processRoleWebBean : jecnRoleProcessBean.getRoleList()) {
					// 活动
					StringBuffer pa = new StringBuffer();
					for (ProcessActiveWebBean processActiveWebBean : processRoleWebBean.getActiveList()) {
						String num = StringUtils.isNotBlank(processActiveWebBean.getActiveId()) ? "["
								+ processActiveWebBean.getActiveId() + "]" : "";
						pa.append("<div><a target='_blank' class='a-operation' href='processActive.action?activeId="
								+ processActiveWebBean.getActiveFigureId() + "'>" + num
								+ JecnCommon.quote(processActiveWebBean.getActiveName()) + "</a></div>");
					}
					// 相关制度
					StringBuffer prule = new StringBuffer();
					for (Object[] obj : processRoleWebBean.getRuleList()) {
						if (obj[3] != null) {
							if (obj[3].toString().equals("1")) {
								prule
										.append("<div><a target='_blank' class='a-operation' href='rule.action?from=process&reqType=public&type=ruleModeFile&ruleId="
												+ obj[1].toString() + "'>" + obj[2].toString() + "</a></div>");
							} else if (obj[3].toString().equals("2")) {
								prule
										.append("<div><a target='_blank' class='a-operation' href='rule.action?from=process&reqType=public&type=ruleFile&ruleId="
												+ obj[1].toString()
												+ "'>"
												+ JecnCommon.quote(obj[2].toString())
												+ "</a></div>");
							}
						}
					}
					// 相关标准
					StringBuffer pstan = new StringBuffer();
					for (Object[] obj : processRoleWebBean.getStanList()) {
						if (obj[3] != null) {
							pstan
									.append("<div><a target='_blank' class='a-operation' href='standard.action?from=process&reqType=public&standardType="
											+ obj[3].toString()
											+ "&standardId="
											+ obj[1].toString()
											+ "'>"
											+ JecnCommon.quote(obj[2].toString()) + "</a></div>");
						}

					}
					// 相关风险
					StringBuffer prisk = new StringBuffer();
					for (Object[] obj : processRoleWebBean.getRiskList()) {
						String a;
						if (obj[2].toString().length() > 12) {// 因文本超出表格做的判断
							a = obj[2].toString().substring(0, 8) + "...";

							prisk
									.append("<div> <a target='_blank' class='a-operation' href='getRiskById.action?riskDetailId="
											+ obj[1].toString() + "'>" + JecnCommon.quote(a)

											+ "</a></div>");
						} else {
							prisk
									.append("<div> <a target='_blank' class='a-operation' href='getRiskById.action?riskDetailId="
											+ obj[1].toString() + "'>" + JecnCommon.quote(obj[2].toString())

											+ "</a></div>");
						}
					}
					// 所属流程地图
					StringBuffer belongMap = new StringBuffer();
					if (processRoleWebBean.getPtype() != null) {
						if (processRoleWebBean.getPtype() == 0) {
							belongMap
									.append("<div><a target='_blank' class='a-operation' href='process.action?type=processMap&flowId="
											+ processRoleWebBean.getPflowId()
											+ "'>"
											+ JecnCommon.quote(processRoleWebBean.getPflowName()) + "</a></div>");
						} else {
							belongMap
									.append("<div><a target='_blank' class='a-operation' href='process.action?type=process&flowId="
											+ processRoleWebBean.getPflowId()
											+ "'>"
											+ processRoleWebBean.getPflowName() + "</a></div>");
						}
					}
					s.append("[\""
							+ processRoleWebBean.getFlowId()
							+ "\",\""
							+ jecnRoleProcessBean.getPosName()
							+ "\",\""
							+ processRoleWebBean.getRoleName()

							+ "\",\""
							+ JecnCommon.quote(processRoleWebBean.getFlowName())
							+ (StringUtils.isNotBlank(processRoleWebBean.getFlowNumber()) ? "["
									+ processRoleWebBean.getFlowNumber() + "]" : "") + "\",\"" + pa.toString()
							+ "\",\"" + belongMap.toString() + "\",\"" + prule.toString() + "\",\"" + pstan.toString()
							+ "\",\"" + prisk.toString() + "\"],");
				}

			}
			String json = "";
			if (s.length() > 2) {
				json = s.substring(0, s.length() - 1);
			} else {
				json = s.toString();
			}

			json += "]";
			json = json.replaceAll("\n", "<br/>");
			outJsonString(json);
		} catch (Exception e) {
			log.error(e);
		}
		return SUCCESS;
	}

	/**
	 * 获取流程属性
	 * 
	 * @author fuzhh Dec 21, 2012
	 * @return
	 */
	public String findProcessAttribute() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			processAttribute = webProcessService.getProcessAttributeWebBean(flowId, isp);
		} catch (Exception e) {
			log.error("获取流程属性错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-10-11
	 * @description:
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getChildFlows() {
		try {
			List<WebTreeBean> list = webProcessService.getChildProcessForWeb(node, getProjectId(), isAdmin());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error(e);
		}

		return null;
	}

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的流程
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String attenTionProcess() {
		AttenTionSearchBean attenTionSearchBean = new AttenTionSearchBean();
		attenTionSearchBean.setName(processName);
		attenTionSearchBean.setNumber(processNum);
		attenTionSearchBean.setOrgId(orgId);
		attenTionSearchBean.setSecretLevel(secretLevel);
		attenTionSearchBean.setUserId(getPeopleId());
		attenTionSearchBean.setIsFlow(isFlow);
		try {
			int total = webProcessService.getTotalAttenTionProcess(attenTionSearchBean, getProjectId());
			List<ProcessWebBean> list = webProcessService.getAttenTionProcess(attenTionSearchBean, start, limit,
					getProjectId());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("查询我关注的流程报错！", e);
		}
		return null;
	}

	/**
	 * 获取流程操作说明
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @return
	 */
	public String findProcessFile() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			otherLoginType = JecnContants.otherLoginType;
			processShowFile = webProcessService.getProcessShowFile(flowId, isp);

			if (JecnContants.configMaps.get(ConfigItemPartMapMark.flowFileDownMenuShow.toString()) != null) {
				JecnConfigItemBean jecnConfigItemBean = JecnContants.configMaps
						.get(ConfigItemPartMapMark.flowFileDownMenuShow.toString());
				if ("1".equals(jecnConfigItemBean.getValue())) {
					if (!isAdminExcludeViewAdmin() && !isSecondAdmin()) {
						Set<Long> flowSet = new HashSet<Long>();
						flowSet.add(flowId);
						processFileDownloadIsShow = orgAccessPermissionsService.peopleHasFlowFileDownloadPerm(flowSet,
								getPeopleId());
					}
				}
			}

		} catch (Exception e) {
			log.error("查询流程操作说明数据异常!", e);
		}
		return SUCCESS;
	}

	/**
	 * 获取流程地图操作说明
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @return
	 */
	public String findProcessMapFile() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			processMapDownloadBean = flowStructureService.getProcessMapFile(flowId, isp);
			String flowAim = processMapDownloadBean.getFlowAim();
			String flowArea = processMapDownloadBean.getFlowArea();
			String flowNounDefine = processMapDownloadBean.getFlowNounDefine();
			String flowInput = processMapDownloadBean.getFlowInput();
			String flowOutput = processMapDownloadBean.getFlowOutput();
			String flowShowStep = processMapDownloadBean.getFlowShowStep();
			flowAim = flowAim.replaceAll("\n", "<br>");
			processMapDownloadBean.setFlowAim(flowAim.replaceAll(" ", "&nbsp;"));
			flowArea = flowArea.replaceAll("\n", "<br>");
			processMapDownloadBean.setFlowArea(flowArea.replaceAll(" ", "&nbsp;"));
			flowNounDefine = flowNounDefine.replaceAll("\n", "<br>");
			processMapDownloadBean.setFlowNounDefine(flowNounDefine.replaceAll(" ", "&nbsp;"));
			flowInput = flowInput.replaceAll("\n", "<br>");
			processMapDownloadBean.setFlowInput(flowInput.replaceAll(" ", "&nbsp;"));
			flowOutput = flowOutput.replaceAll("\n", "<br>");
			processMapDownloadBean.setFlowOutput(flowOutput.replaceAll(" ", "&nbsp;"));
			flowShowStep = flowShowStep.replaceAll("\n", "<br>");
			processMapDownloadBean.setFlowShowStep(flowShowStep.replaceAll(" ", "&nbsp;"));
		} catch (Exception e) {
			log.error("查询流程地图操作说明数据异常！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程图概况信息
	 * 
	 * @author fuzhh Jan 9, 2013
	 * @return
	 */
	public String findProcessProfileInformation() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			processBaseInfoBean = webProcessService.getProcessBaseInfoBean(flowId, isp);
			alterProcessPropertyConfig();

		} catch (Exception e) {
			log.error("查询流程概况信息错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程属性隐藏或者显示设置
	 */
	private void alterProcessPropertyConfig() {
		processTypeIsShow = JecnUtil.ifValueIsOneReturnTrue("4");
		supportToolsIsShow = JecnUtil.ifValueIsOneReturnTrue("supportTools");
		accessoriesIsShow = JecnUtil.ifValueIsOneReturnTrue("enclosure");
	}

	/**
	 * 流程图操作模板
	 * 
	 * @author fuzhh Jan 16, 2013
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String findProcessTemplate() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			processActiveTemplateList = webProcessService.getProcessActiveBeanList(flowId, isp);
		} catch (Exception e) {
			log.error("查询流程图操作模板错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 操作模板下载
	 * 
	 * @author fuzhh Jan 22, 2013
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String downloadProcessTemplate() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			processActiveTemplateList = webProcessService.getProcessActiveBeanList(flowId, isp);
		} catch (Exception e) {
			log.error("查询流程图操作模板错误！", e);
		}

		// 获取模板文件路径
		String filePath = JecnPath.APP_PATH + "document/documentModel/processModel.xls";
		ProcessTemplateDownload templateDownload = new ProcessTemplateDownload();
		templateDownload.setExcelModeFilePath(filePath);

		// 获取待创建的Excel文件路径
		templateDownload.setExcelOutPutFilePath(JecnFinal.getAllTempPath(JecnFinal.saveExcelTempFilePath()));
		templateDownload.processOptionExcel(processActiveTemplateList);

		// 获得文件
		File file = new File(templateDownload.getExcelOutPutFilePath());
		if (!file.isFile() || !file.exists()) {
			return null;
		}
		byte[] content;
		try {
			// 把一个文件转化为字节
			content = JecnUtil.getByte(file);
			file.delete();
			createDoc = new JecnCreateDoc();
			createDoc.setBytes(content);
			fileName = "processTemplate.xls";
		} catch (Exception e) {
			log.error(e);
		}
		return SUCCESS;
	}

	/**
	 * 文控信息
	 * 
	 * @author fuzhh Jan 16, 2013
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String findHistoryNew() {
		try {
			taskHistoryNewList = webProcessService.getTaskHistoryNew(flowId);
		} catch (Exception e) {
			log.error("获取文控信息错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-12-25
	 * @description:流程搜索
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String searchProcessList() {
		ProcessSearchBean processSearchBean = new ProcessSearchBean();
		processSearchBean.setProcessName(processName);
		processSearchBean.setProcessNum(processNum);
		processSearchBean.setProcessType(processType);
		processSearchBean.setSecretLevel(secretLevel);
		processSearchBean.setOrgId(orgId);
		processSearchBean.setOrgName(orgName);
		processSearchBean.setPosId(posId);
		processSearchBean.setPosName(posName);
		processSearchBean.setOrgLookName(orgLookName);
		processSearchBean.setOrgLookId(orgLookId);
		processSearchBean.setPosLookName(posLookName);
		processSearchBean.setPosLookId(posLookId);
		processSearchBean.setProjectId(getProjectId());
		try {
			int total = webProcessService.getTotalSearchProcess(processSearchBean, getPeopleId(), getProjectId(),
					isAdmin());
			List<ProcessWebBean> list = null;
			if (total > 0) {
				list = webProcessService.searchProcess(processSearchBean, start, limit, getPeopleId(), getProjectId(),
						isAdmin());
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonPage(jsonArray.toString(), total);
			} else {
				outJsonPage("[]", total);
			}

		} catch (Exception e) {
			log.error("流程搜索错误！", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-12-25
	 * @description:流程地图搜索
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String searchProcessMapList() {
		ProcessSearchBean processSearchBean = new ProcessSearchBean();
		processSearchBean.setProcessName(processName);
		processSearchBean.setSecretLevel(secretLevel);
		processSearchBean.setOrgLookId(orgLookId);
		processSearchBean.setOrgLookName(orgLookName);
		processSearchBean.setPosLookId(posLookId);
		processSearchBean.setPosLookName(posLookName);
		processSearchBean.setProjectId(getProjectId());
		try {
			int total = webProcessService.getTotalSearchProcessMap(processSearchBean, getPeopleId(), getProjectId(),
					isAdmin());
			List<ProcessMapWebBean> list = webProcessService.searchProcessMap(processSearchBean, start, limit,
					getPeopleId(), getProjectId(), isAdmin());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);

		} catch (Exception e) {
			log.error("流程地图搜索错误！", e);
		}
		return null;
	}

	/**
	 * 相关流程制度数据
	 * 
	 * @author fuzhh Jan 16, 2013
	 * @return
	 */
	public String findRelatedProcessRuleStandard() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			relatedProcessRuleStandard = webProcessService.getRelatedProcessRuleStandard(flowId, isp);
			ruleTypeIsShow = JecnUtil.ifValueIsOneReturnTrue("ruleType");
		} catch (Exception e) {
			log.error("查询相关流程制度标准数据错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程地图 相关制度
	 * 
	 * @author fuzhh 2013-12-9
	 * @return
	 */
	public String findRelatedRule() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			ruleObj = flowStructureService.getFlowRelateRulesWeb(flowId, isp);
			StringBuffer sb = new StringBuffer("[");
			for (int i = 0; i < ruleObj.size(); i++) {
				Object[] obj = ruleObj.get(i);
				sb.append("{");
				// 制度ID
				sb.append(JecnCommon.contentJson("ruleId", obj[0]) + ",");
				// 文件ID
				sb.append(JecnCommon.contentJson("fileId", obj[3]) + ",");
				// 制度名称
				String ruleName = "";
				if (obj[1] != null) {
					ruleName = obj[1].toString();
				}
				sb.append(JecnCommon.contentJson("ruleName", ruleName) + ",");
				// 制度编号
				String ruleNum = "";
				if (obj[2] != null) {
					ruleNum = obj[2].toString();
				}
				sb.append(JecnCommon.contentJson("ruleNum", ruleNum) + ",");
				// 责任部门ID
				sb.append(JecnCommon.contentJson("orgId", obj[7]) + ",");
				String orgName = "";
				if (obj[8] != null) {
					orgName = obj[8].toString();
				}
				// 责任部门名称
				sb.append(JecnCommon.contentJson("ruleOrgName", orgName) + ",");
				// 制度类别
				String ruleType = "";
				if (obj[6] != null) {
					ruleType = obj[6].toString();
				}
				sb.append(JecnCommon.contentJson("ruleType", ruleType) + ",");
				// 制度类型
				sb.append(JecnCommon.contentJson("isDir", obj[4]));
				if (i == ruleObj.size() - 1) {
					sb.append("}");
				} else {
					sb.append("},");
				}
			}
			sb.append("]");
			outJsonString(sb.toString());
		} catch (Exception e) {
			log.error("查询流程地图相关制度数据错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * ***********************【流程KPI处理 start】***********************************
	 */

	/**
	 * 获取流程KPI名称相关信息
	 * 
	 * @return
	 */
	public String showFlowKpiNames() {
		try {
			tempProcessKpiBean = flowPKIService.geTempProcessKpiBean(flowId);
		} catch (Exception e) {
			log.error("显示流程KPI信息异常！tempProcessKpiBean = " + tempProcessKpiBean, e);
		}
		return SUCCESS;
	}

	/**
	 * 查询流程KPi是否有操作权限
	 * 
	 * @return
	 */
	public String showKpiOption() {
		int emableOption = 0;
		// 勾选允许数据录入
		WebLoginBean webLoginBean = (WebLoginBean) this.getSession().getAttribute("webLoginBean");
		if (webLoginBean.getIsAdmin()) {
			emableOption = 1;
		} else {
			boolean allowSupplierKPIData = "1".equals(JecnContants.getValue("allowSupplierKPIData")) ? true : false;
			// 允许数据录入标志
			if (allowSupplierKPIData) {
				try {
					JecnFlowKpiName jecnFlowKpiName = flowPKIService.getFlowKPI(kpiAndId);
					long peopleId = webLoginBean.getJecnUser().getPeopleId();
					if (null != jecnFlowKpiName && null != jecnFlowKpiName.getKpiDataPeopleId()
							&& peopleId == jecnFlowKpiName.getKpiDataPeopleId()) {
						emableOption = 1;
					}
				} catch (Exception e) {
					log.error("查询流程KPI异常", e);
				}
			}
		}
		this.outJsonString("" + emableOption);
		return null;
	}

	/**
	 * 导入KPI流程 值
	 */
	public String excelImporteFlowKpiImage() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		long loginUserId = webLoginBean.getJecnUser().getPeopleId();
		String result = flowStructureService.excelImporteFlowKpiImage(flowId, kpiAndId, upload, kpiHorType, startTime,
				endTime, loginUserId);
		if ("SUCCESS".equals(result)) {
			ResultHtmlNotClose(JecnUtil.getValue("inportSuccess"));// 导入成功
		} else {
			ResultHtmlNotClose(result);// 导入成功
		}
		return null;
	}

	/***************************************************************************
	 * 导出KPI值 TODO
	 * 
	 * @throws Exception
	 */
	public String downLoadflowKpi() throws Exception {
		if (searchKpiBean == null || searchKpiBean.getFlowId() == null || searchKpiBean.getKpiHorType() == null
				|| searchKpiBean.getKpiId() == null) {
			throw new NullPointerException("流程KPI获取数据异常！");
		}
		createDoc = flowPKIService.exportFlowKpi(searchKpiBean);
		fileName = createDoc.getFileName();
		return SUCCESS;
	}

	/** * 模版下载 ** */
	public String downLoadflowKpiModelFile() {
		try {
			File file = new File(SyncTool.getExcelKPIModelPath());
			// 获得文件
			if (!file.isFile() || !file.exists()) {
				return null;
			}
			byte[] content;
			try {
				// 把一个文件转化为字节
				content = JecnUtil.getByte(file);
				createDoc = new JecnCreateDoc();
				createDoc.setBytes(content);
				fileName = "inputKPIMouldExcel.xls";
			} catch (Exception e) {
				log.error(e);
				return null;
			}
			return SUCCESS;
		} catch (Exception e) {
			log.error("导出KPI模版下载异常！", e);
		}
		return null;
	}

	/**
	 * 查看KPI详情
	 */
	public void showKpidatil() {
		JecnFlowKpiName flowKpi = null;
		try {
			if (kpiAndId == 0) {
				log.error("kpiAndId is null");
				throw new Exception("kpiAndId IS NULL ");
			}
			// kpi的bean
			flowKpi = flowPKIService.getFlowKPI(kpiAndId);
			// 存放的是显示的label
			List<ShowConfig> showConfigList = new ArrayList<ShowConfig>();
			// 显示的值
			Map<String, String> map = new HashMap<String, String>();

			// 获得配置项
			List<JecnConfigItemBean> configs = configService.selectTableConfigItemBeanByType(1, 16);
			for (JecnConfigItemBean configItem : configs) {

				ShowConfig showConfig = new ShowConfig();
				showConfig.setWidth(300);
				showConfig.setHeight(20);
				showConfig.setFieldLabel(configItem.getName());
				showConfig.setName(configItem.getMark().toString());
				showConfig.setCls("textfield-readonly");
				showConfig.setXtype("textfield");
				showConfigList.add(showConfig);

				if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiName.toString())) {// KPI类型

					// 0为结果性指标、1为过程性指标
					if (flowKpi.getKpiName() != null && !"".equals(flowKpi.getKpiName())) {
						map.put("kpiName", flowKpi.getKpiName());
					}
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiType.toString())) {// KPI类型

					// 0为结果性指标、1为过程性指标
					if (flowKpi.getKpiType() != null) {
						int kpiType = flowKpi.getKpiType();
						map.put("kpiType", JecnResourceUtil.getKpiType(kpiType));
					}
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiUtilName.toString())) {// KPI单位名称

					// KPI值单位名称
					String kpiVertical = flowKpi.getKpiVertical();
					map.put("kpiUtilName", JecnResourceUtil.getKpiVertical(Integer.valueOf(kpiVertical)));
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiTargetValue.toString())) {// KPI目标值
					// KPI目标值
					map.put("kpiTargetValue", flowKpi.getKpiTarget());
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiTimeAndFrequency.toString())) {// 数据统计时间频率
					map.put("kpiTimeAndFrequency", JecnResourceUtil.getKpiHorizontal(Integer.valueOf(flowKpi
							.getKpiHorizontal())));
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDefined.toString())) {// KPI定义
					// KPI定义
					map.put("kpiDefined", flowKpi.getKpiDefinition());
					showConfig.setHeight(80);
					showConfig.setXtype("textarea");
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDesignFormulas.toString())) {// 流程KPI计算公式
					// 流程计算公式
					map.put("kpiDesignFormulas", flowKpi.getKpiStatisticalMethods());
					showConfig.setHeight(80);
					showConfig.setXtype("textarea");
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDataProvider.toString())) {// 数据提供者
					// 数据提供者
					if (flowKpi.getKpiDataPeopleId() != null) {
						long kpiDataPeopleIdInt = flowKpi.getKpiDataPeopleId();
						JecnUser jecnUser = personService.get(kpiDataPeopleIdInt);
						if (jecnUser != null) {
							map.put("kpiDataProvider", jecnUser.getTrueName());
						} else {
							map.put("kpiDataProvider", "");
						}

					} else {
						map.put("kpiDataProvider", "");
					}
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDataAccess.toString())) {// 数据获取方式
					map.put("kpiDataAccess", JecnResourceUtil.getKpiDataMethod(flowKpi.getKpiDataMethod()));
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiIdSystem.toString())) {// IT系统

					List<JecnFlowSustainTool> sustainTools = webProcessService
							.getSustainTools(flowKpi.getKpiAndId(), 0);
					if (sustainTools.size() == 0) {
						map.put("kpiIdSystem", "");
					} else {
						StringBuffer buf = new StringBuffer();

						for (int i = 0; i < sustainTools.size(); i++) {
							if (i == 0) {
								buf.append(sustainTools.get(i).getFlowSustainToolShow());
							} else {
								buf.append("/");
								buf.append(sustainTools.get(i).getFlowSustainToolShow());
							}
						}

						map.put("kpiIdSystem", buf.toString());
					}

				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiSourceIndex.toString())) {// 指标来源
					// 指标来源
					if (flowKpi.getKpiTargetType() != null) {
						int kpiTargetTypeInt = flowKpi.getKpiTargetType();
						map.put("kpiSourceIndex", JecnResourceUtil.getKpiTargetType(kpiTargetTypeInt));
					} else {
						map.put("kpiSourceIndex", "");
					}

				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiRrelevancy.toString())) {// 相关度
					// 相关度
					if (flowKpi.getKpiRelevance() != null) {
						int kpiRelevance = flowKpi.getKpiRelevance();
						map.put("kpiRrelevancy", JecnResourceUtil.getKpiRelevance(kpiRelevance));
					}
				} else if (configItem.getMark().toString().equals(
						ConfigItemPartMapMark.kpiSupportLevelIndicator.toString())) {// 支撑的一级指标
					// 支持的一级指标
					String firstTargetId = flowKpi.getFirstTargetId();
					if (StringUtils.isNotBlank(firstTargetId)) {
						JecnKPIFirstTargetBean jecnKPIFirstTargetBean = flowPKIService.getFirstTarget(firstTargetId);
						if (jecnKPIFirstTargetBean != null) {
							map.put("kpiSupportLevelIndicator", jecnKPIFirstTargetBean.getTargetContent());
						} else {
							map.put("kpiSupportLevelIndicator", "");
						}

					}
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPurpose.toString())) {// 设置目的
					if (StringUtils.isNotBlank(flowKpi.getKpiPurpose())) {
						map.put("kpiPurpose", flowKpi.getKpiPurpose());
					} else {
						map.put("kpiPurpose", "");
					}
					showConfig.setCls("textarea-readonly");
					showConfig.setHeight(80);
					showConfig.setXtype("textarea");

				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPoint.toString())) {// 测量点
					if (StringUtils.isNotBlank(flowKpi.getKpiPurpose())) {
						map.put("kpiPoint", flowKpi.getKpiPoint());
					} else {
						map.put("kpiPoint", "");
					}
					showConfig.setHeight(80);
					showConfig.setXtype("textarea");
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPeriod.toString())) {// 统计周期
					if (StringUtils.isNotBlank(flowKpi.getKpiPurpose())) {
						map.put("kpiPeriod", flowKpi.getKpiPeriod());
					} else {
						map.put("kpiPeriod", "");
					}
					showConfig.setHeight(80);
					showConfig.setXtype("textarea");
				} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiExplain.toString())) {// 说明
					if (StringUtils.isNotBlank(flowKpi.getKpiPurpose())) {
						map.put("kpiExplain", flowKpi.getKpiExplain());
					} else {
						map.put("kpiExplain", "");
					}
					showConfig.setHeight(80);
					showConfig.setXtype("textarea");
				}
			}

			// // 是否勾选 显示指标来源、支持一级指标、相关度三属性
			// // 1：启用、0：未启用
			// int showKPIProperty = 0;
			// int allowSupplierKPIData = 0;
			// for (JecnConfigItemBean configItem : JecnContants.peopleItemList)
			// {
			// if ("showKPIProperty".equals(configItem.getMark())
			// && "1".equals(configItem.getValue())) {
			// showKPIProperty = 1;
			// } else if ("allowSupplierKPIData".equals(configItem.getMark())
			// && "1".equals(configItem.getValue())) {
			// allowSupplierKPIData = 1;
			// }
			// }
			// map.put("showKPIProperty", String.valueOf(showKPIProperty));
			// // 允许数据录入者提供数据
			// map.put("allowSupplierKPIData", String
			// .valueOf(allowSupplierKPIData));

			Gson gson = new Gson();
			String json = gson.toJson(map);
			String items = gson.toJson(showConfigList);

			this.outJsonForSuccessParam(true, "\"items\":" + items + ",\"data\":" + json, this.getResponse());
		} catch (Exception e) {
			log.error("加载KPI详情异常", e);
		}
	}

	/**
	 * 编辑KPI值
	 */
	public void updateFlowKpiImage() {
		try {
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");

			JSONArray jsonArray = JSONArray.fromObject(arrKpi);
			long loginUserId = webLoginBean.getJecnUser().getPeopleId();
			List<JecnFlowKpi> dd = JSONArray.toList(jsonArray, JecnFlowKpi.class);
			for (JecnFlowKpi tempKpi : dd) {
				// 编辑
				if (tempKpi.getKpiId() > 0) {
					JecnFlowKpi jecnFlowKpi = flowPKIService.getFlowKPIValue(tempKpi.getKpiId());
					jecnFlowKpi.setKpiHorVlaue(JecnCommon.getDateByString(tempKpi.getKpiHorVlaueStr(), "yyy-MM"));
					jecnFlowKpi.setKpiValue(tempKpi.getKpiValue());
					jecnFlowKpi.setUpdateTime(new Date());
					jecnFlowKpi.setUpdatePeopleId(loginUserId);
					flowStructureService.updateKPIVaule(jecnFlowKpi, loginUserId, tempKpi.getFlowId());
				}
				// 添加
				else {
					JecnFlowKpi jecnFlowKpi = new JecnFlowKpi();
					jecnFlowKpi.setKpiAndId(tempKpi.getKpiAndId());
					jecnFlowKpi.setKpiValue(tempKpi.getKpiValue());
					jecnFlowKpi.setKpiHorVlaue(JecnCommon.getDateByString(tempKpi.getKpiHorVlaueStr(), "yyy-MM"));
					jecnFlowKpi.setCreateTime(new Date());
					jecnFlowKpi.setCreatePeopleId(loginUserId);
					jecnFlowKpi.setUpdateTime(new Date());
					jecnFlowKpi.setUpdatePeopleId(loginUserId);
					flowStructureService.addKPIVaule(jecnFlowKpi, loginUserId, tempKpi.getFlowId());
				}
			}
		} catch (Exception e) {
			log.error("编辑程KPI异常 ", e);
		}
	}

	/** * 清空KPI的值 ** */
	public void deleteFlowKpiImage() {
		try {
			if (jecnFlowKpi.getKpiId() != null && !"".equals(jecnFlowKpi.getKpiId())) {
				WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
				long loginUserId = webLoginBean.getJecnUser().getPeopleId();
				flowStructureService.deleteKPIValues(jecnFlowKpi.getKpiId(), loginUserId, flowId);
			}
			this.outJsonString("SUCCESS");
		} catch (Exception e) {
			log.error("移除KPI值的异常！jecnFlowKpi = " + jecnFlowKpi, e);
		}
	}

	/**
	 * 流程KPI搜索
	 * 
	 * @return
	 */
	public String searchFlowKpi() {
		if (searchKpiBean == null || searchKpiBean.getFlowId() == null || searchKpiBean.getKpiName() == null
				|| searchKpiBean.getKpiHorType() == null || searchKpiBean.getKpiId() == null) {
			throw new NullPointerException("流程KPI搜索获取数据异常！");
		}
		try {
			// KPI类型 1季度 0 月份
			String kpiType = searchKpiBean.getKpiHorType();
			// 根据输入的时间来获取比较差数yyyy-MM-dd
			String StringStartDate = searchKpiBean.getStartTime();
			String StringEndDate = searchKpiBean.getEndTime();
			// 原始数据
			List<JecnFlowKpi> tmpListFlowKpi = flowPKIService.getFlowKPIValue(searchKpiBean.getKpiId(),
					StringStartDate, StringEndDate);
			// 显示到前台的数据
			List<JecnFlowKpi> listFlowKpi = JecnUtil.kpiModelAddCount(tmpListFlowKpi, kpiType, StringStartDate,
					StringEndDate, searchKpiBean);
			// 输出到前台
			JSONArray jsonArray = JSONArray.fromObject(listFlowKpi);

			JecnFlowKpiName jecnFlowKpiName = flowPKIService.getFlowKPI(searchKpiBean.getKpiId());

			if (jecnFlowKpiName != null && jecnFlowKpiName.getKpiTarget() != null
					&& !"".equals(jecnFlowKpiName.getKpiTarget())) {

				jecnFlowKpiName.setKpiTarget(jecnFlowKpiName.getKpiTarget());
			}
			List<Map<String, Double>> listDate = new ArrayList<Map<String, Double>>();
			Map<String, Double> map = null;
			for (JecnFlowKpi jecnFlowKpi : tmpListFlowKpi) {
				map = new HashMap<String, Double>();
				map.put(JecnCommon.getStringbyDate(jecnFlowKpi.getKpiHorVlaue(), "yyyy-MM"),
						jecnFlowKpi.getKpiValue() == null ? 0 : Double.valueOf(jecnFlowKpi.getKpiValue()));
				listDate.add(map);
			}

			int chartHeight = 400;
			int chartWidth = 300;
			// 将值与值之间的间隔设为20
			if (listDate.size() > 5) {
				chartWidth = listDate.size() * 60;
			}

			// 生成的图片右侧的图片定为100
			chartWidth = chartWidth + 100;

			JFreeChart createChart = createChart(jecnFlowKpiName, listDate);
			// 生成图片返回图片的路径
			String path = JecnPath.APP_PATH;
			// 获取存储临时文件的目录
			String tempFileDir = "images/tempImage/" + "PROCESS_KPI_VALUE/";

			// 使用登录人的id为图片的名称
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
			// 文件名称
			String timefile = curPeopleId.toString() + ".png";
			String imagePath = path + tempFileDir + timefile;
			// 创建临时文件目录
			File fileDir = new File(path + tempFileDir);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			} else {// 删除图片
				File tempFileImage = new File(imagePath);
				if (tempFileImage.exists()) {
					tempFileImage.delete();
				}

			}

			String imageUrl = tempFileDir + timefile;
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(imagePath);
				ChartUtilities.writeChartAsPNG(fos, createChart, chartWidth, chartHeight);
				fos.flush();
			} catch (Exception e) {
				log.error("ProcessAction searchFlowKpi方法生成图片错误", e);
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (Exception ex) {
						log.error("ProcessAction类searchFlowKpi()的关闭生成的图片的流异常", ex);
					}

				}

			}

			outJsonString("{\"root\":" + jsonArray.toString() + ",\"totalProperty\":" + listFlowKpi.size()
					+ ",\"imageUrl\":" + "\"" + imageUrl + "\"" + "}");

		} catch (Exception e) {
			log.error("searchFlowKpi 搜索流程KPI信息异常！tempProcessKpiBean = " + tempProcessKpiBean, e);
		}
		return null;
	}

	/**
	 * 
	 * @param jecnFlowKpi
	 *            流程kpi Bean
	 * @param list
	 *            流程kpi的日期与值
	 * @return JFreeChart
	 */
	private JFreeChart createChart(JecnFlowKpiName jecnFlowKpi, List<Map<String, Double>> list) {

		// 数据源
		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();
		double maxValue = 0;
		// 为了页面显示清晰 设置为unit
		int unit = 1;
		for (int i = 0; i < list.size(); i++) {
			Map<String, Double> map = list.get(i);
			for (String key : map.keySet()) {
				Double val = (Double) map.get(key);
				if (null != val) {
					if (val > maxValue) {
						maxValue = val.intValue();
					}
					localDefaultCategoryDataset.addValue(val, JecnUtil.getValue("Graph"), key);

				}

			}
		}

		// 判断最大值是否小于10如果小于10将最大值设为11
		if (maxValue < 10) {
			maxValue = 11;
			unit = 1;
		} else {
			unit = (int) (maxValue / 10);
			maxValue = maxValue + unit;

		}
		// 添加目标值的值
		if (!JecnCommon.isNullOrEmtryTrim(jecnFlowKpi.getKpiTarget())) { // 目标值的标签
			localDefaultCategoryDataset.addValue(Double.valueOf(jecnFlowKpi.getKpiTarget()), JecnUtil
					.getValue("targetValue"), "");
		}

		// 第一个boolean值的意思是折线或者柱状图的是否显示描述
		JFreeChart localJFreeChart = ChartFactory.createLineChart(jecnFlowKpi.getKpiName(), "", "",
				localDefaultCategoryDataset, PlotOrientation.VERTICAL, false, false, false);
		localJFreeChart.addSubtitle(new TextTitle());

		CategoryPlot localCategoryPlot = (CategoryPlot) localJFreeChart.getPlot();

		// 折线的描述（就是描述各种类型的线或者图）
		LegendTitle legend = new LegendTitle(localCategoryPlot);
		legend.setPosition(RectangleEdge.LEFT);
		BlockContainer blockcontainer = new BlockContainer(new BorderArrangement());
		blockcontainer.add(legend, RectangleEdge.LEFT);// 这行代码可以控制位置
		CompositeTitle compositetitle = new CompositeTitle(blockcontainer);
		compositetitle.setPosition(RectangleEdge.RIGHT);// 放置图形代表区域位置的代码

		localJFreeChart.addSubtitle(compositetitle);

		// 背景色
		localCategoryPlot.setBackgroundAlpha(0.5f);

		ValueAxis rangeAxis = localCategoryPlot.getRangeAxis(0);
		rangeAxis.setLabelPaint(Color.blue);

		CategoryAxis domainAxis = localCategoryPlot.getDomainAxis();
		// 设置两列之间间隔的宽度
		domainAxis.setCategoryMargin(0.1);

		// 下边栏的倾斜度
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

		// 目标值水平线，如果目标值不存在则目标值的横线不显示。
		String kpiTarget = jecnFlowKpi.getKpiTarget();
		if (!JecnCommon.isNullOrEmtryTrim(kpiTarget)) {
			double kpiTargetValue = Double.valueOf(kpiTarget);
			if (kpiTargetValue > maxValue) {
				unit = (int) (kpiTargetValue / 10);
				maxValue = kpiTargetValue + unit;

			}
			IntervalMarker localIntervalMarker = new IntervalMarker(kpiTargetValue, kpiTargetValue);
			// 填充的颜色
			localIntervalMarker.setOutlinePaint(Color.red);
			localCategoryPlot.addRangeMarker(localIntervalMarker, Layer.BACKGROUND);
		}

		NumberAxis localNumberAxis = (NumberAxis) localCategoryPlot.getRangeAxis();
		// 设置刻度长度
		localNumberAxis.setAutoTickUnitSelection(true);
		// 设置显示的最大值
		localNumberAxis.setUpperBound(maxValue);
		NumberTickUnit leftNtu = new NumberTickUnit(unit);
		localNumberAxis.setTickUnit(leftNtu);

		// 将纵坐标的那列注册到左边还是右边
		localCategoryPlot.setRangeAxis(0, localNumberAxis);

		localNumberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		LineAndShapeRenderer localLineAndShapeRenderer = (LineAndShapeRenderer) localCategoryPlot.getRenderer(0);
		localLineAndShapeRenderer.setBaseShapesVisible(true);
		localLineAndShapeRenderer.setDrawOutlines(true);
		localLineAndShapeRenderer.setUseFillPaint(true);
		localLineAndShapeRenderer.setBaseFillPaint(Color.RED);
		localLineAndShapeRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		localLineAndShapeRenderer.setBaseItemLabelsVisible(true);

		// 设置折线的颜色
		localLineAndShapeRenderer.setSeriesPaint(0, Color.BLUE);
		localLineAndShapeRenderer.setSeriesPaint(1, Color.RED);

		// 每一个刻度的横线
		localLineAndShapeRenderer.setBaseFillPaint(Color.white);
		return localJFreeChart;
	}

	/***************************************************************************
	 * 我的工作活动搜索 2012-01-21
	 * 
	 * @param node
	 */
	public String searchMyActiveList() {
		// 获取
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		try {
			listProcessActiveWebBean = webProcessService.searchMyActive(activeName, processName, processNum, startDate,
					endDate, webLoginBean.getJecnUser().getPeopleId(), webLoginBean.getProjectId());
		} catch (Exception e) {
			log.error(e);
			return ERROR;
		}

		return SUCCESS;
	}

	/** ***********************【流程KPI处理 end】*********************************** */

	/**
	 * 流程体系
	 * 
	 * @author fuzhh Jan 17, 2013
	 * @return
	 */
	public String findProcessSystemList() {
		try {
			// 获取部门所在级别
			int depth = webProcessService.findFlowDepth(flowId);

			processSystemList = webProcessService.getProcessSystemList(flowId, getProjectId(), depth, showAll);

			// 数据
			StringBuffer storesBuf = new StringBuffer();
			storesBuf.append("[");
			for (int i = 0; i < processSystemList.getLevelTotal(); i++) {
				storesBuf.append(JecnCommon.storesJson(i + "level", i + "flowName") + ",");
				storesBuf.append(JecnCommon.storesJson(i + "levelNum", i + "flowIdInput") + ",");
			}
			storesBuf.append(JecnCommon.storesJson("flowId", "flowId") + ",");
			storesBuf.append(JecnCommon.storesJson("typeByData", "typeByData") + ",");
			storesBuf.append(JecnCommon.storesJson("processName", "processName") + ",");
			storesBuf.append(JecnCommon.storesJson("isflow", "isflow") + ",");
			storesBuf.append(JecnCommon.storesJson("flowIdInput", "flowIdInput") + ",");

			storesBuf.append(JecnCommon.storesJson("flowNum", "flowNum") + ",");

			storesBuf.append(JecnCommon.storesJson("resPeopleName", "resPeopleName") + ",");
			storesBuf.append(JecnCommon.storesJson("orgName", "orgName") + ",");
			storesBuf.append(JecnCommon.storesJson("versionId", "versionId") + ",");
			storesBuf.append(JecnCommon.storesJson("guardianId", "guardianId") + ",");
			storesBuf.append(JecnCommon.storesJson("guardianName", "guardianName") + ",");
			storesBuf.append(JecnCommon.storesJson("draftPerson", "draftPerson") + ",");
			storesBuf.append(JecnCommon.storesJson("expiry", "expiry") + ",");
			storesBuf.append(JecnCommon.storesJson("optimization", "optimization") + ",");
			storesBuf.append(JecnCommon.storesJson("pubDate", "pubDate") + ",");
			// 下次审视优化时间
			storesBuf.append(JecnCommon.storesJson("reviewDate", "reviewDate") + "]");
			stores = storesBuf.toString();

			// 表头
			StringBuffer columnsBuf = new StringBuffer();
			columnsBuf.append("[new Ext.grid.RowNumberer(),");
			for (int i = 0; i < processSystemList.getLevelTotal(); i++) {
				if (i >= depth) {
					// 级名称
					columnsBuf.append(JecnCommon.columnsJson(i + JecnUtil.getValue("levelName"), i + "level", null)
							+ ",");
					// 级编号
					columnsBuf.append(JecnCommon
							.columnsJson(i + JecnUtil.getValue("levelNumber"), i + "levelNum", null)
							+ ",");
				}
			}
			// 流程名称
			columnsBuf.append(JecnCommon
					.columnsJson(JecnUtil.getValue("processName"), "processName", "processRenderer")
					+ ",");
			// 流程编号
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processNumber"), "flowIdInput", null) + ",");

			// 发布状态
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("pubState"), "flowNum", null) + ",");

			// 责任部门
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("responsibilityDepartment"), "orgName", null)
					+ ",");
			// 流程责任人
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processResponsiblePersons"), "resPeopleName",
					null)
					+ ",");
			boolean isGuardShow = false;
			if ("1".equals(JecnContants.getValue("guardianPeople"))) {
				isGuardShow = true;
			}
			if (isGuardShow) {
				// 流程监护人
				columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processTheGuardian"), "guardianName", null)
						+ ",");
			}
			// 拟稿人
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("peopleMakeADraft"), "draftPerson", null) + ",");
			// 版本号
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("versionNumber"), "versionId", null) + ",");
			// 发布日期
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("releaseDate"), "pubDate", null) + ",");
			// 有效日期
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("validity"), "expiry", null) + ",");
			// 是否及时优化
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("theTimelyOptimization"), "optimization", null)
					+ ",");
			// 下次审视优化时间
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("nextViewNeedFinishTime"), "reviewDate", null)
					+ "]");

			columns = columnsBuf.toString();

			StringBuffer sb = new StringBuffer("[");

			ProcessWebBean processWebBean = null;
			// 取出级别目录
			int resultSize = processSystemList.getListProcessWebBean().size();
			for (int i = 0; i < resultSize; i++) {
				processWebBean = processSystemList.getListProcessWebBean().get(i);
				if (processWebBean.getIsflow() == 0) {
					continue;
				}
				sb.append("{");
				String flowName = processWebBean.getFlowName();
				// 流程ID
				sb.append(JecnCommon.contentJson("flowId", processWebBean.getFlowId()) + ",");
				// 存储每个流程的所属流程地图信息
				List<Object[]> mapInfo = new ArrayList<Object[]>();
				// 获取级别信息
				ProcessMapTemplateDownload.getFlowLevelInfo(processWebBean, processSystemList.getListProcessWebBean(),
						mapInfo);
				// 追加级别信息至JSON
				for (Object[] objects : mapInfo) {
					sb.append(JecnCommon.contentJson(objects[0] + "flowName", objects[1]) + ",");
					sb.append(JecnCommon.contentJson(objects[0] + "flowIdInput", objects[2]) + ",");
				}

				// 流程名称
				sb.append(JecnCommon.contentJson("processName", flowName) + ",");
				// 流程编号
				sb.append(JecnCommon.contentJson("flowIdInput", processWebBean.getFlowIdInput()) + ",");
				// 发布状态
				switch (processWebBean.getTypeByData()) {
				case 0:
					// 待建
					sb.append(JecnCommon.contentJson("flowNum", JecnUtil.getValue("hasNotPub")) + ",");
					break;
				case 1:
					// 审批
					sb.append(JecnCommon.contentJson("flowNum", JecnUtil.getValue("waitForApprove")) + ",");
					break;
				case 2:
					// 发布
					sb.append(JecnCommon.contentJson("flowNum", JecnUtil.getValue("hasPub")) + ",");
					break;
				default:
					break;
				}

				// 监护人ID
				sb.append(JecnCommon.contentJson("guardianId", processWebBean.getGuardianId()) + ",");
				// 监护人名称
				sb.append(JecnCommon.contentJson("guardianName", processWebBean.getGuardianName()) + ",");
				// 流程类型
				sb.append(JecnCommon.contentJson("flowType", processWebBean.getFlowType()) + ",");
				// 拟稿人
				sb.append(JecnCommon.contentJson("draftPerson", processWebBean.getDraftPerson()) + ",");
				// 有效日期
				String expiry = "";
				if (processWebBean.getTypeByData() == 2) {
					if (processWebBean.getExpiry() == 0) {
						// 永久
						expiry = JecnUtil.getValue("permanent");
					} else {
						expiry = String.valueOf(processWebBean.getExpiry());
					}
				}

				sb.append(JecnCommon.contentJson("expiry", expiry) + ",");
				// 是否及时优化
				String optimizationVal = "";
				if (1 == processWebBean.getOptimization()) {
					// 是
					optimizationVal = JecnUtil.getValue("is");
				} else if (2 == processWebBean.getOptimization()) {
					// 否
					optimizationVal = JecnUtil.getValue("no");
				}
				sb.append(JecnCommon.contentJson("optimization", optimizationVal) + ",");
				// 密级
				sb.append(JecnCommon.contentJson("isPublic", processWebBean.getIsPublic()) + ",");
				// 是否为流程
				sb.append(JecnCommon.contentJson("isflow", processWebBean.getIsflow()) + ",");
				// 流程所处级别
				sb.append(JecnCommon.contentJson("level", processWebBean.getLevel()) + ",");
				// 流程未更新时间
				sb.append(JecnCommon.contentJson("noUpdateDate", processWebBean.getNoUpdateDate()) + ",");
				// 责任部门ID
				sb.append(JecnCommon.contentJson("orgId", processWebBean.getOrgId()) + ",");
				// 责任部门名称
				String orgName = "<a href='organization.action?orgId=" + processWebBean.getOrgId()
						+ "' target='_blank' style='cursor: pointer;' class='table'>" + processWebBean.getOrgName()
						+ "</a>";
				sb.append(JecnCommon.contentJson("orgName", orgName) + ",");
				// 发布时间
				sb.append(JecnCommon.contentJson("pubDate", processWebBean.getPubDate()) + ",");
				// 流程责任人ID
				sb.append(JecnCommon.contentJson("resPeopleId", processWebBean.getResPeopleId()) + ",");
				// 流程责任人名称
				sb.append(JecnCommon.contentJson("resPeopleName", processWebBean.getResPeopleName()) + ",");
				// 流程所处阶段
				sb.append(JecnCommon.contentJson("typeByData", processWebBean.getTypeByData()) + ",");
				// 流程责任人类型
				sb.append(JecnCommon.contentJson("typeResPeople", processWebBean.getTypeResPeople()) + ",");
				// 流程更新状态
				sb.append(JecnCommon.contentJson("updateType", processWebBean.getUpdateType()) + ",");
				// 流程版本号
				sb.append(JecnCommon.contentJson("versionId", processWebBean.getVersionId()) + ",");
				// 下次审视需完成时间
				if (processWebBean.getExpiry() != 0) {
					sb.append(JecnCommon.contentJson("reviewDate", processWebBean.getNextScanDate()));
				}
				// 有效期为永久时会多出一个逗号，导致IE6数据显示问题
				if (sb.toString().endsWith(",")) {
					sb.setLength(sb.length() - 1);
				}
				sb.append("},");
			}
			// 存在流程数据的情况下，截取最后一个逗号
			if (sb.toString().endsWith(",")) {
				sb.setLength(sb.length() - 1);
			}
			// 去掉最后一个逗号
			sb.append("]");
			outJsonString(JecnCommon.composeJson(stores, columns, sb.toString(), processSystemList.getNoPubFlowTotal(),
					processSystemList.getApproveFlowTotal(), processSystemList.getPubFlowTotal(), processSystemList
							.getFlowTotal()));
		} catch (Exception e) {
			log.error("获取流程清单错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程体系 下载
	 * 
	 * @author fuzhh Jan 17, 2013
	 * @return
	 */
	public String downfindProcessSystemList() {
		try {
			// 获取部门所在级别
			int depth = webProcessService.findFlowDepth(flowId);
			processSystemList = webProcessService.getProcessSystemList(flowId, getProjectId(), depth, showAll);
			if (processSystemList == null) {
				throw new NullPointerException("获取流程清单错误！");
			}
			byte[] content = ProcessMapTemplateDownload.createExcelile(processSystemList, depth);
			createDoc = new JecnCreateDoc();
			createDoc.setBytes(content);
			// 流程清单文件名称
			fileName = downProcessName;
		} catch (Exception e) {
			log.error(e);
		}
		return SUCCESS;
	}

	/**
	 * 流程地图概况信息
	 * 
	 * @author fuzhh Jan 17, 2013
	 * @return
	 */
	public String findProcessMapBaseInfo() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			processMapBaseInfo = webProcessService.getProcessMapBaseInfo(flowId, isp);
		} catch (Exception e) {
			log.error("获取流程地图概况信息错误！", e);
		}
		return SUCCESS;
	}

	/** ***********************【流程KPI处理 end】*********************************** */

	/**
	 * 部门主导流程搜索
	 */
	@SuppressWarnings("unchecked")
	public String searchDeptProcessList() {
		ProcessSearchBean processSearchBean = new ProcessSearchBean();
		processSearchBean.setProcessName(processName);
		processSearchBean.setProcessNum(processNum);
		// 责任部门
		processSearchBean.setOrgId(orgId);
		processSearchBean.setProjectId(getProjectId());
		processSearchBean.setPeopleId(getPeopleId());
		try {
			int total = webProcessService.getTotalSearchDeptProcess(processSearchBean);
			List<ProcessWebBean> list = webProcessService.searchDeptProcess(processSearchBean, start, limit);
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("部门主导流程搜索错误！", e);
		}
		return null;
	}

	/**
	 * 操作说明下载
	 * 
	 * @author fuzhh Jan 21, 2013
	 * @return
	 */
	public String printFlowDoc() {
		TreeNodeType nodeType = TreeNodeType.process;
		if ("true".equals(isMap)) {
			nodeType = TreeNodeType.processMap;
		}
		boolean isp = true;
		if ("false".equals(isPub)) {
			isp = false;
		}
		try {
			isApp = this.getRequest().getParameter("isApp");
			createDoc = webProcessService.printFlowDoc(flowId, nodeType, isp, getPeopleId(), historyId == 0 ? null
					: historyId);
			if (createDoc != null) {
				fileName = createDoc.getFileName();
			} else {
				fileName = "process.doc";
			}
			if (!fileName.endsWith(".doc")) {
				fileName = JecnUtil.getShowName(fileName, createDoc.getIdInput());
				fileName = fileName + ".doc";
			}
		} catch (Exception e) {
			log.error("获取流程操作说明字节流错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 下载角色流程对应关系
	 * 
	 * @author fuzhh May 7, 2013
	 * @return
	 */
	public String downActiveRole() {
		boolean isp = true;
		if ("false".equals(isPub)) {
			isp = false;
		}
		ActiveRoleBean activeRoleBean;
		try {
			activeRoleBean = webProcessService.getProcessActiveWeb(flowId, isp);
			ActiveRoleDownloadExcel activeRoleDownload = new ActiveRoleDownloadExcel();
			// 获取生成文件的路径
			String path = activeRoleDownload.activeRoleDownloadExcel(activeRoleBean);
			// 读取文件为字节流
			byte[] bytes = JecnFinal.getByteByPath(path);
			// 获取文件
			File file = new File(path);
			// 文件是否存在
			if (file.exists()) {
				// 删除文件
				file.delete();
			}
			createDoc = new JecnCreateDoc();
			createDoc.setBytes(bytes);
			fileName = "roleActive.xls";
		} catch (Exception e) {
			log.error(e);
		}
		return SUCCESS;
	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		// 获得文件
		if (createDoc == null && createDoc.getBytes() == null) {
			// 下载文件信息异常
			outResultHtml(this.getText("fileInformation"), "");
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(createDoc.getBytes());
	}

	/**
	 * 活动明细
	 * 
	 * @author fuzhh Jan 22, 2013
	 * @return
	 */
	public String findProcessActiveWebBean() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			// 是否显示活动类别 1显示 0隐藏
			if ("1".equals(JecnContants.getValue("activityType"))) {
				activityTypeIsShow = true;
			} else {
				activityTypeIsShow = false;
			}

			processActiveWebBean = activeService.getProcessActiveWebBean(activeId, isp);
			return "activitiesDetailed";
		} catch (Exception e) {
			log.error("获取活动说明数据异常!", e);
		}
		return null;
	}

	/**
	 * 流程选择查询数据
	 * 
	 * @author fuzhh Nov 30, 2012
	 * @return
	 */
	public String processSearch() {
		try {
			List<JecnTreeBean> list = webProcessService.searchByName(processName, getProjectId());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询流程数据错误！", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-11-26
	 * @description:文控信息
	 * @return
	 */
	public String findProcessHistoryNewDetail() {
		try {
			taskHistoryNew = webProcessService.getJecnTaskHistoryNew(historyId);
			// 将变更说明的换行改为br,使得可以在页面换行
			if (!JecnCommon.isNullOrEmtryTrim(taskHistoryNew.getModifyExplain())) {
				taskHistoryNew.setModifyExplain(taskHistoryNew.getModifyExplain().replaceAll("\n", "<br/>"));
			}
			if (taskHistoryNew != null && taskHistoryNew.getListJecnTaskHistoryFollow() != null) {
				for (JecnTaskHistoryFollow taskHistoryFollow : taskHistoryNew.getListJecnTaskHistoryFollow()) {
					if (taskHistoryFollow.getAppellation() != null
							&& !"".equals(taskHistoryFollow.getAppellation().trim())
							&& !taskHistoryFollow.getAppellation().endsWith("：")) {
						taskHistoryFollow.setAppellation(taskHistoryFollow.getAppellation() + "：");
					}
				}
			}
		} catch (Exception e) {
			log.error("查询文控详细信息错误！", e);
		}
		return SUCCESS;
	}

	public void setNode(long node) {
		this.node = node;
	}

	public String getAllProcessType() {
		try {
			List<ProceeRuleTypeBean> processTypeList = processRuleTypeService.getFlowAttributeType();
			getRequest().setAttribute("processTypeList", processTypeList);
			return SUCCESS;
		} catch (Exception e) {
			log.error("获取制度类别错误！", e);
		}
		return null;
	}

	/**
	 * @author yxw 2013-1-28
	 * @description: 流程详情跳转
	 * @return
	 */
	public String process() {
		JecnFlowStructure jecnFlowStructure = null;
		try {
			if (StringUtils.isBlank(type) || type.equals("process") || type.equals("processMap")) {
				jecnFlowStructure = webProcessService.getJecnFlowStructure(flowId);
				if (null == jecnFlowStructure) {
					return JecnFinal.NOPUBLIC;// 没有发布
				}
				if (jecnFlowStructure != null && jecnFlowStructure.getDelState() == 1) {
					super.ResultHtml(JecnUtil.getValue("thisProcessIsOnRecycle"), "");
					return null;
				}
				if (StringUtils.isBlank(type)) {// SVG 点击链接
					type = jecnFlowStructure.getIsFlow() == 1 ? "process" : "processMap";
					if (!StringUtils.isBlank(isPub) && "false".equals(isPub)) {
						type = type + "T";
					}
				}
			}
			return type;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return null;
	}

	/**
	 * @author yxw 2013-3-26
	 * @description:空方法，用于树节点双击权限验证
	 * @return
	 */
	public String processLook() {
		return null;
	}

	/**
	 * @author yxw 2013-3-1
	 * @description:我的工作模版及指导书
	 * @return
	 */
	public String myWorkModelDirectBook() {
		int total;
		try {
			total = webProcessService.getTotalMyWorkModel(flowId, activeId, fileSearchName, getPeopleId(),
					getProjectId());
			if (total > 0) {
				List<FileWebBean> list = webProcessService.searchMyWorkModel(flowId, activeId, fileSearchName,
						getPeopleId(), getProjectId(), start, limit);
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonPage(jsonArray.toString(), total);
				return SUCCESS;
			} else {
				outJsonPage("[]", total);
			}
		} catch (Exception e) {
			log.error("查询我的工作模板错误！", e);
		}

		return null;
	}

	/**
	 * @author yxw 2013-3-1
	 * @description:我参与的流程 下拉列表显示
	 * @return
	 */
	public String myJoinProcessList() {
		try {
			List<KeyValueBean> list = webProcessService.getMyJoinProcessList(getPeopleId(), getProjectId());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonString(jsonArray.toString());
		} catch (Exception e) {
			log.error("查询我参与的流程出错", e);
		}
		return null;
	}

	/**
	 * @author yxw 2013-3-1
	 * @description:根据流程和用户ID得到相关活动
	 * @return
	 */
	public String activeListByProcessId() {
		try {
			List<KeyValueBean> list = webProcessService.getActiveListByProcessId(getPeopleId(), flowId);
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonString(jsonArray.toString());
		} catch (Exception e) {
			log.error("根据流程和用户ID得到相关活动出错", e);
		}
		return null;
	}

	/**
	 * 流程检错
	 * 
	 * @author fuzhh 2013-9-9
	 * @return
	 */
	public String processCheckResult() {
		int total;
		try {
			total = webProcessService.getProcessCheckCount(processCheckType, getProjectId());
			// 获取流程检错信息
			processCheckList = webProcessService.getProcessCheckList(processCheckType, start, limit, getProjectId());

			JSONArray jsonArray = JSONArray.fromObject(processCheckList);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("获取流程检错信息错误！", e);
		}
		return null;
	}

	/**
	 * 2014-04-22
	 * 
	 * @description:数据对接，我关注的流程、我参与的流程
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String buttPublicProcess() {
		try {
			int total = webProcessService.getTotalButtProcess(getProjectId(), getPeopleId());
			List<ProcessWebBean> list = webProcessService.getButtProcess(start, limit, getProjectId(), getPeopleId());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("查询我关注的流程、参与的流程报错！", e);
		}
		return null;
	}

	/**
	 * 根据流程类型搜索流程
	 * 
	 * @return
	 */
	public String searchFlowByName() {
		try {
			// 1是流程地图，2是流程
			int flowType = 0;
			if ("process".equals(type)) {
				flowType = 2;
			} else if ("processMap".equals(type)) {
				flowType = 1;
			}
			List<JecnTreeBean> searchFlowByName = flowStructureService.searchPubFlowByName(searchName, getProjectId(),
					flowType);
			// 序列化为json
			JSONArray jsonArray = JSONArray.fromObject(searchFlowByName);
			outJsonString(jsonArray.toString());

		} catch (Exception e) {
			log.error("通过流程名称搜索流程图或者地图错误！", e);
		}

		return null;
	}

	/** * 根据KPIANDID 和日期验证KPI 数据唯一性 *** */
	private boolean checkFlowKpiImage(long kpiAndId, String kpiHorVlaue) {
		try {
			jecnFlowKpi = flowPKIService.getFlowKPIValue(kpiAndId, kpiHorVlaue);
			if (jecnFlowKpi != null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * 
	 * 获取单元格内容
	 * 
	 * @param cell
	 * @return
	 */
	private String getCellText(Cell cell) {
		if (isNullObj(cell)) {
			return null;
		} else {
			String cellContent = cell.getContents();
			if (cellContent != null) {
				return cellContent.trim();
			}
			return null;
		}
	}

	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public void ResultHtmlNotClose(String info) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			HttpServletResponse response = getResponse();
			response.setCharacterEncoding("utf-8");
			response.setContentType("text/html");
			out = response.getWriter();
			out.print(info);
			out.flush();
		} catch (IOException e) {
			log.error("ProcessAction向浏览端输出信息错误！", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception ex) {
					log.error("ProcessAction类ResultHtmlNotClose关闭流异常", ex);
				}
			}
		}
	}

	/**
	 * 导入流程有效期维护数据
	 * 
	 * @author huoyl
	 * @return
	 */
	public String importValidityData() {

		if (upload == null || uploadFileName == null || uploadContentType == null) {
			this.ResultHtmlNotClose(this.getText("fileCanNotNull"));
		} else {
			try {
				WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
				Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
				// 返回是否有错误数据的信息
				int[] result = flowStructureService.importValidityData(upload, curPeopleId, 0);
				// 导入 条，未导入 条
				this.ResultHtmlNotClose(this.getText("import") + result[0] + this.getText("item") + ","
						+ this.getText("notImport") + result[1] + this.getText("item"));

			} catch (Exception e) {
				this.ResultHtmlNotClose(this.getText("importFail"));
				log.error("导入流程有效期维护数据异常", e);
			}
		}

		return NONE;
	}

	public IWebProcessService getWebProcessService() {
		return webProcessService;
	}

	public List<JecnRoleProcessBean> getRoleProcessList() {
		return roleProcessList;
	}

	public void setRoleProcessList(List<JecnRoleProcessBean> roleProcessList) {
		this.roleProcessList = roleProcessList;
	}

	public void setFlowStructureService(IFlowStructureService flowStructureService) {
		this.flowStructureService = flowStructureService;
	}

	public void setWebProcessService(IWebProcessService webProcessService) {
		this.webProcessService = webProcessService;
	}

	public long getNode() {
		return node;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessNum() {
		return processNum;
	}

	public void setProcessNum(String processNum) {
		this.processNum = processNum;
	}

	public IFlowStructureService getFlowStructureService() {
		return flowStructureService;
	}

	public long getFlowId() {
		return flowId;
	}

	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}

	public ProcessShowFile getProcessShowFile() {
		return processShowFile;
	}

	public void setProcessShowFile(ProcessShowFile processShowFile) {
		this.processShowFile = processShowFile;
	}

	public ProcessMapDownloadBean getProcessMapDownloadBean() {
		return processMapDownloadBean;
	}

	public void setProcessMapDownloadBean(ProcessMapDownloadBean processMapDownloadBean) {
		this.processMapDownloadBean = processMapDownloadBean;
	}

	public ProcessAttributeWebBean getProcessAttribute() {
		return processAttribute;
	}

	public void setProcessAttribute(ProcessAttributeWebBean processAttribute) {
		this.processAttribute = processAttribute;
	}

	public long getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}

	public long getPosId() {
		return posId;
	}

	public void setPosId(long posId) {
		this.posId = posId;
	}

	public IFlowPKIService getFlowPKIService() {
		return flowPKIService;
	}

	public void setFlowPKIService(IFlowPKIService flowPKIService) {
		this.flowPKIService = flowPKIService;
	}

	public long getOrgLookId() {
		return orgLookId;
	}

	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}

	public String getOrgLookName() {
		return orgLookName;
	}

	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}

	public long getPosLookId() {
		return posLookId;
	}

	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}

	public String getPosLookName() {
		return posLookName;
	}

	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}

	public List<ProcessActiveWebBean> getProcessActiveTemplateList() {
		return processActiveTemplateList;
	}

	public void setProcessActiveTemplateList(List<ProcessActiveWebBean> processActiveTemplateList) {
		this.processActiveTemplateList = processActiveTemplateList;
	}

	public List<JecnTaskHistoryNew> getTaskHistoryNewList() {
		return taskHistoryNewList;
	}

	public void setTaskHistoryNewList(List<JecnTaskHistoryNew> taskHistoryNewList) {
		this.taskHistoryNewList = taskHistoryNewList;
	}

	public void setRelatedProcessRuleStandard(RelatedProcessRuleStandardBean relatedProcessRuleStandard) {
		this.relatedProcessRuleStandard = relatedProcessRuleStandard;
	}

	public RelatedProcessRuleStandardBean getRelatedProcessRuleStandard() {
		return relatedProcessRuleStandard;
	}

	public ProcessBaseInfoBean getProcessBaseInfoBean() {
		return processBaseInfoBean;
	}

	public void setProcessBaseInfoBean(ProcessBaseInfoBean processBaseInfoBean) {
		this.processBaseInfoBean = processBaseInfoBean;
	}

	public TempProcessKpiBean getTempProcessKpiBean() {
		return tempProcessKpiBean;
	}

	public void setTempProcessKpiBean(TempProcessKpiBean tempProcessKpiBean) {
		this.tempProcessKpiBean = tempProcessKpiBean;
	}

	public long getProcessType() {
		return processType;
	}

	public void setProcessType(long processType) {
		this.processType = processType;
	}

	public ProcessSystemListBean getProcessSystemList() {
		return processSystemList;
	}

	public void setProcessSystemList(ProcessSystemListBean processSystemList) {
		this.processSystemList = processSystemList;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public ProcessMapBaseInfoBean getProcessMapBaseInfo() {
		return processMapBaseInfo;
	}

	public void setProcessMapBaseInfo(ProcessMapBaseInfoBean processMapBaseInfo) {
		this.processMapBaseInfo = processMapBaseInfo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public TempSearchKpiBean getSearchKpiBean() {
		return searchKpiBean;
	}

	public void setSearchKpiBean(TempSearchKpiBean searchKpiBean) {
		this.searchKpiBean = searchKpiBean;
	}

	public String getFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), fileName, isApp);
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public JecnCreateDoc getCreateDoc() {
		return createDoc;
	}

	public void setCreateDoc(JecnCreateDoc createDoc) {
		this.createDoc = createDoc;
	}

	public String getIsMap() {
		return isMap;
	}

	public void setIsMap(String isMap) {
		this.isMap = isMap;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public long getActiveId() {
		return activeId;
	}

	public void setActiveId(long activeId) {
		this.activeId = activeId;
	}

	public ProcessActiveWebBean getProcessActiveWebBean() {
		return processActiveWebBean;
	}

	public void setProcessActiveWebBean(ProcessActiveWebBean processActiveWebBean) {
		this.processActiveWebBean = processActiveWebBean;
	}

	public String getActiveName() {
		return activeName;
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}

	public JecnTaskHistoryNew getTaskHistoryNew() {
		return taskHistoryNew;
	}

	public void setTaskHistoryNew(JecnTaskHistoryNew taskHistoryNew) {
		this.taskHistoryNew = taskHistoryNew;
	}

	public long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}

	public String getIsPub() {
		return isPub;
	}

	public void setIsPub(String isPub) {
		this.isPub = isPub;
	}

	public List<ProcessActiveWebBean> getListProcessActiveWebBean() {
		return listProcessActiveWebBean;
	}

	public void setListProcessActiveWebBean(List<ProcessActiveWebBean> listProcessActiveWebBean) {
		this.listProcessActiveWebBean = listProcessActiveWebBean;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFileSearchName() {
		return fileSearchName;
	}

	public void setFileSearchName(String fileSearchName) {
		this.fileSearchName = fileSearchName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getOtherLoginType() {
		return otherLoginType;
	}

	public void setOtherLoginType(int otherLoginType) {
		this.otherLoginType = otherLoginType;
	}

	public List<ProcessBaseInfoBean> getProcessCheckList() {
		return processCheckList;
	}

	public void setProcessCheckList(List<ProcessBaseInfoBean> processCheckList) {
		this.processCheckList = processCheckList;
	}

	public long getProcessCheckType() {
		return processCheckType;
	}

	public void setProcessCheckType(long processCheckType) {
		this.processCheckType = processCheckType;
	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public String getStores() {
		return stores;
	}

	public void setStores(String stores) {
		this.stores = stores;
	}

	public boolean getShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}

	public List<Object[]> getRuleObj() {
		return ruleObj;
	}

	public void setRuleObj(List<Object[]> ruleObj) {
		this.ruleObj = ruleObj;
	}

	public IWebProcessActiveService getActiveService() {
		return activeService;
	}

	public int getIsFlow() {
		return isFlow;
	}

	public void setIsFlow(int isFlow) {
		this.isFlow = isFlow;
	}

	public void setActiveService(IWebProcessActiveService activeService) {
		this.activeService = activeService;
	}

	public List<JecnFlowKpi> getListFlowKpi() {
		return listFlowKpi;
	}

	public void setListFlowKpi(List<JecnFlowKpi> listFlowKpi) {
		this.listFlowKpi = listFlowKpi;
	}

	public JecnFlowKpi getJecnFlowKpi() {
		return jecnFlowKpi;
	}

	public void setJecnFlowKpi(JecnFlowKpi jecnFlowKpi) {
		this.jecnFlowKpi = jecnFlowKpi;
	}

	public long getKpiAndId() {
		return kpiAndId;
	}

	public void setKpiAndId(long kpiAndId) {
		this.kpiAndId = kpiAndId;
	}

	public long getKpiId() {
		return kpiId;
	}

	public void setKpiId(long kpiId) {
		this.kpiId = kpiId;
	}

	public String getKpiHorVlaue() {
		return kpiHorVlaue;
	}

	public void setKpiHorVlaue(String kpiHorVlaue) {
		this.kpiHorVlaue = kpiHorVlaue;
	}

	public String getKpiValue() {
		return kpiValue;
	}

	public void setKpiValue(String kpiValue) {
		this.kpiValue = kpiValue;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getArrKpi() {
		return arrKpi;
	}

	public void setArrKpi(String arrKpi) {
		this.arrKpi = arrKpi;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	public void setConfigService(IJecnConfigItemService configService) {
		this.configService = configService;
	}

	public String getArtificialPersonValue() {
		return JecnContants.getValue("guardianPeople");
	}

	public void setArtificialPersonValue(String artificialPersonValue) {
		this.artificialPersonValue = artificialPersonValue;
	}

	public String getResponsibilityPeopleValue() {
		return JecnContants.getValue("fictionPeople");
	}

	public void setResponsibilityPeopleValue(String responsibilityPeopleValue) {
		this.responsibilityPeopleValue = responsibilityPeopleValue;
	}

	public int getOtherOperaType() {
		return otherOperaType;
	}

	public void setOtherOperaType(int otherOperaType) {
		this.otherOperaType = otherOperaType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getKpiHorType() {
		return kpiHorType;
	}

	public void setKpiHorType(String kpiHorType) {
		this.kpiHorType = kpiHorType;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public IOrgAccessPermissionsService getOrgAccessPermissionsService() {
		return orgAccessPermissionsService;
	}

	public void setOrgAccessPermissionsService(IOrgAccessPermissionsService orgAccessPermissionsService) {
		this.orgAccessPermissionsService = orgAccessPermissionsService;
	}

	public boolean isProcessFileDownloadIsShow() {
		return processFileDownloadIsShow;
	}

	public void setProcessFileDownloadIsShow(boolean processFileDownloadIsShow) {
		this.processFileDownloadIsShow = processFileDownloadIsShow;
	}

	public boolean isActivityTypeIsShow() {
		return activityTypeIsShow;
	}

	public void setActivityTypeIsShow(boolean activityTypeIsShow) {
		this.activityTypeIsShow = activityTypeIsShow;
	}

	public boolean isProcessTypeIsShow() {
		return processTypeIsShow;
	}

	public void setProcessTypeIsShow(boolean processTypeIsShow) {
		this.processTypeIsShow = processTypeIsShow;
	}

	public boolean isSupportToolsIsShow() {
		return supportToolsIsShow;
	}

	public void setSupportToolsIsShow(boolean supportToolsIsShow) {
		this.supportToolsIsShow = supportToolsIsShow;
	}

	public boolean isAccessoriesIsShow() {
		return accessoriesIsShow;
	}

	public void setAccessoriesIsShow(boolean accessoriesIsShow) {
		this.accessoriesIsShow = accessoriesIsShow;
	}

	public boolean isRuleTypeIsShow() {
		return ruleTypeIsShow;
	}

	public void setRuleTypeIsShow(boolean ruleTypeIsShow) {
		this.ruleTypeIsShow = ruleTypeIsShow;
	}
}
