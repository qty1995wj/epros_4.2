package epros.draw.gui.line.shape;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.line.JecnBaseDividingLinePanel;

/**
 * 
 * 注释框的线段
 * 
 * @author ZHOUXY
 * 
 */
public class CommentLine extends JecnBaseDividingLinePanel {
	/** 线段关联的注释框 */
	private CommentText commentText = null;

	public CommentLine(JecnBaseDivedingLineData flowElementData) {
		super(flowElementData);
		this.setLineResizePanelEnd(null);
		this.setLineResizePanelCenter(null);
	}

	/**
	 * 画线
	 * 
	 * @param g
	 */
	public void paintComentLine(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; // 定义画笔
		g2d.setStroke(this.getFlowElementData().getBodyBasicStroke());// fulin
		// 加
		// 设置虚线用
		g2d.setColor(this.getFlowElementData().getBodyColor());
		g2d.drawLine(this.getStartPoint().x, this.getStartPoint().y, this
				.getEndPoint().x, this.getEndPoint().y);
	}

	public JecnBaseDivedingLineData getFlowElementData() {
		if (commentText == null) {
			return (JecnBaseDivedingLineData) flowElementData;
		}
		return (JecnBaseDivedingLineData) commentText.getFlowElementData()
				.getLineData();
	}

	/**
	 * 获取克隆数据对象
	 */
	public CommentLine clone() {
		CommentLine commentLine = (CommentLine) this.currClone();
		return commentLine;
	}

	public CommentText getCommentText() {
		return commentText;
	}

	public void setCommentText(CommentText commentText) {
		this.commentText = commentText;
	}
}
