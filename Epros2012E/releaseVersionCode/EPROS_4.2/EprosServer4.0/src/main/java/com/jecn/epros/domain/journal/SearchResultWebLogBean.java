package com.jecn.epros.domain.journal;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.common.relatedfiles.RelatedFileBean;
import com.jecn.epros.service.TreeNodeUtils;

import java.util.List;

public class SearchResultWebLogBean extends FlowSearchResultWebLogBean {

    private LinkResource.ResourceType type;

    private String dirName;

    /**
     * 相关文件
     */
    List<RelatedFileBean> relatedFiles;

    public List<RelatedFileBean> getRelatedFiles() {
        return relatedFiles;
    }

    public void setRelatedFiles(List<RelatedFileBean> relatedFiles) {
        this.relatedFiles = relatedFiles;
    }

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public LinkResource getLink() {
        return new LinkResource(super.getFlowId(), super.getFlowName(), type, TreeNodeUtils.NodeType.search);
    }

    public LinkResource.ResourceType getType() {
        return type;
    }

    public void setType(LinkResource.ResourceType type) {
        this.type = type;
    }
}
