<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
              document.body.onresize=function(){
			    setShowDocument('ruleDocumentDiv')
			 }
		</script>
	</head>

	<body>
		<div id="ruleDocumentDiv" class="div" style="width: 100%;overflow-y: scroll;" align="center">
				<table width="98%" border="0" cellpadding="0" cellspacing="1"
					align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
					<tr class="FixedTitleRow" align="center">
					    <td width="2%" class="gradient">
					    </td>
						<td width="15%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 版本号 -->
							<s:text name="versionNumber" />
						</td>
						<td width="15%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 拟稿人 -->
							<s:text name="peopleMakeADraft" />
						</td>
						<td width="15%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 开始时间 -->
							<s:text name="startTime" />
						</td>
						<td width="15%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 结束时间 -->
							<s:text name="endTime" />
						</td>
						<td width="15%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 返工次数 -->
							<s:text name="reworkNumber" />
						</td>
						<td width="15%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 发布日期 -->
							<s:text name="releaseDate" />
						</td>
						<td width="6%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 操作  -->
							<s:text name="operation" />
						</td>
						<td width="2%" class="gradient">
					    </td>
					</tr>
					<s:iterator value="taskHistoryNewList" status="st">
						<tr align="center" bgcolor="#ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#ffffff';" height="20">
							<td align="right" class="gradient">
								<s:property value="#st.index+1" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="versionId" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="draftPerson" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="startTime" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="endTime" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="approveCount" />
							</td>
							<td class="t5" style="word-break: break-all;">
							   <s:date name="publishDate" format="yyyy-MM-dd" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<a style="cursor: pointer;"
									href="processHistoryNewDetail.action?historyId=<s:property value='id'/>"
									target='_blank' class="table"><s:text name="view" /> </a>
							</td>
							<td></td>
						</tr>
					</s:iterator>
				</table>
		</div>
	</body>
</html>