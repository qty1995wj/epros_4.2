package wordxml.element.node.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.element.bean.common.BorderBean;
import wordxml.util.Tools;

public class CommonUtil {
	private static final Log log = LogFactory.getLog(CommonUtil.class);

	/**
	 * 构建边框
	 * 
	 * @param borderBean
	 *            边框属性bean
	 * @param borderEle
	 *            边框节点
	 */
	public static void buildBorder(BorderBean borderBean, Element borderEle) {
		if (borderBean == null || borderEle == null) {
			log.info("borderBean or borderEle is not valid");
			return;
		}
		borderEle.addAttribute("w:val", borderBean.getType().getValue());
		borderEle.addAttribute("wx:bdrwidth", "" + borderBean.getBorderWidth()
				* 20);
		borderEle.addAttribute("w:sz", "" + borderBean.getBorderWidth() * 8);
		borderEle.addAttribute("w:space", "" + borderBean.getSpace());
		borderEle.addAttribute("w:color", Tools.convertColorToRGBStr(borderBean
				.getColor()));

	}
}
