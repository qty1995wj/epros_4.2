package com.jecn.epros.server.service.reports.excel;

import java.io.File;
import java.util.Date;
import java.util.List;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.process.temp.TempApproveTimeHeadersBean;
import com.jecn.epros.server.bean.process.temp.TempTaskApproveTime;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnUtil;

public class TaskApproveTimeDownExcelService {

	private Log log = LogFactory.getLog(this.getClass());
	/** 数据对象 */
	private List<TempTaskApproveTime> taskApproveTimeList = null;
	private TempApproveTimeHeadersBean headersBean = new TempApproveTimeHeadersBean();

	public byte[] getExcelFile() {

		byte[] tempDate = null;
		if (taskApproveTimeList == null) {
			return null;
		}
		File file = null;
		WritableWorkbook wbookData = null;
		int startDataRow = 2;
		try {
			// 第一行
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);

			// 样式
			WritableCellFormat titleStyle = getTitleStyle();
			WritableCellFormat tableHeaderStyle = getTableHeaderStyle();
			WritableCellFormat taskStyle = getTableHeaderStyle();
			taskStyle.setBackground(Colour.YELLOW);
			WritableCellFormat tableContentStyle = getTableContentStyle();

			// 开始写入第一页
			WritableSheet dataSheet = wbookData.createSheet(JecnUtil
					.getValue("taskApproveTimeStatistics"), 0);
            dataSheet.setRowView(0, 400);
			dataSheet.addCell(new Label(0, 0, JecnUtil.getValue("prefixFirst")
					+ JecnCommon.getStringbyDate(new Date())
					+ JecnUtil.getValue("suffixFirst"), titleStyle));

			// 第二行
			List<String> columns = headersBean.getHeaders();
			dataSheet.setColumnView(0, 18);
			dataSheet.setRowView(1, 400);
			//任务名称
			dataSheet.addCell(new Label(0, 1, JecnUtil.getValue("taskName"), taskStyle));
			// 写入表头
			for (int i = 0; i < columns.size(); i++) {
				dataSheet.setColumnView(i+1, 22);
				String header = columns.get(i);
				header = (header == null) ? "" : header;
				dataSheet.addCell(new Label(i+1, 1, header, tableHeaderStyle));
			}

			// 第三行开始
			List<String> order = headersBean.getOrder();
			// 设置内容
			for (int i = 0; i < taskApproveTimeList.size(); i++) {
				TempTaskApproveTime bean = taskApproveTimeList.get(i);
				if (bean == null) {
					bean = new TempTaskApproveTime();
				}
				int row = startDataRow + i;
				dataSheet.setRowView(row, 360);
				// 任务名
				dataSheet.addCell(new Label(0, row, bean.getTaskName(),tableContentStyle));

				for (int j=0; j<order.size();j++) {
					int col=j+1;
					String mark=order.get(j);
					if (mark.equals("taskAppPeopleMake")) {// 拟稿人
						String peopleMake = JecnUtil.valueToString(bean
								.getPeopleMake());
						dataSheet.addCell(new Label(col, row, peopleMake,tableContentStyle));
					} else if (mark
							.equals(ConfigItemPartMapMark.taskAppDocCtrlUser
									.toString())) {// 文控审核人
						String documentControl = JecnUtil.valueToString(bean
								.getDocumentControl());
						dataSheet.addCell(new Label(col, row, documentControl,tableContentStyle));

					} else if (mark
							.equals(ConfigItemPartMapMark.taskAppDeptUser
									.toString())) { // 部门审核人
						String department = JecnUtil.valueToString(bean
								.getDepartment());
						dataSheet.addCell(new Label(col, row, department,tableContentStyle));

					} else if (mark
							.equals(ConfigItemPartMapMark.taskAppReviewer
									.toString())) {// 评审人会审
						String reviewPeople = JecnUtil.valueToString(bean
								.getReviewPeople());
						dataSheet.addCell(new Label(col, row, reviewPeople,tableContentStyle));

					} else if (mark.equals(ConfigItemPartMapMark.taskAppPzhUser
							.toString())) {// 批准人审核
						String approval = JecnUtil.valueToString(bean
								.getApproval());
						dataSheet.addCell(new Label(col, row, approval,tableContentStyle));

					} else if (mark
							.equals(ConfigItemPartMapMark.taskAppOprtUser
									.toString())) {// 各业务体系负责人
						String eachBusiness = JecnUtil.valueToString(bean
								.getEachBusiness());
						dataSheet.addCell(new Label(col, row, eachBusiness,tableContentStyle));

					} else if (mark.equals(ConfigItemPartMapMark.taskAppITUser
							.toString())) { // IT总监
						String director = JecnUtil.valueToString(bean
								.getDirector());
						dataSheet.addCell(new Label(col, row, director,tableContentStyle));

					} else if (mark
							.equals(ConfigItemPartMapMark.taskAppDivisionManager
									.toString())) {// 事业部经理
						String departmentManager = JecnUtil.valueToString(bean
								.getDepartmentManager());
						dataSheet.addCell(new Label(col, row, departmentManager,tableContentStyle));

					} else if (mark
							.equals(ConfigItemPartMapMark.taskAppGeneralManager
									.toString())) {// 总经理

						String manager = JecnUtil.valueToString(bean
								.getManager());
						dataSheet.addCell(new Label(col, row, manager,tableContentStyle));

					} else if ("finishingViews".equals(mark)) { // 整理意见
						String finishingViews = JecnUtil.valueToString(bean
								.getFinishingViews());
						dataSheet.addCell(new Label(col, row, finishingViews,tableContentStyle));
					}
				}
			}

			// 后续处理--------------------------------------
			// 把一个文件转化为字节
			
			wbookData.write();
			//工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData=null;
			
			// 把一个文件转化为字节
			tempDate = JecnUtil.getByte(file);
			return tempDate;
		} catch (Exception e) {
			log.error("TaskApproveTimeDownExcelService类getExcelFile方法异常", e);
			return null;
		} finally {
            if(wbookData!=null)
            {
            	try {
					wbookData.close();
				} catch (Exception e) {
					log.error("TaskApproveTimeDownExcelService关闭excel异常",e);
				} 
            }
			// 删除临时文件
			if (file != null && file.exists()) {
				file.delete();
			}
		}

	}

	// 标题栏
	private WritableCellFormat getTitleStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 11,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.RED);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.LEFT);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		return tempCellFormat;
	}

	// 表头
	private WritableCellFormat getTableHeaderStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 11,
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.LIGHT_ORANGE);
		tempCellFormat.setWrap(true);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	// 内容
	private WritableCellFormat getTableContentStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		return tempCellFormat;
	}

	public List<TempTaskApproveTime> getTaskApproveTimeList() {
		return taskApproveTimeList;
	}

	public void setTaskApproveTimeList(
			List<TempTaskApproveTime> taskApproveTimeList) {
		this.taskApproveTimeList = taskApproveTimeList;
	}

	public TempApproveTimeHeadersBean getHeadersBean() {
		return headersBean;
	}

	public void setHeadersBean(TempApproveTimeHeadersBean headersBean) {
		this.headersBean = headersBean;
	}

}
