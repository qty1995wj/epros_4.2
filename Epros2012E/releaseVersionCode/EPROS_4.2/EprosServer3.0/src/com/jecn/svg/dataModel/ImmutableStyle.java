package com.jecn.svg.dataModel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class ImmutableStyle {

	private Map<String, Object> baseGraphStyleValues = new HashMap<String, Object>();
	private Map<String, Object> baseTextStyleValues = new HashMap<String, Object>();
	private boolean noGraphStyle = false;
	private boolean noTextStyle = false;

	private static final Object NONE = new Object();
	private static Map<String, Field> baseGraphStyleFieldMap = new HashMap<String, Field>();
	private static Map<String, Field> baseGraphTextFieldMap = new HashMap<String, Field>();
	private static Map<String, ImmutableStyle> store = new HashMap<String, ImmutableStyle>();

	private static final Logger log = Logger.getLogger(ImmutableStyle.class);

	static {
		try {
			Field[] graphStyleFields = SvgGraphStyle.class.getDeclaredFields();
			for (Field f : graphStyleFields) {
				f.setAccessible(true);
				baseGraphStyleFieldMap.put(f.getName(), f);
			}

			Field[] textStyleFields = SvgTextStyle.class.getDeclaredFields();
			for (Field f : textStyleFields) {
				f.setAccessible(true);
				baseGraphTextFieldMap.put(f.getName(), f);
			}

			Method[] declaredMethods = ImmutableStyle.class.getDeclaredMethods();
			for (Method method : declaredMethods) {
				// 保留一个公共入口setImmutableStyleToGraph,其余方法均为静态private，用于初始化不可变样式Map
				if (Modifier.isPrivate(method.getModifiers())) {
					store.put(method.getName(), (ImmutableStyle) method.invoke(null));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setImmutableStyleToGraph(SvgBaseGraph graph) {
		if (graph == null) {
			return;
		}
		if (!store.containsKey(graph.getType())) {
			return;
		}
		try {
			ImmutableStyle style = store.get(graph.getType());
			if (style.noGraphStyle) {
				graph.setGraphStyle(null);
			} else {
				for (String fieldName : style.baseGraphStyleValues.keySet()) {
					if (style.baseGraphStyleValues.get(fieldName) == NONE) {
						baseGraphStyleFieldMap.get(fieldName).set(graph.getGraphStyle(), null);
					} else {
						baseGraphStyleFieldMap.get(fieldName).set(graph.getGraphStyle(),
								style.baseGraphStyleValues.get(fieldName));
					}
				}
			}

			if (style.noTextStyle) {
				graph.setTextStyle(null);
			} else {
				for (String fieldName : style.baseTextStyleValues.keySet()) {
					if (style.baseTextStyleValues.get(fieldName) == NONE) {
						baseGraphTextFieldMap.get(fieldName).set(graph.getTextStyle(), null);
					} else {
						baseGraphTextFieldMap.get(fieldName).set(graph.getTextStyle(),
								style.baseTextStyleValues.get(fieldName));
					}
				}
			}
		} catch (Exception e) {
			log.error("图形<" + graph.getType() + ">的不可变样式属性设置异常，请查看Function " + graph.getType(), e);
		}
	}

//	/**
//	 * 活动
//	 * 
//	 * @return
//	 */
//	private static ImmutableStyle RoundRectWithLine() {
//		ImmutableStyle style = new ImmutableStyle();
//		style.baseTextStyleValues.put("anchor", SvgTextStyle.textAnchors[4]);
//		return style;
//	}

	/**
	 * 返入
	 * 
	 * @return
	 */
	private static ImmutableStyle ReverseArrowhead() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseTextStyleValues.put("anchor", SvgTextStyle.textAnchors[4]);
		return style;
	}

	/**
	 * 返出
	 * 
	 * @return
	 */
	private static ImmutableStyle RightArrowhead() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseTextStyleValues.put("anchor", SvgTextStyle.textAnchors[4]);
		return style;
	}

	/**
	 * 信息系统
	 * 
	 * @return
	 */
	private static ImmutableStyle DataImage() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("transform", NONE);
		style.baseTextStyleValues.put("anchor", SvgTextStyle.textAnchors[4]);
		return style;
	}

	/**
	 * 协作框
	 * 
	 * @return
	 */
	private static ImmutableStyle DottedRect() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		return style;
	}

	/**
	 * PA
	 * 
	 * @return
	 */
	private static ImmutableStyle PAFigure() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		return style;
	}

	/**
	 * KSF
	 * 
	 * @return
	 */
	private static ImmutableStyle KSFFigure() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		return style;
	}

	/**
	 * KCP
	 * 
	 * @return
	 */
	private static ImmutableStyle KCPFigure() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		return style;
	}

	/**
	 * 图片插入框
	 * 
	 * @return
	 */
	private static ImmutableStyle IconFigure() {
		ImmutableStyle style = new ImmutableStyle();
		style.noGraphStyle = true;
		style.noTextStyle = true;
		return style;
	}

	/**
	 * IT决策
	 * 
	 * @return
	 */
	private static ImmutableStyle ITRhombus() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseTextStyleValues.put("anchor", SvgTextStyle.textAnchors[4]);
		return style;
	}

	/**
	 * 决策
	 * 
	 * @return
	 */
	private static ImmutableStyle Rhombus() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseTextStyleValues.put("anchor", SvgTextStyle.textAnchors[4]);
		return style;
	}

	/**
	 * 注释框
	 * 
	 * @return
	 */
	private static ImmutableStyle CommentText() {
		ImmutableStyle style = new ImmutableStyle();
		style.noGraphStyle = true;
		return style;
	}

	/**
	 * 阶段分隔符
	 * 
	 * @return
	 */
	private static ImmutableStyle PartitionFigure() {
		ImmutableStyle style = new ImmutableStyle();
		style.noTextStyle = true;
		style.baseGraphStyleValues.put("strokeWidth", NONE);
		style.baseGraphStyleValues.put("strokeDashArray", NONE);
		return style;
	}

	/**
	 * 泳池
	 * 
	 * @return
	 */
	private static ImmutableStyle ModelFigure() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		return style;
	}

	/**
	 * 0级
	 * 
	 * @return
	 */
	private static ImmutableStyle FlowLevelFigureOne() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		style.noTextStyle = true;
		return style;
	}

	/**
	 * 1级
	 * 
	 * @return
	 */
	private static ImmutableStyle FlowLevelFigureTwo() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		style.noTextStyle = true;
		return style;
	}

	/**
	 * 2级
	 * 
	 * @return
	 */
	private static ImmutableStyle FlowLevelFigureThree() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		style.noTextStyle = true;
		return style;
	}

	/**
	 * 3级
	 * 
	 * @return
	 */
	private static ImmutableStyle FlowLevelFigureFour() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("graph3dStyle", NONE);
		style.baseGraphStyleValues.put("shadow", NONE);
		style.noTextStyle = true;
		return style;
	}

	/**
	 * 接口
	 * 
	 * @return
	 */
	private static ImmutableStyle ImplFigure() {
		ImmutableStyle style = new ImmutableStyle();
		style.baseGraphStyleValues.put("transform", NONE);
		return style;
	}

}
