package epros.draw.gui.workflow.aid;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnLineTextPanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.shortcut.JecnShortCutKeyProcess;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 流程元素编辑框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTextArea extends JTextArea {

	/** 画图面板 */
	private JecnDrawDesktopPane workflow = null;
	/** 双击流程元素图形进入编辑使用的文本框 滚动面板 */
	private JScrollPane editScrollPane = null;

	/** 编辑流程元素时所双击的流程元素 */
	private JecnBaseFlowElementPanel flowElementPanel = null;
	/** 编辑输入框文档对象 */
	private JecnPlainDocument plainDocument = null;
	/** 编辑前的输入库内容 */
	private String beforeEidtText = null;

	public JecnTextArea(JecnDrawDesktopPane workflow) {
		if (workflow == null) {
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.workflow = workflow;
		initComponent();

	}

	private void initComponent() {
		// editTextArea的容器
		editScrollPane = new JScrollPane(this);
		// 编辑输入框文档对象
		plainDocument = new JecnPlainDocument(this);

		// editTextArea的容器:横滚动条不让显示；竖滚动条需要时显示
		editScrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		editScrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		editScrollPane.setBorder(null);
		editScrollPane.setVisible(false);

		// 添加自定义文档对象
		this.setDocument(plainDocument);

		// 设置默认的背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		this.setBorder(BorderFactory.createLineBorder(JecnUIUtil
				.getEditTextAreaBorderColor()));

		this.workflow.add(editScrollPane);
		// 编辑框改变大小，editScrollPane滚动面板改变大小
		this.addComponentListener(new JecnTextAreaComponentAdapter());
		this.addFocusListener(new JecnFocusListener());

		// 注册快捷键
		JecnShortCutKeyProcess.getShortCutKeyProcess()
				.initJecnTextAreaKeyAction(this);

	}

	/**
	 * 
	 * 绘制组件
	 * 
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(1));
	}

	/**
	 * 
	 * 判断内容是否合法
	 * 
	 * @return boolean 合法返回true；不合法返回false
	 */
	public boolean isEditFail() {
		if (this.isShowing()) {
			// 判断输入框内容是否正确，不正确是弹出菜单提示
			boolean check = plainDocument.showPopupMenu(this.getText(), true);
			if (!check) {// 验证失败
				return false;
			}
		}
		return true;

	}

	/**
	 * 
	 * 添加编辑输入框所关联的流程元素
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel 所关联的流程元素
	 */
	void addFlowElementPanel(JecnBaseFlowElementPanel flowElementPanel) {
		if (flowElementPanel == null) {
			return;
		}
		if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {
			JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
			addLineSegmentPanel(linePanel);
			return;
		}

		// **************成对**************//
		this.flowElementPanel = flowElementPanel;
		this.plainDocument.setFlowElementPanel(flowElementPanel);
		// 记录编辑前的输入框内容
		this.beforeEidtText = flowElementPanel.getFlowElementData()
				.getFigureText();
		// 设置编辑框换行策略
		setFlowElemEditWap(flowElementPanel);
		// **************成对**************//

		// 编辑文本框显示
		editScrollPane.setVisible(true);

		// 图形隐藏
		flowElementPanel.setVisible(false);
		// 设置文本域 字体
		this.setFont(flowElementPanel.getFlowElementData().getFont());
		// 设置位置点
		editScrollPane.setLocation(flowElementPanel.getX(), flowElementPanel
				.getY());
		// 如果为泳池
		if (flowElementPanel instanceof ModelFigure) {
			ModelFigure modelFigure = (ModelFigure) flowElementPanel;
			// 设置文本域大小
			this.setSize(modelFigure.getZoomDividingX(), flowElementPanel
					.getHeight());
			// 大小
			editScrollPane.setSize(modelFigure.getZoomDividingX(),
					flowElementPanel.getHeight());
			// 图形显示
			flowElementPanel.setVisible(true);
		} else {
			// 设置文本域大小
			this.setSize(flowElementPanel.getWidth(), flowElementPanel
					.getHeight());
			// 大小
			editScrollPane.setSize(flowElementPanel.getWidth(),
					flowElementPanel.getHeight());
		}
		// 设置层级
		workflow.setLayer(editScrollPane, workflow.getMaxZOrderIndex());
		// 给文本域赋值
		this.setText((flowElementPanel.getFlowElementData().getFigureText()));
		// 获取输入焦点,显示选中状态
		this.requestFocusInWindow();
		// 选中全部
		this.selectAll();
	}

	/**
	 * 
	 * 添加编辑输入框所关联的流程元素
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel 所关联的流程元素
	 */
	void addLineSegmentPanel(JecnBaseFlowElementPanel flowElementPanel) {
		if (flowElementPanel == null) {
			return;
		}
		JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
		JecnLineTextPanel textPanel = linePanel.getTextPanel();
		if (textPanel == null) {
			return;
		}

		this.plainDocument.setFlowElementPanel(flowElementPanel);
		this.flowElementPanel = flowElementPanel;
		// 记录编辑前的输入框内容
		this.beforeEidtText = textPanel.getLineData().getFigureText();

		// 编辑文本框显示
		editScrollPane.setVisible(true);

		// 图形隐藏
		textPanel.setVisible(false);
		// 设置文本域 字体
		this.setFont(textPanel.getLineData().getFont());
		// 设置位置点
		editScrollPane.setLocation(textPanel.getX(), textPanel.getY());
		// 如果为泳池

		// 设置文本域大小
		this.setSize(textPanel.getWidth(), textPanel.getHeight());
		// 大小
		editScrollPane.setSize(textPanel.getWidth(), textPanel.getHeight());
		// 设置层级
		workflow.setLayer(editScrollPane, workflow.getMaxZOrderIndex());
		// 给文本域赋值
		this.setText((textPanel.getLineData().getFigureText()));
		// 获取输入焦点,显示选中状态
		this.requestFocusInWindow();
		// 选中全部
		this.selectAll();
	}

	/**
	 * 
	 * 移除编辑输入框所关联的流程元素
	 * 
	 * @return boolean true:移除成功；false:移除不成功
	 */
	boolean removeFlowElemPanel() {
		if (flowElementPanel == null) {
			// 值空
			clearData();
			return true;
		}

		// 判断输入框内容是否正确，不正确是弹出菜单提示
		boolean check = isEditFail();
		if (!check) {// 验证失败
			return false;
		}

		// 是否修改内容，必须内容不一样返回true，否则返回false
		boolean isUpdate = isUpdateText();

		// 操作前数据
		JecnUndoRedoData undoData = null;
		// 操作后数据
		JecnUndoRedoData redoData = null;
		if (isUpdate) {// 编辑前后内容不相同
			// 操作前数据
			undoData = new JecnUndoRedoData();
			// 操作后数据
			redoData = new JecnUndoRedoData();

			// 操作前
			undoData.recodeFlowElement(flowElementPanel);
		}

		// 隐藏
		editScrollPane.setVisible(false);
		// 图形显示
		flowElementPanel.setVisible(true);
		// 内容
		flowElementPanel.getFlowElementData().setFigureText(this.getText());

		// 线处理
		if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 如果是连接线
			JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
			linePanel.removeLineByEditTextArea();
		}

		// *************************撤销恢复*************************//
		if (isUpdate) {// 编辑前后内容不相同
			// 操作后
			redoData.recodeFlowElement(flowElementPanel);
			// 记录这次操作的数据集合
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory
					.createEdit(undoData, redoData);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getUndoRedoManager()
					.addEdit(unredoProcess);
		}

		// *************************撤销恢复*************************//

		// 值空
		clearData();

		return true;
	}

	/**
	 * 
	 * 设置自动换行
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel 流程元素
	 */
	private void setFlowElemEditWap(JecnBaseFlowElementPanel flowElementPanel) {
		if (flowElementPanel == null) {
			return;
		}
		if (flowElementPanel instanceof JecnBaseFigurePanel) {
			// 自动换行
			this.setLineWrap(true);
			this.setWrapStyleWord(true);
		} else {
			// 自动换行
			this.setLineWrap(false);
			this.setWrapStyleWord(false);
		}
	}

	/**
	 * 
	 * 是否修改内容，必须内容不一样返回true，否则返回false
	 * 
	 * @return boolean 编辑前后内容相同返回false；反之返回true
	 */
	private boolean isUpdateText() {
		return (DrawCommon.checkStringSame(this.beforeEidtText, this.getText())) ? false
				: true;
	}

	/**
	 * 
	 * 清除数据
	 * 
	 */
	private void clearData() {
		// 值空
		flowElementPanel = null;
		this.plainDocument.setFlowElementPanel(null);
		this.setText("");
		beforeEidtText = null;
	}

	public JScrollPane getEditScrollPane() {
		return editScrollPane;
	}

	public JecnBaseFlowElementPanel getFlowElementPanel() {
		return flowElementPanel;
	}

}
