package epros.designer.gui.system.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.system.JecnTablePageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public abstract class JecnTablePaging {
	/** 分页面板 */
	private JPanel pagePanel = new JPanel();
	public JLabel nowPage = new JLabel(" ");
	/** 上一页 */
	private JLabel previousPage = new JLabel(new ImageIcon("images/icons/previouspage.gif"));
	/** 下一页 */
	private JLabel nextPage = new JLabel(new ImageIcon("images/icons/nextpage.gif"));
	/** 首页 */
	private JLabel homePage = new JLabel(JecnProperties.getValue("homePage"));
	/** 尾页 */
	private JLabel endPage = new JLabel(JecnProperties.getValue("endPage"));

	/** 总页数 */
	private JLabel totalPages = new JLabel();
	/** 结果滚动面板 */
	private JScrollPane resultScrollPanel = null;
	/** 结果面板 */
	private JPanel resultPanel = null;

	private SearchTable jecnTable;

	private int pageSize = 20;

	public SearchTable getJecnTable() {
		return jecnTable;
	}

	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	public JecnTablePaging(int row, JecnPanel infoPanel, Insets insets, JecnDialog jecnDialog) {
		initCompotents();
		GridBagConstraints c = new GridBagConstraints(0, row, 4, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(resultPanel, c);
		initLayout();
	}

	public void initCompotents() {
		// 结果面板
		resultPanel = new JPanel();
		// table 滚动面板
		resultScrollPanel = new JScrollPane();
		// 表
		jecnTable = new SearchTable();
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		pagePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		homePage.setForeground(Color.BLUE);
		endPage.setForeground(Color.BLUE);

		resultScrollPanel.setViewportView(jecnTable);
	}

	private void initLayout() {
		resultPanel.setLayout(new BorderLayout());
		resultPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), ""));
		pagePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 结果滚动面板
		resultPanel.add(resultScrollPanel, BorderLayout.CENTER);
		resultPanel.add(pagePanel, BorderLayout.SOUTH);
		// ************结果面板布局************//

		pagePanel.add(new JLabel(JecnProperties.getValue("regulation")));// 第

		pagePanel.add(nowPage);

		pagePanel.add(new JLabel(JecnProperties.getValue("page")));// 页
		// 上一页
		pagePanel.add(previousPage);
		// 下一页
		pagePanel.add(nextPage);
		// 首页
		pagePanel.add(homePage);
		// 尾页
		pagePanel.add(endPage);
		JPanel p = new JPanel();
		p.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(120, 25);
		p.setPreferredSize(dimension);
		p.setMinimumSize(dimension);
		pagePanel.add(p);
		// 总页数：
		pagePanel.add(new JLabel(JecnProperties.getValue("totalPages")));
		pagePanel.add(totalPages);
		pagePanel.add(new JLabel("  "));
		previousPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// 如果为初始页或第一天时，点上一而直接返回，不再去请求后台
				if (DrawCommon.isNullOrEmtryTrim(nowPage.getText()) || nowPage.getText().equals("1")) {
					return;
				}
				refreshTable("previous");
			}

		});
		// 下一页
		nextPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				refreshTable("next");
			}

		});
		// 首页
		homePage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				refreshTable("home");
			}

		});
		// 尾页
		endPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				refreshTable("end");
			}

		});
	}

	/**
	 * 获得选中行
	 * 
	 * @return
	 */
	public Set<Long> getSelectIds() {
		int[] rows = this.jecnTable.getSelectedRows();
		Set<Long> selectIds = new HashSet<Long>();
		for (int row : rows) {
			if (jecnTable.getModel().getValueAt(row, 0) != null
					&& !"".equals(jecnTable.getModel().getValueAt(row, 0).toString())) {
				selectIds.add(Long.valueOf(jecnTable.getModel().getValueAt(row, 0).toString()));
			}
		}
		return selectIds;
	}

	public void refreshTable(String operationType) {
		JecnTablePageBean jecnTablePageBean = getJecnTablePageBean(operationType, pageSize);
		if (jecnTablePageBean != null) {
			list = jecnTablePageBean.getListJecnTreeBean();
		} else {
			list = new ArrayList<JecnTreeBean>();
		}
		jecnTable.refluseTable();
		if (list != null && list.size() > 0) {
			nowPage.setText(String.valueOf(jecnTablePageBean.getCurPage()));
			totalPages.setText(String.valueOf(jecnTablePageBean.getTotalPage()));
		} else {
			nowPage.setText(" ");
			totalPages.setText("0");
		}
	}

	public List<JecnTreeBean> getList() {
		return list;
	}

	public abstract JecnTablePageBean getJecnTablePageBean(String operationType, int pageSize);

	/**
	 * @author yxw 2012-5-10
	 * @description:转换成Table的内容
	 * @param list
	 * @return
	 */
	public abstract Vector<Vector<String>> getContent(List<JecnTreeBean> list);

	/**
	 * @author yxw 2012-5-9
	 * @description:获得搜索table的标题
	 * @return
	 */
	public abstract Vector<String> getTableTitle();

	class SearchTable extends JecnTable {

		public SearchTable() {
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {// 双击
						// 编辑处理
						isClickTable();
					}
				}
			});
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent(list));
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		public void refluseTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}

			TableColumnModel columnModel = this.getColumnModel();
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}
			TableColumn tableColumn = columnModel.getColumn(1);
			tableColumn.setMinWidth(150);
			tableColumn.setMaxWidth(150);
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0 };
		}
	}

	public abstract void isClickTable();

	/**
	 * 选中JTable的行
	 * 
	 * @param id
	 * @param jTable
	 */
	public void selectTableRows(Long id) {
		JecnTableModel model = (JecnTableModel) jecnTable.getModel();
		for (int i = 0; i < model.getRowCount(); i++) {
			if ((model.getValueAt(i, 0) != null) && (!"".equals(model.getValueAt(i, 0).toString()))) {
				if (id.toString().equals(model.getValueAt(i, 0).toString())) {
					jecnTable.setRowSelectionInterval(i, i);
					break;
				}
			}
		}
	}
}
