package com.jecn.epros.server.service.popedom.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.action.web.login.ad.anjianzhiyuan.JecnAnJianZhiYuanAfterItem;
import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekSyncUserEnum;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LdapAuthVerifier;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LianHeDianZiAfterItem;
import com.jecn.epros.server.action.web.login.ad.yuexiu.YueXiuLoginAction;
import com.jecn.epros.server.bean.log.JecnPersonalPermRecord;
import com.jecn.epros.server.bean.popedom.JecnRoleContent;
import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.bean.popedom.LoginBean;
import com.jecn.epros.server.bean.popedom.UserAddReturnBean;
import com.jecn.epros.server.bean.popedom.UserEditBean;
import com.jecn.epros.server.bean.popedom.UserInfo;
import com.jecn.epros.server.bean.popedom.UserPageBean;
import com.jecn.epros.server.bean.popedom.UserRole;
import com.jecn.epros.server.bean.popedom.UserSearchBean;
import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.Pager;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.common.JecnEnumUtil.AuthorName;
import com.jecn.epros.server.dao.JecnSqlConstant;
import com.jecn.epros.server.dao.popedom.IJecnRoleDao;
import com.jecn.epros.server.dao.popedom.IOrganizationDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.project.ICurProjectDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.system.IJecnUserLoginLogDao;
import com.jecn.epros.server.dao.system.IUnReadMessageDao;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.util.JecnDesignerPassward;
import com.jecn.epros.server.util.JecnKey;
import com.jecn.epros.server.util.JecnUserSecurityKey;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.popedom.PosBean;
import com.jecn.epros.server.webBean.popedom.UserWebInfoBean;
import com.jecn.epros.server.webBean.popedom.UserWebSearchBean;
import com.jecn.webservice.cms.designerSSO.CMSDesignerServiceClient;

@Transactional(rollbackFor = Exception.class)
public class PersonServiceImpl extends AbsBaseService<JecnUser, Long> implements IPersonService {

	private Logger log = Logger.getLogger(PersonServiceImpl.class);

	private IPersonDao personDao;

	private IJecnRoleDao jecnRoleDao;

	private ICurProjectDao curProjectDao;
	/** 未读消息 */
	private IUnReadMessageDao unReadMesgDao;
	/** 登录日志 */
	private IJecnUserLoginLogDao userLoginLogDao;
	/** 组织 */
	private IOrganizationDao organizationDao;

	private IJecnConfigItemDao configItemDao;

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}

	public IOrganizationDao getOrganizationDao() {
		return organizationDao;
	}

	public void setOrganizationDao(IOrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	public IJecnUserLoginLogDao getUserLoginLogDao() {
		return userLoginLogDao;
	}

	public void setUserLoginLogDao(IJecnUserLoginLogDao userLoginLogDao) {
		this.userLoginLogDao = userLoginLogDao;
	}

	public IUnReadMessageDao getUnReadMesgDao() {
		return unReadMesgDao;
	}

	public void setUnReadMesgDao(IUnReadMessageDao unReadMesgDao) {
		this.unReadMesgDao = unReadMesgDao;
	}

	public IPersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(IPersonDao personDao) {
		this.personDao = personDao;
		this.baseDao = personDao;
	}

	@Override
	public UserAddReturnBean savePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception {
		try {
			UserAddReturnBean returnBean = editPersonRoleValidate(roleIds, jecnUser, true);
			if (returnBean.getType() != 0) {
				return returnBean;
			}
			// 默认是解锁
			jecnUser.setIsLock(Long.valueOf(0));
			Long peopleId = personDao.save(jecnUser);
			returnBean.setUserId(peopleId);
			// 添加岗位与人员表
			if (figureIds != null && !"".equals(figureIds)) {
				String[] figureIdsArr = figureIds.split(",");
				for (String figureId : figureIdsArr) {
					if (figureId == null || "".equals(figureId)) {
						continue;
					}
					Long posId = Long.valueOf(figureId);
					JecnUserInfo jecnUserInfo = new JecnUserInfo();
					jecnUserInfo.setFigureId(posId);
					jecnUserInfo.setPeopleId(peopleId);
					personDao.getSession().save(jecnUserInfo);
				}
			}
			// 添加角色与人员表
			if (roleIds != null && !"".equals(roleIds)) {
				String[] roleIdsArr = roleIds.split(",");
				for (String roleId : roleIdsArr) {
					if (roleId == null || "".equals(roleId)) {
						continue;
					}
					Long roleAuthId = Long.valueOf(roleId);
					UserRole userRole = new UserRole();
					userRole.setRoleId(roleAuthId);
					userRole.setPeopleId(peopleId);
					personDao.getSession().save(userRole);
				}
			}

			return returnBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private UserAddReturnBean editPersonRoleValidate(String roleIds, JecnUser jecnUser, boolean isAdd) throws Exception {
		UserAddReturnBean userAddReturnBean = new UserAddReturnBean();
		userAddReturnBean.setPassword(JecnDesignerPassward.getDesignerEncrypt(jecnUser.getPassword()));
		if (StringUtils.isEmpty(roleIds)) {
			return userAddReturnBean;
		}

		// true:角色变更
		boolean isEditRole = false;
		if (!isAdd) {// 编辑
			// 判断是否存在设计权限，存在则不验证用户权限
			List<JecnRoleInfo> list = jecnRoleDao.getDefaultRoleByPeopleId(jecnUser.getPeopleId());
			for (JecnRoleInfo jecnRoleInfo : list) {
				if (AuthorName.secondAdmin.toString().equals(jecnRoleInfo.getFilterId())
						|| AuthorName.admin.toString().equals(jecnRoleInfo.getFilterId())
						|| AuthorName.design.toString().equals(jecnRoleInfo.getFilterId())) {
					String[] roleIdsArr = roleIds.split(",");
					for (String roleId : roleIdsArr) {
						if (roleId.equals(jecnRoleInfo.getRoleId().toString())) {
							// 存在设计权限返回
							return userAddReturnBean;
						}
					}
					isEditRole = true;
				}
			}

			if (StringUtils.isNotBlank(roleIds) && list.size() == 0) {
				isEditRole = true;
			}
		}

		Map<Long, AuthorName> resultMap = jecnRoleDao.getAdminAndDesignId();
		// 默认角色总数
		int curUserInt = 0;
		// 二级管理员总数
		int secondAdminInt = 0;

		// 设计者
		int designerUserCount = 0;
		// 设计者和管理员
		int adminCount = 0;

		if (JecnContants.isAdminKey) {
			designerUserCount = jecnRoleDao.getDesignerUserTotal("('" + JecnCommonSql.getStrDesignAdmin() + "')");
			// 管理员总数
			adminCount = jecnRoleDao.getDesignerUserTotal(JecnCommonSql.getAdminAuthString());
		} else {
			// 默认角色总数
			curUserInt = personDao.getCurUserInt();
			// 二级管理员总数
			secondAdminInt = jecnRoleDao.getDesignerUserTotal("('" + JecnCommonSql.getStrSecondAdmin() + "')");
		}

		// true: 可以登录设计器的人（包含二级管理员）
		boolean isExistDesignUser = false;
		// 二级管理员 单独在验证
		boolean isExistProcessAdminUser = false;
		boolean isExistAdminUser = false;
		// 添加角色与人员表
		if (roleIds != null && !"".equals(roleIds)) {
			String[] roleIdsArr = roleIds.split(",");
			for (String roleId : roleIdsArr) {
				if (roleId == null || "".equals(roleId)) {
					continue;
				}
				Long roleAuthId = Long.valueOf(roleId);
				if (resultMap.containsKey(roleAuthId)) {
					if (resultMap.get(roleAuthId) == AuthorName.secondAdmin) {
						isExistProcessAdminUser = true;
					} else if (resultMap.get(roleAuthId) == AuthorName.admin) {
						isExistAdminUser = true;
					} else {
						isExistDesignUser = true;
					}
				}
			}
		}
		int NUM = 1;
		if (!isAdd && !isEditRole) {
			NUM = 0;
		}
		if (JecnContants.isAdminKey) {
			if (isExistDesignUser) {
				// 当前用户数
				if (JecnContants.userInt < designerUserCount + NUM) {
					userAddReturnBean.setType(1);
					return userAddReturnBean;
				}
			} else if (isExistProcessAdminUser || isExistAdminUser) {// 添加中为二级管理员角色
				if (JecnContants.adminInt < adminCount + NUM) {
					userAddReturnBean.setType(3);
					return userAddReturnBean;
				}
			}
		} else {// 新版本秘钥处理
			if (isExistDesignUser || isExistAdminUser) {
				// 当前用户数
				if (JecnContants.userInt < curUserInt + NUM) {
					userAddReturnBean.setType(1);
					return userAddReturnBean;
				}
			} else if (isExistProcessAdminUser) {// 添加中为二级管理员角色
				if (JecnContants.secondInt < secondAdminInt + NUM) {
					userAddReturnBean.setType(2);
					return userAddReturnBean;
				}
			}
		}

		return userAddReturnBean;
	}

	@Override
	public UserAddReturnBean updatePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception {
		try {
			UserAddReturnBean returnBean = editPersonRoleValidate(roleIds, jecnUser, false);
			if (returnBean.getType() != 0) {
				return returnBean;
			}
			jecnUser.setPassword(JecnUserSecurityKey.getsecret(jecnUser.getPassword()));
			// 更新
			personDao.update(jecnUser);
			// 岗位更新
			if (figureIds == null || "".equals(figureIds)) {
				String hql = "delete from JecnUserInfo where peopleId=?";
				personDao.execteHql(hql, jecnUser.getPeopleId());
			} else {
				String hql = "from JecnUserInfo where peopleId=" + jecnUser.getPeopleId();
				// 老数据 岗位与人员关联数据
				List<JecnUserInfo> listJecnUserInfo = personDao.listHql(hql);
				String[] figureIdsArr = figureIds.split(",");
				// 老数据 不需要删除
				List<Long> listNotDel = new ArrayList<Long>();
				// 新数据 需要保持
				List<JecnUserInfo> listSave = new ArrayList<JecnUserInfo>();

				for (String figureId : figureIdsArr) {
					if (figureId == null || "".equals(figureId)) {
						continue;
					}
					Long posId = Long.valueOf(figureId);
					boolean isExist = false;
					for (JecnUserInfo jecnUserInfo : listJecnUserInfo) {
						if (posId.equals(jecnUserInfo.getFigureId())) {
							isExist = true;
							break;
						}
					}
					if (isExist) {
						listNotDel.add(posId);
					} else {
						JecnUserInfo jecnUserInfo = new JecnUserInfo();
						jecnUserInfo.setFigureId(posId);
						jecnUserInfo.setPeopleId(jecnUser.getPeopleId());
						listSave.add(jecnUserInfo);
					}
				}
				if (listNotDel.size() > 0) {
					hql = "delete from JecnUserInfo where peopleId=? and figureId not in"
							+ JecnCommonSql.getIds(listNotDel);
					personDao.execteHql(hql, jecnUser.getPeopleId());
				} else {
					hql = "delete from JecnUserInfo where peopleId=?";
					personDao.execteHql(hql, jecnUser.getPeopleId());
				}
				for (JecnUserInfo jecnUserInfo : listSave) {
					personDao.getSession().save(jecnUserInfo);
				}
			}
			// 角色更新
			if (roleIds == null || "".equals(roleIds)) {
				String hql = "delete from UserRole where peopleId=?";
				personDao.execteHql(hql, jecnUser.getPeopleId());
			} else {
				String hql = "from UserRole where peopleId=?";
				// 老数据 岗位与人员关联数据
				List<UserRole> listUserRole = personDao.listHql(hql, jecnUser.getPeopleId());
				String[] roleIdsArr = roleIds.split(",");
				// 老数据 不需要删除
				List<Long> listNotDel = new ArrayList<Long>();
				// 新数据 需要保持
				List<UserRole> listSave = new ArrayList<UserRole>();
				for (String roleId : roleIdsArr) {
					if (roleId == null || "".equals(roleId)) {
						continue;
					}
					Long roleAuthId = Long.valueOf(roleId);
					boolean isExist = false;
					for (UserRole userRole : listUserRole) {
						if (roleAuthId.equals(userRole.getRoleId())) {
							isExist = true;
							break;
						}
					}
					if (isExist) {
						listNotDel.add(roleAuthId);
					} else {
						UserRole userRole = new UserRole();
						userRole.setRoleId(roleAuthId);
						userRole.setPeopleId(jecnUser.getPeopleId());
						listSave.add(userRole);
					}
				}
				if (listNotDel.size() > 0) {
					hql = "delete from UserRole where peopleId=? and roleId not in" + JecnCommonSql.getIds(listNotDel);
					personDao.execteHql(hql, jecnUser.getPeopleId());
				} else {
					hql = "delete from UserRole where peopleId=?";
					personDao.execteHql(hql, jecnUser.getPeopleId());
				}
				for (UserRole userRole : listSave) {
					personDao.getSession().save(userRole);
				}
			}
			returnBean.setUserId(jecnUser.getPeopleId());
			return returnBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * @author zhangchen May 23, 2012
	 * @description：人员查询sql通用的部分
	 * @param sql
	 * @param listOrgIds
	 * @param roleId
	 * @param loginName
	 * @param trueName
	 * @return
	 */
	private String getSqlCommon(String sql, Long orgId, String roleType, String loginName, String trueName,
			Set<Long> setOrgIds, Long isDelete) {
		if ((loginName != null && !"".equals(loginName)) || (trueName != null && !"".equals(trueName)) || orgId != null
				|| (roleType != null && !"".equals(roleType))) {

			if (roleType != null && !"".equals(roleType)) {
				sql = sql + ",jecn_user_role jur,jecn_role_info jri";
			}
			if (orgId != null) {
				sql = sql + ",jecn_user_position_related jupr,jecn_flow_org_image jfoi,jecn_flow_org jfo";
			}
			sql = sql + " where ju.ISLOCK=" + isDelete + " ";
			if (loginName != null && !"".equals(loginName)) {
				sql = sql + " and ju.login_name like '%" + loginName + "%'";
			}
			if (trueName != null && !"".equals(trueName)) {
				sql = sql + " and ju.true_name like '%" + trueName + "%'";
			}
			if (roleType != null && !"".equals(roleType)) {
				sql = sql + " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
						+ roleType + "'";
			}
			if (orgId != null) {
				sql = sql
						+ " and ju.people_id=jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and jfo.org_id in";
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {

					sql = sql + " (select org_id from MY_ORG)";
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					sql = sql
							+ "( select t.org_id from jecn_flow_org t CONNECT BY PRIOR t.org_id = t.per_org_id START WITH t.org_id ="
							+ orgId + " )";
				} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
					if (setOrgIds.size() > 0) {
						sql = sql + JecnCommonSql.getIdsSet(setOrgIds);
					}
				}
			}
			sql = sql + " )";
		}
		return sql;
	}

	@Override
	public UserPageBean searchUser(Long orgId, String roleType, String loginName, String trueName, int curPage,
			String operationType, int pageSize, Long projectId, Long isDelete, boolean isLogin) throws Exception {

		try {
			String sql = "";
			String sqlSQLSERVER = "";
			sql = " select distinct ju.people_id,ju.LOGIN_NAME from jecn_user ju";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				if (orgId != -1) {
					sqlSQLSERVER = "WITH MY_ORG AS (SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID =" + orgId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}
			// 角色类型
			if (StringUtils.isNotBlank(roleType)) {
				sql = sql + ",jecn_user_role jur,jecn_role_info jri";
			}
			// 部门
			if (orgId != -1 && !JecnCommon.isMySql()) {
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					sql = sql + ",MY_ORG org";
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					sql = sql
							+ ",(select jfo.* from jecn_flow_org jfo CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ orgId + ") org";
				}
				sql = sql + " ,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
			}

			// 登陆名称
			sql = sql + " where ju.ISLOCK=" + isDelete;
			if (isLogin) { // 判断是否登录
				sql += "and ju.MAC_ADDRESS is not null";
			}
			if (StringUtils.isNotBlank(loginName)) {
				sql = sql + " and upper(ju.login_name) like upper('%" + loginName + "%')";
			}
			// 真实名称
			if (StringUtils.isNotBlank(trueName)) {
				if (JecnConfigTool.isIflytekLogin()) {
					List<String> names = new ArrayList<String>();
					names.add(trueName);
					Map<String, String> userMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUser(names);
					if (userMap != null && !userMap.isEmpty()) {
						names = new ArrayList<String>();
						for (Map.Entry<String, String> bean : userMap.entrySet()) {
							names.add(bean.getKey());
						}
						sql = sql + " and ju.login_name in" + JecnCommonSql.getStrs(names);
					}
				} else {
					sql = sql + " and upper(ju.true_name) like upper('%" + trueName + "%')";
				}
			}
			// 角色类型
			if (StringUtils.isNotBlank(roleType)) {
				sql = sql + " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
						+ roleType + "'";
			}
			// 部门
			if (orgId != -1 && !JecnCommon.isMySql()) {
				sql = sql
						+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
			}

			int totalCount = 0;
			if (orgId != -1 && JecnCommon.isMySql()) {
				List<Object[]> totalList = personDao.listNativeSql(sql);
				List<Object[]> listTotal = getUserBeanObject(orgId, projectId);
				List<Object[]> listTotalUserObj = new ArrayList<Object[]>();
				if (totalList.size() > 0) {
					for (Object[] userBean : totalList) {
						for (Object[] obj : listTotal) {
							if (Long.valueOf(userBean[0].toString()).equals(Long.valueOf(obj[0].toString()))) {
								listTotalUserObj.add(userBean);
							}
						}
					}
				}
				totalCount = listTotalUserObj.size();
			} else {
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					if (!"".equals(sqlSQLSERVER)) {
						sql = sqlSQLSERVER + "select count(1) from (" + sql + ") temp";
					} else {
						sql = "select count(1) from (" + sql + ") temp";
					}
				} else {
					sql = "select count(1) from (" + sql + ") temp";
				}
				// 计算查询结果数量
				totalCount = personDao.countAllByParamsNativeSql(sql);// personDao.countAllByParamsNativeSql(sql);
			}
			Pager pager = JecnCommon.getPager(curPage, pageSize, operationType, totalCount);
			int startRow = pager.getStartRow();
			int totalPage = pager.getTotalPages();
			UserPageBean userPageBean = new UserPageBean();
			userPageBean.setCurPage(pager.getCurrentPage());
			userPageBean.setTotalCount(totalCount);
			userPageBean.setTotalPage(totalPage);
			if (totalCount < 1) {
				return userPageBean;
			}
			List<Object[]> listResult = null;
			String sqlContent = "";
			if (JecnCommon.isSQLServer()) {
				String withSql = "";
				sqlContent = " select distinct ju.people_id,ju.login_name,ju.true_name ,ju.email,ju.mac_address from jecn_user ju";

				if (orgId != -1) {
					withSql = "WITH MY_ORG AS (SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID =" + orgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}

				// 角色类型
				if (StringUtils.isNotBlank(roleType)) {
					sqlContent = sqlContent + ",jecn_user_role jur,jecn_role_info jri";
				}
				// 部门
				if (orgId != -1) {
					sqlContent = sqlContent + ",MY_ORG org";
					sqlContent = sqlContent + " ,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
				}

				// 登陆名称
				sqlContent = sqlContent + " where ju.ISLOCK=" + isDelete;
				if (isLogin) { // 判断是否登录
					sqlContent += "and ju.MAC_ADDRESS is not null";
				}
				if (StringUtils.isNotBlank(loginName)) {
					sqlContent = sqlContent + " and upper(ju.login_name) like upper('%" + loginName + "%')";
				}
				// 真实名称
				if (StringUtils.isNotBlank(trueName)) {
					sqlContent = sqlContent + " and ju.true_name like '%" + trueName + "%'";
				}
				// 角色类型
				if (StringUtils.isNotBlank(roleType)) {
					sqlContent = sqlContent
							+ " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
							+ roleType + "'";
				}
				// 部门
				if (orgId != -1) {
					sqlContent = sqlContent
							+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";

				}
				if (orgId != -1) {
					listResult = personDao.listSQLServerWith(withSql, sqlContent, "people_id", startRow, pageSize);
				} else {
					listResult = this.personDao.listSQLServerSelect(sqlContent, "people_id", startRow, pageSize);
				}

			} else if (JecnCommon.isOracle()) {

				sqlContent = " select distinct ju.people_id,ju.login_name,ju.true_name ,ju.email,ju.mac_address from jecn_user ju";

				// 角色类型
				if (StringUtils.isNotBlank(roleType)) {
					sqlContent = sqlContent + ",jecn_user_role jur,jecn_role_info jri";
				}
				// 部门
				if (orgId != -1) {

					sqlContent = sqlContent
							+ ",(select jfo.* from jecn_flow_org jfo CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ orgId + ") org";
					sqlContent = sqlContent + " ,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
				}

				// 登陆名称
				sqlContent = sqlContent + " where ju.ISLOCK=" + isDelete;
				if (isLogin) { // 判断是否登录
					sqlContent += "and ju.MAC_ADDRESS is not null";
				}
				if (StringUtils.isNotBlank(loginName)) {
					sqlContent = sqlContent + " and upper(ju.login_name) like upper('%" + loginName + "%')";
				}
				// 真实名称
				if (StringUtils.isNotBlank(trueName)) {
					if (JecnConfigTool.isIflytekLogin()) {
						List<String> names = new ArrayList<String>();
						names.add(trueName);
						Map<String, String> userMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUser(names);
						if (userMap != null && !userMap.isEmpty()) {
							names = new ArrayList<String>();
							for (Map.Entry<String, String> bean : userMap.entrySet()) {
								names.add(bean.getKey());
							}
							sqlContent = sqlContent + " and ju.login_name in" + JecnCommonSql.getStrs(names);
						}
					} else {
						sqlContent = sqlContent + " and upper(ju.true_name) like upper('%" + trueName + "%')";
					}
				}
				// 角色类型
				if (StringUtils.isNotBlank(roleType)) {
					sqlContent = sqlContent
							+ " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
							+ roleType + "'";
				}
				// 部门
				if (orgId != -1) {
					sqlContent = sqlContent
							+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
				}

				sqlContent = sqlContent + " order by ju.login_name";
				listResult = this.personDao.listNativeSql(sqlContent, startRow, pageSize);

			} else if (JecnCommon.isMySql()) {
				sqlContent = " select distinct ju.people_id,ju.login_name,ju.true_name ,ju.email,ju.mac_address from jecn_user ju";
				// 角色类型
				if (StringUtils.isNotBlank(roleType)) {
					sqlContent = sqlContent + ",jecn_user_role jur,jecn_role_info jri";
				}
				// 登陆名称
				sqlContent = sqlContent + " where ju.ISLOCK=" + isDelete;
				if (StringUtils.isNotBlank(loginName)) {
					sqlContent = sqlContent + " and ju.login_name like '%" + loginName + "%'";
				}
				// 真实名称
				if (StringUtils.isNotBlank(trueName)) {
					sqlContent = sqlContent + " and ju.true_name like '%" + trueName + "%'";
				}
				// 角色类型
				if (StringUtils.isNotBlank(roleType)) {
					sqlContent = sqlContent
							+ " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
							+ roleType + "'";
				}
				sqlContent = sqlContent + " order by people_id limit " + startRow + "," + pageSize;
				listResult = this.personDao.listNativeSql(sqlContent);
			}
			if (listResult == null) {
				return userPageBean;
			}

			List<UserSearchBean> listUserSearchBean = new ArrayList<UserSearchBean>();

			List<String> list = new ArrayList<String>();
			for (Object[] obj : listResult) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				UserSearchBean userSearchBean = new UserSearchBean();
				userSearchBean.setPeopleId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					userSearchBean.setLoginName(obj[1].toString());
				} else {
					userSearchBean.setLoginName("");
				}
				if (obj[2] != null) {
					userSearchBean.setTrueName(obj[2].toString());
				} else {
					userSearchBean.setTrueName("");
				}
				if (obj[3] != null) {
					userSearchBean.setEmail(obj[3].toString());
				} else {
					userSearchBean.setEmail("");
				}
				if (obj[4] != null) {
					userSearchBean.setLogin(true);
				} else {
					userSearchBean.setLogin(false);
				}

				list.add(userSearchBean.getLoginName());
				listUserSearchBean.add(userSearchBean);
			}

			if (JecnConfigTool.isIflytekLogin()) {
				getListUserSearchBean(listUserSearchBean, list);
			}

			// 部门条件
			if (orgId != -1 && JecnCommon.isMySql()) {
				listResult = getUserBeanObject(orgId, projectId);
				List<UserSearchBean> listUserObj = new ArrayList<UserSearchBean>();
				for (UserSearchBean userBean : listUserSearchBean) {
					for (Object[] obj : listResult) {
						if (userBean.getLoginName().equals(obj[1].toString())) {
							listUserObj.add(userBean);
						}
					}
				}
				listUserSearchBean = listUserObj;
			}
			userPageBean.setListUserSearchBean(listUserSearchBean);
			return userPageBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 讯飞 - 根据登录名获取姓名
	 * 
	 * @param listUserSearchBean
	 * @param list
	 */
	private void getListUserSearchBean(List<UserSearchBean> listUserSearchBean, List<String> list) {
		Map<String, String> maps = JecnIflytekSyncUserEnum.INSTANCE.getMapUserName(list);
		for (UserSearchBean bean : listUserSearchBean) {
			String trueName = maps.get(bean.getLoginName());
			bean.setTrueName(StringUtils.isBlank(trueName) ? bean.getLoginName() : trueName);
		}
	}

	private List<Object[]> getUserBeanObject(Long orgId, Long projectId) {
		// 用于搜索岗位ID的部门ID集合
		List<Long> orgIdList = new ArrayList<Long>();
		orgIdList.add(orgId);
		// 部门所有ID
		List<Object[]> orgObjList = new ArrayList<Object[]>();
		try {
			orgObjList = organizationDao.getAllOrgByProjectId(projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 获取所有部门ID集合
		List<Long> orgAllIds = new ArrayList<Long>();
		for (Object[] objOrgId : orgObjList) {
			orgAllIds.add(Long.valueOf(objOrgId[0].toString()));
		}
		// 是否存在父节点
		boolean isFlag = false;
		// 循环中符合条件的Id集合
		List<Long> containtsIds = new ArrayList<Long>();
		for (Object[] objOrg : orgObjList) {
			isFlag = false;
			// 父节Id与当前组织Id一样
			if (Long.valueOf(objOrg[2].toString()).equals(orgId)) {
				orgIdList.add(Long.valueOf(objOrg[0].toString()));
				// 移除组织ID集合中对应的组织ID
				orgAllIds.remove(Long.valueOf(objOrg[0].toString()));

				// 循环剩下的组织集合
				for (Object[] orgPer : orgObjList) {
					// 若不存在则将当前objOrg中的Id赋值给orgId
					if (orgAllIds.contains(Long.valueOf(orgPer[2].toString()))
							&& orgId.equals(Long.valueOf(orgPer[2].toString()))
							&& !containtsIds.contains(Long.valueOf(objOrg[0].toString()))
							&& !Long.valueOf(objOrg[0].toString()).equals(Long.valueOf(orgPer[0].toString()))
							&& !orgIdList.contains(Long.valueOf(orgPer[0].toString()))) {
						isFlag = true;
					}
				}
				containtsIds.add(Long.valueOf(objOrg[0].toString()));
				if (isFlag) {
					continue;
				} else {
					// 将当前节点赋值给orgId
					orgId = Long.valueOf(objOrg[0].toString());
				}
			}
		}

		// 根据部门ID查询部门对应岗位下的人员
		String sql = "select ju.PEOPLE_ID,ju.LOGIN_NAME,ju.TRUE_NAME"
				+ "  from jecn_user_position_related jr, jecn_user ju" + " where jr.figure_id in (select t.figure_id"
				+ "                          from jecn_flow_org_image t, jecn_flow_org jfo"
				+ "                         where t.org_id = jfo.org_id"
				+ "                           and jfo.del_state = 0"
				+ "                           and t.org_id in (1, 2, 11)"
				+ "                           and jfo.projectid = " + projectId
				+ "                           and t.figure_type = 'PositionFigure'"
				+ "                         order by t.org_id, t.sort_id)"
				+ "   and ju.people_id = jr.people_id and ju.islock=0 ";
		List<Object[]> listResult = this.personDao.listNativeSql(sql);

		return listResult;
	}

	@Override
	public UserEditBean getUserEditBeanByPeopleId(Long peopleId) throws Exception {
		try {
			JecnUser jecnUser = personDao.get(peopleId);
			if (jecnUser == null) {
				return null;
			}
			if (jecnUser.getManagerId() != null) {
				JecnUser managerUser = personDao.get(jecnUser.getManagerId());
				jecnUser.setManagerName(managerUser.getTrueName());
			}
			UserEditBean userEidtBean = new UserEditBean();
			userEidtBean.setJecnUser(jecnUser);
			String sql = "select distinct jupr.figure_id,jfoi.figure_text,jfo.org_name"
					+ "       from jecn_user_position_related jupr,jecn_flow_org_image jfoi,jecn_flow_org jfo"
					+ "       where jupr.people_id=? and jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id"
					+ "       and jfo.del_state=0";
			List<Object[]> list = personDao.listNativeSql(sql, peopleId);
			StringBuffer strPNames = new StringBuffer();
			List<JecnTreeBean> posList = new ArrayList<JecnTreeBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
					continue;
				}
				JecnTreeBean b = new JecnTreeBean();
				b.setId(Long.valueOf(obj[0].toString()));
				b.setName(obj[1].toString());
				b.setPname(obj[2].toString());
				posList.add(b);
				// strIds.append(obj[0].toString() + ",");
				// strNames.append(obj[1].toString() + "/");
				strPNames.append(obj[2].toString() + "/");
			}
			userEidtBean.setPosList(posList);
			userEidtBean.setOrgNames(JecnCommon.getStringByBuffer(strPNames));
			sql = "select distinct jri.role_id,jri.role_name,jri.filter_id from jecn_user_role jur,jecn_role_info jri where jur.people_id=? and jur.role_id=jri.role_id";
			list = personDao.listNativeSql(sql, peopleId);
			List<JecnTreeBean> roleList = new ArrayList<JecnTreeBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[1] == null) {
					continue;
				}
				JecnTreeBean b = new JecnTreeBean();
				b.setId(Long.valueOf(obj[0].toString()));
				b.setName(obj[1].toString());
				if (obj[2] != null) {
					b.setNumberId(obj[2].toString());
				}
				roleList.add(b);
			}
			userEidtBean.setRoleList(roleList);
			return userEidtBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchUserByName(String name, Long projectId) throws Exception {
		String sql = "SELECT DISTINCT JU.PEOPLE_ID," + "                JU.TRUE_NAME,"
				+ "                JFLO.FIGURE_TEXT," + "                JFO.ORG_NAME,"
				+ "                JU.LOGIN_NAME" + "  FROM JECN_USER JU"
				+ "  LEFT JOIN JECN_USER_POSITION_RELATED JUP" + "    ON JU.PEOPLE_ID = JUP.PEOPLE_ID"
				+ "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFLO" + "    ON JUP.FIGURE_ID = JFLO.FIGURE_ID"
				+ "  LEFT JOIN JECN_FLOW_ORG JFO" + "    ON JFLO.ORG_ID = JFO.ORG_ID" + "   AND JFO.DEL_STATE = 0"
				+ "   AND JFO.PROJECTID = ?" + " WHERE JU.ISLOCK=0 AND UPPER(JU.TRUE_NAME) LIKE UPPER(?)";
		List<Object[]> list = personDao.listNativeSql(sql, 0, JecnCommonSql.pageSize, projectId, "%" + name + "%");

		return getSearchResult(list);
	}

	private List<JecnTreeBean> getSearchResult(List<Object[]> list) {
		List<JecnTreeBean> listTreeBean = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			boolean isExist = false;
			for (JecnTreeBean jecnTreeBean : listTreeBean) {
				if (jecnTreeBean.getId().toString().equals(obj[0].toString())) {
					if (StringUtils.isNotBlank(jecnTreeBean.getPname())) {
						if (obj[2] != null)
							jecnTreeBean.setPname(jecnTreeBean.getPname() + "/" + obj[2].toString());
					} else {
						jecnTreeBean.setPname(obj[2].toString());
					}
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				JecnTreeBean b = new JecnTreeBean();
				b.setId(Long.valueOf(obj[0].toString()));
				b.setName(obj[1].toString());
				if (obj[2] != null) {
					b.setPname(obj[2].toString());
				}
				if (obj[3] != null) {
					b.setOrgName(obj[3].toString());
				}
				b.setTreeNodeType(TreeNodeType.person);
				b.setLoginName(obj[4].toString());
				listTreeBean.add(b);
			}
		}
		return listTreeBean;
	}

	/**
	 * 角色权限获取人员选择结果集
	 * 
	 * @param name
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		List<Object[]> list = personDao.searchRoleAuthByName(name, projectId, peopleId);
		return getSearchResult(list);
	}

	@Override
	public void updateLogin(Long peopleId) throws Exception {
		try {
			JecnUser jecnUser = personDao.get(peopleId);
			if (jecnUser != null) {
				jecnUser.setMacAddress(null);
			}
			personDao.update(jecnUser);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int delUsers(List<Long> listPeopleIds) throws Exception {
		try {
			return personDao.deletePeoples(listPeopleIds);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnUser> getUserByLoginName(String loginName) throws Exception {
		try {
			String hql = "from JecnUser where loginName=? ";
			return personDao.listHql(hql, loginName);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnUser> getUserByLoginName(String loginName, Long id) throws Exception {
		try {
			String hql = "from JecnUser where loginName=? and peopleId<>? ";
			return personDao.listHql(hql, loginName, id);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/***************************************************************************
	 * @author zhangchen Oct 7, 2010
	 * @description：人员对象转换成树对象
	 * @param obj
	 * @return
	 */
	private static List<JecnTreeBean> findByListObjects(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 人员节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 人员节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 人员节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			jecnTreeBean.setTreeNodeType(TreeNodeType.person);
			// 岗位节点父类名称
			jecnTreeBean.setPname("");
			jecnTreeBean.setChildNode(false);
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	public List<JecnTreeBean> getChildPoss(Long positionId) throws Exception {
		try {
			String hql = "select ju.peopleId,ju.trueName,jui.figureId from JecnUser as ju,JecnUserInfo as jui where ju.isLock=0 and ju.peopleId = jui.peopleId and jui.figureId=?";
			List<Object[]> list = personDao.listHql(hql, positionId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public LoginBean loginDesign(String loginName, String password, String macAddressClient, int loginType)
			throws Exception {
		try {
			LoginBean loginBean = new LoginBean();
			// 版本类型（0：标准版（默认）; 99:完全版
			loginBean.setVersionType(JecnContants.versionType);
			// 流程元素编辑点:1：左上进右下出（默认）；0：四个编辑点都能进出
			loginBean.setEditPortType(JecnContants.editPortType);
			loginBean.setOtherLoginType(JecnContants.otherLoginType);

			// 用户存不存在
			if (!this.personDao.isUserExist(loginName) && loginType != 2) {
				loginBean.setLoginState(1);
				return loginBean;
			}
			// 加密用户密码 已在前台加密
			// passward = JecnUserSecurityKey.getsecret(passward).trim();
			// 密码正不正确
			JecnUser jecnUser = null;
			if (loginName.equals("admin")) {
				jecnUser = this.personDao.getJecnUserByLoginNameAndPassword(loginName, password);
			} else if (JecnContants.otherLoginType == 14) {// 联合电子
				password = JecnUserSecurityKey.getdesecret(password);
				if (LdapAuthVerifier.newInstance(LianHeDianZiAfterItem.DOMAIN + loginName, password,
						LianHeDianZiAfterItem.LDAP_URL).verifyAuth() == LdapAuthVerifier.SUCCESS) {
					jecnUser = this.personDao.getJecnUserByLoginName(loginName);
				}
			} else if (JecnContants.otherLoginType == 42) {// 招商证券
				password = JecnUserSecurityKey.getdesecret(password);
				String resultInfo = CMSDesignerServiceClient.INSTANCE.RserValidate(loginName, password);
				log.info("CMS用户验证返回：" + resultInfo);
				if (resultInfo.contains("SUCCESS")) {
					jecnUser = this.personDao.getJecnUserByLoginName(loginName);
					loginType = 3;
				}
			} else if (JecnContants.otherLoginType == 12) {// 安健致远
				password = JecnUserSecurityKey.getdesecret(password);
				int result = JecnAnJianZhiYuanAfterItem.doLdapAuthority(loginName, password);
				log.info("安健致远:权限验证结果:" + result);
				// 验证成功
				if (result == 0) {
					jecnUser = this.personDao.getJecnUserByLoginName(loginName);
				}
			} else if (JecnContants.otherLoginType == 52) {
				password = JecnUserSecurityKey.getdesecret(password);
				int result = YueXiuLoginAction.designerLogin(loginName, password);

				if (result == 0) {// 验证成功
					jecnUser = this.personDao.getJecnUserByLoginName(loginName);
					loginType = 3;
				}
			}
			if (loginType == 2) {// 域用户登录
				jecnUser = isLoginByDomainName(loginName);
			} else if (loginType == 3) {// 刷新
				jecnUser = personDao.getJecnUserByLoginName(loginName);
			} else {
				jecnUser = this.personDao.getJecnUserByLoginNameAndPassword(loginName, password);
			}
			if (jecnUser == null) {
				if (loginType == 2) {
					// 域用户登录，提示用户不存在
					loginBean.setLoginState(1);
				} else {
					loginBean.setLoginState(2);
				}
				return loginBean;
			}
			// 用户是不是锁定
			if (jecnUser.getIsLock() == null || jecnUser.getIsLock().intValue() == 1) {
				loginBean.setLoginState(3);
				return loginBean;
			}

			// 判断另一台电脑已登录此用户
			if (JecnUserSecurityKey.isRegistration(macAddressClient, jecnUser.getMacAddress())) {
				loginBean.setLoginState(4);
				return loginBean;
			}

			// /判断是否是默认密码
			String defPwd = JecnUserSecurityKey.getsecret("123456").trim();
			if (defPwd.equals(password)) {
				loginBean.setLoginState(0);
				loginBean.setJecnUser(jecnUser);
				return loginBean;
			} else {
				// 判断有没有登录设计器的权限
				List<JecnRoleInfo> listJecnRoleInfo = this.jecnRoleDao.getDefaultRoleByPeopleId(jecnUser.getPeopleId());
				// 是不是管理员
				boolean isAdmin = false;
				// 是不是设计者
				boolean isDesign = false;
				// true:二级管理员
				boolean isSecondAdmin = false;

				for (JecnRoleInfo jecnRoleInfo : listJecnRoleInfo) {
					if (JecnUtil.isAdmin(jecnRoleInfo.getFilterId())) {
						isAdmin = true;
						break;
					} else if (JecnUtil.isDesign(jecnRoleInfo.getFilterId())) {
						isDesign = true;
					} else if (JecnUtil.isDesignFileAdmin(jecnRoleInfo.getFilterId())) {
						// 是不是设计者管理员
						loginBean.setDesignFileAdmin(true);
					} else if (JecnUtil.isDesignProcessAdmin(jecnRoleInfo.getFilterId())) {
						// 是不是设计者管理员
						loginBean.setDesignProcessAdmin(true);
					} else if (JecnUtil.isDesignRuleAdmin(jecnRoleInfo.getFilterId())) {
						// 是不是设计者管理员
						loginBean.setDesignRuleAdmin(true);
					} else if (JecnUtil.isSecondAdmin(jecnRoleInfo.getFilterId()) && JecnCommon.isShowSecondAdmin()) {// 启用二级管理员，并且当前角色为二级管理员
						isSecondAdmin = true;
						break;
					}
				}
				// 用户没有设计权限
				if (!isAdmin && !isDesign && !isSecondAdmin) {
					loginBean.setLoginState(5);
					return loginBean;
				}

				loginBean.setLoginState(0);
				loginBean.setJecnUser(jecnUser);

				// 登录验证通过添加 人员站位
				String hql = "update JecnUser set macAddress=? where peopleId=?";
				personDao.execteHql(hql, macAddressClient.split(",")[0], jecnUser.getPeopleId());

				// 更新登录日志
				// hql =
				// "insert into JECN_USER_LOGIN_LOG (ID,USER_ID,SESSION_ID,LOGIN_TIME,LOGIN_OUT_TYPE,LOGIN_TYPE) values(?,?,null,?,-1,?)";
				// personDao.execteNative(hql,
				// JecnCommon.getUUID(),loginBean.getJecnUser().getPeopleId(),new
				// Date(),1);
				userLoginLogDao.addUserLoginLog(loginBean.getJecnUser().getPeopleId(), null, 1);
				if (isAdmin) {
					loginBean.setAdmin(true);
				} else {
					if (isSecondAdmin) {
						loginBean.setSecondAdmin(true);
					}
					// 自定义角色权限
					setRoleCustomContent(loginBean, loginBean.getJecnUser().getPeopleId());
				}
				return loginBean;
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 根据域名登录
	 * 
	 * @param userName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JecnUser isLoginByDomainName(String userName) {
		String domainName = JecnContants.DOMAIN_SUFFIX;
		if (StringUtils.isBlank(domainName)) {
			log.error("设计器域名登录异常！ domainName = " + domainName);
			return null;
		}
		log.info("domainName = " + domainName);
		String sql = "SELECT T.* FROM JECN_USER T WHERE LOWER(T.EMAIL) =LOWER ('" + userName + domainName + "')";

		log.info("domainNameSQL = " + sql);
		List<JecnUser> list = this.personDao.getSession().createSQLQuery(sql).addEntity(JecnUser.class).list();
		if (list.size() == 0 || list.get(0) == null) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * 获取自定义角色权限
	 * 
	 * @param loginBean
	 * @param peopleId
	 * @throws Exception
	 */
	private void setRoleCustomContent(LoginBean loginBean, Long peopleId) throws Exception {
		// 获得自定义权限
		List<JecnRoleContent> listJecnRoleContent = this.jecnRoleDao.getJecnRoleContentsByPeopleId(peopleId);
		for (JecnRoleContent jecnRoleContent : listJecnRoleContent) {
			if (jecnRoleContent.getType() == null || jecnRoleContent.getRelateId() == null) {
				continue;
			}
			switch (jecnRoleContent.getType().intValue()) {
			// 涉及角色信息 流程
			case 0:
				if (loginBean.getSetFlow() == null) {
					loginBean.setSetFlow(new HashSet<Long>());
				}
				loginBean.getSetFlow().add(jecnRoleContent.getRelateId());
				break;
			// 涉及角色信息 文件
			case 1:
				if (loginBean.getSetFile() == null) {
					loginBean.setSetFile(new HashSet<Long>());
				}
				loginBean.getSetFile().add(jecnRoleContent.getRelateId());
				break;
			// 涉及角色信息 标准
			case 2:
				if (loginBean.getSetStandiad() == null) {
					loginBean.setSetStandiad(new HashSet<Long>());
				}
				loginBean.getSetStandiad().add(jecnRoleContent.getRelateId());
				break;
			// 涉及角色信息 制度
			case 3:
				if (loginBean.getSetRule() == null) {
					loginBean.setSetRule(new HashSet<Long>());
				}
				loginBean.getSetRule().add(jecnRoleContent.getRelateId());
				break;
			case 4:
				if (loginBean.getSetRisk() == null) {
					loginBean.setSetRisk(new HashSet<Long>());
				}
				loginBean.getSetRisk().add(jecnRoleContent.getRelateId());
				break;
			case 5:
				if (loginBean.getSetOrg() == null) {
					loginBean.setSetOrg(new HashSet<Long>());
				}
				loginBean.getSetOrg().add(jecnRoleContent.getRelateId());
			case 7:
				if (loginBean.getSetPosGroup() == null) {
					loginBean.setSetPosGroup(new HashSet<Long>());
				}
				loginBean.getSetPosGroup().add(jecnRoleContent.getRelateId());
			case 8:
				if (loginBean.getSetTerm() == null) {
					loginBean.setSetTerm(new HashSet<Long>());
				}
				loginBean.getSetTerm().add(jecnRoleContent.getRelateId());
			default:
				break;
			}
		}
	}

	public IJecnRoleDao getJecnRoleDao() {
		return jecnRoleDao;
	}

	public void setJecnRoleDao(IJecnRoleDao jecnRoleDao) {
		this.jecnRoleDao = jecnRoleDao;
	}

	@Override
	public boolean isExistLoginNameAdd(String loginName, Long projectId) throws Exception {
		try {
			String hql = "select count(*) from JecnUser where loginName=? and isLock=0";
			if (this.personDao.countAllByParamsHql(hql, loginName) > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public boolean isExistLoginNameEdit(String loginName, Long projectId, Long peopleId) throws Exception {
		try {
			String hql = "select count(*) from JecnUser where loginName=?  and isLock=0 and peopleId<>?";
			if (this.personDao.countAllByParamsHql(hql, loginName, peopleId) > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPersonByOrg(Long orgId) throws Exception {
		return null;
	}

	@Override
	public void moveEditAllByLoginName(Long userId) throws Exception {
		try {
			// 组织
			String hql = "update JecnFlowOrg set occupier = 0 where occupier=?";
			personDao.execteHql(hql, userId);
			// 流程
			hql = "update JecnFlowStructureT set occupier = 0 where occupier=?";
			personDao.execteHql(hql, userId);
			// 人员
			hql = "update JecnUser set macAddress = null where peopleId=?";
			personDao.execteHql(hql, userId);
			// 更新登录日志
			// hql =
			// "update JECN_USER_LOGIN_LOG set LOGIN_OUT_TIME =?,LOGIN_OUT_TYPE = 0 where user_id = ? and login_out_time is null and login_type = 1";
			// personDao.execteNative(hql, new Date(),userId);
			userLoginLogDao.updateUserLoginLog(String.valueOf(userId), 1, 1);

		} catch (Exception e) {
			log.error(userId + "用户资源释放出错", e);
			throw e;
		}
	}

	@Override
	public void moveEditAll() throws Exception {
		try {
			// 组织
			String hql = "update JecnFlowOrg set occupier = 0 where occupier <>0 ";
			personDao.execteHql(hql);
			// 流程
			hql = "update JecnFlowStructureT set occupier = 0 where occupier <>0";
			personDao.execteHql(hql);
			// 人员
			hql = "update JecnUser set macAddress = null where macAddress is not null";
			personDao.execteHql(hql);
		} catch (Exception e) {
			log.error("资源全部释放出错", e);
			throw e;
		}
	}

	/**
	 * 获得登陆bean
	 * 
	 * @param loginName
	 * @param password
	 * @param loginType
	 *            true为验证密码，false为不验证密码
	 * @return
	 * @throws Exception
	 */
	@Override
	public WebLoginBean getWebLoginBean(String loginName, String password, boolean loginType) throws Exception {
		try {
			JecnUser jecnUser = null;
			if (loginType) {
				// 加密用户密码
				password = JecnUserSecurityKey.getsecret(password).trim();
				jecnUser = personDao.getJecnUserByLoginNameAndPassword(loginName, password);
			} else {
				jecnUser = personDao.getJecnUserByLoginName(loginName);
			}
			WebLoginBean webLoginBean = new WebLoginBean();
			// 登陆状态 0为用户密码不正确，1是登陆成功
			if (jecnUser == null) {
				webLoginBean.setLoginState(0);
				return webLoginBean;
			}
			Long peopleId = jecnUser.getPeopleId();
			webLoginBean.setLoginState(1);
			webLoginBean.setJecnUser(jecnUser);
			// 0:岗位ID，1：岗位名称；2：部门ID；3：部门名称；4：部门Tpath
			List<Object[]> listPos = personDao.getPosOrgInfo(peopleId);
			Set<Long> listPosIds = new HashSet<Long>();
			Set<Long> listOrgIds = new HashSet<Long>();
			Set<Long> listOrgPathPids = new HashSet<Long>();

			// 岗位、组织信息
			List<PosBean> listPosBean = new ArrayList<PosBean>();
			for (Object[] obj : listPos) {
				if (obj == null || obj[0] == null || obj[2] == null || obj[4] == null) {
					continue;
				}
				// 岗位ID
				listPosIds.add(Long.valueOf(obj[0].toString()));
				// 部门ID
				listOrgIds.add(Long.valueOf(obj[2].toString()));

				JecnCommon.getIdsByTpath(listOrgPathPids, obj[4].toString());

				PosBean posBean = new PosBean();
				// 岗位ID
				posBean.setPosId(Long.valueOf(obj[0].toString()));
				// 岗位名称
				if (obj[1] != null) {
					posBean.setPosName(obj[1].toString());
				} else {
					posBean.setPosName("");
				}
				// 部门ID
				posBean.setOrgId(Long.valueOf(obj[2].toString()));
				// 部门名称
				if (obj[3] != null) {
					posBean.setOrgName(obj[3].toString());
				} else {
					posBean.setOrgName("");
				}
				listPosBean.add(posBean);
			}
			webLoginBean.setListOrgIds(listOrgIds);
			webLoginBean.setListPosIds(listPosIds);
			webLoginBean.setListOrgPathPIds(listOrgPathPids);
			webLoginBean.setListPosBean(listPosBean);
			// 项目ID
			JecnCurProject jecnCurProject = curProjectDao.getJecnCurProjectById();
			if (jecnCurProject != null) {
				JecnProject project = (JecnProject) curProjectDao.getSession().get(JecnProject.class,
						jecnCurProject.getCurProjectId());
				webLoginBean.setProjectName(project.getProjectName().replaceAll(" ", "&nbsp;").replaceAll("<", "&lt;")
						.replaceAll(">", "&gt;"));
				if (jecnCurProject != null) {
					webLoginBean.setProjectId(jecnCurProject.getCurProjectId());
				}
			}

			String value = configItemDao.selectValue(6, ConfigItemPartMapMark.projectManagement.toString());
			webLoginBean.setShowProject(JecnConfigContents.ITEM_IS_SHOW.equals(value) ? 1 : 0);
			// 未读消息数量
			int unReadMesgNum = unReadMesgDao.getUnReadMesgNum(webLoginBean.getJecnUser().getPeopleId());
			webLoginBean.setMessageNum(unReadMesgNum);
			String sql = "select distinct jri.role_id,jri.role_name,jri.filter_id from jecn_user_role jur,jecn_role_info jri where jur.people_id=? and jur.role_id=jri.role_id";
			List<Object[]> list = personDao.listNativeSql(sql, peopleId);
			List<JecnTreeBean> roleList = new ArrayList<JecnTreeBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[1] == null) {
					continue;
				}
				JecnTreeBean b = new JecnTreeBean();
				b.setId(Long.valueOf(obj[0].toString()));
				b.setName(obj[1].toString());
				if (obj[2] != null) {
					b.setNumberId(obj[2].toString());
				}
				roleList.add(b);
			}
			webLoginBean.setRoleList(roleList);

			// 是不是流程管理员
			webLoginBean.setAdmin(this.jecnRoleDao.isExistDefaultRole(JecnCommonSql.getStrAdmin(), peopleId));
			if (webLoginBean.isAdmin()) {
				return webLoginBean;
			}

			// 是不是二级管理员
			webLoginBean.setSecondAdmin(this.jecnRoleDao
					.isExistDefaultRole(JecnCommonSql.getStrSecondAdmin(), peopleId));
			if (webLoginBean.isSecondAdmin()) {
				return webLoginBean;
			}
			// 是不是浏览管理员
			webLoginBean.setViewAdmin(this.jecnRoleDao.isExistDefaultRole(JecnCommonSql.getStrViewAdmin(), peopleId));
			if (webLoginBean.isViewAdmin()) {
				return webLoginBean;
			}
			// 是不是下载管理员
			webLoginBean.setViewAdmin(this.jecnRoleDao
					.isExistDefaultRole(JecnCommonSql.getStrDownLoadAdmin(), peopleId));
			return webLoginBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public ICurProjectDao getCurProjectDao() {
		return curProjectDao;
	}

	public void setCurProjectDao(ICurProjectDao curProjectDao) {
		this.curProjectDao = curProjectDao;
	}

	@Override
	public void restorationUsers(List<Long> listPeopleIds) throws Exception {
		try {
			personDao.restorationPeoples(listPeopleIds);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void userPasswordUpdate(Long userId, String newPassword) throws Exception {
		try {
			String hql = "update  JecnUser set password=? where peopleId=?";
			personDao.execteHql(hql, newPassword, userId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<Long> getAllAdminList() throws Exception {
		try {
			String hql = "select u.peopleId from UserRole as u,JecnRoleInfo as r where u.roleId=r.roleId and r.filterId='admin'";
			return personDao.listHql(hql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 根据 人员主键ID获取登录人员信息
	 * 
	 * @param peopleId
	 *            登录人主键ID
	 * @return JecnUser 登录人人员信息
	 * @throws Exception
	 */
	@Override
	public JecnUser getUserById(Long peopleId) throws Exception {
		return personDao.get(peopleId);
	}

	@Override
	public int getTotalPerson(UserWebSearchBean userWebSearchBean) throws Exception {
		try {
			String sqlSQLSERVER = "";
			String sql = " select count(distinct ju.people_id) from jecn_user ju";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
					if (userWebSearchBean.getOrgId() != -1) {
						sqlSQLSERVER = "WITH MY_ORG AS (SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ userWebSearchBean.getOrgId()
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
						sql = sqlSQLSERVER + sql;
					}
				}
			}
			// 角色类型
			if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
				sql = sql + ",jecn_user_role jur,jecn_role_info jri";
			}
			// 部门
			if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
				if (userWebSearchBean.getOrgId() != -1) {
					if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
						sql = sql + ",MY_ORG org";
					} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
						sql = sql
								+ ",(select jfo.* from jecn_flow_org jfo CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
								+ userWebSearchBean.getOrgId() + ") org";
					}
				} else {
					sql = sql + ",(select jfo.* from jecn_flow_org jfo where jfo.org_name like '%"
							+ userWebSearchBean.getOrgName() + "%') org";
				}
				sql = sql + " ,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
			} else {
				if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
					sql = sql + " ,jecn_flow_org org,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
				}
			}

			// 登陆名称
			sql = sql + " where ju.ISLOCK=0";
			if (StringUtils.isNotBlank(userWebSearchBean.getLoginName())) {
				sql = sql + " and " + JecnCommonSql.getUpperString("ju.login_name") + " like "
						+ JecnCommonSql.getUpperLikeString(userWebSearchBean.getLoginName());
			}
			// 真实名称
			if (StringUtils.isNotBlank(userWebSearchBean.getTrueName())) {
				sql = sql + " and " + JecnCommonSql.getUpperString("ju.true_name") + " like "
						+ JecnCommonSql.getUpperLikeString(userWebSearchBean.getTrueName());
			}
			// 角色类型
			if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
				sql = sql + " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
						+ userWebSearchBean.getRoleType() + "'";
			}
			// 部门
			if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
				sql = sql
						+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
				if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
					if (userWebSearchBean.getPosId() != -1) {
						sql = sql + " and jfoi.figure_id=" + userWebSearchBean.getPosId();
					} else {
						sql = sql + " and jfoi.figure_text like '%" + userWebSearchBean.getPosName() + "%'";
					}
				}
			} else {
				if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
					sql = sql
							+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
					if (userWebSearchBean.getPosId() != -1) {
						sql = sql + " and jfoi.figure_id=" + userWebSearchBean.getPosId();
					} else {
						sql = sql + " and jfoi.figure_text like '%" + userWebSearchBean.getPosName() + "%'";
					}
				}
			}
			return this.personDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<UserWebInfoBean> getListPerson(UserWebSearchBean userWebSearchBean, int start, int limit)
			throws Exception {
		try {
			String sql = "";
			String withSql = "";
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {

				sql = " select distinct ju.people_id,ju.login_name,ju.true_name from jecn_user ju";
				if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
					if (userWebSearchBean.getOrgId() != -1) {
						withSql = "WITH MY_ORG AS (SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ userWebSearchBean.getOrgId()
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				// 角色类型
				if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
					sql = sql + ",jecn_user_role jur,jecn_role_info jri";
				}
				// 部门
				if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
					if (userWebSearchBean.getOrgId() != -1) {
						sql = sql + ",MY_ORG org";
					} else {
						sql = sql + ",(select jfo.* from jecn_flow_org jfo where jfo.org_name like '%"
								+ userWebSearchBean.getOrgName() + "%') org";
					}
					sql = sql + " ,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
				} else {
					if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
						sql = sql + " ,jecn_flow_org org,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
					}
				}

				// 登陆名称
				sql = sql + " where ju.ISLOCK=0";
				if (StringUtils.isNotBlank(userWebSearchBean.getLoginName())) {
					sql = sql + " and " + JecnCommonSql.getUpperString("ju.login_name") + " like "
							+ JecnCommonSql.getUpperLikeString(userWebSearchBean.getLoginName());
				}
				if (StringUtils.isNotBlank(userWebSearchBean.getTrueName())) {
					sql = sql + " and " + JecnCommonSql.getUpperString("ju.true_name") + " like "
							+ JecnCommonSql.getUpperLikeString(userWebSearchBean.getTrueName());
				}
				// 角色类型
				if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
					sql = sql + " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
							+ userWebSearchBean.getRoleType() + "'";
				}
				// 部门
				if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
					sql = sql
							+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
					if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
						if (userWebSearchBean.getPosId() != -1) {
							sql = sql + " and jfoi.figure_id=" + userWebSearchBean.getPosId();
						} else {
							sql = sql + " and jfoi.figure_text like '%" + userWebSearchBean.getPosName() + "%'";
						}
					}
				} else {
					if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
						sql = sql
								+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
						if (userWebSearchBean.getPosId() != -1) {
							sql = sql + " and jfoi.figure_id=" + userWebSearchBean.getPosId();
						} else {
							sql = sql + " and jfoi.figure_text like '%" + userWebSearchBean.getPosName() + "%'";
						}
					}
				}
				if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
					if (userWebSearchBean.getOrgId() != -1) {
						listObj = personDao.listSQLServerWith(withSql, sql, "people_id", start, limit);
					}
				} else {
					listObj = this.personDao.listSQLServerSelect(sql, "people_id", start, limit);
				}

			} else if (JecnCommon.isOracle()) {
				sql = " select distinct ju.people_id,ju.login_name,ju.true_name from jecn_user ju";

				// 角色类型
				if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
					sql = sql + ",jecn_user_role jur,jecn_role_info jri";
				}
				// 部门
				if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
					if (userWebSearchBean.getOrgId() != -1) {

						sql = sql
								+ ",(select jfo.* from jecn_flow_org jfo CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
								+ userWebSearchBean.getOrgId() + ") org";

					} else {
						sql = sql + ",(select jfo.* from jecn_flow_org jfo where jfo.org_name like '%"
								+ userWebSearchBean.getOrgName() + "%') org";
					}
					sql = sql + " ,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
				} else {
					if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
						sql = sql + " ,jecn_flow_org org,jecn_flow_org_image jfoi,jecn_user_position_related jupr";
					}
				}

				// 登陆名称
				sql = sql + " where ju.ISLOCK=0";
				if (StringUtils.isNotBlank(userWebSearchBean.getLoginName())) {
					sql = sql + " and " + JecnCommonSql.getUpperString("ju.login_name") + " like "
							+ JecnCommonSql.getUpperLikeString(userWebSearchBean.getLoginName());
				}
				// 真实名称
				if (StringUtils.isNotBlank(userWebSearchBean.getTrueName())) {
					sql = sql + " and " + JecnCommonSql.getUpperString("ju.true_name") + " like "
							+ JecnCommonSql.getUpperLikeString(userWebSearchBean.getTrueName());
				}
				// 角色类型
				if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
					sql = sql + " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
							+ userWebSearchBean.getRoleType() + "'";
				}
				// 部门
				if (StringUtils.isNotBlank(userWebSearchBean.getOrgName())) {
					sql = sql
							+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
					if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
						if (userWebSearchBean.getPosId() != -1) {
							sql = sql + " and jfoi.figure_id=" + userWebSearchBean.getPosId();
						} else {
							sql = sql + " and jfoi.figure_text like '%" + userWebSearchBean.getPosName() + "%'";
						}
					}
				} else {
					if (StringUtils.isNotBlank(userWebSearchBean.getPosName())) {
						sql = sql
								+ " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
						if (userWebSearchBean.getPosId() != -1) {
							sql = sql + " and jfoi.figure_id=" + userWebSearchBean.getPosId();
						} else {
							sql = sql + " and jfoi.figure_text like '%" + userWebSearchBean.getPosName() + "%'";
						}
					}
				}

				sql = sql + " order by ju.login_name";
				listObj = this.personDao.listNativeSql(sql, start, limit);

			}

			List<UserWebInfoBean> listUserWebInfoBean = new ArrayList<UserWebInfoBean>();
			if (listObj == null) {
				return listUserWebInfoBean;
			}
			Set<Long> setIds = new HashSet<Long>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
					continue;
				}
				setIds.add(Long.valueOf(obj[0].toString()));
				UserWebInfoBean userWebInfoBean = new UserWebInfoBean();
				userWebInfoBean.setUserId(Long.valueOf(obj[0].toString()));
				userWebInfoBean.setLoginName(obj[1].toString());
				userWebInfoBean.setTrueName(obj[2].toString());
				listUserWebInfoBean.add(userWebInfoBean);
			}

			if (setIds.size() == 0) {
				return listUserWebInfoBean;
			}

			List<Object[]> listPos = new ArrayList<Object[]>();
			// 部门岗位
			sql = "select jfoi.figure_id, jfoi.figure_text, jfo.org_id,jfo.org_name,jupr.people_id from jecn_user_position_related jupr,jecn_flow_org_image jfoi,jecn_flow_org jfo"
					+ "       where jupr.figure_id=jfoi.figure_id and jfoi.org_id = jfo.org_id and jfo.del_state=0"
					+ "       and jupr.people_id in";
			List<String> listStr = JecnCommonSql.getSetSqls(sql, setIds);
			for (String str : listStr) {
				List<Object[]> list = this.personDao.listNativeSql(str);
				for (Object[] obj : list) {
					listPos.add(obj);
				}
			}
			// 角色
			List<Object[]> listRole = new ArrayList<Object[]>();
			sql = "select jri.role_id,jri.role_name,jur.people_id from jecn_role_info jri,jecn_user_role jur where jri.role_id=jur.role_id and jur.people_id in";
			listStr = JecnCommonSql.getSetSqls(sql, setIds);
			for (String str : listStr) {
				List<Object[]> list = this.personDao.listNativeSql(str);
				for (Object[] obj : list) {
					listRole.add(obj);
				}
			}

			for (UserWebInfoBean userWebInfoBean : listUserWebInfoBean) {
				// 岗位
				List<String> listPosStr = new ArrayList<String>();
				// 部门
				List<String> listOrgStr = new ArrayList<String>();
				// 角色
				List<String> listRoleStr = new ArrayList<String>();
				for (Object[] objPos : listPos) {
					if (objPos == null || objPos[0] == null || objPos[1] == null || objPos[2] == null
							|| objPos[3] == null || objPos[4] == null) {
						continue;
					}
					if (userWebInfoBean.getUserId().toString().equals(objPos[4].toString())) {
						listPosStr.add(objPos[1].toString());
						listOrgStr.add(objPos[3].toString());
					}
				}
				for (Object[] objRole : listRole) {
					if (objRole == null || objRole[0] == null || objRole[1] == null || objRole[2] == null) {
						continue;
					}
					if (userWebInfoBean.getUserId().toString().equals(objRole[2].toString())) {
						listRoleStr.add(objRole[1].toString());
					}
				}
				userWebInfoBean.setListOrg(listOrgStr);
				userWebInfoBean.setListPos(listPosStr);
				userWebInfoBean.setListRole(listRoleStr);
			}

			return listUserWebInfoBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<Long> getPosUserInfoList(Long figureId) throws Exception {
		String sql = "select people_id from JECN_USER_POSITION_RELATED where FIGURE_ID =" + figureId;
		return personDao.getSQLQuery(sql).list();
	}

	/**
	 * 添加人员 验证最大用户
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public int addPeopleValidate() throws Exception {
		int userTotalCount = personDao.userTotalCount();
		int serverKeyCount = JecnKey.getServerPeopleCounts();
		if (userTotalCount >= serverKeyCount) {// 人员
			return serverKeyCount;
		}
		return -1;
	}

	/**
	 * 通过peopleId获得用户信息 hyl
	 * 
	 * @return webLoginBean
	 * @throws Exception
	 */
	@Override
	public UserInfo getUserInfoById(Long peopleId) throws Exception {

		UserInfo userInfo = new UserInfo();
		try {
			JecnUser jecnUser = personDao.getJecnUserById(peopleId);

			// 设置用户id
			jecnUser.setPeopleId(peopleId);
			userInfo.setJecnUser(jecnUser);
			// 岗位、组织信息
			List<Object[]> listPos = personDao.getPosOrgInfo(peopleId);
			List<PosBean> listPosBean = new ArrayList<PosBean>();
			for (Object[] obj : listPos) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}
				PosBean posBean = new PosBean();
				// 岗位ID
				posBean.setPosId(Long.valueOf(obj[0].toString()));
				// 岗位名称
				if (obj[1] != null) {
					posBean.setPosName(obj[1].toString());
				} else {
					posBean.setPosName("");
				}
				// 部门ID
				posBean.setOrgId(Long.valueOf(obj[2].toString()));
				// 部门名称
				if (obj[3] != null) {
					posBean.setOrgName(obj[3].toString());
				} else {
					posBean.setOrgName("");
				}
				listPosBean.add(posBean);
			}
			userInfo.setListPosBean(listPosBean);
			// 角色信息
			List<JecnRoleInfo> roleList = jecnRoleDao.getDefaultRoleByPeopleId(peopleId);
			userInfo.setRoleList(roleList);

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return userInfo;

	}

	@Override
	public void updateJecnUser(JecnUser jecnUser) throws Exception {
		personDao.update(jecnUser);
	}

	@Override
	public JecnUser getJecnUserByPeopleId(Long peopleId) {
		String hql = "from JecnUser where peopleId=?";
		List<JecnUser> users = personDao.listHql(hql, peopleId);
		JecnUser user = null;
		if (users.size() > 0) {
			user = users.get(0);
		}
		return user;
	}

	@Override
	public JecnUser getJecnUserByEmail(String email) throws Exception {
		return personDao.getJecnUserByEmail(email);
	}

	@Override
	public JecnUser getJecnUserByPhone(String phone) throws Exception {
		return personDao.getJecnUserByPhone(phone);
	}

	@Override
	public int findAdminUseCount() {
		return personDao.findAdminUseCount();
	}

	@Override
	public int findDesignUseCount() {
		return personDao.findDesignUseCount();
	}

	@Override
	public JecnPersonalPermRecord getPersionalPermRecord(Long id, int type, Long peopleId) throws Exception {
		String hql = "from JecnPersonalPermRecord where rId=? and rType=? and userId=?";
		try {
			JecnPersonalPermRecord record = this.personDao.getObjectHql(hql, id, type, peopleId);
			return record;
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	@Override
	public Map<String, String> getJecnIftekSyncUser(List<String> name) {
		Map<String, String> mapUser = new HashMap<String, String>();
		if (name != null && !name.isEmpty()) {
			mapUser = JecnIflytekSyncUserEnum.INSTANCE.getMapUser(name);
		}
		return mapUser;
	}

	/**
	 * 科大讯飞 根据登录名获取人员信息
	 */
	@Override
	public List<JecnTreeBean> searchUserByIftekSyncLoginName(List<String> names, Long projectId) {
		List<String> loginNames = new ArrayList<String>();
		Map<String, String> jecnIftekSyncUser = JecnIflytekSyncUserEnum.INSTANCE.getMapUser(names);
		List<JecnTreeBean> searchResult = new ArrayList<JecnTreeBean>();
		if (jecnIftekSyncUser != null && !jecnIftekSyncUser.isEmpty()) {
			// 获取所有登录名称
			for (Map.Entry<String, String> entry : jecnIftekSyncUser.entrySet()) {
				loginNames.add(entry.getKey().toUpperCase());
			}
			if (loginNames != null && !loginNames.isEmpty()) {
				String sql = "SELECT DISTINCT JU.PEOPLE_ID," + "                JU.TRUE_NAME,"
						+ "                JFLO.FIGURE_TEXT," + "                JFO.ORG_NAME,"
						+ "                JU.LOGIN_NAME" + "  FROM JECN_USER JU"
						+ "  LEFT JOIN JECN_USER_POSITION_RELATED JUP" + "    ON JU.PEOPLE_ID = JUP.PEOPLE_ID"
						+ "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFLO" + "    ON JUP.FIGURE_ID = JFLO.FIGURE_ID"
						+ "  LEFT JOIN JECN_FLOW_ORG JFO" + "    ON JFLO.ORG_ID = JFO.ORG_ID"
						+ "   AND JFO.DEL_STATE = 0" + "   AND JFO.PROJECTID = ?"
						+ " WHERE JU.ISLOCK=0 AND UPPER(JU.LOGIN_NAME) IN " + JecnCommonSql.getStrs(loginNames);
				List<Object[]> list = personDao.listNativeSql(sql, 0, JecnCommonSql.pageSize, projectId);
				searchResult = getSearchResult(list);
				// 相同登录名的时候 封装 treeBean
				for (Map.Entry<String, String> entry : jecnIftekSyncUser.entrySet()) {
					for (JecnTreeBean bean : searchResult) {
						if (entry.getKey().equals(bean.getLoginName())) {
							bean.setName(entry.getValue());
						}
					}
				}
			}
		}
		return searchResult;
	}

	@Override
	public String getListUserSearchBean(String loginName) {
		List<String> loginNames = new ArrayList<String>();
		loginNames.add(loginName);
		Map<String, String> mapUserName = JecnIflytekSyncUserEnum.INSTANCE.getMapUserName(loginNames);
		return (mapUserName != null && !mapUserName.isEmpty()) ? mapUserName.get(loginName) : "";
	}

}
