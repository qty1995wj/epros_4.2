/**
 * SetItemValue.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class SetItemValue  implements java.io.Serializable {
    private java.lang.String userCode;

    private java.lang.String bizObjectSchemaCode;

    private java.lang.String bizObjectId;

    private java.lang.String keyName;

    private java.lang.Object keyValue;

    public SetItemValue() {
    }

    public SetItemValue(
           java.lang.String userCode,
           java.lang.String bizObjectSchemaCode,
           java.lang.String bizObjectId,
           java.lang.String keyName,
           java.lang.Object keyValue) {
           this.userCode = userCode;
           this.bizObjectSchemaCode = bizObjectSchemaCode;
           this.bizObjectId = bizObjectId;
           this.keyName = keyName;
           this.keyValue = keyValue;
    }


    /**
     * Gets the userCode value for this SetItemValue.
     * 
     * @return userCode
     */
    public java.lang.String getUserCode() {
        return userCode;
    }


    /**
     * Sets the userCode value for this SetItemValue.
     * 
     * @param userCode
     */
    public void setUserCode(java.lang.String userCode) {
        this.userCode = userCode;
    }


    /**
     * Gets the bizObjectSchemaCode value for this SetItemValue.
     * 
     * @return bizObjectSchemaCode
     */
    public java.lang.String getBizObjectSchemaCode() {
        return bizObjectSchemaCode;
    }


    /**
     * Sets the bizObjectSchemaCode value for this SetItemValue.
     * 
     * @param bizObjectSchemaCode
     */
    public void setBizObjectSchemaCode(java.lang.String bizObjectSchemaCode) {
        this.bizObjectSchemaCode = bizObjectSchemaCode;
    }


    /**
     * Gets the bizObjectId value for this SetItemValue.
     * 
     * @return bizObjectId
     */
    public java.lang.String getBizObjectId() {
        return bizObjectId;
    }


    /**
     * Sets the bizObjectId value for this SetItemValue.
     * 
     * @param bizObjectId
     */
    public void setBizObjectId(java.lang.String bizObjectId) {
        this.bizObjectId = bizObjectId;
    }


    /**
     * Gets the keyName value for this SetItemValue.
     * 
     * @return keyName
     */
    public java.lang.String getKeyName() {
        return keyName;
    }


    /**
     * Sets the keyName value for this SetItemValue.
     * 
     * @param keyName
     */
    public void setKeyName(java.lang.String keyName) {
        this.keyName = keyName;
    }


    /**
     * Gets the keyValue value for this SetItemValue.
     * 
     * @return keyValue
     */
    public java.lang.Object getKeyValue() {
        return keyValue;
    }


    /**
     * Sets the keyValue value for this SetItemValue.
     * 
     * @param keyValue
     */
    public void setKeyValue(java.lang.Object keyValue) {
        this.keyValue = keyValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetItemValue)) return false;
        SetItemValue other = (SetItemValue) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userCode==null && other.getUserCode()==null) || 
             (this.userCode!=null &&
              this.userCode.equals(other.getUserCode()))) &&
            ((this.bizObjectSchemaCode==null && other.getBizObjectSchemaCode()==null) || 
             (this.bizObjectSchemaCode!=null &&
              this.bizObjectSchemaCode.equals(other.getBizObjectSchemaCode()))) &&
            ((this.bizObjectId==null && other.getBizObjectId()==null) || 
             (this.bizObjectId!=null &&
              this.bizObjectId.equals(other.getBizObjectId()))) &&
            ((this.keyName==null && other.getKeyName()==null) || 
             (this.keyName!=null &&
              this.keyName.equals(other.getKeyName()))) &&
            ((this.keyValue==null && other.getKeyValue()==null) || 
             (this.keyValue!=null &&
              this.keyValue.equals(other.getKeyValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserCode() != null) {
            _hashCode += getUserCode().hashCode();
        }
        if (getBizObjectSchemaCode() != null) {
            _hashCode += getBizObjectSchemaCode().hashCode();
        }
        if (getBizObjectId() != null) {
            _hashCode += getBizObjectId().hashCode();
        }
        if (getKeyName() != null) {
            _hashCode += getKeyName().hashCode();
        }
        if (getKeyValue() != null) {
            _hashCode += getKeyValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetItemValue.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">SetItemValue"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "userCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bizObjectSchemaCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "bizObjectSchemaCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bizObjectId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "bizObjectId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "keyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "keyValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
