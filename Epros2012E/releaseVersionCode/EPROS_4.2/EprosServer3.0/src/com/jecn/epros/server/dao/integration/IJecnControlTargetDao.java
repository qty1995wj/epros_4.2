package com.jecn.epros.server.dao.integration;

import java.util.Set;

import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.common.IBaseDao;
/****
 * 控制目标表操作类
 * 2013-10-30
 *
 */
public interface IJecnControlTargetDao extends IBaseDao<JecnControlTarget, Long> {
	
	/***
	 * 删除与风险相关的数据
	 * @param setIds
	 * @throws Exception
	 */
	public void delControlTargetByRiskIds(Set<Long> setIds) throws Exception;
	

}
