package com.jecn.epros.util.excel;

import com.jecn.epros.domain.InventoryTable;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

public class SheetCellStyle {

    public static final short DEFAULT_COLOR = HSSFColor.BLACK.index;
    public static final short PUB_COLOR = HSSFColor.GREEN.index;
    public static final short APPROVE_COLOR = HSSFColor.LIGHT_ORANGE.index;
    public static final short NOT_PUB_COLOR = HSSFColor.RED.index;

    /**
     * 表格内容样式
     */
    public static HSSFCellStyle getInventoryCellStyle(HSSFWorkbook workbook, short fontColor, short align) {
        // 生成另一个字体
        HSSFFont cellFont = createDefaultFont(workbook);
        cellFont.setColor(fontColor);
        return getInventoryCellStyle(workbook, cellFont, align);
    }

    public static HSSFCellStyle getInventoryCellStyle(HSSFWorkbook workbook, HSSFFont cellFont, short align) {
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
        fillAllBorder(style);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setAlignment(align);
        style.setWrapText(true);
        // 把字体应用到当前的样式
        style.setFont(cellFont);
        return style;
    }

    public static HSSFCellStyle getInventoryCellStyle(HSSFWorkbook workbook, InventoryTable.Status status, InventoryTable.Align align) {
        short statusVal = getStatusVal(status);
        short alignVal = getAlignVal(align);
        return getInventoryCellStyle(workbook, statusVal, alignVal);
    }

    private static short getStatusVal(InventoryTable.Status status) {
        short statusVal = DEFAULT_COLOR;
        if (status == InventoryTable.Status.pub) {
            statusVal = PUB_COLOR;
        } else if (status == InventoryTable.Status.approve) {
            statusVal = APPROVE_COLOR;
        } else if (status == InventoryTable.Status.notPub) {
            statusVal = NOT_PUB_COLOR;
        }
        return statusVal;
    }

    private static short getAlignVal(InventoryTable.Align align) {
        short alignVal = HSSFCellStyle.ALIGN_CENTER;
        if (align == InventoryTable.Align.center) {
            alignVal = HSSFCellStyle.ALIGN_CENTER;
        } else if (align == InventoryTable.Align.left) {
            alignVal = HSSFCellStyle.ALIGN_LEFT;
        } else if (align == InventoryTable.Align.right) {
            alignVal = HSSFCellStyle.ALIGN_RIGHT;
        }
        return alignVal;
    }

    public static HSSFFont createDefaultFont(HSSFWorkbook workbook) {
        HSSFFont font = workbook.createFont();
        setDefaultFontStyle(font);
        return font;
    }

    /**
     * 表头单元格样式
     *
     * @param workbook
     * @return
     */
    public static HSSFCellStyle getInventoryTitleCellStyle(HSSFWorkbook workbook) {
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFPalette palette = workbook.getCustomPalette();
        // 自定义颜色
        palette.setColorAtIndex((short) 40, (byte) (197), (byte) (217), (byte) (241));
        style.setFillForegroundColor((short) 40);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        fillAllBorder(style);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.ALIGN_CENTER);

        // 生成一个字体
        HSSFFont font = createDefaultFont(workbook);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        font.setBold(true);
        // 把字体应用到当前的样式
        style.setFont(font);
        return style;
    }

    private static void setDefaultFontStyle(HSSFFont titleFont) {
        titleFont.setColor(HSSFColor.BLACK.index);
        titleFont.setFontHeightInPoints((short) 11);
        titleFont.setFontName("宋体");
    }

    /**
     * 顶部的总计发布未发布等单元格样式
     *
     * @param workbook
     * @return
     */
    public static HSSFCellStyle getInventoryNumCellStyle(HSSFWorkbook workbook, short color) {
        HSSFCellStyle style = workbook.createCellStyle();
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.ALIGN_CENTER);
        // 生成一个字体
        HSSFFont font = createDefaultFont(workbook);
        font.setColor(color);
        // 把字体应用到当前的样式
        style.setFont(font);
        return style;
    }

    /**
     * 序号的字体样式
     *
     * @param workbook
     * @return
     */
    public static HSSFCellStyle getInventoryIndexCellStyle(HSSFWorkbook workbook) {
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
        fillAllBorder(style);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // 生成另一个字体
        HSSFFont cellFont = createDefaultFont(workbook);

        // 把字体应用到当前的样式
        style.setFont(cellFont);
        return style;
    }

    private static void fillAllBorder(HSSFCellStyle style) {
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
    }

}
