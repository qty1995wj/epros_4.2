package com.jecn.webservice.jinke.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;
import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;
import com.jecn.webservice.jbshihua.service.DeptService;
import com.jecn.webservice.jbshihua.service.UserService;
import com.jecn.webservice.jinke.IJKOASyncService;
import com.jecn.webservice.wanhua.impl.WanHuaOADeptServiceImpl;

@WebService
public class JKOASyncServiceImpl implements IJKOASyncService {
	private Logger log = Logger.getLogger(WanHuaOADeptServiceImpl.class);

	@Override
	@WebMethod
	public String saveSyncDeptAndUsers(String data) {
		Map<String, String> map = new HashMap<String, String>();
		if (StringUtils.isBlank(data)) {
			map.put("state", "0");
			map.put("message", "同步失败！，初始化json为空");
			JSONArray jsonArray = JSONArray.fromObject(map);
			String json = jsonArray.toString();
			log.error(json);
			return json;
		}
		log.info("获取数据：" + data);
		JSONArray jsonArray = JSONArray.fromObject(data);
		// 根据数据对象获取代保存的数据 部门
		List<JecnOrgView> saveDepts = new ArrayList<JecnOrgView>();
		// 根据数据对象获取代保存的数据 人员
		List<JecnUserPosView> saveUsers = new ArrayList<JecnUserPosView>();

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			String masterClass = jsonObject.getString("MASTER_CLASS");
			if ("002001".equals(masterClass)) { // 组织数据
				saveDepts.add(getOrgView(jsonObject));
			} else if ("002002".equals(masterClass)) { // 人员数据
				JecnUserPosView userPosView = getUserPosView(jsonObject);
				if (userPosView == null) {
					continue;
				}
				saveUsers.add(userPosView);
			}
		}

		DeptService deptService = (DeptService) ApplicationContextUtil.getContext().getBean("DeptServiceImpl");
		UserService userService = (UserService) ApplicationContextUtil.getContext().getBean("UserServiceImpl");
		try {
			if (saveDepts != null && !saveDepts.isEmpty()) {
				deptService.recevieOrgJB(saveDepts);
				log.info("带存储部门总数：" + saveDepts.size());
			}
			if (saveUsers != null && !saveUsers.isEmpty()) {
				userService.receiveUser(saveUsers);
				log.info("带存储人员岗位总数：" + saveUsers.size());
			}
			map.put("state", "1");
			map.put("message", "同步成功！");
		} catch (Exception e) {
			map.put("state", "0");
			map.put("message", "同步失败！");
			log.error(JSONArray.fromObject(map).toString(), e);
			e.printStackTrace();
		}
		return JSONArray.fromObject(map).toString();
	}

	private JecnOrgView getOrgView(JSONObject jsonObject) {
		JecnOrgView deptInfo = new JecnOrgView();
		deptInfo.setOrgNum(jsonObject.getString("ORGANIZATIONCODE"));
		deptInfo.setOrgName(jsonObject.getString("FULLNAME"));
		deptInfo.setPerOrgNum(jsonObject.getString("HIGORGZATIONCODE"));
		deptInfo.setOrgStatus(jsonObject.getString("ISUSEDCODE"));
		deptInfo.setUUID(jsonObject.getString("ORGANIZATIONCODE"));
		return deptInfo;
	}

	private JecnUserPosView getUserPosView(JSONObject json) {
		String PERSONTYPE = json.getString("PERSONTYPE");
		if (!"A".equals(PERSONTYPE)) {
			log.info(json.toString());
			return null;
		}
		JecnUserPosView addUser = new JecnUserPosView();
		addUser.setUserNum(json.getString("PERSONNELCODE"));
		addUser.setTrueName(json.getString("PERSONNELNAME"));
		addUser.setLoginName(json.getString("PERACCOUNTNAME"));
		addUser.setOrgNum(json.getString("EXT_F"));
		addUser.setPosName(json.getString("STAPERSONNELROLE"));
		addUser.setEmail(json.getString("COMPEMAIL"));
		addUser.setStatus(json.getString("ISUSEDCODE"));
		if (StringUtils.isNotBlank(addUser.getOrgNum()) || StringUtils.isNotBlank(addUser.getPosName())) {
			addUser.setPosNum(addUser.getOrgNum() + "_" + addUser.getPosName());
		}

		if (StringUtils.isBlank(addUser.getPosNum()) || StringUtils.isBlank(addUser.getPosName())
				|| StringUtils.isBlank(addUser.getOrgNum())) {
			addUser.setPosNum("");
			addUser.setPosName("");
			addUser.setOrgNum("");
		}
		return addUser;
	}

}
