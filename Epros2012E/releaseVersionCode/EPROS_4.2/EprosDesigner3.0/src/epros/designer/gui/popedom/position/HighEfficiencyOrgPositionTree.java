package epros.designer.gui.popedom.position;

import java.awt.event.MouseEvent;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.popedom.person.PersonChooseTreeListener;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
/**
 * @author yxw 2012-6-14
 * @description：
 */
public class HighEfficiencyOrgPositionTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(HighEfficiencyOrgPositionTree.class);
	public HighEfficiencyOrgPositionTree(Long startId) {
		
		super(startId);
	}
	
	public HighEfficiencyOrgPositionTree() {
		
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new PersonChooseTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.organizationRoot,JecnProperties.getValue("orgPosition"));
		try {
			List<JecnTreeBean> list=ConnectionPool.getOrganizationAction().getChildOrgsAndPositon(0L,JecnConstants.projectId,true);
			JecnTreeCommon.addNLevelNodes(list, rootNode);
		} catch (Exception e) {
			log.error("HighEfficiencyOrgPositionTree getTreeModel is error！",e);
		}
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
