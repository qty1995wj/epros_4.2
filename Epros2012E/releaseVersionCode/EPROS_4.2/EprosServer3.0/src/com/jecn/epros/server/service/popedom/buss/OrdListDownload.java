package com.jecn.epros.server.service.popedom.buss;

import java.io.File;
import java.util.Map;
import java.util.Set;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.popedom.OrgListBean;
import com.jecn.epros.server.webBean.popedom.OrgRelateFlowBean;
import com.jecn.epros.server.webBean.popedom.ProcessListBean;

/**
 * 创建组织清单下载
 * 
 * @author fuzhh 2013-12-5
 * 
 */
public class OrdListDownload {
	private static final Logger log = Logger.getLogger(OrdListDownload.class);
	private static boolean isGuardShow = false;

	/**
	 * 创建岗位说明书
	 * 
	 * @param descriptionBean
	 *            JobDescriptionBean 岗位说明书数据
	 * @throws Exception
	 * 
	 */
	public static byte[] createOrgListDesc(OrgListBean orgList, int depth)
			throws Exception {
		if (orgList == null) {
			throw new NullPointerException();
		}
		WritableWorkbook wbookData = null;
		if ("1".equals(JecnContants.getValue("guardianPeople"))) {
			isGuardShow = true;
		}
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			File file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// 组织清单
			WritableSheet wsheet = wbookData.createSheet(JecnUtil
					.getValue("groupListing"), 0);
			// 表样式
			WritableCellFormat wformat = new WritableCellFormat();
			wformat.setBorder(Border.ALL, BorderLineStyle.THIN);

			WritableFont fontTitle = new WritableFont(WritableFont.TIMES, 12,
					WritableFont.BOLD);
			WritableCellFormat wformatTitle = new WritableCellFormat(fontTitle);
			// 表标题样式
			wformatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);

			// 获取总的级别个数
			int levelTotal = orgList.getLevelTotal();
			int count = 0;
			// 第一行为标题行
			final int titleRow = 1;
			for (int i = 0; i < levelTotal; i++) {// 创建级别标题
				if (i >= depth) {
					// 级
					wsheet.addCell(new Label(count, titleRow, i
							+ JecnUtil.getValue("level"), wformatTitle));
					wsheet.setColumnView(count, 20);
					count++;
				}
			}
			// 部门职责
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("departmentsResponsibility"), wformatTitle));
			wsheet.setColumnView(count, 50);
			count += 1;
			// 流程名称
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("processName"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;
			// 流程编号
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("processNumber"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;

			// 流程KPI
			if(JecnUtil.isVersionType()){
				wsheet.addCell(new Label(count, titleRow, JecnUtil
						.getValue("processKPI"), wformatTitle));
				wsheet.setColumnView(count, 20);
				count += 1;
			}
			
			// 关键活动
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("keyActivities"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;
			// 流程责任人
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("processResponsiblePersons"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;
			if (isGuardShow) {
				// 流程监护人
				wsheet.addCell(new Label(count, titleRow, JecnUtil
						.getValue("processTheGuardian"), wformatTitle));
				wsheet.setColumnView(count, 20);
				count += 1;
			}
			// 拟稿人
			//processDraftPeople
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("peopleMake"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;
			// 版本号
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("versionNumber"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;
			// 发布日期
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("releaseDate"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;
			// 有效期
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("validity"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;
			// 是否及时优化
			wsheet.addCell(new Label(count, titleRow, JecnUtil
					.getValue("theTimelyOptimization"), wformatTitle));
			wsheet.setColumnView(count, 20);
			count += 1;

			WritableFont fontContent = new WritableFont(WritableFont.TIMES, 12);
			WritableCellFormat wformatContent = new WritableCellFormat(
					fontContent);
			// 设置自动换行
			wformatContent.setWrap(true);
			// 表标题样式
			wformatContent.setBorder(Border.ALL, BorderLineStyle.THIN);
			// 添加数据
			addSheetData(orgList, wsheet, wformatContent, depth);
			// 写入Exel工作表
			wbookData.write();
			// 关闭Excel工作薄对象
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			// 把一个文件转化为字节
			byte[] tempDate = JecnUtil.getByte(file);
			if (file.exists()) {
				file.delete();
			}
			return tempDate;
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("关闭WritableWorkbook异常", e);
				}
			}
		}
		
		return null;
	}

	/**
	 * 添加数据
	 * 
	 * @param processSystemListBean
	 * @param wsheet
	 * @param wformatTitle
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private static void addSheetData(OrgListBean orgList, WritableSheet wsheet,
			WritableCellFormat wformatTitle, int depth)
			throws RowsExceededException, WriteException {
		int count = 2;
		// 总行数
		int totalCell = orgList.getLevelTotal() + 10-depth;
		totalCell += isGuardShow ? 1 : 0;
		totalCell += JecnUtil.isVersionType() ? 1 : 0;
		for (int i = 0; i < orgList.getOrgRelatsList().size(); i++) {
			OrgRelateFlowBean orgRelateFlow = orgList.getOrgRelatsList().get(i);
			//相关流程
			Map<Long, ProcessListBean> relateProcessMap=orgRelateFlow.getProcessMap();
			if (relateProcessMap.keySet().size() == 0) {
				for (int k = 0; k < orgList.getLevelTotal(); k++) {
					if (k >= depth) {
						if (k == orgRelateFlow.getLevel()) {
							String orgName = orgRelateFlow.getOrgName();
							// 添加级别名称
							wsheet.addCell(new Label(k - depth, count, orgName,
									wformatTitle));
						} else {
							wsheet.addCell(new Label(k - depth, count, "",
									wformatTitle));
						}
					}
				}
				// 部门职责
				String orgDuties = orgRelateFlow.getOrgDuties();
				wsheet.addCell(new Label(orgList.getLevelTotal() - depth,
						count, orgDuties, wformatTitle));
				// 剩余的空白行
				for (int cell = orgList.getLevelTotal()-depth+1; cell < totalCell; cell++) {
					wsheet.addCell(new Label(cell, count, "", wformatTitle));
				}
				count++;
			} else {
				boolean isOrgOne = true;
				Set<Long> orgIdSet = orgRelateFlow.getProcessMap().keySet();
				for (Long orgId : orgIdSet) {
					ProcessListBean processList = orgRelateFlow.getProcessMap()
							.get(orgId);
					if (isOrgOne) {
						for (int k = 0; k < orgList.getLevelTotal(); k++) {
							if (k >= depth) {
								if (k == orgRelateFlow.getLevel()) {
									// 添加级别名称
									wsheet.addCell(new Label(k - depth, count,
											orgRelateFlow.getOrgName(),
											wformatTitle));
								} else {
									wsheet.addCell(new Label(k - depth, count,
											"", wformatTitle));
								}
							}
						}
						// 部门职责
						String orgDuties = orgRelateFlow.getOrgDuties();
						wsheet.addCell(new Label(orgList.getLevelTotal()
								- depth, count, orgDuties, wformatTitle));
						isOrgOne = false;
					} else {
						for (int k = 0; k < orgList.getLevelTotal(); k++) {
							if (k >= depth) {
								wsheet.addCell(new Label(k - depth, count, "",
										wformatTitle));
							}
						}
						wsheet.addCell(new Label(orgList.getLevelTotal()
								- depth, count, "", wformatTitle));
					}
					// 实际内容列开始
					int detailCellNum = orgList.getLevelTotal() - depth + 1;
					// 流程名称
					wsheet.addCell(new Label(detailCellNum, count, processList
							.getFlowName(), wformatTitle));
					detailCellNum++;
					// 流程编号
					wsheet.addCell(new Label(detailCellNum, count, processList
							.getFlowNumber(), wformatTitle));
					detailCellNum++;
					
					// 流程KPI 高级版显示
					if(JecnUtil.isVersionType()){
						StringBuffer kpiStr = new StringBuffer("");
						Set<Long> kpiIdSet = processList.getFlowKPIMap().keySet();
						for (Long kpiId : kpiIdSet) {
							String str = processList.getFlowKPIMap().get(kpiId);
							kpiStr.append(str);
							kpiStr.append("\n");
						}
						wsheet.addCell(new Label(detailCellNum, count, kpiStr.toString(),
								wformatTitle));
						detailCellNum++;
					}
					// 关键活动
					StringBuffer activeKey = new StringBuffer("");
					Set<Long> activeIdSet = processList.getProcessActiveMap()
							.keySet();
					for (Long activeId : activeIdSet) {
						String activeName = processList.getProcessActiveMap()
								.get(activeId);
						if (!JecnCommon.isNullOrEmtryTrim(activeName)) {
							activeKey.append(activeName);
						}
						activeKey.append("\n");
					}
					wsheet.addCell(new Label(detailCellNum, count, activeKey
							.toString(), wformatTitle));
					detailCellNum++;
					// 流程责任人
					wsheet.addCell(new Label(detailCellNum, count, processList
							.getResPeopleName(), wformatTitle));
					detailCellNum++;
					if (isGuardShow) {
						wsheet.addCell(new Label(detailCellNum, count,
								processList.getGuardianName(), wformatTitle));
						detailCellNum++;
					}
					// 拟稿人
					wsheet.addCell(new Label(detailCellNum, count, processList
							.getDraftPerson(), wformatTitle));
					detailCellNum++;
					// 版本号
					wsheet.addCell(new Label(detailCellNum, count, processList
							.getVersionId(), wformatTitle));
					detailCellNum++;
					// 发布日期
					wsheet.addCell(new Label(detailCellNum, count, processList
							.getPubDate(), wformatTitle));
					detailCellNum++;
					// 有效期
					String expirt = "";
					if (processList.getExpiry() != null
							&& processList.getExpiry() != 0) {
						expirt = String.valueOf(processList.getExpiry());
					} else if (processList.getExpiry() == null
							|| processList.getExpiry() == 0) {
						// 永久
						expirt = JecnUtil.getValue("permanent");
					}
					wsheet.addCell(new Label(detailCellNum, count, expirt,
							wformatTitle));
					detailCellNum++;
					// 是否及时优化
					String optimization = "";
					if (processList.getOptimization() == 1) {
						// 是
						optimization = JecnUtil.getValue("is");
					} else if (processList.getOptimization() == 2) {
						// 否
						optimization = JecnUtil.getValue("no");
					}
					wsheet.addCell(new Label(detailCellNum, count,
							optimization, wformatTitle));
					count++;
				}
			}
		}
		//第一行统计		
		int space = totalCell - 5;
		for (int i = 0; i < space; i++) {
			// 空白行
			wsheet.addCell(new Label(i, 0, "", wformatTitle));
		}
		wsheet.addCell(new Label(space, 0, JecnUtil.getValue("orgTotal") + "("
				+ orgList.getOrgTotal() + ")", wformatTitle));
		wsheet.addCell(new Label(space + 1, 0, JecnUtil.getValue("flowTotal")
				+ "(" + orgList.getFlowTotal() + ")", wformatTitle));

		wsheet.addCell(new Label(space + 2, 0, JecnUtil
				.getValue("generationToBuildProcess")
				+ "(" + orgList.getNoPubFlowTotal() + ")", wformatTitle));
		wsheet.addCell(new Label(space + 3, 0, JecnUtil
				.getValue("forApprovalProcess")
				+ "(" + orgList.getApproveFlowTotal() + ")", wformatTitle));

		wsheet.addCell(new Label(space + 4, 0, JecnUtil
				.getValue("releaseProcess")
				+ "(" + orgList.getPubFlowTotal() + ")", wformatTitle));
	}
}
