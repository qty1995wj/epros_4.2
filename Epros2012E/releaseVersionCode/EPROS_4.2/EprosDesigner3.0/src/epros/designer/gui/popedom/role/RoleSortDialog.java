package epros.designer.gui.popedom.role;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class RoleSortDialog extends JecnSortDialog {

	private static Logger log = Logger.getLogger(RoleSortDialog.class);

	public RoleSortDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree);
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			ConnectionPool.getJecnRole().updateSortRoles(list, this.getSelectNode().getJecnTreeBean().getId(),
					JecnConstants.projectId, JecnConstants.getUserId());
			return true;
		} catch (Exception e) {
			log.error("RoleSortDialog saveData is error！", e);
			return false;
		}

	}

	@Override
	public List<JecnTreeBean> getChild() {
		try {
			return JecnRoleCommon.getRoleTreeBeans(this.getSelectNode());
		} catch (Exception e) {
			log.error("RoleSortDialog getChild is error!", e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public void okButtonAction() {
		List<JecnTreeDragBean> listResult = super.updateRecord();
		// 把默认目录加进去
		JecnTreeDragBean jecnTreeDragBean = new JecnTreeDragBean();
		jecnTreeDragBean.setSortId(0);
		jecnTreeDragBean.setTreeNodeType(TreeNodeType.roleDefaultDir);
		jecnTreeDragBean.setId(0L);
		listResult.add(0, jecnTreeDragBean);
		if (listResult.size() > 0) {
			if (saveData(listResult)) {
				JecnTreeCommon.sortNodes(getjTree(), getSelectNode(), listResult);
				this.dispose();
			}
		}

	}
}
