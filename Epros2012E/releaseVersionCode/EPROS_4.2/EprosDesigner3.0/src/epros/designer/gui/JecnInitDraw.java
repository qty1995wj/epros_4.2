package epros.designer.gui;

import java.awt.GridBagConstraints;

import javax.swing.SwingConstants;

import epros.designer.gui.workflow.JecnWorkflowMenuItem;
import epros.designer.service.JecnDesignerImpl;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolbarConstant.TooBarTitleNumEnum;
import epros.draw.constant.JecnWorkflowConstant.EasyProcessType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.designer.JecnDesigner;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.top.toolbar.JecnOftenPartPanel;
import epros.draw.gui.top.toolbar.JecnToolBarPanel;
import epros.draw.system.JecnUserCheckInfoProcess;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 初始化和画图工具结合功能，此对象系统组件实例化，最好在资源文件加载后就执行
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnInitDraw {
	/**
	 * 
	 * 初始化和画图工具结合功能
	 * 
	 */
	public static void initDraw() {
		// 1、 设置设计器标识
		JecnDesigner.setEprosType(EprosType.eprosDesigner);
		// 画图工具（不带操作说明等）
		JecnSystemStaticData.setEasyProcessType(EasyProcessType.eprosDesigner);
		// 工具栏标题个数
		JecnSystemStaticData.setTooBarTitleNumEnum(TooBarTitleNumEnum.five);
		// 2、 加载校验信息
		JecnUserCheckInfoProcess.start();
		// 3、 创建设计器与画图工具接口对象
		JecnDesignerProcess.getDesignerProcess().setDesigner(new JecnDesignerImpl());

		// 根据 CommonConfig.properties 文件加载是否 显示流程图背景网格线
		JecnSystemStaticData.setShowGrid(JecnDesignerProcess.getDesignerProcess().isShowGrid());

		// 4、 *****************右键菜单*****************//
		// 流程图面板右键
		JecnDesigner.addPartMapWorkflowMenuItem(JecnWorkflowMenuItem.getWorkflowMenuItem().getProcessMenuItemList());
		// 流程地图：画图面板右键菜单
		JecnDesigner
				.addTotalMapWorkflowMenuItem(JecnWorkflowMenuItem.getWorkflowMenuItem().getProcessMapMenuItemList());
		// 组织图：画图面板右键菜单
		JecnDesigner.addOrgMapWorkflowMenuItem(JecnWorkflowMenuItem.getWorkflowMenuItem().getOrgMapMenuItemList());

		// 流程地图元素右键
		JecnDesigner.addTotalMapFlowElemMenuItem(JecnWorkflowMenuItem.getWorkflowMenuItem()
				.getProcessMapElemwMenuItemList());
		// 流程图元素右键
		JecnDesigner
				.addPartMapFlowElemMenuItem(JecnWorkflowMenuItem.getWorkflowMenuItem().getProcessElemMenuItemList());
		// 组织图元素右键
		JecnDesigner.addOrgMapFlowElemMenuItem(JecnWorkflowMenuItem.getWorkflowMenuItem().getOrgMapElemMenuItemList());
		// *****************右键菜单*****************//
	}

	/**
	 * 
	 * 设计器特殊点：画图工具打开转移到常用面板中做导入；另存为改成导出
	 * 
	 * @param toolbar
	 */
	public static void initToolBar(JecnToolBarPanel toolbar) {
		// 打开（导入）
		JecnToolbarContentButton openMapBtn = toolbar.getFileToolBarItemPanel().getOpenMapBtn();
		// 打开
		openMapBtn.setHorizontalTextPosition(SwingConstants.RIGHT);
		openMapBtn.setVerticalTextPosition(SwingConstants.CENTER);
		openMapBtn.setEnabled(false);

		JecnOftenPartPanel oftenPartPanel = toolbar.getOftenPartPanel();
		// 另存为(导出)
		JecnToolbarContentButton saveAsBtn = oftenPartPanel.getSaveAsBtn();
		// 另存为图片
		JecnToolbarContentButton saveAsImageBtn = oftenPartPanel.getSaveAsImageBtn();

		// 打开
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets0(), 0, 0);
		oftenPartPanel.add(openMapBtn.getJToolBar(), c);
		openMapBtn.setText(JecnProperties.getValue("importFile"));

		// 另保存
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		oftenPartPanel.add(saveAsBtn.getJToolBar(), c);
		saveAsBtn.setText(JecnProperties.getValue("exportFile"));

		// 另存为图片
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		oftenPartPanel.add(saveAsImageBtn.getJToolBar(), c);
	}
}
