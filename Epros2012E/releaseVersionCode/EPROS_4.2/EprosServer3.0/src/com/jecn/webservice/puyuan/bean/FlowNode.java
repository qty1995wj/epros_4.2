package com.jecn.webservice.puyuan.bean;

/**
 * 流程元素
 * 
 * @author chehuanbo
 * @date 2014-01-19
 * 
 */
public class FlowNode {
    
	/**元素ID*/
	private Long nodeId;
	/**元素名称*/
	private String nodeName;
	/**元素类型*/
	private String nodeType;
	/*** X坐标 **/
	private int xNodePoint;
	/*** Y坐标 *****/
	private int yNodePoint;
	/***元素宽度 **/
    private int NodeWidth;
    /***元素高度*/
    private int NodeHeight;
    /*** 活动对应的角色**/
    private String activeRole;    
    /**活动对应角色ID*/
	private int activeRoleId;
	/**活动说明*/
	private String activeDesc;
	public String getNodeId() {
		return nodeId.toString();
	}
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeName() {
		if(nodeName == null){
			return "";
		}
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public String getxNodePoint() {
		return String.valueOf(xNodePoint);
	}
	public void setxNodePoint(int xNodePoint) {
		this.xNodePoint = xNodePoint;
	}
	public String getyNodePoint() {
		return String.valueOf(yNodePoint);
	}
	public void setyNodePoint(int yNodePoint) {
		this.yNodePoint = yNodePoint;
	}
	public String getNodeWidth() {
		return String.valueOf(NodeWidth);
	}
	public void setNodeWidth(int nodeWidth) {
		NodeWidth = nodeWidth;
	}
	public String getNodeHeight() {
		return String.valueOf(NodeHeight);
	}
	public void setNodeHeight(int nodeHeight) {
		NodeHeight = nodeHeight;
	}
	public String getNodeType() {
		if(nodeType == null){
			return "";
		}
		return nodeType;
	}
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	public String getActiveRole() {
		if(activeRole == null){
			return "";
		}
		return activeRole;
	}
	public void setActiveRole(String activeRole) {
		this.activeRole = activeRole;
	}
	public String getActiveRoleId() {
		if(activeRoleId == 0){
			return "";
		}
		return String.valueOf(activeRoleId);
	}
	public void setActiveRoleId(int activeRoleId) {
		this.activeRoleId = activeRoleId;
	}
	public String getActiveDesc() {
		if(activeDesc == null){
			return "";
		}
		return activeDesc;
	}
	public void setActiveDesc(String activeDesc) {
		this.activeDesc = activeDesc;
	}
	
	
	
	
	
	
	
	
	
	
}
