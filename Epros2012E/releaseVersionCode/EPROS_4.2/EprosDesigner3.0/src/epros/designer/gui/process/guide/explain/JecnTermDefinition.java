package epros.designer.gui.process.guide.explain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.define.TermDefineChooseDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.TermFigureData;
import epros.draw.gui.figure.shape.part.TermFigureAR;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 术语定义
 * 
 * @author zhr
 * 
 */
public class JecnTermDefinition extends JecnFileDescriptionTable {

	// 文件选择按钮初始化
	protected JButton selectBut = new JButton(JecnProperties.getValue("select"));
	/**
	 * 选择的数据
	 */
	protected List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();

	// 初始化的数据
	protected Set<JecnTreeBean> initTreeBeans = new HashSet<JecnTreeBean>();

	List<TermFigureAR> figurePanels = new ArrayList<TermFigureAR>();
	private JecnFlowBasicInfoT flowBasicInfoT;

	public JecnTermDefinition(int index, String name, boolean isRequest, JecnPanel contentPanel, String tip,
			JecnFlowBasicInfoT flowBasicInfoT) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowBasicInfoT = flowBasicInfoT;
		super.initTable();
		this.titlePanel.add(selectBut);
		initEvent();
		table.setTableColumn(0, 180);
	}

	private void initEvent() {
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				select();
			}
		});
	}

	protected void select() {
		TermDefineChooseDialog chooseDialog = new TermDefineChooseDialog(treeBeans);
		chooseDialog.disableButton();
		chooseDialog.setVisible(true);
		if (chooseDialog.isOperation()) {// 操作
			Vector<Vector<String>> data = ((JecnTableModel) getTable().getModel()).getDataVector();
			if (treeBeans != null && !treeBeans.isEmpty()) {
				if (data != null) {
					data.clear();
				}
				for (JecnTreeBean treeBean : treeBeans) {
					addContent(treeBean);
				}
			}
		}
	}

	@Override
	protected void dbClickMethod() {
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("name"));
		title.add(JecnProperties.getValue("difine"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> riskContent = new Vector<Vector<String>>();
		if (figurePanels == null || figurePanels.isEmpty()) {
			figurePanels = JecnDrawMainPanel.getMainPanel().getWorkflow().getTermFigureList();
		}
		Set<Long> ids = new HashSet<Long>();
		for (TermFigureAR termFigureAR : figurePanels) {
			TermFigureData flowElementData = termFigureAR.getFlowElementData();
			if (ids.contains(flowElementData.getDesignerFigureData().getLinkId())
					|| flowElementData.getDesignerFigureData().getLinkId() == null) {
				continue;
			}
			ids.add(flowElementData.getDesignerFigureData().getLinkId());

			Vector<String> content = new Vector<String>();
			// 名称
			content.add(termFigureAR.getFlowElementData().getFigureText());
			// 定义
			content.add(termFigureAR.getFlowElementData().getTermDefine());
			encapsulationBean(termFigureAR.getFlowElementData());
			riskContent.add(content);
		}
		return riskContent;
	}

	private void encapsulationBean(TermFigureData data) {
		if (data.getDesignerFigureData().getLinkId() != null && data.getDesignerFigureData().getLinkId() != 0L) {
			JecnTreeBean bean = new JecnTreeBean();
			bean.setContent(data.getTermDefine());
			bean.setName(data.getFigureText());
			bean.setId(data.getDesignerFigureData().getLinkId());
			treeBeans.add(bean);
			initTreeBeans.add(bean);
		}
	}

	private void addContent(JecnTreeBean bean) {
		Set<Long> ids = new HashSet<Long>();
		Vector<String> content = new Vector<String>();
		// 名称
		content.add(bean.getName());
		// 定义
		content.add(bean.getContent());
		((DefaultTableModel) table.getModel()).addRow(content);
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] {};
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return initTreeBeans.size() != treeBeans.size();
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	public List<JecnTreeBean> getAdd() {
		List<JecnTreeBean> result = new ArrayList<JecnTreeBean>();
		if (treeBeans != null && !treeBeans.isEmpty()) {
			for (JecnTreeBean treeBean : treeBeans) {
				if (!initTreeBeans.contains(treeBean)) {
					result.add(treeBean);
				}
			}
		}
		return result;
	}
}
