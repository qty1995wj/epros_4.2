package com.jecn.epros.server.webBean.integration;

/**
 * 风险清单 控制点相关活动bean
 * 
 * @author fuzhh 2013-12-13
 * 
 */
public class ControlActiveBean {
	/** 活动名称 */
	private String activeName;
	/** 活动ID */
	private Long acieveId;
	/** 活动说明 */
	private String activeShow;
	/** 流程名称 */
	private String flowName;
	/** 流程ID */
	private Long flowId;
	/** 责任部门ID */
	private Long resOrgId;
	/** 责任部门名称 */
	private String resOrgName;
	/** 控制方法 */
	private int controlMethod;
	/** 控制类型 */
	private int controlType;
	/** 是否为关键控制点 */
	private int keyControl;
	/** 控制频率 */
	private int controlFrequency;
	/** 活动控制点编号 */
	private String controlNum;

	public String getActiveName() {
		return activeName;
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}

	public Long getAcieveId() {
		return acieveId;
	}

	public void setAcieveId(Long acieveId) {
		this.acieveId = acieveId;
	}

	public String getActiveShow() {
		return activeShow;
	}

	public void setActiveShow(String activeShow) {
		this.activeShow = activeShow;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getResOrgId() {
		return resOrgId;
	}

	public void setResOrgId(Long resOrgId) {
		this.resOrgId = resOrgId;
	}

	public String getResOrgName() {
		if (resOrgName == null) {
			resOrgName = "";
		}
		return resOrgName;
	}

	public void setResOrgName(String resOrgName) {
		this.resOrgName = resOrgName;
	}

	public int getControlMethod() {
		return controlMethod;
	}

	public void setControlMethod(int controlMethod) {
		this.controlMethod = controlMethod;
	}

	public int getControlType() {
		return controlType;
	}

	public void setControlType(int controlType) {
		this.controlType = controlType;
	}

	public int getKeyControl() {
		return keyControl;
	}

	public void setKeyControl(int keyControl) {
		this.keyControl = keyControl;
	}

	public int getControlFrequency() {
		return controlFrequency;
	}

	public void setControlFrequency(int controlFrequency) {
		this.controlFrequency = controlFrequency;
	}

	public String getControlNum() {
		if (controlNum == null) {
			controlNum = "";
		}
		return controlNum;
	}

	public void setControlNum(String controlNum) {
		this.controlNum = controlNum;
	}
}
