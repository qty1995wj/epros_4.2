package com.jecn.epros.domain.propose;

import org.springframework.web.multipart.MultipartFile;

/**
 * 提交合理化建议的bean
 *
 * @author user
 */
public class ProposeCreate {

    /**
     * 关联的id
     **/
    private Long id;
    /**
     * 合理化建议类型：0流程，1制度，2流程架构，3文件，4风险，5标准
     **/
    private Integer type;
    /**
     * 合理化建议内容
     **/
    private String content;
    /**
     * 附件
     **/
    private MultipartFile file;
    /**
     * 0设置的处理人可见1所有人可见
     **/
    private Integer showType;


    public Integer getShowType() {
        return showType;
    }

    public void setShowType(Integer showType) {
        this.showType = showType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

}
