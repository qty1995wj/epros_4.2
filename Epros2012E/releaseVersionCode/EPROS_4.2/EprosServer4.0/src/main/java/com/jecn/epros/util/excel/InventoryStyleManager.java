package com.jecn.epros.util.excel;

import com.jecn.epros.domain.InventoryCell;
import com.jecn.epros.domain.InventoryCellStyle;
import com.jecn.epros.domain.InventoryTable.Align;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.util.*;

public class InventoryStyleManager {

    public static final String TITLE = "titleStyles";
    public static final String CONTENT = "contentStyles";

    public static Map<String, List<InventoryCellStyle>> getPosInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getInternationalizationValue(posInventoryTitles));
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(posTitleCellStyles);
        contentStyles.addAll(posContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getUserRoleInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getInternationalizationValue(roleUserInfoInventoryTitles));
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(processTitleCellStyles);
        contentStyles.addAll(processContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getPosStatisticsInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getInternationalizationValue(posStatisticsInventoryTitles));
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(posTitleCellStyles);
        contentStyles.addAll(posContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }


    public static Map<String, List<InventoryCellStyle>> getOrgInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getInternationalizationValue(orgInventoryTitles));
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(orgTitleCellStyles);
        contentStyles.addAll(orgContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getFileInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getInternationalizationValue(fileInventoryTitles));
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(fileTitleCellStyles);
        contentStyles.addAll(fileContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getRiskInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, InventoryStyleManager.getRiskTitles());
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(riskTitleCellStyles);
        contentStyles.addAll(riskContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getStandardInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getInternationalizationValue(standardInventoryTitles));
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(standardTitleCellStyles);
        contentStyles.addAll(standardContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getRuleInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getRuleTitles());
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(ruleTitleCellStyles);
        contentStyles.addAll(ruleContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getProcessSystemInventoryStyles(
            List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getInternationalizationValue(processSystemInventoryTitles));
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(processSystemTitleCellStyles);
        contentStyles.addAll(processSystemContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    public static Map<String, List<InventoryCellStyle>> getProcessInventoryStyles(List<InventoryCell> titleCells) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = getBaseTitleStyles(titleCells, getProcessTitles());
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        titleStyles.addAll(processTitleCellStyles);
        contentStyles.addAll(processContentCellStyles);
        return buildResultMap(titleStyles, contentStyles);
    }

    private static List<InventoryCellStyle> getBaseTitleStyles(List<InventoryCell> titleCells, List<String> srcTitles) {
        int lvCellNums = titleCells.size() - srcTitles.size();
//        String firstColumn = srcTitles.get(0);
//        for (int i = 0; i < titleCells.size(); i++) {
//            // 获取级别样式的数量
//            if (firstColumn.equals(titleCells.get(i).getText())) {
//                lvCellNums = i;
//                break;
//            }
//        }
//        if (ConfigUtils.inventoryShowDirEnd()) {
//            lvCellNums = titleCells.size() - srcTitles.size();
//        }
        List<InventoryCellStyle> titleStyles = new ArrayList<InventoryCellStyle>();
        while (lvCellNums > 0) {
            titleStyles.add(lvCellStyle);
            lvCellNums--;
        }
        return titleStyles;
    }


    public static final Map<String, List<InventoryCellStyle>> buildResultMap(List<InventoryCellStyle> titleStyles,
                                                                             List<InventoryCellStyle> contentStyles) {
        Map<String, List<InventoryCellStyle>> result = new HashMap<String, List<InventoryCellStyle>>();
        result.put(TITLE, titleStyles);
        result.put(CONTENT, contentStyles);
        return result;
    }

    private static final InventoryCellStyle lvCellStyle = new InventoryCellStyle();
    //添加表头信息请先添加 国际化文件
    private static final String[] processMapInventoryTitles = new String[]{
            "personLiable",//责任人
            "responsibilityDepartment",//责任部门
            "releaseStatus"//发布状态
    };
    private static final String[] processInventoryTitles = new String[]{
            "processName",//流程名称
            "processNum",//流程编号
            "releaseStatus",//发布状态
            "relatedDocuments",//相关文件
            "responsibilityDepartment",//责任部门
            "processResponsiblePerson",//流程责任人
            "processGuardian",//流程监护人
            "DrafterC",//拟稿人
            "versionNum",//版本号
            "ReleaseDate",//发布日期
            "termOfValidit",//有效期
            "whetherOrNotToOptimizeInTime",//是否及时优化
            "TheNextTimeYouLookAtTheTimeToFinish"//下次审视需完成时间
    };
    private static final String[] processMengNiuInventoryLevelTitles = new String[]{
            "processName",//流程名称
            "processNum",//流程编号
            "level",//级别
            "releaseStatus",//发布状态
            "relatedDocuments",//相关文件
            "responsibilityDepartment",//责任部门
            "versionNum",//版本号
            "ReleaseDate",//发布日期
            "termOfValidit",//有效期
    };
    private static final String[] processInventoryLevelTitles = new String[]{
            "processName",//流程名称
            "processNum",//流程编号
            "level",//级别
            "releaseStatus",//发布状态
            "relatedDocuments",//相关文件
            "responsibilityDepartment",//责任部门
            "processResponsiblePerson",//流程责任人
            "processGuardian",//流程监护人
            "DrafterC",//拟稿人
            "versionNum",//版本号
            "ReleaseDate",//发布日期
            "termOfValidit",//有效期
            "whetherOrNotToOptimizeInTime",//是否及时优化
            "TheNextTimeYouLookAtTheTimeToFinish"//下次审视需完成时间
    };
    private static final List<InventoryCellStyle> processTitleCellStyles; //流程
    private static final List<InventoryCellStyle> processContentCellStyles;

    private static final String[] processSystemInventoryTitles = new String[]{
            "ReleaseDate",//发布日期
            "versionNum",//版本号
            "relatedDocuments"//相关文件
    };
    private static final List<InventoryCellStyle> processSystemTitleCellStyles;
    private static final List<InventoryCellStyle> processSystemContentCellStyles;

    private static final String[] ruleInventoryTitles = new String[]{
            "ruleName",//制度名称
            "ruleNum",//制度编号
            "relatedDocuments",//相关文件
            "ruleResDept",//责任部门
            "ruleResPeople",//责任人
            "DrafterC",//拟稿人
            "versionNum",//版本号
            "ReleaseDate",//发布日期
            "termOfValidit"//有效期
    };

    private static final String[] ruleInventoryLevelTitles = new String[]{
            "ruleName",//制度名称
            "ruleNum",//制度编号
            "level",//级别
            "relatedDocuments",//相关文件
            "ruleResDept",//责任部门
            "ruleResPeople",//责任人
            "DrafterC",//拟稿人
            "versionNum",//版本号
            "ReleaseDate",//发布日期
            "termOfValidit"//有效期
    };

    private static final String[] ruleMengNiuInventoryLevelTitles = new String[]{
            "ruleName",//制度名称
            "ruleNum",//制度编号
            "level",//级别
            "releaseStatus",//发布状态
            "relatedDocuments",//相关文件
            "ruleResDept",//责任部门
            "versionNum",//版本号
            "ReleaseDate",//发布日期
            "termOfValidit"//有效期
    };

    private static final List<InventoryCellStyle> ruleTitleCellStyles;
    private static final List<InventoryCellStyle> ruleContentCellStyles;

    private static final String[] standardInventoryTitles = new String[]{
            "name",//名称
            "content",//内容
            "relatedDocuments"//相关文件
    };
    private static final List<InventoryCellStyle> standardTitleCellStyles;
    private static final List<InventoryCellStyle> standardContentCellStyles;

    private static final String[] riskInventoryTitles = new String[]{
            "riskNum",//风险编号
            "riskText",//风险描述
            "riskGrade",//风险等级
            "basicControl",//企业内部控制基本规范、应用指引和解读相关要求
            "standardizedControl",//标准化控制
            "relatedRule",//相关制度
            "controlTarget",//控制目标
            "controlNum",//控制编号
            "controlActivitySimpleDescription",//控制活动简描述
            "roleName",//角色名称
            "activityNam",//活动名称
            "processName",//流程名称
            "responsibilityDepartment",//责任部门
            "controlMethod",//控制方法
            "controlTypeYF",//控制类型：预防性/发现性
            "whetherControl",//是否为关键控制
            "controlFrequency"//控制频率
    };
    private static final String[] riskInventoryTitlesAddRiskType = new String[]{
            "riskNum",//风险编号
            "riskText",//风险描述
            "riskGrade",//风险等级
            "riskType",//"风险类别",
            "basicControl",//企业内部控制基本规范、应用指引和解读相关要求
            "standardizedControl",//标准化控制
            "relatedRule",//相关制度
            "controlTarget",//控制目标
            "controlNum",//控制编号
            "controlActivitySimpleDescription",//控制活动简描述
            "roleName",//角色名称
            "activityNam",//活动名称
            "processName",//流程名称
            "responsibilityDepartment",//责任部门
            "controlMethod",//控制方法
            "controlTypeYF",//控制类型：预防性/发现性
            "whetherControl",//是否为关键控制
            "controlFrequency"//控制频率
    };
    private static final List<InventoryCellStyle> riskTitleCellStyles;
    private static final List<InventoryCellStyle> riskContentCellStyles;

    private static final String[] fileInventoryTitles = new String[]{
            "documentName",//文件名称
            "documentNumber",//文件编号
            "relatedDocuments",//相关文件
            "responsibilityDepartment",//责任部门
            "personLiable",//责任人
            "fileCreator",//拟稿人
            "createTime",//创建时间
            "updateTimeC"//更新时间

    };

    private static final String[] fileMengNiuInventoryTitles = new String[]{
            "documentName",//文件名称
            "documentNumber",//文件编号
            "responsibilityDepartment",//责任部门
            "fileCreator",//文件创建人
            "createTime",//创建时间
            "updateTimeC",//更新时间
            "relatedDocuments",//相关文件
    };

    private static final List<InventoryCellStyle> fileTitleCellStyles;
    private static final List<InventoryCellStyle> fileContentCellStyles;

    private static final String[] orgInventoryTitles = new String[]{
            "processName",//流程名称
            "processNum",//流程编号
            "orgType",//部门类型
            "keyActivities",//关键活动
            "processKPI",//流程KPI
            "relatedDocuments",//相关文件
            "processResponsiblePerson",//"流程责任人
            "processGuardian",//流程监护人
            "DrafterC",//拟稿人
            "versionNum",//版本号
            "ReleaseDate",//发布日期
            "termOfValidit",//有效期
            "whetherOrNotToOptimizeInTime",//是否及时优化
            "TheNextTimeYouLookAtTheTimeToFinish"//下次审视需完成时间
    };
    private static final List<InventoryCellStyle> orgTitleCellStyles;
    private static final List<InventoryCellStyle> orgContentCellStyles;

    private static final String[] posInventoryTitles = new String[]{
            "post",//岗位
            "personnel",//人员
            "executionRole",//执行角色
            "roleResponsibility",//角色职责
            "process",//流程
            "activity",//活动
            "timeLimitTargetOrStatus",//办理时限(目标/现状)
    };

    private static final String[] posStatisticsInventoryTitles = new String[]{
            "post",//岗位
            "belongDepartment",//责任部门
            "executionRole",//执行角色
            "roleResponsibility",//角色职责
            "process",//流程
            "activity",//活动
            "timeLimitTargetOrStatus",//办理时限(目标/现状)
    };

    private static final String[] roleUserInfoInventoryTitles = new String[]{
            "name",//名称
            "belongDepartment",//所属部门
            "defaultRoleType",//默认角色类型
            "customRoleType",//自定义角色类型
            "flowRes",//流程权限范围
            "fileRes",//文档权限范围
            "standRes",//标准权限范围
            "ruleRes",//制度权限范围
            "riskRes",//风险权限范围
            "termsRes",//术语权限范围
            "orgRes",//组织权限范围
            "posGroupkRes"//岗位组权限范围
    };
    private static final String[] posGroupStatisticsInventoryTitles = new String[]{
            "positionGroup",//岗位组
            "describe",//说明
            "post",//岗位
            "executionRole",//执行角色
            "roleResponsibility",//角色职责
            "process",//流程
            "activity",//活动
            "timeLimitTargetOrStatus",//办理时限(目标/现状)
    };
    private static final String[] reportPermissionsReportsTitles = new String[]{
            "mainDepartment",//责任部门
            "documentName",//名称
            "authOrgName",//部门权限名称
            "authName",//权限
            "authPosName",//岗位权限名称
            "authName",//权限
            "authPosGName",//岗位组权限名称
            "authName"//权限
    };

    private static final String[] chineseEnglishListCountInventoryTitles = new String[]{
            "chineseVersion",//中文版
            "EnglishVersion"//英文版
    };
    private static final List<InventoryCellStyle> posTitleCellStyles;
    private static final List<InventoryCellStyle> posContentCellStyles;

    static {
        // -----流程清单
        // 表头样式
        processTitleCellStyles = new ArrayList<InventoryCellStyle>();
        processTitleCellStyles.addAll(createCellStyles(2, 2));
        processTitleCellStyles.addAll(createCellStyles(1, 1));
        processTitleCellStyles.addAll(createCellStyles(2, 9));
        processTitleCellStyles.addAll(createCellStyles(1, 4));

        processContentCellStyles = new ArrayList<InventoryCellStyle>(getContentListByTitleList(processTitleCellStyles));

        // -----流程系统清单

        processSystemTitleCellStyles = new ArrayList<InventoryCellStyle>();
        processSystemTitleCellStyles.addAll(createCellStyles(1, 1));
        processSystemTitleCellStyles.addAll(createCellStyles(2, 5));

        processSystemContentCellStyles = new ArrayList<InventoryCellStyle>(
                getContentListByTitleList(processSystemTitleCellStyles));

        // -------制度清单
        ruleTitleCellStyles = new ArrayList<InventoryCellStyle>();
        ruleTitleCellStyles.addAll(createCellStyles(2, 9));
        ruleTitleCellStyles.addAll(createCellStyles(1, 1));

        ruleContentCellStyles = new ArrayList<InventoryCellStyle>(getContentListByTitleList(ruleTitleCellStyles));

        // ---------标准清单
        standardTitleCellStyles = new ArrayList<InventoryCellStyle>();
        standardTitleCellStyles.addAll(createCellStyles(2, 1));
        standardTitleCellStyles.addAll(createCellStyles(4, 1));
        standardTitleCellStyles.addAll(createCellStyles(2, 3));

        standardContentCellStyles = new ArrayList<InventoryCellStyle>(getContentListByTitleList(ruleTitleCellStyles));

        // ---------风险清单
        riskTitleCellStyles = new ArrayList<InventoryCellStyle>();
        riskTitleCellStyles.addAll(createCellStyles(2, 1));
        riskTitleCellStyles.addAll(createCellStyles(4, 1));
        riskTitleCellStyles.addAll(createCellStyles(1, 1));

        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.isShowRiskType)) {
            riskTitleCellStyles.addAll(createCellStyles(1, 1));
        }
        riskTitleCellStyles.addAll(createCellStyles(4, 2));
        riskTitleCellStyles.addAll(createCellStyles(4, 2));
        riskTitleCellStyles.addAll(createCellStyles(2, 8));
        riskTitleCellStyles.addAll(createCellStyles(1, 4));

        riskContentCellStyles = new ArrayList<InventoryCellStyle>(getContentListByTitleList(riskTitleCellStyles));

        // ---------文件清单
        fileTitleCellStyles = new ArrayList<InventoryCellStyle>();
        fileTitleCellStyles.addAll(createCellStyles(2, 4));
        fileTitleCellStyles.addAll(createCellStyles(1, 2));
        fileTitleCellStyles.addAll(createCellStyles(2, 6));

        fileContentCellStyles = new ArrayList<InventoryCellStyle>(getContentListByTitleList(fileTitleCellStyles));

        //------组织清单
        orgTitleCellStyles = new ArrayList<InventoryCellStyle>();
        orgTitleCellStyles.addAll(createCellStyles(2, 12));
        orgTitleCellStyles.addAll(createCellStyles(1, 4));

        orgContentCellStyles = new ArrayList<InventoryCellStyle>(getContentListByTitleList(orgTitleCellStyles));

        //------岗位清单
        posTitleCellStyles = new ArrayList<InventoryCellStyle>();
        posTitleCellStyles.addAll(createCellStyles(2, 7));

        posContentCellStyles = new ArrayList<InventoryCellStyle>(getContentListByTitleList(posTitleCellStyles));


    }

    public static List<String> getProcessTitles() {
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.securityLevel)) {
            return Collections.unmodifiableList(getInternationalizationValue(processInventoryLevelTitles));
        }
        return Collections.unmodifiableList(getInternationalizationValue(processInventoryTitles));
    }

    public static List<String> getProcessMengNiuTitles() {
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.securityLevel)) {
            return Collections.unmodifiableList(getInternationalizationValue(processMengNiuInventoryLevelTitles));
        }
        return Collections.unmodifiableList(getInternationalizationValue(processMengNiuInventoryLevelTitles));
    }

    public static List<String> getProcessMapTitles() {
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.securityLevel)) {
            return Collections.unmodifiableList(getInternationalizationValue(processMapInventoryTitles));
        }
        return Collections.unmodifiableList(getInternationalizationValue(processMapInventoryTitles));
    }

    private static Collection<InventoryCellStyle> getContentListByTitleList(List<InventoryCellStyle> titles) {

        List<InventoryCellStyle> contents = new ArrayList<InventoryCellStyle>();
        InventoryCellStyle content = null;
        for (InventoryCellStyle title : titles) {
            content = new InventoryCellStyle();
            contents.add(content);
            if (title.getType() == 2 || title.getType() == 4) {
                content.setAlign(Align.left);
            }
        }

        return contents;
    }

    public static List<String> getProcessSystemTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(processSystemInventoryTitles));
    }

    public static List<String> getRuleTitles() {
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.ruleScurityLevel)) {
            return Collections.unmodifiableList(getInternationalizationConfigValue(ruleInventoryLevelTitles));
        }
        return Collections.unmodifiableList(getInternationalizationConfigValue(ruleInventoryTitles));
    }

    public static List<String> getRuleMengNiuTitles() {
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.ruleScurityLevel)) {
            return Collections.unmodifiableList(getInternationalizationConfigValue(ruleMengNiuInventoryLevelTitles));
        }
        return Collections.unmodifiableList(getInternationalizationConfigValue(ruleMengNiuInventoryLevelTitles));
    }


    public static List<String> getStandardTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(standardInventoryTitles));
    }

    public static List<InventoryCellStyle> createCellStyles(int type, int num) {
        if (num <= 0) {
            throw new IllegalArgumentException();
        }
        List<InventoryCellStyle> styles = new ArrayList<InventoryCellStyle>();
        for (int i = 0; i < num; i++) {
            styles.add(new InventoryCellStyle(type));
        }
        return styles;
    }

    public static List<String> getRiskTitles() {
        if (ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.isShowRiskType)) {
            return Collections.unmodifiableList(getInternationalizationValue(riskInventoryTitlesAddRiskType));
        }
        return Collections.unmodifiableList(getInternationalizationValue(riskInventoryTitles));
    }

    public static List<String> getFileTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(fileInventoryTitles));
    }

    public static List<String> getMengNiuFileTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(fileMengNiuInventoryTitles));
    }

    public static List<String> getOrgTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(orgInventoryTitles));
    }

    public static List<String> getPosTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(posInventoryTitles));
    }

    public static List<String> getPosStatisticsTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(posStatisticsInventoryTitles));
    }

    public static List<String> getPosGroupStatisticsTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(posGroupStatisticsInventoryTitles));
    }

    public static List<String> reportPermissionsReportsTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(reportPermissionsReportsTitles));
    }

    public static List<String> getChineseEnglishListCountInventoryTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(chineseEnglishListCountInventoryTitles));
    }


    public static List<String> getRoleUserInfoTitles() {
        return Collections.unmodifiableList(getInternationalizationValue(roleUserInfoInventoryTitles));
    }

    /**
     * 清单 列表获取国际化内容
     *
     * @param key
     * @return
     */
    private static List<String> getInternationalizationValue(String... key) {
        List<String> keyList = Arrays.asList(key); //获取国际化的key 集合
        List<String> valList = new ArrayList<String>(); //国际化文本内容
        keyList.forEach(str -> valList.add(JecnProperties.getValue(str)));
        return valList;
    }


    /**
     * s
     * 清单 列表获取国际化内容 数据库中
     *
     * @param key
     * @return
     */
    private static List<String> getInternationalizationConfigValue(String... key) {
        List<String> keyList = Arrays.asList(key); //获取国际化的key 集合
        List<String> valList = new ArrayList<String>(); //国际化文本内容
        keyList.forEach(str -> valList.add((ConfigUtils.getName(str) == null || "".equals(ConfigUtils.getName(str))) ? JecnProperties.getValue(str) : ConfigUtils.getName(str)));
        return valList;
    }
}
