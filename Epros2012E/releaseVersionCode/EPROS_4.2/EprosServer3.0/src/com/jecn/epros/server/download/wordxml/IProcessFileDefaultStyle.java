package com.jecn.epros.server.download.wordxml;

import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.table.TableBean;

public interface IProcessFileDefaultStyle {

	/**
	 * 默认表格内容样式
	 * 
	 * @param tblContentStyle
	 */
	ParagraphBean tblContentStyle();

	/**
	 * 默认表格样式
	 * 
	 * @param tblStyle
	 */
	TableBean tblStyle();

	/**
	 * 默认表格标题样式
	 * 
	 * @param tblTitileStyle
	 */
	ParagraphBean tblTitleStyle();

	/**
	 * 默认文本项样式
	 * 
	 * @param textContentStyle
	 */
	ParagraphBean textContentStyle();

	/**
	 * 默认文本项标题样式
	 * 
	 * @param textTitleStyle
	 */
	ParagraphBean textTitleStyle();

}
