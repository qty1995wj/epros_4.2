package com.jecn.epros.service.standard.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.InventoryCell;
import com.jecn.epros.domain.InventoryTable;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.file.FileBean;
import com.jecn.epros.domain.file.FileDirCommonBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.process.relatedFile.RelatedRule;
import com.jecn.epros.domain.report.InventoryBaseInfo;
import com.jecn.epros.domain.standard.*;
import com.jecn.epros.mapper.FileMapper;
import com.jecn.epros.mapper.InventoryMapper;
import com.jecn.epros.mapper.StandardMapper;
import com.jecn.epros.service.ServiceExpandTreeData;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.TreeNodeUtils.TreeType;
import com.jecn.epros.service.standard.StandardService;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Params;
import com.jecn.epros.util.Utils;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class StandardServiceImpl implements StandardService {

    @Autowired
    private StandardMapper standardMapper;

    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private InventoryMapper inventoryMapper;

    @Override
    public List<TreeNode> findChildStandards(Map<String, Object> map) {
        map.put("typeAuth", TYPEAUTH.STANDARD);
        List<TreeNode> treeNodes = standardMapper.findChildStandards(map);
        treeNodes = ServiceExpandTreeData.findExpandNodesByDataList(treeNodes, TreeType.standard);
        return TreeNodeUtils.setNodeType(treeNodes, TreeType.standard);
    }

    @Override
    public StandardBaseInfoBean findBaseInfoBean(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        StandardBean standardBean = standardMapper.getStandardBean(id);
        if (standardBean == null) {
            return null;
        }
        // 标准详情
        StandardBaseInfoBean standardBaseInfoBean = new StandardBaseInfoBean();
        int standardType = standardBean.getStanType().intValue();
        standardBaseInfoBean.setStanType(standardType);
        switch (standardType) {
            case 1:
                //查询文件是否存在
                Params params = new Params();
                params.add("fileId",standardBean.getRelationId());
                params.add("isPub",true);
                String pub  ="1";
                if(standardBean.getRelationId()!= null){
                    pub  = fileMapper.judgmentFileExists(params.toMap());
                }
                switch (pub){
                    case "1":
                        standardBaseInfoBean.setName(standardBean.getCriterionClassName()+ JecnProperties.getValue("falseDeletion"));
                        standardBaseInfoBean.setDelState("2");
                        break;
                    case "2":
                        standardBaseInfoBean.setName(standardBean.getCriterionClassName()+JecnProperties.getValue("repealed"));
                        standardBaseInfoBean.setDelState("1");
                        break;
                    case "0":
                        standardBaseInfoBean.setName(standardBean.getCriterionClassName());
                        standardBaseInfoBean.setDelState("0");
                        break;
                }
                standardBaseInfoBean.setFileNum(standardBean.getDocId());
                standardBaseInfoBean.setFileId(standardBean.getRelationId());
                standardBaseInfoBean.setSecretLevel(standardBean.getIsPublic());
                break;
            case 4:
                standardBaseInfoBean.setName(standardBean.getCriterionClassName());
                standardBaseInfoBean.setIsProfile(standardBean.getIsproFile());
                standardBaseInfoBean.setStanContent(standardBean.getStanContent());
                break;
            case 5:
                standardBaseInfoBean.setName(standardBean.getCriterionClassName());
                StandardBean pStandardBean = standardMapper.getStandardBean(standardBean.getPreCriterionClassId());
                standardBaseInfoBean.setClauseName(pStandardBean.getCriterionClassName());
                standardBaseInfoBean.setStanContent(Common.replaceWarp(standardBean.getStanContent()));
                break;
            default:
                break;
        }
        standardBaseInfoBean.setNote(standardBean.getNote());
        // 标准相关附件：目前就条款有附件
        List<StandardFileContent> fileContents = standardMapper.getFileContents(standardBean.getCriterionClassId());
        standardBaseInfoBean.setFileContents(fileContents);
        return standardBaseInfoBean;
    }

    @Override
    public StandardRelatedFileBean findRelatedFiles(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        // 相关流程
        List<RelatedFlow> relatedFlows = standardMapper.findRelatedFlows(id);
        // 相关制度
        List<RelatedRule> relatedRules = standardMapper.findRelatedRules(id);
        StandardRelatedFileBean standardRelatedFileBean = new StandardRelatedFileBean();
        standardRelatedFileBean.setRelatedProcesses(relatedFlows);
        standardRelatedFileBean.setRelatedRules(relatedRules);
        return standardRelatedFileBean;
    }

    @Override
    public List<SearchStandardResultBean> searchStandard(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "t.criterion_class_id");
        map.put("typeAuth", TYPEAUTH.STANDARD);
        List<SearchStandardResultBean> list = standardMapper.searchStandard(map);
        // 根据标准获取标准父节点
        Set<Long> ids = getIdsByStandardList(list);
        if (ids.isEmpty()) {
            return list;
        }
        Map<String, Object> paramMap = new HashedMap();
        paramMap.put("ids", ids);
        List<BaseBean> files = standardMapper.getRelatedParentNameByIds(paramMap);
        setParentNameByList(files, list);
        return list;
    }

    private void setParentNameByList(List<BaseBean> files, List<SearchStandardResultBean> list) {
        for (SearchStandardResultBean resultBean : list) {
            String parentPathName = "";
            for (BaseBean file : files) {
                if (resultBean.getId().intValue() == file.getId().intValue()) {
                    if (StringUtils.isBlank(parentPathName)) {
                        parentPathName = file.getName();
                    } else {
                        parentPathName += "/" + file.getName();
                    }
                }
            }
            resultBean.setParentName(parentPathName);
        }
    }

    private Set<Long> getIdsByStandardList(List<SearchStandardResultBean> list) {
        Set<Long> ids = new HashSet<>();
        for (SearchStandardResultBean resultBean : list) {
            ids.add(resultBean.getId());
        }
        return ids;
    }

    @Override
    public int searchStandardTotal(Map<String, Object> map) {
        return standardMapper.searchStandardTotal(map);
    }

    @Override
    public Inventory getInventory(Map<String, Object> paramMap) {
        Boolean isPaging = (Boolean) paramMap.get("isPaging");
        List<StandardInfoBean> standardAndDirs = new ArrayList<>();
        if (isPaging) {
            // 查询分页的流程
            Paging paging = (Paging) paramMap.get("paging");
            // 通过view_sort分页查询流程
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "view_sort");
            List<String> paths = inventoryMapper.fetchPaths(paramMap);
            // 获得排序之后最终要显示标准
            if (paths != null && paths.size() > 0) {
                Set<String> dirIds = new HashSet<>();
                Set<String> standardIds = new HashSet<>();
                for (String path : paths) {
                    String[] ids = path.split("-");
                    int lastIndex = ids.length - 1;
                    for (int i = 0; i < ids.length; i++) {
                        if (i == lastIndex) {
                            standardIds.add(ids[i]);
                        } else {
                            dirIds.add(ids[i]);
                        }
                    }
                }
                // 将查询到的流程和他的所有父节点都集中处理为(1,2,3)类型
                String ids = Utils.convertPathCollectToSqlIn(paths);
                paramMap.put("ids", ids);
                // 从数据库获得清单项的详细信息并排序
                List<StandardInfoBean> tempStandardAndDirs = standardMapper.fetchInventory(paramMap);
                for (StandardInfoBean standard : tempStandardAndDirs) {
                    if (standard.getStandType() != 0 && !standardIds.contains(String.valueOf(standard.getStandardId()))) {
                        continue;
                    } else {
                        standardAndDirs.add(standard);
                    }
                }
            }
        } else {
            List<StandardInfoBean> dirs = standardMapper.fetchInventoryParent(paramMap).stream().filter(standard -> standard.getStandType() == 0).collect(Collectors.toList());
            standardAndDirs.addAll(dirs);
            // 获得制度数据,经过view_sort排序的数据
            standardAndDirs.addAll(standardMapper.fetchInventory(paramMap));
        }

        InventoryBaseInfo baseInfo = inventoryMapper.getInventoryBaseInfo(paramMap);
        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setMaxLevel(baseInfo.getMaxLevel());
        inventory.setTotalNum(baseInfo.getTotal());

        Map<Long, FileDirCommonBean> dirs = new HashMap<>();
        List<StandardInfoBean> standards = new ArrayList<>();
        for (StandardInfoBean bean : standardAndDirs) {
            if (bean.getStandType() == 0) {
                dirs.put(bean.getStandardId(), new FileDirCommonBean(bean.getStandardId(), bean.getStandName(), "", bean.getLevel()));
            } else {
                standards.add(bean);
            }
        }

        // 获取清单数据实体
        InventoryTable inventoryTable = getInventoryTable(isPaging, dirs, standards, inventory.getStartLevel(), inventory.getMaxLevel(), paramMap);
        inventory.setData(inventoryTable);
        inventory.setStyleMap(InventoryStyleManager.getStandardInventoryStyles(inventoryTable.getTitileContents()));

        return inventory;
    }

    private InventoryTable getInventoryTable(boolean isPaging, Map<Long, FileDirCommonBean> dirs,
                                             List<StandardInfoBean> rules, int minLv, int maxLv, Map<String, Object> paramMap) {
        InventoryTable data = new InventoryTable();
        // 添加标题行的数据
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(minLv, maxLv, InventoryStyleManager.getStandardTitles()));

        for (StandardInfoBean standard : rules) {
            List<InventoryCell> contentRow = new ArrayList<InventoryCell>();
            // 添加自动生成的级别数据
            contentRow.addAll(ExcelUtils.getLevelsNoNum(dirs, minLv, maxLv, standard.getPath()));
            // 添加流程的相关数据
            contentRow.addAll(getContentCells(standard));
            data.addContentRow(contentRow);
        }
        return data;
    }


    private List<InventoryCell> getContentCells(StandardInfoBean standard) {

        List<InventoryCell> contentCells = new ArrayList<InventoryCell>();

        InventoryCell cell = new InventoryCell();
//        cell.setText(standard.getStandName());
//       (0是目录，1文件标准，2流程标准 3流程地图标准4标准条款5条款要求)
        ResourceType type = null;
        List<LinkResource> links = new ArrayList<LinkResource>();
        Long rid = null;
        if (standard.getStandType() == 1) {
            type = ResourceType.STANDARD;
            rid = standard.getStandardId();
        } else if (standard.getStandType() == 2) {
            type = ResourceType.PROCESS;
            rid = standard.getRelatedId();
        } else if (standard.getStandType() == 3) {
            type = ResourceType.PROCESS_MAP;
            rid = standard.getRelatedId();
        } else if (standard.getStandType() == 4) {
            type = ResourceType.STANDARD;
            rid = standard.getStandardId();
        } else if (standard.getStandType() == 5) {
            type = ResourceType.STANDARD;
            rid = standard.getStandardId();
        }
        links.add(new LinkResource(rid, standard.getStandName(), type,TreeNodeUtils.NodeType.inventory));
        cell.setLinks(links);
        contentCells.add(cell);

        cell = new InventoryCell();
        cell.setText(standard.getStandContent());
        contentCells.add(cell);

        List<StandardRelatePRFBean> listStandardRefBean = standard.getListStandardRefBean();
        List<LinkResource> relatedProcesses = new ArrayList<LinkResource>();
        List<LinkResource> relatedActivitys = new ArrayList<LinkResource>();
        List<LinkResource> relatedStandards = new ArrayList<LinkResource>();

        /** 1:流程，2：制度,3:活动 */
        for (StandardRelatePRFBean bean : listStandardRefBean) {
            if (bean.getRelatedType() == 1) {
                type = ResourceType.PROCESS;
                relatedProcesses.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type,TreeNodeUtils.NodeType.process));
            } else if (bean.getRelatedType() == 2) {
                type = ResourceType.RULE;
                relatedStandards.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type,TreeNodeUtils.NodeType.rule));
            } else if (bean.getRelatedType() == 3) {
                type = ResourceType.ACTIVITY;
                relatedActivitys.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type,TreeNodeUtils.NodeType.activity));
            }
        }
        List<LinkResource> related = new ArrayList<>();
        related.addAll(relatedProcesses);
        related.addAll(relatedStandards);
        related.addAll(relatedActivitys);
        cell = new InventoryCell();
        cell.setLinks(related);
        contentCells.add(cell);

        return contentCells;
    }

}
