package epros.designer.gui.process.mode;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JPopupMenu;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.service.process.JecnProcess;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 流程模板管理树 包括：流程模板，流程文件模板
 * 
 * @author 2012-06-26
 * 
 */
public class FlowModelHighEfficiencyTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(FlowModelHighEfficiencyTree.class);

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new FlowModelMagTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			// 根节点"流程模板管理"
			JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.modeRoot, JecnProperties
					.getValue("processModeManage"));
			// 流程树节点
			JecnTreeNode flowModelMa = JecnTreeCommon.createTreeRoot(TreeNodeType.processModeRoot, JecnProperties
					.getValue("processMode"));
			// 流程文件节点
			// JecnTreeNode
			// flowFileMa=JecnTreeCommon.createTreeRoot(TreeNodeType.processModeFileRoot,JecnProperties.getValue("processModeFile"));
			rootNode.add(flowModelMa);
			// rootNode.add(flowFileMa);
			return new JecnTreeModel(rootNode);
		} catch (Exception e) {
			log.error("FlowModelHighEfficiencyTree getTreeModel is error", e);
			return null;
		}
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		// 击左键
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {

			if (evt.getClickCount() == 2) {
				TreePath[] treePath = this.getSelectionPaths();
				if (treePath != null && treePath.length == 1) {
					JecnTreeNode node = (JecnTreeNode) treePath[0].getLastPathComponent();
					JecnTreeCommon.autoExpandNode(this, node);
					try {
						JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
						if (!JecnDesignerCommon.isAdmin()) {
							return;
						}
						// 流程地图打开
						if (jecnTreeBean.getTreeNodeType() == TreeNodeType.processMapMode) {
							// 判断是否已打开
							if (JecnProcess.isOpen(jecnTreeBean.getId().intValue(), MapType.totalMap)) {
								return;
							}
							ProcessOpenMapData processMapData = ConnectionPool.getProcessModeAction()
									.getProcessMapModeData(jecnTreeBean.getId());
							JecnProcess.openJecnProcessTotalMap(processMapData, false);
							// 更新table标签图标
							JecnDrawMainPanel.getMainPanel().getWorkflow().getTabbedTitlePanel().setTabIcon(
									(Icon) JecnTreeRenderer.class.getField(jecnTreeBean.getTreeNodeType().toString())
											.get(null));
							// 流程打开
						} else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.processMode) {
							// 判断是否已打开
							if (JecnProcess.isOpen(jecnTreeBean.getId().intValue(), MapType.partMap)) {
								return;
							}
							ProcessOpenData processOpenData = ConnectionPool.getProcessModeAction().getProcessModeData(
									jecnTreeBean.getId());

							JecnProcess.openJecnProcessMap(processOpenData, false);
							// 更新table标签图标
							JecnDrawMainPanel.getMainPanel().getWorkflow().getTabbedTitlePanel().setTabIcon(
									(Icon) JecnTreeRenderer.class.getField(jecnTreeBean.getTreeNodeType().toString())
											.get(null));
						}

					} catch (Exception e) {
						log.error("FlowModelHighEfficiencyTree jTreeMousePressed is error", e);
						JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
					}
				}

			}

		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = this.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean().getTreeNodeType();
				// 保存去掉不同类型后的节点
				List<JecnTreeNode> selectNodes = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean().getTreeNodeType())) {
							selectNodes.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}
				if (selectNodes.size() > 0) {
					JPopupMenu menu = null;
					if (selectNodes.size() == 1) {
						// 选择了一个节点
						menu = getPopupMenu(selectNodes, 0);
						this.setSelectionPath(listTreePaths.get(0));
						if (menu != null) {
							menu.show(evt.getComponent(), evt.getX(), evt.getY());
							JecnTreeCommon.autoExpandNode(this, selectNodes.get(0));
						}

						return;
					}
					// 多选择时去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(selectNodes, this);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							menu = getPopupMenu(selectNodes, 0);
							JecnTreeCommon.autoExpandNode(this, selectNodes.get(0));
							this.setSelectionPath(treePaths[0]);
						} else {
							menu = getPopupMenu(selectNodes, 1);
							this.setSelectionPaths(treePaths);
						}
						if (menu != null) {
							menu.show(evt.getComponent(), evt.getX(), evt.getY());
						}

					}
				}

			}
		}

	}

	private JPopupMenu getPopupMenu(List<JecnTreeNode> selectNodes, int selectMutil) {
		JPopupMenu menu = null;
		switch (selectNodes.get(0).getJecnTreeBean().getTreeNodeType()) {
		case processModeRoot: // 流程模板根节点
			menu = new FlowModelTreeMenu(this, selectNodes, selectMutil);
			break;
		case processMapMode: // 流程地图模板节点
			menu = new FlowModelTreeMenu(this, selectNodes, selectMutil);
			break;
		case processMode:// 流程图模板节点
			menu = new FlowModelTreeMenu(this, selectNodes, selectMutil);
			break;
		case processModeFileRoot:// 流程模板文件根节点
			break;
		case processModeFileDir: // 流程模板文件目录
			break;
		case processModeFile: // 流程模板文件
			break;
		default:
			break;
		}
		return menu;
	}

}
