package epros.designer.gui.system.config.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;

import epros.designer.gui.system.config.ui.property.type.JecnAddItemPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 点击加入按钮弹出的确认对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAddItemJDialog extends JecnDialog {
	/** 配置界面 */
	private JecnAbtractBaseConfigDialog dialog = null;
	/** 主面板 */
	private JecnAddItemPanel mainPanel = null;

	public JecnAddItemJDialog(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		this.dialog = dialog;

		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	protected void initComponents() {
		// 主面板
		mainPanel = new JecnAddItemPanel(dialog, this);

		// 大小
		Dimension size = new Dimension(390, 415);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		// 模态
		this.setModal(true);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 关闭处理模式
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//
		this.setLocationRelativeTo(dialog);

		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
	}

	public JecnAddItemPanel getMainPanel() {
		return mainPanel;
	}

}
