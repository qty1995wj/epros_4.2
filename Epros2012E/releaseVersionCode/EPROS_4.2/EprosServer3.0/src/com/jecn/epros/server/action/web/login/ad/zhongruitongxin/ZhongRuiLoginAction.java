package com.jecn.epros.server.action.web.login.ad.zhongruitongxin;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class ZhongRuiLoginAction extends JecnAbstractADLoginAction {

	static {
		ZhongRuiAfterItem.start();
	}

	public ZhongRuiLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		if ("admin".equals(loginAction.getLoginName())) {
			return loginAction.userLoginGen(true);
		}

		String ticket = loginAction.getRequest().getParameter("ticket");
		// 不存在ticket说明未向中睿接口发起请求
		if (StringUtils.isBlank(ticket)) {
			return redirectToZhongRuiTicketService();
		}
		// 获取用户登录名
		loginAction.setLoginName(requestUserLoginNameFromZhongRui(ticket));
		log.info("获取登录名成功！ loginName = " + loginAction.getLoginName());
		// 获取跳转类型
		String forward = loginAction.getRequest().getParameter("epros_forward");
		if ("processView".equals(forward)) {// 直接打开流程图
			return loginToProcess();
		}
		return loginAction.userLoginGen(false);
	}

	private String loginToProcess() {
		String ret = loginAction.userLoginGen(false);
		String redirectURL = null;
//		log.info("登录返回值ret = " + ret);
		if (LoginAction.MAIN.equals(ret) || LoginAction.SUCCESS.equals(ret)) {// 登录成功
			String flowId = loginAction.getRequest().getParameter("flowId");
			String type = loginAction.getRequest().getParameter("type");
			redirectURL = ZhongRuiAfterItem.EPROS_URL + "process.action?type=" + type + "&flowId=" + flowId;
//			log.info("请求成功后登录流程地址：redirectURL = " + redirectURL);
			loginAction.setCustomRedirectURL(redirectURL);
			return LoginAction.REDIRECT;
		}
		return ret;
	}

	/**
	 * 重定向至中睿Ticket服务
	 * 
	 * @return
	 */
	private String redirectToZhongRuiTicketService() {
		String redirectURL = ZhongRuiAfterItem.TICKET_URL + "?" + "urlpath="
				+ ZhongRuiAfterItem.EPROS_LOGIN_URL;
		String forwardTo = loginAction.getRequest().getParameter(
				"epros_forward");
		if (StringUtils.isNotBlank(forwardTo)) {
			redirectURL += "&epros_forward=" + forwardTo;
		}
		if("processView".equals(forwardTo)){
			redirectURL += "&flowId=" + loginAction.getRequest().getParameter("flowId");
			redirectURL += "&type=" + loginAction.getRequest().getParameter("type");
		}
//		log.info("二次请求 redirectURL = " + redirectURL);
		
		loginAction.setCustomRedirectURL(redirectURL);
		return LoginAction.REDIRECT;
	}

	/**
	 * 访问中睿接口获取用户登录名
	 * 
	 * @param ticket
	 * @return
	 * @throws Exception
	 */
	private String requestUserLoginNameFromZhongRui(String ticket) {
		String xml_url = ZhongRuiAfterItem.USERINFO_XML_URL + "?" + "Token="
				+ ticket;
		GetMethod get = new GetMethod(xml_url);
		InputStream in = null;
		String loginName = null;
		try {
			HttpClient client = new HttpClient();
			client.executeMethod(get);
			in = get.getResponseBodyAsStream();
			loginName = getUserLoginNameByXmlStream(in);
		} catch (Exception e) {
			log.error("获取用户信息时出现异常", e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				log.error("释放IO资源异常", e);
			}
			get.releaseConnection();
		}
		return loginName;
	}

	/**
	 * 获取xml文档中的用户登录名信息
	 * 
	 * @param userInfoXml
	 * @return
	 * @throws Exception
	 */
	private String getUserLoginNameByXmlStream(InputStream userInfoXml)
			throws Exception {
		if (userInfoXml == null) {
			throw new Exception("用户信息XML流数据为Null");
		}
		Document doc = new SAXReader().read(userInfoXml);
		Element loginNameElement = doc.getRootElement().element(
				ZhongRuiAfterItem.USER_LOGINNAME_KEY);
		if (loginNameElement == null) {
			log.error("xml中未找到指定的元素，xml内容如下：\n" + doc.asXML());
			throw new Exception("XMl中不存在" + "" + "元素");
		}
		return loginNameElement.getText();
	}
}
