package com.jecn.epros.domain.standard;

import com.jecn.epros.util.HttpUtil;

/**
 * Created by xiaohu on 2017/6/27.
 */
public class StandardFileContent {
    private Long id;
    private String name;

    public String getHrefURL() {
        return HttpUtil.getDownLoadFileHttp(id, true, "standardFile");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
