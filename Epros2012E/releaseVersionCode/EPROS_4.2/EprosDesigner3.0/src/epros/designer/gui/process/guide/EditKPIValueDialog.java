package epros.designer.gui.process.guide;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnTextField;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.JecnTextFieldAndCalendarPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程KPI值
 * 
 * @author 2012-07-10
 * 
 */
public abstract class EditKPIValueDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(EditKPIValueDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息显示Panel */
	private JecnPanel infoPanel = new JecnPanel();

	/** 按钮 Panel */
	private JecnPanel butPanel = new JecnPanel();

	/** KPI名称Lab */
	private JLabel kpiNameLab = new JLabel(JecnProperties.getValue("kpiNameC"));

	/** KPI名称Field */
	private JecnTextField kpiNameField = new JecnTextField();

	/** KPI纵坐标Lab */
	private JLabel longitudinalLab = new JLabel(JecnProperties
			.getValue("kpiOrdinateC"));

	/** KPI纵坐标Field */
	protected JecnTextField longitudinalField = new JecnTextField();

	// ===================不同KPI类型， 不同的横坐标值==============
	/** 横坐标Panel */
	private JecnPanel transversePanel = new JecnPanel();

	/** KPI横坐标Lab */
	private JLabel transverseLab = new JLabel(JecnProperties
			.getValue("kpiAbscissaC"));

	/** KPI横坐标Field 日期选择 */
	protected JecnTextFieldAndCalendarPanel transverseDateField = new JecnTextFieldAndCalendarPanel();

	// ========单位名称===========================================
	/** KPI纵坐标单位名称Lab */
	private JLabel unitLongitudinalLab = new JLabel(JecnProperties
			.getValue("unitNameC"));

	/** KPI纵坐标单位名称Field */
	protected JecnTextField unitLongitudinalField = new JecnTextField();

	/** KPI横坐标单位名称Lab */
	private JLabel uniTransverseLab = new JLabel(JecnProperties
			.getValue("unitNameC"));

	/** KPI横坐标单位名称Field */
	protected JecnTextField uniTransverseField = new JecnTextField();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties
			.getValue("cancelBtn"));

	/** 确定提示信息 */
	protected JLabel okShowInfo = new JLabel();

	/** 必填提示符 */
	private JLabel oneLab = new JLabel(JecnProperties.getValue("required"));
	private JLabel twoLab = new JLabel(JecnProperties.getValue("required"));
	private JLabel treeLab = new JLabel(JecnProperties.getValue("required"));
	private JLabel fourLab = new JLabel(JecnProperties.getValue("required"));
	/** 提示 */
	private VerifyLabel longitudinalVerf = new VerifyLabel();
	private VerifyLabel transverseVerf = new VerifyLabel();
	/** 横坐标Panel下的组件 */
	private JLabel longOneLab = new JLabel();
	private JLabel longtwoLab = new JLabel();

	protected JecnTextField longOneField = new JecnTextField();
	protected JecnTextField longtwoField = new JecnTextField();

	private JLabel yearLab = new JLabel(JecnProperties.getValue("as2012"));
	private JLabel otherLab = new JLabel(JecnProperties.getValue("as1"));

	// 年类型
	protected JecnTextField yearField = new JecnTextField();

	private JecnTextField flagField = new JecnTextField();

	/** 设置大小 */
	Dimension dimension = null;

	private Long flagNum;

	public Long getFlagNum() {
		return flagNum;
	}

	public void setFlagNum(Long flagNum) {
		this.flagNum = flagNum;
		this.flagField.setText(String.valueOf(flagNum));
		this.kpiNameField.setText(getKPIName());
		this.unitLongitudinalField.setText(getLongField());
		this.uniTransverseField.setText(getTransverseField());
		initLayout();
	}

	public EditKPIValueDialog() {
		this.setTitle(getKPIValueTitle());
		this.setSize(600, 215);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setResizable(true);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		butPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// dimension = new Dimension(350, 20);

		kpiNameField.setEditable(false);
		unitLongitudinalField.setEditable(false);
		uniTransverseField.setEditable(false);

		kpiNameField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		unitLongitudinalField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		uniTransverseField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		
		oneLab.setForeground(Color.red);
		twoLab.setForeground(Color.red);
		treeLab.setForeground(Color.red);
		fourLab.setForeground(Color.red);
		longitudinalVerf.setForeground(Color.red);
		transverseVerf.setForeground(Color.red);

		okShowInfo.setForeground(Color.red);

		// this.unitLongitudinalField.setText(getLongField());
		// this.uniTransverseField.setText(getTransverseField());

		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(7, 2, 7, 2);
		Insets insetu = new Insets(7, 2, 0, 2);
		Insets insetd = new Insets(0, 2, 7, 2);
		Insets insetl = new Insets(0, 0, 0, 0);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 名称
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(kpiNameLab, c);
		c = new GridBagConstraints(1, 0, 4, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		infoPanel.add(kpiNameField, c);
		// 纵坐标
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetu, 0, 0);
		infoPanel.add(longitudinalLab, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insetu,
				0, 0);
		infoPanel.add(longitudinalField, c);
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetu, 0, 0);
		infoPanel.add(oneLab, c);
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetu, 0, 0);
		infoPanel.add(unitLongitudinalLab, c);
		c = new GridBagConstraints(4, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insetu,
				0, 0);
		infoPanel.add(unitLongitudinalField, c);
		// 提示
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insetl,
				0, 0);
		infoPanel.add(longitudinalVerf, c);
		// 横坐标
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insetl,
				0, 0);
		infoPanel.add(transverseLab, c);
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetl, 0, 0);
		infoPanel.add(transversePanel, c);
		transversePanel.setLayout(new GridBagLayout());
		// transversePanel.setBorder(BorderFactory.createTitledBorder(""));
		// /===========================start
		// 横坐标transversePanel======================
		String strFlag = flagField.getText();
		if (strFlag != null && !"".equals(strFlag)) {

			if (strFlag.equals("0")) {
				// 天
				// dimension = new Dimension(150, 20);
				// transversePanel.setPreferredSize(dimension);
				// transversePanel.setMaximumSize(dimension);
				// transversePanel.setMinimumSize(dimension);
				// transverseDateField
				c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, insetl, 0, 0);
				transversePanel.add(transverseDateField, c);
			} else if (strFlag.equals("4")) {
				// 年
				// dimension = new Dimension(150, 20);
				// transversePanel.setPreferredSize(dimension);
				// transversePanel.setMaximumSize(dimension);
				// transversePanel.setMinimumSize(dimension);
				c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, insetl, 0, 0);
				transversePanel.add(yearField, c);
			} else {
				// dimension = new Dimension(150, 37);
				// transversePanel.setPreferredSize(dimension);
				// transversePanel.setMaximumSize(dimension);
				// transversePanel.setMinimumSize(dimension);
				dimension = new Dimension(46, 20);

				longOneField.setPreferredSize(dimension);
				longOneField.setMaximumSize(dimension);
				longOneField.setMinimumSize(dimension);

				longtwoField.setPreferredSize(dimension);
				longtwoField.setMaximumSize(dimension);
				longtwoField.setMinimumSize(dimension);
				if (strFlag.equals("1")) {
					// 周
					longOneLab.setText(JecnProperties.getValue("yearC"));
					longtwoLab.setText(JecnProperties.getValue("weekC"));

				} else if (strFlag.equals("2")) {
					// 月
					longOneLab.setText(JecnProperties.getValue("yearC"));
					longtwoLab.setText(JecnProperties.getValue("monthC"));
				} else if (strFlag.equals("3")) {
					// 月
					longOneLab.setText(JecnProperties.getValue("yearC"));
					longtwoLab.setText(JecnProperties.getValue("quarterC"));
				}
				// longOneLab
				c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.NONE,
						insetl, 0, 0);
				transversePanel.add(longOneLab, c);
				c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
						insetl, 0, 0);
				transversePanel.add(longOneField, c);
				c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.NONE,
						insetl, 0, 0);
				transversePanel.add(treeLab, c);
				c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.NONE,
						insetl, 0, 0);
				transversePanel.add(longtwoLab, c);
				c = new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
						insetl, 0, 0);
				transversePanel.add(longtwoField, c);
				// yearLab
				c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.NONE,
						insetl, 0, 0);
				transversePanel.add(yearLab, c);
				c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.NONE,
						insetl, 0, 0);
				transversePanel.add(otherLab, c);
			}
		}

		// /===========================横坐标transversePanel======================
		c = new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insetl,
				0, 0);
		infoPanel.add(twoLab, c);
		c = new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insetl,
				0, 0);
		infoPanel.add(uniTransverseLab, c);
		c = new GridBagConstraints(4, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetl, 0, 0);
		infoPanel.add(uniTransverseField, c);

		c = new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetl, 0, 0);
		infoPanel.add(transverseVerf, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.EAST, GridBagConstraints.BOTH, insetd, 0, 0);
		mainPanel.add(new JLabel(), c);
		// 按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetd, 0, 0);
		mainPanel.add(butPanel, c);
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetd, 0, 0);
		butPanel.add(okShowInfo, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetd, 0, 0);
		butPanel.add(okBut, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetd, 0, 0);
		butPanel.add(cancelBut, c);
		// butPanel.add(okBut);
		// butPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelButPerformed() {
		this.dispose();
	}

	/***************************************************************************
	 * 确定
	 */
	private void okButPerformed() {
		// if (longitudinalField.getText() == null
		// || "".equals(longitudinalField.getText())) {
		// longitudinalVerf.setText("KPI纵坐标值不能为空");
		// return;
		// } else {
		// longitudinalVerf.setText("");
		// }
		//
		// if (yearField.getText() == null || "".equals(yearField.getText())) {
		// transverseVerf.setText("KPI横坐标值不能为空");
		// return;
		// } else {
		// transverseVerf.setText("");
		// }
		// String reg = "^[0-9]*$";
		// String nreg = "^\\d{4}$";
		// String strLongitudinal = this.longitudinalField.getText();
		// // SimpleDateFormat sdf = null;
		// // sdf = new SimpleDateFormat("yyyy");
		// // Date dateHonName = null;
		// if (strLongitudinal == null || "".equals(strLongitudinal)) {
		// this.okShowInfo.setText("请输入KPI纵坐标值");
		// return;
		// } else if (!strLongitudinal.matches(reg)) {
		// this.okShowInfo.setText("KPI纵坐标值只能输入数字");
		// return;
		// }
		// // if (this.transverseDateField.getText() == null
		// // || "".equals(this.transverseDateField.getText().toString())) {
		// // this.okShowInfo.setText("请输入KPI横坐标值！");
		// // return;
		// // }
		// // 周
		// String weekTex = this.longtwoField.getText();// 周 月 季度 天数
		// String yearTex = this.longOneField.getText();// 年
		// if (weekTex == null || "".equals(weekTex)) {
		// this.okShowInfo.setText("KPI横坐标值周不能为空");
		// return;
		// }else if(Integer.parseInt(weekTex) > 52){
		// this.okShowInfo.setText("KPI横坐标值周值不能大于52");
		// return;
		// }
		// if (yearTex == null || "".equals(yearTex)) {
		// this.okShowInfo.setText("KPI横坐标值年不能为空");
		// return;
		// } else if(!yearTex.matches(nreg)){
		// // dateHonName = sdf.parse(honValue);
		// this.okShowInfo.setText("KPI横坐标值年只能输入四位数字");
		// return;
		// }
		saveData();
	}

	/**
	 * @description:保存数据,并显示
	 */
	public abstract void saveData();

	public abstract String getKPIName();

	public abstract String getLongField();

	public abstract String getTransverseField();

	public abstract String getKPIValueTitle();
	/**
	 * 得到某一年周的总数
	 * 
	 * @param year
	 * @return
	 */
	public static int getMaxWeekNumOfYear(int year) {
		Calendar c = new GregorianCalendar();
		c.set(year, Calendar.DECEMBER, 31, 23, 59, 59);
		return getWeekOfYear(c.getTime());
	}
	/**
	 * 取得当前日期是多少周
	 * 
	 * @param date
	 * @return
	 */
	public static int getWeekOfYear(Date date) {
		Calendar c = new GregorianCalendar();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setMinimalDaysInFirstWeek(7);
		c.setTime(date);
		return c.get(Calendar.WEEK_OF_YEAR);
	}
	
	class VerifyLabel extends JLabel {
		public VerifyLabel() {
			Dimension dimensions = new Dimension(150, 15);
			this.setPreferredSize(dimensions);
			this.setMaximumSize(dimensions);
			this.setMinimumSize(dimensions);
			this.setForeground(Color.red);
		}
	}
}
