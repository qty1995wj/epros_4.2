package epros.draw.data.figure;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;

/**
 * 接口、子流程数据层对象
 * 
 * @author ZXH
 * 
 */
public class JecnImplData extends JecnFigureData {

	/** 接口类型 1是上游流程,2是下游流程,3是过程流程,4是子流程 */
	private int implType;
	/** 接口名称 **/
	private String implName;
	/** 备注 **/
	private String implNote;
	/** 流程要求 **/
	private String processRequirements;

	public JecnImplData(MapElemType mapElemType) {
		super(mapElemType);
	}

	/**
	 * 
	 * 实例化对象
	 * 
	 */
	public JecnImplData newInstance() {
		return new JecnImplData(mapElemType);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnImplData clone() {
		return (JecnImplData) this.currClone();
	}

	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnImplData) || !(destFlowElementData instanceof JecnImplData)) {
			return;
		}

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnImplData src = (JecnImplData) srcFlowElementData;
		JecnImplData dest = (JecnImplData) destFlowElementData;
		dest.setImplName(src.getImplName());
		dest.setImplNote(src.getImplNote());
		dest.setProcessRequirements(src.getProcessRequirements());
	}

	public void setFigureArrts(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		super.setFigureArrts(srcFlowElementData, destFlowElementData);
		if (!(srcFlowElementData instanceof JecnImplData && destFlowElementData instanceof JecnImplData)) {
			return;
		}
		
		JecnImplData src = (JecnImplData) srcFlowElementData;
		JecnImplData dest = (JecnImplData) destFlowElementData;
		dest.setImplName(src.getImplName());
		dest.setImplNote(src.getImplNote());
		dest.setProcessRequirements(src.getProcessRequirements());
	}

	public String getImplName() {
		return implName;
	}

	public void setImplName(String implName) {
		this.implName = implName;
	}

	public String getImplNote() {
		return implNote;
	}

	public void setImplNote(String implNote) {
		this.implNote = implNote;
	}

	public String getProcessRequirements() {
		return processRequirements;
	}

	public void setProcessRequirements(String processRequirements) {
		this.processRequirements = processRequirements;
	}
}
