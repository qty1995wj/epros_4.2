package com.jecn.epros.server.bean.download;

import java.io.Serializable;

public class RuleFlowBean implements Serializable {
	/** 制度标题ID */
	private Long ruleTitleId;
	/** 流程名称 */
	private String flowName;
	/** 流程编号 */
	private String flowInput;

	public Long getRuleTitleId() {
		return ruleTitleId;
	}

	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowInput() {
		return flowInput;
	}

	public void setFlowInput(String flowInput) {
		this.flowInput = flowInput;
	}
}
