package epros.draw.gui.top.toolbar.element;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;

/**
 * 
 * 3D、字体竖排、字体加粗选择按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElement3DPanel extends JecnAbstarctFlowElementPanel {
	/** 字体加粗 */
	private JCheckBox fontStyleRadioButton = null;
	/** 3D效果 */
	private JCheckBox flag3DRadioButton = null;

	public JecnFlowElement3DPanel(JecnFlowElementPanel flowElementPanel) {
		super(flowElementPanel);
		init();
	}

	@Override
	protected void initComponents() {
		// 字体加粗
		fontStyleRadioButton = new JCheckBox(flowElementPanel
				.getResourceManager().getValue("fontStyle"));
		// 3D效果
		flag3DRadioButton = new JCheckBox(flowElementPanel.getResourceManager()
				.getValue("flag3D"));

		this.setLayout(new GridLayout(1, 3, 40, 40));

		// 透明
		fontStyleRadioButton.setOpaque(false);
		flag3DRadioButton.setOpaque(false);

		// 事件
		fontStyleRadioButton.addItemListener(this.flowElementPanel);
		flag3DRadioButton.addItemListener(this.flowElementPanel);
	}

	@Override
	protected void initLayout() {
		// 竖排文字
		this.add(new JLabel());
		// 字体加粗
		this.add(fontStyleRadioButton);
		// 3D效果
		this.add(flag3DRadioButton);
	}

	@Override
	public void initData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null) {
			return;
		}
		// 字体加粗
		fontStyleRadioButton
				.setSelected((flowElementData.getFontStyle() == 1) ? true
						: false);
		// 3D效果
		flag3DRadioButton.setSelected(flowElementData.isFlag3D());
	}

	/**
	 * 
	 * 单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() != fontStyleRadioButton
				&& e.getSource() != flag3DRadioButton) {// 竖排文字、字体加粗、3D效果
			return;
		}
		itemStateChangedProcess(e);
	}

	private void itemStateChangedProcess(ItemEvent e) {
		// 选中为true，取消选中为false
		boolean flag = (e.getStateChange() == e.SELECTED) ? true : false;

		if (e.getSource() == fontStyleRadioButton) {// 字体加粗
			JecnElementAttributesUtil.updateFontStyle(flowElementPanel
					.getSelectedBasePanel(), (flag) ? Font.BOLD : Font.PLAIN);
		} else if (e.getSource() == flag3DRadioButton) {// 3D效果
			JecnElementAttributesUtil.updateFlag3D(flowElementPanel
					.getSelectedBasePanel(), flag);
		}
	}

	public JCheckBox getFontStyleRadioButton() {
		return fontStyleRadioButton;
	}

	public JCheckBox getFlag3DRadioButton() {
		return flag3DRadioButton;
	}

}
