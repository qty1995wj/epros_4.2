package com.jecn.epros.domain;

/**
 * 组织图的连接线线段
 *
 * @author Administrator
 */
public class JecnLineSegmentDep implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;// 主键ID
    private Long figureId;// 线段的FigureID
    private Long startX;// 线的开始点的X
    private Long startY;// 线的开始点的Y
    private Long endX;// 线的结束点的X
    private Long endY;// 线的结束点的Y

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public Long getStartX() {
        return startX;
    }

    public void setStartX(Long startX) {
        this.startX = startX;
    }

    public Long getStartY() {
        return startY;
    }

    public void setStartY(Long startY) {
        this.startY = startY;
    }

    public Long getEndX() {
        return endX;
    }

    public void setEndX(Long endX) {
        this.endX = endX;
    }

    public Long getEndY() {
        return endY;
    }

    public void setEndY(Long endY) {
        this.endY = endY;
    }
}
