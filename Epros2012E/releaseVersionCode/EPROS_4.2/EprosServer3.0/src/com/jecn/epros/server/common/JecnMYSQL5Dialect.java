package com.jecn.epros.server.common;

import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.dialect.MySQL5Dialect;

/**
 * 
 * 为了解决hibernate不支持mysql的text类型
 * 
 * @author xiaobo
 *
 */
public class JecnMYSQL5Dialect extends MySQL5Dialect {
	public JecnMYSQL5Dialect(){
		super();//.registerVarcharTypes();
		registerHibernateType( Types.LONGVARCHAR, Hibernate.TEXT.getName() );
		//下载流程文件（带附件），有blob字段
		registerHibernateType(Types.LONGVARBINARY, Hibernate.BLOB.getName()); 
	}
}