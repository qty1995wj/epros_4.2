/*      Copyright (c) 2012 JECN Corporation                     */
/*      JECN CONFIDENTIAL AND PROPRIETARY                       */
/*      All rights reserved by JECN Corporation.                */
/*      This program must be used solely for the purpose for    */
/*      which it was furnished by JECN Corporation.   No part   */
/*      of this program may be reproduced  or  disclosed  to    */
/*      others,  in  any form,  without  the  prior  written    */
/*      permission of JECN Corporation. Use  of  copyright      */
/*      notice does not evidence publication of the program.    */
package epros.draw.unredo;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnWorkflowConstant.UnRedoType;
import epros.draw.data.line.JecnDiviLineDataCloneable;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.MapLineFigure;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.top.toolbar.menu.JecnReplaceProcessMapPopupMenu;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnRemoveFlowElementUnit;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.unredo.data.JecnUnRedoDividingLineData;
import epros.draw.unredo.data.JecnUnRedoFigureData;
import epros.draw.unredo.data.JecnUnRedoManhattanLineData;
import epros.draw.unredo.data.JecnUnRedoVHLineData;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 
 * 撤销恢复一次操作处理类：记录操作前的数据和操作后数据
 * 
 * 执行撤销和恢复使用undo、redo方法
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUnRedoProcess {

	/** 当前操作前记录 */
	private JecnUndoRedoData undoData = null;
	/** 当前操作后记录 */
	private JecnUndoRedoData redoData = null;
	/** 产生撤销恢复动作类型 */
	private UnRedoType unRedoType = UnRedoType.none;

	/**
	 * 
	 * 构造函数
	 * 
	 * 注意：操作前后的数据对象和流程元素本身数据对象是同一个对象情况
	 * 
	 * @param undoUndoRedoDataRecode
	 *            JecnUnRedoDataRecode 操作前数据
	 * @param redoUndoRedoDataRecode
	 *            JecnUnRedoDataRecode 操作后数据
	 * @param unRedoType
	 *            UnRedoType 产生撤销恢复动作类型
	 */
	public JecnUnRedoProcess(JecnUndoRedoData undoData, JecnUndoRedoData redoData, UnRedoType unRedoType) {
		this.undoData = undoData;
		this.redoData = redoData;
		if (unRedoType == null) {
			this.unRedoType = UnRedoType.none;
		} else {
			this.unRedoType = unRedoType;
		}
	}

	/**
	 * 
	 * 撤销
	 * 
	 */
	void undo() {
		// 清除编辑框，清除选中点:true：清除成功 false：清除失败
		boolean isClear = JecnWorkflowUtil.hideAllResizeHandle();
		if (!isClear) {
			return;
		}
		// // 中断快速添加图形功能
		// JecnSystemStaticData.setInterruptSortCutFigure(true);

		undoProcess();
	}

	/**
	 * 
	 * 恢复
	 * 
	 */
	void redo() {
		// 清除编辑框，清除选中点:true：清除成功 false：清除失败
		boolean isClear = JecnWorkflowUtil.hideAllResizeHandle();
		if (!isClear) {
			return;
		}
		redoProcess();
	}

	/**
	 * 
	 * 撤销
	 * 
	 * 添加类型：删除操作后流程元素以及其数据
	 * 
	 * 修改类型：操作前的数据替换操作后的数据，对象不发生改变
	 * 
	 * 删除类型：添加操作前的流程元素以及恢复其数据
	 * 
	 */
	private void undoProcess() {
		switch (unRedoType) {
		case edit:// 修改类型
			editProcess(undoData);
			break;
		case add:// 添加类型
			removeProcess(redoData);
			break;
		case delete:// 删除类型
			addProcess(undoData);
			break;
		case addAndEdit:// 添加且修改
			undoAddEditProcess(redoData, undoData);
			break;
		case deleteAndAdd:// 删除一个，新增一个
			undoDeleteAddProcess(redoData, undoData);
			break;
		default:
			return;
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 
	 * 恢复
	 * 
	 * 添加类型：添加操作后的流程元素以及恢复其数据
	 * 
	 * 修改类型：操作后的数据替换操作前的数据，对象不发生改变
	 * 
	 * 删除类型：删除操作前流程元素以及其数据
	 * 
	 */
	private void redoProcess() {
		switch (unRedoType) {
		case edit:// 修改类型
			editProcess(redoData);
			break;
		case add:// 添加类型
			addProcess(redoData);
			break;
		case delete:// 删除类型
			removeProcess(undoData);
			break;
		case addAndEdit:// 添加且修改
			redoAddEditProcess(redoData, redoData);
			break;
		case deleteAndAdd:// 删除，且添加
			undoDeleteAddProcess(undoData, redoData);
			break;
		default:
			return;
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 
	 * 撤销：操作前的数据替换操作后的数据，对象不发生改变。即参数为撤销前对象undoData
	 * 
	 * 恢复：操作后的数据替换操作前的数据，对象不发生改变。即参数为撤销后对象redoData
	 * 
	 * 注意：对象不发生改变是指除连接线的小线段外的所有流程元素都没有发生对象变化，只是属性值改变。所以处理时需要删除小线段再添加小线段
	 * 
	 * @param editData
	 *            JecnUndoRedoData 待操作的数据
	 */
	private void editProcess(JecnUndoRedoData editData) {
		if (editData == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 图形
		List<JecnUnRedoFigureData> figureDataCloneList = editData.getFigureDataCloneList();
		// 连接线
		List<JecnUnRedoManhattanLineData> lineDataCloneList = editData.getManhattanLineDataCloneList();
		// 横竖分割线
		List<JecnUnRedoVHLineData> vhLineDataCloneList = editData.getVhLineDataCloneList();
		// 分割线
		List<JecnUnRedoDividingLineData> dividingLineDataList = editData.getDividingLineDataList();
		// 图形集合：为了获得相关联的连接线
		List<JecnBaseFigurePanel> figureList = new ArrayList<JecnBaseFigurePanel>();

		if (figureDataCloneList != null) {// 图形
			for (JecnUnRedoFigureData figureData : figureDataCloneList) {
				// 替换属性值
				figureData.getFigure().getFlowElementData().setFlowElementAttributes(figureData.getFigureDataClone());
				// 设置大小、位置点
				figureData.getFigure().reSetFigureSize();
				// 设置层级
				figureData.getFigure().reSetFlowElementZorder();

				figureList.add(figureData.getFigure());

				JecnDesignerProcess.getDesignerProcess().addElemLink(figureData.getFigure(),
						JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType());
			}

			// 重画图形相关的连接线
			JecnDraggedFiguresPaintLine.paintLineByFigures(figureList);
		}

		if (vhLineDataCloneList != null) {// 横竖分割线
			for (JecnUnRedoVHLineData vhLineData : vhLineDataCloneList) {
				// 替换属性值
				vhLineData.getVhLines().getFlowElementData().setFlowElementAttributes(vhLineData.getVhLineDataClone());
				// 设置位置点和大小
				vhLineData.getVhLines().setVHLineBounds();
			}
		}

		if (dividingLineDataList != null) {// 分割线
			for (JecnUnRedoDividingLineData dividingLineData : dividingLineDataList) {
				// 替换属性值
				dividingLineData.getDividingLine().getFlowElementData().setFlowElementAttributes(
						dividingLineData.getDivedingLineDataClone());
				// 当前放大缩小倍数下线段的克隆数据
				JecnDiviLineDataCloneable cloneable = dividingLineData.getDivedingLineData().getZoomDiviLineCloneable();
				// 设置拖动点位置及分割线原始位置
				dividingLineData.getDividingLine().initOriginalArributs(cloneable.getStartXY().getPoint(),
						cloneable.getEndXY().getPoint(), dividingLineData.getDividingLine(),
						dividingLineData.getDividingLine().getLineResizePanelCenter());
			}
		}

		if (lineDataCloneList != null) {// 连接线
			for (JecnUnRedoManhattanLineData lineData : lineDataCloneList) {
				lineData.getManhattanLine().getFlowElementData().setFlowElementAttributes(
						lineData.getManhattanLineDataClone());
				lineData.getManhattanLine().drawLineSegmentByList(
						lineData.getManhattanLineDataClone().getLineSegmentDataList());
				if (lineData.getManhattanLineDataClone().getFigureText() == null
						|| "".equals(lineData.getManhattanLineDataClone().getFigureText())) {
					lineData.getManhattanLine().removeTextPanel();
				} else {
					lineData.getManhattanLine().addTempPanel();
				}
				// 删除图形和线的关联关系
				JecnRemoveFlowElementUnit.removeListFigureRefManhattanLine(lineData.getManhattanLine().getStartFigure()
						.getListFigureRefManhattanLine(), lineData.getManhattanLine());

				// 删除图形和线的关联关系
				JecnRemoveFlowElementUnit.removeListFigureRefManhattanLine(lineData.getManhattanLine().getEndFigure()
						.getListFigureRefManhattanLine(), lineData.getManhattanLine());

				// 流程架构图，连接线脱离元素特殊处理
				mapLineFigureEdit(lineData);

				// 设置连接线开始图形和结束图形
				lineData.getManhattanLine().setStartFigure(lineData.getStartFigure());
				lineData.getManhattanLine().setEndFigure(lineData.getEndFigure());

				// 设置图形和线的关联关系
				JecnAddFlowElementUnit.addRefManhattLineToFigure(lineData.getManhattanLine());
				// 连接线执行放大缩小算法
				JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowZoomProcess().zoomManhattanLine(
						lineData.getManhattanLine());
			}
		}
	}

	private void mapLineFigureEdit(JecnUnRedoManhattanLineData lineData) {
		if (!JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {
			return;
		}
		// 允许连接线脱离元素单独存在
		if (lineData.getManhattanLine().getStartFigure() instanceof MapLineFigure) {
			if (lineData.getManhattanLine().getStartFigure() == lineData.getStartFigure()) {
				// 连线连接虚拟元素，移动情况
				lineData.getStartFigure().setSizeAndLocation(lineData.getManhattanLineData().getZoomStartPoint());
			} else {
				// 连线 由虚拟元素移动到标准元素情况
				JecnRemoveFlowElementUnit.removeFlowElement(lineData.getManhattanLine().getStartFigure());
			}
		} else if (lineData.getStartFigure() instanceof MapLineFigure) {
			// 连线由标准元素移动到虚拟元素
			JecnAddFlowElementUnit.addFlowElement(lineData.getStartFigure().getLocation(), lineData.getStartFigure());
		}

		// 连接线结束点
		if (lineData.getManhattanLine().getEndFigure() instanceof MapLineFigure) {
			if (lineData.getManhattanLine().getEndFigure() == lineData.getEndFigure()) {
				lineData.getEndFigure().setSizeAndLocation(lineData.getManhattanLineData().getZoomEndPoint());
			} else {
				JecnRemoveFlowElementUnit.removeFlowElement(lineData.getManhattanLine().getEndFigure());
			}
		} else if (lineData.getEndFigure() instanceof MapLineFigure) {
			JecnAddFlowElementUnit.addFlowElement(lineData.getEndFigure().getLocation(), lineData.getEndFigure());
		}
	}

	/**
	 * 
	 * 移除给定的流程元素
	 * 
	 * @param removeData
	 *            JecnUndoRedoData 待删除的流程元素
	 */
	private void removeProcess(JecnUndoRedoData removeData) {
		if (removeData == null) {
			return;
		}

		// 图形
		List<JecnUnRedoFigureData> figureDataCloneList = removeData.getFigureDataCloneList();
		// 连接线
		List<JecnUnRedoManhattanLineData> lineDataCloneList = removeData.getManhattanLineDataCloneList();
		// 横竖分割线
		List<JecnUnRedoVHLineData> vhLineDataCloneList = removeData.getVhLineDataCloneList();
		// 分割线
		List<JecnUnRedoDividingLineData> dividingLineDataList = removeData.getDividingLineDataList();

		// 图形
		removeFigureProcess(figureDataCloneList);

		if (lineDataCloneList != null) {// 连接线
			for (JecnUnRedoManhattanLineData lineData : lineDataCloneList) {
				// 移除连接线
				JecnRemoveFlowElementUnit.removeManLine(lineData.getManhattanLine());
			}
		}

		if (vhLineDataCloneList != null) {// 横竖分割线
			for (JecnUnRedoVHLineData vhLineData : vhLineDataCloneList) {
				// 移除横竖分割线
				JecnRemoveFlowElementUnit.removeVHLine(vhLineData.getVhLines());
			}
		}

		if (dividingLineDataList != null) {// 分割线
			for (JecnUnRedoDividingLineData dividingLineData : dividingLineDataList) {
				// 移除分割线
				JecnRemoveFlowElementUnit.removeDividing(dividingLineData.getDividingLine());
			}
		}
	}

	/**
	 * 
	 * 添加给定的流程元素
	 * 
	 * @param addData
	 *            JecnUndoRedoData 待添加流程元素
	 */
	private void addProcess(JecnUndoRedoData addData) {
		if (addData == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 图形
		List<JecnUnRedoFigureData> figureDataCloneList = addData.getFigureDataCloneList();
		// 连接线
		List<JecnUnRedoManhattanLineData> lineDataCloneList = addData.getManhattanLineDataCloneList();
		// 横竖分割线
		List<JecnUnRedoVHLineData> vhLineDataCloneList = addData.getVhLineDataCloneList();
		// 分割线
		List<JecnUnRedoDividingLineData> dividingLineDataList = addData.getDividingLineDataList();

		// 图形
		addFigureProcess(figureDataCloneList);

		if (lineDataCloneList != null) {// 连接线
			for (JecnUnRedoManhattanLineData lineData : lineDataCloneList) {
				// 把备份数据替换连接线的数据
				lineData.getManhattanLineData().setFlowElementAttributes(lineData.getManhattanLineDataClone());
				for (JecnLineSegmentData lineSegmentData : lineData.getManhattanLineData().getLineSegmentDataList()) {
					lineData.getManhattanLine().createLineSegmentLine(lineSegmentData, lineSegmentData.isEndLine());
				}

				// 添加连接线
				JecnAddFlowElementUnit.addDeleteUndoManLine(lineData.getManhattanLine());
				// 连接线执行放大缩小算法
				JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowZoomProcess().zoomManhattanLine(
						lineData.getManhattanLine());
				// 清空克隆连接线小线段临时数据：剪切->撤销->粘贴
				if (lineData.getManhattanLine().curLineSegmentsClone != null) {
					lineData.getManhattanLine().curLineSegmentsClone.clear();
				}
			}
		}

		if (vhLineDataCloneList != null) {// 横竖分割线
			for (JecnUnRedoVHLineData vhLineData : vhLineDataCloneList) {
				// 把备份数据替换横竖分割线的数据
				vhLineData.getVHLineData().setFlowElementAttributes(vhLineData.getVhLineDataClone());

				JecnVHLineData currVHLineData = vhLineData.getVhLines().getFlowElementData();

				// 添加横竖分割线
				JecnAddFlowElementUnit.addVHLine(vhLineData.getVhLines(), new Point(currVHLineData.getVXInt(),
						currVHLineData.getHYInt()));
			}
		}

		if (dividingLineDataList != null) {// 分割线
			for (JecnUnRedoDividingLineData dividingLineData : dividingLineDataList) {
				// 添加分割线
				JecnAddFlowElementUnit.addDividingLine(dividingLineData.getDividingLine());
			}
		}
	}

	/**
	 * 
	 * 撤销添加且删除操作
	 * 
	 * @param addData
	 *            JecnUndoRedoData
	 */
	private void undoAddEditProcess(JecnUndoRedoData addData, JecnUndoRedoData editData) {
		if (addData == null || editData == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 添加的图形
		List<JecnUnRedoFigureData> addFigureDataCloneList = addData.getAddFigureDataCloneList();

		// 编辑处理
		editProcess(editData);
		// 删除添加的图形
		removeFigureProcess(addFigureDataCloneList);
		// 移除横竖分割线
		removeVHLineProcess(addData.getAddVHLineDataCloneList());
	}

	/**
	 * 元素置换
	 * 
	 * @param delData
	 * @param addData
	 */
	private void undoDeleteAddProcess(JecnUndoRedoData delData, JecnUndoRedoData addData) {
		if (delData == null || addData == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		addAndDelProcess(delData, addData);

	}

	private void addAndDelProcess(JecnUndoRedoData delData, JecnUndoRedoData addData) {
		if (addData == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 图形
		List<JecnUnRedoFigureData> addFigureDataList = addData.getFigureDataCloneList();

		// 图形集合：为了获得相关联的连接线
		List<JecnBaseFigurePanel> figureList = new ArrayList<JecnBaseFigurePanel>();

		for (int i = 0; i < delData.getFigureDataCloneList().size(); i++) {
			JecnUnRedoFigureData figureBean = delData.getFigureDataCloneList().get(i);
			JecnBaseFigurePanel delFigure = figureBean.getFigure();
			// 删除面板元素
			JecnRemoveFlowElementUnit.removeReplaceFigure(delFigure);

			JecnUnRedoFigureData figureData = addFigureDataList.get(i);

			// 备份的数据替换掉图形的数据
			figureData.getFigureData().setFlowElementAttributes(figureData.getFigureDataClone());

			// 设置连接线 开始图形和结束图形
			JecnReplaceProcessMapPopupMenu.repalceLineActive(delFigure, figureData.getFigure());
			// 添加图形
			JecnAddFlowElementUnit.addFigure(figureData.getFigure().getCurrLocation(), figureData.getFigure());
			figureList.add(figureData.getFigure());
		}

		// 重画图形相关的连接线
		JecnDraggedFiguresPaintLine.paintLineByFigures(figureList);
	}

	/**
	 * 
	 * 恢复添加且删除操作
	 * 
	 * @param addData
	 *            JecnUndoRedoData
	 */
	private void redoAddEditProcess(JecnUndoRedoData addData, JecnUndoRedoData editData) {
		if (addData == null || editData == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 添加的图形
		List<JecnUnRedoFigureData> addFigureDataCloneList = addData.getAddFigureDataCloneList();
		// 编辑处理
		editProcess(editData);
		// 添加：添加的图形
		addFigureProcess(addFigureDataCloneList);

		// 移除横竖分割线
		addVHLineProcess(addData.getAddVHLineDataCloneList());
	}

	/**
	 * 
	 * 添加图形操作
	 * 
	 * @param figureDataCloneList
	 *            List<JecnUnRedoFigureData>
	 */
	private void addFigureProcess(List<JecnUnRedoFigureData> figureDataCloneList) {
		if (figureDataCloneList != null) {// 图形
			for (JecnUnRedoFigureData figureData : figureDataCloneList) {
				// 备份的数据替换掉图形的数据
				figureData.getFigureData().setFlowElementAttributes(figureData.getFigureDataClone());
				// 添加图形
				JecnAddFlowElementUnit.addFigure(figureData.getFigure().getCurrLocation(), figureData.getFigure());
				figureData.getFigure().initDesigner();
			}
		}
	}

	private void addVHLineProcess(List<JecnUnRedoVHLineData> vhLineDataCloneList) {
		if (vhLineDataCloneList != null) {// 图形
			for (JecnUnRedoVHLineData vhLineData : vhLineDataCloneList) {
				// 备份的数据替换掉图形的数据
				vhLineData.getVHLineData().setFlowElementAttributes(vhLineData.getVhLineDataClone());
				JecnVHLineData currVHLineData = vhLineData.getVhLines().getFlowElementData();
				// 添加横竖分割线
				JecnAddFlowElementUnit.addVHLine(vhLineData.getVhLines(), new Point(currVHLineData.getVXInt(),
						currVHLineData.getHYInt()));
			}
		}
	}

	/**
	 * 
	 * 删除图形操作
	 * 
	 * @param figureDataCloneList
	 *            List<JecnUnRedoFigureData>
	 */
	private void removeFigureProcess(List<JecnUnRedoFigureData> figureDataCloneList) {
		if (figureDataCloneList != null) {// 图形
			for (JecnUnRedoFigureData figureData : figureDataCloneList) {
				// 移除图形
				JecnRemoveFlowElementUnit.removeFigure(figureData.getFigure());
			}
		}
	}

	private void removeVHLineProcess(List<JecnUnRedoVHLineData> vhLineDataCloneList) {
		if (vhLineDataCloneList != null) {// 图形
			for (JecnUnRedoVHLineData vhLineData : vhLineDataCloneList) {
				// 移除图形
				JecnRemoveFlowElementUnit.removeVHLine(vhLineData.getVhLines());
			}
		}
	}
}
