package com.jecn.epros.server.common;


public interface IBaseService <M extends java.io.Serializable, PK extends java.io.Serializable> {

	public PK save(M model);
	
    public void saveOrUpdate(M model);
    
    public void update(M model);
    
    public void merge(M model);

    public void delete(PK id);

    public void deleteObject(M model);

    public M get(PK id);
    
    public boolean exists(PK id);
    

}
