package epros.designer.gui.recycle;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;

/***
 * 回收站树节点展开合并节点
 * 
 * @author Administrator
 * @Date 2015-03-10
 */
public abstract class RecycleTreeListener extends JecnTreeListener {
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(RecycleTreeListener.class);

	public RecycleTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	/**
	 * 合并
	 */
	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = getTreeExpandedData(node);
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("RecycleTreeListener treeExpanded is error！", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

	protected abstract List<JecnTreeBean> getTreeExpandedData(JecnTreeNode node)
			throws Exception;

	

	

	public JecnHighEfficiencyTree getjTree() {
		return jTree;
	}

	public void setjTree(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

}
