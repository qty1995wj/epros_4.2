package com.jecn.epros.server.bean.process;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;

public class FlowDriver {
	
	private Long flowId;
	/**流程名称**/
	private String flowName;
	/**流程驱动类型**/
	private String quantity;
	/**开始时间**/
	private String startTime;
	/**开始时间**/
	private String endTime;
	//---------
	/**经过格式化的驱动时间**/
	private String driverTime;
	private List<JecnUser> users;
	
	@Override
	public String toString() {
		return "FlowDriver [flowName=" + flowName + ", startTime=" + startTime + "]";
	}
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getDriverTime() {
		return driverTime;
	}
	public void setDriverTime(String driverTime) {
		this.driverTime = driverTime;
	}
	public List<JecnUser> getUsers() {
		return users;
	}
	public void setUsers(List<JecnUser> users) {
		this.users = users;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

}
