package com.jecn.epros.server.connector.fuyao;

public class FuYaoTaskBean {
	/** 注册系统编码 */
	private String registerCode;
	/** 第三方系统待办主键 */
	private String taskId;
	/** 待办标题 */
	private String title;
	/** 待办发起人姓名 */
	private String senderName;
	/** 类别 */
	private String classify;
	/** 内容类型 */
	private String contentType;
	/** 状态0待办，1已办 */
	private String state;
	/** 第三方系统发送者主键 */
	private String thirdSenderId;
	/** 第三方系统接收人主键 */
	private String thirdReceiverId;
	/** 待办发起日期，格式yyyy-MM-dd HH:mm */
	private String creationDate;
	/** 原生app的下载地址 */
	private String content;
	/** H5穿透地址 */
	private String h5url;
	/** PC穿透地址 */
	private String url;
	/** 登录名称/人员编码/手机号/电子邮件 */
	private String noneBindingSender;
	/** 登录名称/人员编码/手机号/电子邮件 */
	private String noneBindingReceiver;

	public String getRegisterCode() {
		return registerCode;
	}

	public void setRegisterCode(String registerCode) {
		this.registerCode = registerCode;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getThirdSenderId() {
		return thirdSenderId;
	}

	public void setThirdSenderId(String thirdSenderId) {
		this.thirdSenderId = thirdSenderId;
	}

	public String getThirdReceiverId() {
		return thirdReceiverId;
	}

	public void setThirdReceiverId(String thirdReceiverId) {
		this.thirdReceiverId = thirdReceiverId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getH5url() {
		return h5url;
	}

	public void setH5url(String h5url) {
		this.h5url = h5url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNoneBindingSender() {
		return noneBindingSender;
	}

	public void setNoneBindingSender(String noneBindingSender) {
		this.noneBindingSender = noneBindingSender;
	}

	public String getNoneBindingReceiver() {
		return noneBindingReceiver;
	}

	public void setNoneBindingReceiver(String noneBindingReceiver) {
		this.noneBindingReceiver = noneBindingReceiver;
	}

}
