package com.jecn.epros.server.common;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class AbsBaseDao<M extends java.io.Serializable, PK extends java.io.Serializable> implements
		IBaseDao<M, PK> {
	protected static final Logger LOGGER = LoggerFactory.getLogger(AbsBaseDao.class);

	private final Class<M> clazz;
	/** 查询总数SQL */
	private final String HQL_COUNT_ALL;
	/** 查询所有的数据SQL */
	private final String HQL_LIST_ALL;

	@SuppressWarnings("unchecked")
	public AbsBaseDao() {
		this.clazz = (Class<M>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		// this.clazz=(Class<M>)getClass();
		this.HQL_COUNT_ALL = "select count(*) from " + this.clazz.getSimpleName();
		this.HQL_LIST_ALL = "from " + this.clazz.getSimpleName();
	}

	@Autowired
	@Qualifier("sessionFactory")
	public SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public PK save(M model) {
		return (PK) getSession().save(model);
	}

	@Override
	public void saveOrUpdate(M model) {
		getSession().saveOrUpdate(model);
	}

	@Override
	public void update(M model) {
		getSession().update(model);

	}

	@Override
	public void merge(M model) {
		getSession().merge(model);
	}

	@Override
	public void delete(PK id) {
		getSession().delete(this.get(id));

	}

	@Override
	public void deleteObject(M model) {
		getSession().delete(model);

	}

	@SuppressWarnings("unchecked")
	@Override
	public M get(PK id) {
		return (M) getSession().get(this.clazz, id);
	}

	@Override
	public boolean exists(PK id) {
		return get(id) != null;
	}

	@Override
	public int countAll() {
		Long count = getObjectHql(HQL_COUNT_ALL);
		return count.intValue();
	}

	@Override
	public List<M> listAll() {
		return listHql(HQL_LIST_ALL);
	}

	@Override
	public int countAllByParamsHql(String hql, Object... params) {
		Long count = getObjectHql(hql, params);
		return count.intValue();
	}

	@Override
	public int countAllByParamsNativeSql(String sql, Object... params) {
		Number count = getObjectNativeSql(sql, params);
		return count == null ? 0 : count.intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getObjectHql(String hql, Object... params) {
		Query query = getQuery(hql);
		setParameters(query, params);
		return (T) query.uniqueResult();
	}

	@Override
	public <T> T getObjectNativeSql(String sql, Object... params) {
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, params);
		return (T) sqlQuery.uniqueResult();
	}

	public <T> List<T> listHql(String hql, Object... params) {
		return listHql(hql, -1, -1, params);
	}

	@Override
	public <T> List<T> listNativeSql(String sql, Object... params) {
		return listNativeSql(sql, -1, -1, params);
	}

	@Override
	public <T> List<T> listHql(String hql, int start, int pageSize, Object... params) {
		Query query = getSession().createQuery(hql);
		setParameters(query, params);
		if (start > -1 && pageSize > -1) {
			query.setMaxResults(pageSize);
			if (start != 0) {
				query.setFirstResult(start);
			}
		}
		List<T> results = query.list();
		return results;
	}



	@Override
	public <T> List<T> listNativeSql(String sql, int start, int pageSize, Object... params) {
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		setParameters(sqlQuery, params);
		if (start > -1 && pageSize > -1) {
			sqlQuery.setMaxResults(pageSize);
			if (start != 0) {
				sqlQuery.setFirstResult(start);
			}
		}

		List<T> results = sqlQuery.list();
		return results;
	}

	@Override
	public int execteHql(String hql, final Object... paramlist) {
		Query query = getSession().createQuery(hql);
		setParameters(query, paramlist);
		Object result = query.executeUpdate();
		return result == null ? 0 : ((Integer) result).intValue();
	}

	@Override
	public int execteNative(String sql, final Object... paramlist) {
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		setParameters(sqlQuery, paramlist);
		Object result = sqlQuery.executeUpdate();
		return result == null ? 0 : ((Integer) result).intValue();
	}

	protected void setParameters(Query query, Object[] params) {
		if (query.getQueryString().indexOf('?') == -1) {
			return;
		}
		;
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				if (params[i] instanceof Date) {
					// 日期使用setTimestamp
					query.setTimestamp(i, (Date) params[i]);
				} else {
					query.setParameter(i, params[i]);
				}
			}
		}
	}

	@Override
	public Query getQuery(String hql) {
		return getSession().createQuery(hql);
	}

	@Override
	public SQLQuery getSQLQuery(String sql) {
		return getSession().createSQLQuery(sql);
	}

	@Override
	public List<M> listNativeAddEntity(String sql, final Object... paramlist) {
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, paramlist);
		return sqlQuery.addEntity(clazz).list();
	}

	@Override
	public List<?> listNativeAddEntity(String sql, Class<?> z, Object... paramlist) {
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, paramlist);
		return sqlQuery.addEntity(z).list();
	}

	@Override
	public <T> List<T> listSQLServerWith(String withSql, String selectSql, String orderByName, int start, int pageSize,
			Object... params) {
		String sql = withSql + "," + "resultAll as (" + selectSql + "),"
				+ " result as (select *,ROW_NUMBER() OVER (order by " + orderByName + ") as rowNumber from resultAll)"
				+ " select * from result where rowNumber BETWEEN " + (start + 1) + " and " + (start + pageSize);
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, params);
		return sqlQuery.list();
	}

	@Override
	public <T> List<T> listSQLServerSelect(String selectSql, String orderByName, int start, int pageSize,
			Object... params) {
		String sql = " with resultAll as (" + selectSql + ")," + " JECN_RESULT as (select *,ROW_NUMBER() OVER (order by "
				+ orderByName + ") as rowNumber from resultAll)" + " select * from JECN_RESULT where rowNumber BETWEEN "
				+ (start + 1) + " and " + (start + pageSize);
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, params);
		return sqlQuery.list();

	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getNativeResultToMap(final String sql, final Object... params) {
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, params);
		return (Map) sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getNativeResultToListMap(final String sql, final Object... params) {
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, params);
		return (List) sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
	}

	public <T> List<T> getNativeResultToBeanListMap(final String sql, Class target, final Object... params) {
		SQLQuery sqlQuery = getSQLQuery(sql);
		setParameters(sqlQuery, params);
		return (List) sqlQuery.setResultTransformer(Transformers.aliasToBean(target)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> listObjectNativeSql(String sql, String param, Type type, Object... params) {
		SQLQuery sqlQuery = getSession().createSQLQuery(sql).addScalar(param, type);
		setParameters(sqlQuery, params);
		List<T> results = sqlQuery.list();
		return results;
	}
}
