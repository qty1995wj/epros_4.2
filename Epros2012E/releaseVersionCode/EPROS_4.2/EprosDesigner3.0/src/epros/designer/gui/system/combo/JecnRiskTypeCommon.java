package epros.designer.gui.system.combo;

import java.awt.Insets;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

public class JecnRiskTypeCommon extends KeyValComboBoxCommon {
	private static Logger log = Logger.getLogger(JecnRiskTypeCommon.class);

	public JecnRiskTypeCommon(int row, JecnPanel infoPanel, Insets insets, String key, JecnDialog jecnDialog,
			boolean isRequest) {
		super(row, infoPanel, insets, key, jecnDialog, getLevelTitle(), isRequest);
	}

	/**
	 * 保密级别 title初始化
	 * 
	 * @return
	 */
	private static String getLevelTitle() {
		return JecnProperties.getValue("riskTypeC");
	}

	@Override
	public Vector getListCheckBoxPo() {
		Vector model = new Vector();
		try {
			List<JecnDictionary> list = ConnectionPool.getOrganizationAction().getDictionarys(DictionaryEnum.JECN_RISK_TYPE);
			for (JecnDictionary dictionary : list) {
				model.add(new CheckBoxPo(dictionary.getCode().toString(), dictionary.getValue()));
			}
		} catch (Exception e1) {
			log.error("JecnRiskTypeCommon getListCheckBoxPo is error", e1);
		}
		return model;
	}

}
