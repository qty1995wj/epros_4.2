package com.jecn.epros.domain.process;

import com.jecn.epros.util.Utils;
import io.swagger.annotations.ApiModelProperty;

public class ProcessDetailInfo {

	@ApiModelProperty(value = "流程描述", position = 0)
	private String description;
	@ApiModelProperty(value = "流程目的", position = 1)
	private String target;
	@ApiModelProperty(value = "流程输入", position = 2)
	private String input;
	@ApiModelProperty(value = "流程输出", position = 3)
	private String output;
	@ApiModelProperty(value = "流程起点", position = 4)
	private String startingPoint;
	@ApiModelProperty(value = "流程终点", position = 5)
	private String endingPoint;
	@ApiModelProperty(value = "流程KPI", position = 6)
	private String kpi;

	public String getDescription() {
		return Utils.splitToBr(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTarget() {
		return Utils.splitToBr(target);
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getInput() {
		return Utils.splitToBr(input);
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return Utils.splitToBr(output);
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getStartingPoint() {
		return Utils.splitToBr(startingPoint);
	}

	public void setStartingPoint(String startingPoint) {
		this.startingPoint = startingPoint;
	}

	public String getEndingPoint() {
		return Utils.splitToBr(endingPoint);
	}

	public void setEndingPoint(String endingPoint) {
		this.endingPoint = endingPoint;
	}

	public String getKpi() {
		return Utils.splitToBr(kpi);
	}

	public void setKpi(String kpi) {
		this.kpi = kpi;
	}

}
