package com.jecn.epros.server.action.web.login.ad.BDF;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

/**
 * 巴德富配置文件读取类
 * 
 */
public class JecnBDFAfterItem {

	private static Logger log = Logger.getLogger(JecnBDFAfterItem.class);
	/**
	 * 默认浏览器登录地址
	 */
	public static String BROWSER_ADDRESS = null;
	public static String BROWSER_ADDRESS_TWO = null;

	/**
	 * 读取巴德富配置文件
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午02:38:20
	 */
	static {

		log.info("开始读取配置文件内容");
		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/bdf/bdf.properties";

		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			BROWSER_ADDRESS = props.getProperty("BROWSER_ADDRESS");
			BROWSER_ADDRESS_TWO = props.getProperty("BROWSER_ADDRESS_TWO");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}

}
