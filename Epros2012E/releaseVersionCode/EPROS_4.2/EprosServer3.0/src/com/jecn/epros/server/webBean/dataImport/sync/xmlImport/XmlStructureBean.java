package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

import java.io.Serializable;

public class XmlStructureBean implements Serializable {
	private long id;
	private String name;
	private int type;
	private long pid;
	private String fullName;
	private String columnName;

	// private List<XmlStructureBean> children;
	// private List<String> attributes;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getFullName() {
		return fullName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	// public List<XmlStructureBean> getChildren() {
	// return children;
	// }
	//
	// public void setChildren(List<XmlStructureBean> children) {
	// this.children = children;
	// }
	//
	// public List<String> getAttributes() {
	// return attributes;
	// }
	//
	// public void setAttributes(List<String> attributes) {
	// this.attributes = attributes;
	// }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final XmlStructureBean other = (XmlStructureBean) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
