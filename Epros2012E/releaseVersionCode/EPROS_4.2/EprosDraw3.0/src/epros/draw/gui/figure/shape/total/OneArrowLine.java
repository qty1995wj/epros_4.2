/*
 * OneArrowLine.java
 *单向箭头
 * Created on 2008年12月3日, 下午2:53
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.total;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.util.JecnUIUtil;

/**
 * 单向箭头
 * 
 * @author zhagnxh
 * @date： 日期：Mar 22, 2012 时间：3:35:57 PM
 */
public class OneArrowLine extends JecnBaseFigurePanel {

	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int x5;
	private int x6;
	private int x7;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int y5;
	private int y6;
	private int y7;

	public OneArrowLine(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = userHeight * 2 / 5;
			x2 = userWidth * 4 / 5;
			y2 = userHeight * 2 / 5;
			x3 = userWidth * 4 / 5;
			y3 = userHeight * 1 / 5;
			x4 = userWidth - 1;
			y4 = userHeight / 2;
			x5 = userWidth * 4 / 5;
			y5 = userHeight * 4 / 5;
			x6 = userWidth * 4 / 5;
			y6 = userHeight * 3 / 5;
			x7 = 0;
			y7 = userHeight * 3 / 5;
		} else if (flowElementData.getCurrentAngle() == 45.0) {
			x1 = userWidth * 16 / 50;
			y1 = userHeight * 9 / 50;
			x2 = userWidth * 41 / 50;
			y2 = userHeight * 34 / 50;
			x3 = userWidth * 19 / 20;
			y3 = userHeight * 11 / 20;
			x4 = userWidth * 9 / 10;
			y4 = userHeight * 9 / 10;
			x5 = userWidth * 11 / 20;
			y5 = userHeight * 19 / 20;
			x6 = userWidth * 34 / 50;
			y6 = userHeight * 41 / 50;
			x7 = userWidth * 9 / 50;
			y7 = userHeight * 16 / 50;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = userWidth * 3 / 5;
			y1 = 0;
			x2 = userWidth * 3 / 5;
			y2 = userHeight * 4 / 5;
			x3 = userWidth * 4 / 5;
			y3 = userHeight * 4 / 5;
			x4 = userWidth / 2;
			y4 = userHeight - 1;
			x5 = userWidth * 1 / 5;
			y5 = userHeight * 4 / 5;
			x6 = userWidth * 2 / 5;
			y6 = userHeight * 4 / 5;
			x7 = userWidth * 2 / 5;
			y7 = 0;
		} else if (flowElementData.getCurrentAngle() == 135.0) {
			x1 = userWidth * 41 / 50;
			y1 = userHeight * 16 / 50;
			x2 = userWidth * 16 / 50;
			y2 = userHeight * 41 / 50;
			x3 = userWidth * 9 / 20;
			y3 = userHeight * 19 / 20;
			x4 = userWidth / 10;
			y4 = userHeight * 9 / 10;
			x5 = userWidth * 1 / 20;
			y5 = userHeight * 11 / 20;
			x6 = userWidth * 9 / 50;
			y6 = userHeight * 34 / 50;
			x7 = userWidth * 34 / 50;
			y7 = userHeight * 9 / 50;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = userWidth / 5;
			y2 = userHeight / 5;
			x3 = userWidth / 5;
			y3 = userHeight * 2 / 5;
			x4 = userWidth;
			y4 = userHeight * 2 / 5;
			x5 = userWidth;
			y5 = userHeight * 3 / 5;
			x6 = userWidth / 5;
			y6 = userHeight * 3 / 5;
			x7 = userWidth / 5;
			y7 = userHeight * 4 / 5;

		} else if (flowElementData.getCurrentAngle() == 225.0) {
			x1 = userWidth * 1 / 10;
			y1 = userHeight * 1 / 10;
			x2 = userWidth * 9 / 20;
			y2 = userHeight * 1 / 20;
			x3 = userWidth * 16 / 50;
			y3 = userHeight * 9 / 50;
			x4 = userWidth * 41 / 50;
			y4 = userHeight * 34 / 50;
			x5 = userWidth * 34 / 50;
			y5 = userHeight * 41 / 50;
			x6 = userWidth * 9 / 50;
			y6 = userHeight * 16 / 50;
			x7 = userWidth * 1 / 20;
			y7 = userHeight * 9 / 20;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth * 4 / 5;
			y2 = userHeight / 5;
			x3 = userWidth * 3 / 5;
			y3 = userHeight / 5;
			x4 = userWidth * 3 / 5;
			y4 = userHeight - 1;
			x5 = userWidth * 2 / 5;
			y5 = userHeight - 1;
			x6 = userWidth * 2 / 5;
			y6 = userHeight * 1 / 5;
			x7 = userWidth / 5;
			y7 = userHeight / 5;
		} else if (flowElementData.getCurrentAngle() == 315.0) {
			x1 = userWidth * 9 / 10;
			y1 = userHeight / 10;
			x2 = userWidth * 19 / 20;
			y2 = userHeight * 9 / 20;
			x3 = userWidth * 41 / 50;
			y3 = userHeight * 16 / 50;
			x4 = userWidth * 16 / 50;
			y4 = userHeight * 41 / 50;
			x5 = userWidth * 9 / 50;
			y5 = userHeight * 34 / 50;
			x6 = userWidth * 34 / 50;
			y6 = userHeight * 9 / 50;
			x7 = userWidth * 11 / 20;
			y7 = userHeight / 20;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 }, new int[] {
				y1, y2, y3, y4, y5, y6, y7 }, 7);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintArrowLineTop(g2d, 5);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintArrowLineTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintArrowLineTop(Graphics2D g2d, int shadowCount) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (flowElementData.getCurrentAngle() == 45.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount, x2 - shadowCount,
					x3 - shadowCount, x4 - shadowCount, x5 - shadowCount,
					x6 - shadowCount, x7 - shadowCount }, new int[] { y1, y2,
					y3, y4, y5, y6, y7 }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount, x2 - shadowCount,
					x3 - shadowCount, x4 - shadowCount, x5 - shadowCount,
					x6 - shadowCount, x7 - shadowCount }, new int[] { y1, y2,
					y3, y4, y5, y6, y7 }, 7);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount, x2 - shadowCount,
					x3 - shadowCount, x4 - shadowCount, x5 - shadowCount,
					x6 - shadowCount, x7 - shadowCount }, new int[] { y1, y2,
					y3, y4, y5, y6, y7 }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount, x2 - shadowCount,
					x3 - shadowCount, x4 - shadowCount, x5 - shadowCount,
					x6 - shadowCount, x7 - shadowCount }, new int[] { y1, y2,
					y3, y4, y5, y6, y7 }, 7);
		} else if (flowElementData.getCurrentAngle() == 135.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount,
					x4 - shadowCount, x5 - shadowCount, x6 - shadowCount, x7 },
					new int[] { y1 - shadowCount, y2 - shadowCount, y3,
							y4 - shadowCount, y5 - shadowCount,
							y6 - shadowCount, y7 - shadowCount }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount,
					x4 - shadowCount, x5 - shadowCount, x6 - shadowCount, x7 },
					new int[] { y1 - shadowCount, y2 - shadowCount, y3,
							y4 - shadowCount, y5 - shadowCount,
							y6 - shadowCount, y7 - shadowCount }, 7);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4 - shadowCount,
					x5 - shadowCount, x6, x7 }, new int[] { y1, y2, y3, y4, y5,
					y6, y7 }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4 - shadowCount,
					x5 - shadowCount, x6, x7 }, new int[] { y1, y2, y3, y4, y5,
					y6, y7 }, 7);
		} else if (flowElementData.getCurrentAngle() == 225.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4 - shadowCount, y5 - shadowCount,
							y6, y7 }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4 - shadowCount, y5 - shadowCount,
							y6, y7 }, 7);
		} else if (flowElementData.getCurrentAngle() == 315.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		}
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4, x5 + 5,
					x6 + 5, x7 + 5 }, new int[] { y1 + 5, y2 + 5, y3 + 5,
					y4 + 5, y5 + 5, y6 + 5, y7 + 5 }, 7);
		} else if (flowElementData.getCurrentAngle() == 45.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4 + 5, x5 + 5,
					x6 + 5, x7 + 5 }, new int[] { y1 + 5, y2 + 5, y3 + 5,
					y4 + 5, y5 + 5, y6 + 5, y7 + 5 }, 7);
		} else if (flowElementData.getCurrentAngle() == 90.0) {

			// 阴影
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4 + 5, x5 + 5,
					x6 + 5, x7 + 5 }, new int[] { y1 + 5, y2, y3, y4, y5, y6,
					y7 + 5 }, 7);
		} else if (flowElementData.getCurrentAngle() == 135.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4, x5 + 5,
					x6 + 5, x7 + 5 }, new int[] { y1 + 5, y2 + 5, y3 + 5,
					y4 + 5, y5, y6 + 5, y7 + 5 }, 7);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4, x5, x6 + 5,
					x7 + 5 }, new int[] { y1 + 5, y2 + 5, y3 + 5, y4 + 5,
					y5 + 5, y6 + 5, y7 + 5 }, 7);

		} else if (flowElementData.getCurrentAngle() == 225.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4 + 5, x5 + 5,
					x6 + 5, x7 + 5 }, new int[] { y1, y2, y3, y4, y5, y6, y7 },
					7);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4 + 5, x5 + 5,
					x6 + 5, x7 + 5 }, new int[] { y1 + 5, y2 + 5, y3 + 5, y4,
					y5, y6 + 5, y7 + 5 }, 7);
		} else if (flowElementData.getCurrentAngle() == 315.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 + 5, x4, x5, x6 + 5,
					x7 + 5 }, new int[] { y1 + 5, y2 + 5, y3 + 5, y4 + 5,
					y5 + 5, y6 + 5, y7 + 5 }, 7);
		}
	}

	/**
	 * 
	 * 边缘渐变 循环
	 * 
	 * @param gw
	 * @param g2d
	 * @param x
	 * @param y
	 */
	private void paintShadowsFor(int gw, Graphics2D g2d, int[] x, int[] y) {
		for (int i = gw; i >= 2; i -= 2) {
			float pct = (float) (gw - i) / (gw - 1);
			g2d.setPaint(new GradientPaint(0.0f, userHeight * 0.25f,
					flowElementData.getShadowColor(), 0.0f, userHeight,
					JecnUIUtil.getShadowBodyColor()));
			g2d.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_ATOP, pct));
			g2d.setStroke(new BasicStroke(i));
			g2d.drawPolygon(x, y, 7);
		}
	}

	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DOneLow(g2d, indent3Dvalue);
	}

	/**
	 * 
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DTwoLine(g2d, indent3Dvalue);
	}

	/**
	 * 既有3D又有阴影顶层填充
	 * 
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		paint3DAndShawsTop(g2d, indent3Dvalue);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DOneLow(g2d, 0);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DTwoLine(g2d, 0);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 * 
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		if (this.getFlowElementData().getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue / 2,
					x2 + indent3Dvalue / 2, x3 + indent3Dvalue / 2,
					x4 - indent3Dvalue, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue, y2 + indent3Dvalue,
							y3 + indent3Dvalue, y4, y5 - indent3Dvalue,
							y6 - indent3Dvalue, y7 - indent3Dvalue }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue / 2,
					x2 + indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 + indent3Dvalue,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue }, new int[] {
					y1 + indent3Dvalue, y2 + indent3Dvalue, y3 + indent3Dvalue,
					y4 - indent3Dvalue / 2, y5 - indent3Dvalue / 2,
					y6 - indent3Dvalue / 2, y7 - indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue, x4,
					x5 + indent3Dvalue, x6 + indent3Dvalue / 2,
					x7 + indent3Dvalue / 2 }, new int[] {
					y1 + indent3Dvalue / 2, y2 + indent3Dvalue / 2,
					y3 + indent3Dvalue / 2, y4 - indent3Dvalue / 2,
					y5 + indent3Dvalue / 2, y6 + indent3Dvalue / 2,
					y7 + indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue,
					x4 + indent3Dvalue / 2, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue / 2, y2 + indent3Dvalue / 2,
							y3 - indent3Dvalue / 2, y4 - indent3Dvalue / 2,
							y5 + indent3Dvalue, y6 + indent3Dvalue / 2,
							y7 + indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 - indent3Dvalue / 2,
					x6 - indent3Dvalue / 2, x7 - indent3Dvalue / 2 },
					new int[] { y1, y2 + indent3Dvalue, y3 + indent3Dvalue / 2,
							y4 + indent3Dvalue / 2, y5 - indent3Dvalue / 2,
							y6 - indent3Dvalue / 2, y7 - indent3Dvalue }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue / 2, y2 + indent3Dvalue / 2,
							y3 + indent3Dvalue / 2, y4 + indent3Dvalue / 2,
							y5 - indent3Dvalue / 2, y6 - indent3Dvalue / 2,
							y7 - indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue / 2,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 + indent3Dvalue / 2, x6 + indent3Dvalue / 2,
					x7 + indent3Dvalue / 2 }, new int[] {
					y1 + indent3Dvalue / 2, y2, y3 - indent3Dvalue / 2,
					y4 - indent3Dvalue / 2, y5 - indent3Dvalue / 2,
					y6 - indent3Dvalue / 2, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue / 2, y2 - indent3Dvalue / 2,
							y3 - indent3Dvalue / 2, y4 - indent3Dvalue / 2,
							y5 + indent3Dvalue / 2, y6 + indent3Dvalue / 2,
							y7 + indent3Dvalue / 2 }, 7);
		}
	}

	/**
	 * 3D+阴影顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	private void paint3DAndShawsTop(Graphics2D g2d, int indent3Dvalue) {
		// indent3Dvalue = indent3Dvalue + shadowCount;
		if (this.getFlowElementData().getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue / 2,
					x2 + indent3Dvalue / 2, x3 + indent3Dvalue / 2,
					x4 - indent3Dvalue, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue, y2 + indent3Dvalue,
							y3 + indent3Dvalue, y4, y5 - indent3Dvalue,
							y6 - indent3Dvalue, y7 - indent3Dvalue }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue / 2,
					x2 + indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 + indent3Dvalue,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue }, new int[] {
					y1 + indent3Dvalue, y2 + indent3Dvalue, y3 + indent3Dvalue,
					y4 - indent3Dvalue / 2, y5 - indent3Dvalue / 2,
					y6 - indent3Dvalue / 2, y7 - indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue, x4,
					x5 + indent3Dvalue, x6 + indent3Dvalue / 2,
					x7 + indent3Dvalue / 2 }, new int[] {
					y1 + indent3Dvalue / 2, y2 + indent3Dvalue / 2,
					y3 + indent3Dvalue / 2, y4 - indent3Dvalue / 2,
					y5 + indent3Dvalue / 2, y6 + indent3Dvalue / 2,
					y7 + indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue,
					x4 + indent3Dvalue / 2, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue / 2, y2 + indent3Dvalue / 2,
							y3 - indent3Dvalue / 2, y4 - indent3Dvalue / 2,
							y5 + indent3Dvalue, y6 + indent3Dvalue / 2,
							y7 + indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 - indent3Dvalue / 2,
					x6 - indent3Dvalue / 2, x7 - indent3Dvalue / 2 },
					new int[] { y1, y2 + indent3Dvalue, y3 + indent3Dvalue / 2,
							y4 + indent3Dvalue / 2, y5 - indent3Dvalue / 2,
							y6 - indent3Dvalue / 2, y7 - indent3Dvalue }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue / 2, y2 + indent3Dvalue / 2,
							y3 + indent3Dvalue / 2, y4 + indent3Dvalue / 2,
							y5 - indent3Dvalue / 2, y6 - indent3Dvalue / 2,
							y7 - indent3Dvalue / 2 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue / 2,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 + indent3Dvalue / 2, x6 + indent3Dvalue / 2,
					x7 + indent3Dvalue / 2 }, new int[] {
					y1 + indent3Dvalue / 2, y2, y3 - indent3Dvalue / 2,
					y4 - indent3Dvalue / 2, y5 - indent3Dvalue / 2,
					y6 - indent3Dvalue / 2, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2,
					x2 - indent3Dvalue / 2, x3 - indent3Dvalue / 2,
					x4 - indent3Dvalue / 2, x5 + indent3Dvalue / 2,
					x6 + indent3Dvalue / 2, x7 + indent3Dvalue / 2 },
					new int[] { y1 + indent3Dvalue / 2, y2 - indent3Dvalue / 2,
							y3 - indent3Dvalue / 2, y4 - indent3Dvalue / 2,
							y5 + indent3Dvalue / 2, y6 + indent3Dvalue / 2,
							y7 + indent3Dvalue / 2 }, 7);
		}
	}

	public void paint3DOneLow(Graphics2D g2d, int indent3Dvalue) {
		if (this.getFlowElementData().getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		}
	}

	/**
	 * 3D效果边框
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            有阴影是边框缩进值
	 */
	public void paint3DTwoLine(Graphics2D g2d, int indent3Dvalue) {
		if (this.getFlowElementData().getCurrentAngle() == 0.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		} else if (this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 },
					new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
		}

	}

}
