package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.web.login.ad.lingnan.LingNanAfterItem;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 基于Json同步数据
 * 
 * @author ZXH
 * @date 2019-3-9
 * 
 */
public class ImportLingNamUserByJson extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(ImportLingNamUserByJson.class);

	public ImportLingNamUserByJson(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 获取部门数据集合
	 */
	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取Json数据
		String result = LingNanAfterItem.getOrgData();
		return getDpetList(result);
	}

	/**
	 * 获取人员数据集合
	 */
	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取Json数据
		String result = LingNanAfterItem.getUserPostData();
		return getUserList(result);
	}

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	public List<DeptBean> getDpetList(String result) throws Exception {
		// String status = JSONObject.fromObject(result).getString("success");
		// if ("true".equals(status)) {
		// log.error("获取部门异常！result = ");
		// return new ArrayList<DeptBean>();
		// }
		/** 部门 */
		List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
		DeptBean deptBean = null;
		String returnObj = JSONObject.fromObject(result).getString("obj");
		log.info("获取部门数据成功！");
		JSONArray jsonArray = JSONArray.fromObject(returnObj);

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			deptBean = new DeptBean();
			deptBean.setDeptNum(jsonObject.getString("ORGNUM"));
			deptBean.setDeptName(jsonObject.getString("ORGNAME"));
			if (jsonObject.get("PREORGNUM") == null) {
				deptBean.setPerDeptNum("0");
			} else {
				deptBean.setPerDeptNum(jsonObject.get("PREORGNUM").toString());
			}

			deptBeanList.add(deptBean);
		}
		return deptBeanList;
	}

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	public List<UserBean> getUserList(String result) throws Exception {
		/** 人员 */
		List<UserBean> userBeanList = new ArrayList<UserBean>();
		UserBean userBean = null;

		String returnObj = JSONObject.fromObject(result).getString("obj");
		JSONArray jsonArray = JSONArray.fromObject(returnObj);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			userBean = new UserBean();
			userBean.setUserNum(jsonObject.getString("EMPNUM"));
			userBean.setTrueName(jsonObject.getString("EMPNAME"));

			userBean.setPosNum(jsonObject.getString("POSNUM"));
			userBean.setPosName(jsonObject.getString("PSONAME"));
			userBean.setDeptNum(jsonObject.getString("ORGNUM"));

			if (StringUtils.isBlank(userBean.getPosNum()) || StringUtils.isBlank(userBean.getDeptNum())
					|| StringUtils.isBlank(userBean.getPosName())) {
				userBean.setDeptNum(null);
				userBean.setPosName(null);
				userBean.setPosNum(null);
			}
			userBeanList.add(userBean);
		}

		editUserEmail(userBeanList);
		return userBeanList;
	}

	/**
	 * 1.邮箱是单独查询 2.需要根据用户编号重新赋值邮箱
	 */
	private void editUserEmail(List<UserBean> userBeanList) {
		// 获取人员邮箱json
		String result = LingNanAfterItem.getUserEmail();
		// 1.解析 邮箱 json
		String returnObj = JSONObject.fromObject(result).getString("obj");
		JSONArray jsonArray = JSONArray.fromObject(returnObj);
		// 循环人员集合
		for (UserBean user : userBeanList) {
			// 循环邮箱集合
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
				if (!jsonObject.containsKey("EMAIL")) {
					continue;
				}
				// 判断编号是否相同 确认是否为同一个人员
				if (user.getUserNum().equals(jsonObject.getString("EMPNUM"))) {
					// 重新给人员赋值邮箱
					user.setEmail(jsonObject.getString("EMAIL"));
					user.setEmailType(0);
				}
			}

		}

	}
}
