package com.jecn.epros.server.bean.popedom;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class PositonInfo implements java.io.Serializable {
	/** 岗位基本信息 */
	private JecnPositionFigInfo positionBean;
	/** 上级岗位 */
	private List<JecnTreeBean> upPositionList;
	/** 下级岗位 */
	private List<JecnTreeBean> lowerPositionList;

	/**
	 * 获取岗位附件结果集
	 * 
	 * @return
	 */
	public List<JecnTreeBean> getFileList() {
		if (positionBean == null) {
			return null;
		}
		List<JecnTreeBean> posFileList = new ArrayList<JecnTreeBean>();
		if (positionBean.getFileId() != null
				&& positionBean.getFileId().intValue() != -1) {
			JecnTreeBean treeBean = new JecnTreeBean();
			treeBean.setId(positionBean.getFileId());
			treeBean.setName(positionBean.getFileName());
			treeBean.setTreeNodeType(TreeNodeType.file);
			posFileList.add(treeBean);
		}
		return posFileList;
	}

	public JecnPositionFigInfo getPositionBean() {
		return positionBean;
	}

	public void setPositionBean(JecnPositionFigInfo positionBean) {
		this.positionBean = positionBean;
	}

	public List<JecnTreeBean> getUpPositionList() {
		return upPositionList;
	}

	public void setUpPositionList(List<JecnTreeBean> upPositionList) {
		this.upPositionList = upPositionList;
	}

	public List<JecnTreeBean> getLowerPositionList() {
		return lowerPositionList;
	}

	public void setLowerPositionList(List<JecnTreeBean> lowerPositionList) {
		this.lowerPositionList = lowerPositionList;
	}

	public List<JecnTreeBean> getInListRule() {
		return inListRule;
	}

	public void setInListRule(List<JecnTreeBean> inListRule) {
		this.inListRule = inListRule;
	}

	public List<JecnTreeBean> getOutListRule() {
		return outListRule;
	}

	public void setOutListRule(List<JecnTreeBean> outListRule) {
		this.outListRule = outListRule;
	}

	/** 部门内相关制度 */
	private List<JecnTreeBean> inListRule;
	/** 部门内相关制度 */
	private List<JecnTreeBean> outListRule;

}
