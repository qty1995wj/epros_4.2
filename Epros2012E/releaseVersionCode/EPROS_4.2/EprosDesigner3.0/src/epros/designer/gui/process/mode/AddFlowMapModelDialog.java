package epros.designer.gui.process.mode;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;

/***
 * 新建流程地图模板
 * @author 2012-06-26
 *
 */
public class AddFlowMapModelDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddFlowMapModelDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;
	
	public AddFlowMapModelDialog(JecnTreeNode pNode,JTree jTree){
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("newProcessMapMode");
	}
	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		//向流程模板Bean中添加数据JecnFlowStructureMode
		JecnFlowStructureT jecnFlowStructureT = new JecnFlowStructureT();
		//流程地图模板名称
		jecnFlowStructureT.setFlowName(getName());
		jecnFlowStructureT.setDataType(1);
		//类别：0：流程地图模板   1：流程模板
		jecnFlowStructureT.setIsFlow(0L);
		//父节点
		jecnFlowStructureT.setPerFlowId(pNode.getJecnTreeBean().getId());
		//排序ID
		jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode)));
		Date createDatime = new Date();
		jecnFlowStructureT.setCreateDate(createDatime);
		jecnFlowStructureT.setProjectId(-1L);
		jecnFlowStructureT.setOccupier(0L);
		jecnFlowStructureT.setDelState(0L);
		jecnFlowStructureT.setPeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnFlowStructureT.setUpdateDate(createDatime);
		jecnFlowStructureT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		
		//执行数据库表的添加保存
		try {
			Long id = ConnectionPool.getProcessModeAction().addFlowModel(jecnFlowStructureT,-1);
			//向树节点添加流程模板节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.processMapMode);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddFlowMapModelDialog saveData is error",e);
		}
		
	}
	
	/**
	 * 验证名称
	 */
	public boolean validateName(String name, JLabel jLable){
		return JecnValidateCommon.validateName(name, promptLab);
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name,
				TreeNodeType.processMapMode);
	}
	protected void validateEnglishName() {// 验证英文名称
	}

	protected void addEnNamePanel(Insets insets) {
	}


}
