package epros.designer.gui.system.field;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import epros.designer.gui.common.JecnTextField;
import epros.designer.util.JecnProperties;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUserCheckUtil;

public class TextFieldRequest {
	/** 流程名称Lab */
	private JLabel jecnLab = new JLabel();

	/** 流程名称Field */
	private JecnTextField jecnField = new JecnTextField();

	/** 必填提示符 */
	private JLabel jecnRequset = new JLabel("*");

	private String tipName;

	private String text;

	public TextFieldRequest(int rows, JPanel infoPanel, Insets insets, String labName, String text, String tipName) {
		jecnLab.setText(labName);
		jecnField.setText(text);
		this.tipName = tipName;
		this.text = text;
		jecnRequset.setForeground(Color.red);
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(jecnLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(jecnField, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(jecnRequset, c);
	}

	public boolean isUpdate() {
		if (!text.equals(jecnField.getText())) {
			return true;
		}
		return false;
	}

	public String getResultStr() {
		return jecnField.getText().trim();
	}

	public boolean validateStr(JLabel verfyLab) {
		String name = jecnField.getText().trim();
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			verfyLab.setText(tipName + JecnProperties.getValue("isNotEmpty"));
			return true;
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(name)) {
			verfyLab.setText(tipName + JecnProperties.getValue("notInputThis"));
			return true;
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {
			// 不能超过122个字符或61个汉字
			verfyLab.setText(tipName + JecnProperties.getValue("lengthNotBaiOut"));
			return true;
		}
		return false;
	}
}
