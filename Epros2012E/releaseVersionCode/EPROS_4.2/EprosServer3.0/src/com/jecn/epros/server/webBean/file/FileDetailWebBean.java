package com.jecn.epros.server.webBean.file;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author weidp
 * @description 文件清单
 * @date： 日期：2014-4-14 时间：上午11:38:34
 */
public class FileDetailWebBean {
	/** 文件Id **/
	private Long fileId;
	/** 父文件Id **/
	private Long perFileId;
	/** 级别 **/
	private int level;
	/** 文件名称 **/
	private String fileName;
	/** 文件编号 **/
	private String docNumber;
	/** 责任部门 **/
	private String responsibleDeptName;
	/** 创建人 **/
	private String creator;
	/** 创建时间 **/
	private String createTime;
	/** 更新时间 **/
	private String updateTime;
	/** 文件使用情况 **/
	private List<FileDetailRelatedWebBean> fileDetailRelatedList = new ArrayList<FileDetailRelatedWebBean>();
	/** 是否文件夹 0是目录,1是文件 **/
	private Integer isDir;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getResponsibleDeptName() {
		return responsibleDeptName;
	}

	public void setResponsibleDeptName(String responsibleDeptName) {
		this.responsibleDeptName = responsibleDeptName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getPerFileId() {
		return perFileId;
	}

	public void setPerFileId(Long perFileId) {
		this.perFileId = perFileId;
	}

	public List<FileDetailRelatedWebBean> getFileDetailRelatedList() {
		return fileDetailRelatedList;
	}

	public void setFileDetailRelatedList(
			List<FileDetailRelatedWebBean> fileDetailRelatedList) {
		this.fileDetailRelatedList = fileDetailRelatedList;
	}

	public Integer getIsDir() {
		return isDir;
	}

	public void setIsDir(Integer isDir) {
		this.isDir = isDir;
	}

	public String getHrefFileName() {
		return "<a href='file.action?visitType=infoFile&fileId=" + this.fileId
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.fileName + "</a>";
	}

}
