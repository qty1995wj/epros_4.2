package epros.designer.gui.standard;

import javax.swing.JTree;

import com.jecn.epros.server.bean.standard.StandardBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;

public class EditStanClauseRequireDialog extends StanClauseRequireDialog {

	private static final long serialVersionUID = 1L;

	private String clauseRequireAreaText = "";

	public EditStanClauseRequireDialog(JecnTreeNode node, JTree jTree) {
		super(node, jTree);
		this.setTitle(JecnProperties.getValue("editStanClauseRequire"));
		initData();
	}

	private void initData() {
		try {
			StandardBean standardBean = ConnectionPool.getStandardAction().getStandardBean(
					node.getJecnTreeBean().getId());
			clauseRequireAreaText = standardBean.getStanContent();
			clauseRequireArea.setText(clauseRequireAreaText);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void okButtonAction() {
		String clauseContent = clauseRequireArea.getText().trim();
		if (DrawCommon.isNullOrEmtry(clauseContent)) {
			tipLabel.setText(JecnUserCheckInfoData.getNameNotNull());
			return;
		}
		String name = "";
		if (clauseContent.length() > 8) {
			name = clauseContent.substring(0, 8) + "...";
		} else {
			name = clauseContent;
		}
		try {
			ConnectionPool.getStandardAction().updateStanClauseRequire(this.node.getJecnTreeBean().getId(), name,
					clauseContent, JecnConstants.getUserId());
			node.getJecnTreeBean().setName(name);
			node.getJecnTreeBean().setContent(clauseContent);
			node.setUserObject(name);
			((JecnTreeModel) jTree.getModel()).reload(node);
			// 关闭窗体
			this.setVisible(false);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("editTermsAndConditionsRequired"));
			log.error(JecnProperties.getValue("editTermsAndConditionsRequired"), e);
		}
	}

	public boolean isUpdate() {
		String clauseContent = clauseRequireArea.getText().trim();
		if (!clauseRequireAreaText.equals(clauseContent)) {
			return true;
		}
		return false;
	}

}
