package epros.draw.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * 操作系统属性值处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOPUtil {
	private static final Log log = LogFactory.getLog(JecnOPUtil.class);

	/**
	 * 
	 * 获取WindowMAC地址
	 * 
	 * @return String
	 */
	public static String getWindowsMACAddress() {
		// MAC地址字符串
		String address = "";

		InputStreamReader inputStreamReader = null;
		BufferedReader br = null;
		try {

			ProcessBuilder pb = new ProcessBuilder("ipconfig", "/all");
			Process p = pb.start();

			// jvm语言环境是中文时，使用gbk方式读数据，其他默认
			if (Locale.getDefault().getLanguage().indexOf("zh") > -1) {
				inputStreamReader = new InputStreamReader(p.getInputStream(),
						"GBK");
			} else {
				inputStreamReader = new InputStreamReader(p.getInputStream());
			}

			br = new BufferedReader(inputStreamReader);

			String line;
			while ((line = br.readLine()) != null) {
				if (line.indexOf("Physical Address") != -1
						|| line.indexOf("物理地址") != -1) {
					log
							.info("JecnOPUtil类getWindowsMACAddress方法：获取本地MAC地址。地MAC地址："
									+ line);
					// mac地址开始序号
					int macIndex = line.indexOf("-") - 2;
					if (macIndex < 0 || macIndex > line.length()) {
						continue;
					}

					line = line.substring(macIndex);
					if (!DrawCommon.isNullOrEmtryTrim(line)) {// 不为空
						address += line + ",";
					}
				}
			}

			return address;
		} catch (Exception ex) {
			log.error("JecnOPUtil类getWindowsMACAddress方法：获取本地MAC地址异常。", ex);
		} finally {
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {
					log
							.error(
									"JecnOPUtil类getWindowsMACAddress方法：inputStreamReader关闭异常。",
									e);
				}
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log
							.error(
									"JecnOPUtil类getWindowsMACAddress方法：BufferedReader关闭异常。",
									e);
				}
			}
		}
		return address;
	}

	/**
	 * 
	 * 获取Linux系统MAC地址
	 * 
	 * @return String
	 */
	public static String getLinuxMACAddress() {
		// MAC地址字符串
		String address = "";

		InputStreamReader inputStreamReader = null;
		BufferedReader br = null;
		try {

			ProcessBuilder pb = new ProcessBuilder("ifconfig", "-a");
			Process p = pb.start();

			inputStreamReader = new InputStreamReader(p.getInputStream());
			br = new BufferedReader(inputStreamReader);

			String line;
			while ((line = br.readLine()) != null) {
				if (line.indexOf("Link encap:Ethernet    HWaddr") != -1) {
					log
							.info("JecnOPUtil类getWindowsMACAddress方法：获取本地MAC地址。地MAC地址："
									+ line);
					int macIndex = line.indexOf("HWaddr") + 7;
					if (macIndex < 0 || macIndex > line.length()) {
						continue;
					}

					line = line.substring(macIndex);

					if (!DrawCommon.isNullOrEmtryTrim(line)) {// 不为空
						address += line + ",";
					}
					break;
				}
			}

			return address;
		} catch (Exception ex) {
			log.error("JecnOPUtil类getLinuxMACAddress方法：获取本地MAC地址异常。", ex);
		} finally {
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {
					log
							.error(
									"JecnOPUtil类getLinuxMACAddress方法：inputStreamReader关闭异常。",
									e);
				}
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log
							.error(
									"JecnOPUtil类getLinuxMACAddress方法：BufferedReader关闭异常。",
									e);
				}
			}
		}
		return address;
	}
}
