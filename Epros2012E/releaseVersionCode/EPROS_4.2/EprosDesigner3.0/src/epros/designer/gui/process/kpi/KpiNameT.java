package epros.designer.gui.process.kpi;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import epros.designer.util.JecnProperties;

public class KpiNameT {
	private Long kpiAndId;// 主键ID
	private Long flowId;// 流程ID KPI_VERTICAL
	private String kpiName;// KPI的名称
	private Integer kpiType;// 0为:结果性指标、1为:过程性指标
	private String kpiDefinition;// kpi定义
	private String kpiStatisticalMethods;// kpi统计方法
	private String kpiTarget;// kpi目标值
	private String kpiHorizontal;// KPI纵坐标 数据统计时间/频率 0：月 1：季度
	/**
	 * 0:百分比 1:季度 2:月 3:周 4:工作日 5:天 6:小时 7:分钟 8:个 9:人 10:ppm 11:元、12：次
	 */
	private String kpiVertical;// KPI横向坐标标
	private Date creatTime;// 创建时间
	private Integer kpiTargetOperator;// KPI目标值比较符号
	private Integer kpiDataMethod;// 数据获取方式
	private String kpiDataMethodString;// 数据获取方式
	private Long kpiDataPeopleId;// 数据提供者
	private Integer kpiRelevance;// 相关度
	private Integer kpiTargetType;// 指标来源
	private String firstTargetId;// 支撑的一级指标ID
	private Date updateTime;// 更新时间
	private Long createPeopleId;// 创建人
	private Long updatePeopleId;// 更新人
	private String kpiDataPeopleName;// 数据提供者名称
	private String firstTargetContent;// 支撑的一级指标内容
	private String kpiITSystemIds;// IT系统Id集合
	private String kpiITSystemNames;// IT系统名称集合
	private String kpiPurpose;// 设置目的
	private String kpiPoint;// 测量点
	private String kpiPeriod;// 统计周期
	private String kpiExplain;// 说明

	public String getKpiPurpose() {
		return kpiPurpose;
	}

	public void setKpiPurpose(String kpiPurpose) {
		this.kpiPurpose = kpiPurpose;
	}

	public String getKpiPoint() {
		return kpiPoint;
	}

	public void setKpiPoint(String kpiPoint) {
		this.kpiPoint = kpiPoint;
	}

	public String getKpiPeriod() {
		return kpiPeriod;
	}

	public void setKpiPeriod(String kpiPeriod) {
		this.kpiPeriod = kpiPeriod;
	}

	public String getKpiExplain() {
		return kpiExplain;
	}

	public void setKpiExplain(String kpiExplain) {
		this.kpiExplain = kpiExplain;
	}

	public String getKpiITSystemIds() {
		return kpiITSystemIds;
	}

	public void setKpiITSystemIds(String kpiITSystemIds) {
		this.kpiITSystemIds = kpiITSystemIds;
	}

	public Long getKpiAndId() {
		return kpiAndId;
	}

	public void setKpiAndId(Long kpiAndId) {
		this.kpiAndId = kpiAndId;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getKpiName() {
		return kpiName;
	}

	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	public String getKpiHorizontal() {
		return kpiHorizontal;
	}

	public void setKpiHorizontal(String kpiHorizontal) {
		this.kpiHorizontal = kpiHorizontal;
	}

	public String getKpiVertical() {
		return kpiVertical;
	}

	public void setKpiVertical(String kpiVertical) {
		this.kpiVertical = kpiVertical;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public Integer getKpiType() {
		return kpiType;
	}

	public void setKpiType(Integer kpiType) {
		this.kpiType = kpiType;
	}

	public String getKpiDefinition() {
		return kpiDefinition;
	}

	public void setKpiDefinition(String kpiDefinition) {
		this.kpiDefinition = kpiDefinition;
	}

	public String getKpiStatisticalMethods() {
		return kpiStatisticalMethods;
	}

	public void setKpiStatisticalMethods(String kpiStatisticalMethods) {
		this.kpiStatisticalMethods = kpiStatisticalMethods;
	}

	public String getKpiTarget() {
		return kpiTarget;
	}

	public void setKpiTarget(String kpiTarget) {
		this.kpiTarget = kpiTarget;
	}

	public Integer getKpiTargetOperator() {
		return kpiTargetOperator;
	}

	public void setKpiTargetOperator(Integer kpiTargetOperator) {
		this.kpiTargetOperator = kpiTargetOperator;
	}

	public Integer getKpiDataMethod() {
		return kpiDataMethod;
	}

	public void setKpiDataMethod(Integer kpiDataMethod) {
		this.kpiDataMethod = kpiDataMethod;
	}

	public Long getKpiDataPeopleId() {
		return kpiDataPeopleId;
	}

	public void setKpiDataPeopleId(Long kpiDataPeopleId) {
		this.kpiDataPeopleId = kpiDataPeopleId;
	}

	public Integer getKpiRelevance() {
		return kpiRelevance;
	}

	public void setKpiRelevance(Integer kpiRelevance) {
		this.kpiRelevance = kpiRelevance;
	}

	public Integer getKpiTargetType() {
		return kpiTargetType;
	}

	public void setKpiTargetType(Integer kpiTargetType) {
		this.kpiTargetType = kpiTargetType;
	}

	public String getFirstTargetId() {
		return firstTargetId;
	}

	public void setFirstTargetId(String firstTargetId) {
		this.firstTargetId = firstTargetId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public String getKpiDataPeopleName() {
		return kpiDataPeopleName;
	}

	public void setKpiDataPeopleName(String kpiDataPeopleName) {
		this.kpiDataPeopleName = kpiDataPeopleName;
	}

	public String getFirstTargetContent() {
		return firstTargetContent;
	}

	public void setFirstTargetContent(String firstTargetContent) {
		this.firstTargetContent = firstTargetContent;
	}

	public String getKpuStringType() {
		if (kpiType != null && kpiType == 0) {
			return JecnProperties.getValue("effectTarget");//结果性指标
		} else if (kpiType != null && kpiType == 1) {
			return JecnProperties.getValue("efficiencyTarget");// 过程性指标
		} else {
			return null;
		}
	}

	public String getKpiStringHorizontal() {
		if ("0".equals(kpiHorizontal)) {
			return JecnProperties.getValue("monthLab");// 月
		} else if ("1".equals(kpiHorizontal)) {
			return JecnProperties.getValue("quarterLab");// 季度
		} else {
			return JecnProperties.getValue("monthLab");// 月
		}
	}

	public String getKpiStringVertical() {
		if (StringUtils.isBlank(kpiVertical)) {
			return JecnProperties.getValue("KPIPercentage");// 百分比
		}
		int verValue = Integer.valueOf(kpiVertical);
		String verStr = "";
		switch (verValue) {
		case 0:
			verStr = JecnProperties.getValue("KPIPercentage");// 百分比
			break;
		case 1:
			verStr = JecnProperties.getValue("quarterLab");// 季度1
			break;
		case 2:
			verStr = JecnProperties.getValue("monthLab");// 月
			break;
		case 3:
			verStr = JecnProperties.getValue("weekLab");// 周3
			break;
		case 4:
			verStr = JecnProperties.getValue("workdDay");// 工作日4
			break;
		case 5:
			verStr = JecnProperties.getValue("dayLab");// 天5
			break;
		case 6:
			verStr = JecnProperties.getValue("KPIHour");// 小时6
			break;
		case 7:
			verStr = JecnProperties.getValue("task_minutes"); // 分钟7
			break;
		case 8:
			verStr = JecnProperties.getValue("oneUnit");// 个8
			break;
		case 9:
			verStr = JecnProperties.getValue("personUnit");// 人9
			break;
		case 10:
			verStr = "ppm";
			break;
		case 11:
			verStr = JecnProperties.getValue("kpiYuan");// 元11
			break;
		case 12:
			verStr = JecnProperties.getValue("kpiSecondary");// 次12
			break;
		default:
			break;
		}
		return verStr;
	}

	public String getKpiStringTargetType() {
		if (kpiTargetType == null) {
			return "";
		} else if (kpiTargetType == 0) {
			return JecnProperties.getValue("twoLevelTarget");// 二级指标
		} else if (kpiTargetType == 1) {
			return JecnProperties.getValue("personPBCTarget");// 个人PBC指标
		} else if (kpiTargetType == 2) {
			return JecnProperties.getValue("otherImpWork");// 其他重要工作
		}
		return "";

	}

	public String getStringkpiRelevance() {
		if (kpiRelevance == null) {
			return "";
		} else if (kpiRelevance == 0) {
			return JecnProperties.getValue("strongCorrelation");// 强相关
		} else if (kpiRelevance == 1) {
			return JecnProperties.getValue("weakCorrelation");// 弱相关
		} else if (kpiRelevance == 2) {
			return JecnProperties.getValue("noCorrelation");// 不相关
		}
		return "";
	}

	public String getKpiDataMethodString() {
		if (kpiDataMethod == null) {
			return JecnProperties.getValue("conManual");// 人工
		} else if (kpiDataMethod == 0) {
			return JecnProperties.getValue("conManual");// 人工
		} else {
			return JecnProperties.getValue("conSys");// 系统
		}
	}

	public String getKpiITSystemNames() {
		return kpiITSystemNames;
	}

	public void setKpiITSystemNames(String kpiITSystemNames) {
		this.kpiITSystemNames = kpiITSystemNames;
	}
}
