package epros.designer.gui.popedom.role.choose;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;

public class RoleManageProcessTreeListener extends JecnTreeListener{
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(RoleManageProcessTreeListener.class);

	public RoleManageProcessTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}
	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if(!jTree.isAllowExpand()){
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = JecnDesignerCommon.getRoleManageProcessTreeBeanList(node.getJecnTreeBean().getId(),false);
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("RoleManageProcessTreeListener treeExpanded is error", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
		
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if(!jTree.isAllowExpand()){
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}
	
	
	}


