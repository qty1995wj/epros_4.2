package com.jecn.epros.domain.process.processFile;

import java.util.List;

public class FlowDriver {
    private String frequency;// 频率
    private String startTime;// 开始时间
    private String endTime;// 结束时间
    private String quantity;// 时间类型：1:日，2:周，3:月，4:季，5:年
    private List<? extends JecnFlowDriverTimeBase> times;

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public List<? extends JecnFlowDriverTimeBase> getTimes() {
        return times;
    }

    public void setTimes(List<? extends JecnFlowDriverTimeBase> times) {
        this.times = times;
    }
}
