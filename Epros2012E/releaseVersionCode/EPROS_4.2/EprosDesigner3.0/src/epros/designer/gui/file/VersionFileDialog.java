package epros.designer.gui.file;

import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.JTree;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

public class VersionFileDialog extends JecnDialog {
	/** 文件版本信息面板 */
	private JScrollPane fileVersionPanel = null;

	public VersionFileDialog(JecnTreeNode selectNode, JTree jTree) {
		// 设置窗体大小
		this.setSize(650, 505);
		this.setMinimumSize(new Dimension(650, 505));
		this.setTitle(JecnProperties.getValue("versionInfo"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);
		this.setModal(true);
		// 文件版本信息面板
		fileVersionPanel = new JScrollPane();
		fileVersionPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 文件使用情况
		FileVersionDialog fileVersionDialog = new FileVersionDialog(jTree, selectNode);

		fileVersionDialog.setVisible(true);
		fileVersionPanel.setViewportView(fileVersionDialog);
		this.getContentPane().add(fileVersionPanel);
	}

}
