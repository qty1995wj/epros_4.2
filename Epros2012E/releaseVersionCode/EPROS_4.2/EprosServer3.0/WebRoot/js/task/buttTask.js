function initEprosButtTask() {
	var mian_index_id="eprosButtTaskGridPanel_id";
	Ext.getCmp("main-panel").removeAll(true);
	Ext.getCmp("main-panel").load({
				url : 'buttTask.action',
				scripts : true,
				callback : function(el, success, response) {
					if (response.responseText.indexOf('login.jsp') > 0) {
						return;
					}
					buttTask();
				}
			});
}
function buttTask() {
	buttTaskStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : 100
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "getMyTask.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "taskId",
			mapping : "taskId"
		}, {
			name : "taskName",
			mapping : "taskName"
		}, {
			name : "taskType",
			mapping : "taskType"
		}, {
			name : "createName",
			mapping : "createName"
		}, {
			name : "taskStage",
			mapping : "taskStage"
		}, {
			name : "stageName",
			mapping : "stageName"
		}, {
			name : "taskElseState",
			mapping : "taskElseState"
		}, {
			name : "updateTime",
			mapping : "updateTime"
		},{
		    name : "reJect",
			mapping : "reJect"
		}])

	});

	buttTaskColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : "taskId",
				dataIndex : "taskId",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_taskName,
				dataIndex : "taskName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_taskType,
				dataIndex : "taskType",
				align : "center",
				menuDisabled : true,
				renderer : taskTypeRenderer,
				hidden : true
			}, {
				header : i_creatName,
				dataIndex : "createName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : "taskStage",
				dataIndex : "taskStage",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_stage,
				dataIndex : "stageName",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : "taskElseState",
				dataIndex : "taskElseState",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : '更新时间',
				dataIndex : "updateTime",
				align : "center",
				menuDisabled : true
			}, {
				header : i_operation,
				align : "center",
				menuDisabled : true,
				renderer : buttTaskRenderer
			} ]);
	buttTaskBbar = new Ext.PagingToolbar( {
		pageSize : 100,
		store : buttTaskStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	buttTaskGridPanel = new Ext.grid.GridPanel( {
		id : "eprosButtTaskGridPanel_id",
		renderTo : 'eprosButtTask_id',
		height : 170,

		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样。
		},
		frame : false,
		store : buttTaskStore,
		colModel : buttTaskColumnModel,
		bbar : buttTaskBbar
	})
}
/**
 * 任务列表操作
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 */
function buttTaskRenderer(value, cellmeta, record) {
	// 任务ID
	var taskId = record.data["taskId"];
	// 任务阶段
	var taskStage = record.data["taskStage"];
	// 拟稿人操作，0是驳回重新提交状态,1是评审意见整理状态
	var taskElseState = record.data["taskElseState"];
	// 任务类型
	var taskType = record.data["taskType"];
	// 任务类型
	var reJect = record.data["reJect"];

	// 审批操作连接
	var varApp;
	// 删除操作连接
	var varDelete;
	// 拟稿人操作
	var varState;
	// div
	var varDiv;

	if (taskElseState == null) {
		return;
	}

	if (taskId == null || taskStage == null) {// 任务主键ID或任务阶段不存在
		return;
	}
	var appUrl = "taskId=" + taskId + "&taskType=" + taskType + "&taskStage="
			+ taskStage;

	var varReject;
	if (reJect != null && reJect == 1) {// 撤回
		varReject = "<a style='cursor: pointer;' onclick='reBackMyTask("
				+ taskId + "," + taskType + ",4);' class='a-operation'>"
				+ i_reCall + "</a>";
	}
	// 查阅
	var varView = "<a target='_blank' href='taskLookUp.action?" + appUrl
			+ "' class='a-operation'>" + i_lookUp + "</a>";
	// 删除操作 "<a href='myTaskDelete.action?" +appUrl+ "'
	// class='a-operation'>"+i_Delete+"</a>";
	varDelete = "<a style='cursor: pointer;' onclick='deleteMyTask(" + taskId
			+ "," + taskType + ");' class='a-operation'>" + i_Delete + "</a>";

	// taskElseState : 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;3:拟稿人无审批状态;4:拟稿人为当前阶段审批人
	if (taskStage == 0 || taskStage == 10 || taskElseState == 3
			|| taskElseState == 4) {// 拟稿人阶段或整理意见阶段
		if (taskElseState == 0) {// 重新提交状态
			varState = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "&taskOperation=" + 1 + "' class='a-operation'>"
					+ i_reSubmit + "</a>";
		} else if (taskElseState == 1) {// 评审意见整理
			varState = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "&taskOperation=" + 2 + "' class='a-operation'>"
					+ i_opinion + "</a>";
		}
		if (taskElseState == 3) {// 拟稿人不是审批人，只有查阅和删除权限
			varDiv = "<div >";
			if (varReject != null) {
				varDiv = varDiv + varReject + " | " + varView;
			} else {
				varDiv = varDiv + varView;
			}
			varDiv = varDiv + "</a><a></a></div>";
		} else if (taskElseState == 4) {// 拟稿人是审批人
			if (varReject != null) {// 存在驳回权限
				alert(varReject);
			}
			varApp = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "' class='a-operation'>" + i_approve + "</a>";
			varDiv = "<div >" + varApp + " | " + varView 
					+ "</a><a></a></div>";
		} else if (taskElseState == 5 && (taskStage == 10 || taskStage == 0)) {// 打回整理撤回操作，或打回操作
			// 我的任务显示撤回按钮，无拟稿人情况
			varDiv = reCallNoDraft(varReject, varView);
		} else {
			varDiv = "<div >" + varState + " | " + varView
					+ "</a><a></a></div>";
		}
	} else if (taskStage != 5) {// 其他审核阶段按钮为审批 (当前审批阶段)
		if (reJect == 1) {// 如果存在驳回状态
			if (taskElseState == 3) {// 拟稿人，只有查阅和删除权限
				// 我的任务显示撤回按钮，拟稿人操作
				varDiv = reCallIsDraft(varReject, varView);
			} else {
				// 我的任务显示撤回按钮，无拟稿人情况
				varDiv = reCallNoDraft(varReject, varView);
			}
		} else {

			varApp = "<a target='_blank' href='taskApprove.action?" + appUrl
					+ "' class='a-operation'>" + i_approve + "</a>";
			// 组织显示的按钮
			varDiv = "<div >" + varApp + " | " + varView + "</a><a></a></div>";
		}
	}
	return varDiv;
}

/**
 * 我的任务显示撤回按钮，无拟稿人情况
 * 
 * @return {}
 */
function reCallNoDraft(varReject, varView) {
	return "<div >" + varReject + " | " + varView + "</a><a></a></div>";
}
/**
 * 我的任务撤回任务
 */
function reBackMyTask(taskId, taskType, taskOperation) {
	// "是否撤回！"
	Ext.MessageBox.confirm(i_tip, i_whetherReCall, function(btn) {
		if (btn == 'yes') {// 点击的是‘是’
				Ext.Ajax.request( {
					url : "taskReCall.action?",
					params : "taskId=" + taskId + "&taskType=" + taskType
							+ "&taskOperation=" + taskOperation,
					success : function() {
					// 撤回成功！
					Ext.MessageBox.alert(i_tip, i_reCallSuccess);
				}
				})
			}

		});
}
