package epros.designer.gui.process.flow;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.RecycleTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程回收 路径
 * 
 * @author 2012-08-08
 * 
 */
public class RecylePathDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(RecylePathDialog.class);
	/** 节点类型 0是流程地图；1是流程；2是组织 */
	private int nodeType;
	/** 选中节点的ID */
	private Long id;

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(500, 500);
	/** 树节点Panel */
	private JecnPanel treePanel = new JecnPanel(470, 420);
	/** 树 */
	/** 树 */
	private FlowRecycleTree systemSetTree = null;
	/** 按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("closeBtn"));
	/**恢复按钮*/
	private JButton recyleBut = new JButton(JecnProperties.getValue("recyBtn"));
	
	private boolean isOpersion =false;

	/**删除的节点ID集合*/
	private List<Long> listIds = new ArrayList<Long>();
	/**是否可操作恢复:0:否，1：是*/
	private int isRecy;
	public RecylePathDialog(Long id, int nodeType,int isRecy) {
		this.setSize(500, 500);
		this.id = id;
		this.nodeType = nodeType;
		this.isRecy = isRecy;
		this.setTitle(JecnProperties.getValue("recyclePath"));// recyclePath
		this.setLocationRelativeTo(null);
		this.setModal(true);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		recyleBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				recyleButPerformed();
			}
		});

		if(isRecy==0){
			recyleBut.setVisible(false);
		}
		initLayout();
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 3, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(treePanel, c);
		treePanel.setBorder(BorderFactory.createTitledBorder(""));
		treePanel.setLayout(new GridBagLayout());
		systemSetTree = new FlowRecycleTree(id, nodeType);
		// 树
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		treePanel.add(systemSetTree, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		treePanel.add(new JLabel(), c);
		// 按钮
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets, 0, 0);
		mainPanel.add(new JLabel(), c);
		//恢复按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(recyleBut, c);
		//关闭按钮
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(cancelBut, c);

		this.getContentPane().add(mainPanel);
	}

	private void cancelButPerformed() {
		this.dispose();
	}
	/**
	 * 恢复删除的文件
	 *
	 */
	private void recyleButPerformed(){
		JecnTreeNode root = (JecnTreeNode) systemSetTree.getModel().getRoot();
		visitAllNodes(root);
		isOpersion = true;
		this.dispose();
		
	}
	/**
	 * 遍历所有节点
	 * @param node
	 */
	public void visitAllNodes(JecnTreeNode node) {
	       if (node.getChildCount() >= 0) {
	           for (Enumeration e=node.children(); e.hasMoreElements(); ) {
	        	   JecnTreeNode treeNode = (JecnTreeNode)e.nextElement();
	        	   //复选框选中的节点对应ID存入List集合
	        	   if(treeNode.isSelected()){
	        		   listIds.add(treeNode.getJecnTreeBean().getId());   
	        	   }
	               visitAllNodes(treeNode);
	           }
	       }
	   }

	class FlowRecycleTree extends RecycleTree {

		public FlowRecycleTree(Long id, int nodeType) {
			super(id, nodeType);
		}

		@Override
		public List<JecnTreeBean> getTreeData() {
			List<JecnTreeBean> list = null;
			try {
				if (nodeType == 1 || nodeType == 0) {
					list = ConnectionPool.getProcessAction()
							.getParentsContainSelf(JecnConstants.projectId, id);
				} else if (nodeType == 2) {
					list = ConnectionPool.getOrganizationAction()
							.getParentsContainSelf(JecnConstants.projectId, id);
				}

			} catch (Exception e) {
				log.error("RecylePathDialog FlowRecycleTree is error", e);
				return null;
			}
			return list;
		}

		@Override
		public int viewType() {
			return nodeType;
		}

		@Override
		public Long getTreeId() {
			return id;
		}

	}

	public List<Long> getListIds() {
		return listIds;
	}

	public void setListIds(List<Long> listIds) {
		this.listIds = listIds;
	}

	public boolean isOpersion() {
		return isOpersion;
	}

	public void setOpersion(boolean isOpersion) {
		this.isOpersion = isOpersion;
	}
	
}
