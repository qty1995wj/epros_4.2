package epros.draw.gui.figure.shape;

import java.awt.BasicStroke;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.util.JecnUIUtil;

public class PaintOvalFigure {
	private JecnFigureData flowElementData = null;

	public PaintOvalFigure(JecnBaseFigurePanel figurePanel) {
		flowElementData = figurePanel.getFlowElementData();
	}

	public void paintNoneFigure(Graphics2D g2d, int x1, int y1, int userWidth, int userHeight) {
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth), Math.abs(y1
				- userHeight));
	}

	public void paintInputHandTop(Graphics2D g2d, int shadowCount, int x1, int y1, int userWidth, int userHeight) {
		// 顶层 原图，榜值均为零
		g2d.fillOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - shadowCount, Math
				.abs(y1 - userHeight)
				- shadowCount);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - shadowCount, Math
				.abs(y1 - userHeight)
				- shadowCount);
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DHandLow(Graphics2D g2d, int indent3Dvalue, int x1, int y1, int userWidth, int userHeight) {
		g2d.fillOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - indent3Dvalue, Math
				.abs(y1 - userHeight)
				- indent3Dvalue);
		g2d.setStroke(flowElementData.getBasicStroke());
	}

	public void paint3DOvalLine(Graphics2D g2d, int indent3Dvalue, int x1, int y1, int userWidth, int userHeight) {
		g2d.drawOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - indent3Dvalue, Math
				.abs(y1 - userHeight)
				- indent3Dvalue);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d, int x1, int y1, int userWidth, int userHeight) {
		g2d.fillOval(Math.min(x1, userWidth) + 4, Math.min(y1, userHeight) + 4, Math.abs(x1 - userWidth) - 4 - 4, Math
				.abs(y1 - userHeight) - 4 - 4);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d, int x1, int y1, int userWidth, int userHeight) {
		g2d.fillOval(Math.min(x1, userWidth) + 4, Math.min(y1, userHeight) + 4, Math.abs(x1 - userWidth) - 4 - 4 - 4,
				Math.abs(y1 - userHeight) - 4 - 4 - 4);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	public void paintShadows(Graphics2D g2d, int shadowCount, int x1, int y1, int userWidth, int userHeight) {
		// 阴影
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillOval(Math.min(x1, userWidth) + shadowCount, Math.min(y1, userHeight) + shadowCount, Math.abs(x1
				- userWidth)
				- shadowCount, Math.abs(y1 - userHeight) - shadowCount);
		g2d.setPaint(JecnUIUtil.getShadowBodyColor());
		g2d.setStroke(new BasicStroke());
		g2d.drawOval(Math.min(x1, userWidth) + shadowCount, Math.min(y1, userHeight) + shadowCount, Math.abs(x1
				- userWidth)
				- shadowCount, Math.abs(y1 - userHeight) - shadowCount);
	}

}
