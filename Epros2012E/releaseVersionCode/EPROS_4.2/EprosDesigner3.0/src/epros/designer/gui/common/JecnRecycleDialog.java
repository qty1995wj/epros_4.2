package epros.designer.gui.common;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/***
 * 回收站
 * @author hyl
 * 回收站弹出框
 */

public class JecnRecycleDialog extends JecnDialog {

	/** 主面板 */
	private JecnPanel mainPanel = null;
	/** 按钮面板 */
	private JecnPanel butPanel = new JecnPanel();
	/** 关闭按钮 */
	private JButton closeBut = new JButton(JecnProperties.getValue("closeBtn"));

	public JecnRecycleDialog(JecnPanel mainPanel) {
		this.mainPanel = mainPanel;
		this.setSize(800, 500);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		initCompotents();
		initLayout();

	}

	private void initCompotents() {
		// 设置背景颜色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		butPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 关闭按钮
		closeBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeDialog();
			}
		});
	}

	private void closeDialog() {
		this.dispose();
	}

	private void initLayout() {      

		this.setLayout(new BorderLayout());
		// 主面板
		this.add(mainPanel, BorderLayout.CENTER);
		// 关闭按钮面板
		this.add(butPanel, BorderLayout.SOUTH);

		// 关闭面板
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 按钮面板添加组件
		butPanel.add(closeBut);

	}

}
