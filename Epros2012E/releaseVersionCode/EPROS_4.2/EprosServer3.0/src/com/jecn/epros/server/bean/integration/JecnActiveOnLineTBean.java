package com.jecn.epros.server.bean.integration;

import java.io.Serializable;
import java.util.Date;

import com.jecn.epros.server.common.JecnCommon;

/**
 * 
 * 活动与信息化关系临时表
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-12 时间：下午05:01:46
 */
public class JecnActiveOnLineTBean implements Serializable {
	private String id;
	/** 流程ID */
	private Long processId;
	/** 活动ID */
	private Long activeId;
	/** 支持工具ID */
	private Long toolId;
	/** 上线时间 */
	private Date onLineTime;
	/** 系统名称 */
	private String sysName;

	private String strTime;
	/** url地址*/
	private String linkUrl;

	/** 信息化说明 */
	private String figureUUID;
	private String informationDescription;

	public String getStrTime() {
		strTime = JecnCommon.getStringbyDate(onLineTime);
		return strTime;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getProcessId() {
		return processId;
	}

	public void setProcessId(Long processId) {
		this.processId = processId;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getToolId() {
		return toolId;
	}

	public void setToolId(Long toolId) {
		this.toolId = toolId;
	}

	public Date getOnLineTime() {
		return onLineTime;
	}

	public void setOnLineTime(Date onLineTime) {
		this.onLineTime = onLineTime;
	}

	public String getInformationDescription() {
		return informationDescription;
	}

	public void setInformationDescription(String informationDescription) {
		this.informationDescription = informationDescription;
	}

	public void setStrTime(String strTime) {
		this.strTime = strTime;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	
	
	
	
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
}
