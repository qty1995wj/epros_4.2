<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>
<html>
<head>

<script type="text/javascript">
var taskTotal="${taskTotal}";
</script>
</head>
<body>
<div id="myhome_id" style="width: 100%;height:100%">
<div id='myhomeLeft_id' style="width:175px;height:100%;margin-left:2px;padding:0px;background:#F2F2F2;;">
		<ul id=myhomeUl_id>
			<li id="${basePath}jsp/myhome/positionRepository.jsp;positionRepository" style="padding-left:30px; text-align: left;"><s:text name="myProcess"/></li><!-- 岗位知识库 -->
			<s:if test="#application.isHiddenSystem==0">
			<li id="${basePath}jsp/myhome/attentionRule.jsp;atentionRuleGrid" style="padding-left:30px; text-align: left;"><s:text name="myRule"/></li><!-- 我的制度 -->
			</s:if>
			<li id="${basePath}jsp/myhome/myJobDescription.jsp;posonChanged" style="padding-left:30px; text-align: left;"><s:text name="myPositionDetail"/></li><!-- 我的岗位说明书-->
			<li id="initSearch.action;myTask" style="padding-left:30px; text-align: left;"><s:text name="myTask"/></li><!-- 我的体系任务 -->
			<li id="${basePath}jsp/myhome/myMaterial.jsp" style="padding-left:30px; text-align: left;"><s:text name="myInformation"/></li><!-- 个人信息 -->
			<li id="${basePath}jsp/myhome/attentionFile.jsp;atentionFileGrid" style="padding-left:30px; text-align: left;"><s:text name="myRepository"/></li><!-- 我的知识库-->
		</ul>
</div>
</div>
</body>
</html>