package epros.draw.gui.top.shortcut;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import epros.draw.constant.JecnShortcutsConstants;
import epros.draw.gui.figure.unit.JecnFlowElementCopyAndPaste;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnTextArea;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;

/**
 * 
 * 快捷键动作处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAbstractAction extends AbstractAction {

	/** 唯一标识 */
	private String actionKey = null;

	public JecnAbstractAction(String actionKey) {
		this.actionKey = actionKey;
	}

	public void actionPerformed(ActionEvent e) {
		// 截图标识
		boolean isIconshot = false;
		// 保存标识
		boolean isSave = false;
		if (JecnShortcutsConstants.KEY_OPEN.equals(actionKey)) {// 打开流程图/流程地图/组织图Ctrl+O
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getFileToolBarItemPanel().getOpenMapBtn());
		} else if (JecnShortcutsConstants.KEY_SAVE.equals(actionKey)) { // 保存Ctrl+S
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getSaveBtn());
			isSave = true;
		} else if (JecnShortcutsConstants.KEY_SAVE_AS.equals(actionKey)) {// 另存为Ctrl+Shift+A
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getSaveAsBtn());
		} else if (JecnShortcutsConstants.KEY_SAVE_IMAGE.equals(actionKey)) {// 导出为图像Ctrll+Shift+G
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getSaveAsImageBtn());
		} else if (JecnShortcutsConstants.KEY_UNDO.equals(actionKey)) {// 撤销Ctrl+Z
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getUndoBtn());
		} else if (JecnShortcutsConstants.KEY_REDO.equals(actionKey)) {// 恢复Ctrl+Y
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getRedoBtn());
		} else if (JecnShortcutsConstants.KEY_CUT.equals(actionKey)) {// 剪切Ctrl+X
			// 正在编辑就不执行自定义快捷键，主要是解决编辑中（JtextArea）全选、剪切、粘贴、复制功能与自定义全选、剪切、粘贴、复制冲突问题
			if (e.getSource() instanceof JecnTextArea) {
				return;
			}
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getCutBtn());
		} else if (JecnShortcutsConstants.KEY_COPY.equals(actionKey)) {// 复制Ctrl+C
			// 正在编辑就不执行自定义快捷键，主要是解决编辑中（JtextArea）全选、剪切、粘贴、复制功能与自定义全选、剪切、粘贴、复制冲突问题
			if (e.getSource() instanceof JecnTextArea) {
				return;
			}
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getCopyBtn());
		} else if (JecnShortcutsConstants.KEY_PASTE.equals(actionKey)) {// 粘贴Ctrl+V
			// 正在编辑就不执行自定义快捷键，主要是解决编辑中（JtextArea）全选、剪切、粘贴、复制功能与自定义全选、剪切、粘贴、复制冲突问题
			if (e.getSource() instanceof JecnTextArea) {
				return;
			}
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getPasteBtn());
		} else if (JecnShortcutsConstants.KEY_DELETE.equals(actionKey)) {// 删除Delete
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getDeleteBtn());
		} else if (JecnShortcutsConstants.KEY_ALL_SELECT.equals(actionKey)) {// 全选Ctrl+A
			// 正在编辑就不执行自定义快捷键，主要是解决编辑中（JtextArea）全选、剪切、粘贴、复制功能与自定义全选、剪切、粘贴、复制冲突问题
			if (e.getSource() instanceof JecnTextArea) {
				return;
			}
			JecnAddFlowElementUnit.selectAllFigure();
			JecnWorkflowUtil.clearStates();
			return;
		} else if (JecnShortcutsConstants.KEY_BIG_IMAGE.equals(actionKey)) { // Ctrl+
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getZoomBigBtn());
		} else if (JecnShortcutsConstants.KEY_SMALL_IMAGE.equals(actionKey)) { // 截图Ctrl-
			e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getZoomSmallBtn());
		} else {
			return;
		}

		// 清除 拖动添加图形等操作产生的临时数据，流程元素库选中指针状态
		// boolean true：右键菜单显示返回状态；false:其他操作
		if (!isSave) {// 非保存
			if (JecnWorkflowUtil.clearStates()) {
				return;
			}
		}

		if (isIconshot) {// 截图
			JecnFlowElementCopyAndPaste.iconShot();
			return;
		}

		// 执行
		JecnDrawMainPanel.getMainPanel().actionPerformed(e);
	}
}
