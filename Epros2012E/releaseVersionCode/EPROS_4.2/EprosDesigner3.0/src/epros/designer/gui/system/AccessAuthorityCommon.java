package epros.designer.gui.system;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.ConfigCommon.ConfigCommonEnum;
import epros.designer.gui.system.combo.JecnSecurityLevelCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class AccessAuthorityCommon {
	private static Logger log = Logger.getLogger(AccessAuthorityCommon.class);
	/** 密级Lab */
	private JLabel isPublicLab = new JLabel(JecnProperties.getValue("isScretC"));
	/** 密级ComboBox */
	private JComboBox isPublicCombox = new JComboBox(new String[] { JecnProperties.getValue("secret"),
			JecnProperties.getValue("gongKai") });

	/** 密级 0是秘密 1是公开 */
	private int isPublic;

	private List<JecnTreeBean> orglist = new ArrayList<JecnTreeBean>();
	private List<JecnTreeBean> poslist = new ArrayList<JecnTreeBean>();
	private List<JecnTreeBean> posGroupList = new ArrayList<JecnTreeBean>();

	private int rows;

	/** 部门多选 */
	DepartMutilSelectCommon departMutilSelectCommon = null;
	/** 岗位多选 */
	PosMutilSelectCommon posMutilSelectCommon = null;
	/** 岗位组多选 */
	PosGroupMutilSelectCommon posGroupMutilSelectCommon = null;

	/** 保密级别 */
	private int confidentialityLevel = 0;
	JecnSecurityLevelCommon jecnSecurityLevelCommon = null;

	private int scurityLevel;

	public AccessAuthorityCommon(int row, JecnPanel infoPanel, Insets insets, Long relateId, int type,
			JecnDialog jecnDialog, boolean isNew, boolean isDownLoad, String securityLevelName) {
		this(row, infoPanel, insets, relateId, type, jecnDialog, isNew, isDownLoad);
		if(jecnSecurityLevelCommon != null){
			jecnSecurityLevelCommon.getJecnLab().setText(securityLevelName);
			isPublicLab.setText(JecnProperties.getValue("levelC"));
		}
	}

	public AccessAuthorityCommon(int row, JecnPanel infoPanel, Insets insets, Long relateId, int type,
			JecnDialog jecnDialog, boolean isNew, boolean isDownLoad) {
		LookPopedomBean lookPopedomBean = null;
		isDownLoad = isDownLoad ? getIsDownLoadType(type, isDownLoad) : isDownLoad;
		try {
			lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(relateId, type);
		} catch (Exception e1) {
			log.error(JecnProperties.getValue("getAccessAuthorityException"), e1);
			JecnOptionPane.showMessageDialog(jecnDialog, JecnProperties.getValue("getAccessAuthorityException"));
		}
		if (lookPopedomBean.getIsPublic() == null) {
			isPublic = 0;
		} else {
			if (isNew) { // 新建的情况下取 系统配置权限中的 公开秘密
				if (type == 0) { // 流程
					isPublic = JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubPartMap) ? 1 : 0;
				} else if (type == 1) {
					isPublic = JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubFile) ? 1 : 0;
				} else if (type == 2) {
					isPublic = JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubStand) ? 1 : 0;
				} else if (type == 3) {
					isPublic = JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubRule) ? 1 : 0;
				}
			} else {
				isPublic = lookPopedomBean.getIsPublic().intValue();
			}
		}
		rows = row;
		Dimension dimension = new Dimension(450, 20);
		isPublicCombox.setPreferredSize(dimension);
		isPublicCombox.setMaximumSize(dimension);
		isPublicCombox.setMinimumSize(dimension);
		if (isPublic == 0) {
			isPublicCombox.setSelectedIndex(0);
		} else if (isPublic == 1) {
			isPublicCombox.setSelectedIndex(1);
		}

		// ==============保密等级
		// 1:显示；2：必填==========================================
		scurityLevel = scurityLevelValue(type);
		if (scurityLevel != 0) {
			// 保密级别
			if (lookPopedomBean.getConfidentialityLevel() != null
					&& lookPopedomBean.getConfidentialityLevel().intValue() != -1) {
				confidentialityLevel = lookPopedomBean.getConfidentialityLevel();
			}
			jecnSecurityLevelCommon = new JecnSecurityLevelCommon(rows, infoPanel, insets, String
					.valueOf(confidentialityLevel), jecnDialog, scurityLevel == 2);
			rows++;
			confidentialityLevel = getConfidentialityLevelStatic();
		}

		// ==============密级==========================================
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(isPublicLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(isPublicCombox, c);
		isPublicCombox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {// 公开 隐藏
					hiddenOrShowCompotents();
				}
			}
		});
		rows++;

		// ============= 部门权限=========================================
		orglist = lookPopedomBean.getOrgList();
		departMutilSelectCommon = new DepartMutilSelectCommon(rows, infoPanel, insets, orglist, jecnDialog, false,
				JecnProperties.getValue("departCompetenceC"), isDownLoad);
		rows++;
		
		// ============= 岗位权限=========================================
		poslist = lookPopedomBean.getPosList();
		posMutilSelectCommon = new PosMutilSelectCommon(rows, infoPanel, insets, poslist, jecnDialog, false, isDownLoad);
		rows++;
		
		// ================岗位组权限=======================================
		posGroupList = lookPopedomBean.getPosGroupList();
		posGroupMutilSelectCommon = new PosGroupMutilSelectCommon(rows, infoPanel, insets, posGroupList, jecnDialog,
				false, isDownLoad);
		rows++;
		hiddenOrShowCompotents();
	}

	private void hiddenOrShowCompotents() {
		if (JecnConfigTool.pubDisableAuthSelect(isPublicCombox.getSelectedIndex())) {
			departMutilSelectCommon.hiddenCompotents();
			posMutilSelectCommon.hiddenCompotents();
			posGroupMutilSelectCommon.hiddenCompotents();
		} else {
			departMutilSelectCommon.showCompotents();
			posMutilSelectCommon.showCompotents();
			posGroupMutilSelectCommon.showCompotents();
		}
	}

	private boolean getIsDownLoadType(int type, boolean isDownLoad) {
		if (type == 0) { // 流程
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessDownLoad);
		} else if (type == 1) {
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileDownLoad);
		} else if (type == 3) {
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleDownLoad);
		}
		return isDownLoad;
	}

	/**
	 * 获取保密级别 是否必填
	 * 
	 * @param type
	 *            0是流程1是文件2是标准3制度
	 * @return 1:显示；2：必填
	 */
	private int scurityLevelValue(int type) {
		if (type == 0) {
			return JecnConfigTool.getResultValue(ConfigItemPartMapMark.securityLevel.toString());
		} else if (type == 1) {
			return JecnConfigTool.getResultValue(ConfigItemPartMapMark.fileScurityLevel.toString());
		} else if (type == 3) {
			return JecnConfigTool.getResultValue(ConfigItemPartMapMark.ruleScurityLevel.toString());
		}
		return 0;
	}

	/**
	 * 获得下一次的添加行数
	 * 
	 * @return
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	public boolean isUpdate() {

		if (scurityLevel != 0) {
			if (getConfidentialityLevelStatic() != confidentialityLevel) {
				return true;
			}
		}

		// 密级是否修改
		if (getPublicStatic() != isPublic) {
			return true;
		}
		// 部门查阅权限
		if (departMutilSelectCommon.isUpdate()) {
			return true;
		}
		// 岗位查阅权限
		if (posMutilSelectCommon.isUpdate()) {
			return true;
		}
		// 岗位组查阅权限
		if (posGroupMutilSelectCommon.isUpdate()) {
			return true;
		}
		return false;
	}

	/**
	 * 获得保密级别
	 * 
	 * @return
	 */
	public int getConfidentialityLevelStatic() {
		if (jecnSecurityLevelCommon != null) {
			String key = jecnSecurityLevelCommon.getKey();
			if (StringUtils.isNotBlank(key)) {
				ConfigCommon.INSTANTCE.writeCommonConfigProperties(key, ConfigCommonEnum.securityLevel);
				return Integer.parseInt(key);
			}
		}
		return 0;
	}

	/**
	 * 获得公开密级
	 * 
	 * @return
	 */
	public int getPublicStatic() {
		return isPublicCombox.getSelectedIndex();
	}

	/**
	 * 获取 查阅权限ID 数组
	 * 
	 * @return
	 */
	public String getOrgIds() {
		return departMutilSelectCommon.getResultIds();
	}

	public String getPosIds() {
		return posMutilSelectCommon.getResultIds();
	}

	public String getPosGroupIds() {
		return posGroupMutilSelectCommon.getResultIds();
	}

	/**
	 * 获取查阅权限ID 集合 带下载权限标识
	 * 
	 * @return
	 */
	public List<AccessId> getOrgAccessIds() {
		return departMutilSelectCommon.getResultAccessIds();
	}

	public List<AccessId> getPosAccessIds() {
		return posMutilSelectCommon.getResultAccessIds();
	}

	public List<AccessId> getPosGroupAcessIds() {
		return posGroupMutilSelectCommon.getResultAccessIds();
	}
}
