package epros.draw.designer;

import java.util.ArrayList;
import java.util.List;

public class JecnFigureInoutT {
	// 主键
	private String id;
	private Long flowId;
	// 活动ID
	private Long figureId;
	// 活动ID
	private String figureUUID;
	// 0 输入 1 输出
	private Integer type;
	// 输入 or 输出名称
	private String name;
	// 说明
	private String explain;
	// 模板ID
	private Long fileId;
	/** 模板 文件名称 不存数据库 */
	private String fileName;
	private Long sortId;
	/** 样例 */
	private List<JecnFigureInoutSampleT> listSampleT;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public List<JecnFigureInoutSampleT> getListSampleT() {
		return listSampleT;
	}

	public void setListSampleT(List<JecnFigureInoutSampleT> listSampleT) {
		this.listSampleT = listSampleT;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public JecnFigureInoutT clone() {
		JecnFigureInoutT clone = new JecnFigureInoutT();
//		clone.setId(id);
		clone.setFlowId(flowId);
		clone.setExplain(explain);
		clone.setFigureId(figureId);
		clone.setFileId(fileId);
		clone.setFileName(fileName);
		clone.setName(name);
		clone.setSortId(sortId);
		clone.setType(type);
		clone.setFigureUUID(figureUUID);
		if (this.listSampleT != null) {
			List<JecnFigureInoutSampleT> cloneList = new ArrayList<JecnFigureInoutSampleT>();
			for (JecnFigureInoutSampleT inoutSampleT : this.listSampleT) {
				cloneList.add(inoutSampleT.clone());
			}
			clone.setListSampleT(cloneList);
		}
		return clone;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JecnFigureInoutT) {
			JecnFigureInoutT bean = (JecnFigureInoutT) obj;
			if (this.name != null && bean.getName() != null) {
				if (!this.name.equals(bean.getName())) {
					return false;
				}
			}
			if (this.explain != null && bean.getExplain() != null) {
				if (!this.explain.equals(bean.getExplain())) {
					return false;
				}
			}
			if (this.fileId != null && bean.getFileId() != null) {
				if (!this.fileId.equals(bean.getFileId())) {
					return false;
				}
			}
			if (this.listSampleT != null && bean.getListSampleT() != null) {
				if (this.listSampleT.size() != bean.getListSampleT().size()) {
					return false;
				}
				if (!this.listSampleT.containsAll(bean.getListSampleT())) {
					return false;
				}
			}
			if (this.sortId != null && bean.getSortId() != null) {
				if (!this.sortId.equals(bean.getSortId())) {
					return false;
				}
			} else {
				return false;
			}
			return true;
		}

		return super.equals(obj);
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public Long getSortId() {
		return sortId;
	}

	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

}
