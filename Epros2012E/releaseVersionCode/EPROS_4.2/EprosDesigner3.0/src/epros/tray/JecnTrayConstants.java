package epros.tray;

public class JecnTrayConstants {
	/** 数据库配置文件 */
	static final String DB_CONFIG_FILE = "/server/jecn/EprosServer3.0/WEB-INF/classes/database.properties";
	/** JecnConfig.properties****/
	static final String JECN_CONFIG = "/resources/JecnConfig.properties";
	/** TOMCAT配置文件 */
	static final String TOMCAT_CONFIG_FILE = "/server/conf/server.xml";
	
	/** MYSQL5 配置文件 */
	static final String MYSQL_CONFIG_FILE = "/mysql/my.ini";

	/** epros软件在window任务管理器中显示进程名称 */
	static final String EPROS_NAME = "EprosServer3.0.exe";

	// tomcat服务进程名称
	static final String TOMCAT_NAME = "jecneprosServerD2";
	// mysql服务进程名称
	static final String MYSQL_NAME = "jecneprosDBD2";

	/** ************CMD命令************* */
	static final String CMD_TASKLIST = "tasklist";
	// 强行终止tomcat服务
	static final String CMD_TASKKILL_SERVER = "taskkill /im jecneprosServerD2.exe /f";
	// 强行终止mysql服务
	static final String CMD_TASKKILL_DB = "taskkill /im jecneprosDBD2.exe /f";
	
	static final String CMD_NET_STOP_EPROSSERVER3 = "net stop jecneprosServerD2";
	static final String CMD_NET_START_EPROSSERVER3 = "net start jecneprosServerD2";
	static final String CMD_NET_START_ORACLE = "net start OracleServiceXE";
	static final String CMD_NET_START_ORACLE_LISTENER = "net start OracleXETNSListener";

	static final String CMD_NET_START_MYSQL5 = "net start jecneprosDBD2";
	static final String CMD_NET_STOP_MYSQL5 = "net stop jecneprosDBD2";
	/** ************CMD命令************* */
}
