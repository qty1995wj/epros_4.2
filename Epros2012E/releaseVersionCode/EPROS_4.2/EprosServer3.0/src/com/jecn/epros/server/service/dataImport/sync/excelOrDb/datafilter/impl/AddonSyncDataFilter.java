package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncSpecialDataFilter;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;
import com.jecn.webservice.addon.OAPersonnelBaseData;
import com.jecn.webservice.addon.OAPersonnelBaseDataService;
import com.jecn.webservice.addon.PROP;
import com.jecn.webservice.addon.RETURNMSG;

/**
 * 基础过滤类
 * 
 * @author weidp
 * @date 2014-07-30
 */
public class AddonSyncDataFilter extends AbstractSyncSpecialDataFilter {

	public AddonSyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	@Override
	protected void initSpecialDataPath() {
		this.specialDataPath = JecnPath.CLASS_PATH + "cfgFile/addon/addonSyncSpecialData.xls";
	}

	@Override
	public void filterDeptDataBeforeCheck() {

	}

	@Override
	public void removeUnusefulDataAfterCheck() {

	}

	@Override
	public void filterUserPosDataBeforeCheck() {
		removeUserErrorData();
		setUserEmailInfo();
	}

	/**
	 * 添加用户Email
	 * 
	 * @throws Exception
	 */
	private void setUserEmailInfo() {
		OAPersonnelBaseData port = new OAPersonnelBaseDataService(OAPersonnelBaseDataService.WSDL_LOCATION).getDomino();
		RETURNMSG allUserInfo = port.getpersonnelallinfo("crmuser", "123321");
		if (allUserInfo.getEMPLOYEELIST() == null || allUserInfo.getEMPLOYEELIST().getEMPLOYEE().size() == 0) {
			log.error("未获取任何数据，the return message is" + allUserInfo.getMESSAGE());
			return;
		}

		List<PROP> employee = allUserInfo.getEMPLOYEELIST().getEMPLOYEE();
		for (UserBean userBean : syncData.getUserPosBeanList()) {
			for (int i = employee.size() - 1; i >= 0; i--) {
				if (employee.get(i) == null) {
					employee.remove(i);
					continue;
				}
				// Domino 数据中 epmloyeeId没有进行左右字符串的空格处理
				if (employee.get(i).getEMPLOYEEID().equals(userBean.getUserNum())) {
					userBean.setEmailType(0);
					userBean.setEmail(employee.get(i).getEMAIL());
					employee.remove(i);
					break;
				}
			}
		}
	}

	/**
	 * 删除数据错误的用户
	 */
	private void removeUserErrorData() {
		Set<String> removeUserNum = new HashSet<String>(Arrays.asList("01006461"));
		Set<String> removeDeptNum = new HashSet<String>(Arrays.asList("00000000"));
		for (int i = syncData.getUserPosBeanList().size() - 1; i >= 0; i--) {
			UserBean userBean = syncData.getUserPosBeanList().get(i);
			if (removeUserNum.contains(userBean.getUserNum()) || removeDeptNum.contains(userBean.getDeptNum())) {
				syncData.getUserPosBeanList().remove(i);
			}
		}
	}
}
