package com.jecn.webservice.zhongsh.flowpush.bean;

import org.apache.commons.lang.StringUtils;

/**
 * 活动
 * 
 * @author user
 * 
 */
public class WBState {

	private String guid = "";
	private String stateName = "";
	private String description = "";
	private String type = "Normal";
	private String colorSchema = "Default";
	private String positionX = "";
	private String positionY = "";
	/** 开始和 结束 为2和2,；分支和聚合2,2 DECISION：2,2 */
	private String stateWidth = "4";
	private String stateHeight = "2";

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName == null ? "" : stateName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description == null ? "" : description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = StringUtils.isEmpty(type) ? "Normal" : type;
	}

	public String getColorSchema() {
		return colorSchema;
	}

	public void setColorSchema(String colorSchema) {
		this.colorSchema = colorSchema;
	}

	public String getPositionX() {
		return positionX;
	}

	public void setPositionX(String positionX) {
		this.positionX = positionX;
	}

	public String getPositionY() {
		return positionY;
	}

	public void setPositionY(String positionY) {
		this.positionY = positionY;
	}

	public String getStateWidth() {
		return stateWidth;
	}

	public void setStateWidth(String stateWidth) {
		this.stateWidth = stateWidth;
	}

	public String getStateHeight() {
		return stateHeight;
	}

	public void setStateHeight(String stateHeight) {
		this.stateHeight = stateHeight;
	}
}
