<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="attentionProcesssearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td height="25px" width="10%" align="right">
								<!-- 流程名称： -->
								<s:text name="processNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;"
									id="attentionProcessName_id" maxLength="32" />
							</td>


							<td width="10%" align="right">
								<!-- 流程编号： -->
								<s:text name="processNumberC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;"
									id="attentionProcessNumber_id" maxLength="32" />

							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a
									onClick="toggleDiv(attentionProcesssearchText,'aproPackUp','aproSearchVal')"
									style="cursor: pointer;" class="packUp"> <img
										id="aproPackUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="aproSearchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td height="25px" align="right">
								<!-- 责任部门： -->
								<s:text name="responsibilityDepartmentC" />
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="attentionOrgName_id" maxLength="32" readonly="readonly" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","attentionOrgName_id","attentionOrg_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
							</td>
							<!-- 责任部门ID -->
							<input type="hidden" id="attentionOrg_id" value='-1'/>

							<td align="right">
								<!-- 密集: -->
								<s:text name="intensiveC" />
							</td>
							<td align="left">
								<select style="width: 80%;" id="attentionProcess_intensive">
									<option value="0" selected="selected">
										${secret}
									</option>
									<option value="1">
										<s:property value="#application.public"/>
									</option>
									<option value="-1">
										<s:text name="all" />
									</option>
									
									
								</select>
							</td>
							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="attentionProcessSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="attentionProcessReset()" />
				</div>
			</div>

			<div id="atentionProcessGrid_id">

			</div>
		</div>
	</body>
</html>