package epros.designer.gui.popedom.positiongroup;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class PosGroupSortDialog extends JecnSortDialog {
	private static Logger log = Logger.getLogger(PosGroupSortDialog.class);
	public PosGroupSortDialog(JecnTreeNode pNode, JecnTree jTree){
		super(pNode,jTree);
	}

	@Override
	public List<JecnTreeBean> getChild() {
		try {
			return ConnectionPool.getPosGroup().getChildPositionGroup(
					this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("PosGroupSortDialog getChild is error！",e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			ConnectionPool.getPosGroup().updateSortPosGroup(list,
					this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId,JecnUtil.getUserId());
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("sortSuccess"));
//			this.dispose();
			return true;
		} catch (Exception e) {
			log.error("PosGroupSortDialog saveData is error！",e);
			return false;
		}
	}

}
