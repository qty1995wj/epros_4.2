package com.jecn.epros.domain.user;

/**
 * Created by admin on 2017/3/10.
 */
public enum PermissionsMenuEnum {
    DASHBOARD, PROCESS, TASK, SUGGEST, SYSTEM, REPORTS,COMPLIANCE, RULE, STANDARD, RISK, FILE, ORGANIZATION,LIFECYCLE,ABOLISH
}
