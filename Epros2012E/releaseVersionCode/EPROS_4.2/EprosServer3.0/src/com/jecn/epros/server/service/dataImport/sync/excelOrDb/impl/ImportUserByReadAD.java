package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.web.login.ad.envicool.EnvicoolLoginAction;
import com.jecn.epros.server.action.web.login.ad.envicool.JecnEnvicoolItem;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LdapAuthVerifier;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class ImportUserByReadAD {

	protected final Log log = LogFactory.getLog(this.getClass());

	protected List<UserBean> userList = new ArrayList<UserBean>();

	public ImportUserByReadAD() {
		Ldapbyuserinfo();
	}

	/**
	 * 英维克-域用户获取
	 */
	@SuppressWarnings("unchecked")
	public boolean Ldapbyuserinfo() {
		LdapAuthVerifier ldapAuthVerifier = EnvicoolLoginAction.getLdapAuthVerifier();
		int result = ldapAuthVerifier.verifyAuth();

		log.info("权限验证结果:" + result);
		// 验证成功
		if (result != LdapAuthVerifier.SUCCESS) {
			return false;
		}
		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String searchFilter = JecnEnvicoolItem.FILTER_TYPE + ")";

		// searchFilter = "(&(objectClass=user)(cn=*" + "xiaohu" + "*))";
		// Specify the Base for the search 搜索域节点
		// String searchBase = "DC=jecn,DC=com,DC=cn";
		String returnedAtts[] = { "employeeID", "name", "userPrincipalName", "sAMAccountName", "mail" }; // 定制返回属性
		searchCtls.setReturningAttributes(returnedAtts); // 设置返回属性集

		try {
			NamingEnumeration answer = ldapAuthVerifier.verifyAndGetContext().search(JecnEnvicoolItem.BASE,
					searchFilter, searchCtls);
			if (answer == null) {
				return false;
			}
			UserBean userBean = null;
			while (answer.hasMoreElements()) {// 遍历结果集
				SearchResult sr = (SearchResult) answer.next();// 得到符合搜索条件的DN
				if (sr.getName() == null) {
					continue;
				}
				log.info(sr);
				if (sr.getName().contains("CN=Users") || sr.getName().contains("CN=Computers")
						|| sr.getName().contains("OU=test") || sr.getName().contains("CN=PDC")) {
					continue;
				}
				Attributes Attrs = sr.getAttributes();// 得到符合条件的属性集
				if (Attrs == null) {
					continue;
				}
				try {
					userBean = new UserBean();
					for (NamingEnumeration ne = Attrs.getAll(); ne.hasMore();) {
						Attribute attr = (Attribute) ne.next();// 得到下一个属性
						// 定制返回属性
						if (attr.getID().equals("sAMAccountName")) {
							userBean.setUserNum(attr.get().toString());
						} else if (attr.getID().equals("name")) {
							userBean.setTrueName(attr.get().toString());
						} else if (attr.getID().equals("mail") && StringUtils.isBlank(userBean.getEmail())) {
							userBean.setEmail(attr.get().toString());
							userBean.setEmailType(0);
						} else if (attr.getID().equals("telephoneNumber")) {
							userBean.setPhone(attr.get().toString());
						} else if (attr.getID().equals("userPrincipalName") && StringUtils.isBlank(userBean.getEmail())) {
							userBean.setEmail(attr.get().toString());
							userBean.setEmailType(0);
						}
					}
					if (userBean.getUserNum().equals("admin")) {
						continue;
					}
					userList.add(userBean);
				} catch (NamingException e) {
					log.error("", e);
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("", e);
			return false;
		}
		return true;
	}

	public List<UserBean> getUserBeanList() throws Exception {
		return userList;
	}

}
