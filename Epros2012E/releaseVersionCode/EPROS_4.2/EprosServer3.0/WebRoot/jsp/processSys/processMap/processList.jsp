<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>

		<div style="width: 100%;" align="right">
			<table style="margin-right: 25px; margin-top: 3px;">
				<tr align="left">
					<td align="right" style="padding-right: 5px;">
						<s:text name="total"></s:text>
						<span id="allFlowTotal"></span>
					</td>
					<td align="right" style="color: #FB1A1C; padding-right: 5px;">
						<s:text name="generationToBuildProcess"></s:text>
						<span id="noPubFlowTotal"></span>
					</td>
					<td align="right" style="color: #e7a100; padding-right: 5px;">
						<s:text name="forApprovalProcess"></s:text>
						<span id="approveFlowTotal"></span>
					</td>
					<td align="right" style="color: #00c317; padding-right: 15px;">
						<s:text name="releaseProcess"></s:text>
						<span id="pubFlowTotal"></span>
					</td>
					<td>
						<!--
						<img src="${basePath}images/allShow.gif" style="cursor: pointer;"
							target='_blank' class="table" onclick="showAllProcessList()" />
						 -->	
						<s:if test="#session.webLoginBean.isAdmin||#session.webLoginBean.isViewAdmin">
						<img src="${basePath}images/download.gif" style="cursor: pointer;"
							target='_blank' class="table" onclick="downloadProcessList()" />
						</s:if>
					</td>
				</tr>
			</table>
		</div>
		<div id="processListShow_id" />

			<input type="hidden" id="depth" />
			<input type="hidden" id="processId" />
			<input type="hidden" id="isPop" />
	</body>
</html>