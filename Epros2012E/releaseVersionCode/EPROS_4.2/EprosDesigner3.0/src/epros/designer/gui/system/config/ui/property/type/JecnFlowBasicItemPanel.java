package epros.designer.gui.system.config.ui.property.type;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigSpinner;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.RadioData;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUIUtil;

/**
 * 流程其它配置
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-26 时间：下午04:30:22
 */
public class JecnFlowBasicItemPanel extends JecnAbstractPropertyBasePanel implements ActionListener, ChangeListener {

	/** 显示指标来源 支撑的一级指标 相关度 */
	private JecnConfigCheckBox showKPIPropertyBox = new JecnConfigCheckBox(JecnProperties.getValue("showKPIProperty"));

	/*** 流程图角色只关联一个岗位时显示部门名称与岗位名称 */
	private JecnConfigCheckBox shouOrgPosNameBox = new JecnConfigCheckBox(JecnProperties.getValue("shouOrgPosName"));

	/** 1：左上进右下出（默认） **/
	private JecnConfigRadioButton twoEditPointRadio = new JecnConfigRadioButton(JecnProperties.getValue("twoEditPoint"));
	/** 0：四个编辑点都能进出 **/
	private JecnConfigRadioButton bothEdifPointRadio = new JecnConfigRadioButton(JecnProperties
			.getValue("bothEditPoint"));

	/** 左进右出 */
	private JecnConfigRadioButton leftInRightOut = new JecnConfigRadioButton(JecnProperties.getValue("leftIntoRight"));

	/** 横向流程、纵向流程 */
	/************** 【流程图横向和纵向配置】 *********************/
	private JecnConfigRadioButton hRadio = new JecnConfigRadioButton(JecnProperties.getValue("horizontal"));
	private JecnConfigRadioButton vRadio = new JecnConfigRadioButton(JecnProperties.getValue("vertical"));
	private JecnConfigRadioButton vhRadio = new JecnConfigRadioButton(JecnProperties.getValue("horVer"));

	/************** 【流程编号选择配置】 *********************/
	/** value = 1,2,3,4 ；默认1 */
	private JecnConfigRadioButton codeRadio0 = new JecnConfigRadioButton("1,2,3,...10,11...");
	private JecnConfigRadioButton codeRadio1 = new JecnConfigRadioButton("01,02,03,...10,11...");
	private JecnConfigRadioButton codeRadio2 = new JecnConfigRadioButton("001,002,003,...010,011...");
	private JecnConfigRadioButton codeRadio3 = new JecnConfigRadioButton("010,020,030,...100,110...990");

	/************** 协作框内的活动编号在自动编号时是否保持一致 *********************/
	private JecnConfigRadioButton sameNumber = new JecnConfigRadioButton(JecnProperties.getValue("isLab"));
	private JecnConfigRadioButton unSameNumber = new JecnConfigRadioButton(JecnProperties.getValue("notLab"));
	/** 活动类别 */
	private RadioData activityTypeData;
	private JecnConfigRadioButton isShowRadio;
	private JecnConfigRadioButton isHiddenRadio;
	/**
	 * 下载权限
	 */
	private JecnConfigRadioButton isShowDownLoadRadio = new JecnConfigRadioButton(JecnProperties.getValue("isLab"));
	private JecnConfigRadioButton isHiddenDownLoadRadio = new JecnConfigRadioButton(JecnProperties.getValue("notLab"));

	/**
	 * 是否启用修改角色别名
	 */
	// 是
	private JecnConfigRadioButton isShowUpdateRoleAlias = new JecnConfigRadioButton(JecnProperties.getValue("isLab"));
	// 否
	private JecnConfigRadioButton isHiddenUpdateRoleAlias = new JecnConfigRadioButton(JecnProperties.getValue("notLab"));
	// 首次
	private JecnConfigRadioButton isFirstUpdateRoleAlias = new JecnConfigRadioButton(JecnProperties.getValue("first"));

	/**
	 * 是否隐藏活动编号类别
	 */
	private JecnConfigRadioButton isShowActivityNumType = new JecnConfigRadioButton(JecnProperties.getValue("isShow"));
	private JecnConfigRadioButton isHiddenActivityNumType = new JecnConfigRadioButton(JecnProperties
			.getValue("isHidden"));

	/**
	 * 是否启用新建客户元素
	 */
	private JecnConfigRadioButton isShowCustomerElement = new JecnConfigRadioButton(JecnProperties.getValue("isShow"));
	private JecnConfigRadioButton isHiddenCustomerElement = new JecnConfigRadioButton(JecnProperties
			.getValue("isHidden"));

	/**
	 * 元素链接显示位置
	 */
	// 中上
	private JecnConfigRadioButton elementLocationTop = new JecnConfigRadioButton(JecnProperties.getValue("upperMiddle"));

	// 右下
	private JecnConfigRadioButton elementLocationLowerRight = new JecnConfigRadioButton(JecnProperties.getValue("lowerRight"));

	/**
	 * 是否显示开始结束元素
	 */
	private JecnConfigRadioButton isShowBeginningEndElement = new JecnConfigRadioButton(JecnProperties
			.getValue("isShow"));
	private JecnConfigRadioButton isHiddenBeginningEndElement = new JecnConfigRadioButton(JecnProperties
			.getValue("isHidden"));

	/** 角色活动上下限面板 */
	private JecnPanel roluRoundPanel = null;
	/** 角色个数上限 */
	private JLabel roleMaxLabel = null;
	protected JecnConfigSpinner roleMaxSpinner = null;
	/** 角色个数下限 */
	private JLabel roleMinLabel = null;
	private JecnConfigSpinner roleMinSpinner = null;
	/** 活动个数上限 */
	private JLabel roundMaxRectLabel = null;
	private JecnConfigSpinner roundMaxRectSpinner = null;
	/** 活动个数下限 */
	private JLabel roundMinRectLabel = null;
	private JecnConfigSpinner roundMinRectSpinner = null;

	public JecnFlowBasicItemPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	private void initComponents() {

		showKPIPropertyBox.setSelected(false);
		shouOrgPosNameBox.setSelected(false);

		showKPIPropertyBox.setOpaque(false);
		shouOrgPosNameBox.setOpaque(false);

		showKPIPropertyBox.addActionListener(this);
		shouOrgPosNameBox.addActionListener(this);

		// 流程元素编辑点
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(twoEditPointRadio);
		bgpp.add(bothEdifPointRadio);
		bgpp.add(leftInRightOut);

		// 左上进右下出
		twoEditPointRadio.setOpaque(false);
		twoEditPointRadio.setSelected(true);
		// 四个编辑点都能进出
		bothEdifPointRadio.setOpaque(false);
		leftInRightOut.setOpaque(false);

		// 左上进右下出
		twoEditPointRadio.addActionListener(this);
		// 四个编辑点都能进出
		bothEdifPointRadio.addActionListener(this);
		leftInRightOut.addActionListener(this);

		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setLayout(new GridBagLayout());

		// 新建流程，横向、纵向流程配置
		initVHRadio();
		// 编号配置
		initCodeRadio();
		// 自动编号
		initSameNumberRadio();
		initDownLoadRadio();
		initUpdateRoleAlias();
		initActivityNumType();
		initCustomerElement();
		initBeginningEndElement();
		initElementLocationElement();

		// 制度类别
		activityTypeData = new RadioData(JecnProperties.getValue("ruleTypeC"), JecnProperties.getValue("isShow"),
				JecnProperties.getValue("isHidden"));
		isShowRadio = activityTypeData.getRadioButton1();
		isHiddenRadio = activityTypeData.getRadioButton2();
		isShowRadio.addActionListener(this);
		isHiddenRadio.addActionListener(this);

		// **************角色活动上下限 start **************//
		// 角色活动上下限面板
		roluRoundPanel = new JecnPanel();
		// 角色活动上下限面板
		roluRoundPanel.setOpaque(false);
		roluRoundPanel.setBorder(null);
		// 角色个数上限
		roleMaxLabel = new JLabel(JecnProperties.getValue("task_roleMaxNumName"));
		roleMaxSpinner = new JecnConfigSpinner(new SpinnerNumberModel(10, 1, 1000, 1));
		// 角色个数下限
		roleMinLabel = new JLabel(JecnProperties.getValue("task_roleMinNumName"));
		roleMinSpinner = new JecnConfigSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
		// 活动个数上限
		roundMaxRectLabel = new JLabel(JecnProperties.getValue("task_roundMaxNumName"));
		roundMaxRectSpinner = new JecnConfigSpinner(new SpinnerNumberModel(20, 1, 1000, 1));
		// 活动个数下限
		roundMinRectLabel = new JLabel(JecnProperties.getValue("task_roundMinNumName"));
		roundMinRectSpinner = new JecnConfigSpinner(new SpinnerNumberModel(1, 1, 1000, 1));

		// 事件
		// 角色个数上限
		roleMaxSpinner.addChangeListener(this);
		// 角色个数下限
		roleMinSpinner.addChangeListener(this);
		// 活动个数上限
		roundMaxRectSpinner.addChangeListener(this);
		// 活动个数下限
		roundMinRectSpinner.addChangeListener(this);
		// **************角色活动上下限 end **************//
	}

	private void initSameNumberRadio() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(sameNumber);
		bgpp.add(unSameNumber);
		sameNumber.setOpaque(false);
		unSameNumber.setOpaque(false);
		// 监听
		sameNumber.addActionListener(this);
		unSameNumber.addActionListener(this);
	}

	private void initDownLoadRadio() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(isShowDownLoadRadio);
		bgpp.add(isHiddenDownLoadRadio);
		isShowDownLoadRadio.setOpaque(false);
		isHiddenDownLoadRadio.setOpaque(false);
		// 监听
		isShowDownLoadRadio.addActionListener(this);
		isHiddenDownLoadRadio.addActionListener(this);
	}

	private void initUpdateRoleAlias() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(isShowUpdateRoleAlias);
		bgpp.add(isHiddenUpdateRoleAlias);
		bgpp.add(isFirstUpdateRoleAlias);
		isShowUpdateRoleAlias.setOpaque(false);
		isHiddenUpdateRoleAlias.setOpaque(false);
		isFirstUpdateRoleAlias.setOpaque(false);
		// 监听
		isShowUpdateRoleAlias.addActionListener(this);
		isHiddenUpdateRoleAlias.addActionListener(this);
		isFirstUpdateRoleAlias.addActionListener(this);
	}

	private void initActivityNumType() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(isShowActivityNumType);
		bgpp.add(isHiddenActivityNumType);
		isShowActivityNumType.setOpaque(false);
		isHiddenActivityNumType.setOpaque(false);
		// 监听
		isShowActivityNumType.addActionListener(this);
		isHiddenActivityNumType.addActionListener(this);
	}

	private void initCustomerElement() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(isShowCustomerElement);
		bgpp.add(isHiddenCustomerElement);
		isShowCustomerElement.setOpaque(false);
		isHiddenCustomerElement.setOpaque(false);
		// 监听
		isShowCustomerElement.addActionListener(this);
		isHiddenCustomerElement.addActionListener(this);
	}

	private void initBeginningEndElement() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(isShowBeginningEndElement);
		bgpp.add(isHiddenBeginningEndElement);
		isShowBeginningEndElement.setOpaque(false);
		isHiddenBeginningEndElement.setOpaque(false);
		// 监听
		isShowBeginningEndElement.addActionListener(this);
		isHiddenBeginningEndElement.addActionListener(this);
	}

	private void initElementLocationElement() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(elementLocationTop);
		bgpp.add(elementLocationLowerRight);
		elementLocationTop.setOpaque(false);
		elementLocationLowerRight.setOpaque(false);
		// 监听
		elementLocationTop.addActionListener(this);
		elementLocationLowerRight.addActionListener(this);
	}

	private void initVHRadio() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(hRadio);
		bgpp.add(vRadio);
		bgpp.add(vhRadio);
		hRadio.setOpaque(false);
		vRadio.setOpaque(false);
		vhRadio.setOpaque(false);
		// 监听
		hRadio.addActionListener(this);
		vRadio.addActionListener(this);
		vhRadio.addActionListener(this);
	}

	private void initCodeRadio() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(codeRadio0);
		bgpp.add(codeRadio1);
		bgpp.add(codeRadio2);
		bgpp.add(codeRadio3);

		codeRadio0.setOpaque(false);
		codeRadio1.setOpaque(false);
		codeRadio2.setOpaque(false);
		codeRadio3.setOpaque(false);
		// 监听
		codeRadio0.addActionListener(this);
		codeRadio1.addActionListener(this);
		codeRadio2.addActionListener(this);
		codeRadio3.addActionListener(this);
	}

	private void initLayout() {

		// 元素编辑点配置面板
		JecnPanel editPointPanel = getLayoutEditPointPanel();
		editPointPanel.setMinimumSize(new Dimension(200, 120));
		// 横向纵向流程配置面板
		JecnPanel vhPanel = getLayoutVHPanel();
		// 活动编号配置面板
		JecnPanel codePanel = getCodePanel();

		// 协作框内的活动自动编号规则配置面板
		JecnPanel autoCodePanel = getSameNumberInDottedRectPanel();

		JecnPanel activityType = getActivityTypePanel();

		JecnPanel isDownLoad = getIsDownLoadPanel();

		JecnPanel roleAlias = getIsUpdateRoleAlias();

		JecnPanel isActivityNumType = getActivityNumTypePanel();

		JecnPanel isCustomerElement = getCustomerElement();

		JecnPanel isBeginningEndElement = getBeginningEndElement();

		JecnPanel elementLocationPanel = getElementLocationPanel();

		int gridy = 0;

		// 流程元素编辑点
		GridBagConstraints c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		this.add(editPointPanel, c);

		/*
		 * c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0,
		 * GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,
		 * 0, 0, 0), 0, 0); this.add(vhPanel, c);
		 */

		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(codePanel, c);

		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(isActivityNumType, c);

		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(autoCodePanel, c);

		JecnPanel panl = new JecnPanel();
		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		panl.add(activityType, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		panl.add(isCustomerElement, c);
		c = new GridBagConstraints(2, gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		panl.add(isBeginningEndElement, c);
		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(panl, c);

		JecnPanel pan = new JecnPanel();
		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		pan.add(isDownLoad, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		pan.add(roleAlias, c);
		c = new GridBagConstraints(2, gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		pan.add(elementLocationPanel, c);
		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(pan, c);

		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(roluRoundPanel, c);

		// **************角色、活动上下限 start **************//
		// 角色个数上限
		Insets insets = new Insets(5, 5, 5, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		roluRoundPanel.add(roleMaxLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		roluRoundPanel.add(roleMaxSpinner, c);

		// 角色个数下限
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		roluRoundPanel.add(roleMinLabel, c);
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		roluRoundPanel.add(roleMinSpinner, c);
		// 活动个数上限
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		roluRoundPanel.add(roundMaxRectLabel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		roluRoundPanel.add(roundMaxRectSpinner, c);

		// 活动个数下限
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		roluRoundPanel.add(roundMinRectLabel, c);
		c = new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		roluRoundPanel.add(roundMinRectSpinner, c);
		// ****************角色、活动上下限 end ***************//

		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(new JLabel(), c);
	}

	private JecnPanel getLayoutEditPointPanel() {
		JecnPanel editPointPanel = new JecnPanel();
		editPointPanel.setLayout(new GridBagLayout());
		editPointPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("flowElementEditPoint")));
		// 流程元素编辑点
		GridBagConstraints c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		editPointPanel.add(twoEditPointRadio, c);
		// 流程元素编辑点
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		editPointPanel.add(bothEdifPointRadio, c);
		// 流程元素编辑点
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		editPointPanel.add(leftInRightOut, c);
		return editPointPanel;
	}

	private JecnPanel getLayoutVHPanel() {
		JecnPanel vhPanel = new JecnPanel();
		vhPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		vhPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("flowSethc")));
		vhPanel.add(hRadio);
		vhPanel.add(vRadio);
		vhPanel.add(vhRadio);
		return vhPanel;
	}

	private JecnPanel getSameNumberInDottedRectPanel() {
		JecnPanel sameNumberPanel = new JecnPanel();
		sameNumberPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		sameNumberPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("isItConsistent")));
		sameNumberPanel.add(sameNumber);
		sameNumberPanel.add(unSameNumber);
		return sameNumberPanel;
	}

	private JecnPanel getActivityTypePanel() {
		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("isShowActivityType")));
		activityPanel.add(isShowRadio);
		activityPanel.add(isHiddenRadio);
		return activityPanel;
	}

	/**
	 * 是否具有下载权限
	 * 
	 * @return
	 */
	private JecnPanel getIsDownLoadPanel() {
		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("downLoadAccess")));
		activityPanel.add(isShowDownLoadRadio);
		activityPanel.add(isHiddenDownLoadRadio);
		return activityPanel;
	}

	/**
	 * 是否修改角色别名
	 * 
	 * @return
	 */
	private JecnPanel getIsUpdateRoleAlias() {
		JecnPanel roleAlias = new JecnPanel();
		roleAlias.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		roleAlias.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("WhetherRoleAliases")));
		roleAlias.add(isShowUpdateRoleAlias);
		roleAlias.add(isHiddenUpdateRoleAlias);
		roleAlias.add(isFirstUpdateRoleAlias);
		return roleAlias;
	}

	/**
	 * 是否显示活动编号类别
	 * 
	 * @return
	 */
	private JecnPanel getActivityNumTypePanel() {
		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("isShowActType")));
		activityPanel.add(isShowActivityNumType);
		activityPanel.add(isHiddenActivityNumType);
		return activityPanel;
	}

	/**
	 * 新建是否添加开始结束元素
	 * 
	 * @return
	 */
	private JecnPanel getBeginningEndElement() {
		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("whetherAddStartEnd")));
		activityPanel.add(isShowBeginningEndElement);
		activityPanel.add(isHiddenBeginningEndElement);
		return activityPanel;
	}

	/**
	 * 是否新建添加客户元素
	 * 
	 * @return
	 */
	private JecnPanel getCustomerElement() {
		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("whetherAddCustomer")));
		activityPanel.add(isShowCustomerElement);
		activityPanel.add(isHiddenCustomerElement);
		return activityPanel;
	}

	/**
	 * 元素链接显示位置
	 * 
	 * @return
	 */
	private JecnPanel getElementLocationPanel() {
		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("elementLocation")));
		activityPanel.add(elementLocationTop);
		activityPanel.add(elementLocationLowerRight);
		return activityPanel;
	}

	private JecnPanel getCodePanel() {
		JecnPanel codePanel = new JecnPanel();
		codePanel.setLayout(new GridBagLayout());
		codePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("activityNumSetting")));

		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		codePanel.add(codeRadio0, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		codePanel.add(codeRadio1, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		codePanel.add(codeRadio2, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		codePanel.add(codeRadio3, c);

		return codePanel;
	}

	@Override
	public boolean check() {
		// 角色个数上限值,角色个数下限值,提示信息KEY
		// boolean true:存在错误信息
		boolean role = checkSpinner(roleMaxSpinner, roleMinSpinner, "basicRoleMaxInfo");
		// 活动个数上限值,活动个数下限值,提示信息KEY
		boolean round = checkSpinner(roundMaxRectSpinner, roundMinRectSpinner, "basicRoundMaxInfo");

		if (role && round) {// 验证成功
			if (!getOkJButton().isEnabled()) {// 验证成功
				getOkJButton().setEnabled(true);
			}
			getInfoLable().setText("");
			return true;
		} else {
			return false;
		}
	}

	private JecnUserInfoTextArea getInfoLable() {
		return dialog.getOkCancelJButtonPanel().getInfoLabel();
	}

	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getTableItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();
			if (ConfigItemPartMapMark.basicRoleMax.toString().equals(mark)) {// 角色上限
				roleMaxSpinner.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.basicRoleMin.toString().equals(mark)) {// 角色下限
				roleMinSpinner.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.basicRoundMax.toString().equals(mark)) {// 活动上限
				roundMaxRectSpinner.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.basicRoundMin.toString().equals(mark)) {// 活动上限
				roundMinRectSpinner.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.showKPIProperty.toString().equals(mark)) { // 显示指标来源
				// 支撑的一级指标
				// 相关度
				showKPIPropertyBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					showKPIPropertyBox.setSelected(true);
				} else {
					showKPIPropertyBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.shouOrgPosName.toString().equals(mark)) { // 流程图角色关联岗位时其显示部门名称与岗位名称
				shouOrgPosNameBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					shouOrgPosNameBox.setSelected(true);
				} else {
					shouOrgPosNameBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.otherEditPort.toString().equals(mark)) {// 流程元素编辑点
				bothEdifPointRadio.setItemBean(itemBean);
				twoEditPointRadio.setItemBean(itemBean);
				leftInRightOut.setItemBean(itemBean);
				if ("0".equals(value)) {// 左上进右下出
					bothEdifPointRadio.setSelected(true);
				} else if ("1".equals(value)) {
					twoEditPointRadio.setSelected(true);
				} else {
					leftInRightOut.setSelected(true);
				}
			} else if (ConfigItemPartMapMark.activeCodeSelect.toString().equals(mark)) {
				initActiveCodeData(itemBean);
			} else if (ConfigItemPartMapMark.vhFlowSelect.toString().equals(mark)) {
				initVHFlowSelect(itemBean);
			} else if (ConfigItemPartMapMark.sameNumberInDottedRect.toString().equals(mark)) {
				initSameNumberInDottedRect(itemBean);
			} else if (ConfigItemPartMapMark.activityType.toString().equals(mark)) {// 制度类别是否显示：1显示
				isShowRadio.setItemBean(itemBean);
				isHiddenRadio.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {// 显示
					isShowRadio.setSelected(true);
				} else {
					isHiddenRadio.setSelected(true);
				}
			} else if (ConfigItemPartMapMark.isShowProcessDownLoad.toString().equals(mark)) {// 流程下载权限是否显示：1显示
				initIsDownLoad(itemBean);
			} else if (ConfigItemPartMapMark.activityNumIsShow.toString().equals(mark)) {// 流程下载权限是否显示：1显示
				initActivityNumIsShow(itemBean);
			} else if (ConfigItemPartMapMark.isUpdateRoleAlias.toString().equals(mark)) {// 是否修改角色别名
				initIsUpdateRoleAlias(itemBean);
			} else if (ConfigItemPartMapMark.isShowProcessCustomer.toString().equals(mark)) {// 是否显示客户元素
				initCustomerElementIsShow(itemBean);
			} else if (ConfigItemPartMapMark.isShowBeginningEndElement.toString().equals(mark)) {// 新建是否添加开始结束元素
				initBeginningEndElementIsShow(itemBean);
			} else if (ConfigItemPartMapMark.elementLocation.toString().equals(mark)) {// 新建是否添加开始结束元素
				initelementLocationState(itemBean);
			}
		}

	}

	/**
	 * 初始化协作框内的活动编号在自动编号时是否保持一致
	 * 
	 * @param itemBean
	 */
	private void initSameNumberInDottedRect(JecnConfigItemBean itemBean) {
		sameNumber.setItemBean(itemBean);
		unSameNumber.setItemBean(itemBean);

		if ("0".equals(itemBean.getValue())) {
			unSameNumber.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			sameNumber.setSelected(true);
		}
	}

	/**
	 * 初始化 下载权限是否启用
	 * 
	 * @param itemBean
	 */
	private void initIsDownLoad(JecnConfigItemBean itemBean) {
		isShowDownLoadRadio.setItemBean(itemBean);
		isHiddenDownLoadRadio.setItemBean(itemBean);

		if ("0".equals(itemBean.getValue())) {
			isHiddenDownLoadRadio.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			isShowDownLoadRadio.setSelected(true);
		}
	}

	/**
	 * 初始化 下载权限是否启用
	 * 
	 * @param itemBean
	 */
	private void initIsUpdateRoleAlias(JecnConfigItemBean itemBean) {
		isShowUpdateRoleAlias.setItemBean(itemBean);
		isHiddenUpdateRoleAlias.setItemBean(itemBean);
		isFirstUpdateRoleAlias.setItemBean(itemBean);

		if ("1".equals(itemBean.getValue())) {
			isHiddenUpdateRoleAlias.setSelected(true);
		} else if ("0".equals(itemBean.getValue())) {
			isShowUpdateRoleAlias.setSelected(true);
		} else if ("2".equals(itemBean.getValue())) {
			isFirstUpdateRoleAlias.setSelected(true);
		}
	}

	/**
	 * 初始化 活动编号类别是否启用
	 * 
	 * @param itemBean
	 */
	private void initActivityNumIsShow(JecnConfigItemBean itemBean) {
		isShowActivityNumType.setItemBean(itemBean);
		isHiddenActivityNumType.setItemBean(itemBean);

		if ("0".equals(itemBean.getValue())) {
			isHiddenActivityNumType.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			isShowActivityNumType.setSelected(true);
		}
	}

	/**
	 * 初始化 客户元素是否启用
	 * 
	 * @param itemBean
	 */
	private void initCustomerElementIsShow(JecnConfigItemBean itemBean) {
		isShowCustomerElement.setItemBean(itemBean);
		isHiddenCustomerElement.setItemBean(itemBean);

		if ("0".equals(itemBean.getValue())) {
			isHiddenCustomerElement.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			isShowCustomerElement.setSelected(true);
		}
	}

	/**
	 * 初始化 是否启用 开始结束元素
	 * 
	 * @param itemBean
	 */
	private void initBeginningEndElementIsShow(JecnConfigItemBean itemBean) {
		isShowBeginningEndElement.setItemBean(itemBean);
		isHiddenBeginningEndElement.setItemBean(itemBean);

		if ("0".equals(itemBean.getValue())) {
			isHiddenBeginningEndElement.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			isShowBeginningEndElement.setSelected(true);
		}
	}

	/**
	 * 初始化 是否启用 开始结束元素
	 * 
	 * @param itemBean
	 */
	private void initelementLocationState(JecnConfigItemBean itemBean) {
		elementLocationTop.setItemBean(itemBean);
		elementLocationLowerRight.setItemBean(itemBean);
		// 0 是 中上 1是右下
		if ("0".equals(itemBean.getValue())) {
			elementLocationTop.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			elementLocationLowerRight.setSelected(true);
		}
	}

	private void initActiveCodeData(JecnConfigItemBean itemBean) {
		codeRadio0.setItemBean(itemBean);
		codeRadio3.setItemBean(itemBean);
		codeRadio1.setItemBean(itemBean);
		codeRadio2.setItemBean(itemBean);

		if ("0".equals(itemBean.getValue())) {
			codeRadio0.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			codeRadio1.setSelected(true);
		} else if ("2".equals(itemBean.getValue())) {
			codeRadio2.setSelected(true);
		} else if ("3".equals(itemBean.getValue())) {
			codeRadio3.setSelected(true);
		}
	}

	private void initVHFlowSelect(JecnConfigItemBean itemBean) {
		hRadio.setItemBean(itemBean);
		vRadio.setItemBean(itemBean);
		vhRadio.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			hRadio.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			vRadio.setSelected(true);
		} else if ("2".equals(itemBean.getValue())) {
			vhRadio.setSelected(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == showKPIPropertyBox) {// 显示指标来源 支撑的一级指标
				// 相关度1：选中；0不选中
				if (showKPIPropertyBox.getItemBean() == null) {
					return;
				}
				if (showKPIPropertyBox.isSelected()) {// 选中
					showKPIPropertyBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					showKPIPropertyBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			} else if (e.getSource() == shouOrgPosNameBox) {// 流程图角色关联岗位时其显示部门名称与岗位名称
				if (shouOrgPosNameBox.getItemBean() == null) {
					return;
				}
				if (shouOrgPosNameBox.isSelected()) {// 选中
					shouOrgPosNameBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					shouOrgPosNameBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
		if (e.getSource() instanceof JecnConfigRadioButton) {
			// 流程元素编辑点
			JecnConfigRadioButton configRadioButton = (JecnConfigRadioButton) e.getSource();
			if (configRadioButton.isSelected()) {// 选中
				if (configRadioButton == bothEdifPointRadio) {// 四个编辑点都能进出
					bothEdifPointRadio.getItemBean().setValue("0");
				} else if (configRadioButton == twoEditPointRadio) {// 左上进右下出
					twoEditPointRadio.getItemBean().setValue("1");
				} else if (configRadioButton == leftInRightOut) {// 左上进右下出
					leftInRightOut.getItemBean().setValue("2");
				} else if (configRadioButton == hRadio) {// 横向
					hRadio.getItemBean().setValue("0");
				} else if (configRadioButton == vRadio) {// 纵向
					vRadio.getItemBean().setValue("1");
				} else if (configRadioButton == vhRadio) {// 横纵向
					vhRadio.getItemBean().setValue("2");
				} else if (configRadioButton == codeRadio0) {// 
					codeRadio0.getItemBean().setValue("0");
				} else if (configRadioButton == codeRadio1) {// 
					codeRadio1.getItemBean().setValue("1");
				} else if (configRadioButton == codeRadio2) {//
					codeRadio2.getItemBean().setValue("2");
				} else if (configRadioButton == codeRadio3) {//
					codeRadio3.getItemBean().setValue("3");
				} else if (configRadioButton == sameNumber) {
					sameNumber.getItemBean().setValue("1");
				} else if (configRadioButton == unSameNumber) {
					unSameNumber.getItemBean().setValue("0");
				} else if (configRadioButton == isShowRadio) {// 显示
					isShowRadio.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else if (configRadioButton == isHiddenRadio) {// 隐藏
					isShowRadio.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				} else if (configRadioButton == isShowDownLoadRadio) {// 显示
					isShowDownLoadRadio.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else if (configRadioButton == isHiddenDownLoadRadio) {// 隐藏
					isHiddenDownLoadRadio.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				} else if (configRadioButton == isShowActivityNumType) {// 显示
					isShowActivityNumType.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else if (configRadioButton == isHiddenActivityNumType) {// 隐藏
					isHiddenActivityNumType.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				} else if (configRadioButton == isShowUpdateRoleAlias) {// 显示
					isShowUpdateRoleAlias.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				} else if (configRadioButton == isHiddenUpdateRoleAlias) {// 隐藏
					isHiddenUpdateRoleAlias.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else if (configRadioButton == isShowCustomerElement) {// 显示
					isShowCustomerElement.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else if (configRadioButton == isHiddenCustomerElement) {// 隐藏
					isHiddenCustomerElement.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				} else if (configRadioButton == isShowBeginningEndElement) {// 显示
					isShowBeginningEndElement.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else if (configRadioButton == isHiddenBeginningEndElement) {// 隐藏
					isHiddenBeginningEndElement.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				} else if (configRadioButton == elementLocationTop) {// 元素链接位置
					// 0是中上
					elementLocationTop.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				} else if (configRadioButton == elementLocationLowerRight) {// 元素链接位置
					// 1是右下
					elementLocationLowerRight.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else if (configRadioButton == isFirstUpdateRoleAlias) {// 首次
					isFirstUpdateRoleAlias.getItemBean().setValue("2");
				}
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JecnConfigSpinner) {
			JecnConfigSpinner configSpinner = (JecnConfigSpinner) e.getSource();
			if (configSpinner == roleMaxSpinner) { // 角色个数上限
				// 角色个数上限值,角色个数下限值,提示信息KEY
				spinnerValue(roleMaxSpinner, roleMinSpinner, "basicRoleMaxInfo", true);
			} else if (configSpinner == roleMinSpinner) { // 角色个数下限
				// 角色个数上限值,角色个数下限值,提示信息KEY
				spinnerValue(roleMaxSpinner, roleMinSpinner, "basicRoleMaxInfo", false);
			} else if (configSpinner == roundMaxRectSpinner) { // 活动个数上限
				// 活动个数上限值,活动个数下限值,提示信息KEY
				spinnerValue(roundMaxRectSpinner, roundMinRectSpinner, "basicRoundMaxInfo", true);
			} else if (configSpinner == roundMinRectSpinner) { // 活动个数下限
				// 活动个数上限值,活动个数下限值,提示信息KEY
				spinnerValue(roundMaxRectSpinner, roundMinRectSpinner, "basicRoundMaxInfo", false);
			}

			// 整体校验
			check();
		}

	}

	/**
	 * 
	 * 校验成功时复制到数据层
	 * 
	 * @param maxSpinner
	 *            JecnConfigSpinner 上限
	 * @param minSpinner
	 *            JecnConfigSpinner 下限
	 * @param infoKey
	 *            String 提示信息KEY
	 * @param isMax
	 *            boolean true：上限；false：下限
	 * 
	 */
	private void spinnerValue(JecnConfigSpinner maxSpinner, JecnConfigSpinner minSpinner, String infoKey, boolean isMax) {

		if (checkSpinner(maxSpinner, minSpinner, infoKey)) {// 校验成功：true；失败：false
			if (isMax) {
				maxSpinner.getItemBean().setValue(maxSpinner.getValue().toString());
			} else {
				minSpinner.getItemBean().setValue(minSpinner.getValue().toString());
			}
		}
	}

	/**
	 * 
	 * 校验：上下限制大于等于1且上限值大于等于下限值时失败
	 * 
	 * @param maxSpinner
	 *            JecnConfigSpinner 上限
	 * @param minSpinner
	 *            JecnConfigSpinner 下限
	 * @param infoKey
	 *            提示信息
	 * @return boolean 校验成功：true；失败：false
	 */
	private boolean checkSpinner(JecnConfigSpinner maxSpinner, JecnConfigSpinner minSpinner, String infoKey) {
		// 上限
		int maxvalue = Integer.parseInt(maxSpinner.getValue().toString());
		// 下限
		int minValue = Integer.parseInt(minSpinner.getValue().toString());

		if (maxvalue <= 0 || minValue <= 0 || maxvalue < minValue) {// 上下限小于等于0或上限值小于下限值
			if (getOkJButton().isEnabled()) {
				getOkJButton().setEnabled(false);
			}
			getInfoLable().setText(JecnProperties.getValue(infoKey));

			return false;
		}
		return true;
	}

	private JButton getOkJButton() {
		return dialog.getOkCancelJButtonPanel().getOkJButton();
	}
}
