package epros.designer.gui;

import java.util.Properties;

import javax.swing.UIManager;

import jecntool.file.JecnFileUtil;
import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.tray.JecnProcessD2;
import epros.tray.JecnServerUtil;
import epros.tray.key.JecnKeyCheckD2;

public class JecnD2 {
	/** 是否试用密钥  true：是；false：正式密钥*/
	public static boolean isTmpKey=false;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// 解决ABC输入法BUG：java Swing 里面的文本框在输入的时候会弹出一个“输入窗口”
			System.setProperty("java.awt.im.style", "on-the-spot");
			// 外观
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
		}
		// 1、 加载资源文件
		JecnProperties.loadProperties();
		// 2、 初始化画图面板必须设置
		JecnInitDraw.initDraw();
		
		JecnConstants.versionType=1;
		
		new JecnKeyCheckD2().process();
		
		/**
		 * 主要解决d2单机版修改端口不能启动问题  ConnectionPool 默认端口是2088
		 * 解决方式启动之前从配置文件JecnConfig.properties 中读取serverPort3端口
		 * **/
		String jecnConfigPath = JecnServerUtil.getJecnConfigPath();
		Properties jecnProps = JecnFileUtil.readProperties(jecnConfigPath);
		ConnectionPool.setServerPort(jecnProps.getProperty("serverPort3"));
		
		JecnProcessD2 d2 = new JecnProcessD2();
		d2.processEprosD2(true);
		
		// 3、显示主面板
		JecnLoginDesignerFrame rd = new JecnLoginDesignerFrame();
		rd.setVisible(true);
	}

}
