package epros.designer.gui.process.flowmap;

import com.jecn.epros.server.bean.process.JecnMainFlowT;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;

/**
 * 
 * 流程架构描述
 * 
 * @author Administrator
 * 
 */
public class FlowMapDesDialog extends FlowMapShowDialog {
	private JecnMainFlowT jecnMainFlow = null;

	public FlowMapDesDialog(JecnTree jTree, JecnTreeNode selectNode) {
		super(selectNode, jTree, false);

		this.setTitle(JecnProperties.getValue("flowMapDes"));

		// 流程地图ID
		Long id = selectNode.getJecnTreeBean().getId();
		// 流程地图名称
		String flowMapName = selectNode.getJecnTreeBean().getName();
		// 流程地图编号
		String flowMapNum = selectNode.getJecnTreeBean().getNumberId();
		// 根据ID获得流程地图描述数据
		try {
			jecnMainFlow = ConnectionPool.getProcessAction().getFlowMapInfo(id);
			mapNumField.setText(flowMapNum);
			flowMapField.setText(flowMapName);
			flowMapField.setEditable(false);
			// 目的
			mapPurposeField.setText(jecnMainFlow.getFlowAim());
			// 范围
			mapRangeField.setText(jecnMainFlow.getFlowArea());
			// 名称定义
			mapNameDefinedField.setText(jecnMainFlow.getFlowNounDefine());
			// 输入
			mapInputField.setText(jecnMainFlow.getFlowInput());
			// 输出
			mapOutputField.setText(jecnMainFlow.getFlowOutput());
			// 步骤说明
			mapStepDescField.setText(jecnMainFlow.getFlowShowStep());
			this.setFileId(jecnMainFlow.getFileId());
			this.initLayout();
		} catch (Exception e1) {
			log.error("FlowMapDesDialog is error", e1);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}

	}

	public boolean isUpdate() {
		// 流程地图描述是否修改过 提示取消

		// 编号
		if (!this.getSelectNode().getJecnTreeBean().getNumberId().equals(this.mapNumField.getText().toString())) {
			return true;
		}
		// 目的
		String flowAim = "";
		if (jecnMainFlow.getFlowAim() == null) {
			flowAim = "";
		} else {
			flowAim = jecnMainFlow.getFlowAim();
		}
		if (!flowAim.equals(this.mapPurposeField.getText().toString())) {
			return true;
		}
		// 范围
		String fowAreaRuange = "";
		if (jecnMainFlow.getFlowArea() == null) {
			fowAreaRuange = "";
		} else {
			fowAreaRuange = jecnMainFlow.getFlowArea();
		}
		if (!fowAreaRuange.equals(this.mapRangeField.getText().toString())) {
			return true;
		}
		// 名称定义
		String fowNounDefine = "";
		if (jecnMainFlow.getFlowNounDefine() == null) {
			fowNounDefine = "";
		} else {
			fowNounDefine = jecnMainFlow.getFlowNounDefine();
		}
		if (!fowNounDefine.equals(this.mapNameDefinedField.getText().toString())) {
			return true;
		}
		// 输入
		String fowInput = "";
		if (jecnMainFlow.getFlowInput() == null) {
			fowInput = "";
		} else {
			fowInput = jecnMainFlow.getFlowInput();
		}
		if (!fowInput.equals(this.mapInputField.getText().toString())) {
			return true;
		}
		// 输出
		String fowOutput = "";
		if (jecnMainFlow.getFlowOutput() == null) {
			fowOutput = "";
		} else {
			fowOutput = jecnMainFlow.getFlowOutput();
		}
		if (!fowOutput.equals(this.mapOutputField.getText().toString())) {
			return true;
		}
		// 步骤说明
		String fowShowStep = "";
		if (jecnMainFlow.getFlowShowStep() == null) {
			fowShowStep = "";
		} else {
			fowShowStep = jecnMainFlow.getFlowShowStep();
		}
		if (!fowShowStep.equals(this.mapStepDescField.getText().toString())) {
			return true;
		}

		Long newFileId = null;
		if (fileEnclosureSelectCommon != null) {
			newFileId = fileEnclosureSelectCommon.getResultFileId();
		}
		// 附件名称
		if (jecnMainFlow.getFileId() == null) {
			if (newFileId != null) {
				return true;
			}
		} else {
			if (newFileId == null) {
				return true;
			} else if (!jecnMainFlow.getFileId().equals(newFileId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 确定 保存流程地图描述
	 */
	public void okButPerformed() {
		if (isUpdate()) {
			// 流程编号
			if (DrawCommon.checkNameMaxLength(this.mapNumField.getText().toString())) {
				verfyLab.setText(JecnProperties.getValue("flowNum") + JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}
			// 目的
			if (DrawCommon.checkNoteLength(mapPurposeField.getText().toString())) {
				verfyLab.setText(JecnProperties.getValue("purpose") + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 范围
			if (DrawCommon.checkNoteLength(mapRangeField.getText().toString())) {
				verfyLab.setText(JecnProperties.getValue("flowScope") + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 名称定义
			if (DrawCommon.checkNoteLength(mapNameDefinedField.getText().toString())) {
				verfyLab.setText(mapNameDefinedLab.getText().substring(0, mapNameDefinedLab.getText().length() - 1)
						+ JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 输入
			if (DrawCommon.checkNoteLength(mapInputField.getText().toString())) {
				verfyLab.setText(JecnProperties.getValue("actIn") + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 输出
			if (DrawCommon.checkNoteLength(mapOutputField.getText().toString())) {
				verfyLab.setText(JecnProperties.getValue("actOut") + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 步骤说明
			if (DrawCommon.checkNoteLength(mapStepDescField.getText().toString())) {
				verfyLab.setText(mapStepDescLab.getText().substring(0, mapStepDescLab.getText().length() - 1)
						+ JecnProperties.getValue("lengthNotOut"));
				return;
			}

			// 目的
			jecnMainFlow.setFlowAim(this.mapPurposeField.getText());
			// 范围
			jecnMainFlow.setFlowArea(this.mapRangeField.getText());
			// 名称定义
			jecnMainFlow.setFlowNounDefine(this.mapNameDefinedField.getText());
			// 输入
			jecnMainFlow.setFlowInput(this.mapInputField.getText());
			// 输出
			jecnMainFlow.setFlowOutput(this.mapOutputField.getText());
			// 步骤说明
			jecnMainFlow.setFlowShowStep(this.mapStepDescField.getText());
			// 附件名称
			Long newFileId = null;
			if (fileEnclosureSelectCommon != null) {
				newFileId = fileEnclosureSelectCommon.getResultFileId();
			}
			// 附件ID
			jecnMainFlow.setFileId(newFileId);
			jecnMainFlow.setUpdateOrnot(Long.valueOf(0));
			// //流程编号
			String flowNum = null;
			if (this.mapNumField.getText() != null && !"".equals(this.mapNumField.getText())) {
				flowNum = this.mapNumField.getText().toString();
			}

			// 流程地图描述 执行数据库保存
			try {
				ConnectionPool.getProcessAction().updateFlowMap(jecnMainFlow, JecnConstants.getUserId(), flowNum);
				// 更新树节点名称
				this.getSelectNode().getJecnTreeBean().setNumberId(this.mapNumField.getText());
				JecnTreeCommon.flushCurrentTreeNodeToUpdate(this.getSelectNode());
			} catch (Exception e) {
				log.error("FlowMapDesDialog okButPerformed is error", e);
			}
		}
		this.setVisible(false);
	}

}
