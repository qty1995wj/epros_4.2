package epros.designer.service.system;

import javax.swing.JComponent;

import epros.designer.gui.JecnDesignerFrame;
import epros.designer.gui.process.activitiesProperty.ActivitiesDetailsJpanel;
import epros.designer.gui.process.roleDetail.RoleMainPanel;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

public class JecnElementAttrsbuteCommon {
	private static JecnElementAttributePanel elePanel;

	public static JComponent getElementAttributePanel(JecnBaseFigurePanel curFigurePanel) {
		if (curFigurePanel == null) {
			// 1、存在元素 ，需更新元素
			// 2、面板属性为空
			return elePanel;
		}
		return createElementAttributePanel(curFigurePanel);
	}

	public static void updateElementArrtibute(JecnBaseFigurePanel curFigurePanel) {
		if (elePanel == null || elePanel.getFigureData() == null) {
			return;
		}

		MapElemType elemType = curFigurePanel.getFlowElementData().getMapElemType();
		if (!isElementAttrbute(elemType)) {
			return;
		}

		if (curFigurePanel.getFlowElementData().getFlowElementUUID().equals(
				elePanel.getFigurePanel().getFlowElementData().getFlowElementUUID())) {
			// 更新上一个元素属性
			boolean isSave = elePanel.saveButAction();
			if (isSave) {
				elePanel.getFigurePanel().updateUI();
			}
			return;
		}
		// 初始化数据
		elePanel.setFigurePanel(curFigurePanel);
		elePanel.initElementPanelData(curFigurePanel);
		curFigurePanel.updateUI();
	}

	/**
	 * 元素属性 - 展示元素验证
	 * 
	 * @param curElemType
	 * @return
	 */
	private static boolean isElementAttrbute(MapElemType curElemType) {
		return DrawCommon.isActive(curElemType) || DrawCommon.isRole(curElemType);
	}

	public static boolean isSameTypeElement(JecnBaseFigurePanel curFigurePanel) {
		if (elePanel == null) {
			return false;
		}
		JecnFigureData existsFigure = elePanel.getFigureData();
		MapElemType curElemType = curFigurePanel.getFlowElementData().getMapElemType();
		if (DrawCommon.isStandardActive(existsFigure.getMapElemType()) && DrawCommon.isStandardActive(curElemType)) {
			return true;
		} else if (DrawCommon.isImplActive(existsFigure.getMapElemType()) && DrawCommon.isImplActive(curElemType)) {
			return true;
		} else if (DrawCommon.isRole(existsFigure.getMapElemType()) && DrawCommon.isRole(curElemType)) {
			return true;
		} else if (existsFigure.getMapElemType() == curElemType) {
			return true;
		}
		return false;
	}

	public static JComponent createElementAttributePanel(JecnBaseFigurePanel curFigurePanel) {
		JecnFigureData figureData = curFigurePanel.getFlowElementData();
		if (DrawCommon.isActive(figureData.getMapElemType())) {
			elePanel = new ActivitiesDetailsJpanel((JecnActiveData) figureData, (JecnBaseActiveFigure) curFigurePanel);
		} else if (DrawCommon.isRole(figureData.getMapElemType())) {
			elePanel = new RoleMainPanel((JecnBaseRoleFigure) curFigurePanel);
		}
		return elePanel.getElementAttrubutePanel();
	}

	public static void cleanElementAttrsbute() {
		elePanel = null;
		JecnPanel panel = JecnDrawMainPanel.getMainPanel().getEleFigurePanel();
		panel.removeAll();
		panel.updateUI();

		JecnDesignerFrame.getDesignModuleShow().hidderEleFigure();
	}
}
