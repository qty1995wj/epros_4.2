function buttPublicFlow() {
	buttPublicFlowStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : 5
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "buttPublicProcess.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "flowId",
			mapping : "flowId"
		}, {
			name : "flowName",
			mapping : "flowName"
		}, {
			name : "pubDate",
			mapping : "pubDate"
		}])

	});

	buttPublicFlowColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : "flowId",
				dataIndex : "flowId",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : buttFlowRenderer
			}, {
				header : i_publicTime,
				dataIndex : "pubDate",
				align : "center",
				menuDisabled : true
			}]);
	buttPublicFlowBbar = new Ext.PagingToolbar( {
		pageSize : 5,
		store : buttPublicFlowStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	buttPublicFlowGridPanel = new Ext.grid.GridPanel( {
		id : "eprosButtPublicFlowGridPanel_id",
		renderTo : 'eprosButtPublicFlow_id',
		height: 160,	
		autoHeight:true,
		containerScroll:false,
		autoScroll:false,
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样。
		},
		frame : false,
		store : buttPublicFlowStore,
		colModel : buttPublicFlowColumnModel,
		bbar : buttPublicFlowBbar
	});
	buttPublicFlowGridPanel.store.on('load',function(){
		var count=buttPublicFlowGridPanel.getStore().getCount();
		if(count<1)                
				{
					buttPublicFlowGridPanel.hide();
					
				} else       
				{ 
					buttPublicFlowGridPanel.show();
				}
	});
}
function buttFlowRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['flowId']+"' >"+value+"</a>";
}