package epros.draw.gui.swing.calendar;

import java.awt.BorderLayout;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JToolBar;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 带有Toolbar的按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnCalendarButton extends JButton {
	/** 装载按钮容器 */
	protected JToolBar toolBar = null;

	public JecnCalendarButton() {
		initComponents();
	}

	public JecnCalendarButton(Action a) {
		super(a);
		initComponents();
	}

	public JecnCalendarButton(Icon icon) {
		super(icon);
		initComponents();
	}

	public JecnCalendarButton(String text) {
		super(text);
		initComponents();
	}

	public JecnCalendarButton(String text, Icon icon) {
		super(text, icon);
		initComponents();
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	private void initComponents() {
		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 不显示焦点状态
		this.setFocusPainted(false);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.add(this);
	}
}