package com.jecn.epros.server.webBean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

/**
 * 流程概况信息
 * 
 * @author Administrator
 * 
 */
public class ProcessBaseInfoBean implements Serializable {
	/** 流程ID */
	private Long flowId;
	/** 流程名称 */
	private String flowName;
	/** 流程编号 */
	private String flowIdInput;
	/** 起始活动 */
	private String flowStartpoint;
	/** 终止活动 */
	private String flowEndpoint;
	/** 流程输入 */
	private String input;
	/** 流程输出 */
	private String ouput;
	/** 流程客户 */
	private String flowCustom;
	/** 支持工具 */
	private List<String> listTools;
	/** 附件ID */
	private Long fileId;
	/** 文件名称 */
	private String fileName;
	/** 密级 */
	private int isPublic;
	/** 岗位查阅权限 */
	private List<JecnTreeBean> posList;
	/** 岗位组查阅权限 */
	private List<JecnTreeBean> posGroupList;
	/** 部门查阅权限 */
	private List<JecnTreeBean> orgList;
	/** 流程类别 */
	private String flowTypeName;
	/** 责任人ID */
	private Long orgId;
	/** 责任人名称 */
	private String orgName;
	/** 责任人类型 */
	private Integer typeResPeople;
	/** 责任人ID */
	private Long resPeopleId;
	/** 责任人名称 */
	private String resPeopleName;
	/** 所属流程ID */
	private Long perFlowId;
	/** 所属流程名称 */
	private String perFlowName;
	/** 所属流程类型 */
	private String preFlowType;
	/** 有效期 */
	private String expiry;
	/** 监护人ID */
	private Long guardianId;
	/** 监护人名称 */
	private String guardianName;
	/** 拟制人ID */
	private Long fictionPeopleId;
	/** 拟制人名称 */
	private String fictionPeopleName;
	/** 是否显示拟制人 */
	private boolean showFiction;
	/** 是否显示监护人 */
	private boolean showGuardian;

	public boolean isShowFiction() {
		return showFiction;
	}

	public void setShowFiction(boolean showFiction) {
		this.showFiction = showFiction;
	}

	public boolean isShowGuardian() {
		return showGuardian;
	}

	public void setShowGuardian(boolean showGuardian) {
		this.showGuardian = showGuardian;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowStartpoint() {
		return flowStartpoint;
	}

	public void setFlowStartpoint(String flowStartpoint) {
		this.flowStartpoint = flowStartpoint;
	}

	public String getFlowEndpoint() {
		return flowEndpoint;
	}

	public void setFlowEndpoint(String flowEndpoint) {
		this.flowEndpoint = flowEndpoint;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOuput() {
		return ouput;
	}

	public List<JecnTreeBean> getPosGroupList() {
		return posGroupList;
	}

	public void setPosGroupList(List<JecnTreeBean> posGroupList) {
		this.posGroupList = posGroupList;
	}

	public void setOuput(String ouput) {
		this.ouput = ouput;
	}

	public String getFlowCustom() {
		return flowCustom;
	}

	public void setFlowCustom(String flowCustom) {
		this.flowCustom = flowCustom;
	}

	public List<String> getListTools() {
		return listTools;
	}

	public void setListTools(List<String> listTools) {
		this.listTools = listTools;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}

	public List<JecnTreeBean> getPosList() {
		return posList;
	}

	public void setPosList(List<JecnTreeBean> posList) {
		this.posList = posList;
	}

	public List<JecnTreeBean> getOrgList() {
		return orgList;
	}

	public void setOrgList(List<JecnTreeBean> orgList) {
		this.orgList = orgList;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowIdInput() {
		return flowIdInput;
	}

	public void setFlowIdInput(String flowIdInput) {
		this.flowIdInput = flowIdInput;
	}

	public String getFlowTypeName() {
		return flowTypeName;
	}

	public void setFlowTypeName(String flowTypeName) {
		this.flowTypeName = flowTypeName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getTypeResPeople() {
		return typeResPeople;
	}

	public void setTypeResPeople(Integer typeResPeople) {
		this.typeResPeople = typeResPeople;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}

	public String getResPeopleName() {
		return resPeopleName;
	}

	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}

	public Long getPerFlowId() {
		return perFlowId;
	}

	public void setPerFlowId(Long perFlowId) {
		this.perFlowId = perFlowId;
	}

	public String getPerFlowName() {
		return perFlowName;
	}

	public void setPerFlowName(String perFlowName) {
		this.perFlowName = perFlowName;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public Long getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public Long getFictionPeopleId() {
		return fictionPeopleId;
	}

	public void setFictionPeopleId(Long fictionPeopleId) {
		this.fictionPeopleId = fictionPeopleId;
	}

	public String getFictionPeopleName() {
		return fictionPeopleName;
	}

	public void setFictionPeopleName(String fictionPeopleName) {
		this.fictionPeopleName = fictionPeopleName;
	}

	public String getPreFlowType() {
		return preFlowType;
	}

	public void setPreFlowType(String preFlowType) {
		this.preFlowType = preFlowType;
	}
}
