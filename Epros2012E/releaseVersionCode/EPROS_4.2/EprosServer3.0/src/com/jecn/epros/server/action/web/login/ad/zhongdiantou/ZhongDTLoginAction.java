package com.jecn.epros.server.action.web.login.ad.zhongdiantou;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 
 * 中电投：单点登录 http://192.168.1.1:8080/login.action?adLoginName=&adPassWord=
 * 
 * @author ZHAGNXH
 * 
 */
public class ZhongDTLoginAction extends JecnAbstractADLoginAction {
	public ZhongDTLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 单点登录入口
	 * 
	 * @return String
	 */
	public String login() {
		String queryString = loginAction.getRequest().getQueryString();
		if (StringUtils.isEmpty(queryString)) {
			return loginAction.loginGen();
		}
		try {
			String[] str = queryString.split("&");
			// 单点登录名称
			String adLoginName = str[1];
			// 单点登录 解密秘钥Key
			String key = str[2];
			log.info("adLoginName = " + adLoginName + " key = " + key);
			// 解析登录名
			String loginName = ZhongDTAES.decrypt(getSubString(key), getSubString(adLoginName));
			// 解密
			loginAction.setLoginName(loginName);

			// 通过登录名称登录
			return loginAction.userLoginGen(false);
		} catch (Exception e) {
			log.error("获取用户信息异常 queryString = " + queryString, e);
			return LoginAction.INPUT;
		}
	}

	private String getSubString(String str) {
		return str.substring(2, str.length());
	}
	// public String login() {
	// try {
	// // 单点登录名称
	// String adLoginName =
	// this.loginAction.getRequest().getParameter("username");
	// // 单点登录密码
	// String adPassWord =
	// this.loginAction.getRequest().getParameter("password");
	//
	// if (StringUtils.isBlank(adLoginName)) {// 单点登录名称为空，执行普通登录路径
	// return loginAction.loginGen();
	// }
	//
	// if (StringUtils.isBlank(adPassWord)) {// 单点登录密码为空，返回登录页面
	// log.error("密码为空" + adPassWord);
	// return LoginAction.INPUT;
	// }
	//
	// // 获取解密对象
	// DES des = JecnZhongDTAfterItem.start();
	//
	// // 解密
	// loginAction.setLoginName(des.decode(adLoginName));
	// loginAction.setPassword(des.decode(adPassWord));
	//
	// // 通过登录名称、密码执行普通登录
	// return loginAction.loginGen();
	//
	// } catch (Exception e) {
	// log.error("获取用户信息异常", e);
	// return LoginAction.INPUT;
	// }
	// }
}
