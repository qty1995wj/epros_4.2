package com.jecn.epros.server.service.task.app;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.IBaseService;

/**
 * 任务试运行到期邮件提醒处理类接口
 * 
 * @author ZHANGXH
 * @date： 日期：Jul 1, 2013 时间：11:30:04 AM
 */
public interface ITaskRunTestService extends IBaseService<JecnUser, Long> {
	/**
	 * 发送试用行邮件
	 * 
	 * @throws Exception
	 */
	public void setEmailByDate() throws Exception;
}
