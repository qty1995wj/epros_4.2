package com.jecn.webservice.jbshihua.service.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;
import com.jecn.webservice.jbshihua.entity.DeptViewDao;
import com.jecn.webservice.jbshihua.service.DeptService;

@Transactional(rollbackFor = Exception.class)
public class DeptServiceImpl extends AbsBaseService<JecnOrgView, String> implements DeptService {

	private DeptViewDao deptViewDao;

	public DeptViewDao getDeptViewDao() {
		return deptViewDao;
	}

	public void setDeptViewDao(DeptViewDao deptViewDao) {
		this.deptViewDao = deptViewDao;
	}

	public int recevieOrgJB(List<JecnOrgView> depts) throws Exception {
		List<String> getDeptUUID = getDeptUUID();
		for (JecnOrgView dept : depts) {
			if (getDeptUUID.contains(dept.getUUID())) {// 原来没有 新增的
				deptViewDao.updateDeptByUUID(dept);
			} else {
				deptViewDao.insertDeptByUUID(dept);
			}
		}

		return 0;
	}
	
	private List<String> getDeptUUID(){
		String sql = "SELECT uuid FROM jecn_org_info";
		return deptViewDao.listObjectNativeSql(sql, "uuid", Hibernate.STRING);
	}
	
	public int recevieOrg(List<JecnOrgView> depts) throws Exception {
		for (JecnOrgView dept : depts) {
			if ("0".equals(dept.getOrgStatus())) {// 新增
				deptViewDao.insertDept(dept);
			} else if ("1".equals(dept.getOrgStatus()) || "2".equals(dept.getOrgStatus())) {// 更新和删除
				deptViewDao.updateDept(dept);
			}
		}
		return 0;
	}

	@Override
	public List<JecnOrgView> getAllDepts() throws Exception {
		String hql = "from JecnOrgView";
		return deptViewDao.listHql(hql);
	}
}
