package com.jecn.epros.server.action.designer.system.impl;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.service.system.IJecnElementsLibraryServie;

public class JecnElementsLibraryActionImpl implements
		com.jecn.epros.server.action.designer.system.IJecnElementsLibraryAction {
	private IJecnElementsLibraryServie elementsLibraryService;

	public IJecnElementsLibraryServie getElementsLibraryService() {
		return elementsLibraryService;
	}

	public void setElementsLibraryService(IJecnElementsLibraryServie elementsLibraryService) {
		this.elementsLibraryService = elementsLibraryService;
	}

	@Override
	public void updateElementsLibary(List<String> list, int type) throws Exception {
		elementsLibraryService.updateElementsLibary(list, type);
	}

	@Override
	public List<JecnElementsLibrary> getShowElementsLibraryByType(int type) throws Exception {
		return elementsLibraryService.getShowElementsLibraryByType(type);
	}

	@Override
	public List<JecnElementsLibrary> getAllElementsLibraryByType(int type) throws Exception {
		return elementsLibraryService.getAllElementsLibraryByType(type);
	}

	/**
	 * 待添加元素集合
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnElementsLibrary> getAddElementsLibraryByType(int type) throws Exception {
		return elementsLibraryService.getAddElementsLibraryByType(type);
	}
}
