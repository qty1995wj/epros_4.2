package com.jecn.epros.domain.process.processFile;

/**
 * Created by admin on 2017/4/8.
 */
public class TermDefinition {
    /**
     * 术语名称
     */
    private String name;
    /**
     * 定义
     */
    private String instructions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
