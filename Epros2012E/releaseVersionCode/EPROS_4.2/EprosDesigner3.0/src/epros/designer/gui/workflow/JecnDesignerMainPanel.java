package epros.designer.gui.workflow;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.jecn.epros.server.bean.process.JecnProcessTemplateBean;
import com.jecn.epros.server.bean.process.JecnProcessTemplateFollowBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.ResourceHighEfficiencyTree;
import epros.designer.gui.system.ToolbarSetingButtonPanel;
import epros.designer.gui.tree.JecnModelPanel;
import epros.designer.gui.tree.JecnTreeMainPanel;
import epros.designer.gui.tree.JecnTreeTabbedPane;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.designer.JecnDesigner;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 设计器左面：树面板+流程向导的容器
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesignerMainPanel {
	/** 树面板+流程向导的容器 */
	private JPanel treeGuidePanel = null;
	/** 资源管理器+流程模板的容器 */
	private JecnTreeTabbedPane treeTabbedPane = null;
	/** JecnTreeMainPanel */
	private JecnTreeMainPanel treePanel = null;
	/** 流程模板 */
	private JecnModelPanel modelPanel = null;
	/** 流程向导/大纲面板 */
	private JPanel flowGuidePanel = null;
	/** 设计器的主界面 */
	private JecnSplitPane mainSplitPane = null;
	/** 树和流程向导的分割面板 */
	private JecnSplitPane splitPane = null;
	/** 流程设计向导 */
	private JecnGuidePanel guidePanel = null;
	/** 树面板+流程向导的容器 */
	private final int treeGuideWidth = 250;

	private static JecnDesignerMainPanel designerMainPanel = null;
	/** 存放模型主表数据 */
	private static Map<String, JecnProcessTemplateBean> processTemplateMap = null;
	/** 存放模型从表数据 */
	private static Map<String, JecnProcessTemplateFollowBean> templateFollowMap = null;

	/** 按钮工具栏 */
	private ToolbarSetingButtonPanel toolbarSetingButtonPanel;

	public ToolbarSetingButtonPanel getToolbarSetingButtonPanel() {
		return toolbarSetingButtonPanel;
	}

	public void setToolbarSetingButtonPanel(ToolbarSetingButtonPanel toolbarSetingButtonPanel) {
		this.toolbarSetingButtonPanel = toolbarSetingButtonPanel;
	}

	private JecnDesignerMainPanel() {
		initComponent();
	}

	/**
	 * 
	 * 主界面实例
	 * 
	 */
	public static JecnDesignerMainPanel getDesignerMainPanel() {
		if (designerMainPanel == null) {
			designerMainPanel = new JecnDesignerMainPanel();
		}
		return designerMainPanel;
	}

	public static Map<String, JecnProcessTemplateBean> getProcessTemplateMap() {
		if (processTemplateMap == null) {
			processTemplateMap = new HashMap<String, JecnProcessTemplateBean>();
		}
		return processTemplateMap;
	}

	public static Map<String, JecnProcessTemplateFollowBean> getTemplateFollowMap() {
		if (templateFollowMap == null) {
			templateFollowMap = new HashMap<String, JecnProcessTemplateFollowBean>();
		}
		return templateFollowMap;
	}

	private void initComponent() {
		// 树面板+流程向导的容器的容器
		treeGuidePanel = new JPanel();
		// 资源管理器+流程模板的容器
		treeTabbedPane = new JecnTreeTabbedPane();
		// 树面板
		treePanel = new JecnTreeMainPanel(this);
		// 流程模板
		modelPanel = new JecnModelPanel();
		// 流程向导/大纲面板
		flowGuidePanel = new JPanel();
		// 流程设计向导
		guidePanel = new JecnGuidePanel(MapType.partMap);

		// 左面分割面板:树面板+流程向导的容器
		splitPane = new JecnSplitPane(JSplitPane.VERTICAL_SPLIT, treeGuidePanel, treeTabbedPane, flowGuidePanel);

		// 设计器的主界面
		mainSplitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, JecnDrawMainPanel.getMainPanel(),
				treeGuidePanel, JecnDrawMainPanel.getMainPanel().getSplitPane());
		// 工具栏标题栏设置按钮
		toolbarSetingButtonPanel = new ToolbarSetingButtonPanel();

		Dimension desPanelSize = new Dimension(200, 600);
		Dimension guideSize = new Dimension(0, 270);

		// 主面板
		// 当放大时右面获取多余空间
		mainSplitPane.setResizeWeight(0);
		// 左:右=2:8
		mainSplitPane.setDividerLocation(treeGuideWidth);

		// 左面分割面板:树面板+流程向导的容器
		// 当放大时上面获取多余空间
		splitPane.setResizeWeight(1.0);

		// 树面板+流程向导的容器的容器
		treeGuidePanel.setLayout(new BorderLayout());
		treeGuidePanel.setOpaque(false);
		treeGuidePanel.setPreferredSize(desPanelSize);

		// 流程向导/大纲面板
		flowGuidePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		flowGuidePanel.setOpaque(false);
		flowGuidePanel.setBorder(null);
		flowGuidePanel.setPreferredSize(guideSize);

		// **************添加组件*****************//
		// 树面板+流程向导的容器的容器
		treeGuidePanel.add(splitPane);
		// 设计器的主界面添加到画图工具中主面板上
		JecnDrawMainPanel.getMainPanel().add(mainSplitPane, BorderLayout.CENTER);

		// 添加设置按钮到工具栏标题栏中
		JecnDesigner.insertTitleButton(JecnProperties.getValue("toolbarTitleSetName"), toolbarSetingButtonPanel, 3);

		flowGuidePanel.setLayout(new BorderLayout(0, 0));
		flowGuidePanel.add(guidePanel);

		treeTabbedPane.addTab(JecnProperties.getValue("resourcesManage"), treePanel);
		if (JecnConfigTool.isShowProcessMode() && JecnDesignerCommon.isAdmin() && JecnConstants.isPubShow()) {
			treeTabbedPane.addTab(JecnProperties.getValue("processMode"), modelPanel);
		}
		if (JecnConstants.versionType == 1) {
			JecnDrawMainPanel.getMainPanel().getModuleShowPanel().modelCheckBoxFlase();
		}

	}

	public JecnSplitPane getMainSplitPane() {
		return mainSplitPane;
	}

	public JecnSplitPane getSplitPane() {
		return splitPane;
	}

	public int getTreeGuideWidth() {
		return treeGuideWidth;
	}

	public ResourceHighEfficiencyTree getTree() {
		return treePanel.getTree();
	}

	public JecnTreeMainPanel getTreePanel() {
		return treePanel;
	}

	public JecnGuidePanel getGuidePanel() {
		return guidePanel;
	}

	public JecnModelPanel getModelPanel() {
		return modelPanel;
	}
}
