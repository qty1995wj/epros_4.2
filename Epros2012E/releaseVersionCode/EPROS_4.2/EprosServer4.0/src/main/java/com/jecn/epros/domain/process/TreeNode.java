package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.service.TreeNodeUtils.NodeType;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {


    /**
     * 子节点
     */
    private List<TreeNode> children = new ArrayList<>();
    /**
     * 节点id
     */
    private String id;
    /**
     * 节点名称
     */
    private String text;
    /**
     * 是否为叶子节点
     */
    private boolean isLeaf;
    /**
     * 节点类型
     */
    private NodeType type;
    /**
     * 0目录 1 文件
     */
    private int isDir;

    /**
     * 父节点名称
     */
    private String pText;
    /**
     * 父节点id
     */
    private String pId;

    /**
     * 是否为选中的节点
     */
    private boolean select;

    private boolean isChecked;
    /**
     * 节点类型（流程节点时，默认是流程和架构，1：流程（文件）节点）
     */
    private int nodeType;

    private String status;
    /**
     * 是否有下载权限
     */
    private boolean isDownloadAuth;

    /**
     * 流程、制度 ZIP附件下载
     */
    private boolean isZipDownloadAuth;

    private Integer isFileLocal;// g020 0是从文件库关联的1是从本地上传的

    //人员登录名称
    private String loginName;

    /**
     * 虚拟架构类型目前只有适用部门的
     */
    private NodeType inventedType;
    private String inventedId;

    public TreeNode() {

    }

    public TreeNode(String id, String text, boolean isLeaf, NodeType type, int isDir, String pText, String pId, boolean select, boolean isChecked) {
        this.id = id;
        this.text = text;
        this.isLeaf = isLeaf;
        this.type = type;
        this.isDir = isDir;
        this.pText = pText;
        this.pId = pId;
        this.select = select;
        this.isChecked = isChecked;
    }

    public void setIsLeaf(int childsSize) {
        if (childsSize > 0) {
            this.isLeaf = false;
        } else {
            this.isLeaf = true;
        }
    }

    //---------------------------------------------------------

    public NodeType getInventedType() {
        return inventedType;
    }

    public void setInventedType(NodeType inventedType) {
        this.inventedType = inventedType;
    }

    public String getInventedId() {
        return inventedId;
    }

    public void setInventedId(String inventedId) {
        this.inventedId = inventedId;
    }

    public boolean getIsLeaf() {
        return isLeaf;
    }

    public Integer getIsFileLocal() {
        return isFileLocal;
    }

    public void setIsFileLocal(Integer isFileLocal) {
        this.isFileLocal = isFileLocal;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getpText() {
        return pText;
    }

    public void setpText(String pText) {
        this.pText = pText;
    }


    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

    public int getIsDir() {
        return isDir;
    }

    public void setIsDir(int isDir) {
        this.isDir = isDir;
    }

    public boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        isChecked = isChecked;
    }

    @JsonIgnore
    public int getNodeType() {
        return nodeType;
    }

    public void setNodeType(int nodeType) {
        this.nodeType = nodeType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDownloadAuth() {
        return isDownloadAuth;
    }

    public void setDownloadAuth(boolean downloadAuth) {
        isDownloadAuth = downloadAuth;
    }

    public boolean isZipDownloadAuth() {
        return isZipDownloadAuth;
    }

    public void setZipDownloadAuth(boolean zipDownloadAuth) {
        isZipDownloadAuth = zipDownloadAuth;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
}
