package com.jecn.epros.sqlprovider.server3;

/**
 * 发布sql
 *
 * @author ZHAGNXIAOHU
 * @date： 日期：Dec 19, 2012 时间：4:27:03 PM
 */
public class SqlConstant {
    /**
     * 流程基本信息
     */
    public final static String FLOW_BASIC_INFO_T = "insert into JECN_FLOW_BASIC_INFO "
            + "(FLOW_ID, FLOW_NAME, FLOW_PURPOSE, FLOW_STARTPOINT, FLOW_ENDPOINT, INPUT, "
            + "OUPUT, APPLICABILITY, NOUT_GLOSSARY,"
            + " FLOW_CUSTOM, FILE_ID, IS_SHOW_LINE_X, IS_SHOW_LINE_Y, IS_X_OR_Y, "
            + "DRIVER_TYPE, DRIVER_RULES, TEST_RUN_NUMBER, TYPE_RES_PEOPLE, "
            + "RES_PEOPLE_ID, TYPE_ID, DFLOW_SUPPLEMENT, NO_APPLICABILITY, "
            + "FLOW_SUMMARIZE, EXPIRY,WARD_PEOPLE_ID,FICTION_PEOPLE_ID, "
            + "FLOW_RELATEDFILE, FLOW_CUSTOMONE, FLOW_CUSTOMTWO, FLOW_CUSTOMTHREE, FLOW_CUSTOMFOUR, FLOW_CUSTOMFIVE) "
            + "select FLOW_ID, FLOW_NAME, FLOW_PURPOSE, FLOW_STARTPOINT, FLOW_ENDPOINT,"
            + " INPUT, OUPUT, APPLICABILITY, NOUT_GLOSSARY, "
            + "  FLOW_CUSTOM, FILE_ID, IS_SHOW_LINE_X,"
            + " IS_SHOW_LINE_Y, IS_X_OR_Y, DRIVER_TYPE, DRIVER_RULES,"
            + " TEST_RUN_NUMBER, TYPE_RES_PEOPLE, RES_PEOPLE_ID, TYPE_ID,"
            + " DFLOW_SUPPLEMENT, NO_APPLICABILITY, FLOW_SUMMARIZE,"
            + " EXPIRY,WARD_PEOPLE_ID,FICTION_PEOPLE_ID, FLOW_RELATEDFILE, FLOW_CUSTOMONE, FLOW_CUSTOMTWO, FLOW_CUSTOMTHREE, FLOW_CUSTOMFOUR, FLOW_CUSTOMFIVE from jecn_flow_basic_info_t where flow_id=#{flowId}";
    /**
     * 岗位查阅权限
     */
    public final static String JECN_ACCESS_PERMISSIONS_T = "insert into JECN_ACCESS_PERMISSIONS (ID, FIGURE_ID, RELATE_ID, TYPE) "
            + "SELECT ID, FIGURE_ID, RELATE_ID, TYPE FROM JECN_ACCESS_PERMISSIONS_T WHERE type=#{type} and RELATE_ID=#{rid}";
    /**
     * 部门查阅权限
     */
    public final static String JECN_ORG_ACCESS_PERM_T = "insert into JECN_ORG_ACCESS_PERMISSIONS (ID, ORG_ID, RELATE_ID, TYPE)"
            + "SELECT ID, ORG_ID, RELATE_ID, TYPE FROM JECN_ORG_ACCESS_PERM_T WHERE type=#{type} and RELATE_ID=#{rid}";
    /**
     * 岗位组查阅权限
     */
    public final static String JECN_ORG_GROUP_PERM_T = "insert into JECN_GROUP_PERMISSIONS(ID, POSTGROUP_ID, RELATE_ID, TYPE) select ID, POSTGROUP_ID, RELATE_ID, TYPE from JECN_GROUP_PERMISSIONS_T WHERE type=#{type} AND RELATE_ID=#{rid}";
    /**
     * 发布流程对象
     */
    public final static String JECN_FLOW_STRUCTURE_T = "insert into JECN_FLOW_STRUCTURE (FLOW_ID, PROJECTID, FLOW_NAME, ISFLOW, PRE_FLOW_ID, FLOW_LEVEL, STR_WIDTH, STR_HEIGHT, FLOW_ID_INPUT, IS_PUBLIC, SORT_ID, DEL_STATE, PEOPLE_ID, CREATE_DATE, UPDATE_PEOPLE_ID, UPDATE_DATE, FILE_PATH, HISTORY_ID, ERROR_STATE, DATA_TYPE, OCCUPIER, T_PATH, T_LEVEL )"
            + "SELECT FLOW_ID, PROJECTID, FLOW_NAME, ISFLOW, PRE_FLOW_ID, FLOW_LEVEL, STR_WIDTH, STR_HEIGHT, FLOW_ID_INPUT, IS_PUBLIC, SORT_ID, DEL_STATE, PEOPLE_ID, CREATE_DATE, UPDATE_PEOPLE_ID, UPDATE_DATE, FILE_PATH, HISTORY_ID, ERROR_STATE, DATA_TYPE, OCCUPIER, T_PATH, T_LEVEL "
            + " FROM JECN_FLOW_STRUCTURE_T WHERE FLOW_ID=#{flowId}";
    /**
     * 发布流程驱动规则基本信息
     */
    public final static String JECN_FLOW_DRIVERT = "insert into JECN_FLOW_DRIVER (FLOW_ID, FREQUENCY, START_TIME, END_TIME, QUANTITY)"
            + "SELECT FLOW_ID, FREQUENCY, START_TIME, END_TIME, QUANTITY FROM JECN_FLOW_DRIVERT WHERE FLOW_ID=#{flowId}";
    /**
     * 发布流程保持记录
     */
    public final static String JECN_FLOW_RECORD_T = "insert into JECN_FLOW_RECORD (ID, FLOW_ID, RECORD_NAME, RECORD_SAVE_PEOPLE, SAVE_LOCATION, FILE_TIME, SAVE_TIME, RECORD_APPROACH)"
            + "SELECT ID, FLOW_ID, RECORD_NAME, RECORD_SAVE_PEOPLE, SAVE_LOCATION, FILE_TIME, SAVE_TIME, RECORD_APPROACH "
            + "FROM JECN_FLOW_RECORD_T WHERE FLOW_ID=#{flowId}";

    /**
     * 发布流程支持工具
     */
    public final static String Jecn_Flow_Tool_Related_T = "insert into JECN_FLOW_TOOL_RELATED (FLOW_TOOL_ID, FLOW_ID, FLOW_SUSTAIN_TOOL_ID)"
            + "SELECT FLOW_TOOL_ID, FLOW_ID, FLOW_SUSTAIN_TOOL_ID "
            + "FROM Jecn_Flow_Tool_Related_T WHERE FLOW_ID=#{flowId}";

    /**
     * 添加相关制度的数据
     */
    public final static String FLOW_RELATED_CRITERION_T = "insert into FLOW_RELATED_CRITERION (FLOW_CRITERON_ID, FLOW_ID, CRITERION_CLASS_ID)"
            + "SELECT FLOW_CRITERON_ID, FLOW_ID, CRITERION_CLASS_ID "
            + "FROM FLOW_RELATED_CRITERION_T WHERE FLOW_ID=#{flowId}";

    /**
     * 添加相关标准的数据
     */
    public final static String JECN_FLOW_STANDARD_T = "insert into JECN_FLOW_STANDARD (ID, FLOW_ID, CRITERION_CLASS_ID)"
            + "SELECT ID, FLOW_ID, CRITERION_CLASS_ID "
            + "FROM JECN_FLOW_STANDARD_T WHERE FLOW_ID=#{flowId}";

    /**
     * 添加图形的数据
     */
    public final static String JECN_FLOW_STRUCTURE_IMAGE_T = "insert into JECN_FLOW_STRUCTURE_IMAGE "
            + "(FIGURE_ID, FLOW_ID, FIGURE_TYPE, FIGURE_TEXT, "
            + "START_FIGURE, END_FIGURE, X_POINT, Y_POINT, WIDTH, "
            + "HEIGHT, FONT_SIZE, BGCOLOR, FONTCOLOR, LINK_FLOW_ID, "
            + "LINECOLOR, FONTPOSITION, HAVESHADOW, CIRCUMGYRATE,"
            + " BODYLINE, BODYCOLOR, ISERECT, FIGURE_IMAGE_ID, "
            + "Z_ORDER_INDEX, ACTIVITY_SHOW, ACTIVITY_ID, FONT_BODY,"
            + " FRAME_BODY, FONT_TYPE, ROLE_RES, IS_3D_EFFECT, "
            + "FILL_EFFECTS, LINE_THICKNESS, START_TIME, STOP_TIME,"
            + " TIME_TYPE, SHADOW_COLOR,INNER_CONTROL_RISK,STANDARD_CONDITIONS,ACTIVE_TYPE_ID,ISONLINE,DIVIDING_X,ACTIVITY_INPUT,ACTIVITY_OUTPUT)"
            + "SELECT FIGURE_ID, FLOW_ID, FIGURE_TYPE, FIGURE_TEXT, START_FIGURE, END_FIGURE, X_POINT,"
            + " Y_POINT, WIDTH, HEIGHT, FONT_SIZE, BGCOLOR, FONTCOLOR,"
            + " LINK_FLOW_ID, LINECOLOR, FONTPOSITION, HAVESHADOW, "
            + "CIRCUMGYRATE, BODYLINE, BODYCOLOR, ISERECT, "
            + "FIGURE_IMAGE_ID, Z_ORDER_INDEX, ACTIVITY_SHOW, "
            + "ACTIVITY_ID, FONT_BODY, FRAME_BODY, FONT_TYPE, "
            + "ROLE_RES, IS_3D_EFFECT, FILL_EFFECTS, LINE_THICKNESS, "
            + "START_TIME, STOP_TIME, TIME_TYPE, SHADOW_COLOR ,"
            + "INNER_CONTROL_RISK,STANDARD_CONDITIONS ,ACTIVE_TYPE_ID,ISONLINE,DIVIDING_X,ACTIVITY_INPUT,ACTIVITY_OUTPUT"
            + " FROM JECN_FLOW_STRUCTURE_IMAGE_T WHERE FLOW_ID=#{flowId}";

    /**
     * 角色活动关系表
     */
    public final static String JECN_ROLE_ACTIVE_T = "insert into JECN_ROLE_ACTIVE (AUTO_INCREASE_ID, FIGURE_ROLE_ID, FIGURE_ACTIVE_ID)"
            + "SELECT jrat.AUTO_INCREASE_ID, jrat.FIGURE_ROLE_ID, jrat.FIGURE_ACTIVE_ID "
            + "FROM  JECN_ROLE_ACTIVE_T jrat,jecn_flow_structure_image_t jfsit where jrat.figure_role_id"
            + "        = jfsit.figure_id and jfsit.flow_id=#{flowId}";

    /**
     * 添加线的线段的数据
     */
    public final static String JECN_LINE_SEGMENT_T = "insert into JECN_LINE_SEGMENT (ID, FIGURE_ID, START_X, START_Y, END_X, END_Y)"
            + "SELECT jlst.ID, jlst.FIGURE_ID, jlst.START_X, jlst.START_Y, jlst.END_X, jlst.END_Y "
            + "FROM JECN_LINE_SEGMENT_T jlst,jecn_flow_structure_image_t jfsit"
            + "       where jlst.figure_id=jfsit.figure_id and jfsit.flow_id=#{flowId}";

    /**
     * 添加角色的岗位的数据
     */
    public final static String PROCESS_STATION_RELATED_T = "insert into PROCESS_STATION_RELATED (FLOW_STATION_ID, FIGURE_FLOW_ID, FIGURE_POSITION_ID, TYPE)"
            + "SELECT psrt.FLOW_STATION_ID, psrt.FIGURE_FLOW_ID, psrt.FIGURE_POSITION_ID, psrt.TYPE "
            + "FROM PROCESS_STATION_RELATED_T psrt,jecn_flow_structure_image_t jfsit"
            + "       where psrt.figure_flow_id=jfsit.figure_id and jfsit.flow_id=#{flowId}";

    /**
     * 添加活动的指标的数据
     */
    public final static String JECN_REF_INDICATORST = "insert into JECN_REF_INDICATORS (ID, ACTIVITY_ID, INDICATOR_NAME, INDICATOR_VALUE)"
            + "SELECT jrit.ID, jrit.ACTIVITY_ID, jrit.INDICATOR_NAME, jrit.INDICATOR_VALUE "
            + "FROM JECN_REF_INDICATORST jrit,jecn_flow_structure_image_t jfsi"
            + "      where jrit.activity_id = jfsi.figure_id and jfsi.flow_id=#{flowId}";

    /**
     * 添加活动的输入和操作规范的数据
     */
    public final static String JECN_ACTIVITY_FILE_T = "insert into JECN_ACTIVITY_FILE (FILE_ID, FILE_TYPE,FIGURE_ID,FILE_S_ID)"
            + "SELECT jaft.FILE_ID,"
            + "jaft.FILE_TYPE,"
            + "jaft.FIGURE_ID,"
            + "jaft.FILE_S_ID "
            + "FROM JECN_ACTIVITY_FILE_T jaft,jecn_flow_structure_image_t jfsit"
            + " where  jaft.figure_id=jfsit.figure_id and jfsit.flow_id=#{flowId}";

    /**
     * 添加活动输出的数据
     */
    public final static String JECN_MODE_FILE_T = "insert into JECN_MODE_FILE (MODE_FILE_ID, FIGURE_ID, FILE_M_ID)"
            + "SELECT jmft.MODE_FILE_ID, "
            + "jmft.FIGURE_ID, "
            + "jmft.FILE_M_ID "
            + "FROM JECN_MODE_FILE_T jmft,jecn_flow_structure_image_t jfsit where jmft.figure_id=jfsit.figure_id and jfsit.flow_id=#{flowId}";
    /**
     * 发布流程责任部门
     */
    public final static String JECN_FLOW_RELATED_ORG_T = "insert into JECN_FLOW_RELATED_ORG (FLOW_ORG_ID, FLOW_ID, ORG_ID) "
            + "SELECT FLOW_ORG_ID, FLOW_ID, ORG_ID FROM JECN_FLOW_RELATED_ORG_T WHERE FLOW_ID=#{flowId}";

    /**
     * 获取岗位查阅权限对应的人员
     */
    public final static String POS_ACCESS_PERMISS = "SELECT DISTINCT U.PEOPLE_ID" +
            "  FROM JECN_USER U" +
            " INNER JOIN JECN_USER_POSITION_RELATED U_P" +
            "    ON U.PEOPLE_ID = U_P.PEOPLE_ID" +
            "  LEFT JOIN JECN_ACCESS_PERMISSIONS_T J_P" +
            "    ON U_P.FIGURE_ID = J_P.FIGURE_ID" +
            "   AND J_P.type=#{type}" +
            "   AND J_P.RELATE_ID=#{rid}" +
            " WHERE U.ISLOCK = 0" +
            "   AND J_P.RELATE_ID IS NOT NULL";

    /**
     * 获取岗位查阅权限对应的人员2--审批
     */
    public final static String POS_ACCESS_PERMISS_APPROVE = "SELECT DISTINCT U.PEOPLE_ID" +
            "  FROM JECN_USER U" +
            " INNER JOIN JECN_USER_POSITION_RELATED U_P" +
            "    ON U.PEOPLE_ID = U_P.PEOPLE_ID" +
            "  LEFT JOIN JECN_ACCESS_PERMISSIONS_T J_P" +
            "    ON J_P.FIGURE_ID = U_P.FIGURE_ID" +
            "   AND J_P.RELATE_ID=#{rid}" +
            " WHERE U.ISLOCK = 0" +
            "   AND J_P.RELATE_ID IS NOT NULL";
    /**
     * 获取岗位组查阅权限相对应的人员2--审批
     */
    public final static String GROUP_ACCESS_PERMISS_APPROVE = "SELECT J_U_P_R.PEOPLE_ID FROM JECN_USER_POSITION_RELATED J_U_P_R"
            + " WHERE FIGURE_ID IN(SELECT J_F_I.FIGURE_ID  FROM JECN_FLOW_ORG_IMAGE J_F_I "
            + " WHERE  FIGURE_ID IN (SELECT FIGURE_ID FROM JECN_POSITION_GROUP_R J_P_R"
            + " WHERE J_P_R.GROUP_ID IN(SELECT J_P.POSTGROUP_ID FROM JECN_GROUP_PERMISSIONS J_P WHERE J_P.RELATE_ID=#{rid})))";

    /**
     * 获取部门查阅权限对应的人员2--审批
     */
    public final static String ORG_ACCESS_PERMISS_APPROVE = "select distinct u.people_id"
            + "          from jecn_user u, jecn_user_position_related u_p"
            + "         where u.islock = 0"
            + "           and u.people_id = u_p.people_id"
            + "           and u_p.figure_id in"
            + "               (select distinct j_p.figure_id"
            + "                  from Jecn_Flow_Org_Image j_p, Jecn_Org_Access_Perm_t j_o"
            + "                 where j_p.org_id = j_o.org_id  and j_o.RELATE_ID=#{rid})";

    /**
     * 获取岗位组查阅权限相对应的人员
     */
    public final static String GROUP_ACCESS_PERMISS = "SELECT JU.PEOPLE_ID" +
            "  FROM JECN_USER JU" +
            " INNER JOIN JECN_USER_POSITION_RELATED J_U_P_R" +
            "    ON JU.PEOPLE_ID = J_U_P_R.PEOPLE_ID" +
            " INNER JOIN JECN_FLOW_ORG_IMAGE J_F_I" +
            "    ON J_U_P_R.FIGURE_ID = J_F_I.FIGURE_ID" +
            " INNER JOIN JECN_POSITION_GROUP_R J_P_R" +
            "    ON J_F_I.FIGURE_ID = J_P_R.FIGURE_ID" +
            " INNER JOIN JECN_GROUP_PERMISSIONS J_P" +
            "    ON J_P_R.GROUP_ID = J_P.POSTGROUP_ID" +
            "   AND J_P.type=#{type}" +
            "   AND J_P.RELATE_ID=#{rid} " +
            " WHERE JU.ISLOCK = 0 ";
    /**
     * 获取部门查阅权限对应的人员
     */
    public final static String ORG_ACCESS_PERMISS = "SELECT DISTINCT U.PEOPLE_ID" +
            "  FROM JECN_USER U" +
            " INNER JOIN JECN_USER_POSITION_RELATED U_P" +
            "    ON U.PEOPLE_ID = U_P.PEOPLE_ID" +
            "  LEFT JOIN JECN_FLOW_ORG_IMAGE J_P" +
            "    ON U_P.FIGURE_ID = J_P.FIGURE_ID" +
            " INNER JOIN JECN_ORG_ACCESS_PERM_T J_O" +
            "    ON J_P.ORG_ID = J_O.ORG_ID" +
            "   AND J_O.type=#{type}" +
            "   AND J_O.RELATE_ID=#{rid}" +
            " WHERE U.ISLOCK = 0" +
            "   AND J_P.FIGURE_ID IS NOT NULL";


    /**
     * 获取流程中参与的人员ID
     */
    public final static String FLOW_OART_IN_PEOPLE = "select  u.email_type,u.email, u.people_id"
            + "       from jecn_user u,"
            + "       jecn_user_position_related u_p"
            + "       where u.people_id = u_p.people_id and u.islock=0  and"
            + "      ( u_p.figure_id in"
            + "       (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
            + "       ,jecn_flow_structure_image_t jfsi"
            + "       where psrt.type='0' and psrt.figure_position_id = jfoi.figure_id and jfoi.org_id = jfo.org_id"
            + "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id=#{flowId})"
            + "       or"
            + "       u_p.figure_id in"
            + "        (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
            + "       ,jecn_flow_structure_image_t jfsi,jecn_position_group_r jpgr"
            + "       where psrt.type='1' and psrt.figure_position_id = jpgr.group_id and jpgr.figure_id=jfoi.figure_id"
            + "        and jfoi.org_id = jfo.org_id"
            + "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id=#{flowId}))";
    /**
     * 活动线上信息
     */
    public final static String ACTIVE_ONLINE = "INSERT INTO JECN_ACTIVE_ONLINE "
            + "  (ID, PROCESS_ID, ACTIVE_ID, TOOL_ID, ONLINE_TIME)"
            + "  SELECT T.ID, T.PROCESS_ID, T.ACTIVE_ID, T.TOOL_ID, T.ONLINE_TIME"
            + "    FROM JECN_ACTIVE_ONLINE_T T" + "   WHERE T.PROCESS_ID = #{flowId}";
    /**
     * 活动控制点
     */
    public final static String ACTIVE_CONTROL_POINT = "INSERT INTO JECN_CONTROL_POINT "
            + "  (ID,"
            + "   ACTIVE_ID,"
            + "   CONTROL_CODE,"
            + "   KEY_POINT,"
            + "   FREQUENCY,"
            + "   METHOD,"
            + "   TYPE,"
            + "   RISK_ID,"
            + "   TARGET_ID,"
            + "   CREATE_PERSON,"
            + "   CREATE_TIME,"
            + "   UPDATE_PERSON,"
            + "   UPDATE_TIME,"
            + "   NOTE)"
            + "  SELECT T.ID,"
            + "         T.ACTIVE_ID,"
            + "         T.CONTROL_CODE,"
            + "         T.KEY_POINT,"
            + "         T.FREQUENCY,"
            + "         T.METHOD,"
            + "         T.TYPE,"
            + "         T.RISK_ID,"
            + "         T.TARGET_ID,"
            + "         T.CREATE_PERSON,"
            + "         T.CREATE_TIME,"
            + "         T.UPDATE_PERSON,"
            + "         T.UPDATE_TIME,"
            + "         T.NOTE"
            + "    FROM JECN_CONTROL_POINT_T T"
            + "   WHERE EXISTS (SELECT 1"
            + "            FROM JECN_FLOW_STRUCTURE_IMAGE_T IT"
            + "           WHERE IT.FIGURE_ID = T.ACTIVE_ID"
            + "             AND IT.FLOW_ID = #{flowId})";

    /**
     * 活动相关标准
     */
    public final static String ACTIVE_STANDARD = "INSERT INTO JECN_ACTIVE_STANDARD "
            + "  (ID, ACTIVE_ID, STANDARD_ID, RELA_TYPE, CLAUSE_ID)"
            + "  SELECT T.ID, T.ACTIVE_ID, T.STANDARD_ID, T.RELA_TYPE, T.CLAUSE_ID"
            + "    FROM JECN_ACTIVE_STANDARD_T T"
            + "   WHERE EXISTS (SELECT 1"
            + "            FROM JECN_FLOW_STRUCTURE_IMAGE_T IT"
            + "           WHERE IT.FIGURE_ID = T.ACTIVE_ID"
            + "             AND IT.FLOW_ID = #{flowId})";

}
