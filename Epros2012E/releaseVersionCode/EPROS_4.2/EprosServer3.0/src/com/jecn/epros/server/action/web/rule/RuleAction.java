package com.jecn.epros.server.action.web.rule;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleContent;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.excel.RuleTemplateDownload;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.system.IProcessRuleTypeService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.rule.RuleFileWebBean;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.rule.RuleSearchBean;
import com.jecn.epros.server.webBean.rule.RuleSystemListBean;
import com.opensymphony.xwork2.ActionContext;

public class RuleAction extends BaseAction {
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	/** 制度ID */
	private long ruleId = -1;
	/** 制度名称 */
	private String ruleName;
	/** 制度编号 */
	private String ruleNum;
	/** 制度类别 */
	private long ruleType = -1;
	/** 责任部门 */
	private long orgId = -1;
	/** 责任部门名称 */
	private String orgName;
	/** 查阅部门 */
	private long orgLookId = -1;
	/** 查阅部门名称 */
	private String orgLookName;
	/** 查阅岗位 */
	private long posLookId = -1;
	/** 查阅岗位名称 */
	private String posLookName;
	/** 密级 */
	private long secretLevel = -1;
	private long node;

	/** 文控信息id */
	private long historyId;
	/** 制度类型 */
	private String type;// 制度文件:ruleFile 制度模板文件:ruleModeFile
	// 临时:ruleFileT,ruleModeFileT
	/** 请求来源 applet:浏览端图形连接来源 */
	private String source;
	/** 制度属性 */
	private RuleInfoBean ruleInfoBean;
	/** 制度操作说明 */
	private List<JecnRuleOperationCommon> listJecnRuleOperationCommon;
	/** 制度文控信息 */
	private List<JecnTaskHistoryNew> taskHistoryNewList;
	/** 制度显示名称 */
	private String ruleShowName;
	/** 文件ID */
	private Long fileId;
	/** 制度详细信息 */
	private JecnTaskHistoryNew taskHistoryNew;
	/** 制度内容 */
	private RuleFileWebBean ruleFileWebBean;
	/** 制度service */
	private IRuleService ruleService;
	/** 类别service */
	private IProcessRuleTypeService processRuleTypeService;
	/** 制度类别 */
	private List<ProceeRuleTypeBean> ruleTypeList;

	private static final Logger log = Logger.getLogger(RuleAction.class);

	private String fileName = "ruleName";
	/** 制度操作说明数据 */
	private JecnCreateDoc createDoc;
	/** 查询标示 "ture"查询正式表 "false"查询临时表 */
	private String isPub;
	/** 制度清单 */
	private RuleSystemListBean ruleSystemList;
	/** 拼装数据源 */
	private String stores = "";
	/** 拼装表头 */
	private String columns = "";
	/***/
	private String downRuleName = "制度清单.xls";
	/** 是否为制度文件 */
	private String isRuleFile = "false";
	/** 制度相关文件 */
	private List<JecnRuleOperationCommon> listRuleRelateFile;
	/** 是否显示全部 */
	private boolean showAll = true;
	/** 制度文件上传方式 */
	private int isFileLocal;
	/** g020制度文件内容流 */
	private InputStream ruleContentStream;
	/** 制度类别是否显示 **/
	private boolean ruleTypeIsShow = false;
	private String isApp;

	public InputStream getRuleContentStream() {
		return ruleContentStream;
	}

	public IRuleService getRuleService() {
		return ruleService;
	}

	public void setRuleService(IRuleService ruleService) {
		this.ruleService = ruleService;
	}

	/**
	 * @author yxw 2012-12-3
	 * @description:制度树节点加载
	 * @return
	 */
	public String getChildRules() {
		try {
			List<WebTreeBean> list = ruleService.getChildRulesForWeb(node, getProjectId(), this.getPeopleId(), this
					.isAdmin());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询制度树数据错误！", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-11-26
	 * @description:查询制度列表
	 * @return
	 */
	public String getRuleList() {

		RuleSearchBean ruleSearchBean = new RuleSearchBean();
		ruleSearchBean.setOrgId(orgId);
		ruleSearchBean.setOrgName(orgName);
		ruleSearchBean.setOrgLookId(orgLookId);
		ruleSearchBean.setOrgLookName(orgLookName);
		ruleSearchBean.setPosLookId(posLookId);
		ruleSearchBean.setPosLookName(posLookName);
		ruleSearchBean.setRuleId(ruleId);
		ruleSearchBean.setRuleType(ruleType);
		ruleSearchBean.setRuleMum(ruleNum);
		ruleSearchBean.setRuleName(ruleName);
		ruleSearchBean.setSecretLevel(secretLevel);
		ruleSearchBean.setProjectId(getProjectId());
		ruleSearchBean.setAdmin(isAdmin());
		ruleSearchBean.setPeopleId(getPeopleId());
		try {
			int total = ruleService.searchRule(ruleSearchBean);
			List<RuleInfoBean> listRuleInfoBean = ruleService.searchRule(ruleSearchBean, start, limit);
			JSONArray jsonArray = JSONArray.fromObject(listRuleInfoBean);
			outJsonPage(jsonArray.toString(), total);

		} catch (Exception e) {
			log.error("查询制度列表错误！", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-11-26
	 * @description:得到制度属性
	 * @return
	 */
	public String getRuleProperty() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			ruleInfoBean = ruleService.getRuleInfoBean(ruleId, isp, getPeopleId());
			ruleTypeIsShow = JecnUtil.ifValueIsOneReturnTrue("ruleType");
		} catch (Exception e) {
			log.error("查询制度属性错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-11-26
	 * @description:制度文控信息
	 * @return
	 */
	public String getRuleHistoryList() {
		try {
			int type = 2;
			if ("true".equals(isRuleFile)) {
				type = 3;
			}
			taskHistoryNewList = ruleService.getJecnTaskHistoryNewList(ruleId, type);
		} catch (Exception e) {
			log.error("查询制度文控信息错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-11-26
	 * @description:得到文控信息详情
	 * @return
	 */
	public String getRuleHistoryDetail() {
		try {
			taskHistoryNew = ruleService.getJecnTaskHistoryNew(historyId);
		} catch (Exception e) {
			log.error("获取制度文控详细信息错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-11-26
	 * @description:制度内容
	 * @return
	 */
	public String getRuleContent() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			ruleFileWebBean = ruleService.getRuleFileBean(ruleId, isp, getPeopleId());
			ruleTypeIsShow = JecnUtil.ifValueIsOneReturnTrue("ruleType");
		} catch (Exception e) {
			log.error("获取制度内容出错", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-11-26
	 * @description:制度说明
	 * @return
	 */
	public String getRuleExplanation() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			listJecnRuleOperationCommon = ruleService.findRuleOperationShow(ruleId, isp);
			if (listJecnRuleOperationCommon != null) {
				ruleShowName = listJecnRuleOperationCommon.get(0).getRuleName();
				for (JecnRuleOperationCommon ruleOperationCommon : listJecnRuleOperationCommon) {
					RuleContentT ruleContentT = ruleOperationCommon.getRuleContentT();
					if (ruleContentT != null && ruleContentT.getContentStr() != null) {
						String contentStrT = ruleContentT.getContentStr().trim();
						contentStrT = contentStrT.replaceAll("\n", "<br>");
						ruleContentT.setContentStr(contentStrT.replaceAll(" ", "&nbsp;"));
					}
					RuleContent ruleContent = ruleOperationCommon.getRuleContent();
					if (ruleContent != null && ruleContent.getContentStr() != null) {
						String contentStr = ruleContent.getContentStr().trim();
						contentStr = contentStr.replaceAll("\n", "<br>");
						ruleContent.setContentStr(contentStr.replaceAll(" ", "&nbsp;"));
					}
				}
			}
		} catch (Exception e) {
			log.error("获取制度说明错误！", e);
		}
		return SUCCESS;
	}

	/***
	 * 获取相关文件数据
	 * 
	 */
	public String getRuleRelateFile() {
		try {
			listRuleRelateFile = ruleService.getRuleRelateFile(ruleId);
		} catch (Exception e) {
			log.error("获取相关文件数据出错", e);
		}
		return SUCCESS;

	}

	/**
	 * @author yxw 2012-12-3
	 * @description:获取制度类别
	 * @return
	 */
	public String getAllRuleType() {
		try {
			ruleTypeList = processRuleTypeService.getFlowAttributeType();
			return SUCCESS;
		} catch (Exception e) {
			log.error("获取制度类别错误！", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的制度
	 * @return
	 */
	public String attenTionRule() {
		AttenTionSearchBean attenTionSearchBean = new AttenTionSearchBean();
		attenTionSearchBean.setName(ruleName);
		attenTionSearchBean.setNumber(ruleNum);
		attenTionSearchBean.setOrgId(posLookId);
		attenTionSearchBean.setSecretLevel(secretLevel);
		attenTionSearchBean.setUserId(getPeopleId());
		try {
			int total = ruleService.getTotalAttenTionRule(attenTionSearchBean, getProjectId());
			List<RuleInfoBean> list = ruleService.getAttenTionRule(attenTionSearchBean, start, limit, getProjectId());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);

		} catch (Exception e) {
			log.error("查询我关注的制度报错！", e);
		}

		return null;
	}

	/**
	 * 制度操作说明下载
	 * 
	 * @author fuzhh Jan 22, 2013
	 * @return
	 */
	public String downloadRuleFile() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			isApp = this.getRequest().getParameter("isApp");
			createDoc = ruleService.downloadRuleFile(ruleId, isp);
			fileName = createDoc.getFileName();
			if (!fileName.endsWith(".doc")) {
				fileName = fileName + ".doc";
			}
			// 制度下载日志
			ruleService.saveRuleUseRecord(ruleId, getPeopleId());
		} catch (Exception e) {
			log.error("制度操作说明下载数据异常！", e);
		}
		return SUCCESS;
	}

	/**
	 * 制度清单
	 * 
	 * @author fuzhh Feb 22, 2013
	 * @return
	 */
	public String findRuleSystemList() {
		try {

			// 获取当前制度节点所在级别
			int ruleSysLevel = ruleService.findRuleSysSevel(ruleId);

			// 获取选中节点下制度数据
			ruleSystemList = ruleService.findRuleSystemListBean(ruleId, getProjectId(), ruleSysLevel, showAll);

			// 数据
			StringBuffer storesBuf = new StringBuffer();
			storesBuf.append("[");
			// 级别名称
			for (int i = 0; i < ruleSystemList.getLevelTotal(); i++) {
				storesBuf.append(JecnCommon.storesJson(i + "level", i + "ruleName") + ",");
			}
			// 主键ID
			storesBuf.append(JecnCommon.storesJson("ruleId", "ruleId") + ",");

			// 制度名称
			storesBuf.append(JecnCommon.storesJson("ruleName", "ruleName") + ",");

			// 制度编号
			storesBuf.append(JecnCommon.storesJson("ruleNum", "ruleNum") + ",");

			// 责任部门ID
			storesBuf.append(JecnCommon.storesJson("dutyOrgId", "dutyOrgId") + ",");

			// 责任部门名称
			storesBuf.append(JecnCommon.storesJson("dutyOrg", "dutyOrg") + ",");

			// 版本号
			storesBuf.append(JecnCommon.storesJson("versionId", "versionId") + ",");

			// 发布日期
			storesBuf.append(JecnCommon.storesJson("pubDate", "pubDate") + "]");
			stores = storesBuf.toString();

			// 表头
			StringBuffer columnsBuf = new StringBuffer();
			columnsBuf.append("[new Ext.grid.RowNumberer(),");
			for (int i = 0; i < ruleSystemList.getLevelTotal(); i++) {
				if (i >= ruleSysLevel) {
					columnsBuf.append(JecnCommon.columnsJson(i + JecnUtil.getValue("level"), i + "level", null) + ",");
				}
			}

			// 制度名称
			columnsBuf.append(JecnCommon.columnsJson("制度名称", "ruleName", null) + ",");

			// 制度编号
			columnsBuf.append(JecnCommon.columnsJson("制度编号", "ruleNum", null) + ",");

			// 责任部门名称
			columnsBuf.append(JecnCommon.columnsJson("责任部门名称", "dutyOrg", null) + ",");

			// 版本号
			columnsBuf.append(JecnCommon.columnsJson("版本号", "versionId", null) + ",");

			// 发布日期
			columnsBuf.append(JecnCommon.columnsJson("发布日期", "pubDate", null) + "]");

			columns = columnsBuf.toString();

			/******************** 重新排列制度集合顺序 ************************/
			StringBuffer sb = new StringBuffer("[");
			for (int i = 0; i < ruleSystemList.getListRuleInfoBean().size(); i++) {
				StringBuffer ruleSysStr = new StringBuffer();
				RuleInfoBean ruleInfoBean = ruleSystemList.getListRuleInfoBean().get(i);
				if (ruleInfoBean.getIsDir() == 0) { // 如果是制度目录跳过不再拼装
					continue;
				} else {
					// 查找当前制度的父级目录并且拼装到StringBuffer中
					findPerDir(ruleInfoBean, ruleSysStr, ruleSysLevel);
				}
				// 制度ID
				ruleSysStr.append(JecnCommon.contentJson("ruleId", ruleInfoBean.getRuleId()) + ",");
				// 制度名称
				String ruleSysName = "";
				if (ruleInfoBean.getIsDir() == 1) { // 制度
					if (ruleInfoBean.getTypeByData() == 0) {// 待建
						ruleSysName = "<span style='color: #FB1A1C;'"
						// href='rule.action?type=ruleModeFile&ruleId= +
								// ruleInfoBean.getRuleId()
								+ "' target='_blank'  class='table'>" + ruleInfoBean.getRuleName() + "</span>";
					} else if (ruleInfoBean.getTypeByData() == 1) {// 审批
						ruleSysName = "<a style='color: #e7a100;' href='rule.action?type=ruleModeFile&ruleId="
								+ ruleInfoBean.getRuleId()
								+ "' target='_blank' style='cursor: pointer;' class='table'>"
								+ ruleInfoBean.getRuleName() + "</a>";
					} else if (ruleInfoBean.getTypeByData() == 2) {// 发布
						ruleSysName = "<a style='color: #00c317;' href='rule.action?type=ruleModeFile&ruleId="
								+ ruleInfoBean.getRuleId()
								+ "' target='_blank' style='cursor: pointer;' class='table'>"
								+ ruleInfoBean.getRuleName() + "</a>";
					}
				} else if (ruleInfoBean.getIsDir() == 2) {// 制度文件
					if (ruleInfoBean.getTypeByData() == 0) {// 待建
						ruleSysName = "<span style='color: #FB1A1C;'"
						// href='rule.action?type=ruleFile&ruleId="+ruleInfoBean.getRuleId()+"&fileId="//+
								// ruleInfoBean.getFileId()
								+ "' target='_blank' class='table'>" + ruleInfoBean.getRuleName() + "</span>";
					} else if (ruleInfoBean.getTypeByData() == 1) {// 审批
						ruleSysName = "<a style='color: #e7a100;' href='rule.action?type=ruleFileT&ruleId="
								+ ruleInfoBean.getRuleId()
								+ "' target='_blank' style='cursor: pointer;' class='table'>"
								+ ruleInfoBean.getRuleName() + "</a>";
					} else if (ruleInfoBean.getTypeByData() == 2) {// 发布
						ruleSysName = "<a style='color: #00c317;' href='rule.action?type=ruleFile&ruleId="
								+ ruleInfoBean.getRuleId()
								+ "' target='_blank' style='cursor: pointer;' class='table'>"
								+ ruleInfoBean.getRuleName() + "</a>";
					}
				}

				ruleSysStr.append(JecnCommon.contentJson("ruleName", ruleSysName) + ",");
				// 制度编号
				ruleSysStr.append(JecnCommon.contentJson("ruleNum", ruleInfoBean.getRuleNum()) + ",");
				// 责任部门名称
				ruleSysStr.append(JecnCommon.contentJson("dutyOrg", ruleInfoBean.getDutyOrg()) + ",");
				// 版本号
				ruleSysStr.append(JecnCommon.contentJson("versionId", ruleInfoBean.getVersionId()) + ",");
				// 发布日期
				ruleSysStr.append(JecnCommon.contentJson("pubDate", ruleInfoBean.getPubDate()));
				sb.append("{");
				sb.append(ruleSysStr);
				sb.append("},");
			}
			if (sb.toString().endsWith(",")) {
				// 去掉最后一个逗号
				sb.setLength(sb.length() - 1);
			}
			sb.append("]");
			outJsonString(JecnCommon.composeJson(stores, columns, sb.toString(), ruleSystemList.getNoPubRuleTotal(),
					ruleSystemList.getApproveRuleTotal(), ruleSystemList.getPubRuleTotal(), ruleSystemList
							.getRuleTotal()));
		} catch (Exception e) {
			log.error("查询制度清单数据错误", e);
		}
		return SUCCESS;
	}

	/**
	 * 递归查找
	 * 
	 * @author chehuanbo
	 * @date 2014-10-24
	 * @param ruleInfoBean
	 *            制度的基本信息bean
	 * @param ruleSysStr
	 * @param ruleSysLevel
	 *            当前制度的最大级别
	 * @since V3.06
	 * 
	 */
	public void findPerDir(RuleInfoBean ruleInfoBean, StringBuffer ruleSysStr, int ruleSysLevel) {
		try {
			RuleInfoBean ruleInfoBean1 = ruleInfoBean;
			for (RuleInfoBean ruleInfoBean2 : ruleSystemList.getListRuleInfoBean()) {

				if (ruleInfoBean.getRulePerId() == ruleInfoBean2.getRuleId()) {
					String levelName = ruleInfoBean2.getRuleName();
					ruleSysStr.append(JecnCommon.contentJson(ruleInfoBean2.getLevel() + "ruleName", levelName) + ",");
					ruleInfoBean1 = ruleInfoBean2;
				}
			}
			if (ruleInfoBean1.getLevel() != ruleSysLevel) {
				findPerDir(ruleInfoBean1, ruleSysStr, ruleSysLevel); // 递归查找
			}
		} catch (Exception e) {
			log.error("递归查找当前制度的上级目录的出现异常,RuleAction中的findPerDir()", e);
		}
	}

	/**
	 * 制度清单下载
	 * 
	 * @author fuzhh Feb 25, 2013
	 * @return
	 */
	public String downloadRuleSystemList() {
		try {
			// 获取当前制度节点所在级别
			int ruleSysLevel = ruleService.findRuleSysSevel(ruleId);
			ruleSystemList = ruleService.findRuleSystemListBean(ruleId, getProjectId(), ruleSysLevel, showAll);
			if (ruleSystemList == null) {
				log.info("获取制度清单错误！");
				throw new NullPointerException("获取制度清单错误！");
			}
			byte[] content = RuleTemplateDownload.createExcelile(ruleSystemList, ruleSysLevel);
			createDoc = new JecnCreateDoc();
			createDoc.setBytes(content);
			// 流程清单文件名称
			fileName = downRuleName;
		} catch (Exception e) {
			log.error("下载制度清单发生异常！！！", e);
		}
		return SUCCESS;
	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		// 获得文件
		if (createDoc == null && createDoc.getBytes() == null) {
			// 下载文件信息异常
			outResultHtml(this.getText("fileInformation"), "");
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(createDoc.getBytes());
	}

	/**
	 * @author yxw 2013-1-25
	 * @description:制度详情 用于制度元素链接
	 * @return
	 */
	public String ruleDetail() {
		int ruleType = 0;
		if ("applet".equals(source)) {
			try {
				Rule rule = ruleService.getRule(ruleId);
				if (rule != null) {
					ruleType = rule.getIsDir();
				} else {
					return JecnFinal.NOPUBLIC;
				}
			} catch (Exception e) {

			}
		} else {
			RuleT ruleT = ruleService.get(ruleId);
			ruleType = ruleT.getIsDir();
		}

		if (ruleType == 1) {
			if ("false".equals(isPub)) {
				return "ruleModeFileT";
			}
			return "ruleModeFile";
		} else if (ruleType == 2) {
			if ("false".equals(isPub)) {
				return "ruleFileT";
			} else {
				return "ruleFile";
			}

		}
		return null;
	}

	/**
	 * @author yxw 2013-2-21
	 * @description:双击目录节点 查询目录下制度列表
	 * @return
	 */
	public String ruleListByPid() {
		try {
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get(
					JecnContants.SESSION_KEY);
			// 获取登录人的部门ID集合和岗位ID集合(字符串拼装)
			boolean flag = false;
			if (webLoginBean.isAdmin()) {
				flag = true;
			} else if (webLoginBean.isViewAdmin()) {
				flag = true;
			}
			int total = ruleService.searchRuleListTotal(ruleId, flag, webLoginBean.getProjectId(), webLoginBean
					.getJecnUser().getPeopleId());
			if (total > 0) {
				List<RuleInfoBean> listRuleInfoBean = ruleService.searchRuleList(ruleId, flag, webLoginBean
						.getProjectId(), webLoginBean.getJecnUser().getPeopleId(), start, limit);
				JSONArray jsonArray = JSONArray.fromObject(listRuleInfoBean);
				outJsonPage(jsonArray.toString(), total);
			} else {
				outJsonPage("[]", total);
			}

		} catch (Exception e) {
			log.error("查询制度列表错误！", e);
		}
		return null;
	}

	/**
	 * 组织选择查询数据
	 * 
	 * @author fuzhh Nov 30, 2012
	 * @return
	 */
	public String ruleSearch() {
		try {
			List<JecnTreeBean> list = ruleService.searchByName(ruleName, getProjectId());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询组织数据错误！", e);
		}
		return null;
	}

	/**
	 * @author yxw 2013-3-27
	 * @description:
	 * @return
	 */
	public String rule() {

		if (StringUtils.isBlank(type)) {
			int ruleType = 0;
			if ("applet".equals(source)) {
				try {
					Rule rule = ruleService.getRule(ruleId);
					if (rule != null) {
						ruleType = rule.getIsDir();
					} else {
						return JecnFinal.NOPUBLIC;
					}
				} catch (Exception e) {

				}
			} else {
				RuleT ruleT = ruleService.get(ruleId);
				ruleType = ruleT.getIsDir();
			}

			if (ruleType == 1) {
				if ("false".equals(isPub)) {
					return "ruleModeFileT";
				}
				return "ruleModeFile";
			} else if (ruleType == 2) {
				if ("false".equals(isPub)) {
					return "ruleFileT";
				} else {
					return "ruleFile";
				}

			}
		} else {

			if ("ruleFile".equals(type)) {
				if ("false".equals(isPub)) {
					return "ruleFileT";
				}
				return "ruleFile";
			} else if ("ruleModeFile".equals(type)) {
				if ("false".equals(isPub)) {
					return "ruleModeFileT";
				}
				return "ruleModeFile";
			} else if ("ruleFileT".equals(type)) {
				return "ruleFileT";
			} else if ("ruleModeFileT".equals(type)) {
				return "ruleModeFileT";
			} else if ("downFile".equals(type)) {
				try {
					ruleService.saveRuleUseRecord(ruleId, getPeopleId());
				} catch (Exception e) {
					log.error("记录制度下载出错", e);
				}
				return "downFile";
			}
		}
		return null;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getRuleId() {
		return ruleId;
	}

	public void setRuleId(long ruleId) {
		this.ruleId = ruleId;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public long getRuleType() {
		return ruleType;
	}

	public void setRuleType(long ruleType) {
		this.ruleType = ruleType;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public long getOrgLookId() {
		return orgLookId;
	}

	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}

	public long getPosLookId() {
		return posLookId;
	}

	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}

	public long getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}

	public long getNode() {
		return node;
	}

	public void setNode(long node) {
		this.node = node;
	}

	public String getRuleNum() {
		return ruleNum;
	}

	public void setRuleNum(String ruleNum) {
		this.ruleNum = ruleNum;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgLookName() {
		return orgLookName;
	}

	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}

	public String getPosLookName() {
		return posLookName;
	}

	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}

	public RuleInfoBean getRuleInfoBean() {
		return ruleInfoBean;
	}

	public void setRuleInfoBean(RuleInfoBean ruleInfoBean) {
		this.ruleInfoBean = ruleInfoBean;
	}

	public List<JecnRuleOperationCommon> getListJecnRuleOperationCommon() {
		return listJecnRuleOperationCommon;
	}

	public void setListJecnRuleOperationCommon(List<JecnRuleOperationCommon> listJecnRuleOperationCommon) {
		this.listJecnRuleOperationCommon = listJecnRuleOperationCommon;
	}

	public List<JecnTaskHistoryNew> getTaskHistoryNewList() {
		return taskHistoryNewList;
	}

	public void setTaskHistoryNewList(List<JecnTaskHistoryNew> taskHistoryNewList) {
		this.taskHistoryNewList = taskHistoryNewList;
	}

	public String getRuleShowName() {
		return ruleShowName;
	}

	public void setRuleShowName(String ruleShowName) {
		this.ruleShowName = ruleShowName;
	}

	public long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}

	public JecnTaskHistoryNew getTaskHistoryNew() {
		return taskHistoryNew;
	}

	public void setTaskHistoryNew(JecnTaskHistoryNew taskHistoryNew) {
		this.taskHistoryNew = taskHistoryNew;
	}

	public RuleFileWebBean getRuleFileWebBean() {
		return ruleFileWebBean;
	}

	public void setRuleFileWebBean(RuleFileWebBean ruleFileWebBean) {
		this.ruleFileWebBean = ruleFileWebBean;
	}

	public IProcessRuleTypeService getProcessRuleTypeService() {
		return processRuleTypeService;
	}

	public void setProcessRuleTypeService(IProcessRuleTypeService processRuleTypeService) {
		this.processRuleTypeService = processRuleTypeService;
	}

	public List<ProceeRuleTypeBean> getRuleTypeList() {
		return ruleTypeList;
	}

	public void setRuleTypeList(List<ProceeRuleTypeBean> ruleTypeList) {
		this.ruleTypeList = ruleTypeList;
	}

	public String getFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), fileName, isApp);
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getIsPub() {
		return isPub;
	}

	public void setIsPub(String isPub) {
		this.isPub = isPub;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public RuleSystemListBean getRuleSystemList() {
		return ruleSystemList;
	}

	public void setRuleSystemList(RuleSystemListBean ruleSystemList) {
		this.ruleSystemList = ruleSystemList;
	}

	public String getDownRuleName() {
		return downRuleName;
	}

	public void setDownRuleName(String downRuleName) {
		this.downRuleName = downRuleName;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getIsRuleFile() {
		return isRuleFile;
	}

	public void setIsRuleFile(String isRuleFile) {
		this.isRuleFile = isRuleFile;
	}

	public List<JecnRuleOperationCommon> getListRuleRelateFile() {
		return listRuleRelateFile;
	}

	public void setListRuleRelateFile(List<JecnRuleOperationCommon> listRuleRelateFile) {
		this.listRuleRelateFile = listRuleRelateFile;
	}

	public boolean isShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}

	public int getIsFileLocal() {
		return isFileLocal;
	}

	public void setIsFileLocal(int isFileLocal) {
		this.isFileLocal = isFileLocal;
	}

	public boolean isRuleTypeIsShow() {
		return ruleTypeIsShow;
	}

	public void setRuleTypeIsShow(boolean ruleTypeIsShow) {
		this.ruleTypeIsShow = ruleTypeIsShow;
	}

}
