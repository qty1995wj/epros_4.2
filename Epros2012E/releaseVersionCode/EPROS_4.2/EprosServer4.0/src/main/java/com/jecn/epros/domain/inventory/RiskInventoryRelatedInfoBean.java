package com.jecn.epros.domain.inventory;

import java.util.List;


/**
 * 风险与内控知识库的关联
 *
 * @author user
 *         sql
 */
public class RiskInventoryRelatedInfoBean {
    private Long riskId;
    private List<RiskInventiryInfoBean> listRiskInventiryInfoBean;

    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    public List<RiskInventiryInfoBean> getListRiskInventiryInfoBean() {
        return listRiskInventiryInfoBean;
    }

    public void setListRiskInventiryInfoBean(List<RiskInventiryInfoBean> listRiskInventiryInfoBean) {
        this.listRiskInventiryInfoBean = listRiskInventiryInfoBean;
    }
}
