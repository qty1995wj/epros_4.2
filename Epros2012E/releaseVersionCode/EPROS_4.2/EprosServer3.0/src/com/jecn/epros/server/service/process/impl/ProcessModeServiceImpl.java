package com.jecn.epros.server.service.process.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.process.IProcessModeDao;
import com.jecn.epros.server.service.process.IProcessModeService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

@Transactional
public class ProcessModeServiceImpl extends
		AbsBaseService<JecnFlowStructureT, Long> implements IProcessModeService {
	private final static Log log = LogFactory
			.getLog(ProcessModeServiceImpl.class);
	private IProcessModeDao processModeDao;

	public IProcessModeDao getProcessModeDao() {
		return processModeDao;
	}

	public void setProcessModeDao(IProcessModeDao processModeDao) {
		this.processModeDao = processModeDao;
		this.baseDao = processModeDao;
	}

	@Override
	public void deleteFlowModel(List<Long> listIds) throws Exception {
		try {
			String sql = "";
			// 模板ID
			Set<Long> setIds = new HashSet<Long>();
			// 流程模板元素表主键ID
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JFF WHERE JFF.FLOW_ID in"
						+ JecnCommonSql.getIds(listIds)
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_JECN INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_JECN.FLOW_ID=JFS.PRE_FLOW_ID)"
						+ " select distinct FLOW_ID from MY_JECN WHERE DATA_TYPE=1";
				List<Object> list = processModeDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select distinct jm.FLOW_ID from JECN_FLOW_STRUCTURE_T jm WHERE jm.DATA_TYPE=1"
						+ " connect by prior jm.FLOW_ID = jm.PRE_FLOW_ID START WITH jm.FLOW_ID in "
						+ JecnCommonSql.getIds(listIds);
				List<Object> list = processModeDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				String hql = "select flowId,perFlowId from JecnFlowStructureT where DATA_TYPE=1";
				List<Object[]> listAll = processModeDao.listHql(hql);
				setIds = JecnCommon.getAllChilds(listIds, listAll);
			}
			if (setIds.size() == 0) {
				return;
			}
			// 删除流程模板元素表
			sql = "delete from JecnFlowStructureImageT where flowId in";
			List<String> listHqls = JecnCommonSql.getSetSqls(sql, setIds);
			for (String hql : listHqls) {
				processModeDao.execteHql(hql);
			}
			// 删除流程模板
			sql = "delete from JecnFlowStructureT where flowId in";
			listHqls = JecnCommonSql.getSetSqls(sql, setIds);
			for (String hql : listHqls) {
				processModeDao.execteHql(hql);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getAllFlowMapModels() throws Exception {
		try {
			String sql = "select fm.FLOW_ID,fm.FLOW_NAME,fm.isflow,fm.PRE_FLOW_ID,"
					+ "(select count(*) from JECN_FLOW_STRUCTURE_T where data_type=1 and PRE_FLOW_ID=fm.FLOW_ID) as count"
					+ " from JECN_FLOW_STRUCTURE_T fm where fm.data_type=1"
					+ " order by fm.PRE_FLOW_ID,fm.SORT_ID,fm.FLOW_ID";
			List<Object[]> list = processModeDao.listNativeSql(sql);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getFlowModels(Long flowMapModelId)
			throws Exception {
		try {
			String sql = "select fm.FLOW_ID,fm.FLOW_NAME,fm.isflow,fm.PRE_FLOW_ID,"
					+ "(select count(*) from JECN_FLOW_STRUCTURE_T where data_type=1 and PRE_FLOW_ID=fm.FLOW_ID) as count"
					+ " from JECN_FLOW_STRUCTURE_T fm  where data_type=1 and fm.PRE_FLOW_ID = ?"
					+ " order by fm.PRE_FLOW_ID,fm.SORT_ID,fm.FLOW_ID";
			List<Object[]> list = processModeDao.listNativeSql(sql,
					flowMapModelId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 将流程模板的对象 转为树JecnTreeBean对象
	 * 
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjects(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null
					|| obj[2] == null || obj[3] == null || obj[4] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();

			// 流程模板节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 流程模板名称
			jecnTreeBean.setName(obj[1].toString());
			// 模板类型
			if ("0".equals(obj[2].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.processMapMode);
			} else if ("1".equals(obj[2].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.processMode);
			}
			// 流程模板父ID
			jecnTreeBean.setPid(Long.valueOf(obj[3].toString()));
			// 流程模板父类名称
			jecnTreeBean.setPname("");

			// 是否子节点
			if (Integer.parseInt(obj[4].toString()) > 0) {
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId) throws Exception {
		try {
			String hql = " update JecnFlowStructureT set perFlowId=? where flowId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, listIds);
			for (String str : listStr) {
				processModeDao.execteHql(str, pId);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void reName(String newName, Long id, Long updatePersonId)
			throws Exception {
		try {
			String hql = " update JecnFlowStructureT set flowName=?,updatePeopleId=?,updateDate=? where flowId =?";
			processModeDao.execteHql(hql, newName, updatePersonId, new Date(),
					id);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchByName(String name, int type)
			throws Exception {
		try {
			String sql = "select t.FLOW_ID,t.FLOW_NAME,t.ISFLOW,t.PRE_FLOW_ID,(SELECT COUNT(*) FROM JECN_FLOW_STRUCTURE_T WHERE PRE_FLOW_ID = t.FLOW_ID) AS COUNT"
					+ " FROM JECN_FLOW_STRUCTURE_T t WHERE t.FLOW_NAME like ? AND t.DATA_TYPE=1 ";
			if (type == 0 || type == 1) {
				sql = sql + " and  t.ISFLOW = ? ";
			}
			sql = sql + " ORDER BY t.PRE_FLOW_ID,t.SORT_ID,t.FLOW_ID";
			List<Object[]> list = null;
			if (type == 0 || type == 1) {
				list = processModeDao.listNativeSql(sql, 0, 20,
						"%" + name + "%",type );
			} else {
				list = processModeDao.listNativeSql(sql, 0, 20,
						"%" + name + "%");
			}
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId)
			throws Exception {
		try {
			String hql = "from JecnFlowStructureT where dataType=1 and perFlowId=?";
			List<JecnFlowStructureT> listJecnFlowStructure = processModeDao
					.listHql(hql, pId);
			List<JecnFlowStructureT> listUpdate = new ArrayList<JecnFlowStructureT>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (JecnFlowStructureT jecnFlowStructureT : listJecnFlowStructure) {
					if (jecnTreeDragBean.getId().equals(
							jecnFlowStructureT.getFlowId())) {
						if (jecnFlowStructureT.getSortId() == null
								|| (jecnTreeDragBean.getSortId() != jecnFlowStructureT
										.getSortId().intValue())) {
							jecnFlowStructureT.setSortId(Long
									.valueOf(jecnTreeDragBean.getSortId()));
							listUpdate.add(jecnFlowStructureT);
							break;
						}
					}
				}
			}
			for (JecnFlowStructureT jecnFlowStructureT : listUpdate) {
				processModeDao.update(jecnFlowStructureT);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 新建流程模板管理
	 * 
	 * @param jecnFlowStructureMode
	 * @param isXorY
	 *            0横向图，1 纵向图
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long saveFlowModel(JecnFlowStructureT jecnFlowStructureT, int isXorY)
			throws Exception {
		processModeDao.save(jecnFlowStructureT);
		if (isXorY != -1) {// 0横向，1纵向图
			// 流程基本信息表
			JecnFlowBasicInfoT basicInfoT = new JecnFlowBasicInfoT();
			basicInfoT.setFlowId(jecnFlowStructureT.getFlowId());
			basicInfoT.setIsXorY((long) isXorY);
			basicInfoT.setFlowName(jecnFlowStructureT.getFlowName());
			processModeDao.getSession().save(basicInfoT);
		}
		return jecnFlowStructureT.getFlowId();
	}

}
