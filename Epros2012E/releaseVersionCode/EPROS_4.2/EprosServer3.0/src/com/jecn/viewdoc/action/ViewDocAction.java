package com.jecn.viewdoc.action;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.viewdoc.bean.ViewDocContentIdCache;
import com.jecn.viewdoc.convert.ViewDocsManager;
import com.jecn.viewdoc.convert.ViewDocsPathManager;
import com.jecn.viewdoc.convert.view.ProcessCommonView;
import com.jecn.viewdoc.convert.view.ProcessPageofficeView;
import com.jecn.viewdoc.convert.view.View;
import com.jecn.viewdoc.service.IViewDocService;

public class ViewDocAction extends BaseAction {

	private static final long serialVersionUID = 1L;

	@Resource
	private IFileService fileService;
	@Resource(name = "RuleServiceImpl")
	private IRuleService ruleService;
	@Resource
	private IStandardService standardService;
	@Resource(name = "FlowStructureServiceImpl")
	private IFlowStructureService flowStructureService;
	@Resource(name = "WebProcessServiceImpl")
	private IWebProcessService webProcessService;

	@Resource
	private IViewDocService viewDocService;
	/** type=0和1时为文件ID，2和3时为制度ID */
	private Long relatedId;
	private Boolean isPub;
	/** 文件(未发布)0 文件(已发布)1 制度文件(未发布) 2 */
	private Integer type;
	private String viewDocPath;
	private Map<String, String> fileInfo;
	// 文件预览 嵌套地址
	private String viewPath = "pdfjs/web/view.jsp";
	/**
	 * 文控id
	 */
	private Long h;
	private String mapIconPath;
	private Long userId;

	/**
	 * @deprecated
	 * @return
	 */
	public String viewDoc() {
		ViewDocContentIdCache currentCache = null;
		boolean isSuccess = false;
		try {
			fileInfo = getNameAndContentMaps();
			if (!checkFileViewable(fileInfo.get("FileName"))) {
				return ERROR;
			}
			currentCache = viewDocService.getCache(relatedId, type);
			if (currentCache == null) {
				currentCache = createCache();
				viewDocService.save(currentCache);
			} else {
				if (isNewestCache(Long.valueOf(fileInfo.get("ContentId")), currentCache.getContentId())
						&& isExists(currentCache)) {
					currentCache.getCachePath();
				} else {
					FileUtils.deleteQuietly(new File(getAbsoluteCachePath(currentCache.getCachePath())));
					FileOpenBean fileOpenBean = getFileOpenBean();
					currentCache.setCachePath(getViewDocPath(fileOpenBean));
					currentCache.setContentId(fileOpenBean.getId());
					viewDocService.update(currentCache);
				}
			}
			isSuccess = true;
		} catch (Exception e) {
			log.error("获取文件：[" + fileInfo.get("FileName") + "]时，出现异常", e);
		}
		if (isSuccess) {
			if (currentCache.getCachePath().endsWith("html")) {
				viewDocPath = currentCache.getCachePath();
			} else {
				String params = assembleFileNameAndDownloadURL(currentCache.getCachePath());
				viewDocPath = ViewDocsPathManager.VIEWER_JS + params;
			}
		} else {
			viewDocPath = getDownloadFileURL();
		}
		return SUCCESS;
	}

	/**
	 * 流程文件预览
	 * 
	 * @return
	 */
	public String viewProcess() {
		if (JecnConfigTool.isLiBangLoginType()) {
			View view = new ProcessPageofficeView(webProcessService, relatedId, TreeNodeType.process, isPub, userId, h,
					getRequest(), getResponse());
			viewDocPath = view.getPath();
			return "pageoffice";
		} else {
			View view = new ProcessCommonView(webProcessService, relatedId, TreeNodeType.process, isPub, userId, h,
					getRequest(), getResponse());
			viewDocPath = view.getPath();
			return SUCCESS;
		}
	}

	public String viewProcessIcon() {
		if (StringUtils.isBlank(mapIconPath) || StringUtils.isBlank(JecnContants.jecn_path)) {
			log.info("mapIconPath=" + mapIconPath + ";JecnContants.jecn_path=" + JecnContants.jecn_path);
			this.setErrorInfo(getText("downFileNoFind"));
			return ERRORINFO;
		}
		File file = new File(JecnContants.jecn_path + mapIconPath);
		if (!file.exists() || !file.isFile()) {// 判断文件是否是标准文件，是否存在
			log.info("不是文件或文件不存在。path=" + JecnContants.jecn_path + mapIconPath);
			this.setErrorInfo(getText("downFileNoFind"));
			return ERRORINFO;
		}

		String flowName = getMapName();
		fileInfo = new HashMap<String, String>();
		try {
			// TODO 文件名称暂时以文件id替代
			byte[] results = JecnUtil.getByte(file);
			viewDocPath = ViewDocsManager.buildViewDoc(results, flowName, "PROCESS");
			fileInfo.put("FileName", flowName);
		} catch (Exception e) {
			log.error("", e);
		}

		return SUCCESS;
	}

	private String getMapName() {
		if (StringUtils.isBlank(mapIconPath) || relatedId == null) {
			log.error("下载图片为空! mapIconPath = " + mapIconPath + " flowId = " + relatedId);
			return "";
		}
		int index = mapIconPath.lastIndexOf(".");
		String mapName = null;
		// 保存图片名称
		String name = null;
		try {
			String isApp = this.getRequest().getParameter("isApp");
			// 暂时未处理任务查阅图片情况
			if ("true".equals(isPub)) {
				JecnFlowStructure flowStructure = flowStructureService.findJecnFlowStructureById(relatedId);
				mapName = flowStructure.getFlowName();
			} else {
				JecnFlowStructureT flowStructure = flowStructureService.get(relatedId);
				mapName = flowStructure.getFlowName();
			}

			log.info("mapName = " + mapName);
			if (StringUtils.isBlank(mapName)) {
				name = "new Icon";
			} else {
				name = mapName;
			}
			if (index >= 0) {
				name = name + mapIconPath.substring(index);
			}
			// 通过浏览器转码
			name = JecnCommon.getDownFileNameByEncoding(getResponse(), name, isApp);
			log.info("通过浏览器转码 mapName = " + mapName);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}

		return name;
	}

	private boolean isExists(ViewDocContentIdCache currentCache) throws Exception {
		File file = new File(getAbsoluteCachePath(currentCache.getCachePath()));
		return file.exists();
	}

	private String getAbsoluteCachePath(String cachePath) {
		return JecnPath.APP_PATH + cachePath.substring(ViewDocsPathManager.RELAVTIVE_PATH.length(), cachePath.length());
	}

	private String assembleFileNameAndDownloadURL(String file) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("?name=");
		try {
			buffer.append(URLEncoder.encode(fileInfo.get("FileName"), "UTF-8").replace("+", "%20"));
		} catch (UnsupportedEncodingException e) {
			log.error("转码异常", e);
		}
		buffer.append("&file=" + file);
		buffer.append("&");

		buffer.append("downloadURL=");
		/** 文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 */
		buffer.append(getDownloadFileURL());
		return buffer.toString();
	}

	/**
	 * 文件预览URL：history 情况下，不支持文件下载
	 * 
	 * @return
	 */
	private String getDownloadFileURL() {
		if (!ViewDocsManager.enabledOnlineViewDownload() || type == null) {
			return "none";
		}
		StringBuffer buffer = new StringBuffer();
		switch (type) {
		case 0:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("isPub=false");
			break;
		case 1:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("isPub=true");
			break;
		case 2:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=ruleLocal");
			buffer.append("&");
			buffer.append("isPub=true");
			break;
		case 3:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=ruleLocal");
			buffer.append("&");
			buffer.append("isPub=false");
			break;
		case 4:
		case 5:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=processFile");
			buffer.append("&");
			buffer.append("isPub=false");
			break;
		case 6:
			buffer.append("downloadFileAndRuleFile.action?");
			buffer.append("fileId=" + relatedId);
			buffer.append("&");
			buffer.append("downFileType=standardFile");
			break;
		default:
			break;
		}
		return buffer.toString();
	}

	private ViewDocContentIdCache createCache() throws Exception {
		ViewDocContentIdCache cache = new ViewDocContentIdCache();
		cache.setRelatedId(relatedId);
		cache.setType(type);

		FileOpenBean fileOpenBean = getFileOpenBean();
		cache.setCachePath(getViewDocPath(fileOpenBean));
		cache.setContentId(fileOpenBean.getId());
		return cache;
	}

	private String getViewDocPath(FileOpenBean fileOpenBean) throws Exception {
		switch (type) {
		case 0:
		case 1:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(),
					isPub ? "FILE" : "FILE_T");
			break;
		case 2:
		case 3:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(),
					isPub ? "RULE" : "RULE_T");
			break;
		case 4:
		case 5:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(),
					"PROCESS_FILE");
			break;
		case 6:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(),
					"STANDARD_FILE");
			break;
		default:
			break;
		}
		return viewDocPath;
	}

	/**
	 * 获取文件名称和文件内容主键ID
	 * 
	 * @param cache
	 * @param fileInfo
	 * @throws Exception
	 */
	private FileOpenBean getFileOpenBean() throws Exception {
		FileOpenBean fileOpenBean = null;
		switch (type) {
		case 0:
		case 1:
			fileOpenBean = fileService.openVersion(null, Long.valueOf(fileInfo.get("ContentId")));
			break;
		case 2:
		case 3:
			fileOpenBean = ruleService.g020OpenRuleFile(Long.valueOf(fileInfo.get("ContentId")));
			break;
		case 4:
		case 5:
			fileOpenBean = flowStructureService.openFileContent(Long.valueOf(fileInfo.get("ContentId")));
			break;
		case 6:
			fileOpenBean = standardService.openFileContent(Long.valueOf(fileInfo.get("ContentId")));
			break;
		default:
			break;
		}

		return fileOpenBean;
	}

	private Map<String, String> getNameAndContentMaps() throws Exception {
		if (type == null || relatedId == null || isPub == null) {
			throw new IllegalArgumentException("在线预览获取参数非法！type = " + type + "relatedId=" + relatedId + "isPub="
					+ isPub);
		}
		Map<String, String> fileInfo = null;
		switch (type) {
		case 0:
		case 1:
			fileInfo = fileService.getFileNameAndContentId(relatedId, isPub);
			break;
		case 2:
		case 3:
			fileInfo = ruleService.getRuleNameAndContentId(relatedId, isPub);
			break;
		case 4:
		case 5:
			fileInfo = flowStructureService.getProcessFileNameAndContentId(relatedId, isPub);
			break;
		case 6:
			fileInfo = new HashMap<String, String>();
			FileOpenBean openBean = standardService.openFileContent(relatedId);
			fileInfo.put("FileName", openBean.getName());
			fileInfo.put("ContentId", String.valueOf(openBean.getId()));
			break;
		default:
			break;
		}
		return fileInfo;
	}

	private boolean isNewestCache(Long curContentId, Long cacheContentId) {
		return curContentId.longValue() == cacheContentId;
	}

	private boolean checkFileViewable(String fileName) {
		return ViewDocsManager.isAllowedExtension(fileName);
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public Boolean getIsPub() {
		return isPub;
	}

	public void setIsPub(Boolean isPub) {
		this.isPub = isPub;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getViewDocPath() {
		return viewDocPath;
	}

	public void setViewDocPath(String viewDocPath) {
		this.viewDocPath = viewDocPath;
	}

	public Long getH() {
		return h;
	}

	public void setH(Long h) {
		this.h = h;
	}

	public String getViewPath() {
		return viewPath;
	}

	public void setViewPath(String viewPath) {
		this.viewPath = viewPath;
	}

	public void setMapIconPath(String mapIconPath) {
		this.mapIconPath = mapIconPath;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
