package com.jecn.epros.service.standard;

import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.standard.SearchStandardResultBean;
import com.jecn.epros.domain.standard.StandardBaseInfoBean;
import com.jecn.epros.domain.standard.StandardRelatedFileBean;

import java.util.List;
import java.util.Map;

public interface StandardService {
    /**
     * 树节点展开
     *
     * @param map
     * @return
     */
    public List<TreeNode> findChildStandards(Map<String, Object> map);


    /**
     * 概括信息
     *
     * @param map
     * @return
     */
    public StandardBaseInfoBean findBaseInfoBean(Map<String, Object> map);


    /**
     * 相关文件
     *
     * @param map
     * @return
     */
    public StandardRelatedFileBean findRelatedFiles(Map<String, Object> map);


    /**
     * 搜索
     *
     * @param map
     * @return
     */
    public List<SearchStandardResultBean> searchStandard(Map<String, Object> map);

    /**
     * 搜索 总数
     *
     * @param map
     * @return
     */
    public int searchStandardTotal(Map<String, Object> map);


    public Inventory getInventory(Map<String,Object> paramMap);
}
