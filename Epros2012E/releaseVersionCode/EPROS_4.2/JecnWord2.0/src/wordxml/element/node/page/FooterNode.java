package wordxml.element.node.page;

import org.dom4j.Element;
import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.WordElementFactory;
import wordxml.element.dom.page.Footer;

/**
 * 页脚dom
 * 
 * @author admin
 * 
 */
public class FooterNode {

	/**
	 * 创建页脚数据
	 * 
	 * @param footer
	 * @param sectPr
	 */
	protected static void createFooter(Footer footer, Element sectPr) {
		if (footer == null || sectPr == null) {
			return;
		}
		Element ftr = sectPr.addElement("w:ftr").addAttribute("w:type",
				footer.getType().toString());
		for (IBodyElement bodyEle : footer.getBodyElements()) {
			WordElementFactory.produceElement(bodyEle, ftr);
		}

	}
}
