package epros.draw.gui.swing;

import java.awt.Dimension;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 自定义属性的面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPanel extends JPanel {

	public JecnPanel() {
		initComponents();
	}

	/**
	 * 
	 * 固定大小
	 * 
	 * @param width
	 *            int 宽
	 * @param height
	 *            int 高
	 */
	public JecnPanel(int width, int height) {
		this();
		Dimension size = new Dimension(width, height);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setMaximumSize(size);
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {
		// 设置布局
		this.setLayout(new GridBagLayout());
		// 设置背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

	}
}
