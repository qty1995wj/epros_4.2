package epros.designer.gui.popedom.organization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.service.process.JecnSaveOrg;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 新建岗位
 * 
 * @author 2012-06-08
 * 
 *         修改人：chehuanbo 时间：2014-010-23 描述：添加统一部门允许岗位名称相同 版本：V3.06
 * 
 */

public class AddPositionDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddPositionDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 控件显示面板 */
	private JPanel topPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = null;

	/** 名称Lab */
	private JLabel nameLab = null;

	/** 名称填写框 */
	private JTextField nameTextField = null;

	/** 编号Lab */
	private JLabel numLab = null;

	/*** 编号填写框 */
	private JTextField numTextField = null;

	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBut = null;

	/** 名称验证提示 */
	protected JLabel promptLab = null;

	/** 判断是否点击的确定按钮 */
	private boolean isOk = false;

	/** 设置面板控件大小 */
	Dimension dimension = null;

	/** 设置统一部门下岗位名称相同 0:不允许 1：允许 */
	private Long allowSamePosName = null;

	/** 名称必填项 *号提示 */
	private JLabel requiredMarkLab = new JLabel(JecnProperties.getValue("required"));
	/** 岗位编号必填项 *号提示 */
	private JLabel requiredNumLab = new JLabel(JecnProperties.getValue("required"));
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddPositionDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		initCompotents();
		initLayout();
		this.setModal(true);
	}

	// public String getName() {
	// return name;
	// }
	//
	// public void setName(String name) {
	// this.name = name;
	// this.nameTextField.setText(name);
	// }

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 控件显示面板
		topPanel = new JPanel();

		// 按钮面板
		buttonPanel = new JPanel();

		// 名称Lab
		nameLab = new JLabel();

		// 名称填写框
		nameTextField = new JTextField();

		// 编号Lab
		numLab = new JLabel();

		// 编号填写框
		numTextField = new JTextField();

		// 名称验证提示
		promptLab = new JLabel();

		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));

		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

		// 设置Dialog大小
		this.setSize(330, 150);

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置控件显示面板的默认背景色
		topPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置按钮面板的默认背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		dimension = new Dimension(330, 90);
		mainPanel.setPreferredSize(dimension);
		mainPanel.setMaximumSize(dimension);
		mainPanel.setMinimumSize(dimension);

		// 设置控件显示面板的大小
		dimension = new Dimension(330, 80);
		topPanel.setPreferredSize(dimension);
		topPanel.setMaximumSize(dimension);
		topPanel.setMinimumSize(dimension);

		// 设置按钮面板的大小
		dimension = new Dimension(323, 30);
		buttonPanel.setPreferredSize(dimension);
		buttonPanel.setMaximumSize(dimension);
		buttonPanel.setMinimumSize(dimension);

		// 设置验证提示文字颜色
		promptLab.setForeground(Color.red);

		// 为确认按钮增加回车事件
		this.getRootPane().setDefaultButton(okBut);

		requiredMarkLab.setForeground(Color.red);
		requiredNumLab.setForeground(Color.red);
		// 岗位编号添加默认值
		this.numTextField.setText(DrawCommon.getUUID());

		try {
			allowSamePosName = Long.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "allowSamePosName"));
		} catch (Exception e) {
			log.error("AddPositionDialog  is error", e);
		}
	}

	/**
	 * 布局
	 */
	protected void initLayout() {
		// 主面板布局
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 5, 1, 5);
		Insets insetb = new Insets(1, 5, 5, 5);
		Insets insett = new Insets(5, 5, 1, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(topPanel, c);
		// 控件显示面板 布局
		topPanel.setLayout(new GridBagLayout());
		// topPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 名称Lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetb, 0, 0);
		topPanel.add(nameLab, c);
		// 名称填写框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetb, 0, 0);
		topPanel.add(nameTextField, c);
		// *号提示
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 0, 0, 1), 0, 0);
		topPanel.add(requiredMarkLab, c);
		// 编号Lab
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insett, 0, 0);
		topPanel.add(numLab, c);
		// 编号填写框
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insett, 0, 0);
		topPanel.add(numTextField, c);

		// *号提示
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 0, 0, 1), 0, 0);
		topPanel.add(requiredNumLab, c);
		// 按钮面板 布局
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// buttonPanel.setBorder(BorderFactory.createTitledBorder(""));
		buttonPanel.add(promptLab);
		// 确定
		buttonPanel.add(okBut);
		// 取消
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

		// 确定按钮事件监听
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消按钮事件监听
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
		// Dialog标题
		this.setTitle(getDialogTitle());

		// 设置名称Lab的值
		this.nameLab.setText(getNameLab());
		// 设置编号Lab的值
		this.numLab.setText(JecnProperties.getValue("actBaseNumC"));

		// 设置Dialog的大小不被改变
		this.setResizable(false);
	}

	// 获取Dialog标题
	public String getDialogTitle() {
		return JecnProperties.getValue("newPosition");
	}

	// 获取名称Lab
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	// 确定事件
	public void okButtonAction() {

		String name = nameTextField.getText();
		// nameTextField.setText(name);
		String num = numTextField.getText();

		// 验证名称是否正确
		if (!validateName(JecnProperties.getValue("name"), name, promptLab)) {
			return;
		}
		// 验证编号是否正确
		if (!validateName(JecnProperties.getValue("actBaseNum"), num, promptLab)) {
			return;
		}
		try {
			// 判断是否允许同一部门下岗位名称相同
			if (allowSamePosName == 0) {
				// 验证是否重名
				if (validateNodeRepeat(name.trim())) {
					promptLab.setText(JecnProperties.getValue("nameHaved"));
					return;
				} else {
					promptLab.setText("");
				}
			}
			// 验证编号全表唯一
			if (ConnectionPool.getOrganizationAction().validateAddUpdatePosNum(num.trim(), null)) {
				promptLab.setText(JecnProperties.getValue("posNumHaved"));
				return;
			} else {
				promptLab.setText("");
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			log.error("AddPositionDialog okButtonAction is error！", e);
			return;

		}
		saveData();
		isOk = true;
	}

	public boolean validateName(String nameLab, String name, JLabel jLable) {
		// return JecnValidateCommon.validateNameNoRestrict(name, promptLab);
		String s = "";
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			s = JecnProperties.getValue("chinaColon") + JecnProperties.getValue("isNotEmpty");
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {
			// 名称不能超过122个字符或61个汉字
			s = JecnUserCheckInfoData.getNameLengthInfo();
		}
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(nameLab + s);
			return false;
		}
		jLable.setText("");
		return true;
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证是否重名
	 * @return
	 */
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.position);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:保存数据,并显示
	 */
	public void saveData() {
		// 数据库执行添加
		try {
			JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.PositionFigure);
			JecnFlowOrgImage jecnFlowOrgImage = new JecnFlowOrgImage();
			JecnSaveOrg.getFlowOrgImageData(figureData, pNode.getJecnTreeBean().getId(), jecnFlowOrgImage);
			jecnFlowOrgImage.setFigureText(this.nameTextField.getText().trim());
			jecnFlowOrgImage.setFigureNumberId(this.numTextField.getText().trim());
			jecnFlowOrgImage.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode)));
			Long id = ConnectionPool.getOrganizationAction().addPosition(jecnFlowOrgImage);

			// 向树节点添加新建的组织节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(this.nameTextField.getText().trim());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setNumberId(this.numTextField.getText().trim());
			jecnTreeBean.setTreeNodeType(TreeNodeType.position);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			// 关闭窗体
			this.dispose();
		} catch (Exception e) {
			log.error("AddPositionDialog saveData is error", e);
		}
	}

	// 取消事件
	public void cancelButtonAction() {
		isOk = false;
		this.dispose();
	}

	public JTextField getNameTextField() {
		return nameTextField;
	}

	public void setNameTextField(JTextField nameTextField) {
		this.nameTextField = nameTextField;
	}

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}
}
