package com.jecn.newapp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jecn.authentication.AuthenticationCookieUtils;
import com.jecn.epros.bean.authetication.AuthenticationRequest;
import com.jecn.epros.bean.authetication.TokenAnalysis;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.JecnSendTaskTOMainSystemFactory;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.service.AuthenticationService;

@Component
public class NewAppRequestDispatcher implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(NewAppRequestDispatcher.class);

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
			ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		// if (true) {
		if (JecnConfigTool.isNotEnableNewApp()) {
			chain.doFilter(request, response);
			return;
		}
		RequestUrlHandler urlHandler = new RequestUrlHandler(request, response);
		if (urlHandler.isLoginRequest()) {
			chain.doFilter(request, response);
			return;
		}
		if (urlHandler.isDingdingRequest()) {
			chain.doFilter(request, response);
			return;
		}
		// LOGGER.info("request uri:" + urlHandler.uri);
		//
		// LOGGER.info("request params:" +
		// urlHandler.uriParameterMap.toString());

		CredentialHandler credentialHandler = new CredentialHandler(request, response);
		credentialHandler.initialCredentials();

		if (!credentialHandler.isSessionCredentialValid()) {
			// make sure session is valid
			if (credentialHandler.isTokenCredentialValid()) {
				credentialHandler.initialSessionCredential();
			} else {
				response.sendRedirect(urlHandler.getLoginPageUrl());
				return;
			}
		}
		if (urlHandler.isMustToRedirectToNewApp()) {
			LOGGER.info("redirect uri:" + urlHandler.getRedirectUrlOfNewApp());
			// make sure token is valid
			LOGGER.info("credentialHandler.isTokenCredentialValid():" + credentialHandler.isTokenCredentialValid());
			if (!credentialHandler.isTokenCredentialValid()) {
				credentialHandler.initialTokenCredential();
			}
			response.sendRedirect(urlHandler.getRedirectUrlOfNewApp());
			return;
		} else {
			chain.doFilter(request, resp);
		}
	}

	@Override
	public void destroy() {
	}

	private static class RequestUrlHandler {
		private static final Set<String> loginUriList = new HashSet<String>(Arrays.asList("login.action",
				"loginMail.action", "taskLogin.action", "webservice/", "gugeGuide.htm", "zzsGuide.htm",
				"ChromeWIN7.exe", "ChromeXP.exe"));
		private String uri;
		private String context;
		private final Map<String, String> uriParameterMap;
		private String newAppUri;
		private HttpServletResponse response;
		private HttpServletRequest request;

		public RequestUrlHandler(HttpServletRequest request, HttpServletResponse response) {
			// context would like '/EprosServer3.0/'
			this.context = request.getContextPath() + "/";
			this.uri = request.getRequestURI().substring(this.context.length());
			this.uriParameterMap = getParameterMap(request);
			this.response = response;
			this.request = request;
		}

		public boolean isLoginRequest() {
			if (isBaseEprosUrl()) {
				return true;
			}
			for (String url : loginUriList) {
				if (this.uri.indexOf(url) != -1) {
					return true;
				}
			}
			if (uri.indexOf("exit.action") != -1) {
				// 清空cookie
				AuthenticationCookieUtils.clearCookie(response);
				return true;
			}
			return false;
		}
		
		public boolean isDingdingRequest(){
			String url = request.getRequestURL().toString();
			if(url.endsWith(".js")			//静态JS
				||url.contains("Ding")
				||url.contains("dd")){		//钉钉的请求
				return true;
			}
			return false;
		}

		public String getLoginPageUrl() {
			return JecnConfigTool.getNewAppContext() + "/page/login";
		}

		public boolean isMustToRedirectToNewApp() {
			adaptRequestUriIfNull();
			return newAppUri != NewAppUrlAdapter.OTHER;
		}

		public String getRedirectUrlOfNewApp() {
			adaptRequestUriIfNull();
			boolean app = "true".equals(uriParameterMap.get("isApp")) ? true : false;
			if (app) {
				return JecnContants.NEW_APP_CONTEXT_APP + newAppUri;
			}
			return JecnConfigTool.getNewAppContext() + newAppUri;
		}

		private void adaptRequestUriIfNull() {
			if (newAppUri == null) {
				newAppUri = NewAppUrlAdapter.adapt(uri, uriParameterMap, request);
			}
		}

		private static Map<String, String> getParameterMap(HttpServletRequest request) {
			Map<String, String> map = new HashMap<String, String>();
			Enumeration<String> enumeration = request.getParameterNames();
			while (enumeration.hasMoreElements()) {
				String paramName = enumeration.nextElement();
				String paramValue = request.getParameter(paramName);
				map.put(paramName, paramValue);
			}
			return map;
		}

		private static class NewAppUrlAdapter {
			private static Map<String, String> staticUriMap = new HashMap<String, String>();
			private static Map<String, String> dynamicUriMap = new HashMap<String, String>();
			private static final String OTHER = "Other";

			{
				// not support yet
				dynamicUriMap.put("loginTaskApprove.action?taskId=${mailTaskId}", "task/{id}");
				dynamicUriMap.put("processActive.action?activeId=${mailActiveId}", "");

				// wait to confirm
				dynamicUriMap.put("getFileInformation.action?fileId=${mailFileId}", "/files/{id}/info");
				dynamicUriMap
						.put(
								"updateSingleStateFromEmail.action?rtnlProposeId=${rtnlProposeId}&amp;handleFlag=${handleFlag}",
								"");
				dynamicUriMap.put("getProposeInfo.action?rtnlProposeId=${rtnlProposeId}", "");
			}

			static String adapt(String originalUri, Map<String, String> uriParameterMap, HttpServletRequest request) {
				if (originalUri.indexOf("index.jsp") != -1) {
					String index = uriParameterMap.get("index");
					if (StringUtils.isBlank(index)) {
						return dashboard();
					} else if (index.indexOf("process") != -1) {
						return "/processes/0/chart";
					} else if (index.indexOf("file") != -1) {
						return "/files/0/search?opts=tree";
					} else if (index.indexOf("processSearch") != -1) {
						return "/processes/0/search?opts=tree";
					} else if (index.indexOf("taskManage") != -1) {
						return "/tasks/search";
					}
				} else if (originalUri.indexOf("error.jsp") != -1) {
					return dashboard();
				} else if (originalUri.indexOf("login.jsp") != -1) {
					return "/page/login";
				} else if (originalUri.indexOf("guidePage.jsp") != -1) {
					return dashboard();
				} else if (originalUri.indexOf("taskToDo.jsp") != -1) {
					return "myTaskForInclude";
				}
				return adaptDynamicUrl(originalUri, uriParameterMap, request);
			}

			private static String adaptDynamicUrl(String originalUri, Map<String, String> uriParameterMap,
					HttpServletRequest request) {
				int p = "true".equals(uriParameterMap.get("isPub")) ? 1 : 0;
				boolean app = "true".equals(uriParameterMap.get("isApp")) ? true : false;
				if (originalUri.indexOf("process.action") != -1) {
					return MessageFormat.format("processes/{0}/chart?opts=tree&p=" + p, uriParameterMap.get("flowId"));
				} else if (originalUri.indexOf("loginPublicApprove.action") != -1) {
					return MessageFormat.format("processes/{0}/chart?p=" + p, uriParameterMap.get("flowId"));
				} else if (originalUri.indexOf("taskLookUp.action") != -1) {
					return MessageFormat.format("tasks/{0}/view?integration=true", uriParameterMap.get("taskId"));
				} else if (originalUri.indexOf("rule.action") != -1) {
					// http://192.168.0.181:8080/static-web/#/rules/774/ruleSearch?p=1&opts=tree;
					String type = uriParameterMap.get("type");
					if ("ruleDir".equals(type)) {// 制度-列表-更多 显示到制度清单
						return MessageFormat.format("rules/{0}/ruleSearch?opts=tree", "");
					} else if ("matrix".equals(type)) {// 财务矩阵地址
						return MessageFormat.format("rules/{0}/inventory?p=" + p + "&opts=tree", uriParameterMap
								.get("ruleId"));
					}
					return MessageFormat.format("rules/{0}/baseInfo?opts=tree&p=" + p, uriParameterMap.get("ruleId"));
				} else if (originalUri.indexOf("file.action") != -1) {
					return MessageFormat.format("files/{0}/info?opts=tree&p=" + p, uriParameterMap.get("fileId"));
				} else if (originalUri.indexOf("loginTaskApprove.action") != -1
						|| originalUri.indexOf("taskApprove.action") != -1
						|| originalUri.indexOf("taskLookUp.action") != -1) {// 任务审批转换

					IJecnBaseTaskService taskService = (IJecnBaseTaskService) ApplicationContextUtil.getContext()
							.getBean("flowTaskServiceImpl");
					try {
						JecnTaskBeanNew jecnTaskBeanNew = taskService.getTaskById(Long.valueOf(uriParameterMap
								.get("taskId")));
						if (jecnTaskBeanNew == null) {
							return "";
						}
						// 查阅任务
						boolean isView = "true".equals(uriParameterMap.get("isView")) ? true : false;
						WebLoginBean loginBean = getWebLoginBean(request);
						if (!isView && loginBean != null) {
							isView = taskService.hasApproveTask(jecnTaskBeanNew.getId(), loginBean.getJecnUser()
									.getPeopleId(), jecnTaskBeanNew.getState());
						}

						String ticket = uriParameterMap.get("ticket");
						if (ticket != null && isView) {// 蒙牛-OA中点击
							LOGGER.info("待办异常，转为已办！" + jecnTaskBeanNew.getTaskName());
							List<Long> cancelPeoples = new ArrayList<Long>();
							cancelPeoples.add(loginBean.getJecnUser().getPeopleId());
							JecnSendTaskTOMainSystemFactory.INSTANCE.sendInfoCancels(jecnTaskBeanNew, null, null,
									cancelPeoples, null);
						}
						if (!app) {
							if (isView) {
								return MessageFormat.format("tasks/{0}/view?integration=true&ticket=" + ticket,
										uriParameterMap.get("taskId"));
							}
							if (jecnTaskBeanNew.getTaskElseState() == 4) {
								return MessageFormat.format("tasks/{0}/resubmit?integration=true&ticket=" + ticket,
										uriParameterMap.get("taskId"));
							} else if (jecnTaskBeanNew.getState() == 10) {// 整理意见
								return MessageFormat.format("tasks/{0}/organize", uriParameterMap.get("taskId"));
							} else {
								return MessageFormat.format("tasks/{0}/approve?integration=true&ticket=" + ticket,
										uriParameterMap.get("taskId"));
							}
						} else {// App请求
							if (isView) {
								return "commission.html?taskOperation=TASK_VIEW&taskId="
										+ uriParameterMap.get("taskId") + "&isApp=true";
							}
							if (jecnTaskBeanNew.getTaskElseState() == 4) {
								return "commission.html?taskOperation=TASK_RESUBMIT&taskId="
										+ uriParameterMap.get("taskId") + "&isApp=true";
							} else {
								return "commission.html?taskOperation=TASK_APPROVE&taskId="
										+ uriParameterMap.get("taskId") + "&isApp=true";
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if (originalUri.indexOf("getFileInformation.action") != -1) {
					return MessageFormat.format("files/{0}/info?opts=tree", uriParameterMap.get("fileId"));
				}
				return OTHER;
			}

			static WebLoginBean getWebLoginBean(HttpServletRequest request) {
				Object loginBean = request.getSession().getAttribute(JecnContants.SESSION_KEY);
				if (loginBean != null) {
					return (WebLoginBean) loginBean;
				}
				return null;
			}

			static String dashboard() {
				return "";
			}
		}

		private boolean isBaseEprosUrl() {
			if (request.getRequestURL() == null) {
				return false;
			}
			// LOGGER.info(" URL = " + request.getRequestURL().toString());
			String[] strs = request.getRequestURL().toString().split(";");
			// LOGGER.info(" getBaseEprosUrl = " +
			// JecnConfigTool.getBaseEprosUrl());
			for (String string : strs) {
				if (string.equals(JecnConfigTool.getBaseEprosUrl())) {
					return true;
				}
			}
			return false;
		}
	}

	private static class CredentialHandler {

		private static IPersonService personService = (IPersonService) ApplicationContextUtil.getContext().getBean(
				"PersonServiceImpl");
		private static AuthenticationService authenticationService = (AuthenticationService) ApplicationContextUtil
				.getContext().getBean("authenticationServiceImpl");

		private HttpServletRequest request;
		private HttpServletResponse response;
		private HttpSession session;
		private String token;
		private TokenAnalysis tokenAnalysis;

		private CredentialHandler(HttpServletRequest request, HttpServletResponse response) {
			this.request = request;
			this.response = response;
		}

		public void initialCredentials() {
			this.session = request.getSession();
			this.token = AuthenticationCookieUtils.getAuthToken(request.getCookies());
			if (isTokenCookieExists()) {
				tokenAnalysis = authenticationService.analyze(token);
				if (isChangeLoginName(tokenAnalysis.getUsername())) {
					this.token = null;
				}
			}
		}

		private boolean isChangeLoginName(String tokenUserName) {
			WebLoginBean loginBean = getWebLoginBean();
			if (loginBean != null) {
				return tokenUserName != loginBean.getJecnUser().getLoginName();
			}
			return false;
		}

		public boolean isSessionCredentialValid() {
			return getWebLoginBean() != null;
		}

		private WebLoginBean getWebLoginBean() {
			Object loginBean = session.getAttribute(JecnContants.SESSION_KEY);
			if (loginBean != null) {
				return (WebLoginBean) loginBean;
			}
			return null;
		}

		public void initialSessionCredential() {
			String username = tokenAnalysis.getUsername();
			try {
				WebLoginBean webLoginBean = personService.getWebLoginBean(username, null, false);
				session.setAttribute(JecnContants.SESSION_KEY, webLoginBean);
			} catch (Exception e) {
				System.out.println(e);
				LOGGER.error("初始化sessionfalse", e);
			}
		}

		private boolean isTokenCookieExists() {
			return StringUtils.isNotBlank(token);
		}

		public boolean isTokenCredentialValid() {
			return isTokenCookieExists() && tokenAnalysis.isValid();
		}

		public void initialTokenCredential() {
			LOGGER.info("开始将token写入到cookie中");
			String username = getWebLoginBean().getJecnUser().getLoginName();
			String trueName = getWebLoginBean().getJecnUser().getTrueName();
			AuthenticationRequest authenticationRequest = new AuthenticationRequest();
			authenticationRequest.setUsername(username);
			token = authenticationService.authentication(authenticationRequest);
			response.addCookie(AuthenticationCookieUtils.createAuthTokenCookie(token, false, Integer
					.parseInt(JecnConfigTool.getItemValue(ConfigItemPartMapMark.cookieTime.toString())) * 60,
					JecnConfigTool.getCookieDomain()));

			try {
				response.addCookie(AuthenticationCookieUtils.createAuthLoginNameCookie(username, false, -1,
						JecnConfigTool.getCookieDomain()));
				response.addCookie(AuthenticationCookieUtils.createAuthTrueNameCookie(trueName, false, -1,
						JecnConfigTool.getCookieDomain()));
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("写入cookie错误", e);
			}
		}
	}
}
