package epros.draw.data.workflow;

/**
 * 
 * 
 * 创建流程图时前提条件对应的数据类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPartMapCreateData {
	/** 流程图名称 */
	private String name = null;
	/** 是否是横向标识 true：横向 false：纵向 */
	private boolean isHFlag = true;
	/** 预估角色 */
	private int roleCount = 0;
	/** 预估活动 */
	private int roundRectCount = 0;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHFlag() {
		return isHFlag;
	}

	public void setHFlag(boolean isHFlag) {
		this.isHFlag = isHFlag;
	}

	public int getRoleCount() {
		return roleCount;
	}

	public void setRoleCount(int roleCount) {
		this.roleCount = roleCount;
	}

	public int getRoundRectCount() {
		return roundRectCount;
	}

	public void setRoundRectCount(int roundRectCount) {
		this.roundRectCount = roundRectCount;
	}

}
