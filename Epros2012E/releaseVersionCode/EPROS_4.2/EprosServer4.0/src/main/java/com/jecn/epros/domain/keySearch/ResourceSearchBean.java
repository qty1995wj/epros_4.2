package com.jecn.epros.domain.keySearch;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;

import java.util.Date;

/**
 * 关键字搜索查询添加和返回结果数据对象
 *
 * @author ZXH
 */
public class ResourceSearchBean {
    /**
     * 搜索的文件ID
     */
    private Long relatedId;
    /**
     * 所属对应文件的类型：1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
     */
    private int relatedType;
    /**
     * 关联文件名称
     */
    private String realtedName;
    /**
     * 关联的文件父目录
     */
    private String relatedParentName;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateDate;

    @JsonIgnore
    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public int getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(int relatedType) {
        this.relatedType = relatedType;
    }

    @JsonIgnore
    public String getRealtedName() {
        return realtedName;
    }

    public void setRealtedName(String realtedName) {
        this.realtedName = realtedName;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LinkResource getLink() {
        return new LinkResource(relatedId, realtedName, getResourceType(), TreeNodeUtils.NodeType.search);
    }

    private ResourceType getResourceType() {
        ResourceType resourceType = null;
        // 1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;7:角色;8：活动
        switch (relatedType) {
            case 1:
                resourceType = ResourceType.PROCESS_MAP;
                break;
            case 2:
                resourceType = ResourceType.PROCESS;
                break;
            case 3:
                resourceType = ResourceType.FILE;
                break;
            case 4:
                resourceType = ResourceType.RULE;
                break;
            case 5:
                resourceType = ResourceType.STANDARD;
                break;
            case 6:
                resourceType = ResourceType.RISK;
                break;
        }
        return resourceType;
    }

    public String getRelatedParentName() {
        return relatedParentName;
    }

    public void setRelatedParentName(String relatedParentName) {
        this.relatedParentName = relatedParentName;
    }
}
