package com.jecn.epros.server.action.web.login.ad.powerlong;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.HttpRequest;

/**
 * 宝龙地产SSO
 * 
 * @author ZXH
 * 
 */
public class PowerLongLoginAction extends JecnAbstractADLoginAction {

	private static final String PARM = "_stk_";

	public PowerLongLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {

		String adLoginName = this.loginAction.getLoginName();

		if ("admin".equals(adLoginName)) {
			return loginAction.userLoginGen(true);
		}
		String result = this.loginAction.getRequest().getParameter(PARM);
		if (StringUtils.isBlank(result)) {
			loginAction.setInputUrl(JecnPowerLongAfterItem.getValue("SSO_URL"));
			return LoginAction.INPUT;
		}

		// 二次请求获取用户信息，参数为result
		String userInfo = HttpRequest.sendGet(JecnPowerLongAfterItem.getValue("SSO_VALIDATE"), "_stk_=" + result);
		log.info("SSO_VALIDATE userInfo  =" + userInfo);

		String code = JSONObject.fromObject(userInfo).getString("code");
		if ("200".equals(code)) {
			String returnObj = JSONObject.fromObject(userInfo).getString("data");
			if (!"null".equals(returnObj) && StringUtils.isNotBlank(returnObj)) {
				String loginName = JSONObject.fromObject(returnObj).getString("username");
				log.info("SSO IS SUCCESS !" + loginName);
				return this.loginByLoginName(loginName);
			}
		}
		loginAction.setInputUrl(JecnPowerLongAfterItem.getValue("SSO_URL"));
		return LoginAction.INPUT;
	}

}
