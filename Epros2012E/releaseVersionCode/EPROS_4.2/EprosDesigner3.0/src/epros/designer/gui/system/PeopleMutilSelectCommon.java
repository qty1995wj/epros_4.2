package epros.designer.gui.system;

import java.awt.Insets;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.popedom.person.PersonChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

public class PeopleMutilSelectCommon extends MutilSelectCommon {

	public PeopleMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			String labName, JecnDialog jecnDialog, boolean isRequest) {
		super(rows, infoPanel, insets, list, labName, jecnDialog, isRequest);
	}

	@Override
	protected JecnSelectChooseDialog getJecnSelectChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		return new PersonChooseDialog(list, jecnDialog);
	}

	// 科大讯飞 根据登录名 获取真实姓名
	protected void getIflytekPeopleName(List<JecnTreeBean> jecnTreeBean) {
		if (JecnConfigTool.isIflytekLogin()) {
			if (jecnTreeBean != null && !jecnTreeBean.isEmpty()) {
				for (JecnTreeBean bean : jecnTreeBean) {
					JecnUser jecnUserByPeopleId = ConnectionPool.getPersonAction().getJecnUserByPeopleId(bean.getId());
					String name = ConnectionPool.getPersonAction().getListUserSearchBean(
							jecnUserByPeopleId.getLoginName());
					if (StringUtils.isNotBlank(name)) {
						bean.setName(name);
					}
				}
			}
		}
	}

}
