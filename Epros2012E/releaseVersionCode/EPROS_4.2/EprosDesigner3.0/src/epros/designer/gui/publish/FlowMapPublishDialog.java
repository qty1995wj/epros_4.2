package epros.designer.gui.publish;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 流程地图发布
 * 
 * @author Administrator 2012-10-23
 */
public class FlowMapPublishDialog extends BasePublishDialog {

	public FlowMapPublishDialog(JecnTreeNode treeNode) {
		super(treeNode);
		if (TreeNodeType.processMap != treeBean.getTreeNodeType()) {// 不是流程地图节点
			log.error("FlowMapPublishDialog ！　treeBean.getTreeNodeType()　＝"
					+ treeBean.getTreeNodeType());
			return;
		}
	}

	/**
	 * 初始化变量
	 * 
	 */
	public void initialize() {
		type = 4;
		prfName = JecnProperties.getValue("processMapNameC");
		strTitle = JecnProperties.getValue("processMapPubI");
	}

}
