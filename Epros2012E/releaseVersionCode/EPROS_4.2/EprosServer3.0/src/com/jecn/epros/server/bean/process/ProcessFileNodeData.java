package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class ProcessFileNodeData implements Serializable {
	private JecnFlowStructureT flowStructureT;
	private FlowFileContent fileContent;
	private JecnFlowBasicInfoT basicInfoT;
	private Long createPeopleId;
	/** 流程属性 */
	private ProcessInfoBean infoBean;
	private List<JecnTreeBean> relatedStandards;
	private List<JecnTreeBean> relatedRules;
	/** 相关风险 */
	private List<JecnTreeBean> relatedRisks;
	/** 标准化文件 */
	private List<JecnTreeBean> relatedStandardizeds;

	public JecnFlowStructureT getFlowStructureT() {
		return flowStructureT;
	}

	public void setFlowStructureT(JecnFlowStructureT flowStructureT) {
		this.flowStructureT = flowStructureT;
	}

	public ProcessInfoBean getInfoBean() {
		return infoBean;
	}

	public void setInfoBean(ProcessInfoBean infoBean) {
		this.infoBean = infoBean;
	}

	public List<JecnTreeBean> getRelatedStandards() {
		return relatedStandards;
	}

	public void setRelatedStandards(List<JecnTreeBean> relatedStandards) {
		this.relatedStandards = relatedStandards;
	}

	public List<JecnTreeBean> getRelatedRules() {
		return relatedRules;
	}

	public void setRelatedRules(List<JecnTreeBean> relatedRules) {
		this.relatedRules = relatedRules;
	}

	public List<JecnTreeBean> getRelatedRisks() {
		return relatedRisks;
	}

	public void setRelatedRisks(List<JecnTreeBean> relatedRisks) {
		this.relatedRisks = relatedRisks;
	}

	public List<JecnTreeBean> getRelatedStandardizeds() {
		return relatedStandardizeds;
	}

	public void setRelatedStandardizeds(List<JecnTreeBean> relatedStandardizeds) {
		this.relatedStandardizeds = relatedStandardizeds;
	}

	public JecnFlowBasicInfoT getBasicInfoT() {
		return basicInfoT;
	}

	public void setBasicInfoT(JecnFlowBasicInfoT basicInfoT) {
		this.basicInfoT = basicInfoT;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public FlowFileContent getFileContent() {
		return fileContent;
	}

	public void setFileContent(FlowFileContent fileContent) {
		this.fileContent = fileContent;
	}
}
