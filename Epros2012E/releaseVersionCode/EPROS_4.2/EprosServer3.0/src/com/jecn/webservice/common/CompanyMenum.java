package com.jecn.webservice.common;

/**
 * 
 * @ClassName: CompanyMenum 
 * @Description: 流程同步枚举公共类
 * @author cheshaowei
 * @date 2015-4-14 上午10:54:36
 */
public class CompanyMenum {
    
	/**公司类型*/
	public enum CompanyType{
		bps,
		xinghan
	}
	
}
