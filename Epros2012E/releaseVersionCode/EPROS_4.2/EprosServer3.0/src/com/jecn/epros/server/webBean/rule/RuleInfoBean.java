package com.jecn.epros.server.webBean.rule;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

/**
 * @author yxw 2012-11-26
 * @description：制度搜索显示列表对应bean
 */
public class RuleInfoBean implements Serializable {
	/** 制度目录ID */
	private long ruleId;
	/** 制度名称 */
	private String ruleName;
	/** 制度编号 */
	private String ruleNum;
	/** 制度类别ID */
	private Long ruleTypeNameId;
	/** 制度类别 */
	private String ruleTypeName;
	/** 责任部门 */
	private long dutyOrgId;
	/** 责任部门 */
	private String dutyOrg;
	/** 密级 */
	private int secretLevel;
	/** 发布时间 */
	private String pubDate;
	/** 制度类型 0：目录1：制度模板文件2：制度文件*/
	private int isDir;
	/** 岗位查阅权限 */
	private List<JecnTreeBean> listPos;
	/** 岗位组查阅权限 */
	private List<JecnTreeBean> listPosGroup;
	/** 部门查阅权限 */
	private List<JecnTreeBean> listOrg;
	/** 制度文件ID */
	private Long fileId;
	/** 版本号*/
	private String versionId;
	/** 处于什么阶段 0是待建，1是审批，2是发布 */
	private int typeByData;
	/** 级别 */
	private int level;
	/** 制度父节点ID */
	private long rulePerId;
	/**制度责任人ID*/
	private long responsibleId;
	/**制度责任人名称*/
	private String responsibleName;
	
	public long getResponsibleId() {
		return responsibleId;
	}

	public void setResponsibleId(long responsibleId) {
		this.responsibleId = responsibleId;
	}

	public String getResponsibleName() {
		return responsibleName;
	}

	public void setResponsibleName(String responsibleName) {
		this.responsibleName = responsibleName;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setListPosGroup(List<JecnTreeBean> listPosGroup) {
		this.listPosGroup = listPosGroup;
	}

	public List<JecnTreeBean> getListPosGroup() {
		return listPosGroup;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public long getRuleId() {
		return ruleId;
	}

	public void setRuleId(long ruleId) {
		this.ruleId = ruleId;
	}

	public String getRuleNum() {
		return ruleNum;
	}

	public void setRuleNum(String ruleNum) {
		this.ruleNum = ruleNum;
	}

	public String getDutyOrg() {
		return dutyOrg;
	}

	public void setDutyOrg(String dutyOrg) {
		this.dutyOrg = dutyOrg;
	}

	public String getRuleTypeName() {
		return ruleTypeName;
	}

	public void setRuleTypeName(String ruleTypeName) {
		this.ruleTypeName = ruleTypeName;
	}

	public int getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(int secretLevel) {
		this.secretLevel = secretLevel;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public List<JecnTreeBean> getListPos() {
		return listPos;
	}

	public void setListPos(List<JecnTreeBean> listPos) {
		this.listPos = listPos;
	}

	public List<JecnTreeBean> getListOrg() {
		return listOrg;
	}

	public void setListOrg(List<JecnTreeBean> listOrg) {
		this.listOrg = listOrg;
	}

	public long getDutyOrgId() {
		return dutyOrgId;
	}

	public void setDutyOrgId(long dutyOrgId) {
		this.dutyOrgId = dutyOrgId;
	}

	public int getIsDir() {
		return isDir;
	}

	public void setIsDir(int isDir) {
		this.isDir = isDir;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public Long getRuleTypeNameId() {
		return ruleTypeNameId;
	}

	public void setRuleTypeNameId(Long ruleTypeNameId) {
		this.ruleTypeNameId = ruleTypeNameId;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public int getTypeByData() {
		return typeByData;
	}

	public void setTypeByData(int typeByData) {
		this.typeByData = typeByData;
	}

	public long getRulePerId() {
		return rulePerId;
	}

	public void setRulePerId(long rulePerId) {
		this.rulePerId = rulePerId;
	}

}
