package com.jecn.epros.server.bean.process.inout;

import java.io.Serializable;
import java.util.List;

public class JecnFlowInoutBase implements Serializable {
	private String id;
	private Long flowId;
	private Integer type;
	private String name;
	private String explain;
	private String pos;
	private Long sortId;
	private List<JecnFlowInoutPosT> flowInoutPosTs;

	public Long getSortId() {
		return sortId;
	}

	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public List<JecnFlowInoutPosT> getFlowInoutPosTs() {
		return flowInoutPosTs;
	}

	public void setFlowInoutPosTs(List<JecnFlowInoutPosT> flowInoutPosTs) {
		this.flowInoutPosTs = flowInoutPosTs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JecnFlowInoutBase) {
			JecnFlowInoutBase bean = (JecnFlowInoutBase) obj;
			if (this.id != null && bean.getId() != null) {
				if (!this.id.equals(bean.getId())) {
					return false;
				}
			}
			this.name = this.name == null ? "" : this.name;
			if (this.name != null && bean.getName() != null) {
				if (!this.name.equals(bean.getName())) {
					return false;
				}
			}
			this.explain = this.explain == null ? "" : this.explain;
			if (this.explain != null && bean.getExplain() != null) {
				if (!this.explain.equals(bean.getExplain())) {
					return false;
				}
			}
			this.pos = this.pos == null ? "" : this.pos;
			if (this.pos != null && bean.getPos() != null) {
				if (!this.pos.equals(bean.getPos())) {
					return false;
				}
			}
			if (this.sortId != null && bean.getSortId() != null) {
				if (!this.sortId.equals(bean.getSortId())) {
					return false;
				}
			} else {
				return false;
			}
			return true;
		}
		return super.equals(obj);
	}
}
