package com.jecn.epros.server.download.word;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.io.XMLWriter;

public class DocModeCommon {
	private static final Logger log = Logger.getLogger(DocModeCommon.class);

	/**
	 * 写人xml
	 * 
	 * @author fuzhh Apr 23, 2013
	 * @param document
	 * @param filePathName
	 *            文件路径名称
	 * @return
	 */
	public static boolean docXmlFile(Document document, String filePathName) {
		boolean flag = true;
		FileOutputStream fileOutPut = null;
		OutputStreamWriter outPut = null;
		XMLWriter writer = null;
		try {
			// 获取文件流
			fileOutPut = new FileOutputStream(filePathName);
			// 转换编码格式
			outPut = new OutputStreamWriter(fileOutPut, "UTF-8");
			// 输出成xml
			writer = new XMLWriter(outPut);
			writer.write(document);
		} catch (Exception ex) {
			flag = false;
			log.error("",ex);
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
				if (fileOutPut != null) {
					fileOutPut.close();
				}
				if (outPut != null) {
					outPut.close();
				}
			} catch (IOException e) {
				flag = false;
				log.error("写入xml 关闭流异常！", e);
			}
		}
		return flag;
	}

	/**
	 * 读取xml 放入 word中
	 * 
	 * @author fuzhh Apr 24, 2013
	 * @param xmlPath
	 *            xml 存放路径
	 * @param docPath
	 *            doc 存放路径
	 * @return
	 */
	public static void readXmlToDoc(String xmlPath, String docPath) {
		// Word.Application代表COM OLE编程标识，可查询MSDN得到
		// ActiveXComponent app = new ActiveXComponent("Word.Application");
		// // 设置Word不可见
		// app.setProperty("Visible", new Variant(false));
		// // 调用Application对象的Documents属性，获得Documents对象
		// Dispatch docs = app.getProperty("Documents").toDispatch();
		// // 打开xml 文件
		// Dispatch doc = Dispatch.call(docs, "Open", xmlPath, new
		// Variant(false),
		// new Variant(true)).toDispatch();
		//
		// Dispatch.call(doc,// 要转换的文档
		// "SaveAS", docPath, // 要保存的word文件名
		// 12);
		// // 关闭打开的Word文件
		// Dispatch.call(doc, "Close", false// 设置不保存改变
		// );
		// // 关闭Word应用程序
		// app.invoke("Quit", new Variant[] {});
	}
}
