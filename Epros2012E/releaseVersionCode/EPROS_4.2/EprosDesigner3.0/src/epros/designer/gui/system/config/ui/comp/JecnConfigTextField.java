package epros.designer.gui.system.config.ui.comp;

import javax.swing.JTextField;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

/**
 * 
 * 输入框
 * 
 * @author Administrator
 *
 */
public class JecnConfigTextField extends JTextField {

	/** 数据对象 */
	private JecnConfigItemBean itemBean = null;

	public JecnConfigItemBean getItemBean() {
		return itemBean;
	}

	public void setItemBean(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
	}

}
