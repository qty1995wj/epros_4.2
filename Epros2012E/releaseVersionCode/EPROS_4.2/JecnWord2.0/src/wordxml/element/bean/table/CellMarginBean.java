package wordxml.element.bean.table;

public class CellMarginBean {

	/**** 距离顶部的距离 单位 厘米 *******/
	private float top;
	/**** 距离左边的距离 单位 厘米 *******/
	private float left;
	/**** 距离底部的距离 单位 厘米 *******/
	private float bottom;
	/**** 距离右边的距离 单位 厘米 *******/
	private float right;

	/**
	 * 表格内边距 （单位：厘米）
	 * 
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 */
	public CellMarginBean(float top, float left, float bottom, float right) {
		this.top = top;
		this.left = left;
		this.bottom = bottom;
		this.right = right;
	}

	public float getTop() {
		return top;
	}

	public void setTop(float top) {
		this.top = top;
	}

	public float getLeft() {
		return left;
	}

	public void setLeft(float left) {
		this.left = left;
	}

	public float getBottom() {
		return bottom;
	}

	public void setBottom(float bottom) {
		this.bottom = bottom;
	}

	public float getRight() {
		return right;
	}

	public void setRight(float right) {
		this.right = right;
	}

}
