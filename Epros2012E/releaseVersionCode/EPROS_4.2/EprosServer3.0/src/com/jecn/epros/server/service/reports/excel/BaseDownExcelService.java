package com.jecn.epros.server.service.reports.excel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.util.JecnUtil;

/**
 * 
 * 统计excel下载父类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class BaseDownExcelService {
	protected Log log = LogFactory.getLog(this.getClass());
	/** 模板路径 */
	protected String mouldPath = null;
	/** 开始填充数据行数 */
	protected int startDataRow = 2;

	/**
	 * 
	 * 获取数据
	 * 
	 * @return byte[]
	 */
	public byte[] getExcelFile() {
		if (mouldPath == null) {
			log.info("BaseDownExcelService类模板文件路径为空。mouldPath=" + mouldPath);
			return null;
		}

		// 输出流
		ByteArrayOutputStream out = null;

		Workbook wbookMould = null;
		WritableWorkbook wbookData = null;

		try {
			// 模板文件
			File mouldFile = new File(mouldPath);
			if (!mouldFile.exists() || !mouldFile.isFile()) {// 判断文件是否存在
				log.info("BaseDownExcelService：给定路径对应文件不存在。mouldPath="
						+ mouldPath);
				return null;
			}

			out = new ByteArrayOutputStream();

			// 工作薄
			wbookMould = Workbook.getWorkbook(mouldFile);
			wbookData = Workbook.createWorkbook(out, wbookMould);
			
			try{//提前释放资源
				wbookMould.close();
				wbookMould=null;
			}catch (Exception e) {
				log.error("", e);
			}

			// 编辑excel内容
			boolean ref = editExcleBefore(wbookData);
			if (!ref) {
				return null;
			}

			// 写入服务器本地
			wbookData.write();

			// 关闭编辑工作薄
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			return out.toByteArray();

		} catch (Exception ex) {
			log.error("BaseDownExcelService类getExcelFile方法：", ex);
			return null;
			
		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("BaseDownExcelService类getExcelFile方法：", e);
				}
			}
			if (wbookMould != null) {
				try {
					wbookMould.close();
				} catch (Exception e) {
					log.error("", e);
				}
			}

			if (out != null) { // 输出流
				try {
					out.close();
				} catch (IOException e) {
					log.error("BaseDownExcelService类getExcelFile方法：", e);
				}
			}
		}
	}

	/**
	 * 
	 * 获取前缀
	 * 
	 * @return String
	 */
	protected String getPrefixPath() {
		URL url = this.getClass().getResource("");
		String path = "";
		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			log.error("BaseDownExcelService类getPrefixPath方法：", e);
			path = url.getPath().replaceAll("%20", " ");
		}
		return path;
	}

	/**
	 * 
	 * 获取样式
	 * 
	 * @return WritableCellFormat
	 */
	protected WritableCellFormat getWritableCellFormat() {
		// 样式
		WritableFont wfc = new WritableFont(WritableFont.ARIAL, 11,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.RED);
		return new WritableCellFormat(wfc);
	}

	/**
	 * 
	 * 获取给定时间格式的时间字符串
	 * 
	 * @return
	 */
	protected String getDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(new Date());
	}

	/**
	 * 
	 * 居中
	 * 
	 * @return
	 */
	protected Alignment getAlignment() {
		return Alignment.CENTRE;
	}

	/**
	 * 
	 * 添加第一行数据
	 * 
	 * @param dataSheet
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	protected void addFirstRowData(WritableSheet dataSheet)
			throws RowsExceededException, WriteException {
		if (dataSheet == null) {
			return;
		}
		// 第一行
		// 字体样式
		WritableCellFormat wcfFC = getWritableCellFormat();
		// 当前时间（yyyy-MM-dd）
		String date = getDate();
		// 日期(
		String prefixFirst = JecnUtil.getValue("prefixFirst");
		// )统计：
		String suffixFirst = JecnUtil.getValue("suffixFirst");
		String content = prefixFirst + date + suffixFirst;
		dataSheet.addCell(new Label(0, 0, content, wcfFC));
	}

	/**
	 * 
	 * 编辑excel内容
	 * 
	 * @param wbookData
	 *            WritableWorkbook
	 * @return boolean true：编辑成功；false：编辑失败
	 */
	private boolean editExcleBefore(WritableWorkbook wbookData) {
		// 获取sheet
		WritableSheet[] sheetArry = wbookData.getSheets();
		if (sheetArry.length <= 0) {
			return false;
		}
		// sheet
		WritableSheet dataSheet = sheetArry[0];
		if (dataSheet == null) {
			return false;
		}

		return editExcle(dataSheet);
	}

	/**
	 * 
	 * 编辑excel内容
	 * 
	 * @param dataSheet
	 *            WritableSheet
	 * @return boolean true：编辑成功；false：编辑失败
	 */
	protected abstract boolean editExcle(WritableSheet dataSheet);
}
