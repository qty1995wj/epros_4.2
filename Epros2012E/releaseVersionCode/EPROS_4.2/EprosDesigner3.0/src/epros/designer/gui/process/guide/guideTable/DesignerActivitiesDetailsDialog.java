package epros.designer.gui.process.guide.guideTable;

import java.util.Vector;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.process.activitiesProperty.ActivitiesDetailsDialog;
import epros.designer.gui.process.guide.DesignerProcessOperationIntrusDialog;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/*******************************************************************************
 * 活动明细
 * 
 * @author 2012-07-27
 * 
 */
public class DesignerActivitiesDetailsDialog extends ActivitiesDetailsDialog {

	/** 活动说明 */
	private DesignerProcessOperationIntrusDialog processOperationIntrusDialog;
	/** 活动数据 */
	private JecnActivityShowBean activityShowBean;

	public DesignerActivitiesDetailsDialog(JecnActivityShowBean activityShowBean,
			DesignerProcessOperationIntrusDialog processOperationIntrusDialog) {
		super(activityShowBean.getActiveFigure().getFlowElementData(), activityShowBean.getActiveFigure());
		this.processOperationIntrusDialog = processOperationIntrusDialog;
		this.activityShowBean = activityShowBean;
	}

	/***************************************************************************
	 * 取消
	 */
	protected void cancelPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int tipAction = dialogCloseBeforeMsgTipAction();
			if (tipAction == 2) {// 信息提示：true：需要执行关闭 false：不要执行关闭
				// this.setVisible(false);
				this.okButPerformed();
			} else if (tipAction == 1) {
				this.setVisible(false);
			}
		}
	}

	public void okButPerformed() {
		super.okButPerformed();
		// 问题区域
		for (int index = processOperationIntrusDialog.getQuestionAreaTable().getModel().getRowCount() - 1; index >= 0; index--) {
			String id = processOperationIntrusDialog.getQuestionAreaTable().getValueAt(index, 0).toString();
			if (DrawCommon.isNullOrEmtryTrim(id)) {
				continue;
			}
			if (id.equals(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID())) {
				processOperationIntrusDialog.getQuestionAreaTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getActivityNum(), index, 1);
				processOperationIntrusDialog.getQuestionAreaTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getFigureText(), index, 2);
				processOperationIntrusDialog.getQuestionAreaTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow(), index, 3);
				processOperationIntrusDialog.getQuestionAreaTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShowAndControl(), index, 4);
			}
		}
		// 关键控制点
		for (int index = processOperationIntrusDialog.getKeyContrPointTable().getModel().getRowCount() - 1; index >= 0; index--) {
			String id = processOperationIntrusDialog.getKeyContrPointTable().getValueAt(index, 0).toString();
			if (DrawCommon.isNullOrEmtryTrim(id)) {
				continue;
			}
			if (id.equals(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID())) {
				processOperationIntrusDialog.getKeyContrPointTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getActivityNum(), index, 1);
				processOperationIntrusDialog.getKeyContrPointTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getFigureText(), index, 2);
				processOperationIntrusDialog.getKeyContrPointTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow(), index, 3);
				processOperationIntrusDialog.getKeyContrPointTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShowAndControl(), index, 4);
			}
		}
		// 关键成功因素
		for (int index = processOperationIntrusDialog.getKeySuccessTable().getModel().getRowCount() - 1; index >= 0; index--) {
			String id = processOperationIntrusDialog.getKeySuccessTable().getValueAt(index, 0).toString();
			if (DrawCommon.isNullOrEmtryTrim(id)) {
				continue;
			}
			if (id.equals(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID())) {
				processOperationIntrusDialog.getKeySuccessTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getActivityNum(), index, 1);
				processOperationIntrusDialog.getKeySuccessTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getFigureText(), index, 2);
				processOperationIntrusDialog.getKeySuccessTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow(), index, 3);
				processOperationIntrusDialog.getKeySuccessTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShowAndControl(), index, 4);
			}
		}
		// 活动明细
		for (int index = processOperationIntrusDialog.getDrawActiveDescTable().getModel().getRowCount() - 1; index >= 0; index--) {
			String id = processOperationIntrusDialog.getDrawActiveDescTable().getValueAt(index, 0).toString();
			if (DrawCommon.isNullOrEmtryTrim(id)) {
				continue;
			}
			if (id.equals(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID())) {
				processOperationIntrusDialog.getDrawActiveDescTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getActivityNum(), index, 1);
				processOperationIntrusDialog.getDrawActiveDescTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getFigureText(), index, 3);
				processOperationIntrusDialog.getDrawActiveDescTable().setValueAt(
						activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow(), index, 4);
				if (JecnConstants.loginBean.getOtherLoginType() == 4) {
					// 对应内控矩阵风险点
					processOperationIntrusDialog.getDrawActiveDescTable().setValueAt(
							activityShowBean.getActiveFigure().getFlowElementData().getInnerControlRisk(), index, 5);

					// 对应标准条款
					processOperationIntrusDialog.getDrawActiveDescTable().setValueAt(
							activityShowBean.getActiveFigure().getFlowElementData().getStandardConditions(), index, 6);
				} else {
					// 输入
					String fileImport = "";
					for (JecnActivityFileT activityFileT : activityShowBean.getActiveFigure().getFlowElementData()
							.getListJecnActivityFileT()) {
						if (activityFileT.getFileType() == 0) {
							fileImport += activityFileT.getFileName() + "/";
						}
					}
					if (!"".equals(fileImport)) {
						fileImport = fileImport.substring(0, fileImport.length() - 1);
					}
					processOperationIntrusDialog.getDrawActiveDescTable().setValueAt(fileImport, index, 5);

					// 输出
					String fileOutport = "";
					for (JecnModeFileT modeFileT : activityShowBean.getActiveFigure().getFlowElementData()
							.getListModeFileT()) {
						fileOutport += modeFileT.getModeName() + "/";
					}
					if (!"".equals(fileOutport)) {
						fileOutport = fileOutport.substring(0, fileOutport.length() - 1);
					}
					processOperationIntrusDialog.getDrawActiveDescTable().setValueAt(fileOutport, index, 6);
				}
			}
		}

		// 相关流程
		for (int index = processOperationIntrusDialog.getFlowFileTable().getModel().getRowCount() - 1; index >= 0; index--) {
			Vector<Vector<String>> flowFileContent = new Vector<Vector<String>>();
			// 输出
			for (JecnModeFileT modeFileT : activityShowBean.getActiveFigure().getFlowElementData().getListModeFileT()) {
				Vector<String> flowFileContentStr = new Vector<String>();
				// 文件ID
				flowFileContentStr.add(String.valueOf(modeFileT.getFileMId()));
				// 文件编号
				flowFileContentStr.add(modeFileT.getFileNumber());
				// 文件名称
				flowFileContentStr.add(modeFileT.getModeName());
				// 类型输出
				flowFileContentStr.add("output");
				// 图形的UIID
				flowFileContentStr.add(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID());
				flowFileContent.add(flowFileContentStr);
			}
			// 输入 和 操作规范
			for (JecnActivityFileT activityFileT : activityShowBean.getActiveFigure().getFlowElementData()
					.getListJecnActivityFileT()) {
				// 判断为输入
				if (activityFileT.getFileType() != null && activityFileT.getFileType().intValue() == 0) {
					Vector<String> flowFileContentStr = new Vector<String>();
					// 文件ID
					flowFileContentStr.add(String.valueOf(activityFileT.getFileSId()));
					// 文件编号
					flowFileContentStr.add(activityFileT.getFileNumber());
					// 文件名称
					flowFileContentStr.add(activityFileT.getFileName());
					// 类型输入
					flowFileContentStr.add("input");
					// 图形的UIID
					flowFileContentStr
							.add(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID());
					flowFileContent.add(flowFileContentStr);
				}
			}
			((FlowFileTable) processOperationIntrusDialog.getFlowFileTable()).getTableModel(flowFileContent);
			((FlowFileTable) processOperationIntrusDialog.getFlowFileTable()).setTableModel();
		}

		// 相关流程
		for (int index = processOperationIntrusDialog.getPracticesTable().getModel().getRowCount() - 1; index >= 0; index--) {
			Vector<Vector<String>> practicesContent = new Vector<Vector<String>>();
			// 输入 和 操作规范
			for (JecnActivityFileT activityFileT : activityShowBean.getActiveFigure().getFlowElementData()
					.getListJecnActivityFileT()) {
				// 判断为操作规范
				if (activityFileT.getFileType() != null && activityFileT.getFileType().intValue() == 1) {
					Vector<String> practicesContentStr = new Vector<String>();
					// 文件ID
					practicesContentStr.add(String.valueOf(activityFileT.getFileSId()));
					// 文件编号
					practicesContentStr.add(activityFileT.getFileNumber());
					// 文件名称
					practicesContentStr.add(activityFileT.getFileName());
					// 类型操作规范
					practicesContentStr.add("practicesContent");
					// 图形的UIID
					practicesContentStr.add(activityShowBean.getActiveFigure().getFlowElementData()
							.getFlowElementUUID());

					practicesContent.add(practicesContentStr);
				}
			}
			((FlowFileTable) processOperationIntrusDialog.getPracticesTable()).getTableModel(practicesContent);
			((FlowFileTable) processOperationIntrusDialog.getPracticesTable()).setTableModel();
		}
		// 面板保存
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
	}
}
