package epros.designer.gui.integration.risk;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * 风险点属性 2013-11-05
 * 
 */
public class RiskPropertyDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(RiskPropertyDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(600, 470);

	/** 风险基本信息面板 */
	private JScrollPane baseInfoPanel = new JScrollPane();

	/** 控制目标面板 */
	private JScrollPane controlTargetPanel = new JScrollPane();

	/** JTabbedPane */
	private JTabbedPane tabPanel = new JTabbedPane();

	/** 按钮面板 */
	private JPanel buttonPanel = new JecnPanel(600, 30);

	/*** 验证提示 */
	private JLabel verfyLab = new JLabel();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	// 基本信息
	private EditRiskBaseInfoPanel riskBaseInfoPanel = null;
	// 控制目标
	private EditRiskControlTargetPanel riskControlTargetPanel = null;

	/** 风险数据集合 **/
	private JecnRisk jecnRisk = new JecnRisk();
	/** 是否存在 */
	private boolean isExists = true;
	/** 是否有操作权限 true:有操作权限 false:没有 默认:false */
	private boolean isOperation = false;

	public boolean isExists() {
		return isExists;
	}

	public RiskPropertyDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		// 设置窗体大小
		this.setSize(670, 500);
		this.setTitle(JecnProperties.getValue("riskPointProperty"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);

		// 设置面板背景颜色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		baseInfoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		controlTargetPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 添加ui
		tabPanel.setUI(new JecnWindowsTabbedPaneUI());
		// 获取风险基本信息、控制目标数据
		try {
			jecnRisk = ConnectionPool.getJecnRiskAction().getJecnRiskById(selectNode.getJecnTreeBean().getId());
		} catch (Exception e1) {
			log.error("RiskPropertyDialog RiskPropertyDialog is error", e1);
		}

		if (jecnRisk == null) {
			isExists = false;
			// 风险已删除！
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("riskIsDelete"));
			return;
		}
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

		verfyLab.setForeground(Color.red);
		this.setModal(true);
		initLayout();
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelPerformed();
				return false;
			}
		});
	}

	/***
	 * 布局
	 */
	private void initLayout() {
		Container contentPane = this.getContentPane();
		tabPanel.add(baseInfoPanel, 0);
		tabPanel.setTitleAt(0, JecnProperties.getValue("actBaseInfo"));
		tabPanel.add(controlTargetPanel, 1);
		tabPanel.setTitleAt(1, JecnProperties.getValue("controlTarget"));

		// 风险基本信息
		riskBaseInfoPanel = new EditRiskBaseInfoPanel(selectNode, jTree, jecnRisk);
		riskBaseInfoPanel.setVisible(true);
		baseInfoPanel.setViewportView(riskBaseInfoPanel);

		// 控制目标
		riskControlTargetPanel = new EditRiskControlTargetPanel(selectNode, jTree, jecnRisk);
		riskControlTargetPanel.setVisible(true);
		controlTargetPanel.setViewportView(riskControlTargetPanel);
		// 按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		contentPane.add(buttonPanel, BorderLayout.SOUTH);
		contentPane.add(tabPanel, BorderLayout.CENTER);
	}

	/***
	 * 确定
	 */
	private void okButPerformed() {
		if (!isUpdate()) {
			this.setVisible(false);
			return;
		}
		/****** ======================风险基本信息===================== */
		// JecnRisk jecnRisk = new JecnRisk();
		// jecnRisk.setIsDir(1);
		// jecnRisk.setSort(JecnTreeCommon.getMaxSort(selectNode));
		verfyLab.setText("");
		riskControlTargetPanel.controlTargetVerfy.setText("");
		// 判断控制目标是否保存
		String textAreaTargetStr = riskControlTargetPanel.controlTargetArea.getText();
		// 判断是添加还是编辑以及前后两次数据是否相等如果相等不执行修改
		if (riskControlTargetPanel.isAddOrUpdate == 1) {
			// 获取当前选中行，用来判断控制目标与数据库对应数据是否不同，为了点击确定时弹出对话框显示：“是否保存”
			int selectControTargetNum = riskControlTargetPanel.controlTargetTable.getSelectedRow();
			if (selectControTargetNum != -1) {
				String tableTargetStr = riskControlTargetPanel.controlTargetTable.getModel().getValueAt(
						selectControTargetNum, 2).toString();
				if (!tableTargetStr.equals(textAreaTargetStr)) {
					if (textAreaTargetStr == null || "".equals(textAreaTargetStr)) {
						riskControlTargetPanel.controlTargetVerfy.setText(JecnProperties.getValue("controlTarget")
								+ JecnProperties.getValue("isNotEmpty"));
						return;
					} else if (DrawCommon.checkNoteLength(textAreaTargetStr)) {// 验证输入内容不能大于600个汉字或1200字符
						riskControlTargetPanel.controlTargetVerfy.setText(JecnProperties.getValue("controlTarget")
								+ JecnProperties.getValue("lengthNotOut"));
						return;
					}
					int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
							.getValue("isSaveEditControlTarget"), null, JecnOptionPane.YES_NO_OPTION,
							JecnOptionPane.INFORMATION_MESSAGE);
					if (option == JecnOptionPane.YES_OPTION) {
						riskControlTargetPanel.controlTargetTable.getModel().setValueAt(textAreaTargetStr,
								selectControTargetNum, 2);
					}
				}
			}
		} else {
			if (textAreaTargetStr != null && !"".equals(textAreaTargetStr)) {
				// 验证输入内容不能大于600个汉字或1200字符
				if (DrawCommon.checkNoteLength(textAreaTargetStr)) {
					riskControlTargetPanel.controlTargetVerfy.setText(JecnProperties.getValue("controlTarget")
							+ JecnProperties.getValue("lengthNotOut"));
					return;
				}
				int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isSaveAddControlTarget"),
						null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.YES_OPTION) {
					Vector<String> v = new Vector<String>();
					v.add("");
					v.add(String.valueOf(riskControlTargetPanel.controlTargetTable.getModel().getRowCount() + 1));
					v.add(riskControlTargetPanel.controlTargetArea.getText());
					riskControlTargetPanel.controlTargetTable.addRow(v);
					riskControlTargetPanel.controlTargetArea.setText("");
				}
			}

		}

		// 风险编号
		String riskNum = riskBaseInfoPanel.riskCodeFiled.getText();
		// 风险编号非空验证
		if (DrawCommon.isNullOrEmtryTrim(riskNum)) {
			verfyLab.setText(JecnProperties.getValue("riskNum") + JecnProperties.getValue("isNotEmpty"));
			return;
		}
		// 编号长度验证
		else if (DrawCommon.checkNameMaxLength(riskNum)) {
			verfyLab.setText(JecnProperties.getValue("riskNum") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		} else {
			// 验证风险编号是否重名
			try {
				int isRepeat = ConnectionPool.getJecnRiskAction().riskNumIsRepeat(riskNum, jecnRisk.getId());
				if (isRepeat == 0) {
					jecnRisk.setRiskCode(riskNum);
				} else {
					verfyLab.setText(JecnProperties.getValue("riskNum") + JecnProperties.getValue("repeatXist"));
					return;
				}

			} catch (Exception e) {
				log.error("RiskPropertyDialog okButPerformed is error", e);
			}
		}
		// 风险等级类别 1：高， 2：中， 3：低
		if (riskBaseInfoPanel.gradeCombox.getSelectedItem() != null
				&& !"".equals(riskBaseInfoPanel.gradeCombox.getSelectedItem())) {
			jecnRisk.setGrade(riskBaseInfoPanel.gradeCombox.getSelectedIndex());
		}
		// 风险点描述
		String riskDes = riskBaseInfoPanel.riskDescArea.getText();
		// 风险点描述非空验证
		if (DrawCommon.isNullOrEmtryTrim(riskDes)) {
			verfyLab.setText(JecnProperties.getValue("riskPointDes") + JecnProperties.getValue("isNotEmpty"));
			return;
		}
		// 风险点描述长度验证
		if (riskDes != null && !"".equals(riskDes)) {
			if (DrawCommon.checkNoteLength(riskDes)) {
				verfyLab.setText(JecnProperties.getValue("riskPointDes") + JecnProperties.getValue("lengthNotOut"));
				return;
			} else {
				jecnRisk.setRiskName(riskDes);
			}
		}
		// 标准化控制
		String standardControl = riskBaseInfoPanel.standdardControlArea.getText();
		// 标准化控制长度验证
		if (standardControl != null && !"".equals(standardControl)) {
			if (DrawCommon.checkNoteLength(standardControl)) {
				verfyLab.setText(JecnProperties.getValue("standardControl") + JecnProperties.getValue("lengthNotOut"));
				return;
			} else {
				jecnRisk.setStandardControl(standardControl);
			}
		}
		// 内控指引知识库

		// 更新人
		jecnRisk.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 更新时间
		jecnRisk.setUpdateTime(new Date());
		// 风险类别
		if (riskBaseInfoPanel.getRiskType() != null) {
			jecnRisk.setRiskType(Integer.valueOf(riskBaseInfoPanel.getRiskType().getKey().toString()));
		}

		try {
			// 控制目标
			jecnRisk.setListControlTarget(getResultsControlTarget());
			// 内控指引知识库
			jecnRisk.setListControlGuide(getResultControlGuide());

			ConnectionPool.getJecnRiskAction().EditJecnRiskProperty(jecnRisk);
			// 向树节点添加风险点节点
			JecnTreeBean jecnTreeBean = selectNode.getJecnTreeBean();
			jecnTreeBean.setName(riskDes);
			jecnTreeBean.setContent(riskDes);
			if (riskDes.length() > 8) {
				riskDes = riskDes.substring(0, 8) + "...";
			}
			if (selectNode != null && jTree != null) {
				JecnTreeCommon.reNameNode(jTree, selectNode, riskNum + " " + riskDes);
			}
		} catch (Exception e) {
			log.error("RiskPropertyDialog okButPerformed is error", e);
			return;
		}
		this.setVisible(false);
	}

	private List<JecnControlGuide> getResultControlGuide() {
		// 获取控制目标表格中的数据
		Vector<Vector<String>> dataControlGuide = ((DefaultTableModel) riskBaseInfoPanel.controlGuideTable.getModel())
				.getDataVector();
		List<JecnControlGuide> listControlGuide = new ArrayList<JecnControlGuide>();
		if (dataControlGuide.size() > 0) {
			for (Vector<String> str : dataControlGuide) {
				JecnControlGuide jecnControlGuide = new JecnControlGuide();
				jecnControlGuide.setId(Long.valueOf(str.get(0).toString()));
				// jecnControlGuide.setParentId(Long.valueOf(str.get(1).toString()));
				jecnControlGuide.setName(str.get(1));
				listControlGuide.add(jecnControlGuide);
			}
		}
		return listControlGuide;
	}

	private List<JecnControlTarget> getResultsControlTarget() {
		// 获取控制目标表格中的数据
		Vector<Vector<String>> data = ((DefaultTableModel) riskControlTargetPanel.controlTargetTable.getModel())
				.getDataVector();
		List<JecnControlTarget> listControlTarget = new ArrayList<JecnControlTarget>();
		if (data.size() > 0) {
			for (Vector<String> str : data) {
				if (str.get(0) == null || "".equals(str.get(0))) {
					JecnControlTarget jecnControlTarget = new JecnControlTarget();
					jecnControlTarget.setDescription(str.get(2));
					// 更新人
					jecnControlTarget.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
					// 更新时间
					jecnControlTarget.setUpdateTime(new Date());
					// 关联风险点主键ID
					jecnControlTarget.setRiskId(selectNode.getJecnTreeBean().getId());
					jecnControlTarget.setSort(Integer.parseInt(str.get(1)));
					// 创建人
					jecnControlTarget.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
					// 创建时间
					jecnControlTarget.setCreateTime(new Date());
					listControlTarget.add(jecnControlTarget);
				} else {
					for (JecnControlTarget jecnControlTarget : jecnRisk.getListControlTarget()) {
						// if(str.get(0) != null ||
						// !"".equals(str.get(0))){smallike
						if (jecnControlTarget.getId().equals(Long.valueOf(str.get(0).toString()))) {
							JecnControlTarget controlTarget = new JecnControlTarget();
							// 复制属性
							try {
								PropertyUtils.copyProperties(controlTarget, jecnControlTarget);
							} catch (Exception e) {
								e.printStackTrace();
								log.error("", e);
								JecnOptionPane.showMessageDialog(null, "System is error!");
								return null;
							}
							controlTarget.setDescription(str.get(2));
							// 更新人
							controlTarget.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
							// 更新时间
							controlTarget.setUpdateTime(new Date());
							controlTarget.setSort(Integer.parseInt(str.get(1)));
							listControlTarget.add(controlTarget);
							break;
						}
						// }
					}
				}

			}
		}
		return listControlTarget;
	}

	private boolean isUpdate() {
		String riskNum = riskBaseInfoPanel.riskCodeFiled.getText();
		String riskDes = riskBaseInfoPanel.riskDescArea.getText();
		String standardControl = riskBaseInfoPanel.standdardControlArea.getText();
		int indexGrade = riskBaseInfoPanel.gradeCombox.getSelectedIndex();

		// 控制目标
		List<JecnControlTarget> listControlTarget = getResultsControlTarget();

		// 内控指引知识库
		List<JecnControlGuide> listControlGuide = getResultControlGuide();
		if (listControlTarget.size() != jecnRisk.getListControlTarget().size()) {
			return true;
		}
		if (listControlGuide.size() != jecnRisk.getListControlGuide().size()) {
			return true;
		}
		// 內控是否存在更新、添加
		for (JecnControlGuide newControlGuide : listControlGuide) {
			boolean isExist = false;
			for (JecnControlGuide oldControlGuide : jecnRisk.getListControlGuide()) {
				if (newControlGuide.getId().equals(oldControlGuide.getId())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}

		// 控制目标是否存在更新、添加
		for (JecnControlTarget newControlTarget : listControlTarget) {
			if (newControlTarget.getId() == null) {
				return true;
			}
			boolean isExist = false;
			for (JecnControlTarget oldControlTarget : jecnRisk.getListControlTarget()) {
				if (newControlTarget.getId().equals(oldControlTarget.getId())
						&& newControlTarget.getDescription().equals(oldControlTarget.getDescription()) && newControlTarget.getSort().equals(oldControlTarget.getSort())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		if (!jecnRisk.getRiskCode().equals(riskNum)) {
			return true;
		}
		if (!jecnRisk.getRiskName().equals(riskDes)) {
			return true;
		}
		if (jecnRisk.getStandardControl() == null) {
			jecnRisk.setStandardControl("");
		}
		if (!jecnRisk.getStandardControl().equals(standardControl)) {
			return true;
		}
		if (jecnRisk.getGrade() != indexGrade) {
			return true;
		}
		if (riskBaseInfoPanel.getRiskType() != null && riskBaseInfoPanel.getRiskType().isUpdate()) {
			return true;
		}
		return false;
	}

	/***
	 * 取消按钮
	 */
	private void cancelPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	/**
	 * 点击详情，隐藏按钮
	 * 
	 */
	public void hiddenComs() {
		riskBaseInfoPanel.hiddenComps();
		this.okBut.setVisible(false);
		riskControlTargetPanel.hiddenComps();
	}

	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}

}
