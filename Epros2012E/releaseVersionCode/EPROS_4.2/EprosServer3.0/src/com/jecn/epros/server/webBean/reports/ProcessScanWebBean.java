package com.jecn.epros.server.webBean.reports;

import java.io.Serializable;

public class ProcessScanWebBean implements Serializable {
	/** 流程ID */
	private long flowId;
	/** 流程名称 */
	private String flowName;
	/** 流程编号 */
	private String flowIdInput;// 流程编号
	/** 责任部门Id /流程架构id*/
	private Long id;
	/** 责任部门 /流程架构id*/
	private String name;
	/** 流程监护人的部门id*/
	private Long guardianOrgId;
	/** 流程监护人的部门name*/
	private String guardianOrgName;
	/** 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人) */
	private int typeResPeople;// 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
	/** 流程责任人Id */
	private Long resPeopleId;// 流程责任人Id
	/** 流程责任人 */
	private String resPeopleName;// 流程责任人
	/** 发布时间 */
	private String pubDate;
	/** 需要审视的日期  计划*/
	private String needFinishDate;
	/** 需审视流程发布时间*/
	private String needScanPubDate;
	/** 当前发布时间*/
	private String curScanPubDate;
	/** 版本号 */
	private String versionId;
	/** 是否为流程 */
	private int isFlow;
	/** 流程监护人ID */
	private Long guardianId;
	/** 流程监护人名称 */
	private String guardianName;
	/** 流程拟稿人 */
	private String draftPerson;
	/** 流程有效期 */
	private int expiry;
	/** 是否及时优化 0否 ,1是 ,2空 */
	private int optimization;
	/** 责任部门下按时完成的审视的流程数量*/
	private int scanCount=0;
	/** 责任部门下未审视的流程数量*/
	private int notScanCount=0;
	/** 责任部门下所有需要审视的流程数量*/
	private int allScanCount=0;
	/** 流程审视比例  审视完成的除以所有的*/
	private int scanPercent=0;
	/** 流程责任部门 name*/
	private String dutyName;
	/** 变更说明**/
	private String modifyExplain;
	
	private Long trueOrgId;
	private String trueOrgName;
	
	public Long getTrueOrgId() {
		return trueOrgId;
	}
	public void setTrueOrgId(Long trueOrgId) {
		this.trueOrgId = trueOrgId;
	}
	public String getTrueOrgName() {
		return trueOrgName;
	}
	public void setTrueOrgName(String trueOrgName) {
		this.trueOrgName = trueOrgName;
	}
	public String getModifyExplain() {
		return modifyExplain;
	}
	public void setModifyExplain(String modifyExplain) {
		this.modifyExplain = modifyExplain;
	}
	public String getDutyName() {
		return dutyName;
	}
	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}
	public long getFlowId() {
		return flowId;
	}
	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getFlowIdInput() {
		return flowIdInput;
	}
	public void setFlowIdInput(String flowIdInput) {
		this.flowIdInput = flowIdInput;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getGuardianOrgId() {
		return guardianOrgId;
	}
	public void setGuardianOrgId(Long guardianOrgId) {
		this.guardianOrgId = guardianOrgId;
	}
	public String getGuardianOrgName() {
		return guardianOrgName;
	}
	public void setGuardianOrgName(String guardianOrgName) {
		this.guardianOrgName = guardianOrgName;
	}
	public int getTypeResPeople() {
		return typeResPeople;
	}
	public void setTypeResPeople(int typeResPeople) {
		this.typeResPeople = typeResPeople;
	}
	public Long getResPeopleId() {
		return resPeopleId;
	}
	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}
	public String getResPeopleName() {
		return resPeopleName;
	}
	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public String getNeedFinishDate() {
		return needFinishDate;
	}
	public void setNeedFinishDate(String needFinishDate) {
		this.needFinishDate = needFinishDate;
	}
	public String getNeedScanPubDate() {
		return needScanPubDate;
	}
	public void setNeedScanPubDate(String needScanPubDate) {
		this.needScanPubDate = needScanPubDate;
	}
	public String getCurScanPubDate() {
		return curScanPubDate;
	}
	public void setCurScanPubDate(String curScanPubDate) {
		this.curScanPubDate = curScanPubDate;
	}
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public int getIsFlow() {
		return isFlow;
	}
	public void setIsFlow(int isFlow) {
		this.isFlow = isFlow;
	}
	public Long getGuardianId() {
		return guardianId;
	}
	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}
	public String getGuardianName() {
		return guardianName;
	}
	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}
	public String getDraftPerson() {
		return draftPerson;
	}
	public void setDraftPerson(String draftPerson) {
		this.draftPerson = draftPerson;
	}
	public int getExpiry() {
		return expiry;
	}
	public void setExpiry(int expiry) {
		this.expiry = expiry;
	}
	public int getOptimization() {
		return optimization;
	}
	public void setOptimization(int optimization) {
		this.optimization = optimization;
	}
	public int getScanCount() {
		return scanCount;
	}
	public void setScanCount(int scanCount) {
		this.scanCount = scanCount;
	}
	public int getNotScanCount() {
		return notScanCount;
	}
	public void setNotScanCount(int notScanCount) {
		this.notScanCount = notScanCount;
	}
	public int getAllScanCount() {
		return allScanCount;
	}
	public void setAllScanCount(int allScanCount) {
		this.allScanCount = allScanCount;
	}
	public int getScanPercent() {
		return scanPercent;
	}
	public void setScanPercent(int scanPercent) {
		this.scanPercent = scanPercent;
	}
	
	
	
}