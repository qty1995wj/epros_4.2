package com.jecn.epros.server.download.wordxml;

import java.util.ArrayList;
import java.util.List;

import wordxml.element.dom.page.Sect;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.RuleDownloadBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.util.JecnUtil;

public class RuleWordPlex {

	private RuleDownloadBean ruleDownloadBean;
	private RuleModel model;
	private ProcessFileProperty docProperty;

	public RuleWordPlex(RuleModel model, RuleDownloadBean downloadBean) {
		this.ruleDownloadBean = downloadBean;
		this.model = model;
		this.docProperty = getDocProperty();
	}

	private Table createTableItem(RuleItem item, float[] width, List<String[]> rowData) {
		return model.createTableItem(item, width, rowData);
	}

	private ProcessFileProperty getDocProperty() {
		return model.getDocProperty();
	}

	private void createTitle(RuleItem item) {
		model.createTitle(item);
	}

	public void createHistoryTable(Sect sect) {
		JecnTaskHistoryNew history = ruleDownloadBean.getLastHistory();
		if (history != null) {
			List<JecnTaskHistoryFollow> flows = history.getListJecnTaskHistoryFollow();
			List<String[]> rowData = new ArrayList<String[]>();
			for (JecnTaskHistoryFollow flow : flows) {
				rowData.add(new String[] { JecnUtil.nullToEmpty(flow.getAppellation()),
						JecnUtil.nullToEmpty(flow.getName()),
						JecnUtil.nullToEmpty(JecnUtil.DateToStr(flow.getApprovalTime(), "yyyy-MM-dd")) });
			}
			createTable(sect, rowData, new float[] { 5.82F, 5.82F, 5.82F });
		}
	}

	public Table createTable(Sect sect, List<String[]> rowData, float[] width) {
		return createTableItem(new RuleItem(sect), width, rowData);
	}

}
