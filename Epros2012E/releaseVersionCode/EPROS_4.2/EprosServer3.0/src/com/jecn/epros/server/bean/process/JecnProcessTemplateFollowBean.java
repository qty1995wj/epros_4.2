package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

public class JecnProcessTemplateFollowBean implements Serializable {
	private long id; //主键
	private String imageName;//图片名称
	private String name;//名称
	private long modeId;//关联的模型主表ID
	private Blob imageContent;//图片内容
	private byte[] bytes;
	private Long createPeopleId;//创建人peopleId
	private Date createDate;// 创建时间
	private Date updateDate;// 更新时间
	private Long updatePeopleId;// 更新人ID
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Blob getImageContent() {
		return imageContent;
	}
	public void setImageContent(Blob imageContent) {
		this.imageContent = imageContent;
	}
	public byte[] getBytes() {
		return bytes;
	}
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	public Long getCreatePeopleId() {
		return createPeopleId;
	}
	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public long getModeId() {
		return modeId;
	}
	public void setModeId(long modeId) {
		this.modeId = modeId;
	}
	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}
}
