
package com.jecn.webservice.zhongsh.rule.download;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DeleteSystemDocumentBySystemIdResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deleteSystemDocumentBySystemIdResult"
})
@XmlRootElement(name = "DeleteSystemDocumentBySystemIdResponse")
public class DeleteSystemDocumentBySystemIdResponse {

    @XmlElement(name = "DeleteSystemDocumentBySystemIdResult")
    protected boolean deleteSystemDocumentBySystemIdResult;

    /**
     * 获取deleteSystemDocumentBySystemIdResult属性的值。
     * 
     */
    public boolean isDeleteSystemDocumentBySystemIdResult() {
        return deleteSystemDocumentBySystemIdResult;
    }

    /**
     * 设置deleteSystemDocumentBySystemIdResult属性的值。
     * 
     */
    public void setDeleteSystemDocumentBySystemIdResult(boolean value) {
        this.deleteSystemDocumentBySystemIdResult = value;
    }

}
