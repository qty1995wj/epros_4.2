package epros.designer.gui.choose;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class DeleteableTable extends JTable {
	protected List<JecnTreeBean> data = null;
	protected List<JecnTreeBean> dataBack = new ArrayList<JecnTreeBean>();

	public DeleteableTable(List<JecnTreeBean> data) {
		this.data = data;
		if (data != null) {
			this.dataBack.addAll(data);
		}
		initListener();
	}

	private void initListener() {
		final JTable t = this;
		final List<JecnTreeBean> data = this.data;
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					// 获得行位置
					Integer row = ((JTable) e.getSource()).rowAtPoint(e.getPoint());
					Integer col = ((JTable) e.getSource()).columnAtPoint(e.getPoint());
					Set<Integer> escape = removeEscapeColumns();
					if (escape.contains(col)) {
						return;
					}
					DefaultTableModel model = ((DefaultTableModel) t.getModel());
					String v = model.getValueAt(row, getIdColumn()).toString();
					for (JecnTreeBean x : data) {
						if (v.equals(x.getId().toString())) {
							// 如果是文件类型选择打开
							// 支持批量选择但是不支持
							// 批量打开文件或者删除文件
							if (x.getTreeNodeType() == TreeNodeType.file) {
								openFile(x);
								return;
							} else {
								model.removeRow(row);
								data.remove(x);
								return;
							}
						}
					}
				}
			}
		});
	}

	private void openFile(JecnTreeBean node) {
		FileOpenBean openFile;
		try {
			openFile = ConnectionPool.getFileAction().openFile(node.getId());

			if (openFile == null) {
				JecnOptionPane
						.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("fileHasDel"));
				return;
			}
			JecnUtil.openFile(openFile.getName(), openFile.getFileByte());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	protected int getIdColumn() {
		return 0;
	}

	/**
	 * 双击删除要排除的列
	 * 
	 * @return
	 */
	protected Set<Integer> removeEscapeColumns() {
		HashSet<Integer> set = new HashSet<Integer>();
		set.add(2);
		return set;
	}
}
