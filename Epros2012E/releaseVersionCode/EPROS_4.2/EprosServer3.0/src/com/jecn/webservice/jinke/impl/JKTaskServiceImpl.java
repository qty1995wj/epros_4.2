package com.jecn.webservice.jinke.impl;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.external.TaskParam;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.webservice.jinke.IJKTaskService;

@WebService
public class JKTaskServiceImpl implements IJKTaskService {
	private Logger log = Logger.getLogger(IJKTaskService.class);

	@Override
	public MessageResult handleTaskExternal(TaskParam param) {
		IJecnBaseTaskService service = (IJecnBaseTaskService) ApplicationContextUtil.getContext().getBean("flowTaskServiceImpl");
		return service.handleTaskExternal(param);
	}

}
