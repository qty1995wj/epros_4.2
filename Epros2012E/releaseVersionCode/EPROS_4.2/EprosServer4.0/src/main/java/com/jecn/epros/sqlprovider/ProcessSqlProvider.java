package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.process.FlowBasicInfoT;
import com.jecn.epros.domain.process.SearchBean;
import com.jecn.epros.domain.process.kpi.KPIValuesBean;
import com.jecn.epros.security.JecnContants;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.*;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class ProcessSqlProvider extends BaseSqlProvider {

    String sqlCommonResult = " select DISTINCT t.flow_id,t.flow_name,t.isflow,"
            + " case when pub.flow_id is null then 0 else 1 end as count,"
            + " t.flow_id_input,t.pre_flow_id, t.sort_id ";

    String sqlCommonFrom = " from jecn_flow_structure t "
            + " left join jecn_flow_structure pub on t.flow_id=pub.pre_flow_id and pub.del_state=0";

    /**
     * 流程关联的风险
     */
    public static String getFlowRrelatedRisk(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "	select distinct jr.*,flows.flow_id from flows " +
                "     inner join JECN_FLOW_STRUCTURE_IMAGE" + pub + " jfsi on jfsi.FLOW_ID=flows.flow_id" +
                "     inner join JECN_CONTROL_POINT jcp on jcp.ACTIVE_ID=jfsi.FIGURE_ID" +
                "     inner join jecn_risk jr on jr.id=jcp.risk_id";
        return sql;
    }

    public String findItSystemByActiveId(Map<String, Object> map) {
        String strPub = map.get("isPub").toString();
        String sql = "SELECT OT.ID id," +
                "       OT.PROCESS_ID processId,"
                + "       OT.ACTIVE_ID activeId,"
                + "  OT.TOOL_ID toolId,"
                + "       OT.ONLINE_TIME onLineTime,"
                + "       ST.FLOW_SUSTAIN_TOOL_DESCRIBE sysName,OT.INFORMATION_DESCRIPTION informationDescription"
                + "  FROM JECN_ACTIVE_ONLINE" + strPub + " OT, JECN_FLOW_SUSTAIN_TOOL ST"
                + " WHERE OT.TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID" + "   AND OT.PROCESS_ID =#{flowId}";
        return sql;
    }

    /**
     * 任务查询文控Id
     *
     * @param map
     * @return
     */
    public static String findHistoryId(Map<String, Object> map) {
        String sql = "SELECT T.ID" +
                "  FROM JECN_TASK_HISTORY_NEW T" +
                " WHERE T.RELATE_ID = #{relateId}" +
                "   AND T.TYPE = #{type}" +
                "   AND T.TASK_ID =#{taskId}";
        return sql;
    }

    public static String getProcessArchitectureBaseSql(Map<String, Object> params) {
        String sql = "";
        String pub = Utils.getPubByParams(params);
        Long historyId = (Long) params.get("historyId");
        if (historyId != null && historyId > 0) {
            sql = findHistoryProcessArchitectureBaseSql(params);
        } else {
            sql = findProcessArchitectureBaseSql(params);
        }
        return sql;
    }

    public static String findProcessArchitectureBaseSql(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String versionSql = "";
        if (StringUtils.isBlank(pub)) {
            versionSql = "JTH.VERSION_ID,";
        } else {
            versionSql = "TBN.HISTORY_VISTION VERSION_ID,";
        }
        String sql = "SELECT JFS.FLOW_ID,JFS.FLOW_NAME,JFS.FLOW_ID_INPUT,JFS.T_LEVEL" +
                "     ,JMF.FLOW_DESCRIPTION,JMF.FLOW_AIM,JMF.FLOW_INPUT,JMF.FLOW_OUTPUT,JMF.FLOW_START,JMF.FLOW_END,JMF.FLOW_KPI" +
                "     ,PFLOW.FLOW_ID AS P_FLOW_ID,PFLOW.FLOW_NAME AS P_FLOW_NAME" +
                "     ,JFO.ORG_ID,JFO.ORG_NAME" +
                "     ,CASE WHEN JMF.TYPE_RES_PEOPLE=0 THEN JU." + IfytekCommon.getUserField() +
                "      WHEN JMF.TYPE_RES_PEOPLE=1 THEN JFOI.FIGURE_TEXT END AS RES_PEOPLE," +
                versionSql +
                "       JFS.PUB_TIME ,JUF." + IfytekCommon.getUserField() + " COMMISSIONER,JFS.KEYWORD,JMF.SUPPLIER,JMF.CUSTOMER,JMF.RISK_OPPORTUNITY" +
                "     FROM JECN_FLOW_STRUCTURE" + pub + " JFS" +
                "     LEFT JOIN JECN_MAIN_FLOW" + pub + " JMF ON JMF.FLOW_ID=JFS.FLOW_ID" +
                "     LEFT JOIN JECN_USER JU ON JMF.TYPE_RES_PEOPLE=0 AND JU.PEOPLE_ID = JMF.RES_PEOPLE_ID " +
                "     LEFT JOIN JECN_USER JUF  ON JUF.PEOPLE_ID = JFS.COMMISSIONER_ID " +
                "     LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JMF.TYPE_RES_PEOPLE=1 AND JFOI.FIGURE_ID = JMF.RES_PEOPLE_ID" +
                "     LEFT JOIN JECN_FLOW_RELATED_ORG" + pub + " JFRO ON JFRO.FLOW_ID = JFS.FLOW_ID" +
                "     LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JFRO.ORG_ID AND JFO.DEL_STATE=0" +
                "     LEFT JOIN JECN_FLOW_STRUCTURE" + pub + " PFLOW ON PFLOW.FLOW_ID=JFS.PRE_FLOW_ID" +
                "    LEFT JOIN JECN_TASK_HISTORY_NEW JTH" +
                "    ON JTH.ID = JFS.HISTORY_ID" +
                "     LEFT JOIN JECN_TASK_BEAN_NEW TBN" +
                "    ON TBN.R_ID = JFS.FLOW_ID AND TBN.TASK_TYPE IN (0,4) AND TBN.STATE <>5    and tbn.is_lock = 1" +
                "     WHERE JFS.FLOW_ID=#{id}";
        return sql;
    }

    public static String findHistoryProcessArchitectureBaseSql(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql =
                "SELECT JFS.FLOW_ID," +
                        "       JFS.FLOW_NAME," +
                        "       JFS.FLOW_ID_INPUT," +
                        "       JFS.T_LEVEL," +
                        "       JMF.FLOW_DESCRIPTION," +
                        "       JMF.FLOW_AIM," +
                        "       JMF.FLOW_INPUT," +
                        "       JMF.FLOW_OUTPUT," +
                        "       JMF.FLOW_START," +
                        "       JMF.FLOW_END," +
                        "       JMF.FLOW_KPI," +
                        "       PFLOW.FLOW_ID AS P_FLOW_ID," +
                        "       PFLOW.FLOW_NAME AS P_FLOW_NAME," +
                        "       JFO.ORG_ID," +
                        "       JFO.ORG_NAME," +
                        "       CASE" +
                        "         WHEN JMF.TYPE_RES_PEOPLE = 0 THEN" +
                        "          JU." + IfytekCommon.getUserField() +
                        "         WHEN JMF.TYPE_RES_PEOPLE = 1 THEN" +
                        "          JFOI.FIGURE_TEXT" +
                        "       END AS RES_PEOPLE," +
                        "       JTH.VERSION_ID," +
                        "       JFS.PUB_TIME ,JUF." + IfytekCommon.getUserField() + " COMMISSIONER,JFS.KEYWORD,JMF.SUPPLIER,JMF.CUSTOMER,JMF.RISK_OPPORTUNITY" +
                        "  FROM JECN_FLOW_STRUCTURE_H JFS" +
                        "  LEFT JOIN JECN_MAIN_FLOW_H JMF" +
                        "    ON JMF.FLOW_ID = JFS.FLOW_ID" +
                        "   AND JMF.HISTORY_ID = JFS.HISTORY_ID" +
                        "  LEFT JOIN JECN_USER JU" +
                        "    ON JMF.TYPE_RES_PEOPLE = 0" +
                        "   AND JU.PEOPLE_ID = JMF.RES_PEOPLE_ID " +
                        "  LEFT JOIN JECN_USER JUF  ON JUF.PEOPLE_ID = JFS.COMMISSIONER_ID " +
                        "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI" +
                        "    ON JMF.TYPE_RES_PEOPLE = 1" +
                        "   AND JFOI.FIGURE_ID = JMF.RES_PEOPLE_ID" +
                        "  LEFT JOIN JECN_FLOW_RELATED_ORG_H JFRO" +
                        "    ON JFRO.FLOW_ID = JFS.FLOW_ID" +
                        "   AND JFRO.HISTORY_ID = JFS.HISTORY_ID" +
                        "  LEFT JOIN JECN_FLOW_ORG JFO" +
                        "    ON JFO.ORG_ID = JFRO.ORG_ID" +
                        "   AND JFO.DEL_STATE = 0" +
                        "  LEFT JOIN JECN_FLOW_STRUCTURE_H PFLOW" +
                        "    ON PFLOW.FLOW_ID = JFS.PRE_FLOW_ID" +
                        "   AND PFLOW.HISTORY_ID = JFS.HISTORY_ID" +
                        "  LEFT JOIN JECN_TASK_HISTORY_NEW JTH" +
                        "    ON JTH.ID = JFS.HISTORY_ID" +
                        " WHERE JFS.FLOW_ID = #{id}" +
                        "   and jfs.history_id = #{historyId}";
        return sql;
    }


    public static String getFlowRelatedRule(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "	select distinct jr.id,jr.rule_name,flows.flow_id from flows " +
                "	inner join FLOW_RELATED_CRITERION" + pub + " frc on flows.flow_id=frc.flow_id" +
                "	inner join jecn_rule" + pub + " jr on jr.id=frc.CRITERION_CLASS_ID and jr.del_state=0";
        return sql;
    }

    //文控
    public static String getFlowHistoryRelatedRule(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "select distinct jr.id,jr.rule_name,flows.flow_id from flows" +
                "    inner join FLOW_RELATED_CRITERION_H frc" +
                "    on flows.flow_id=frc.flow_id and flows.history_id = frc.history_id" +
                "    inner join jecn_rule jr" +
                "    on jr.id=frc.CRITERION_CLASS_ID ";
        return sql;
    }

    public static String getFlowRelatedStandard(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "     select DISTINCT jcc.CRITERION_CLASS_ID,JCC.CRITERION_CLASS_NAME,flows.flow_id,jf.version_id from flows" +
                "     inner join JECN_FLOW_STANDARD" + pub + " jfs on flows.flow_id=jfs.FLOW_ID" +
                "     inner join JECN_CRITERION_CLASSES jcc on jcc.CRITERION_CLASS_ID=jfs.CRITERION_CLASS_ID left join jecn_file_t jf" +
                "      on jf.file_id = jcc.related_id" +
                "     UNION" +
                "     select distinct jcc.CRITERION_CLASS_ID,JCC.CRITERION_CLASS_NAME,flows.flow_id,jf.version_id from flows" +
                "     inner join JECN_FLOW_STRUCTURE_IMAGE" + pub + "  jfsi on flows.flow_id=jfsi.FLOW_ID" +
                "     inner join JECN_ACTIVE_STANDARD" + pub + "  JAS on jfsi.FIGURE_ID=JAS.ACTIVE_ID" +
                "     inner join JECN_CRITERION_CLASSES jcc on jcc.CRITERION_CLASS_ID=jas.STANDARD_ID left join jecn_file_t jf" +
                "      on jf.file_id = jcc.related_id";
        return sql;
    }

    public static String getFlowHistoryRelatedStandard(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "select DISTINCT jcc.CRITERION_CLASS_ID,JCC.CRITERION_CLASS_NAME,flows.flow_id from flows" +
                " inner join JECN_STANDARD_HISTORY jfs on flows.flow_id=jfs.RELATE_ID AND FLOWS.HISTORY_ID = JFS.HISTORY_ID" +
                " inner join JECN_CRITERION_CLASSES jcc on jcc.CRITERION_CLASS_ID=jfs.STANDARD_ID" +
                "      UNION" +
                " select distinct  jcc.CRITERION_CLASS_ID,JCC.CRITERION_CLASS_NAME,flows.flow_id from flows" +
                " inner join JECN_FLOW_STRUCTURE_IMAGE_H  jfsi on flows.flow_id=jfsi.FLOW_ID AND FLOWS.HISTORY_ID =JFSI.HISTORY_ID" +
                " inner join JECN_ACTIVE_STANDARD_H  JAS on jfsi.FIGURE_ID=JAS.ACTIVE_ID  AND FLOWS.HISTORY_ID = JAS.HISTORY_ID" +
                " inner join JECN_CRITERION_CLASSES jcc on jcc.CRITERION_CLASS_ID=jas.STANDARD_ID";

        return sql;
    }


    public static String getFlowRelatedTemplate(Map<String, Object> params) {
        String sql = "";
        if (!ConfigUtils.useNewInout()) {
            String pub = Utils.getPubByParams(params);
            sql = "     select DISTINCT fi.id file_id,fi.file_name,jfs.flow_id" +
                    "                 from flows jfs" +
                    "                inner join JECN_FLOW_STRUCTURE_IMAGE" + pub + " jfsi" +
                    "                   on jfs.flow_id = jfsi.FLOW_ID" +
                    "                  and jfs.del_state = 0" +
                    "                inner join JECN_ACTIVITY_FILE" + pub + " jaf" +
                    "                   on jaf.FIGURE_ID = jfsi.FIGURE_ID" +
                    "                  and jaf.FILE_TYPE = 0" +
                    "                inner join jecn_file" + pub + " jf" +
                    "                   on jaf.FILE_S_ID = jf.file_id   and jf.del_state = 0" +
                    "                inner join file_content fi     " +
                    "                 on fi.id = jf.version_id      " +
                    "                 " +
                    "               union" +
                    "     select DISTINCT fi.id file_id,fi.file_name,jfs.flow_id" +
                    "                 from flows jfs" +
                    "                inner join JECN_FLOW_STRUCTURE_IMAGE" + pub + " jfsi" +
                    "                   on jfs.flow_id = jfsi.FLOW_ID" +
                    "                  and jfs.del_state = 0" +
                    "                inner join JECN_MODE_FILE" + pub + " jmf" +
                    "                   on jmf.FIGURE_ID = jfsi.FIGURE_ID" +
                    "                inner join jecn_file" + pub + " jf" +
                    "                   on jmf.FILE_M_ID = jf.file_id     and jf.del_state = 0" +
                    "                inner join file_content fi     " +
                    "                 on fi.id = jf.version_id      ";
        } else {
            sql = getFlowRelatedTermTemplate(params);
        }
        return sql;
    }

    //新版输入输出
    public static String getFlowRelatedTermTemplate(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = " select DISTINCT ft.version_id file_id, ft.file_name,inout.flow_id" +
                "  from JECN_FIGURE_IN_OUT" + pub + "  inout" +
                " inner join flows jf" +
                "    on inout.flow_id = jf.flow_id" +
                "   and jf.del_state = 0" +
                " inner join jecn_file" + pub + " ft" +
                "    on inout.file_id = ft.file_id" +
                "   and ft.del_state = 0" +
                " union " +
                "select  DISTINCT ft.version_id file_id, ft.file_name,inout.flow_id" +
                "  from JECN_FIGURE_IN_OUT" + pub + " inout" +
                " inner join JECN_FIGURE_IN_OUT_SAMPLE" + pub + " smp" +
                "    on inout.id = smp.in_out_id" +
                " inner join flows jf" +
                "    on inout.flow_id = jf.flow_id" +
                "   and jf.del_state = 0" +
                " inner join jecn_file" + pub + " ft" +
                "    on ft.file_id = smp.file_id" +
                "   and ft.del_state = 0";
        return sql;
    }

    //文控
    public static String getFlowHistoryRelatedTemplate(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "";
        if (!ConfigUtils.useNewInout()) {
            sql = "            select DISTINCT JF.ID FILE_ID,jf.file_name,jfs.flow_id" +
                    "                            from flows jfs" +
                    "                           inner join JECN_FLOW_STRUCTURE_IMAGE_H jfsi" +
                    "                              on jfs.flow_id = jfsi.FLOW_ID  and jfsi.history_id = jfs.history_id" +
                    "                             and jfs.del_state = 0" +
                    "                           inner join JECN_ACTIVITY_FILE_H jaf" +
                    "                              on jaf.FIGURE_ID = jfsi.FIGURE_ID  and jfs.history_id = jaf.history_id" +
                    "                             and jaf.FILE_TYPE = 0" +
                    "                           inner join FILE_CONTENT jf" +
                    "                              on jaf.FILE_S_ID = jf.ID  " +
                    "                          union" +
                    "                select DISTINCT JF.ID FILE_ID,jf.file_name,jfs.flow_id" +
                    "                            from flows jfs" +
                    "                           inner join JECN_FLOW_STRUCTURE_IMAGE_H jfsi" +
                    "                              on jfs.flow_id = jfsi.FLOW_ID and  jfs.history_id = jfsi.history_id" +
                    "                             and jfs.del_state = 0" +
                    "                           inner join JECN_MODE_FILE_H jmf" +
                    "                              on jmf.FIGURE_ID = jfsi.FIGURE_ID  and jfs.history_id = jmf.history_id" +
                    "                           inner join FILE_CONTENT jf" +
                    "                              on jmf.FILE_M_ID = jf.ID   ";
        } else {
            sql = getFlowHistoryRelatedTermTemplate(params);
        }
        return sql;
    }

    //文控
    public static String getFlowHistoryRelatedTermTemplate(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = " select  DISTINCT ft.version_id file_id, ft.file_name,inout.flow_id" +
                "  from JECN_FIGURE_IN_OUT_H  inout" +
                " inner join flows jf" +
                "    on inout.flow_id = jf.flow_id  and jf.HISTORY_ID = inout.history_id" +
                "   and jf.del_state = 0" +
                " inner join jecn_file ft" +
                "    on inout.file_id = ft.file_id" +
                "   and ft.del_state = 0" +
                " union " +
                " select  DISTINCT ft.version_id file_id, ft.file_name,inout.flow_id" +
                "  from JECN_FIGURE_IN_OUT_H inout" +
                " inner join JECN_FIGURE_IN_OUT_SAMPLE_H smp" +
                "    on inout.id = smp.in_out_id and inout.HISTORY_ID = smp.history_id" +
                " inner join flows jf" +
                "    on inout.flow_id = jf.flow_id  and jf.HISTORY_ID = inout.history_id" +
                "   and jf.del_state = 0" +
                " inner join jecn_file ft" +
                "    on ft.file_id = smp.file_id" +
                "   and ft.del_state = 0 ";
        return sql;
    }

    public static String getFlowRelatedOperation(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "     select DISTINCT fi.id file_id,jf.file_name,jfs.flow_id" +
                "                 from flows jfs" +
                "                inner join JECN_FLOW_STRUCTURE_IMAGE" + pub + " jfsi" +
                "                   on jfs.flow_id = jfsi.FLOW_ID" +
                "                  and jfs.del_state = 0" +
                "                inner join JECN_ACTIVITY_FILE" + pub + " jaf" +
                "                   on jaf.FIGURE_ID = jfsi.FIGURE_ID" +
                "                  and jaf.FILE_TYPE = 1" +
                "                inner join jecn_file" + pub + " jf" +
                "                   on jaf.FILE_S_ID = jf.file_id          and jf.del_state = 0" +
                "                inner join file_content fi     " +
                "                 on fi.id = jf.version_id      ";
        return sql;
    }

    //文控
    public static String getFlowHistoryRelatedOperation(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "select DISTINCT JF.ID FILE_ID,jf.file_name,jfs.flow_id" +
                "                            from flows jfs" +
                "                           inner join JECN_FLOW_STRUCTURE_IMAGE_H jfsi" +
                "                              on jfs.flow_id = jfsi.FLOW_ID AND JFS.HISTORY_ID = JFSI.HISTORY_ID" +
                "                             and jfs.del_state = 0" +
                "                           inner join JECN_ACTIVITY_FILE_H  jaf" +
                "                              on jaf.FIGURE_ID = jfsi.FIGURE_ID AND  JFS.HISTORY_ID = JAF.HISTORY_ID" +
                "                             and jaf.FILE_TYPE = 1" +
                "                           inner join FILE_CONTENT jf" +
                "                              on jaf.FILE_S_ID = jf.ID ";
        return sql;
    }

    /**
     * 流程标准化文件
     *
     * @param params
     * @return
     */
    public static String getFlowRelatedStandardfile(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "	SELECT distinct" +
                "		A.ID," +
                "		FI.ID FILE_ID," +
                "		B.FILE_NAME, " +
                "		flows.flow_id " +
                "	FROM FLOWS " +
                "	INNER JOIN FLOW_STANDARDIZED_FILE" + pub + " A ON FLOWS.FLOW_ID=A.FLOW_ID" +
                "	INNER JOIN JECN_FILE" + pub + " B ON A.FILE_ID = B.FILE_ID AND B.del_state = 0" +
                "  INNER JOIN FILE_CONTENT FI ON B.VERSION_ID = FI.ID";

/*        "SELECT distinct A.ID, C.ID FILE_ID, B.FILE_NAME, flows.flow_id" +
                "  FROM FLOWS" +
                " INNER JOIN FLOW_STANDARDIZED_FILE A" +
                "    ON FLOWS.FLOW_ID = A.FLOW_ID" +
                " INNER JOIN JECN_FILE B" +
                "    ON A.FILE_ID = B.FILE_ID" +
                " INNER JOIN FILE_CONTENT C" +
                " ON C.ID = B.VERSION_ID" +
                "   AND B.del_state = 0";*/
        return sql;
    }

    /**
     * 流程标准化文件 文控
     *
     * @param params
     * @return
     */
    public static String getFlowHistoryRelatedStandardfile(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "	SELECT distinct" +
                "		A.ID," +
                "		B.ID FILE_ID," +
                "		B.FILE_NAME, " +
                "		flows.flow_id " +
                "	FROM FLOWS " +
                "	INNER JOIN FLOW_STANDARDIZED_FILE_H A ON FLOWS.FLOW_ID=A.FLOW_ID AND FLOWS.HISTORY_ID = A.HISTORY_ID" +
                "	INNER JOIN FILE_CONTENT B ON A.FILE_ID = B.ID  ";
        return sql;
    }


    public static String getFlowRelatedRisk(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "	select jr.ID,jr.NAME,flows.flow_id from flows " +
                " inner join JECN_FLOW_RELATED_RISK" + pub + " jfrr on flows.flow_id=jfrr.FLOW_ID" +
                " inner join JECN_RISK jr on jfrr.RISK_ID=jr.ID";
        return sql;
    }

    //文控
    public static String getFlowHistoryRelatedRisk(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "select jr.ID,jr.NAME,flows.flow_id from flows " +
                " inner join JECN_RELATE_RISK_HISTORY  jfrr" +
                " on flows.flow_id=jfrr.RELATE_ID  AND jfrr.history_id = flows.history_id" +
                " inner join jecn_risk  jr" +
                " on jfrr.RISK_ID = jr.id  ";
        return sql;
    }

    public static String getFlowRelatedActivityRisk(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql =
                " SELECT JR.ID,JR.NAME,JFS.FLOW_ID FROM" +
                        " flows JFS " +
                        " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + pub + " JFSIT on JFS.FLOW_ID = JFSIT.FLOW_ID AND JFSIT.FIGURE_TYPE IN" + SqlCommon.getActiveString() +
                        " INNER JOIN JECN_CONTROL_POINT JCPT ON JCPT.ACTIVE_ID = JFSIT.FIGURE_ID" +
                        " INNER JOIN JECN_RISK JR ON JR.ID = JCPT.RISK_ID";
        return sql;
    }

    //文控
    public static String getFlowHistoryRelatedActivityRisk(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "SELECT JR.ID,JR.NAME,JFS.FLOW_ID FROM flows JFS" +
                " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_H JFSIT" +
                " on JFS.FLOW_ID = JFSIT.FLOW_ID AND JFSIT.HISTORY_ID = JFS.HISTORY_ID AND JFSIT.FIGURE_TYPE IN " + SqlCommon.getActiveString() +
                " INNER JOIN JECN_CONTROL_POINT_H JCPT" +
                " ON JCPT.ACTIVE_ID = JFSIT.FIGURE_ID AND JCPT.HISTORY_ID = JFS.HISTORY_ID" +
                " INNER JOIN JECN_RISK JR" +
                " ON JR.ID = JCPT.RISK_ID";
        return sql;
    }

    public static String getFlowRelatedFlows(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "SELECT DISTINCT T.FLOW_ID AS ID,T.FLOW_NAME AS NAME,JFS.FLOW_ID  FROM FLOWS JFS,"
                + "  JECN_FLOW_STRUCTURE_IMAGE" + pub + "  JFSI"
                + "  INNER JOIN JECN_FLOW_STRUCTURE" + pub + " T ON JFSI.LINK_FLOW_ID = T.FLOW_ID AND T.DEL_STATE = 0 "
                + "  WHERE  JFS.FLOW_ID = JFSI.FLOW_ID "
                + "  AND JFSI.IMPL_TYPE IN ('1', '2', '3', '4')  AND JFSI.FIGURE_TYPE IN" + SqlCommon.getImplString();
        return sql;
    }

    //文控
    public static String getFlowHistoryRelatedFlows(Map<String, Object> params) {
        String pub = Utils.getPubByParams(params);
        String sql = "SELECT DISTINCT T.FLOW_ID AS ID,T.FLOW_NAME AS NAME,JFS.FLOW_ID  FROM FLOWS JFS" +
                "                   INNER JOIN    JECN_FLOW_STRUCTURE_IMAGE_H  JFSI" +
                "                   ON JFSI.HISTORY_ID = JFS.HISTORY_ID" +
                "                   INNER JOIN JECN_FLOW_STRUCTURE T" +
                "                   ON JFSI.LINK_FLOW_ID = T.FLOW_ID AND T.DEL_STATE = 0 " +
                "                   WHERE  JFS.FLOW_ID = JFSI.FLOW_ID" +
                "                   AND JFSI.IMPL_TYPE IN ('1', '2', '3', '4')  AND JFSI.FIGURE_TYPE IN " + SqlCommon.getImplString();

        return sql;
    }

    private String getsqlCommonSearch(Map<String, Object> map) {
        return " where t.pre_flow_id=" + map.get("pid").toString() + " AND t.DATA_TYPE = 0 and t.projectid="
                + map.get("projectId").toString() + " and t.del_state=0 order by t.pre_flow_id,t.sort_id,t.flow_id";
    }

    /**
     * 系统管理员sql
     *
     * @param map
     * @return
     */
    public String findChildFlowsAdminSql(Map<String, Object> map) {
        return sqlCommonResult + sqlCommonFrom + getsqlCommonSearch(map);
    }

    /**
     * 流程名称，编号，密级
     *
     * @param map "_T" 临时流程
     * @return
     */
    public String findFlowStructureInfoSql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT T.FLOW_ID,T.FLOW_NAME,T.FLOW_ID_INPUT,T.IS_PUBLIC,T.PRE_FLOW_ID," +
                    "T.NODE_TYPE,T.CONFIDENTIALITY_LEVEL,T.FILE_CONTENT_ID ,T.COMMISSIONER_ID,T.KEYWORD FROM JECN_FLOW_STRUCTURE" + isPub
                    + " T  WHERE T.FLOW_ID = " + flowId;
        } else {
            sql = findFlowStructureHistoryInfoSql(map);
        }
        return sql;
    }

    /**
     * 流程名称，编号，密级 文控
     *
     * @param map "
     * @return
     */
    public String findFlowStructureHistoryInfoSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long flowId = (Long) map.get("flowId");
        return "SELECT T.FLOW_ID,T.FLOW_NAME,T.FLOW_ID_INPUT,T.IS_PUBLIC,T.PRE_FLOW_ID," +
                "T.NODE_TYPE,T.CONFIDENTIALITY_LEVEL,T.FILE_CONTENT_ID,T.COMMISSIONER_ID,T.KEYWORD FROM JECN_FLOW_STRUCTURE_H"
                + " T WHERE T.HISTORY_ID = #{historyId} AND T.FLOW_ID = " + flowId;
    }

    /**
     * 流程ID获取流程基本信息
     *
     * @param map
     * @return
     */
    public String findFlowBasicInfoSql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT T.FLOW_ID,T.FLOW_CUSTOM," + "       T.FLOW_STARTPOINT," + "       T.FLOW_ENDPOINT," + "       T.INPUT,"
                    + "       T.OUPUT," + "       T.TYPE_ID," + "       T.EXPIRY," + "       T.FICTION_PEOPLE_ID,"
                    + "       T.WARD_PEOPLE_ID," + "       T.TYPE_RES_PEOPLE," + "       T.RES_PEOPLE_ID,JF.VERSION_ID FILE_ID "
                    + "  FROM JECN_FLOW_BASIC_INFO" + isPub + " T" +
                    " LEFT JOIN JECN_FILE" + isPub + " JF ON JF.FILE_ID = T.FILE_ID  " + " WHERE T.FLOW_ID =" + flowId;
        } else {
            sql = findFlowBasicHistoryInfoSql(map);
        }
        return sql;
    }

    /**
     * 文控ID获取流程基本信息
     *
     * @param map
     * @return
     */
    public String findFlowBasicHistoryInfoSql(Map<String, Object> map) {
        return "SELECT T.FLOW_ID,T.FLOW_CUSTOM," + "       T.FLOW_STARTPOINT," + "       T.FLOW_ENDPOINT," + "       T.INPUT,"
                + "       T.OUPUT," + "       T.TYPE_ID," + "       T.EXPIRY," + "       T.FICTION_PEOPLE_ID,"
                + "       T.WARD_PEOPLE_ID," + "       T.TYPE_RES_PEOPLE," + "       T.RES_PEOPLE_ID,T.FILE_ID"
                + "  FROM JECN_FLOW_BASIC_INFO_H T   WHERE T.HISTORY_ID =#{historyId} AND T.FLOW_ID = #{flowId}";
    }

    /**
     * 流程类别名称 TYPE_NAME
     *
     * @param typeId
     * @return
     */
    public String findFlowTypeSql(Long typeId) {
        return "SELECT TYPE_NAME FROM JECN_FLOW_TYPE WHERE TYPE_ID = " + typeId;
    }

    /**
     * 流程责任部门
     *
     * @param map (isPub = "_T") 临时表
     * @return
     */
    public String findFlowResOrgSql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT O.ORG_ID, O.ORG_NAME" + "  FROM JECN_FLOW_RELATED_ORG" + isPub + " FRO"
                    + " INNER JOIN JECN_FLOW_ORG O" + "    ON O.ORG_ID = FRO.ORG_ID" + " WHERE FRO.FLOW_ID = " + flowId;
        } else {
            sql = findFlowResHistoryOrgSql(map);
        }
        return sql;
    }

    /**
     * 流程责任部门 文控
     *
     * @param map
     * @return
     */
    public String findFlowResHistoryOrgSql(Map<String, Object> map) {
        return "SELECT O.ORG_ID, O.ORG_NAME" + "  FROM JECN_FLOW_RELATED_ORG_H FRO"
                + " INNER JOIN JECN_FLOW_ORG O" + "    ON O.ORG_ID = FRO.ORG_ID" + " WHERE FRO.HISTORY_ID = #{historyId} AND FRO.FLOW_ID = #{flowId}";
    }

    /**
     * 相关流程
     *
     * @param map
     * @return
     */
    public String findRelatedProcessSql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        Long historyId = getHistoryId(map);
        String isPub = (String) map.get("isPub");
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT JFSI.LINK_FLOW_ID," + "       CASE" + "         WHEN T.FLOW_ID IS NULL THEN"
                    + "          JFSI.FIGURE_TEXT" + "         ELSE" + "          T.FLOW_NAME" + "       END FLOW_NAME,"
                    + "       T.FLOW_ID_INPUT," + "       ORG.ORG_NAME," + "       CASE"
                    + "         WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN" + "          JU.TRUE_NAME"
                    + "         WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN" + "          JFOI.FIGURE_TEXT"
                    + "       END AS RES_PEOPLE," + "       T.UPDATE_DATE," + "       JFSI.IMPL_TYPE"
                    + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JFSI" + "  INNER JOIN JECN_FLOW_STRUCTURE_T"
                    + " T" + "    ON JFSI.LINK_FLOW_ID = T.FLOW_ID" + "   AND T.DEL_STATE = 0"
                    + "  LEFT JOIN JECN_FLOW_BASIC_INFO" + isPub + " JFBI" + "    ON JFBI.FLOW_ID = T.FLOW_ID"
                    + " LEFT JOIN JECN_FLOW_RELATED_ORG" + isPub + " JFRO"
                    + " ON JFRO.FLOW_ID = JFBI.FLOW_ID LEFT JOIN JECN_FLOW_ORG ORG"
                    + " ON JFRO.ORG_ID = ORG.ORG_ID AND ORG.DEL_STATE = 0"
                    + " LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI"
                    + "    ON JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID" + "  LEFT JOIN JECN_USER JU" + "    ON JU.ISLOCK = 0"
                    + "   AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" + " WHERE JFSI.IMPL_TYPE IN (1, 2, 3, 4)"
                    + "   AND JFSI.FIGURE_TYPE IN " + SqlCommon.getImplString() + "   AND JFSI.FLOW_ID = " + flowId
                    + " ORDER BY JFSI.X_POINT,JFSI.Y_POINT ASC";
        } else {
            sql = findRelatedProcessHistorySql(map);
        }
        return sql;
    }

    /**
     * 相关流程 文控
     *
     * @param map
     * @return
     */
    public String findRelatedProcessHistorySql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        String sql = "SELECT JFSI.LINK_FLOW_ID," + "       CASE" + "         WHEN T.FLOW_ID IS NULL THEN"
                + "          JFSI.FIGURE_TEXT" + "         ELSE" + "          T.FLOW_NAME" + "       END FLOW_NAME,"
                + "       T.FLOW_ID_INPUT," + "       ORG.ORG_NAME," + "       CASE"
                + "         WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN" + "          JU.TRUE_NAME"
                + "         WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN" + "          JFOI.FIGURE_TEXT"
                + "       END AS RES_PEOPLE," + "       T.UPDATE_DATE," + "       JFSI.IMPL_TYPE"
                + "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JFSI" + "  INNER JOIN JECN_FLOW_STRUCTURE_T "
                + " T" + "    ON JFSI.LINK_FLOW_ID = T.FLOW_ID" + "   AND T.DEL_STATE = 0 "
                + "  LEFT JOIN JECN_FLOW_BASIC_INFO_H JFBI" + "    ON JFBI.FLOW_ID = T.FLOW_ID  AND JFBI.HISTORY_ID = #{historyId}"
                + " LEFT JOIN JECN_FLOW_RELATED_ORG_H JFRO"
                + " ON JFRO.FLOW_ID = JFBI.FLOW_ID and JFRO.HISTORY_ID = #{historyId}" +
                " LEFT JOIN JECN_FLOW_ORG ORG"
                + " ON JFRO.ORG_ID = ORG.ORG_ID AND ORG.DEL_STATE = 0"
                + " LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI"
                + "    ON JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID" + "  LEFT JOIN JECN_USER JU" + "    ON JU.ISLOCK = 0"
                + "   AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" + " WHERE JFSI.IMPL_TYPE IN (1, 2, 3, 4)"
                + "   AND JFSI.FIGURE_TYPE IN " + SqlCommon.getImplString() + " AND JFSI.FLOW_ID = " + flowId + "  AND JFSI.HISTORY_ID = #{historyId}"
                + " ORDER BY JFSI.IMPL_TYPE";
        return sql;
    }

    public String findRelatedRuleSql(Map<String, Object> map) {
        // Long flowId = (Long) map.get("flowId");
        String isPub = (String) map.get("isPub");
        return "select r.id, r.rule_name,"
                + "        case when r.is_dir=2 and r.is_file_local=0 then jf.doc_id else r.rule_number END as rule_code"
                + "        ,o.org_name," + "        case when  r.TYPE_RES_PEOPLE=0 then ju.true_name"
                + "                when r.type_res_people=1 then jfoi.figure_text END AS RES_PEOPLE"
                + "           from FLOW_RELATED_CRITERION" + isPub + " rs,JECN_RULE" + isPub + " r"
                + "           left join JECN_FLOW_ORG o on r.org_id = o.org_id and o.del_state=0"
                + "           LEFT JOIN JECN_USER JU ON r.TYPE_RES_PEOPLE=0 AND JU.ISLOCK = 0  AND JU.PEOPLE_ID = r.RES_PEOPLE_ID"
                + "           LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON r.TYPE_RES_PEOPLE=1 AND JFOI.FIGURE_ID= r.RES_PEOPLE_ID"
                + "           LEFT JOIN JECN_FILE JF ON r.is_dir=2 and r.is_file_local=0 and JF.FILE_ID= r.File_Id"
                + "           where r.del_state=0 and rs.criterion_class_id = r.id and rs.flow_id =#{flowId}";
    }

    /**
     * 相关标准
     *
     * @param map
     * @return
     */
    public String findRelatedStandardSql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT C.CRITERION_CLASS_ID, C.CRITERION_CLASS_NAME, C.STAN_TYPE"
                    + "  FROM JECN_FLOW_STANDARD" + isPub + " FRC, JECN_CRITERION_CLASSES C"
                    + " WHERE FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID" + "   AND C.STAN_TYPE <> 1"
                    + "   AND FRC.FLOW_ID =  " + flowId
                    + " UNION "
                    + "SELECT C.CRITERION_CLASS_ID, C.CRITERION_CLASS_NAME, C.STAN_TYPE"
                    + "  FROM JECN_FLOW_STANDARD" + isPub + " FRC, JECN_CRITERION_CLASSES C, JECN_FILE" + isPub + " JF"
                    + " WHERE FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID" + "   AND C.STAN_TYPE = 1"
                    + "   AND JF.FILE_ID = C.RELATED_ID" + "   AND JF.DEL_STATE = 0" + "   AND FRC.FLOW_ID = " + flowId
                    + " UNION "
                    + "SELECT C.CRITERION_CLASS_ID, C.CRITERION_CLASS_NAME, C.STAN_TYPE"
                    + "  FROM JECN_ACTIVE_STANDARD" + isPub + "      JAS," + "       JECN_FLOW_STRUCTURE_IMAGE" + isPub + " SI,"
                    + "       JECN_CRITERION_CLASSES    C" + " WHERE JAS.ACTIVE_ID = SI.FIGURE_ID"
                    + "   AND SI.FIGURE_TYPE IN" + SqlCommon.getActiveString()
                    + "   AND JAS.STANDARD_ID = C.CRITERION_CLASS_ID" + "   AND SI.FLOW_ID =  " + flowId;
        } else {
            sql = findRelatedStandardHistorySql(map);
        }
        return sql;
    }

    /**
     * 相关标准 文控
     *
     * @param map
     * @return
     */
    public String findRelatedStandardHistorySql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        return "SELECT C.CRITERION_CLASS_ID, C.CRITERION_CLASS_NAME, C.STAN_TYPE" +
                "  FROM JECN_STANDARD_HISTORY   FRC, JECN_CRITERION_CLASSES C" +
                "   WHERE FRC.STANDARD_ID = C.CRITERION_CLASS_ID  AND FRC.RELATE_TYPE = 0  AND C.STAN_TYPE <> 1  " +
                "    AND FRC.RELATE_ID =" + flowId + "    AND FRC.HISTORY_ID = #{historyId} " +
                " UNION  " +
                "  SELECT C.CRITERION_CLASS_ID, C.CRITERION_CLASS_NAME, C.STAN_TYPE" +
                "  FROM JECN_STANDARD_HISTORY    FRC, JECN_CRITERION_CLASSES C, FILE_CONTENT   JF" +
                " WHERE FRC.STANDARD_ID = C.CRITERION_CLASS_ID    AND C.STAN_TYPE = 1 AND  FRC.RELATE_TYPE = 0" +
                " AND JF.FILE_ID = C.RELATED_ID       AND FRC.RELATE_ID = " + flowId + "    AND FRC.HISTORY_ID =#{historyId}" +
                "  UNION  " +
                "  SELECT C.CRITERION_CLASS_ID, C.CRITERION_CLASS_NAME, C.STAN_TYPE" +
                "  FROM JECN_ACTIVE_STANDARD_H      JAS,         JECN_FLOW_STRUCTURE_IMAGE_H    SI," +
                "  JECN_CRITERION_CLASSES   C   WHERE JAS.ACTIVE_ID = SI.FIGURE_ID  AND JAS.HISTORY_ID = SI.HISTORY_ID     " +
                " AND SI.FIGURE_TYPE IN  " + SqlCommon.getActiveString() +
                " AND JAS.STANDARD_ID = C.CRITERION_CLASS_ID     AND SI.HISTORY_ID = #{historyId}     AND SI.FLOW_ID =  " + flowId;

    }


    public String findRelatedRiskSql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT DISTINCT JR.RISK_CODE,JR.ID,JR.NAME" + "  FROM JECN_RISK JR"
                    + " INNER JOIN JECN_CONTROL_POINT" + isPub + " JCPT" + "    ON JR.ID = JCPT.RISK_ID"
                    + " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JFSIT" + "    ON JCPT.ACTIVE_ID = JFSIT.FIGURE_ID"
                    + " WHERE JFSIT.FLOW_ID = " + flowId;
        } else {
            sql = findRelatedRiskHistorySql(map);
        }
        return sql;
    }

    /**
     * 相关风险文控
     *
     * @param map
     * @return
     */
    public String findRelatedRiskHistorySql(Map<String, Object> map) {
        Long flowId = (Long) map.get("flowId");
        return " SELECT DISTINCT JR.RISK_CODE, JR.ID, JR.NAME" +
                " FROM JECN_RISK JR" +
                " INNER JOIN JECN_CONTROL_POINT JCPT" +
                "   ON JR.ID = JCPT.RISK_ID" +
                " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_H JFSIT" +
                "   ON JCPT.ACTIVE_ID = JFSIT.FIGURE_ID" +
                "  AND JFSIT.HISTORY_ID =  #{historyId} AND  JFSIT.FLOW_ID = " + flowId;
    }

    /**
     * 流程文控信息
     *
     * @param map
     * @return
     */
    public String findDocumentControlsSql(Map<String, Object> map) {
        return "SELECT T.ID,T.VERSION_ID,T.PUBLISH_DATE,T.START_TIME,T.END_TIME,T.DRAFT_PERSON,T.APPROVE_COUNT"
                + " FROM JECN_TASK_HISTORY_NEW T WHERE T.RELATE_ID = #{flowId} AND T.TYPE = #{type}";
    }

    public String findDocumentControlByIdSql(Map<String, Object> map) {
        return "SELECT T.ID,T.VERSION_ID,T.PUBLISH_DATE,T.START_TIME,T.END_TIME,T.DRAFT_PERSON,T.APPROVE_COUNT"
                + " FROM JECN_TASK_HISTORY_NEW T WHERE T.ID =#{historyId}";
    }

    public String findDocumentControlFollowsSql(Map<String, Object> map) {
        return "SELECT T.APPELLATION,T.NAME FROM JECN_TASK_HISTORY_FOLLOW T WHERE T.HISTORY_ID =#{historyId} ORDER BY T.SORT";
    }

    /**
     * 获取流程对应的KPI 名称
     *
     * @param map
     * @return
     */
    public String findKpiNamesSql(Map<String, Object> map) {
        return "SELECT T.KPI_AND_ID,T.KPI_NAME FROM JECN_FLOW_KPI_NAME T WHERE T.FLOW_ID = #{flowId}";
    }

    /**
     * 获取KPI值
     *
     * @param map
     * @return
     */
    public String findKpiValuesByDateSql(Map<String, Object> map) {
        String startTime = null;
        String endTime = null;
        if (map.get("startTime") != null) {
            startTime = map.get("startTime").toString() + " 00:00:00";
        }
        if (map.get("startTime") != null) {
            endTime = map.get("endTime").toString() + " 23:59:59";
        }
        return "SELECT T.KPI_ID,T.KPI_HOR_VALUE,T.KPI_VALUE FROM JECN_FLOW_KPI T "
                + "WHERE T.KPI_AND_ID = #{kpiAndId} " +
                "AND T.KPI_HOR_VALUE >= " + getStrToDBFunction(startTime) +
                " AND T.KPI_HOR_VALUE<= " + getStrToDBFunction(endTime)
                + "order by T.KPI_HOR_VALUE ASC";
    }

    public String findKpiNameByIdSql(Map<String, Object> map) {
        return "SELECT T.KPI_AND_ID,T.FLOW_ID,T.KPI_NAME," + "       T.KPI_TYPE," + "       T.KPI_DEFINITION,"
                + "       T.KPI_STATISTICAL_METHODS," + "       T.KPI_TARGET," + "       T.KPI_HORIZONTAL,"
                + "       T.KPI_VERTICAL," + "       T.CREAT_TIME," + "       T.KPI_TARGET_OPERATOR,"
                + "       T.KPI_DATA_METHOD," + "       T.KPI_DATA_PEOPEL_ID," + "       T.KPI_RELEVANCE,"
                + "       T.KPI_TARGET_TYPE," + "       T.FIRST_TARGER_ID," + "       T.UPDATE_TIME,"
                + "       T.CREATE_PEOPLE_ID," + "       T.UPDATE_PEOPLE_ID," + "       T.KPI_PURPOSE,"
                + "       T.KPI_POINT," + "       T.KPI_PERIOD," + "       T.KPI_EXPLAIN"
                + "  FROM JECN_FLOW_KPI_NAME T" + " WHERE T.KPI_AND_ID = #{kpiAndId}";
    }

    public String insertKpiSql(Map<String, Object> map) {
        if (BaseSqlProvider.isOracle()) {
            return "INSERT INTO JECN_FLOW_KPI" + "  (KPI_ID,KPI_VALUE," + "   KPI_HOR_VALUE," + "   KPI_AND_ID,"
                    + "   CREATE_TIME," + "   UPDATE_TIME," + "   CREATE_PEOPLE_ID," + "   UPDATE_PEOPLE_ID)"
                    + " VALUES"
                    + "  (JECN_FLOW_KPI_SQUENCE.NEXTVAL,#{kpiValue},#{kpiHorVlaue},#{kpiAndId},#{createTime},#{updateTime},#{createPeopleId},#{updatePeopleId})";
        } else {
            return "INSERT INTO JECN_FLOW_KPI" + "  (KPI_VALUE," + "   KPI_HOR_VALUE," + "   KPI_AND_ID,"
                    + "   CREATE_TIME," + "   UPDATE_TIME," + "   CREATE_PEOPLE_ID," + "   UPDATE_PEOPLE_ID)"
                    + " VALUES"
                    + "  (#{kpiValue},#{kpiHorVlaue},#{kpiAndId},#{createTime},#{updateTime},#{createPeopleId},#{updatePeopleId})";
        }
    }

    public String updateKpiSql(KPIValuesBean kpiValue) {
        return "UPDATE JECN_FLOW_KPI SET KPI_VALUE = " + kpiValue.getKpiValue() + " WHERE KPI_ID = " + kpiValue.getKpiId();
    }

    public String deleteFlowKpi(Map<String, Object> map) {
        Long flowId = Long.valueOf(map.get("flowId").toString());
        String sql = "";
        if (isOracle()) {
            sql = "DELETE FROM JECN_FLOW_KPI V" + " WHERE EXISTS (SELECT 1 FROM JECN_FLOW_KPI_NAME K"
                    + "          LEFT JOIN JECN_FLOW_KPI_NAME_T KT ON K.KPI_AND_ID = KT.KPI_AND_ID"
                    + "         WHERE KT.KPI_AND_ID IS NULL AND V.KPI_AND_ID = K.KPI_AND_ID AND K.FLOW_ID = " + flowId
                    + ")";
        } else {
            sql = "DELETE V FROM JECN_FLOW_KPI V" + " WHERE EXISTS (SELECT 1 FROM JECN_FLOW_KPI_NAME K"
                    + "          LEFT JOIN JECN_FLOW_KPI_NAME_T KT ON K.KPI_AND_ID = KT.KPI_AND_ID"
                    + "         WHERE KT.KPI_AND_ID IS NULL AND V.KPI_AND_ID = K.KPI_AND_ID AND K.FLOW_ID = " + flowId
                    + ")";
        }

        return sql;
    }

    public String getRelateRuleByFlowMapId(Long flowId) {

        String sql = "SELECT DISTINCT T.ID," + "                T.IS_DIR," + "                CASE"
                + "                  WHEN T.IS_DIR = 2 THEN" + "                   (SELECT JF.VERSION_ID"
                + "                      FROM JECN_FILE_T JF"
                + "                     WHERE JF.FILE_ID = T.FILE_ID AND JF.DEL_STATE=0)" + "                  ELSE"
                + "                   NULL" + "                END AS VERSION_ID" + "  FROM JECN_RULE_T T"
                + "  LEFT JOIN JECN_RULE JR ON T.ID = JR.ID" + " WHERE JR.ID IS NULL" + "   AND T.ID IN (SELECT JR.ID"
                + "                  FROM JECN_RULE_T JR, JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "                 WHERE JR.ID = JFSI.LINK_FLOW_ID AND JR.DEL_STATE = 0" + "                   AND JFSI.FIGURE_TYPE ="
                + SqlCommon.getSystemString() + "                   AND JFSI.FLOW_ID = " + flowId + ")" + " UNION "
                + "SELECT DISTINCT T.ID," + "                T.IS_DIR," + "                CASE"
                + "                  WHEN T.IS_DIR = 2 THEN" + "                   (SELECT JF.VERSION_ID"
                + "                      FROM JECN_FILE_T JF"
                + "                     WHERE JF.FILE_ID = T.FILE_ID and jf.del_state=0)" + "                  ELSE"
                + "                   NULL" + "                END AS VERSION_ID" + "  FROM JECN_RULE_T T"
                + "  LEFT JOIN JECN_RULE JR ON T.ID = JR.ID" + " WHERE JR.ID IS NULL  AND JR.DEL_STATE = 0" + "   AND T.ID IN (SELECT JR.ID"
                + "                  FROM JECN_RULE_T JR, FLOW_RELATED_CRITERION_T FRC"
                + "                 WHERE JR.ID = FRC.CRITERION_CLASS_ID" + "                   AND FRC.FLOW_ID = "
                + flowId + ")";
        return sql;

    }

    public String getRelateRuleByFlowId(Long flowId) {
        String sql = "SELECT DISTINCT T.ID,T.IS_DIR,CASE WHEN T.IS_DIR=2 AND T.IS_FILE_LOCAL=0 THEN (SELECT JF.VERSION_ID FROM JECN_FILE_T JF WHERE JF.FILE_ID=T.FILE_ID) " +
                "ELSE NULL END AS VERSION_ID FROM JECN_RULE_T T LEFT JOIN JECN_RULE JR ON T.ID=JR.ID WHERE JR.ID IS NULL"
                + "   AND T.ID IN (SELECT JR.ID FROM JECN_RULE_T JR,FLOW_RELATED_CRITERION_T FRC WHERE JR.ID=FRC.CRITERION_CLASS_ID " +
                "AND FRC.FLOW_ID= )" + flowId;
        return sql;
    }

    public String insertTemplet(Map<String, Object> paramMap) {
        String sql = "";
        if (isOracle()) {
            // 添加样例表的数据
            sql = "insert into jecn_templet (id,mode_file_id,file_id)"
                    + "       select JECN_TEMPLET_SQUENCE.Nextval,jt.mode_file_id,jt.file_id from jecn_templet_t jt"
                    + "       where jt.mode_file_id in"
                    + "       (select jmf.mode_file_id from jecn_mode_file_t jmf,jecn_flow_structure_image_t jfsi where jmf.figure_id = jfsi.figure_id and  jfsi.flow_id=#{flowId})";
        } else {
            // 添加样例表的数据
            sql = "insert into jecn_templet (mode_file_id,file_id)"
                    + "       select jt.mode_file_id,jt.file_id from jecn_templet_t jt"
                    + "       where jt.mode_file_id in"
                    + "       (select jmf.mode_file_id from jecn_mode_file_t jmf,jecn_flow_structure_image_t jfsi where jmf.figure_id = jfsi.figure_id and  jfsi.flow_id=#{flowId})";
        }

        return sql;
    }

    /**
     * 流程级别信息
     *
     * @param map
     * @return
     */
    public String getBasicInfoSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == 0 || historyId == null) {
            sql = "select " +
                    " BI.FLOW_ID,FS. FLOW_NAME,BI. FLOW_PURPOSE,BI. FLOW_STARTPOINT,BI. FLOW_ENDPOINT,BI. INPUT,BI. "
                    + "OUPUT,BI. APPLICABILITY,BI. NOUT_GLOSSARY,BI."
                    + " FLOW_CUSTOM, JF.VERSION_ID FILE_ID,BI.IS_SHOW_LINE_X,BI. IS_SHOW_LINE_Y,BI. IS_X_OR_Y,BI. "
                    + "DRIVER_TYPE,BI. DRIVER_RULES,BI. TEST_RUN_NUMBER,BI. TYPE_RES_PEOPLE,BI. "
                    + "RES_PEOPLE_ID,BI. TYPE_ID,BI. DFLOW_SUPPLEMENT,BI. NO_APPLICABILITY,BI. "
                    + "FLOW_SUMMARIZE,BI. EXPIRY,BI.WARD_PEOPLE_ID,BI.FICTION_PEOPLE_ID,BI. "
                    + "FLOW_RELATEDFILE,BI. FLOW_CUSTOMONE,BI. FLOW_CUSTOMTWO,BI. FLOW_CUSTOMTHREE,BI. FLOW_CUSTOMFOUR,BI. FLOW_CUSTOMFIVE" +
                    " from JECN_FLOW_BASIC_INFO" + isPub + " BI " +
                    " INNER JOIN JECN_FLOW_STRUCTURE" + isPub + " FS ON FS.FLOW_ID = BI.FLOW_ID" +
                    " LEFT JOIN JECN_FILE" + isPub + " JF ON JF.FILE_ID = BI.FILE_ID " +
                    " where BI.FLOW_ID=#{flowId}";
        } else {
            sql = getBasicInfoHistorySql(map);
        }
        return sql;
    }


    /**
     * 流程级别信息 文控
     *
     * @param map
     * @return
     */
    public String getBasicInfoHistorySql(Map<String, Object> map) {
        return "select * from JECN_FLOW_BASIC_INFO_H where  FLOW_ID=#{flowId} AND HISTORY_ID=#{historyId}";
    }

    /**
     * 查询是否有文控
     *
     * @param map
     * @return
     */
    public String getBasicInfoHistoryCount(Map<String, Object> map) {
        return "select count(FLOW_ID) from JECN_FLOW_STRUCTURE_H where  FLOW_ID=#{id} AND HISTORY_ID=#{historyId}";
    }

    /**
     * 流程主表信息
     *
     * @param map
     * @return
     */
    public String findFlowStructureByMapSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        return "select * from JECN_FLOW_STRUCTURE" + isPub + " where FLOW_ID=#{flowId}";
    }

    public String getFlowDriverSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String pub = "";
        if (StringUtils.isNotBlank(isPub)) {
            isPub = "T";
            pub = "_T";
        }
        String sql = "SELECT" +
                "       JFD.FLOW_ID," +
                "       JFD.FREQUENCY," +
                "       JFD.START_TIME," +
                "       JFD.END_TIME," +
                "       JFD.QUANTITY," +
                "       JFDT.MONTH," +
                "       JFDT.WEEK," +
                "       JFDT.DAY," +
                "       JFDT.HOUR" +
                "  FROM JECN_FLOW_DRIVER" + isPub + " JFD" +
                "  LEFT JOIN JECN_FLOW_DRIVER_TIME" + pub + " JFDT" +
                "    ON JFD.FLOW_ID = JFDT.FLOW_ID" +
                "  WHERE JFD.FLOW_ID=#{flowId}";
        return sql;
    }

    /**
     * 流程责任部门
     *
     * @param map
     * @return
     */
    public String getFlowDutyOrgSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT O.ORG_ID, O.ORG_NAME  FROM JECN_FLOW_RELATED_ORG" + isPub + " FRO"
                + " INNER JOIN JECN_FLOW_ORG O    ON O.ORG_ID = FRO.ORG_ID  WHERE FRO.FLOW_ID = #{flowId}";
        return sql;
    }

    private static Long getHistoryId(Map<String, Object> map) {
        boolean iFH = map.containsKey("historyId");
        if (iFH == true) {
            return Long.valueOf(map.get("historyId").toString());
        }
        return 0L;
    }

    /**
     * (流程文件活动明细)获取活动信息
     *
     * @param map
     * @return
     */
    public String findActivityListByFlowIdSql(Map<String, Object> map) {
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            String isPub = (String) map.get("isPub");
            sql = "SELECT T.FIGURE_ID, T.ACTIVITY_ID,T.FIGURE_TEXT,"
                    + "       T.ACTIVITY_SHOW,T.LINECOLOR, T.ROLE_RES,"
                    + "       T.INNER_CONTROL_RISK,T.STANDARD_CONDITIONS,T.ACTIVITY_INPUT,T.ACTIVITY_OUTPUT,T.TARGET_VALUE,T.STATUS_VALUE FROM JECN_FLOW_STRUCTURE_IMAGE"
                    + isPub + " T" + " WHERE T.FIGURE_TYPE IN" + SqlCommon.getActiveString() + "   AND FLOW_ID = #{flowId}";
        } else {
            sql = findActivityListByHistoryIdSql(map);
        }
        return sql;
    }

    /**
     * (流程文件活动明细)获取活动信息  文控
     *
     * @param map
     * @return
     */
    public String findActivityListByHistoryIdSql(Map<String, Object> map) {
        String sql = "SELECT T.FIGURE_ID, T.ACTIVITY_ID,T.FIGURE_TEXT,"
                + "       T.ACTIVITY_SHOW,T.LINECOLOR, T.ROLE_RES,"
                + "       T.INNER_CONTROL_RISK,T.STANDARD_CONDITIONS,T.ACTIVITY_INPUT,T.ACTIVITY_OUTPUT,T.TARGET_VALUE,T.STATUS_VALUE " +
                " FROM JECN_FLOW_STRUCTURE_IMAGE_H T" +
                " WHERE T.FIGURE_TYPE IN" + SqlCommon.getActiveString() + " AND FLOW_ID = #{flowId}  AND T.HISTORY_ID = #{historyId}";
        return sql;
    }

    /**
     * 流程文件角色信息
     *
     * @param map
     * @return
     */
    public String findRoleListByFlowIdSql(Map<String, Object> map) {
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            String isPub = (String) map.get("isPub");
            sql = "SELECT T.FIGURE_ID, T.FIGURE_TEXT, T.ROLE_RES FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " T"
                    + " WHERE T.FIGURE_TYPE IN " + SqlCommon.getRoleString() + " AND FLOW_ID = #{flowId} ORDER BY T.Y_POINT";
        } else {
            sql = findRoleListByHistorySql(map);
        }
        return sql;
    }

    /**
     * 流程文件角色信息 文控
     *
     * @param map
     * @return
     */
    public String findRoleListByHistorySql(Map<String, Object> map) {
        String sql = "SELECT T.FIGURE_ID, T.FIGURE_TEXT, T.ROLE_RES FROM JECN_FLOW_STRUCTURE_IMAGE_H T"
                + " WHERE T.FIGURE_TYPE IN " + SqlCommon.getRoleString() + " AND FLOW_ID = #{flowId} AND T.HISTORY_ID = #{historyId} ORDER BY T.Y_POINT";
        return sql;
    }

    /**
     * 角色活动对应关系 文控
     *
     * @param map
     * @return
     */
    public String findRoleRelatedActiveByHistorySql(Map<String, Object> map) {
        String sql = "SELECT RA.FIGURE_ROLE_ID, RA.FIGURE_ACTIVE_ID,FSI.FIGURE_TEXT,FSI.ROLE_RES"
                + "  FROM JECN_ROLE_ACTIVE_H RA" + " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_H FSI"
                + "    ON FSI.FIGURE_ID = RA.FIGURE_ROLE_ID AND FSI.HISTORY_ID = RA.HISTORY_ID " + " WHERE FSI.FLOW_ID = #{flowId} AND FSI.HISTORY_ID = #{historyId}";
        return sql;
    }

    /**
     * 角色活动对应关系
     *
     * @param map
     * @return
     */
    public String findRoleRelatedActiveByFlowIdSql(Map<String, Object> map) {
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            String isPub = (String) map.get("isPub");
            sql = "SELECT RA.FIGURE_ROLE_ID, RA.FIGURE_ACTIVE_ID,FSI.FIGURE_TEXT,FSI.ROLE_RES"
                    + "  FROM JECN_ROLE_ACTIVE" + isPub + " RA" + " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + isPub + " FSI"
                    + "    ON FSI.FIGURE_ID = RA.FIGURE_ROLE_ID" + " WHERE FSI.FLOW_ID = #{flowId}";
        } else {
            sql = findRoleRelatedActiveByHistorySql(map);
        }
        return sql;
    }

    /**
     * 活动输入
     *
     * @param map
     * @return
     */
    public String findActivityInputByFlowIdSql(Map<String, Object> map) {
        String sql = "";
        if (!ConfigUtils.useNewInout()) { //是否启用新版输入输出
            String isPub = (String) map.get("isPub");
            Long historyId = getHistoryId(map);
            if (historyId == null || historyId == 0) {
                sql = "SELECT T.FILE_ID," +
                        "       JSI.FIGURE_ID," +
                        "       JF.VERSION_ID FILE_S_ID," +
                        "       JF.FILE_NAME," +
                        "       T.FILE_TYPE," +
                        "       JF.DOC_ID," +
                        "       JFO.ORG_NAME," +
                        "       JSI.ACTIVITY_INPUT" +
                        "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JSI" +
                        "  LEFT JOIN JECN_ACTIVITY_FILE" + isPub + " T" +
                        "    ON T.FIGURE_ID = JSI.FIGURE_ID" +
                        "  LEFT JOIN JECN_FILE" + isPub + " JF" +
                        "    ON T.FILE_S_ID = JF.FILE_ID" +
                        "   AND JF.DEL_STATE = 0" +
                        "  LEFT JOIN JECN_FLOW_ORG JFO" +
                        "    ON JFO.ORG_ID = JF.ORG_ID" +
                        " WHERE JSI.FLOW_ID = #{flowId}" +
                        "   AND (T.FILE_ID IS NOT NULL OR (JSI.ACTIVITY_OUTPUT IS NOT NULL AND JSI.ACTIVITY_OUTPUT <> ' ' ))";
            } else {
                sql = findActivityInputByHistorySql(map);
            }
        } else {
            sql = findActivityTermInputByFlowIdSql(map);
        }
        return sql;
    }


    /**
     * 新版活动输入
     *
     * @param map
     * @return
     */
    public String findActivityTermInputByFlowIdSql(Map<String, Object> map) {
        String idStr = CommonSqlProvider.isOracle() ? "to_char(t.file_id)" : "cast(t.file_id as varchar)";
        String sql = "";
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        if (historyId == null || historyId == 0) {
            sql = " SELECT INOUT.ID        FILE_ID," +
                    "       INOUT.FIGURE_ID," +
                    "       FT.VERSION_ID   FILE_S_ID," +
                    "       FT.FILE_NAME," +
                    "       INOUT.TYPE      FILE_TYPE," +
                    "       FT.DOC_ID," +
                    "       JFO.ORG_NAME," +
                    "       INOUT.EXPLAIN   ACTIVITY_INPUT," +
                    "       SAM.TYPE" +
                    "  FROM JECN_FIGURE_IN_OUT" + isPub + " INOUT" +
                    " INNER JOIN JECN_FIGURE_IN_OUT_SAMPLE" + isPub + " SAM " +
                    " ON INOUT.ID = SAM.IN_OUT_ID  " +
                    " AND SAM.TYPE = 0" +
                    " INNER JOIN JECN_FILE" + isPub + " FT" +
                    "    ON SAM.FILE_ID = FT.FILE_ID" +
                    "   AND FT.DEL_STATE = 0" +
                    "  LEFT JOIN JECN_FLOW_ORG JFO" +
                    "    ON JFO.ORG_ID = FT.ORG_ID" +
                    "    WHERE INOUT.FLOW_ID =  #{flowId} AND INOUT.TYPE = 0";
        } else {
            sql = " SELECT INOUT.ID        FILE_ID," +
                    "       INOUT.FIGURE_ID," +
                    "       FT.VERSION_ID   FILE_S_ID," +
                    "       FT.FILE_NAME," +
                    "       INOUT.TYPE      FILE_TYPE," +
                    "       '' DOC_ID," +
                    "       INOUT.EXPLAIN   ACTIVITY_INPUT," +
                    "       SAM.TYPE" +
                    "  FROM JECN_FIGURE_IN_OUT_H INOUT" +
                    " INNER JOIN JECN_FIGURE_IN_OUT_SAMPLE_H SAM " +
                    " ON INOUT.ID = SAM.IN_OUT_ID  AND INOUT.HISTORY_ID = SAM.HISTORY_ID " +
                    " AND SAM.TYPE = 0" +
                    " INNER JOIN JECN_FILE FT" +
                    "    ON SAM.FILE_ID = FT.FILE_ID" +
                    "   AND FT.DEL_STATE = 0" +
                    "  LEFT JOIN JECN_FLOW_ORG JFO" +
                    "    ON JFO.ORG_ID = FT.ORG_ID" +
                    "    WHERE INOUT.FLOW_ID =  #{flowId} AND INOUT.HISTORY_ID = #{historyId}  AND INOUT.TYPE = 0 ";
        }
        return sql;
    }

    /**
     * 活动输入 文控
     *
     * @param map
     * @return
     */
    public String findActivityInputByHistorySql(Map<String, Object> map) {
        String sql = "SELECT T.FILE_ID," +
                "       JSI.FIGURE_ID," +
                "       T.FILE_S_ID," +
                "       JF.FILE_NAME," +
                "       T.FILE_TYPE," +
                "       '' DOC_ID," +
                "       JSI.ACTIVITY_INPUT" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JSI" +
                "  LEFT JOIN JECN_ACTIVITY_FILE_H T" +
                "    ON T.FIGURE_ID = JSI.FIGURE_ID AND T.HISTORY_ID = JSI.HISTORY_ID and T.FILE_TYPE=0" +
                "  LEFT JOIN FILE_CONTENT JF" +
                "    ON T.FILE_S_ID = JF.ID " +
                " WHERE JSI.FLOW_ID = #{flowId} AND  JSI.HISTORY_ID = #{historyId}" +
                "   AND (T.FILE_ID IS NOT NULL OR JSI.ACTIVITY_INPUT IS NOT NULL)";
        return sql;
    }

    /**
     * 活动操作规范 文控
     *
     * @param map
     * @return
     */
    public String findActivityOperateByHistorySql(Map<String, Object> map) {
        String sql = "SELECT T.FILE_ID," +
                "       JSI.FIGURE_ID," +
                "       T.FILE_S_ID," +
                "       JF.FILE_NAME," +
                "       T.FILE_TYPE," +
                "       '' DOC_ID," +
                "       JSI.ACTIVITY_INPUT" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JSI" +
                "  LEFT JOIN JECN_ACTIVITY_FILE_H T" +
                "    ON T.FIGURE_ID = JSI.FIGURE_ID AND T.HISTORY_ID = JSI.HISTORY_ID and T.FILE_TYPE=1" +
                "  LEFT JOIN FILE_CONTENT JF" +
                "    ON T.FILE_S_ID = JF.ID " +
                " WHERE JSI.FLOW_ID = #{flowId} AND  JSI.HISTORY_ID = #{historyId}" +
                "   AND (T.FILE_ID IS NOT NULL OR JSI.ACTIVITY_INPUT IS NOT NULL)";
        return sql;
    }


    /**
     * 活动输出
     *
     * @param map
     * @return
     */
    public String findActivityOutputByFlowIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (!ConfigUtils.useNewInout()) { //是否启用新版输入输出
            if (historyId == null || historyId == 0) {
                sql = "SELECT T.MODE_FILE_ID," +
                        "       JSI.FIGURE_ID," +
                        "       JF.VERSION_ID FILE_M_ID," +
                        "       JF.FILE_NAME," +
                        "       JF.DOC_ID," +
                        "       JSI.ACTIVITY_OUTPUT" +
                        "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JSI" +
                        "  LEFT JOIN JECN_MODE_FILE" + isPub + " T" +
                        "    ON T.FIGURE_ID = JSI.FIGURE_ID" +
                        "  LEFT JOIN JECN_FILE" + isPub + " JF" +
                        "    ON T.FILE_M_ID = JF.FILE_ID" +
                        "   AND JF.DEL_STATE = 0" +
                        " WHERE JSI.FLOW_ID =  #{flowId}" +
                        "   AND (T.MODE_FILE_ID IS NOT NULL OR(JSI.ACTIVITY_OUTPUT IS NOT NULL AND JSI.ACTIVITY_OUTPUT <> ' '))";
            } else {
                sql = findActivityOutputByHistorySql(map);
            }
        } else {
            sql = findActivityTermOutputByFlowIdSql(map);
        }
        return sql;
    }

    /**
     * 新版活动输出
     *
     * @param map
     * @return
     */
    public String findActivityTermOutputByFlowIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT INOUT.ID        MODE_FILE_ID," +
                    "       INOUT.FIGURE_ID," +
                    "       FT.VERSION_ID   FILE_M_ID," +
                    "       FT.FILE_NAME," +
                    "       FT.DOC_ID," +
                    "       INOUT.EXPLAIN   ACTIVITY_OUTPUT" +
                    "  FROM JECN_FIGURE_IN_OUT" + isPub + " INOUT" +
                    "  INNER JOIN JECN_FIGURE_IN_OUT_SAMPLE" + isPub + " SAM" +
                    "  ON INOUT.ID = SAM.IN_OUT_ID AND SAM.TYPE = 0" +
                    " INNER JOIN JECN_FILE" + isPub + " FT" +
                    "    ON SAM.FILE_ID = FT.FILE_ID" +
                    "   AND FT.DEL_STATE = 0" +
                    "   WHERE INOUT.FLOW_ID =  #{flowId}";
        } else {
            sql = "SELECT INOUT.ID        MODE_FILE_ID," +
                    "       INOUT.FIGURE_ID," +
                    "       FT.VERSION_ID   FILE_M_ID," +
                    "       FT.FILE_NAME," +
                    "       FT.DOC_ID," +
                    "       INOUT.EXPLAIN   ACTIVITY_OUTPUT" +
                    "  FROM JECN_FIGURE_IN_OUT_H INOUT" +
                    "  INNER JOIN JECN_FIGURE_IN_OUT_SAMPLE_H SAM" +
                    "  ON INOUT.ID = SAM.IN_OUT_ID AND INOUT.HISTORY_ID = SAM.HISTORY_ID AND SAM.TYPE = 0" +
                    " INNER JOIN JECN_FILE FT" +
                    "    ON SAM.FILE_ID = FT.FILE_ID" +
                    "   AND FT.DEL_STATE = 0" +
                    "   WHERE INOUT.FLOW_ID =  #{flowId} AND INOUT.TYPE =1 AND INOUT.HISTORY_ID = #{historyId}";
        }
        return sql;
    }

    /**
     * 活动输出 文控
     *
     * @param map
     * @return
     */
    public String findActivityOutputByHistorySql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT T.MODE_FILE_ID," +
                "       JSI.FIGURE_ID," +
                "       T.FILE_M_ID," +
                "       JF.FILE_NAME," +
                "       '' DOC_ID," +
                "       JSI.ACTIVITY_OUTPUT" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JSI" +
                "  LEFT JOIN JECN_MODE_FILE_H T" +
                "    ON T.FIGURE_ID = JSI.FIGURE_ID AND T.HISTORY_ID = JSI.HISTORY_ID" +
                "  LEFT JOIN FILE_CONTENT JF" +
                "    ON T.FILE_M_ID = JF.ID" +
                " WHERE JSI.FLOW_ID =  #{flowId} AND  JSI.HISTORY_ID =  #{historyId}" +
                "   AND (T.MODE_FILE_ID IS NOT NULL OR JSI.ACTIVITY_OUTPUT IS NOT NULL)";
        return sql;
    }

    /**
     * 角色相关岗位
     *
     * @param map
     * @return
     */
    public String getRoleRelatedPostSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT T.FIGURE_ID ROLE_ID, JF.FIGURE_ID AS POS_ID, JF.FIGURE_TEXT"
                    + "  FROM JECN_FLOW_ORG_IMAGE       JF," + "       JECN_FLOW_ORG             JFO,"
                    + "       JECN_FLOW_STRUCTURE_IMAGE" + isPub + " T," + "       PROCESS_STATION_RELATED" + isPub
                    + "   RS" + " WHERE JF.ORG_ID = JFO.ORG_ID" + "   AND JFO.DEL_STATE = 0"
                    + "   AND JF.FIGURE_TYPE = 'PositionFigure'" + "   AND JF.FIGURE_ID = RS.FIGURE_POSITION_ID"
                    + "   AND RS.TYPE = '0'" + "   AND RS.FIGURE_FLOW_ID = T.FIGURE_ID" + "   AND T.FIGURE_TYPE IN "
                    + SqlCommon.getRoleString() + "   AND T.FLOW_ID = #{flowId}";
        } else {
            sql = getRoleRelatedPostHistorySql(map);
        }
        return sql;
    }

    /**
     * 角色相关岗位 文控
     *
     * @param map
     * @return
     */
    public String getRoleRelatedPostHistorySql(Map<String, Object> map) {
        String sql = "SELECT T.FIGURE_ID ROLE_ID, JF.FIGURE_ID AS POS_ID, JF.FIGURE_TEXT"
                + "  FROM JECN_FLOW_ORG_IMAGE       JF," + "       JECN_FLOW_ORG             JFO,"
                + "       JECN_FLOW_STRUCTURE_IMAGE_H T," + "       PROCESS_STATION_RELATED_H"
                + "   RS" + " WHERE JF.ORG_ID = JFO.ORG_ID" + "   AND JFO.DEL_STATE = 0 AND T.HISTORY_ID = RS.HISTORY_ID"
                + "   AND JF.FIGURE_TYPE = 'PositionFigure'" + "   AND JF.FIGURE_ID = RS.FIGURE_POSITION_ID"
                + "   AND RS.TYPE = '0'" + "   AND RS.FIGURE_FLOW_ID = T.FIGURE_ID" + "   AND T.FIGURE_TYPE IN "
                + SqlCommon.getRoleString() + " AND T.FLOW_ID = #{flowId}  AND T.HISTORY_ID = #{historyId}";
        return sql;
    }

    /**
     * 角色相关岗位组对应岗位
     *
     * @param map
     * @return
     */
    public String getRoleRelatedPostGroupSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            if (!ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.processFileShowPositionType)) {//true：岗位组，false ：岗位
                sql = "SELECT T.FIGURE_ID ROLE_ID, PG.NAME FIGURE_TEXT" + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " T"
                        + " INNER JOIN PROCESS_STATION_RELATED" + isPub + " RS"
                        + "    ON RS.FIGURE_FLOW_ID = T.FIGURE_ID" + "   AND RS.TYPE = '1'"
                        + "  LEFT JOIN JECN_POSITION_GROUP PG" + "    ON PG.ID = RS.FIGURE_POSITION_ID"
                        + " WHERE T.FLOW_ID =#{flowId}";
            } else {
                sql = "SELECT T.FIGURE_ID ROLE_ID, JF.FIGURE_ID AS POS_ID, JF.FIGURE_TEXT"
                        + "  FROM JECN_FLOW_ORG_IMAGE       JF," + "       JECN_FLOW_ORG             JFO,"
                        + "       JECN_FLOW_STRUCTURE_IMAGE" + isPub + " T," + "       PROCESS_STATION_RELATED" + isPub
                        + "   RS," + "       JECN_POSITION_GROUP_R     JPG" + " WHERE JF.ORG_ID = JFO.ORG_ID"
                        + "   AND JFO.DEL_STATE = 0" + "   AND JF.FIGURE_TYPE = 'PositionFigure'"
                        + "   AND JF.FIGURE_ID = JPG.FIGURE_ID" + "   AND JPG.GROUP_ID = RS.FIGURE_POSITION_ID"
                        + "   AND RS.TYPE = '1'" + "   AND RS.FIGURE_FLOW_ID = T.FIGURE_ID" + "   AND T.FIGURE_TYPE IN "
                        + SqlCommon.getRoleString() + "   AND T.FLOW_ID = #{flowId}";
            }
        } else {
            sql = getRoleRelatedPostGroupHistorySql(map);
        }
        return sql;
    }

    /**
     * 角色相关岗位组对应岗位 文控
     *
     * @param map
     * @return
     */
    public String getRoleRelatedPostGroupHistorySql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "";
        if (!ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.processFileShowPositionType)) {//true：岗位组，false ：岗位
            sql = "SELECT T.FIGURE_ID ROLE_ID, PG.NAME FIGURE_TEXT" + "  FROM JECN_FLOW_STRUCTURE_IMAGE_H T"
                    + " INNER JOIN PROCESS_STATION_RELATED_H  RS"
                    + "    ON RS.FIGURE_FLOW_ID = T.FIGURE_ID" + "   AND RS.TYPE = '1'"
                    + "  LEFT JOIN JECN_POSITION_GROUP PG" + "    ON PG.ID = RS.FIGURE_POSITION_ID"
                    + " WHERE  T.FLOW_ID = #{flowId}  AND T.HISTORY_ID = #{historyId}";
        } else {
            sql = "SELECT T.FIGURE_ID ROLE_ID, JF.FIGURE_ID AS POS_ID, JF.FIGURE_TEXT"
                    + "  FROM JECN_FLOW_ORG_IMAGE      JF," + "       JECN_FLOW_ORG             JFO,"
                    + "       JECN_FLOW_STRUCTURE_IMAGE_H  T," + "       PROCESS_STATION_RELATED_H"
                    + "   RS," + "       JECN_POSITION_GROUP_R     JPG" + " WHERE JF.ORG_ID = JFO.ORG_ID"
                    + "   AND JFO.DEL_STATE = 0  AND T.HISTORY_ID = RS.HISTORY_ID " + "   AND JF.FIGURE_TYPE = 'PositionFigure'"
                    + "   AND JF.FIGURE_ID = JPG.FIGURE_ID" + "   AND JPG.GROUP_ID = RS.FIGURE_POSITION_ID"
                    + "   AND RS.TYPE = '1'" + "   AND RS.FIGURE_FLOW_ID = T.FIGURE_ID" + "   AND T.FIGURE_TYPE IN "
                    + SqlCommon.getRoleString() + " AND T.FLOW_ID = #{flowId}  AND T.HISTORY_ID = #{historyId}";
        }
        return sql;
    }

    /**
     * 流程相关制度
     *
     * @param map
     * @return
     */
    public String getPorcessRelatedRuleSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT JR.ID, JR.RULE_NAME, JR.RULE_NUMBER,JR.IS_DIR" + "  FROM FLOW_RELATED_CRITERION" + isPub
                    + " T, JECN_RULE" + isPub + " JR" + " WHERE T.CRITERION_CLASS_ID = JR.ID"
                    + "   AND T.FLOW_ID =#{flowId} AND JR.DEL_STATE = 0";
        } else {
            sql = getPorcessRelatedRuleHistorySql(map);
        }
        return sql;
    }

    /**
     * 流程相关制度 文控
     *
     * @param map
     * @return
     */
    public String getPorcessRelatedRuleHistorySql(Map<String, Object> map) {
        String sql = "SELECT JR.ID, JR.RULE_NAME, JR.RULE_NUMBER,JR.IS_DIR" + "  FROM FLOW_RELATED_CRITERION_H"
                + " T, JECN_RULE_T JR" + " WHERE T.CRITERION_CLASS_ID = JR.ID   "
                + "   AND T.HISTORY_ID =#{historyId} AND T.FLOW_ID =#{flowId}  AND JR.DEL_STATE = 0";
        return sql;
    }

    /**
     * 流程文控信息
     *
     * @param map
     * @return
     */
    public String findDoucumentControlSql(Map<String, Object> map) {
        String sql = "SELECT HN.VERSION_ID," + "       HN.MODIFY_EXPLAIN," + "       HN.DRAFT_PERSON,"
                + "       HN.PUBLISH_DATE," + "       HF.NAME," + "       HF.APPELLATION"
                + "  FROM JECN_TASK_HISTORY_NEW HN" + " RIGHT JOIN JECN_TASK_HISTORY_FOLLOW HF"
                + "    ON HF.HISTORY_ID = HN.ID" + " WHERE RELATE_ID = #{flowId}" + "   AND TYPE = 0"
                + " ORDER BY HN.ID, HF.SORT";
        return sql;
    }

    /**
     * 流程KPI
     *
     * @param map
     * @return
     */
    public String findFlowKpiSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = getFlowKpiBaseInfo(isPub) + " WHERE T.FLOW_ID = #{flowId}";
        } else {
            sql = findFlowKpiHistorySql(map);
        }
        return sql;
    }

    /**
     * 流程KPI  文控
     *
     * @param map
     * @return
     */
    public String findFlowKpiHistorySql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT T.KPI_AND_ID," + "       T.FLOW_ID," + "       T.KPI_NAME," + "       T.KPI_TYPE,"
                + "       T.KPI_DEFINITION," + "       T.KPI_STATISTICAL_METHODS," + "       T.KPI_TARGET,"
                + "       T.KPI_HORIZONTAL," + "       T.KPI_VERTICAL," + "       T.CREAT_TIME,"
                + "       T.KPI_TARGET_OPERATOR," + "       T.KPI_DATA_METHOD," + "       T.KPI_DATA_PEOPEL_ID,"
                + "       T.KPI_RELEVANCE," + "       T.KPI_TARGET_TYPE," + "       T.FIRST_TARGER_ID,"
                + "       T.UPDATE_TIME," + "       T.CREATE_PEOPLE_ID," + "       T.UPDATE_PEOPLE_ID,"
                + "       JU.TRUE_NAME," + "       JFT.TARGER_CONTENT," + "       T.KPI_PURPOSE,"
                + "       T.KPI_POINT," + "       T.KPI_EXPLAIN," + "       T.KPI_PERIOD" + "  FROM JECN_FLOW_KPI_NAME_H  T"
                + "  LEFT JOIN JECN_USER JU ON T.KPI_DATA_PEOPEL_ID = JU.PEOPLE_ID"
                + "  LEFT JOIN JECN_FIRST_TARGER JFT ON T.FIRST_TARGER_ID = JFT.ID  WHERE T.HISTORY_ID = #{historyId} AND T.FLOW_ID = #{flowId}";
        return sql;
    }

    /**
     * 流程支持KPI对应支持工具
     *
     * @param map
     * @return
     * @throws Exception
     */
    public String findFlowToolsSql(Map<String, Object> map) throws Exception {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT DISTINCT T.KPI_AND_ID, ST.FLOW_SUSTAIN_TOOL_DESCRIBE" + "  FROM JECN_FLOW_KPI_NAME" + isPub
                    + " T" + " INNER JOIN JECN_FLOW_STRUCTURE" + isPub + " FST" + "    ON T.FLOW_ID = FST.FLOW_ID"
                    + " INNER JOIN JECN_SUSTAIN_TOOL_CONN" + isPub + " TOOL" + "    ON T.KPI_AND_ID = TOOL.RELATED_ID"
                    + "   AND TOOL.RELATED_TYPE = 0" + " INNER JOIN JECN_FLOW_SUSTAIN_TOOL ST"
                    + "    ON TOOL.SUSTAIN_TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID" + " WHERE FST.FLOW_ID = #{flowId}";
        } else {
            sql = findFlowToolsHistorySql(map);
        }
        return sql;
    }

    /**
     * 流程支持KPI对应支持工具  文控
     *
     * @param map
     * @return
     * @throws Exception
     */
    public String findFlowToolsHistorySql(Map<String, Object> map) throws Exception {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT DISTINCT T.KPI_AND_ID, ST.FLOW_SUSTAIN_TOOL_DESCRIBE" + "  FROM JECN_FLOW_KPI_NAME_H T"
                + " INNER JOIN JECN_FLOW_STRUCTURE_H FST" + "    ON T.FLOW_ID = FST.FLOW_ID  AND T.HISTORY_ID = FST.HISTORY_ID"
                + " INNER JOIN JECN_SUSTAIN_TOOL_CONN" + isPub + " TOOL" + "    ON T.KPI_AND_ID = TOOL.RELATED_ID"
                + "   AND TOOL.RELATED_TYPE = 0" + " INNER JOIN JECN_FLOW_SUSTAIN_TOOL ST"
                + "    ON TOOL.SUSTAIN_TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID" + " WHERE FST.FLOW_ID = #{flowId} AND FST.HISTORY_ID = #{historyId}";
        return sql;
    }

    /**
     * 流程记录
     *
     * @param map
     * @return
     */
    public String findFlowRecordSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT T.RECORD_NAME,T.RECORD_SAVE_PEOPLE,T.SAVE_LOCATION,T.FILE_TIME,T.SAVE_TIME,T.RECORD_APPROACH "
                    + "FROM JECN_FLOW_RECORD" + isPub + " T WHERE T.FLOW_ID = #{flowId}";
        } else {
            sql = findFlowRecordHistorySql(map);
        }
        return sql;
    }

    /**
     * 流程记录 文控
     *
     * @param map
     * @return
     */
    public String findFlowRecordHistorySql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        return "SELECT T.RECORD_NAME,T.RECORD_SAVE_PEOPLE,T.SAVE_LOCATION,T.FILE_TIME,T.SAVE_TIME,T.RECORD_APPROACH "
                + "FROM JECN_FLOW_RECORD_H T WHERE T.FLOW_ID = #{flowId} AND  T.HISTORY_ID = #{historyId}";
    }

    /**
     * 流程清单（以流程为主）
     *
     * @param map
     * @return
     */
    public String fetchInventory(Map<String, Object> map) {
        String isVersion = map.get("downloadType") == null ? null : map.get("downloadType").toString();
        if ("1".equals(isVersion)) {
            return getVersionInventorySql(map);
        } else if ("2".equals(isVersion)) {
            return getInventoryProcessMapSql(map);
        } else {
            return getInventorySql(map);
        }
    }

    private String getInventorySql(Map<String, Object> paramMap) {
        String sql = " select jfs.flow_id,jfs.pre_flow_id,jfs.flow_name,pub.pub_time,"
                + " jfs.flow_id_input,jfs.isflow,"
                + " jt.version_id,jfo.org_id,jfo.org_name,"
                + " case when t.type_res_people = 0 then ju.true_name"
                + " when t.type_res_people = 1 then jfoi.figure_text end as res_people,"
                + " case WHEN pub.flow_id is NULL then 0 else 1"
                + "	end as pub_state,jfs.t_level, gju.TRUE_NAME AS guardian_people,"
                + " jt.draft_person,jt.expiry,jt.NEXT_SCAN_DATE,jfs.sort_id,"
                + "		  case when pub.flow_id IS NULL and jtbn.state is not null THEN 1 " //审批中
                + "		  when pub.flow_id is not null then 2"//发布
                + "		  else 0 end as type_by_data,jfs.t_Path,jfs.CONFIDENTIALITY_LEVEL"//未发布 待建
                + "  from ("
                + InventorySqlProvider.getFlows(paramMap, 2) + ") jfs"
                + "  left join jecn_flow_basic_info_t t on t.flow_id = jfs.flow_id"
                + "  left join jecn_task_history_new jt on jt.id = jfs.history_id"
                + "  left join jecn_user ju on t.type_res_people = 0 and ju.people_id = t.res_people_id and ju.isLock=0"
                + "  left join jecn_flow_org_image jfoi on t.type_res_people = 1 and jfoi.figure_id=t.res_people_id"
                + "  left join jecn_flow_related_org jfro on t.flow_id=jfro.flow_id"
                + "  left join jecn_flow_org jfo on jfro.org_id = jfo.org_id  and jfo.del_state = 0"
                + "  left join jecn_flow_structure pub on pub.flow_id = jfs.flow_id"
                + "  left join jecn_task_bean_new jtbn on jtbn.task_type = 0"
                + "                                   and jtbn.r_id = jfs.flow_id"
                + "                                   and jtbn.is_lock = 1"
                + "                                   and jtbn.state <> 5"
                + "  left join jecn_user gju on gju.people_id = t.Ward_People_Id"
                + "  where jfs.del_state = 0"
                + " order by jfs.view_sort";
        return sql;
    }

    private String getInventoryProcessMapSql(Map<String, Object> paramMap) {
        String sql = "SELECT JFS.FLOW_ID," +
                "       JFS.FLOW_NAME," +
                "       JFS.ISFLOW," +
                "       JFS.PRE_FLOW_ID," +
                "       JFS.T_LEVEL," +
                "       JFO.ORG_ID," +
                "       JFO.ORG_NAME," +
                "       CASE" +
                "         WHEN T.TYPE_RES_PEOPLE = 0 AND JFS.ISFLOW=0 THEN" +
                "          JU.TRUE_NAME" +
                "         WHEN T.TYPE_RES_PEOPLE = 1 AND JFS.ISFLOW=0 THEN" +
                "          JFOI.FIGURE_TEXT" +
                "         WHEN JFBI.TYPE_RES_PEOPLE = 0 AND JFS.ISFLOW=1 THEN" +
                "          JU_FLOW.TRUE_NAME" +
                "         WHEN JFBI.TYPE_RES_PEOPLE = 1 AND JFS.ISFLOW=1 THEN" +
                "          JFOI_FLOW.FIGURE_TEXT" +
                "       END AS RES_PEOPLE," +
                "       CASE" +
                "         WHEN JFI.FLOW_ID IS NOT NULL THEN" +
                "          1" +
                "         ELSE" +
                "          0" +
                "       END PUB_STATE," +
                "       CASE" +
                "         WHEN JFI.FLOW_ID IS NULL AND TAB.STATE IS NOT NULL THEN" +
                "          1" +
                "         WHEN JFI.FLOW_ID IS NOT NULL THEN" +
                "          2" +
                "         ELSE" +
                "          0" +
                "       END AS TYPE_BY_DATA," +
                "       JFS.T_PATH,JFS.flow_id_input" +
                "  FROM ( " + SqlCommon.getFlowChildObjectsSqlCommon(Long.valueOf(paramMap.get("flowId").toString()), Long.valueOf(paramMap.get("projectId").toString())) + " ) JFS" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE JFI" +
                "    ON JFS.FLOW_ID = JFI.FLOW_ID" +
                "   AND JFI.DEL_STATE = 0" +
                "  LEFT JOIN JECN_TASK_BEAN_NEW TAB" +
                "    ON TAB.TASK_TYPE in (0,4)" +
                "   AND TAB.R_ID = JFS.FLOW_ID" +
                "   AND TAB.IS_LOCK = 1" +
                "   AND TAB.STATE <> 5" +
                "  LEFT JOIN JECN_MAIN_FLOW_T T" +
                "    ON T.FLOW_ID = JFS.FLOW_ID AND JFS.ISFLOW=0" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON T.TYPE_RES_PEOPLE = 0" +
                "   AND JU.PEOPLE_ID = T.RES_PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI" +
                "    ON T.TYPE_RES_PEOPLE = 1" +
                "   AND JFOI.FIGURE_ID = T.RES_PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_BASIC_INFO_T JFBI ON JFBI.FLOW_ID=JFS.FLOW_ID AND JFS.ISFLOW=1" +
                "  LEFT JOIN JECN_USER JU_FLOW  ON JFBI.TYPE_RES_PEOPLE = 0" +
                "   AND JU_FLOW.PEOPLE_ID = JFBI.RES_PEOPLE_ID" +
                "   AND JU_FLOW.ISLOCK = 0" +
                "   LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI_FLOW" +
                "    ON JFBI.TYPE_RES_PEOPLE = 1" +
                "   AND JFOI_FLOW.FIGURE_ID = JFBI.RES_PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_RELATED_ORG_T JFRO" +
                "    ON JFS.FLOW_ID = JFRO.FLOW_ID" +
                "  LEFT JOIN JECN_FLOW_ORG JFO" +
                "    ON JFRO.ORG_ID = JFO.ORG_ID" +
                "   AND JFO.DEL_STATE = 0" +
                "   ORDER BY JFS.VIEW_SORT";
        return sql;
    }


    private String getVersionInventorySql(Map<String, Object> paramMap) {

        String sql = "select jfs.flow_id," +
                "       jfs.pre_flow_id," +
                "       case" +
                "         when pub.flow_name is null then" +
                "          jfs.flow_name" +
                "         else" +
                "          pub.flow_name" +
                "       end flow_name," +
                "       case" +
                "         when pub.pub_time is null then" +
                "          jfs.pub_time" +
                "         else" +
                "          pub.pub_time" +
                "       end pub_time," +
                "       case" +
                "         when pub.flow_id_input is null then" +
                "          jfs.flow_id_input" +
                "         else" +
                "          pub.flow_id_input" +
                "       end flow_id_input," +
                "       jfs.isflow," +
                "       jt.version_id," +
                "       jfo.org_id," +
                "       jfo.org_name," +
                "       case" +
                "         when t.type_res_people = 0 then" +
                "          ju.true_name" +
                "         when t.type_res_people = 1 then" +
                "          jfoi.figure_text" +
                "       end as res_people," +
                "       case" +
                "         when FST.flow_id is null then" +
                "          0" +
                "         else" +
                "          1" +
                "       end as pub_state," +
                "       jfs.t_level," +
                "       gju.true_name as guardian_people," +
                "       jt.draft_person," +
                "       jt.expiry," +
                "       jt.next_scan_date," +
                "       jfs.sort_id," +
                "       case" +
                "         when pub.flow_id is null and jtbn.state is not null then" +
                "          1" +
                "         when pub.flow_id is not null then" +
                "          2" +
                "         else" +
                "          0" +
                "       end as type_by_data," +
                "       jfs.t_path,PUB.CONFIDENTIALITY_LEVEL" +
                "  from  (" + InventorySqlProvider.getFlows(paramMap, 2) + ") JFS" +
                "   LEFT JOIN JECN_FLOW_STRUCTURE FST " +
                "   ON FST.FLOW_ID = JFS.FLOW_ID " +
                "   LEFT JOIN JECN_FLOW_STRUCTURE_H PUB" +
                "    ON PUB.FLOW_ID = JFS.FLOW_ID" +
                "  LEFT JOIN JECN_TASK_HISTORY_NEW JT" +
                "    ON JT.ID = PUB.HISTORY_ID" +
                "  LEFT JOIN JECN_FLOW_BASIC_INFO_H T" +
                "    ON T.FLOW_ID = JFS.FLOW_ID" +
                "   AND T.HISTORY_ID = JT.ID" +
                "  LEFT JOIN JECN_USER JU" +
                "    ON T.TYPE_RES_PEOPLE = 0" +
                "   AND JU.PEOPLE_ID = T.RES_PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI" +
                "    ON T.TYPE_RES_PEOPLE = 1" +
                "   AND JFOI.FIGURE_ID = T.RES_PEOPLE_ID" +
                "  LEFT JOIN JECN_FLOW_RELATED_ORG_T JFRO" +
                "    ON T.FLOW_ID = JFRO.FLOW_ID" +
                "  LEFT JOIN JECN_FLOW_ORG JFO" +
                "    ON JFRO.ORG_ID = JFO.ORG_ID" +
                "   AND JFO.DEL_STATE = 0" +
                "  LEFT JOIN JECN_TASK_BEAN_NEW JTBN" +
                "    ON JTBN.TASK_TYPE = 0" +
                "   AND JTBN.R_ID = JFS.FLOW_ID" +
                "   AND JTBN.IS_LOCK = 1" +
                "   AND JTBN.STATE <> 5" +
                "  LEFT JOIN JECN_USER GJU" +
                "    ON GJU.PEOPLE_ID = T.WARD_PEOPLE_ID" +
                " WHERE JFS.DEL_STATE = 0" +
                " ORDER BY JFS.VIEW_SORT";
        return sql;
    }

    /**
     * 流程体系清单
     *
     * @param paramMap
     * @return
     */
    public String fetchInventoryFlowSystem(Map<String, Object> paramMap) {
        Long flowId = Long.valueOf(paramMap.get("flowId").toString());
        Long projectId = Long.valueOf(paramMap.get("projectId").toString());
        String sql = " select distinct jfs.flow_id,jfs.flow_name,"
                + "       jfs.flow_id_input,jfs.isflow,"
                + "       case when pub.flow_id is null and jtbn.id is null then 0"
                + "       when pub.flow_id is null and jtbn.id is not null then 1 "
                + "       when pub.flow_id is not null then 2 end as is_pub,jfs.pub_time,jt.version_id,jfs.pre_flow_id,"
                + "       jfs.t_level,jfs.sort_id,jfs.t_path from ("
                + SqlCommon.getFlowChildObjectsSqlCommon(flowId, projectId) + ") jfs"
                + "  left join jecn_task_history_new jt on jt.id = jfs.history_id"
                + "  left join jecn_flow_structure pub on pub.flow_id = jfs.flow_id"
                + "  left join jecn_task_bean_new jtbn on jtbn.task_type in (0,4)"
                + "                                   and jtbn.r_id = jfs.flow_id"
                + "                                   and jtbn.is_lock = 1"
                + "                                   and jtbn.state <> 5"
                + "  where jfs.del_state = 0  order by jfs.pre_flow_id, jfs.sort_id,jfs.flow_id";
        return sql;
    }


    /**
     * 流程架构，文件说明
     *
     * @param map
     * @return
     */
    public String findProcessMapFileSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT T.FLOW_AIM," + "       T.FLOW_AREA," + "       T.FLOW_NOUN_DEFINE,"
                + "       T.FLOW_INPUT," + "       T.FLOW_OUTPUT," + "       T.FLOW_SHOW_STEP," + "       JF.FILE_NAME,"
                + "       JF.FILE_ID" + "  FROM JECN_MAIN_FLOW" + isPub + " T" + "  LEFT JOIN JECN_FILE" + isPub + " JF"
                + "    ON JF.FILE_ID = T.FILE_ID" + " WHERE T.FLOW_ID = #{flowId}";
        return sql;
    }

    /**
     * 流程搜索
     *
     * @param map
     * @return
     */
    public String flowSearch(Map<String, Object> map) {
        return this.flowSearchCommon(map, false);
    }

    /**
     * 流程架构搜索 总数
     *
     * @param map
     * @return
     */
    public String flowMapSearchTotal(Map<String, Object> map) {
        return this.flowMapSearchCommon(map, true);
    }

    /**
     * 流程架构搜索
     *
     * @param map
     * @return
     */
    public String flowMapSearch(Map<String, Object> map) {
        return this.flowMapSearchCommon(map, false);
    }

    /**
     * 流程搜索 总数
     *
     * @param map
     * @return
     */
    public String flowSearchTotal(Map<String, Object> map) {
        return this.flowSearchCommon(map, true);
    }

    /**
     * 获得流程架构搜索的公共sql
     *
     * @param map
     * @param isCount
     * @return
     */
    public String flowMapSearchCommon(Map<String, Object> map, boolean isCount) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchBean searchBean = (SearchBean) map.get("searchBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        // 是不是搜索所有流程节点
        boolean isAll = false;
        if (Contants.isMainFlowAdmin || isAdmin) {
            isAll = true;
        }
        String sql = "select distinct t.flow_id,t.flow_name,t.flow_id_input,t.pre_flow_id,pflow.flow_name as p_flow_name,t.is_public,t.pub_time";
        if (isCount) {
            sql = "select count(distinct t.flow_id)";
        }

        sql = sql + " from jecn_flow_structure t";
        if (!isAll) {
            map.put("typeAuth", AuthSqlConstant.TYPEAUTH.FLOW);
            sql = sql + " Inner join " + CommonSqlTPath.INSTANCE.getFileSearch(map)
                    + " on MY_AUTH_FILES.Flow_id=t.flow_id";
        }
        sql = sql + " left join jecn_flow_structure pflow on pflow.flow_id = t.pre_flow_id"
                + " where t.isflow=0 and t.del_state=0 and t.projectid=" + projectId;

        // 流程名称
        if (StringUtils.isNotBlank(searchBean.getName())) {
            sql = sql + " and t.flow_name like '%" + searchBean.getName() + "%'";
        }
        // 制度编号
        if (StringUtils.isNotBlank(searchBean.getNumber())) {
            sql = sql + " and t.flow_id_input like '%" + searchBean.getNumber() + "%'";
        }
        // 密级
        if (searchBean.getSecret() != -1) {
            sql = sql + " and t.is_public=" + searchBean.getSecret();
        }
        return sql;
    }

    /**
     * 流程搜索 通用bean
     *
     * @param map
     * @param isCount
     * @return
     */
    private String flowSearchCommon(Map<String, Object> map, boolean isCount) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchBean searchBean = (SearchBean) map.get("searchBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        // 是不是搜索所有流程节点
        boolean isAll = false;
        if (isAdmin) {
            isAll = true;
        } else if (Contants.isFlowAdmin && searchBean.getFileType() == 1) {
            isAll = true;
        } else if (Contants.isMainFlowAdmin && searchBean.getFileType() == 0) {
            isAll = true;
        }
        String sql = "select distinct t.flow_id,t.flow_name,t.flow_id_input,org.org_id,org.org_name"
                + "       ";
        if (searchBean.getFileType() == 0) {
            sql += " ,jmf.type_res_people,jmf.res_people_id,case when t.isflow=0 and jmf.type_res_people = 0  then" +
                    "                  jum.true_name" +
                    "                 when t.isflow=0 and jmf.type_res_people = 1 then" +
                    "                  jfoim.figure_text" +
                    "               end as res_people";
        } else if (searchBean.getFileType() == 1) {
            sql += " ,jfbi.type_res_people,jfbi.res_people_id,case when t.isflow=1 and jfbi.type_res_people = 0  then" +
                    "                  ju.true_name" +
                    "                 when t.isflow=1 and jfbi.type_res_people = 1 then" +
                    "                  jfoi.figure_text" +
                    "               end as res_people";
        }
        sql += " ,t.pub_time,t.is_public";
        if (isCount) {
            sql = "select count(distinct t.flow_id)";
        }
        sql = sql + " from jecn_flow_structure t "
                + " left join jecn_flow_related_org jfro on jfro.flow_id=t.flow_id"
                + " left join jecn_flow_org org on jfro.org_id=org.org_id and org.del_state=0";

        if (!isCount) {
            if (searchBean.getFileType() == 0) {
                sql = sql + " left join jecn_main_flow jmf on t.isflow=0 and jmf.flow_id = t.flow_id"
                        + " left join jecn_flow_org_image jfoim on jmf.type_res_people = 1 and jfoim.figure_id = jmf.res_people_id"
                        + " left join jecn_user jum on jmf.type_res_people = 0 and jum.islock = 0 and jum.people_id = jmf.res_people_id";
            } else if (searchBean.getFileType() == 1) {
                sql = sql + " left join jecn_flow_basic_info jfbi on jfbi.flow_id=t.flow_id"
                        + " left join jecn_flow_org_image jfoi on jfbi.type_res_people=1 and jfoi.figure_id=jfbi.res_people_id"
                        + " left join jecn_user ju on jfbi.type_res_people=0 and ju.islock=0 and ju.people_id=jfbi.res_people_id";
            }

        }
        if (!isAll) {
            map.put("typeAuth", AuthSqlConstant.TYPEAUTH.FLOW);
            sql = sql + " Inner join " + CommonSqlTPath.getFlowAuthorityNodes(map) + " on MY_AUTH_FILES.Flow_id=t.flow_id";
        }
        sql += " where t.del_state=0 and t.projectid=" + projectId;
        // 流程名称
        if (StringUtils.isNotBlank(searchBean.getName())) { // 编号  名称 统一关键字 搜索
            sql = sql + " and (t.flow_name like '%" + searchBean.getName() + "%'" + " or t.flow_id_input like '%" + searchBean.getName() + "%' or  t.keyword like '%" + searchBean.getName() + "%')";
        }
        // 责任部门
        if (searchBean.getOrgId() != null) {
            sql = sql + " and org.org_id=" + searchBean.getOrgId();
        }
        // 密级
        if (searchBean.getSecret() != -1) {
            sql = sql + " and t.is_public=" + searchBean.getSecret();
        }
        if (searchBean.getFileType() != -1) {
            sql += " and t.isflow=" + searchBean.getFileType();
        }
        return sql;
    }

    /**
     * 流程架构相关制度
     *
     * @param map
     * @return
     */
    public String findFlowMapRelatedFileSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long processId = Long.valueOf(map.get("processId").toString());
        return "SELECT R.ID id," +
                "       R.RULE_NAME name" +
                "  FROM FLOW_RELATED_CRITERION RS, JECN_RULE" + isPub + " R" +
                " WHERE RS.FLOW_CRITERON_ID = R.ID AND R.DEL_STATE=0" +
                "   AND RS.FLOW_ID = " + processId;
    }

    /**
     * 流程操作模板
     *
     * @param map
     * @return
     */
    public String findOperationModelSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        String sql = "SELECT FSI.FIGURE_ID,FSI.Figure_Text," +
                "       FSI.ACTIVITY_ID, FSI.ACTIVITY_OUTPUT, FSI.ACTIVITY_INPUT," +
                "       F1.FILE_ID      IN_FILE_ID," +
                "       F1.FILE_NAME    IN_FILE_NAME," +
                "       F2.FILE_ID      OPER_FILE_ID," +
                "       F2.FILE_NAME    OPER_FILE_NAME," +
                "       F3.FILE_ID      OUT_FILE_ID," +
                "       F3.FILE_NAME    OUT_FILE_NAME," +
                "       F4.FILE_ID      TMP_FILE_ID," +
                "       F4.FILE_NAME    TMP_FILE_NAME" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " FSI" +
                "  LEFT JOIN JECN_ACTIVITY_FILE" + isPub + " IN_F" +
                "    ON IN_F.FIGURE_ID = FSI.FIGURE_ID" +
                "   AND IN_F.FILE_TYPE = 0" +
                "  LEFT JOIN JECN_FILE" + isPub + " F1" +
                "    ON F1.FILE_ID = IN_F.FILE_S_ID" +
                "  LEFT JOIN JECN_ACTIVITY_FILE" + isPub + " OPER_F" +
                "    ON OPER_F.FIGURE_ID = FSI.FIGURE_ID" +
                "   AND OPER_F.FILE_TYPE = 1" +
                "  LEFT JOIN JECN_FILE" + isPub + " F2" +
                "    ON F2.FILE_ID = OPER_F.FILE_S_ID" +
                "  LEFT JOIN JECN_MODE_FILE" + isPub + " MF" +
                "    ON MF.FIGURE_ID = FSI.FIGURE_ID" +
                "  LEFT JOIN JECN_FILE" + isPub + " F3" +
                "    ON F3.FILE_ID = MF.FILE_M_ID" +
                "  LEFT JOIN JECN_TEMPLET" + isPub + " JT" +
                "    ON JT.MODE_FILE_ID = MF.MODE_FILE_ID" +
                "  LEFT JOIN JECN_FILE" + isPub + " F4" +
                "    ON F4.FILE_ID = JT.FILE_ID" +
                " WHERE FSI.FLOW_ID = #{flowId}" +
                "   AND FSI.FIGURE_TYPE IN" + SqlCommon.getActiveString();
        return sql;
    }

    /**
     * 更新流程基本信息表
     *
     * @param paramMap
     * @return
     */
    public String updateFlowBasicInfo(Map<String, Object> paramMap) {

        FlowBasicInfoT basicInfo = (FlowBasicInfoT) paramMap.get("obj");
        String pub = paramMap.get("pub").toString();
        String sql = "update JECN_FLOW_BASIC_INFO" + pub + " set TYPE_ID=" + basicInfo.getTypeId() + " where flow_id=" + basicInfo.getFlowId();
        return sql;
    }

    public String fetchProcessesSupportResources(Map<String, Object> paramMap) {
        String pub = Utils.getPubByParams(paramMap);
        String flows = "with flows as (select jfs.* from JECN_FLOW_STRUCTURE" + pub + " jfs where jfs.flow_id=#{id} and jfs.isflow=1)";
        return getFlowAllRelatedFiles(flows, paramMap);
    }

    //文控
    public String fetchProcessesSupportHistoryResources(Map<String, Object> paramMap) {
        String pub = Utils.getPubByParams(paramMap);
        String flows = "with flows as (select jfs.* from JECN_FLOW_STRUCTURE_H jfs where jfs.HISTORY_ID=#{historyId} and jfs.isflow=1)";
        return getFlowAllHistoryRelatedFiles(flows, paramMap);
    }

    //文控
    public static String getFlowAllHistoryRelatedFiles(String flows, Map<String, Object> paramMap) {
        String pubStateSql = "1 as is_public";
        String sql = flows + "select id,name,type,r_id from ( select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedTemplate(paramMap) + ")a"
                + " union "
                + " select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedOperation(paramMap) + ")a"
                + " union "
                + " select distinct a.id as id,a.rule_name as name,'RULE' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedRule(paramMap) + ")a"
                + " union "
                + " select distinct a.id as id,a.name as name,'RISK' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedRisk(paramMap) + ")a"
                + " union"
                + " select distinct a.id as id,a.name as name,'RISK' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedActivityRisk(paramMap) + ")a"
                + " union "
                + " select distinct a.criterion_class_id as id,a.criterion_class_name as name,'STANDARD' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedStandard(paramMap) + ")a"
                + " union "
                + " select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedStandardfile(paramMap) + ")a"
                + " union "
                + " select distinct a.id as id,a.name as name,'PROCESS' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowHistoryRelatedFlows(paramMap) + ")a"
                + " )a order by type";
        return sql;
    }

    public static String getFlowAllRelatedFiles(String flows, Map<String, Object> paramMap) {
        String pubStateSql = "1 as is_public";
        String pub = Utils.getPubByParams(paramMap);
        if ("_T".equals(pub)) {
            pubStateSql = "0 as is_public";
        }
        String sql = flows + "select id,name,type,r_id,standardFileId from ( select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id as r_id,-1 as standardFileId, " + pubStateSql + " from (" + getFlowRelatedTemplate(paramMap) + ")a"
                + " union "
                + " select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id  as r_id,-1 as standardFileId," + pubStateSql + " from (" + getFlowRelatedOperation(paramMap) + ")a"
                + " union "
                + " select distinct a.id as id,a.rule_name as name,'RULE' as type,a.flow_id  as r_id,-1 as standardFileId, " + pubStateSql + " from (" + getFlowRelatedRule(paramMap) + ")a"
                + " union "
                + " select distinct a.id as id,a.name as name,'RISK' as type,a.flow_id  as r_id,-1 as standardFileId, " + pubStateSql + " from (" + getFlowRelatedRisk(paramMap) + ")a"
                + " union"
                + " select distinct a.id as id,a.name as name,'RISK' as type,a.flow_id  as r_id,-1 as standardFileId, " + pubStateSql + " from (" + getFlowRelatedActivityRisk(paramMap) + ")a"
                + " union "
                + " select distinct a.criterion_class_id as id,a.criterion_class_name as name,'STANDARD' as type,a.flow_id  as r_id, a.version_id as standardFileId," + pubStateSql + " from (" + getFlowRelatedStandard(paramMap) + ")a"
                + " union "
                + " select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id  as r_id,-1 as standardFileId, " + pubStateSql + " from (" + getFlowRelatedStandardfile(paramMap) + ")a"
                + " union "
                + " select distinct a.id as id,a.name as name,'PROCESS' as type,a.flow_id  as r_id,-1 as standardFileId ," + pubStateSql + " from (" + getFlowRelatedFlows(paramMap) + ")a"
                + " )a order by type";
        return sql;
    }

    public static String getFlowMapAllRelatedFiles(String flows, Map<String, Object> paramMap) {
        Long historyId = getHistoryId(paramMap);
        String sql = "";
        if (historyId == null || historyId == 0) {
            String pubStateSql = "1 as is_public";
            String pub = Utils.getPubByParams(paramMap);
            if ("_T".equals(pub)) {
                pubStateSql = "0 as is_public";
            }
            sql = flows + " select id,name,type,r_id,is_public from ( "
                    + " select distinct a.ID as id,a.RULE_NAME as name,'RULE' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowRelatedRule(paramMap) + ")a"
                    + " union "
                    + " select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id  as r_id," + pubStateSql + " from (" + getFlowRelatedStandardfile(paramMap) + ")a"
                    + " )a order by type";
        } else {
            sql = flows + " select id,name,type,r_id,is_public from ( "
                    + " select distinct a.ID as id,a.RULE_NAME as name,'RULE' as type,a.flow_id  as r_id, 1 as is_public from (" + getFlowHistoryRelatedRule(paramMap) + ")a"
                    + " union "
                    + " select distinct a.file_id as id,a.file_name as name,'FILE' as type,a.flow_id  as r_id,  1 as is_public from (" + getFlowHistoryRelatedStandardfile(paramMap) + ")a"
                    + " )a order by type";
        }
        return sql;
    }

    public String getFlowKpiMapSql(Map<String, Object> paramMap) {
        String isPub = (String) paramMap.get("isPub");
        String sql = getFlowKpiBaseInfo(isPub) + " WHERE T.KPI_AND_ID = #{kpiAndId}";
        return sql;
    }

    private String getFlowKpiBaseInfo(String isPub) {
        String sql = "SELECT T.KPI_AND_ID," + "       T.FLOW_ID," + "       T.KPI_NAME," + "       T.KPI_TYPE,"
                + "       T.KPI_DEFINITION," + "       T.KPI_STATISTICAL_METHODS," + "       T.KPI_TARGET,"
                + "       T.KPI_HORIZONTAL," + "       T.KPI_VERTICAL," + "       T.CREAT_TIME,"
                + "       T.KPI_TARGET_OPERATOR," + "       T.KPI_DATA_METHOD," + "       T.KPI_DATA_PEOPEL_ID,"
                + "       T.KPI_RELEVANCE," + "       T.KPI_TARGET_TYPE," + "       T.FIRST_TARGER_ID,"
                + "       T.UPDATE_TIME," + "       T.CREATE_PEOPLE_ID," + "       T.UPDATE_PEOPLE_ID,"
                + "       JU.TRUE_NAME," + "       JFT.TARGER_CONTENT," + "       T.KPI_PURPOSE,"
                + "       T.KPI_POINT," + "       T.KPI_EXPLAIN," + "       T.KPI_PERIOD" + "  FROM JECN_FLOW_KPI_NAME"
                + isPub + " T" + "  LEFT JOIN JECN_USER JU ON T.KPI_DATA_PEOPEL_ID = JU.PEOPLE_ID"
                + "  LEFT JOIN JECN_FIRST_TARGER JFT ON T.FIRST_TARGER_ID = JFT.ID";
        return sql;
    }

    public String fetchFlowSustainToolByKpiId(Map<String, Object> params) {
        String pub = params.get("pub").toString();
        String sql = "SELECT" +
                "		JFST.*" +
                "	FROM" +
                "		Jecn_Flow_Sustain_Tool jfst" +
                "	INNER JOIN JECN_SUSTAIN_TOOL_CONN" + pub + " jst ON jfst.flow_sustain_tool_id = jst.SUSTAIN_TOOL_ID" +
                "	AND jst.RELATED_TYPE = 0" +
                "	AND jst.RELATED_ID =#{ kpiId }";
        return sql;
    }

    public String getTermDefinesSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT distinct t.id,T.INSTRUCTIONS instructions,FST.FIGURE_TEXT name" +
                    "  FROM TERM_DEFINITION_LIBRARY T" +
                    "  LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE" + isPub + " FST" +
                    "    ON T.ID = FST.LINK_FLOW_ID" +
                    "   AND FST.FIGURE_TYPE = 'TermFigureAR'" +
                    "   AND FST.FLOW_ID = #{flowId}" +
                    " WHERE FST.FIGURE_ID IS NOT NULL";
        } else {
            sql = getTermDefinesHistorySql(map);
        }
        return sql;
    }

    /**
     * 术语文控
     *
     * @param map
     * @return
     */
    public String getTermDefinesHistorySql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        String sql = "SELECT distinct t.id,T.INSTRUCTIONS instructions,FST.FIGURE_TEXT name" +
                "  FROM TERM_DEFINITION_LIBRARY T" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE_H FST" +
                "    ON T.ID = FST.LINK_FLOW_ID" +
                "   AND FST.FIGURE_TYPE = 'TermFigureAR'" +
                "   AND FST.HISTORY_ID = #{historyId} AND FST.FLOW_ID = #{flowId} " +
                " WHERE FST.FIGURE_ID IS NOT NULL";
        return sql;
    }

    public String getInterfaceDescriptionSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT  JFSI.FIGURE_ID id," +
                    "       JFSI.FIGURE_TEXT name," +
                    "       JFSI.IMPL_TYPE implType," +
                    "       JFSI.IMPL_NAME implName," +
                    "       JFSI.IMPL_NOTE implNote," +
                    "       JFSI.PROCESS_REQUIREMENTS processRequirements," +
                    "       JFS.FLOW_ID rid," +
                    "       JFS.FLOW_NAME rname" +
                    "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JFSI" +
                    " INNER JOIN JECN_FLOW_STRUCTURE_T JFS" +
                    "    ON JFSI.LINK_FLOW_ID = JFS.FLOW_ID" +
                    " WHERE JFSI.FLOW_ID = #{flowId}" +
                    "   AND JFSI.IMPL_TYPE IN (1, 2, 3, 4)  and JFS.Del_State = 0 order by jfsi.x_point,jfsi.y_point asc";
        } else {
            sql = getInterfaceDescriptionHistorySql(map);
        }
        return sql;
    }

    /**
     * 流程接口描述 文控
     *
     * @param map
     * @return
     */
    public String getInterfaceDescriptionHistorySql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        return "SELECT  JFSI.FIGURE_ID id," +
                "       JFSI.FIGURE_TEXT name," +
                "       JFSI.IMPL_TYPE implType," +
                "       JFSI.IMPL_NAME implName," +
                "       JFSI.IMPL_NOTE implNote," +
                "       JFSI.PROCESS_REQUIREMENTS processRequirements," +
                "       JFS.FLOW_ID rid," +
                "       JFS.FLOW_NAME rname" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JFSI" +
                " INNER JOIN JECN_FLOW_STRUCTURE_H JFS" +
                "    ON JFSI.LINK_FLOW_ID = JFS.FLOW_ID  AND JFSI.HISTORY_ID = JFS.HISTORY_ID" +
                " WHERE JFSI.HISTORY_ID = #{historyId} AND  JFSI.FLOW_ID = #{flowId}" +
                "   AND JFSI.IMPL_TYPE IN (1, 2, 3, 4)  and JFS.Del_State = 0  order by jfsi.x_point,jfsi.y_point asc";

    }

    public String getFlowSustainToolSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        String sql = "SELECT JFST.FLOW_SUSTAIN_TOOL_DESCRIBE" +
                "  FROM JECN_FLOW_SUSTAIN_TOOL JFST" +
                " INNER JOIN JECN_FLOW_TOOL_RELATED" + isPub + " T" +
                "    ON T.FLOW_SUSTAIN_TOOL_ID = JFST.FLOW_SUSTAIN_TOOL_ID" +
                " WHERE T.FLOW_ID = #{flowId}";
        return sql;
    }

    public String findFlowMapRelatedFile(Map<String, Object> paramMap) {
        String pub = Utils.getPubByParams(paramMap);
        Long historyId = getHistoryId(paramMap);
        String flows = "";
        if (historyId == null || historyId == 0) {
            flows = "with flows as (select flow_id,flow_name from JECN_FLOW_STRUCTURE" + pub + " where flow_id=#{id})";
        } else {
            flows = "with flows as (select flow_id,flow_name,HISTORY_ID from JECN_FLOW_STRUCTURE_H where flow_id=#{id} AND HISTORY_ID = #{historyId})";
        }
        return getFlowMapAllRelatedFiles(flows, paramMap);
    }

    public String findRelatedStandardFilesSql(Map<String, Object> paramMap) {
        String pub = paramMap.get("isPub").toString();
        Long historyId = getHistoryId(paramMap);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "	SELECT distinct" +
                    "		B.VERSION_ID AS ID," +
                    "		B.FILE_NAME AS NAME, " +
                    "      B.DOC_ID AS docId" +
                    "	FROM FLOW_STANDARDIZED_FILE" + pub + " A " +
                    "	INNER JOIN JECN_FILE" + pub + " B ON A.FILE_ID = B.FILE_ID AND B.del_state = 0" +
                    "   where a.flow_id=#{flowId}";
        } else {
            sql = findRelatedStandardFilesHistorySql(paramMap);
        }
        return sql;
    }

    /**
     * 流程标准化文件文控
     *
     * @param paramMap
     * @return
     */
    public String findRelatedStandardFilesHistorySql(Map<String, Object> paramMap) {
        String pub = paramMap.get("isPub").toString();
        String sql = "	SELECT distinct" +
                "		B.ID," +
                "		B.FILE_NAME AS NAME, " +
                "      '' AS docId" +
                "	FROM FLOW_STANDARDIZED_FILE_H A " +
                "	INNER JOIN FILE_CONTENT B ON A.FILE_ID = B.ID" +
                "   where A.HISTORY_ID=#{historyId} AND A.FLOW_ID = #{flowId}";
        return sql;
    }

    public String fetchInventoryParent(Map<String, Object> paramMap) {
        String pub = Utils.getPubByParams(paramMap);
        String sql = " select p.flow_id,p.pre_flow_id,p.flow_name,p.flow_id_input,p.isflow from jecn_flow_structure" + pub + " c " +
                " inner join jecn_flow_structure" + pub + " p on c.flow_id=#{flowId} and c.flow_id<>p.flow_id and c.t_path is not null and p.t_path is not null and c.t_path like p.t_path" + BaseSqlProvider.getConcatChar() + "'%'";
        return sql;
    }

    public String getSubFlows(Map<String, Object> paramMap) {
        String pub = Utils.getPubByParams(paramMap);
        String sql = "SELECT JFS.FLOW_ID,JFS.FLOW_NAME,JFS.ISFLOW FROM JECN_FLOW_STRUCTURE" + pub + " JFS WHERE JFS.DEL_STATE = 0 AND  JFS.PRE_FLOW_ID=#{id} order by jfs.sort_id asc";
        return sql;
    }

    /**
     * 流程适应部门
     *
     * @param map
     * @return
     */
    public String findFlowApplyOrgNamesSql(Map<String, Object> map) {
        String pub = map.get("isPub").toString();
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "SELECT O.ORG_NAME" +
                    "  FROM JECN_FLOW_ORG O" +
                    " INNER JOIN JECN_FLOW_APPLY_ORG" + pub + " AO" +
                    "    ON O.ORG_ID = AO.ORG_ID" +
                    "   AND O.DEL_STATE = 0" +
                    " WHERE AO.RELATED_ID = #{flowId}";
        } else {
            sql = findFlowApplyOrgNamesHistorySql(map);
        }
        return sql;
    }

    /**
     * 流程适应部门  文控
     *
     * @param map
     * @return
     */
    public String findFlowApplyOrgNamesHistorySql(Map<String, Object> map) {
        return "SELECT O.ORG_NAME" +
                "  FROM JECN_FLOW_ORG O" +
                " INNER JOIN JECN_FLOW_APPLY_ORG_H AO" +
                "    ON O.ORG_ID = AO.ORG_ID" +
                "   AND O.DEL_STATE = 0" +
                " WHERE  AO.RELATED_ID = #{flowId} AND AO.HISTORY_ID = #{historyId}";
    }

    /**
     * 获取流程相关附件（表单样例）
     *
     * @param map
     * @return
     */
    public String findRelatedFilesSql(Map<String, Object> map) {
        String pub = map.get("isPub").toString();
        String sql = "";
        if (ConfigUtils.useNewInout()) {
            sql = findNewRelatedFilesSql(pub);
        } else {
            sql = findOldRelatedFilesSql(pub);
        }
        return sql;
    }

    public String findNewRelatedFilesSql(String pub) {
        return "SELECT JF.VERSION_ID id, JF.FILE_NAME name" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " JSI" +
                "  LEFT JOIN JECN_ACTIVITY_FILE" + pub + " T" +
                "    ON T.FIGURE_ID = JSI.FIGURE_ID" +
                "   AND T.FILE_TYPE = 0" +
                "  LEFT JOIN JECN_FILE" + pub + " JF" +
                "    ON T.FILE_S_ID = JF.FILE_ID" +
                "   AND JF.DEL_STATE = 0" +
                " WHERE JSI.FLOW_ID = #{flowId}" +
                "   AND JF.VERSION_ID IS NOT NULL" +
                " UNION " +
                " SELECT JF.VERSION_ID id, JF.FILE_NAME name" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " JSI" +
                "  LEFT JOIN  JECN_FIGURE_IN_OUT_SAMPLE" + pub + " SAM" +
                "  ON SAM.FLOW_ID = JSI.FLOW_ID" +
                "  AND SAM.TYPE = 0" +
                "   LEFT JOIN JECN_FILE" + pub + " JF" +
                "    ON SAM.FILE_ID = JF.FILE_ID" +
                "   AND JF.DEL_STATE = 0" +
                " WHERE JSI.FLOW_ID = #{flowId}" +
                "   AND JF.VERSION_ID IS NOT NULL" +
                " UNION " +
                "SELECT FT.VERSION_ID id, FT.FILE_NAME name" +
                "  FROM JECN_FIGURE_FILE" + pub + " FF" +
                " INNER JOIN JECN_FILE" + pub + " FT" +
                "    ON FF.FILE_ID = FT.FILE_ID" +
                "   AND FT.DEL_STATE = 0" +
                " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + pub + " IT" +
                "    ON FF.FIGURE_ID = IT.FIGURE_ID" +
                "   AND IT.FLOW_ID = #{flowId}";
    }


    public String findOldRelatedFilesSql(String pub) {
        return "SELECT JF.VERSION_ID id, JF.FILE_NAME name" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " JSI" +
                "  LEFT JOIN JECN_ACTIVITY_FILE" + pub + " T" +
                "    ON T.FIGURE_ID = JSI.FIGURE_ID" +
                "  LEFT JOIN JECN_FILE" + pub + " JF" +
                "    ON T.FILE_S_ID = JF.FILE_ID" +
                "   AND JF.DEL_STATE = 0" +
                " WHERE JSI.FLOW_ID = #{flowId}" +
                "   AND JF.VERSION_ID IS NOT NULL" +
                " UNION " +
                "SELECT JF.VERSION_ID id, JF.FILE_NAME name" +
                "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " JSI" +
                "  LEFT JOIN JECN_MODE_FILE" + pub + " T" +
                "    ON T.FIGURE_ID = JSI.FIGURE_ID" +
                "  LEFT JOIN JECN_FILE" + pub + " JF" +
                "    ON T.FILE_M_ID = JF.FILE_ID" +
                "   AND JF.DEL_STATE = 0" +
                " WHERE JSI.FLOW_ID = #{flowId}" +
                "   AND JF.VERSION_ID IS NOT NULL" +
                " UNION " +
                "SELECT FT.VERSION_ID id, FT.FILE_NAME name" +
                "  FROM JECN_FIGURE_FILE" + pub + " FF" +
                " INNER JOIN JECN_FILE" + pub + " FT" +
                "    ON FF.FILE_ID = FT.FILE_ID" +
                "   AND FT.DEL_STATE = 0" +
                " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + pub + " IT" +
                "    ON FF.FIGURE_ID = IT.FIGURE_ID" +
                "   AND IT.FLOW_ID = #{flowId}";
    }

    /**
     * 获取流程相关附件
     *
     * @param map
     * @return
     */
    public String findMapRelatedFilesSql(Map<String, Object> map) {
        String pub = map.get("isPub").toString();
        return "SELECT JF.VERSION_ID id, JF.FILE_NAME name" +
                "  FROM FLOW_STANDARDIZED_FILE" + pub + " FSF" +
                " INNER JOIN JECN_FILE" + pub + " JF" +
                "    ON JF.FILE_ID = FSF.FILE_ID" +
                "   AND JF.del_state = 0" +
                " WHERE FSF.FLOW_ID = #{flowId}" +
                " UNION " +
                "SELECT FT.VERSION_ID id, FT.FILE_NAME name" +
                "  FROM JECN_FIGURE_FILE" + pub + " FF" +
                " INNER JOIN JECN_FILE" + pub + " FT" +
                "    ON FF.FILE_ID = FT.FILE_ID" +
                "   AND FT.DEL_STATE = 0" +
                " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + pub + " IT" +
                "    ON FF.FIGURE_ID = IT.FIGURE_ID" +
                "   AND IT.FLOW_ID = #{flowId}";

    }

    public String findNewInout(Map<String, Object> map) {

        Long flowId = (Long) map.get("flowId");
        Long historyId = getHistoryId(map);
        String isPub = (String) map.get("isPub");
        String pub = "";
        if (historyId != null && historyId != 0) {
            pub = "_H";
        } else {
            pub = isPub;
        }
        String condition = " where jfio.flow_id=#{flowId} and jfio.type=#{inoutType} ";
        if ("_H".equalsIgnoreCase(pub)) {
            condition += " and jfio.history_id=" + historyId + " and jfiop.history_id=" + historyId;
        }
        String sql = "select * from (select jfio.id," +
                "       jfio.flow_id," +
                "       jfio.type," +
                "       jfio.name," +
                "       jfio.explain," +
                "       jfiop.id         as pg_id," +
                "       jfiop.in_out_id," +
                "       jfiop.r_id," +
                "       jfoi.figure_text as r_name," +
                "       T.Org_Name       as p_name," +
                "       jfiop.r_type," +
                "       jfio.sort_id," +
                "       jfio.pos" +
                "  from jecn_flow_in_out" + pub + " jfio" +
                "  left join jecn_flow_in_out_pos" + pub + " jfiop" +
                "    on jfio.id = jfiop.in_out_id" +
                "   and jfiop.r_type = 0" +
                "  left join jecn_flow_org_image jfoi" +
                "    on jfiop.r_id = jfoi.figure_id" +
                "  left join jecn_flow_org org" +
                "    on jfoi.org_id = org.org_id" +
                "  left join jecn_flow_org T " +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("org") + condition +
                " union" +
                " select jfio.id," +
                "       jfio.flow_id," +
                "       jfio.type," +
                "       jfio.name," +
                "       jfio.explain," +
                "       jfiop.id        as pg_id," +
                "       jfiop.in_out_id," +
                "       jfiop.r_id," +
                "       jpg.name        as r_name," +
                "        T.Name         as p_name," +
                "       jfiop.r_type," +
                "       jfio.sort_id," +
                "       jfio.pos" +
                "  from jecn_flow_in_out" + pub + " jfio" +
                "  left join jecn_flow_in_out_pos" + pub + " jfiop" +
                "    on jfio.id = jfiop.in_out_id" +
                "   and jfiop.r_type = 1" +
                "  left join jecn_position_group jpg" +
                "    on jfiop.r_id = jpg.id" +
                "  left join jecn_position_group T" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("jpg") + "     and t.id <> jpg.id " + condition + " ) t order by t.sort_id ";
        return sql;
    }

    public String fetchInouts(Map<String, Object> map) {
//        Long flowId = (Long) map.get("flowId");
//        Long historyId = getHistoryId(map);
//        String isPub = (String) map.get("isPub");
//        String pub = "";
//        if (historyId != null && historyId != 0) {
//            pub = "_H";
//        } else {
//            pub = isPub;
//        }
//        String condition = " where jfio.flow_id=#{flowId} ";
//        String historyCondition = "";
//        String fileColumn = "version_id";
//        String fileCondition = "file_id";
//        String table = "jecn_file" + pub;
//        if ("_H".equalsIgnoreCase(pub)) {
//            condition += " and jfio.history_id=" + historyId;
//            historyCondition = " and jfios.history_id=" + historyId;
//            fileColumn = "id";
//            table = "file_content";
//            fileCondition = "id";
//        }
//        String sql = "select" +
//                "       jfio.id," +
//                "       jfio.figure_id," +
//                "       jfio.name," +
//                "       jf." + fileColumn + " as file_id," +
//                "       jf.file_name," +
//                "       jfio.explain," +
//                "       jfio.type," +
//                "       jfios.id       as s_id," +
//                "       jff." + fileColumn + "    as s_f_id," +
//                "       jff.file_name  as s_f_name," +
//                "       jfios.type as s_type" +
//                "  from jecn_figure_in_out" + pub + " jfio" +
//                "  left join " + table + " jf" +
//                "    on jfio.file_id = jf." + fileCondition +
//                "  left join jecn_figure_in_out_sample" + pub + " jfios" +
//                "    on jfio.id = jfios.in_out_id" + historyCondition +
//                "  left join " + table + "  jff" +
//                "    on jfios.file_id = jff." + fileCondition + condition +
//                " order by jfio.sort_id,jff.file_name asc ";
        Long flowId = (Long) map.get("flowId");
        Long historyId = getHistoryId(map);
        String isPub = (String) map.get("isPub");
        String pub = "";
        if (historyId != null && historyId != 0) {
            pub = "_H";
        } else {
            pub = isPub;
        }
        String condition = " where jfio.flow_id=#{flowId} ";
        String historyCondition = "";
        String fileColumn = "version_id";
        String fileCondition = "file_id";
        String table = "jecn_file" + pub;
        if ("_H".equalsIgnoreCase(pub)) {
            condition += " and jfio.history_id=" + historyId;
            historyCondition = " and jfios.history_id=" + historyId;
            table = "jecn_file";
        }
        String sql = "select" +
                "       jfio.id," +
                "       jfio.figure_id," +
                "       jfio.name," +
                "       jfio.explain," +
                "       jfio.type," +
                "       jfios.id       as s_id," +
                "       jff." + fileColumn + "    as s_f_id," +
                "       jff.file_name  as s_f_name," +
                "       jfios.type as s_type" +
                "  from jecn_figure_in_out" + pub + " jfio" +
                "  left join jecn_figure_in_out_sample" + pub + " jfios" +
                "    on jfio.id = jfios.in_out_id" + historyCondition +
                "  left join " + table + "  jff" +
                "    on jfios.file_id = jff." + fileCondition + condition +
                " order by jfio.sort_id,jff.file_name asc ";
        return sql;
    }

    public String findInventedFlows(Map<String, Object> map) {
        String sql = "select DISTINCT JFO.ORG_ID AS FILE_ID," +
                "         JFO.ORG_NAME AS FILE_NAME," +
                "         SUM(CASE" +
                "           WHEN JFS.FLOW_ID IS NULL THEN" +
                "            0" +
                "           ELSE" +
                "            1" +
                "         END) AS FILE_COUNT," +
                "         JFO.SORT_ID" +
                "    from JECN_FLOW_ORG JFO" +
                "    INNER JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFO.ORG_ID=JFOI.ORG_ID" +
                "    INNER JOIN JECN_USER_POSITION_RELATED JUP ON JFOI.FIGURE_ID=JUP.FIGURE_ID AND JUP.PEOPLE_ID=#{peopleId}" +
                "    LEFT JOIN JECN_FLOW_APPLY_ORG JF ON JF.ORG_ID=JFO.ORG_ID" +
                "    LEFT JOIN JECN_FLOW_STRUCTURE JFS" +
                "      ON JF.RELATED_ID = JFS.FLOW_ID" +
                "     AND JFS.DEL_STATE = 0" +
                "     GROUP BY JFO.ORG_ID,JFO.ORG_NAME,JFO.SORT_ID " +
                "    ORDER BY JFO.SORT_ID ASC";
        return sql;
    }


    public String getProcessMapResources() {
        String sql = "select p_flow.flow_id p_id," +
                "       p_flow.flow_name  p_name," +
                "       c_flow.flow_id c_id," +
                "       c_flow.flow_name c_name " +
                "  from jecn_flow_structure p_flow" +
                " inner join jecn_flow_structure c_flow" +
                "    on p_flow.flow_id = c_flow.pre_flow_id" +
                "   and p_flow.t_level = 0" +
                "   and p_flow.isflow = 0 " +
                "    and c_flow.isflow = 0 ";
        return sql;
    }

    public String findActivityOperateByFlowIdSql(Map<String, Object> map){
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql="";
        if (historyId == null || historyId == 0) {
            sql = "SELECT T.FILE_ID," +
                    "       JSI.FIGURE_ID," +
                    "       JF.VERSION_ID FILE_S_ID," +
                    "       JF.FILE_NAME," +
                    "       T.FILE_TYPE," +
                    "       JF.DOC_ID," +
                    "       JFO.ORG_NAME," +
                    "       JSI.ACTIVITY_INPUT" +
                    "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JSI" +
                    "  LEFT JOIN JECN_ACTIVITY_FILE" + isPub + " T" +
                    "    ON T.FIGURE_ID = JSI.FIGURE_ID AND T.FILE_TYPE=1" +
                    "  LEFT JOIN JECN_FILE" + isPub + " JF" +
                    "    ON T.FILE_S_ID = JF.FILE_ID" +
                    "   AND JF.DEL_STATE = 0" +
                    "  LEFT JOIN JECN_FLOW_ORG JFO" +
                    "    ON JFO.ORG_ID = JF.ORG_ID" +
                    " WHERE JSI.FLOW_ID = #{flowId}" +
                    "   AND (T.FILE_ID IS NOT NULL OR (JSI.ACTIVITY_OUTPUT IS NOT NULL AND JSI.ACTIVITY_OUTPUT <> ' ' ))";
        } else {
            sql = findActivityOperateByHistorySql(map);
        }
        return sql;
    }

}