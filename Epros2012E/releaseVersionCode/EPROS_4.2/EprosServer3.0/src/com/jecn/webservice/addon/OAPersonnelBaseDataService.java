package com.jecn.webservice.addon;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.1 2015-07-09T09:58:27.508+08:00
 * Generated source version: 3.1.1
 * 
 */
@WebServiceClient(name = "OA_personnelBaseDataService", wsdlLocation = "http://oa.vatti.com.cn/oadata/WebServiceFlow.nsf/OA_personnelBaseData?WSDL", targetNamespace = "urn:DefaultNamespace")
public class OAPersonnelBaseDataService extends Service {

	public final static URL WSDL_LOCATION;

	public final static QName SERVICE = new QName("urn:DefaultNamespace", "OA_personnelBaseDataService");
	public final static QName Domino = new QName("urn:DefaultNamespace", "domino");
	static {
		URL url = null;
		try {
			url = new URL("http://oa.vatti.com.cn/oadata/WebServiceFlow.nsf/OA_personnelBaseData?WSDL");
		} catch (MalformedURLException e) {
			java.util.logging.Logger.getLogger(OAPersonnelBaseDataService.class.getName()).log(
					java.util.logging.Level.INFO, "Can not initialize the default wsdl from {0}",
					"http://oa.vatti.com.cn/oadata/WebServiceFlow.nsf/OA_personnelBaseData?WSDL");
		}
		WSDL_LOCATION = url;
	}

	public OAPersonnelBaseDataService(URL wsdlLocation) {
		super(wsdlLocation, SERVICE);
	}

	public OAPersonnelBaseDataService(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public OAPersonnelBaseDataService() {
		super(WSDL_LOCATION, SERVICE);
	}

	/**
	 * 
	 * @return returns OAPersonnelBaseData
	 */
	@WebEndpoint(name = "domino")
	public OAPersonnelBaseData getDomino() {
		return super.getPort(Domino, OAPersonnelBaseData.class);
	}

	/**
	 * 
	 * @param features
	 *            A list of {@link javax.xml.ws.WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns OAPersonnelBaseData
	 */
	@WebEndpoint(name = "domino")
	public OAPersonnelBaseData getDomino(WebServiceFeature... features) {
		return super.getPort(Domino, OAPersonnelBaseData.class, features);
	}

}
