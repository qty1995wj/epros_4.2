package com.jecn.epros.server.dao.tree;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IJecnTreeDao extends IBaseDao<String, String> {
	/**
	 * 获取树节点的子节点sort最大值
	 * 
	 * @param treeBean
	 * @return
	 * @throws Exception
	 */
	public int getTreeChildMaxSortId(JecnTreeBean treeBean) throws Exception;
}
