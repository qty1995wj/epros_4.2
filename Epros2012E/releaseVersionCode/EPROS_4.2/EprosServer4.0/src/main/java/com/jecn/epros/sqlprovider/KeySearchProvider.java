package com.jecn.epros.sqlprovider;

import com.jecn.epros.security.JecnContants;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Set;

public class KeySearchProvider extends BaseSqlProvider {

    /**
     * 资源检索搜索
     *
     * @param map
     * @return
     */
    public String findKeySearchResultSql(Map<String, Object> map) {
        String sql = "SELECT * FROM(" + findSearchSqlByType(map) + ") TMP_RESULT";
        return sql;
    }

    /**
     * 资源检索，count
     *
     * @param map
     * @return
     */
    public String findKeySearchCountResultSql(Map<String, Object> map) {
        String sql = findSearchSqlByType(map);
        sql = "SELECT COUNT(AllFileTable.RELATEDID) RELATEDID,AllFileTable.RELATETYPE FROM (" + sql
                + ") AllFileTable GROUP BY AllFileTable.RELATETYPE";
        return sql;
    }

    /**
     * RELATETYPE 0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;
     *
     * @param map
     * @return
     */
    private String findSearchSqlByType(Map<String, Object> map) {
        if (map.get("relatedType") == null) {
            throw new IllegalArgumentException("资源检索参数非法！relatedType = " + map.get("relatedType"));
        }
        int relatedType = (int) map.get("relatedType");
        switch (relatedType) {
            case 0:
                return getAllAuthSearchSql(map);
            case 1:
                return getSearchFlowMapSql(map);
            case 2:
                return getSearchFlowSql(map);
            case 3:
                return getSearchFileSql(map);
            case 4:
                return getSearchSystemFileSql(map);
            case 5:// 标准
                return getSearchStandardSql(map);
            case 6:
                return getSearchRiskSql(map);
            case 7:
                return getSearchFlowMapSql(map) + " UNION " + getSearchFlowSql(map);
        }
        return "";
    }

    /**
     * 根据查阅权限获取需要查询的sql
     *
     * @return
     */
    private String getAllAuthSearchSql(Map<String, Object> map) {
        StringBuilder builder = new StringBuilder();
        builder.append(getSearchFlowSql(map));
        builder.append(" UNION ALL ");
        builder.append(getSearchFlowMapSql(map));
        builder.append(" UNION ALL ");
        builder.append(getSearchFileSql(map));
        builder.append(" UNION ALL ");
        builder.append(getSearchSystemFileSql(map));
        builder.append(" UNION ALL ");
        builder.append(getSearchStandardSql(map));
        builder.append(" UNION ALL ");
        builder.append(getSearchRiskSql(map));
        return builder.toString();
    }


    private String getSearchFlowSql(Map<String, Object> map) {
        if (map.get("searchName") == null) {
            return "";
        }
        map.put("typeAuth", TYPEAUTH.FLOW);
        Set<Long> listFigureIds = (Set<Long>) map.get("listFigureIds");
        String searchName = valueOfString(map.get("searchName"));
        StringBuilder builder = new StringBuilder();

        builder.append("SELECT DISTINCT T.FLOW_ID RELATEDID,");
        builder.append("                T.FLOW_NAME REALTEDNAME,");
        builder.append("                T.PUB_TIME UPDATEDATE, 2 AS RELATETYPE,");
        builder.append("                PT.FLOW_NAME PARENTNAME");
        builder.append("  FROM JECN_FLOW_STRUCTURE T");
        builder.append("  LEFT JOIN JECN_FLOW_STRUCTURE PT ON T.PRE_FLOW_ID = PT.FLOW_ID ");
        if (!JecnContants.isFlowAdmin()) {
            builder.append(" INNER JOIN ").append("( " + CommonSqlTPath.INSTANCE.getAuthSQLCommon(map)
                    + getFlowJoinRole(listFigureIds) + " ) MY_AUTH_FILES");
            builder.append("    ON MY_AUTH_FILES.FLOW_ID = T.FLOW_ID");
        }
        builder.append(" WHERE T.DEL_STATE = 0");
        builder.append("    AND T.ISFLOW=1 AND T.PROJECTID = #{projectId}");
        if (StringUtils.isNotEmpty(searchName)) {
            builder.append("    AND T.FLOW_NAME LIKE '%" + searchName + "%'");
        }
        return builder.toString();
    }

    private String valueOfString(Object obj) {
        return obj == null ? "" : obj.toString();
    }


    private String getSearchFlowMapSql(Map<String, Object> map) {
        if (map.get("searchName") == null) {
            return "";
        }
        map.put("typeAuth", TYPEAUTH.FLOW);
        Set<Long> listFigureIds = (Set<Long>) map.get("listFigureIds");
        String searchName = valueOfString(map.get("searchName"));
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT T.FLOW_ID RELATEDID,");
        builder.append("                T.FLOW_NAME REALTEDNAME,");
        builder.append("                T.PUB_TIME UPDATEDATE, 1 AS RELATETYPE,");
        builder.append("                PT.FLOW_NAME PARENTNAME");
        builder.append("  FROM JECN_FLOW_STRUCTURE T");
        builder.append("  LEFT JOIN JECN_FLOW_STRUCTURE PT ON T.PRE_FLOW_ID = PT.FLOW_ID ");
        if (!JecnContants.isMainFlowAdmin()) {
            builder.append(" INNER JOIN ").append("( " + CommonSqlTPath.INSTANCE.getAuthSQLCommon(map)
                    + getFlowJoinRole(listFigureIds) + " ) MY_AUTH_FILES");
            builder.append("    ON MY_AUTH_FILES.FLOW_ID = T.FLOW_ID");
        }
        builder.append(" WHERE T.DEL_STATE = 0");
        builder.append("    AND T.ISFLOW=0 AND T.PROJECTID = #{projectId}");
        if (StringUtils.isNotEmpty(searchName)) {
            builder.append("    AND T.FLOW_NAME LIKE '%" + searchName + "%'");
        }
        return builder.toString();
    }


    private String getSearchFileSql(Map<String, Object> map) {
        if (map.get("searchName") == null) {
            return "";
        }
        map.put("typeAuth", TYPEAUTH.FILE);
        String searchName = valueOfString(map.get("searchName"));
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT T.FILE_ID RELATEDID,");
        builder.append("                T.FILE_NAME REALTEDNAME,");
        builder.append("                T.PUB_TIME UPDATEDATE, 3 AS RELATETYPE,");
        builder.append("                PT.FILE_NAME PARENTNAME");
        builder.append("  FROM JECN_FILE T");
        builder.append("  LEFT JOIN JECN_FILE PT ON T.PER_FILE_ID = PT.FILE_ID ");
        if (!JecnContants.isFileAdmin()) {
            builder.append(" INNER JOIN ").append(CommonSqlTPath.INSTANCE.getFileSearch(map));
            builder.append("    ON MY_AUTH_FILES.FILE_ID = T.FILE_ID");
        }
        builder.append(" WHERE T.DEL_STATE = 0 AND T.HIDE=1 ");
        builder.append("    AND T.PROJECT_ID = #{projectId}  AND T.IS_DIR = 1 ");
        if (StringUtils.isNotEmpty(searchName)) {
            builder.append("  AND T.FILE_NAME LIKE '%" + searchName + "%'");
        }
        return builder.toString();
    }

    /**
     * 0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;
     *
     * @param map
     * @return
     */
    private String getSearchSystemFileSql(Map<String, Object> map) {
        if (map.get("searchName") == null) {
            return "";
        }
        map.put("typeAuth", TYPEAUTH.RULE);
        String searchName = valueOfString(map.get("searchName"));
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT T.ID RELATEDID,");
        builder.append("                T.RULE_NAME REALTEDNAME,");
        builder.append("                T.PUB_TIME UPDATEDATE, 4 AS RELATETYPE,");
        builder.append("                PT.RULE_NAME PARENTNAME");
        builder.append("  FROM JECN_RULE T");
        builder.append("  LEFT JOIN JECN_RULE PT ON T.PER_ID = PT.ID ");
        if (!JecnContants.isSystemAdmin()) {
            builder.append(" INNER JOIN ").append(CommonSqlTPath.INSTANCE.getFileSearch(map));
            builder.append("    ON MY_AUTH_FILES.ID = T.ID");
        }
        builder.append(" WHERE T.PROJECT_ID = #{projectId} AND T.DEL_STATE=0 AND T.IS_DIR <> 0");
        if (StringUtils.isNotEmpty(searchName)) {
            builder.append("  AND T.RULE_NAME LIKE '%" + searchName + "%'");
        }
        return builder.toString();
    }

    /**
     * 0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;
     *
     * @param map
     * @return
     */
    private String getSearchStandardSql(Map<String, Object> map) {
        if (map.get("searchName") == null) {
            return "";
        }
        map.put("typeAuth", TYPEAUTH.STANDARD);
        String searchName = valueOfString(map.get("searchName"));
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT T.CRITERION_CLASS_ID RELATEDID,");
        builder.append("                T.CRITERION_CLASS_NAME REALTEDNAME,");
        builder.append("                T.UPDATE_DATE UPDATEDATE, 5 AS RELATETYPE,");
        builder.append("                PT.CRITERION_CLASS_NAME PARENTNAME");
        builder.append("  FROM JECN_CRITERION_CLASSES T");
        builder.append("  LEFT JOIN JECN_CRITERION_CLASSES PT ON T.PRE_CRITERION_CLASS_ID = PT.CRITERION_CLASS_ID ");
        if (!JecnContants.isCriterionAdmin()) {
            builder.append(" INNER JOIN ").append(CommonSqlTPath.INSTANCE.getFileSearch(map));
            builder.append("    ON MY_AUTH_FILES.CRITERION_CLASS_ID = T.CRITERION_CLASS_ID");
        }
        builder.append(" WHERE T.PROJECT_ID = #{projectId} AND T.STAN_TYPE <> 0 ");
        if (StringUtils.isNotEmpty(searchName)) {
            builder.append("  AND T.CRITERION_CLASS_NAME LIKE '%" + searchName + "%'");
        }
        return builder.toString();
    }

    /**
     * 0:默认所有；1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;
     *
     * @param map
     * @return
     */
    private String getSearchRiskSql(Map<String, Object> map) {
        if (map.get("searchName") == null) {
            return "";
        }
        String searchName = valueOfString(map.get("searchName"));
        StringBuilder builder = new StringBuilder();

        builder.append("SELECT DISTINCT T.ID RELATEDID, T.NAME REALTEDNAME, T.UPDATE_TIME UPDATEDATE, 6 AS RELATETYPE,");
        builder.append("                PT.NAME PARENTNAME");
        builder.append("  FROM JECN_RISK T");
        builder.append("  LEFT JOIN JECN_RISK PT ON T.PARENT_ID = PT.ID ");
        builder.append(" WHERE T.PROJECT_ID = #{projectId} AND T.IS_DIR = 1 ");
        if (StringUtils.isNotEmpty(searchName)) {
            builder.append("  AND T.NAME LIKE '%" + searchName + "%'");
        }
        return builder.toString();
    }

    /**
     * 流程中参与角色
     *
     * @param listFigureIds
     * @return
     */
    public static String getFlowJoinRole(Set<Long> listFigureIds) {
        if (listFigureIds.size() == 0) {
            return "";
        }
        return " UNION " + " SELECT DISTINCT FS.T_PATH ,FS.FLOW_ID" + "    FROM JECN_FLOW_STRUCTURE FS"
                + "   INNER JOIN JECN_FLOW_STRUCTURE_IMAGE FSI" + "      ON FS.FLOW_ID = FSI.FLOW_ID"
                + "   INNER JOIN (SELECT FIGURE_FLOW_ID, FIGURE_POSITION_ID FIGURE_ID"
                + "                FROM PROCESS_STATION_RELATED" + "               WHERE TYPE = '0'"
                + "              UNION" + "              SELECT SR.FIGURE_FLOW_ID, PGR.FIGURE_ID"
                + "                FROM PROCESS_STATION_RELATED SR"
                + "               INNER JOIN JECN_POSITION_GROUP_R PGR"
                + "                  ON SR.FIGURE_POSITION_ID = PGR.GROUP_ID"
                + "               WHERE SR.TYPE = '1') ROLE_FIGURE"
                + "      ON FSI.FIGURE_ID = ROLE_FIGURE.FIGURE_FLOW_ID" + "   AND ROLE_FIGURE.FIGURE_ID IN "
                + SqlCommon.getIdsSet(listFigureIds);
    }
}
