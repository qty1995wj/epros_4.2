package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 信息系统（老版：数据体）
 * 
 * @date： 日期：Mar 21, 2012 时间：2:14:30 PM
 * @author Administrator
 */
public class DataImage extends JecnBaseActiveFigure {

	private int ovalHeight = 20;
	private int x1 = 0;
	private int y1 = 15;

	private double sacle = 0;

	public DataImage(JecnFigureData jecnActiveData) {
		super(jecnActiveData);
	}

	public void paintComponent(Graphics g) {
		sacle = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		ovalHeight = DrawCommon.convertDoubleToInt(20 * sacle);
		y1 = DrawCommon.convertDoubleToInt(15 * sacle);
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - DrawCommon.convertDoubleToInt(31 * sacle);
		paintFigure(g);
		initInfoFigure();
		Graphics2D g2d = (Graphics2D) g;
		if (getFlowElementData().getActivityNum() != null) {
			int activityIdLength = g2d.getFontMetrics(this.getCurScaleFont()).stringWidth(
					getFlowElementData().getActivityNum().toString());
			g2d.drawString(getFlowElementData().getActivityNum().toString(), userWidth / 2 - activityIdLength / 2,
					ovalHeight);
		}
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawOval(x1, y1 - ovalHeight / 2 + userHeight, userWidth, ovalHeight);

		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawLine(x1, y1, x1, userHeight + ovalHeight / 2 + DrawCommon.convertDoubleToInt(5 * sacle));
		g2d.drawLine(userWidth, y1, userWidth, userHeight + ovalHeight / 2 + DrawCommon.convertDoubleToInt(3 * sacle));
		g2d.drawOval(x1, y1 - ovalHeight / 2, userWidth, ovalHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintTop(Graphics2D g2d) {
		// 设置顶层填充
		paintGeneralTop(g2d, 1);
		// 添加顶层
		paintDataImageTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 填充普通图形的顶层 0 为普通，1 阴影
		paintGeneralTop(g2d, 0);
		// 添加顶层
		paintDataImageTop(g2d, 0);
	}

	/**
	 * 画数据体原图顶层
	 * 
	 * @param g2d
	 *            画笔
	 * @param type
	 *            0：原图；1：阴影时 添加原图设置原图顶层
	 */
	private void paintGeneralTop(Graphics2D g2d, int type) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		if (type == 0) {
			g2d.drawOval(x1, y1 - ovalHeight / 2 + userHeight, userWidth, ovalHeight);
		}
		super.paintBackColor(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintDataImageTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		g2d.fillOval(x1, y1 - ovalHeight / 2 + userHeight - shadowCount, userWidth - shadowCount, ovalHeight);
		g2d.fillOval(x1, y1 - ovalHeight / 2, userWidth - shadowCount, ovalHeight);
		g2d.fillRect(x1, y1, userWidth - shadowCount, userHeight - shadowCount);
		if (shadowCount != 0) {
			g2d.setStroke(flowElementData.getBasicStroke());
		}
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawLine(x1, y1, x1, userHeight + ovalHeight / 2 + DrawCommon.convertDoubleToInt(5 * sacle) - shadowCount);
		g2d.drawLine(userWidth - shadowCount, y1, userWidth - shadowCount, userHeight + ovalHeight / 2
				+ DrawCommon.convertDoubleToInt(3 * sacle) - shadowCount);
		g2d.drawOval(x1, y1 - ovalHeight / 2, userWidth - shadowCount, ovalHeight);
		if (getFlowElementData().getActivityNum() != null) {
			int activityIdLength = g2d.getFontMetrics(this.getFont()).stringWidth(
					getFlowElementData().getActivityNum().toString());
			g2d.drawString(getFlowElementData().getActivityNum().toString(), userWidth / 2 - activityIdLength / 2,
					ovalHeight);
		}

	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillOval(x1 + 5, y1 - ovalHeight / 2 + userHeight, userWidth - 5, ovalHeight);
		g2d.fillOval(x1 + 5, y1 - ovalHeight / 2 + 5, userWidth - 5, ovalHeight);
		g2d.fillRect(x1 + 5, y1 + 5, userWidth - 5, userHeight - 5);
	}

	/**
	 * 
	 * 3D背景渐变色
	 * 
	 * @param g
	 */
	public void paint3DFigure(Graphics2D g2d) {
		paintGeneralFigurePart(g2d);
	}

	/**
	 * 
	 * 重新既有3D既有阴影
	 */
	public void paintShadowAnd3DFigure(Graphics2D g2d) {
		paintShadowFigure(g2d);
	}
}
