package epros.draw.gui.operationConfig;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.visio.JecnFileNameFilter;

public class ConfigFileConPanyPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(ConfigFileConPanyPanel.class);
	// ////////////////公司名称公司logo//////////////////
	// /** 公司名称 lable * */
	// private JLabel compLabel = null;
	/** 公司logo lable */
	private JLabel logoLabel = null;
	// /** 公司名称 */
	// private JTextField compFiled = null;
	// /** 公司名称提示信息 */
	// private JecnUserInfoTextArea cmpNameInfoArea = null;
	/** logo */
	private JLabel logoImageLabel = null;
	/** logo选择按钮 */
	private JButton logoButton = null;

	/** 上传的logo图标 */
	private File selectedFile = null;

	private String logoIcomPath = null;

	/** 是否使用logo图标 true:读取logo */
	private JCheckBox logoCheckBox = null;
	/** 查看全图：logo */
	private JButton viewLogoButton = null;
	/** 服务器logo图片位置 */
	private String sysInfoPath = null;

	// ////////////////公司名称公司logo//////////////////
	public ConfigFileConPanyPanel(String sysInfoPath) {
		this.sysInfoPath = sysInfoPath;
		initCompany();
		initButtonAction();
		initData();
	}

	private void initButtonAction() {

		// logo选择按钮
		logoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				uploadImage();
			}
		});

		// 查看
		viewLogoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				viewLogoButton();
			}
		});
	}

	private void initData() {
		try {
			String tmpPath = null;
			if (!JecnSystemStaticData.isEprosDesigner()) {// 不是设计器
				tmpPath = JecnFileUtil.getSyncPath("/epros/draw/configFile/operationConfig/operationConfig.xml");
				tmpPath = tmpPath.substring(0, tmpPath.length() - "operationConfig.xml".length());
				logoIcomPath = tmpPath + "logo.png";
			} else {// 设计器登录
				tmpPath = sysInfoPath;
				logoIcomPath = tmpPath + "wordLogo.png";
			}
		} catch (Exception e) {
			log.error("获取logo图片路径时失败", e);
		}

		// logo图片
		String path = null;
		if (!JecnSystemStaticData.isEprosDesigner()) {// 不是设计器
			path = JecnFileUtil.getLogoFilePath();
		} else {// 设计器登录
			path = sysInfoPath;
		}
		if (!DrawCommon.isNullOrEmtryTrim(path)) {
			ImageIcon icon = getImageIcon(new File(path));
			if (icon != null) {
				logoImageLabel.setIcon(icon);
				// 存在图片可查看
				viewLogoButton.setEnabled(true);
			}
		}
		// 是否使用logo标识 true:显示；false：隐藏
		if (JecnSystemStaticData.isShowLogoIcon()) {
			logoCheckBox.setSelected(true);
		} else {
			logoCheckBox.setSelected(false);
		}
	}

	/**
	 * 初始化公司信息
	 * 
	 */
	private void initCompany() {
		// this = new JecnPanel();

		this.setBorder(BorderFactory.createTitledBorder(""));
		this.setBorder(null);

		logoLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("compLogoC"));

		// // 公司名称提示信息
		// cmpNameInfoArea = new JecnUserInfoTextArea();
		// cmpNameInfoArea.setVisible(false);
		// logo
		logoImageLabel = new JLabel();
		Dimension dimension = new Dimension(150, 35);
		logoImageLabel.setPreferredSize(dimension);
		logoImageLabel.setMinimumSize(dimension);
		logoImageLabel.setMaximumSize(dimension);
		logoImageLabel.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("logoNote"));

		// select
		logoButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("select"));
		logoButton.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("logoNote"));

		// 是否使用
		logoCheckBox = new JCheckBox(JecnResourceUtil.getJecnResourceUtil().getValue("useOrNotUse"));
		logoCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 查看
		viewLogoButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("previewLogo"));

		if (!JecnSystemStaticData.isEprosDesigner()) {// 不是
			// 存在图片可查看
			viewLogoButton.setEnabled(false);
		}
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.logoImageLabel.setVisible(false);
		this.logoCheckBox.setVisible(false);

		initLayOutCompany();
	}

	/**
	 * 公司信息初始化布局
	 * 
	 */
	private void initLayOutCompany() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);

		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(logoLabel, c);

		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(logoImageLabel, c);

		c = new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(new JLabel(), c);

		// 是否使用
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(logoCheckBox, c);

		// 查看
		c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(viewLogoButton, c);

		// 选择按钮
		c = new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(logoButton, c);
	}

	/**
	 * 上传图标
	 * 
	 */
	private void uploadImage() {
		try {
			JecnFileChooser fileChooser = new JecnFileChooser();
			// 移除系统给定的文件过滤器
			fileChooser.setAcceptAllFileFilterUsed(false);
			// logo图标选择
			fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("logoSelect"));
			String[] str = new String[] { "gif", "jpeg", "png" };
			// 过滤文件后缀
			fileChooser.setFileFilter(new JecnFileNameFilter(str));
			// 设置按钮为打开
			fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileChooser.setEidtTextFiled(false);
			fileChooser.setApproveButtonText(JecnResourceUtil.getJecnResourceUtil().getValue("fileChooserOKBtn"));
			// 显示文件选择器
			int isOk = fileChooser.showOpenDialog(JecnDrawMainPanel.getMainPanel());

			// 选择确认（yes、ok）后返回该值。
			if (isOk == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				ImageIcon icon = getImageIcon(selectedFile);
				if (icon != null) {
					logoImageLabel.setIcon(icon);
					logoImageLabel.revalidate();
					this.repaint();

					// 存在图片可查看
					viewLogoButton.setEnabled(true);
				}
			}
			if (JecnSystemStaticData.isEprosDesigner()) {// 设计器登录
				if (selectedFile != null && selectedFile.exists() && selectedFile.isFile()) {// 选择图片

					// logo图标写入本地
					boolean ret = JecnFileUtil.writeLogoFileToOPLocal(selectedFile, logoIcomPath);
					if (!ret) {
						JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnResourceUtil
								.getJecnResourceUtil().getValue("logoIconSaveFail"));
						log.info("selectedFile=" + selectedFile.getPath() + " 存放路径=" + JecnFileUtil.getLogoFilePath());
						return;
					}
				}

			}
		} catch (Exception e) {
			log.error("uploadImage", e);
		}
	}

	/**
	 * 
	 * 获取本地图片
	 * 
	 * @param file
	 * @return
	 */
	private ImageIcon getImageIcon(File file) {
		ImageIcon icon = null;
		if (file != null && file.exists() && file.isFile()) {
			Image img = Toolkit.getDefaultToolkit().createImage(file.getPath());
			icon = new ImageIcon(img);
		}
		return icon;
	}

	/**
	 * 查看本地logo图标
	 */
	private void viewLogoButton() {
		String filePath = null;
		if (selectedFile != null) {// 存在上传文件，查看应该查看当前上传文件
			filePath = selectedFile.getPath();
		} else {
			if (JecnSystemStaticData.isEprosDesigner()) {// 设计器登录
				filePath = sysInfoPath + "/wordLogo.png";
			} else {
				filePath = JecnFileUtil.getLogoFilePath();
			}
		}
		if (DrawCommon.isNullOrEmtryTrim(filePath)) {
			// 文件不存在！
			JecnOptionPane.showMessageDialog(null, JecnResourceUtil.getJecnResourceUtil().getValue("fileIsNotExist"));
			return;
		}
		JecnFileUtil.openFile(filePath);
	}

	public File getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(File selectedFile) {
		this.selectedFile = selectedFile;
	}

	public String getLogoIcomPath() {
		return logoIcomPath;
	}

	public void setLogoIcomPath(String logoIcomPath) {
		this.logoIcomPath = logoIcomPath;
	}

	public JCheckBox getLogoCheckBox() {
		return logoCheckBox;
	}

	public void setLogoCheckBox(JCheckBox logoCheckBox) {
		this.logoCheckBox = logoCheckBox;
	}

	public JLabel getLogoImageLabel() {
		return logoImageLabel;
	}

	public void setLogoImageLabel(JLabel logoImageLabel) {
		this.logoImageLabel = logoImageLabel;
	}
}
