package com.jecn.epros.server.action.designer.system;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

/**
 * 
 * 配置信息表（JECN_CONFIG_ITEM）操作类
 * 
 * @author ZHOUXY
 * 
 */
public interface IJecnConfigItemAciton {
	/**
	 * 
	 * 查询流程图设置值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 流程图设置值
	 */
	public List<JecnConfigItemBean> select(int typeBigModel);

	/**
	 * 
	 * 查询流程文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程文件排序后显示数据
	 */
	public List<JecnConfigItemBean> selectShowFlowFile();

	/**
	 * 
	 * 查询流程建设规范显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程建设规范显示数据
	 */
	public List<JecnConfigItemBean> selectShowPartMapStand();

	/**
	 * 
	 * 查询任务审批环节数据（带文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节数据（带文控类型）
	 */
	public List<JecnConfigItemBean> selectShowTaskApp(int typeBigModel);

	/**
	 * 
	 * 查询任务审批环节数据（除去文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节显示数据
	 */
	public List<JecnConfigItemBean> selectTaskAppTable(int typeBigModel);

	/**
	 * 
	 * 查询所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 */
	public List<JecnConfigItemBean> selectTaskShowAllItems(int typeBigModel);

	/**
	 * 
	 * 查询所有表示公开秘密数据集合
	 * 
	 * @return List<JecnConfigItemBean> 所有表示公开秘密数据集合
	 */
	public List<JecnConfigItemBean> selectAllPub();

	/**
	 * 
	 * 修改JECN_CONFIG_ITEM表
	 * 
	 * @param configItemList
	 *            List<JecnConfigItemBean>
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return boolean true:修改成功；false：修改失败
	 */
	public boolean update(List<JecnConfigItemBean> configItemList, int typeBigModel);

	/**
	 * 
	 * 通过大类下唯一标示获取value字段值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String value字段值或NULL
	 */
	public String selectValue(int typeBigModel, String mark);

	/**
	 * 
	 * 通过大类下唯一标示获取数据
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String 一行数据字段值或NULL
	 */
	public JecnConfigItemBean selectConfigItemBean(int typeBigModel, String mark);

	/**
	 * 查询流程基本信息
	 * 
	 * @return List<JecnConfigItemBean>查询流程基本信息
	 */
	public List<JecnConfigItemBean> selectBaseInfoFlowFile();

	/**
	 * 获取系统配置项信息
	 * 
	 * @param typeBigModel
	 *            int 0：流程地图 ,1：流程图 ,2：制度, 3：文件, 4：邮箱配置, 5：权限配置
	 * @param typeSmallModel
	 *            int 0：审批环节配置, 1：任务界面显示项配置, 2：基本配置, 3：流程图建设规范 ,4：流程文件,
	 *            5：邮箱配置,6：权限配置
	 * @return List<JecnConfigItemBean> 系统配置项信息
	 * @throws Exception
	 */
	public List<JecnConfigItemBean> selectConfigItemBeanByType(int typeBigModel, int typeSmallModel);

	/**
	 * 
	 * 查询流程地图文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程文件排序后显示数据
	 */
	public List<JecnConfigItemBean> selectFlowMapShowFlowFile();

	/**
	 * 配置信息
	 * 
	 * @param bigModel
	 * @param smallModel
	 * @param isSort
	 *            是否排序 true从小到大 false大到小 null忽略
	 * @param value
	 *            值如果为null表示不填写
	 * @return
	 */
	public List<JecnConfigItemBean> selectShowSortConfigItemBeanByType(int bigModel, int smallModel);

	public List<JecnConfigItemBean> selectTableConfigItemBeanByType(int bigModel, int smallModel);
	

	public List<JecnConfigItemBean> selectKpiConfigItemBeanByTop(int top);

	/**
	 * 从缓存中根据mark查找value
	 * @param mark
	 * @return
	 */
	public String selectValueFromCache(ConfigItemPartMapMark mark);

	JecnConfigItemBean selectItemBeanByMark(String mark);
	
	void update(List<JecnConfigItemBean> updateItemList);

	public List<Object[]> selectMaintainNode();

	public JecnElementsLibrary getElement(String string);
}
