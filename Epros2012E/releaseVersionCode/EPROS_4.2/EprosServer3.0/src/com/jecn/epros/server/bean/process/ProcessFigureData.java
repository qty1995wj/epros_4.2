package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutT;

/**
 * 流程元素数据对象
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-12-3 时间：下午02:25:24
 */
public class ProcessFigureData implements Serializable {
	/** 元素 */
	private JecnFlowStructureImageT jecnFlowStructureImageT;
	/** 角色与岗位关联 */
	private List<JecnFlowStationT> listRolePosition;
	/** 输出 */
	private List<JecnModeFileT> listModeFileT;
	/** 输入和操作规范 */
	private List<JecnActivityFileT> listJecnActivityFileT;
	/** 指标 */
	private List<JecnRefIndicatorsT> listRefIndicatorsT;
	/** 控制点 */
	private JecnControlPointT controlPoint;
	/** 控制点集合 C */
	private List<JecnControlPointT> listControlPoint;
	/** 活动标准关联表 */
	private List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT;
	/** 线上信息 */
	private List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean;
	/** 相关链接集合 */
	private List<JecnFigureFileTBean> listJecnFigureFileTBean;

	/** 术语选择：术语定义 */
	private String termDefine;

	/** 关联的元素 **/
	private Long relatedId;

	/** 新版 输入输出 */
	private List<JecnFigureInoutT> figureInList;

	private List<JecnFigureInoutT> figureOutList;

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public List<JecnFigureFileTBean> getListJecnFigureFileTBean() {
		return listJecnFigureFileTBean;
	}

	public void setListJecnFigureFileTBean(List<JecnFigureFileTBean> listJecnFigureFileTBean) {
		this.listJecnFigureFileTBean = listJecnFigureFileTBean;
	}

	public JecnControlPointT getControlPoint() {
		return controlPoint;
	}

	public void setControlPoint(JecnControlPointT controlPoint) {
		this.controlPoint = controlPoint;
	}

	public List<JecnActiveStandardBeanT> getListJecnActiveStandardBeanT() {
		return listJecnActiveStandardBeanT;
	}

	public void setListJecnActiveStandardBeanT(List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT) {
		this.listJecnActiveStandardBeanT = listJecnActiveStandardBeanT;
	}

	public List<JecnActiveOnLineTBean> getListJecnActiveOnLineTBean() {
		return listJecnActiveOnLineTBean;
	}

	public void setListJecnActiveOnLineTBean(List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean) {
		this.listJecnActiveOnLineTBean = listJecnActiveOnLineTBean;
	}

	public JecnFlowStructureImageT getJecnFlowStructureImageT() {
		return jecnFlowStructureImageT;
	}

	public void setJecnFlowStructureImageT(JecnFlowStructureImageT jecnFlowStructureImageT) {
		this.jecnFlowStructureImageT = jecnFlowStructureImageT;
	}

	public List<JecnFlowStationT> getListRolePosition() {
		return listRolePosition;
	}

	public void setListRolePosition(List<JecnFlowStationT> listRolePosition) {
		this.listRolePosition = listRolePosition;
	}

	public List<JecnModeFileT> getListModeFileT() {
		return listModeFileT;
	}

	public void setListModeFileT(List<JecnModeFileT> listModeFileT) {
		this.listModeFileT = listModeFileT;
	}

	public List<JecnActivityFileT> getListJecnActivityFileT() {
		return listJecnActivityFileT;
	}

	public void setListJecnActivityFileT(List<JecnActivityFileT> listJecnActivityFileT) {
		this.listJecnActivityFileT = listJecnActivityFileT;
	}

	public List<JecnRefIndicatorsT> getListRefIndicatorsT() {
		return listRefIndicatorsT;
	}

	public void setListRefIndicatorsT(List<JecnRefIndicatorsT> listRefIndicatorsT) {
		this.listRefIndicatorsT = listRefIndicatorsT;
	}

	public List<JecnControlPointT> getListControlPoint() {
		return listControlPoint;
	}

	public void setListControlPoint(List<JecnControlPointT> listControlPoint) {
		this.listControlPoint = listControlPoint;
	}

	public String getTermDefine() {
		return termDefine;
	}

	public void setTermDefine(String termDefine) {
		this.termDefine = termDefine;
	}

	public List<JecnFigureInoutT> getFigureInList() {
		return figureInList;
	}

	public void setFigureInList(List<JecnFigureInoutT> figureInList) {
		this.figureInList = figureInList;
	}

	public List<JecnFigureInoutT> getFigureOutList() {
		return figureOutList;
	}

	public void setFigureOutList(List<JecnFigureInoutT> figureOutList) {
		this.figureOutList = figureOutList;
	}

}
