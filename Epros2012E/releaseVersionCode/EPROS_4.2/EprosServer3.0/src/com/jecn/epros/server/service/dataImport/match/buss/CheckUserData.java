package com.jecn.epros.server.service.dataImport.match.buss;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.constant.MatchErrorInfo;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;

public class CheckUserData {
	/** 记录登录名称个数 :为验证服务店最大用户 手机数据 */
	private Set<String> userNumberList = new HashSet<String>();

	/**
	 * 
	 * 校验人员+岗位原始数据
	 * 
	 * @param baseBean
	 * @return
	 */
	public boolean checkUserPosRow(BaseBean baseBean) {

		boolean ret = true;

		// 获取人员临时表数据
		List<UserMatchBean> userPosBeanList = baseBean.getUserPosBeanList();
		if (userPosBeanList == null || userPosBeanList.size() == 0) {
			return ret;
		}

		for (UserMatchBean userPosBean : userPosBeanList) {
			// 人员所有字段都为空
			if (userPosBean.isAttributesNull()) {
				userPosBean.addError(MatchErrorInfo.USER_ALL_NULL);
				ret = false;
			}

			// 一条数据校验 单个值校验********************
			if (!checkOneUserPosBean(userPosBean)) {
				ret = false;
			}
			userNumberList.add(userPosBean.getLoginName());
		}
		return ret;
	}

	/**
	 * 
	 * 验证人员（行内校验）
	 * 
	 * @param userBean
	 * @param baseBean
	 * @return
	 */
	private boolean checkOneUserPosBean(UserMatchBean userBean) {
		boolean ret = true;
		// 人员编号去前缀后缀空后为空
		if (JecnCommon.isNullOrEmtryTrim(userBean.getLoginName())) {
			userBean.addError(MatchErrorInfo.USER_NUM_NULL);
			ret = false;
		} else {
			if (MatchConstant.USER_ADMIN.equals(userBean.getLoginName())) {
				// 人员编号不能为"admin"
				userBean.addError(MatchErrorInfo.USER_NUM_AMIN);
				ret = false;
			} else if (MatchTool.checkNameMaxLength(userBean.getLoginName())) {
				// 人员编号(登录名称)不能超过122个字符
				userBean.addError(MatchErrorInfo.LOGIN_NAME_LENGTH_ERROR);
				ret = false;
			}
			userBean.setLoginName(userBean.getLoginName().trim());
		}

		// 真实姓名去前缀后缀空后为空
		if (JecnCommon.isNullOrEmtryTrim(userBean.getTrueName())) {
			userBean.addError(MatchErrorInfo.TRUE_NAME_NULL);
			ret = false;
		} else {
			// if (MatchTool.checkNameFileSpecialChar(userBean.getTrueName())) {
			// // 真实姓名由中文、英文、数字及“_”、“-”组成
			// userBean.addError(MatchErrorInfo.TRUE_NUME_SPECIAL_CHAR);
			// ret = false;
			// } else
			if (MatchTool.checkNameMaxLength(userBean.getTrueName())) {
				// 真实姓名不能超过122个字符
				userBean.addError(MatchErrorInfo.TRUE_NAME_LENGTH_ERROR);
				ret = false;
			}
			userBean.setTrueName(userBean.getTrueName().trim());
		}

		// 邮箱：必须满足邮箱格式
		if (JecnCommon.isNullOrEmtryTrim(userBean.getEmail())) {
			userBean.setEmail(null);
			// userBean.setEmailType(Integer.valueOf(null));
		} else {
			// 邮箱地址不能超过122个字符
			if (MatchTool.checkNameMaxLength(userBean.getEmail())) {
				userBean.addError(MatchErrorInfo.EMAIL_LENGTH_ERROR);
				ret = false;
			}
			// 去空
			userBean.setEmail(userBean.getEmail().trim());
			//
			// // 邮箱格式不正确
			// Pattern emailer = Pattern.compile(MatchConstant.EMAIL_FORMAT);
			// if (!emailer.matcher(userBean.getEmail()).matches()) {
			// userBean.addError(ErrorInfo.EMAIL_FORMAT_FAIL);
			// ret = false;
			// } else {
			// // 内外网邮件标识:填写邮箱地址，必须指定内网还是外网(内网：0,外网：1)
			//
			if (!userBean.checkEmailType()) {
				userBean.addError(MatchErrorInfo.EMAIL_TYPE_FAIL);
				ret = false;
			}
			// }
		}

		// 电话
		if (JecnCommon.isNullOrEmtryTrim(userBean.getPhone())) {
			userBean.setPhone(null);
		} else {
			// 联系电话不能超过122个字符
			if (MatchTool.checkNameMaxLength(userBean.getPhone())) {
				userBean.addError(MatchErrorInfo.PHONE_LENGTH_ERROR);
				ret = false;
			}

			// 去空
			userBean.setPhone(userBean.getPhone().trim());
		}

		// 岗位编号
		if (!JecnCommon.isNullOrEmtryTrim(userBean.getPosNum())) {// 岗位编号不为空时
			// 任职岗位编号不能超过122个字符
			if (MatchTool.checkNameMaxLength(userBean.getPosNum())) {
				userBean.addError(MatchErrorInfo.POS_NUM_LENGTH_ERROR);
				ret = false;
			}
			userBean.setPosNum(userBean.getPosNum().trim());
		} else {// 岗位编号为空时
			userBean.setPosNum(null);
		}

		return ret;
	}

	/**
	 * 
	 * 人员行内校验、行间校验
	 * 
	 */
	private boolean checkUserRow(BaseBean baseBean) {
		if (baseBean == null || baseBean.isNull()) {
			return false;
		}

		boolean ret = true;

		// 获取人员临时表数据
		List<UserMatchBean> userBeanList = baseBean.getUserPosBeanList();
		if (userBeanList == null || userBeanList.size() == 0) {
			return ret;
		}

		// 用户数据行内校验
		for (UserMatchBean userBean : userBeanList) {
			if (!checkOneUserBean(userBean, baseBean)) {
				ret = false;
			}
		}

		if (!ret) {
			return ret;
		}

		// ---------------行间校验
		// 人员编号唯一
		ret = checkUserRows(userBeanList);
		// ---------------行间校验

		return ret;
	}

	private boolean checkOneUserBean(UserMatchBean userBean, BaseBean baseBean) {
		boolean ret = true;
		// 人员编号去前缀后缀空后为空
		if (JecnCommon.isNullOrEmtryTrim(userBean.getLoginName())) {
			userBean.addError(MatchErrorInfo.USER_NUM_NULL);
			ret = false;
		} else {
			userBean.setLoginName(userBean.getLoginName().trim());
			// 人员编号不能为"admin"
			if (MatchConstant.USER_ADMIN.equals(userBean.getLoginName())) {
				userBean.addError(MatchErrorInfo.USER_NUM_AMIN);
				ret = false;
			}
		}

		// 真实姓名去前缀后缀空后为空
		if (JecnCommon.isNullOrEmtryTrim(userBean.getTrueName())) {
			userBean.addError(MatchErrorInfo.TRUE_NAME_NULL);
			ret = false;
		} else {
			userBean.setTrueName(userBean.getTrueName().trim());
		}

		// 任职岗位编号不为空时，任职岗位编号在岗位集合中必须存在
		if (JecnCommon.isNullOrEmtryTrim(userBean.getPosNum())) {
			userBean.setPosNum(null);

		} else {

			// 任职岗位编号不为空时，任职岗位编号在岗位集合中必须存在
			boolean existsUserByPos = baseBean.existsUserByPos(userBean);
			if (!existsUserByPos) {
				userBean
						.addError(MatchErrorInfo.USER_POS_NUM_NOT_EXISTS_POS_LIST);
				ret = false;
			} else {
				userBean.setPosNum(userBean.getPosNum().trim());
			}
		}

		// 邮箱：必须满足邮箱格式
		if (JecnCommon.isNullOrEmtryTrim(userBean.getEmail())) {
			userBean.setEmail(null);
			// userBean.setEmailType(Integer.valueOf(null));
		} else {
			// 去空
			userBean.setEmail(userBean.getEmail().trim());
			// 邮箱地址不能超过122个字符
			if (MatchTool.checkNameMaxLength(userBean.getEmail())) {
				userBean.addError(MatchErrorInfo.EMAIL_LENGTH_ERROR);
				ret = false;
			}
			// 邮箱格式不正确
			// Pattern emailer = Pattern.compile(MatchConstant.EMAIL_FORMAT);
			// if (!emailer.matcher(userBean.getEmail()).matches()) {
			// userBean.addError(ErrorInfo.EMAIL_FORMAT_FAIL);
			// ret = false;
			// } else {
			// 内外网邮件标识:填写邮箱地址，必须指定内网还是外网(内网：0,外网：1)

			if (!userBean.checkEmailType()) {
				userBean.addError(MatchErrorInfo.EMAIL_TYPE_FAIL);
				ret = false;
			}
			// }
		}

		// 电话
		if (JecnCommon.isNullOrEmtryTrim(userBean.getPhone())) {
			userBean.setPhone(null);
		} else {
			// 去空
			userBean.setPhone(userBean.getPhone().trim());
		}
		// //标识位不为空
		// if
		// (JecnCommon.isNullOrEmtryTrim(String.valueOf(userBean.getPersonFlag())))
		// {
		// // userBean.addError(ErrorInfo.TRUE_NAME_NULL);
		// ret = false;
		// } else {
		// userBean.setPersonFlag(userBean.getPersonFlag());
		// }
		return ret;
	}

	/**
	 * 
	 * 判断人员编号是否唯一，唯一返回true，不唯一返回false
	 * 
	 * @param userBeanList
	 *            List<UserBean>人员集合
	 * @return boolean 唯一返回true，不唯一返回false
	 */
	private boolean checkUserRows(List<UserMatchBean> userBeanList) {
		boolean ret = true;
		for (UserMatchBean userBean : userBeanList) {
			for (UserMatchBean userBean2 : userBeanList) {
				// 不是同一个对象，人员编号相同
				if (!userBean.equals(userBean2)
						&& userBean.getLoginName().equals(
								userBean2.getLoginName())) {

					if (JecnCommon.isNullOrEmtryTrim(userBean.getPosNum())
							|| JecnCommon.isNullOrEmtryTrim(userBean2
									.getPosNum())) {
						// 同一个人员不同岗位情况，岗位编号不能为空
						userBean
								.addError(MatchErrorInfo.USER_NUM_MUTIL_POS_NUM_NOT_NULL);
						ret = false;
					} else if (userBean.getPosNum().equals(
							userBean2.getPosNum())) {
						// 出现多条人员编号和岗位编号相同的数据
						userBean.addError(MatchErrorInfo.USER_DATA_NOT_ONLY);
						ret = false;
					} else {
						// 判断真实姓名、邮箱、邮箱类型、电话是否相同
						if (!checkRowAndRowSame(userBean, userBean2)) {
							ret = false;
						}
					}
				}
			}
		}
		return ret;
	}

	/**
	 * 
	 * 前提条件：给定参数是属于同一人员不同记录情况下 判断真实姓名、邮箱、邮箱类型、电话是否相同
	 * 
	 * @param userBean
	 * @param userBean2
	 * @return
	 */
	private boolean checkRowAndRowSame(UserMatchBean userBean,
			UserMatchBean userBean2) {
		boolean ret = false;
		// 真实姓名
		if (!userBean.getTrueName().equals(userBean2.getTrueName())) {
			userBean.addError(MatchErrorInfo.USER_TRUE_NAME_NOT_SAME);
			return ret;
		}

		// 邮箱:userBean对象邮箱不为空情况下，邮箱不相等或邮箱类型不相等
		if (!JecnCommon.isNullOrEmtryTrim(userBean.getEmail())
				&& (!userBean.getEmail().equals(userBean2.getEmail()) || userBean
						.getEmailType() != userBean2.getEmailType())) {
			userBean.addError(MatchErrorInfo.USER_EMAIL_NOT_SAME);
			return ret;
		} else if (!JecnCommon.isNullOrEmtryTrim(userBean2.getEmail())
				&& (!userBean2.getEmail().equals(userBean.getEmail()) || userBean2
						.getEmailType() != userBean.getEmailType())) {// 邮箱:userBean2对象邮箱不为空情况下，邮箱不相等或邮箱类型不相等
			userBean.addError(MatchErrorInfo.USER_EMAIL_NOT_SAME);
			return ret;
		}

		// 电话不为空，电话不相等
		if (!JecnCommon.isNullOrEmtryTrim(userBean.getPhone())
				&& !userBean.getPhone().equals(userBean2.getPhone())) {
			userBean.addError(MatchErrorInfo.USER_PHONE_NOT_SAME);
			return ret;
		} else if (!JecnCommon.isNullOrEmtryTrim(userBean2.getPhone())
				&& !userBean2.getPhone().equals(userBean.getPhone())) {
			userBean.addError(MatchErrorInfo.USER_PHONE_NOT_SAME);
			return ret;
		}

		return true;
	}

	public Set<String> getUserNumberList() {
		return userNumberList;
	}
}
