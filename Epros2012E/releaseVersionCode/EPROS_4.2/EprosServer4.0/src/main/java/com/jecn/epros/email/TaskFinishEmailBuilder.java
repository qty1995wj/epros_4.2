package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.task.TaskBean;

public class TaskFinishEmailBuilder extends DynamicContentEmailBuilder {

    @Override
    protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

        Object[] data = getData();
        TaskBean taskBeanNew = (TaskBean) data[0];

        return createEmail(user, taskBeanNew);
    }

    /**
     * 任务审批完成给拟稿人、审批人员、拥有查阅权限的人员、固定人员发送邮件
     *
     * @param userObject  0:内外网；1：邮件地址;2:人员主键ID
     * @param taskBeanNew 任务主表
     */
    private EmailBasicInfo createEmail(JecnUser user,
                                       TaskBean taskBeanNew) {
        /**
         * rId taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,
         * 3:制度文件ID，4：流程地图ID
         */
        int taskType = taskBeanNew.getTaskType();
        String rId = String.valueOf(taskBeanNew.getRid());

        // 邮件标题 任务xxx审批完成，请查阅！
        String subject = ToolEmail.getTaskSubjectForPeopleMake(taskBeanNew);
        // 邮件内容
        String taskContent = ToolEmail.getTaskDesc(taskBeanNew);

        EmailContentBuilder builder = new EmailContentBuilder(getTip());
        builder.setContent(taskContent);
        builder.setResourceUrl(getHttpUrl(rId, taskType, String.valueOf(user.getId())));
        EmailBasicInfo basicInfo = new EmailBasicInfo();
        basicInfo.setSubject(subject);
        basicInfo.setContent(builder.buildContent());
        basicInfo.addRecipients(user);

        return basicInfo;
    }

    private String getHttpUrl(String rId, int taskType, String peopleId) {
        // 任务审批完成
        String httpUrl = "";
        // 如果是流程任务
        if (taskType == 0) {
            httpUrl = ToolEmail.getHttpUrl(rId, "process", peopleId);
        } else if (taskType == 2)// 制度模板任务
        {
            httpUrl = ToolEmail.getHttpUrl(rId, "ruleModeFile", peopleId);
        } else if (taskType == 3)// 制度文件任务
        {
            httpUrl = ToolEmail.getHttpUrl(rId, "ruleFile", peopleId);
        } else if (taskType == 4)// 流程地图
        {
            httpUrl = ToolEmail.getHttpUrl(rId, "processMap", peopleId);
        } else if (taskType == 1)// 文件任务
        {
            httpUrl = ToolEmail.getHttpUrl(rId, "infoFile", peopleId);
        }

        return httpUrl;
    }

}
