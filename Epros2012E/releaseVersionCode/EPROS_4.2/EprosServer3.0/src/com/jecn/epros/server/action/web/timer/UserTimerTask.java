package com.jecn.epros.server.action.web.timer;

import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 定时器：
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jan 14, 2013 时间：11:28:14 AM
 */
public class UserTimerTask extends TimerTask {
	private final Log log = LogFactory.getLog(UserTimerTask.class);
	/** 执行同步数据 */
	private TimerAction timerAction;

	public UserTimerTask(TimerAction timerAction) {
		this.timerAction = timerAction;
	}

	@Override
	public void run() {
		AbstractConfigBean autoBean = timerAction.getAbstractConfigBean();
		if (timerAction == null || autoBean == null) {
			throw new NullPointerException("run()定时器加载异常！timerAction = "
					+ timerAction + " autoBean = " + autoBean);
		}
		// 判断是否是自动导入 1为自动导入
		if (autoBean != null
				&& !SyncConstant.AUTO_SYNC_USER_DATA_FLAG.equals(autoBean
						.getIaAuto())) {
			return;
		}
		try {
			timerAction.synData();
		} catch (Exception e) {
			log.error("定时器执行异常！", e);
		}
	}
}
