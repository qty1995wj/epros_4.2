package com.jecn.epros.server.action.designer.define.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.define.ITermDefinitionAction;
import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.define.ITermDefinitionService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class TermDefinitionActionImpl implements ITermDefinitionAction {
	private ITermDefinitionService termDefinition;

	public ITermDefinitionService getTermDefinition() {
		return termDefinition;
	}

	public void setTermDefinition(ITermDefinitionService termDefinition) {
		this.termDefinition = termDefinition;
	}

	@Override
	public TermDefinitionLibrary createDir(TermDefinitionLibrary termDefinitionLibrary) throws Exception {
		return termDefinition.createDir(termDefinitionLibrary);
	}

	@Override
	public TermDefinitionLibrary createTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception {
		// TODO Auto-generated method stub
		return termDefinition.createTermDefine(termDefinitionLibrary);
	}

	@Override
	public void delete(List<Long> listIds, Long updatePersonId) throws Exception {
		termDefinition.delete(listIds, updatePersonId);
	}

	@Override
	public TermDefinitionLibrary editTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception {
		return termDefinition.editTermDefine(termDefinitionLibrary);
	}

	@Override
	public List<JecnTreeBean> getChilds(Long pId) throws Exception {
		return termDefinition.getChilds(pId);
	}

	@Override
	public boolean isExistAdd(String name, Long id, int type) throws Exception {
		return termDefinition.isExistAdd(name, id, type);
	}

	@Override
	public boolean isExistEdit(String name, Long id, int type, Long pId) throws Exception {
		return termDefinition.isExistEdit(name, id, type, pId);
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		termDefinition.moveNodes(listIds, pId, updatePersonId, moveNodeType);

	}

	@Override
	public void reName(String name, Long id,Long updatePeopleId) throws Exception {
		termDefinition.reName(name, id,updatePeopleId);

	}

	@Override
	public List<JecnTreeBean> searchByName(String name) throws Exception {
		return termDefinition.searchByName(name);
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception {
		termDefinition.updateSortNodes(list, pId, updatePersonId);

	}

	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long peopleId,Long projectId) throws Exception {
		return termDefinition.searchRoleAuthByName(name,peopleId,projectId);
	}

	@Override
	public TermDefinitionLibrary getTermDefinitionLibraryById(Long id) throws Exception {
		return termDefinition.getTermDefinitionLibraryById(id);
	}

	@Override
	public List<JecnTreeBean> getChildDirs(Long pId) throws Exception {
		return termDefinition.getChildDirs(pId);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id) throws Exception {
		return termDefinition.getPnodes(id);
	}

	@Override
	public List<TermDefinitionLibrary> listTermsByIds(List<Long> idList) throws Exception {
		return termDefinition.listTermsByIds(idList);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChilds(long pid, Long projectId, long userId) {
		return termDefinition.getRoleAuthChilds(pid,projectId,userId);
	}
}
