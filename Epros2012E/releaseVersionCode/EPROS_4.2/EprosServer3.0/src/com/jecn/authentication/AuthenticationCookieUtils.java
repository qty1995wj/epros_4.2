package com.jecn.authentication;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.jecn.epros.server.common.JecnConfigTool;

public class AuthenticationCookieUtils {

	public static final String TOKEN_COOKIE_NAME = "Auth-Token";
	public static final String LOGIN_NAME = "loginUserName";
	public static final String TRUE_NAME = "loginTrueName";

	public static Cookie createAuthTokenCookie(String token, boolean secure, int expiry, String domain) {
		Cookie cookie = new Cookie(TOKEN_COOKIE_NAME, token);
		cookie.setSecure(secure);
		cookie.setMaxAge(expiry);
		cookie.setDomain(domain);
		cookie.setPath("/");
		return cookie;
	}

	public static Cookie createAuthLoginNameCookie(String loginName, boolean secure, int expiry, String domain)
			throws UnsupportedEncodingException {
		Cookie cookie = new Cookie(LOGIN_NAME, URLEncoder.encode(loginName, "UTF-8"));
		cookie.setSecure(secure);
		cookie.setMaxAge(expiry);
		cookie.setDomain(domain);
		cookie.setPath("/");
		return cookie;
	}

	public static Cookie createAuthTrueNameCookie(String trueName, boolean secure, int expiry, String domain)
			throws UnsupportedEncodingException {
		Cookie cookie = new Cookie(TRUE_NAME, URLEncoder.encode(trueName, "UTF-8"));
		cookie.setSecure(secure);
		cookie.setMaxAge(expiry);
		cookie.setDomain(domain);
		cookie.setPath("/");
		return cookie;
	}

	public static String getAuthToken(Cookie[] cookies) {
		if(null == cookies) return "";
		for (Cookie c : cookies) {
			if (TOKEN_COOKIE_NAME.equals(c.getName())) {
				return c.getValue();
			}
		}
		return "";
	}

	public static void clearCookie(HttpServletResponse response) {
		Cookie tokenCookie = new Cookie(TOKEN_COOKIE_NAME, null);
		// Cookie tokenCookie1 = new Cookie(TOKEN_COOKIE_NAME, null);
		tokenCookie.setMaxAge(0);
		tokenCookie.setSecure(false);
		tokenCookie.setPath("/");
		tokenCookie.setDomain(JecnConfigTool.getCookieDomain());

		// tokenCookie1.setMaxAge(0);
		// tokenCookie1.setSecure(false);
		// tokenCookie1.setPath("/static-web");
		// tokenCookie1.setDomain(JecnConfigTool.getCookieDomain());
		response.addCookie(tokenCookie);
		// response.addCookie(tokenCookie1);
	}
}
