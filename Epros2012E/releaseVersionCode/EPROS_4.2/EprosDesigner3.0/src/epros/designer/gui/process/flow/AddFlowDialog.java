package epros.designer.gui.process.flow;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.process.JecnProcessCommon;
import epros.designer.gui.system.FlowModeSelectCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProcessUtil;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.MapNameText;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 新建流程图
 * 
 * @author 2012-06-05
 * 
 */
public class AddFlowDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddFlowDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/** 排列方式选择按钮面板 */
	private JecnPanel radioButPanel = new JecnPanel();

	/** 流程名称Lab */
	private JLabel flowNameLab = new JLabel(JecnProperties.getValue("flowNameC"));
	/** 流程名称Field */
	private JecnTextField flowField = new JecnTextField();
	/** 必填提示符 */
	private JLabel flowShowLab = new JLabel("*");

	/** 流程编号Lab */
	private JLabel flowNumLab = new JLabel(JecnProperties.getValue("flowNumC"));
	/** 流程编号Field */
	private JecnTextField flowNumField = new JecnTextField();

	/** 流程客户Lab */
	private JLabel flowClientLab = new JLabel(JecnProperties.getValue("flowClientC"));
	/** 流程客户Field */
	private JecnTextField flowClientField = new JecnTextField();

	/** 起始活动Lab */
	private JLabel startActivityLab = new JLabel(JecnProperties.getValue("startActiveC"));
	/** 起始活动Field */
	private JecnTextField startActivityField = new JecnTextField();

	/** 终止活动Lab */
	private JLabel endActivityLab = new JLabel(JecnProperties.getValue("endActiveC"));
	/** 终止活动Field */
	private JecnTextField endActivityField = new JecnTextField();

	/** 流程输入Lab */
	private JLabel flowInLab = new JLabel(JecnProperties.getValue("flowInC"));
	/** 流程输入Field */
	private JecnTextArea flowInArea = new JecnTextArea();
	private JScrollPane flowInScrollPane = new JScrollPane(flowInArea);

	/** 流程输出Lab */
	private JLabel flowOutLab = new JLabel(JecnProperties.getValue("flowOut"));
	/** 流程输出Field */
	private JecnTextArea flowOutArea = new JecnTextArea();
	private JScrollPane flowOutScrollPane = new JScrollPane(flowOutArea);

	/** 流程模板 */
	private FlowModeSelectCommon flowModeSelectCommon;

	/** 横向 单选按钮 */
	private JRadioButton horizontalRadBut = new JRadioButton(JecnProperties.getValue("horizontal"));
	private JLabel horizonImg = new JLabel();
	private JLabel butEmpty = new JLabel();
	ImageIcon horizontalIm = new ImageIcon("images/icons/horizontal.gif");

	/** 纵向 单选按钮 */
	private JRadioButton verticalRadBut = new JRadioButton(JecnProperties.getValue("vertical"));
	private JLabel verticalImg = new JLabel();
	private JLabel imgEmpty = new JLabel();
	ImageIcon verticalIm = new ImageIcon("images/icons/vertical.gif");

	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 项的内容大小 */
	Dimension dimension = null;
	/** 角色、活动 总面板 */
	private JecnPanel roleRoundRectPanel = new JecnPanel();
	/** 预估角色 */
	private JLabel roleLabel = new JLabel(JecnProperties.getValue("estimateRole"));
	private JSpinner roleSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));
	/** 预估活动 */
	private JLabel roundRectLabel = new JLabel(JecnProperties.getValue("estimateActive"));
	private JSpinner roundRectSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));

	/** 提示Lab */
	private JLabel verfyLab = new JLabel();

	private JecnTreeNode pNode = null;
	private JecnTree jTree = null;
	/***
	 * 是否添加
	 */
	private boolean isShowMode = false;

	public AddFlowDialog(JecnTreeNode pNode, JecnTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		isShowMode = JecnConfigTool.isShowProcessMode();
		if (isShowMode) {
			this.setSize(getWidthMax(), 540);
		} else {
			this.setSize(getWidthMax(), 500);
		}
		this.setTitle(JecnProperties.getValue("newProcess"));
		this.setResizable(true);
		this.setModal(true);

		/** 横向 纵向 按钮实现 单选效果 */
		ButtonGroup bgp = new ButtonGroup();
		bgp.add(horizontalRadBut);
		bgp.add(verticalRadBut);
		// 默认横向按钮选中
		horizontalRadBut.setSelected(true);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		radioButPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		horizontalRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		verticalRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		flowShowLab.setForeground(Color.red);
		horizonImg.setIcon(horizontalIm);
		verticalImg.setIcon(verticalIm);

		// 空白
		dimension = new Dimension(80, 0);
		butEmpty.setPreferredSize(dimension);
		butEmpty.setMaximumSize(dimension);
		butEmpty.setMinimumSize(dimension);

		verfyLab.setForeground(Color.red);

		// 输入
		flowInArea.setBorder(null);
		flowInScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowInScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowInScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 输出
		flowOutArea.setBorder(null);
		flowOutScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowOutScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowOutScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		this.setLocationRelativeTo(null);
		// 布局
		initLayout();
		actionInit();

		if (JecnConfigTool.isAutoNumber()) {
			flowNumField.setEditable(false);
		}
	}

	/***************************************************************************
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(3, 1, 3, 1);
		Insets insetss = new Insets(0, 1, 0, 1);
		Insets insetsCo = new Insets(3, 1, 3, 1);
		Insets insetsImg = new Insets(0, 0, 0, 0);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		int row = 0;
		// ============流程名称====================================
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(flowNameLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(flowField, c);
		c = new GridBagConstraints(3, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(flowShowLab, c);
		row++;
		// ==============流程编号==========================================
		if (!JecnConfigTool.isAutoNumber()) {
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(flowNumLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(flowNumField, c);
		}
		row++;
		// ==============流程客户==========================================
		if (JecnConfigTool.isShowProcessCustomer()) {
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(flowClientLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(flowClientField, c);
			row++;
		}
		// ==============起始活动==========================================
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(startActivityLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(startActivityField, c);
		row++;
		// ==============终止活动==========================================
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(endActivityLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(endActivityField, c);
		row++;
		// ==============流程输入==========================================
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(flowInLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(flowInScrollPane, c);
		row++;
		// ==============流程输出==========================================
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(flowOutLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(flowOutScrollPane, c);

		row++;
		if (isShowMode) {
			// 流程模板
			flowModeSelectCommon = new FlowModeSelectCommon(row, infoPanel, insets, null, this, JecnProperties
					.getValue("flowModelC"), false);
			row++;
		}
		// =============================角色活动================
		c = new GridBagConstraints(0, row, 3, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetsCo, 0, 0);
		infoPanel.add(roleRoundRectPanel, c);
		roleRoundRectPanel.setLayout(new GridBagLayout());
		roleRoundRectPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("roleActive")));

		// 预估角色
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsCo, 0, 0);
		roleRoundRectPanel.add(roleLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetsCo, 0, 0);
		roleRoundRectPanel.add(roleSpinner, c);
		// 预估活动
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsCo, 0, 0);
		roleRoundRectPanel.add(roundRectLabel, c);
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetsCo, 0, 0);
		roleRoundRectPanel.add(roundRectSpinner, c);
		row++;
		// =====================单选按钮面板===================================
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsImg, 0, 0);
		infoPanel.add(radioButPanel, c);
		radioButPanel.setLayout(new GridBagLayout());

		vhRadioSelect(c, insetsImg, insetss);
		row++;
		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insetss, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

	}

	/**
	 * 根据配置显示 横向纵向流程
	 * 
	 * @param c
	 * @param insetsImg
	 * @param insetss
	 */
	private void vhRadioSelect(GridBagConstraints c, Insets insetsImg, Insets insetss) {
		// 默认横向：0; 1：纵向； 2：横向和纵向;
		String value = ConnectionPool.getConfigAciton().selectValueFromCache(ConfigItemPartMapMark.vhFlowSelect);
		if ("0".equals(value)) {
			addHRadio(c, insetsImg, insetss, 0);
		} else if ("1".equals(value)) {
			addVRadio(c, insetsImg, insetss, 1);
			verticalRadBut.setSelected(true);
		} else {
			addHRadio(c, insetsImg, insetss, 0);
			addVRadio(c, insetsImg, insetss, 2);
		}
	}

	/**
	 * 只有横向，值为0，横纵都存在 值为2
	 * 
	 * @param c
	 * @param insetsImg
	 * @param insetss
	 * @param gridx
	 */
	private void addHRadio(GridBagConstraints c, Insets insetsImg, Insets insetss, int gridx) {
		// 横向图标
		c = new GridBagConstraints(gridx, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsImg, 0, 0);
		radioButPanel.add(horizonImg, c);
		// 空白
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsImg, 0, 0);
		radioButPanel.add(imgEmpty, c);

		// 横向单选按钮
		c = new GridBagConstraints(gridx, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetss, 0, 0);
		radioButPanel.add(horizontalRadBut, c);
	}

	/**
	 * 
	 * @param c
	 * @param insetsImg
	 * @param insetss
	 * @param gridx
	 *            只有纵向，值为1，横纵都存在 值为2
	 */
	private void addVRadio(GridBagConstraints c, Insets insetsImg, Insets insetss, int gridx) {
		// 纵向图标
		c = new GridBagConstraints(gridx, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsImg, 0, 0);
		radioButPanel.add(verticalImg, c);

		// 空白
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetss, 0, 0);
		radioButPanel.add(butEmpty, c);
		// 纵向单选按钮
		c = new GridBagConstraints(gridx, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetss, 0, 0);
		radioButPanel.add(verticalRadBut, c);
	}

	private void actionInit() {

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 确定

		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

	}

	/***************************************************************************
	 * 确定
	 */
	private void okButPerformed() {
		// 流程名称
		String flowName = flowField.getText().toString().trim();
		if (DrawCommon.isNullOrEmtryTrim(flowName)) {
			// 名称不能为空
			verfyLab.setText(JecnProperties.getValue("flowName") + JecnProperties.getValue("isNotEmpty"));
			return;
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(flowName)) {
			verfyLab.setText(JecnProperties.getValue("flowName") + JecnProperties.getValue("notInputThis"));
			return;
		} else if (JecnUserCheckUtil.getTextLength(flowName) > 122) {
			// 不能超过122个字符或61个汉字
			verfyLab.setText(JecnProperties.getValue("flowName") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 流程编号
		if (DrawCommon.checkNameMaxLength(this.flowNumField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("flowNum") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 客户
		if (DrawCommon.checkNameMaxLength(flowClientField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("flowClient") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 起始活动
		if (DrawCommon.checkNameMaxLength(startActivityField.getText().toString())) {
			verfyLab.setText(startActivityLab.getText().substring(0, startActivityLab.getText().length() - 1)
					+ JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 终止活动
		if (DrawCommon.checkNameMaxLength(endActivityField.getText().toString())) {
			verfyLab.setText(endActivityLab.getText().substring(0, endActivityLab.getText().length() - 1)
					+ JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 输入
		if (DrawCommon.checkNoteLength(flowInArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("actIn") + JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 输出
		if (DrawCommon.checkNoteLength(flowOutArea.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("actOut") + JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 角色个数
		int roles = Integer.valueOf(roleSpinner.getValue().toString()).intValue();
		// 活动个数
		int actives = Integer.valueOf(roundRectSpinner.getValue().toString()).intValue();

		// 点击确定验证流程面板活动和角色个数是否超出范围
		if (JecnValidateCommon.checkRoleAndActiveLimit(roles, actives)) {
			return;
		}
		try {
			if (validateNamefullPath(flowName, 1)) {
				return;
			}
			// 判断是否重名
			if (ConnectionPool.getProcessAction().validateAddName(flowName, this.pNode.getJecnTreeBean().getId(), 1,
					JecnConstants.projectId)) {
				verfyLab.setText(JecnProperties.getValue("nameHaved"));
				return;
			}
		} catch (Exception e1) {
			log.error("AddFlowDialog okButPerformed is error", e1);
		}

		// 流程图基本信息表
		JecnFlowBasicInfoT jecnFlowBasicInfoT = new JecnFlowBasicInfoT();
		jecnFlowBasicInfoT.setFlowName(flowField.getText().trim());
		jecnFlowBasicInfoT.setFlowCustom(flowClientField.getText().trim());
		jecnFlowBasicInfoT.setFlowStartpoint(startActivityField.getText().trim());
		jecnFlowBasicInfoT.setFlowEndpoint(endActivityField.getText().trim());
		jecnFlowBasicInfoT.setInput(flowInArea.getText().trim());
		jecnFlowBasicInfoT.setOuput(flowOutArea.getText().trim());
		jecnFlowBasicInfoT.setIsXorY(horizontalRadBut.isSelected() ? 0L : 1L);

		try {
			Long isPublic = 0L;
			// 获取父节点的密级，部门权限，岗位权限
			LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
					pNode.getJecnTreeBean().getId(), 0);
			// 新建流程图时候如果 全局权限设置公开则新建流程图也是公开
			if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubPartMap)) {
				isPublic = 1L;
			} else {
				isPublic = Long.valueOf(lookPopedomBean.getIsPublic());
			}
			// 岗位权限
			String posIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosList());
			// 组织权限
			String orgIds = JecnDesignerCommon.getLook(lookPopedomBean.getOrgList());
			// 岗位组权限
			String posGroupIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosGroupList());

			// 新增到数据库
			JecnFlowStructureT jecnFlowStructureT = JecnProcessCommon.createProcess(flowName, flowNumField.getText(),
					isPublic, orgIds, posIds, "", pNode, jecnFlowBasicInfoT, jTree, posGroupIds);
			if (this.isShowMode && flowModeSelectCommon.getIdResult() != null) {
				JecnProcessUtil.initWorkflowByModeId(flowModeSelectCommon.getIdResult(), jecnFlowStructureT);
			} else {
				// 客户
				String flowClient = flowClientField.getText();
				// 起始活动
				String startActivity = startActivityField.getText();
				// 终止活动
				String endActivit = endActivityField.getText();
				// 生成一个面板
				JecnFlowMapData totalMapData = JecnDesignerCommon.createFlowMapData(MapType.partMap, jecnFlowStructureT
						.getFlowId());
				totalMapData.setName(flowName);
				boolean isHFlag = horizontalRadBut.isSelected() ? true : false;
				totalMapData.setRoleCount(roles);
				totalMapData.setRoundRectCount(actives);
				totalMapData.setHFlag(isHFlag);
				totalMapData.setSaveTrue();
				// 输入
				totalMapData.getFlowOperationBean().setFlowInput(flowInArea.getText());
				// 输出
				totalMapData.getFlowOperationBean().setFlowOutput(flowOutArea.getText());
				// 客户
				totalMapData.getFlowOperationBean().setFlowCustom(flowClient);
				// 起始活动
				totalMapData.getFlowOperationBean().setFlowStartActive(startActivity);
				// 终止活动
				totalMapData.getFlowOperationBean().setFlowEndActive(endActivit);
				JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);
				// 创建流程图
				getCreateFlow(totalMapData, flowClient, startActivity, endActivit);
			}
			JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			drawDesktopPane.getTabbedTitlePanel().setTabIcon(
					(Icon) JecnTreeRenderer.class.getField(TreeNodeType.process.toString() + "NoPub").get(null));
			// 角色移动
			if (JecnSystemStaticData.isShowRoleMove()) {
				drawDesktopPane.removeJecnRoleMobile();
				// 添加角色移动
				drawDesktopPane.createAddJecnRoleMobile();
			} else {
				// 移除角色移动
				drawDesktopPane.removeJecnRoleMobile();
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("newProcessException"));
			log.error(JecnProperties.getValue("newProcessException"), e);
		}
		this.setVisible(false);
	}

	/**
	 * 提示 文件库中是否有重名文件
	 * 
	 * @param names
	 * @return
	 */
	private boolean validateNamefullPath(String flowName, int type) {
		try {
			String ns = ConnectionPool.getProcessAction().validateNamefullPath(flowName,
					this.pNode.getJecnTreeBean().getId(), type, JecnConstants.projectId);
			if (!"".equals(ns)) {
				String tipText = ns + " "+JecnProperties.getValue("itExistsDirectories");
				// 是否删除提示框
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), tipText, null,
						JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 创建流程图
	 * 
	 * @author fuzhh Jan 28, 2013
	 * @param partMapData
	 *            流程图数据
	 * @param flowClient
	 *            客户
	 * @param startActivity
	 *            起始活动
	 * @param endActivity
	 *            终止活动
	 */
	public void getCreateFlow(JecnFlowMapData partMapData, String flowClient, String startActivity, String endActivity) {
		// 获取面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		workflow.getFlowMapData().setHFlag(partMapData.isHFlag());
		// 横纵向
		if (partMapData.isHFlag()) {

			// 存在时间轴 添加元素坐标下移45像素
			int valueY = JecnDesignerProcess.getDesignerProcess().isShowProcessCustomer() ? 45 : -10;

			for (int k = 0; k <= partMapData.getRoleCount(); k++) {
				// 添加横分割线
				JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.ParallelLines, LineDirectionEnum.horizontal);
				ParallelLines baseVHLine = new ParallelLines(vhLineData);
				// 设置层级
				baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
				// 设置位置
				JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(0, 160 + valueY + 100 * k));
			}

			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.VerticalLine, LineDirectionEnum.vertical);
			VerticalLine baseVHLine = new VerticalLine(vhLineData);
			// 设置层级
			baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
			JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(160, 0));
			// 添加角色
			for (int i = 0; i < partMapData.getRoleCount(); i++) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowRoleType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(30, 180 + valueY + i * 100);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}
			int x = 180;
			if (JecnConfigTool.isShowBeginningEndElement()) {
				// 添加流程开始
				JecnBaseFlowElementPanel startFigureAR = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.FlowFigureStart);
				JecnBaseFigurePanel startFigureARFigure = (JecnBaseFigurePanel) startFigureAR;
				Point startPoint = new Point(x, 190 + valueY);
				JecnAddFlowElementUnit.addFigure(startPoint, startFigureARFigure);
				x = 330;
			}
			// 添加活动
			int index = 0;
			for (int j = 0; j < partMapData.getRoundRectCount(); j++) {

				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowActiveType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				if (j == 0 && startActivity != null && !"".equals(startActivity)) {
					baseFigure.getFlowElementData().setFigureText(startActivity);
				} else if (j == partMapData.getRoundRectCount() - 1 && endActivity != null && !"".equals(endActivity)) {
					baseFigure.getFlowElementData().setFigureText(endActivity);
				}
				Point point = new Point(x + j * 150, 180 + valueY);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
				index = j;
			}

			if (JecnConfigTool.isShowBeginningEndElement()) {
				// 添加流程结束
				JecnBaseFlowElementPanel startFigureAR = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.FlowFigureStop);
				JecnBaseFigurePanel startFigureARFigure = (JecnBaseFigurePanel) startFigureAR;
				Point startPoint = new Point(x + (index + 1) * 150, 190 + valueY);
				JecnAddFlowElementUnit.addFigure(startPoint, startFigureARFigure);
			}

			if (JecnConfigTool.isShowProcessCustomer()) {
				// 添加客户
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.CustomFigure);
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(30, 80 + valueY);
				if (flowClient != null && !"".equals(flowClient)) {
					baseFigure.getFlowElementData().setFigureText(flowClient);
				}
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}

			workflow.getCurrDrawPanelSizeByPanelList();
			// 添加自由文本框
			JecnBaseFlowElementPanel freeNameText = JecnCreateFlowElement.getFlowEmelentByType(MapElemType.MapNameText);
			MapNameText mapNameText = (MapNameText) freeNameText;
			Point freeTextPoint = new Point(workflow.getPreferredSize().width / 2, 30);
			mapNameText.resizeByFigureText();
			JecnAddFlowElementUnit.addFigure(freeTextPoint, mapNameText);
		} else {
			// 横竖分割线
			for (int k = 0; k <= partMapData.getRoleCount(); k++) {
				// 添加竖分割线
				JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.VerticalLine, LineDirectionEnum.vertical);
				VerticalLine baseVHLine = new VerticalLine(vhLineData);
				// 设置层级
				baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
				JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(195 + 150 * k, 0));
			}
			// 添加横分割线
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.ParallelLines, LineDirectionEnum.horizontal);
			ParallelLines baseVHLine = new ParallelLines(vhLineData);
			// 设置层级
			baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
			JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(0, 110));

			// 添加角色
			for (int i = 0; i < partMapData.getRoleCount(); i++) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowRoleType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(220 + i * 150, 30);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}

			// 添加活动
			for (int j = 0; j < partMapData.getRoundRectCount(); j++) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowActiveType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				if (j == 0 && startActivity != null && !"".equals(startActivity)) {
					baseFigure.getFlowElementData().setFigureText(startActivity);
				} else if (j == partMapData.getRoundRectCount() - 1 && endActivity != null && !"".equals(endActivity)) {
					baseFigure.getFlowElementData().setFigureText(endActivity);
				}
				Point point = new Point(220, 130 + j * 100);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}
			// 添加客户
			if (JecnConfigTool.isShowProcessCustomer()) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.CustomFigure);
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				if (flowClient != null && !"".equals(flowClient)) {
					baseFigure.getFlowElementData().setFigureText(flowClient);
				}
				Point point = new Point(70, 30);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}

			workflow.getCurrDrawPanelSizeByPanelList();

			// 添加自由文本框
			JecnBaseFlowElementPanel freeText = JecnCreateFlowElement.getFlowEmelentByType(MapElemType.MapNameText);
			MapNameText mapNameText = (MapNameText) freeText;
			mapNameText.getFlowElementData().setFigureText(workflow.getFlowMapData().getName());

			Point freeTextPoint = new Point(30, workflow.getPreferredSize().height / 2);
			mapNameText.resizeByFigureText();
			JecnAddFlowElementUnit.addFigure(freeTextPoint, mapNameText);

		}
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private boolean isUpdate() {
		// 流程名称
		if (this.flowField.getText() != null && !"".equals(this.flowField.getText())) {
			return true;
		}
		// 流程编号
		if (this.flowNumField.getText() != null && !"".equals(this.flowNumField.getText())) {
			return true;
		}
		// 客户
		if (this.flowClientField.getText() != null && !"".equals(this.flowClientField.getText())) {
			return true;
		}
		// 起始活动
		if (this.startActivityField.getText() != null && !"".equals(this.startActivityField.getText())) {
			return true;
		}
		// 终止活动
		if (this.endActivityField.getText() != null && !"".equals(this.endActivityField.getText())) {
			return true;
		}
		// 输入
		if (this.flowInArea.getText() != null && !"".equals(this.flowInArea.getText())) {
			return true;
		}
		// 输出
		if (this.flowOutArea.getText() != null && !"".equals(this.flowOutArea.getText())) {
			return true;
		}
		return false;
	}

	class VerifLabel extends JLabel {
		public VerifLabel() {
			initComponents();
		}

		private void initComponents() {
			// 项的内容大小
			dimension = new Dimension(450, 15);
			this.setPreferredSize(dimension);
			this.setMinimumSize(dimension);
			this.setMaximumSize(dimension);
			this.setForeground(Color.red);
		}
	}
}
