package com.jecn.epros.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.TreeNodeUtils;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class MyStar {

    private static final String[] TYPE = new String[]{"流程", "架构", "制度", "风险", "文件", "标准"};

    private static final String[] ENTYPE = new String[]{"Process", "Process Map", "Institution", "Risk", "File", "Standard"};

    @ApiModelProperty("主键")
    private String id;
    @ApiModelProperty("用户Id")
    private Long userId;
    @ApiModelProperty("关联Id")
    private Long relatedId;
    @ApiModelProperty("类别")
    private Integer type;
    @ApiModelProperty("名称")
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    @ApiModelProperty("创建时间")
    private Date createDate;
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    @ApiModelProperty("排序时间")
    private Date sortedDate;

    @ApiModelProperty("资源链接")
    public LinkResource getLink() {
        LinkResource link = new LinkResource(relatedId, name);
        switch (TYPE[type]) {
            case "流程":
                link.setType(LinkResource.ResourceType.PROCESS);
                break;
            case "架构":
                link.setType(LinkResource.ResourceType.PROCESS_MAP);
                break;
            case "制度":
                link.setType(LinkResource.ResourceType.RULE);
                break;
            case "风险":
                link.setType(LinkResource.ResourceType.RISK);
                break;
            case "文件":
                link.setType(LinkResource.ResourceType.FILE);
                break;
            case "标准":
                link.setType(LinkResource.ResourceType.STANDARD);
                break;
        }
        link.setFrom(TreeNodeUtils.NodeType.star);
        return link;
    }

    public String getTypeName() {
        if (type == null) {
            return "ERROR";
        }
        return AuthenticatedUserUtil.isLanguageType() == 1 ? ENTYPE[type] : TYPE[type];
    }

    public String getTypeEnName() {
        if (type == null) {
            return "ERROR";
        }
        return ENTYPE[type];
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getSortedDate() {
        return sortedDate;
    }

    public void setSortedDate(Date sortedDate) {
        this.sortedDate = sortedDate;
    }

    public static Integer getStarType(TreeNodeUtils.NodeType nodeType) {
        Integer type = -1;
        switch (nodeType) {
            case process:
            case processFile:
                type = 0;
                break;
            case processMap:
                type = 1;
                break;
            case ruleFile:
            case ruleModelFile:
                type = 2;
                break;
            case risk:
                type = 3;
                break;
            case file:
                type = 4;
                break;
            case standard:
            case standardProcess:
            case standardProcessMap:
            case standardClauseRequire:
            case standardClause:
                type = 5;
                break;
            default:
                throw new IllegalArgumentException("getStarType不支持的类型:" + nodeType);
        }
        return type;
    }
}
