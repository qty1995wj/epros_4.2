<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<script type="text/javascript" src="${basePath}js/common.js">

</script>
		<script type="text/javascript" src="${basePath}js/firstPage.js"
			charset="UTF-8">

</script>
		<!-- 文件详情 -->
		<title><s:text name="stanDetail" /></title>
		<script type="text/javascript">
window.onresize = function() {
	setShowStandardDetailFile(relatedRuleDivId)
}
</script>
	</head>

	<body onload="setShowStandardDetailFile(relatedRuleDivId)"
		style="height: 100%; margin: 0px; padding: 0px;">
		<s:if test="callType!=1">
			<div style="height: 25px; width: 100%; overflow: hidden;"
				align="right" class="gradient minWidth">
				<%@include file="/navigation.jsp"%>
			</div>
		</s:if>
		<div id="div" style="height: 100%;">
			<div id="rule" style="margin: 10px 15px 15px 15px;" align="left">
				<s:if test="standardType==1">
					<table width="97%;">
						<tr>
							<td width="120px;" class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 文件名称： -->
								<s:text name="fileNameC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<a target='_blank'
									href="downloadFile.action?fileId=<s:property
								value='standardDetailBean.fileBean.fileId' />&isPerm=false"
									class="table"> <s:property
										value="standardDetailBean.fileBean.fileName" /> </a>
							</td>
						</tr>
						<tr>
							<td class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 文件编号： -->
								<s:text name="fileNumberC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<s:property value='standardDetailBean.fileBean.fileNum' />
							</td>
						</tr>
						<tr>
							<td class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 密级： -->
								<s:text name="intensiveC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<s:if test="standardDetailBean.secretLevel== 1">
									<s:property value="#application.public"/>
								</s:if>
								<s:elseif test="standardDetailBean.secretLevel == 0">
									${secret}
								</s:elseif>
							</td>
						</tr>
			
				
					</table>
				</s:if>
				<s:elseif test="standardType==4">
					<table width="97%;">
						<tr>
							<td width="140px;" class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 名称： -->
								<s:text name="nameC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<s:property value="standardDetailBean.standardWebBean.name" />
							</td>
						</tr>
						<tr>
							<td class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 是否形成程序文件： -->
								<s:text name="isProfileC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<s:if test='standardDetailBean.standardWebBean.isProfile==0'>
									<s:text name="no" />
								</s:if>
								<s:else>
									<s:text name="is" />
								</s:else>
							</td>
						</tr>
						<tr>
							<td width="140px;" class="td_dashed" valign="top">
								<img src="${basePath}images/common/triangle.gif">
								<!-- 内容： -->
								<s:text name="mesnonullgcontent" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<s:property
									value="standardDetailBean.standardWebBean.stanContent"
									escape="false" />
							</td>
						</tr>
					</table>
				</s:elseif>
				<s:elseif test="standardType==5">
					<table width="97%;">
						<tr>
							<td width="140px;" class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 名称： -->
								<s:text name="belongClause" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<s:property value="standardDetailBean.standardWebBean.pname" />
							</td>
						</tr>
						<tr>
							<td width="140px;" class="td_dashed" valign="top">
								<img src="${basePath}images/common/triangle.gif">
								<!-- 内容： -->
								<s:text name="mesnonullgcontent" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<s:property
									value="standardDetailBean.standardWebBean.stanContent"
									escape="false" />
							</td>
						</tr>
					</table>
				</s:elseif>

			</div>
			<!--相关流程 -->
			<div id="relatedProcessDiv" style="margin: 10px 15px 15px 15px;"
				align="left">
				<table width="98%" style="margin-bottom: 5px;">
					<tr style="width: 100%;">
						<td
							style="font-weight: bold; font-size: 13px; padding: 8px 12px 5px 12px;">
							<s:text name="relatedProcess" />
						</td>
					</tr>
				</table>

				<table width="97%" border="0" cellpadding="0" cellspacing="1"
					align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
					<tr class="FixedTitleRow" align="center">
						<td width="2%" class="gradient"></td>
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 流程名称 -->
							<s:text name="processName" />
						</td>
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 流程编号 -->
							<s:text name="processNumber" />
						</td>
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 责任部门-->
							<s:text name="responsibilityDepartment" />
						</td>
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 责任人 -->
							<s:text name="responsiblePersons" />
						</td>
						<td width="2%" class="gradient"></td>
					</tr>

					<s:iterator value="standardDetailBean.listProcessBean" status="st">
						<tr align="center" bgcolor="#ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#F7F7F7';" height="20">
							<td align="right" class="gradient">
								<s:property value="#st.index=1" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<a
									href="process.action?type=process&flowId=<s:property value='flowId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="flowName" /> </a>
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="flowIdInput" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<a href="organization.action?orgId=<s:property value='orgId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="orgName" /> </a>
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="resPeopleName" />
							</td>
							<td></td>
						</tr>
					</s:iterator>
				</table>
			</div>
			<!--相关制度 -->
			<div id="relatedRuleDivId" style="margin: 10px 15px 15px 15px;"
				align="left">
				<table width="98%" style="margin-bottom: 5px;">
					<tr style="width: 100%;">
						<td
							style="font-weight: bold; font-size: 13px; padding: 8px 12px 5px 12px;">
							<s:text name="relatedRule" />
						</td>
					</tr>
				</table>

				<table width="97%" border="0" cellpadding="0" cellspacing="1"
					align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
					<tr class="FixedTitleRow" align="center">
						<td width="2%" class="gradient"></td>
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 制度名称 -->
							<s:text name="ruleName" />
						</td>
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 制度编号 -->
							<s:text name="ruleNumber" />
						</td>
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 责任部门-->
							<s:text name="responsibilityDepartment" />
						</td>
						<s:if test="#request.ruleTypeIsShow">
						<td width="24%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 制度类别 -->
							<s:text name="ruleType" />
						</td>
						</s:if>
						<td width="2%" class="gradient"></td>
					</tr>

					<s:iterator value="standardDetailBean.listRule" status="st">
						<tr align="center" bgcolor="#ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#F7F7F7';" height="20">
							<td align="right" class="gradient">
								<s:property value="#st.index=1" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<a
									href="rule.action?type=<s:if test="isDir== 1">ruleModeFile</s:if><s:else>ruleFile</s:else>&ruleId=<s:property value='ruleId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="ruleName" /> </a>
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="ruleNum" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<a
									href="organization.action?orgId=<s:property value='dutyOrgId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="dutyOrg" /> </a>
							</td>
							<s:if test="#request.ruleTypeIsShow">
							<td class="t5" style="word-break: break-all;">
								<s:property value="ruleTypeName" />
							</td>
							</s:if>
							<td></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</div>
	</body>
</html>