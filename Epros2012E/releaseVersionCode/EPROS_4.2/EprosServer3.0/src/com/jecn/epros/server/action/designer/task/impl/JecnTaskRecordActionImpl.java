package com.jecn.epros.server.action.designer.task.impl;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.action.designer.task.IJecnTaskRecordAction;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.PrfRelatedTemplet;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.task.TaskTemplet;
import com.jecn.epros.server.bean.task.temp.JecnTaskResubmitDesignerBean;
import com.jecn.epros.server.service.task.design.IJecnTaskRecordService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class JecnTaskRecordActionImpl implements IJecnTaskRecordAction {
	/** 任务审批关联查询及验证接口 */
	private IJecnTaskRecordService taskRecordService;

	/**
	 * 获得人员名称和人员主键记录的集合信息
	 * 
	 * @param set
	 *            人员主键集合 , Long projectId
	 * @return 人员名称和人员主键记录的集合信息
	 * @throws Exception
	 */
	public List<Object[]> getJecnUserByUserIds(Set<Long> set, Long projectId) throws Exception {
		return taskRecordService.getJecnUserByUserIds(set, projectId);
	}

	public IJecnTaskRecordService getTaskRecordService() {
		return taskRecordService;
	}

	public void setTaskRecordService(IJecnTaskRecordService taskRecordService) {
		this.taskRecordService = taskRecordService;
	}

	/**
	 * 判断审批人是否为相同人员
	 * 
	 * @param userIds
	 * @return true 存在相同人员
	 * @throws Exception
	 */
	public boolean checkAuditPeopleIsSame(Set<Long> userIds) throws Exception {
		return false;
	}

	/**
	 * 验证版本号是否存在
	 * 
	 * @param id
	 *            流程或制度ID
	 * @param version
	 *            版本号
	 * @param type
	 *            类型 0 流程，1文件，2制度
	 * @return true 版本好存在
	 * @throws DaoException
	 */
	public boolean isExistVersionbyFlowId(Long id, String version, int type) throws Exception {
		if (version == null || "".equals(version.trim())) {
			return false;
		}
		return taskRecordService.isExistVersionbyFlowId(id, version, type);
	}

	public boolean isEdit(Long rid, TreeNodeType nodeType) throws Exception {
		return taskRecordService.isEdit(rid, nodeType);
	}

	/**
	 * 是否审批
	 * 
	 * @param rid
	 *            关联ID
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean isInTask(Long rid, TreeNodeType nodeType) throws Exception {
		return taskRecordService.isInTask(rid, nodeType);
	}

	/**
	 * 是否审批
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean idsIsInTask(List<Long> rids, TreeNodeType nodeType) throws Exception {
		return taskRecordService.idsIsInTask(rids, nodeType);
	}

	/**
	 * 验证人员是否存在岗位
	 * 
	 * @param peopleid
	 *            人员主键ID
	 * @return true ：存在岗位
	 * @throws Exception
	 */
	@Override
	public boolean isExsitPos(Long peopleid) throws Exception {
		int count = taskRecordService.isExsitPos(peopleid);
		if (count > 0) {// 存在岗位
			return true;
		}
		return false;
	}

	/**
	 * 提交审批验证
	 * 
	 * @param rid
	 *            关联ID
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean submitToTask(Long rid, TreeNodeType nodeType) throws Exception {
		return taskRecordService.submitToTask(rid, nodeType);
	}

	@Override
	public boolean reSubmitToTask(Long rid, TreeNodeType nodeType) throws Exception {
		return taskRecordService.reSubmitToTask(rid, nodeType);
	}

	/**
	 * 是否已发布
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点是已发布状态
	 * @throws Exception
	 */
	@Override
	public boolean idsIsPub(List<Long> rids, TreeNodeType nodeType) throws Exception {
		return taskRecordService.idsIsPub(rids, nodeType);
	}

	/**
	 * 制度文件引用文件管理中的文件是否审批
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean idsIsInRuleTask(List<Long> rids, TreeNodeType nodeType) throws Exception {
		return taskRecordService.idsIsInRuleTask(rids, nodeType);
	}

	/**
	 * 文件树节点在制度文件中是否已发布（文件、制度文件）
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点是已发布状态
	 * @throws Exception
	 */
	@Override
	public boolean idsIsRulePubByFile(List<Long> rids, TreeNodeType nodeType) throws Exception {
		return taskRecordService.idsIsRulePubByFile(rids, nodeType);
	}

	@Override
	public List<TaskTemplet> listTaskTempletsByType(int taskType) throws Exception {
		return taskRecordService.listTaskTempletsByType(taskType);
	}

	@Override
	public List<TaskConfigItem> listTaskConfigItemByTempletId(String templetId) throws Exception {

		return taskRecordService.listTaskConfigItemByTempletId(templetId);
	}

	@Override
	public TaskTemplet saveTaskTemplet(TaskTemplet taskTemplet) throws Exception {
		return taskRecordService.saveTaskTemplet(taskTemplet);
	}

	@Override
	public void updateTaskTemplet(TaskTemplet taskTemplet) throws Exception {

		taskRecordService.updateTaskTemplet(taskTemplet);

	}

	@Override
	public boolean validateUpdateName(String name, String id, int taskType) throws Exception {

		return taskRecordService.validateUpdateName(name, id, taskType);
	}

	@Override
	public void deleteTaskTemplet(List<String> listIds) throws Exception {

		taskRecordService.deleteTaskTemplet(listIds);

	}

	@Override
	public void updateTaskConfigItems(List<TaskConfigItem> configs) throws Exception {
		taskRecordService.updateTaskConfigItems(configs);
	}

	@Override
	public void deletePrfRelated(Long prfId, int taskType) throws Exception {
		taskRecordService.deletePrfRelated(prfId, taskType);
	}

	public void saveOrUpdatePrfRelated(Long prfId, int taskType, String approveId, String borrowId, String desuetudeId,
			Long updatePeopleId) throws Exception {
		taskRecordService.saveOrUpdatePrfRelated(prfId, taskType, approveId, borrowId, desuetudeId, updatePeopleId);
	}

	@Override
	public List<PrfRelatedTemplet> listPrfRelatedTemplet() throws Exception {
		return taskRecordService.listPrfRelatedTemplet();
	}

	@Override
	public boolean validateAddName(String name, int taskType) throws Exception {
		return taskRecordService.validateAddName(name, taskType);
	}

	@Override
	public JecnTaskResubmitDesignerBean getReSubmitTaskBeanNew(Long rid, TreeNodeType nodeType) throws Exception {
		return taskRecordService.getReSubmitTaskBeanNew(rid, nodeType);
	}

	@Override
	public List<JecnTaskStage> getJecnTaskStageList(Long taskId) {
		return taskRecordService.getJecnTaskStageList(taskId);
	}

	@Override
	public String getTempletIdByTempletType(Long id, int taskType, Integer templetType) throws Exception {
		return taskRecordService.getTempletIdByTempletType(id, taskType, templetType);
	}

	@Override
	public int getChildCount(Long id, TreeNodeType treeNodeType) throws Exception {
		return taskRecordService.getChildCount(id, treeNodeType);
	}
}
