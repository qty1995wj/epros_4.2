package epros.designer.gui.system.seting;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnRecycleDialog;
import epros.designer.gui.project.RecycleProjectManageDiaolog;
import epros.designer.gui.recycle.FileRecyclePanel;
import epros.designer.gui.recycle.OrgRecyclePanel;
import epros.designer.gui.recycle.ProcessRecyclePanel;
import epros.designer.gui.recycle.RuleRecyclePanel;
import epros.designer.gui.system.JecnToolbarButton;
import epros.designer.util.JecnProperties;

/**
 * 
 * 回收站按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnRecover implements ActionListener {
	/** 项目回收按钮标识 */
	private final String PROJECT_RECOVER_CMD = "projectRecover";
	/** 流程回收按钮标识 */
	private final String PROCESS_RECOVER_CMD = "processRecover";
	/** 组织回收按钮标识 */
	private final String ORG_RECOVER_CMD = "orgRecover";
	/*** 文件回收按钮表示 */
	private final String FILE_RECOVER_CMD = "fileRecover";
	/*** 制度回收按钮表示 */
	private final String RULE_RECOVER_CMD = "ruleRecover";
	/** 选择按钮标识 */
	private final String SELECTEDING_CMD = "selectedingRecover";

	/** 回收站容器 */
	private JecnGroupButton groupRecoverBtn = null;
	/** 当前选中回收站 */
	private JecnSelectedButton currSelectedBtn = null;
	/** 选择按钮 */
	private JecnSelectingButton selectingBtn = null;

	/** 项目回收 */
	private JecnToolbarButton projectRecoverButton = null;
	/** 流程回收 */
	private JecnToolbarButton processRecoverButton = null;
	/** 组织回收 */
	private JecnToolbarButton orgRecoverButton = null;
	/** 文件回收 */
	private JecnToolbarButton fileRecoverButton = null;
	/** 制度回收 */
	private JecnToolbarButton ruleRecoverButton = null;

	private JecnPopupMenuButton popupMenu = null;

	public JecnRecover() {
		// 初始化组件
		initComponent();
	}

	/**
	 * 
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {
		// 当前选中回收站
		currSelectedBtn = new JecnSelectedButton(this);
		// 选择回收站
		selectingBtn = new JecnSelectingButton();
		// 回收站容器
		groupRecoverBtn = new JecnGroupButton(currSelectedBtn, selectingBtn);

		// 项目回收
		projectRecoverButton = new JecnToolbarButton(JecnProperties.getValue("projectRecover"), PROJECT_RECOVER_CMD);
		// 流程回收
		processRecoverButton = new JecnToolbarButton(JecnProperties.getValue("processRecover"), PROCESS_RECOVER_CMD);
		// 组织回收
		orgRecoverButton = new JecnToolbarButton(JecnProperties.getValue("orgRecover"), ORG_RECOVER_CMD);
		fileRecoverButton = new JecnToolbarButton(JecnProperties.getValue("fileRecover"), FILE_RECOVER_CMD);
		ruleRecoverButton = new JecnToolbarButton(JecnProperties.getValue("ruleRecover"), RULE_RECOVER_CMD);
		

		selectingBtn.setActionCommand(SELECTEDING_CMD);
		processRecoverButton.setActionCommand(PROCESS_RECOVER_CMD);
		projectRecoverButton.setActionCommand(PROJECT_RECOVER_CMD);
		orgRecoverButton.setActionCommand(ORG_RECOVER_CMD);
		fileRecoverButton.setActionCommand(FILE_RECOVER_CMD);
		ruleRecoverButton.setActionCommand(RULE_RECOVER_CMD);

		Dimension thisSize = new Dimension(70, 20);
		// 大小
		currSelectedBtn.setPreferredSize(thisSize);
		currSelectedBtn.setMaximumSize(thisSize);
		currSelectedBtn.setMinimumSize(thisSize);

		// 选择按钮
		selectingBtn.addActionListener(this);
		// 项目回收
		projectRecoverButton.addActionListener(this);
		// 流程回收
		processRecoverButton.addActionListener(this);
		// 组织回收
		orgRecoverButton.addActionListener(this);
		// 文件回收
		fileRecoverButton.addActionListener(this);
		// 制度回收
		ruleRecoverButton.addActionListener(this);

		currSelectedBtn.setTextIcon(processRecoverButton);
	}

	/**
	 * 创建回收站Meun
	 */
	private void createMenu() {
		if (popupMenu == null) {
			List<JecnToolbarButton> btnList = new ArrayList<JecnToolbarButton>();
			btnList.add(processRecoverButton);

			if (JecnDesignerCommon.isAdmin() || JecnDesignerCommon.isSecondAdmin()) {
				// true:显示项目管理
				if (JecnConstants.isShowProjectManagement) {
					// 是否存在浏览端，不存在则隐藏：项目回收
					if (JecnConstants.isPubShow()) {
						btnList.add(projectRecoverButton);
					}
				}
				btnList.add(orgRecoverButton);
			}
			btnList.add(ruleRecoverButton);
			btnList.add(fileRecoverButton);
			popupMenu = new JecnPopupMenuButton(btnList);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String cmdStr = e.getActionCommand();
		createMenu();
		if (SELECTEDING_CMD.equals(cmdStr)) {// 选择按钮
			popupMenu.show(currSelectedBtn, 30, 25);
			popupMenu.setPopupSize(new Dimension(currSelectedBtn.getWidth() + 15, popupMenu.getHeight()));
			popupMenu.repaint();
			return;
		}

		if (popupMenu.isVisible()) {
			popupMenu.setVisible(false);
		}

		JecnRecycleDialog recycleDialog = null;

		if (PROCESS_RECOVER_CMD.equals(cmdStr)) {// 流程回收
			if (JecnConstants.projectId == null) {
				return;
			}
			recycleDialog = new JecnRecycleDialog(new ProcessRecyclePanel());
			recycleDialog.setTitle(JecnProperties.getValue("flowRecoverStation"));
			recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
			recycleDialog.setVisible(true);
			currSelectedBtn.setTextIcon(processRecoverButton);
		} else if (PROJECT_RECOVER_CMD.equals(cmdStr)) {// 项目回收
			if (!JecnDesignerCommon.isAdminTip()) {
				return;
			}
			RecycleProjectManageDiaolog projectDialog = new RecycleProjectManageDiaolog();
			projectDialog.setDefaultCloseOperation(RecycleProjectManageDiaolog.DISPOSE_ON_CLOSE);
			projectDialog.setVisible(true);
			currSelectedBtn.setTextIcon(projectRecoverButton);
		} else if (ORG_RECOVER_CMD.equals(cmdStr)) { // 组织回收
			if (JecnConstants.projectId == null || !JecnDesignerCommon.isAdminTip()) {
				return;
			}
			recycleDialog = new JecnRecycleDialog(new OrgRecyclePanel());
			recycleDialog.setTitle(JecnProperties.getValue("orgRecoverStation"));
			recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
			recycleDialog.setVisible(true);
			currSelectedBtn.setTextIcon(orgRecoverButton);
		}else if (RULE_RECOVER_CMD.equals(cmdStr)) {// 制度回收
			if (JecnConstants.projectId == null) {
				return;
			}
			recycleDialog = new JecnRecycleDialog(new RuleRecyclePanel());
			recycleDialog.setTitle(JecnProperties.getValue("ruleRecoverStation"));
			recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
			recycleDialog.setVisible(true);
			currSelectedBtn.setTextIcon(ruleRecoverButton);
		} else if (FILE_RECOVER_CMD.equals(cmdStr)) {// 文件回收
			if (JecnConstants.projectId == null) {
				return;
			}
			recycleDialog = new JecnRecycleDialog(new FileRecyclePanel());
			recycleDialog.setTitle(JecnProperties.getValue("fileRecoverStation"));
			recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
			recycleDialog.setVisible(true);
			currSelectedBtn.setTextIcon(fileRecoverButton);
		}
	}

	public JecnGroupButton getGroupRecoverBtn() {
		return groupRecoverBtn;
	}
}
