package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.task.TaskHistoryNew;
import com.jecn.epros.email.ToolEmail.MailLoginEnum;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class ProcessPublishRecordHistoryEmailBuilder extends DynamicContentEmailBuilder {

    @Override
    protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {
        TaskHistoryNew historyNew = (TaskHistoryNew) getData()[0];
        // 生成邮件的正文
        EmailContentBuilder contentBuilder = new EmailContentBuilder(getTip());
        contentBuilder.setContent(getEmailContent(historyNew.getModifyExplain(), historyNew.getRelateName(), historyNew
                .getDraftPerson()));
        contentBuilder.setResourceUrl(getEprosResourceUrl(user.getId(), historyNew.getRelateId(), historyNew
                .getType()));

        // 生成邮件信息对象
        EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
        emailBasicInfo.setSubject(getSubject(historyNew.getRelateName()));
        emailBasicInfo.setContent(contentBuilder.buildContent());
        emailBasicInfo.addRecipients(user);
        return emailBasicInfo;
    }

    /**
     * 获取邮件内容
     *
     * @param modifyExplain
     * @param relatedName
     * @param draftPerson
     * @return
     */
    private String getEmailContent(String modifyExplain, String relatedName, String draftPerson) {
        StringBuilder builder = new StringBuilder();

        // 文件名称
        builder.append(JecnProperties.getValue("fileNameC"));
        builder.append(relatedName);
        builder.append(EmailContentBuilder.strBr);

        // 更新时间
        String updateTime = Common.getStringbyDate(new Date());
        builder.append(JecnProperties.getValue("updateTime"));
        builder.append(updateTime);
        builder.append(EmailContentBuilder.strBr);

        // 变更说明
        String desc = "";
        if (StringUtils.isNotBlank(modifyExplain)) {
            desc = modifyExplain.replaceAll("\n", "<br>");
        }
        builder.append(JecnProperties.getValue("changeInstructions"));
        builder.append(desc);
        builder.append(EmailContentBuilder.strBr);

        // 拟稿人
        builder.append(JecnProperties.getValue("Drafter"));
        builder.append(draftPerson);
        builder.append(EmailContentBuilder.strBr);

        return builder.toString();
    }

    /**
     * 获取需要打开的链接信息
     *
     * @param peopleId
     * @param relatedId
     * @param type
     * @return
     */
    private String getEprosResourceUrl(Long peopleId, Long relatedId, Integer type) {
        String link = "";
        switch (type) {
            case 0: // 流程
                link = "/loginMail.action?mailFlowType=process&mailFlowId=" + relatedId + "&mailPeopleId=" + peopleId
                        + "&accessType=" + MailLoginEnum.mailPubApprove.toString();
                break;
            case 1:// 文件
                link = "/loginMail.action?mailFileId=" + relatedId + "&mailPeopleId=" + peopleId + "&accessType="
                        + MailLoginEnum.mailOpenFile.toString() + "&isPub=true";
                break;
            case 2:// 制度模板
                link = "/loginMail.action?mailRuleType=ruleModeFile&mailRuleId=" + relatedId + "&mailPeopleId=" + peopleId
                        + "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
                break;
            case 3:// 制度文件
                link = "/loginMail.action?mailRuleType=ruleFile&mailRuleId=" + relatedId + "&mailPeopleId=" + peopleId
                        + "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
                break;
            case 4:// 流程架构
                link = "/loginMail.action?mailFlowType=processMap&mailFlowId=" + relatedId + "&mailPeopleId=" + peopleId
                        + "&accessType=" + MailLoginEnum.mailPubApprove.toString();
                break;
            default:
                link = "error.jsp";
        }
        return link;
    }

    private String getSubject(String relatedName) {
        return '"' + relatedName + '"' + "," + ToolEmail.IS_FINISH_PUB;
    }

}
