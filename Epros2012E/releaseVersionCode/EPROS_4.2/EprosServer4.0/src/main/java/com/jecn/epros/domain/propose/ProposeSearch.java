package com.jecn.epros.domain.propose;

import com.jecn.epros.service.TreeNodeUtils.NodeType;

/**
 * 合理化建议搜索
 *
 * @author user
 */
public class ProposeSearch {
    /**
     * 主题id
     **/
    private Long relatedId;
    /**
     * 0流程，1制度，2流程架构，3文件，4风险，5标准
     **/
    private Integer type;
    /**
     * 当前选中的是那个tab,也就是当前在哪个state
     **/
    private Integer curState = -1;

    private NodeType nodeType;

    private boolean myPropose;

    public boolean isMyPropose() {
        return myPropose;
    }

    public void setMyPropose(boolean myPropose) {
        this.myPropose = myPropose;
    }

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public Integer getType() {
        if (type == null && nodeType != null) {
            switch (nodeType) {
                case process:
                    return 0;
                case ruleFile:
                    return 1;
                case ruleModelFile:
                    return 1;
                case processMap:
                    return 2;
                case file:
                    return 3;
                case risk:
                    return 4;
                case standard:
                    return 5;
            }
        }
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCurState() {
        return curState;
    }

    public void setCurState(Integer curState) {
        this.curState = curState;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }
}
