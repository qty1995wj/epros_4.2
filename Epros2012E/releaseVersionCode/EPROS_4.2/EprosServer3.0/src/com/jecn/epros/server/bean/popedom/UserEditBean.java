package com.jecn.epros.server.bean.popedom;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class UserEditBean implements java.io.Serializable {
	private String orgNames;
	private List<JecnTreeBean> posList;
	private List<JecnTreeBean> roleList;
	private JecnUser jecnUser;

	public String getOrgNames() {
		return orgNames;
	}

	public void setOrgNames(String orgNames) {
		this.orgNames = orgNames;
	}

	public List<JecnTreeBean> getPosList() {
		return posList;
	}

	public void setPosList(List<JecnTreeBean> posList) {
		this.posList = posList;
	}

	public List<JecnTreeBean> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<JecnTreeBean> roleList) {
		this.roleList = roleList;
	}

	public JecnUser getJecnUser() {
		return jecnUser;
	}

	public void setJecnUser(JecnUser jecnUser) {
		this.jecnUser = jecnUser;
	}

	/**
	 * 
	 * 获取系统角色集合字符串
	 * 
	 * @return String A/B/C格式字符串或“”
	 */
	public String getRoleListString() {
		if (roleList == null || roleList.size() == 0) {
			return "";
		}
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < roleList.size(); i++) {
			JecnTreeBean treeBean = roleList.get(i);
			buff.append(treeBean.getName());
			if (i != roleList.size() - 1) {
				buff.append("/");
			}
		}
		return buff.toString();
	}
}
