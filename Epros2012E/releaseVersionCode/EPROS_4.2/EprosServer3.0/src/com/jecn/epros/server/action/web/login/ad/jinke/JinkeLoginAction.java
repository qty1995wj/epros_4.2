package com.jecn.epros.server.action.web.login.ad.jinke;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.webservice.jinke.IKmextLoginOtherWebService;
import com.jecn.webservice.jinke.IKmextLoginOtherWebServiceServiceLocator;

public class JinkeLoginAction extends JecnAbstractADLoginAction {
	public JinkeLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {
		try {
			return webserviceLogin();
		} catch (Exception e) {
			e.printStackTrace();
			return LoginAction.INPUT;
		}
	}

	private String webserviceLogin() throws Exception {
		String token = loginAction.getRequest().getParameter("token");
		if (StringUtils.isBlank(token)) {
			return LoginAction.INPUT;
		}
		log.info("token = " + token);
		IKmextLoginOtherWebServiceServiceLocator locator = new IKmextLoginOtherWebServiceServiceLocator();
		IKmextLoginOtherWebService service = locator.getIKmextLoginOtherWebServicePort();
		String json = service.getTozkend(token);

		JSONObject jo = JSONObject.parseObject(json);
		String state = jo.getString("returnState");
		String loginName = jo.getString("Message");
		if ("2".equals(state) && !StringUtils.isBlank(loginName)) {
			return loginByLoginName(loginName);
		} else {
			log.error(json);
		}
		return LoginAction.INPUT;
	}

	/**
	 * 返回结果页
	 */
	public String returnPage() {
		return null;
	}

	public static void main(String[] args) {
		String loginName = "adminEPROS";
		try {
			System.out.println(JecnUtil.MD5(loginName.toString().getBytes("UTF-8")).toLowerCase());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
