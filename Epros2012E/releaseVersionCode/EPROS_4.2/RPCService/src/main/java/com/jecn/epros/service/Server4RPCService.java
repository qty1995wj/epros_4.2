package com.jecn.epros.service;

import java.util.Map;

public interface Server4RPCService {

	String getEpsChart(Map<String, Object> params);

	Map<String, Object> downloadProcessDoc(Map<String, Object> params);

	Map<String, Object> downloadProcessXls(Map<String, Object> params);

	Map<String, Object> downloadProcessListXls(Map<String, Object> params);

}
