package epros.designer.table;

import java.util.List;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.process.guide.explain.JecnActiveDescDialog;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;

/**
 * 关键活动中问题区域，关键成功因素，关键控制点使用的table
 * 
 * @author user
 * 
 */
public abstract class JecnActiveTable extends JecnDBClickTable {

	/** 0问题区域 1关键成功因素 2关键控制点3关键控制点 (合规) **/
	private int type;

	private ProcessOperationDialog processOperationDialog;

	public JecnActiveTable(int type, ProcessOperationDialog processOperationDialog) {
		super();
		this.type = type;
		this.processOperationDialog = processOperationDialog;
		setColumnMinValue();
	}

	private void setColumnMinValue() {
		setTableColumn(1, 60);
		setTableColumn(2, 120);
	}

	@Override
	protected void dbClickMethod() {
		int selectRow = this.getSelectedRow();
		List<JecnActivityShowBean> listAllActivityShow = null;
		if (type == 0) {
			listAllActivityShow = processOperationDialog.getFlowOperationBean().getListPAActivityShow();
		} else if (type == 1) {
			listAllActivityShow = processOperationDialog.getFlowOperationBean().getListKSFActivityShow();
		} else if (type == 2) {
			listAllActivityShow = processOperationDialog.getFlowOperationBean().getListKCPActivityShow();
		} else if (type == 3) {
			listAllActivityShow = processOperationDialog.getFlowOperationBean().getListKCPActivityAllowShow();
		}
		JecnActiveDescDialog dialog = new JecnActiveDescDialog(listAllActivityShow.get(selectRow),
				processOperationDialog);
		dialog.setVisible(true);
		this.setRowSelectionInterval(selectRow, selectRow);
		this.updateUI();
	}

	@Override
	public int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	public boolean isSelectMutil() {
		// TODO Auto-generated method stub
		return false;
	}

}
