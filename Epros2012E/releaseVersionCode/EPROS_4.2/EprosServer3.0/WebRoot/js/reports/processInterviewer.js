//流程访问人统计
function processInterviewerGrid() {
	processInterviewerGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "processInterviewer.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "flowId",
								mapping : "processWebBean.flowId"
							}, {
								name : "flowName",
								mapping : "processWebBean.flowName"
							}, {
								name : "flowIdInput",
								mapping : "processWebBean.flowIdInput"
							}, {
								name : "resPeopleName",
								mapping : "processWebBean.resPeopleName"
							}, {
								name : "orgName",
								mapping : "processWebBean.orgName"
							},{
								name : "accessName",
								mapping : "accessName"
							},{
								name : "accessTime",
								mapping : "accessTime"
							}])

			});

	processInterviewerGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			},{
				header : i_interviewer,
				dataIndex : "accessName",
				align : "center",
				menuDisabled : true
			},{
				header : i_accessTime,
				dataIndex : "accessTime",
				align : "center",
				menuDisabled : true
			}

	]);
	processInterviewerGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processInterviewerGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processInterviewerGridPanel = new Ext.grid.GridPanel({
				id : "processInterviewerGridPanel_id",
				renderTo : 'processInterviewerGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-200,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processInterviewerGridStore,
				colModel : processInterviewerGridColumnModel,
				bbar : processInterviewerGridBbar
			})
	reports_index_id='reports_processInterviewer_id'; 		
}

/**
 * 流程访问人统计查询
 */
function processInterviewerGridSearch(){
     // 访问人ID
     var userId = document.getElementById("userId_interview").value;
	 //流程ID
	 var processId=document.getElementById("processId_interview_id").value;
	  //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_interview_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
	 // 部门ID
	 var orgId = document.getElementById("orgLookId_interview_id").value;
	 // 岗位ID
	 var posId = document.getElementById("participateInPostId_interview_id").value.split("_")[0];
	  // 结束时间大于开始时间
	 if (startTime > endTime) {
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return;
	 }
   	 processInterviewerGridStore.setBaseParam("userId", userId);
     processInterviewerGridStore.setBaseParam("processId", processId);
     processInterviewerGridStore.setBaseParam("resOrgId", resOrgId);
     processInterviewerGridStore.setBaseParam("startTime", startTime);
     processInterviewerGridStore.setBaseParam("endTime", endTime);
     processInterviewerGridStore.setBaseParam("orgId", orgId);
     processInterviewerGridStore.setBaseParam("posId", posId);

	 processInterviewerGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
				
}

/**
 * 流程访问人统计重置
 **/
function processInterviewerSearchReset(){
        document.getElementById("orgDutyName_interview_id").value = "";
		document.getElementById("processName_interview_id").value = "";
		document.getElementById("orgDutyId_interview_id").value = "-1";
		document.getElementById("processId_interview_id").value = "-1";
		document.getElementById("startTime").value = "";
		document.getElementById("endTime").value = "";
		document.getElementById("userName_interview").value = "";
		document.getElementById("orgLookName_interview_id").value = "";
		document.getElementById("participateInPostName_interview_id").value = "";
		document.getElementById("userId_interview").value = "-1";
		document.getElementById("orgLookId_interview_id").value = "-1";
		document.getElementById("participateInPostId_interview_id").value = "-1";
}

// 流程访问人下载
function processInterviewerDownloadExcel(){
   // 访问人ID
     var userId = document.getElementById("userId_interview").value;
	 //流程ID
	 var processId=document.getElementById("processId_interview_id").value;
	  //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_interview_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
	 // 部门ID
	 var orgId = document.getElementById("orgLookId_interview_id").value;
	 // 岗位ID
	 var posId = document.getElementById("participateInPostId_interview_id").value;
	 window.location.href="downloadProcessInterviewer.action?userId="+userId+"&processId="+processId+"&resOrgId="+resOrgId
	                        +"&startTime="+startTime+"&endTime="+endTime+"&orgId="+orgId+"&posId="+posId;
}