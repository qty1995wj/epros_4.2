package com.jecn.epros.server.bean.task.temp;

import java.io.Serializable;

public class JecnTaskStateBean implements Serializable {
	private Long statePeopleId;
	private String statePeopleName;
	private int state;

	public Long getStatePeopleId() {
		return statePeopleId;
	}

	public void setStatePeopleId(Long statePeopleId) {
		this.statePeopleId = statePeopleId;
	}

	public String getStatePeopleName() {
		return statePeopleName;
	}

	public void setStatePeopleName(String statePeopleName) {
		this.statePeopleName = statePeopleName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
}
