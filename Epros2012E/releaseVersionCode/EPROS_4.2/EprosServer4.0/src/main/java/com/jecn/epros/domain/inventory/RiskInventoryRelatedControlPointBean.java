package com.jecn.epros.domain.inventory;

import java.util.List;

/**
 * sql
 *
 * @author user
 */
public class RiskInventoryRelatedControlPointBean {
    //以下字段来自于JecnRisk
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 父ID
     */
    private Long parentId;
    /**
     * 编号
     */
    private String riskCode;
    /**
     * 是否是目录：0：是，1：否
     */
    private int isDir;
    /**
     * 风险描述或目录名
     */
    private String riskName;
    /**
     * 等级
     */
    private int grade;

    /**
     * 标准化控制
     */
    private String standardControl;
    /***创建人*/
    private Long createPersonId;
    private String tPath;
    private int tLevel;

    /**
     * 风险关联控制点
     */
    private List<RiskInventoryControlPointBean> listRiskInventoryControlPointBean;

    private String riskType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public int getIsDir() {
        return isDir;
    }

    public void setIsDir(int isDir) {
        this.isDir = isDir;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getStandardControl() {
        return standardControl;
    }

    public void setStandardControl(String standardControl) {
        this.standardControl = standardControl;
    }

    public Long getCreatePersonId() {
        return createPersonId;
    }

    public void setCreatePersonId(Long createPersonId) {
        this.createPersonId = createPersonId;
    }

    public String gettPath() {
        return tPath;
    }

    public void settPath(String tPath) {
        this.tPath = tPath;
    }

    public int gettLevel() {
        return tLevel;
    }

    public void settLevel(int tLevel) {
        this.tLevel = tLevel;
    }

    public List<RiskInventoryControlPointBean> getListRiskInventoryControlPointBean() {
        return listRiskInventoryControlPointBean;
    }

    public void setListRiskInventoryControlPointBean(
            List<RiskInventoryControlPointBean> listRiskInventoryControlPointBean) {
        this.listRiskInventoryControlPointBean = listRiskInventoryControlPointBean;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }
}
