package com.jecn.epros.domain.report;

import io.swagger.annotations.ApiModelProperty;

public class PartOpenParam {

    @ApiModelProperty(value = "流程架构或者责任部门id")
    private String id;
    @ApiModelProperty(value = "0责任部门  1流程架构")
    private String type;
    @ApiModelProperty(value = "唯一标识，如果打开活动的饼状图那么是该区域是哪个区域，如果打开的是信息化，那么是IT系统的主键id")
    private String part;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

}
