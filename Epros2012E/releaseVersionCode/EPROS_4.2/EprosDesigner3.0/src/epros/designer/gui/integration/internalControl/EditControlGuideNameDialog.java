package epros.designer.gui.integration.internalControl;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 内控指引知识库目录/条款重命名
 * 
 * @author Administrator
 * 
 */
public class EditControlGuideNameDialog extends JecnEditNameDialog {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger
			.getLogger(EditControlGuideNameDialog.class);

	/** 树节点 */
	private JecnTreeNode selectNode = null;
	/** 树 */
	private JTree jTree = null;

	/**
	 * 内控指引知识库目录/条款重命名构造方法
	 * 
	 * @param pNode
	 *            树节点
	 * @param jTree
	 *            树
	 */
	public EditControlGuideNameDialog(JecnTreeNode pNode, JTree jTree) {
		this.selectNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		try {
			this.setName(selectNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("EditControlGuideNameDialog is error", e);
		}
		this.setName(selectNode.getJecnTreeBean().getName());
	}

	/**
	 * 标题
	 */
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	/**
	 * 目录名称
	 */
	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	/**
	 * 修改数据
	 */
	@Override
	public void saveData() {
		try {
			ConnectionPool.getControlGuideAction().reControlGuideDirName(
					selectNode.getJecnTreeBean().getId(), this.getName(),
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			// 重命名
			JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		} catch (Exception e) {
			log.error("EditControlGuideNameDialog saveData is error", e);
		}
		this.dispose();
	}

	/**
	 * 验证是否重名
	 */
	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}
}
