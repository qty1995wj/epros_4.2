package epros.designer.gui.standard;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class StandardMoveChooseDialog extends JecnMoveChooseDialog {
	private static Logger log = Logger.getLogger(StandardMoveChooseDialog.class);

	public StandardMoveChooseDialog(List<JecnTreeNode> listMoveNodes, JecnTree jTree) {
		super(listMoveNodes, jTree);
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		try {
			ConnectionPool.getStandardAction().moveNodes(ids, pid, JecnConstants.getUserId(), this.getMoveNodeType());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("StandardMoveChooseDialog savaData is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return false;
		}

	}

	@Override
	public JecnTree getJecnTree() {
		// if (JecnConstants.isMysql) {
		// return new RoutineStandardMoveTree(this.getListIds());
		// } else {
		return new HighEfficiencyStandardMoveTree(this.getListIds());
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 名称
		return title;
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = ConnectionPool.getStandardAction()
					.getChildStandards(pid, JecnConstants.projectId);
			for (JecnTreeNode node : listMoveNodes) {
				if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.standardClauseRequire) {
					continue;
				}
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}

				}
			}
		} catch (Exception e) {
			log.error("StandardMoveChooseDialog validateName is error", e);
			return false;

		}
		return false;
	}
}
