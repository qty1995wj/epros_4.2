package com.jecn.epros.server.service.file;

import com.jecn.epros.server.bean.file.FileDownLoadCountBean;
import com.jecn.epros.server.common.IBaseService;

/**
 * 文件下载次数限制service(interface)
 * 
 * @author CHB
 * */
public interface IDownLoadCountService extends
		IBaseService<FileDownLoadCountBean, Long> {

	/**
	 * 文件下载统计
	 * 
	 * @author CHB
	 * @param peopleId
	 *            :人员ID dowLoadMaxCount:最大下载次数
	 * 
	 * **/
	public void downLoadCount(Long peopleId, int dowLoadMaxCount);

	/**
	 * 根据人员ID下载次数
	 * 
	 * @author CHB
	 * @param peopleId
	 *            :人员ID dowLoadMaxCount:最大下载次数
	 * 
	 * **/
	public boolean downLoadNum(Long peopleId, int dowLoadMaxCount);

	/**
	 * 
	 * 清空下载统计表数据
	 * 
	 * @author CHB
	 * 
	 * */
	public void clearDownLoadCountData() throws Exception;

}
