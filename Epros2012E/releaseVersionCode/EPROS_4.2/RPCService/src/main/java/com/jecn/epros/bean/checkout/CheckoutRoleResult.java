package com.jecn.epros.bean.checkout;

import java.io.Serializable;
import java.util.List;

public class CheckoutRoleResult implements Serializable{
    private String name;
    private List<CheckoutRoleItem> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CheckoutRoleItem> getData() {
        return data;
    }

    public void setData(List<CheckoutRoleItem> data) {
        this.data = data;
    }
}
