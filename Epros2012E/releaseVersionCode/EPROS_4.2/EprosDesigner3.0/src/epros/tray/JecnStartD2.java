package epros.tray;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;
import jecntool.util.Tool;

import org.apache.log4j.Logger;

import epros.designer.gui.JecnD2Frame;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 
 * 启动EPROS-D2
 * 
 * @author zhouxy
 * 
 */
class JecnStartD2 {
	private Logger log = Logger.getLogger(JecnStartD2.class);

	/**
	 * 1、检测mysql是否已经启动，标记为启动，
	 * 
	 * 2、mysql标记启动，执行下一步骤； 标记未启动，检测当前端口是否占用，占用+1端口再检测直到检测到空闲端口并使用此端口，启动mysql服务，
	 * 启动失败提示用户启动失败联系软件开发公司并退出；
	 * 
	 * 3、检测tomcat是否已经启动，如果启动，标记为启动再停止；
	 * 
	 * 4、检测RMI端口是否占用，空闲执行下一步骤；占用+1检测直到检测到空闲端口并使用此端口，再执行下一步骤；
	 * 
	 * 5、tomcat标记启动，启动tomcat服务，启动成功执行下一步骤，启动失败提示用户启动失败联系软件开发公司并退出；
	 * 标记未启动，检测端口是否占用
	 * ，占用+1再检测直到检测到空闲端口并使用此端口，启动tomcat服务，启动成功执行下一步骤，启动失败提示用户启动失败联系软件开发公司并退出；
	 * 
	 * 5、启动EPORS-D2软件成功，进入主页面；失败提示用户启动失败联系软件开发公司并退出。
	 * 
	 */

	/** 用户中断 */
	private boolean interrupt = false;

	/** tomcat，rmi，mysql端口处理对象 */
	private JecnPort jecnPort = null;

	/** *D2等待提示框* */
	private JecnD2Frame rd = null;

	public JecnStartD2() {
		jecnPort = new JecnPort();
		rd = new JecnD2Frame();
	}

	/**
	 * 
	 * 启动tomcat和mysql
	 * 
	 * @return ResultEnum null：启动成功；非null表示有错误
	 * @throws Exception
	 */
	ResultEnum startEprosD2() {
		ResultEnum ret = null;

		rd.setVisible(true);
		rd.updateTipInfo(JecnProperties.getValue("D2Step01"), 5);
		// jecneprosServerD2
		// jecneprosDBD2
		long begD2 = System.currentTimeMillis();
		long beg = System.currentTimeMillis();

		// 1、检测mysql是否已经启动，标记为启动
		log.info("执行扫描进程开始:命令：" + JecnTrayConstants.CMD_TASKLIST);
		ResultBean retBean = JecnCmd.cmdExec(JecnTrayConstants.CMD_TASKLIST);
		String resultStr = retBean.getResultStr();
		long end = System.currentTimeMillis();
		log.info("执行扫描进程结束:返回值resultStr=" + resultStr + "；状态resultState="
				+ retBean.getResultState() + ";耗时=" + (end - beg));

		// tomcat服务是否已经启动 已启动：true；false：未启动
		boolean isRunTomcat = false;
		// mysql服务是否已经启动 已启动：true；false：未启动
		boolean isRunMySQL = false;

		if (retBean.getResultState() == 0) {// 执行成功
			if (Tool.isNotBlank(resultStr)) {
				isRunMySQL = resultStr.indexOf(JecnTrayConstants.MYSQL_NAME) >= 0;
				isRunTomcat = resultStr.indexOf(JecnTrayConstants.TOMCAT_NAME) >= 0;
				log.info("检查Windows进程中是否包含:" + JecnTrayConstants.MYSQL_NAME
						+ "、" + JecnTrayConstants.TOMCAT_NAME
						+ "进程。判断结果isRunMySQL=" + isRunMySQL + "，isRunTomcat="
						+ isRunTomcat);

				// 能操作进程执行分支
				ret = process(isRunTomcat, isRunMySQL);
			} else {
				// 能操作进程但是没有返回值情况，也算成不能操作进程方式处理
				ret = process2();
			}
		} else {// 无法操作进程命令，执行分支
			ret = process2();
		}
		
		if(ret!=null){
			return ret;
		}
		
		rd.updateTipInfo(JecnProperties.getValue("D2Step06"), 100);
		rd.setVisible(false);
		
		end = System.currentTimeMillis();
		log.info("启动总耗时：" + (end - begD2));
		return ret;
	}

	/**
	 * 
	 * 能操作进程执行分支
	 * 
	 * @param isRunTomcat
	 *            boolean
	 * @param isRunMySQL
	 *            boolean
	 * @return ResultEnum
	 */
	private ResultEnum process(boolean isRunTomcat, boolean isRunMySQL) {
		log.info("能操作进程命令分支执行开始...");

		long beg = 0l;
		long end = 0l;
		/** *******mysql start********* */
		if (isRunMySQL) {// 已启动(有mysql进程)
			if (!JecnServerUtil.mysql5IsRun()) {// 有mysql进程但不能执行sql查询时，需要停止mysql服务重新启动
				log.info("3=====有mysql进程但不能执行sql查询，重新启动mysql服务===");
				if (JecnServerUtil.stopMysql()) {// 停止成功
					ResultEnum ret = checkMysqlStopFinished();
					if (ret == null) {
						log.info("3.2=====开始杀死 mysql进程");
						boolean killRet = killServer();
						log.info("3.3=====开始杀死 mysql进程killRet:" + killRet);
//						if (!killRet) {
//							return ResultEnum.stopMysqlErr;
//						}
					} //else {
//						return ret;
//					}
					// 启动mysql
					log.info("4====启动mysql=========");
					beg=System.currentTimeMillis();
					
					ret = startMysql();
					
					end=System.currentTimeMillis();
					log.info("4.1====启动mysql======返回值===" + ret);
					log.info("4.1====启动mysql======耗时:===" + (end-beg));
					if (ret != null) {
						return ret;
					}
				} else {// 停止不了已启动mysql，提示用户需要手动停止mysql进程，再重启EPROS-D2
					return ResultEnum.stopMysqlErr;
				}
			}
		} else {// 未启动
			// MYSQL端口检测+RMI端口检测：目的database.properties只读写一次
			log.info("4.3=====未启动mysql=配置mysql端口======");
			beg =System.currentTimeMillis();
			if (!jecnPort.mysqlPort()) {
				log.error("4.4=========配置mysql端口错误=========");
				return ResultEnum.freePortErr;// 配置空闲端口时报错，提示用户再重启EPROS-D2
			}
			end=System.currentTimeMillis();
			log.info("4.4=========配置mysql端口耗时："+(end-beg));
			beg=System.currentTimeMillis();
			ResultEnum ret = startMysql();
			end=System.currentTimeMillis();
			log.info("4.5=====启动mysql===结果：===ret=" + ret);
			log.info("4.6=====启动mysql===耗时："+(end-beg));
			if (ret != null) {
				return ret;
			}
		}
		
		/** *******mysql end********* */
		/** *******tomcat start********* */
		log.info("5.=====tomcat服务是否已经启动起来：" + isRunTomcat);
		if (isRunTomcat) {// 已启动(有tomcat进程)
			if (!JecnServerUtil.tomcatIsRun()) {// 判断tomcat是否正常使用，能正常使用就不用停止tomcat
				log.info("5.1====tomcat服务启动起来,运行不正常=====");
				if (JecnServerUtil.stopTomcat()) {// 停止成功
					log.info("5.2=====停止tomcat服务成功=========");
					ResultEnum ret = checkTomcatStopFinished();
					if (ret == null) {
						boolean killRet = killServer();
						log.info("5.3==特殊情况下killServer,结果：" + killRet);
//						if (!killRet) {
//							return ResultEnum.stopTomcatErr;
//						}
					} //else {
//						return ret;
//					}
					// 启动tomcat
					log.info("5.4====启动tomcat=============");
					beg =System.currentTimeMillis();
					ret = startTomcat();
					end =System.currentTimeMillis();
					log.info("5.4====启动tomcat结果：============="+ret);
					log.info("5.5====启动tomcat启动tomcat耗时 ："+(end-beg));
//					if (ret != null) {
//						return ret;
//					}
				} else {// 停止不了已启动tomcat
					log.error("5.6.=====error：停止不了已启动tomcat");
					return ResultEnum.stopTomcatErr;// 停止不了已启动tomcat，提示用户需要手动停止tomcat进程，再重启EPROS-D2
				}
			}
		} else {// 未启动
			log.info("5.7=====tomcat未启动========");
			log.info("5.8=====配置RMI端口========");
			// RMI端口检测
			if (!jecnPort.rmiPort()) {
				log.error("5.9=========配置rmi端口错误=========");
				return ResultEnum.freePortErr;// 配置空闲端口时报错，提示用户再重启EPROS-D2
			}
			log.info("5.10=====配置TOMCAT空闲端口====");
			// TOMCAT端口检测
			beg=System.currentTimeMillis();
			if (!jecnPort.tomcatPort()) {
				log.error("5.11==error==配置TOMCAT空闲端口报错====");
				return ResultEnum.freePortErr;// 配置空闲端口时报错，提示用户再重启EPROS-D2
			}
			end=System.currentTimeMillis();
			log.info("5.12=====配置tomcat端口耗时："+(end-beg));
			// 启动tomcat
			log.info("5.13=====启动tomcat====");
			beg=System.currentTimeMillis();
			ResultEnum ret = startTomcat();
			log.info("5.14=====启动tomca结果：==" + ret);
			end=System.currentTimeMillis();
			log.info("5.15=====启动tomcat==耗时："+(end-beg));
			if (ret != null) {
				return ret;
			}
		}
		/** *******tomcat end********* */

		log.info("能操作进程命令分支执行结束...");
		return null;
	}

	/**
	 * 
	 * 无法操作进程命令，执行分支
	 * 
	 * @return ResultEnum
	 */
	private ResultEnum process2() {
		log.info("无法操作进程命令分支执行开始...");

		long beg = 0l;
		long end = 0l;
		/** *******mysql start********* */
		// 判断mysql不能正常访问，启动mysql，启动失败，停止mysql
		if (!JecnServerUtil.mysql5IsRun()) {// mysql没有启动
			// 启动不正常情况目前处理不了

			// 未启动
			// MYSQL端口检测+RMI端口检测：目的database.properties只读写一次
			log.info("4.3=====未启动mysql=配置mysql端口======");
			beg = System.currentTimeMillis();
			if (!jecnPort.mysqlPort()) {
				log.error("4.4=========配置mysql端口错误=========");
				return ResultEnum.freePortErr;// 配置空闲端口时报错，提示用户再重启EPROS-D2
			}
			end = System.currentTimeMillis();
			log.info("4.4=========配置mysql端口耗时：" + (end - beg));
			beg = System.currentTimeMillis();
			ResultEnum ret = startMysql();
			end = System.currentTimeMillis();
			log.info("4.5=====启动mysql===结果：===ret=" + ret);
			log.info("4.6=====启动mysql===耗时：" + (end - beg));
			if (ret != null) {
				// 启动失败，请查看Windows任务管理器中，是否存在[jecneprosServerD2]、[jecneprosDBD2]两个进程，
				//如果存在停止此两进程后再启动启动EPROS-D2
				JecnOptionPane.showMessageDialog(null, JecnProperties
						.getValue("D2Info1"));
				return ret;
			}
		}
		/** *******mysql end********* */

		/** *******tomcat start********* */
		if (!JecnServerUtil.tomcatIsRun()) {// tomcat没有启动
			// 启动不正常情况目前处理不了

			// 未启动
			log.info("5.7=====tomcat未启动========");
			log.info("5.8=====配置RMI端口========");
			// RMI端口检测
			if (!jecnPort.rmiPort()) {
				log.error("5.9=========配置rmi端口错误=========");
				return ResultEnum.freePortErr;// 配置空闲端口时报错，提示用户再重启EPROS-D2
			}
			log.info("5.10=====配置TOMCAT空闲端口====");
			// TOMCAT端口检测
			beg = System.currentTimeMillis();
			if (!jecnPort.tomcatPort()) {
				log.error("5.11==error==配置TOMCAT空闲端口报错====");
				return ResultEnum.freePortErr;// 配置空闲端口时报错，提示用户再重启EPROS-D2
			}
			end = System.currentTimeMillis();
			log.info("5.12=====配置tomcat端口耗时：" + (end - beg));
			// 启动tomcat
			log.info("5.13=====启动tomcat====");
			beg = System.currentTimeMillis();
			ResultEnum ret = startTomcat();
			log.info("5.14=====启动tomca结果：==" + ret);
			end = System.currentTimeMillis();
			log.info("5.15=====启动tomcat==耗时：" + (end - beg));
			if (ret != null) {
				//启动失败，请查看Windows任务管理器中，是否存在[jecneprosServerD2]进程，
				//如果存在停止此进程后再启动启动EPROS-D2
				JecnOptionPane.showMessageDialog(null, JecnProperties
						.getValue("D2Info2"));
				return ret;
			}
		}
		/** *******tomcat end********* */

		log.info("无法操作进程命令分支执行结束...");
		return null;
	}

	/**
	 * 
	 * 启动tomcat
	 * 
	 * @return ResultEnum 启动成功返回null，错误返回ResultEnum对象
	 * 
	 */
	private ResultEnum startTomcat() {
		rd.updateTipInfo(JecnProperties.getValue("D2Step04"), 55);
		boolean ret = JecnServerUtil.startTomcat();
		if (!ret) {// 启动失败
			log.error("=======启动tomcat失败===================");
			return ResultEnum.startTomcatErr;// 启动tomcat失败，提示用户稍后重新启动
		}
		rd.updateTipInfo(JecnProperties.getValue("D2Step05"), rd
				.getProcessBarValue() + 5);
		int time = 2000;
		while (true) {
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				log.error("", e);
			}
			time = 500;
			if (rd.getProcessBarValue() <= 95) {
				rd.updateTipInfo(JecnProperties.getValue("D2Step05"), rd
						.getProcessBarValue() + 5);
			}
			if (JecnServerUtil.tomcatIsRun()) {// 启动成功
				break;
			}
			if (interrupt) {// 用户中断
				return ResultEnum.userInterrupt;
			}
		}

		return null;
	}

	/**
	 * 
	 * 启动mysql
	 * 
	 * @return ResultEnum 启动成功返回null，错误返回ResultEnum对象
	 * 
	 */
	private ResultEnum startMysql() {
		rd.updateTipInfo(JecnProperties.getValue("D2Step02"), 15);
		long beg = System.currentTimeMillis();
		boolean ret = JecnServerUtil.startMysql();
		if (!ret) {// 启动失败
			log.error("=======启动tomcat失败===================");
			return ResultEnum.startMysqlErr;// 启动mysql失败，提示用户稍后重新启动
		}
		rd.updateTipInfo(JecnProperties.getValue("D2Step03"), rd
				.getProcessBarValue() + 5);
		int time = 1000;
		while (true) {
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				log.error("", e);
			}
			time = 500;
			if (rd.getProcessBarValue() <= 50) {
				rd.updateTipInfo(JecnProperties.getValue("D2Step03"), rd
						.getProcessBarValue() + 5);
			}
			if (JecnServerUtil.mysql5IsRun()) {// 启动成功
				break;
			}
			if (interrupt) {// 用户中断
				return ResultEnum.userInterrupt;
			}
		}

		return null;
	}

	/**
	 * 
	 * tomcat已运行情况下，停止tomcat才调用此方法
	 * 
	 * 通过判断tomcat端口是否空闲来判断停止tomcat是否成
	 * 
	 * @return ResultEnum null或错误信息
	 * 
	 */
	private ResultEnum checkTomcatStopFinished() {
		while (true) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				log.error("", e);
			}
			if (Tool.checkPort(jecnPort.getTmcPort())) {// 端口空闲
				break;
			}
			if (interrupt) {// 用户中断
				return ResultEnum.userInterrupt;
			}
		}
		return null;
	}

	/**
	 * 
	 * mysql已运行情况下，停止mysql才调用此方法
	 * 
	 * 通过判断mysql端口是否空闲来判断停止mysql是否成
	 * 
	 * @return ResultEnum null或错误信息
	 * 
	 */
	private ResultEnum checkMysqlStopFinished() {
		while (true) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				log.error("", e);
			}
			if (Tool.checkPort(jecnPort.getMysqlPort())) {// 端口空闲
				break;
			}
			if (interrupt) {// 用户中断
				return ResultEnum.userInterrupt;
			}
		}
		return null;
	}

	enum ResultEnum {
		stopMysqlErr, // 停止不了已启动mysql，提示用户需要手动停止mysql进程，再重启EPROS-D2
		startMysqlErr, // 启动mysql失败，提示用户稍后重新启动EPROS-D2
		userInterrupt, // 用户手动停止
		startTomcatErr, // 启动tomcat失败，提示用户稍后重新启动EPROS-D2
		stopTomcatErr, // 停止不了已启动tomcat，提示用户需要手动停止tomcat进程，再重启EPROS-D2
		freePortErr, // 配置空闲端口时报错，提示用户再重启EPROS-D2

	}

	/**
	 * 
	 * 强行杀掉tocmat进程
	 * 
	 * @return boolean 杀成功返回true，杀失败或别的异常返回false
	 */
	private boolean killServer() {
		log.info("强行杀掉tomcat的命令：===" + JecnTrayConstants.CMD_TASKKILL_SERVER);
		return killServer(JecnTrayConstants.CMD_TASKLIST,
				JecnTrayConstants.CMD_TASKKILL_SERVER, 0);
	}

	/*
	 * public static void main(String[] arg) { JecnStartD2 d3 = new
	 * JecnStartD2(); d3.killServer(); }
	 */

	/**
	 * 
	 * 强行杀掉mysql进程
	 * 
	 * @return boolean 杀成功返回true，杀失败或别的异常返回false
	 */
	/*
	 * private boolean killDB() { log.info("强行杀掉mysql的命令：===" +
	 * JecnTrayConstants.CMD_TASKKILL_DB); return
	 * killServer(JecnTrayConstants.CMD_TASKLIST,
	 * JecnTrayConstants.CMD_TASKKILL_DB,1); }
	 */

	/**
	 * 
	 * 此方法一般运用在正常操作下还是关闭不了给定进程时候使用
	 * 
	 * 通过进程判断给定进程是否存在，存在强行杀掉
	 * 
	 * @param tasklist
	 *            String "tasklist"
	 * @param taskkill
	 *            String "taskkill /im jecneprosDBD2.exe /f"
	 * @param int
	 *            0:tomcat 1:mysql
	 * @return boolean 杀成功返回true，杀失败或别的异常返回false
	 */
	private boolean killServer(String tasklist, String taskkill, int type) {
		ResultBean retBean = JecnCmd.cmdExec(tasklist);
		int state = retBean.getResultState();
		String content = retBean.getResultStr();

		if (state == 0) {// 成功(正常走分支)
			boolean flag = false;
			if (Tool.isNotBlank(content)) {
				if (type == 0) {// tomcat
					flag = content.indexOf(JecnTrayConstants.TOMCAT_NAME) >= 0;
				} else if (type == 1) {// mysql
					flag = content.indexOf(JecnTrayConstants.MYSQL_NAME) >= 0;
				}
			}
			if (flag) {
				retBean = JecnCmd.cmdExec(taskkill);
				if (retBean.getResultState() != 0) {// 强行停止失败
					return false;
				}
			}
		} else {// 一般情况不会出现
			JecnCmd.cmdExec(taskkill);
		}
		return true;
	}
}