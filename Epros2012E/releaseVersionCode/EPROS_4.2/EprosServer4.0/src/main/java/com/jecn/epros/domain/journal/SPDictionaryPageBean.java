package com.jecn.epros.domain.journal;


import com.jecn.epros.domain.system.Dictionary;

import java.util.List;
import java.util.Map;

/**
 * java字典项分页查询
 *
 * @Author: Angus
 * @CreateDate: 2018/7/29 15:10
 * @Description: descroption
 **/
public class SPDictionaryPageBean {

    private int total;
    private List<Map<Dictionary,Dictionary>> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Map<Dictionary,Dictionary>> getData() {
        return data;
    }

    public void setData(List<Map<Dictionary,Dictionary>> data) {
        this.data = data;
    }
}
