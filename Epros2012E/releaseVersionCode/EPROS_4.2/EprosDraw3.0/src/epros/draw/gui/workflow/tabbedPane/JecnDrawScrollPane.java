package epros.draw.gui.workflow.tabbedPane;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.workflow.grid.JecnShapeScalePanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 滚动面板：画图面板的容器
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDrawScrollPane extends JScrollPane implements
		AdjustmentListener {

	/** 画图面板 */
	private JecnDrawDesktopPane desktopPane = null;

	/** 行标题面板 */
	private JecnShapeScalePanel shapeScaleRowPanel;
	/** 列标题面板 */
	private JecnShapeScalePanel shapeScaleColumnPanel;

	public JecnDrawScrollPane(JecnDrawDesktopPane desktopPane) {
		if (desktopPane == null) {
			JecnLogConstants.LOG_DRAW_SCROLL_PANE
					.error("JecnDrawScrollPane类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.desktopPane = desktopPane;

		initComponents();
	}

	private void initComponents() {
		// 横竖滚动条需要时候才显示
		this
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 边框
		this.setBorder(null);
		this.getViewport().setBorder(null);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.getViewport()
				.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 四个角
		this.setCorner(ScrollPaneConstants.UPPER_LEFT_CORNER,
				new JecnAllMapButton().getJToolBar());
		this.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, new JecnPanel());
		this.setCorner(ScrollPaneConstants.LOWER_LEFT_CORNER, new JecnPanel());
		this.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, new JecnPanel());

		// 添加画图面板
		this.setViewportView(desktopPane);
		
		//鼠标滚动大小
		this.getHorizontalScrollBar().setUnitIncrement(60);
		this.getVerticalScrollBar().setUnitIncrement(60);

		// 滚动条事件
		this.getHorizontalScrollBar().addAdjustmentListener(this);
		this.getVerticalScrollBar().addAdjustmentListener(this);

		// 初始化刻度面板
		initScalePanel();
	}

	/**
	 * 
	 * 创建画图面板的刻度
	 * 
	 * 
	 */
	private void initScalePanel() {

		// 行标题面板
		shapeScaleRowPanel = JecnShapeScalePanel.createRowShapeScalePanel(this);
		// 列标题面板
		shapeScaleColumnPanel = JecnShapeScalePanel
				.createColumnShapeScalePanel(this);

		// 添加到滚动面板上
		this.setColumnHeaderView(shapeScaleColumnPanel);
		this.setRowHeaderView(shapeScaleRowPanel);

		this.getColumnHeader().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		this.getRowHeader().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
	}

	/**
	 * 
	 * 显示/隐藏滚动面板的横竖标题栏
	 * 
	 */
	public void showScrollPanelHeader() {
		if (JecnSystemStaticData.isWorkflowTitleShow()) {// 选中
			if (this.getColumnHeader() != null
					&& !this.getColumnHeader().isVisible()) {
				this.getColumnHeader().setVisible(true);
			}
			if (this.getRowHeader() != null && !this.getRowHeader().isVisible()) {
				this.getRowHeader().setVisible(true);
			}

			if (!shapeScaleColumnPanel.isVisible()) {
				shapeScaleColumnPanel.setVisible(true);
			}

			if (!shapeScaleRowPanel.isVisible()) {
				shapeScaleRowPanel.setVisible(true);
			}
		} else {// 未选中
			if (this.getColumnHeader() != null
					&& this.getColumnHeader().isVisible()) {
				this.getColumnHeader().setVisible(false);
			}
			if (this.getRowHeader() != null && this.getRowHeader().isVisible()) {
				this.getRowHeader().setVisible(false);
			}

			if (shapeScaleColumnPanel.isVisible()) {
				shapeScaleColumnPanel.setVisible(false);
			}

			if (shapeScaleRowPanel.isVisible()) {
				shapeScaleRowPanel.setVisible(false);
			}
		}
	}

	/**
	 * 
	 * 滚动条事件
	 * 
	 */
	public void adjustmentValueChanged(AdjustmentEvent e) {
		if (e.getSource() instanceof JScrollBar) {
			if (e.getSource() == this.getHorizontalScrollBar()) {// 横滚动条处理
				hBarAdjustmentValueChanged(e);
			} else if (e.getSource() == this.getVerticalScrollBar()) {// 竖滚动条处理
				vBarAdjustmentValueChanged(e);
			}
		}
	}

	/**
	 * 
	 * 横滚动条处理
	 * 
	 * @param e
	 *            AdjustmentEvent
	 */
	private void hBarAdjustmentValueChanged(AdjustmentEvent e) {
		if (JecnSystemStaticData.isShowRoleMove()
				&& desktopPane.getRoleMobile() != null) { // 判断角色移动是否选中
			// 面板横竖标识 true 为横向 false 为纵向
			boolean workflowFlag = desktopPane.getFlowMapData().isHFlag();
			if (workflowFlag) {
				desktopPane.getRoleMobile().getRoleMove().setLocation(
						e.getValue(), 0);
			}
		}
	}

	/**
	 * 
	 * 竖滚动条处理
	 * 
	 * @param e
	 *            AdjustmentEvent
	 */
	private void vBarAdjustmentValueChanged(AdjustmentEvent e) {
		if (JecnSystemStaticData.isShowRoleMove()
				&& desktopPane.getRoleMobile() != null) {// 判断角色移动是否选中
			// 面板横竖标识 true 为横向 false 为纵向
			boolean workflowFlag = desktopPane.getFlowMapData().isHFlag();
			if (!workflowFlag) {
				desktopPane.getRoleMobile().getRoleMove().setLocation(0,
						e.getValue());
			}
		}
	}

	/**
	 * 
	 * 设置当前横滚动条位置
	 * 
	 * @param value
	 *            int 当前滚动条值
	 */
	public void setHorizontalScrollBarValue(int value) {
		this.getHorizontalScrollBar().setValue(value);
	}

	/**
	 * 
	 * 设置当前竖滚动条位置
	 * 
	 * @param value
	 *            int 当前滚动条值
	 */
	public void setVerticalScrollBarValue(int value) {
		this.getVerticalScrollBar().setValue(value);
	}

	public JecnDrawDesktopPane getDesktopPane() {
		return desktopPane;
	}

	public JecnShapeScalePanel getShapeScaleRowPanel() {
		return shapeScaleRowPanel;
	}

	public JecnShapeScalePanel getShapeScaleColumnPanel() {
		return shapeScaleColumnPanel;
	}

}
