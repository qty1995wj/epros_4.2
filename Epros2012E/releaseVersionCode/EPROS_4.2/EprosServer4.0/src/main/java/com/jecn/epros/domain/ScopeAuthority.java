package com.jecn.epros.domain;

import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.HttpUtil;

import java.util.List;

public class ScopeAuthority {

    private String id;
    private String name;
    /**
     * FILE FILE RULE RISK STANDARD
     **/
    private ResourceType type;

    /**
     * 0 临时  1 正式
     **/
    private int isPublic = 1;
    /**
     * 关联与某个文件的id 例如，流程关联的文件rid就是流程id,而上面的id和name是文件的id和name
     **/
    private String rid;

    private String url;

    private Long standardFileId;

    private List<ScopeAuthority> scopeAuthority;


    public List<ScopeAuthority> getScopeAuthority() {
        return scopeAuthority;
    }

    public void setScopeAuthority(List<ScopeAuthority> scopeAuthority) {
        this.scopeAuthority = scopeAuthority;
    }

    public String getUrl() {
        if (id != null && "FILE".equals(type.toString())) {
            url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "");
        }
        if ((standardFileId != null && standardFileId != -1) && "STANDARD".equals(type.toString())) {
            url = HttpUtil.getDownLoadFileHttp(Long.valueOf(standardFileId), true, "");
        }
        return url;
    }

    public Long getStandardFileId() {
        return standardFileId;
    }

    public void setStandardFileId(Long standardFileId) {
        this.standardFileId = standardFileId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LinkResource getLink() {
        return new LinkResource(id, name, type, isPublic, TreeNodeUtils.NodeType.process, getUrl());
    }


    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getTypeName() {
        return type.getName();
    }

    public String getTypeEnName() {
        return type.getEnName();
    }

    public String getFrom() {
        return TreeNodeUtils.NodeType.main.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

}
