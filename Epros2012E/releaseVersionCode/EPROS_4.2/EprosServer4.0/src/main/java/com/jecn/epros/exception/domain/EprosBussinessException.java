package com.jecn.epros.exception.domain;

public class EprosBussinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    public static final String RESOURCE_NOT_FOUND = "请求的资源不存在";

    public EprosBussinessException(String message) {
        super(message);
    }

    public EprosBussinessException(String message, Throwable e) {
        super(message, e);
    }

}
