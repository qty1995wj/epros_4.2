/**
 * SubmitWorkItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class SubmitWorkItem  implements java.io.Serializable {
    private java.lang.String workItemId;

    private java.lang.String commentText;

    public SubmitWorkItem() {
    }

    public SubmitWorkItem(
           java.lang.String workItemId,
           java.lang.String commentText) {
           this.workItemId = workItemId;
           this.commentText = commentText;
    }


    /**
     * Gets the workItemId value for this SubmitWorkItem.
     * 
     * @return workItemId
     */
    public java.lang.String getWorkItemId() {
        return workItemId;
    }


    /**
     * Sets the workItemId value for this SubmitWorkItem.
     * 
     * @param workItemId
     */
    public void setWorkItemId(java.lang.String workItemId) {
        this.workItemId = workItemId;
    }


    /**
     * Gets the commentText value for this SubmitWorkItem.
     * 
     * @return commentText
     */
    public java.lang.String getCommentText() {
        return commentText;
    }


    /**
     * Sets the commentText value for this SubmitWorkItem.
     * 
     * @param commentText
     */
    public void setCommentText(java.lang.String commentText) {
        this.commentText = commentText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitWorkItem)) return false;
        SubmitWorkItem other = (SubmitWorkItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.workItemId==null && other.getWorkItemId()==null) || 
             (this.workItemId!=null &&
              this.workItemId.equals(other.getWorkItemId()))) &&
            ((this.commentText==null && other.getCommentText()==null) || 
             (this.commentText!=null &&
              this.commentText.equals(other.getCommentText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getWorkItemId() != null) {
            _hashCode += getWorkItemId().hashCode();
        }
        if (getCommentText() != null) {
            _hashCode += getCommentText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmitWorkItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">SubmitWorkItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workItemId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "workItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "commentText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
