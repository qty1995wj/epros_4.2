<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js"></script>
		
		<s:if test="#session.country=='US'">
		</s:if><s:else>
		<script type="text/javascript"
			src="${basePath}js/epros_zh_CN.js" charset="UTF-8"></script>
		<script type="text/javascript"
			src="${basePath}ext/ext-lang-zh_CN.js" charset="UTF-8"></script>
		</s:else>
		<script type="text/javascript" src="${basePath}js/common.js"></script>
        <script type="text/javascript" src="${basePath}js/firstPage.js" charset="UTF-8"></script>
		<script type="text/javascript"
			src="${basePath}js/reports/processAnalysis.js"></script>
			<!--  流程分析详情 -->
		<title><s:text name="processAnalysisForDetails" /></title>
		<script type="text/javascript">
			var basePath = "${basePath}";
			window.onerror = function() { return true; };
			window.onresize=function(){
			     var width = document.documentElement.clientWidth;
			     var height = document.documentElement.clientHeight-20;
                 processAnalysisGridPanel.setSize(width,height);
			}
		</script>
	</head>
	<body onload="processAnalysisGrid()">
	    	<div style="height: 25px; width: 100%; overflow: hidden;"
				align="right" class="gradient minWidth">
				<%@include file="/navigation.jsp"%>
			</div>
			<div id="processAnalysisGrid_id">
			</div>
			<!-- 流程ID -->
			<input type="hidden" id="processId_id" value="${param.processId}" />
			<!-- 时间类型 -->
			<input type="hidden" id="timeType" value="${param.timeType}" />
			<!-- 岗位ID -->
			<input type="hidden" id="posId" value="${param.posId}" />
			<!-- 流程类别 -->
			<input type="hidden" id="processType" value="${param.processType}" />
	</body>
</html>