package com.jecn.epros.server.service.dataImport.sync.excelOrDb;

/**
 * 人员同步行间校验处理接口
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jun 5, 2013 时间：5:16:36 PM
 */
public interface ICheckUserDataColumns {
	/**
	 * 人员原始数据行间校验
	 * 
	 * @return boolean false 存在异常数据
	 */
	public boolean checkUserPosColu(boolean isSyncUserOnly);

	/**
	 * 部门外数据行间校验
	 * 
	 * @return
	 */
	public boolean checkDeptColu();
}
