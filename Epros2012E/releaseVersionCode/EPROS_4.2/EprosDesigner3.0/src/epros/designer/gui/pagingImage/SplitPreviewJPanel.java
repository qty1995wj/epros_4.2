package epros.designer.gui.pagingImage;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;

import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 打印内容显示面板
 */
public class SplitPreviewJPanel extends JPanel {
	private JecnDrawDesktopPane workflow;
	private int movX;
	private double roleWidth;
	private double scale;

	public SplitPreviewJPanel(int pageWidth, int pageHight, int movX, double roleWidth, double scale,
			JecnDrawDesktopPane workflow) {
		this.workflow = workflow;
		this.roleWidth = roleWidth;
		this.movX = movX;
		this.scale = scale;
		if (movX != 0) {
			this.setPreferredSize(new Dimension((int) (pageWidth * scale), (int) (pageHight * scale)));
		} else {
			this.setPreferredSize(new Dimension((int) ((pageWidth + roleWidth) * scale), (int) (pageHight * scale)));
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// 获取G2D 画笔
		Graphics2D g2d = (Graphics2D) g;
		// 获取原版面板设置
		RenderingHints rhO = g2d.getRenderingHints();
		// 新建面板设置 抗锯齿
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// 设置面板
		g2d.setRenderingHints(rh);
		if (movX != 0) {
			g2d.transform(AffineTransform.getTranslateInstance(-(movX + roleWidth) * scale, 0));
		}
		g2d.transform(AffineTransform.getScaleInstance(scale, scale));
		ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
		// 获取面板下所有组件
		updateDoubleBuffered(workflow, dbcomponents);
		workflow.paint(g2d);
		// 设置缓冲区
		resetDoubleBuffered(dbcomponents);
		// 还原面板设置
		g2d.setRenderingHints(rhO);
		// 销毁画笔
		g2d.dispose();
	}

	/**
	 * 获取组件集合
	 * 
	 * @author fuzhh 2013-11-7
	 * 
	 */
	private void updateDoubleBuffered(JComponent component, ArrayList<JComponent> dbcomponents) {
		if (component.isDoubleBuffered()) {
			// 添加组件到集合
			dbcomponents.add(component);
			// 设置不使用缓冲区
			component.setDoubleBuffered(false);
		}
		// 获取面板下 所有组件
		for (int i = 0; i < component.getComponentCount(); i++) {
			Component c = component.getComponent(i);
			if (c instanceof JComponent) {
				updateDoubleBuffered((JComponent) c, dbcomponents);
			}
		}
	}

	/**
	 * 设置使用缓冲区
	 * 
	 * @author fuzhh 2013-11-7
	 * 
	 */
	private void resetDoubleBuffered(ArrayList<JComponent> dbcomponents) {
		for (JComponent component : dbcomponents) {
			component.setDoubleBuffered(true);
		}
	}
}
