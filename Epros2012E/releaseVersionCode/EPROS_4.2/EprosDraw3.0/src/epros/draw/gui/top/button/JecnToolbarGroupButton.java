package epros.draw.gui.top.button;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 组合按钮：按钮内部存在左右两个按钮实现不同功能
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolbarGroupButton extends JecnToolbarContentButton implements
		MouseListener {
	/** 左面按钮 */
	private JecnColorSelectButton leftBtn = null;
	/** 右面按钮 */
	private JecnToolbarContentButton rightBtn = null;

	public JecnToolbarGroupButton(JecnColorSelectButton leftBtn,
			JecnToolbarContentButton rightBtn) {
		super(null, null);

		if (leftBtn == null || rightBtn == null) {
			JecnLogConstants.LOG_TOOLBAR_GROUP_BUTTON
					.error("JecnToolbarGroupButton类构造函数：参数为null：leftBtn="
							+ leftBtn + ";rightBtn=" + rightBtn);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.leftBtn = leftBtn;
		this.rightBtn = rightBtn;

		initComponents();
	}

	/**
	 * 
	 * 布局组件
	 * 
	 */
	private void initComponents() {

		// 隐藏选中虚线边框
		leftBtn.setFocusable(false);
		rightBtn.setFocusable(false);
		// 图片内容显示方式：靠左
		rightBtn.setHorizontalAlignment(SwingConstants.CENTER);

		// 无边框
		this.getJToolBar().setBorder(null);
		leftBtn.getJToolBar().setBorder(null);
		rightBtn.getJToolBar().setBorder(null);

		// 注册鼠标事件
		leftBtn.addMouseListener(this);
		rightBtn.addMouseListener(this);

		this.setLayout(new BorderLayout(0, 0));
		this.add(leftBtn.getJToolBar(), BorderLayout.CENTER);
		this.add(rightBtn.getJToolBar(), BorderLayout.EAST);
	}

	public JecnColorSelectButton getLeftBtn() {
		return leftBtn;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		if (e.getSource() instanceof JecnToolbarContentButton) {
			// 源按钮
			JecnToolbarContentButton sourceBtn = (JecnToolbarContentButton) e
					.getSource();

			// 目标按钮
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(
					sourceBtn, e, JecnToolbarGroupButton.this);

			// 触发给定事件
			JecnToolbarGroupButton.this.dispatchEvent(desMouseEvent);

		} else if (e.getSource() instanceof JecnColorSelectButton) {
			// 源按钮
			JecnColorSelectButton sourceBtn = (JecnColorSelectButton) e
					.getSource();

			// 目标按钮
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(
					sourceBtn, e, JecnToolbarGroupButton.this);

			// 触发给定事件
			JecnToolbarGroupButton.this.dispatchEvent(desMouseEvent);
		}
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}
}
