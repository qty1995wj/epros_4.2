package com.jecn.epros.domain.user;

import java.util.List;

/**
 * Created by ZXH on 2017/3/23.
 */
public class MyProcessesData {
    /**
     * 岗位Id
     */
    private Long posId;
    /**
     * 岗位名称
     */
    private String posName;

    private List<RoleRelationProcess> relationProcessList;

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public List<RoleRelationProcess> getRelationProcessList() {
        return relationProcessList;
    }

    public void setRelationProcessList(List<RoleRelationProcess> relationProcessList) {
        this.relationProcessList = relationProcessList;
    }
}
