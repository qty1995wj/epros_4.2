package wordxml.element.bean.common;

import java.awt.Color;

import wordxml.constant.Constants.ShdType;

/**
 * 阴影属性
 * 
 */
public class ShdBean {
	// 单例
	private static final ShdBean singleton = new ShdBean();
	// // <w:shd w:val="pct-15" w:color="auto" w:fill="FFFFFF"
	// wx:bgcolor="D9D9D9"/>
	/*** 底纹样式值 （clear、solid、pct-5,pct-10,pct-15,pct-20） sdk766行 **/
	private ShdType val = ShdType.PCT15; // "pct-15";
	/*** 前景颜色 默认自动 ******/
	private String color = "auto";
	/*** 背景填充颜色 ********/
	private Color fill = Color.white;// "FFFFFF";

	/*** 在HTML等效的背景颜色。这是考虑到各种阴影设置，代表了相应的最终呈现的颜色 ******************/
	// private String bgcolor = "D9D9D9";

	public ShdBean() {
	}

	public ShdBean(ShdType val, String color, Color fill/* , String bgcolor */) {
		this.val = val;
		this.color = color;
		this.fill = fill;
		// this.bgcolor = bgcolor;
	}

	public ShdType getVal() {
		return val;
	}

	public void setVal(ShdType val) {
		this.val = val;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Color getFill() {
		return fill;
	}

	public void setFill(Color fill) {
		this.fill = fill;
	}

	/*
	 * public String getBgcolor() { return bgcolor; }
	 * 
	 * public void setBgcolor(String bgcolor) { this.bgcolor = bgcolor; }
	 */

	public static ShdBean getInstance() {
		return singleton;
	}

}
