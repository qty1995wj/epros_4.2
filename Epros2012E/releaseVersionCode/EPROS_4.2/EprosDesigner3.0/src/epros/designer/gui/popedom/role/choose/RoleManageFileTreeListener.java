package epros.designer.gui.popedom.role.choose;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnUtil;

public class RoleManageFileTreeListener extends JecnTreeListener {
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(RoleManageFileTreeListener.class);

	public RoleManageFileTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	/**
	 * 合拢
	 */
	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	/**
	 * 展开
	 */
	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = ConnectionPool.getFileAction().getRoleAuthChildFiles(
						node.getJecnTreeBean().getId(), JecnConstants.projectId, JecnConstants.getUserId());
				if (list != null && ((RoleManageHighEfficiencyFileTree) jTree).getChooseType() == 1) {
					for (int i = list.size() - 1; i >= 0; i--) {
						JecnTreeBean treeBean = list.get(i);
						if (treeBean.getTreeNodeType() == TreeNodeType.file && !JecnUtil.isImage(treeBean.getName())) {
							list.remove(treeBean);
						}
					}
				}
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("RoleManageFileTreeListener treeExpanded is error", e);
			}
			JecnTreeCommon.selectNode(jTree, node);
		}
	}

}
