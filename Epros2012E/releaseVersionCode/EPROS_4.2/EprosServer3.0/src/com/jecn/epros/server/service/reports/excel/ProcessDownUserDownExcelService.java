package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableSheet;

import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.ProcessAccessBean;

/**
 * 
 * 流程文档下载人统计
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessDownUserDownExcelService extends BaseDownExcelService {
	/** 数据对象 */
	private List<ProcessAccessBean> processAccessList = null;

	public ProcessDownUserDownExcelService() {
		this.mouldPath = getPrefixPath() + "mould/processDownUserReport.xls";
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (processAccessList == null) {
			return true;
		}

		try {
			// 第一行
			this.addFirstRowData(dataSheet);

			// 第三行开始
			for (int i = 0; i < processAccessList.size(); i++) {
				ProcessAccessBean bean = processAccessList.get(i);
				ProcessWebBean processWebBean = bean.getProcessWebBean();
				if (processWebBean == null) {
					processWebBean = new ProcessWebBean();
				}
				int row = startDataRow + i;
				// 流程名称
				dataSheet.addCell(new Label(0, row, processWebBean
						.getFlowName()));
				// 流程编号
				dataSheet.addCell(new Label(1, row, processWebBean
						.getFlowIdInput()));
				// 流程责任人
				dataSheet.addCell(new Label(2, row, processWebBean
						.getResPeopleName()));
				// 责任部门
				dataSheet
						.addCell(new Label(3, row, processWebBean.getOrgName()));
				// 访问人
				dataSheet.addCell(new Label(4, row, String.valueOf(bean
						.getAccessName())));
				// 访问时间
				dataSheet.addCell(new Label(5, row, String.valueOf(bean
						.getAccessTime())));
			}
			return true;
		} catch (Exception e) {
			log.error("ProcessAccessSumDownExcelService类editExcle方法", e);
			return false;
		}
	}

	public List<ProcessAccessBean> getProcessAccessList() {
		return processAccessList;
	}

	public void setProcessAccessList(List<ProcessAccessBean> processAccessList) {
		this.processAccessList = processAccessList;
	}
}
