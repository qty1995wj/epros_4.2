package epros.designer.gui.system.config.ui.property.type;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

/**
 * 
 * 全局权限一行面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPubItemPanel extends JecnPanel implements ActionListener {
	/** 流程地图 */
	private JLabel label = null;
	/** 流程地图-公开 */
	private JecnConfigRadioButton openRadio = null;
	/** 流程地图-秘密 */
	private JecnConfigRadioButton secretRadio = null;

	/** 标签内容 */
	private String labelText = null;

	public JecnPubItemPanel(String labelText) {
		this.labelText = labelText;

		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 流程地图
		label = new JLabel(labelText);
		// 流程地图-公开
		openRadio = new JecnConfigRadioButton(JecnProperties
				.getValue("gongKai"));
		// 流程地图-秘密
		secretRadio = new JecnConfigRadioButton(JecnProperties
				.getValue("secret"));

		this.setBorder(null);

		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(openRadio);
		buttonGroup.add(secretRadio);
		// 默认公开
		openRadio.setSelected(true);

		openRadio.setOpaque(false);
		secretRadio.setOpaque(false);

		openRadio.addActionListener(this);
		secretRadio.addActionListener(this);
		openRadio.setActionCommand("openRadio");
		secretRadio.setActionCommand("secretRadio");

		label.setHorizontalAlignment(SwingConstants.RIGHT);

		Dimension size = new Dimension(100, 25);
		label.setPreferredSize(size);
		label.setMaximumSize(size);
		label.setMinimumSize(size);
	}

	private void initLayout() {
		Insets insets = new Insets(0, 20, 1, 20);
		Insets insetsRight = new Insets(0, 20, 1, 100);
		GridBagConstraints c = null;
		// 流程label
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(label, c);
		// 流程公开选项
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(openRadio, c);
		// 流程秘密选项
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetsRight,
				0, 0);
		this.add(secretRadio, c);
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insetsRight,
				0, 0);
		this.add(new JLabel(), c);
	}

	/**
	 * 
	 * 初始化数据
	 * 
	 * @param itemBean
	 *            JecnConfigItemBean
	 */
	public void initData(JecnConfigItemBean itemBean) {
		if (itemBean == null) {
			return;
		}
		openRadio.setItemBean(itemBean);
		secretRadio.setItemBean(itemBean);

		// 值
		String value = itemBean.getValue();

		if (JecnConfigContents.ITEM_PUB_OPEN.equals(value)) {// 全局权限：公开
			// (0：秘密;1：公开)
			openRadio.setSelected(true);
		} else {
			secretRadio.setSelected(true);
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton) {
			JecnConfigRadioButton btn = (JecnConfigRadioButton) e.getSource();
			if (btn.isSelected()) {// 是否选中
				// 数据对象
				JecnConfigItemBean itemBean = btn.getItemBean();
				if (itemBean == null) {
					return;
				}
				// (0：秘密 1：公开)
				if ("openRadio".equals(btn.getActionCommand())) {// 公开
					btn.getItemBean()
							.setValue(JecnConfigContents.ITEM_PUB_OPEN);
				} else if ("secretRadio".equals(btn.getActionCommand())) {// 秘密
					btn.getItemBean().setValue(
							JecnConfigContents.ITEM_PUB_SECRET);
				}
			}
		}
	}
}
