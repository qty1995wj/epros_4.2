package com.jecn.epros.bean.checkout;

import java.io.Serializable;
import java.util.List;

public class CheckoutItem implements Serializable {
    private String name;
    private List<String> cellA;
    private List<String> cellB;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCellA() {
        return cellA;
    }

    public void setCellA(List<String> cellA) {
        this.cellA = cellA;
    }

    public List<String> getCellB() {
        return cellB;
    }

    public void setCellB(List<String> cellB) {
        this.cellB = cellB;
    }
}
