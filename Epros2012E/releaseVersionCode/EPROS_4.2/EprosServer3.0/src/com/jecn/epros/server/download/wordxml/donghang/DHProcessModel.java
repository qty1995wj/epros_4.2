package com.jecn.epros.server.download.wordxml.donghang;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellBean;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

/**
 * 东航操作说明下载
 * 
 * @author admin
 * 
 */
public class DHProcessModel extends ProcessFileModel {
	/*** 段落行距1.5倍行距 ****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1.5F,
			LineRuleType.AUTO);

	private ParagraphLineRule vLine = new ParagraphLineRule(1F,
			LineRuleType.AUTO);

	public DHProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle("、", textTitleStyle());
		getDocProperty().setNewTblWidth(17.49F);
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(2.5F, 2F, 1.1F, 2F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);

	}

	/**
	 * 添加封面节点封皮内容
	 */
	private void addTitleSectContent() {
		// 宋体 五号 左对齐 1.5倍数行距
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体",
				Constants.wuhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		TableBean tabBean = new TableBean();
		tabBean.setBorder(0.5F);
		tabBean.setTableAlign(Align.center);
		tabBean.setCellMargin(0, 0.19F, 0, 0.19F);
		List<String[]> rowData = new ArrayList<String[]>();
		// 评审人集合
		/** 评审人集合 0 评审人类型 1名称 2日期 */
		List<Object[]> peopleList = processDownloadBean.getPeopleList();
		String[] str = null;
		for (Object[] obj : peopleList) {
			str = new String[] { "" + obj[0], "" + obj[1], "日期：", "" + obj[2] };
			rowData.add(str);
		}
		getDocProperty().getTitleSect().createParagraph("");
		getDocProperty().getTitleSect().createParagraph("");
		Table tab = JecnWordUtil.createTab(getDocProperty().getTitleSect(),
				tabBean, new float[] { 4.36F, 4.36F, 4.36F, 4.36F }, rowData,
				pBean);
		/******* 设置第一行样式 黑体五号 *********/
		pBean = new ParagraphBean("宋体", Constants.wuhao, true);
		// 设置第一列样式
		tab.setCellStyle(0, pBean);
		tab.setCellStyle(2, pBean);
		/**** 设置行高和 垂直居中 ******/
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.getRow(i).setHeight(0.71F);
		}
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		Table table = null;
		Paragraph p = null;
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.wuhao, false);
		List<String[]> rowData = new ArrayList<String[]>();
		Header hdr = sect.createHeader(type);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		// 流程编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		// 文件版本
		String flowVersion = processDownloadBean.getFlowVersion();
		// 发布时间
		String releaseDate = processDownloadBean.getReleaseDate();
		// 生效时间
		String ectiveDate = processDownloadBean.getEffectiveDate();
		// 公司名称
		String companyName = processDownloadBean.getCompanyName();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		rowData.add(new String[] { "", "文件编号", flowInputNum });
		rowData.add(new String[] { "", "文件版本", flowVersion });
		rowData.add(new String[] { "", "发布日期", releaseDate });
		rowData.add(new String[] { "", "生效日期", ectiveDate });
		rowData.add(new String[] { "", "页    码", "第" });
		table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 10.69F,
				2.25F, 4.44F }, rowData, pBean);
		table.getRow(0).getCell(0).setRowspan(5);
		for (int i = 0; i < table.getRowCount(); i++) {
			table.getRow(i).setHeight(0.5F);
		}
		/*** 宋体 小二 加粗 1.5倍行距 ******/
		ParagraphBean tempPBean = new ParagraphBean(Align.center, "宋体",
				Constants.xiaoer, true);
		tempPBean.setSpace(0f, 0f, vLine1);
		/*** 宋体 二号加粗 单倍行距 ******/
		ParagraphBean temp1PBean = new ParagraphBean(Align.center, "宋体",
				Constants.erhao, false);
		temp1PBean.setSpace(0f, 0f, vLine);
		/***** 公司logo图标 ****/
		Image logo = new Image(getDocProperty().getLogoImage());
		PositionBean positionBean = new PositionBean(PositionType.absolute,
				3.05F, 0, 19.7F, 0);
		logo.setSize(9.38F, 1.22F);
		logo.setPositionBean(positionBean);
		// table.getRow(0).getCell(0).createParagraph(logo);
		// table.getRow(0).getCell(0).getParagraph(0).appendTextRun(companyName);
		table.getRow(0).getCell(0).getParagraph(0).appendGraphRun(logo);
		table.getRow(0).getCell(0).createParagraph("公司logo图标", temp1PBean);
		// table.getRow(0).getCell(0).getParagraph(0).setParagraphBean(temp1PBean);
		/***** 流程名称 ****/
		p = table.getRow(0).getCell(0).createParagraph(flowName, tempPBean);
		p = table.getRow(4).getCell(2).getParagraph(0);
		p.appendCurPageRun();
		p.appendTextRun("页 共  ");
		p.appendTotalPagesRun(0);
		p.appendTextRun(" 页");
		hdr.createParagraph("");

		// 右边第二个 三个 单元格样式
		CellBean rightCellBean = new CellBean();
		rightCellBean.setBorderBottom(1);
		CellBean rightCellTitleBean = new CellBean();
		rightCellTitleBean.setBorderTop(2);
		rightCellTitleBean.setBorderBottom(1);

		// 左边第一个单元格样式
		CellBean leftCellBean = new CellBean();
		leftCellBean.setBorderBottom(1);
		leftCellBean.setBorderRight(1);
		CellBean leftCellTitleBean = new CellBean();
		leftCellTitleBean.setBorderTop(2);
		leftCellTitleBean.setBorderBottom(1);
		leftCellTitleBean.setBorderRight(1);
		for (int i = 0; i < table.getRowCount(); i++) {
			if (i == 0) {
				table.getRow(i).getCell(0).setCellBean(leftCellTitleBean);
				table.getRow(i).getCell(1).setCellBean(rightCellTitleBean);
				table.getRow(i).getCell(2).setCellBean(rightCellTitleBean);
			} else {
				table.getRow(i).getCell(0).setCellBean(leftCellBean);
				table.getRow(i).getCell(1).setCellBean(rightCellBean);
				table.getRow(i).getCell(2).setCellBean(rightCellBean);
			}
		}
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体",
				Constants.xiaosi, false);
		Footer ftr = sect.createFooter(type);
		ftr.createParagraph("公司机密,未经批准不得扩散", pBean);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		firstSect.setSectBean(getSectBean());
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.5F, 2F, 1.1F, 2F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		addTitleSectContent();
		createCommhdrftr(titleSect);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体",
				Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0.19F, 0, 0.19F);
		// 表格左缩进0.56厘米
		tblStyle.setTableLeftInd(0.56F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体",
				Constants.wuhao, false);
		textContentStyle.setSpace(0.2F, 0.2F, vLine1);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "宋体",
				Constants.wuhao, true);
		textTitleStyle.setSpace(0f, 0f, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

}
