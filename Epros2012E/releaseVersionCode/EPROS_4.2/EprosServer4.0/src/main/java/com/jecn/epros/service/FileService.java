package com.jecn.epros.service;

import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.file.FileBaseInfoBean;
import com.jecn.epros.domain.file.FileUseDetailBean;
import com.jecn.epros.domain.file.SearchFileResultBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;

import java.util.List;
import java.util.Map;

public interface FileService {

    Attachment fetchAttachmentById(Long id);

    /**
     * 树节点展开
     *
     * @param map
     * @return
     */
    public List<TreeNode> findChildFiles(Map<String, Object> map);


    /**
     * 概括信息
     *
     * @param map
     * @return
     */
    public FileBaseInfoBean findBaseInfoBean(Map<String, Object> map);


    /**
     * 搜索
     *
     * @param map
     * @return
     */
    public List<SearchFileResultBean> searchFile(Map<String, Object> map);

    /**
     * 搜索 总数
     *
     * @param map
     * @return
     */
    public int searchFileTotal(Map<String, Object> map);


    public List<FileUseDetailBean> findFileUseDetail(Map<String, Object> map);

    Inventory getInventory(Map<String, Object> paramMap);

}
