package com.jecn.epros.server.service.dataImport.match.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.dao.dataImport.match.IMatchUserImportDao;
import com.jecn.epros.server.service.dataImport.match.IPostionMatchService;
import com.jecn.epros.server.webBean.dataImport.match.ActualPosBean;
import com.jecn.epros.server.webBean.dataImport.match.PosEprosRelBean;

/**
 * 
 * 岗位匹配处理类
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Mar 1, 2013 时间：8:59:29 AM
 */
@Transactional(rollbackFor = Exception.class)
public class PostionMatchServiceImpl implements IPostionMatchService {
	private final Log log = LogFactory.getLog(PostionMatchServiceImpl.class);
	private IMatchUserImportDao matchUserImportDao;

	/**
	 * 获取岗位匹配总数
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getStatcionRowCount() throws Exception {
		return 0;
	}

	/**
	 * 获取当前页岗位匹配集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<PosEprosRelBean> getStatcionData(int pageSize, int startRow)
			throws Exception {
		return null;
	}

	@Override
	public boolean checkProjectId() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 获取未匹配的岗位结果
	 * 	@param type: 1:未匹配岗位集合，5：未匹配流程岗位，6：未匹配基准岗位
	 * @return
	 * @throws Exception
	 */
	public List<ActualPosBean> getActualPosBeanList(int type, int startRow,
			int pageSize,String posName) throws Exception {
		List<ActualPosBean> list = new ArrayList<ActualPosBean>();
		List<Object[]> objectList = null;
		if (type == 1) {//为匹配岗位集合
			objectList = matchUserImportDao.getActualPosBeanList(pageSize, startRow,posName);
			for(Object[] actPosObj : objectList){
					ActualPosBean posBean = new ActualPosBean();
					if (actPosObj[5] != null) {
						posBean.setActPosName(actPosObj[5].toString());
					}
					if (actPosObj[2] != null) {
						posBean.setBasePosName(actPosObj[2].toString());
					}
					if(actPosObj[4] != null){
						posBean.setActPosNum(actPosObj[4].toString());
					}
					if(actPosObj[6] != null){
						posBean.setDeptNum(actPosObj[6].toString());
					}
					posBean.setRadioType(type);
					list.add(posBean);
			}
			return list;
		} else if (type == 5 || type == 6) {
			// 3 :0 部门名称，1岗位名称 2：0，基准岗位编号，1 基准岗位名称
			objectList = matchUserImportDao.getNoMatchObject(
					type, startRow, pageSize,posName);
			
			for (Object[] object : objectList) {
				ActualPosBean posBean = new ActualPosBean();
				if (object[0] != null) {
					posBean.setActPosName(object[0].toString());
				}
				if (object[1] != null) {
					posBean.setBasePosName(object[1].toString());
				}
				if(type == 5){
					if(object[2] != null){
						posBean.setId(Long.valueOf(object[2].toString()));
					}
				}
				posBean.setRadioType(type);
				list.add(posBean);
			}
			return list;
		}
		return null;
	}

	/**
	 * 
	 * 获取未匹配的岗位或基准岗位或流程岗位
	 * 
	 * @param type
	 *            1:HR实际岗位；5：流程岗位；6基准岗位
	 * @return
	 * @throws DaoException
	 */
	@Override
	public int getNoMatchObjectCount(int type,String posName) throws Exception {
		return matchUserImportDao.getNoMatchObjectCount(type,posName);
	}

	public IMatchUserImportDao getMatchUserImportDao() {
		return matchUserImportDao;
	}

	public void setMatchUserImportDao(IMatchUserImportDao matchUserImportDao) {
		this.matchUserImportDao = matchUserImportDao;
	}
}
