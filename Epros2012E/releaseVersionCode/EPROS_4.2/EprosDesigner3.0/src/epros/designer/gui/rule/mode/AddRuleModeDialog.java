package epros.designer.gui.rule.mode;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.bean.rule.RuleModeTitleBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 
 * 工具栏-设置-制度模板-新建模板
 * 
 * @author zhangj
 * 
 */
public class AddRuleModeDialog extends RuleModeDialog {

	private static Logger log = Logger.getLogger(AddRuleModeDialog.class);
	private JecnTreeNode pNode = null;
	private JecnTree jTree = null;

	public AddRuleModeDialog(JecnTreeNode pNode, JecnTree jTree) {
		this.setTitle(JecnProperties.getValue("newMode"));
		this.pNode = pNode;
		this.jTree = jTree;
	}

	@Override
	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 目的
		Vector<String> row = new Vector<String>();
		row.add("1");
		//row.add(JecnProperties.getValue("purpose"));
		row.add("目的");
		row.add("Objectives");
		row.add(JecnProperties.getValue("content"));
		row.add(JecnProperties.getValue("task_emptyBtn"));
		vs.add(row);
		// 适用范围
		row = new Vector<String>();
		row.add("2");
		//row.add(JecnProperties.getValue("useScope"));
		row.add("适用范围");
		row.add("Applicable scope");
		row.add(JecnProperties.getValue("content"));
		row.add(JecnProperties.getValue("task_emptyBtn"));
		vs.add(row);
		// 职责
		row = new Vector<String>();
		row.add("3");
		//row.add(JecnProperties.getValue("responsibility"));
		row.add("职责");
		row.add("Responsibility");
		row.add(JecnProperties.getValue("content"));
		row.add(JecnProperties.getValue("task_emptyBtn"));
		vs.add(row);
		// 工作程序
		row = new Vector<String>();
		row.add("4");
		//row.add(JecnProperties.getValue("jobProgram"));
		row.add("工作程序");
		row.add("Working Procedure");
		row.add(JecnProperties.getValue("content"));
		row.add(JecnProperties.getValue("task_emptyBtn"));
		vs.add(row);
		// 附件/附表
		row = new Vector<String>();
		row.add("5");
		/*row.add(JecnProperties.getValue("accessories"));*/
		row.add("附件/附表");
		row.add("Attachment/Table");
		row.add(JecnProperties.getValue("fileForm"));
		row.add(JecnProperties.getValue("task_emptyBtn"));
		vs.add(row);
		// 支持文件
		row = new Vector<String>();
		row.add("6");
		//row.add(JecnProperties.getValue("supportFile"));
		row.add("支持文件");
		row.add("Supporting files");
		row.add(JecnProperties.getValue("fileForm"));
		row.add(JecnProperties.getValue("task_emptyBtn"));
		vs.add(row);
		return vs;
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name,
				TreeNodeType.ruleMode);
	}

	@Override
	public void savaData(List<RuleModeTitleBean> titleList) throws Exception {

		RuleModeBean ruleModeBean = new RuleModeBean();
		ruleModeBean.setModeName(this.getNameField().getText().trim());
		ruleModeBean.setModeType(1);
		// 更新人
		ruleModeBean.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser()
				.getPeopleId());
		ruleModeBean.setCreatePeopleId(JecnConstants.loginBean.getJecnUser()
				.getPeopleId());
		// 设置父ID
		ruleModeBean.setParentId(pNode.getJecnTreeBean().getId());
		// 设置序号
		ruleModeBean.setSortId(JecnTreeCommon.getMaxSort(pNode));
		// 保存到数据库
		Long id = ConnectionPool.getRuleModeAction().addRuleMode(ruleModeBean,
				titleList);

		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		jecnTreeBean.setId(id);
		jecnTreeBean.setName(this.getNameField().getText().trim());
		jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
		jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
		jecnTreeBean.setTreeNodeType(TreeNodeType.ruleMode);
		JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
		this.dispose();
	}

}
