package epros.draw.data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 
 * 所有流程图或流程地图都存放在此类中
 * 
 * @author Administrator
 * 
 */
public class JecnMapListData {

	/** 流程地图或流程图集合 */
	private Map<String, JecnFlowMapData> flowMapList = new ConcurrentHashMap<String, JecnFlowMapData>();

	/**
	 * 
	 * 已存在获取流程图或流程地图个数
	 * 
	 * @return int
	 */
	public int getMapCount() {
		return flowMapList.size();
	}

}
