package epros.designer.gui.system.config.ui.property.type;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 流程发布相关配置
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2014-2-26 时间：下午01:52:52
 */
public class JecnRulePublicItemPanel extends JecnAbstractPropertyBasePanel implements ActionListener {
	/** 相关文件，复选框 */
	private JecnConfigCheckBox checkRelateFileBox;
	/** 是否废止相关文件，复选框 */
	private JecnConfigCheckBox checkAbolishBox;
	

	private JecnPanel checkPanel = null;
	
	private JecnPanel abolishPanel = null;

	public JecnRulePublicItemPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 文件：
		checkRelateFileBox = new JecnConfigCheckBox(JecnProperties.getValue("file"));
		checkAbolishBox = new JecnConfigCheckBox(JecnProperties.getValue("AbolishFile"));

		checkPanel = new JecnPanel();
		abolishPanel = new JecnPanel();
		
		checkPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		checkRelateFileBox.setSelected(false);
		checkAbolishBox.setSelected(false);

		checkRelateFileBox.setOpaque(false);
		checkAbolishBox.setOpaque(false);
		
		checkRelateFileBox.addActionListener(this);
		checkAbolishBox.addActionListener(this);
		
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setLayout(new GridBagLayout());
	}

	/**
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		checkPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		checkPanel.add(checkRelateFileBox);
		checkPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("releaseIf_file")));

		
		abolishPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		abolishPanel.add(checkAbolishBox); //单选框
		abolishPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("abolishIf_file")));

		int rows = 0;
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		this.add(checkPanel, c);
		rows++;

		c = new GridBagConstraints(0, rows, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(abolishPanel, c);
	}

	@Override
	public boolean check() {
		return false;
	}

	/**
	 * 初始化获取数据
	 * 
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;
		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();
			if (ConfigItemPartMapMark.pubRuleFile.toString().equals(mark)) {// 发布流程相关文件
				checkRelateFileBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					checkRelateFileBox.setSelected(true);
				} else {
					checkRelateFileBox.setSelected(false);
				}
			}
			if (ConfigItemPartMapMark.ruleAbolish.toString().equals(mark)) {
				checkAbolishBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					checkAbolishBox.setSelected(true);
				} else {
					checkAbolishBox.setSelected(false);
				}
			}
			
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == checkRelateFileBox) {// 1：选中；0不选中
				if (checkRelateFileBox.getItemBean() == null) {
					return;
				}
				if (checkRelateFileBox.isSelected()) {// 选中
					checkRelateFileBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					checkRelateFileBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}else if(e.getSource() == checkAbolishBox){
				if (checkAbolishBox.isSelected()) {// 选中
					checkAbolishBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					checkAbolishBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
	}
}
