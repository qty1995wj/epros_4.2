<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
response.setHeader("P3P" , "CP=\"CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR\"" );
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/search.css" />
<link rel="stylesheet" type="text/css" href="css/top.css" />
<link rel="stylesheet" type="text/css" href="css/jecn.css" />
<link rel="stylesheet" type="text/css"
	href="ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="ext/resources/css/xtheme-gray.css" />

<script type="text/javascript" src="ext/adapter/ext/ext-base.js" charset="UTF-8"></script>
<script type="text/javascript" src="ext/ext-all.js" charset="UTF-8"></script>

<s:if test="#session.country=='US'">
</s:if>
<s:else>
<script type="text/javascript"
	src="js/epros_zh_CN.js" charset="UTF-8"></script>
<script type="text/javascript"
	src="ext/ext-lang-zh_CN.js" charset="UTF-8"></script>
</s:else>	
<script type="text/javascript" src="jquery/jquery-1.6.2.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/common.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/processSys/eprosButtPublicFlow.js"></script>
</head>
<body onload="buttPublicFlow();">
<div>
<div id="eprosButtPublicFlow_id">

</div>
</div>
</body>
</html>