package com.jecn.epros.domain.process.inout;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * 流程文件专用
 */
public class JecnFlowInoutPosBean implements Serializable {
    private String id;
    /**
     * 输入输出主键
     */
    private String inoutId;
    private String rName;
    private List<String> pName;
    private Long rId;
    /**
     * 0岗位 1岗位组
     */
    private Integer rType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInoutId() {
        return inoutId;
    }

    public void setInoutId(String inoutId) {
        this.inoutId = inoutId;
    }

    public Long getrId() {
        return rId;
    }

    public void setrId(Long rId) {
        this.rId = rId;
    }

    public Integer getrType() {
        return rType;
    }

    public void setrType(Integer rType) {
        this.rType = rType;
    }

    public String getrName() {
        return rName;
    }

    public String getName() {
        StringBuilder str = new StringBuilder();
        if (pName != null && !pName.isEmpty()) {
            for (String p : pName) {
                str.append(p);
                if (StringUtils.isNotBlank(str)) {
                    str.append("/");
                }
            }
        }
        str.append(rName);
        str.append("<br>");
        return str.toString();
    }

    public void setpName(List<String> pName) {
        this.pName = pName;
    }

    public void setrName(String rName) {
        this.rName = rName;
    }

    public List<String> getpName() {
        return pName;
    }
}
