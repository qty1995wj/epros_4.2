package epros.designer.gui.system.config.ui.comp;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JTextArea;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

/**
 * 
 * 输入框
 * 
 * @author Administrator
 * 
 */
public class JecnConfigTextArea extends JTextArea {

	/** 数据对象 */
	private JecnConfigItemBean itemBean = null;

	public JecnConfigTextArea() {
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	}

	public JecnConfigItemBean getItemBean() {
		return itemBean;
	}

	public void setItemBean(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
	}

}
