package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 流程相关风险
 * 
 *@author admin
 * @date 2017-4-20下午06:03:49
 */
public class JecnFlowRelatedRisk implements Serializable {
	private String id;
	private Long flowId;
	private Long riskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getRiskId() {
		return riskId;
	}

	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}

}
