package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

import java.util.ArrayList;
import java.util.List;

public class PositionBean {
	private Long id;
	private String posId;
	private String posName;
	private Long deptId;
	private Integer isMatch;
	private List<PersonBean> personList = new ArrayList<PersonBean>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public List<PersonBean> getPersonList() {
		return personList;
	}

	public void setPersonList(List<PersonBean> personList) {
		this.personList = personList;
	}

	public Integer getIsMatch() {
		return isMatch;
	}

	public void setIsMatch(Integer isMatch) {
		this.isMatch = isMatch;
	}

}
