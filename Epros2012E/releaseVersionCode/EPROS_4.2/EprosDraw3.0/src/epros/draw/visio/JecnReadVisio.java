package epros.draw.visio;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.Element;

import epros.draw.util.DrawCommon;

/**
 * 读取visio数据
 * 
 * @author fuzhh 2013-6-9
 * 
 */
public class JecnReadVisio {
	/** 字体Map集合数据 <ID,字体名称> */
	private Map<String, String> fontMap = new HashMap<String, String>();
	/** 存储全部图形ID和图形标识的MAP集合 */
	private List<JecnVisioFigureData> figureList = new ArrayList<JecnVisioFigureData>();
	/** 存储线数据最后处理 */
	private Map<Element, JecnVisioFigureData> lineMap = new HashMap<Element, JecnVisioFigureData>();
	/** 存储没有标识的图形节点 */
	private List<Element> notFlagList = new ArrayList<Element>();
	/** 没有标识的图形数据 */
	private List<JecnVisioFigureData> notFigureTypeList = new ArrayList<JecnVisioFigureData>();
	/** Master数据Map集合 key:ID value:nameU */
	private List<JecnMasterDataBean> masterDataList = new ArrayList<JecnMasterDataBean>();

	/**
	 * 处理visio
	 * 
	 * @author fuzhh 2013-7-3
	 * @return
	 */
	public List<JecnVisioPageDataBean> readVisioData(String path) {
		// String path = "D:\\sssAaa.xml";
		// 获取document
		Document document = JecnVisioXmlUtil.getDocumentByXml(path);
		// 获取根节点
		Element rootElement = JecnVisioXmlUtil.getRootElement(document);
		// 找到 Pages(页)节点
		List<Element> pagesList = new ArrayList<Element>();
		JecnVisioXmlUtil.getAllChildNodeList(rootElement, pagesList, "Pages");
		// 字体节点
		List<Element> faceNamesList = new ArrayList<Element>();
		JecnVisioXmlUtil.getAllChildNodeList(rootElement, faceNamesList,
				"FaceNames");
		for (Element faceNamesEle : faceNamesList) {
			List<Element> faceNameList = new ArrayList<Element>();
			JecnVisioXmlUtil.getAllChildNodeList(faceNamesEle, faceNameList,
					"FaceName");
			for (Element faceNameEle : faceNameList) {
				// 获取ID 属性
				String id = JecnVisioXmlUtil.getNodeAttValue(faceNameEle, "ID");
				// 获取字体名称
				String name = JecnVisioXmlUtil.getNodeAttValue(faceNameEle,
						"Name");
				fontMap.put(id, name);
			}
		}

		// 找到 Master 节点
		List<Element> mastersList = new ArrayList<Element>();
		JecnVisioXmlUtil.getAllChildNodeList(rootElement, mastersList,
				"Masters");
		for (Element mastersEle : mastersList) {
			List<Element> masterList = new ArrayList<Element>();
			JecnVisioXmlUtil.getAllChildNodeList(mastersEle, masterList,
					"Master");
			for (Element masterEle : masterList) {
				JecnMasterDataBean masterDataBean = new JecnMasterDataBean();
				// 获取ID
				String masterId = JecnVisioXmlUtil.getNodeAttValue(masterEle,
						"ID");
				// 获取NameU
				String nameU = JecnVisioXmlUtil.getNodeAttValue(masterEle,
						"NameU");
				masterDataBean.setMasterId(masterId);
				masterDataBean.setNameU(nameU);
				// 存储样式的List
				List<JecnVisioStyleDateBean> visioStyleDateList = new ArrayList<JecnVisioStyleDateBean>();
				// 样式节点 ix 属性
				String charIx = "";
				// text 节点
				Element textNodeEle = null;

				// 获取Master下的Char节点
				List<Element> charNodesList = new ArrayList<Element>();
				JecnVisioXmlUtil.getAllChildNodeList(masterEle, charNodesList,
						"Char");
				for (Element charEle : charNodesList) {
					JecnVisioStyleDateBean visioStyleDate = new JecnVisioStyleDateBean();
					shapeCharNodeHandle(charEle, visioStyleDate);
					charIx = JecnVisioXmlUtil.getNodeAttValue(charEle, "IX");
					visioStyleDate.setIx(charIx);
					visioStyleDateList.add(visioStyleDate);
				}
				// 获取Master下的Char节点
				List<Element> textNodesList = new ArrayList<Element>();
				JecnVisioXmlUtil.getAllChildNodeList(masterEle, textNodesList,
						"Text");
				for (Element textEle : textNodesList) {
					textNodeEle = textEle;
					break;
				}
				if (textNodeEle != null) {
					// 获取样式标识
					String textIx = shapeTextNodeHandle(textNodeEle);
					// 循环样式
					for (JecnVisioStyleDateBean visioStyleDate : visioStyleDateList) {
						if (textIx.equals(visioStyleDate.getIx())) {
							masterDataBean
									.setVisioStyleDateBean(visioStyleDate);
							break;
						}
					}
				}
				masterDataList.add(masterDataBean);
			}

		}

		// 页面集合
		List<JecnVisioPageDataBean> visioPageDataBeanList = new ArrayList<JecnVisioPageDataBean>();
		for (Element pagesEle : pagesList) {
			List<Element> pageList = new ArrayList<Element>();
			JecnVisioXmlUtil.getAllChildNodeList(pagesEle, pageList, "Page");
			for (Element pageEle : pageList) {
				JecnVisioPageDataBean visioPageDataBean = new JecnVisioPageDataBean();
				pageHandle(pageEle, visioPageDataBean);
				visioPageDataBeanList.add(visioPageDataBean);
			}
		}
		return visioPageDataBeanList;
	}

	/**
	 * 处理页面节点
	 * 
	 * @author fuzhh 2013-6-9
	 * @param pagesEle
	 *            页面节点
	 */
	public JecnVisioPageDataBean pageHandle(Element pagesEle,
			JecnVisioPageDataBean visioPageDataBean) {
		// 存储visio 图形的集合
		List<JecnFlowStructureImage> flowStructureImageList = new ArrayList<JecnFlowStructureImage>();
		// 获取 page的Nameu属性
		String nameUStr = JecnVisioXmlUtil.getNodeAttValue(pagesEle, "NameU");
		visioPageDataBean.setPageNameU(nameUStr);
		// 获取page的Name属性
		String nameStr = JecnVisioXmlUtil.getNodeAttValue(pagesEle, "Name");
		visioPageDataBean.setPageName(nameStr);
		// 找到Shapes节点
		List<Element> shapesList = new ArrayList<Element>();
		JecnVisioXmlUtil.getAllChildNodeList(pagesEle, shapesList, "Shapes");
		// 循环Shapes节点
		for (Element shapesEle : shapesList) {
			shapesHandle(shapesEle, flowStructureImageList);
		}
		visioPageDataBean.setFlowStructureImageList(flowStructureImageList);
		return visioPageDataBean;
	}

	/**
	 * 处理内容节点
	 * 
	 * @author fuzhh 2013-6-9
	 * @param shapesEle
	 *            内容节点
	 */
	public void shapesHandle(Element shapesEle,
			List<JecnFlowStructureImage> flowStructureImageList) {
		// 找到shape节点
		List<Element> shapeList = new ArrayList<Element>();
		JecnVisioXmlUtil.getAllChildNodeList(shapesEle, shapeList, "Shape");
		// 读取顺序
		int index = 0;
		for (Element shapeEle : shapeList) {
			// 存储图形数据
			JecnFlowStructureImage flowStructureImage = new JecnFlowStructureImage();
			// 图形标识
			JecnVisioFigureData visioFigureData = shapeChildAttribute(shapeEle,
					index);
			if (!DrawCommon.isNullOrEmtry(visioFigureData.getFugureType())) {
				// 处理形状下的节点数据
				shapeChildNodeHandle(shapeEle, flowStructureImage,
						visioFigureData, false);
				if (!DrawCommon.isNullOrEmtry(flowStructureImage.getFigureType())) {
					flowStructureImageList.add(flowStructureImage);
				}
			} else {
				notFigureTypeList.add(visioFigureData);
			}
		}

		// 没有标识的图形
		for (Element shapeEle : notFlagList) {
			// 存储图形数据
			JecnFlowStructureImage flowStructureImage = new JecnFlowStructureImage();
			String master = JecnVisioXmlUtil
					.getNodeAttValue(shapeEle, "Master");
			// 获取 shape 的 ID
			String shapeId = JecnVisioXmlUtil.getNodeAttValue(shapeEle, "ID");
			if (DrawCommon.isNullOrEmtry(shapeId)) {
				continue;
			}
			// 图形标识
			JecnVisioFigureData JecnVisioFigureData = null;
			for (JecnVisioFigureData visioFigureDataBean : notFigureTypeList) {
				if (shapeId.equals(visioFigureDataBean.getFigureId())) {
					JecnVisioFigureData = visioFigureDataBean;
					break;
				}
			}
			if (JecnVisioFigureData != null && !DrawCommon.isNullOrEmtry(master)) {
				String nameUval = null;
				// 图形的ID
				for (JecnMasterDataBean masterDataBean : masterDataList) {
					if (master.equals(masterDataBean.getMasterId())) {
						nameUval = masterDataBean.getNameU();
						break;
					}
				}
				if (!DrawCommon.isNullOrEmtry(nameUval)) {
					JecnVisioFigureData.setFugereFlag(nameUval);
					String nameU = null;
					String[] nameUStrLen = nameUval.split("\\.");
					if (nameUStrLen.length > 0) {
						nameU = nameUStrLen[0];
					} else {
						nameU = nameUval;
					}
					JecnVisioFigureData.setFugureType(nameU);
					// 处理形状下的节点数据
					shapeChildNodeHandle(shapeEle, flowStructureImage,
							JecnVisioFigureData, false);
					if (!DrawCommon.isNullOrEmtry(flowStructureImage.getFigureType())) {
						flowStructureImageList.add(flowStructureImage);
					}
				}
			}
			// else if (JecnVisioFigureData != null
			// && JecnVisioFigureData.getMasterShape() != null) {
			// if ("7".equals(JecnVisioFigureData.getMasterShape())) {
			// JecnVisioFigureData.setFugureType("roleFigure");
			// // 处理形状下的节点数据
			// shapeChildNodeHandle(shapeEle, flowStructureImage,
			// JecnVisioFigureData, false);
			// if (!Tool.isNullOrEmtry(flowStructureImage.getFigureType())) {
			// flowStructureImageList.add(flowStructureImage);
			// }
			// }
			// }
		}
		// 获取map key值
		Set<Element> keySet = lineMap.keySet();
		// 循环key值
		for (Element lineEle : keySet) {// 遍历key
			// 存储图形数据
			JecnFlowStructureImage flowStructureImage = new JecnFlowStructureImage();
			// 图形数据
			JecnVisioFigureData visioFigureData = lineMap.get(lineEle);
			// 处理形状下的节点数据
			shapeChildNodeHandle(lineEle, flowStructureImage, visioFigureData,
					true);
			flowStructureImageList.add(flowStructureImage);
		}
		// 清空所有存储 图形 线的数据
		lineMap.clear();
		notFlagList.clear();
		masterDataList.clear();
		notFigureTypeList.clear();
		figureList.clear();
		fontMap.clear();
	}

	/**
	 * 处理图形的属性 ， 图形的标识
	 * 
	 * @author fuzhh 2013-7-10
	 * @param shapeEle
	 *            图形
	 * @param index
	 *            读取顺序
	 * @return
	 */
	public JecnVisioFigureData shapeChildAttribute(Element shapeEle, int index) {
		// 存储图形数据
		JecnVisioFigureData visioFigureData = new JecnVisioFigureData();
		String masterShapeVal = JecnVisioXmlUtil.getNodeAttValue(shapeEle,
				"MasterShape");
		// 获取 shape 的 ID
		String shapeId = JecnVisioXmlUtil.getNodeAttValue(shapeEle, "ID");
		visioFigureData.setFigureId(shapeId);
		visioFigureData.setIndex(index);
		String nameUStr = "";
		// 获取 shape的Nameu属性（图形标识）
		String nameUStrVal = JecnVisioXmlUtil
				.getNodeAttValue(shapeEle, "NameU");
		visioFigureData.setFugereFlag(nameUStrVal);
		String master = JecnVisioXmlUtil.getNodeAttValue(shapeEle, "Master");
		visioFigureData.setMaster(master);
		visioFigureData.setMasterShape(masterShapeVal);
		// 获取shape的Name属性 （图形中文唯一标识）
		// String nameStr = JecnVisioXmlUtil.getNodeAttValue(shapeEle, "Name");
		// 获取图形标识
		if (nameUStrVal != null) {
			String[] nameUStrLen = nameUStrVal.split("\\.");
			if (nameUStrLen.length > 0) {
				nameUStr = nameUStrLen[0];
			} else {
				nameUStr = nameUStrVal;
			}
		}
		visioFigureData.setFugureType(nameUStr);
		if (DrawCommon.isNullOrEmtry(nameUStr)) {
			notFlagList.add(shapeEle);
		}
		// 图形数据添加到List集合
		figureList.add(visioFigureData);
		return visioFigureData;
	}

	/**
	 * 处理形状节点下的节点数据
	 * 
	 * @author fuzhh 2013-6-9
	 * @param shapeEle
	 *            形状节点
	 */
	public void shapeChildNodeHandle(Element shapeEle,
			JecnFlowStructureImage flowStructureImage,
			JecnVisioFigureData visioFigureDatar, boolean isLine) {
		if (visioFigureDatar == null || visioFigureDatar.getFigureId() == null) {
			return;
		}
		String master = JecnVisioXmlUtil.getNodeAttValue(shapeEle, "Master");
		String nameUStr = visioFigureDatar.getFugureType();
		if (!isLine && "Dynamic connector".equalsIgnoreCase(nameUStr)) {
			lineMap.put(shapeEle, visioFigureDatar);
			return;
		}
		// 连接线是否有箭头
		boolean isCable = true;
		// 图形标识
		String figureType = figureMatching(nameUStr, isCable);
		flowStructureImage.setFigureType(figureType);
		// 获取 UIID 放入图形中
		flowStructureImage.setFigureId(Long.valueOf(visioFigureDatar
				.getFigureId()));
		// 获取Shape下的子节点
		List<Element> nodesList = JecnVisioXmlUtil.getChildNodesList(shapeEle);
		// 存储样式的List
		List<JecnVisioStyleDateBean> visioStyleDateList = new ArrayList<JecnVisioStyleDateBean>();
		// 样式节点 ix 属性
		String charIx = "";
		// text 节点
		Element textNodeEle = null;
		// 循环所有的子节点
		for (Element nodeEle : nodesList) {
			// 找到XForm节点
			if ("XForm".equalsIgnoreCase(nodeEle.getQualifiedName())) {
				shapeXFormNodeHandle(nodeEle, flowStructureImage);
			} else if ("Line".equalsIgnoreCase(nodeEle.getQualifiedName())) {
				isCable = shapeLineNodeHandle(nodeEle, flowStructureImage);
			} else if ("Fill".equalsIgnoreCase(nodeEle.getQualifiedName())) {
				shapeFillNodeHandle(nodeEle, flowStructureImage);
			} else if ("Char".equalsIgnoreCase(nodeEle.getQualifiedName())) {
				JecnVisioStyleDateBean visioStyleDate = new JecnVisioStyleDateBean();
				shapeCharNodeHandle(nodeEle, visioStyleDate);
				charIx = JecnVisioXmlUtil.getNodeAttValue(nodeEle, "IX");
				visioStyleDate.setIx(charIx);
				visioStyleDateList.add(visioStyleDate);
			} else if ("Text".equalsIgnoreCase(nodeEle.getQualifiedName())) {
				textNodeEle = nodeEle;
			} else if ("XForm1D".equalsIgnoreCase(nodeEle.getQualifiedName())) { // 连接线
				String lineType = figureMatching("Dynamic connector", isCable);
				flowStructureImage.setFigureType(lineType);
				JecnConnectingLineBean connectingLineBean = shapeXForm1DNodeHandle(nodeEle);
				// 开始图形
				String beginXStr = connectingLineBean.getBeginXStr();
				boolean isContinue = selectFigureId(flowStructureImage,
						beginXStr, true);
				if (!isContinue) {
					continue;
				}
				// 结束图形
				String endXStr = connectingLineBean.getEndXStr();
				isContinue = selectFigureId(flowStructureImage, endXStr, false);
				if (!isContinue) {
					continue;
				}
			}
			// 处理文字节点
			boolean isFlag = handleTextNode(flowStructureImage,
					visioStyleDateList, textNodeEle);
			if (!isFlag && !DrawCommon.isNullOrEmtry(master)) {
				for (JecnMasterDataBean masterDataBean : masterDataList) {
					if (master.equals(masterDataBean.getMasterId())) {
						handleFontNode(flowStructureImage, masterDataBean
								.getVisioStyleDateBean());
					}
				}
			}
		}
	}

	/**
	 * 处理文字的节点
	 * 
	 * @author fuzhh 2013-7-16
	 * @param visioStyleDateList
	 *            节点下 样式的集合
	 * @param textNodeEle
	 *            文件节点
	 */
	public boolean handleTextNode(JecnFlowStructureImage flowStructureImage,
			List<JecnVisioStyleDateBean> visioStyleDateList, Element textNodeEle) {
		boolean isFlag = false;
		if (textNodeEle != null) {
			// 获取样式标识
			String textIx = shapeTextNodeHandle(textNodeEle);
			// 获取内容
			String textVal = JecnVisioXmlUtil.getNodeValue(textNodeEle);
			// System.out.println("textIx:" + textIx+"-------textVal:"+textVal);
			// 存储内容
			flowStructureImage.setFigureText(textVal.replaceAll("\n", ""));
			// 循环样式
			for (JecnVisioStyleDateBean visioStyleDate : visioStyleDateList) {
				if (textIx.equals(visioStyleDate.getIx())) {
					handleFontNode(flowStructureImage, visioStyleDate);
					isFlag = true;
				}
			}
		}
		return isFlag;
	}

	/**
	 * 设置字体样式
	 * 
	 * @author fuzhh 2013-7-16
	 * @param flowStructureImage
	 * @param visioStyleDate
	 */
	public void handleFontNode(JecnFlowStructureImage flowStructureImage,
			JecnVisioStyleDateBean visioStyleDate) {
		if (visioStyleDate != null && visioStyleDate.getColor() != null) {
			// 颜色
			flowStructureImage.setFontColor(visioStyleDate.getColor());
		} else {
			flowStructureImage.setFontColor("0,0,0");
		}

		// 是否为黑体
		if (visioStyleDate != null && "1".equals(visioStyleDate.getStyle())) {
			flowStructureImage.setFontBody(1L);
		} else {
			flowStructureImage.setFontBody(0L);
		}
		String fontType = "";
		if (visioStyleDate != null && visioStyleDate.getAsianFont() != null) {
			// 字体类型
			fontType = fontMap.get(visioStyleDate.getAsianFont());
		} else {
			fontType = "宋体";
		}
		flowStructureImage.setFontType(fontType);

		// 字体大小
		if (visioStyleDate != null && visioStyleDate.getSize() != null) {
			double sizeDou = Double.valueOf(visioStyleDate.getSize()) * 100;
			flowStructureImage.setFontSize((long) sizeDou);
		} else {
			flowStructureImage.setFontSize(12L);
		}
	}

	/**
	 * 设置连接线数据
	 * 
	 * @author fuzhh 2013-7-15
	 * @param flowStructureImage
	 * @param visioLine
	 *            visio 图形连接线
	 * @param isStartEnd
	 *            true 开始点, false 结束点
	 * @return
	 */
	public boolean selectFigureId(JecnFlowStructureImage flowStructureImage,
			String visioLine, boolean isStartEnd) {
		String figre = null;
		String figureId = null;
		String location = "";
		if (visioLine != null) {
			String[] figreStr = visioLine.split(",");
			if (figreStr != null && figreStr.length > 0) {
				String figureOne = figreStr[0];
				String[] figureTwo = figureOne.split("!");
				if (figureTwo.length >= 2) {
					String startFigureName = figureTwo[0];
					figre = startFigureName;

					String startLocationName = figureTwo[1];
					String[] startLoc = startLocationName.split("\\.");
					if (startLoc.length == 2) {
						location = startLoc[1];
					} else {
						location = "1";
					}
				} else {
					figre = figureOne;
					location = "1";
				}
			} else {
				figre = visioLine;
				location = "1";
			}
		} else {
			return false;
		}
		if (figre == null) {
			return false;
		}
		String figureType = null;
		Long figureIdVal = null;
		for (JecnVisioFigureData visioFigureDataBean : figureList) {
			if (figre != null
					&& figre.equalsIgnoreCase(visioFigureDataBean
							.getFugereFlag())) {
				figureIdVal = Long.valueOf(visioFigureDataBean.getFigureId());
				figureType = visioFigureDataBean.getFugureType();
				break;
			}
		}
		if (figureIdVal == null) {
			String[] nameId = figre.split("\\.");
			if (nameId.length == 2) {
				figureId = nameId[1];
				for (JecnVisioFigureData visioFigureDataBean : figureList) {
					if (figureId != null
							&& figureId.equals(visioFigureDataBean
									.getFigureId())) {
						figureIdVal = Long.valueOf(visioFigureDataBean
								.getFigureId());
						figureType = visioFigureDataBean.getFugureType();
						break;
					}
				}
			} else {
				return false;
			}
		}
		if (figureIdVal == null) {
			return false;
		}
		if (isStartEnd) {
			flowStructureImage.setStartFigure(figureIdVal);
			if ("Decision".equalsIgnoreCase(figureType)) {
				flowStructureImage.setPoLongx(getDecisionLocation(location));
			} else if ("Terminator".equalsIgnoreCase(figureType)) {
				flowStructureImage.setPoLongx(getTerminatorLocation(location));
			} else if ("External Data".equalsIgnoreCase(figureType)
					|| "Database".equalsIgnoreCase(figureType)) {
				flowStructureImage.setPoLongx(getDatabaseLocation(location));
			} else {
				flowStructureImage.setPoLongx(getLocation(location));
			}

		} else {
			flowStructureImage.setEndFigure(figureIdVal);
			if ("Decision".equalsIgnoreCase(figureType)) {
				flowStructureImage.setPoLongy(getDecisionLocation(location));
			} else if ("Terminator".equalsIgnoreCase(figureType)) {
				flowStructureImage.setPoLongy(getTerminatorLocation(location));
			} else if ("External Data".equalsIgnoreCase(figureType)
					|| "Database".equalsIgnoreCase(figureType)) {
				flowStructureImage.setPoLongx(getDatabaseLocation(location));
			} else {
				flowStructureImage.setPoLongy(getLocation(location));
			}
		}
		return true;
	}

	/**
	 * visio中的图形 与 系统中的图形 匹配
	 * 
	 * @author fuzhh 2013-6-28
	 * @param nameStr
	 *            visio 中图形标识
	 */
	public String figureMatching(String nameStr, boolean isCable) {
		if (JecnVisioModeXmlData.visioMap.size() <= 0 || nameStr == null) {
			return null;
		}
		// 系统标识
		String figureType = null;
		// 连接线 单独处理
		if ("Dynamic connector".equalsIgnoreCase(nameStr)) {// 动态连接线
			if (isCable) {
				// 连接线 (带箭头)
				figureType = "ManhattanLine";
			} else {
				// 连接线 (不带箭头)
				figureType = "CommonLine";
			}
		}
		// 获取map key值
		Set<String> keySet = JecnVisioModeXmlData.visioMap.keySet();
		// 循环key值
		for (String figureKey : keySet) {// 遍历key
			// 获取key 对应的value
			String figureVal = JecnVisioModeXmlData.visioMap.get(figureKey);
			if (figureVal != null) {
				String[] visioFigureLen = figureVal.split(",");
				for (String visioFigureVal : visioFigureLen) {
					if (nameStr.equalsIgnoreCase(visioFigureVal)) {
						figureType = figureKey;
					}
				}
			}
		}
		return figureType;
	}

	/**
	 * 处理形状下的XForm节点
	 * 
	 * @author fuzhh 2013-6-9
	 * @param nodeEle
	 *            形状节点
	 */
	public void shapeXFormNodeHandle(Element nodeEle,
			JecnFlowStructureImage flowStructureImage) {
		// 获取XForm下所有的子节点
		List<Element> xFormList = JecnVisioXmlUtil.getChildNodesList(nodeEle);
		// // 是否水平翻转
		// String flipX = "";
		// // 是否垂直翻转
		// String flipY = "";
		for (Element xformEle : xFormList) {
			// 获取节点文字
			String nodeValue = JecnVisioXmlUtil.getNodeValue(xformEle);
			if (nodeValue == null) {
				continue;
			}
			if ("Width".equals(xformEle.getName())) {
				// 形状在绘图单位的宽度
				String width = nodeValue;
				double widthDou = Double.valueOf(width) * 100;
				flowStructureImage.setWidth((long) widthDou);
			} else if ("Height".equals(xformEle.getName())) {
				// 形状在绘图单位的高度
				String height = nodeValue;
				double heightDou = Double.valueOf(height) * 100;
				flowStructureImage.setHeight((long) heightDou);
			} else if ("PinX".equals(xformEle.getName())) {
				// 图形的x坐标
				String pinX = nodeValue;
				double locPinXDou = Double.valueOf(pinX) * 100;
				flowStructureImage.setPoLongx((long) locPinXDou);
			} else if ("PinY".equals(xformEle.getName())) {
				// 图形的y坐标
				String pinY = nodeValue;
				double locPinYDou = Double.valueOf(pinY) * 100;
				flowStructureImage.setPoLongy((long) locPinYDou);
			} else if ("Angle".equals(xformEle.getName())) {
				// 代表其父形状的当前旋转的角度
				// String angle = nodeValue;
				// double angleInt = Double.valueOf(angle);
				// if (30 < angleInt && angleInt < 120) {
				// angleInt = 90;
				// } else if (120 < angleInt && angleInt < 210) {
				// angleInt = 180;
				// } else if (210 < angleInt && angleInt < 300) {
				// angleInt = 270;
				// } else {
				// angleInt = 0;
				// }
				// flowStructureImage.setCircumgyrate(String.valueOf(angleInt));
				flowStructureImage.setCircumgyrate("0");
			}

			// else if ("FlipX".equals(xformEle.getName())) {
			// // 指示的形状是否已经水平翻转。(0:形状并没有被水平翻转 1形状已经水平翻转)
			// flipX = nodeValue;
			// } else if ("FlipY".equals(xformEle.getName())) {
			// // 指示的形状是否已垂直翻转。(0:形状并没有被垂直翻转 1形状已垂直翻转)
			// flipY = nodeValue;
			// }
		}
		// String currentAngle = "";
	}

	/**
	 * 获取形状下的线
	 * 
	 * @author fuzhh 2013-6-9
	 * @param nodeEle
	 *            形状节点
	 */
	public boolean shapeLineNodeHandle(Element nodeEle,
			JecnFlowStructureImage flowStructureImage) {
		boolean isCable = true;
		// 获取Line下所有的子节点
		List<Element> lineList = JecnVisioXmlUtil.getChildNodesList(nodeEle);
		for (Element lineEle : lineList) {
			// 获取节点文字
			String nodeValue = JecnVisioXmlUtil.getNodeValue(lineEle);
			if (nodeValue == null) {
				continue;
			}
			if ("LineWeight".equals(lineEle.getName())) {
				// 指定形状的线条粗细 度量单位默认为DT 最小0 最大1
				// String lineWeight = nodeValue;
				// double lineWeightDou = Double.valueOf(lineWeight) * 1000;
				// flowStructureImage.setLineThickness((int) lineWeightDou);
				flowStructureImage.setLineThickness(1);
			} else if ("LineColor".equals(lineEle.getName())) {
				// 指定形状的线条颜色
				String lineColor = nodeValue;
				Color color = parseToColor(lineColor.replaceAll("#", ""));
				String colorStr = shiftColor(color);
				flowStructureImage.setBodyColor(colorStr);
			} else if ("LinePattern".equals(lineEle.getName())) {
				// 指定的形状的线模式 0:没有线的模式 1:连接线
				// 2-23:图案对应的索引表Microsoft Office Visio中的线条图案
				// USE（strval:一个自定义的线条图案，，其中strval是模式使用的名称
				String linePattern = nodeValue;
				if ("0".equals(linePattern) || "1".equals(linePattern)) {
					flowStructureImage.setBodyLine("0");
				} else {
					flowStructureImage.setBodyLine("1");
				}
			} else if ("EndArrow".equals(lineEle.getName())) {
				// 指示线是否有一个箭头或其他线端格式 结束点 样式同上(0：无箭头 其他为有箭头)
				String endArrow = nodeValue;
				if ("0".equals(endArrow)) {
					isCable = false;
				}
			}
		}
		return isCable;
	}

	/**
	 * 获取形状下的fill节点
	 * 
	 * @author fuzhh 2013-6-9
	 * @param nodeEle
	 *            形状节点
	 */
	public void shapeFillNodeHandle(Element nodeEle,
			JecnFlowStructureImage flowStructureImage) {
		// 获取Fill下所有的子节点
		List<Element> fillList = JecnVisioXmlUtil.getChildNodesList(nodeEle);
		String fileColorStr = "";
		// 第一种颜色
		String oneColorStr = "";
		// 第二种颜色
		String twoColorStr = "";
		// 渐变方向
		String fillColorChangType = "";
		for (Element fillEle : fillList) {
			// 获取节点文字
			String nodeValue = JecnVisioXmlUtil.getNodeValue(fillEle);
			if (nodeValue == null) {
				continue;
			}
			if ("FillForegnd".equals(fillEle.getName())) {
				// 指定颜色用于前台的形状的填充模式。
				String fillForegnd = nodeValue;
				Color fillForegndColor = parseToColor(fillForegnd.replaceAll(
						"#", ""));
				twoColorStr = shiftColor(fillForegndColor);
			} else if ("FillBkgnd".equals(fillEle.getName())) {
				// 指定颜色用于背景的形状的填充模式。
				String fillBkgnd = nodeValue;
				Color fillBkgndColor = parseToColor(fillBkgnd.replaceAll("#",
						""));
				oneColorStr = shiftColor(fillBkgndColor);
			} else if ("FillPattern".equals(fillEle.getName())) {
				// 指定的填充图案的形状。
				String fillPattern = nodeValue;
				if ("28".equals(fillPattern) || "29".equals(fillPattern)
						|| "31".equals(fillPattern)) {
					fillColorChangType = "1";
				} else if ("30".equals(fillPattern) || "33".equals(fillPattern)
						|| "34".equals(fillPattern) || "38".equals(fillPattern)) {
					fillColorChangType = "2";
				} else if ("26".equals(fillPattern) || "35".equals(fillPattern)
						|| "40".equals(fillPattern)) {
					fillColorChangType = "3";
				} else {
					fillColorChangType = "4";
				}
			} else if ("ShdwBkgnd".equals(fillEle.getName())) {
				// 阴影形状的填充图案的背景（补）使用指定的颜色
				String shdwBkgnd = nodeValue;
				Color shdwBkgndColor = parseToColor(shdwBkgnd.replaceAll("#",
						""));
				String colorStr = shiftColor(shdwBkgndColor);
				flowStructureImage.setShadowColor(colorStr);
			} else if ("ShdwPattern".equals(fillEle.getName())) {
				// 指定的填充图案形状的影子 0:无（透明填充） 1:固体前景色 2-40:什锦模式
				String shdwPattern = nodeValue;
				if ("0".equals(shdwPattern) || "1".equals(shdwPattern)) {
					flowStructureImage.setHavaShadow(0L);
				} else {
					flowStructureImage.setHavaShadow(1L);
				}
			}
		}
		if (!"".equals(twoColorStr) && twoColorStr != null) {
			fileColorStr = oneColorStr + "," + twoColorStr + ","
					+ fillColorChangType;
		} else {
			fileColorStr = oneColorStr;
		}
		if (!DrawCommon.isNullOrEmtry(fileColorStr)) {
			flowStructureImage.setBGColor(fileColorStr);
		}
	}

	/**
	 * 获取形状下的Char节点
	 * 
	 * @author fuzhh 2013-6-9
	 * @param nodeEle
	 *            形状节点
	 */
	public void shapeCharNodeHandle(Element nodeEle,
			JecnVisioStyleDateBean visioStyleDate) {
		// 获取Char下所有的子节点
		List<Element> charList = JecnVisioXmlUtil.getChildNodesList(nodeEle);
		for (Element charEle : charList) {
			// 获取节点文字
			String nodeValue = JecnVisioXmlUtil.getNodeValue(charEle);
			if (nodeValue == null) {
				continue;
			}
			if ("Font".equals(charEle.getName())) {
				// 指定的ID号来格式化文本所使用的字体。
				String font = nodeValue;
				visioStyleDate.setFontId(Long.valueOf(font));
			} else if ("Color".equals(charEle.getName())) {
				// 中包含的一个字符的元素时， 颜色元素指定下列操作之一
				String color = nodeValue;
				Color fColor = parseToColor(color.replaceAll("#", ""));
				String colorStr = shiftColor(fColor);
				visioStyleDate.setColor(colorStr);
			} else if ("Style".equals(charEle.getName())) {
				// 指定字符格式应用于一系列文本的文本块的形状 1:黑体 2:斜体 3:下划线 4:小写
				String style = nodeValue;
				visioStyleDate.setStyle(style);
			} else if ("Case".equals(charEle.getName())) {
				// 确定的情况下，一个形状的文本 所有资本字母（大写）（1）和首字母大写（2）不改变文本的外观全部用大写字母输入。
				// 这些选项中的小写字母必须输入文字显示效果
				// 0:正常情况 1:所有字母大写 2:仅首字母大写
				String caseStr = nodeValue;
				visioStyleDate.setCaseStr(caseStr);
			} else if ("Pos".equals(charEle.getName())) {
				// 指定形状的文本相对于基线的位置 0:正常位置 1:上标 2:下标
				String pos = nodeValue;
				visioStyleDate.setPos(pos);
			} else if ("FontScale".equals(charEle.getName())) {
				// 指定的字体宽度
				String fontScale = nodeValue;
				visioStyleDate.setFontScale(fontScale);
			} else if ("Size".equals(charEle.getName())) {
				// 指定字体的大小
				String size = nodeValue;
				visioStyleDate.setSize(size);
			} else if ("DblUnderline".equals(charEle.getName())) {
				// 指定的文本范围是否它下面有双下划线 0:没有 1：有
				String dblUnderline = nodeValue;
				visioStyleDate.setDblUnderline(dblUnderline);
			} else if ("Overline".equals(charEle.getName())) {
				// 指定文本是否有它上面的一条线 0:文本没有它上面的一条线 1:它上面有一行文字
				String overline = nodeValue;
				visioStyleDate.setOverline(overline);
			} else if ("Strikethru".equals(charEle.getName())) {
				// 指定文本是否格式化为带删除线 0:文本没有被格式化为带删除线 1:文本格式化为带删除线
				String strikethru = nodeValue;
				visioStyleDate.setStrikethru(strikethru);
			} else if ("DoubleStrikethrough".equals(charEle.getName())) {
				// 确定文本是否被格式化为双删除线 0:不要双删除线文本格式设置为（默认） 1:双删除线格式文本
				String doubleStrikethrough = nodeValue;
				visioStyleDate.setDoubleStrikethrough(doubleStrikethrough);
			} else if ("RTLText".equals(charEle.getName())) {
				// 确定是否运行当前字符是从左到右或从右到左的文字方向 0:文字方向是从左右侧（默认） 1:文字方向是从右到左
				String rTLText = nodeValue;
				visioStyleDate.setrTLText(rTLText);
			} else if ("UseVertical".equals(charEle.getName())) {
				// 确定字符是否运行垂直或水平 0:文字方向是水平的（默认） 1:文字方向是垂直的
				String useVertical = nodeValue;
				visioStyleDate.setUseVertical(useVertical);
			} else if ("Letterspace".equals(charEle.getName())) {
				// 指定两个或多个字符之间的空间量。 空间可以添加或减去点增量的二十分之一。
				String letterspace = nodeValue;
				visioStyleDate.setLetterspace(letterspace);
			} else if ("ColorTrans".equals(charEle.getName())) {
				// 确定层或形状的文本颜色的透明度，从0（完全不透明）到1（完全透明）
				String colorTrans = nodeValue;
				visioStyleDate.setColorTrans(colorTrans);
			} else if ("AsianFont".equals(charEle.getName())) {
				// 指定的ID号，用来格式化文本包含亚洲字符的字体。
				String asianFont = nodeValue;
				visioStyleDate.setAsianFont(asianFont);
			}
		}
	}

	/**
	 * 处理形状下的XForm1D节点
	 * 
	 * @author fuzhh 2013-6-9
	 * @param nodeEle
	 *            形状节点
	 */
	public JecnConnectingLineBean shapeXForm1DNodeHandle(Element nodeEle) {
		// 线段bean
		JecnConnectingLineBean connectingLineBean = new JecnConnectingLineBean();
		// 获取XForm1D下所有的子节点
		List<Element> xForm1DList = JecnVisioXmlUtil.getChildNodesList(nodeEle);
		for (Element xForm1DEle : xForm1DList) {
			// 获取节点文字
			String nodeValue = JecnVisioXmlUtil.getNodeValue(xForm1DEle);
			if ("BeginX".equals(xForm1DEle.getName())) {
				String beginX = nodeValue;
				String beginXVal = JecnVisioXmlUtil.getNodeAttValue(xForm1DEle,
						"F");
				String beginXStr = remove(beginXVal);
				connectingLineBean.setBeginX(beginX);
				connectingLineBean.setBeginXStr(beginXStr);
			} else if ("BeginY".equals(xForm1DEle.getName())) {
				String beginY = nodeValue;
				String beginYVal = JecnVisioXmlUtil.getNodeAttValue(xForm1DEle,
						"F");
				String beginYStr = remove(beginYVal);
				connectingLineBean.setBeginY(beginY);
				connectingLineBean.setBeginYStr(beginYStr);
			} else if ("EndX".equals(xForm1DEle.getName())) {
				String endX = nodeValue;
				String endXVal = JecnVisioXmlUtil.getNodeAttValue(xForm1DEle,
						"F");
				String endXStr = remove(endXVal);
				connectingLineBean.setEndX(endX);
				connectingLineBean.setEndXStr(endXStr);
			} else if ("EndY".equals(xForm1DEle.getName())) {
				String endY = nodeValue;
				String endYVal = JecnVisioXmlUtil.getNodeAttValue(xForm1DEle,
						"F");
				String endYStr = remove(endYVal);
				connectingLineBean.setEndY(endY);
				connectingLineBean.setEndYStr(endYStr);
			}
		}
		return connectingLineBean;
	}

	/**
	 * 处理形状下的Text节点
	 * 
	 * @author fuzhh 2013-6-9
	 * @param nodeEle
	 *            形状节点
	 */
	public String shapeTextNodeHandle(Element nodeEle) {
		// 获取Text下所有的子节点
		List<Element> textDList = JecnVisioXmlUtil.getChildNodesList(nodeEle);
		String cp = "";
		for (Element textEle : textDList) {
			if ("cp".equals(textEle.getName())) {
				String nodeValue = JecnVisioXmlUtil.getNodeAttValue(textEle,
						"IX");
				cp = nodeValue;
			}
		}
		return cp;
	}

	/**
	 * 获取图形的位置点转换为系统数据
	 * 
	 * @author fuzhh 2013-7-4
	 * @param location
	 * @return elemType 线的枚举类型 1左 2上 3右 4下
	 */
	public Long getLocation(String location) {
		String endLocation = null;
		if ("X2".equals(location) || "Y2".equals(location)) {
			endLocation = "3";
		} else if ("X3".equals(location) || "Y3".equals(location)) {
			endLocation = "4";
		} else if ("X4".equals(location) || "Y4".equals(location)) {
			endLocation = "2";
		} else {
			endLocation = "1";
		}
		return Long.valueOf(endLocation);
	}

	/**
	 * 判定 位置点 换算
	 * 
	 * @author fuzhh 2013-7-16
	 * @param location
	 * @return elemType 线的枚举类型 1左 2上 3右 4下
	 */
	public Long getDecisionLocation(String location) {
		String endLocation = null;
		if ("X2".equals(location) || "Y2".equals(location)) {
			endLocation = "3";
		} else if ("X3".equals(location) || "Y3".equals(location)) {
			endLocation = "2";
		} else if ("X4".equals(location) || "Y4".equals(location)) {
			endLocation = "4";
		} else {
			endLocation = "1";
		}
		return Long.valueOf(endLocation);
	}

	/**
	 * 终结符 位置点 换算
	 * 
	 * @author fuzhh 2013-7-16
	 * @param location
	 * @return elemType 线的枚举类型 1左 2上 3右 4下
	 */
	public Long getTerminatorLocation(String location) {
		String endLocation = null;
		if ("X2".equals(location) || "Y2".equals(location)) {
			endLocation = "2";
		} else if ("X3".equals(location) || "Y3".equals(location)) {
			endLocation = "1";
		} else if ("X4".equals(location) || "Y4".equals(location)) {
			endLocation = "3";
		} else {
			endLocation = "4";
		}
		return Long.valueOf(endLocation);
	}

	/**
	 * 信息系统 位置点 换算
	 * 
	 * @author fuzhh 2013-7-16
	 * @param location
	 * @return elemType 线的枚举类型 1左 2上 3右 4下
	 */
	public Long getDatabaseLocation(String location) {
		String endLocation = null;
		if ("X2".equals(location) || "Y2".equals(location)) {
			endLocation = "2";
		} else if ("X3".equals(location) || "Y3".equals(location)) {
			endLocation = "4";
		} else if ("X4".equals(location) || "Y4".equals(location)) {
			endLocation = "3";
		} else {
			endLocation = "1";
		}
		return Long.valueOf(endLocation);
	}

	/**
	 * @param s
	 *            要操作的字符串
	 * @return
	 */
	public String remove(String str) {
		if (str == null) {
			return str;
		}
		int i = str.lastIndexOf("(");
		str = str.substring(i + 1, str.length());
		int j = str.indexOf(")");
		str = str.substring(0, j);
		// 是否存在 引号
		if (str.contains("\'")) {
			str = str.replaceAll("\'", "");
		}
		return str;
	}

	/**
	 * @param c
	 *            例如 ff0000 六位的十六进制字符串表示红色
	 * @return java.awt.Color 对象的实例
	 * @exception NumberFormatException
	 */
	public Color parseToColor(String c) {
		return new Color(Integer.valueOf(c, 16));
	}

	public String shiftColor(Color color) {
		String colorStr = color.getRed() + "," + color.getGreen() + ","
				+ color.getBlue();
		return colorStr;
	}
}
