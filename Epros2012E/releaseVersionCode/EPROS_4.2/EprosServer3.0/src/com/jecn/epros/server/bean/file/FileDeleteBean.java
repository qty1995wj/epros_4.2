package com.jecn.epros.server.bean.file;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangchen May 30, 2012
 * @description：删除file的bean
 */
public class FileDeleteBean implements Serializable{
	private long fileId;
	private String fileName;
	private Date createTime;
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
