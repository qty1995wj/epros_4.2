package com.jecn.epros.server.webBean.rule;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;

public class RuleFileWebBean implements Serializable{
	
	/**文件bean*/
	private FileWebBean fileCommonBean;
	
	/**相关流程*/
	private List<ProcessWebBean> listProcessBean;
	
	/**制度文件基本信息**/
	private RuleInfoBean ruleInfoBean;

	public FileWebBean getFileCommonBean() {
		return fileCommonBean;
	}

	public void setFileCommonBean(FileWebBean fileCommonBean) {
		this.fileCommonBean = fileCommonBean;
	}

	public List<ProcessWebBean> getListProcessBean() {
		return listProcessBean;
	}

	public void setListProcessBean(List<ProcessWebBean> listProcessBean) {
		this.listProcessBean = listProcessBean;
	}

	public RuleInfoBean getRuleInfoBean() {
		return ruleInfoBean;
	}

	public void setRuleInfoBean(RuleInfoBean ruleInfoBean) {
		this.ruleInfoBean = ruleInfoBean;
	}
	
}
