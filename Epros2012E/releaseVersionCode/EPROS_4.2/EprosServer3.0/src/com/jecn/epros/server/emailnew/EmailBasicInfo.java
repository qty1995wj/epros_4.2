package com.jecn.epros.server.emailnew;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;

public class EmailBasicInfo {

	/** 邮件主题 */
	private String subject;
	/** 邮件正文 */
	private String content;
	/** 收件人集合 */
	private List<JecnUser> recipients = new ArrayList<JecnUser>();

	/** 附件 */
	private List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<EmailAttachment> getAttachments() {
		return attachments;
	}

	public List<JecnUser> getRecipients() {
		return recipients;
	}

	public void addRecipients(JecnUser user) {
		this.recipients.add(user);
	}

	public void addRecipients(List<JecnUser> users) {
		this.recipients.addAll(users);
	}

	public void setAttachments(List<EmailAttachment> attachments) {
		this.attachments = attachments;
	}

}
