package com.jecn.epros.server.webBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.webBean.popedom.OrgBean;
import com.jecn.epros.server.webBean.popedom.PosBean;

/**
 * @author yxw 2012-11-26
 * @description：web端用户登录bean
 */
public class WebLoginBean implements Serializable {
	/** 登陆状态 0为用户密码不正确，1是登陆成功 */
	private int loginState;
	/** 人员基本对象 */
	private JecnUser jecnUser;
	/** 登录人部门ID集合和岗位ID集合 */
	private Set<Long> listPosIds;
	private Set<Long> listOrgIds;
	/** 部门父节点集合（含本身） */
	private Set<Long> listOrgPathPIds;

	/** 岗位、组织信息 */
	private List<PosBean> listPosBean;
	/** 组织 */
	private List<OrgBean> listOrgBean;

	/** 角色信息 */
	private List<JecnTreeBean> roleList;
	/** 是不是流程管理员 */
	private boolean isAdmin;
	/** 是不是浏览管理员 */
	private boolean isViewAdmin;
	/** 是不是下载管理员 */
	private boolean isDownLoadAdmin;
	/** 当前项目ID */
	private long projectId;
	/** 当前项目名称 */
	private String projectName;
	/** 未读消息数量 */
	private int messageNum;
	/** 1:显示项目名称，并可切换项目；0 不可切换项目 */
	private int showProject;
	/** 是不是二级管理员**/
	private boolean isSecondAdmin;

	public boolean isSecondAdmin() {
		return isSecondAdmin;
	}

	public void setSecondAdmin(boolean isSecondAdmin) {
		this.isSecondAdmin = isSecondAdmin;
	}

	public int getMessageNum() {
		return messageNum;
	}

	public void setMessageNum(int messageNum) {
		this.messageNum = messageNum;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public JecnUser getJecnUser() {
		return jecnUser;
	}

	public void setJecnUser(JecnUser jecnUser) {
		this.jecnUser = jecnUser;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public boolean getIsAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public boolean isViewAdmin() {
		return isViewAdmin;
	}

	public boolean getIsViewAdmin() {
		return isViewAdmin;
	}

	public void setViewAdmin(boolean isViewAdmin) {
		this.isViewAdmin = isViewAdmin;
	}

	public boolean isDownLoadAdmin() {
		return isDownLoadAdmin;
	}

	public boolean getIsDownLoadAdmin() {
		return isDownLoadAdmin;
	}

	public void setDownLoadAdmin(boolean isDownLoadAdmin) {
		this.isDownLoadAdmin = isDownLoadAdmin;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public int getLoginState() {
		return loginState;
	}

	public void setLoginState(int loginState) {
		this.loginState = loginState;
	}

	public List<JecnTreeBean> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<JecnTreeBean> roleList) {
		this.roleList = roleList;
	}

	public Set<Long> getListPosIds() {
		return listPosIds;
	}

	public void setListPosIds(Set<Long> listPosIds) {
		this.listPosIds = listPosIds;
	}

	public Set<Long> getListOrgIds() {
		return listOrgIds;
	}

	public void setListOrgIds(Set<Long> listOrgIds) {
		this.listOrgIds = listOrgIds;
	}

	public Set<Long> getListOrgPathPIds() {
		return listOrgPathPIds;
	}

	public void setListOrgPathPIds(Set<Long> listOrgPathPIds) {
		this.listOrgPathPIds = listOrgPathPIds;
	}

	public List<OrgBean> getListOrgBean() {
		return listOrgBean;
	}

	public void setListOrgBean(List<OrgBean> listOrgBean) {
		this.listOrgBean = listOrgBean;
	}

	public List<PosBean> getListPosBean() {
		return listPosBean;
	}

	public void setListPosBean(List<PosBean> listPosBean) {
		this.listPosBean = listPosBean;
		if (listPosBean != null && listPosBean.size() > 0) {
			Set<Long> listIds = new HashSet<Long>();
			listOrgBean = new ArrayList<OrgBean>();
			for (PosBean posBean : listPosBean) {
				if (!listIds.contains(posBean.getOrgId())) {
					listIds.add(posBean.getOrgId());
					OrgBean orgBean = new OrgBean();
					orgBean.setOrgId(posBean.getOrgId());
					orgBean.setOrgName(posBean.getOrgName());
					listOrgBean.add(orgBean);
				}
			}
		}
	}


	public void setShowProject(int showProject) {
		this.showProject = showProject;
	}

	public int getShowProject() {
		return showProject;
	}
}
