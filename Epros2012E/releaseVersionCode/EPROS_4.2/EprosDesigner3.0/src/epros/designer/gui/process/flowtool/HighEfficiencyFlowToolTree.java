package epros.designer.gui.process.flowtool;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class HighEfficiencyFlowToolTree extends JecnHighEfficiencyTree {
	private Logger log = Logger.getLogger(HighEfficiencyFlowToolTree.class);
	/** 所有支持工具 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	private FlowToolManageDialog  flowToolManageDialog = null;
	private Long flag = null;

	public HighEfficiencyFlowToolTree(Long flag) {
		this.flag = flag;
	}

	public HighEfficiencyFlowToolTree(Long flag,FlowToolManageDialog flowToolManageDialog){
		this.flag = flag;
		this.flowToolManageDialog = flowToolManageDialog;
	}
	
	public HighEfficiencyFlowToolTree() {
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new FlowToolTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFlowTool().getChildSustainTools(
					Long.valueOf(0));
		} catch (Exception e) {
			log.error("HighEfficiencyFlowToolTree getTreeModel is error", e);
		}
		return JecnFlowToolCommon.getFlowToolTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		if (flag != null && "0".equals(String.valueOf(flag))) {
			JecnFlowToolCommon.treeMousePressed(evt, this,flowToolManageDialog);
		}
	}

}
