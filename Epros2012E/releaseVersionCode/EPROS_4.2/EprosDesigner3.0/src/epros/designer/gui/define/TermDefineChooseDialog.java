package epros.designer.gui.define;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;

public class TermDefineChooseDialog extends JecnSelectChooseDialog {

	public TermDefineChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 40, jecnDialog);
		this.setSelectMutil(false);
		this.setTitle(JecnProperties.getValue("TremDefineSelect"));
	}

	public TermDefineChooseDialog(List<JecnTreeBean> list) {
		super(list, 40);
		this.setTitle(JecnProperties.getValue("TremDefineSelect"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		data.add(jecnTreeBean.getPname());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				if (jecnTreeBean.getId() == null || "".equals(jecnTreeBean.getName())) {
					continue;
				}
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public JecnTree getJecnTree() {
		return new JecnTermDefineTree();
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 术语名称
		return title;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getTermDefinitionAction().searchByName(name);
		} catch (Exception e) {
			log.error(JecnProperties.getValue("termDefineSearchDataError"), e);
			return new ArrayList<JecnTreeBean>();
		}
	}
	
	public void disableButton(){
		this.deleteBtn.setEnabled(false);
		this.remove(this.deleteBtn);
		this.emptyBtn.setEnabled(false);
		this.remove(this.emptyBtn);
	}

}
