package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnPropertySetJDialog;
/***
 * 属性配置按钮
 * @Time 2014-10-15
 *
 */
public class JecnPropertySetButton extends JecnAbstractBaseJButton {

	public JecnPropertySetButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		this.setEnabled(false);
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {
		if (dialog.getSelectedPropContPanel() == null) {
			return;
		}
		//属性配置按钮点击打开的显示框
		JecnPropertySetJDialog propertySetDialog = new JecnPropertySetJDialog(dialog);
		propertySetDialog.setTitle(this.getText());
		propertySetDialog.setVisible(true);
	}
}
