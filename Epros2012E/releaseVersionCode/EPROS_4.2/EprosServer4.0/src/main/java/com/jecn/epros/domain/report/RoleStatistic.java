package com.jecn.epros.domain.report;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色数统计
 *
 * @author user
 */
public class RoleStatistic extends ReportBaseBean {

    private List<Part> parts = new ArrayList<Part>();

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

}
