package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.system.UserWebSearchBean;
import com.jecn.epros.domain.system.ViewRoleDetailBean;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.security.JecnContants;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.ProcessSqlConstant;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.IDUtils;
import net.sf.jsqlparser.statement.insert.Insert;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SystemManageDynaSqlProvider extends BaseSqlProvider {

    public String insertUser(final JecnUser user) {
        return new SQL() {
            {
                INSERT_INTO("JECN_USER");

                if (user.getName() != null) {
                    VALUES("NAME", "#{name}");
                }

                if (user.getEmail() != null) {
                    VALUES("EMAIL", "#{email}");
                }
            }
        }.toString();
    }

    public String updateUser(final JecnUser user) {
        return new SQL() {
            {
                UPDATE("JECN_USER");

                if (user.getName() != null) {
                    SET("NAME = #{name}");
                }

                if (user.getEmail() != null) {
                    SET("EMAIL = #{email}");
                }
                WHERE("id = #{id}");
            }
        }.toString();
    }

    public String deleteUser(int userId) {
        return new SQL() {
            {
                DELETE_FROM("JECN_USER");
                WHERE("id = #{userId}");
            }
        }.toString();
    }

    public String selectUsersForManage(UserWebSearchBean userWebSearchBean) {
        return new SQL() {
            {
                SELECT("ju.people_id,ju.login_name,ju.true_name,jfoi.figure_id, jfoi.figure_text, jfo.org_id,jfo.org_name");
                FROM("jecn_user ju");
                LEFT_OUTER_JOIN("jecn_user_position_related jupr on jupr.people_id=ju.people_id");
                LEFT_OUTER_JOIN("jecn_flow_org_image jfoi on jupr.figure_id=jfoi.figure_id");
                LEFT_OUTER_JOIN("jecn_flow_org jfo on jfoi.org_id = jfo.org_id and jfo.del_state=0");
                if (StringUtils.isNotBlank(userWebSearchBean.getLoginName())) {
                    WHERE("upper(ju.login_name) like upper('%" + userWebSearchBean.getLoginName() + "%')");
                }
                if (StringUtils.isNotBlank(userWebSearchBean.getTrueName())) {
                    WHERE("upper(ju.true_name) like upper('%" + userWebSearchBean.getTrueName() + "%')");
                }
            }
        }.toString();
    }

    public String getUserCount(UserWebSearchBean userWebSearchBean) {
        String sql = " select count(distinct people_id) from (" + getUserSql(userWebSearchBean) + " )a";
        return sql;
    }

    private String getUserSql(UserWebSearchBean userWebSearchBean) {
        String sql = " select distinct ju.people_id,ju.login_name,ju.true_name from jecn_user ju";
        // 角色类型
        if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
            sql = sql + ",jecn_user_role jur,jecn_role_info jri";
        }
        // 部门
        if (userWebSearchBean.getOrgId() != -1) {
            sql = sql
                    + ",(select c.* from jecn_flow_org c,jecn_flow_org p"
                    + "	where c.t_path like " + SqlCommon.getJoinFunc("p") + " and p.org_id=" + userWebSearchBean.getOrgId() + ") org";
            sql = sql + " ,jecn_flow_org_image jfoi";
        }

        // 部门和岗位查询条件公用的表
        if (userWebSearchBean.getOrgId() != -1 || userWebSearchBean.getPosId() != -1) {
            sql += ",jecn_user_position_related jupr ";
        }

        //------------------拼接结果查询条件----------------------

        // 登陆名称
        sql = sql + " where ju.ISLOCK=0";
        if (StringUtils.isNotBlank(userWebSearchBean.getLoginName())) {
            sql = sql + " and " + SqlCommon.getUpperString("ju.login_name") + " like "
                    + SqlCommon.getUpperLikeString(userWebSearchBean.getLoginName());
        }
        // 真实名称 科大讯飞 真实姓名 根据登录名查询
        if (ConfigUtils.isIflytekLogin()) {
            if (userWebSearchBean.getIfLoginName() != null && !userWebSearchBean.getIfLoginName().isEmpty()) {
                sql = sql + " and ju.login_name in " + SqlCommon.getStrs(userWebSearchBean.getIfLoginName());
            }
        } else {
            // 真实名称
            if (StringUtils.isNotBlank(userWebSearchBean.getTrueName())) {
                sql = sql + " and " + SqlCommon.getUpperString("ju.true_name") + " like "
                        + SqlCommon.getUpperLikeString(userWebSearchBean.getTrueName());
            }
        }
        // 角色类型
        if (StringUtils.isNotBlank(userWebSearchBean.getRoleType())) {
            sql = sql + " and ju.people_id=jur.people_id and jur.role_id=jri.role_id and jri.filter_id='"
                    + userWebSearchBean.getRoleType() + "'";
        }
        // 部门
        if (userWebSearchBean.getOrgId() != -1) {
            sql = sql
                    + " and ju.people_id = jupr.people_id and jupr.figure_id=jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0 ";
        }

        // 岗位
        if (userWebSearchBean.getPosId() != -1) {
            sql = sql + " and jupr.people_id=ju.people_id and jupr.figure_id=" + userWebSearchBean.getPosId();
        }
        return sql;
    }

    public String fetchUsers(UserWebSearchBean userWebSearchBean) {
        return getUserSql(userWebSearchBean);
    }


    public String getTotalRole(Map<String, String> paramMap) {

        String roleName = paramMap.get("roleName");
        String sql = "select count(jri.role_id) from jecn_role_info jri where jri.is_dir=0";
        if (StringUtils.isNotBlank(roleName)) {
            sql = sql + " and jri.role_name like '%" + roleName + "%'";
        }

        return sql;
    }

    public String getUserRoleInfo(Map<String, Object> paramMap) {
        String orgName = paramMap.get("orgName") != null ? paramMap.get("orgName").toString() : "";
        String roleId = paramMap.get("roleId") != null ? paramMap.get("roleId").toString() : "";
        String roleName = paramMap.get("roleName") != null ? paramMap.get("roleName").toString() : "";
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT PEOPLE_ID FROM (SELECT DISTINCT JU.PEOPLE_ID " +
                "  FROM JECN_USER JU" +
                " INNER JOIN JECN_USER_ROLE UR" +
                "    ON JU.PEOPLE_ID = UR.PEOPLE_ID" +
                "      AND JU.ISLOCK = 0" +
                " INNER JOIN JECN_ROLE_INFO RI" +
                "    ON UR.ROLE_ID = RI.ROLE_ID" +
                "  LEFT JOIN JECN_USER_POSITION_RELATED JUP" +
                "    ON JU.PEOPLE_ID = JUP.PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFLO" +
                "    ON JUP.FIGURE_ID = JFLO.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG JFO" +
                "    ON JFLO.ORG_ID = JFO.ORG_ID" +
                "   AND JFO.DEL_STATE = 0" +
                "   AND JFO.PROJECTID = 1 ");
        sql.append(getRoleSql("WHERE(", "   AND RI.FILTER_ID = 'design'"));
        sql.append(getRoleSql("OR", "   AND RI.FILTER_ID ='admin'"));
        sql.append(getRoleSql("OR", "   AND RI.FILTER_ID ='secondAdmin'")).append(")");
        if (StringUtils.isNotBlank(orgName)) {
            sql.append("  AND JFO.ORG_NAME LIKE '%" + orgName + "%'");
        }
        if (StringUtils.isNotBlank(roleId)) {
            sql.append(getRoleSql("AND", "   AND RI.FILTER_ID = '" + roleId + "'"));
        }
        if (StringUtils.isNotBlank(roleName)) {
            sql.append(getRoleSql("AND", "   AND RI.ROLE_NAME LIKE '%" + roleName + "%'"));
        }
        sql.append(") roleTable");
        return sql.toString();
    }


    public String getTotalUserRoleInfo(Map<String, Object> paramMap) {
        String orgName = paramMap.get("orgName") != null ? paramMap.get("orgName").toString() : "";
        String roleId = paramMap.get("roleId") != null ? paramMap.get("roleId").toString() : "";
        String roleName = paramMap.get("roleName") != null ? paramMap.get("roleName").toString() : "";
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(DISTINCT JU.PEOPLE_ID) " +
                "  FROM JECN_USER JU" +
                " INNER JOIN JECN_USER_ROLE UR" +
                "    ON JU.PEOPLE_ID = UR.PEOPLE_ID" +
                "      AND JU.ISLOCK = 0" +
                " INNER JOIN JECN_ROLE_INFO RI" +
                "    ON UR.ROLE_ID = RI.ROLE_ID " +
                "  LEFT JOIN JECN_USER_POSITION_RELATED JUP" +
                "    ON JU.PEOPLE_ID = JUP.PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFLO" +
                "    ON JUP.FIGURE_ID = JFLO.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG JFO" +
                "    ON JFLO.ORG_ID = JFO.ORG_ID" +
                "   AND JFO.DEL_STATE = 0" +
                "   AND JFO.PROJECTID = 1 ");
        sql.append(getRoleSql("WHERE(", "   AND RI.FILTER_ID = 'design'"));
        sql.append(getRoleSql("OR", "   AND RI.FILTER_ID ='admin'"));
        sql.append(getRoleSql("OR", "   AND RI.FILTER_ID ='secondAdmin'")).append(")");
        if (StringUtils.isNotBlank(orgName)) {
            sql.append(" AND JFO.ORG_NAME LIKE '%" + orgName + "%'");
        }
        if (StringUtils.isNotBlank(roleId)) {
            sql.append(getRoleSql("AND", "   AND RI.FILTER_ID= '" + roleId + "'"));
        }
        if (StringUtils.isNotBlank(roleName)) {
            sql.append(getRoleSql("AND", "   AND RI.ROLE_NAME LIKE '%" + roleName + "%'"));
        }
        return sql.toString();
    }


    public String getTotalViewRole(Map<String, String> paramMap) {

        String roleName = paramMap.get("roleName");
        String sql = "select count(rp.id) from ROLE_PERMISSIONS rp WHERE 1=1";
        if (StringUtils.isNotBlank(roleName)) {
            sql = sql + " and rp.name like '%" + roleName + "%'";
        }

        return sql;
    }

    public String fetchUserRoles(Map<String, String> paramMap) {

        String roleName = paramMap.get("roleName");
        String sql = "SELECT JRI.ROLE_ID,JRI.ROLE_NAME,JRI.ROLE_REMARK FROM JECN_ROLE_INFO JRI WHERE JRI.IS_DIR=0";
        if (StringUtils.isNotBlank(roleName)) {
            sql = sql + " and jri.role_name like '%" + roleName + "%'";
        }
        return sql;
    }

    public String fetchUserRoleInfos(Map<String, Object> paramMap) {
        String orgName = paramMap.get("orgName") != null ? paramMap.get("orgName").toString() : "";
        String roleId = paramMap.get("roleId") != null ? paramMap.get("roleId").toString() : "";
        List<String> ids = paramMap.get("peopleIds") != null ? (List<String>) paramMap.get("peopleIds") : new ArrayList<>();
        String roleName = paramMap.get("roleName") != null ? paramMap.get("roleName").toString() : "";
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT JU.PEOPLE_ID," +
                "                JU.TRUE_NAME," +
                "                RI.ROLE_ID," +
                "                RI.ROLE_NAME," +
                "                RI.FILTER_ID," +
                "                JFO.ORG_ID," +
                "                JFO.ORG_NAME," +
                "                JF.FLOW_ID," +
                "                JF.FLOW_NAME," +
                "                JFC.FLOW_ID P_FLOW_ID," +
                "                JFC.FLOW_NAME P_FLOW_NAME," +
                "                JFT.FILE_ID," +
                "                JFT.FILE_NAME," +
                "                JFTC.FILE_ID P_FILE_ID," +
                "                JFTC.FILE_NAME P_FILE_NAME," +
                "                CL.CRITERION_CLASS_ID," +
                "                CL.CRITERION_CLASS_NAME," +
                "                CLC.CRITERION_CLASS_ID P_CRITERION_CLASS_ID," +
                "                CLC.CRITERION_CLASS_NAME P_CRITERION_CLASS_NAME," +
                "                RT.ID RULE_ID," +
                "                RT.RULE_NAME," +
                "                RTC.ID  P_RULE_ID," +
                "                RTC.RULE_NAME P_RULE_NAME," +
                "                RIS.ID RISK_ID," +
                "                RIS.NAME RISK_NAME," +
                "                RISC.ID  P_RISK_ID," +
                "                RISC.NAME  P_RISK_NAME," +
                "TERM.ID                     TERM_ID ," +
                "TERM.NAME                     TERM_NAME," +
                "TERMP.ID                      TP_ID," +
                "TERMP.NAME                    TP_NAME," +
                "                ORG.ORG_ID ORGR_ID," +
                "                ORG.ORG_NAME  ORGR_NAME," +
                "                ORGC.ORG_ID P_ORGR_ID," +
                "                ORGC.ORG_NAME  P_ORGR_NAME," +
                "                POS.ID POSG_ID," +
                "                POS.NAME POSG_NAME" +
                "  FROM JECN_USER JU" +
                " INNER JOIN JECN_USER_ROLE UR" +
                "    ON JU.PEOPLE_ID = UR.PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                " INNER JOIN JECN_ROLE_INFO RI" +
                "    ON UR.ROLE_ID = RI.ROLE_ID" +
                "  LEFT JOIN JECN_USER_POSITION_RELATED JUP" +
                "    ON JU.PEOPLE_ID = JUP.PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFLO" +
                "    ON JUP.FIGURE_ID = JFLO.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG JFO" +
                "    ON JFLO.ORG_ID = JFO.ORG_ID" +
                "   AND JFO.DEL_STATE = 0" +
                "   AND JFO.PROJECTID = 1" +
                "  LEFT JOIN JECN_ROLE_CONTENT RC" +
                "    ON RI.ROLE_ID = RC.ROLE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_T JF" +
                "    ON RC.RELATE_ID = JF.FLOW_ID" +
                "   AND RC.TYPE = 0" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_T JFC" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("JF", "JFC") +
                "    AND JFC.FLOW_ID <> JF.FLOW_ID" +
                "  LEFT JOIN JECN_FILE_T JFT" +
                "    ON RC.RELATE_ID = JFT.FILE_ID" +
                "   AND RC.TYPE = 1" +
                "  LEFT JOIN JECN_FILE_T JFTC" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("JFT", "JFTC") +
                "    AND JFTC.FILE_ID <> JFT.FILE_ID" +
                "  LEFT JOIN JECN_CRITERION_CLASSES CL" +
                "    ON RC.RELATE_ID = CL.Criterion_Class_Id" +
                "   AND RC.TYPE = 2" +
                "  LEFT JOIN JECN_CRITERION_CLASSES CLC" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("CL", "CLC") +
                "    AND CLC.Criterion_Class_Id <> CL.Criterion_Class_Id" +
                "  LEFT JOIN JECN_RULE_T RT" +
                "    ON RC.RELATE_ID = RT.ID" +
                "   AND RC.TYPE = 3" +
                "  LEFT JOIN JECN_RULE_T RTC" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("RT", "RTC") +
                "    AND RTC.ID <> RT.ID" +
                "  LEFT JOIN JECN_RISK RIS" +
                "    ON RC.RELATE_ID = RIS.ID" +
                "   AND RC.TYPE = 4" +
                "  LEFT JOIN JECN_RISK RISC" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("RIS", "RISC") +
                "    AND RISC.ID <> RIS.ID" +
                "   LEFT JOIN TERM_DEFINITION_LIBRARY TERM" +
                "  ON RC.RELATE_ID = TERM.ID" +
                " AND RC.TYPE = 8 " +
                " LEFT JOIN TERM_DEFINITION_LIBRARY TERMP" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("TERM", "TERMP") +
                " AND TERMP.ID <> TERM.ID " +
                "  LEFT JOIN JECN_FLOW_ORG ORG" +
                "    ON RC.RELATE_ID = ORG.ORG_ID" +
                "   AND RC.TYPE = 5" +
                "  LEFT JOIN JECN_FLOW_ORG ORGC" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("ORG", "ORGC") +
                "    AND ORGC.ORG_ID <> ORG.ORG_ID" +
                "  LEFT JOIN JECN_POSITION_GROUP POS" +
                "    ON RC.RELATE_ID = POS.ID" +
                "   AND RC.TYPE = 7 ");
        sql.append(getRoleSql("WHERE(", "   AND RI.FILTER_ID = 'design'"));
        sql.append(getRoleSql("OR", "   AND RI.FILTER_ID ='admin'"));
        sql.append(getRoleSql("OR", "   AND RI.FILTER_ID ='secondAdmin'")).append(")");
        if (!ids.isEmpty()) {
            sql.append(" AND JU.PEOPLE_ID IN " + SqlCommon.getStrs(ids));
        } else {
            if (StringUtils.isNotBlank(orgName)) {
                sql.append(" AND JFO.ORG_NAME LIKE '%" + orgName + "%'");
            }
            if (StringUtils.isNotBlank(roleId)) {
                sql.append(getRoleSql("AND", "   AND RI.FILTER_ID = '" + roleId + "'"));
            }
            if (StringUtils.isNotBlank(roleName)) {
                sql.append(getRoleSql("AND", "   AND RI.ROLE_NAME LIKE '%" + roleName + "%'"));
            }
        }
        sql.append("ORDER BY JU.PEOPLE_ID");
        return sql.toString();
    }

    private String getRoleSql(String conditionType, String sql) {
        String sqls = conditionType + " EXISTS (SELECT 1" +
                "       FROM JECN_USER JUU" +
                "      INNER JOIN JECN_USER_ROLE UR" +
                "         ON JUU.PEOPLE_ID = UR.PEOPLE_ID" +
                "        AND JUU.ISLOCK = 0" +
                "      INNER JOIN JECN_ROLE_INFO RI" +
                "         ON UR.ROLE_ID = RI.ROLE_ID" +
                "      WHERE 1 = 1";
        sqls += sql;
        sqls += "  AND JUU.PEOPLE_ID = JU.PEOPLE_ID)";
        return sqls;
    }

    public String fetchViewRoles(Map<String, String> paramMap) {

        String roleName = paramMap.get("roleName");
        String sql = "SELECT RP.ID,RP.NAME,RP.TYPE ROLETYPE,RP.UPDATE_TIME " +
                " FROM ROLE_PERMISSIONS rp WHERE 1=1";
        if (StringUtils.isNotBlank(roleName)) {
            sql = sql + " and rp.name like '%" + roleName + "%'";
        }
        return sql;
    }

    public String viewRoleAuthList(String roleId) {
        String length = SqlCommon.getLengthFunc();
        String sql = "SELECT PM.ID,PM.NAME AS TEXT,PM.EN_NAME ENTEXT," +
                " CASE  WHEN PID ='NULL' THEN 'false' ELSE 'true' END AS ISLEAF,'auth' AS TYPE ,0 AS ISDIR," +
                " CASE WHEN RPM.ROLE_ID IS NULL THEN 'false' ELSE 'true' END AS ISCHECKED," +
                " 'true' AS SELECTED,PM.PID AS PID, " +
                " CASE WHEN (PID <> 'NULL') THEN 'true' ELSE 'false' END AS LEAF,PM.SORTID " +
                " FROM PERMISSIONS_MENU PM LEFT JOIN ROLE_PERMISSIONS_MENU RPM " +
                " ON PM.ID = RPM.MENU_ID AND RPM.ROLE_ID = '" + roleId + "' ORDER BY PM.SORTID ";
        return sql;
    }

//    public String viewRoleAuthList(Long roleId) {
//        String len = SqlCommon.getLengthFunc();
//        String sql = "SELECT pm.id,pm.name as text," +
//                " case " + len + "(pm.sortid) when 2 then 'false' else 'true' end as isleaf,'auth' as type ,0 as isDir," +
//                " case when rpm.ROLE_ID IS NULL then 'false' else 'true' end as isChecked," +
//                " 'true' as selected,pm.pid as pid, " +
//                " case when (" + len + "(pm.sortid)>2) then 'true' else 'false' end as leaf,pm.sortid " +
//                " from PERMISSIONS_MENU pm left join ROLE_PERMISSIONS_MENU rpm " +
//                " on pm.ID = rpm.MENU_ID and rpm.ROLE_ID = " + roleId + " order by CAST(pm.ID as int)";
//        return sql;
//    }


    public String getProcessCheckCount(Map<String, String> paramMap) {

        int processCheckType = Integer.valueOf(paramMap.get("processCheckType").toString());
        long projectId = Long.valueOf(paramMap.get("projectId").toString());
        String sql = "";
        if (processCheckType == -1) { // 全部
            sql = "select count(*) from (" + ProcessSqlConstant.notThoseResponsible(projectId) + "  union  "
                    + ProcessSqlConstant.notTheDepartment(projectId) + "  union  "
                    + ProcessSqlConstant.roleCorrPos(projectId) + ") countSum";
        } else if (processCheckType == 0) {// 流程责任人存在的数据
            sql = ProcessSqlConstant.notThoseResponsibleCount(projectId);
        } else if (processCheckType == 1) { // 责任部门不存在
            sql = ProcessSqlConstant.notTheDepartmentCount(projectId);
        } else if (processCheckType == 2) { // 角色没有对应岗位
            sql = ProcessSqlConstant.roleCorrPosCount(projectId);
        }

        return sql;
    }

    public String fetchProcessCheckList(Map<String, String> paramMap) {

        int processCheckType = Integer.valueOf(paramMap.get("processCheckType").toString());
        long projectId = Long.valueOf(paramMap.get("projectId").toString());

        String sql = "SELECT FLOW_ID,FLOW_NAME,FLOW_ID_INPUT FROM (";
        if (processCheckType == -1) { // 全部
            sql += ProcessSqlConstant.notThoseResponsible(projectId) + "  union  "
                    + ProcessSqlConstant.notTheDepartment(projectId) + "  union  "
                    + ProcessSqlConstant.roleCorrPos(projectId);
        } else if (processCheckType == 0) {// 流程责任人存在的数据
            sql += ProcessSqlConstant.notThoseResponsible(projectId);
        } else if (processCheckType == 1) { // 责任部门不存在
            sql += ProcessSqlConstant.notTheDepartment(projectId);
        } else if (processCheckType == 2) { // 角色没有对应岗位
            sql += ProcessSqlConstant.roleCorrPos(projectId);
        }

        sql += " )a order by flow_id";

        return sql;
    }

    public String addRole(ViewRoleDetailBean role) {
        String sql = "insert into ROLE_PERMISSIONS(ID,NAME,TYPE,CREATE_TIME,CREATE_PEOPLE_ID,UPDATE_TIME,UPDATE_PEOPLE_ID) " +
                "values(#{id},#{roleName},#{roleType},#{createTime},#{createPeople},#{updateTime},#{updatePeople})";
        return sql;
    }

    public String findRolesUser(ViewRoleDetailBean role) {
        String sql = "select u.people_id as ID, ju.true_name NAME, ORG.ORG_NAME ORG_NAME, u.type,ju.login_name" +
                "  from USER_ROLE_PERMISSIONS u" +
                " inner join Jecn_User ju" +
                "    on u.people_id = ju.people_id" +
                " inner join JECN_USER_POSITION_RELATED JUPR" +
                "    ON JUPR.PEOPLE_ID = JU.PEOPLE_ID" +
                " inner JOIN JECN_FLOW_ORG_IMAGE O" +
                "    ON O.FIGURE_ID = JUPR.FIGURE_ID" +
                " inner JOIN JECN_FLOW_ORG ORG" +
                "    ON ORG.ORG_ID = O.ORG_ID" +
                " where u.role_id = #{id} and u.type = 0" +
                " union all " +
                " select org.org_id as id, org.org_name NAME, uorg.org_name ORG_NAME, O.type,'' login_name" +
                "  from USER_ROLE_PERMISSIONS o" +
                " inner join JECN_FLOW_ORG org" +
                "    on o.people_id = org.org_id" +
                "  left join JECN_FLOW_ORG uorg" +
                "    on org.per_org_id = uorg.org_id" +
                " where o.role_id = #{id} and o.type = 1" +
                " union all " +
                "select pos.figure_id   as id," +
                "       pos.figure_text MAME," +
                "       org.org_name    ORG_NAME," +
                "       p.type,'' login_name" +
                "  from USER_ROLE_PERMISSIONS p" +
                " inner join JECN_FLOW_ORG_IMAGE pos" +
                "    on p.people_id = pos.figure_id" +
                "  left join JECN_FLOW_ORG org" +
                "    on pos.org_id = org.org_id" +
                " where p.role_id = #{id} and p.type = 2";

        return sql;
    }


    public String addRolesUser(ViewRoleDetailBean role) {
        Long[] peopleIds = role.getPeopleId();
        Long[] orgId = role.getOrgId();
        Long[] posId = role.getPosId();
        StringBuilder sql = new StringBuilder();
        String guid = null;
        sql.append("INSERT  INTO USER_ROLE_PERMISSIONS(GUID,PEOPLE_ID,ROLE_ID,TYPE) ");
        for (int i = 0; i < peopleIds.length; i++) {
            guid = IDUtils.makeUUID();
            sql.append("select '" + guid + "' GUID, '" + peopleIds[i] + "' PEOPLE_ID , #{id} ROLE_ID, 0 TYPE");
            if (BaseSqlProvider.isOracle()) {
                sql.append("  FROM DUAL ");
            }
            if (i == peopleIds.length - 1 && orgId.length <= 0 && posId.length <= 0) {
                break;
            }
            sql.append(" UNION ALL ");
        }
        for (int i = 0; i < orgId.length; i++) {
            guid = IDUtils.makeUUID();
            sql.append("select '" + guid + "' GUID, '" + orgId[i] + "' PEOPLE_ID , #{id} ROLE_ID, 1 TYPE");
            if (BaseSqlProvider.isOracle()) {
                sql.append("  FROM DUAL ");
            }
            if (i == orgId.length - 1 && posId.length <= 0) {
                break;
            }
            sql.append(" UNION ALL ");
        }
        for (int i = 0; i < posId.length; i++) {
            guid = IDUtils.makeUUID();
            sql.append("select '" + guid + "' GUID, '" + posId[i] + "' PEOPLE_ID , #{id} ROLE_ID, 2 TYPE");
            if (BaseSqlProvider.isOracle()) {
                sql.append("  FROM DUAL ");
            }
            if (i == posId.length - 1) {
                break;
            }
            sql.append(" UNION ALL ");
        }
        return sql.toString();
    }

    public String delRolesUserToPeople(ViewRoleDetailBean role) {
        Long[] peopleIds = role.getPeopleId();
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM USER_ROLE_PERMISSIONS WHERE  PEOPLE_ID in (");
        for (int i = 0; i < peopleIds.length; i++) {
            sql.append(peopleIds[i]);
            sql.append(",");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(")");
        return sql.toString();
    }


    public String deleteRole(ViewRoleDetailBean role) {
        return "DELETE FROM ROLE_PERMISSIONS WHERE ID = #{id} ";
    }

    public String deleteRolesUserToRole(ViewRoleDetailBean role) {
        return "DELETE FROM USER_ROLE_PERMISSIONS WHERE ROLE_ID = #{id}";
    }

    public String findRolesByUser(Long pepoleId) {
        String sql = "SELECT U.GUID ID, U.PEOPLE_ID PID, U.ROLE_ID RID,JU.TRUE_NAME U_NAME,RP.NAME R_NAME" +
                "  FROM USER_ROLE_PERMISSIONS U" +
                "  INNER JOIN JECN_USER JU" +
                "  ON U.PEOPLE_ID = JU.PEOPLE_ID" +
                "  INNER JOIN  ROLE_PERMISSIONS RP" +
                "  ON RP.ID = U.ROLE_ID" +
                " WHERE U.PEOPLE_ID = #{pepoleId}";
        return sql;
    }

    public String findRolesThePosByUser(Long pepoleId) {
        String sql = "SELECT RP.ID" +
                "  FROM JECN_USER JU" +
                " INNER JOIN JECN_USER_POSITION_RELATED RE" +
                "    ON JU.PEOPLE_ID = RE.PEOPLE_ID" +
                " INNER JOIN JECN_FLOW_ORG_IMAGE POS" +
                "    ON POS.FIGURE_ID = RE.FIGURE_ID" +
                " INNER JOIN USER_ROLE_PERMISSIONS U" +
                "    ON U.PEOPLE_ID = POS.FIGURE_ID" +
                "   AND U.TYPE = 2" +
                " INNER JOIN ROLE_PERMISSIONS RP" +
                "    ON RP.ID = U.ROLE_ID" +
                " WHERE JU.PEOPLE_ID =#{pepoleId}";
        return sql;
    }

    public String findRolesTheOrgByUser(Long pepoleId) {
        String sql = "SELECT RP.ID" +
                "  FROM JECN_USER JU" +
                " INNER JOIN JECN_USER_POSITION_RELATED RE" +
                "    ON JU.PEOPLE_ID = RE.PEOPLE_ID" +
                " INNER JOIN JECN_FLOW_ORG_IMAGE POS" +
                "    ON POS.FIGURE_ID = RE.FIGURE_ID" +
                " INNER JOIN JECN_FLOW_ORG ORG" +
                "    ON POS.ORG_ID = ORG.ORG_ID" +
                " INNER JOIN JECN_FLOW_ORG T" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("ORG") +
                " INNER JOIN USER_ROLE_PERMISSIONS U" +
                "    ON U.PEOPLE_ID = T.ORG_ID" +
                "   AND U.TYPE = 1" +
                " INNER JOIN ROLE_PERMISSIONS RP" +
                "    ON RP.ID = U.ROLE_ID" +
                " WHERE JU.PEOPLE_ID = #{pepoleId}";
        return sql;
    }

    public String deleteRoleAssociationMenu(ViewRoleDetailBean role) {
        String sql = "DELETE FROM ROLE_PERMISSIONS_MENU WHERE ROLE_ID = #{id}";
        return sql;
    }

}
