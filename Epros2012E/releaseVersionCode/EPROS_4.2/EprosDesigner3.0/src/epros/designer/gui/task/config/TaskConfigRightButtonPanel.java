package epros.designer.gui.task.config;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.task.TaskConfigItem;

import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 右面按钮面板，其包括加入、上移、下移、删除、默认等按钮
 * 
 * @author ZHOUXY
 * 
 */
public class TaskConfigRightButtonPanel extends JecnPanel {
	/** 添加按钮 */
	private JButton addJButton = null;
	/** 删除按钮 */
	private JButton deleteJButton = null;
	/** 编辑按钮 */
	private JButton editJButton = null;
	/** 上移按钮 */
	private JButton upJButton = null;
	/** 下移按钮 */
	private JButton downJButton = null;
	/** 默认按钮 */
	private JButton defaultJButton = null;

	private TaskConfigDialog dialog;

	// private TaskConfigTableScrollPane tablePanel;
	// private TaskConfigEditPanel editPanel;

	public TaskConfigRightButtonPanel(TaskConfigDialog dialog) {

		this.dialog = dialog;
		// this.editPanel = dialog.getTaskEditPanel();
		// this.tablePanel = dialog.getTaskEditPanel().getTaskTablePanel();
		initComponents();
		initLayout();
	}

	protected void initComponents() {
		// 无边框
		this.setBorder(null);

		// 添加按钮
		addJButton = new JButton(JecnProperties.getValue("task_addBtn"));

		// 删除按钮
		deleteJButton = new JButton(JecnProperties.getValue("task_deleteBtn"));

		// 编辑按钮
		editJButton = new JButton(JecnProperties.getValue("task_editBtn"));

		// 上移按钮
		upJButton = new JButton(JecnProperties.getValue("task_upBtn"));

		// 下移按钮
		downJButton = new JButton(JecnProperties.getValue("task_downBtn"));

		// 默认按钮
		defaultJButton = new JButton(JecnProperties.getValue("task_defaultBtn"));

		// 事件
		addJButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				add();
			}
		});
		deleteJButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				del();
			}
		});
		editJButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				edit();
			}
		});
		upJButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				up();
			}
		});
		downJButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				down();
			}
		});
		defaultJButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				def();
			}
		});
	}

	private void add() {

		// 加入对话框
		AddTaskItemDialog addDialog = new AddTaskItemDialog(dialog);
		// 标题
		addDialog.setTitle("");

		addDialog.setVisible(true);

	}

	private void del() {
		dialog.getTaskEditPanel().getTaskTablePanel().delShowItem();
	}

	private void edit() {
		// 获取选中显示项
		List<TaskConfigItem> selectedItemBeanList = dialog.getTaskEditPanel().getTaskTablePanel()
				.getSelectedItemBeanList();
		if (selectedItemBeanList == null || selectedItemBeanList.size() != 1) {
			return;
		}
		EditTaskItemDialog editDialog = new EditTaskItemDialog(dialog, selectedItemBeanList.get(0));
		editDialog.setVisible(true);
	}

	private void up() {
		dialog.getTaskEditPanel().getTaskTablePanel().upShowUItem();
	}

	private void down() {
		dialog.getTaskEditPanel().getTaskTablePanel().downShowUItem();
	}

	private void def() {
		dialog.getTaskEditPanel().reSetApproveItems();
	}

	private void initLayout() {
		// 布局
		Insets insets = new Insets(6, 7, 6, 7);
		// 添加按钮
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		this.add(addJButton, c);
		// 删除按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(deleteJButton, c);
		// 编辑按钮
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(editJButton, c);
		// 上移按钮
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(upJButton, c);
		// 下移按钮
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(downJButton, c);
		// 默认按钮
		c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(defaultJButton, c);
		// 空闲区域
		c = new GridBagConstraints(0, 6, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(new JLabel(), c);
	}

	public void buttonEditable(boolean editable) {
		addJButton.setEnabled(editable);
		/** 删除按钮 */
		deleteJButton.setEnabled(editable);
		/** 编辑按钮 */
		editJButton.setEnabled(editable);
		/** 上移按钮 */
		upJButton.setEnabled(editable);
		/** 下移按钮 */
		downJButton.setEnabled(editable);
		/** 默认按钮 */
		defaultJButton.setEnabled(editable);
	}
}
