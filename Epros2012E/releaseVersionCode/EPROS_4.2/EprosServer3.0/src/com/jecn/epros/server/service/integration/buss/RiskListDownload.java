package com.jecn.epros.server.service.integration.buss;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.excel.ExcelFont;
import com.jecn.epros.server.download.excel.SetExcelStyle;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.integration.RiskDataListBean;
import com.jecn.epros.server.webBean.integration.RiskList;

/**
 * 风险清单下载
 * 
 * @author fuzhh 2013-12-5
 * 
 */
public class RiskListDownload {
	private static Logger log = Logger.getLogger(RiskListDownload.class);

	/**
	 * 风险清单下载
	 * 
	 * @author fuzhh 2013-12-17
	 * @param riskList
	 *            风险清单数据
	 * @param depth
	 * @return
	 * @throws Exception
	 */
	public static byte[] createRiskListDesc(RiskList riskList, int depth)
			throws Exception {
		if (riskList == null) {
			throw new NullPointerException();
		}
		WritableWorkbook wbookData = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			File file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// 风险清单
			WritableSheet wsheet = wbookData.createSheet(JecnUtil
					.getValue("riskList"), 0);
			// 设置标题字体样式
			WritableFont fontTitle1 = new WritableFont(WritableFont
					.createFont("宋体"), 11, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK); // 字体加粗
			WritableCellFormat wformatTitle1 = SetExcelStyle
					.setExcelCellStyle(new ExcelFont(fontTitle1, Colour.YELLOW));
			// 设置字体标题背景特殊样式
			WritableCellFormat wformatTitle2 = SetExcelStyle
					.setExcelCellStyle(new ExcelFont(fontTitle1,
							Colour.LIGHT_ORANGE));
			// 设置字体样式（普通）
			WritableFont fontStyle5 = new WritableFont(WritableFont
					.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
			// 设置单元格内容居中
			WritableCellFormat wformatContecnt5 = SetExcelStyle
					.setExcelCellStyle(new ExcelFont(Colour.WHITE,
							VerticalAlignment.CENTRE, Alignment.CENTRE,
							fontStyle5));
			wformatContecnt5.setWrap(true);
			List<WritableCellFormat> cellFormats = new ArrayList<WritableCellFormat>();
			cellFormats.add(wformatTitle1);
			cellFormats.add(wformatTitle2);
			cellFormats.add(wformatContecnt5);

			// 获取总的级别个数
			int levelTotal = riskList.getLevelTotal();
			int count = 1;
			// 行号
			wsheet.addCell(new Label(0, 1, "", cellFormats.get(0)));
			wsheet.setColumnView(0, 5);
			wsheet.setRowView(1, 440);
			for (int i = 0; i < levelTotal; i++) {// 创建级别标题
				if (i >= depth) {
					// 级
					wsheet.addCell(new Label(count, 1, i
							+ JecnUtil.getValue("level"), cellFormats.get(0)));
					wsheet.setColumnView(count, 20);
					count++;
				}
			}
			// 风险点编号
			wsheet.addCell(new Label(count, 1, JecnUtil.getValue("riskNumber"),
					cellFormats.get(1)));
			// 风险描述
			wsheet.addCell(new Label(count + 1, 1, JecnUtil
					.getValue("riskName"), cellFormats.get(1)));
			// 风险等级
			wsheet.addCell(new Label(count + 2, 1, JecnUtil
					.getValue("riskGrade"), cellFormats.get(1)));
			// 企业内部控制基本规范、应用指引和解读相关要求
			wsheet.addCell(new Label(count + 3, 1, JecnUtil
					.getValue("DetailGuide"), cellFormats.get(0)));
			// 标准化控制
			wsheet.addCell(new Label(count + 4, 1, JecnUtil
					.getValue("standardControl"), cellFormats.get(0)));
			// 控制目标
			wsheet.addCell(new Label(count + 5, 1, JecnUtil
					.getValue("controlTarget"), cellFormats.get(0)));
			// 控制编号
			wsheet.addCell(new Label(count + 6, 1, JecnUtil
					.getValue("controlNumber"), cellFormats.get(0)));
			// 控制活动简描述
			wsheet.addCell(new Label(count + 7, 1, JecnUtil
					.getValue("controlActiveDescription"), cellFormats.get(0)));
			// 角色名称
			wsheet.addCell(new Label(count + 8, 1, JecnUtil
					.getValue("roleName"), cellFormats.get(0)));
			// 活动名称
			wsheet.addCell(new Label(count + 9, 1, JecnUtil
					.getValue("activityTitle"), cellFormats.get(0)));
			// 流程名称
			wsheet.addCell(new Label(count + 10, 1, JecnUtil
					.getValue("processName"), cellFormats.get(0)));
			// 制度名称
			wsheet.addCell(new Label(count + 11, 1, JecnUtil
					.getValue("ruleName"), cellFormats.get(0)));
			// 责任部门
			wsheet.addCell(new Label(count + 12, 1, JecnUtil
					.getValue("responsibilityDepartment"), cellFormats.get(0)));
			// 控制方法：人工/自动
			wsheet.addCell(new Label(count + 13, 1, JecnUtil
					.getValue("controlMethod"), cellFormats.get(0)));
			// 控制类型：预防性/发现性
			wsheet.addCell(new Label(count + 14, 1, JecnUtil
					.getValue("controlTypePreventive"), cellFormats.get(0)));
			// 是否为关键控制
			wsheet
					.addCell(new Label(count + 15, 1, JecnUtil
							.getValue("whetherItIsTheKeyToControl"),
							cellFormats.get(0)));
			// 控制频率
			wsheet.addCell(new Label(count + 16, 1, JecnUtil
					.getValue("controlFrequency"), cellFormats.get(0)));

			// 设置列宽
			wsheet.setColumnView(count, 20);
			wsheet.setColumnView(count + 1, 20);
			wsheet.setColumnView(count + 2, 20);
			wsheet.setColumnView(count + 3, 60);
			wsheet.setColumnView(count + 4, 20);
			wsheet.setColumnView(count + 5, 20);
			wsheet.setColumnView(count + 6, 20);
			wsheet.setColumnView(count + 7, 50);
			wsheet.setColumnView(count + 8, 20);
			wsheet.setColumnView(count + 9, 20);
			wsheet.setColumnView(count + 10, 20);
			wsheet.setColumnView(count + 11, 20);
			wsheet.setColumnView(count + 12, 30);
			wsheet.setColumnView(count + 13, 30);
			wsheet.setColumnView(count + 14, 20);
			wsheet.setColumnView(count + 15, 20);
			wsheet.setColumnView(count + 16, 20);

			WritableFont fontContent = new WritableFont(WritableFont.TIMES, 12);
			WritableCellFormat wformatContent = new WritableCellFormat(
					fontContent);
			// 设置自动换行
			wformatContent.setWrap(true);
			// 表标题样式
			wformatContent.setBorder(Border.ALL, BorderLineStyle.THIN);
			// 添加数据
			addSheetData(riskList, wsheet, cellFormats, depth);
			// 写入Exel工作表
			wbookData.write();
			// 关闭Excel工作薄对象
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			// 把一个文件转化为字节
			byte[] tempDate = JecnUtil.getByte(file);
			if (file.exists()) {
				file.delete();
			}
			return tempDate;
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("关闭WritableWorkbook异常", e);
				}
			}
		}

		return null;
	}

	/**
	 * 添加数据
	 * 
	 * @author fuzhh 2013-12-17
	 * @param riskList
	 * @param wsheet
	 * @param wformatTitle
	 * @param depth
	 *            当前选中节点级别
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private static void addSheetData(RiskList riskList, WritableSheet wsheet,
			List<WritableCellFormat> cellFormats, int depth)
			throws RowsExceededException, WriteException {

		// 获取当前清单总共有几级
		int levelTotal = riskList.getLevelTotal();
		int count = 2; // 行
		int rowNum = 1;
		for (int i = 0; i < riskList.getDataListBeans().size(); i++) {
			// 设置数据行高
			wsheet.setRowView(count, 360);

			RiskDataListBean riskDataListBean = riskList.getDataListBeans()
					.get(i);

			// 如果是目录执行下次循环，因为是以风险为标准查找的父级目录
			if (riskDataListBean.getIsDir() == 0) {
				continue;
			}
			// 临时目录集合
			List<RiskDirExcelBean> riskDirExcelList = new ArrayList<RiskDirExcelBean>();
			riskDirExcelList.clear();

			// 查找当前风险的所有的父级目录
			findDir(riskList, riskDataListBean, riskDirExcelList);

			// 获取当前风险总共有几级目录
			int total = riskDirExcelList.size() - 1;

			// 行号
			wsheet.addCell(new Label(0, count, String.valueOf(rowNum),
					cellFormats.get(2)));

			// ==添加目录到excel Start==
			for (int j = 1; j <= levelTotal; j++) {
				if (j <= riskDirExcelList.size()) {
					String riskName = riskDirExcelList.get(total).getRiskName();
					// 添加级别名称
					wsheet.addCell(new Label(j, count, riskName, cellFormats
							.get(2)));
				} else {
					wsheet.addCell(new Label(j, count, "", cellFormats.get(2)));
				}
				if (total >= 0) {
					total--;
				}
			}
			// =========end============

			// 风险编号
			wsheet.addCell(new Label(levelTotal - depth + 1, count,
					riskDataListBean.getRiskCode(), cellFormats.get(2)));
			// 风险描述
			wsheet.addCell(new Label(levelTotal - depth + 2, count,
					riskDataListBean.getRiskName(), cellFormats.get(2)));
			// 风险等级
			String grade = "";
			int riskGrade = riskDataListBean.getRiskGrade();
			if (riskGrade == 1) {
				// 高
				grade = JecnUtil.getValue("high");
			} else if (riskGrade == 2) {
				// 中
				grade = JecnUtil.getValue("In");
			} else if (riskGrade == 3) {
				// 低
				grade = JecnUtil.getValue("Low");
			}
			wsheet.addCell(new Label(levelTotal - depth + 3, count, grade,
					cellFormats.get(2)));

			// 企业内部控制
			wsheet.addCell(new Label(levelTotal - depth + 4, count,
					riskDataListBean.getGuideVal(), cellFormats.get(2)));

			// 标准化控制
			wsheet.addCell(new Label(levelTotal - depth + 5, count,
					riskDataListBean.getStandardControl(), cellFormats.get(2)));

			// 控制目标
			wsheet.addCell(new Label(levelTotal - depth + 6, count,
					riskDataListBean.getControlDescroption(), cellFormats
							.get(2)));

			// 控制编号
			wsheet.addCell(new Label(levelTotal - depth + 7, count,
					riskDataListBean.getControlNum(), cellFormats.get(2)));

			// 控制活动简述
			wsheet.addCell(new Label(levelTotal - depth + 8, count,
					riskDataListBean.getActiveShow(), cellFormats.get(2)));

			// 角色名称
			wsheet.addCell(new Label(levelTotal - depth + 9, count,
					riskDataListBean.getRoleName(), cellFormats.get(2)));

			// 活动名称
			wsheet.addCell(new Label(levelTotal - depth + 10, count,
					riskDataListBean.getActiveName(), cellFormats.get(2)));

			// 流程名称
			wsheet.addCell(new Label(levelTotal - depth + 11, count,
					riskDataListBean.getFlowName(), cellFormats.get(2)));

			// 制度名称
			wsheet.addCell(new Label(levelTotal - depth + 12, count,
					riskDataListBean.getRuleName(), cellFormats.get(2)));
			boolean isRule = false;
			if (riskDataListBean.getRuleName() != null
					&& !"".equals(riskDataListBean.getRuleName())) {
				isRule = true;
			}
			String resOrg = riskDataListBean.getResOrgName();
			if (isRule) {
				resOrg = riskDataListBean.getRuleOrgName();
			}
			// 责任部门
			wsheet.addCell(new Label(levelTotal - depth + 13, count, resOrg,
					cellFormats.get(2)));

			// ==控制方法 默认：人工==
			String controlMethod = JecnUtil.getValue("artificial");
			int conMethod = riskDataListBean.getControlMethod();
			if (conMethod == 1) {
				// IT
				controlMethod = JecnUtil.getValue("it");
			} else if (conMethod == 2) {
				// 人工/IT
				controlMethod = JecnUtil.getValue("renOrIt");
			}
			wsheet.addCell(new Label(levelTotal - depth + 14, count,
					controlMethod, cellFormats.get(2)));
			// =====================

			// ==控制类型 默认：预防性
			String controlType = JecnUtil.getValue("preventive");
			int conType = riskDataListBean.getControlType();
			if (conType == 1) { // 发现型
				// 发现性
				controlType = JecnUtil.getValue("discover");
			}
			wsheet.addCell(new Label(levelTotal - depth + 15, count,
					controlType, cellFormats.get(2)));
			// ====================

			// ==关键控制点 默认：是
			String keyControl = JecnUtil.getValue("is");
			if (riskDataListBean.getKeyControl() == 1) {
				// 否
				keyControl = JecnUtil.getValue("no");
			}
			wsheet.addCell(new Label(levelTotal - depth + 16, count,
					keyControl, cellFormats.get(2)));
			// ===================

			// 控制频率 默认 随时
			String controlFrequency = JecnUtil.getValue("noScheduled");
			if (riskDataListBean.getControlFrequency() == 1) {
				// 日
				controlFrequency = JecnUtil.getValue("day");
			} else if (riskDataListBean.getControlFrequency() == 2) {
				// 周
				controlFrequency = JecnUtil.getValue("weeks");
			} else if (riskDataListBean.getControlFrequency() == 3) {
				// 月
				controlFrequency = JecnUtil.getValue("month");
			} else if (riskDataListBean.getControlFrequency() == 4) {
				// 季度
				controlFrequency = JecnUtil.getValue("seasons");
			} else if (riskDataListBean.getControlFrequency() == 5) {
				// 年度
				controlFrequency = JecnUtil.getValue("year");
			}
			wsheet.addCell(new Label(levelTotal - depth + 17, count,
					controlFrequency, cellFormats.get(2)));
			rowNum++;
			count++;
		}
		// 总计
		wsheet.addCell(new Label(1, 0, JecnUtil.getValue("totalNum")
				+ String.valueOf(rowNum - 1), cellFormats.get(2)));
		wsheet.setRowView(0, 360);
	}

	/***
	 * 递归查找当前风险的所有父级目录，并且拼装到JSON中
	 * 
	 * @param riskList
	 *            风险清单封装 riskDataBean 单个风险清单数据 bufer
	 * @author chehuanbo
	 * @since V3.06
	 */
	private static void findDir(RiskList riskList,
			RiskDataListBean riskDataBean,
			List<RiskDirExcelBean> riskDirExcelList) {

		RiskDataListBean riskDataListBean1 = riskDataBean;

		boolean isExit = false; // 判断是否存在父节点 ，默认：false 不存在

		for (RiskDataListBean perRiskListBean : riskList.getDataListBeans()) {
			if (riskDataListBean1.getParentId().longValue() == perRiskListBean
					.getRiskId()) {
				RiskDirExcelBean riskDirExcelBean = new RiskDirExcelBean();
				// 添加风险目录名称
				riskDirExcelBean.setRiskName(perRiskListBean.getRiskName());
				riskDirExcelList.add(riskDirExcelBean);
				// 更改临时数据bean 主要用来下一次查找父级节点的查找
				riskDataListBean1 = perRiskListBean;
				isExit = true;
				break;
			}
		}

		if (isExit) { // 如果存在父节点，继续递归查找
			findDir(riskList, riskDataListBean1, riskDirExcelList);
		}
	}
}

/***
 * 风险目录bean
 * 
 * @author chehuanbo
 * 
 */
class RiskDirExcelBean {

	private String riskName = null;

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}
}
