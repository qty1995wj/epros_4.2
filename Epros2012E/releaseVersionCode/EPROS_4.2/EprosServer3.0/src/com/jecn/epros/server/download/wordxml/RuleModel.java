package com.jecn.epros.server.download.wordxml;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import wordxml.constant.Constants.NFC;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.dom.WordDocument;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.util.FileUtil;

import com.jecn.epros.server.bean.download.RuleContentBean;
import com.jecn.epros.server.bean.download.RuleDownloadBean;
import com.jecn.epros.server.bean.download.RuleFileBean;
import com.jecn.epros.server.bean.download.RuleFlowBean;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnRuleCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.viewdoc.MSOffice.OfficeTools;

public abstract class RuleModel implements IProcessFileDefaultStyle {
	/** log 日志 */
	protected final Log log = LogFactory.getLog(RuleModel.class);
	// 空白内容标志
	protected static final String blank = JecnUtil.getValue("none");
	// 数据Bean
	protected RuleDownloadBean ruleDownloadBean;
	// 操作说明的目标路径
	private String path;
	// word文档
	private WordDocument doc;
	// 文档常用属性
	protected ProcessFileProperty docProperty;

	// 记录当前多少项，为了显示15.1之类的
	protected int numberMark;

	protected RuleWordPlex word;

	/**
	 * 
	 * @param ruleDownloadBean
	 *            数据Bean
	 * @param path
	 *            文档的路径
	 * @param flowChartDirection
	 *            true:横向显示流程图， false:纵向显示流程图
	 */
	protected RuleModel(RuleDownloadBean ruleDownloadBean, String path, boolean flowChartDirection) {
		this.ruleDownloadBean = ruleDownloadBean;
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			this.path = path;
		} else {
			this.path = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
		}
		// 创建文档
		this.doc = new WordDocument(FileUtil.getClassPathDocumentFile());
		// 生成文档属性
		docProperty = new ProcessFileProperty(doc, flowChartDirection, ruleDownloadBean.getJecnConfigItemBean());
		docProperty.setNewRowHeight(0.6F);
		word = new RuleWordPlex(this, ruleDownloadBean);
	}

	/**
	 * 包括英文 数字 特殊符号) 默认字体
	 */
	protected void setDefAscii(String font) {
		doc.setAscii(font);
	}

	/**
	 * 设置(中文)默认字体
	 */
	protected void setDefFareast(String font) {
		doc.setFareast(font);

	}

	/**
	 * 设置标题样式
	 * 
	 * @param chart
	 *            标题序号和文本之间的字符 例如 、
	 * @param titlePBean
	 */
	protected void setDocStyle(String chart, ParagraphBean titlePBean) {
		doc.setStyle(chart, titlePBean);
	}

	/**
	 * 设置文档序号类型
	 * 
	 * @param nfc
	 */
	protected void setDocNFC(NFC nfc) {
		doc.setNfc(nfc);
	}

	/**
	 * 设置文档序号从多少开始
	 * 
	 * @param startNum
	 */
	protected void setDocStartNum(int startNum) {
		doc.setStartNum(startNum);
	}

	protected void setHandNumber() {
		doc.setHandNumber();
	}

	/**
	 * 初始化入口 通过调用子类的代码来覆盖默认样式，以及给各节添加个性化内容
	 */
	protected final void init() {
		docProperty.setTblContentStyle(tblContentStyle());
		docProperty.setTblTitleStyle(tblTitleStyle());
		docProperty.setTblStyle(tblStyle());
		docProperty.setTextContentStyle(textContentStyle());
		docProperty.setTextTitleStyle(textTitleStyle());
		initTitleSect(docProperty.getTitleSect());
		initFirstSect(docProperty.getFirstSect());
		initFlowChartSect(docProperty.getFlowChartSect());
		initSecondSect(docProperty.getSecondSect());
	}

	/**
	 * 构建并输出文档
	 */
	public String write() {
		try {
			build();
			doc.write(path);
		} catch (Exception e) {
			log.error("构建操作说明文档异常", e);
		}
		return OfficeTools.xmlWordSaveAsOfficeWord(path);
	}

	/**
	 * 按照换行符截取字符串数据
	 * 
	 * @param content
	 * @return
	 */
	private String[] getContents(String content) {
		if (StringUtils.isBlank(content)) {
			return new String[] { blank };
		}
		return content.split("\n");
	}

	/**
	 * 调用子类的具体实现来构建文档
	 */
	protected void build() throws Exception {
		// 初始化 子类中的属性也就是通过调用子类的代码来覆盖默认样式
		init();
		beforeHandle();
		RuleItem ruleFileItem = new RuleItem();
		ruleFileItem.setDataBean(ruleDownloadBean);
		// 生成所有项
		if (ruleDownloadBean.getRuleTitleTList() != null) {

			Map<Long, RuleContentBean> contentMap = new HashMap<Long, RuleContentBean>();
			Map<Long, List<RuleFileBean>> fileMap = new HashMap<Long, List<RuleFileBean>>();
			Map<Long, List<RuleFlowBean>> flowMap = new HashMap<Long, List<RuleFlowBean>>();
			Map<Long, List<RuleFlowBean>> mapMap = new HashMap<Long, List<RuleFlowBean>>();
			Map<Long, List<JecnStandarCommon>> standardMap = new HashMap<Long, List<JecnStandarCommon>>();
			Map<Long, List<JecnRiskCommon>> riskMap = new HashMap<Long, List<JecnRiskCommon>>();
			Map<Long, List<JecnRuleCommon>> ruleMap = new HashMap<Long, List<JecnRuleCommon>>();

			List<RuleContentBean> ruleContentList = ruleDownloadBean.getRuleContentList();
			List<RuleFileBean> ruleFileList = ruleDownloadBean.getRuleFileList();
			List<RuleFlowBean> ruleFlowList = ruleDownloadBean.getRuleFlowList();
			List<RuleFlowBean> ruleFlowMapList = ruleDownloadBean.getRuleFlowMapList();
			List<JecnStandarCommon> ruleStandardList = ruleDownloadBean.getRuleStandardList();
			List<JecnRiskCommon> ruleRiskList = ruleDownloadBean.getRuleRiskList();
			List<JecnRuleCommon> ruleRuleList = ruleDownloadBean.getRuleRuleList();

			for (RuleContentBean c : ruleContentList) {
				contentMap.put(c.getRuleTitleId(), c);
			}

			for (RuleFileBean c : ruleFileList) {
				if (fileMap.get(c.getRuleTitleId()) == null) {
					fileMap.put(c.getRuleTitleId(), new ArrayList<RuleFileBean>());
				}
				fileMap.get(c.getRuleTitleId()).add(c);
			}

			for (RuleFlowBean c : ruleFlowList) {
				if (flowMap.get(c.getRuleTitleId()) == null) {
					flowMap.put(c.getRuleTitleId(), new ArrayList<RuleFlowBean>());
				}
				flowMap.get(c.getRuleTitleId()).add(c);
			}

			for (RuleFlowBean c : ruleFlowMapList) {
				if (mapMap.get(c.getRuleTitleId()) == null) {
					mapMap.put(c.getRuleTitleId(), new ArrayList<RuleFlowBean>());
				}
				mapMap.get(c.getRuleTitleId()).add(c);
			}

			for (JecnStandarCommon c : ruleStandardList) {
				if (standardMap.get(c.getRuleTitleId()) == null) {
					standardMap.put(c.getRuleTitleId(), new ArrayList<JecnStandarCommon>());
				}
				standardMap.get(c.getRuleTitleId()).add(c);
			}

			for (JecnRiskCommon c : ruleRiskList) {
				if (riskMap.get(c.getRuleTitleId()) == null) {
					riskMap.put(c.getRuleTitleId(), new ArrayList<JecnRiskCommon>());
				}
				riskMap.get(c.getRuleTitleId()).add(c);
			}

			for (JecnRuleCommon c : ruleRuleList) {
				if (ruleMap.get(c.getRuleTitleId()) == null) {
					ruleMap.put(c.getRuleTitleId(), new ArrayList<JecnRuleCommon>());
				}
				ruleMap.get(c.getRuleTitleId()).add(c);
			}

			for (RuleTitleT ruleTitle : ruleDownloadBean.getRuleTitleTList()) {
				numberMark++;
				itemBeforeHandle(ruleTitle, ruleFileItem);
				ruleFileItem.setItemTitle(ruleTitle.getTitleName());
				ruleFileItem.setBelongTo(docProperty.getSecondSect());

				Integer type = ruleTitle.getType();
				Long id = ruleTitle.getId();
				if (type == 0) {
					if (JecnConfigTool.isMengNiuViewFileCode()
							&& StringUtils.isBlank(contentMap.get(id).getRuleContent())) {
						continue;
					}
					content(ruleTitle, ruleFileItem, contentMap.get(id));
				} else if (type == 1) {// 文件表单
					file(ruleTitle, ruleFileItem, fileMap.get(id), true);
				} else if (type == 2) {// 流程
					flow(ruleTitle, ruleFileItem, flowMap.get(id), true);
				} else if (type == 3) {// 架构
					map(ruleTitle, ruleFileItem, mapMap.get(id), true);
				} else if (type == 4) {// 标准
					standard(ruleTitle, ruleFileItem, standardMap.get(id), true);
				} else if (type == 5) {// 风险
					risk(ruleTitle, ruleFileItem, riskMap.get(id), true);
				} else if (type == 6) {// 制度
					rule(ruleTitle, ruleFileItem, ruleMap.get(id), true);
				}

				itemAfterHandle(ruleTitle, ruleFileItem);
			}
		}
		afterHandle();
	}

	private void rule(RuleTitleT ruleTitle, RuleItem ruleFileItem, List<JecnRuleCommon> list, boolean hasTitle) {
		String[] titles = { "制度编号", "制度名称" };
		List<String[]> contents = new ArrayList<String[]>();
		float[] width = { 7, 7 };
		if (list == null) {
			contents = null;
		} else {
			for (JecnRuleCommon c : list) {
				contents.add(new String[] { nullToEmpty(c.getRuleNumber()), nullToEmpty(c.getRuleName()) });
			}
		}
		createTableItem(ruleFileItem, titles, contents, width, hasTitle);
	}

	protected void itemAfterHandle(RuleTitleT ruleTitle, RuleItem ruleItem) {

	}

	protected void itemBeforeHandle(RuleTitleT ruleTitle, RuleItem ruleItem) {

	}

	protected void beforeHandle() {

	}

	protected void afterHandle() {

	}

	protected void risk(RuleTitleT ruleTitle, RuleItem ruleFileItem, List<JecnRiskCommon> list, boolean hasTitle) {
		String[] titles = { "风险编号", "风险描述" };
		List<String[]> contents = new ArrayList<String[]>();
		float[] width = { 7, 7 };
		if (list == null) {
			contents = null;
		} else {
			for (JecnRiskCommon c : list) {
				contents.add(new String[] { c.getRiskNumber(), c.getRiskName() });
			}
		}
		createTableItem(ruleFileItem, titles, contents, width, hasTitle);
	}

	protected void standard(RuleTitleT ruleTitle, RuleItem ruleFileItem, List<JecnStandarCommon> list, boolean hasTitle) {
		String[] titles = { "标准名称", "目录" };
		List<String[]> contents = new ArrayList<String[]>();
		float[] width = { 5, 9 };
		if (list == null) {
			contents = null;
		} else {
			for (JecnStandarCommon c : list) {
				contents.add(new String[] { c.getStandarName(), nullToEmpty(c.gettPath()) });
			}
		}
		createTableItem(ruleFileItem, titles, contents, width, hasTitle);

	}

	protected void map(RuleTitleT ruleTitle, RuleItem ruleFileItem, List<RuleFlowBean> list, boolean hasTitle) {
		String[] titles = { "架构编号", "架构名称" };
		List<String[]> contents = new ArrayList<String[]>();
		float[] width = { 7, 7 };
		if (list == null) {
			contents = null;
		} else {
			for (RuleFlowBean c : list) {
				contents.add(new String[] { c.getFlowInput(), c.getFlowName() });
			}
		}
		createTableItem(ruleFileItem, titles, contents, width, hasTitle);
	}

	protected void flow(RuleTitleT ruleTitle, RuleItem ruleFileItem, List<RuleFlowBean> list, boolean hasTitle) {
		String[] titles = { "流程编号", "流程名称" };
		List<String[]> contents = new ArrayList<String[]>();
		float[] width = { 7, 7 };
		if (list == null) {
			contents = null;
		} else {
			for (RuleFlowBean c : list) {
				contents.add(new String[] { c.getFlowInput(), c.getFlowName() });
			}
		}
		createTableItem(ruleFileItem, titles, contents, width, hasTitle);
	}

	protected void file(RuleTitleT ruleTitle, RuleItem ruleFileItem, List<RuleFileBean> list, boolean hasTitle) {
		String[] titles = { "文件编号", "文件名称" };
		List<String[]> contents = new ArrayList<String[]>();
		float[] width = { 7, 7 };
		if (list == null) {
			contents = null;
		} else {
			for (RuleFileBean c : list) {
				contents.add(new String[] { c.getFileInput(), c.getFileName() });
			}
		}
		createTableItem(ruleFileItem, titles, contents, width, hasTitle);
	}

	protected void content(RuleTitleT ruleTitle, RuleItem ruleFileItem, RuleContentBean ruleContentBean) {
		createTextItem(ruleFileItem, ruleContentBean.getRuleContent());
	}

	protected void createTableItem(RuleItem ruleItem, String[] titles, List<String[]> contents, float[] width,
			boolean hasTitle) {
		if (contents == null || contents.size() == 0) {
			createTextItem(ruleItem, hasTitle, blank);
			return;
		} else {
			// 标题
			if (hasTitle) {
				createTitle(ruleItem);
			}
			List<String[]> data = new ArrayList<String[]>();
			data.add(titles);
			data.addAll(contents);
			Table tbl = createTableItem(ruleItem, width, data);
			tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
					.isTblTitleCrossPageBreaks());
			tbl.setValign(Valign.center);
		}
	}

	private Paragraph getNoneParagraph() {
		ParagraphBean pBean = textContentStyle();
		Paragraph none = new Paragraph(JecnUtil.getValue("none"), pBean);
		return none;
	}

	/**
	 * 初始化封面节
	 * 
	 * @param sect
	 */
	protected abstract void initTitleSect(final Sect titleSect);

	/**
	 * 初始化第一个内容节
	 * 
	 * @param sect
	 */
	protected abstract void initFirstSect(final Sect firstSect);

	/**
	 * 初始化流程图节
	 * 
	 * @param sect
	 */
	protected abstract void initFlowChartSect(final Sect flowChartSect);

	/**
	 * 初始化第二个节
	 * 
	 * @param sect
	 */
	protected abstract void initSecondSect(final Sect secondSect);

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	protected void createTextItem(RuleItem ruleItem, String... content) {
		this.createTextItem(ruleItem, true, content);
	}

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	protected void createTextItem(RuleItem ruleItem, boolean hasTitle, String... content) {
		if (hasTitle) {
			createTitle(ruleItem);
		}
		List<String> result = new ArrayList<String>();
		for (String c : content) {
			if (StringUtils.isNotBlank(c)) {
				String[] arr = c.split("\n");
				if (arr != null && arr.length > 0) {
					for (String a : arr) {
						if (StringUtils.isNotBlank(a)) {
							result.add(a);
						}
					}
				}
			}
		}
		ruleItem.getBelongTo().createParagraph(result.toArray(new String[result.size()]),
				docProperty.getTextContentStyle());
	}

	/**
	 * 创建表格项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	protected Table createTableItem(RuleItem ruleItem, float[] width, List<String[]> rowData) {
		if (docProperty.getNewTblWidth() > 0) {
			resizeTblWidth(width);
		}
		// 创建表格
		Table tbl = ruleItem.getBelongTo().createTable(docProperty.getTblStyle(), width);
		if (docProperty.getNewRowHeight() > 0) {
			tbl.createTableRow(rowData, docProperty.getTblContentStyle(), docProperty.getNewRowHeight());
		} else {
			tbl.createTableRow(rowData, docProperty.getTblContentStyle());
		}
		return tbl;
	}

	/**
	 * 重设表格宽度
	 * 
	 * @param oldWidth
	 */
	private void resizeTblWidth(float[] oldWidth) {
		float old = 0;
		for (float f : oldWidth) {
			old += f;
		}
		// 获取缩放比例
		float scale = Float.valueOf(String.format("%.2f", docProperty.getNewTblWidth() / old));
		for (int i = 0; i < oldWidth.length; i++) {
			oldWidth[i] = oldWidth[i] * scale;
		}
	}

	/**
	 * 创建标题
	 * 
	 * @param titleName
	 * @param contentSect
	 */
	protected void createTitle(RuleItem ruleItem) {
		ruleItem.getBelongTo().createParagraph(ruleItem.getItemTitle(), docProperty.getTextTitleStyle());
	}

	public ProcessFileProperty getDocProperty() {
		return docProperty;
	}

	public String nullToEmpty(String text) {
		if (text == null) {
			return "";
		} else {
			return text.trim();
		}
	}

	public String getPublicName() {
		return JecnUtil.custOmResourceMap.get("public");
	}

	public String getSecretName() {
		return JecnUtil.custOmResourceMap.get("secret");
	}

	/**
	 * 获得操作说明中配置获得公开和秘密的名称 TODO
	 * 
	 * @return
	 */
	public String getPubOrSec() {
		return "";
	}

	public WordGraph getLogoImage(float width, float height) {
		Image logo = new Image(docProperty.getLogoImage());
		logo.setSize(width, height);
		return logo;
	}

	public WordGraph getImage(float width, float height, String imageName) {
		Image logo = new Image(JecnPath.APP_PATH + "images/" + imageName);
		logo.setSize(width, height);
		return logo;
	}

	public String dateToStr(Date date) {
		if (date == null) {
			return "";
		}
		return JecnUtil.DateToStr(date, "yyyy-MM-dd");
	}

	public String dateToStr(Date date, String formate) {
		if (date == null) {
			return "";
		}
		return JecnUtil.DateToStr(date, formate);
	}

}
