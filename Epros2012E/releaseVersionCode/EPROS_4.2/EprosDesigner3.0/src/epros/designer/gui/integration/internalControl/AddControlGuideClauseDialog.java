package epros.designer.gui.integration.internalControl;

import java.util.Date;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;

/**
 * 新建条款
 * 
 * @author Administrator
 * 
 */
public class AddControlGuideClauseDialog extends JecnClauseDialog {
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger
			.getLogger(AddControlGuideClauseDialog.class);
	/** 树节点 */
	private JecnTreeNode pNode = null;
	/** 树 */
	private JTree jTree = null;

	public AddControlGuideClauseDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		initListener();
	}

	private void initListener() {
		//给叉添加取消提示事件
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButtonAction();
				return false;
			}
		});	
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("newStanClause");
	}

	@Override
	public void saveData() {
		// 向内控指引知识库Bean中添加数据
		JecnControlGuide jecnControlGuide = new JecnControlGuide();
		// 条款名称
		jecnControlGuide.setName(getName());
		jecnControlGuide.setDescription(getContent());
		// 创建人
		jecnControlGuide.setCreatePerson(JecnConstants.loginBean.getJecnUser()
				.getPeopleId());
		// 更新人
		jecnControlGuide.setUpdatePerson(JecnConstants.loginBean.getJecnUser()
				.getPeopleId());
		// 父ID
		jecnControlGuide.setParentId(pNode.getJecnTreeBean().getId());
		// 序号
		jecnControlGuide.setSort(JecnTreeCommon.getMaxSort(pNode));
		// 类型，0目录，1条款
		jecnControlGuide.setType(1);
		// 目录创建时间
		jecnControlGuide.setCreateTime(new Date());
		// 更新时间
		jecnControlGuide.setUpdateTime(new Date());
		jecnControlGuide.setProjectId(JecnConstants.projectId);
		// 向数据库表中添加数据
		try {
			Long id = ConnectionPool.getControlGuideAction()
					.addJecnControlGuideDir(jecnControlGuide);
			// 向树节点添加内控指引知识库条款
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(jecnControlGuide.getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setContent(getContent());
			jecnTreeBean.setTreeNodeType(TreeNodeType.innerControlClause);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddControlGuideClauseDialog saveData is error!", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name,
				TreeNodeType.innerControlClause);
	}

	@Override
	protected void cancelButtonAction() {
		if(dataIsChange()){
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			//是
			if(optionTig==2){
				okButtonAction();
			//否
			}else if(optionTig==1){
				this.dispose();
			}
		}else{
			this.dispose();
		}
	}
	/**
	 * 内容是否变化
	 * @author hyl
	 * @return 是返回true 否则false
	 */
	private boolean dataIsChange(){
		
		//编辑内容不匹配就是变化
		if(this.getNameTextField().getText() != null&&!this.getNameTextField().getText().trim().equals("")){
			return true;
		}
		if(this.getCenterText().getText() != null&&!this.getCenterText().getText().trim().equals("")){
			return true;
		}
		return false;
	}
	

}
