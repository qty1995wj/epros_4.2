package epros.draw.visio;

import java.util.List;

/**
 * visio 图形页面数据
 * 
 * @author fuzhh 2013-7-3
 * 
 */
public class JecnVisioPageDataBean {

	/** 页面的nameU属性 */
	private String pageNameU;
	/** 页面的name属性 */
	private String pageName;
	/** 页面图形的数据集合 */
	private List<JecnFlowStructureImage> flowStructureImageList;

	public String getPageNameU() {
		return pageNameU;
	}

	public void setPageNameU(String pageNameU) {
		this.pageNameU = pageNameU;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public List<JecnFlowStructureImage> getFlowStructureImageList() {
		return flowStructureImageList;
	}

	public void setFlowStructureImageList(
			List<JecnFlowStructureImage> flowStructureImageList) {
		this.flowStructureImageList = flowStructureImageList;
	}

}
