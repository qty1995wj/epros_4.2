/**
 * SSOWebServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cetc10.sso;

import com.jecn.epros.server.action.web.login.ad.cetc10.JecnCetc10AfterItem;

public class SSOWebServiceLocator extends org.apache.axis.client.Service implements com.jecn.webservice.cetc10.sso.SSOWebService {

    public SSOWebServiceLocator() {
    }


    public SSOWebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SSOWebServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SSOWebServiceSoap
    private java.lang.String SSOWebServiceSoap_address = JecnCetc10AfterItem.SSO_URL;

    public java.lang.String getSSOWebServiceSoapAddress() {
        return SSOWebServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SSOWebServiceSoapWSDDServiceName = "SSOWebServiceSoap";

    public java.lang.String getSSOWebServiceSoapWSDDServiceName() {
        return SSOWebServiceSoapWSDDServiceName;
    }

    public void setSSOWebServiceSoapWSDDServiceName(java.lang.String name) {
        SSOWebServiceSoapWSDDServiceName = name;
    }

    public com.jecn.webservice.cetc10.sso.SSOWebServiceSoap getSSOWebServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SSOWebServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSSOWebServiceSoap(endpoint);
    }

    public com.jecn.webservice.cetc10.sso.SSOWebServiceSoap getSSOWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.jecn.webservice.cetc10.sso.SSOWebServiceSoapStub _stub = new com.jecn.webservice.cetc10.sso.SSOWebServiceSoapStub(portAddress, this);
            _stub.setPortName(getSSOWebServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSSOWebServiceSoapEndpointAddress(java.lang.String address) {
        SSOWebServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.jecn.webservice.cetc10.sso.SSOWebServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.jecn.webservice.cetc10.sso.SSOWebServiceSoapStub _stub = new com.jecn.webservice.cetc10.sso.SSOWebServiceSoapStub(new java.net.URL(SSOWebServiceSoap_address), this);
                _stub.setPortName(getSSOWebServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SSOWebServiceSoap".equals(inputPortName)) {
            return getSSOWebServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "SSOWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "SSOWebServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SSOWebServiceSoap".equals(portName)) {
            setSSOWebServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
