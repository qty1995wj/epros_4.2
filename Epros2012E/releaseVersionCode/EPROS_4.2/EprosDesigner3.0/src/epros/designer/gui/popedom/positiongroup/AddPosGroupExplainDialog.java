package epros.designer.gui.popedom.positiongroup;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.popedom.organization.OrgResponseDialog;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 添加岗位组说明
 * 
 * @author 2018-11-6
 * 
 */
public class AddPosGroupExplainDialog extends JecnDialog {

	private static Logger log = Logger.getLogger(AddPosGroupExplainDialog.class);

	/** 岗位组说明Area */
	private JecnTextArea PosGroupExplainArea = new JecnTextArea();
	/** 岗位组说明滚动面板 */
	private JScrollPane PosGroupExplainScrollPane = new JScrollPane(PosGroupExplainArea);

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 验证提示信息 */
	private JLabel verfyLabel = new JLabel();

	private JecnTreeNode selectNode = null;
	// 说明
	String posGroupExplain = "";

	public AddPosGroupExplainDialog(JecnTreeNode node) {
		selectNode = node;
		this.setSize(450, 350);
		this.setLocationRelativeTo(null);
		this.setTitle(JecnProperties.getValue("posGroupExplain"));
		initData();
		initAssembly();
		initEvent();
	}

	private void initAssembly() {
		this.setLayout(new GridBagLayout());
		// 岗位组说明
		PosGroupExplainArea.setBorder(null);
		PosGroupExplainScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		PosGroupExplainScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		PosGroupExplainScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		verfyLabel.setForeground(Color.red);
		// 换行
		PosGroupExplainArea.setLineWrap(true);

		GridBagConstraints c = null;
		Insets insets = new Insets(5, 8, 5, 8);

		// 组织职责显示框
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(PosGroupExplainScrollPane, c);
		JecnPanel buttonPanel = new JecnPanel();
		// 按钮
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLabel);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

	}

	private void initEvent() {
		// 确定
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				if (checkUpdate() && submit()) {
					// 关闭窗体
					return true;
				}
				return true;
			}
		});
	}

	private void initData() {
		try {
			posGroupExplain = ConnectionPool.getPosGroup().getPosGroupExplain(selectNode.getJecnTreeBean().getId());
			PosGroupExplainArea.setText(posGroupExplain != null ? posGroupExplain : "");
		} catch (SQLException e) {
			e.printStackTrace();
			log.error("initData");
		} catch (IOException e) {
			log.error("initData");
		}
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	private boolean checkUpdate() {
		String text = PosGroupExplainArea.getText();
		posGroupExplain = posGroupExplain == null ? "" : posGroupExplain;
		text = text == null ? "" : text;
		if (posGroupExplain.equals(text)) {
			return false;
		}
		return true;
	}

	private void okButPerformed() {
		String posGroupExplain = PosGroupExplainArea.getText();
		/*
		 * String error = JecnUserCheckUtil.checkAreaNote(posGroupExplain); if
		 * (StringUtils.isNotBlank(error)) { verfyLabel.setText(error); return;
		 * }
		 */
		if (checkUpdate()) {
			try {
				boolean isUpdate = ConnectionPool.getPosGroup().updatePosGroupExplain(
						selectNode.getJecnTreeBean().getId(), posGroupExplain, JecnUtil.getUserId());
				if (isUpdate) {
					this.dispose();
				} else {
					JecnOptionPane.showMessageDialog(this, "保存失败！");
				}
			} catch (Exception e) {
				log.error("", e);
				JecnOptionPane.showMessageDialog(this, "保存失败！");
			}
		} else {
			// 关闭窗体
			this.dispose();
		}
	}

	/***
	 * 取消
	 */
	private void cancelButPerformed() {
		if (checkUpdate() && submit()) {
			// 关闭窗体
			this.dispose();
		} else {
			// 关闭窗体
			this.dispose();
		}
	}

	private boolean submit() {
		int optionTig = this.dialogCloseBeforeMsgTipAction();
		if (optionTig == 2) {// 是
			okButPerformed();
			return false;
		} else if (optionTig == 1) {// 否
			return true;
		} else {// 取消
			return false;
		}
	}

}
