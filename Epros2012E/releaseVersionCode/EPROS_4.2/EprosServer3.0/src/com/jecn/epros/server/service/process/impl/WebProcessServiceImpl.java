package com.jecn.epros.server.service.process.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.download.ProcessMapDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.process.JecnFlowRecord;
import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.process.JecnMainFlow;
import com.jecn.epros.server.bean.process.JecnRefIndicators;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.ProcessInterfacesDescriptionBean;
import com.jecn.epros.server.bean.process.driver.JecnFlowDriverTimeBase;
import com.jecn.epros.server.bean.process.temp.TmpKpiShowValues;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.bean.system.JecnUserTotal;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IFlowOrgImageDao;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPositionFigInfoDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IFlowToolDao;
import com.jecn.epros.server.dao.process.IJecnUserTotalDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.system.IProcessRuleTypeDao;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.download.word.ProcessDownDataUtil;
import com.jecn.epros.server.download.word.SelectDownloadProcess;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.process.buss.FlowKpiTitleAndValues;
import com.jecn.epros.server.service.process.util.MyFlowSqlUtil;
import com.jecn.epros.server.util.JecnCommonSqlConstant;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.KeyValueBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.RoleBean;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.process.ActiveModeBean;
import com.jecn.epros.server.webBean.process.ActiveRoleBean;
import com.jecn.epros.server.webBean.process.JecnRoleProcessBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;
import com.jecn.epros.server.webBean.process.ProcessAttributeWebBean;
import com.jecn.epros.server.webBean.process.ProcessBaseInfoBean;
import com.jecn.epros.server.webBean.process.ProcessMapBaseInfoBean;
import com.jecn.epros.server.webBean.process.ProcessMapWebBean;
import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;
import com.jecn.epros.server.webBean.process.ProcessSearchBean;
import com.jecn.epros.server.webBean.process.ProcessShowFile;
import com.jecn.epros.server.webBean.process.ProcessSystemListBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.process.RelatedProcessRuleStandardBean;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.standard.StandardWebBean;
import com.jecn.viewdoc.MSOffice.OfficeTools;
import com.jecn.viewdoc.convert.ViewDocsManager;

@Transactional
public class WebProcessServiceImpl extends AbsBaseService<JecnFlowStructureT, Long> implements IWebProcessService {
	private Logger log = Logger.getLogger(FlowStructureServiceImpl.class);
	private IFlowStructureDao flowStructureDao;
	private IFileDao fileDao;
	private IOrgAccessPermissionsDao orgAccessPermissionsDao;
	private IPositionAccessPermissionsDao positionAccessPermissionsDao;
	private IProcessRuleTypeDao processRuleTypeDao;
	private IProcessBasicInfoDao processBasicDao;
	private IPersonDao userDao;
	private IFlowOrgImageDao flowOrgImageDao;
	/** 岗位基本信息 */
	private IPositionFigInfoDao positionFigInfoDao;
	private IJecnConfigItemDao configItemDao;
	private IJecnDocControlDao docControlDao;
	private IProcessKPIDao processKPIDao;
	private IProcessRecordDao processRecordDao;
	/** 记录查阅流程图 下载操作说明 */
	private IJecnUserTotalDao userTotalDao;
	private IFlowToolDao flowToolDao;
	@Resource
	private IPersonService personService;
	@Resource
	private IFileService fileService;

	public IFlowToolDao getFlowToolDao() {
		return flowToolDao;
	}

	public void setFlowToolDao(IFlowToolDao flowToolDao) {
		this.flowToolDao = flowToolDao;
	}

	public IPersonDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IPersonDao userDao) {
		this.userDao = userDao;
	}

	public IFlowOrgImageDao getFlowOrgImageDao() {
		return flowOrgImageDao;
	}

	public void setFlowOrgImageDao(IFlowOrgImageDao flowOrgImageDao) {
		this.flowOrgImageDao = flowOrgImageDao;
	}

	public IProcessBasicInfoDao getProcessBasicDao() {
		return processBasicDao;
	}

	public void setProcessBasicDao(IProcessBasicInfoDao processBasicDao) {
		this.processBasicDao = processBasicDao;
	}

	public IProcessRuleTypeDao getProcessRuleTypeDao() {
		return processRuleTypeDao;
	}

	public void setProcessRuleTypeDao(IProcessRuleTypeDao processRuleTypeDao) {
		this.processRuleTypeDao = processRuleTypeDao;
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	public IOrgAccessPermissionsDao getOrgAccessPermissionsDao() {
		return orgAccessPermissionsDao;
	}

	public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
		this.orgAccessPermissionsDao = orgAccessPermissionsDao;
	}

	public IPositionAccessPermissionsDao getPositionAccessPermissionsDao() {
		return positionAccessPermissionsDao;
	}

	public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
		this.positionAccessPermissionsDao = positionAccessPermissionsDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	@Override
	public ProcessBaseInfoBean getProcessBaseInfoBean(Long processId, boolean isPub) throws Exception {
		ProcessBaseInfoBean processBaseInfoBean = new ProcessBaseInfoBean();
		processBaseInfoBean.setFlowId(processId);
		if (isPub) {
			JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(processId);
			if (jecnFlowStructure == null) {
				return null;
			}
			// 流程名称
			if (StringUtils.isNotBlank(jecnFlowStructure.getFlowName())) {
				processBaseInfoBean.setFlowName(jecnFlowStructure.getFlowName());
			} else {
				processBaseInfoBean.setFlowName("");
			}
			// 流程编号
			if (StringUtils.isNotBlank(jecnFlowStructure.getFlowIdInput())) {
				processBaseInfoBean.setFlowIdInput(jecnFlowStructure.getFlowIdInput());
			} else {
				processBaseInfoBean.setFlowIdInput("");
			}
			// 密集
			if (jecnFlowStructure.getIsPublic() != null) {
				processBaseInfoBean.setIsPublic(jecnFlowStructure.getIsPublic().intValue());
			}
			JecnFlowBasicInfo jecnFlowBasicInfo = flowStructureDao.findJecnFlowBasicInfoById(processId);
			if (jecnFlowBasicInfo != null) {
				// 流程客户
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getFlowCustom())) {
					processBaseInfoBean.setFlowCustom(jecnFlowBasicInfo.getFlowCustom());
				} else {
					processBaseInfoBean.setFlowCustom("");
				}
				// 起始活动
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getFlowStartpoint())) {
					processBaseInfoBean.setFlowStartpoint(jecnFlowBasicInfo.getFlowStartpoint());
				} else {
					processBaseInfoBean.setFlowStartpoint("");
				}
				// 终止活动
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getFlowEndpoint())) {
					processBaseInfoBean.setFlowEndpoint(jecnFlowBasicInfo.getFlowEndpoint());
				} else {
					processBaseInfoBean.setFlowEndpoint("");
				}
				// 流程输入
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getInput())) {
					processBaseInfoBean.setInput(jecnFlowBasicInfo.getInput().replaceAll("\n", "<br>").replaceAll(" ",
							"&nbsp;"));
				} else {
					processBaseInfoBean.setInput("");
				}
				// 流程输出
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getOuput())) {
					processBaseInfoBean.setOuput(jecnFlowBasicInfo.getOuput().replaceAll("\n", "<br>").replaceAll(" ",
							"&nbsp;"));
				} else {
					processBaseInfoBean.setOuput("");
				}
				// 流程类别
				if (jecnFlowBasicInfo.getTypeId() != null) {
					ProceeRuleTypeBean proceeRuleTypeBean = this.processRuleTypeDao.get(jecnFlowBasicInfo.getTypeId());
					if (proceeRuleTypeBean != null) {
						processBaseInfoBean.setFlowTypeName(proceeRuleTypeBean.getTypeName());
					}
				}

				// 有效期
				if (jecnFlowBasicInfo.getExpiry() != null) {
					processBaseInfoBean.setExpiry(jecnFlowBasicInfo.getExpiry().toString());
				} else {
					// 永久
					processBaseInfoBean.setExpiry(JecnUtil.getValue("permanent"));
				}

				if ("1".equals(JecnContants.getValue("fictionPeople"))) { // 流程拟制人
					// 流程拟制人ID 和名称
					if (jecnFlowBasicInfo.getFictionPeopleId() != null) {
						processBaseInfoBean.setFictionPeopleId(jecnFlowBasicInfo.getFictionPeopleId());
						JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getFictionPeopleId());
						if (jecnUser != null) {
							processBaseInfoBean.setFictionPeopleName(jecnUser.getTrueName());
						}
					}
					processBaseInfoBean.setShowFiction(true);
				}
				if ("1".equals(JecnContants.getValue("guardianPeople"))) { // 流程监护人
					// 流程监护人ID 和 名称
					if (jecnFlowBasicInfo.getGuardianId() != null) {
						processBaseInfoBean.setGuardianId(jecnFlowBasicInfo.getGuardianId());
						JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getGuardianId());
						if (jecnUser != null) {
							processBaseInfoBean.setGuardianName(jecnUser.getTrueName());
						}
					}
					processBaseInfoBean.setShowGuardian(true);
				}

				// 责任人ID
				Object[] obj = processBasicDao.getFlowRelateOrgIds(processId, true);
				if (obj != null && obj[0] != null && obj[1] != null) {
					// 责任部门Id
					processBaseInfoBean.setOrgId(Long.valueOf(obj[0].toString()));
					// 责任部门
					processBaseInfoBean.setOrgName(obj[1].toString());
				}
				// 责任人类型
				if (jecnFlowBasicInfo.getTypeResPeople() != null) {
					processBaseInfoBean.setTypeResPeople(jecnFlowBasicInfo.getTypeResPeople());
				}

				// 责任人ID
				processBaseInfoBean.setResPeopleId(jecnFlowBasicInfo.getResPeopleId());
				if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 0
						&& jecnFlowBasicInfo.getResPeopleId() != null && jecnFlowBasicInfo.getResPeopleId() != -1) {
					JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnUser != null) {
						processBaseInfoBean.setResPeopleName(jecnUser.getTrueName());
					}
				} else if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 1
						&& jecnFlowBasicInfo.getResPeopleId() != null && jecnFlowBasicInfo.getResPeopleId() != -1) {
					JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnFlowOrgImage != null) {
						processBaseInfoBean.setResPeopleName(jecnFlowOrgImage.getFigureText());
					}
				}
			} else {
				processBaseInfoBean.setFlowCustom("");
				processBaseInfoBean.setFlowStartpoint("");
				processBaseInfoBean.setFlowEndpoint("");
				processBaseInfoBean.setInput("");
				processBaseInfoBean.setOuput("");
				processBaseInfoBean.setFlowTypeName("");
				processBaseInfoBean.setOrgName("");
				processBaseInfoBean.setResPeopleName("");
			}

			// 附件
			if (jecnFlowBasicInfo.getFileId() != null) {
				JecnFileBean jecnFileBean = this.fileDao.findJecnFileBeanById(jecnFlowBasicInfo.getFileId());
				if (jecnFileBean != null) {
					// 附件ID
					processBaseInfoBean.setFileId(jecnFileBean.getFileID());
					// 附件名称
					if (StringUtils.isNotBlank(jecnFileBean.getFileName())) {
						processBaseInfoBean.setFileName(jecnFileBean.getFileName());
					} else {
						processBaseInfoBean.setFileName("");
					}
				} else {
					processBaseInfoBean.setFileId(null);
					processBaseInfoBean.setFileName("");
				}
			}
			// 所属流程
			Long perFlowId = jecnFlowStructure.getPerFlowId();
			if (perFlowId != null) {
				jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(perFlowId);
				if (jecnFlowStructure != null) {
					processBaseInfoBean.setPerFlowId(perFlowId);
					// 所属流程名称
					if (jecnFlowStructure.getFlowName() != null) {
						processBaseInfoBean.setPerFlowName(jecnFlowStructure.getFlowName());
						processBaseInfoBean.setPreFlowType(jecnFlowStructure.getIsFlow() == 1 ? "process"
								: "processMap");
					} else {
						processBaseInfoBean.setPerFlowName("");
						processBaseInfoBean.setPreFlowType("");
					}
				} else {
					processBaseInfoBean.setPerFlowName("");
				}
			}
		} else {
			JecnFlowStructureT jecnFlowStructure = flowStructureDao.get(processId);
			if (jecnFlowStructure == null) {
				return null;
			}
			// 流程名称
			if (StringUtils.isNotBlank(jecnFlowStructure.getFlowName())) {
				processBaseInfoBean.setFlowName(jecnFlowStructure.getFlowName());
			} else {
				processBaseInfoBean.setFlowName("");
			}
			// 流程编号
			if (StringUtils.isNotBlank(jecnFlowStructure.getFlowIdInput())) {
				processBaseInfoBean.setFlowIdInput(jecnFlowStructure.getFlowIdInput());
			} else {
				processBaseInfoBean.setFlowIdInput("");
			}
			// 密集
			if (jecnFlowStructure.getIsPublic() != null) {
				processBaseInfoBean.setIsPublic(jecnFlowStructure.getIsPublic().intValue());
			}
			JecnFlowBasicInfoT jecnFlowBasicInfo = processBasicDao.get(processId);
			if (jecnFlowBasicInfo != null) {
				// 流程客户
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getFlowCustom())) {
					processBaseInfoBean.setFlowCustom(jecnFlowBasicInfo.getFlowCustom());
				} else {
					processBaseInfoBean.setFlowCustom("");
				}
				// 起始活动
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getFlowStartpoint())) {
					processBaseInfoBean.setFlowStartpoint(jecnFlowBasicInfo.getFlowStartpoint());
				} else {
					processBaseInfoBean.setFlowStartpoint("");
				}
				// 终止活动
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getFlowEndpoint())) {
					processBaseInfoBean.setFlowEndpoint(jecnFlowBasicInfo.getFlowEndpoint());
				} else {
					processBaseInfoBean.setFlowEndpoint("");
				}
				// // 流程输入
				// if (StringUtils.isNotBlank(jecnFlowBasicInfo.getInput())) {
				// processBaseInfoBean.setInput(jecnFlowBasicInfo.getInput());
				// } else {
				// processBaseInfoBean.setInput("");
				// }
				// // 流程输出
				// if (StringUtils.isNotBlank(jecnFlowBasicInfo.getOuput())) {
				// processBaseInfoBean.setOuput(jecnFlowBasicInfo.getOuput());
				// } else {
				// processBaseInfoBean.setOuput("");
				// }
				// 流程输入
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getInput())) {
					processBaseInfoBean.setInput(jecnFlowBasicInfo.getInput().replaceAll("\n", "<br>").replaceAll(" ",
							"&nbsp;"));
				} else {
					processBaseInfoBean.setInput("");
				}
				// 流程输出
				if (StringUtils.isNotBlank(jecnFlowBasicInfo.getOuput())) {
					processBaseInfoBean.setOuput(jecnFlowBasicInfo.getOuput().replaceAll("\n", "<br>").replaceAll(" ",
							"&nbsp;"));
				} else {
					processBaseInfoBean.setOuput("");
				}
				// 流程类别
				if (jecnFlowBasicInfo.getTypeId() != null) {
					ProceeRuleTypeBean proceeRuleTypeBean = this.processRuleTypeDao.get(jecnFlowBasicInfo.getTypeId());
					if (proceeRuleTypeBean != null) {
						processBaseInfoBean.setFlowTypeName(proceeRuleTypeBean.getTypeName());
					}
				}
				// 有效期
				if (jecnFlowBasicInfo.getExpiry() != null) {
					processBaseInfoBean.setExpiry(jecnFlowBasicInfo.getExpiry().toString());
				} else {
					// 永久
					processBaseInfoBean.setExpiry(JecnUtil.getValue("permanent"));
				}
				if ("1".equals(JecnContants.getValue("fictionPeople"))) { // 流程拟制人
					// 流程拟制人ID 和名称
					if (jecnFlowBasicInfo.getFictionPeopleId() != null) {
						processBaseInfoBean.setFictionPeopleId(jecnFlowBasicInfo.getFictionPeopleId());
						JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getFictionPeopleId());
						if (jecnUser != null) {
							processBaseInfoBean.setFictionPeopleName(jecnUser.getTrueName());
						}
					}
					processBaseInfoBean.setShowFiction(true);
				}

				if ("1".equals(JecnContants.getValue("guardianPeople"))) { // 流程监护人
					// 流程监护人ID 和 名称
					if (jecnFlowBasicInfo.getGuardianId() != null) {
						processBaseInfoBean.setGuardianId(jecnFlowBasicInfo.getGuardianId());
						JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getGuardianId());
						if (jecnUser != null) {
							processBaseInfoBean.setGuardianName(jecnUser.getTrueName());
						}
					}
					processBaseInfoBean.setShowGuardian(true);
				}
				// 责任人ID
				Object[] obj = processBasicDao.getFlowRelateOrgIds(processId, false);
				if (obj != null && obj[0] != null && obj[1] != null) {
					// 责任部门Id
					processBaseInfoBean.setOrgId(Long.valueOf(obj[0].toString()));
					// 责任部门
					processBaseInfoBean.setOrgName(obj[1].toString());
				}
				// 责任人类型
				if (jecnFlowBasicInfo.getTypeResPeople() != null) {
					processBaseInfoBean.setTypeResPeople(jecnFlowBasicInfo.getTypeResPeople());
				}
				// 责任人ID
				processBaseInfoBean.setResPeopleId(jecnFlowBasicInfo.getResPeopleId());
				if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 0
						&& jecnFlowBasicInfo.getResPeopleId() != null && jecnFlowBasicInfo.getResPeopleId() != -1) {
					JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnUser != null) {
						processBaseInfoBean.setResPeopleName(jecnUser.getTrueName());
					}
				} else if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 1
						&& jecnFlowBasicInfo.getResPeopleId() != null && jecnFlowBasicInfo.getResPeopleId() != -1) {
					JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnFlowOrgImage != null) {
						processBaseInfoBean.setResPeopleName(jecnFlowOrgImage.getFigureText());
					}
				}
			} else {
				processBaseInfoBean.setFlowCustom("");
				processBaseInfoBean.setFlowStartpoint("");
				processBaseInfoBean.setFlowEndpoint("");
				processBaseInfoBean.setInput("");
				processBaseInfoBean.setOuput("");
				processBaseInfoBean.setFlowTypeName("");
				processBaseInfoBean.setOrgName("");
				processBaseInfoBean.setResPeopleName("");
			}
			// 附件
			if (jecnFlowBasicInfo.getFileId() != null) {
				JecnFileBeanT jecnFileBean = this.fileDao.get(jecnFlowBasicInfo.getFileId());
				if (jecnFileBean != null) {
					// 附件ID
					processBaseInfoBean.setFileId(jecnFileBean.getFileID());
					// 附件名称
					if (StringUtils.isNotBlank(jecnFileBean.getFileName())) {
						processBaseInfoBean.setFileName(jecnFileBean.getFileName());
					} else {
						processBaseInfoBean.setFileName("");
					}
				} else {
					processBaseInfoBean.setFileId(null);
					processBaseInfoBean.setFileName("");
				}
			}
			// 所属流程
			Long perFlowId = jecnFlowStructure.getPerFlowId();
			if (perFlowId != null) {
				jecnFlowStructure = flowStructureDao.get(perFlowId);
				if (jecnFlowStructure != null) {
					processBaseInfoBean.setPerFlowId(perFlowId);
					// 所属流程名称
					if (jecnFlowStructure.getFlowName() != null) {
						processBaseInfoBean.setPerFlowName(jecnFlowStructure.getFlowName());
					} else {
						processBaseInfoBean.setPerFlowName("");
					}
				} else {
					processBaseInfoBean.setPerFlowName("");
				}
			}

		}
		// 支持工具
		processBaseInfoBean.setListTools(flowStructureDao.getListTools(processId, isPub));

		return processBaseInfoBean;
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	@Override
	public List<ProcessActiveWebBean> getProcessActiveBeanList(Long processId, boolean isPub) throws Exception {
		// 获取活动输入输出说明配置
		String activityNote = configItemDao.selectValue(1, ConfigItemPartMapMark.activityFileShow.toString());

		List<ProcessActiveWebBean> listResult = new ArrayList<ProcessActiveWebBean>();
		// 活动明细
		List<Object[]> listActiveAll = flowStructureDao.getProcessActiveShowByFlowId(processId, isPub);
		// 活动排序
		JecnUtil.getActivityObjSort(listActiveAll);
		// 活动输入、操作规范文件
		List<Object[]> listActiveInputAndOperation = flowStructureDao.findAllActiveInputAndOperationByFlowId(processId,
				isPub);
		// 活动输出
		List<Object[]> listActiveMode = flowStructureDao.findAllActiveOutputByFlowId(processId, isPub);
		// 活动的样例
		List<Object[]> listActiveTemplet = flowStructureDao.findAllActiveTempletByFlowId(processId, isPub);
		for (Object[] objActive : listActiveAll) {
			if (objActive == null || objActive[0] == null) {
				continue;
			}
			Object activeKeyId = objActive[0];
			ProcessActiveWebBean processActiveBean = new ProcessActiveWebBean();
			// 活动输入输出说明配置
			processActiveBean.setActivityNoteShow(activityNote);
			// 活动ID
			processActiveBean.setActiveFigureId(Long.valueOf(activeKeyId.toString()));
			// 活动编号
			if (objActive[1] != null) {
				processActiveBean.setActiveId(objActive[1].toString());
			} else {
				processActiveBean.setActiveId("");
			}
			// 活动名称
			if (objActive[2] != null) {
				processActiveBean.setActiveName(objActive[2].toString());
			} else {
				processActiveBean.setActiveName("");
			}
			// 输入输出说明
			processActiveBean.setActivityInput(objActive[6] != null ? objActive[6].toString() : "");
			processActiveBean.setActivityOutput(objActive[7] != null ? objActive[7].toString() : "");
			// 活动输入
			List<FileWebBean> listInput = new ArrayList<FileWebBean>();

			// 活动操作规范
			List<FileWebBean> listOperation = new ArrayList<FileWebBean>();
			for (Object[] obj : listActiveInputAndOperation) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[4] == null) {
					continue;
				}
				// 活动下的输入和操作规范
				if (activeKeyId.toString().equals(obj[1].toString())) {
					FileWebBean fileCommonBean = new FileWebBean();
					// 文件ID
					fileCommonBean.setFileId(Long.valueOf(obj[2].toString()));
					// 文件名称
					if (obj[3] != null) {
						fileCommonBean.setFileName(obj[3].toString());
					} else {
						fileCommonBean.setFileName("");
					}
					if ("0".equals(obj[4].toString())) {
						listInput.add(fileCommonBean);
					} else if ("1".equals(obj[4].toString())) {
						listOperation.add(fileCommonBean);
					}
				}
			}
			// 活动输入
			processActiveBean.setListInput(listInput);
			// 活动操作规范
			processActiveBean.setListOperation(listOperation);
			// 活动输出
			List<ActiveModeBean> listMode = new ArrayList<ActiveModeBean>();
			for (Object[] objMode : listActiveMode) {
				if (objMode == null || objMode[0] == null || objMode[1] == null || objMode[2] == null) {
					continue;
				}
				if (activeKeyId.toString().equals(objMode[1].toString())) {
					ActiveModeBean activeModeBean = new ActiveModeBean();
					FileWebBean fileMode = new FileWebBean();
					// 文件Id
					fileMode.setFileId(Long.valueOf(objMode[2].toString()));
					// 文件名称
					if (objMode[3] != null) {
						fileMode.setFileName(objMode[3].toString());
					} else {
						fileMode.setFileName("");
					}
					activeModeBean.setModeFile(fileMode);
					// 样例
					List<FileWebBean> listTemplet = new ArrayList<FileWebBean>();
					for (Object[] obj : listActiveTemplet) {
						if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
							continue;
						}
						if (obj[1].toString().equals(objMode[0].toString())) {
							FileWebBean fileCommonBean = new FileWebBean();
							// 文件Id
							fileCommonBean.setFileId(Long.valueOf(obj[2].toString()));
							// 文件名称
							if (obj[3] != null) {
								fileCommonBean.setFileName(obj[3].toString());
							} else {
								fileCommonBean.setFileName("");
							}
							listTemplet.add(fileCommonBean);
						}
					}
					activeModeBean.setListTemplet(listTemplet);
					listMode.add(activeModeBean);
				}
			}
			processActiveBean.setListMode(listMode);
			listResult.add(processActiveBean);
		}
		return listResult;
	}

	@Override
	public ProcessAttributeWebBean getProcessAttributeWebBean(Long processId, boolean isPub) throws Exception {
		ProcessAttributeWebBean processAttributeWebBean = new ProcessAttributeWebBean();
		// 流程ID
		processAttributeWebBean.setFlowId(processId);
		if (isPub) {
			JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(processId);
			if (jecnFlowStructure == null) {
				return null;
			}
			// 流程名称
			processAttributeWebBean.setFlowName(jecnFlowStructure.getFlowName());
			JecnFlowBasicInfo jecnFlowBasicInfo = flowStructureDao.findJecnFlowBasicInfoById(processId);
			if (jecnFlowBasicInfo != null) {
				// 流程类别
				if (jecnFlowBasicInfo.getTypeId() != null) {
					ProceeRuleTypeBean proceeRuleTypeBean = this.processRuleTypeDao.get(jecnFlowBasicInfo.getTypeId());
					if (proceeRuleTypeBean != null) {
						processAttributeWebBean.setFlowTypeName(proceeRuleTypeBean.getTypeName());
					}
				}
				// 责任人ID
				Object[] obj = processBasicDao.getFlowRelateOrgIds(processId, true);
				if (obj != null && obj[0] != null && obj[1] != null) {
					// 责任部门Id
					processAttributeWebBean.setOrgId(Long.valueOf(obj[0].toString()));
					// 责任部门
					processAttributeWebBean.setOrgName(obj[1].toString());
				}
				// 责任人类型
				if (jecnFlowBasicInfo.getTypeResPeople() != null) {
					processAttributeWebBean.setTypeResPeople(jecnFlowBasicInfo.getTypeResPeople());
				}

				// 责任人ID
				processAttributeWebBean.setResPeopleId(jecnFlowBasicInfo.getResPeopleId());
				if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 0
						&& jecnFlowBasicInfo.getResPeopleId() != null && jecnFlowBasicInfo.getResPeopleId() != -1) {
					JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnUser != null) {
						processAttributeWebBean.setResPeopleName(jecnUser.getTrueName());
					}
				} else if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 1
						&& jecnFlowBasicInfo.getResPeopleId() != null && jecnFlowBasicInfo.getResPeopleId() != -1) {
					JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnFlowOrgImage != null) {
						processAttributeWebBean.setResPeopleName(jecnFlowOrgImage.getFigureText());
					}
				}
			}
			// 所属流程
			Long perFlowId = jecnFlowStructure.getPerFlowId();
			if (perFlowId != null) {
				jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(perFlowId);
				if (jecnFlowStructure != null) {
					processAttributeWebBean.setPerFlowId(perFlowId);
					// 所属流程名称
					if (jecnFlowStructure.getFlowName() != null) {
						processAttributeWebBean.setPerFlowName(jecnFlowStructure.getFlowName());
					} else {
						processAttributeWebBean.setPerFlowName("");
					}
				} else {
					processAttributeWebBean.setPerFlowName("");
				}
			}
		} else {
			JecnFlowStructureT jecnFlowStructure = flowStructureDao.get(processId);
			if (jecnFlowStructure == null) {
				return null;
			}
			// 流程名称
			processAttributeWebBean.setFlowName(jecnFlowStructure.getFlowName());
			JecnFlowBasicInfoT jecnFlowBasicInfo = this.processBasicDao.get(processId);
			if (jecnFlowBasicInfo != null) {
				// 流程类别
				if (jecnFlowBasicInfo.getTypeId() != null) {
					ProceeRuleTypeBean proceeRuleTypeBean = this.processRuleTypeDao.get(jecnFlowBasicInfo.getTypeId());
					if (proceeRuleTypeBean != null) {
						processAttributeWebBean.setFlowTypeName(proceeRuleTypeBean.getTypeName());
					}
				}
				// 责任人ID
				Object[] obj = processBasicDao.getFlowRelateOrgIds(processId, true);
				if (obj != null && obj[0] != null && obj[1] != null) {
					// 责任部门Id
					processAttributeWebBean.setOrgId(Long.valueOf(obj[0].toString()));
					// 责任部门
					processAttributeWebBean.setOrgName(obj[1].toString());
				}
				// 责任人类型
				if (jecnFlowBasicInfo.getTypeResPeople() != null) {
					processAttributeWebBean.setTypeResPeople(jecnFlowBasicInfo.getTypeResPeople());
				}
				// 责任人ID
				processAttributeWebBean.setResPeopleId(jecnFlowBasicInfo.getResPeopleId());
				if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 0) {
					JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnUser != null) {
						processAttributeWebBean.setResPeopleName(jecnUser.getTrueName());
					}
				} else if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 1) {
					JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(jecnFlowBasicInfo.getResPeopleId());
					if (jecnFlowOrgImage != null) {
						processAttributeWebBean.setResPeopleName(jecnFlowOrgImage.getFigureText());
					}
				}
			}
			// 所属流程
			Long perFlowId = jecnFlowStructure.getPerFlowId();
			if (perFlowId != null) {
				jecnFlowStructure = flowStructureDao.get(perFlowId);
				if (jecnFlowStructure != null) {
					processAttributeWebBean.setPerFlowId(perFlowId);
					// 所属流程名称
					if (jecnFlowStructure.getFlowName() != null) {
						processAttributeWebBean.setPerFlowName(jecnFlowStructure.getFlowName());
					} else {
						processAttributeWebBean.setPerFlowName("");
					}
				} else {
					processAttributeWebBean.setPerFlowName("");
				}
			}
		}
		return processAttributeWebBean;
	}

	@Override
	public List<WebTreeBean> getChildProcessForWeb(Long pid, Long projectId, boolean isAdmin) throws Exception {
		List<Object[]> list = flowStructureDao.getChildFlows(pid, projectId, isAdmin);

		List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[2] == null || obj[3] == null) {
				continue;
			}
			WebTreeBean webTreeBean = new WebTreeBean();
			// ID
			webTreeBean.setId(obj[0].toString());
			// 名称
			if (obj[1] != null) {
				webTreeBean.setTrueText(obj[1].toString().trim());
				if (obj[4] != null && !"".equals(obj[4].toString().trim()) && JecnContants.processShowNumber) {
					webTreeBean.setText(obj[4].toString().trim() + " " + obj[1].toString());
				} else {
					webTreeBean.setText(obj[1].toString());
				}
			} else {
				webTreeBean.setTrueText("");
				webTreeBean.setText("");
			}
			if ("0".equals(obj[2].toString())) {
				// 流程地图
				webTreeBean.setType(TreeNodeType.processMap.toString());
				webTreeBean.setIcon("images/treeNodeImages/" + TreeNodeType.processMap.toString() + ".gif");
			} else if ("1".equals(obj[2].toString())) {
				// 流程
				webTreeBean.setType(TreeNodeType.process.toString());
				webTreeBean.setIcon("images/treeNodeImages/" + TreeNodeType.process.toString() + ".gif");
			}
			// 长度
			if (Integer.parseInt(obj[3].toString()) > 0) {
				webTreeBean.setLeaf(false);
			} else {
				webTreeBean.setLeaf(true);
			}
			if (obj.length > 7) {
				if (obj[7] != null && Integer.parseInt(obj[7].toString()) == 1) {
					listWebTreeBean.add(webTreeBean);
				}
			} else {
				listWebTreeBean.add(webTreeBean);
			}
		}
		return listWebTreeBean;
	}

	/**
	 * 获得流程搜索的公共sql
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	private String getSql(PublicSearchBean publicSearchBean, Long projectId) {
		String sql = " from jecn_flow_structure ju"
				+ " left join (select jfro.org_id,jfo.org_name,jfro.flow_id from jecn_flow_related_org_t jfro,jecn_flow_org jfo where jfro.org_id=jfo.org_id and jfo.del_state=0) org on org.flow_id=ju.flow_id"
				+ " where ju.del_state=0 and ju.projectid= " + projectId;
		// getSql 新添加密级条件(不能是默认 is_public =1)
		if (-1 != publicSearchBean.getSecretLevel()) { // -1:全部,0秘密,1公开
			// 不拼接is_public条件
			sql = sql + "  and ju.is_public= " + publicSearchBean.getSecretLevel();
		}
		// 流程名称
		if (StringUtils.isNotBlank(publicSearchBean.getName())) {
			sql = sql + " and ju.flow_name like '%" + publicSearchBean.getName() + "%'";
		}
		// 流程编号
		if (StringUtils.isNotBlank(publicSearchBean.getNumber())) {
			sql = sql + " and ju.flow_id_input like '%" + publicSearchBean.getNumber() + "%'";
		}
		// 责任部门
		if (StringUtils.isNotBlank(publicSearchBean.getOrgName())) {
			if (publicSearchBean.getOrgId() != -1) {
				sql = sql + " and org.org_id=" + publicSearchBean.getOrgId();
			} else {
				sql = sql + " and org.org_id in (select jfo.org_id from jecn_flow_org jfo where jfo.org_name like '%"
						+ publicSearchBean.getOrgName() + "%' )";
			}
		}
		return sql;
	}

	@Override
	public int getTotalPublicProcess(PublicSearchBean publicSearchBean, Long projectId) throws Exception {
		try {
			String sql = "select count ( ju.flow_id)";
			sql = sql + getSql(publicSearchBean, projectId);
			return flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<PublicFileBean> getPublicProcess(PublicSearchBean publicSearchBean, int start, int limit, Long projectId)
			throws Exception {
		String sql = "select ju.flow_id,ju.flow_name,ju.flow_id_input,ju.isflow,org.org_id,org.org_name,ju.is_public ";
		sql = sql + getSql(publicSearchBean, projectId);
		sql += " order by ju.pre_flow_id,ju.sort_id,ju.flow_id";
		List<Object[]> listObjects = flowStructureDao.listNativeSql(sql, start, limit);
		List<PublicFileBean> listResult = new ArrayList<PublicFileBean>();
		for (Object[] obj : listObjects) {
			if (obj == null || obj[0] == null || obj[3] == null) {
				continue;
			}
			PublicFileBean publicFileBean = new PublicFileBean();
			publicFileBean.setId(Long.valueOf(obj[0].toString()));
			if (obj[1] != null) {
				publicFileBean.setName(obj[1].toString());
			} else {
				publicFileBean.setName("");
			}
			if (obj[2] != null) {
				publicFileBean.setNumber(obj[2].toString());
			} else {
				publicFileBean.setNumber("");
			}
			if ("0".equals(obj[3].toString())) {
				publicFileBean.setType(1);
			} else {
				publicFileBean.setType(0);
			}
			if (obj[5] != null) {
				publicFileBean.setOrgName(obj[5].toString());
			} else {
				publicFileBean.setOrgName("");
			}
			if (obj[6] != null) {
				publicFileBean.setSecretLevel(Integer.parseInt(obj[6].toString()));
			}
			listResult.add(publicFileBean);
		}
		return listResult;
	}

	/**
	 * 获得流程查阅bean
	 * 
	 * @param obj
	 * @return
	 */
	private ProcessWebBean getProcessWebBeanByObject(Object[] obj, int isFlow) {
		if (obj == null || obj[0] == null) {
			return null;
		}
		ProcessWebBean processWebBean = new ProcessWebBean();
		// 流程ID
		processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
		// 流程名称
		if (obj[1] != null) {
			processWebBean.setFlowName(obj[1].toString());
		}
		// 流程编号
		if (obj[2] != null) {
			processWebBean.setFlowIdInput(obj[2].toString());
		}
		if (isFlow == 1) {
			// 责任部门
			if (obj[3] != null && obj[4] != null) {
				processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
				processWebBean.setOrgName(obj[4].toString());
			}
			// 责任人
			if (obj[5] != null && obj[6] != null && obj[7] != null) {
				processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
				processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
				processWebBean.setResPeopleName(obj[7].toString());
			}
			// 更新时间
			if (obj[8] != null && obj[10] != null) {
				processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[8].toString())));
				processWebBean.setNoUpdateDate(JecnCommon.getMonth(JecnCommon.getDateByString(obj[10].toString()),
						JecnCommon.getDateByString(obj[8].toString())));
			}
			// 密级
			if (obj[9] != null) {
				processWebBean.setIsPublic(Integer.parseInt(obj[9].toString()));
			}
		} else {
			// 更新时间
			if (obj[3] != null) {
				processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[3].toString())));
				// processWebBean.setNoUpdateDate(JecnCommon.getMonth(JecnCommon
				// .getDateByString(obj[5].toString()), JecnCommon
				// .getDateByString(obj[3].toString())));
			}
			// 密级
			if (obj[4] != null) {
				processWebBean.setIsPublic(Integer.parseInt(obj[4].toString()));
			}
		}
		return processWebBean;
	}

	@Override
	public int getTotalAttenTionProcess(AttenTionSearchBean attenTionSearchBean, Long projectId) throws Exception {
		try {
			if (attenTionSearchBean.getIsFlow() == 1) {
				return this.flowStructureDao.countAllByParamsNativeSql(MyFlowSqlUtil.getAttenTionFlowCount(
						attenTionSearchBean, projectId));
			} else {
				return this.flowStructureDao.countAllByParamsNativeSql(MyFlowSqlUtil.getAttenTionFlowMapCount(
						attenTionSearchBean, projectId));
			}

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessWebBean> getAttenTionProcess(AttenTionSearchBean attenTionSearchBean, int start, int limit,
			Long projectId) throws Exception {
		String sql = "";
		if (attenTionSearchBean.getIsFlow() == 1) {
			sql = MyFlowSqlUtil.getAttenTionFlow(attenTionSearchBean, projectId);
		} else {
			sql = MyFlowSqlUtil.getAttenTionFlowMap(attenTionSearchBean, projectId);
		}
		List<Object[]> listObject = flowStructureDao.listNativeSql(sql, start, limit);
		List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
		if (listObject == null) {
			return listProcessWebBean;
		}
		ProcessWebBean processWebBean = null;
		for (Object[] obj : listObject) {
			processWebBean = getProcessWebBeanByObject(obj, attenTionSearchBean.getIsFlow());
			if (processWebBean != null)
				listProcessWebBean.add(processWebBean);
		}
		return listProcessWebBean;
	}

	// 查询我参与的流程
	@Override
	public List<JecnRoleProcessBean> getMyJoinProcess(Long userId, String processName, String processNum, Long posId,
			Long projectId) throws Exception {
		List<JecnRoleProcessBean> listResult = new ArrayList<JecnRoleProcessBean>();
		// 岗位相关角色,及流程(不包括岗位组)
		List<Object[]> listRoleNoGroup = this.flowOrgImageDao.getRoleNOGroupByList(userId, projectId);
		// 岗位相关角色,及流程(岗位组)
		List<Object[]> listRoleGroup = this.flowOrgImageDao.getRoleGroupByList(userId, projectId);

		Set<Long> setRoleIds = new HashSet<Long>();
		Set<Long> setFlowIds = new HashSet<Long>();
		// 去重复的角色
		List<Object[]> listRole = new ArrayList<Object[]>();
		for (Object[] obj : listRoleNoGroup) {
			if (obj[0] != null) {
				setRoleIds.add(Long.valueOf(obj[0].toString()));
				listRole.add(obj);
			}
			if (obj[3] != null) {
				setFlowIds.add(Long.valueOf(obj[3].toString()));
			}
		}
		for (Object[] obj : listRoleGroup) {
			if (obj[0] != null) {
				if (!setRoleIds.contains(Long.valueOf(obj[0].toString()))) {
					setRoleIds.add(Long.valueOf(obj[0].toString()));
					listRole.add(obj);
				}
			}
			if (obj[3] != null) {
				if (!setFlowIds.contains(Long.valueOf(obj[3].toString()))) {
					setFlowIds.add(Long.valueOf(obj[3].toString()));
				}
			}
		}

		List<Object[]> listActive = new ArrayList<Object[]>();// 岗位下相关流程的活动
		List<Object[]> listRules = null;// 相关制度
		List<Object[]> listStans = null;// 相关标准
		List<Object[]> listRisks = null;// 相关风险
		if (setRoleIds.size() > 0) {
			listActive = positionFigInfoDao.getActiveByRoleListIds(setRoleIds);
			listRules = flowStructureDao.getProcessRules(setFlowIds);
			listStans = flowStructureDao.getProcessStandards(setFlowIds);
			listRisks = flowStructureDao.getProcessRisks(setFlowIds);
		}

		// 获得人员的岗位和部门信息
		List<Object[]> listPos = this.userDao.getPosOrgInfo(userId);
		// 循环岗位
		for (Object[] obj : listPos) {
			if (obj == null || obj[0] == null || obj[2] == null) {
				continue;
			}
			if (posId != -1) {
				if (!obj[0].toString().equals(posId.toString())) {
					continue;
				}
			}
			JecnRoleProcessBean jecnRoleProcessBean = new JecnRoleProcessBean();
			// 岗位ID
			jecnRoleProcessBean.setPosId(Long.valueOf(obj[0].toString()));
			// 岗位名称
			if (obj[1] != null) {
				jecnRoleProcessBean.setPosName(obj[1].toString());
			} else {
				jecnRoleProcessBean.setPosName("");
			}
			// 参与的角色集合
			List<ProcessRoleWebBean> roleList = new ArrayList<ProcessRoleWebBean>();
			// 参与角色
			for (Object[] roleObj : listRole) {
				if (roleObj == null || roleObj[0] == null || roleObj[3] == null || roleObj[6] == null) {
					continue;
				}
				// 岗位下参与的角色
				if (obj[0].toString().equals(roleObj[6].toString())) {
					ProcessRoleWebBean processRoleWebBean = new ProcessRoleWebBean();
					// 角色Id
					processRoleWebBean.setRoleId(Long.valueOf(roleObj[0].toString()));
					// 角色名称
					if (roleObj[1] != null)
						processRoleWebBean.setRoleName(roleObj[1].toString());
					// 角色职责
					if (roleObj[2] != null)
						processRoleWebBean.setRoleRes(roleObj[2].toString());
					// 流程id
					processRoleWebBean.setFlowId(Long.valueOf(roleObj[3].toString()));

					if (StringUtils.isNotBlank(processName)) {
						// 流程名称
						if (roleObj[4] == null || "".equals(roleObj[4].toString())
								|| roleObj[4].toString().indexOf(processName) == -1) {
							continue;
						}
					}
					if (roleObj[4] != null) {
						processRoleWebBean.setFlowName(roleObj[4].toString());
					}
					// 流程编号
					if (StringUtils.isNotBlank(processNum)) {
						if (roleObj[5] == null || "".equals(roleObj[5].toString())
								|| roleObj[5].toString().indexOf(processNum) == -1) {
							continue;
						}
					}
					if (roleObj[5] != null) {
						processRoleWebBean.setFlowNumber(roleObj[5].toString());
					}
					if (roleObj[7] != null) {
						processRoleWebBean.setPflowId(Long.valueOf(roleObj[7].toString()));
					}
					if (roleObj[8] != null) {
						processRoleWebBean.setPflowName(roleObj[8].toString());
					}
					if (roleObj[9] != null) {
						processRoleWebBean.setPtype(Long.valueOf(roleObj[9].toString()));
					}

					// 参与的活动
					List<ProcessActiveWebBean> activeList = new ArrayList<ProcessActiveWebBean>();
					for (Object[] objActive : listActive) {
						// a.figureId,a.activityId,a.figureText,b.figureRoleId
						if (objActive == null || objActive[0] == null || objActive[3] == null) {
							continue;
						}
						// 角色下参与的活动
						if (roleObj[0].toString().equals(objActive[3].toString())) {
							ProcessActiveWebBean processActiveWebBean = new ProcessActiveWebBean();
							// 活动主键
							processActiveWebBean.setActiveFigureId(Long.valueOf(objActive[0].toString()));
							// 活动编号
							if (objActive[1] != null) {
								processActiveWebBean.setActiveId(objActive[1].toString());
							}
							// 活动名称
							if (objActive[2] != null) {
								processActiveWebBean.setActiveName(objActive[2].toString());
							}
							activeList.add(processActiveWebBean);
						}
					}
					// 相关制度
					List<Object[]> ruleList = new ArrayList<Object[]>();
					for (Object[] objRule : listRules) {
						if (objRule == null || objRule[0] == null || objRule[1] == null || objRule[2] == null
								|| objRule[3] == null) {
							continue;
						}
						if (roleObj[3].toString().equals(objRule[0].toString())) {
							ruleList.add(objRule);
						}
					}
					// 相关标准
					List<Object[]> stanList = new ArrayList<Object[]>();
					for (Object[] objStan : listStans) {
						if (objStan == null || objStan[0] == null || objStan[1] == null || objStan[2] == null
								|| objStan[3] == null) {
							continue;
						}
						// 标准文件
						if ("1".equals(objStan[3].toString()) && objStan[4] == null) {
							continue;
						}

						if (roleObj[3].toString().equals(objStan[0].toString())) {
							stanList.add(objStan);
						}
					}
					List<Object[]> riskList = new ArrayList<Object[]>();
					for (Object[] objRisk : listRisks) {
						if (objRisk == null || objRisk[0] == null || objRisk[1] == null || objRisk[2] == null) {
							continue;
						}
						if (roleObj[3].toString().equals(objRisk[0].toString())) {
							riskList.add(objRisk);
						}
					}
					// 相关风险
					JecnUtil.getActivitySort(activeList);
					processRoleWebBean.setActiveList(activeList);
					processRoleWebBean.setRuleList(ruleList);
					processRoleWebBean.setStanList(stanList);
					processRoleWebBean.setRiskList(riskList);
					roleList.add(processRoleWebBean);
				}
			}
			if (roleList.size() == 0) {
				continue;
			}
			jecnRoleProcessBean.setRoleList(roleList);
			listResult.add(jecnRoleProcessBean);
		}
		return listResult;
	}

	@Override
	public int getTotalSearchProcess(ProcessSearchBean processSearchBean, long peopleId, long projectId, boolean isAdmin)
			throws Exception {
		try {
			String sql = MyFlowSqlUtil.getSqlProcessSearch(processSearchBean, peopleId, projectId, true, isAdmin);
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessWebBean> searchProcess(ProcessSearchBean processSearchBean, int start, int limit, long peopleId,
			long projectId, boolean isAdmin) throws Exception {
		// 是不是搜索所有流程节点
		String sql = MyFlowSqlUtil.getSqlProcessSearch(processSearchBean, peopleId, projectId, false, isAdmin);
		List<Object[]> listObject = flowStructureDao.listNativeSql(sql, start, limit);
		List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
		for (Object[] obj : listObject) {
			ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObject(obj);
			if (processWebBean != null)
				listProcessWebBean.add(processWebBean);
		}
		return listProcessWebBean;
	}

	@Override
	public int getTotalSearchProcessMap(ProcessSearchBean processSearchBean, long peopleId, long projectId,
			boolean isAdmin) throws Exception {
		try {

			String sql = MyFlowSqlUtil.getSqlProcessMapSearch(processSearchBean, peopleId, projectId, true, isAdmin);
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessMapWebBean> searchProcessMap(ProcessSearchBean processSearchBean, int start, int limit,
			long peopleId, long projectId, boolean isAdmin) throws Exception {
		try {
			String sql = MyFlowSqlUtil.getSqlProcessMapSearch(processSearchBean, peopleId, projectId, false, isAdmin);
			List<Object[]> listObject = flowStructureDao.listNativeSql(sql, start, limit);
			List<ProcessMapWebBean> listProcessWebBean = new ArrayList<ProcessMapWebBean>();
			for (Object[] obj : listObject) {
				if (obj == null || obj[0] == null || obj[3] == null) {
					continue;
				}
				ProcessMapWebBean processMapWebBean = new ProcessMapWebBean();
				// 流程ID
				processMapWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null) {
					processMapWebBean.setFlowName(obj[1].toString());
				}
				// 流程编号
				if (obj[2] != null) {
					processMapWebBean.setFlowIdInput(obj[2].toString());
				}
				// 流程所属流程地图
				if (obj[3] != null && obj[4] != null) {
					processMapWebBean.setPid(Long.valueOf(obj[3].toString()));
					processMapWebBean.setpName(obj[4].toString());
				}
				// 密级
				if (obj[5] != null) {
					processMapWebBean.setIsPublic(Integer.parseInt(obj[5].toString()));
				}
				// 发布时间
				if (obj[6] != null) {
					processMapWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[6]
							.toString())));
				}
				listProcessWebBean.add(processMapWebBean);
			}
			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public IPositionFigInfoDao getPositionFigInfoDao() {
		return positionFigInfoDao;
	}

	public void setPositionFigInfoDao(IPositionFigInfoDao positionFigInfoDao) {
		this.positionFigInfoDao = positionFigInfoDao;
	}

	@Override
	public ProcessShowFile getProcessShowFile(Long processId, boolean isPub) throws Exception {
		try {
			ProcessShowFile processShowFile = new ProcessShowFile();
			Long pId = null;
			// 操作说明配置
			List<JecnConfigItemBean> configItemBeanList = configItemDao.selectShowFlowFile();
			// 获取活动输入输出说明配置
			String activityNote = configItemDao.selectValue(1, ConfigItemPartMapMark.activityFileShow.toString());
			// 流程驱动规则时间驱动配置
			String flowDrivenRuleTimeConfig = configItemDao.selectValue(6, ConfigItemPartMapMark.isShowDateFlowFile
					.toString());
			// 获取流程文件
			String flowFileType = configItemDao.selectValue(1, ConfigItemPartMapMark.flowFileType.toString());
			if (!JecnCommon.isNullOrEmtryTrim(flowFileType)) {
				processShowFile.setFlowFileType(Integer.valueOf(flowFileType));
			}
			processShowFile.setFlowDrivenRuleTimeConfig(flowDrivenRuleTimeConfig);
			processShowFile.setActivityNoteShow(activityNote);
			processShowFile.setJecnConfigItemBean(configItemBeanList);
			if (isPub) {
				JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(processId);
				JecnFlowBasicInfo jecnFlowBasicInfo = flowStructureDao.findJecnFlowBasicInfoById(processId);
				if (jecnFlowStructure == null) {
					return null;
				}
				pId = jecnFlowStructure.getPerFlowId();
				// 流程名称
				processShowFile.setFlowName(jecnFlowStructure.getFlowName());
				// 流程ID
				processShowFile.setFlowId(jecnFlowStructure.getFlowId().toString());
				// 流程客户
				String flowCustom = jecnFlowBasicInfo.getFlowCustom();
				if (flowCustom != null) {
					flowCustom = flowCustom.replaceAll("\n", "<br>");
					processShowFile.setFlowCustom(flowCustom.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustom("");
				}

				// 目的
				String flowPurpose = jecnFlowBasicInfo.getFlowPurpose();
				if (flowPurpose != null) {
					flowPurpose = flowPurpose.replaceAll("\n", "<br>");
					processShowFile.setFlowPurpose(flowPurpose.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowPurpose("");
				}
				// 适用范围
				String applicability = jecnFlowBasicInfo.getApplicability();
				if (applicability != null) {
					applicability = applicability.replaceAll("\n", "<br>");
					processShowFile.setApplicability(applicability.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setApplicability("");
				}
				// 术语定义
				String noutGlossary = jecnFlowBasicInfo.getNoutGlossary();
				if (noutGlossary != null) {
					noutGlossary = noutGlossary.replaceAll("\n", "<br>");
					processShowFile.setNoutGlossary(noutGlossary.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setNoutGlossary("");
				}
				// 流程输入
				String input = jecnFlowBasicInfo.getInput();
				if (input != null) {
					input = input.replaceAll("\n", "<br>");
					processShowFile.setFlowInput(input.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowInput("");
				}
				// 流程输出
				String ouput = jecnFlowBasicInfo.getOuput();
				if (ouput != null) {
					ouput = ouput.replaceAll("\n", "<br>");
					processShowFile.setFlowOutput(ouput.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowOutput("");
				}
				// 不适用范围
				String noApplicability = jecnFlowBasicInfo.getNoApplicability();
				if (noApplicability != null) {
					noApplicability = noApplicability.replaceAll("\n", "<br>");
					processShowFile.setNoApplicability(noApplicability.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setNoApplicability("");
				}
				// 补充说明
				String flowSupplement = jecnFlowBasicInfo.getFlowSupplement();
				if (flowSupplement != null) {
					flowSupplement = flowSupplement.replaceAll("\n", "<br>");
					processShowFile.setFlowSupplement(flowSupplement.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowSupplement("");
				}
				// 概况信息
				String flowSummarize = jecnFlowBasicInfo.getFlowSummarize();
				if (flowSummarize != null) {
					flowSummarize = flowSummarize.replaceAll("\n", "<br>");
					processShowFile.setFlowSummarize(flowSummarize.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowSummarize("");
				}
				// 驱动规则的类型
				if (jecnFlowBasicInfo.getDriveType() != null) {
					FlowDriverBean flowDriverBean = ProcessDownDataUtil.getFlowDriverBean(processId, null,
							jecnFlowBasicInfo, flowStructureDao);
					String driveRules = flowDriverBean.getDriveRules();
					if (driveRules != null) {
						driveRules = driveRules.replaceAll("\n", "<br>");
						flowDriverBean.setDriveRules(driveRules.replaceAll(" ", "&nbsp;"));
					}
					processShowFile.setFlowDriver(flowDriverBean);
				}
				// 流程创建人
				if (jecnFlowStructure.getPeopleId() != null) {
					// 获取用户名称
					String userName = processBasicDao.getUserName(jecnFlowStructure.getPeopleId());
					processShowFile.setCreateName(userName);
				}
				// 起始活动
				String startActivity = jecnFlowBasicInfo.getFlowStartpoint();
				// 终止活动
				String endActivity = jecnFlowBasicInfo.getFlowEndpoint();
				// 流程边界
				String[] startEndActivity = new String[] { startActivity, endActivity };
				processShowFile.setStartEndActive(startEndActivity);
				// 流程责任属性
				processShowFile.setResponFlow(ProcessDownDataUtil.getProcessAttributeBean(processId, jecnFlowBasicInfo
						.getTypeResPeople() == null ? 0 : Long.valueOf(jecnFlowBasicInfo.getTypeResPeople()),
						jecnFlowBasicInfo.getResPeopleId(), jecnFlowBasicInfo.getGuardianId(), jecnFlowBasicInfo
								.getFictionPeopleId(), processShowFile.getCreateName(), processBasicDao));
				// 流程关键评测指标数据 0 名称 1 类型 2 KPI定义 3统计方法 4 目标值
				// processShowFile.setFlowKpiList(processKPIDao
				// .getJecnFlowKpiNameList(processId));
				processShowFile.setKpiShowValues(getConfigKpiNames(getFlowKpiNameTList(processId)));
				// 记录保存数据 0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式
				processShowFile.setFlowRecordList(processRecordDao.getJecnFlowRecordListByFlowId(processId));
				// 自定义要素1
				String flowCustomOne = jecnFlowBasicInfo.getFlowCustomOne();
				if (flowCustomOne != null) {
					flowCustomOne = flowCustomOne.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomOne(flowCustomOne.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomOne("");
				}
				// 自定义要素2
				String flowCustomTwo = jecnFlowBasicInfo.getFlowCustomTwo();
				if (flowCustomTwo != null) {
					flowCustomTwo = flowCustomTwo.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomTwo(flowCustomTwo.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomTwo("");
				}
				// 自定义要素3
				String flowCustomThree = jecnFlowBasicInfo.getFlowCustomThree();
				if (flowCustomThree != null) {
					flowCustomThree = flowCustomThree.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomThree(flowCustomThree.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomThree("");
				}
				// 自定义要素4
				String flowCustomFour = jecnFlowBasicInfo.getFlowCustomFour();
				if (flowCustomFour != null) {
					flowCustomFour = flowCustomFour.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomFour(flowCustomFour.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomFour("");
				}
				// 自定义要素5
				String flowCustomFive = jecnFlowBasicInfo.getFlowCustomFive();
				if (flowCustomFive != null) {
					flowCustomFive = flowCustomFive.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomFive(flowCustomFive.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomFive("");
				}

			} else {
				JecnFlowStructureT jecnFlowStructure = flowStructureDao.get(processId);
				JecnFlowBasicInfoT jecnFlowBasicInfo = (JecnFlowBasicInfoT) processBasicDao.getSession().get(
						JecnFlowBasicInfoT.class, processId);
				if (jecnFlowStructure == null) {
					return null;
				}
				pId = jecnFlowStructure.getPerFlowId();
				// 流程名称
				processShowFile.setFlowName(jecnFlowStructure.getFlowName());
				// 流程ID
				processShowFile.setFlowId(jecnFlowStructure.getFlowId().toString());
				// 流程客户
				String flowCustom = jecnFlowBasicInfo.getFlowCustom();
				if (flowCustom != null) {
					flowCustom = flowCustom.replaceAll("\n", "<br>");
					processShowFile.setFlowCustom(flowCustom.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustom("");
				}

				// 目的
				String flowPurpose = jecnFlowBasicInfo.getFlowPurpose();
				if (flowPurpose != null) {
					flowPurpose = flowPurpose.replaceAll("\n", "<br>");
					processShowFile.setFlowPurpose(flowPurpose.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowPurpose("");
				}
				// 适用范围
				String applicability = jecnFlowBasicInfo.getApplicability();
				if (applicability != null) {
					applicability = applicability.replaceAll("\n", "<br>");
					processShowFile.setApplicability(applicability.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setApplicability("");
				}
				// 术语定义
				String noutGlossary = jecnFlowBasicInfo.getNoutGlossary();
				if (noutGlossary != null) {
					noutGlossary = noutGlossary.replaceAll("\n", "<br>");
					processShowFile.setNoutGlossary(noutGlossary.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setNoutGlossary("");
				}
				// 流程输入
				String input = jecnFlowBasicInfo.getInput();
				if (input != null) {
					input = input.replaceAll("\n", "<br>");
					processShowFile.setFlowInput(input.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowInput("");
				}
				// 流程输出
				String ouput = jecnFlowBasicInfo.getOuput();
				if (ouput != null) {
					ouput = ouput.replaceAll("\n", "<br>");
					processShowFile.setFlowOutput(ouput.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowOutput("");
				}
				// 不适用范围
				String noApplicability = jecnFlowBasicInfo.getNoApplicability();
				if (noApplicability != null) {
					noApplicability = noApplicability.replaceAll("\n", "<br>");
					processShowFile.setNoApplicability(noApplicability.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setNoApplicability("");
				}
				// 补充说明
				String flowSupplement = jecnFlowBasicInfo.getFlowSupplement();
				if (flowSupplement != null) {
					flowSupplement = flowSupplement.replaceAll("\n", "<br>");
					processShowFile.setFlowSupplement(flowSupplement.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowSupplement("");
				}
				// 概况信息
				String flowSummarize = jecnFlowBasicInfo.getFlowSummarize();
				if (flowSummarize != null) {
					flowSummarize = flowSummarize.replaceAll("\n", "<br>");
					processShowFile.setFlowSummarize(flowSummarize.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowSummarize("");
				}
				// 驱动规则的类型
				if (jecnFlowBasicInfo.getDriveType() != null) {
					FlowDriverBean flowDriverBean = ProcessDownDataUtil.getFlowDriverBean(processId, jecnFlowBasicInfo,
							null, flowStructureDao);
					String driveRules = flowDriverBean.getDriveRules();
					if (driveRules != null) {
						driveRules = driveRules.replaceAll("\n", "<br>");
						flowDriverBean.setDriveRules(driveRules.replaceAll(" ", "&nbsp;"));
					}
					processShowFile.setFlowDriver(flowDriverBean);
				}
				// 流程创建人
				if (jecnFlowStructure.getPeopleId() != null) {
					// 获取用户名称
					String userName = processBasicDao.getUserName(jecnFlowStructure.getPeopleId());
					processShowFile.setCreateName(userName);
				}
				// 起始活动
				String startActivity = jecnFlowBasicInfo.getFlowStartpoint();
				// 终止活动
				String endActivity = jecnFlowBasicInfo.getFlowEndpoint();
				// 流程边界
				String[] startEndActivity = new String[] { startActivity, endActivity };
				processShowFile.setStartEndActive(startEndActivity);
				// 流程责任属性
				processShowFile.setResponFlow(ProcessDownDataUtil.getProcessAttributeBean(processId, jecnFlowBasicInfo
						.getTypeResPeople() == null ? 0 : Long.valueOf(jecnFlowBasicInfo.getTypeResPeople()),
						jecnFlowBasicInfo.getResPeopleId(), jecnFlowBasicInfo.getGuardianId(), jecnFlowBasicInfo
								.getFictionPeopleId(), processShowFile.getCreateName(), processBasicDao));
				// 流程关键评测指标数据 0 名称 1 类型 2 KPI定义 3统计方法 4 目标值
				List<JecnFlowKpiNameT> listJecnFlowKpiNameT = processKPIDao.getJecnFlowKpiNameTList(processId);
				List<JecnFlowKpiName> listJecnFlowKpiName = new ArrayList<JecnFlowKpiName>();
				for (JecnFlowKpiNameT jecnFlowKpiNameT : listJecnFlowKpiNameT) {
					JecnFlowKpiName jecnFlowKpiName = new JecnFlowKpiName();
					Integer kpiType = jecnFlowKpiNameT.getKpiType();
					PropertyUtils.copyProperties(jecnFlowKpiName, jecnFlowKpiNameT);
					jecnFlowKpiName.setKpiType(kpiType);
					listJecnFlowKpiName.add(jecnFlowKpiName);
				}
				// processShowFile.setFlowKpiList(listJecnFlowKpiName);
				processShowFile.setKpiShowValues(getConfigKpiNames(listJecnFlowKpiName));
				// 记录保存数据 0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式
				List<JecnFlowRecordT> listJecnFlowRecordT = processRecordDao.getJecnFlowRecordTListByFlowId(processId);
				List<JecnFlowRecord> listJecnFlowRecord = new ArrayList<JecnFlowRecord>();
				for (JecnFlowRecordT jecnFlowRecordT : listJecnFlowRecordT) {
					JecnFlowRecord jecnFlowRecord = new JecnFlowRecord();
					PropertyUtils.copyProperties(jecnFlowRecord, jecnFlowRecordT);
					listJecnFlowRecord.add(jecnFlowRecord);
				}
				processShowFile.setFlowRecordList(listJecnFlowRecord);
				// 自定义要素1
				String flowCustomOne = jecnFlowBasicInfo.getFlowCustomOne();
				if (flowCustomOne != null) {
					flowCustomOne = flowCustomOne.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomOne(flowCustomOne.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomOne("");
				}
				// 自定义要素2
				String flowCustomTwo = jecnFlowBasicInfo.getFlowCustomTwo();
				if (flowCustomTwo != null) {
					flowCustomTwo = flowCustomTwo.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomTwo(flowCustomTwo.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomTwo("");
				}
				// 自定义要素3
				String flowCustomThree = jecnFlowBasicInfo.getFlowCustomThree();
				if (flowCustomThree != null) {
					flowCustomThree = flowCustomThree.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomThree(flowCustomThree.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomThree("");
				}
				// 自定义要素4
				String flowCustomFour = jecnFlowBasicInfo.getFlowCustomFour();
				if (flowCustomFour != null) {
					flowCustomFour = flowCustomFour.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomFour(flowCustomFour.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomFour("");
				}
				// 自定义要素5
				String flowCustomFive = jecnFlowBasicInfo.getFlowCustomFive();
				if (flowCustomFive != null) {
					flowCustomFive = flowCustomFive.replaceAll("\n", "<br>");
					processShowFile.setFlowCustomFive(flowCustomFive.replaceAll(" ", "&nbsp;"));
				} else {
					processShowFile.setFlowCustomFive("");
				}
			}

			List<Object[]> listActiveShow = null;
			/** ******************** 南京石化多两数据 *********************8 */
			if (JecnContants.otherOperaType == 6) {
				// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6对应内控矩阵风险点 7对应标准条款
				listActiveShow = flowStructureDao.getActiveByFlowId(processId, isPub);
			} else {
				// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明
				listActiveShow = flowStructureDao.getActiveShowByFlowId(processId, isPub);
			}
			/** ****************************************************** */

			// 角色明细数据 0是角色主键ID 1角色名称 2角色职责
			List<Object[]> listRoleShow = flowStructureDao.getRoleShowByFlowId(processId, isPub);
			// 流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
			List<Object[]> listRoleAndActiveRelate = flowStructureDao.findAllRoleAndActiveRelateByFlowId(processId,
					isPub);
			// 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号
			List<Object[]> listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowId(processId, isPub);
			// 流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
			List<Object[]> listOutFiles = flowStructureDao.findAllActiveOutputByFlowId(processId, isPub);
			// 活动明细数据
			processShowFile.setAllActivityShowList(getActiveShow(listActiveShow, listRoleShow, listRoleAndActiveRelate,
					listInputFiles, listOutFiles));
			// 关键活动数据
			getKeyActiveShow(listActiveShow, processShowFile.getPaActivityShowList(), processShowFile
					.getKsfActivityShowList(), processShowFile.getKcpActivityShowList());
			// 流程下所有的角色和岗位关系表 0是角色主键ID 1是岗位主键ID 2是岗位名称
			List<Object[]> roleRelatePos = flowStructureDao.finaAllRoleAndPosRelates(processId, isPub);
			// 流程下所有的角色和岗位组关系表 0是角色主键ID 1是岗位主键ID 2是岗位名称
			List<Object[]> roleRelatePosGroup = flowStructureDao.finaAllRoleAndPosGroupPosRelates(processId, isPub);
			processShowFile.setRoleList(getRoleShow(listRoleShow, roleRelatePos, roleRelatePosGroup));

			// 相关流程
			List<Object[]> listObjectsRelateFlows = flowStructureDao.findAllRelateFlows(processId, isPub);
			processShowFile.setRelatedProcessList(getRelatedProcessBeanList(listObjectsRelateFlows));

			// 流程记录 0 文件编号 1 文件名称 输入和输出集合
			List<FileWebBean> processRecordList = new ArrayList<FileWebBean>();
			// 操作规范 0 文件编号 1 文件名称
			List<FileWebBean> operationTemplateList = new ArrayList<FileWebBean>();
			getFlowFiles(processRecordList, operationTemplateList, listInputFiles, listOutFiles);
			processShowFile.setProcessRecordList(processRecordList);
			processShowFile.setOperationTemplateList(operationTemplateList);
			// 相关制度名称
			processShowFile.setRuleNameList(getRelateRules(processBasicDao.getFlowRelateRules(processId, isPub)));
			// 相关标准
			processShowFile.setStandardList(getRelateStandards(flowStructureDao.getFlowStandards(processId, isPub)));
			// 相关风险
			processShowFile.setRiskList(flowStructureDao.getRiskByFlowId(processId, isPub));
			// 获取文控信息
			List<Object[]> documentList = new ArrayList<Object[]>();
			ProcessDownDataUtil.getTaskHistoryNewList(processId, docControlDao, documentList, true);
			processShowFile.setDocumentList(documentList);

			// 术语
			processShowFile.setDefinitions(getDefinitionsByFlowId(processId, isPub));
			// 接口
			processShowFile.setProcessInterfacesDescriptionBean(getProcessInterfacesDescriptionBean(processId, pId,
					isPub));
			// 驱动时间
			if (processShowFile.getFlowDriver() != null) {
				processShowFile.getFlowDriver().setTimes(getFlowDriverTimes(processId, isPub));
			}

			return processShowFile;
		} catch (Exception e) {
			log.error("获取操作说明数据异常！", e);
			throw e;
		}
	}

	private List<? extends JecnFlowDriverTimeBase> getFlowDriverTimes(Long processId, boolean isPub) {
		String tableName = isPub ? "JecnFlowDriverTime" : "JecnFlowDriverTimeT";
		String hql = "from " + tableName + " where flowId=?";
		return this.baseDao.listHql(hql, processId);
	}

	private List<TermDefinitionLibrary> getDefinitionsByFlowId(Long processId, boolean isPub) throws Exception {
		return flowStructureDao.getDefinitionsByFlowId(processId, isPub);
	}

	private ProcessInterfacesDescriptionBean getProcessInterfacesDescriptionBean(Long processId, Long pId, boolean isPub)
			throws Exception {
		ProcessInterfacesDescriptionBean descriptionBean = new ProcessInterfacesDescriptionBean();
		if (pId != 0) {
			if (isPub) {
				JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(pId);
				descriptionBean.setPid(jecnFlowStructure.getFlowId());
				descriptionBean.setPname(jecnFlowStructure.getFlowName());
				descriptionBean.setPnumber(jecnFlowStructure.getFlowIdInput());
			} else {
				JecnFlowStructureT jecnFlowStructure = flowStructureDao.get(pId);
				descriptionBean.setPid(jecnFlowStructure.getFlowId());
				descriptionBean.setPname(jecnFlowStructure.getFlowName());
				descriptionBean.setPnumber(jecnFlowStructure.getFlowIdInput());
			}
		}
		List<ProcessInterface> interfaces = flowStructureDao.findFlowInterfaces(processId, isPub);
		for (ProcessInterface i : interfaces) {
			if (i.getImplType() == 1) {
				descriptionBean.getUppers().add(i);
			} else if (i.getImplType() == 2) {
				descriptionBean.getLowers().add(i);
			} else if (i.getImplType() == 3) {
				descriptionBean.getProcedurals().add(i);
			} else if (i.getImplType() == 4) {
				descriptionBean.getSons().add(i);
			}
		}
		return descriptionBean;
	}

	public List<JecnFlowKpiName> getFlowKpiNameTList(Long flowId) throws Exception {
		List<Object[]> list = this.processKPIDao.getFlowKpiNameTList(flowId, "");
		List<Object[]> flowTools = flowToolDao.getFlowTools(flowId, "");
		List<JecnFlowKpiName> kpiList = new ArrayList<JecnFlowKpiName>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null) {
				return null;
			}
			JecnFlowKpiName flowKpi = new JecnFlowKpiName();
			flowKpi.setKpiAndId(Long.valueOf(obj[0].toString()));// 主键ID0
			flowKpi.setFlowId(Long.valueOf(obj[1].toString()));// 流程Id1
			flowKpi.setKpiName(obj[2].toString());// kpi名称2
			if (obj[3] != null && !"".equals(obj[3])) {
				flowKpi.setKpiType(Integer.parseInt(obj[3].toString()));// kpi类别3
			}
			if (obj[4] != null && !"".equals(obj[4])) {
				flowKpi.setKpiDefinition(obj[4].toString());// kpi定义4
			}
			if (obj[5] != null && !"".equals(obj[5])) {
				flowKpi.setKpiStatisticalMethods(obj[5].toString());// kpi计算公式5
			}
			if (obj[6] != null && !"".equals(obj[6])) {
				flowKpi.setKpiTarget(obj[6].toString());// 目标值6
			}
			flowKpi.setKpiHorizontal(obj[7].toString());// 数据统计时间\频率7
			flowKpi.setKpiVertical(obj[8].toString());// kpi值单位名称8
			flowKpi.setCreatTime(JecnCommon.getDateByString(obj[9].toString()));// 创建时间9
			flowKpi.setKpiTargetOperator(Integer.parseInt(obj[10].toString()));// kpi目标值比较符号10
			flowKpi.setKpiDataMethod(Integer.parseInt(obj[11].toString()));// kpi数据获取方式11
			if (obj[12] != null && !"".equals(obj[12])) {
				flowKpi.setKpiDataPeopleId(Long.valueOf(obj[12].toString()));// 数据提供者ID
				// 13
			}
			flowKpi.setKpiRelevance(Integer.parseInt(obj[13].toString()));// 相关度14
			flowKpi.setKpiTargetType(Integer.parseInt(obj[14].toString()));// 指标来源15
			if (obj[15] != null && !"".equals(obj[15])) {
				flowKpi.setFirstTargetId(obj[15].toString());// 支撑的一级指标ID16
			}
			flowKpi.setUpdateTime(JecnCommon.getDateByString(obj[16].toString()));// 更新时间17
			flowKpi.setCreatePeopleId(Long.valueOf(obj[17].toString()));// 创建人ID
			flowKpi.setUpdatePeopleId(Long.valueOf(obj[18].toString()));// 更新人Id
			if (obj[19] != null && !"".equals(obj[19])) {
				flowKpi.setKpiDataPeopleName(obj[19].toString());// 数据提供者名称
			}
			if (obj[20] != null && !"".equals(obj[20])) {
				flowKpi.setFirstTargetContent(obj[20].toString());// 支撑的一级指标名称21
			}
			if (obj[21] != null && !"".equals(obj[21])) {
				flowKpi.setKpiPurpose(obj[21].toString());
			}
			if (obj[22] != null && !"".equals(obj[22])) {
				flowKpi.setKpiPoint(obj[22].toString());
			}
			if (obj[23] != null && !"".equals(obj[23])) {
				flowKpi.setKpiExplain(obj[23].toString());
			}
			if (obj[24] != null && !"".equals(obj[24])) {
				flowKpi.setKpiPeriod(obj[24].toString());
			}
			// IT系统名称
			flowKpi.setKpiITSystemNames(getToolNamesByID(flowTools, flowKpi.getKpiAndId()));
			kpiList.add(flowKpi);
		}
		return kpiList;
	}

	private String getToolNamesByID(List<Object[]> flowTools, Long kpiId) {
		if (flowTools == null || kpiId == null) {
			return "";
		}
		StringBuffer buffer = new StringBuffer();
		for (Object[] objects : flowTools) {
			if (objects[0] == null || objects[1] == null) {
				continue;
			}
			if (kpiId.toString().equals(objects[0].toString())) {
				if (buffer.length() == 0) {
					buffer.append(objects[1]);
				} else {
					buffer.append("/").append(objects[1]);
				}
			}
		}
		return buffer.toString();
	}

	private TmpKpiShowValues getConfigKpiNames(List<JecnFlowKpiName> kpiNameList) {
		return FlowKpiTitleAndValues.INSTANCE.getConfigKpiNameAndValues(kpiNameList, configItemDao
				.selectTableConfigItemBeanByType(1, 16));
	}

	/**
	 * 流程相关标准
	 * 
	 * @param list
	 * @return
	 */
	private List<StandardBean> getRelateStandards(List<Object[]> list) {
		List<StandardBean> listResult = new ArrayList<StandardBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[3] == null) {
				continue;
			}
			StandardBean standardBean = new StandardBean();
			// 主键Id
			standardBean.setCriterionClassId(Long.valueOf(obj[0].toString()));
			// 标准名称
			if (obj[1] != null)
				standardBean.setCriterionClassName(obj[1].toString());
			// 关联Id
			if (obj[2] != null) {
				standardBean.setRelationId(Long.valueOf(obj[2].toString()));
			}
			// 类型
			standardBean.setStanType(Integer.valueOf(obj[3].toString()));
			listResult.add(standardBean);
		}
		return listResult;
	}

	/**
	 * 流程相关制度
	 * 
	 * @param list
	 * @return
	 */
	private List<Rule> getRelateRules(List<Object[]> list) {
		List<Rule> listResult = new ArrayList<Rule>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[4] == null) {
				continue;
			}
			Rule rule = new Rule();
			// 制度主键Id
			rule.setId(Long.valueOf(obj[0].toString()));
			// 制度名称
			if (obj[1] != null)
				rule.setRuleName(obj[1].toString());
			// 制度编号
			if (obj[2] != null)
				rule.setRuleNumber(obj[2].toString());
			// 制度文件
			if (obj[3] != null)
				rule.setFileId(Long.valueOf(obj[3].toString()));
			// 制度类型
			if (obj[4] != null)
				rule.setIsDir(Integer.valueOf(obj[4].toString()));
			listResult.add(rule);
		}
		return listResult;
	}

	/**
	 * 获得流程记录和流程操作规范
	 * 
	 * @param processRecordList
	 * @param operationTemplateList
	 * @param listInputFiles
	 * @param listOutFiles
	 */
	private void getFlowFiles(List<FileWebBean> processRecordList, List<FileWebBean> operationTemplateList,
			List<Object[]> listInputFiles, List<Object[]> listOutFiles) {
		// 检查有不有重复 输入和输出文件
		List<Long> listRecordListIds = new ArrayList<Long>();
		// 检查有不有重复 操作规范文件
		List<Long> listoperationTemplateListIds = new ArrayList<Long>();

		for (Object[] objOutFiles : listOutFiles) {
			if (objOutFiles == null || objOutFiles[0] == null || objOutFiles[1] == null || objOutFiles[2] == null) {
				continue;
			}
			if (!listRecordListIds.contains(Long.valueOf(objOutFiles[2].toString()))) {
				listRecordListIds.add(Long.valueOf(objOutFiles[2].toString()));
				FileWebBean fileWebBean = new FileWebBean();
				// 文件ID
				fileWebBean.setFileId(Long.valueOf(objOutFiles[2].toString()));
				// 文件名称
				if (objOutFiles[3] != null)
					fileWebBean.setFileName(objOutFiles[3].toString());
				// 文件编号
				if (objOutFiles[4] != null) {
					fileWebBean.setFileNum(objOutFiles[4].toString());
				}
				processRecordList.add(fileWebBean);
			}
		}

		for (Object[] objInputFiles : listInputFiles) {
			if (objInputFiles == null || objInputFiles[0] == null || objInputFiles[1] == null
					|| objInputFiles[2] == null || objInputFiles[4] == null) {
				continue;
			}

			// 操作规范
			if ("1".equals(objInputFiles[4].toString())) {
				if (!listoperationTemplateListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
					listoperationTemplateListIds.add(Long.valueOf(objInputFiles[2].toString()));
					FileWebBean fileWebBean = new FileWebBean();
					// 文件ID
					fileWebBean.setFileId(Long.valueOf(objInputFiles[2].toString()));
					// 文件名称
					if (objInputFiles[3] != null)
						fileWebBean.setFileName(objInputFiles[3].toString());
					// 文件编号
					if (objInputFiles[5] != null) {
						fileWebBean.setFileNum(objInputFiles[5].toString());
					}
					operationTemplateList.add(fileWebBean);
				}
			} else if ("0".equals(objInputFiles[4].toString())) {
				if (!listRecordListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
					listRecordListIds.add(Long.valueOf(objInputFiles[2].toString()));
					FileWebBean fileWebBean = new FileWebBean();
					// 文件ID
					fileWebBean.setFileId(Long.valueOf(objInputFiles[2].toString()));
					// 文件名称
					if (objInputFiles[3] != null)
						fileWebBean.setFileName(objInputFiles[3].toString());
					// 文件编号
					if (objInputFiles[5] != null) {
						fileWebBean.setFileNum(objInputFiles[5].toString());
					}
					processRecordList.add(fileWebBean);
				}
			}
		}
	}

	/**
	 * 获得相关流程
	 * 
	 * @param listRelateFlows
	 * @return
	 */
	private static List<RelatedProcessBean> getRelatedProcessBeanList(List<Object[]> listRelateFlows) {
		List<RelatedProcessBean> listResult = new ArrayList<RelatedProcessBean>();
		for (Object[] obj : listRelateFlows) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			RelatedProcessBean relatedProcessBean = new RelatedProcessBean();
			// 接口流程类型
			relatedProcessBean.setLinecolor(obj[0].toString());
			// 流程编号
			if (obj[1] != null) {
				relatedProcessBean.setFlowNumber(obj[1].toString());
			} else {
				relatedProcessBean.setFlowNumber("");
			}
			// 流程名称
			if (obj[2] != null) {
				relatedProcessBean.setFlowName(obj[2].toString());
			} else {
				relatedProcessBean.setFlowName("");
			}
			if (obj[3] != null) {// linkFlowId
				relatedProcessBean.setFlowId(Long.valueOf(obj[3].toString()));
			}
			listResult.add(relatedProcessBean);
		}
		return listResult;
	}

	/**
	 * 角色信息集合 0 角色名称 1 岗位名称 2 角色职责
	 * 
	 * @param listRoleShow
	 * @param roleRelatePos
	 *            流程下所有的角色和岗位组关系表 0是角色主键ID 1是岗位主键ID 2是岗位名称
	 * @param roleRelatePosGroup
	 *            0是角色主键ID 1是岗位主键ID 2是岗位名称
	 * @return
	 */
	private List<ProcessRoleWebBean> getRoleShow(List<Object[]> listRoleShow, List<Object[]> roleRelatePos,
			List<Object[]> roleRelatePosGroup) {

		List<ProcessRoleWebBean> roleList = new ArrayList<ProcessRoleWebBean>();
		for (Object[] objRoleShow : listRoleShow) {
			if (objRoleShow == null || objRoleShow[0] == null) {
				continue;
			}
			ProcessRoleWebBean processRoleWebBean = new ProcessRoleWebBean();
			// 主键ID
			processRoleWebBean.setRoleId(Long.valueOf(objRoleShow[0].toString()));
			// 角色名称
			if (objRoleShow[1] != null)
				processRoleWebBean.setRoleName(objRoleShow[1].toString());
			// 岗位名称
			List<JecnTreeBean> listPos = new ArrayList<JecnTreeBean>();
			List<Long> listIds = new ArrayList<Long>();
			for (Object[] objPos : roleRelatePos) {
				if (objPos == null || objPos[0] == null || objPos[1] == null || objPos[2] == null) {
					continue;
				}
				if (objRoleShow[0].toString().equals(objPos[0].toString())) {
					if (!listIds.contains(Long.valueOf(objPos[1].toString()))) {
						listIds.add(Long.valueOf(objPos[1].toString()));
						JecnTreeBean jecnTreeBean = new JecnTreeBean();
						jecnTreeBean.setId(Long.valueOf(objPos[1].toString()));
						jecnTreeBean.setName(objPos[2].toString());
						listPos.add(jecnTreeBean);
					}
				}
			}
			for (Object[] objPos : roleRelatePosGroup) {
				if (objPos == null || objPos[0] == null || objPos[1] == null || objPos[2] == null) {
					continue;
				}
				if (objRoleShow[0].toString().equals(objPos[0].toString())) {
					if (!listIds.contains(Long.valueOf(objPos[1].toString()))) {
						listIds.add(Long.valueOf(objPos[1].toString()));
						JecnTreeBean jecnTreeBean = new JecnTreeBean();
						jecnTreeBean.setId(Long.valueOf(objPos[1].toString()));
						jecnTreeBean.setName(objPos[2].toString());
						listPos.add(jecnTreeBean);
					}
				}
			}
			processRoleWebBean.setListPos(listPos);
			// 角色职责
			if (objRoleShow[2] != null)
				processRoleWebBean.setRoleRes(objRoleShow[2].toString());
			roleList.add(processRoleWebBean);
		}
		return roleList;
	}

	/**
	 * 获取关键活动说明
	 * 
	 * @author fuzhh 2014-1-14
	 * @param listActiveShow
	 * @param paActivityList
	 * @param ksfActivityList
	 * @param kcpActivityList
	 */
	private void getKeyActiveShow(List<Object[]> listActiveShow, List<ProcessActiveWebBean> paActivityList,
			List<ProcessActiveWebBean> ksfActivityList, List<ProcessActiveWebBean> kcpActivityList) {
		JecnUtil.getActivityObjSort(listActiveShow);
		for (Object[] objActives : listActiveShow) {
			if (objActives == null || objActives[0] == null) {
				continue;
			}
			ProcessActiveWebBean processActiveWebBean = new ProcessActiveWebBean();
			// 活动主键ID
			processActiveWebBean.setActiveFigureId(Long.valueOf(objActives[0].toString()));
			// 活动编号
			if (objActives[1] != null)
				processActiveWebBean.setActiveId(objActives[1].toString());
			// 活动名称
			if (objActives[2] != null)
				processActiveWebBean.setActiveName(objActives[2].toString());
			// 活动说明
			if (objActives[3] != null) {
				String activeShow = objActives[3].toString().replaceAll("\n", "<br>");
				processActiveWebBean.setActiveShow(activeShow);
			}
			// 关键活动类型
			if (objActives[4] != null) {
				// 关键活动类型：1为PA,2为KSF,3为KCP
				if ("1".equals(objActives[4].toString())) {
					processActiveWebBean.setKeyActiveState(Integer.parseInt(objActives[4].toString()));
					paActivityList.add(processActiveWebBean);
				} else if ("2".equals(objActives[4].toString())) {
					processActiveWebBean.setKeyActiveState(Integer.parseInt(objActives[4].toString()));
					ksfActivityList.add(processActiveWebBean);
				} else if ("3".equals(objActives[4].toString())) {
					processActiveWebBean.setKeyActiveState(Integer.parseInt(objActives[4].toString()));
					kcpActivityList.add(processActiveWebBean);
				} else {
					processActiveWebBean.setKeyActiveState(-1);
				}
			} else {
				processActiveWebBean.setKeyActiveState(-1);
			}
			// 关键说明
			if (objActives[5] != null) {
				String keyActive = objActives[5].toString().replaceAll("\n", "<br>");
				processActiveWebBean.setKeyActiveShow(keyActive);
			} else {
				processActiveWebBean.setKeyActiveShow("");
			}

			if (objActives[8] != null) {// 接口（活动）/子流程（活动）
				processActiveWebBean.setLinkFlowId(Long.valueOf(objActives[8].toString()));
			}
			if (objActives[9] != null) {// 接口（活动）
				processActiveWebBean.setActivityFlowType(JecnUtil.isActivityFlow(objActives[9].toString()) ? 1 : 0);
			}
		}
	}

	/**
	 * 活动明显 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
	 * 
	 * @param listActiveShow
	 *            0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明
	 * @param listRoleShow
	 *            角色明细数据 0是角色主键ID 1角色名称 2角色职责
	 * @param listRoleAndActiveRelate
	 *            流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
	 * @param listInputFiles
	 *            0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号
	 * @param listOutFiles
	 *            流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
	 * @return
	 */
	private List<ProcessActiveWebBean> getActiveShow(List<Object[]> listActiveShow, List<Object[]> listRoleShow,
			List<Object[]> listRoleAndActiveRelate, List<Object[]> listInputFiles, List<Object[]> listOutFiles) {
		List<ProcessActiveWebBean> listResult = new ArrayList<ProcessActiveWebBean>();
		// 活动排序
		JecnUtil.getActivityObjSort(listActiveShow);
		for (Object[] objActives : listActiveShow) {
			if (objActives == null || objActives[0] == null) {
				continue;
			}
			ProcessActiveWebBean processActiveWebBean = new ProcessActiveWebBean();
			// 活动主键ID
			processActiveWebBean.setActiveFigureId(Long.valueOf(objActives[0].toString()));
			// 活动编号
			if (objActives[1] != null)
				processActiveWebBean.setActiveId(objActives[1].toString());
			// 关键活动类型
			if (objActives[4] != null) {
				// 关键活动类型：1为PA,2为KSF,3为KCP
				if ("1".equals(objActives[4].toString()) || "2".equals(objActives[4].toString())
						|| "3".equals(objActives[4].toString())) {
					processActiveWebBean.setKeyActiveState(Integer.parseInt(objActives[4].toString()));
				} else {
					processActiveWebBean.setKeyActiveState(-1);
				}
			} else {
				processActiveWebBean.setKeyActiveState(-1);
			}
			// 关键说明
			if (objActives[5] != null) {
				String keyActive = objActives[5].toString().replaceAll("\n", "<br>");
				processActiveWebBean.setKeyActiveShow(keyActive);
			} else {
				processActiveWebBean.setKeyActiveShow("");
			}
			// 执行角色
			for (Object[] objRoleAndActiveRelate : listRoleAndActiveRelate) {
				if (objRoleAndActiveRelate == null || objRoleAndActiveRelate[0] == null
						|| objRoleAndActiveRelate[1] == null) {
					continue;
				}
				if (objActives[0].equals(objRoleAndActiveRelate[1])) {
					for (Object[] objRoleShow : listRoleShow) {
						if (objRoleShow == null || objRoleShow[0] == null) {
							continue;
						}
						if (objRoleAndActiveRelate[0].equals(objRoleShow[0])) {
							// 执行角色ID
							processActiveWebBean.setRoleFigureId(Long.valueOf(objRoleShow[0].toString()));
							// 角色名称
							if (objRoleShow[1] != null) {
								processActiveWebBean.setRoleName(objRoleShow[1].toString());
							}
							// 角色职责
							if (objRoleShow[2] != null) {
								processActiveWebBean.setRoleShow(objRoleShow[2].toString().replaceAll("\n", "<br>"));
							}
							break;
						}
					}
					break;
				}
			}
			// 活动名称
			if (objActives[2] != null)
				processActiveWebBean.setActiveName(objActives[2].toString());
			// 活动说明
			if (objActives[3] != null) {
				String activeShow = objActives[3].toString().replaceAll("\n", "<br>");
				processActiveWebBean.setActiveShow(activeShow);
			}
			// 输入说明、输出说明
			if (objActives[6] != null) {
				processActiveWebBean.setActivityInput(objActives[6].toString());
			}
			if (objActives[7] != null) {
				processActiveWebBean.setActivityOutput(objActives[7].toString());
			}

			if (objActives[8] != null) {// 接口（活动）
				processActiveWebBean.setLinkFlowId(Long.valueOf(objActives[8].toString()));
			}
			if (objActives[9] != null) {// 接口（活动）
				processActiveWebBean.setActivityFlowType(JecnUtil.isActivityFlow(objActives[9].toString()) ? 1 : 0);
			}
			// 输入
			List<FileWebBean> listInput = new ArrayList<FileWebBean>();
			// 活动操作规范
			List<FileWebBean> listOperation = new ArrayList<FileWebBean>();
			// 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号
			for (Object[] objInputFiles : listInputFiles) {
				if (objInputFiles == null || objInputFiles[0] == null || objInputFiles[1] == null
						|| objInputFiles[2] == null || objInputFiles[4] == null) {
					continue;
				}

				// 不是这个活动的输入
				if (!objActives[0].toString().equals(objInputFiles[1].toString())) {
					continue;
				}
				FileWebBean fileWebBean = new FileWebBean();
				// 文件ID
				fileWebBean.setFileId(Long.valueOf(objInputFiles[2].toString()));
				// 文件名称
				if (objInputFiles[3] != null)
					fileWebBean.setFileName(objInputFiles[3].toString());
				// 文件编号
				if (objInputFiles[5] != null) {
					fileWebBean.setFileNum(objInputFiles[5].toString());
				}
				// 操作规范
				if ("1".equals(objInputFiles[4].toString())) {
					listOperation.add(fileWebBean);
				} else {
					listInput.add(fileWebBean);
				}
			}
			// 活动输入
			processActiveWebBean.setListInput(listInput);
			// 活动操作规范
			processActiveWebBean.setListOperation(listOperation);

			// 输出
			List<ActiveModeBean> listMode = new ArrayList<ActiveModeBean>();
			// 流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
			for (Object[] objOutFiles : listOutFiles) {
				if (objOutFiles == null || objOutFiles[0] == null || objOutFiles[1] == null || objOutFiles[2] == null) {
					continue;
				}
				// 不是这个活动的输出
				if (!objActives[0].toString().equals(objOutFiles[1].toString())) {
					continue;
				}
				ActiveModeBean activeModeBean = new ActiveModeBean();
				FileWebBean fileWebBean = new FileWebBean();
				// 主键ID
				fileWebBean.setModeId(Integer.valueOf(objOutFiles[0].toString()));
				// 文件ID
				fileWebBean.setFileId(Long.valueOf(objOutFiles[2].toString()));
				// 文件名称
				if (objOutFiles[3] != null)
					fileWebBean.setFileName(objOutFiles[3].toString());
				// 文件编号
				if (objOutFiles[4] != null) {
					fileWebBean.setFileNum(objOutFiles[4].toString());
				}
				activeModeBean.setModeFile(fileWebBean);
				listMode.add(activeModeBean);
			}
			processActiveWebBean.setListMode(listMode);
			if (objActives.length > 6) {
				// 对应内控矩阵风险点
				if (objActives[6] != null) {
					processActiveWebBean.setInnerControlRisk(objActives[6].toString());
				}
				// 对应标准条款
				if (objActives[7] != null) {
					processActiveWebBean.setStandardConditions(objActives[7].toString());
				}
			}
			listResult.add(processActiveWebBean);
		}
		return listResult;
	}

	@Override
	public ProcessMapDownloadBean getProcessMapDownloadBean(Long processId) throws Exception {
		try {
			JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(processId);
			JecnMainFlow jecnMainFlow = (JecnMainFlow) processBasicDao.getSession().get(JecnMainFlow.class, processId);
			if (jecnFlowStructure == null) {
				return null;
			}
			ProcessMapDownloadBean processMapDownloadBean = new ProcessMapDownloadBean();
			// 流程Id
			processMapDownloadBean.setFlowId(processId);
			// 流程名称
			processMapDownloadBean.setFlowName(jecnFlowStructure.getFlowName());
			// 流程编号
			processMapDownloadBean.setFlowInputNum(jecnFlowStructure.getFlowIdInput());
			// // 流程密级
			// if (jecnFlowStructure.getIsPublic() != null)
			// processMapDownloadBean.setFlowIsPublic(jecnFlowStructure
			// .getIsPublic().toString());
			// // 公司的名称
			// // 获取公司名称
			// String companyName = configItemDao.selectValue(
			// JecnConfigContents.TYPE_BIG_ITEM_PUB,
			// ConfigItemPartMapMark.basicCmp.toString());
			// processMapDownloadBean.setCompanyName(companyName);
			// // 审核人 数据
			// List<Object[]> peopleList = new ArrayList<Object[]>();
			// String version = DownloadUtil.getJecnTaskHistoryNew(peopleList,
			// jecnFlowStructure.getHistoryId(), docControlDao);
			// // 版本
			// if (version != null) {
			// processMapDownloadBean.setFlowVersion(version);
			// } else {
			// processMapDownloadBean.setFlowVersion("");
			// }
			// processMapDownloadBean.setPeopleList(peopleList);
			// 目的
			processMapDownloadBean.setFlowAim(jecnMainFlow.getFlowAim());
			// 范围
			processMapDownloadBean.setFlowArea(jecnMainFlow.getFlowArea());
			// 术语定义
			processMapDownloadBean.setFlowNounDefine(jecnMainFlow.getFlowNounDefine());
			// 输入
			processMapDownloadBean.setFlowInput(jecnMainFlow.getFlowInput());
			// 输出
			processMapDownloadBean.setFlowOutput(jecnMainFlow.getFlowOutput());
			// 步骤
			processMapDownloadBean.setFlowShowStep(jecnMainFlow.getFlowShowStep());
			// 附件
			if (jecnMainFlow.getFileId() != null) {
				JecnFileBean jecnFileBean = fileDao.findJecnFileBeanById(jecnMainFlow.getFileId());
				if (jecnFileBean != null) {
					processMapDownloadBean.setFileId(jecnFileBean.getFileID());
					processMapDownloadBean.setFileName(jecnFileBean.getFileName());
				}
			}
			return processMapDownloadBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 
	 * @author fuzhh Jan 21, 2013
	 * @param processId
	 * @param type
	 *            0流程图 4流程地图
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTaskHistoryNew> getTaskHistoryNew(Long processId) throws Exception {
		try {
			return docControlDao.getTaskHistoryNewListById(processId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public RelatedProcessRuleStandardBean getRelatedProcessRuleStandard(Long processId, boolean isPub) throws Exception {
		try {
			RelatedProcessRuleStandardBean relatedProcessRuleStandardBean = new RelatedProcessRuleStandardBean();
			// 相关流程
			List<Object[]> listObjectsRelateFlows = flowStructureDao.findAllRelateFlowsWeb(processId, isPub);
			List<ProcessWebBean> relatedProcessList = new ArrayList<ProcessWebBean>();
			for (Object[] obj : listObjectsRelateFlows) {
				// t.flow_id,t.flow_name,t.flow_id_input,org_name,jfbi.type_res_people,jfbi.res_people_id,res_people,t.update_date,t.is_public,t.create_date,jfsi.linecolor
				if (obj[11] != null) {
					ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObject(obj);
					if (processWebBean != null) {
						processWebBean.setFlowType(Integer.parseInt(obj[11].toString()));
						relatedProcessList.add(processWebBean);
					}
				}
			}
			relatedProcessRuleStandardBean.setRelatedProcessList(relatedProcessList);
			// 相关制度
			List<Object[]> listObjectsRelateRules = processBasicDao.getFlowRelateRulesWeb(processId, isPub);
			List<RuleInfoBean> relatedRuleList = new ArrayList<RuleInfoBean>();
			for (Object[] obj : listObjectsRelateRules) {
				RuleInfoBean ruleInfoBean = JecnUtil.getRuleInfoBeanByObject(obj);
				if (ruleInfoBean != null)
					relatedRuleList.add(ruleInfoBean);
			}
			relatedProcessRuleStandardBean.setRelatedRuleList(relatedRuleList);
			// 相关标准
			List<Object[]> listObjectsRelateStandrads = flowStructureDao.getFlowStandards(processId, isPub);
			List<StandardWebBean> relatedStandardList = new ArrayList<StandardWebBean>();
			for (Object[] obj : listObjectsRelateStandrads) {
				if (obj == null || obj[0] == null || obj[3] == null) {
					continue;
				}
				StandardWebBean standardWebBean = new StandardWebBean();
				// 主键Id
				standardWebBean.setId(Long.valueOf(obj[0].toString()));
				// 标准名称
				if (obj[1] != null)
					standardWebBean.setName(obj[1].toString());
				// 关联Id
				if (obj[2] != null) {
					standardWebBean.setFileId(Long.valueOf(obj[2].toString()));
				}
				// 类型
				standardWebBean.setType(obj[3].toString());
				relatedStandardList.add(standardWebBean);
			}
			relatedProcessRuleStandardBean.setRelatedStandardList(relatedStandardList);
			// 相关风险
			List<JecnRisk> relatedRiskList = flowStructureDao.getRiskByFlowId(processId, isPub);
			relatedProcessRuleStandardBean.setRelatedRiskList(relatedRiskList);
			return relatedProcessRuleStandardBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public IJecnConfigItemDao getConfigItemDao() {
		return configItemDao;
	}

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}

	public IJecnDocControlDao getDocControlDao() {
		return docControlDao;
	}

	public void setDocControlDao(IJecnDocControlDao docControlDao) {
		this.docControlDao = docControlDao;
	}

	public IProcessKPIDao getProcessKPIDao() {
		return processKPIDao;
	}

	public void setProcessKPIDao(IProcessKPIDao processKPIDao) {
		this.processKPIDao = processKPIDao;
	}

	public IProcessRecordDao getProcessRecordDao() {
		return processRecordDao;
	}

	public void setProcessRecordDao(IProcessRecordDao processRecordDao) {
		this.processRecordDao = processRecordDao;
	}

	/**
	 * 通过对象获得ProcessWebBean
	 * 
	 * @param obj
	 * @return
	 */
	private ProcessWebBean getProcessWebBeanByObj(Object[] obj) {
		ProcessWebBean processWebBean = new ProcessWebBean();
		// 流程ID
		processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
		// 父节点ID
		processWebBean.setParentFlowId(Long.valueOf(obj[1].toString()));
		// 流程名称
		if (obj[2] != null)
			processWebBean.setFlowName(obj[2].toString());
		// 流程编号
		if (obj[3] != null)
			processWebBean.setFlowIdInput(obj[3].toString());
		// 流程类型
		processWebBean.setIsflow(Integer.parseInt(obj[5].toString()));
		// 当前版本号
		if (obj[6] != null)
			processWebBean.setVersionId(obj[6].toString());
		// 责任部门
		if (obj[7] != null && obj[11] != null) {
			processWebBean.setOrgName(obj[7].toString());
			processWebBean.setOrgId(Long.valueOf(obj[11].toString()));
		}
		// 责任人类型
		if (obj[8] != null && obj[9] != null && obj[10] != null) {
			// 责任人类型
			processWebBean.setTypeResPeople(Integer.parseInt(obj[8].toString()));
			// 责任人ID
			processWebBean.setResPeopleId(Long.valueOf(obj[9].toString()));
			// 责任人名称
			processWebBean.setResPeopleName(obj[10].toString());
		}
		// if ("1".equals(obj[5].toString())) {
		// 流程状态
		if (obj[12] != null) {
			// 发布
			processWebBean.setTypeByData(2);
			// 发布日期
			if (obj[4] != null)
				processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[4].toString())));
		} else {
			if (obj[13] != null) {
				// 审批中
				processWebBean.setTypeByData(1);
			} else {
				// 待建
				processWebBean.setTypeByData(0);
			}
		}
		// }
		// 监护人ID
		if (obj[15] != null) {
			processWebBean.setGuardianId(Long.valueOf(obj[15].toString()));
		}
		// 监护人名称
		if (obj[16] != null) {
			processWebBean.setGuardianName(obj[16].toString());
		} else {
			processWebBean.setGuardianName("");
		}
		// 拟稿人
		if (obj[17] != null) {
			processWebBean.setDraftPerson(obj[17].toString());
		} else {
			processWebBean.setDraftPerson("");
		}
		int expiry = 0;
		// 有效期
		if (obj[18] != null) {
			expiry = Integer.valueOf(obj[18].toString());
			processWebBean.setExpiry(expiry);
		}

		// 下次审视需完成的时间
		String nextScanDate = "";
		// 有文控记录
		if (obj[19] != null) {
			nextScanDate = JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[19].toString()));
		}
		processWebBean.setNextScanDate(nextScanDate);

		// 是否及时优化 0空 ,1是 ,2否
		int optimization = 0;
		// 已发布的且非永久且下次审视时间不为空
		if (obj[4] != null && expiry != 0 && nextScanDate != null && !"".equals(nextScanDate)) {

			Date nextScan = JecnCommon.getDateByString(nextScanDate);
			Date nowDate = new Date();
			// 当前时间在下次审视时间之前 则为否
			if (nextScan.before(nowDate)) {
				optimization = 2;
			}
		}
		processWebBean.setOptimization(optimization);

		return processWebBean;
	}

	/**
	 * 查询流程处在的级别
	 * 
	 * @author fuzhh 2013-12-5
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public int findFlowDepth(Long flowId) throws Exception {
		JecnFlowStructureT bean = this.flowStructureDao.get(flowId);
		return bean.gettLevel();
	}

	@Override
	public ProcessSystemListBean getProcessSystemList(Long processMapId, Long projectId, int depth, boolean isAll)
			throws Exception {
		try {
			List<Long> listIds = new ArrayList<Long>();
			listIds.add(processMapId);
			String sqlCommon = "( SELECT jfst.* FROM jecn_flow_structure_t jfst INNER JOIN ("
					+ JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.processMap)
					+ ") sub on sub.flow_id=jfst.flow_Id" + "  WHERE jfst.projectid=" + projectId + ")";
			String sql = " select distinct jfs.flow_id,jfs.pre_flow_id,jfs.flow_name,"
					+ "       jfs.flow_id_input,jfs.pub_time,jfs.isflow,"
					+ "       jt.version_id,jfo.org_name,t.type_res_people,"
					+ "       t.res_people_id,case when t.type_res_people = 0 then ju.true_name"
					+ "        when t.type_res_people = 1 then jfoi.figure_text"
					+ "       end as res_people, jfo.org_id,"
					+ "       pub.flow_id as p_flow_id, jtbn.state,jfs.t_level,"
					+ "       gju.people_id,gju.TRUE_NAME,"
					+ "       jt.draft_person,"
					+ "       jt.expiry,jt.NEXT_SCAN_DATE,jfs.sort_id"
					+ "  from "
					+ sqlCommon
					+ " jfs"
					+ "  left join jecn_flow_basic_info_t t on t.flow_id = jfs.flow_id"
					+ "  left join jecn_task_history_new jt on jt.id = jfs.history_id"
					+ "  left join jecn_user ju on t.type_res_people = 0 and ju.people_id = t.res_people_id and ju.isLock=0"
					+ "  left join jecn_flow_org_image jfoi on t.type_res_people = 1 and jfoi.figure_id=t.res_people_id"
					+ "  left join jecn_flow_related_org_t jfro on t.flow_id=jfro.flow_id"
					+ "  left join jecn_flow_org jfo on jfro.org_id = jfo.org_id  and jfo.del_state = 0"
					+ "  left join jecn_flow_structure pub on pub.flow_id = jfs.flow_id"
					+ "  left join jecn_task_bean_new jtbn on jtbn.task_type = 0"
					+ "                                   and jtbn.r_id = jfs.flow_id"
					+ "                                   and jtbn.is_lock = 1"
					+ "                                   and jtbn.state <> 5"
					+ "  left join jecn_user gju on gju.people_id = t.Ward_People_Id" + "  where jfs.del_state = 0"
					+ " order by jfs.pre_flow_id, jfs.sort_id,jfs.flow_id";
			List<Object[]> listAll = this.flowStructureDao.listNativeSql(sql);
			// 存储流程的集合
			List<ProcessWebBean> listResult = new ArrayList<ProcessWebBean>();
			// 获取最大的 级别数
			int maxCount = 0;
			// 待建流程个数
			int noPubFlowTotal = 0;
			// 审批流程个数
			int approveFlowTotal = 0;
			// 发布流程个数
			int pubFlowTotal = 0;
			// 流程个数
			int processCount = 0;
			// 循环查询出来的数据
			for (Object[] obj : listAll) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[5] == null || obj[14] == null) {
					continue;
				}
				ProcessWebBean processWebBean = getProcessWebBeanByObj(obj);
				listResult.add(processWebBean);
				// 获取级别数
				int lev = Integer.valueOf(obj[14].toString());
				processWebBean.setLevel(lev);
				if (processWebBean.getIsflow() == 0) {
					if (lev > maxCount) {
						maxCount = lev;
					}
				} else if (processWebBean.getIsflow() == 1) {
					processCount++;
					if (processWebBean.getTypeByData() == 0) {
						noPubFlowTotal++;
					} else if (processWebBean.getTypeByData() == 1) {
						approveFlowTotal++;
					} else if (processWebBean.getTypeByData() == 2) {
						pubFlowTotal++;
					}
				}
			}
			ProcessSystemListBean processSystemListBean = new ProcessSystemListBean();
			// 设置总级别数
			processSystemListBean.setLevelTotal(maxCount + 1);
			// 流程个数
			processSystemListBean.setFlowTotal(processCount);
			// 待建流程个数
			processSystemListBean.setNoPubFlowTotal(noPubFlowTotal);
			// 审批流程个数
			processSystemListBean.setApproveFlowTotal(approveFlowTotal);
			// 发布流程个数
			processSystemListBean.setPubFlowTotal(pubFlowTotal);
			// 流程清单数据排序
			List<ProcessWebBean> listSortResult = new ArrayList<ProcessWebBean>();
			Set<Long> flowIdSet = new HashSet<Long>();
			processListSort(processMapId, listResult, listSortResult, flowIdSet);
			// 设置流程数据
			processSystemListBean.setListProcessWebBean(listSortResult);
			return processSystemListBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程清单数据排序
	 * 
	 * @author fuzhh 2013-12-12
	 * @param processSystemListBean
	 * @param sortProcessList
	 */
	private void processListSort(Long flowId, List<ProcessWebBean> listResult, List<ProcessWebBean> listSortResult,
			Set<Long> flowIdSet) {
		for (ProcessWebBean processWebBean : listResult) {
			if (flowId.equals(processWebBean.getFlowId())) {
				listSortResult.add(processWebBean);
			} else if (flowId.equals(processWebBean.getParentFlowId())) {
				boolean isQuery = false;
				for (Long processId : flowIdSet) {
					if (processId.equals(processWebBean.getFlowId())) {
						isQuery = true;
					}
				}
				if (!isQuery) {
					flowIdSet.add(processWebBean.getFlowId());
					processListSort(processWebBean.getFlowId(), listResult, listSortResult, flowIdSet);
				}
			}
		}
	}

	@Override
	public ProcessMapBaseInfoBean getProcessMapBaseInfo(Long processMapId, boolean isPub) throws Exception {
		try {
			ProcessMapBaseInfoBean processMapBaseInfoBean = new ProcessMapBaseInfoBean();
			// 流程ID
			processMapBaseInfoBean.setFlowMapId(processMapId);
			if (isPub) {
				JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(processMapId);
				if (jecnFlowStructure == null) {
					return null;
				}
				// 流程地图名称
				if (jecnFlowStructure.getFlowName() != null)
					processMapBaseInfoBean.setFlowMapName(jecnFlowStructure.getFlowName());
				// 流程地图编号
				if (jecnFlowStructure.getFlowIdInput() != null)
					processMapBaseInfoBean.setFlowMapIdInput(jecnFlowStructure.getFlowIdInput());
				// 密级
				if (jecnFlowStructure.getIsPublic() != null)
					processMapBaseInfoBean.setIsPublic(jecnFlowStructure.getIsPublic().intValue());

			} else {
				JecnFlowStructureT jecnFlowStructure = flowStructureDao.get(processMapId);
				if (jecnFlowStructure == null) {
					return null;
				}
				// 流程地图名称
				if (jecnFlowStructure.getFlowName() != null)
					processMapBaseInfoBean.setFlowMapName(jecnFlowStructure.getFlowName());
				// 流程地图编号
				if (jecnFlowStructure.getFlowIdInput() != null)
					processMapBaseInfoBean.setFlowMapIdInput(jecnFlowStructure.getFlowIdInput());
				// 密级
				if (jecnFlowStructure.getIsPublic() != null)
					processMapBaseInfoBean.setIsPublic(jecnFlowStructure.getIsPublic().intValue());
			}
			// 岗位查询权限
			processMapBaseInfoBean.setPosList(positionAccessPermissionsDao.getPositionAccessPermissions(processMapId,
					0, isPub));
			// 部门查询权限
			processMapBaseInfoBean.setOrgList(orgAccessPermissionsDao.getOrgAccessPermissions(processMapId, 0, isPub));
			return processMapBaseInfoBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 搜索责任部门
	 * 
	 * @param processSearchBean
	 * @return
	 */
	private String searchDeptProcessSql(ProcessSearchBean processSearchBean) {
		String sql = " from jecn_flow_structure t ,jecn_flow_related_org jfro ,jecn_flow_org jfo,jecn_flow_basic_info jfbi"
				+ " left join jecn_user ju on ju.people_id=jfbi.res_people_id and ju.islock=0"
				+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org_image jfoi,jecn_flow_org jfo_t where jfoi.org_id=jfo_t.org_id and jfo_t.del_state=0 and jfoi.figure_type="
				+ JecnCommonSql.getPosString()
				+ ") pos on pos.figure_id=jfbi.res_people_id"
				+ " where t.isflow=1 and t.flow_id=jfro.flow_id "
				+ " and jfro.org_id = jfo.org_id and jfo.del_state=0 and t.del_state=0 and t.projectid="
				+ processSearchBean.getProjectId() + " and t.flow_id=jfbi.flow_id";

		// 流程名称
		if (StringUtils.isNotBlank(processSearchBean.getProcessName())) {
			sql = sql + " and t.flow_name like '%" + processSearchBean.getProcessName() + "%'";
		}
		// 流程编号
		if (StringUtils.isNotBlank(processSearchBean.getProcessNum())) {
			sql = sql + " and t.flow_id_input like '%" + processSearchBean.getProcessNum() + "%'";
		}
		// 责任部门
		if (processSearchBean.getOrgId() != -1) {
			sql = sql + " and jfro.org_id=" + processSearchBean.getOrgId();
		} else {
			sql = sql
					+ " and jfro.org_id in (select jfoi.org_id from jecn_user_position_related jupr,jecn_flow_org_image jfoi,jecn_flow_org org where  jupr.people_id="
					+ processSearchBean.getPeopleId()
					+ " and jupr.figure_id = jfoi.figure_id and jfoi.org_id=org.org_id and org.del_state=0)";
		}
		// 类别
		if (processSearchBean.getProcessType() != -1) {
			sql = sql + " and jfbi.type_id=" + processSearchBean.getProcessType();
		}
		// 密级
		if (processSearchBean.getSecretLevel() != -1) {
			sql = sql + " and t.is_public=" + processSearchBean.getSecretLevel();
		}
		return sql;
	}

	@Override
	public int getTotalSearchDeptProcess(ProcessSearchBean processSearchBean) throws Exception {
		try {
			String sql = "select count(t.flow_id)" + searchDeptProcessSql(processSearchBean);
			return flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessWebBean> searchDeptProcess(ProcessSearchBean processSearchBean, int start, int limit)
			throws Exception {

		try {
			String sql = "select t.flow_id,t.flow_name,t.flow_id_input," + "      jfo.org_id, jfo.org_name"
					+ "       ,jfbi.type_res_people,jfbi.res_people_id,"
					+ "       case when jfbi.type_res_people=0 then ju.true_name"
					+ "       when jfbi.type_res_people=1 then pos.figure_text" + "       end as res_people"
					+ "       ,t.pub_time,t.is_public,t.create_date" + searchDeptProcessSql(processSearchBean)
					+ "		 order by t.pub_time desc";

			List<Object[]> listObject = flowStructureDao.listNativeSql(sql, start, limit);
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
			for (Object[] obj : listObject) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessWebBean processWebBean = new ProcessWebBean();
				// 流程ID
				processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null) {
					processWebBean.setFlowName(obj[1].toString());
				}
				// 流程编号
				if (obj[2] != null) {
					processWebBean.setFlowIdInput(obj[2].toString());
				}
				// 责任部门
				if (obj[3] != null && obj[4] != null) {
					processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
					processWebBean.setOrgName(obj[4].toString());
				}
				// 责任人
				if (obj[5] != null && obj[6] != null && obj[7] != null) {
					processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
					processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
					processWebBean.setResPeopleName(obj[7].toString());
				}
				// 更新时间
				if (obj[8] != null && obj[10] != null) {
					processWebBean
							.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[8].toString())));
					processWebBean.setNoUpdateDate(JecnCommon.getMonth(JecnCommon.getDateByString(obj[10].toString()),
							JecnCommon.getDateByString(obj[8].toString())));
				}
				// 密级
				if (obj[9] != null) {
					processWebBean.setIsPublic(Integer.parseInt(obj[9].toString()));
				}
				listProcessWebBean.add(processWebBean);
			}
			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public JecnCreateDoc printFlowDoc(Long refId, TreeNodeType nodeType, boolean isPub, Long userId, Long historyId) {
		// 获得流程地图和流程发布时候所生成的流程文件
		try {
			JecnCreateDoc createDoc = null;
			if (historyId != null && historyId > 0) {
				if (TreeNodeType.process == nodeType) {
					JecnTaskHistoryNew history = getJecnTaskHistoryNew(historyId);
					String flowName = getHistoryFlowName(refId, historyId);
					if (history != null && StringUtils.isNotBlank(history.getFilePath())) {
						File file = new File(JecnContants.jecn_path + history.getFilePath());
						if (file.exists()) {
							createDoc = new JecnCreateDoc();
							createDoc.setFileName(flowName + ".doc");
							createDoc.setBytes(FileUtils.readFileToByteArray(file));
							return createDoc;
						}
					}
				}
			} else {
				createDoc = DownloadUtil.getFlowFile(refId, nodeType, isPub, configItemDao, docControlDao,
						flowStructureDao, processBasicDao, processKPIDao, processRecordDao);
			}
			JecnUserTotal userTotal = new JecnUserTotal();
			userTotal.setAccessDate(new Date());
			userTotal.setFlowId(refId);
			userTotal.setPeopleId(userId);
			userTotal.setType(1);
			userTotal.setAccessFrom(0);
			userTotalDao.save(userTotal);
			return createDoc;
		} catch (Exception e) {
			log.error("浏览的生成操作说明异常！", e);
			return null;
		}
	}

	public JecnCreateDoc printFlowPDF(Long refId, TreeNodeType nodeType, boolean isPub, Long userId, Long historyId) {
		// 获得流程地图和流程发布时候所生成的流程文件
		try {
			JecnCreateDoc createDoc = null;
			if (historyId != null) {
				if (TreeNodeType.process == nodeType) {
					JecnTaskHistoryNew history = getJecnTaskHistoryNew(historyId);
					String flowName = getHistoryFlowName(refId, historyId);
					if (history != null && StringUtils.isNotBlank(history.getFilePath())) {
						File file = new File(JecnContants.jecn_path + history.getFilePath());
						if (file.exists()) {
							createDoc = new JecnCreateDoc();
							createDoc.setFileName(flowName + ".doc");
							createDoc.setBytes(FileUtils.readFileToByteArray(file));
							return createDoc;
						}
					}
				}
			} else {
				createDoc = DownloadUtil.getFlowFile(refId, nodeType, isPub, configItemDao, docControlDao,
						flowStructureDao, processBasicDao, processKPIDao, processRecordDao);
			}
			JecnUser jecnUser = userDao.get(userId);

			byte[] pdfByte = ViewDocsManager.getBytesByProcessDocToPDF(createDoc.getBytes(), "doc2pdf.doc", "PROCESS",
					jecnUser.getTrueName());
			createDoc.setBytes(pdfByte);
			JecnUserTotal userTotal = new JecnUserTotal();
			userTotal.setAccessDate(new Date());
			userTotal.setFlowId(refId);
			userTotal.setPeopleId(userId);
			userTotal.setType(1);
			userTotal.setAccessFrom(0);
			userTotalDao.save(userTotal);
			return createDoc;
		} catch (Exception e) {
			log.error("浏览的生成操作说明异常！", e);
			return null;
		}
	}

	private String getHistoryFlowName(Long flowId, Long historyId) {
		String sql = "select a.flow_id,a.flow_name from jecn_flow_structure_h a where a.flow_id=? and a.history_id=?";
		List<Object[]> objs = this.docControlDao.listNativeSql(sql, flowId, historyId);
		if (objs.size() > 0) {
			return JecnUtil.nullToEmpty(objs.get(0)[1]);
		}
		return flowStructureDao.get(flowId).getFlowName();
	}

	@Override
	public List<ProcessActiveWebBean> searchMyActive(String activeName, String processName, String processNum,
			String startDate, String endDate, Long peopleId, Long projectId) throws Exception {
		String sql = "select distinct jfsi_t.figure_id,jfsi_t.figure_text,jfsi_t.activity_id,t.flow_id,t.flow_name,t.flow_id_input"
				+ "  ,org.org_name,jfbi.type_res_people,jfbi.res_people_id"
				+ "     ,case when jfbi.type_res_people=0 then ju.true_name"
				+ "     when jfbi.type_res_people=1 then pos.figure_text"
				+ "      end as res_people"
				+ ",t.pub_time,t.is_public,t.create_date,jfsi_t.start_time,jfsi_t.stop_time,jfsi_t.time_type,org.org_id"
				+ " from jecn_flow_structure_image jfsi_t,jecn_flow_structure t"
				+ ",jecn_flow_basic_info jfbi"
				+ " left join (select jfro.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) org on org.flow_id=jfbi.flow_id"
				+ " left join jecn_user ju on ju.people_id=jfbi.res_people_id and ju.islock=0"
				+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org_image jfoi,jecn_flow_org jfo_t where jfoi.org_id=jfo_t.org_id and jfo_t.del_state=0 and jfoi.figure_type="
				+ JecnCommonSql.getPosString()
				+ ") pos on pos.figure_id=jfbi.res_people_id"
				+ " where jfsi_t.flow_id=t.flow_id"
				+ "     and t.del_state=0 and t.projectid="
				+ projectId
				+ " and t.flow_id=jfbi.flow_id and jfsi_t.figure_id in ("
				+ "select jra.figure_active_id from jecn_flow_structure_image jfsi,jecn_role_active jra"
				+ "  where  jfsi.figure_id=jra.figure_role_id and"
				+ "       (jfsi.figure_id in (select ps.figure_flow_id from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related ps,jecn_user_position_related jupr"
				+ "         where ps.figure_position_id = jfoi.figure_id and ps.type='0' and jfoi.org_id=jfo.org_id"
				+ "         and jfo.del_state=0 and jfoi.figure_id=jupr.figure_id and jupr.people_id="
				+ peopleId
				+ ")"
				+ "       or"
				+ "       jfsi.figure_id in  (select ps.figure_flow_id from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related ps"
				+ "          ,jecn_position_group_r jpgr,jecn_user_position_related jupr where ps.figure_position_id=jpgr.group_id and ps.type='1' and jpgr.figure_id=jfoi.figure_id"
				+ "          and jfoi.org_id=jfo.org_id and jfo.del_state=0 and jfoi.figure_id=jupr.figure_id and jupr.people_id="
				+ peopleId + ")))";
		// 活动名称
		if (StringUtils.isNotBlank(activeName)) {
			sql = sql + " and  jfsi_t.figure_text like '%" + activeName + "%'";
		}
		// 流程名称
		if (StringUtils.isNotBlank(processName)) {
			sql = sql + " and  t.flow_name like '%" + processName + "%'";
		}
		// 流程编号
		if (StringUtils.isNotBlank(processNum)) {
			sql = sql + " and  t.flow_id_input like '%" + processNum + "%'";
		}
		List<Object[]> listObj = flowStructureDao.listNativeSql(sql);

		List<ProcessActiveWebBean> listResult = new ArrayList<ProcessActiveWebBean>();
		for (Object[] obj : listObj) {
			if (obj == null || obj[0] == null || obj[3] == null) {
				continue;
			}
			// 活动主键
			ProcessActiveWebBean processActiveWebBean = new ProcessActiveWebBean();
			processActiveWebBean.setActiveFigureId(Long.valueOf(obj[0].toString()));
			// 活动名称
			if (obj[1] != null)
				processActiveWebBean.setActiveName(obj[1].toString());
			// 活动编号
			if (obj[2] != null)
				processActiveWebBean.setActiveId(obj[2].toString());
			// 流程主键
			ProcessWebBean processWebBean = new ProcessWebBean();
			processWebBean.setFlowId(Long.valueOf(obj[3].toString()));
			// 流程名称
			if (obj[4] != null) {
				processWebBean.setFlowName(obj[4].toString());
			}
			// 流程编号
			if (obj[5] != null) {
				processWebBean.setFlowIdInput(obj[5].toString());
			}
			// 责任部门
			if (obj[6] != null) {
				processWebBean.setOrgName(obj[6].toString());
			}
			// 责任部门ID
			if (obj[16] != null) {
				processWebBean.setOrgId(Long.valueOf(obj[16].toString()));
			}
			// 责任人
			if (obj[7] != null && obj[8] != null && obj[9] != null) {
				processWebBean.setTypeResPeople(Integer.parseInt(obj[7].toString()));
				processWebBean.setResPeopleId(Long.valueOf(obj[8].toString()));
				processWebBean.setResPeopleName(obj[9].toString());
			}
			// 更新时间
			if (obj[10] != null && obj[12] != null) {
				processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[10].toString())));
				processWebBean.setNoUpdateDate(JecnCommon.getMonth(JecnCommon.getDateByString(obj[12].toString()),
						JecnCommon.getDateByString(obj[10].toString())));
			}
			// 密级
			if (obj[11] != null) {
				processWebBean.setIsPublic(Integer.parseInt(obj[11].toString()));
			}
			// //开始时间
			// if(obj[13] != null){
			// processActiveWebBean.setStartTime(obj[13].toString());
			// }
			// //结束时间
			// if(obj[14] != null){
			// processActiveWebBean.setStopTime(obj[14].toString());
			// }

			// else if(obj[13] == null && obj[14] == null){
			// processActiveWebBean.setStartTime(newDate.format(new Date()));
			// processActiveWebBean.setStopTime(obj[14].toString());
			// }
			// 时间类型
			SimpleDateFormat newDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat newDates = new SimpleDateFormat("yyyy-MM-dd");
			if (startDate != null && !"".equals(startDate)) {
				if (endDate == null || "".equals(endDate)) {
					return null;
				} else {
					if (obj[15] != null) {
						processActiveWebBean.setTimeType(Integer.valueOf(obj[15].toString()));
						// 开始时间obj[13] //结束时间obj[14]
						String[] strStarts = null;
						String[] strEnds = null;
						String strStart = null;
						String strEnd = null;
						// 条件输入的开始时间， 结束时间
						String[] strInputStarts = null;
						String[] strInputEnds = null;
						String strInputStart = null;
						String strInputEnd = null;
						// //条件输入的开始时间， 结束时间
						strInputStarts = startDate.split("-");
						strInputEnds = endDate.split("-");
						strInputStart = strInputStarts[1] + strInputStarts[2];
						strInputEnd = strInputEnds[1] + strInputEnds[2];
						if (obj[13] != null && !"".equals(obj[13])) {
							// 数据库中的开始结束时间，逗号隔开
							strStarts = obj[13].toString().split(",");
							strEnds = obj[14].toString().split(",");
							if (obj[15].toString().equals("1")) {// 日
								// 逗号隔开
								// strStarts = obj[13].toString().split(",");
								// strEnds = obj[14].toString().split(",");
								processActiveWebBean.setStartTime(obj[13].toString());
								processActiveWebBean.setStopTime(obj[14].toString());
								processActiveWebBean.setProcessWebBean(processWebBean);
								listResult.add(processActiveWebBean);
							} else if (obj[15].toString().equals("2")) {// 周
								// 输入的数据，开始时间和结束时间相隔天数
								long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
								long diff = newDates.parse(endDate).getTime() - newDates.parse(startDate).getTime();
								long day = diff / nd;// 计算差多少天
								// 判断相差天数，若大于7，则表示，时间段搜索条件从星期一到星期日，显示所有符合条件的周类型的数据
								if (day >= 7) {
									processActiveWebBean.setStartTime(obj[13].toString());
									processActiveWebBean.setStopTime(obj[14].toString());
									processActiveWebBean.setProcessWebBean(processWebBean);
									listResult.add(processActiveWebBean);
								} else {// 时间段相差的天数小宇7天，则判断相差时间的星期差
									final String dayNames[] = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
									// 输入开始时间：周
									String startWeekString = "";
									int startWeekInt = 0;
									Calendar calendarStart = Calendar.getInstance();
									calendarStart.setTime(newDates.parse(startDate));
									int dayOfWeek = calendarStart.get(Calendar.DAY_OF_WEEK);
									startWeekString = dayNames[dayOfWeek - 1];
									for (int i = 0; i < dayNames.length; i++) {
										if (startWeekString.equals(dayNames[i])) {
											startWeekInt = i;
										}
									}
									// 输入结束时间：周
									String endWeekString = "";
									int endWeekInt = 0;
									Calendar calendarEnd = Calendar.getInstance();
									calendarEnd.setTime(newDates.parse(endDate));
									int dayOfWeekEnd = calendarEnd.get(Calendar.DAY_OF_WEEK);
									endWeekString = dayNames[dayOfWeekEnd - 1];
									for (int i = 0; i < dayNames.length; i++) {
										if (endWeekString.equals(dayNames[i])) {
											endWeekInt = i;
										}
									}
									// 搜索条件开始结束时间 段，包含的星期数===========
									List<String> weekLists = new ArrayList<String>();
									// 搜索条件 开始时间大于结束时间
									if (startWeekInt > endWeekInt) {
										for (int s = 0; s < 7; s++) {
											if (startWeekInt <= s) {
												weekLists.add(String.valueOf(s));
											}
										}
										for (int e = 6; e >= 0; e--) {
											if (endWeekInt >= e) {
												weekLists.add(String.valueOf(e));
											}
										}
									}
									// 搜索条件 开始时间小于结束时间
									else if (startWeekInt < endWeekInt) {
										for (int s = 0; s < 7; s++) {
											// 大于开始时间并且小于结束时间
											if (startWeekInt <= s && s <= endWeekInt) {
												weekLists.add(String.valueOf(s));
											}
										}
									} else if (startWeekInt == endWeekInt) {
										weekLists.add(String.valueOf(startWeekInt));
									}
									// 数据库中的开始时间 是否存在于搜索条件 时间段内时间
									if (weekLists.contains(String.valueOf(Integer.parseInt(obj[13].toString()) + 1))) {
										processActiveWebBean.setStartTime(obj[13].toString());
										processActiveWebBean.setStopTime(obj[14].toString());
										processActiveWebBean.setProcessWebBean(processWebBean);
										listResult.add(processActiveWebBean);
									}
								}
							} else if (obj[15].toString().equals("3")) {// 月
								// 输入的数据，开始时间和结束时间相隔天数
								long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
								long diff = newDates.parse(endDate).getTime() - newDates.parse(startDate).getTime();
								long day = diff / nd;// 计算差多少天
								// 输入搜索条件时间段内所有日期数
								List<String> monthsList = new ArrayList<String>();
								// 数据库中时间段
								List<String> dateMonthsList = new ArrayList<String>();
								// 判断相差天数，若大于31，则表示，时间段搜索条件从星期一到星期日，显示所有符合条件的周类型的数据
								if (day >= 31) {
									processActiveWebBean.setStartTime(obj[13].toString());
									processActiveWebBean.setStopTime(obj[14].toString());
									processActiveWebBean.setProcessWebBean(processWebBean);
									listResult.add(processActiveWebBean);
								} else {
									Calendar calendarStart = Calendar.getInstance();
									calendarStart.setTime(newDates.parse(endDate));
									String singleMoth = null;
									for (int m = 0; m <= day; m++) {
										if (m == 0) {
											calendarStart.add(Calendar.DATE, -m);
										} else {
											calendarStart.add(Calendar.DATE, -1);
										}

										singleMoth = newDates.format(calendarStart.getTime()).split("-")[2];
										if (singleMoth.toString().substring(0, 1).equals("0")) {
											singleMoth = singleMoth.toString().substring(1, 2);
										}
										monthsList.add(singleMoth);
									}
									// 数据库中的开始时间是否存在于 搜索条件时间段内的时间
									if (monthsList.contains(String.valueOf(Integer.parseInt(obj[13].toString()) + 1))) {
										processActiveWebBean.setStartTime(obj[13].toString());
										processActiveWebBean.setStopTime(obj[14].toString());
										processActiveWebBean.setProcessWebBean(processWebBean);
										listResult.add(processActiveWebBean);
									}

								}

							} else if (obj[15].toString().equals("4")) {// 季度
								// 判断 输入的时间处于哪个季度
								String dateStartStr = null;
								String dateEndStr = null;
								// 所处的季度的首月日期
								dateStartStr = this.getSean(strInputStarts, strInputStarts[0]);
								dateEndStr = this.getSean(strInputEnds, strInputEnds[0]);
								// 开始时间所处季度的 第几天
								long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
								long diffStart = newDates.parse(startDate).getTime()
										- newDates.parse(dateStartStr).getTime();
								long dayStart = diffStart / nd;
								// 结束时间所处季度的 第几天
								long diffEnd = newDates.parse(endDate).getTime() - newDates.parse(dateEndStr).getTime();
								long dayEnd = diffEnd / nd;

								// 输入的数据，开始时间和结束时间相隔天数
								long sean = 1000 * 24 * 60 * 60;// 一天的毫秒数
								long diff = newDates.parse(endDate).getTime() - newDates.parse(startDate).getTime();
								long day = diff / sean;// 计算差多少天

								// 搜索条件 开始结束时间段
								List<String> inputSeanList = new ArrayList<String>();
								// 数据库开始结束时间段
								List<String> dataSeanList = new ArrayList<String>();

								// 判断相差天数，若大于92，则显示所有符合条件的季类型的数据
								if (day >= 92) {
									processActiveWebBean.setStartTime(obj[13].toString());
									processActiveWebBean.setStopTime(obj[14].toString());
									processActiveWebBean.setProcessWebBean(processWebBean);
									listResult.add(processActiveWebBean);
								} else {

									// 搜索输入条件开始时间大于结束时间 (跨季度)=========
									if (dayStart > dayEnd) {
										for (int s = 0; s <= 92; s++) {
											if (dayStart <= s) {
												inputSeanList.add(String.valueOf(s));
											}
										}
										for (int e = 92; e >= 0; e--) {
											if (dayEnd >= e) {
												inputSeanList.add(String.valueOf(e));
											}
										}
									}
									// 搜索输入条件开始时间小于结束时间(同一季度)
									else if (dayStart < dayEnd) {
										for (int s = 0; s <= 92; s++) {
											if (dayStart <= s && s <= dayEnd) {
												inputSeanList.add(String.valueOf(s));
											}
										}
									} else if (dayStart == dayEnd) {
										// for (int s = 0; s <= 92; s++) {
										// if (dayStart <= s && s <= dayEnd) {
										inputSeanList.add(String.valueOf(dayStart));
										// }
										// }
									}
									// 数据库中的开始时间 是否存在于搜索条件时间段内的时间，存在显示数据
									if (inputSeanList
											.contains(String.valueOf(Integer.parseInt(obj[13].toString()) + 1))) {
										processActiveWebBean.setStartTime(obj[13].toString());
										processActiveWebBean.setStopTime(obj[14].toString());
										processActiveWebBean.setProcessWebBean(processWebBean);
										listResult.add(processActiveWebBean);
									}
								}
							} else if (obj[15].toString().equals("5")) {// 年
								strStarts[0] = String.valueOf(Integer.parseInt(strStarts[0]) + 1);
								strStarts[1] = String.valueOf(Integer.parseInt(strStarts[1]) + 1);
								if (strStarts[0].length() == 1) {
									strStarts[0] = "0" + strStarts[0];
								}
								if (strStarts[1].length() == 1) {
									strStarts[1] = "0" + strStarts[1];
								}
								strStart = strStarts[0] + strStarts[1];
								strEnd = strEnds[0] + strEnds[1];

								// 如果开始时间与结束时间相差一年及一年以上，则添加该数据
								if (Integer.parseInt(strInputStarts[0]) < Integer.parseInt(strInputEnds[0])
										&& Integer.parseInt(strInputStart) <= Integer.parseInt(strInputEnd)) {
									processActiveWebBean.setStartTime(obj[13].toString());
									processActiveWebBean.setStopTime(obj[14].toString());
									processActiveWebBean.setProcessWebBean(processWebBean);
									listResult.add(processActiveWebBean);
								} else {
									// 搜索条件 开始到结束时间段，日期数据
									List<String> inputYearList = new ArrayList<String>();
									// 数据库 开始到结束时间段，日期数据集合
									List<String> dataYearList = new ArrayList<String>();

									// 输入的数据，开始时间和结束时间相隔天数=============
									long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
									long diff = newDates.parse(endDate).getTime() - newDates.parse(startDate).getTime();
									long day = diff / nd;// 计算差多少天

									Calendar calendarStart = Calendar.getInstance();
									calendarStart.setTime(newDates.parse(endDate));
									for (int m = 0; m <= day; m++) {
										if (m == 0) {
											calendarStart.add(Calendar.DATE, -m);
										} else {
											calendarStart.add(Calendar.DATE, -1);
										}
										inputYearList.add(newDates.format(calendarStart.getTime()).split("-")[1]
												+ newDates.format(calendarStart.getTime()).split("-")[2]);
									}
									// 数据库中的开始时间是否存在于 搜索条件时间段内的时间
									if (inputYearList.contains(strStart)) {
										processActiveWebBean.setStartTime(obj[13].toString());
										processActiveWebBean.setStopTime(obj[14].toString());
										processActiveWebBean.setProcessWebBean(processWebBean);
										listResult.add(processActiveWebBean);
									}
								}
							}
						}
					}
				}
			}
			// 如果搜索条件输入时间为空
			else if (startDate == null || "".equals(startDate)) {
				if (obj[15] != null) {
					processActiveWebBean.setTimeType(Integer.valueOf(obj[15].toString()));
					if (obj[13] != null && !"".equals(obj[13])) {
						// 没有时间搜索条件的时候，显示所有的我的工作活动
						processActiveWebBean.setStartTime(obj[13].toString());
						processActiveWebBean.setStopTime(obj[14].toString());
					}
					processActiveWebBean.setProcessWebBean(processWebBean);
					listResult.add(processActiveWebBean);

				}
			}
			// processActiveWebBean.setProcessWebBean(processWebBean);
			// listResult.add(processActiveWebBean);
		}
		return listResult;
	}

	private String getSean(String[] dateTime, String yearTime) {
		SimpleDateFormat newDates = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendarStart = Calendar.getInstance();
		calendarStart.setTime(new Date());
		// 今天的日期
		String todayStr = newDates.format(calendarStart.getTime());
		if (yearTime == null || "".equals(yearTime)) {
			yearTime = todayStr.split("-")[0];
		}
		String dateStr = null;
		if (Integer.parseInt(dateTime[1].toString()) == 1 || Integer.parseInt(dateTime[1].toString()) == 2
				|| Integer.parseInt(dateTime[1].toString()) == 3) {
			// 第一季度
			dateStr = yearTime + "-01-01";
		} else if (Integer.parseInt(dateTime[1].toString()) == 4 || Integer.parseInt(dateTime[1].toString()) == 5
				|| Integer.parseInt(dateTime[1].toString()) == 6) {
			// 第二季度
			dateStr = yearTime + "-04-01";

		} else if (Integer.parseInt(dateTime[1].toString()) == 7 || Integer.parseInt(dateTime[1].toString()) == 8
				|| Integer.parseInt(dateTime[1].toString()) == 9) {
			// 第 三季度
			dateStr = yearTime + "-07-01";

		} else if (Integer.parseInt(dateTime[1].toString()) == 10 || Integer.parseInt(dateTime[1].toString()) == 11
				|| Integer.parseInt(dateTime[1].toString()) == 12) {
			// 第四季度
			dateStr = yearTime + "-10-01";

		}
		return dateStr;
	}

	/**
	 * 文控信息版本记录
	 * 
	 * @param id
	 *            ID
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long id) throws Exception {
		try {
			return docControlDao.getJecnTaskHistoryNew(id);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获得有效期邮件提醒内容
	 * 
	 * @param type
	 *            0流程 2制度
	 * @throws Exception
	 */
	public void sendOrgEmail(int type) throws Exception {

		String value = JecnConfigTool.getItemValue(JecnConfigContents.ConfigItemPartMapMark.mailExpiryTip.toString());
		if (!"1".equals(value)) {
			return;
		}

		// 获取流程有效期配置
		List<JecnConfigItemBean> itemList = JecnContants.flowEndDateList;
		if (itemList == null) {
			return;
		}
		String mark;
		// 系统管理员
		boolean isAdminMail = false;
		boolean isAdminMessage = false;

		// 流程监护人
		boolean isWardPeopleMail = false;
		boolean isWardPeopleMes = false;
		// 流程责任人
		boolean isResPeopleMail = false;
		boolean isResPeopleMes = false;
		// 流程拟制人
		boolean isFictionPeopleMail = false;
		boolean isFictionPeopleMes = false;
		if (type == 0) {
			for (JecnConfigItemBean jecnConfigItemBean : itemList) {
				mark = jecnConfigItemBean.getMark();
				if (JecnCommon.isNullOrEmtryTrim(mark)) {
					break;
				}
				if (ConfigItemPartMapMark.mailFlowRefRationPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 流程责任人邮件配置
					isResPeopleMail = true;
				} else if (ConfigItemPartMapMark.mailFlowGuardianPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 流程监护人邮件配置
					isWardPeopleMail = true;
				} else if (ConfigItemPartMapMark.mailFlowFictionPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 拟制人邮件配置
					isFictionPeopleMail = true;
				} else if (ConfigItemPartMapMark.mailSystemManager.toString() // 系统管理员邮件配置
						.equals(mark) && jecnConfigItemBean.getValue().equals("1")) {
					isAdminMail = true;
				} else if (ConfigItemPartMapMark.messageFlowFictionPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 拟制人消息配置
					isFictionPeopleMes = true;
				} else if (ConfigItemPartMapMark.messageFlowRefRationPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 流程责任人消息配置
					isResPeopleMes = true;
				} else if (ConfigItemPartMapMark.messageFlowGuardianPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 流程监护人消息配置
					isWardPeopleMes = true;
				} else if (ConfigItemPartMapMark.messageSystemManager.toString() // 系统管理员
						// 消息配置
						.equals(mark) && jecnConfigItemBean.getValue().equals("1")) {
					isAdminMessage = true;
				}
			}
		} else {// 制度
			for (JecnConfigItemBean jecnConfigItemBean : itemList) {
				mark = jecnConfigItemBean.getMark();
				if (JecnCommon.isNullOrEmtryTrim(mark)) {
					break;
				}
				if (ConfigItemPartMapMark.mailRuleRefRationPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 制度责任人邮件配置
					isResPeopleMail = true;
				} else if (ConfigItemPartMapMark.mailRuleGuardianPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 制度监护人邮件配置
					isWardPeopleMail = true;
				} else if (ConfigItemPartMapMark.mailRuleSystemManager.toString() // 系统管理员邮件配置
						.equals(mark) && jecnConfigItemBean.getValue().equals("1")) {
					isAdminMail = true;
				} else if (ConfigItemPartMapMark.messageRuleRefRationPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 制度责任人消息配置
					isResPeopleMes = true;
				} else if (ConfigItemPartMapMark.messageRuleGuardianPeople.toString().equals(mark)
						&& jecnConfigItemBean.getValue().equals("1")) {// 制度监护人消息配置
					isWardPeopleMes = true;
				} else if (ConfigItemPartMapMark.messageRuleSystemManager.toString() // 系统管理员
						// 消息配置
						.equals(mark) && jecnConfigItemBean.getValue().equals("1")) {
					isAdminMessage = true;
				}
			}
		}

		String sql = getNeedTipExpiry(type);
		// 获取需要发送邮件/消息的流程数据
		List<Object[]> flowObjList = null;
		if (!JecnCommon.isNullOrEmtryTrim(sql)) {
			flowObjList = docControlDao.listNativeSql(sql);
		}
		if (flowObjList == null || flowObjList.size() == 0) {
			return;
		}
		/** 获取流程责任人Ids、流程监护人Ids、流程拟制人Ids集合,去掉重复ID * */
		Set<Long> peopleIds = new HashSet<Long>();
		for (Object[] objects : flowObjList) {
			// 流程监护人
			if (objects[4] != null && !"".equals(objects[4])) {
				peopleIds.add(Long.valueOf(objects[4].toString()));
			}
			// 流程拟制人
			if (objects[5] != null && !"".equals(objects[5])) {
				peopleIds.add(Long.valueOf(objects[5].toString()));
			}
			// 流程责任人
			if (objects[6] != null && !"".equals(objects[6])) {
				peopleIds.add(Long.valueOf(objects[6].toString()));
			}
		}

		// 根据获取的人员ID集合获取人员信息
		List<JecnUser> peopleList = null;
		try {
			peopleList = userDao.getJecnUserList(peopleIds);
		} catch (Exception e) {
			log.error("根据ID集合 获取人员数据出错", e);
		}
		Map<Long, JecnUser> allPeopleInfo = new HashMap<Long, JecnUser>();
		if (peopleList != null) {
			for (JecnUser user : peopleList) {
				allPeopleInfo.put(user.getPeopleId(), user);
			}
		}
		// 系统管理员邮件信息
		List<JecnUser> adminList = new ArrayList<JecnUser>();
		if (isAdminMail || isAdminMessage) {// 存在管理员
			adminList = userDao.getAllAdminUser();
		}

		// 如果不存在收件人
		if ((allPeopleInfo == null || allPeopleInfo.isEmpty()) && (adminList == null || adminList.isEmpty())) {
			return;
		}

		// 流程数据集合
		Map<Long, Map<Long, JecnUser>> needSendEmail = new HashMap<Long, Map<Long, JecnUser>>();
		Map<Long, Map<Long, JecnUser>> needSendMsg = new HashMap<Long, Map<Long, JecnUser>>();
		Set<Long> handlerIdSet = new HashSet<Long>();
		for (Object[] objectFlow : flowObjList) {
			Long flowId = JecnUtil.valueToLong(objectFlow[0]);
			// 同一流程需要处理多次时，表明此流程的数据中的流程责任人类型为岗位
			if (handlerIdSet.contains(flowId)) {
				if (objectFlow[6] != null && !"".equals(objectFlow[6])) {
					Long resPeopleId = Long.valueOf(objectFlow[6].toString());
					JecnUser user = allPeopleInfo.get(resPeopleId);
					if (isResPeopleMes) {// 发送==消息==给流程责任人
						needSendMsg.get(flowId).put(user.getPeopleId(), user);
					}
					if (isResPeopleMail) {
						needSendEmail.get(flowId).put(user.getPeopleId(), user);
					}
				}
			} else {
				handlerIdSet.add(flowId);
				needSendMsg.put(flowId, new HashMap<Long, JecnUser>());
				needSendEmail.put(flowId, new HashMap<Long, JecnUser>());
				if (isAdminMail) {
					for (JecnUser user : adminList) {
						needSendEmail.get(flowId).put(user.getPeopleId(), user);
					}
				}
				if (isAdminMessage) {
					for (JecnUser user : adminList) {
						needSendMsg.get(flowId).put(user.getPeopleId(), user);
					}
				}
				// 人员数据
				JecnUser user = null;
				// 流程监护人
				if (objectFlow[4] != null && !"".equals(objectFlow[4])) {
					Long wardPeopleId = Long.valueOf(objectFlow[4].toString());
					user = allPeopleInfo.get(wardPeopleId);
					if (user != null) {
						if (isWardPeopleMes) {
							needSendMsg.get(flowId).put(user.getPeopleId(), user);
						}
						if (isWardPeopleMail) {
							needSendEmail.get(flowId).put(user.getPeopleId(), user);
						}
					}
				}
				// 流程拟制人
				if (objectFlow[5] != null && !"".equals(objectFlow[5])) {
					Long tempId = Long.valueOf(objectFlow[5].toString());
					user = allPeopleInfo.get(tempId);
					if (user != null) {
						if (isFictionPeopleMes) {
							needSendMsg.get(flowId).put(user.getPeopleId(), user);
						}
						if (isFictionPeopleMail) {
							needSendEmail.get(flowId).put(user.getPeopleId(), user);
						}
					}
				}
				// 流程责任人
				if (objectFlow[6] != null && !"".equals(objectFlow[6])) {
					Long resPeopleId = Long.valueOf(objectFlow[6].toString());
					user = allPeopleInfo.get(resPeopleId);
					if (user != null) {
						if (isResPeopleMes) {// 发送==消息==给流程责任人
							needSendMsg.get(flowId).put(user.getPeopleId(), user);
						}
						if (isResPeopleMail) {
							needSendEmail.get(flowId).put(user.getPeopleId(), user);
						}
					}
				}
			}
		}
		// 发送邮件
		for (Long flowId : needSendEmail.keySet()) {
			Object[] flowInfo = getFlowInfoById(flowId, flowObjList);
			if (flowInfo == null) {
				continue;
			}
			List<JecnUser> emailUsers = new ArrayList<JecnUser>(needSendEmail.get(flowId).values());
			if (emailUsers != null && emailUsers.size() > 0) {
				BaseEmailBuilder emailBuilder = EmailBuilderFactory
						.getEmailBuilder(EmailBuilderType.PROCESS_EXPIRY_DATE);
				emailBuilder.setData(emailUsers, new Object[] { flowInfo[1], flowInfo[8], type });
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				JecnUtil.saveEmail(buildEmail);
			}
		}
		// 发送消息
		for (Long flowId : needSendMsg.keySet()) {
			Object[] flowInfo = getFlowInfoById(flowId, flowObjList);
			List<JecnUser> msgUsers = new ArrayList<JecnUser>(needSendMsg.get(flowId).values());
			if (msgUsers != null && msgUsers.size() > 0) {
				createExpiryMessage(flowInfo, msgUsers, type);
			}
		}
	}

	private String getNeedTipExpiry(int type) {
		String sql = null;
		int a = Integer.valueOf(JecnConfigTool
				.getItemValue(JecnConfigContents.ConfigItemPartMapMark.mailExpiryAdvanceTip.toString()));
		int b = Integer.valueOf(JecnConfigTool
				.getItemValue(JecnConfigContents.ConfigItemPartMapMark.mailExpiryFrequencyTip.toString()));
		if (type == 0) {
			sql = getFlowNeedTipExpiry(a, b);
		} else {
			sql = getRuleNeedTipExpiry(a, b);
		}
		return sql;
	}

	private String getRuleNeedTipExpiry(int advance, int frequency) {
		Date date = new Date();
		SimpleDateFormat strDate = new SimpleDateFormat("yyyy-MM-dd");
		String nowDate = strDate.format(date);
		String diff = "";
		if (JecnCommon.isSQLServer()) {
			diff = " datediff(day,cast( '" + nowDate + "' as datetime), JTHN.NEXT_SCAN_DATE)";
		} else if (JecnCommon.isOracle()) {
			diff = "  (to_date(to_char(JTHN.NEXT_SCAN_DATE,'yyyy-mm-dd'),'yyyy-mm-dd')-to_date('" + nowDate
					+ "','yyyy-mm-dd'))";
		}

		String sql = " select a.* from ("
				+ "	SELECT JT.ID,"
				+ "				 JT.RULE_NAME,"
				+ "				 JT.EXPIRY,"
				+ "				 JT.pub_time,"
				+ "				 JT.WARD_PEOPLE_ID,"
				+ "				 NULL AS FICTION_PEOPLE_ID,"
				+ "				 CASE WHEN JT.TYPE_RES_PEOPLE=1 THEN JUPR.PEOPLE_ID"
				+ "				 ELSE JT.RES_PEOPLE_ID END AS PEOPLE_ID,"
				+ "				 JT.TYPE_RES_PEOPLE,JTHN.NEXT_SCAN_DATE,"
				+ diff
				+ " as diff"
				+ "		FROM JECN_RULE JT"
				+ "     INNER JOIN JECN_TASK_HISTORY_NEW JTHN ON JT.HISTORY_ID = JTHN.ID AND JTHN.EXPIRY<>0"
				+ "		LEFT JOIN JECN_USER_POSITION_RELATED JUPR ON JT.TYPE_RES_PEOPLE=1 AND JUPR.FIGURE_ID=JT.RES_PEOPLE_ID"
				+ "		LEFT JOIN JECN_USER JU ON JT.TYPE_RES_PEOPLE<>1 AND JU.PEOPLE_ID = JT.RES_PEOPLE_ID "
				+ "     WHERE  JT.DEL_STATE=0 )a where a.diff>0 and a.diff<=" + advance;
		if (JecnCommon.isSQLServer()) {
			sql += " and a.diff%" + frequency + "=0";
		} else if (JecnCommon.isOracle()) {
			sql += " and mod(a.diff," + frequency + ")=0";
		}
		sql = sql + " order by a.ID";
		return sql;
	}

	private String getFlowNeedTipExpiry(int advance, int frequency) {
		Date date = new Date();
		SimpleDateFormat strDate = new SimpleDateFormat("yyyy-MM-dd");
		String nowDate = strDate.format(date);
		String diff = "";
		if (JecnCommon.isSQLServer()) {
			diff = " datediff(day,cast( '" + nowDate + "' as datetime), JTHN.NEXT_SCAN_DATE)";
		} else if (JecnCommon.isOracle()) {
			diff = "  (to_date(to_char(JTHN.NEXT_SCAN_DATE,'yyyy-mm-dd'),'yyyy-mm-dd')-to_date('" + nowDate
					+ "','yyyy-mm-dd'))";
		}
		String sql = " select a.* from ("
				+ " SELECT T.FLOW_ID,"
				+ "                  T.FLOW_NAME,"
				+ "                  T.EXPIRY,"
				+ "                  JT.pub_time,"
				+ "                  T.WARD_PEOPLE_ID,"
				+ "                  T.FICTION_PEOPLE_ID,"
				+ "                  CASE WHEN T.TYPE_RES_PEOPLE=1 THEN JUPR.PEOPLE_ID"
				+ "                  ELSE T.RES_PEOPLE_ID END AS PEOPLE_ID,"
				+ "                  T.TYPE_RES_PEOPLE,JTHN.NEXT_SCAN_DATE,"
				+ diff
				+ " as diff"
				+ "             FROM JECN_FLOW_STRUCTURE JT"
				+ "             INNER JOIN JECN_FLOW_BASIC_INFO_T T ON T.FLOW_ID=JT.FLOW_ID"
				+ "             LEFT JOIN JECN_USER_POSITION_RELATED JUPR ON T.TYPE_RES_PEOPLE=1 AND JUPR.FIGURE_ID=T.RES_PEOPLE_ID"
				+ "             LEFT JOIN JECN_USER JU ON T.TYPE_RES_PEOPLE<>1 AND JU.PEOPLE_ID = T.RES_PEOPLE_ID"
				+ "			    INNER JOIN JECN_TASK_HISTORY_NEW JTHN ON JT.HISTORY_ID = JTHN.ID AND JTHN.EXPIRY<>0"
				+ "             WHERE  JT.DEL_STATE=0 )a where a.diff>0 and a.diff<=" + advance;
		if (JecnCommon.isSQLServer()) {
			sql += " and a.diff%" + frequency + "=0";
		} else if (JecnCommon.isOracle()) {
			sql += " and mod(a.diff," + frequency + ")=0";
		}
		sql = sql + " order by a.FLOW_ID";
		return sql;
	}

	/**
	 * 获取流程信息（发送有效期邮件使用）
	 * 
	 * @param flowId
	 * @param flowObjList
	 * @return
	 */
	private Object[] getFlowInfoById(Long flowId, List<Object[]> flowObjList) {
		for (Object[] objects : flowObjList) {
			if (objects[0].toString().equals(String.valueOf(flowId))) {
				return objects;
			}
		}

		return null;
	}

	/**
	 * 有效期提醒消息 obj 流程对象
	 * 
	 * @param obj
	 *            流程对象 objUser 是人员对象 isExist 检查是不是已经发过
	 * @time 2014-07-25
	 */
	private void createExpiryMessage(Object[] obj, List<JecnUser> users, int type) {
		try {
			if (obj == null || obj[0] == null || obj[1] == null || obj[3] == null) {
				log.info("WebProcessServiceImpl.createOrgEmail():obj == null || objUser == null || objUser[2] == null");
				return;
			}
			for (JecnUser jecnUser : users) {
				// 流程名称
				String flowName = obj[1].toString();
				// 时间
				String updateDate = obj[3].toString();
				Long peopleId = jecnUser.getPeopleId();
				// 生成消息
				JecnMessage message = new JecnMessage();
				// 几个月没有更新
				int ex = JecnUtil.getMonthDiff(JecnUtil.DateToStr(new Date()), updateDate);
				String mesContent = JecnUtil.getValue("flowExpiryTwo") + flowName
						+ handle(JecnUtil.getValue("flowExpiryThree"), type) + ex
						+ handle(JecnUtil.getValue("flowExpiryFour"), type) + flowName
						+ handle(JecnUtil.getValue("flowExpiryFive"), type);
				// 设置邮件标题
				message.setMessageTopic(mesContent);
				// 保存消息
				createMessage(peopleId, message);
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private String handle(String value, int type) {
		if (type == 0) {
			return value;
		}
		return value.replaceAll("流程", "制度");
	}

	/**
	 * 保存消息
	 * 
	 * @param peopleId
	 * @param jecnMessage
	 */
	private void createMessage(Long peopleId, JecnMessage jecnMessage) {
		Session s = this.docControlDao.getSession();
		// 已发布！
		if (peopleId != null) {
			jecnMessage.setPeopleId(0L);
			jecnMessage.setMessageType(0);
			jecnMessage.setNoRead(Long.valueOf(0));
			jecnMessage.setInceptPeopleId(peopleId);
			jecnMessage.setCreateTime(new Date());
			s.save(jecnMessage);
			s.flush();
		}
	}

	/**
	 * 通过流程名称 查询流程
	 */
	public List<JecnTreeBean> searchByName(String processName, Long projectId) throws Exception {

		return null;
	}

	@Override
	public int getTotalMyWorkModel(Long processId, Long activeId, String fileName, Long userId, Long projectId)
			throws Exception {
		try {
			String sql = "";
			if (activeId != -1) {
				sql = "select count(distinct jf.file_id) from jecn_file jf where  jf.del_state=0 and"
						+ "        (jf.file_id in (select jmf.file_m_id from jecn_mode_file jmf where jmf.figure_id ="
						+ activeId
						+ " )"
						+ "        or jf.file_id in ( select jt.file_id from jecn_mode_file mode_file,jecn_templet jt where jt.mode_file_id = mode_file.mode_file_id and mode_file.figure_id="
						+ activeId
						+ ")"
						+ "        or  jf.file_id in (select jaf.file_s_id from jecn_activity_file jaf where jaf.figure_id ="
						+ activeId + "))";

			} else {
				String sqlCommon = "select active.figure_id from jecn_flow_structure_image active,jecn_role_active jra"
						+ "    where active.figure_id=jra.figure_active_id and (jra.figure_role_id in"
						+ "   ( select jfsi.figure_id"
						+ "    from jecn_flow_structure_image jfsi,process_station_related prs,jecn_flow_structure jfs"
						+ "    ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr,jecn_position_group_r jpgr"
						+ "    where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0"
						+ "    and jfoi.figure_id=jpgr.figure_id and jpgr.group_id=prs.figure_position_id and prs.type='1'"
						+ "    and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id";
				if (processId != -1) {
					sqlCommon = sqlCommon + " and jfs.flow_id =" + processId;
				} else {
					sqlCommon = sqlCommon + " and jfs.del_state=0 and jfs.del_state=0 and jfs.projectid=" + projectId;
				}
				sqlCommon = sqlCommon
						+ " and jupr.people_id="
						+ userId
						+ " )"
						+ "    or"
						+ "    jra.figure_role_id in("
						+ "    select jfsi.figure_id"
						+ "    from jecn_flow_structure_image jfsi,process_station_related prs"
						+ "    ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr,jecn_flow_structure jfs"
						+ "    where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 "
						+ "    and jfoi.figure_id=prs.figure_position_id and prs.type='0'"
						+ "    and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id";
				if (processId != -1) {
					sqlCommon = sqlCommon + " and jfs.flow_id =" + processId;
				} else {
					sqlCommon = sqlCommon + " and jfs.del_state=0 and jfs.del_state=0 and jfs.projectid=" + projectId;
				}
				sqlCommon = sqlCommon + "    and jupr.people_id=" + userId + "))";

				sql = "select count(distinct jf.file_id) from jecn_file jf where jf.del_state=0 and"
						+ "        (jf.file_id in (select jmf.file_m_id from jecn_mode_file jmf where jmf.figure_id in ("
						+ sqlCommon
						+ ")"
						+ " )"
						+ "        or jf.file_id in ( select jt.file_id from jecn_mode_file mode_file,jecn_templet jt where jt.mode_file_id = mode_file.mode_file_id and mode_file.figure_id in ("
						+ sqlCommon
						+ ")"
						+ ")"
						+ "        or  jf.file_id in (select jaf.file_s_id from jecn_activity_file jaf where jaf.figure_id in ("
						+ sqlCommon + ")))";

			}
			if (StringUtils.isNotBlank(fileName)) {
				sql = sql + " and jf.file_name like '%" + fileName + "%'";
			}

			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 查阅我的操作模版
	 * 
	 * @param processId
	 * @param activeId
	 * @param fileName
	 * @param userId
	 * @param projectId
	 * @return
	 */
	private String getSqlMyWorkModel(Long processId, Long activeId, String fileName, Long userId, Long projectId) {
		String sql = "";
		if (activeId != -1) {
			sql = "select distinct jf.file_id,jf.file_name,jf.doc_id from jecn_file jf where jf.del_state=0 and"
					+ "        (jf.file_id in (select jmf.file_m_id from jecn_mode_file jmf where jmf.figure_id ="
					+ activeId
					+ " )"
					+ "        or jf.file_id in ( select jt.file_id from jecn_mode_file mode_file,jecn_templet jt where jt.mode_file_id = mode_file.mode_file_id and mode_file.figure_id="
					+ activeId
					+ ")"
					+ "        or  jf.file_id in (select jaf.file_s_id from jecn_activity_file jaf where jaf.figure_id ="
					+ activeId + "))";

		} else {
			String sqlCommon = "select active.figure_id from jecn_flow_structure_image active,jecn_role_active jra"
					+ "    where active.figure_id=jra.figure_active_id and (jra.figure_role_id in"
					+ "   ( select jfsi.figure_id"
					+ "    from jecn_flow_structure_image jfsi,process_station_related prs,jecn_flow_structure jfs"
					+ "    ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr,jecn_position_group_r jpgr"
					+ "    where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0"
					+ "    and jfoi.figure_id=jpgr.figure_id and jpgr.group_id=prs.figure_position_id and prs.type='1'"
					+ "    and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id";
			if (processId != -1) {
				sqlCommon = sqlCommon + " and jfs.flow_id =" + processId;
			} else {
				sqlCommon = sqlCommon + " and jfs.del_state=0 and jfs.del_state=0 and jfs.projectid=" + projectId;
			}
			sqlCommon = sqlCommon
					+ " and jupr.people_id="
					+ userId
					+ " )"
					+ "    or"
					+ "    jra.figure_role_id in("
					+ "    select jfsi.figure_id"
					+ "    from jecn_flow_structure_image jfsi,process_station_related prs"
					+ "    ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr,jecn_flow_structure jfs"
					+ "    where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 "
					+ "    and jfoi.figure_id=prs.figure_position_id and prs.type='0'"
					+ "    and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id";
			if (processId != -1) {
				sqlCommon = sqlCommon + " and jfs.flow_id =" + processId;
			} else {
				sqlCommon = sqlCommon + " and jfs.del_state=0 and jfs.del_state=0 and jfs.projectid=" + projectId;
			}
			sqlCommon = sqlCommon + "    and jupr.people_id=" + userId + "))";

			sql = "select distinct jf.file_id,jf.file_name,jf.doc_id from jecn_file jf where jf.del_state=0 and"
					+ "        (jf.file_id in (select jmf.file_m_id from jecn_mode_file jmf where jmf.figure_id in ("
					+ sqlCommon
					+ ")"
					+ " )"
					+ "        or jf.file_id in ( select jt.file_id from jecn_mode_file mode_file,jecn_templet jt where jt.mode_file_id = mode_file.mode_file_id and mode_file.figure_id in ("
					+ sqlCommon
					+ ")"
					+ ")"
					+ "        or  jf.file_id in (select jaf.file_s_id from jecn_activity_file jaf where jaf.figure_id in ("
					+ sqlCommon + ")))";

		}
		if (StringUtils.isNotBlank(fileName)) {
			sql = sql + " and jf.file_name like '%" + fileName + "%'";
		}
		return sql;
	}

	@Override
	public List<FileWebBean> searchMyWorkModel(Long processId, Long activeId, String fileName, Long userId,
			Long projectId, int start, int limit) throws Exception {
		try {
			String sql = this.getSqlMyWorkModel(processId, activeId, fileName, userId, projectId);
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql, start, limit);
			List<FileWebBean> listFileWebBean = new ArrayList<FileWebBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				FileWebBean fileWebBean = new FileWebBean();
				fileWebBean.setFileId(Long.valueOf(obj[0].toString()));
				// 名称
				if (obj[1] != null) {
					fileWebBean.setFileName(obj[1].toString());
				}
				// 编号
				if (obj[2] != null) {
					fileWebBean.setFileNum(obj[2].toString());
				}
				listFileWebBean.add(fileWebBean);
			}
			return listFileWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<KeyValueBean> getMyJoinProcessList(Long userId, Long projectId) throws Exception {
		try {
			String sql = "select distinct flow.flow_id,flow.flow_name,flow.flow_id_input from jecn_flow_structure flow where (flow.flow_id in"
					+ "           (select  jfs.flow_id"
					+ "           from jecn_flow_structure_image jfsi,jecn_flow_structure jfs,process_station_related prs"
					+ "           ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr,jecn_position_group_r jpgr"
					+ "           where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and jfo.projectid="
					+ projectId
					+ "           and jfoi.figure_id=jpgr.figure_id and jpgr.group_id=prs.figure_position_id and prs.type='1'"
					+ "           and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id = jfs.flow_id and jfs.projectid="
					+ projectId
					+ " and jfs.del_state=0"
					+ "           and jupr.people_id="
					+ userId
					+ ")"
					+ "           or"
					+ "           flow.flow_id in("
					+ "           select  jfs.flow_id"
					+ "           from jecn_flow_structure_image jfsi,jecn_flow_structure jfs,process_station_related prs"
					+ "           ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr"
					+ "           where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and jfo.projectid="
					+ projectId
					+ "           and jfoi.figure_id=prs.figure_position_id and prs.type='0'"
					+ "           and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id = jfs.flow_id and jfs.projectid="
					+ projectId + " and jfs.del_state=0" + "           and jupr.people_id=" + userId + "))";

			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<KeyValueBean> listKeyValueBean = new ArrayList<KeyValueBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				KeyValueBean keyValueBean = new KeyValueBean();
				keyValueBean.setId(Long.valueOf(Long.valueOf(obj[0].toString())));
				// 名称
				if (obj[1] != null) {
					keyValueBean.setName(obj[1].toString());
				}
				// 编号
				if (obj[2] != null) {
					keyValueBean.setNumber(obj[2].toString());
				}
				listKeyValueBean.add(keyValueBean);
			}
			return listKeyValueBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<KeyValueBean> getActiveListByProcessId(Long userId, Long processId) throws Exception {
		try {
			String sql = "select distinct active.figure_id,active.figure_text,active.activity_id from jecn_flow_structure_image active,jecn_role_active jra"
					+ "    where active.figure_id=jra.figure_active_id and (jra.figure_role_id in"
					+ "   ( select jfsi.figure_id"
					+ "    from jecn_flow_structure_image jfsi,process_station_related prs"
					+ "    ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr,jecn_position_group_r jpgr"
					+ "    where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0"
					+ "    and jfoi.figure_id=jpgr.figure_id and jpgr.group_id=prs.figure_position_id and prs.type='1'"
					+ "    and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id ="
					+ processId
					+ "    and jupr.people_id="
					+ userId
					+ " )"
					+ "    or"
					+ "    jra.figure_role_id in("
					+ "    select jfsi.figure_id"
					+ "    from jecn_flow_structure_image jfsi,process_station_related prs"
					+ "    ,jecn_flow_org jfo,jecn_flow_org_image jfoi,jecn_user_position_related jupr"
					+ "    where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 "
					+ "    and jfoi.figure_id=prs.figure_position_id and prs.type='0'"
					+ "    and prs.figure_flow_id = jfsi.figure_id and jfsi.flow_id ="
					+ processId
					+ "    and jupr.people_id=" + userId + "))";
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<KeyValueBean> listKeyValueBean = new ArrayList<KeyValueBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				KeyValueBean keyValueBean = new KeyValueBean();
				keyValueBean.setId(Long.valueOf(Long.valueOf(obj[0].toString())));
				// 名称
				if (obj[1] != null) {
					keyValueBean.setName(obj[1].toString());
				}
				// 编号
				if (obj[2] != null) {
					keyValueBean.setNumber(obj[2].toString());
				}
				listKeyValueBean.add(keyValueBean);
			}
			return listKeyValueBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public ActiveRoleBean getProcessActiveWeb(long flowId, boolean isPub) throws Exception {
		ActiveRoleBean activeRoleBean = new ActiveRoleBean();
		// 面板横纵向
		String workflowWH = flowStructureDao.getFlowDirectionByFlowId(flowId, isPub);
		// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明
		List<Object[]> listActiveShow = flowStructureDao.getActiveShowByFlowId(flowId, isPub);
		// 角色明细数据 0是角色主键ID 1角色名称 2角色职责 3图形X点 4图形Y点
		List<Object[]> listRoleShow = flowStructureDao.getRoleByFlowId(flowId, isPub);
		// 角色排序
		if (!JecnCommon.isNullOrEmtryTrim(workflowWH)) {
			roleSorting(Integer.valueOf(workflowWH), listRoleShow);
		}
		// 流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
		List<Object[]> listRoleAndActiveRelate = flowStructureDao.findAllRoleAndActiveRelateByFlowId(flowId, isPub);

		// 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号
		List<Object[]> listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowId(flowId, isPub);
		// 流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
		List<Object[]> listOutFiles = flowStructureDao.findAllActiveOutputByFlowId(flowId, isPub);
		// 指标 0主键ID ， 1指标名 ， 2指标值 , 3活动ID
		List<Object[]> listRefIndicators = flowStructureDao.getRefIndicators(flowId, isPub);
		// 样例
		List<Object[]> activeTempletList = flowStructureDao.findAllActiveTempletByFlowId(flowId, isPub);
		// 活动明细数据
		List<RoleBean> roleBeanList = getRoleBeanList(listActiveShow, listRoleShow, listRoleAndActiveRelate,
				listInputFiles, listOutFiles, listRefIndicators, activeTempletList);
		// 存储活动和角色数据集合
		activeRoleBean.setRoleList(roleBeanList);

		// 获取流程数据
		if (isPub) {
			JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(flowId);
			// 获取流程名称
			String flowName = jecnFlowStructure.getFlowName();
			// 存储流程名称
			activeRoleBean.setFlowName(flowName);
			// 获取图片存储路径
			String imgPath = jecnFlowStructure.getFilePath();
			// 存储图片路径
			activeRoleBean.setImgPath(imgPath);
		} else {
			JecnFlowStructureT jecnFlowStructure = flowStructureDao.get(flowId);
			// 获取流程名称
			String flowName = jecnFlowStructure.getFlowName();
			// 存储流程名称
			activeRoleBean.setFlowName(flowName);
			// 获取图片存储路径
			String imgPath = jecnFlowStructure.getFilePath();
			// 存储图片路径
			activeRoleBean.setImgPath(imgPath);
		}
		return activeRoleBean;
	}

	/**
	 * 角色排序
	 * 
	 * @author fuzhh May 15, 2013
	 * @param 面板方向
	 *            0横 1纵
	 * @param rolwObjList
	 */
	private void roleSorting(int workflowWH, List<Object[]> roleObjList) {
		if (0 == workflowWH) {
			if (roleObjList == null || roleObjList.size() == 0) {
				return;
			}
			Collections.sort(roleObjList, new Comparator<Object[]>() {
				public int compare(Object[] r1, Object[] r2) {
					if ((r1 == null && r1[3] == null && r1[4] == null)
							|| (r2 == null && r2[3] == null && r1[4] == null)) {
						throw new IllegalArgumentException("参数不合法");
					}
					int sortId1 = Integer.valueOf(r1[3].toString());
					int sortId2 = Integer.valueOf(r2[3].toString());
					if (sortId1 < sortId2) {
						return -1;
					} else if (sortId1 > sortId2) {
						return 1;
					} else {
						int sortIdy1 = Integer.valueOf(r1[4].toString());
						int sortIdy2 = Integer.valueOf(r2[4].toString());
						if (sortIdy1 < sortIdy2) {
							return -1;
						} else if (sortIdy1 > sortIdy2) {
							return 1;
						} else {
							return 0;
						}
					}
				}
			});
		} else if (1 == workflowWH) {
			if (roleObjList == null || roleObjList.size() == 0) {
				return;
			}
			Collections.sort(roleObjList, new Comparator<Object[]>() {
				public int compare(Object[] r1, Object[] r2) {
					if ((r1 == null && r1[3] == null && r1[4] == null)
							|| (r2 == null && r1[3] == null && r2[4] == null)) {
						throw new IllegalArgumentException("参数不合法");
					}
					int sortId1 = Integer.valueOf(r1[4].toString());
					int sortId2 = Integer.valueOf(r2[4].toString());
					if (sortId1 < sortId2) {
						return -1;
					} else if (sortId1 > sortId2) {
						return 1;
					} else {
						int sortIdx1 = Integer.valueOf(r1[3].toString());
						int sortIdx2 = Integer.valueOf(r2[3].toString());
						if (sortIdx1 < sortIdx2) {
							return -1;
						} else if (sortIdx1 > sortIdx2) {
							return 1;
						} else {
							return 0;
						}
					}
				}
			});
		}
	}

	/**
	 * 流程活动数据 含指标
	 * 
	 * @author fuzhh Apr 26, 2013
	 * 
	 * @param listActiveShow
	 *            0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明
	 * @param listRoleShow
	 *            角色明细数据 0是角色主键ID 1角色名称 2角色职责
	 * @param listRoleAndActiveRelate
	 *            流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
	 * @param listInputFiles
	 *            0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号
	 * @param listOutFiles
	 *            流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
	 * @param listRefIndicators
	 *            流程下所有的指标数据 0主键ID ， 1指标名 ， 2指标值 , 3活动ID
	 * @param activeTempletList
	 *            活动样例的数据 0主键ID ， 1输出的模板主键ID ， 2文件ID , 3文件名称 ,4文件编号
	 * @return
	 */
	public List<RoleBean> getRoleBeanList(List<Object[]> listActiveShow, List<Object[]> listRoleShow,
			List<Object[]> listRoleAndActiveRelate, List<Object[]> listInputFiles, List<Object[]> listOutFiles,
			List<Object[]> listRefIndicators, List<Object[]> activeTempletList) {
		// 活动明细数据
		List<ProcessActiveWebBean> processActiveWebList = getActiveShow(listActiveShow, listRoleShow,
				listRoleAndActiveRelate, listInputFiles, listOutFiles);
		if (listRefIndicators != null) {
			for (ProcessActiveWebBean processActiveWebBean : processActiveWebList) {
				// 指标集合
				List<JecnRefIndicators> refIndicatorsList = new ArrayList<JecnRefIndicators>();
				for (Object[] obj : listRefIndicators) {
					if (obj[0] == null || obj[3] == null) {
						continue;
					}
					if (Long.valueOf(obj[3].toString()).equals(processActiveWebBean.getActiveFigureId())) {
						JecnRefIndicators refIndicators = new JecnRefIndicators();
						// 主键ID
						refIndicators.setId(Long.valueOf(obj[0].toString()));
						// 指标名称
						if (obj[1] != null) {
							refIndicators.setIndicatorName(obj[1].toString());
						}
						// 指标值
						if (obj[2] != null) {
							refIndicators.setIndicatorValue(obj[2].toString());
						}
						// 活动ID
						refIndicators.setActivityId(Long.valueOf(obj[3].toString()));
						// 添加指标bean
						refIndicatorsList.add(refIndicators);
					}
				}
				processActiveWebBean.setIndicatorsList(refIndicatorsList);
			}
		}
		// 角色对应活动bean
		List<RoleBean> roleBeanList = new ArrayList<RoleBean>();
		// 循环角色
		for (Object[] obj : listRoleShow) {
			if (obj[0] == null) {
				continue;
			}
			RoleBean roleBean = new RoleBean();
			// 角色名称
			if (obj[1] != null) {
				roleBean.setRoleName(obj[1].toString());
			}
			// 角色职责
			if (obj[2] != null) {
				roleBean.setRoleShow(obj[2].toString());
			}
			List<ProcessActiveWebBean> processActivList = new ArrayList<ProcessActiveWebBean>();
			for (ProcessActiveWebBean processActiveWebBean : processActiveWebList) {
				if (Long.valueOf(obj[0].toString()).equals(processActiveWebBean.getRoleFigureId())) {
					processActivList.add(processActiveWebBean);
				}
				if (processActiveWebBean.getListMode() != null) {
					for (ActiveModeBean activeModeBean : processActiveWebBean.getListMode()) {
						if (activeModeBean.getModeFile() == null) {
							continue;
						}
						List<FileWebBean> listTemplet = new ArrayList<FileWebBean>();
						for (Object[] activeObj : activeTempletList) {
							if (activeObj[0] == null || activeObj[1] == null) {
								continue;
							} else if (Integer.valueOf(activeObj[1].toString()) == activeModeBean.getModeFile()
									.getModeId()) {
								FileWebBean fileWebBean = new FileWebBean();
								if (activeObj[2] != null) { // 文件ID
									fileWebBean.setFileId(Long.valueOf(activeObj[2].toString()));
								}
								if (activeObj[3] != null) { // 文件名称
									fileWebBean.setFileName(activeObj[3].toString());
								}
								if (activeObj[4] != null) { // 文件编号
									fileWebBean.setFileNum(activeObj[4].toString());
								}
								listTemplet.add(fileWebBean);
							}
						}
						activeModeBean.setListTemplet(listTemplet);
					}
				}
			}
			roleBean.setProcessActiveWebList(processActivList);
			roleBeanList.add(roleBean);
		}
		return roleBeanList;
	}

	@Override
	public JecnFlowStructure getJecnFlowStructure(Long processId) throws Exception {
		return (JecnFlowStructure) flowStructureDao.getSession().get(JecnFlowStructure.class, processId);
	}

	/**
	 * 获取流程检错信息
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public List<ProcessBaseInfoBean> getProcessCheckList(long processCheckType, int start, int limit, Long projectId)
			throws Exception {
		return flowStructureDao.getProcessCheckList(processCheckType, start, limit, projectId);
	}

	/**
	 * 获取流程检错信息大小
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public int getProcessCheckCount(long processCheckType, Long projectId) throws Exception {
		return flowStructureDao.getProcessCheckCount(processCheckType, projectId);
	}

	public IJecnUserTotalDao getUserTotalDao() {
		return userTotalDao;
	}

	public void setUserTotalDao(IJecnUserTotalDao userTotalDao) {
		this.userTotalDao = userTotalDao;
	}

	@Override
	public List<ProcessWebBean> getButtProcess(int start, int limit, Long projectId, Long userId) throws Exception {

		// 拼装with 结果集
		StringBuffer withBuffer = new StringBuffer();
		MyFlowSqlUtil.buttProcessSelectWithSql(projectId, withBuffer, userId);

		StringBuffer sqlBuffer = new StringBuffer();
		// 查询条件
		MyFlowSqlUtil.buttBufferSelect(projectId, sqlBuffer);
		List<Object[]> listObject = null;
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {// Sqlerver
			listObject = flowStructureDao.listSQLServerWith(withBuffer.toString(), sqlBuffer.toString(),
					" pub_time DESC", start, limit);

		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {// ORACLE
			String sql = withBuffer.toString() + sqlBuffer.toString() + "ORDER BY T.pub_time DESC";
			listObject = flowStructureDao.listNativeSql(sql, start, limit);
		}

		List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();

		if (listObject == null) {
			return listProcessWebBean;
		}
		ProcessWebBean processWebBean = null;
		for (Object[] obj : listObject) {
			processWebBean = getButtProcessWebBeanByObject(obj, 1);
			if (processWebBean != null)
				listProcessWebBean.add(processWebBean);
		}
		return listProcessWebBean;
	}

	/**
	 * 获得流程查阅bean
	 * 
	 * @param obj
	 * @return
	 */
	private ProcessWebBean getButtProcessWebBeanByObject(Object[] obj, int isFlow) {
		if (obj == null || obj[0] == null) {
			return null;
		}
		ProcessWebBean processWebBean = new ProcessWebBean();
		// 流程ID
		processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
		// 流程名称
		if (obj[1] != null) {
			processWebBean.setFlowName(obj[1].toString());
		}
		// 流程编号
		if (obj[2] != null) {
			processWebBean.setFlowIdInput(obj[2].toString());
		}
		// 更新时间
		if (obj[3] != null && obj[5] != null) {
			processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[3].toString())));
			processWebBean.setNoUpdateDate(JecnCommon.getMonth(JecnCommon.getDateByString(obj[5].toString()),
					JecnCommon.getDateByString(obj[3].toString())));
		}
		// 密级
		if (obj[4] != null) {
			processWebBean.setIsPublic(Integer.parseInt(obj[4].toString()));
		}
		return processWebBean;
	}

	@Override
	public int getTotalButtProcess(Long projectId, Long userId) throws Exception {
		try {
			String sql = MyFlowSqlUtil.buttProcessSqlCountSelect(projectId, userId);
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnFlowSustainTool> getSustainTools(long relatedId, int type) throws Exception {

		return this.flowStructureDao.getSustainTool(relatedId, type);
	}

	@Override
	public Map<EXCESS, List<FileOpenBean>> getFlowFilesDownLoad(Long flowId, boolean isPub, Long peopleId)
			throws Exception {
		JecnUser jecnUser = userDao.get(peopleId);
		WebLoginBean bean = personService.getWebLoginBean(jecnUser.getLoginName(), null, false);
		return SelectDownloadProcess.getFlowFileDownLoad(flowId, flowStructureDao, fileService, isPub, bean);
	}
}
