package com.jecn.epros.service.home.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.system.UserWebSearchBean;
import com.jecn.epros.mapper.SystemManageMapper;
import com.jecn.epros.mapper.UserMapper;
import com.jecn.epros.service.home.SystemManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SystemManageServiceImpl implements SystemManageService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SystemManageMapper systemManageMapper;

    @Override
    public List<JecnUser> fetchUsers() {
        return userMapper.findAllUsers();
    }

    @Override
    public List<JecnUser> fetchUsersByPage(int startPage, int pageSize) {
        PageHelper.startPage(startPage, pageSize, "people_id");
        return userMapper.findAllUsers();
    }

    @Override
    public List<JecnUser> fetchUsersForManage(UserWebSearchBean userWebSearchBean) {
        return systemManageMapper.selectUsersForManage2(userWebSearchBean);
    }

}
