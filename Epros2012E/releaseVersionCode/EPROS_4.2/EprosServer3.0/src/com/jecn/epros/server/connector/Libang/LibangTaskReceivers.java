package com.jecn.epros.server.connector.Libang;

public class LibangTaskReceivers {
	/** 针对接收人的任务标题，如果为空，则默认为任务的标题 */
	private String Title;

	private String Url;

	/** 任务接收人的唯一标识，如果SSO是绑定账号则为绑定的三方用户标识。如果SSO是同一体系则为登录帐号 */
	private String LoginName;
	/** 名称 */
	private String DisplayName;

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public String getLoginName() {
		return LoginName;
	}

	public void setLoginName(String loginName) {
		LoginName = loginName;
	}

	public String getDisplayName() {
		return DisplayName;
	}

	public void setDisplayName(String displayName) {
		DisplayName = displayName;
	}

}
