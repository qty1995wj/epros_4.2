package com.jecn.epros.server.bean.task.temp;

import java.io.Serializable;
import java.util.List;

/**
 * 记录审核人主键ID和名称
 * 
 * @author ZHAGNXIAOHU
 * 
 */
public class TempAuditPeopleBean implements Serializable{
	/** 审核人人员主键ID */
	private String auditId;
	/** 审核人真实姓名 */
	private String auditName;
	/** 各阶段审核人标题名称 */
	private String auditLable;
	/** 审批阶段 特殊阶段（整理意见）10 */
	private int state;
	/** 审批阶段Id */
	private long stageId;
	/** 是否审批 2:为当前任务 1：已审批，0 未审批 */
	private int isApproval;
	/** 是否显示 1：显示 0不显示 */
	private int isShow;
	/** 审批时间 */
	private String appDate;
	/** 1：必填；0：不必填 */
	private int isEmpty = 0;
	/** 1：选人；0：没有选人 */
	private int isSelectedUser = 0;
    /** 页面显示审批人 0 为id 1为审批人姓名*/
	private List<String[]> tableAuditIdNames;
	
	public List<String[]> getTableAuditIdNames() {
		
		return tableAuditIdNames;
	}

	public void setTableAuditIdNames(List<String[]> tableAuditIdNames) {
		this.tableAuditIdNames = tableAuditIdNames;
	}

	public int getIsSelectedUser() {
		return isSelectedUser;
	}

	public void setIsSelectedUser(int isSelectedUser) {
		this.isSelectedUser = isSelectedUser;
	}

	public String getAuditId() {
		return auditId;
	}

	public String getAuditName() {
		return auditName;
	}

	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}

	public String getAuditLable() {
		return auditLable;
	}

	public void setAuditLable(String auditLable) {
		this.auditLable = auditLable;
	}

	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(int isApproval) {
		this.isApproval = isApproval;
	}

	public int getIsShow() {
		return isShow;
	}

	public void setIsShow(int isShow) {
		this.isShow = isShow;
	}

	public String getAppDate() {
		return appDate;
	}

	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}

	public int getIsEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(int isEmpty) {
		this.isEmpty = isEmpty;
	}
	
	public long getStageId() {
		return stageId;
	}

	public void setStageId(long stageId) {
		this.stageId = stageId;
	}

	public TempAuditPeopleBean clone() {
		TempAuditPeopleBean auditPeopleBean = new TempAuditPeopleBean();
		auditPeopleBean.setAppDate(this.appDate);
		auditPeopleBean.setAuditId(this.auditId);
		auditPeopleBean.setAuditLable(this.auditLable);
		auditPeopleBean.setAuditName(this.auditName);
		auditPeopleBean.setIsApproval(this.isApproval);
		auditPeopleBean.setIsEmpty(this.isEmpty);
		auditPeopleBean.setIsShow(this.isShow);
		auditPeopleBean.setState(this.state);
		auditPeopleBean.setStageId(this.stageId);
		return auditPeopleBean;
	}
}
