package com.jecn.epros.server.action.web.login.ad.kinglong;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.landray.sso.client.EKPSSOUserData;

public class KingLongLoginAction extends JecnAbstractADLoginAction {

	public static String USER_NAME = "epros.server.action.web.login.ad.kinglong";

	public KingLongLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {
			// 单点登录传递过来的登录名称
			String adLoginName = null;

			Object loginlotus = loginAction.getSession().getAttribute(USER_NAME);
			if (loginlotus != null) {
				adLoginName = String.valueOf(loginlotus);
			}

			log.info("loginlotus = " + loginlotus);

			if (StringUtils.isBlank(adLoginName)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}

			// 验证登录
			return this.loginByLoginName(adLoginName);
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		} finally {
			if (StringUtils.isNotBlank(loginAction.getLoginName())) {
				EKPSSOUserData userData = EKPSSOUserData.getInstance();
				userData.changeCurrentUser(loginAction.getLoginName());
			}
		}
	}

}
