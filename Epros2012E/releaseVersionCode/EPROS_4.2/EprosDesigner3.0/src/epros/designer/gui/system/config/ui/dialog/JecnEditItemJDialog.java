package epros.designer.gui.system.config.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;

import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.designer.gui.system.config.ui.property.type.JecnEditItemPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 点击加入按钮弹出的确认对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEditItemJDialog extends JecnDialog {
	/** 配置界面 */
	private JecnAbtractBaseConfigDialog dialog = null;
	/** 主面板 */
	private JecnEditItemPanel mainPanel = null;
	/** 被操作的表对象 */
	private JecnTableScrollPane tableScrollPane = null;

	public JecnEditItemJDialog(JecnAbtractBaseConfigDialog dialog,
			JecnTableScrollPane tableScrollPane) {
		super(dialog);
		if (dialog == null || tableScrollPane == null) {
			throw new IllegalArgumentException("JecnEditItemJDialog is error");
		}
		this.dialog = dialog;
		this.tableScrollPane = tableScrollPane;

		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	protected void initComponents() {
		// 主面板
		mainPanel = new JecnEditItemPanel(dialog, this);

		// 大小
		Dimension size = new Dimension(430, 200);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		// 模态
		this.setModal(true);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 关闭处理模式
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//
		this.setLocationRelativeTo(dialog);

		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
	}

	public JecnEditItemPanel getMainPanel() {
		return mainPanel;
	}

	public JecnTableScrollPane getTableScrollPane() {
		return tableScrollPane;
	}

}
