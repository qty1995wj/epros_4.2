package epros.designer.gui.system.combo;

public class CheckBoxPo {
	public String key = null;
	public String value = null;
	public Object value2 = null;

	public CheckBoxPo() {
	
	}

	public CheckBoxPo(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public CheckBoxPo(String key, String value, Object value2) {
		this(key, value);
		this.value2 = value2;
	}
	
	@Override
	public final String toString() {
		return this.value;
	}
}
