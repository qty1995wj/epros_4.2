package epros.draw.data.figure;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.designer.JecnFigureFileTBean;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.system.JecnSystemData;

/**
 * 
 * 图形属性类
 * 
 * @author Administrator
 * 
 */
public class JecnFigureData extends JecnAbstractFlowElementData {
	/** 图形位置点，大小处理备份对象 */
	protected JecnFigureDataCloneable figureDataCloneable = null;
	/** 流程地图添加附件集合 */
	private List<JecnFigureFileTBean> listFigureFileTBean = null;

	/**
	 * 
	 * 根据本地图形配置创建对象
	 * 
	 * @param mapElemType
	 */
	public JecnFigureData(MapElemType mapElemType) {
		super(mapElemType);
		figureDataCloneable = new JecnFigureDataCloneable();
		// 初始化图形属性
		JecnSystemData.initCurrFlowElementData(this);
	}

	@Override
	/**
	 * 
	 * 实例化对象
	 * 
	 */
	public JecnFigureData newInstance() {
		return new JecnFigureData(mapElemType);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnFigureData clone() {
		return (JecnFigureData) this.currClone();
	}

	@Override
	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnFigureData) || !(destFlowElementData instanceof JecnFigureData)) {
			return;
		}

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnFigureData src = (JecnFigureData) srcFlowElementData;
		JecnFigureData dest = (JecnFigureData) destFlowElementData;
//		if (src.getListFigureFileTBean() != null) {
//			List<JecnFigureFileTBean> listFigureFileTBean = new ArrayList<JecnFigureFileTBean>();
//			for (JecnFigureFileTBean figureFileTBean :src.getListFigureFileTBean()) {
//				listFigureFileTBean.add(figureFileTBean.clone());
//			}
//			setListFigureFileTBean(listFigureFileTBean);
//		}
		dest.setFigureDataCloneable(src.getFigureDataCloneable().clone());
	}

	public JecnFigureDataCloneable getFigureDataCloneable() {
		return figureDataCloneable;
	}

	public void setFigureDataCloneable(JecnFigureDataCloneable figureDataCloneable) {
		if (figureDataCloneable == null) {
			JecnLogConstants.LOG_JecnFigureData
					.error("JecnFigureData类setFigureDataCloneable方法：参数不能为空。figureDataCloneable=" + figureDataCloneable);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FIAL);

		}
		this.figureDataCloneable = figureDataCloneable;
	}

	public int getFigureSizeX() {
		return this.figureDataCloneable.getWidth();
	}

	public void setFigureSizeX(int figureSizeX) {
		if (this.figureDataCloneable == null) {
			return;
		}
		this.figureDataCloneable.changFigure(new Dimension(figureSizeX, this.figureDataCloneable.getHeight()), 1.0D);
		this.figureSizeX = figureSizeX;
	}

	public int getFigureSizeY() {
		return this.figureDataCloneable.getHeight();
	}

	public void setFigureSizeY(int figureSizeY) {
		if (this.figureDataCloneable == null) {
			return;
		}
		this.figureDataCloneable.changFigure(new Dimension(this.figureDataCloneable.getWidth(), figureSizeY), 1.0D);
		this.figureSizeY = figureSizeY;
	}

	public List<JecnFigureFileTBean> getListFigureFileTBean() {
		if (listFigureFileTBean == null) {
			listFigureFileTBean = new ArrayList<JecnFigureFileTBean>();
		}
		return listFigureFileTBean;
	}

	
	public void setListFigureFileTBean(List<JecnFigureFileTBean> listFigureFileTBean) {
		this.listFigureFileTBean = listFigureFileTBean;
	}

	/**
	 * 图形原始状态下位置点
	 * 
	 * @return
	 */
	public Point getOriginalLocation() {
		return this.figureDataCloneable.getLeftUpXY().getPoint();
	}
}
