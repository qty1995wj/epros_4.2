package com.jecn.epros.service;

import java.util.Map;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.system.SyncConfig;

public interface IUserImportRPCService {

    /**
     * 下载人员导入模板
     **/
    public ByteFileResult downUserTemplet();

    /**
     * 导出错误数据
     **/
    public ByteFileResult downUserErrorData();

    /**
     * 导出变更数据
     **/
    public ByteFileResult downUserChangeData();

    /**
     * 导出人员数据
     **/
    public ByteFileResult downUserData();

    /**
     * 导入人员数据
     **/
    public MessageResult importUserData(ByteFile file);

    /**
     * db同步
     **/
    public MessageResult importUserDataByDB();

    /**
     * db同步保存配置项 <br/>
     * String startTime  <br/>
     * String intervalDay  <br/>
     * String isAuto <br/>
     **/
    public MessageResult saveSyncConfig(Map<String, Object> params);

    /**
     * 获得同步配置项
     *
     * @return
     */
    SyncConfig getSyncConfig();
}
