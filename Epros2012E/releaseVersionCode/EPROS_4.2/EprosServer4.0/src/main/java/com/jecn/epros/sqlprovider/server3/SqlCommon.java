package com.jecn.epros.sqlprovider.server3;

import com.jecn.epros.domain.EnumClass.TreeNodeType;
import com.jecn.epros.sqlprovider.BaseSqlProvider;

import java.io.Reader;
import java.sql.Clob;
import java.util.*;

public class SqlCommon {

    public static int pageSize = 20;

    /**
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：获得默认角色（sql 浏览端）
     */
    public static String getDefaultAuthString() {
        return "('admin','design','isDesignFileAdmin','isDesignProcessAdmin','isDesignRuleAdmin','viewAdmin')";
    }

    /**
     * 登录验证获取默认角色权限节点
     *
     * @return
     */
    public static String getLoginDefaultAuthString() {
        if (Common.isShowSecondAdmin()) {
            return "('admin','secondAdmin','design','isDesignFileAdmin','isDesignProcessAdmin','isDesignRuleAdmin','viewAdmin')";
        } else {
            return getDefaultAuthString();
        }
    }

    public static String getSecondAdminDefaultAuthString() {
        return "('design','isDesignFileAdmin','isDesignProcessAdmin','isDesignRuleAdmin')";
    }

    /**
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：获得设计的默认角色（sql 浏览端）
     */
    public static String getDesignDefaultAuthString() {
        return "('admin','design')";
    }

    public static String getDesignAuthString() {
        return "('admin','design','secondAdmin')";
    }

    /**
     * @param roleType
     * @return
     * @author zhangchen Nov 2, 2012
     * @description：是否是流程管理员
     */
    public static boolean isAdmin(String roleType) {
        if ("admin".equals(roleType)) {
            return true;
        }
        return false;
    }

    /**
     * 查询人员在项目下的岗位集合sql
     *
     * @param peopleId
     * @param projectId
     * @return
     */
    public static String findPosSql(Long peopleId, Long projectId) {
        String sql = "select jupr.figure_id from jecn_user_position_related jupr,jecn_flow_org_image pos,jecn_flow_org org"
                + "       where jupr.figure_id= pos.figure_id and pos.org_id=org.org_id and org.del_state=0 and org.projectid="
                + projectId + "       and jupr.people_id=" + peopleId;
        return sql;
    }

    /**
     * 查询人员在项目下的岗位集合sql
     *
     * @param peopleId
     * @param projectId
     * @return
     */
    public static String findOrgSql(Long peopleId, Long projectId) {
        String sql = "SELECT DISTINCT ORG.ORG_ID" + "  FROM JECN_FLOW_ORG ORG"
                + " WHERE ORG.DEL_STATE = 0 AND ORG.PROJECTID = " + projectId + "START WITH ORG.ORG_ID IN"
                + "            (SELECT POS.ORG_ID" + "               FROM JECN_USER_POSITION_RELATED JUPR,"
                + "                    JECN_FLOW_ORG_IMAGE        POS" + "              WHERE JUPR.PEOPLE_ID = "
                + peopleId + "                AND JUPR.FIGURE_ID = POS.FIGURE_ID)"
                + "CONNECT BY PRIOR ORG.PER_ORG_ID = ORG.ORG_ID";
        return sql;
    }

    /**
     * 管理员权限 字符串
     *
     * @return
     */
    public static String getStrAdmin() {
        return "admin";
    }

    /**
     * 浏览管理员权限 字符串
     *
     * @return
     */
    public static String getStrViewAdmin() {
        return "viewAdmin";
    }

    /**
     * 下载管理员权限 字符串
     *
     * @return
     */
    public static String getStrDownLoadAdmin() {
        return "downLoadAdmin";
    }

    public static String getStrSecondAdmin() {
        return "secondAdmin";
    }

    /**
     * @param roleType
     * @return
     * @author zhangchen Nov 2, 2012
     * @description：是否是流程管理员
     */
    public static boolean isDesign(String roleType) {
        if ("design".equals(roleType)) {
            return true;
        }
        return false;
    }

    public static boolean isSecondAdmin(String roleType) {
        if ("secondAdmin".equals(roleType)) {
            return true;
        }
        return false;
    }

    /**
     * 活动(sql)
     *
     * @return
     */
    public static String getActiveString() {
        return getOperationActiveString();
    }

    /**
     * 操作说明-活动
     *
     * @return
     */
    public static String getOperationActiveString() {
        return "('Rhombus','ITRhombus','DataImage','RoundRectWithLine','ActiveFigureAR','ExpertRhombusAR','ActivityImplFigure','ActivityOvalSubFlowFigure')";
    }

    /**
     * 岗位组统计-活动 不包含 接口 活动 子流程活动
     *
     * @return
     */
    public static String getOperationPostGString() {
        return "('Rhombus','ITRhombus','DataImage','RoundRectWithLine','ActiveFigureAR','ExpertRhombusAR')";
    }

    /**
     * 角色(sql)
     *
     * @return
     */
    public static String getRoleString() {
        return "('RoleFigure','CustomFigure','VirtualRoleFigure','RoleFigureAR')";
    }

    /**
     * 角色(sql)
     *
     * @return
     */
    public static String getNoCoustomRoleString() {
        return "('RoleFigure','VirtualRoleFigure','RoleFigureAR')";
    }

    /**
     * 角色(sql)
     *
     * @return
     */
    public static String getOnlyRoleString() {
        return "('RoleFigure','RoleFigureAR')";
    }

    /**
     * 连接线（sql）
     *
     * @return
     */
    public static String getLineString() {// sql查询用到的线
        return "('ManhattanLine','CommonLine')";
    }

    /**
     * 接口（sql）
     *
     * @return
     */
    public static String getImplString() {// sql查询用到的线
        return "('OvalSubFlowFigure','ImplFigure','ActivityImplFigure','ActivityOvalSubFlowFigure','InterfaceRightHexagon')";
    }


    /**
     * @author zhangchen Aug 6, 2012
     * @description：制度（sql） @return
     */
    public static String getSystemString() {
        return "'SystemFigure'";
    }

    /**
     * @author zhangchen Aug 6, 2012
     * @description：制度（sql） @return
     */
    public static String getIconString() {
        return "'IconFigure'";
    }

    /**
     * @return
     * @author zhangchen Aug 6, 2012
     * @description：岗位
     */
    public static String getPosString() {
        return "'PositionFigure'";
    }

    /**
     * @return
     * @author zhangchen Aug 6, 2012
     * @description：组织
     */
    public static String getOrgString() {
        return "'Rect'";
    }

    /**
     * SQLSERVER：sqlserver 数据库，ORACLE：oracle数据库
     *
     * @author wanglikai
     */
    public enum DBType {
        SQLSERVER, ORACLE, MYSQL, NONE
    }

    /**
     * @param list
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：通过list封装in
     */
    public static String getIds(List<Long> list) {

        StringBuffer buf = new StringBuffer();
        buf.append(" (");
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                buf.append(list.get(i));
            } else {
                buf.append("," + list.get(i));
            }
        }
        buf.append(")");
        return buf.toString();
    }

    /**
     * 通过list封装in
     *
     * @param list
     * @return
     */
    public static String getStrs(List<String> list) {
        StringBuffer buf = new StringBuffer();
        buf.append(" (");
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                buf.append("'" + list.get(i) + "'");
            } else {
                buf.append("," + "'" + list.get(i) + "'");
            }
        }
        buf.append(")");
        return buf.toString();
    }

    /**
     * @param sql
     * @param list
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：通过list封装sql集合
     */
    public static List<String> getListSqls(String sql, List<Long> list) {
        // 计数

        List<String> listResult = new ArrayList<String>();
        List<Long> listTmp = new ArrayList<Long>();

        /**
         * 整数倍的查询
         */
        int countRow = 0;
        for (Long id : list) {
            countRow++;
            listTmp.add(id);
            if (countRow % 100 == 0) {
                // 每100条执行一次查询操作
                listResult.add(sql + getIds(listTmp));
                listTmp.clear();
            }
        }

        /**
         * 余数的查询
         */
        if (listTmp.size() > 0) {
            listResult.add(sql + getIds(listTmp));
        }
        return listResult;
    }

    /**
     * @param set
     * @return
     * @author zhangchen Jul 2, 2012
     * @description：通过set封装sql集合（最后面添加一个括号） @param sql
     */
    public static List<String> getListSqlAddBracket(String sql, List<Long> list) {
        // 计数

        List<String> listResult = new ArrayList<String>();
        List<Long> listTmp = new ArrayList<Long>();

        /**
         * 整数倍的查询
         */
        int countRow = 0;
        for (Long id : list) {
            countRow++;
            listTmp.add(id);
            if (countRow % 100 == 0) {
                // 每100条执行一次查询操作
                listResult.add(sql + getIds(listTmp) + ")");
                listTmp.clear();
            }
        }

        /**
         * 余数的查询
         */
        if (listTmp.size() > 0) {
            listResult.add(sql + getIds(listTmp) + ")");
        }
        return listResult;
    }

    /**
     * @param set
     * @return
     * @author zhangchen Jul 2, 2012
     * @description：通过set封装sql集合（最后面添加一个括号） @param sql
     */
    public static List<String> getSetSqlAddBracket(String sql, Set<Long> set) {
        // 计数

        List<String> listResult = new ArrayList<String>();
        List<Long> listTmp = new ArrayList<Long>();

        /**
         * 整数倍的查询
         */
        int countRow = 0;
        Iterator<Long> it = set.iterator();
        while (it.hasNext()) {
            Long id = it.next();
            countRow++;
            listTmp.add(id);
            if (countRow % 100 == 0) {
                // 每100条执行一次查询操作
                listResult.add(sql + getIds(listTmp) + ")");
                listTmp.clear();
            }
        }

        /**
         * 余数的查询
         */
        if (listTmp.size() > 0) {
            listResult.add(sql + getIds(listTmp) + ")");
        }
        return listResult;
    }

    /**
     * @param sql
     * @param set
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：通过set封装sql集合
     */
    public static List<String> getSetSqls(String sql, Set<Long> set) {
        // 计数

        List<String> listResult = new ArrayList<String>();
        List<Long> listTmp = new ArrayList<Long>();

        /**
         * 整数倍的查询
         */
        int countRow = 0;
        Iterator<Long> it = set.iterator();
        while (it.hasNext()) {
            Long id = it.next();
            countRow++;
            listTmp.add(id);
            if (countRow % 100 == 0) {
                // 每100条执行一次查询操作
                listResult.add(sql + getIds(listTmp));
                listTmp.clear();
            }
        }

        /**
         * 余数的查询
         */
        if (listTmp.size() > 0) {
            listResult.add(sql + getIds(listTmp));
        }
        return listResult;
    }

    /**
     * 通过Set封装in
     *
     * @param set
     * @return
     */
    public static String getIdsSet(Set<Long> set) {
        StringBuffer buf = new StringBuffer();
        buf.append(" (");
        Iterator<Long> it = set.iterator();
        int count = 0;
        while (it.hasNext()) {
            Long flowId = it.next();
            if (count == 0) {
                buf.append(flowId);
            } else {
                buf.append("," + flowId);
            }
            count++;
        }
        buf.append(")");
        return buf.toString();
    }

    /**
     * @param sql
     * @param set
     * @return
     * @author ZHANGXIAOHU
     * @description：通过set封装sql集合
     */
    public static List<String> getStringSqlsBySet(String sql, Set<String> set) {
        // 计数

        List<String> listResult = new ArrayList<String>();
        List<String> listTmp = new ArrayList<String>();

        /**
         * 整数倍的查询
         */
        int countRow = 0;
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String id = it.next();
            countRow++;
            listTmp.add(id);
            if (countRow % 100 == 0) {
                // 每100条执行一次查询操作
                listResult.add(sql + getStrs(listTmp));
                listTmp.clear();
            }
        }

        /**
         * 余数的查询
         */
        if (listTmp.size() > 0) {
            listResult.add(sql + getStrs(listTmp));
        }
        return listResult;
    }

    /**
     * 将Clob转成String ,静态方法
     *
     * @param clob 字段
     * @return 内容字串，如果出现错误，返回 null
     * @author fuzhh 2013-12-16
     */
    public static String clobToString(Clob clob) {
        if (clob == null)
            return null;
        StringBuffer sb = new StringBuffer();
        Reader clobStream = null;
        try {
            clobStream = clob.getCharacterStream();
            char[] b = new char[60000];// 每次获取60K
            int i = 0;
            while ((i = clobStream.read(b)) != -1) {
                sb.append(b, 0, i);
            }
        } catch (Exception ex) {
            sb = null;
        } finally {
            try {
                if (clobStream != null) {
                    clobStream.close();
                }
            } catch (Exception e) {
            }
        }
        if (sb == null)
            return null;
        else
            return sb.toString();
    }

    /**
     * 获取系统管理员邮件信息
     *
     * @return
     */
    public static String getAdminEmailSql() {
        // 获取系统管理员sql
        return "SELECT JU.*" + "  FROM JECN_USER JU" + " INNER JOIN JECN_USER_ROLE JUR ON JU.PEOPLE_ID = JUR.PEOPLE_ID"
                + " INNER JOIN JECN_ROLE_INFO JRI ON JUR.ROLE_ID = JRI.ROLE_ID" + " WHERE JRI.FILTER_ID = 'admin'";
    }

    public static String getUpperString(String str) {
        return "upper(" + str + ")";
    }

    public static String getUpperLikeString(String str) {
        return "upper('%" + str + "%')";
    }

    /**
     * 获得流程的所有子节点sql 通用
     *
     * @param flowId
     * @param projectId
     * @return
     */
    public static String getFlowChildObjectsSqlCommon(Long flowId, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(flowId);
        return "SELECT jfst.* FROM jecn_flow_structure_t jfst INNER JOIN ("
                + SqlCommon.getChildObjectsSqlByType(listIds, TreeNodeType.processMap)
                + ") sub on sub.flow_id=jfst.flow_Id" + "  WHERE jfst.del_state=0 and jfst.projectid=" + projectId;
    }

    /**
     * 获得制度的所有子节点sql 通用
     *
     * @param ruleId
     * @param projectId
     * @return
     */
    public static String getRuleChildObjectsSqlCommon(Long ruleId, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(ruleId);
        return "SELECT jfst.* FROM JECN_RULE_T jfst INNER JOIN ("
                + SqlCommon.getChildObjectsSqlByType(listIds, TreeNodeType.ruleDir) + ") sub on sub.id=jfst.id"
                + "  WHERE jfst.del_state=0 and jfst.project_id=" + projectId;
    }

    /**
     * 获得文件的所有子节点sql 通用
     *
     * @param fileId
     * @param projectId
     * @return
     */
    public static String getFileChildObjectsSqlCommon(Long fileId, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(fileId);
        return "SELECT jfst.* FROM JECN_FILE_T jfst INNER JOIN ("
                + SqlCommon.getChildObjectsSqlByType(listIds, TreeNodeType.fileDir) + ") sub on sub.file_id=jfst.file_id and jfst.hide=1 and jfst.del_state=0"
                + "  WHERE jfst.project_id=" + projectId;
    }

    /**
     * 获得标准的所有子节点sql 通用
     *
     * @param standardId
     * @param projectId
     * @return
     */
    public static String getStandardChildObjectsSqlCommon(Long standardId, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(standardId);
        return "SELECT jfst.* FROM JECN_CRITERION_CLASSES jfst INNER JOIN ("
                + SqlCommon.getChildObjectsSqlByType(listIds, TreeNodeType.standardDir) + ") sub on sub.CRITERION_CLASS_ID=jfst.CRITERION_CLASS_ID"
                + "  WHERE jfst.project_id=" + projectId;
    }

    /**
     * 获得风险的所有子节点sql 通用
     *
     * @param riskId
     * @param projectId
     * @return
     */
    public static String getRiskChildObjectsSqlCommon(Long riskId, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(riskId);
        return "SELECT jfst.* FROM jecn_risk jfst INNER JOIN ("
                + SqlCommon.getChildObjectsSqlByType(listIds, TreeNodeType.riskDir) + ") sub on sub.id=jfst.id"
                + "  WHERE jfst.project_id=" + projectId;
    }

    /**
     * 获得部门的所有子节点sql 通用
     *
     * @param orgId
     * @param projectId
     * @return
     */
    public static String getOrgChildObjectsSqlCommon(Long orgId, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(orgId);
        return "select jfo.*"
                + "                         from JECN_FLOW_ORG jfo" + " INNER JOIN ("
                + SqlCommon.getChildObjectsSqlByType(listIds, TreeNodeType.organization)
                + ") org on org.org_id = jfo.org_id  where jfo.del_state = 0 and jfo.Projectid=" + projectId;
    }

    /**
     * 通过类型获得所有子节点包含self
     *
     * @param listIds
     * @param treeNodeType
     * @return
     */
    public static String getChildObjectsSqlByType(List<Long> listIds, TreeNodeType treeNodeType) {
        switch (treeNodeType) {
            case process:
            case processMap:
            case processMapMode:
            case processMode:
                return SqlCommon.getChildObjectsSql("JECN_FLOW_STRUCTURE_T", "FLOW_ID", "PRE_FLOW_ID", "ISFLOW", listIds,
                        treeNodeType);
            case standardDir:
            case standard:
            case standardProcess:
            case standardProcessMap:
            case standardClause:
                return SqlCommon.getChildObjectsSql("JECN_CRITERION_CLASSES", "CRITERION_CLASS_ID",
                        "PRE_CRITERION_CLASS_ID", "STAN_TYPE", listIds, treeNodeType);
            case standardClauseRequire:
                return "SELECT T.CRITERION_CLASS_ID,T.PRE_CRITERION_CLASS_ID,T.STAN_TYPE FROM JECN_CRITERION_CLASSES T WHERE T.CRITERION_CLASS_ID  in "
                        + SqlCommon.getIds(listIds);
            case organization:
                return SqlCommon.getChildObjectsSql("JECN_FLOW_ORG", "ORG_ID", "PER_ORG_ID", "", listIds, treeNodeType);
            case position:
                return "SELECT T.FIGURE_ID,T.ORG_ID FROM JECN_FLOW_ORG_IMAGE T WHERE T.FIGURE_ID in "
                        + SqlCommon.getIds(listIds);
            case ruleDir:
                return SqlCommon.getChildObjectsSql("JECN_RULE_T", "ID", "PER_ID", "IS_DIR", listIds, treeNodeType);
            case ruleFile:
            case ruleModeFile:
                return "SELECT T.ID,T.Per_Id,T.IS_DIR FROM JECN_RULE_T T WHERE T.ID  in" + SqlCommon.getIds(listIds);
            case riskDir:
                return SqlCommon.getChildObjectsSql("JECN_RISK", "ID", "PARENT_ID", "IS_DIR", listIds, treeNodeType);
            case riskPoint:
                return "SELECT T.ID,T.PARENT_ID,T.IS_DIR FROM JECN_RISK T WHERE T.ID  in " + SqlCommon.getIds(listIds);
            case fileDir:
                return SqlCommon.getChildObjectsSql("JECN_FILE_T", "FILE_ID", "PER_FILE_ID", "IS_DIR", listIds,
                        treeNodeType);
            case file:
                return "SELECT T.FILE_ID,T.Per_File_Id,T.IS_DIR FROM JECN_FILE_T T WHERE T.FILE_ID  in "
                        + SqlCommon.getIds(listIds);
            case positionGroupDir:
                return SqlCommon.getChildObjectsSql("JECN_POSITION_GROUP", "ID", "PER_ID", "IS_DIR", listIds, treeNodeType);
            case positionGroup:
                return "SELECT id,per_id,IS_DIR FROM JECN_POSITION_GROUP WHERE ID in" + SqlCommon.getIds(listIds);
            default:
                return "";
        }
    }

    /**
     * 树形结构表，获取节点的子节点集合（删除）
     *
     * @param tableName 表名称
     * @param id        表主键ID
     * @param pId       表父节点PID
     * @param listIds   节点ID
     * @return
     */
    public static String getChildObjectsSql(String tableName, String id, String pId, String typeNode,
                                            List<Long> listIds, TreeNodeType treeNodeType) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("SELECT C." + id + ",C." + pId);
        if (treeNodeType != TreeNodeType.organization) {
            buffer.append(",C." + typeNode);
        }
        buffer.append(" FROM ").append(tableName).append(" C ");
        buffer.append(" INNER JOIN ").append(tableName).append(" P ON C.T_PATH LIKE ").append(getJoinFunc("P"));
        buffer.append(" WHERE P.").append(id).append(" IN ").append(getIds(listIds));
        return buffer.toString();
    }

    /**
     * 拼装like查询 字符串
     *
     * @param alias 表别名
     * @return String
     */
    public static String getJoinFunc(String alias) {
        if (BaseSqlProvider.isOracle()) {
            return "CONCAT(" + alias + ".T_PATH,'%')";
        } else {
            return alias + ".T_PATH + '%' ";
        }
    }

    /**
     * 获得该流程架构或者责任部门下所有的未删除的流程(包括子节点的子节点)
     *
     * @param paramMap
     * @return
     */
    public static String getOrgOrMapFlow(Map<String, Object> paramMap) {

        List<Long> mapIds = (List<Long>) paramMap.get("mapIds");
        List<Long> orgIds = (List<Long>) paramMap.get("orgIds");
        String pub = (String) paramMap.get("pub");
        boolean isMap = false;
        boolean isOrg = false;
        if (mapIds != null && mapIds.size() > 0) {
            isMap = true;
        }
        if (orgIds != null && orgIds.size() > 0) {
            isOrg = true;
        }
        String sql = "with flows as( ";
        if (isOrg) {
            sql += "SELECT jfst.flow_id, jfo.org_id as id, jfo.org_name as name, 0 AS type"
                    + "				FROM jecn_flow_org jfo" + "				LEFT JOIN jecn_flow_org" + pub + " jfoo"
                    + "					ON jfoo.t_path like " + SqlCommon.getJoinFunc("jfo")
                    + "				LEFT JOIN JECN_FLOW_RELATED_ORG" + pub + " jfrot"
                    + "					ON jfoo.org_id = jfrot.org_id" + "				LEFT JOIN jecn_flow_structure"
                    + pub + " jfst" + "					ON jfrot.flow_id = jfst.flow_id"
                    + "				 AND jfst.del_state = 0" + "				 AND jfst.ISFLOW = 1"
                    + " and jfst.projectId=#{projectId}" + "			 WHERE jfo.org_id IN "
                    + SqlCommon.getIds(orgIds);
        }
        if (isOrg && isMap) {
            sql += "			UNION";
        }
        if (isMap) {
            sql += "			SELECT jfst.flow_id, jfo.flow_id as id, jfo.flow_name as NAME, 1 AS type"
                    + "				FROM jecn_flow_structure" + pub + " jfo"
                    + "				LEFT JOIN jecn_flow_structure" + pub + " jfst"
                    + "					ON jfst.t_path like " + SqlCommon.getJoinFunc("jfo")
                    + "				 AND jfst.del_state = 0" + "				 AND jfst.ISFLOW = 1"
                    + " and jfst.projectId=#{projectId}" + "			 WHERE jfo.flow_id IN "
                    + SqlCommon.getIds(mapIds);
        }
        sql += " )";

        return sql;
    }

    /**
     * field数据库字段在开始时间结束时间之间
     *
     * @param startTime 结束时间
     * @param endTime   开始时间
     * @param field     数据库字段
     * @return
     */
    public static String timeBetweenTheTwoSql(String startTime, String endTime, String field) {
        if (BaseSqlProvider.isSQLServer()) {
            return " DATEDIFF(day," + startTime + "," + field + ")>=0 and datediff(day," + field + "," + endTime
                    + ")>=0";
        } else if (BaseSqlProvider.isOracle()) {
            return " to_char(" + field + ",'yyyy-MM-dd')>=" + startTime + " and  to_char(" + field + ",'yyyy-MM-dd')<="
                    + endTime;
        } else {
            return "";
        }
    }

    /**
     * field数据库字段在开始时间结束时间之间
     *
     * @param startTime 结束时间
     * @param endTime   开始时间
     * @param field     数据库字段
     * @return
     */
    public static String timeBetweenTheTwoSql(Date startTime, Date endTime, String field) {
        if (BaseSqlProvider.isSQLServer()) {
            return " DATEDIFF(day," + Common.getStringbyDate(startTime) + "," + field + ")>=0 and datediff(day," + field
                    + "," + Common.getStringbyDate(endTime) + ")>=0";
        } else if (BaseSqlProvider.isOracle()) {
            return " to_char(" + field + ",'yyyy-MM-dd')>='" + Common.getStringbyDate(startTime) + "' and  to_char("
                    + field + ",'yyyy-MM-dd')<='" + Common.getStringbyDate(endTime) + "'";
        } else {
            return "";
        }
    }

    /**
     * 把日期格式转化成年月
     *
     * @param dateField
     * @return
     */
    public static String DateCastYearAndMonthStr(String dateField) {
        if (BaseSqlProvider.isSQLServer()) {
            return "convert(char(7)," + dateField + ",20)";
        } else if (BaseSqlProvider.isOracle()) {
            return "to_char(" + dateField + ",'yyyy-MM')";
        }
        return dateField;
    }

    /**
     * 获得部门下的人
     *
     * @param orgId
     * @param projectId
     * @return
     */
    public static String getUserByOrgId(Long orgId, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(orgId);
        return getUserByOrgId(listIds, projectId);
    }

    /**
     * 获得部门下的人
     *
     * @param projectId
     * @return
     */
    public static String getUserByOrgId(List<Long> listIds, Long projectId) {
        return "SELECT DISTINCT JU.PEOPLE_ID FROM JECN_FLOW_ORG_IMAGE JFOI,JECN_FLOW_ORG ORG " + " INNER JOIN ("
                + getChildObjectsSqlByType(listIds, TreeNodeType.organization) + ") JFO ON JFO.ORG_ID = ORG.ORG_ID"
                + "       ,JECN_USER_POSITION_RELATED JUPR,JECN_USER JU"
                + "       WHERE ORG.ORG_ID=JFOI.ORG_ID AND JFOI.FIGURE_ID = JUPR.FIGURE_ID"
                + "       AND JUPR.PEOPLE_ID = JU.PEOPLE_ID AND JU.ISLOCK=0  AND ORG.DEL_STATE=0 AND ORG.PROJECTID="
                + projectId;
    }

    /**
     * 获得岗位下的人
     *
     * @return
     */
    public static String getUserByPosId(Long posId) {
        return "SELECT JU.PEOPLE_ID FROM JECN_USER_POSITION_RELATED JUPR,JECN_USER JU"
                + "       WHERE  JUPR.PEOPLE_ID = JU.PEOPLE_ID AND JU.ISLOCK=0 AND JUPR.FIGURE_ID=" + posId;
    }

    /**
     * 获得岗位下的人
     *
     * @return
     */
    public static String getUserByPosId(List<Long> posIds) {
        return "SELECT JU.PEOPLE_ID FROM JECN_USER_POSITION_RELATED JUPR,JECN_USER JU"
                + "       WHERE  JUPR.PEOPLE_ID = JU.PEOPLE_ID AND JU.ISLOCK=0 AND JUPR.FIGURE_ID in " + SqlCommon.getIds(posIds);
    }

    /**
     * 获得日志文件类型
     *
     * @param fileId
     * @param fileType
     * @return
     */
    public static String getFileIdByFileType(Long fileId, int fileType, Long projectId) {
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(fileId);
        // 文件类型 0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构
        switch (fileType) {
            case 0:
                return "SELECT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME"
                        + "  ,0 AS FILE_TYPE" + "   FROM JECN_FLOW_STRUCTURE JFS "
                        + (fileId == null
                        ? ""
                        : ("INNER JOIN (" + getChildObjectsSqlByType(listIds, TreeNodeType.process)
                        + ") JFO ON  JFO.FLOW_ID = JFS.FLOW_ID and jfo.isflow=1")
                        + "WHERE JFS.DEL_STATE=0 AND JFS.projectid=" + projectId);
            case 1:
                return "SELECT JFS.ID AS FILE_ID,JFS.RULE_NAME AS FILE_NAME" + "  ,1 AS FILE_TYPE"
                        + "   FROM JECN_RULE JFS " + (fileId == null
                        ? ""
                        : (" INNER JOIN (" + getChildObjectsSqlByType(listIds, TreeNodeType.ruleDir)
                        + ") JFO ON  JFO.ID = JFS.ID") + " WHERE JFS.IS_DIR<>0 AND JFS.project_id="
                        + projectId);
            case 2:
                return "SELECT JFS.FILE_ID AS FILE_ID,JFS.FILE_NAME AS FILE_NAME" + "  ,2 AS FILE_TYPE"
                        + "   FROM JECN_FILE JFS " + (fileId == null
                        ? ""
                        : (" INNER JOIN (" + getChildObjectsSqlByType(listIds, TreeNodeType.fileDir)
                        + ") JFO ON  JFO.FILE_ID = JFS.FILE_ID")
                        + " WHERE JFS.IS_DIR<>0 AND JFS.DEL_STATE=0 AND JFS.project_id=" + projectId);
            case 3:
                return "SELECT JFS.CRITERION_CLASS_ID AS FILE_ID,JFS.CRITERION_CLASS_NAME AS FILE_NAME"
                        + "  ,3 AS FILE_TYPE" + "   FROM JECN_CRITERION_CLASSES JFS" + (fileId == null
                        ? ""
                        : (" INNER JOIN (" + getChildObjectsSqlByType(listIds, TreeNodeType.standardDir)
                        + ") JFO ON  JFO.CRITERION_CLASS_ID = JFS.CRITERION_CLASS_ID")
                        + " WHERE JFS.STAN_TYPE<>0 AND JFS.project_id=" + projectId);
            case 4:
                return "SELECT JFS.ID AS FILE_ID,JFS.NAME AS FILE_NAME" + "  ,4 AS FILE_TYPE" + "   FROM JECN_RISK JFS "
                        + (fileId == null
                        ? ""
                        : (" INNER JOIN (" + getChildObjectsSqlByType(listIds, TreeNodeType.riskDir)
                        + ") JFO ON  JFO.ID = JFS.ID") + " WHERE JFS.IS_DIR<>0 AND JFS.project_id = "
                        + projectId);
            case 5:
                return "SELECT JFS.ORG_ID AS FILE_ID,JFS.ORG_NAME AS FILE_NAME" + "  ,5 AS FILE_TYPE"
                        + "   FROM JECN_FLOW_ORG JFS " + (fileId == null
                        ? ""
                        : (" INNER JOIN (" + getChildObjectsSqlByType(listIds, TreeNodeType.organization)
                        + ") JFO ON  JFO.ORG_ID = JFS.ORG_ID") + " WHERE JFS.DEL_STATE=0 AND JFS.projectid="
                        + projectId);
            case 6:
                return "SELECT JFS.FIGURE_ID AS FILE_ID,JFS.FIGURE_TEXT AS FILE_NAME    ,6 AS FILE_TYPE"
                        + "            FROM JECN_FLOW_ORG_IMAGE JFS"
                        + "            ,JECN_FLOW_ORG ORG WHERE JFS.ORG_ID=ORG.ORG_ID AND ORG.DEL_STATE=0 AND ORG.PROJECTID="
                        + projectId + " AND JFS.FIGURE_TYPE=" + getPosString() + (fileId == null ? ""
                        : ("             AND JFS.FIGURE_ID=" + fileId));
            case 7:
                return "SELECT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME"
                        + "  ,7 AS FILE_TYPE" + "   FROM JECN_FLOW_STRUCTURE JFS "
                        + (fileId == null
                        ? ""
                        : ("INNER JOIN (" + getChildObjectsSqlByType(listIds, TreeNodeType.process)
                        + ") JFO ON  JFO.FLOW_ID = JFS.FLOW_ID and jfo.isflow=0")
                        + "WHERE JFS.DEL_STATE=0 AND JFS.projectid=" + projectId);

            default:
                return "";
        }
    }

    public static String getBetweenAndSql(String field) {
        if (BaseSqlProvider.isSQLServer()) {
            return "'" + field + "'";
        } else if (BaseSqlProvider.isOracle()) {
            return "to_date('" + field + "','yyyy-mm-dd hh24:mi:ss')";
        }
        return "";
    }

    public static String getLengthFunc() {
        if (BaseSqlProvider.isOracle()) {
            return "length";
        } else {
            return "len";
        }
    }
}
