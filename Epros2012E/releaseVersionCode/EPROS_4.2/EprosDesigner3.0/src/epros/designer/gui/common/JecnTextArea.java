package epros.designer.gui.common;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JTextArea;

/***
 * JTextArea
 * 
 * @author 2012-06-06
 * 
 */
public class JecnTextArea extends JTextArea {

	public JecnTextArea() {
//		Dimension dimension = new Dimension(390, 50);
//		this.setPreferredSize(dimension);
//		this.setMaximumSize(dimension);
//		this.setMinimumSize(dimension);
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	}
	/**
	 * @param widthInt
	 */
	public JecnTextArea(int widthInt) {
		Dimension dimension = new Dimension(widthInt, 50);
		this.setPreferredSize(dimension);
		this.setMaximumSize(dimension);
		this.setMinimumSize(dimension);
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	}

	/**
	 * @param widthInt
	 * @param heigthInt
	 */
	public JecnTextArea(int widthInt,int heigthInt) {
		Dimension dimension = new Dimension(widthInt, heigthInt);
		this.setPreferredSize(dimension);
		this.setMaximumSize(dimension);
		this.setMinimumSize(dimension);
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
	}
}
