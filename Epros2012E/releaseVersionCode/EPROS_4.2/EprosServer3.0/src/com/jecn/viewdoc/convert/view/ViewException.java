package com.jecn.viewdoc.convert.view;

/**
 * 预览异常类
 * @author hyl
 *
 */
public class ViewException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ViewException(String message) {
		super(message);
	}

	public ViewException(String message, Throwable cause) {
		super(message, cause);
	}

}
