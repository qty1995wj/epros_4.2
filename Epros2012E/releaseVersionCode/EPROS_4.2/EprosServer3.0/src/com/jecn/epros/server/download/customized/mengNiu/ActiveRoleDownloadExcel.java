package com.jecn.epros.server.download.customized.mengNiu;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import jxl.CellView;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.process.JecnRefIndicators;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.RoleBean;
import com.jecn.epros.server.webBean.process.ActiveModeBean;
import com.jecn.epros.server.webBean.process.ActiveRoleBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;

/**
 * 流程角色关联生成Excel
 * 
 * @author fuzhh Apr 25, 2013
 * 
 */
public class ActiveRoleDownloadExcel {
	private static final Logger log = Logger
			.getLogger(ActiveRoleDownloadExcel.class);

	public String activeRoleDownloadExcel(ActiveRoleBean activeRoleBean) {
		// 临时文件存储路径
		String temporaryFilePath = JecnFinal.getAllTempPath(JecnFinal.saveExcelTempFilePath());
		// 创建导出的模板文件
		File fileData = new File(temporaryFilePath);

		WritableWorkbook wbookData = null;
		try {
			wbookData = Workbook.createWorkbook(fileData);
			// Sheet1名字 // 流程表单
			WritableSheet activeSheet = wbookData.createSheet(JecnUtil
					.getValue("processTheForm"), 0);
			// 表样式
			WritableCellFormat wformat = new WritableCellFormat();
			wformat.setBorder(Border.ALL, BorderLineStyle.THIN);
			WritableFont fontTitle = new WritableFont(WritableFont
					.createFont("微软雅黑"), 9, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE);
			WritableCellFormat wformatTitle = new WritableCellFormat(fontTitle);
			// 标题样式
			wformatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);
			// 设置对齐
			wformatTitle.setAlignment(Alignment.CENTRE);
			wformatTitle.setVerticalAlignment(VerticalAlignment.CENTRE);
			// 设置自动换行
			wformatTitle.setWrap(true);
			// 设置背景色
			wformatTitle.setBackground(Colour.LIGHT_GREEN);
			// 创建标题
			// 岗位
			activeSheet.addCell(new Label(0, 1, JecnUtil.getValue("pos"),
					wformatTitle));
			// 职责
			activeSheet.addCell(new Label(0, 2, JecnUtil
					.getValue("dutiesAndResponsibilities"), wformatTitle));
			// 流程
			activeSheet.addCell(new Label(0, 3, JecnUtil.getValue("flow"),
					wformatTitle));
			// 目标
			activeSheet.addCell(new Label(0, 4, JecnUtil.getValue("theTarget"),
					wformatTitle));
			// 行为
			activeSheet.addCell(new Label(0, 5, JecnUtil.getValue("behavior"),
					wformatTitle));
			// 动作
			activeSheet.addCell(new Label(0, 6, JecnUtil.getValue("action"),
					wformatTitle));
			// 需要填写的表格
			activeSheet.addCell(new Label(0, 7, JecnUtil
					.getValue("needToFillOutTheForm"), wformatTitle));

			/*
			 * WritableFont.createFont("宋体")：设置字体为宋体 10：设置字体大小
			 * WritableFont.NO_BOLD:设置字体非加粗（BOLD：加粗 NO_BOLD：不加粗） false：设置非斜体
			 * UnderlineStyle.NO_UNDERLINE：没有下划线
			 */
			WritableFont font = new WritableFont(WritableFont
					.createFont("微软雅黑"), 16, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE);
			// 公司名称样式
			WritableCellFormat wft = new WritableCellFormat(font);
			// 表标题样式
			wft.setBorder(Border.ALL, BorderLineStyle.THIN);
			// 设置对齐
			wft.setAlignment(Alignment.CENTRE);
			// 设置自动换行
			wft.setWrap(true);
			// 公司名称
			activeSheet.addCell(new Label(0, 0, activeRoleBean.getFlowName(),
					wft));
			// 合并公司名称
			activeSheet
					.mergeCells(0, 0, activeRoleBean.getRoleList().size(), 0);
			// 合并活动
			activeSheet
					.mergeCells(1, 5, activeRoleBean.getRoleList().size(), 5);
			// 合并流程
			activeSheet
					.mergeCells(1, 3, activeRoleBean.getRoleList().size(), 3);
			// 设置数据
			outPutExcelBuss(activeSheet, activeRoleBean);

			// 写入本地
			wbookData.write();
			//必须调用close才往输出流中写数据
			wbookData.close();
			wbookData=null;
		} catch (Exception e) {
			log.error("生成模板数据异常！", e);
			return null;
		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("生成活动角色关联Excel后关闭WritableWorkbook异常！", e);
				}
			}
		}
		return temporaryFilePath;
	}

	/**
	 * 生成显示数据
	 * 
	 * @author fuzhh May 2, 2013
	 * @param activeSheet
	 * @param activeRoleBean
	 * @throws WriteException
	 */
	private void outPutExcelBuss(WritableSheet activeSheet,
			ActiveRoleBean activeRoleBean) throws WriteException {
		WritableFont font = new WritableFont(WritableFont.createFont("微软雅黑"),
				9, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE);
		// 内容样式
		WritableCellFormat wformat = new WritableCellFormat(font);
		wformat.setBorder(Border.ALL, BorderLineStyle.THIN);
		// 设置对齐
		wformat.setVerticalAlignment(VerticalAlignment.CENTRE);
		// 设置自动换行
		wformat.setWrap(true);

		// 角色内容样式
		WritableCellFormat roleFormat = new WritableCellFormat(font);
		roleFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		// 设置对齐
		roleFormat.setAlignment(Alignment.CENTRE);
		roleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		// 设置自动换行
		roleFormat.setWrap(true);

		// 存储活动编号 和 活动说明的集合
		SortedMap<Integer, String> activeMap = new TreeMap<Integer, String>();
		// 存储没有编号 ， 或编号不为数字的 活动说明集合
		List<String> activeList = new ArrayList<String>();
		// 最大的活动个数
		int maxActiveCount = 3;
		// 最大的文件数
		int maxFileCount = 3;
		// 角色职责个数
		int maxRoleShow = 0;
		// 指标最大的个数
		int maxIndicator = 3;
		// 图片宽度
		int imgWidth = 1;
		// 图片高度
		int imgHeight = 1;
		// 所占行数
		int colSize = 1;
		// 列宽
		int colWidth = 13;

		BufferedImage bufferedImage = null;
		File file = new File(JecnFinal.getAllTempPath(activeRoleBean
				.getImgPath()));
		if (file.exists()) {
			byte[] imgByte = null;
			try {
				activeSheet.addCell(new Label(1, 3, "", wformat));
				bufferedImage = ImageIO.read(file);
				// 图片宽度
				float imgWid = bufferedImage.getWidth();
				// 图片高度
				float imgHeig = bufferedImage.getHeight();
				// 图片横向放大缩小比例
				float scaleWidth = 1;
				// 图片纵向放大缩小比例
				float scaleHeight = 1;
				// 图片方大缩小倍数
				float scale = 1;
				if (imgWid > 1200) {
					scaleWidth = 1200 / imgWid;
				}
				if (imgHeig > 600) {
					scaleHeight = 600 / imgHeig;
				}
				if (scaleWidth < scaleHeight) {
					scale = scaleWidth;
				} else {
					scale = scaleHeight;
				}
				// 执行放大缩小
				BufferedImage newBufImg = scaleImg(bufferedImage, scale);
				imgWidth = newBufImg.getWidth();
				imgHeight = newBufImg.getHeight();
				for (int i = 0; i <= activeRoleBean.getRoleList().size(); i++) {
					if ((activeRoleBean.getRoleList().size() - i) > 0) {
						int wid = imgWidth
								/ (activeRoleBean.getRoleList().size() - i);
						if (wid > 97) {
							colSize = activeRoleBean.getRoleList().size() - i;
							colWidth = wid / 7;
							break;
						}
					} else {
						colSize = 1;
						colWidth = 97;
						break;
					}
				}
				imgByte = imgToByte(newBufImg);
			} catch (IOException e) {
				log.error("图片流异常！", e);
			}
			if (imgByte != null) {
				WritableImage wImage = new WritableImage(1, 3, colSize, 1,
						imgByte);
				activeSheet.addImage(wImage);
			}
		}

		for (int i = 0; i < activeRoleBean.getRoleList().size(); i++) {
			// 指标个数
			int indicatorCount = 0;
			// 文件个数
			int fileCount = 0;

			// 设置列宽
			activeSheet.setColumnView(i + 1, colWidth);
			RoleBean roleBean = activeRoleBean.getRoleList().get(i);
			// 角色名称
			activeSheet.addCell(new Label(i + 1, 1, roleBean.getRoleName(),
					roleFormat));
			// 角色职责
			String roleShow = roleBean.getRoleShow();
			if (!JecnCommon.isNullOrEmtryTrim(roleShow)) {
				maxRoleShow++;
			}
			activeSheet.addCell(new Label(i + 1, 2, roleShow, wformat));

			// 获取最大的活动数
			if (roleBean.getProcessActiveWebList().size() > maxActiveCount) {
				maxActiveCount = roleBean.getProcessActiveWebList().size();
			}
			// 指标名称和指标值
			String indicatorStr = "";
			// 活动名称
			String activeName = "";
			// 需要输入的文件名称
			String inputName = "";
			// 输出样例的文件名称
			String outTempletName = "";
			// 输出表单的文件名称
			String outModeName = "";
			// 操作规范文件名称
			String operationName = "";

			int indicaCount = 1;
			// 循环活动
			for (ProcessActiveWebBean processActiveWeb : roleBean
					.getProcessActiveWebList()) {
				indicatorCount += processActiveWeb.getIndicatorsList().size();
				// 指标
				for (int j = 0; j < processActiveWeb.getIndicatorsList().size(); j++) {
					JecnRefIndicators refIndicators = processActiveWeb
							.getIndicatorsList().get(j);
					indicatorStr += indicaCount + "、"
							+ refIndicators.getIndicatorName() + ":"
							+ refIndicators.getIndicatorValue() + "\n";
					indicaCount++;
				}
				// 活动编号
				String activeId = processActiveWeb.getActiveId();
				// 活动说明
				String activeShow = processActiveWeb.getActiveShow();
				// 转换后的活动编号
				int activeNum = -1;
				if (!JecnCommon.isNullOrEmtryTrim(activeShow)) {
					if (!JecnCommon.isNullOrEmtryTrim(activeId)) {
						// 截取自动编号后面的数字
						String curActivityId = activeId.substring(activeId
								.lastIndexOf("-") + 1, activeId.length());
						if (JecnCommon.judgeObjIsNum(curActivityId)) {
							activeNum = Integer.valueOf(curActivityId);
							// 添加 活动编号
							activeMap.put(activeNum, activeId + "、"
									+ activeShow);
						} else {
							activeList.add(activeId + "、" + activeShow);
						}
					} else {
						activeList.add(activeShow);
					}
				}

				// 活动名称
				activeName += processActiveWeb.getActiveName() + "\n";
				// 去重用的set
				Set<Long> fileSet = new HashSet<Long>();
				// 输入
				for (FileWebBean fileWebBean : processActiveWeb.getListInput()) {
					boolean isInsert = true;
					for (Long fileId : fileSet) {
						if (fileWebBean.getFileId() == fileId.longValue()) {
							isInsert = false;
							break;
						}
					}
					if (isInsert) {
						inputName += fileWebBean.getFileName() + "\n";
						fileSet.add(fileWebBean.getFileId());
					}
				}
				// 输出
				fileSet.clear();
				for (ActiveModeBean activeModeBean : processActiveWeb
						.getListMode()) {
					boolean isInsert = true;
					for (Long fileId : fileSet) {
						if (activeModeBean.getModeFile().getFileId() == fileId
								.longValue()) {
							isInsert = false;
							break;
						}
					}
					if (isInsert) {
						// 输出表单文件名称
						outModeName += activeModeBean.getModeFile()
								.getFileName()
								+ "\n";
						fileSet.add(activeModeBean.getModeFile().getFileId());
					}
					if (activeModeBean.getListTemplet() != null) {
						for (FileWebBean fileWebBean : activeModeBean
								.getListTemplet()) {
							boolean isModeInsert = true;
							for (Long fileId : fileSet) {
								if (fileWebBean.getFileId() == fileId
										.longValue()) {
									isModeInsert = false;
									break;
								}
							}
							if (isModeInsert) {
								// 输出样例文件名称
								outTempletName += fileWebBean.getFileName()
										+ "\n";
								fileSet.add(fileWebBean.getFileId());
							}

						}
					}
				}
				// 操作规范
				fileSet.clear();
				for (FileWebBean fileWebBean : processActiveWeb
						.getListOperation()) {
					boolean isInsert = true;
					for (Long fileId : fileSet) {
						if (fileWebBean.getFileId() == fileId.longValue()) {
							isInsert = false;
							break;
						}
					}
					if (isInsert) {
						operationName += fileWebBean.getFileName() + "\n";
						fileSet.add(fileWebBean.getFileId());
					}
				}
				fileSet = null;
			}
			// 目标
			activeSheet.addCell(new Label(i + 1, 4, indicatorStr, wformat));
			// 添加活动名称
			activeSheet.addCell(new Label(i + 1, 6, activeName, wformat));
			// 拼装需要填写的表格
			String tableName = "";
			if (!JecnCommon.isNullOrEmtryTrim(inputName)) {
				// 输入
				tableName += JecnUtil.getValue("input") + ":\n   " + inputName;
				fileCount += 2;
			}
			if (!JecnCommon.isNullOrEmtryTrim(operationName)) {
				// 操作规范
				tableName += JecnUtil.getValue("operationSpecifications")
						+ ":\n   " + operationName;
				fileCount += 2;
			}
			if (!JecnCommon.isNullOrEmtryTrim(outModeName)) {
				// 表单
				tableName += JecnUtil.getValue("output") + ":\n   "
						+ outModeName;
				fileCount += 2;
			}
			if (!JecnCommon.isNullOrEmtryTrim(outTempletName)) {
				// 样例
				tableName += "\n   " + outTempletName;
				fileCount += 2;
			}
			activeSheet.addCell(new Label(i + 1, 7, tableName, wformat));
			if (fileCount > maxFileCount) {
				maxFileCount = fileCount;
			}
			if (indicatorCount > maxIndicator) {
				maxIndicator = indicatorCount;
			}
		}
		if (maxRoleShow < 3) {
			maxRoleShow = 3;
		}
		// 活动说明
		String activeShow = getActiveShow(activeMap, activeList);
		activeSheet.addCell(new Label(1, 5, activeShow, wformat));
		int actinvShowCount = activeMap.size() + activeList.size();
		if (actinvShowCount < 3) {
			actinvShowCount = 3;
		}
		// 设置列高
		CellView cellView = new CellView();
		cellView.setAutosize(true);
		activeSheet.setRowView(0, 500);
		activeSheet.setRowView(1, 350);
		activeSheet.setRowView(2, 500 * maxRoleShow);
		activeSheet.setRowView(3, imgHeight * 15);
		activeSheet.setRowView(4, 400 * maxIndicator);
		activeSheet.setRowView(5, 500 * actinvShowCount);
		activeSheet.setRowView(6, 300 * maxActiveCount);
		activeSheet.setRowView(7, 300 * maxFileCount);
		// 设置列宽
		activeSheet.setColumnView(0, 12);
		// byte[] bstrLength = rs.getString(j).getBytes(); //中文字符算两个字节
		// sheet.setColumnView(j-1, bstrLength.length+2);
	}

	/**
	 * 转换Map,List 为String数据输出
	 * 
	 * @author fuzhh Apr 26, 2013
	 * @param activeMap
	 * @return
	 */
	private String getActiveShow(SortedMap<Integer, String> activeMap,
			List<String> activeList) {
		String activeShow = "";
		Set<Integer> set = activeMap.keySet();
		for (int s : set) {
			activeShow += activeMap.get(s) + "\n";
		}
		for (String str : activeList) {
			activeShow += str + "\n";
		}
		return activeShow;
	}

	/**
	 * 图片的缩放
	 * 
	 * @author fuzhh May 2, 2013
	 * @param image
	 * @param scale
	 *            倍数
	 * @return
	 */
	public BufferedImage scaleImg(BufferedImage image, float scale) {
		int w = (int) (image.getWidth(null) * scale);
		int h = (int) (image.getHeight(null) * scale);

		BufferedImage tmp = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		tmp.getGraphics().drawImage(
				image.getScaledInstance(w, h, Image.SCALE_SMOOTH), 0, 0, null);
		// Graphics2D g2 = tmp.createGraphics();
		// g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		// RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		// g2.drawImage(image, 0, 0, w, h, null);
		// g2.dispose();
		return tmp;
	}

	/**
	 * img 转换为 byte[]
	 * 
	 * @author fuzhh May 2, 2013
	 * @param image
	 * @return
	 */
	public byte[] imgToByte(BufferedImage image) {
		ByteArrayOutputStream imageStream = null;
		try {
			imageStream = new ByteArrayOutputStream();
			ImageIO.write(image, "png", imageStream);
			byte[] imgByte = imageStream.toByteArray();
			return imgByte;
		} catch (IOException e) {
			log.error("图片转换异常！", e);
			return null;
		} finally {
			if (imageStream != null) {
				try {
					imageStream.close();
				} catch (IOException e) {
					log.error("关闭图片流失败！", e);
				}
			}
		}
	}
}
