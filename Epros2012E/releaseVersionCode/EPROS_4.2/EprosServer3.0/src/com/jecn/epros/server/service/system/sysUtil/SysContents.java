package com.jecn.epros.server.service.system.sysUtil;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;

public class SysContents {
	/** 服务器到期提醒！ */
	public final static String SUB_JECT = "服务器到期提醒！";

	public final static String MAIL_CONTENT_2 = "天后到期，密钥到期后平台会自动停止，为了不影响贵公司正常工作，请及时联系杰成公司更新密钥。";

	public final static String MAIL_CONTENT_3 = "<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;杰成公司联系方式：<BR>";

	public final static String MAIL_CONTENT_4 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联系电话：400-619-4800<BR>";

	public final static String MAIL_CONTENT_5 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司网址：http://www.jecn.com.cn<BR><BR>";

	public final static String MAIL_CONTENT_6 = "&nbsp;&nbsp;&nbsp;&nbsp;给您带来的不便请您谅解！！！<BR><BR>";

}
