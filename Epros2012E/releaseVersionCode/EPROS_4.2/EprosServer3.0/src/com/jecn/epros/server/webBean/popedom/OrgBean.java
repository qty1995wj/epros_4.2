package com.jecn.epros.server.webBean.popedom;

import java.io.Serializable;

/**
 * @author yxw 2012-12-26
 * @description：浏览端组织显示列表数据
 */
public class OrgBean  implements Serializable{
	/**组织ID*/
	private Long orgId;
	/**组织名称*/
	private String orgName;
	/**上级部门ID*/
	private Long orgPid;
	/**上级部门名称*/
	private String orgPname;
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Long getOrgPid() {
		return orgPid;
	}
	public void setOrgPid(Long orgPid) {
		this.orgPid = orgPid;
	}
	public String getOrgPname() {
		return orgPname;
	}
	public void setOrgPname(String orgPname) {
		this.orgPname = orgPname;
	}
}
