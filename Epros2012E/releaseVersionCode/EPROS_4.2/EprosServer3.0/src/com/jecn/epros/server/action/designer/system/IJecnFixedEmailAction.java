package com.jecn.epros.server.action.designer.system;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnFixedEmail;

/***
 * 发布 固定人员处理JECN_FIXED_EMAIL
 * 2014-04-09
 *
 */
public interface IJecnFixedEmailAction {

	/***
	 * 添加  固定人员发送邮件消息数据
	 * @param fixedEmail
	 * @return
	 * @throws Exception
	 */
	public void AddFixedEmail(List<JecnFixedEmail> fixedEmail) throws Exception;
	
	/***
	 * 删除  固定人员发送邮件消息数据
	 * @param fixedEmailIds
	 * @throws Exception
	 */
	public void deleteFixedEmail(List<String> fixedEmailIds) throws Exception;
	
	/***
	 *   获取固定人员发送邮件消息数据
	 * @return
	 * @throws Exception
	 */
	public List<JecnFixedEmail> getFixedEmailList() throws Exception;
}
