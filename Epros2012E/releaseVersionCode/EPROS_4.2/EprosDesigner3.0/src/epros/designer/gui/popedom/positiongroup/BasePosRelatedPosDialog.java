package epros.designer.gui.popedom.positiongroup;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * HR基准岗位所关联的岗位
 * 
 * @author Administrator 2015-03-23
 */
public class BasePosRelatedPosDialog extends JecnDialog {
	private JecnPanel mainPanel = null;
	private JecnPanel infoPanel = null;
	private JecnPanel basePosTablePanel = null;
	private JecnPanel buttonPanel = null;
	/** table滚动面板 */
	private JScrollPane scrollPanel = null;
	/** table数据 */
	protected BasePosRelatedPosTable basePosable = null;
	/** 分页面板 */
	private JPanel pagePanel = new JPanel();
	private JLabel nowPage = new JLabel(" ");

	/** 上一页 */
	private JLabel previousPage = new JLabel(new ImageIcon(
			"images/icons/previouspage.gif"));

	/** 下一页 */
	private JLabel nextPage = new JLabel(new ImageIcon(
			"images/icons/nextpage.gif"));
	/** 首页 */
	private JLabel homePage = new JLabel(JecnProperties.getValue("homePage"));
	/** 尾页 */
	private JLabel endPage = new JLabel(JecnProperties.getValue("endPage"));
	private String operationType = null;
	/** 总页数 */
	private JLabel totalPages = new JLabel();
	/** 关闭 **/
	private JButton canelButton = null;

	/** 基准岗位ID */
	private String baseId = null;

	public BasePosRelatedPosDialog(String baseId) {
		this.baseId = baseId;
		this.setResizable(true); // 设置窗体不能改变大小
		this.setModal(true); // 模态化窗体
		this.setSize(450, 400);
		this.setLocationRelativeTo(null); // 居中
		this.setTitle(JecnProperties.getValue("hrBasePos"));
		initCom();
		initLayout();
		// 初始化岗位对应基准岗位
		searchPerson();
	}

	/** 初始化参数 */
	private void initCom() {
		scrollPanel = new JScrollPane();
		basePosable = new BasePosRelatedPosTable();
		// 取消
		canelButton = new JButton(JecnProperties.getValue("closed"));

		mainPanel = new JecnPanel();
		infoPanel = new JecnPanel();

		basePosTablePanel = new JecnPanel();
		buttonPanel = new JecnPanel();
		scrollPanel = new JScrollPane();
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		basePosTablePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		scrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		pagePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		scrollPanel.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		scrollPanel.setViewportView(basePosable);

		// 取消
		canelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canelButtonPerformed();
			}
		});
		// 上一页
		previousPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// 如果为初始页或第一天时，点上一而直接返回，不再去请求后台
				if (DrawCommon.isNullOrEmtryTrim(nowPage.getText())
						|| nowPage.getText().equals("1")) {
					return;
				}
				operationType = "previous";
				searchPerson();
			}

		});
		// 下一页
		nextPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				operationType = "next";
				searchPerson();
			}

		});
		// 首页
		homePage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				operationType = "home";
				searchPerson();
			}

		});
		// 尾页
		endPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				operationType = "end";
				searchPerson();
			}
		});
	}

	private void canelButtonPerformed() {
		this.dispose();
	}

	/** 初始化布局 */
	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets2 = new Insets(0, 5, 0, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets2, 0, 0);
		mainPanel.add(infoPanel, c);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets2, 0, 0);
		infoPanel.add(basePosTablePanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("connPosition")));
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets2,
				0, 0);
		basePosTablePanel.add(scrollPanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0);
		infoPanel.add(pagePanel, c);

		pagePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		mainPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(canelButton);

		pagePanel.add(new JLabel(JecnProperties.getValue("regulation")));// 第
		pagePanel.add(nowPage);

		pagePanel.add(nowPage);

		pagePanel.add(new JLabel(JecnProperties.getValue("page")));// 页JecnProperties.getValue("page")
		// 上一页
		pagePanel.add(previousPage);
		// 下一页
		pagePanel.add(nextPage);
		// 首页
		pagePanel.add(homePage);
		// 尾页
		pagePanel.add(endPage);
		JPanel p = new JPanel();
		p.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(70, 1);
		p.setPreferredSize(dimension);
		p.setMinimumSize(dimension);
		pagePanel.add(p);
		// 总页数：
		pagePanel.add(new JLabel(JecnProperties.getValue("totalPages")));// JecnProperties.getValue("totalPages")
		pagePanel.add(totalPages);
		pagePanel.add(new JLabel("  "));
		this.getContentPane().add(mainPanel);

	}

	private void searchPerson() {
		// 基准岗位对应的岗位集合
		List<Object[]> objectList = null;
		try {
			objectList = ConnectionPool.getPosGroup().getBaseRelPos(
					baseId,
					DrawCommon.isNullOrEmtryTrim(nowPage.getText()) ? "0" : nowPage
							.getText(), "20", operationType);
		} catch (Exception e) {
			log.error("searchPerson getBaseRelPos is error！", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("systemError"));
			return;
		}
		Object[] obj = objectList.get(objectList.size() - 1);
		if (obj != null) {
			nowPage.setText(obj[1].toString());
			totalPages.setText(obj[0].toString());
		}
		objectList.remove(obj);

		// 分页查询结果 显示
		basePosable.pageQueryResult(objectList);
	}

}
