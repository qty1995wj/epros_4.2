package com.jecn.epros.mapper;

import com.jecn.epros.sqlprovider.RoleSqlProvider;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

public interface RoleMapper {


    @SelectProvider(type = RoleSqlProvider.class, method = "getSecondAdminRoleId")
    @ResultType(java.lang.Integer.class)
    public Long getSecondAdminRoleId(Long peopleId);


}
