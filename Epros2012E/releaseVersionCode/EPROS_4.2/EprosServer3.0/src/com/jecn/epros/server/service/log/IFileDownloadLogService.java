package com.jecn.epros.server.service.log;

import com.jecn.epros.server.bean.log.FileDownloadLog;
import com.jecn.epros.server.common.IBaseService;

public interface IFileDownloadLogService extends
		IBaseService<FileDownloadLog, Long> {

}
