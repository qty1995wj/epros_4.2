package wordxml.element.bean.paragraph;

import wordxml.constant.Constants.LineRuleType;

/**
 * 段落间距bean
 */
public class ParagraphSpaceBean {

	/** 段前距离 单位/行 转换为缇 0.1*100=10 */
	private Float beforeLines;
	/** 段后距离 单位/行 转换为缇 0.1*100=10 */
	private Float afterLines;
	/** 行距类型 默认行距类型为：固定值，12磅 */
	private ParagraphLineRule lineRule = new ParagraphLineRule(12, LineRuleType.EXACT);
	/** 单位类型 默认是行 0行 1是磅 **/
	private int type = 0;

	public ParagraphSpaceBean() {

	}

	public ParagraphSpaceBean(Float beforeLines, Float afterLines) {
		this.beforeLines = beforeLines;
		this.afterLines = afterLines;
	}

	public ParagraphSpaceBean(Float beforeLines, Float afterLines, ParagraphLineRule lineRule) {
		this(beforeLines, afterLines);
		this.lineRule = lineRule;
	}

	public ParagraphSpaceBean(Float beforeLines, Float afterLines, int type) {
		this.beforeLines = beforeLines;
		this.afterLines = afterLines;
		this.type = type;
	}

	public void setLineRule(ParagraphLineRule lineRule) {
		this.lineRule = lineRule;
	}

	public LineRuleType getRuleType() {
		return this.lineRule.getRuleType();
	}

	public float getLine() {
		return this.lineRule.getLine();
	}

	public Float getBeforeLines() {
		return beforeLines;
	}

	public void setBeforeLines(Float beforeLines) {
		this.beforeLines = beforeLines;
	}

	public Float getAfterLines() {
		return afterLines;
	}

	public void setAfterLines(Float afterLines) {
		this.afterLines = afterLines;
	}

	public ParagraphLineRule getLineRule() {
		return lineRule;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
