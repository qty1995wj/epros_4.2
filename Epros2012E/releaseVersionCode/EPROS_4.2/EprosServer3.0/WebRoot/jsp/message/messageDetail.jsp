<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../../css/global.css" rel="stylesheet" type="text/css" />
<link href="../../css/style.css" rel="stylesheet" type="text/css" />
<title><s:text name="mesgDesInfo" /></title><!-- 消息详细信息： -->
</head>
<body style="height: 710px;">
	<table width="99%" border="0" align="center" cellpadding="0"
			cellspacing="0">
			<tr>
<!--				<td width="8" valign="bottom"><img src="../../images/main_left4.jpg" width="8" height="7" border="0" align="absmiddle" /></td>-->
				<td class="main_middle4"></td>
<!--				<td width="8"><img src="../../images/main_right4.jpg" width="8" height="7" border="0" align="absmiddle" /></td>-->
			</tr>
			<tr>
				<td class="main_left5">
					&nbsp;
				</td>
				<td valign="top" style="background-color: #FFFFFF" id="showFrame">
					<table width="100%" border="0" align="center" cellpadding="0"
						cellspacing="0">
						<tr>
							<td valign="top" style="background-color: #FFFFFF">
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
									<tr>
										<td valign="top">
											<table width="100%" border="0" align="center" cellpadding="0"
												cellspacing="0">
												<tr>
													<td width="9" valign="top">
														<img src="${basePath}images/main_left10.jpg" width="9" height="29" border="0" />
													</td>
													<td   style="background-image: url('${basePath}images/main_middle6.jpg');">
														<span class="t4" style="font-size: 14px;font-weight: bold" ><s:text name="readMesage" /></span><!-- 阅读消息： -->
													</td>
													<td width="8" valign="top">
														<img src="${basePath}images/main_right7.jpg" width="8" height="29" border="0" align="middle" />
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td height="441" style="background-color: #FFFFFF;border: 1px;border-style: solid;border-color: lightgray;" class="border04" id="screenRight" >
											<div style="padding-top: 10px;width: 100%;border: 1px;border-color: gray;">
												<div class="scroll01" style="HEIGHT: 700px;" id="contextJecnId">
													<table width="90%" border="0" cellpadding="0"
														cellspacing="0">
														<tr>
															<td height="35" align="right" style="font-size: 12px;">
<!--																<s:text name="Sender"/>:--><s:text name="sender" /><!-- 发送人： -->
															</td>
															<td width="90%" >
																<input name="textfield" type="text" class="input02" style="width: 90%"
																	value=${jecnMessageReaded.peopleName } size="80" readonly /></td>
														</tr>
														<tr>
															<td height="35" align="right" style="font-size: 12px;">
<!--																<s:text name="Topics"/>:--><s:text name="mainTitle" /><!-- 主题： -->
															</td>
															<td width="90%" >
																<input name="textfield2" type="text" class="input02"  style="width: 90%"
																	value=${jecnMessageReaded.messageTopic } size="80" readonly /></td>
														</tr>
														<tr>
															<td valign="top" align="right" style="font-size: 12px;">
<!--																<s:text name="rocessPerformanceReport.Content"/>:--><s:text name="mesgcontent" /><!-- 内容： -->
															</td>
															<td width="90%" >
																<textarea name="textarea" cols="59" rows="10" style="width: 90%"
																	class="input01" readonly>${jecnMessageReaded.messageContent}</textarea>
															</td>
														</tr>
													</table>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="16" valign="top" style="background-color: #FFFFFF" class="border02">
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
				<td class="main_right5">
					&nbsp;
				</td>
			</tr>
			<tr>
<!--				<td><img src="../../images/main_left6.jpg" width="8" height="8" border="0" align="absmiddle" /></td>-->
				<td class="main_middle5"></td>
<!--				<td><img src="../../images/main_right6.jpg" width="8" height="8" border="0" align="absmiddle" /></td>-->
			</tr>

		</table>
</body>
</html>