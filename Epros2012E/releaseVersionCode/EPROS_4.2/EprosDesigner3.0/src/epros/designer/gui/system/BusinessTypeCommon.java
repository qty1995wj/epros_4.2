package epros.designer.gui.system;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.ConfigCommon.ConfigCommonEnum;
import epros.designer.gui.system.combo.CheckBoxPo;
import epros.designer.gui.system.combo.JecnSecurityLevelCommon;
import epros.designer.gui.system.combo.KeyValComboBoxCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnResourceUtil;

/**
 * 业务类型
 * 
 * @author zhangchen
 * 
 */
public class BusinessTypeCommon extends KeyValComboBoxCommon  {
	private static Logger log = Logger.getLogger(JecnSecurityLevelCommon.class);

	public BusinessTypeCommon(int row, JecnPanel infoPanel, Insets insets, String key, JecnDialog jecnDialog,
			boolean isRequest) {
		super(row, infoPanel, insets, key, jecnDialog, getLevelTitle(), isRequest);
	}
	/**
	 * 保密级别 title初始化
	 * 
	 * @return
	 */
	private static String getLevelTitle() {
		return "业务类型";
	}

	@SuppressWarnings("unchecked")
	@Override
	public Vector getListCheckBoxPo() {
		Vector model = new Vector();
		try {
			List<JecnDictionary> list = ConnectionPool.getOrganizationAction().getDictionarys(DictionaryEnum.BUSS_TYPE);
			int languageType = JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1;
			model.add(new CheckBoxPo("0", " "));//加入默认值为空
			for (JecnDictionary dictionary : list) {
				model.add(new CheckBoxPo(dictionary.getCode().toString(), dictionary.getValue(languageType)));
			}
		} catch (Exception e1) {
			log.error("JecnSecurityLevelCommon getListCheckBoxPo is error", e1);
		}
		return model;
	}

}
