package epros.draw.gui.figure;

import java.awt.Graphics2D;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;

public enum JecnPaintEnum {
	INTANCE;

	/**
	 * 无填充
	 * 
	 * @param flowElementData
	 * @param mapElemType
	 * @param g2d
	 */
	public void paintNoneFigure(MapElemType mapElemType, JecnAbstractFlowElementData flowElementData, Graphics2D g2d,
			int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6,
			int userWidth, int userHeight) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		switch (mapElemType) {
		case Oval:// 椭圆
		case OvalSubFlowFigure:
			g2d.drawOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth), Math.abs(y1
					- userHeight));
			break;
		case Triangle:// 三角形
		case EndFigure:
		case KeyPointFigure:
			g2d.drawPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
			break;
		case FunctionField:// 四边形
		case RoleFigure:
		case CustomFigure:
		case RoleFigureAR:
		case Rect:
		case DateFreeText:
		case PositionFigure:
		case TermFigureAR:
		case VirtualRoleFigure:
			g2d.drawRect(0, 0, userWidth, userHeight);
			break;
		case Pentagon:// 五边形
		case SystemFigure://制度
		case StageSeparatorPentagon://流程图 阶段分隔符五边形
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
			break;
		case RightHexagon:// 箭头六边形
		case InterfaceRightHexagon://接口六边形
			g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
			break;
		case InputHandFigure:// 手动输入
			g2d.drawPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
			break;
		case IsoscelesTrapezoidFigure:// 等腰梯形
			g2d.drawPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
			break;
		case FreeText:// 自由文件框
			if (flowElementData.getBodyWidth() > 0) {
				g2d.drawRect(0, 0, userWidth, userHeight);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintHandTop(MapElemType mapElemType, JecnAbstractFlowElementData flowElementData, Graphics2D g2d,
			int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6,
			int shadowCount, int shadowCountmax, int userWidth, int userHeight) {
		switch (mapElemType) {
		case Oval:// 椭圆
		case OvalSubFlowFigure:
			g2d.fillOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - shadowCount,
					Math.abs(y1 - userHeight) - shadowCount);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - shadowCount,
					Math.abs(y1 - userHeight) - shadowCount);
			break;
		case Triangle:// 三角形
		case EndFigure:
		case KeyPointFigure:
			paintTriangleTop(flowElementData, g2d, x1, x2, x3, y1, y2, y3, shadowCount);
			break;
		case FunctionField:// 四边形
		case FreeText:
		case RoleFigure:
		case CustomFigure:
		case RoleFigureAR:
		case Rect:
		case DateFreeText:
		case PositionFigure:
		case TermFigureAR:
		case VirtualRoleFigure:
			paintRectTop(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, shadowCount,
					shadowCountmax, userWidth, userHeight);
			break;
		case Pentagon:// 五边形
		case SystemFigure://制度
		case StageSeparatorPentagon://流程图 阶段分隔符五边形
			paintPentagonHandTop(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, shadowCount,
					shadowCountmax);
			break;
		case RightHexagon:// 箭头六边形
		case InterfaceRightHexagon://接口六边形
			paintRightHexagonTop(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, shadowCount);
			break;
		case InputHandFigure:// 手动输入
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount, x4 }, new int[] { y1 - shadowCount, y2,
					y3 - shadowCount, y4 - shadowCount }, 4);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount, x4 }, new int[] { y1 - shadowCount, y2,
					y3 - shadowCount, y4 - shadowCount }, 4);
			break;
		case IsoscelesTrapezoidFigure:// 等腰梯形
			// 填充图形
			g2d.fillPolygon(new int[] { x1, x2 + shadowCount, x3 + shadowCount, x4 }, new int[] { y1, y2,
					y3 + shadowCount, y4 + shadowCount }, 4);
			// 图形边框色
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.drawPolygon(new int[] { x1, x2 + shadowCount, x3 + shadowCount, x4 }, new int[] { y1, y2,
					y3 + shadowCount, y4 + shadowCount }, 4);
			break;
		default:
			break;
		}
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DHandLow(MapElemType mapElemType, JecnAbstractFlowElementData flowElementData, Graphics2D g2d,
			int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6,
			int indent3Dvalue, int indent3Dvaluemax, int userWidth, int userHeight) {
		switch (mapElemType) {
		case Oval:// 椭圆
		case OvalSubFlowFigure:
			g2d.fillOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - indent3Dvalue,
					Math.abs(y1 - userHeight) - indent3Dvalue);
			g2d.setStroke(flowElementData.getBasicStroke());
			break;
		case Triangle:// 三角形
		case EndFigure:
		case KeyPointFigure:
			paint3DTriangleLow(flowElementData, g2d, x1, x2, x3, y1, y2, y3, indent3Dvalue);
			break;
		case FunctionField:// 四边形
		case FreeText:
		case RoleFigure:
		case CustomFigure:
		case RoleFigureAR:
		case Rect:
		case DateFreeText:
		case PositionFigure:
		case TermFigureAR:
		case VirtualRoleFigure:
			g2d.fillRect(0, 0, userWidth - indent3Dvalue, userHeight - indent3Dvalue);
			g2d.setStroke(flowElementData.getBasicStroke());
			break;
		case Pentagon:// 五边形
		case SystemFigure://制度
		case StageSeparatorPentagon://流程图 阶段分隔符五边形
			paintPentagon3DHandLow(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue,
					indent3Dvaluemax);
			break;
		case RightHexagon:// 箭头六边形
		case InterfaceRightHexagon://接口六边形
			paint3DRightHexagonLow(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue);
			break;
		case InputHandFigure:// 手动输入
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 }, new int[] {
					y1 - indent3Dvalue, y2, y3 - indent3Dvalue, y4 - indent3Dvalue }, 4);
			g2d.setStroke(flowElementData.getBasicStroke());
			break;
		case IsoscelesTrapezoidFigure:// 等腰梯形
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 }, new int[] { y1, y2,
					y3 - indent3Dvalue, y4 - indent3Dvalue }, 4);
			g2d.setStroke(flowElementData.getBasicStroke());
			break;
		default:
			break;
		}
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	public void paint3DInputHandLine(MapElemType mapElemType, JecnAbstractFlowElementData flowElementData,
			Graphics2D g2d, int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5,
			int y6, int indent3Dvalue, int indent3Dvaluemmax, int userWidth, int userHeight) {
		switch (mapElemType) {
		case Oval:// 椭圆
		case OvalSubFlowFigure:
			g2d.drawOval(Math.min(x1, userWidth), Math.min(y1, userHeight), Math.abs(x1 - userWidth) - indent3Dvalue,
					Math.abs(y1 - userHeight) - indent3Dvalue);
			break;
		case Triangle:// 三角形
		case EndFigure:
		case KeyPointFigure:
			paint3DTriangleLine(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue);
			break;
		case FunctionField:// 四边形
		case FreeText:
		case RoleFigure:
		case CustomFigure:
		case RoleFigureAR:
		case Rect:
		case DateFreeText:
		case PositionFigure:
		case TermFigureAR:
		case VirtualRoleFigure:
			g2d.drawRect(0, 0, userWidth - indent3Dvalue, userHeight - indent3Dvalue);
			break;
		case Pentagon:// 五边形
		case SystemFigure://制度
		case StageSeparatorPentagon://流程图 阶段分隔符五边形
			paintPentagon3DInputHandLine(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6,
					indent3Dvalue, indent3Dvaluemmax);
			break;
		case RightHexagon:// 箭头六边形
		case InterfaceRightHexagon://接口六边形
			paintRightHexagon3DInputHandLine(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue);
			break;
		case InputHandFigure:// 手动输入
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 }, new int[] {
					y1 - indent3Dvalue, y2, y3 - indent3Dvalue, y4 - indent3Dvalue }, 4);
			break;
		case IsoscelesTrapezoidFigure:// 等腰梯形
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 }, new int[] { y1, y2,
					y3 - indent3Dvalue, y4 - indent3Dvalue }, 4);
			break;
		default:
			break;
		}
	}

	public void paint3DPartTop(MapElemType mapElemType, JecnAbstractFlowElementData flowElementData, Graphics2D g2d,
			int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6,
			int indent3Dvalue, int userWidth, int userHeight) {
		switch (mapElemType) {
		case Oval:// 椭圆
		case OvalSubFlowFigure:
			g2d.fillOval(Math.min(x1, userWidth) + 4, Math.min(y1, userHeight) + 4, Math.abs(x1 - userWidth) - 4 - 4,
					Math.abs(y1 - userHeight) - 4 - 4);
			break;
		case Triangle:// 三角形
		case EndFigure:
		case KeyPointFigure:
			paintTriangle3DPartTop(flowElementData, g2d, x1, x2, x3, y1, y2, y3, indent3Dvalue);
			break;
		case FunctionField:// 四边形
		case FreeText:
		case RoleFigure:
		case CustomFigure:
		case RoleFigureAR:
		case Rect:
		case DateFreeText:
		case PositionFigure:
		case TermFigureAR:
		case VirtualRoleFigure:
			g2d.fillRect(indent3Dvalue, indent3Dvalue, userWidth - indent3Dvalue - indent3Dvalue, userHeight
					- indent3Dvalue - indent3Dvalue);
			break;
		case Pentagon:// 五边形
		case SystemFigure://制度
		case StageSeparatorPentagon://流程图 阶段分隔符五边形
			paintPentagon3DPartTop(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue);
			break;
		case RightHexagon:// 箭头六边形
			paintRightHexagon3DPartTop(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6,
					indent3Dvalue);
			break;
		case InputHandFigure:// 手动输入
			g2d.fillPolygon(new int[] { x1 + 5, x2 - 5, x3 - 5, x4 + 5 }, new int[] { y1 + 5, y2 + 5, y3 - 5, y4 - 5 },
					4);
			break;
		case IsoscelesTrapezoidFigure:// 等腰梯形
			g2d.fillPolygon(
					new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 + indent3Dvalue },
					new int[] { y1 + indent3Dvalue, y2 + indent3Dvalue, y3 - indent3Dvalue, y4 - indent3Dvalue }, 4);
			break;
		default:
			break;
		}
	}

	public void paint3DAndShadowsPartTop(MapElemType mapElemType, JecnAbstractFlowElementData flowElementData,
			Graphics2D g2d, int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5,
			int y6, int indent3Dvalue, int userWidth, int userHeight) {
		switch (mapElemType) {
		case Oval:// 椭圆
		case OvalSubFlowFigure:
			g2d.fillOval(Math.min(x1, userWidth) + 4, Math.min(y1, userHeight) + 4,
					Math.abs(x1 - userWidth) - 4 - 4 - 4, Math.abs(y1 - userHeight) - 4 - 4 - 4);
			break;
		case Triangle:// 三角形
		case EndFigure:
		case KeyPointFigure:
			paintTriangle3DAndShadowsPartTop(flowElementData, g2d, x1, x2, x3, y1, y2, y3, indent3Dvalue);
			break;
		case FunctionField:// 四边形
		case FreeText:
		case RoleFigure:
		case CustomFigure:
		case RoleFigureAR:
		case Rect:
		case DateFreeText:
		case PositionFigure:
		case TermFigureAR:
		case VirtualRoleFigure:
			g2d.fillRect(indent3Dvalue, indent3Dvalue, userWidth - 3 * indent3Dvalue, userHeight - 3 * indent3Dvalue);
			break;
		case Pentagon:// 五边形
		case SystemFigure://制度
		case StageSeparatorPentagon://流程图 阶段分隔符五边形
			paintPentagon3DAndShadowsPartTop(flowElementData, g2d, x1, x2, x3, x4, x5,  y1, y2, y3, y4, y5, 
					indent3Dvalue);
			break;
		case RightHexagon:// 箭头六边形
		case InterfaceRightHexagon://接口六边形
			paintRightHexagon3DAndShadowsPartTop(flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6,
					indent3Dvalue);
			break;
		case InputHandFigure:// 手动输入
			g2d.fillPolygon(new int[] { x1 + 5, x2 - 5, x3 - 5, x4 + 5 }, new int[] { y1 + 5, y2 + 5, y3 - 5, y4 - 5 },
					4);
			break;
		case IsoscelesTrapezoidFigure:// 等腰梯形
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - 2 * indent3Dvalue, x3 - 2 * indent3Dvalue,
					x4 + indent3Dvalue }, new int[] { y1 + indent3Dvalue, y2 + indent3Dvalue, y3 - 2 * indent3Dvalue,
					y4 - 2 * indent3Dvalue }, 4);
			break;
		default:
			break;
		}
	}

	private void paintTriangle3DAndShadowsPartTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1,
			int x2, int x3, int y1, int y2, int y3, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1, x2 - 2 * indent3Dvalue, x3 + indent3Dvalue }, new int[] {
					y1 + indent3Dvalue, y2 - 2 * indent3Dvalue, y3 - 2 * indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - 2 * indent3Dvalue, x2 + indent3Dvalue, x3 + indent3Dvalue }, new int[] {
					y1 - indent3Dvalue / 2, y2 - 2 * indent3Dvalue, y3 + indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2, x2 + indent3Dvalue, x3 - 2 * indent3Dvalue },
					new int[] { y1 - 2 * indent3Dvalue, y2 + indent3Dvalue, y3 + indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - 2 * indent3Dvalue, x3 - 2 * indent3Dvalue },
					new int[] { y1 - indent3Dvalue / 2, y2 + indent3Dvalue, y3 - 2 * indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	private void paintRightHexagon3DAndShadowsPartTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d,
			int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6,
			int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + 2 * indent3Dvalue, x2 - indent3Dvalue, x3 - 2 * indent3Dvalue,
					x4 - indent3Dvalue, x5 + 2 * indent3Dvalue, x6 + indent3Dvalue / 2 }, new int[] {
					y1 + indent3Dvalue, y2 + indent3Dvalue, y3 - indent3Dvalue / 2, y4 - 2 * indent3Dvalue,
					y5 - 2 * indent3Dvalue, y6 }, 6);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			// 顶层 原图，榜值均为零
			g2d
					.fillPolygon(new int[] { x1 + indent3Dvalue, x2, x3 - 2 * indent3Dvalue, x4 - 2 * indent3Dvalue,
							x5, x6 + indent3Dvalue }, new int[] { y1 + 2 * indent3Dvalue, y2 + indent3Dvalue,
							y3 + 2 * indent3Dvalue, y4 - 2 * indent3Dvalue, y5 - 2 * indent3Dvalue,
							y6 - 2 * indent3Dvalue }, 6);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue / 2, x2 - 2 * indent3Dvalue, x3 - 2 * indent3Dvalue,
					x4 - 2 * indent3Dvalue, x5 + indent3Dvalue / 2, x6 + indent3Dvalue }, new int[] {
					y1 + indent3Dvalue, y2 + indent3Dvalue, y3 - indent3Dvalue / 2, y4 - 2 * indent3Dvalue,
					y5 - 2 * indent3Dvalue, y6 - indent3Dvalue / 2 }, 6);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2, x3 - 2 * indent3Dvalue, x4 - 2 * indent3Dvalue, x5,
					x6 + indent3Dvalue }, new int[] { y1 + indent3Dvalue, y2 + indent3Dvalue, y3 + indent3Dvalue,
					y4 - 2 * indent3Dvalue, y5 - 2 * indent3Dvalue, y6 - 2 * indent3Dvalue }, 6);
		}
	}

	private void paintPentagon3DAndShadowsPartTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1,
			int x2, int x3, int x4, int x5, int y1, int y2, int y3, int y4, int y5,  int doubleLineWidth) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + doubleLineWidth, x2 - 3 - 2, x3 - 5 - doubleLineWidth, x4 - 3 - 2,
					x5 + doubleLineWidth }, new int[] { y1 + doubleLineWidth, y2 + doubleLineWidth, y3 - 3,
					y4 - 5 - doubleLineWidth, y5 - 5 - doubleLineWidth }, 5);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + doubleLineWidth, x2 - 3 - doubleLineWidth, x3 - 5 - doubleLineWidth,
					x4 - 3, x5 + doubleLineWidth }, new int[] { y1 + doubleLineWidth, y2 + doubleLineWidth, y3 - 3 - 2,
					y4 - 5 - doubleLineWidth, y5 - 3 - 2 }, 5);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + 3 + doubleLineWidth, x2 + 2, x3 - 4 - doubleLineWidth,
					x4 - 4 - doubleLineWidth, x5 + 2 }, new int[] { y1 - 3, y2 + doubleLineWidth, y3 + doubleLineWidth,
					y4 - 4 - doubleLineWidth, y5 - 4 - doubleLineWidth }, 5);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 - 3, x2 - 5 - doubleLineWidth, x3 - 5 - doubleLineWidth,
					x4 + doubleLineWidth, x5 + doubleLineWidth }, new int[] { y1 + doubleLineWidth, y2 + 2,
					y3 - 3 - doubleLineWidth, y4 - 3 - doubleLineWidth, y5 + 2 }, 5);
		}
	}

	private void paintPentagon3DPartTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int doubleLineWidth) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + doubleLineWidth, x2 - 2, x3 - doubleLineWidth, x4 - 2,
					x5 + doubleLineWidth }, new int[] { y1 + doubleLineWidth, y2 + doubleLineWidth, y3,
					y4 - doubleLineWidth, y5 - doubleLineWidth }, 5);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + doubleLineWidth, x2 - doubleLineWidth, x3 - doubleLineWidth, x4,
					x5 + doubleLineWidth }, new int[] { y1 + doubleLineWidth, y2 + doubleLineWidth, y3 - 2,
					y4 - doubleLineWidth, y5 - 2 }, 5);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + doubleLineWidth, x2 + 2, x3 - doubleLineWidth, x4 - doubleLineWidth,
					x5 + 2 }, new int[] { y1, y2 + doubleLineWidth, y3 + doubleLineWidth, y4 - doubleLineWidth,
					y5 - doubleLineWidth }, 5);

		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - doubleLineWidth, x3 - doubleLineWidth, x4 + doubleLineWidth,
					x5 + doubleLineWidth }, new int[] { y1 + doubleLineWidth, y2 + 2, y3 - doubleLineWidth,
					y4 - doubleLineWidth, y5 + 2 }, 5);
		}
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paintPentagon3DInputHandLine(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1,
			int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6,
			int indent3Dvaluemin, int indent3Dvaluemmax) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemmax, x4 - indent3Dvaluemin, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemmax, y5 - indent3Dvaluemmax }, 5);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemmax, x4 - indent3Dvaluemin, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemmax, y5 - indent3Dvaluemin }, 5);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawPolygon(
					new int[] { x1 + indent3Dvaluemin, x2, x3 - indent3Dvaluemmax, x4 - indent3Dvaluemmax, x5 },
					new int[] { y1 - indent3Dvaluemin, y2, y3, y4 - indent3Dvaluemmax, y5 - indent3Dvaluemmax }, 5);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawPolygon(
					new int[] { x1 - indent3Dvaluemin, x2 - indent3Dvaluemmax, x3 - indent3Dvaluemmax, x4, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemin, y5 }, 5);
		}
	}

	private void paintPentagon3DHandLow(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int indent3Dvaluemin,
			int indent3Dvaluemax) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemax, x4 - indent3Dvaluemin, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemax, y5 - indent3Dvaluemax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemax, x4 - indent3Dvaluemin, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemax, y5 - indent3Dvaluemin }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvaluemin, x2, x3 - indent3Dvaluemax, x4 - indent3Dvaluemax, x5 },
					new int[] { y1 - indent3Dvaluemin, y2, y3, y4 - indent3Dvaluemax, y5 - indent3Dvaluemax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvaluemin, x2 - indent3Dvaluemax, x3 - indent3Dvaluemax, x4, x5 },
					new int[] { y1, y2, y3 - indent3Dvaluemin, y4 - indent3Dvaluemin, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	protected void paintRectTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2, int x3,
			int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int shadowCountmin,
			int shadowCountmax, int userWidth, int userHeight) {
		// 顶层 原图，榜值均为零
		g2d.fillRect(0, 0, userWidth - shadowCountmin, userHeight - shadowCountmin);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRect(0, 0, userWidth - shadowCountmin, userHeight - shadowCountmin);
	}

	private void paintPentagonHandTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int shadowCountmin,
			int shadowCountmax) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmax, x4 - shadowCountmin, x5 },
					new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmax, y5 - shadowCountmax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmax, x4 - shadowCountmin, x5 },
					new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmax, y5 - shadowCountmax }, 5);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmax, x4 - shadowCountmin, x5 },
					new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmax, y5 - shadowCountmin }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmax, x4 - shadowCountmin, x5 },
					new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmax, y5 - shadowCountmin }, 5);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCountmin, x2, x3 - shadowCountmax, x4 - shadowCountmax, x5 },
					new int[] { y1 - shadowCountmin, y2, y3, y4 - shadowCountmax, y5 - shadowCountmax }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 + shadowCountmin, x2, x3 - shadowCountmax, x4 - shadowCountmax, x5 },
					new int[] { y1 - shadowCountmin, y2, y3, y4 - shadowCountmax, y5 - shadowCountmax }, 5);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCountmin, x2 - shadowCountmax, x3 - shadowCountmax, x4, x5 },
					new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmin, y5 }, 5);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCountmin, x2 - shadowCountmax, x3 - shadowCountmax, x4, x5 },
					new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmin, y5 }, 5);
		}
	}

	private void paintRightHexagonTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int shadowCountmin) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5,
					x6 - shadowCountmin }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin,
					y6 - shadowCountmin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5,
					x6 - shadowCountmin }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin,
					y6 - shadowCountmin }, 6);

		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5, x6 },
					new int[] { y1, y2 - shadowCountmin, y3, y4 - shadowCountmin, y5 - shadowCountmin,
							y6 - shadowCountmin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5, x6 },
					new int[] { y1, y2 - shadowCountmin, y3, y4 - shadowCountmin, y5 - shadowCountmin,
							y6 - shadowCountmin }, 6);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmin,
					y5 - shadowCountmin, y6 - shadowCountmin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmin,
					y5 - shadowCountmin, y6 - shadowCountmin }, 6);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin, y6 },
					6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin, y6 },
					6);

		}
	}

	protected void paintTriangleTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int y1, int y2, int y3, int shadowCount) {
		// 顶层 原图，榜值均为零
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount / 2, x2, x3 }, new int[] { y1 - shadowCount / 2,
					y2 - shadowCount, y3 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount / 2, x2, x3 }, new int[] { y1 - shadowCount / 2,
					y2 - shadowCount, y3 }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount }, new int[] { y1 - shadowCount / 2, y2,
					y3 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount }, new int[] { y1 - shadowCount / 2, y2,
					y3 - shadowCount }, 3);
		}
	}

	/**
	 * 3D图形底层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	protected void paint3DTriangleLow(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int y1, int y2, int y3, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2, x2 - indent3Dvalue, x3 }, new int[] { y1,
					y2 - indent3Dvalue, y3 - indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue, x2, x3 }, new int[] { y1 - indent3Dvalue / 2,
					y2 - indent3Dvalue, y3 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2, x2, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue, y2, y3 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue / 2, y2, y3 - indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	private void paint3DRightHexagonLow(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int indent3Dvaluemin) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin, x5,
					x6 - indent3Dvaluemin }, new int[] { y1, y2, y3 - indent3Dvaluemin / 2, y4 - indent3Dvaluemin,
					y5 - indent3Dvaluemin, y6 - indent3Dvaluemin / 2 }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemin / 2, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin,
					x5 - indent3Dvaluemin / 2, x6 }, new int[] { y1, y2, y3, y4 - indent3Dvaluemin,
					y5 - indent3Dvaluemin, y6 - indent3Dvaluemin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin,
					x5 - indent3Dvaluemin, x6 }, new int[] { y1, y2, y3 - indent3Dvaluemin / 2, y4 - indent3Dvaluemin,
					y5 - indent3Dvaluemin, y6 - indent3Dvaluemin / 2 }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvaluemin / 2, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin,
					x5 - indent3Dvaluemin / 2, x6 }, new int[] { y1, y2, y3, y4 - indent3Dvaluemin,
					y5 - indent3Dvaluemin, y6 - indent3Dvaluemin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	protected void paint3DTriangleLine(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue / 2, x2 - indent3Dvalue, x3 }, new int[] { y1,
					y2 - indent3Dvalue, y3 - indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue, x2, x3 }, new int[] { y1 - indent3Dvalue / 2,
					y2 - indent3Dvalue, y3 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue / 2, x2, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue, y2, y3 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue / 2, y2, y3 - indent3Dvalue }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paintRightHexagon3DInputHandLine(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int indent3Dvaluemin) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin, x5,
					x6 - indent3Dvaluemin }, new int[] { y1, y2, y3 - indent3Dvaluemin / 2, y4 - indent3Dvaluemin,
					y5 - indent3Dvaluemin, y6 - indent3Dvaluemin / 2 }, 6);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvaluemin / 2, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin,
					x5 - indent3Dvaluemin / 2, x6 }, new int[] { y1, y2, y3, y4 - indent3Dvaluemin,
					y5 - indent3Dvaluemin, y6 - indent3Dvaluemin }, 6);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawPolygon(
					new int[] { x1, x2 - indent3Dvaluemin, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin, x5, x6 },
					new int[] { y1, y2, y3 - indent3Dvaluemin / 2, y4 - indent3Dvaluemin, y5 - indent3Dvaluemin,
							y6 - indent3Dvaluemin / 2 }, 6);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvaluemin / 2, x3 - indent3Dvaluemin, x4 - indent3Dvaluemin,
					x5 - indent3Dvaluemin / 2, x6 }, new int[] { y1, y2, y3, y4 - indent3Dvaluemin,
					y5 - indent3Dvaluemin, y6 - indent3Dvaluemin }, 6);
		}
	}

	private void paintRightHexagon3DPartTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1,
			int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + 2 * indent3Dvalue, x2 - indent3Dvalue / 2, x3 - indent3Dvalue,
					x4 - indent3Dvalue / 2, x5 + 2 * indent3Dvalue, x6 + indent3Dvalue }, new int[] {
					y1 + indent3Dvalue, y2 + indent3Dvalue, y3, y4 - indent3Dvalue, y5 - indent3Dvalue, y6 }, 6);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2, x3 - indent3Dvalue, x4 - indent3Dvalue, x5,
					x6 + indent3Dvalue }, new int[] { y1 + 2 * indent3Dvalue, y2 + indent3Dvalue,
					y3 + 2 * indent3Dvalue, y4 - indent3Dvalue, y5 - indent3Dvalue, y6 - indent3Dvalue }, 6);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 - indent3Dvalue,
					x5 + indent3Dvalue, x6 + indent3Dvalue }, new int[] { y1 + indent3Dvalue, y2 + indent3Dvalue, y3,
					y4 - indent3Dvalue, y5 - indent3Dvalue, y6 }, 6);

		} else if (flowElementData.getCurrentAngle() == 270.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2, x3 - indent3Dvalue, x4 - indent3Dvalue, x5,
					x6 + indent3Dvalue }, new int[] { y1 + indent3Dvalue, y2 + indent3Dvalue, y3 + indent3Dvalue,
					y4 - indent3Dvalue, y5 - indent3Dvalue, y6 - indent3Dvalue }, 6);
		}
	}

	private void paintTriangle3DPartTop(JecnAbstractFlowElementData flowElementData, Graphics2D g2d, int x1, int x2,
			int x3, int y1, int y2, int y3, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 + indent3Dvalue }, new int[] {
					y1 + 2 * indent3Dvalue, y2 - indent3Dvalue, y3 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue, x2 + indent3Dvalue, x3 + indent3Dvalue }, new int[] { y1,
					y2 - indent3Dvalue, y3 + indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2 + indent3Dvalue, x3 - indent3Dvalue }, new int[] { y1 - indent3Dvalue,
					y2 + indent3Dvalue, y3 + indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue, x3 - indent3Dvalue }, new int[] { y1,
					y2 + indent3Dvalue, y3 - indent3Dvalue }, 3);
		}
	}
}
