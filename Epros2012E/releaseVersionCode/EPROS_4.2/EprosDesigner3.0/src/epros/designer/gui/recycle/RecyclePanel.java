package epros.designer.gui.recycle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneLayout;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.table.JecnRecyleTable;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.swing.textfield.search.JecnSearchPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

public abstract class RecyclePanel extends JecnPanel {

	private static Logger log = Logger.getLogger(RecyclePanel.class);
	/*** 树滚动面板 在左侧 */
	private JScrollPane treeLeftPanel = new JScrollPane();
	/** 树 */
	/*** 搜索框等在右侧 */
	private RecycleTree recycleTree = null;
	private JecnPanel rightPanel = new JecnPanel();;
	/** table滚动面板 存放table信息 */
	private JScrollPane tablePanel = new JScrollPane();
	/*** 搜索Panel */
	protected JecnSearchPanel searchPanel = new JecnSearchPanel();
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();
	/** splitPanel 左侧为树面板 右侧为内容面板 */
	protected JecnSplitPane splitPanel = null;
	/** table数据 */
	protected JecnRecyleTable jecnRecyleTable = null;
	/** 删除按钮 */
	private JButton delButton = new JButton(JecnProperties
			.getValue("deleteBtn"));
	/** 定位按钮 */
	private JButton orientateButton = new JButton(JecnProperties
			.getValue("fixedPosition"));

	public JecnRecyleTable getJecnRecyleTable() {
		return jecnRecyleTable;
	}

	public void setJecnRecyleTable(JecnRecyleTable jecnRecyleTable) {
		this.jecnRecyleTable = jecnRecyleTable;
	}

	/** 复选框 */
	private JCheckBox selectAllBox = new JCheckBox(JecnProperties
			.getValue("selectAll"));;

	public JCheckBox getSelectAllBox() {
		return selectAllBox;
	}

	public void setSelectAllBox(JCheckBox selectAllBox) {
		this.selectAllBox = selectAllBox;
	}

	public RecyclePanel() {
		initCompotents();
		initLayout();
	}

	/**
	 * 树面板的border名称
	 * 
	 * @return
	 */
	public abstract String getTreeBorderName();

	private void initCompotents() {

		treeLeftPanel.setMinimumSize(new Dimension(200, 400));
		// 初始化树
		recycleTree = initTree();
		treeLeftPanel.setViewportView(recycleTree);
		treeLeftPanel.setBorder(BorderFactory
				.createTitledBorder(getTreeBorderName()));

		treeLeftPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		rightPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		tablePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		searchPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 添加搜索监听
		searchPanel.addButtonActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchButPerformed();
				searchPanel.textFiledRequestFocusInWindow();
			}
		});

		// 透明
		selectAllBox.setOpaque(false);

		selectAllBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				selectAllBoxAction();

			}
		});
		// 删除
		delButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				delButton();

			}
		});
		// 定位
		orientateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				orientateButtonBAction();

			}
		});

	}

	/**
	 * 定位
	 */
	public void orientateButtonBAction() {
		int selectRow = this.jecnRecyleTable.getSelectedRow();
		if (selectRow != -1) {
			DefaultTableModel defaultTableModel = (DefaultTableModel) jecnRecyleTable
					.getModel();
			if (defaultTableModel.getValueAt(selectRow, 3) != null
					&& !"".equals(defaultTableModel.getValueAt(selectRow, 3)
							.toString())
					&& defaultTableModel.getValueAt(selectRow, 5) != null
					&& !"".equals(defaultTableModel.getValueAt(selectRow, 5)
							.toString())) {
				Long id = Long.valueOf(defaultTableModel.getValueAt(selectRow,
						3).toString());
				TreeNodeType type = this.getType(defaultTableModel.getValueAt(
						selectRow, 5).toString());
				if (type != null) {
					try {
						this.recycleTree.setAllowExpand(false);
						this.searchNode(id, type, this.recycleTree);// 之类实现与数据库交互
						this.recycleTree.setAllowExpand(true);
					} catch (Exception e) {
						log.error("RecyclePanel orientateButtonBAction is error", e);
					}

				}
			}
		}

	}

	/**
	 * @author yxw 2012-8-9
	 * @description:搜索节点
	 * @param id
	 * @param type
	 * @param jTree
	 * @throws Exception
	 */
	private void searchNode(Long id, TreeNodeType type, JTree jTree)
			throws Exception {
		// 节点唯一标示
		String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(
				id, type);
		// 判断节点是不是已经在数节点上
		JecnTreeNode node = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier,
				jTree);
		if (node != null) {
			JecnTreeCommon.selectNode(jTree, node);
			return;
		}
		TreeNodeType rootNodeType = null;
		List<JecnTreeBean> list = null;
		switch (type) {
		case process:
		case processMap:
			list = ConnectionPool.getProcessAction().getPnodesRecy(id,
					JecnConstants.projectId);
			rootNodeType = TreeNodeType.processRoot;
			break;
		case ruleDir:
		case ruleModeFile:
		case ruleFile:
			list = ConnectionPool.getRuleAction().getPnodesRecy(id,
					JecnConstants.projectId);
			rootNodeType = TreeNodeType.ruleRoot;
			break;
		case organization:
			list = ConnectionPool.getOrganizationAction().getPnodesRecy(id,
					JecnConstants.projectId);
			rootNodeType = TreeNodeType.organizationRoot;
			break;
		case standardDir:
		case standard:
		case standardClause:
		case standardClauseRequire:
			break;
		case fileDir:
		case file:
			list = ConnectionPool.getFileAction().getPnodesRecy(id,
					JecnConstants.projectId);
			rootNodeType = TreeNodeType.fileRoot;
			break;
		default:
			break;
		}
		if (list != null && rootNodeType != null) {
			searchRecycleNode(list, id, jTree, rootNodeType, type);
		}
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：搜索节点
	 * @param list
	 * @param jecnTreeBean
	 * @param jTree
	 * @param treeNodeType
	 *            根节点类型
	 */
	private void searchRecycleNode(List<JecnTreeBean> list, Long id,
			JTree jTree, TreeNodeType rootNodeType, TreeNodeType nodeType) {
		// 根节点
		JecnTreeNode searchRootNode = JecnTreeCommon.getSearchRootNode(jTree,
				rootNodeType);
		if (searchRootNode == null) {
			return;
		}
		// 添加搜索节点
		JecnTreeCommon.addChildNodesSearch(searchRootNode, list, jTree);
		// 获得树节点
		JecnTreeNode selectNode = JecnTreeCommon.getTreeNode(JecnTreeCommon
				.getNodeUniqueIdentifier(id, nodeType), jTree);
		if (selectNode != null) {
			// 控制不在去加载
			JecnTreeCommon.selectNode(jTree, selectNode);
		}
	}

	/**
	 *获得选中的枚举类型
	 * 
	 * @param type
	 * @return
	 */
	private TreeNodeType getType(String type) {
		if (!"".equals(type)) {
			// liu
			if (TreeNodeType.processMap.toString().equals(type)) {
				return TreeNodeType.processMap;
			} else if (TreeNodeType.process.toString().equals(type)) {
				return TreeNodeType.process;
			} else if (TreeNodeType.file.toString().equals(type)) {
				return TreeNodeType.file;
			} else if (TreeNodeType.fileDir.toString().equals(type)) {
				return TreeNodeType.fileDir;
			} else if (TreeNodeType.organization.toString().equals(type)) {
				return TreeNodeType.organization;
			}else if (TreeNodeType.ruleDir.toString().equals(type)) {
				return TreeNodeType.ruleDir;
			} else if (TreeNodeType.ruleFile.toString().equals(type)) {
				return TreeNodeType.ruleFile;
			} else if (TreeNodeType.ruleModeFile.toString().equals(type)) {
				return TreeNodeType.ruleModeFile;
			}
		}
		return null;
	}

	public void delButton() {
		List<JecnTreeBean> listSelectIds = this.getSelectTableBeans();
		if (listSelectIds.size() > 0) {
			// 是否恢复
			int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData
					.getFrame(), JecnProperties.getValue("delNoRecovery"),
					null, JecnOptionPane.YES_NO_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return;
			}
			List<Long> listIds = new ArrayList<Long>();
			for (JecnTreeBean jecnTreeBean : listSelectIds) {
				listIds.add(jecnTreeBean.getId());
			}
			/** 删除节点 */
			if (listIds.size() > 0) {
				try {
					delDataAction(listIds);// 子类实现与数据库交互
				} catch (Exception e) {
					log.error("RecyclePanel delButton is error", e);
				}
			}
			for (JecnTreeBean jecnTreeBean : listSelectIds) {
				JecnTreeNode node = JecnTreeCommon.getTreeNode(jecnTreeBean,
						this.recycleTree);
				if (node != null) {
					JecnTreeCommon.removeNode(recycleTree, node);
				}
			}

			// 清除table中删除的数据
			removeTableDate(listSelectIds);

			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(),
					JecnProperties.getValue("deleteSuccess"));
		}
	}

	/**
	 * 清空table面板中被删除的节点以及子节点
	 * 
	 * @param listSelectIds
	 */
	private void removeTableDate(List<JecnTreeBean> listSelectIds) {

		DefaultTableModel model = (DefaultTableModel) this.jecnRecyleTable
				.getModel();
		int rows = model.getRowCount();
		// 选中删除的流程的节点ID及子节点集合
		if (listSelectIds != null && listSelectIds.size() > 0) {
			for (JecnTreeBean bean : listSelectIds) {
				for (int i = rows - 1; i >= 0; i--) {
					if (Long.valueOf(model.getValueAt(i, 3).toString()).equals(
							bean.getId())) {
						((DefaultTableModel) jecnRecyleTable.getModel())
								.removeRow(i);
						rows--;
					}
				}
			}
		} else {
			for (int i = rows - 1; i >= 0; i--) {
				if (((JCheckBox) model.getValueAt(i, 0)).isSelected()) {
					((DefaultTableModel) jecnRecyleTable.getModel())
							.removeRow(i);
				}
			}
		}
		jecnRecyleTable.updateUI();
		selectAllBox.setSelected(false);
		if(jecnRecyleTable.getRowCount()==0){
			selectAllBox.setEnabled(false);
		}
	}

	/**
	 * 删除
	 */
	public abstract void delDataAction(List<Long> listSelectIds)
			throws Exception;

	/**
	 * 获得Table选中行的集合
	 * 
	 * @return
	 */
	public List<JecnTreeBean> getSelectTableBeans() {
		List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
		int row = jecnRecyleTable.getRowCount();
		DefaultTableModel defaultTableModel = (DefaultTableModel) jecnRecyleTable
				.getModel();
		for (int i = 0; i < row; i++) {
			if (((JCheckBox) defaultTableModel.getValueAt(i, 0)).isSelected()) {
				if (defaultTableModel.getValueAt(i, 3) != null
						&& !"".equals(defaultTableModel.getValueAt(i, 3)
								.toString())
						&& defaultTableModel.getValueAt(i, 5) != null
						&& !"".equals(defaultTableModel.getValueAt(i, 5)
								.toString())) {
					JecnTreeBean jecnTreeBean = new JecnTreeBean();
					jecnTreeBean.setId(Long.valueOf(defaultTableModel
							.getValueAt(i, 3).toString()));
					jecnTreeBean.setTreeNodeType(getType(defaultTableModel
							.getValueAt(i, 5).toString()));
					listJecnTreeBean.add(jecnTreeBean);
				}

			}
		}
		return listJecnTreeBean;
	}

	/**
	 * 全选操作
	 */
	private void selectAllBoxAction() {
		if (selectAllBox.isSelected()) {
			int row = jecnRecyleTable.getRowCount();
			DefaultTableModel defaultTableModel = (DefaultTableModel) jecnRecyleTable
					.getModel();
			for (int i = 0; i < row; i++) {
				((JCheckBox) defaultTableModel.getValueAt(i, 0))
						.setSelected(true);
			}
		} else {
			int row = jecnRecyleTable.getRowCount();
			DefaultTableModel defaultTableModel = (DefaultTableModel) jecnRecyleTable
					.getModel();
			for (int i = 0; i < row; i++) {
				((JCheckBox) defaultTableModel.getValueAt(i, 0))
						.setSelected(false);
			}
		}
		jecnRecyleTable.updateUI();
	}

	/**
	 * 在搜索面板 点击确定按钮执行的动作
	 */
	protected void searchButPerformed() {
		String name = searchPanel.getText().trim();
		if (!"".equals(name)) {
			this.jecnRecyleTable.refrehTable(searchByName(name));
		}
	}

	/**
	 * 抽象方法：通过搜索的名称获得list
	 * 
	 * @param name
	 *            搜索的名称
	 * @return
	 */
	public abstract List<JecnTreeBean> searchByName(String name);

	/***
	 * 搜索名称Lab
	 * 
	 * @return
	 */
	public abstract String getSearchName();

	/***
	 * 初始化树节点数据
	 * 
	 * @return
	 */
	public abstract RecycleTree initTree();

	private void initLayout() {
		this.setLayout(new BorderLayout());
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(selectAllBox);
		buttonPanel.add(orientateButton);
		buttonPanel.add(delButton);

		rightPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("quickSearch")));
		rightPanel.setLayout(new GridBagLayout());
		Insets insets = new Insets(5, 5, 5, 5);

		// 搜索框
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		searchPanel.setSearchTitleName(getSearchName());
		rightPanel.add(searchPanel, c);

		// 中间table面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		rightPanel.add(tablePanel, c);

		// table面板添加table
		tablePanel.setOpaque(false);
		tablePanel.setLayout(new ScrollPaneLayout());
		// table初始化
		jecnRecyleTable = new JecnRecyleTable(this);
		tablePanel.setViewportView(jecnRecyleTable);

		// 下边按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		rightPanel.add(buttonPanel, c);

		splitPanel = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, null,
				treeLeftPanel, rightPanel);
		// 左:右=3:7
		splitPanel.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPanel.setResizeWeight(0.0);
		splitPanel.showBothPanel();

		// 将splitPanel面板添加到当前面板
		this.add(splitPanel, BorderLayout.CENTER);
	}

}
