package com.jecn.epros.server.action.designer.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnProcessTemplateBean;
import com.jecn.epros.server.bean.process.JecnProcessTemplateFollowBean;

public interface IProcessTemplateAction {
	/**
	 * @author yxw 2012-8-10
	 * @description:
	 * @param processTemplateBean
	 *            如果有主键 则修改主表，如果没有主键则保存主表
	 * @param processTemplateFollowBean
	 * @return
	 * @throws Exception
	 */
	public JecnProcessTemplateBean saveTemplate(JecnProcessTemplateBean processTemplateBean,
			JecnProcessTemplateFollowBean processTemplateFollowBean) throws Exception;

	/**
	 * @author yxw 2012-8-10
	 * @description:
	 * @param processTemplateBean
	 * @param id
	 *            模型从表ID
	 * @throws Exception
	 */
	public void deleteTemplate(JecnProcessTemplateBean processTemplateBean, long id) throws Exception;

	/**
	 * @author yxw 2012-8-10
	 * @description:
	 * @param id
	 *            从表主键ID
	 * @param name
	 *            名称
	 * @return
	 * @throws Exception
	 */
	public void updateTemplate(long id, String name, Long peopleId) throws Exception;

	/**
	 * @author zhangchen Aug 10, 2012
	 * @description：查询模板数据
	 * @return
	 * @throws Exception
	 */
	public List<JecnProcessTemplateBean> getJecnProcessTemplateBeanList() throws Exception;
}
