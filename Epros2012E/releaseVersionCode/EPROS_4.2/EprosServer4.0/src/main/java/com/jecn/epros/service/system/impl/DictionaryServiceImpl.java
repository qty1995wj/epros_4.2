package com.jecn.epros.service.system.impl;


import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.domain.system.DictionaryManage;
import com.jecn.epros.mapper.DictionaryMapper;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.service.system.DictionaryService;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.DictionaryEnum;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@Transactional
public class DictionaryServiceImpl implements DictionaryService {

    @Autowired
    private DictionaryMapper dictionaryMapper;

    @Override
    public Map<DictionaryEnum, List<Dictionary>> fetchAllDictionary() {
        List<Dictionary> dictionaries = dictionaryMapper.fetchAllDictionary();
        return dictionaries.stream().collect(Collectors.groupingBy(Dictionary::getName));
    }

    @Override
    public List<String> getDictionaryItems() {
        return dictionaryMapper.getDictionaryItem();
    }

    @Override
    public List<Dictionary> getDictionaryAll() {
        return dictionaryMapper.fetchDictionary();
    }

    @Override
    public int saveDictionary(DictionaryManage dm) {
        int s = 0;
        //先循环更新旧的数据
        for (Dictionary d : dm.getOldData()) {
            int u = dictionaryMapper.updateDictionaryItem(d.toMap());
            s += u;
        }
        //再新增数据
        for (Dictionary d : dm.getNewData()) {
            Map m = d.toMap();
            m.put("id", UUID.randomUUID().toString());
            int u = dictionaryMapper.insertDictionaryItem(m);
            s += u;
        }
        return s;
    }

    @Override
    public List<Dictionary> getDictionaryByName(DictionaryEnum e) {
        Map<String, Object> doMap = new HashedMap();
        doMap.put("NAME", e);
        List<Dictionary> dics = dictionaryMapper.getDictionaryItemByName(doMap);
        return dics;
    }

}
