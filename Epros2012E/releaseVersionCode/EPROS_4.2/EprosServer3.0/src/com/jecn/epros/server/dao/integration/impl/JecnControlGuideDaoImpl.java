package com.jecn.epros.server.dao.integration.impl;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.integration.IJecnControlGuideDao;

/**
 * 内控指引知识库Dao实现类
 * @author Administrator
 *
 */
public class JecnControlGuideDaoImpl extends AbsBaseDao<JecnControlGuide, Long> 
		implements IJecnControlGuideDao {
	@Override
	public void deleteControlGuide(Set<Long> setIds) throws Exception {
		String sql="delete from JECN_RISK_GUIDE where guide_id in ";
		List<String> listStr = JecnCommonSql.getSetSqls(sql, setIds);
			for (String str : listStr) {
			this.execteNative(str);
		};
		String hql = "delete from JecnControlGuide where id in ";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
	}

}
