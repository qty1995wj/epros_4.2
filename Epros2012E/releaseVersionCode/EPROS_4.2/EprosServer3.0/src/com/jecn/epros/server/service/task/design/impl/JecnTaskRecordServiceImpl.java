package com.jecn.epros.server.service.task.design.impl;

import java.io.InputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.sql.BLOB;

import org.apache.log4j.Logger;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekSyncUserEnum;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.JecnTaskTestRunFile;
import com.jecn.epros.server.bean.task.PrfRelatedTemplet;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.task.TaskItemRelatedPeople;
import com.jecn.epros.server.bean.task.TaskTemplet;
import com.jecn.epros.server.bean.task.temp.JecnTaskResubmitDesignerBean;
import com.jecn.epros.server.bean.task.temp.JecnTaskStateBean;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.task.IJecnAbstractTaskDao;
import com.jecn.epros.server.dao.task.design.IJecnTaskRecordDao;
import com.jecn.epros.server.service.task.design.IJecnTaskRecordService;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * 任务审批信息验证处理类实现
 * 
 * @author Administrator
 * @date： 日期：Oct 24, 2012 时间：10:40:17 AM
 */
@Transactional(rollbackFor = Exception.class)
public class JecnTaskRecordServiceImpl implements IJecnTaskRecordService {
	private Logger log = Logger.getLogger(JecnTaskRecordServiceImpl.class);
	private IJecnTaskRecordDao taskRecordDao;
	private IJecnConfigItemDao configDao = null;

	private IJecnAbstractTaskDao abstractTaskDao = null;

	/**
	 * 获得人员名称和人员主键记录的集合信息
	 * 
	 * @param set
	 *            人员主键集合
	 * @param projectId
	 *            当前项目ID
	 * @return 人员名称和人员主键记录的集合信息
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getJecnUserByUserIds(Set<Long> set, Long protectId) throws Exception {
		return taskRecordDao.getJecnUserByUserIds(set, protectId);
	}

	/**
	 * 判断审批人是否为相同人员
	 * 
	 * @param userIds
	 *            人员岗位关联表主键ID集合（集合大于100 不能用此方法）
	 * @return true 存在相同人员
	 * @throws Exception
	 */
	public boolean checkAuditPeopleIsSame(Set<Long> userIds) throws Exception {
		int userCount = taskRecordDao.getPeopleIdCountByUserIds(userIds);
		// 存在相同人员
		if (userCount != userIds.size()) {
			return true;
		}
		return false;
	}

	public IJecnTaskRecordDao getTaskRecordDao() {
		return taskRecordDao;
	}

	public void setTaskRecordDao(IJecnTaskRecordDao taskRecordDao) {
		this.taskRecordDao = taskRecordDao;
	}

	/**
	 * 验证版本号是否存在
	 * 
	 * @param id
	 *            流程或制度ID
	 * @param version
	 *            版本号
	 * @param type
	 *            类型 0 流程，1文件，2制度
	 * @return true 版本好存在
	 * @throws DaoException
	 */
	public boolean isExistVersionbyFlowId(Long id, String version, int type) throws Exception {
		List<Long> list = taskRecordDao.isExistVersionbyFlowId(id, version, type);
		if (list.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 根据树节点类型返回对应的int值
	 * 
	 * @param nodeType
	 *            树节点类型枚举
	 * @return
	 */
	private int getNoteType(TreeNodeType nodeType) {
		int type = 0;
		if (nodeType == null) {
			return type;
		}
		switch (nodeType) {
		case process:// 流程
			type = 0;
			break;
		case processMap:// 流程地图
		case processMapRelation:
			type = 4;
			break;
		case ruleModeFile:// 制度模板文件
			type = 2;
			break;
		case ruleFile:// 制度模板文件
			type = 3;
			break;
		case file:// 制度模板文件
		case fileDir:// 文件目录
			type = 1;
			break;
		default:
			break;
		}
		return type;
	}

	@Override
	public boolean isEdit(Long rid, TreeNodeType nodeType) throws Exception {
		int type = this.getNoteType(nodeType);
		// 自己的任务
		int count = taskRecordDao.isBeInTask(rid, type);
		if (count > 0) {
			return false;
		}
		return true;
	}

	/**
	 * 是否审批 只有在拟稿人状态下或整理意见（设计者有节点权限才能打开）
	 * 
	 * @param rid
	 *            关联ID
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean isInTask(Long rid, TreeNodeType nodeType) throws Exception {
		// 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		int type = this.getNoteType(nodeType);
		String sql = "select count(t.id) from JECN_TASK_BEAN_NEW t where t.R_ID=? "
				+ "and t.TASK_TYPE=? and (t.state<>5 and t.state<>0 and t.state<>10 and t.edit<>1) and t.is_lock=1";
		int count = taskRecordDao.countAllByParamsNativeSql(sql, rid, type);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 是否审批
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean idsIsInTask(List<Long> rids, TreeNodeType nodeType) throws Exception {
		// 判断节点存在处于任务中的
		String sql = JecnDaoUtil.getListRefIdsInTask(rids, nodeType);
		if ("".equals(sql)) {
			return false;
		}
		int count = taskRecordDao.countAllByParamsNativeSql(sql);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 获取试运行报告
	 * 
	 * @param id
	 *            试运行主键ID
	 * @return JecnTaskTestRunFile 试运行对应的文件
	 * @throws Exception
	 */
	@Override
	public JecnTaskTestRunFile getJecnTaskTestRunFile(Long id) throws Exception {
		String hql = "from JecnTaskTestRunFile where id=?";
		List<JecnTaskTestRunFile> list = this.taskRecordDao.listHql(hql, id);
		if (list.size() == 0 || list.get(0) == null) {
			return null;
		}
		JecnTaskTestRunFile jecnTaskTestRunFile = list.get(0);
		SerializableBlob sb = (SerializableBlob) jecnTaskTestRunFile.getFileStream();
		Blob wrapBlob = sb.getWrappedBlob();
		byte[] content = new byte[(int) wrapBlob.length()];
		InputStream in = null;
		try {
			if (JecnContants.dbType == DBType.SQLSERVER) {
				in = wrapBlob.getBinaryStream();
			} else if (JecnContants.dbType == DBType.ORACLE) {
				BLOB blob = (BLOB) wrapBlob;
				in = blob.getBinaryStream();
			}
			in.read(content);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
					log.error("", e);
				}
			}
		}
		jecnTaskTestRunFile.setBytes(content);
		return jecnTaskTestRunFile;
	}

	/**
	 * 验证人员是否存在岗位
	 * 
	 * @param peopleid
	 *            人员主键ID
	 * @return true ：存在岗位
	 * @throws Exception
	 */
	@Override
	public int isExsitPos(Long peopleid) throws Exception {
		String sql = "select count(*) from jecn_user_position_related where people_id=?";
		return taskRecordDao.countAllByParamsNativeSql(sql, peopleid);
	}

	/**
	 * 提交审批验证
	 * 
	 * @param rid
	 *            关联ID
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean submitToTask(Long rid, TreeNodeType nodeType) throws Exception {
		// 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		int type = this.getNoteType(nodeType);
		String sql = "select count(t.id) from JECN_TASK_BEAN_NEW t where t.R_ID=? and t.TASK_TYPE=? and t.state<>5  and t.is_lock=1";
		int count = taskRecordDao.countAllByParamsNativeSql(sql, rid, type);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 是否是重新提交
	 * 
	 * @param rid
	 * @param nodeType
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean reSubmitToTask(Long rid, TreeNodeType nodeType) throws Exception {
		// 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		int type = this.getNoteType(nodeType);
		String sql = "select count(t.id) from JECN_TASK_BEAN_NEW t where t.R_ID=? and t.TASK_TYPE=? AND t.is_lock=1 AND T.TASK_ELSE_STATE = 4";
		int count = taskRecordDao.countAllByParamsNativeSql(sql, rid, type);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 是否已发布
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点是已发布状态
	 * @throws Exception
	 */
	@Override
	public boolean idsIsPub(List<Long> rids, TreeNodeType nodeType) throws Exception {
		// 判断节点是已发布状态
		String sql = JecnDaoUtil.getSqlExistPub(rids, nodeType);
		int count = taskRecordDao.countAllByParamsNativeSql(sql);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 文件删除审批验证*********************** 制度文件引用文件管理中的文件是否审批
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	@Override
	public boolean idsIsInRuleTask(List<Long> rids, TreeNodeType nodeType) throws Exception {
		// 判断节点存在处于任务中的
		String sql = JecnDaoUtil.getListRefIdsInRuleTask(rids, nodeType);
		if ("".equals(sql)) {
			return false;
		}
		int count = taskRecordDao.countAllByParamsNativeSql(sql);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 文件树节点在制度文件中是否已发布（文件、制度文件）
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点是已发布状态
	 * @throws Exception
	 */
	@Override
	public boolean idsIsRulePubByFile(List<Long> rids, TreeNodeType nodeType) throws Exception {
		// 判断节点是已发布状态
		String sql = JecnDaoUtil.getSqlExistRulePub(rids, nodeType);
		int count = taskRecordDao.countAllByParamsNativeSql(sql);
		if (count > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<TaskTemplet> listTaskTempletsByType(int taskType) throws Exception {
		String hql = "from TaskTemplet where type=? order by createTime asc";
		List<TaskTemplet> templets = taskRecordDao.listHql(hql, taskType);
		return templets;
	}

	@Override
	public List<TaskConfigItem> listTaskConfigItemByTempletId(String templetId) throws Exception {

		String hql = "from TaskConfigItem where templetId=? order by sort asc";
		List<TaskConfigItem> configItems = taskRecordDao.listHql(hql, templetId);
		Map<String, TaskConfigItem> map = new HashMap<String, TaskConfigItem>();
		for (TaskConfigItem item : configItems) {
			map.put(item.getId(), item);
		}
		String sql = " SELECT TCI.ID, JU.PEOPLE_ID, JU.TRUE_NAME, JU.LOGIN_NAME"
				+ "  FROM TASK_CONFIG_ITEM TCI, TASK_ITEM_RELATED_PEOPLE TIRP, JECN_USER JU"
				+ " WHERE TCI.TEMPLET_ID = ?" + "   AND TIRP.CONFIG_ID = TCI.ID"
				+ "   AND TIRP.PEOPLE_ID = JU.PEOPLE_ID";

		List<Object[]> objs = taskRecordDao.listNativeSql(sql, templetId);

		List<String> loginNames = new ArrayList<String>();
		for (Object[] obj : objs) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
				continue;
			}
			String id = obj[0].toString();
			TaskConfigItem taskConfigItem = map.get(id);
			if (taskConfigItem != null) {
				JecnUser user = new JecnUser();
				user.setPeopleId(Long.valueOf(obj[1].toString()));
				// 科大讯飞需要根据登录名重新查询真实名称
				user.setTrueName(obj[2].toString());
				user.setLoginName(obj[3].toString());
				taskConfigItem.getUsers().add(user);
				loginNames.add(user.getLoginName());
			}
		}
		if (JecnConfigTool.isIflytekLogin()) {
			Map<String, String> result = JecnIflytekSyncUserEnum.INSTANCE.getMapUserName(loginNames);
			for (TaskConfigItem configItem : configItems) {
				for (JecnUser user : configItem.getUsers()) {
					user.setTrueName(result.get(user.getLoginName()) == null ? "" : result.get(user.getLoginName()));
				}
			}
		}
		return configItems;
	}

	@Override
	public TaskTemplet saveTaskTemplet(TaskTemplet taskTemplet) throws Exception {

		taskTemplet.setCreateTime(new Date());

		taskRecordDao.getSession().save(taskTemplet);
		// 需要默认插入几个阶段
		String id = taskTemplet.getId();
		if (id == null) {
			throw new NullPointerException();
		}

		List<TaskConfigItem> items = createConfigItems(id);

		for (TaskConfigItem item : items) {
			taskRecordDao.getSession().save(item);
		}

		return taskTemplet;
	}

	private List<TaskConfigItem> createConfigItems(String id) {
		List<TaskConfigItem> configs = new ArrayList<TaskConfigItem>();
		// 目前配置废弃，暂时不区分配置
		List<JecnConfigItemBean> configItemBeans = configDao.selectConfigItemBeanByType(1, 0);
		TaskConfigItem item = null;
		for (JecnConfigItemBean configItemBean : configItemBeans) {
			item = new TaskConfigItem();
			item.setMark(configItemBean.getMark());
			item.setName(configItemBean.getName());
			item.setDefaultName(configItemBean.getDefaultName());
			item.setSort(configItemBean.getSort() == null ? 0 : configItemBean.getSort());
			item.setDefaultSort(configItemBean.getDefaultSort() == null ? 0 : configItemBean.getDefaultSort());
			item.setIsEmpty(configItemBean.getIsEmpty() == null ? 0 : configItemBean.getIsEmpty());
			item.setIsDefaultEmpty(configItemBean.getDefaultIsEmpty() == null ? 0 : configItemBean.getDefaultIsEmpty());
			item.setValue(configItemBean.getValue());
			item.setDefaultValue(configItemBean.getDefaultValue());
			item.setTempletId(id);
			item.setEnName(configItemBean.getEnName());
			configs.add(item);
		}
		return configs;
	}

	@Override
	public void updateTaskTemplet(TaskTemplet taskTemplet) throws Exception {
		String hql = "from TaskTemplet where id=?";
		TaskTemplet templet = taskRecordDao.getObjectHql(hql, taskTemplet.getId());
		templet.setName(taskTemplet.getName());
		templet.seteName(taskTemplet.geteName());
		templet.setUpdatePeople(taskTemplet.getUpdatePeople());
		templet.setUpdateTime(taskTemplet.getUpdateTime());
		taskRecordDao.getSession().update(templet);

	}

	@Override
	public boolean validateUpdateName(String name, String id, int taskType) throws Exception {
		String sql = "select count(*) from task_templet where  name=? and id<>? and type=?";
		int count = taskRecordDao.countAllByParamsNativeSql(sql, name, id, taskType);
		if (count == 0) {
			return false;
		}
		return true;
	}

	@Override
	public void deleteTaskTemplet(List<String> listIds) throws Exception {

		// 删除模板主表的数据
		String templet = "delete from task_templet where id = ?";
		// 删除文件和模板表关联数据
		String prfRelTemplet = "delete from PRF_RELATED_TEMPLET where templet_id=?";
		// 删除配置项表数据
		String config = "delete from task_config_item where templet_id=?";
		// 删除配置项和人员关联表数据
		String configRelPeople = "delete from TASK_ITEM_RELATED_PEOPLE where config_id in ( select tci.id from task_config_item tci where tci.templet_id=?)";

		for (String id : listIds) {
			taskRecordDao.execteNative(configRelPeople, id);
			taskRecordDao.execteNative(config, id);
			taskRecordDao.execteNative(prfRelTemplet, id);
			taskRecordDao.execteNative(templet, id);
		}

	}

	@Override
	public void updateTaskConfigItems(List<TaskConfigItem> configs) throws Exception {

		// 删除配置项和人员关联表数据
		String configRelPeopleHql = "delete from TaskItemRelatedPeople where configId=?";
		// 插入新数据
		String configItemHql = "update TaskConfigItem set name=?,isEmpty=?,value=?,sort=?,enName=? where id=?";
		for (TaskConfigItem configItem : configs) {
			// 删除配置项和人员关联表数据
			taskRecordDao.execteHql(configRelPeopleHql, configItem.getId());
			// 插入新数据
			for (JecnUser user : configItem.getUsers()) {
				TaskItemRelatedPeople relatedPeople = new TaskItemRelatedPeople();
				relatedPeople.setConfigId(configItem.getId());
				relatedPeople.setPeopleId(user.getPeopleId());
				taskRecordDao.getSession().save(relatedPeople);
			}

			// 更新配置项表
			taskRecordDao.execteHql(configItemHql, configItem.getName(), configItem.getIsEmpty(),
					configItem.getValue(), configItem.getSort(), configItem.getEnName(), configItem.getId());
		}

	}

	@Override
	public void deletePrfRelated(Long prfId, int taskType) throws Exception {

		String sql = "delete from prf_related_templet where related_id=? and type=?";
		taskRecordDao.execteNative(sql, prfId, taskType);

	}

	@Override
	public void saveOrUpdatePrfRelated(Long prfId, int taskType, String approveId, String borrowId, String desuetudeId,
			Long updatePeopleId) throws Exception {
		// 查询该节点下的所有流程、文件、制度
		String sql = "";
		if (taskType == 0 || taskType == 4) {
			sql = "	select C.FLOW_ID,C.FLOW_NAME from JECN_FLOW_STRUCTURE_T C"
					+ "	INNER JOIN JECN_FLOW_STRUCTURE_T P ON C.T_PATH LIKE P.T_PATH " + JecnCommonSql.getConcatChar()
					+ "'%' AND p.FLOW_ID=?";
		} else if (taskType == 1) {
			sql = "	select C.FILE_ID,C.FILE_NAME from JECN_FILE_T C"
					+ "	INNER JOIN JECN_FILE_T P ON C.T_PATH LIKE P.T_PATH " + JecnCommonSql.getConcatChar()
					+ "'%' AND p.FILE_ID=?";
		} else if (taskType == 2 || taskType == 3) {
			sql = "	select C.ID,C.RULE_NAME from JECN_RULE_T C"
					+ "	INNER JOIN JECN_RULE_T P ON C.T_PATH LIKE P.T_PATH " + JecnCommonSql.getConcatChar()
					+ "'%' AND p.ID=?";
		}
		List<Object[]> objs = taskRecordDao.listNativeSql(sql, prfId);
		for (Object[] obj : objs) {
			if (obj[0] == null || obj[1] == null) {
				continue;
			}
			Long rid = Long.valueOf(obj[0].toString());
			deletePrfRelated(rid, taskType);
			JecnUtil.createFileAddPrfRelatedTemplets(taskRecordDao, rid, obj[1].toString(), taskType, approveId,
					borrowId, desuetudeId, updatePeopleId);
		}

	}

	@Override
	public List<PrfRelatedTemplet> listPrfRelatedTemplet() throws Exception {
		String hql = "from PrfRelatedTemplet";
		return taskRecordDao.listHql(hql);
	}

	@Override
	public boolean validateAddName(String name, int taskType) throws Exception {
		String sql = "select count(*) from task_templet where  name=? and type=?";
		int count = taskRecordDao.countAllByParamsNativeSql(sql, name, taskType);
		if (count == 0) {
			return false;
		}
		return true;
	}

	public IJecnConfigItemDao getConfigDao() {
		return configDao;
	}

	public void setConfigDao(IJecnConfigItemDao configDao) {
		this.configDao = configDao;
	}

	public IJecnAbstractTaskDao getAbstractTaskDao() {
		return abstractTaskDao;
	}

	public void setAbstractTaskDao(IJecnAbstractTaskDao abstractTaskDao) {
		this.abstractTaskDao = abstractTaskDao;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JecnTaskResubmitDesignerBean getReSubmitTaskBeanNew(Long rid, TreeNodeType nodeType) {
		// 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		int type = this.getNoteType(nodeType);
		String sql = "select * from JECN_TASK_BEAN_NEW t where t.R_ID=" + rid + " and t.TASK_TYPE=" + type
				+ " and t.is_lock=1";
		List<JecnTaskBeanNew> list = taskRecordDao.getSession().createSQLQuery(sql).addEntity(JecnTaskBeanNew.class)
				.list();
		JecnTaskResubmitDesignerBean designerBean = new JecnTaskResubmitDesignerBean();
		designerBean.setTaskBeanNew(list.get(0));

		sql = "SELECT TT.APPROVE_PID statePeopleId, T.STAGE_MARK state" + "  FROM JECN_TASK_PEOPLE_CONN_NEW TT"
				+ " INNER JOIN JECN_TASK_STAGE T" + "    ON TT.STAGE_ID = T.ID" + " WHERE T.TASK_ID = "
				+ designerBean.getTaskBeanNew().getId();

		List<Object[]> taskStateBeans = taskRecordDao.listNativeSql(sql);
		designerBean.setTaskStateBeans(getTaskStateBeans(taskStateBeans));
		return designerBean;
	}

	private List<JecnTaskStateBean> getTaskStateBeans(List<Object[]> objs) {
		List<JecnTaskStateBean> beans = new ArrayList<JecnTaskStateBean>();
		JecnTaskStateBean bean = null;
		for (Object[] objects : objs) {
			if (objects[0] == null || objects[1] == null) {
				continue;
			}
			bean = new JecnTaskStateBean();
			bean.setState(Integer.valueOf(objects[1].toString()));
			bean.setStatePeopleId(Long.valueOf(objects[0].toString()));
			beans.add(bean);
		}
		return beans;
	}

	@Override
	public List<JecnTaskStage> getJecnTaskStageList(Long taskId) {
		return abstractTaskDao.getJecnTaskStageList(taskId);
	}

	@Override
	public String getTempletIdByTempletType(Long relatedId, int taskType, Integer templetType) {
		String hql = "from PrfRelatedTemplet where relatedId=? and type=? and templetType=?";
		List<PrfRelatedTemplet> relateds = configDao.listHql(hql, relatedId, taskType, templetType);
		if (relateds.size() > 0) {
			return relateds.get(0).getTempletId();
		}
		return null;
	}

	@Override
	public int getChildCount(Long id, TreeNodeType treeNodeType) {
		// TODO Auto-generated method stub
		return 0;
	}
}
