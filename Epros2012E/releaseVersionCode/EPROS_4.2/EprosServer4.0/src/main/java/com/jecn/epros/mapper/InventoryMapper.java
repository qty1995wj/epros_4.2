package com.jecn.epros.mapper;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.inventory.*;
import com.jecn.epros.domain.report.InventoryBaseInfo;
import com.jecn.epros.sqlprovider.InventorySqlProvider;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface InventoryMapper {

    /**
     * 获得制度相关流程架构
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findRuleChildFlowMaps")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findRuleChildFlowMaps(Map<String, Object> map);

    /**
     * 获得制度相关流程
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findRuleChildFlows")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findRuleChildFlows(Map<String, Object> map);

    /**
     * 获得制度相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findRuleChildRules")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findRuleChildRules(Map<String, Object> map);


    /**
     * 制度相关标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findRuleRelatedStandard")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findRuleRelatedStandard(Map<String, Object> map);

    /**
     * 制度相关风险
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findRuleRelatedRisk")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findRuleRelatedRisk(Map<String, Object> map);

    /**
     * 组织相关流程的相关KPI
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findOrgRelatedKPI")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findOrgRelatedKPI(Map<String, Object> map);

    /**
     * 组织相关流程的相关活动
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findOrgRelatedActivity")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findOrgRelatedActivity(Map<String, Object> map);

    /**
     * 组织相关流程的相关流程
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findOrgRelatedProcess")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findOrgRelatedProcess(Map<String, Object> map);

    /**
     * 组织相关流程的相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findOrgRelatedRule")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findOrgRelatedRule(Map<String, Object> map);

    /**
     * 组织相关流程的相关标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findOrgRelatedStandard")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findOrgRelatedStandard(Map<String, Object> map);

    /**
     * 组织相关流程的相关风险
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findOrgRelatedRisk")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findOrgRelatedRisk(Map<String, Object> map);

    /**
     * 部门下的所有部门和岗位
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findPosOrgs")
    @ResultMap("mapper.Inventory.posInventoryOrgBean")
    public List<PosInventoryOrgBean> findPosOrgs(Map<String, Object> map);

    /**
     * 部门下的所有岗位的相关角色和活动
     *
     * @param map
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findPosRoleAndActive")
    @ResultMap("mapper.Inventory.posInventoryRelatedRoleBean")
    public List<PosInventoryRelatedRoleBean> findPosRoleAndActive(Map<String, Object> map);

    /**
     * 部门下的所有岗位的人员
     *
     * @param posId
     * @return
     */
    @SelectProvider(type = InventorySqlProvider.class, method = "findPosUser")
    @ResultMap("mapper.Inventory.JecnUserResult")
    public List<JecnUser> findPosUser();



    @SelectProvider(type = InventorySqlProvider.class, method = "fetchRiskRelatedInfo")
    @ResultMap("mapper.Inventory.fetchRiskRelatedInfo")
    public List<RiskInventoryRelatedInfoBean> fetchRiskRelatedInfo(Map<String, Object> paramMap);

    @SelectProvider(type = InventorySqlProvider.class, method = "fetchRiskRelatedRule")
    @ResultMap("mapper.Inventory.fetchRiskRelatedRule")
    public List<RiskInventoryRelateRuleBean> fetchRiskRelatedRule(Map<String, Object> paramMap);

    @SelectProvider(type = InventorySqlProvider.class, method = "getInventoryBaseInfo")
    @ResultMap("mapper.Inventory.inventoryBaseInfo")
    InventoryBaseInfo getInventoryBaseInfo(Map<String, Object> map);

    @SelectProvider(type = InventorySqlProvider.class, method = "getInventoryOrgFlowsBaseInfo")
    @ResultMap("mapper.Inventory.inventoryBaseInfo")
    InventoryBaseInfo getInventoryOrgFlowsBaseInfo(Map<String, Object> paramMap);

    @SelectProvider(type = InventorySqlProvider.class, method = "fetchPaths")
    List<String> fetchPaths(Map<String, Object> map);

    @SelectProvider(type = InventorySqlProvider.class, method = "findFlowRelateds")
    @ResultMap("mapper.Process.Resource")
    List<Resource> findFlowRelateds(Map<String, Object> paramMap);

    @SelectProvider(type = InventorySqlProvider.class, method = "findRuleRelatedStandardizedSql")
    @ResultMap("mapper.Inventory.fileInventoryRelateBean")
    public List<FileInventoryRelateBean> findRuleRelatedStandardized(Map<String, Object> map);
}
