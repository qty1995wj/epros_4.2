/**
 * UnifiedMessageServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cms.task;

public interface UnifiedMessageServiceSoap_PortType extends java.rmi.Remote {

    /**
     * 待办写入接口AddPendingTask(所有除ID外的字段) : Sn-流程实例唯一标识;ProcessName-流程名称;ActivityName-节点名称;Originator-流程实例发起人Account;OriginatorName=流程实例发起人姓名;OriginatorDeptId-发起人部门ID;OriginatorDeptName-发起人部门名称;SendTo-目标接受者（多人用逗号分割);RedirectUser-转交人;Subject-主题;Url-处理地址;ProcessBuildDate-流程实例发起时间;CreateDate-任务产生时间;InComingSysName-待办来源系统名称;Comments-备注;ExtendCol1-5-扩展;
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect addPendingTask(java.lang.String sn, java.lang.String processName, java.lang.String activityName, java.lang.String originator, java.lang.String originatorName, java.lang.String originatorDeptId, java.lang.String originatorDeptName, java.lang.String sendTo, java.lang.String redirectUser, java.lang.String subject, java.lang.String url, java.util.Calendar processBuildDate, java.util.Calendar createDate, java.lang.String inComingSysName, java.lang.String comments, java.lang.String extendCol1, java.lang.String extendCol2, java.lang.String extendCol3, java.lang.String extendCol4, java.lang.String extendCol5) throws java.rmi.RemoteException;

    /**
     * 待办转已办接口ChangeStatusToHandled sn-流程实例唯一标识，ActivityName-节点名称，SendTo-目标接受者(单个人)，sysName待办来源系统名称
     * 代办代阅都只一次
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect changeStatusToHandled(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String sendTo) throws java.rmi.RemoteException;

    /**
     * 待办转已办接口ChangeStatusToHandled sn-流程实例唯一标识，ActivityName-节点名称，SendTo-目标接受者(单个人)，url
     * 链接(由于待办已办查看链接不一致)，sysName待办来源系统名称 代办代阅都只一次
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect changeStatusToHandledByAUrl(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String sendTo, java.lang.String url) throws java.rmi.RemoteException;

    /**
     * 待办转已办接口ChangeStatusToHandled id-主键
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect changeStatusToHandledByID(int id) throws java.rmi.RemoteException;

    /**
     * 批量 待办转已办接口ChangeStatusToHandled sn-流程实例唯一标识，sysname-系统名称，ActivityName-节点名称，url
     * 链接
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect changeBatchStatusToHandled(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String url) throws java.rmi.RemoteException;

    /**
     * 批量删除 待办DeleteBatchBySN sn-流程实例唯一标识，ActivityName-节点名称，url 链接
     */
    public void deleteBatchBySN(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String url) throws java.rmi.RemoteException;

    /**
     * 判断是否已经推送待办 sn-流程实例唯一标识，ActivityName-节点名称，SendTo-处理人，目标接受者(单个人)，sysName-待办来源系统名称
     */
    public boolean existsPendingTask(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String sendTo) throws java.rmi.RemoteException;

    /**
     * 判断是否已经推送待办 id-主键
     */
    public boolean existsPendingTaskByID(int id) throws java.rmi.RemoteException;

    /**
     * 删除待办 sn-流程实例唯一标识，ActivityName节点名称，SendTo-处理人，目标接受者(单个人)，sysName-待办来源系统名称
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect deletePendingTask(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String sendTo) throws java.rmi.RemoteException;

    /**
     * 删除待办 id-主键
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect deletePendingTaskByID(int id) throws java.rmi.RemoteException;

    /**
     * 删除待办 sysName-待办来源系统名称(必须),ProcessName-流程名称
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect deletePendingTaskBySysNameProcessName(java.lang.String sysname, java.lang.String processName) throws java.rmi.RemoteException;

    /**
     * 待阅写入接口ReadTaskIn(所有除ID外的字段): Sn-外系统标识其系统内部唯一标识; TaskUrl-任务导航Url;
     * Title-流程标题; ProcessName-流程名称; TaskOriginator-任务发起人Account; TaskOriginatorName-任务发起人姓名;
     * TaskCreatDate-任务产生时间-DateTime ;sysName-外系统名称 ; ActivityName -节点名称
     * ; SendTo - 目标接受者（多人用逗号分割)
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect addReadTask(java.lang.String sn, java.lang.String taskUrl, java.lang.String title, java.lang.String processName, java.lang.String taskOriginator, java.lang.String taskOriginatorName, java.util.Calendar taskCreatDate, java.lang.String sysName, java.lang.String activityName, java.lang.String sendTo) throws java.rmi.RemoteException;

    /**
     * 待阅转已阅接口PendingTaskOut(sn,sysname) sn流程，CCActivity环节，SendTo处理人(单个人)，sysName系统
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect changeStatusReadTask(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String sendTo) throws java.rmi.RemoteException;

    /**
     * 待阅转已阅接口 id-主键
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect changeStatusReadTaskByID(int id) throws java.rmi.RemoteException;

    /**
     * 判断是否已经推送待阅 sn-流程实例唯一标识，ActivityName-节点名称，SendTo-处理人(单个人)，目标接受者，sysName-待办来源系统名称
     */
    public boolean existsReadTask(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String sendTo) throws java.rmi.RemoteException;

    /**
     * 判断是否已经推送待阅 id-主键
     */
    public boolean existsReadTaskByID(int id) throws java.rmi.RemoteException;

    /**
     * 删除待阅 sn-流程实例唯一标识，ActivityName节点名称，SendTo-处理人，目标接受者(单个人)，sysName-待办来源系统名称
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect deleteReadTask(java.lang.String sn, java.lang.String sysname, java.lang.String activityName, java.lang.String sendTo) throws java.rmi.RemoteException;

    /**
     * 删除待阅 id-主键
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect deleteReadTaskByID(int id) throws java.rmi.RemoteException;

    /**
     * 草稿写入接口  DraftIn(sn,sysname) Sn-外系统标识其内部唯一id; Title-流程标题;ProcessName-流程名称;
     * SavedTime-保存时间;Url-导航Url;SysName-来源系统;Originator-起草人Account; OriginatorName-起草人姓名
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect addDraft(java.lang.String sn, java.lang.String sysname, java.lang.String title, java.lang.String processName, java.util.Calendar savedTime, java.lang.String url, java.lang.String originator, java.lang.String originatorName) throws java.rmi.RemoteException;

    /**
     * 草稿移除接口 DraftOut(Sn-外系统标识其内部唯一,SysName-来源系统)
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect deleteDraft(java.lang.String sn, java.lang.String sysname) throws java.rmi.RemoteException;

    /**
     * 草稿移除接口 DeleteDraftByID(id-主键)
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect deleteDraftByID(int id) throws java.rmi.RemoteException;

    /**
     * 消息推送 dataid-数据ID,title-标题,baseId-大类【OA01-邮件系统，OA02-流程中心，OA03-资讯中心】,
     * subId-小类【S01	待办，S02	待阅等】,comSub（不是必填项）,toUserAccount-读者（或接收人，支持多人（逗号分割））,sendUserAccount-发送人,publishdate-发送时间,createon-创建时间
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect addPushMessage(java.lang.String dataid, java.lang.String title, java.lang.String baseId, java.lang.String subId, java.lang.String comSub, java.lang.String toUserAccount, java.lang.String sendUserAccount, java.lang.String publishdate, java.lang.String createon) throws java.rmi.RemoteException;

    /**
     * 微信消息推送 fromUserAccount-发送人,toUserAccount-接收人，title-标题，content-内容，types-类型（如：news,text），url-链接地址
     */
    public com.jecn.webservice.cms.task.ProMessageOjbect pushWechat(java.lang.String fromUserAccount, java.lang.String toUserAccount, java.lang.String title, java.lang.String content, java.lang.String agentId, java.lang.String types, java.lang.String mediaId, java.lang.String url) throws java.rmi.RemoteException;
}
