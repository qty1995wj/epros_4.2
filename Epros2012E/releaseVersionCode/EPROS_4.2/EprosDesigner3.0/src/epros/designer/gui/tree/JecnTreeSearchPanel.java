package epros.designer.gui.tree;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.ResourceHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 树的快速查询面板
 * 
 * @author ZHOUXY
 * 
 */
@SuppressWarnings("serial")
public class JecnTreeSearchPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(JecnTreeSearchPanel.class);
	/** 搜索输入框 */
	private JTextField textField = null;
	/** 搜索节点类型下拉框：流程、组织、标准、制度 */
	private JComboBox comboBox = null;
	/** 分隔符 */
	private JSeparator separator = null;
	/** 搜索弹出popup */
	private JPopupMenu searchPopup = null;
	/** 搜索Jpanel */
	private JecnPanel searchPanel = null;
	/** 搜索内容 */
	private JTable searchTable = null;
	private SearchTableMode searchMode = null;
	/** 树主面板 */
	private JecnTreeMainPanel treeMainPanel = null;
	private List<JecnTreeBean> tableList = null;

	public JecnTreeSearchPanel(JecnTreeMainPanel treeMainPanel) {
		this.treeMainPanel = treeMainPanel;
		initComponent();
		initLayout();
	}

	private void initComponent() {
		// 搜索输入框
		textField = new JTextField();
		// 搜索节点类型下拉框：流程、标准、制度、组织、岗位
		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
			if (!JecnDesignerCommon.isAdmin() && JecnConfigTool.isHiddenSelectTreePanel()) {
				comboBox = new JComboBox(new String[] { JecnProperties.getValue("process"),
						JecnProperties.getValue("standard"), JecnProperties.getValue("rule"),
						JecnProperties.getValue("term") });
			} else {
				comboBox = new JComboBox(new String[] { JecnProperties.getValue("process"),
						JecnProperties.getValue("standard"), JecnProperties.getValue("rule"),
						JecnProperties.getValue("organization"), JecnProperties.getValue("position"),
						JecnProperties.getValue("term") });
			}
		} else {
			comboBox = new JComboBox(new String[] { JecnProperties.getValue("process"),
					JecnProperties.getValue("organization"), JecnProperties.getValue("position") });
		}

		// 分隔符
		separator = new JSeparator(SwingConstants.HORIZONTAL);

		// 分隔符
		separator.setBorder(JecnUIUtil.getTootBarBorder());
		separator.setOpaque(false);

		searchPanel = new JecnPanel();
		searchPanel.setLayout(new BorderLayout());
		searchPanel.setBorder(null);
		searchPanel.setOpaque(false);

		searchPopup = new JPopupMenu();
		searchPopup.setLayout(new BorderLayout());
		searchPopup.setBorder(null);

		// 创建标题
		Vector<String> title = new Vector<String>();
		// id
		title.add("id");
		// 名称
		title.add("name");
		// 类型
		title.add("type");
		searchMode = new SearchTableMode(title);
		searchTable = new JTable();
		searchTable.setModel(searchMode);
		// 支持选中行
		searchTable.setRowSelectionAllowed(true);
		// 选中行：单选
		searchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		searchTable.setRowMargin(0);
		searchTable.setShowGrid(false);
		searchTable.setBorder(null);
		searchTable.setRowHeight(25);

		// 隐藏第一列
		TableColumn tableColumnOne = searchTable.getColumnModel().getColumn(0);
		tableColumnOne.setMinWidth(0);
		tableColumnOne.setMaxWidth(0);
		// 设置第三行宽度
		TableColumn tableColumnThree = searchTable.getColumnModel().getColumn(2);
		tableColumnThree.setMinWidth(20);
		tableColumnThree.setMaxWidth(20);
		// 设置table默认行高
		searchPanel.add(searchTable, BorderLayout.CENTER);
		searchPopup.add(searchPanel);

		textField.setBorder(JecnUIUtil.getTootBarBorder());

		textField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				search();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				search();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {

			}
		});

		searchTable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					searchTableMousePressed();
				}
			}
		});

		textField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {// 使用方向键选择table行
				if (searchTable == null || searchTable.getRowCount() <= 0) {
					return;
				}

				int keyCodeInt = e.getKeyCode();
				// 当前选中行
				int index = searchTable.getSelectedRow();
				if (keyCodeInt == KeyEvent.VK_DOWN || keyCodeInt == KeyEvent.VK_KP_DOWN) {
					if (index < 0 || index >= searchTable.getRowCount() - 1) {
						index = 0;
					} else {
						index += 1;
					}
					searchTable.setRowSelectionInterval(index, index);
				} else if (keyCodeInt == KeyEvent.VK_UP || keyCodeInt == KeyEvent.VK_KP_UP) {
					if (index <= 0) {
						index = searchTable.getRowCount() - 1;
					} else {
						index -= 1;
					}
					searchTable.setRowSelectionInterval(index, index);
				}

			}

			public void keyReleased(KeyEvent e) {// 回车打开搜索对应节点
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					searchTableMousePressed();
				}
			}

		});
	}

	private void initLayout() {
		// 搜索节点类型下拉框：流程、组织、标准、制度
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0202(), 0, 0);
		this.add(comboBox, c);
		// 搜索输入框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,

		GridBagConstraints.SOUTHEAST, GridBagConstraints.BOTH, new Insets(2, 0, 2, 2), 0, 0);
		this.add(textField, c);
		// 分隔符
		c = new GridBagConstraints(0, 1, 3, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(separator, c);

	}

	/**
	 * 
	 * 获取树
	 * 
	 * @return ResourceHighEfficiencyTree
	 */
	public ResourceHighEfficiencyTree getTree() {
		return this.treeMainPanel.getTree();
	}

	/**
	 * @author yxw 2012-7-31
	 * @description:搜索并定位到树节点
	 */
	private void search() {
		if (DrawCommon.isNullOrEmtryTrim(textField.getText())) {
			return;
		}
		String name = textField.getText().trim();
		try {

			int selectedComboIndex = comboBox.getSelectedIndex();
			if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
				switch (selectedComboIndex) {
				case 0:// 流程
					if (JecnDesignerCommon.isOnlyViewDesignAuthor()) {
						tableList = JecnDesignerCommon.getDesignerResultList(ConnectionPool.getProcessAction()
								.searchFlowByName(name, JecnConstants.projectId, 0, null));
					} else {
						tableList = ConnectionPool.getProcessAction().searchFlowByName(name, JecnConstants.projectId,
								0, null);
					}
					break;
				case 1:// 标准
					tableList = ConnectionPool.getStandardAction().searchByName(name, JecnConstants.projectId, null);
					break;
				case 2:// 制度
					tableList = ConnectionPool.getRuleAction().searchByName(name, JecnConstants.projectId, null);
					break;
				case 3:// 组织
					tableList = ConnectionPool.getOrganizationAction().searchByName(name, JecnConstants.projectId);
					break;
				case 4:// 岗位
					tableList = ConnectionPool.getOrganizationAction().searchPositionByName(name,
							JecnConstants.projectId);
					break;
				case 5:// 岗位
					tableList = ConnectionPool.getTermDefinitionAction().searchByName(name);
					break;
				default:
					tableList = null;
					break;
				}
			} else {
				switch (selectedComboIndex) {
				case 0:// 流程
					tableList = ConnectionPool.getProcessAction().searchFlowByName(name, JecnConstants.projectId, 0,
							null);
					break;

				case 1:// 组织
					tableList = ConnectionPool.getOrganizationAction().searchByName(name, JecnConstants.projectId);
					break;
				case 2:// 岗位
					tableList = ConnectionPool.getOrganizationAction().searchPositionByName(name,
							JecnConstants.projectId);
					break;
				case 3:// 岗位
					tableList = ConnectionPool.getTermDefinitionAction().searchByName(name);
					break;
				default:
					tableList = null;
					break;
				}

			}

			// 清空表格数据
			for (int index = searchMode.getRowCount() - 1; index >= 0; index--) {
				searchMode.removeRow(index);
			}
			if (tableList != null && tableList.size() > 0) {
				Vector<Vector<Object>> data = toTableData(tableList);

				// 增加
				for (Vector<Object> v : data) {
					searchMode.addRow(v);
				}
				if (searchTable.getRowCount() > 0) {// 默认选中第一行
					searchTable.setRowSelectionInterval(0, 0);
				}

				// 表高
				int height = searchTable.getRowCount() * searchTable.getRowHeight();
				// 输入框宽度
				int width = textField.getWidth();
				Dimension d = new Dimension(width, height);
				// 将弹出窗口的大小设置为指定的宽度和高度。
				searchPopup.setPopupSize(d);

				if (!searchPopup.isVisible()) {
					searchPopup.show(textField, 0, textField.getHeight());
					this.textField.requestFocusInWindow();
				}

			}
		} catch (Exception e1) {
			log.error("JecnTreeSearchPanel search is error", e1);
		}

	}

	private Vector<Vector<Object>> toTableData(List<JecnTreeBean> list) {
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		if (list != null) {

			for (JecnTreeBean b : list) {
				Vector<Object> v = new Vector<Object>();
				v.add(b.getId());
				v.add(b.getName());
				v.add(new ImageIcon("images/treeNodeImages/" + b.getTreeNodeType().toString() + ".gif"));
				data.add(v);
			}
		}
		return data;
	}

	private void searchTableMousePressed() {
		int index = searchTable.getSelectedRow();
		if (tableList == null || tableList.size() == 0 || index < 0 || index >= tableList.size()) {
			return;
		}
		// 控制不在去加载
		treeMainPanel.getTree().setAllowExpand(false);
		JecnTreeBean jecnTreeBean = tableList.get(searchTable.getSelectedRow());
		if (TreeNodeType.position.equals(jecnTreeBean.getTreeNodeType())) {
			try {
				JecnTreeCommon.searchNodePos(jecnTreeBean, treeMainPanel.getTree());
			} catch (Exception e) {
				log.error("JecnTreeSearchPanel searchTableMousePressed is error", e);
			}
		} else {
			try {
				JecnTreeCommon.searchNode(jecnTreeBean, treeMainPanel.getTree());
			} catch (Exception e) {
				log.error("JecnTreeSearchPanel searchTableMousePressed is error", e);
			}
		}
		searchPopup.setVisible(false);
		treeMainPanel.getTree().setAllowExpand(true);

	}

	class SearchTableMode extends DefaultTableModel {

		public SearchTableMode(Vector<String> title) {
			super(title, 0);
		}

		@Override
		public Class getColumnClass(int columnIndex) {
			if (columnIndex == 2) {
				return ImageIcon.class;
			}
			return super.getColumnClass(columnIndex);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

}
