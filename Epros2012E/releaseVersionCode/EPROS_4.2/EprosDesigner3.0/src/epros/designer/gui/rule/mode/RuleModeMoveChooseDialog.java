package epros.designer.gui.rule.mode;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class RuleModeMoveChooseDialog extends JecnMoveChooseDialog {
	private static Logger log = Logger.getLogger(RuleModeMoveChooseDialog.class);

	public RuleModeMoveChooseDialog(List<JecnTreeNode> listMoveNodes, JecnTree moveTree) {
		super(listMoveNodes, moveTree);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		try {
			ConnectionPool.getRuleModeAction().moveRuleModes(ids, pid, JecnUtil.getUserId());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("RuleModeMoveChooseDialog savaData is error", e);
			return false;
		}
	}

	@Override
	public JecnTree getJecnTree() {

		return new HighEfficiencyRuleModeMoveTree(getListIds());
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return title;
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = ConnectionPool.getRuleModeAction().getChildRuleMode(pid);
			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}

				}
			}
		} catch (Exception e) {
			log.error("RuleModeMoveChooseDialog validateName is error", e);
			return false;

		}
		return false;
	}
}
