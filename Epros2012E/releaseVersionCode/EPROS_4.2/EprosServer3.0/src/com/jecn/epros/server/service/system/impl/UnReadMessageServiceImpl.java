package com.jecn.epros.server.service.system.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.bean.system.JecnMessageReaded;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.system.IReadMessageDao;
import com.jecn.epros.server.dao.system.IUnReadMessageDao;
import com.jecn.epros.server.service.system.IUnReadMessageService;
import com.jecn.epros.server.util.JecnUtil;

/***
 * 未读消息
 * 
 * @author 2013-03-05
 * 
 */
@Transactional
public class UnReadMessageServiceImpl implements IUnReadMessageService {

	private IUnReadMessageDao unReadMessageDao;
	private IReadMessageDao readMessageDao;

	public IUnReadMessageDao getUnReadMessageDao() {
		return unReadMessageDao;
	}

	public void setUnReadMessageDao(IUnReadMessageDao unReadMessageDao) {
		this.unReadMessageDao = unReadMessageDao;
	}

	public IReadMessageDao getReadMessageDao() {
		return readMessageDao;
	}

	public void setReadMessageDao(IReadMessageDao readMessageDao) {
		this.readMessageDao = readMessageDao;
	}

	@Override
	public void deleteUnReadMesgs(List<Long> mesgIds) throws Exception {
		// 删除项目
		Set<Long> mesgIdSet = new HashSet<Long>();
		for (Long mesgId : mesgIds) {
			mesgIdSet.add(mesgId);
		}
		String hql = "delete from JecnMessage where messageId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, mesgIdSet);
		for (String str : list) {
			unReadMessageDao.execteHql(str);
		}
	}

	@Override
	public List<JecnMessage> getAllUnReadMesgList(int startNum, int limit,
			Long inceptPeopleId) throws Exception {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)
				|| JecnContants.dbType.equals(DBType.MYSQL)) {
			sql = "select jm.message_id,jm.people_id,"
					+ "jm.message_content,"
					+ "jm.message_topic,"
					+ "jm.or_read,"
					+ " jm.incept_people_id,"
					+ " jm.create_time,"
					+ "ju.LOGIN_NAME"
					+ " from jecn_message jm, jecn_user ju "
					+ "where jm.incept_people_id = ju.people_id and jm.incept_people_id = '"
					+ inceptPeopleId + "'";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "select jm.message_id,jm.people_id,"
					+ "jm.message_content,"
					+ "jm.message_topic,"
					+ "jm.or_read,"
					+ " jm.incept_people_id,"
					+ " to_char(jm.create_time,'yyyy-mm-dd hh24:mi:ss'),"
					+ "ju.LOGIN_NAME"
					+ " from jecn_message jm, jecn_user ju "
					+ "where jm.incept_people_id = ju.people_id and jm.incept_people_id = '"
					+ inceptPeopleId + "'";
		}
		List<Object[]> listObjects = unReadMessageDao.listNativeSql(sql,
				startNum, limit);
		List<JecnMessage> listResult = new ArrayList<JecnMessage>();
		for (Object[] obj : listObjects) {
			JecnMessage jecnMessage = getUnMessageWebBeanByObject(obj);
			if (jecnMessage != null) {
				listResult.add(jecnMessage);
			}
		}
		return listResult;
	}

	public JecnMessage getUnMessageWebBeanByObject(Object[] obj) {
		if (obj == null || obj[0] == null || obj[4] == null) {
			return null;
		}

		SimpleDateFormat newDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		JecnMessage jecnMessage = new JecnMessage();
		// 主键ID
		jecnMessage.setMessageId(Long.valueOf(obj[0].toString()));
		// 人员ID
		if (obj[1] != null) {
			jecnMessage.setPeopleId(Long.valueOf(obj[1].toString()));
		} else {
			jecnMessage.setPeopleId(null);
		}
		// 信息内容
		if (obj[2] != null) {
			jecnMessage.setMessageContent(obj[2].toString());
		} else {
			jecnMessage.setMessageContent("");
		}
		// 信息标题
		if (obj[3] != null) {
			jecnMessage.setMessageTopic(obj[3].toString());
		} else {
			jecnMessage.setMessageTopic("");
		}
		// 是否已读
		if (obj[4] != null) {
			jecnMessage.setNoRead(Long.valueOf(obj[4].toString()));
		} else {
			jecnMessage.setNoRead(null);
		}
		// 收件人ID
		if (obj[5] != null) {
			jecnMessage.setInceptPeopleId(Long.valueOf(obj[5].toString()));
		} else {
			jecnMessage.setInceptPeopleId(null);
		}
		// 创建日期
		if (obj[6] != null) {
			try {
				jecnMessage.setCreateTime(newDate.parse(obj[6].toString()));
			} catch (ParseException e) {
			}
		} else {
			jecnMessage.setNoRead(null);
		}
		// 人员名称和发送日期
		if (obj[7] != null || obj[6] != null) {
			if (JecnContants.dbType.equals(DBType.SQLSERVER)
					|| JecnContants.dbType.equals(DBType.MYSQL)) {
				try {
					//系统
					jecnMessage.setPeopleTime(JecnUtil.getValue("messageSystem") + "/"
							+ newDate.format(newDate.parse(obj[6].toString())));
				} catch (ParseException e) {
				}
			} else {
				jecnMessage.setPeopleTime(JecnUtil.getValue("messageSystem") + "/"
						+ obj[6].toString());
			}
		} else {
			jecnMessage.setPeopleTime(null);
		}
		return jecnMessage;
	}

	@Override
	public int getTotalUnMessageList(Long inceptPeopleId) throws Exception {
		String sql = "select count (MESSAGE_ID) from JECN_MESSAGE where INCEPT_PEOPLE_ID = "
				+ inceptPeopleId;
		return unReadMessageDao.countAllByParamsNativeSql(sql);
	}

	@Override
	public JecnMessageReaded searchUnReadMesgById(Long messageId,
			String peopleName) throws Exception {
		// 未读消息详细表中显示的数据
		JecnMessageReaded readMessage = new JecnMessageReaded();
		JecnMessage jecnMessage = unReadMessageDao.get(messageId);
		List<Long> mesgIdList = new ArrayList<Long>();

		if (jecnMessage != null) {
			readMessage.setPeopleName(peopleName);
			// 主键ID
			readMessage.setMessageId(jecnMessage.getMessageId());
			// 发件人
			readMessage.setPeopleId(jecnMessage.getPeopleId());
			// 信息内容
			readMessage.setMessageContent(jecnMessage.getMessageContent());
			// 信息标题
			readMessage.setMessageTopic(jecnMessage.getMessageTopic());
			// 是否已读
			readMessage.setNoRead(jecnMessage.getNoRead());
			// 收件人
			readMessage.setInceptPeopleId(jecnMessage.getInceptPeopleId());
			// 创建时间
			readMessage.setCreateTime(jecnMessage.getCreateTime());
			// ===================执行已读消息添加
			readMessageDao.save(readMessage);
			mesgIdList.add(Long.valueOf(jecnMessage.getMessageId()));
		} else if (jecnMessage == null) {
			readMessage = readMessageDao.get(messageId);
			readMessage.setPeopleName(peopleName);
			mesgIdList.add(Long.valueOf(readMessage.getMessageId()));
		}
		// ===================删除未读消息对应的数据
		this.deleteUnReadMesgs(mesgIdList);
		// 返回已读消息表中的数据显示到未读消息详细页面中
		return readMessage;

	}

	@Override
	public int getUnReadMesgNum(Long inceptPeopleId) throws Exception {
		return unReadMessageDao.getUnReadMesgNum(inceptPeopleId);
	}

	@Override
	public void tagUnReadMesgs(List<Long> msgIdList) throws Exception {
		for (Long msgId : msgIdList) {
			JecnMessage msg = unReadMessageDao.get(msgId);
			unReadMessageDao.delete(msgId); // 删除未读消息
			JecnMessageReaded readMsg = new JecnMessageReaded();
			readMsg.setMessageId(msg.getMessageId());
			readMsg.setPeopleId(msg.getPeopleId());
			readMsg.setMessageContent(msg.getMessageContent());
			readMsg.setMessageTopic(msg.getMessageTopic());
			readMsg.setNoRead(msg.getNoRead());
			readMsg.setInceptPeopleId(msg.getInceptPeopleId());
			readMsg.setCreateTime(msg.getCreateTime());
			readMessageDao.save(readMsg); // 复制到已读消息，并保存
		}
	}

}
