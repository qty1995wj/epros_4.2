package com.jecn.epros.domain;

import com.jecn.epros.service.TreeNodeUtils.NodeType;
import io.swagger.annotations.ApiModelProperty;

public class MyStarAddForm {

    @ApiModelProperty("关联Id")
    private Long relatedId;
    @ApiModelProperty("类别")
    private NodeType type;
    @ApiModelProperty("名称")
    private String name;


    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
