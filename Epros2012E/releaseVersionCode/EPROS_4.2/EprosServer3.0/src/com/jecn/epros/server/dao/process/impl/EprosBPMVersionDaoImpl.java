package com.jecn.epros.server.dao.process.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.bean.process.EprosBPMVersion;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.process.IEprosBPMVersionDao;

/**
 * ERPOS与BPM对接，版本记录DAO处理
 * 
 *@author ZXH
 * @date 2016-6-17上午09:53:43
 */
public class EprosBPMVersionDaoImpl extends AbsBaseDao<String, String> implements IEprosBPMVersionDao {
	@SuppressWarnings("unchecked")
	@Override
	public List<EprosBPMVersion> findEprosBpmVersions(String flowId) throws Exception {
		String sql = "SELECT T.FLOW_ID,T.FLOW_NAME,T.VERSION,T.UPDATE_TIME FROM EPROS_BMP_VERSION T";
		if (StringUtils.isNotEmpty(flowId)) {
			sql = sql + " WHERE T.FLOW_ID = " + flowId;
		}
		return (List<EprosBPMVersion>) this.listNativeAddEntity(sql, EprosBPMVersion.class);
	}
}
