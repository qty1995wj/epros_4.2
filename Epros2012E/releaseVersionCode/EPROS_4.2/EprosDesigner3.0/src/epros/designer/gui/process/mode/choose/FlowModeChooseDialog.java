package epros.designer.gui.process.mode.choose;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class FlowModeChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(FlowModeChooseDialog.class);
	/**模板类型     0：流程地图，1：流程图, 2:包括流程图与流程地图*/
	private int type  =2;

	/**
	 * @param list
	 * @param chooseType
	 *            10流程地图模板选择11 流程图模板18流程模板选择(流程图模板和流程图地图模板均可选择)
	 */
	public FlowModeChooseDialog(List<JecnTreeBean> list, int chooseType) {
		super(list, chooseType);
		
		this.setTitle(JecnProperties.getValue("modeSelect"));
	}
	public FlowModeChooseDialog(List<JecnTreeBean> list, int chooseType,int type) {
		super(list, chooseType);
		this.type = type;
		this.setTitle(JecnProperties.getValue("modeSelect"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		data.add(jecnTreeBean.getPname());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {

		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				data.add(jecnTreeBean.getPname());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {

		try {
			List<JecnTreeBean> list = ConnectionPool.getProcessModeAction()
					.searchByName(name, JecnConstants.projectId,type);
			return list;
		} catch (Exception e) {
			log.error("FlowModeChooseDialog searchByName is error", e);
		}
		return null;
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("modeNameC");
	}

	@Override
	public JecnTree getJecnTree() {

		return new FlowModelChooseTree();
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 名称
		return title;
	}

}
