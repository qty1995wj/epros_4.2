package epros.draw.gui.swing.calendar;

import javax.swing.table.DefaultTableModel;

/**
 * 
 * 表模型
 * 
 * @author ZHOUXY
 * 
 */
class JecnCalendarDefaultTableModel extends DefaultTableModel {
	JecnCalendarDefaultTableModel() {

	}
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}