package com.jecn.webservice.wanhua.task.interfaceserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for HEADER complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;HEADER&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element name=&quot;SOURCEID&quot; type=&quot;{http://www.w3.org/2001/XMLSchema}string&quot;/&gt;
 *         &lt;element name=&quot;DESTINATIONID&quot; type=&quot;{http://www.w3.org/2001/XMLSchema}string&quot;/&gt;
 *         &lt;element name=&quot;ACTION&quot; type=&quot;{http://www.w3.org/2001/XMLSchema}string&quot;/&gt;
 *         &lt;element name=&quot;SIZE&quot; type=&quot;{http://www.w3.org/2001/XMLSchema}string&quot;/&gt;
 *         &lt;element name=&quot;DATE&quot; type=&quot;{http://www.w3.org/2001/XMLSchema}string&quot;/&gt;
 *         &lt;element name=&quot;BO&quot; type=&quot;{http://www.w3.org/2001/XMLSchema}string&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HEADER", propOrder = { "sourceid", "destinationid", "action", "size", "date", "bo" })
public class HEADER {

	@XmlElement(name = "SOURCEID", required = true)
	protected String sourceid;
	@XmlElement(name = "DESTINATIONID", required = true)
	protected String destinationid;
	@XmlElement(name = "ACTION", required = true)
	protected String action;
	@XmlElement(name = "SIZE", required = true)
	protected String size;
	@XmlElement(name = "DATE", required = true)
	protected String date;
	@XmlElement(name = "BO", required = true)
	protected String bo;

	/**
	 * Gets the value of the sourceid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSOURCEID() {
		return sourceid;
	}

	/**
	 * Sets the value of the sourceid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSOURCEID(String value) {
		this.sourceid = value;
	}

	/**
	 * Gets the value of the destinationid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDESTINATIONID() {
		return destinationid;
	}

	/**
	 * Sets the value of the destinationid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDESTINATIONID(String value) {
		this.destinationid = value;
	}

	/**
	 * Gets the value of the action property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getACTION() {
		return action;
	}

	/**
	 * Sets the value of the action property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setACTION(String value) {
		this.action = value;
	}

	/**
	 * Gets the value of the size property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSIZE() {
		return size;
	}

	/**
	 * Sets the value of the size property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSIZE(String value) {
		this.size = value;
	}

	/**
	 * Gets the value of the date property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDATE() {
		return date;
	}

	/**
	 * Sets the value of the date property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDATE(String value) {
		this.date = value;
	}

	/**
	 * Gets the value of the bo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBO() {
		return bo;
	}

	/**
	 * Sets the value of the bo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBO(String value) {
		this.bo = value;
	}

}
