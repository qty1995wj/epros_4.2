package epros.designer.gui.autoNum.mulinsen;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.autoNum.AutoNumJecnTree;
import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.JecnConstants;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 流程、制度、文件自动编号管理页面
 * 
 * @author ZXH
 * 
 */
public class MulinsenAutoNumSelectDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(MulinsenAutoNumSelectDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	private JecnPanel buttonPanel = null;
	/** L2 auto_productionLine */
	protected TableScroll codeL2ItemScroolPanel = null;
	/** 公司代码 */
	private TableScroll companyCodeScrollPanel = null;
	/** L3 */
	protected AutoTreeScroolPanel codeL3ScrollPanel = null;
	/** 文件类别 auto_fileType */
	protected TableScroll fileTypeScroolPanel = null;

	private JButton okButton = null;

	/** 公司代码 */
	private JecnConfigItemBean companyItem;
	private JecnConfigItemBean codeL2ItemItem;
	private JecnConfigItemBean fileTypeItem;

	/** 名称验证提示 */
	private JLabel promptLab = null;
	private JLabel viewLab = null;

	private static final String SEPARATOR_N = "\n";
	private static final String SEPARATOR_H = "-";

	private JecnTreeNode selectNode;
	/** 0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件 */
	private int relatedType;

	private AutoCodeBean codeBean = null;
	/** true:加载本地 */
	private boolean isLocal;

	private boolean isView;

	private String viewCode;

	private String flowNum;

	public MulinsenAutoNumSelectDialog(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		initData();
		initCompotents();
		initLayout();
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
	}

	public MulinsenAutoNumSelectDialog(JecnTreeNode selectNode, JecnDialog dialog, boolean isView, String flowNum) {
		super(dialog);
		this.isView = isView;
		this.selectNode = selectNode;
		this.flowNum = getJoinNum(flowNum);
		initData();
		initCompotents();
		initLayout();
		this.setLocationRelativeTo(dialog);
	}

	private String getJoinNum(String flowNum) {
		if (StringUtils.isBlank(flowNum)) {
			return flowNum;
		}

		String str[] = flowNum.split(SEPARATOR_H);
		if (str.length == 3) {
			return flowNum;
		}
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < str.length - 1; i++) {
			builder.append(str[i]).append(SEPARATOR_H);
		}
		return builder.length() == 0 ? flowNum : builder.substring(0, builder.length() - 1);
	}

	private void initData() {
		companyItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_companyCode);
		codeL2ItemItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_productionLine);
		fileTypeItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_fileType);
		relatedType = getSelectNodeType(selectNode);
		try {
			codeBean = ConnectionPool.getAutoCodeAction().getAutoCodeBean(selectNode.getJecnTreeBean().getId(),
					relatedType);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(JecnProperties.getValue("autoNumDataERROR"), e);
			return;
		}
		if (codeBean == null) {
			// 读取本地配置
			codeBean = ConfigCommon.INSTANTCE.getAutoDataByConfig(relatedType);
			isLocal = true;
		}
	}

	private void initCompotents() {
		mainPanel = new JPanel();
		// 设置Dialog大小
		this.setSize(StringUtils.isBlank(flowNum) ? 755 : 330, 450);
		this.setResizable(false);
		this.setModal(true);
		this.setTitle(JecnProperties.getValue("autoNum"));

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		buttonPanel = new JecnPanel();
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 公司代码
		companyCodeScrollPanel = new TableScroll(getStringArray(companyItem.getValue()), AutoCodeEnum.companyCode);
		companyCodeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				companyItem.getName(JecnResourceUtil.getLocale())));

		// L3
		codeL3ScrollPanel = new AutoTreeScroolPanel("L3", 5);

		// 文件类型
		fileTypeScroolPanel = new TableScroll(getStringArray(fileTypeItem.getValue()), AutoCodeEnum.fileType);
		fileTypeScroolPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), fileTypeItem
				.getName(JecnResourceUtil.getLocale())));

		// L2
		codeL2ItemScroolPanel = new TableScroll(getStringArray(codeL2ItemItem.getValue()),
				AutoCodeEnum.processMapCodeL2);
		codeL2ItemScroolPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				codeL2ItemItem.getName(JecnResourceUtil.getLocale())));

		okButton = new JButton(JecnProperties.getValue("autoNum"));
		this.getContentPane().add(mainPanel);

		promptLab = createJLabel();
		viewLab = createJLabel();
		viewLab.setText(selectNode.getJecnTreeBean().getNumberId());

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isCheckUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButtonAction();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private JLabel createJLabel() {
		JLabel jLabel = new JLabel();
		jLabel.setForeground(Color.red);
		// 设置验证提示Label的大小
		return jLabel;
	}

	private String[] getStringArray(String value) {
		return value == null ? null : value.split(SEPARATOR_N);
	}

	private String okButtonAction() {

		String fileType = fileTypeScroolPanel.getTableCellValue();
		if (DrawCommon.isNullOrEmtryTrim(fileType) || "-1".equals(fileType)) {
			promptLab.setText(JecnProperties.getValue("fileCategoryCanNotBeEmpty"));
			return null;
		}

		// 2、true：更新
		boolean isUpdate = isCheckUpdate();
		if (!isUpdate) {
			this.dispose();
			return null;
		}

		if (isUpdate) {
			int dialog = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isAutoCode"), null,
					JecnOptionPane.YES_NO_OPTION);
			// 是否自动编号
			if (dialog == JecnOptionPane.NO_OPTION) {
				return null;
			}
		}

		codeBean = createCodeBean();

		try {
			// 3、获取数据源，更新数据库
			codeBean = ConnectionPool.getAutoCodeAction().saveAutoCode(codeBean, selectNode.getJecnTreeBean().getId(),
					relatedType);
			int num = codeBean.getCodeTotal();
			// 记录本地
			ConfigCommon.INSTANTCE.setAutoDataByCodeBean(codeBean, relatedType);
			// 保存成功,更新数据对象
			isLocal = false;
			codeBean.setCodeTotal(num);
			setViewLableText(codeBean);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, "MulinsenAutoNumSelectDialog okButtonAction is error！");
			log.error("MulinsenAutoNumSelectDialog okButtonAction is error！", e);
			return null;
		}
		return null;

	}

	private AutoCodeBean createCodeBean() {
		String processMapCode = codeL3ScrollPanel.getTreeCode();
		String fileType = fileTypeScroolPanel.getTableCellValue();
		String companyCode = companyCodeScrollPanel.getTableCellValue();
		AutoCodeBean codeBean = new AutoCodeBean();
		codeBean.setCompanyCode(getSplitCode(companyCode));
		if (StringUtils.isBlank(flowNum)) {
			codeBean.setProductionLine(getSplitCode(codeL2ItemScroolPanel.getTableCellValue()));
			codeBean.setSmallClass(getSplitCode(processMapCode));
		} else {
			codeBean.setProductionLine("");
			codeBean.setSmallClass("");
			codeBean.setProcessCode(flowNum);
		}
		codeBean.setFileType(getSplitCode(fileType));
		codeBean.setCreatePersonId(JecnConstants.getUserId());
		return codeBean;
	}

	private void setViewLableText(AutoCodeBean codeBean) {
		viewCode = getResultAutoCode(codeBean, isView);
		if (isView) {
			viewCode = viewCode + "##" + codeBean.getCodeTotal();
			this.dispose();
			return;
		}
		selectNode.getJecnTreeBean().setNumberId(viewCode);
		viewLab.setText(viewCode);
	}

	private String getResultAutoCode(AutoCodeBean codeBean, boolean isView) {
		StringBuilder builder = new StringBuilder();
		if (codeBean.getCodeTotal() == -1) {
			return null;
		}
		// 0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
		viewAutoNum(builder, codeBean, isView);
		return builder.toString();
	}

	private void viewAutoNum(StringBuilder builder, AutoCodeBean codeBean, boolean isView) {
		if (StringUtils.isBlank(codeBean.getProductionLine()) && StringUtils.isBlank(codeBean.getSmallClass())
				&& StringUtils.isBlank(codeBean.getFileType())) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("isCanEmpty"));
			return;
		}
		if (StringUtils.isBlank(flowNum)) {
			if (StringUtils.isNotBlank(codeBean.getProductionLine())) {
				// L2
				builder.append(codeBean.getProductionLine()).append(SEPARATOR_H);
			}
			if (StringUtils.isNotBlank(codeBean.getSmallClass())) {
				// L3
				builder.append(codeBean.getSmallClass()).append(SEPARATOR_H);
			}
		} else {
			if (StringUtils.isNotBlank(codeBean.getProcessCode())) {
				// 流程编号
				builder.append(codeBean.getProcessCode()).append(SEPARATOR_H);
			}
		}
		// 文件类别
		builder.append(codeBean.getFileType()).append(
				codeBean.getCodeTotal() < 10 ? "0" + codeBean.getCodeTotal() : codeBean.getCodeTotal());

		if (StringUtils.isNotBlank(codeBean.getCompanyCode()) && !"-1".equals(codeBean.getCompanyCode())) {
			builder.append(SEPARATOR_H).append(codeBean.getCompanyCode());
		}
	}

	/**
	 * 检查是否更新
	 * 
	 * @return
	 */
	private boolean isCheckUpdate() {
		if (isLocal) {
			return true;
		}
		if (isEqualsUpdate(codeBean.getCompanyCode(), getSplitCode(companyCodeScrollPanel.getTableCellValue()))) {
			return true;
		} else if (isEqualsUpdate(codeBean.getFileType(), getSplitCode(fileTypeScroolPanel.getTableCellValue()))) {
			return true;
		} else if (StringUtils.isBlank(flowNum)) {
			if (isEqualsUpdate(codeBean.getProductionLine(), getSplitCode(codeL2ItemScroolPanel.getTableCellValue()))) {
				return true;
			} else if (isEqualsUpdate(codeBean.getSmallClass(), getSplitCode(codeL3ScrollPanel.getTreeCode()))) {
				return true;
			}
		}
		return false;
	}

	private boolean isEqualsUpdate(String oldStr, String newStr) {
		if (StringUtils.isBlank(oldStr) && StringUtils.isBlank(newStr)) {
			return false;
		}
		return StringUtils.isBlank(oldStr) && StringUtils.isNotBlank(newStr) ? true : !oldStr.equals(newStr);
	}

	/**
	 * 0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * 
	 * @param nodeType
	 * @return
	 */
	private int getSelectNodeType(JecnTreeNode jecnTreeNode) {
		int relatedType = 0;
		TreeNodeType nodeType = jecnTreeNode.getJecnTreeBean().getTreeNodeType();
		if (nodeType == TreeNodeType.processMap) {
			relatedType = 0;
		} else if (nodeType == TreeNodeType.process || nodeType == TreeNodeType.processFile) {
			relatedType = 1;
		} else if (nodeType == TreeNodeType.ruleDir) {
			relatedType = 2;
		} else if (nodeType == TreeNodeType.ruleModeFile) {
			relatedType = 3;
		} else if (nodeType == TreeNodeType.ruleFile && jecnTreeNode.getJecnTreeBean().getDataType() == 1) {
			relatedType = 3;
		} else if (nodeType == TreeNodeType.fileDir) {
			relatedType = 4;
		} else if (nodeType == TreeNodeType.file) {
			relatedType = 5;
		}
		return relatedType;
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 0, 5);

		// 添加底部面板
		JecnPanel bottom = new JecnPanel();

		c = new GridBagConstraints(0, 1, 6, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(bottom, c);

		// 添加底部按钮
		addBottomPanel(bottom);

		// 添加编号规则面板
		addAutoCodePanel(c, insets);
	}

	private void addAutoCodePanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, insets,
				0, 0);
		mainPanel.add(companyCodeScrollPanel, c);

		if (StringUtils.isBlank(flowNum)) {
			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
					insets, 0, 0);
			mainPanel.add(codeL2ItemScroolPanel, c);

			c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			mainPanel.add(codeL3ScrollPanel, c);
		}
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, insets,
				0, 0);
		mainPanel.add(fileTypeScroolPanel, c);
	}

	private void addBottomPanel(JecnPanel bottom) {
		bottom.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0);
		bottom.add(viewLab, c);

		c = new GridBagConstraints(1, 0, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		bottom.add(buttonPanel, c);
		// 添加按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(promptLab);
		buttonPanel.add(okButton);
	}

	class TableScroll extends JScrollPane {
		private ResultTable resultTable;

		private AutoCodeEnum codeEnum;

		public TableScroll(String[] strs, AutoCodeEnum codeEnum) {
			resultTable = new ResultTable(strs, codeEnum);
			initCompotents();
			this.codeEnum = codeEnum;
		}

		public ResultTable getResultTable() {
			return resultTable;
		}

		private void initCompotents() {
			Dimension dimension = new Dimension(150, 370);
			this.setMinimumSize(dimension);
			// 滚动条不显示
			this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			// 背景颜色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 边框
			this.setBorder(null);
			this.setViewportBorder(null);
			this.setViewportView(resultTable);

			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {// 双击
						clearSelection();
					}
				}
			});
		}

		private void clearSelection() {
			resultTable.clearSelection();
		}

		/**
		 * 单元格编码
		 * 
		 * @return
		 */
		public String getTableCellValue() {
			if (resultTable.getSelectedRowCount() == 0) {
				return "-1";
			}
			if ("".equals(resultTable.getModel().getValueAt(resultTable.getSelectedRow(), 0).toString())) {
				return "-1";
			}
			return resultTable.getModel().getValueAt(resultTable.getSelectedRow(), 0).toString();
		}
	}

	private String getSplitCode(Object obj) {
		if (obj == null) {
			return "";
		}
		String[] strs = obj.toString().split(" ");
		return strs[0];
	}

	class ResultTable extends JecnTable {
		private String[] strs;
		private String curCode = "";

		public ResultTable(String[] strs, AutoCodeEnum codeEnum) {
			this.strs = strs;
			curCode = getCodeRules(codeEnum);
			initTableModel();
			hiddenTableHeader();
			// 表格反选
			selectCell();
		}

		/** 初始化model */
		void initTableModel() {
			this.setModel(getTableModel());
			this.setShowGrid(false);
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 自定义表头UI
			// this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent());
		}

		@Override
		public int[] gethiddenCols() {
			return null;
		}

		@Override
		public boolean isSelectMutil() {
			return false;
		}

		private Vector<Vector<String>> getContent() {
			Vector<Vector<String>> content = new Vector<Vector<String>>();
			if (strs != null) {
				for (String str : strs) {
					Vector<String> data = new Vector<String>();
					data.add(str);
					content.add(data);
				}
			}
			return content;
		}

		private Vector<String> getTableTitle() {
			Vector<String> title = new Vector<String>();
			title.add("");
			return title;
		}

		private void selectCell() {
			int row = 0;
			if (strs == null) {
				return;
			}
			for (String str : strs) {
				if (curCode != null && curCode.equals(getSplitCode(str))) {
					// 表格反选
					this.setRowSelectionInterval(0, row);
					return;
				}
				row++;
			}
		}
	}

	private String getCodeRules(AutoCodeEnum codeEnum) {
		if (codeBean == null) {
			return "";
		}
		switch (codeEnum) {
		case companyCode:
			return codeBean.getCompanyCode();
		case processMapCodeL2:
			return codeBean.getProductionLine();
		case fileType:
			return codeBean.getFileType();
		default:
			break;
		}
		return "";
	}

	class AutoTreeScroolPanel extends JScrollPane {
		/** 自动编号tree面板 */
		protected AutoNumJecnTree jecnTree = null;

		public AutoTreeScroolPanel(String title, int treeType) {
			jecnTree = getTree(treeType);
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 边框
			// 树滚动面板
			this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
			this.setViewportView(jecnTree);

			allowExpandByName(jecnTree, treeType);
		}

		public AutoNumJecnTree getJecnTree() {
			return jecnTree;
		}

		public String getTreeCode() {
			JecnTreeNode selectNode = this.getJecnTree().getSelectNode();
			if (selectNode == null || selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processCodeRoot) {
				return null;
			}
			return selectNode.getJecnTreeBean().getName();
		}
	}

	/**
	 * 
	 * @param treeType
	 *            3:单位代码，4：文件类别
	 * @return
	 */
	private AutoNumJecnTree getTree(int treeType) {
		return new MulinsenProcessMapCodeAutoTree(treeType);
	}

	/**
	 * 自动编号表格选项分类
	 * 
	 * @author ZXH
	 * 
	 */
	enum AutoCodeEnum {
		companyCode, processMapCodeL2, fileType
	}

	public String getViewCode() {
		return viewCode;
	}

	/**
	 * 树节点展开
	 * 
	 * @param jecnTree
	 * @param treeType
	 */
	private void allowExpandByName(AutoNumJecnTree jecnTree, int treeType) {
		try {
			if (codeBean == null) {
				return;
			}
			JecnTreeBean treeBean = ConnectionPool.getAutoCodeAction().getTreeBeanByName(
					treeType == 3 ? codeBean.getProductionLine() : codeBean.getFileType());
			if (treeBean == null) {
				return;
			}
			jecnTree.allowExpandByTreeBean(treeBean);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("过程代码获取异常！", e);
		}
	}

}
