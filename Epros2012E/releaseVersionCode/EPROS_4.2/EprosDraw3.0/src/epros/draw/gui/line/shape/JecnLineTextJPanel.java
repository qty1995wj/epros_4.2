package epros.draw.gui.line.shape;

import javax.swing.JPanel;

import epros.draw.gui.line.JecnBaseLinePanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 线上内容面板
 * 
 * @author Administrator
 * 
 */
public class JecnLineTextJPanel extends JPanel {
	/** 连接线 */
	protected JecnBaseLinePanel linePanel = null;

	public JecnLineTextJPanel(JecnBaseLinePanel linePanel) {
		if (linePanel == null) {
			// TODO
		}
		this.linePanel = linePanel;

		initComponents();
	}

	private void initComponents() {
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}
}
