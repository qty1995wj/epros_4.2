package epros.designer.gui.rule.mode;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnManageDialog;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class RoutineRuleModeTree extends JecnRoutineTree{
	private static Logger log = Logger.getLogger(RoutineRuleModeTree.class);
	/**所有节点*/
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	/** 制度模板对话框 */
	private JecnManageDialog jecnManageDialog = null;
	
	public RoutineRuleModeTree(JecnManageDialog jecnManageDialog){
		this.jecnManageDialog=jecnManageDialog;
	}
	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.ruleModeRoot,JecnProperties.getValue("ruleMode"));//"制度模板"
		try {
			list=ConnectionPool.getRuleModeAction().getAllNode();
			JecnTreeCommon.addNLevelNodes(list,rootNode);
			
		} catch (Exception e) {
			log.error("RoutineRuleModeTree getTreeModel is error",e);
		}
		return new JecnTreeModel(rootNode);
		
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
