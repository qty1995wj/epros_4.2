package com.jecn.epros.server.connector.iflytek;

public class IflytekDeleteContent {
	/** 流程任务id */
	private String flowId;
	/** 标题 */
	private String requestName;
	/** 流程名称 */
	private String workflowName;
	/** 步骤名称（节点名称） */
	private String nodeName;
	/** 类型编码(传001) */
	private String msgTypeCode = "001";
	/** 接收人（域账号），多个用户使用英文逗号隔开 */
	private String receiver;

	// /** 流程分类 */
	// private String workflowtype;
	// /** 流程备注信息 */
	// private String requestnamenew;
	// /** 撤销人(域账号) */
	// private String cancelAccount;
	// /** 撤销人所对应的步骤名称（节点名称） */
	// private String cancelNodeName;

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getMsgTypeCode() {
		return msgTypeCode;
	}

	public void setMsgTypeCode(String msgTypeCode) {
		this.msgTypeCode = msgTypeCode;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
}
