package com.jecn.epros.server.action.rmi.impl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.system.sysUtil.ConfigUtilEnum;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.service.IOpenServiceInterface;
import com.jecn.webservice.crs.task.RuleInfoData;
import com.jecn.webservice.crs.task.impl.CrsTaskWebServiceImpl;

public class OpenServiceInterfaceImpl implements IOpenServiceInterface {
	private Logger log = LoggerFactory.getLogger(ChartRPCServiceImpl.class);

	@Resource(name = "RuleServiceImpl")
	private IRuleService ruleService;

	@Resource(name = "PersonServiceImpl")
	private IPersonService personService;

	/**
	 * 根据用户登录名获取可查阅的制度
	 * 
	 * @param userCode
	 *            登录名
	 * @param PNUM
	 *            待获取的制度数，-1：查询所有
	 * @return
	 */
	@Override
	public String findRuleListsByUserCode(String userCode, int PNUM) {
		if (StringUtils.isBlank(userCode)) {
			return "登录名称不能为空！";
		}
		try {
			if (PNUM == 0) {
				PNUM = -1;
			}
			List<Object[]> ruleList = ruleService.getRuleByWebService(userCode, 0, PNUM);
			List<RuleInfoData> resultList = CrsTaskWebServiceImpl.getRuleListByObjectsList(ruleList);
			JSONArray jsonArray = JSONArray.fromObject(resultList);
			return jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" userCode = " + userCode + " PNUM = " + PNUM + e.getMessage());
			return "根据登录名 " + userCode + " 获取制度数据异常！";
		}
	}

	/**
	 * 获取 系统 管理员 设计管理员 数量 以及秘钥使用信息
	 * 
	 * @return Map<String, String> : ADMIN_NUM : 管理员数量 总数 DESIGN_NUM : 流程设计专家 总数
	 *         DESIGN_NUM_USE:流程设计专家 使用数 KEY_TYPE : 秘钥类型 永久 or 临时
	 *         KEY_EXPIRY_TIME : 秘钥到期时间
	 */
	@Override
	public Map<String, String> findSytemKeyAndUserNumber() {
		Map<String, String> roleMap = new HashMap<String, String>();
		int admin_use_count = personService.findAdminUseCount();// 管理员使用数
		int design_use_count = personService.findDesignUseCount();// 设计用户使用数
		roleMap.put("ADMIN_NUM", ConfigUtilEnum.INSTANCE.getAdminCount() + "");
		roleMap.put("DESIGN_NUM", ConfigUtilEnum.INSTANCE.getDesignerCount() + "");
		roleMap.put("KEY_TYPE", ConfigUtilEnum.INSTANCE.isTest() + "");
		roleMap.put("ADMIN_NUM_USE", admin_use_count + "");
		roleMap.put("DESIGN_NUM_USE", design_use_count + "");
		roleMap.put("KEY_EXPIRY_TIME", ConfigUtilEnum.INSTANCE.getKeyExpiryTime());
		return roleMap;
	}

	/**
	 * 获取设计器安装程序 如果不存在安装程序 返回 null
	 */
	@Override
	public ByteFileResult downLoadEprosDesigner() {
		ByteFileResult file = null;
		try {
			byte[] data = readUpdateExe();
			if (data != null && data.length > 0) {
				file = new ByteFileResult(true, "succes");
				file.setBytes(data);
				file.setFileName("designer.exe");
			}
			return file;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file = new ByteFileResult(false, "fail");
	}

	/**
	 * 读取更新的exe文件
	 * 
	 * @author fuzhh Apr 7, 2013
	 * @return
	 * @throws Exception
	 */
	public byte[] readUpdateExe() throws Exception {
		String path = JecnPath.APP_PATH + "downLoad/" + "designer.exe";
		InputStream in = new FileInputStream(path);
		return getByte(in);
	}

	/**
	 * 把一个文件转化为字节
	 * 
	 * @param file
	 * @return byte[]
	 * @throws Exception
	 */
	public byte[] getByte(InputStream in) throws Exception {
		byte[] bytes = null;
		if (in != null) {
			try {
				int length = in.available();
				if (length > Integer.MAX_VALUE) // 当文件的长度超过了int的最大值
				{
					return null;
				}
				bytes = new byte[length];
				int offset = 0;
				int numRead = 0;
				while (offset < bytes.length && (numRead = in.read(bytes, offset, bytes.length - offset)) >= 0) {
					offset += numRead;
				}
				// 如果得到的字节长度和file实际的长度不一致就可能出错了
				if (offset < bytes.length) {
					return null;
				}
			} finally {
				try {
					in.close();
				} catch (Exception e) {
					log.error("", e);
				}
			}
		}
		return bytes;
	}

}
