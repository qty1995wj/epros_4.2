package epros.designer.gui.popedom.person;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LoginBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnLoginUtil;
import epros.designer.util.JecnProperties;
import epros.draw.event.JecnOperActionProcess;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.JecnOperInstrToolBarItemPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 密码修改
 * 
 * @author yxw 2013-1-11
 * @description：密码修改
 */
public class PasswordUpdateDialog extends JecnDialog {
	private final Logger log = Logger.getLogger(PasswordUpdateDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 密码输入panel */
	private JPanel passwordPanel = null;
	/** 按钮panel */
	private JPanel buttonPanel = null;
	/** 原密码label */
	private JLabel oldPasswordLabel = null;
	/** 新密码label */
	private JLabel newPasswordOneLabel = null;
	/** 再次输入新密码label */
	private JLabel newPasswordTwoLabel = null;
	/** 原密码Field */
	private JPasswordField oldPasswordField = null;
	/** 新密码Field */
	private JPasswordField newPasswordOneField = null;
	/** 再次输入新密码Field */
	private JPasswordField newPasswordTwoField = null;
	private JLabel tipLabel = null;
	/** 确定按钮 */
	private JButton okBut = null;
	/** 取消按钮 */
	private JButton cancelBut = null;

	public PasswordUpdateDialog(JDialog dialog) {
		super(dialog);
		initCompotents();
		initLayout();
		buttonActionInit();
	}

	/**
	 * @author yxw 2013-1-11
	 * @description:组件初始化
	 */
	private void initCompotents() {
		this.setSize(300, 200);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		this.setTitle(JecnProperties.getValue("passwordUpdate"));

		mainPanel = new JPanel();
		passwordPanel = new JPanel();
		buttonPanel = new JPanel();
		oldPasswordLabel = new JLabel(JecnProperties.getValue("oldPasswordC"));
		newPasswordOneLabel = new JLabel(JecnProperties
				.getValue("newPasswordOneC"));
		newPasswordTwoLabel = new JLabel(JecnProperties
				.getValue("newPasswordTwoC"));
		oldPasswordField = new JPasswordField();
		newPasswordOneField = new JPasswordField();
		newPasswordTwoField = new JPasswordField();
		tipLabel = new JLabel();
		tipLabel.setForeground(Color.RED);
		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		passwordPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	/**
	 * @author yxw 2013-1-11
	 * @description:面板布局
	 */
	private void initLayout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);
		mainPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						10, 10, 2, 10), 0, 0);
		mainPanel.add(passwordPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		mainPanel.add(buttonPanel, c);
		// 密码修改面板
		passwordPanel.setLayout(new GridBagLayout());
		insets = new Insets(2, 1, 5, 2);
		// 原密码
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		passwordPanel.add(oldPasswordLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		passwordPanel.add(oldPasswordField, c);

		// 新密码
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		passwordPanel.add(newPasswordOneLabel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		passwordPanel.add(newPasswordOneField, c);

		// 再次新密码
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		passwordPanel.add(newPasswordTwoLabel, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		passwordPanel.add(newPasswordTwoField, c);

		// 空闲区域
		c = new GridBagConstraints(0, 3, 2, 1, 1.0, 1.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		passwordPanel.add(tipLabel, c);
		// buttonPanel.add(tipLabel);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);

	}

	private void buttonActionInit() {
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PasswordUpdateDialog.this.dispose();
			}

		});
	}

	private void okButPerformed() {
		String oldPassword = new String(this.oldPasswordField.getPassword());
		String newPasswordOne = new String(this.newPasswordOneField
				.getPassword());
		String newPasswordTwo = new String(this.newPasswordTwoField
				.getPassword());
		// 原密码验证
		if (DrawCommon.isNullOrEmtryTrim(oldPassword)) {
			tipLabel.setText(JecnProperties.getValue("oldPasswordNoEmpty"));
			return;
		} else if (DrawCommon.checkPasswordMaxLength(oldPassword)) {
			tipLabel.setText(JecnProperties.getValue("passwordIsEnough"));
			return;
		}

		// 新密码验证
		if (DrawCommon.isNullOrEmtryTrim(newPasswordOne)) {
			tipLabel.setText(JecnProperties.getValue("newPasswordNoEmpty"));
			return;
		} else if (DrawCommon.checkPasswordMaxLength(newPasswordOne)) {
			tipLabel.setText(JecnProperties.getValue("passwordIsEnough"));
			return;
		}

		// 原密码与新密码不能相同 如果相同弹出提示信息
		if (newPasswordOne.equals(oldPassword)) {
			tipLabel.setText(JecnProperties
					.getValue("newPasswordCanNotSameAsOldPassword"));
			return;
		}

		if (!newPasswordOne.equals(newPasswordTwo)) {
			tipLabel.setText(JecnProperties.getValue("twicePasswordNoEquals"));
			return;
		}
		Pattern pat = Pattern
				.compile("[A-Za-z0-9~`!@#$%^&*()\\[\\]_+-={}:\"|;'<>?,./\\\\]+");// 密码只能输入字母、数字或特殊字符!
		String text = JecnProperties.getValue("passwordNoChinese");
		if (JecnConfigTool.isIflytekLogin()) {// 判断是否是科大讯飞登录
			pat = Pattern
					.compile("^(?=.*[0-9].*)(?=.*[A-Z].*)(?=.*[a-z].*).{6,20}$");// 必须包含大小写字母和数字
			text = JecnProperties.getValue("ItMustNumber");
		}
		if (!pat.matcher(newPasswordOne).matches()) {
			if (JecnConfigTool.isIflytekLogin()) {
				JecnOptionPane.showMessageDialog(null, text);
			} else {
				tipLabel.setText(text);
			}
			return;
		}
		try {
			LoginBean loginBean = ConnectionPool.getPersonAction().loginDesign(
					JecnConstants.loginBean.getJecnUser().getLoginName(),
					oldPassword, JecnLoginUtil.getMacAddress(), 0);
			if (loginBean.getLoginState() == 2) {
				tipLabel.setText(JecnProperties.getValue("oldPasswordError"));
				return;
			}
			String pass = ConnectionPool.getPersonAction().userPasswordUpdate(
					JecnConstants.getUserId(), newPasswordOne.trim());
			JecnConstants.loginBean.getJecnUser().setPassword(pass);
			this.dispose();
		} catch (Exception e) {
			log.error("PasswordUpdateDialog  okButPerformed is error", e);
		}
	}
}
