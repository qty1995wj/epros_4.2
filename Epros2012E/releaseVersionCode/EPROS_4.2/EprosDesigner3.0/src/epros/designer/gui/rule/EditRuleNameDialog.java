package epros.designer.gui.rule;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/***
 * 制度重命名
 * 
 * @author 2012-06-12
 * 
 */
public class EditRuleNameDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditRuleNameDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditRuleNameDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		try {
			this.setName(selectNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("EditRuleNameDialog is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		try {
			int userType = 0;
			if (selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.ruleDir) {
				userType = JecnConstants.loginBean.isAdmin() ? 0 : 1;
			}
			// 制度重命名
			ConnectionPool.getRuleAction().reName(this.getName(), selectNode.getJecnTreeBean().getId(),
					JecnConstants.getUserId(), userType);
		} catch (Exception e) {
			log.error("EditRuleNameDialog saveData is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
		// 重命名
		JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}

	@Override
	public boolean validateName(String name, JLabel jLable) {
		if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.ruleDir) {
			return JecnValidateCommon.validateNameNoRestrict(name, promptLab);
		} else {
			return JecnValidateCommon.validateName(name, promptLab);
		}

	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}
}
