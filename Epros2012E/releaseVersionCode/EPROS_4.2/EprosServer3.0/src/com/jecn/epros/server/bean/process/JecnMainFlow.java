package com.jecn.epros.server.bean.process;

public class JecnMainFlow implements java.io.Serializable {
	private Long flowId;// 流程ID
	private String flowAim;// 目的
	private String flowArea;// 范围
	private String flowInput;// 输入
	private String flowOutput;// 输出
	private String flowShowStep;// 步骤
	private String flowNounDefine;// 术语定义
	private String fileName;// 文件名称（不存放在数据库中）
	private Long updateOrnot;// 更新与否
	private Long fileId;// 文件ID
	private String flowDescription;// 流程描述
	private String flowStart;// 流程起始
	private String flowEnd;// 流程终止
	private String flowKpi;// 流程KPI
	private Integer typeResPeople;// 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
	private Long resPeopleId;// 流程责任人Id
	private String supplier;// 供应商
	private String customer;// 客户
	private String riskAndOpportunity;// 风险与机会

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getRiskAndOpportunity() {
		return riskAndOpportunity;
	}

	public void setRiskAndOpportunity(String riskAndOpportunity) {
		this.riskAndOpportunity = riskAndOpportunity;
	}
	public JecnMainFlow() {
	}

	public JecnMainFlow(Long flowId) {
		this.flowId = flowId;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowAim() {
		return flowAim;
	}

	public void setFlowAim(String flowAim) {
		this.flowAim = flowAim;
	}

	public String getFlowArea() {
		return flowArea;
	}

	public void setFlowArea(String flowArea) {
		this.flowArea = flowArea;
	}

	public String getFlowInput() {
		return flowInput;
	}

	public void setFlowInput(String flowInput) {
		this.flowInput = flowInput;
	}

	public String getFlowOutput() {
		return flowOutput;
	}

	public void setFlowOutput(String flowOutput) {
		this.flowOutput = flowOutput;
	}

	public String getFlowShowStep() {
		return flowShowStep;
	}

	public void setFlowShowStep(String flowShowStep) {
		this.flowShowStep = flowShowStep;
	}

	public String getFlowNounDefine() {
		return flowNounDefine;
	}

	public void setFlowNounDefine(String flowNounDefine) {
		this.flowNounDefine = flowNounDefine;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getUpdateOrnot() {
		return updateOrnot;
	}

	public void setUpdateOrnot(Long updateOrnot) {
		this.updateOrnot = updateOrnot;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFlowDescription() {
		return flowDescription;
	}

	public void setFlowDescription(String flowDescription) {
		this.flowDescription = flowDescription;
	}

	public String getFlowStart() {
		return flowStart;
	}

	public void setFlowStart(String flowStart) {
		this.flowStart = flowStart;
	}

	public String getFlowEnd() {
		return flowEnd;
	}

	public void setFlowEnd(String flowEnd) {
		this.flowEnd = flowEnd;
	}

	public String getFlowKpi() {
		return flowKpi;
	}

	public void setFlowKpi(String flowKpi) {
		this.flowKpi = flowKpi;
	}

	public Integer getTypeResPeople() {
		return typeResPeople;
	}

	public void setTypeResPeople(Integer typeResPeople) {
		this.typeResPeople = typeResPeople;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}
}
