package com.jecn.epros.server.service.process;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.process.JecnActivityFileT;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnModeFileT;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutT;

public class ActivityUpdateData implements Serializable {
	/** 如果为null，就代表不用更新 */
	private JecnFlowStructureImageT jecnFlowStructureImageT;
	/** 输出 */
	private List<JecnModeFileT> listModeFileT;
	/** 输入和操作规范 */
	private List<JecnActivityFileT> listJecnActivityFileT;
	/** 指标 */
	private List<JecnRefIndicatorsT> listRefIndicatorsT;
	/** 控制点 */
	private JecnControlPointT controlPoint;

	/** 控制点集合 C */
	private List<JecnControlPointT> listControlPoint;

	/** 活动标准关联表 */
	private List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT;
	/** 线上信息 */
	private List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean;
	/** 活动的主键ID */
	private Long figureId;

	/** 活动输入、输出 new */
	private List<JecnFigureInoutT> listFigureInTs;
	private String figureUUID;
	/** 活动输入、输出 new */
	private List<JecnFigureInoutT> listFigureOutTs;

	public JecnFlowStructureImageT getJecnFlowStructureImageT() {
		return jecnFlowStructureImageT;
	}

	public void setJecnFlowStructureImageT(JecnFlowStructureImageT jecnFlowStructureImageT) {
		this.jecnFlowStructureImageT = jecnFlowStructureImageT;
	}

	public List<JecnModeFileT> getListModeFileT() {
		return listModeFileT;
	}

	public void setListModeFileT(List<JecnModeFileT> listModeFileT) {
		this.listModeFileT = listModeFileT;
	}

	public List<JecnActivityFileT> getListJecnActivityFileT() {
		return listJecnActivityFileT;
	}

	public void setListJecnActivityFileT(List<JecnActivityFileT> listJecnActivityFileT) {
		this.listJecnActivityFileT = listJecnActivityFileT;
	}

	public List<JecnRefIndicatorsT> getListRefIndicatorsT() {
		return listRefIndicatorsT;
	}

	public void setListRefIndicatorsT(List<JecnRefIndicatorsT> listRefIndicatorsT) {
		this.listRefIndicatorsT = listRefIndicatorsT;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public JecnControlPointT getControlPoint() {
		return controlPoint;
	}

	public void setControlPoint(JecnControlPointT controlPoint) {
		this.controlPoint = controlPoint;
	}

	public List<JecnActiveStandardBeanT> getListJecnActiveStandardBeanT() {
		if (listJecnActiveStandardBeanT == null) {
			listJecnActiveStandardBeanT = new ArrayList<JecnActiveStandardBeanT>();
		}
		return listJecnActiveStandardBeanT;
	}

	public void setListJecnActiveStandardBeanT(List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT) {
		this.listJecnActiveStandardBeanT = listJecnActiveStandardBeanT;
	}

	public List<JecnActiveOnLineTBean> getListJecnActiveOnLineTBean() {
		if (listJecnActiveOnLineTBean == null) {
			listJecnActiveOnLineTBean = new ArrayList<JecnActiveOnLineTBean>();
		}
		return listJecnActiveOnLineTBean;
	}

	public void setListJecnActiveOnLineTBean(List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean) {
		this.listJecnActiveOnLineTBean = listJecnActiveOnLineTBean;
	}

	public List<JecnControlPointT> getListControlPoint() {
		if (listControlPoint == null) {
			listControlPoint = new ArrayList<JecnControlPointT>();
		}
		return listControlPoint;
	}

	public void setListControlPoint(List<JecnControlPointT> listControlPoint) {
		this.listControlPoint = listControlPoint;
	}

	public List<JecnFigureInoutT> getListFigureInTs() {
		return listFigureInTs;
	}

	public void setListFigureInTs(List<JecnFigureInoutT> listFigureInTs) {
		this.listFigureInTs = listFigureInTs;
	}

	public List<JecnFigureInoutT> getListFigureOutTs() {
		return listFigureOutTs;
	}

	public void setListFigureOutTs(List<JecnFigureInoutT> listFigureOutTs) {
		this.listFigureOutTs = listFigureOutTs;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

}
