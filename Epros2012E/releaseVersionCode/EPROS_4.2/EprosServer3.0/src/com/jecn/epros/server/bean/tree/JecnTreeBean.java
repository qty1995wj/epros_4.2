package com.jecn.epros.server.bean.tree;

import java.io.Serializable;
import java.util.Date;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class JecnTreeBean implements Serializable {
	private static final long serialVersionUID = 1L;
	/** 节点类型 */
	private TreeNodeType treeNodeType;
	/** 节点ID */
	private Long id;
	/** 父节点ID */
	private Long pid;
	/** 节点名称 */
	private String name;
	/** 父节点名称 */
	private String pname;
	/** 是否有子节点 */
	private boolean isChildNode;
	/** 创建时间 */
	private Date createTime;
	/** 保存方式，0本地保存，1数据库大字段保存 */
	private Integer saveType;

	/** 是否发布 true是发布，false是未发布 */
	private boolean isPub;

	/** 编号 */
	private String numberId;
	/** 制度模板ID */
	private Long modeId;
	/** 关联ID */
	private Long relationId;
	/** 内容 */
	private String content;
	/** 流程、组织是否删除表示：0：否，1： 是 */
	private int isDelete = 0;
	/** 部门名称 ***/
	private String orgName;
	/** T_Path */
	private String t_Path;

	private int sortId;

	/** 是否更新 **/
	private boolean isUpdate = false;
	/** 0：文件库关联；1：本地上传 */
	private int dataType;

	/** 0:流程/架构图； 1：流程（文件）/ 2:部门鸟瞰图 */
	private int nodeType;

	private String loginName;
	/**
	 * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
	 * 8:事业部经理 9：总经理 10:整理意见
	 */
	private int state;

	/** 审批类型 0审批 1借阅 2废止 **/
	private int approveType = -1;
	/** 1可编辑，0不可编辑（表示是否在任务中是否可编辑，并不代表有无权限） **/
	private Integer edit;
	// 部门 岗位 岗位组 是否具有下载权限
	private boolean isDownLoad;

	public boolean canEdit() {
		if (state == 5) {
			return true;
		}
		if (edit != null && edit == 0) {
			return false;
		}
		return true;
	}

	// ------------

	public int getState() {
		return state;
	}

	public boolean isDownLoad() {
		return isDownLoad;
	}

	public void setDownLoad(boolean isDownLoad) {
		this.isDownLoad = isDownLoad;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getApproveType() {
		return approveType;
	}

	public void setApproveType(int approveType) {
		this.approveType = approveType;
	}

	public JecnTreeBean() {

	}

	public JecnTreeBean(Long id, String name, TreeNodeType treeNodeType) {
		this.id = id;
		this.name = name;
		this.treeNodeType = treeNodeType;
	}

	public String getT_Path() {
		return t_Path;
	}

	public void setT_Path(String tPath) {
		t_Path = tPath;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	public TreeNodeType getTreeNodeType() {
		return treeNodeType;
	}

	public void setTreeNodeType(TreeNodeType treeNodeType) {
		this.treeNodeType = treeNodeType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public boolean isChildNode() {
		return isChildNode;
	}

	public void setChildNode(boolean isChildNode) {
		this.isChildNode = isChildNode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getSaveType() {
		return saveType;
	}

	public void setSaveType(Integer saveType) {
		this.saveType = saveType;
	}

	public boolean isPub() {
		return isPub;
	}

	public void setPub(boolean isPub) {
		this.isPub = isPub;
	}

	public String getNumberId() {
		return numberId;
	}

	public void setNumberId(String numberId) {
		this.numberId = numberId;
	}

	public Long getModeId() {
		return modeId;
	}

	public void setModeId(Long modeId) {
		this.modeId = modeId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public boolean isUpdate() {
		return isUpdate;
	}

	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getEdit() {
		return edit;
	}

	public void setEdit(Integer edit) {
		this.edit = edit;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JecnTreeBean) {
			JecnTreeBean bean = (JecnTreeBean) obj;
			if (this.id != null && bean.getId() != null) {
				if (!this.id.equals(bean.getId())) {
					return false;
				}
			}
			if (this.name != null && bean.getName() != null) {
				if (!this.name.equals(bean.getName())) {
					return false;
				}
			}
			return true;
		}
		return super.equals(obj);
	}

	public int getSortId() {
		return sortId;
	}

	public void setSortId(int sortId) {
		this.sortId = sortId;
	}

}
