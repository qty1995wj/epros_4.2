package com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.service.project.ICurProjectService;
import com.jecn.epros.server.util.ApplicationContextUtil;

public class SyncTool {
	private static Logger log = Logger.getLogger(SyncTool.class);

	/**
	 * 
	 * 获取sync目录路径(../WebRoot/Release/com/jecn/dataimport/sync/)
	 * 
	 * @return
	 */
	public static String getSyncPath() {
		URL url = SyncTool.class.getResource("");
		String path = "";
		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			LogFactory.getLog(SyncTool.class).error("Tool类getSyncPath方法：", e);
			path = url.getPath().replaceAll("%20", " ");
		}
		if (!isNullOrEmtryTrim(path)) {
			path = path.substring(0, path.length() - "excelOrDb/util/".length());
		}

		return path;
	}

	/**
	 * 
	 * 获取Excel模板所在目录路径(../WebRoot/Release/com/jecn/dataimport/sync/doc/model/)
	 * 
	 * @return
	 */
	public static String getExcelModelDict() {
		// ../jecn/dataimport/sync/
		String path = getSyncPath();
		if (!isNullOrEmtryTrim(path)) {
			path = path + "doc/model/";
		}

		return path;
	}

	/**
	 * 
	 * 获取Excel模板所在目录路径(../WebRoot/Release/com/jecn/dataimport/sync/doc/data/)
	 * 
	 * @return
	 */
	public static String getExcelDataDict() {
		// ../jecn/dataimport/sync/
		String path = getSyncPath();
		if (!isNullOrEmtryTrim(path)) {
			path = path + "doc/data/";
		}

		return path;
	}

	/**
	 * 
	 * 获取Excel导入模板路径
	 * 
	 * @return String
	 */
	public static String getExcelModelPath() {
		// ../jecn/dataimport/sync/doc/model
		String path = getExcelModelDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.IMPOT_EXCEL_MODEL_FILE_NAME;
		}

		return path;
	}

	/***
	 * 获取KPI导入模版路径
	 * 
	 * @return
	 */
	public static String getExcelKPIModelPath() {
		// ../jecn/dataimport/sync/doc/model
		String path = getExcelModelDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.IMPOT_EXCEL_KPIMODEL_FILE_NAME;
		}
		return path;
	}

	/**
	 * 获取人员变更导出的excel路径(最新excel) zhangft 2013-6-13
	 * 
	 * @return String
	 */
	public static String getPersonnelChangesFilePath() {
		String fileName = "";
		try {
			// 得到文件夹路径 D:/eprosFileLibrary/personnelChangesData/
			String dirPath = JecnFinal.getAllTempPath(JecnFinal.savePersonnelChangesDataPath());
			File fileDir = new File(dirPath);
			if (fileDir.isDirectory()) {
				File[] fileList = fileDir.listFiles();
				if (fileList.length != 0) {
					File maxFile = fileList[0];
					Long maxTime = Long.parseLong(fileList[0].getName()
							.substring(0, fileList[0].getName().indexOf(".")));
					for (int i = 0; i < fileList.length; i++) {
						File tempFile = fileList[i];
						Long tempTime = Long.parseLong(fileList[i].getName().substring(0,
								fileList[i].getName().indexOf(".")));
						if (maxTime < tempTime) {
							maxTime = tempTime;
							maxFile = tempFile;
						}
					}
					fileName = dirPath + maxFile.getName();
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return fileName;
	}

	/**
	 * 
	 * 获取人员数据Excel导出模板路径
	 * 
	 * @return String
	 */
	public static String getOutputExcelModelPath() {
		// ../jecn/dataimport/sync/doc/model
		String path = getExcelModelDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.OUTPUT_EXCEL_MODEL_FILE_NAME;
		}
		return path;
	}

	/**
	 * 人员变更数据导出 Excel模板路径 zhangft 2013-5-29
	 * 
	 * @return String
	 */
	public static String getExportDataOfPersonnelChangesModelPath() {
		// ../jecn/dataimport/sycn/doc/model
		String path = getExcelModelDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.EXPORT_DATA_OF_PERSONNELCHANGES_MODEL_FILE_NAME;
		}
		return path;
	}

	/**
	 * 
	 * 获取人员错误数据Excel导出 模板路径
	 * 
	 * @return String
	 */
	public static String getOutputErrorExcelModelPath() {
		// ../jecn/dataimport/sync/doc/model
		String path = getExcelModelDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.OUTPUT_EXCEL_ERROR_MOULD_NAME;
		}
		return path;
	}

	/**
	 * 获取人员错误数据Excel导出模板路径(附带基准岗位)
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public static String getOutputErrorExcelBaseModelPaht() {
		// ../jecn/dataimport/sync/doc/model
		String path = getExcelModelDict();
		if (StringUtils.isNotBlank(path)) {
			path = path + SyncConstant.OUTPU_EXCEL_ERROR_MOULD_NAME_BASE;
		}
		return path;
	}

	/**
	 * 
	 * 获取Excel导入数据路径
	 * 
	 * @return String
	 */
	public static String getExcelDataPath() {
		// ../jecn/dataimport/sync/
		String path = getExcelDataDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.IMPORT_FILE_PAHT;
		}
		return path;
	}

	/**
	 * 
	 * 获取Excel导入KPI数据路径
	 * 
	 * @return String
	 */
	public static String getExcelKPIFILEPath() {
		// ../jecn/dataimport/sync/
		String path = getExcelDataDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.IMPORT_KPIFILE_PAHT;
		}
		return path;
	}

	/**
	 * 
	 * 获取人员数据导出Excel文件路径
	 * 
	 * @return String
	 */
	public static String getOutPutExcelDataPath() {
		// ../jecn/dataimport/sync/
		String path = getExcelDataDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.OUTPUT_EXCEL_DATA_FILE_NAME;
		}
		return path;
	}

	/**
	 * 人员变更数据导出 Excel 文件路径 zhangft 2013-5-29
	 * 
	 * @return
	 */
	public static String getExportDataOfPersonnelChangesDataPath() {
		// ../jecn/dataimport/sycn/
		String path = getExcelDataDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.EXPORT_DATA_OF_PERSONNELCHANGES_FILE_NAME;
		}
		return path;
	}

	/**
	 * 
	 * 获取人员错误数据导出Excel文件路径
	 * 
	 * @return String
	 */
	public static String getOutPutExcelErrorDataPath() {
		// ../jecn/dataimport/sync/
		String path = getExcelDataDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.OUTPUT_EXCEL_ERROR_DATA_NAME;
		}
		return path;
	}

	/**
	 * 
	 * 获取人员错误数据导出Excel文件路径
	 * 
	 * @return String
	 */
	public static String getOutPutExcelKPIDataPath() {
		// ../jecn/dataimport/sync/
		String path = getExcelDataDict();
		if (!isNullOrEmtryTrim(path)) {
			path = path + SyncConstant.OUTPUT_EXCEL_KPI_DATA_NAME;
		}
		return path;
	}

	/**
	 * 
	 * 获取配置文件的路径(/sync/config/UserConfig.xml)
	 * 
	 * @return String
	 */
	public static String getUserConfigPath() {
		String path = getSyncPath();
		if (!isNullOrEmtryTrim(path)) {
			path = path + "config/UserDBConfig.xml";
		}

		return path;
	}

	/**
	 * 
	 * 邮箱类型是否是0、1
	 * 
	 * @return
	 */
	public static boolean isEmailTypeStr(String emailType) {
		return ("0".equals(emailType) || "1".equals(emailType)) ? true : false;
	}

	/**
	 * 
	 * 给定参数是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmtry(String str) {
		return (str == null || "".equals(str)) ? true : false;
	}

	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	public static boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * zhangft 2013-5-30 给定参数是否 不为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	public static boolean isNotNullObj(Object obj) {
		return (obj != null) ? true : false;
	}

	/**
	 * 
	 * 
	 * 
	 * @param str
	 * @return
	 */
	public static String emtryToNull(String str) {
		if (isNullOrEmtryTrim(str)) {
			return null;
		} else {
			return str.trim();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param str
	 * @return
	 */
	public static String nullToEmtry(String str) {
		if (isNullOrEmtryTrim(str)) {
			return "";
		} else {
			return str.trim();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param obj
	 * @return String
	 */
	public static String nullToEmtryByObj(Object obj) {
		if (isNullObj(obj)) {
			return "";
		} else {
			return obj.toString().trim();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param obj
	 * @return
	 */
	public static Integer nullToEmtryIntByObj(Object obj) {
		if (isNullObj(obj)) {
			return null;
		} else {
			return Integer.valueOf(obj.toString().trim());
		}
	}

	/**
	 * 
	 * 获取HttpSession中项目ID
	 * 
	 * @return String 项目ID值
	 * @throws Exception
	 */
	public static Long getProjectIDBySession() throws Exception {
		ICurProjectService projectService = (ICurProjectService) ApplicationContextUtil.getContext().getBean(
				"CurProjectServiceImpl");
		return projectService.getJecnCurProjectById().getCurProjectId();
	}

	/**
	 * 
	 * 名称不能大于61个汉字或122字母
	 * 
	 * @param name
	 * @return 超过返回true，没有超过返回false
	 */
	public static boolean checkNameMaxLength(String name) {
		return (name != null && MatchTool.getTextLength(name.trim()) > 122) ? true : false;
	}

	public static boolean checkNameMaxLength255(String name) {
		return (name != null && MatchTool.getTextLength(name.trim()) > 255) ? true : false;
	}

	/**
	 * 
	 * 验证是否含有/*?!,%;^#$&`~|:[]{}"等字符
	 * 
	 * 
	 * @param name
	 *            名称
	 * @return boolean
	 *         不是由部门名称由中文、英文、数字及“_”、“-”组成返回true；是由部门名称由中文、英文、数字及“_”、“-”组成返回false
	 */
	public static boolean checkNameFileSpecialChar(String name) {
		return (name != null && name.trim().replaceAll(SyncConstant.IN_STRING, "").length() != 0) ? true : false;
	}

	/**
	 * 人员同步数据校验异常类型
	 * 
	 * @author Administrator
	 * @date： 日期：May 30, 2013 时间：4:26:43 PM
	 */
	public enum DataErrorType {
		none, // 无验证错误
		rowsType, // 行间校验出错异常
		columnsType, // 行内校验出错异常
		serverCount, // 服务用户超出限制
		basePos, // 基准岗位校验出错
		baseRelPos, // 基准岗位与岗位关系校验出错
		syncDataOrgNumError, // 组织出现大量数据变更
		syncDataUserNumError
		// 岗位或人员出现大量数据变更
	}
}
