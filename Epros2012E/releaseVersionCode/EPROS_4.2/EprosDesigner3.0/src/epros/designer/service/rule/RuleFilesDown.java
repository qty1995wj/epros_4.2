package epros.designer.service.rule;

import java.io.File;
import java.io.FileOutputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFileChooser;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.system.JecnSystemData;

public class RuleFilesDown {

	private static final Logger log = Logger.getLogger(RuleFilesDown.class);

	/**
	 * 下载选中的流程文件
	 * 
	 * @author fuzhh 2013-9-4
	 * @param isDownFile
	 * @param selectNodes
	 */
	public String downloadSelectProcess(Long pid, String pName) throws Exception {
		if (pid == null) {
			return "error";
		}

		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSavaRuleUpdateDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}
		JecnFileChooser fileChooser = new JecnFileChooser(pathUrl);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		// 设置标题：下载流程图操作说明
		fileChooser.setDialogTitle(JecnProperties.getValue("ruleBatchDownload"));
		// 文件名称获取当前面板名称
		// 文件过滤类型只显示文件夹
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setSelectedFile(new File(""));
		int i = fileChooser.showSaveDialog(null);
		// 选择确认（yes、ok）后返回该值。
		if (i == JFileChooser.APPROVE_OPTION) {
			// 获取输出文件的路径
			String path = fileChooser.getSelectedFile().getPath();
			if (StringUtils.isBlank(path)) {
				return "error";
			}
			getFilePath(path + "\\" + pName);
			multipleDownloadFiles(getTreeBeansByPid(pid), path, pName);
		}
		return "success";
	}

	private void multipleDownloadFiles(List<JecnTreeBean> list, String selectPath, String dirName) throws Exception {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		for (JecnTreeBean treeBean : list) {
			if (TreeNodeType.ruleDir == treeBean.getTreeNodeType()) {// 创建文件目录
				// 创建文件目录
				getFilePath(selectPath + "\\" + dirName + "\\" + treeBean.getName());
				map.put(treeBean.getId(), treeBean.getName());
			} else if (TreeNodeType.ruleFile == treeBean.getTreeNodeType()) {
				// 创建文件
				crateFile(treeBean, selectPath + "\\" + dirName);
			}
		}

		for (Entry<Long, String> entry : map.entrySet()) {
			List<JecnTreeBean> childs = getTreeBeansByPid(entry.getKey());
			multipleDownloadFiles(childs, selectPath, dirName + "\\" + entry.getValue());
		}
	}

	private void crateFile(JecnTreeBean treeBean, String dirName) throws Exception {
		RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(treeBean.getId());
		FileOpenBean openBean = null;
		if (ruleT.getIsFileLocal() == 1) {// 本地上传制度
			openBean = getRuleLocalFile(ruleT.getFileId());
		} else {
			openBean = getRuleRelatedFile(ruleT.getFileId());
		}
		String filePath = dirName + "\\" + treeBean.getName();

		// 文件输出流
		FileOutputStream fileFos = null;
		try {
			// 输出文件到本地
			fileFos = new FileOutputStream(filePath);
			fileFos.write(openBean.getFileByte());
		} catch (Exception e1) {
			log.error("RuleFilesDown crateFile is error！", e1);
		} finally {
			if (fileFos != null) {
				try {
					fileFos.close();
				} catch (Exception e2) {
					log.error("RuleFilesDown crateFile is error！", e2);
				}
			}
		}
	}

	private List<JecnTreeBean> getTreeBeansByPid(Long pid) throws Exception {
		return ConnectionPool.getRuleAction().getChildRules(pid, JecnConstants.projectId);
	}

	private FileOpenBean getRuleLocalFile(Long id) throws Exception {
		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getRuleAction().g020OpenRuleFile(id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("RuleFilesDown getTreeBeansByPid is error！", e);
			throw e;
		}
		return fileOpenBean;
	}

	private FileOpenBean getRuleRelatedFile(Long id) throws Exception {
		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getFileAction().openFile(id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("RuleFilesDown getRuleRelatedFile is error", e);
			throw e;
		}
		return fileOpenBean;
	}

	/**
	 * 判断文件目录是存在不存在创建
	 * 
	 * @author fuzhh 2013-9-5
	 * @param path
	 */
	private void getFilePath(String path) {
		// 判断文件目录是否存在不存在创建
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
	}
}
