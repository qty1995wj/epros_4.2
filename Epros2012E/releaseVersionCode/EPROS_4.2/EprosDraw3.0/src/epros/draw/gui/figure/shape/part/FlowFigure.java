/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 流程开始，流程结束基类
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：10:36:02 AM
 */
public class FlowFigure extends JecnBaseFigurePanel {

	private int x = 0;
	private int y = 0;
	private int arcWidth;
	private int arcHeight;

	public FlowFigure(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		// if (flowElementData.getCurrentAngle() == 0.0
		// || flowElementData.getCurrentAngle() == 180.0) {
		x = 0;
		y = 0;
		if (userWidth > userHeight) {
			arcWidth = 3 * userWidth / 5;
		} else {
			arcWidth = 3 * userHeight / 5;
		}
		arcHeight = arcWidth;
		// } else if (flowElementData.getCurrentAngle() == 270.0
		// || flowElementData.getCurrentAngle() == 90.0) {
		// x = 0;
		// y = 0;
		// arcWidth = 3 * userHeight / 5;
		// arcHeight = arcWidth;
		// }
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRoundRect(x, y, userWidth, userHeight, arcWidth, arcHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintEventTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintEventTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintEventTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		g2d.fillRoundRect(x, y, userWidth - shadowCount, userHeight
				- shadowCount, arcWidth, arcHeight);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight
				- shadowCount, arcWidth, arcHeight);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DEventLow(g2d, 0);
	}

	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DEventLow(g2d, indent3Dvalue);
	}

	private void paint3DEventLow(Graphics2D g2d, int indent3Dvalue) {
		g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight
				- indent3Dvalue, arcWidth, arcHeight);
		g2d.setStroke(flowElementData.getBasicStroke());
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DEventLine(g2d, 0);
	}

	private void paint3DEventLine(Graphics2D g2d, int indent3Dvalue) {
		g2d.drawRoundRect(x, y, userWidth - indent3Dvalue, userHeight
				- indent3Dvalue, arcWidth, arcHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DEventLine(g2d, 5);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see epros.gui.figure.JecnBaseFigure#paint3DPartTop(java.awt.Graphics2D)
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		g2d.fillRoundRect(x + 4, y + 4, userWidth - 4 - 4, userHeight - 4 - 4,
				arcWidth, arcHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		g2d.fillRoundRect(x + 4, y + 4, userWidth - 4 - 4 - 5,
				userHeight - 4 - 4 - 5, arcWidth, arcHeight);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillRoundRect(x + 5, y + 5, userWidth - 5, userHeight - 5,
				arcWidth, arcHeight);
	}
}
