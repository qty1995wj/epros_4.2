package epros.designer.gui.system;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;

/**
 * 包含文本域的滚动面板
 * 
 * @author ZXH
 * @date 2017-2-7下午02:22:41
 */
public class CommonJTextField {
	private JTextField textField = null;
	private JLabel label = null;

	private String value = null;

	private String labName = null;

	public CommonJTextField(int rows, JecnPanel infoPanel, Insets insets, String labName) {
		this.labName = labName;
		this.textField = new JTextField();

		label = new JLabel(labName);
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(label, c);

		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(7, 3, 7, 5), 0, 0);
		infoPanel.add(textField, c);

		value = textField.getText();
	}

	public JTextField getTextField() {
		return textField;
	}

	public String getTextValue() {
		return textField.getText();
	}

	public boolean isUpdate() {
		if (StringUtils.isBlank(value) && StringUtils.isBlank(textField.getText())) {
			return false;
		}
		return textField.getText().equals(value);
	}

	/**
	 * 提示验证
	 * 
	 * @param verfyLab
	 * @return
	 */
	public boolean isValidateNotPass(JLabel verfyLab) {
		return JecnValidateCommon.validateFieldNotPass(textField.getText(), verfyLab, "");
	}

}
