package epros.designer.gui.process.guide.explain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowRecordT;

import epros.designer.gui.process.guide.guideTable.FlowRecordJDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/**
 * 记录保存
 * 
 * @author user
 * 
 */
public class JecnProcessRecord extends JecnFileDescriptionTable {

	private Logger log = Logger.getLogger(JecnProcessRecord.class);
	private Long flowId;
	protected List<JecnFlowRecordT> listJecnFlowRecordT = null;
	protected final List<JecnFlowRecordT> initData = new ArrayList<JecnFlowRecordT>();

	/** 添加 */
	protected JButton addBut = new JButton(JecnProperties.getValue("add"));

	/** 删除 */
	protected JButton deleteBut = new JButton(JecnProperties.getValue("delete"));

	/** 编辑 */
	protected JButton editBut = new JButton(JecnProperties.getValue("edit"));

	public JecnProcessRecord(int index, String name, boolean isRequest, JecnPanel contentPanel, Long flowId, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		this.titlePanel.add(addBut);
		this.titlePanel.add(editBut);
		this.titlePanel.add(deleteBut);
		try {
			listJecnFlowRecordT = ConnectionPool.getProcessAction().getJecnFlowRecordTListByFlowId(flowId);
		} catch (Exception e) {
			log.error("", e);
		}

		if (listJecnFlowRecordT != null && listJecnFlowRecordT.size() > 0) {
			initData.addAll(listJecnFlowRecordT);
		}

		addBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}
		});
		deleteBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}
		});
		editBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}
		});

		super.initTable();
	}

	/**
	 * 记录保存添加
	 */
	public void addButPerformed() {
		JecnFlowRecordT flowRecordT = new JecnFlowRecordT();
		FlowRecordJDialog flowRecordJDialog = new FlowRecordJDialog(flowRecordT, true);
		flowRecordT.setFlowId(flowId);
		flowRecordJDialog.setVisible(true);
		if (flowRecordJDialog.isSave) {
			listJecnFlowRecordT.add(flowRecordT);
			((JecnTableModel) table.getModel()).addRow(getRowDataByJecnFlowRecordT(flowRecordT));
			table.updateUI();
		}
	}

	/**
	 * 编辑
	 * 
	 */
	public void editButPerformed() {
		int row = table.getSelectedRow();
		if (row == -1) {
			return;
		}
		JecnTableModel jecnTableModel = (JecnTableModel) table.getModel();
		JecnFlowRecordT flowRecordT = this.getJecnFlowRecordTbyTableRowData(jecnTableModel, row);
		FlowRecordJDialog flowRecordJDialog = new FlowRecordJDialog(flowRecordT, false);
		flowRecordJDialog.setVisible(true);
		int index = 1;
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getRecordName()), row, index++);
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getDocId()), row, index++);
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getRecordTransferPeople()), row, index++);
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getRecordSavePeople()), row, index++);
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getSaveLaction()), row, index++);
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getFileTime()), row, index++);
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getSaveTime()), row, index++);
		jecnTableModel.setValueAt(valueOfString(flowRecordT.getRecordApproach()), row, index++);
		table.updateUI();
	}

	private String valueOfString(String str) {
		return str == null ? "" : str;
	}

	/**
	 * 获得当行数据
	 * 
	 * @param jecnTableModel
	 * @param row
	 * @return
	 */
	private JecnFlowRecordT getJecnFlowRecordTbyTableRowData(JecnTableModel jecnTableModel, int row) {
		JecnFlowRecordT flowRecordT = new JecnFlowRecordT();
		flowRecordT.setId(jecnTableModel.getValueAt(row, 0) == null
				|| "".equals(jecnTableModel.getValueAt(row, 0).toString()) ? null : Long.valueOf(jecnTableModel
				.getValueAt(row, 0).toString()));
		int index = 1;
		flowRecordT.setRecordName(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setDocId(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setRecordTransferPeople(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setRecordSavePeople(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setSaveLaction(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setFileTime(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setSaveTime(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setRecordApproach(valueOfString(jecnTableModel.getValueAt(row, index++)));
		flowRecordT.setFlowId(flowId);
		return flowRecordT;
	}

	private String valueOfString(Object obj) {
		return obj == null ? "" : obj.toString();
	}

	public List<JecnFlowRecordT> getResultListJecnFlowRecordT() {
		List<JecnFlowRecordT> list = new ArrayList<JecnFlowRecordT>();
		JecnTableModel jecnTableModel = (JecnTableModel) table.getModel();
		for (int i = 0; i < table.getRowCount(); i++) {
			list.add(this.getJecnFlowRecordTbyTableRowData(jecnTableModel, i));
		}
		return list;
	}

	/**
	 * 记录保存删除
	 */
	public void deleteButPerformed() {
		// 获取选中删除的行
		int[] selectRows = table.getSelectedRows();
		if (selectRows.length == 0) {
			return;
		}
		// 删除记录保存表格中的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) table.getModel()).removeRow(selectRows[i]);
			listJecnFlowRecordT.remove(selectRows[i]);
		}

	}

	@Override
	protected void dbClickMethod() {
		editButPerformed();
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("recordName"));
		title.add(JecnProperties.getValue("num"));
		title.add(JecnProperties.getValue("transferPeople"));
		title.add(JecnProperties.getValue("saveResponsiblePersons"));
		title.add(JecnProperties.getValue("savPlace"));
		title.add(JecnProperties.getValue("filingTime"));
		title.add(JecnProperties.getValue("storageLife"));
		title.add(JecnProperties.getValue("treatmentDue"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> flowRecordContent = new Vector<Vector<String>>();
		for (JecnFlowRecordT flowRecordT : listJecnFlowRecordT) {
			flowRecordContent.add(getRowDataByJecnFlowRecordT(flowRecordT));
		}
		return flowRecordContent;
	}

	private Vector<String> getRowDataByJecnFlowRecordT(JecnFlowRecordT flowRecordT) {
		Vector<String> flowRecordContentStr = new Vector<String>();
		// 编号ID
		String numberId = "";
		if (flowRecordT.getId() != null) {
			numberId = String.valueOf(flowRecordT.getId());
		}
		flowRecordContentStr.add(numberId);
		// 记录名称
		flowRecordContentStr.add(flowRecordT.getRecordName());
		// 编号
		flowRecordContentStr.add(flowRecordT.getDocId() == null ? "" : flowRecordT.getDocId());
		// 转移责任人
		flowRecordContentStr.add(flowRecordT.getRecordTransferPeople() == null ? "" : flowRecordT
				.getRecordTransferPeople());
		// 保存责任人
		flowRecordContentStr.add(flowRecordT.getRecordSavePeople());
		// 保存场所
		flowRecordContentStr.add(flowRecordT.getSaveLaction());
		// 归档时间
		flowRecordContentStr.add(flowRecordT.getFileTime());
		// 保存期限
		flowRecordContentStr.add(flowRecordT.getSaveTime());
		// 到期处理方式
		flowRecordContentStr.add(flowRecordT.getRecordApproach() == null ? "" : flowRecordT.getRecordApproach());
		return flowRecordContentStr;
	}

	@Override
	protected int[] gethiddenCols() {
		if (JecnConfigTool.isShowRecordTransferAndNum()) {
			return new int[] { 0 };
		} else {
			return new int[] { 0, 2, 3 };
		}

	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		List<JecnFlowRecordT> listResult = this.getResultListJecnFlowRecordT();
		if (initData.size() != listResult.size()) {
			return true;
		}
		for (JecnFlowRecordT newJecnFlowRecordT : listResult) {
			Long id = newJecnFlowRecordT.getId();
			if (id == null) {
				return true;
			}
			for (JecnFlowRecordT oldJecnFlowRecordT : initData) {
				if (id.equals(oldJecnFlowRecordT.getId())) {
					if (this.isChange(newJecnFlowRecordT, oldJecnFlowRecordT)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean isChange(JecnFlowRecordT newJecnFlowRecordT, JecnFlowRecordT oldJecnFlowRecordT) {
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getRecordName(), oldJecnFlowRecordT.getRecordName())) {
			return true;
		}
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getRecordApproach(), oldJecnFlowRecordT.getRecordApproach())) {
			return true;
		}
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getDocId(), oldJecnFlowRecordT.getDocId())) {
			return true;
		}
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getRecordTransferPeople(), oldJecnFlowRecordT
				.getRecordTransferPeople())) {
			return true;
		}
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getRecordSavePeople(), oldJecnFlowRecordT.getRecordSavePeople())) {
			return true;
		}
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getSaveLaction(), oldJecnFlowRecordT.getSaveLaction())) {
			return true;
		}
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getSaveTime(), oldJecnFlowRecordT.getSaveTime())) {
			return true;
		}
		if (JecnUtil.isChangeString(newJecnFlowRecordT.getFileTime(), oldJecnFlowRecordT.getFileTime())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
