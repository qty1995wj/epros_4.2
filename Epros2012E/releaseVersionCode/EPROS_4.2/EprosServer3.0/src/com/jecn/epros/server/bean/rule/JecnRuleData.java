package com.jecn.epros.server.bean.rule;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

/**
 * 制度更新数据对象
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-21 时间：下午05:22:01
 */
public class JecnRuleData implements Serializable {
	/**
	 * 制度主表对象
	 * 
	 */
	private RuleT ruelBean;
	private List<JecnRuleOperationCommon> listJecnRuleOperationCommon;
	private String posIds;
	private String orgIds;
	private String posGroupIds;
	private Long updatePersonId;
	
	private AccessId accIds;

	/** 制度关联风险集合 */
	private Set<Long> ruleRiskTList;
	/** 制度关联标准集合 */
	private Set<Long> standardTList;
	/** 制度关联制度集合 */
	private Set<Long> ruleInstitutionList;
	/** 制度文件是否重新选择 */
	private boolean fileChange = false;

	/** 标准化文件 */
	private List<JecnTreeBean> relatedStandardizeds;

	
	

	public Set<Long> getRuleInstitutionList() {
		return ruleInstitutionList;
	}

	public void setRuleInstitutionList(Set<Long> ruleInstitutionList) {
		this.ruleInstitutionList = ruleInstitutionList;
	}

	public boolean isFileChange() {
		return fileChange;
	}

	public void setFileChange(boolean fileChange) {
		this.fileChange = fileChange;
	}

	public String getPosGroupIds() {
		return posGroupIds;
	}

	public void setPosGroupIds(String posGroupIds) {
		this.posGroupIds = posGroupIds;
	}

	public Set<Long> getRuleRiskTList() {
		return ruleRiskTList;
	}

	public void setRuleRiskTList(Set<Long> ruleRiskTList) {
		this.ruleRiskTList = ruleRiskTList;
	}

	public Set<Long> getStandardTList() {
		return standardTList;
	}

	public void setStandardTList(Set<Long> standardTList) {
		this.standardTList = standardTList;
	}

	public RuleT getRuelBean() {
		return ruelBean;
	}

	public void setRuelBean(RuleT ruelBean) {
		this.ruelBean = ruelBean;
	}

	public List<JecnRuleOperationCommon> getListJecnRuleOperationCommon() {
		return listJecnRuleOperationCommon;
	}

	public void setListJecnRuleOperationCommon(List<JecnRuleOperationCommon> listJecnRuleOperationCommon) {
		this.listJecnRuleOperationCommon = listJecnRuleOperationCommon;
	}

	public String getPosIds() {
		return posIds;
	}

	public void setPosIds(String posIds) {
		this.posIds = posIds;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public Long getUpdatePersonId() {
		return updatePersonId;
	}

	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}

	public List<JecnTreeBean> getRelatedStandardizeds() {
		return relatedStandardizeds;
	}

	public void setRelatedStandardizeds(List<JecnTreeBean> relatedStandardizeds) {
		this.relatedStandardizeds = relatedStandardizeds;
	}

	public AccessId getAccIds() {
		return accIds;
	}

	public void setAccIds(AccessId accIds) {
		this.accIds = accIds;
	}
	
	

}
