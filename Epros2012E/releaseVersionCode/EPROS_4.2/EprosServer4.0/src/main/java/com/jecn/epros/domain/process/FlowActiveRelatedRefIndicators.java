package com.jecn.epros.domain.process;

public class FlowActiveRelatedRefIndicators {
    private String indicatorName;//指标名
    private String indicatorValue;//指标值

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public String getIndicatorValue() {
        return indicatorValue;
    }

    public void setIndicatorValue(String indicatorValue) {
        this.indicatorValue = indicatorValue;
    }
}
