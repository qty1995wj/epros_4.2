package com.jecn.epros.domain.org;

import java.util.List;

public class PosStatisticsViewBean {
    private int total;
    private List<SearchPosStatisticsResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SearchPosStatisticsResultBean> getData() {
        return data;
    }

    public void setData(List<SearchPosStatisticsResultBean> data) {
        this.data = data;
    }
}
