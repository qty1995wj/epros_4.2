package com.jecn.epros.domain.journal;


import java.util.List;

/**
 * Created by lenovo on 2017/1/5.
 */
public class FlowViewWebLogBean {
    private int total;
    private List<? extends FlowSearchResultWebLogBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<? extends FlowSearchResultWebLogBean> getData() {
        return data;
    }

    public void setData(List<? extends FlowSearchResultWebLogBean> data) {
        this.data = data;
    }
}
