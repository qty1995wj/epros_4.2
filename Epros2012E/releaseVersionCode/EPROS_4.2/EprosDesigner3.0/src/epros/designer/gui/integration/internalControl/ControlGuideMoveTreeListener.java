package epros.designer.gui.integration.internalControl;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

/**
 * 内控指引知识库节点移动监听
 * 
 * @author Administrator
 * 
 */
public class ControlGuideMoveTreeListener extends JecnTreeListener {

	private static Logger log = Logger
			.getLogger(ControlGuideMoveTreeListener.class);

	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();
	/** 树 */
	private JTree jTree;

	/**
	 * 内控指引知识库节点移动监听构造方法
	 * 
	 * @param listIds
	 *            待移动ID集合
	 * @param jTree
	 *            树
	 */
	public ControlGuideMoveTreeListener(List<Long> listIds, JTree jTree) {
		this.listIds = listIds;
		this.jTree = jTree;
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = ConnectionPool
						.getControlGuideAction().getChildJecnControlGuideDirs(
								node.getJecnTreeBean().getId());
				JecnTreeCommon
						.expansionTreeMoveNode(jTree, list, node, listIds);
			} catch (Exception e) {
				log.error("ControlGuideMoveTreeListener treeExpanded is error！", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

}
