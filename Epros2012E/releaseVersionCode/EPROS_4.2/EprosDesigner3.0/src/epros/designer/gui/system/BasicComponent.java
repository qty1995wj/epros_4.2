package epros.designer.gui.system;

import javax.swing.JLabel;

/**
 * 自定义组件的规范接口
 * @author user
 *
 */
public interface BasicComponent {
	
	public boolean isRequest();
	public boolean isUpdate();
	public boolean isValidateNotPass(JLabel verfyLab);
	public void setEnabled(boolean enable);

}
