<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
  <link rel="stylesheet" type="text/css" href="css/search.css" />
<link rel="stylesheet" type="text/css" href="css/top.css" />
<link rel="stylesheet" type="text/css" href="css/jecn.css" />
<link rel="stylesheet" type="text/css" href="css/selectItem.css" />
<link rel="stylesheet" type="text/css"
	href="ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="ext/resources/css/xtheme-gray.css" />
<link rel="stylesheet" type="text/css" href="css/info.css" />
<script type="text/javascript" src="ext/adapter/ext/ext-base.js" charset="UTF-8"></script>
<script type="text/javascript" src="ext/ext-all.js" charset="UTF-8"></script>
<s:if test="#session.country=='US'">
</s:if><s:else>
<script type="text/javascript"
	src="js/epros_zh_CN.js" charset="UTF-8"></script>
<script type="text/javascript"
	src="ext/ext-lang-zh_CN.js" charset="UTF-8"></script>
</s:else>
<script type="text/javascript" src="jquery/jquery-1.6.2.js" charset="UTF-8"></script>
<script type="text/javascript" src="jquery/ajaxfileupload.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/common.js" charset="UTF-8"></script>
<script type="text/javascript" src="externalJs/initExternalResource.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/top.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/myhome/myhome.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/myhome/processCheck.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/myhome/deptProcess.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/processSys/process.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/processSys/processSys.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/standardSys/standardSys.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/popedom/orgPosition.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/myhome/publicFile.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/myhome/myTask.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/myhome/attention.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/file/fileSys.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/file/fileDetailList.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/task/taskManagementSearch.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/rule/ruleSys.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/search/search.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/systemManager/sysManager.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/systemManager/personImport.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/reports.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processRole.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processActivities.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processApplication.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processInputOutput.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processAnalysis.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processAccess.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processDocumentNumber.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processDocumentPeople.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/reports/processInterviewer.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/systemManager/matchImport.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/systemManager/matchPostTree.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/task/taskCommon.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/rtnlPropose.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/riskSystem/riskSystem.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/processSys/processList.js"></script>
<script type="text/javascript" src="js/popedom/orgList.js"></script>
<script type="text/javascript" src="js/rule/ruleSysList.js"></script>
<script type="text/javascript" src="js/riskSystem/riskList.js"></script>
<script type="text/javascript" src="js/standardSys/standardList.js"></script>
<script type="text/javascript" src="js/reports/taskApproveTime.js"></script>
<script type="text/javascript" src="js/reports/processDetailListReport.js"></script>
<script type="text/javascript" src="js/reports/processScanOptimizeReport.js"></script>
<script type="text/javascript" src="js/reports/processKPIFollowReport.js"></script>
<script type="text/javascript" src="js/rule/singleRuleRtnlPropose.js"></script>
<script type="text/javascript" src="js/browserDetect.js" charset="UTF-8"></script>
<script type="text/javascript">
	var basePath ='${basePath}';
	var index='${param.index}';
	var versionType=${versionType};
	var isAdmin=${webLoginBean.isAdmin};
	var isViewAdmin=${webLoginBean.isViewAdmin};
	var isMapFileShow = ${mapFileShow};
	var isDocControlPermission=${isDocControlPermission};
	var isHiddenStandard=${isHiddenStandard};
	var isHiddenSystem=${isHiddenSystem};
	var isHiddenRisk=${isHiddenRisk};
	var isHiddenBDF = ${isHiddenBDF};
	var isShowUserManager = ${isShowUserManager};
	var isShowRoleManager = ${isShowRoleManager};
</script>
</head>
<body>
	<input id="externalResource" type="hidden" value="<s:property value="externalResourceContentType"/>" />
</body>
</html>