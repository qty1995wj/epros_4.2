package com.jecn.epros.server.action.web.login.ad.mulinsen;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.HttpRequest;

public class MulinsenLoginAction extends JecnAbstractADLoginAction {

	public final static String PARAM = "ssoAuth";

	public MulinsenLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	static {
		MulinsenAfterItem.start();
	}

	public String login() {
		String ssoAuth = this.loginAction.getRequest().getParameter(PARAM);
		log.info("ssoAuth = " + ssoAuth);
		if (StringUtils.isBlank(ssoAuth)) {
			// 重定向到登录页
			loginAction.setCustomRedirectURL(MulinsenAfterItem.SSOLoginUrl);
			return LoginAction.REDIRECT;
		}

		// 获取URL请求的地址
		String response = HttpRequest.sendGet(MulinsenAfterItem.SSO_ADDRESS, "auth=" + ssoAuth);
		log.info("resultJson = " + response);

		if (StringUtils.isBlank(response)) {
			// 重定向到登录页
			loginAction.setCustomRedirectURL(MulinsenAfterItem.SSOLoginUrl);
			return LoginAction.REDIRECT;
		}
		// 获取返回状态
		String status = JSONObject.fromObject(response).getString("Status");
		if ("1".equals(status)) {
			String loginName = JSONObject.fromObject(response).getJSONObject("SuccessData").getString("JobNum");
			return loginByLoginName(loginName);
		} else {
			log.error(JSONObject.fromObject(response).getString("ErrorMessage"));
			return LoginAction.INPUT;
		}
	}

}
