<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
		<script type="text/javascript" src="${basePath}js/common.js"
			charset="UTF-8"></script>
		<script type="text/javascript">
var basePath = "${basePath}";
</script>
	</head>
	<!-- 合理化建议搜索 -->
	<body>
		<div id="div" class="div" style="height: 100%;">
			<form action="getRtnlProposeList.action" id="rtnlProposeForm">
				<div id="search" style="border-width: 1px;
	border-style: solid;
	border-color: RGB(199, 199, 199);
	margin-top: 10px;
	margin-left: 15px;
	margin-right: 15px;
	margin-bottom: 0px;"  align="center">
					<div id="rtnlProposeSearchText" style="overflow: hidden;"
						class="minWidth">
						<table width="100%">
							<tr>
							
								<td align="right">
									<!-- 类型: -->
									<s:text name="typeC" />
								</td>
								<td align="left">
									<select style="width: 80%" id="rtnlType_id">
										<option value="-1">
											<s:text name="all" />
										</option>
										<option value="0">
											<s:text name="flow" />
										</option>
										<s:if test="#application.isHiddenSystem==0" >
											<option value="1">
												<s:text name="rule" />
											</option>
										</s:if>
									</select>
								</td>
							
							
								<td width="10%" height="25px" align="right">
									<!-- 名称： -->
									<s:text name="nameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 80%" id="processName"
										maxLength="32" />
								</td>

								<td style="vertical-align: middle;" width="10%">
									<a onClick="toggle(rtnlProposeSearchText)"
										style="cursor: pointer;" class="packUp"> <img id="packUp"
											src="${basePath}images/iParticipateInProcess/on.gif"
											align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
												name="packUp" /> </span> </a>
								</td>
							</tr>
							<tr>
							
								<td width="10%" align="right">
									<!-- 编号： -->
									<s:text name="prfNumberC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" type="text" style="width: 80%"
										id="processNumber" maxLength="32" />
								</td>
								
								<td height="25px;" align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_rtnlPropose_name"
										onkeyup="search(orgDutyName_rtnlPropose_name,orgDutyId_rtnlPropose_id,search_rtnlPropose_orgDutyName,'org');"
										onblur="divNode(search_rtnlPropose_orgDutyName)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_rtnlPropose_name","orgDutyId_rtnlPropose_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_rtnlPropose_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td></td>
							</tr>
							<tr>
								<td align="right">
									<!-- 责任人: -->
									<s:text name="responsiblePersonsC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="flowResPeople_rtnlPropose_name"
										onkeyup="search(flowResPeople_rtnlPropose_name,flowResPeople_rtnlPropose_id,search_rtnlPropose_flowResPeople,'person');"
										onblur="divNode(search_rtnlPropose_flowResPeople)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("person","flowResPeople_rtnlPropose_name","flowResPeople_rtnlPropose_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_rtnlPropose_flowResPeople"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>
							
								<td height="25px;" align="right">
									<!-- 提交人: -->
									<s:text name="proposeSendPerson" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="submitPeople_rtnlPropose_name"
										onkeyup="search(submitPeople_rtnlPropose_name,submitPeople_rtnlPropose_id,search_rtnlPropose_submitPeople,'person');"
										onblur="divNode(search_rtnlPropose_submitPeople)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("person","submitPeople_rtnlPropose_name","submitPeople_rtnlPropose_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_rtnlPropose_submitPeople"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td></td>
							</tr>
							<tr align="left">
								<td align="right">
									<!-- 时间段: -->
									<s:text name="timeC" />
								</td>
								<td align="left">
									<s:textfield readonly="true" cssClass="Wdate" id="startTime"
										cssStyle="width:36%;border-color: #7F9DB9;"
										onclick="WdatePicker();" theme="simple" />
									-
									<s:textfield readonly="true" cssClass="Wdate" id="endTime"
										cssStyle="width:36%;border-color: #7F9DB9;"
										onclick="WdatePicker();" theme="simple" />
									<!-- <input type="checkbox" checked="checked"
									onclick="taskCheckbox();" id="checkbox"> -->
								</td>
								<td>
								</td>
								<td>
								  <!-- 添加这个div的作用仅仅是隐藏我处理的建议 如果要放开这个功能那么把div删除即可 -->
								  <div style="display:none;">
									<input type="checkbox" id="handleProposeId" />
									<span style="vertical-align: top; margin-right: 10px;"><s:text name="myDealPropose" /></span>
								  </div>
									<input type="checkbox" id="submitProposeId" />
									<span style="vertical-align: top; margin-right: 10px;"><s:text name="myPropose" /></span>
								</td>
								<td>
								</td>
							</tr>
						</table>
					</div>

					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;"
							onclick="tabNavigationProposeBoard(0,1) " />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;" onclick="proposeSearchReset()" />
					</div>
				</div>
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_rtnlPropose_id" value="-1" />
				<!-- 流程责任人ID -->
				<input type="hidden" id="flowResPeople_rtnlPropose_id" value="-1" />
				<!-- 提交人ID -->
				<input type="hidden" id="submitPeople_rtnlPropose_id" value="-1" />
				<!-- 合理化建议类型0：流程；1：制度 -->
				<input type="hidden" id="rtnlType_id" value ="-1"> 
			</form>
			<div align="right" style="border-color: RGB(199, 199, 199); margin: 1px 13px 0px 3px;">
			<!-- 提交建议 -->
				<img src="${basePath}images/propose/submitPropose.gif" style="cursor: pointer;" onclick="showDiv(0,1,'-1','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','-1','','','-1',1);">
			</div>
			<!-- 点击提交建议按钮弹出窗体 start -->
				<div id="bg" class="bg"></div>
				<div id="show" class="show" style="display: none;overFlow-x: hidden; overFlow-y: hidden;">
					<form action="uploadRtnlProposeFile.action" method ="POST" enctype ="multipart/form-data" id="proposeFormId">
						<input type="hidden" id="isFlowRtnl" name="isFlowRtnl" value="<s:property  value="isFlowRtnl" />"/>
						<input type="hidden" id="addOrUpdateId" name="addOrUpdateName" value="0"/>
							<!-- 流程ID -->
						<input type="hidden" id="flowId"  name="flowId" value="-1" />
						<div>
								<!-- 类型: -->
							<s:text name="typeC" />
							<select style="" id="chooseRtnlType_id">
								<option value="0" onclick="selectOptionClick()">
									<s:text name="flow" />
								</option>
								<option value="1" onclick="selectOptionClick()">
									<s:text name="rule" />
								</option>
							</select>
							<!-- 流程/制度名称： -->
							<s:text name="nameC"></s:text>
							<input type="text" style="width: 25%; vertical-align: middle;"
								id="processName_id" maxLength="32" readonly="readonly"/>
							<!-- 选择 -->
							<span style="cursor: pointer;"
								onclick='selectProcessOrRuleWindow("processName_id","flowId")'><img
									style="vertical-align: middle;"
									src="${basePath}images/common/select.gif" /> </span>
						</div>
						<textarea style="width: 100%;height: 285px;" name="fileProposeContent" id="fileProposeContentId"></textarea><br>
							<div style="height: 20px;">
								<span id="addUploadFileSpan" style="display:none;">
									<input type="file" name="upload" id="upfileProposeId" style="padding: 3px 0px 3px 3px;margin: 5px 0px 5px 0px;"/>
									<a href="#" class="flowPropose" id="hideAddFileInputId" onclick="hideAddFileInput()"><s:text name="delete" /></a>
								</span>
							</div>
							<span style="float:right;">
								<input type="button" value="<s:text name="addFilePropose" />" onclick="addFilePropose()" id="buttonAddFile" style="margin-top: 5px;">
								<input type="button" value="<s:text name="okBut" />" onclick="okUploadFile(1,'<s:property  value="flowId" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />')" style="width: 50px;margin-top: 5px;"/>
								<input type="button" value="<s:text name="cancelBut" />" onclick="hideDiv();" style="width: 50px;margin-top: 5px;"/>
							</span>
					</form>
				</div>
			<!-- 点击提交建议按钮弹出窗体 end -->		
			<div id="rtnlProposeShowDiv"
				style="border-color: RGB(199, 199, 199); margin: 0px 10px 10px 13px;" >
				<table width="100%">
					<tr align="left">
						<td>
							<input type="hidden" id="hideStateValue" value="-1" />
							<div
								style=" text-align: center; margin: 0 aoto; background-color: #f7f7f7;zoom:1;background:0px 50%">
								<ul
									style="margin: 0 auto;  list-style-type: none;">
									<li id="liAllStatePropose"
										onclick="tabNavigationProposeBoard(0,'<s:property  value="currentPage" />')"
										class="tabone1">
										<s:text name="all" />(
										<span id="allProId"><s:property value="allStateNum" />
										</span>)
									</li>
									<li id="liReadedPropose"
										onclick="tabNavigationProposeBoard(1,'<s:property  value="currentPage" />')"
										class="tabone2">
										<s:text name="readedPropose" />(
										<span id="readProId"><s:property value="readStateNum" />
										</span>)
									</li>
									<li id="liUnReadPropose"
										onclick="tabNavigationProposeBoard(2,'<s:property  value="currentPage" />')"
										class="tabone2">
										<s:text name="noReadPropose" />(
										<span id="unReadProId"><s:property
												value="unReadStateNum" /> </span>)
									</li>
									<li id="liAcceptPropose"
										onclick="tabNavigationProposeBoard(3,'<s:property  value="currentPage" />')"
										class="tabone2">
										<s:text name="acceptPropose" />(
										<span id="acceptProId"><s:property
												value="acceptStateNum" /> </span>)
									</li>
									<li id="liRefusePropose"
										onclick="tabNavigationProposeBoard(4,'<s:property  value="currentPage" />')"
										class="tabone2">
										<s:text name="refusePropose" />(
										<span id="refuseProId"><s:property
												value="refuseStateNum" /> </span>)
									</li>
								</ul>
								<div style="display:inline;float:left;width: 56%;float: left; height: 25px; margin-top: 10px; margin-bottom: 5px;padding-top: 0px; border-top: gray 2px solid; border-bottom: 1px gray solid; border-right: 1px gray solid; padding-bottom: 5px;line-height:25px;list-style-image:none;list-style-type:none; background-color: #f7f7f7">
								</div>
							</div>
						</td>
					</tr>
				</table>
				<div id="rtnlProposeShow_Grid" style="margin-right: 4px;">

				</div>
				<div>
					<input type="hidden" id="ruleFlowShowDiv" size="20" />
				</div>
			</div>
		</div>
	</body>
</html>