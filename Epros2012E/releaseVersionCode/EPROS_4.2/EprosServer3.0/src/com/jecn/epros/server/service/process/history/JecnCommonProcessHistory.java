package com.jecn.epros.server.service.process.history;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;

/**
 * 流程文控
 * 
 * @author zhr
 * 
 */
public class JecnCommonProcessHistory {

	/**
	 * 样例文控表 JECN_TEMPLET_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnTempletHistorySql(Long processId, Long historyId) {
		String sql = "";
		if (JecnContants.dbType == DBType.ORACLE) {
			// 添加样例表的数据
			sql = "INSERT INTO JECN_TEMPLET_H (ID,MODE_FILE_ID,FILE_ID,HISTORY_ID,GUID)"
					+ "       SELECT JECN_TEMPLET_SQUENCE.NEXTVAL,JT.MODE_FILE_ID,JF.VERSION_ID, "
					+ historyId
					+ " HISTORY_ID,"
					+ JecnCommonSql.getSqlGUID()
					+ "  FROM JECN_TEMPLET_T JT"
					+ "  LEFT JOIN JECN_FILE_t JF ON JF.FILE_ID = JT.FILE_ID"
					+ "       WHERE JT.MODE_FILE_ID IN"
					+ "       (SELECT JMF.MODE_FILE_ID FROM JECN_MODE_FILE_T JMF,JECN_FLOW_STRUCTURE_IMAGE_T JFSI WHERE JMF.FIGURE_ID = JFSI.FIGURE_ID AND  JFSI.FLOW_ID="
					+ processId + ")";
		} else {
			// 添加样例表的数据
			sql = "INSERT INTO JECN_TEMPLET_H (ID,MODE_FILE_ID,FILE_ID,HISTORY_ID,GUID)"
					+ "       SELECT JT.ID,JT.MODE_FILE_ID,JF.VERSION_ID,"
					+ historyId
					+ " HISTORY_ID,"
					+ JecnCommonSql.getSqlGUID()
					+ "  FROM JECN_TEMPLET_T JT"
					+ "  LEFT JOIN JECN_FILE_t JF ON JF.FILE_ID = JT.FILE_ID"
					+ "       WHERE JT.MODE_FILE_ID IN"
					+ "       (SELECT JMF.MODE_FILE_ID FROM JECN_MODE_FILE_T JMF,JECN_FLOW_STRUCTURE_IMAGE_T JFSI WHERE JMF.FIGURE_ID = JFSI.FIGURE_ID AND  JFSI.FLOW_ID="
					+ processId + ")";
		}
		return sql;
	}

	/**
	 * 活动输出文控表 JECN_MODE_FILE_H FILE_M_ID:表单文件ID
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnModeFileHistorySql(Long processId, Long historyId) {
		return "INSERT INTO JECN_MODE_FILE_H (MODE_FILE_ID, FIGURE_ID, FILE_M_ID,HISTORY_ID,GUID)"
				+ "SELECT JMFT.MODE_FILE_ID, " + "JMFT.FIGURE_ID, " + "JF.VERSION_ID," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + "   FROM JECN_MODE_FILE_T JMFT" + " LEFT JOIN JECN_FILE_t JF "
				+ " ON JMFT.FILE_M_ID = JF.FILE_ID "
				+ ",JECN_FLOW_STRUCTURE_IMAGE_T JFSIT WHERE JMFT.FIGURE_ID=JFSIT.FIGURE_ID AND JFSIT.FLOW_ID="
				+ processId;

	}

	/**
	 * 活动输入和操作规范文控表 JECN_ACTIVITY_FILE_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnActivityFileHistorySql(Long processId, Long historyId) {
		return "insert into JECN_ACTIVITY_FILE_H(FILE_ID, FILE_TYPE,FIGURE_ID,FILE_S_ID,HISTORY_ID,GUID)"
				+ "SELECT jaft.FILE_ID," + "jaft.FILE_TYPE," + "jaft.FIGURE_ID," + "JF.VERSION_ID," + historyId
				+ " HISTORY_ID, " + JecnCommonSql.getSqlGUID() + "   FROM JECN_ACTIVITY_FILE_T jaft"
				+ "   LEFT JOIN JECN_FILE_T JF " + "   ON jaft.FILE_S_ID = JF.FILE_ID "
				+ "  ,jecn_flow_structure_image_t jfsit"
				+ "   where  jaft.figure_id=jfsit.figure_id and jfsit.flow_id=" + processId;
	}

	/**
	 * 活动指标文控表 JECN_REF_INDICATORS_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnRefIndicatorstHistorySql(Long processId, Long historyId) {
		return "insert into JECN_REF_INDICATORS_H (ID, ACTIVITY_ID, INDICATOR_NAME, INDICATOR_VALUE,HISTORY_ID,GUID)"
				+ "SELECT jrit.ID, jrit.ACTIVITY_ID, jrit.INDICATOR_NAME, jrit.INDICATOR_VALUE," + historyId
				+ " HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ "      FROM JECN_REF_INDICATORST jrit,jecn_flow_structure_image_t jfsi"
				+ "      where jrit.activity_id = jfsi.figure_id and jfsi.flow_id=" + processId;
	}

	/**
	 * 流程与岗位关系文控表 PROCESS_STATION_RELATED_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getProcessStationRelatedHistorySql(Long processId, Long historyId) {
		return "insert into PROCESS_STATION_RELATED_H (FLOW_STATION_ID, FIGURE_FLOW_ID, FIGURE_POSITION_ID, TYPE,HISTORY_ID,GUID)"
				+ "SELECT psrt.FLOW_STATION_ID, psrt.FIGURE_FLOW_ID, psrt.FIGURE_POSITION_ID, psrt.TYPE,"
				+ historyId
				+ " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID()
				+ "       FROM PROCESS_STATION_RELATED_T psrt,jecn_flow_structure_image_t jfsit"
				+ "       where psrt.figure_flow_id=jfsit.figure_id and jfsit.flow_id=" + processId;
	}

	/**
	 * 活动角色关联文控表 JECN_ROLE_ACTIVE_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnRoleActiveHistorySql(Long processId, Long historyId) {
		return "insert into JECN_ROLE_ACTIVE_H (AUTO_INCREASE_ID, FIGURE_ROLE_ID, FIGURE_ACTIVE_ID,HISTORY_ID,GUID)"
				+ "SELECT jrat.AUTO_INCREASE_ID, jrat.FIGURE_ROLE_ID, jrat.FIGURE_ACTIVE_ID," + historyId
				+ " HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ "  FROM  JECN_ROLE_ACTIVE_T jrat,jecn_flow_structure_image_t jfsit where jrat.figure_role_id"
				+ "  = jfsit.figure_id and jfsit.flow_id=" + processId;
	}

	/**
	 * 流程图基本信息文控表 JECN_FLOW_BASIC_INFO_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowBasicInfo(Long processId, Long historyId) {
		return "insert into JECN_FLOW_BASIC_INFO_H "
				+ "(FLOW_ID, FLOW_NAME, FLOW_PURPOSE, FLOW_STARTPOINT, FLOW_ENDPOINT, INPUT, "
				+ "OUPUT, APPLICABILITY, NOUT_GLOSSARY,"
				+ " FLOW_CUSTOM, FILE_ID, IS_SHOW_LINE_X, IS_SHOW_LINE_Y, IS_X_OR_Y, "
				+ "DRIVER_TYPE, DRIVER_RULES, TEST_RUN_NUMBER, TYPE_RES_PEOPLE, "
				+ "RES_PEOPLE_ID, TYPE_ID, DFLOW_SUPPLEMENT, NO_APPLICABILITY, "
				+ "FLOW_SUMMARIZE, EXPIRY,WARD_PEOPLE_ID,FICTION_PEOPLE_ID, "
				+ "FLOW_RELATEDFILE, FLOW_CUSTOMONE, FLOW_CUSTOMTWO, FLOW_CUSTOMTHREE, FLOW_CUSTOMFOUR, FLOW_CUSTOMFIVE,HISTORY_ID,GUID) "
				+ "select jfb.FLOW_ID, jfb.FLOW_NAME, jfb.FLOW_PURPOSE, jfb.FLOW_STARTPOINT, jfb.FLOW_ENDPOINT,"
				+ "    jfb.INPUT, jfb.OUPUT, jfb.APPLICABILITY, jfb.NOUT_GLOSSARY,"
				+ "     jfb.FLOW_CUSTOM, jf.version_id, jfb.IS_SHOW_LINE_X,"
				+ "    jfb.IS_SHOW_LINE_Y, jfb.IS_X_OR_Y, jfb.DRIVER_TYPE, jfb.DRIVER_RULES,"
				+ "     jfb.TEST_RUN_NUMBER, jfb.TYPE_RES_PEOPLE, jfb.RES_PEOPLE_ID, jfb.TYPE_ID,"
				+ "     jfb.DFLOW_SUPPLEMENT, jfb.NO_APPLICABILITY, jfb.FLOW_SUMMARIZE,"
				+ "    jfb.EXPIRY,WARD_PEOPLE_ID,jfb.FICTION_PEOPLE_ID, jfb.FLOW_RELATEDFILE, jfb.FLOW_CUSTOMONE,"
				+ "     jfb.FLOW_CUSTOMTWO, jfb.FLOW_CUSTOMTHREE, jfb.FLOW_CUSTOMFOUR, jfb.FLOW_CUSTOMFIVE,"
				+ historyId + " HISTORY_ID," + JecnCommonSql.getSqlGUID() + "    from jecn_flow_basic_info_t  jfb"
				+ "    left join" + "    jecn_file_t jf" + "    on jf.file_id = jfb.file_id" + "    where jfb.flow_id="
				+ processId;

	}

	/**
	 * 流程图与支持工具关系文控表 JECN_FLOW_TOOL_RELATED_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowToolRelatedHistorySql(Long processId, Long historyId) {
		return "insert into JECN_FLOW_TOOL_RELATED_H (FLOW_TOOL_ID, FLOW_ID, FLOW_SUSTAIN_TOOL_ID,HISTORY_ID,GUID)"
				+ "SELECT FLOW_TOOL_ID, FLOW_ID, FLOW_SUSTAIN_TOOL_ID," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + "  FROM Jecn_Flow_Tool_Related_T WHERE FLOW_ID=" + processId;
	}

	/**
	 * 流程图操作说明记录保存文控表 JECN_FLOW_RECORD_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowRecordHistorySql(Long processId, Long historyId) {
		return "insert into JECN_FLOW_RECORD_H "
				+ "(ID, FLOW_ID, RECORD_NAME, RECORD_SAVE_PEOPLE, SAVE_LOCATION, FILE_TIME, SAVE_TIME, "
				+ "RECORD_APPROACH,DOC_ID,RECORD_TRANSFER_PEOPLE,HISTORY_ID,GUID) "
				+ "SELECT ID, FLOW_ID, RECORD_NAME, RECORD_SAVE_PEOPLE, SAVE_LOCATION,"
				+ " FILE_TIME, SAVE_TIME, RECORD_APPROACH ,DOC_ID,RECORD_TRANSFER_PEOPLE, " + historyId
				+ " HISTORY_ID," + JecnCommonSql.getSqlGUID() + "  FROM JECN_FLOW_RECORD_T WHERE FLOW_ID=" + processId;
	}

	/**
	 * 流程kpi文控表 JECN_FLOW_KPI_NAME_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowKpiNameHistorySql(Long processId, Long historyId) {
		return "INSERT INTO JECN_FLOW_KPI_NAME_H"
				+ " (KPI_AND_ID,FLOW_ID,KPI_NAME,KPI_TYPE,KPI_DEFINITION,KPI_STATISTICAL_METHODS,"
				+ "KPI_TARGET,KPI_HORIZONTAL,KPI_VERTICAL,CREAT_TIME,KPI_TARGET_OPERATOR,"
				+ "KPI_DATA_METHOD,KPI_DATA_PEOPEL_ID,KPI_RELEVANCE,KPI_TARGET_TYPE,"
				+ "FIRST_TARGER_ID,UPDATE_TIME,CREATE_PEOPLE_ID,UPDATE_PEOPLE_ID,KPI_PURPOSE,"
				+ "KPI_POINT,KPI_PERIOD,KPI_EXPLAIN,HISTORY_ID,GUID)" + " SELECT KPI_AND_ID,FLOW_ID,KPI_NAME,KPI_TYPE,"
				+ "KPI_DEFINITION,KPI_STATISTICAL_METHODS,KPI_TARGET,"
				+ "KPI_HORIZONTAL,KPI_VERTICAL,CREAT_TIME,KPI_TARGET_OPERATOR,"
				+ "KPI_DATA_METHOD,KPI_DATA_PEOPEL_ID,KPI_RELEVANCE,KPI_TARGET_TYPE,"
				+ "FIRST_TARGER_ID,UPDATE_TIME,CREATE_PEOPLE_ID,UPDATE_PEOPLE_ID,"
				+ "KPI_PURPOSE,KPI_POINT,KPI_PERIOD,KPI_EXPLAIN," + historyId + "  HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + "  FROM JECN_FLOW_KPI_NAME_T WHERE FLOW_ID = " + processId;
	}

	/**
	 * JECN_ACTIVE_ONLINE_H 活动与信息化关系文控表
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnActiveOnlineHistorySql(Long processId, Long historyId) {
		return "INSERT INTO JECN_ACTIVE_ONLINE_H "
				+ "  (ID, PROCESS_ID, ACTIVE_ID, TOOL_ID, ONLINE_TIME,INFORMATION_DESCRIPTION,HISTORY_ID,GUID,LINK_URL)"
				+ "  SELECT T.ID, T.PROCESS_ID, T.ACTIVE_ID, T.TOOL_ID, T.ONLINE_TIME,T.INFORMATION_DESCRIPTION,"
				+ historyId + " HISTORY_ID," + JecnCommonSql.getSqlGUID() + ",LINK_URL  FROM JECN_ACTIVE_ONLINE_T T"
				+ "   WHERE T.PROCESS_ID = " + processId;
	}

	/**
	 * 控制点文控表 JECN_CONTROL_POINT_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnControlPointHistorySql(Long processId, Long historyId) {
		return "INSERT INTO JECN_CONTROL_POINT_H " + "  (ID," + "   ACTIVE_ID," + "   CONTROL_CODE," + "   KEY_POINT,"
				+ "   FREQUENCY," + "   METHOD," + "   TYPE," + "   RISK_ID," + "   TARGET_ID," + "   CREATE_PERSON,"
				+ "   CREATE_TIME," + "   UPDATE_PERSON," + "   UPDATE_TIME," + "   NOTE,HISTORY_ID,GUID)"
				+ "  SELECT T.ID," + "         T.ACTIVE_ID," + "         T.CONTROL_CODE," + "         T.KEY_POINT,"
				+ "         T.FREQUENCY," + "         T.METHOD," + "         T.TYPE," + "         T.RISK_ID,"
				+ "         T.TARGET_ID," + "         T.CREATE_PERSON," + "         T.CREATE_TIME,"
				+ "         T.UPDATE_PERSON," + "         T.UPDATE_TIME," + "         T.NOTE," + historyId
				+ " HISTORY_ID," + JecnCommonSql.getSqlGUID() + "    FROM JECN_CONTROL_POINT_T T"
				+ "   WHERE EXISTS (SELECT 1" + "            FROM JECN_FLOW_STRUCTURE_IMAGE_T IT"
				+ "           WHERE IT.FIGURE_ID = T.ACTIVE_ID" + "             AND IT.FLOW_ID = " + processId + ")";

	}

	/**
	 * JECN_ACTIVE_STANDARD_H 活动与标准关系文控表表
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnActiveStandardHistorySql(Long processId, Long historyId) {
		return "INSERT INTO JECN_ACTIVE_STANDARD_H "
				+ "  (ID, ACTIVE_ID, STANDARD_ID, RELA_TYPE, CLAUSE_ID,HISTORY_ID,GUID)"
				+ "  SELECT T.ID, T.ACTIVE_ID, T.STANDARD_ID, T.RELA_TYPE, T.CLAUSE_ID," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + "    FROM JECN_ACTIVE_STANDARD_T T" + "   WHERE EXISTS (SELECT 1"
				+ "            FROM JECN_FLOW_STRUCTURE_IMAGE_T IT" + "           WHERE IT.FIGURE_ID = T.ACTIVE_ID"
				+ "             AND IT.FLOW_ID = " + processId + ")";
	}

	/**
	 * to from 关联关系文控表 JECN_TO_FROM_ACTIVE_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnToFromActiveHistorySql(Long processId, Long historyId) {
		return "insert into " + "JECN_TO_FROM_ACTIVE_H(TO_FROM_FIGURE_ID, ACTIVE_FIGURE_ID, FLOW_ID,HISTORY_ID,GUID) "
				+ "select TO_FROM_FIGURE_ID, ACTIVE_FIGURE_ID, FLOW_ID," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + "  from JECN_TO_FROM_ACTIVE_T where flow_id=" + processId;
	}

	/**
	 * 相关风险 JECN_RELATE_RISK_HISTORY
	 * 
	 * @param relateId
	 * @param historyId
	 * @return sql: RELATE_TYPE ： 0 流程 1制度 2 文件
	 */
	private static String getJecnRuleRiskHistory(Long relateId, Long historyId) {
		return "INSERT INTO JECN_RELATE_RISK_HISTORY" + "(ID,RELATE_ID, RELATE_TYPE, RISK_ID,HISTORY_ID)" + "SELECT "
				+ JecnCommonSql.getSqlGUID() + ",FLOW_ID,0 RELATE_TYPE,RISK_ID, " + historyId + " HISTORY_ID"
				+ "   FROM JECN_FLOW_RELATED_RISK_T WHERE FLOW_ID=" + relateId;
	}

	/**
	 * 相关标准 JECN_STANDARD_HISTORY
	 * 
	 * @param relateId
	 * @param historyId
	 * @return RELATE_TYPE ： 0 流程 1制度 2 文件
	 */
	private static String getJecnStandardHistory(Long relateId, Long historyId) {
		return "INSERT INTO JECN_STANDARD_HISTORY " + "(ID,RELATE_ID,RELATE_TYPE,STANDARD_ID,HISTORY_ID)" + "SELECT "
				+ JecnCommonSql.getSqlGUID() + ",FLOW_ID,0  RELATE_TYPE,CRITERION_CLASS_ID, " + historyId
				+ " HISTORY_ID" + "  FROM JECN_FLOW_STANDARD_T WHERE FLOW_ID=" + relateId;
	}

	/**
	 * 流程适用部门 JECN_FLOW_APPLY_ORG_H
	 * 
	 * @param relateId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowApplyOrgHistorySql(Long processId, Long historyId) {
		return " INSERT INTO JECN_FLOW_APPLY_ORG_H (ID,RELATED_ID,ORG_ID,HISTORY_ID,GUID) "
				+ "SELECT ID,RELATED_ID,ORG_ID," + historyId + " HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ "  FROM JECN_FLOW_APPLY_ORG_T WHERE RELATED_ID = " + processId;
	}

	/**
	 * 流程驱动提醒人 JECN_FLOW_DRIVER_EMAIL_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowDriverEmailHistorySql(Long processId, Long historyId) {
		return " INSERT INTO JECN_FLOW_DRIVER_EMAIL_H (ID,RELATED_ID,PEOPLE_ID,HISTORY_ID,GUID) "
				+ "SELECT ID,RELATED_ID,PEOPLE_ID," + historyId + " HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ "  FROM JECN_FLOW_DRIVER_EMAIL_T WHERE RELATED_ID = " + processId;
	}

	/**
	 * JECN_FLOW_DRIVER_H 流程图时间驱动表
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowDriverHistorySql(Long processId, Long historyId) {
		return "insert into JECN_FLOW_DRIVER_H (FLOW_ID, FREQUENCY, START_TIME, END_TIME, QUANTITY,HISTORY_ID,GUID)"
				+ "SELECT FLOW_ID, FREQUENCY, START_TIME, END_TIME, QUANTITY," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + "   FROM JECN_FLOW_DRIVERT WHERE FLOW_ID=" + processId;
	}

	/**
	 * 流程文控信息
	 * 
	 * @param ruleId
	 * @param historyId
	 * @param baseDao
	 */
	public static void recordProcessHistory(Long processId, Long historyId, IBaseDao baseDao) {
		baseDao.execteNative(getJecnTempletHistorySql(processId, historyId));// 添加样例表数据
		baseDao.execteNative(getJecnModeFileHistorySql(processId, historyId));// 添加活动输出的数据
		baseDao.execteNative(getJecnActivityFileHistorySql(processId, historyId));// 活动输入和操作规范文控
		baseDao.execteNative(getJecnRefIndicatorstHistorySql(processId, historyId));// 活动指标
		baseDao.execteNative(getProcessStationRelatedHistorySql(processId, historyId));// 流程与岗位关系
		baseDao.execteNative(getJecnRoleActiveHistorySql(processId, historyId));// 活动角色关联
		baseDao.execteNative(getJecnFlowBasicInfo(processId, historyId));// 流程图基本信息
		baseDao.execteNative(getJecnFlowToolRelatedHistorySql(processId, historyId));// 流程图与支持工具关系
		baseDao.execteNative(getJecnFlowRecordHistorySql(processId, historyId));// 流程图操作说明记录
		baseDao.execteNative(getJecnFlowKpiNameHistorySql(processId, historyId));// 流程kpi
		baseDao.execteNative(getJecnFlowDriverHistorySql(processId, historyId));// 流程图时间驱动
		baseDao.execteNative(getJecnActiveOnlineHistorySql(processId, historyId));// 活动与信息化关系
		baseDao.execteNative(getJecnControlPointHistorySql(processId, historyId));// 控制点文控
		baseDao.execteNative(getJecnActiveStandardHistorySql(processId, historyId));// 活动与标准关系
		baseDao.execteNative(getJecnToFromActiveHistorySql(processId, historyId));// to-from关联关系
		baseDao.execteNative(getJecnRuleRiskHistory(processId, historyId));// 相关风险
		baseDao.execteNative(getJecnStandardHistory(processId, historyId));// 相关标准
		baseDao.execteNative(getJecnFlowApplyOrgHistorySql(processId, historyId));// 流程适用部门
		baseDao.execteNative(getJecnFlowDriverEmailHistorySql(processId, historyId));// 流程驱动提醒人
		baseDao.execteNative(getJecnFlowInoutHistorySql(processId, historyId));// 新版输入输出
		baseDao.execteNative(getJecnFlowInoutPosHistorySql(processId, historyId));// 和岗位岗位组关联关系表
		baseDao.execteNative(getJecnFigureInoutHistorySql(processId, historyId));// 新版活动输入输出
		baseDao.execteNative(getJecnFigureInoutSampleHistorySql(processId, historyId));// 输入输出样例关联表
		baseDao.execteNative(getJecnFlowDriverTimeHistorySql(processId, historyId));// 驱动规则时间表
	}

	private static String getJecnFlowDriverTimeHistorySql(Long processId, Long historyId) {
		String sql = "INSERT INTO JECN_FLOW_DRIVER_TIME_H(ID,FLOW_ID,MONTH,WEEK,DAY,HOUR,HISTORY_ID,GUID)"
				+ " SELECT ID,FLOW_ID,MONTH,WEEK,DAY,HOUR," + historyId + " HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ " FROM JECN_FLOW_DRIVER_TIME_T WHERE FLOW_ID=" + processId;
		return sql;
	}

	private static String getJecnFigureInoutSampleHistorySql(Long processId, Long historyId) {
		String sql = "INSERT INTO JECN_FIGURE_IN_OUT_SAMPLE_H(ID,FLOW_ID,IN_OUT_ID,FILE_ID,HISTORY_ID,GUID,TYPE)"
				+ " SELECT JFIOS.ID,JFIOS.FLOW_ID,JFIOS.IN_OUT_ID,JFIOS.FILE_ID," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + ",JFIOS.TYPE FROM JECN_FIGURE_IN_OUT_SAMPLE_T JFIOS"
				+ " INNER JOIN JECN_FIGURE_IN_OUT_T JFIO ON JFIOS.IN_OUT_ID=JFIO.ID"
				+ " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_T JFSI ON JFSI.FIGURE_ID=JFIO.FIGURE_ID AND JFSI.FLOW_ID="
				+ processId;
		return sql;
	}

	private static String getJecnFigureInoutHistorySql(Long processId, Long historyId) {
		String sql = " INSERT INTO JECN_FIGURE_IN_OUT_H(ID,FLOW_ID,FIGURE_ID,TYPE,NAME,EXPLAIN,FILE_ID,SORT_ID,HISTORY_ID,GUID)"
				+ " SELECT JFIO.ID,JFIO.FLOW_ID,JFIO.FIGURE_ID,JFIO.TYPE,JFIO.NAME,JFIO.EXPLAIN,JFIO.FILE_ID,JFIO.SORT_ID,"
				+ historyId
				+ " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID()
				+ " FROM JECN_FIGURE_IN_OUT_T JFIO"
				+ " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_T JFSI ON JFSI.FIGURE_ID=JFIO.FIGURE_ID AND JFSI.FLOW_ID="
				+ processId;
		return sql;
	}

	private static String getJecnFlowInoutPosHistorySql(Long processId, Long historyId) {
		String sql = "INSERT INTO JECN_FLOW_IN_OUT_POS_H(ID,FLOW_ID,IN_OUT_ID,R_ID,R_TYPE,HISTORY_ID,GUID)"
				+ " SELECT JFIOP.ID,JFIOP.FLOW_ID,JFIOP.IN_OUT_ID,JFIOP.R_ID,JFIOP.R_TYPE," + historyId
				+ " HISTORY_ID," + JecnCommonSql.getSqlGUID() + " FROM JECN_FLOW_IN_OUT_POS_T JFIOP"
				+ " INNER JOIN JECN_FLOW_IN_OUT_T JFIO ON JFIOP.IN_OUT_ID=JFIO.ID AND JFIO.FLOW_ID=" + processId;
		return sql;
	}

	private static String getJecnFlowInoutHistorySql(Long processId, Long historyId) {
		String sql = "INSERT INTO JECN_FLOW_IN_OUT_H(ID,FLOW_ID,TYPE,NAME,EXPLAIN,POS,SORT_ID,HISTORY_ID,GUID)"
				+ " SELECT ID,FLOW_ID,TYPE,NAME,EXPLAIN,POS,SORT_ID," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + " FROM JECN_FLOW_IN_OUT_T WHERE FLOW_ID=" + processId;
		return sql;
	}
}
