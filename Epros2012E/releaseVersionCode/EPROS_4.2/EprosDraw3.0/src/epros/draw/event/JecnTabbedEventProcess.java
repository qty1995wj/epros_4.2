package epros.draw.event;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.event.flowElement.JecnFlowElementEventProcess;
import epros.draw.event.flowElement.MapManLineCommon;
import epros.draw.gui.box.JecnToolBoxMainPanel;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnAdjacentFigure;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.figure.shape.DateFreeText;
import epros.draw.gui.figure.shape.JecnTempFigurePanel;
import epros.draw.gui.figure.shape.MapNameText;
import epros.draw.gui.figure.shape.part.ExpertRhombusAR;
import epros.draw.gui.figure.shape.part.ReverseArrowhead;
import epros.draw.gui.figure.unit.JecnFlowElementCopyAndPaste;
import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.CommentLine;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnSelectAreaPanel;
import epros.draw.gui.workflow.popup.JecnFlowElemPopupMenu;
import epros.draw.gui.workflow.popup.JecnWorkflowPopupMenu;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitleButton;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 画图面板以及相关容器的事件类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTabbedEventProcess {
	/** 鼠标移动到面板生成的待添加流程元素图形 */
	public static JecnBaseFlowElementPanel tempFlowElementPanel = null;
	/** 获取片段对应的所有图形 */
	public static List<JecnBaseFlowElementPanel> flowElementPanelCloneList = null;

	/** 创建虚拟边框panel */
	public static JecnTempFigurePanel tempFigurePanel = null;

	public static Point oldPoint = null;

	/** 鼠标左键操作 : 分割线移动时，获取不到鼠标左键和右键，只有在点击和释放的时候能获取 */
	private static boolean isButton1;

	/**
	 * 
	 * 面板鼠标进入面板事件处理
	 * 
	 * @param e
	 */
	public static void mouseeEntered(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || desktopPane.getJecnTempFigurePanel() != null) {
			return;
		}
		// boolean true:流程元素库状态为连接线，false ：状态不是连接线，为其它流程元素;
		boolean isManLine = isManLine();

		// 是否选中格式刷
		if (desktopPane.getFormatPainterData() != null) {
			String path = JecnFileUtil.IMAGES_PATH_PREFIX + "toolbar/formatPainter-B.png"; // 储存鼠标图片的位置
			URL classUrl = JecnResourceUtil.class.getResource(path);
			Image image = Toolkit.getDefaultToolkit().getImage(classUrl);
			Toolkit tk = Toolkit.getDefaultToolkit();
			Cursor cursor = tk.createCustomCursor(image, new Point(10, 10), "norm");
			desktopPane.setCursor(cursor); // panel 也可以是其他组件
		}

		if (!isManLine && desktopPane.getTemplateId() == null) {// 如果流程元素库属性不是连接线
			// 获取流程元素对象
			tempFlowElementPanel = getJecnBaseFlowElementPanel();
			if (tempFlowElementPanel == null) {
				return;
			}
			if (tempFlowElementPanel instanceof ReverseArrowhead) {
				tempFlowElementPanel.getFlowElementData().setCurrentAngle(180);
			}
			// 添加流程元素虚拟图形
			addTempFlowElementPanel(e);
			return;
		} else if (desktopPane.getTemplateId() != null && !"".equals(desktopPane.getTemplateId().trim())) {// 添加片段
			// 获取导入模板文件
			JecnTemplateData template = JecnFlowTemplate.importTemplateXml(desktopPane.getTemplateId(), desktopPane
					.getFlowMapData().getMapType());

			if (template == null || template.getSelectWidth() == null || "".equals(template.getSelectWidth().trim())
					|| template.getSelectHeight() == null || "".equals(template.getSelectHeight().trim())) {
				return;
			}
			// 获取片段对应的所有图形
			flowElementPanelCloneList = JecnFlowTemplate.getAllCloneFlowElementByTemplate(template);

			if (flowElementPanelCloneList == null || flowElementPanelCloneList.size() == 0) {
				return;
			}
			if (desktopPane.getJecnTempFigurePanel() == null) {
				tempFigurePanel = new JecnTempFigurePanel();
				desktopPane.setJecnTempFigurePanel(tempFigurePanel);
			} else {
				tempFigurePanel = desktopPane.getJecnTempFigurePanel();
			}

			// 获取当前放大缩小倍数下的虚拟边框宽度和高度
			int tempWidth = DrawCommon.convertDoubleToInt(Integer.valueOf(template.getSelectWidth())
					* desktopPane.getWorkflowScale());

			int tempHeight = DrawCommon.convertDoubleToInt(Integer.valueOf(template.getSelectHeight())
					* desktopPane.getWorkflowScale());
			// 获取当前放大缩小倍数下的虚拟边框位置点
			int oldX = DrawCommon.convertDoubleToInt(template.getTemplatePoint().x * desktopPane.getWorkflowScale()
					- tempWidth / 2.0);

			int oldY = DrawCommon.convertDoubleToInt(template.getTemplatePoint().y * desktopPane.getWorkflowScale()
					- tempHeight / 2.0);

			Point location = new Point(oldX, oldY);
			oldPoint = location;
			// 设置虚拟边框panel位置点大小
			tempFigurePanel.setBounds(location, tempWidth, tempHeight);

			// 添加虚拟图形到面板
			desktopPane.add(tempFigurePanel);
			// 设置虚拟图形相对面板的层级
			desktopPane.setLayer(tempFigurePanel, desktopPane.getMaxZOrderIndex() + 1);

		}
	}

	/**
	 * 
	 * 鼠标在面板上移动
	 * 
	 * @param e
	 */
	public static void mouseMoved(MouseEvent e) {
		// boolean true:流程元素库状态为连接线，false ：状态不是连接线，为其它流程元素;
		boolean isManLine = isManLine();
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		if (isManLine) {// 连接线- 移动特殊处理
			JecnPaintFigureUnit.selectMoveInDesk(e, null);
		}
		if (!isManLine && desktopPane.getTemplateId() == null) {// 如果流程元素库属性不是连接线
			addTempFlowElementPanel(e);
		} else if (desktopPane.getJecnTempFigurePanel() != null && flowElementPanelCloneList != null
				&& flowElementPanelCloneList.size() > 0) {// 添加片段面板鼠标移动
			// 鼠标相对图形的中心点
			Point centerPoint = getCenterPoint(e.getPoint(), desktopPane.getJecnTempFigurePanel().getWidth(),
					desktopPane.getJecnTempFigurePanel().getHeight());

			// 鼠标移动虚拟图形位置点到达边界处理
			mouseToBound(centerPoint);

			// 选中流程元素集合处理
			for (JecnBaseFlowElementPanel curFigurePanel : flowElementPanelCloneList) {
				if (curFigurePanel instanceof JecnBaseFigurePanel) {// 是否为图形
					JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) curFigurePanel;

					JecnTempFigureBean tempFigureBean = curFigure.getTempFigureBean();

					tempFigureBean.setCurrentAngle(curFigure.getFlowElementData().getCurrentAngle());
					// 放大缩小后的图形大小和位置点
					JecnFigureDataCloneable currFigureDataCloneable = curFigure.getFlowElementData()
							.getFigureDataCloneable().getZoomFigureDataProcess(
									JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
					// 获取图形的位置点
					int tempPointX = currFigureDataCloneable.getX();
					int tempPointY = currFigureDataCloneable.getY();
					// oldPoint当前面板下虚框位置点
					Point localPoint = new Point(tempPointX - oldPoint.x + centerPoint.x, tempPointY - oldPoint.y
							+ centerPoint.y);
					// 设置图形位置点和大小
					tempFigureBean.reSetTempFigureBean(localPoint, currFigureDataCloneable.getWidth(),
							currFigureDataCloneable.getHeight());

					if (!tempFigurePanel.getTempFigureList().contains(tempFigureBean)) {// 记录选中的所有图形
						tempFigurePanel.getTempFigureList().add(tempFigureBean);
					}
				} else if (curFigurePanel instanceof JecnBaseDividingLinePanel) {
					JecnBaseDividingLinePanel curDiviLine = (JecnBaseDividingLinePanel) curFigurePanel;

					JecnTempFigureBean tempFigureBean = curDiviLine.getTempFigureBean();

					Point startPoint = curDiviLine.getStartPoint();
					Point endPoint = curDiviLine.getEndPoint();

					int width = 0;
					int height = 0;

					if (curDiviLine.getFlowElementData().getMapElemType().equals(MapElemType.HDividingLine)) {// 横线
						width = Math.abs(startPoint.x - endPoint.x);
						// 线条宽度
						height = curDiviLine.getFlowElementData().getBodyWidth();
					} else {
						height = Math.abs(startPoint.y - endPoint.y);

						// 线条宽度
						width = curDiviLine.getFlowElementData().getBodyWidth();
					}

					// 获取图形的位置点
					int tempPointX = Math.min(startPoint.x, endPoint.x);
					int tempPointY = Math.min(startPoint.y, endPoint.y);

					Point localPoint = new Point(tempPointX - oldPoint.x + centerPoint.x, tempPointY - oldPoint.y
							+ centerPoint.y);
					// 设置图形位置点和大小
					tempFigureBean.reSetTempFigureBean(localPoint, width, height);
					if (!tempFigurePanel.getTempFigureList().contains(tempFigureBean)) {// 记录选中的所有图形
						tempFigurePanel.getTempFigureList().add(tempFigureBean);
					}
				}
			}
			// 设置虚拟边框位置点
			desktopPane.getJecnTempFigurePanel().setLocation(centerPoint);
			JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
		}
	}

	/**
	 * 
	 * 获取流程元素库状态
	 * 
	 * @return boolean true:流程元素库状态为连接线，false ：状态不是连接线，为其它流程元素;
	 */
	public static boolean isManLine() {
		// 获取流程元素库状态
		MapElemType selectedBtnType = JecnDrawMainPanel.getMainPanel().getBoxPanel().getSelectedBtnType();
		if (MapElemType.ManhattanLine.equals(selectedBtnType) || MapElemType.CommonLine.equals(selectedBtnType)) {// 如果流程元素库属性不是连接线
			return true;
		}
		return false;
	}

	/**
	 * 添加连接线，不依赖图形
	 * 
	 * @return
	 */
	public static boolean isShowLineNoFigure() {
		return isManLine() && JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure();
	}

	/**
	 * 
	 * 
	 * 画图面板的按下操作 (鼠标点击面板时间处理)
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void drawDesktopPaneMousePressed(MouseEvent e) {
		if (e == null || !(e.getSource() instanceof JecnDrawDesktopPane)) {// 参数为空，或不是画图面板释放事件
			return;
		}
		// 画图面板
		JecnDrawDesktopPane drawDesktopPane = (JecnDrawDesktopPane) e.getSource();
		// 键盘事件未执行完，切换选中元素，处理键盘释放事件
		drawDesktopPane.getKeyHandler().keyReleased();
		// 获取焦点
		if (!drawDesktopPane.isFocusOwner()) {
			drawDesktopPane.requestFocusInWindow();
		}

		// **************清除提示信息**************//
		// 解决活动提示信息消除不了情况
		ToolTipManager.sharedInstance().mouseExited(e);
		// **************清除提示信息**************//

		// **************清除快捷添加图形显示小三角形**************//
		// 清除快捷添加图形显示小三角形
		JecnDrawMainPanel.getMainPanel().interruptLinkFigure();
		// **************清除快捷添加图形显示小三角形**************//

		// **************清除圈选框**************//
		JecnSelectAreaPanel selectPanel = drawDesktopPane.getAidCompCollection().getSelecedAreaPanel();
		selectPanel.setVisibleFalse();
		// **************清除圈选框**************//

		// **************清空临时线段**************//
		JecnWorkflowUtil.disposeLines();
		// 临时小线段集合，比如拖动过程中生成线段，拖动结束后清除该临时线段
		JecnWorkflowUtil.disposeAllTempLine();

		// **************清空临时线段**************//

		drawDesktopPane.setStartPoint(e.getPoint());
		// 鼠标点击
		isButton1 = e.getButton() == MouseEvent.BUTTON1 ? true : false;
		if (MapElemType.DividingLine.equals(JecnDrawMainPanel.getMainPanel().getBoxPanel().getSelectedBtnType())
				|| isManLine()) {// 工具栏点击分割线
			// 临时线段记录开始点
			drawDesktopPane.setStartPoint(e.getPoint());
		} else {
			// 显示选中框
			drawDesktopPane.getAidCompCollection().getSelecedAreaPanel().setStartPoint(e.getPoint());
			// drawDesktopPane.getAidCompCollection().getSelecedAreaPanel()
			// .setEndPoint(null);
		}
		// 删除面板虚拟线段，清空线段对应的图形
		JecnAdjacentFigure.removeTempLine();
		// 是否按住Ctrl true：按住Ctrl false：为按住
		boolean isCtrl = JecnWorkflowUtil.isCtrl(e);
		if (isCtrl) {
			return;
		}
		// 清空面板上所有元素的选中点
		JecnWorkflowUtil.hideAllResizeHandle();
	}

	/**
	 * 
	 * 
	 * 画图面板的释放操作
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public static void drawDesktopPaneMouseReleased(MouseEvent e) {
		if (e == null || !(e.getSource() instanceof JecnDrawDesktopPane)) {// 参数为空，或不是画图面板释放事件
			return;
		}
		// 画图面板
		JecnDrawDesktopPane workflow = (JecnDrawDesktopPane) e.getSource();

		// 流程地图画线
		if (isShowLineNoFigure()) {
			// 流程架构，面板添加连接线
			boolean isManLine = MapManLineCommon.INSTANCE.addManLine(e);
			if (isManLine) {
				return;
			}
		}
		// ************************1添加图形************************//
		if (addFlowElementPanel(e)) {// 执行添加模型
			return;
		}

		// ************************2添加模型************************//
		if (workflow.getTemplateId() != null && workflow.getJecnTempFigurePanel() != null && addTemplatePanel(e)) {// 执行添加模型
			return;
		}

		// 添加完成后，选中指针
		JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();

		// ************************3圈选************************//
		// 鼠标圈选处理
		boolean seletecdArea = JecnWorkflowUtil.getSelectAreaPanels();
		// **************清除圈选框**************//
		JecnSelectAreaPanel selectPanel = workflow.getAidCompCollection().getSelecedAreaPanel();
		selectPanel.setVisibleFalse();

		workflow.setSelectArea(seletecdArea);

		if (seletecdArea) {// 圈选
			JecnFlowElementCopyAndPaste.multipleFormatPainterFigureAttrs(workflow.getCurrentSelectElement());
			// **************清除圈选框**************//
			workflow.getCurrDrawPanelSizeByPanelList();
			return;
		}

		// 清除格式刷状态
		JecnWorkflowUtil.clearFormatPainter();

		if (e.getButton() == MouseEvent.BUTTON1 || e.getButton() == MouseEvent.BUTTON3 && e.isPopupTrigger()) {// 鼠标点击左键
			if (isMousePressedLine(e.getPoint(), e)) {
				return;
			}
		}

		// ************************4右键菜单************************//
		if (e.getButton() == MouseEvent.BUTTON3 && e.isPopupTrigger()) {// 右键菜单
			// 画图面板类型
			MapType mapType = workflow.getFlowMapData().getMapType();
			// 弹出确认对话框
			JecnWorkflowPopupMenu workflowPopupMenu = null;
			switch (mapType) {
			case partMap:// 流程图
				workflowPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapWorkflowPopupMenu();
				break;
			case totalMap:// 流程地图
			case totalMapRelation:// 集成关系图
				workflowPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapWorkflowPopupMenu();
				break;
			case orgMap:// 组织图
				workflowPopupMenu = JecnDrawMainPanel.getMainPanel().getOrgMapWorkflowPopupMenu();
				break;
			default:
				return;
			}

			// 判断角色移动是否选中
			workflowPopupMenu.setRoleMoveIcon();

			// 设计器使用：菜单项隐藏/显示实现方法
			JecnDesignerProcess.getDesignerProcess().beforeWorkflowPopupShow();

			// 记录右键菜单点击图形相关点
			JecnFlowElementCopyAndPaste.setEditPastePoint(e.getPoint());
			// 面板右键菜单
			workflowPopupMenu.getWorkflowPopupMenu().show(JecnDrawMainPanel.getMainPanel().getWorkflow(), e.getX(),
					e.getY());
		} else {
			JecnFlowElementCopyAndPaste.setEditPastePoint(null);
		}
	}

	/**
	 * 连接线选中判断，非ctrl时，清空选重点
	 * 
	 * @param e
	 */
	private static void isCtrlHideAllResizeHandle(MouseEvent e) {
		boolean isCtrl = JecnWorkflowUtil.isCtrl(e);
		if (!isCtrl) {
			JecnWorkflowUtil.hideAllResizeHandle();
		}
	}

	/**
	 * 添加元素
	 * 
	 * @Title: addFlowElementPanel
	 * @Description: TODO
	 * @param @param e
	 * @return void
	 * @author ZXH
	 * @date 2015-9-9 下午04:40:43
	 * @throws
	 */
	private static boolean addFlowElementPanel(MouseEvent e) {
		if (tempFlowElementPanel != null) {
			if (e.getButton() == MouseEvent.BUTTON1) {// 左键添加图形
				// 添加流程元素
				boolean isAdd = addFlowElement(e.getPoint());
				if (isAdd) {// 未执行添加流程元素
					cancleAddFigure();
					JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
					return true;
				}
			} else if (e.getButton() == MouseEvent.BUTTON3) {// 右键取消条件
				cancleAddFigure();
				return true;
			}
		}
		return false;
	}

	/**
	 * 添加模型
	 * 
	 * @Description: TODO
	 * @param @param e
	 * @return boolean true:执行添加（包含取消添加）,false:未执行添加
	 * @author ZXH
	 * @date 2015-9-9 下午04:40:43
	 * @throws
	 */
	private static boolean addTemplatePanel(MouseEvent e) {
		if (tempFigurePanel != null) {
			if (e.getButton() == MouseEvent.BUTTON1) {// 左键添加图形
				boolean isTemplate = addTemplatePanel();
				if (isTemplate) {
					cancleAddFigure();
					return true;
				}
			} else if (e.getButton() == MouseEvent.BUTTON3) {// 右键取消条件
				cancleAddFigure();
				return true;
			}
		}
		return false;
	}

	public static void cancleAddFigure() {
		// 添加完成后，选中指针
		JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();
		// **************清除圈选框**************//
		JecnSelectAreaPanel selectPanel = JecnDrawMainPanel.getMainPanel().getWorkflow().getAidCompCollection()
				.getSelecedAreaPanel();
		selectPanel.setVisibleFalse();
		// **************清除圈选框**************//
	}

	/**
	 * 
	 * 面板添加片段
	 * 
	 * @return boolean true:添加片段；false 未执行添加片段
	 */
	public static boolean addTemplatePanel() {
		// 画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || desktopPane.getTemplateId() == null || desktopPane.getJecnTempFigurePanel() == null) {
			return false;
		}
		// 添加片段数据到面板
		JecnFlowTemplate.addTemplateToWorkflow(desktopPane.getTemplateId(), desktopPane.getJecnTempFigurePanel()
				.getLocation());
		desktopPane.getCurrDrawPanelSizeByPanelList();
		return true;
	}

	/**
	 * 添加流程元素虚拟图形
	 * 
	 * @param e
	 *            MouseEvent鼠标事件
	 */
	public static void addTempFlowElementPanel(MouseEvent e) {
		if (tempFlowElementPanel == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 鼠标点击面板的点
		Point ePoint = e.getPoint();
		// 获取流程元素高度和宽度
		int figureHeight = DrawCommon.convertDoubleToInt(tempFlowElementPanel.getFlowElementData().getFigureSizeY()
				* desktopPane.getWorkflowScale());
		int figureWidth = DrawCommon.convertDoubleToInt(tempFlowElementPanel.getFlowElementData().getFigureSizeX()
				* desktopPane.getWorkflowScale());

		// 鼠标相对图形的中心点
		Point centerPoint = getCenterPoint(ePoint, figureWidth, figureHeight);
		// 鼠标移动虚拟图形位置点到达边界处理
		mouseToBound(centerPoint);
		if (tempFlowElementPanel instanceof JecnBaseFigurePanel) {
			JecnBaseFigurePanel baseFigurePanel = (JecnBaseFigurePanel) tempFlowElementPanel;
			addTempFigurePanel(baseFigurePanel.getTempFigureBean(), centerPoint, figureWidth, figureHeight);
			// 拖动单个虚拟图形边框生成相邻图形间的对齐虚拟直线
			JecnFlowElementEventProcess.drawTempFigureLine(baseFigurePanel);
		} else if (tempFlowElementPanel instanceof DividingLine) {// 如果添加的是分割线，记录分割线鼠标点击和释放的坐标值
			DividingLine dividingLine = (DividingLine) tempFlowElementPanel;
			// 记录分割线100%状态下的克隆值
		} else if (tempFlowElementPanel instanceof HDividingLine) {
			HDividingLine linePanel = (HDividingLine) tempFlowElementPanel;
			addTempFigurePanel(linePanel.getTempFigureBean(), centerPoint, figureWidth, figureHeight);
		} else if (tempFlowElementPanel instanceof VDividingLine) {
			VDividingLine linePanel = (VDividingLine) tempFlowElementPanel;
			addTempFigurePanel(linePanel.getTempFigureBean(), centerPoint, figureWidth, figureHeight);
		} else if (tempFlowElementPanel instanceof JecnBaseVHLinePanel) {
			JecnBaseVHLinePanel linePanel = (JecnBaseVHLinePanel) tempFlowElementPanel;
			if (linePanel.getFlowElementData().getMapElemType().equals(MapElemType.ParallelLines)) {
				figureWidth = JecnDrawMainPanel.getMainPanel().getWorkflow().getWidth();
				centerPoint = new Point(0, centerPoint.y);
			} else {
				figureHeight = JecnDrawMainPanel.getMainPanel().getWorkflow().getHeight();
				centerPoint = new Point(centerPoint.x, 0);
			}
			addTempFigurePanel(linePanel.getTempFigureBean(), centerPoint, figureWidth, figureHeight);
		}
		// 面板放大，滚动条跟随移动
		// JecnFlowElementEventProcess.reSetWorkFlowSizeAndScroolFollow(desktopPane.getJecnTempFigurePanel());
		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}

	/**
	 * 鼠标移动虚拟图形位置点到达边界处理
	 * 
	 * @param localPoint
	 */
	public static void mouseToBound(Point localPoint) {
		if (localPoint == null) {
			return;
		}
		if (localPoint.x <= 0) {
			localPoint.x = 0;
		}
		if (localPoint.y <= 0) {
			localPoint.y = 0;
		}
	}

	/**
	 * 面板添加流程元素虚拟边框(单个流程元素)
	 * 
	 * @param tempFigurePanel
	 *            JecnTempFigurePanel流程元素虚拟边框图形
	 * @param centerPoint
	 *            鼠标相对虚拟边框图形的中心点
	 * @param figureWidth
	 *            虚拟边框宽度
	 * @param figureHeight
	 *            虚拟边框高度
	 */
	private static void addTempFigurePanel(JecnTempFigureBean tempFigureBean, Point centerPoint, int figureWidth,
			int figureHeight) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane.getJecnTempFigurePanel() == null) {// 如果不存虚拟图形
			// 获取鼠标点击流程元素库的虚拟图形
			desktopPane.setJecnTempFigurePanel(new JecnTempFigurePanel());
			desktopPane.getJecnTempFigurePanel().getTempFigureList().add(tempFigureBean);
		}
		// 设置虚拟图形位置、大小
		desktopPane.getJecnTempFigurePanel().setBounds(centerPoint, figureWidth, figureHeight);

		// 添加图形时图形位置点和填充边框位置点相同
		tempFigureBean.setLocalX(centerPoint.x);
		tempFigureBean.setLocalY(centerPoint.y);
		tempFigureBean.setUserWidth(figureWidth);
		tempFigureBean.setUserHeight(figureHeight);

		// 添加虚拟图形到面板
		desktopPane.add(desktopPane.getJecnTempFigurePanel());

		desktopPane.setLayer(desktopPane.getJecnTempFigurePanel(), desktopPane.getMaxZOrderIndex() + 10);
	}

	/**
	 * 获取鼠标相对图形中心点
	 * 
	 * @param mousePoint
	 *            鼠标位置
	 * @param figureWidth
	 *            图形宽
	 * @param figureHeight
	 *            图形高
	 * @return Point 鼠标相对图形的中心点
	 */
	public static Point getCenterPoint(Point mousePoint, int figureWidth, int figureHeight) {
		if (mousePoint == null) {
			return null;
		}
		return new Point(mousePoint.x - figureWidth / 2, mousePoint.y - figureHeight / 2);
	}

	/**
	 * 删除拖动生成的虚拟图形
	 * 
	 */
	public static void removeTempFigure() {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 记录片段的ID
		desktopPane.setTemplateId(null);

		if (flowElementPanelCloneList != null) {// 获取片段对应的所有图形
			flowElementPanelCloneList.clear();
		}

		if (desktopPane.getJecnTempFigurePanel() != null) {
			desktopPane.getJecnTempFigurePanel().getTempFigureList().clear();
			desktopPane.remove(desktopPane.getJecnTempFigurePanel());
			desktopPane.setJecnTempFigurePanel(null);
			tempFlowElementPanel = null;
			oldPoint = null;
		}
	}

	/**
	 * 鼠标点击面板 判断是否在线段区域内
	 * 
	 * @param ePoint
	 *            Point鼠标相对面板的坐标点
	 */
	public static boolean isMousePressedLine(Point ePoint, MouseEvent e) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {// 参数为空，或不是画图面板释放事件
			return false;
		}
		// 画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 鼠标是否点击连接线
		for (JecnBaseDividingLinePanel dividingLine : desktopPane.getDiviLineList()) {
			if (dividingLine.lineContainsPoint(ePoint.x, ePoint.y)) {// 鼠标是否点击连接线
				isCtrlHideAllResizeHandle(e);
				// 点击图形处理
				JecnWorkflowUtil.setCurrentSelect(dividingLine);
				if (desktopPane.getCurrentSelectElement().size() == 1) {
					// 修改工具栏的状态
					JecnElementAttributesUtil.updateEditTool(dividingLine);
					// 是否点击右键菜单
					rightPopupMenuProcess(e, dividingLine);
				}
				return true;
			}
		}
		// 鼠标是否点击连接线
		for (JecnBaseVHLinePanel baseVHLinePanel : desktopPane.getVhLinePanelList()) {
			if (!baseVHLinePanel.isVisible()) {// 如果分割线未显示 返回false
				continue;
			}
			if (baseVHLinePanel.lineContainsPoint(ePoint.x, ePoint.y)) {// 鼠标是否点击连接线
				isCtrlHideAllResizeHandle(e);
				JecnWorkflowUtil.hideAllResizeHandle();
				// 点击图形处理
				JecnWorkflowUtil.setCurrentSelect(baseVHLinePanel);
				if (desktopPane.getCurrentSelectElement().size() == 1) {
					// 修改工具栏的状态
					JecnElementAttributesUtil.updateEditTool(baseVHLinePanel);
					// 是否点击右键菜单
					rightPopupMenuProcess(e, baseVHLinePanel);
				}
				return true;
			}
		}
		// 连接线判断
		for (JecnBaseManhattanLinePanel baseLine : desktopPane.getManLineList()) {
			if (baseLine.lineContainsPoint(ePoint.x, ePoint.y)) {
				isCtrlHideAllResizeHandle(e);
				// 点击图形处理
				JecnWorkflowUtil.setCurrentSelect(baseLine);

				// 左键点击且图形点击次数为2
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
					baseLine.doubleClick();
				}
				if (desktopPane.getCurrentSelectElement().size() == 1) {
					// 修改工具栏的状态
					JecnElementAttributesUtil.updateEditTool(baseLine);
					// 是否点击右键菜单
					rightPopupMenuProcess(e, baseLine);
				}
				return true;
			}
		}
		for (CommentLine commentLine : desktopPane.getCommentLineList()) {// 鼠标点击的是否为注释框小线段
			if (commentLine.lineContainsPoint(ePoint.x, ePoint.y)) {// 鼠标是否点击注释框小线段
				isCtrlHideAllResizeHandle(e);
				// 设置选中图形
				JecnWorkflowUtil.setCurrentSelect(commentLine.getCommentText());
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * 执行菜单操作 点击线段外制定区域 弹出右键餐单
	 * 
	 * @param e
	 *            MouseEvent 鼠标动作
	 * @return boolean 执行菜单操作返回true ，否则返回false
	 */
	public static boolean rightPopupMenuProcess(MouseEvent e, JecnBaseFlowElementPanel flowElementPanel) {
		if (e.getButton() == MouseEvent.BUTTON3
		// && e.isPopupTrigger()
		) {// 流程元素右键操作且此鼠标事件是否为该平台的弹出菜单触发事件
			// 获取面板
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			// 画图面板类型
			MapType mapType = desktopPane.getFlowMapData().getMapType();

			JecnFlowElemPopupMenu flowElemPopupMenu = null;
			switch (mapType) {
			case partMap:// 流程图
				flowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
				break;
			case totalMap:// 流程地图
				flowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
				break;
			case orgMap:// 组织图
				flowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getOrgMapFlowElemPopupMenu();
				break;
			default:
				return true;
			}

			// 当前点击流程元素传递到弹出菜单对象中
			flowElemPopupMenu.setElementPanel(flowElementPanel);

			// 设计器使用：菜单项隐藏/显示实现方法
			JecnDesignerProcess.getDesignerProcess().beforeFlowElemPopupShow(flowElementPanel);
			// 显示弹出菜单
			flowElemPopupMenu.getFlowElemPopupMenu().show(desktopPane, e.getX(), e.getY());
			return true;
		}
		return false;
	}

	/**
	 * 设置角色，活动特有菜单项是否置灰
	 * 
	 * @param flowElementPanel
	 * @param flowElemPopupMenu
	 */
	public static void setActiveAndRoleMenuItemEnable(JecnBaseFlowElementPanel flowElementPanel,
			JecnFlowElemPopupMenu flowElemPopupMenu) {
		// 如果图形属于活动
		if (flowElementPanel instanceof JecnBaseActiveFigure && !(flowElementPanel instanceof ExpertRhombusAR)) {
			flowElemPopupMenu.getRoundRectMenuItem().setEnabled(true);
			flowElemPopupMenu.getAntoNumMenuItem().setEnabled(true);
		} else {
			flowElemPopupMenu.getRoundRectMenuItem().setEnabled(false);
			flowElemPopupMenu.getAntoNumMenuItem().setEnabled(false);
		}
		if (flowElementPanel instanceof JecnBaseRoleFigure) {// 角色
			// 角色明细
			flowElemPopupMenu.setEnableRoleItem(true);
		} else {
			// 角色明细
			flowElemPopupMenu.setEnableRoleItem(false);
		}
	}

	/**
	 * 
	 * 
	 * 画图面板的标题面板的关闭按钮鼠标移动事件
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void tabbedTitleButtonMouseMove(MouseEvent e) {
		if (e == null) {
			return;
		}
		if (e.getSource() instanceof JecnTabbedTitleButton) {// 画图面板的分页标题面板
			JecnTabbedTitleButton tabbedTitleButtn = (JecnTabbedTitleButton) e.getSource();
			if (tabbedTitleButtn.getIcon() != JecnWorkflowUtil.getTabSelectedCloseBtnIcon()) {// 按钮图标不是给定图片
				tabbedTitleButtn.setIcon(JecnWorkflowUtil.getTabSelectedCloseBtnIcon());
			}
		}
	}

	/**
	 * 
	 * 
	 * 画图面板的标题面板的关闭按钮鼠标退出事件
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void tabbedTitleButtonMouseExit(MouseEvent e) {
		if (e == null) {
			return;
		}
		if (e.getSource() instanceof JecnTabbedTitleButton) {// 画图面板的分页标题面板
			JecnTabbedTitleButton tabbedTitleButtn = (JecnTabbedTitleButton) e.getSource();
			tabbedTitleButtn.setIcon(JecnWorkflowUtil.getTabCloseBtnIcon());
		}
	}

	/**
	 * 
	 * 
	 * 画图面板鼠标退出事件
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void workflowMouseExit(MouseEvent e) {
		if (e != null && e.getSource() instanceof JecnDrawDesktopPane) {
			// 画图面板
			JecnDrawDesktopPane workflow = (JecnDrawDesktopPane) e.getSource();
			boolean flag = workflow.contains(e.getPoint());
			if (!flag) {// 清除快捷添加图形显示小三角形*
				// 清除快捷添加图形显示小三角形
				JecnDrawMainPanel.getMainPanel().interruptLinkFigure();
			}
		}
	}

	/**
	 * 
	 * 
	 * 画图面板的释放事件
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void tabbedTitleButtonMouseReleased(MouseEvent e) {
		if (e == null) {
			return;
		}
		if (e.getSource() instanceof JecnTabbedTitleButton) {// 画图面板的分页标题面板
			if (e.getButton() == MouseEvent.BUTTON3) {
				// 源按钮
				JecnTabbedTitleButton sourceBtn = (JecnTabbedTitleButton) e.getSource();
				// 目标按钮
				MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(sourceBtn, e, JecnDrawMainPanel
						.getMainPanel().getTabbedPane());
				// 触发给定事件
				JecnDrawMainPanel.getMainPanel().getTabbedPane().dispatchEvent(desMouseEvent);
			}
		}
	}

	/**
	 * 
	 * 添加流程元素
	 * 
	 * @param ePoint
	 *            Point鼠标相对面板的位置点
	 * @return boolean true：添加流程元素成功；false：添加流程元素失败
	 */
	public static boolean addFlowElement(Point ePoint) {
		// 画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

		if (ePoint == null || desktopPane == null || tempFlowElementPanel == null) {
			return false;
		}

		Point centerPoint = ePoint;

		if (desktopPane.getJecnTempFigurePanel() != null) {
			// 单个图形添加 虚拟图形位置点既是当前图形位置点
			centerPoint = desktopPane.getJecnTempFigurePanel().getLocation();
		}

		if (tempFlowElementPanel instanceof DividingLine) {// 如果添加的是分割线，记录分割线鼠标点击和释放的坐标值
			DividingLine dividingLine = (DividingLine) tempFlowElementPanel;
			// 记录分割线100%状态下的克隆值
			dividingLine.originalCloneData(desktopPane.getStartPoint(), ePoint);
			desktopPane.setStartPoint(null);

			// 重新计算画图面板的大小
			desktopPane.getCurrDrawPanelSize(dividingLine);

		} else if (tempFlowElementPanel instanceof DateFreeText) {// 最后更改时间文本框
			DateFreeText freeText = (DateFreeText) tempFlowElementPanel;
			// 根据跟定日期转换字符串日期格式yyyy-MM-dd
			String strDate = JecnWorkflowUtil.getStringbyDate(new Date());
			freeText.getFlowElementData().setFigureText(strDate);
		} else if (tempFlowElementPanel instanceof MapNameText) {// 最后更改时间文本框
			MapNameText freeText = (MapNameText) tempFlowElementPanel;
			// 获取默认流程名称
			freeText.getFlowElementData().setFigureText(desktopPane.getFlowMapData().getName());

		}

		// 添加流程元素到面板
		JecnAddFlowElementUnit.addFlowElement(centerPoint, tempFlowElementPanel);

		// 新添加的流程元素添加到面板图形选中集合中
		JecnWorkflowUtil.setCurrentSelect(tempFlowElementPanel);

		if (JecnWorkflowUtil.isAddLayoutBottom(tempFlowElementPanel)) {// 给定图形是否添加需要置底，添加需要置底的图形：关键活动（PA,KSF,KCP）、协作框、泳池
			// 操作前数据
			JecnUndoRedoData undoData = new JecnUndoRedoData();
			// 操作后数据
			JecnUndoRedoData redoData = new JecnUndoRedoData();

			// 把图形置于底层***************
			desktopPane.getGraphicsLevel().setDownFigureLayerProcess(tempFlowElementPanel, undoData, redoData);

			// 添加图形：记录撤销恢复数据
			redoData.recodeFlowElementByAddEdit(tempFlowElementPanel);

			JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createAddAndEdit(undoData, redoData);
			desktopPane.getUndoRedoManager().addEdit(undoProcess);

		} else {// (关键活动（PA,KSF,KCP）、协作框、泳池除外)撤销恢复

			// 操作后数据
			JecnUndoRedoData redoData = new JecnUndoRedoData();
			// 记录撤销恢复数据
			redoData.recodeFlowElement(tempFlowElementPanel);
			JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createAdd(redoData);
			desktopPane.getUndoRedoManager().addEdit(undoProcess);
		}
		if (desktopPane.getCurrentSelectElement().size() == 1) {
			// 修改工具栏的状态
			JecnElementAttributesUtil.updateEditTool(desktopPane.getCurrentSelectElement().get(0));
		}
		return true;
	}

	/**
	 * 获取流程元素对象
	 * 
	 * @param drawDesktopPane
	 *            面板
	 * @return JecnBaseFlowElementPanel 流程元素对象
	 */
	public static JecnBaseFlowElementPanel getJecnBaseFlowElementPanel() {
		// 选中流程元素类型
		MapElemType selectedBtnType = JecnDrawMainPanel.getMainPanel().getBoxPanel().getSelectedBtnType();
		return JecnCreateFlowElement.getFlowEmelentByType(selectedBtnType);
	}

	/**
	 * 面板鼠标拖拽处理
	 * 
	 * @param e
	 */
	public static void buttonMouseDragged(MouseEvent e) {
		if (e == null || !(e.getSource() instanceof JecnDrawDesktopPane)) {
			return;
		}
		// 画图面板
		JecnDrawDesktopPane drawDesktopPane = (JecnDrawDesktopPane) e.getSource();

		if (isShowLineNoFigure()) {// 流程架构图，允许画连接线
			MapManLineCommon.INSTANCE.deskTopMouseDragged(e);
			return;
		}
		// 流程元素库面板
		JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();

		if (isButton1 && MapElemType.DividingLine.equals(boxPanel.getSelectedBtnType())) {// 左键拖动画出分割线
			if (drawDesktopPane.activeLine == null) {
				drawDesktopPane.activeLine = new TempDividingLine(drawDesktopPane.getStartPoint(), e.getPoint());
				// 添加图形画图面板
				drawDesktopPane.add(drawDesktopPane.activeLine);
				if (!drawDesktopPane.getTempDividingList().contains(drawDesktopPane.activeLine)) {
					drawDesktopPane.getTempDividingList().add(drawDesktopPane.activeLine);
				}
			} else {
				drawDesktopPane.activeLine.setEndPoint(e.getPoint());
			}
			if (drawDesktopPane.activeLine.getStartPoint() != null && drawDesktopPane.activeLine.getEndPoint() != null) {
				// 画临时分割线 要时刻刷新面板
				drawDesktopPane.updateUI();
			}
		} else {
			// 设置选中框大小
			drawDesktopPane.getAidCompCollection().getSelecedAreaPanel().setEndPoint(e.getPoint());
		}
	}
}
