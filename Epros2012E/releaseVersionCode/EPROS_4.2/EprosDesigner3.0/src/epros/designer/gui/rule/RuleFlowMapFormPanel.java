package epros.designer.gui.rule;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.jecn.epros.server.bean.rule.JecnFlowCommon;

import epros.designer.gui.rule.table.RulFlowMapTable;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/***
 * 流程地图表单Panel
 * 2013-11-22
 *
 */
public class RuleFlowMapFormPanel extends JecnPanel {
	private List<JecnFlowCommon> listJecnFlowMapCommon;
	private  Long titleId;
	private RulFlowMapTable ruleFlowMapTable;
	/** 滚动面板 */
	private JScrollPane ruleFlowMapScrollPane;
	
	public RuleFlowMapFormPanel(String name, Long titleId,
			List<JecnFlowCommon> listJecnFlowMapCommon, int i){
		this.listJecnFlowMapCommon = listJecnFlowMapCommon;
		this.titleId = titleId;
		initComponents();
//		this.setBorder(getTitledBorder(i + "、" + name));
		// 编辑“制度操作说明”  流程地图表单
		if (listJecnFlowMapCommon != null && listJecnFlowMapCommon.size() > 0) {
		}
	}
	/**
	 * 获得操作说明的标题布局
	 * 
	 * @param title
	 * @return
	 */
	private TitledBorder getTitledBorder(String title) {
		return javax.swing.BorderFactory.createTitledBorder(
				javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1), title,
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("宋体", 1, 12), Color.BLACK);
	}

	private void initComponents() {
		this.setSize(500,100);
		this.setLayout(new BorderLayout());
		ruleFlowMapTable = new RulFlowMapTable(listJecnFlowMapCommon,titleId);
		ruleFlowMapScrollPane = new JScrollPane();
		ruleFlowMapScrollPane.setBorder(null);
		ruleFlowMapScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		ruleFlowMapScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		 this.add(ruleFlowMapScrollPane,BorderLayout.CENTER);
		ruleFlowMapScrollPane.setViewportView(ruleFlowMapTable);
		ruleFlowMapScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	public List<JecnFlowCommon> getListJecnFlowMapCommon() {
		return listJecnFlowMapCommon;
	}
	public void setListJecnFlowMapCommon(
			List<JecnFlowCommon> listJecnFlowMapCommon) {
		this.listJecnFlowMapCommon = listJecnFlowMapCommon;
	}
	public Long getTitleId() {
		return titleId;
	}
	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}
	public RulFlowMapTable getRuleFlowMapTable() {
		return ruleFlowMapTable;
	}
	public void setRuleFlowMapTable(RulFlowMapTable ruleFlowMapTable) {
		this.ruleFlowMapTable = ruleFlowMapTable;
	}
	public JScrollPane getRuleFlowMapScrollPane() {
		return ruleFlowMapScrollPane;
	}
	public void setRuleFlowMapScrollPane(JScrollPane ruleFlowMapScrollPane) {
		this.ruleFlowMapScrollPane = ruleFlowMapScrollPane;
	}
	
}
