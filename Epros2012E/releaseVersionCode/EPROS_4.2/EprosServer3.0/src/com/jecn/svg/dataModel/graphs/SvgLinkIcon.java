package com.jecn.svg.dataModel.graphs;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT;
import com.jecn.epros.server.common.JecnConfigTool;

public class SvgLinkIcon {

	@JsonIgnore
	private Long figureId;

	private String fileId;
	private String fileName;
	private IconName iconName;
	// 输入输出ID
	private String inoutId;
	// 新版输入输出名称
	private String name;

	// 樣例 模板
	private List<JecnFigureInoutSampleT> sample;

	// 前段页面判断是否是新版输入输出 默认是否
	private boolean useNewInout = false;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isUseNewInout() {
		return useNewInout;
	}

	public void setUseNewInout(boolean useNewInout) {
		this.useNewInout = useNewInout;
	}

	public enum IconName {
		ACTIVE_IN, // 活动输入和操作规范
		ACTIVE_OUT, // 活动输出
		ACTIVE_FILE_IN, ACTIVE_FILE_OUT, ACTIVE_IN_TEXT, ACTIVE_OUT_TEXT, POSITION, // 岗位及岗位组
		KEY_POSITION, // 主责岗位
		CONTROL_POINT, // 控制点
		KEY_CONTROL_POINT, // 关键控制点
		ACTIVE_STANDARD, // 活动相关标准
		FILE, // 文档
		UPPER_FLOW, // 上游流程
		LOWER_FLOW, // 下游流程
		PROCEDURE_FLOW, // 过程性流程
		SON_FLOW, // 子流程
		LINK_RULE, // 制度
		LINK_FLOW, // 流程
		LINK_FLOW_MAP, // 流程架构
		LINK_ORG
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInoutId() {
		return inoutId;
	}

	public void setInoutId(String inoutId) {
		this.inoutId = inoutId;
	}

	public List<JecnFigureInoutSampleT> getSample() {
		return sample;
	}

	public void setSample(List<JecnFigureInoutSampleT> sample) {
		this.sample = sample;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public IconName getIconName() {
		return iconName;
	}

	public void setIconName(IconName iconName) {
		this.iconName = iconName;
	}

}
