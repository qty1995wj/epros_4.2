package com.jecn.epros.domain.process;

/**
 * 流程支持工具
 */
public class FlowSustainTool {
    private Long flowSustainToolId;//主键ID
    private String flowSustainToolShow;//支持工具内容
    private Long preFlowSustainToolId;//父ID
    private Integer sortId;        //排序
    private String tLevel;
    private String tPath;

    public Long getFlowSustainToolId() {
        return flowSustainToolId;
    }

    public void setFlowSustainToolId(Long flowSustainToolId) {
        this.flowSustainToolId = flowSustainToolId;
    }

    public String getFlowSustainToolShow() {
        return flowSustainToolShow;
    }

    public void setFlowSustainToolShow(String flowSustainToolShow) {
        this.flowSustainToolShow = flowSustainToolShow;
    }

    public Long getPreFlowSustainToolId() {
        return preFlowSustainToolId;
    }

    public void setPreFlowSustainToolId(Long preFlowSustainToolId) {
        this.preFlowSustainToolId = preFlowSustainToolId;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public String gettLevel() {
        return tLevel;
    }

    public void settLevel(String tLevel) {
        this.tLevel = tLevel;
    }

    public String gettPath() {
        return tPath;
    }

    public void settPath(String tPath) {
        this.tPath = tPath;
    }
}
