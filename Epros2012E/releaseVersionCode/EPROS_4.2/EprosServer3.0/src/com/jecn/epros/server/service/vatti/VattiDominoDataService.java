package com.jecn.epros.server.service.vatti;

import com.jecn.epros.server.webBean.vatti.DominoDataBean;

/**
 * 华帝Domino平台Service
 * 
 * @author weidp
 * @date： 日期：2014-5-27 时间：上午09:59:15
 */
public interface VattiDominoDataService {

	/**
	 * 华帝 获取Domino平台所需的【制度】数据
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-9 时间：上午10:56:46
	 * @return
	 */
	public DominoDataBean getDominoRuleData(Long ruleId) throws Exception;

	/**
	 * 华帝 获取Domino平台所需的【流程】数据
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-27 时间：下午02:27:35
	 * @param flowId
	 * @param type
	 */
	public DominoDataBean getDominoFlowData(Long flowId) throws Exception;
}
