package epros.designer.gui.process;

import java.awt.event.InputEvent;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 流程地图设置连接
 * 
 * @ClassName: LinkProcessChooseDialog
 * @Description: TODO
 * @author ZHANGXIAOHU
 * @date 2015-7-3 下午04:14:45
 */
public class LinkProcessChooseDialog extends ProcessChooseDialog {

	public LinkProcessChooseDialog(List<JecnTreeBean> list) {
		super(list);
	}

	public LinkProcessChooseDialog(List<JecnTreeBean> list, int chooseType) {
		super(list, chooseType);
	}

	/**
	 * 双击树选择
	 * 
	 * @param evt
	 */
	public void jTreeMousePressed(java.awt.event.MouseEvent evt) {
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			if (evt.getClickCount() == 2) {
				if (!this.getjTree().isSelectionEmpty()) {
					javax.swing.tree.TreePath treePath = this.getjTree().getSelectionPath();
					JecnTreeBean jecnTreeBean = ((JecnTreeNode) treePath.getLastPathComponent()).getJecnTreeBean();
					if (jecnTreeBean.getApproveType() == 2) {
						// 判断第二节点的面板是否打开
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("fileDelOrAbolish"));
						return;
					}
					JecnTreeNode node = (JecnTreeNode) treePath.getLastPathComponent();
					// 验证是否有操作权限 (选择文件、制度、标准、风险、流程时验证)
					if (!JecnTreeCommon.isAuthNodeAddTip(node, null)) {
						return;
					}
					/** 判断是否已经存在结果行里面 */
					if (listJTabelResult(jecnTreeBean)) {
						if (isSelectMutil()) {// 判断是否许选择多条
							// 增加table一行数据
							resultTable.addRow(convertRowData(jecnTreeBean));
						} else {
							resultTable.remoeAll();
							// 增加
							resultTable.addRow(convertRowData(jecnTreeBean));
						}
					}
				}
			}
		}
	}

	public void okBtnAction() {
		if (getListJecnTreeBean().size() != 1) {
			return;
		}
		if (!JecnConstants.loginBean.isAdmin()) { // 管理员
			// 判断节点是否有权限
			JecnTreeNode node = JecnTreeCommon.getTreeNode(getListJecnTreeBean().get(0), this.getjTree());
			// 验证是否有操作权限 (选择文件、制度、标准、风险、流程时验证)
			if (!JecnTreeCommon.isAuthNodeAddTip(node, null)) {
				return;
			}
		}
		super.okBtnAction();
	}
}
