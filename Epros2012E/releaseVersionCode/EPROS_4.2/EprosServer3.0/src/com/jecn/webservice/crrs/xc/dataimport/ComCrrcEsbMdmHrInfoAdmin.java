/**
 * ComCrrcEsbMdmHrInfoAdmin.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crrs.xc.dataimport;

public interface ComCrrcEsbMdmHrInfoAdmin extends javax.xml.rpc.Service {
    public java.lang.String getHRComPublisherHttpSoap11EndpointAddress();

    public com.jecn.webservice.crrs.xc.dataimport.HRComPublisherPortType getHRComPublisherHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.crrs.xc.dataimport.HRComPublisherPortType getHRComPublisherHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
