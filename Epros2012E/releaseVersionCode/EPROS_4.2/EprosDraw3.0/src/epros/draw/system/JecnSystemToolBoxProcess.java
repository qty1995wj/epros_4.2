package epros.draw.system;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import epros.draw.constant.JecnToolBoxConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.util.JecnFileUtil;

/**
 * 
 * 不常用图形数据对象
 * 
 * 从本地位置文件中读取不常用图形，或者把流程元素库上的不常用图形写入到本地文件的数据对象
 * 
 * @author ZHOUXY
 * 
 */
class JecnSystemToolBoxProcess {
	/** 流程图：需要不常用流程元素 */
	private static List<MapElemType> partMapToolBoxDataList = null;
	/** 流程地图：需要不常用流程元素 */
	private static List<MapElemType> totalMapToolBoxDataList = null;
	/** 组织图：需要不常用流程元素 */
	private static List<MapElemType> orgMapToolBoxDataList = null;

	/** 流程图文件路径 */
	private static final String partMapPath = "box/partMapGenFlowElem.properties";
	/** 流程地图文件路径 */
	private static final String totalMapPath = "box/totalMapGenFlowElem.properties";
	/** 组织图文件路径 */
	private static final String orgMapPath = "box/orgMapGenFlowElem.properties";

	/**
	 * 
	 * 流程图:
	 * 
	 * 读取不常用图标存放本地文件，添加到数据对象partMapToolBoxDataList中
	 * 
	 */
	static List<MapElemType> readPartListByPropertyFile() {
		if (partMapToolBoxDataList == null) {
			partMapToolBoxDataList = new ArrayList<MapElemType>();

			// 获取不常用图标存放文件内容
			Properties properties = JecnFileUtil.readProperties(partMapPath);

			if (properties == null) {
				return partMapToolBoxDataList;
			}
			// 不常用线
			getPropertiesToList(partMapToolBoxDataList,
					JecnToolBoxConstant.partMapGenNoShowFigureType, properties);
			// 不常用图形
			getPropertiesToList(partMapToolBoxDataList,
					JecnToolBoxConstant.partMapGenNoShowLineType, properties);
		}

		return partMapToolBoxDataList;
	}

	/**
	 * 
	 * 流程地图:
	 * 
	 * 读取不常用图标存放本地文件，添加到数据对象totalMapToolBoxDataList中
	 * 
	 */
	static List<MapElemType> readTotalListByPropertyFile() {
		if (totalMapToolBoxDataList == null) {
			totalMapToolBoxDataList = new ArrayList<MapElemType>();

			// 获取不常用图标存放文件内容
			Properties properties = JecnFileUtil.readProperties(totalMapPath);

			if (properties == null) {
				return totalMapToolBoxDataList;
			}

			// 不常用图形
			getPropertiesToList(totalMapToolBoxDataList,
					JecnToolBoxConstant.totalMapGenNoShowFigureType, properties);
			// 不常用线
			getPropertiesToList(totalMapToolBoxDataList,
					JecnToolBoxConstant.totalMapGenNoShowLineType, properties);
		}

		return totalMapToolBoxDataList;
	}

	/**
	 * 
	 * 组织图:
	 * 
	 * 读取不常用图标存放本地文件，添加到数据对象orgMapToolBoxDataLList中
	 * 
	 */
	static List<MapElemType> readOrgListByPropertyFile() {
		if (orgMapToolBoxDataList == null) {
			orgMapToolBoxDataList = new ArrayList<MapElemType>();

			// 获取不常用图标存放文件内容
			Properties properties = JecnFileUtil.readProperties(orgMapPath);

			if (properties == null) {
				return orgMapToolBoxDataList;
			}

			// 不常用图形
			getPropertiesToList(orgMapToolBoxDataList,
					JecnToolBoxConstant.orgMapGenNoShowFigureType, properties);
			// 不常用线
			getPropertiesToList(orgMapToolBoxDataList,
					JecnToolBoxConstant.orgMapGenNoShowLineType, properties);
		}
		return orgMapToolBoxDataList;
	}

	/**
	 * 
	 * 流程图：写入不常用图标数据
	 * 
	 * @return boolean 写入成功返回true；写入失败返回false
	 */
	static boolean writePartMapByPropertyFile() {
		// 待写入的properties对象
		Properties properties = writeListToProperties(partMapToolBoxDataList);
		if (properties == null) {
			return false;
		}

		// 执行写入操作
		boolean ret = JecnFileUtil.writeProperties(partMapPath, properties,
				null);
		return ret;
	}

	/**
	 * 
	 * 流程地图：写入不常用图标数据
	 * 
	 * @return boolean 写入成功返回true；写入失败返回false
	 */
	static boolean writeTotalMapToPropertyFile() {
		// 待写入的properties对象
		Properties properties = writeListToProperties(totalMapToolBoxDataList);
		if (properties == null) {
			return false;
		}

		// 执行写入操作
		boolean ret = JecnFileUtil.writeProperties(totalMapPath, properties,
				null);
		return ret;
	}

	/**
	 * 
	 * 组织图：写入不常用图标数据
	 * 
	 * @return boolean 写入成功返回true；写入失败返回false
	 */
	static boolean writeOrgMapToPropertyFile() {
		// 待写入的properties对象
		Properties properties = writeListToProperties(orgMapToolBoxDataList);
		if (properties == null) {
			return false;
		}

		// 执行写入操作
		boolean ret = JecnFileUtil
				.writeProperties(orgMapPath, properties, null);
		return ret;
	}

	/**
	 * 
	 * 通过MapElemTypeArray数组流程元素类型查找Properties文件中对应的值,如果此值为true则把此值保存到toolBoxDataList中
	 * 
	 * 如果给定的流程元素在properties文件中没有找到相应的值，那么就默认不显示（false）
	 * 
	 * @param toolBoxDataList
	 *            List<MapElemType>不常用流程元素集合
	 * @param MapElemTypeArray
	 *            MapElemType[] 不常用流程元素数组 JecnToolBoxConstant类中定义的数组
	 * @param properties
	 *            Properties 本地Properties对象
	 */
	private static void getPropertiesToList(List<MapElemType> toolBoxDataList,
			MapElemType[] MapElemTypeArray, Properties properties) {

		if (toolBoxDataList == null || MapElemTypeArray == null
				|| properties == null) {
			return;
		}
		for (MapElemType mapElemType : MapElemTypeArray) {// 不常用流程元素
			// properties中获取的流程元素显示Value
			String fileImage = properties.getProperty(mapElemType.toString());
			if (Boolean.parseBoolean(fileImage)) {
				toolBoxDataList.add(mapElemType);
			}
		}
	}

	/**
	 * 
	 * 把toolBoxDataList值写入到Properties对象中
	 * 
	 * @param toolBoxDataList
	 *            List<MapElemType>
	 * @return Properties
	 */
	private static Properties writeListToProperties(
			List<MapElemType> toolBoxDataList) {
		if (toolBoxDataList == null) {
			return null;
		}
		// 待写入的properties对象
		Properties properties = new Properties();

		for (MapElemType mapElemType : toolBoxDataList) {
			properties.setProperty(mapElemType.toString(), "true");
		}
		return properties;
	}
}
