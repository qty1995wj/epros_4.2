package epros.designer.service.process;

import java.awt.Color;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnLineSegmentDep;
import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnUtil;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.top.toolbar.io.JecnIOUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.util.DrawCommon;

/**
 * 打开组织图
 * 
 * @author fuzhh
 * 
 */
public class JecnOrganization {
	private static Logger log = Logger.getLogger(JecnOrganization.class);

	/**
	 * 生成组织面板
	 * 
	 * @param openOrgBean
	 *            组织面板数据
	 */
	public static void openOrganizationMap(JecnOpenOrgBean openOrgBean) {
		// 创建面板
		createOrganizationMap(openOrgBean);
		// 添加图形线到面板
		setOrgMapData(openOrgBean);
	}

	/**
	 * 生成组织面板
	 * 
	 * @param openOrgBean
	 *            组织面板数据
	 */
	public static void createOrganizationMap(JecnOpenOrgBean openOrgBean) {
		if (openOrgBean.getJecnFlowOrg() == null) {
			return;
		}
		if (!JecnProcess.isOpen(openOrgBean.getJecnFlowOrg().getOrgId().intValue(), MapType.orgMap)) {
			// 创建组织图
			JecnFlowMapData organizationData = JecnDesignerCommon.createFlowMapData(MapType.orgMap, openOrgBean
					.getJecnFlowOrg().getOrgId());
			// 设置组织图名称
			organizationData.setName(openOrgBean.getJecnFlowOrg().getOrgName());
			// 面板宽
			String width = openOrgBean.getJecnFlowOrg().getOrgWidth();
			organizationData.setWorkflowWidth(DrawCommon.isNullOrEmtryTrim(width) ? 3000 : Integer
					.valueOf(width.trim()));
			// 面板高
			String height = openOrgBean.getJecnFlowOrg().getOrgHeight();
			organizationData.setWorkflowHeight(DrawCommon.isNullOrEmtryTrim(height) ? 3000 : Integer.valueOf(height
					.trim()));
			// 添加面板
			JecnDrawMainPanel.getMainPanel().addWolkflow(organizationData);
		} else {
			// 清空面板
			JecnIOUtil.resetWorkflow();
		}
	}

	/**
	 * 设置组织图数据
	 * 
	 * @param processMapData
	 *            流程地图数据bean
	 */
	public static void setOrgMapData(JecnOpenOrgBean openOrgBean) {
		// 存储线的数据集合
		Map<JecnFlowOrgImage, Integer> lineDataMap = new HashMap<JecnFlowOrgImage, Integer>();
		// 获取所有图形集合
		List<JecnBaseFlowElementPanel> baseFlowElementList = new ArrayList<JecnBaseFlowElementPanel>();
		// 数据根据层级排序
		sortFlowOrgImageList(openOrgBean.getFlowOrgImageList());
		// 排序
		// sortFlowOrgImageXList(openOrgBean.getFlowOrgImageList());
		// Y轴为空的，Y的值
		for (int i = 0; i < openOrgBean.getFlowOrgImageList().size(); i++) {
			JecnFlowOrgImage flowOrgImage = openOrgBean.getFlowOrgImageList().get(i);
			// 生成除连接线外的所有图形
			getOrgData(flowOrgImage, lineDataMap, baseFlowElementList, i);
		}
		// 生成连接线
		getLineData(lineDataMap, baseFlowElementList);
		JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 设置组织图数据
	 * 
	 * @param flowOrgImage
	 *            组织图属性
	 * @param lineDataMap
	 *            存储组织图线
	 * @param baseFlowElementList
	 *            存储所有图形
	 */
	public static void getOrgData(JecnFlowOrgImage flowOrgImage, Map<JecnFlowOrgImage, Integer> lineDataMap,
			List<JecnBaseFlowElementPanel> baseFlowElementList, int count) {
		// 面板为空返回
		if (getWorkflow() == null) {
			return;
		}
		if ((MapElemType.CommonLine.toString().equals(flowOrgImage.getFigureType()))
				|| (MapElemType.ManhattanLine.toString().equals(flowOrgImage.getFigureType()))) { // 连接线
			lineDataMap.put(flowOrgImage, getOrderIndex());
		} else {
			getFigureImagerData(flowOrgImage, baseFlowElementList, getOrderIndex(), count);
		}

	}

	/**
	 * * 设置图形数据
	 * 
	 * @param flowStructureImageT
	 *            服务图形数据的bean
	 * @param baseFlowElementList
	 *            新建存储转换图形的List
	 * @param processFigureData
	 *            流程图属性数据
	 * @param orderIndex
	 *            图形的层级
	 */
	private static void getFigureImagerData(JecnFlowOrgImage flowOrgImage,
			List<JecnBaseFlowElementPanel> baseFlowElementList, int orderIndex, int count) {
		// 图形标识
		String figureType = flowOrgImage.getFigureType();
		// 创建图形数据对象
		JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.valueOf(figureType));
		// 图形主键
		figureData.setFlowElementId(flowOrgImage.getFigureId());
		// 图形UIID唯一标识，放入的是主键ID （画图面板上的唯一标识，不代表数据库）
		figureData.setFlowElementUUID(flowOrgImage.getFigureId().toString());
		// 图形上添加的字符
		figureData.setFigureText(flowOrgImage.getFigureText());
		// 图形的X点
		int pointX = 0;
		if (flowOrgImage.getPoLongx() == null || flowOrgImage.getPoLongx() == 0L) {
			// 获取最大的X
			pointX = 15;
		} else {
			pointX = flowOrgImage.getPoLongx().intValue();
		}
		// 图形的Y点
		int pointY = 0;
		if (flowOrgImage.getPoLongy() == null || flowOrgImage.getPoLongy() == 0L) {
			if (flowOrgImage.getHeight() != null && flowOrgImage.getHeight() != 0L) {
				pointY = 10 + (count * 10) + (count * flowOrgImage.getHeight().intValue());
			} else {
				pointY = 10;
			}
		} else {
			pointY = flowOrgImage.getPoLongy().intValue();
		}
		// 图形的位置点
		Point point = new Point(pointX, pointY);
		// 图形的宽度
		if (flowOrgImage.getWidth() != null) {
			figureData.setFigureSizeX(flowOrgImage.getWidth().intValue());
		}
		// 图形的高度
		if (flowOrgImage.getHeight() != null) {
			figureData.setFigureSizeY(flowOrgImage.getHeight().intValue());
		}
		// 字体的大小
		if (flowOrgImage.getFontSize() != null) {
			figureData.setFontSize(flowOrgImage.getFontSize().intValue());
		}
		// 图形的层级
		figureData.setZOrderIndex(orderIndex);
		// 图形填充效果
		if (flowOrgImage.getFillEffects() != null) {
			int figureFillEffects = flowOrgImage.getFillEffects();
			if (0 == figureFillEffects) {
				figureData.setFillType(FillType.none);
			} else if (1 == figureFillEffects) {
				figureData.setFillType(FillType.one);
			} else {
				figureData.setFillType(FillType.two);
			}
		}
		// 图形的连接ID
		if (flowOrgImage.getLinkOrgId() != null) {
			figureData.getDesignerFigureData().setLinkId(flowOrgImage.getLinkOrgId());
		}
		// 图形的填充颜色
		String figureFillCololr = flowOrgImage.getBGColor();
		if (!DrawCommon.isNullOrEmtryTrim(figureFillCololr)) {
			String[] backColors = figureFillCololr.split(",");
			if (backColors.length == 3) {// 单色效果
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				figureData.setFillColor(newBackColor);
			} else if (backColors.length == 7) {// 双色效果+渐变方向
				Color oneBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				Color twoBackColor = new Color(Integer.parseInt(backColors[3]), Integer.parseInt(backColors[4]),
						Integer.parseInt(backColors[5]));
				figureData.setFillColor(oneBackColor);
				figureData.setChangeColor(twoBackColor);

				String colorChangType = backColors[6];
				if ("1".equals(colorChangType)) {
					// 向上倾斜
					figureData.setFillColorChangType(FillColorChangType.topTilt);
				} else if ("2".equals(colorChangType)) {
					// 向下倾斜
					figureData.setFillColorChangType(FillColorChangType.bottom);
				} else if ("3".equals(colorChangType)) {
					// 垂直
					figureData.setFillColorChangType(FillColorChangType.vertical);
				} else {
					// 水平
					figureData.setFillColorChangType(FillColorChangType.horizontal);
				}

			}
		}
		// 字体颜色
		String fontColor = flowOrgImage.getFontColor();
		if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
			String[] backColors = fontColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setFontColor(newBackColor);
		}

		// 字体是否加粗0为正常，1为加粗，2为斜体
		if (flowOrgImage.getFontBody() != null) {
			int fontBody = flowOrgImage.getFontBody().intValue();
			figureData.setFontStyle(fontBody);
		}
		// 字体类型:默认为（宋体）
		String fontType = flowOrgImage.getFontType();
		figureData.setFontName(fontType);
		// 字体位置
		if (flowOrgImage.getFontPosition() != null) {
			figureData.setFontPosition(Integer.valueOf(flowOrgImage.getFontPosition()));
		}
		// 是否存在阴影
		Long favaShadow = flowOrgImage.getHavaShadow();
		if (favaShadow != null) {
			if (favaShadow.intValue() == 1) {
				figureData.setShadowsFlag(true);
			} else {
				figureData.setShadowsFlag(false);
			}
		}
		// 阴影颜色
		String shadowColor = flowOrgImage.getShadowColor();
		if (!DrawCommon.isNullOrEmtryTrim(shadowColor)) {
			String[] backColors = shadowColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setShadowColor(newBackColor);
		}

		// 旋转角度值
		if (flowOrgImage.getCircumgyrate() != null) {
			figureData.setCurrentAngle(Double.valueOf(flowOrgImage.getCircumgyrate()));
		}

		// 边框样式
		if (flowOrgImage.getBodyLine() != null) {
			figureData.setBodyStyle(Integer.valueOf(flowOrgImage.getBodyLine()));
		}
		// 边框宽度
		if (flowOrgImage.getFrameBody() != null) {
			figureData.setBodyWidth(flowOrgImage.getFrameBody().intValue());
		}
		// 边框颜色
		String bodyColor = flowOrgImage.getBodyColor();
		if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
			String[] backColors = bodyColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setBodyColor(newBackColor);
		}
		// 是否显示3D
		if (flowOrgImage.getIs3DEffect() != null) {
			int figure3D = flowOrgImage.getIs3DEffect();
			if (figure3D == 1) {
				figureData.setFlag3D(true);
			} else {
				figureData.setFlag3D(false);
			}
		}
		if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			// 获取连接线数据对象
			JecnBaseDivedingLineData lineData = new JecnBaseDivedingLineData(MapElemType.CommentLine);
			// 注释框小线段开始点
			int startX = 0;
			if (flowOrgImage.getStartFigure() != null) {
				startX = flowOrgImage.getStartFigure().intValue();
			}
			// 注释框小线段开始点
			int startY = 0;
			if (flowOrgImage.getEndFigure() != null) {
				startY = flowOrgImage.getEndFigure().intValue();
			}
			Point startPoint = new Point(startX, startY);
			// 注释框结束点
			Point endPoint = JecnProcess.reCommonLineEPoint(flowOrgImage.getPoLongx().intValue(), flowOrgImage
					.getPoLongy().intValue(), Double.valueOf(flowOrgImage.getCircumgyrate()), flowOrgImage.getWidth()
					.intValue(), flowOrgImage.getHeight().intValue());
			// 设置线段大小 记录线段原始状态下数值
			lineData.getDiviLineCloneable().initOriginalArributs(startPoint, endPoint, figureData.getBodyWidth(),
					getWorkflow().getWorkflowScale());
			commentData.setLineData(lineData);

		} else if (MapElemType.valueOf(figureType) == MapElemType.IconFigure) { // 图标插入框
			figureData.getDesignerFigureData().setLinkId(flowOrgImage.getLinkOrgId());
		} else if (MapElemType.valueOf(figureType) == MapElemType.PositionFigure) { // 岗位
			figureData.getDesignerFigureData().setPosNumber(flowOrgImage.getFigureNumberId());
		}
		// 生成图形
		JecnBaseFigurePanel baseFigureElement = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowFigure(MapElemType
				.valueOf(figureType), figureData);

		if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
			CommentText commentText = (CommentText) baseFigureElement;
			// 默认算法 取数据层
			commentText.isDefault();
		}
		if (figureData.getDesignerFigureData().getLinkId() != null) {
			FileOpenBean fileOpenBean = null;
			try {
				fileOpenBean = ConnectionPool.getFileAction().openFile(figureData.getDesignerFigureData().getLinkId());
			} catch (Exception e) {
				log.error("JecnOrganization getFigureImagerData is error", e);
			}
			if (fileOpenBean != null) {
				String path = JecnUtil.downFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
				IconFigure iconFigure = null;
				if (baseFigureElement instanceof IconFigure) {
					iconFigure = (IconFigure) baseFigureElement;
					if (path != null) {
						java.awt.Image img = Toolkit.getDefaultToolkit().getImage(path);
						iconFigure.setImg(img);
						iconFigure.repaint();
					}
				}
			}

		}
		// 设置图形的位置
		baseFigureElement.setSizeAndLocation(point);
		// 增加链接
		JecnDesignerCommon.addElemLink(baseFigureElement, getWorkflow().getFlowMapData().getMapType());
		baseFlowElementList.add(baseFigureElement);
		// 给面板内存数据赋值
		JecnFigureData figureDataClone = figureData.clone();
		if (figureData.getDesignerFigureData().getLinkId() != null) {
			figureDataClone.getDesignerFigureData().setLinkId(figureData.getDesignerFigureData().getLinkId());
		}
		figureDataClone.setFlowElementId(flowOrgImage.getFigureId());
		getFigureDataList().add(figureDataClone);
		// 添加图形到面板
		JecnAddFlowElementUnit.addFigure(baseFigureElement);
	}

	/**
	 * 连接线
	 * 
	 * @param lineDataMap
	 *            存储线和线的层级
	 * @param baseFlowElementList
	 *            存储图形的集合
	 */
	private static void getLineData(Map<JecnFlowOrgImage, Integer> lineDataMap,
			List<JecnBaseFlowElementPanel> baseFlowElementList) {
		Iterator<JecnFlowOrgImage> iterator = lineDataMap.keySet().iterator();
		while (iterator.hasNext()) {
			JecnFlowOrgImage flowOrgImage = iterator.next();
			// 获取线的标识
			String figureType = flowOrgImage.getFigureType();
			// 获取连接线的数据对象
			JecnManhattanLineData manhattanLineData = new JecnManhattanLineData(MapElemType.valueOf(figureType));
			// 线的主键
			manhattanLineData.setFlowElementId(flowOrgImage.getFigureId());
			// 线的宽度
			if (flowOrgImage.getLineThickness() != null) {
				manhattanLineData.setBodyWidth(flowOrgImage.getLineThickness());
			}
			// 线的类型
			manhattanLineData.setBodyStyle(Integer.valueOf(flowOrgImage.getBodyLine() == null ? "0" : flowOrgImage
					.getBodyLine()));
			// 字体颜色
			String fontColor = flowOrgImage.getFontColor();
			if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
				String[] backColors = fontColor.split(",");
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				manhattanLineData.setFontColor(newBackColor);
			}

			// 字体加粗样式
			if (flowOrgImage.getFontBody() != null) {
				manhattanLineData.setFontStyle(flowOrgImage.getFontBody().intValue());
			}
			// 字体类型
			if (flowOrgImage.getFontType() != null) {
				manhattanLineData.setFontName(flowOrgImage.getFontType());
			}
			// 字体大小
			if (flowOrgImage.getFontSize() != null) {
				manhattanLineData.setFontSize(flowOrgImage.getFontSize().intValue());
			}

			// 图形的开始UIID
			if (flowOrgImage.getStartFigure() != null) {
				manhattanLineData.setStartFigureUUID(flowOrgImage.getStartFigure().toString());
			}
			// 图形的结束UIID
			if (flowOrgImage.getEndFigure() != null) {
				manhattanLineData.setEndFigureUUID(flowOrgImage.getEndFigure().toString());
			}
			// 图形的开始ID
			manhattanLineData.setStartId(flowOrgImage.getStartFigure());
			// 图形的结束ID
			manhattanLineData.setEndId(flowOrgImage.getEndFigure());
			// 获取开始点的编辑点
			int poLongx = flowOrgImage.getPoLongx().intValue();
			EditPointType startEditPointType = DrawCommon.getLineType(poLongx);
			manhattanLineData.setStartEditPointType(startEditPointType);

			// 获取结束点的编辑点
			int poLongy = flowOrgImage.getPoLongy().intValue();
			EditPointType editPointType = DrawCommon.getLineType(poLongy);
			manhattanLineData.setEndEditPointType(editPointType);

			// 线段颜色
			String bodyColor = flowOrgImage.getLineColor();
			if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
				String[] backColors = bodyColor.split(",");
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				manhattanLineData.setBodyColor(newBackColor);
			}

			// 给线段赋值
			manhattanLineData.setFigureText(flowOrgImage.getFigureText());
			// 获取层级
			int orderIndex = lineDataMap.get(flowOrgImage);
			manhattanLineData.setZOrderIndex(orderIndex);
			// 根据标识位获取连接线对象
			JecnBaseManhattanLinePanel baseManhattanLine = JecnPaintFigureUnit.getJecnBaseManhattanLinePanel(
					figureType, manhattanLineData);

			JecnFlowImport flowImport = new JecnFlowImport();
			// 设置连接线的开始图形和结束图形
			flowImport.setManLineRefFigure(baseFlowElementList, baseManhattanLine);

			// 获取连接线数据对象
			List<JecnManhattanRouterResultData> routerResultList = new ArrayList<JecnManhattanRouterResultData>();

			Map<JecnManhattanRouterResultData, Long> map = new HashMap<JecnManhattanRouterResultData, Long>();
			// 获取小线段的数据
			for (JecnLineSegmentDep lineSegmentDep : flowOrgImage.getLineSegmentDepList()) {
				Point startPoint = new Point(lineSegmentDep.getStartX().intValue(), lineSegmentDep.getStartY()
						.intValue());
				Point endPoint = new Point(lineSegmentDep.getEndX().intValue(), lineSegmentDep.getEndY().intValue());
				// 收集小线段数据
				JecnManhattanRouterResultData data = new JecnManhattanRouterResultData();
				data.setStartPoint(startPoint);
				data.setEndPint(endPoint);
				routerResultList.add(data);
				map.put(data, lineSegmentDep.getFigureId());
			}
			if (routerResultList == null) {
				return;
			}
			// 排序
			routerResultList = baseManhattanLine.reOrderLinePoint(routerResultList);
			for (JecnManhattanRouterResultData data : routerResultList) {
				JecnLineSegmentData lineSegmentData = baseManhattanLine.addLineSegment(data.getStartPoint(), data
						.getEndPint());
				Long figureId = map.get(data);
				lineSegmentData.setFlowLineID(figureId);
			}
			// 面板添加线
			JecnAddFlowElementUnit.addImportManLine(baseManhattanLine);
			// 添加数据到线段显示
			baseManhattanLine.insertTextToLine();
			// 把线的克隆数据放入内存中
			JecnManhattanLineData manhattanLineDataClone = manhattanLineData.clone();
			manhattanLineDataClone.setFlowElementId(flowOrgImage.getFigureId());
			getLineDataList().add(manhattanLineDataClone);
		}
	}

	/**
	 * 生成组织图
	 * 
	 * @param rootOrg
	 *            组织图根节点
	 * @param orgList
	 *            根节点下的组织和岗位
	 */
	public static void createOrgMap(JecnTreeBean rootOrg, List<JecnTreeBean> orgList) {
		// 内存中图形的数据
		if (getWorkflow() != null && getWorkflow().getFlowMapData().getDesignerData().getOldFigureList() != null) {
			getWorkflow().getFlowMapData().getDesignerData().getOldFigureList().clear();
		}
		try {
			JecnOpenOrgBean orgOpenData = ConnectionPool.getOrganizationAction().openOrgMap(rootOrg.getId());
			JecnOrganization.openOrganizationMap(orgOpenData);
		} catch (Exception e) {
			log.error("JecnOrganization createOrgMap is error！", e);
		}
		// 清空组织面板
		resetOreWorkflow();
		// 重新生成组织图
		setOrgDataToWorkflow(rootOrg, orgList);
		getWorkflow().repaint();
	}

	private static void setOrgDataToWorkflow(JecnTreeBean rootOrg, List<JecnTreeBean> orgList) {
		int orgX = 30;
		int orgY = 30;
		// 生成组织
		JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(MapElemType.Rect);
		// 转成图形
		JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
		// 设置组织名称
		baseFigure.getFlowElementData().setFigureText(rootOrg.getName());
		// 设置组织位置点
		Point point = new Point(orgX, orgY);
		// 把根组织添加到面板
		JecnAddFlowElementUnit.addFigure(point, baseFigure);
		// 存储组织的List
		List<JecnTreeBean> orgTreeBeanList = new ArrayList<JecnTreeBean>();
		// 存储岗位的List
		List<JecnTreeBean> posTreeBeanList = new ArrayList<JecnTreeBean>();

		if (orgList != null) {
			// 循环List
			for (int i = 0; i < orgList.size(); i++) {
				JecnTreeBean treeBean = orgList.get(i);
				if (treeBean.getTreeNodeType() == TreeNodeType.organization) {
					orgTreeBeanList.add(treeBean);
				} else if (treeBean.getTreeNodeType() == TreeNodeType.position) {
					posTreeBeanList.add(treeBean);
				}
			}
			// 循环组织
			for (int j = 0; j < orgTreeBeanList.size(); j++) {
				JecnTreeBean orgTreeBean = orgList.get(j);
				// 生成组织
				JecnBaseFlowElementPanel treeBeanFlowElement = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.Rect);
				// 转成图形
				JecnBaseFigurePanel treeBeanBaseFigure = (JecnBaseFigurePanel) treeBeanFlowElement;
				// 组织连接ID
				((JecnFigureData) treeBeanFlowElement.getFlowElementData()).getDesignerFigureData().setLinkId(
						orgTreeBean.getId());
				// 设置组织名称
				treeBeanBaseFigure.getFlowElementData().setFigureText(orgTreeBean.getName());
				// 设置组织位置点
				Point treeBeanPoint = new Point(orgX * 2 + baseFigure.getWidth(), orgY * (j + 2)
						+ baseFigure.getHeight() * (j + 1));
				// 把组织添加到面板
				JecnAddFlowElementUnit.addFigure(treeBeanPoint, treeBeanBaseFigure);

				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.ManhattanLine);
				// 开始点为右编辑点
				linePanel.getFlowElementData().setStartEditPointType(EditPointType.right);
				// 结束点为左编辑点
				linePanel.getFlowElementData().setEndEditPointType(EditPointType.left);
				// 开始图形
				linePanel.setStartFigure(baseFigure);
				// 结束图形
				linePanel.setEndFigure(treeBeanBaseFigure);
				// 增加链接
				JecnDesignerCommon.addElemLink((JecnBaseFigurePanel) treeBeanFlowElement, getWorkflow()
						.getFlowMapData().getMapType());
				// 面板添加线
				JecnAddFlowElementUnit.addManhattinLine(linePanel);
			}
			// 循环岗位
			for (int k = 0; k < posTreeBeanList.size(); k++) {
				JecnTreeBean posTreeBean = posTreeBeanList.get(k);
				// 生成岗位
				JecnBaseFlowElementPanel treeBeanFlowElement = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.PositionFigure);
				// 转成图形
				JecnBaseFigurePanel treeBeanBaseFigure = (JecnBaseFigurePanel) treeBeanFlowElement;
				// 设置岗位名称
				treeBeanBaseFigure.getFlowElementData().setFigureText(posTreeBean.getName());
				// 设置岗位主键ID
				treeBeanBaseFigure.getFlowElementData().setFlowElementId(posTreeBean.getId());
				// 设置编号
				treeBeanBaseFigure.getFlowElementData().getDesignerFigureData().setPosNumber(posTreeBean.getNumberId());
				// 设置岗位置点
				Point treeBeanPoint = new Point(orgX * 3 + baseFigure.getWidth() * 2, orgY * (k + 1)
						+ baseFigure.getHeight() * k);
				// 把岗位添加到面板
				JecnAddFlowElementUnit.addFigure(treeBeanPoint, treeBeanBaseFigure);
			}
		}

	}

	/**
	 * 重置组织图面板
	 */
	private static void resetOreWorkflow() {
		if (getWorkflow() != null) {
			// 回到原始视图
			getWorkflow().getWorkflowZoomProcess().zoomByNewWorkflowScale(1.0);
			// 清空面板
			getWorkflow().removeAll();
			// 重置面板上的List
			JecnIOUtil.resetWorkflowList();
			// 面板保存标识为true
			getWorkflow().getFlowMapData().setSave(true);
		}
	}

	/**
	 * 
	 * 组织图给定参数按照序号排序(按层级排序)
	 * 
	 * @param itemBeanList
	 */
	private static void sortFlowOrgImageList(List<JecnFlowOrgImage> jecnFlowOrgImageList) {
		if (jecnFlowOrgImageList == null || jecnFlowOrgImageList.size() == 0) {
			return;
		}
		Collections.sort(jecnFlowOrgImageList, new Comparator<JecnFlowOrgImage>() {
			public int compare(JecnFlowOrgImage r1, JecnFlowOrgImage r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("JecnOrganization sortFlowOrgImageList is error");
				}
				int sortId1 = r1.getOrderIndex() == null ? 0 : r1.getOrderIndex().intValue();
				int sortId2 = r2.getOrderIndex() == null ? 0 : r2.getOrderIndex().intValue();
				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 
	 * 组织图给定参数按照序号排序(按X坐标排序)
	 * 
	 * @param itemBeanList
	 */
	private static void sortFlowOrgImageXList(List<JecnFlowOrgImage> jecnFlowOrgImageList) {
		if (jecnFlowOrgImageList == null || jecnFlowOrgImageList.size() == 0) {
			return;
		}
		Collections.sort(jecnFlowOrgImageList, new Comparator<JecnFlowOrgImage>() {
			public int compare(JecnFlowOrgImage r1, JecnFlowOrgImage r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("JecnOrganization sortFlowOrgImageXList is error");
				}
				int sortId1 = r1.getPoLongx() == null ? 0 : r1.getPoLongx().intValue();
				int sortId2 = r2.getPoLongx() == null ? 0 : r2.getPoLongx().intValue();
				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 获取面板
	 * 
	 * @return 画图面板
	 */
	private static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 获取面板最大层级，并把层级数加一
	 * 
	 * @return 面板最大层级
	 */
	private static int getOrderIndex() {
		if (getWorkflow() == null) {
			return -1;
		}
		// 获取最大层级
		int orderIndex = getWorkflow().getMaxZOrderIndex();
		// 最大层级加一
		getWorkflow().setMaxZOrderIndex(orderIndex + 2);
		return orderIndex;
	}

	/**
	 * 获取保存图形的集合
	 * 
	 * @return 保存图形的集合
	 */
	private static List<JecnFigureData> getFigureDataList() {
		if (getWorkflow() == null) {
			return null;
		}
		return getWorkflow().getFlowMapData().getDesignerData().getOldFigureList();
	}

	/**
	 * 获取保存线的集合
	 * 
	 * @return 保存线的集合
	 */
	private static List<JecnBaseLineData> getLineDataList() {
		if (getWorkflow() == null) {
			return null;
		}
		return getWorkflow().getFlowMapData().getDesignerData().getOldLineList();
	}
}