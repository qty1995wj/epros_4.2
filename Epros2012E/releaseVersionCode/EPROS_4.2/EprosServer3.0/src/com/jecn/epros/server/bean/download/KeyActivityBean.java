package com.jecn.epros.server.bean.download;

import java.io.Serializable;

public class KeyActivityBean implements Serializable {
	/**
	 * 活动主键ID
	 */
	private Long id;
	/** 关键活动类型：1为PA,2为KSF,3为KCP */
	private String activeKey;
	/** 活动编号 */
	private String activityNumber;
	/** 活动名称 */
	private String activityName;
	/** 活动说明 */
	private String activityShow;
	/** 关键说明 */
	private String activityShowControl;
	/** 农夫山泉的时候才有数据 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 */
	private Object[] activeInfo = new Object[6];

	public String getActiveKey() {
		return activeKey;
	}

	public void setActiveKey(String activeKey) {
		this.activeKey = activeKey;
	}

	public String getActivityNumber() {
		return activityNumber;
	}

	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getActivityShow() {
		return activityShow;
	}

	public void setActivityShow(String activityShow) {
		this.activityShow = activityShow;
	}

	public String getActivityShowControl() {
		return activityShowControl;
	}

	public void setActivityShowControl(String activityShowControl) {
		this.activityShowControl = activityShowControl;
	}

	public Object[] getActiveInfo() {
		return activeInfo;
	}

	public void setActiveInfo(Object[] activeInfo) {
		this.activeInfo = activeInfo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
