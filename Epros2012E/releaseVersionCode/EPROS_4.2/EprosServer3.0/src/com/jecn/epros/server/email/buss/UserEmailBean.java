package com.jecn.epros.server.email.buss;

/**
 * 人员邮箱发送方式记录
 * 
 * @author Administrator
 * @date： 日期：Dec 13, 2012 时间：10:39:47 AM
 */
public class UserEmailBean {
	/** 邮件地址 */
	private String emailAdderss;
	/** 邮件内外网标识 inner:内网；outer：外网 */
	private String emailType;
	/** 邮件接收者还是抄送人 0：邮件接收者(主送)；1：邮件抄送人 */
	private String recipient;

	public String getEmailAdderss() {
		return emailAdderss;
	}

	public void setEmailAdderss(String emailAdderss) {
		this.emailAdderss = emailAdderss;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
}
