package epros.designer.gui.rule;

import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.rule.JecnStandarCommon;

import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

public class RuleStandarFormPanel extends JecnFileDescriptionTable {
	private List<JecnStandarCommon> listJecnStandarCommon;

	public RuleStandarFormPanel(int index, String name, JecnPanel contentPanel,
			List<JecnStandarCommon> listJecnStandarCommon, boolean isRequest) {
		super(index, name, isRequest, contentPanel);
		this.listJecnStandarCommon = listJecnStandarCommon;
		initTable();
	}

	@Override
	protected void dbClickMethod() {

	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("standardName"));
		title.add(JecnProperties.getValue("keyActiveType"));
		return new JecnTableModel(title, getContent());
	}

	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		if (listJecnStandarCommon != null && listJecnStandarCommon.size() > 0) {
			for (JecnStandarCommon jecnStandarCommon : listJecnStandarCommon) {
				Vector<String> data = new Vector<String>();
				if (jecnStandarCommon != null) {
					// 标准ID
					data.add(jecnStandarCommon.getStandarId().toString());
					// 标准名称
					data.add(jecnStandarCommon.getStandarName());
					// 标准类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
					String standarTypeStr = jecnStandarCommon.getStandaType();
					if ("0".equals(standarTypeStr)) {
						data.add(JecnProperties.getValue("dir"));
					} else if ("1".equals(standarTypeStr)) {
						data.add(JecnProperties.getValue("fileStandar"));
					} else if ("2".equals(standarTypeStr)) {
						data.add(JecnProperties.getValue("flowStandar"));
					} else if ("3".equals(standarTypeStr)) {
						data.add(JecnProperties.getValue("flowMapStandar"));
					} else if ("4".equals(standarTypeStr)) {
						data.add(JecnProperties.getValue("standarClause"));
					} else if ("5".equals(standarTypeStr)) {
						data.add(JecnProperties.getValue("stanClauseRequire"));
					}
					vector.add(data);
				}
			}
		}
		return vector;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
