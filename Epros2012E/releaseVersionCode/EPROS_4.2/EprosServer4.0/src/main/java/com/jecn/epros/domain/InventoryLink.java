package com.jecn.epros.domain;

/**
 * @author hyl
 */
public class InventoryLink extends LinkResource {

    public InventoryLink(String id, String name, ResourceType type) {
        super(id, name, type);
    }

}
