package epros.designer.gui.rule.mode;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
/***
 * 制度模板选择框
 * 	
 *	2012-10-25
 */
public class RuleModeChooseDialog extends JecnSelectChooseDialog  {
	private static Logger log = Logger.getLogger(RuleModeChooseDialog.class);
	public RuleModeChooseDialog(List<JecnTreeBean> list){
		super(list,12);
		this.setTitle(JecnProperties.getValue("ruleModeSelect"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyChooseRuleModeTree();
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("modeNameC");
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title=new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("modeName"));//模板名称：
		return title;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getRuleModeAction().findRuleModeByName(name);
		} catch (Exception e) {
			log.error("RuleModeChooseDialog searchByName is error",e);
		}
		return null;
	}

}
