package com.jecn.viewdoc.convert;

import java.io.File;

import ooo.connector.BootstrapSocketConnector;

import org.apache.log4j.Logger;

import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import com.sun.star.util.XCloseable;

public class DocumentConverter {

	private static Logger log = Logger.getLogger(DocumentConverter.class);

	protected static final String PDF = "writer_pdf_Export";
	protected static final String IMG = "IMG";
	protected static final String EXCEL = "calc8";
	protected static final String NONE = "none";

	private static XComponentContext createContext() throws Exception {
		return BootstrapSocketConnector.bootstrap(ViewDocsPathManager.OPENOFFICE);
	}

	private static XComponentLoader createLoader(XComponentContext context) throws Exception {
		// get the remote office service manager
		XMultiComponentFactory mcf = context.getServiceManager();
		Object desktop = mcf.createInstanceWithContext("com.sun.star.frame.Desktop", context);
		return UnoRuntime.queryInterface(XComponentLoader.class, desktop);
	}

	private static XComponent loadDocument(XComponentLoader loader, String inputFilePath) throws Exception{
		// Preparing properties for loading the document
		PropertyValue[] propertyValues = new PropertyValue[1];
		propertyValues[0] = new PropertyValue();
		propertyValues[0].Name = "Hidden";
		propertyValues[0].Value = new Boolean(true);

		// Composing the URL by replacing all backslashs
		File inputFile = new File(inputFilePath);
		String inputUrl = "file:///" + inputFile.getAbsolutePath().replace('\\', '/');
		try {
			return loader.loadComponentFromURL(inputUrl, "_blank", 0, propertyValues);
		} catch (Exception e) {
			throw new Exception("输入文件为：inputUrl:" + inputUrl, e);
		}
	}

	private static void convertDocument(XComponent doc, String outputFilePath, String convertType) throws Exception {
		// Preparing properties for converting the document
		PropertyValue[] propertyValues = new PropertyValue[2];
		// Setting the flag for overwriting
		propertyValues[0] = new PropertyValue();
		propertyValues[0].Name = "Overwrite";
		propertyValues[0].Value = new Boolean(true);
		// Setting the filter name
		propertyValues[1] = new PropertyValue();
		propertyValues[1].Name = "FilterName";
		propertyValues[1].Value = convertType;

		// Composing the URL by replacing all backslashs
		File outputFile = new File(outputFilePath);
		String outputUrl = "file:///" + outputFile.getAbsolutePath().replace('\\', '/');

		// Getting an object that will offer a simple way to store
		// a document to a URL.
		XStorable storable = UnoRuntime.queryInterface(XStorable.class, doc);
		// Storing and converting the document
		storable.storeToURL(outputUrl, propertyValues);
	}

	private static void closeDocument(Object doc) throws Exception {
		// Closing the converted document. Use XCloseable.clsoe if the
		// interface is supported, otherwise use XComponent.dispose
		XCloseable closeable = UnoRuntime.queryInterface(XCloseable.class, doc);

		if (closeable != null) {
			closeable.close(false);
		} else {
			XComponent component = UnoRuntime.queryInterface(XComponent.class, doc);
			component.dispose();
		}
	}

	public static void convert(ConvertInfo info) throws Exception {
		XComponent doc = null;
		try {
			XComponentContext context = createContext();
			XComponentLoader compLoader = createLoader(context);
			doc = loadDocument(compLoader, info.getInputPath());
			convertDocument(doc, info.getOutputPath(), info.getConvertType());
		} finally {
			try {
				if (doc != null) {
					closeDocument(doc);
				}
			} catch (Exception e) {
				log.error("关闭文档时出现异常", e);
			}
		}
	}
}
