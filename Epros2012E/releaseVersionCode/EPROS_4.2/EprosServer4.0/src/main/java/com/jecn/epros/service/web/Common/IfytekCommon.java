package com.jecn.epros.service.web.Common;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.common.history.DocumentControlFollow;
import com.jecn.epros.domain.file.FileBaseInfoBean;
import com.jecn.epros.domain.file.FileDetailWebBean;
import com.jecn.epros.domain.inventory.FlowInventoryBean;
import com.jecn.epros.domain.inventory.OrgInventoryRelatedBean;
import com.jecn.epros.domain.journal.UserVisitsResult;
import com.jecn.epros.domain.process.FlowSearchResultBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.propose.Propose;
import com.jecn.epros.domain.propose.ProposeTopic;
import com.jecn.epros.domain.rule.RuleInfoBean;
import com.jecn.epros.domain.system.RoleInfoBean;
import com.jecn.epros.domain.task.MyTaskBean;
import com.jecn.epros.domain.task.TempAuditPeopleBean;
import com.jecn.epros.mapper.SelectFileMapper;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.web.JecnIflytekSyncUserEnum;
import com.jecn.epros.util.ConfigUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.nio.charset.Charset;
import java.util.*;

public class IfytekCommon {
    private static final Logger log = Logger.getLogger(IfytekCommon.class);


    private static ResourceBundle iflytekConfig;

    static {
        iflytekConfig = ResourceBundle.getBundle("config/iflytek/iflytekConfig");
    }

    public static String getValue(String key) {
        return iflytekConfig.getString(key);
    }

    public static String sendPost(String url, String json, String encoding) throws Exception {
        String body = "";

        // 创建httpclient对象
        CloseableHttpClient client = HttpClients.createDefault();
        // 创建post方式请求对象
        HttpPost httpPost = new HttpPost(url);

        // 设置参数到请求对象中
        httpPost.setEntity(new StringEntity(json, Charset.forName("UTF-8")));

        System.out.println("请求地址：" + url);

        // 设置header信息
        // 指定报文头【Content-type】、【User-Agent】
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
        httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

        // 执行请求操作，并拿到结果（同步阻塞）
        CloseableHttpResponse response = client.execute(httpPost);
        // 获取结果实体
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // 按指定编码转换结果实体为String类型
            body = EntityUtils.toString(entity, encoding);
        }
        EntityUtils.consume(entity);
        // 释放链接
        response.close();
        return body;
    }

    /**
     * 获取map 值
     *
     * @param map
     * @param key
     * @return string
     */
    public static String getMapValueStr(Map map, String key) {
        if (map != null && !map.isEmpty()) {
            return map.get(key).toString();
        }
        return "";
    }

    /**
     * 科大讯飞专用 根据登陆类型获取  人员字段名称
     */

    public static String getUserField() {
        //ConfigUtils.isIflytekLogin() ? "LOGIN_NAME" : "TRUE_NAME"
        return "TRUE_NAME";
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     * type 0 是人员 1 是岗位
     */
    public static String getIflytekUserName(String loginName, int type) {
        if (type == 1) {
            return "";
        }
        if (StringUtils.isNotBlank(loginName)) {
            List list = new ArrayList();
            list.add(loginName);
            if (ConfigUtils.isIflytekLogin()) {
                Map doMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUserByLoginName(list);
                return getMapValue(doMap, loginName);
            }
            return loginName;
        }
        return "";
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekUserName(List<Map<String, Object>> maps, String trueNameKey, String loginNameKey) {
        if (ConfigUtils.isIflytekLogin()) {
            List list = new ArrayList();
            for (Map<String, Object> mapBean : maps) {
                list.add(getMapValueStr(mapBean, loginNameKey));
            }
            if (list.isEmpty()) {
                return;
            }
            Map<String, String> nameMaps = getUserNameByLoginName(list);
            if (nameMaps != null && !nameMaps.isEmpty()) {
                for (Map<String, Object> mapBean : maps) {
                    if ("0".equals(getMapValueStr(mapBean, "type"))) {
                        mapBean.put(trueNameKey, getMapValue(nameMaps, mapBean.get(loginNameKey).toString()));
                    }
                }
            }
        }
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekUserName(List<TreeNode> treeNodes, Map<String, String> userMap) {
        if (treeNodes != null && !treeNodes.isEmpty())
            if (ConfigUtils.isIflytekLogin()) {
                for (TreeNode bean : treeNodes) {
                    bean.setText(getMapValue(userMap, bean.getLoginName()));
                }
            }
    }

    /**
     * 人员选择，根据姓名查询 特殊处理
     *
     * @param treeNodes
     * @param map
     * @return List<TreeNode>
     */
    public static List<TreeNode> getSearchResultByTrueName(SelectFileMapper selectFileMapper, Map<String, Object> map) {
        //科大讯飞 查询人员需要 根据真实姓名查询 登录名后 根据登录名查询具体人员
        List<String> list = new ArrayList();
        list.add(map.get("name") != null ? map.get("name").toString() : "");
        // key：登录名，value：真实姓名
        Map<String, String> userMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUserTrueName(list);
        // 获取根据真实姓名查询返回的登录名
        String trueName = map.get("name").toString();

        List<String> searchLoginNames = new ArrayList();
        for (Map.Entry<String, String> nameBean : userMap.entrySet()) {
            if (nameBean.getValue().equals(trueName)) {
                searchLoginNames.add(nameBean.getKey());
            }
        }
        if (searchLoginNames.isEmpty()) {
            return new ArrayList<>();
        }
        map.put("name", searchLoginNames);

        List<TreeNode> treeNodes = TreeNodeUtils.setNodeType(selectFileMapper.searchPeople(map), TreeNodeUtils.TreeType.people);
        // 返回结果集
        getIflytekUserName(treeNodes, userMap);
        return treeNodes;
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekTaskUserName(List<MyTaskBean> tasks) {
        if (tasks == null || tasks.isEmpty() || !ConfigUtils.isIflytekLogin()) {
            return;
        }
        Set<String> loginNames = new HashSet();
        for (MyTaskBean bean : tasks) {
            loginNames.add(bean.getCreateName());
            if (StringUtils.isNotBlank(bean.getResPeopleName())) {
                loginNames.add(bean.getResPeopleName());
            }
        }
        List<String> names = new ArrayList();
        names.addAll(loginNames);

        // key：登录名；value：姓名
        Map<String, String> cNameMaps = getUserNameByLoginName(names);
        if (cNameMaps != null && !cNameMaps.isEmpty()) {
            for (MyTaskBean bean : tasks) {
                bean.setCreateName(getMapValue(cNameMaps, bean.getCreateName()));
                if (StringUtils.isNotBlank(bean.getResPeopleName())) {
                    bean.setResPeopleName(getMapValue(cNameMaps, bean.getResPeopleName()));
                }
            }
        }
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekHistoryUserName(List<DocumentControlFollow> doc) {
        if (doc != null && !doc.isEmpty()) {
            if (ConfigUtils.isIflytekLogin()) {
                List list = new ArrayList();
                for (DocumentControlFollow bean : doc) {
                    list.add(bean.getName());
                }
                if (list.isEmpty()) {
                    return;
                }
                Map<String, String> rNameMaps = getUserNameByLoginName(list);
                if (rNameMaps != null && !rNameMaps.isEmpty()) {
                    for (DocumentControlFollow bean : doc) {
                        bean.setName(getMapValue(rNameMaps, bean.getName()));
                    }
                }
            }
        }
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekhProposeTopicUserName(List<ProposeTopic> topic) {
        if (topic != null && !topic.isEmpty()) {
            if (ConfigUtils.isIflytekLogin()) {
                List list = new ArrayList();
                for (ProposeTopic bean : topic) {
                    list.add(bean.getHandlePeopleName());
                }
                if (list.isEmpty()) {
                    return;
                }
                Map<String, String> rNameMaps = getUserNameByLoginName(list);
                if (rNameMaps != null && !rNameMaps.isEmpty()) {
                    for (ProposeTopic bean : topic) {
                        bean.setHandlePeopleName(getMapValue(rNameMaps, bean.getHandlePeopleName()));
                    }
                }
            }
        }
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekhProposeUserName(List<Propose> proposes) {
        if (proposes != null && !proposes.isEmpty()) {
            if (ConfigUtils.isIflytekLogin()) {
                List list = new ArrayList();
                for (Propose bean : proposes) {
                    list.add(bean.getCreatePeopoleName());
                }
                if (list.isEmpty()) {
                    return;
                }
                Map<String, String> rNameMaps = getUserNameByLoginName(list);
                if (rNameMaps != null && !rNameMaps.isEmpty()) {
                    for (Propose bean : proposes) {
                        bean.setCreatePeopoleName(getMapValue(rNameMaps, bean.getCreatePeopoleName()));
                    }
                }
            }
        }
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekhRoleUserName(List<RoleInfoBean> roleInfos) {
        if (roleInfos != null && !roleInfos.isEmpty()) {
            if (ConfigUtils.isIflytekLogin()) {
                List list = new ArrayList();
                for (RoleInfoBean bean : roleInfos) {
                    list.add(bean.getUserName());
                }
                if (list.isEmpty()) {
                    return;
                }
                Map<String, String> rNameMaps = getUserNameByLoginName(list);
                if (rNameMaps != null && !rNameMaps.isEmpty()) {
                    for (RoleInfoBean bean : roleInfos) {
                        bean.setUserName(getMapValue(rNameMaps, bean.getUserName()));
                    }
                }
            }
        }
    }

    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekhUserVisitsName(List<UserVisitsResult> userVisitsResult) {
        if (userVisitsResult != null && !userVisitsResult.isEmpty()) {
            if (ConfigUtils.isIflytekLogin()) {
                List list = new ArrayList();
                for (UserVisitsResult bean : userVisitsResult) {
                    list.add(bean.getPeopleName());
                }
                if (list.isEmpty()) {
                    return;
                }
                Map<String, String> rNameMaps = getUserNameByLoginName(list);
                if (rNameMaps != null && !rNameMaps.isEmpty()) {
                    for (UserVisitsResult bean : userVisitsResult) {
                        bean.setPeopleName(getMapValue(rNameMaps, bean.getPeopleName()));
                    }
                }
            }
        }
    }


    /**
     * 科大讯飞 需要根据登录名重新查询人员名称
     */
    public static void getIflytekUserName(FileBaseInfoBean baseInfo) {
        if (baseInfo != null) {
            if (ConfigUtils.isIflytekLogin()) {
                String commissionerName = getIflytekUserName(baseInfo.getCommissionerName(), 0);
                baseInfo.setCommissionerName(commissionerName);
                String trueName = getIflytekUserName(baseInfo.getTrueName(), 0);
                baseInfo.setTrueName(trueName);
            }
        }

    }

    /**
     * 科大讯飞 需要根据真实姓名重新查询人员登录名
     */
    public static List<String> getIflytekUserLoginName(String trueName) {
        if (StringUtils.isNotBlank(trueName)) {
            List list = new ArrayList();
            list.add(trueName);
            if (ConfigUtils.isIflytekLogin()) {
                Map doMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUserTrueName(list);
                if (doMap != null && !doMap.isEmpty()) {
                    return getMapNameByTrueName(doMap, trueName);
                }
            }
        }
        return null;
    }


    /**
     * 公用方法 每一百条 请求一次接口
     */

    public static Map<String, String> getUserNameByLoginName(List<String> loginNames) {
        List<String> names = new ArrayList<>();
        Map<String, String> doMap = new HashedMap();
        for (String login : loginNames) {
            names.add(login);
            if (names.size() == 100) {
                doMap.putAll(JecnIflytekSyncUserEnum.INSTANCE.getMapUserByLoginName(names));
                names = new ArrayList<>();
            }
        }
        if (names != null && !names.isEmpty()) {
            doMap.putAll(JecnIflytekSyncUserEnum.INSTANCE.getMapUserByLoginName(names));
        }
        return doMap;
    }

    public static String getMapValue(Map<String, String> map, String loginName) {
        return map.get(loginName) == null ? loginName : map.get(loginName);
    }

    public static List<String> getMapNameByTrueName(Map<String, String> map, String trueName) {
        List<String> names = new ArrayList();
        if (map != null && !map.isEmpty() && StringUtils.isNotBlank(trueName)) {
            for (Map.Entry nameBean : map.entrySet()) {
                if (nameBean.getValue().equals(trueName)) {
                    names.add(nameBean.getKey().toString());
                }
            }
        }
        return names;
    }

    //科大讯飞 根据登录名重新查询人员名称 流程清单 流程架构清单
    public static void ifytekCommonGetProcessUserName(List<FlowInventoryBean> processes) {
        if (ConfigUtils.isIflytekLogin()) {
            List loginNames = new ArrayList();
            for (FlowInventoryBean bean : processes) {
                if (StringUtils.isNotBlank(bean.getResPeople())) {
                    loginNames.add(bean.getResPeople());
                }
                if (StringUtils.isNotBlank(bean.getGuardianPeople())) {
                    loginNames.add(bean.getGuardianPeople());
                }
                if (StringUtils.isNotBlank(bean.getDraftPeople())) {
                    loginNames.add(bean.getDraftPeople());
                }
            }
            if (loginNames.isEmpty()) {
                return;
            }
            Map<String, String> resMap = null;
            if (loginNames != null && !loginNames.isEmpty()) {
                resMap = getUserNameByLoginName(loginNames);
            }
            for (FlowInventoryBean bean : processes) {
                bean.setResPeople(getMapValue(resMap, bean.getResPeople()));
                bean.setGuardianPeople(getMapValue(resMap, bean.getGuardianPeople()));
                bean.setDraftPeople(getMapValue(resMap, bean.getDraftPeople()));
            }
        }
    }

    //科大讯飞 根据登录名重新查询人员名称  制度清单
    public static void ifytekCommonGetRuleUserName(List<RuleInfoBean> rule) {
        if (ConfigUtils.isIflytekLogin()) {
            //责任人
            List loginNames = new ArrayList();
            for (RuleInfoBean bean : rule) {
                if (StringUtils.isNotBlank(bean.getResponsibleName())) {
                    loginNames.add(bean.getResponsibleName());
                }
            }
            if (loginNames.isEmpty()) {
                return;
            }
            Map<String, String> resMap = null;
            if (loginNames != null && !loginNames.isEmpty()) {
                resMap = getUserNameByLoginName(loginNames);
            }
            for (RuleInfoBean bean : rule) {
                bean.setResponsibleName(getMapValue(resMap, bean.getResponsibleName()));
            }
        }
    }

    //科大讯飞 根据登录名重新查询人员名称
    public static void ifytekCommonGetFileUserName(List<FileDetailWebBean> file) {
        if (ConfigUtils.isIflytekLogin()) {
            //责任人
            List loginNames = new ArrayList();
            for (FileDetailWebBean bean : file) {
                if (StringUtils.isNotBlank(bean.getResponsibleName())) {
                    loginNames.add(bean.getResponsibleName());
                }
                if (StringUtils.isNotBlank(bean.getCreator())) {
                    loginNames.add(bean.getCreator());
                }

            }
            if (loginNames.isEmpty()) {
                return;
            }
            Map<String, String> resMap = null;
            if (loginNames != null && !loginNames.isEmpty()) {
                resMap = getUserNameByLoginName(loginNames);
            }
            for (FileDetailWebBean bean : file) {
                bean.setResponsibleName(getMapValue(resMap, bean.getResponsibleName()));
                bean.setCreator(getMapValue(resMap, bean.getCreator()));
            }
        }
    }

    //科大讯飞 根据登录名重新查询人员名称 部门
    public static void ifytekCommonGetOrgUserName(List<OrgInventoryRelatedBean> flows) {
        if (ConfigUtils.isIflytekLogin()) {
            //责任人
            List loginNames = new ArrayList();
            for (OrgInventoryRelatedBean bean : flows) {
                if (StringUtils.isNotBlank(bean.getResPeople())) {
                    loginNames.add(bean.getResPeople());
                }
                if (StringUtils.isNotBlank(bean.getGuardianPeople())) {
                    loginNames.add(bean.getGuardianPeople());
                }
                if (StringUtils.isNotBlank(bean.getDraftPeople())) {
                    loginNames.add(bean.getDraftPeople());
                }
            }

            if (loginNames.isEmpty()) {
                return;
            }
            Map<String, String> resMap = null;
            if (loginNames != null && !loginNames.isEmpty()) {
                resMap = getUserNameByLoginName(loginNames);
            }

            for (OrgInventoryRelatedBean bean : flows) {
                bean.setResPeople(getMapValue(resMap, bean.getResPeople()));
                bean.setGuardianPeople(getMapValue(resMap, bean.getGuardianPeople()));
                bean.setDraftPeople(getMapValue(resMap, bean.getDraftPeople()));
            }
        }
    }


    //科大讯飞 根据登录名重新查询人员名称 岗位 根据人员 对象
    public static void ifytekCommonGetPosUserName(List<JecnUser> user) {
        if (ConfigUtils.isIflytekLogin()) {
            List loginNames = new ArrayList();
            for (JecnUser bean : user) {
                if (StringUtils.isNotBlank(bean.getLoginName())) {
                    loginNames.add(bean.getLoginName());
                }
            }
            if (loginNames.isEmpty()) {
                return;
            }
            Map<String, String> resMap = getUserNameByLoginName(loginNames);
            for (JecnUser bean : user) {
                String trueName = resMap.get(bean.getLoginName());
                bean.setName(StringUtils.isBlank(trueName) ? bean.getLoginName() : resMap.get(bean.getLoginName()));
            }
        }
    }

    /**
     * 任务各阶段审批人
     *
     * @param listAuditPeopleBean
     */
    public static void ifytekCommonTempAuditPeopleBean(List<TempAuditPeopleBean> listAuditPeopleBean) {
        if (!ConfigUtils.isIflytekLogin() || listAuditPeopleBean == null || listAuditPeopleBean.isEmpty()) {
            return;
        }
        List<String> loginNames = new ArrayList<>();
        for (TempAuditPeopleBean auditPeopleBean : listAuditPeopleBean) {
            if (StringUtils.isBlank(auditPeopleBean.getAuditName())) {
                continue;
            }
            String[] arr = auditPeopleBean.getAuditName().split("\n");
            if (arr.length > 1) {
                for (String arrName : arr) {
                    loginNames.add(arrName);
                }
            } else {
                loginNames.add(auditPeopleBean.getAuditName());
            }
        }
        if (loginNames.isEmpty()) {
            return;
        }
        Map<String, String> userMap = IfytekCommon.getUserNameByLoginName(loginNames);

        for (TempAuditPeopleBean auditPeopleBean : listAuditPeopleBean) {
            if (StringUtils.isBlank(auditPeopleBean.getAuditName())) {
                continue;
            }
            String[] arr = auditPeopleBean.getAuditName().split("\n");
            String trueName = null;
            if (arr.length > 1) {
                for (String arrName : arr) {
                    if (StringUtils.isBlank(trueName)) {
                        trueName = getMapValue(userMap, arrName);
                    } else {
                        trueName += "\n" + getMapValue(userMap, arrName);
                    }
                }
            } else {
                trueName = getMapValue(userMap, auditPeopleBean.getAuditName());
            }
            auditPeopleBean.setAuditName(trueName);
        }
    }

    /**
     * 流程搜索
     *
     * @param resultBeans
     */
    public static void iflytekCommonSearchFlow(List<FlowSearchResultBean> resultBeans) {
        if (!ConfigUtils.isIflytekLogin() || resultBeans == null || resultBeans.isEmpty()) {
            return;
        }
        List<String> loginNames = new ArrayList<>();
        for (FlowSearchResultBean result : resultBeans) {
            loginNames.add(result.getResPeople());
        }
        if (loginNames.isEmpty()) {
            return;
        }
        Map<String, String> userMap = IfytekCommon.getUserNameByLoginName(loginNames);

        for (FlowSearchResultBean result : resultBeans) {
            result.setResPeople(getMapValue(userMap, result.getResPeople()));
        }
    }


    public static List<String> iflytekCommonLisStrByLoginName(List<String> loginName) {
        if (loginName == null || !loginName.isEmpty()) {
            return new ArrayList<String>();
        }
        Map<String, String> mapBean = IfytekCommon.getUserNameByLoginName(loginName);
        List<String> trNames = new ArrayList<String>();
        if (mapBean != null && !mapBean.isEmpty()) {
            for (String nameBean : loginName) {
                trNames.add(IfytekCommon.getMapValue(mapBean, nameBean));
            }
        }
        return trNames;
    }

}
