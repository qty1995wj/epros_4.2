package epros.draw.gui.line;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.data.line.JecnLineSegmentDataCloneable;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.line.resize.JecnManhattanLineResizePanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.swing.JecnManhattanRouter;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.zoom.JecnWorkflowZoomProcess;
import epros.draw.util.DrawCommon;

/**
 * 
 * 连接线（带箭头连接线、不带箭头连接线）基类
 * 
 * @author ZHOUXY
 */
public abstract class JecnBaseManhattanLinePanel extends JecnBaseLinePanel {
	/** 箭头默认宽度（横向箭头） 纵向时宽高互换 */
	protected int arrowW = 10;
	/** 箭头默认高度 */
	protected int arrowH = 8;
	/** 是否显示线段选中点 True 显示，false 隐藏 */
	protected boolean isShowResizeHandle = false;
	/** 线是否拖动过 (拖动过不为曼哈顿算法) */
	protected boolean isDrraged = false;
	/** 选中点：开始点 */
	protected JecnManhattanLineResizePanel lineStartResizePanel = null;
	/** 选中点：结束点 */
	protected JecnManhattanLineResizePanel lineEndResizePanel = null;
	/** 双击连接线显示文本 */
	protected JecnLineTextPanel textPanel = null;

	/** 复制、剪切时：记录连接线当前状态下的连接线小线段数据集合（克隆后的小线段） */
	public List<LineSegment> curLineSegmentsClone = new ArrayList<LineSegment>();

	/** 流程架构图 箭头连接线【不连接图形情况下开始点和结束点】 */
	private Point arrowStartPoint = null;
	private Point arrowEndPoint = null;

	public JecnBaseManhattanLinePanel(JecnManhattanLineData flowElementData) {
		super(flowElementData);
		initComponts();
	}

	public void initComponts() {
		// 曼哈顿线开始点选中点
		lineStartResizePanel = JecnManhattanLineResizePanel.createStartManhattLineResizePanel(this);
		// 曼哈顿线结束点选中点
		lineEndResizePanel = JecnManhattanLineResizePanel.createEndManhattLineResizePanel(this);
	}

	/**
	 * 根据路由算法画曼哈顿线段
	 * 
	 */
	public void paintLineByRouter() {
		JecnManhattanRouter manhattanRouter = new JecnManhattanRouter();
		if (this.getOriginalStartPoint() == null || this.getOriginalEndPoint() == null
				|| JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		List<JecnManhattanRouterResultData> listRouterData = manhattanRouter.route(this.getOriginalStartPoint(), this
				.getFlowElementData().getStartEditPointType(), this.getOriginalEndPoint(), this.getFlowElementData()
				.getEndEditPointType());
		if (listRouterData == null || listRouterData.size() == 0 || lineSegments == null) {
			return;
		}
		this.isDrraged = false;
		// 根据路由算法获取的数据重新生成小线段
		drawLineSegmentByListData(listRouterData);
		// 是否执行放大缩小算法
		if (JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale() != 1.0) {
			new JecnWorkflowZoomProcess(JecnDrawMainPanel.getMainPanel().getWorkflow()).zoomManhattanLine(this);
		}
	}

	/**
	 * 导入、片段连接线画线
	 * 
	 * @param linePanel
	 */
	public void paintImportManLine(JecnBaseManhattanLinePanel linePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || linePanel.getLineSegments() == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		for (LineSegment lineSegment : linePanel.getLineSegments()) {
			desktopPane.add(lineSegment);
			lineSegment.drawArc(lineSegment);
			// 设置小线段层级
			setLineSegmentLayer(lineSegment, desktopPane);
		}
		linePanel.addArrowPanel();
	}

	/**
	 * 拖动曼哈顿线段端点时执行的路由算法
	 * 
	 * @param fromPt
	 *            开始点（出）
	 * @param fromDir
	 * @param toPt
	 *            结束点 （进）
	 * @param toDir
	 */
	public void paintDraggedLinePort(Point fromPt, EditPointType fromDir, Point toPt, EditPointType toDir) {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnManhattanRouter manhattanRouter = new JecnManhattanRouter();
		List<JecnManhattanRouterResultData> listRouterData = manhattanRouter.route(fromPt, fromDir, toPt, toDir);
		if (listRouterData == null || listRouterData.size() == 0 || lineSegments == null) {
			return;
		}
		this.isDrraged = false;
		// 根据路由算法获取的数据重新生成小线段
		drawLineSegmentByListData(listRouterData);
		// 设置连接线文字区域层级
		setLayerTextPanel(JecnDrawMainPanel.getMainPanel().getWorkflow());
	}

	/**
	 * 根据数据对象添加小线段
	 * 
	 * @param listRouterData
	 *            记录线段开始点和结束点数据
	 */
	public void drawLineSegmentByListData(List<JecnManhattanRouterResultData> listRouterData) {
		if (listRouterData == null || listRouterData.size() == 0) {// 不存在线段数据集
			return;
		}
		JecnDrawDesktopPane curWorflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 清空小线段数据对象
		this.getFlowElementData().getLineSegmentDataList().clear();
		// 情况面板上小线段
		this.disposeLines();
		for (JecnManhattanRouterResultData jecnManhattanRouterResultData : listRouterData) {
			JecnLineSegmentDataCloneable lineSegmentDataClone = new JecnLineSegmentDataCloneable();
			// 获取线段原始状态下 位置点和坐标点数据对象
			lineSegmentDataClone.reSetAttributes(jecnManhattanRouterResultData.getStartPoint(),
					jecnManhattanRouterResultData.getEndPint(), this.getFlowElementData().getBodyWidth(), 1.0);
			// 获取线段数据层对象
			JecnLineSegmentData lineSegmentData = new JecnLineSegmentData(lineSegmentDataClone);

			this.getFlowElementData().getLineSegmentDataList().add(lineSegmentData);
			// 创建连接线对象
			createLineSegmentLine(lineSegmentData, jecnManhattanRouterResultData.isEndLine());
		}
		// 设置连接线文字区域层级
		setLayerTextPanel(curWorflow);
		// 设置连接线编辑区域位置、大小
		this.insertTextToLine();
		// 添加箭头
		addArrowPanel();
	}

	public void addArrowAndTextPanel() {
		// 设置连接线文字区域层级
		addTempPanel();
		// 添加箭头
		addArrowPanel();
	}

	/**
	 * 创建小线段对象添加到面板
	 * 
	 * @param lineSegmentData
	 *            小线段数据对象
	 * @param isEndLine
	 *            是否为结束线段标识 True结束线段
	 */
	public void createLineSegmentLine(JecnLineSegmentData lineSegmentData, boolean isEndLine) {
		JecnDrawDesktopPane curWorflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (curWorflow == null || lineSegmentData == null) {
			return;
		}
		// 创建连接线对象
		LineSegment lineSegment = new LineSegment(this, lineSegmentData);

		// 添加到连接线集合
		lineSegments.add(lineSegment);
		// 结束线段设置箭头默认大小
		if (isManLine() && isEndLine) {
			lineSegment.setArrowW(arrowW);
			lineSegment.setArrowH(arrowH);
		}
		// 设置小线段层级
		setLineSegmentLayer(lineSegment, curWorflow);
		// 画弧
		lineSegment.drawArc(lineSegment);
		// 添加到面板
		curWorflow.add(lineSegment);
	}

	/**
	 * 是否是带箭头的连接线
	 * 
	 * @return
	 */
	public boolean isManLine() {
		if (MapElemType.ManhattanLine.equals(this.getFlowElementData().getMapElemType())) {
			return true;
		}
		return false;
	}

	/**
	 * 撤销恢复添加连接线小线段
	 * 
	 * @param lineSegmentDataList
	 *            List<JecnLineSegmentData> 小线段数据层 记录线段开始点和结束点数据
	 */
	public void drawLineSegmentByList(List<JecnLineSegmentData> lineSegmentDataList) {
		if (lineSegmentDataList == null || lineSegmentDataList.size() == 0) {// 不存在线段数据集
			return;
		}
		JecnDrawDesktopPane curWorflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 清空小线段数据对象
		this.getFlowElementData().getLineSegmentDataList().clear();
		// 情况面板上小线段
		this.disposeLines();
		for (JecnLineSegmentData lineSegmentData : lineSegmentDataList) {
			Point startPoint = lineSegmentData.getOriginalStartXYData().getPoint();
			Point endPoint = lineSegmentData.getOriginalEndXYData().getPoint();
			// 获取线段原始状态下 位置点和坐标点数据对象
			lineSegmentData.getLineSegmentData().reSetAttributes(startPoint, endPoint,
					this.getFlowElementData().getBodyWidth(), 1.0);

			this.getFlowElementData().getLineSegmentDataList().add(lineSegmentData);
			// 创建连接线对象
			LineSegment lineSegment = new LineSegment(this, lineSegmentData);

			// 添加到连接线集合
			lineSegments.add(lineSegment);
			// // 结束线段设置箭头默认大小
			// if (MapElemType.ManhattanLine.equals(this.getFlowElementData()
			// .getMapElemType())
			// && jecnManhattanRouterResultData.isEndLine()) {
			// lineSegment.setArrowW(arrowW);
			// lineSegment.setArrowH(arrowH);
			// }
			// 设置小线段层级
			setLineSegmentLayer(lineSegment, curWorflow);
			lineSegment.drawArc(lineSegment);
			// 添加到面板
			curWorflow.add(lineSegment);
		}
		// 设置连接线文字区域层级
		setLayerTextPanel(curWorflow);
		// 设置连接线编辑区域位置、大小
		this.insertTextToLine();
		// 添加箭头
		addArrowPanel();
	}

	/**
	 * 添加小线段到线段集合
	 * 
	 * @param startPoint
	 *            Point小线段开始点
	 * @param endPoint
	 *            Point小线段结束点
	 */
	public JecnLineSegmentData addLineSegment(Point startPoint, Point endPoint) {
		JecnLineSegmentDataCloneable lineSegmentDataClone = new JecnLineSegmentDataCloneable();
		// 获取线段原始状态下 位置点和坐标点数据对象
		lineSegmentDataClone.reSetAttributes(startPoint, endPoint, this.getFlowElementData().getBodyWidth(), 1.0);
		// 获取线段数据层对象
		JecnLineSegmentData lineSegmentData = new JecnLineSegmentData(lineSegmentDataClone);
		this.getFlowElementData().getLineSegmentDataList().add(lineSegmentData);
		// 创建连接线对象
		LineSegment lineSegment = new LineSegment(this, lineSegmentData);
		this.getLineSegments().add(lineSegment);
		return lineSegmentData;
	}

	/**
	 * 设置连接线文字区域层级
	 * 
	 * @param curWorflow
	 */
	private void setLayerTextPanel(JecnDrawDesktopPane curWorflow) {
		if (curWorflow == null) {
			return;
		}
		if (textPanel != null && this.getFlowElementData().getFigureText() != null) {
			curWorflow.setLayer(textPanel, curWorflow.getMaxZOrderIndex() + 1);
		}
	}

	/**
	 * 设置小线段层级
	 * 
	 * @param lineSegment
	 *            当前线段
	 * @param workflow
	 *            面板
	 */
	private void setLineSegmentLayer(LineSegment lineSegment, JecnDrawDesktopPane workflow) {
		workflow.setLayer(lineSegment, this.getFlowElementData().getZOrderIndex());
	}

	protected void addArrowPanel() {

	}

	/**
	 * 重画箭头
	 */
	protected void reDrawArrow() {

	}

	protected void deleteArrowPanel() {

	}

	/**
	 * 清空面板小线段和小线段相关属性
	 * 
	 */
	public void disposeLines() {
		if (JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane curWorkflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		for (int i = 0; i < lineSegments.size(); i++) {
			LineSegment lineSegment = lineSegments.get(i);
			// 面板删除连接线
			curWorkflow.remove(lineSegment);
			// 删除圆弧
			lineSegment.removeDrawArc(lineSegment);
			// 清空原始连接线小线段选中点
			curWorkflow.remove(lineSegment.lineResizePanelStart);
			curWorkflow.remove(lineSegment.lineResizePanelCenter);
			curWorkflow.remove(lineSegment.lineResizePanelEnd);

		}
		// 删除箭头
		deleteArrowPanel();
		lineSegments.clear();
	}

	/**
	 * 
	 * 放大缩清除面板线的选中点
	 */
	private void clearResizePanel() {
		for (int i = 0; i < lineSegments.size(); i++) {
			LineSegment lineSegment = lineSegments.get(i);
			// 清空原始连接线小线段选中点
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(lineSegment.lineResizePanelStart);
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(lineSegment.lineResizePanelCenter);
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(lineSegment.lineResizePanelEnd);
			// 判断是否存在圆弧,存在则删除
			lineSegment.removeDrawArc(lineSegment);
			// 重新添加圆弧
			lineSegment.drawArc(lineSegment);

		}
	}

	/**
	 * 获取联系当前状态下的开始点
	 * 
	 * @return
	 */
	public Point getStartPoint() {
		if (startFigure == null) {
			return arrowStartPoint;
		}
		return startFigure.getEditPortPanel(this.getFlowElementData().getStartEditPointType(), startFigure)
				.getZoomCenterPoint();
	}

	/**
	 * 获取联系当前状态下的开始点
	 * 
	 * @return
	 */
	public Point getOriginalStartPoint() {
		if (startFigure == null) {
			return arrowStartPoint;
		}
		return startFigure.getEditPortPanel(this.getFlowElementData().getStartEditPointType(), startFigure)
				.getOriginalCenterPoint();
	}

	/**
	 * 获取联系当前状态下的开始点
	 * 
	 * @return
	 */
	public Point getEndPoint() {
		if (endFigure == null) {
			return arrowEndPoint;
		}
		return endFigure.getEditPortPanel(this.getFlowElementData().getEndEditPointType(), endFigure)
				.getZoomCenterPoint();
	}

	/**
	 * 获取联系当前状态下的开始点
	 * 
	 * @return
	 */
	public Point getOriginalEndPoint() {
		if (endFigure == null) {
			return arrowEndPoint;
		}
		return endFigure.getEditPortPanel(this.getFlowElementData().getEndEditPointType(), endFigure)
				.getOriginalCenterPoint();
	}

	public JecnManhattanLineData getFlowElementData() {
		return (JecnManhattanLineData) flowElementData;
	}

	public double getWorkflowScale() {
		return flowElementData.getWorkflowScale();
	}

	public EditPointType getEndEditPointType() {
		return this.getFlowElementData().getEndEditPointType();
	}

	public EditPointType getStartEditPointType() {
		return this.getFlowElementData().getStartEditPointType();
	}

	/**
	 * 拖动线段时 生成虚线
	 * 
	 * @param pPoint
	 * @return
	 */
	public JecnTempLine addTempPoint(Point pPoint) {
		JecnTempLine line = null;
		if (tempOldPoint != null) {
			// 获取当前状态下的开始点和结束点
			Point curStartPoint = getCurTempXY(tempOldPoint);
			Point curEndPoint = getCurTempXY(pPoint);
			// 画线
			line = drawTempLine(curStartPoint.x, curStartPoint.y, curEndPoint.x, curEndPoint.y);
			// 从左向右的顺序
			if (tempOldPoint.x >= pPoint.x) {
				// 100%时 开始点 和结束点
				line.setOriginalStartPt(new Point(pPoint.x, pPoint.y));
				line.setOriginalEndPt(new Point(tempOldPoint.x, tempOldPoint.y));

				// 当前比例下开始点和结束点
				line.setTempStartPt(curStartPoint);
				line.setTempEndPt(curEndPoint);
			} else {
				// 100%时 开始点 和结束点
				line.setOriginalStartPt(new Point(tempOldPoint.x, tempOldPoint.y));
				line.setOriginalEndPt(new Point(pPoint.x, pPoint.y));
				// 当前比例下开始点和结束点
				line.setTempStartPt(curEndPoint);
				line.setTempEndPt(curStartPoint);
			}

			// JecnDrawMainPanel.getMainPanel().getWorkflow().add(comp);
			// 添加设置层级
			JecnDrawMainPanel.getMainPanel().getWorkflow().setLayer(line, flowElementData.getZOrderIndex());
			JecnDrawMainPanel.getMainPanel().getWorkflow().getTempLines().add(line);
		}
		tempOldPoint = pPoint;
		return line;
	}

	/**
	 * 拖动连接线生成虚线的小线段
	 * 
	 * @param startX
	 *            开始点的横坐标
	 * @param startY
	 *            开始点的纵坐标
	 * @param endX
	 *            结束点的横坐标
	 * @param endY
	 *            结束点的纵坐标
	 * @return
	 */
	public JecnTempLine drawTempLine(int startX, int startY, int endX, int endY) {
		JecnTempLine newLine = new JecnTempLine();
		// 虚线颜色红色
		newLine.setBackground(Color.RED);
		int lineSizeWidth = flowElementData.getBodyWidth() / 2 + 1;
		// 临时小线段集合，比如拖动过程中生成线段，拖动结束后清除该临时线段
		List<JecnTempLine> tempLines = JecnDrawMainPanel.getMainPanel().getWorkflow().getTempLines();
		if (startX == endX) {
			if (tempLines.size() >= 1 && startY > endY && lineSizeWidth > 1) {
				startY = startY + lineSizeWidth;
			}
			// 获取当前比例下 线的大小和位置点
			newLine.setBounds(startX, Math.min(startY, endY), lineSizeWidth, Math.abs(startY - endY));
		}
		if (startY == endY) {
			if (tempLines.size() >= 1 && startX > endX && lineSizeWidth > 1) {
				startX = startX + lineSizeWidth;
			}
			// 获取当前比例下 线的大小和位置点
			newLine.setBounds(Math.min(startX, endX), endY, Math.abs(startX - endX), lineSizeWidth);
		}
		return newLine;
	}

	/**
	 * 当前面板下执行画线（放大缩小、改变线属性算法） 小线段数据层不变
	 * 
	 * 连接线文字显示框放大缩小在显示框类中paintComponent放大做执行了
	 * 
	 */
	public void paintZoomLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		// 连接线的小线段集合
		List<LineSegment> lineSegments = this.getLineSegments();
		for (int j = 0; j < lineSegments.size(); j++) {
			LineSegment tmpLineSegment = lineSegments.get(j);
			// 去弧

			// 获取放大缩小后的小线段数据对象
			JecnLineSegmentDataCloneable lineSegmentDataCloneable = tmpLineSegment.getLineSegmentData()
					.getLineSegmentData().getZoomedLineSegmentCloneble(desktopPane.getWorkflowScale());

			tmpLineSegment.drawLineSegment(lineSegmentDataCloneable.getStartXInt(), lineSegmentDataCloneable
					.getStartYInt(), lineSegmentDataCloneable.getEndXInt(), lineSegmentDataCloneable.getEndYInt());
		}

		// 放大缩清除面板线的选中点
		clearResizePanel();
		// 重画箭头
		reDrawArrow();
		// 放大连接线显示框大小
		zoomTextPanel();

		if (this.isShowResizeHandle()) {// 是否显示连接线选中点
			this.showManhattanLineResizeHandles();
		}
	}

	/**
	 * 
	 * 添加临时线 到面板上
	 * 
	 */
	public void paintTempLine() {
		// 临时小线段集合，比如拖动过程中生成线段，拖动结束后清除该临时线段
		List<JecnTempLine> tempLines = JecnDrawMainPanel.getMainPanel().getWorkflow().getTempLines();
		for (int i = 0; i < tempLines.size(); i++) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().add(tempLines.get(i));
		}
	}

	/**
	 * 获取当前比例下的位置点
	 * 
	 * @param tempPoint
	 * @return
	 */
	protected Point getCurTempXY(Point tempPoint) {
		Point point = new Point();
		point.setLocation(tempPoint.getX() * this.getWorkflowScale(), tempPoint.getY() * this.getWorkflowScale());
		return point;
	}

	public int getArrowW() {
		return arrowW;
	}

	public int getArrowH() {
		return arrowH;
	}

	/**
	 * 鼠标点击区域是否在曼哈顿线范围内
	 * 
	 * @param px
	 * @param py
	 * @return
	 */
	public boolean lineContainsPoint(int px, int py) {
		// 当前面板的放大缩小倍数
		for (int i = 0; i < lineSegments.size(); i++) {
			LineSegment lineSegment = lineSegments.get(i);
			double startX = lineSegment.getZoomSPoint().getX();
			double startY = lineSegment.getZoomSPoint().getY();
			double endX = lineSegment.getZoomEPoint().getX();
			double endY = lineSegment.getZoomEPoint().getY();
			// 判断是否在区域内 纵线
			if (startX == endX && py <= Math.max(startY, endY) && py >= Math.min(startY, endY)) {
				if (Math.abs(startX - px) <= 10) {
					return true;
				}
			}
			// 判断是否在区域内 横线
			if (startY == endY && px <= Math.max(startX, endX) && px >= Math.min(startX, endX)) {
				if (Math.abs(startY - py) <= 10) {
					return true;
				}
			}
			if (px <= Math.max(startX, endX) && px >= Math.min(startX, endX) && py <= Math.max(startY, endY)
					&& py >= Math.min(startY, endY)) {
				double a, b, x, y;
				a = (double) (startY - endY) / (double) (startX - endX);
				b = (double) startY - a * (double) startX;
				x = (py - b) / a;
				y = a * px + b;
				return (Math.min(Math.abs(x - px), Math.abs(y - py)) <= 10);

			}
		}
		return false;
	}

	/**
	 * 
	 * 点击连接线显示线的选中点
	 * 
	 * @param pLine
	 */
	public void showManhattanLineResizeHandles() {
		// 获取画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 删除线的选中点
		hideManhattanLineResizeHandles();
		// 线段选中点为显示状态
		isShowResizeHandle = true;
		// 连接线层级
		int oRderLayer = this.getFlowElementData().getZOrderIndex();
		// 连接线编辑框层级
		if (textPanel != null) {
			int textPanelLayer = textPanel.getTextPanelOrder();
			if (textPanelLayer > oRderLayer) {
				oRderLayer = textPanelLayer;
			}
		}
		int lineSize = lineSegments.size();
		for (int i = 0; i < lineSize; i++) {// 添加小线段选中点
			// ----------------------------
			LineSegment lineSegment = lineSegments.get(i);
			// lineSegment.setManhattanLine(pLine);
			Point startPos = lineSegment.getZoomSPoint();
			Point endPos = lineSegment.getZoomEPoint();
			// 开始显示点
			lineSegment.lineResizePanelStart.setLocation(DrawCommon.convertDoubleToInt(startPos.getX()
					- lineSegment.lineResizePanelStart.getWidth() / 2.0), DrawCommon.convertDoubleToInt(startPos.getY()
					- lineSegment.lineResizePanelStart.getHeight() / 2.0));
			workflow.add(lineSegment.lineResizePanelStart);
			workflow.setLayer(lineSegment.lineResizePanelStart, oRderLayer + lineSize);
			// 结束显示点
			lineSegment.lineResizePanelEnd.setLocation(DrawCommon.convertDoubleToInt(endPos.getX()
					- lineSegment.lineResizePanelEnd.getWidth() / 2.0), DrawCommon.convertDoubleToInt(endPos.getY()
					- lineSegment.lineResizePanelEnd.getHeight() / 2.0));
			workflow.add(lineSegment.lineResizePanelEnd);
			workflow.setLayer(lineSegment.lineResizePanelEnd, oRderLayer + lineSize);
			// 中心显示点
			lineSegment.lineResizePanelCenter.setLocation(DrawCommon.convertDoubleToInt(startPos.getX()
					+ (endPos.x - startPos.x) / 2.0 - lineSegment.lineResizePanelCenter.getWidth() / 2.0), DrawCommon
					.convertDoubleToInt(startPos.getY() + (endPos.y - startPos.y) / 2.0
							- lineSegment.lineResizePanelCenter.getHeight() / 2.0));
			workflow.add(lineSegment.lineResizePanelCenter);
			workflow.setLayer(lineSegment.lineResizePanelCenter, oRderLayer + lineSize);
		}
		int manResizeW = this.lineEndResizePanel.getWidth();
		int manResizeH = this.lineEndResizePanel.getHeight();

		int startX = this.getStartPoint().x;
		int startY = this.getStartPoint().y;
		int endX = this.getEndPoint().x;
		int endy = this.getEndPoint().y;
		// 添加曼哈顿线段显示点
		this.lineStartResizePanel.setLocation(startX - manResizeW / 2, startY - manResizeH / 2);
		this.lineEndResizePanel.setLocation(endX - manResizeW / 2, endy - manResizeH / 2);

		if (textPanel != null && StringUtils.isNotBlank(textPanel.getLineData().getFigureText())
				&& this.textPanel.getTextResizePanel() != null) {
			workflow.add(this.textPanel.getTextResizePanel());
			workflow.setLayer(this.textPanel.getTextResizePanel(), oRderLayer + lineSize + 2);
		}

		workflow.setLayer(this.lineStartResizePanel, oRderLayer + lineSize + 1);
		workflow.setLayer(this.lineEndResizePanel, oRderLayer + lineSize + 1);
		workflow.add(this.lineStartResizePanel);
		workflow.add(this.lineEndResizePanel);
	}

	/**
	 * 
	 * 隐藏选中点
	 * 
	 * @param pLine
	 */
	public void hideManhattanLineResizeHandles() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		if (isShowResizeHandle) {
			isShowResizeHandle = false;
			// 隐藏小线段选中点
			for (int i = 0; i < lineSegments.size(); i++) {
				LineSegment lineSegment = lineSegments.get(i);
				if (lineSegment.lineResizePanelStart != null) {
					workflow.remove(lineSegment.lineResizePanelStart);
				}
				if (lineSegment.lineResizePanelCenter != null) {
					workflow.remove(lineSegment.lineResizePanelCenter);
				}
				if (lineSegment.lineResizePanelEnd != null) {
					workflow.remove(lineSegment.lineResizePanelEnd);
				}
			}
			// 隐藏曼哈顿线段选中点
			workflow.remove(this.lineStartResizePanel);
			workflow.remove(this.lineEndResizePanel);
			if (textPanel != null && this.textPanel.getTextResizePanel() != null) {
				workflow.remove(this.textPanel.getTextResizePanel());
			}
		}
	}

	/**
	 * 
	 * 添加结束点
	 * 
	 * 当添加第一个点
	 * 
	 * 添加结束点
	 * 
	 * @param pPoint
	 *            小线段结束点
	 * @return // 根据路由算法生成小线段临时数据对象集合 List<JecnManhattanRouterResultData>
	 *         lineSegmentDataList
	 */
	public List<JecnManhattanRouterResultData> getRouterResultData(List<Point> listTempPoint) {
		if (listTempPoint == null || listTempPoint.size() == 0) {
			return null;
		}
		List<JecnManhattanRouterResultData> lineSegmentDataList = new ArrayList<JecnManhattanRouterResultData>();
		int tempSize = listTempPoint.size() - 1;
		for (int i = tempSize; i >= 0; i--) {
			if (tempOldPoint == null) {
				tempOldPoint = listTempPoint.get(i);
			} else {
				JecnManhattanRouterResultData resultData = new JecnManhattanRouterResultData();
				resultData.setStartPoint(tempOldPoint);
				resultData.setEndPint(listTempPoint.get(i));
				tempOldPoint = listTempPoint.get(i);
				if (i == 0 && this.getFlowElementData().getOriginalEndFigurePoint().equals(listTempPoint.get(i))) {
					// 结束线段
					resultData.setEndLine(true);
				}
				lineSegmentDataList.add(resultData);
			}
		}
		tempOldPoint = null;
		return lineSegmentDataList;
	}

	/**
	 * 检测线段是否为错误线段情况 相邻的两条线段开始点和结束点横坐标相同或纵坐标相同
	 * 
	 * @param manhattanLine
	 *            曼哈顿连接线
	 * @return true:错误线段
	 */
	public boolean checkLine() {
		if (this.getLineSegments().size() == 0) {
			return false;
		}
		LineSegment theLineSegment = this.getLineSegments().get(this.getLineSegments().size() - 1);
		int stx = theLineSegment.getStartPoint().x;
		int sty = theLineSegment.getStartPoint().y;
		int endx = theLineSegment.getEndPoint().x;
		int endy = theLineSegment.getEndPoint().y;
		for (int i = 1; i < this.getLineSegments().size(); i++) {
			LineSegment theLineSegment0 = this.getLineSegments().get(i - 1);
			LineSegment theLineSegment1 = this.getLineSegments().get(i);
			if ((theLineSegment0.getStartPoint().x == theLineSegment0.getEndPoint().x && theLineSegment0.getEndPoint().x == stx)
					&& (theLineSegment1.getStartPoint().x == theLineSegment1.getEndPoint().x && theLineSegment1
							.getEndPoint().x == stx)
					&& (theLineSegment0.getStartPoint().x == theLineSegment1.getStartPoint().x)
					&& (theLineSegment0.getStartPoint().x == theLineSegment1.getEndPoint().x) && (stx == endx)) {
				return true;
			} else if ((theLineSegment0.getStartPoint().y == theLineSegment0.getEndPoint().y && theLineSegment0
					.getEndPoint().y == sty)
					&& (theLineSegment1.getStartPoint().y == theLineSegment1.getEndPoint().y && theLineSegment1
							.getEndPoint().y == sty)
					&& (theLineSegment0.getStartPoint().y == theLineSegment1.getStartPoint().y)
					&& (theLineSegment0.getStartPoint().y == theLineSegment1.getEndPoint().y) && (sty == endy)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 小线段数据集排序
	 * 
	 * @param lineSegmentDataList
	 */
	public List<JecnManhattanRouterResultData> reOrderLinePoint(List<JecnManhattanRouterResultData> lineSegmentDataList) {
		if (lineSegmentDataList == null) {
			return null;
		}
		List<JecnManhattanRouterResultData> list = new ArrayList<JecnManhattanRouterResultData>();
		// 开始点位置方向
		Point mStartP = this.getOriginalStartPoint();
		Point newStartP = null;
		for (int i = 0; i < lineSegmentDataList.size(); i++) {
			JecnManhattanRouterResultData data = lineSegmentDataList.get(i);
			// 开始点
			Point startP = data.getStartPoint();
			// 结束点
			Point endP = data.getEndPint();
			if (startP.equals(mStartP)) {
				// 线段开始点和线的开始位置点相等
				list.add(data);
				newStartP = data.getEndPint();
				lineSegmentDataList.remove(i);
				break;
			}
			if (endP.equals(mStartP)) {
				// 线段结束点和线的开始位置点相等
				data.setStartPoint(mStartP);
				data.setEndPint(startP);
				list.add(data);
				newStartP = data.getEndPint();
				lineSegmentDataList.remove(i);
				break;
			}
		}
		if (newStartP != null) {
			reformSegmentsChild(list, newStartP, lineSegmentDataList);
		}
		if (list.size() > 0 && this.isManLine()) {// 带箭头连接线
			// 获取线段最后一条线段数据设置小线段箭头属性
			JecnManhattanRouterResultData data = list.get(list.size() - 1);
			// 改线段有箭头
			data.setEndLine(true);
		}
		return list;
	}

	/**
	 * 
	 * 连接线文字显示框放大缩小使用
	 * 
	 */
	private void zoomTextPanel() {
		if (this.textPanel != null) {// 连接线编辑框存在
			textPanel.zoomTextPanel();
		}
	}

	/**
	 * 递归算法，根据开始点和线的集合递归，当前开始点为下个线段的结束点
	 * 
	 * @param list
	 * @param pStartP
	 *            输入开始点
	 * @param lineSegmentDataList
	 */
	private void reformSegmentsChild(List<JecnManhattanRouterResultData> list, Point pStartP,
			List<JecnManhattanRouterResultData> lineSegmentDataList) {
		Point newStartP = null;
		for (int i = 0; i < lineSegmentDataList.size(); i++) {
			JecnManhattanRouterResultData data = lineSegmentDataList.get(i);
			Point startP = data.getStartPoint();
			Point endP = data.getEndPint();
			if (startP.equals(pStartP)) {
				list.add(data);
				newStartP = data.getEndPint();
				lineSegmentDataList.remove(i);
				break;
			}
			if (endP.equals(pStartP)) {
				data.setStartPoint(pStartP);
				data.setEndPint(startP);
				// 开始点和结束点互换更换
				// reSetData(data);
				lineSegmentDataList.remove(i);
				newStartP = data.getEndPint();
				list.add(data);
				break;
			}
		}
		if (newStartP != null) {
			reformSegmentsChild(list, newStartP, lineSegmentDataList);
		}
	}

	/**
	 * 获取小线段集合的备份数据
	 * 
	 * @param lineSegmentsClone
	 * @return
	 */
	public List<LineSegment> getLineSegmentsClone(JecnBaseManhattanLinePanel linePanel) {
		List<LineSegment> lineSegmentsClone = new ArrayList<LineSegment>();
		for (LineSegment lineSegment : this.getLineSegments()) {
			LineSegment tmpLine = lineSegment.clone();
			// 克隆小线段记录大线
			tmpLine.baseManhattanLine = linePanel;
			// 添加小线段到克隆数据集合
			lineSegmentsClone.add(tmpLine);
		}
		return lineSegmentsClone;
	}

	public boolean isDrraged() {
		return isDrraged;
	}

	public void setDrraged(boolean isDrraged) {
		this.isDrraged = isDrraged;
	}

	public JecnLineTextPanel getTextPanel() {
		return textPanel;
	}

	public void setTextPanel(JecnLineTextPanel textPanel) {
		this.textPanel = textPanel;
	}

	/**
	 * 连接线鼠标双击事件
	 * 
	 */
	public void doubleClick() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		if (textPanel == null) {// 连接线文本编辑框不存在，创建对象
			textPanel = new JecnLineTextPanel(this);
		}
		workflow.add(textPanel);

		if (DrawCommon.isNullOrEmtryTrim(this.getFlowElementData().getFigureText()) || textPanel.getWidth() < 100) {
			textPanel.setSize(100, 60);
		}
		// 编辑图形时显示的滚动面板
		workflow.getAidCompCollection().addTextAreaRefFlowElemPanel(this);

		// 设置连接线编辑区域位置、大小
		textPanel.insertTextToLineTextPanel();

		// 设置编辑框的大小
		workflow.getAidCompCollection().getEditTextArea().setSize(textPanel.getWidth(), textPanel.getHeight());
		// 设置滚动面板大小
		workflow.getAidCompCollection().getEditScrollPane().setSize(textPanel.getWidth(), textPanel.getHeight());
		// 设置滚动面板位置点
		workflow.getAidCompCollection().getEditScrollPane().setLocation(textPanel.getLocation());

		// 获取鼠标焦点
		workflow.getAidCompCollection().getEditTextArea().requestFocusInWindow();
		// 隐藏选中点
		this.hideManhattanLineResizeHandles();
		// 选中集合清空连接线
		workflow.getCurrentSelectElement().remove(this);
	}

	/**
	 * 
	 * 移除编辑输入框时，如果内容为空时内容框值NULL
	 * 
	 */
	public void removeLineByEditTextArea() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}

		if (DrawCommon.isNullOrEmtryTrim(this.getFlowElementData().getFigureText())) {
			// 不存在文本删除连接线编辑框
			removeTextPanel();
			return;
		}
		this.getTextPanel().setVisible(true);
		// // 存在获取编辑区域面板的层级
		// int layer = this.getFlowElementData().getZOrderIndex();
		// // 设置连接线编辑框层级
		// workflow.setLayer(this.getTextPanel(), layer + 1);
		// 根据编辑框内应显示的文字区域大小重置编辑框的位置和大小
		this.getTextPanel().insertTextToLineTextPanel();
	}

	/**
	 * 撤销恢复添加文本显示框
	 * 
	 */
	public void addTempPanel() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || this.getTextPanel() == null) {
			return;
		}
		workflow.add(this.getTextPanel());
		// 移除编辑输入框时，如果内容为空时内容框值NULL
		removeLineByEditTextArea();
	}

	/**
	 * 删除连接线编辑框
	 * 
	 */
	public void removeTextPanel() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || this.getTextPanel() == null) {
			return;
		}
		if (DrawCommon.isNullOrEmtryTrim(this.getFlowElementData().getFigureText())) {
			// 不存在文本隐藏编辑框
			this.getTextPanel().setVisible(false);
			this.getFlowElementData().setFigureText(null);
			workflow.remove(this.getTextPanel());
			// this.lineTextJPanel = null;
		}
	}

	/**
	 * 添加数据到线段显示
	 */
	public void insertTextToLine() {
		if (DrawCommon.isNullOrEmtryTrim(this.getFlowElementData().getFigureText())) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		if (textPanel == null) {
			textPanel = new JecnLineTextPanel(this);
			workflow.add(textPanel);
		}
		// 添加数据到线段显示
		textPanel.insertTextToLineTextPanel();
	}

	/**
	 * 获取克隆数据对象
	 * 
	 */
	public JecnBaseManhattanLinePanel clone() {
		JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) this.currClone();
		// 把源图形属性赋值给目标图形属性
		setFigureAttributes(this, linePanel);
		return linePanel;
	}

	/**
	 * 
	 * 把源图形属性赋值给目标图形属性
	 * 
	 * @param srcLinePanel
	 *            源属性
	 * @param destlinePanel
	 *            目标属性
	 */
	public void setFigureAttributes(JecnBaseManhattanLinePanel srcLinePanel, JecnBaseManhattanLinePanel destlinePanel) {
		if (srcLinePanel == null || destlinePanel == null) {
			return;
		}
		// 是否拖动
		destlinePanel.setDrraged(srcLinePanel.isDrraged());
		// 记录曼哈顿线小线段的集合
		destlinePanel.getLineSegments().addAll(srcLinePanel.getLineSegmentsClone(destlinePanel));

		destlinePanel.setStartFigure(srcLinePanel.getStartFigure());
		destlinePanel.setEndFigure(srcLinePanel.getEndFigure());
		// 如果存在为剪切记录的临时数据集合
		for (LineSegment lineSegment : srcLinePanel.curLineSegmentsClone) {
			LineSegment tmpLine = lineSegment.clone();
			// 克隆小线段记录大线
			tmpLine.baseManhattanLine = this;
			// 添加小线段到克隆数据集合
			destlinePanel.curLineSegmentsClone.add(tmpLine);
		}
		// 记录连接线文本框对象克隆值
		if (srcLinePanel.getTextPanel() == null || srcLinePanel.getFlowElementData().getFigureText() == null
				|| "".equals(srcLinePanel.getFlowElementData().getFigureText().trim())) {
			return;
		}
		destlinePanel.setTextPanel(srcLinePanel.getTextPanel().clone(destlinePanel));
	}

	/**
	 * 设置连接线开始图形和结束图形
	 * 
	 * @param startEditPort
	 * @param endEditPort
	 */
	public void reSetEditFigure(JecnEditPortPanel startEditPort, JecnEditPortPanel endEditPort) {
		if (startEditPort == null || endEditPort == null) {
			return;
		}
		// 设置连接线开始图形和结束图形
		this.setStartFigure(startEditPort.getBaseFigurePanel());
		this.setEndFigure(endEditPort.getBaseFigurePanel());
		// 设置连接线对应图形端口的位置
		this.getFlowElementData().setStartEditPointType(startEditPort.getEditPointType());
		this.getFlowElementData().setEndEditPointType(endEditPort.getEditPointType());
	}

	/**
	 * 
	 * 
	 * 获取当前给定的连接线顶端点对应的图形的编辑点类型
	 * 
	 * @param resizePanel
	 *            JecnManhattanLineResizePanel 当前给定连接线端点
	 * @return EditPointType
	 */
	public EditPointType getCurrEditPointType(JecnManhattanLineResizePanel resizePanel) {
		if (resizePanel == null) {
			return null;
		}

		EditPointType editPointType = null;
		switch (resizePanel.getReSizeLineType()) {
		case start:
			editPointType = this.getStartEditPointType();
			break;
		case end:
			editPointType = this.getEndEditPointType();
			break;
		default:
			break;
		}
		return editPointType;
	}

	/**
	 * 
	 * 
	 * 获取当前给定的连接线顶端点对应的图形
	 * 
	 * @param resizePanel
	 *            JecnManhattanLineResizePanel 当前给定连接线端点
	 * @return JecnBaseFigurePanel
	 */
	public JecnBaseFigurePanel getCurrEditPointFigure(JecnManhattanLineResizePanel resizePanel) {
		if (resizePanel == null) {
			return null;
		}
		JecnBaseFigurePanel figure = null;
		switch (resizePanel.getReSizeLineType()) {
		case start:
			figure = this.getStartFigure();
			break;
		case end:
			figure = this.getEndFigure();
			break;
		default:
			break;
		}
		return figure;
	}

	public boolean isShowResizeHandle() {
		return isShowResizeHandle;
	}

	public JecnManhattanLineResizePanel getLineStartResizePanel() {
		return lineStartResizePanel;
	}

	public JecnManhattanLineResizePanel getLineEndResizePanel() {
		return lineEndResizePanel;
	}

	public List<LineSegment> getCurLineSegmentsClone() {
		return curLineSegmentsClone;
	}

	public Point getArrowStartPoint() {
		return arrowStartPoint;
	}

	public void setArrowStartPoint(Point arrowStartPoint) {
		this.arrowStartPoint = arrowStartPoint;
	}

	public Point getArrowEndPoint() {
		return arrowEndPoint;
	}

	public void setArrowEndPoint(Point arrowEndPoint) {
		this.arrowEndPoint = arrowEndPoint;
	}
}
