package com.jecn.epros.server.action;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.designer.update.impl.JecnUpdateFileActionImpl;
import com.jecn.epros.server.util.JecnKey;

/**
 * @author yxw 2012-8-3
 * @description：tomcat 启动前
 */
public class StartBeforeListener implements ServletContextListener {

	private final static Log log = LogFactory.getLog(StartBeforeListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	/**
	 * 验证密钥
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			if (!JecnKey.checkKey()) {
				System.exit(0);
			}
		} catch (Exception e2) {
			System.exit(0);
		}
		Properties props = new Properties();
		// 获取URL
		String url = JecnUpdateFileActionImpl.class.getClassLoader()
				.getResource("database.properties").toString().substring(6);
		// 转换空格
		String empUrl = url.replace("%20", " ");
		log.info("database.properties:" + url);
		FileInputStream in = null;
		try {
			in = new FileInputStream(empUrl);
			props.load(in);
			System.setProperty("java.rmi.server.hostname", props
					.getProperty("rmi.ip"));
		} catch (IOException e) {
			log.error("database.properties", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("关闭流错误！", e1);
				}
			}
		}
	}
}
