package epros.designer.gui.process;

import javax.swing.JTree;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.process.JecnSaveProcessData;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class JecnProcessCommon {
	/**
	 * @author yxw 2012-8-9
	 * @description:创建地图
	 * @param name
	 * @param pNode
	 * @param flowNumber
	 * @param jecnMainFlow
	 * @param posIds
	 * @param orgIds
	 * @param jTree
	 * @throws Exception
	 */
	public static JecnFlowStructureT createProcessMap(String name, Long isPublic, JecnTreeNode pNode,
			String flowNumber, JecnMainFlowT jecnMainFlow, String posIds, String orgIds, JTree jTree, String posGroupIds)
			throws Exception {
		// 流程主表临时表
		JecnFlowStructureT jecnFlowStructureT = getMapFlowStructureT(name, isPublic, pNode, flowNumber);
		jecnFlowStructureT = ConnectionPool.getProcessAction().addFlowMap(jecnFlowStructureT, jecnMainFlow, posIds,
				orgIds, posGroupIds, JecnConstants.getUserId());
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		jecnTreeBean.setId(jecnFlowStructureT.getFlowId());
		jecnTreeBean.setName(name);
		jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
		jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
		jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
		jecnTreeBean.setNumberId(flowNumber);
		JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
		return jecnFlowStructureT;
	}

	private static JecnFlowStructureT getMapFlowStructureT(String name, Long isPublic, JecnTreeNode pNode,
			String flowNumber) {
		JecnFlowStructureT jecnFlowStructureT = new JecnFlowStructureT();
		jecnFlowStructureT.setProjectId(JecnConstants.projectId);
		jecnFlowStructureT.setFlowName(name);
		jecnFlowStructureT.setIsFlow(0L);
		jecnFlowStructureT.setPerFlowId(pNode.getJecnTreeBean().getId());
		jecnFlowStructureT.setFlowIdInput(flowNumber);
		jecnFlowStructureT.setIsPublic(isPublic);
		jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode)));
		jecnFlowStructureT.setDelState(0L);
		jecnFlowStructureT.setPeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnFlowStructureT.setUpdatePeopleId(JecnConstants.getUserId());
		jecnFlowStructureT.setOccupier(JecnConstants.getUserId());
		return jecnFlowStructureT;
	}

	public static JecnFlowStructureT createAerialProcessMap(String name, Long isPublic, JecnTreeNode pNode,
			String flowNumber, JecnMainFlowT jecnMainFlow, String posIds, String orgIds, JTree jTree, String posGroupIds)
			throws Exception {
		JecnFlowStructureT jecnFlowStructureT = getMapFlowStructureT(name, isPublic, pNode, flowNumber);
		jecnFlowStructureT.setNodeType(2);
		jecnFlowStructureT.setIsPublic(1L);
		jecnFlowStructureT = ConnectionPool.getProcessAction().addFlowMap(jecnFlowStructureT, jecnMainFlow, posIds,
				orgIds, posGroupIds, JecnConstants.getUserId());
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		jecnTreeBean.setId(jecnFlowStructureT.getFlowId());
		jecnTreeBean.setName(name);
		jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
		jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
		jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
		jecnTreeBean.setNumberId(flowNumber);
		jecnTreeBean.setNodeType(jecnFlowStructureT.getNodeType());
		JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
		return jecnFlowStructureT;
	}

	public static JecnFlowStructureT createProcess(String name, String num, Long isPublic, String orgIds,
			String posIds, String sptIds, JecnTreeNode pNode, JecnFlowBasicInfoT jecnFlowBasicInfoT, JTree jTree,
			String posGroupIds) throws Exception {
		JecnSaveProcessData processData = new JecnSaveProcessData();
		// 流程主表
		JecnFlowStructureT jecnFlowStructureT = getNewFlowStructureT(name, num, pNode);
		jecnFlowStructureT.setIsPublic(isPublic);
		// 默认为永久
		setFlowExpiry(jecnFlowBasicInfoT);

		processData.setJecnFlowStructureT(jecnFlowStructureT);
		processData.setJecnFlowBasicInfoT(jecnFlowBasicInfoT);
		processData.setPosIds(posIds);
		processData.setPosGroupIds(posGroupIds);
		processData.setOrgIds(orgIds);
		processData.setSptIds(sptIds);
		processData.setUpdatePersionId(JecnConstants.getUserId());
		jecnFlowStructureT = ConnectionPool.getProcessAction().addFlow(processData);
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		jecnTreeBean.setId(jecnFlowStructureT.getFlowId());
		jecnTreeBean.setName(name);
		jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
		jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
		jecnTreeBean.setTreeNodeType(TreeNodeType.process);
		jecnTreeBean.setNumberId(jecnFlowStructureT.getFlowIdInput());
		JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
		return jecnFlowStructureT;
	}

	public static JecnFlowStructureT getNewFlowStructureT(String name, String num, JecnTreeNode pNode) {
		JecnFlowStructureT jecnFlowStructureT = new JecnFlowStructureT();
		jecnFlowStructureT.setProjectId(JecnConstants.projectId);
		jecnFlowStructureT.setFlowName(name);
		jecnFlowStructureT.setIsFlow(1L);
		jecnFlowStructureT.setPerFlowId(pNode.getJecnTreeBean().getId());
		jecnFlowStructureT.setFlowIdInput(num);
		jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode)));
		jecnFlowStructureT.setDelState(0L);
		jecnFlowStructureT.setPeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnFlowStructureT.setUpdatePeopleId(JecnConstants.getUserId());
		jecnFlowStructureT.setOccupier(JecnConstants.getUserId());
		return jecnFlowStructureT;
	}

	public static void setFlowExpiry(JecnFlowBasicInfoT jecnFlowBasicInfoT) {
		// 获取流程配置-->流程属性中 有效期配置数据
		JecnConfigItemBean configItemBean = ConnectionPool.getConfigAciton().selectConfigItemBean(
				JecnConfigContents.TYPE_BIG_ITEM_PARTMAP, "processValidDefaultValue");
		// 默认为永久
		jecnFlowBasicInfoT.setExpiry(0L);
		if (configItemBean.getValue() != null && !"".equals(configItemBean.getValue())) {
			jecnFlowBasicInfoT.setExpiry(Long.valueOf(configItemBean.getValue()));
		}
	}

	/**
	 * 获取待创建的流程节点
	 * 
	 * @param name
	 * @param num
	 * @param pNode
	 * @param jecnFlowBasicInfoT
	 * @return
	 */
	public static JecnFlowStructureT createProcessFile(String name, String num, JecnTreeNode pNode,
			JecnFlowBasicInfoT jecnFlowBasicInfoT) {
		// 流程主表
		JecnFlowStructureT jecnFlowStructureT = getNewFlowStructureT(name, num, pNode);
		// 默认为永久
		setFlowExpiry(jecnFlowBasicInfoT);
		return jecnFlowStructureT;
	}

	public static JecnFlowStructureT refreshTreeProcessFileNode(String name, String num, JecnTreeNode pNode,
			JecnFlowStructureT jecnFlowStructureT, JTree jTree) throws Exception {
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		jecnTreeBean.setId(jecnFlowStructureT.getFlowId());
		jecnTreeBean.setName(name);
		jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
		jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
		jecnTreeBean.setTreeNodeType(TreeNodeType.processFile);
		jecnTreeBean.setNumberId(num);
		JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
		return jecnFlowStructureT;
	}
}
