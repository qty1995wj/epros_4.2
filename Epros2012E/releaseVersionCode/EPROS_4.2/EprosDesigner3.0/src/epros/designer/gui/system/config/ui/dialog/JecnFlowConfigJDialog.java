package epros.designer.gui.system.config.ui.dialog;

import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.util.JecnProperties;

/**
 * 
 * 流程图配置对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowConfigJDialog extends JecnAbtractBaseConfigDialog {
	public JecnFlowConfigJDialog(JecnConfigDesgBean configBean) {
		super(configBean);
		this.setTitle(JecnProperties.getValue("setTitlePartMapName"));
	}
}
