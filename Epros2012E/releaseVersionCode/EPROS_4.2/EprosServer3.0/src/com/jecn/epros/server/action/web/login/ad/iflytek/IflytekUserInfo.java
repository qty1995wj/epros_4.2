package com.jecn.epros.server.action.web.login.ad.iflytek;

import java.io.Serializable;

public class IflytekUserInfo implements Serializable {
	private String accontNames;
	private String emplName;

	public String getAccontNames() {
		return accontNames;
	}

	public void setAccontNames(String accontNames) {
		this.accontNames = accontNames;
	}

	public String getEmplName() {
		return emplName;
	}

	public void setEmplName(String emplName) {
		this.emplName = emplName;
	}
}
