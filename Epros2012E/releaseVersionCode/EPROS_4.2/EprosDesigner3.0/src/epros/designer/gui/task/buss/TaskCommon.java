package epros.designer.gui.task.buss;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.popedom.person.PersonChooseDialog;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUserCheckUtil;

public class TaskCommon {
	/** 日志 */
	private static final Logger log = Logger.getLogger(TaskCommon.class);

	/**
	 * 获得真实姓名通过用户ID
	 * 
	 * @author 张臣
	 * @param listUser
	 * @param userId
	 * @return
	 */
	public static String getTrueName(List<Object[]> listUser, String userId) {
		if (userId != null && !"".equals(userId)) {
			for (Object[] obj : listUser) {
				if (obj[1] != null && userId.equals(obj[1].toString())) {
					if (obj[0] != null) {
						return obj[0].toString();

					}
				}
			}
		}
		return "";
	}

	/**
	 * 读取文件获取文件字节流
	 * 
	 * @param f
	 *            File待读取文件
	 * @return byte[]
	 */
	public static byte[] changeFileToByteByFile(File f) {
		if (f == null) {
			log.error("TaskCommon changeFileToByteByFile is error！");
			return null;
		}
		try {
			FileInputStream fis = new FileInputStream(f);
			byte[] content = new byte[fis.available()];
			fis.read(content);
			fis.close();
			return content;
		} catch (IOException e) {
			log.error("TaskCommon changeFileToByteByFile is error！", e);
			return null;
		}
	}

	/**
	 * 解析id集合和name 集合
	 * 
	 * @param strIds
	 *            ID集合','分隔
	 * @param strNames
	 *            名称集合'/'分隔
	 * @return List<JecnTreeBean>
	 */
	public static List<JecnTreeBean> getTreeBeanList(String strIds, String strNames) {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		if (DrawCommon.isNullOrEmtryTrim(strIds) || DrawCommon.isNullOrEmtryTrim(strNames)) {
			return list;
		}
		String[] listIds = strIds.split(",");
		String[] listNames = strNames.split("/");
		for (int i = 0; i < listIds.length; i++) {
			JecnTreeBean treeBean = new JecnTreeBean();
			treeBean.setId(Long.valueOf(listIds[i].trim()));
			treeBean.setName(listNames[i]);
			list.add(treeBean);
		}
		return list;
	}

	/**
	 * typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置
	 * 
	 * @return 不在0到5的范围内不进行查询
	 */
	public static int typeBigModel(JecnTreeBean treeBean) {
		if (treeBean == null) {
			return 6;
		}
		switch (treeBean.getTreeNodeType()) {
		case processMap:// 流程地图
			return 0;
		case process:// 流程图
		case processFile:
			return 1;
		case ruleModeFile:// 制度模板文件
		case ruleFile:// 制度模板文件
			return 2;
		case file:// 文件
		case fileDir:
			return 3;
		default:
			break;
		}
		return 6;
	}

	/**
	 * 人员选择
	 * 
	 * @param listPerson
	 * @param strIds
	 *            人员ID集合
	 * @param filedName
	 *            单选文本
	 * @param areaName
	 *            多选文本域
	 * @param isMuil
	 *            是否多选
	 * @param taskJDialog
	 * @return
	 */
	public static String selectPeopleAudit(List<JecnTreeBean> listPerson, String strIds, JTextField filedName,
			JTextArea areaName, boolean isMuil, JecnDialog taskJDialog) {
		if (filedName == null) {
			listPerson = TaskCommon.getTreeBeanList(strIds, areaName.getText());
		} else {
			listPerson = TaskCommon.getTreeBeanList(strIds, filedName.getText());
		}

		PersonChooseDialog personChooseDialog = new PersonChooseDialog(listPerson, taskJDialog);
		// 是否支持多选 true 多选
		personChooseDialog.setSelectMutil(isMuil);
		personChooseDialog.setVisible(true);

		if (personChooseDialog.isOperation()) {// 点击确定
			if (listPerson != null && listPerson.size() > 0) {

				String ids = "";
				String names = "";
				for (JecnTreeBean treeBean : listPerson) {
					ids = ids + treeBean.getId().toString() + ",";
					names = names + treeBean.getName() + "/";
				}
				if (!"".equals(ids)) {
					ids = JecnTool.dropChars(ids, 1);
				}
				if (!"".equals(names)) {
					names = JecnTool.dropChars(names, 1);
				}
				strIds = ids;
				if (filedName == null) {
					areaName.setText(names);
				} else {
					filedName.setText(names);
				}

			} else {
				if (filedName == null) {
					areaName.setText("");
				} else {
					filedName.setText("");
				}
				strIds = "";
			}
		}
		return strIds;
	}

	/**
	 * 从本地上传文件，并把上传文件信息显示的JTable
	 * 
	 * @param jtable
	 * @param jDialog
	 * @param isTaskFile
	 * @param pNode
	 * @throws DaoException
	 */
	public static void uploadFiles(JTable jtable, JDialog jDialog) {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileManageDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
		fileChoose.setEidtTextFiled(false);

		// 可以同时上传多个文件
		fileChoose.setMultiSelectionEnabled(true);
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
		int i = fileChoose.showSaveDialog(jDialog);
		if (i == JFileChooser.APPROVE_OPTION) {
			// 选择的文件
			File[] files = fileChoose.getSelectedFiles();

			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSaveFileManageDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			Vector<Vector<Object>> vs = ((DefaultTableModel) jtable.getModel()).getDataVector();
			F: for (File f : files) {
				for (Vector t : vs) {
					if (f.getPath().equals((String) t.get(2))) {
						continue F;
					}
				}
				Vector<Object> v = new Vector<Object>();
				v.add("");
				v.add(JecnUtil.getFileName(f.getPath()));
				v.add(f.getPath());
				((DefaultTableModel) jtable.getModel()).addRow(v);
			}
		}
	}

	/**
	 * 
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getFileName(String filePath) {
		String inString = ".*\\\\(.*\\..*)";
		Pattern pattern = Pattern.compile(inString);
		Matcher matcher = pattern.matcher(filePath);
		boolean b = matcher.matches();
		if (b) {
			return matcher.group(1);
		}
		return filePath;
	}

	/**
	 * 检测文本是否符合规定要求，不为空、不超出规定范围
	 * 
	 * @param peopleNames
	 *            文本名称
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static boolean checTextName(JecnDialog taskJDialog, String peopleNames, JLabel jLabel) {
		String str = JecnUserCheckUtil.checkNullName(peopleNames);
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
			JecnOptionPane.showMessageDialog(taskJDialog, str);
			return true;
		}
		return false;
	}

	/**
	 * 检查数据是否为空
	 * 
	 * @param taskJDialog
	 * @param peopleNames
	 * @param jLabel
	 * @return
	 */
	public static boolean checTextNameIsNotNull(JecnDialog taskJDialog, String peopleNames, JLabel jLabel) {
		String str = "";
		if (DrawCommon.isNullOrEmtryTrim(peopleNames)) {
			str = jLabel.getText() + JecnUserCheckInfoData.getNameNotNull();
			JecnOptionPane.showMessageDialog(taskJDialog, str);
			return true;
		}
		return false;
	}

	/**
	 * 检测文本是否符合规定要求，不为空、不超出规定范围
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static String checTextName(JTextField textField, JLabel jLabel) {
		String str = JecnUserCheckUtil.checkNullName(textField.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
		}
		return str;
	}

	/**
	 * 检测文本是否符合规定要求，可以为空、不包含特殊字符、不超出规定范围
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static String checkNameContainsNull(JTextField textField, JLabel jLabel) {
		if (DrawCommon.isNullOrEmtryTrim(textField.getText())) {
			return "";
		}
		String str = JecnUserCheckUtil.checkNameContainsNull(textField.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
		}
		return str;
	}

	/**
	 * 检测文本域是否符合规定要求，可以为空、不包含特殊字符、不超出规定范围
	 * 
	 * @param textArea
	 *            文本域
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static String checkNameContainsNull(JTextArea textArea, JLabel jLabel) {
		if (DrawCommon.isNullOrEmtryTrim(textArea.getText())) {
			return "";
		}
		String str = JecnUserCheckUtil.checkNameContainsNull(textArea.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
		}
		return str;
	}

	/**
	 * 检测文本是否符合规定要求，不为空、不包含特殊字符、不超出规定范围
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static String checkAreaName(JTextArea textArea, JLabel jLabel) {
		String str = JecnUserCheckUtil.checkAreaName(textArea.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
		}
		return str;
	}

	/**
	 * 检测文本是否符合规定要求，可以为空、不包含特殊字符、不超出规定范围
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static boolean checkNameContainsNull(JecnDialog taskJDialog, JTextField textField, JLabel jLabel) {
		if (DrawCommon.isNullOrEmtryTrim(textField.getText())) {
			return false;
		}
		String str = JecnUserCheckUtil.checkNameContainsNull(textField.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
			JecnOptionPane.showMessageDialog(taskJDialog, str);
			return true;
		}
		return false;
	}

	/**
	 * 检测文本是否符合规定要求，不为空、不包含特殊字符、不超出规定范围
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static boolean checkAreaName(JecnDialog taskJDialog, JTextArea textArea, JLabel jLabel) {
		String str = JecnUserCheckUtil.checkAreaName(textArea.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
			JecnOptionPane.showMessageDialog(taskJDialog, str);
			return true;
		}
		return false;
	}

	/**
	 * 检测不能输入的文本是否符合规定要求，不为空、不超出规定范围
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static boolean checkAreaNameNotImput(JecnDialog taskJDialog, String peopleIds, JLabel jLabel) {
		String str = checkAreaNameNotImput(peopleIds);
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
			JecnOptionPane.showMessageDialog(taskJDialog, str);
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 校验名称是否满足要求,验证（名称不为空，字符正确，长度）成功返回“”，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（名称不为空，字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkAreaNameNotImput(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			return JecnUserCheckInfoData.getNameNotNull();
		} else if (JecnUserCheckUtil.getTextLength(name) > 1200) {
			// 不能超过1200个字符或600个汉字
			return JecnUserCheckInfoData.getNoteLengthInfo();
		}
		return "";
	}

	/**
	 * 验证文本域输入内容是否越界 字符最大1200
	 * 
	 * @param taskJDialog
	 * @param textArea
	 *            需验证的文本域
	 * @param jLabel
	 *            需验证文本域对应的标签
	 * @return true：存在错误信息
	 */
	public static boolean checkAreaLength(JecnDialog taskJDialog, JTextArea textArea, JLabel jLabel) {
		String str = JecnUserCheckUtil.checkNoteLength(textArea.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
			JecnOptionPane.showMessageDialog(taskJDialog, str);
			return true;
		}
		return false;
	}

	/**
	 * 同目录下此文件是否已存在 true是存在，false是不存在
	 * 
	 * @param node
	 *            树节点
	 * @param fileName
	 *            新的文件名称
	 * @param fileOldName
	 *            历史文件名称
	 * @return
	 */
	public static boolean isDirExistFileName(JecnTreeNode node, String fileName, String fileOldName) {
		for (int i = 0; i < node.getChildCount(); i++) {
			JecnTreeNode newTreeNodeTemp = (JecnTreeNode) node.getChildAt(i);
			String name = newTreeNodeTemp.getJecnTreeBean().getName();
			// 是否为文件 true：文件。
			boolean isFile = false;
			if ("file".equals(newTreeNodeTemp.getJecnTreeBean().getTreeNodeType().toString())) {
				isFile = true;
			}

			if (!"".equals(fileName) && isFile) {
				if (fileOldName == null) {
					if (fileName.equals(name)) {
						return true;
					}
				} else {
					if (!fileOldName.equals(name) && fileName.equals(name)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 把文件转换成byte数组
	 * 
	 * @param path
	 * @return
	 */
	public static byte[] changeFileToByte(String path) {
		try {
			FileInputStream fis = new FileInputStream(new File(path));
			byte[] content = new byte[fis.available()];
			fis.read(content);
			fis.close();
			return content;
		} catch (IOException e) {
			log.error("TaskCommon changeFileToByte is error", e);
		}
		return null;
	}

	/**
	 * 根据树几点返回对应任务默认文件URL
	 * 
	 * @param treeNode
	 *            选中的树节点
	 * @return String 任务默认文件URL
	 */
	public static String getPropertiesUrl(JecnTreeNode treeNode) {
		String url = "";
		if (treeNode == null) {
			return url;
		}
		TreeNodeType nodeType = treeNode.getJecnTreeBean().getTreeNodeType();
		switch (nodeType) {
		case process:// 流程任务
		case processFile:
			url = "resources/fileProperties/flowDefaultPeople.properties";
			break;
		case processMap:// 流程地图任务
			url = "resources/fileProperties/flowMapDefaultPeople.properties";
			break;
		case ruleFile:// 制度文件任务
			url = "resources/fileProperties/ruleFileDefaultPeople.properties";
			break;
		case ruleModeFile:// 制度模板文件任务
			url = "resources/fileProperties/ruleFileDefaultPeople.properties";
			break;
		case file:// 更新文件任务
		case fileDir:// 上传文件任务
			url = "resources/fileProperties/FiledefaultPeople.properties";
			break;
		default:
			break;
		}
		return url;
	}

	/**
	 * 根据节点获取查阅权限对应的type
	 * 
	 * @param treeNode
	 *            树节点
	 * @return type:0是流程;1是文件2;是标准;3制度
	 */
	public static int getViewType(JecnTreeNode treeNode) {
		if (treeNode == null) {
			return 5;
		}
		int type = 0;
		switch (treeNode.getJecnTreeBean().getTreeNodeType()) {
		case process:// 流程
		case processMap:// 流程地图
			type = 0;
			break;
		case ruleFile:// 制度
		case ruleModeFile:// 制度模板文件
			type = 3;
			break;
		case file:// 文件
		case fileDir:// 文件目录
			type = 1;
			break;
		}
		return type;
	}

	/**
	 * typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @param treeNode
	 *            当前树节点
	 * @return 0：流程地图；1：流程图；2：制度；3：文件；-1:错误
	 */
	public static int getCinfigItemType(JecnTreeNode treeNode) {
		if (treeNode == null) {
			return -1;
		}
		// typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
		int type = 0;
		switch (treeNode.getJecnTreeBean().getTreeNodeType()) {
		case processMap:// 流程地图
			type = 0;
			break;
		case process:// 流程
			type = 1;
			break;
		case ruleFile:// 制度
		case ruleModeFile:// 制度模板文件
			type = 2;
		case file:// 文件
		case fileDir:// 文件目录
			type = 3;
			break;
		default:
			break;
		}
		return type;
	}
}
