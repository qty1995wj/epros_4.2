package epros.draw.gui.figure.unit;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.data.JecnElement;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.gui.box.JecnNewUpdateTemplateDialog;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.JecnFlowExport;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnXmlUtil;

/**
 * 片段的导出,导入,修改,删除类
 * 
 * @author fuzhh
 */
public class JecnFlowTemplate {

	/** 返回：文件路径 + 系统毫秒数＋ jpg */
	private static String TEMP_COPY = "/epros/draw/configFile/model/images/";
	/** 文件扩展名 */
	private static String COPY_FILE_TYPE = ".jpg";

	/** 流程图的XML路径 */
	private static String PART_MAP_COPY_PRINT = "model/partMap.xml";
	/** 流程地图的XML路径 */
	private static String TOTAL_MAP_COPY_PRINT = "model/totalMap.xml";
	/** 组织图的XML路径 */
	private static String ORG_MAP_COPY_PRINT = "model/orgMap.xml";

	/** 系统使用的配置文件存放路径前缀 */
	private final static String PROPERTY_FILE_PATH_PREFIX = "/epros/draw/configFile/";

	/** 存放模型片段对应的名称导入时存储 */
	private static Map<String, String> templateNameMap = null;

	/** 模板修改时设计器中存储的数据 */
	private static Map<String, String> updateTemplateMap = null;

	/** 需要保存到设计器的数据 */
	private static JecnTemplateData saveTemplateData = null;

	/** 需要删除的设计器数据 */
	private static Map<String, String> deleteTemplateMap = null;

	private static final Log log = LogFactory.getLog(JecnFlowTemplate.class);

	/**
	 * 导出模板文件
	 * 
	 * @param name
	 *            模板名称
	 * @param mapType
	 *            面板类型
	 * @param templateType
	 *            模板标识 title 为标题
	 * @return
	 */
	public static JecnTemplateData exportPartData(String name, MapType mapType) {
		// 判断是否存储到xml中
		if (JecnUserCheckInfoData.isTemplateType()) {
			// 调用存储方法
			JecnTemplateData templateData = exportFigureLine(getWorkflow().getCurrentSelectElement(), name, mapType);
			return templateData;
		}
		return null;
	}

	/**
	 * 
	 * 返回临时文件名称：UIID ＋ jpg
	 * 
	 * @return
	 */
	private static String getPathCopy() {
		return DrawCommon.getUUID() + COPY_FILE_TYPE;
	}

	/**
	 * 获取模板xml的名称和ID
	 * 
	 * @param mapType
	 *            模型类型
	 * @return 读取出来的数据
	 */
	public static List<JecnTemplateData> getMapTemplate(MapType mapType) {
		String xmlAbPath = getXmlPath(mapType);
		if (DrawCommon.isNullOrEmtryTrim(xmlAbPath)) {
			return null;
		}
		// 流程地图
		return readXmlreturnIdAndName(xmlAbPath);
	}

	/**
	 * 返回文件路径
	 * 
	 * @param type
	 *            是否为标题
	 * @return 路径
	 */
	public static String getXmlPath(MapType mapType) {
		// 文件路径
		String xmlPath = null;

		switch (mapType) {
		case totalMap:// 流程地图
		case totalMapRelation:
			xmlPath = PROPERTY_FILE_PATH_PREFIX + TOTAL_MAP_COPY_PRINT;
			break;
		case partMap:// 流程图
			xmlPath = PROPERTY_FILE_PATH_PREFIX + PART_MAP_COPY_PRINT;
			break;
		case orgMap:// 组织图
			xmlPath = PROPERTY_FILE_PATH_PREFIX + ORG_MAP_COPY_PRINT;
			break;
		// case totalMapRelation:// 组织图
		// xmlPath = PROPERTY_FILE_PATH_PREFIX + TOTAL_MAP_COPY_PRINT;
		// break;
		default:
			return null;
		}

		return JecnFileUtil.getSyncPath(xmlPath);
	}

	/**
	 * 根据传入的路径返回ID和名称的Map集合
	 * 
	 * @param xmlPath
	 *            xml的路径
	 * @param type
	 *            是标题还是模板
	 * @return
	 */
	private static List<JecnTemplateData> readXmlreturnIdAndName(String xmlPath) {
		// 存储片段数据的List
		List<JecnTemplateData> templateDataList = new ArrayList<JecnTemplateData>();
		// 获取路径下的xml
		Document doc = JecnXmlUtil.getDocumentByAllPath(xmlPath);
		if (doc != null) {
			// 获取xml下 所有的partData节点数据
			NodeList partData = doc.getElementsByTagName("partData");
			for (int i = 0; i < partData.getLength(); i++) {
				// 片段的数据bean
				JecnTemplateData templateData = new JecnTemplateData();
				Element partElement = (Element) partData.item(i);
				// 获取ID
				String ID = partElement.getAttribute("ID");
				templateData.setTemplateId(ID);
				// 获取图片路径
				String imagerPath = partElement.getAttribute("relevancyImager");
				templateData.setImagerPath(JecnFileUtil.getSyncPath(TEMP_COPY) + imagerPath);
				if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
					// 如果为设计器就去获取Map中的值
					if (templateNameMap != null && templateNameMap.size() > 0) {
						templateData.setTemplateName(templateNameMap.get(imagerPath));
					}
				} else {
					// 获取名称
					String templateName = partElement.getAttribute("partName");
					templateData.setTemplateName(templateName);
				}

				// 片段类型
				String templateMapType = partElement.getAttribute("templateMapType");
				templateData.setMapType(templateMapType);
				// 把片段数据添加的List中
				templateDataList.add(templateData);
			}
		}
		return templateDataList;
	}

	/**
	 * 添加图形线到面板
	 * 
	 * @param type
	 *            false 为标题
	 * @param ID
	 *            片段标识
	 * @param Point
	 *            面板点击位置
	 */
	public static void addTemplateToWorkflow(String ID, Point point) {
		JecnTemplateData template = null;
		if (getWorkflow() != null) {
			template = importTemplateXml(ID, getWorkflow().getFlowMapData().getMapType());
		}
		if (template == null) {
			return;
		}
		if (template == null) {
			log.info("读取ID为" + ID + "的节点错误！");
			return;
		} else {

			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			// 中心点X位移
			int moveX = (int) (point.getX() - (template.getTemplatePoint().getX() * desktopPane.getWorkflowScale()) + Integer
					.valueOf(template.getSelectWidth())
					* desktopPane.getWorkflowScale() / 2);

			// 中心点Y位移
			int moveY = (int) (point.getY() - template.getTemplatePoint().getY() * desktopPane.getWorkflowScale() + Integer
					.valueOf(template.getSelectHeight())
					* desktopPane.getWorkflowScale() / 2);
			// 记录所有克隆后的数据集合
			List<JecnBaseFlowElementPanel> allFlowElementClone = new ArrayList<JecnBaseFlowElementPanel>();

			// 大线对应小线的Map
			Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> manLineMapList = template
					.getManLineMapList();

			// 记录所有元素
			List<JecnBaseFlowElementPanel> allFlowElement = new ArrayList<JecnBaseFlowElementPanel>();

			// 获取需要添加的连接线
			addImportManLine(manLineMapList, allFlowElement, moveX, moveY);

			// 把所有图形添加到所有元素集合
			allFlowElement.addAll(template.getTemplateElementList());
			// 根据复制或剪切的集合获取需要粘贴的集合数据
			JecnFlowElementCopyAndPaste.getPasteList(allFlowElement, allFlowElementClone);

			// 重新排序层级
			sortAllFlowElementCloneList(allFlowElementClone);
			// 创建记录撤销恢复的流程元素数据的对象
			JecnUndoRedoData redoData = new JecnUndoRedoData();
			for (JecnBaseFlowElementPanel baseFlowElement : allFlowElementClone) {
				if (baseFlowElement instanceof JecnBaseFigurePanel) {
					// 获取图形的panel
					JecnBaseFigurePanel baseFigurePanel = (JecnBaseFigurePanel) baseFlowElement;
					if (baseFigurePanel instanceof CommentText) {// 判断是否为注释框
						CommentText commentText = (CommentText) baseFigurePanel;
						// 执行拖动位移算法
						commentText.isMove();
					} else if (baseFigurePanel instanceof IconFigure) { // 图标插入框
						// 判断是否为设计器
						if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
							IconFigure iconFigure = (IconFigure) baseFigurePanel;
							// 获取关联ID
							Long linkId = iconFigure.getFlowElementData().getDesignerFigureData().getLinkId();
							// 判断ID 是否为空
							if (linkId != null) {
								JecnDesignerProcess.getDesignerProcess().openFile(linkId, iconFigure);
							}
						}
					}
					// 放大缩小后的图形大小和位置点
					JecnFigureDataCloneable currFigureDataCloneable = baseFigurePanel.getFlowElementData()
							.getFigureDataCloneable().getZoomFigureDataProcess(
									JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
					// 获取图形的位置点
					int tempPointX = currFigureDataCloneable.getX();
					int tempPointY = currFigureDataCloneable.getY();
					Point tempPoint = new Point(tempPointX + moveX, tempPointY + moveY);
					// 重置图形的位置点
					baseFigurePanel.setSizeAndLocation(tempPoint);
					// 面板添加流程元素
					JecnAddFlowElementUnit.addFigure(baseFigurePanel);
					// 设置流程元素层级
					setFlowElementOrder(baseFigurePanel);

				} else if (baseFlowElement instanceof JecnBaseManhattanLinePanel) {
					JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) baseFlowElement;
					// 设置流程元素层级
					setFlowElementOrder(linePanel);
					// 面板添加流程元素
					JecnAddFlowElementUnit.addImportManLine(linePanel);
					// 添加数据到线段显示
					linePanel.insertTextToLine();

					desktopPane.getWorkflowZoomProcess().zoomManhattanLine(linePanel);
				} else if (baseFlowElement instanceof JecnBaseDividingLinePanel) {
					JecnBaseDividingLinePanel linePanel = (JecnBaseDividingLinePanel) baseFlowElement;
					// 设置流程元素层级
					setFlowElementOrder(linePanel);
					addVHDiviLine(linePanel, moveX, moveY);
					desktopPane.getWorkflowZoomProcess().zoomDiviLine(linePanel);
					JecnAddFlowElementUnit.importAddDiviLine(linePanel);
				}
				// 记录操作后数据
				redoData.recodeFlowElement(baseFlowElement);
			}
			// 记录这次操作的数据集合
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createAdd(redoData);
			getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
			getWorkflow().revalidate();
			getWorkflow().repaint();

			getWorkflow().getPanelList();
		}
	}

	/**
	 * 获取片段对应的所有图形
	 * 
	 * @param template
	 *            JecnTemplateData片段存储数据类
	 * @return List<JecnBaseFlowElementPanel>片段对应的所有图形
	 */
	public static List<JecnBaseFlowElementPanel> getAllCloneFlowElementByTemplate(JecnTemplateData template) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || template == null) {
			return null;
		}
		// 记录所有克隆后的数据集合
		List<JecnBaseFlowElementPanel> allFlowElementClone = new ArrayList<JecnBaseFlowElementPanel>();
		// 记录所有元素
		List<JecnBaseFlowElementPanel> allFlowElement = new ArrayList<JecnBaseFlowElementPanel>();

		// 把所有图形添加到所有元素集合
		allFlowElement.addAll(template.getTemplateElementList());
		// 根据复制或剪切的集合获取需要粘贴的集合数据
		JecnFlowElementCopyAndPaste.getPasteList(allFlowElement, allFlowElementClone);

		return allFlowElementClone;
	}

	/**
	 * 设置流程元素层级
	 * 
	 * @param flowElementPanel
	 */
	public static void setFlowElementOrder(JecnBaseFlowElementPanel flowElementPanel) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || flowElementPanel == null) {
			return;
		}
		// 设置层级
		flowElementPanel.getFlowElementData().setZOrderIndex(desktopPane.getMaxZOrderIndex());
		desktopPane.setMaxZOrderIndex(desktopPane.getMaxZOrderIndex() + 2);
		if (flowElementPanel instanceof DividingLine || flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 分割线和连接线不设置层级
			// 连接线获取层级，小线段层级为大线数据层层级
			return;
		}
		// 设置图形层级
		flowElementPanel.reSetFlowElementZorder();
	}

	private static void addVHDiviLine(JecnBaseDividingLinePanel jecnBaseDividingLinePanel, int moveX, int moveY) {

		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();

		if (jecnBaseDividingLinePanel instanceof HDividingLine || jecnBaseDividingLinePanel instanceof VDividingLine) {
			// 获取图形的位置点
			int tempPointX = DrawCommon.convertDoubleToInt(jecnBaseDividingLinePanel.getFlowElementData()
					.getOriginalPoint().x
					* scale);
			int tempPointY = DrawCommon.convertDoubleToInt(jecnBaseDividingLinePanel.getFlowElementData()
					.getOriginalPoint().y
					* scale);

			Point tempPoint = new Point();

			tempPoint.setLocation(tempPointX + moveX, tempPointY + moveY);

			int width = jecnBaseDividingLinePanel.getFlowElementData().getDiviLineCloneable().getWidth();

			int height = jecnBaseDividingLinePanel.getFlowElementData().getDiviLineCloneable().getHeight();

			jecnBaseDividingLinePanel.getFlowElementData().setFigureSizeX(width);

			jecnBaseDividingLinePanel.getFlowElementData().setFigureSizeY(height);
			jecnBaseDividingLinePanel.setVHDiviLineBounds(tempPoint);
		} else if (jecnBaseDividingLinePanel instanceof DividingLine) {
			int startX = jecnBaseDividingLinePanel.getStartPoint().x;
			int startY = jecnBaseDividingLinePanel.getStartPoint().y;

			Point tempStartPoint = new Point();

			tempStartPoint.setLocation(startX + moveX, startY + moveY);
			int endX = jecnBaseDividingLinePanel.getEndPoint().x;
			int endY = jecnBaseDividingLinePanel.getEndPoint().y;

			Point tempEndPoint = new Point();

			tempEndPoint.setLocation(endX + moveX, endY + moveY);
			// 记录分割线100%状态下的克隆值
			jecnBaseDividingLinePanel.originalCloneData(tempStartPoint, tempEndPoint);
		}
	}

	/**
	 * 
	 * 给定参数按照序号排序
	 * 
	 * @param itemBeanList
	 */
	public static void sortAllFlowElementCloneList(List<JecnBaseFlowElementPanel> allFlowElementClone) {

		if (allFlowElementClone == null || allFlowElementClone.size() == 0) {
			return;
		}

		Collections.sort(allFlowElementClone, new Comparator<JecnBaseFlowElementPanel>() {
			public int compare(JecnBaseFlowElementPanel r1, JecnBaseFlowElementPanel r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("参数不合法");
				}
				int sortId1 = r1.getFlowElementData().getZOrderIndex();
				int sortId2 = r2.getFlowElementData().getZOrderIndex();
				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 添加导入的连接线
	 * 
	 * @param manLineMapList
	 *            大线对应的小线的 Map结婚
	 * @param addListPanel
	 *            记录所有元素
	 * @param moveX
	 *            位移的X
	 * @param moveY
	 *            位移的Y
	 */
	public static void addImportManLine(
			Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> manLineMapList,
			List<JecnBaseFlowElementPanel> addListPanel, int moveX, int moveY) {
		Iterator<JecnBaseManhattanLinePanel> iterator = manLineMapList.keySet().iterator();

		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();

		while (iterator.hasNext()) {
			JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) iterator.next();
			// 获取小线段数据
			List<JecnManhattanRouterResultData> datas = manLineMapList.get(linePanel);
			// 清空小线段集合
			linePanel.getLineSegments().clear();

			linePanel.getFlowElementData().getLineSegmentDataList().clear();
			for (JecnManhattanRouterResultData resultData : datas) {
				// 获取开始点
				Point startPoint = new Point();
				// 获取结束点
				Point endPoint = new Point();

				startPoint.setLocation(resultData.getStartPoint().x + moveX / scale, resultData.getStartPoint().y
						+ moveY / scale);
				endPoint.setLocation(resultData.getEndPint().x + moveX / scale, resultData.getEndPint().y + moveY
						/ scale);

				linePanel.addLineSegment(startPoint, endPoint);
			}

			if (addListPanel != null && !addListPanel.contains(linePanel)) {
				addListPanel.add(linePanel);
			}
		}
	}

	/**
	 * 导入模板文件
	 * 
	 * @param type
	 *            是否为标题
	 * @param ID
	 *            片段唯一标识
	 */
	public static JecnTemplateData importTemplateXml(String ID, MapType mapType) {
		// 读取流程地图
		JecnTemplateData selectTemplateData = null;
		// 循环缓存中的数据
		for (JecnTemplateData template : JecnSystemData.getTemplateDataList()) {
			if (ID.equals(template.getTemplateId())) {
				if (template.getTemplateElementList().size() > 0) {
					selectTemplateData = template;
					return selectTemplateData;
				} else {
					// 读取xml的document
					Document doc = JecnXmlUtil.getDocumentByAllPath(getXmlPath(mapType));
					if (doc == null) {
						return null;
					}
					selectTemplateData = readTemplateData(doc, template);
					return selectTemplateData;
				}
			}
		}
		return null;
	}

	/**
	 * * 导出图形的数据
	 * 
	 * @param currentSelectElement
	 *            图形的数据
	 * @param xmlPath
	 *            导出的路径
	 */
	public static JecnTemplateData exportFigureLine(List<JecnBaseFlowElementPanel> currentSelectElement, String name,
			MapType mapType) {
		String xmlPath = getXmlPath(mapType);
		// 返回临时文件名称：文件路径 + 系统毫秒数＋ jpg
		String pathCopy = getPathCopy();
		// 图片存储的完整路径名
		String imagerPath = JecnFileUtil.getSyncPath(TEMP_COPY) + pathCopy;
		JecnFlowElementCopyAndPaste flowElementCopyAndPaste = new JecnFlowElementCopyAndPaste();
		// 获取复制时的选中区域 根据大小导出为图形
		boolean isSave = flowElementCopyAndPaste.copyImageToSystem(imagerPath, 100, 60);
		if (!isSave) {
			JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnResourceUtil.getJecnResourceUtil()
					.getValue("selectedGraphicsIllegal"));
			return null;
		}
		Document doc = null;
		// 判断文件夹是否存在
		File f = new File(xmlPath);
		if (f.exists()) {
			doc = JecnXmlUtil.getDocumentByAllPath(xmlPath);
		} else {
			// 读取XML 获取Document 对象
			doc = JecnXmlUtil.getDocument();
		}
		// 新建模板数据
		JecnTemplateData templateData = new JecnTemplateData();

		JecnElement partInfo = null;
		if (doc.getElementsByTagName("mainPart").getLength() > 0) {
			partInfo = new JecnElement(doc.getDocumentElement());
		} else {
			partInfo = new JecnElement(doc.createElement("mainPart"));
		}
		JecnElement partData = new JecnElement(doc.createElement("partData"));
		// 片段名称
		partData.setAttribute("partName", name);
		templateData.setTemplateName(name);

		// 关联图片名称
		partData.setAttribute("relevancyImager", pathCopy);
		templateData.setImagerPath(imagerPath);

		// 获取所有选中组件元素
		List<JecnBaseFlowElementPanel> allComponents = new ArrayList<JecnBaseFlowElementPanel>();
		// 获取所有选中组件元素
		JecnFlowElementCopyAndPaste.getListBySelectList(allComponents);

		JecnSelectFigureXY figureXY = JecnWorkflowUtil.getSelectFigureXY(allComponents);

		double originalScale = 1 / getWorkflow().getWorkflowScale();

		// 选中区域的宽
		int selectWidth = (int) ((figureXY.getMaxX() - figureXY.getMinX()) * originalScale + 10);
		partData.setAttribute("selectWidth", String.valueOf(selectWidth));
		templateData.setSelectWidth(String.valueOf(selectWidth));

		// 选中区域的高
		int selectHeight = (int) ((figureXY.getMaxY() - figureXY.getMinY()) * originalScale + 10);
		partData.setAttribute("selectHeight", String.valueOf(selectHeight));
		templateData.setSelectHeight(String.valueOf(selectHeight));

		// 选中区域的中心点
		int selectElementX = (int) (((figureXY.getMaxX() - figureXY.getMinX()) / 2 + figureXY.getMinX()) * originalScale);

		int selectElementY = (int) (((figureXY.getMaxY() - figureXY.getMinY()) / 2 + figureXY.getMinY()) * originalScale);

		// 选中区域中心点的X
		partData.setAttribute("selectElementX", String.valueOf(selectElementX));
		// 选中区域中心点的Y
		partData.setAttribute("selectElementY", String.valueOf(selectElementY));
		templateData.setTemplatePoint(new Point(selectElementX, selectElementY));

		// 唯一ID
		String templateId = DrawCommon.getUUID();
		partData.setAttribute("ID", templateId);
		templateData.setTemplateId(templateId);
		// 类型
		partData.setAttribute("templateMapType", mapType.toString());
		templateData.setMapType(mapType.toString());

		// 调用导出
		JecnFlowExport flowExport = new JecnFlowExport();

		// 是否是横向标识 true：横向 false：纵向
		boolean falg = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().isHFlag();
		for (JecnBaseFlowElementPanel baseFlowElementPanel : allComponents) {
			if (baseFlowElementPanel instanceof JecnBaseFigurePanel) {
				// 保存图形到XML
				flowExport.getExportFigure(doc, partData, (JecnFigureData) baseFlowElementPanel.getFlowElementData(),
						falg);
				// 添加图形到模板数据
				// templateData.getTemplateElementList().add(baseFlowElementPanel.clone());
			} else if (baseFlowElementPanel instanceof JecnBaseDividingLinePanel
					|| baseFlowElementPanel instanceof JecnBaseManhattanLinePanel) { // 保存线(连接线或斜线)
				// 获取线的数据
				JecnBaseLineData baseLineData = (JecnBaseLineData) baseFlowElementPanel.getFlowElementData();
				// 导出线到XML
				flowExport.getExportLine(doc, partData, baseLineData);
			}
		}
		partInfo.appendChild(partData.getElement());
		// 判断如果xml中存在了 主节点则把 节点添加到主节点中
		if (doc.getElementsByTagName("mainPart").getLength() > 0) {
			partData.appendChild(doc.createTextNode(""));
		} else { // 新建主节点
			doc.appendChild(partInfo.getElement());
		}
		// 返回添加是否成功
		boolean flag = JecnXmlUtil.writeXmlByDocumentAndAllPath(xmlPath, doc);
		if (flag) {
			// 判断是否为设计器
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
				saveTemplateData = templateData;
				// 图片名称
				saveTemplateData.setImagerName(pathCopy);
				JecnDesignerProcess.getDesignerProcess().saveTemplate();
			}
			return templateData;
		}
		return null;
	}

	/**
	 * 删除xml中的节点
	 * 
	 * @param xmlPath
	 *            xml路径
	 * @param ID
	 *            片段的唯一ID
	 */
	public static boolean deleteXmlNode(MapType mapType, String Id) {
		// 获取路径
		String xmlPath = getXmlPath(mapType);
		// 获取dom
		Document doc = JecnXmlUtil.getDocumentByAllPath(xmlPath);
		// 图片路径
		String imagerPath = JecnFileUtil.getSyncPath(TEMP_COPY);
		// 图片名称
		String imagerName = null;
		if (doc != null) {
			// 获取主节点
			NodeList partData = doc.getElementsByTagName("partData");
			for (int i = 0; i < partData.getLength(); i++) {
				Element partElement = (Element) partData.item(i);
				if (Id.equals(partElement.getAttribute("ID"))) {
					// 图片名称
					imagerName = partElement.getAttribute("relevancyImager");
					// 获取图片路径
					imagerPath += imagerName;
					// 获取此节点的父节点 在删除此节点
					partElement.getParentNode().removeChild(partElement);
				}
			}
			boolean deleteXmlFlag = JecnXmlUtil.writeXmlByDocumentAndAllPath(xmlPath, doc);
			// 修改xml成功执行下面的操作
			if (deleteXmlFlag) {
				// 判断是否为设计器
				if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
					mapType = replaceType(mapType);
					getDeleteTemplateMap().put(mapType.toString(), imagerName);
					// 调用删除方法
					JecnDesignerProcess.getDesignerProcess().deleteTemplate();
				}
				if (imagerPath != null) {
					// 读取图片
					File imagerFile = new File(imagerPath);
					// 删除图片
					imagerFile.delete();
				}
				// 删除缓存中的数据
				deleteTemplateList(Id);
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * 删除缓存中的数据
	 * 
	 * @param Id
	 *            片段唯一标识
	 */
	private static void deleteTemplateList(String Id) {
		for (int i = 0; i < JecnSystemData.getTemplateDataList().size(); i++) {
			JecnTemplateData templateData = JecnSystemData.getTemplateDataList().get(i);
			if (templateData.getTemplateId().equals(Id)) {
				JecnSystemData.getTemplateDataList().remove(i);
			}
		}
	}

	/**
	 * 修改xml节点数据
	 * 
	 * @param xmlPath
	 *            路径
	 * @param ID
	 *            片段的唯一ID
	 * 
	 */
	public static boolean updateXmlNode(MapType mapType, String ID, String templateName) {
		// 获取路径
		String xmlPath = getXmlPath(mapType);
		// 获取dom
		Document doc = JecnXmlUtil.getDocumentByAllPath(xmlPath);
		// 图片名称
		String imageName = null;
		if (doc != null) {
			// 获取主节点
			NodeList partData = doc.getElementsByTagName("partData");
			if (partData != null && partData.getLength() > 0) {
				for (int i = 0; i < partData.getLength(); i++) {
					Element partElement = (Element) partData.item(i);
					if (ID.equals(partElement.getAttribute("ID"))) {
						partElement.setAttribute("partName", templateName);
					}
					imageName = partElement.getAttribute("relevancyImager");
				}
			} else {
				return false;
			}

			boolean xmlFlag = JecnXmlUtil.writeXmlByDocumentAndAllPath(xmlPath, doc);
			if (!xmlFlag) {
				return false;
			}
		} else {
			return false;
		}
		// 如果为设计器,就把图片名称和修改的名称传入设计器中
		if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
			if (imageName != null) {
				getUpdateTemplateMap().put(imageName, templateName);
				JecnDesignerProcess.getDesignerProcess().updateTemplate();
			}
		}
		return true;
	}

	/**
	 * 读取xml 中 partData下的数据
	 * 
	 * @param doc
	 */
	private static JecnTemplateData readTemplateData(Document doc, JecnTemplateData template) {
		// 获取读取XML figures节点的数据
		NodeList templateData = doc.getElementsByTagName("partData");
		// 获取导出xml的对象
		JecnFlowImport flowImport = new JecnFlowImport();
		for (int i = 0; i < templateData.getLength(); i++) {
			Element templateElement = (Element) templateData.item(i);
			// 获取ID
			String templateId = templateElement.getAttribute("ID");
			// 判断ID是否相等
			if (templateId.equals(template.getTemplateId())) {
				// 获取中心点X
				String selectElementX = templateElement.getAttribute("selectElementX");
				// 获取中心点Y
				String selectElementY = templateElement.getAttribute("selectElementY");
				// 中心点的Point
				Point templatePoint = new Point(Integer.valueOf(selectElementX), Integer.valueOf(selectElementY));
				template.setTemplatePoint(templatePoint);

				// 区域的宽
				String selectWidth = templateElement.getAttribute("selectWidth");
				template.setSelectWidth(selectWidth);
				// 区域的高
				String selectHeight = templateElement.getAttribute("selectHeight");
				template.setSelectHeight(selectHeight);

				// 获取节点下的图形的数据
				NodeList templateFigureData = templateElement.getElementsByTagName("figures");
				// 接收返回回来的参数
				List<JecnBaseFlowElementPanel> templateElementList = flowImport.gainFiguresDate(templateFigureData);

				// 获取节点下的线条的数据
				NodeList commonLinesLineData = templateElement.getElementsByTagName("CommonLines");

				// 不带箭头的连接下
				flowImport.gainConnectingLine(commonLinesLineData, templateElementList, template.getManLineMapList());

				NodeList manhattanLineLineData = templateElement.getElementsByTagName("manhattanLines");
				// 带箭头的连接下
				flowImport.gainConnectingLine(manhattanLineLineData, templateElementList, template.getManLineMapList());

				// 直线，横线，斜线
				NodeList baseDivedingLine = templateElement.getElementsByTagName("dividLines");
				flowImport.getBaseDivedingLineData(baseDivedingLine, templateElementList);

				template.setTemplateElementList(templateElementList);
				return template;
			}
		}
		return null;
	}

	/**
	 * 获取面板
	 */
	private static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 通过模板数据取出相对应的按钮
	 * 
	 * @param templateMap
	 *            对应数据的集合
	 * @param Id
	 *            模板的Id
	 * @return
	 */
	public static JecnToolBar getToolBarToTemplateData(Map<JecnTemplateData, JecnToolBar> templateMap, String Id) {
		Iterator<JecnTemplateData> it = templateMap.keySet().iterator();
		while (it.hasNext()) {
			// 获取Map的Key值
			JecnTemplateData templateData = (JecnTemplateData) it.next();
			if (Id.equals(templateData.getTemplateId())) {
				// 获取Map的value
				JecnToolBar toolBar = (JecnToolBar) templateMap.get(templateData);
				return toolBar;
			}
		}
		return null;
	}

	public static Map<String, String> getTemplateNameMap() {
		if (templateNameMap == null) {
			templateNameMap = new HashMap<String, String>();
		}
		return templateNameMap;
	}

	public static Map<String, String> getUpdateTemplateMap() {
		if (updateTemplateMap == null) {
			updateTemplateMap = new HashMap<String, String>();
		}
		return updateTemplateMap;
	}

	public static void setUpdateTemplateMap(Map<String, String> updateTemplateMap) {
		JecnFlowTemplate.updateTemplateMap = updateTemplateMap;
	}

	public static JecnTemplateData getSaveTemplateData() {
		return saveTemplateData;
	}

	public static void setSaveTemplateData(JecnTemplateData saveTemplateData) {
		JecnFlowTemplate.saveTemplateData = saveTemplateData;
	}

	public static Map<String, String> getDeleteTemplateMap() {
		if (deleteTemplateMap == null) {
			deleteTemplateMap = new HashMap<String, String>();
		}
		return deleteTemplateMap;
	}

	public static void setDeleteTemplateMap(Map<String, String> deleteTemplateMap) {
		JecnFlowTemplate.deleteTemplateMap = deleteTemplateMap;
	}

	private static MapType replaceType(MapType mapType) {
		return mapType == MapType.totalMapRelation ? MapType.totalMap : mapType;
	}

}
