package epros.designer.gui.popedom.person;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.UserEditBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnTableScrollPane;
import epros.draw.gui.swing.JecnToolbarButton;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 登录人个人信息
 * 
 * @author ZHOUXY
 * 
 */
class JecnPersonInfoDialog extends JecnDialog implements ActionListener {
	private final Log log = LogFactory.getLog(JecnPersonInfoDialog.class);
	/** 主界面 */
	private JecnPanel mainPanel = null;

	/** 登录名称标签 */
	private JLabel loginNameTitleLabel = null;
	/** 登录名称内容 */
	private JLabel loginNameContentLabel = null;

	/** 真实姓名标签 */
	private JLabel trueNameTitileLabel = null;
	/** 真实姓名内容 */
	private JLabel trueNameContentLabel = null;

	/** 系统角色标签 */
	private JLabel roleLabel = null;
	/** 系统角色内容 */
	private JTextArea roleTextArea = null;

	/** 人员对于岗位 */
	private JecnTableScrollPane tableScrollPane = null;

	/** 按钮面板 */
	private JPanel btnsPanel = null;
	/** 密码修改按钮 */
	private JecnToolbarButton updatePwdBtn = null;
	/** 关闭按钮 */
	private JButton closeBtn = null;

	JecnPersonInfoDialog() {
		initComponents();
		initLayout();
		initData();
	}

	private void initComponents() {
		// 主界面
		mainPanel = new JecnPanel();

		// 登录名称标签
		loginNameTitleLabel = new JLabel();
		// 登录名称内容
		loginNameContentLabel = new JLabel();

		// 真实姓名标签
		trueNameTitileLabel = new JLabel();
		// 真实姓名内容
		trueNameContentLabel = new JLabel();

		// 系统角色标签
		roleLabel = new JLabel();
		// 系统角色内容
		roleTextArea = new JTextArea();

		// 人员对于岗位
		tableScrollPane = new JecnTableScrollPane();

		// 按钮面板
		btnsPanel = new JPanel();
		// 密码修改按钮
		updatePwdBtn = new JecnToolbarButton();
		// 关闭按钮
		closeBtn = new JButton();

		this.setTitle(JecnProperties.getValue("personInfoTitle"));
		updatePwdBtn.setText(JecnProperties.getValue("passwordUpdate"));
		loginNameTitleLabel.setText(JecnProperties.getValue("loginNameC"));
		trueNameTitileLabel.setText(JecnProperties.getValue("realNameC"));
		roleLabel.setText(JecnProperties.getValue("personInfoRoleName"));
		closeBtn.setText(JecnProperties.getValue("personInfoCloseName"));

		btnsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 3));
		btnsPanel.setOpaque(false);
		btnsPanel.setBorder(null);

		roleTextArea.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		roleTextArea.setWrapStyleWord(true);
		roleTextArea.setLineWrap(true);
		roleTextArea.setEditable(false);

		// 表头
		Object[] tableHeaderArray = new Object[] { JecnProperties.getValue("personInfoPosName"),
				JecnProperties.getValue("personInfoDestName") };
		// 添加表模型
		JecnDefaultTableModel tableModel = new JecnDefaultTableModel();
		tableScrollPane.getTable().setModel(tableModel);
		// 设置表头
		tableModel.setColumnIdentifiers(tableHeaderArray);
		tableScrollPane.getTable().setRowHeight(25);

		Dimension size = new Dimension(600, 450);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);

		updatePwdBtn.addActionListener(this);
		closeBtn.addActionListener(this);
		// 是否存在浏览端，如果存在浏览端则隐藏岗位列表
		if (!JecnConstants.isPubShow()) {
			tableScrollPane.setVisible(false);
			this.setMinimumSize(null);
			this.setSize(400, 185);
			this.setModal(true);
			this.setLocationRelativeTo(null);

		}
	}

	private void initLayout() {

		this.getContentPane().add(mainPanel);

		Insets insetsLeftFirst = new Insets(10, 15, 5, 3);
		Insets insetsRightFirst = new Insets(10, 0, 5, 15);

		Insets insetsLeft = new Insets(5, 15, 5, 3);
		Insets insetsRight = new Insets(5, 0, 5, 15);

		Insets insets = new Insets(5, 5, 5, 5);

		GridBagConstraints c = null;
		// 登录名称标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsLeftFirst, 0, 0);
		mainPanel.add(loginNameTitleLabel, c);
		// 登录名称内容
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insetsRightFirst, 0, 0);
		mainPanel.add(loginNameContentLabel, c);
		// 密码修改按钮
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 5), 0, 0);
		mainPanel.add(updatePwdBtn.getJToolBar(), c);

		// 真实姓名标签
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsLeft, 0, 0);
		mainPanel.add(trueNameTitileLabel, c);
		// 真实姓名内容
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		mainPanel.add(trueNameContentLabel, c);

		// 系统角色标签
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLeft,
				0, 0);
		mainPanel.add(roleLabel, c);
		// 系统角色内容
		c = new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		mainPanel.add(roleTextArea, c);

		// 人员对于岗位
		c = new GridBagConstraints(0, 3, 3, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(tableScrollPane, c);
		// 是否存在浏览端，如果存在浏览端则添加自动撑满Label()
		if (!JecnConstants.isPubShow()) {
			c = new GridBagConstraints(0, 4, 3, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
					insets, 0, 0);
			mainPanel.add(new JLabel(), c);
		}

		// 按钮面板
		c = new GridBagConstraints(0, 5, 3, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(btnsPanel, c);
		// 关闭按钮
		btnsPanel.add(closeBtn);
	}

	private void initData() {
		Long id = JecnConstants.loginBean.getJecnUser().getPeopleId();
		if (id == null) {
			return;
		}
		try {
			UserEditBean userEditBean = ConnectionPool.getPersonAction().getUserEidtBeanByPeopleId(id);

			if (userEditBean == null || userEditBean.getJecnUser() == null) {
				return;
			}
			if (userEditBean.getRoleList() != null) {
				List<JecnTreeBean> roleList = userEditBean.getRoleList();
				for (JecnTreeBean jecnTreeBean : roleList) {
					if(jecnTreeBean.getNumberId() != null && StringUtils.isNotBlank(jecnTreeBean.getNumberId())){
						jecnTreeBean.setName(JecnProperties.getValue(jecnTreeBean.getNumberId()));
					}
				}
			}
			// 登录名称内容
			loginNameContentLabel.setText(userEditBean.getJecnUser().getLoginName());
			// 真实姓名内容
			trueNameContentLabel.setText(getTrueName(userEditBean.getJecnUser()));
			// 系统角色内容
			roleTextArea.setText(userEditBean.getRoleListString());

			List<JecnTreeBean> posList = userEditBean.getPosList();
			if (posList == null || posList.size() == 0) {
				return;
			}
			for (JecnTreeBean treeBean : posList) {
				// 岗位名称
				String posName = treeBean.getName();
				// 部门名称
				String destName = treeBean.getPname();

				((DefaultTableModel) tableScrollPane.getTable().getModel()).addRow(new String[] { posName, destName });
			}

		} catch (Exception e) {
			log.error("JecnPersonInfoDialog initData is error", e);
		}
	}

	private String getTrueName(JecnUser user) {
		if (!JecnConfigTool.isIflytekLogin()) {
			return user.getTrueName();
		}
		return ConnectionPool.getPersonAction().getListUserSearchBean(user.getLoginName());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == updatePwdBtn) {
			PasswordUpdateDialog passwordUpdateDialog = new PasswordUpdateDialog(this);
			passwordUpdateDialog.setVisible(true);
		} else if (e.getSource() == closeBtn) {
			this.setVisible(false);
		}
	}

	/**
	 * 
	 * 表的模型类
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class JecnDefaultTableModel extends DefaultTableModel {

		/**
		 * 
		 * 是否编辑
		 * 
		 */
		public boolean isCellEditable(int row, int column) {
			return false;
		}

	}
}
