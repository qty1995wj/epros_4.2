package com.jecn.epros.service.keySearch;

import com.jecn.epros.domain.keySearch.ResourceSearchResultBean;

import java.util.Map;

public interface KeySearchService {

    ResourceSearchResultBean keyWordSearch(Map<String, Object> map);

}
