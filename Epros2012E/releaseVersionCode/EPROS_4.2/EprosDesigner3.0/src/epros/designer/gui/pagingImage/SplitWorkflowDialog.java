package epros.designer.gui.pagingImage;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.util.JecnProperties;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 界面生成为图片
 * 
 * @author fuzhh 2013-11-6
 * 
 */
public class SplitWorkflowDialog extends JecnDialog {

	/** 主panel */
	private JPanel mainPanel;
	/** 滚动面板 */
	private JScrollPane imageJScrollPanel;
	/** 图片存放panel */
	private JPanel imagePanel;

	/** 面板 */
	private JecnDrawDesktopPane workflow = null;
	/** 所有图形List */
	private List<JecnBaseFlowElementPanel> allFigureList;
	/** 面板大小 */
	private JecnSelectFigureXY figureXY;

	/** 需要导出的JPanel集合 */
	public List<JPanel> panelList = null;

	/** dialog 宽度 */
	private int dialogWidth = 620;
	/** dialog 高度 */
	private int dialogHeight = 700;

	public SplitWorkflowDialog() {
		// 初始化面板
		workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 初始化组件
		initComponents();

		setPageDiaSize();
		// 创建图片面板
		createImageWorkflow();
	}

	/**
	 * 创建图片面板
	 * 
	 * @author fuzhh 2013-11-12
	 * 
	 */
	private void createImageWorkflow() {
		// 获取所有图形
		allFigureList = workflow.getPanelList();
		// 面板大小
		figureXY = JecnWorkflowUtil.getMaxSize(workflow, allFigureList);
		// 生成面板 数据
		setPicture();
	}

	/**
	 * 组件初始化
	 * 
	 * @author fuzhh 2013-11-6
	 * 
	 */
	private void initComponents() {
		// 设置为模态窗口
		this.setModal(true);
		// 设置标题 预览面板
		this.setTitle(JecnProperties.getValue("previewPanel"));
		// 设置初始大小
		this.setSize(dialogWidth, dialogHeight);
		// 关闭退出
		this.setDefaultCloseOperation(SplitWorkflowDialog.DISPOSE_ON_CLOSE);
		// 设置居中 ， 在size 之后
		setLocationRelativeTo(null);

		// 初始化主panel
		mainPanel = new JPanel();
		// 设置主Panel 的布局方式
		mainPanel.setLayout(new BorderLayout());
		// 设置主panel的背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 初始化图片存放panel
		imagePanel = new JPanel();
		// 设置图片panel的存放方式
		imagePanel.setLayout(new GridBagLayout());
		// 初始化滚动面板
		imageJScrollPanel = new JScrollPane(imagePanel);
		// 设置panel的默认背景色
		imagePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置背景色
		imageJScrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		imageJScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 把滚动面板放入主panel中
		mainPanel.add(imageJScrollPanel, BorderLayout.CENTER);
		// 把主panel放入自定义的panel
		this.add(mainPanel);
	}

	/**
	 * 
	 * 
	 * author fuzhh
	 * 
	 * 2013-11-6 上午10:09:53
	 */
	private void setPicture() {
		imagePanel.removeAll();

		panelList = new ArrayList<JPanel>();
		// 角色面板大小
		int roleWidth = PagingImageUtil.getVHPagingLine(workflow);
		// 面板高度
		int workflowHeight = figureXY.getMaxY();
		// 分页宽度
		int pagingWidth = workflow.getFlowMapData().getPageSetingBean().getPageWidth();
		// 面板缩放比例
		double scale = 1.0;
		// 面板x大小
		int workflowWidth = figureXY.getMaxX();

		// 图形截取面板
		int movX = 0;

		// 面板个数
		int count = (int) ((workflowWidth - roleWidth) / pagingWidth + 1);
		for (int i = 0; i < count; i++) {
			// 每页图像显示区域
			movX = i * pagingWidth;
			// 生成角色面板
			SplitPageRoleJPanel splitPageRoleJPanel = new SplitPageRoleJPanel(workflow, figureXY, scale);
			// 打印内容panel
			SplitPreviewJPanel splitPreviewJPanel = new SplitPreviewJPanel(pagingWidth, workflowHeight, movX,
					roleWidth, scale, workflow);
			// 拼装的panel
			SplitPageImageJPanel splitPageImageJPanel = new SplitPageImageJPanel(splitPreviewJPanel,
					splitPageRoleJPanel, pagingWidth + roleWidth + 5, workflowHeight, i, workflow.getBackground());
			GridBagConstraints c = null;
			if (i == count - 1) {
				c = new GridBagConstraints(0, i, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
						GridBagConstraints.HORIZONTAL, new Insets(3, 0, 30, 0), 0, 0);
			} else {
				c = new GridBagConstraints(0, i, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
						GridBagConstraints.HORIZONTAL, new Insets(3, 0, 3, 0), 0, 0);
			}
			panelList.add(splitPageImageJPanel);
			imagePanel.add(splitPageImageJPanel, c);
		}
		imagePanel.repaint();
		imagePanel.updateUI();
	}

	public List<JPanel> getPanelList() {
		return panelList;
	}

	public void setPanelList(List<JPanel> panelList) {
		this.panelList = panelList;
	}

	public void setPageDiaSize() {
		this.setSize(dialogWidth, dialogHeight);
	}
}
