package com.jecn.epros.server.bean.process;

/**
 * to、from和活动关联关系（设计器）
 * 
 * @author Administrator
 * 
 */
public class JecnToFromActiveT implements java.io.Serializable {

	/** 对应TO FROM JecnFlowStructureImageT的figureId 主键id **/
	private Long toFromFigureId = -1L;
	/** 对应活动JecnFlowStructureImageT的figureId **/
	private Long activeFigureId = -1L;
	/** 设计器TO FROM唯一标识 **/
	private String toFromFigureUUId;
	/** 设计器活动唯一标识 **/
	private String activeFigureUUId;
	/** 流程id **/
	private Long flowId;

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getToFromFigureUUId() {
		return toFromFigureUUId;
	}

	public void setToFromFigureUUId(String toFromFigureUUId) {
		this.toFromFigureUUId = toFromFigureUUId;
	}

	public String getActiveFigureUUId() {
		return activeFigureUUId;
	}

	public void setActiveFigureUUId(String activeFigureUUId) {
		this.activeFigureUUId = activeFigureUUId;
	}

	public Long getToFromFigureId() {
		return toFromFigureId;
	}

	public void setToFromFigureId(Long toFromFigureId) {
		this.toFromFigureId = toFromFigureId;
	}

	public Long getActiveFigureId() {
		return activeFigureId;
	}

	public void setActiveFigureId(Long activeFigureId) {
		this.activeFigureId = activeFigureId;
	}
}
