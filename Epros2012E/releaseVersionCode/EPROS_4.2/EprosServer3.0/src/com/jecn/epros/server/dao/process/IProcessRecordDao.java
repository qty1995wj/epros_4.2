package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowRecord;
import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessRecordDao extends IBaseDao<JecnFlowRecordT, Long> {
	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程下的记录保存
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowRecordT> getJecnFlowRecordTListByFlowId(Long flowId)
			throws Exception;
	
	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程下的记录保存
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowRecord> getJecnFlowRecordListByFlowId(Long flowId)
			throws Exception;
	
	
	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程下的记录保存  0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式 6编号 7移交责任
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRecordListByFlowId(Long flowId,boolean isPub)
			throws Exception;
}
