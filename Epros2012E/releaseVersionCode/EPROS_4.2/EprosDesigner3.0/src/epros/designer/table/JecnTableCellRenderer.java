package epros.designer.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import epros.designer.gui.rule.edit.BaseCommonJTable;
import epros.designer.util.JecnProperties;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 渲染
 * 
 * @author Administrator
 * @date： 日期：2013-11-20 时间：上午10:51:08
 */
public class JecnTableCellRenderer extends JLabel implements TableCellRenderer {

	// true：需要加色渲染
	private boolean isDef = true;

	public JecnTableCellRenderer(String text) {
		this.setHorizontalAlignment(JLabel.CENTER);
		this.setVerticalAlignment(JLabel.CENTER);
		// 详情
		this.setText(text);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
//		BaseCommonJTable editTable = null;
//		if (table instanceof BaseCommonJTable) {
//			editTable = (BaseCommonJTable) table;
//		}
//		this.isDef = (editTable.getMyRow() == row && editTable.getMyColumn() == column);
		return this;
	}

	/**
	 * 
	 * 绘制组件
	 * 
	 */
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;

		int w = this.getWidth();
		int h = this.getHeight();
		if (isDef) {
			// 渐变
			g2d.setPaint(new GradientPaint(0, 0, JecnUIUtil.getTopNoColor(), 0, h / 2, JecnUIUtil.getButtomNoColor()));
			g2d.fillRect(0, 0, w, h);
			this.setForeground(Color.red);
		} else {
			this.setForeground(Color.black);
		}

		super.paint(g);
	}
}
