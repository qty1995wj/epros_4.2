package com.jecn.epros.server.service.reports.excel;

import java.io.File;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceWebBean;

/**
 * 
 * 下载流程有效期维护数据
 * 
 * @author huoyl
 * 
 */
public class ProcessExpiryDownExcelService {

	private Log log = LogFactory.getLog(this.getClass());
	/** 数据对象 */
	private ProcessExpiryMaintenanceBean processExpiryMaintenanceBean;

	private int type = 0;

	public ProcessExpiryDownExcelService(ProcessExpiryMaintenanceBean processExpiryMaintenanceBean) {
		this.processExpiryMaintenanceBean = processExpiryMaintenanceBean;
		this.type = processExpiryMaintenanceBean.getType();
	}


	public byte[] getExcelFile() throws Exception {

		byte[] tempDate = null;
		if (processExpiryMaintenanceBean == null) {
			throw new Exception("ProcessExpiryDownExcelService类processExpiryMaintenanceBean为空");
		}

		WritableWorkbook wbookData = null;
		File file = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path + JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// 写出文件
			writeExcelFile(wbookData);
			// 后续处理--------------------------------------
			wbookData.write();
			// 工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;
			// 把一个文件转化为字节
			tempDate = JecnUtil.getByte(file);

		} catch (Exception e) {
			throw new Exception("ProcessExpiryDownExcelService类getExcelFile方法异常", e);

		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();

				} catch (Exception e) {
					log.error(e);
					throw new Exception("ProcessExpiryDownExcelService类关闭excel工作表异常", e);
				}
			}
			// 删除临时文件
			if (file != null && file.exists()) {
				file.delete();
			}
		}

		return tempDate;
	}

	public void writeExcelFile(WritableWorkbook wbookData) throws Exception {
		// 当前sheet页
		int curSheet = 0;
		// 行
		int row = 0;
		// 列
		int col = 0;
		// 序号
		int index = 1;

		WritableSheet wsheet = wbookData.createSheet("sheet1", curSheet);

		// 序号
		String indexStr = JecnUtil.getValue("index");

		// 文控 id
		String docId = JecnUtil.getValue("control") + "id";
		// 流程id
		String flowId = handleName(JecnUtil.getValue("flow")) + "id";
		// 流程名称
		String processName = handleName(JecnUtil.getValue("processNameOrKeyProcess"));
		// 流程编号
		String flowIdInput = handleName(JecnUtil.getValue("processNumber"));
		// 责任部门
		String responsibilityDepartment = JecnUtil.getValue("responsibilityDepartment");
		// 流程责任人
		String processResponsiblePersons = handleName(JecnUtil.getValue("processResponsiblePersons"));
		// 流程监护人
		String processTheGuardian = handleName(JecnUtil.getValue("processTheGuardian"));
		// 流程拟制人
		String processArtificialPerson = handleName(JecnUtil.getValue("processArtificialPerson"));
		// 发布日期
		String pubDate = JecnUtil.getValue("releaseDate");
		// 版本号
		String versionId = JecnUtil.getValue("versionNumber");

		// 有效期
		String validity = JecnUtil.getValue("validity");
		// 下次审视需要完成时间
		String nextScanTime = JecnUtil.getValue("nextViewNeedFinishTime");
		// 错误原因
		String errorReason = JecnUtil.getValue("errorReason");

		// 定义样式
		WritableCellFormat contentStyle = getSheetContentStyle();
		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		// 表头的样式 背景为橘黄色 字体为黑色
		WritableCellFormat titleStyleO = getSheetTitleStyleO();

		// 流程监护人是否显示
		boolean guardianPeopleIsShow = this.processExpiryMaintenanceBean.isGuardianPeopleIsShow();
		// 流程拟制人是否显示
		boolean artificialPersonIsShow = this.processExpiryMaintenanceBean.isArtificialPersonIsShow();

		// ------------------------写出表头------------------------
		// -------------调整列宽
		// 索引
		wsheet.setColumnView(col++, 5);
		// 文控id
		wsheet.setColumnView(col++, 28);
		// 流程id
		wsheet.setColumnView(col++, 28);
		// 流程名称（或关键过程）
		wsheet.setColumnView(col++, 28);
		// 流程编号
		wsheet.setColumnView(col++, 28);
		// 流程责任部门
		wsheet.setColumnView(col++, 28);
		// 流程责任人
		wsheet.setColumnView(col++, 20);
		// 流程监护人
		if (guardianPeopleIsShow) {
			wsheet.setColumnView(col++, 20);
		}
		// 流程拟制人
		if (artificialPersonIsShow) {
			wsheet.setColumnView(col++, 20);
		}
		// 发布日期
		wsheet.setColumnView(col++, 28);
		// 版本号
		wsheet.setColumnView(col++, 20);
		// 有效期
		wsheet.setColumnView(col++, 10);
		// 下次审视需完成时间
		wsheet.setColumnView(col++, 28);
		// 错误信息列
		if (processExpiryMaintenanceBean.isError()) {
			wsheet.setColumnView(col++, 40);
		}

		// -------------写出表头的内容

		col = 0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, indexStr, titleStyleY));
		wsheet.addCell(new Label(col++, row, docId, titleStyleY));
		wsheet.addCell(new Label(col++, row, flowId, titleStyleY));
		wsheet.addCell(new Label(col++, row, processName, titleStyleY));
		wsheet.addCell(new Label(col++, row, flowIdInput, titleStyleY));
		wsheet.addCell(new Label(col++, row, responsibilityDepartment, titleStyleY));
		wsheet.addCell(new Label(col++, row, processResponsiblePersons, titleStyleY));
		if (guardianPeopleIsShow) {
			wsheet.addCell(new Label(col++, row, processTheGuardian, titleStyleY));
		}
		if (artificialPersonIsShow) {
			wsheet.addCell(new Label(col++, row, processArtificialPerson, titleStyleY));
		}
		wsheet.addCell(new Label(col++, row, pubDate, titleStyleY));
		wsheet.addCell(new Label(col++, row, versionId, titleStyleY));
		wsheet.addCell(new Label(col++, row, validity, titleStyleO));
		wsheet.addCell(new Label(col++, row, nextScanTime, titleStyleO));
		// 错误信息列
		if (processExpiryMaintenanceBean.isError()) {
			wsheet.addCell(new Label(col++, row, errorReason, titleStyleY));
		}

		// 写出所有数据
		for (ProcessExpiryMaintenanceWebBean webBean : processExpiryMaintenanceBean.getExpiryMaintenanceWebs()) {
			// 写入下一行
			row++;
			col = 0;
			wsheet.setRowView(row, 400);
			wsheet.addCell(new Label(col++, row, String.valueOf(index), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getDocId().toString(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getFlowId().toString(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getFlowName(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getFlowIdInput(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getDutyOrgName(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getDutyPeopleName(), contentStyle));
			if (guardianPeopleIsShow) {
				wsheet.addCell(new Label(col++, row, webBean.getGuardianName(), contentStyle));
			}
			if (artificialPersonIsShow) {
				wsheet.addCell(new Label(col++, row, webBean.getPeopleMakeName(), contentStyle));
			}
			wsheet.addCell(new Label(col++, row, webBean.getPubDate(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getVersionId(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getExpiry(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getNextScandDate(), contentStyle));
			if (processExpiryMaintenanceBean.isError()) {
				wsheet.addCell(new Label(col++, row, webBean.getErrorInfo(), contentStyle));
			}

			index++;
		}

	}

	private String handleName(String value) {
		if (type == 0) {
			return value;
		} else {
			return value.replaceAll("流程", "制度");
		}
	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleY() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.RED);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);

		return tempCellFormat;
	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleO() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);

		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc, NumberFormats.TEXT);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);
		tempCellFormat.setBackground(Colour.LIGHT_ORANGE);
		return tempCellFormat;
	}

	/**
	 * 表头的内容样式
	 */
	private WritableCellFormat getSheetContentStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.NO_BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc, NumberFormats.TEXT);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	public ProcessExpiryMaintenanceBean getProcessExpiryMaintenanceBean() {
		return processExpiryMaintenanceBean;
	}

	public void setProcessExpiryMaintenanceBean(ProcessExpiryMaintenanceBean processExpiryMaintenanceBean) {
		this.processExpiryMaintenanceBean = processExpiryMaintenanceBean;
	}

}
