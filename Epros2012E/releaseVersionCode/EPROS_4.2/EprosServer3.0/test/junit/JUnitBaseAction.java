package junit;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsSpringTestCase;
import org.springframework.test.context.support.GenericXmlContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.jecn.epros.server.action.StaredAfterListener;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionProxy;

/**
 * 
 * JUnit测试action时使用的基类
 * 
 * http请求方式默认为post
 * 
 * 
 */
public class JUnitBaseAction extends StrutsSpringTestCase {
	// * 来源： http://blog.csdn.net/woshiyjk/article/details/7822465
	// *
	// * 继承：StrutsSpringTestCase后junit的注解@Before和@after不起作用了
	// *
	// * 采用了StrutsTestCase 的setUp()和tearDown()方法代替
	// *
	// * BeforeClass和AfterClass注解也不能用（必须是静态的）
	// *
	// * 测试普通的Action(有返回值的action)
	// *
	// * ActionProxy proxy = getActionProxy("/Login.action");
	// *
	// * LoginActionaction =(LoginAction)proxy.getAction();
	// *
	// * String s = action.execute();
	// *
	// * assertEquals("success", s);
	// *
	// * 测试ajax的action
	// *
	// * String s = executeAction("/Login.action");
	// * //测试ajax的类，s为response里的输出数据，若没有返回值为空字符串
	// *
	// * assertEquals("{success:true}", s);
	// *
	// * 如果response里没有返回值：assertEquals(“”,s);

	protected final Log log = LogFactory.getLog(JUnitBaseAction.class);

	/** HTTP get请求方式 */
	protected final String HTTP_GET = "get";
	/** HTTP post请求方式 */
	protected final String HTTP_POST = "post";

	/** 用户信息 */
	protected WebLoginBean webLoginBean = null;

	/** 用户IPersonService **/
	protected static IPersonService personService;

	/**
	 * 由于spring 配置文件有多个 故重写 默认加载的是类路径下的applicationContext.xml 文件
	 */
	@Override
	protected void setupBeforeInitDispatcher() throws Exception {
		if (applicationContext == null) {
//			StaredAfterListener.servletContext = this.servletContext;
			String[] contextLocations = new String[] {
					"classpath:applicationContext.xml",
					"classpath:/sshconfig/spring/*.xml" };
			GenericXmlContextLoader xmlContextLoader = new GenericXmlContextLoader();
			applicationContext = xmlContextLoader.loadContext(contextLocations);
		}
		if (personService == null) {
			personService = (IPersonService) applicationContext
					.getBean("PersonServiceImpl");
		}
		this.servletContext.setAttribute(
				WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE,
				applicationContext);
	}

	/**
	 * 
	 * 执行方法之前初始化 ，类似与@before
	 * 
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		request.setMethod(HTTP_POST);
	}

	/**
	 * 
	 * 执行对应的方法，返回类型（对应struts配置文件中<result>节点名称，如果没有对应报错），
	 * 
	 * 如果需要response对象内容，直接执行：response.getContentAsString()
	 * 
	 * @param requestUrl
	 *            String 从根目录开始，格式："/login.action"
	 * 
	 * @return String struts配置文件中<result>节点名称
	 * 
	 * @throws Exception
	 */
	public String executeActionProxy(String requestUrl) throws Exception {
		ActionProxy actionProxy = getActionProxy(requestUrl);
		// session用户信息存储map
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (webLoginBean != null) {
			map.put("webLoginBean", webLoginBean);
		}
		actionProxy.getInvocation().getInvocationContext().setSession(map);

		return actionProxy.execute();
	}

	/**
	 * 
	 * 设置http请求方式为get
	 * 
	 */
	protected void setHttpGet() {
		request.setMethod(HTTP_GET);
	}

	/**
	 * 
	 * 设置http请求方式为post
	 * 
	 */
	protected void setHttpPost() {
		request.setMethod(HTTP_POST);
	}

	/**
	 * 
	 * 获取用户信息对象，存放在session中
	 * 
	 * @return
	 */
	public WebLoginBean getWebLoginBean() {
		return webLoginBean;
	}

	public void setWebLoginBean(WebLoginBean webLoginBean) {
		this.webLoginBean = webLoginBean;
	}

	/**
	 * 
	 * 创建用户信息例子
	 * 
	 * @return WebLoginBean
	 */
	protected WebLoginBean createWebLoginBean() {
		WebLoginBean webLoginBean = new WebLoginBean();
		webLoginBean.setAdmin(true);
		webLoginBean.setViewAdmin(true);
		webLoginBean.setDownLoadAdmin(true);
		JecnUser jecnUser = new JecnUser();
		jecnUser.setLoginName("admin");
		jecnUser.setPassword("a");
		jecnUser.setPeopleId(1L);
		webLoginBean.setJecnUser(jecnUser);
		return webLoginBean;
	}

	/**
	 * 根据用户名密码 获取 WebLoginBean
	 * 
	 * @param loginName
	 * @param password
	 * @param pwdType
	 * @return
	 */
	protected WebLoginBean createWebLoginBean(String loginName, String password) {
		try {
			WebLoginBean webLoginBean = personService.getWebLoginBean(
					loginName, password, true);
			// 登陆状态 0为用户密码不正确，1是登陆成功
			if (webLoginBean.getLoginState() != 0) {
				return webLoginBean;
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error("用户登录异常", e);
		}
		return null;
	}

}
