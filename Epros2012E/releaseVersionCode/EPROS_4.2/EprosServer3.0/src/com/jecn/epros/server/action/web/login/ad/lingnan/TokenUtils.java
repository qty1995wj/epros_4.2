package com.jecn.epros.server.action.web.login.ad.lingnan;

import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;

public class TokenUtils {
	private static final String LANG = "utf-8";
	private static char[] HEXCHAR = { '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	public static String genLtpaToken(String user, String key, long expireHour)
			throws Exception {
		byte[] hbts = { 0, 1, 2, 3 };
		String createTime = Long
				.toHexString(System.currentTimeMillis() / 1000L).toUpperCase();
		byte[] cbts = createTime.getBytes("utf-8");
		String endTime = Long.toHexString(
				System.currentTimeMillis() / 1000L + expireHour * 3600L)
				.toUpperCase();
		byte[] ebts = endTime.getBytes("utf-8");
		byte[] ubts = user.getBytes("utf-8");
		byte[] sourceBts = new byte[20 + ubts.length];
		byte[] kbts = Base64.decodeBase64(key.getBytes("utf-8"));
		System.out.println(kbts.length);
		if (kbts.length != 20) {
			throw new Exception("Invalid key! key should be 20 bytes");
		}
		System.arraycopy(hbts, 0, sourceBts, 0, 4);
		System.arraycopy(cbts, 0, sourceBts, 4, 8);
		System.arraycopy(ebts, 0, sourceBts, 12, 8);
		System.arraycopy(ubts, 0, sourceBts, 20, ubts.length);
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(sourceBts);
		md.update(kbts);
		byte[] resultBts = new byte[sourceBts.length + 20];
		System.arraycopy(sourceBts, 0, resultBts, 0, sourceBts.length);
		System.arraycopy(md.digest(), 0, resultBts, sourceBts.length, 20);
		String token = new String(Base64.encodeBase64(resultBts));
		return token;
	}

	public static String genLtpaToken(String user, String key, long expire,
			TimeUnit timeUnit) throws Exception {
		byte[] hbts = { 0, 1, 2, 3 };
		String createTime = Long
				.toHexString(System.currentTimeMillis() / 1000L).toUpperCase();
		byte[] cbts = createTime.getBytes("utf-8");
		String endTime = "";
		if (timeUnit == TimeUnit.HOURS)
			endTime = Long.toHexString(
					System.currentTimeMillis() / 1000L + expire * 3600L)
					.toUpperCase();
		else if (timeUnit == TimeUnit.MINUTES)
			endTime = Long.toHexString(
					System.currentTimeMillis() / 1000L + expire * 60L)
					.toUpperCase();
		else if (timeUnit == TimeUnit.SECONDS) {
			endTime = Long.toHexString(
					System.currentTimeMillis() / 1000L + expire).toUpperCase();
		}
		byte[] ebts = endTime.getBytes("utf-8");
		byte[] ubts = user.getBytes("utf-8");
		byte[] sourceBts = new byte[20 + ubts.length];
		byte[] kbts = Base64.decodeBase64(key.getBytes("utf-8"));
		System.arraycopy(hbts, 0, sourceBts, 0, 4);
		System.arraycopy(cbts, 0, sourceBts, 4, 8);
		System.arraycopy(ebts, 0, sourceBts, 12, 8);
		System.arraycopy(ubts, 0, sourceBts, 20, ubts.length);
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(sourceBts);
		md.update(kbts);
		byte[] resultBts = new byte[sourceBts.length + 20];
		System.arraycopy(sourceBts, 0, resultBts, 0, sourceBts.length);
		System.arraycopy(md.digest(), 0, resultBts, sourceBts.length, 20);
		String token = new String(Base64.encodeBase64(resultBts));
		return token;
	}

	public static String parseLtpaToken(String tokenStr, String key)
			throws Exception {
		tokenStr = processTokenStr(tokenStr);
		byte[] token = Base64.decodeBase64(tokenStr.getBytes());
		byte[] curDigest = new byte[20];
		System.arraycopy(token, token.length - 20, curDigest, 0, 20);
		byte[] source = new byte[token.length];
		System.arraycopy(token, 0, source, 0, source.length - 20);
		byte[] keyBts = Base64.decodeBase64(key.getBytes());
		if (keyBts.length != 20) {
			throw new Exception("Invalid key! key should be 20 bytes");
		}
		System.arraycopy(keyBts, 0, source, source.length - 20, 20);

		byte[] userBts = new byte[token.length - 40];
		System.arraycopy(token, 20, userBts, 0, userBts.length);
		String userName = new String(userBts, "utf-8");

		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] dig = md.digest(source);
		if (!MessageDigest.isEqual(curDigest, dig)) {
			throw new Exception("У������ʧ�ܣ���Կ����ȷ���߱��۸Ĺ�,token="
					+ tokenStr + ",user=" + userName);
		}
		byte[] endBts = new byte[8];
		System.arraycopy(token, 12, endBts, 0, 8);
		long endTime1 = Long.parseLong(new String(endBts), 16);

		long endTime2 = System.currentTimeMillis() / 1000L;
		if (endTime1 < endTime2) {
			throw new Exception("�����Ѿ�����,token=" + tokenStr + ",user="
					+ userName + ",endTime=" + new Date(endTime1 * 1000L));
		}
		return userName;
	}

	public static String genLRToken(String user, String pubKey, String priKey,
			long expireHour) throws Exception {
		return genLRToken(user, pubKey, expireHour);
	}

	public static String genLRToken(String user, String pubKey, long expireHour)
			throws Exception {
		user = user.trim();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		long time = System.currentTimeMillis();
		String tokenInfo = dateFormat.format(new Date(time));
		tokenInfo = tokenInfo + user;
		time += expireHour * 3600L * 1000L;
		tokenInfo = tokenInfo + dateFormat.format(new Date(time));
		tokenInfo = tokenInfo
				+ UUID.randomUUID().toString().replaceAll("-", "");
		byte[] cipherText = tokenInfo.getBytes("utf-8");
		return byte2HexString(encrypt(cipherText, pubKey));
	}

	public static String genLRToken(String user, String pubKey, long expire,
			TimeUnit timeUnit) throws Exception {
		user = user.trim();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		long time = System.currentTimeMillis();
		String tokenInfo = dateFormat.format(new Date(time));
		tokenInfo = tokenInfo + user;
		if (timeUnit == TimeUnit.HOURS)
			time += expire * 3600L * 1000L;
		else if (timeUnit == TimeUnit.MINUTES)
			time += expire * 60L * 1000L;
		else if (timeUnit == TimeUnit.SECONDS) {
			time += expire * 1000L;
		}
		tokenInfo = tokenInfo + dateFormat.format(new Date(time));
		tokenInfo = tokenInfo
				+ UUID.randomUUID().toString().replaceAll("-", "");
		byte[] cipherText = tokenInfo.getBytes("utf-8");
		return byte2HexString(encrypt(cipherText, pubKey));
	}

	public static String parseLRToken(String token, String pubKey, String priKey)
			throws Exception {
		return parseLRToken(token, priKey);
	}

	public static String parseLRToken(String token, String priKey)
			throws Exception {
		token = processTokenStr(token);
		byte[] cipherText = hexString2Bytes(token);
		String tokenInfo = new String(decrypt(cipherText, priKey), "utf-8");
		tokenInfo = tokenInfo.substring(0, tokenInfo.length() - 32);
		String username = tokenInfo.substring(14, tokenInfo.length() - 14);
		String expireStr = tokenInfo.substring(tokenInfo.length() - 14);

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		long time = System.currentTimeMillis();
		Date expireTime = dateFormat.parse(expireStr);
		if (expireTime.getTime() < time) {
			throw new Exception("LRToken����,token=" + token + ",user="
					+ username);
		}
		return username;
	}

	private static byte[] encrypt(byte[] input, String pubKey) throws Exception {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		KeySpec keySpec = new X509EncodedKeySpec(hexString2Bytes(pubKey));
		Key publicKey = keyFactory.generatePublic(keySpec);
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(1, publicKey);
		return cipher.doFinal(input);
	}

	private static byte[] decrypt(byte[] input, String priKey) throws Exception {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		KeySpec keySpec = new PKCS8EncodedKeySpec(hexString2Bytes(priKey));
		Key privateKey = keyFactory.generatePrivate(keySpec);
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(2, privateKey);
		return cipher.doFinal(input);
	}

	private static final byte[] hexString2Bytes(String s) {
		byte[] bytes = new byte[s.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = ((byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2),
					16));
		}
		return bytes;
	}

	private static String byte2HexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			sb.append(HEXCHAR[((b[i] & 0xF0) >>> 4)]);
			sb.append(HEXCHAR[(b[i] & 0xF)]);
		}
		return sb.toString();
	}

	private static String processTokenStr(String tokenStr) {
		int temp = tokenStr.length() % 4;
		switch (temp) {
		case 3:
			return tokenStr + "=";
		case 2:
			return tokenStr + "==";
		case 1:
			return tokenStr + "===";
		}
		return tokenStr;
	}
	
	public static void main(String[] args) {
		String privateKey = "30820278020100300d06092a864886f70d0101010500048202623082025e02010002818100bdeffceb7240cc0688b62f38e81388c4ff90391bfa489421c2e3635d501bc4edfd6ed4329f6ee660390af24cb0399165f3c6b67ddb92ce09946dc75bfe088bf72c7206bc3b323359c645a0329678182d5672fcf3052686c76e014f894bf79bd8d1a99a55c8f5fae5ee8db88a5c2ebec4cdf7262537da8f5ef815831c75d7e803020301000102818027a95661ffe0a1f9f7503bba2153faea500758b58be4ddf1d2d552a96891f3e45258733d02e7d079654ff0daacadc8bbc654c744aeb6100a16117a60ae42309f975ef5cc3c749a317f36b20d7dcd0685427267b229bdbfdf65524f165b50f86223ec90ecb1e49bd8afef8cab7ce4431648dd4ac245449d9a6709f23701fb9e71024100e5fe646d4340c9e9604d9261a6eb0380e6f4b619153e854d6cdc52bc26f93bdd110d2ab7f47c4cd977e521da42251a31dd2b27e4174109970bb968dd5565dcb9024100d36a172a3e221c9bf0900a4bba8babf7a2ffb4847ef04cce2e3ba2315929081900f7a231bf201949c4e764e3e3cb0d0a4a2a8fe98adfc4b0e2287c79d30b649b02410086dc95cccffbc4a9d933835b4982532a99c2f2a860a1a98ecdff9f6ffeb67498db6fca15d601b11a6fc867025178ea144529121a185ca59ff15b06178b04d0d9024100aa0ae2a6ef62b0701c853e42bac02c88f96981e02fd9476d6e72387a48a46006fc6830e4a23d4719b66e070fe4e02494ea2261ae0e2cfd1ee8487815e4517d910241009500a070a66871e86b2c9759f435ca4239aab910adcda4d9c5ed2e5fd2483769f918fd16abe1cb13c45049133d428f00bf5bb1fe20eebb7690c8bbf8f295eb9e";
//	    String publicKey="30820278020100300d063030820";
	    String token="AAECAzVDOTA5OTc0NUM5MERGQzR1c2VyMRoc5u9DH1WE1IdRBC/crwrQ94YX";
		try {
			 token=genLtpaToken("user1",privateKey,5);
//			parseLRToken(token,null,privateKey);
			System.out.println(token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}