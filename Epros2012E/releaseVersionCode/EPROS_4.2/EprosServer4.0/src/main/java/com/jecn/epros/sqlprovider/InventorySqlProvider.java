package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.EnumClass.TreeNodeType;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InventorySqlProvider extends BaseSqlProvider {

    /**
     * 获得流程架构下所有流程架构和流程节点的相关制度
     *
     * @param map
     * @return
     */
    public String findFlowMapAndFlowRelatedRuleSql(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "SELECT JFS.FLOW_ID AS ID,R.ID AS FILE_ID,R.RULE_NAME AS FILE_NAME,2 AS FILE_TYPE"
                + "  FROM FLOW_RELATED_CRITERION_T RS,JECN_RULE_T R" + "  ,("
                + getFlows(map, 2) + ") JFS"
                + "  WHERE JFS.FLOW_ID=RS.FLOW_ID AND RS.CRITERION_CLASS_ID = R.ID" + "  AND JFS.DEL_STATE=0"
                + "   ORDER BY JFS.FLOW_ID";
    }


    /**
     * 获得制度相关流程架构
     *
     * @param map
     * @return
     */
    public String findRuleChildFlowMaps(Map<String, Object> map) {
        Long ruleId = Long.valueOf(map.get("ruleId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT RULES.ID,JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,0 AS FILE_TYPE"
                + "             FROM (" + SqlCommon.getRuleChildObjectsSqlCommon(ruleId, projectId) + ") RULES"
                + "              ,FLOW_RELATED_CRITERION FRC" + "              ,JECN_FLOW_STRUCTURE JFS"
                + "             WHERE RULES.ID =FRC.CRITERION_CLASS_ID  AND FRC.FLOW_ID = JFS.FLOW_ID"
                + "             AND JFS.ISFLOW=0 AND JFS.DEL_STATE=0 AND RULES.IS_DIR<>0"
                + "             ORDER BY RULES.ID";
        return sql;
    }

    /**
     * 获得制度目录下的相关流程
     *
     * @param map
     * @return
     */
    public String findRuleChildFlows(Map<String, Object> map) {
        String sql = "SELECT RULES.ID,JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,1 AS FILE_TYPE"
                + "             FROM (" + getRules(map) + ") RULES"
                + "              ,FLOW_RELATED_CRITERION FRC" + "              ,JECN_FLOW_STRUCTURE JFS"
                + "             WHERE RULES.ID =FRC.CRITERION_CLASS_ID  AND FRC.FLOW_ID = JFS.FLOW_ID"
                + "             AND JFS.ISFLOW=1 AND JFS.DEL_STATE=0 AND RULES.IS_DIR<>0"
                + "             ORDER BY RULES.ID";
        return sql;
    }

    /**
     * 获得制度目录下的相关制度
     *
     * @param map
     * @return
     */
    public String findRuleChildRules(Map<String, Object> map) {
        String sql = "SELECT RULES.ID,JFS.ID AS FILE_ID,JFS.RULE_NAME AS FILE_NAME,2 AS FILE_TYPE"
                + "             FROM (" + getRules(map) + ") RULES"
                + "              ,JECN_RULE_RELATED_RULE_T FRC" + "              ,JECN_RULE_T JFS"
                + "             WHERE RULES.ID = FRC.RULE_ID  AND FRC.RELATED_ID = JFS.ID"
                + "             AND JFS.DEL_STATE = 0" +
                "               AND RULES.IS_DIR <> 0" +
                " ORDER BY RULES.ID";
        return sql;
    }

    /**
     * 制度相关标准
     *
     * @param map
     * @return
     */
    public String findRuleRelatedStandard(Map<String, Object> map) {
        return "SELECT RULES.ID, JCC.CRITERION_CLASS_ID AS FILE_ID, JCC.CRITERION_CLASS_NAME AS FILE_NAME,3 AS FILE_TYPE"
                + "    FROM (" + getRules(map) + ") RULES,"
                + "              JECN_RULE_STANDARD JRS,JECN_CRITERION_CLASSES JCC"
                + "             WHERE RULES.ID = JRS.RULE_ID AND  JRS.STANDARD_ID =JCC.CRITERION_CLASS_ID"
                + "              AND RULES.IS_DIR<>0 ORDER BY RULES.ID";
    }

    /**
     * 制度相关风险
     *
     * @param map
     * @return
     */
    public String findRuleRelatedRisk(Map<String, Object> map) {

        return "SELECT RULES.ID,JR.ID AS FILE_ID,JR.NAME AS FILE_NAME,4 AS FILE_TYPE" + "   FROM ("
                + getRules(map)
                + ") RULES ,JECN_RISK  JR,JECN_RULE_RISK  JRR"
                + "   WHERE RULES.ID =JRR.RULE_ID AND JRR.RISK_ID = JR.ID" + "   AND RULES.IS_DIR<>0 ORDER BY RULES.ID";
    }

    /**
     * 部门清单通用
     *
     * @return
     */
    private String getOrgInventoryCommon(Map<String, Object> param) {
        return "SELECT DISTINCT JFST.FLOW_ID,JFST.ISFLOW,JFST.DEL_STATE" + "             FROM ("
                + getOrgs(param) + ") MO"
                + "             INNER JOIN JECN_FLOW_RELATED_ORG_T JFROT ON MO.ORG_ID = JFROT.ORG_ID"
                + "             INNER JOIN JECN_FLOW_STRUCTURE_T JFST ON JFROT.FLOW_ID=JFST.FLOW_ID AND JFST.DEL_STATE=0";
    }

    /**
     * 获得责任部门包括子部门下所有流程节点的相关流程
     *
     * @param map
     * @return
     */
    public String findOrgRelatedProcess(Map<String, Object> map) {
        Long orgId = Long.valueOf(map.get("orgId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "SELECT JFS.FLOW_ID AS ID,T.FLOW_ID AS FILE_ID,T.FLOW_NAME AS FILE_NAME,1 AS FILE_TYPE" + "  FROM ("
                + getOrgInventoryCommon(map) + ") JFS," + "  JECN_FLOW_STRUCTURE_IMAGE_T  JFSI"
                + "  LEFT JOIN JECN_FLOW_STRUCTURE_T T ON JFSI.LINK_FLOW_ID = T.FLOW_ID AND T.DEL_STATE = 0 AND T.PROJECTID="
                + projectId + "  WHERE  JFS.FLOW_ID = JFSI.FLOW_ID"
                + "  AND JFSI.IMPL_TYPE IN ('1', '2', '3', '4')  AND JFSI.FIGURE_TYPE in" + SqlCommon.getImplString()
                + "  ORDER BY JFS.FLOW_ID";
    }

    /**
     * 获得责任部门包括子部门下所有流程节点的相关制度
     *
     * @param map
     * @return
     */
    public String findOrgRelatedRule(Map<String, Object> map) {
        Long orgId = Long.valueOf(map.get("orgId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "SELECT JFS.FLOW_ID AS ID,R.ID AS FILE_ID,R.RULE_NAME AS FILE_NAME,2 AS FILE_TYPE"
                + "  FROM FLOW_RELATED_CRITERION_T RS,JECN_RULE_T R" + "  ,(" + getOrgInventoryCommon(map)
                + ") JFS" + "  WHERE JFS.FLOW_ID=RS.FLOW_ID AND RS.CRITERION_CLASS_ID = R.ID"
                + "  AND JFS.ISFLOW=1 AND JFS.DEL_STATE=0" + "   ORDER BY JFS.FLOW_ID";
    }

    /**
     * 获得责任部门包括子部门下所有流程节点的相关标准
     *
     * @param map
     * @return
     */
    public String findOrgRelatedStandard(Map<String, Object> map) {
        Long orgId = Long.valueOf(map.get("orgId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "SELECT *  FROM (SELECT JFS.FLOW_ID AS ID,C.CRITERION_CLASS_ID AS FILE_ID, C.CRITERION_CLASS_NAME AS FILE_NAME, 3 AS FILE_TYPE"
                + "  FROM JECN_FLOW_STANDARD_T FRC, JECN_CRITERION_CLASSES C" + "  ,("
                + getOrgInventoryCommon(map) + ") JFS"
                + " WHERE JFS.FLOW_ID =FRC.FLOW_ID AND JFS.ISFLOW=1 AND JFS.DEL_STATE=0"
                + "   AND FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID" + "   AND C.STAN_TYPE <> 0" + "UNION"
                + "  SELECT JFS.FLOW_ID AS ID,C.CRITERION_CLASS_ID AS FILE_ID, C.CRITERION_CLASS_NAME AS FILE_NAME, 3 AS FILE_TYPE"
                + "  FROM (" + getOrgInventoryCommon(map) + ") JFS," + "       JECN_ACTIVE_STANDARD_T JAS,"
                + "       JECN_FLOW_STRUCTURE_IMAGE_T SI," + "       JECN_CRITERION_CLASSES C"
                + " WHERE JFS.FLOW_ID =SI.FLOW_ID AND JFS.ISFLOW=1 AND JFS.DEL_STATE=0"
                + "   AND SI.FIGURE_ID = JAS.ACTIVE_ID" + "   AND SI.FIGURE_TYPE IN" + SqlCommon.getActiveString()
                + "   AND JAS.STANDARD_ID = C.CRITERION_CLASS_ID" + "    AND C.STAN_TYPE <> 0)RESULT"
                + "    ORDER BY RESULT.ID";
    }

    /**
     * 获得责任部门包括子部门下所有流程节点的相关风险
     *
     * @param map
     * @return
     */
    public String findOrgRelatedRisk(Map<String, Object> map) {
        Long orgId = Long.valueOf(map.get("orgId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "SELECT distinct JFS.FLOW_ID AS ID,JR.ID AS FILE_ID,JR.NAME AS FILE_NAME,4 AS FILE_TYPE" + "  FROM ("
                + getOrgInventoryCommon(map) + ") JFS, JECN_RISK JR"
                + " INNER JOIN JECN_CONTROL_POINT JCPT ON JR.ID = JCPT.RISK_ID"
                + " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_T JFSIT ON JCPT.ACTIVE_ID = JFSIT.FIGURE_ID"
                + " WHERE JFS.FLOW_ID = JFSIT.FLOW_ID AND JFS.DEL_STATE=0 AND JFS.ISFLOW=1 AND JFSIT.FIGURE_TYPE IN"
                + SqlCommon.getActiveString() + " ORDER BY JFS.FLOW_ID";
    }

    /**
     * 获得责任部门包括子部门下所有流程节点的关键活动
     *
     * @param map
     * @return
     */
    public String findOrgRelatedActivity(Map<String, Object> map) {
        Long orgId = Long.valueOf(map.get("orgId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "SELECT JFS.FLOW_ID AS ID, JFSI.FIGURE_ID AS FILE_ID,JFSI.FIGURE_TEXT AS FILE_NAME,5 AS FILE_TYPE  FROM ("
                + getOrgInventoryCommon(map) + ") JFS" + "        ,JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "        WHERE JFS.FLOW_ID=JFSI.FLOW_ID AND JFSI.FIGURE_TYPE IN " + SqlCommon.getActiveString()
                + "        AND JFSI.LINECOLOR IN ('1','2','3','4')" + "        ORDER BY JFS.FLOW_ID";
    }

    /**
     * 获得责任部门包括子部门下所有流程节点的KPI
     *
     * @param map
     * @return
     */
    public String findOrgRelatedKPI(Map<String, Object> map) {
        Long orgId = Long.valueOf(map.get("orgId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "SELECT JFS.FLOW_ID AS ID, KPI.KPI_AND_ID AS FILE_ID,KPI.KPI_NAME AS FILE_NAME,6 AS FILE_TYPE"
                + "        FROM (" + getOrgInventoryCommon(map) + ") JFS"
                + "         ,JECN_FLOW_KPI_NAME_T KPI" + "         WHERE JFS.FLOW_ID=KPI.FLOW_ID"
                + "         ORDER BY JFS.FLOW_ID";
    }

    /**
     * 部门下的所有部门和岗位
     *
     * @param map
     * @return
     */
    public String findPosOrgs(Map<String, Object> map) {
        return "SELECT JFO.ORG_ID,JFO.ORG_NAME,JFO.PER_ORG_ID,JFO.T_LEVEL,JFO.T_PATH,JFOI.FIGURE_ID,JFOI.FIGURE_TEXT"
                + "       FROM JECN_FLOW_ORG JFO INNER JOIN ("
                + getOrgs(map)
                + ") org on org.org_id = JFO.org_id "
                + "       LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFOI.FIGURE_TYPE = " + SqlCommon.getPosString()
                + "       AND JFO.ORG_ID=JFOI.ORG_ID"
                + " WHERE  JFO.DEL_STATE=0"
                + "       ORDER BY JFO.VIEW_SORT,JFOI.SORT_ID ASC";
    }


    public String findPosRoleAndActive(Map<String, Object> map) {
        Long orgId = Long.valueOf(map.get("orgId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(orgId);
        String sql = "SELECT JFOI.FIGURE_ID" +
                "       FROM JECN_FLOW_ORG JFO INNER JOIN ("
                + SqlCommon.getChildObjectsSqlByType(listIds, TreeNodeType.organization)
                + ") org on org.org_id = JFO.org_id " +
                "       INNER JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFOI.FIGURE_TYPE='PositionFigure'" +
                "       AND JFO.ORG_ID=JFOI.ORG_ID" +
                "       WHERE JFO.PROJECTID=" + projectId + " AND JFO.DEL_STATE=0";
        return
                "SELECT  POS_RESULT.* FROM (" +
                        "     SELECT POS.FIGURE_ID, PROCESS_ROLE.FIGURE_ID      AS ROLE_ID," +
                        "               PROCESS_ROLE.FIGURE_TEXT AS ROLE_NAME," +
                        "               PROCESS_ROLE.ROLE_RES," +
                        "               JFS.FLOW_ID," +
                        "               JFS.FLOW_NAME," +
                        "               ACTIVE.FIGURE_ID    AS FILE_ID," +
                        "               ACTIVE.FIGURE_TEXT  AS FILE_NAME," +
                        "                ACTIVE.ACTIVITY_ID," +
                        "               5  AS FILE_TYPE," +
                        "               ACTIVE.TARGET_VALUE," +
                        "               ACTIVE.STATUS_VALUE" +
                        "          FROM (" + sql + ") POS" +
                        "           INNER JOIN PROCESS_STATION_RELATED POS_ROLE" +
                        "            ON POS.FIGURE_ID = POS_ROLE.FIGURE_POSITION_ID AND POS_ROLE.TYPE='0'" +
                        "           INNER JOIN JECN_FLOW_STRUCTURE_IMAGE PROCESS_ROLE ON POS_ROLE.FIGURE_FLOW_ID = PROCESS_ROLE.FIGURE_ID" +
                        "           INNER JOIN JECN_FLOW_STRUCTURE JFS ON PROCESS_ROLE.FLOW_ID=JFS.FLOW_ID AND JFS.DEL_STATE=0" +
                        "           LEFT JOIN JECN_ROLE_ACTIVE JRA ON PROCESS_ROLE.FIGURE_ID=JRA.FIGURE_ROLE_ID" +
                        "           LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE ACTIVE  ON ACTIVE.FIGURE_ID = JRA.FIGURE_ACTIVE_ID" +
                        "         UNION" +
                        "         SELECT POS.FIGURE_ID, PROCESS_ROLE.FIGURE_ID      AS ROLE_ID," +
                        "               PROCESS_ROLE.FIGURE_TEXT    AS ROLE_NAME," +
                        "               PROCESS_ROLE.ROLE_RES," +
                        "               JFS.FLOW_ID," +
                        "               JFS.FLOW_NAME," +
                        "               ACTIVE.FIGURE_ID    AS FILE_ID," +
                        "               ACTIVE.FIGURE_TEXT  AS FILE_NAME," +
                        "               ACTIVE.ACTIVITY_ID," +
                        "               5  AS FILE_TYPE," +
                        "               ACTIVE.TARGET_VALUE," +
                        "               ACTIVE.STATUS_VALUE" +
                        "           FROM (" + sql + ") POS" +
                        "           INNER JOIN JECN_POSITION_GROUP_R JPGR ON POS.FIGURE_ID = JPGR.FIGURE_ID" +
                        "           INNER JOIN PROCESS_STATION_RELATED POS_ROLE" +
                        "            ON JPGR.GROUP_ID = POS_ROLE.FIGURE_POSITION_ID AND POS_ROLE.TYPE='1'" +
                        "           INNER JOIN JECN_FLOW_STRUCTURE_IMAGE PROCESS_ROLE ON POS_ROLE.FIGURE_FLOW_ID = PROCESS_ROLE.FIGURE_ID" +
                        "           INNER JOIN JECN_FLOW_STRUCTURE JFS ON PROCESS_ROLE.FLOW_ID=JFS.FLOW_ID AND JFS.DEL_STATE=0" +
                        "           LEFT JOIN JECN_ROLE_ACTIVE JRA ON PROCESS_ROLE.FIGURE_ID=JRA.FIGURE_ROLE_ID" +
                        "           LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE ACTIVE" +
                        "         ON ACTIVE.FIGURE_ID = JRA.FIGURE_ACTIVE_ID) POS_RESULT ORDER BY POS_RESULT.FIGURE_ID,POS_RESULT.ROLE_ID,POS_RESULT.ACTIVITY_ID";
    }


    public String findPosUser() {
        String sql = "SELECT  PT.FIGURE_ID,U.* " +
                "  FROM JECN_USER_POSITION_RELATED PT " +
                " INNER JOIN JECN_USER U " +
                "    ON PT.PEOPLE_ID = U.PEOPLE_ID " +
                "   AND U.ISLOCK = 0 " ;
        return sql;
    }


    /**
     * 风险关联的制度
     *
     * @param map
     * @return
     */
    public String fetchRiskRelatedRule(Map<String, Object> map) {
        String sql = getRisk(map);
        sql += "select jrr.RISK_ID," +
                "	jru.id file_id," +
                "	jru.rule_name file_name," +
                "	2 as file_type" +
                " from " +
                "	RISK jr" +
                "	INNER JOIN JECN_RULE_RISK jrr ON jrr.RISK_ID = jr.id" +
                "	INNER JOIN jecn_rule jru ON jrr.rule_id = jru.id and jru.del_state = 0";

        return sql;
    }

    /**
     * 风险关联
     *
     * @param map
     * @return
     */
    public String fetchRiskRelatedInfo(Map<String, Object> map) {
        String sql = getRisk(map);
        sql += "	select jrg.RISK_ID," +
                "	jcg.id as infoId," +
                "  jcg.name," +
                "  jcg.DESCRIPTION" +
                "	 from JECN_RISK_GUIDE jrg" +
                "  inner join RISK jr on jrg.RISK_ID=jr.id" +
                "  inner join JECN_CONTROL_GUIDE jcg on jcg.ID=jrg.GUIDE_ID";

        return sql;
    }

    private String getRisk(Map<String, Object> map) {

        Long id = (Long) map.get("id");
        Long projectId = (Long) map.get("projectId");
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(id);

        return "with RISK AS (" +
                getRisks(map)
                + " )";
    }

    public String getInventoryBaseInfo(Map<String, Object> map) {
        if (map.containsKey("downloadType") && "2".equals(map.get("downloadType").toString())) {
            map.put("containsFlow", true);
            return getProcessInventoryInfo(map);
        } else {
            String type = map.get("type").toString();
            switch (type) {
                case "processMap":
                    return getProcessInventoryInfo(map);
                case "rule":
                    return getRuleInventoryInfo(map);
                case "file":
                    return getFileInventoryInfo(map);
                case "standard":
                    return getStandardInventoryInfo(map);
                case "risk":
                    return getRiskInventoryInfo(map);
                case "organization":
                    return getOrgInventoryInfo(map);
                case "position":
                    return getPosInventoryInfo(map);
            }
        }
        return null;
    }

    /**
     * 选中的部门下所有的部门下的相关流程的信息
     *
     * @param map
     * @return
     */
    public String getInventoryOrgFlowsBaseInfo(Map<String, Object> map) {
        String sql = "	WITH datas AS (" +
                getOrgInventoryCommon(map) + " where JFST.DEL_STATE=0" +
                "	)," +
                "	 nums AS (" +
                "		SELECT" +
                "			SUM (n.total) AS total," +
                "			SUM (n.pub) AS pub," +
                "			SUM (n.not_pub) AS not_pub," +
                "           SUM (n.approve) as approve" +
                "		FROM" +
                "			(" +
                "				SELECT" +
                "					CASE" +
                "               when jtbn.ID is not null then 0" +
                "				WHEN f2.flow_id IS NULL THEN" +
                "					1" +
                "				ELSE" +
                "					0" +
                "				END AS not_pub," +
                "				CASE" +
                "			WHEN f2.flow_id IS NOT NULL THEN" +
                "				1" +
                "			ELSE" +
                "				0" +
                "			END AS pub," +
                "			1 AS total," +
                "  case when f2.flow_id is null and jtbn.ID is not null then 1 else 0 end as approve" +
                "		FROM" +
                "			datas f1" +
                "		LEFT JOIN JECN_FLOW_STRUCTURE f2 ON f1.flow_id = f2.flow_id" +
                " left join JECN_TASK_BEAN_NEW jtbn on jtbn.IS_LOCK=1 and jtbn.STATE<>5 and jtbn.R_ID=f1.flow_id and jtbn.TASK_TYPE in (0,4)" +
                "			) n" +
                "	)" +
                " SELECT" +
                "		n.* " +
                "	FROM" +
                "		nums n";
        return sql;
    }

    private String getPosInventoryInfo(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long orgId = Long.valueOf(map.get("orgId").toString());
        return "WITH datas AS" +
                " (" +
                "select org.t_level,pos.* from (" +
                SqlCommon.getOrgChildObjectsSqlCommon(orgId, projectId) +
                ") org" +
                " left join JECN_FLOW_ORG_IMAGE pos on org.org_id=pos.org_id and figure_type='PositionFigure'" +
                ")," +
                "nums AS" +
                " (SELECT SUM(n.total) AS total" +
                "    FROM (SELECT " +
                " case when f1.figure_id is null then 0 else 1 end as total" +
                "            FROM datas f1" +
                "          ) n)," +
                "max_level AS" +
                " (SELECT MAX(f.t_level) AS max_level FROM datas f)" +
                "SELECT n.*, (SELECT max_level FROM max_level) AS max_level FROM nums n";
    }

    private String getOrgInventoryInfo(Map<String, Object> map) {

        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long orgId = Long.valueOf(map.get("orgId").toString());
        return "WITH datas AS" +
                " (" +
                "" +
                SqlCommon.getOrgChildObjectsSqlCommon(orgId, projectId) +
                "" +
                ")," +
                "nums AS" +
                " (SELECT SUM(n.total) AS total" +
                "    FROM (SELECT " +
                "                 1 AS total" +
                "            FROM datas f1" +
                "          ) n)," +
                "max_level AS" +
                " (SELECT MAX(f.t_level) AS max_level FROM datas f)" +
                "SELECT n.*, (SELECT max_level FROM max_level) AS max_level FROM nums n";

    }

    private String getRiskInventoryInfo(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long id = Long.valueOf(map.get("id").toString());
        return "WITH datas AS" +
                " (" +
                "" +
                SqlCommon.getRiskChildObjectsSqlCommon(id, projectId) +
                "" +
                ")," +
                "nums AS" +
                " (SELECT SUM(n.total) AS total" +
                "    FROM (SELECT " +
                "                 1 AS total" +
                "            FROM datas f1" +
                "           WHERE f1.is_dir=1) n)," +
                "max_level AS" +
                " (SELECT MAX(f.t_level) AS max_level FROM datas f WHERE f.is_dir = 0)" +
                "SELECT n.*, (SELECT max_level FROM max_level) AS max_level FROM nums n";
    }

    /**
     * 查询该目录下的所有的文件或者其它并且最终会按照view_sort排序，分页查询若干条到若干条的数据
     *
     * @param map
     * @return
     */
    public String fetchPaths(Map<String, Object> map) {
        String type = map.get("type").toString();
        String sql = "";
        Long projectId = Long.valueOf(map.get("projectId").toString());
        switch (type) {
            case "processMap":
                Long flowId = Long.valueOf(map.get("flowId").toString());
                sql = "(" + SqlCommon.getFlowChildObjectsSqlCommon(flowId, projectId) + ")a where ISFLOW=1 AND DEL_STATE=0";
                break;
            case "rule":
                Long ruleId = Long.valueOf(map.get("ruleId").toString());
                sql = "(" + SqlCommon.getRuleChildObjectsSqlCommon(ruleId, projectId) + ")a where is_dir<>0";
                break;
            case "file":
                Long fileId = Long.valueOf(map.get("id").toString());
                sql = "(" + SqlCommon.getFileChildObjectsSqlCommon(fileId, projectId) + ")a where del_state=0 AND IS_DIR<>0 and hide=1";
                break;
            case "standard":
                Long standardId = Long.valueOf(map.get("id").toString());
                sql = "(" + SqlCommon.getStandardChildObjectsSqlCommon(standardId, projectId) + ")a where STAN_TYPE<>0";
                break;
            case "risk":
                Long riskId = Long.valueOf(map.get("id").toString());
                sql = "(" + SqlCommon.getRiskChildObjectsSqlCommon(riskId, projectId) + ")a where IS_DIR<>0";
                break;
            case "organization":
            case "position":
                Long orgId = Long.valueOf(map.get("orgId").toString());
                sql = "(" + SqlCommon.getOrgChildObjectsSqlCommon(orgId, projectId) + ")a ";
                break;
        }
        sql = "select t_path from " + sql;
        return sql;
    }

    private String getStandardInventoryInfo(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long id = Long.valueOf(map.get("id").toString());
        return "WITH datas AS" +
                " (" +
                "" +
                SqlCommon.getStandardChildObjectsSqlCommon(id, projectId) +
                "" +
                ")," +
                "nums AS" +
                " (SELECT SUM(n.total) AS total" +
                "    FROM (SELECT " +
                "                 1 AS total" +
                "            FROM datas f1" +
                "           WHERE f1.STAN_TYPE <>0) n)," +
                "max_level AS" +
                " (SELECT MAX(f.t_level) AS max_level FROM datas f WHERE f.STAN_TYPE = 0)" +
                "SELECT n.*, (SELECT max_level FROM max_level) AS max_level FROM nums n";
    }

    private String getFileInventoryInfo(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long id = Long.valueOf(map.get("id").toString());
        return "WITH datas AS" +
                " (" +
                "" +
                SqlCommon.getFileChildObjectsSqlCommon(id, projectId) +
                "" +
                ")," +
                "nums AS" +
                " (SELECT SUM(n.total) AS total," +
                "         SUM(n.pub) AS pub," +
                "         SUM(n.not_pub) AS not_pub," +
                "         SUM (n.approve) as approve" +
                "    FROM (SELECT CASE" +
                "                   when jtbn.ID is not null then 0" +
                "                   WHEN f2.file_id IS NULL THEN" +
                "                    1" +
                "                   ELSE" +
                "                    0" +
                "                 END AS not_pub," +
                "                 CASE" +
                "                   WHEN f2.file_id IS NOT NULL THEN" +
                "                    1" +
                "                   ELSE" +
                "                    0" +
                "                 END AS pub," +
                "                 1 AS total," +
                "  case when f2.file_id is null and jtbn.ID is not null then 1 else 0 end as approve" +
                "            FROM datas f1" +
                "            LEFT JOIN JECN_FILE f2 ON f1.file_id = f2.file_id" +
                " left join JECN_TASK_BEAN_NEW jtbn on jtbn.IS_LOCK=1 and jtbn.STATE<>5 and jtbn.R_ID=f1.file_id and jtbn.TASK_TYPE = 1" +
                "           WHERE f1.is_dir <> 0) n)," +
                "max_level AS" +
                " (SELECT MAX(f.t_level) AS max_level FROM datas f WHERE f.is_dir = 0)" +
                "SELECT n.*, (SELECT max_level FROM max_level) AS max_level FROM nums n";
    }

    private String getRuleInventoryInfo(Map<String, Object> map) {
        Long ruleId = Long.valueOf(map.get("ruleId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "	WITH datas AS (" +
                SqlCommon.getRuleChildObjectsSqlCommon(ruleId, projectId) +
                "	)," +
                "	 nums AS (" +
                "		SELECT" +
                "			SUM (n.total) AS total," +
                "			SUM (n.pub) AS pub," +
                "			SUM (n.not_pub) AS not_pub," +
                "           SUM (n.approve) as approve" +
                "		FROM" +
                "			(" +
                "				SELECT" +
                "					CASE" +
                "				WHEN f2.id IS NULL THEN" +
                "					1" +
                "				ELSE" +
                "					0" +
                "				END AS not_pub," +
                "				CASE" +
                "			WHEN f2.id IS NOT NULL THEN" +
                "				1" +
                "			ELSE" +
                "				0" +
                "			END AS pub," +
                "			1 AS total," +
                "  case when f2.id is null and jtbn.ID is not null then 1 else 0 end as approve" +
                "		FROM" +
                "			datas f1" +
                "		LEFT JOIN JECN_RULE f2 ON f1.id = f2.id" +
                " left join JECN_TASK_BEAN_NEW jtbn on jtbn.IS_LOCK=1 and jtbn.STATE<>5 and jtbn.R_ID=f1.id and jtbn.TASK_TYPE in (2,3)" +
                "		WHERE" +
                "			f1.is_dir <> 0" +
                "			) n" +
                "	)," +
                "	 max_level AS (" +
                "		SELECT" +
                "			MAX (f.t_level) AS max_level" +
                "		FROM" +
                "			datas f" +
                "		WHERE" +
                "			f.is_dir = 0" +
                "	) SELECT" +
                "		n.*, (" +
                "			SELECT" +
                "				max_level" +
                "			FROM" +
                "				max_level" +
                "		) AS max_level" +
                "	FROM" +
                "		nums n";
    }

    /**
     * 查询流程的最深级别，发布未发布总数审批数量
     *
     * @param map
     * @return
     */
    private String getProcessInventoryInfo(Map<String, Object> map) {
        boolean isContainFlowMap = false;

        if (map.containsKey("containsFlow") && (Boolean) map.get("containsFlow")) {
            isContainFlowMap = true;
        }
        String condition = isContainFlowMap ? " " : " WHERE f.isflow = 0 ";
        Long flowId = Long.valueOf(map.get("flowId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "	WITH datas AS (" +
                SqlCommon.getFlowChildObjectsSqlCommon(flowId, projectId) +
                "	)," +

                "	 nums AS (" +
                "		SELECT" +
                "			SUM (n.total) AS total," +
                "			SUM (n.pub) AS pub," +
                "			SUM (n.not_pub) AS not_pub," +
                "           SUM (n.approve) as approve" +
                "		FROM" +
                "			(" +
                "				SELECT" +
                "					CASE" +
                "               when jtbn.ID is not null then 0" +
                "				WHEN f2.flow_id IS NULL THEN" +
                "					1" +
                "				ELSE" +
                "					0" +
                "				END AS not_pub," +
                "				CASE" +
                "			WHEN f2.flow_id IS NOT NULL THEN" +
                "				1" +
                "			ELSE" +
                "				0" +
                "			END AS pub," +
                "			1 AS total," +
                "  case when f2.flow_id is null and jtbn.ID is not null then 1 else 0 end as approve" +
                "		FROM" +
                "			datas f1" +
                "		LEFT JOIN JECN_FLOW_STRUCTURE f2 ON f1.flow_id = f2.flow_id" +
                " left join JECN_TASK_BEAN_NEW jtbn on jtbn.IS_LOCK=1 and jtbn.STATE<>5 and jtbn.R_ID=f1.flow_id and jtbn.TASK_TYPE in (0,4)";
        sql += isContainFlowMap ? " " : "	 WHERE f1.isflow = 1";
        sql += "			) n" +
                "	)," +
                "	 max_level AS (" +
                "		SELECT" +
                "			MAX (f.t_level) AS max_level" +
                "		FROM" +
                "			datas f" +
                "			" + condition +
                "	) SELECT" +
                "		n.*, (" +
                "			SELECT" +
                "				max_level" +
                "			FROM" +
                "				max_level" +
                "		) AS max_level" +
                "	FROM" +
                "		nums n";
        return sql;
    }


    /**
     * 获得所有的流程
     *
     * @param param
     * @param type  0流程架构 1流程 其它流程架构和流程
     * @return
     */
    public static String getFlows(Map<String, Object> param, int type) {
        String condition = "";
        if (type == 0) {
            condition = " and jfst.ISFLOW=0";
        } else if (type == 1) {
            condition = " and jfst.ISFLOW=1";
        }
        if (param.get("ids") != null && StringUtils.isNotBlank(param.get("ids").toString())) {// 获得in中具体的流程和架构
            return "select * from JECN_FLOW_STRUCTURE_T jfst WHERE jfst.DEL_STATE=0 AND jfst.PROJECTID=#{projectId} and jfst.flow_id in " + param.get("ids").toString() + condition;
        } else if (param.get("flowId") != null && StringUtils.isNotBlank(param.get("flowId").toString())) {// 该流程架构/流程下的全部的子流程
            Long flowId = Long.valueOf(param.get("flowId").toString());
            Long projectId = Long.valueOf(param.get("projectId").toString());
            return SqlCommon.getFlowChildObjectsSqlCommon(flowId, projectId) + condition;
        } else {// 所有的流程
            return "select * from JECN_FLOW_STRUCTURE_T jfst WHERE jfst.DEL_STATE=0 AND jfst.PROJECTID=#{projectId}" + condition;
        }
    }

    public static String getRules(Map<String, Object> param) {
        if (param.get("ids") != null && StringUtils.isNotBlank(param.get("ids").toString())) {
            return "select * from JECN_RULE_T jfst WHERE jfst.project_id=#{projectId} and jfst.id in " + param.get("ids").toString();
        } else if (param.get("ruleId") != null && StringUtils.isNotBlank(param.get("ruleId").toString())) {
            Long ruleId = Long.valueOf(param.get("ruleId").toString());
            Long projectId = Long.valueOf(param.get("projectId").toString());
            return SqlCommon.getRuleChildObjectsSqlCommon(ruleId, projectId);
        } else {// 所有的流程
            return "select * from JECN_FLOW_STRUCTURE_T jfst WHERE jfst.DEL_STATE=0 AND jfst.PROJECTID=#{projectId}";
        }
    }

    public static String getStandards(Map<String, Object> param) {
        if (param.get("ids") != null && StringUtils.isNotBlank(param.get("ids").toString())) {
            return "select * from JECN_CRITERION_CLASSES jfst WHERE jfst.project_id=#{projectId} and jfst.CRITERION_CLASS_ID in " + param.get("ids").toString();
        } else if (param.get("id") != null && StringUtils.isNotBlank(param.get("id").toString())) {
            Long standardId = Long.valueOf(param.get("id").toString());
            Long projectId = Long.valueOf(param.get("projectId").toString());
            return SqlCommon.getStandardChildObjectsSqlCommon(standardId, projectId);
        } else {
            return "select * from JECN_CRITERION_CLASSES jfst WHERE jfst.PROJECT_ID=#{projectId}";
        }
    }

    public static String getFiles(Map<String, Object> param) {
        if (param.get("ids") != null && StringUtils.isNotBlank(param.get("ids").toString())) {
            return "select * from JECN_FILE_T jfst WHERE jfst.project_id=#{projectId} and jfst.del_state=0 and jfst.file_id in " + param.get("ids").toString();
        } else if (param.get("id") != null && StringUtils.isNotBlank(param.get("id").toString())) {
            Long fileId = Long.valueOf(param.get("id").toString());
            Long projectId = Long.valueOf(param.get("projectId").toString());
            return SqlCommon.getFileChildObjectsSqlCommon(fileId, projectId);
        } else {
            return "select * from JECN_FILE_T jfst WHERE jfst.PROJECT_ID=#{projectId} and jfst.del_state=0";
        }
    }

    public static String getRisks(Map<String, Object> param) {
        if (param.get("ids") != null && StringUtils.isNotBlank(param.get("ids").toString())) {
            return "select * from jecn_risk jfst WHERE jfst.project_id=#{projectId} and jfst.id in " + param.get("ids").toString();
        } else if (param.get("id") != null && StringUtils.isNotBlank(param.get("id").toString())) {
            Long riskId = Long.valueOf(param.get("id").toString());
            Long projectId = Long.valueOf(param.get("projectId").toString());
            return SqlCommon.getRiskChildObjectsSqlCommon(riskId, projectId);
        } else {
            return "select * from jecn_risk jfst WHERE jfst.PROJECT_ID=#{projectId}";
        }
    }

    public static String getOrgs(Map<String, Object> param) {
        if (param.get("ids") != null && StringUtils.isNotBlank(param.get("ids").toString())) {
            return "select * from JECN_FLOW_ORG jfst WHERE jfst.projectid=#{projectId} and jfst.org_id in " + param.get("ids").toString();
        } else if (param.get("orgId") != null && StringUtils.isNotBlank(param.get("orgId").toString())) {
            Long orgId = Long.valueOf(param.get("orgId").toString());
            Long projectId = Long.valueOf(param.get("projectId").toString());
            return SqlCommon.getOrgChildObjectsSqlCommon(orgId, projectId);
        } else {
            return "select * from JECN_FLOW_ORG jfst WHERE jfst.PROJECTID=#{projectId}";
        }
    }

    public String findFlowRelateds(Map<String, Object> param) {
        String sql = "with flows as (" + getFlows(param, 1) + ")";
        return ProcessSqlProvider.getFlowAllRelatedFiles(sql, param);
    }

    /**
     * 制度标准化文件
     *
     * @param map
     * @return
     */
    public String findRuleRelatedStandardizedSql(Map<String, Object> map) {
        return "SELECT RULES.ID, JF.FILE_ID, JF.FILE_NAME, 7 AS FILE_TYPE" +
                "  FROM (" + getRules(map) + ") RULES" +
                " INNER JOIN RULE_STANDARDIZED_FILE RSF" +
                "    ON RULES.ID = RSF.RELATED_ID" +
                " INNER JOIN JECN_FILE_T JF" +
                "    ON JF.FILE_ID = RSF.FILE_ID" +
                " WHERE RULES.IS_DIR <> 0" +
                " ORDER BY RULES.ID";
    }

}
