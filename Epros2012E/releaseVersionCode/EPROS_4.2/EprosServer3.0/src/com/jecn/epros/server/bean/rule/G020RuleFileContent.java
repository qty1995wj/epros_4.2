package com.jecn.epros.server.bean.rule;

import java.sql.Blob;
import java.util.Date;

import com.jecn.epros.server.bean.file.JecnFileContent;

/**
 * 制度文件内容表 本地上传方式存储到数据库
 * 
 * @author hyl
 * 
 */
public class G020RuleFileContent implements java.io.Serializable {
	/** 主键 */
	private Long id;
	/** 制度主表主键 */
	private Long ruleId;
	/** 文件内容 存放数据库 */
	private Blob fileStream;
	
	private byte[] bytes;
	/** 文件路径 存放本地 */
	private String filePath;
	/** 更新时间 */
	private Date updateTime;
	/** 更新人 */
	private Long updatePeopleId;
	/** 文件名称 */
	private String fileName;
	/** 文件版本存储标识  0:本地；1：数据库 */
	private int isVersionLocal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public Blob getFileStream() {
		return fileStream;
	}

	public void setFileStream(Blob fileStream) {
		this.fileStream = fileStream;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getIsVersionLocal() {
		return isVersionLocal;
	}

	public void setIsVersionLocal(int isVersionLocal) {
		this.isVersionLocal = isVersionLocal;
	}

	 static void add(JecnFileContent jecnFileContent) {
		// TODO Auto-generated method stub
		
	}
}
