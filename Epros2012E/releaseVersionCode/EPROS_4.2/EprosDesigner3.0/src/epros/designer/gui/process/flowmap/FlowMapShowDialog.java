package epros.designer.gui.process.flowmap;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.system.FileEnclosureSelectCommon;
import epros.designer.gui.system.FlowMapModeSelectCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.util.JecnUIUtil;

public abstract class FlowMapShowDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();// 510, 660

	/** center面板 */
	private JecnPanel centerPanel = new JecnPanel();// /510, 505

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();// 500, 30

	/** 流程地图名称Lab */
	private JLabel flowMapLab = new JLabel(JecnProperties.getValue("flowMapNameC"));
	/** 流程地图名称Field */
	JTextField flowMapField = new JTextField();// 170, 20
	/** 流程地图名称必填*号 */
	private JLabel flowMapNameRequired = new JLabel(JecnProperties.getValue("required"));

	/** 流程地图编号Lab */
	private JLabel mapNumLab = new JLabel(JecnProperties.getValue("flowMapNumC"));
	/** 流程地图编号Field */
	JTextField mapNumField = new JTextField();// 170, 20

	/** 目的Lab */
	private JLabel mapPurposeLab = new JLabel(JecnProperties.getValue("flowMaPurposeC"));
	/** 目的Field */
	JecnTextArea mapPurposeField = new JecnTextArea();// 428, 45
	/** 目的滚动面板 */
	private JScrollPane mapPurposeScrollPane = new JScrollPane(mapPurposeField);

	/** 范围Lab */
	private JLabel mapRangeLab = new JLabel(JecnProperties.getValue("flowMapRangeC"));
	/** 范围Field */
	JecnTextArea mapRangeField = new JecnTextArea();// 428, 45
	/** 范围滚动面板 */
	private JScrollPane mapRangeScrollPane = new JScrollPane(mapRangeField);

	/** 名称定义Lab */
	JLabel mapNameDefinedLab = new JLabel(JecnProperties.getValue("flowNameDefinC"));
	/** 名称定义Field */
	JecnTextArea mapNameDefinedField = new JecnTextArea();// /428, 45
	/** 名称定义滚动面板 */
	private JScrollPane mapNameDefinedScrollPane = new JScrollPane(mapNameDefinedField);

	/** 输入Lab */
	private JLabel mapInputLab = new JLabel(JecnProperties.getValue("flowMapInC"));
	/** 输入Field */
	JecnTextArea mapInputField = new JecnTextArea();// 428, 45
	/** 输入滚动面板 */
	private JScrollPane mapInputScrollPane = new JScrollPane(mapInputField);

	/** 输出Lab */
	private JLabel mapOutputLab = new JLabel(JecnProperties.getValue("flowMapOutC"));
	/** 输出Field */
	JecnTextArea mapOutputField = new JecnTextArea();// 428, 45
	/** 输出滚动面板 */
	private JScrollPane mapOutputScrollPane = new JScrollPane(mapOutputField);

	/** 步骤说明Lab */
	JLabel mapStepDescLab = new JLabel(JecnProperties.getValue("mapStepDesc"));
	/** 步骤说明Field */
	JecnTextArea mapStepDescField = new JecnTextArea();// 428, 45
	/** 步骤说明滚动面板 */
	private JScrollPane mapStepDescScrollPane = new JScrollPane(mapStepDescField);

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 验证提示 */
	JLabel verfyLab = new JLabel();
	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	private JecnTreeNode selectNode = null;
	private JecnTree jTree = null;

	/** 面板宽 */
	private int dialogWidth = getWidthMin();
	/** 面板高 */
	private int dialogHeigh = 150;

	/**
	 * 附件
	 */
	FileEnclosureSelectCommon fileEnclosureSelectCommon;
	/** 附件的ID */
	private Long fileId = null;

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	/** 目的是否显示 */
	private boolean isShowPurpose = false;
	/** 范围是否显示 */
	private boolean isShowRange = false;
	/** 术语定义是否显示 */
	private boolean isShowDefine = false;
	/** 输入是否显示 */
	private boolean isShowInput = false;
	/** 输出是否显示 */
	private boolean isShowOutput = false;
	/** 步骤说明是否显示 */
	private boolean isShowInstructions = false;
	/** 附件是否显示 */
	private boolean isShowAttachment = false;

	/** 是否显示模板 */
	private boolean isShowMode = false;

	/** 流程模板 **/
	private FlowMapModeSelectCommon flowMapModeSelectCommon;

	/***
	 * 是否添加
	 */
	private boolean isAdd = false;

	public FlowMapShowDialog(JecnTreeNode selectNode, JecnTree jTree, boolean isAdd) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.isAdd = isAdd;
		this.setTitle(JecnProperties.getValue("newFlowMap"));
		initConfig();
		this.setSize(dialogWidth, dialogHeigh);
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 目的
		mapPurposeField.setBorder(null);
		mapPurposeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapPurposeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapPurposeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 范围
		mapRangeField.setBorder(null);
		mapRangeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapRangeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapRangeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 名称定义
		mapNameDefinedField.setBorder(null);
		mapNameDefinedScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapNameDefinedScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapNameDefinedScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 输入
		mapInputField.setBorder(null);
		mapInputScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapInputScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapInputScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 输出
		mapOutputField.setBorder(null);
		mapOutputScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapOutputScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapOutputScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 步骤说明
		mapStepDescField.setBorder(null);
		mapStepDescScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapStepDescScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapStepDescScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		flowMapNameRequired.setForeground(Color.red);
		verfyLab.setForeground(Color.red);
		isShowMode = JecnConfigTool.isShowProcessMode();

		// 事件实始化
		acitonInit();

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	private void acitonInit() {
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();

			}
		});

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	protected abstract boolean isUpdate();

	protected abstract void okButPerformed();

	/**
	 * @author yxw 2012-6-28
	 * @description:布局
	 */
	protected void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 3, 3, 3);
		// centerPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		mainPanel.add(centerPanel, c);
		centerPanel.setLayout(new GridBagLayout());
		int row = 0;
		// 流程名称
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		centerPanel.add(flowMapLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(flowMapField, c);
		c = new GridBagConstraints(3, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		centerPanel.add(flowMapNameRequired, c);
		row++;

		if (JecnConfigTool.getResultValue("isShowProcessMapNum") == 1) {// 是否显示编号
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapNumLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			centerPanel.add(mapNumField, c);
		}
		row++;
		if (this.isShowPurpose) {
			// 目的
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapPurposeLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapPurposeScrollPane, c);
			row++;
		}
		if (this.isShowRange) {
			// 范围mapRangeLab
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapRangeLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapRangeScrollPane, c);
			row++;
		}
		if (this.isShowDefine) {
			// 名称定义
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapNameDefinedLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapNameDefinedScrollPane, c);
			row++;
		}
		if (this.isShowInput) {
			// 输入
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapInputLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapInputScrollPane, c);
			row++;
		}

		if (this.isShowOutput) {
			// 输出
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapOutputLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapOutputScrollPane, c);
			row++;
		}

		if (this.isShowInstructions) {
			// 操作说明
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapStepDescLab, c);
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapStepDescScrollPane, c);
			row++;
		}
		if (this.isShowAttachment) {
			// 附件
			fileEnclosureSelectCommon = new FileEnclosureSelectCommon(row, centerPanel, insets, fileId, this, false);
			row++;
		}
		// 流程模板
		if (isShowMode && isAdd) {
			flowMapModeSelectCommon = new FlowMapModeSelectCommon(row, centerPanel, insets, null, this, JecnProperties
					.getValue("flowMapMdelC"), false);
		}
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				new Insets(0, 3, 0, 3), 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

	/***
	 * 返回流程架构模板ID
	 * 
	 * @return
	 */
	public Long getModeId() {
		if (isShowMode && isAdd) {
			return flowMapModeSelectCommon.getIdResult();
		}
		return null;
	}

	private void initConfig() {
		boolean isExist = false;
		List<JecnConfigItemBean> processMapItemList = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(0, 4);
		for (JecnConfigItemBean configItemBean : processMapItemList) {
			if ("0".equals(configItemBean.getValue())) {// 不显示
				continue;
			}
			isExist = true;
			if ("processMapPurpose".equals(configItemBean.getMark())) { // 目的
				isShowPurpose = true;
				dialogHeigh = dialogHeigh + 60;
			} else if ("processMapScope".equals(configItemBean.getMark())) {// 范围
				isShowRange = true;
				dialogHeigh = dialogHeigh + 60;
			} else if ("processMapTerms".equals(configItemBean.getMark())) {// 术语定义
				isShowDefine = true;
				dialogHeigh = dialogHeigh + 60;
			} else if ("processMapInput".equals(configItemBean.getMark())) {// 输入
				isShowInput = true;
				dialogHeigh = dialogHeigh + 60;
			} else if ("processMapOutput".equals(configItemBean.getMark())) {// 输出
				isShowOutput = true;
				dialogHeigh = dialogHeigh + 60;
			} else if ("processMap".equals(configItemBean.getMark())) {// 流程地图

			} else if ("processMapInstructions".equals(configItemBean.getMark())) {// 步骤说明
				isShowInstructions = true;
				dialogHeigh = dialogHeigh + 60;
			} else if ("processMapAttachment".equals(configItemBean.getMark())) {// 附件
				isShowAttachment = true;
				dialogHeigh = dialogHeigh + 40;
			} else if (isShowMode && isAdd) {
				dialogHeigh = dialogHeigh + 40;
			}
		}
		if (isExist) {
			dialogWidth = getWidthMax();
		}
	}

	public JecnTreeNode getSelectNode() {
		return selectNode;
	}

	public JecnTree getjTree() {
		return jTree;
	}

	/**
	 * 获得附件
	 * 
	 * @return
	 */
	public Long getFileEnclosureId() {
		if (fileEnclosureSelectCommon != null) {
			return fileEnclosureSelectCommon.getResultFileId();
		}
		return null;
	}

	class VerifLabel extends JLabel {
		public VerifLabel() {
			initComponents();
		}

		private void initComponents() {
			// 项的内容大小
			Dimension dimension = new Dimension(420, 15);
			this.setPreferredSize(dimension);
			this.setMinimumSize(dimension);
			this.setMaximumSize(dimension);
			this.setForeground(Color.red);
		}
	}
}
