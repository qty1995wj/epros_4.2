package epros.designer.gui.process.mode.choose;

import java.awt.event.MouseEvent;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.popedom.person.JecnPersonInfoPanel;
import epros.designer.gui.process.mode.FlowModelMagTreeListener;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
/**
 * 流程模板管理树
 * 包括：流程模板，流程文件模板
 * @author 2012-06-26
 *
 */
public class FlowModelChooseTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(FlowModelChooseTree.class);
	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new FlowModelMagTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			// 根节点"流程模板管理"
			//流程树节点	
			JecnTreeNode rootNode=JecnTreeCommon.createTreeRoot(TreeNodeType.processModeRoot,JecnProperties.getValue("flowModel"));
			List<JecnTreeBean>list=ConnectionPool.getProcessModeAction().getFlowModels(0L);
			JecnTreeCommon.addNLevelNodes(list, rootNode);
			return new JecnTreeModel(rootNode) ;
		} catch (Exception e) {
			log.error("FlowModelChooseTree getTreeModel is error",e);
			return null;
		}
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
