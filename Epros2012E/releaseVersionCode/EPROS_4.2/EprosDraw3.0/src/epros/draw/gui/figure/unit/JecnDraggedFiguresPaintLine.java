package epros.draw.gui.figure.unit;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseKeyActiveFigure;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.zoom.JecnWorkflowZoomProcess;

/**
 * 拖动图形 画图形相关连接线
 * 
 * @author Administrator
 * @date： 日期：Apr 27, 2012 时间：8:00:36 PM
 */
public class JecnDraggedFiguresPaintLine {
	/** 线段过短常量标识 */
	private static int IS_SHORT_VALUE = 10;

	/**
	 * 
	 * 通过图形查找相应的连接线
	 * 
	 * 拖动图形画图形相关连接线
	 * 
	 * @param figureList
	 */
	public static void paintLineByFigures(List<JecnBaseFigurePanel> figureList) {
		if (figureList == null || JecnDrawMainPanel.getMainPanel() == null
				|| JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		// 得到图形相关所有连接线
		Set<JecnBaseManhattanLinePanel> listBaseLines = getLineListByFigure(figureList);
		if (listBaseLines == null || listBaseLines.size() == 0) {
			return;
		}
		for (JecnBaseManhattanLinePanel jecnBaseManhattanLinePanel : listBaseLines) {
			if (jecnBaseManhattanLinePanel.isDrraged()) {// 如果线段不是曼哈顿生成的线段执行脱线算法
				draggedFigureDrawLine(jecnBaseManhattanLinePanel);
			} else {// 执行曼哈顿算法
				jecnBaseManhattanLinePanel.paintLineByRouter();
			}
			if (jecnBaseManhattanLinePanel.isShowResizeHandle()) {
				jecnBaseManhattanLinePanel.showManhattanLineResizeHandles();
			}
		}
	}

	/**
	 * 画单个图形相关连接线
	 * 
	 * @param figure
	 *            图形
	 */
	public static void paintLineByFigures(JecnBaseFigurePanel figure) {
		Set<JecnBaseManhattanLinePanel> listBaseLine = new HashSet<JecnBaseManhattanLinePanel>();
		// 得到图形相关所有连接线
		getManhattanLineList(listBaseLine, figure);
		for (JecnBaseManhattanLinePanel jecnBaseManhattanLinePanel : listBaseLine) {
			if (jecnBaseManhattanLinePanel.isDrraged()) {// 如果线段不是曼哈顿生成的线段执行脱线算法
				draggedFigureDrawLine(jecnBaseManhattanLinePanel);
			} else {// 执行曼哈顿算法
				jecnBaseManhattanLinePanel.paintLineByRouter();
			}
		}
	}

	/**
	 * 
	 * 拖动图形后花连接线 连接线不执行曼哈顿算法
	 * 
	 * @param jecnBaseManhattanLinePanel
	 */
	public static void draggedFigureDrawLine(JecnBaseManhattanLinePanel jecnBaseManhattanLinePanel) {
		if (jecnBaseManhattanLinePanel.getLineSegments() == null) {

			JecnLogConstants.LOG_JECN_PAINT_LINE_IF_DRAGGED_FIGURE.error("拖动图形画线算法线段数组为空！"
					+ jecnBaseManhattanLinePanel.getLineSegments());
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		if (jecnBaseManhattanLinePanel.curLineSegmentsClone.size() > 0) {// 剪切后复制
			jecnBaseManhattanLinePanel.getLineSegments().addAll(jecnBaseManhattanLinePanel.curLineSegmentsClone);
			jecnBaseManhattanLinePanel.curLineSegmentsClone.clear();
		}

		if (!jecnBaseManhattanLinePanel.isDrraged()) {// 如果拖动属性为false
			// 默认执行曼哈顿算法
			jecnBaseManhattanLinePanel.paintLineByRouter();
			return;
		}
		// 获取小线段的数目
		int lineSegmentNum = jecnBaseManhattanLinePanel.getLineSegments().size();

		// 只有一条线段,曼哈顿算法画线
		if (lineSegmentNum == 0 || lineSegmentNum == 1) {
			// 默认执行曼哈顿算法
			jecnBaseManhattanLinePanel.paintLineByRouter();
			return;
		}

		// 错误情况:ManhattanLine中的多条线段位于一条直线上
		if (jecnBaseManhattanLinePanel.checkLine()) {
			// 默认执行曼哈顿算法
			jecnBaseManhattanLinePanel.paintLineByRouter();
			return;
		}
		if (lineSegmentNum < 1) {
			return;
		}
		// 正常情况下拖动图形画连接线
		otherCasesToDrawLine(jecnBaseManhattanLinePanel);
	}

	/**
	 * 正常情况下拖动图形画连接线
	 * 
	 * @param baseManhattLine
	 */
	private static void otherCasesToDrawLine(JecnBaseManhattanLinePanel baseManhattLine) {
		// 新的ManhattanLine线 100%状态下的开始、结束点
		Point newStartPt = baseManhattLine.getOriginalStartPoint();
		Point newEndPt = baseManhattLine.getOriginalEndPoint();

		// 原来的ManhattanLine线开始、结束点
		Point oldStartPt = baseManhattLine.getLineSegments().get(0).getStartPoint();
		Point oldEndPt = baseManhattLine.getLineSegments().get(baseManhattLine.getLineSegments().size() - 1)
				.getEndPoint();

		// 开始点和结束点都未变化
		if (newStartPt.equals(oldStartPt) && newEndPt.equals(oldEndPt)) {
			return;
		} // 初始化小线段备份数据集
		List<LineSegment> lineSegmentsClone = baseManhattLine.getLineSegmentsClone(baseManhattLine);

		if (lineSegmentsClone.size() == 0 && baseManhattLine.curLineSegmentsClone.size() == 0) {
			baseManhattLine.paintLineByRouter();
		}
		if (!newStartPt.equals(oldStartPt) && newEndPt.equals(oldEndPt)) {// ManhattanLine开始点变化(需要加点的边界限制)
			drawLineFromStart(oldStartPt, newStartPt, lineSegmentsClone);
		} else if (newStartPt.equals(oldStartPt) && !newEndPt.equals(oldEndPt)) {// ManhattanLine结束点变化(需要加点的边界限制)
			drawLineFromEnd(oldEndPt, newEndPt, lineSegmentsClone);
		} else if (!newStartPt.equals(oldStartPt) && !newEndPt.equals(oldEndPt)) {// ManhattanLine整条变化
			drawAllLine(newStartPt, newEndPt, lineSegmentsClone);
		}
		// 是否执行放大缩小算法
		if (JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale() != 1.0) {
			new JecnWorkflowZoomProcess(JecnDrawMainPanel.getMainPanel().getWorkflow())
					.zoomManhattanLine(baseManhattLine);
		}
	}

	/**
	 * 
	 * ManhattanLine开始点变化画线
	 * 
	 * @param oldPoint
	 *            拖动前端点
	 * @param newPoint
	 *            拖动后端点
	 * @param lineSegmentsClone
	 *            备份的小线段集合
	 */
	private static void drawLineFromStart(Point oldPoint, Point newPoint, List<LineSegment> lineSegmentsClone) {
		if (lineSegmentsClone == null || lineSegmentsClone.size() < 1) {
			return;
		}
		// 获取包含端点的线段（端点为开始点）
		LineSegment curLineSegment = lineSegmentsClone.get(0);
		// 获取线段标准点
		LineSegment standardLineSegment = lineSegmentsClone.get(1);
		Point standardPoint = standardLineSegment.getEndPoint();
		// 第一天线段的领一个端点
		Point otherPoint = draggedLineGetRefPoint(curLineSegment, oldPoint, newPoint, standardPoint);

		if (otherPoint == null) {
			return;
		}
		// 画线
		List<Point> listTempPoint = new ArrayList<Point>();

		listTempPoint.add(newPoint);
		listTempPoint.add(otherPoint);
		listTempPoint.add(standardPoint);

		// 删除备份小线段中的第一条和第二条线段
		lineSegmentsClone.remove(curLineSegment);
		lineSegmentsClone.remove(standardLineSegment);

		// 画线
		paintLineByListPoint(listTempPoint, lineSegmentsClone, curLineSegment.getBaseManhattanLine());
	}

	/**
	 * 根据端点集合生成新的连接线
	 * 
	 * @param listTempPoint
	 *            生成新的端点集合
	 * @param lineSegmentsClone
	 *            备份小线段集合
	 * @param manhattanLinePanel
	 *            当前操作的曼哈顿线段
	 */
	public static void paintLineByListPoint(List<Point> listTempPoint, List<LineSegment> lineSegmentsClone,
			JecnBaseManhattanLinePanel manhattanLinePanel) {
		// 获取当前线段下小线段集合
		List<JecnManhattanRouterResultData> routerResultList = manhattanLinePanel.getRouterResultData(listTempPoint);

		if (routerResultList == null) {
			return;
		}
		if (lineSegmentsClone != null && lineSegmentsClone.size() > 0) {
			for (LineSegment lineSegment : lineSegmentsClone) {
				JecnManhattanRouterResultData data = new JecnManhattanRouterResultData();
				data.setStartPoint(lineSegment.getStartPoint());
				data.setEndPint(lineSegment.getEndPoint());
				routerResultList.add(data);
			}
		}
		// 排序
		routerResultList = manhattanLinePanel.reOrderLinePoint(routerResultList);
		// 添加连接线
		manhattanLinePanel.drawLineSegmentByListData(routerResultList);

		// 刷新面板
		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}

	/**
	 * 拖动线是横线获取关联点处理
	 * 
	 * @param curLineSegment
	 *            与图形先关联的线段
	 * @param oldPoint
	 *            图形和线的端点（线的开始点或结束点）
	 * @param newPoint
	 *            拖动后生成的新的端点
	 * @param standardPoint
	 *            标准点(当前线段关联线段的开始点或结束点，该点不在当前线段上)
	 */
	private static Point draggedLineGetRefPoint(LineSegment curLineSegment, Point oldPoint, Point newPoint,
			Point standardPoint) {
		// 获取拖动下移动的方向
		MovePath movePath = getMovePath(oldPoint, newPoint);
		// 当前线段的另一个端点（开始点或结束点 newPoint未开始，otherPoint为结束）
		Point otherPoint = new Point();
		switch (curLineSegment.getLineSegmentData().getLineDirectionEnum()) {
		case horizontal: // 水平拖动
			// 设置端点坐标
			otherPoint.x = standardPoint.x;
			otherPoint.y = newPoint.y;
			if (isToShortHorizontal(movePath, curLineSegment, otherPoint, newPoint, standardPoint)) {
				return null;
			}
			break;
		case vertical:// 垂直拖动
			// 设置端点坐标
			otherPoint.x = newPoint.x;
			otherPoint.y = standardPoint.y;
			if (isToShortVertical(movePath, curLineSegment, otherPoint, newPoint, standardPoint)) {
				return null;
			}
			break;
		}

		return otherPoint;
	}

	/**
	 * 拖动图形关联线段为横线时，线段过短处理
	 * 
	 * @param movePath
	 *            鼠标拖动图形移动方向
	 * @param curLineSegment
	 *            图形关联的线段
	 * @param otherPoint
	 *            和图形关联线段的另一个端点
	 * @param newPoint
	 *            拖动图形生成的新的端点
	 * @param standardPoint
	 *            标准点(当前线段关联线段的开始点或结束点，该点不在当前线段上)
	 * @return true 执行曼哈顿路由算法
	 */
	private static boolean isToShortHorizontal(MovePath movePath, LineSegment curLineSegment, Point otherPoint,
			Point newPoint, Point standardPoint) {
		switch (movePath) {
		case horizontal:
			if (Math.abs(otherPoint.y - standardPoint.y) <= IS_SHORT_VALUE) {
				// 执行曼哈顿路由算法
				curLineSegment.getBaseManhattanLine().paintLineByRouter();
				return true;
			}
			break;
		case vertical:
			if (Math.abs(otherPoint.x - newPoint.x) <= IS_SHORT_VALUE) {
				// 执行曼哈顿路由算法
				curLineSegment.getBaseManhattanLine().paintLineByRouter();
				return true;
			}
			break;
		case tilt:
			if (Math.abs(otherPoint.y - standardPoint.y) <= IS_SHORT_VALUE
					|| Math.abs(otherPoint.x - newPoint.x) <= IS_SHORT_VALUE) {
				// 执行曼哈顿路由算法
				curLineSegment.getBaseManhattanLine().paintLineByRouter();
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * 拖动图形关联线段为横线时，线段过短处理
	 * 
	 * @param movePath
	 *            鼠标拖动图形移动方向
	 * @param curLineSegment
	 *            图形关联的线段
	 * @param otherPoint
	 *            和图形关联线段的另一个端点
	 * @param newPoint
	 *            拖动图形生成的新的端点
	 * @param standardPoint
	 *            标准点(当前线段关联线段的开始点或结束点，该点不在当前线段上)
	 * @return true 执行曼哈顿路由算法
	 */
	private static boolean isToShortVertical(MovePath movePath, LineSegment curLineSegment, Point otherPoint,
			Point newPoint, Point standardPoint) {
		switch (movePath) {
		case horizontal:// 水平移动
			if (Math.abs(otherPoint.x - standardPoint.x) <= IS_SHORT_VALUE) {
				// 执行曼哈顿路由算法
				curLineSegment.getBaseManhattanLine().paintLineByRouter();
				return true;
			}
			break;
		case vertical:
			if (Math.abs(otherPoint.y - newPoint.y) <= IS_SHORT_VALUE) {
				// 执行曼哈顿路由算法
				curLineSegment.getBaseManhattanLine().paintLineByRouter();
				return true;
			}
			break;
		case tilt:
			if (Math.abs(otherPoint.y - newPoint.y) <= IS_SHORT_VALUE
					|| Math.abs(otherPoint.x - standardPoint.x) <= IS_SHORT_VALUE) {
				// 执行曼哈顿路由算法
				curLineSegment.getBaseManhattanLine().paintLineByRouter();
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * 拖动图形的拖动方向
	 * 
	 * @author Administrator
	 * @date： 日期：May 7, 2012 时间：1:54:29 PM
	 */
	private enum MovePath {
		// up, // ↑上移
		// down, // ↓下移
		// left, // ←左移
		// right, // →右移
		// leftUp, // ↖左上
		// rightUp, // ↗右上
		// leftDown, // ↙左下
		// rightDown
		// // ↘右下
		horizontal, // 水平移动
		vertical, // 垂直移动
		tilt
		// 倾斜移动

	}

	/**
	 * 
	 * 拖动图形移动方向
	 * 
	 * @param oldPoint
	 * @param newPoint
	 * @return
	 */
	private static MovePath getMovePath(Point oldPoint, Point newPoint) {
		int oldX = oldPoint.x;
		int oldY = oldPoint.y;
		int newX = newPoint.x;
		int newY = newPoint.y;
		if (oldY == newY && oldX != newX) {// 纵坐标相同，横坐标不懂，水平移动
			return MovePath.horizontal;
		} else if (oldX == newX && oldY != newY) {// 横坐标相同，纵坐标不懂，纵向移动
			return MovePath.vertical;
		} else if (oldY != newY && oldX != newX) {
			return MovePath.tilt;
		}
		return null;
	}

	/**
	 * 
	 * ManhattanLine结束变化画线
	 * 
	 * @param theOldPoint
	 * @param newEndPt
	 * @param lineSegmentsClone
	 * @param lineSegmentNum
	 */
	private static void drawLineFromEnd(Point oldPoint, Point newPoint, List<LineSegment> lineSegmentsClone) {
		if (lineSegmentsClone == null || lineSegmentsClone.size() < 1) {
			return;
		}
		// 获取包含端点的线段（端点为结束点）
		LineSegment curLineSegment = lineSegmentsClone.get(lineSegmentsClone.size() - 1);
		// 获取线段标准点
		LineSegment standardLineSegment = lineSegmentsClone.get(lineSegmentsClone.size() - 2);
		Point standardPoint = standardLineSegment.getStartPoint();
		// 第一天线段的领一个端点
		Point otherPoint = draggedLineGetRefPoint(curLineSegment, oldPoint, newPoint, standardPoint);

		if (otherPoint == null) {
			return;
		}
		// 画线
		List<Point> listTempPoint = new ArrayList<Point>();

		listTempPoint.add(newPoint);
		listTempPoint.add(otherPoint);
		listTempPoint.add(standardPoint);

		// 删除备份小线段中的第一条和第二条线段
		lineSegmentsClone.remove(curLineSegment);
		lineSegmentsClone.remove(standardLineSegment);

		// 画线
		paintLineByListPoint(listTempPoint, lineSegmentsClone, curLineSegment.getBaseManhattanLine());
	}

	/**
	 * 
	 * ManhattanLine整条变化画线
	 * 
	 * @param newStartPt
	 * @param newEndPt
	 * @param lineSegmentsClone
	 * @param lineSegmentNum
	 */
	private static void drawAllLine(Point newStartPt, Point newEndPt, List<LineSegment> lineSegmentsClone) {
		if (lineSegmentsClone == null || lineSegmentsClone.size() < 1) {
			return;
		}
		// 记录线段端点集合
		List<Point> listTempPoint = new ArrayList<Point>();
		Point prePoint = new Point(newStartPt.x, newStartPt.y);
		Point newPoint = null;
		// 记录开始点
		listTempPoint.add(newStartPt);
		int lineLength = 0;// 线的长度

		for (int i = 0; i < lineSegmentsClone.size() - 1; i++) {
			LineSegment theLineSegment = lineSegmentsClone.get(i);
			switch (theLineSegment.getLineSegmentData().getLineDirectionEnum()) {
			case horizontal:
				lineLength = theLineSegment.getEndPoint().x - theLineSegment.getStartPoint().x;
				newPoint = new Point(prePoint.x + lineLength, prePoint.y);
				if (i == lineSegmentsClone.size() - 2) {// 尾线
					if (newPoint.x != newEndPt.x) {
						newPoint.x = newEndPt.x;
					}
				}
				break;
			case vertical:
				lineLength = theLineSegment.getEndPoint().y - theLineSegment.getStartPoint().y;
				newPoint = new Point(prePoint.x, prePoint.y + lineLength);
				if (i == lineSegmentsClone.size() - 2) {// 尾线
					if (newPoint.y != newEndPt.y) {
						newPoint.y = newEndPt.y;
					}
				}
				break;
			}
			listTempPoint.add(newPoint);
			// 更新端点集合
			prePoint.x = newPoint.x;
			prePoint.y = newPoint.y;
		}
		// 清空临时端点
		prePoint = null;
		listTempPoint.add(newEndPt);
		// 画线
		paintLineByListPoint(listTempPoint, null, lineSegmentsClone.get(0).getBaseManhattanLine());
		// 清空备份的小线段集合
		lineSegmentsClone = null;
	}

	/**
	 * 得到与图形相关的连接线
	 * 
	 * @param curFigureList
	 * @return
	 */
	public static Set<JecnBaseManhattanLinePanel> getLineListByFigure(List<JecnBaseFigurePanel> curFigureList) {
		Set<JecnBaseManhattanLinePanel> listBaseLine = new HashSet<JecnBaseManhattanLinePanel>();
		for (int i = 0; i < curFigureList.size(); i++) {
			// 得到线
			JecnBaseFigurePanel curFigure = curFigureList.get(i);
			// 获取图形相关联系集合
			getManhattanLineList(listBaseLine, curFigure);
		}
		return listBaseLine;
	}

	/**
	 * 获取图形相关联系集合
	 * 
	 * @param listBaseLine
	 *            连接线集合
	 * @param curFigure
	 *            图形元素
	 * @return Set<JecnBaseManhattanLinePanel>连接线集合
	 */
	public static void getManhattanLineList(Set<JecnBaseManhattanLinePanel> listBaseLine, JecnBaseFigurePanel curFigure) {
		// 获取图形相关连线数据集合
		List<JecnFigureRefManhattanLine> listRefLine = curFigure.getListFigureRefManhattanLine();
		for (JecnFigureRefManhattanLine jecnFigureRefManhattanLine : listRefLine) {
			if (jecnFigureRefManhattanLine != null && jecnFigureRefManhattanLine.getManhattanLinePanel() != null) {
				listBaseLine.add(jecnFigureRefManhattanLine.getManhattanLinePanel());
			}
		}
	}

	/**
	 * 
	 * 获取当前画图面板选中流程元素(图形和线)以及选中图形相关联的连接线.
	 * 
	 * @return List<JecnBaseFlowElementPanel> 不重复的流程元素
	 */
	public static List<JecnBaseFlowElementPanel> getSelectFlowElemRefLine() {
		// 当前面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return new ArrayList<JecnBaseFlowElementPanel>();
		}
		// 选中流程元素
		List<JecnBaseFlowElementPanel> currentSelectElement = workflow.getCurrentSelectElement();

		return getSelectFlowElemRefLine(currentSelectElement);
	}

	/**
	 * 
	 * 获取选中流程元素(图形和线)以及选中图形相关联的连接线.
	 * 
	 * @param currentSelectElement
	 *            List<JecnBaseFlowElementPanel> 选中流程元素
	 * 
	 * @return List<JecnBaseFlowElementPanel> 不重复的流程元素
	 */
	public static List<JecnBaseFlowElementPanel> getSelectFlowElemRefLine(
			List<JecnBaseFlowElementPanel> currentSelectElement) {
		// 用于撤销恢复:记录被操作对象集合
		List<JecnBaseFlowElementPanel> seletetPanelList = new ArrayList<JecnBaseFlowElementPanel>();

		if (currentSelectElement == null) {
			return seletetPanelList;
		}

		for (int i = 0; i < currentSelectElement.size(); i++) {
			JecnBaseFlowElementPanel flowElementPanel = currentSelectElement.get(i);

			if (!seletetPanelList.contains(flowElementPanel)) {// 不包含
				seletetPanelList.add(flowElementPanel);
			}

			if (flowElementPanel instanceof JecnBaseFigurePanel) {// 选中图形
				// 图形
				JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) flowElementPanel;

				// 图形相关连接线数据集合
				List<JecnFigureRefManhattanLine> listRefLine = curFigure.getListFigureRefManhattanLine();

				for (JecnFigureRefManhattanLine jecnFigureRefManhattanLine : listRefLine) {
					// 图形相关连接线
					JecnBaseManhattanLinePanel manhattanLinePanel = jecnFigureRefManhattanLine.getManhattanLinePanel();

					if (manhattanLinePanel != null && !seletetPanelList.contains(manhattanLinePanel)) {// 图形相关联连接线存在且不包含于seletetPanelList集合中
						seletetPanelList.add(manhattanLinePanel);
					}
				}
			}
		}
		return seletetPanelList;
	}

	/**
	 * 
	 * 使用场合：切换关键活动时
	 * 
	 * 替换用新的关键活动对象替换旧的关键活动对象，新的关键活动对象获取旧的关键活动对于的所有数据对象，唯一标识figureName除外
	 * 
	 * @param newActiveFigure
	 *            JecnBaseKeyActiveFigure newActiveFigure新的关键活动对象
	 * @param oldActiveFigure
	 *            JecnBaseKeyActiveFigure oldActiveFigure旧的关键活动对象
	 */
	public static void coverActiveFigure(JecnBaseKeyActiveFigure newActiveFigure,
			JecnBaseKeyActiveFigure oldActiveFigure) {
		if (newActiveFigure == null || oldActiveFigure == null) {
			return;
		}

		// 获取旧的关键活动对应的连接线对象集合
		Set<JecnBaseManhattanLinePanel> oldManLineList = new HashSet<JecnBaseManhattanLinePanel>();
		// 获取图形相关联系集合
		getManhattanLineList(oldManLineList, oldActiveFigure);
		// 新的图形替换连接线中原有图形
		setNewManLineRefEditPanel(oldManLineList, newActiveFigure, oldActiveFigure);
		// 删除旧的图形元素对应的连接线集合
		oldActiveFigure.getListFigureRefManhattanLine().clear();
	}

	/**
	 * 新的图形替换连接线中原有图形
	 * 
	 * @param oldManLineList
	 *            Set<JecnBaseManhattanLinePanel> 连接线数集合
	 * @param newActiveFigure
	 *            JecnBaseKeyActiveFigure 新的图形对象
	 */
	public static void setNewManLineRefEditPanel(Set<JecnBaseManhattanLinePanel> oldManLineList,
			JecnBaseKeyActiveFigure newActiveFigure, JecnBaseKeyActiveFigure oldActiveFigure) {
		if (oldManLineList == null || newActiveFigure == null) {
			return;
		}
		for (JecnBaseManhattanLinePanel linePanel : oldManLineList) {
			if (linePanel.getFlowElementData().getStartEditPointType() == null
					|| linePanel.getFlowElementData().getEndEditPointType() == null) {// 如果连接线的编辑点变位置标识不存在跳出当前循环
				continue;

			}
			// 判断待变化的是否为开始图形
			boolean isStart = isSamePanel(linePanel.getStartFigure(), oldActiveFigure);
			EditPointType editPointType = null;
			if (isStart) {// 开始图形变化
				// 重新设置连接线开始图形和结束图形
				linePanel.setStartFigure(newActiveFigure);
				editPointType = linePanel.getFlowElementData().getStartEditPointType();
			} else {
				// 重新设置连接线开始图形和结束图形
				linePanel.setEndFigure(newActiveFigure);
				editPointType = linePanel.getFlowElementData().getEndEditPointType();
			}

			// 设置连接线的开始图形和结束图形与连接线的关系数据
			addRefManhattLineToFigure(linePanel, newActiveFigure, isStart, editPointType);
		}
	}

	/**
	 * 设置连接线的开始图形和结束图形与连接线的关系数据
	 * 
	 * @param linePanel
	 *            连接线
	 * @param startEditPort
	 *            开始图形编辑点
	 * @param endEditPort
	 *            结束图形编辑点
	 */
	public static void addRefManhattLineToFigure(JecnBaseManhattanLinePanel linePanel,
			JecnBaseFigurePanel newActiveFigure, boolean isStart, EditPointType editPointType) {
		// 设置连接线开始图形与线段的关联数据
		JecnFigureRefManhattanLine refManhattanLine = new JecnFigureRefManhattanLine(linePanel, editPointType, isStart);

		if (isStart) {
			// 设置连接线结束图形与线段的关联数据
			linePanel.getStartFigure().getListFigureRefManhattanLine().add(refManhattanLine);
		} else {
			// 添加关联线段数据到图形
			linePanel.getEndFigure().getListFigureRefManhattanLine().add(refManhattanLine);
		}
	}

	/**
	 * 判断两个图形是否为同一个图形
	 * 
	 * @param lineFigurePanel
	 *            JecnBaseFigurePanel 线相关图形
	 * @param oldActiveFigure
	 *            JecnBaseFigurePanel 传入图形
	 * @return True 相同对象，false 不同对象
	 */
	public static boolean isSamePanel(JecnBaseFigurePanel lineFigurePanel, JecnBaseFigurePanel oldActiveFigure) {
		if (lineFigurePanel == null || oldActiveFigure == null) {
			return false;
		}
		if (lineFigurePanel == oldActiveFigure) {
			return true;
		}
		return false;
	}

	public void addImportManLine(JecnBaseManhattanLinePanel linePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || linePanel.getLineSegments() == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		for (LineSegment lineSegment : linePanel.getLineSegments()) {
			desktopPane.add(lineSegment);
			lineSegment.drawArc(lineSegment);
		}
	}
}
