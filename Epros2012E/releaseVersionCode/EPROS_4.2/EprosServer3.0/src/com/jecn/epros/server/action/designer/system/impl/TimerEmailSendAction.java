package com.jecn.epros.server.action.designer.system.impl;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.Ding.DingMessage;
import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekLoginItem;
import com.jecn.epros.server.action.web.login.ad.mulinsen.MunlinsenOASendEnum;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.email.buss.EmailConfigItemBean;
import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.email.buss.SMTPSendEmailBuilder;
import com.jecn.epros.server.service.email.IEmailService;
import com.jecn.epros.server.util.ApplicationContextUtil;

/**
 * 
 * 邮件定时发送
 * 
 * @author hyl
 * 
 */
public class TimerEmailSendAction {

	private final static Log log = LogFactory.getLog(TimerEmailSendAction.class);
	/** 邮件是否正在发送 **/
	private boolean emailIsSending = false;
	/** 邮件队列 **/
	private BlockingQueue<EmailMessageBean> emailQueue = new LinkedBlockingQueue<EmailMessageBean>();

	public void putAll(List<EmailMessageBean> emailList) {
		emailQueue.addAll(emailList);
	}

	public void put(EmailMessageBean emailBean) {
		try {
			emailQueue.put(emailBean);
		} catch (InterruptedException e) {
			log.error("向邮件队列添加数据异常", e);
		}
	}

	public void timerExecte() {
		if (emailIsSending) {
			return;
		}
		try {
			emailIsSending = true;
			if (emailQueue.isEmpty()) {
				return;
			}
			log.info("开始发送邮件");
			// 查询内外网配置项
			EmailConfigItemBean emailConfig = EmailConfigItemBean.getConfigItemBean();
			// 获取配置数据
			emailConfig.initEmailItem();

			// 操作数据库的service
			IEmailService emailService = (IEmailService) ApplicationContextUtil.getContext()
					.getBean("emailServiceImpl");
			EmailMessageBean messageBean = null;
			while (!emailQueue.isEmpty()) {
				messageBean = emailQueue.take();
				if (sendMailSpecial()) {
					sendMailByLogin(emailConfig, messageBean, emailService);
				} else if (JecnContants.otherLoginType == 53) {// 顾家家居 钉钉通知
					dingdingSend(emailService, messageBean);
				} else {
					// 如果邮箱地址和内外网标示有一个为空那么发送失败
					sendEmailCommon(emailConfig, messageBean, emailService);
				}
			}
		} catch (Exception e) {
			log.error("", e);
		} finally {
			emailIsSending = false;
		}
	}

	private void dingdingSend(IEmailService emailService, EmailMessageBean messageBean)
			throws UnsupportedEncodingException, Exception {
		DingMessage dm = new DingMessage();
		com.alibaba.fastjson.JSONObject resultMsg = dm.sendMail(messageBean);
		if (resultMsg != null) {
			// 邮件发送成功更新发送成功标示
			emailService.updateEmailSuccessState(messageBean.getEmailId());
		} else {
			// 邮件发送失败
			emailService.updateEmailSendState(2, messageBean.getEmailId());
		}
	}

	private void sendEmailCommon(EmailConfigItemBean emailConfig, EmailMessageBean messageBean,
			IEmailService emailService) throws Exception {
		SMTPSendEmailBuilder outerBuilder = new SMTPSendEmailBuilder(emailConfig, true);
		SMTPSendEmailBuilder innerBuilder = new SMTPSendEmailBuilder(emailConfig, false);
		// 初始化邮件session是否正确因为如果相关的配置项如果不填写的话是无法初始化session
		boolean outerSessionInitSuccess = outerBuilder.initSession();
		boolean innerSessionInitSuccess = innerBuilder.initSession();
		if (StringUtils.isBlank(messageBean.getUser().getEmail())
				|| StringUtils.isBlank(messageBean.getUser().getEmailType())) {
			changeSendStateToFalse(emailService, messageBean);
			return;
		}
		if ("inner".equals(messageBean.getUser().getEmailType())) {
			if (!innerSessionInitSuccess) {
				changeSendStateToFalse(emailService, messageBean);
			} else {
				// 内网false 外网true
				sendEmail(emailService, innerBuilder, messageBean);
			}
		} else if ("outer".equals(messageBean.getUser().getEmailType())) {
			if (!outerSessionInitSuccess) {
				changeSendStateToFalse(emailService, messageBean);
			} else {
				sendEmail(emailService, outerBuilder, messageBean);
			}
		}
	}

	/**
	 * 特殊登录类型客户，邮件处理方式
	 * 
	 * @date 2018-5-9
	 * @param messageBean
	 * @return
	 */
	private boolean sendMailSpecial() {
		return JecnContants.otherLoginType == 41 || JecnConfigTool.isIflytekLogin();
	}

	/**
	 * 发送邮件特殊处理接口
	 * 
	 * @date 2018-5-9
	 * @param messageBean
	 * @param emailService
	 * @throws Exception
	 */
	private void sendMailByLogin(EmailConfigItemBean emailConfig, EmailMessageBean messageBean,
			IEmailService emailService) throws Exception {
		if (JecnContants.otherLoginType == 41) {
			try {
				String result = MunlinsenOASendEnum.INSTANCE.sendMail(messageBean);
				log.info("result = " + new String(result.getBytes("UTF-8")));
				String status = JSONObject.fromObject(result).getString("status");
				if ("0".equals(status)) {
					log.error("sendMailByLogin error： " + new String(result.getBytes("UTF-8")));
					// 邮件发送失败
					emailService.updateEmailSendState(2, messageBean.getEmailId());
				} else {
					// 邮件发送成功更新发送成功标示
					emailService.updateEmailSuccessState(messageBean.getEmailId());
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		} else if (JecnConfigTool.isIflytekLogin()) {
			JecnUser user = messageBean.getUser();
			if (StringUtils.isBlank(user.getLoginName())) {
				return;
			}
			user.setEmailType("inner");
			user.setEmail(user.getLoginName().toLowerCase() + JecnIflytekLoginItem.getValue("MAIL_SUFFIX"));
			sendEmailCommon(emailConfig, messageBean, emailService);
		}
	}

	public static void main(String[] args) {
		String result = "{\"status\" : 1, \"info\" : \"发送成功\" }";
		String status = JSONObject.fromObject(result).getString("status");
		System.out.println(status);
	}

	/**
	 * 发送状态为失败
	 * 
	 * @param emailService
	 * @param messageBean
	 * @throws Exception
	 */
	private void changeSendStateToFalse(IEmailService emailService, EmailMessageBean messageBean) throws Exception {

		try {
			// 更改状态为发送失败
			emailService.updateEmailSendState(2, messageBean.getEmailId());
		} catch (Exception e) {
			log.error("", e);
		}

	}

	private void sendEmail(IEmailService emailService, SMTPSendEmailBuilder emailBuilder, EmailMessageBean messageBean) {

		try {
			boolean sendSuccess = emailBuilder.sendEmail(messageBean);
			if (sendSuccess) {
				// 邮件发送成功更新发送成功标示
				emailService.updateEmailSuccessState(messageBean.getEmailId());
			} else {
				// 邮件发送失败
				emailService.updateEmailSendState(2, messageBean.getEmailId());
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}
}
