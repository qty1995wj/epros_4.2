package com.jecn.epros.server.dao.popedom;

import java.util.List;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnAccessPermissionsT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IPositionAccessPermissionsDao extends IBaseDao<JecnAccessPermissionsT, Long> {
	/**
	 * @author yxw 2012-10-30
	 * @description:
	 * @param relateId
	 * @param type
	 * @param posIds
	 * @param posType  0:本节点1：包括子节点 
	 * @param userType 0是管理员  新增时设为-1
	 * @param projectId
	 * @param set mysql数据库 存在默认子节点的权限
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, String posIds,int posType,int userType,Long projectId,TreeNodeType nodeType) throws Exception;
	
	
	/**
	 * @author yxw 2012-10-30
	 * @description:
	 * @param relateId
	 * @param type
	 * @param posIds
	 * @param posType  0:本节点1：包括子节点 
	 * @param userType 0是管理员  新增时设为-1
	 * @param projectId
	 * @param set mysql数据库 存在默认子节点的权限
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, List<AccessId> posIds,int posType,int userType,Long projectId,TreeNodeType nodeType) throws Exception;
	
	/**
	 * @author yxw 2012-6-15
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的岗位权限
	 * @param relateId
	 * @param type 0是流程1是文件2是标准3制度
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPositionAccessPermissions(Long relateId,
			int type,boolean isPub) throws Exception;
	
	
	/**
	 * @author xiaobo 
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的岗位组权限
	 * @param relateId
	 * @param type 0是流程1是文件2是标准3制度
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPositionAccessPermissionsGroup(Long relateId,
			int type,boolean isPub) throws Exception;
	
	
	/**
	 * @author yxw 2012-6-15
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的岗位权限
	 * @param relateId
	 * @return
	 * @throws Exception
	 */
	
	/**
	 * @author yxw 2012-10-30
	 * @description:单个流程
	 * @param relateId
	 * @param type  0是流程、1是文件、2是标准、3制度
	 * @param posIds
	 * @param isSave true是新增，false是更新
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, String posIds,boolean isSave) throws Exception;
	/**
	 * @author yxw 2012-10-30
	 * @description:单个流程
	 * @param relateId
	 * @param type  0是流程、1是文件、2是标准、3制度
	 * @param posIds
	 * @param isSave true是新增，false是更新
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, List<AccessId> Ids,boolean isSave) throws Exception;
	
	public void del(Long relateId, int type, String posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception;
	public void del(Long relateId, int type, List<AccessId> posIds, int posType, int userType, Long projectId,
					TreeNodeType nodeType) throws Exception;

	public void add(Long relateId, int type, String posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType)throws Exception;

	public void add(Long relateId, int type, List<AccessId> posIds, int posType, int userType, Long projectId,
			TreeNodeType nodeType)throws Exception;
}
