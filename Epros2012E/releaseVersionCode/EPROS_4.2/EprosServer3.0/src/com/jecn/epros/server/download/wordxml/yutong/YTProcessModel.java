package com.jecn.epros.server.download.wordxml.yutong;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.AntiFakeBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.AntiFake;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 裕同
 * 
 * @author jecn
 * 
 */
public class YTProcessModel extends ProcessFileModel {
	/**
	 * 0name 1posname 2orgname
	 */
	private final Map<Long, List<String[]>> approves = new HashMap<Long, List<String[]>>();

	public YTProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(25F);
		getDocProperty().setNewRowHeight(0.82F);
		getDocProperty().setFlowChartScaleValue(6.2F);
		getDocProperty().setTblTitleShadow(true);
		getDocProperty().setFlowChartDirection(true);
		setDocStyle("、", textTitleStyle());
		setDefAscii("Times New Roman");
		setDefFareast("宋体");
		log.error("裕同未添加操作说明头信息!");
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		createCommonHeader(firstSect);
	}

	private void createCommonHeader(Sect sect) {
		Table table = sect.createHeader(HeaderOrFooterType.odd).createTable(tblStyle(),
				new float[] { 10.28F, 3.68F, 4.81F });

		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		table.createTableRow(Arrays.asList(new String[] { "文件名称： ", "文件编号",
				nullToEmpty(processDownloadBean.getFlowInputNum()) }, new String[] { "", "版    本",
				processDownloadBean.getVersion() }, new String[] { "", "页    码", "" }), pBean, 0.79F);

		table.getRow(0).getCell(0).setRowspan(3);
		table.getRow(0).getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.left, "宋体", Constants.wuhao, false));
		table.getRow(0).getCell(0).getParagraph(0).appendTextRun(processDownloadBean.getFlowName(),
				new FontBean(Constants.sanhao, true));
		Paragraph pageP = table.getRow(2).getCell(2).getParagraph(0);
		pageP.appendCurPageRun();
		pageP.appendTextRun(" OF ");
		pageP.appendTotalPagesRun();
		for (TableRow row : table.getRows()) {
			row.setValign(Valign.center);
		}
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		flowChartSect.setSectBean(sectBean);
		createCommonHeader(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		createCommonHeader(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getTitleSectBean();
		titleSect.setSectBean(sectBean);
		createTitlePage(titleSect);
	}

	private void createTitlePage(Sect sect) {
		// 版本等信息
		Table t = sect.createTable(tblStyle(), new float[] { 11.79F, 7.16F });
		ParagraphBean pBean = new ParagraphBean(Align.left, Constants.xiaosi, false);
		/*
		 * TableRow f1 = t.createTableRow(new String[] {
		 * "ShenZhen YUTO Packaging Technology Co., Ltd\n深圳市裕同包装科技股份有限公司 ",
		 * "Doc No.:\n文件编号:" +
		 * nullToEmpty(processDownloadBean.getFlowInputNum()) }, pBean);
		 */
		String enName = "";
		String name = "";
		// 操作说明头信息
		FlowFileHeadData flowFileHeadData = this.processDownloadBean.getFlowFileHeadData();
		if (flowFileHeadData != null) {
			enName = nullToEmpty(flowFileHeadData.getCompanyEnName());
			name = nullToEmpty(flowFileHeadData.getCompanyName());
		}
		TableRow f1 = t.createTableRow(new String[] { enName + "\n" + name,
				"Doc No.:\n文件编号:" + nullToEmpty(processDownloadBean.getFlowInputNum()) }, pBean);
		TableRow f2 = t.createTableRow(new String[] { "",
				"Version:\n" + "修订版本号: " + nullToEmpty(processDownloadBean.getVersion()) }, pBean);
		TableRow f3 = t.createTableRow(new String[] { "",
				"Effective Date:\n生效日期：" + nullToEmpty(processDownloadBean.getPubDate("yyyy-MM-dd")) }, pBean);
		TableRow f4 = t.createTableRow(new String[] { "", "Page  of  :\n第  1 页 共    页" }, pBean);
		// 判断公司LOGO 不为空 加载
		Paragraph imageP = new Paragraph(" ");
		f1.getCell(0).addParagraph(imageP);
		if (flowFileHeadData != null && StringUtils.isNotBlank(flowFileHeadData.getFilePath())
				&& new File(flowFileHeadData.getFilePath()).exists()) {
			Image logo = new Image(flowFileHeadData.getFilePath());
			logo.setSize(8F, 1.43F);// 图片大小
			imageP.appendGraphRun(logo);
		}
		f1.getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.center, Constants.xiaosi, true).setSpace(0.5F, 0F, new ParagraphLineRule(1.5F,
						LineRuleType.AUTO)));
		f1.getCell(0).getParagraph(1).setParagraphBean(
				new ParagraphBean(Align.center, "隶书", 56, true).setSpace(0F, 0F, new ParagraphLineRule(1F,
						LineRuleType.AUTO)));

		f4.getCell(1).getParagraph(1).setParagraphBean(new ParagraphBean(Align.center, Constants.xiaosi, true));
		// 合并单元格
		f1.getCell(0).setRowspan(4);

		f4.getCell(1).getParagraph(1).appendTotalPagesRun();

		// 文件名称
		t = sect.createTable(getProcessNameTableStyle(), new float[] { 4.16F, 14.79F });
		TableRow f5 = t.createTableRow(new String[] { "Doc. Title:", processDownloadBean.getFlowName() }, pBean);
		TableRow f6 = t.createTableRow(new String[] { "文件名称：", "" }, pBean);
		f5.getCell(1).getParagraph(0).setParagraphBean(new ParagraphBean(Align.center, Constants.erhao, true));
		f5.getCell(1).setvAlign(Valign.center);
		f5.getCell(1).setRowspan(2);

		// 版次标题
		t = sect.createTable(tblStyle(), new float[] { 2.88F, 2.67F, 13.4F });
		TableRow f7 = t.createTableRow(new String[] { "Version\n版次", "Rev. Date\n更改日期", "Revision Resume\n更改之摘要" },
				new ParagraphBean(Align.center, Constants.xiaosi, false));

		// 版本内容记录
		createHistory(sect);
		// 最新的审批人
		createApprovePeople(sect);

	}

	private void createApprovePeople(Sect sect) {
		ParagraphBean pBean = new ParagraphBean(Align.center, Constants.xiaosi, false);
		JecnTaskHistoryNew latestHistoryNew = processDownloadBean.getLatestHistoryNew();
		String authorPeople = latestHistoryNew == null ? "" : nullToEmpty(latestHistoryNew.getDraftPerson());
		// 批准人
		String approvePeople = "";
		// 审核阶段
		String reviewPeople = "";
		if (latestHistoryNew != null) {
			List<JecnTaskHistoryFollow> fllows = latestHistoryNew.getListJecnTaskHistoryFollow();
			Set<Long> draftPeopleIds = new HashSet<Long>();
			Set<Long> reviewPeopleIds = new HashSet<Long>();
			Set<Long> approveIds = new HashSet<Long>();

			for (JecnTaskHistoryFollow p : fllows) {
				Integer state = p.getStageMark();
				if (state == null) {
					continue;
				}
				if (state == 3) {
					p.getId();
					reviewPeopleIds = getIds(p.getApproveId());
					reviewPeople = p.getName();
				} else if (state == 4) {
					approveIds = getIds(p.getApproveId());
					approvePeople = p.getName();
				} else if (state == 0) {
					draftPeopleIds = getIds(p.getApproveId());
					authorPeople = p.getName();
				}
			}

			Set<Long> ids = new HashSet<Long>();
			ids.addAll(draftPeopleIds);
			ids.addAll(reviewPeopleIds);
			ids.addAll(approveIds);
			// 搞这么复杂只是为了少查几遍
			initApproveOrgAndPos(ids);

			if (approveIds.size() > 0) {
				for (Long id : approveIds) {
					approvePeople = getPeopleNames(id);
					break;
				}
			}
			if (reviewPeopleIds.size() > 0) {
				for (Long id : reviewPeopleIds) {
					approvePeople += getPeopleNames(id) + "/";
				}
			}
			if (StringUtils.isNotBlank(approvePeople) && approvePeople.endsWith("/")) {
				approvePeople = approvePeople.substring(0, approvePeople.length() - 1);
			}

			if (approveIds.size() > 0) {
				for (Long id : approveIds) {
					approvePeople = getPeopleNames(id);
					break;
				}
			}
			int reviewCount = reviewPeopleIds.size();
			if (reviewCount == 0) {
				reviewCount = 1;
			}
		}

		Table t = sect.createTable(tblStyle(), new float[] { 2.88F, 2.67F, 3.12F, 4.16F, 3.46F, 2.66F });
		t.createTableRow(new String[] { "Prepared By\n制 订 ", authorPeople, "Reviewed By\n审 核 ", reviewPeople,
				"Approved By\n核 准 ", approvePeople }, pBean);
		for (TableRow row : t.getRows()) {
			row.setHeight(1.5F);
			row.setValign(Valign.center);
		}
	}

	private void createHistory(Sect sect) {
		List<String[]> data = new ArrayList<String[]>();
		ParagraphBean pBean = new ParagraphBean(Align.center, Constants.xiaosi, false);
		if (processDownloadBean.getHistorys() != null && processDownloadBean.getHistorys().size() > 0) {
			Collections.reverse(processDownloadBean.getHistorys());
			for (JecnTaskHistoryNew history : processDownloadBean.getHistorys()) {
				data.add(new String[] { nullToEmpty(history.getVersionId()),
						nullToEmpty(JecnUtil.DateToStr(history.getPublishDate(), "yyyy-MM-dd")),
						nullToEmpty(history.getModifyExplain()) });
			}
		} else {
			data.add(new String[] { "", "", "" });
		}

		Table t = sect.createTable(tblStyle(), new float[] { 2.88F, 2.67F, 13.4F });
		t.createTableRow(data, pBean);
		for (TableRow row : t.getRows()) {
			row.setHeight(1.5F);
			row.setValign(Valign.center);
		}

	}

	private TableBean getProcessNameTableStyle() {
		TableBean tblStyle = tblStyle();
		tblStyle.setBorderInsideH(0F);
		tblStyle.setBorderInsideV(0F);
		return tblStyle;
	}

	private String getPeopleNames(Long id) {
		return approves.get(id).get(0)[0];
	}

	private Set<Long> getIds(String idStr) {
		Set<Long> ids = new HashSet<Long>();

		if (StringUtils.isNotBlank(idStr)) {
			String[] idArray = idStr.split(",");
			for (String id : idArray) {
				if (StringUtils.isNotBlank(id)) {
					ids.add(Long.valueOf(id));
				}
			}
		}
		return ids;

	}

	private void initApproveOrgAndPos(Set<Long> idSet) {
		if (idSet == null || idSet.size() == 0) {
			return;
		}
		String sql = "	select ju.people_id,ju.TRUE_NAME,jfoi.FIGURE_TEXT,jfo.ORG_NAME from JECN_USER ju"
				+ "	left join JECN_USER_POSITION_RELATED jupr on ju.people_id=jupr.people_id"
				+ "	left join JECN_FLOW_ORG_IMAGE jfoi on jfoi.figure_id=jupr.figure_id"
				+ "	left join JECN_FLOW_ORG jfo on jfoi.ORG_ID=jfo.ORG_ID" + "  where ju.people_id in"
				+ JecnCommonSql.getIdsSet(idSet);
		List<Object[]> objs = processDownloadBean.getBaseDao().listNativeSql(sql);
		if (objs.size() > 0) {
			for (Object[] obj : objs) {
				putOrElseUpdate(Long.valueOf(obj[0].toString()), new String[] { JecnUtil.nullToEmpty(obj[1]),
						JecnUtil.nullToEmpty((obj[2])), JecnUtil.nullToEmpty(obj[3]) });
			}
		}
	}

	private void putOrElseUpdate(Long peopleId, String[] info) {
		List<String[]> list = approves.get(peopleId);
		if (list == null) {
			approves.put(peopleId, new ArrayList<String[]>());
		}
		approves.get(peopleId).add(info);
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		word.paragraphStyleKpi(processFileItem, oldRowData);
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	@Override
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		super.a08(processFileItem, rowData);
		// // 标题
		// createTitle(processFileItem);
		// float[] width = null;
		// if (JecnContants.showPosNameBox) {
		// width = new float[] { 3.5F, 3.5F, 7.5F };
		// } else if (!JecnContants.showPosNameBox) {
		// width = new float[] { 5.0F, 9.5F };
		// }
		// List<String[]> rowData2 = new ArrayList<String[]>();
		// // 角色名称
		// String roleName = JecnUtil.getValue("roleName");
		// // 岗位名称
		// String positionName = "对应职位/岗位";
		// // 角色职责
		// String roleResponsibility = "职责";
		// if (JecnContants.showPosNameBox) {
		// // 标题行
		// rowData2.add(0, new String[] { roleName, roleResponsibility,
		// positionName });
		// } else {
		// // 标题行
		// rowData2.add(0, new String[] { roleName, roleResponsibility });
		// }
		// for (String[] str : rowData) {
		// if (JecnContants.showPosNameBox) {
		// rowData2.add(new String[] { str[0], str[2], str[1] });
		// } else {
		// rowData2.add(new String[] { str[0], str[2] });
		// }
		// }
		// Table tbl = createTableItem(processFileItem, width, rowData2);
		// tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(),
		// getDocProperty().isTblTitleShadow(), getDocProperty()
		// .isTblTitleCrossPageBreaks());
	}

	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> dataRow) {
		Table tab = super.createTableItem(processFileItem, width, dataRow);
		tab.setValign(Valign.center);
		return tab;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(1.5F, 1.5F, 1.5F, 1.5F, 1F, 1F);
		sectBean.setSize(29.7F, 21);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getTitleSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(1.5F, 1.5F, 1.5F, 1.5F, 1F, 1F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getCharSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(3.02F, 1F, 1.27F, 1F, 1.25F, 1F);
		sectBean.setSize(29.7F, 21F);
		return sectBean;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.shiyihao, false));
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		/** 默认表格样式 ****/
		TableBean tblStyle = new TableBean();// 
		// 所有边框 1.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.01 厘米
		// tblStyle.setTableLeftInd(0.01F);
		tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, new FontBean("宋体", Constants.shiyihao, false));
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.shiyihao, false));
		// 2字符转厘米 0.4240284
		textContentStyle.setInd(0, 0, 0, 0.8480568f);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(18F, LineRuleType.AT_LEAST));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.shiyihao, true));
		textTitleStyle.setInd(0F, 0, 0F, 0F); // 段落缩进 厘米
		textTitleStyle.setSpace(0F, 0F, new ParagraphLineRule(18F, LineRuleType.AT_LEAST)); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	protected AntiFake getAntiFake() {
		Long flowId = processDownloadBean.getFlowStructure() == null ? processDownloadBean.getFlowStructureT()
				.getFlowId() : processDownloadBean.getFlowStructure().getFlowId();
		String hql = "from JecnFlowStructure where flowId=?";
		IBaseDao baseDao = processDownloadBean.getBaseDao();
		List<Object> objs = baseDao.listHql(hql, flowId);
		String pubName = "已发布";
		if (objs.size() == 0) {
			pubName = "未发布";
		}
		AntiFake fake = AntiFake.createTextAntiFake(pubName, new AntiFakeBean("宋体", Constants.wuhao));
		return fake;
	}

}
