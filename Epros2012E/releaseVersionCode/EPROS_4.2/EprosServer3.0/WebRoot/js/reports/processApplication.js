function processApplicationGrid() {
	processApplicationGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "processApplication.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "processId",
									mapping : "processId"
								}, {
									name : "processName",
									mapping : "processName"
								}, {
									name : "roleNum",
									mapping : "roleNum"
								}, {
									name : "peNum",
									mapping : "peNum"
								}])

			});

	processApplicationGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "processName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processApplicationNameRenderer
			}, {
				header : i_roleNumber,
				dataIndex : "roleNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_usingTheNumber,
				dataIndex : "peNum",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_operation,
				align : "center",
				menuDisabled : true,
				renderer : processApplicationRenderer
			}
	]);
	processApplicationGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processApplicationGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processApplicationGridPanel = new Ext.grid.GridPanel({
				id : "processApplicationGridPanel_id",
				renderTo : 'processApplicationGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-150,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processApplicationGridStore,
				colModel : processApplicationGridColumnModel,
				bbar : processApplicationGridBbar
			})
	reports_index_id='reports_processApplication_id'; 	
}

/**
 * 流程应用人数统计查询
 */
function processApplicationGridSearch(){
	 //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_application_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_application_id").value;
   	 processApplicationGridStore.setBaseParam("processId", processId);
   	 processApplicationGridStore.setBaseParam("resOrgId", resOrgId);
	 processApplicationGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
}

// 流程应用人数统计详情链接
function processApplicationRenderer(value, cellmeta, record){
		return "<div><a target='_blank' class='a-operation' href='" + basePath
				+ "processApplicationDetails.action?processId="
				+ record.data['processId'] + "'>" + i_detail + "</a>";
}


// 流程应用人数统计重置
function processApplicationReset(){
		document.getElementById("processName_application_id").value = "";
		document.getElementById("processId_application_id").value = "-1";
		document.getElementById("orgDutyName_application_id").value = "";
		document.getElementById("orgDutyId_application_id").value = "-1";
}

/**
 * 流程名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processApplicationNameRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['processId']+"' >"+value+"</a>";
}

// 流程应用人数统计下载
function processApplicationDownloadExcel(){
    //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_application_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_application_id").value;
     window.location.href="downloadProcessApplication.action?resOrgId="+resOrgId+"&processId="+processId;
}
