package epros.draw.gui.swing.calendar;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 日期选择
 * 
 * 设置月时请用setMonth方法
 * 
 * @author 2012-07-10
 */
public class JecnCalendarDialog extends JecnDialog {
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 北面面板 */
	private JPanel northPanel = null;
	/** 中间表 */
	private JecnCalendarTable table = null;
	/** 南面面板 */
	private JPanel southPanel = null;

	/** 年內容标签 */
	private JLabel yearTextLabel = null;
	/** 年单位标签 */
	private JLabel yearUnitLabel = null;
	/** 月內容标签 */
	private JLabel monthTextLabel = null;
	/** 年內容标签 */
	private JLabel monthUnitLabel = null;

	/** 降年按钮 */
	private JecnCalendarButton yearDownBtn = null;
	/** 增年按钮 */
	private JecnCalendarButton yearUpBtn = null;
	/** 降月按钮 */
	private JecnCalendarButton monthDownBtn = null;
	/** 增月按钮 */
	private JecnCalendarButton monthUpBtn = null;

	/** 显示今天按钮 */
	private JecnCalendarButton todayBtn = null;

	/** 日历对象 */
	private Calendar calendar = null;

	/** 当前操作年 */
	private int year = -1;
	/** 当前操作月 */
	private int month = -1;
	/** 当前操作天 */
	private int date = -1;

	/** 初始化时此JecnCalendarDialog对象给定年 */
	private int oldYear = -1;
	/** 初始化时此JecnCalendarDialog对象给定月 */
	private int oldMonth = -1;
	/** 初始化时此JecnCalendarDialog对象给定月 */
	private int oldDate = -1;
	/** 星期名称 */
	private String[] tableHeaderNames = null;
	/** 月数值 */
	private String[] months = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };

	/** 表获取焦点标示：true： */
	private boolean flag = true;

	/** 是否确定：true：确定；false：取消 */
	private boolean isOk = true;

	public JecnCalendarDialog() {
		init();
		initData();
	}

	public JecnCalendarDialog(JFrame frame) {
		super(frame);
		init();
		initData();
	}

	public JecnCalendarDialog(Date date) {
		init();
		this.calendar.setTime(date);
		initData();
	}

	/**
	 * 
	 * @param year
	 *            int 年
	 * @param month
	 *            int 月(1~12)
	 * @param date
	 *            int 天(1~31)
	 * 
	 */
	public JecnCalendarDialog(int year, int month, int date) {
		init();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DATE, date);
		initData();
	}

	protected void validateTree() {
		super.validateTree();
		if (flag) {
			table.requestFocusInWindow();
			flag = false;
		}
	}

	private void init() {
		initResource();
		initComponent();
		initLayout();
		initEvent();
	}

	/**
	 * 
	 * 初始化资源文件
	 * 
	 */
	private void initResource() {
		// 周日、周一、周二、周三、周四、周五、周六
		tableHeaderNames = new String[] { JecnResourceUtil.getJecnResourceUtil().getValue("week7"),
				JecnResourceUtil.getJecnResourceUtil().getValue("week1"),
				JecnResourceUtil.getJecnResourceUtil().getValue("week2"),
				JecnResourceUtil.getJecnResourceUtil().getValue("week3"),
				JecnResourceUtil.getJecnResourceUtil().getValue("week4"),
				JecnResourceUtil.getJecnResourceUtil().getValue("week5"),
				JecnResourceUtil.getJecnResourceUtil().getValue("week6") };

		// 月（1~12）
		months = new String[] { JecnResourceUtil.getJecnResourceUtil().getValue("month1"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month2"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month3"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month4"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month5"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month6"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month7"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month8"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month9"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month10"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month11"),
				JecnResourceUtil.getJecnResourceUtil().getValue("month12") };
	}

	private void initComponent() {
		// 日历对象
		this.calendar = Calendar.getInstance();

		// 主界面
		mainPanel = new JPanel();
		// 北面面板
		northPanel = new JPanel();
		// 中间表
		table = new JecnCalendarTable(this);

		// 南面面板
		southPanel = new JPanel();

		// 年內容标签
		yearTextLabel = new JLabel();
		// 年单位标签
		yearUnitLabel = new JLabel();
		// 月內容标签
		monthTextLabel = new JLabel();
		// 月单位标签
		monthUnitLabel = new JLabel();

		// 降月按钮
		monthDownBtn = new JecnCalendarButton(JecnFileUtil
				.getImageIconforPath("/epros/draw/images/swing/calendar/monthDown.gif"));
		// 增月按钮
		monthUpBtn = new JecnCalendarButton(JecnFileUtil
				.getImageIconforPath("/epros/draw/images/swing/calendar/monthUp.gif"));
		// 降年按钮
		yearDownBtn = new JecnCalendarButton(JecnFileUtil
				.getImageIconforPath("/epros/draw/images/swing/calendar/yearDown.gif"));
		// 增年按钮
		yearUpBtn = new JecnCalendarButton(JecnFileUtil
				.getImageIconforPath("/epros/draw/images/swing/calendar/yearUp.gif"));

		// 显示今天按钮
		todayBtn = new JecnCalendarButton(JecnResourceUtil.getJecnResourceUtil().getValue("todayName"));

		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("dialogTitle"));
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);
		this.setLocationByPlatform(true);
		this.setModal(true);
		this.setResizable(false);
		this.setSize(250, 300);
		this.setLocationRelativeTo(null);

		mainPanel.setLayout(new BorderLayout());
		northPanel.setLayout(new GridBagLayout());
		southPanel.setLayout(new GridBagLayout());

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		northPanel.setOpaque(false);
		southPanel.setOpaque(false);

		Dimension northPanelSize = new Dimension(10, 25);
		northPanel.setMinimumSize(northPanelSize);
		northPanel.setPreferredSize(northPanelSize);

		mainPanel.setBorder(null);
		northPanel.setBorder(null);
		table.setBorder(null);
		southPanel.setBorder(null);

		Font font = new Font(Font.DIALOG, Font.BOLD, 15);
		// 年內容标签
		yearTextLabel.setFont(font);
		// 年单位标签
		yearUnitLabel.setFont(font);
		// 月內容标签
		monthTextLabel.setFont(font);
		// 月单位标签
		monthUnitLabel.setFont(font);

		// 赋值
		String yearName = JecnResourceUtil.getJecnResourceUtil().getValue("yearName");
		yearName = (DrawCommon.isNullOrEmtry(yearName)) ? " " : yearName;
		String monthName = JecnResourceUtil.getJecnResourceUtil().getValue("monthName");
		monthName = (DrawCommon.isNullOrEmtry(monthName)) ? " " : monthName;
		yearUnitLabel.setText(yearName);
		monthUnitLabel.setText(monthName);
	}

	private void initLayout() {
		this.getContentPane().add(mainPanel);

		// *************主面板布局*************//
		mainPanel.add(northPanel, BorderLayout.NORTH);
		mainPanel.add(table, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		// *************主面板布局*************//

		GridBagConstraints c;
		Insets insetsLeftFirst = new Insets(0, 10, 0, 2);
		Insets insetsLeftSecond = new Insets(0, 0, 0, 2);

		Insets insetsRightFirst = new Insets(0, 2, 0, 10);
		Insets insetsRightSecond = new Insets(0, 2, 0, 0);
		// *************北面面板布局*************//
		// 降年按钮
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				insetsLeftFirst, 0, 0);
		northPanel.add(yearDownBtn.getJToolBar(), c);
		// 降月按钮
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				insetsLeftSecond, 0, 0);
		northPanel.add(monthDownBtn.getJToolBar(), c);
		// 空闲区域
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		northPanel.add(new JLabel(), c);

		if (DrawCommon.isNullOrEmtryTrim(this.yearUnitLabel.getText())) {// 英文
			// 月內容标签
			c = new GridBagConstraints(3, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(monthTextLabel, c);
			// 年单位标签
			c = new GridBagConstraints(4, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(yearUnitLabel, c);
			// 年內容标签
			c = new GridBagConstraints(5, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(yearTextLabel, c);
			// 月单位标签
			c = new GridBagConstraints(6, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(monthUnitLabel, c);
		} else {
			// 年內容标签
			c = new GridBagConstraints(3, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(yearTextLabel, c);
			// 年单位标签
			c = new GridBagConstraints(4, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(yearUnitLabel, c);
			// 月內容标签
			c = new GridBagConstraints(5, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(monthTextLabel, c);
			// 月单位标签
			c = new GridBagConstraints(6, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets0(), 0, 0);
			northPanel.add(monthUnitLabel, c);
		}

		// 空闲区域
		c = new GridBagConstraints(7, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		northPanel.add(new JLabel(), c);
		// 增月按钮
		c = new GridBagConstraints(8, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				insetsRightSecond, 0, 0);
		northPanel.add(monthUpBtn.getJToolBar(), c);
		// 增年按钮
		c = new GridBagConstraints(9, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				insetsRightFirst, 0, 0);
		northPanel.add(yearUpBtn.getJToolBar(), c);
		// *************北面面板布局*************//

		// *************南面板布局*************//
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		southPanel.add(todayBtn.getJToolBar(), c);
		// 空闲区域
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		southPanel.add(new JLabel(), c);
		// *************南面板布局*************//
	}

	private void initData() {
		oldYear = calendar.get(Calendar.YEAR);
		oldMonth = calendar.get(Calendar.MONTH);
		oldDate = calendar.get(Calendar.DATE);

		this.year = oldYear;
		this.month = oldMonth;
		this.date = oldDate;

		initComsValues();
	}

	private void initComsValues() {
		// 年內容标签
		yearTextLabel.setText(String.valueOf(year));
		// 月內容标签
		monthTextLabel.setText(months[month]);

		refreshTableData();
	}

	/**
	 * 
	 * 初始化事件
	 * 
	 */
	private void initEvent() {
		// 关闭按钮事件
		this.addJecnDialogListener(new JecnDialogListener() {
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				return dialogCloseBeforeProcess(e);
			}

		});

		// 降年按钮
		yearDownBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedYear(e, false);
			}
		});

		// 增年按钮
		yearUpBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedYear(e, true);
			}
		});
		// 降月按钮
		monthDownBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedMonth(e, false);
			}
		});
		// 增月按钮
		monthUpBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedMonth(e, true);
			}
		});

		// 今天按钮
		todayBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedNowDate(e);
			}
		});
	}

	/**
	 * 
	 * 获取yyyy-MM-dd格式字符串
	 * 
	 * @return String yyyy-MM-dd格式字符串
	 */
	public String getCurrSelectedData() {
		return getDataString(calendar.getTime(), "yyyy-MM-dd");
	}

	/**
	 * 
	 * 获取当前选中单元格数据，重新赋值给日历对象，关闭对话框
	 * 
	 * @param rowIndex
	 *            int 选中单元格所在行 -1表示不表行内
	 * @param columnIndex
	 *            int 选中单元格所在列 -1表示不表列内
	 * 
	 */
	void execute(int rowIndex, int columnIndex) {
		String value = table.selectedCellValue(rowIndex, columnIndex);
		if (DrawCommon.isNullOrEmtryTrim(value)) {
			return;
		}
		// 选中天
		this.date = Integer.parseInt(value);

		// 重新给日历赋值
		calendar.set(Calendar.YEAR, JecnCalendarDialog.this.year);
		calendar.set(Calendar.MONTH, JecnCalendarDialog.this.month);
		calendar.set(Calendar.DATE, JecnCalendarDialog.this.date);

		// 关闭对话框
		JecnCalendarDialog.this.setVisible(false);
	}

	/**
	 * 
	 * 加载表数据
	 * 
	 */
	void refreshTableData() {
		JecnCalendarDefaultTableModel tableModel = this.table.getTableModel();
		for (int i = tableModel.getRowCount() - 1; i > 0; i--) {
			tableModel.removeRow(i);
		}

		int maxDate = calendar.getActualMaximum(Calendar.DATE);
		calendar.set(Calendar.DATE, 1);
		int startDay = calendar.get(Calendar.DAY_OF_WEEK);
		for (int i = 0; i < 6; i++) {
			tableModel.addRow(new Object[7]);
		}
		int selectRow = -1;
		int selectColumn = -1;
		for (int i = 0; i < maxDate; i++) {
			int m = (int) ((startDay - 1) / 7) + 1;
			int n = (startDay - 1) % 7;
			tableModel.setValueAt(String.valueOf(i + 1), m, n);
			startDay = startDay + 1;
			if (date != -1 && date == i + 1) {
				selectRow = m;
				selectColumn = n;
				calendar.set(Calendar.DATE, date);
			}
		}
		table.changeSelection(selectRow, selectColumn, false, false);
	}

	/**
	 * 
	 * 当前日期
	 * 
	 */
	int getData() {
		return this.date;
	}

	/**
	 * 
	 * 设置当前日期
	 * 
	 * @param data
	 */
	void setData(int data) {
		this.date = data;
	}

	Calendar getCalendar() {
		return calendar;
	}

	JecnCalendarTable getTable() {
		return table;
	}

	String[] getTableHeaderNames() {
		return tableHeaderNames;
	}

	/**
	 * 
	 * 关闭按钮事件
	 * 
	 * @param e
	 *            JecnDialogEvent
	 * @return boolean true:关闭；false：不关闭
	 */
	private boolean dialogCloseBeforeProcess(JecnDialogEvent e) {
		this.year = oldYear;
		this.month = oldMonth;
		this.date = oldDate;

		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DATE, date);

		isOk = false;

		return true;
	}

	/**
	 * 
	 * 年按钮事件
	 * 
	 * @param e
	 *            ActionEvent 按钮事件
	 * @param isUp
	 *            boolean true：+1年；false：-1年
	 */
	private void actionPerformedYear(ActionEvent e, boolean isUp) {
		if (isUp) {
			year = year + 1;
		} else {
			year = year - 1;
		}
		calendar.set(Calendar.YEAR, year);
		initComsValues();
	}

	/**
	 * 
	 * 月按钮事件
	 * 
	 * @param e
	 *            ActionEvent 按钮事件
	 * @param isUp
	 *            boolean true：+1月；false：-1月
	 */
	private void actionPerformedMonth(ActionEvent e, boolean isUp) {
		int tmpMonth = 0;
		if (isUp) {
			tmpMonth = month + 1;
		} else {
			tmpMonth = month - 1;
		}
		if (tmpMonth < 0) {
			year = year - 1;
			tmpMonth = 11;
		} else if (tmpMonth >= months.length) {
			year = year + 1;
			tmpMonth = 0;
		}
		month = tmpMonth;

		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);

		// 重新赋值
		initComsValues();
	}

	/**
	 * 
	 * 年按钮事件
	 * 
	 * @param e
	 *            ActionEvent 按钮事件
	 * @param isUp
	 *            boolean true：+1年；false：-1年
	 */
	private void actionPerformedNowDate(ActionEvent e) {
		// 今天日期
		Calendar nowCalendar = Calendar.getInstance();

		year = nowCalendar.get(Calendar.YEAR);
		month = nowCalendar.get(Calendar.MONTH);
		date = nowCalendar.get(Calendar.DATE);

		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DATE, date);

		initComsValues();
	}

	/**
	 * 
	 * 通过给定时间获取给定格式时间字符串
	 * 
	 * @param date
	 *            Date 给定时间
	 * @param format
	 *            String 给定时间格式
	 * @return String 时间字符串
	 */
	private String getDataString(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * 
	 * 是否点击确定
	 * 
	 * @return boolean true：点击有效值获取点击enter键获取有效日期标示 false：点击关闭按钮，日期初始化日期
	 */
	public boolean isOk() {
		return isOk;
	}
}
