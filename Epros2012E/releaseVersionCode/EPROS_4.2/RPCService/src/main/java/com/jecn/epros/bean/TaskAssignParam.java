package com.jecn.epros.bean;

public class TaskAssignParam extends BaseTaskParam {
	
	private String userAssignedId;

	public String getUserAssignedId() {
		return userAssignedId;
	}

	public void setUserAssignedId(String userAssignedId) {
		this.userAssignedId = userAssignedId;
	}
}
