package com.jecn.epros.bean.system;

import java.io.Serializable;

public class SyncConfig implements Serializable {
    /**
     * 同步时间 (格式：13:01)
     **/
    private String startTime;
    /**
     * 同步时间间隔天
     **/
    private Integer intervalDay;
    /**
     * 是否自动同步 1自动，0非自动
     **/
    private Integer isAuto;
    /**
     * 同步类型
     **/
    private int syncType;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Integer getIntervalDay() {
        return intervalDay;
    }

    public void setIntervalDay(Integer intervalDay) {
        this.intervalDay = intervalDay;
    }

    public Integer getIsAuto() {
        return isAuto;
    }

    public void setIsAuto(Integer isAuto) {
        this.isAuto = isAuto;
    }

    public int getSyncType() {
        return syncType;
    }

    public void setSyncType(int syncType) {
        this.syncType = syncType;
    }
}
