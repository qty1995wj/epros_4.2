package com.jecn.epros.domain.propose;

public class ProposeTotalChartData {
    /**
     * 搜索条件 组织、岗位或人员
     */
    private Long id;
    private String name;
    /**
     * 按照月份选择
     */
    private String dateStr;
    /**
     * 统计月份合理化建议个数
     */
    private int totalPropose;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public int getTotalPropose() {
        return totalPropose;
    }

    public void setTotalPropose(int totalPropose) {
        this.totalPropose = totalPropose;
    }
}
