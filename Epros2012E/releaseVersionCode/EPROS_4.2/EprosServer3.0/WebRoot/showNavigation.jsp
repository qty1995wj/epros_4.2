<table class="gradient" align="right">
          <script>
              var basePath = "${basePath}";
          </script>
          <tr class="gradient" style="color:gray;font-size: 12px;">
              <s:if test="#session.webLoginBean.isAdmin || #session.webLoginBean.isViewAdmin">
	              <s:if test="#application.versionType">
	               <td>&nbsp;<a class="table" style="cursor: pointer;" onclick="reportsHomePage()"><s:text name="homePage"/></a></td>
	               <td>|<a class="table" style="cursor: pointer;" onclick="reportsPersonallyFunction()"><s:text name="myhome"/></a></td>
	             </s:if>
	             <s:else>
	                <td>&nbsp;<a class="table" style="cursor: pointer;" onclick="reportsPersonallyFunction()"><s:text name="myhome"/></a></td>
	             </s:else>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsFlowStruct()"><s:text name="processSystem"/></a></td>
	              <s:if test="#application.versionType">
		              <td>|<a class="table" style="cursor: pointer;" onclick="reportsRuleSysNav()"><s:text name="guide.RuleSystem"/></a></td>
		              <td>|<a class="table" style="cursor: pointer;" onclick="reportsStandardSysNav()"><s:text name="standardSystem"/></a></td>
		             <td>|<a class="table" style="cursor: pointer;" onclick="reportsRiskSystemManage()"><s:text name="riskSys"/></a></td>
	              </s:if>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsFileSysNav()"><s:text name="guide.Repository"/></a></td>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsOrgAndPostion()"><s:text name="organizationAndTheJob"/></a></td>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsTaskDialog()"><s:text name="taskManagement"/></a></td>
	              <s:if test="#application.versionType">
	                 <td>|<a class="table" style="cursor: pointer;" onclick="reportsRationalizationProposals()"><s:text name="rationalizationproposals"/></a></td>
	              </s:if>
	              <s:if test="#application.versionType">
	              	  <td>|<a class="table" style="cursor: pointer;" onclick="reportsReportDialog()"><s:text name="theReport"/></a></td>
	              </s:if>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsSystemManager()"><s:text name="systemManagement"/></a></td>
             </s:if>
             <s:else>
                  <td><a class="table" style="cursor: pointer;" onclick="reportsPersonallyFunction()"><s:text name="myhome"/></a></td>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsFlowStruct()"><s:text name="processSystem"/></a></td>
	              <s:if test="#application.versionType">
		                <td>|<a class="table" style="cursor: pointer;" onclick="reportsRuleSysNav()"><s:text name="guide.RuleSystem"/></a></td>
		                <td>|<a class="table" style="cursor: pointer;" onclick="reportsStandardSysNav()"><s:text name="standardSystem"/></a></td>
	              </s:if>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsFileSysNav()"><s:text name="guide.Repository"/></a></td>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsOrgAndPostion()"><s:text name="organizationAndTheJob"/></a></td>
	              <td>|<a class="table" style="cursor: pointer;" onclick="reportsTaskDialog()"><s:text name="taskManagement"/></a></td>
	              <s:if test="#application.versionType">
	                 <td>|<a class="table" style="cursor: pointer;" onclick="reportsRationalizationProposals()"><s:text name="rationalizationproposals"/></a></td>
	              </s:if>
             </s:else>
          </tr>
</table>