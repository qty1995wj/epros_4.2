package epros.designer.gui.popedom.organization;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.integration.MultiLineCellRenderer;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * @author yxw 2012-9-4
 * @description：部门选择
 */
public class OrgChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(OrgChooseDialog.class);
	
	public OrgChooseDialog(List<JecnTreeBean> list) {
		super(list, 0);
		this.setTitle(JecnProperties.getValue("deptChoose"));
	}

	public OrgChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 0, jecnDialog);
		this.setTitle(JecnProperties.getValue("deptChoose"));
	}

	public OrgChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog,boolean isShowBut) {
		super(list, 0, jecnDialog,isShowBut);
		this.setTitle(JecnProperties.getValue("deptChoose"));
	}

	public OrgChooseDialog(List<JecnTreeBean> list,boolean isShowBut) {
		super(list, 0,isShowBut);
		this.setTitle(JecnProperties.getValue("deptChoose"));
	}


	public OrgChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog, Long startId) {
		super(list, 0, jecnDialog, startId);
		this.setTitle(JecnProperties.getValue("deptChoose"));
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("deptNameC");
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyOrgPersonSearchTree();
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("deptName"));// 部门名称
		return title;
	}

	/***
	 * 批量添加面板
	 * 
	 * @param c
	 * @param insets
	 */
	protected void addPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(addAllPanel, c);
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getOrganizationAction().searchByName(name, JecnConstants.projectId);
		} catch (Exception e) {
			log.error("OrgChooseDialog searchByName is error！", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null && !list.isEmpty()) {
			getParentsName(list); // 根据部门IDS 获取父级目录
			for (JecnTreeBean jecnTreeBean : list) {
				if (jecnTreeBean.getId() != null || jecnTreeBean.getName() != null) {
					Vector<String> data = new Vector<String>();
					data.add(jecnTreeBean.getId().toString());
					data.add(jecnTreeBean.getName()
							+ (jecnTreeBean.getPname() == null ? " " : "(" + jecnTreeBean.getPname() + ")"));
					content.add(data);
				}
			}
		}
		return content;
	}

	private void getParentsName(List<JecnTreeBean> list) {
		try {
			List childrenIds = new ArrayList<Long>();
			Map<Long, String> ParentsNames = new HashMap<Long, String>();
			for (JecnTreeBean jecnTreeBean : list) {// 获取要查询父节点的 ID
				childrenIds.add(jecnTreeBean.getId());
			}
			ParentsNames = ConnectionPool.getProcessAction().getNodeParentById(childrenIds, false,
					TreeNodeType.organization);
			for (JecnTreeBean jecnTreeBean : list) {
				jecnTreeBean.setPname(ParentsNames.get(jecnTreeBean.getId()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:双击Table
	 */
	protected void searchTablemousePressed(MouseEvent evt) {
		if (evt.getClickCount() == 2) {
			if (this.selectMutil && isKeyCtrlorShift) {
				int[] rows = searchTable.getSelectedRows();
				for (int row : rows) {
					singleSearchTable(row);
				}
			} else {
				int row = searchTable.getSelectedRow();
				singleSearchTable(row);
			}

		}
		if (evt.getClickCount() == 1) {
			int row = searchTable.getSelectedRow();
			int newRowHeight = searchTable.getRowHeight(row);
			if (row != -1) {
				if (oldRowHeight >= newRowHeight) {
					MultiLineCellRenderer multiLineCellRenderer = new MultiLineCellRenderer(row, 1, 0);
					multiLineCellRenderer.setToolTipText(searchTable.getValueAt(row, 1).toString());
					searchTable.setDefaultRenderer(Object.class, multiLineCellRenderer);
				} else if (oldRowHeight < newRowHeight) {
					// 选中后高度设置
					if (newRowHeight != oldRowHeight) {
						searchTable.setRowHeight(row, oldRowHeight);
						MultiLineCellRenderer multiLineCellRenderer = new MultiLineCellRenderer(row, 1, 1);
						multiLineCellRenderer.setToolTipText(searchTable.getValueAt(row, 1).toString());
						searchTable.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 1, 1));
					}
				}
			}
		}
	}
}
