package com.jecn.epros.bean.authetication;

import java.io.Serializable;

public class TokenAnalysis implements Serializable {

    private TokenStatus status;

    private String username;

    private String token;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public TokenStatus getStatus() {
        return status;
    }

    public void setStatus(TokenStatus status) {
        this.status = status;
    }

    public boolean isValid() {
        return this.status == TokenStatus.VALID;
    }

    public enum TokenStatus implements Serializable {
        VALID(0, "合法值"),
        NULL(1, "空值"),
        INVALID(2, "无效值"),
        ILLEGAL(3, "非法值");

        private int status;
        private String message;

        TokenStatus(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}