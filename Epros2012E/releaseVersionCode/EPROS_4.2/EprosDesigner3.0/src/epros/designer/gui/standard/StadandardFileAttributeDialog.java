package epros.designer.gui.standard;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.standard.StandardBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

public class StadandardFileAttributeDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();// 510, 660

	/** center面板 */
	private JecnPanel centerPanel = new JecnPanel();// /510, 505

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();// 500, 30

	/** 标准文件名称Lab */
	private JLabel standardFileNameLab = new JLabel(JecnProperties.getValue("standardFileC"));
	/** 标准文件名称Field */
	JTextField standardFileNameField = new JTextField();// 170, 20

	/** 标准文件编号Lab */
	private JLabel standardFileNumLab = new JLabel(JecnProperties.getValue("standardFileNumberC"));
	/** 标准文件编号Field */
	JTextField standardFileNumField = new JTextField();// 170, 20

	/** 查阅权限 */
	private AccessAuthorityCommon accessAuthorityCommon = null;

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 面板高 */
	private int dialogHeigh = 480;

	private JecnTreeNode selectTreeNode;

	public StadandardFileAttributeDialog(JecnTreeNode selectTreeNode) {
		this.selectTreeNode = selectTreeNode;
		this.setTitle(JecnProperties.getValue("stadandardFileAttribute"));
		this.setSize(getWidthMax(), dialogHeigh);
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		initLayout();
		initData();
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		cancelBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	private void okButtonAction() {
		if (accessAuthorityCommon.isUpdate()) {// 没有更新
			try {
				ConnectionPool.getOrganizationAction().saveOrUpdateAccessPermissions(
						selectTreeNode.getJecnTreeBean().getId(), 2,
						selectTreeNode.getJecnTreeBean().getTreeNodeType(), accessAuthorityCommon.getPublicStatic(), 0,
						accessAuthorityCommon.getOrgIds(), 0, accessAuthorityCommon.getPosIds(), 0, 0,
						JecnConstants.projectId, accessAuthorityCommon.getPosGroupIds(), 0, 0);
				this.dispose();
			} catch (Exception e) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("updateAccess"));
				log.error(JecnProperties.getValue("updateAccess"), e);
			}
		}
		this.setVisible(false);
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!accessAuthorityCommon.isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButtonAction();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private void initData() {
		try {
			StandardBean standardBean = ConnectionPool.getStandardAction().getStandardBean(
					selectTreeNode.getJecnTreeBean().getId());
			if (standardBean.getRelationId() != null) {
				JecnFileBeanT jecnFileBean = ConnectionPool.getFileAction()
						.getFilBeanById(standardBean.getRelationId());
				if (jecnFileBean != null) {
					standardFileNameField.setText(selectTreeNode.getJecnTreeBean().getName());
					if (jecnFileBean.getDocId() != null) {
						standardFileNumField.setText(jecnFileBean.getDocId());
					}
				}
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("standardFileAttributeInitializationError"));
			log.error(JecnProperties.getValue("standardFileAttributeInitializationError"), e);
		}

	}

	/**
	 * @author yxw 2012-6-28
	 * @description:布局
	 */
	protected void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 3, 3, 3);
		// centerPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		mainPanel.add(centerPanel, c);
		centerPanel.setLayout(new GridBagLayout());
		int row = 0;
		// 流程名称
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		centerPanel.add(standardFileNameLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(standardFileNameField, c);
		c = new GridBagConstraints(3, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		standardFileNameField.setEditable(false);
		row++;

		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		centerPanel.add(standardFileNumLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(standardFileNumField, c);
		standardFileNumField.setEditable(false);
		row++;
		accessAuthorityCommon = new AccessAuthorityCommon(row, centerPanel, insets, selectTreeNode.getJecnTreeBean()
				.getId(), 2, this,false,false);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				new Insets(0, 3, 0, 3), 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

}
