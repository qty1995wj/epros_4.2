package epros.designer.gui.task.config;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 任务审批环节面板 包含table面板和按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class TaskConfigEditPanel extends JecnPanel implements ActionListener {

	/** 主导发布类型面板 */
	private JecnPanel otherPanel = null;
	/** 任务审批配置table项 **/
	private TaskConfigTableScrollPane taskTablePanel;
	/** 按钮面板 **/
	private TaskConfigRightButtonPanel buttonPanel;
	/** 任务审批table和按钮面板panel **/
	private JecnPanel centerPanel;
	/** 主导发布类型:task_whoConntrolLead */
	private JLabel docPubTypeLabel = null;
	/** 文控主导审核发布 */
	private JRadioButton docControlLeadRadioButton = null;
	/** 拟稿人主导发布 */
	private JRadioButton docNotControlLeadRadioButton = null;
	/** 主导审批类型数据 **/
	private TaskConfigItem approveType = null;

//	private String templetId;
	private int taskType;
	private TaskConfigDialog dialog;
	private boolean isConfigItemReadOnly;

	public TaskConfigEditPanel(TaskConfigDialog dialog, boolean isEidt) {
		this.dialog = dialog;
		this.taskType = dialog.getTaskType();
		this.isConfigItemReadOnly = isEidt;
		iniComponents();
		initLayout();
		initData();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {

		centerPanel = new JecnPanel();
		// 其他项面板
		otherPanel = new JecnPanel();

		taskTablePanel = new TaskConfigTableScrollPane(dialog);

		buttonPanel = new TaskConfigRightButtonPanel(dialog);
		buttonPanel.setVisible(isConfigItemReadOnly);

		// 主导发布类型:
		docPubTypeLabel = new JLabel(JecnProperties.getValue("task_whoConntrolLead"));
		// 文控主导审核发布
		docControlLeadRadioButton = new JRadioButton(JecnProperties.getValue("task_docControllead"));
		// 非文控主导审核发布
		docNotControlLeadRadioButton = new JRadioButton(JecnProperties.getValue("task_notDocControlLead"));

		// 其他项面板
		otherPanel.setOpaque(false);
		otherPanel.setBorder(null);

		// 添加按钮组
		ButtonGroup group = new ButtonGroup();
		group.add(docControlLeadRadioButton);
		group.add(docNotControlLeadRadioButton);
		docControlLeadRadioButton.setEnabled(isConfigItemReadOnly);
		docNotControlLeadRadioButton.setEnabled(isConfigItemReadOnly);

		// 事件
		docControlLeadRadioButton.addActionListener(this);
		docNotControlLeadRadioButton.addActionListener(this);
		// 命令按钮名称
		docControlLeadRadioButton.setActionCommand("docControlLeadRadioButton");
		docNotControlLeadRadioButton.setActionCommand("docNotControlLeadRadioButton");

		docControlLeadRadioButton.setOpaque(false);
		docNotControlLeadRadioButton.setOpaque(false);
		// 默认
		docNotControlLeadRadioButton.setSelected(true);
		
/*		if(JecnConfigTool.isHKWSOperType()){
			otherPanel.setVisible(false);
		}
*/
	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {

		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(taskTablePanel, BorderLayout.CENTER);
		centerPanel.add(otherPanel, BorderLayout.SOUTH);

		this.setLayout(new BorderLayout());
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.EAST);

		Insets insets = new Insets(5, 5, 5, 5);

		// 流程责任人:标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docPubTypeLabel, c);
		// 流程责任人:人员
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docControlLeadRadioButton, c);
		// 流程责任人: 岗位
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docNotControlLeadRadioButton, c);
		// 空闲区域
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		otherPanel.add(new JLabel(), c);
	}

	public void reSetApproveItems() {

		reSetApprovteData();
		initData();
	}

	private void reSetApprovteData() {
		List<TaskConfigItem> configItems = dialog.getConfigs();
		for (TaskConfigItem config : configItems) {
			config.setName(config.getDefaultName());
			config.setEnName(config.getEnName());
			config.setIsEmpty(config.getIsDefaultEmpty());
			config.setSort(config.getDefaultSort());
			config.setValue(config.getDefaultValue());
			config.getUsers().clear();
		}
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 * @throws Exception
	 */
	public void initData() {

		// 主导审批类型
		approveType = taskTablePanel.getApproveType();
		if (approveType != null) {
			if (approveType.getValue().equals("0")) {
				docNotControlLeadRadioButton.setSelected(true);
			} else if (approveType.getValue().equals("1")) {
				docControlLeadRadioButton.setSelected(true);
			}
		}

		// 加载任务滚动面板数据
		List<TaskConfigItem> configs = taskTablePanel.getShowConfigItems(true);
		taskTablePanel.initData(configs);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton) {
			JRadioButton btn = (JRadioButton) e.getSource();
			if (btn.isSelected()) {// 是否选中
				// 主导发布类型：1：文控审核主导审批发布；0：正常审批发布（拟稿人主导发布）（默认）
				if ("docControlLeadRadioButton".equals(btn.getActionCommand())) {// 文控主导审核发布
					approveType.setValue("1");
					// 校验
					check();
				} else if ("docNotControlLeadRadioButton".equals(btn.getActionCommand())) {// 拟稿人主导发布
					approveType.setValue("0");
					// 校验
					check();
				}
			}
		}
	}

	/**
	 * 
	 * 审批环节不能为空；
	 * 
	 * 文控主导审核发布为文控主导，审批环节必须有文控审核人且必填
	 * 
	 * 审批环节存在评审人会审时，评审人会审后必须存在其他审核人，当评审人必填，其后其他审核人至少有一个必填
	 * 
	 * @return boolean 校验成功返回true；校验失败返回false
	 */
	public boolean check() {
		List<TaskConfigItem> showItemBeanList = taskTablePanel.getShowConfigItems(true);
		if (showItemBeanList == null || showItemBeanList.size() == 0) { //
			// 3.审批环节不能为空
			dialog.updateBtnAndInfo(JecnProperties.getValue("taskApprRowNull"));
			return false;
		}

		if (docControlLeadRadioButton.isSelected()) {//
			// 1.文控主导审核发布为文控主导，审批环节必须有文控审核人
			// 文控审核人
			String taskAppDocCtrlUser = ConfigItemPartMapMark.taskAppDocCtrlUser.toString();

			// 存在文控审核人且必填
			TaskConfigItem docCtrlItemBean = getConfigItemBeanNotEmptyByMark(showItemBeanList, taskAppDocCtrlUser);
			if (docCtrlItemBean == null) {
				dialog.updateBtnAndInfo(JecnProperties.getValue("taskApprExistsNotEmptyApprUser"));
				return false;
			}
		}

		// 评审人会审
		String taskAppReviewer = ConfigItemPartMapMark.taskAppReviewer.toString();

		// 是否存在评审人且其之后存在审核人
		boolean ret = getNextItemBeanByMark(showItemBeanList, taskAppReviewer);

		if (!ret) {// 不存在/存在且其后不存在审核人
			dialog.updateBtnAndInfo(JecnProperties.getValue("taskApprOtherUser"));
			return false;
		}

		// 评审人必填，其后其他审核人至少有一个必填
		ret = getNextItemBeanNotEmptyByMark(showItemBeanList, taskAppReviewer);
		if (!ret) {// 不存在/存在且其后不存在审核人
			dialog.updateBtnAndInfo(JecnProperties.getValue("taskApprOtherNotEmptyUser"));
			return false;
		}

		dialog.updateBtnAndInfo(null);
		return true;
	}

	/**
	 * 
	 * 判断给定标识是否在集合中是否存在且必填，存在且必填时判断其后是否至少存在一个记录B且必填，满足条件返回true：不满足条件返回false
	 * 
	 * @param showITemBeanList
	 *            List<JecnConfigItemBean> 被查找的集合
	 * @param mark
	 *            String 唯一标识
	 * @return boolean 对象存在且其对象之后还存在对象返回true；其他情况返回false
	 * 
	 */
	public boolean getNextItemBeanNotEmptyByMark(List<TaskConfigItem> showITemBeanList, String mark) {
		if (showITemBeanList == null || showITemBeanList.size() == 0 || DrawCommon.isNullOrEmtryTrim(mark)) {
			return false;
		}

		// 评审人会审必填标识：true：必填；false：非必填
		boolean paraMarkEmptyFlag = false;
		// 给定的审批环节存在标识：true：存在；false：不存在
		boolean paraMarkExistsFlag = false;

		for (TaskConfigItem itemBean : showITemBeanList) {
			if (paraMarkExistsFlag) {// 存在
				if (paraMarkEmptyFlag) {// 必填
					if (itemBean.getIsEmpty() == 1) {// 评审人会审存在且必填，其后至少要存在一个审核人且必填
						return true;
					}
				} else {
					// 给定审批环节后是否存在审批环节：true：存在；false：不存在
					return true;
				}
			}
			if (mark.equals(itemBean.getMark())) {
				if (itemBean.getIsEmpty() == 1) {// true：必填；false：非必填
					paraMarkEmptyFlag = true;
				}
				paraMarkExistsFlag = true;
			}
		}

		if (!paraMarkExistsFlag) {// 没有评审人会审，返回true
			return true;
		}

		return false;
	}

	/**
	 * 
	 * 判断给定标识是否在集合中是否存在，存在时判断其后是否至少存在一个记录B，存在返回true：不存在返回false
	 * 
	 * @param showITemBeanList
	 *            List<JecnConfigItemBean> 被查找的集合
	 * @param mark
	 *            String 唯一标识
	 * @return boolean 对象存在且其对象之后还存在对象返回true；其他情况返回false
	 * 
	 */
	public boolean getNextItemBeanByMark(List<TaskConfigItem> showITemBeanList, String mark) {
		if (showITemBeanList == null || showITemBeanList.size() == 0 || DrawCommon.isNullOrEmtryTrim(mark)) {
			return false;
		}

		// 给定的审批环节存在标识：true：存在；false：不存在
		boolean paraMarkExistsFlag = false;
		for (TaskConfigItem itemBean : showITemBeanList) {
			if (paraMarkExistsFlag) {
				// 给定审批环节后是否存在审批环节：true：存在；false：不存在
				return true;
			}
			if (mark.equals(itemBean.getMark())) {
				paraMarkExistsFlag = true;
			}
		}
		if (!paraMarkExistsFlag) {// 没有评审人会审，返回true
			return true;
		}

		return false;
	}

	/**
	 * 
	 * 判断给定标识是否在集合中是否存在且必填，存在且必填时返回JecnConfigItemBean对象；否则返回NULL
	 * 
	 * 
	 * @param showITemBeanList
	 *            List<JecnConfigItemBean>
	 * @param mark
	 *            mark
	 * @return JecnConfigItemBean JecnConfigItemBean对象或NULL
	 */
	private TaskConfigItem getConfigItemBeanNotEmptyByMark(List<TaskConfigItem> showITemBeanList, String mark) {
		if (showITemBeanList == null || showITemBeanList.size() == 0 || DrawCommon.isNullOrEmtryTrim(mark)) {
			return null;
		}

		for (TaskConfigItem itemBean : showITemBeanList) {
			if (mark.equals(itemBean.getMark())) {// 是否给定的唯一标识
				if (itemBean.getIsEmpty() == 1) {// true：必填；false：非必填
					return itemBean;
				}
			}
		}
		return null;
	}

	public JecnPanel getOtherPanel() {
		return otherPanel;
	}

	public void setOtherPanel(JecnPanel otherPanel) {
		this.otherPanel = otherPanel;
	}

	public TaskConfigTableScrollPane getTaskTablePanel() {
		return taskTablePanel;
	}

	public void setTaskTablePanel(TaskConfigTableScrollPane taskTablePanel) {
		this.taskTablePanel = taskTablePanel;
	}

	public TaskConfigRightButtonPanel getButtonPanel() {
		return buttonPanel;
	}

	public void setButtonPanel(TaskConfigRightButtonPanel buttonPanel) {
		this.buttonPanel = buttonPanel;
	}

	public JecnPanel getCenterPanel() {
		return centerPanel;
	}

	public void setCenterPanel(JecnPanel centerPanel) {
		this.centerPanel = centerPanel;
	}

	public JLabel getDocPubTypeLabel() {
		return docPubTypeLabel;
	}

	public void setDocPubTypeLabel(JLabel docPubTypeLabel) {
		this.docPubTypeLabel = docPubTypeLabel;
	}

	public JRadioButton getDocControlLeadRadioButton() {
		return docControlLeadRadioButton;
	}

	public void setDocControlLeadRadioButton(JRadioButton docControlLeadRadioButton) {
		this.docControlLeadRadioButton = docControlLeadRadioButton;
	}

	public JRadioButton getDocNotControlLeadRadioButton() {
		return docNotControlLeadRadioButton;
	}

	public void setDocNotControlLeadRadioButton(JRadioButton docNotControlLeadRadioButton) {
		this.docNotControlLeadRadioButton = docNotControlLeadRadioButton;
	}

	public TaskConfigItem getApproveType() {
		return approveType;
	}

	public void setApproveType(TaskConfigItem approveType) {
		this.approveType = approveType;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public TaskConfigDialog getDialog() {
		return dialog;
	}

	public void setDialog(TaskConfigDialog dialog) {
		this.dialog = dialog;
	}

}
