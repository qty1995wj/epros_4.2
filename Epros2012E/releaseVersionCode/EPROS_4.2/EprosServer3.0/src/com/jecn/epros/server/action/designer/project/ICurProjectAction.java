package com.jecn.epros.server.action.designer.project;

import com.jecn.epros.server.bean.project.JecnCurProject;

/***
 * 设置主项目管理Action接口
 * @author 2012-05-23
 *
 */
public interface ICurProjectAction {
	/**
	 * 设置主项目
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean updateCurProject(Long projectId) throws Exception;
	/**
	 * 根据项目Id查询主项目表信息
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public JecnCurProject getJecnCurProjectById() throws Exception;		

}
