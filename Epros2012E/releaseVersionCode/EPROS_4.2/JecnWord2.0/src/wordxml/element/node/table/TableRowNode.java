package wordxml.element.node.table;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;
import wordxml.util.Tools;
import wordxml.util.Unit;

public class TableRowNode {
	private static final Log log = LogFactory.getLog(TableRowNode.class);

	/**
	 * 创建表格行
	 * 
	 * @param tbl
	 * @param row
	 */
	public static void createTblRow(Element tbl, TableRow row, boolean canBreak) {
		if (tbl == null || row == null) {
			log.info("TableRowNode class createTblRow Argument is not valid");
		}
		Element tr = tbl.addElement("w:tr");
		// 设置行属性
		Element trPr = tr.addElement("w:trPr");
		if (!canBreak) {
			// 取消跨页断行
			trPr.addElement("w:cantSplit");
		}
		if (!Tools.isBlank(Unit.convertCmToTwip(row.getHeight()))) {
			trPr.addElement("w:trHeight").addAttribute("w:val", Unit.convertCmToTwip(row.getHeight()));
		}
		if (row.isHeader()) {
			trPr.addElement("w:tblHeader");
		}

		for (TableCell tableCell : row.getCells()) {
			TableCellNode.createTblCell(tr, tableCell);
		}
	}

}
