package epros.designer.gui.rule;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.JecnRuleData;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.process.flow.file.FlowRelatedRiskPanel;
import epros.designer.gui.process.flow.file.FlowRelatedRulePanel;
import epros.designer.gui.process.flow.file.FlowRelatedStandardPanel;
import epros.designer.gui.process.flow.file.FlowRelatedStandardizedFilePanel;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * 编辑制度信息
 * 
 * @author 2012-06-07
 * 
 */
public class EditRuleInfoDialog extends JecnDialog {
	protected Logger log = Logger.getLogger(EditRuleInfoDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	/** 制度操作说明面板Operating Instructions */
	protected JScrollPane operationPanel = new JScrollPane();
	/** 制度基本信息面板 */
	protected JScrollPane baseInfoPanel = new JScrollPane();
	/** JTabbedPane */
	protected JTabbedPane tabPanel = new JTabbedPane();
	/** 按钮面板 */
	protected JPanel buttonPanel = new JecnPanel(600, 30);

	/** 确定 */
	protected JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消 */
	protected JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 验证提示 */
	protected JLabel verfyLab = new JLabel();
	/** 制度操作说明 */
	protected RuleOperatePanel ruleOperatePanel = null;
	/** 制度基本信息 */
	protected RuleBaseInfoPanel ruleBaseInfoPanel = null;
	/** 关联风险 */
	protected FlowRelatedRiskPanel riskPanel = null;
	/** 相关标准化文件 */
	protected FlowRelatedStandardizedFilePanel relatedStandardizedFilePanel = null;

	/** 相关制度 */
	protected FlowRelatedRulePanel relatedRule = null;
	List<JecnTreeBean> relatedRules = null;
	protected JScrollPane ruleScrollPanel;

	protected JScrollPane standardizedFileScrollPanel;
	/** 风险滚动面板 */
	protected JScrollPane riskScrollPanel;
	/** 相关标准 */
	protected FlowRelatedStandardPanel refStandardPanel = null;
	/** 相关标准 滚动面板 */
	protected JScrollPane standardScrollPanel;

	protected Long ruleId = null;

	private TreeNodeType treeNodeType;

	// 制度标准化文件
	protected List<JecnTreeBean> relatedStandardizeds = null;

	private boolean isOnlyView = false;

	public EditRuleInfoDialog(Long comId, TreeNodeType treeNodeType, boolean isOnlyView) {
		initCom(comId, treeNodeType, isOnlyView);
	}

	public EditRuleInfoDialog(Long comId, JecnTreeNode selectNode, boolean isOnlyView, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		initCom(comId, selectNode.getJecnTreeBean().getTreeNodeType(), isOnlyView);
	}

	public void initCom(Long ruleId, TreeNodeType treeNodeType, boolean isOnlyView) {
		this.ruleId = ruleId;
		this.treeNodeType = treeNodeType;
		this.isOnlyView = isOnlyView;
		// 设置窗体大小
		this.setSize(850, 670);
		this.setTitle(JecnProperties.getValue("ruleFileAttribute"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(false);

		verfyLab.setForeground(Color.red);
		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		operationPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		baseInfoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 添加ui
		tabPanel.setUI(new JecnWindowsTabbedPaneUI());
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
		this.setModal(true);
		initLayout();
	}

	/**
	 * 布局
	 */
	protected void initLayout() {
		// 相关制度
		try {
			relatedRules = ConnectionPool.getRuleAction().getRuleTreeBeanByRuleId(ruleId);
		} catch (Exception e) {
			log.error("", e);
		}

		// 制度相关风险
		List<JecnTreeBean> listRisks = new ArrayList<JecnTreeBean>();
		try {
			listRisks = ConnectionPool.getJecnRiskAction().findJecnRuleRiskBeanTByRuleId(ruleId);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			log.error("EditRuleInfoDialog initLayout is error！", e);
			return;
		}
		List<JecnTreeBean> listStandards = new ArrayList<JecnTreeBean>();
		try {
			listStandards = ConnectionPool.getJecnRiskAction().findJecnRuleStandardBeanTByRuleId(ruleId);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			log.error("EditRuleInfoDialog initLayout is error！", e);
			return;
		}

		// 制度基本信息
		ruleBaseInfoPanel = new RuleBaseInfoPanel(ruleId, this);
		baseInfoPanel.setViewportView(ruleBaseInfoPanel);
		baseInfoPanel.getVerticalScrollBar().setValue(0);
		ruleBaseInfoPanel.getRuleNumField().setColumns(20);
		ruleBaseInfoPanel.getRuleNameField().setColumns(20);

		// 制度操作说明
		ruleOperatePanel = new RuleOperatePanel(ruleId);
		operationPanel.setViewportView(ruleOperatePanel);

		// 制度相关标准
		refStandardPanel = new FlowRelatedStandardPanel(listStandards);
		standardScrollPanel = new JScrollPane();
		standardScrollPanel.setViewportView(refStandardPanel);

		// 制度关联风险
		riskPanel = new FlowRelatedRiskPanel(listRisks);
		riskScrollPanel = new JScrollPane();
		riskScrollPanel.setViewportView(riskPanel);

		// 制度关联制度
		relatedRule = new FlowRelatedRulePanel(relatedRules, ruleId);
		ruleScrollPanel = new JScrollPane();
		ruleScrollPanel.setViewportView(relatedRule);

		int index = 0;
		tabPanel.add(baseInfoPanel, index);
		tabPanel.setTitleAt(index, JecnProperties.getValue("ruleBaseInfo"));// 制度基本信息
		index++;
		tabPanel.add(operationPanel, index);
		tabPanel.setTitleAt(index, JecnProperties.getValue("ruleOperation"));// "制度操作说明"
		index++;
		// 标准化文件
		initStandardizedFiles(index);
		index++;
		if (treeNodeType != TreeNodeType.ruleModeFile) {
			tabPanel.add(standardScrollPanel, index);
			tabPanel.setTitleAt(index, JecnProperties.getValue("relatedStandard"));// 相关标准
			index++;
			tabPanel.add(riskScrollPanel, index);
			tabPanel.setTitleAt(index, JecnProperties.getValue("relatedRisk"));// 相关风险
			index++;
			tabPanel.add(ruleScrollPanel, index);
			tabPanel.setTitleAt(index, JecnProperties.getValue("relatedRule"));// 相关制度
			index++;
		}

		// 制度文件没有制度操作说明
		if (this.treeNodeType == TreeNodeType.ruleFile) {
			tabPanel.remove(operationPanel);
		}
		// 按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		if (!isOnlyView) {
			buttonPanel.add(okBut);
		}
		buttonPanel.add(cancelBut);

		Container contentPane = this.getContentPane();
		contentPane.add(buttonPanel, BorderLayout.SOUTH);
		contentPane.add(tabPanel, BorderLayout.CENTER);
	}

	private void initStandardizedFiles(int index) {
		if (treeNodeType == TreeNodeType.ruleModeFile) {
			return;
		}
		try {
			relatedStandardizeds = ConnectionPool.getRuleAction().getStandardizedFiles(ruleId);
			relatedStandardizedFilePanel = new FlowRelatedStandardizedFilePanel(relatedStandardizeds);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			log.error("EditRuleInfoDialog initStandardizedFiles is error！", e);
			return;
		}

		// 标准化文件
		standardizedFileScrollPanel = new JScrollPane();
		standardizedFileScrollPanel.setViewportView(relatedStandardizedFilePanel);
		standardizedFileScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		tabPanel.add(standardizedFileScrollPanel, index);
		tabPanel.setTitleAt(index, JecnProperties.getValue("standardFile"));// 标准化文件
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (isOnlyView) {
			this.setVisible(false);
		} else {
			if (!isUpdate()) {// 没有更新
				this.setVisible(false);
			} else {
				int optionTig = this.dialogCloseBeforeMsgTipAction();
				if (optionTig == 2) {
					okButPerformed();
				} else if (optionTig == 1) {
					this.setVisible(false);
				}
			}
		}
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	protected boolean isUpdate() {
		// 制度基本信息
		if (ruleBaseInfoPanel.isUpdate()) {
			return true;
		}
		// 获取制度操作说明
		if (ruleOperatePanel != null) {
			if (ruleOperatePanel.isUpdate()) {
				return true;
			}
		}
		// 相关标准
		if (refStandardPanel.isUpdate()) {
			return true;
		}

		// 相关风险
		if (riskPanel.isUpdate()) {
			return true;
		}
		// 相关制度
		if (relatedRule.isUpdate()) {
			return true;
		}
		// 标准化文件
		if (relatedStandardizedFilePanel != null && relatedStandardizedFilePanel.isUpdate()) {
			return true;
		}
		return false;
	}

	/***
	 * 确定
	 */
	protected void okButPerformed() {
		verfyLab.setText("");
		try {
			if (!isUpdate()) {
				this.setVisible(false);
				return;
			}
			// 关键字 长度验证
			if (ruleBaseInfoPanel.keyWordField != null
					&& DrawCommon.checkNameMaxLength(ruleBaseInfoPanel.keyWordField.getResultStr())) {
				verfyLab.setText(JecnProperties.getValue("keyWord") + JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}
			// 验证制度编号长度
			if (DrawCommon.checkNameMaxLength(ruleBaseInfoPanel.ruleNumField.getText())) {
				verfyLab.setText(JecnProperties.getValue("ruleNum") + JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}
			// 验证自定义输入项长度
			if (ruleBaseInfoPanel.validationLength()) {
				return;
			}
			RuleT ruleT = ruleBaseInfoPanel.getRuleT();
			if (ruleBaseInfoPanel.ruleLinkText != null) {
				String linkValue = ruleBaseInfoPanel.ruleLinkText.getTextValue();
				if ("".equals(linkValue)) {
					verfyLab.setText(JecnProperties.getValue("inUrlNotNull"));
					return;
				}
				Pattern pattern = Pattern
						.compile("^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$");
				if (!pattern.matcher(linkValue).matches()) {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("linkFormatIsIncorrect"));
					return;
				}
				ruleT.setRuleUrl(linkValue);
			}

			// 有效期
			if (ruleBaseInfoPanel.expiryCommon != null) {
				if (ruleBaseInfoPanel.expiryCommon.isValidateNotPass(verfyLab)) {
					return;
				}
			}
			// 获取制度操作说明
			List<JecnRuleOperationCommon> ruleList = new ArrayList<JecnRuleOperationCommon>();
			if (this.treeNodeType == TreeNodeType.ruleModeFile && ruleOperatePanel != null) {
				ruleList = ruleOperatePanel.getResultData();
			}// 更新制度信息
			JecnRuleData ruleData = ruleBaseInfoPanel.getJecnRuleData();
			ruleData.setListJecnRuleOperationCommon(ruleList);
			if (this.treeNodeType == TreeNodeType.ruleFile) {
				ruleData.setRuleRiskTList(riskPanel.getResultIds());
				ruleData.setStandardTList(refStandardPanel.getResultIds());
				ruleData.setRuleInstitutionList(relatedRule.getResultIds());
			} else {
				ruleData.setRuleRiskTList(ruleOperatePanel.getRiskIds());
				ruleData.setStandardTList(ruleOperatePanel.getStandardIds());
				ruleData.setRuleInstitutionList(ruleOperatePanel.getRuleIds());
			}
			ruleData.setRelatedStandardizeds(relatedStandardizeds);
			ConnectionPool.getRuleAction().updateRule(ruleData);
			selectNode.getJecnTreeBean().setUpdate(true);
			JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), selectNode);
			this.dispose();
		} catch (Exception e) {
			log.error("EditRuleInfoDialog okButPerformed is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			return;
		}
		this.setVisible(false);
	}

	public RuleOperatePanel getRuleOperatePanel() {
		return ruleOperatePanel;
	}

	public void setRuleOperatePanel(RuleOperatePanel ruleOperatePanel) {
		this.ruleOperatePanel = ruleOperatePanel;
	}

	public RuleBaseInfoPanel getRuleBaseInfoPanel() {
		return ruleBaseInfoPanel;
	}

	public void setRuleBaseInfoPanel(RuleBaseInfoPanel ruleBaseInfoPanel) {
		this.ruleBaseInfoPanel = ruleBaseInfoPanel;
	}

	public JTabbedPane gettabPanel() {
		return tabPanel;
	}

}
