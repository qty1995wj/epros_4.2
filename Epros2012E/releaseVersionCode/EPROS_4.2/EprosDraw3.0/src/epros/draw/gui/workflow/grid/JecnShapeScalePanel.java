package epros.draw.gui.workflow.grid;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;

import javax.swing.JSlider;
import javax.swing.SwingConstants;

import com.sun.java.swing.plaf.windows.WindowsSliderUI;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.event.flowElement.JecnVHLineEventProcess;
import epros.draw.gui.figure.shape.JecnTempFigurePanel;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.grid.JecnShapeScaleProcess.RowColumnEnum;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnDrawScrollPane;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 画图板面列标题（面板上的刻度）或行标题（面板左面刻度）对应的控件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShapeScalePanel extends JSlider implements MouseListener,
		MouseMotionListener {

	/** 刻度算法对象 */
	private JecnShapeScaleProcess shapeScaleProcess = null;
	/** 此面板的容器 */
	private JecnDrawScrollPane scrollPane = null;
	/** 刻度类型：行刻度、竖刻度 */
	private RowColumnEnum rowColumnEnum = null;

	/** 是否执行拖动 */
	private boolean isDragged = false;

	/** 拖动分割线生成虚拟分割线 */
	private JecnTempFigurePanel tempFigurePanel = null;

	// 设置虚拟图形临时数据
	private JecnTempFigureBean tempFigureBean = null;

	private Point oldPoint = null;
	private LineDirectionEnum lineDirectionEnum = null;

	/**
	 * 
	 * 构造函数
	 * 
	 * @param rowColumnEnum
	 *            RowColumnEnum 刻度类型：行刻度、竖刻度
	 */
	private JecnShapeScalePanel(JecnDrawScrollPane scrollPane,
			RowColumnEnum rowColumnEnum) {
		if (scrollPane == null || rowColumnEnum == null) {
			JecnLogConstants.LOG_SHAPE_SCALE_PANEL
					.error("JecnShapeScalePanel类构造函数：参数为null. scrollPane="
							+ scrollPane + ";rowColumnEnum=" + rowColumnEnum);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.scrollPane = scrollPane;
		this.rowColumnEnum = rowColumnEnum;

		// 初始化
		initComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 刻度算法对象
		shapeScaleProcess = new JecnShapeScaleProcess(rowColumnEnum);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 透明
		this.setOpaque(false);
		// 无边框
		this.setBorder(null);
		// 选中焦点框不显示
		this.setFocusable(false);

		// 设置UI
		if (this.getUI() instanceof WindowsSliderUI) {
			this.setUI(new JecnSliderUI(this));
		}

		switch (rowColumnEnum) {
		case ROW_FLAG: // 行标题
			this.setOrientation(SwingConstants.VERTICAL);
			// 开始位置为上面
			setInverted(true);
			break;
		case COLUMN_FLAG: // 列标题
			this.setOrientation(SwingConstants.HORIZONTAL);
			break;
		}
		// 默认为最小值
		this.setValue(this.getMinimum());

		// 事件
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	/**
	 * 
	 * 获取行标题面板
	 * 
	 * @param scrollPane
	 *            JecnDrawScrollPane 画图面板的滚动面板
	 * @return JecnShapeScalePanel
	 */
	public static JecnShapeScalePanel createRowShapeScalePanel(
			JecnDrawScrollPane scrollPane) {
		return new JecnShapeScalePanel(scrollPane, RowColumnEnum.ROW_FLAG);
	}

	/**
	 * 
	 * 获取竖标题面板
	 * 
	 * @param scrollPane
	 *            JecnDrawScrollPane 画图面板的滚动面板
	 * @return JecnShapeScalePanel
	 */
	public static JecnShapeScalePanel createColumnShapeScalePanel(
			JecnDrawScrollPane scrollPane) {
		return new JecnShapeScalePanel(scrollPane, RowColumnEnum.COLUMN_FLAG);
	}

	/**
	 * 根据放大缩小倍数 添加面板刻度
	 * 
	 * @param multiple
	 * @param rowColumnFlag
	 */
	public void setCurrSize(int newWidth, int newHeight) {
		// 当前类的宽
		int userWidth = 0;
		// 当前类的高
		int userHeight = 0;

		// 计算刻度面板大小
		switch (rowColumnEnum) {
		case COLUMN_FLAG:
			// 重新给面板赋宽、高
			userWidth = newWidth;
			userHeight = JecnWorkflowConstant.SCALE_PANEL_WIDTH;
			this.setMaximum(newWidth);
			break;
		case ROW_FLAG:
			userWidth = JecnWorkflowConstant.SCALE_PANEL_WIDTH;
			userHeight = newHeight;
			this.setMaximum(newHeight);
			break;
		default:
			JecnLogConstants.LOG_SHAPE_SCALE_PANEL
					.error("JecnShapeScalePanel类setCurrSize方法：处理逻辑错误. rowColumnEnum="
							+ rowColumnEnum);
			break;
		}

		// *********** 计算类*********** //
//		// 设置放大倍数
//		this.shapeScaleProcess.setMultiple(scrollPane.getDesktopPane()
//				.getWorkflowScale());
		// 设置大小
		shapeScaleProcess.setWidth(userWidth);
		shapeScaleProcess.setHeight(userHeight);
		// *********** 计算类*********** //

		// 设置当前面板大小
		this.setPreferredSize(new Dimension(userWidth, userHeight));
		this.setSize(userWidth, userHeight);
	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {
		// 正在编辑且内容不合法
		if (!scrollPane.getDesktopPane().isEditFail()) {
			return;
		}

		int v = 0;
		switch (rowColumnEnum) {
		case COLUMN_FLAG:
			v = e.getX();
			break;
		case ROW_FLAG:
			v = e.getY();
			break;
		default:
			break;
		}
		this.setValue(v);

		// 清除角色移动
		JecnDrawMainPanel.getMainPanel().getToolbar()
				.getFormatToolBarItemPanel().setRoleMoveCheckBoxFalse();
	}

	public void mouseReleased(MouseEvent e) {
		if (isDragged) {
			// 画图面板
			JecnDrawDesktopPane workflow = scrollPane.getDesktopPane();
			if (!workflow.isEditFail()) {// 正在编辑且内容不合法
				return;
			}

			isDragged = false;
			workflow.remove(tempFigurePanel);

			int diffXY = 0;
			switch (lineDirectionEnum) {
			case horizontal:
				// 横线去垂直位移
				diffXY = tempFigurePanel.getLocation().y - oldPoint.y;
				JecnVHLineEventProcess.lineUnit.moveAllFiguresAndLines(
						oldPoint.y, diffXY);
				break;
			case vertical:
				diffXY = tempFigurePanel.getLocation().x - oldPoint.x;
				JecnVHLineEventProcess.lineUnit.moveAllFiguresAndLines(
						oldPoint.x, diffXY);
				break;
			default:
				break;
			}
			JecnVHLineEventProcess.lineUnit.cleanTempList();
			tempFigurePanel.getTempFigureList().clear();
			workflow.remove(tempFigurePanel);
		}
	}

	public void mouseDragged(MouseEvent e) {
		// 画图面板
		JecnDrawDesktopPane desktopPane = scrollPane.getDesktopPane();
		if (!desktopPane.isEditFail()) {// 正在编辑且内容不合法
			return;
		}

		if (!isDragged) {
			// 获取面板所有流程元素集合
			List<JecnBaseFlowElementPanel> allFlowList = desktopPane
					.getPanelList();
			// 设置拖动标识
			isDragged = true;
			// 创建拖动的虚拟图形
			tempFigurePanel = new JecnTempFigurePanel();
			// 虚拟图形边框数据
			tempFigureBean = new JecnTempFigureBean();
			switch (rowColumnEnum) {
			case ROW_FLAG:// 行标题，横线
				// 鼠标点击横向分割线记录点击时鼠标的纵坐标
				oldPoint = e.getPoint();
				// 设置图形位置点和大小
				tempFigureBean.reSetTempFigureBean(oldPoint, desktopPane
						.getFlowMapData().getWorkflowWidth(), 1);
				// 设置拖动的虚拟分割线mapElemType类型
				tempFigureBean.setMapElemType(MapElemType.ParallelLines);
				tempFigurePanel.setBounds(oldPoint, desktopPane
						.getFlowMapData().getWorkflowWidth(), 1);
				// 拖动的虚拟分割线横竖标识
				lineDirectionEnum = LineDirectionEnum.horizontal;
				break;
			case COLUMN_FLAG:
				// 设置图形位置点和大小
				oldPoint = e.getPoint();
				// 设置图形位置点和大小
				tempFigureBean.reSetTempFigureBean(oldPoint, 1, desktopPane
						.getFlowMapData().getWorkflowHeight());
				// 设置拖动的虚拟分割线mapElemType类型
				tempFigureBean.setMapElemType(MapElemType.VerticalLine);
				// 鼠标点击横向分割线记录点击时鼠标的横坐标
				tempFigurePanel.setBounds(oldPoint, 1, desktopPane
						.getFlowMapData().getWorkflowHeight());
				// 拖动的虚拟分割线横竖标识
				lineDirectionEnum = LineDirectionEnum.vertical;
				break;
			default:
				break;
			}
			// 设置拖动前的鼠标位置点
			JecnVHLineEventProcess.setOldX(oldPoint.x);
			JecnVHLineEventProcess.setOldY(oldPoint.y);
			// 记录拖动前待拖动的流程元素数据
			JecnVHLineEventProcess.initFigureList(allFlowList,
					lineDirectionEnum, oldPoint, tempFigurePanel);
			tempFigurePanel.getTempFigureList().add(tempFigureBean);
		}
		if (oldPoint == null) {
			return;
		}
		int iaa = oldPoint.x - e.getPoint().x;
		if (rowColumnEnum.equals(RowColumnEnum.COLUMN_FLAG) && iaa == 0) {// 纵向分割线
			if (oldPoint.x < 10) {
				return;
			}
		} else {
			if (oldPoint.y < 10 && iaa == 0) {
				return;
			}
		}
		// 获取分割线间拖动时未选中的图形的最大值
		int maxFigureXY = JecnVHLineEventProcess.lineUnit.getMaxFigureDiff();
		// 拖动虚拟分割线
		JecnVHLineEventProcess.reSetTempFigure(oldPoint, e.getPoint(),
				lineDirectionEnum, tempFigurePanel, maxFigureXY);
	}

	public void mouseMoved(MouseEvent e) {
	}

	/**
	 * 
	 * 滚动条UI
	 * 
	 * @author ZHOUXY
	 * 
	 */
	class JecnSliderUI extends WindowsSliderUI {
		private BasicStroke stroke = new BasicStroke(2);
		// 底边长度
		private int w = 10;
		private int w2 = 5;

		public JecnSliderUI(JSlider slider) {
			super(slider);
		}

		/**
		 * 绘制指示图标
		 */
		public void paintThumb(Graphics g) {

			Graphics2D g2d = (Graphics2D) g;

			// 两个底边，一个顶点
			int xL = 0;
			int yL = 0;
			int xL2 = 0;
			int yL2 = 0;
			int xH = 0;
			int yH = 0;

			int[] xArray = null;
			int[] yArray = null;

			g2d.setColor(JecnUIUtil.getScaleColor());
			g2d.setStroke(stroke);

			switch (rowColumnEnum) {// 当前thumb位置
			case ROW_FLAG: // 行标题
				xL = thumbRect.x;
				yL = thumbRect.y;
				xL2 = thumbRect.x;
				yL2 = thumbRect.y + w;
				xH = thumbRect.x + w;
				yH = thumbRect.y + w2;

				xArray = new int[] { xL, xL2, xH };
				yArray = new int[] { yL, yL2, yH };

				// 三角形
				g2d.fillPolygon(xArray, yArray, 3);
				g2d.drawPolygon(xArray, yArray, 3);

				// 直线
				g2d.fillRect(thumbRect.x, thumbRect.y + w2, thumbRect.width, 1);

				break;
			case COLUMN_FLAG: // 列标题
				xL = thumbRect.x;
				yL = thumbRect.y;
				xL2 = thumbRect.x + w;
				yL2 = thumbRect.y;
				xH = thumbRect.x + w2;
				yH = thumbRect.y + w;

				xArray = new int[] { xL, xL2, xH };
				yArray = new int[] { yL, yL2, yH };

				g2d.fillPolygon(xArray, yArray, 3);
				g2d.drawPolygon(xArray, yArray, 3);

				// 直线
				g2d
						.fillRect(thumbRect.x + w2, thumbRect.y, 1,
								thumbRect.height);
				break;
			}
		}

		/**
		 * 绘制刻度轨迹
		 */
		public void paintTrack(Graphics g) {
			// 绘制刻度
			shapeScaleProcess.drawScale(g, JecnUIUtil
					.getDefaultBackgroundColor(), Color.black);
		}
	}
}
