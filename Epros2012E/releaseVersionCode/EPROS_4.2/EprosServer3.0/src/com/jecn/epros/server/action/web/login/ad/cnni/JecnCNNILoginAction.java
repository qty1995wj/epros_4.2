package com.jecn.epros.server.action.web.login.ad.cnni;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 
 * 中国科学院计算机网络信息中心单点登录
 * 
 * @author ZHOUXY
 * 
 */
public class JecnCNNILoginAction extends JecnAbstractADLoginAction {
	public JecnCNNILoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		return null;
	}
}
