package epros.designer.gui.process.flow;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessFileNodeData;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.process.JecnProcessCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

/*******************************************************************************
 * 流程文件
 * 
 * @author 2012-07-04
 * 
 */
public class AddFlowFileDialog extends BaseFlowFileDialog {
	private JecnTree jTree = null;

	public AddFlowFileDialog(JecnTreeNode selectNode, JecnTree jTree) {
		super(selectNode, true);
		this.jTree = jTree;
	}

	protected void okButton() {
		ProcessFileNodeData processFileData = new ProcessFileNodeData();
		try {
			String flowName = filePropertyPanel.getFlowNameField().getText();
			if(validateNamefullPath(flowName,1)){
				return;
			}
			// 判断是否重名
			if (ConnectionPool.getProcessAction().validateAddName(flowName, this.selectNode.getJecnTreeBean().getId(),
					1, JecnConstants.projectId)) {
				verfyLab.setText(JecnProperties.getValue("nameHaved"));
				return;
			}

			String errorText = isValidate();
			if (!DrawCommon.isNullOrEmtry(errorText)) {
				verfyLab.setText(errorText);
				return;
			}

			processFileData.setBasicInfoT(new JecnFlowBasicInfoT());
			// 新增到数据库
			JecnFlowStructureT flowStructureT = JecnProcessCommon.createProcessFile(flowName, filePropertyPanel
					.getFlowNumField().getText(), selectNode, processFileData.getBasicInfoT());
			processFileData.setFlowStructureT(flowStructureT);

			commonProcessFileData(processFileData);

			flowStructureT = ConnectionPool.getProcessAction().createProcessFile(processFileData);
			// 刷新树节点
			JecnProcessCommon.refreshTreeProcessFileNode(filePropertyPanel.getFlowNameField().getText(), flowStructureT
					.getFlowIdInput(), selectNode, flowStructureT, jTree);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		this.dispose();
	}
	/**
	 * 提示 文件库中是否有重名文件
	 * @param names
	 * @return
	 */
	private boolean validateNamefullPath(String flowName,int type){
		try {
			String ns = ConnectionPool.getProcessAction().validateNamefullPath(flowName,this.selectNode.getJecnTreeBean().getId(), type, JecnConstants.projectId);
			if(!"".equals(ns)){
				String tipText = ns+" "+JecnProperties.getValue("itExistsDirectories");
				// 是否删除提示框
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData
						.getFrame(), tipText, null,
						JecnOptionPane.YES_NO_OPTION,
						JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
