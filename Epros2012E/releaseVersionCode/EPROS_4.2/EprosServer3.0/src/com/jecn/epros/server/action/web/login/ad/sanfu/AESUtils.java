package com.jecn.epros.server.action.web.login.ad.sanfu;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 三福百货单点登录 获取信息解密处理
 * 
 * @author 客户提供
 * 
 */
public class AESUtils {
	private static final Logger log = Logger.getLogger(AESUtils.class);
	private final static String algorithm = "AES";

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptBASE64(String key) throws Exception {
		return (new BASE64Decoder()).decodeBuffer(key);
	}

	/**
	 * BASE64加密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String encryptBASE64(byte[] key) throws Exception {
		return (new BASE64Encoder()).encodeBuffer(key);
	}

	/**
	 * 加密
	 * 
	 * @param data
	 * @param rawKey
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String data, String rawKey) {
		byte[] key = rawKey.getBytes();
		// Instantiate the cipher
		try {
			// KeyGenerator kgen = KeyGenerator.getInstance("AES");
			// kgen.init(256, new SecureRandom(key));
			// SecretKey secretKey = kgen.generateKey();
			// byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			// IvParameterSpec iv = new IvParameterSpec(key);

			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			byte[] encrypted = cipher.doFinal(data.getBytes());
			return encryptBASE64(encrypted);
		} catch (Exception e) {
			// App.log.info("AES", "获取加密串出错," + e.getMessage());
			log.error(e);
			return "";
		}

	}

	/**
	 * 解密
	 * 
	 * @param encrypted
	 * @param rawKey
	 * @return http://localhost:8080//apps-client/action/user/portalcheck?Para=OoaKzb6S6TJ7UGoTGqQ6VPhEsbvz%2b1f1pIp2gXcILFfm%2bjpYfFem7%2fweT9TFl0HHcQB3qkdthnbMMvEstDsvgkjo17Gqt6sB8NggtPn5SBV9NmSmPsV%2bnd2ryhPfLGQBK0GYh9m8tz8E4FtDBEkwIA%3d%3d
	 */
	public static String decrypt(String encrypted, String rawKey) {
		try {
			encrypted = encrypted.replace("%2b", "+");
			encrypted = encrypted.replace("%2f", "/");
			encrypted = encrypted.replace("%3d", "=");
			// System.out.println(encrypted);
			byte[] tmp = decryptBASE64(encrypted);
			byte[] key = rawKey.getBytes();

			SecretKeySpec skeySpec = new SecretKeySpec(key, algorithm);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(2, skeySpec);

			byte[] decrypted = cipher.doFinal(tmp);
			return new String(decrypted);
		} catch (Exception e) {
			// App.log.info("AES", "获取解密串出错," + e.getMessage());
			return "";
		}

	}

//	public static void main(String[] args) throws Exception {
//		String data = "Employeeid=06400007&Expired_Date=2012-07-19&Form_System=GreenOffice&Target_Type=Login";
//		// Employeeid=06400490&Expired_Date=2012-07-19
//		// 15:58:38.992&From_System=GreenOffice&Target_Type=Login
//		// Employeeid=06400490&Expired_Date=2012-07-19
//		// 15:58:38.992&Form_System=GreenOffice&Target_Type=Login
//		// Employeeid=06400490&Expired_Date=2012-07-19
//		// 15:58:38.992&Form_System=GreenOffice&Target_Type=Login
//		// Employeeid=06400490&Expired_Date=2012-07-19
//		// 15:58:38.992&From_System=GreenOffice&Target_Type=Login
//		// Employeeid=06400490&Expired_Date=2012-07-19
//		// 15:58:38.992&Form_System=GreenOffice&Target_Type=Login
//		String key = "BCFD3E51207C0CF5";
//		System.out.println("密钥为：" + key);
//		long lStart = System.currentTimeMillis();
//		// 加密
//		String encrypted = encrypt(data, key);
//		System.out.println("原文：" + data);
//		System.out.println("加密后：" + encrypted);
//		long lUseTime = System.currentTimeMillis() - lStart;
//		System.out.println("加密耗时：" + lUseTime + "毫秒");
//
//		System.out.println();
//		// 解密
//		lStart = System.currentTimeMillis();
//		String decrypted = decrypt(
//				"OoaKzb6S6TJ7UGoTGqQ6VIdAhxroa77vUBzQo6KjXct3loxM3dBm8WC6KnGRhTGInpVX/Lu6OsX0Ls1X2D3VAkjo17Gqt6sB8NggtPn5SBV9NmSmPsV+nd2ryhPfLGQBK0GYh9m8tz8E4FtDBEkwIA==",
//				key);// 解密串
//		System.out.println("解密后： " + decrypted);
//		lUseTime = System.currentTimeMillis() - lStart;
//		System.out.println("解密耗时：" + lUseTime + "毫秒");
//	}
}
