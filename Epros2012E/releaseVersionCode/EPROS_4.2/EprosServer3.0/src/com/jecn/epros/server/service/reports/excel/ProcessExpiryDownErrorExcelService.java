package com.jecn.epros.server.service.reports.excel;

import java.io.File;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 
 * 下载流程有效期维护错误数据
 * 
 * @author huoyl
 * 
 */
public class ProcessExpiryDownErrorExcelService {

	private int type = 0;

	public ProcessExpiryDownErrorExcelService() {

	}

	public byte[] getExcelFile(Long peopleId) throws Exception {

		byte[] tempDate = null;

		File file = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.REPORT_ERROR_DIR + peopleId;
			if (type == 0) {
				filePath += "ProcessExpiryMaintenanceError.xls";
			} else {
				filePath += "RuleExpiryMaintenanceError.xls";
			}

			// 创建导出到excel
			file = new File(filePath);
			if (!file.exists()) {
				return new byte[0];
			}
			tempDate = JecnUtil.getByte(file);

		} catch (Exception e) {
			throw new Exception("ProcessExpiryDownErrorExcelService类getExcelFile方法异常", e);

		}
		return tempDate;

	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
