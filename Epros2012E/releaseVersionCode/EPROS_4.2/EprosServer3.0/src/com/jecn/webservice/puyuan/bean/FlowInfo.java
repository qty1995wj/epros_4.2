package com.jecn.webservice.puyuan.bean;

import java.util.List;


/**
 * 普元流程信息
 * 
 * @author chehuanbo
 * @version 1.0
 * @date 2014-01-19
 * 
 * */
public class FlowInfo {
	
	/**流程ID*/
	private Long flowId;
	/**流程名称*/
	private String flowName; 
	/**流程创建者*/
	private String flowCreateName;
	/**流程创建者所属部门*/
	private String flowDepart;
	
	/***** 流程节点集合 *********/
	private List<FlowNode> flowNode;
	/***** 流程线条集合 *********/
	private List<FlowLink> flowLink;

	
	
	public String getFlowId() {
		if(flowId == null){
			return "";
		}
		return flowId.toString();
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		if(flowName == null){
			return "";
		}
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getFlowCreateName() {
		if(flowCreateName==null){
			return "";
		}
		return flowCreateName;
	}
	public void setFlowCreateName(String flowCreateName) {
		this.flowCreateName = flowCreateName;
	}
	public String getFlowDepart() {
		if(flowDepart == null){
           return "";			
 		}
		return flowDepart;
	}
	public void setFlowDepart(String flowDepart) {
		this.flowDepart = flowDepart;
	}
	public List<FlowNode> getFlowNode() {
		return flowNode;
	}
	public void setFlowNode(List<FlowNode> flowNode) {
		this.flowNode = flowNode;
	}
	public List<FlowLink> getFlowLink() {
		return flowLink;
	}
	public void setFlowLink(List<FlowLink> flowLink) {
		this.flowLink = flowLink;
	}
	

}
