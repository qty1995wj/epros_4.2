package com.jecn.epros.server.connector.Libang;

import java.util.List;

public class LibangTasksBean {
	/** 应用代码 */
	private String AppCode;
	/** 任务分类代码 */
	private String CategoryCode;
	/** 任务标题 */
	private String Title;
	/** 待办详情页链接地址 */
	private String Url;
	/** 任务最开始的创建人登录帐号 */
	private String Initiator;

	/** 任务最开始的创建人 */
	private String InitiatorName;

	/** 任务的发起人 登录帐号 */
	private String Sender;

	/** OA中Mobile端的待办任务链接 */
	private String SenderName;

	private String RefID;
	/** 任务处理人接收到任务的时间时间戳 */
	private int ReceiveDate;

	private int Mode = 1;

	private String CCs;

	private List<LibangTaskReceivers> Receivers;

	public String getAppCode() {
		return AppCode;
	}

	public void setAppCode(String appCode) {
		AppCode = appCode;
	}

	public String getCategoryCode() {
		return CategoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		CategoryCode = categoryCode;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public String getInitiator() {
		return Initiator;
	}

	public void setInitiator(String initiator) {
		Initiator = initiator;
	}

	public String getInitiatorName() {
		return InitiatorName;
	}

	public void setInitiatorName(String initiatorName) {
		InitiatorName = initiatorName;
	}

	public String getSender() {
		return Sender;
	}

	public void setSender(String sender) {
		Sender = sender;
	}

	public String getSenderName() {
		return SenderName;
	}

	public void setSenderName(String senderName) {
		SenderName = senderName;
	}

	public String getRefID() {
		return RefID;
	}

	public void setRefID(String refID) {
		RefID = refID;
	}

	public int getReceiveDate() {
		return ReceiveDate;
	}

	public void setReceiveDate(int receiveDate) {
		ReceiveDate = receiveDate;
	}

	public int getMode() {
		return Mode;
	}

	public void setMode(int mode) {
		Mode = mode;
	}

	public List<LibangTaskReceivers> getReceivers() {
		return Receivers;
	}

	public void setReceivers(List<LibangTaskReceivers> receivers) {
		Receivers = receivers;
	}

	public String getCCs() {
		return CCs;
	}

	public void setCCs(String cCs) {
		CCs = cCs;
	}

	public String toString() {
		return "RefID = " + RefID + "  Initiator = " + Initiator + "  Url = " + Url;
	}

}
