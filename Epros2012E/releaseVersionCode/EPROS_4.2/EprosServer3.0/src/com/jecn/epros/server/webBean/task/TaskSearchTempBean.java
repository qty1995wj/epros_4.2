package com.jecn.epros.server.webBean.task;

/**
 * 任务搜索条件
 * 
 * @author ZHANGXH
 * @date： 日期：Nov 23, 2012 时间：3:05:34 PM
 */
public class TaskSearchTempBean {
	/** 任务名称 */
	private String taskName;
	/** 创建人人员主键ID */
	private Long createPeopleId;
	/** 任务阶段 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段 */
	private int taskStage = -1;
	/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
	private int taskType = -1;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/** 搁置任务 true为搁置*/
	private boolean delayTask;
	 
	public boolean isDelayTask() {
		return delayTask;
	}

	public void setDelayTask(boolean delayTask) {
		this.delayTask = delayTask;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public int getTaskStage() {
		return taskStage;
	}

	public void setTaskStage(int taskStage) {
		this.taskStage = taskStage;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

}
