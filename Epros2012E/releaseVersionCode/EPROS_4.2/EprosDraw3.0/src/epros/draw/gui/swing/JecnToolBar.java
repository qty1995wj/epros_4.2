package epros.draw.gui.swing;

import java.awt.BorderLayout;

import javax.swing.JToolBar;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 按钮容器类
 * 
 * @author Administrator
 * 
 */
public class JecnToolBar extends JToolBar {

	public JecnToolBar() {
		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置透明
//		this.setOpaque(false);
		// 不运行拖动
		this.setFloatable(false);
		// 设置布局管理器
		this.setLayout(new BorderLayout());
		//无边框
		this.setBorder(null);
	}
}
