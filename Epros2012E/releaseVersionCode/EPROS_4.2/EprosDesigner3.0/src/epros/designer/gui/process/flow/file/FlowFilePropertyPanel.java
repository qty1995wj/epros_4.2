package epros.designer.gui.process.flow.file;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.process.FlowFileContent;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessInfoBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.process.flow.BaseFlowFileDialog;
import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.gui.system.BusinessTypeCommon;
import epros.designer.gui.system.CategoryCommon;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.gui.system.FileEnclosureSelectCommon;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.gui.system.SupportToolsCommon;
import epros.designer.gui.system.field.JecnTipTextField;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.JecnSystemData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 流程（文件） 属性面板
 * 
 * @author ZXH
 * @date 2017-4-20下午01:44:47
 */
public class FlowFilePropertyPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(FlowFilePropertyPanel.class);

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(200, 300);
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** 流程名称Lab */
	private JLabel flowNameLab = new JLabel(JecnProperties.getValue("flowNameC"));
	/** 流程名称Field */
	private JecnTextField flowNameField = new JecnTextField();

	private JButton openBut = new JButton(JecnProperties.getValue("openBtn"));
	private JButton uploadBut = new JButton(JecnProperties.getValue("uploadBut"));

	/** 流程编号Lab */
	private JLabel flowNumLab = new JLabel(JecnProperties.getValue("flowNumC"));
	/** 流程编号Field */
	private JecnTextField flowNumField = new JecnTextField();
	/** 有效期验证提示 */
	private JLabel flowNumVerfLab = new JLabel();

	/** 有效期显示值 */
	private String validityVaule = null;
	/** 有效期Lab */
	private JLabel validityLab = new JLabel(JecnProperties.getValue("validMonthC"));
	/** 有效期Field */
	private JTextField validityField = new JecnTextField();
	/** 有效期验证提示 */
	private JLabel validityVerfLab = new JLabel();
	/** 是否永久有效复选框 */
	private JCheckBox validityCheckBox = new JCheckBox(JecnProperties.getValue("foreverC"));

	/** 设置大小 */
	Dimension dimension = null;

	// 流程ID===============================================自动获取
	private Long flowId = null;// 6359L;

	/** 基本信息 */
	private ProcessAttributeBean processAttributeBean = null;
	/** 流程类别 */
	private CategoryCommon categoryCommon = null;
	/** 流程类别 */
	private BusinessTypeCommon businessTypeCommon = null;
	/** 流程责任人 */
	private PersonliableSelectCommon personliableSelect = null;
	/** 流程监护人 */
	private PeopleSelectCommon guardianPeopleSelectCommon = null;
	/** 流程拟制人 */
	private PeopleSelectCommon fictionPeopleSelectCommon = null;
	/** 责任部门 */
	private ResDepartSelectCommon resDepartSelectCommon = null;

	/** 查阅权限 */
	private AccessAuthorityCommon accessAuthorityCommon = null;

	/** 支持工具 */
	private SupportToolsCommon supportToolsCommon;

	/** 附件 */
	private FileEnclosureSelectCommon fileEnclosureSelectCommon;

	private JecnTreeNode selectNode = null;
	/** 流程类别显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessCategory = 0;
	/** 流程拟制人显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessIntendedPerson = 0;
	/** 流程监护人显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessGuardian = 0;
	/** 流程附件显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessEnclosure = 0;
	/** 流程支持工具显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessSupportTools = 0;
	/** 流程业务类型显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessBussType = 0;

	private JecnDialog dialog;

	// 根据流程ID获得支持工具ID集合
	private List<JecnTreeBean> listFlowTool = new ArrayList<JecnTreeBean>();
	// 附件
	private Long fileId = null;

	private FlowFileContent fileContent = null;

	private boolean isAdd;
	/** 流程专员人 */
	private PeopleSelectCommon commissionerSelect = null;
	/** 关键字 */
	private JecnTipTextField keyWordField = null;

	// 流程编号
	String oldFlowNumberId = "";
	// 流程编号
	String oldFlowName = "";

	public FlowFilePropertyPanel(JecnTreeNode selectNode, BaseFlowFileDialog dialog, boolean isAdd) {
		this.selectNode = selectNode;
		this.isAdd = isAdd;
		this.dialog = dialog;
		init();
	}

	private void init() {
		initConfig();
		initData();
		initComponent();
		initLayout();
		initListener();
	}

	private void initComponent() {
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		validityCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		validityVerfLab.setForeground(Color.red);
	}

	private void initListener() {
		// 有效期
		validityVaule = processAttributeBean.getExpiry();
		if (validityVaule != null && !"".equals(validityVaule)) {
			if (validityVaule.equals("0")) {
				validityCheckBox.setSelected(true);
				validityField.setEditable(false);
			} else {
				validityCheckBox.setSelected(false);
				validityField.setEditable(true);
				validityField.setText(validityVaule);
			}
		} else {
			// 获取流程配置-->流程属性中 有效期配置数据
			if (JecnConfigTool.processValidityAllocation() != null
					&& !"".equals(JecnConfigTool.processValidityAllocation())) {
				if ("0".equals(JecnConfigTool.processValidityAllocation())) {
					validityCheckBox.setSelected(true);
					validityField.setEditable(false);
				} else {
					validityCheckBox.setSelected(false);
					validityField.setEditable(true);
					validityField.setText(JecnConfigTool.processValidityAllocation());
				}
				validityVaule = JecnConfigTool.processValidityAllocation();
			} else {
				validityCheckBox.setSelected(false);
				validityField.setEditable(true);
			}

		}

		// 有效期永久性
		validityCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (validityCheckBox.isSelected()) {
					validityField.setEditable(false);
					validityField.setText("");
				} else {
					validityField.setEditable(true);
				}
			}
		});

		uploadBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				uploadFile();
			}
		});
		openBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				openProcessFile();
			}
		});
	}

	void uploadFile() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSavaRuleUpdateDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);

		fileChoose.setEidtTextFiled(false);

		// 单选
		fileChoose.setMultiSelectionEnabled(false);
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
		int i = fileChoose.showSaveDialog(this);
		if (i == JFileChooser.APPROVE_OPTION) {
			File file = fileChoose.getSelectedFile();
			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();

			fileContent = new FlowFileContent();
			fileContent.setFileName(JecnUtil.getFileName(file.getName()));
			fileContent.setBytes(JecnUtil.changeFileToByte(file.getPath()));
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSavaRuleUpdateDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();

			this.flowNameField.setText(getSubFileName(file.getName()));
		}
	}

	private String getSubFileName(String name) {
		return name.substring(0, name.lastIndexOf("."));
	}

	void openProcessFile() {
		if (fileContent != null && fileContent.getBytes() != null) {
			JecnUtil.openFile(fileContent.getFileName(), fileContent.getBytes());
		} else if (flowId != -1) {
			JecnFlowStructureT flowStructureT;
			try {
				flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);
				if (flowStructureT.getFileContentId() != null) {
					FileOpenBean openBean = ConnectionPool.getProcessAction().openFileContent(
							flowStructureT.getFileContentId());
					JecnUtil.openFile(openBean.getName(), openBean.getFileByte());
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.error("FlowFilePropertyPanel openProcessFile is error！", e);
			}
		}
	}

	private void initData() {
		if (isAdd || selectNode == null || selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.processFile) {
			processAttributeBean = new ProcessAttributeBean();
			flowId = -1L;
			return;
		}

		if (selectNode.getJecnTreeBean().getNumberId() != null) { //原始编号
			oldFlowNumberId = selectNode.getJecnTreeBean().getNumberId();
		}
		oldFlowName = selectNode.getJecnTreeBean().getName();//原始名称
		this.flowNameField.setText(selectNode.getJecnTreeBean().getName());
		this.flowId = selectNode.getJecnTreeBean().getId();

		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {
			flowNumField.setEditable(false);
		}
		this.flowNumField.setText(selectNode.getJecnTreeBean().getNumberId());
		// 获取流程类别信息
		try {
			processAttributeBean = ConnectionPool.getProcessAction().getFlowAttribute(flowId);
		} catch (Exception e1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("gettingFlowAttrError"));
			log.error("FlowFilePropertyPanel initData is error", e1);
		}

		try {
			// 获得支持工具树对象
			listFlowTool = ConnectionPool.getFlowTool().getSustainToolsByFlowId(flowId);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("gettingToolsError"));
			log.error("FlowFilePropertyPanel initData is error", e);
		}

		try {
			JecnFlowBasicInfoT flowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(flowId);
			if (flowBasicInfoT != null) {
				fileId = flowBasicInfoT.getFileId();
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("gettingFlowObjError"));
			log.error("FlowFilePropertyPanel initData is error", e);
		}
	}

	/**
	 * 初始化配置
	 */
	private void initConfig() {
		isShowProcessCategory = JecnConfigTool.processCategoryShowType();
		this.isShowProcessIntendedPerson = JecnConfigTool.processIntendedPersonShowType();
		this.isShowProcessGuardian = JecnConfigTool.processGuardianShowType();
		this.isShowProcessSupportTools = JecnConfigTool.processSupportToolsShowType();
		this.isShowProcessEnclosure = JecnConfigTool.processEnclosureShowType();
		this.isShowProcessBussType = JecnConfigTool.processBussTypeShowType();
	}

	private void initLayout() {

		GridBagConstraints c = null;
		// Insets insets = new Insets(4, 5, 3, 5);
		Insets insets = new Insets(5, 5, 5, 5);
		// 信息面板=========================布局
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(mainPanel, c);

		mainPanel.setLayout(new GridBagLayout());

		Insets insetd = new Insets(0, 5, 0, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		int rows = 0;
		// 流程名称
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(this.flowNameLab, c);
		c = new GridBagConstraints(1, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(this.flowNameField, c);

		c = new GridBagConstraints(2, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(this.uploadBut, c);

		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(this.openBut, c);

		rows++;

		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(this.flowNumLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(this.flowNumField, c);
		if (JecnConfigTool.isRequest("189")) {// 流程编号是否必填
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
		rows++;

		if (JecnConfigTool.isAutoNumber()) {
			flowNumField.setEditable(false);
		}
		if (this.isShowProcessCategory != 0) {
			// 流程类别
			categoryCommon = new CategoryCommon(rows, infoPanel, insets, processAttributeBean.getProcessTypeId(), this,
					JecnConfigTool.isRequest("4"));
			rows++;
		}
		// 业务类型
		if (this.isShowProcessBussType != 0) {
			businessTypeCommon = new BusinessTypeCommon(rows, infoPanel, insets, processAttributeBean.getBussType(),
					dialog, isShowProcessBussType == 2);
			rows++;
		}
		// 流程责任人
		personliableSelect = new PersonliableSelectCommon(rows, infoPanel, insets,
				processAttributeBean.getDutyUserId(), processAttributeBean.getDutyUserType(), processAttributeBean
						.getDutyUserName(), dialog, 0, JecnConfigTool.isRequest("3"));
		rows++;

		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessCommissioner)) {
			// 流程专员
			commissionerSelect = new PeopleSelectCommon(rows, infoPanel, insets, processAttributeBean
					.getCommissionerId(), processAttributeBean.getCommissionerName(), dialog, JecnProperties
					.getValue("commissionerC"), JecnConfigTool.isRequest("isShowProcessCommissioner"));
			rows++;
		}

		if (this.isShowProcessGuardian != 0) {
			// 流程监护人
			guardianPeopleSelectCommon = new PeopleSelectCommon(rows, infoPanel, insets, processAttributeBean
					.getGuardianId(), processAttributeBean.getGuardianName(), dialog, JecnProperties
					.getValue("flowGuardianPeopleC"), isShowProcessGuardian == 2);
			rows++;
		}
		if (this.isShowProcessIntendedPerson != 0) {
			// 流程拟制人
			fictionPeopleSelectCommon = new PeopleSelectCommon(rows, infoPanel, insets, processAttributeBean
					.getFictionPeopleId(), processAttributeBean.getFictionPeopleName(), dialog, JecnProperties
					.getValue("flowFictionPeopleC"), isShowProcessIntendedPerson == 2);
			rows++;
		}
		// 责任部门
		resDepartSelectCommon = new ResDepartSelectCommon(rows, infoPanel, insets, processAttributeBean.getDutyOrgId(),
				processAttributeBean.getDutyOrgName(), dialog, JecnConfigTool.isRequest("5"));
		rows++;

		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessCommissioner)) {
			// 关键字
			this.keyWordField = new JecnTipTextField(rows, infoPanel, insets, JecnProperties.getValue("keyWordC"),
					processAttributeBean.getKeyWord() == null ? "" : processAttributeBean.getKeyWord(), JecnProperties
							.getValue("keyWordC"));
			if (JecnConfigTool.isRequest("isShowProcessKeyWord")) {// 关键字是否必填
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}

		// 有效期 validityLab
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(validityLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(validityField, c);
		// 是否永久有效validityCheckBox
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(validityCheckBox, c);
		c = new GridBagConstraints(4, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(new CommonJlabelMustWrite(), c);
		rows++;

		// 有效期验证提示
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetd, 0, 0);
		infoPanel.add(validityVerfLab, c);
		rows++;

		// 查阅权限
		accessAuthorityCommon = new AccessAuthorityCommon(rows, infoPanel, insets, flowId, 0, dialog, isAdd, true);

		rows = accessAuthorityCommon.getRows();
		if (this.isShowProcessSupportTools != 0) {
			// 支持工具
			supportToolsCommon = new SupportToolsCommon(rows, infoPanel, insets, dialog, listFlowTool,
					isShowProcessSupportTools == 2);
			rows++;
		}

		if (this.isShowProcessEnclosure != 0) {
			fileEnclosureSelectCommon = new FileEnclosureSelectCommon(rows, infoPanel, insets, fileId, dialog,
					isShowProcessEnclosure == 2);
		}
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	public boolean isUpdate() {

		if (!this.flowNameField.getText().equals(oldFlowName)) {
			return true;
		}
		if (!this.flowNumField.getText().equals(oldFlowNumberId)) {
			return true;
		}
		if (fileContent != null) { // 文件内容更新
			return true;
		}
		// 流程类别
		if (categoryCommon != null) {
			if (categoryCommon.isUpdate()) {
				return true;
			}
		}
		// 流程类别
		if (businessTypeCommon != null) {
			if (businessTypeCommon.isUpdate()) {
				return true;
			}
		}
		// 责任人
		if (personliableSelect != null) {
			if (personliableSelect.getSingleSelectCommon().isUpdate()) {
				return true;
			}
		}
		// 流程监护人
		if (guardianPeopleSelectCommon != null) {
			if (guardianPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 流程拟制人
		if (fictionPeopleSelectCommon != null) {
			if (fictionPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 责任部门
		if (resDepartSelectCommon != null) {
			if (resDepartSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 专员
		if (commissionerSelect != null) {
			if (commissionerSelect.isUpdate()) {
				return true;
			}
		}
		if (keyWordField != null) {
			if (keyWordField.isUpdate()) {
				return true;
			}
		}
		// 有效期
		String newValidityVaule = "";
		if (this.validityCheckBox.isSelected()) {
			newValidityVaule = "0";
		} else {
			newValidityVaule = this.validityField.getText().trim();
		}
		if (!this.validityVaule.equals(newValidityVaule)) {
			return true;
		}
		// 查阅权限
		if (accessAuthorityCommon != null) {
			if (accessAuthorityCommon.isUpdate()) {
				return true;
			}
		}
		// 支持工具
		if (supportToolsCommon != null) {
			if (supportToolsCommon.isUpdate()) {
				return true;
			}
		}
		// 附件
		if (fileEnclosureSelectCommon != null) {
			if (fileEnclosureSelectCommon.isUpdate()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 监护人名称
	 * 
	 * @return
	 */
	public String getGuardianName() {
		if (this.guardianPeopleSelectCommon != null) {
			return guardianPeopleSelectCommon.getNameResult();
		}
		return "";
	}

	/**
	 * 拟制人名称
	 * 
	 * @return
	 */
	public String getFictionPeopleName() {
		if (this.fictionPeopleSelectCommon != null) {
			return fictionPeopleSelectCommon.getNameResult();
		}
		return "";
	}

	/***************************************************************************
	 * 确定
	 */
	public ProcessInfoBean okBut() {
		ProcessInfoBean processInfoBean = new ProcessInfoBean();

		processInfoBean.setFlowId(flowId);
		processInfoBean.setFlowNumber(flowNumField.getText().trim());
		processInfoBean.setValidityValue(DrawCommon.isNullOrEmtry(this.validityField.getText()) ? 0 : Integer
				.parseInt(this.validityField.getText()));
		// 流程类别
		Long typeId = null;
		if (this.categoryCommon != null) {
			typeId = categoryCommon.getTypeResultId();
		}
		processInfoBean.setTypeId(typeId);
		String bussType = "";
		if (businessTypeCommon != null) {
			bussType = businessTypeCommon.getKey();
		}
		processInfoBean.setBussType(bussType);
		// 流程责任人
		int resPersonType = personliableSelect.getPeopleResType();
		Long resPersonId = personliableSelect.getSingleSelectCommon().getIdResult();
		processInfoBean.setResPersonId(resPersonId);
		processInfoBean.setResPersonType(resPersonType);
		// 监护人
		Long guardianId = null;
		if (this.guardianPeopleSelectCommon != null) {
			guardianId = guardianPeopleSelectCommon.getIdResult();
		}
		processInfoBean.setGuardianId(guardianId);
		// 拟制人
		Long fictionPeopleId = null;
		if (this.fictionPeopleSelectCommon != null) {
			fictionPeopleId = fictionPeopleSelectCommon.getIdResult();
		}
		processInfoBean.setFictionPeopleId(fictionPeopleId);
		// 责任部门
		Long resOrgId = this.resDepartSelectCommon.getIdResult();
		processInfoBean.setResOrgId(resOrgId);
		// 查阅权限
		int isPublic = this.accessAuthorityCommon.getPublicStatic();
		processInfoBean.setIsPublic(isPublic);
		/*
		 * String orgAccessAuthorityids = accessAuthorityCommon.getOrgIds();
		 * processInfoBean.setOrgAccessAuthorityids(orgAccessAuthorityids);
		 * String posAccessAuthorityids = accessAuthorityCommon.getPosIds();
		 * processInfoBean.setPosAccessAuthorityids(posAccessAuthorityids);
		 * String posGroupAccessAuthorityids =
		 * accessAuthorityCommon.getPosGroupIds();
		 * processInfoBean.setPosGroupAccessAuthorityids
		 * (posGroupAccessAuthorityids);
		 */
		AccessId accessId = new AccessId();
		// 部门查阅
		accessId.setOrgAccessId(accessAuthorityCommon.getOrgAccessIds());
		// 岗位查阅
		accessId.setPosAccessId(accessAuthorityCommon.getPosAccessIds());
		// 岗位组查阅
		accessId.setPosGroupAccessId(accessAuthorityCommon.getPosGroupAcessIds());
		processInfoBean.setAccId(accessId);
		// 支持工具
		String supportToolIds = "";
		if (supportToolsCommon != null) {
			supportToolIds = supportToolsCommon.getResultIds();
		}
		processInfoBean.setSupportToolIds(supportToolIds);
		// 附件
		Long fileId = null;
		if (this.fileEnclosureSelectCommon != null) {
			fileId = fileEnclosureSelectCommon.getResultFileId();
		}
		processInfoBean.setFileId(fileId);
		// 保密级别
		processInfoBean.setConfidentialityLevel(this.accessAuthorityCommon.getConfidentialityLevelStatic());

		if (this.keyWordField != null) {
			processInfoBean.setKeyWord(this.keyWordField.getResultStr());
		}
		if (commissionerSelect != null) {
			processInfoBean.setCommissionerId(commissionerSelect.getIdResult());
		}
		return processInfoBean;
	}

	public String isValidate() {
		flowNumVerfLab.setText("");

		// 获取有效期
		String reg = "^[0-9]*$";
		String strValidity = this.validityField.getText();
		strValidity = strValidity == null ? null : strValidity.trim();
		if (validityCheckBox.isSelected()) {
		} else {
			if (strValidity == null || "".equals(strValidity.trim())) {
				flowNumVerfLab.setText(JecnProperties.getValue("validNotNullC"));
			} else if (!strValidity.matches(reg)) {
				flowNumVerfLab.setText(JecnProperties.getValue("integerGreaterThan"));
			} else if (strValidity.length() > 5 || Integer.parseInt(strValidity) > 9000) {// /如果输入的有效期大于9000则提示重新输入
				// “strValidity.length() >
				// 5”判断是为了避免Integer.parseInt(strValidity)报错，不能超过9000个月
				flowNumVerfLab.setText(JecnProperties.getValue("validSuperfluousMonthC"));
			} else if (Integer.parseInt(strValidity) <= 0) {
				flowNumVerfLab.setText(JecnProperties.getValue("validSuperfluousZeroC"));
			}
		}
		if (this.flowId == -1 && (fileContent == null || fileContent.getBytes() == null)) {
			flowNumVerfLab.setText(JecnProperties.getValue("fileUp"));
		}

		// 关键字
		if (keyWordField != null) {
			if (DrawCommon.checkNameMaxLength(keyWordField.getResultStr().trim())) {
				flowNumVerfLab.setText(JecnProperties.getValue("keyWord") + JecnProperties.getValue("lengthNotBaiOut"));
			}
		}

		// 流程编号验证
		if (DrawCommon.checkNameMaxLength(flowNumField.getText().trim())) {
			flowNumVerfLab.setText(JecnProperties.getValue("actBaseNum") + JecnProperties.getValue("lengthNotBaiOut"));
		}

		// 流程名称
		String flowName = flowNameField.getText().toString().trim();
		if (DrawCommon.isNullOrEmtryTrim(flowName)) {
			// 名称不能为空
			flowNumVerfLab.setText(JecnProperties.getValue("flowName") + JecnProperties.getValue("isNotEmpty"));
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(flowName)) {
			flowNumVerfLab.setText(JecnProperties.getValue("flowName") + JecnProperties.getValue("notInputThis"));
		} else if (JecnUserCheckUtil.getTextLength(flowName) > 122) {
			// 不能超过122个字符或61个汉字
			flowNumVerfLab.setText(JecnProperties.getValue("flowName") + JecnProperties.getValue("lengthNotBaiOut"));
		}

		return flowNumVerfLab.getText();
	}

	public JecnTextField getFlowNameField() {
		return flowNameField;
	}

	public Long getFlowId() {
		return flowId;
	}

	public JecnTextField getFlowNumField() {
		return flowNumField;
	}

	public FlowFileContent getFileContent() {
		return fileContent;
	}
}
