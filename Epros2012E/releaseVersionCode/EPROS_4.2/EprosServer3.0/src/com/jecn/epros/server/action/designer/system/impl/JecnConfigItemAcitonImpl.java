package com.jecn.epros.server.action.designer.system.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.designer.system.IJecnConfigItemAciton;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.service.system.IJecnConfigItemService;

/**
 * 
 * 配置信息表（JECN_CONFIG_ITEM）操作类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigItemAcitonImpl implements IJecnConfigItemAciton {
	private final Log log = LogFactory.getLog(JecnConfigItemAcitonImpl.class);

	/** 业务处理对象 */
	private IJecnConfigItemService configService = null;

	/**
	 * 
	 * 查询流程图设置值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 流程图设置值
	 */
	public List<JecnConfigItemBean> select(int typeBigModel) {
		if (!check(typeBigModel)) {
			return new ArrayList<JecnConfigItemBean>();
		}
		return configService.select(typeBigModel);
	}

	/**
	 * 
	 * 查询流程文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程文件排序后显示数据
	 */
	@Override
	public List<JecnConfigItemBean> selectShowFlowFile() {
		return configService.selectShowFlowFile();
	}

	/**
	 * 
	 * 查询流程建设规范显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程建设规范显示数据
	 */
	public List<JecnConfigItemBean> selectShowPartMapStand() {
		return configService.selectShowPartMapStand();
	}

	/**
	 * 
	 * 查询任务审批环节数据（带文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节数据（带文控类型）
	 */
	public List<JecnConfigItemBean> selectShowTaskApp(int typeBigModel) {
		if (!check(typeBigModel)) {
			return new ArrayList<JecnConfigItemBean>();
		}
		return configService.selectShowTaskApp(typeBigModel);
	}

	/**
	 * 
	 * 查询任务审批环节数据（除去文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节显示数据
	 */
	public List<JecnConfigItemBean> selectTaskAppTable(int typeBigModel) {
		if (!check(typeBigModel)) {
			return new ArrayList<JecnConfigItemBean>();
		}
		return configService.selectTaskAppTable(typeBigModel);
	}

	/**
	 * 
	 * 查询所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 */
	public List<JecnConfigItemBean> selectTaskShowAllItems(int typeBigModel) {
		if (!check(typeBigModel)) {
			return new ArrayList<JecnConfigItemBean>();
		}
		return configService.selectTaskShowAllItems(typeBigModel);
	}

	/**
	 * 
	 * 查询所有表示公开秘密数据集合
	 * 
	 * @return List<JecnConfigItemBean> 所有表示公开秘密数据集合
	 */
	public List<JecnConfigItemBean> selectAllPub() {
		return configService.selectAllPub();
	}

	/**
	 * 
	 * 修改JECN_CONFIG_ITEM表
	 * 
	 * @param configItemList
	 *            List<JecnConfigItemBean>
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return boolean true:修改成功；false：修改失败
	 */
	public boolean update(List<JecnConfigItemBean> configItemList, int typeBigModel) {
		try {
			return configService.update(configItemList, typeBigModel);
		} catch (Exception ex) {
			log.error("JecnConfigItemAcitonImpl类 update方法：", ex);
			return false;
		}
	}

	/**
	 * 
	 * 通过大类下唯一标示获取value字段值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String value字段值或NULL
	 */
	public String selectValue(int typeBigModel, String mark) {
		return configService.selectValue(typeBigModel, mark);
	}

	/**
	 * 
	 * 通过大类下唯一标示获取数据
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String 一行数据字段值或NULL
	 */
	public JecnConfigItemBean selectConfigItemBean(int typeBigModel, String mark) {
		return configService.selectConfigItemBean(typeBigModel, mark);
	}

	/**
	 * 查询流程基本信息
	 * 
	 * @return List<JecnConfigItemBean>查询流程基本信息
	 */
	@Override
	public List<JecnConfigItemBean> selectBaseInfoFlowFile() {
		return configService.selectBaseInfoFlowFile();
	}

	/**
	 * 获取系统配置项信息
	 * 
	 * @param typeBigModel
	 *            int 0：流程地图 ,1：流程图 ,2：制度, 3：文件, 4：邮箱配置, 5：权限配置
	 * @param typeSmallModel
	 *            int 0：审批环节配置, 1：任务界面显示项配置, 2：基本配置, 3：流程图建设规范 ,4：流程文件,
	 *            5：邮箱配置,6：权限配置
	 * @return List<JecnConfigItemBean> 系统配置项信息
	 * @throws Exception
	 */
	@Override
	public List<JecnConfigItemBean> selectConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
		return configService.selectConfigItemBeanByType(typeBigModel, typeSmallModel);
	}

	/**
	 * 
	 * 查询流程地图文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程文件排序后显示数据
	 */
	@Override
	public List<JecnConfigItemBean> selectFlowMapShowFlowFile() {
		return configService.selectFlowMapShowFlowFile();
	}

	/**
	 * 
	 * 满足规则返回true，不满足返回false
	 * 
	 * @param typeBigModel
	 *            int
	 * @return boolean
	 */
	private boolean check(int typeBigModel) {
		return (typeBigModel >= 0 && typeBigModel <= 5) ? true : false;
	}

	public IJecnConfigItemService getConfigService() {
		return configService;
	}

	public void setConfigService(IJecnConfigItemService configService) {
		this.configService = configService;
	}

	@Override
	public List<JecnConfigItemBean> selectShowSortConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
		return configService.selectShowSortConfigItemBeanByType(typeBigModel, typeSmallModel);
	}

	@Override
	public List<JecnConfigItemBean> selectTableConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
		return configService.selectTableConfigItemBeanByType(typeBigModel, typeSmallModel);
	}

	@Override
	public List<JecnConfigItemBean> selectKpiConfigItemBeanByTop(int top) {
		return configService.selectKpiConfigItemBeanByTop(top);
	}

	@Override
	public String selectValueFromCache(ConfigItemPartMapMark mark) {
		JecnConfigItemBean jecnConfigItemBean = JecnContants.configMaps.get(mark.toString());
		if (jecnConfigItemBean == null) {
			return null;
		}
		return jecnConfigItemBean.getValue();
	}

	/**
	 * 在缓存中获取 JecnConfigItemBean
	 * 
	 * @param mark
	 * @return
	 */
	@Override
	public JecnConfigItemBean selectItemBeanByMark(String mark) {
		return JecnContants.configMaps.get(mark);
	}

	@Override
	public void update(List<JecnConfigItemBean> updateItemList) {
		configService.update(updateItemList);
	}

	@Override
	public List<Object[]> selectMaintainNode() {
		return configService.selectMaintainNode();
	}

	@Override
	public JecnElementsLibrary getElement(String mark) {
		return configService.getElement(mark);
	}
}
