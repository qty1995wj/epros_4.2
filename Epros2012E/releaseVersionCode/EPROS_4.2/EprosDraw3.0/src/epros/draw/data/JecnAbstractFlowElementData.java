package epros.draw.data;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;

import org.apache.commons.lang.StringUtils;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.GeoEnum;
import epros.draw.data.figure.JecnImageLabelData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.designer.JecnDesignerFigureData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 图形数据、线数据的基类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAbstractFlowElementData implements Cloneable {

	/** 数据库流程元素唯一标识 */
	protected long flowElementId = -1;
	/** 当前画板 流程元素唯一标识(随机生成UUID.randomUUID().toString()) */
	protected String flowElementUUID = null;
	/** 元素唯一标识，画图面板保存后，根据此标识判断是否相同元素 (随机生成UUID.randomUUID().toString()) */
	protected String UUID;
	/** 流程元素类型 */
	protected MapElemType mapElemType = null;

	/** 元素形狀 */
	protected MapElemType eleShape = null;
	/** 当前放大缩小倍数 */
	protected double workflowScale = 1.0;

	/** 流程元素宽 */
	protected int figureSizeX = 0;
	/** 流程元素高 */
	protected int figureSizeY = 0;

	// ///////////////////字体 start/////////////////////
	/** 字体颜色 默认黑色 */
	protected Color fontColor = null;
	/** 字体位置（0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下），默认正中4 */
	protected int fontPosition = 4;
	/** 字体样式 默认宋体 */
	protected String fontName = "宋体";
	/** 字形样式（普通、加粗BOLD、斜线ITALIC） 默认普通样式PLAIN（0） ，1 加粗 */
	protected int fontStyle = Font.PLAIN;
	/** 字号即字体大小 默认值12 */
	protected int fontSize = 12;
	/** 字体排列方式 false:字体横排 true:字体竖排 默认横排 */
	protected boolean fontUpRight = false;
	// ///////////////////字体 end/////////////////////

	/** 填充:none为未填充,one为单色效果,two为双色效果 默认未填充单色效果 */
	protected FillType fillType = FillType.one;
	/** 背景颜色(填充颜色是双色时，此字段标识颜色1；填充颜色是单色时，此字段是单色填充颜色) */
	protected Color fillColor = null;
	/** 颜色2：当双色时才使用此颜色 */
	protected Color changeColor = null;
	/** 渐变方向 */
	protected FillColorChangType fillColorChangType = null;

	/** 线条图元轮廓呈现属性集合:由bodyWidth字段+bodyStyle字段创建而成 */
	protected BasicStroke basicStroke = null;
	/** 线条类型：默认实线 ； 0：实线；1：虚线；2：点线；3：双线 */
	protected int bodyStyle = 0;
	/** 线条颜色 */
	protected Color bodyColor = null;
	/** 线条粗细（线条宽度）(1~10) */
	protected int bodyWidth = 1;

	/** 阴影 True：存在阴影，False：不存在阴影 */
	protected boolean shadowsFlag = false;
	/** 阴影颜色 */
	protected Color shadowColor = null;

	/** 3D效果标识 True：存在3D效果，False：不存在3D效果 */
	protected boolean flag3D = false;

	/** 旋转角度值 右旋 + 90°，左旋 -90° （单项箭头和双向箭头选择角度为45°） */
	protected double currentAngle = 0.0;

	/** 普通、3D、阴影 默认普通 */
	protected GeoEnum geoEnum = GeoEnum.general;

	/** 流程元素文字内容 */
	protected String figureText = null;
	/** 名称文本框 打开流程 默认的记录元素当前值 figureText 自动获取的为 流程节点值 */
	protected String oldNameText = null;
	/** 流程元素上显示的图标的数据对象 */
	protected JecnImageLabelData imageLabelData = null;

	/** 是否显示编辑四个点 true:显示 false：不显示 */
	protected boolean editPoint = false;
	/** 流程元素层级 */
	protected int zOrderIndex = -10;
	/** 说明，如果是活动，显示为活动说明 */
	protected String avtivityShow;

	/** 是否支持双击：isDoubleClick */
	protected boolean isDoubleClick = false;
	/** 是否支持快速添加图形：isSortAddFigure */
	protected boolean isSortAddFigure = false;
	/** 设计器 */
	protected JecnDesignerFigureData designerFigureData = null;

	public void setDesignerFigureData(JecnDesignerFigureData designerFigureData) {
		this.designerFigureData = designerFigureData;
	}

	public JecnAbstractFlowElementData(MapElemType mapElemType) {
		if (!DrawCommon.isFlowElementType(mapElemType)) {
			JecnLogConstants.LOG_ABSTRACT_FLOW_ELEMENT_DATA.error("JecnAbstractFlowElementData类构造函数：参数不合法。mapElemType="
					+ mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FIAL);

		}
		this.mapElemType = mapElemType;
	}

	public MapElemType getMapElemType() {
		return mapElemType;
	}

	public void setMapElemType(MapElemType mapElemType) {
		if (!DrawCommon.isFlowElementType(mapElemType)) {
			JecnLogConstants.LOG_ABSTRACT_FLOW_ELEMENT_DATA.error("JecnAbstractFlowElementData类构造函数：参数不合法。mapElemType="
					+ mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FIAL);

		}
		this.mapElemType = mapElemType;
	}

	/**
	 * 获取字体样式
	 * 
	 * @return
	 */
	public Font getFont() {
		return new Font(fontName, fontStyle, fontSize);
	}

	/**
	 * 
	 * 图元轮廓大小
	 * 
	 * @return
	 */
	public BasicStroke getBasicStroke() {
		if (basicStroke == null) {// 默认画笔值 1.0
			basicStroke = JecnUIUtil.getBasicStrokeOne();
			// 默认线条
			this.bodyWidth = 1;
		}
		return basicStroke;
	}

	public int getBodyStyle() {
		return bodyStyle;
	}

	public int getBodyWidth() {
		return bodyWidth;
	}

	/**
	 * 设置线条类型，并且设置线条图元轮廓呈现属性集合
	 * 
	 * @param bodyStyle
	 *            int
	 */
	public void setBodyStyle(int bodyStyle) {
		this.bodyStyle = bodyStyle;
		if (this.getBodyColor() == null) {
			this.setBodyWidth(0);
		}
		basicStroke = getBodyBasicStroke(bodyStyle, this.getBodyWidth());
	}

	/**
	 * 
	 * 设置线条粗细，并且设置线条图元轮廓呈现属性集合
	 * 
	 * @param bodyWidth
	 */
	public void setBodyWidth(int bodyWidth) {
		if (this.getBodyColor() == null) {
			this.bodyWidth = 0;
		} else {
			this.bodyWidth = bodyWidth;
		}
		basicStroke = getBodyBasicStroke(this.getBodyStyle(), bodyWidth);
	}

	public Color getBodyColor() {
		return bodyColor;
	}

	public void setBodyColor(Color bodyColor) {
		this.bodyColor = bodyColor;
	}

	public boolean isShadowsFlag() {
		return shadowsFlag;
	}

	public void setShadowsFlag(boolean shadowsFlag) {
		if (DrawCommon.isNOShadowAnd3D(mapElemType)) {// 不允许设置阴影效果或3D效果
			return;
		}
		this.shadowsFlag = shadowsFlag;
		setGeoEnumByXml();
	}

	public boolean isFlag3D() {
		return flag3D;
	}

	public void setFlag3D(boolean flag3D) {
		if (DrawCommon.isNOShadowAnd3D(mapElemType)) {// 不允许设置阴影效果或3D效果
			return;
		}
		this.flag3D = flag3D;
		setGeoEnumByXml();
	}

	public double getCurrentAngle() {
		return currentAngle;
	}

	public void setCurrentAngle(double currentAngle) {
		this.currentAngle = currentAngle;
	}

	public GeoEnum getGeoEnum() {
		return geoEnum;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public Color getFontColor() {
		return fontColor;
	}

	public void setFontColor(Color fontColor) {
		this.fontColor = fontColor;
	}

	public int getFontPosition() {
		return fontPosition;
	}

	public void setFontPosition(int fontPosition) {
		this.fontPosition = fontPosition;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public int getFontStyle() {
		return fontStyle;
	}

	public void setFontStyle(int fontStyle) {
		this.fontStyle = fontStyle;
	}

	public int getFontSize() {
		return fontSize;
	}

	public boolean getFontUpRight() {
		return fontUpRight;
	}

	public void setFontUpRight(boolean fontUpRight) {
		this.fontUpRight = fontUpRight;
	}

	public long getFlowElementId() {
		return flowElementId;
	}

	public void setFlowElementId(long flowElementId) {
		this.flowElementId = flowElementId;
	}

	/**
	 * 获取当前面板下的放大缩小倍数
	 * 
	 * @return JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale()
	 */
	public double getWorkflowScale() {
		return JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
	}

	public void setWorkflowScale(double workflowScale) {
		this.workflowScale = workflowScale;
	}

	public int getFigureSizeX() {
		return figureSizeX;
	}

	public void setFigureSizeX(int figureSizeX) {
		this.figureSizeX = figureSizeX;
	}

	public int getFigureSizeY() {
		return figureSizeY;
	}

	public void setFigureSizeY(int figureSizeY) {
		this.figureSizeY = figureSizeY;
	}

	/**
	 * 阴影默认颜色
	 * 
	 * @return
	 */
	public Color getShadowColor() {
		return shadowColor;
	}

	public void setShadowColor(Color shadowColor) {
		this.shadowColor = shadowColor;
	}

	public String getFigureText() {
		return figureText;
	}

	public void setFigureText(String figureText) {
		if (DrawCommon.checkStringSame(this.figureText, figureText)) {// 当前值和原来值相等是不更新
			return;
		}
		if (StringUtils.isNotBlank(figureText) && figureText.endsWith("\n")) {
			figureText = figureText.substring(0, figureText.length() - 1);
		}
		this.figureText = figureText;
	}

	public void setGeoEnum(GeoEnum geoEnum) {
		this.geoEnum = geoEnum;
	}

	public Color getFillColor() {
		if ((FillType.two == this.fillType || FillType.one == this.fillType) && fillColor == null) {
			return fillColor = JecnUIUtil.getDefaultBackgroundColor();
		}
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		// 处理数据层，处理无填充色元素
		if (DrawCommon.isNoSetFillColor(this.mapElemType)) {
			this.fillType = FillType.none;
			return;
		}
		this.fillColor = fillColor;
	}

	public Color getChangeColor() {
		if (FillType.two == this.fillType && changeColor == null) {
			return changeColor = JecnUIUtil.getDefaultBackgroundColor();
		}
		return changeColor;
	}

	public void setChangeColor(Color changeColor) {
		// 处理数据层，处理无填充色元素
		if (DrawCommon.isNoSetFillColor(this.mapElemType)) {
			this.fillType = FillType.none;
			return;
		}
		this.changeColor = changeColor;
	}

	public FillColorChangType getFillColorChangType() {
		return fillColorChangType;
	}

	public void setFillColorChangType(FillColorChangType fillColorChangType) {
		this.fillColorChangType = fillColorChangType;
	}

	public FillType getFillType() {
		return fillType;
	}

	public void setFillType(FillType fillType) {
		this.fillType = fillType;
	}

	public JecnImageLabelData getImageLabelData() {
		return imageLabelData;
	}

	public void setImageLabelData(JecnImageLabelData imageLabelData) {
		this.imageLabelData = imageLabelData;
	}

	public boolean getEditPoint() {
		return editPoint;
	}

	public void setEditPoint(boolean editPoint) {
		this.editPoint = editPoint;
	}

	/**
	 * 
	 * 获取绘制线条边框BasicStroke对象
	 * 
	 * @return BasicStroke
	 */
	public BasicStroke getBodyBasicStroke() {
		return getBodyBasicStroke(this.bodyStyle, this.bodyWidth);
	}

	/**
	 * 获取绘制线条边框BasicStroke对象
	 * 
	 * @param bodyStyle
	 *            int 线条类型：默认实线 ； 0：实线；1：虚线；2：点线；3：双线
	 * @param bodyWidth
	 *            int 线条宽（1~10）
	 * @return BasicStroke
	 * 
	 */
	public BasicStroke getBodyBasicStroke(int bodyStyle, int bodyWidth) {
		BasicStroke basicStroke = null;
		if (bodyWidth == 0) {
			return new BasicStroke(bodyWidth);
		}
		switch (bodyStyle) {
		case 0:// 实线
			basicStroke = new BasicStroke(bodyWidth);
			break;
		case 1:// 虚线
			float[] dottedLineSpace = { 10.0f * bodyWidth };
			basicStroke = new BasicStroke(bodyWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f,
					dottedLineSpace, 0.0f);
			break;
		case 2:// 点画线
			float[] pointDrawLineSpace = { 5.0f };
			basicStroke = new BasicStroke(bodyWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f,
					pointDrawLineSpace, 0.0f);
			break;
		case 3:// 点线
			float[] pointLineSpace = { 2.0f * bodyWidth };
			basicStroke = new BasicStroke(bodyWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f,
					pointLineSpace, 0.0f);
			break;
		default:
			basicStroke = new BasicStroke(bodyWidth);
			break;
		}
		return basicStroke;
	}

	public int getZOrderIndex() {
		return zOrderIndex;
	}

	public void setZOrderIndex(int orderIndex) {
		this.zOrderIndex = orderIndex;
	}

	public String getAvtivityShow() {
		return avtivityShow;
	}

	public void setAvtivityShow(String avtivityShow) {
		this.avtivityShow = avtivityShow;
	}

	
	public String getOldNameText() {
		return oldNameText;
	}

	public void setOldNameText(String oldNameText) {
		this.oldNameText = oldNameText;
	}

	/**
	 * 
	 * 两个流程元素数据对象：newData属性值赋值给oldData属性值
	 * 
	 * @param newData
	 *            JecnAbstractFlowElementData 提供属性值的对象
	 * 
	 */
	public void converFlowElementData(JecnAbstractFlowElementData newData) {
		converFlowElementData(this, newData);
	}

	/**
	 * 
	 * 两个流程元素数据对象：newData属性值赋值给oldData属性值
	 * 
	 * @param oldData
	 *            JecnAbstractFlowElementData 待更新属性值的对象
	 * @param newData
	 *            JecnAbstractFlowElementData 提供属性值的对象
	 * 
	 */
	public void converFlowElementData(JecnAbstractFlowElementData oldData, JecnAbstractFlowElementData newData) {
		if (oldData == null || oldData == null) {
			return;
		}

		// ******************** 老版配置 START ********************//
		// 边框颜色
		oldData.setBodyColor(newData.getBodyColor());
		// 边框类型：0 1 2 3
		oldData.setBodyStyle(newData.getBodyStyle());
		// 流程元素宽
		oldData.setFigureSizeX(newData.getFigureSizeX());
		// 流程元素高
		oldData.setFigureSizeY(newData.getFigureSizeY());

		// 填充类型（填充类型：无填充、单色、双色）
		oldData.setFillType(newData.getFillType());
		// 填充颜色
		oldData.setFillColor(newData.getFillColor());
		// 填充颜色2
		oldData.setChangeColor(newData.getChangeColor());
		// 渐变方向类型(topTilt:向上倾斜 bottom:向下倾斜 vertical:垂直 horizontal:水平)
		oldData.setFillColorChangType(newData.getFillColorChangType());

		// 字体颜色
		oldData.setFontColor(newData.getFontColor());
		// 字体位置 0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
		oldData.setFontPosition(newData.getFontPosition());
		// 字体大小
		oldData.setFontSize(newData.getFontSize());
		// 字体竖排 1:竖排文字，0：不是竖排文字
		oldData.setFontUpRight(newData.getFontUpRight());
		// ******************** 老版配置 END ********************//

		// 字形样式（0:普通 1:加粗、2:斜线） 目前支持加粗和普通
		oldData.setFontStyle(newData.getFontStyle());
		// 字体样式 默认宋体
		oldData.setFontName(newData.getFontName());
		// 阴影 0:无阴影 1：有阴影
		oldData.setShadowsFlag(newData.isShadowsFlag());
		// 阴影颜色
		oldData.setShadowColor(newData.getShadowColor());

		// 线条粗细 (1~10)
		oldData.setBodyWidth(newData.getBodyWidth());
		// 3D效果 True：存在3D效果，False：不存在3D效果
		oldData.setFlag3D(newData.isFlag3D());
		// 是否显示编辑点
		oldData.setEditPoint(newData.getEditPoint());
		// 说明
		oldData.setAvtivityShow(newData.getAvtivityShow());

		// 旋转角度值 右旋 + 90°，左旋 -90° （单项箭头和双向箭头选择角度为45°）
		oldData.setCurrentAngle(newData.getCurrentAngle());
		oldData.setEleShape(newData.getEleShape());
		if (oldData.getEleShape() != null) {
			oldData.setMapElemType(newData.getMapElemType());
		}
	}

	/**
	 * 根据3D、阴影状态设置GeoEnum 类型
	 * 
	 */
	public void setGeoEnumByXml() {
		if (flag3D && shadowsFlag) {// 既是3D也是阴影
			this.geoEnum = GeoEnum.shadowAnd3D;
		} else if (!shadowsFlag && flag3D) {// 3D效果
			this.geoEnum = GeoEnum.f3d;
		} else if (shadowsFlag && !flag3D) {// 阴影效果
			this.geoEnum = GeoEnum.shadow;
		} else if (!shadowsFlag && !flag3D) {// 既不是阴影也不是3D效果,设为普通效果
			this.geoEnum = GeoEnum.general;
		}
	}

	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {
		if (srcFlowElementData == null || destFlowElementData == null) {
			JecnLogConstants.LOG_ABSTRACT_FLOW_ELEMENT_DATA
					.error("JecnFigureData类setFlowElementAttributes方法：参数不能为空。srcFlowElementData=" + srcFlowElementData
							+ ";destFlowElementData=" + destFlowElementData);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		// 流程元素唯一标识
		// destFlowElementData.setFlowElementId(srcFlowElementData
		// .getFlowElementId());

		// 流程元素类型
		destFlowElementData.setMapElemType(srcFlowElementData.getMapElemType());

		// 当前放大缩小倍数
		destFlowElementData.setWorkflowScale(srcFlowElementData.getWorkflowScale());
		// 流程元素宽
		destFlowElementData.setFigureSizeX(srcFlowElementData.getFigureSizeX());
		// 流程元素高
		destFlowElementData.setFigureSizeY(srcFlowElementData.getFigureSizeY());
		// 旋转角度值 右旋 + 90°，左旋 -90° （单项箭头和双向箭头选择角度为45°）
		destFlowElementData.setCurrentAngle(srcFlowElementData.getCurrentAngle());
		// 公共属性
		setFormatPainterFigureAttrs(srcFlowElementData, destFlowElementData);
		// 流程元素文字内容
		destFlowElementData.setFigureText(srcFlowElementData.getFigureText());
		
		destFlowElementData.setOldNameText(srcFlowElementData.getOldNameText());
		// 流程元素上显示的图标的数据对象TODO
		// destFlowElementData.setImageLabelData(srcFlowElementData.getImageLabelData());

		// 是否显示编辑四个点 true:显示 false：不显示
		destFlowElementData.setEditPoint(srcFlowElementData.getEditPoint());
		// 流程元素层级
		destFlowElementData.setZOrderIndex(srcFlowElementData.getZOrderIndex());
		// 是否支持双击：isDoubleClick
		destFlowElementData.setDoubleClick(srcFlowElementData.getDoubleClick());
		// 是否支持快速添加图形：isSortAddFigure
		destFlowElementData.setSortAddFigure(srcFlowElementData.getSortAddFigure());
		// 说明
		destFlowElementData.setAvtivityShow(srcFlowElementData.getAvtivityShow());
		// 设计器数据克隆
		destFlowElementData.setDesignerFigureData(srcFlowElementData.getDesignerFigureData().clone());
	}

	/**
	 * 复制格式刷属性(字体颜色、大小、样式、背景色、边框色、边框样式)
	 * 
	 * @param formatPainterData
	 *            格式刷元素对象
	 * @param elementData
	 *            复制的元素都花心
	 */
	public static void setFormatPainterFigureAttrs(JecnAbstractFlowElementData formatPainterData,
			JecnAbstractFlowElementData elementData) {
		// 字体颜色 默认黑色
		elementData.setFontColor(formatPainterData.getFontColor());
		// 字体位置（0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下），默认正中4
		elementData.setFontPosition(formatPainterData.getFontPosition());
		// 字体名称 默认宋体
		elementData.setFontName(formatPainterData.getFontName());
		// 字形样式（普通、加粗BOLD、斜线ITALIC） 默认普通样式PLAIN（0） ，1 加粗
		elementData.setFontStyle(formatPainterData.getFontStyle());
		// 字号即字体大小 默认值12
		elementData.setFontSize(formatPainterData.getFontSize());
		// 字体排列方式 false:字体横排 true:字体竖排 默认横排
		elementData.setFontUpRight(formatPainterData.getFontUpRight());
		// ///////////////////字体 end/////////////////////

		// 填充:none为未填充,one为单色效果,two为双色效果 默认未填充单色效果
		elementData.setFillType(formatPainterData.getFillType());
		// 背景颜色(填充颜色是双色时，此字段标识颜色1；填充颜色是单色时，此字段是单色填充颜色)
		elementData.setFillColor(formatPainterData.getFillColor());
		// 颜色2：当双色时才使用此颜色
		elementData.setChangeColor(formatPainterData.getChangeColor());
		// 渐变方向
		elementData.setFillColorChangType(formatPainterData.getFillColorChangType());

		// 线条图元轮廓呈现属性集合:由bodyWidth字段+bodyStyle字段创建而成 因此不用克隆

		// 线条类型：默认实线 ； 0：实线；1：虚线；2：点线；3：双线
		elementData.setBodyStyle(formatPainterData.getBodyStyle());
		// 线条颜色
		elementData.setBodyColor(formatPainterData.getBodyColor());
		// 线条粗细（线条宽度）(1~10)
		elementData.setBodyWidth(formatPainterData.getBodyWidth());

		// 阴影 True：存在阴影，False：不存在阴影
		elementData.setShadowsFlag(formatPainterData.isShadowsFlag());
		// 阴影颜色
		elementData.setShadowColor(formatPainterData.getShadowColor());

		// 3D效果标识 True：存在3D效果，False：不存在3D效果
		elementData.setFlag3D(formatPainterData.isFlag3D());
		// 普通、3D、阴影 默认普通
		elementData.setGeoEnum(formatPainterData.getGeoEnum());

		elementData.setEleShape(formatPainterData.getEleShape());
	}

	public void setFigureArrts(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {
		// 流程元素层级
		destFlowElementData.setZOrderIndex(srcFlowElementData.getZOrderIndex());
	}

	/**
	 * 
	 * 
	 * 把给定的流程元素数据赋值给当前流程元素
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData) {
		if (srcFlowElementData == null) {
			return;
		}

		this.setFlowElementAttributes(srcFlowElementData, this);
	}

	/**
	 * 
	 * 克隆当前对象
	 * 
	 */
	protected JecnAbstractFlowElementData currClone() {
		JecnAbstractFlowElementData flowElementDataClone = newInstance();
		if (flowElementDataClone == null) {
			return null;
		}
		// 复制属性
		flowElementDataClone.setFlowElementAttributes(this, flowElementDataClone);

		if (this.designerFigureData != null) {
			flowElementDataClone.designerFigureData = this.designerFigureData.clone();
		}
		return flowElementDataClone;
	}

	public String getFlowElementUUID() {
		return flowElementUUID;
	}

	public void setFlowElementUUID(String flowElementUUID) {
		this.flowElementUUID = flowElementUUID;
	}

	public boolean getDoubleClick() {
		return isDoubleClick;
	}

	public void setDoubleClick(boolean isDoubleClick) {
		this.isDoubleClick = isDoubleClick;
	}

	public boolean getSortAddFigure() {
		return isSortAddFigure;
	}

	public void setSortAddFigure(boolean isSortAddFigure) {
		this.isSortAddFigure = isSortAddFigure;
	}

	/** 实例化流程元素对象 */
	public abstract JecnAbstractFlowElementData newInstance();

	/** 克隆对象 */
	public abstract JecnAbstractFlowElementData clone();

	public MapElemType getEleShape() {
		return eleShape;
	}

	public void setEleShape(MapElemType eleShape) {
		this.eleShape = eleShape;
	}

	public JecnDesignerFigureData getDesignerFigureData() {
		if (designerFigureData == null) {
			designerFigureData = new JecnDesignerFigureData();
		}
		return designerFigureData;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
}
