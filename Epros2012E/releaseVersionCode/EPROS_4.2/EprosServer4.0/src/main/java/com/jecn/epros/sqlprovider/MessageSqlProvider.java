package com.jecn.epros.sqlprovider;

public class MessageSqlProvider {

    public String saveMessage() {

        String id = "";
        String value = "";
        if (BaseSqlProvider.isOracle()) {
            id = " MESSAGE_ID,";
            value = " JECN_MESSAGE_SQUENCE.nextval,";
        }
        String sql = "insert into JECN_MESSAGE(" + id
                + "PEOPLE_ID,MESSAGE_CONTENT,MESSAGE_TOPIC,OR_READ,INCEPT_PEOPLE_ID,CREATE_TIME,MESSAGE_TYPE) values("
                + value
                + "#{peopleId},#{messageContent},#{messageTopic},#{noRead},#{inceptPeopleId},#{createTime},#{messageType})";

        return sql;
    }

}
