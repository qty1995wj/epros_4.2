package epros.designer.gui.pagingImage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

/***
 * 显示面板
 * 
 * @author fuzhh
 * 
 */
public class SplitPageImageJPanel extends JPanel {

	/**
	 * 拼装角色面板和截取面板
	 * 
	 * @param splitPageJPanel
	 *            截取面板
	 * @param splitPageRoleJPanel
	 *            角色面板
	 * @param pageWidth
	 *            面板宽度
	 * @param pageHight
	 *            面板高度
	 * @param i
	 *            面板序号
	 */
	public SplitPageImageJPanel(SplitPreviewJPanel splitPreviewJPanel, SplitPageRoleJPanel splitPageRoleJPanel,
			int pageWidth, int pageHight, int i, Color workflowColor) {
		// 设置布局方式
		FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT);
		flowLayout.setHgap(0);
		flowLayout.setVgap(0);
		this.setLayout(flowLayout);
		if (i != 0) {
			this.add(splitPageRoleJPanel);
			this.add(splitPreviewJPanel);
		} else {
			this.add(splitPreviewJPanel);
		}

		// 设置panel大小
		this.setPreferredSize(new Dimension(pageWidth, pageHight));
		this.setBackground(workflowColor);

		this.setBorder(new MatteBorder(1, 1, 1, 1, Color.black));
	}
}
