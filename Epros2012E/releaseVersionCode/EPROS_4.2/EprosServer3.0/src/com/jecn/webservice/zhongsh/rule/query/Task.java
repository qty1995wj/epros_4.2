
package com.jecn.webservice.zhongsh.rule.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Task complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Task">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaskId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaskStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreNodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reviewer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkflowTemplateName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Task_Create_Time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Workflow_Start_Time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Prereviewer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Executiver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XmlString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmisUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FormId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubmiterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StarterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TemplateId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TacheName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompileDept" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="URLType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsInvalid" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Task", propOrder = {
    "taskId",
    "instanceId",
    "taskStatus",
    "preNodeName",
    "reviewer",
    "workflowTemplateName",
    "taskCreateTime",
    "workflowStartTime",
    "prereviewer",
    "executiver",
    "xmlString",
    "smisUrl",
    "formId",
    "projectName",
    "submiterName",
    "starterName",
    "documentName",
    "nodeName",
    "templateId",
    "version",
    "tacheName",
    "compileDept",
    "urlType",
    "isInvalid"
})
public class Task {

    @XmlElement(name = "TaskId")
    protected String taskId;
    @XmlElement(name = "InstanceId")
    protected String instanceId;
    @XmlElement(name = "TaskStatus")
    protected String taskStatus;
    @XmlElement(name = "PreNodeName")
    protected String preNodeName;
    @XmlElement(name = "Reviewer")
    protected String reviewer;
    @XmlElement(name = "WorkflowTemplateName")
    protected String workflowTemplateName;
    @XmlElement(name = "Task_Create_Time", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar taskCreateTime;
    @XmlElement(name = "Workflow_Start_Time", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar workflowStartTime;
    @XmlElement(name = "Prereviewer")
    protected String prereviewer;
    @XmlElement(name = "Executiver")
    protected String executiver;
    @XmlElement(name = "XmlString")
    protected String xmlString;
    @XmlElement(name = "SmisUrl")
    protected String smisUrl;
    @XmlElement(name = "FormId")
    protected String formId;
    @XmlElement(name = "ProjectName")
    protected String projectName;
    @XmlElement(name = "SubmiterName")
    protected String submiterName;
    @XmlElement(name = "StarterName")
    protected String starterName;
    @XmlElement(name = "DocumentName")
    protected String documentName;
    @XmlElement(name = "NodeName")
    protected String nodeName;
    @XmlElement(name = "TemplateId")
    protected String templateId;
    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "TacheName")
    protected String tacheName;
    @XmlElement(name = "CompileDept")
    protected String compileDept;
    @XmlElement(name = "URLType")
    protected String urlType;
    @XmlElement(name = "IsInvalid")
    protected boolean isInvalid;

    /**
     * 获取taskId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * 设置taskId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskId(String value) {
        this.taskId = value;
    }

    /**
     * 获取instanceId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * 设置instanceId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstanceId(String value) {
        this.instanceId = value;
    }

    /**
     * 获取taskStatus属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskStatus() {
        return taskStatus;
    }

    /**
     * 设置taskStatus属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskStatus(String value) {
        this.taskStatus = value;
    }

    /**
     * 获取preNodeName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreNodeName() {
        return preNodeName;
    }

    /**
     * 设置preNodeName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreNodeName(String value) {
        this.preNodeName = value;
    }

    /**
     * 获取reviewer属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReviewer() {
        return reviewer;
    }

    /**
     * 设置reviewer属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReviewer(String value) {
        this.reviewer = value;
    }

    /**
     * 获取workflowTemplateName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkflowTemplateName() {
        return workflowTemplateName;
    }

    /**
     * 设置workflowTemplateName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkflowTemplateName(String value) {
        this.workflowTemplateName = value;
    }

    /**
     * 获取taskCreateTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaskCreateTime() {
        return taskCreateTime;
    }

    /**
     * 设置taskCreateTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaskCreateTime(XMLGregorianCalendar value) {
        this.taskCreateTime = value;
    }

    /**
     * 获取workflowStartTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWorkflowStartTime() {
        return workflowStartTime;
    }

    /**
     * 设置workflowStartTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWorkflowStartTime(XMLGregorianCalendar value) {
        this.workflowStartTime = value;
    }

    /**
     * 获取prereviewer属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrereviewer() {
        return prereviewer;
    }

    /**
     * 设置prereviewer属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrereviewer(String value) {
        this.prereviewer = value;
    }

    /**
     * 获取executiver属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutiver() {
        return executiver;
    }

    /**
     * 设置executiver属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutiver(String value) {
        this.executiver = value;
    }

    /**
     * 获取xmlString属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlString() {
        return xmlString;
    }

    /**
     * 设置xmlString属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlString(String value) {
        this.xmlString = value;
    }

    /**
     * 获取smisUrl属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmisUrl() {
        return smisUrl;
    }

    /**
     * 设置smisUrl属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmisUrl(String value) {
        this.smisUrl = value;
    }

    /**
     * 获取formId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormId() {
        return formId;
    }

    /**
     * 设置formId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormId(String value) {
        this.formId = value;
    }

    /**
     * 获取projectName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * 设置projectName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectName(String value) {
        this.projectName = value;
    }

    /**
     * 获取submiterName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmiterName() {
        return submiterName;
    }

    /**
     * 设置submiterName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmiterName(String value) {
        this.submiterName = value;
    }

    /**
     * 获取starterName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStarterName() {
        return starterName;
    }

    /**
     * 设置starterName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStarterName(String value) {
        this.starterName = value;
    }

    /**
     * 获取documentName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * 设置documentName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentName(String value) {
        this.documentName = value;
    }

    /**
     * 获取nodeName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * 设置nodeName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeName(String value) {
        this.nodeName = value;
    }

    /**
     * 获取templateId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateId() {
        return templateId;
    }

    /**
     * 设置templateId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateId(String value) {
        this.templateId = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取tacheName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTacheName() {
        return tacheName;
    }

    /**
     * 设置tacheName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTacheName(String value) {
        this.tacheName = value;
    }

    /**
     * 获取compileDept属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompileDept() {
        return compileDept;
    }

    /**
     * 设置compileDept属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompileDept(String value) {
        this.compileDept = value;
    }

    /**
     * 获取urlType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURLType() {
        return urlType;
    }

    /**
     * 设置urlType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURLType(String value) {
        this.urlType = value;
    }

    /**
     * 获取isInvalid属性的值。
     * 
     */
    public boolean isIsInvalid() {
        return isInvalid;
    }

    /**
     * 设置isInvalid属性的值。
     * 
     */
    public void setIsInvalid(boolean value) {
        this.isInvalid = value;
    }

}
