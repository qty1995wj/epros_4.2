package epros.draw.gui.key;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import sun.misc.BASE64Decoder;

import com.sun.org.apache.xml.internal.security.utils.Base64;

import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnOPUtil;
import epros.draw.util.JecnResourceUtil;

public class JecnKeyProces {
	/** 内容Key */
	private final String KEY_NAME = "key.license";

	/** 密钥内容 */
	public static String value = null;

	/**
	 * 
	 * 验证是否通过
	 * 
	 * @return String 通过返回NULL，没有通过返回提示信息
	 */
	public String checkKey() {
		// 读取文件内容：文件不存在或为空之间返回
		if (!readFileValue()) {
			return JecnResourceUtil.getJecnResourceUtil().getValue("infoNull");
		}

		return null;
	}

	/**
	 * 
	 * 读取文件内容
	 * 
	 * @return boolean true：读取成功；false;读取失败或内容为空
	 */
	private boolean readFileValue() {
		// 获取密钥文件内容
		getFileValue();
		if (DrawCommon.isNullOrEmtryTrim(value)) {// 为空
			return false;
		}

		boolean isNum = checkIsNumber(value.substring(0, 12));
		if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {// 设计器
			// 是否为数字
			if (isNum) {// 试用

			} else {

			}
		} else {// 本地
			// 是否为数字
			if (isNum) {// 试用
				// true：合法；false：不合法
				boolean isDate = isDateValid();
				if (!isDate) {
					return false;
				}
			} else {
				// true：合法；false：不合法
				boolean isMac = isMacValid();
				if (!isMac) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * MAC地址验证
	 * 
	 * @return
	 */
	private boolean isMacValid() {
		// 验证是否通过：true通过；false不通过
		boolean flag = false;
		if (DrawCommon.isNullOrEmtryTrim(value) && value.length() < 17) {
			return flag;
		}
		String strMac = value.substring(0, 17);

		// 获取所有MAC地址，结构：A,B,C
		String macStr = getMACAddress();
		if (DrawCommon.isNullOrEmtryTrim(macStr)) {
			return flag;
		}
		// 拆分MAC地址
		String[] allMACAddress = macStr.split(",");

		for (int i = 0; i < allMACAddress.length; i++) {
			// MAC地址是否和密钥中MAC相等
			if (strMac.equals(allMACAddress[i])) {
				flag = true;
				break;
			}
		}

		return flag;
	}

	/**
	 * 
	 * 获取本机的MAC地址
	 * 
	 * @return
	 */
	private String getMACAddress() {
		// MAC地址
		String address = null;

		// 操作系统名称
		String os = System.getProperty("os.name");
		if (DrawCommon.isNullOrEmtryTrim(os)) {
			return address;
		} else if (os.startsWith("Windows")) {// Windows系统
			address = JecnOPUtil.getWindowsMACAddress();
		} else {// linux系统
			address = JecnOPUtil.getLinuxMACAddress();
		}
		return address;
	}

	/**
	 * 日期验证
	 * 
	 * @return
	 */
	private boolean isDateValid() {
		String dateValue = getDateStr();
		// 使用天数
		// 时间格式化对象
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// 时间验证 获取秘密开始时间
		Date date = null;
		try {
			date = sdf.parse(dateValue);
			if (new Date().after(date)) {// 是否到期
				// 到期
				return false;
			}
			return true;
		} catch (ParseException e) {
		}
		return false;
	}

	/**
	 * 获取日期
	 * 
	 * @return
	 */
	private String getDateStr() {
		return value.substring(0, 8);
	}

	/**
	 * 
	 * 获取给定注册文件内容
	 * 
	 */
	private void getFileValue() {
		// 获取Properties文件
		Properties properties = JecnFileUtil.readPropertiesForFileName(JecnWorkflowConstant.KEY_FILE_NAME);

		if (properties == null) {// 文件不存在
			return;
		}
		// 文件内容
		String license = properties.getProperty(KEY_NAME);
		if (DrawCommon.isNullOrEmtryTrim(license)) {// 空
			return;
		}
		// 获取解密后密钥内容
		try {
			value = decrypt(license);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param curStr
	 *            传入的参数
	 * @return
	 */
	private boolean checkIsNumber(String curStr) {
		if (curStr.matches("[0-9]*")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 校验密钥，提示用户
	 * 
	 * @return String 返回提示信息或NULL
	 */
	String checkKeyTipUser() {
		String lastData = isLastData();
		if (!DrawCommon.isNullOrEmtryTrim(lastData)) {
			return JecnResourceUtil.getJecnResourceUtil().getValue("infoVailBefore") + lastData
					+ JecnResourceUtil.getJecnResourceUtil().getValue("infoVailAfter");
		}
		return null;
	}

	private boolean isTest() {
		return checkIsNumber(value.substring(0, 12));
	}

	/**
	 * 
	 * 当密钥有效期只有一个月就开始提示密钥快过期，知道密钥过期位置
	 * 
	 * @param valueArray
	 *            String[]
	 * @return String 有效期时间
	 */
	private String isLastData() {
		if (!isTest()) {
			return null;
		}
		// 开始时间
		String dateStr = getDateStr();
		// 时间格式化对象
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

		try {
			// 时间验证 获取秘密到期时间
			Date date = format.parse(dateStr);
			Calendar maxVailedC = Calendar.getInstance();
			maxVailedC.setTime(date);

			// // 开始时间+使用天数
			// maxVailedC.add(Calendar.DATE, Integer.parseInt(days)); // 日期加

			Calendar upMaxVailedC = (Calendar) maxVailedC.clone();
			// 获取最大有效时间的上一月
			upMaxVailedC.add(Calendar.MONTH, -1);

			Calendar sysCurrC = Calendar.getInstance();

			// 最大有效期前一个月<M<最大有效期
			String lastData = null;
			if (sysCurrC.after(upMaxVailedC) && sysCurrC.before(maxVailedC)) {
				lastData = format.format(maxVailedC.getTime());
			}

			return lastData;

		} catch (ParseException e) {
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * 解密
	 * 
	 * @param keyValue
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String keyValue) throws Exception {
		byte[] byteDecrypt = decode(keyValue);
		return new String(decryptBASE64(new String(byteDecrypt)));
	}

	/**
	 * <p>
	 * BASE64字符串解码为二进制数据
	 * </p>
	 * 
	 * @param base64
	 * @return
	 * @throws Exception
	 */
	public static byte[] decode(String base64) throws Exception {
		return Base64.decode(base64.getBytes());
	}

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private static byte[] decryptBASE64(String key) throws Exception {
		return (new BASE64Decoder()).decodeBuffer(key);
	}

	public String getValue() {
		return value;
	}
}
