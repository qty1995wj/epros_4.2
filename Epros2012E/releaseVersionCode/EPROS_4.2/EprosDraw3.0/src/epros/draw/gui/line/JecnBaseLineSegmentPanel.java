package epros.draw.gui.line;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.resize.JecnLineResizePanel;
import epros.draw.gui.line.shape.DrawArc;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 
 * 小线段基类
 * 
 * 小线段是指画面面板上实际的线段组件
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnBaseLineSegmentPanel extends JPanel {
	/** 连接线 */
	protected JecnBaseManhattanLinePanel baseManhattanLine = null;
	/** 小线段数据对象 */
	protected JecnLineSegmentData lineSegmentData = null;
	/** 与此小线段相交的线段及对应的弧 */
	protected Map<LineSegment, DrawArc> drawArcMap = new HashMap<LineSegment, DrawArc>();

	// ******************** 线段上对应的三个点 start ********************//
	/** 选中点：开始点 */
	protected JecnLineResizePanel lineResizePanelStart = null;
	/** 选中点：中间点 */
	protected JecnLineResizePanel lineResizePanelCenter = null;
	/** 选中点：结束点 */
	protected JecnLineResizePanel lineResizePanelEnd = null;

	// ******************** 线段上对应的三个点 end ********************//

	/**
	 * 
	 * 创建小线段构造函数
	 * 
	 * @param manhattanLine
	 *            ManhattanLine 连接线
	 * @param lineSegmentData
	 *            JecnLineSegmentData 小线段数据对象
	 */
	public JecnBaseLineSegmentPanel(
			JecnBaseManhattanLinePanel baseManhattanLine,
			JecnLineSegmentData lineSegmentData) {
		if (lineSegmentData == null || baseManhattanLine == null) {
			JecnLogConstants.LOG_BASE_LINE_SEGMENT_PANEL
					.error("JecnBaseLineSegmentPanel类构造函数：参数为null.lineSegmentData="
							+ lineSegmentData
							+ ";baseManhattanLine="
							+ baseManhattanLine);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.baseManhattanLine = baseManhattanLine;
		this.lineSegmentData = lineSegmentData;
		// 添加流程元素鼠标事件
		this.addMouseListener(JecnDrawMainPanel.getMainPanel());
		this.addMouseMotionListener(JecnDrawMainPanel.getMainPanel());
	}

	public JecnLineSegmentData getLineSegmentData() {
		return lineSegmentData;
	}

	/**
	 * 
	 * @return
	 */
	public JecnBaseManhattanLinePanel getBaseManhattanLine() {
		return baseManhattanLine;
	}

	/**
	 * 
	 * 获取线条粗细
	 * 
	 * @return int
	 */
	public int getLineWidth() {
		return this.getBaseManhattanLine().getFlowElementData().getBodyWidth();
	}

	/**
	 * 
	 * 线条图元轮廓呈现属性
	 * 
	 * @return BasicStroke
	 */
	public BasicStroke getBasicStroke() {
		return this.getBaseManhattanLine().getFlowElementData()
				.getBasicStroke();
	}

	/**
	 * 
	 * 获取填充颜色1
	 * 
	 * @return
	 */
	public Color getFillColor() {
		return this.getBaseManhattanLine().getFlowElementData().getBodyColor();
	}
}
