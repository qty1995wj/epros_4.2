package com.jecn.epros.domain.standard;

/**
 * 标准清单相关文件信息
 *
 * @author hyl
 */
public class StandardRelatePRFBean {

    private Long relatedId;
    private String relatedName;
    /**
     * 1:流程，2：制度,3:活动 4:文件
     */
    private Integer relatedType;

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public Integer getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(Integer relatedType) {
        this.relatedType = relatedType;
    }
}
