package com.jecn.epros.server.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.util.JecnUtil;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Image;

public class JecnFinal {
	public static Logger log = Logger.getLogger(JecnFinal.class);

	public static final String NOPUBLIC = "noPublic";
	/** 选择文件已删除！ */
	public static final String IS_DELETE = "isDelete";
	/** 文件存放的根目录* */
	public static final String FILE_DIR = "eprosFileLibrary/";
	/** 保存流程或流程地图时候 保存流程图或流程地图的根目录* */
	public static final String FLOW_CHART = "flowChart/";
	/** *发布流程或流程地图时候 发布流程或流程地图的程序文件* */
	public static final String FLOW_FILE_RELEASE = "flowFileRelease/";
	/** 发布制度时候，发布制度的程序文件* */
	public static final String RULE_FILE_RELEASE = "ruleFileRelease/";
	public static final String STANDARD_FILE_RELEASE = "standardFileRelease/";
	/** 文件存放的根目录* */
	public static final String FILE_LIBRARY = "fileLibrary/";
	/** 存放临时文件夹 */
	public static final String TEMP_DIR = "templet_dir/";
	/** 流程或流程地图发布生成的图片目录 */
	public static final String PUB_FLOW_CHART = "flowChartRelease/";
	/** 人员变更excel数据导出 zhangft 2013-6-5 */
	public static final String PERSONNEL_CHANGES_DATA = "personnelChangesData/";
	/** 报表错误数据目录 */
	public static final String REPORT_ERROR_DIR = "reportErrorData/";
	/** 流程操作说明头信息(公司logo)*/
	public static final String FLOW_HEADINFO = "flowHeadInfo/";

	/** */

	/**
	 * 获取文件临时目录
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @return
	 */
	public static String saveTempFilePath() {
		return FILE_DIR + TEMP_DIR + JecnCommon.getUUID() + ".doc";
	}

	/**
	 * 获取文件临时目录
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @return
	 */
	public static String saveXmlFilePath() {
		return FILE_DIR + TEMP_DIR + JecnCommon.getUUID() + ".xml";
	}

	/**
	 * 获取文件临时目录
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @return
	 */
	public static String saveExcelTempFilePath() {
		return FILE_DIR + TEMP_DIR + JecnCommon.getUUID() + ".xls";
	}

	/**
	 * @author yxw 2012-11-13
	 * @description:返回临时文件全路径
	 * @param filePath
	 * @return
	 */
	public static String getAllTempPath(String filePath) {
		return JecnContants.jecn_path + filePath;
	}

	/**
	 * @author fuzhh Nov 7, 2012
	 * @description：获得临时文件的路径
	 * @param suffex
	 * @return
	 * @throws Exception
	 */
	public static byte[] getTempFileBytes(String filePath) throws Exception {
		byte[] bytes = JecnFinal.getBytesByFilePath(filePath);
		JecnFinal.deleteFile(filePath);
		return bytes;
	}

	/**
	 * @author zhangft 2013-6-5
	 * @description: 返回人员数据变更xml目录
	 * @return filePath
	 */
	public static String savePersonnelChangesDataPath() {
		return FILE_DIR + PERSONNEL_CHANGES_DATA;
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：启动服务时候，生成本地文件库的根目录
	 */
	public static void startService() {
		String rootPath = JecnContants.jecn_path + FILE_DIR;
		// 创建根目录
		File file = new File(rootPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 创建流程图或流程地图保存的根目录
		file = new File(rootPath + FLOW_CHART);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 创建流程图或流程地图发布的根目录
		file = new File(rootPath + PUB_FLOW_CHART);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 创建流程或流程地图发布的程序文件
		file = new File(rootPath + FLOW_FILE_RELEASE);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 创建制度发布的程序文件
		file = new File(rootPath + RULE_FILE_RELEASE);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 创建文件库
		file = new File(rootPath + FILE_LIBRARY);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 临时文件库
		file = new File(rootPath + TEMP_DIR);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 人员变更数据导出文件库 zhangft 2013-6-5
		file = new File(rootPath + PERSONNEL_CHANGES_DATA);
		if (!file.exists()) {
			file.mkdirs();
		}

		// 报表错误数据目录
		file = new File(rootPath + REPORT_ERROR_DIR);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 流程操作说明头信息(公司logo)
		file = new File(rootPath + FLOW_HEADINFO);
		if (!file.exists()) {
			file.mkdirs();
		}

	}

	/**
	 * @author zhangchen May 30, 2012
	 * @description：通过日期获得字符串
	 * @param date
	 * @return
	 */
	public static String getStringbyDate(Date date) {
		return new SimpleDateFormat("yyyyMM").format(date);
	}

	/***************************************************************************
	 * @author zhangchen Nov 1, 2012
	 * @description：获得文件全路径
	 * @param rootPathType
	 *            文件保存类型的根目录
	 * @param createDate
	 *            文件创建的时间
	 * @param suffixName
	 *            文件的后缀名
	 * @return
	 */
	public static String saveFilePath(String rootPathType, Date createDate, String suffixName) {
		File file = new File(JecnContants.jecn_path + FILE_DIR + rootPathType + getStringbyDate(createDate));
		if (!file.exists()) {
			file.mkdir();
		}
		return FILE_DIR + rootPathType + getStringbyDate(createDate) + "/" + JecnCommon.getUUID() + suffixName;
	}

	/**
	 * @author zhangchen May 30, 2012
	 * @description：获得文件的后缀名
	 * @param fileName
	 * @param fileId
	 * @return
	 */
	public static String getSuffixName(String fileName) {
		if (fileName.indexOf(".") == -1) {
			return "";
		}
		return fileName.substring(fileName.lastIndexOf("."), fileName.length());
	}

	/**
	 * @author zhangchen May 30, 2012
	 * @description：保存流程地图和流程图
	 * @param bytes
	 * @return
	 * @throws IOException
	 */
	public static String getImagePathFileAndSave(byte[] bytes) throws IOException {
		String filePath = JecnFinal.saveFilePath(FLOW_CHART, new Date(), ".jpg");
		JecnFinal.createFile(filePath, bytes);
		return filePath;
	}

	/**
	 * 保存分页流程图
	 * 
	 * @author fuzhh 2014-2-21
	 * @param byteList
	 * @return
	 * @throws IOException
	 */
	public static void savePagingImage(List<byte[]> byteList, String fileName) throws IOException {
		int i = 1;
		for (byte[] data : byteList) {
			String filePath = JecnFinal.savePagingImagePath(FLOW_CHART, new Date(), ".jpg", fileName, i);
			JecnFinal.createFile(filePath, data);
			i++;
		}
	}

	/**
	 * 获取图片分页路径
	 * 
	 * @author fuzhh 2014-2-21
	 * @param rootPathType
	 *            文件保存类型的根目录
	 * @param createDate
	 *            文件创建的时间
	 * @param suffixName
	 *            文件的后缀名
	 * @return
	 */
	public static String savePagingImagePath(String rootPathType, Date createDate, String suffixName, String fileName,
			int i) {
		File file = new File(JecnContants.jecn_path + FILE_DIR + rootPathType + getStringbyDate(createDate) + "/"
				+ fileName);
		if (!file.exists()) {
			file.mkdir();
		}
		return FILE_DIR + rootPathType + getStringbyDate(createDate) + "/" + fileName + "/" + i + suffixName;
	}

	/**
	 * @author zhangchen May 30, 2012
	 * @description：保存流程地图和流程图
	 * @param bytes
	 * @return
	 * @throws IOException
	 */
	public static String getImagePathByPub(byte[] bytes) throws IOException {
		String filePath = JecnFinal.saveFilePath(PUB_FLOW_CHART, new Date(), ".jpg");
		JecnFinal.createFile(filePath, bytes);
		return filePath;
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：创建文件的路径
	 * @param filePath
	 * @param bytes
	 * @throws IOException
	 */
	public static void createFile(String filePath, byte[] bytes) throws IOException {
		File file = new File(JecnContants.jecn_path + filePath);

		FileOutputStream out = null;
		try {
			file.createNewFile();
			out = new FileOutputStream(file, false);
			out.write(bytes);
			out.flush();
		} catch (IOException ex) {
			log.error("写出异常", ex);
			throw ex;
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				log.error("关闭输出流异常", ex);
			}
		}
	}

	/**
	 * 写人临时文件
	 * 
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	public static File writeFileToTempDir(byte[] bytes) throws Exception {
		String temporaryFilePath = JecnFinal.getAllTempPath(JecnFinal.saveExcelTempFilePath());
		return wirteFileToTempDirByPath(temporaryFilePath, bytes);
	}

	public static File wirteFileToTempDirByPath(String path, byte[] bytes) throws Exception {
		File file = new File(path);
		OutputStream out = null;
		try {
			out = new FileOutputStream(file);
			out.write(bytes);
			out.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		return file;
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：创建文件的路径
	 * @param filePath
	 * @param bytes
	 * @throws IOException
	 */
	public static void updateFile(String filePath, String fileOldPath, byte[] bytes) throws IOException {
		deleteFile(fileOldPath);
		createFile(filePath, bytes);
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：删除文件
	 * @param filePath
	 */
	public static void deleteFile(String filePath) {
		if (filePath == null || "".equals(filePath)) {
			log.info("JecnFinal中方法deleteFile：文件丢失");
			return;
		}
		File file = new File(JecnContants.jecn_path + filePath);
		if (file.exists()) {
			file.delete();
		} else {
			log.info("JecnFinal中方法deleteFile：文件丢失");
		}
	}

	/**
	 * 删除文件夹
	 * 
	 * @author fuzhh 2014-2-24
	 * @param path
	 */
	public static boolean doDelete(String path) {
		File dir = new File(JecnContants.jecn_path + path);
		if (!dir.exists()) {
			return false;
		}
		File[] subs = dir.listFiles();
		for (File sub : subs) {
			sub.delete();
		}
		return dir.delete();
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：通过路径获得文件的字节
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	public static byte[] getBytesByFilePath(String filePath) throws Exception {
		if (filePath == null || "".equals(filePath)) {
			log.info("JecnFinal中方法getBytesByFilePath：filePath=" + filePath);
			return null;
		}
		File file = new File(JecnContants.jecn_path + filePath);
		if (file.exists()) {
			return JecnUtil.getByte(file);
		} else {
			log.info("JecnFinal中方法getBytesByFilePath：文件丢失 filePath=" + filePath);
			return null;
		}
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：通过路径获得文件的字节
	 * @param filePath
	 *            文件全路径
	 * @return
	 * @throws Exception
	 */
	public static byte[] getByteByPath(String filePath) throws Exception {
		if (filePath == null || "".equals(filePath)) {
			log.info("JecnFinal中方法getByteByPath参数为空：filePath=" + filePath);
			return null;
		}
		File file = new File(filePath);
		if (file.exists()) {
			return JecnUtil.getByte(file);
		} else {
			log.info("JecnFinal中方法getByteByPath：文件丢失 filePath=" + filePath);
			return null;
		}
	}

	/**
	 * 获得Image对象
	 * 
	 * @param filePath
	 * @return
	 * @throws BadElementException
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws Exception
	 */
	public static Image getImage(String filePath) throws BadElementException, MalformedURLException, IOException,
			Exception {
		if (filePath == null || "".equals(filePath)) {
			log.info("JecnFinal中方法getImage：文件丢失");
			return null;
		}
		File file = new File(JecnContants.jecn_path + filePath);
		if (file.exists()) {
			return Image.getInstance(JecnUtil.getByte(file));
		} else {
			log.info("JecnFinal中方法getImage：文件丢失");
			return null;
		}
	}

	/**
	 * 获取File
	 * 
	 * @author fuzhh 2014-1-16
	 * @param path
	 * @return
	 */
	public static File checkPathFile(String path) {
		if (SyncTool.isNullOrEmtry(path)) {
			return null;
		}
		File file = new File(path);
		if (file != null && file.exists()) {
			return file;
		}
		return null;
	}

	/**
	 * dom4j 获取document
	 * 
	 * @author fuzhh 2014-1-16
	 * @param path
	 * @return
	 */
	public static Document getDocumentByXml(String path) {
		if (SyncTool.isNullOrEmtry(path)) {
			return null;
		}
		try {
			SAXReader reader = new SAXReader();
			Document doc = null;
			File file = checkPathFile(path);
			if (file != null) {
				doc = reader.read(file);
			}
			return doc;
		} catch (Exception e) {
			log.error("dom4J 获取document异常！", e);
			return null;
		}
	}

	public static String saveFileToLocal(byte[] bytes, String fileName, String fileDir) throws IOException {
		if (bytes == null || StringUtils.isEmpty(fileName)) {
			return null;
		}
		// 文件后缀名称
		String suffixName = JecnFinal.getSuffixName(fileName);
		// 保存数据库路径
		String filePath = JecnFinal.saveFilePath(fileDir, new Date(), suffixName);
		// 创建文件
		JecnFinal.createFile(filePath, bytes);
		return filePath;
	}
}
