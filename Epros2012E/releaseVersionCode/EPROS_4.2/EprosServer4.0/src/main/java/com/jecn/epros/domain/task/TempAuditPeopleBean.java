package com.jecn.epros.domain.task;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 记录审核人主键ID和名称
 *
 * @author ZHAGNXIAOHU
 */
public class TempAuditPeopleBean {
    /**
     * 审核人人员主键ID
     */
    private String auditId;
    /**
     * 审核人真实姓名
     */
    private String auditName;
    /**
     * 各阶段审核人标题名称
     */
    private String auditLabel;
    /**
     * 审批阶段 特殊阶段（整理意见）10
     */
    private int state;
    /**
     * 审批阶段Id
     */
    private long stageId;
    /**
     * 是否审批 2:为当前任务 1：已审批，0 未审批
     */
    private int isApproval;
    /**
     * 是否显示 1：显示 0不显示
     */
    private int isShow;
    /**
     * 审批时间
     */
    private String appDate;
    /**
     * 1：必填；0：不必填
     */
    private int isEmpty = 0;
    /**
     * 1：选人；0：没有选人
     */
    private int isSelectedUser = 0;

    public List<LinkResource> getTableAuditIdNames() {
        List<LinkResource> tableAuditIdNames = new ArrayList<LinkResource>();
        if (auditId != null && auditName != null) {
            String[] auditIds = auditId.split(",");
            String[] auditNames = auditName.split("\n");
            for (int i = 0; i < auditIds.length; i++) {
                tableAuditIdNames.add(new LinkResource(auditIds[i], auditNames[i], ResourceType.PERSON));
            }
        }
        return tableAuditIdNames;
    }

    public List<JecnUser> getAudits() {
        List<JecnUser> auditUsers = new ArrayList<>();
        if (StringUtils.isEmpty(auditId) || StringUtils.isEmpty(auditName)) {
            return auditUsers;
        }
        String[] ids = auditId.split(",");
        String[] names = auditName.split("\n");
        for (int index = 0; index < ids.length; index++) {
            auditUsers.add(new JecnUser(Long.parseLong(ids[index]), names[index]));
        }
        return auditUsers;
    }

    public int getIsSelectedUser() {
        return isSelectedUser;
    }

    public void setIsSelectedUser(int isSelectedUser) {
        this.isSelectedUser = isSelectedUser;
    }

    public String getAuditId() {
        return auditId;
    }

    public String getAuditName() {
        return auditName;
    }

    public void setAuditName(String auditName) {
        this.auditName = auditName;
    }

    public String getAuditLabel() {
        return auditLabel;
    }

    public void setAuditLabel(String auditLabel) {
        this.auditLabel = auditLabel;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getIsApproval() {
        return isApproval;
    }

    public void setIsApproval(int isApproval) {
        this.isApproval = isApproval;
    }

    public int getIsShow() {
        return isShow;
    }

    public void setIsShow(int isShow) {
        this.isShow = isShow;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public int getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(int isEmpty) {
        this.isEmpty = isEmpty;
    }

    public long getStageId() {
        return stageId;
    }

    public void setStageId(long stageId) {
        this.stageId = stageId;
    }

}
