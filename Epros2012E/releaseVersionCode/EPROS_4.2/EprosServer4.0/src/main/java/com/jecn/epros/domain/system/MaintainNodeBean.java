package com.jecn.epros.domain.system;

/**
 * Created by Administrator on 2018/11/21.
 */
public class MaintainNodeBean {
    //唯一主键
    private String id;
    //标识
    private String mark;
    //名称
    private String name;
    //英文名称
    private String enName;
    //默认名称
    private String defaultName;
    //默认英文名称
    private String defaultEnName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    public String getDefaultEnName() {
        return defaultEnName;
    }

    public void setDefaultEnName(String defaultEnName) {
        this.defaultEnName = defaultEnName;
    }
}
