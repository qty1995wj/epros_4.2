package epros.designer.gui.system;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.choose.DeptCompetecnTable;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

public abstract class MutilSelectCommon extends SelectCommon {
	/** Lab */
	private JLabel jecnLab = new JLabel(JecnProperties.getValue("departCompetenceC"));
	/** Area */
	protected DeptCompetecnTable jecnTable = null;
	/** 滚动面板 */
	private JScrollPane jecnScrollPane = null; // new
	/** ids */
	private String ids = "";
	/** ids */
	private Map<String, Object> idsAndDownLoadType = new HashMap<String, Object>();
	/** 选择按钮 */
	protected JButton selectBut = new JButton(JecnProperties.getValue("selectBtn"));

	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	private JecnDialog jecnDialog;

	protected JPanel buttoPanel;

	protected boolean isDownLoad = false;

	public MutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list, String labName,
			JecnDialog jecnDialog, boolean isRequest) {
		this.list = list;
		this.jecnDialog = jecnDialog;
		init(rows, infoPanel, insets, list, labName, jecnDialog, isRequest);
	}

	public MutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list, String labName,
			JecnDialog jecnDialog, boolean isRequest, boolean isDowload) {
		this.list = list;
		this.jecnDialog = jecnDialog;
		this.isDownLoad = isDowload;
		init(rows, infoPanel, insets, list, labName, jecnDialog, isRequest);
	}

	private void init(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list, String labName,
			JecnDialog jecnDialog, boolean isRequest) {
		jecnLab.setText(labName);
		//科大讯飞 需要根据 登录名重新查询人员名称
		getIflytekPeopleName(list);
		jecnTable = new DeptCompetecnTable(list, isDownLoad);
		jecnScrollPane = new JScrollPane();
		jecnScrollPane.setViewportView(jecnTable);
		jecnScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnScrollPane.setPreferredSize(new Dimension(200, 75));
		if (list != null && list.size() > 0) {
			StringBuffer sbIds = new StringBuffer();
			StringBuffer downLoadTypeArray = new StringBuffer();
			for (JecnTreeBean o : list) {
				sbIds.append(o.getId() + ",");
				downLoadTypeArray.append(o.isDownLoad() + ",");
			}
			ids = sbIds.substring(0, sbIds.length() - 1);
			idsAndDownLoadType.put("ids", sbIds.substring(0, sbIds.length() - 1));
			idsAndDownLoadType.put("isDownLoad", downLoadTypeArray.substring(0, downLoadTypeArray.length() - 1));
		}
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(jecnLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(jecnScrollPane, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(selectBut, c);
		if (isRequest) {
			c = new GridBagConstraints(4, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					new Insets(3, 1, 3, 1), 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnSelectChooseDialog jecnChooseDialog = getJecnSelectChooseDialog(getList(),
						MutilSelectCommon.this.jecnDialog);
				jecnChooseDialog.setVisible(true);
				if (jecnChooseDialog.isOperation()) {
					if (getList() != null && getList().size() > 0) {
						jecnTable.remoeAll();
						for (int i = 0; i < getList().size(); i++) {
							Vector vec = new Vector();
							vec.add(0, getList().get(i).getId());
							vec.add(1, getList().get(i).getName());
							vec.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), getList().get(i)
									.isDownLoad()));
							((DefaultTableModel) jecnTable.getModel()).addRow(vec);
						}
					} else {
						jecnTable.remoeAll();
					}
				}
			}
		});
	}

	public List<JecnTreeBean> getList() {
		return list;
	}

	public String getResultIds() {
		return JecnUtil.getIds(list);
	}

	// 科大讯飞 根据登录名 获取真实姓名
	protected void getIflytekPeopleName(List<JecnTreeBean> jecnTreeBean) {

	}

	public List<AccessId> getResultAccessIds() {
		int rowCount = jecnTable.getRowCount();
		int columnCount = jecnTable.getColumnCount();
		List<AccessId> list = new ArrayList<AccessId>();
		for (int i = 0; i < rowCount; i++) {
			AccessId access = new AccessId();
			Long id = Long.valueOf(jecnTable.getValueAt(i, 0).toString());// 获取选中Id
			access.setAccessId(id);
			JCheckBox box = null;
			if (columnCount > 2) {
				box = (JCheckBox) jecnTable.getValueAt(i, 2); // 获取选中状态
				access.setDownLoad(box.isSelected());
			} else {
				access.setDownLoad(false);
			}
			list.add(access);
		}
		return list;
	}

	public Set<Long> getResultIdsSet() {
		Set<Long> set = new HashSet<Long>();
		for (JecnTreeBean jecnTreeBean : list) {
			if (jecnTreeBean != null && jecnTreeBean.getId() != null) {
				set.add(jecnTreeBean.getId());
			}
		}
		return set;
	}

	// 获取IDS 集合
	private String[] getIds() {
		String[] strids = null;
		if (idsAndDownLoadType.get("ids") != null) {
			String ids = idsAndDownLoadType.get("ids").toString();
			if (!"".equals(ids)) {
				strids = ids.split(",");
			}
		}
		return strids;
	}

	// 获取下载权限集合
	private String[] getDownLoadTypeArray() {
		String[] strids = null;
		if (idsAndDownLoadType.get("isDownLoad") != null) {
			String isDownLoad = idsAndDownLoadType.get("isDownLoad").toString();
			if (!"".equals(isDownLoad)) {
				strids = isDownLoad.split(",");
			}
		}
		return strids;
	}

	/**
	 * 多选是否更新
	 * 
	 * @param oldStrIds
	 * @param list
	 * @return
	 */
	public boolean isUpdate() {
		String[] strids = getIds();
		String[] downLoadArray = getDownLoadTypeArray();
		if (strids != null || downLoadArray != null) {
			if (list.size() == 0) {
				return true;
			} else {
				if (strids.length != list.size()) {
					return true;
				} else {
					for (String id : strids) {
						boolean isExist = false;
						for (JecnTreeBean jecnTreeBean : list) {
							if (id.equals(jecnTreeBean.getId().toString())) {
								isExist = true;
							}
						}
						if (!isExist) {
							return true;
						}
					}
				}
				// 验证 是否有下载权限
				for (int i = 0; i < list.size(); i++) {
					for (int j = 0; j < downLoadArray.length; j++) {
						boolean isDownLoad = list.get(i).isDownLoad();
						if (!downLoadArray[i].toString().equals(String.valueOf(isDownLoad))) {
							return true;
						}
					}

				}

			}
		} else {
			if (list.size() > 0) {
				return true;
			}
		}
		return false;
	}

	public JLabel getJecnLab() {
		return jecnLab;
	}

	protected void hiddenCompotents() {
		this.jecnScrollPane.setEnabled(false);
		this.jecnLab.setEnabled(false);
		this.selectBut.setEnabled(false);
	}

	protected void showCompotents() {
		this.jecnScrollPane.setEnabled(true);
		this.jecnLab.setEnabled(true);
		this.selectBut.setEnabled(true);
	}

	public void disableButton() {
		selectBut.setEnabled(false);
	}

	protected abstract JecnSelectChooseDialog getJecnSelectChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog);
}
