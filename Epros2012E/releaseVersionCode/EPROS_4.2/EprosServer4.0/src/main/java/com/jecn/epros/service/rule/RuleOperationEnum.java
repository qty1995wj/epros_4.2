package com.jecn.epros.service.rule;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.process.processFile.BaseProcessFileBean;
import com.jecn.epros.domain.process.processFile.BaseProcessFileBean.ProcessFileType;
import com.jecn.epros.domain.process.processFile.ProcessFileTable;
import com.jecn.epros.domain.process.processFile.ProcessFileText;
import com.jecn.epros.domain.process.relatedFile.RelatedStandard;
import com.jecn.epros.mapper.RuleMapper;
import com.jecn.epros.mapper.StandardMapper;
import com.jecn.epros.security.AuthenticatedUser;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.process.ProcessFileEnum;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.ejb.config.JeeNamespaceHandler;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.stream.Collectors;

/**
 * 制度操作说明
 *
 * @author ZXH
 */
public enum RuleOperationEnum {
    INSTANCE;

    /**
     * 制度操作说明
     *
     * @param map
     * @param ruleMapper
     * @return
     * @throws Exception
     */
    public List<BaseProcessFileBean> getRuleOperationContents(Map<String, Object> map, RuleMapper ruleMapper, StandardMapper standardMapper)
            throws Exception {
        ProcessFileEnum.INSTANCE.setIsPub(valueOfInt(map.get("isPubs").toString()));
        ProcessFileEnum.INSTANCE.setHistoryId(Long.valueOf(map.get("historyId").toString()));
        List<BaseProcessFileBean> fileBeans = new ArrayList<>();
        long historyId = (long) map.get("historyId");
        List<Map<String, Object>> ruleTitles = null;
        List<Map<String, Object>> ruleFiles = null;
        List<Map<String, Object>> contentMaps = null;
        map.put("languageType", AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getLanguage());//加入国际化类型
        if (historyId == 0) { //判断是否存在historyId
            ruleTitles = ruleMapper.findRuleTitleByRuleId(map);
            if (ruleTitles == null) {
                return fileBeans;
            }
            // 1、RT.ID(模板titleId),JF.FILE_ID, JF.FILE_NAME, JF.DOC_ID
            ruleFiles = ruleMapper.findRuleFileByRuleId(map);
            // 2、制度模板，内容 RT.ID, RC.CONTENT
            contentMaps = ruleMapper.findRuleContentByRuleId(map);
        } else {
            ruleTitles = ruleMapper.findRuleTitleHistoryByRuleId(map);
            if (ruleTitles == null) {
                return fileBeans;
            }
            // 1、RT.ID(模板titleId),JF.FILE_ID, JF.FILE_NAME, JF.DOC_ID
            ruleFiles = ruleMapper.findRuleFileHistoryByRuleId(map);
            // 2、制度模板，内容 RT.ID, RC.CONTENT
            contentMaps = ruleMapper.findRuleContentHistoryByRuleId(map);

        }

        // 是否显示序号
        boolean isShowIndex = ConfigUtils.isShowDownLoadWordIndex();
        BaseProcessFileBean processFileBean = null;
        // ID,TITLE_NAME,T.TYPE
        for (Map<String, Object> titleMap : ruleTitles) {
            switch (valueOfInt(titleMap.get("TYPE"))) {// 0是内容，1是文件表单，2是流程表单；3流程架构；4标准；5风险
                case 0:
                    processFileBean = getFileText(titleMap.get("ID"), contentMaps);
                    break;
                case 1:// 1是文件表单
                    processFileBean = getRuleFileTable(ruleFiles, titleMap.get("ID"), historyId);
                    break;
                case 2:// 流程表单
                    map.put("isFlow", 1);
                    List<Map<String, Object>> relatedFlows = ruleMapper.findRuleRelatedFlowById(map);
                    processFileBean = getRelatedFlowTable(relatedFlows);
                    break;
                case 3:// 流程架构
                    map.put("isFlow", 0);
                    relatedFlows = ruleMapper.findRuleRelatedFlowById(map);
                    processFileBean = getRelatedFlowMapTable(relatedFlows);
                    break;
                case 4:// 标准
                    List<Map<String, Object>> ruleRelatedStandards = ruleMapper.findRuleRelatedStandardById(map);
                    Set<Long> standardIds = ruleRelatedStandards.stream().map(m -> Long.valueOf(m.get("CRITERION_CLASS_ID").toString())).collect(Collectors.toSet());
                    List<BaseBean> list = getStandardParentNames(standardMapper, standardIds);
                    processFileBean = getRelatedStandardTable(ruleRelatedStandards, list);
                    break;
                case 5:// 风险
                    List<Map<String, Object>> ruleRelatedRisks = ruleMapper.findRelatedRiskById(map);
                    processFileBean = getRelatedRiskTable(ruleRelatedRisks);
                    break;
                case 6:// 制度
                    List<Map<String, Object>> ruleRruleRisks = ruleMapper.findRelatedRuleById(map);
                    processFileBean = getRelatedRuleTable(ruleRruleRisks);
                    break;
                default:
                    break;
            }

            processFileBean.setTitle(valueOfString(titleMap.get("TITLE_NAME")));
            processFileBean.setShowIndex(isShowIndex);
            fileBeans.add(processFileBean);
        }
        return fileBeans;
    }

    private List<BaseBean> getStandardParentNames(StandardMapper standardMapper, Set<Long> ids) {
        if (ids == null || ids.isEmpty()) {
            return new ArrayList<>();
        }
        Map<String, Object> paramMap = new HashedMap();
        paramMap.put("ids", ids);
        return standardMapper.getRelatedParentNameByIds(paramMap);
    }

    private ProcessFileTable getRelatedFlowTable(List<Map<String, Object>> relatedFlows) {
        ProcessFileTable fileTable = getTableH();
        if (relatedFlows == null) {
            return fileTable;
        }
        // 表头
        addTwoCellRowTable(fileTable, JecnProperties.getValue("processNum"), JecnProperties.getValue("processName"));
        // key:FS.FLOW_ID, FS.FLOW_NAME, FS.FLOW_ID_INPUT
        for (Map<String, Object> map : relatedFlows) {
            addTwoCellRowTable(fileTable, valueOfString(map.get("FLOW_ID_INPUT")),
                    getLinks(valueOfString(map.get("FLOW_ID")), valueOfString(map.get("FLOW_NAME")), ResourceType.PROCESS));
        }

        return fileTable;
    }

    private ProcessFileTable getRelatedFlowMapTable(List<Map<String, Object>> relatedFlows) {
        ProcessFileTable fileTable = getTableH();
        if (relatedFlows == null) {
            return fileTable;
        }
        // 表头
        addTwoCellRowTable(fileTable, JecnProperties.getValue("processMapNum"), JecnProperties.getValue("mapName"));
        // key:FS.FLOW_ID, FS.FLOW_NAME, FS.FLOW_ID_INPUT
        for (Map<String, Object> map : relatedFlows) {
            addTwoCellRowTable(fileTable, valueOfString(map.get("FLOW_ID_INPUT")), getLinks(
                    valueOfString(map.get("FLOW_ID")), valueOfString(map.get("FLOW_NAME")), ResourceType.PROCESS_MAP));
        }
        return fileTable;
    }

    /**
     * 制度相关标准
     *
     * @param ruleRelatedStandards
     * @return
     */
    private ProcessFileTable getRelatedStandardTable(List<Map<String, Object>> ruleRelatedStandards, List<BaseBean> list) {
        ProcessFileTable fileTable = getTableH();
        if (ruleRelatedStandards == null) {
            return fileTable;
        }
        // 表头
        addTwoCellRowTable(fileTable, JecnProperties.getValue("standardName"), JecnProperties.getValue("dir"));
        // key:CRITERION_CLASS_ID,JCC.CRITERION_CLASS_NAME,JCC.STAN_TYPE
        for (Map<String, Object> map : ruleRelatedStandards) {
            addTwoCellRowTable(fileTable, getLinks(valueOfString(map.get("CRITERION_CLASS_ID")),
                    valueOfString(map.get("CRITERION_CLASS_NAME")), ResourceType.STANDARD), getStandardParentNames(list, Long.valueOf(valueOfString(map.get("CRITERION_CLASS_ID")))));
        }
        return fileTable;
    }

    private String getStandardParentNames(List<BaseBean> list, Long id) {
        if (list == null || list.size() == 0 || id == null) {
            return "";
        }
        String parentPathName = "";
        for (BaseBean file : list) {
            if (id.intValue() == file.getId().intValue()) {
                if (StringUtils.isBlank(parentPathName)) {
                    parentPathName = file.getName();
                } else {
                    parentPathName += "/" + file.getName();
                }
            }
        }
        return parentPathName;
    }

    /**
     * 制度相关风险
     *
     * @return
     */
    private ProcessFileTable getRelatedRiskTable(List<Map<String, Object>> ruleRelatedRisks) {
        ProcessFileTable fileTable = getTableH();
        if (ruleRelatedRisks == null) {
            return fileTable;
        }
        // 表头
        addTwoCellRowTable(fileTable, JecnProperties.getValue("riskNum"), JecnProperties.getValue("riskText"));
        // JR.ID, JR.NAME, JR.RISK_CODE
        for (Map<String, Object> map : ruleRelatedRisks) {
            addTwoCellRowTable(fileTable, valueOfString(map.get("RISK_CODE")),
                    getLinks(valueOfString(map.get("ID")), valueOfString(map.get("NAME")), ResourceType.RISK));
        }
        return fileTable;
    }

    /**
     * 制度相关制度
     *
     * @return
     */
    private ProcessFileTable getRelatedRuleTable(List<Map<String, Object>> ruleRelatedRules) {
        ProcessFileTable fileTable = getTableH();
        if (ruleRelatedRules == null) {
            return fileTable;
        }
        // 表头
        addTwoCellRowTable(fileTable, JecnProperties.getValue("ruleNum"), JecnProperties.getValue("ruleName"));
        // JR.ID, JR.NAME, JR.RISK_CODE
        for (Map<String, Object> map : ruleRelatedRules) {
            addTwoCellRowTable(fileTable, valueOfString(map.get("RULE_NUMBER")), getLinks(valueOfString(map.get("ID")), valueOfString(map.get("RULE_NAME")), ResourceType.RULE));
        }
        return fileTable;
    }

    private ProcessFileText getFileText(Object titleId, List<Map<String, Object>> contentMaps) throws Exception {
        ProcessFileText fileText = ProcessFileEnum.INSTANCE.createFileText();
        fileText.setContent(getContentByMaps(contentMaps, titleId));
        return fileText;
    }

    private String getContentByMaps(List<Map<String, Object>> contentMaps, Object titleId) throws Exception {
        for (Map<String, Object> map : contentMaps) {
            if (titleId.toString().equals(map.get("ID").toString())) {
                return getStringByClob(map.get("CONTENT"));
            }
        }
        return "";
    }

    private int valueOfInt(Object obj) {
        return Integer.valueOf(obj.toString());
    }

    private String valueOfString(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    /**
     * 过去制度操作说明-制度文件（表单、附件）
     *
     * @param ruleFiles
     * @param titleId
     * @return
     */
    private ProcessFileTable getRuleFileTable(List<Map<String, Object>> ruleFiles, Object titleId, long historyId) {
        ProcessFileTable fileTable = getTableH();
        if (ruleFiles == null || titleId == null) {
            return fileTable;
        }
        // 表头
        addTwoCellRowTable(fileTable, JecnProperties.getValue("documentNumber"), JecnProperties.getValue("documentName"));
        for (Map<String, Object> map : ruleFiles) {
            if (titleId.toString().equals(map.get("ID").toString())) {// 制度模板，titleId
                ProcessFileEnum.INSTANCE.setHistoryId(historyId);
                addTwoCellRowTable(fileTable, valueOfString(map.get("DOC_ID")),
                        getLinks(valueOfString(map.get("FILE_ID")), valueOfString(map.get("FILE_NAME")), ResourceType.FILE));
            }
        }
        return fileTable;
    }

    private String getStringByClob(Object object) throws Exception {
        if (object == null) {
            return "";
        }
        return ProcessFileEnum.INSTANCE.getStringByClob((Clob) object);
    }

    private List<LinkResource> getLinks(String id, String name, ResourceType resourceType) {
        return ProcessFileEnum.INSTANCE.getResourceLinks(id, name, resourceType);
    }

    private void addTwoCellRowTable(ProcessFileTable processFileTable, String content1, String content2) {
        ProcessFileEnum.INSTANCE.addTwoCellRowTable(processFileTable, content1, content2);
    }

    private void addTwoCellRowTable(ProcessFileTable processFileTable, String content1,
                                    List<LinkResource> linkResources) {
        ProcessFileEnum.INSTANCE.addTwoCellRowTable(processFileTable, content1, linkResources);
    }

    private void addTwoCellRowTable(ProcessFileTable processFileTable,
                                    List<LinkResource> linkResources, String content1) {
        ProcessFileEnum.INSTANCE.addTwoCellRowTable(processFileTable, linkResources, content1);
    }

    private ProcessFileTable getTableH() {
        ProcessFileTable processFileBean = new ProcessFileTable();
        processFileBean.setFileType(ProcessFileType.tableH);
        return processFileBean;
    }

    /**
     * 标准类型
     *
     * @param type
     * @return
     */
    private String getTypeName(String type) {
        return Common.standardTypeVal(Integer.valueOf(type));
    }
}
