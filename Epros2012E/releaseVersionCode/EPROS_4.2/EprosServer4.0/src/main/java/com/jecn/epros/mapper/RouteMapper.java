package com.jecn.epros.mapper;

import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.sqlprovider.RouteSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;


public interface RouteMapper {
    /**
     * 流程路径
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RouteSqlProvider.class, method = "findFlowRoute")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findFlowRoute(Map<String, Object> map);

    /**
     * 制度路径
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RouteSqlProvider.class, method = "findRuleRoute")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findRuleRoute(Map<String, Object> map);

    /**
     * 标准路径
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RouteSqlProvider.class, method = "findStandardRoute")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findStandardRoute(Map<String, Object> map);

    /**
     * 风险路径
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RouteSqlProvider.class, method = "findRiskRoute")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findRiskRoute(Map<String, Object> map);

    /**
     * 文件路径
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RouteSqlProvider.class, method = "findFileRoute")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findFileRoute(Map<String, Object> map);
}
