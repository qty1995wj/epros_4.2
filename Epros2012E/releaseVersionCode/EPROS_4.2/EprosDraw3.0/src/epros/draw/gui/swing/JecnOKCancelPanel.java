package epros.draw.gui.swing;

import java.awt.Color;
import java.awt.GridBagConstraints;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 确认取消按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOKCancelPanel extends JecnPanel {
	/** 确定 */
	private JButton okBtn = null;
	/** 取消 */
	private JButton cancelBtn = null;
	/** 空闲区域 */
	private JLabel freeLabel = null;

	public JecnOKCancelPanel() {

		initComponents();
		initBtnPanelLayout();
	}

	private void initComponents() {
		// 空闲区域
		freeLabel = new JLabel();
		// 确定
		okBtn = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue(JecnActionCommandConstants.OK_BUTTON));
		okBtn.setActionCommand(JecnActionCommandConstants.OK_BUTTON);
		// 取消
		cancelBtn = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				JecnActionCommandConstants.CANCEL_BUTTON));
		cancelBtn.setActionCommand(JecnActionCommandConstants.CANCEL_BUTTON);

		freeLabel.setOpaque(false);
		freeLabel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		freeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		freeLabel.setForeground(Color.red);
	}

	/**
	 * 
	 * 按钮区域
	 * 
	 */
	private void initBtnPanelLayout() {

		// 空闲区域
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTHEAST,
				GridBagConstraints.BOTH, JecnUIUtil.getInsets0(), 0, 0);
		this.add(freeLabel, c);

		// 确定
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		this.add(okBtn, c);

		// 取消
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		this.add(cancelBtn, c);

	}

	public JButton getOkBtn() {
		return okBtn;
	}

	public JButton getCancelBtn() {
		return cancelBtn;
	}

	public JLabel getFreeLabel() {
		return freeLabel;
	}
}
