package com.jecn.epros.domain.propose;

import java.util.ArrayList;
import java.util.List;

public class ProposeView {

    private Integer allNum = 0;
    private Integer readNum = 0;
    private Integer unReadNum = 0;
    private Integer acceptNum = 0;
    private Integer refuseNum = 0;
    private List<ProposePageView> proposes = new ArrayList<ProposePageView>();
    private Integer curState = 0;

    public Integer getAllNum() {
        return allNum;
    }

    public void setAllNum(Integer allNum) {
        this.allNum = allNum;
    }

    public Integer getReadNum() {
        return readNum;
    }

    public void setReadNum(Integer readNum) {
        this.readNum = readNum;
    }

    public Integer getUnReadNum() {
        return unReadNum;
    }

    public void setUnReadNum(Integer unReadNum) {
        this.unReadNum = unReadNum;
    }

    public Integer getAcceptNum() {
        return acceptNum;
    }

    public void setAcceptNum(Integer acceptNum) {
        this.acceptNum = acceptNum;
    }

    public Integer getRefuseNum() {
        return refuseNum;
    }

    public void setRefuseNum(Integer refuseNum) {
        this.refuseNum = refuseNum;
    }

    public List<ProposePageView> getProposes() {
		return proposes;
	}

	public void setProposes(List<ProposePageView> proposes) {
		this.proposes = proposes;
	}

	public Integer getCurState() {
        return curState;
    }

    public void setCurState(Integer curState) {
        this.curState = curState;
    }


}
