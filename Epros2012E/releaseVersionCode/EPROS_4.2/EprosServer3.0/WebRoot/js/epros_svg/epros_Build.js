/**
 * 定义Jquery 下载
 * @param options
 */
$.download = function (options) {
    if (options) {
        var inputs = "", submitForm;
        $.each(options.data, function (key, value) {
            inputs += key + "="+ value + "&";
        });
        window.open(options.url + "?" + inputs);
    }
};

/**
 * 等待框特效
 */
var spinHolder = (function spinner(holderid, R1, R2, count, stroke_width, colour) {
    var sectorsCount = count || 12,
        color = colour || "#fff",
        width = stroke_width || 15,
        r1 = Math.min(R1, R2) || 35,
        r2 = Math.max(R1, R2) || 60,
        cx = r2 + width,
        cy = r2 + width,
        r = Raphael(holderid, r2 * 2 + width * 2, r2 * 2 + width * 2),

        sectors = [],
        opacity = [],
        beta = 2 * Math.PI / sectorsCount,

        pathParams = {stroke: color, "stroke-width": width, "stroke-linecap": "round"};
    Raphael.getColor.reset();
    for (var i = 0; i < sectorsCount; i++) {
        var alpha = beta * i - Math.PI / 2,
            cos = Math.cos(alpha),
            sin = Math.sin(alpha);
        opacity[i] = 1 / sectorsCount * i;
        sectors[i] = r.path([["M", cx + r1 * cos, cy + r1 * sin], ["L", cx + r2 * cos, cy + r2 * sin]]).attr(pathParams);
        if (color == "rainbow") {
            sectors[i].attr("stroke", Raphael.getColor());
        }
    }
    var tick;
    (function ticker() {
        opacity.unshift(opacity.pop());
        for (var i = 0; i < sectorsCount; i++) {
            sectors[i].attr("opacity", opacity[i]);
        }
        r.safari();
        tick = setTimeout(ticker, 1000 / sectorsCount);
    }());
    return function () {
        clearTimeout(tick);
        r.remove();
    };
}("holder", 10, 15, 15, 3, "grey"));

(function () {
    var docE = window.document.documentElement,

        win = window,
        /**
         * 流程图数据对象
         */
        flowData = {
            zoomVal: 0,
            // 图形文本偏移量
            graphTextDiff: 5,
            // 绘制3d图形时，主图形缩放8像素
            graph3dDiff: 8,
            // 悬浮提示的文本样式
            toolTipStyle: {
                "font-family": "microsoft yahei",
                "font-size": 12,
                "font-anchor": "left-top"
            },

            iconNoClickEvent: ["POSITION", "KEY_POSITION"],

            connectionLines: ["CommonLine", "ManhattanLine"],

            activityGraphs: ["RoundRectWithLine", "DataImage", "Rhombus", "ITRhombus","ActiveFigureAR","ExpertRhombusAR"],

            iconShowWithTbl: ["ACTIVE_IN", "ACTIVE_OUT", "CONTROL_POINT", "KEY_CONTROL_POINT", "ACTIVE_STANDARD", "FILE"],
													   
            activityFlowGraphs: ['ActivityImplFigure','ActivityOvalSubFlowFigure'],
            
            navPaperHeight: 30

        },

    // 初始画布宽度（存在纵向滚动条时减去15px） 此处初始化时为了防止图形较少时，横竖分割线无法充满整个页面
        paperWidth = docE.clientWidth,

    // 初始画布高度
        paperHeight = docE.clientHeight - 30,


        hrefObj = (function () {
            var href = window.location.href,
                paramStr = href.substring(href.indexOf("?") + 1),
                paramArray = paramStr.split("&"),
                tempParam,
                paramObj = {};
            $.each(paramArray, function (i, item) {
                tempParam = item.split("=");
                paramObj[tempParam[0]] = tempParam[1];
            });
            return paramObj;
        }());

    (function () {
        $.ajax({
            url: "svg.servlet",
            data: hrefObj,
            dataType: "json",
            cache: false,
            success: function (json) {
                // 销毁等待框
                spinHolder();
                flowData.isIE = (function () {
                    var ua = navigator.userAgent.toLowerCase(),
                        rMsie = /(msie\s|trident.*rv:)([\w.]+)/;
                    return rMsie.exec(ua) != null ? true : false;
                }())
                flowData.graphs = json.baseGraphs || {};
                flowData.flowImagePath = json.flowImagePath;
                flowData.flowName = json.flowName;
                flowData.preFlowId = json.preFlowId;
                flowData.flowId = json.flowId;
                flowData.isVerticalFlow = json.isVerticalFlow;
                flowData.preFlowType = json.preFlowType;
                flowData.clientWidth = paperWidth;
                flowData.paperHeight = json.height > paperHeight ? json.height : paperHeight;
                flowData.paperWidth = json.width > paperWidth ? json.width : paperWidth - (flowData.isIE ? 18 : 15);
                flowData.panelType = json.panelType;
                flowData.roleMoveRectSize = (function () {
                    if (json.roleMoveEndX == 0) {
                        return;
                    }
                    return flowData.isVerticalFlow ?
                    {w: flowData.paperWidth, h: json.roleMoveEndX} : {w: json.roleMoveEndX, h: flowData.paperHeight};
                }());
                flowData.hrefObj = hrefObj;
                flowData.showName = json.showName;
                // 构建svg图形
                buildSvg(flowData)();
            }
        });
    }());
}());


function buildSvg(f) {
    var graph,
        graphType,
        maskLayer,
        graphStyle,
        textStyle,
        hoverText,
        graph3d,
        graphShadow,
        _d,
        paper,
        mainPaper,
        roleRect,
        roleMovePaper,
        navPaper,
        fontPxCache = [],
        myLinkSet,
        otherLinkSet,

        build3d = function () {
            graph3d = graph.clone().attr(graphStyle.graph3dStyle);

            if (graph3d.type == "path" || graph3d.type == "set") {
                _d["x"] += f.graph3dDiff / 2;
                _d["y"] += f.graph3dDiff / 2;
                _d["width"] -= f.graph3dDiff;
                _d["height"] -= f.graph3dDiff;
                graphStyle["stroke-width"] = 0;
                graph.remove();//清除原始图形，重新绘制
                graph = eprosGraphTypes[graphType]().attr(graphStyle);

            } else if (graph3d.type == "ellipse") {
                graph.attr({
                    "rx": graph.attr("rx") - f.graph3dDiff / 3,
                    "ry": graph.attr("ry") - f.graph3dDiff / 3,
                    "stroke-width": 0
                });
            } else {
                graph.attr({
                    "x": graph.attr("x") + f.graph3dDiff / 2,
                    "y": graph.attr("y") + f.graph3dDiff / 2,
                    "width": graph.attr("width") - f.graph3dDiff,
                    "height": graph.attr("height") - f.graph3dDiff,
                    "stroke-width": 0
                });
            }

            if (graphType == "ImplFigure") {
                graph3d[1].toFront();
                graph[1].toFront();
                graph[1].attr({
                    width: graph[1].attr("width") - f.graph3dDiff / 2,
                    height: graph[1].attr("height") - f.graph3dDiff / 3
                });
            } else if (graphType == "ITRhombus") {
                graph[1].attr("stroke-width", 1);
            } else if (graphType == "RoundRectWithLine") {
                graph3d[1].remove();
                graph[1].attr("stroke-width", 1);
            }else if(graphType == "ORFigureAR"){
            	graph[1].attr("stroke-width", 1);
            }else if(graphType == "AndFigureAR"){
            	graph[1].attr("stroke-width", 1);
            }else if(graphType == "XORFigureAR"){
            	graph[1].attr("stroke-width", 1);
            }else if(graphType == "XORFigureAR"){
            	graph[1].attr("stroke-width", 1);
            }else if(graphType == "RoleFigureAR"){
            	graph[1].attr("stroke-width", 1);
            }

            graph.toFront();
        },

        buildShadow = function () {
            var source = graph3d || graph;
            source.clone().attr({
                fill: graphStyle.shadow,
                "stroke-width": 0,
                transform: "t3,3" + graph.attr("transform")
            });
            graph3d && graph3d.toFront();
            graph && graph.toFront();
        },

        buildText = function () {
            // textStyle 不为空时说明有特殊文本需要显示，比如活动的编号信息
            if ((_d.text || _d.specialData ) && textStyle) {
                if ($.inArray(graphType, f.connectionLines) != -1) {
                	textStyle["text-anchor"]="center-middle";
                    var tEl = paper.text(_d.x, _d.y, _d.text).data("textStyle",textStyle ).attr(textStyle);
                    paper.rect(tEl.getBBox().x, tEl.getBBox().y, tEl.getBBox().width, tEl.getBBox().height + 2).attr({
                        "fill": "white",
                        "stroke-width": 0
                    });
                    tEl.toFront();
                } else {
                    var ls = getTextLocationAndGraphSize(),
                        newText = getNewText(_d.text, ls.width, textStyle);

                    $.inArray(graphType, f.activityGraphs) != -1 && setActivitiesHeaderText(ls);
					$.inArray(graphType, f.activityFlowGraphs) != -1 && setActivityFlowNumber(ls);
					
                    newText && paper.text(ls.x, ls.y, newText).data("textStyle", textStyle).attr(textStyle);
                }
            }
        },
        
        // 设置活动内的子流程、接口的编号
        setActivityFlowNumber = function(ls) {
        	if (_d.specialData) {
        		paper.text(ls.x, ls.y - ls.height / 2 + 5,  _d.specialData).attr(textStyle);
        	}
        }

        setActivitiesHeaderText = function (ls) {
            if (_d.specialData) {
            	var specialData = _d.specialData.split(",");
//            	console.log(specialData);
//            	console.log(_d.type);
            	if(specialData.length > 1 && specialData[1]!=="") {
					setOnlineInfo(specialData[1])
            	}
            	setActivityNumber(ls,specialData[0]);	
            }
        },
        
        /** 设置线上 IT 信息化*/
        setOnlineInfo = function(info) {
			var bgBox = paper.text(_d.x, _d.y - 8, info).attr({
				"font-family" : "宋体",
				"font-size" : "12px",
				"fill" : "#FFFFFF",
				"font-weight" : "bold"
			}).getBBox();
			paper.rect(bgBox.x-1, bgBox.y - 3,bgBox.width + 4, bgBox.height + 5).attr({
				"fill" : "#000000"
			}).toBack();
        },
	
        /** 设置活动编号 */
        setActivityNumber = function(ls,number) {
			graphType == f.activityGraphs[0] && paper.text(ls.x, ls.y - ls.height / 2,  number).attr(textStyle);
            graphType == f.activityGraphs[1] && paper.text(ls.x, ls.y - ls.height / 2 + 7, number).attr(textStyle);
            graphType == f.activityGraphs[2] && paper.text(ls.x, ls.y - ls.height / 2 + 5, number).attr(textStyle);
            graphType == f.activityGraphs[3] && paper.text(ls.x, ls.y - ls.height / 2 + 7, number).attr(textStyle);
            // 专家决策
            graphType == f.activityGraphs[5] && paper.text(ls.x, ls.y - ls.height / 2 + 7, number).attr(textStyle);
            if (graphType == f.activityGraphs[4]) {// AR活动处理
				var bgBox = paper.text(_d.x + _d.width - 2, _d.y - 5, number).attr({
					"font-family" : "宋体",
					"font-size" : "12px",
					"fill" : "#000000",
					"font-weight" : "bold",
					"text-anchor":"end"
				}).getBBox();
            }
		},

        getNewText = function (text, width, textStyle) {
            if (text == undefined) {
                return;
            }
            var newText = "",
                charPx = 0,
                sumPx = 0,
                fontPx = getFontPx(textStyle);

            $.each(text, function (i, c) {
                charPx = c.match(/[^\x00-\xff]/ig) == null ? fontPx.en : fontPx.cn;
                isSpecialFontFamily(textStyle) && c.match(/ /) != null && (charPx /= 2);
                if (c == "\n") {
                    sumPx = 0;
                    newText += c;
                } else if (charPx > width) {
                    sumPx = 0;
                    newText += c + "\n";
                } else if (sumPx + charPx > width) {
                    sumPx = 0;
                    newText += "\n" + c;
                    sumPx += charPx;
                } else {
                    sumPx += charPx;
                    newText += c;
                }
            });
            return isSpecialFontFamily(textStyle) ? newText.replace(/    /g, "　") : newText.replace(/  /g, "　");
        },

        isSpecialFontFamily = function (textStyle) {
            return textStyle["font-family"].match("^华文|^方正|^微软雅黑$") != null;
        },

        getFontPx = function (textStyle) {
            for (var i = fontPxCache.length - 1; i >= 0; i--) {
                if (fontPxCache[i]["font-size"] == textStyle["font-size"] && fontPxCache[i]["font-family"] == textStyle["font-family"]) {
                    return fontPxCache[i];
                }
            }
            $("#newText").css(textStyle);
            var enPx = $("#newText").text("a").width(),
                cnPx = $("#newText").text("啊").width();
            fontPxCache.push({
                "font-size": textStyle["font-size"],
                "font-family": textStyle["font-family"],
                en: enPx,
                cn: cnPx
            });
            return fontPxCache[fontPxCache.length - 1];
        },

        getTextLocationAndGraphSize = function () {
            var ta = textStyle["text-anchor"] || "center-middle",
                angle = getGraphAngle(),
                rd = getRotateGraphData(angle),
                x,
                y,
                width = rd.width,
                height = rd.height;
            if (graphType == "ModelFigure") {
                width = Number(_d.specialData);
            }

            ta.indexOf("top") !== -1 && (y = rd.y + f.graphTextDiff) ||
            ta.indexOf("middle") !== -1 && (y = rd.y + height / 2) ||
            ta.indexOf("bottom") !== -1 && (y = rd.y + height - f.graphTextDiff);

            ta.indexOf("left") !== -1 && (x = rd.x + f.graphTextDiff) ||
            ta.indexOf("center") !== -1 && (x = rd.x + width / 2) ||
            ta.indexOf("right") !== -1 && (x = rd.x + width - f.graphTextDiff);

            if (graphType == "ReverseArrowhead" || graphType == "RightArrowhead") {
                angle == 0 ? (y = rd.y + height * 0.75) :
                    angle == 90 ? (y = rd.y + height / 2) && (x = rd.x + width * 0.25) :
                        angle == 180 ? (y = rd.y + height * 0.25) :
                            angle == 270 ? (y = rd.y + height / 2) && (x = rd.x + width * 0.75) : 0;
            } else if (graphType == "RoundRectWithLine") {
                y = rd.y + height / 3 + (height - height / 3) / 2;
            } else if (graphType == "Rhombus" || graphType == "ITRhombus" || graphType == "DataImage"|| graphType == "ExpertRhombusAR") {
                y += textStyle["font-size"] / 2;
            } else if (graphType == "CommentText") {
                y -= f.graphTextDiff / 2;
                x -= f.graphTextDiff / 2;
            }

            return {
                x: x,
                y: y,
                width: width,
                height: height
            };
        },

        getGraphAngle = function () {
            if (graphStyle && graphStyle.transform) {
                return Number(graphStyle.transform.substring(1));
            } else {
                return 0;
            }
        },

        getRotateGraphData = function (angle) {
            var rotateData = {
                x: _d.x,
                y: _d.y,
                width: _d.width || 0,// 动态连接线内的文本没有width和height，所以给0
                height: _d.height || 0
            };
            if (angle == 90 || angle == 270) {
                rotateData.x = _d.x + _d.width / 2 - _d.height / 2;
                rotateData.y = _d.y + _d.height / 2 - _d.width / 2;
                rotateData.width = _d.height;
                rotateData.height = _d.width;
            }
            return rotateData;
        },

        buildMaskLayer = function () {
            maskLayer = graph.clone().attr({
                fill: "white",
                "stroke-width": 0,
                "fill-opacity": 0
            });
        },

        buildHoverTextOnMaskLayer = function () {
            if (hoverText) {
                var newHoverText = "",
                    hoverTextSvg,
                    hoverTextBgRect,
                    maxToolTipWidth = 500,
                    show = Raphael.animation({opacity: 1}),
                    hide = Raphael.animation({opacity: 0});
                $.each(hoverText, function (i, item) {
                    newHoverText += "#" + item[0] + "#" + "\n \n" + item[1] + "\n \n";
                });
                newHoverText = getNewText(newHoverText, maxToolTipWidth, f.toolTipStyle);

                maskLayer.hover(function (event) {
                    var textBox,
                        textX = (event.clientX + 15 + $(window).scrollLeft()) / (1 + f.zoomVal),
                        textY = (event.clientY + $(window).scrollTop()) / (1 + f.zoomVal);
                    hoverTextSvg = this.paper.text(textX, textY, newHoverText).attr(f.toolTipStyle);
                    textBox = hoverTextSvg.getBBox();
                    hoverTextBgRect = this.paper.rect(
                        textBox.x - f.graphTextDiff,
                        textBox.y - f.graphTextDiff * 2,
                        textBox.width + f.graphTextDiff * 4,
                        textBox.height + f.graphTextDiff * 4)
                        .attr({
                            "fill": "#FFFFE1",
                            "stroke-width": 1,
                            "stroke-opacity": 0.8
                        });
                    hoverTextSvg.toFront();
                    hoverTextBgRect.animate(show.delay(700));
                    hoverTextSvg.animate(show.delay(700));
                    if (!f.isIE) {
                        hoverTextBgRect.animate(hide);
                        hoverTextSvg.animate(hide);
                    }
                    this.paper == roleMovePaper && this.paper.setSize(this.paper.width + maxToolTipWidth, this.paper.height);
                }, function (el) {
                    this.paper == roleMovePaper && this.paper.setSize(f.roleMoveRectSize.w * (1 + f.zoomVal), f.roleMoveRectSize.h * (1 + f.zoomVal));
                    hoverTextSvg.remove();
                    hoverTextBgRect.remove();
                });
            }
        },

        openResouceByType = function (type, id) {
            if (type == "active") {
                return function () {
                    window.open("processActive.action?activeId=" + id + "&isPub=" + f.hrefObj["isPub"]);
                }
            } else if (type == "flow") {
                return function () {
                    window.open("process.action?flowId=" + id + "&isPub=" + f.hrefObj["isPub"] + "&type=process");
                }
            } else if (type == "flowMap") {
                return function () {
                    window.open("process.action?flowId=" + id + "&isPub=" + f.hrefObj["isPub"] + "&type=processMap");
                }
            } else if (type == "rule") {
                return function () {
                    window.open("rule.action?ruleId=" + id + "&isPub=" + f.hrefObj["isPub"])
                }
            } else if (type == "org") {
                return function () {
                    window.open("organization.action?orgId=" + id);
                }
            }
        },

        buildDblEventOnMaskLayer = function () {
            if (hasDblClickEvent()) {
                maskLayer.attr("cursor", "pointer");
                maskLayer.dblclick((function () {
                    if ($.inArray(graphType, f.activityGraphs) !== -1) {
                        return openResouceByType("active", _d.figureId);
                    }

                    for (var iconName in _d.linkIcons) {
                        switch (iconName) {
                            case "UPPER_FLOW":
                            case "LOWER_FLOW":
                            case "PROCEDURE_FLOW":
                            case "SON_FLOW":
                            case "LINK_FLOW":
                                return openResouceByType("flow", _d.linkIcons[iconName][0].fileId);
                            case "LINK_FLOW_MAP":
                                return openResouceByType("flowMap", _d.linkIcons[iconName][0].fileId);
                            case "LINK_RULE":
                                return openResouceByType("rule", _d.linkIcons[iconName][0].fileId);
                            case "LINK_ORG":
                                return openResouceByType("org", _d.linkIcons[iconName][0].fileId);
                        }
                    }
                }()));
            }
        },

        hasDblClickEvent = function () {
            if ($.inArray(graphType, f.activityGraphs) != -1) {
                return true;
            }

            for (var iconName in _d.linkIcons) {
                switch (iconName) {
                    case "UPPER_FLOW":
                    case "LOWER_FLOW":
                    case "PROCEDURE_FLOW":
                    case "SON_FLOW":
                    case "LINK_FLOW":
                    case "LINK_FLOW_MAP":
                    case "LINK_RULE":
                    case "LINK_ORG":
                        return true;
                }
            }
            return false;
        },

        getIconLocationAndSize = function (iconName) {
            var rotateData = getRotateGraphData(getGraphAngle()),
                x,
                y,
                image,
                iconWidth = iconName == "LINK_FLOW" || iconName == "LINK_FLOW_MAP" || iconName == "LINK_ORG" || iconName == "LINK_RULE" ? 8 : 16,
                iconHeight = iconName == "LINK_FLOW"||  iconName == "LINK_FLOW_MAP" || iconName == "LINK_ORG" || iconName == "LINK_RULE" ? 8 : 16,
                getActivityGraphY = function () {
                    switch (graphType) {
                        case "Rhombus":
                        case "ITRhombus":
                        case "ExpertRhombusAR":
                            return rotateData.y + rotateData.height / 2 - iconHeight / 2 - 2;
                        case "RoundRectWithLine":
                        case "DataImage":
                        case "ActiveFigureAR":
                            return rotateData.y;
                    }
                };

            switch (iconName) {
                case "ACTIVE_IN":
                    x = rotateData.x + 2;
                    y = getActivityGraphY();
                    break;
                case "ACTIVE_OUT":
                    x = rotateData.x + rotateData.width - iconWidth - 2;
                    y = getActivityGraphY();
                    break;
                case "CONTROL_POINT":
                case "KEY_CONTROL_POINT":
                    x = rotateData.x + rotateData.width * 0.25;
                    y = getActivityGraphY();
                    break;
                case "ACTIVE_STANDARD":
                    x = rotateData.x + rotateData.width * 0.6;
                    y = getActivityGraphY();
                    break;
                case "FILE":
                    x = rotateData.x + rotateData.width / 2 - iconWidth / 2;
                    y = rotateData.y + rotateData.height - iconHeight + 2;
                    break;
                case "POSITION":
                case "KEY_POSITION":
                case "UPPER_FLOW":
                case "LOWER_FLOW":
                case "PROCEDURE_FLOW":
                case "SON_FLOW":
                case "LINK_FLOW":
                case "LINK_FLOW_MAP":
                case "LINK_RULE":
                case "LINK_ORG":
                    x = rotateData.x + rotateData.width / 2 - iconWidth / 2;
                    y = rotateData.y + rotateData.height * 0.1;
                    break;
            }

            return {x: x, y: y, width: iconWidth, height: iconHeight};
        },

        getIconPath = function (iconName) {
            return "images/svgIcons/" + iconName + ".gif";
        },

        getIconDialogTitle = function (iconName) {
            switch (iconName) {
                case "ACTIVE_IN":
                case "ACTIVE_OUT":
                case "FILE":
                    return "文件";
                case "CONTROL_POINT":
                case "KEY_CONTROL_POINT":
                    return "控制点编号";
                case "ACTIVE_STANDARD":
                    return "标准";
            }
        },

        buildIcon = function () {
            if (_d.linkIcons == undefined) {
                return;
            }
            $.each(_d.linkIcons, function (iconName, iconData) {
                    var iconls = getIconLocationAndSize(iconName),
                        image = paper.image(getIconPath(iconName), iconls.x, iconls.y, iconls.width, iconls.height);
                    otherLinkSet.push(image);
                    if ($.inArray(iconName, f.iconNoClickEvent) == -1) {
                        image.attr("cursor", "pointer");
                        image.click(getIconHandler(iconName, iconData));
                    }
                }
            );
        },

        getIconHandler = function (iconName, iconData) {
            if ($.inArray(iconName, f.iconShowWithTbl) != -1) {
                return function () {
                    var trHtml,
                        clickFunction;
                    $("#linkInfoDiv table tr").remove();
                    $.each(iconData, function (i, linkData) {
                        trHtml = "<tr><td";
                        switch (iconName) {
                            case "ACTIVE_IN":
                            case "ACTIVE_OUT":
                            case "FILE":
                                trHtml += " id='" + _d.figureId + "-" + iconName + "-" + linkData.fileId + "'";
                                clickFunction = function () {
                                    downloadFile(linkData.fileId);
                                };
                                break;
                            default:
                                trHtml += " style='cursor:default'";
                        }
                        trHtml += ">" + linkData.fileName + "</td></tr>";
                        $("#linkInfoDiv table").append(trHtml);
                        clickFunction && $("#" + _d.figureId + "-" + iconName + "-" + linkData.fileId).on("dblclick", clickFunction);
                    });
                    $("#linkInfoDiv").dialog({
                        position: [this.attr("x") * (1 + f.zoomVal) - $(window).scrollLeft(), this.attr("y") * (1 + f.zoomVal) - $(window).scrollTop()],
                        title: getIconDialogTitle(iconName)
                    });
                    $("#linkInfoDiv").dialog("open");
                }
            } else {
            	switch (iconName) {
	            	case "LINK_RULE":
	            		return openResouceByType("rule", iconData[0].fileId);
	            	case "LINK_ORG":
	            		return openResouceByType("org", iconData[0].fileId);
	            	case "LINK_FLOW":
	            	case "UPPER_FLOW":
	            	case "LOWER_FLOW":
	            	case "PROCEDURE_FLOW":
	            	case "SON_FLOW":
	            		return	openResouceByType("flow", iconData[0].fileId);
	            	case "LINK_FLOW_MAP":
	            		return	openResouceByType("flowMap", iconData[0].fileId);
	            	default:
	            		break;
            	}
            }
        },

        buildMyLink = function () {
            if (f.panelType == "FLOW_MAP" && _d.isMyLink) {
                myLinkSet.push(paper.image("images/svgIcons/myLink.gif", _d.x + 2, _d.y, 16, 16).attr("opacity", 1));
            }
        },

        /**
         * 下载文件
         * @param fileId
         */
        downloadFile = function (fileId) {
            $.download({
                url: "downloadFile.action",
                data: {
                    fileId: fileId,
                    from: (function () {
                        if (f.panelType == "FLOW")
                            return "process";
                        else if (f.panelType == "FLOW_MAP")
                            return "processMap";
                    }()),
                    reqType: "public",
                    isPub: f.hrefObj["isPub"]
                }
            });
            $("#linkInfoDiv").dialog("close");
        },

        resetDrawingPaper = function () {
            if (roleRect && graphStyle !== "ParallelLines" && graphStyle != "VerticalLine" && Raphael.isPointInsideBBox(roleRect.getBBox(), _d.x, _d.y)) {
                paper = roleMovePaper;
            } else {
                paper = mainPaper;
            }
        },

        buildRoleMove = function () {
            if (f.roleMoveRectSize == undefined) {
                return;
            }

            roleMovePaper = Raphael(0, f.navPaperHeight, f.roleMoveRectSize.w, f.roleMoveRectSize.h)
                .setViewBox(0, 0, f.roleMoveRectSize.w, f.roleMoveRectSize.h, false);

            roleRect = roleMovePaper.rect(0, 0, f.roleMoveRectSize.w, f.roleMoveRectSize.h).attr({
                "fill": "rgb(245,245,245)",
                "stroke-width": 0
            });

            roleMovePaper.canvas.id = "roleMove";
            $("#roleMove").css("position", "fixed");
			
            if (f.isVerticalFlow) {
            	$(window).scroll(function () {
            		$("#roleMove").css("left", - $(window).scrollLeft());
	            });
            }else{
	            $(window).scroll(function () {
	                $("#roleMove").css("top", f.navPaperHeight - $(window).scrollTop());
	            });
            }
        },

        buildNavPaper = function () {
            navPaper = Raphael(0, 0, f.paperWidth, f.navPaperHeight);
            var navBgRect = navPaper.rect(0, 0, navPaper.width, navPaper.height + 1).attr({
                    "stroke-width": 0,
                    "fill": "rgb(245,245,245)"
                }),
                linkIconSet = navPaper.set(),
                myLinkIcon,
                myLinkText,
                otherLinkIcon,
                otherLinkText,
                downloadFileIcon,
                linkRectIcon,
                linkBgRect,

                zoomer = function (val) {
                    f.zoomVal = (val !== undefined ? f.zoomVal + val : 0);
                    // 保持navPaper宽度始终看起来没有变化
                    roleMovePaper && roleMovePaper.setSize(f.roleMoveRectSize.w * (1 + f.zoomVal), f.roleMoveRectSize.h * (1 + f.zoomVal));
                    mainPaper.setSize(f.paperWidth * (1 + f.zoomVal), f.paperHeight * (1 + f.zoomVal));
                },

                showUpperFlow = function () {
                    window.open("process.action?flowId=" + f.preFlowId + "&type=" + f.preFlowType + (f.hrefObj["isPub"] == "false" ? "T" : ""));
                },

                buildLinkRect = function () {
                    linkBgRect = navPaper.rect(f.clientWidth - 130, 25, 110, f.panelType == "FLOW_MAP" ? 50 : 30).attr({
                        "stroke-width": 1,
                        "stroke-opacity": 0.2,
                        "fill": "#EFEFEF"
                    });

                    changeImgStatus = function (img, imgSet) {
                        if (img.attr("src").indexOf("unSelected.jpg") != -1) {
                            img.attr("src", "images/svgIcons/selected.jpg");
                            imgSet.attr("opacity", 1);
                        } else {
                            img.attr("src", "images/svgIcons/unSelected.jpg");
                            imgSet.attr("opacity", 0);
                        }
                    }

                    if (f.panelType == "FLOW_MAP") {
                        myLinkIcon = navPaper.image("images/svgIcons/selected.jpg", f.clientWidth - 120, 33, 15, 15);
                        myLinkText = navPaper.text(f.clientWidth - 100, 40, "我参与链接图标").attr("cursor", "default");
                        myLinkIcon.click(function () {
                            changeImgStatus(myLinkIcon, myLinkSet);
                        });
                        myLinkText.click(function () {
                            changeImgStatus(myLinkIcon, myLinkSet);
                        });
                    }
                    otherLinkIcon = navPaper.image("images/svgIcons/selected.jpg", f.clientWidth - 120, f.panelType == "FLOW_MAP" ? 53 : 33, 15, 15);
                    otherLinkText = navPaper.text(f.clientWidth - 100, f.panelType == "FLOW_MAP" ? 60 : 40, "其他链接图标").attr("cursor", "default");
                    otherLinkIcon.click(function () {
                        changeImgStatus(otherLinkIcon, otherLinkSet)
                    });
                    otherLinkText.click(function () {
                        changeImgStatus(otherLinkIcon, otherLinkSet)
                    });

                    linkIconSet.push(
                        linkBgRect,
                        myLinkIcon,
                        otherLinkIcon,
                        myLinkText,
                        otherLinkText
                    );

                    linkIconSet.attr("opacity", 0);
                },

                buildNavIcons = function () {
                    navPaper.image("images/navMenuIcons/oftenZoomBig.gif", 20, 5, 16, 16).click(function () {
                        zoomer(0.1);
                    }).attr("cursor", "pointer");
                    navPaper.image("images/navMenuIcons/oftenZoomSmall.gif", 20 * 3, 5, 16, 16).click(function () {
                        // 只支持缩放到0.2倍
                        if (1 + f.zoomVal - 0.1 >= 0.2) {
                            zoomer(-0.1);
                        }
                    }).attr("cursor", "pointer");

                    navPaper.image("images/navMenuIcons/oftenOriginal.gif", 20 * 5, 5, 16, 16).click(function () {
                        zoomer();
                    }).attr("cursor", "pointer");

                    f.panelType != "DEPT" && f.preFlowId != 0 && navPaper.image("images/navMenuIcons/backMapProcess.gif", 20 * 7, 5, 16, 16).click(function () {
                        showUpperFlow();
                    }).attr("cursor", "pointer");

                    f.flowName && navPaper.text(20 * (f.preFlowId != 0 ? 9 : 7), 18, f.flowName).attr({
                        "text-anchor": "start",
                        "font-family": "microsoft yahei",
                        "font-size": 16,
                        "font-weight": "bold"
                    });

                    f.panelType != "DEPT" && (downloadFileIcon = navPaper.image("images/navMenuIcons/downIconBtn.gif", f.clientWidth - 20 * 4, 5, 16, 16).click(function () {
                        $.download({
                                url: "downloadMapIconAction.action",
                                data: {
                                    mapIconPath: f.flowImagePath,
                                    mapName: f.showName,
                                    flowId:f.flowId
                                }
                            }
                        );
                    }).attr("cursor", "pointer"));

                    linkRectIcon = navPaper.image("images/navMenuIcons/ShowlinkBtn.gif", f.clientWidth - 20 * 2, 5, 16, 16).click(function () {
                        if (linkBgRect.attr("opacity") == 1) {
                            linkIconSet.attr("opacity", 0);
                            navPaper.setSize(navPaper.width, 30);
                        } else {
                            linkIconSet.attr("opacity", 1);
                            navPaper.setSize(navPaper.width, 80);
                        }
                    }).attr("cursor", "pointer");
                };

            navPaper.canvas.id = "navigator";
            $("#navigator").css("position", "fixed");

            buildLinkRect();

            buildNavIcons();

            $("body").on("click", function (event) {
                if (!Raphael.isPointInsideBBox(linkBgRect.getBBox(), event.clientX, event.clientY) && !Raphael.isPointInsideBBox(linkRectIcon.getBBox(), event.clientX, event.clientY)) {
                    linkIconSet.attr("opacity", 0);
                }
                if ($("#linkInfoDiv").dialog("isOpen")) {
                    var dialog = $("#linkInfoDiv").parent(),
                        clientX = event.clientX + $(window).scrollLeft(),
                        clientY = event.clientY + $(window).scrollTop();
                    if (!(clientX >= dialog.position().left && clientX <= dialog.position().left + dialog.width())) {
                        $("#linkInfoDiv").dialog("close");
                    } else {
                        if (!(clientY >= dialog.position().top && clientY <= dialog.position().top + dialog.height())) {
                            $("#linkInfoDiv").dialog("close");
                        }
                    }
                }
            });

            $(window).resize(function () {
                navPaper.setSize($(window).width(), navPaper.height);
                navBgRect.attr("width", navPaper.width);
                linkRectIcon.attr("x", $(window).width() - 20 * 2);
                downloadFileIcon && downloadFileIcon.attr("x", $(window).width() - 20 * 4);
            });

        },

        setSpecialFigureAttrs = function () {
            var isFlowLevelFigure = graphType.indexOf("FlowLevelFigure") != -1,
                isModelFigure = graphType == "ModelFigure";
            if (!isFlowLevelFigure && !isModelFigure) {
                return true;
            }

            isFlowLevelFigure && setFlowLevelFigureAttrs();
            isModelFigure && setModelFigureAttrs();
            return false;
        },

        setModelFigureAttrs = function () {
            var fillColor = graphStyle.fill.split("-");
            graph.attr(graphStyle);
            graph[0].attr("fill", "180" + "-" + fillColor[1] + ":50" + "-" + fillColor[2]);
        },

        setFlowLevelFigureAttrs = function () {
            var levelIndex = graphType.indexOf("Two") != -1 ? 1 : graphType.indexOf("Three") != -1 ? 2 : graphType.indexOf("Four") != -1 ? 3 : 0,
                color = graphStyle["fill"].split("-"),
                bigColor = color.length == 1 ? color[0] : color[1],
				smallColor = color.length == 1 ? color[0]: color[2];

            graph.forEach(function (el, index) {
                levelIndex == index ? el.attr("fill", smallColor) : el.attr("fill", bigColor);
                el.attr("stroke-width", 0);
            });
        },

        eprosGraphTypes = {

            reBuildConnectingLinePath: function (line) {
                var diff = 7,
                    right = line.startX < line.endX,
                    d = "";

                $.each(line.crossOverPoints, function (i, item) {
                    if (i == 0) {
                        d += " M" + line.startX + "," + line.startY;
                        d += " L" + (item[0] + (right ? -diff : diff)) + "," + item[1];
                    } else {
                        d += " H" + (item[0] + (right ? -diff : diff));
                    }
                    d += " M" + (item[0] + (right ? -diff : diff)) + "," + item[1];
                    d += " Q" + item[0] + " " + (item[1] - diff * 2) + "," + (item[0] + (right ? diff : -diff )) + " " + item[1];
                });
                d += " H" + line.endX;
                return d;
            },
            
             // 时间轴
            FlowTimeLine : function() {
				var d, 
					y = 105, 
					vLineEndY = y - 18,
					preX,
					code,
					targetTitle ,
					statusTitle ,
					sumWidth,
					arrowHeight=4;
				$.each(_d.nodes, function(i, timeLineNode) {
					
					d == undefined ? (d = "M" + (f.roleMoveRectSize.w+15) + "," + y) : d += "M" + preX + "," + y;
				
					d += "L" + (timeLineNode.x + timeLineNode.width-5) + "," + y;
					d += "V" + vLineEndY;
					
					// 箭头
					d += "M"+(timeLineNode.x-1 + timeLineNode.width - 5) + "," + y;
					d += "L"+(timeLineNode.x-1 + timeLineNode.width-15)+","+(y+arrowHeight);
					
					d += "M"+(timeLineNode.x-1 + timeLineNode.width - 5) + "," + y;
					d += "L"+(timeLineNode.x-1 + timeLineNode.width-15)+","+(y-arrowHeight);
					
					// 如果非连续的时间轴节点，在时间节点开始位置显示一条竖线
					if(timeLineNode.isContinuous){
						d += "M" + (timeLineNode.x) + "," + y;
						d += "V" + vLineEndY;
					}
					
					code = '('+timeLineNode.number+')';
					if(i==0){
						targetTitle = "目标值(d)：" + code;
						statusTitle = "现状值(d)：" + code;
						sumWidth = timeLineNode.x;
					}else{
						targetTitle = code;
						statusTitle = code;
					}
					
					preX = (timeLineNode.x + timeLineNode.width-5);
					
					// 编号
					// paper.text(timeLineNode.x + timeLineNode.width +5, y - 28,code).attr("text-anchor", "end");
					
					// 现状值
					paper.text(timeLineNode.x + timeLineNode.width-20,y - 10,
							statusTitle + timeLineNode.statusValue).attr("text-anchor", "end");
					
					// 目标值
					paper.text(timeLineNode.x + timeLineNode.width-20, y + 10,
							targetTitle + timeLineNode.value).attr("text-anchor", "end");
				});
				
				if(_d.sumStatusValue > 0 || _d.sumTargetValue > 0){
					// 汇总值
					roleMovePaper.text(25 , y - 60,"现状值（总）："+_d.sumStatusValue+"d").attr("text-anchor", "start");
					roleMovePaper.text(25, y - 40,"目标值（总）："+_d.sumTargetValue+"d").attr("text-anchor", "start");
				}
				console.log(d);
				return paper.path(d);
			},

            // 连接线
            CommonLine: function () {
                var d = "",
                    lineData = _d.lines;

                //  有数据情况下,生成path
                if (lineData.length) {
                    for (var i = 0; i < lineData.length; i++) {
                        if (lineData[i].crossOverPoints.length) {
                            d += this.reBuildConnectingLinePath(lineData[i]);
                        } else if (i == 0) {
                            if (lineData[0].startX == lineData[0].endX) {
                                lineData[0].startY < lineData[0].endY ? (lineData[0].startY -= 1) : (lineData[0].startY += 1);
                            }
                            if (lineData[0].startY == lineData[0].endY) {
                                lineData[0].startX < lineData[0].endX ? (lineData[0].startX -= 1) : (lineData[0].startX += 1);
                            }
                            d += " M" + lineData[0].startX + "," + lineData[0].startY;
                            d += " L" + lineData[0].endX + "," + lineData[0].endY;
                        } else if (lineData[i].startX == lineData[i].endX) {
                            d += " V" + lineData[i].endY;
                        } else if (lineData[i].startY == lineData[i].endY) {
                            d += " H" + lineData[i].endX;
                        }
                    }
                    return paper.path(d);
                }
            },

            //  曼哈顿线
            ManhattanLine: function () {
                var linePath = this.CommonLine(),
                isTwoArrowLine = _d.isTwoArrowLine,
                // -w:[number] -h:[number] 修改了raphael.js addArrow方法的源码 w:width h:height
                    arrowEnd = "-w:9-h:9";
                if (graphStyle["stroke-width"] !== "1") {
                    arrowEnd = "-w:5-h:5";
                }
                if(isTwoArrowLine==1){// 显示双向箭头
                	return linePath.attr({"arrow-end": "block" + arrowEnd,"arrow-start":"block"+arrowEnd});
                }
                return linePath.attr({"arrow-end": "block" + arrowEnd});
            },

            //  单向箭头
            OneArrowLine: function () {
                var d = "M" + _d.x + "," + (_d.y + _d.height * 0.4);
                d += " H" + (_d.x + _d.width * 0.8);
                d += " V" + (_d.y + _d.height * 0.2);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
                d += " L" + (_d.x + _d.width * 0.8) + "," + (_d.y + _d.height * 0.8);
                d += " V" + (_d.y + _d.height * 0.6);
                d += " H" + _d.x + "Z";

                return paper.path(d);
            },

            //  双向箭头
            TwoArrowLine: function () {
                var d = "M" + _d.x + "," + (_d.y + _d.height * 0.5);
                d += " L" + (_d.x + _d.width * 0.2) + "," + (_d.y + _d.height * 0.2);
                d += " V" + (_d.y + _d.height * 0.4);
                d += " H" + (_d.x + _d.width * 0.8);
                d += " V" + (_d.y + _d.height * 0.2);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
                d += " L" + (_d.x + _d.width * 0.8) + "," + (_d.y + _d.height * 0.8);
                d += " V" + (_d.y + _d.height * 0.6);
                d += " H" + (_d.x + _d.width * 0.2);
                d += " V" + (_d.y + _d.height * 0.8);
                d += " L" + _d.x + "," + (_d.y + _d.height * 0.5);

                return paper.path(d);
            },

            // 分割线
            DividingLine: function () {
                return paper.path("M" + (_d.startX) + "," + (_d.startY) + " L" + (_d.endX) + "," + (_d.endY));
            },

            // 横线
            VDividingLine: function () {
                return this.DividingLine();
            },

            // 纵线
            HDividingLine: function () {
                return this.DividingLine();
            },

            // 横分割线
            ParallelLines: function () {
                return paper.path("M" + _d.startX + "," + _d.startY + " H" + f.paperWidth);
            },

            // 纵分割线
            VerticalLine: function () {
                return paper.path("M" + _d.startX + "," + _d.startY + " V" + f.paperHeight);
            },


            // 流程图:子流程
            OvalSubFlowFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // 流程地图:椭圆
            Oval: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // 流程地图 三角形
            Triangle: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " L" + _d.x + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
                return paper.path(d);
            },

            // 流程地图 矩形功能域
            FunctionField: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },

            // 五边形
            Pentagon: function () {
                var d = "M" + _d.x + "," + _d.y;
                d += " H" + (_d.x + _d.width - 19);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
                d += " L" + (_d.x + _d.width - 19) + "," + (_d.y + _d.height);
                d += " H" + _d.x;
                d += " V" + _d.y;

                return paper.path(d);
            },

            // 箭头六边形
            RightHexagon: function () {
                var d = "M" + _d.x + "," + _d.y;
                d += " L" + (_d.x + ( _d.width - 19)) + "," + _d.y;
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
                d += " L" + (_d.x + (_d.width - 19)) + "," + (_d.y + _d.height);
                d += " L" + _d.x + "," + (_d.y + _d.height);
                d += " L" + (_d.x + 19) + "," + (_d.y + _d.height * 0.5);
                d += " L" + _d.x + "," + _d.y;

                return paper.path(d);
            },

            // 泳池
            ModelFigure: function () {
                var leftRectWidth = Number(_d.specialData),
                    leftRect = paper.rect(_d.x, _d.y, leftRectWidth, _d.height),
                    rightRect = paper.rect(_d.x + leftRectWidth, _d.y, _d.width - leftRectWidth, _d.height),
                    set = paper.set([leftRect, rightRect]);
                return set;
            },

            // 协作框
            DottedRect: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },

            // 自由文本框
            FreeText: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },

            // 时间框
            DateFreeText: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },

            // 注释框
            CommentText: function () {
                var singerLine = _d.specialData.split(","),
                    d;
                d = "M" + singerLine[0] + "," + singerLine[1];
                d += " L" + _d.x + "," + (_d.y + _d.height / 2);
                paper.path(d).toBack();
                d = "  M" + (_d.x + 10) + "," + _d.y;
                d += " H" + (_d.x);
                d += " V" + (_d.y + _d.height);
                d += " H" + (_d.x + 10);
                return paper.path(d);
            },

            // 阶段分隔符
            PartitionFigure: function () {
                var set = paper.set(),
                    headD,
                    footD;
                headD = "M" + _d.x + "," + _d.y;
                headD += " L" + (_d.x + _d.width) + "," + (_d.y + 17.5);
                headD += " L" + _d.x + "," + (_d.y + 35);
                footD = "M" + _d.x + "," + (_d.y + 35);
                footD += " V" + (_d.y + _d.height);
                set.push(
                    paper.path(headD).attr("stroke-width", 3),
                    paper.path(footD).attr({
                        "stroke-dasharray": "- ",
                        "stroke-width": 1.5
                    })
                );
                return set;
            },

            // 图标插入框
            IconFigure: function () {
                if (_d.specialData) {
                    return paper.image("writefileByte.action?isPub=false&fileId=" + _d.specialData, _d.x, _d.y, _d.width, _d.height);
                } else {
                    return paper.rect(_d.x, _d.y, _d.width, _d.height);
                }
            },

            // 手动输入
            InputHandFigure: function () {
                var d = "M" + _d.x + "," + (_d.y + _d.height * 0.3);
                d += " L" + (_d.x + _d.width) + "," + _d.y;
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
                d += " L" + _d.x + "," + (_d.y + _d.height);
                d += " L" + _d.x + "," + (_d.y + _d.height * 0.3);
                return paper.path(d);
            },

            // 制度
            SystemFigure: function () {
                var d = "M" + _d.x + "," + _d.y;
                d += " H" + (_d.x + _d.width * 0.85);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
                d += " L" + (_d.x + _d.width * 0.85) + "," + (_d.y + _d.height);
                d += " H" + _d.x;
                d += " L" + _d.x + "," + _d.y;

                return paper.path(d);
            },

            // 活动
            RoundRectWithLine: function () {
                // 横向直线
                var hLineStartX = _d.x,
                    hLineEndX = _d.x + _d.width,
                    hLineY = _d.y + +_d.height / 3,
                    roundRect = paper.set(),
                    rect,
                    hLine;
                roundRect.push(
                    rect = paper.rect(_d.x, _d.y, _d.width, _d.height, 10),
                    hLine = paper.path("M" + hLineStartX + "," + hLineY + "L" + hLineEndX + "," + hLineY)
                );
                return roundRect;
            },
            
            // 角色
            RoleFigure: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },
            

            // 客户
            CustomFigure: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },
            
            // 返入
            ReverseArrowhead: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " V" + (_d.y + _d.height / 2);
                d += " L" + _d.x + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height / 2);
                return paper.path(d);
            },

            // 返出
            RightArrowhead: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " V" + (_d.y + _d.height / 2);
                d += " H" + _d.x;
                d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
                d += " H" + (_d.x + _d.width / 2);
                return paper.path(d);
            },

            // 决策
            Rhombus: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " L" + _d.x + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
                return paper.path(d);
            },

            // 事件
            EventFigure: function () {
                var d = "M" + _d.x + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width * 0.2) + "," + _d.y;
                d += " H" + (_d.x + _d.width * 0.8);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width * 0.8) + "," + (_d.y + _d.height);
                d += " H" + (_d.x + _d.width * 0.2);
                d += " L" + _d.x + "," + (_d.y + _d.height / 2);
                return paper.path(d);
            },

            // 接口
            ImplFigure: function () {
                var set = paper.set(),
                    d;
                d = "M" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.6);
                d += " L" + (_d.x + _d.width - 10) + "," + (_d.y + _d.height * 0.2);
                d += " H" + (_d.x + _d.width * 0.2 + 10);
                d += " L" + (_d.x + _d.width * 0.2) + "," + (_d.y + _d.height * 0.6);
                d += " L" + (_d.x + _d.width * 0.2 + 10) + "," + (_d.y + _d.height);
                d += " H" + (_d.x + _d.width - 10);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.6);

                set.push(  
                    paper.path(d),
                    paper.rect(_d.x, _d.y, _d.width * 0.7, _d.height * 0.8, 7)
                );
                return set;
            },
            
            // 活动-接口
            ActivityImplFigure:function(){
            	return this.ImplFigure();
            },
            
            // 活动-子流程
            ActivityOvalSubFlowFigure:function(){
            	return this.OvalSubFlowFigure();
            },

            // 文档
            FileImage: function () {
                var d = "M" + _d.x + "," + _d.y;
                d += " H" + (_d.x + _d.width);
                d += " V" + (_d.y + _d.height * 0.85);
                d += " M" + _d.x + "," + _d.y;
                d += " V" + (_d.y + _d.height * 0.85);
                d += " Q" + (_d.x + _d.width * 0.2) + ","
                    + (_d.y + _d.height * 1.1) + " "
                    + (_d.x + _d.width * 0.5) + "," + (_d.y + _d.height * 0.85);
                d += " S" + (_d.x + _d.width * 0.8) + ","
                    + (_d.y + _d.height * 0.5) + " "
                    + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.85);

                return paper.path(d);
            },

            // 信息系统
            DataImage: function () {
                var set = paper.set(),
                    d = "M" + _d.x + "," + (_d.y + 15);
                d += " V" + (_d.y + _d.height - 12);
                d += " C" + (_d.x + _d.width * 0.1) + " " + (_d.y + _d.height) + "," + (_d.x + _d.width * 0.9) + " " + (_d.y + _d.height) + "," + (_d.x + _d.width) + " " + (_d.y + _d.height - 12);
                d += " V" + (_d.y + 15);
                set.push(
                    paper.path(d),
                    paper.ellipse(_d.x + _d.width / 2, _d.y + 15, _d.width / 2, 10)
                );
                return set;
            },

            // 风险点
            RiskPointFigure: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " L" + _d.x + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
                return paper.path(d);
            },

            // 关键控制点
            KeyPointFigure: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " L" + _d.x + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
                return paper.path(d);
            },

            // 跳转:转出
            ToFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // 跳转:转入
            FromFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // 结束
            EndFigure: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " L" + _d.x + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
                return paper.path(d);
            },

            // PA:问题区域
            PAFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // KSF:关键成功因素
            KSFFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // KCP:关键控制点
            KCPFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // XOR:多种情况下只能选中一种
            XORFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // OR:多种情况下只能选中一种或多种
            ORFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // 流程开始
            FlowFigureStart: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height, 30);
            },

            // 流程结束
            FlowFigureStop: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height, 30);
            },

            // AND
            AndFigure: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },

            // 虚拟角色
            VirtualRoleFigure: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },

            // IT决策
            ITRhombus: function () {
                var set = paper.set(),
                    x = _d.x,
                    y = _d.y,
                    width = _d.width,
                    height = _d.height,
                    buildPath = function (x, y, width, height) {
                        var d = "M" + (x + width / 2) + "," + y;
                        d += " L" + x + "," + (y + height / 2);
                        d += " L" + (x + width / 2) + "," + (y + height);
                        d += " L" + (x + width) + "," + (y + height / 2);
                        d += " L" + (x + width / 2) + "," + y;
                        return d;
                    };

                set.push(
                    paper.path(buildPath(x, y, width, height)),
                    paper.path(buildPath(x + 10, y + 7, width - 20, height - 14))
                );
                return set;
            },

            // 等腰图形
            IsoscelesTrapezoidFigure: function () {
                var d = "M" + _d.x + "," + _d.y;
                d += " H" + (_d.x + _d.width);
                d += " L" + (_d.x + _d.width * 0.75) + "," + (_d.y + _d.height);
                d += " H" + (_d.x + _d.width * 0.25);
                d += " L" + _d.x + "," + _d.y;
                return paper.path(d);
            },

            // 岗位
            PositionFigure: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },

            Rect: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },

            // 0级
            FlowLevelFigureOne: function () {
                return this.buildFlowLevelFigure();
            },

            // 1级
            FlowLevelFigureTwo: function () {
                return this.buildFlowLevelFigure();
            },

            // 2级
            FlowLevelFigureThree: function () {
                return this.buildFlowLevelFigure();
            },

            // 3级
            FlowLevelFigureFour: function () {
                return this.buildFlowLevelFigure();
            },

            // 流程图/流程地图/组织图名称文本框
            MapNameText: function () {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            },
 			// 菱形
            MapRhombus: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " L" + _d.x + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
                return paper.path(d);
            },
            
            
            /** AR Graph Start  **/
            
            // 活动AR
            ActiveFigureAR: function () {
                return  paper.rect(_d.x, _d.y, _d.width, _d.height, 4);
            },
               // 角色AR
            RoleFigureAR: function () {
            	// 横向直线
                var vLineStartX = _d.x+5,
                    vLineEndX = _d.x+5,
                    vLineStartY = _d.y,
                    vLineEndY = _d.y +_d.height,
                    roleRectAR = paper.set(),
                    roleRect,
                    vLine;
                roleRectAR.push(
                    roleRect = paper.rect(_d.x, _d.y, _d.width, _d.height),
                    
                    vLine = paper.path("M" + vLineStartX + "," + vLineStartY + "L" + vLineEndX + "," + vLineEndY).attr("stroke-dasharray","- ")
                );
                return roleRectAR;
            },
            
            // 术语
            TermFigureAR: function () {
            	var set = paper.set(),
            		rotateData;
				rotateData = getRotateGraphData(getGraphAngle());
				set.push(
                	paper.rect(_d.x, _d.y, _d.width, _d.height),
                    paper.image("images/svgIcons/POINT.gif", rotateData.x + rotateData.width - 16 - 4, rotateData.y + rotateData.height - 16 - 4, 16, 16)
                );
				return set;
            },
              //XORFigureAR:多种情况下只能选中一种
            XORFigureAR: function () {
                // 横向直线
                var startX1 = _d.x + _d.width/4,
                    endX1 = _d.x + _d.width*3/4,
                    startY1 = _d.y +_d.height/4,
                    endY1 = _d.y + +_d.height*3/4,
                    
                    startX2 =_d.x + _d.width*3/4,
                    endX2 = _d.x + _d.width/4,
                    startY2 = _d.y +_d.height/4,
                    endY2 = _d.y +_d.height*3/4,
                    
                    xorEllipseAR = paper.set(),
                    xorEllipse,
                    xorLine;
               		xorLine = "M" + startX1 + "," + startY1 + "L" + endX1 + "," + endY1;
                    xorLine += "M" + startX2 + "," + startY2 + "L" + endX2 + "," + endY2;
                xorEllipseAR.push(
                    xorEllipse = paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2),
                    paper.path(xorLine)
                );
                return xorEllipseAR;
            },

            //ORFigureAR:多种情况下只能选中一种或多种
            ORFigureAR: function () {
                // 横向直线
                var startX1 = _d.x + _d.width/4,
                    endX1 = _d.x + _d.width/2,
                    startY1 = _d.y +_d.height/3,
                    endY1 = _d.y + +_d.height*2/3,
                    
                    startX2 =_d.x + _d.width*3/4,
                    endX2 = _d.x + _d.width/2,
                    startY2 = _d.y +_d.height/3,
                    endY2 = _d.y +_d.height*2/3,
                    
                    orEllipseAR = paper.set(),
                    orEllipse,
                    orLine;
               		orLine = "M" + startX1 + "," + startY1 + "L" + endX1 + "," + endY1;
                    orLine += "M" + startX2 + "," + startY2 + "L" + endX2 + "," + endY2;
                orEllipseAR.push(
                    orEllipse = paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2),
                    paper.path(orLine)
                );
                return orEllipseAR;
            },
 			// ANDFigureAR:多种情况下只能选中一种
            AndFigureAR: function () {
                 var startX1 = _d.x + _d.width/4,
                    endX1 = _d.x + _d.width/2,
                    startY1 = _d.y + +_d.height*2/3,
                    endY1 = _d.y +_d.height/3,
                    
                    startX2 =_d.x + _d.width*3/4,
                    endX2 = _d.x + _d.width/2,
                    startY2 = _d.y +_d.height*2/3,
                    endY2 = _d.y +_d.height/3,
                    
                    andEllipseAR = paper.set(),
                    andEllipse,
                    andLine;
               		andLine = "M" + startX1 + "," + startY1 + "L" + endX1 + "," + endY1;
                    andLine += "M" + startX2 + "," + startY2 + "L" + endX2 + "," + endY2;
                andEllipseAR.push(
                    andEllipse = paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2),
                    paper.path(andLine)
                );
                return andEllipseAR;
            },

            // 开始：
            StartFigureAR: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },
            // 结束：
            EndFigureAR: function () {
                return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
            },
            
            // KCPFigureAR
            KCPFigureAR : function() {
				var d = "M" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
				d += " L" + (_d.x + _d.width) + "," + _d.y;
				d += " L" + _d.x + "," + _d.y;
				return paper.path(d);
			},
			
			 // 专家决策决策
            ExpertRhombusAR: function () {
                var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                d += " L" + _d.x + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
                d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
                d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
                return paper.path(d);
            },
            
            /** AR Graph End  **/
            
            // 0-3级流程构建处理
            buildFlowLevelFigure: function () {
                var standW = _d.width * 22 / 100,
                    standH = _d.height * 22 / 100,
                    spaceW = _d.width * 4 / 100,
                    spaceH = _d.height * 4 / 100,
                    triangleD,
                    trapeziumD_1,
                    trapeziumD_2,
                    trapeziumD_3;

                // 顶部三角形
                triangleD = "M" + (_d.x + _d.width / 2) + "," + _d.y;
                triangleD += " L" + (_d.x + _d.width / 2 - standW / 2) + "," + (_d.y + standH);
                triangleD += " H" + (_d.x + _d.width / 2 + standW / 2) + "z";

                // 梯形1
                trapeziumD_1 = " M" + (_d.x + _d.width / 2 - standW / 2 - spaceW / 2 ) + "," + (_d.y + standH + spaceH);
                trapeziumD_1 += " H" + (_d.x + _d.width / 2 + standW / 2 + spaceW / 2);
                trapeziumD_1 += " L" + (_d.x + _d.width / 2 + standW + spaceW / 2) + "," + (_d.y + standH * 2 + spaceH);
                trapeziumD_1 += " H" + (_d.x + _d.width / 2 - standW - spaceW / 2) + "z";

                // 梯形2
                trapeziumD_2 = " M" + (_d.x + _d.width / 2 - standW - spaceW) + "," + (_d.y + standH * 2 + spaceH * 2);
                trapeziumD_2 += " H" + (_d.x + _d.width / 2 + standW + spaceW);
                trapeziumD_2 += " L" + (_d.x + _d.width / 2 + standW + spaceW + standW / 2) + "," + (_d.y + standH * 3 + spaceH * 2);
                trapeziumD_2 += " H" + (_d.x + _d.width / 2 - standW - spaceW - standW / 2) + "z";

                // 梯形3
                trapeziumD_3 = " M" + (_d.x + _d.width / 2 - standW - spaceW - standW / 2 - spaceW / 2 ) + "," + (_d.y + standH * 3 + spaceH * 3);
                trapeziumD_3 += " H" + (_d.x + _d.width / 2 + standW + spaceW + standW / 2 + spaceW / 2);
                trapeziumD_3 += " L" + (_d.x + _d.width / 2 + standW * 2 + spaceW * 2) + "," + (_d.y + standH * 4 + spaceH * 4);
                trapeziumD_3 += " H" + (_d.x + _d.width / 2 - standW * 2 - spaceW * 2) + "z";

                return paper.set([
                    paper.path(triangleD),
                    paper.path(trapeziumD_1),
                    paper.path(trapeziumD_2),
                    paper.path(trapeziumD_3)]);
            }
        };


    /**
     * 执行逻辑
     */
    return function () {

        $("#linkInfoDiv").dialog({
            autoOpen: false,
            resizable: false
        });

        mainPaper = Raphael(0, 30, f.paperWidth, f.paperHeight)
            .setSize(f.paperWidth, f.paperHeight)
            .setViewBox(0, 0, f.paperWidth, f.paperHeight, false);
        myLinkSet = mainPaper.set();
        otherLinkSet = mainPaper.set();
        buildRoleMove();
        buildNavPaper();
        $.each(f.graphs, function (i, graphData) {
            _d = graphData;
            graphStyle = graphData.graphStyle;
            textStyle = graphData.textStyle;
            graphType = graphData.type;
            hoverText = graphData.hoverTexts;
            graph3d = null;
            graphShadow = null;
            resetDrawingPaper();
            graph = eprosGraphTypes[graphData.type]();
            if (graphStyle && setSpecialFigureAttrs()) {
                graph.attr(graphStyle);
                graphStyle.graph3dStyle && build3d();
                graphStyle.shadow && buildShadow();
            }
            buildText();
            if (hoverText || hasDblClickEvent()) {
                buildMaskLayer();
                buildHoverTextOnMaskLayer();
                buildDblEventOnMaskLayer();
            }
            buildIcon();
            buildMyLink();
        });
    };
};