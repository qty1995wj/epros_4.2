package com.jecn.epros.service.process.impl;

import com.jecn.epros.domain.file.FileDirCommonBean;
import com.jecn.epros.domain.inventory.FlowInventoryBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 处理流程清单分析获得发布未发布数量，流程架构的map，level级别等
 *
 * @author user
 */
class ProcessInventoryDataHandler {

    private Long flowId;
    private List<FlowInventoryBean> flowsAndMaps;

    private List<FlowInventoryBean> sortedFlows = new ArrayList<FlowInventoryBean>();

    private Map<Long, FileDirCommonBean> maps = new HashMap<Long, FileDirCommonBean>();

    private int minLv;
    private int maxLv;
    private int notPubNum;
    private int approvedNum;
    private int pubNum;

    ProcessInventoryDataHandler(Long flowId, List<FlowInventoryBean> flowsAndMaps) {
        this.flowId = flowId;
        this.flowsAndMaps = flowsAndMaps;
    }

    public ProcessInventoryDataHandler handler() {
        FlowInventoryBean flowInventoryBean = flowsAndMaps.get(0);
        maps.put(flowInventoryBean.getFlowId(), new FileDirCommonBean(flowInventoryBean.getFlowId(),
                flowInventoryBean.getFlowName(), flowInventoryBean.getFlowIdInput(), flowInventoryBean.getLevel()));
        minLv = flowInventoryBean.getLevel();
        processListSort(flowId);
        return this;
    }

    /**
     * 分析流程清单原始数据记录分析结果
     */
    private void processListSort(Long startFlowId) {
        for (FlowInventoryBean flowInventoryBean : flowsAndMaps) {
            if (!startFlowId.equals(flowInventoryBean.getpFlowId())) {
                continue;
            }
            if (flowInventoryBean.getIsFlow() == 0) {
                maps.put(flowInventoryBean.getFlowId(),
                        new FileDirCommonBean(flowInventoryBean.getFlowId(), flowInventoryBean.getFlowName(),
                                flowInventoryBean.getFlowIdInput(), flowInventoryBean.getLevel()));
                if (flowInventoryBean.getLevel() > maxLv) {
                    maxLv = flowInventoryBean.getLevel();
                }
            } else {
                /** 处于什么阶段 0是待建，1是审批，2是发布 */
                switch (flowInventoryBean.getTypeByData()) {
                    case 0:
                        notPubNum++;
                        break;
                    case 1:
                        approvedNum++;
                        break;
                    case 2:
                        pubNum++;
                        break;
                    default:
                        break;
                }
                sortedFlows.add(flowInventoryBean);
            }
            processListSort(flowInventoryBean.getFlowId());
        }
    }

    public int getTotolNum() {
        return pubNum + notPubNum + approvedNum;
    }

    public List<FlowInventoryBean> getSortedFlows() {
        return sortedFlows;
    }

    public void setSortedFlows(List<FlowInventoryBean> sortedFlows) {
        this.sortedFlows = sortedFlows;
    }

    public Map<Long, FileDirCommonBean> getMaps() {
        return maps;
    }

    public void setMaps(Map<Long, FileDirCommonBean> maps) {
        this.maps = maps;
    }

    public int getMinLv() {
        return minLv;
    }

    public void setMinLv(int minLv) {
        this.minLv = minLv;
    }

    public int getMaxLv() {
        return maxLv;
    }

    public void setMaxLv(int maxLv) {
        this.maxLv = maxLv;
    }

    public int getNotPubNum() {
        return notPubNum;
    }

    public void setNotPubNum(int notPubNum) {
        this.notPubNum = notPubNum;
    }

    public int getApprovedNum() {
        return approvedNum;
    }

    public void setApprovedNum(int approvedNum) {
        this.approvedNum = approvedNum;
    }

    public int getPubNum() {
        return pubNum;
    }

    public void setPubNum(int pubNum) {
        this.pubNum = pubNum;
    }

}
