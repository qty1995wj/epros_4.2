package com.jecn.epros.server.dao.file.impl;
import org.hibernate.Query;

import com.jecn.epros.server.bean.file.FileDownLoadCountBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.file.IDownLoadCountDao;


/***
 * 下载次数限制DAO，继承AbsBaseDao
 * @author xiaobo
 * 
 * */
public class DownLoadCountDaoImpl extends AbsBaseDao<FileDownLoadCountBean,Long> implements IDownLoadCountDao{

	
	@Override
	public FileDownLoadCountBean findPeopleExits(Long peopleId){
		String hql="from FileDownLoadCountBean where peopleId=?";
		FileDownLoadCountBean downLoadCountBean= getObjectHql(hql, peopleId);
		return downLoadCountBean;
	}

	@Override
	public void clearDownLoadCountData() {
		String hql="delete from FileDownLoadCountBean";
		Query query=getSession().createQuery(hql);
		query.executeUpdate();
	}
}
