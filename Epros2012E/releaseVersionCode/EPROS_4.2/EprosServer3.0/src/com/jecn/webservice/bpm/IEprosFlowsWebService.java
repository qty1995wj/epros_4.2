package com.jecn.webservice.bpm;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.jecn.epros.server.bean.process.EprosBPMVersion;

/**
 * EPROS与BPM系统对接，提供流程推送的版本记录
 * 
 *@author admin
 * @date 2016-6-16上午10:39:01
 */
@WebService(endpointInterface = "IEprosFlowsWebService")
public interface IEprosFlowsWebService {

	@WebMethod(operationName = "findEprosBpmVersions")
	public List<EprosBPMVersion> findEprosBpmVersions(@WebParam(name = "flowId") String flowId);
}
