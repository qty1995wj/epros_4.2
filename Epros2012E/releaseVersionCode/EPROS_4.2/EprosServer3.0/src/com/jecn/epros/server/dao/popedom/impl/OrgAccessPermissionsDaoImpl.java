package com.jecn.epros.server.dao.popedom.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnFlowFilePosPerm;
import com.jecn.epros.server.bean.popedom.JecnOrgAccessPermissionsT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class OrgAccessPermissionsDaoImpl extends AbsBaseDao<JecnOrgAccessPermissionsT, Long> implements
		IOrgAccessPermissionsDao {

	/**
	 * 设置单一节点部门查阅权限（oracle数据库）
	 * 
	 * @param relateId
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 * @param isModifyPub
	 *            true 是连同发布一起修改
	 */
	private void setNodeAuths(List<Long> list, int type, String orgIds, boolean isModifyPub) {
		if (isModifyPub) {
			String hql = "delete from JecnOrgAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
		}

		if (orgIds == null || "".equals(orgIds)) {
			String hql = "delete from JecnOrgAccessPermissionsT where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
			return;
		}

		List<JecnOrgAccessPermissionsT> listOldT = new ArrayList<JecnOrgAccessPermissionsT>();
		String hql = "from JecnOrgAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		for (String str : listStr) {
			List<JecnOrgAccessPermissionsT> listJecnOrgAccessPermissionsT = this.listHql(str, type);
			for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listJecnOrgAccessPermissionsT) {
				listOldT.add(jecnOrgAccessPermissionsT);
			}
		}

		String[] idsArr = orgIds.split(",");
		for (Long relateId : list) {
			for (String str : idsArr) {
				if (str == null || "".equals(str)) {
					continue;
				}
				Long orgId = Long.valueOf(str);
				boolean isExist = false;
				for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listOldT) {
					if (relateId.equals(jecnOrgAccessPermissionsT.getRelateId())
							&& orgId.equals(jecnOrgAccessPermissionsT.getOrgId())
							&& jecnOrgAccessPermissionsT.getType().intValue() == type) {
						isExist = true;
						listOldT.remove(jecnOrgAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
					jecnOrgAccessPermissionsT.setRelateId(relateId);
					jecnOrgAccessPermissionsT.setType(type);
					jecnOrgAccessPermissionsT.setOrgId(orgId);
					this.save(jecnOrgAccessPermissionsT);
				}

			}
		}
		for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listOldT) {
			this.deleteObject(jecnOrgAccessPermissionsT);
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "insert into jecn_org_access_permissions (id,org_id,relate_id,type) select id,org_id,relate_id,type from jecn_org_access_perm_t where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, list);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}

	}

	/**
	 * 设置单一节点部门查阅权限（oracle数据库）
	 * 
	 * @param relateId
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 * @param isModifyPub
	 *            true 是连同发布一起修改
	 */
	private void setNodeAuths(List<Long> list, int type, List<AccessId> accIds, boolean isModifyPub) {
		if (isModifyPub) {
			String hql = "delete from JecnOrgAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
		}

		if (accIds == null || accIds.isEmpty()) {
			String hql = "delete from JecnOrgAccessPermissionsT where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			for (String str : listStr) {
				this.execteHql(str, type);
			}
			return;
		}

		List<JecnOrgAccessPermissionsT> listOldT = new ArrayList<JecnOrgAccessPermissionsT>();
		String hql = "from JecnOrgAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		for (String str : listStr) {
			List<JecnOrgAccessPermissionsT> listJecnOrgAccessPermissionsT = this.listHql(str, type);
			for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listJecnOrgAccessPermissionsT) {
				listOldT.add(jecnOrgAccessPermissionsT);
			}
		}

		for (Long relateId : list) {
			for (AccessId ids : accIds) {
				if (ids == null || ids.getAccessId() == null) {
					continue;
				}
				boolean isExist = false;
				for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listOldT) {
					if (relateId.equals(jecnOrgAccessPermissionsT.getRelateId())
							&& ids.getAccessId().equals(jecnOrgAccessPermissionsT.getOrgId())
							&& jecnOrgAccessPermissionsT.getType().intValue() == type
							&& jecnOrgAccessPermissionsT.getAccessType().intValue() == (ids.isDownLoad() ? 1 : 0)) {
						isExist = true;
						listOldT.remove(jecnOrgAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
					jecnOrgAccessPermissionsT.setRelateId(relateId);
					jecnOrgAccessPermissionsT.setType(type);
					jecnOrgAccessPermissionsT.setOrgId(ids.getAccessId());
					jecnOrgAccessPermissionsT.setAccessType(ids.isDownLoad() ? 1 : 0);
					this.save(jecnOrgAccessPermissionsT);
				}

			}
		}
		for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listOldT) {
			this.deleteObject(jecnOrgAccessPermissionsT);
		}
		if (isModifyPub) {
			getSession().flush();
			String sql = "insert into jecn_org_access_permissions (id,org_id,relate_id,type,ACCESS_TYPE) select id,org_id,relate_id,type,ACCESS_TYPE from jecn_org_access_perm_t where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, list);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}

	}

	/**
	 * @author zhangchen Oct 4, 2013
	 * @description：
	 * @param relateId
	 *            关联ID
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 *            是组织集合 *
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0是管理员（新建时候不能为0）
	 * 
	 * @throws Exception
	 */
	@Override
	public void savaOrUpdate(Long relateId, int type, String orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, orgType, projectId, nodeType);
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.setNodeAuths(listIds, type, orgIds, isModifyPub);
	}

	/**
	 * @author zhangchen Oct 4, 2013
	 * @description：
	 * @param relateId
	 *            关联ID
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 *            是组织集合 *
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0是管理员（新建时候不能为0）
	 * 
	 * @throws Exception
	 */
	@Override
	public void savaOrUpdate(Long relateId, int type, List<AccessId> accId, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, orgType, projectId, nodeType);
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.setNodeAuths(listIds, type, accId, isModifyPub);
	}

	@Override
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type, boolean isPub) throws Exception {
		String sql = "";
		if (isPub) {
			sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count ,joap.access_type"
					+ " from jecn_flow_org jfo,JECN_ORG_ACCESS_PERMISSIONS joap where jfo.org_id = joap.org_id and joap.relate_id=? and joap.type=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
		} else {
			sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count,joap.access_type"
					+ " from jecn_flow_org jfo,JECN_ORG_ACCESS_PERM_T joap where jfo.org_id = joap.org_id and joap.relate_id=? and joap.type=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
		}
		List<Object[]> listOrg = listNativeSql(sql, relateId, type);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listOrg) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrgAccess(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, String orgIds, boolean isSave) throws Exception {
		if (isSave) {
			if (orgIds != null && !"".equals(orgIds)) {
				String[] idsArr = orgIds.split(",");
				for (String str : idsArr) {
					JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
					jecnOrgAccessPermissionsT.setType(type);
					jecnOrgAccessPermissionsT.setRelateId(relateId);
					jecnOrgAccessPermissionsT.setOrgId(Long.valueOf(str));
					this.save(jecnOrgAccessPermissionsT);
				}
			}
			return;
		}
		// 部门呢查阅权限
		String hql = "from JecnOrgAccessPermissionsT where type=? and relateId=?";
		List<JecnOrgAccessPermissionsT> listJecnOrgAccessPermissions = this.listHql(hql, type, relateId);
		if (orgIds != null && !"".equals(orgIds)) {
			String[] idsArr = orgIds.split(",");
			for (String str : idsArr) {
				if (str == null || "".equals(str)) {
					continue;
				}
				boolean isExist = false;
				for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listJecnOrgAccessPermissions) {
					if (str.equals(jecnOrgAccessPermissionsT.getOrgId().toString())) {
						isExist = true;
						listJecnOrgAccessPermissions.remove(jecnOrgAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
					jecnOrgAccessPermissionsT.setType(type);
					jecnOrgAccessPermissionsT.setRelateId(relateId);
					jecnOrgAccessPermissionsT.setOrgId(Long.valueOf(str));
					this.save(jecnOrgAccessPermissionsT);
				}
			}
		}
		for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listJecnOrgAccessPermissions) {
			this.deleteObject(jecnOrgAccessPermissionsT);
		}
	}

	@Override
	public void savaOrUpdate(Long relateId, int type, List<AccessId> Ids, boolean isSave) throws Exception {
		if (isSave) {
			if (Ids != null && !Ids.isEmpty()) {
				for (AccessId accessId : Ids) {
					JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
					jecnOrgAccessPermissionsT.setType(type);
					jecnOrgAccessPermissionsT.setRelateId(relateId);
					jecnOrgAccessPermissionsT.setOrgId(accessId.getAccessId());
					jecnOrgAccessPermissionsT.setAccessType(accessId.isDownLoad() ? 1 : 0);// 是否具有下载权限
					this.save(jecnOrgAccessPermissionsT);
				}
				return;
			}
		}
		// 部门呢查阅权限
		String hql = "from JecnOrgAccessPermissionsT where type=? and relateId=?";
		List<JecnOrgAccessPermissionsT> listJecnOrgAccessPermissions = this.listHql(hql, type, relateId);
		if (Ids != null && !Ids.isEmpty()) {
			for (AccessId accessId : Ids) {
				if (accessId.getAccessId() == null) {
					continue;
				}
				boolean isExist = false;
				for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listJecnOrgAccessPermissions) {
					if (accessId.getAccessId().equals(jecnOrgAccessPermissionsT.getOrgId().toString())) {
						isExist = true;
						listJecnOrgAccessPermissions.remove(jecnOrgAccessPermissionsT);
						break;
					}
				}
				if (!isExist) {
					JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
					jecnOrgAccessPermissionsT.setType(type);
					jecnOrgAccessPermissionsT.setRelateId(relateId);
					jecnOrgAccessPermissionsT.setOrgId(accessId.getAccessId());
					jecnOrgAccessPermissionsT.setAccessType(accessId.isDownLoad() ? 1 : 0);// 是否具有下载权限
					this.save(jecnOrgAccessPermissionsT);
				}
			}
		}
		for (JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT : listJecnOrgAccessPermissions) {
			this.deleteObject(jecnOrgAccessPermissionsT);
		}
	}

	@Override
	public void del(Long relateId, int type, String orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (StringUtils.isBlank(orgIds)) {
			return;
		}
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, orgType, projectId, nodeType);
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, orgIds, isModifyPub);

	}

	@Override
	public void del(Long relateId, int type, List<AccessId> orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		if (orgIds == null || orgIds.isEmpty()) {
			return;
		}
		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, orgType, projectId, nodeType);
		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, orgIds, isModifyPub);

	}

	/**
	 * 删除一些节点的一些部门的查阅权限
	 * 
	 * @author hyl
	 * @param list
	 *            需要修改的节点的查阅权限
	 * @param type
	 * @param orgIdsStr
	 *            需要删除的部门的id，使用逗号分隔
	 * @param isModifyPub
	 *            是否发布
	 * @throws Exception
	 */
	private void delNodeAuths(List<Long> list, int type, String orgIdsStr, boolean isModifyPub) throws Exception {

		if (StringUtils.isBlank(orgIdsStr) || list == null || list.size() == 0) {
			return;
		}
		List<String> orgIds = Arrays.asList(orgIdsStr.split(","));
		if (isModifyPub) {
			String hql = "delete from JecnOrgAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			String lastHql = "";
			for (String subHql : listStr) {
				for (String orgId : orgIds) {
					lastHql = subHql + " and orgId =?";
					this.execteHql(lastHql, type, Long.valueOf(orgId));
				}
			}

		}

		String hql = "delete from JecnOrgAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		String lastHql = "";
		for (String subHql : listStr) {
			for (String orgId : orgIds) {
				lastHql = subHql + " and orgId =?";
				this.execteHql(lastHql, type, Long.valueOf(orgId));
			}
		}

	}

	/**
	 * 删除一些节点的一些部门的查阅权限
	 * 
	 * @author hyl
	 * @param list
	 *            需要修改的节点的查阅权限
	 * @param type
	 * @param orgIdsStr
	 *            需要删除的部门的id，使用逗号分隔
	 * @param isModifyPub
	 *            是否发布
	 * @throws Exception
	 */
	private void delNodeAuths(List<Long> list, int type, List<AccessId> orgIds, boolean isModifyPub) throws Exception {

		if (orgIds == null || list == null || list.size() == 0) {
			return;
		}

		if (isModifyPub) {
			String hql = "delete from JecnOrgAccessPermissions where type=? and relateId in";
			List<String> listStr = JecnCommonSql.getListSqls(hql, list);
			String lastHql = "";
			for (String subHql : listStr) {
				for (AccessId orgId : orgIds) {
					lastHql = subHql + " and orgId =?";
					this.execteHql(lastHql, type, orgId.getAccessId());
				}
			}

		}

		String hql = "delete from JecnOrgAccessPermissionsT where type=? and relateId in";
		List<String> listStr = JecnCommonSql.getListSqls(hql, list);
		String lastHql = "";
		for (String subHql : listStr) {
			for (AccessId orgId : orgIds) {
				lastHql = subHql + " and orgId =?";
				this.execteHql(lastHql, type, orgId.getAccessId());
			}
		}

	}

	@Override
	public void add(Long relateId, int type, String orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {

		if (StringUtils.isBlank(orgIds)) {
			return;
		}

		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, orgType, projectId, nodeType);

		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, orgIds, isModifyPub);

		// 添加
		String[] idsArr = orgIds.split(",");
		for (Long nodeId : listIds) {
			for (String str : idsArr) {
				Long orgId = Long.valueOf(str);
				JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
				jecnOrgAccessPermissionsT.setRelateId(nodeId);
				jecnOrgAccessPermissionsT.setType(type);
				jecnOrgAccessPermissionsT.setOrgId(orgId);
				this.save(jecnOrgAccessPermissionsT);
			}
		}
		if (isModifyPub) {
			getSession().flush();

			String sql = "delete from jecn_org_access_permissions where type=? and relate_id in";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				this.execteNative(str, type);
			}

			sql = "insert into jecn_org_access_permissions (id,org_id,relate_id,type) select id,org_id,relate_id,type from jecn_org_access_perm_t where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}
	}

	@Override
	public void add(Long relateId, int type, List<AccessId> orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception {

		if (orgIds == null || orgIds.isEmpty()) {
			return;
		}

		List<Long> listIds = getNeedChangeAuthNodes(relateId, type, orgType, projectId, nodeType);

		boolean isModifyPub = false;
		if (userType == 0) {
			isModifyPub = true;
		}
		this.delNodeAuths(listIds, type, orgIds, isModifyPub);
		// 添加
		for (Long nodeId : listIds) {
			for (AccessId ids : orgIds) {
				Long orgId = ids.getAccessId();
				JecnOrgAccessPermissionsT jecnOrgAccessPermissionsT = new JecnOrgAccessPermissionsT();
				jecnOrgAccessPermissionsT.setRelateId(nodeId);
				jecnOrgAccessPermissionsT.setType(type);
				jecnOrgAccessPermissionsT.setOrgId(orgId);
				jecnOrgAccessPermissionsT.setAccessType(ids.isDownLoad() ? 1 : 0);
				this.save(jecnOrgAccessPermissionsT);
			}
		}
		if (isModifyPub) {
			getSession().flush();

			String sql = "delete from jecn_org_access_permissions where type=? and relate_id in";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				this.execteNative(str, type);
			}

			sql = "insert into jecn_org_access_permissions (id,org_id,relate_id,type,ACCESS_TYPE) select id,org_id,relate_id,type,ACCESS_TYPE from jecn_org_access_perm_t where type=? and relate_id in";
			listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				this.execteNative(str, type);
			}
		}
	}

	private List<Long> getNeedChangeAuthNodes(Long relateId, int type, int orgType, Long projectId,
			TreeNodeType nodeType) throws Exception {
		List<Long> listIds = new ArrayList<Long>();
		if (orgType == 0) {
			listIds = JecnUtil.getNeedSetAuthNodesSingle(relateId, type, nodeType, this);
		} else if (orgType == 1) {
			listIds = JecnUtil.getNeedSetAuthNodesContainChildNode(relateId, type, projectId, this);
		}
		return listIds;
	}

	@Override
	public List<JecnFlowFilePosPerm> findFlowFileDownloadPerms(Set<Long> flowSet) throws Exception {

		String idsSet = JecnCommonSql.getIdsSet(flowSet);
		String hql = "from JecnFlowFilePosPerm where relateId in " + idsSet;
		List<JecnFlowFilePosPerm> perms = this.listHql(hql);

		return perms;
	}

	/**
	 * 根据父节点的 权限 添加到本节点
	 * 
	 * @param pId
	 * @param thisId
	 */
	public void addPermissionisParentNode(Long pId, Long thisId) {
		if (JecnConfigTool.isPrmissionisParentNode()) {
			String posId = "";
			String orgId = "";
			String posGId = "";
			if (JecnCommon.isOracle()) {
				posId = "JECN_PERMISSIONS_SQUENCE.Nextval id,";
				orgId = "JECN_PERMISSIONS_SQUENCE.Nextval id,";
				posGId = " JECN_PERMISSIONS_SQUENCE.Nextval id,";
			}
			String sql = "insert into Jecn_Access_Permissions_t " + "  select " + posId + " t.figure_id, " + thisId
					+ " relate_id, t.type, t.access_type" + "    from Jecn_Access_Permissions_t t"
					+ "   where t.relate_id = ?";
			this.execteNative(sql, pId);

			sql = "insert into Jecn_Org_Access_Perm_t  select " + orgId + " t.org_id, " + thisId
					+ " relate_id, t.type, t.access_type" + "    from Jecn_Org_Access_Perm_t t"
					+ "   where t.relate_id = ?";
			this.execteNative(sql, pId);

			sql = "insert into JECN_GROUP_PERMISSIONS_T " + "  select " + posGId + " POSTGROUP_ID, " + thisId
					+ " relate_id, t.type, t.access_type" + "    from JECN_GROUP_PERMISSIONS_T t"
					+ "   where t.relate_id = ?";
			this.execteNative(sql, pId);
		}
	}

}
