
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUsersAndRolesResult" type="{http://tempuri.org/}ArrayOfRole" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUsersAndRolesResult"
})
@XmlRootElement(name = "GetUsersAndRolesResponse")
public class GetUsersAndRolesResponse {

    @XmlElement(name = "GetUsersAndRolesResult")
    protected ArrayOfRole getUsersAndRolesResult;

    /**
     * 获取getUsersAndRolesResult属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRole }
     *     
     */
    public ArrayOfRole getGetUsersAndRolesResult() {
        return getUsersAndRolesResult;
    }

    /**
     * 设置getUsersAndRolesResult属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRole }
     *     
     */
    public void setGetUsersAndRolesResult(ArrayOfRole value) {
        this.getUsersAndRolesResult = value;
    }

}
