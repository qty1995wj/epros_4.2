package epros.designer.gui.popedom.role;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnRoleContent;
import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.define.TermDefineChooseDialog;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.integration.risk.RiskChooseDialog;
import epros.designer.gui.popedom.positiongroup.choose.PosGropChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManageFileChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManagePosGropChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManageProcessChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManageRiskChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManageRuleChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManageStandardChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManageTermDefineChooseDialog;
import epros.designer.gui.process.ProcessChooseDialog;
import epros.designer.gui.rule.RuleChooseDialog;
import epros.designer.gui.standard.StandardChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public abstract class RoleDialog extends JecnDialog {
	protected static Logger log = Logger.getLogger(RoleDialog.class);

	/** 主面板 */
	protected JPanel mainPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = new JPanel();

	/** 角色名称Lab */
	private JLabel roleNameLab = null;

	/** 角色textField */
	private JTextField roleNameField = null;

	/** 角色验证Lab */
	private JLabel roleNamePrompt = null;

	/** 角色必填显示 */
	private JLabel verRequiredLab = new JLabel(JecnProperties.getValue("required"));

	/** 流程权限Lab Permissions */
	private JLabel flowPerLab = null;

	/** 流程权限Area */
	private JecnTextArea flowPerArea = null;
	private JScrollPane flowPerScrollPane = null;

	/** 文件权限filePerLab */
	private JLabel filePerLab = null;

	/** 文件权限filePerArea */
	private JecnTextArea filePerArea = null;
	private JScrollPane filePerScrollPane = null;

	/** 标准权限Lab */
	private JLabel standPerLab = null;

	/** 标准权限standPerArea */
	private JecnTextArea standPerArea = null;
	private JScrollPane standPerScrollPane = null;

	/** 制度权限Lab */
	private JLabel rulePerLab = null;

	/** 制度权限rulePerArea */
	private JecnTextArea rulePerArea = null;
	private JScrollPane rulePerScrollPane = null;

	/** 风险权限 C */
	private JLabel riskLab = null;
	private JecnTextArea riskArea = null;
	private JScrollPane riskPerScrollPane = null;
	private JButton riskButton = null;

	/** 术语权限 C */
	private JLabel termDefineLab = null;
	private JecnTextArea termDefineArea = null;
	private JScrollPane termDefinePerScrollPane = null;
	private JButton termDefineButton = null;

	/** 组织权限 */
	protected JLabel orgLab = null;
	protected JecnTextArea orgArea = null;
	protected JScrollPane orgScrollPane = null;
	protected JButton orgButton = null;

	/** 岗位组权限 */
	protected JLabel posGroupLab = null;
	protected JecnTextArea posGroupArea = null;
	protected JScrollPane posGroupScrollPane = null;
	protected JButton posGroupButton = null;

	/** 流程管理员相关人员选择框 */
	protected JLabel personLab = null;
	protected JecnTextArea personArea = null;
	protected JScrollPane personScrollPane = null;
	protected JButton personButton = null;

	/** 角色备注Lab */
	private JLabel roleRemarkLab = null;

	/** 角色备注验证Lab */
	private JLabel roleRemarkPrompt = null;

	/** 角色备注Area */
	private JTextArea roleRemarkArea = null;
	private JScrollPane roleRemarkScrollPane = null;
	/** 备注 */
	private String roleContent = "";
	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBut = null;

	/** 流程权限选择按钮 */
	private JButton flowPerBut = null;

	/** 文件权限选择按钮 */
	private JButton filePerBut = null;

	/** 标准权限选择按钮 */
	private JButton standperBut = null;

	/** 制度权限选择按钮 */
	private JButton rulePerBut = null;

	/** 流程权限id集合 */
	private String flowIds = "";
	/** 文件权限id集合 */
	private String fileIds = "";
	/** 标准权限id集合 */
	private String standardIds = "";
	/** 制度权限id集合 */
	private String ruleIds = "";

	/** 风险权限集合 */
	private String riskIds = "";
	/** 术语权限集合 */
	private String termDefineIds = "";

	/** 岗位组权限集合 */
	private String posGroupIds = "";
	/** 部门权限集合 */
	protected String orgIds = "";
	/** 部门权限集合 */
	protected String personIds = "";

	// 流程权限集合
	protected List<JecnTreeBean> listFlow = new ArrayList<JecnTreeBean>();
	// 文件权限集合
	protected List<JecnTreeBean> listFile = new ArrayList<JecnTreeBean>();
	// 标准权限集合
	protected List<JecnTreeBean> listStandard = new ArrayList<JecnTreeBean>();
	// 制度权限集合
	protected List<JecnTreeBean> listRule = new ArrayList<JecnTreeBean>();
	// 风险权限集合
	protected List<JecnTreeBean> listRisk = new ArrayList<JecnTreeBean>();
	// 术语权限集合
	protected List<JecnTreeBean> listTermDefine = new ArrayList<JecnTreeBean>();
	// 岗位组权限集合
	protected List<JecnTreeBean> listPosGroup = new ArrayList<JecnTreeBean>();
	/** 组织权限集合 */
	protected List<JecnTreeBean> listOrg = new ArrayList<JecnTreeBean>();
	/** 流程管理员 */
	protected List<JecnTreeBean> secondAdminPeoples = new ArrayList<JecnTreeBean>();

	protected JecnRoleInfo jecnRoleInfo = null;
	protected boolean isOperation = false;

	public RoleDialog() {
		initCompotents();
		initLayout();
		actionInit();
		this.setModal(true);
		this.setLocationRelativeTo(null);
	}

	public String getFlowIds() {
		return flowIds;
	}

	public void setFlowIds(String flowIds) {
		this.flowIds = flowIds;
	}

	public String getFileIds() {
		return fileIds;
	}

	public void setFileIds(String fileIds) {
		this.fileIds = fileIds;
	}

	public String getStandardIds() {
		return standardIds;
	}

	public void setStandardIds(String standardIds) {
		this.standardIds = standardIds;
	}

	public String getRuleIds() {
		return ruleIds;
	}

	public void setRuleIds(String ruleIds) {
		this.ruleIds = ruleIds;
	}

	public String getRiskIds() {
		return riskIds;
	}

	public void setRiskIds(String riskIds) {
		this.riskIds = riskIds;
	}

	/** 设置面板控件大小 */
	private Dimension dimension = null;

	/** 角色名称 */
	private String roleName = "";

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
		this.roleNameField.setText(roleName);
	}

	public String getRoleContent() {
		return roleContent;
	}

	public void setRoleContent(String roleContent) {
		this.roleContent = roleContent;
		this.roleRemarkArea.setText(roleContent);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:流程权限Names
	 * @param names
	 */
	public void setFlowPerNames(String names) {
		flowPerArea.setText(names);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:文件权限Names
	 * @param names
	 */
	public void setFilePerNames(String name) {
		filePerArea.setText(name);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:标准权限Names
	 * @param name
	 */
	public void setStandardPerNames(String name) {
		standPerArea.setText(name);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:制度权限Names
	 * @param name
	 */
	public void setRulePerNames(String name) {
		rulePerArea.setText(name);
	}

	/**
	 * @author xiaobo
	 * @description:风险权限Names
	 * @param name
	 */
	public void setRiskPerNames(String name) {
		riskArea.setText(name);
	}

	/**
	 * @author zhanghr
	 * @description:术语权限Names
	 * @param name
	 */
	public void setTermPerNames(String name) {
		termDefineArea.setText(name);
	}

	/**
	 * @author xiaobo
	 * @description:风险权限Names
	 * @param name
	 */
	public void setPosGroupPerNames(String name) {
		posGroupArea.setText(name);
	}

	/***
	 * 初始化组件
	 */
	protected void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 角色名称Lab
		roleNameLab = new JLabel(JecnProperties.getValue("roleNameC"));

		// 角色textfiled
		roleNameField = new JTextField();

		// 角色验证Lab
		roleNamePrompt = new JLabel();

		// 流程权限Lab Permissions
		flowPerLab = new JLabel(JecnProperties.getValue("flowPopedomC"));

		// 流程权限Area
		flowPerArea = new JecnTextArea(false);
		flowPerScrollPane = new JScrollPane(flowPerArea);
		flowPerScrollPane.setBorder(null);
		flowPerScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowPerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 文件权限filePerLab
		filePerLab = new JLabel(JecnProperties.getValue("filePopedomC"));

		// 文件权限filePerArea
		filePerArea = new JecnTextArea(false);
		filePerScrollPane = new JScrollPane(filePerArea);
		filePerScrollPane.setBorder(null);
		filePerScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		filePerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 标准权限Lab
		standPerLab = new JLabel(JecnProperties.getValue("standardPopedomC"));

		// 标准权限standPerArea
		standPerArea = new JecnTextArea(false);
		standPerScrollPane = new JScrollPane(standPerArea);
		standPerScrollPane.setBorder(null);
		standPerScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		standPerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 制度权限Lab
		rulePerLab = new JLabel(JecnProperties.getValue("rulePopedomC"));

		// 制度权限rulePerArea
		rulePerArea = new JecnTextArea(false);
		rulePerScrollPane = new JScrollPane(rulePerArea);
		rulePerScrollPane.setBorder(null);
		rulePerScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		rulePerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// ===============风险权限初始化组件C====================================================
		riskLab = new JLabel(JecnProperties.getValue("riskPower"));
		riskArea = new JecnTextArea(false);
		riskPerScrollPane = new JScrollPane(riskArea);
		riskPerScrollPane.setBorder(null);
		riskPerScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		riskPerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		riskButton = new JButton(JecnProperties.getValue("selectBtn"));
		// ==============================================================================

		// ===============术语权限初始化组件C====================================================
		termDefineLab = new JLabel(JecnProperties.getValue("terminologicalScope"));
		termDefineArea = new JecnTextArea(false);
		termDefinePerScrollPane = new JScrollPane(termDefineArea);
		termDefinePerScrollPane.setBorder(null);
		termDefinePerScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		termDefinePerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		termDefineButton = new JButton(JecnProperties.getValue("selectBtn"));
		// ==============================================================================

		// 角色备注Lab
		roleRemarkLab = new JLabel(JecnProperties.getValue("roleRemarkC"));

		// 角色备注验证Lab
		roleRemarkPrompt = new JLabel();

		// 角色备注TextArea
		roleRemarkArea = new JTextArea();
		roleRemarkScrollPane = new JScrollPane(roleRemarkArea);
		roleRemarkScrollPane.setBorder(null);
		roleRemarkScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		roleRemarkScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		roleRemarkArea.setLineWrap(true);

		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));

		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

		// 流程权限选择按钮
		flowPerBut = new JButton(JecnProperties.getValue("selectBtn"));

		// 文件权限选择按钮
		filePerBut = new JButton(JecnProperties.getValue("selectBtn"));

		// 标准权限选择按钮
		standperBut = new JButton(JecnProperties.getValue("selectBtn"));

		// 制度权限选择按钮
		rulePerBut = new JButton(JecnProperties.getValue("selectBtn"));

		// 设置Dialog窗体大小
		this.setSize(600, 600);

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置窗体大小不被改变
		this.setResizable(true);

		// // 设置主面板的大小
		// dimension = new Dimension(530, 510);
		// mainPanel.setPreferredSize(dimension);
		// mainPanel.setMaximumSize(dimension);
		// mainPanel.setMinimumSize(dimension);

		// // 设置角色名称文本大小
		// dimension = new Dimension(350, 20);
		// roleNameField.setPreferredSize(dimension);
		// roleNameField.setMaximumSize(dimension);
		// roleNameField.setMinimumSize(dimension);
		// =========设置验证提示Label的大小=========
		// dimension = new Dimension(350, 12);
		// // 角色名称验证Lab
		// roleNamePrompt.setPreferredSize(dimension);
		// roleNamePrompt.setMaximumSize(dimension);
		// roleNamePrompt.setMinimumSize(dimension);
		roleNamePrompt.setForeground(Color.red);

		// // 角色备注验证Lab
		// roleRemarkPrompt.setPreferredSize(dimension);
		// roleRemarkPrompt.setMaximumSize(dimension);
		// roleRemarkPrompt.setMinimumSize(dimension);
		roleRemarkPrompt.setForeground(Color.red);

		// 设置角色备注Area大小
		// dimension = new Dimension(430, 55);
		// roleRemarkArea.setPreferredSize(dimension);
		// roleRemarkArea.setMaximumSize(dimension);
		// roleRemarkArea.setMinimumSize(dimension);
		roleRemarkArea.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		verRequiredLab.setForeground(Color.red);

		// 岗位组权限初始化
		posGroupLab = new JLabel(JecnProperties.getValue("posGPowerC"));
		posGroupArea = new JecnTextArea(false);
		posGroupScrollPane = new JScrollPane(posGroupArea);
		posGroupScrollPane.setBorder(null);
		posGroupScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		posGroupScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		posGroupButton = new JButton(JecnProperties.getValue("selectBtn"));

	}

	/**
	 * 布局
	 */
	protected void initLayout() {
		this.setTitle(getDialogTitle());
		// 主面板布局
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(6, 6, 6, 6);
		Insets insety = new Insets(1, 6, 1, 6);
		Insets insetx = new Insets(6, 6, 0, 6);
		Insets insetz = new Insets(0, 6, 6, 6);

		int gridy = 0;
		// 角色名称
		c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(roleNameLab, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		mainPanel.add(roleNameField, c);

		c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(verRequiredLab, c);
		gridy++;
		// c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insetz,
		// 0, 0);
		// mainPanel.add(roleNamePrompt, c);
		// 流程权限
		c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(flowPerLab, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				insets, 0, 0);
		mainPanel.add(flowPerScrollPane, c);
		c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(flowPerBut, c);
		gridy++;

		// 文件权限
		c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(filePerLab, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				insets, 0, 0);
		mainPanel.add(filePerScrollPane, c);
		c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(filePerBut, c);
		gridy++;

		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
			// 标准权限
			c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(standPerLab, c);
			c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
					insets, 0, 0);
			mainPanel.add(standPerScrollPane, c);
			c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(standperBut, c);
			gridy++;
			// 制度权限
			c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(rulePerLab, c);
			c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
					insets, 0, 0);
			mainPanel.add(rulePerScrollPane, c);
			c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(rulePerBut, c);
			gridy++;

			// ==将风险权限添加到主面板C====
			c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(riskLab, c);
			c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
					insets, 0, 0);
			mainPanel.add(riskPerScrollPane, c);
			c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(riskButton, c);
			gridy++;
			// ================================

			// ==将术语权限添加到主面板C====
			c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(termDefineLab, c);
			c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
					insets, 0, 0);
			mainPanel.add(termDefinePerScrollPane, c);
			c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			mainPanel.add(termDefineButton, c);
			gridy++;
			// ================================
		}

		// 组织权限只有系统管理员可以设置
		gridy = initOrgAndPersonPanel(gridy, insets);

		// 岗位组权限
		c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(posGroupLab, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				insets, 0, 0);
		mainPanel.add(posGroupScrollPane, c);
		c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(posGroupButton, c);
		gridy++;

		// 角色备注
		c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(roleRemarkLab, c);
		c = new GridBagConstraints(1, gridy, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				insetx, 0, 0);
		mainPanel.add(roleRemarkScrollPane, c);
		gridy++;

		c = new GridBagConstraints(1, gridy, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insety,
				0, 0);
		mainPanel.add(roleRemarkPrompt, c);

		// 按钮
		c = new GridBagConstraints(0, gridy, 3, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetz, 0, 0);
		mainPanel.add(buttonPanel, c);
		gridy++;

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(roleNamePrompt);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);

	}

	/**
	 * 添加组织权限和流程管理员人员选择
	 */
	protected int initOrgAndPersonPanel(int gridy, Insets insets) {
		return gridy;
	}

	protected void actionInit() {
		flowPerBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnSelectChooseDialog processChooseDialog = null;
				if (JecnDesignerCommon.isAdmin()) {
					processChooseDialog = new ProcessChooseDialog(listFlow);
					processChooseDialog.setChooseType(23);// 设置可以选择文件夹
				} else {
					processChooseDialog = new RoleManageProcessChooseDialog(listFlow);
				}
				processChooseDialog.setVisible(true);
				if (processChooseDialog.isOperation()) {
					if (listFlow != null && listFlow.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : listFlow) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						flowIds = sbIds.substring(0, sbIds.length() - 1);
						flowPerArea.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						flowPerArea.setText("");
						flowIds = "";
					}

				}
			}
		});
		filePerBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnSelectChooseDialog fileChooseDialog = null;
				if (JecnDesignerCommon.isAdmin()) {
					fileChooseDialog = new FileChooseDialog(listFile);
					fileChooseDialog.setChooseType(23);// 设置可以选择文件夹
				} else {
					fileChooseDialog = new RoleManageFileChooseDialog(listFile);
				}
				fileChooseDialog.setVisible(true);
				if (fileChooseDialog.isOperation()) {

					if (listFile != null && listFile.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : listFile) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						fileIds = sbIds.substring(0, sbIds.length() - 1);
						filePerArea.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						filePerArea.setText("");
						fileIds = "";
					}

				}

			}
		});
		standperBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnSelectChooseDialog standardChooseDialog = null;
				if (JecnDesignerCommon.isAdmin()) {
					standardChooseDialog = new StandardChooseDialog(listStandard);
					standardChooseDialog.setChooseType(23);// 设置可以选择文件夹
				} else {
					standardChooseDialog = new RoleManageStandardChooseDialog(listStandard);
				}
				standardChooseDialog.setVisible(true);
				if (standardChooseDialog.isOperation()) {
					if (listStandard != null && listStandard.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : listStandard) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						standardIds = sbIds.substring(0, sbIds.length() - 1);
						standPerArea.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						standPerArea.setText("");
						standardIds = "";
					}
				}
			}
		});
		rulePerBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnSelectChooseDialog ruleChooseDialog = null;
				if (JecnDesignerCommon.isAdmin()) {
					ruleChooseDialog = new RuleChooseDialog(listRule);
					ruleChooseDialog.setChooseType(23);// 设置可以选择文件夹
				} else {
					ruleChooseDialog = new RoleManageRuleChooseDialog(listRule);
				}
				ruleChooseDialog.setVisible(true);
				if (ruleChooseDialog.isOperation()) {
					if (listRule != null && listRule.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : listRule) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						ruleIds = sbIds.substring(0, sbIds.length() - 1);
						rulePerArea.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						rulePerArea.setText("");
						ruleIds = "";
					}

				}
			}
		});

		// 风险权限点击事件C
		riskButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 选择权限对话框C
				JecnSelectChooseDialog riskChooseDialog = null;
				if (JecnDesignerCommon.isAdmin()) {
					riskChooseDialog = new RiskChooseDialog(listRisk);
					riskChooseDialog.setChooseType(23);// 设置可以选择文件夹
				} else {
					riskChooseDialog = new RoleManageRiskChooseDialog(listRisk);
				}
				// 设置标题（风险选择）
				riskChooseDialog.setTitle(JecnProperties.getValue("riskSelect"));
				riskChooseDialog.setVisible(true);
				// 判断权限是否修改C
				if (riskChooseDialog.isOperation()) {
					if (listRisk != null && listRisk.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : listRisk) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						riskIds = sbIds.substring(0, sbIds.length() - 1);
						riskArea.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						riskArea.setText("");
						riskIds = "";
					}
				}
			}
		});

		// 术语权限点击事件C
		termDefineButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 选择权限对话框C
				JecnSelectChooseDialog termDefineChooseDialog = null;
				if (JecnDesignerCommon.isAdmin()) {
					termDefineChooseDialog = new TermDefineChooseDialog(listTermDefine);
					termDefineChooseDialog.setChooseType(32);
				} else {
					termDefineChooseDialog = new RoleManageTermDefineChooseDialog(listTermDefine);
				}
				termDefineChooseDialog.setSelectMutil(true);
				// 设置标题（风险选择）
				termDefineChooseDialog.setTitle(JecnProperties.getValue("termSelect"));
				termDefineChooseDialog.setVisible(true);
				// 判断权限是否修改C
				if (termDefineChooseDialog.isOperation()) {
					if (listTermDefine != null && listTermDefine.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : listTermDefine) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						termDefineIds = sbIds.substring(0, sbIds.length() - 1);
						termDefineArea.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						termDefineArea.setText("");
						termDefineIds = "";
					}
				}
			}
		});

		posGroupButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 选择权限对话框C
				JecnSelectChooseDialog posGroupChooseDialog = null;
				if (JecnDesignerCommon.isAdmin()) {
					posGroupChooseDialog = new PosGropChooseDialog(listPosGroup);
					posGroupChooseDialog.setChooseType(23);// 设置可以选择文件夹
				} else {
					posGroupChooseDialog = new RoleManagePosGropChooseDialog(listPosGroup, 31);
				}
				// 设置标题（岗位组）
				posGroupChooseDialog.setTitle(JecnProperties.getValue("positionGroupSelect"));
				posGroupChooseDialog.setVisible(true);
				// 判断权限是否修改C
				if (posGroupChooseDialog.isOperation()) {
					if (listPosGroup != null && listPosGroup.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : listPosGroup) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						posGroupIds = sbIds.substring(0, sbIds.length() - 1);
						posGroupArea.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						posGroupArea.setText("");
						posGroupIds = "";
					}
				}
			}
		});
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}

		});
		// 取消按钮监听事件
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButAction();
			}

		});
	}

	public void okButtonAction() {
		String name = roleNameField.getText().trim();

		// 验证名称是否正确
		if (!JecnValidateCommon.validateNameNoRestrict(name, roleNamePrompt)) {
			return;
		}
		try {
			if (validateNodeRepeat(name)) {
				roleNamePrompt.setText(JecnProperties.getValue("nameHaved"));
				return;
			} else {
				roleNamePrompt.setText("");
			}
		} catch (Exception e) {
			log.error("RoleDialog okButtonAciton is error！", e);
			// 提示框
			return;
		}
		String content = this.roleRemarkArea.getText();
		if (!JecnValidateCommon.validateContent(content, roleRemarkPrompt)) {
			return;
		}
		this.roleName = name;
		this.roleContent = content;
		saveData();
	}

	public abstract String getDialogTitle();

	/**
	 * @author yxw 2012-5-8
	 * @description:验证是否重名
	 * @return
	 */
	public abstract boolean validateNodeRepeat(String name) throws Exception;

	/**
	 * @author yxw 2012-5-8
	 * @description:保存数据,并显示
	 */
	public abstract void saveData();

	/**
	 * 取消事件
	 */
	public void cancelButAction() {
		this.dispose();
	}

	// JTextArea
	class JecnTextArea extends JTextArea {
		public JecnTextArea(boolean isEdit) {
			initComponents();
			this.setEditable(isEdit);
		}

		private void initComponents() {
			// 设置TextArea大小
			// dimension = new Dimension(350, 55);
			// this.setPreferredSize(dimension);
			// this.setMinimumSize(dimension);
			// this.setMaximumSize(dimension);
			this.setLineWrap(true);
			this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		}
	}

	// 0是流程、1是文件、2是标准、3是制度、4是风险、5组织 7是岗位组 8 是术语
	protected Map<Integer, String> getRelateIdMap() {
		Map<Integer, String> mapIds = new HashMap<Integer, String>();
		mapIds.put(0, getFlowIds());
		mapIds.put(1, getFileIds());
		mapIds.put(2, getStandardIds());
		mapIds.put(3, getRuleIds());
		mapIds.put(4, getRiskIds());
		mapIds.put(5, getOrgIds());
		mapIds.put(7, getPosGroupIds());
		mapIds.put(8, getTermDefineIds());
		return mapIds;
	}

	protected void initEditRoleInfo(JecnTreeBean jecnTreeBean) {
		try {
			jecnRoleInfo = ConnectionPool.getJecnRole().getJecnRoleInfoById(jecnTreeBean.getId());
		} catch (Exception e) {
			log.error("RoleDialog initEditRoleInfo is error！", e);
			return;
		}
		if (jecnRoleInfo == null) {
			return;
		}
		this.setRoleName(jecnRoleInfo.getRoleName());
		this.setRoleContent(jecnRoleInfo.getRoleRemark());

		List<JecnRoleContent> listJecnRoleContent = jecnRoleInfo.getListJecnRoleContent();
		/** 获得流程权限ID的信息 */
		StringBuffer flowIds = new StringBuffer();
		/** 获得流程权限name的信息 */
		StringBuffer flowNames = new StringBuffer();
		/** 获得文件权限ID的信息 */
		StringBuffer fileIds = new StringBuffer();
		/** 获得文件权限Name的信息 */
		StringBuffer fileNames = new StringBuffer();
		/** 获得标准权限ID的信息 */
		StringBuffer criterionIds = new StringBuffer();
		/** 获得标准权限Name的信息 */
		StringBuffer criterionNames = new StringBuffer();
		/** 获得制度权限ID的信息 */
		StringBuffer ruleIds = new StringBuffer();
		/** 获得制度权限Name的信息 */
		StringBuffer ruleNames = new StringBuffer();
		/** 获得风险权限ID的信息C */
		StringBuffer riskIds = new StringBuffer();
		/** 获得风险权限的Name的信息C */
		StringBuffer riskName = new StringBuffer();

		/** 获得术语权限ID的信息C */
		StringBuffer termIds = new StringBuffer();
		/** 获得术语权限的Name的信息C */
		StringBuffer termName = new StringBuffer();

		StringBuffer orgIds = new StringBuffer();
		StringBuffer orgNames = new StringBuffer();

		StringBuffer posGroupIds = new StringBuffer();
		StringBuffer posGroupNames = new StringBuffer();

		for (JecnRoleContent jecnRoleContent : listJecnRoleContent) {
			JecnTreeBean b = new JecnTreeBean();
			int type = jecnRoleContent.getType();
			Long rId = jecnRoleContent.getRelateId();
			String rName = jecnRoleContent.getrName();
			b.setId(rId);
			b.setName(rName);
			switch (type) {
			case 0:
				flowIds.append(rId + ",");
				flowNames.append(rName + "/");
				this.listFlow.add(b);
				break;
			case 1:
				fileIds.append(rId + ",");
				fileNames.append(rName + "/");
				this.listFile.add(b);
				break;
			case 2:
				criterionIds.append(rId + ",");
				criterionNames.append(rName + "/");
				this.listStandard.add(b);
				break;
			case 3:
				ruleIds.append(rId + ",");
				ruleNames.append(rName + "/");
				this.listRule.add(b);
				break;
			case 4: // 风险权限
				riskIds.append(rId + ",");
				riskName.append(rName + "/");
				this.listRisk.add(b);
				break;
			case 5: // 组织权限
				orgIds.append(rId + ",");
				orgNames.append(rName + "/");
				this.listOrg.add(b);
				break;
			case 7: // 岗位组权限
				posGroupIds.append(rId + ",");
				posGroupNames.append(rName + "/");
				this.listPosGroup.add(b);
				break;
			case 8: // 术语权限
				termIds.append(rId + ",");
				termName.append(rName + "/");
				this.listTermDefine.add(b);
				break;
			default:
				break;
			}
		}
		/** 流程权限 */
		String ids = flowIds.toString().trim();
		String names = flowNames.toString().trim();
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {
			this.setFlowIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setFlowPerNames(names.substring(0, names.length() - 1));
			}
		}
		/** 文件权限 */
		ids = fileIds.toString().trim();
		names = fileNames.toString().trim();
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {
			this.setFileIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setFilePerNames(names.substring(0, names.length() - 1));
			}
		}
		/** 标准权限 */
		ids = criterionIds.toString().trim();
		names = criterionNames.toString().trim();
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {
			this.setStandardIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setStandardPerNames(names.substring(0, names.length() - 1));
			}
		}
		/** 制度权限 */
		ids = ruleIds.toString().trim();
		names = ruleNames.toString().trim();
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {
			this.setRuleIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setRulePerNames(names.substring(0, names.length() - 1));
			}
		}

		/**
		 * 风险权限C
		 * 
		 * 修改人：chehuanbo 描述：点击风险选择Dialog的确定按钮时给风险textarea赋值 版本：V3.06
		 * */
		ids = riskIds.toString().trim();
		names = riskName.toString().trim();
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {// 判断是否为空
			this.setRiskIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setRiskPerNames(names.substring(0, names.length() - 1));
			}
		}

		/**
		 * 风险权限C
		 * 
		 * 修改人：chehuanbo 描述：点击风险选择Dialog的确定按钮时给风险textarea赋值 版本：V3.06
		 * */
		ids = termIds.toString().trim();
		names = termName.toString().trim();
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {// 判断是否为空
			this.setTermDefineIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setTermPerNames(names.substring(0, names.length() - 1));
			}
		}

		// 岗位组权限
		// setOrgPerName
		ids = posGroupIds.toString().trim();
		names = posGroupNames.toString().trim();
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {// 判断是否为空
			this.setPosGroupIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setPosGroupPerNames(names.substring(0, names.length() - 1));
			}
		}

		// 组织权限
		// setOrgPerName
		ids = orgIds.toString().trim();
		names = orgNames.toString().trim();

		// 流程管理员初始化数据
		editSecondAdminRole(ids, names);
	}

	protected void editSecondAdminRole(String ids, String names) {

	}

	public String getOrgIds() {
		return orgIds;
	}

	public String getPosGroupIds() {
		return posGroupIds;
	}

	public String getTermDefineIds() {
		return termDefineIds;
	}

	public void setTermDefineIds(String termDefineIds) {
		this.termDefineIds = termDefineIds;
	}

	public void setPosGroupIds(String posGroupIds) {
		this.posGroupIds = posGroupIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public String getPersonIds() {
		return personIds;
	}

	public void setPersonIds(String personIds) {
		this.personIds = personIds;
	}

	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}
}
