package com.jecn.epros.server.service.system;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnMessageReaded;

/***
 * 已读消息
 * @author 
 * 2013-02-27
 *
 */
public interface IReadMessageService {
	/***
	 * 获取所有已读消息数据
	 * @param startNum 开始条数
	 * @param limit 每页显示条数
	 * @return
	 * @throws Exception
	 */
	public List<JecnMessageReaded> getAllReadMessage(int startNum,int limit,Long inceptPeopleId) throws Exception;
	/***
	 *  获取已读数据的条数
	 * @return
	 */
	public int getTotalMessageList(Long inceptPeopleId) throws Exception;
	
	/***
	 * 根据消息主键ID 查询消息详细信息
	 * @param messageId  主键ID
	 */
	public JecnMessageReaded searchReadMesgById(Long messageId) throws Exception;
	
	/***
	 * 删除已读消息
	 * @param mesgId  消息主键ID
	 * @throws Exception
	 */
	public void deleteReadMesgs(List<Long> mesgId) throws Exception;
	
	/***
	 * 添加已读消息
	 * @param messageReaded
	 * @throws Exception
	 */
	public void addReadMessage(JecnMessageReaded messageReaded) throws Exception;
	
}
