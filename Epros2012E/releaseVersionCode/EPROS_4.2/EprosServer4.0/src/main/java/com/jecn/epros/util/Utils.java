package com.jecn.epros.util;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.configuration.runner.ConfigLoadRunner;
import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.InventoryTable;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.inventory.FileInventoryRelateBean;
import com.jecn.epros.domain.system.*;
import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Utils {

    public static final Map<String, String> markToDic = new HashMap<String, String>();

    static {
        initMarkToDic();
    }

    /**
     * 将key对应的name en_name改为value对应的
     */
    private static void initMarkToDic() {
        // ruleScurityLevel, // 制度保密级别
        // fileScurityLevel, // 文件保密级别
        markToDic.put(ConfigContents.ConfigItemPartMapMark.ruleScurityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL.toString());
        markToDic.put(ConfigContents.ConfigItemPartMapMark.fileScurityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL.toString());
        markToDic.put(ConfigContents.ConfigItemPartMapMark.ruleTaskSecurityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL
                .toString());
        markToDic.put(ConfigContents.ConfigItemPartMapMark.securityLevel.toString(), DictionaryEnum.JECN_SECURITY_LEVEL.toString());

    }

    /**
     * 将\n转换为<br>
     *
     * @return
     */
    public static String splitToBr(String source) {
        if (StringUtils.isNotBlank(source)) {
            return source.replaceAll("\n", "<br>");
        }
        return source;
    }

    public static String getLastDayOfMonth(Date date, int diffOfDateYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(calendar.get(Calendar.YEAR) + diffOfDateYear, calendar.get(Calendar.MONTH), calendar.getMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59);
        return dateFormat(calendar.getTime(), "yyyy-MM-dd HH:mm:ss");
    }

    public static String getFirstDayOfMonth(Date date, int diffOfDateYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(calendar.get(Calendar.YEAR) + diffOfDateYear, calendar.get(Calendar.MONTH), 1, 0, 0, 1);
        return dateFormat(calendar.getTime(), "yyyy-MM-dd HH:mm:ss");
    }

    public static String dateFormat(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public static boolean strIsBlank(Object obj) {
        if (obj == null) {
            return true;
        }
        return StringUtils.isBlank(obj.toString());
    }

    public static boolean strIsNotBlank(Object obj) {
        return !strIsBlank(obj);
    }

    /**
     * 转换t_path的集合分解为sql语句in中的数据例如List("1-2-","3-4-")转换为(1,2,3,4)
     *
     * @param paths
     * @return
     */
    public static String convertPathCollectToSqlIn(Collection<String> paths) {
        if (paths == null || paths.size() == 0) {
            throw new IllegalArgumentException("convertPathCollectToSqlIn中内容为空");
        }
        Set<String> s = new HashSet<>();
        for (String path : paths) {
            s.addAll(Arrays.asList(path.split("-")));
        }
        return "(" + s.stream().collect(Collectors.joining(",")) + ")";
    }

    public static Attachment byteFileToAttachment(ByteFile byteFile) {
        if (byteFile == null) {
            return new Attachment();
        }
        Attachment attachment = new Attachment();
        attachment.setContent(byteFile.getBytes());
        attachment.setName(byteFile.getFileName());
        return attachment;
    }

    public static InventoryTable.Status getNodeStatusByType(int status) {
        /** 处于什么阶段 0是待建，1是审批，2是发布 */
        if (status == 0) {
            return InventoryTable.Status.notPub;
        } else if (status == 1) {
            return InventoryTable.Status.approve;
        } else if (status == 2) {
            return InventoryTable.Status.pub;
        } else {
            throw new IllegalArgumentException("getTaskStatusByType未知的状态" + status);
        }
    }

    /**
     * the traditional io way
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static byte[] toByteArray(String filename) throws IOException {

        File f = new File(filename);
        if (!f.exists()) {
            throw new FileNotFoundException(filename);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream((int) f.length());
        BufferedInputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(f));
            int buf_size = 1024;
            byte[] buffer = new byte[buf_size];
            int len = 0;
            while (-1 != (len = in.read(buffer, 0, buf_size))) {
                bos.write(buffer, 0, len);
            }
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bos.close();
        }
    }

    public static String getPubByParams(Map<String, Object> params) {
        if (params.containsKey("pub")) {
            return params.get("pub").toString();
        }
        if (params.containsKey("isPub")) {
            boolean isPub = (Boolean) params.get("isPub");
            return isPub ? "" : "_T";
        }
        return "";
    }

    public static ByteFile multipartFileToByteFile(@RequestParam(value = "file", required = true) MultipartFile mfile) {
        ByteFile file = new ByteFile();
        try {
            file.setBytes(mfile.getBytes());
        } catch (IOException e) {
            throw new EprosBussinessException("读取MultipartFile getBytes()方法出错", e);
        }
        file.setFileName(mfile.getOriginalFilename());
        return file;
    }

    public static void flushDictinary(Map<DictionaryEnum, List<Dictionary>> dictionaryEnumListMap) {
        if (dictionaryEnumListMap == null || dictionaryEnumListMap.size() == 0) {
            return;
        }
        ConfigLoadRunner.dictionaryMap.clear();
        ConfigLoadRunner.dictionaryMap.putAll(dictionaryEnumListMap);
    }

    public static String nullToEmpty(String text) {
        if (text == null) {
            return "";
        }
        if (StringUtils.isNotBlank(text.toString())) {
            return text.toString();
        } else {
            return "";
        }
    }

    public static boolean isEmpty(Collection<?> collection) {
        if (collection == null || collection.size() == 0) {
            return true;
        }
        return false;
    }

    public static <T> String concat(Collection<T> list, String determiner, Function<T, String> trans) {
        if (isEmpty(list)) {
            return "";
        }
        String result = list.stream().map(trans).filter(StringUtils::isNotBlank).collect(Collectors.joining(determiner, "", ""));
        return result;
    }

    public static String concat(Collection<?> list, String determiner) {
        return concat(list, determiner, Object::toString);
    }

    public static List<ConfigItemBean> alterName(List<ConfigItemBean> configs) {
        if (configs == null) {
            return configs;
        }
        try {
            final Map<String, String> markTodic = Utils.markToDic;
            Set<String> keys = markTodic.keySet();
            for (ConfigItemBean config : configs) {
                if (keys.contains(config.getMark())) {
                    List<Dictionary> dics = ConfigUtils.getDictionaryList(DictionaryEnum.valueOf(markTodic.get(config.getMark())));
                    if (!Utils.isEmpty(dics)) {
                        Dictionary dictionary = dics.get(0);
                        if (StringUtils.isNotBlank(dictionary.getTransName())) {
                            config.setName(dictionary.getTransName());
                        }
                        if (StringUtils.isNotBlank(dictionary.getEnTransName())) {
                            config.setEnName(dictionary.getEnTransName());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return configs;
    }

    /**
     * 把一个文件转化为字节
     *
     * @param file
     * @return byte[]
     * @throws Exception
     */
    public static byte[] getByte(File file) {
        byte[] bytes = null;
        if (file != null) {
            InputStream is = null;
            try {
                is = new FileInputStream(file);
                int length = (int) file.length();
                if (length > Integer.MAX_VALUE) // 当文件的长度超过了int的最大值
                {
                    return null;
                }
                bytes = new byte[length];
                int offset = 0;
                int numRead = 0;
                while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                    offset += numRead;
                }
                // 如果得到的字节长度和file实际的长度不一致就可能出错了
                if (offset < bytes.length) {
                    return null;
                }
            } catch (Exception e) {
//                log.error("文件转换为字节流异常！", e);
                e.printStackTrace();
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
//                        log.error("关闭输入流异常！", e);
                        e.printStackTrace();
                    }
                }
            }
        }
        return bytes;
    }

}
