package epros.draw.gui.top.toolbar.element;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnColorChooserProcess;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 元素属性设置面板
 * 
 * 主要设置图形元素的UI属性
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementPanel extends JecnPanel implements ActionListener, ItemListener, ChangeListener {

	/** 恢复默认按钮动作命名 */
	protected final String BTN_RESETBTN = "resetBtn";
	/** 确定按钮动作命名 */
	protected final String BTN_OKBTN = "okBtn";
	/** 取消按钮动作命名 */
	protected final String BTN_CANCELTBTN = "cancelBtn";
	/** 字体颜色按钮动作命名 */
	protected final String BTN_FONT_COLOR = "fontColor";
	/** 填充颜色1按钮动作命名 */
	protected final String BTN_FILL_COLOR = "fillColor";
	/** 填充颜色2按钮动作命名 */
	protected final String BTN_CHANGE_COLOR = "changeColor";
	/** 线条颜色按钮动作命名 */
	protected final String BTN_BODY_COLOR = "bodyColor";
	/** 阴影颜色按钮动作命名 */
	protected final String BTN_SHADOW_COLOR = "shadowColor";

	// ****************面板容器****************//
	/** 图形阅览：显示流程元素面板 */
	protected JDesktopPane previewPanel = null;
	/** 元素属性面板 */
	protected JecnPanel eleAttribusPanel = null;
	/** 按钮区域 */
	protected JecnFlowElementButtonsPanel btnPanel = null;
	// ****************面板容器****************//

	// ***********元素属性面板（eleAttribusPanel）下组件***********//
	/** 阴影面板 */
	protected JecnFlowElementShadowPanel shadowMainPanel = null;
	/** 流程元素大小面板 */
	protected JecnFlowElementSizePanel sizeMainPanel = null;

	/** 填充面板 */
	protected JecnFlowElementFillPanel fillMainPanel = null;
	/** 渐变色面板 */
	protected JecnFlowElementChangeColorPanel changeColorMainPanel = null;
	/** 字体、样式面板 */
	protected JecnFlowElemFontStylePanel fontStyleMainPanel = null;

	/** 常用色面板 */
	protected JecnFlowElementOftenColorPanel oftenColorMainPanel = null;
	/** 3D+字体竖排+字体加粗 */
	protected JecnFlowElement3DPanel ele3DPanel = null;
	// ***********元素属性面板（eleAttribusPanel）下组件***********//

	/** 浏览区域（previewPanel）：true：显示面板下面 ；false：显示在面板上面 */
	protected boolean downLayoutFlag = false;

	/** 承载流程元素面板的容器 */
	protected Container window = null;

	/** 最内部面板的第一个组件的间距 */
	protected Insets insetsFirst = new Insets(9, 3, 9, 3);
	/** 标签间距 */
	protected Insets insetsLabel = new Insets(9, 11, 9, 3);
	/** 内容间距 */
	protected Insets insetsContent = new Insets(9, 0, 9, 7);

	/** 阴影、元素大小、填充中内容面板大小 */
	protected Dimension sizeColor = new Dimension(95, 20);
	/** 字体样式中内容面板大小 */
	protected Dimension sizeFontStyleContent = new Dimension(75, 20);
	/** 常用颜色小框的大小 */
	protected Dimension sizeOftenColorPanel = new Dimension(55, 16);

	/** 流程元素大小 */
	protected Dimension sizeFlowElementPanel = new Dimension(650, 520);
	/** 流程元素浏览 */
	protected Dimension sizePreviewPanel = new Dimension(650, 78);
	/** 底部按钮面板大小 */
	protected Dimension sizeBottomBtnPanel = new Dimension(650, 35);

	/** 选中流程元素 */
	protected JecnBaseFlowElementPanel selectedBasePanel = null;

	/**
	 * 
	 * 右键点击流程元素时使用
	 * 
	 * @param window
	 *            Container JecnFlowElementPanel的容器
	 * @param downLayoutFlag
	 *            boolean 浏览区域（previewPanel）：true：显示面板下面 ；false：显示在面板上面
	 */
	public JecnFlowElementPanel(Container window, boolean downLayoutFlag) {
		if (window == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_PANEL.error("JecnFlowElementPanel类构造函数：参数为null。window=" + window);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.window = window;

		this.downLayoutFlag = downLayoutFlag;

		initComponents();
		initLayout();
	}

	/**
	 * 
	 * 
	 * 创建组件以及属性赋值
	 * 
	 */
	private void initComponents() {

		// 图形阅览：显示流程元素面板
		previewPanel = new JDesktopPane();
		// 元素属性面板
		eleAttribusPanel = new JecnPanel();
		// 按钮区域
		btnPanel = new JecnFlowElementButtonsPanel(this);

		// 阴影面板
		shadowMainPanel = new JecnFlowElementShadowPanel(this);
		// 流程元素大小面板
		sizeMainPanel = new JecnFlowElementSizePanel(this);

		// 填充面板
		fillMainPanel = new JecnFlowElementFillPanel(this);
		// 渐变色面板
		changeColorMainPanel = new JecnFlowElementChangeColorPanel(this);
		// 字体、样式面板
		fontStyleMainPanel = new JecnFlowElemFontStylePanel(this);

		// 常用色面板
		oftenColorMainPanel = new JecnFlowElementOftenColorPanel(this);
		// 3D+字体竖排+字体加粗
		ele3DPanel = new JecnFlowElement3DPanel(this);

		// 元素属性面板
		eleAttribusPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				JecnResourceUtil.getJecnResourceUtil().getValue("elementAttribute")));

		// 图形阅览：显示流程元素面板
		// 边框标题
		previewPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("figureView")));
		// 背景
		previewPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 大小
		previewPanel.setLayout(null);
		previewPanel.setSize(sizePreviewPanel);
		previewPanel.setPreferredSize(sizePreviewPanel);
		previewPanel.setMinimumSize(sizePreviewPanel);

		// 事件：流程元素面板大小变化选中浏览图形也跟着变化
		this.addComponentListener(new JecnFlowElementPanelComponentAdapter());
	}

	/**
	 * 
	 * 布局
	 * 
	 */
	private void initLayout() {
		// 布局
		// *********************第一层面板布局 start*********************//
		GridBagConstraints c;

		// 流程元素阅览面板至于顶部
		int previewPanelRow = 0;
		int leftRightPanelRow = 1;
		if (downLayoutFlag) {// 流程元素阅览面板至于底部
			previewPanelRow = 1;
			leftRightPanelRow = 0;
		}

		// 浏览面板
		c = new GridBagConstraints(0, previewPanelRow, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.BOTH, JecnUIUtil.getInsets0(), 0, 0);
		this.add(previewPanel, c);

		// 元素属性面板
		c = new GridBagConstraints(0, leftRightPanelRow, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, JecnUIUtil.getInsets0(), 0, 0);
		this.add(eleAttribusPanel, c);

		// 按钮区域
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(btnPanel, c);

		// *********************第一层面板布局 end*********************//

		// ********************* 元素属性面板 start*********************//

		// *************** 第一行 start***************//
		// 阴影面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		eleAttribusPanel.add(shadowMainPanel, c);
		// 元素属性大小
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		eleAttribusPanel.add(sizeMainPanel, c);
		// *************** 第一行 end***************//

		// *************** 第二、三行 start***************//
		// 填充面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		eleAttribusPanel.add(fillMainPanel, c);
		// 渐变色面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		eleAttribusPanel.add(changeColorMainPanel, c);
		// 字体、样式面板
		c = new GridBagConstraints(1, 1, 1, 2, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		eleAttribusPanel.add(fontStyleMainPanel, c);
		// *************** 第二、三行 end***************//

		// *************** 第四行 start***************//
		// 常用色面板
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		eleAttribusPanel.add(oftenColorMainPanel, c);
		// 3D+字体竖排+字体加粗
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		eleAttribusPanel.add(ele3DPanel, c);
		// *************** 第四行 end***************//
		// ********************* 元素属性面板 end*********************//
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 添加图形到流程元素界面，修改属性
	 * 
	 * @param flowELementObj
	 *            JecnBaseFlowElementPanel 流程元素
	 * 
	 */
	public void addFlowELementObj(JecnBaseFlowElementPanel flowELementObj) {
		if (flowELementObj == null) {
			return;
		}

		// 清除鼠标事件和鼠标拖动事件
		flowELementObj.removeFlowElemPanelEvent();

		// 添加流程元素到浏览区域
		boolean ret = initpreviewPanelData(flowELementObj);
		if (!ret) {
			return;
		}
		// 流程元素属性值
		addFlowElementValue(this.selectedBasePanel.getFlowElementData());

	}

	/**
	 * 
	 * 初始化浏览区域图形数据
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 */
	private boolean initpreviewPanelData(JecnBaseFlowElementPanel mapElemPanel) {
		if (mapElemPanel == null) {// 图形对象是NULL 不添加
			// 选中流程元素
			selectedBasePanel = null;
			return false;
		}

		// 切换元素，阴影特殊处理
		if (selectedBasePanel != null && selectedBasePanel instanceof JecnBaseFigurePanel) {
			JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) selectedBasePanel;
			figurePanel.removeRelatedPanel();
		}

		// 移除子组件
		previewPanel.removeAll();
		// 选中流程元素
		selectedBasePanel = mapElemPanel;
		// 居中
		setSelectedSize();
		// 添加图形画图面板
		previewPanel.add(mapElemPanel);

		// 添加阴影
		if (mapElemPanel instanceof JecnBaseFigurePanel) {
			JecnBaseFigurePanel baseFigurePanel = (JecnBaseFigurePanel) mapElemPanel;
			baseFigurePanel.addShadowPanel();
		}

		previewPanel.repaint();

		return true;
	}

	/**
	 * 
	 * 设置选中流程元素大小
	 * 
	 */
	void setSelectedSize() {
		if (selectedBasePanel == null) {
			return;
		}
		int width = selectedBasePanel.getFlowElementData().getFigureSizeX();
		int height = selectedBasePanel.getFlowElementData().getFigureSizeY();

		setSelectedSize(width, height);
	}

	/**
	 * 
	 * 根据给定宽高设置选中流程元素大小
	 * 
	 * @param width
	 * @param height
	 */
	void setSelectedSize(int width, int height) {
		selectedBasePanel.setBounds((previewPanel.getWidth() - width) / 2, (previewPanel.getHeight() - height) / 2,
				width, height);
	}

	/**
	 * 
	 * 添加属性值
	 * 
	 * @param flowElementData
	 *            JecnAbstractFlowElementData 给定流程元素属性值
	 */
	private void addFlowElementValue(JecnAbstractFlowElementData flowElementData) {
		// 阴影
		this.shadowMainPanel.initData(flowElementData);
		// 元素大小
		this.sizeMainPanel.initData(flowElementData);
		// 填充
		this.fillMainPanel.initData(flowElementData);
		// 渐变色
		this.changeColorMainPanel.initData(flowElementData);
		// 字体、形状
		this.fontStyleMainPanel.initData(flowElementData);
		// 常用颜色
		this.oftenColorMainPanel.initData(flowElementData);
		// 3D+字体竖排+字体加粗
		this.ele3DPanel.initData(flowElementData);
	}

	public void stateChanged(ChangeEvent e) {
		this.sizeMainPanel.stateChanged(e);
		this.previewPanel.repaint();
	}

	public void itemStateChanged(ItemEvent e) {
		// 渐变色
		this.changeColorMainPanel.itemStateChanged(e);
		// 填充色
		this.fillMainPanel.itemStateChanged(e);
		// 字体、形状
		this.fontStyleMainPanel.itemStateChanged(e);
		// 3D+字体竖排+字体加粗
		this.ele3DPanel.itemStateChanged(e);
		// 阴影
		this.shadowMainPanel.itemStateChanged(e);

		this.previewPanel.repaint();
	}

	/**
	 * 
	 * 按钮动作
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		// 颜色按钮处理方法
		actionByColorBtnPanel(e);
		// 按钮面板动作事件:恢复默认、确定、取消
		actionByBtnsPanel(e);
		// 常用颜色按钮处理
		actionByOftenColorBtnPanel(e);

		this.previewPanel.repaint();
	}

	/**
	 * 
	 * 
	 * 常用颜色按钮动作处理
	 * 
	 */
	private void actionByOftenColorBtnPanel(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if ("fillOftenColorLeft".equals(btn.getActionCommand())) {// 填充常用色:布局靠左
				if (this.fillMainPanel.getNoFillButton().isSelected()) {
					return;
				}
				Color color = this.oftenColorMainPanel.getOftenFillColorPanelLeft().getBackground();
				// 填充颜色1
				this.fillMainPanel.getFillColorPanel().setBackground(color);
				// 更新数据层的字体颜色
				selectedBasePanel.getFlowElementData().setFillColor(color);
			} else if ("fillOftenColorCenter".equals(btn.getActionCommand())) {// 填充常用色:布局靠中间
				if (this.fillMainPanel.getNoFillButton().isSelected()) {
					return;
				}
				Color color = this.oftenColorMainPanel.getOftenFillColorPanelConter().getBackground();
				// 填充颜色1
				this.fillMainPanel.getFillColorPanel().setBackground(color);
				// 更新数据层的字体颜色
				selectedBasePanel.getFlowElementData().setFillColor(color);
			} else if ("fillOftenColorRight".equals(btn.getActionCommand())) {// 填充常用色:布局靠右
				if (this.fillMainPanel.getNoFillButton().isSelected()) {
					return;
				}
				Color color = this.oftenColorMainPanel.getOftenFillColorPanelRight().getBackground();
				// 填充颜色1
				this.fillMainPanel.getFillColorPanel().setBackground(color);
				// 更新数据层的字体颜色
				selectedBasePanel.getFlowElementData().setFillColor(color);
			} else if ("lineOftenColorLeft".equals(btn.getActionCommand())) {// 线条常用色:布局靠左
				Color color = this.oftenColorMainPanel.getOftenBodyColorPanelLeft().getBackground();
				// 线条颜色
				this.fontStyleMainPanel.getBodyColorPanel().setBackground(color);
				// 更新数据层的线条颜色
				selectedBasePanel.getFlowElementData().setBodyColor(color);
			} else if ("lineOftenColorCenter".equals(btn.getActionCommand())) {// 线条常用色:布局靠中间
				Color color = this.oftenColorMainPanel.getOftenBodyColorPanelConter().getBackground();
				// 线条颜色
				this.fontStyleMainPanel.getBodyColorPanel().setBackground(color);
				// 更新数据层的线条颜色
				selectedBasePanel.getFlowElementData().setBodyColor(color);
			} else if ("lineOftenColorRight".equals(btn.getActionCommand())) {// 线条常用色:布局靠右
				Color color = this.oftenColorMainPanel.getOftenBodyColorPanelRight().getBackground();
				// 线条颜色
				this.fontStyleMainPanel.getBodyColorPanel().setBackground(color);
				// 更新数据层的线条颜色
				selectedBasePanel.getFlowElementData().setBodyColor(color);
			} else if ("fontOftenColorLeft".equals(btn.getActionCommand())) {// 字体常用色:布局靠左
				Color color = this.oftenColorMainPanel.getOftenFontColorPanelLeft().getBackground();
				// 字体颜色
				this.fontStyleMainPanel.getFontColorPanel().setBackground(color);
				// 更新数据层的线条颜色
				selectedBasePanel.getFlowElementData().setFontColor(color);
			} else if ("fontOftenColorCenter".equals(btn.getActionCommand())) {// 字体常用色:布局靠中间
				Color color = this.oftenColorMainPanel.getOftenFontColorPanelConter().getBackground();
				// 字体颜色
				this.fontStyleMainPanel.getFontColorPanel().setBackground(color);
				// 更新数据层的线条颜色
				selectedBasePanel.getFlowElementData().setFontColor(color);
			} else if ("fontOftenColorRight".equals(btn.getActionCommand())) {// 字体常用色:布局靠右
				Color color = this.oftenColorMainPanel.getOftenFontColorPanelRight().getBackground();
				// 字体颜色
				this.fontStyleMainPanel.getFontColorPanel().setBackground(color);
				// 更新数据层的线条颜色
				selectedBasePanel.getFlowElementData().setFontColor(color);
			}
		}
	}

	/**
	 * 
	 * 
	 * 按钮面板动作事件:恢复默认、确定、取消
	 * 
	 */
	private void actionByBtnsPanel(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (BTN_RESETBTN.equals(btn.getActionCommand())) {// 恢复默认
				if (selectedBasePanel != null) {// 有选中流程元素
					// 恢复成默认属性值
					JecnSystemData.initCurrFlowElementDataBySystemData(this.selectedBasePanel.getFlowElementData());

					this.addFlowElementValue(this.selectedBasePanel.getFlowElementData());
					// 重设大小
					this.setSelectedSize(this.selectedBasePanel.getFlowElementData().getFigureSizeX(),
							this.selectedBasePanel.getFlowElementData().getFigureSizeY());

					if (MapElemType.CommentText == selectedBasePanel.getFlowElementData().getMapElemType()) {// 判断是否为注释框
						// 恢复到默认值
						((CommentText) selectedBasePanel).isFirstAdd();
					}
				}

			} else if (BTN_OKBTN.equals(btn.getActionCommand())) {// 确定
				// 常用颜色保存到本地:true：更新成功或无更新 false：更新失败
				this.oftenColorMainPanel.writeOftenColorToPropertyFile();
				// 常用颜色保存到本地
				okBtnProcess(e);
				if (JecnDrawMainPanel.getMainPanel().getWorkflow() != null) {// 点击元素元素属性应用
					// 更新面板
					JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
				}
				// 关闭对话框
				window.setVisible(false);
				//
			} else if (BTN_CANCELTBTN.equals(btn.getActionCommand())) {// 取消
				window.setVisible(false);
			}
		}
	}

	/**
	 * 
	 * 确认按钮处理时间
	 * 
	 * @param e
	 *            ActionEvent 事件
	 */
	protected void okBtnProcess(ActionEvent e) {
	}

	/**
	 * 
	 * 颜色按钮处理方法
	 * 
	 * @param e
	 *            ActionEvent 按钮事件
	 */
	private void actionByColorBtnPanel(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			// 选择颜色:没有选择颜色时返回NULL
			Color selectedColor = null;
			if (BTN_FONT_COLOR.equals(btn.getActionCommand())) {// 字体颜色
				// 字体颜色面板设置颜色
				selectedColor = processSelectedColor(JecnResourceUtil.getJecnResourceUtil().getValue(BTN_FONT_COLOR),
						this.fontStyleMainPanel.getFontColorPanel());
				if (selectedColor == null) {// 没有选择颜色
					return;
				}

				// 更新数据层的字体颜色
				selectedBasePanel.getFlowElementData().setFontColor(
						this.fontStyleMainPanel.getFontColorPanel().getBackground());
				// 更新字体常用颜色
				this.oftenColorMainPanel.setCurrOftenColor(ToolBarElemType.editfontSelectColor, selectedColor);

			} else if (BTN_FILL_COLOR.equals(btn.getActionCommand())) {// 填充颜色1
				// 设置填充颜色1
				selectedColor = processSelectedColor(JecnResourceUtil.getJecnResourceUtil().getValue("changeColor1"),
						this.fillMainPanel.getFillColorPanel());
				if (selectedColor == null) {// 没有选择颜色
					return;
				}

				// 更新数据层的填充颜色1
				selectedBasePanel.getFlowElementData().setFillColor(
						this.fillMainPanel.getFillColorPanel().getBackground());
				// 更新填充常用颜色
				this.oftenColorMainPanel.setCurrOftenColor(ToolBarElemType.editFillSelectColor, selectedColor);
			} else if (BTN_CHANGE_COLOR.equals(btn.getActionCommand())) {// 填充颜色2
				// 设置填充颜色2
				selectedColor = processSelectedColor(JecnResourceUtil.getJecnResourceUtil().getValue("changeColor2"),
						this.fillMainPanel.getChangeColorPanel());
				if (selectedColor == null) {// 没有选择颜色
					return;
				}

				// 更新数据层的填充颜色2
				selectedBasePanel.getFlowElementData().setChangeColor(
						this.fillMainPanel.getChangeColorPanel().getBackground());
				// 更新字体常用颜色
				this.oftenColorMainPanel.setCurrOftenColor(ToolBarElemType.editFillSelectColor, selectedColor);
			} else if (BTN_BODY_COLOR.equals(btn.getActionCommand())) {// 线条颜色
				selectedColor = processSelectedColor(JecnResourceUtil.getJecnResourceUtil().getValue(BTN_BODY_COLOR),
						this.fontStyleMainPanel.getBodyColorPanel());
				if (selectedColor == null) {// 没有选择颜色
					return;
				}

				// 更新数据层的填充颜色1
				selectedBasePanel.getFlowElementData().setBodyColor(
						this.fontStyleMainPanel.getBodyColorPanel().getBackground());
				// 更新线条常用颜色
				this.oftenColorMainPanel.setCurrOftenColor(ToolBarElemType.editLineSelectColor, selectedColor);

			} else if (BTN_SHADOW_COLOR.equals(btn.getActionCommand())) {// 阴影颜色
				selectedColor = processSelectedColor(JecnResourceUtil.getJecnResourceUtil().getValue(BTN_SHADOW_COLOR),
						this.shadowMainPanel.getShadowColorPanel());
				if (selectedColor == null) {// 没有选择颜色
					return;
				}
				// 更新数据层的阴影颜色
				selectedBasePanel.getFlowElementData().setShadowColor(
						this.shadowMainPanel.getShadowColorPanel().getBackground());

			}

		}
	}

	/**
	 * 
	 * 选择颜色
	 * 
	 * @param title
	 *            String 颜色选择器的标题
	 * @param elePanel
	 *            JecnElementButtonPanel 颜色显示面板
	 * @return Color 返回选择颜色，当没有选择颜色时返回NULL
	 */
	private Color processSelectedColor(String title, JecnElementButtonPanel elePanel) {
		// 打开颜色选择器选择颜色
		Color color = JecnColorChooserProcess.showColorChooserDialog(title, elePanel.getBackground());
		if (color != null) {
			elePanel.setBackground(color);
		}
		return color;
	}

	/**
	 * 
	 * 通过给定的KEY获取资源文件中值,此值必须是数字型字符串
	 * 
	 * @param key
	 *            String 资源文件KEY名称
	 */
	public int getIntByResourceKey(String key) {
		String value = JecnResourceUtil.getJecnResourceUtil().getValue(key);
		return (value == null) ? 0 : Integer.valueOf(value);
	}

	public Insets getInsetsFirst() {
		return insetsFirst;
	}

	public Insets getInsetsLabel() {
		return insetsLabel;
	}

	public Insets getInsetsContent() {
		return insetsContent;
	}

	public Dimension getSizeColor() {
		return sizeColor;
	}

	public Dimension getSizeFontStyleContent() {
		return sizeFontStyleContent;
	}

	public Dimension getSizeOftenColorPanel() {
		return sizeOftenColorPanel;
	}

	public Dimension getSizeFlowElementPanel() {
		return sizeFlowElementPanel;
	}

	public Dimension getSizeBottomBtnPanel() {
		return sizeBottomBtnPanel;
	}

	public JecnResourceUtil getResourceManager() {
		return JecnResourceUtil.getJecnResourceUtil();
	}

	public JecnFlowElementShadowPanel getShadowMainPanel() {
		return shadowMainPanel;
	}

	public JecnFlowElementSizePanel getSizeMainPanel() {
		return sizeMainPanel;
	}

	public JecnFlowElementFillPanel getFillMainPanel() {
		return fillMainPanel;
	}

	public JecnFlowElementChangeColorPanel getChangeColorMainPanel() {
		return changeColorMainPanel;
	}

	public JecnFlowElemFontStylePanel getFontStyleMainPanel() {
		return fontStyleMainPanel;
	}

	public JecnFlowElementOftenColorPanel getOftenColorMainPanel() {
		return oftenColorMainPanel;
	}

	public JecnFlowElement3DPanel getEle3DPanel() {
		return ele3DPanel;
	}

	public JecnBaseFlowElementPanel getSelectedBasePanel() {
		return selectedBasePanel;
	}

	public String getBTN_RESETBTN() {
		return BTN_RESETBTN;
	}

	public String getBTN_OKBTN() {
		return BTN_OKBTN;
	}

	public String getBTN_CANCELTBTN() {
		return BTN_CANCELTBTN;
	}

	public String getBTN_FONT_COLOR() {
		return BTN_FONT_COLOR;
	}

	public String getBTN_FILL_COLOR() {
		return BTN_FILL_COLOR;
	}

	public String getBTN_CHANGE_COLOR() {
		return BTN_CHANGE_COLOR;
	}

	public String getBTN_BODY_COLOR() {
		return BTN_BODY_COLOR;
	}

	public String getBTN_SHADOW_COLOR() {
		return BTN_SHADOW_COLOR;
	}

	public JDesktopPane getPreviewPanel() {
		return previewPanel;
	}
}
