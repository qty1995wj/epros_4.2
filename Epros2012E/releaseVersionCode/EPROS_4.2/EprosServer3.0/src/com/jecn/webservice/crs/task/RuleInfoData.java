package com.jecn.webservice.crs.task;

public class RuleInfoData {
	private String id;
	/** 制度名称 */
	private String ruleName;
	private String ruleNum;
	private String pubTime;
	/** 拟稿人 */
	private String drafter;
	/** 发布部门 */
	private String pubOrg;
	/** 制度访问地址 */
	private String url;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleNum() {
		return ruleNum;
	}

	public void setRuleNum(String ruleNum) {
		this.ruleNum = ruleNum;
	}

	public String getPubTime() {
		return pubTime;
	}

	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}

	public String getDrafter() {
		return drafter;
	}

	public void setDrafter(String drafter) {
		this.drafter = drafter;
	}

	public String getPubOrg() {
		return pubOrg;
	}

	public void setPubOrg(String pubOrg) {
		this.pubOrg = pubOrg;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
