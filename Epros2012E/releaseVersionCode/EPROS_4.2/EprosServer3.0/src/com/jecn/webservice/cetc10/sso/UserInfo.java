/**
 * UserInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cetc10.sso;

public class UserInfo  implements java.io.Serializable {
    private java.lang.String empID;

    private java.lang.String empName;

    private java.lang.String empSex;

    private java.lang.String deptID;

    private java.lang.String deptShort;

    private java.lang.String userType;

    private java.lang.String loginID;

    public UserInfo() {
    }

    public UserInfo(
           java.lang.String empID,
           java.lang.String empName,
           java.lang.String empSex,
           java.lang.String deptID,
           java.lang.String deptShort,
           java.lang.String userType,
           java.lang.String loginID) {
           this.empID = empID;
           this.empName = empName;
           this.empSex = empSex;
           this.deptID = deptID;
           this.deptShort = deptShort;
           this.userType = userType;
           this.loginID = loginID;
    }


    /**
     * Gets the empID value for this UserInfo.
     * 
     * @return empID
     */
    public java.lang.String getEmpID() {
        return empID;
    }


    /**
     * Sets the empID value for this UserInfo.
     * 
     * @param empID
     */
    public void setEmpID(java.lang.String empID) {
        this.empID = empID;
    }


    /**
     * Gets the empName value for this UserInfo.
     * 
     * @return empName
     */
    public java.lang.String getEmpName() {
        return empName;
    }


    /**
     * Sets the empName value for this UserInfo.
     * 
     * @param empName
     */
    public void setEmpName(java.lang.String empName) {
        this.empName = empName;
    }


    /**
     * Gets the empSex value for this UserInfo.
     * 
     * @return empSex
     */
    public java.lang.String getEmpSex() {
        return empSex;
    }


    /**
     * Sets the empSex value for this UserInfo.
     * 
     * @param empSex
     */
    public void setEmpSex(java.lang.String empSex) {
        this.empSex = empSex;
    }


    /**
     * Gets the deptID value for this UserInfo.
     * 
     * @return deptID
     */
    public java.lang.String getDeptID() {
        return deptID;
    }


    /**
     * Sets the deptID value for this UserInfo.
     * 
     * @param deptID
     */
    public void setDeptID(java.lang.String deptID) {
        this.deptID = deptID;
    }


    /**
     * Gets the deptShort value for this UserInfo.
     * 
     * @return deptShort
     */
    public java.lang.String getDeptShort() {
        return deptShort;
    }


    /**
     * Sets the deptShort value for this UserInfo.
     * 
     * @param deptShort
     */
    public void setDeptShort(java.lang.String deptShort) {
        this.deptShort = deptShort;
    }


    /**
     * Gets the userType value for this UserInfo.
     * 
     * @return userType
     */
    public java.lang.String getUserType() {
        return userType;
    }


    /**
     * Sets the userType value for this UserInfo.
     * 
     * @param userType
     */
    public void setUserType(java.lang.String userType) {
        this.userType = userType;
    }


    /**
     * Gets the loginID value for this UserInfo.
     * 
     * @return loginID
     */
    public java.lang.String getLoginID() {
        return loginID;
    }


    /**
     * Sets the loginID value for this UserInfo.
     * 
     * @param loginID
     */
    public void setLoginID(java.lang.String loginID) {
        this.loginID = loginID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UserInfo)) return false;
        UserInfo other = (UserInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.empID==null && other.getEmpID()==null) || 
             (this.empID!=null &&
              this.empID.equals(other.getEmpID()))) &&
            ((this.empName==null && other.getEmpName()==null) || 
             (this.empName!=null &&
              this.empName.equals(other.getEmpName()))) &&
            ((this.empSex==null && other.getEmpSex()==null) || 
             (this.empSex!=null &&
              this.empSex.equals(other.getEmpSex()))) &&
            ((this.deptID==null && other.getDeptID()==null) || 
             (this.deptID!=null &&
              this.deptID.equals(other.getDeptID()))) &&
            ((this.deptShort==null && other.getDeptShort()==null) || 
             (this.deptShort!=null &&
              this.deptShort.equals(other.getDeptShort()))) &&
            ((this.userType==null && other.getUserType()==null) || 
             (this.userType!=null &&
              this.userType.equals(other.getUserType()))) &&
            ((this.loginID==null && other.getLoginID()==null) || 
             (this.loginID!=null &&
              this.loginID.equals(other.getLoginID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEmpID() != null) {
            _hashCode += getEmpID().hashCode();
        }
        if (getEmpName() != null) {
            _hashCode += getEmpName().hashCode();
        }
        if (getEmpSex() != null) {
            _hashCode += getEmpSex().hashCode();
        }
        if (getDeptID() != null) {
            _hashCode += getDeptID().hashCode();
        }
        if (getDeptShort() != null) {
            _hashCode += getDeptShort().hashCode();
        }
        if (getUserType() != null) {
            _hashCode += getUserType().hashCode();
        }
        if (getLoginID() != null) {
            _hashCode += getLoginID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UserInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "UserInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "empID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "empName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empSex");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "empSex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deptID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "deptID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deptShort");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "deptShort"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "userType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.cdmtc.com/ADSSOWebService", "loginID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
