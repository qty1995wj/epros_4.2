package epros.designer.gui.recycle;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 文件回收站
 * 
 * @author Administrator
 * @date 2015-03-10
 */
public class OrgRecyclePanel extends RecyclePanel {

	private OrgRecycleTree recycleTree;
	private Logger log = Logger.getLogger(OrgRecyclePanel.class);

	public OrgRecyclePanel() {
		super();
	}

	@Override
	public String getSearchName() {
		return JecnProperties.getValue("orgNameC");
	}

	@Override
	public RecycleTree initTree() {
		recycleTree = new OrgRecycleTree();
		recycleTree.setRecyclePanel(this);
		return recycleTree;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {

		try {
			if (JecnConstants.loginBean.isAdmin()) {
				return ConnectionPool.getOrganizationAction()
						.getDelAuthorityByName(name, JecnConstants.projectId);
			}
		} catch (Exception e) {
			log.error("OrgRecyclePanel searchByName is error", e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	public void delDataAction(List<Long> listIds) {
		try {
			if (JecnConstants.loginBean.isAdmin()) {
				ConnectionPool.getOrganizationAction().trueDelete(listIds,
						JecnConstants.getUserId());
			}
		} catch (Exception e) {
			log.error("OrgRecyclePanel delDataAction is error", e);
		}
	}

	@Override
	public String getTreeBorderName() {

		return JecnProperties.getValue("orgManage");
	}
}
