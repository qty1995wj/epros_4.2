package epros.draw.gui.workflow.util;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnImplData;
import epros.draw.data.figure.JecnModeFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.data.figure.JecnToFromRelatedData;
import epros.draw.data.figure.TermFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.figure.MapLineFigure;
import epros.draw.gui.figure.shape.DateFreeText;
import epros.draw.gui.figure.shape.FlowLevelFigureFour;
import epros.draw.gui.figure.shape.FlowLevelFigureOne;
import epros.draw.gui.figure.shape.FlowLevelFigureThree;
import epros.draw.gui.figure.shape.FlowLevelFigureTwo;
import epros.draw.gui.figure.shape.FreeText;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.figure.shape.MapNameText;
import epros.draw.gui.figure.shape.part.ActiveFigureAR;
import epros.draw.gui.figure.shape.part.ActivityImplFigure;
import epros.draw.gui.figure.shape.part.ActivityOvalSubFlowFigure;
import epros.draw.gui.figure.shape.part.AndFigure;
import epros.draw.gui.figure.shape.part.AndFigureAR;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.shape.part.CustomFigure;
import epros.draw.gui.figure.shape.part.DataImage;
import epros.draw.gui.figure.shape.part.DottedRect;
import epros.draw.gui.figure.shape.part.ERPImage;
import epros.draw.gui.figure.shape.part.EndFigure;
import epros.draw.gui.figure.shape.part.EndFigureAR;
import epros.draw.gui.figure.shape.part.EventFigure;
import epros.draw.gui.figure.shape.part.ExpertRhombusAR;
import epros.draw.gui.figure.shape.part.FileImage;
import epros.draw.gui.figure.shape.part.FlowFigureStart;
import epros.draw.gui.figure.shape.part.FlowFigureStop;
import epros.draw.gui.figure.shape.part.FromFigure;
import epros.draw.gui.figure.shape.part.ITRhombus;
import epros.draw.gui.figure.shape.part.ImplFigure;
import epros.draw.gui.figure.shape.part.KCPFigure;
import epros.draw.gui.figure.shape.part.KCPFigureAR;
import epros.draw.gui.figure.shape.part.KCPFigureComp;
import epros.draw.gui.figure.shape.part.KSFFigure;
import epros.draw.gui.figure.shape.part.KeyPointFigure;
import epros.draw.gui.figure.shape.part.ORFigure;
import epros.draw.gui.figure.shape.part.ORFigureAR;
import epros.draw.gui.figure.shape.part.OvalSubFlowFigure;
import epros.draw.gui.figure.shape.part.PAFigure;
import epros.draw.gui.figure.shape.part.PositionFigure;
import epros.draw.gui.figure.shape.part.Rect;
import epros.draw.gui.figure.shape.part.ReverseArrowhead;
import epros.draw.gui.figure.shape.part.Rhombus;
import epros.draw.gui.figure.shape.part.RightArrowhead;
import epros.draw.gui.figure.shape.part.RiskPointFigure;
import epros.draw.gui.figure.shape.part.RoleFigure;
import epros.draw.gui.figure.shape.part.RoleFigureAR;
import epros.draw.gui.figure.shape.part.RoundRectWithLine;
import epros.draw.gui.figure.shape.part.StageSeparatorPentagon;
import epros.draw.gui.figure.shape.part.StartFigureAR;
import epros.draw.gui.figure.shape.part.TermFigureAR;
import epros.draw.gui.figure.shape.part.ToFigure;
import epros.draw.gui.figure.shape.part.Triangle;
import epros.draw.gui.figure.shape.part.VirtualRoleFigure;
import epros.draw.gui.figure.shape.part.XORFigure;
import epros.draw.gui.figure.shape.part.XORFigureAR;
import epros.draw.gui.figure.shape.total.FunctionField;
import epros.draw.gui.figure.shape.total.InputHandFigure;
import epros.draw.gui.figure.shape.total.IsoscelesTrapezoidFigure;
import epros.draw.gui.figure.shape.total.MapFigureEnd;
import epros.draw.gui.figure.shape.total.MapRhombus;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.figure.shape.total.OneArrowLine;
import epros.draw.gui.figure.shape.total.Oval;
import epros.draw.gui.figure.shape.total.PartitionFigure;
import epros.draw.gui.figure.shape.total.Pentagon;
import epros.draw.gui.figure.shape.total.RightHexagon;
import epros.draw.gui.figure.shape.total.SystemDept;
import epros.draw.gui.figure.shape.total.SystemFigure;
import epros.draw.gui.figure.shape.total.TrialReport;
import epros.draw.gui.figure.shape.total.TwoArrowLine;
import epros.draw.gui.line.shape.CommentLine;
import epros.draw.gui.line.shape.CommonLine;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.ManhattanLine;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

/**
 * 根据MapElemType 类型创建流程元素对象
 * 
 * @author ZHAGNXH
 * @date： 日期：May 31, 2012 时间：2:30:06 PM
 */
public class JecnCreateFlowElement {

	/**
	 * 添加图形专用
	 * 
	 * 
	 * 获取MapElemType 类型制定的流程元素对象
	 * 
	 * @param drawDesktopPane
	 *            画板
	 * @param selectedBtnType
	 *            选中的流程元素类型
	 * @return JecnBaseFlowElementPanel 流程元素对象
	 */
	public static JecnBaseFlowElementPanel getFlowEmelentByType(MapElemType selectedBtnType) {
		// 根据MapElemType 类型创建流程元素对象
		JecnBaseFlowElementPanel flowElement = createFlowElementObj(selectedBtnType);
		if (flowElement == null) {// 图形对象是NULL 不添加
			return null;
		}
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 给图形数据层 层级数赋值
		flowElement.getFlowElementData().setZOrderIndex(drawDesktopPane.getMaxZOrderIndex());
		// 层级加一
		drawDesktopPane.setMaxZOrderIndex(drawDesktopPane.getMaxZOrderIndex() + 2);
		return flowElement;
	}

	/**
	 * 
	 * 创建流程元素对象
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return JecnBasePanel 流程元素面板对象 或NULL
	 */
	public static JecnBaseFlowElementPanel createFlowElementObj(MapElemType mapElemType) {
		if (!DrawCommon.isFlowElementType(mapElemType)) {// 判断给定参数：空、指针或添加符号三种返回false
			return null;
		}
		// 线
		switch (mapElemType) {
		case CommonLine: // 连接线 3
			JecnManhattanLineData commonLineData = new JecnManhattanLineData(mapElemType);
			return new CommonLine(commonLineData);
		case ManhattanLine: // 连接线(带箭头) 4
			JecnManhattanLineData manhattanLineData = new JecnManhattanLineData(mapElemType);
			return new ManhattanLine(manhattanLineData);
		case DividingLine: // 分割线 7
			JecnBaseDivedingLineData dividingLineData = new JecnBaseDivedingLineData(mapElemType);
			return new DividingLine(dividingLineData);
		case HDividingLine: // 横向分隔符
			JecnBaseDivedingLineData hDiviLineData = new JecnBaseDivedingLineData(mapElemType);
			return new HDividingLine(hDiviLineData);
		case VDividingLine: // 纵向分隔符
			JecnBaseDivedingLineData vDiviLineData = new JecnBaseDivedingLineData(mapElemType);
			return new VDividingLine(vDiviLineData);
		case CommentLine:
			JecnBaseDivedingLineData commentLineData = new JecnBaseDivedingLineData(mapElemType);
			return new CommentLine(commentLineData);
		case ParallelLines: // 横分割线 1
			JecnVHLineData hLineData = new JecnVHLineData(mapElemType, LineDirectionEnum.horizontal);
			return new ParallelLines(hLineData);
		case VerticalLine:// 竖分割线 2
			JecnVHLineData vLineData = new JecnVHLineData(mapElemType, LineDirectionEnum.vertical);
			return new VerticalLine(vLineData);
		}

		// 创建图形数据对象
		JecnFigureData figureData = createFigureData(mapElemType);

		// 创建图形对象
		JecnBaseFlowElementPanel baseFlowElementPanel = getFlowFigure(mapElemType, figureData);
		return baseFlowElementPanel;
	}

	/**
	 * 
	 * 创建流程元素数据对象
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return JecnFigureData 流程元素数据对象
	 */
	public static JecnFigureData createFigureData(MapElemType mapElemType) {
		if (!DrawCommon.isFlowElementType(mapElemType)) {
			return null;
		}
		switch (mapElemType) {
		case OneArrowLine:// // 流程图：单向箭头 5
		case TwoArrowLine:// // 流程地图：双向箭头 6
		case Oval:// 流程地图：圆形 9
		case Triangle:// 流程地图 三角形 10
		case FunctionField:// // 矩形功能域 11
		case Pentagon:// // 流程地图：五边形 12
		case RightHexagon:// // 流程地图：箭头六边形 13

		case DottedRect:// 协作框 15
		case FreeText:// 自由文本框 16
		case IconFigure:// // 图标插入框 18
		case InputHandFigure:// 流程地图 手动输入 19
		case SystemFigure:// 流程地图 制度 20
		case ReverseArrowhead:// // 流程图：返入 24
		case RightArrowhead:// // 流程图：返出 25
		case EventFigure: // 事件 27
		case FileImage:// // 流程图：文档 29
		case EndFigure:// // 流程图：结束 33
		case XORFigure:// // 流程图：XOR:多种情况下只能选中一种 37
		case ORFigure:// // 流程图：OR:多种情况下只能选中一种或多种 38
		case FlowFigureStart:// // 流程图：流程开始 39
		case FlowFigureStop:// // 流程图：流程结束 40
		case AndFigure:// // 流程图：AND 41
		case PositionFigure:// // 岗位 43
		case Rect:// 组织 44
		case PartitionFigure:// 阶段分隔符 45
		case PAFigure:// // 流程图：PA:问题区域 34
		case KSFFigure:// KSF:关键成功因素 35
		case KCPFigure:// // 流程图：KCP:关键控制点 36
		case KCPFigureComp: // 关键合规点

			/** ******************新增图形************************* */
		case IsoscelesTrapezoidFigure:// 流程地图：等腰梯形 47
		case DateFreeText:// 流程地图/流程图：最后更改时间文本框 48
		case FlowLevelFigureOne:// 流程地图/流程图等级 一级
		case FlowLevelFigureTwo:// 流程地图/流程图等级 二级
		case FlowLevelFigureThree:// 流程地图/流程图等级 三级
		case FlowLevelFigureFour:// 流程地图/流程图等级 四级
		case MapNameText:// 流程图/流程地图/组织图名称文本框
		case KeyPointFigure:// 关键控制点
		case RiskPointFigure:// 风险点
		case MapRhombus:// 菱形
		case MapLineFigure:// 流程架构，拖动连线，不关联元素时，虚拟图形，不显示
			/** 【石勘院，新增元素 start】 */
		case StartFigureAR:
		case EndFigureAR:
		case KCPFigureAR:
		case XORFigureAR:
		case AndFigureAR:
		case ORFigureAR:
			/** 【石勘院，新增元素 start】 */
		case MapFigureEnd:
		case SystemDept:
		case TrialReport:
		case StageSeparatorPentagon:
		case ReverseMapArrowhead:
		case RightMapArrowhead:
		case ERPImage:
			return new JecnFigureData(mapElemType);
		case TermFigureAR:// 术语
			return new TermFigureData(mapElemType);

		case ModelFigure:// // 流程地图：泳池 14
			return new JecnModeFigureData(mapElemType);

		case RoleFigure:// // 流程图： 角色 22
		case CustomFigure:// 客户 23
		case VirtualRoleFigure: // 虚拟角色 42
		case RoleFigureAR:
			return new JecnRoleData(mapElemType);
		case RoundRectWithLine:// // 流程图：活动 21
		case Rhombus:// // 流程图：决策框26
		case DataImage:// // 流程图：信息系统 30
		case ITRhombus:// IT决策 46
		case ActiveFigureAR:// 勘探院添加AR（aris）活动
		case ExpertRhombusAR:// 专家决策
		case ActivityImplFigure:
		case ActivityOvalSubFlowFigure:
			return new JecnActiveData(mapElemType);
		case CommentText:// // 流程图：注释框 17
			return new JecnCommentLineData(mapElemType);
		case ToFigure:// // 流程图：跳转:转出 31
		case FromFigure:// // 流程图：跳转:转入 32
			return new JecnToFromRelatedData(mapElemType);
		case ImplFigure: // 接口 28
		case OvalSubFlowFigure:// // 流程图：子流程 8
		case InterfaceRightHexagon://接口六边形
			return new JecnImplData(mapElemType);
		default:
			return null;
		}
	}

	/**
	 * 创建图形
	 * 
	 * @param mapElemType
	 *            流程元素类型
	 * @param figureData
	 *            图形数据对象
	 * @return 流程元素面板对象 或NULL
	 */
	public static JecnBaseFlowElementPanel getFlowFigure(MapElemType mapElemType, JecnFigureData figureData) {
		if (MapElemType.FreeText == figureData.getEleShape()) {
			return new FreeText(figureData);
		}
		switch (mapElemType) {
		case OneArrowLine:// // 流程图：单向箭头 5
			return new OneArrowLine(figureData);
		case TwoArrowLine:// // 流程地图：双向箭头 6
			return new TwoArrowLine(figureData);

		case OvalSubFlowFigure:// // 流程图：子流程 8
			return new OvalSubFlowFigure(figureData);
		case Oval:// 流程地图：圆形 9
			return new Oval(figureData);
		case Triangle:// 流程地图 三角形 10
			return new Triangle(figureData);
		case FunctionField:// // 矩形功能域 11
			return new FunctionField(figureData);
		case Pentagon:// // 流程地图：五边形 12
			return new Pentagon(figureData);//
		case RightHexagon:// // 流程地图：箭头六边形 13
		case InterfaceRightHexagon://接口六边形
			return new RightHexagon(figureData);
		case ModelFigure:// // 流程地图：泳池 14
			return new ModelFigure(figureData);
		case DottedRect:// 协作框 15
			return new DottedRect(figureData);
		case FreeText:// 自由文本框 16
			figureData.setEleShape(MapElemType.FreeText);
			return new FreeText(figureData);
		case CommentText:// // 流程图：注释框 17
			return new CommentText(figureData);
		case IconFigure:// // 图标插入框 18
			return new IconFigure(figureData);
		case InputHandFigure:// 流程地图 手动输入 19
			return new InputHandFigure(figureData);
		case SystemFigure:// 流程地图 制度 20
			return new SystemFigure(figureData);
		case RoundRectWithLine:// // 流程图：活动 21
			return new RoundRectWithLine(figureData);
		case RoleFigure:// // 流程图： 角色 22
			return new RoleFigure(figureData);
		case CustomFigure:// 客户 23
			return new CustomFigure(figureData);
		case ReverseArrowhead:// // 流程图：返入 24
		case ReverseMapArrowhead:
			return new ReverseArrowhead(figureData);
		case RightArrowhead:// // 流程图：返出 25
		case RightMapArrowhead:
			return new RightArrowhead(figureData);
		case Rhombus:// // 流程图：决策框26
			return new Rhombus(figureData);
		case EventFigure: // 事件 27
			return new EventFigure(figureData);
		case ImplFigure: // 接口 28
			return new ImplFigure(figureData);
		case FileImage:// // 流程图：文档 29
			return new FileImage(figureData);
		case DataImage:// // 流程图：信息系统 30
			return new DataImage(figureData);
		case ERPImage://ERP
			return new ERPImage(figureData);
		case ToFigure:// // 流程图：跳转:转出 31
			return new ToFigure(figureData);
		case FromFigure:// // 流程图：跳转:转入 32
			return new FromFigure(figureData);
		case EndFigure:// // 流程图：结束 33
			return new EndFigure(figureData);
		case PAFigure:// // 流程图：PA:问题区域 34
			return new PAFigure(figureData);
		case KSFFigure:// KSF:关键成功因素 35
			return new KSFFigure(figureData);
		case KCPFigure:// // 流程图：KCP:关键控制点 36
			return new KCPFigure(figureData);
		case KCPFigureComp: // 关键合规点
			return new KCPFigureComp(figureData);
		case XORFigure:// // 流程图：XOR:多种情况下只能选中一种 37
			return new XORFigure(figureData);
		case ORFigure:// // 流程图：OR:多种情况下只能选中一种或多种 38
			return new ORFigure(figureData);
		case FlowFigureStart:// // 流程图：流程开始 39
			return new FlowFigureStart(figureData);
		case FlowFigureStop:// // 流程图：流程结束 40
			return new FlowFigureStop(figureData);
		case AndFigure:// // 流程图：AND 41
			return new AndFigure(figureData);
		case VirtualRoleFigure: // 虚拟角色 42
			return new VirtualRoleFigure(figureData);
		case PositionFigure:// // 岗位 43
			return new PositionFigure(figureData);
		case Rect:// 组织 44
			return new Rect(figureData);
		case PartitionFigure:// 阶段分隔符 45
			return new PartitionFigure(figureData);
		case ITRhombus:// IT决策 46
			return new ITRhombus(figureData);

			/**
			 * **************************新增图形***********************************
			 * *
			 */
		case IsoscelesTrapezoidFigure:// 等腰梯形 47
			return new IsoscelesTrapezoidFigure(figureData);
		case DateFreeText:// 最后更改时间文本框 48
			return new DateFreeText(figureData);
		case FlowLevelFigureOne:// 流程等级图形 一级49
			return new FlowLevelFigureOne(figureData);
		case FlowLevelFigureTwo:// 流程等级图形 二级50
			return new FlowLevelFigureTwo(figureData);
		case FlowLevelFigureThree:// 流程等级图形 三级51
			return new FlowLevelFigureThree(figureData);
		case FlowLevelFigureFour:// 流程等级图形 四级52
			return new FlowLevelFigureFour(figureData);
		case MapNameText:
			return new MapNameText(figureData);
		case KeyPointFigure:// 关键控制点
			return new KeyPointFigure(figureData);
		case RiskPointFigure:// 风险点
			return new RiskPointFigure(figureData);
		case MapRhombus:// 菱形
			return new MapRhombus(figureData);
		case MapLineFigure:
			return new MapLineFigure(figureData);
			/**** 石勘院新增元素 ****/
		case StartFigureAR:
			return new StartFigureAR(figureData);
		case EndFigureAR:
			return new EndFigureAR(figureData);
		case RoleFigureAR:
			return new RoleFigureAR(figureData);
		case KCPFigureAR:
			return new KCPFigureAR(figureData);
		case XORFigureAR:
			return new XORFigureAR(figureData);
		case AndFigureAR:
			return new AndFigureAR(figureData);
		case ORFigureAR:
			return new ORFigureAR(figureData);
		case TermFigureAR:
			return new TermFigureAR(figureData);
		case ActiveFigureAR:
			return new ActiveFigureAR(figureData);
		case ExpertRhombusAR:
			return new ExpertRhombusAR(figureData);
		case ActivityImplFigure:
			return new ActivityImplFigure(figureData);
		case ActivityOvalSubFlowFigure:
			return new ActivityOvalSubFlowFigure(figureData);
		case MapFigureEnd:
			return new MapFigureEnd(figureData);
		case SystemDept:
			return new SystemDept(figureData);
		case TrialReport:
			return new TrialReport(figureData);
		case StageSeparatorPentagon:
			return new StageSeparatorPentagon(figureData);
		}

		return null;
	}
}
