/*
 * TwoArrowLine.java
 *双向箭头
 * Created on 2008年12月3日, 下午3:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.total;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 双向箭头
 * 
 * @author zhangxh
 * @date： 日期：Mar 23, 2012 时间：3:02:43 PM
 */
public class TwoArrowLine extends JecnBaseFigurePanel {
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int x5;
	private int x6;
	private int x7;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int y5;
	private int y6;
	private int y7;
	private int x8;
	private int y8;
	private int x9;
	private int y9;
	private int x10;
	private int y10;

	public TwoArrowLine(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		if (flowElementData.getCurrentAngle() == 0.0
				|| flowElementData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth() / 5;
			y2 = this.getHeight() / 5;
			x3 = this.getWidth() / 5;
			y3 = this.getHeight() * 2 / 5;
			x4 = this.getWidth() * 4 / 5;
			y4 = this.getHeight() * 2 / 5;
			x5 = this.getWidth() * 4 / 5;
			y5 = this.getHeight() / 5;
			x6 = this.getWidth() - 1;
			y6 = this.getHeight() / 2;
			x7 = this.getWidth() * 4 / 5;
			y7 = this.getHeight() * 4 / 5;
			x8 = this.getWidth() * 4 / 5;
			y8 = this.getHeight() * 3 / 5;
			x9 = this.getWidth() / 5;
			y9 = this.getHeight() * 3 / 5;
			x10 = this.getWidth() / 5;
			y10 = this.getHeight() * 4 / 5;
		} else if (flowElementData.getCurrentAngle() == 45.0
				|| flowElementData.getCurrentAngle() == 225.0) {
			x1 = this.getWidth() * 1 / 10;
			y1 = this.getHeight() * 1 / 10;
			x2 = this.getWidth() * 9 / 20;
			y2 = this.getHeight() * 1 / 20;
			x3 = this.getWidth() * 16 / 50;
			y3 = this.getHeight() * 9 / 50;
			x4 = this.getWidth() * 41 / 50;
			y4 = this.getHeight() * 34 / 50;
			x5 = this.getWidth() * 19 / 20;
			y5 = this.getHeight() * 11 / 20;
			x6 = this.getWidth() * 9 / 10;
			y6 = this.getHeight() * 9 / 10;
			x7 = this.getWidth() * 11 / 20;
			y7 = this.getHeight() * 19 / 20;
			x8 = this.getWidth() * 34 / 50;
			y8 = this.getHeight() * 41 / 50;
			x9 = this.getWidth() * 8 / 50;
			y9 = this.getHeight() * 16 / 50;
			x10 = this.getWidth() * 1 / 20;
			y10 = this.getHeight() * 9 / 20;

		} else if (flowElementData.getCurrentAngle() == 90.0
				|| flowElementData.getCurrentAngle() == 270.0) {
			x1 = this.getWidth() / 2;
			y1 = 0;
			x2 = this.getWidth() * 4 / 5;
			y2 = this.getHeight() / 5;
			x3 = this.getWidth() * 3 / 5;
			y3 = this.getHeight() / 5;
			x4 = this.getWidth() * 3 / 5;
			y4 = this.getHeight() * 4 / 5;
			x5 = this.getWidth() * 4 / 5;
			y5 = this.getHeight() * 4 / 5;
			x6 = this.getWidth() / 2;
			y6 = this.getHeight() - 1;
			x7 = this.getWidth() / 5;
			y7 = this.getHeight() * 4 / 5;
			x8 = this.getWidth() * 2 / 5;
			y8 = this.getHeight() * 4 / 5;
			x9 = this.getWidth() * 2 / 5;
			y9 = this.getHeight() * 1 / 5;
			x10 = this.getWidth() / 5;
			y10 = this.getHeight() / 5;

		} else if (flowElementData.getCurrentAngle() == 135.0
				|| flowElementData.getCurrentAngle() == 315.0) {
			x1 = this.getWidth() * 9 / 10;
			y1 = this.getHeight() / 10;
			x2 = this.getWidth() * 19 / 20;
			y2 = this.getHeight() * 9 / 20;
			x3 = this.getWidth() * 41 / 50;
			y3 = this.getHeight() * 16 / 50;
			x4 = this.getWidth() * 16 / 50;
			y4 = this.getHeight() * 41 / 50;
			x5 = this.getWidth() * 9 / 20;
			y5 = this.getHeight() * 19 / 20;
			x6 = this.getWidth() / 10;
			y6 = this.getHeight() * 9 / 10;
			x7 = this.getWidth() * 1 / 20;
			y7 = this.getHeight() * 11 / 20;
			x8 = this.getWidth() * 9 / 50;
			y8 = this.getHeight() * 34 / 50;
			x9 = this.getWidth() * 34 / 50;
			y9 = this.getHeight() * 9 / 50;
			x10 = this.getWidth() * 11 / 20;
			y10 = this.getHeight() * 1 / 20;

		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 },
				new int[] { y1, y2, y3, y4, y5, y6, y7, y8, y9, y10 }, 10);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintArrowLineTop(g2d, 0);
	}

	/**
	 * 双向箭头阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		// 阴影
		g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount,
				x3 + shadowCount, x4 + shadowCount, x5 + shadowCount, x6,
				x7 + shadowCount, x8 + shadowCount, x9 + shadowCount,
				x10 + shadowCount }, new int[] { y1 + shadowCount,
				y2 + shadowCount, y3 + shadowCount, y4 + shadowCount,
				y5 + shadowCount, y6 + shadowCount, y7, y8 + shadowCount,
				y9 + shadowCount, y10 }, 10);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		paintArrowLineTop(g2d, shadowCount);
	}

	/**
	 * 无阴影无3D 普通效果 顶层
	 * 
	 */
	private void paintArrowLineTop(Graphics2D g2d, int shadowCount) {
		g2d.fillPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount,
				x4 - shadowCount, x5 - shadowCount, x6 - shadowCount,
				x7 - shadowCount, x8 - shadowCount, x9 - shadowCount,
				x10 - shadowCount }, new int[] { y1 - shadowCount, y2,
				y3 - shadowCount, y4 - shadowCount, y5, y6 - shadowCount,
				y7 - shadowCount, y8 - shadowCount, y9 - shadowCount,
				y10 - shadowCount }, 10);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount,
				x4 - shadowCount, x5 - shadowCount, x6 - shadowCount,
				x7 - shadowCount, x8 - shadowCount, x9 - shadowCount,
				x10 - shadowCount }, new int[] { y1 - shadowCount, y2,
				y3 - shadowCount, y4 - shadowCount, y5, y6 - shadowCount,
				y7 - shadowCount, y8 - shadowCount, y9 - shadowCount,
				y10 - shadowCount }, 10);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DTwoLow(g2d, 0);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DTwoLine(g2d, 0);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 * 
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		if (this.getFlowElementData().getCurrentAngle() == 0.0
				|| this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
					x3 - indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
					x6 - indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
					x9 - indent3Dvalue, x10 - indent3Dvalue },
					new int[] { y1, y2 + indent3Dvalue, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue, y6,
							y7 - indent3Dvalue, y8 - indent3Dvalue,
							y9 - indent3Dvalue, y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0
				|| this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.fillPolygon(
					new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
							x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2, x5,
							x6 - indent3Dvalue / 2, x7 + indent3Dvalue,
							x8 + indent3Dvalue, x9 + indent3Dvalue,
							x10 + indent3Dvalue }, new int[] {
							y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue,
							y6 - indent3Dvalue / 2, y7 - indent3Dvalue,
							y8 - indent3Dvalue / 2, y9 - indent3Dvalue / 2,
							y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0
				|| this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue }, new int[] { y1 + indent3Dvalue,
					y2 - indent3Dvalue, y3 - indent3Dvalue,
					y4 + indent3Dvalue / 2, y5 + indent3Dvalue / 2,
					y6 - indent3Dvalue, y7 + indent3Dvalue / 2,
					y8 + indent3Dvalue / 2, y9 - indent3Dvalue,
					y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0
				|| this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6 + indent3Dvalue, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue },
					new int[] { y1 + indent3Dvalue, y2 - indent3Dvalue,
							y3 - indent3Dvalue, y4 - indent3Dvalue,
							y5 - indent3Dvalue, y6 - indent3Dvalue,
							y7 + indent3Dvalue, y8 + indent3Dvalue,
							y9 + indent3Dvalue, y10 + indent3Dvalue }, 10);
		}
	}

	public void paint3DTwoTop(Graphics2D g2d, int indent3Dvalue) {
		indent3Dvalue = indent3Dvalue + shadowCount;
		if (this.getFlowElementData().getCurrentAngle() == 0.0
				|| this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
					x3 - indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
					x6 - indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
					x9 - indent3Dvalue, x10 - indent3Dvalue },
					new int[] { y1, y2 + indent3Dvalue, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue, y6,
							y7 - indent3Dvalue, y8 - indent3Dvalue,
							y9 - indent3Dvalue, y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0
				|| this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.fillPolygon(
					new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
							x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2, x5,
							x6 - indent3Dvalue / 2, x7 + indent3Dvalue,
							x8 + indent3Dvalue, x9 + indent3Dvalue,
							x10 + indent3Dvalue }, new int[] {
							y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue,
							y6 - indent3Dvalue / 2, y7 - indent3Dvalue,
							y8 - indent3Dvalue / 2, y9 - indent3Dvalue / 2,
							y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0
				|| this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue }, new int[] { y1 + indent3Dvalue,
					y2 - indent3Dvalue, y3 - indent3Dvalue,
					y4 + indent3Dvalue / 2, y5 + indent3Dvalue / 2,
					y6 - indent3Dvalue, y7 + indent3Dvalue / 2,
					y8 + indent3Dvalue / 2, y9 - indent3Dvalue,
					y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0
				|| this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6 + indent3Dvalue, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue },
					new int[] { y1 + indent3Dvalue, y2 - indent3Dvalue,
							y3 - indent3Dvalue, y4 - indent3Dvalue,
							y5 - indent3Dvalue, y6 - indent3Dvalue,
							y7 + indent3Dvalue, y8 + indent3Dvalue,
							y9 + indent3Dvalue, y10 + indent3Dvalue }, 10);
		}
	}

	public void paint3DTwoLow(Graphics2D g2d, int indent3Dvalue) {
		if (this.getFlowElementData().getCurrentAngle() == 0.0
				|| this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
					x3 - indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
					x6 - indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
					x9 - indent3Dvalue, x10 - indent3Dvalue },
					new int[] { y1, y2 + indent3Dvalue, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue, y6,
							y7 - indent3Dvalue, y8 - indent3Dvalue,
							y9 - indent3Dvalue, y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0
				|| this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.fillPolygon(
					new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
							x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2, x5,
							x6 - indent3Dvalue / 2, x7 + indent3Dvalue,
							x8 + indent3Dvalue, x9 + indent3Dvalue,
							x10 + indent3Dvalue }, new int[] {
							y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue,
							y6 - indent3Dvalue / 2, y7 - indent3Dvalue,
							y8 - indent3Dvalue / 2, y9 - indent3Dvalue / 2,
							y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0
				|| this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue }, new int[] { y1 + indent3Dvalue,
					y2 - indent3Dvalue, y3 - indent3Dvalue,
					y4 + indent3Dvalue / 2, y5 + indent3Dvalue / 2,
					y6 - indent3Dvalue, y7 + indent3Dvalue / 2,
					y8 + indent3Dvalue / 2, y9 - indent3Dvalue,
					y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0
				|| this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6 + indent3Dvalue, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue },
					new int[] { y1 + indent3Dvalue, y2 - indent3Dvalue,
							y3 - indent3Dvalue, y4 - indent3Dvalue,
							y5 - indent3Dvalue, y6 - indent3Dvalue,
							y7 + indent3Dvalue, y8 + indent3Dvalue,
							y9 + indent3Dvalue, y10 + indent3Dvalue }, 10);
		}

		g2d.setStroke(flowElementData.getBasicStroke());
	}

	public void paint3DTwoLine(Graphics2D g2d, int indent3Dvalue) {
		// if (this.getFlowElementData().getCurrentAngle() == 0.0
		// || this.getFlowElementData().getCurrentAngle() == 180.0) {
		// g2d.drawPolygon(new int[] { x1, x2 + indent3Dvalue,
		// x3 + indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
		// x6 + indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
		// x9 + indent3Dvalue, x10 + indent3Dvalue }, new int[] {
		// y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
		// y4 + indent3Dvalue, y5, y6 + indent3Dvalue,
		// y7 + indent3Dvalue, y8 + indent3Dvalue, y9 + indent3Dvalue,
		// y10 + indent3Dvalue }, 10);
		// } else if (this.getFlowElementData().getCurrentAngle() == 45.0
		// || this.getFlowElementData().getCurrentAngle() == 225.0) {
		// g2d.drawPolygon(new int[] { x1, x2 + indent3Dvalue,
		// x3 + indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
		// x6 + indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
		// x9 + indent3Dvalue, x10 + indent3Dvalue }, new int[] {
		// y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
		// y4 + indent3Dvalue, y5, y6 + indent3Dvalue,
		// y7 + indent3Dvalue, y8 + indent3Dvalue, y9 + indent3Dvalue,
		// y10 + indent3Dvalue }, 10);
		// } else if (this.getFlowElementData().getCurrentAngle() == 90.0
		// || this.getFlowElementData().getCurrentAngle() == 270.0) {
		// g2d.drawPolygon(new int[] { x1, x2 + indent3Dvalue,
		// x3 + indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
		// x6 + indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
		// x9 + indent3Dvalue, x10 + indent3Dvalue }, new int[] {
		// y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
		// y4 + indent3Dvalue, y5, y6 + indent3Dvalue,
		// y7 + indent3Dvalue, y8 + indent3Dvalue, y9 + indent3Dvalue,
		// y10 + indent3Dvalue }, 10);
		// } else if (this.getFlowElementData().getCurrentAngle() == 135.0
		// || this.getFlowElementData().getCurrentAngle() == 315.0) {
		// g2d.drawPolygon(new int[] { x1, x2 + indent3Dvalue,
		// x3 + indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
		// x6 + indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
		// x9 + indent3Dvalue, x10 + indent3Dvalue }, new int[] {
		// y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
		// y4 + indent3Dvalue, y5, y6 + indent3Dvalue,
		// y7 + indent3Dvalue, y8 + indent3Dvalue, y9 + indent3Dvalue,
		// y10 + indent3Dvalue }, 10);
		// }
		if (this.getFlowElementData().getCurrentAngle() == 0.0
				|| this.getFlowElementData().getCurrentAngle() == 180.0) {
			g2d.drawPolygon(new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
					x3 - indent3Dvalue, x4 + indent3Dvalue, x5 + indent3Dvalue,
					x6 - indent3Dvalue, x7 + indent3Dvalue, x8 + indent3Dvalue,
					x9 - indent3Dvalue, x10 - indent3Dvalue },
					new int[] { y1, y2 + indent3Dvalue, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue, y6,
							y7 - indent3Dvalue, y8 - indent3Dvalue,
							y9 - indent3Dvalue, y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 45.0
				|| this.getFlowElementData().getCurrentAngle() == 225.0) {
			g2d.drawPolygon(
					new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue,
							x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2, x5,
							x6 - indent3Dvalue / 2, x7 + indent3Dvalue,
							x8 + indent3Dvalue, x9 + indent3Dvalue,
							x10 + indent3Dvalue }, new int[] {
							y1 + indent3Dvalue, y2, y3 + indent3Dvalue,
							y4 + indent3Dvalue, y5 + indent3Dvalue,
							y6 - indent3Dvalue / 2, y7 - indent3Dvalue,
							y8 - indent3Dvalue / 2, y9 - indent3Dvalue / 2,
							y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0
				|| this.getFlowElementData().getCurrentAngle() == 270.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue }, new int[] { y1 + indent3Dvalue,
					y2 - indent3Dvalue, y3 - indent3Dvalue,
					y4 + indent3Dvalue / 2, y5 + indent3Dvalue / 2,
					y6 - indent3Dvalue, y7 + indent3Dvalue / 2,
					y8 + indent3Dvalue / 2, y9 - indent3Dvalue,
					y10 - indent3Dvalue }, 10);
		} else if (this.getFlowElementData().getCurrentAngle() == 135.0
				|| this.getFlowElementData().getCurrentAngle() == 315.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue,
					x3 - indent3Dvalue / 2, x4 - indent3Dvalue / 2,
					x5 - indent3Dvalue, x6 + indent3Dvalue, x7 + indent3Dvalue,
					x8 + indent3Dvalue / 2, x9 + indent3Dvalue / 2,
					x10 + indent3Dvalue },
					new int[] { y1 + indent3Dvalue, y2 - indent3Dvalue,
							y3 - indent3Dvalue, y4 - indent3Dvalue,
							y5 - indent3Dvalue, y6 - indent3Dvalue,
							y7 + indent3Dvalue, y8 + indent3Dvalue,
							y9 + indent3Dvalue, y10 + indent3Dvalue }, 10);
		}
	}

	/**
	 * 既有3D又有阴影顶层填充
	 * 
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		paint3DTwoTop(g2d, linevalue);
	}

	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DTwoLow(g2d, indent3Dvalue);
	}

	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DTwoLine(g2d, indent3Dvalue);
	}
}
