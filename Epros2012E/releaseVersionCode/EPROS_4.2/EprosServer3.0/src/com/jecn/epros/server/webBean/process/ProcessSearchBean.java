package com.jecn.epros.server.webBean.process;
/**
 * @author yxw 2012-12-25
 * @description：流程和流程地图搜索条件bean
 */
public class ProcessSearchBean {
	/**流程名称*/
	private String processName;
	/**流程编号*/
	private String processNum;
	/**流程类别*/
	private long processType=-1;
	/**责任部门*/
	private long orgId;
	/**责任部门名称*/
	private String orgName;
	/**岗位id*/
	private long posId=-1;
	/**岗位名称*/
	private String posName;
	/**查阅部门*/
	private long orgLookId=-1;
	/**部门名称*/
	private String orgLookName;
	/**查阅岗位*/
	private long posLookId=-1;
	/**查阅岗位名称*/
	private String posLookName;
	/**密级*/
	private long secretLevel=-1;
	/**项目ID*/
	private long projectId;
	/**登陆人ID*/
	private Long peopleId;
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getProcessNum() {
		return processNum;
	}
	public void setProcessNum(String processNum) {
		this.processNum = processNum;
	}
	public long getProcessType() {
		return processType;
	}
	public void setProcessType(long processType) {
		this.processType = processType;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getPosId() {
		return posId;
	}
	public void setPosId(long posId) {
		this.posId = posId;
	}
	public String getPosName() {
		return posName;
	}
	public void setPosName(String posName) {
		this.posName = posName;
	}
	public long getOrgLookId() {
		return orgLookId;
	}
	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}
	public String getOrgLookName() {
		return orgLookName;
	}
	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}
	public long getPosLookId() {
		return posLookId;
	}
	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}
	public String getPosLookName() {
		return posLookName;
	}
	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}
	public long getSecretLevel() {
		return secretLevel;
	}
	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	
}
