package epros.designer.gui.rule.mode;

import java.util.Vector;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;

/***
 * 制度模板管理 加入标题
 * 
 * @author zhangjie 2012-05-09
 * 
 */
public class AddRuleModelTitleDialog extends RuleModeTitelDialog {

	private JecnTable titleTable = null;

	public AddRuleModelTitleDialog(JecnTable titleTable) {
		// 标题"加入标题"
		this.setTitle(JecnProperties.getValue("addTitle"));
		this.titleTable = titleTable;
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		Vector<Vector<String>> vs = ((JecnTableModel) this.titleTable
				.getModel()).getDataVector();

		if (vs != null) {
			for (Vector<String> v : vs) {
				if (name.equals(v.get(1))) {
					return true;
				}

			}
		}
		return false;
	}

	@Override
	public boolean validateTitleType(String titleType) {
		Vector<Vector<String>> vs = ((JecnTableModel) this.titleTable
				.getModel()).getDataVector();

		if (vs != null) {
			for (Vector<String> v : vs) {
				if (titleType.equals(v.get(2))) {
					return true;
				}

			}
		}
		return false;
	}

}
