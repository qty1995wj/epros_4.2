package com.jecn.epros.server.action.web.dataImport.sync.xmlImport;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.dom4j.DocumentException;

import com.jecn.epros.server.action.web.timer.TimerAction;
import com.jecn.epros.server.action.web.timer.UserAutoBuss;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.service.dataImport.sync.xmlImport.ImportExcelDataFromRemoteDB;
import com.jecn.epros.server.service.dataImport.sync.xmlImport.XmlContants;
import com.jecn.epros.server.service.dataImport.sync.xmlImport.dbSource.IXmlDataImportService;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.DataBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.XmlConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.XmlStructureBean;

/**
 * XML人员同步Action
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Feb 25, 2013 时间：3:25:17 PM
 */
public class XmlImportAction extends TimerAction {
	private static final long serialVersionUID = 1825503835836539375L;
	/** XML人员岗位匹配 */
	private IXmlDataImportService iXmlDataImport;
	private DataBean dataBean = new DataBean();
	private List<XmlStructureBean> xmlList = new ArrayList<XmlStructureBean>();
	private XmlConfigBean xmlConfigBean;
	private String title;
	private File upload;
	private String uploadContentType; // 这两个属性是必须有的,有框架要用
	private String uploadFileName; // 这两个属性是必须有的,有框架要用
	private String savePath = "./";
	private String filePath;// 试运行报告文件路径
	private String fileName;// 试运行报告文件名称
	/** 添加LOG日志 */
	private static final Logger log = Logger.getLogger(XmlImportAction.class);
	/** 部门ID */
	private Long orgId;
	/** 0不加载人员1加载人员 */
	private int posLoadtype;
	/** 定时器处理 */
	private UserAutoBuss userAutoBuss = null;

	// 读取配置文件
	public String viewParams() {
		return "success";
	}

	// 写入配置文件
	public String saveParams() {
		iXmlDataImport.writeConfigFile(dataBean);
		// iXmlDataImport.importExcel(dataBean.getUserFilePath());
		if ("1".equals(dataBean.getIaAuto())) {// 自动同步
			userAutoBuss.timerStop();
			userAutoBuss.timerStart();

			userAutoBuss.setTimerAction(this);
		} else {
			userAutoBuss.timerStop();
		}
		return SUCCESS;
	}

	public String createXmlStructure() {
		HttpServletResponse response = ServletActionContext.getResponse();
		PrintWriter out = null;
		try {
			out = response.getWriter();
			iXmlDataImport.createXmlStructure(dataBean.getUserFilePath());
			xmlList = iXmlDataImport.getXmlStructure();
		} catch (Exception e) {
			log.error(e);
			if (e instanceof DocumentException) {// 文件读取异常
				out.println("<html>");
				out.println("<head>");
				out
						.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
				out.println("<title></title>");
				out.println("</head>");
				out.println("<script language='javascript'>");
				out.println("alert('用户文件读取失败!');");
				out.println("window.location.href='dataImport_viewParams.do';");
				out.println("window.close();");
				out.println("</script>");
				out.println("</html>");
				return null;
			}
		}
		return "viewParams";
	}

	public String doImport() {
		HttpServletResponse response = ServletActionContext.getResponse();
		PrintWriter out = null;
		FileOutputStream fos;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			log.error(e);
		}
		try {
			if (!this.getUploadFileName().endsWith(".xls")
					&& !this.getUploadFileName().endsWith(".XLS")
					&& !this.getUploadFileName().endsWith(".xlsx")
					&& !this.getUploadFileName().endsWith(".XLSX")) {
				out.println("<html>");
				out.println("<head>");
				out
						.println("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">");
				out.println("<title></title>");
				out.println("</head>");
				out.println("<script language='javascript'>");
				out.println("alert('请选择标准的Excel文件!');");
				out.println("window.location.href='dataImport_viewParams.do';");
				out.println("window.close();");
				out.println("</script>");
				out.println("</html>");
				return null;
			} else {
				fos = new FileOutputStream(savePath + "\\"
						+ "sameNamesExcel.xls");
				FileInputStream fis = new FileInputStream(this.getUpload());
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = fis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
			}

			iXmlDataImport.importExcel();// 初始化数据
			iXmlDataImport.saveOrg(String.valueOf(getProjectId()));// 组织

			iXmlDataImport.excelRead(String.valueOf(getProjectId()));

			if (XmlContants.sameNames.size() > 0) {
				iXmlDataImport.writeExcel(savePath + "\\"
						+ "sameNamesExcel.xls");
				// fos = new FileOutputStream(savePath + "\\" +
				// "sameNamesExcel.xls");
				// FileInputStream fis = new FileInputStream(this.getUpload());
				// byte[] buffer = new byte[1024];
				// int len = 0;
				// while ((len = fis.read(buffer)) > 0) {
				// fos.write(buffer, 0, len);
				// }
				out.println("<html>");
				out.println("<head>");
				out
						.println("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">");
				out.println("<title></title>");
				out.println("</head>");
				out.println("<script language='javascript'>");
				out.println("alert('请选择标准的Excel文件!');");
				out.println("window.location.href='dataImport_viewParams.do';");
				out.println("window.close();");
				out.println("</script>");
				out.println("</html>");
			}
		} catch (Exception ex) {
			log.error("xml岗位匹配同步失败！", ex);
		}
		return "viewParams";
	}

	// 远程组织数据同步
	public String doRemoteImport() {
		try {
			ImportExcelDataFromRemoteDB.importExcelFromDB();// 从远程数据库获得excel

			iXmlDataImport.importExcel();// 初始化数据
			iXmlDataImport.saveOrg(String.valueOf(getProjectId()));// 组织
			iXmlDataImport.excelRead(String.valueOf(getProjectId()));

		} catch (Exception ex) {
			log.error(ex);
		}
		return "viewParams";
	}

	// 人员同步
	public String synchronizationPeoples() {
		try {
			iXmlDataImport.synchronizationPeoples();
		} catch (Exception ex) {
			log.error("", ex);
		}
		return "viewParams";
	}

	public String openFile() throws Exception {
		filePath = "./sameNamesExcel.xls";
		fileName = "sameNamesExcel.xls";
		return "openFile";
	}

	public InputStream getInputStream() throws Exception {
		// 获得文件
		File f1 = new File(filePath);
		FileInputStream fis = new FileInputStream(f1);
		byte[] content = new byte[fis.available()];
		fis.read(content);
		return new ByteArrayInputStream(content);
	}

	// 对于配置中的 ${fileName}, 获得下载保存时的文件名
	public String getFileName() {
		try {
			// 中文文件名也是需要转码为 ISO8859-1,否则乱码
			return new String(fileName.getBytes(), "ISO8859-1");
		} catch (UnsupportedEncodingException e) {
			return "impossible.txt";
		}
	}

	/**
	 * @author yxw 2012-12-1
	 * @description:加载组织和岗位
	 * @return
	 */
	public String getChildsOrgAndPos() {
		List<JecnTreeBean> list;
		try {
			list = iXmlDataImport.getChildOrgsAndPositon(orgId,
					posLoadtype == 0 ? false : true);
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean.setId(jecnTreeBean.getId().toString() + "_"
							+ jecnTreeBean.getTreeNodeType().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setIcon("images/treeNodeImages/"
							+ jecnTreeBean.getTreeNodeType().toString()
							+ ".gif");
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType()
							.toString());
					listWebTreeBean.add(webTreeBean);

				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("", e);
		}

		return null;
	}
	
	//--------岗位组查阅权限开始-----------------------------------
	
	/**
	 * @author yxw 2012-12-1
	 * @description:加载组织和岗位
	 * @return
	 */
	public String getChildsOrgAndPos2() {
		List<JecnTreeBean> list;
		try {
			list = iXmlDataImport.getChildOrgsAndPositon(orgId,
					posLoadtype == 0 ? false : true);
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean.setId(jecnTreeBean.getId().toString() + "_"
							+ jecnTreeBean.getTreeNodeType().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setIcon("images/treeNodeImages/"
							+ jecnTreeBean.getTreeNodeType().toString()
							+ ".gif");
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType()
							.toString());
					listWebTreeBean.add(webTreeBean);

				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("", e);
		}

		return null;
	}
	
	
	
	//--------岗位组查阅权限结束-----------------------------------
	
	
	
	
	

	public DataBean getDataBean() {
		return dataBean;
	}

	public void setDataBean(DataBean dataBean) {
		this.dataBean = dataBean;
	}

	public List<XmlStructureBean> getXmlList() {
		return xmlList;
	}

	public void setXmlList(List<XmlStructureBean> xmlList) {
		this.xmlList = xmlList;
	}

	public XmlConfigBean getXmlConfigBean() {
		return xmlConfigBean;
	}

	public void setXmlConfigBean(XmlConfigBean xmlConfigBean) {
		this.xmlConfigBean = xmlConfigBean;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public IXmlDataImportService getIXmlDataImport() {
		return iXmlDataImport;
	}

	public void setIXmlDataImport(IXmlDataImportService xmlDataImport) {
		iXmlDataImport = xmlDataImport;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public int getPosLoadtype() {
		return posLoadtype;
	}

	public void setPosLoadtype(int posLoadtype) {
		this.posLoadtype = posLoadtype;
	}

	@Override
	public AbstractConfigBean getAbstractConfigBean() {
		return null;
	}

	@Override
	public void synData() throws Exception {
		synchronizationPeoples();
	}
}
