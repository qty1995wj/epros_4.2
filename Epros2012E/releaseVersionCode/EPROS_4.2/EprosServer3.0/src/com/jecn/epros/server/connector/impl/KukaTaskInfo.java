package com.jecn.epros.server.connector.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jecn.Ding.Dingtools;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.task.IJecnAbstractTaskDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;

/**
 * 顾家待办
 * 
 * @author Angus
 * @date 2019年1月20日
 * 
 */
public class KukaTaskInfo extends JecnBaseTaskSend {
	
	private IPersonDao personDao;


	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
			// 创建任务
			createTasks(myTaskBean);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 审批完成，删除待办
			deleteToDoTask(myTaskBean, curPeopleId, this.personDao);
		} else {// 一条from的数据,一条to 的数据
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());
			// 上一个阶段，处理任务，代办待办删除
			deleteToDoTask(myTaskBean, curPeopleId, this.personDao);
			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean);
			}
		}
	}
	
	/**
	 * 创建待办
	 * @param myTaskBean
	 * @throws Exception
	 * 
	 * 钉钉参数
	 * userid	String	必须	manager70	待办事项对应的用户id
		create_time	Long	必须	1496678400000	待办时间。Unix时间戳，毫秒级
		title	String	必须	标题	待办事项的标题
		url	String	必须	https://oa.dingtalk.com	待办事项的跳转链接
		formItemList	List	必须		待办事项表单
		└title	String	必须	表单标题	表单标题
		└content	String	必须	表单内容	表单内容
	 * 
	 */
	private void createTasks(MyTaskBean myTaskBean) throws Exception {
		for (Long handlerPeopleId : myTaskBean.getHandlerPeopleIds()) {
			Map<String, String> map = new HashMap<String, String>();
			log.info("CpersonDao"+personDao);
			JecnUser user = personDao.get(handlerPeopleId);
			
			String phone = user.getPhoneStr();//顾家家居此处作为钉钉userid使用
			JSONObject o = new JSONObject();
			o.put("userid", phone);
			o.put("create_time", new Date().getTime());
			o.put("inittime", myTaskBean.getCreateTime());
			o.put("title", "EPROS任务审批");
			o.put("url", getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
			Map formItemList = new HashMap();
			formItemList.put("title", myTaskBean.getTaskName());
			formItemList.put("content",myTaskBean.getTaskName()+new Date().getTime());
			o.put("formItemList", formItemList);

			JSONObject out = Dingtools.workAdd(o.toJSONString());
			if(out != null){
				log.info("顾家发送任务待办:taskid="+myTaskBean.getTaskId()+"peopleId="+handlerPeopleId+"record_id="+out.getString("record_id"));
			}else{
				log.error("顾家发送任务待办失败:taskid="+myTaskBean.getTaskId()+"peopleId="+handlerPeopleId);
			}
			
		}
	}

	/**
	 * 删除待办
	 * 
	 * @param myTaskBean
	 * @param handlerPeopleId
	 * @throws Exception
	 * 钉钉参数
	 * 1，先获取待办列表
	 *  userid	String	必须	manager70	待办事项对应的用户id
		offset	Number	必须	0	分页游标，从0开始，如返回结果中has_more为true，则表示还有数据，offset再传上一次的offset+limit
		limit	Number	必须	50	分页大小，最多50
		status	Number	必须	1	待办事项状态，0表示未完成，1表示完成
	   2，再找到当前的任务待办更新
	    userid	String	必须	manager70	待办事项对应的用户id
		record_id	String	必须	record123	待办事项唯一id
	 */
	private void deleteToDoTask(MyTaskBean myTaskBean, Long handlerPeopleId, IPersonDao personDap) throws Exception {
		log.info("DpersonDao"+personDao);
		log.info("取消代办peopleId="+handlerPeopleId);
		JecnUser user = personDao.get(handlerPeopleId);
		log.info("取消代办user="+user.getLoginName());
		String phone = user.getPhoneStr();//顾家家居此处作为钉钉userid使用
		log.info("取消代办phone="+phone);
		//先查询当前未完成待办
		JSONObject o = new JSONObject();
		o.put("userid", phone);
		o.put("offset", 0);
		o.put("limit", 50);
		o.put("status", 0);
		JSONObject taskList = Dingtools.workGet(o.toJSONString());
		if(taskList != null){
			/**
			 *  errcode	返回码
				errmsg	对返回码的文本描述内容
				has_more	true和false，其中true表示还有多余的数据
				record_id	待办事项id，可用此id调用更新待办的接口
				create_time	待办事项发起时间
				title	待办标题
				url	待办跳转链接
				forms	待办表单列表
				└title	表单标题
				└content	表单内容
			 */
			JSONArray records = taskList.getJSONObject("records").getJSONArray("list");
			for(Object record : records){
				String record_id = "";
				JSONObject ro = (JSONObject)record; 
				if(ro.getString("url").indexOf(String.valueOf(myTaskBean.getTaskId()))>-1 
						&& ro.getString("title").indexOf("EPROS")>-1){
					//找到当前待办中包含当前任务ID的待办
					record_id = ro.getString("record_id");
				}
				//取消这条待办
				/**
				 * userid	String	必须	manager70	待办事项对应的用户id
				 * record_id	String	必须	record123	待办事项唯一id
				 */
				if("".equals(record_id)){
					//log.error("顾家发送取消待办失败:taskid="+myTaskBean.getTaskId()+"peopleId="+handlerPeopleId+"record_id没查到");
					continue ;
				}
				JSONObject delJson = new JSONObject();
				delJson.put("userid", phone);
				delJson.put("record_id", record_id);
				JSONObject out = Dingtools.workUpdate(delJson.toJSONString());
				if(out != null){
					log.info("顾家发送取消待办:taskid="+myTaskBean.getTaskId()+"peopleId="+handlerPeopleId+"record_id="+record_id);
				}else{
					log.error("顾家发送取消待办失败:taskid="+myTaskBean.getTaskId()+"peopleId="+handlerPeopleId+"record_id="+record_id);
				}
			}
		}else{
			log.error("顾家任务查询失败:userid="+phone);
			return;
		}
//		postQueue(o);
//		String out = this.sendPost(url, o.toJSONString());
//		log.info(out);
	}

	protected String getMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("顾家家居ToDOTasks获取系统配置URL为空!");
		}
//		return basicEprosURL + "loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
//				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
		return basicEprosURL + "ddTask.html?t="+taskId+"&p="+loginId+"&v="+isView;
	}

//	protected String getMibileMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
//		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
//		if (StringUtils.isBlank(basicEprosURL)) {
//			throw new IllegalArgumentException("getCetc10ToDOTasks获取系统配置URL为空!");
//		}
//		return basicEprosURL + "/loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
//				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
//	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
//		JecnUser createUser = personDao.get(beanNew.getCreatePersonId());
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setCreateTime(JecnCommon.getStringbyDateHMS(beanNew.getCreateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew.getTaskName()));
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setRid(beanNew.getRid().toString());
//		myTaskBean.setCreatePeopleLoginName(createUser.getLoginName());
		myTaskBean.setOperationType(beanNew.getTaskElseState());
		myTaskBean.setStageName(beanNew.getStateMark());
		myTaskBean.setCreatePeopleId(beanNew.getCreatePersonId());
		return myTaskBean;
	}

	private String getTaskName(String name) {
		return name;
	}

	/**
	 * 取消任务代办
	 */
	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew beanNew, JecnUser jecnUser, JecnUser createUser) {
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());
		try {
			deleteToDoTask(myTaskBean, jecnUser.getPeopleId(), this.personDao);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());

		// 批量删除待办
		deleteTasks(myTaskBean, cancelPeoples);

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean);
	}

	private void deleteTasks(MyTaskBean myTaskBean, List<Long> handlerPeopleIds) throws Exception {
		for (Long handlerPeopleId : handlerPeopleIds) {
			deleteToDoTask(myTaskBean, handlerPeopleId, this.personDao);
		}
	}

}
