/*
 * RoundRectWithLine.java 活动 Created on 2008年11月6日, 上午10:56
 * 
 * To change this template, choose Tools | Template Manager and open the
 * template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseActiveFigure;

/**
 * 
 * 活动图形类
 * 
 * @date： 日期：Mar 20, 2012 时间：5:00:06 PM
 * @author zhangxh
 */
public class RoundRectWithLine extends JecnBaseActiveFigure {

	/** X坐标 */
	private final int x = 0;
	/** Y坐标 */
	private final int y = 0;

	/**
	 * 初始化类对象
	 * 
	 * @param activeData
	 *            JecnFigureData JecnActiveData对象
	 * 
	 */
	public RoundRectWithLine(JecnFigureData jecnActiveData) {
		super(jecnActiveData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		paintFigure(g);
		Graphics2D g2d = (Graphics2D) g;
		// 设置活动编号位置
		int textHeight = g2d.getFontMetrics(this.getCurScaleFont()).getHeight();
		if (getFlowElementData().getActivityNum() != null) {
			int activityIdLength = g2d.getFontMetrics(this.getCurScaleFont()).stringWidth(
					getFlowElementData().getActivityNum().toString());
			if (flowElementData.getCurrentAngle() == 0.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(), this.getWidth() / 2 - activityIdLength
						/ 2, textHeight);
			}
			if (flowElementData.getCurrentAngle() == 90.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(),
						this.getWidth() - activityIdLength - 4, this.getHeight() / 2 + textHeight / 2 - 1);
			}
			if (flowElementData.getCurrentAngle() == 180.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(), this.getWidth() / 2 + activityIdLength
						/ 2 - 3, this.getHeight() - textHeight / 2);
			}
			if (flowElementData.getCurrentAngle() == 270.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(), activityIdLength / 2 + 3, this
						.getHeight() / 2 + 2);
			}
		}

		initInfoFigure();
	}

	/**
	 * 无填充色
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRoundRect(x, y, userWidth, userHeight, 20, 20);
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawLine(0, this.getHeight() / 3, userWidth, this.getHeight() / 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawLine(0, this.getHeight() * 2 / 3, userWidth, this.getHeight() * 2 / 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawLine(this.getWidth() * 2 / 3, 0, this.getWidth() * 2 / 3, this.getHeight() - 1);
		} else {
			g2d.drawLine(this.getWidth() / 3, 0, this.getWidth() / 3, this.getHeight() - 1);
		}
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 零度或180
		// 阴影
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillRoundRect(x + shadowCount, y + shadowCount, userWidth - shadowCount, userHeight - shadowCount, 20, 20);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintActiveTop(Graphics2D g2d, int shadowCount) {
		// 顶层
		g2d.fillRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, 20, 20);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, 20, 20);
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawLine(0, this.getHeight() / 3, userWidth - shadowCount, this.getHeight() / 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawLine(this.getWidth() * 2 / 3, 0, this.getWidth() * 2 / 3, this.getHeight() - shadowCount);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawLine(0, this.getHeight() * 2 / 3, userWidth - shadowCount, this.getHeight() * 2 / 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawLine(this.getWidth() / 3, 0, this.getWidth() / 3, this.getHeight() - shadowCount);
		}
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintActiveTop(g2d, 0);
	}

	/**
	 * 阴影
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintActiveTop(g2d, shadowCount);
	}

	/**
	 * 3D效果最下层
	 */

	public void paint3DLow(Graphics2D g2d) {
		paint3DLow(g2d, 0);
	}

	/**
	 * 3D效果和阴影最下层
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DLow(g2d, linevalue);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, 20, 20);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, 20, 20);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, 20, 20);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, 20, 20);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DPartLine(g2d, 0);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DPartLine(g2d, indent3Dvalue);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d, int shadowCount) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, 20, 20);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, 20, 20);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, 20, 20);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, 20, 20);
		}
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		paint3DAndShadows(g2d, 0);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		paint3DAndShadows(g2d, shadowCount);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 * @param shadowCount
	 */
	public void paint3DAndShadows(Graphics2D g2d, int shadowCount) {
		g2d.fillRoundRect(x + 4, y + 4, userWidth - 4 - 4 - shadowCount, userHeight - 4 - 4 - shadowCount, 20, 20);
		g2d.setPaint(flowElementData.getBodyColor());
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawLine(4, this.getHeight() / 3 + 4, userWidth - 4 - 4 - shadowCount, this.getHeight() / 3 + 4);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawLine(this.getWidth() * 2 / 3 + 4, 0, this.getWidth() * 2 / 3 + 4, this.getHeight() - 4 - 4
					- shadowCount);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d
					.drawLine(4, this.getHeight() * 2 / 3 - 4, userWidth - 4 - 4 - shadowCount,
							this.getHeight() * 2 / 3 - 4);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawLine(this.getWidth() / 3 - 4, 0, this.getWidth() / 3 - 4, this.getHeight() - 4 - 4 - shadowCount);
		}
	}
}
