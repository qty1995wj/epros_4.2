package com.jecn.epros.server.dao.file;
import com.jecn.epros.server.bean.file.FileDownLoadCountBean;
import com.jecn.epros.server.common.IBaseDao;


public interface IDownLoadCountDao extends IBaseDao<FileDownLoadCountBean,Long>{
   
	  
	
	/**
	 * 根据人员ID,判断当前登录人员是否在下载统计表中存在
	 * @param 
	 *    peopleId:人员ID 
	 *    return： 
	 *        FileDownLoadCountBean（下载统计表bean）
	 * */
	public FileDownLoadCountBean findPeopleExits(Long peopleId);
	/**
	 * 
	 * 清空文件文件下载统计表中的数据
	 * 
	 * */
    public void clearDownLoadCountData();	
}
