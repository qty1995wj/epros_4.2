package epros.designer.table;


public class JecnTableCommon {

	/**
	 * 选中JTable的行
	 * 
	 * @param id
	 * @param jTable
	 */
	public static void getTableSelect(Long id, JecnTable jecnTable) {
		JecnTableModel model = (JecnTableModel) jecnTable.getModel();
		for (int i = 0; i < model.getRowCount(); i++) {
			if ((model.getValueAt(i, 0) != null)
					&& (!"".equals(model.getValueAt(i, 0).toString()))) {
				if (id.toString().equals(model.getValueAt(i, 0).toString())) {
					jecnTable.setRowSelectionInterval(i, i);
					break;
				}
			}
		}
	}

}
