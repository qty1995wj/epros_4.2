package com.jecn.webservice.xinghan.dao.impl;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jecn.epros.server.bean.process.JecnFlowStructureImage;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.webservice.xinghan.dao.IXinghanProcessSynchronous;

@Component
public class XinghanProcessSyncronousImpl extends
		AbsBaseDao<JecnFlowStructureImage, Long> implements
		IXinghanProcessSynchronous {

	Logger log = Logger.getLogger(XinghanProcessSyncronousImpl.class);

	/***************************************************************************
	 * 
	 * 根据流程ID获取流程所属的流程架构以及当前要同步的流程信息
	 * 
	 * @author chehuanbo
	 * 
	 **************************************************************************/
	public List<Object[]> findAllByFlowMapId(Long flowId) {

		String sql = "WITH FLOW AS"
				+ " (SELECT LEVEL              AS LEVEL2,"
				+ "         JFST.FLOW_ID       FLOW_ID,"
				+ "         JFST.PROJECTID     PROJECTID,"
				+ "         JFST.FLOW_NAME     FLOW_NAME,"
				+ "         JFST.FLOW_ID_INPUT FLOW_INPUT"
				+ "    FROM JECN_FLOW_STRUCTURE JFST"
				+ "   START WITH JFST.FLOW_ID = ?"
				+ "  CONNECT BY PRIOR JFST.PRE_FLOW_ID = JFST.FLOW_ID"
				+ "   ORDER BY LEVEL2 DESC)"
				+ " SELECT FL.LEVEL2, FL.FLOW_ID, FL.PROJECTID, FL.FLOW_NAME, FL.FLOW_INPUT"
				+ "  FROM (SELECT F.LEVEL2,"
				+ "               F.FLOW_ID,"
				+ "               F.PROJECTID,"
				+ "               F.FLOW_NAME,"
				+ "               F.FLOW_INPUT,"
				+ "               ROWNUM AS R"
				+ "          FROM FLOW F) FL"
				+ " WHERE R = 2"
				+ " UNION ALL"
				+ " SELECT NUll, FST.FLOW_ID, FST.PROJECTID, FST.FLOW_NAME, FST.FLOW_ID_INPUT"
				+ "  FROM JECN_FLOW_STRUCTURE FST" + " WHERE FST.FLOW_ID = ?";
        if(JecnContants.dbType.equals(DBType.SQLSERVER)){
        	sql= 
        		" WITH FLOW AS" +
        		" (SELECT 1                  AS LEVEL2," + 
        		"         JFST.FLOW_ID       FLOW_ID," + 
        		"         JFST.PROJECTID     PROJECTID," + 
        		"         JFST.FLOW_NAME     FLOW_NAME," + 
        		"         JFST.FLOW_ID_INPUT FLOW_INPUT," + 
        		"         JFST.PRE_FLOW_ID   PRE_FLOW_ID" + 
        		"    FROM JECN_FLOW_STRUCTURE JFST" + 
        		"   WHERE JFST.FLOW_ID = ?" + 
        		"  UNION ALL" + 
        		"  SELECT FL.LEVEL2 + 1," + 
        		"         JEF.FLOW_ID," + 
        		"         JEF.PROJECTID," + 
        		"         JEF.FLOW_NAME," + 
        		"         JEF.FLOW_ID_INPUT," + 
        		"         JEF.PRE_FLOW_ID" + 
        		"    FROM FLOW FL" + 
        		"   INNER JOIN JECN_FLOW_STRUCTURE JEF" + 
        		"      ON JEF.FLOW_ID = FL.PRE_FLOW_ID)" + 
        		" SELECT TOP 1 LEVEL2, FLOW_ID, PROJECTID, FLOW_NAME, FLOW_INPUT, PRE_FLOW_ID" + 
        		"  FROM FLOW" + 
        		" WHERE LEVEL2 IN (SELECT TOP 2 LEVEL2 FROM FLOW ORDER BY LEVEL2 DESC)" + 
        		" UNION ALL" + 
        		" SELECT 1                  AS LEVEL2," + 
        		"       JFST.FLOW_ID       FLOW_ID," + 
        		"       JFST.PROJECTID     PROJECTID," + 
        		"       JFST.FLOW_NAME     FLOW_NAME," + 
        		"       JFST.FLOW_ID_INPUT FLOW_INPUT," + 
        		"       JFST.PRE_FLOW_ID   PRE_FLOW_ID" + 
        		"  FROM JECN_FLOW_STRUCTURE JFST" + 
        		" WHERE JFST.FLOW_ID = ?";
        }
		List<Object[]> objects = null;

		try {
			objects = this.listNativeSql(sql, flowId, flowId);
		} catch (Exception e) {
			log.error("根据流程ID获取流程所属的流程架构以及当前要同步的流程信息出现异常! "
					+ "类：findAllByFlowMapId 方法findAllByFlowMapId 行：63", e);
		}
           
		return objects;
	}
	

	/**
	 * 
	 * 根据流程ID获取所有的流程元素
	 * 
	 * @author chehuanbo
	 * 
	 */
	public List<Object[]> findAllByFlowImageId(Long flowId) {

		String sql = "SELECT" + "   JFSIT.FLOW_ID," + "   JFSIT.FIGURE_ID,"
				+ "   JFSIT.FIGURE_TYPE," + "   JFSIT.FIGURE_TEXT,"
				+ "   JFSIT.START_FIGURE," + "   JFSIT.END_FIGURE,"
				+ "   JFSIT.X_POINT," + "   JFSIT.Y_POINT," + "   JFSIT.WIDTH,"
				+ "   JFSIT.HEIGHT," + " JFSIT.ACTIVITY_ID,"
				+ " JFSIT.CIRCUMGYRATE"
				+ " FROM JECN_FLOW_STRUCTURE_IMAGE JFSIT WHERE JFSIT.FLOW_ID=?"
				+ " AND JFSIT.FIGURE_TYPE IN (" + " 'RoundRectWithLine',"
				+ " 'CommonLine'," + " 'ManhattanLine',"
				+ " 'ReverseArrowhead'," + " 'RightArrowhead'," + " 'Rhombus',"
				+ " 'DottedRect'," + " 'ImplFigure'," + " 'OvalSubFlowFigure',"
				+ " 'DataImage'," + " 'ToFigure'," + " 'EndFigure',"
				+ " 'XORFigure'," + " 'ORFigure'," + " 'FlowFigureStart',"
				+ " 'FlowFigureStop'," + " 'AndFigure'," + " 'ITRhombus',"
				+ " 'EventFigure'" + " )";

		List<Object[]> objects = null;

		try {
			objects = this.listNativeSql(sql, flowId);
		} catch (Exception e) {
			log.error("根据流程ID获取所有的流程元素出现异常! 类：BpsProcessSyncronousImpl 方法findAllByFlowImageId 行：63",
							e);
		}

		return objects;
	}
    
	
	/**
	 * 根据流程元素ID查询线段信息
	 * 
	 * @author chehuanbo
	 * 
	 */
	public List<Object[]> findSegmentId(Long figureId) {

		String sql = "SELECT JFLS.FIGURE_ID, JFLS.START_X, JFLS.START_Y"
				+ "  FROM (SELECT FLS.FIGURE_ID, FLS.START_X, FLS.START_Y"
				+ "          FROM JECN_LINE_SEGMENT FLS"
				+ "         INNER JOIN JECN_FLOW_STRUCTURE_IMAGE JFSI"
				+ "            ON JFSI.FIGURE_ID = FLS.FIGURE_ID"
				+ "        UNION ALL"
				+ "        SELECT FLS.FIGURE_ID, FLS.END_X, FLS.END_Y A"
				+ "          FROM JECN_LINE_SEGMENT FLS"
				+ "         INNER JOIN JECN_FLOW_STRUCTURE_IMAGE JFSI"
				+ "            ON JFSI.FIGURE_ID = FLS.FIGURE_ID) JFLS"
				+ " GROUP BY JFLS.FIGURE_ID, JFLS.START_X, JFLS.START_Y"
				+ " HAVING COUNT(*)>1" + " AND JFLS.FIGURE_ID=?";

		List<Object[]> objects = null;
		try {
			objects = this.listNativeSql(sql, figureId);
		} catch (Exception e) {
			log.error("根据流程元素ID查询线段信息出现异常! 类：BpsProcessSyncronousImpl 方法findSegmentId 行：75",
							e);
		}

		return objects;

	}

	/**
	 * 根据协作框，查找连接当前协作框的连接线
	 * 
	 * @author chehuanbo
	 * 
	 */
	public List<Object[]> findAllByDottedRectId(Long figureId) {

		String sql = "SELECT" + "   JFSIT.FLOW_ID," + "   JFSIT.FIGURE_ID,"
				+ "   JFSIT.FIGURE_TYPE," + "   JFSIT.FIGURE_TEXT,"
				+ "   JFSIT.START_FIGURE," + "   JFSIT.END_FIGURE,"
				+ "   JFSIT.X_POINT," + "   JFSIT.Y_POINT," + "   JFSIT.WIDTH,"
				+ "   JFSIT.HEIGHT" + " FROM JECN_FLOW_STRUCTURE_IMAGE JFSIT "
				+ " WHERE JFSI.END_FIGURE = ? OR JFSI.START_FIGURE = ?";

		List<Object[]> objects = null;

		try {
			objects = this.listHql(sql, figureId, figureId);
		} catch (Exception e) {
			log.error(
							"根据流程ID获取所有的流程元素出现异常! 类：BpsProcessSyncronousImpl 方法findAllByFlowImageId 行：63",
							e);
		}

		return objects;
	}

}
