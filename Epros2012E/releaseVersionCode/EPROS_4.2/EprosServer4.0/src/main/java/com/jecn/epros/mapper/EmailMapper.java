package com.jecn.epros.mapper;

import com.jecn.epros.domain.email.Email;
import com.jecn.epros.domain.email.EmailAttachment;
import com.jecn.epros.domain.email.EmailContent;
import com.jecn.epros.domain.email.EmailRelatedAttachment;
import org.apache.ibatis.annotations.Insert;

public interface EmailMapper {

    /**
     * 保存邮件到数据库
     *
     * @param email
     */
    @Insert("insert into jecn_email(id,create_date,send_date,send_state,content_id,people_id) values(#{id},#{createDate},#{sendDate,jdbcType=DATE},#{sendState},#{contentId},#{peopleId})")
    void saveEmail(Email email);

    /**
     * 保存邮件内容表
     *
     * @param emailContent
     */
    @Insert("insert into JECN_EMAIL_CONTENT(id,subject,content) values(#{id},#{subject},#{content})")
    void saveEmailContent(EmailContent emailContent);

    /**
     * 保存附件表
     *
     * @param related
     */
    @Insert("insert into JECN_EMAIL_ATTACHMENT(id,attachment,attachment_name) values(#{id},#{attachment,jdbcType=BLOB},#{attachmentName})")
    void saveEmailAttachment(EmailAttachment emailAttachment);

    /**
     * 保存邮件附件关联表
     *
     * @param related
     */
    @Insert("insert into JECN_EMAIL_ATTACHMENT(id,attachment_id,email_id) values(#{id},#{attachmentId},#{emailId})")
    void saveEmailRelatedAttachment(EmailRelatedAttachment related);

}
