package epros.designer.gui.process.flowtool;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;

/**
 * 支持工具名称修改
 * 
 * @author fuzhh Apr 9, 2013
 * 
 */
public class UpdateFlowToolDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(UpdateFlowToolDialog.class);
	private JecnTreeBean jecnTreeBean = null;
	private JTree jTree = null;
	private JecnFlowSustainTool jecnFlowSustainTool = null;
	private boolean isOk = false;

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}

	public UpdateFlowToolDialog(JecnTreeBean jecnTreeBean, JecnTree jTree) {
		this.jecnTreeBean = jecnTreeBean;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		try {
			jecnFlowSustainTool = ConnectionPool.getFlowTool().getJecnSustainToolInfoById(jecnTreeBean.getId());
		} catch (Exception e) {
			log.error("UpdateFlowToolDialog UpdateFlowToolDialog is error", e);
		}
		if (jecnFlowSustainTool == null) {
			return;
		}
		this.setName(jecnFlowSustainTool.getFlowSustainToolShow());
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("updateI");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("contentC");
	}

	@Override
	public void saveData() {
		jecnFlowSustainTool.setFlowSustainToolShow(getName());
		try {
			ConnectionPool.getFlowTool().updateSustainTool(jecnFlowSustainTool, JecnUtil.getUserId());
			isOk = true;
			this.dispose();
		} catch (Exception e) {
			log.error("UpdateFlowToolDialog saveData is error", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		JecnTreeNode selectNode = JecnTreeCommon.getTreeNode(jecnTreeBean, jTree);
		if (selectNode != null) {
			return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
		} else {
			try {
				return ConnectionPool.getFlowTool().validateRepeatNameEidt(name, jecnTreeBean.getId(),
						jecnTreeBean.getPid());
			} catch (Exception e) {
				log.error("UpdateFlowToolDialog validateNodeRepeat is error", e);
			}
		}
		return false;
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
