package epros.draw.gui.top.toolbar.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 创建流程地图时弹出的对话框
 * 
 * @author ZHOUX
 * 
 */
public class JecnCreateTotalMapDialog extends JecnDialog implements ActionListener, CaretListener {
	/** 内容面板 */
	private JecnPanel mainPanel = null;
	/** 名称标签 */
	private JLabel label = null;
	/** 流程地图名称 */
	private JTextField nameTextField = null;
	/** 提示信息框 */
	private JTextArea infoTextArea = null;
	/** 确认取消按钮面板 */
	private JecnOKCancelPanel okCancelPanel = null;
	/** 名称内容 */
	private String name = null;

	public JecnCreateTotalMapDialog() {
		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 内容面板
		mainPanel = new JecnPanel();
		// 名称标签
		label = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("totalMapName"));
		// 流程地图名称
		nameTextField = new JTextField();
		// 提示信息框
		infoTextArea = new JTextArea();
		// 确认取消按钮面板
		okCancelPanel = new JecnOKCancelPanel();

		this.getContentPane().add(mainPanel);

		// 大小
		Dimension size = new Dimension(365, 125);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setSize(size);
		// 位置
		this.setLocationRelativeTo(null);
		// 模态
		this.setModal(true);
		// 不可调整大小
		// this.setResizable(false);
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("fileCreateTotalMapTitle"));

		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		// 大小
		Dimension contentSize = new Dimension(175, 20);
		// 流程地图名称
		nameTextField.setPreferredSize(contentSize);
		nameTextField.setMinimumSize(contentSize);

		// 提示信息框
		infoTextArea.setEditable(false);
		infoTextArea.setLineWrap(true);
		infoTextArea.setVisible(false);
		// 背景颜色和前景颜色
		infoTextArea.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoTextArea.setForeground(Color.red);

		// 事件
		nameTextField.addCaretListener(this);
		okCancelPanel.getOkBtn().addActionListener(this);
		okCancelPanel.getCancelBtn().addActionListener(this);
	}

	/**
	 * 
	 * 
	 * 
	 */
	private void initLayout() {
		// 内容面板new Insets(20, 25, 0, 30)
		Insets labelInsets = new Insets(20, 25, 0, 0);
		Insets nameInsets = new Insets(20, 3, 0, 30);
		Insets infoTextAreaInsets = new Insets(0, 30, 0, 10);
		Insets okCancelPanelInsets = new Insets(5, 0, 0, 30);
		// 名称标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, labelInsets, 0, 0);
		mainPanel.add(label, c);
		// 流程地图名称
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				nameInsets, 0, 0);
		mainPanel.add(nameTextField, c);
		// 提示信息框
		c = new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				infoTextAreaInsets, 0, 0);
		mainPanel.add(infoTextArea, c);
		// 空闲区域
		c = new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				infoTextAreaInsets, 0, 0);
		mainPanel.add(new JLabel(), c);
		// 确认取消按钮面板
		c = new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				okCancelPanelInsets, 0, 0);
		mainPanel.add(okCancelPanel, c);

	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	/**
	 * 
	 * 
	 * 按钮动作事件
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	private void actionPerformedProcess(ActionEvent e) {
		if (okCancelPanel.getOkBtn() == e.getSource()) {// 确认按钮
			boolean ret = checkName();
			if (ret) {
				name = nameTextField.getText();
			} else {
				return;
			}
		} else if (okCancelPanel.getCancelBtn() == e.getSource()) {// 取消按钮
			name = null;
		}

		this.setVisible(false);
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	private void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == nameTextField) {// 流程地图名称
			checkName();
		}
	}

	/**
	 * 
	 * 在名称输入框中输入字符时，验证内容是否正确
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	private boolean checkName() {

		String text = nameTextField.getText();

		// 验证数据
		String checkString = JecnUserCheckUtil.checkName(text);

		if (!DrawCommon.isNullOrEmtryTrim(checkString)) {

			// 显示提示框、添加提示框内容
			if (!infoTextArea.isVisible()) {
				infoTextArea.setVisible(true);
			}
			infoTextArea.setText(checkString);

			return false;
		}

		// 隐藏提示框、清除提示框内容
		if (infoTextArea.isVisible()) {
			infoTextArea.setVisible(false);
		}
		infoTextArea.setText("");

		return true;
	}

	public String getName() {
		return name;
	}

}
