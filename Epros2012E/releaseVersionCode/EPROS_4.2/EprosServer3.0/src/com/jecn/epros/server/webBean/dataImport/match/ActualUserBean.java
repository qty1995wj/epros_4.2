package com.jecn.epros.server.webBean.dataImport.match;

/**
 * 实际人员表（与流程匹配的临时表）
 * @author zhangjie
 * 20120214
 */
public class ActualUserBean {
	/**主键ID*/
	private Long id;
	
	/**登录名称(人员编号)*/
	private String loginName;
	
	/**真实姓名*/
	private String trueName;
	
	/**邮箱地址*/
	private String email;
	
	/**联系电话*/
	private String phone;
	
	/**内外网邮件标识*/
	private int emailType;
	
	/**所属 实际岗位编码*/
	private String posNum;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getEmailType() {
		return emailType;
	}

	public void setEmailType(int emailType) {
		this.emailType = emailType;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}
}
