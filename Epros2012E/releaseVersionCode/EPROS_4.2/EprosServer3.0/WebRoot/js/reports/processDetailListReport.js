//流程清单报表


/**
 * 流程访问数统计重置
 **/
function processDetailListReportReset(){
        document.getElementById("orgDutyName_access_id").value = "-1";
		document.getElementById("orgDutyId_access_id").value = "-1";
		document.getElementById("endTime").value = "";
		document.getElementById("select_processMap_id").innerHTML="";
		document.getElementById("select_organization_id").innerHTML="";
}

// 流程访问数统计下载
function processDetailListReportDownloadExcel(){
     //责任部门ID 最多20个拿逗号分隔
	 var orgId=document.getElementById("orgDutyId_access_id").value;
	 //流程地图ID 最多20个拿逗号分隔
	 var processMapId=document.getElementById("processMapId_access_id").value;
	 
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
	 
	 //判断部门id与流程地图id是否为空
	 if(((orgId==-1||orgId=="")&&(processMapId==-1||processMapId=="")))
	{
		 //必须选择责任部门或流程地图！
		 alert(i_mustSelectOrgOrProcessMap);
		 return;
	}
	 
	//结束日期不填
	if(endTime==""){
		alert(i_expiryDateCantNull);
		return;
	}
	
	document.getElementById("endTimeSub").value=endTime;
	
	var processDetailForm=document.getElementById("processDetailForm");
	
	processDetailForm.action="downloadProcessDetailList.action";
	processDetailForm.submit();
	
}

