package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.AddonSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.BaseSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.Cetc10SyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.Cetc29SyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.EnvicoolSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.EpsonSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.FiberhomeSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.HisenseSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.LiBangSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.MengNiuSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.MulinsenSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.ShiHuaSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.UaesSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl.ZZNCSyncDataFilter;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

public class SyncDataFilterFactory {

	public static AbstractSyncDataFilter create(BaseBean baseBean) {
		if (JecnContants.otherLoginType == 8) {
			return new AddonSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 10) {
			return new HisenseSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 14) {
			return new UaesSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 23) {
			return new ZZNCSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 20) {
			return new ShiHuaSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 28) {
			return new LiBangSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 24) {
			return new Cetc10SyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 11) {
			return new Cetc29SyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 19) {
			return new FiberhomeSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 36) {
			return new EnvicoolSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 37) {
			return new MengNiuSyncDataFilter(baseBean);
		} else if ((JecnContants.otherLoginType == 38)) {
			return new EpsonSyncDataFilter(baseBean);
		} else if (JecnContants.otherLoginType == 41) {// 木林森
			return new MulinsenSyncDataFilter(baseBean);
		}
		return new BaseSyncDataFilter(baseBean);
	}
}
