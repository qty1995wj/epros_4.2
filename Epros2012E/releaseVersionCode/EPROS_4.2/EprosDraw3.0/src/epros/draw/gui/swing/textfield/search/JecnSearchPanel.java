package epros.draw.gui.swing.textfield.search;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 输入库+搜索按钮面板组件，用于搜索过滤功能
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSearchPanel extends JPanel {
	/** 搜索标题 */
	private JLabel searchLab = null;

	/** 输入panel */
	private JPanel searchPanel = null;
	/** 输入框 */
	private JTextField textFlied = null;
	/** 时间按钮 */
	private JButton searchButton = null;
	/** 按钮的点击事件 */
	private ActionListener btnActionListener = null;

	private ImageIcon micon = null;
	private ImageIcon selectedIcon = null;

	public JecnSearchPanel() {
		initComponents();
	}

	private void initComponents() {
		// 搜索标题
		searchLab = new JLabel();
		// 输入panel
		searchPanel = new JPanel();
		// 输入框
		textFlied = new JTextField();
		// 时间按钮
		searchButton = new JButton();

		micon = JecnFileUtil
				.getImageIconforPath("/epros/draw/images/swing/textfield/search.png");
		selectedIcon = JecnFileUtil
				.getImageIconforPath("/epros/draw/images/swing/textfield/searchSelected.png");

		searchLab.setOpaque(false);

		searchPanel.setBorder(null);
		searchPanel.setOpaque(false);
		searchPanel.setLayout(new BorderLayout());
		searchPanel.setBorder(JecnUIUtil.getTootBarBorder());
		searchPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		textFlied.setBorder(null);

		// 不显示焦点状态
		searchButton.setFocusPainted(false);
		searchButton.setBorder(null);
		searchButton.setOpaque(false);
		searchButton.setIcon(micon);
		searchButton.setRolloverIcon(selectedIcon);

		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new BorderLayout());

		this.add(searchLab, BorderLayout.WEST);
		this.add(searchPanel, BorderLayout.CENTER);

		searchPanel.add(textFlied, BorderLayout.CENTER);
		searchPanel.add(searchButton, BorderLayout.EAST);
	}

	public void setSearchTitleName(String titleName) {
		if (titleName != null) {
			searchLab.setText(titleName);
		}
	}

	/**
	 * 
	 * 添加按钮点击事件
	 * 
	 * @param lnr
	 *            ActionListener
	 */
	public void addButtonActionListener(ActionListener lnr) {
		if (lnr != null) {
			if (btnActionListener != null) {
				searchButton.removeActionListener(btnActionListener);
			}
			searchButton.addActionListener(lnr);
			btnActionListener = lnr;
		}
	}

	public String getText() {
		return textFlied.getText();
	}

	public void setText(String t) {
		this.textFlied.setText(t);
	}

	public void setEditable(boolean b) {
		this.textFlied.setEditable(b);
	}

	/**
	 * 
	 * 输入框获取焦点
	 * 
	 */
	public void textFiledRequestFocusInWindow() {
		textFlied.requestFocusInWindow();
	}
}
