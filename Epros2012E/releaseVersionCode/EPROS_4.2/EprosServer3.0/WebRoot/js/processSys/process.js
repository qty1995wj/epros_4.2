function processGrid() {

	processGridStore = new Ext.data.Store( {
		proxy : new Ext.data.HttpProxy( {
			url : "searchProcessList.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "flowId",
			mapping : "flowId"
		}, {
			name : "flowName",
			mapping : "flowName"
		}, {
			name : "flowIdInput",
			mapping : "flowIdInput"
		}, {
			name : "resPeopleName",
			mapping : "resPeopleName"
		}, {
			name : "orgName",
			mapping : "orgName"
		}, {
			name : "secretLevel",
			mapping : "isPublic"
		}, {
			name : "pubDate",
			mapping : "pubDate"
		} ])

	});

	processGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	processGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : processGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	processGridPanel = new Ext.grid.GridPanel( {
		id : "processGridPanel_id",
		renderTo : 'processGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 215,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		//columnLines:true,//显示列的分隔线
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		bodyStyle : 'border-left: 0px;',
		frame : false,
		store : processGridStore,
		colModel : processGridColumnModel,
		bbar : processGridBbar
	})

}

function processMapGrid() {

	processMapGridStore = new Ext.data.Store( {

		proxy : new Ext.data.HttpProxy( {
			url : "searchProcessMapList.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "flowId",
			mapping : "flowId"
		}, {
			name : "flowName",
			mapping : "flowName"
		}, {
			name : "pName",
			mapping : "pName"
		}, {
			name : "pid",
			mapping : "pid"
		}, {
			name : "secretLevel",
			mapping : "isPublic"
		}, {
			name : "pubDate",
			mapping : "pubDate"
		} ])

	});

	processMapGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_processMapName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : processMapNameRenderer
			}, {
				header : i_belongProcessMap,
				dataIndex : "pName",
				align : "center",
				menuDisabled : true,
				renderer : processMapPnameRenderer
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	processMapGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : processMapGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	processMapGridPanel = new Ext.grid.GridPanel( {
		id : "processMapGridPanel_id",
		renderTo : 'processMapGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 215,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		bodyStyle : 'border-left: 0px;',
		frame : false,
		store : processMapGridStore,
		colModel : processMapGridColumnModel,
		bbar : processMapGridBbar
	})

}
/*
 * 相关制度
 */
function processMapRelateRuleGrid(id, isPop) {
	
	processMapRelateRuleGridStore = new Ext.data.JsonStore( {
		autoLoad : true,
		baseParams : {
			flowId : id
		},
		proxy : new Ext.data.HttpProxy( {
			url : "findRelatedRule.action"
		}),
		fields : [ {
			name : "ruleId",
			mapping : "ruleId"
		}, {
			name : "ruleName",
			mapping : "ruleName"
		}, {
			name : "ruleNum",
			mapping : "ruleNum"
		}, {
			name : "orgId",
			mapping : "orgId"
		}, {
			name : "ruleOrgName",
			mapping : "ruleOrgName"
		}, {
			name : "ruleType",
			mapping : "ruleType"
		}, {
			name : "isDir",
			mapping : "isDir"
		}, {
			name : "fileId",
			mapping : "fileId"
		} ]
	});

	processMapRelateRuleGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), { // 制度名称
				header : i_ruleName,
				dataIndex : "ruleName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : ruleProcessNameRenderer
			}, {
				header : i_ruleNum, // 制度编号
				dataIndex : "ruleNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_dutyOrg, // 责任部门
				dataIndex : "ruleOrgName",
				align : "center",
				menuDisabled : true,
				renderer : processMapOrgNameRenderer
			}, {
				header : i_ruleType, // 制度类别
				dataIndex : "ruleType",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	
	Ext.Ajax.request({
	url : "getConfigValueByMark",
				method : "post",
				async : false,
				params: {
                    mark:"ruleType"
                },
				success : function(g) {
				    if(g.responseText=="0"){
				    	processMapRelateRuleGridColumnModel.setHidden(4,true); 
				    }
				}
			});
	
	if (isPop == true || isPop == "true") {
		processRuleHeight = document.documentElement.clientHeight - 55
	} else {
		processRuleHeight = document.documentElement.clientHeight - 115
	}
	processMapRelateRuleGridPanel = new Ext.grid.GridPanel( {
		id : "processMapRelateRuleGridPanel_id",
		renderTo : 'processMapRelatedRuleId',
		height : processRuleHeight,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		border : false,
		frame : false,
		store : processMapRelateRuleGridStore,
		colModel : processMapRelateRuleGridColumnModel
	})
}

/**
 * 制度名称增加链接
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 * @return {}
 */
function ruleProcessNameRenderer(value, cellmeta, record) {
	// 制度类型
	var isDir = record.data["isDir"];
	if (isDir == 1) {
		return "<div  ext:qtip='" + value
				+ "'><a target='_blank' class='a-operation' href='" + basePath
				+ "rule.action?from=processMap&type=ruleModeFile&reqType=public&ruleId="
				+ record.data['ruleId'] + "'>" + value + "</a>";
	} else if (isDir == 2) {
		return "<div  ext:qtip='" + value
				+ "'><a target='_blank' class='a-operation' href='" + basePath
				+ "rule.action?from=processMap&type=ruleFile&reqType=public&ruleId="
				+ record.data['ruleId'] + "&fileId=" + record.data['fileId']
				+ "'>" + value + "</a>";
	}
}

function processSysSearch(){
	if(document.getElementById("processRadio").checked){
		processGridSearch();
	}else{
		processMapGridSearch();
	}
}


/**
 * 组织名称增加超链接 用于Ext Grid
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 * @return {}
 */
function processMapOrgNameRenderer(value, cellmeta, record) {
	return "<div  ext:qtip='"
			+ value
			+ "'><a target='_blank' class='a-operation' href='organization.action?orgId="
			+ record.data['orgId'] + "' >" + value + "</a>";
}

/**
 * 流程搜索
 */
function processGridSearch(){
	//流程名称
	processGridStore.setBaseParam("processName", $("#processName_id").attr("value"));
	//流程编号									  
	processGridStore.setBaseParam("processNum",  $("#processNumber_id").attr("value"));
	//流程类别
	processGridStore.setBaseParam("processType",  $("#processType_id").val());
	//密级
	processGridStore.setBaseParam("secretLevel",  $("#processIntensive_id").val());
	//责任部门名称
	processGridStore.setBaseParam("orgName",$("#orgDutyName_process_id").attr("value"));
	//责任部门ID
	processGridStore.setBaseParam("orgId",$("#orgDutyId_process_id").attr("value"));
	//参与岗位
	processGridStore.setBaseParam("posName",$("#participateInPostName_process_id").attr("value"));
	//参与岗位ID
	processGridStore.setBaseParam("posId",$("#participateInPostId_process_id").attr("value").split("_")[0]);
	//查阅部门名称
	processGridStore.setBaseParam("orgLookName",$("#orgLookName_process_id").attr("value"));
	//部阅部门ID
	processGridStore.setBaseParam("orgLookId",$("#orgLookId_process_id").attr("value"));
	//查阅岗位名称
	processGridStore.setBaseParam("posLookName",$("#posLookName_process_id").attr("value"));
	//查阅岗位ID
	processGridStore.setBaseParam("posLookId",$("#posLookId_process_id").attr("value").split("_")[0]);
	processGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
}
/**
 * 流程地图搜索
 */
function processMapGridSearch(){
	
	  //查阅部门名称
	 var orgLookName=document.getElementById("orgMapLookName_mapProcess_id").value;
	 //查阅部门ID 
	 var orgLookId=document.getElementById("orgMapLookId_mapProcess_id").value;
	 //查阅岗位名称
	 var posLookName=document.getElementById("posMapLookName_mapProcess_id").value;
	 //查阅岗位ID
	 var posLookId=document.getElementById("posMapLookId_mapProcess_id").value.split("_")[0];
	 //名称
	 processMapGridStore.setBaseParam("processName", $("#processMapName_id").attr("value"));
	  //密级
	 processMapGridStore.setBaseParam("secretLevel",  $("#processMapIntensive_id").val());
	
	 processMapGridStore.setBaseParam("orgLookName", orgLookName);
	 if(orgLookId&&orgLookId!=""){
     	processMapGridStore.setBaseParam("orgLookId", orgLookId);
     }
     processMapGridStore.setBaseParam("posLookName", posLookName);
     if(posLookId&&posLookId!=""){
     	processMapGridStore.setBaseParam("posLookId", posLookId);
     }

	 processMapGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
}

/**
 * 查询条件重置
 */
function processReset() {
	if(document.getElementById("processRadio").checked){
		document.getElementById("processName_id").value = "";
		document.getElementById("processNumber_id").value = "";
		document.getElementById("processType_id").options[0].selected=true;
		document.getElementById("processIntensive_id").options[0].selected=true;
		document.getElementById("orgDutyName_process_id").value = "";
		document.getElementById("orgDutyId_process_id").value =-1;
		
		document.getElementById("participateInPostName_process_id").value = "";
		document.getElementById("participateInPostId_process_id").value = -1;
		
		document.getElementById("orgLookName_process_id").value = "";
		document.getElementById("orgLookId_process_id").value = -1;
		document.getElementById("posLookName_process_id").value = "";
		document.getElementById("posLookId_process_id").value = -1;
		
	}else{
		document.getElementById("processMapName_id").value = "";
		document.getElementById("processMapIntensive_id").options[0].selected=true;
		document.getElementById("orgMapLookName_mapProcess_id").value = "";
		document.getElementById("posMapLookName_mapProcess_id").value = "";
		document.getElementById("orgMapLookId_mapProcess_id").value = -1;
		document.getElementById("posMapLookId_mapProcess_id").value = -1;
	}

	
}

/**
 * 流程名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processNameRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['flowId']+"' >"+value+"</a>";
}

/**
 * 流程地图名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processMapNameRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=processMap&flowId="+record.data['flowId']+"' >"+value+"</a>";
}

/**
 * 所属流程地图名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processMapPnameRenderer(value, cellmeta, record){
	return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=processMap&flowId="+record.data['pid']+"' >"+value+"</a>";
}