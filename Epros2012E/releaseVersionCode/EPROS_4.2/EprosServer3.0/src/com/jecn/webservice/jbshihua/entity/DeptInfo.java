package com.jecn.webservice.jbshihua.entity;

import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;

public class DeptInfo {

	private Integer id;
	private String deptCode;
	private String deptName;
	private String deptType;
	private String deptParentCode;
	private String deptStatus;

	public DeptInfo() {
	}

	public DeptInfo(String deptCode, String deptName, String deptParentCode, String deptStatus) {
		this.deptCode = deptCode;
		this.deptName = deptName;
		this.deptParentCode = deptParentCode;
		this.deptStatus = deptStatus;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeptCode() {
		return this.deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = (deptCode == null ? null : deptCode.trim());
	}

	public String getDeptName() {
		return this.deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = (deptName == null ? null : deptName.trim());
	}

	public String getDeptType() {
		return this.deptType;
	}

	public void setDeptType(String deptType) {
		this.deptType = (deptType == null ? null : deptType.trim());
	}

	public String getDeptParentCode() {
		return this.deptParentCode;
	}

	public void setDeptParentCode(String deptParentCode) {
		this.deptParentCode = (deptParentCode == null ? null : deptParentCode.trim());
	}

	public String getDeptStatus() {
		return this.deptStatus;
	}

	public void setDeptStatus(String deptStatus) {
		this.deptStatus = deptStatus;
	}

	public JecnOrgView toOrgViewBean() {
		return new JecnOrgView(this.deptCode, this.deptName, this.deptParentCode, this.deptStatus);
	}
}
