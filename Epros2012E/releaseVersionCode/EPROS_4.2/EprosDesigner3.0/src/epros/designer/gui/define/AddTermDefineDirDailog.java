package epros.designer.gui.define;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class AddTermDefineDirDailog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddTermDefineDirDailog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddTermDefineDirDailog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addDirI");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		TermDefinitionLibrary termDefinitionLibrary = JecnTermDefineCommon.getTermDefinitionLibrary(this.getName(),
				pNode, 0, "");
		// 执行数据库表的添加保存
		try {
			termDefinitionLibrary = ConnectionPool.getTermDefinitionAction().createDir(termDefinitionLibrary);
			// 向树节点添加文件 目录
			JecnTreeBean jecnTreeBean = JecnTermDefineCommon.getJecnTreeBean(termDefinitionLibrary);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddTermDefineDirDailog saveData is error", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return ConnectionPool.getTermDefinitionAction().isExistAdd(name, pNode.getJecnTreeBean().getId(), 0);
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}
}
