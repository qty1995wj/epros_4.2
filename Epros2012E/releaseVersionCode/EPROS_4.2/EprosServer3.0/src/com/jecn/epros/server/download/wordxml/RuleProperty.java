package com.jecn.epros.server.download.wordxml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.LineRuleType;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.WordDocument;
import wordxml.element.dom.page.Sect;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.util.JecnPath;

public class RuleProperty {
	// 流程图横纵向标志 true:横向 false:纵向
	private boolean _flowChartDirection;
	// 封面节
	private Sect _titleSect;
	// 第一节（包含流程图节前的内容）
	private Sect _firstSect;
	// 流程图节
	private Sect _flowChartSect;
	// 第二节 （包含流程图节后的内容）
	private Sect _secondSect;
	// 默认标题样式
	private ParagraphBean _textTitleStyle;
	// 默认文本样式
	private ParagraphBean _textContentStyle;
	// 默认单元格标题样式
	private ParagraphBean _tblTitleStyle;
	// 默认单元格文本样式
	private ParagraphBean _tblContentStyle;
	// 默认表格样式
	private TableBean _tblStyle;
	// 默认节样式
	private SectBean _sectStyle;
	// 配置项与对应的节
	private Map<String, Sect> _markSect = new HashMap<String, Sect>();
	// 配置项
	private List<JecnConfigItemBean> _configItemBeanList;
	// 表格标题阴影 默认没有
	private boolean _tblTitleShadow = false;
	//表格标题是否跨页断行（默认支持）
	private boolean _tblTitleCrossPageBreaks = true;
	// 子类中的表格的宽度
	private float _newTblWidth;
	// 子类中的表格的行高
	private float _newRowHeight;
	/******* 公司logo ************/
	private String logoImage = JecnPath.APP_PATH + "images/wordLogo.png";
	// 流程图高度缩放值
	private float _flowChartScaleValue;

	/**
	 * 
	 * @param doc
	 * @param flowChartDirection
	 * @param configItemBeanList
	 */
	public RuleProperty(WordDocument doc, boolean flowChartDirection,
			List<JecnConfigItemBean> configItemBeanList) {
		_configItemBeanList = configItemBeanList;
		_flowChartDirection = flowChartDirection;
		_titleSect = doc.createSect();
		_firstSect = doc.createSect();
		_flowChartSect = doc.createSect();
		_secondSect = doc.createSect();
		initalizeStyleBean();
		initMarkSect();
	}

	/**
	 * 初始化属性Bean
	 * 包括段落的默认属性 
	 * 表格的默认属性 
	 * 节的默认的属性
	 */
	private void initalizeStyleBean() {
		ParagraphLineRule lineRule = new ParagraphLineRule(20,
				LineRuleType.EXACT);
		_textTitleStyle = new ParagraphBean(Align.left, new FontBean("宋体",
				Constants.xiaosi, true));
		_textTitleStyle.setInd(0, 0, 0.63F, 0); // 段落缩进 厘米
		_textTitleStyle.setSpace(0.5F, 0.2F, lineRule); // 设置段落行间距

		_tblTitleStyle = new ParagraphBean(Align.center, new FontBean("宋体",
				Constants.wuhao, true));

		_tblContentStyle = new ParagraphBean(Align.left, new FontBean("宋体",
				Constants.wuhao, false));

		_textContentStyle = new ParagraphBean(Align.left, new FontBean("宋体",
				Constants.xiaosi, false));
		_textContentStyle.setInd(0.52F, 0, 0, 0);
		_textContentStyle.setSpace(0f, 0f, lineRule);

		_tblStyle = new TableBean();// 默认表格样式
		// 所有边框 1.5 磅
		_tblStyle.setBorder(1.5F);
		_tblStyle.setBorderInsideH(1);
		_tblStyle.setBorderInsideV(1);
		// 表格左缩进0.34 厘米
		_tblStyle.setTableLeftInd(0.34F);
		// 设置单元格内边距父
		_tblStyle.setCellMargin(0.1F, 0.2F, 0, 0.2F);

		// 默认节样式
		_sectStyle = new SectBean();
		// 设置页边距
		_sectStyle.setPage(1.5F, 1.5F, 1.5F, 1.5F, 1.5F, 1.75F);
		_sectStyle.setSize(21, 29.7F);

		// 初始化节点
		_titleSect.setSectBean(_sectStyle);
		_firstSect.setSectBean(_sectStyle);
		// 横向显示时设置节属性
		if (_flowChartDirection) {
			SectBean sectBean = new SectBean();
			sectBean.setHorizontal(_flowChartDirection);
			sectBean.setSize(29.7F, 21);
			sectBean.setPage(1.5F, 1.5F, 1.5F, 1.5F, 1.5F, 1.75F);
			_flowChartSect.setSectBean(sectBean);
		}
		_secondSect.setSectBean(_sectStyle);
	}

	/**
	 * 获取项的所属节点
	 * 划归 a1 a2 a3...属于哪一节
	 * 在a9也就是流程图是纵向显示，都在第一节
	 * 如果横向显示a9放入流程图节，之前的放入第一节 后面的放入第二节
	 * 
	 * @param docProperty
	 *            .getFirstSect()Content
	 * @param secondSectContent
	 * @param mark
	 * @return
	 */
	private void initMarkSect() {
		// 划分内容归属于哪一个节
		boolean beforeFlowChart = true;
		if(_configItemBeanList!=null){
			for (JecnConfigItemBean configItem : _configItemBeanList) {
				if ("0".equals(configItem.getValue())) {// 是否显示
					continue;
				}
				// 流程图纵向显示时，所有内容都在第一节里
				if (!_flowChartDirection) {
					_markSect.put(configItem.getMark(), _firstSect);
				} else {
					// 流程图 横向显示
					if ("a9".equals(configItem.getMark())) {
						_markSect.put(configItem.getMark(), _flowChartSect);
						beforeFlowChart = false;
						continue;
					}
					// 处理非a9的项
					if (beforeFlowChart) {
						_markSect.put(configItem.getMark(), _firstSect);
					} else {
						_markSect.put(configItem.getMark(), _secondSect);
					}
				}
			}
		}
	}

	public boolean isFlowChartDirection() {
		return _flowChartDirection;
	}

	public void setFlowChartDirection(boolean flowChartDirection) {
		this._flowChartDirection = flowChartDirection;
	}

	public Sect getTitleSect() {
		return _titleSect;
	}

	public void setTitleSect(Sect titleSect) {
		this._titleSect = titleSect;
	}

	public Sect getFirstSect() {
		return _firstSect;
	}

	public void setFirstSect(Sect firstSect) {
		this._firstSect = firstSect;
	}

	public Sect getFlowChartSect() {
		return _flowChartSect;
	}

	public void setFlowChartSect(Sect flowChartSect) {
		this._flowChartSect = flowChartSect;
	}

	public Sect getSecondSect() {
		return _secondSect;
	}

	public void setSecondSect(Sect secondSect) {
		this._secondSect = secondSect;
	}

	public ParagraphBean getTextTitleStyle() {
		return _textTitleStyle;
	}

	public void setTextTitleStyle(ParagraphBean textTitleStyle) {
		if (textTitleStyle != null) {
			this._textTitleStyle = textTitleStyle;
		}
	}

	public ParagraphBean getTextContentStyle() {
		return _textContentStyle;
	}

	public void setTextContentStyle(ParagraphBean textContentStyle) {
		if (textContentStyle != null) {
			this._textContentStyle = textContentStyle;
		}
	}

	public ParagraphBean getTblTitleStyle() {
		return _tblTitleStyle;
	}

	public void setTblTitleStyle(ParagraphBean tblTitleStyle) {
		if (tblTitleStyle != null) {
			this._tblTitleStyle = tblTitleStyle;
		}
	}

	public ParagraphBean getTblContentStyle() {
		return _tblContentStyle;
	}

	public void setTblContentStyle(ParagraphBean tblContentStyle) {
		if (tblContentStyle != null) {
			this._tblContentStyle = tblContentStyle;
		}
	}

	public TableBean getTblStyle() {
		return _tblStyle;
	}

	public void setTblStyle(TableBean tblStyle) {
		if (tblStyle != null) {
			this._tblStyle = tblStyle;
		}
	}

	public SectBean getSectStyle() {
		return _sectStyle;
	}

	public void setSectStyle(SectBean sectStyle) {
		if (sectStyle != null) {
			this._sectStyle = sectStyle;
		}
	}

	public Map<String, Sect> getMarkSect() {
		return _markSect;
	}

	public void setMarkSect(Map<String, Sect> markSect) {
		this._markSect = markSect;
	}

	public List<JecnConfigItemBean> getConfigItemBeanList() {
		return _configItemBeanList;
	}

	public void setConfigItemBeanList(
			List<JecnConfigItemBean> configItemBeanList) {
		this._configItemBeanList = configItemBeanList;
	}

	public boolean isTblTitleShadow() {
		return _tblTitleShadow;
	}

	public void setTblTitleShadow(boolean tblTitleShadow) {
		this._tblTitleShadow = tblTitleShadow;
	}

	public boolean isTblTitleCrossPageBreaks() {
		return _tblTitleCrossPageBreaks;
	}
	public void setTblTitleCrossPageBreaks(boolean tblTitleCrossPageBreaks) {
		_tblTitleCrossPageBreaks = tblTitleCrossPageBreaks;
	}

	public float getNewTblWidth() {
		return _newTblWidth;
	}

	public void setNewTblWidth(float newTblWidth) {
		this._newTblWidth = newTblWidth;
	}

	public float getNewRowHeight() {
		return _newRowHeight;
	}

	public void setNewRowHeight(float newRowHeight) {
		_newRowHeight = newRowHeight;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public float getFlowChartScaleValue() {
		return _flowChartScaleValue;
	}

	public void setFlowChartScaleValue(float flowChartScaleValue) {
		this._flowChartScaleValue = flowChartScaleValue;
	}
}
