package com.jecn.epros.server.service.dataImport.match.constant;


/**
 * 人员导入所用到的SQL语句
 * 
 * @author zhangjie 20120213
 * 
 */
public class MatchSqlConstant {

	// ---------------------------------------项目 开始
	/** 获取部门临时表中项目ID */
	public final static String SELECT_JECN_SANFU_DEPT_PROJECT_ID = "SELECT PROJECT_ID FROM JECN_MATCH_DEPT_BEAN T GROUP BY PROJECT_ID";
	// ---------------------------------------项目 结束

	// ---------------------------------------部门 开始
	/** 删除部门临时表所有数据 */
	public final static String DELETE_SANFU_IO_DEPT_SQL = "DELETE FROM JECN_MATCH_DEPT_BEAN";
	/** 同一部门下部门名称不能重复 */
	public final static String SELECT_DEPT_NAME_SAME_PDEPT = "SELECT DISTINCT TD.*"
			+ "  FROM JECN_MATCH_DEPT_BEAN TD"
			+ "  JOIN JECN_MATCH_DEPT_BEAN TD2 ON TD.DEPT_ID <> TD2.DEPT_ID"
			+ "                           AND TD.DEPT_NAME = TD2.DEPT_NAME"
			+ "                           AND TD.P_DEPT_NUM = TD2.P_DEPT_NUM";

	/** 查询 实际部门表所有数据 */
	public final static String SELECT_ACT_DEPT = "SELECT * FROM JECN_MATCH_ACTUAL_DEPT";

	/** 待导入部门中在各自实际库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_DEPT_ORACLE = "UPDATE JECN_MATCH_DEPT_BEAN DT"
			+ "   SET DT.DEPT_FLAG = 1"
			+ " WHERE EXISTS (SELECT 1 FROM JECN_MATCH_ACTUAL_DEPT G WHERE G.DEPT_NUM = DT.DEPT_NUM AND G.PROJECT_ID=:PROJECTID)";
	/** 待导入部门中在各自实际库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_DEPT_SQLSERVER = "UPDATE  DT"
			+ "   SET DT.DEPT_FLAG = 1 FROM JECN_MATCH_DEPT_BEAN DT"
			+ " WHERE EXISTS (SELECT 1 FROM JECN_MATCH_ACTUAL_DEPT G WHERE G.DEPT_NUM = DT.DEPT_NUM AND G.PROJECT_ID=:PROJECTID)";
	/** 插入部门临时表中标识是插入的部门 */
	// oracle
	public final static String INSERT_SANFU_ACT_DEPT_ORACLE = "INSERT INTO JECN_MATCH_ACTUAL_DEPT(DEPT_ID,DEPT_NUM,DEPT_NAME,P_DEPT_NUM,PROJECT_ID)"
			+ " SELECT SANFU_ACTUAL_DEPT_SQUENCE.NEXTVAL AS ORG_ID, "
			+ "       DEPT_NUM                      AS DEPT_NUM,"
			+ "       DEPT_NAME                     AS DEPT_NAME,"
			+ "       P_DEPT_NUM                    AS P_DEPT_NUM,"
			+ "       :PROJECTID                    AS PROJECT_ID"
			+ "  FROM JECN_MATCH_DEPT_BEAN G WHERE G.DEPT_FLAG=0";
	// sqlserver
	public final static String INSERT_SANFU_ACT_DEPT_SQLSERVER = "INSERT INTO JECN_MATCH_ACTUAL_DEPT(DEPT_NUM,DEPT_NAME,P_DEPT_NUM,PROJECT_ID)"
			+ " SELECT "
			+ "       DEPT_NUM                      AS DEPT_NUM,"
			+ "       DEPT_NAME                     AS DEPT_NAME,"
			+ "       P_DEPT_NUM                    AS P_DEPT_NUM,"
			+ "       :PROJECTID                    AS PROJECT_ID"
			+ "  FROM JECN_MATCH_DEPT_BEAN G WHERE G.DEPT_FLAG=0";
	/** 获取 标识为更新 的部门临时表信息 */
	public final static String SELECT_DEPTBEAN_LIST_FLAG = "SELECT * FROM JECN_MATCH_DEPT_BEAN WHERE DEPT_FLAG = :flag";

	/** 删除部门实际表： 在部门临时表中不存在的数据 */
	public final static String DELETE_ACT_DEPT_DEPTNUM = "DELETE FROM JECN_MATCH_ACTUAL_DEPT "
			+ "WHERE DEPT_NUM NOT IN "
			+ "(SELECT DP.DEPT_NUM FROM JECN_MATCH_DEPT_BEAN DP)";

	// ---------------------------------------部门结束

	// ---------------------------------------岗位 开始
	/** 删除岗位临时表所有数据 */
	public final static String DELETE_SANFU_IO_POS_SQL = "DELETE FROM JECN_MATCH_POS_BEAN";

	/** 查询实际岗位表所有数据 */
	public final static String SELECT_ACT_POS = "SELECT * FROM JECN_MATCH_ACTUAL_POS";

	/** 待导入岗位以中在各自实际库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_POS_ORACLE = "UPDATE JECN_MATCH_POS_BEAN PS"
			+ "   SET PS.POS_FLAG = 1"
			+ " WHERE EXISTS (SELECT 1"
			+ "          FROM JECN_MATCH_ACTUAL_POS G, JECN_MATCH_ACTUAL_DEPT D"
			+ "         WHERE G.ACT_POS_NUM = PS.ACT_POS_NUM"
			+ "           AND G.DEPT_NUM = D.DEPT_NUM"
			+ "           AND D.PROJECT_ID = :PROJECTID)";

	/** 待导入岗位以中在各自实际库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_POS_SQLSERVER = "UPDATE PS"
			+ "   SET PS.POS_FLAG = 1 FROM JECN_MATCH_POS_BEAN PS"
			+ " WHERE EXISTS (SELECT 1"
			+ "          FROM JECN_MATCH_ACTUAL_POS G, JECN_MATCH_ACTUAL_DEPT D"
			+ "         WHERE G.ACT_POS_NUM = PS.ACT_POS_NUM"
			+ "           AND G.DEPT_NUM = D.DEPT_NUM"
			+ "           AND D.PROJECT_ID = :PROJECTID)";

	/** 插入岗位临时表中标识是插入的岗位 */
	// oracle
	public final static String INSERT_SANFU_ACT_POS_ORACLE = "INSERT INTO JECN_MATCH_ACTUAL_POS"
			+ "  (ID,"
			+ "   BASE_POS_NUM,"
			+ "   BASE_POS_NAME,"
			+ "   DEPT_NUM,"
			+ "   ACT_POS_NUM,"
			+ "   ACT_POS_NAME)"
			+ "  SELECT SANFU_ACTUAL_POS_SQUENCE.NEXTVAL,"
			+ "         BASE_POS_NUM,"
			+ "         BASE_POS_NAME,"
			+ "         DEPT_NUM,"
			+ "         ACT_POS_NUM,"
			+ "         ACT_POS_NAME"
			+ "    FROM JECN_MATCH_POS_BEAN G WHERE G.POS_FLAG = 0";

	// sqlserver
	public final static String INSERT_SANFU_ACT_POS_SQLSERVER = "INSERT INTO JECN_MATCH_ACTUAL_POS"
			+ "  ("
			+ "   BASE_POS_NUM,"
			+ "   BASE_POS_NAME,"
			+ "   DEPT_NUM,"
			+ "   ACT_POS_NUM,"
			+ "   ACT_POS_NAME)"
			+ "  SELECT "
			+ "         BASE_POS_NUM,"
			+ "         BASE_POS_NAME,"
			+ "         DEPT_NUM,"
			+ "         ACT_POS_NUM,"
			+ "         ACT_POS_NAME"
			+ "    FROM JECN_MATCH_POS_BEAN G WHERE G.POS_FLAG = 0";
	/** 获取 更新标识 的岗位临时表信息 */
	public final static String SELECT_POSBEAN_LIST_FLAG = "SELECT * FROM JECN_MATCH_POS_BEAN WHERE POS_FLAG = :flag";

	/** 删除岗位实际表： 在岗位临时表中不存在的数据 */
	public final static String DELETE_ACT_POS_ACTNUM = "DELETE FROM JECN_MATCH_ACTUAL_POS "
			+ "WHERE ACT_POS_NUM NOT IN "
			+ "(SELECT AP.ACT_POS_NUM FROM JECN_MATCH_POS_BEAN AP)";

	// ---------------------------------------岗位 结束

	// ---------------------------------------人员开始
	/** 删除人员临时表所有数据 */
	public final static String DELETE_SHAFU_IO_USER_SQL = "DELETE FROM JECN_MATCH_USER_BEAN";

	/** 查询实际人员表所有信息 */
	public final static String SELECT_ACT_USER = "SELECT * FROM JECN_MATCH_ACTUAL_PERSON";

	/** 待导入人员中在各自实际库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_USER_ORACLE = "update JECN_MATCH_USER_BEAN"
			+ "   set PERSON_FLAG = 1"
			+ " WHERE Login_Name in"
			+ "       (select PE.Login_Name"
			+ "          from JECN_MATCH_USER_BEAN PE"
			+ "         INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                   AP.LOGIN_NAME"
			+ "                                               AND PE.True_Name <>"
			+ "                                                   AP.True_Name"
			+ "        union "
			+ "        select PE.Login_Name"
			+ "          from JECN_MATCH_USER_BEAN PE"
			+ "         INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                   AP.LOGIN_NAME"
			+ "                                               AND PE.Phone <> AP.Phone"
			+ "        union "
			+ "        select PE.Login_Name"
			+ "          from JECN_MATCH_USER_BEAN PE"
			+ "         INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                   AP.LOGIN_NAME"
			+ "                                               AND PE.Phone is null"
			+ "                                               AND AP.Phone is not null"
			+ "        union "
			+ "        select PE.Login_Name"
			+ "          from JECN_MATCH_USER_BEAN PE"
			+ "         INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                   AP.LOGIN_NAME"
			+ "                                               AND PE.Phone is not null"
			+ "                                               AND AP.Phone is null"
			+ "        union "
			+ "        select PE.Login_Name"
			+ "          from JECN_MATCH_USER_BEAN PE"
			+ "         INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                   AP.LOGIN_NAME"
			+ "                                               AND PE.Email <> AP.Email"
			+ "        union "
			+ "        select PE.Login_Name"
			+ "          from JECN_MATCH_USER_BEAN PE"
			+ "         INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                   AP.LOGIN_NAME"
			+ "                                               AND PE.Email is null"
			+ "                                               AND AP.Email is not null"
			+ "        union "
			+ "        select PE.Login_Name"
			+ "          from JECN_MATCH_USER_BEAN PE"
			+ "         INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                   AP.LOGIN_NAME"
			+ "                                               AND PE.Email is not null"
			+ "                                               AND AP.Email is null"
			+ "       union"
			+ "       select PE.Login_Name"
			+ "         from JECN_MATCH_USER_BEAN PE"
			+ "        INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                  AP.LOGIN_NAME"
			+ "                                              AND PE.Position_Num <>"
			+ "                                                  AP.POSITION_NUM"
			+ "                                                     union"
			+ "       select PE.Login_Name"
			+ "         from JECN_MATCH_USER_BEAN PE"
			+ "        INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                  AP.LOGIN_NAME"
			+ "                                              AND PE.POSITION_NUM is null"
			+ "                                              AND AP.POSITION_NUM is not null"
			+ "       union"
			+ "       select PE.Login_Name"
			+ "         from JECN_MATCH_USER_BEAN PE"
			+ "        INNER JOIN JECN_MATCH_ACTUAL_PERSON AP ON PE.LOGIN_NAME ="
			+ "                                                  AP.LOGIN_NAME"
			+ "                                              AND PE.POSITION_NUM is not null"
			+ "                                              AND AP.POSITION_NUM is null"
			+ ")";

	/** 待导入人员中在各自实际库中存在的数据， 更新其操作标识设置为1 */
	public final static String UPDATE_PRO_FLAG_USER_SQLSERVER = "UPDATE PE "
			+ "  SET PE.PERSON_FLAG = 1 FROM JECN_MATCH_USER_BEAN PE "
			+ "WHERE EXISTS (SELECT 1 FROM JECN_MATCH_ACTUAL_PERSON G WHERE G.LOGIN_NAME = PE.LOGIN_NAME "
			+ "AND  "
			+ "(G.POSITION_NUM <> PE.POSITION_NUM OR G.TRUE_NAME <> PE.TRUE_NAME "
			+ "OR G.EMAIL <> PE.EMAIL " + "OR G.PHONE <> PE.PHONE "
			+ "OR G.EMAIL_TYPE <> PE.EMAIL_TYPE ))";
	/***/
	public final static String UPDATE_JECN_USER_COUNT = "SELECT count(*) from JECN_USER  where LOGIN_NAME <> 'admin' and ISLOCK = 0  and LOGIN_NAME NOT IN (SELECT AP.LOGIN_NAME FROM JECN_MATCH_USER_BEAN AP)";
	/** 更新流程人员表：在人员实际表中不存在的数据 给予锁定状态 0:解锁 1：锁定 */
	public final static String UPDATE_JECN_USER_ISLOCK = "UPDATE JECN_USER"
			+ "  SET ISLOCK = 1" + " WHERE LOGIN_NAME NOT IN "
			+ " (SELECT AP.LOGIN_NAME FROM JECN_MATCH_ACTUAL_PERSON AP) "
			+ " AND LOGIN_NAME <> 'admin'";
	/** 插入人员临时表中标识是插入的人员 */
	// oracle
	public final static String INSERT_SANFU_ACT_USER_ORACLE = "INSERT INTO JECN_MATCH_ACTUAL_PERSON"
			+ "(ID,"
			+ "LOGIN_NAME,"
			+ "TRUE_NAME,"
			+ "EMAIL,"
			+ "PHONE,"
			+ "EMAIL_TYPE,"
			+ "POSITION_NUM)"
			+ "  SELECT SANFU_ACTUAL_PERSON_SQUENCE.NEXTVAL,"
			+ "         LOGIN_NAME,"
			+ "         TRUE_NAME,"// --登录名称
			+ "         EMAIL,"
			+ "         PHONE,"
			+ "         EMAIL_TYPE,"
			+ "         POSITION_NUM"
			+ "    FROM JECN_MATCH_USER_BEAN G WHERE G.PERSON_FLAG = 0";
	// sqlserver
	public final static String INSERT_SANFU_ACT_USER_SQLSERVER = "INSERT INTO JECN_MATCH_ACTUAL_PERSON"
			+ "("
			+ "LOGIN_NAME,"
			+ "TRUE_NAME,"
			+ "EMAIL,"
			+ "PHONE,"
			+ "EMAIL_TYPE,"
			+ "POSITION_NUM)"
			+ "  SELECT "
			+ "         LOGIN_NAME,"
			+ "         TRUE_NAME,"// --登录名称
			+ "         EMAIL,"
			+ "         PHONE,"
			+ "         EMAIL_TYPE,"
			+ "         POSITION_NUM"
			+ "    FROM JECN_MATCH_USER_BEAN G WHERE G.PERSON_FLAG = 0";

	/** 删除人员实际表： 在人员临时表中不存在的数据 */
	public final static String DELETE_ACT_USER_LOGINAME = "DELETE FROM JECN_MATCH_ACTUAL_PERSON "
			+ "WHERE LOGIN_NAME NOT IN "
			+ "(SELECT SP.LOGIN_NAME FROM JECN_MATCH_USER_BEAN SP)"
			+ " AND POSITION_NUM NOT IN"
			+ " (SELECT SP.POSITION_NUM FROM JECN_MATCH_USER_BEAN SP)";

	/** 查询流程岗位与实际岗位关联表信息 */
	public final static String SELECT_POS_USER_REL = "SELECT pl.*"
			+ "  FROM JECN_MATCH_POS_EPROS_REL pl" + " where exists (select 1"
			+ "          from jecn_flow_org_image fi, jecn_flow_org fo"
			+ "         where fi.org_id = fo.org_id"
			+ "           and fo.projectid = ?"
			+ "           and fi.figure_id = pl.eps_position_id)";

	/** 根据人员岗位关联表中的临时岗位ID查询岗位临时表中的信息 */
	// public final static String SELECT_POS_BY_RELID =
	// "SELECT * FROM JECN_MATCH_POS_BEAN"
	// +"WHERE ID IN (SELECT POSITION_ID FROM JECN_MATCH_POS_EPROS_REL)";
	public final static String SELECT_ACT_POS_LIST = "SELECT * FROM JECN_MATCH_ACTUAL_POS";

	/** 获取 添加 的人员临时表信息 flag=1更新 */
	public final static String SELECT_USERBEAN_LIST_FLAG = "SELECT * FROM JECN_MATCH_USER_BEAN WHERE PERSON_FLAG = :flag";

	/** 添加人员获取流程中没有的人员记录@param flag 0:添加，1：更新 */
	public final static String SELECT_NO_DIFF_LOGINNAME = "select SF.*"
			+ "  from JECN_MATCH_USER_BEAN SF" + " where not EXISTS"
			+ " (select 1 from jecn_user u where u.login_name = SF.login_name)";

	/** 添加人员信息 */
	public final static String INSERT_JECN_USER_INFO = "INSERT INTO JECN_USER"
			+ "(PEOPLE_ID,"
			+ "USER_NUMBER,"
			+ "LOGIN_NAME,"
			+ "TRUE_NAME,"
			+ "EMAIL,"
			+ "PHONE_STR)"
			+ "EMAIL_TYPE,"
			+ "  SELECT SANFU_ACTUAL_PERSON_SQUENCE.NEXTVAL,"
			+ "         LOGIN_NAME,"
			+ "LOGIN_NAME"
			+ "         TRUE_NAME,"// --登录名称
			+ "         EMAIL,"
			+ "         PHONE,"
			+ "         CASE WHEN EMAIL_TYPE=:EMAIL_INNER THEN 'inner' WHEN EMAIL_TYPE=:EMAIL_OUTER THEN 'outer' END,"
			+ "         POSITION_NUM"
			+ "    FROM JECN_MATCH_USER_BEAN G WHERE G.PERSON_FLAG = 0";

	/** 获取人员数据 */
	public final static String SELECT_JECN_USER_LISTINF = "SELECT * FROM JECN_USER";

	/** 根据 实际岗位表数据在岗位临时表中不存在的岗位编码 查询流程岗位与实际岗位关联表数据 */
	public final static String SELECT_POS_EPS_REL_LIST = "SELECT DISTINCT EPS_POSITION_ID FROM JECN_MATCH_POS_EPROS_REL "
			+ " WHERE (POSITION_NUM NOT IN  (SELECT SAP.ACT_POS_NUM FROM JECN_MATCH_POS_BEAN SAP) AND REL_TYPE=0) "
			+ "OR (POSITION_NUM NOT IN (SELECT distinct S.BASE_POS_NUM FROM JECN_MATCH_POS_BEAN S) AND REL_TYPE=1)";

	/** 岗位删除后，删除流程岗位与实际岗位关联表中 只与删除的岗位表中实际岗位编码对应的数据 */
	public final static String DELETE_JECN_POSITION_EPROS_REL = "DELETE FROM JECN_MATCH_POS_EPROS_REL "
			+ " WHERE (POSITION_NUM NOT IN  (SELECT SAP.ACT_POS_NUM FROM JECN_MATCH_POS_BEAN SAP) AND REL_TYPE=0) "
			+ "OR (POSITION_NUM NOT IN (SELECT distinct S.BASE_POS_NUM FROM JECN_MATCH_POS_BEAN S) AND REL_TYPE=1) ";

	// 获取需要更新的流程人员数据
	public final static String GET_UPDATE_JECN_USER_LIST = "SELECT U.*"
			+ "  FROM JECN_USER U" + " WHERE exists (select 1"
			+ "          from JECN_MATCH_USER_BEAN SF_U"
			+ "         where SF_U.PERSON_FLAG = 1"
			+ "           AND SF_U.LOGIN_NAME = U.LOGIN_NAME)";

	// 获取临时表中人员岗位变更的记录 SF_U.POSITION_NUM,SF_U.LOGIN_NAME
	public final static String SF_PERSON_POS_CHANGE_LIST = "SELECT SF_U.POSITION_NUM,SF_U.LOGIN_NAME FROM JECN_MATCH_ACTUAL_PERSON A,JECN_MATCH_USER_BEAN SF_U"
			+ " WHERE A.LOGIN_NAME=SF_U.LOGIN_NAME AND SF_U.POSITION_NUM <> A.POSITION_NUM group by SF_U.POSITION_NUM, SF_U.LOGIN_NAME";
	/** 查询 流程岗位与人员关联表数据 */
	public final static String SELECT_JECN_USER_POS_REL = "SELECT * FROM JECN_USER_POSITION_RELATED";

	/** 流程人员岗位管理表 */
	public final static String SELECT_FLOW_USER_REF_POS = "SELECT * FROM JECN_MATCH_ACTUAL_DEPT";

	/** 获取操作表的基准岗位 sql */
	public final static String SELECT_FLOW_BASE_POS = " SELECT BASE_POS_NUM, BASE_POS_NAME,0,0 "
			+ " FROM JECN_MATCH_ACTUAL_POS group by BASE_POS_NUM, BASE_POS_NAME order by BASE_POS_NUM,BASE_POS_NAME";

	/** 获取岗位匹配总数 */
	public final static String GET_POS_EPS_REL_COUNT = "SELECT COUNT(distinct EPS_POSITION_ID) FROM JECN_MATCH_POS_EPROS_REL";

	/** 根据匹配岗位的编码、ID查找对应的岗位名称 */
	public final static String SELECT_POS_EPS_NAMES_LIST = "";

	/** 根据关联ID 删除 流程岗位与实际岗位关联表 */
	public final static String DELETE_REL_ID = "DELETE JECN_MATCH_POS_EPROS_REL WHERE EPS_POSITION_ID =:FlowPosID";
	/** 根据流程岗位ID删除 流程岗位与人员关联表 */
	public final static String DELETE_FLOWREL_ID = "DELETE JECN_USER_POSITION_RELATED WHERE FIGURE_ID =:FlowPosID";
	/** 添加岗位匹配关系是判读是否存在该岗位匹配关系 若不存在则添加 */
	public final static String GET_ISHAV_REL_COUNT = "SELECT COUNT(*) FROM JECN_MATCH_POS_EPROS_REL WHERE EPS_POSITION_ID IN :EPS_POSITION_ID";

	/** 根据流程岗位ID 查询流程岗位与实际岗位关联表数据 */
	public final static String FIND_REL_BY_FLOWID = "SELECT POSITION_NUM FROM JECN_MATCH_POS_EPROS_REL WHERE EPS_POSITION_ID = :STATIONID";

	/** 获取没有匹配关系的岗位 */
	public final static String FIND_STATION_NO_MATCH = "SELECT SF_P.*,SF_D.DEPT_NAME FROM JECN_MATCH_ACTUAL_POS SF_P ,JECN_MATCH_ACTUAL_DEPT SF_D WHERE SF_P.ACT_POS_NUM NOT IN "
			+ "(SELECT POSITION_NUM FROM JECN_MATCH_POS_EPROS_REL WHERE REL_TYPE=0) AND SF_P.BASE_POS_NUM NOT IN "
			+ "(SELECT POSITION_NUM FROM JECN_MATCH_POS_EPROS_REL WHERE REL_TYPE=1) AND SF_P.DEPT_NUM = SF_D.DEPT_NUM";

	/** 获取未匹配的HR岗位中总数 */
	public final static String FIND_STATION_NO_MATCH_COUNT = "SELECT COUNT(*) FROM JECN_MATCH_ACTUAL_POS SF_P WHERE SF_P.ACT_POS_NUM NOT IN "
			+ "(SELECT POSITION_NUM FROM JECN_MATCH_POS_EPROS_REL WHERE REL_TYPE=0) AND SF_P.BASE_POS_NUM NOT IN "
			+ "(SELECT POSITION_NUM FROM JECN_MATCH_POS_EPROS_REL WHERE REL_TYPE=1)";

	/** 获取没有匹配关系的流程岗位 */
	public final static String FIND_FLOW_POS_NAME = "SELECT distinct F_O.ORG_NAME,F_P.FIGURE_TEXT,F_P.Figure_Id FROM JECN_FLOW_ORG_IMAGE F_P,JECN_FLOW_ORG F_O "
			+ "WHERE F_O.ORG_ID=F_P.ORG_ID AND F_P.FIGURE_TYPE='PositionFigure' AND F_P.FIGURE_ID NOT IN (SELECT distinct EPS_POSITION_ID FROM JECN_MATCH_POS_EPROS_REL) ";
	/** 获取没有匹配关系的流程岗位 */
	public final static String FIND_FLOW_POS_NAME_COUNT = "SELECT COUNT(*) FROM ( SELECT distinct F_O.ORG_NAME, F_P.FIGURE_TEXT,F_P.Figure_Id FROM JECN_FLOW_ORG_IMAGE F_P,JECN_FLOW_ORG F_O "
			+ "WHERE F_O.ORG_ID=F_P.ORG_ID AND F_P.FIGURE_TYPE='PositionFigure' AND F_P.FIGURE_ID NOT IN (SELECT distinct EPS_POSITION_ID FROM JECN_MATCH_POS_EPROS_REL) ";

	/** 获取没有匹配关系的基准岗位 */
	public final static String FIND_BASE__POS_NO_MATCH = "SELECT BASE_POS_NUM, BASE_POS_NAME  FROM "
			+ "JECN_MATCH_ACTUAL_POS WHERE BASE_POS_NUM NOT IN "
			+ "(SELECT POSITION_NUM  FROM JECN_MATCH_POS_EPROS_REL WHERE REL_TYPE = 1) ";

	/** 获取没有匹配关系的基准岗位 */
	public final static String FIND_BASE__POS_NO_MATCH_COUNT = "SELECT COUNT(distinct BASE_POS_NUM)  FROM "
			+ "JECN_MATCH_ACTUAL_POS WHERE BASE_POS_NUM NOT IN "
			+ "(SELECT POSITION_NUM  FROM JECN_MATCH_POS_EPROS_REL WHERE REL_TYPE = 1) ";
}
