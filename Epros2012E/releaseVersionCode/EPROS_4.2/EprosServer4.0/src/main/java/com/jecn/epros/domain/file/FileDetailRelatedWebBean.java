package com.jecn.epros.domain.file;

/**
 * 文件清单关联bean
 */
public class FileDetailRelatedWebBean {

    /**
     * 文件使用情况Id
     **/
    private Long relatedId;
    /**
     * 文件使用情况Name
     **/
    private String relatedName;
    /**
     * 0是流程架构、1是流程、2是制度、3标准、4是组织 、5 岗位 6风险
     */
    private Integer relatedType;

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public Integer getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(Integer relatedType) {
        this.relatedType = relatedType;
    }

}
