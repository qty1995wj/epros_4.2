package epros.designer.gui.popedom.role.choose;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

/***
 * 选择术语RiskMoveTreeListener 2013-11-11
 * 
 */
public class RoleManageTermTreeListener extends JecnTreeListener {
	private static Logger log = Logger.getLogger(RoleManageTermTreeListener.class);
	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();

	private JTree jTree;

	public RoleManageTermTreeListener(List<Long> listIds, JTree jTree) {
		this.listIds = listIds;
		this.jTree = jTree;
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = ConnectionPool.getTermDefinitionAction().getRoleAuthChilds(
						node.getJecnTreeBean().getId(), JecnConstants.projectId, JecnConstants.getUserId());
				JecnTreeCommon.expansionTreeMoveNode(jTree, list, node, listIds);
			} catch (Exception e) {
				log.error("RoleManageTermTreeListener treeExpanded is error", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

}
