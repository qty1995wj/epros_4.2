function orgPosition() {
	var orgPositionRoot = new Ext.tree.AsyncTreeNode({
		text : i_orgPosition,
		expanded : true,
		id : "0",
		listeners : {
			'expand' : function() {
				if (orgPositionRoot.hasChildNodes()) {
					node = orgPositionRoot.childNodes[0];
					Ext.Ajax.request({
						url : basePath + "organizationLook.action",
						params : {
							orgId : node.id.split("_")[0],
							dbClick : 'dbClick'
						},
						success : function(response, r) {
							if (response.responseText == 'noPopedom') {
								orgPositionTabPanel
										.setActiveTab("orgPositionSearchTab_id");
								return;
							}
							node.select();
							processMapContainerJsp!="processMapSvg.jsp"&&Ext.MessageBox.wait(i_isLoading, i_tip);
							orgPositionTabPanel.insert(0, {
								title : i_orgMap,// 组织图
								id : "orgMap_id",
								autoLoad : {
									url : basePath
											+ "jsp/processSys/"+processMapContainerJsp+"?mapType=organization&mapID="
											+ node.id.split("_")[0],
									callback : function() {
										Ext.TaskMgr.start(taskTimer);
									}
								}
							});
							orgPositionTabPanel.insert(1, {
										title : i_orgResponsibility,// 部门职责
										id : "orgResponsibility_id",
										autoLoad : basePath
												+ "findOrgDuties.action?orgId="
												+ node.id.split("_")[0]
									});
//							if (isAdmin || isViewAdmin) {
								orgPositionTabPanel.insert(2, {
									title : i_orgList,// 组织清单
									id : "relationProcess_id",
									autoLoad : {
										url : basePath
												+ "jsp/popedom/relatedProcessList.jsp",
										callback : function() {
											orgListJson(node.id.split("_")[0],false);
										}
									}
								});
//							}
							orgPositionTabPanel.setActiveTab("orgMap_id");
						}
					});

				}

			}
		}
	})

	var orgPositionLoader = new Ext.tree.TreeLoader({
				dataUrl : "getChildsOrgAndPos.action"
			})
	var orgPositionTabPanel = new Ext.TabPanel({
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				height : Ext.getCmp("main-panel").getInnerHeight(),
				items : [{
							title : i_search,// 搜索
							id : "orgPositionSearchTab_id",
							autoLoad : {
								url : basePath
										+ "jsp/popedom/orgAndPostSearch.jsp",
								callback : function() {
									orgGrid();
									positionGrid();
								}
							}

						}],
				listeners : {
					'bodyresize' : function(p, width, height) {
						if (p.getActiveTab() == null) {
							return;
						}
						if (p.getActiveTab().getId() == 'orgPositionSearchTab_id') {
							if (document.getElementById("org") != null
									&& document.getElementById("org").checked) {
								if (typeof(orgGridPanel) != "undefined") {
									orgGridPanel.setSize(width, height - 110);
								}
							} else {
								if (typeof(positionGridPanel) != "undefined") {
									positionGridPanel.setSize(width, height
													- 110);
								}
							}
						} else if (p.getActiveTab().getId() == 'orgResponsibility_id') {

						} else if (p.getActiveTab().getId() == 'relationProcess_id') {
							setRelatedProcessWH('relatedProcessShowDiv');
						} else if (p.getActiveTab().getId() == 'positionDes_id') {
							setMaxjobDesWH('positionDes_div_id');
						}
					},
					'tabchange' : function(tabPanel, panel) {
						if(typeof(panel) == "undefined"){
							return;
						}
						if (panel.getId() == 'orgPositionSearchTab_id') {
							if (document.getElementById("org") != null
									&& document.getElementById("org").checked) {
								if (typeof(orgGridPanel) != "undefined") {
									orgGridPanel.setSize(tabPanel.getWidth(), tabPanel.getHeight() - 137);
								}
							} else {
								if (typeof(positionGridPanel) != "undefined") {
									positionGridPanel.setSize(tabPanel.getWidth(), tabPanel.getHeight()
													- 137);
								}
							}
						} else if (panel.getId() == 'orgResponsibility_id') {

						} else if (panel.getId() == 'relationProcess_id') {
							setRelatedProcessWH('relatedProcessShowDiv');
						} else if (panel.getId() == 'positionDes_id') {
							setMaxjobDesWH('positionDes_div_id');
						}

					}
				}

			})
	var orgPositionTree = new Ext.tree.TreePanel({
		root : orgPositionRoot,
		height : Ext.getCmp("main-panel").getInnerHeight() - 25,
		loader : orgPositionLoader,
		useArrows : true,
		border:false,
		autoScroll : true,
		rootVisible : false,// 隐藏根节点
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			"dblclick" : function(node, e) {
				if (node.attributes.type == "organization") {
					Ext.Ajax.request({
						url : basePath + "organizationLook.action",
						params : {
							orgId : node.id.split("_")[0],
							dbClick : 'dbClick'
						},
						success : function(response, r) {
							if (response.responseText == 'noPopedom') {
								alert(i_noPopedom);// 您没有访问权限!
								return;
							}
							// var length = orgPositionTabPanel.items.length;
							// for (var i = length - 2; i >= 0; i--) {
							// var p = orgPositionTabPanel.getComponent(i);
							// orgPositionTabPanel.remove(p, true);
							// }
							processMapContainerJsp!="processMapSvg.jsp"&&Ext.MessageBox.wait(i_isLoading, i_tip);
							orgPositionTabPanel.removeAll(true);
							orgPositionTabPanel.insert(0, {
								title : i_orgMap,// 组织图
								id : "orgMap_id",
								autoLoad : {
									url : basePath
											+ "jsp/processSys/"+processMapContainerJsp+"?mapType=organization&mapID="
											+ node.id.split("_")[0],
									callback : function() {
										Ext.TaskMgr.start(taskTimer);
									}
								}
							});
							orgPositionTabPanel.insert(1, {
										title : i_orgResponsibility,// 部门职责
										id : "orgResponsibility_id",
										autoLoad : basePath
												+ "findOrgDuties.action?orgId="
												+ node.id.split("_")[0]
									});
//							if (isAdmin || isViewAdmin) {
								orgPositionTabPanel.insert(2, {
									title : i_orgList,// 组织清单
									id : "relationProcess_id",
									autoLoad : {
										url : basePath
												+ "jsp/popedom/relatedProcessList.jsp",
										callback : function() {
											orgListJson(node.id.split("_")[0],false);
										}
									}
								});
//							}
							orgPositionTabPanel.add({
								title : i_search,// 搜索
								id : "orgPositionSearchTab_id",
								autoLoad : {
									url : basePath
											+ "jsp/popedom/orgAndPostSearch.jsp",
									callback : function() {
										orgGrid();
										positionGrid();
									}
								}

							})
							orgPositionTabPanel.setActiveTab("orgMap_id");

						}

					})

				} else if (node.attributes.type == "position") {
					Ext.Ajax.request({
						url : basePath + "stationLook.action",
						params : {
							posId : node.id.split("_")[0],
							dbClick : 'dbClick'
						},
						success : function(response, r) {
							if (response.responseText == 'noPopedom') {
								alert(i_noPopedom);// 您没有访问权限!
								return;
							}
							// var length = orgPositionTabPanel.items.length;
							// for (var i = length - 2; i >= 0; i--) {
							// var p = orgPositionTabPanel.getComponent(i);
							// orgPositionTabPanel.remove(p, true);
							// }
							orgPositionTabPanel.removeAll(true);
							orgPositionTabPanel.insert(0, {
								title : i_positionDes,// 岗位说明书
								id : "positionDes_id",
								autoLoad : {
									url : basePath
											+ "orgSpecification.action?posType=false&posId="
											+ node.id.split("_")[0],
									callback : function() {
										setMaxjobDesWH('positionDes_div_id');
									}
								}
							});
							orgPositionTabPanel.add({
								title : i_search,// 搜索
								id : "orgPositionSearchTab_id",
								autoLoad : {
									url : basePath
											+ "jsp/popedom/orgAndPostSearch.jsp",
									callback : function() {
										orgGrid();
										positionGrid();
									}
								}

							})
							orgPositionTabPanel.setActiveTab("positionDes_id");
						}
					});

				}
			}

		}

	})

	var orgPositionPanel = new Ext.Panel({
				renderTo : "orgPosition_id",
				border : false,
				id : 'orgPositionPanel_index_id',
				layout : "border",
				height : Ext.getCmp("main-panel").getInnerHeight(),
				items : [{
					region : "west",
					border : 0,
					title : "<div  style='text-align:center;font-weight:bold'>"
							+ i_orgPosition + "</div>",
					split : true,
					collapseMode : "mini",
					width : 180,
					items : [orgPositionTree]

				}, {
					region : "center",
					border : 0,
					id : "orgPosition_center_panel_id",
					items : [orgPositionTabPanel],
					listeners : {
						'bodyresize' : function(p, width, height) {
							orgPositionTree.setHeight(height - 5);
							orgPositionTabPanel.setSize(width, height);

						}
					}
				}]
			});

}

function posOrgSwitching() {
	if (document.getElementById("org").checked) {
		document.getElementById("orgDiv").style.display = "block";
		document.getElementById("posDiv").style.display = "none";
		document.getElementById("positionGrid_id").style.display = "none";
		document.getElementById("orgGrid_id").style.display = "block";
		orgGridPanel.setSize(Ext.getCmp("orgPosition_center_panel_id")
						.getInnerWidth(), Ext
						.getCmp("orgPosition_center_panel_id").getInnerHeight()
						- 137);
	} else {
		document.getElementById("posDiv").style.display = "block";
		document.getElementById("orgDiv").style.display = "none";
		document.getElementById("positionGrid_id").style.display = "block";
		document.getElementById("orgGrid_id").style.display = "none";
		positionGridPanel.doLayout(true);
		positionGridPanel.setSize(Ext.getCmp("orgPosition_center_panel_id")
						.getInnerWidth(), Ext
						.getCmp("orgPosition_center_panel_id").getInnerHeight()
						- 137);
	}
}
/**
 * 部门列表
 */
function orgGrid() {

	orgGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "searchOrgList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "id",
									mapping : "id"
								}, {
									name : "name",
									mapping : "name"
								}, {
									name : "pid",
									mapping : "pid"
								}, {
									name : "pname",
									mapping : "pname"
								}])

			});

	orgGridColumnModel = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header : i_orgName,
				dataIndex : "name",
				align : "center",
				menuDisabled : true,
				renderer : orgNameRenderer
			}, {
				header : i_superiorOrg,
				dataIndex : "pname",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : orgPnameRenderer
			}

	]);
	orgGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : orgGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	orgGridPanel = new Ext.grid.GridPanel({
				id : "orgGridPanel_id",
				renderTo : 'orgGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 137,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : orgGridStore,
				colModel : orgGridColumnModel,
				bbar : orgGridBbar
			})

}

function positionGrid() {

	positionGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "searchPosList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "id",
									mapping : "id"
								}, {
									name : "name",
									mapping : "name"
								}, {
									name : "pid",
									mapping : "pid"
								}, {
									name : "pname",
									mapping : "pname"
								}])

			});

	positionGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_positionName,
				dataIndex : "name",
				align : "center",
				menuDisabled : true,
				renderer : positionNameRenderer
			}, {
				header : i_belongOrg,
				dataIndex : "pname",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : orgPnameRenderer
			}

	]);
	positionGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : positionGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});
     
			
	//解决IE6滚动条不显示问题
	var vstr = $.browser.version;
	var extWidth;
    if($.browser.msie&&(vstr =='6.0')||vstr=='7.0'){ //判断ie6、7浏览器
    	extWidth=Ext.get("relationProcess_id").getWidth();
    }else{
    	extWidth='100%';
    }
	positionGridPanel = new Ext.grid.GridPanel({
				id : "positionGridPanel_id",
				renderTo : 'positionGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 137,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : positionGridStore,
				colModel : positionGridColumnModel,
				bbar : positionGridBbar
			})

}

/**
 * 部门或岗位搜索
 */
function orgOrPosSearch() {
	if (document.getElementById("org").checked) {
		orgGridSearch();
	} else {
		positionGridSearch();
	}

}

/**
 * 部门或岗位搜索条件重置
 */
function orgOrPosReset() {
	if (document.getElementById("org").checked) {
		orgSearchReset();
	} else {
		positionSearchReset();
	}
}

/**
 * 部门搜索
 */
function orgGridSearch() {
	// 部门名称
	var orgName = document.getElementById("orgName_id").value;
	orgGridStore.setBaseParam("searchName", orgName);
	orgGridStore.load({
				params : {
					start : 0,
					limit : pageSize
				}
			});
}
/**
 * 部门搜索重置
 */
function orgSearchReset() {
	document.getElementById("orgName_id").value = ""
}

/**
 * 岗位搜索
 */
function positionGridSearch() {
	// 岗位名称
	var orgName = document.getElementById("posName_id").value;
	positionGridStore.setBaseParam("searchName", orgName);
	positionGridStore.load({
				params : {
					start : 0,
					limit : pageSize
				}
			});
}
/**
 * 岗位搜索重置
 */
function positionSearchReset() {
	// 岗位名称重置
	document.getElementById("posName_id").value = "";
}

/**
 * 组织名称增加超链接 用于Ext Grid
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 * @return {}
 */
function orgNameRenderer(value, cellmeta, record) {
	return "<div  ext:qtip='"
			+ value
			+ "'><a target='_blank' class='a-operation' href='organization.action?orgId="
			+ record.data['id'] + "' >" + value + "</a>";
}

/**
 * 上级组织名称增加超链接 用于Ext Grid
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 * @return {}
 */
function orgPnameRenderer(value, cellmeta, record) {
	return "<div  ext:qtip='"
			+ value
			+ "'><a target='_blank' class='a-operation' href='organization.action?orgId="
			+ record.data['pid'] + "' >" + value + "</a>";
}

/**
 * 岗位名称增加超链接 用于Ext Grid
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 * @return {}
 */
function positionNameRenderer(value, cellmeta, record) {
	return "<div  ext:qtip='"
			+ value
			+ "'><a target='_blank' class='a-operation' href='orgSpecification.action?posId="
			+ record.data['id'] + "' >" + value + "</a>";
}

function updatePassword() {
	var userFormPanel = new Ext.form.FormPanel({
				defaultType : "textfield",
				labelAlign : "right",
				// fileUpload : true,
				bodyStyle:"padding:5px",
				labelWidth : 80,
				baseCls : "x-plain",
				defaults : {
					width : 200
				},
				items : [{
							id : 'originalPassword',
							name : 'originalPassword',
							fieldLabel : i_originalPassword,
							inputType : 'password',
							allowBlank : false,
							blankText : i_TheOriginalPasswordCantForEmpty,
							maxLength : 19,
							maxLengthText : i_passwordLength,
							regex:/^[A-Za-z0-9\~\`\!\@\#\$\%\^\&\*\(\)\_\+\-\=\{\}\[\]\:\"\|\;\'\<\>\?\,\.\/\\]+$/,
							regexText:i_passTip

						}, {
							id : 'newPassword',
							name : 'newPassword',
							fieldLabel : i_theNewPassword,
							inputType : 'password',
							allowBlank : false,
							blankText : i_TheNewPasswordCantForEmpty,
							maxLength : 19,
							maxLengthText : i_passwordLength,
							regex:/^[A-Za-z0-9\~\`\!\@\#\$\%\^\&\*\(\)\_\+\-\=\{\}\[\]\:\"\|\;\'\<\>\?\,\.\/\\]+$/,
							regexText:i_passTip

						}, {
							id : 'surePassword',
							name : 'surePassword',
							fieldLabel : i_confirmPassword,
							inputType : 'password',
							allowBlank : false,
							blankText : i_TwoPasswordInputInconsistency,
							maxLength : 19,
							maxLengthText : i_passwordLength,
							regex:/^[A-Za-z0-9\~\`\!\@\#\$\%\^\&\*\(\)\_\+\-\=\{\}\[\]\:\"\|\;\'\<\>\?\,\.\/\\]+$/,
							regexText:i_passTip

						}]

			})
	var userWindow = new Ext.Window({
		title : i_updatePassword,// 修改密码,
		height : 150,
		width : 320,
		closeAction : "close",
		resizable : false,// 四边和四角改变window的大小
		modal : true,
		items : userFormPanel,
		buttons : [{
			text : i_ok,// 确定
			handler : function() {

				// 原密码
				var originalPassword = userFormPanel.getForm()
						.findField("originalPassword").getValue();
				// 新密码
				var newPassword = userFormPanel.getForm()
						.findField("newPassword").getValue();
				// 确认密码
				var surePassword = userFormPanel.getForm()
						.findField("surePassword").getValue();
				if (newPassword != surePassword) {
					Ext.Msg.alert(i_tip,i_TwoPasswordInputInconsistency);
					return;
				}
				userFormPanel.getForm().submit({
							url : "updatePass.action",// 这里是接收数据的程序
							success : function(_form, _action) {
								var msg = _action.result.msg;
								if (msg == null) {
									Ext.Msg.alert(i_tip, i_AccessServerAnomaly);
								} else if (msg == 1) {
									Ext.Msg.alert(i_tip,
											i_PasswordChangingSuccess);
									userWindow.close();
								} else if (msg == 2) {
									Ext.Msg.alert(i_tip,
											i_OriginalPasswordMistake);
								} else if (msg == 3) {
									Ext.Msg.alert(i_tip, i_AccessServerAnomaly);
								}

							}
						});

			}
		}]
	});
	userWindow.show();
}