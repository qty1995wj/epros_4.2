package epros.draw.visio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

import epros.draw.util.JecnFileUtil;

/**
 * 读取visio模板数据
 * 
 * @author fuzhh 2013-7-12
 * 
 */
public class JecnVisioModeXmlData {
	/** 存储系统图形 对应的visio图形数据 用 , 隔开 */
	public static Map<String, String> visioMap = new HashMap<String, String>();

	/**
	 * 获取文件路径
	 * 
	 * @author fuzhh 2013-7-12
	 * @return 路径
	 */
	private static String getVisioModePath() {
		// 获取文件名称
		String fileName = JecnFileUtil.getVisioModeXmlPath();
		// 配置文件路径
		String path = JecnFileUtil.getSyncPath(fileName);
		return path;
	}

	/**
	 * 获取 visio模板 的dom4j Documet对象
	 * 
	 * @author fuzhh 2013-7-12
	 * @param modePath
	 * @return
	 */
	private static Document getVisioModeDoc() {
		return JecnVisioXmlUtil.getDocumentByXml(getVisioModePath());
	}

	/**
	 * 获取visio模板数据
	 * 
	 * @author fuzhh 2013-7-12
	 */
	public static void getVisioFigureData() {
		Document document = getVisioModeDoc();
		// 获取xml 的根节点
		Element rootElement = JecnVisioXmlUtil.getRootElement(document);
		// 获取根节点下的子节点
		List<Element> figureList = JecnVisioXmlUtil
				.getChildNodesList(rootElement);
		for (Element ele : figureList) {
			// 获取节点名称
			String nodeName = ele.getQualifiedName();
			List<Element> visioFigureList = JecnVisioXmlUtil
					.getChildNodesList(ele);
			String nodeValue = "";
			for (Element figureEle : visioFigureList) {
				// 获取节点内容
				nodeValue = JecnVisioXmlUtil.getNodeValue(figureEle);
			}
			visioMap.put(nodeName, nodeValue);
		}
	}

}
