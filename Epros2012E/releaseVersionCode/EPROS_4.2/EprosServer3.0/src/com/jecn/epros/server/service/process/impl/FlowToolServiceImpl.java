package com.jecn.epros.server.service.process.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.process.JecnSustainToolConnTBean;
import com.jecn.epros.server.bean.process.JecnSustainToolRelatedT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.process.IFlowToolDao;
import com.jecn.epros.server.service.process.IFlowToolService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/*******************************************************************************
 * 支持工具处理层
 * 
 * @author 2012-05-14
 * 
 */
@Transactional
public class FlowToolServiceImpl extends AbsBaseService<JecnFlowSustainTool, Long> implements IFlowToolService {
	private final static Log log = LogFactory.getLog(FlowToolServiceImpl.class);
	private IFlowToolDao flowToolDao;

	public IFlowToolDao getFlowToolDao() {
		return flowToolDao;
	}

	public void setFlowToolDao(IFlowToolDao flowToolDao) {
		this.flowToolDao = flowToolDao;
		this.baseDao = flowToolDao;
	}

	@Override
	public void deleteSustainTools(List<Long> listIds, Long peopleId) throws Exception {
		try {
			Set<Long> setIds = new HashSet<Long>();
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM JECN_FLOW_SUSTAIN_TOOL JFF WHERE JFF.FLOW_SUSTAIN_TOOL_ID in"
						+ JecnCommonSql.getIds(listIds)
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_JECN INNER JOIN JECN_FLOW_SUSTAIN_TOOL JFS ON MY_JECN.FLOW_SUSTAIN_TOOL_ID=JFS.PRE_FLOW_SUSTAIN_TOOL_ID)"
						+ " select distinct FLOW_SUSTAIN_TOOL_ID from MY_JECN";
				List<Object> list = flowToolDao.listNativeSql(sql);

				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select distinct jm.FLOW_SUSTAIN_TOOL_ID from JECN_FLOW_SUSTAIN_TOOL jm"
						+ " connect by prior jm.FLOW_SUSTAIN_TOOL_ID = jm.PRE_FLOW_SUSTAIN_TOOL_ID START WITH jm.FLOW_SUSTAIN_TOOL_ID in "
						+ JecnCommonSql.getIds(listIds);
				List<Object> list = flowToolDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				for (Long id : listIds) {
					setIds.add(id);
				}
			}
			if (setIds.size() > 0) {
				JecnUtil.saveJecnJournals(setIds, 17, 3, peopleId, flowToolDao);
				// 删除支持工具与流程的相关表（临时）
				sql = "delete from JecnSustainToolRelatedT where flowSustainToolId in";
				List<String> listSqls = JecnCommonSql.getSetSqls(sql, setIds);
				for (String delSql : listSqls) {
					flowToolDao.execteHql(delSql);
				}
				// 删除支持工具与流程的相关表
				sql = "delete from JecnSustainToolRelated where flowSustainToolId in";
				listSqls = JecnCommonSql.getSetSqls(sql, setIds);
				for (String delSql : listSqls) {
					flowToolDao.execteHql(delSql);
				}
				// 活动支持工具
				sql = "delete from JecnActiveOnLineBean where toolId in";
				listSqls = JecnCommonSql.getSetSqls(sql, setIds);
				for (String delSql : listSqls) {
					flowToolDao.execteHql(delSql);
				}
				sql = "delete from JecnActiveOnLineTBean where toolId in";
				listSqls = JecnCommonSql.getSetSqls(sql, setIds);
				for (String delSql : listSqls) {
					flowToolDao.execteHql(delSql);
				}
				// 删除支持工具
				sql = "delete from JecnFlowSustainTool where flowSustainToolId in";
				listSqls = JecnCommonSql.getSetSqls(sql, setIds);
				for (String delSql : listSqls) {
					flowToolDao.execteHql(delSql);
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：制度模板子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjects(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 支持工具节点
			jecnTreeBean.setTreeNodeType(TreeNodeType.tool);

			// 支持工具节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 支持工具节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 支持工具节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 支持工具节点父类名称
			jecnTreeBean.setPname("");
			if (obj.length > 3) {
				if (Integer.parseInt(obj[3].toString()) > 0) {
					// 支持工具是否有子节点
					jecnTreeBean.setChildNode(true);
				} else {
					jecnTreeBean.setChildNode(false);
				}
			} else {
				jecnTreeBean.setChildNode(false);
			}
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	public List<JecnTreeBean> getChildSustainTools(Long pId) throws Exception {
		try {
			String sql = "select t.flow_sustain_tool_id,t.flow_sustain_tool_describe,t.pre_flow_sustain_tool_id,"
					+ " (select count(*) from jecn_flow_sustain_tool where pre_flow_sustain_tool_id=t.flow_sustain_tool_id) as count "
					+ " from jecn_flow_sustain_tool t where t.pre_flow_sustain_tool_id=? order by t.pre_flow_sustain_tool_id,t.sort_id,t.flow_sustain_tool_id";
			List<Object[]> list = flowToolDao.listNativeSql(sql, pId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getSustainToolDirsByNameMove(String name, List<Long> listIds) throws Exception {
		try {
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM jecn_flow_sustain_tool JFF WHERE JFF.flow_sustain_tool_id in"
						+ JecnCommonSql.getIds(listIds)
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_JECN INNER JOIN jecn_flow_sustain_tool JFS ON MY_JECN.flow_sustain_tool_id=JFS.pre_flow_sustain_tool_id)"
						+ " select t.flow_sustain_tool_id,t.flow_sustain_tool_describe,t.pre_flow_sustain_tool_id,"
						+ "(select count(*) from jecn_flow_sustain_tool where pre_flow_sustain_tool_id=t.flow_sustain_tool_id)"
						+ " from jecn_flow_sustain_tool t where and t.flow_sustain_tool_describe like ? and t.flow_sustain_tool_id not in (select flow_sustain_tool_id from MY_JECN)"
						+ " order by t.pre_flow_sustain_tool_id,t.sort_id,t.flow_sustain_tool_id";
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select t.flow_sustain_tool_id,t.flow_sustain_tool_describe,t.pre_flow_sustain_tool_id,"
						+ "(select count(*) from jecn_flow_sustain_tool where pre_flow_sustain_tool_id=t.flow_sustain_tool_id)"
						+ " from jecn_flow_sustain_tool t where t.flow_sustain_tool_describe like ?  and t.flow_sustain_tool_id not in (select jm.flow_sustain_tool_id from jecn_flow_sustain_tool jm"
						+ " connect by prior jm.flow_sustain_tool_id = jm.pre_flow_sustain_tool_id START WITH jm.flow_sustain_tool_id in "
						+ JecnCommonSql.getIds(listIds)
						+ " ) order by t.pre_flow_sustain_tool_id,t.sort_id,t.flow_sustain_tool_id";
			}
			List<Object[]> list = flowToolDao.listNativeSql(sql, 0, 20, "%" + name + "%");
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getSustainToolsByName(String name) throws Exception {
		try {
			String sql = "select t.flow_sustain_tool_id,t.flow_sustain_tool_describe,t.pre_flow_sustain_tool_id"
					+ " from jecn_flow_sustain_tool t where t.flow_sustain_tool_describe like ? order by t.pre_flow_sustain_tool_id,t.sort_id,t.flow_sustain_tool_id";
			List<Object[]> list = flowToolDao.listNativeSql(sql, 0, 20, "%" + name + "%");
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void moveSustainTools(List<Long> listIds, Long pId, Long peopleId) throws Exception {
		try {
			String hql = " update JecnFlowSustainTool set preFlowSustainToolId=? where flowSustainToolId in "
					+ JecnCommonSql.getIds(listIds);
			flowToolDao.execteHql(hql, pId);
			JecnUtil.saveJecnJournals(listIds, 17, 4, peopleId, flowToolDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateSortSustainTools(List<JecnTreeDragBean> list, Long pId, Long peopleId) throws Exception {
		try {
			String hql = "from JecnFlowSustainTool where preFlowSustainToolId=?";
			List<JecnFlowSustainTool> listJecnFlowSustainTool = flowToolDao.listHql(hql, pId);
			List<JecnFlowSustainTool> listUpdate = new ArrayList<JecnFlowSustainTool>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (JecnFlowSustainTool jecnFlowSustainTool : listJecnFlowSustainTool) {
					if (jecnTreeDragBean.getId().equals(jecnFlowSustainTool.getFlowSustainToolId())) {
						if (jecnFlowSustainTool.getSortId() == null
								|| (jecnTreeDragBean.getSortId() != jecnFlowSustainTool.getSortId().intValue())) {
							jecnFlowSustainTool.setSortId(jecnTreeDragBean.getSortId());
							listUpdate.add(jecnFlowSustainTool);
							break;
						}
					}
				}
			}

			for (JecnFlowSustainTool jecnFlowSustainTool : listUpdate) {
				flowToolDao.update(jecnFlowSustainTool);
				JecnUtil.saveJecnJournal(jecnFlowSustainTool.getFlowSustainToolId(), jecnFlowSustainTool
						.getFlowSustainToolShow(), 17, 5, peopleId, flowToolDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid) throws Exception {
		try {
			String hql = "select count(*) from JecnFlowSustainTool where flowSustainToolShow=? and preFlowSustainToolId=? and flowSustainToolId<>?";
			return flowToolDao.countAllByParamsHql(hql, newName, pid, id) > 0 ? true : false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 树加载，获得所有支持工具
	public List<JecnTreeBean> getSustainTools() throws Exception {
		try {
			String sql = "select t.flow_sustain_tool_id,t.flow_sustain_tool_describe,t.pre_flow_sustain_tool_id,"
					+ " (select count(*) from jecn_flow_sustain_tool where pre_flow_sustain_tool_id=t.flow_sustain_tool_id) as count "
					+ " from jecn_flow_sustain_tool t order by t.pre_flow_sustain_tool_id,t.sort_id,t.flow_sustain_tool_id";

			List<Object[]> list = flowToolDao.listNativeSql(sql);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<Long> getFlowToolIds(Long flowId) throws Exception {
		try {
			String sql = "select tr.FLOW_SUSTAIN_TOOL_ID from JECN_FLOW_TOOL_RELATED_T tr where tr.FLOW_ID = ?";
			List<Long> listIds = flowToolDao.listNativeSql(sql, flowId);
			return listIds;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getSustainToolsByIDs(List<Long> listIds) throws Exception {
		try {
			List<Object[]> list = new ArrayList<Object[]>();
			if (listIds.size() > 0) {
				String sql = "select t.flow_sustain_tool_id,t.flow_sustain_tool_describe,t.pre_flow_sustain_tool_id"
						+ " from jecn_flow_sustain_tool t where t.flow_sustain_tool_id in "
						+ JecnCommonSql.getIds(listIds)
						+ " order by t.pre_flow_sustain_tool_id,t.sort_id,t.flow_sustain_tool_id";
				list = flowToolDao.listNativeSql(sql);
			}
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public List<JecnTreeBean> getSustainToolsByFlowId(Long flowId) throws Exception {
		String sql = "SELECT T.FLOW_SUSTAIN_TOOL_ID,T.FLOW_SUSTAIN_TOOL_DESCRIBE,T.PRE_FLOW_SUSTAIN_TOOL_ID"
				+ "            FROM JECN_FLOW_SUSTAIN_TOOL T,JECN_FLOW_TOOL_RELATED_T TR WHERE T.FLOW_SUSTAIN_TOOL_ID = TR.FLOW_SUSTAIN_TOOL_ID"
				+ "            AND TR.FLOW_ID=" + flowId
				+ "           ORDER BY T.PRE_FLOW_SUSTAIN_TOOL_ID,T.SORT_ID,T.FLOW_SUSTAIN_TOOL_ID";
		List<Object[]> list = flowToolDao.listNativeSql(sql);
		return findByListObjects(list);
	}

	@Override
	public void addFlowToolRelated(List<Long> listIds, Long flowId) throws Exception {
		try {
			// 删除原来流程ID对应的支持工具的关联关系
			String sql = "delete from JECN_FLOW_TOOL_RELATED_T where FLOW_ID = " + flowId;
			flowToolDao.execteNative(sql);
			// 添加新的关联关系
			List<JecnSustainToolRelatedT> listRelated = new ArrayList<JecnSustainToolRelatedT>();
			for (Long toolId : listIds) {
				JecnSustainToolRelatedT jecnSustainToolRelated = new JecnSustainToolRelatedT();
				jecnSustainToolRelated.setFlowId(flowId);
				jecnSustainToolRelated.setFlowSustainToolId(toolId);
				listRelated.add(jecnSustainToolRelated);
			}

			for (JecnSustainToolRelatedT sustainToolRelated : listRelated) {
				flowToolDao.getSession().save(sustainToolRelated);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/***
	 * 根据关联ID、关联类型获取与支持工具关联临时表数据
	 * 
	 * @param relatedId关联ID
	 * @param relatedType
	 *            关联类型
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getSustainToolConnTList(Long relatedId, int relatedType) throws Exception {
		String hql = "select SUSTAIN_TOOL_ID from JECN_SUSTAIN_TOOL_CONN_T where RELATED_ID = " + relatedId
				+ " and RELATED_TYPE = " + relatedType;
		return flowToolDao.listNativeSql(hql, relatedId, relatedType);
	}

	/***
	 * 添加"与支持工具关联临时表"的数据
	 * 
	 * @param listIds
	 * @param relatedId
	 * @param relatedType
	 * @throws Exception
	 */
	@Override
	public void addSustainToolConnTRelated(List<Long> listIds, Long relatedId, int relatedType) throws Exception {
		try {
			// 删除原来关联ID、关联类型对应的支持工具的关联关系
			String sql = "delete from JECN_SUSTAIN_TOOL_CONN_T where RELATED_ID = ? and RELATED_TYPE = ?";
			flowToolDao.execteNative(sql, relatedId, relatedType);
			// 添加新的关联关系
			List<JecnSustainToolConnTBean> listRelated = new ArrayList<JecnSustainToolConnTBean>();
			for (Long toolId : listIds) {
				JecnSustainToolConnTBean jecnSustainToolConnTBean = new JecnSustainToolConnTBean();
				jecnSustainToolConnTBean.setRelatedId(relatedId);
				jecnSustainToolConnTBean.setRelatedType(relatedType);
				jecnSustainToolConnTBean.setSustainToolId(toolId);
				listRelated.add(jecnSustainToolConnTBean);
			}
			for (JecnSustainToolConnTBean sustainToolConnTBean : listRelated) {
				flowToolDao.getSession().save(sustainToolConnTBean);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public Long saveTool(JecnFlowSustainTool jecnFlowSustainTool, Long peopleId) throws Exception {
		flowToolDao.save(jecnFlowSustainTool);
		JecnUtil.saveJecnJournal(jecnFlowSustainTool.getFlowSustainToolId(), jecnFlowSustainTool
				.getFlowSustainToolShow(), 17, 1, peopleId, flowToolDao);
		return jecnFlowSustainTool.getFlowSustainToolId();
	}

	@Override
	public void updateTool(JecnFlowSustainTool jecnFlowSustainTool, Long peopleId) throws Exception {
		flowToolDao.update(jecnFlowSustainTool);
		JecnUtil.saveJecnJournal(jecnFlowSustainTool.getFlowSustainToolId(), jecnFlowSustainTool
				.getFlowSustainToolShow(), 17, 2, peopleId, flowToolDao);
	}

}
