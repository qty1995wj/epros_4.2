package com.jecn.epros.server.connector.iflytek;

public class IflytekContent {
	/** 流程任务id */
	private String flowId;
	/** 标题 */
	private String requestName;
	/** 流程名称 */
	private String workflowName;
	/** 步骤名称（节点名称） */
	private String nodeName;
	/** PC地址 */
	private String pcUrl;
	/** APP地址 */
	private String appUrl;
	/** 创建人（对应域账号） */
	private String creator;
	/** 创建日期时间(yyyy-MM-dd HH:ss:mm) */
	private String createDateTime;
	/** 接收人（域账号），多个用户使用英文逗号隔开 */
	private String receiver;
	/** 类型编码(传001) */
	private String msgTypeCode;
	/** 是否可以批量通过(0:否1:是) */
	private String batchStatus = "0";
	/** 流程分类 */
	private String workflowtype;
	/** 流程备注信息 */
	private String requestnamenew;
	/** 是否抄送单据(0:否1:是) */
	private String copy = "0";
	/** 流程二级分类 */
	private String twoLevelClass;

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getPcUrl() {
		return pcUrl;
	}

	public void setPcUrl(String pcUrl) {
		this.pcUrl = pcUrl;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getMsgTypeCode() {
		return msgTypeCode;
	}

	public void setMsgTypeCode(String msgTypeCode) {
		this.msgTypeCode = msgTypeCode;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getWorkflowtype() {
		return workflowtype;
	}

	public void setWorkflowtype(String workflowtype) {
		this.workflowtype = workflowtype;
	}

	public String getRequestnamenew() {
		return requestnamenew;
	}

	public void setRequestnamenew(String requestnamenew) {
		this.requestnamenew = requestnamenew;
	}

	public String getCopy() {
		return copy;
	}

	public void setCopy(String copy) {
		this.copy = copy;
	}

	public String getTwoLevelClass() {
		return twoLevelClass;
	}

	public void setTwoLevelClass(String twoLevelClass) {
		this.twoLevelClass = twoLevelClass;
	}

	@Override
	public String toString() {
		return "IflytekContent [flowId=" + flowId + ", requestName=" + requestName + ", workflowName="
				+ workflowName + ", nodeName=" + nodeName + ", pcUrl=" + pcUrl + ", appUrl=" + appUrl
				+ ", creator=" + creator + ", createDateTime=" + createDateTime + "]";
	}
}
