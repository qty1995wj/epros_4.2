package epros.designer.gui.system.config.ui.property.type;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 权限配置
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPubPanel extends JecnAbstractPropertyBasePanel {
	/** 流程 (0：秘密 1：公开) */
	private JecnPubItemPanel pubPartMapPanel = null;
	/** 流程地图 (0：秘密 1：公开) */
	private JecnPubItemPanel pubTotalMapPanel = null;
	/** 组织 (0：秘密 1：公开) */
	private JecnPubItemPanel pubOrgPanel = null;
	/** 岗位 (0：秘密 1：公开) */
	private JecnPubItemPanel pubPosPanel = null;
	/** 制度 (0：秘密 1：公开) */
	private JecnPubItemPanel pubRulePanel = null;
	/** 标准 (0：秘密 1：公开) */
	private JecnPubItemPanel pubStandPanel = null;
	/** 文件 (0：秘密 1：公开) */
	private JecnPubItemPanel pubFilePanel = null;

	/** 权限面板 （流程、制度、文件等公开，秘密权限设置面板） */
	private JecnPanel permissePanel = null;
	/** 权限面板 （流程、制度、流程地图相关文件公开，秘密权限设置面板） */
	private JecnPanel permisseFlowMapRelateFilePanel = null;
	private JecnPanel permisseFlowRelateFilePanel = null;
	private JecnPanel permisseRuleRelateFilePanel = null;

	/** 流程地图相关制度 (0：秘密 1：公开) */
	private JecnPubItemPanel permissionFlowMapRelateRulePanel = null;
	/** 流程地图相关文件 (0：秘密 1：公开) */
	private JecnPubItemPanel permissionFlowMapRelateFilePanel = null;
	/** 流程相关标准0：秘密 1：公开) */
	private JecnPubItemPanel permissionFlowRelateStandardPanel = null;
	/** 流程相关制度 (0：秘密 1：公开) */
	private JecnPubItemPanel permissionFlowRelateRulePanel = null;
	/** 流程相关文件 (0：秘密 1：公开) */
	private JecnPubItemPanel permissionFlowRelateFilePanel = null;
	/** 制度相关标准 (0：秘密 1：公开) */
	private JecnPubItemPanel permissionRuleRelateStandardPanel = null;
	/** 制度相关文件 (0：秘密 1：公开) */
	private JecnPubItemPanel permissionRuleRelateFilePanel = null;

	public JecnPubPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		initComponents();
		initLayout();
	}

	private void initComponents() {

		permissePanel = new JecnPanel();
		permissePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		permissePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("companyFileSet")));//公司内文件权限设置

		permisseFlowMapRelateFilePanel = new JecnPanel();
		permisseFlowMapRelateFilePanel.setBorder(BorderFactory
				.createTitledBorder(JecnProperties.getValue("flowMapSetFile")));//针对可查阅的流程架构，设置相关文件权限

		permisseFlowRelateFilePanel = new JecnPanel();
		permisseFlowRelateFilePanel.setBorder(BorderFactory
				.createTitledBorder(JecnProperties.getValue("flowSetFile")));//针对可查阅的流程，设置相关文件权限

		permisseRuleRelateFilePanel = new JecnPanel();
		permisseRuleRelateFilePanel.setBorder(BorderFactory
				.createTitledBorder(JecnProperties.getValue("ruleSetFile")));//针对可查阅的制度，设置相关文件权限
		// 流程 (0：秘密 1：公开)
		pubPartMapPanel = new JecnPubItemPanel(JecnProperties
				.getValue("processC"));

		// 流程地图 (0：秘密 1：公开)
		pubTotalMapPanel = new JecnPubItemPanel(JecnProperties
				.getValue("processMapC"));

		// 组织 (0：秘密 1：公开)
		pubOrgPanel = new JecnPubItemPanel(JecnProperties
				.getValue("organizationC"));
		// 岗位 (0：秘密 1：公开)
		pubPosPanel = new JecnPubItemPanel(JecnProperties.getValue("positionC"));
		// 制度 (0：秘密 1：公开)
		pubRulePanel = new JecnPubItemPanel(JecnProperties.getValue("ruleC"));
		// 标准 (0：秘密 1：公开)
		pubStandPanel = new JecnPubItemPanel(JecnProperties
				.getValue("standardC"));
		// 文件 (0：秘密 1：公开)
		pubFilePanel = new JecnPubItemPanel(JecnProperties.getValue("fileC"));
		// 流程 (0：秘密 1：公开)
		permissionFlowMapRelateRulePanel = new JecnPubItemPanel(JecnProperties
				.getValue("ruleC"));
		// 流程 (0：秘密 1：公开)
		permissionFlowMapRelateFilePanel = new JecnPubItemPanel(JecnProperties
				.getValue("fileC"));
		// 流程 (0：秘密 1：公开)
		permissionFlowRelateStandardPanel = new JecnPubItemPanel(JecnProperties
				.getValue("standardC"));
		// 流程 (0：秘密 1：公开)
		permissionFlowRelateRulePanel = new JecnPubItemPanel(JecnProperties
				.getValue("ruleC"));
		// 流程 (0：秘密 1：公开)
		permissionFlowRelateFilePanel = new JecnPubItemPanel(JecnProperties
				.getValue("fileC"));
		// 流程 (0：秘密 1：公开)
		permissionRuleRelateStandardPanel = new JecnPubItemPanel(JecnProperties
				.getValue("standardC"));
		// 流程 (0：秘密 1：公开)
		permissionRuleRelateFilePanel = new JecnPubItemPanel(JecnProperties
				.getValue("fileC"));

		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new GridBagLayout());
		//判断是否是标准版，如果是标准版将其隐藏
		if(JecnConstants.isEPS()){
			permissionFlowMapRelateRulePanel.setVisible(false);
			permissionFlowRelateStandardPanel.setVisible(false);
			permissionFlowRelateRulePanel.setVisible(false);
			permisseRuleRelateFilePanel.setVisible(false);
		}
	}

	private void initLayout() {
		initPermissePanelLayout();
		initPermisseRelateFilePanel();
		GridBagConstraints c = null;
		// 添加权限面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(permissePanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(permisseFlowMapRelateFilePanel, c);

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(permisseFlowRelateFilePanel, c);

		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(permisseRuleRelateFilePanel, c);

		// 空白
		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, JecnUIUtil
						.getInsets0(), 0, 0);
		this.add(new JLabel(), c);
	}

	private void initPermissePanelLayout() {
		Insets insets5555 = new Insets(0, 5, 0, 5);
		// 流程地图 (0：秘密 1：公开)
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permissePanel.add(pubTotalMapPanel, c);
		// 流程 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permissePanel.add(pubPartMapPanel, c);

		// 组织 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permissePanel.add(pubOrgPanel, c);
		// 岗位 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permissePanel.add(pubPosPanel, c);
		if (JecnConstants.versionType==99||JecnConstants.versionType==100) {
			// 制度 (0：秘密 1：公开)
			c = new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets5555, 0, 0);
			permissePanel.add(pubRulePanel, c);
			// 标准 (0：秘密 1：公开)
			c = new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets5555, 0, 0);
			permissePanel.add(pubStandPanel, c);
		}

		// 文件 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permissePanel.add(pubFilePanel, c);
	}

	private void initPermisseRelateFilePanel() {
		Insets insets5555 = new Insets(0, 5, 0, 5);

		// 流程地图 (0：秘密 1：公开)
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permisseFlowMapRelateFilePanel.add(permissionFlowMapRelateRulePanel, c);
		// 流程 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permisseFlowMapRelateFilePanel.add(permissionFlowMapRelateFilePanel, c);

		// 流程地图 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permisseFlowRelateFilePanel.add(permissionFlowRelateStandardPanel, c);
		// 流程 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permisseFlowRelateFilePanel.add(permissionFlowRelateRulePanel, c);
		// 流程地图 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permisseFlowRelateFilePanel.add(permissionFlowRelateFilePanel, c);
		// 流程 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permisseRuleRelateFilePanel.add(permissionRuleRelateStandardPanel, c);
		// 流程地图 (0：秘密 1：公开)
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets5555, 0, 0);
		permisseRuleRelateFilePanel.add(permissionRuleRelateFilePanel, c);

	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {

		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean
				.getOtherItemList()) {// 非表数据
			// 唯一标识
			String mark = itemBean.getMark();

			if (ConfigItemPartMapMark.pubPartMap.toString().equals(mark)) { // 流程(0：秘密1：公开)
				pubPartMapPanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.pubTotalMap.toString()
					.equals(mark)) {// 流程地图(0：秘密1：公开)
				pubTotalMapPanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.pubOrg.toString().equals(mark)) {// 组织(0：秘密1：公开)
				pubOrgPanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.pubPos.toString().equals(mark)) {// 岗位(0：秘密1：公开)
				pubPosPanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.pubRule.toString().equals(mark)) {// 制度(0：秘密1：公开)
				pubRulePanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.pubStand.toString().equals(mark)) { // 标准(0：秘密1：公开)
				pubStandPanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.pubFile.toString().equals(mark)) {// 文件(0：秘密1：公开)
				pubFilePanel.initData(itemBean);
			}

			else if (ConfigItemPartMapMark.permissionFlowMapRelateRule
					.toString().equals(mark)) {// 流程地图相关制度(0：秘密1：公开)
				permissionFlowMapRelateRulePanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.permissionFlowMapRelateFile
					.toString().equals(mark)) {// 流程地图相关文件(0：秘密1：公开)
				permissionFlowMapRelateFilePanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.permissionFlowRelateStandard
					.toString().equals(mark)) {// 流程图相关标准(0：秘密1：公开)
				permissionFlowRelateStandardPanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.permissionFlowRelateRule
					.toString().equals(mark)) {// 流程图相关制度(0：秘密1：公开)
				permissionFlowRelateRulePanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.permissionFlowRelateFile
					.toString().equals(mark)) {// 流程图相关文件(0：秘密1：公开)
				permissionFlowRelateFilePanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.permissionRuleRelateStandard
					.toString().equals(mark)) {// 制度相关标准(0：秘密1：公开)
				permissionRuleRelateStandardPanel.initData(itemBean);
			} else if (ConfigItemPartMapMark.permissionRuleRelateFile
					.toString().equals(mark)) {// 制度相关文件(0：秘密1：公开)
				permissionRuleRelateFilePanel.initData(itemBean);
			}
		}
	}

	@Override
	public boolean check() {
		return true;
	}
}
