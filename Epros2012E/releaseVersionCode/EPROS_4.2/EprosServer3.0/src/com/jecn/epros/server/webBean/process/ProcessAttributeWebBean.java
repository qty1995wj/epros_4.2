package com.jecn.epros.server.webBean.process;

import java.io.Serializable;

/**
 * 流程属性
 * @author Administrator
 *
 */
public class ProcessAttributeWebBean implements Serializable{
	/**流程ID*/
	private Long flowId;
	/**流程名称*/
	private String flowName;
	/**所属流程地图ID*/
	private Long perFlowId;
	/**所属流程地图名称*/
	private String perFlowName;
	/**流程地图类别*/
	private String flowTypeName;
	/**责任部门Id*/
	private Long orgId;
	/**责任部门*/
	private String orgName;
	/**0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)*/
	private int typeResPeople;//0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
	/**流程责任人Id*/
	private Long resPeopleId;//流程责任人Id
	/**流程责任人*/
	private String resPeopleName;//流程责任人
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public Long getPerFlowId() {
		return perFlowId;
	}
	public void setPerFlowId(Long perFlowId) {
		this.perFlowId = perFlowId;
	}
	public String getPerFlowName() {
		return perFlowName;
	}
	public void setPerFlowName(String perFlowName) {
		this.perFlowName = perFlowName;
	}
	public String getFlowTypeName() {
		return flowTypeName;
	}
	public void setFlowTypeName(String flowTypeName) {
		this.flowTypeName = flowTypeName;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public int getTypeResPeople() {
		return typeResPeople;
	}
	public void setTypeResPeople(int typeResPeople) {
		this.typeResPeople = typeResPeople;
	}
	public Long getResPeopleId() {
		return resPeopleId;
	}
	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}
	public String getResPeopleName() {
		return resPeopleName;
	}
	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}
}
