package epros.designer.gui.integration.internalControl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 内控指引知识库树菜单
 * 
 * @author Administrator
 * 
 */
public class ControlGuideTreeMenu extends JPopupMenu {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(ControlGuideTreeMenu.class);

	private JecnTree jTree = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;
	/** 单选节点 */
	private JecnTreeNode selectNode = null;
	/** 创建目录 */
	private JMenuItem createDir;
	/** 创建条款 */
	private JMenuItem createClause;
	/** 重命名 */
	private JMenuItem reName;
	/** 刷新 */
	private JMenuItem refresh;
	/** 删除 */
	private JMenuItem deleteMenu;
	/** 节点移动 */
	private JMenuItem treeMove;
	/** 节点排序 */
	private JMenuItem treeSort;
	/** 条款编辑 */
	private JMenuItem updateClause;
	/** 内控指引知识库管理框 */
	@SuppressWarnings("unused")
	private ControlGuideManageDialog controlGuideManageDialog = null;
	/** 内控指引知识库选择框 */
	@SuppressWarnings("unused")
	private ControlGuideChooseDialog controlGuideChooseDialog = null;

	public ControlGuideTreeMenu(JecnTree jTree, ControlGuideManageDialog controlGuideManageDialog) {
		this.jTree = jTree;
		this.controlGuideManageDialog = controlGuideManageDialog;
		initCompotents();
	}

	public ControlGuideTreeMenu(JecnTree jTree, ControlGuideChooseDialog controlGuideChooseDialog) {
		this.jTree = jTree;
		this.controlGuideChooseDialog = controlGuideChooseDialog;
		initCompotents();
	}

	public ControlGuideTreeMenu() {
	}

	/**
	 * 初始化组件
	 */
	public void initCompotents() {
		createDir = new JMenuItem(JecnProperties.getValue("addDir"), new ImageIcon("images/menuImage/newRuleDir.gif"));
		createClause = new JMenuItem(JecnProperties.getValue("createClause"));
		reName = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon("images/menuImage/rename.gif"));
		refresh = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon("images/menuImage/refresh.gif"));
		deleteMenu = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon("images/menuImage/delete.gif"));
		// 节点移动
		treeMove = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon("images/menuImage/nodeMove.gif"));
		// 节点排序
		treeSort = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon("images/menuImage/nodeSort.gif"));
		updateClause = new JMenuItem(JecnProperties.getValue("editClause"));

		this.add(createDir);
		this.add(createClause);
		this.add(reName);
		this.add(refresh);
		this.add(deleteMenu);
		this.add(treeMove);
		this.add(treeSort);
		this.add(updateClause);

		// 创建目录
		createDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createDirPerformed();
			}
		});
		// 创建条款
		createClause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createClausePerformed();
			}
		});
		// 刷新
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refurbishPerformed();
			}
		});
		// 重命名
		reName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reNamePerformed();
			}
		});
		// 删除
		deleteMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteMenuPerformed();
			}
		});
		// 节点排序
		treeSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeSortAction();
			}
		});
		// 节点移动
		treeMove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeMoveAction();
			}
		});
		// 条款编辑
		updateClause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateClause();
			}
		});

	}

	/**
	 * 初始化
	 */
	public void init() {
		createDir.setVisible(false);
		createClause.setVisible(false);
		reName.setVisible(false);
		refresh.setVisible(false);
		deleteMenu.setVisible(false);
		treeMove.setVisible(false);
		treeSort.setVisible(false);
		updateClause.setVisible(false);
	}

	/**
	 * 编辑条款
	 */
	private void updateClause() {
		if (selectNode == null) {
			return;
		}
		EditClauseDialog editClauseDialog = new EditClauseDialog(selectNode, jTree);
		editClauseDialog.setVisible(true);
	}

	/**
	 * 创建目录
	 */
	private void createDirPerformed() {
		if (selectNode == null) {
			return;
		}
		AddControlGuideDir addControlGuideDir = new AddControlGuideDir(selectNode, jTree);
		addControlGuideDir.setVisible(true);
	}

	/**
	 * 创建条款
	 */
	private void createClausePerformed() {
		if (selectNode == null) {
			return;
		}
		AddControlGuideClauseDialog addControlGuideClauseDialog = new AddControlGuideClauseDialog(selectNode, jTree);
		addControlGuideClauseDialog.setVisible(true);
	}

	/**
	 * 刷新
	 */
	public void refurbishPerformed() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getControlGuideAction().getChildJecnControlGuides(id,
					JecnConstants.projectId);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("ControlGuideTreeMenu refurbishPerformed is error", ex);
		}
	}

	/**
	 * 重命名
	 */
	private void reNamePerformed() {
		EditControlGuideNameDialog editControlGuideNameDialog = new EditControlGuideNameDialog(selectNode, jTree);
		editControlGuideNameDialog.setVisible(true);
	}

	/**
	 * 删除
	 */
	private void deleteMenuPerformed() {
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		if (listNode.size() == 0) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode node : listNode) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		try {
			ConnectionPool.getControlGuideAction().deleteControlGuides(listIds, JecnUtil.getUserId());
			// 删除节点
			JecnTreeCommon.removeNodes(jTree, listNode);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("deleteSuccess"));
		} catch (Exception e) {
			log.error("ControlGuideTreeMenu deleteMenuPerformed is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("del_control_error"));
		}
		ControlGuideManageDialog.guideNameText.setText("");
		ControlGuideManageDialog.guideNameText.setEditable(false);
		ControlGuideManageDialog.guideNameText.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ControlGuideManageDialog.controlGuideContent.setText("");
		ControlGuideManageDialog.promptLab.setText("");
		ControlGuideManageDialog.controlGuideContent.setEditable(false);
		ControlGuideManageDialog.controlGuideContent.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ControlGuideManageDialog.saveButton.setEnabled(false);
		ControlGuideManageDialog.recoverButton.setEnabled(false);
	}

	/**
	 * 排序
	 */
	private void treeSortAction() {
		ControlGuideSortDailog controlGuideSortDailog = new ControlGuideSortDailog(selectNode, jTree);
		controlGuideSortDailog.setVisible(true);
	}

	/**
	 * 节点移动
	 */
	private void treeMoveAction() {
		if (!JecnDesignerCommon.validateRepeatNodesName(listNode)) {
			return;
		}
		ControlGuideNodeMoveDialog controlGuideNodeMoveDialog = new ControlGuideNodeMoveDialog(listNode, jTree);
		controlGuideNodeMoveDialog.setFocusableWindowState(true);
		controlGuideNodeMoveDialog.setVisible(true);
	}

	public List<JecnTreeNode> getListNode() {
		return listNode;
	}

	public void setListNode(List<JecnTreeNode> listNode) {
		this.listNode = listNode;
		if (listNode.size() == 1) {
			this.selectNode = listNode.get(0);
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case innerControlRoot:
			refresh.setVisible(true);
			// if (JecnCommon.isAdmin()) {
			createDir.setVisible(true);
			treeSort.setVisible(true);
			// }
			break;
		// 目录节点
		case innerControlDir:
			refresh.setVisible(true);
			// int authTypeFileDir = JecnTreeCommon.isAuthNode(selectNode);
			// if (authTypeFileDir == 1 || authTypeFileDir == 2) {
			createDir.setVisible(true);
			createClause.setVisible(true);
			treeSort.setVisible(true);
			// if (authTypeFileDir == 2) {
			reName.setVisible(true);
			deleteMenu.setVisible(true);
			treeMove.setVisible(true);
			// }
			// }
			break;
		// 内控条款
		case innerControlClause:
			// int authTypeFile = JecnTreeCommon.isAuthNode(selectNode);
			// if (authTypeFile == 2) {
			deleteMenu.setVisible(true);
			treeMove.setVisible(true);
			// }
			HighEfficiencyJecnControlGuideTree highEfficiencyJecnControlGuideTree = (HighEfficiencyJecnControlGuideTree) jTree;
			if (highEfficiencyJecnControlGuideTree.getIsControlGuideMange() == 1) {
				updateClause.setVisible(true);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		if (JecnTreeCommon.isAuthNodes(listNode)) {
			// 删除
			deleteMenu.setVisible(true);
			// 移动
			treeMove.setVisible(true);
		}

	}

}
