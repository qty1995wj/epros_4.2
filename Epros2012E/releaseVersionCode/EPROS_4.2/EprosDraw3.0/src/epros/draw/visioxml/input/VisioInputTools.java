package epros.draw.visioxml.input;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import visioxml.constant.Constants.Align;
import visioxml.constant.Constants.FillPattern;
import visioxml.constant.Constants.FontStyle;
import visioxml.constant.Constants.LinePattern;
import visioxml.constant.Constants.LineRoute;
import visioxml.constant.Constants.Valign;
import visioxml.util.Unit;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 导入visio 属性配置文件
 * 
 * @author admin
 * 
 */
public class VisioInputTools {
	private static final Log log = LogFactory.getLog(VisioInputTools.class);

	/**
	 * 英寸转像素
	 * 
	 * @param feet
	 * @return
	 */
	public static double feetToPx(String feet) {
		return Unit.feetToPX(Double.valueOf(feet));
	}

	/**
	 * X偏移
	 * 
	 * @param feet
	 * @return
	 */
	public static double changeX(String feet, double figureWidth) {
		return feetToPx(feet) - (figureWidth / 2);
	}

	/**
	 * X偏移
	 * 
	 * @param feet
	 * @return
	 */
	public static double changeX1D(String feet) {
		return feetToPx(feet);
	}

	/**
	 * Y偏移
	 * 
	 * @param feet
	 * @return
	 */
	public static double changeY1D(String feet, double panelHeight) {
		return panelHeight - feetToPx(feet);
	}

	/**
	 * Y偏移
	 * 
	 * @param feet
	 * @return
	 */
	public static double changeY(String feet, double panelHeight, double figureHeight) {
		return panelHeight - feetToPx(feet) - (figureHeight / 2);
	}

	/**
	 * 图形4个连接点转换
	 * 
	 * @param route
	 * @return
	 */
	public static EditPointType converLineRoute(LineRoute route) {
		switch (route) {
		case TOP:
			return EditPointType.top;
		case LEFT:
			return EditPointType.left;
		case RIGHT:
			return EditPointType.right;
		case BOTTOM:
			return EditPointType.bottom;
		default:
			log.error("Visio图形四个连接点转换异常,route=" + route.getValue());
			return null;
		}
	}

	/**
	 * 转换visio中的线类型 0：实线；1：虚线；2：点线；3：双线
	 * 
	 * @param pattern
	 * @return
	 */
	public static int converLinePattern(LinePattern pattern) {
		switch (pattern) {
		case DEFAULT:
			return 0;
		case LINE_02:// 虚线
			return 1;
		case LINE_09://
			return 2;
		case LINE_03:
			return 3;
		default:
			return 0;
		}
	}

	/**
	 * 普通、加粗BOLD、斜线ITALIC） 默认普通样式PLAIN（0） ，1 加粗
	 * 
	 * @param style
	 * @return
	 */
	public static int converFontStyle(FontStyle style) {
		switch (style) {
		case DEFAULT:
			return 0;
		case JIACHU:
			return 1;
		case QINXIE:
			return 2;
		default:
			return 0;
		}
	}

	/**
	 * 转换字体位置 字体位置（0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下），默认正中4
	 * 
	 * @return
	 */
	public static int converFontPosition(Align align, Valign valign) {
		if (Align.LEFT == align && Valign.TOP == valign) {// 左上
			return 0;
		} else if (Align.CENTER == align && Valign.TOP == valign) {// 正上
			return 1;
		} else if (Align.RIGHT == align && Valign.TOP == valign) {// 右上
			return 2;
		} else if (Align.LEFT == align && Valign.MIDDLE == valign) { // 左中
			return 3;
		} else if (Align.CENTER == align && Valign.MIDDLE == valign) {// 正中
			return 4;
		} else if (Align.RIGHT == align && Valign.MIDDLE == valign) {// 右中
			return 5;
		} else if (Align.LEFT == align && Valign.BOTTOM == valign) { // 左下
			return 6;
		} else if (Align.CENTER == align && Valign.BOTTOM == valign) {// 正下
			return 7;
		} else if (Align.RIGHT == align && Valign.BOTTOM == valign) {// 右下
			return 8;
		}
		return 4;
	}

	/**
	 * 转换填充方式
	 * 
	 * @param fillPattern
	 * @return
	 */
	public static FillType coverFillPattern(FillPattern fillPattern) {
		switch (fillPattern) {
		case _00:
			return FillType.none;
		case _01:
			return FillType.one;
		default:
			return FillType.two;
		}
	}

	/**
	 * 转换渐变方向
	 * 
	 * @param fillPattern
	 * @return
	 */
	public static FillColorChangType coverFillColorChangType(FillPattern fillPattern) {
		switch (fillPattern) {
		case _39:
			return FillColorChangType.topTilt;
		case _25:
			return FillColorChangType.bottom;
		case _27:
			return FillColorChangType.vertical;
		case _30:
			return FillColorChangType.horizontal;
		default:
			return FillColorChangType.topTilt;
		}
	}

	/**
	 * 判断是否是数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		for (int i = str.length(); --i >= 0;) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 正则判断是否是活动编号
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isactivityNum(String str) {
		boolean flag = false;
		Pattern pattern = Pattern.compile("\\w+\\-\\d+");
		flag = pattern.matcher(str).matches();
		if (!flag) {
			pattern = Pattern.compile("[\u4e00-\u9fa5]+\\-\\d+");
			flag = pattern.matcher(str).matches();
		}
		if (!flag) {
			pattern = Pattern.compile("\\d+");
			flag = pattern.matcher(str).matches();
		}
		return flag;
	}

	/**
	 * 匹配出活动编号
	 * 
	 * @param str
	 * @return
	 */
	public static String getActivityNum(String str) {
		Pattern pattern = Pattern.compile("\\w+\\-\\d+");
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()) {
			return matcher.group();
		}
		pattern = Pattern.compile("[\u4e00-\u9fa5]+\\-\\d+");
		matcher = pattern.matcher(str);
		if (matcher.find()) {
			return matcher.group();
		}
		pattern = Pattern.compile("\\d+");
		matcher = pattern.matcher(str);
		if (matcher.find()) {
			return matcher.group();
		}
		return "";
	}

	/**
	 * 判断是有中文
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isChineseChar(String str) {
		if (str == null || "".equals(str.trim())) {
			return false;
		}
		boolean temp = false;
		Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
		Matcher m = p.matcher(str);
		if (m.find()) {
			temp = true;
		}
		return temp;
	}

	/**
	 * double 转换为Int 四舍五入
	 * 
	 * @param d
	 * @return
	 */
	public static int DoubleToInt(double d) {
		return Integer.parseInt(new java.text.DecimalFormat("0").format(d));
	}

	/**
	 * 设置连接线的开始图形和结束图形
	 * 
	 * @param panleList
	 *            List<JecnBaseFlowElementPanel>XML读取的所有图形元素
	 * @param manLine
	 *            XML读取的当前曼哈顿线段
	 */
	public static void setManLineRefFigure(List<JecnBaseFlowElementPanel> panleList, JecnBaseManhattanLinePanel manLine) {
		if (panleList == null || manLine == null) {
			return;
		}
		JecnManhattanLineData lineData = manLine.getFlowElementData();
		if (lineData.getStartFigureUUID() == null || lineData.getEndFigureUUID() == null) {
			return;
		}
		for (JecnBaseFlowElementPanel flowElementPanel : panleList) {
			if (!(flowElementPanel instanceof JecnBaseFigurePanel)) {
				continue;
			}
			JecnFigureData figureData = (JecnFigureData) flowElementPanel.getFlowElementData();
			if (lineData.getStartFigureUUID().equals(figureData.getFlowElementUUID())) {
				manLine.setStartFigure((JecnBaseFigurePanel) flowElementPanel);
			} else if (lineData.getEndFigureUUID().equals(figureData.getFlowElementUUID())) {
				manLine.setEndFigure((JecnBaseFigurePanel) flowElementPanel);
			}
		}
	}

	/**
	 * 获取面板
	 * 
	 * @return 当前面板
	 */
	public static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}
}
