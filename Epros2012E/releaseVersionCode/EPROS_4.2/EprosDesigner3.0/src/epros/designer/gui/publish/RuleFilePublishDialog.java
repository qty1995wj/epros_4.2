package epros.designer.gui.publish;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 制度文件
 * 
 * @author Administrator 2012-10-23
 */
public class RuleFilePublishDialog extends BasePublishDialog {

	public RuleFilePublishDialog(JecnTreeNode treeNode) {
		super(treeNode);
		if (!"ruleFile".equals(treeBean.getTreeNodeType().toString())) {
			log.error("RuleFilePublishDialog！　treeBean.getTreeNodeType()　＝"
					+ treeBean.getTreeNodeType());
			return;
		}
	}

	public void initialize() {
		type = 3;
		prfName = JecnProperties.getValue("ruleNameC");
		strTitle = JecnProperties.getValue("rulePubI");
	}
}
