package epros.draw.gui.operationConfig;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 新建，更新名称Dialog
 * 
 * @author zhangjie 2012-05-04
 * 
 */
public abstract class JecnEditNameDialog extends JecnDialog implements CaretListener {
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 控件显示面板 */
	private JPanel topPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = null;

	/** 名称Lab */
	private JLabel nameLab = null;

	/** 名称填写框 */
	private JTextField nameTextField = null;

	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBut = null;

	/** 名称验证提示 */
	protected JLabel promptLab = null;

	/** 设置面板控件大小 */
	Dimension dimension = null;

	public JecnEditNameDialog() {
		initCompotents();
		initLayout();
		this.setModal(true);
	}

	private String name = "";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.nameTextField.setText(name);
	}

	/***************************************************************************
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 控件显示面板
		topPanel = new JPanel();

		// 按钮面板
		buttonPanel = new JPanel();

		// 名称Lab
		nameLab = new JLabel();

		// 名称填写框
		nameTextField = new JTextField();

		// 名称验证提示
		promptLab = new JLabel();

		// 确定按钮
		okBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("confirm"));

		// 取消按钮
		cancelBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancel"));

		// 设置Dialog大小
		this.setSize(330, 120);

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置控件显示面板的默认背景色
		topPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置按钮面板的默认背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		dimension = new Dimension(330, 90);
		mainPanel.setPreferredSize(dimension);
		mainPanel.setMaximumSize(dimension);
		mainPanel.setMinimumSize(dimension);

		// 设置控件显示面板的大小
		dimension = new Dimension(330, 40);
		topPanel.setPreferredSize(dimension);
		topPanel.setMaximumSize(dimension);
		topPanel.setMinimumSize(dimension);

		// 设置按钮面板的大小
		dimension = new Dimension(323, 30);
		buttonPanel.setPreferredSize(dimension);
		buttonPanel.setMaximumSize(dimension);
		buttonPanel.setMinimumSize(dimension);
		nameTextField.addCaretListener(this);

		// 设置验证提示文字颜色
		promptLab.setForeground(Color.red);
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		// 主面板布局
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(topPanel, c);
		// 控件显示面板 布局
		topPanel.setLayout(new GridBagLayout());
		// 名称Lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(nameLab, c);
		// 名称填写框
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(nameTextField, c);
		// 验证提示
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(promptLab, c);
		// 按钮面板 布局
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// buttonPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 确定
		buttonPanel.add(okBut);
		// 取消
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

		// 确定按钮事件监听
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消按钮事件监听
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
		// Dialog标题
		this.setTitle(getDialogTitle());

		// 设置名称Lab的值
		this.nameLab.setText(getNameLab());

		// 设置验证提示Label的大小
		dimension = new Dimension(240, 13);
		promptLab.setPreferredSize(dimension);
		promptLab.setMaximumSize(dimension);
		promptLab.setMinimumSize(dimension);

		// 设置名称显示框JTextField的大小
		dimension = new Dimension(240, 20);
		this.nameTextField.setPreferredSize(dimension);
		nameTextField.setMinimumSize(dimension);
		nameTextField.setMaximumSize(dimension);
		// 设置Dialog的大小不被改变
		this.setResizable(false);
	}

	// 获取Dialog标题
	public abstract String getDialogTitle();

	// 获取名称Lab
	public abstract String getNameLab();

	// 确定事件
	public void okButtonAction() {
		name = nameTextField.getText();
		// 验证名称是否正确
		if (!OperationConfigUtil.validateName(name, promptLab)) {
			return;
		}
		if (validateNodeRepeat(name)) {
			promptLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("thisNameAlreadyExists"));
			return;
		} else {
			promptLab.setText("");
		}
		saveData();
	}

	public void caretUpdate(CaretEvent e) {
		name = nameTextField.getText();
		if (e.getSource() == nameTextField) {// 流程图名称
			// 验证名称是否合法
			if (!OperationConfigUtil.validateName(name, promptLab)) {
				return;
			}
		}
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证是否重名
	 * @return
	 */
	public abstract boolean validateNodeRepeat(String name);

	/**
	 * @author yxw 2012-5-8
	 * @description:保存数据,并显示
	 */
	public abstract void saveData();

	// 取消事件
	public void cancelButtonAction() {
		this.dispose();
	}

	public JTextField getNameTextField() {
		return nameTextField;
	}

	public void setNameTextField(JTextField nameTextField) {
		this.nameTextField = nameTextField;
	}

}
