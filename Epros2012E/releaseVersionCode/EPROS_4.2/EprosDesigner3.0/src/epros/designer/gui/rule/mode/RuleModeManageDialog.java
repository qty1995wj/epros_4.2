package epros.designer.gui.rule.mode;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnManageDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;

/**
 * 
 * 制度模板对话框
 * 
 * @author Administrator
 * 
 */
public class RuleModeManageDialog extends JecnManageDialog {

	private static Logger log = Logger.getLogger(RuleModeManageDialog.class);

	@Override
	public String getDialogTitle() {

		return JecnProperties.getValue("ruleModeManage");
	}

	@Override
	public String getTreePanelTitle() {
		return JecnProperties.getValue("modeTree");
	}

	@Override
	public String getSerachLabelName() {

		return JecnProperties.getValue("modeNameC");
	}

	@Override
	public JecnTree initJecnTree() {
		return new HighEfficiencyRuleModeTree(this);
		// if (JecnConstants.isMysql) {
		// return new RoutineRuleModeTree(this);
		// } else {
		// return new HighEfficiencyRuleModeTree(this);
		// }
	}

	@Override
	public boolean isAddButtonOne() {
		return true;
	}

	@Override
	public boolean isAddButtonTwo() {
		return false;
	}

	@Override
	public boolean isAddButtonThree() {
		return false;
	}

	@Override
	public String buttonOneName() {
		return JecnProperties.getValue("editBtn");
	}

	@Override
	public String buttonTwoName() {
		return null;
	}

	@Override
	public String buttonThreeName() {
		return null;
	}

	/**
	 * 更新模板
	 */
	@Override
	public void buttonOneAction() {
		int[] rows = this.getJecnTable().getSelectedRows();
		if (rows.length != 1) {
			// JecnOptionPane.showMessageDialog(this, JecnProperties
			// .getValue("chooseOneRow"));
			this.getjLabelTip().setText(JecnProperties.getValue("chooseOneRow"));
		} else {
			// 得到要更新的数据
			JecnTreeBean jecnTreeBean = this.getSearchList().get(rows[0]);
			EditRuleModeDialog editRuleModeDialog = new EditRuleModeDialog(jecnTreeBean, this.getjTree());
			editRuleModeDialog.setVisible(true);
			if (editRuleModeDialog.isOperation) {
				// 表格同步修改的数据
				this.getJecnTable().getModel().setValueAt(editRuleModeDialog.getNameField().getText().trim(), rows[0],
						0);
				JecnTreeNode jecnTreeNode = JecnTreeCommon.getTreeNode(jecnTreeBean, this.getjTree());
				if (jecnTreeNode != null) {
					JecnTreeCommon.reNameNode(this.getjTree(), jecnTreeNode, editRuleModeDialog.getNameField()
							.getText().trim());
				}
			}
		}
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		// if (JecnConstants.isMysql) {
		// List<JecnTreeBean> list =
		// JecnTreeCommon.findAllTreeNodes(this.getjTree(), name);
		// return list;
		// } else {
		try {
			return ConnectionPool.getRuleModeAction().findRuleModeByName(name);
		} catch (Exception e) {
			log.error("RuleModeManageDialog searchByName is error", e);
			return new ArrayList<JecnTreeBean>();
		}

		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> v = new Vector<String>();
		v.add(JecnProperties.getValue("modeName"));
		v.add("ID");
		return v;
	}

	@Override
	public Vector<Vector<String>> updateTableContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		for (JecnTreeBean jecnTreeBean : list) {
			Vector<String> v = new Vector<String>();
			v.add(jecnTreeBean.getName());
			v.add(jecnTreeBean.getId().toString());
			vs.add(v);
			v = null;
		}
		return vs;
	}

	@Override
	public void buttonTwoAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void buttonThreeAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean deleteData(List<Long> listIds) {
		try {
			ConnectionPool.getRuleModeAction().deleteRuleMode(listIds, JecnUtil.getUserId());
			return true;
		} catch (Exception e) {
			log.error("RuleModeManageDialog deleteData is error", e);
			return false;
		}

	}
}
