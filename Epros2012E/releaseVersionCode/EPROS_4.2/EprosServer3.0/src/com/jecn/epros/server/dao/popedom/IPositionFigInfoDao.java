package com.jecn.epros.server.dao.popedom;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.common.IBaseDao;

public interface IPositionFigInfoDao extends
		IBaseDao<JecnPositionFigInfo, Long> {
	/**
	 * 获得岗位在项目下参与的角色(没有岗位组)
	 * 
	 * @param posId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRoleNOGroupByPosId(Long posId, Long projectId)
			throws Exception;

	/**
	 * 获得岗位在项目下参与的角色(岗位组)
	 * 
	 * @param posId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRoleGroupByPosId(Long posId, Long projectId)
			throws Exception;

	/**
	 * 通过角色Id获得活动
	 * @param figureIdsList
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getActiveByRoleListIds(Set<Long> figureIdsList)
			throws Exception;
}
