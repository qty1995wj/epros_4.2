package com.jecn.epros.server.action.web.login.ad.ultimus;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 南京物探单点登录所需配置信息，在系统启动时需要启动项
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUltimusAfterItem {
	private static Logger log = Logger.getLogger(JecnUltimusAfterItem.class);
	/** 单点登录传过来的登录名称解密URL */
	static String EPROS_URL = null;
	static String LOGIN_NAME = null;

	/**
	 * 读取本地文件数据
	 * 
	 */
	public static void start() {
		String proPath = "cfgFile/ultimus/ultimus.properties";
		// 读取配置文件
		InputStream fis = null;
		Properties properties = new Properties();

		String url = JecnPath.CLASS_PATH + proPath;
		log.info("配置文件路径:" + url);

		try {
			fis = new FileInputStream(url);
			properties.load(fis);
			// 读值
			EPROS_URL = properties.getProperty("EPROS_URL");
			LOGIN_NAME = properties.getProperty("LOGIN_NAME");
		} catch (FileNotFoundException e) {
			log.error("获取文件路径失败:" + proPath, e);
		} catch (IOException e) {
			log.error("读流异常:" + proPath, e);
		} catch (Exception e) {
			log.error("其他异常:" + proPath, e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error("关闭InputStream流异常:" + proPath, e);
				}
			}
		}
	}
}
