package com.jecn.epros.mapper;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.system.RoleInfoBean;
import com.jecn.epros.domain.system.UserWebSearchBean;
import com.jecn.epros.domain.system.ViewRoleDetailBean;
import com.jecn.epros.sqlprovider.SystemManageDynaSqlProvider;
import org.apache.ibatis.annotations.*;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Mapper 类中不允许方法重载（出现重载方法， MyBatis 会报错）
 */
public interface SystemManageMapper {

    /**
     * 单条语句执行查询
     *
     * @return
     */
    @Select("select ju.people_id,ju.login_name,ju.true_name,jfoi.figure_id, jfoi.figure_text, jfo.org_id,jfo.org_name from jecn_user ju "
            + " left join jecn_user_position_related jupr on jupr.people_id=ju.people_id"
            + " left join jecn_flow_org_image jfoi on jupr.figure_id=jfoi.figure_id"
            + " left join jecn_flow_org jfo on jfoi.org_id = jfo.org_id and and jfo.del_state=0")
    @ResultMap("mapper.SystemManage.UserManageResult")
    List<JecnUser> selectUsersForManage();

    /**
     * 动态语句查询
     *
     * @param userWebSearchBean
     * @return
     */
    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "selectUsersForManage")
    @ResultMap("mapper.SystemManage.UserManageResult")
    List<JecnUser> selectUsersForManage2(UserWebSearchBean userWebSearchBean);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "getUserCount")
    public int getUserManageUserCount(UserWebSearchBean userWebSearchBean);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "fetchUsers")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    List<JecnUser> fetchUsers(UserWebSearchBean userWebSearchBean);

    @Select("SELECT JFOI.FIGURE_ID, JFOI.FIGURE_TEXT, JFO.ORG_ID,JFO.ORG_NAME,JUPR.PEOPLE_ID FROM JECN_USER_POSITION_RELATED JUPR,JECN_FLOW_ORG_IMAGE JFOI,JECN_FLOW_ORG JFO"
            + "       WHERE JUPR.FIGURE_ID=JFOI.FIGURE_ID AND JFOI.ORG_ID = JFO.ORG_ID AND JFO.DEL_STATE=0"
            + "       AND JUPR.PEOPLE_ID IN ${ids}")
    List<Map<String, Object>> fetchPeoplePos(Map<String, String> paramMap);

    @Select("SELECT JRI.ROLE_ID,JRI.ROLE_NAME,JUR.PEOPLE_ID FROM JECN_ROLE_INFO JRI,JECN_USER_ROLE JUR WHERE JRI.ROLE_ID=JUR.ROLE_ID AND JUR.PEOPLE_ID IN ${ids}")
    List<Map<String, Object>> fetchPeopleRole(Map<String, String> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "getTotalRole")
    int getTotalRole(Map<String, String> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "getUserRoleInfo")
    List<String> getUserRoleInfo(Map<String, Object> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "getTotalUserRoleInfo")
    int getTotalUserRoleInfo(Map<String, Object> paramMap);


    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "getTotalViewRole")
    int getTotalViewRole(Map<String, String> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "fetchUserRoles")
    List<Map<String, Object>> fetchUserRoles(Map<String, String> paramMap);


    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "fetchUserRoleInfos")
    @ResultMap("mapper.SystemManage.RoleInfoBeanResult")
    List<RoleInfoBean> fetchUserRoleInfos(Map<String, Object> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "fetchViewRoles")
    List<Map<String, Object>> fetchViewRoles(Map<String, String> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "viewRoleAuthList")
    List<Map<String, Object>> viewRoleAuthList(String roleId);

    @Delete("DELETE FROM ROLE_PERMISSIONS_MENU WHERE ROLE_ID = #{roleId} ")
    public void deleteViewRoleAuth(String roleId);

    @Insert("insert into ROLE_PERMISSIONS_MENU(GUID,ROLE_ID,MENU_ID) VALUES(#{guid},#{roleId},#{menuid})")
    public void insertViewRoleAuth(Map<String, Object> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "getProcessCheckCount")
    int getProcessCheckCount(Map<String, String> paramMap);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "fetchProcessCheckList")
    List<Map<String, Object>> fetchProcessCheckList(Map<String, String> paramMap);

    @InsertProvider(type = SystemManageDynaSqlProvider.class, method = "addRole")
    void addRole(ViewRoleDetailBean role);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "findRolesUser")
    List<Map<String, Object>> findRolesUser(ViewRoleDetailBean role);

    @InsertProvider(type = SystemManageDynaSqlProvider.class, method = "addRolesUser")
    void addRolesUser(ViewRoleDetailBean role);

    //根据人员删除对应角色关联关系
    @DeleteProvider(type = SystemManageDynaSqlProvider.class, method = "delRolesUserToPeople")
    int delRolesUserToPeople(ViewRoleDetailBean role);


    @DeleteProvider(type = SystemManageDynaSqlProvider.class, method = "deleteRole")
    void deleteRole(ViewRoleDetailBean role);

    @DeleteProvider(type = SystemManageDynaSqlProvider.class, method = "deleteRolesUserToRole")
    int deleteRolesUserToRole(ViewRoleDetailBean role);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "findRolesByUser")
    Map<String, Object> findRolesByUser(Long pepoleId);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "findRolesThePosByUser")
    List<String>  findRolesThePosByUser(Long pepoleId);

    @SelectProvider(type = SystemManageDynaSqlProvider.class, method = "findRolesTheOrgByUser")
    List<String>  findRolesTheOrgByUser(Long pepoleId);

    @DeleteProvider(type = SystemManageDynaSqlProvider.class, method = "deleteRoleAssociationMenu")
    void deleteRoleAssociationMenu(ViewRoleDetailBean role);

}
