package com.jecn.webservice.xinghan.service;

public interface IXinghanProSyncService {

	/**
	 * 获取同步到星汉系统中的流程信息以及流程元素
	 * 
	 * @author cheunaob
	 * */
	public int SysncFlowToBps(Long flowId);

}
