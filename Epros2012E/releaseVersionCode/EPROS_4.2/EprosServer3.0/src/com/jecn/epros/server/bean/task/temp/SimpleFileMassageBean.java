package com.jecn.epros.server.bean.task.temp;

import java.io.File;

/**
 * 文件任务 提交文件修改文件信息
 * 
 * @author Administrator
 * 
 */
public class SimpleFileMassageBean {

	/** 文件集合 */
	private File[] files;
	/** 文件名称集合 */
	private String[] fileNames;
	/** 文件任务表 主键ID集合 */
	private String fileIds;

	public File[] getFiles() {
		return files;
	}

	public void setFiles(File[] files) {
		this.files = files;
	}

	public String[] getFileNames() {
		return fileNames;
	}

	public void setFileNames(String[] fileNames) {
		this.fileNames = fileNames;
	}

	public String getFileIds() {
		return fileIds;
	}

	public void setFileIds(String fileIds) {
		this.fileIds = fileIds;
	}
}
