package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;
import com.jecn.webservice.zhongsh.datainput.ArrayOfUser;
import com.jecn.webservice.zhongsh.datainput.Role;
import com.jecn.webservice.zhongsh.datainput.User;
import com.jecn.webservice.zhongsh.datainput.WebServiceForUserAndRole;
import com.jecn.webservice.zhongsh.datainput.WebServiceForUserAndRoleSoap;

/**
 * 中石化-石勘院 webservice 人员同步数据封装
 * 
 *@author ZXH
 * @date 2016-7-1上午10:02:21
 */
public class ImportUserByWbServiceZhongsh extends AbstractImportUser {
	private static final Logger log = Logger.getLogger(ImportUserByWbServiceZhongsh.class);

	private static final QName SERVICE_NAME = new QName("http://tempuri.org/", "WebServiceForUserAndRole");

	WebServiceForUserAndRoleSoap port = null;
	{
		URL wsdlURL = WebServiceForUserAndRole.WSDL_LOCATION;
		WebServiceForUserAndRole ss = new WebServiceForUserAndRole(wsdlURL, SERVICE_NAME);
		port = ss.getWebServiceForUserAndRoleSoap12();
	}

	public ImportUserByWbServiceZhongsh(AbstractConfigBean configBean) {
		super(configBean);
	}

	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		List<DeptBean> deptBeans = new ArrayList<DeptBean>();
		List<Role> roles = null;
		try {
			roles = port.getDeptList().getRole();
		} catch (Exception e) {
			log.error("获取接口数据异常！", e);
			return null;
		}
		DeptBean deptBean = null;
		for (Role role : roles) {
			if (7 == role.getParentRoleID()) {
				role.setParentRoleID(0);
			}
			deptBean = new DeptBean();
			deptBean.setDeptNum(role.getRoleID());
			deptBean.setDeptName(role.getRoleName());
			deptBean.setPerDeptNum(role.getParentRoleID() + "");
			deptBeans.add(deptBean);
		}
		return deptBeans;
	}

	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		List<UserBean> userBeans = new ArrayList<UserBean>();
		List<User> users = null;
		try {
			boolean includeDeputy = true;
			ArrayOfUser arrayOfUser = port.getAllUser2(includeDeputy);
			users = arrayOfUser.getUser();
		} catch (Exception e) {
			log.error("获取接口数据异常！", e);
			return null;
		}

		UserBean userBean = null;
		for (User user : users) {
			if ("admin".equals(user.getUsername())) {
				continue;
			}
			userBean = new UserBean();
			userBean.setUserNum(user.getUsername() + "");
			userBean.setTrueName(user.getDisplayName());
			userBean.setEmail(user.getEmail());
			userBean.setEmailType(0);
			if (user.getJobID() != 0) {
				userBean.setPosNum(user.getJobID() + "");
				userBean.setPosName(user.getJobName());
				userBean.setDeptNum(user.getRoleID() + "");
			}
			userBeans.add(userBean);
		}
		return userBeans;
	}
}
