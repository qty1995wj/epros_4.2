package com.jecn.webservice.jbshihua.entity;

import java.io.Serializable;

public class PostInfo implements Serializable {
	private String uuid;
	private String userCode;
	private String deptNum;
	private String postNum;
	private String posName;
	private String status;

	public PostInfo(String uuid, String userCode, String deptNum, String postNum, String posName, String status) {
		this.uuid = uuid;
		this.userCode = userCode;
		this.deptNum = deptNum;
		this.postNum = postNum;
		this.posName = posName;
		this.status = status;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getPostNum() {
		return postNum;
	}

	public void setPostNum(String postNum) {
		this.postNum = postNum;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
