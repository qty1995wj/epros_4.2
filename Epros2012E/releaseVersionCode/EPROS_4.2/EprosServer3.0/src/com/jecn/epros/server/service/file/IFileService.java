package com.jecn.epros.server.service.file;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.bean.file.FileAttributeBean;
import com.jecn.epros.server.bean.file.FileInfoBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileUseInfoBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.file.FileBean;
import com.jecn.epros.server.webBean.file.FileDetailListWebBean;
import com.jecn.epros.server.webBean.file.FileSearchBean;

public interface IFileService extends IBaseService<JecnFileBeanT, Long> {

	/**
	 * @author yxw 2012-5-29
	 * @description:添加文件目录
	 * @param fileDir
	 * @throws Exception
	 */
	public long addFileDir(JecnFileBeanT fileDir) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:文件目录重命名
	 * @param name
	 * @param id
	 * @param updatePersonId
	 * @throws Exception
	 */
	public void reFileDirName(String name, Long id, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description: 文件上传(增加)
	 * @param file
	 * @param listContent
	 * @param isPub
	 *            true 是直接发布
	 * @return
	 * @throws Exception
	 */
	public void addFiles(List<JecnFileBeanT> fileList, AccessId accId, boolean isPub, int codeTotal) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:根据pId获取文件子节点
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildFiles(Long pId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:根据pId获取文件目录子节点
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildFileDirs(Long pId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:获取所有文件目录/文件节点
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllFiles(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:获取所有文件目录节点
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllFileDirs(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:根所文件名称搜索文件
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public List<JecnFileBeanT> getFilesByName(String name, Long projectId) throws Exception;

	/**
	 * 2012-12-27
	 * 
	 * @description:根所文件名称搜索文件目录
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public List<JecnFileBeanT> getFileDirsByName(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-20
	 * @description:根所文件名称搜索文件,返回JecnTreeBean集合
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getJecnTreeBeanByName(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description: 节点排序
	 * @param list
	 * @param pId
	 * @throws Exception
	 */
	public void updateSortFiles(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveFiles(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:文件基本信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public FileInfoBean getFileBean(Long id, boolean isPub) throws Exception;

	/**
	 * 
	 * @author yxw 2012-5-29
	 * @description:文件删除
	 * @param listIds
	 * @return int 0 删除异常，1删除成功；2：存在节点处于任务中
	 * @throws Exception
	 */
	public void deleteFiles(List<Long> listIds, Long projectId, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:更新文件
	 * @param fileBean
	 * @param updatePersonId
	 * @param isPub
	 *            true是直接发布
	 * @throws Exception
	 */
	public void updateFile(JecnFileBeanT fileBean, Long updatePersonId, boolean isPub, boolean isDesign)
			throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:打开文件
	 * @param id
	 *            文件ID
	 * @return
	 * @throws Exception
	 */
	public FileOpenBean openFile(Long id, boolean isPub) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:文件恢复
	 * @param fileId
	 * @param versionId
	 * @param updatePersonId
	 */
	public void updateRecovery(Long fileId, Long versionId, Long updatePersonId, boolean isPub, boolean isDesign)
			throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:新增判断是否重名
	 * @param names
	 * @param pId
	 * @return 如果没有重名的文件，返回null,有重名的文件则返回重名的文件名称数组
	 * @throws Exception
	 */
	public String[] validateAddName(List<String> names, Long pId, int fileType) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:文件库内判断是否重名
	 * @param names
	 * @param pId
	 * @return 如果没有重名的文件，返回null,有重名的文件则返回重名的文件名称数组
	 * @throws Exception
	 */
	public String[] validateNamefullPath(List<String> names, Long pId, int fileType) throws Exception;

	/**
	 * @author yxw 2012-5-29
	 * @description:更新文件判断是否重名
	 * @param name
	 * @param id
	 * @param pId
	 * @return 重名返回true，不重名返回false
	 * @throws Exception
	 */
	public boolean validateUpdateName(String name, Long id, Long pId) throws Exception;

	/**
	 * @author yxw 2012-5-31
	 * @description:删除版本信息
	 * @param ids
	 * @param fileId
	 * @param createDate
	 * @throws Exception
	 */
	public void deleteVersion(List<Long> ids, Long fileId, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-4
	 * @description:查看历史文件
	 * @param fileId
	 * @param versionId
	 * @param saveType
	 * @param fileCreateDate
	 * @return
	 * @throws Exception
	 */
	public FileOpenBean openVersion(Long fileId, Long versionId) throws Exception;

	/**
	 * @author yxw 2012-6-4
	 * @description: 更新文件属性
	 * @param jecnFileBean文件Bean
	 * @param posIds
	 *            岗位ids
	 * @param orgIds
	 *            组织ids
	 */
	public void updateFileProperty(JecnFileBeanT jecnFileBean, Set<Long> standardIds, Set<Long> riskIds,
			Long updatePersonId) throws Exception;

	/**
	 * @description:根所文件 ID,返回JecnTreeBean集合
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public JecnTreeBean getJecnTreeBeanByIds(Long listIds, Long projectId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：搜索文件节点
	 * @param standardId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodes(Long fileId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-8-17
	 * @description:获得文件是不是有查阅权限
	 * @param fileId
	 * @param projectId
	 * @param setIds
	 * @return
	 * @throws Exception
	 */
	public boolean isFileAuth(Long fileId, Long projectId, Set<Long> setIds, TreeNodeType treeNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-12-18
	 * @description:删除文件时判断有没有权限
	 * @param fileIds
	 *            文件id集合
	 * @param projectId
	 *            项目ID
	 * @param setIds
	 *            做删除操作用户的文件权限集合
	 * @return true 有权限 false 没权限
	 * @throws Exception
	 */
	public boolean isFilesAuth(List<Long> fileIds, Long projectId, Set<Long> setIds) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:撤销发布
	 * @param id
	 *            文件ID
	 * @param peopleId 
	 * @throws Exception
	 */
	public void cancelRelease(Long id, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:直接发布
	 * @param id
	 *            文件ID
	 * @param peopleId 
	 * @throws Exception
	 */
	public void directRelease(Long id, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-12-4
	 * @description:浏览端树加载
	 * @param pId
	 *            父ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildFilesWeb(Long pId, Long projectId, Long peopleId, boolean flag) throws Exception;

	/**
	 * @author yxw 2012-12-4
	 * @description:文件搜索总条数
	 * @param fileSearchBean搜索条件
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public int getFileTotal(FileSearchBean fileSearchBean) throws Exception;

	/**
	 * @author yxw 2013-2-22
	 * @description:根据文件目录ID查询文件总数
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	public int getFileTotalByPid(Long pId, Long projectId, Long peopleId, boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2012-12-4
	 * @description:文件搜索
	 * @param fileSearchBean搜索条件
	 * @param projectId项目ID
	 * @param start开始行数
	 * @param limit每页显示数量
	 * @return
	 * @throws Exception
	 */
	public List<FileWebBean> getFileList(FileSearchBean fileSearchBean, int start, int limit) throws Exception;

	/**
	 * @author yxw 2013-2-22
	 * @description:根据文件目录ID查询文件列表
	 * @param pId
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<FileWebBean> getFileListByPid(Long pId, Long projectId, Long peopleId, boolean isAdmin, int start,
			int limit) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共文件查询总数
	 * @param publicSearchBean搜索条件bean
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public int getTotalPublicFile(PublicSearchBean publicSearchBean, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共文件查询
	 * @param publicSearchBean搜索条件bean
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<PublicFileBean> getPublicFile(PublicSearchBean publicSearchBean, int start, int limit, Long projectId)
			throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的文件
	 * @param attenTionSearchBean查询条件
	 * @param projectId项目id
	 * @return
	 * @throws Exception
	 */
	public int getTotalAttenTionFile(AttenTionSearchBean attenTionSearchBean) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的文件
	 * @param attenTionSearchBean
	 *            查询条件
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<FileWebBean> getAttenTionFile(AttenTionSearchBean attenTionSearchBean, int start, int limit)
			throws Exception;

	/**
	 * @author yxw 2012-12-24
	 * @description:查看文件详情
	 * @param fileId
	 *            文件ID
	 * @return
	 * @throws Exception
	 */
	public FileBean findFileBean(Long fileId, boolean isPub) throws Exception;

	/**
	 * 验证当前选中记录是否为发布文件
	 * 
	 * @param fileId
	 *            文件ID
	 * @return int==1 为发布文件
	 * @throws Exception
	 */
	public int isPubFileCount(Long fileId) throws Exception;

	/**
	 * 验证当前选中记录是否为发布文件
	 * 
	 * @param fileIds
	 *            文件ID集合
	 * @return 为发布文件总数
	 * @throws Exception
	 */
	public int isPubFileCount(List<Long> fileIds) throws Exception;

	/**
	 * @author weidp
	 * @description 更新文件夹及其下所有的文件和文件夹的隐藏属性
	 * @param folderId
	 *            文件夹ID
	 * @param hideFlag
	 *            隐藏属性值 0 隐藏 1显示
	 */
	public void updateHideFiles(Long folderId, int hideFlag) throws Exception;

	/**
	 * @author weidp
	 * @description 获取文件清单
	 * @param fileId
	 *            文件夹Id
	 * @param showAll
	 *            是否显示所有文件，true显示,false显示10条
	 * @return
	 */
	public FileDetailListWebBean getFileDetailList(long fileId, boolean showAll) throws Exception;

	/**
	 * @author weidp
	 * @description 文件清单Excel下载
	 * @param fileDetailList
	 * @return
	 */
	public byte[] downLoadFileDetailListExcel(FileDetailListWebBean fileDetailList);

	/***
	 * 获取文件回收树节点数据
	 * 
	 * @param projectId
	 *            项目ID
	 * @param peopleId
	 *            人员id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pId) throws Exception;

	/**
	 * 文件回收站：恢复删除的文件
	 * 
	 * @param ids
	 * @param peopleId
	 * @throws Exception
	 */
	public void updateRecycleFile(List<Long> ids) throws Exception;

	/***
	 * 获取要恢复的文件目录的子节点
	 * 
	 * @return
	 * @param listIds
	 *            文件目录Id集合
	 * @throws Exception
	 */
	public void updateRecycleFileOrDirIds(List<Long> listIds, Long projectId) throws Exception;

	/***
	 * 文件管理删除文件(假删：放入回收站)
	 * 
	 * @param listIds
	 *            文件ID集合
	 * @param projectId
	 *            项目ID
	 * @param updatePersonId
	 *            更新人ID
	 * @throws Exception
	 */
	public void deleteFileManage(List<Long> listIds, Long projectId, Long updatePersonId, TreeNodeType treeNodeType)
			throws Exception;

	/**
	 * 文件回收站:搜索
	 * 
	 * @param name
	 *            搜索的名称
	 * @param projectId
	 *            项目id
	 * @param setFile
	 *            有权限的文件与文件夹的id的集合
	 * @param isAdmin
	 *            是否是管理员
	 */
	public List<JecnTreeBean> getAllFilesByName(String name, Long projectId, Set<Long> fileIds, boolean isAdmin)
			throws Exception;

	/**
	 * 文件回收站：定位，通过当前节点ID递归获得父节点集合以及同级下的节点的集合
	 * 
	 * @param id
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception;

	public int getMaxSortId(Long id) throws Exception;

	public List<JecnTaskHistoryNew> getTaskHistoryNew(long fileId) throws Exception;

	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long historyId) throws Exception;

	/**
	 * 获取文件的ContentId
	 * 
	 * @param fileId
	 * @param isPub
	 * @return
	 */
	public Map<String, String> getFileNameAndContentId(long fileId, boolean isPub) throws Exception;

	JecnFileBean findJecnFileBeanById(Long fileId) throws Exception;

	public void batchReleaseNotRecord(List<Long> ids, Long peopleId) throws Exception;

	List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception;

	List<JecnFileBeanT> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;

	public void batchCancelReleaseNotRecord(List<Long> ids, Long peopleId) throws Exception;

	List<JecnFileBeanT> getDirectoryFile(Long pId);

	void updateSortFiles(Long pId, Map<Long, Integer> relatedMap, Long updatePersonId) throws Exception;

	public List<FileUseInfoBean> getFileUsages(Long id) throws Exception;

	public List<JecnFileContent> getJecnFileContents(Long id) throws Exception;

	public List<Long> getFileAuthByFlowAutoCreateDir(Map<Integer, Set<Long>> relatedsMap) throws Exception;

	public List<JecnTreeBean> getJecnTreeBeanByIds(Set<Long> ids) throws Exception;

	FileAttributeBean getFileAttributeBeanById(Long id) throws Exception;

	String getFlowNumByFileId(Long fileId) throws Exception;

	void updateFileAttributeBeanById(FileAttributeBean fileAttributeBean, Long id, Long updatePersonId)
			throws Exception;

	Long getFileRelatedFlowId(Long fileId);

	public boolean isNotAbolishOrNotDelete(Long id);

	public List<JecnDictionary> getFileTypeData() throws Exception;

	List<Long> getFileAuthByFlowAutoCreateDir(Set<Long> flowIds) throws Exception;

	public void createFile(String filePath, byte[] changeFileToByte) throws IOException;

	public String saveFilePath(String flowHeadinfo, Date date, String suffixName);

	public List<JecnFileBeanT> getMergeFilesByName(String name, Long projectId);

	public List<JecnFileBeanT> getFilesFullPath(List<Long> addIds);

	public void merge(Long mainId, List<Long> mergeIds);

	public void updateViewSort(TreeNodeType treeNodeType, Long id) throws Exception;

}
