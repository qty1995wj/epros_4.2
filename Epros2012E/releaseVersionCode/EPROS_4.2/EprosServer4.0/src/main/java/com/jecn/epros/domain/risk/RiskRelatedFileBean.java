package com.jecn.epros.domain.risk;

import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.process.relatedFile.RelatedRule;

import java.util.ArrayList;
import java.util.List;

public class RiskRelatedFileBean {
    /**
     * 相关流程
     */
    private List<RelatedFlow> relatedProcesses = new ArrayList<RelatedFlow>();

    /**
     * 相关制度
     */
    private List<RelatedRule> relatedRules = new ArrayList<RelatedRule>();

    public List<RelatedFlow> getRelatedProcesses() {
        return relatedProcesses;
    }

    public void setRelatedProcesses(List<RelatedFlow> relatedProcesses) {
        this.relatedProcesses = relatedProcesses;
    }

    public List<RelatedRule> getRelatedRules() {
        return relatedRules;
    }

    public void setRelatedRules(List<RelatedRule> relatedRules) {
        this.relatedRules = relatedRules;
    }
}
