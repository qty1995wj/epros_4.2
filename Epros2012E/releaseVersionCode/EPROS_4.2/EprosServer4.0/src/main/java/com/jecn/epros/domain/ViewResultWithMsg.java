package com.jecn.epros.domain;

/**
 * Created by user on 2017/5/5.
 */
public class ViewResultWithMsg extends ViewResult {

    private String msg = "";

    public ViewResultWithMsg(boolean success, String msg) {
        super(success);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
