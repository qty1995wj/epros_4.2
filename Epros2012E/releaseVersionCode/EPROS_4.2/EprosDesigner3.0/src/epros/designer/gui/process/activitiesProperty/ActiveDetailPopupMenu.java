package epros.designer.gui.process.activitiesProperty;

import java.awt.AWTEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.constant.JecnWorkflowConstant.FileTypeEnum;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnActiveStandardBeanT;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnControlPointT;
import epros.draw.designer.JecnFigureInoutSampleT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.popup.popup.JecnPopupMenu;
import epros.draw.gui.swing.popup.table.JecnPopupTable;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;

/**
 * 
 * 活动连接按钮处理菜单
 * 
 * @author Administrator
 * 
 */
public class ActiveDetailPopupMenu extends JecnPopupMenu {
	private final static Logger log = Logger.getLogger(ActiveDetailPopupMenu.class);

	/** 表头数组[文件ID,文件名称，文件类型（显示图片）] */
	private String[] headerNameArray = null;
	/** 表对象 */
	private JecnActiveFigurePopupTable table;

	/** 活动数据对象 */
	private JecnActiveData jecnActiveData = null;
	/** 0是连接，1是角色，2是活动输入，3是活动的输出，4是活动操作规范 5是关键控制点 6是标准文件 */
	private int type = -1;

	public ActiveDetailPopupMenu(JecnActiveData jecnActiveData, int type) {
		if (jecnActiveData == null) {
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.jecnActiveData = jecnActiveData;
		this.type = type;

		if (type == 5) {
			initControlTableStyle(); // 初始化控制点表格
		} else if (type == 6) {
			initStandardTableStyle(); // 初始化关联标准表格
		} else {
			initComponents();
		}

		// 根据给定数据对象，初始化表行数据
		initTableData();
	}

	/**
	 * 初始化关键控制点
	 * 
	 * @author weidp
	 * @date 2014-10-28 下午05:01:59
	 */
	private void initControlTableStyle() {
		// 自定义 相关标准 表头
		headerNameArray = new String[] { "id", JecnProperties.getValue("controlPointNum") };
		// 表对象
		table = new JecnActiveFigurePopupTable();
		this.add(table.getTableScrollPane());
		// 隐藏第一列宽度
		TableColumn tableColumnOne = table.getColumnModel().getColumn(0);
		tableColumnOne.setMinWidth(0);
		tableColumnOne.setMaxWidth(0);
	}

	/**
	 * 初始化相关标准表格
	 * 
	 * @author weidp
	 * @date 2014-10-28 下午04:11:10
	 */
	private void initStandardTableStyle() {
		// 自定义 相关标准 表头
		headerNameArray = new String[] { "id", JecnProperties.getValue("standardName"), JecnProperties.getValue("type") };
		// 表对象
		table = new JecnActiveFigurePopupTable();
		this.add(table.getTableScrollPane());
		// 隐藏第一列宽度
		TableColumn tableColumnOne = table.getColumnModel().getColumn(0);
		tableColumnOne.setMinWidth(0);
		tableColumnOne.setMaxWidth(0);
		// 设置第三列宽度
		TableColumn tableColumnThree = table.getColumnModel().getColumn(2);
		tableColumnThree.setMinWidth(55);
		tableColumnThree.setMaxWidth(60);
	}

	private void initComponents() {
		// 表对象
		table = new JecnActiveFigurePopupTable();

		this.add(table.getTableScrollPane());

		// 隐藏第一列宽度
		TableColumn tableColumnOne = table.getColumnModel().getColumn(0);
		tableColumnOne.setMinWidth(0);
		tableColumnOne.setMaxWidth(0);
		// 设置第三列宽度
		TableColumn tableColumnThree = table.getColumnModel().getColumn(2);
		tableColumnThree.setMinWidth(20);
		tableColumnThree.setMaxWidth(20);
	}

	/**
	 * 
	 * 根据给定数据对象，初始化表行数据
	 * 
	 */
	private void initTableData() {
		switch (type) {
		case 2:// 输入
			if (JecnConfigTool.useNewInout()) {
				List<JecnFigureInoutT> listFigureInTs = jecnActiveData.getListFigureInTs();
				if (listFigureInTs != null && listFigureInTs.size() > 0) {
					for (JecnFigureInoutT jecnFigureInoutT : listFigureInTs) {
						// 获取 模板数据
						List<JecnFigureInoutSampleT> listSampleT = jecnFigureInoutT.getListSampleT();
						for (JecnFigureInoutSampleT moduleBean : listSampleT) {
							if (moduleBean.getType() == 0) {
								if (moduleBean.getFileId() != null) {
									Vector<Object> v = new Vector<Object>();
									// 行数据对象
									v.add(moduleBean.getFileId());
									// 文件名称
									v.add(moduleBean.getFileName());
									// 文件图片
									v.add(getFileTypeIcon(moduleBean.getFileName()));
									// 添加行
									this.table.getModel().addRow(v);
								}
							}
						}
					}
				}
			} else {
				List<JecnActivityFileT> listJecnActivityFileT = jecnActiveData.getListJecnActivityFileT();
				if (listJecnActivityFileT != null && listJecnActivityFileT.size() > 0) {
					for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
						if (jecnActivityFileT.getFileType().intValue() == 0) {// 输入
							Vector<Object> v = new Vector<Object>();
							// 行数据对象
							v.add(jecnActivityFileT.getFileSId());
							// 文件名称
							v.add(jecnActivityFileT.getFileName());
							// 文件图片
							v.add(getFileTypeIcon(jecnActivityFileT.getFileName()));

							// 添加行
							this.table.getModel().addRow(v);
						}
					}
				}
			}

			break;
		case 3:// 输出
			if (JecnConfigTool.useNewInout()) {
				List<JecnFigureInoutT> listFigureOutTs = jecnActiveData.getListFigureOutTs();
				if (listFigureOutTs != null && listFigureOutTs.size() > 0) {
					for (JecnFigureInoutT jecnFigureInoutT : listFigureOutTs) {
						// 获取 模板数据
						List<JecnFigureInoutSampleT> listSampleT = jecnFigureInoutT.getListSampleT();
						for (JecnFigureInoutSampleT moduleBean : listSampleT) {
							if (moduleBean.getType() == 0) {
								if (moduleBean.getFileId() != null) {
									Vector<Object> v = new Vector<Object>();
									// 行数据对象
									v.add(moduleBean.getFileId());
									// 文件名称
									v.add(moduleBean.getFileName());
									// 文件图片
									v.add(getFileTypeIcon(moduleBean.getFileName()));
									// 添加行
									this.table.getModel().addRow(v);
								}
							}
						}
					}
				}
			} else {
				List<JecnModeFileT> listModeFileT = jecnActiveData.getListModeFileT();

				if (listModeFileT != null && listModeFileT.size() > 0) {
					for (JecnModeFileT jecnModeFileT : listModeFileT) {
						Vector<Object> v = new Vector<Object>();
						// 行数据对象
						v.add(jecnModeFileT.getFileMId());
						// 文件名称
						v.add(jecnModeFileT.getModeName());
						// 文件图片
						v.add(getFileTypeIcon(jecnModeFileT.getModeName()));

						// 添加行
						this.table.getModel().addRow(v);
					}

				}
			}
			break;
		case 4:// 操作说明
			List<JecnActivityFileT> listOperatorActivityFileT = jecnActiveData.getListJecnActivityFileT();

			if (listOperatorActivityFileT != null && listOperatorActivityFileT.size() > 0) {
				for (JecnActivityFileT jecnActivityFileT : listOperatorActivityFileT) {
					if (jecnActivityFileT.getFileType().intValue() == 1) {
						Vector<Object> v = new Vector<Object>();
						// 行数据对象
						v.add(jecnActivityFileT.getFileSId());
						// 文件名称
						v.add(jecnActivityFileT.getFileName());
						// 文件图片
						v.add(getFileTypeIcon(jecnActivityFileT.getFileName()));

						// 添加行
						this.table.getModel().addRow(v);
					}
				}
			}
			break;
		case 5: // 控制点
			List<JecnControlPointT> controlList = jecnActiveData.getJecnControlPointTList();
			if (controlList != null && controlList.size() != 0) {
				for (JecnControlPointT jecnControlPointT : controlList) {
					Vector<Object> v = new Vector<Object>();
					v.add(jecnControlPointT.getRiskId());
					v.add(jecnControlPointT.getControlCode());
					this.table.getModel().addRow(v);
				}
			}
			break;
		case 6:// 相关标准
			List<JecnActiveStandardBeanT> standardList = jecnActiveData.getListJecnActiveStandardBeanT();
			if (standardList != null && standardList.size() != 0) {
				for (JecnActiveStandardBeanT standardBeanT : standardList) {
					Vector<Object> v = new Vector<Object>();
					v.add(standardBeanT.getStandardId());
					v.add(standardBeanT.getRelaName());
					v.add(JecnUtil.getStandardTypeName(standardBeanT.getRelaType()));
					this.table.getModel().addRow(v);
				}
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 获取文件类型图片
	 * 
	 * @param fileName
	 * @return
	 */
	private ImageIcon getFileTypeIcon(String fileName) {
		String elseFileIcon = "fileImage/" + FileTypeEnum.elsefile.toString() + ".gif";
		if (DrawCommon.isNullOrEmtryTrim(fileName)) {
			return JecnFileUtil.getImageIcon(elseFileIcon);
		}

		// 获取后缀名
		String afEx = getFileExtension(fileName);
		if (DrawCommon.isNullOrEmtryTrim(afEx)) {
			return JecnFileUtil.getImageIcon(elseFileIcon);
		}

		try {
			FileTypeEnum fileTypeEnum = FileTypeEnum.valueOf(afEx);
			return JecnFileUtil.getImageIcon("fileImage/" + fileTypeEnum.toString() + ".gif");
		} catch (Exception ex) {
			return JecnFileUtil.getImageIcon(elseFileIcon);
		}
	}

	/**
	 * 双击面板活动明细显示的文件名称，打开文件
	 */
	private void openSelectedFile() {
		int selectRow = table.getSelectedRow();
		if (selectRow == -1) {
			return;
		}
		// 打开文件
		JecnDesignerCommon.openFile(Long.valueOf(table.getValueAt(selectRow, 0).toString()));
	}

	/**
	 * 双击控制点
	 * 
	 * @author weidp
	 * @date 2014-10-28 下午05:20:00
	 */
	public void openControlPointDetail() {
		int selectRow = table.getSelectedRow();
		if (selectRow == -1) {
			return;
		}
		// 风险ID
		Long riskId = Long.valueOf(table.getValueAt(selectRow, 0).toString());
		JecnDesignerCommon.openRisk(riskId);
	}

	/**
	 * 双击标准
	 * 
	 * @author weidp
	 * @date 2014-10-28 下午04:09:23
	 */
	public void openStanardDetail() {
		int selectRow = table.getSelectedRow();
		if (selectRow == -1) {
			return;
		}
		// 获取标准类型
		int relateType = JecnUtil.getTypeByName(table.getValueAt(selectRow, 2).toString());
		// 标准ID
		long standardId = Long.valueOf(table.getValueAt(selectRow, 0).toString());
		// 打开标准
		JecnDesignerCommon.openStandard(standardId, relateType);
	}

	/**
	 * 
	 * 获取文件的扩展名
	 * 
	 * @param fileName
	 *            String
	 * @return String
	 */
	private String getFileExtension(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
	}

	/**
	 * 
	 * 表
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class JecnActiveFigurePopupTable extends JecnPopupTable {

		/**
		 * 
		 * 按键Enter或点击单元格动作事件处理方法
		 * 
		 * @param row
		 *            int 鼠标对应的行索引
		 * @param column
		 *            int 鼠标对应的列索引
		 * @param e
		 *            AWTEvent 鼠标事件或键盘事件 (MouseEvent/ActionEvent)
		 */
		@Override
		protected void cellEventProcess(int row, int column, AWTEvent e) {
			if (e instanceof MouseEvent) {
				MouseEvent evt = (MouseEvent) e;
				if (MouseEvent.MOUSE_CLICKED == evt.getID()) {
					if (evt.getClickCount() == 2) {// 鼠标双击
						// 双击标准文件
						if (type == 5) {
							openControlPointDetail();
						} else if (type == 6) {
							openStanardDetail();
						} else {
							openSelectedFile();
						}
					}
				}
			}
		}

		@Override
		protected Vector<Object> getRowVector(Object popupRowData) {
			return null;
		}

		@Override
		protected String[] getTableHeaderNames() {
			if (headerNameArray == null) {
				// 表头
				headerNameArray = new String[] { "id", JecnProperties.getValue("activeFigureFileName"), "" };
			}
			return headerNameArray;
		}
	}

}
