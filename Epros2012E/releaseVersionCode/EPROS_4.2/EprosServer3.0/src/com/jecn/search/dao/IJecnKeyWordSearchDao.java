package com.jecn.search.dao;

import java.util.List;
import java.util.Map;

public interface IJecnKeyWordSearchDao {
	List<Object[]> keyWordSearch(String searchName, int relatedType, int start, int pageSize) throws Exception;

	List<Map<String, Object>> keyWordSearchTotal(String searchName, int relatedType) throws Exception;
}
