package wordxml.element.dom.paragraph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.dom.IBody;
import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.graph.WordGraph;

/**
 * 操作段落节点类
 * 
 */
public class Paragraph extends IBody implements IBodyElement {
	
	public static final Paragraph DEFAULT = new Paragraph("");
	
	/*** 段落属性 ****/
	private ParagraphBean paragraphBean;
	/** run元素集合 */
	private List<Run> runs = new ArrayList<Run>(4);
	/** 所属 */
	private IBody parent;
	/** 书签 */
	private String bookMark;
	/*** 段落外部嵌入全文路径 ***/
	private String extFilePath;
	/****分页符标识*******/
	private boolean isWarp;
	
	public Paragraph(String text) {
		this.appendTextRun(text);
	}
	
	public Paragraph(boolean isWarp) {
		this.isWarp = isWarp;
	}
	
	public Paragraph(WordGraph graph) {
		this.appendGraphRun(graph);
	}

	/**
	 * 初始化段落（带样式）
	 * 
	 * @param paragraphBean
	 * @param parent
	 */
	public Paragraph(String text, ParagraphBean paragraphBean) {
		this.appendTextRun(text);
		this.paragraphBean = paragraphBean;
	}

	public Paragraph(WordGraph graph, ParagraphBean pBean) {
		this.appendGraphRun(graph);
		this.paragraphBean = pBean;
	}

	/**
	 * 获取所有运行元素
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午05:12:05
	 * @return
	 */
	public List<Run> getRuns() {
		return Collections.unmodifiableList(runs);
	}

	/**
	 * 创建图形
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午09:26:07
	 * @return
	 */
	public Run appendGraphRun(WordGraph graph) {
		return push(Run.newInstance(this, graph));
	}

	/**
	 * 创建一个运行元素（带文本）
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午05:00:23
	 * @param text
	 * @return
	 */
	public Run appendTextRun(String text) {
		return push(Run.newInstance(this, text, false));
	}

	/**
	 * 创建一个运行元素（带文本及样式）
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午05:00:23
	 * @param text
	 * @return
	 */
	public Run appendTextRun(String text, FontBean fontBean) {
		return push(Run.newInstance(this, text, false, fontBean));
	}

	/**
	 * 创建一个运行元素（带文本及样式）
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午05:00:23
	 * @param text
	 * @return
	 */
	public void appendTextRun(String[] text, FontBean fontBean) {
		if (text == null) {
			return;
		}
		for (String str : text) {
			appendTextRun(str, fontBean);
		}
	}

	/**
	 * 创建当前页Run元素
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午10:24:57
	 * @return
	 */
	public Run appendCurPageRun() {
		return push(Run.newInstance(this, "PAGE", true));
	}

	/**
	 * 创建总页数Run元素
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午10:25:14
	 * @return
	 */
	public Run appendTotalPagesRun() {
		return push(Run.newInstance(this, "NUMPAGES", true));
	}

	/**
	 * 创建总页数Run元素 减去首页封皮的页码
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午10:25:14
	 * @return
	 */
	public Run appendTotalPagesRun(int titleNumPages) {
		return push(Run.newInstance(this, "NUMPAGES", true, titleNumPages));
	}

	/**
	 * 创建当前节的总页数
	 * 
	 * @return
	 */
	public Run appendSectionTotalPagesRun() {
		return push(Run.newInstance(this, "SectionPages", true));
	}

	/**
	 * 添加Run元素至集合
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午10:30:38
	 * @param run
	 * @return
	 */
	private Run push(Run run) {
		this.runs.add(run);
		return run;
	}

	public ParagraphBean getParagraphBean() {
		return paragraphBean;
	}

	public void setParagraphBean(ParagraphBean paragraphBean) {
		this.paragraphBean = paragraphBean;
	}

	public IBody getParent() {
		return parent;
	}

	public void setParent(IBody parent) {
		this.parent = parent;
	}

	public String getBookMark() {
		return bookMark;
	}

	public void setBookMark(String bookMark) {
		this.bookMark = bookMark;
	}

	public String getExtFilePath() {
		return extFilePath;
	}

	public void setExtFilePath(String extFilePath) {
		this.extFilePath = extFilePath;
	}

	public boolean isWarp() {
		return isWarp;
	}

	public void setWarp(boolean isWarp) {
		this.isWarp = isWarp;
	}
	
	
	public static List<Paragraph> defaultParagraphs() {
		ArrayList<Paragraph> p = new ArrayList<Paragraph>();
		p.add(DEFAULT);
		return p;
	}
}
