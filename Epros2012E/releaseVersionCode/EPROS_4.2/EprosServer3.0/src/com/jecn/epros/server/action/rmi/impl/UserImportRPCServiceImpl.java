package com.jecn.epros.server.action.rmi.impl;

import java.io.File;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ctc.wstx.util.StringUtil;
import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.system.SyncConfig;
import com.jecn.epros.server.action.web.dataImport.sync.excelorDB.UserSyncAction;
import com.jecn.epros.server.action.web.timer.UserAutoBuss;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ImportDataFactory;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.PersonnelChangesFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserDataFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserErrorDataFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserImportBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserModelFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.WebConfigBean;
import com.jecn.epros.service.IUserImportRPCService;

public class UserImportRPCServiceImpl implements IUserImportRPCService {

	protected final Log log = LogFactory.getLog(UserImportRPCServiceImpl.class);
	/** 对应的业务处理类 */
	private UserModelFileDownBuss userModelFileDownBuss = null;
	/** 对应的业务处理类 */
	private UserErrorDataFileDownBuss userErrorDataFileDownBuss = null;
	/** 对应的业务处理类 */
	private PersonnelChangesFileDownBuss personnelChangesFileDownBuss = null;
	/** 对应的业务处理类 */
	private UserDataFileDownBuss userDataFileDownBuss = null;
	/** 对应的业务处理类 */
	private UserImportBuss userImportBuss = null;
	/** 定时器处理类 */
	protected UserAutoBuss userAutoBuss;
	/** 获取外来的待导入数据对象 */
	protected ImportDataFactory importDataFactory = null;

	@Override
	public ByteFileResult downUserChangeData() {
		File file = new File(personnelChangesFileDownBuss.getOutFilepath());
		ByteFileResult byteFile = new ByteFileResult();
		if (file.exists()) {
			byteFile.setFileName("inputUserChangeExcel.xls");
			byteFile.setBytes(JecnUtil.getByte(file));
		} else {
			byteFile.setSuccess(false);
			byteFile.setResultMessage(SyncErrorInfo.PERSONNEL_CHANGES_DOWN_FAIL);
		}
		return byteFile;
	}

	@Override
	public ByteFileResult downUserData() {
		// 保存数据到本地
		boolean ret = userDataFileDownBuss.downUserDataExcel();
		File file = new File(userDataFileDownBuss.getOutputExcelDataPath());
		ByteFileResult byteFile = new ByteFileResult();
		if (ret && file.exists()) {
			byteFile.setFileName("inputUserDataExcel.xls");
			byteFile.setBytes(JecnUtil.getByte(file));
		} else {
			byteFile.setResultMessage(SyncErrorInfo.USER_FILE_DOWN_FAIL);
			byteFile.setSuccess(false);
		}
		return byteFile;
	}

	@Override
	public ByteFileResult downUserErrorData() {
		File file = new File(userErrorDataFileDownBuss.getOutFilepath());
		ByteFileResult byteFile = new ByteFileResult();
		if (file.exists()) {
			byteFile.setFileName("inputUserErrorExcel.xls");
			byteFile.setBytes(JecnUtil.getByte(file));
		} else {
			byteFile.setResultMessage(SyncErrorInfo.ERROR_FILE_NOT_EXISTS);
			byteFile.setSuccess(false);
		}
		return byteFile;
	}

	@Override
	public ByteFileResult downUserTemplet() {
		ByteFileResult byteFile = new ByteFileResult();
		File file = new File(userModelFileDownBuss.getOutFilepath());
		if (file.exists()) {
			byteFile.setFileName("inputUserMouldExcel.xls");
			byteFile.setBytes(JecnUtil.getByte(file));
		} else {
			byteFile.setResultMessage(SyncErrorInfo.FILE_SAVE_LOCAL_FAIL);
			byteFile.setSuccess(false);
		}
		return byteFile;
	}

	@Override
	public MessageResult importUserData(ByteFile file) {
		File tempFile = null;
		MessageResult result = new MessageResult();
		try {
			// 写出到临时文件
			// 临时文件存储路径
			tempFile = JecnFinal.writeFileToTempDir(file.getBytes());
			result = userImportBuss.importDataBuss(false, SyncConstant.IMPORT_TYPE_EXCEL, tempFile, tempFile.getName());
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			if (e.getMessage() != null) {
				result.setResultMessage(e.getMessage());
			} else {
				result.setResultMessage("人员同步失败");
			}
		}
		return result;
	}

	@Override
	public MessageResult importUserDataByDB() {
		MessageResult result = new MessageResult();
		try {
			// 获取同步类型
			result = userImportBuss.importDataBuss(false, getImportTypeByData(), null, null);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			if (e.getMessage() != null) {
				result.setResultMessage(e.getMessage());
			} else {
				result.setResultMessage("人员同步失败");
			}
		}
		return result;
	}

	/**
	 * 根据DB标识获取人员同步类型
	 * 
	 * @return
	 */
	private String getImportTypeByData() {
		String importType = null;
		int tmpValue = JecnContants.otherUserSyncType;
		switch (tmpValue) {
		case 2:// DB
		case 9:// 岗位组
			importType = SyncConstant.IMPORT_TYPE_DB;
			break;
		case 4:// webService
			importType = SyncConstant.IMPORT_TYPE_WEBSERVICE;
			break;
		case 5:// sapHr
			importType = SyncConstant.IMPORT_TYPE_SAP_HR;
			break;
		case 6:// XML+岗位匹配 【大唐】
			importType = SyncConstant.IMPORT_TYPE_XML;
			break;
		case 7:// ad（Ldap数据读取）
			importType = SyncConstant.IMPORT_TYPE_AD;
			break;
		case 8:// DB 附带基准岗位
			importType = SyncConstant.IMPORT_TYPE_BASE_DB;
			break;
		}
		return importType;
	}

	@Override
	public MessageResult saveSyncConfig(Map<String, Object> params) {

		String startTime = params.get("startTime").toString();
		String intervalDay = params.get("intervalDay").toString();
		String isAuto = params.get("isAuto").toString();

		MessageResult result = userAutoBuss.saveParams(startTime, intervalDay, isAuto);
		if (!result.isSuccess()) {
			return result;
		}

		// 同步定时器
		UserSyncAction userSyncAction = (UserSyncAction) ApplicationContextUtil.getContext().getBean("userSyncAction");
		userSyncAction.initParams();
		AbstractConfigBean absConfigBean = userSyncAction.getAbstractConfigBean();
		if ("1".equals(absConfigBean.getIaAuto())) {// 自动执行定时器
			userSyncAction.reStartTime();
		}

		return new MessageResult(true, "保存配置数据成功！");
	}

	@Override
	public SyncConfig getSyncConfig() {
		SyncConfig config = new SyncConfig();
		config.setSyncType(JecnContants.otherUserSyncType);
		if (config.getSyncType() == 1) {// EXCEL方式
			return config;
		}
		try {
			AbstractConfigBean initTime = importDataFactory.getInitTime(getImportTypeByData());
			config.setIntervalDay(StringUtils.isBlank(initTime.getIntervalDay().trim()) ? 1 : Integer.valueOf(initTime
					.getIntervalDay().trim()));
			config.setIsAuto(StringUtils.isBlank(initTime.getIaAuto().trim()) ? 1 : Integer.valueOf(initTime
					.getIaAuto().trim()));
			config.setStartTime(initTime.getStartTime());
		} catch (Exception e) {
			log.error("", e);
		}
		return config;
	}

	public UserModelFileDownBuss getUserModelFileDownBuss() {
		return userModelFileDownBuss;
	}

	public void setUserModelFileDownBuss(UserModelFileDownBuss userModelFileDownBuss) {
		this.userModelFileDownBuss = userModelFileDownBuss;
	}

	public UserErrorDataFileDownBuss getUserErrorDataFileDownBuss() {
		return userErrorDataFileDownBuss;
	}

	public void setUserErrorDataFileDownBuss(UserErrorDataFileDownBuss userErrorDataFileDownBuss) {
		this.userErrorDataFileDownBuss = userErrorDataFileDownBuss;
	}

	public PersonnelChangesFileDownBuss getPersonnelChangesFileDownBuss() {
		return personnelChangesFileDownBuss;
	}

	public void setPersonnelChangesFileDownBuss(PersonnelChangesFileDownBuss personnelChangesFileDownBuss) {
		this.personnelChangesFileDownBuss = personnelChangesFileDownBuss;
	}

	public UserDataFileDownBuss getUserDataFileDownBuss() {
		return userDataFileDownBuss;
	}

	public void setUserDataFileDownBuss(UserDataFileDownBuss userDataFileDownBuss) {
		this.userDataFileDownBuss = userDataFileDownBuss;
	}

	public UserImportBuss getUserImportBuss() {
		return userImportBuss;
	}

	public void setUserImportBuss(UserImportBuss userImportBuss) {
		this.userImportBuss = userImportBuss;
	}

	public UserAutoBuss getUserAutoBuss() {
		return userAutoBuss;
	}

	public void setUserAutoBuss(UserAutoBuss userAutoBuss) {
		this.userAutoBuss = userAutoBuss;
	}

	public ImportDataFactory getImportDataFactory() {
		return importDataFactory;
	}

	public void setImportDataFactory(ImportDataFactory importDataFactory) {
		this.importDataFactory = importDataFactory;
	}

}
