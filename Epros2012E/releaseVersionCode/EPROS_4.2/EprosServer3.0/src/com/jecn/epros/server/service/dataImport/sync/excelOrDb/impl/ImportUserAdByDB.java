package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DBConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserDomainNameBean;

/**
 * 由于Epros系统中loginName存放的是工号， 部分公司公司需要将工号更新成域账号，原先的查询不满足当前要求由此出现此类
 * 
 * @author chehuanbo
 * 
 * */
public class ImportUserAdByDB extends ImportUserByDB {

	Logger log = Logger.getLogger(ImportUserAdByDB.class);

	/** 工号和与账号对应集合 */
	private List<UserDomainNameBean> domainNameBeans = new ArrayList<UserDomainNameBean>();

	public ImportUserAdByDB(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}
	
	
	/**
	 * 
	 * 获取人员、部门、工号和域账号对应 数据集合
	 * @author chehuanbo
	 * @return
	 */
	public List<UserDomainNameBean> getDomainUser() throws Exception {
		// 没有执行过执行
		if (!readedFlag) {
			try {
				selectDB((DBConfigBean) configBean);
			} catch (Exception e) {
				throw new IOException(SyncErrorInfo.SELECT_DB_FAIL);
			}
		}
		return domainNameBeans;
	}
    
	/**
	 * 
	 * 查询数据库返回查询结果
	 * 
	 * @param configBean
	 */
	private void selectDB(DBConfigBean configBean) throws Exception {
		// 工号和域账号对应表结果集
		ResultSet resLoginUser = null;
		Statement stmt = null;
		Connection conn = null;
		try {
			// 加载的驱动类
			Class.forName(configBean.getDriver());
			// 创建数据库连接
			conn = DriverManager.getConnection(configBean.getUrl(), configBean
					.getUsername(), configBean.getPassword());
               
			// 创建一个Statement
			stmt = conn.createStatement();
			// 获取工号和域账号，返回结果集
			resLoginUser = stmt.executeQuery(configBean.getUserSql());
			getDomainUser(resLoginUser);
			readedFlag = true;
		} catch (ClassNotFoundException e) {
			log.error("ImportUserByDB类的selectDB方法加驱动时出错：" + e);
			throw e;
		} catch (SQLException e) {
			log.error("ImportUserByDB类的selectDB方法创建连接时出错：" + e);
			throw e;
		} finally {
			if(resLoginUser!=null){//关闭记录集
				try {
					resLoginUser.close();
				} catch (Exception e) {
					log.error("ImportUserByDB类的selectDB方法关闭resLoginUser记录集时出错：", e);
				}
			}
			if (stmt != null) { // 关闭声明
				try {
					stmt.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭声明（Statement）时出错："
							+ e);
					throw e;
				}
			}
			if (conn != null) { // 关闭连接对象
				try {
					conn.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭连接对象（Connection）时出错："+ e);
					throw e;
				}
			}
		}
	}

	/**
	 * 将结果集存放到数据bean中
	 * 
	 * @author chehuanbo
	 * 
	 * **/

	private void getDomainUser(ResultSet resultSet) {
		try {
			while (resultSet.next()) {
				UserDomainNameBean domainNameBean = new UserDomainNameBean();
				domainNameBean.setLoginName(resultSet.getString(0));
				domainNameBean.setUserName(resultSet.getString(1));
				domainNameBeans.add(domainNameBean);
			}
		} catch (SQLException e) {
			log.error("人员同步结果集转换bean出现异常" + this + "getDomainUser() line:135",e);
		}
	}

}
