package com.jecn.epros.server.dao.rule.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.JecnFlowCommon;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfo;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.rule.JecnRuleCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleContent;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleFile;
import com.jecn.epros.server.bean.rule.RuleFileT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.webBean.WebLoginBean;

public class RuleDaoImpl extends AbsBaseDao<RuleT, Long> implements IRuleDao {

	@Override
	public void deleteRules(Set<Long> setIds) throws Exception {
		// 制度内容 -设计器
		String hql = "delete from RuleContentT where ruleTitleId in (select id from RuleTitleT where ruleId in";
		List<String> listStr = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度内容 -浏览端
		hql = "delete from RuleContent where ruleTitleId in (select id from RuleTitle where ruleId in";
		listStr = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度文件-设计器
		hql = "delete from RuleFileT where ruleTitleId in (select id from RuleTitleT where ruleId in";
		listStr = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度文件-浏览端
		hql = "delete from RuleFile where ruleTitleId in (select id from RuleTitle where ruleId in";
		listStr = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

		// 制度标题-设计器
		hql = "delete from RuleTitleT where ruleId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度标题-设计器
		hql = "delete from RuleTitle where ruleId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度图标 设计器
		hql = "update JecnFlowStructureImageT set linkFlowId = -1 where figureType=" + JecnCommonSql.getSystemString()
				+ " and linkFlowId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

		hql = "update JecnFlowStructureImage set linkFlowId = -1 where figureType=" + JecnCommonSql.getSystemString()
				+ " and linkFlowId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

		// 岗位与制度
		hql = "delete from JecnPositionRefSystem where systemId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

		// 制度与流程-设计端
		hql = "delete from FlowCriterionT where criterionClassId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度与流程-浏览端
		hql = "delete from FlowCriterion where criterionClassId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

		// 制度与标准-设计端
		hql = "delete from JecnRuleStandardBeanT where ruleId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度与标准-浏览端
		hql = "delete from JecnRuleStandardBean where ruleId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度与风险-设计端
		hql = "delete from JecnRuleRiskBeanT where ruleId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度与风险-浏览端
		hql = "delete from JecnRuleRiskBean where ruleId in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

		// 查阅权限
		JecnUtil.deleteViewAuth(setIds, 3, this);
		// 设计器权限
		JecnUtil.deleteDesignAuth(setIds, 3, this);
		// 任务
		JecnUtil.deleteTask(setIds, 2, this);
		JecnUtil.deleteTask(setIds, 3, this);
		// 文控信息
		JecnUtil.deleteRecord(setIds, 2, this);
		JecnUtil.deleteRecord(setIds, 3, this);
		// 删除制度文件内容表
		hql = "delete from G020RuleFileContent where ruleId in ";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 删除收藏
		String sql = "delete from jecn_my_star where type=2 and related_id in";
		listStr = JecnCommonSql.getSetSqls(sql, setIds);
		for (String str : listStr) {
			execteNative(str);
		}
		// /制度-设计器
		hql = "delete from RuleT where id in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}
		// 制度-浏览端
		hql = "delete from Rule where id in";
		listStr = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

		// 删除合理化建议 0流程，1制度，2流程架构，3文件，4风险，5标准
		JecnDaoUtil.deleteProposeByRelatedIds(setIds, 1, this);

	}

	@Override
	public void deleteRules(Long projectId) throws Exception {
		String hql = "select id from RuleT where projectId=?";
		List<Long> list = this.listHql(hql, projectId);
		Set<Long> setIds = new HashSet<Long>();
		for (Long id : list) {
			if (id != null) {
				setIds.add(id);
			}
		}
		if (setIds.size() > 0) {
			this.deleteRules(setIds);
		}
	}

	private void revokeRuleCommonData(List<Long> listIds) throws Exception {
		// 删除岗位查阅权限
		String sql = "delete from jecn_access_permissions where type=3 and RELATE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 删除部门查阅权限
		sql = "delete from JECN_ORG_ACCESS_PERMISSIONS where type=3 and RELATE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 删除岗位组查阅权限
		sql = "delete from JECN_GROUP_PERMISSIONS where type=3 and RELATE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 删除制度相关风险
		sql = "DELETE FROM Jecn_Rule_Risk WHERE RULE_ID ";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 删除制度相关标准
		sql = "DELETE FROM JECN_RULE_STANDARD WHERE RULE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 删除制度相关制度
		sql = "DELETE FROM JECN_RULE_RELATED_RULE WHERE RULE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 删除制度信息表
		sql = "delete from JECN_RULE_BASIC_INFO where RULE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 删除组织范围
		sql = "delete from JECN_RULE_APPLY_ORG where RELATED_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 删除制度对象的信息
		sql = "delete from JECN_RULE where id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
	}

	@Override
	public void revokeRuleFileData(List<Long> listIds) throws Exception {
		this.revokeRuleCommonData(listIds);
		// 删除制度对象的信息
		String sql = "delete from RULE_STANDARDIZED_FILE where RELATED_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

	}

	/**
	 * 发布制度-common sql
	 * 
	 * @param listIds
	 * @throws Exception
	 */
	private void releaseRuleCommonData(List<Long> listIds) throws Exception {
		// 岗位查阅权限
		String sql = "insert into jecn_access_permissions(ID, FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE) select ID, FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE from jecn_access_permissions_t  where type=3 and RELATE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			this.execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 部门查阅权限
		sql = "insert into JECN_ORG_ACCESS_PERMISSIONS(ID, ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE) select ID, ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE from JECN_ORG_ACCESS_PERM_T where type=3 and RELATE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			this.execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 岗位组查阅权限
		sql = "insert into JECN_GROUP_PERMISSIONS(ID, POSTGROUP_ID, RELATE_ID, TYPE,ACCESS_TYPE) select ID, POSTGROUP_ID, RELATE_ID, TYPE,ACCESS_TYPE from JECN_GROUP_PERMISSIONS_T where type=3 and RELATE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			this.execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 添加制度相关风险
		sql = "INSERT INTO JECN_RULE_RISK(ID,RULE_ID,RISK_ID) SELECT ID,RULE_ID,RISK_ID FROM JECN_RULE_RISK_T WHERE RULE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 添加制度相关制度
		sql = "INSERT INTO JECN_RULE_RELATED_RULE(GUID,RULE_ID,RELATED_ID) SELECT GUID,RULE_ID,RELATED_ID FROM JECN_RULE_RELATED_RULE_T WHERE RULE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 添加制度相关标准
		sql = "INSERT INTO JECN_RULE_STANDARD (ID,RULE_ID,STANDARD_ID,RELA_TYPE,CLAUSE_ID) SELECT ID,RULE_ID,STANDARD_ID,RELA_TYPE,CLAUSE_ID FROM JECN_RULE_STANDARD_T WHERE RULE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 添加制度基本信息表
		sql = "insert into JECN_RULE_BASIC_INFO(RULE_ID,BUSINESS_SCOPE,OTHER_SCOPE,WRITE_DEPT,"
				+ " DRAFTMAN ,DRAFTING_UNIT ,TEST_RUN_FILE,CHANGE_VERSION_EXPLAIN ,PURPOSE ,APPLICABILITY) "
				+ " select RULE_ID,BUSINESS_SCOPE,OTHER_SCOPE,WRITE_DEPT ,DRAFTMAN ,DRAFTING_UNIT ,"
				+ " TEST_RUN_FILE,CHANGE_VERSION_EXPLAIN ,PURPOSE ,APPLICABILITY from JECN_RULE_BASIC_INFO_T where RULE_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 添加制度组织范围
		sql = "insert into JECN_RULE_APPLY_ORG(ID,RELATED_ID,ORG_ID) select ID,RELATED_ID,ORG_ID from JECN_RULE_APPLY_ORG_T where RELATED_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		// 添加制度对象的信息
		sql = "insert into JECN_RULE (ID, PROJECT_ID, PER_ID, RULE_NUMBER, RULE_NAME, IS_PUBLIC, TYPE_ID, IS_DIR, ORG_ID, SORT_ID, FILE_ID, TEMPLATE_ID, TEST_RUN_NUMBER, CREATE_DATE, CREATE_PEOPLE_ID, UPDATE_DATE, UPDATE_PEOPLE_ID, HISTORY_ID,RES_PEOPLE_ID,TYPE_RES_PEOPLE,IS_FILE_LOCAL,T_PATH,T_LEVEL,PUB_TIME,VIEW_SORT,CONFIDENTIALITY_LEVEL,EXPIRY,WARD_PEOPLE_ID,DEL_STATE,KEYWORD,COMMISSIONER_ID ,RULE_URL,EFFECTIVE_DATE,FICTION_PEOPLE_ID) "
				+ "  select ID, PROJECT_ID, PER_ID, RULE_NUMBER, RULE_NAME, IS_PUBLIC, TYPE_ID, IS_DIR, ORG_ID, SORT_ID, FILE_ID, TEMPLATE_ID, TEST_RUN_NUMBER, CREATE_DATE, CREATE_PEOPLE_ID, UPDATE_DATE, UPDATE_PEOPLE_ID, HISTORY_ID,RES_PEOPLE_ID,TYPE_RES_PEOPLE,IS_FILE_LOCAL,T_PATH,T_LEVEL,PUB_TIME,VIEW_SORT,CONFIDENTIALITY_LEVEL,EXPIRY,WARD_PEOPLE_ID,DEL_STATE,KEYWORD,COMMISSIONER_ID ,RULE_URL,EFFECTIVE_DATE,FICTION_PEOPLE_ID from JECN_RULE_T  where id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

	}

	@Override
	public void releaseRuleFileData(List<Long> listIds) throws Exception {
		// 删除老版本的数据
		this.revokeRuleFileData(listIds);
		// 标准化文件
		String sql = " INSERT INTO RULE_STANDARDIZED_FILE (ID,RELATED_ID,FILE_ID) "
				+ "SELECT ID,RELATED_ID,FILE_ID FROM RULE_STANDARDIZED_FILE_T WHERE RELATED_ID";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}

		this.releaseRuleCommonData(listIds);// 正式表

	}

	@Override
	public void revokeRuleModeData(List<Long> listIds) throws Exception {
		// 删除制度文件
		String sql = "delete from RULE_FILE where RULE_TITLE_ID in (select rt.id from rule_title rt where rule_id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0) + ")";
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqlAddBracket(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 删除制度内容
		sql = "delete from rule_content where RULE_TITLE_ID in (select rt.id from rule_title rt where rule_id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0) + " )";
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqlAddBracket(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 删除制度的标题
		sql = "delete from rule_title where rule_id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		this.revokeRuleCommonData(listIds);
	}

	@Override
	public void releaseRuleModeData(List<Long> listIds) throws Exception {
		this.revokeRuleModeData(listIds);
		// 添加制度文件
		String sql = "insert into RULE_FILE(ID, RULE_TITLE_ID, RULE_FILE_ID)"
				+ "       select rft.ID,rft.RULE_TITLE_ID,rft.RULE_FILE_ID from RULE_FILE_T rft,RULE_TITLE_T rtt where rft.rule_title_id=rtt.id"
				+ "       and rtt.rule_id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 添加制度内容
		sql = "insert into rule_content(ID,RULE_TITLE_ID,CONTENT) "
				+ "       select rft.ID,rft.RULE_TITLE_ID,rft.CONTENT from rule_content_t rft,RULE_TITLE_T rtt where rft.rule_title_id=rtt.id"
				+ "       and rtt.rule_id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		// 添加制度的标题
		sql = "insert into rule_title(ID, RULE_ID, TITLE_NAME, ORDER_NUMBER, TYPE,EN_NAME) "
				+ " select ID,RULE_ID,TITLE_NAME,ORDER_NUMBER,TYPE,EN_NAME from rule_title_t  where rule_id";
		if (listIds.size() == 1) {
			sql = sql + " = " + listIds.get(0);
			execteNative(sql);
		} else {
			sql = sql + " in ";
			List<String> listStr = JecnCommonSql.getListSqls(sql, listIds);
			for (String str : listStr) {
				execteNative(str);
			}
		}
		this.releaseRuleCommonData(listIds);// 制度正式表
	}

	/**
	 * 查询制度标题信息
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @throws Exception
	 */
	public List<RuleTitleT> findRuleTitle(Long ruleId) throws Exception {
		String hql = "from RuleTitleT where ruleId=? order by orderNumber";
		List<RuleTitleT> listRuleTitleT = this.listHql(hql, ruleId);
		return listRuleTitleT;
	}

	/**
	 * 查询制度操作说明内容 (临时表)
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleTitleId
	 *            制度标题ID
	 * @throws Exception
	 */
	public List<RuleContentT> findRuleContentT(Long ruleTitleId) throws Exception {
		String hql = "select a from RuleContentT as a,RuleTitleT as b where a.ruleTitleId = b.id and b.ruleId = ?";
		List<RuleContentT> listRuleContentTData = this.listHql(hql, ruleTitleId);
		List<RuleContentT> listResultRuleT = new ArrayList<RuleContentT>();
		for (RuleContentT ruleContentT : listRuleContentTData) {
			RuleContentT ruleContentTTmp = new RuleContentT();
			ruleContentTTmp.setId(ruleContentT.getId());
			ruleContentTTmp.setRuleTitleId(ruleContentT.getRuleTitleId());
			ruleContentTTmp.setContentStr(ruleContentT.getContentStr());
			listResultRuleT.add(ruleContentTTmp);
		}
		return listResultRuleT;
	}

	/**
	 * 查询制度操作说明内容 (非临时表)
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleTitleId
	 *            制度标题ID
	 * @throws Exception
	 */
	public List<RuleContent> findRuleContent(Long ruleTitleId) throws Exception {
		String hql = "select a from RuleContent as a,RuleTitleT as b where a.ruleTitleId = b.id and b.ruleId = ?";
		List<RuleContent> listRuleContentData = this.listHql(hql, ruleTitleId);
		List<RuleContent> listResultRule = new ArrayList<RuleContent>();
		for (RuleContent ruleContent : listRuleContentData) {
			RuleContent ruleContentTmp = new RuleContent();
			ruleContentTmp.setId(ruleContent.getId());
			ruleContentTmp.setRuleTitleId(ruleContent.getRuleTitleId());

			ruleContentTmp.setContentStr(ruleContent.getContentStr());
			listResultRule.add(ruleContentTmp);
		}
		return listResultRule;
	}

	/**
	 * 查询制度操作说明文件信息 （临时表）
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleTitleId
	 * @return
	 * @throws Exception
	 */
	public List<RuleFileT> findRuleFileT(Long ruleId) throws Exception {
		// 通过制度ID找到制度文件
		List<RuleFileT> listRuleFileT = new ArrayList<RuleFileT>();
		StringBuffer str = new StringBuffer();
		str.append("select a.id,a.ruleTitleId,a.ruleFileId,c.fileName,c.docId");
		str.append(" from RuleFileT as a,RuleTitleT as b,JecnFileBeanT as c where");
		str.append(" a.ruleTitleId = b.id and b.ruleId = ?");
		str.append(" and a.ruleFileId = c.fileID and c.delState=0 ");
		List<Object[]> listObjects = this.listHql(str.toString(), ruleId);
		for (Object[] obj : listObjects) {
			if (obj[0] != null && obj[1] != null && obj[2] != null) {
				RuleFileT ruleFileT = new RuleFileT();
				ruleFileT.setId(Long.valueOf(obj[0].toString()));
				ruleFileT.setRuleTitleId(Long.valueOf(obj[1].toString()));
				ruleFileT.setRuleFileId(Long.valueOf(obj[2].toString()));
				String fileName = "";
				if (obj[3] != null && !"".equals(obj[3].toString())) {
					fileName = obj[3].toString();
				}
				String fileInput = "";
				if (obj[4] != null && !"".equals(obj[4].toString())) {
					fileInput = obj[4].toString();
				}
				ruleFileT.setFileInput(fileInput);
				ruleFileT.setFileName(fileName);
				listRuleFileT.add(ruleFileT);
			}
		}
		return listRuleFileT;
	}

	/**
	 * 查询制度操作说明文件信息 （非临时表）
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @return
	 * @throws Exception
	 */
	public List<RuleFile> findRuleFile(Long ruleId) throws Exception {
		// 通过制度ID找到制度文件
		List<RuleFile> listRuleFile = new ArrayList<RuleFile>();
		StringBuffer str = new StringBuffer();
		str.append("select a.id,a.ruleTitleId,a.ruleFileId,c.fileName,c.docId");
		str.append(" from RuleFile as a,RuleTitleT as b,JecnFileBean as c where");
		str.append(" a.ruleTitleId = b.id and b.ruleId = ?");
		str.append(" and a.ruleFileId = c.fileID and c.delState=0 ");
		List<Object[]> listObjects = this.listHql(str.toString(), ruleId);
		for (Object[] obj : listObjects) {
			if (obj[0] != null && obj[1] != null && obj[2] != null) {
				RuleFile ruleFile = new RuleFile();
				ruleFile.setId(Long.valueOf(obj[0].toString()));
				ruleFile.setRuleTitleId(Long.valueOf(obj[1].toString()));
				ruleFile.setRuleFileId(Long.valueOf(obj[2].toString()));
				String fileName = "";
				if (obj[3] != null && !"".equals(obj[3].toString())) {
					fileName = obj[3].toString();
				}
				String fileInput = "";
				if (obj[4] != null && !"".equals(obj[4].toString())) {
					fileInput = obj[4].toString();
				}
				ruleFile.setFileInput(fileInput);
				ruleFile.setFileName(fileName);
				listRuleFile.add(ruleFile);
			}
		}
		return listRuleFile;
	}

	/**
	 * 查询制度操作说明流程信息
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 * @param isOpration
	 *            是否是操作说明使用的数据0:是，1：否
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowCommon> findRuleFlow(Long ruleId, Long isFlow, int isOpration, boolean isPub) throws Exception {
		// 通过制度ID找到制度流程
		List<JecnFlowCommon> listJecnFlowCommon = new ArrayList<JecnFlowCommon>();
		String isPubStr = isPub ? " " : "_t";
		StringBuffer str = new StringBuffer();
		List<Object[]> listObjects = null;
		if (isOpration == 0) {
			str.append("select a.flow_id, a.flow_name, a.flow_id_input, c.id");
			str.append("  from jecn_flow_structure" + isPubStr + "  a, FLOW_RELATED_CRITERION" + isPubStr
					+ "  b, rule_title_t  c");
			str.append(" where b.flow_id = a.flow_id");
			str.append("   and a.del_state = 0");
			str.append("   and b.criterion_class_id = c.rule_id");
			str.append("   and c.rule_id = ?");
			str.append("   and a.isflow = ?");
			listObjects = this.listNativeSql(str.toString(), ruleId, isFlow);
			for (Object[] obj : listObjects) {
				if (obj[0] != null && obj[3] != null) {
					JecnFlowCommon jecnFlowCommon = new JecnFlowCommon();
					jecnFlowCommon.setFlowId(Long.valueOf(obj[0].toString()));
					String flowName = "";
					if (obj[1] != null && !"".equals(obj[1].toString())) {
						flowName = obj[1].toString();
					}
					jecnFlowCommon.setFlowName(flowName);
					String flowInput = "";
					if (obj[2] != null && !"".equals(obj[2].toString())) {
						flowInput = obj[2].toString();
					}
					jecnFlowCommon.setFlowNumber(flowInput);
					jecnFlowCommon.setRuleTitleId(Long.valueOf(obj[3].toString()));
					listJecnFlowCommon.add(jecnFlowCommon);
				}
			}
		} else {
			str.append("select a.flowId,a.flowName,a.flowIdInput");
			str.append(" from JecnFlowStructure as a,FlowCriterion as b where");
			str.append(" b.flowId = a.flowId and a.delState = 0");
			str.append(" and b.criterionClassId = ? and a.isFlow = ?");
			listObjects = this.listHql(str.toString(), ruleId, isFlow);
			for (Object[] obj : listObjects) {
				if (obj[0] != null) {
					JecnFlowCommon jecnFlowCommon = new JecnFlowCommon();
					jecnFlowCommon.setFlowId(Long.valueOf(obj[0].toString()));
					String flowName = "";
					if (obj[1] != null && !"".equals(obj[1].toString())) {
						flowName = obj[1].toString();
					}
					jecnFlowCommon.setFlowName(flowName);
					String flowInput = "";
					if (obj[2] != null && !"".equals(obj[2].toString())) {
						flowInput = obj[2].toString();
					}
					jecnFlowCommon.setFlowNumber(flowInput);
					listJecnFlowCommon.add(jecnFlowCommon);
				}
			}
		}
		return listJecnFlowCommon;
	}

	@Override
	public List<Long> findRuleFileNoPubFiles(Long ruleId) throws Exception {
		return JecnUtil.findRuleFileNoPubFiles(ruleId, this);
	}

	@Override
	public List<Long> findRuleModeNoPubFiles(Long ruleId) throws Exception {
		return JecnUtil.findRuleModeNoPubFiles(ruleId, this);
	}

	@Override
	public Rule getRuleById(Long id) throws Exception {
		return (Rule) this.getSession().get(Rule.class, id);
	}

	@Override
	public RuleT getRuleTById(Long id) throws Exception {
		return (RuleT) this.getSession().get(RuleT.class, id);
	}

	@Override
	public List<Object[]> getRuleRelateFlows(Long id, boolean isPub) throws Exception {
		if (isPub) {
			String sql = "select jfrc.flow_id,jfs.flow_name,jfs.flow_id_input"
					+ "       ,org.org_id,org.org_name"
					+ "       ,jfbi.res_people_id,jfbi.res_people_id"
					+ "       ,case when jfbi.type_res_people=0 then ju.true_name"
					+ "        when jfbi.type_res_people=1 then jfoi.figure_text"
					+ "        else null end as res_people_name,jfs.update_date,jfs.is_public,jfs.create_date"
					+ "       from jecn_flow_structure jfs"
					+ "       ,jecn_flow_basic_info jfbi"
					+ "       left join (select jfro.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) org on org.flow_id=jfbi.flow_id"
					+ "       left join jecn_flow_org_image jfoi on jfoi.figure_id=jfbi.res_people_id"
					+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
					+ ",FLOW_RELATED_CRITERION jfrc"
					+ "       where jfrc.flow_id = jfs.flow_id and jfs.del_state=0 and jfs.flow_id=jfbi.flow_id and jfrc.criterion_class_id=?";
			return this.listNativeSql(sql, id);
		} else {
			String sql = "select jfrc.flow_id,jfs.flow_name,jfs.flow_id_input"
					+ "       ,org.org_id,org.org_name"
					+ "       ,jfbi.res_people_id,jfbi.res_people_id"
					+ "       ,case when jfbi.type_res_people=0 then ju.true_name"
					+ "        when jfbi.type_res_people=1 then jfoi.figure_text"
					+ "        else null end as res_people_name,jfs.update_date,jfs.is_public,jfs.create_date"
					+ "       from jecn_flow_structure_t jfs"
					+ "       ,jecn_flow_basic_info_t jfbi"
					+ "       left join (select jfro.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) org on org.flow_id=jfbi.flow_id"
					+ "       left join jecn_flow_org_image jfoi on jfoi.figure_id=jfbi.res_people_id"
					+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
					+ ",FLOW_RELATED_CRITERION jfrc"
					+ "       where jfrc.flow_id = jfs.flow_id and jfs.del_state=0 and jfs.flow_id=jfbi.flow_id and jfrc.criterion_class_id=?";
			return this.listNativeSql(sql, id);
		}
	}

	@Override
	public void reNameRuleByRelationId(String newName, Long relationId) {
		String hql = "update  RuleT set ruleName=? where fileId=? and isFileLocal=?";
		this.execteHql(hql, newName, relationId, 0);
		hql = "update  Rule set ruleName=? where fileId=? and isFileLocal=?";
		this.execteHql(hql, newName, relationId, 0);
	}

	@Override
	public List<JecnRiskCommon> findRuleRisk(Long ruleId, int isOpration) throws Exception {
		// 通过制度ID找到制度风险
		List<JecnRiskCommon> listJecnRiskCommon = new ArrayList<JecnRiskCommon>();
		List<Object[]> listObjects = null;
		String str = "";
		if (isOpration == 0) {
			str = "select jr.id,jr.name,jr.risk_code,rt.id as titleId  from jecn_risk  jr,JECN_RULE_RISK_T  jrr,RULE_TITLE_T  rt "
					+ "  where jr.id = jrr.RISK_ID and jrr.RULE_ID = rt.RULE_ID and rt.rule_id=? and rt.type = 5 order by jrr.id";
			listObjects = this.listNativeSql(str, ruleId);
			for (Object[] obj : listObjects) {
				if (obj[0] != null && obj[3] != null) {
					JecnRiskCommon jecnRiskCommon = new JecnRiskCommon();
					jecnRiskCommon.setRiskId(Long.valueOf(obj[0].toString()));
					String riskName = "";
					if (obj[1] != null && !"".equals(obj[1].toString())) {
						riskName = obj[1].toString();
					}
					jecnRiskCommon.setRiskName(riskName);
					String riskNum = "";
					if (obj[2] != null && !"".equals(obj[2].toString())) {
						riskNum = obj[2].toString();
					}
					jecnRiskCommon.setRiskNumber(riskNum);
					jecnRiskCommon.setRuleTitleId(Long.valueOf(obj[3].toString()));
					listJecnRiskCommon.add(jecnRiskCommon);
				}
			}
		} else {
			str = "select jr.id,jr.name,jr.risk_code from jecn_risk  jr,JECN_RULE_RISK_T  jrr "
					+ "  where jr.id = jrr.RISK_ID and jrr.RULE_ID =? ";
			listObjects = this.listNativeSql(str, ruleId);
			for (Object[] obj : listObjects) {
				if (obj[0] != null) {
					JecnRiskCommon jecnRiskCommon = new JecnRiskCommon();
					jecnRiskCommon.setRiskId(Long.valueOf(obj[0].toString()));
					String riskName = "";
					if (obj[1] != null && !"".equals(obj[1].toString())) {
						riskName = obj[1].toString();
					}
					jecnRiskCommon.setRiskName(riskName);
					String riskNum = "";
					if (obj[2] != null && !"".equals(obj[2].toString())) {
						riskNum = obj[2].toString();
					}
					jecnRiskCommon.setRiskNumber(riskNum);
					listJecnRiskCommon.add(jecnRiskCommon);
				}
			}
		}
		return listJecnRiskCommon;
	}

	@Override
	public List<JecnRuleCommon> findRule(Long ruleId, int isOpration, boolean isPub) throws Exception {
		// 通过制度ID找到制度风险
		List<JecnRuleCommon> listJecnRuleCommon = new ArrayList<JecnRuleCommon>();
		String isPubStr = isPub ? " " : "_t";
		List<Object[]> listObjects = null;
		String str = "";
		if (isOpration == 0) {
			str = "select jr.id, jr.Rule_Name, jr.Rule_Number, rt.id as titleId" + "  from JECN_RULE" + isPubStr
					+ " jr, JECN_RULE_RELATED_RULE" + isPubStr + " jrr, RULE_TITLE" + isPubStr + " rt"
					+ " where jr.id = jrr.Related_Id" + "   and jrr.RULE_ID = rt.RULE_ID" + "   and rt.rule_id = ?"
					+ "   and rt.type = 6" + " order by jrr.RULE_ID";
			listObjects = this.listNativeSql(str, ruleId);
			for (Object[] obj : listObjects) {
				if (obj[0] != null && obj[3] != null) {
					JecnRuleCommon jecnRuleCommon = new JecnRuleCommon();
					jecnRuleCommon.setRuleId(Long.valueOf(obj[0].toString()));
					String ruleName = "";
					if (obj[1] != null && !"".equals(obj[1].toString())) {
						ruleName = obj[1].toString();
					}
					jecnRuleCommon.setRuleName(ruleName);
					String ruleNum = "";
					if (obj[2] != null && !"".equals(obj[2].toString())) {
						ruleNum = obj[2].toString();
					}
					jecnRuleCommon.setRuleNumber(ruleNum);
					jecnRuleCommon.setRuleTitleId(Long.valueOf(obj[3].toString()));
					listJecnRuleCommon.add(jecnRuleCommon);
				}
			}
		} else {
			str = "select jr.id, jr.rule_name, jr.rule_number" + "  from jecn_rule" + isPubStr
					+ " jr, JECN_RULE_RELATED_RULE" + isPubStr + " jrr" + " where jr.id = jrr.rule_id"
					+ "   and jrr.RULE_ID = ?";
			listObjects = this.listNativeSql(str, ruleId);
			for (Object[] obj : listObjects) {
				if (obj[0] != null) {
					JecnRuleCommon jecnRuleCommon = new JecnRuleCommon();
					jecnRuleCommon.setRuleId(Long.valueOf(obj[0].toString()));
					String ruleName = "";
					if (obj[1] != null && !"".equals(obj[1].toString())) {
						ruleName = obj[1].toString();
					}
					jecnRuleCommon.setRuleName(ruleName);
					String ruleNum = "";
					if (obj[2] != null && !"".equals(obj[2].toString())) {
						ruleNum = obj[2].toString();
					}
					jecnRuleCommon.setRuleNumber(ruleNum);
					listJecnRuleCommon.add(jecnRuleCommon);
				}
			}
		}
		return listJecnRuleCommon;
	}

	@Override
	public List<JecnStandarCommon> findRuleStandar(Long ruleId, int isOpration) throws Exception {
		// 通过制度ID找到制度标准
		List<JecnStandarCommon> listJecnStandarCommon = new ArrayList<JecnStandarCommon>();
		List<Object[]> listObjects = null;
		String str = "";

		if (isOpration == 0) {
			str = "select jcc.CRITERION_CLASS_ID, jcc.CRITERION_CLASS_NAME, jcc.STAN_TYPE,rt.id,jcc.RELATED_ID,jcc.t_path "
					+ "  from JECN_CRITERION_CLASSES jcc, JECN_RULE_STANDARD_T jrs, RULE_TITLE_T rt "
					+ " where jcc.CRITERION_CLASS_ID = jrs.STANDARD_ID "
					+ "   and jrs.RULE_ID = rt.RULE_ID and rt.rule_id=? and rt.type = 4 order by jrs.id";
			listObjects = this.listNativeSql(str.toString(), ruleId);
			for (Object[] obj : listObjects) {
				if (obj[0] != null && obj[3] != null) {
					JecnStandarCommon jecnStandarCommon = new JecnStandarCommon();
					jecnStandarCommon.setStandarId(Long.valueOf(obj[0].toString()));
					String standarName = "";
					if (obj[1] != null && !"".equals(obj[1])) {
						standarName = obj[1].toString();
					}
					jecnStandarCommon.setStandarName(standarName);
					String standarType = "";
					if (obj[2] != null && !"".equals(obj[2])) {
						standarType = obj[2].toString();
					}
					jecnStandarCommon.setStandaType(standarType);
					jecnStandarCommon.setRuleTitleId(Long.valueOf(obj[3].toString()));
					// 关联ID
					if (obj[4] != null && !"".equals(obj[4])) {
						jecnStandarCommon.setRelatedId(Long.valueOf(obj[4].toString()));
					}
					if (obj[5] != null && !"".equals(obj[5])) {
						jecnStandarCommon.settPath(JecnUtil.nullToEmpty(obj[5]));
					}
					listJecnStandarCommon.add(jecnStandarCommon);
				}
			}
		} else {
			str = "select jcc.CRITERION_CLASS_ID, jcc.CRITERION_CLASS_NAME, jcc.STAN_TYPE,jcc.RELATED_ID,jcc.t_path "
					+ "  from JECN_RULE_STANDARD_T jrs,JECN_CRITERION_CLASSES jcc "
					+ " left join jecn_file_t jft on jcc.related_id=jft.file_id"
					+ " where jcc.CRITERION_CLASS_ID = jrs.STANDARD_ID "
					+ " and (jcc.stan_type<>1 or (jcc.stan_type=1 and jft.del_state=0 and jft.file_id is not null))"
					+ "   and jrs.RULE_ID = ?";
			listObjects = this.listNativeSql(str.toString(), ruleId);
			for (Object[] obj : listObjects) {
				if (obj[0] != null && obj[2] != null) {
					JecnStandarCommon jecnStandarCommon = new JecnStandarCommon();
					jecnStandarCommon.setStandarId(Long.valueOf(obj[0].toString()));
					String standarName = "";
					if (obj[1] != null && !"".equals(obj[1])) {
						standarName = obj[1].toString();
					}
					jecnStandarCommon.setStandarName(standarName);
					String standarType = "";
					if (obj[2] != null && !"".equals(obj[2])) {
						standarType = obj[2].toString();
					}
					jecnStandarCommon.setStandaType(standarType);
					// 关联ID
					if (obj[3] != null && !"".equals(obj[3])) {
						jecnStandarCommon.setRelatedId(Long.valueOf(obj[3].toString()));
					}
					if (obj[4] != null && !"".equals(obj[4])) {
						jecnStandarCommon.settPath(JecnUtil.nullToEmpty(obj[4]));
					}
					listJecnStandarCommon.add(jecnStandarCommon);
				}
			}
		}

		// 补全全路径
		Set<Long> ids = new HashSet<Long>();
		for (JecnStandarCommon c : listJecnStandarCommon) {
			if (StringUtils.isNotBlank(c.gettPath())) {
				String[] split = c.gettPath().split("-");
				for (String s : split) {
					if (StringUtils.isNotBlank(s)) {
						ids.add(Long.valueOf(s));
					}
				}
			}
		}
		if (ids.size() > 0) {
			String hql = "from StandardBean where criterionClassId in " + JecnCommonSql.getIdsSet(ids);
			List<StandardBean> aa = this.listHql(hql);
			Map<Long, String> idName = new HashMap<Long, String>();
			for (StandardBean a : aa) {
				idName.put(a.getCriterionClassId(), a.getCriterionClassName());
			}
			StringBuffer buf = new StringBuffer();
			for (JecnStandarCommon c : listJecnStandarCommon) {
				if (StringUtils.isNotBlank(c.gettPath())) {
					String[] split = c.gettPath().split("-");
					for (int i = 0; i < split.length - 1; i++) {
						String s = split[i];
						if (StringUtils.isNotBlank(s)) {
							String name = idName.get(Long.valueOf(s));
							if (StringUtils.isNotBlank(name)) {
								buf.append(name);
								if (i != split.length - 2) {
									buf.append("/");
								}
							}
						}
					}
				}
				c.settPath(buf.toString());
				buf.setLength(0);
			}

		}

		return listJecnStandarCommon;
	}

	/**
	 * 获取制度责任部门
	 * 
	 * @author fuzhh 2014-1-21
	 * @param orgId
	 * @throws Exception
	 */
	public String getOrgName(Long orgId) throws Exception {
		String sql = "SELECT ORG_NAME FROM JECN_FLOW_ORG WHERE ORG_ID=" + orgId;
		List<Object> list = this.listNativeSql(sql);
		if (list.size() > 0) {
			return list.get(0).toString();
		}
		return null;
	}

	@Override
	public List<Object[]> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception {
		String sql = "select distinct t.id,              t.rule_name,               t.per_id,"
				+ "                t.is_dir,               t.rule_number,                case"
				+ "                  when jr.id is null then" + "                   '0'" + "                  else"
				+ "                   '1'" + "                end as is_pub," + "                case"
				+ "                  when jup.id is null then" + "                   '0'" + "                  else"
				+ "                   '1'" + "                end as count," + "                t.TEMPLATE_ID,"
				+ "                t.FILE_ID," + "                t.sort_id" + "  from jecn_rule_t t"
				+ "  left join jecn_rule jr on t.id = jr.id" + "  left join jecn_rule_t jup on jup.per_id = t.id ";
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(3, peopleId, projectId);
		sql = sql + " where t.per_id = ? and t.project_id = ?" + " order by t.per_id, t.sort_id, t.id";
		return this.listNativeSql(sql, pId, projectId);
	}

	/**
	 * 制度相关标准化文件
	 * 
	 * @param id
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getRuleStandardizedFileIds(Long id, String isPub, WebLoginBean bean) throws Exception {
		return getRuleStandardizedFileIds(id, isPub, bean, 0);
	}

	/**
	 * 制度相关制度文件
	 * 
	 * @param id
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getRuleFileIds(Long id, String isPub, WebLoginBean bean) throws Exception {
		return getRuleFileIdsByAccessType(id, isPub, bean, 0);
	}

	private List<Long> getRuleFileIdsByAccessType(Long id, String isPub, WebLoginBean bean, int accessType) {
		String sql = "SELECT DISTINCT R.FILE_ID FROM JECN_RULE_RELATED_RULE" + isPub + " T   INNER JOIN JECN_RULE"
				+ isPub + " R" + " ON T.RELATED_ID = R.ID" + " AND R.IS_DIR = 2   and r.is_file_local = 0 ";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorRuleIdsSql("ID", bean, isPub, accessType);
		sql += " WHERE  T.RULE_ID = ?";
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG, id);
	}

	@Override
	public JecnRuleBaiscInfoT getJecnRuleBaiscInfoTById(Long ruleId) throws Exception {
		return (JecnRuleBaiscInfoT) this.getSession().get(JecnRuleBaiscInfoT.class, ruleId);
	}

	@Override
	public JecnRuleBaiscInfo getJecnRuleBaiscInfoById(Long ruleId) throws Exception {
		return (JecnRuleBaiscInfo) this.getSession().get(JecnRuleBaiscInfo.class, ruleId);
	}

	@Override
	public List<Long> getStandFileByFlowId(Long ruleId, WebLoginBean loginBean, String isPub) throws Exception {
		return getStandFileByFlowId(ruleId, loginBean, isPub, 0);
	}

	/**
	 * 更新流程关联文件节点
	 * 
	 * @param id
	 *            制度ID
	 * @param relatedFileId
	 *            相关标准化文件ID
	 * @throws Exception
	 */
	@Override
	public void updateParentRelatedFileId(Long id, Long relatedFileId) throws Exception {
		String sql = "UPDATE JECN_RULE_T SET RELATED_FILE_ID = ? WHERE ID =  ?";
		this.execteNative(sql, relatedFileId, id);
		sql = "UPDATE JECN_RULE SET RELATED_FILE_ID = ? WHERE ID =  ?";
		this.execteNative(sql, relatedFileId, id);
	}

	/**
	 * 获取父节点对应的relatedFileId
	 * 
	 * @param path
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getParentRelatedFileByPath(Long id) throws Exception {
		String sql = "SELECT PARENT.ID, FT.FILE_ID, PARENT.RULE_NAME, PARENT.SORT_ID  FROM JECN_RULE_T T"
				+ "  LEFT JOIN JECN_RULE_T PARENT ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT")
				+ " LEFT JOIN JECN_FILE_T FT ON FT.RULE_ID = PARENT.ID" + "   AND FT.IS_DIR = 0 WHERE T.ID = ?"
				+ " ORDER BY PARENT.T_PATH";
		return this.listNativeSql(sql, id);
	}

	@Override
	public Long getRelatedFileId(Long ruleId) throws Exception {
		String sql = "SELECT FT.FILE_ID" + "  FROM JECN_RULE_T T" + "  LEFT JOIN JECN_FILE_T FT"
				+ "    ON FT.RULE_ID = T.ID" + " WHERE FT.RULE_ID IS NOT NULL" + "   AND FT.IS_DIR = 0"
				+ "   AND T.ID = ?";
		Object obj = getObjectNativeSql(sql, ruleId);
		return obj == null ? null : Long.valueOf(obj.toString());
	}

	@Override
	public List<Long> getRelatedFileIdsByRuleIds(List<Long> ids) {
		String sql = "  SELECT T.RELATED_FILE_ID FROM JECN_RULE_T T WHERE T.ID IN " + JecnCommonSql.getIds(ids)
				+ " AND T.RELATED_FILE_ID IS NOT NULL";
		return this.listObjectNativeSql(sql, "RELATED_FILE_ID", Hibernate.LONG);
	}

	@Override
	public List<Long> getRuleStandardizedFileIds(Long id, String isPub, WebLoginBean bean, int accessType) {
		String sql = "SELECT DISTINCT T.FILE_ID FROM RULE_STANDARDIZED_FILE" + isPub + " T";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorFileIdsSql("FILE_ID", bean, isPub, accessType);
		sql += " WHERE RELATED_ID = ?";
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG, id);
	}

	@Override
	public List<Long> getStandFileByFlowId(Long ruleId, WebLoginBean loginBean, String isPub, int accessType) {
		String sql = "SELECT T.FILE_ID FROM (SELECT STAND.RELATED_ID FILE_ID,STAND.CRITERION_CLASS_ID"
				+ "  FROM JECN_RULE_STANDARD" + isPub + " RS, JECN_CRITERION_CLASSES STAND"
				+ " WHERE STAND.CRITERION_CLASS_ID = RS.STANDARD_ID" + "   AND STAND.STAN_TYPE = '1'"
				+ "   AND RS.RULE_ID = " + ruleId + "   AND STAND.PROJECT_ID = " + loginBean.getProjectId() + ") T";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorIdsSqlByType("CRITERION_CLASS_ID", loginBean, isPub,
				TYPEAUTH.STANDARD, accessType);
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
	}

	@Override
	public G020RuleFileContent getG020RuleFileContent(Long fileId) {
		String hql = "from G020RuleFileContent where id=?";
		G020RuleFileContent fileContent = this.getObjectHql(hql, fileId);
		return fileContent;
	}

	@Override
	public List<Long> getRuleFileIds(Long id, String isPub, WebLoginBean bean, int fileAccessType) {
		return getRuleFileIdsByAccessType(id, isPub, bean, fileAccessType);
	}

	@Override
	public List<Long> getRuleModelFileIds(Long id, String isPub, WebLoginBean bean, int accessType) {
		String sql = "select distinct t.file_id" + "  from jecn_file t" + " inner join rule_file rf"
				+ "    on t.file_id = rf.rule_file_id" + "   and t.del_state = 0" + " inner join rule_title rt"
				+ "    on rf.rule_title_id = rt.id" + "   and rt.rule_id =?";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorFileIdsSql("FILE_ID", bean, isPub, accessType);
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG, id);
	}

}
