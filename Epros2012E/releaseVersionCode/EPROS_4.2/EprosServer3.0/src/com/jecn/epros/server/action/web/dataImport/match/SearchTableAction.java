package com.jecn.epros.server.action.web.dataImport.match;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.jecn.epros.server.service.dataImport.match.buss.SearchTableMatchBuss;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 岗位匹配页面
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Apr 7, 2013 时间：5:27:54 PM
 */
public class SearchTableAction extends ActionSupport {
	Logger log = Logger.getLogger(SearchTableAction.class);
	private SearchTableMatchBuss searchTableMatchBuss;

	@Override
	public String execute() throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		String str = "";
		String projectId = (String) request.getSession().getAttribute(
				"curProjectId");

		String searchName = request.getParameter("searchName");
		String stateTree = request.getParameter("state");
		int state = Integer.parseInt(stateTree);
		response.setContentType("text/html;charset=UTF-8");

		PrintWriter out =null;
		try {
			if (!"".equals(projectId) && !"".equals(searchName)) {
				if (state == 11) {
					str = searchTableMatchBuss.getStationSearchResult(
							searchName, Long.valueOf(projectId), state);
				} else if (state == 12) {
					str = searchTableMatchBuss.getStationSearchResult(
							searchName, Long.valueOf(projectId), state);
				}
			}
			if (!"".equals(str)) {
				out= response.getWriter();
				out.println(str);
				if (log.isInfoEnabled()) {
					log.info(str);
				}
				out.flush();
			}
		} catch (Exception ex) {
			log.error("", ex);
		} finally{
			try {
				if (out != null) {
					out.close();
				}
			} catch (Exception e) {
				log.error("关闭流异常", e);
			}
		}
		return null;
	}

	public SearchTableMatchBuss getSearchTableSfBuss() {
		return searchTableMatchBuss;
	}

	public void setSearchTableSfBuss(SearchTableMatchBuss searchTableMatchBuss) {
		this.searchTableMatchBuss = searchTableMatchBuss;
	}

	public SearchTableMatchBuss getSearchTableMatchBuss() {
		return searchTableMatchBuss;
	}

	public void setSearchTableMatchBuss(
			SearchTableMatchBuss searchTableMatchBuss) {
		this.searchTableMatchBuss = searchTableMatchBuss;
	}

}
