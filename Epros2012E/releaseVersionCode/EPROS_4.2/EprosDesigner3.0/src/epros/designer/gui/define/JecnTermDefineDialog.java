package epros.designer.gui.define;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnManageDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class JecnTermDefineDialog extends JecnManageDialog {
	private static Logger log = Logger.getLogger(JecnTermDefineDialog.class);

	@Override
	public void buttonOneAction() {

	}

	@Override
	public String buttonOneName() {
		return null;
	}

	@Override
	public void buttonThreeAction() {
		int[] selectedRow = this.getJecnTable().getSelectedRows();
		if (selectedRow.length != 1) {
			this.getjLabelTip().setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		JecnTreeBean jecnTreeBean = this.getSearchList().get(selectedRow[0]);
		try {
			JecnTreeNode node = JecnTreeCommon.searchNode(jecnTreeBean.getId(), TreeNodeType.termDefine, this
					.getjTree());
			if (node != null) {
				EditTermDefineDialog editTermDefineDialog = new EditTermDefineDialog(node, this.getjTree());
				editTermDefineDialog.setVisible(true);
			}
		} catch (Exception e) {
			log.error(JecnProperties.getValue("termDefineMoveError"), e);
		}

	}

	@Override
	public String buttonThreeName() {
		return JecnProperties.getValue("editTermDefine");
	}

	// 定位
	@Override
	public void buttonTwoAction() {
		int[] selectedRow = this.getJecnTable().getSelectedRows();
		if (selectedRow.length != 1) {
			this.getjLabelTip().setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		JecnTreeBean jecnTreeBean = this.getSearchList().get(selectedRow[0]);
		try {
			JecnTreeCommon.searchNode(jecnTreeBean.getId(), TreeNodeType.termDefine, this.getjTree());
		} catch (Exception e) {
			log.error(JecnProperties.getValue("termDefineMoveError"), e);
		}
	}

	@Override
	public String buttonTwoName() {
		return JecnProperties.getValue("fixedPosition");
	}

	@Override
	public boolean deleteData(List<Long> listIds) {
		// 删除节点
		try {
			ConnectionPool.getTermDefinitionAction().delete(listIds, JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("JecnTermDefineDialog deleteData is error", e);
			return false;
		}
		return true;
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("termDefine");
	}

	@Override
	public String getSerachLabelName() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> vec = new Vector<String>();
		vec.add(JecnProperties.getValue("term"));
		vec.add("ID");
		return vec;
	}

	@Override
	public String getTreePanelTitle() {
		return "";
	}

	@Override
	public JecnTree initJecnTree() {
		return new JecnTermDefineTree();
	}

	@Override
	public boolean isAddButtonOne() {
		return false;
	}

	@Override
	public boolean isAddButtonThree() {
		return false;
	}

	@Override
	public boolean isAddButtonTwo() {
		return true;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getTermDefinitionAction().searchByName(name);
		} catch (Exception e) {
			log.error(JecnProperties.getValue("termDefineSearchDataError"), e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public Vector<Vector<String>> updateTableContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		for (JecnTreeBean jecnTreeBean : list) {
			Vector<String> data = new Vector<String>();
			data.add(jecnTreeBean.getName());
			data.add(jecnTreeBean.getId().toString());
			vector.add(data);
		}
		return vector;
	}

}
