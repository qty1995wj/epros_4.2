package com.jecn.epros.server.webBean.process;

import java.util.List;

/**
 * 角色流程活动关联ID
 * 
 * @author fuzhh Dec 3, 2012
 * 
 */
public class JecnRoleProcessBean {
	/** 岗位Id */
	private Long posId;
	/** 岗位名称 */
	private String posName;
	/** 角色 */
	private List<ProcessRoleWebBean> roleList;

	public Long getPosId() {
		return posId;
	}

	public void setPosId(Long posId) {
		this.posId = posId;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public List<ProcessRoleWebBean> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<ProcessRoleWebBean> roleList) {
		this.roleList = roleList;
	}
}
