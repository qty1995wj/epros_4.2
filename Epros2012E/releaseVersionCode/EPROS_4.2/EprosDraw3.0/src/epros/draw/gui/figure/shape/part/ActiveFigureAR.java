package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.util.DrawCommon;

/**
 * 
 * 活动图形类
 * 
 * @date： 日期：Mar arcValue, arcValue12 时间：5:00:06 PM
 * @author zhangxh
 */
public class ActiveFigureAR extends JecnBaseActiveFigure {
	/** X坐标 */
	private final int x = 0;
	/** Y坐标 */
	private final int y = 0;

	/** 圆角矩形弧度 */
	private int arcValue = 6;

	/**
	 * 初始化类对象
	 * 
	 * @param activeData
	 *            JecnFigureData JecnActiveData对象
	 * 
	 */
	public ActiveFigureAR(JecnFigureData jecnActiveData) {
		super(jecnActiveData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		paintFigure(g);

		// 信息化
		initInfoFigure();

		initInfoActiveNumFigure();
	}

	/**
	 * 设置字的位置
	 * 
	 * @param g
	 * @param type
	 */
	protected void setFontPosition(Graphics2D g, int type) {
		String text = flowElementData.getFigureText();
		if (DrawCommon.isNullOrEmtryTrim(text) || g == null) {
			return;
		}
		g.setFont(this.getCurScaleFont());
		// 字体显示位置点
		drawNoUprightSort(g, text, type, this.getCurScaleFont(), this.getWidth());
	}

	/**
	 * 无填充色
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRoundRect(x, y, userWidth, userHeight, arcValue, arcValue);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 零度或180
		// 阴影
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillRoundRect(x + shadowCount, y + shadowCount, userWidth - shadowCount, userHeight - shadowCount,
				arcValue, arcValue);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintActiveTop(Graphics2D g2d, int shadowCount) {
		// 顶层
		g2d.fillRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, arcValue, arcValue);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, arcValue, arcValue);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintActiveTop(g2d, 0);
	}

	/**
	 * 阴影
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintActiveTop(g2d, shadowCount);
	}

	/**
	 * 3D效果最下层
	 */

	public void paint3DLow(Graphics2D g2d) {
		paint3DLow(g2d, 0);
	}

	/**
	 * 3D效果和阴影最下层
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DLow(g2d, linevalue);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, arcValue, arcValue);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, arcValue, arcValue);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, arcValue, arcValue);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillRoundRect(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue, arcValue, arcValue);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DPartLine(g2d, 0);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DPartLine(g2d, indent3Dvalue);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d, int shadowCount) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, arcValue, arcValue);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, arcValue, arcValue);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, arcValue, arcValue);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawRoundRect(x, y, userWidth - shadowCount, userHeight - shadowCount, arcValue, arcValue);
		}
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		paint3DAndShadows(g2d, 0);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		paint3DAndShadows(g2d, shadowCount);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 * @param shadowCount
	 */
	public void paint3DAndShadows(Graphics2D g2d, int shadowCount) {
		g2d.fillRoundRect(x + 4, y + 4, userWidth - 4 - 4 - shadowCount, userHeight - 4 - 4 - shadowCount, arcValue,
				arcValue);
		g2d.setPaint(flowElementData.getBodyColor());
	}
}
