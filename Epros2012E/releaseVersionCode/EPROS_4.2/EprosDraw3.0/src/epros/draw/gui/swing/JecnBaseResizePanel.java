package epros.draw.gui.swing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 
 * 流程元素选中点，其具有改变该流程元素大小位置点动作事件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBaseResizePanel extends JPanel {

	protected Color bodyColor = Color.GREEN;

	public JecnBaseResizePanel() {
		// 鼠标事件
		this.addMouseListener(JecnDrawMainPanel.getMainPanel());
		this.addMouseMotionListener(JecnDrawMainPanel.getMainPanel());
	}

	@Override
	public void paintComponent(Graphics g) {
		// 画笔
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(bodyColor);
		g2d.setStroke(new BasicStroke(1));
		g2d.fillRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
		g2d.setColor(Color.BLACK);
		g2d.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
	}

	public void setBodyColor(Color bodyColor) {
		this.bodyColor = bodyColor;
	}
}
