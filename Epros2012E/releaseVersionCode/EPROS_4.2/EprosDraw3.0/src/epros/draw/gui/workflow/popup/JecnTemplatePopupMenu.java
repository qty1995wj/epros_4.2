package epros.draw.gui.workflow.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.box.JecnNewUpdateTemplateDialog;
import epros.draw.gui.box.JecnScrollPaneComponentAdapter;
import epros.draw.gui.box.JecnTemplateBoxButton;
import epros.draw.gui.box.JecnTemplateScrollPane;
import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 模型右键菜单处理类
 * 
 * @author FUZHENHUA
 * @date： 日期：Jul 30, 2012 时间：11:28:56 AM
 */
public class JecnTemplatePopupMenu extends JPopupMenu implements ActionListener {
	/** 删除模型 */
	private JMenuItem deleteTemplateMenu;
	/** 修改模型 */
	private JMenuItem updateTemplateMenu;

	public JecnTemplatePopupMenu() {
		// 删除模型
		deleteTemplateMenu = new JMenuItem();
		deleteTemplateMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("deleteTemplate"));
		// 添加事件监听
		deleteTemplateMenu.addActionListener(this);
		deleteTemplateMenu.setName("deleteTemplate");
		// 修改模型
		updateTemplateMenu = new JMenuItem();
		updateTemplateMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("updateTemplate"));
		// 添加事件监听
		updateTemplateMenu.addActionListener(this);
		updateTemplateMenu.setName("updateTemplate");

		this.add(deleteTemplateMenu);
		this.add(updateTemplateMenu);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			// 获取JMenuItem
			JMenuItem menuItem = (JMenuItem) e.getSource();
			String menuName = menuItem.getName();

			// 获取片段ID
			String templateId = JecnSystemData.getTemplateData().getTemplateId();
			if ("deleteTemplate".equals(menuName)) {
				if (templateId != null) {
					// 删除xml是否成功标识
					boolean deleteXmlFlag = true;
					// 调用删除方法
					deleteXmlFlag = JecnFlowTemplate.deleteXmlNode(getWorkflow().getFlowMapData().getMapType(),
							templateId);
					if (deleteXmlFlag) {
						JecnScrollPaneComponentAdapter scrollPaneComponentAdapter = new JecnScrollPaneComponentAdapter();
						// 获取数据对应的按钮
						JecnToolBar toolBar = JecnFlowTemplate.getToolBarToTemplateData(
								JecnSystemData.getTemplateMap(), templateId);
						MapType mapType = getWorkflow().getFlowMapData().getMapType();
						if (mapType == MapType.partMap) {
							// 在面板中删除对应的按钮
							getPartMapModelScrollPane().getTemplatePanel().remove(toolBar);
							// 刷新面板
							getPartMapModelScrollPane().getTemplatePanel().updateUI();
							JecnSystemData.setPartMapCount(JecnSystemData.getPartMapCount() - 1);
							scrollPaneComponentAdapter.setTemplateScrollPaneSize(getPartMapModelScrollPane());
						} else if (mapType == MapType.totalMap || mapType == MapType.totalMapRelation) {
							// 在面板中删除对应的按钮
							getTotalMapModelScrollPane().getTemplatePanel().remove(toolBar);
							// 刷新面板
							getTotalMapModelScrollPane().getTemplatePanel().updateUI();
							JecnSystemData.setTotalMapCount(JecnSystemData.getTotalMapCount() - 1);
							scrollPaneComponentAdapter.setTemplateScrollPaneSize(getTotalMapModelScrollPane());
						}
						// 删除互斥组
						if (toolBar.getComponents().length > 0) {
							JecnDrawMainPanel.getMainPanel().getBoxPanel().getBtnGroup().remove(
									(JecnTemplateBoxButton) toolBar.getComponents()[0]);
						}
					}

				}
			} else if ("updateTemplate".equals(menuName)) {
				// 调用新建和删除界面
				JecnNewUpdateTemplateDialog newTemplate = new JecnNewUpdateTemplateDialog(JecnSystemData
						.getTemplateData());
				newTemplate.setVisible(true);
			}
		}
	}

	/**
	 * 获取流程图滚动面板
	 * 
	 * @return
	 */
	private static JecnTemplateScrollPane getPartMapModelScrollPane() {
		return JecnDrawMainPanel.getMainPanel().getBoxPanel().getPartMapModelScrollPane();
	}

	/**
	 * 获取流程地图滚动面板
	 * 
	 * @return
	 */
	private static JecnTemplateScrollPane getTotalMapModelScrollPane() {
		return JecnDrawMainPanel.getMainPanel().getBoxPanel().getTotalMapModelScrollPane();
	}

	/**
	 * 获取面板
	 */
	private static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}
}
