package com.jecn.webservice.zhongsh.flowpush.bean;

import java.util.ArrayList;
import java.util.List;

public class WBWorkflow {

	private String xmlns = "http://schemas.microsoft.com/winfx/2006/xaml/presentation";
	private String xmlnsx = "http://schemas.microsoft.com/winfx/2006/xaml";
	private String xmlnsWBD = "clr-namespace:WorkBench.BPMDesigner;assembly=WorkBench.BPMDesigner";
	private String workBoxVersion = "4.0.0.3784";
	private String workflowType = "Site";
	private String contentType = "";
	private String workflowName = "";
	private String workflowCode = "";
	private String workflowDescription = "";
	private String startManually = "true";
	private String requireManagePermission = "false";
	private String startOnChange = "false";
	private String startOnCreate = "false";
	private String currentColorSchemaID = "WBInternalColorSchema";
	private String platformType = "SPS";
	private String showActionIndicators = "True";
	private String isApplication = "False";

	/** 角色的集合 **/
	private List<WBRole> wBRoleCollection = new ArrayList<WBRole>();
	/** 活动的集合 **/
	private List<WBState> wBStateCollection = new ArrayList<WBState>();
	/** 线的集合 **/
	private List<WBAction> wBActionCollection = new ArrayList<WBAction>();

	/** 元素对应连线对应关系集合(从State上出去的线大于1个会出现在这个集合中 **/
	private List<WBDecision> wBDecisionCollection = new ArrayList<WBDecision>();

	/** 指定action哪些角色可以执行的集合 **/
	private List<WBRoleActionAssignment> wBRoleActionAssignmentCollection = new ArrayList<WBRoleActionAssignment>();

	/** 角色和活动的关联关系的集合 **/
	private List<WBRoleStateAssignment> wBRoleStateAssignmentCollection = new ArrayList<WBRoleStateAssignment>();

	/** 并行：分支和会聚 对应关系集合 */
	private List<ParallelState> ParallelStateCollection = new ArrayList<ParallelState>();

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	public String getXmlnsx() {
		return xmlnsx;
	}

	public void setXmlnsx(String xmlnsx) {
		this.xmlnsx = xmlnsx;
	}

	public String getXmlnsWBD() {
		return xmlnsWBD;
	}

	public void setXmlnsWBD(String xmlnsWBD) {
		this.xmlnsWBD = xmlnsWBD;
	}

	public String getWorkBoxVersion() {
		return workBoxVersion;
	}

	public void setWorkBoxVersion(String workBoxVersion) {
		this.workBoxVersion = workBoxVersion;
	}

	public String getWorkflowType() {
		return workflowType;
	}

	public void setWorkflowType(String workflowType) {
		this.workflowType = workflowType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getWorkflowDescription() {
		return workflowDescription;
	}

	public void setWorkflowDescription(String workflowDescription) {
		this.workflowDescription = workflowDescription;
	}

	public String getStartManually() {
		return startManually;
	}

	public void setStartManually(String startManually) {
		this.startManually = startManually;
	}

	public String getRequireManagePermission() {
		return requireManagePermission;
	}

	public void setRequireManagePermission(String requireManagePermission) {
		this.requireManagePermission = requireManagePermission;
	}

	public String getStartOnChange() {
		return startOnChange;
	}

	public void setStartOnChange(String startOnChange) {
		this.startOnChange = startOnChange;
	}

	public String getStartOnCreate() {
		return startOnCreate;
	}

	public void setStartOnCreate(String startOnCreate) {
		this.startOnCreate = startOnCreate;
	}

	public String getCurrentColorSchemaID() {
		return currentColorSchemaID;
	}

	public void setCurrentColorSchemaID(String currentColorSchemaID) {
		this.currentColorSchemaID = currentColorSchemaID;
	}

	public String getPlatformType() {
		return platformType;
	}

	public void setPlatformType(String platformType) {
		this.platformType = platformType;
	}

	public String getShowActionIndicators() {
		return showActionIndicators;
	}

	public void setShowActionIndicators(String showActionIndicators) {
		this.showActionIndicators = showActionIndicators;
	}

	public String getIsApplication() {
		return isApplication;
	}

	public void setIsApplication(String isApplication) {
		this.isApplication = isApplication;
	}

	public List<WBRole> getwBRoleCollection() {
		return wBRoleCollection;
	}

	public void setwBRoleCollection(List<WBRole> wBRoleCollection) {
		this.wBRoleCollection = wBRoleCollection;
	}

	public List<WBState> getwBStateCollection() {
		return wBStateCollection;
	}

	public void setwBStateCollection(List<WBState> wBStateCollection) {
		this.wBStateCollection = wBStateCollection;
	}

	public List<WBAction> getwBActionCollection() {
		return wBActionCollection;
	}

	public void setwBActionCollection(List<WBAction> wBActionCollection) {
		this.wBActionCollection = wBActionCollection;
	}

	public List<WBDecision> getwBDecisionCollection() {
		return wBDecisionCollection;
	}

	public void setwBDecisionCollection(List<WBDecision> wBDecisionCollection) {
		this.wBDecisionCollection = wBDecisionCollection;
	}

	public List<WBRoleActionAssignment> getwBRoleActionAssignmentCollection() {
		return wBRoleActionAssignmentCollection;
	}

	public void setwBRoleActionAssignmentCollection(List<WBRoleActionAssignment> wBRoleActionAssignmentCollection) {
		this.wBRoleActionAssignmentCollection = wBRoleActionAssignmentCollection;
	}

	public List<WBRoleStateAssignment> getwBRoleStateAssignmentCollection() {
		return wBRoleStateAssignmentCollection;
	}

	public void setwBRoleStateAssignmentCollection(List<WBRoleStateAssignment> wBRoleStateAssignmentCollection) {
		this.wBRoleStateAssignmentCollection = wBRoleStateAssignmentCollection;
	}

	public List<ParallelState> getParallelStateCollection() {
		return ParallelStateCollection;
	}

	public void setParallelStateCollection(List<ParallelState> parallelStateCollection) {
		ParallelStateCollection = parallelStateCollection;
	}

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

}
