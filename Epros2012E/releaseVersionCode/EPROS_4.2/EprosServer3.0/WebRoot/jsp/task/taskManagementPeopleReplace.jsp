<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	</head>
	<body>
	<div align="center" style="width: 100%; vertical-align: middle;margin-top: 20px">
	<!-- 来源人 -->
		<s:text name="fromApprovePeople"></s:text>

		<input type="text" style="width: 10%; vertical-align: middle;"
			id="fromPeopleName" readonly="readonly"/>
		<!-- 选择 -->
		<span style="cursor: pointer;"
			onclick='selectOrgPosWindow("person","fromPeopleName","fromPeopleId")'><img
				style="vertical-align: middle;"
				src="${basePath}images/common/select.gif" /> </span>
        
     <!-- 目标人 -->
		<s:text name="toApprovePeople"></s:text>
   
		<input type="text" style="width: 10%; vertical-align: middle;"
			id="toPeopleName" readonly="readonly"/>
		<!-- 选择 -->
		<span style="cursor: pointer;"
			onclick='selectOrgPosWindow("person","toPeopleName","toPeopleId")'><img
				style="vertical-align: middle;"
				src="${basePath}images/common/select.gif" /> </span>

		</div>
		<%--确定和重置--%>
		<div align="center" style="vertical-align: middle;margin-top: 40px">
		<img style="cursor: pointer;"
			src="${basePath}images/common/ok.gif"
			onclick="taskManagementReplacePeopleSubmit()" />
		<img style="cursor: pointer;"
			src="${basePath}images/iParticipateInProcess/reset.gif"
			onclick="taskManagementReplacePeopleReset()" />
		</div>
		<input type="text" id="fromPeopleId" style="display: none"/>
		<input type="text" id="toPeopleId" style="display: none"/>
		<div id="errorTask">
		    
		
		</div>
	</body>
</html>
