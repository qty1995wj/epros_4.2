package com.jecn.webservice.guangzhouCsr;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/***
 * 
 * 广州电力动车任务获取webService接口
 * 
 * @author chehuanbo
 * @date 2014-11-18
 * 
 */
@WebService(endpointInterface = "GuangZhouCrsTaskApproval")
public interface ITaskApprovalWebService {

	/**
	 * 
	 * 获取当前用户所有的审批任务
	 * 
	 * @author chhuanbo
	 * @date 2014-11-18
	 * 
	 * */
	@WebMethod(operationName = "getALLTaskApproval")
	public List<CrsTaskBean> getALLTaskApproval(
			@WebParam(name = "loginName") String loginName,
			@WebParam(name = "loginPassWord") String loginPassWord);

}
