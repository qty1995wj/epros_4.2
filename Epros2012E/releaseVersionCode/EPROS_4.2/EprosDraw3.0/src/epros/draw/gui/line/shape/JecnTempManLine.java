package epros.draw.gui.line.shape;

import java.awt.Point;

import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 画连接线虚拟线段
 * 
 * @author ZHANGXH
 * @date： 日期：Jun 15, 2012 时间：10:12:14 AM
 */
public class JecnTempManLine extends CommonLine {

	public JecnTempManLine(JecnManhattanLineData flowElementData) {
		super(flowElementData);
		this.flowElementData.setFigureText(null);
	}

	/** 临时线段开始端点 */
	private JecnEditPortPanel startPort = null;
	/** 临时线段结束端点 */
	private JecnEditPortPanel endPort = null;
	/** 临时线段换算后的结束点 */
	private Point endTempPoint = null;

	public Point getEndTempPoint() {
		return endTempPoint;
	}

	public void setEndTempPoint(Point endTempPoint) {
		this.endTempPoint = endTempPoint;
	}

	public JecnEditPortPanel getStartPort() {
		return startPort;
	}

	public void setStartPort(JecnEditPortPanel startPort) {
		this.startPort = startPort;
	}

	public JecnEditPortPanel getEndPort() {
		return endPort;
	}

	public void setEndPort(JecnEditPortPanel endPort) {
		this.endPort = endPort;
		this.endTempPoint = endPort.getOriginalCenterPoint();
	}

	public Point getOriginalPoint() {
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		return new Point(DrawCommon.convertDoubleToInt(endTempPoint.getX() / scale),
				DrawCommon.convertDoubleToInt(endTempPoint.getY() / scale));
	}
}
