package wordxml.element.node.paragraph;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.constant.Constants.LineRuleType;
import wordxml.element.bean.paragraph.GroupBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphRowIndBean;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.paragraph.ProjectSignBean;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.paragraph.Run;
import wordxml.element.dom.table.Table;
import wordxml.element.node.common.CommonUtil;
import wordxml.element.node.table.TableNode;
import wordxml.util.Tools;
import wordxml.util.Unit;

/*********
 * 段落节点操作
 * 
 * @author admin
 * 
 */
public class ParagraphNode {
	private static final Log log = LogFactory.getLog(ParagraphNode.class);

	/**
	 * 创建一个段落节点
	 * 
	 * @param paragraph
	 * @param parentEle
	 */
	public static void createParagraph(Paragraph paragraph, Element parentEle) {
		if (paragraph == null || parentEle == null
				|| paragraph.getRuns() == null) {
			log
					.info("ParagraphNode class createParagraph Argument is not valid");
			return;
		}
		Element pEle = parentEle.addElement("w:p");
		// 添加书签
		if (paragraph.getBookMark() != null
				&& !"".equals(paragraph.getBookMark())) {
			pEle.addElement("aml:annotation").addAttribute("aml:id", "0")
					.addAttribute("w:type", "Word.Bookmark.Start")
					.addAttribute("w:name", paragraph.getBookMark());
			pEle.addElement("aml:annotation").addAttribute("aml:id", "0")
					.addAttribute("w:type", "Word.Bookmark.End");
		}
		// 分页符标识
		if (paragraph.isWarp()) {
			pEle.addElement("w:r").addElement("w:br").addAttribute("w:type",
					"page");
			return;
		}
		if (paragraph.getParagraphBean() != null) {
			Element pPr = pEle.addElement("w:pPr");
			setGroupAttr(pPr,paragraph);
			setParagraphAttr(pPr, paragraph.getParagraphBean());
		}
		// 外部嵌入文件
		if (!Tools.isBlank(paragraph.getExtFilePath())) {
			Element hlink = pEle.addElement("w:hlink").addAttribute("w:dest",
					paragraph.getExtFilePath());
			pEle = hlink;
		}
		for (Run run : paragraph.getRuns()) {
			RunNode.createRun(run, pEle);
		}
		//处理江波龙时添加表格嵌套表格
		if (paragraph.getTables() != null) {
			for (Table tbl : paragraph.getTables()) {
				TableNode.createTable(tbl, pEle);
			}
		}
	}

	private static void setGroupAttr(Element pPr, Paragraph paragraph) {
		if (paragraph.getParent() != null && paragraph.getParent() instanceof Group) {
			Group group = (Group) paragraph.getParent();
			GroupBean groupBean = group.getGroupBean();
			Element framePr = pPr.addElement("w:framePr");
			if(groupBean.getAlign()==null){
				framePr.addAttribute("w:x", Unit.convertCmToTwip(groupBean.getX()));
			}else{
				framePr.addAttribute("w:x-align", groupBean.getAlign().toString());
			}
			if(groupBean.getVlign()==null){
				framePr.addAttribute("w:y", Unit.convertCmToTwip(groupBean.getY()));
			}else{
				framePr.addAttribute("w:y-align", groupBean.getVlign().toString());
			}
			framePr.addAttribute("w:wrap", "around")
			.addAttribute("w:vanchor", groupBean.getVanchor().getValue())
			.addAttribute("w:hanchor", groupBean.getHanchor().getValue());
			
			if(groupBean.isLock()){
				framePr.addAttribute("w:anchor-lock", "on");
			}
			
			// 宽度固定值
			if (groupBean.getwType() == GroupW.exact) {
				framePr.addAttribute("w:w", Unit.convertCmToTwip(groupBean.getwValue()) + "");
				framePr.addAttribute("w:h-rule","exact");
			}

			// 高度固定或者最小
			if (groupBean.gethType() == GroupH.exact || groupBean.gethType() == GroupH.min) {
				framePr.addAttribute("w:h", Unit.convertCmToTwip(groupBean.gethValue()) + "");
			}
			
//			w:hspace="5670" w:vspace="5670"
			// 距离上边文本距离
			framePr.addAttribute("w:hspace", Unit.convertCmToTwip(groupBean.getHspace()));
			framePr.addAttribute("w:vspace", Unit.convertCmToTwip(groupBean.getVspace()));
		}
	}

	/**
	 * 设置段落的属性
	 * 
	 * @param pPr段落属性节点
	 * @param paragraphBean段落属性bean
	 */
	private static void setParagraphAttr(Element pPr,
			ParagraphBean paragraphBean) {
		/***** 段落风格 *********/
		if (paragraphBean.getpStyle() != null) {
			pPr.addElement("w:pStyle").addAttribute("w:val",
					paragraphBean.getpStyle().getValue());
		}
		// 段落项目符号bean
		/*
		 * <w:pPr> <w:listPr> <w:ilvl w:val="0"/> <w:ilfo w:val="3"/> <wx:font
		 * wx:val="Wingdings"/> </w:listPr> </w:pPr>
		 */
		ProjectSignBean projectSignBean = paragraphBean.getProjectSignBean();
		if (projectSignBean != null) {
			Element listPr = pPr.addElement("w:listPr");
			listPr.addElement("w:ilvl").addAttribute("w:val",
					projectSignBean.getSignLvl() + "");
			listPr.addElement("w:ilfo").addAttribute("w:val",
					projectSignBean.getIlfo() + "");
			listPr.addElement("wx:font").addAttribute("w:val",
					projectSignBean.getFont());
		}
		// 对齐方式
		if (!Tools.isBlank(paragraphBean.getAlign().toString())) {
			pPr.addElement("w:jc").addAttribute("w:val",
					paragraphBean.getAlign().toString());
		}
		// 阴影
		// super.setShd(shd);
		// 禁止在本段分页
		if (paragraphBean.isKeepLines()) {
			pPr.addElement("w:keepLines");
		}
		// 强制在本段分页
		if (paragraphBean.isPageBreakBefore()) {
			pPr.addElement("w:pageBreakBefore");
		}
		// 手动断字
		if (paragraphBean.isSuppressAutoHyphens()) {
			pPr.addElement("w:suppressAutoHyphens")
					.addAttribute("w:val", "off");
		}

		// 段落边框
		if (paragraphBean.getBorderBottomBean() != null) {
			Element pBdr = pPr.addElement("w:pBdr");
			Element bottom = pBdr.addElement("w:bottom");
			CommonUtil.buildBorder(paragraphBean.getBorderBottomBean(), bottom);
		}

		/**** 段落缩进 <w:ind w:left="357" w:hanging="357"/> ******/
		ParagraphRowIndBean indBean = paragraphBean.getParagraphIndBean();
		if (indBean != null) {
			Element ind = pPr.addElement("w:ind");
			// 厘米转缇 左缩进的值为 左缩进加上悬挂缩进的值。
			ind.addAttribute("w:left", Unit.convertCmToTwip(indBean.getLeft()
					+ indBean.getHanging()));
			if (indBean.getRight() != 0) {
				ind.addAttribute("w:right", Unit.convertCmToTwip(indBean
						.getRight()));
			}
			if (indBean.getHanging() != 0) {
				ind.addAttribute("w:hanging", Unit.convertCmToTwip(indBean
						.getHanging()));
			} else {
				ind.addAttribute("w:first-line", Unit.convertCmToTwip(indBean
						.getFirstLine()));
			}

		}

		// 段落间距
		// <w:spacing w:before-lines="50" w:after-lines="20" w:line="400"
		// w:line-rule="exact"/>
		ParagraphSpaceBean spaceBean = paragraphBean.getParagraphSpaceBean();
		if (spaceBean != null) {
			Element spacing = pPr.addElement("w:spacing");
			spacing.addAttribute("w:line-rule", spaceBean.getRuleType()
					.getValue());
			// 间距Bean 磅转缇 *20 绝对值 最小值
			if (spaceBean.getRuleType() == LineRuleType.EXACT
					|| spaceBean.getRuleType() == LineRuleType.AT_LEAST) {
				spacing.addAttribute("w:line", Unit.convertPtToTwip(spaceBean
						.getLine()));
			} else if (spaceBean.getRuleType() == LineRuleType.AUTO) {
				// 设置为Auto时，值等于line*240
				spacing.addAttribute("w:line", String.valueOf(spaceBean
						.getLine() * 240));
			}
			if (spaceBean.getBeforeLines() == null) {
				spacing.addAttribute("w:before-autospacing", "on");
				spacing.addAttribute("w:before", "100");
			} else {
				// 行转缇 *100
				if (spaceBean.getBeforeLines() != 0 && spaceBean.getType() == 0) {
					spacing.addAttribute("w:before-lines", (int) (spaceBean.getBeforeLines() * 100) + "");
				} else if (spaceBean.getBeforeLines() != 0 && spaceBean.getType() == 1) {
					spacing.addAttribute("w:before", (int) (spaceBean.getBeforeLines() * 20) + "");
				}
			}
			if (spaceBean.getAfterLines() == null) {
				spacing.addAttribute("w:after-autospacing", "on");
				spacing.addAttribute("w:after", "100");
			} else {
				if (spaceBean.getAfterLines() != 0 && spaceBean.getType() == 0) {
					spacing.addAttribute("w:after-lines", (int) (spaceBean.getAfterLines() * 100) + "");
				} else if (spaceBean.getAfterLines() != 0 && spaceBean.getType() == 1) {
					spacing.addAttribute("w:after", (int) (spaceBean.getAfterLines() * 20) + "");
				}
			}

		}

	}
}
