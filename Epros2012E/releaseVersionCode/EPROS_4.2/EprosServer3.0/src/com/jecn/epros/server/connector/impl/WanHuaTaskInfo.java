package com.jecn.epros.server.connector.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.ws.BindingProvider;

import com.jecn.epros.server.action.web.login.ad.wanhua.JecnWanHuaAfterItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.wanhua.task.ecs.SI118ECSCommonDataOut;
import com.jecn.webservice.wanhua.task.ecs.SI118ECSCommonDataOutService;
import com.jecn.webservice.wanhua.task.interfaceserver.DATAROW;
import com.jecn.webservice.wanhua.task.interfaceserver.HEADER;
import com.jecn.webservice.wanhua.task.interfaceserver.MESSAGE;
import com.jecn.webservice.wanhua.task.interfaceserver.REQUEST;

/**
 * 万华-OA代办消息
 * 
 * @author ZXH
 * @date 2018-7-24
 */
public class WanHuaTaskInfo extends JecnBaseTaskSend {

	private IPersonDao personDao;

	private static SI118ECSCommonDataOutService service = null;
	private static SI118ECSCommonDataOut outPort = null;

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 * @param 当前操作人
	 *            ，取消当前操作人代办
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew taskBeanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long cancelPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (taskBeanNew.getUpState() == 0 && taskBeanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
			log.info("创建代办开始~");
			// 创建代办
			handlerTaskToDo(myTaskBean, taskBeanNew, handlerPeopleIds, 1, cancelPeopleId);
		} else if (taskBeanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
			myTaskBean.setTaskStage(taskBeanNew.getUpState());

			handlerPeopleIds = new HashSet<Long>();
			handlerPeopleIds.add(cancelPeopleId);
			handlerTaskToDo(myTaskBean, taskBeanNew, handlerPeopleIds, 3, cancelPeopleId);
		} else {// 一条from的数据,一条to 的数据
			// 代办转已办,是否为多审判断
			// 设置上一环节任务待办任务状态为false，表示未已审批；
			myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
			myTaskBean.setTaskStage(taskBeanNew.getUpState());

			Set<Long> cancelIds = new HashSet<Long>();
			cancelIds.add(cancelPeopleId);
			handlerTaskToDo(myTaskBean, taskBeanNew, cancelIds, 2, cancelPeopleId);

			if ((taskBeanNew.getTaskElseState() == 2 || taskBeanNew.getTaskElseState() == 3)
					|| taskBeanNew.getState() != taskBeanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				// 审批任务，发送下一环节任务待办
				handlerTaskToDo(myTaskBean, taskBeanNew, handlerPeopleIds, 1, cancelPeopleId);
			}
		}
	}

	/**
	 * 批量删除代办
	 */
	@Override
	public void sendInfoCancels(JecnTaskBeanNew taskBeanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {

		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
		Set<Long> setCancelPeopleIds = new HashSet<Long>();
		setCancelPeopleIds.addAll(cancelPeoples);

		// 审批任务，代办转为已办
		handlerTaskToDo(myTaskBean, taskBeanNew, setCancelPeopleIds, 2, curPeopleId);

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		// 审批任务，发送下一环节任务待办
		handlerTaskToDo(myTaskBean, taskBeanNew, handlerPeopleIds, 1, curPeopleId);

	}

	/**
	 * 
	 * @param myTaskBean
	 * @param taskBeanNew
	 * @param handlerPeopleIds
	 * @param taskStatus
	 *            代办状态： 待办为1，已办为2,完成3
	 * @throws Exception
	 */
	private void handlerTaskToDo(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Set<Long> handlerPeopleIds,
			int taskStatus, Long curPeopleId) throws Exception {
		MESSAGE message = new MESSAGE();
		HEADER header = new HEADER();
		header.setACTION("CREATE");
		header.setBO("TASK");
		header.setDATE(JecnCommon.getStringbyDateHMS(new Date()));
		header.setDESTINATIONID("PORTAL");
		header.setSIZE("" + handlerPeopleIds.size());
		header.setSOURCEID("EPROS");

		message.setHEADER(header);

		message.setREQUEST(getDataRows(myTaskBean, taskBeanNew, handlerPeopleIds, taskStatus, curPeopleId));
		if (service == null) {
			service = new SI118ECSCommonDataOutService();
			outPort = service.getHTTPPort();
			// 设置用户名和密码
			BindingProvider provider = (BindingProvider) outPort;

			Map<String, Object> req_ctx = provider.getRequestContext();
			req_ctx.put(BindingProvider.USERNAME_PROPERTY, JecnWanHuaAfterItem.getValue("TASK_USER"));
			req_ctx.put(BindingProvider.PASSWORD_PROPERTY, JecnWanHuaAfterItem.getValue("TASK_PWD"));
		}

		outPort.si118ECSCommonDataOut(message);
	}

	public REQUEST getDataRows(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Set<Long> handlerPeopleIds,
			int taskStatus, Long curPeopleId) {
		REQUEST request = new REQUEST();

		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());
		DATAROW datarow = null;
		for (Long handlerPeopleId : handlerPeopleIds) {
			datarow = new DATAROW();
			setDataRows(datarow, myTaskBean, taskBeanNew, handlerPeopleId, createUser, taskStatus, curPeopleId);
			request.getDATAROW().add(datarow);
		}
		return request;
	}

	/**
	 * 
	 * @param myTaskBean
	 * @param taskBeanNew
	 * @param handlerPeopleId
	 *            处理人ID
	 * @param createUser
	 * @param taskStatus
	 *            代办状态： 待办为1，已办为2,完成3
	 * @return
	 */
	private void setDataRows(DATAROW datarow, MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Long handlerPeopleId,
			JecnUser createUser, int taskStatus, Long curPeopleId) {
		// 代办处理人
		JecnUser handlerUser = personDao.get(handlerPeopleId);
		datarow.setOrderSource("EPROS");
		// 流程唯一ID
		datarow.setOrderId(getSN(taskBeanNew.getId(), taskBeanNew.getRid()));
		datarow.setOrderTypeName(taskBeanNew.getTaskName());
		datarow.setOrderCode("");
		datarow.setOrderUserId(createUser.getLoginName());
		datarow.setOrderUserName(createUser.getTrueName());
		datarow.setOrderTime(JecnCommon.getStringbyDateHMS(taskBeanNew.getUpdateTime()));
		datarow.setOrderTaskTime(JecnCommon.getStringbyDateHMS(taskBeanNew.getCreateTime()));
		// 代办唯一ID
		datarow.setOrderTaskId(getTaskOrderId(taskBeanNew, handlerPeopleId, taskStatus));

		// 1：创建代办，2 已办，3 完成
		datarow.setOrderTaskUrl(getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, taskStatus == 1 ? false
				: true));
		datarow.setOrderTaskCurrentUserId(handlerUser.getLoginName());
		datarow.setOrderTaskCurrentUserName(handlerUser.getTrueName());
		// 待办为1，已办为2,完成3
		datarow.setOrderTaskStatus("" + taskStatus);
		datarow.setOrderTaskName(taskStatus == 1 ? taskBeanNew.getStateMark() : taskBeanNew.getUpStateMark());
		datarow.setOrderTaskDesc(getTaskDescTitle(taskBeanNew, handlerUser, taskStatus, taskBeanNew.getResPeopleId()));
		datarow.setOrderDoneTime(taskStatus != 1 ? JecnCommon.getStringbyDateHMS(taskBeanNew.getUpdateTime()) : "");
	}

	// 提交流程审批处理
	private String getTaskDescTitle(JecnTaskBeanNew taskBeanNew, JecnUser handlerUser, int taskStatus, Long resPeopleId) {
		String ZHTaskDescTitle = "";
		String ENTaskDescTitle = "";
		String nameTile = "";
		if (taskBeanNew.getResPeopleId() != null) {
			JecnUser resUser = personDao.get(resPeopleId);
			nameTile = resUser.getTrueName() + "(" + resUser.getLoginName() + ")";
		}
		if (taskBeanNew.getTaskElseState() == 4 && taskStatus == 1) {
			ZHTaskDescTitle = nameTile + "驳回了文件审批流程，请您尽快处理";
			ENTaskDescTitle = nameTile + "rejected document release application.";
		} else {
			ZHTaskDescTitle = nameTile + "提交了文件审批流程，请您尽快处理";
			ENTaskDescTitle = nameTile + "submitted document release application.";
		}
		return "ZH@!" + ZHTaskDescTitle + "_@!@_EN@!" + ENTaskDescTitle;
	}

	private String SYS_NAME = "EPROS";

	private String getSN(Long taskId, Long rId) {
		return SYS_NAME + "_" + taskId + "_" + rId;
	}


	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew) + "审批");
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setStageName(beanNew.getStateMark());
		return myTaskBean;
	}

	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {
		List<Long> cancelPeoples = new ArrayList<Long>();
		cancelPeoples.add(curPeopleId);
		try {
			sendInfoCancels(jecnTaskBeanNew, personDao, null, cancelPeoples, curPeopleId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
