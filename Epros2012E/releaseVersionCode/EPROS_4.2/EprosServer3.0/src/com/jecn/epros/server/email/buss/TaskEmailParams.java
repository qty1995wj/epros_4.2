package com.jecn.epros.server.email.buss;

import com.jecn.epros.server.bean.process.ProcessAttributeBean;

public class TaskEmailParams {
	
	/** 目的 */
	private String flowPurpose;
	/** 适用范围 */
	private String applicability;
	/** 流程责任属性 0流程监护人 1流程责任人 2流程责任部门 */
	private ProcessAttributeBean processAttribute ;
	
	public String getFlowPurpose() {
		return flowPurpose;
	}
	public void setFlowPurpose(String flowPurpose) {
		this.flowPurpose = flowPurpose;
	}
	public String getApplicability() {
		return applicability;
	}
	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}
	public ProcessAttributeBean getProcessAttribute() {
		return processAttribute;
	}
	public void setProcessAttribute(ProcessAttributeBean processAttribute) {
		this.processAttribute = processAttribute;
	}
}
