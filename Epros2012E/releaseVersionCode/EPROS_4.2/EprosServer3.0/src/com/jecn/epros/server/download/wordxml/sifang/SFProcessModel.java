package com.jecn.epros.server.download.wordxml.sifang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ActiveItSystem;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 四方操作说明模版
 * 
 * @author hyl
 * 
 */
public class SFProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍行距 *****/
	private final ParagraphLineRule vLine2 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private final ParagraphLineRule leastVline12 = new ParagraphLineRule(12F, LineRuleType.AT_LEAST);
	private final float tableWidth = 18F;

	public SFProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(tableWidth);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(6.5F);
		setDocStyle(".", textTitleStyle());
	}

	@Override
	public void beforeHandle() {
		Sect firstSect = super.docProperty.getFirstSect();
		ParagraphBean pb = new ParagraphBean(Align.center, "宋体 (中文正文)", Constants.sanhao, true);
		pb.setSpace(2F, 1F, new ParagraphLineRule(1.73F, LineRuleType.AUTO));
		firstSect.createParagraph("一、" + processDownloadBean.getFlowName(), pb);
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	protected void a10(ProcessFileItem processFileItem, String[] ts, List<String[]> rd) {
		processFileItem.setBelongTo(docProperty.getFlowChartSect());
		docProperty.getFlowChartSect().createMutilEmptyParagraph(1);
		createActivityTable(processFileItem);
	}

	public void createActivityTable(ProcessFileItem processFileItem) {
		processFileItem.getBelongTo().breakPage();
		// 标题
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		boolean showTimeLine = JecnConfigTool.showActTimeLine();
		boolean showInOut = JecnContants.showActInOutBox;
		boolean showIT = false;

		// ---------标题
		List<String> titles = new ArrayList<String>();
		List<List<String>> datas = new ArrayList<List<String>>();
		// 活动编号
		titles.add(JecnUtil.getValue("activitynumbers"));
		// 执行角色
		titles.add("执行角色");
		// 活动名称
		titles.add(JecnUtil.getValue("activityTitle"));
		// 活动说明
		titles.add("活动说明");
		if (showInOut) {
			// 输入
			titles.add(JecnUtil.getValue("input"));
			// 输出
			titles.add(JecnUtil.getValue("output"));
		}
		if (showTimeLine) {
			// 现状目标
			titles.add(JecnUtil.getValue("timeLimit") + "\n(现状/目标" + JecnConfigTool.getActiveTimeLineUtil() + ")");
		}
		if (showIT) {
			// 信息化
			titles.add("信息化");
		}
        titles.add("备注");
		// 宽度
		List<Float> widthList = new ArrayList<Float>();
		widthList.add(5F);
		widthList.add(5F);
		widthList.add(5F);
		if (showInOut) {
			widthList.add(5F);
			widthList.add(5F);
		}
		if (showTimeLine) {
			widthList.add(5F);
		}
		if (showIT) {
			widthList.add(5F);
		}
		widthList.add(5F);
		// 其余空间被活动说明占据
		float total = 0;
		for (float f : widthList) {
			total += f;
		}
		float max = 40;
		// 活动说明的宽度
		float last = max - total > 2 ? max - total : 2;
		widthList.add(3, last);

		// --------内容
		Map<Long, String> idToKey = new HashMap<Long, String>();
		if (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent()) {
			List<KeyActivityBean> keyActivityShowList = processFileItem.getDataBean().getKeyActivityShowList();
			for (KeyActivityBean k : keyActivityShowList) {
				idToKey.put(k.getId(), k.getActivityShowControl());
			}
		}
		List<String[]> dataRows = JecnWordUtil.obj2String(processFileItem.getDataBean().getAllActivityShowList());
		Map<String, List<String>> activeIdToITMap = getActiveIdToITMap();
		for (String[] data : dataRows) {
			String content = JecnUtil.nullToEmpty(data[3]);
			// if
			// (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent())
			// {
			// String keyContent =
			// JecnUtil.nullToEmpty(idToKey.get(Long.valueOf(data[8].toString())));
			// if (StringUtils.isNotBlank(content) &&
			// StringUtils.isNotBlank(keyContent)) {
			// content = content + "\n以下是重要说明：\n" + keyContent;
			// } else if (StringUtils.isNotBlank(keyContent)) {
			// content = "以下是重要说明：\n" + keyContent;
			// }
			// }
			List<String> row = new ArrayList<String>(Arrays.asList(data[0], data[1], data[2], content));
			if (showInOut) {
				// 输入
				row.add(data[4]);
				// 输出
				row.add(data[5]);
			}
			if (showTimeLine) {
				// 现状目标
				String targetValue = StringUtils.isBlank(JecnUtil.objToStr(data[6])) ? "" : JecnUtil.objToStr(data[6]);
				String statusValue = StringUtils.isBlank(JecnUtil.objToStr(data[7])) ? "" : JecnUtil.objToStr(data[7]);
				String timeLineValue = ProcessFileModel.concatTimeLineValue(targetValue, statusValue);
				row.add(timeLineValue);
			}
			if (showIT) {
				// 信息化
				List<String> its = activeIdToITMap.get(data[8]);
				if (its != null && its.size() > 0) {
					row.add(JecnUtil.concat(its, "/"));
				} else {
					row.add("");
				}
			}
            row.add("");
			datas.add(row);
		}

		// 添加标题
		int columnSize = titles.size();
		rowData.add(titles.toArray(new String[columnSize]));
		for (List<String> data : datas) {
			rowData.add(data.toArray(new String[columnSize]));
		}
		float[] widths = new float[columnSize];
		for (int i = 0; i < columnSize; i++) {
			widths[i] = widthList.get(i);
		}

		Table tbl = new Table(tblStyle(), widths);
		tbl.createTableRow(rowData, tblContentStyle());
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
		tbl.setValign(Valign.center);
		processFileItem.getBelongTo().addTable(tbl);
	}

	private Map<String, List<String>> getActiveIdToITMap() {
		List<ActiveItSystem> activeItSystems = processDownloadBean.getActiveItSystems();
		Map<String, List<String>> m = new HashMap<String, List<String>>();
		for (ActiveItSystem s : activeItSystems) {
			List<String> ls = JecnUtil.getElseAddList(m, s.getActiveId().toString());
			ls.add(s.getSystemName());
		}
		return m;
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getTitleSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 2F, 2.54F, 3.17F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getCharSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2F, 1.76F, 1.5F, 1.76F, 0.5F, 0.5F);
		sectBean.setSize(42F, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect sect) {
		SectBean sectBean = getTitleSectBean();
		sect.setSectBean(sectBean);
		addTitleContent(sect);
		createTitlehdr(sect, 7F, 6F, 3F);
	}

	/**
	 * 添加首页内容
	 * 
	 * @param sect
	 */
	private void addTitleContent(Sect sect) {
		ParagraphBean pp = new ParagraphBean(Align.left, Fonts.HEI, Constants.sanhao, false);
		pp.setParagraphSpaceBean(new ParagraphSpaceBean(0.5F, 0.5F, vLine1));
		// 按要求写死
		sect.createParagraph("密级：四方股份普通商密▲ 5年", pp);
		sect.createParagraph("编号：" + nullToEmpty(processDownloadBean.getFlowInputNum()), pp);
		sect.createMutilEmptyParagraph(2);

		pp = new ParagraphBean(Align.center, "宋体", Constants.yihao, true);
		pp.setParagraphSpaceBean(new ParagraphSpaceBean(0.5F, 0.5F, vLine1));
		sect.createParagraph(processDownloadBean.getFlowName(), pp);
		sect.createParagraph("说明书", pp);
		sect.createMutilEmptyParagraph(5);

		JecnTaskHistoryNew latestHistoryNew = processDownloadBean.getLatestHistoryNew();
		List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow = new ArrayList<JecnTaskHistoryFollow>();
		if (latestHistoryNew != null) {
			listJecnTaskHistoryFollow = latestHistoryNew.getListJecnTaskHistoryFollow();
		}

		// 拟稿
		String authorPeople = latestHistoryNew == null ? "" : nullToEmpty(latestHistoryNew.getDraftPerson());
		// 审核阶段(会审)
		String reviewPeople = "";
		// 批准人
		String approvalPeople = "";
		// 会签(自己填)
		String joinPeople = "";
		for (JecnTaskHistoryFollow p : listJecnTaskHistoryFollow) {
			Integer state = p.getStageMark();
			if (state == null) {
				continue;
			}
			if (state == 3) {
				reviewPeople = p.getName();
			} else if (state == 4) {
				approvalPeople = p.getName();
			} else if (state == 0) {
				authorPeople = p.getName();
			}
		}
		authorPeople = trim(authorPeople);
		reviewPeople = trim(reviewPeople);
		approvalPeople = trim(approvalPeople);
		joinPeople = trim(joinPeople);

		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体 (中文正文)", Constants.xiaoer, false);
		pBean.setInd(3.5F, 0F, 0, 0);
		pBean.setParagraphSpaceBean(new ParagraphSpaceBean(0F, 0F, new ParagraphLineRule(2F, LineRuleType.AUTO)));
		sect.createParagraph("编制：" + nullToEmpty(authorPeople), pBean);
		sect.createParagraph("审核：" + nullToEmpty(reviewPeople), pBean);
		sect.createParagraph("会签：" + nullToEmpty(joinPeople), pBean);
		sect.createParagraph("批准：" + nullToEmpty(approvalPeople), pBean);

		sect.createMutilEmptyParagraph(4);

		pp = new ParagraphBean(Align.center, "宋体 (中文正文)", Constants.xiaoer, false);
		pp.setParagraphSpaceBean(new ParagraphSpaceBean(0.5F, 0.5F, vLine1));
		sect.createParagraph("中车青岛四方机车车辆股份有限公司", pp);
		if (StringUtils.isNotBlank(processDownloadBean.getOrgName())) {
			sect.createParagraph(
					JecnUtil.ifNotEndWithAppendOrBlank(nullToEmpty(processDownloadBean.getOrgName()), "部"), pp);
		}
		sect.createParagraph(getPubTime(), pp);
	}

	private String getPubTime() {
		return processDownloadBean.getPubDate("yyyy年MM月");
	}

	private String trim(String str) {
		if (StringUtils.isBlank(str)) {
			return "";
		}
		if ("null".equals(str)) {
			return "";
		}
		if (str.endsWith("/")) {
			return str.substring(0, str.length() - 1);
		}
		return str;
	}

	protected void setParagraphBean(Paragraph p, ParagraphBean pBean) {
		p.setParagraphBean(pBean);
	}

	protected void setParagraphBean(List<Paragraph> ps, ParagraphBean pBean) {
		for (Paragraph p : ps) {
			setParagraphBean(p, pBean);
		}
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(firstSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getCharSectBean();
		flowChartSect.setSectBean(sectBean);
		createCharfhdr(flowChartSect);
	}

	private void createTitlehdr(Sect sect, float left, float center, float right) {
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		TableBean lineBean = new TableBean();
		lineBean.setTableAlign(Align.center);
		lineBean.setBorderBottom(0.6f);
		Table lineTable = new Table(lineBean, new float[] { left, center, right });
		List<String[]> row = new ArrayList<String[]>();
		row
				.add(new String[] { "", processDownloadBean.getFlowName() + "说明书",
						"版本号：" + processDownloadBean.getVersion() });
		lineTable.createTableRow(row, new ParagraphBean(Align.left, "黑体", Constants.xiaosi, false));
		Paragraph logoP = lineTable.getRow(0).getCell(0).getParagraph(0);
		Image logo = (Image) super.getLogoImage(5.74F, 0.89F);
		logoP.appendGraphRun(logo);
		header.addTable(lineTable);
		lineTable.setValign(Valign.bottom);

	}

	private void createCommhdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体 (中文正文)", Constants.xiaosi, false);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		TableBean tblStyle = tblStyle();
		tblStyle.setBorder(1.5F);
		Table table = header.createTable(tblStyle, new float[] { left, center, right });
		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "", "集成化产品开发流程", processDownloadBean.getFlowName() });
		data.add(new String[] { "", "", "版本号：" + nullToEmpty(processDownloadBean.getVersion()) });
		table.createTableRow(data, pBean);

		table.getRow(0).getCell(0).setVmergeBeg(true);
		table.getRow(1).getCell(0).setVmerge(true);
		table.getRow(0).getCell(0).setRowspan(2);

		table.getRow(0).getCell(1).setVmergeBeg(true);
		table.getRow(1).getCell(1).setVmerge(true);
		table.getRow(0).getCell(1).setRowspan(2);

		Paragraph logoP = table.getRow(0).getCell(0).getParagraph(0);
		Image logo = (Image) super.getLogoImage(5.74F, 0.89F);
		logoP.appendGraphRun(logo);

		List<TableRow> rows = table.getRows();
		for (TableRow row : rows) {
			row.setHeight(0.78F);
		}
		table.setValign(Valign.center);
		List<Paragraph> paragraphs = table.getRow(0).getCell(1).getParagraphs();
		setParagraphBean(paragraphs, new ParagraphBean(Align.center, "宋体 (中文正文)", Constants.xiaosi, true));

		createHeaderLine(header, left + center + right);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCharCommhdr(Sect sect, float left, float center, float right) {
		createCommhdr(sect, left, center, right);
	}

	private void createHeaderLine(Header header, float width) {
		// 生成一条下划线
		TableBean lineBean = new TableBean();
		lineBean.setTableAlign(Align.center);
		lineBean.setBorderBottom(1f);
		Table lineTable = new Table(lineBean, new float[] { width });
		List<String[]> row = new ArrayList<String[]>();
		row.add(new String[] { "" });
		lineTable.createTableRow(row, new ParagraphBean(Constants.shihao));
		lineTable.getRow(0).setHeight(0.4F);
		header.addTable(lineTable);
	}

	private void createCommfdr(Sect sect) {
		Footer header = sect.createFooter(HeaderOrFooterType.odd);
		Paragraph pageParagraph = new Paragraph("", new ParagraphBean(Align.center, "Arial", Constants.xiaowu, false));
		pageParagraph.appendCurPageRun();
		header.addParagraph(pageParagraph);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 6F, 5.72F, 5.72F);
		createCommfdr(sect);
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCharCommhdr(sect, 6F, 5.72F, 5.72F);
		createCommfdr(sect);
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体 (中文正文)", Constants.xiaosi, false);
		textContentStyle.setInd(0.52F, 0F, 0F, 0F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(20F, LineRuleType.EXACT));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("宋体", Constants.xiaosi, true);
		textTitleStyle.setInd(0F, 0F, 0.74F, 0F);
		textTitleStyle.setSpace(0.5F, 0.2F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体 (中文正文)", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, "宋体 (中文正文)", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(1.5F);
		tblStyle.setBorderInsideH(1);
		tblStyle.setBorderInsideV(1);
		tblStyle.setTableAlign(Align.center);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

}
