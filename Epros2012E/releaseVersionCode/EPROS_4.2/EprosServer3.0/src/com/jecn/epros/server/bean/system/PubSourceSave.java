package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class PubSourceSave implements Serializable {
	private Set<Long> orgs;
	private Set<Long> poses;
	private Set<Long> peoples;
	private Set<Long> groups;
	private Long peopleId;
	private List<JecnTreeBean> resources;

	public List<JecnTreeBean> getResources() {
		return resources;
	}

	public void setResources(List<JecnTreeBean> resources) {
		this.resources = resources;
	}

	public Set<Long> getOrgs() {
		return orgs;
	}

	public void setOrgs(Set<Long> orgs) {
		this.orgs = orgs;
	}

	public Set<Long> getPoses() {
		return poses;
	}

	public void setPoses(Set<Long> poses) {
		this.poses = poses;
	}

	public Set<Long> getPeoples() {
		return peoples;
	}

	public void setPeoples(Set<Long> peoples) {
		this.peoples = peoples;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	public Set<Long> getGroups() {
		return groups;
	}

	public void setGroups(Set<Long> groups) {
		this.groups = groups;
	}

}
