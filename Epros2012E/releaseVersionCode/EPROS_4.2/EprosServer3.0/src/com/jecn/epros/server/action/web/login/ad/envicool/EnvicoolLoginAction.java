package com.jecn.epros.server.action.web.login.ad.envicool;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LdapAuthVerifier;

public class EnvicoolLoginAction extends JecnAbstractADLoginAction {

	public EnvicoolLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	static {
		JecnEnvicoolItem.start();
	}

	public String login() {
		try {
			// 单点登录名称
			String adLoginName = this.loginAction.getLoginName();
			// 单点登录密码
			String adPassWord = this.loginAction.getPassword();

			log.info(" adLoginName = " + adLoginName + " adPassWord = " + adPassWord);
			if ("admin".equals(adLoginName) && StringUtils.isNotBlank(adLoginName)
					&& StringUtils.isNotBlank(adPassWord)) {
				return loginAction.userLoginGen(true);
			}

			// 进行AD域验证
			int result = getLdapAuthVerifierByNameAndPassword(adLoginName + JecnEnvicoolItem.DOMAIN, adPassWord)
					.verifyAuth();
			log.info("权限验证结果:" + result);
			// 验证成功
			if (result == LdapAuthVerifier.SUCCESS) {
				return loginByLoginName(adLoginName);
			} else if (result == LdapAuthVerifier.FAILED) {
				// 用户名或密码不正确！
				this.loginAction.addFieldError("loginMessage", loginAction.getText("nameOrPasswordError"));
			} else if (result == LdapAuthVerifier.CONNECTION_ERROR) {
				// 无法验证用户，请联系系统管理员!
				this.loginAction.addFieldError("loginMessage", loginAction.getText("anJianZhiYuanADConnectFailed"));
			}
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
		return LoginAction.INPUT;
	}

	/**
	 * 管理员账号认证
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static LdapContext getLdapContext() throws NamingException {
		return getLdapAuthVerifier().verifyAndGetContext();
	}

	private LdapAuthVerifier getLdapAuthVerifierByNameAndPassword(String name, String password) {
		return LdapAuthVerifier.newInstance(name, password, JecnEnvicoolItem.LDAP_URL);
	}

	public static LdapAuthVerifier getLdapAuthVerifier() {
		return LdapAuthVerifier.newInstance(JecnEnvicoolItem.ADMIN_NAME, JecnEnvicoolItem.ADMIN_PASSWORD,
				JecnEnvicoolItem.LDAP_URL);
	}
}
