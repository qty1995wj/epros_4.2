package com.jecn.epros.server.email.bean;

/**
 * JecnEmailContent entity. @author MyEclipse Persistence Tools
 */

public class JecnEmailContent implements java.io.Serializable {


	private String id;
	/** 主题**/
	private String subject;
	/** 内容**/
	private String content;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	


}