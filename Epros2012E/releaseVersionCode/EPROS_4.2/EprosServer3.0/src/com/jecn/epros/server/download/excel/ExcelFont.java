package com.jecn.epros.server.download.excel;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.WritableFont;
/***
 * Excel样式设置封装类 
 * @author chehuanbo
 * @since V3.06
 */
public class ExcelFont {
	private Colour colour = Colour.WHITE; // 背景颜色
	private VerticalAlignment vAlignment = VerticalAlignment.CENTRE; // 默认垂直居中
	private Alignment alignment = Alignment.CENTRE; // 默认 水平居中
	private WritableFont writableFont; // 单元格字体大小

	public ExcelFont(WritableFont writableFont) {
       this.writableFont=writableFont;
	}
    
	public ExcelFont(WritableFont writableFont,Colour colour){
		this.writableFont=writableFont;
		this.colour=colour;
	}
	
	public ExcelFont(Colour colour, VerticalAlignment vAlignment,
			Alignment alignment, WritableFont writableFont) {
		this.colour = colour;
		this.vAlignment = vAlignment;
		this.alignment = alignment;
		this.writableFont = writableFont;
	}

	public Colour getColour() {
		return colour;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	public VerticalAlignment getvAlignment() {
		return vAlignment;
	}

	public void setvAlignment(VerticalAlignment vAlignment) {
		this.vAlignment = vAlignment;
	}

	public Alignment getAlignment() {
		return alignment;
	}

	public void setAlignment(Alignment alignment) {
		this.alignment = alignment;
	}

	public WritableFont getWritableFont() {
		return writableFont;
	}

	public void setWritableFont(WritableFont writableFont) {
		this.writableFont = writableFont;
	}


}