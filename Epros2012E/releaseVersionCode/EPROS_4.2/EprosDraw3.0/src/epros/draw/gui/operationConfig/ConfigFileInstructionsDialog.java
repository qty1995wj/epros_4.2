package epros.draw.gui.operationConfig;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 配置流程操作说明
 * 
 * @author 2012-07-02
 * 
 */
public class ConfigFileInstructionsDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(ConfigFileInstructionsDialog.class);

	/** 公司名称+ logo图标面板 */
	private JecnPanel logoPanel = null;
	private ConfigFileConPanyPanel logoComPanyPanel = new ConfigFileConPanyPanel(null);
	/** 主面板 操作说明配置信息 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 包括，结果面板、标题按钮面板 */
	private JecnPanel ruleModePanel = new JecnPanel();
	/** 标题列表面板 */
	private JScrollPane titlePanel = new JScrollPane();
	/** 标题按钮面板面板 */
	private JecnPanel titleButtonPanel = new JecnPanel();
	/** 左标题按钮面板面板 */
	private JecnPanel leftTitleButtonPanel = new JecnPanel();
	/** 右标题按钮面板面板 */
	private JecnPanel rightTitleButtonPanel = new JecnPanel();
	/** 底部按钮面板 */
	private JecnPanel bomButtonPanel = new JecnPanel();

	/** 上移按钮 */
	private JButton upButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("up"));
	/** 下移按钮 */
	private JButton downButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("down"));
	/** 加入标题按钮 */
	private JButton addTitleButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("join"));

	/** 编辑标题按钮 */
	private JButton editTitleButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("edit"));

	/** 删除标题按钮 */
	private JButton deleteButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("delete"));
	/** 默认标题按钮 */
	private JButton defaultButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("default"));

	/** 确定按钮 */
	private JButton okButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("confirm"));
	/** 取消按钮 */
	private JButton cancelButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancel"));

	private JecnTable titleTable = null;

	private boolean isOperation = false;

	// ////////////////公司名称公司logo//////////////////
	/** 公司名称 lable * */
	private JLabel compLabel = null;
	// /** 公司logo lable */
	// private JLabel logoLabel = null;
	/** 公司名称 */
	private JTextField compFiled = null;
	// /** logo */
	// private JLabel logoImageLabel = null;
	// /** logo选择按钮 */
	// private JButton logoButton = null;
	//
	// /** 上传的logo图标 */
	// private File selectedFile = null;

	/** 存放xml数据list */
	private List<DrawOperationDescriptionConfigBean> operConfigList = null;
	//
	// private String logoIcomPath = null;
	//
	// /** 是否使用logo图标 true:读取logo */
	// private JCheckBox logoCheckBox = null;
	// /** 查看全图：logo */
	// private JButton viewLogoButton = null;
	// ////////////////公司名称公司logo//////////////////

	/** 活动明细是否显示输入输出面板 */
	private JecnPanel showActiveIOPanel = null;
	/** 是否显示输入输出复选框，勾上显示，未勾上隐藏 */
	private JCheckBox showActiveIOCheckBox = null;
	/** 是否显示输入输出复选框解释说明 */
	private JTextArea activeIOArea = null;
	/** 说明：勾选上且[ */
	private String activeIONoteBefore = null;
	/** ]显示时，操作说明中才显示活动输入输出！ */
	private String activeIONoteAfter = null;

	public ConfigFileInstructionsDialog() {
		operConfigList = OperationConfigUtil.readOperationConfigXml();
		initCompotents();
		initLayout();
		initButtonAction();
		initData();
	}

	private void initCompotents() {
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("configurationProcessOperatingInstructions"));
		Dimension size = new Dimension(550, 600);
		this.setSize(size);
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		ruleModePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		bomButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		titlePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		titleButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		rightTitleButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		leftTitleButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		titlePanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 公司信息
		initCompany();

		/** ********* 活动明细是否显示输入输出********** */
		// 活动明细是否显示输入输出面板
		showActiveIOPanel = new JecnPanel();
		// 是否显示输入输出复选框，勾上显示，未勾上隐藏
		showActiveIOCheckBox = new JCheckBox();
		activeIOArea = new JTextArea();

		showActiveIOPanel.setBorder(BorderFactory.createTitledBorder(""));

		showActiveIOCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		showActiveIOCheckBox.setText(JecnResourceUtil.getJecnResourceUtil().getValue("showActiveIO"));
		showActiveIOCheckBox.setOpaque(false);
		showActiveIOCheckBox.setBorder(null);
		activeIONoteBefore = JecnResourceUtil.getJecnResourceUtil().getValue("activeIONoteBefore");
		activeIONoteAfter = JecnResourceUtil.getJecnResourceUtil().getValue("activeIONoteAfter");

		activeIOArea.setOpaque(false);
		activeIOArea.setBorder(null);
		activeIOArea.setForeground(Color.GRAY);
		activeIOArea.setEditable(false);
		activeIOArea.setWrapStyleWord(true);
		activeIOArea.setLineWrap(true);
		/** ********* 活动明细是否显示输入输出********** */

		// 显示公司logo图片、是否启用logo图片标识
		logoComPanyPanel.getLogoImageLabel().setVisible(true);
		logoComPanyPanel.getLogoCheckBox().setVisible(true);
		logoComPanyPanel.setBorder(null);

	}

	/**
	 * 初始化公司信息
	 * 
	 */
	private void initCompany() {
		logoPanel = new JecnPanel();

		logoPanel.setBorder(BorderFactory.createTitledBorder(""));
		compLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("compNameC"));
		// logoLabel = new
		// JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("compLogoC"));

		compFiled = new JTextField();
		// logo
		// logoImageLabel = new JLabel();
		// Dimension dimension = new Dimension(150, 35);
		// logoImageLabel.setPreferredSize(dimension);
		// logoImageLabel.setMinimumSize(dimension);
		// logoImageLabel.setMaximumSize(dimension);
		// logoImageLabel.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("logoNote"));

		// select
		// logoButton = new
		// JButton(JecnResourceUtil.getJecnResourceUtil().getValue("select"));
		// logoButton.setToolTipText(JecnResourceUtil.getJecnResourceUtil().getValue("logoNote"));
		//
		// // 是否使用
		// logoCheckBox = new
		// JCheckBox(JecnResourceUtil.getJecnResourceUtil().getValue("useOrNotUse"));
		// logoCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		//
		// // 查看
		// viewLogoButton = new
		// JButton(JecnResourceUtil.getJecnResourceUtil().getValue("viewLogo"));
		//
		// // 存在图片可查看
		// viewLogoButton.setEnabled(false);
		logoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		initLayOutCompany();
	}

	/**
	 * 公司信息初始化布局
	 * 
	 */
	private void initLayOutCompany() {
		logoPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		logoPanel.add(compLabel, c);

		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		logoPanel.add(compFiled, c);

		c = new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		logoPanel.add(logoComPanyPanel, c);

	}

	public void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(logoPanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(ruleModePanel, c);

		ruleModePanel.setLayout(new GridBagLayout());
		ruleModePanel.setBorder(BorderFactory.createTitledBorder(""));

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		ruleModePanel.add(titlePanel, c);
		this.titleTable = new TitleTable();
		titlePanel.setViewportView(titleTable);

		// 活动明细是否显示输入输出面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		ruleModePanel.add(showActiveIOPanel, c);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		ruleModePanel.add(titleButtonPanel, c);

		titleButtonPanel.setLayout(new GridBagLayout());

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		titleButtonPanel.add(leftTitleButtonPanel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		titleButtonPanel.add(rightTitleButtonPanel, c);

		leftTitleButtonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		leftTitleButtonPanel.add(upButton);
		leftTitleButtonPanel.add(downButton);

		rightTitleButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		rightTitleButtonPanel.add(addTitleButton);
		rightTitleButtonPanel.add(deleteButton);
		rightTitleButtonPanel.add(editTitleButton);
		rightTitleButtonPanel.add(defaultButton);

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(bomButtonPanel, c);
		bomButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		bomButtonPanel.add(okButton);
		bomButtonPanel.add(cancelButton);

		this.getContentPane().add(mainPanel);

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		showActiveIOPanel.add(showActiveIOCheckBox, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.BOTH, new Insets(
				2, 6, 2, 2), 0, 0);
		showActiveIOPanel.add(activeIOArea, c);
	}

	public void initButtonAction() {
		// 上移
		upButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				upButtonAction();
			}
		});

		// 下移
		downButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downButtonAction();
			}
		});
		// 加入
		addTitleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addTitleButtonAction();
				// 判断活动说明是否存在显示项中a10
				proShowActiveIO();
			}
		});
		// 编辑
		editTitleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editTitleButtonAction();
				// 设置否显示输入输出复选框解释说明
				proactiveIONoteText();
			}
		});
		// 删除
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				delteTitleButtonAction();
				// 判断活动说明是否存在显示项中a10
				proShowActiveIO();
			}
		});
		// 确定
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
		// 默认
		defaultButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				defaultButtonAction();
				// 判断活动说明是否存在显示项中a10
				proShowActiveIO();
			}
		});
		// // logo选择按钮
		// logoButton.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// uploadImage();
		// }
		// });
		//
		// // 查看
		// viewLogoButton.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// viewLogoButton();
		// }
		// });
	}

	// /**
	// * 查看本地logo图标
	// */
	// private void viewLogoButton() {
	// String filePath = null;
	// if (selectedFile != null) {// 存在上传文件，查看应该查看当前上传文件
	// filePath = selectedFile.getPath();
	// } else {
	// filePath = JecnFileUtil.getLogoFilePath();
	// }
	// if (Tool.isNullOrEmtryTrim(filePath)) {
	// // 文件不存在！
	// JecnOptionPane.showMessageDialog(null,
	// JecnResourceUtil.getJecnResourceUtil()
	// .getValue("fileIsNotExist"));
	// return;
	// }
	// JecnFileUtil.openFile(filePath);
	// }

	private void initData() {
		// 判断活动说明是否存在显示项中a10
		proShowActiveIO();
		// 设置否显示输入输出复选框解释说明
		proactiveIONoteText();

		// try {
		// String tmpPath = JecnFileUtil
		// .getSyncPath("/epros/draw/configFile/operationConfig/operationConfig.xml");
		// tmpPath = tmpPath.substring(0, tmpPath.length()
		// - "operationConfig.xml".length());
		// logoIcomPath = tmpPath + "logo.png";
		// } catch (Exception e) {
		// log.error("获取logo图片路径时失败", e);
		// }
		//
		// // 公司名称
		compFiled.setText(JecnSystemStaticData.getCompName());
		//
		// // logo图片
		// String path = JecnFileUtil.getLogoFilePath();
		// if (!Tool.isNullOrEmtryTrim(path)) {
		// ImageIcon icon = getImageIcon(new File(path));
		// if (icon != null) {
		// logoImageLabel.setIcon(icon);
		// // 存在图片可查看
		// viewLogoButton.setEnabled(true);
		// }
		// }
		//
		// // 是否使用logo标识 true:显示；false：隐藏
		// if (JecnSystemStaticData.isShowLogoIcon()) {
		// logoCheckBox.setSelected(true);
		// } else {
		// logoCheckBox.setSelected(false);
		// }

		// 是否显示活动说明中输入输出项 true:显示；false：隐藏
		if (JecnSystemStaticData.isShowActiveIO()) {
			showActiveIOCheckBox.setSelected(true);
		} else {
			showActiveIOCheckBox.setSelected(false);
		}
	}

	// /**
	// * 上传图标
	// *
	// */
	// private void uploadImage() {
	// try {
	// JecnFileChooser fileChooser = new JecnFileChooser();
	// // 移除系统给定的文件过滤器
	// fileChooser.setAcceptAllFileFilterUsed(false);
	// // logo图标选择
	// fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("logoSelect"));
	// String[] str = new String[] { "gif", "jpeg", "png" };
	// // 过滤文件后缀
	// fileChooser.setFileFilter(new JecnFileNameFilter(str));
	// // 设置按钮为打开
	// fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
	// fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	// fileChooser.setEidtTextFiled(false);
	// fileChooser.setApproveButtonText(JecnResourceUtil.getJecnResourceUtil()
	// .getValue("fileChooserOKBtn"));
	// // 显示文件选择器
	// int isOk = fileChooser.showOpenDialog(JecnDrawMainPanel
	// .getMainPanel());
	//
	// // 选择确认（yes、ok）后返回该值。
	// if (isOk == JFileChooser.APPROVE_OPTION) {
	// selectedFile = fileChooser.getSelectedFile();
	// ImageIcon icon = getImageIcon(selectedFile);
	// if (icon != null) {
	// logoImageLabel.setIcon(icon);
	// logoImageLabel.revalidate();
	// this.repaint();
	//
	// // 存在图片可查看
	// viewLogoButton.setEnabled(true);
	// }
	// }
	// } catch (Exception e) {
	// log.error("uploadImage", e);
	// }
	// }

	/**
	 * 
	 * @author yxw 2012-5-15
	 * @description:上移
	 */
	public void upButtonAction() {
		this.titleTable.moveUpRows();
		// 重设序号
		JecnTableModel model = (JecnTableModel) this.titleTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(i + 1 + "", i, 0);
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:下移
	 */
	public void downButtonAction() {
		this.titleTable.moveDownRows();
		// 重设序号
		JecnTableModel model = (JecnTableModel) this.titleTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(i + 1 + "", i, 0);
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:加入标题
	 */
	public void addTitleButtonAction() {
		AddConfigurationDilaog dialog = new AddConfigurationDilaog(operConfigList);
		dialog.setVisible(true);
		if (dialog.isOperation) {
			// 最大行
			int maxRow = titleTable.getRowCount() + 1;
			for (int i = 0; i < operConfigList.size(); i++) {
				DrawOperationDescriptionConfigBean operConfigBean = operConfigList.get(i);
				for (DrawOperationDescriptionConfigBean operBean : dialog.getAddOpera()) {
					if (operConfigBean.getMark().equals(operBean.getMark())) {
						Vector<String> rowData = new Vector<String>();
						rowData.add(String.valueOf(maxRow));
						rowData.add(operBean.getName());
						rowData.add(String.valueOf(i));
						titleTable.addRow(rowData);
						maxRow++;
					}
				}
			}
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:编辑标题
	 */
	public void editTitleButtonAction() {
		int selectRow = titleTable.getSelectedRow();
		if (selectRow == -1) {
			JecnOptionPane
					.showMessageDialog(this, JecnResourceUtil.getJecnResourceUtil().getValue("pleaseSelectALine"));
			return;
		}
		int flagId = Integer.valueOf(titleTable.getModel().getValueAt(selectRow, 2).toString());
		DrawOperationDescriptionConfigBean configurBean = operConfigList.get(flagId);
		EditConfigDialog editConfigDialog = new EditConfigDialog(configurBean);
		editConfigDialog.setVisible(true);
		if (editConfigDialog.isOperation) {
			titleTable.setValueAt(editConfigDialog.getName(), selectRow, 1);
		}
	}

	/**
	 * 删除
	 */
	public void delteTitleButtonAction() {
		int[] selectRows = titleTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane
					.showMessageDialog(this, JecnResourceUtil.getJecnResourceUtil().getValue("pleaseSelectALine"));
			return;
		}
		if (titleTable.getModel().getRowCount() - selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnResourceUtil.getJecnResourceUtil().getValue("thereAreAtLeastA"));
			return;
		}
		if (selectRows.length > 0) {
			for (int i = selectRows.length - 1; i >= 0; i--) {
				int flagId = Integer.valueOf(titleTable.getModel().getValueAt(selectRows[i], 2).toString());
				DrawOperationDescriptionConfigBean configurBean = operConfigList.get(flagId);
				if (configurBean.getIsShow() == 1) {
					configurBean.setIsShow(0);
					((JecnTableModel) titleTable.getModel()).removeRow(selectRows[i]);
				}
			}
		}
		JecnTableModel model = (JecnTableModel) this.titleTable.getModel();
		int rows = model.getRowCount();

		// 重设序号
		for (int i = 0; i < rows; i++) {
			model.setValueAt(i + 1, i, 0);
		}
	}

	/**
	 * 确定保存
	 * 
	 * @author fuzhh Aug 27, 2012
	 */
	public void okButtonAction() {
		Vector dataVector = ((JecnTableModel) this.titleTable.getModel()).getDataVector();
		Vector<Vector<String>> vs = dataVector;
		for (int j = 0; j < vs.size(); j++) {
			Vector<String> v = vs.get(j);
			DrawOperationDescriptionConfigBean configBean = operConfigList.get(Integer.valueOf(v.get(2)));
			configBean.setTurn(j + 1);
		}

		// 小工具保存table中数据
		savaData(operConfigList);

		// 操作说明其他配置
		JecnFileUtil.writeCompName(compFiled.getText(), logoComPanyPanel.getLogoCheckBox().isSelected(),
				showActiveIOCheckBox.isSelected());

		if (logoComPanyPanel.getSelectedFile() != null && logoComPanyPanel.getSelectedFile().exists()
				&& logoComPanyPanel.getSelectedFile().isFile()) {// 选择图片

			// logo图标写入本地
			boolean ret = JecnFileUtil.writeLogoFileToOPLocal(logoComPanyPanel.getSelectedFile(), logoComPanyPanel
					.getLogoIcomPath());
			if (!ret) {
				JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnResourceUtil
						.getJecnResourceUtil().getValue("logoIconSaveFail"));
				log.info("selectedFile=" + logoComPanyPanel.getSelectedFile().getPath() + " 存放路径="
						+ JecnFileUtil.getLogoFilePath());
				return;
			}
		}

		this.setVisible(false);
	}

	public void cancelButtonAction() {
		this.setVisible(false);
	}

	/**
	 * 默认
	 * 
	 * @param name
	 * @return
	 */
	private void defaultButtonAction() {
		// operConfigList
		// 默认数据：1：默认，0：不是默认
		for (DrawOperationDescriptionConfigBean operConfigBean : operConfigList) {
			if (operConfigBean.getIsDefault() == 1) {
				// 设置为显示
				operConfigBean.setIsShow(1);
			} else if (operConfigBean.getIsDefault() == 0) {
				// 设置为不显示
				operConfigBean.setIsShow(0);
			}
			// 设置为默认排序
			operConfigBean.setTurn(operConfigBean.getDefaultSort());
			// 设置为默认名称
			operConfigBean.setName(operConfigBean.getDefaultName());
		}
		// 根据序号排序
		OperationConfigUtil.sortAllFlowElementCloneList(operConfigList);
		titleTable.updateJecnTable();
	}

	public boolean validateNodeRepeat(String name) {
		return false;
	}

	public void savaData(List<DrawOperationDescriptionConfigBean> operConfigList) {
		try {
			// 判断是否为设计器
			if (EprosType.eprosDesigner != JecnSystemStaticData.getEprosType()) {
				OperationConfigUtil.updateOperationConfigXml(operConfigList);
			}
		} catch (Exception e) {
			log.error("", e);
		}

	}

	public Vector<String> getTableTitle() {
		Vector<String> vector = new Vector<String>();
		vector.add(JecnResourceUtil.getJecnResourceUtil().getValue("serialNumber"));
		vector.add(JecnResourceUtil.getJecnResourceUtil().getValue("name"));
		vector.add("flagId");
		return vector;
	}

	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 获取操作说明数据
		try {
			int count = 1;
			for (int i = 0; i < operConfigList.size(); i++) {
				DrawOperationDescriptionConfigBean operCofigBean = operConfigList.get(i);
				Vector<String> row = new Vector<String>();
				// 是否显示 0:不显示，1：显示
				if (operCofigBean.getIsShow() == 1) {
					row.add(String.valueOf(count));
					count++;
					row.add(operCofigBean.getName());
					row.add(String.valueOf(i));
					vs.add(row);
				}
			}

		} catch (Exception e) {
			log.error("ConfigFileInstructionsDialog类getContent()方法读取操作说明配置错误！", e);
		}
		return vs;
	}

	/**
	 * 
	 * 获取本地图片
	 * 
	 * @param file
	 * @return
	 */
	private ImageIcon getImageIcon(File file) {
		ImageIcon icon = null;
		if (file != null && file.exists() && file.isFile()) {
			Image img = Toolkit.getDefaultToolkit().createImage(file.getPath());
			icon = new ImageIcon(img);
		}
		return icon;
	}

	/**
	 * 
	 * 小工具专用方法
	 * 
	 * 活动说明中输入输出显示/隐藏，同步是否显示输入输出复选框显示/隐藏
	 * 
	 */
	private void proShowActiveIO() {
		// 不是小工具，直接返回
		if (EprosType.eprosDraw != JecnSystemStaticData.getEprosType()) {
			return;
		}
		// 判断活动说明是否存在显示项中a10
		for (DrawOperationDescriptionConfigBean configBean : operConfigList) {
			if (!configBean.isActiveInfo()) {// 不是活动说明
				continue;
			}
			if (1 == (configBean.getIsShow())) {// 1显示
				showActiveIOCheckBox.setEnabled(true);
			} else {// 0隐藏
				showActiveIOCheckBox.setEnabled(false);
			}
		}
	}

	/**
	 * 
	 * 设置否显示输入输出复选框解释说明
	 * 
	 */
	private void proactiveIONoteText() {
		// 不是小工具，直接返回
		if (EprosType.eprosDraw != JecnSystemStaticData.getEprosType()) {
			return;
		}
		for (DrawOperationDescriptionConfigBean bean : operConfigList) {
			if (bean.isActiveInfo()) {// 活动说明
				activeIOArea.setText(activeIONoteBefore + bean.getName() + activeIONoteAfter);
			}
		}
	}

	class TitleTable extends JecnTable {

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent());
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 2 };
		}
	}
}
