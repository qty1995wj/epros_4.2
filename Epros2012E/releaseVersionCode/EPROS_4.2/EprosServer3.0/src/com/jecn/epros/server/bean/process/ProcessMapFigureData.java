package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程地图打开元素数据层
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-11-6 时间：下午02:55:52
 */
public class ProcessMapFigureData implements Serializable {
	/** 元素 */
	private JecnFlowStructureImageT jecnFlowStructureImageT;
	/** 流程地图元素附件集合 */
	private List<JecnFigureFileTBean> listJecnFigureFileTBean;

	public List<JecnFigureFileTBean> getListJecnFigureFileTBean() {
		if (listJecnFigureFileTBean == null) {
			listJecnFigureFileTBean = new ArrayList<JecnFigureFileTBean>();
		}
		return listJecnFigureFileTBean;
	}

	public void setListJecnFigureFileTBean(
			List<JecnFigureFileTBean> listJecnFigureFileTBean) {
		this.listJecnFigureFileTBean = listJecnFigureFileTBean;
	}

	public JecnFlowStructureImageT getJecnFlowStructureImageT() {
		return jecnFlowStructureImageT;
	}

	public void setJecnFlowStructureImageT(
			JecnFlowStructureImageT jecnFlowStructureImageT) {
		this.jecnFlowStructureImageT = jecnFlowStructureImageT;
	}
}
