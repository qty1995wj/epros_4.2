package com.jecn.epros.domain.process.relatedFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.util.JecnProperties;

/**
 * 相关流程
 *
 * @author admin
 */
public class RelatedProcess {
    private Long id;
    private String name;
    /**
     * 1是上游流程,2是下游流程,3是过程流程,4是子流程
     */
    private String typeName;
    private String resPeopleName;
    private String resOrgName;
    private String publicTime;
    /**
     * 流程编号
     */
    private String flowCode;

    public LinkResource getLink() {
        return new LinkResource(id, name, ResourceType.PROCESS);
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublicTime() {
        return publicTime;
    }

    public void setPublicTime(String publicTime) {
        this.publicTime = publicTime;
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        // 1是上游流程,2是下游流程,3是过程流程,4是子流程
        if ("1".equals(typeName)) {
            this.typeName = JecnProperties.getValue("upstreamProcess");//上游流程
        } else if ("2".equals(typeName)) {
            this.typeName = JecnProperties.getValue("downstreamProcess");//下游流程
        } else if ("3".equals(typeName)) {
            this.typeName = JecnProperties.getValue("processFlow");//过程流程
        } else if ("4".equals(typeName)) {
            this.typeName = JecnProperties.getValue("subProcess");//子流程
        } else {
            this.typeName = typeName;
        }
    }

    public String getResPeopleName() {
        return resPeopleName;
    }

    public void setResPeopleName(String resPeopleName) {
        this.resPeopleName = resPeopleName;
    }

    public String getResOrgName() {
        return resOrgName;
    }

    public void setResOrgName(String resOrgName) {
        this.resOrgName = resOrgName;
    }

    public String getFlowCode() {
        return flowCode;
    }

    public void setFlowCode(String flowCode) {
        this.flowCode = flowCode;
    }
}
