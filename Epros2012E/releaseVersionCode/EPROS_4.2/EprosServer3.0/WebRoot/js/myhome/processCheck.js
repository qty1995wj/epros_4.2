function processCheckGrid() {
	processCheckGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "processCheckResult.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "flowId",
			mapping : "flowId"
		}, {
			name : "flowName",
			mapping : "flowName"
		}, {
			name : "flowIdInput",
			mapping : "flowIdInput"
		} ])

	});

	processCheckGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			} ]);
	processCheckGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : processCheckGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	processCheckGridPanel = new Ext.grid.GridPanel( {
		id : "processCheckGridPanel_id",
		renderTo : 'processCheckGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 58,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		store : processCheckGridStore,
		colModel : processCheckGridColumnModel,
		bbar : processCheckGridBbar
	})

}

/**
 * 流程检错查询
 */
function processCheckGridSearch() {
	// 搜索类别
	processCheckGridStore.setBaseParam("processCheckType", $(
			"#prcessCheck_searchType").val());
	processCheckGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});
}