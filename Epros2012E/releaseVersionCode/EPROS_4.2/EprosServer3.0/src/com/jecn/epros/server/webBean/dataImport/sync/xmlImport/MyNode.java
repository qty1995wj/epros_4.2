package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

public class MyNode {
	private String deptIdNode;
	private String deptIdAttr;
	private String deptNameNode;
	private String deptNameAttr;
	private String positionIdNode;
	private String positionIdAttr;
	private String positionNameNode;
	private String positionNameAttr;
	private String personIdNode;
	private String personIdAttr;
	private String userNumberNode;
	private String userNumberAttr;
	private String loginNameNode;
	private String loginNameAttr;
	private String passwordNode;
	private String passwordAttr;
	private String trueNameNode;
	private String trueNameAttr;
	private String phoneNode;
	private String phoneAttr;
	private String emailNode;
	private String emailAttr;

	public String getDeptIdAttr() {
		return deptIdAttr;
	}

	public String getDeptIdNode() {
		return deptIdNode;
	}

	public void setDeptIdNode(String deptIdNode) {
		this.deptIdNode = deptIdNode;
	}

	public String getDeptNameNode() {
		return deptNameNode;
	}

	public void setDeptNameNode(String deptNameNode) {
		this.deptNameNode = deptNameNode;
	}

	public String getPositionIdNode() {
		return positionIdNode;
	}

	public void setPositionIdNode(String positionIdNode) {
		this.positionIdNode = positionIdNode;
	}

	public String getPositionNameNode() {
		return positionNameNode;
	}

	public void setPositionNameNode(String positionNameNode) {
		this.positionNameNode = positionNameNode;
	}

	public String getPersonIdNode() {
		return personIdNode;
	}

	public void setPersonIdNode(String personIdNode) {
		this.personIdNode = personIdNode;
	}

	public String getUserNumberNode() {
		return userNumberNode;
	}

	public void setUserNumberNode(String userNumberNode) {
		this.userNumberNode = userNumberNode;
	}

	public String getLoginNameNode() {
		return loginNameNode;
	}

	public void setLoginNameNode(String loginNameNode) {
		this.loginNameNode = loginNameNode;
	}

	public String getPasswordNode() {
		return passwordNode;
	}

	public void setPasswordNode(String passwordNode) {
		this.passwordNode = passwordNode;
	}

	public String getTrueNameNode() {
		return trueNameNode;
	}

	public void setTrueNameNode(String trueNameNode) {
		this.trueNameNode = trueNameNode;
	}

	public String getPhoneNode() {
		return phoneNode;
	}

	public void setPhoneNode(String phoneNode) {
		this.phoneNode = phoneNode;
	}

	public String getEmailNode() {
		return emailNode;
	}

	public void setEmailNode(String emailNode) {
		this.emailNode = emailNode;
	}

	public void setDeptIdAttr(String deptIdAttr) {
		this.deptIdAttr = deptIdAttr;
	}

	public String getDeptNameAttr() {
		return deptNameAttr;
	}

	public void setDeptNameAttr(String deptNameAttr) {
		this.deptNameAttr = deptNameAttr;
	}

	public String getPositionIdAttr() {
		return positionIdAttr;
	}

	public void setPositionIdAttr(String positionIdAttr) {
		this.positionIdAttr = positionIdAttr;
	}

	public String getPositionNameAttr() {
		return positionNameAttr;
	}

	public void setPositionNameAttr(String positionNameAttr) {
		this.positionNameAttr = positionNameAttr;
	}

	public String getPersonIdAttr() {
		return personIdAttr;
	}

	public void setPersonIdAttr(String personIdAttr) {
		this.personIdAttr = personIdAttr;
	}

	public String getUserNumberAttr() {
		return userNumberAttr;
	}

	public void setUserNumberAttr(String userNumberAttr) {
		this.userNumberAttr = userNumberAttr;
	}

	public String getLoginNameAttr() {
		return loginNameAttr;
	}

	public void setLoginNameAttr(String loginNameAttr) {
		this.loginNameAttr = loginNameAttr;
	}

	public String getPasswordAttr() {
		return passwordAttr;
	}

	public void setPasswordAttr(String passwordAttr) {
		this.passwordAttr = passwordAttr;
	}

	public String getTrueNameAttr() {
		return trueNameAttr;
	}

	public void setTrueNameAttr(String trueNameAttr) {
		this.trueNameAttr = trueNameAttr;
	}

	public String getPhoneAttr() {
		return phoneAttr;
	}

	public void setPhoneAttr(String phoneAttr) {
		this.phoneAttr = phoneAttr;
	}

	public String getEmailAttr() {
		return emailAttr;
	}

	public void setEmailAttr(String emailAttr) {
		this.emailAttr = emailAttr;
	}
}
