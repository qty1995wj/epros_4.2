package com.jecn.epros.server.webBean.dataImport.match;

/**
 * 岗位临时表与流程岗位关联关系表
 * 
 * @author zhangjie 20120208
 */
public class PosEprosRelBean {
	/** 主键ID */
	private Long id;

	/** 实际表中的岗位编码或基准岗位编码 */
	private String posNum;

	/** 流程岗位主键ID */
	private Long epsPosId;

	/** 关联类型:0:岗位匹配，1：基准岗位匹配 */
	private int relType;
	/**实际岗位名称*/
	private String hrPosName;
	/**流程岗位名称*/
	private String flowPosName;
	/**基准岗位名称*/
	private String basePosName;

	public String getHrPosName() {
		return hrPosName;
	}

	public void setHrPosName(String hrPosName) {
		this.hrPosName = hrPosName;
	}

	public String getFlowPosName() {
		return flowPosName;
	}

	public void setFlowPosName(String flowPosName) {
		this.flowPosName = flowPosName;
	}

	public String getBasePosName() {
		return basePosName;
	}

	public void setBasePosName(String basePosName) {
		this.basePosName = basePosName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public Long getEpsPosId() {
		return epsPosId;
	}

	public void setEpsPosId(Long epsPosId) {
		this.epsPosId = epsPosId;
	}

	public int getRelType() {
		return relType;
	}

	public void setRelType(int relType) {
		this.relType = relType;
	}

}
