package com.jecn.epros.domain.file;

public class FileDirCommonBean {
    private Long id;
    private String name;
    private String numId;
    private int curlevel;

    public FileDirCommonBean(Long id, String name, String numId, int curlevel) {
        this.id = id;
        this.name = name;
        this.numId = numId;
        this.curlevel = curlevel;
    }

    public FileDirCommonBean(Long id, String name, int curlevel) {
        this.id = id;
        this.name = name;
        this.curlevel = curlevel;
    }

    public int getCurlevel() {
        return curlevel;
    }

    public void setCurlevel(int curlevel) {
        this.curlevel = curlevel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }
}
