package wordxml.element.bean.common;

import java.awt.Color;

import wordxml.constant.Constants.LineType;

/**
 * 
 * 表格边框属性类 适用于 top、 left、bottom、right、insideH、insideV immutable class
 * 
 * @author weidp
 * 
 */
// Immutable class
public final class BorderBean {
	/** 边框线条类型 single、dashed等 **/
	private final LineType lineType; // "single";
	/*** 边框 线条颜色 ****************/
	private final Color color; // "auto";
	/** 边框宽度 */
	private final float borderWidth;
	// /**** 边框线条间隔 **/
	private final int space;

	// /**** 是否存在阴影 *******/
	// private final boolean shadow = false;

	public BorderBean(LineType lineType, float borderWidth, Color color) {
		this.lineType = lineType;
		this.borderWidth = borderWidth;
		this.color = color;
		this.space = 0;
	}

	public BorderBean(LineType lineType, float borderWidth, Color color,
			int space) {
		this.lineType = lineType;
		this.borderWidth = borderWidth;
		this.color = color;
		this.space = space;
	}

	public LineType getType() {
		return lineType;
	}

	public Color getColor() {
		return color;
	}

	public LineType getLineType() {
		return lineType;
	}

	public float getBorderWidth() {
		return borderWidth;
	}

	public int getSpace() {
		return space;
	}

	/**
	 * 创建新的示例 默认样式
	 * 
	 * @author weidp
	 * @date 2014-10-24 下午05:23:36
	 * @param borderWidth
	 * @return
	 */
	public static BorderBean newInstance(float borderWidth) {
		return new BorderBean(LineType.SINGLE, borderWidth, Color.black);
	}
}
