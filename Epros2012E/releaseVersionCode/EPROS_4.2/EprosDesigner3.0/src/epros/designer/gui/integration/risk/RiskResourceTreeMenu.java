package epros.designer.gui.integration.risk;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.Callback;
import epros.designer.gui.system.ResourceSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnDataImportUtil;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/***
 * 风险资源树节点右键菜单 2013-10-30
 * 
 */
public class RiskResourceTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(RiskResourceTreeMenu.class);
	/** 导入数据 */
	private JMenuItem importItem = new JMenuItem(JecnProperties.getValue("importFile"), new ImageIcon(
			"images/menuImage/import.gif"));
	/** 新建风险目录 */
	private JMenuItem newRiskDirItem = new JMenuItem(JecnProperties.getValue("createRiskDir"), new ImageIcon(
			"images/menuImage/createRiskDir.gif"));
	/** 新建风险点 */
	private JMenuItem newRiskItem = new JMenuItem(JecnProperties.getValue("createRiskPoint"), new ImageIcon(
			"images/menuImage/riskPoint.gif"));
	/** 节点排序 */
	private JMenuItem nodeSortItem = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon(
			"images/menuImage/nodeSort.gif"));
	/** 节点移动 */
	private JMenuItem nodeMoveItem = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon(
			"images/menuImage/nodeMove.gif"));
	/** 刷新 */
	private JMenuItem refreshItem = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon(
			"images/menuImage/refresh.gif"));
	/** 重命名 */
	private JMenuItem renameItem = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon(
			"images/menuImage/rename.gif"));
	/** 删除 */
	private JMenuItem deleteItem = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon(
			"images/menuImage/delete.gif"));
	/** 风险点属性 */
	private JMenuItem riskPointPropertyItem = new JMenuItem(JecnProperties.getValue("riskPointProperty"),
			new ImageIcon("images/menuImage/riskPointProperty.gif"));
	private Separator separatorOne = new Separator();
	/** 单选节点 */
	private JecnTreeNode selectNode = null;
	private JecnTree jTree = null;
	/** 选中节点集合 */
	private List<JecnTreeNode> selectNodes = null;

	public RiskResourceTreeMenu(JecnTree jTree, List<JecnTreeNode> selectNodes, int selectMutil) {
		this.jTree = jTree;
		this.selectNodes = selectNodes;
		if (0 == selectMutil) {
			selectNode = selectNodes.get(0);
		}
		if (selectNodes.size() == 1) {
			// 单选设置menu
			this.singleSelect();
		} else {
			// 多选设置menu
			this.mutilSelect();
		}

		this.add(importItem);// 导入
		this.add(newRiskDirItem);// 新建风险目录
		this.add(newRiskItem);// 新建风险点
		this.add(separatorOne);// 分割线
		this.add(renameItem);// 重命名
		this.add(nodeSortItem);// 节点排序
		this.add(nodeMoveItem);// 节点移动
		this.add(refreshItem);// 刷新
		this.add(deleteItem);// 删除
		this.add(riskPointPropertyItem);// 风险点属性

		menuItemActionInit();

	}

	public void menuItemActionInit() {

		importItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				importItemAction();
			}
		});
		newRiskDirItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newRiskDirItemAction();
			}
		});
		newRiskItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newRiskItemAction();
			}
		});
		nodeSortItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeSortItemAction();
			}
		});
		nodeMoveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeMoveItemAction();
			}
		});
		refreshItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshItemAction();
			}
		});
		renameItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renameItemAction();
			}
		});
		deleteItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteItemAction();
			}
		});
		riskPointPropertyItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				riskPointPropertyItemAction();
			}
		});
	}

	protected void importItemAction() {
		JecnDataImportUtil.importDataByType("risk", selectNode, new Callback<Void>() {
			@Override
			public Void call(Object obj) {
				refreshItemAction();
				return null;
			}
		});
	}

	private void newRiskDirItemAction() {
		AddRiskDirDialog addRiskDirDialog = new AddRiskDirDialog(selectNode, jTree);
		addRiskDirDialog.setVisible(true);
	}

	private void newRiskItemAction() {
		AddRiskDialog addRiskDialog = new AddRiskDialog(selectNode, jTree);
		addRiskDialog.setVisible(true);
	}

	private void nodeSortItemAction() {
		ResourceSortDialog rs = new ResourceSortDialog(selectNode, jTree);
		rs.setVisible(true);
	}

	private void nodeMoveItemAction() {
		if (!JecnDesignerCommon.validateRepeatNodesName(selectNodes)) {
			return;
		}
		RiskMoveChooseDialog riskMoveChooseDialog = new RiskMoveChooseDialog(selectNodes, jTree);
		riskMoveChooseDialog.setVisible(true);
	}

	/***
	 * 刷新
	 */
	private void refreshItemAction() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getJecnRiskAction().getChildsRisk(id, JecnConstants.projectId);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("RiskResourceTreeMenu refreshItemAction is error", ex);
		}

	}

	private void renameItemAction() {
		EditNameRiskDirDialog editNameRiskDirDialog = new EditNameRiskDirDialog(selectNode, jTree);
		editNameRiskDirDialog.setVisible(true);
	}

	private void deleteItemAction() {
		if (selectNodes == null) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode node : selectNodes) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		if (listIds.size() == 0) {
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			// int 0 删除异常，1删除成功；2：存在节点处于任务中
			int returnValue = ConnectionPool.getJecnRiskAction().deleteRisk(listIds, JecnConstants.getUserId());
			/** 删除节点 */
			JecnTreeCommon.removeNodes(jTree, selectNodes);
		} catch (Exception e) {
			log.error("RiskResourceTreeMenu deleteItemAction is error", e);
		}

	}

	/***
	 * 风险点属性Item
	 */
	private void riskPointPropertyItemAction() {
		RiskPropertyDialog riskPropertyDialog = new RiskPropertyDialog(selectNode, jTree);
		riskPropertyDialog.setVisible(true);
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		separatorOne.setVisible(false);
		if (JecnTreeCommon.isAuthNodes(selectNodes)) {

			// 删除
			deleteItem.setVisible(true);
			// 移动
			nodeMoveItem.setVisible(true);
		}

	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		int authType;
		boolean isVisioSeparatorOne = false;
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case riskRoot:
			// 刷新
			refreshItem.setVisible(true);

			if (JecnDesignerCommon.isAdmin()) {
				isVisioSeparatorOne = true;
				importItem.setVisible(true);
				newRiskDirItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
			}
			separatorOne.setVisible(isVisioSeparatorOne);
			break;
		// 风险目录
		case riskDir:
			authType = JecnTreeCommon.isAuthNode(selectNode);
			// 刷新
			refreshItem.setVisible(true);
			// 有权限
			if (authType == 1 || authType == 2) {
				isVisioSeparatorOne = true;
				importItem.setVisible(true);
				newRiskDirItem.setVisible(true);
				// 新建风险点
				newRiskItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
			}

			if (authType == 2) {
				// 重命名
				renameItem.setVisible(true);
				// 节点移动
				nodeMoveItem.setVisible(true);
				// 删除
				deleteItem.setVisible(true);
			}
			separatorOne.setVisible(isVisioSeparatorOne);
			break;
		// 风险点
		case riskPoint:
			authType = JecnTreeCommon.isAuthNode(selectNode);
			if (authType == 1 || authType == 2) {
				// 风险点属性
				riskPointPropertyItem.setVisible(true);
			}
			if (authType == 2) {
				// 节点移动
				nodeMoveItem.setVisible(true);
				// 删除
				deleteItem.setVisible(true);
			}
			separatorOne.setVisible(false);
			break;
		default:
			break;
		}
	}

	/**
	 * 初始化
	 */
	private void init() {
		/** 新建风险目录 */
		newRiskDirItem.setVisible(false);
		importItem.setVisible(false);
		/** 新建风险点 */
		newRiskItem.setVisible(false);
		/** 节点排序 */
		nodeSortItem.setVisible(false);
		/** 节点移动 */
		nodeMoveItem.setVisible(false);
		/** 刷新 */
		refreshItem.setVisible(false);
		/** 重命名 */
		renameItem.setVisible(false);
		/** 删除 */
		deleteItem.setVisible(false);
		/** 风险点属性 **/
		riskPointPropertyItem.setVisible(false);
	}
}
