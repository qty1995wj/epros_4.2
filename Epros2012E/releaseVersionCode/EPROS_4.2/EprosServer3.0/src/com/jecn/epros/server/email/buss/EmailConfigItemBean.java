package com.jecn.epros.server.email.buss;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.service.system.IJecnConfigItemService;
import com.jecn.epros.server.util.ApplicationContextUtil;

/**
 * 邮件配置项信息
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-19 时间：上午10:40:10
 */
public class EmailConfigItemBean {
	/** 邮箱记录日志 */
	private static final Log log = LogFactory.getLog(EmailConfigItemBean.class);
	/** 内网邮件服务smtp协议 */
	private String innerSmtp = "";
	/** 内网邮件发件箱服务器地址 */
	String innerUserAddr = "";
	/** 内网 密码 */
	private String innerPWd = "";
	/** 内网邮箱登陆名 */
	private String innerLoginName = "";
	/** 内网端口 */
	private String innerPort = "";
	/** 内网是否使用ssl */
	private String innerUseSSL = "";
	/** 内网是否使用匿名发送 */
	private String innerUseAnonymity = "";

	/** 外网邮件服务smtp协议 */
	private String outerSmtp = "";
	/** 外网邮件发件箱服务器地址 */
	private String outerUserAddr = "";
	/** 外网密码 */
	private String outerPWd = "";
	/** 外网邮箱登陆名 */
	private String outerLoginName = "";
	/** 外网端口 */
	private String outerPort = "";
	/** 外网是否使用ssl */
	private String outerUseSSL = "";
	/** 外网是否使用匿名发送 */
	private String outerUseAnonymity = "";

	/** 邮件地址提示 */
	private String mailAddrTip = "";
	/** 密码提示 */
	private String mailPwdTip = "";
	/** 邮件落款 */
	private String mailEndContent = "";

	/** http请求显示信息str （邮件内容中 http请求的链接地址） */
	private String httpString;
	/** 配置信息表（JECN_SET_TABLE）操作类 */
	private IJecnConfigItemService configItemService = null;
	/** 邮件对象 */
	private static EmailConfigItemBean configItemBean = new EmailConfigItemBean();
	/** 外网是否使用tls */
	private String outerUseTLS = "";
	/** 内网网是否使用tls */
	private String innerUseTLS = "";
	private String outerAlias = "";
	private String innerAlias = "";

	protected EmailConfigItemBean() {

	}

	/**
	 * 获取邮件配置Bean
	 * 
	 * @return
	 */
	public static EmailConfigItemBean getConfigItemBean() {
		return configItemBean;
	}

	/**
	 * 获取邮件配置项信息
	 * 
	 * @throws Exception
	 */
	public void initEmailItem() {
		try {
			for (JecnConfigItemBean itemBean : JecnContants.emailList) {
				if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerServerAddr.toString())) {// 邮箱内网服务器
					innerSmtp = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerMailAddr.toString())) { // 邮箱内网邮箱地址
					innerUserAddr = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerPwd.toString())) { // 邮箱内网密码
					innerPWd = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterServerAdd.toString())) { // 邮箱内网服务器
					outerSmtp = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterMailAddr.toString())) {// 邮箱外网邮箱地址
					outerUserAddr = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterPwd.toString())) {// 邮箱外网密码
					outerPWd = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailAddrTip.toString())) {// 邮件地址提示
					mailAddrTip = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailPwdTip.toString())) {// 邮件密码提示
					mailPwdTip = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailEndContent.toString())) {// 邮件落款
					mailEndContent = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerLoginName.toString())) {// 内网登陆名
					innerLoginName = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterLoginName.toString())) {// 外网登陆名
					outerLoginName = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerPort.toString())) {// 内网端口
					innerPort = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterPort.toString())) {// 外网端口
					outerPort = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerIsUseSSL.toString())) {// 内网是否启用ssl
					innerUseSSL = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterIsUseSSL.toString())) {// 外网是否启用ssl
					outerUseSSL = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerIsUseAnonymity.toString())) {// 内网是否启用匿名
					innerUseAnonymity = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterIsUseAnonymity.toString())) {// 外网是否启用匿名
					outerUseAnonymity = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterIsUseTLS.toString())) {// 外网是否启用匿名
					outerUseTLS = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerIsUseTLS.toString())) {// 外网是否启用匿名
					innerUseTLS = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxInnerAlias.toString())) {// 内网显示的发件人别名
					innerAlias = itemBean.getValue();
				} else if (itemBean.getMark().equals(ConfigItemPartMapMark.mailBoxOuterAlias.toString())) {
					outerAlias = itemBean.getValue();
				}
			}
		} catch (Exception e) {
			log.error("更新数据库Email配置出现异常", e);
		}

	}

	/**
	 * 配置信息表（JECN_SET_TABLE）操作类
	 * 
	 * @return IJecnConfigItemService
	 */
	private IJecnConfigItemService getConfigItemService() {
		// 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
		if (this.configItemService == null) {
			this.configItemService = (IJecnConfigItemService) ApplicationContextUtil.getContext().getBean(
					"configServiceImpl");
		}
		return this.configItemService;
	}

	public String getInnerSmtp() {
		return innerSmtp;
	}

	public void setInnerSmtp(String innerSmtp) {
		this.innerSmtp = innerSmtp;
	}

	public String getInnerUserAddr() {
		return innerUserAddr;
	}

	public void setInnerUserAddr(String innerUserAddr) {
		this.innerUserAddr = innerUserAddr;
	}

	public String getInnerPWd() {
		return innerPWd;
	}

	public void setInnerPWd(String innerPWd) {
		this.innerPWd = innerPWd;
	}

	public String getOuterSmtp() {
		return outerSmtp;
	}

	public void setOuterSmtp(String outerSmtp) {
		this.outerSmtp = outerSmtp;
	}

	public String getOuterUserAddr() {
		return outerUserAddr;
	}

	public void setOuterUserAddr(String outerUserAddr) {
		this.outerUserAddr = outerUserAddr;
	}

	public String getOuterPWd() {
		return outerPWd;
	}

	public void setOuterPWd(String outerPWd) {
		this.outerPWd = outerPWd;
	}

	public String getMailAddrTip() {
		return mailAddrTip;
	}

	public void setMailAddrTip(String mailAddrTip) {
		this.mailAddrTip = mailAddrTip;
	}

	public String getMailPwdTip() {
		return mailPwdTip;
	}

	public void setMailPwdTip(String mailPwdTip) {
		this.mailPwdTip = mailPwdTip;
	}

	public String getMailEndContent() {
		return mailEndContent;
	}

	public void setMailEndContent(String mailEndContent) {
		this.mailEndContent = mailEndContent;
	}

	/**
	 * 获取http请求显示信息str （邮件内容中 http请求的链接地址）
	 * 
	 * @return
	 */
	public String getHttpString() {
		// http请求显示信息str
		JecnConfigItemBean configItemBean = getConfigItemService().selectConfigItemBean(5,
				ConfigItemPartMapMark.basicEprosURL.toString());
		// http请求显示信息str
		httpString = configItemBean.getValue();
		return httpString;
	}

	public void setHttpString(String httpString) {
		this.httpString = httpString;
	}

	public void setConfigItemService(IJecnConfigItemService configItemService) {
		this.configItemService = configItemService;
	}

	public String getInnerLoginName() {
		return innerLoginName;
	}

	public void setInnerLoginName(String innerLoginName) {
		this.innerLoginName = innerLoginName;
	}

	public String getInnerPort() {
		return innerPort;
	}

	public void setInnerPort(String innerPort) {
		this.innerPort = innerPort;
	}

	public String getInnerUseSSL() {
		return innerUseSSL;
	}

	public void setInnerUseSSL(String innerUseSSL) {
		this.innerUseSSL = innerUseSSL;
	}

	public String getInnerUseAnonymity() {
		return innerUseAnonymity;
	}

	public void setInnerUseAnonymity(String innerUseAnonymity) {
		this.innerUseAnonymity = innerUseAnonymity;
	}

	public String getOuterLoginName() {
		return outerLoginName;
	}

	public void setOuterLoginName(String outerLoginName) {
		this.outerLoginName = outerLoginName;
	}

	public String getOuterPort() {
		return outerPort;
	}

	public void setOuterPort(String outerPort) {
		this.outerPort = outerPort;
	}

	public String getOuterUseSSL() {
		return outerUseSSL;
	}

	public void setOuterUseSSL(String outerUseSSL) {
		this.outerUseSSL = outerUseSSL;
	}

	public String getOuterUseAnonymity() {
		return outerUseAnonymity;
	}

	public void setOuterUseAnonymity(String outerUseAnonymity) {
		this.outerUseAnonymity = outerUseAnonymity;
	}

	public String getOuterUseTLS() {
		return outerUseTLS;
	}

	public void setOuterUseTLS(String outerUseTLS) {
		this.outerUseTLS = outerUseTLS;
	}

	public String getInnerUseTLS() {
		return innerUseTLS;
	}

	public void setInnerUseTLS(String innerUseTLS) {
		this.innerUseTLS = innerUseTLS;
	}

	public String getOuterAlias() {
		return outerAlias;
	}

	public void setOuterAlias(String outerAlias) {
		this.outerAlias = outerAlias;
	}

	public String getInnerAlias() {
		return innerAlias;
	}

	public void setInnerAlias(String innerAlias) {
		this.innerAlias = innerAlias;
	}

}
