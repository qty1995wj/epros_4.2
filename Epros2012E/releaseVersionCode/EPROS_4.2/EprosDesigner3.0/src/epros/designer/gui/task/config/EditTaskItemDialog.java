package epros.designer.gui.task.config;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.popedom.person.PersonChooseDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 点击加入按钮弹出的确认对话框
 * 
 * @author ZHOUXY
 * 
 */
public class EditTaskItemDialog extends JecnDialog {
	/** 配置界面 */
	private TaskConfigDialog dialog = null;
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 审批阶段名称 **/
	private JLabel approveStateLabel = null;
	/** 审批阶段名称 **/
	private JTextField approveStateField = null;

	/** 英文审批阶段名称 **/
	private JLabel approveStateLabelEn = null;
	/** 英文审批阶段名称 **/
	private JTextField approveStateFieldEn = null;

	/** 审批人label **/
	private JLabel approvePeopleLabel = null;
	/** 审批人field **/
	private JTextField approvePeopleField = null;
	/** 审批人按钮 **/
	private JButton approvePeopleButton = null;

	/** 必填 **/
	private JRadioButton mustButton;
	/** 非必填 **/
	private JRadioButton notMustButton;

	/** 按钮面板 **/
	private JPanel buttonPanel = null;

	private JButton okBut;
	private JButton cancelBut;
	/** 中间面板 **/
	private JPanel centerPanel;
	private JLabel tipLabel;
	private TaskConfigItem configItem;
	private List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();

	public EditTaskItemDialog(TaskConfigDialog dialog, TaskConfigItem configItem) {
		super(dialog);
		this.dialog = dialog;
		this.configItem = configItem;

		initComponents();
		initLayout();
		initAcion();
		initData();

	}

	private void initAcion() {

		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (check()) {
					List<JecnUser> users = configItem.getUsers();
					users.clear();
					if (treeBeans.size() > 0) {
						for (JecnTreeBean treeBean : treeBeans) {
							JecnUser user = new JecnUser();
							user.setTrueName(treeBean.getName());
							user.setPeopleId(treeBean.getId());
							users.add(user);
						}
					}
					// 选中必填
					if (mustButton.isSelected()) {
						configItem.setIsEmpty(1);
					} else if (notMustButton.isSelected()) {
						configItem.setIsEmpty(0);
					}
					configItem.setName(approveStateField.getText().trim());
					configItem.setEnName(approveStateFieldEn.getText().trim());
					dialog.getTaskEditPanel().getTaskTablePanel().initShowData();
					EditTaskItemDialog.this.dispose();
				}
			}
		});

		cancelBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EditTaskItemDialog.this.dispose();
			}
		});

		approvePeopleButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				selectApprovePeople();

			}
		});

	}

	private boolean check() {
		String text = approveStateField.getText().trim();

		String enText = approveStateFieldEn.getText().trim();

		// 提示信息
		String info = null;
		if (checkRepeat(text)) {
			info = JecnProperties.getValue("approvalNameHaved");
		}
		if (checkRepeatEn(enText)) {
			info = JecnProperties.getValue("approvalNameHaved");
		}
		if (DrawCommon.isNullOrEmtryTrim(text)) {// 空
			// 名称不能为空
			info = JecnUserCheckInfoData.getNameNotNull();
		} else if (JecnUserCheckUtil.getTextLength(text) > 60 || JecnUserCheckUtil.getTextLength(enText) > 30) {// 长度
			// 名称不能超过20个字符或10个汉字
			info = JecnUserCheckInfoData.getNameLengthInfo().replaceAll("122", "30").replaceAll("61", "10");
		}
		if (info != null) {
			tipLabel.setText(info);
			return false;
		} else {
			tipLabel.setText("");
			return true;
		}
	}

	private boolean checkRepeat(String name) {
		List<TaskConfigItem> configs = dialog.getConfigs();
		for (TaskConfigItem config : configs) {
			if (config == configItem) {
				continue;
			}
			if (config.getName().trim().equals(name.trim())) {
				return true;
			}
		}
		return false;
	}

	private boolean checkRepeatEn(String name) {
		List<TaskConfigItem> configs = dialog.getConfigs();
		for (TaskConfigItem config : configs) {
			if (config == configItem) {
				continue;
			}
			if (config.getEnName() == null) {
				return false;
			}
			if (config.getEnName().trim().equals(name.trim())) {
				return true;
			}
		}
		return false;
	}

	protected void selectApprovePeople() {

		PersonChooseDialog personChooseDialog = new PersonChooseDialog(treeBeans, dialog);
		// 是否支持多选 true 多选
		personChooseDialog.setSelectMutil(isMultipartSelect());
		personChooseDialog.setVisible(true);
		if (personChooseDialog.isOperation()) {// 点击确定

			if (treeBeans != null && treeBeans.size() > 0) {
				// 选择必填
				mustButton.setSelected(true);
			}
			String names = "";
			for (JecnTreeBean bean : treeBeans) {
				names += getSplitChar() + bean.getName();
			}
			if (names.startsWith(getSplitChar())) {
				names = names.replaceFirst(getSplitChar(), "");
			}
			approvePeopleField.setText(names);
		}

	}

	/**
	 * 只有会审\自定义阶段可以多选人
	 * 
	 * @return
	 */
	private boolean isMultipartSelect() {
		if (configItem.getMark().equals(ConfigItemPartMapMark.taskAppReviewer.toString())
				|| configItem.getMark().equals(ConfigItemPartMapMark.taskCustomApproval1.toString())
				|| configItem.getMark().equals(ConfigItemPartMapMark.taskCustomApproval2.toString())
				|| configItem.getMark().equals(ConfigItemPartMapMark.taskCustomApproval3.toString())
				|| configItem.getMark().equals(ConfigItemPartMapMark.taskCustomApproval4.toString())) {
			return true;
		}
		return false;
	}

	private void initTreeBeans() {
		treeBeans.clear();
		List<JecnUser> users = configItem.getUsers();
		for (JecnUser user : users) {
			JecnTreeBean treeBean = new JecnTreeBean();
			treeBean.setName(user.getTrueName());
			treeBean.setId(user.getPeopleId());
			treeBeans.add(treeBean);
		}
	}

	private void initData() {
		initTreeBeans();
		approveStateField.setText(configItem.getName());
		approveStateFieldEn.setText(configItem.getEnName());
		approvePeopleField.setText(concatMultipartName(configItem.getUsers()));

		if (configItem.getIsEmpty() == 1) {
			mustButton.setSelected(true);
		} else if (configItem.getIsEmpty() == 0) {
			notMustButton.setSelected(true);
		}
	}

	private String concatMultipartName(List<JecnUser> users) {
		if (users == null || users.size() == 0) {
			return "";
		}
		String names = "";
		for (JecnUser user : users) {
			names += getSplitChar() + user.getTrueName();
		}
		if (names.startsWith(getSplitChar())) {
			names = names.replaceFirst(getSplitChar(), "");
		}
		return names;
	}

	private String getSplitChar() {
		return ",";
	}

	private void initLayout() {
		Insets insets = new Insets(3, 3, 1, 3);
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		mainPanel.add(centerPanel, c);

		centerPanel.setLayout(new GridBagLayout());
		// 任务名称label
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(approveStateLabel, c);
		// 任务名称field
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(approveStateField, c);

		// 英文任务名称label
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(approveStateLabelEn, c);
		// 英文任务名称field
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(approveStateFieldEn, c);

		// 审批人label
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(approvePeopleLabel, c);
		// 审批人field
		c = new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(approvePeopleField, c);
		// 审批人按钮
		c = new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(approvePeopleButton, c);
		// LABEL
		c = new GridBagConstraints(0, 3, 4, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		JecnPanel btnPanel = new JecnPanel();
		btnPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.add(btnPanel, c);

		btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		// 必填
		btnPanel.add(mustButton, c);
		// 非必填
		btnPanel.add(notMustButton, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mustButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		notMustButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(tipLabel);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		// 大小
		Dimension size = new Dimension(390, 220);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setMaximumSize(size);
		this.setResizable(false);
		// 模态
		this.setModal(true);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLocationRelativeTo(dialog);
		this.setTitle(JecnProperties.getValue("taskApprovalEdit"));
		this.getContentPane().add(mainPanel);

	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	protected void initComponents() {
		// 主面板
		mainPanel = new JPanel();
		centerPanel = new JPanel();
		// 确认取消按钮面板
		buttonPanel = new JPanel();
		approvePeopleLabel = new JLabel(JecnProperties.getValue("defaultApprover"));
		approvePeopleButton = new JButton(JecnProperties.getValue("selectBtn"));
		approvePeopleField = new JTextField();
		approvePeopleField.setEditable(false);
		approveStateField = new JTextField();
		approveStateFieldEn = new JTextField();
		approveStateLabel = new JLabel(JecnProperties.getValue("approvalStage") + "("
				+ JecnProperties.getValue("chineseName") + ")");
		approveStateLabelEn = new JLabel(JecnProperties.getValue("approvalStage") + "("
				+ JecnProperties.getValue("enName") + ")");
		mustButton = new JRadioButton(JecnProperties.getValue("task_notEmptyBtn"));
		notMustButton = new JRadioButton(JecnProperties.getValue("task_emptyBtn"));
		ButtonGroup group = new ButtonGroup();
		group.add(mustButton);
		group.add(notMustButton);
		tipLabel = new JLabel("");
		tipLabel.setForeground(Color.RED);
		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	}

}
