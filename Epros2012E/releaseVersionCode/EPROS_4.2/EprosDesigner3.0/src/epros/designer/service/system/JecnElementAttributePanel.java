package epros.designer.service.system;

import javax.swing.JComponent;
import javax.swing.JPanel;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 元素属性
 * 
 * @author admin
 * 
 */
public class JecnElementAttributePanel extends JPanel {

	private JecnBaseFigurePanel figurePanel;

	private JecnFigureData figureData;

	public JecnElementAttributePanel() {
	}

	public JecnElementAttributePanel(JecnFigureData figureData, JecnBaseFigurePanel figurePanel) {
		this.figureData = figureData;
		this.figurePanel = figurePanel;
	}

	protected boolean saveButAction() {
		return false;
	}

	protected void initElementPanelData(JecnBaseFigurePanel figurePanel) {

	}

	protected JComponent getElementAttrubutePanel() {
		return null;
	}

	public JecnBaseFigurePanel getFigurePanel() {
		return figurePanel;
	}

	public void setFigurePanel(JecnBaseFigurePanel figurePanel) {
		this.figurePanel = figurePanel;
	}

	public JecnFigureData getFigureData() {
		return figureData;
	}

	public void setFigureData(JecnFigureData figureData) {
		this.figureData = figureData;
	}

}
