package com.jecn.epros.service.web.bean.iflytekTask;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.task.MyTaskBean;

import java.sql.Date;

/**
 * Created by Administrator on 2018/11/27.
 */
public class IflytekTaskDataBean {
    //流程任务id
    private String flowId;
    //标题
    private String requestName;
    //流程任务名称
    private String workflowName;
    //步骤名称（节点名称）   阶段名称
    private String nodeName;
    //PC地址
    private String pcUrl;
    //APP地址
    private String appUrl;
    //创建人（对应域账号）
    private String creator;
    //创建流程时间(yyyy-MM-dd HH:ss:mm)
    private String createDateTime;
    //接收人（域账号）
    private String receiver;
    //类型编码(传003)
    private String msgTypeCode;
    //是否可以批量通过(0:否1:是)
    private String batchStatus;
    //流程任务分类
    private String workflowtype;
    //流程任务备注信息
    private String requestnamenew;
    //是否抄送单据(0:否1:是)
    private String copy;
    //流程二级分类
    private String twoLevelClass;
    //待办处理状态(100:待办101:已办)
    private String handleStatus;

    //任务数据
    @JsonIgnore
    private MyTaskBean myTaskBean;

    public MyTaskBean getMyTaskBean() {
        return myTaskBean;
    }

    public void setMyTaskBean(MyTaskBean myTaskBean) {
        this.myTaskBean = myTaskBean;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getPcUrl() {
        return pcUrl;
    }

    public void setPcUrl(String pcUrl) {
        this.pcUrl = pcUrl;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMsgTypeCode() {
        return msgTypeCode;
    }

    public void setMsgTypeCode(String msgTypeCode) {
        this.msgTypeCode = msgTypeCode;
    }

    public String getBatchStatus() {
        return batchStatus;
    }

    public void setBatchStatus(String batchStatus) {
        this.batchStatus = batchStatus;
    }

    public String getWorkflowtype() {
        return workflowtype;
    }

    public void setWorkflowtype(String workflowtype) {
        this.workflowtype = workflowtype;
    }

    public String getRequestnamenew() {
        return requestnamenew;
    }

    public void setRequestnamenew(String requestnamenew) {
        this.requestnamenew = requestnamenew;
    }

    public String getCopy() {
        return copy;
    }

    public void setCopy(String copy) {
        this.copy = copy;
    }

    public String getTwoLevelClass() {
        return twoLevelClass;
    }

    public void setTwoLevelClass(String twoLevelClass) {
        this.twoLevelClass = twoLevelClass;
    }

    public String getHandleStatus() {
        return handleStatus;
    }

    public void setHandleStatus(String handleStatus) {
        this.handleStatus = handleStatus;
    }
}
