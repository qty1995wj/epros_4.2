package epros.designer.gui.process.mode;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.event.JecnFileActionProcess;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 新建流程图模板
 * 
 * @author 2012-06-27
 * 
 */
public class AddFlowModelDialog extends JecnDialog implements ActionListener, CaretListener, ItemListener {
	private static Logger log = Logger.getLogger(AddFlowModelDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;
	/** 内容面板 */
	private JecnPanel mainPanel = null;

	/** 流程图名称标签 */
	private JLabel nameLabel = null;
	/** 流程图名称 */
	private JTextField nameTextField = null;
	/** 提示信息框 */
	private JecnUserInfoTextArea infoTextArea = null;

	/** 角色、活动 总面板 */
	private JecnPanel roleRoundRectPanel = null;
	/** 预估角色 */
	private JLabel roleLabel = null;
	private JSpinner roleSpinner = null;
	/** 预估活动 */
	private JLabel roundRectLabel = null;
	private JSpinner roundRectSpinner = null;

	/** 横向纵向总面板 */
	private JecnPanel vhPanel = null;
	/** 横向面板 */
	private JPanel hPanel = null;
	/** 纵向面板 */
	private JPanel vPanel = null;
	/** 横向图片标签 */
	private JLabel hLabel = null;
	/** 横向单选框 */
	private JRadioButton hRadioBtn = null;
	/** 纵向图片标签 */
	private JLabel vLabel = null;
	/** 纵向单选框 */
	private JRadioButton vRadioBtn = null;
	/** 确认取消按钮面板 */
	private JecnOKCancelPanel okCancelPanel = null;
	/** 点击确认按钮标识 */
	private boolean okFlag = false;
	/** 横纵向标识 0横向，1纵向 */
	private int isXorY = 0;

	public AddFlowModelDialog(JecnTreeNode pNode, JTree jTree) {
		initComponents();
		initLayout();
		this.pNode = pNode;
		this.jTree = jTree;
	}

	private void initComponents() {
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("newFlowMap"));
		// 内容面板
		mainPanel = new JecnPanel();
		// 名称标签
		nameLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("partMapName"));
		// 流程图名称
		nameTextField = new JTextField();
		// 提示信息框
		infoTextArea = new JecnUserInfoTextArea();

		// 角色、活动 总面板
		roleRoundRectPanel = new JecnPanel();
		// 横向纵向总面板
		vhPanel = new JecnPanel();
		// 横向面板
		hPanel = new JPanel();
		// 纵向面板
		vPanel = new JPanel();

		// 预估角色
		roleLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("roleLabel"));
		roleSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));
		// 预估活动
		roundRectLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("roundRectLabel"));
		roundRectSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));

		// 横向图片标签
		hLabel = new JLabel(JecnFileUtil.getCreatePartMapIcon("h.gif"));
		// 横向选中框
		hRadioBtn = new JRadioButton(JecnResourceUtil.getJecnResourceUtil().getValue("hRadioButton"));
		// 纵向图片标签
		vLabel = new JLabel(JecnFileUtil.getCreatePartMapIcon("v.gif"));
		// 纵向选中框
		vRadioBtn = new JRadioButton(JecnResourceUtil.getJecnResourceUtil().getValue("vRadioButton"));

		// 确认取消按钮面板
		okCancelPanel = new JecnOKCancelPanel();

		Dimension size = new Dimension(375, 315);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setSize(size);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		// 布局管理器
		hPanel.setLayout(new BorderLayout());
		vPanel.setLayout(new BorderLayout());

		// 标题
		// 角色、活动 总面板
		roleRoundRectPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				JecnResourceUtil.getJecnResourceUtil().getValue("roleRoundRectBorderTitle")));
		// 横向纵向总面板
		vhPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("vhBorderTitle")));

		// 大小
		// 流程图名称
		Dimension contentSize = new Dimension(175, 20);
		nameTextField.setPreferredSize(contentSize);
		nameTextField.setMinimumSize(contentSize);

		Dimension spinnerSize = new Dimension(77, 20);
		// 角色
		roleSpinner.setPreferredSize(spinnerSize);
		// 活动
		roundRectSpinner.setPreferredSize(spinnerSize);

		// 背景颜色和前景颜色
		// 角色、活动 总面板
		roleRoundRectPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 横向纵向总面板
		vhPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 横向单选框
		hRadioBtn.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 纵向单选框
		vRadioBtn.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		ButtonGroup group = new ButtonGroup();
		group.add(hRadioBtn);
		group.add(vRadioBtn);
		hRadioBtn.setSelected(true);
		// 事件
		nameTextField.addCaretListener(this);
		okCancelPanel.getOkBtn().addActionListener(this);
		okCancelPanel.getCancelBtn().addActionListener(this);
		hRadioBtn.addItemListener(this);
		vRadioBtn.addItemListener(this);
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				okFlag = false;
				return true;
			}
		});
	}

	/**
	 * 
	 * 
	 * 
	 */
	private void initLayout() {
		initOneLayout();
		// 布局角色、活动 总面板
		initRoleRectPanelLayout();
		// 布局横向、纵向总面板
		vhPanelLayout();
	}

	private void initOneLayout() {
		this.getContentPane().add(mainPanel);

		// 内容面板new Insets(20, 25, 0, 30)
		Insets labelInsets = new Insets(20, 25, 0, 0);
		Insets nameInsets = new Insets(20, 3, 0, 30);
		Insets infoTextAreaInsets = new Insets(0, 30, 0, 10);
		Insets okCancelPanelInsets = new Insets(5, 0, 0, 30);

		Insets roleRoundRectInsets = new Insets(20, 25, 0, 30);
		Insets vhInsets = new Insets(0, 25, 0, 30);

		// **********第一行**********//
		// 流程图名称标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, labelInsets, 0, 0);
		mainPanel.add(nameLabel, c);
		// 流程图名称
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				nameInsets, 0, 0);
		mainPanel.add(nameTextField, c);
		// **********第一行**********//

		// 提示信息框
		c = new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				infoTextAreaInsets, 0, 0);
		mainPanel.add(infoTextArea, c);

		// 角色、活动 总面板
		c = new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				roleRoundRectInsets, 0, 0);
		mainPanel.add(roleRoundRectPanel, c);
		// 横向纵向总面板
		c = new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				vhInsets, 0, 0);
		mainPanel.add(vhPanel, c);

		// 空闲区域
		c = new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				infoTextAreaInsets, 0, 0);
		mainPanel.add(new JLabel(), c);

		// 确认取消按钮面板
		c = new GridBagConstraints(0, 5, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				okCancelPanelInsets, 0, 0);
		mainPanel.add(okCancelPanel, c);
	}

	/**
	 * 
	 * 布局角色、活动总面板
	 * 
	 */
	private void initRoleRectPanelLayout() {
		// 角色、活动 总面板
		// 最内部面板的第一个组件的间距
		Insets insetsFirst = new Insets(9, 3, 9, 3);
		// 标签间距
		Insets insetsLabel = new Insets(9, 11, 9, 3);
		// 内容间距
		Insets insetsContent = new Insets(9, 0, 9, 7);

		// 左面元素:预估角色
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsFirst, 0, 0);
		roleRoundRectPanel.add(roleLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		roleRoundRectPanel.add(roleSpinner, c);

		// 空闲区域
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsLabel, 0, 0);
		roleRoundRectPanel.add(new JLabel(), c);

		// 右面元素:预估活动
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		roleRoundRectPanel.add(roundRectLabel, c);
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		roleRoundRectPanel.add(roundRectSpinner, c);
	}

	/**
	 * 
	 * 布局横向、纵向总面板
	 * 
	 */
	private void vhPanelLayout() {
		// 横向纵向总面板
		Insets hInsets = new Insets(9, 85, 0, 40);
		Insets vInsets = new Insets(9, 0, 0, 72);
		// 横向面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, hInsets, 0, 0);
		vhPanel.add(hPanel, c);

		// 纵向面板
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				vInsets, 0, 0);
		vhPanel.add(vPanel, c);

		// 横向图片标签
		hPanel.add(this.hLabel, BorderLayout.NORTH);
		// 横向单选框
		hPanel.add(this.hRadioBtn, BorderLayout.CENTER);

		// 纵向图片标签
		vPanel.add(this.vLabel, BorderLayout.NORTH);
		// 纵向单选框
		vPanel.add(this.vRadioBtn, BorderLayout.CENTER);

	}

	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	/**
	 * 
	 * 横纵向单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		itemStateChangedProcess(e);
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	private void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == nameTextField) {// 流程图名称
			checkName();
		}
	}

	private void itemStateChangedProcess(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {// 选中
			if (e.getSource() == hRadioBtn) { // 横向单选框
				isXorY = 0;
			} else if (e.getSource() == vRadioBtn) {// 纵向单选框
				isXorY = 1;
			}
		}
	}

	/**
	 * 
	 * 在名称输入框中输入字符时，验证内容是否正确
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	private boolean checkName() {
		String text = nameTextField.getText().trim();
		// 验证名称是否合法
		String checkString = JecnUserCheckUtil.checkName(text);
		return infoTextArea.checkInfo(checkString);
	}

	public boolean isOkFlag() {
		return okFlag;
	}

	public void actionPerformedProcess(ActionEvent e) {
		if (okCancelPanel.getOkBtn() == e.getSource()) {// 确认按钮
			// 校验数据
			boolean ret = checkName();
			try {
				// 判断是否重名
				if (JecnTreeCommon.validateRepeatNameAdd(pNode, nameTextField.getText().trim(),
						TreeNodeType.processMode)) {
					infoTextArea.checkInfo(JecnProperties.getValue("nameHaved"));
					return;
				}
			} catch (Exception e1) {
				log.error("AddFlowModelDialog actionPerformedProcess is error", e1);
			}
			if (ret) {// 赋值给数据对象
				okFlag = true;
				saveData();
			} else {
				okFlag = true;
				return;
			}
		} else if (okCancelPanel.getCancelBtn() == e.getSource()) {// 取消按钮
			okFlag = false;
			this.dispose();
		}
	}

	/**
	 * 确定按钮
	 * 
	 */
	public void saveData() {
		// 名称
		String flowName = nameTextField.getText().trim();

		// 角色个数
		int roles = Integer.valueOf(roleSpinner.getValue().toString()).intValue();
		// 活动个数
		int actives = Integer.valueOf(roundRectSpinner.getValue().toString()).intValue();
		// 点击确定验证流程面板活动和角色个数是否超出范围
		if (JecnValidateCommon.checkRoleAndActiveLimit(roles, actives)) {
			return;
		}
		// 向流程模板Bean中添加数据JecnFlowStructureMode
		JecnFlowStructureT jecnFlowStructureT = new JecnFlowStructureT();
		// 流程地图模板名称
		jecnFlowStructureT.setFlowName(flowName);
		// 类别：0：流程地图模板 1：流程模板
		jecnFlowStructureT.setIsFlow(1L);
		jecnFlowStructureT.setDataType(1);
		// 是否删除
		jecnFlowStructureT.setDelState(0L);
		// 父节点
		jecnFlowStructureT.setPerFlowId(pNode.getJecnTreeBean().getId());
		// 排序ID
		jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode)));
		jecnFlowStructureT.setOccupier(0l);
		Date createDatime = new Date();
		jecnFlowStructureT.setCreateDate(createDatime);
		jecnFlowStructureT.setPeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnFlowStructureT.setUpdateDate(createDatime);
		jecnFlowStructureT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 排序ID
		jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode)));
		// 执行数据库表的添加保存
		try {
			Long id = ConnectionPool.getProcessModeAction().addFlowModel(jecnFlowStructureT, isXorY);
			// 向树节点添加流程模板节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(flowName);
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.processMode);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			// 生成一个面板
			JecnFlowMapData totalMapData = new JecnFlowMapData(MapType.partMap);
			totalMapData.setName(flowName);
			totalMapData.setFlowId(id);
			boolean isHFlag = isXorY == 0 ? true : false;
			totalMapData.setRoleCount(roles);
			totalMapData.setRoundRectCount(actives);
			totalMapData.getDesignerData().setModeType(ModeType.partMapMode);
			totalMapData.setHFlag(isHFlag);
			totalMapData.setSaveTrue();
			JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);
			JecnFileActionProcess.getCreateFlow(totalMapData);
			// 重新设置图标
			JecnDrawMainPanel.getMainPanel().getWorkflow().getTabbedTitlePanel().setTabIcon(
					(Icon) JecnTreeRenderer.class.getField(jecnTreeBean.getTreeNodeType().toString()).get(null));
			// 角色移动
			if (JecnSystemStaticData.isShowRoleMove()) {
				getWorkflow().removeJecnRoleMobile();
				// 添加角色移动
				getWorkflow().createAddJecnRoleMobile();
			} else {
				// 移除角色移动
				getWorkflow().removeJecnRoleMobile();
			}
			this.dispose();
		} catch (Exception e) {
			log.error("AddFlowModelDialog saveData is error", e);
		}
	}

	/**
	 * 获取面板
	 * 
	 * @return 画图面板
	 */
	public static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}
}
