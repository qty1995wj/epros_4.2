package epros.designer.gui.autoNum;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 流程、制度、文件自动编号管理页面
 * 
 * @author ZXH
 * 
 */
public class AutoNumManageDialog extends JecnDialog {
	protected static Logger log = Logger.getLogger(AutoNumManageDialog.class);
	/** 主面板 */
	protected JPanel mainPanel = null;

	/** 自动编号tree面板 */
	protected AutoNumJecnTree jecnTree = null;

	protected JecnPanel buttonPanel = null;

	/** 公司代码 */
	protected AreaScrollPanel companyCodeScrollPanel = null;
	/** 生产线或小部门 */
	protected AreaScrollPanel productionLineScrollPanel = null;
	/** 文件类型 */
	protected AreaScrollPanel fileTypeScrollPanel = null;
	/** 小类别 */
	protected AreaScrollPanel smallClassScrollPanel = null;

	/** 树面板 */
	protected JScrollPane treePanel = null;

	protected JButton okButton = null;
	protected JButton calcelButton = null;

	protected JecnConfigItemBean companyItem;
	protected JecnConfigItemBean productionLineItem;
	protected JecnConfigItemBean fileTypeItem;
	protected JecnConfigItemBean smallClassItem;

	/** 0 流程，1 文件 ， 2 制度 */
	private int treeType;

	public AutoNumManageDialog(int treeType) {
		this.treeType = treeType;
		initData();
		initCompotents();
		initLayout();
		initCompotentsValue();
	}

	protected void initData() {
		companyItem = JecnConfigTool.getAutoComptyCode(treeType);
		productionLineItem = JecnConfigTool.getAutoProductionLine(treeType);
		fileTypeItem = JecnConfigTool.getAutoFileType(treeType);
		smallClassItem = JecnConfigTool.getAutoSmallClass(treeType);
		if (companyItem == null || productionLineItem == null || fileTypeItem == null || smallClassItem == null) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("accessServerError"));
			throw new IllegalArgumentException("AutoNumManageDialog initData is error!");
		}
	}

	protected void initCompotentsValue() {
		companyCodeScrollPanel.getTextArea().setText(companyItem.getValue());
		productionLineScrollPanel.getTextArea().setText(productionLineItem.getValue());
		fileTypeScrollPanel.getTextArea().setText(fileTypeItem.getValue());
		smallClassScrollPanel.getTextArea().setText(smallClassItem.getValue());
	}

	protected void initCompotents() {
		mainPanel = new JPanel();
		// 设置Dialog大小
		this.setSize(755, 430);
		this.setResizable(false);
		this.setTitle(JecnProperties.getValue("numberManagement"));
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTree = getTree();

		buttonPanel = new JecnPanel();
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		companyCodeScrollPanel = new AreaScrollPanel();
		productionLineScrollPanel = new AreaScrollPanel();
		fileTypeScrollPanel = new AreaScrollPanel();
		smallClassScrollPanel = new AreaScrollPanel();

		okButton = new JButton(JecnProperties.getValue("okBtn"));
		calcelButton = new JButton(JecnProperties.getValue("cancelBtn"));

		companyCodeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				companyItem.getName()));
		productionLineScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				productionLineItem.getName()));
		fileTypeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), fileTypeItem
				.getName()));
		smallClassScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				smallClassItem.getName()));

		treePanel = new JScrollPane();
		// Dimension dimension = new Dimension(200, 395);
		// treePanel.setPreferredSize(dimension);
		// treePanel.setMinimumSize(dimension);
		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		// 树滚动面板
		treePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties.getValue("processCode")));
		treePanel.setViewportView(jecnTree);
		this.getContentPane().add(mainPanel);

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		calcelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AutoNumManageDialog.this.dispose();
			}
		});
	}

	protected void okButtonAction() {
		companyItem.setValue(companyCodeScrollPanel.getTextArea().getText());
		productionLineItem.setValue(productionLineScrollPanel.getTextArea().getText());
		fileTypeItem.setValue(fileTypeScrollPanel.getTextArea().getText());
		smallClassItem.setValue(smallClassScrollPanel.getTextArea().getText());

		List<JecnConfigItemBean> itemBeans = new ArrayList<JecnConfigItemBean>();
		getOkButtonItems(itemBeans);
		JecnConfigTool.updateItemBeans(itemBeans);
		this.dispose();
	}

	protected void getOkButtonItems(List<JecnConfigItemBean> itemBeans) {
		itemBeans.add(companyItem);
		itemBeans.add(productionLineItem);
		itemBeans.add(fileTypeItem);
		itemBeans.add(smallClassItem);
	}

	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 0, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(companyCodeScrollPanel, c);

		// other
		addOtherPanel(c, insets);

		c = new GridBagConstraints(0, 1, 5, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				0, 5, 0, 5), 0, 0);
		mainPanel.add(buttonPanel, c);

		// 添加按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okButton);
		buttonPanel.add(calcelButton);
	}

	protected void addOtherPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(treePanel, c);

		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(productionLineScrollPanel, c);

		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(fileTypeScrollPanel, c);

		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(smallClassScrollPanel, c);
	}

	class AreaScrollPanel extends JScrollPane {
		private JTextArea textArea = null;

		public AreaScrollPanel() {
			textArea = new JTextArea();
			// 名称输入框
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			textArea.setBorder(BorderFactory.createLineBorder(Color.gray));
			textArea.setBorder(null);

			Dimension dimension = new Dimension(50, 370);
			this.setMinimumSize(dimension);
			this.setViewportView(textArea);
			// 名称输入区域容器
			this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		}

		public JTextArea getTextArea() {
			return textArea;
		}
	}

	private AutoNumJecnTree getTree() {
		if (treeType == 0) {
			return new ProcessAutoNumJecnTree();
		} else if (treeType == 1) {
			return new FileAutoNumJecnTree();
		} else if (treeType == 2) {
			return new RuleAutoNumJecnTree();
		}
		return null;
	}

}
