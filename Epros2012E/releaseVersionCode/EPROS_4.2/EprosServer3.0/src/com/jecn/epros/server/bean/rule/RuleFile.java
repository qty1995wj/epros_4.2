package com.jecn.epros.server.bean.rule;


/**
 * 制度文件表
 * 
 * @author lihongliang
 * 
 */
public class RuleFile implements java.io.Serializable {
	private Long id;//主键ID
	private Long ruleTitleId;//制度标题ID
	private Long ruleFileId;//文件ID
	private String fileName;//文件名称（不存储的数据库，通过JecnFileBean中得到）
	private String fileInput;//文件编号
	
	
	public String getFileInput() {
		return fileInput;
	}
	public void setFileInput(String fileInput) {
		this.fileInput = fileInput;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRuleTitleId() {
		return ruleTitleId;
	}
	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}
	public Long getRuleFileId() {
		return ruleFileId;
	}
	public void setRuleFileId(Long ruleFileId) {
		this.ruleFileId = ruleFileId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
