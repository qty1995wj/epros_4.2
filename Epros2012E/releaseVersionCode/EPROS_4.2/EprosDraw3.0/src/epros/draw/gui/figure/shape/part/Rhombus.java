/*
 * Rhombus.java
 * 决策
 * Created on 日期：Mar 20, 2012 时间：6:33:08 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseRhombus;

/**
 * 
 * 决策框
 * 
 * @author ZHANGXH
 * @date： 日期：Mar 31, 2012 时间：10:34:38 AM
 */
public class Rhombus extends JecnBaseRhombus {

	public Rhombus(JecnFigureData jecnActiveData) {
		super(jecnActiveData);
	}
}
