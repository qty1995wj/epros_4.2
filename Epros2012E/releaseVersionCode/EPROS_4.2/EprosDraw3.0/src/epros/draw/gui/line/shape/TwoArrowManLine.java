package epros.draw.gui.line.shape;

import java.awt.Point;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

public class TwoArrowManLine extends ManhattanLine {

	private ArrowPanel startArrow = null;

	public TwoArrowManLine(JecnManhattanLineData flowElementData) {
		super(flowElementData);
	}

	protected void addArrowPanel() {
		// (结束点箭头)带箭头连接线的箭头
//		arrowPanel = new ArrowPanel(this);
//		// 开始线段箭头
//		startArrow = new ArrowPanel(this);
		// 设置箭头层级
		// 添加箭头到面板
		reDrawArrow();
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 添加箭头到画板
		desktopPane.add(arrowPanel);
		desktopPane.setLayer(arrowPanel, this.getFlowElementData().getZOrderIndex());

		// 添加开始箭头
		desktopPane.add(startArrow);
		desktopPane.setLayer(startArrow, this.getFlowElementData().getZOrderIndex());
	}

	/**
	 * 重新绘制箭头
	 */
	public void reDrawArrow() {
		if (lineSegments == null || lineSegments.size() == 0) {
			JecnLogConstants.LOG_BASE_FLOW_ELEMENT_PANEL.info("ManhattanLine类reDrawArrow方法：lineSegments中没有元素即没有小线段");
			return;
		}
		// 结束箭头
		drawArrowByEditType(this.getFlowElementData().getEndEditPointType(), this.getFlowElementData()
				.getZoomEndPoint(), arrowPanel);
		// 开始箭头
		drawArrowByEditType(this.getFlowElementData().getStartEditPointType(), this.getFlowElementData()
				.getZoomStartPoint(), startArrow);
	}

	private void drawArrowByEditType(EditPointType editPointType, Point point, ArrowPanel arrowPanel) {
		// 获取面板当前放大缩小倍数
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		// 结束点当前大小X坐标
		double endPointX = point.getX();
		// 结束点当前大小Y坐标
		double endPointY = point.getY();
		double lineWidth = 1;
		if (this.getFlowElementData().getBodyWidth() != 1) {
			lineWidth = this.getFlowElementData().getBodyWidth() / 2.0;
		}

		// 获取当前面板下箭头对应的高度和宽度
		int arrowCurW = DrawCommon.convertDoubleToInt(arrowW * scale * lineWidth);
		int arrowCurH = DrawCommon.convertDoubleToInt(arrowH * scale * lineWidth);

		// 循环输出编辑点端口 获取箭头位置
		switch (editPointType) {
		case left:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX - arrowCurW), DrawCommon.convertDoubleToInt(endPointY
					- arrowCurH / 2), arrowCurW, arrowCurH);
			break;
		case top:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX - arrowCurH / 2), DrawCommon.convertDoubleToInt(endPointY
					- arrowCurW), arrowCurH, arrowCurW);
			break;
		case right:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX),
					DrawCommon.convertDoubleToInt(endPointY - arrowCurH / 2), arrowCurW, arrowCurH);
			break;
		case bottom:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX - arrowCurH / 2),
					DrawCommon.convertDoubleToInt(endPointY), arrowCurH, arrowCurW);
			break;
		}
	}
}
