package com.jecn.epros.server.action.web.login.ad.csxw;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class LdapAuthVerifier {

	private static final Logger log = Logger.getLogger(LdapAuthVerifier.class);

	public static final int SUCCESS = 0;
	public static final int FAILED = 1;
	public static final int CONNECTION_ERROR = 0;

	// AD User
	private final String userName;
	// AD Password
	private final String passWord;
	// ldap service url
	private final String ldapServiceUrl;

	private LdapAuthVerifier(String username, String password, String ldapServiceUrl) {
		this.userName = username;
		this.passWord = password;
		this.ldapServiceUrl = ldapServiceUrl;
	}

	/**
	 * 获取环境
	 * 
	 * @return
	 */
	private Hashtable<String, String> getEnv() {
		Hashtable<String, String> table = new Hashtable<String, String>();
		table.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		table.put(Context.SECURITY_AUTHENTICATION, "simple");

		table.put(Context.SECURITY_PRINCIPAL, userName);
		table.put(Context.SECURITY_CREDENTIALS, passWord);
		table.put(Context.PROVIDER_URL, ldapServiceUrl);
		return table;
	}

	/**
	 * Ldap权限验证
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws NamingException
	 */
	public static LdapAuthVerifier newInstance(String username, String password, String ldapServiceUrl) {
		return new LdapAuthVerifier(username, password, ldapServiceUrl);
	}

	/**
	 * 进行权限验证，获取LdapContext
	 * 
	 * @throws NamingException
	 * 
	 * @throws Exception
	 * @throws Exception
	 */
	public LdapContext verifyAndGetContext() throws NamingException {
		LdapContext ctx = null;
		if (!checkUserInfo()) {
			throw new IllegalArgumentException("username or password is empty!");
		}
		ctx = new InitialLdapContext(this.getEnv(), null);
		return ctx;
	}

	/**
	 * 验证权限
	 * 
	 * @return int
	 * @throws Exception
	 */
	public int verifyAuth() {
		LdapContext ctx = null;
		try {
			ctx = verifyAndGetContext();
		} catch (AuthenticationException ex) {
			log.error("Ldap权限验证失败：", ex);
			return FAILED;
		} catch (NamingException e) {
			log.error("连接Ldap服务器出现异常：", e);
			return CONNECTION_ERROR;
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.error("释放资源出现异常：", e);
				}
			}
		}
		return SUCCESS;
	}

	/**
	 * 检查用户名和密码是否为空
	 * 
	 * @return
	 */
	private boolean checkUserInfo() {
		return StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(passWord);
	}

	public String getUsername() {
		return userName;
	}

	public String getPassword() {
		return passWord;
	}

}
