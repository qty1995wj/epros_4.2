package com.jecn.epros.server.dao.dataImport.match;

import java.util.List;

import com.jecn.epros.server.common.IBaseDao;

public interface ISearchTableMatchDao extends IBaseDao<String, String> {

	List<Object[]> getSqlResut(String string, String arg) throws Exception;

}
