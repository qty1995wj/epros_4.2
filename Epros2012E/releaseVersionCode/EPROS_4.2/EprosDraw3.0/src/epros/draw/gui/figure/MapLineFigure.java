package epros.draw.gui.figure;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.shape.JecnEditPortPanel;

public class MapLineFigure extends JecnBaseFigurePanel {

	public MapLineFigure(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
	}

	protected void init() {
		editLeftPortPanel = JecnEditPortPanel.createLeftEditPortPanel(this);
		// 连接线位置点 右
		editRightPortPanel = JecnEditPortPanel.createRightEditPortPanel(this);
		// 连接线位置点 上
		editTopPortPanel = JecnEditPortPanel.createTopEditPortPanel(this);
		// 连接线位置点 下
		editBottomPortPanel = JecnEditPortPanel.createBottomEditPortPanel(this);
	}
}
