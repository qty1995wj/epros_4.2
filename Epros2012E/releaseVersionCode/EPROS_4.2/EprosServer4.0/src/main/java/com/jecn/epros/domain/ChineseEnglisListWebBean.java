package com.jecn.epros.domain;

import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.report.base.CharItem;
import com.jecn.epros.domain.report.base.LineChartOptions;

import java.util.List;

/**
 * 中英文报表统计 VO
 * Created by Administrator on 2018/9/5/005.
 */
public class ChineseEnglisListWebBean {
    //报表数据
    private Inventory inventory;
    private List<ChineseEnglisListBean> rules;

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public List<ChineseEnglisListBean> getRules() {
        return rules;
    }

    public void setRules(List<ChineseEnglisListBean> rules) {
        this.rules = rules;
    }
}
