package epros.designer.gui.integration.risk;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnChooseInfoDialog;
import epros.designer.gui.integration.MultiLineCellRenderer;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 选择控制目标
 * 2013-11-08
 *
 */
public class ChooseControlTargetDialog extends JecnChooseInfoDialog {
	private Logger log = Logger.getLogger(ChooseControlTargetDialog.class);

	/***选中控制目标ID*/
	private Long controlTargetId = null;
	/***选中控制目标详细内容*/
	private String controlTargetDesc = null;

	/**控制目标数据集合*/
	private List<JecnControlTarget> controlTargetList = new ArrayList<JecnControlTarget>();
	
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	/**所属风险ID**/
	private Long riskId;
	/**风险编号*/
	private String riskNum;
	private String controlTarget;
	/**是否点击确定按钮*/
	private boolean okButton=false;
	
	public ChooseControlTargetDialog(){
		super();
		initData();
	}
	
	public ChooseControlTargetDialog(Long riskId){
		super();
		this.riskId=riskId;
		initData();
		
	}
	
	/**
	 * 
	 * 结果集数据初始化
	 * 
	 * */
	public void initData() {
		if(riskId!=null){
			resultTable.remoeAll();
			// 控制目标数据集合
			try {
				controlTargetList = ConnectionPool.getJecnRiskAction()
						.getAllControlTargetByRiskId(riskId);
			} catch (Exception e1) {
				log.error("ChooseControlTargetDialog initData is error", e1);
			}
			// 增加
			for (JecnControlTarget controlTarget : controlTargetList) {
				Vector<String> data = new Vector<String>();
				data.add(String.valueOf(controlTarget.getId()));// 控制目标ID
				data.add(controlTarget.getRiskId().toString());// 风险ID
				data.add(null);// 风险名称
				data.add(controlTarget.getDescription());// 控制目标
				data.add(controlTarget.getRiskNum());// 风险编号
				resultTable.addRow(data);
			}
		}
	}
	
	
	@Override
	public String getChooseTitle() {
		return JecnProperties.getValue("ChoiceControlTarget");
	}
	@Override
	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}
	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyChooseControlTargetTree();
	}
	@Override
	public String getResultTitle() {
		return JecnProperties.getValue("controlTarget");
	}
	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add("riskId");
		title.add("riskDesc");
		/*
		 * 控制目标
		 */
		title.add(JecnProperties.getValue("controlTarget"));
		/*
		 * 风险编号
		 */
		title.add(JecnProperties.getValue("riskNum"));
		return title;
	}
	@Override
	public String getTreeTitle() {
		/*
		 * 风险
		 */
		return JecnProperties.getValue("risk");
	}
	@Override
	public void okButPerformed() {
		verfyLab.setText("");
		int row = resultTable.getSelectedRow();
		if(row != -1){
			// 表格行：0控制目标主键ID，1//风险点ID 2；风险描述3:控制目标4//风险点编号
			// 显示选中行将数据显示到下方的controlTargetDescArea中
			Object object = null;
			Long controlTargetId = Long.valueOf(this.resultTable
					.getModel().getValueAt(row, 0).toString());
			String controlTargetDesc = this.resultTable.getModel()
					.getValueAt(row, 3).toString();
			// 风险ID
			object = this.resultTable.getModel().getValueAt(row, 1);
			if (object != null) {
				this.riskId = Long.valueOf(object.toString());
			}
			// 更新编号
			object = this.resultTable.getModel().getValueAt(row, 4);
			if (object != null) {
				this.riskNum = object.toString();
			}
			this.controlTargetId = controlTargetId;
			this.controlTargetDesc = controlTargetDesc;
		}else{
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		okButton = true;
		this.dispose();
	}
	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			if (evt.getClickCount() == 1) {
				if (!jTree.isSelectionEmpty()) {
					javax.swing.tree.TreePath treePath = jTree.getSelectionPath();
					JecnTreeBean jecnTreeBean = ((JecnTreeNode) treePath.getLastPathComponent()).getJecnTreeBean();
					if (jecnTreeBean.getTreeNodeType() == TreeNodeType.riskPoint) {
						resultTable.remoeAll();

						// 控制目标数据集合
						try {
							controlTargetList = ConnectionPool
									.getJecnRiskAction()
									.getAllControlTargetByRiskId(
											jecnTreeBean.getId());
						} catch (Exception e1) {
							log.error("jTreeMousePressed is error",e1);
						}
						// 增加
						for (JecnControlTarget controlTarget : controlTargetList) {
							Vector<String> data = new Vector<String>();
							data.add(String.valueOf(controlTarget.getId()));// 控制目标ID
							data.add(jecnTreeBean.getId().toString());// 风险ID
							data.add(jecnTreeBean.getName());// 风险名称
							data.add(controlTarget.getDescription());// 控制目标
							data.add(controlTarget.getRiskNum());// 风险编号
							resultTable.addRow(data);
						}
						//设置选中第一行
						if(controlTargetList.size()>0){
							resultTable.setRowSelectionInterval(0,0);
						}
					}
				
				}
			}
		}
	}
	/**
	 * @description:单击Table行
	 */
	@Override
	public void controlTargetTablemousePressed(MouseEvent evt) {
		if (evt.getClickCount() == 1) {
			int row = resultTable.getSelectedRow();
			int newRowHeight = resultTable.getRowHeight(row);
			if(row != -1){
				if(oldRowHeight>=newRowHeight){
					resultTable.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 1,0));
					return;
				}else if(oldRowHeight<newRowHeight){
					//选中后高度设置
					if (newRowHeight != oldRowHeight) { 
						resultTable.setRowHeight(row, oldRowHeight);
//							controlGuideTable.repaint();
//							controlGuideScrollPane.setViewportView(controlGuideTable);
						resultTable.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 1,1));     
							}
						}
					}
				}
		}

	public boolean isOkButton() {
		return okButton;
	}

	public Long getControlTargetId() {
		return controlTargetId;
	}

	public void setControlTargetId(Long controlTargetId) {
		this.controlTargetId = controlTargetId;
	}

	public String getControlTargetDesc() {
		return controlTargetDesc;
	}

	public void setControlTargetDesc(String controlTargetDesc) {
		this.controlTargetDesc = controlTargetDesc;
	}

	public Long getRiskId() {
		return riskId;
	}

	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}

	public String getRiskNum() {
		return riskNum;
	}

	public void setRiskNum(String riskNum) {
		this.riskNum = riskNum;
	}

	public String getControlTarget() {
		return controlTarget;
	}

	public void setControlTarget(String controlTarget) {
		this.controlTarget = controlTarget;
	}
	
	
}
