package com.jecn.epros.domain.propose;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ProposeTotalData {
    /***主键ID */
    private String id;
    /***建议内容*/
    private String proposeContent;
    private Long fileId;
    private String fileName;
    /**
     * 文件类型 0是流程，1是制度，2是文件，3是标准，4风险,7是流程架构
     */
    private int fileType;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date creatTime;
    private Long submitPeopleId;
    private String submitPeopleName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProposeContent() {
        return proposeContent;
    }

    public void setProposeContent(String proposeContent) {
        this.proposeContent = proposeContent;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Long getSubmitPeopleId() {
        return submitPeopleId;
    }

    public void setSubmitPeopleId(Long submitPeopleId) {
        this.submitPeopleId = submitPeopleId;
    }

    public String getSubmitPeopleName() {
        return submitPeopleName;
    }

    public void setSubmitPeopleName(String submitPeopleName) {
        this.submitPeopleName = submitPeopleName;
    }
}
