package epros.designer.gui.system.config.process;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.util.ConnectionPool;

/**
 * 
 * 配置数据处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigProcess {

	/**
	 * 
	 * 流程地图（大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置）
	 * 
	 * @return JecnConfigBean 流程地图数据
	 */
	public static JecnConfigDesgBean getTotalMapConfigData() {
		return getConfigBean(getConfigData(JecnConfigContents.TYPE_BIG_ITEM_TOTALMAP),
				JecnConfigContents.TYPE_BIG_ITEM_TOTALMAP);
	}

	/**
	 * 
	 * 流程图（大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置）
	 * 
	 * @return JecnConfigBean 流程图数据
	 */
	public static JecnConfigDesgBean getPartMapConfigData() {
		return getConfigBean(getConfigData(JecnConfigContents.TYPE_BIG_ITEM_PARTMAP),
				JecnConfigContents.TYPE_BIG_ITEM_PARTMAP);
	}

	/**
	 * 
	 * 制度（大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置）
	 * 
	 * @return JecnConfigBean 制度数据
	 */
	public static JecnConfigDesgBean getRuleConfigData() {
		return getConfigBean(getConfigData(JecnConfigContents.TYPE_BIG_ITEM_ROLE),
				JecnConfigContents.TYPE_BIG_ITEM_ROLE);
	}

	/**
	 * 
	 * 文件（大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置）
	 * 
	 * @return JecnConfigBean 文件数据
	 */
	public static JecnConfigDesgBean getFileConfigData() {
		return getConfigBean(getConfigData(JecnConfigContents.TYPE_BIG_ITEM_FILE),
				JecnConfigContents.TYPE_BIG_ITEM_FILE);
	}

	/**
	 * 
	 * 邮箱（大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置）
	 * 
	 * @return JecnConfigBean 邮箱数据
	 */
	public static JecnConfigDesgBean getMailConfigData() {
		return getConfigBean(getConfigData(JecnConfigContents.TYPE_BIG_ITEM_MAIL),
				JecnConfigContents.TYPE_BIG_ITEM_MAIL);
	}

	/**
	 * 
	 * 权限（大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置）
	 * 
	 * @return JecnConfigBean 权限数据
	 */
	public static JecnConfigDesgBean getPubConfigData() {
		return getConfigBean(getConfigData(JecnConfigContents.TYPE_BIG_ITEM_PUB), JecnConfigContents.TYPE_BIG_ITEM_PUB);
	}

	/**
	 * 
	 * 后台数据转换成JecnConfigBean
	 * 
	 * @param list
	 *            List<JecnConfigItemBean>
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return JecnConfigBean
	 */
	private static JecnConfigDesgBean getConfigBean(List<JecnConfigItemBean> list, int typeBigModel) {
		// 后台数据
		JecnConfigDesgBean configBean = new JecnConfigDesgBean(list, typeBigModel);
		if (list == null || list.size() == 0) {
			return configBean;
		}

		for (JecnConfigItemBean itemBean : list) {
			// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置
			// 6：权限配置7：任务操作8：消息邮件配置
			int typeSmallModel = itemBean.getTypeSmallModel();
			switch (typeSmallModel) {
			case 0:// 审批环节配置
				configBean.getTaskApprTypeDesgBean().addItemList(itemBean);
				break;
			case 1:// 任务界面显示项配置
				configBean.getTaskShowTypeDesgBean().addItemList(itemBean);
				break;
			case 2:// 基本配置
				configBean.getBasicTypeDesgBean().addItemList(itemBean);
				break;
			case 3:// 流程图建设规范
				configBean.getStandedTypeDesgBean().addItemList(itemBean);
				break;
			case 4:// 流程文件
				configBean.getFlowFileTypeDesgBean().addItemList(itemBean);
				break;
			case 5:// 邮箱配置
				configBean.getMailTypeDesgBean().addItemList(itemBean);
				break;
			case 6:// 权限配置
				configBean.getPubSetTypeDesgBean().addItemList(itemBean);
				break;
			case 7:// 任务操作配置
				configBean.getTaskOperatDesgBean().addItemList(itemBean);
				break;
			case 8:// 消息邮件配置
				configBean.getMessageEmailDesgBean().addItemList(itemBean);
				break;
			case 9:// 合理化建议
				configBean.getRationProDesgBean().addItemList(itemBean);
				break;
			case 10:// 流程属性配置
				configBean.getFlowBasicBean().addItemList(itemBean);
				break;
			case 11:// 发布配置
				configBean.getPubRelateFileBean().addItemList(itemBean);
				break;
			case 14:// 下载次数配置
				configBean.getDownLoadCountBean().addItemList(itemBean);
				break;
			case 15:// 文控信息配置
				configBean.getDocumentControlBean().addItemList(itemBean);
				break;
			case 16:// 流程KPI配置
				configBean.getFlowKPIAttributeBean().addItemList(itemBean);
				break;
			case 18:// 流程架构-其它配置
				configBean.getFlowMapOtherBean().addItemList(itemBean);
				break;
			default:	
				break;
			}
		}
		return configBean;
	}

	/**
	 * 
	 * 获取配置数据
	 * 
	 * @param type
	 *            int 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @return List<JecnConfigItemBean> 配置数据
	 */
	private static List<JecnConfigItemBean> getConfigData(int type) {
		return ConnectionPool.getConfigAciton().select(type);
	}
}
