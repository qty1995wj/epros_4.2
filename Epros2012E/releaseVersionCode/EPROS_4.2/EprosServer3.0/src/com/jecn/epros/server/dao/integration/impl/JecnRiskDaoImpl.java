package com.jecn.epros.server.dao.integration.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.dao.integration.IJecnRiskDao;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/*******************************************************************************
 * 风险表操作类 2013-10-30
 * 
 */
public class JecnRiskDaoImpl extends AbsBaseDao<JecnRisk, Long> implements
		IJecnRiskDao {

	@Override
	public void deleteRisks(Set<Long> setIds) throws Exception {
		// 风险-设计器
		String hql = "delete from JecnRisk where id in ";
		List<String> listStr = JecnCommonSql.getSetSqlAddBracket(hql, setIds);
		for (String str : listStr) {
			execteHql(str);
		}

	}

	/**
	 * 
	 * 全部展示
	 * 
	 * 根据风险目录ID获取当前目录下所有风险控制清单
	 * 
	 * @param id
	 *            当前目录ID
	 * @param projectId
	 *            项目ID
	 * @return List<Object[]>
	 * @throws Exception
	 * @author ZHF
	 */
	public List<Object[]> getRiskInventoryALL(Long id, Long projectId,boolean isAll) throws Exception {
		List<Long> listIds = new ArrayList<Long>();
		listIds.add(id);
		String sql = "with RISK AS (SELECT R.ID,R.IS_DIR,R.RISK_CODE,R.GRADE,"
				+ "  R.NAME,R.STANDARDCONTROL,R.T_LEVEL AS LEV,R.PARENT_ID,R.SORT"
				+ "  FROM JECN_RISK R INNER JOIN ("
				+ JecnCommonSql.getChildObjectsSqlByType(listIds,
						TreeNodeType.riskDir) + ") jr on r.id=jr.id"
				+ "  WHERE R.PROJECT_ID=" + projectId + " )";
		sql += " select " + selectRiskList();
		return listNativeSql(sql);
	}

	/**
	 * 风险清单 oracle 和sql server 一致sql
	 * 
	 * @author fuzhh 2013-12-17
	 * @return
	 */
	private String selectRiskList() {
		String sql = "   tmp.LEV,"
				+ "                tmp.GRADE,"
				+ "                tmp.RISK_ID,"
				+ "                tmp.IS_DIR,"
				+ "                tmp.RISK_CODE,"
				+ "                tmp.RISK_NAME,"
				+ "                tmp.STANDARDCONTROL,"
				+ "                tmp.GUIDE_ID,"// 内控指引Id
				+ "                tmp.GUIDE_NAME,"// 内控指引名称
				+ "                tmp.GUIDE_DESCRIPTION,"// 内控指引描述
				+ "                tmp.RC_ID,"// 控制目标ID
				+ "                tmp.RISK_DES,"// 控制目标说明
				+ "                tmp.CONTROL_CODE,"// 控制点编号
				+ "                tmp.ACTIVITY_SHOW,"// 控制点说明
				+ "                tmp.FIGURE_ID,"// 控制点主键
				+ "                tmp.FIGURE_TEXT,"// 控制点名称
				+ "                tmp.FLOW_ID,"// 控制点对应流程ID
				+ "                tmp.FLOW_NAME,"// 控制点对应流程名称
				+ "                tmp.RULE_ID,"// 风险对应制度ID
				+ "                tmp.RULE_NAME,"// 风险对应制度名称
				+ "                tmp.ORG_ID,"// 风险对应流程责任部门ID
				+ "                tmp.ORG_NAME,"// 风险对应流程责任部门名称
				+ "                tmp.RULE_ORG_ID,"// 风险对应制度责任部门ID
				+ "                tmp.RULE_ORG_NAME,"// 风险对应制度责任部门名称
				+ "                tmp.METHOD,"
				+ "                tmp.CPT_TYPE,"
				+ "                tmp.KEY_POINT,"
				+ "                tmp.FREQUENCY,"
				+ "                tmp.PARENT_ID,"
				+ "                tmp.Role_Text,tmp.SORT,tmp.Activity_Id from (";
		sql = sql
				+ "   select RISK.LEV,"
				+ "                RISK.GRADE,"
				+ "                RISK.ID AS RISK_ID,"
				+ "                RISK.IS_DIR,"
				+ "                RISK.RISK_CODE,"
				+ "                RISK.NAME AS RISK_NAME,"
				+ "                RISK.STANDARDCONTROL,"
				+ "                JCG.ID AS GUIDE_ID,"// 内控指引Id
				+ "                JCG.NAME AS GUIDE_NAME,"// 内控指引名称
				+ "                JCG.DESCRIPTION AS GUIDE_DESCRIPTION,"// 内控指引描述
				+ "                RC.ID AS RC_ID,"// 控制目标ID
				+ "                RC.DESCRIPTION AS RISK_DES,"// 控制目标说明
				+ "                CPT.CONTROL_CODE,"// 控制点编号
				+ "                FSIT.ACTIVITY_SHOW,"// 控制点说明
				+ "                FSIT.FIGURE_ID,"// 控制点主键
				+ "                FSIT.FIGURE_TEXT,"// 控制点名称
				+ "                FST.FLOW_ID,"// 控制点对应流程ID
				+ "                FST.FLOW_NAME,"// 控制点对应流程名称
				+ "                null AS RULE_ID,"// 风险对应制度ID
				+ "                null AS RULE_NAME,"// 风险对应制度名称
				+ "                FO.ORG_ID,"// 风险对应流程责任部门ID
				+ "                FO.ORG_NAME,"// 风险对应流程责任部门名称
				+ "                null AS RULE_ORG_ID,"// 风险对应制度责任部门ID
				+ "                null AS RULE_ORG_NAME,"// 风险对应制度责任部门名称
				+ "                CPT.METHOD,"
				+ "                CPT.TYPE AS CPT_TYPE,"
				+ "                CPT.KEY_POINT,"
				+ "                CPT.FREQUENCY,"
				+ "                RISK.PARENT_ID,"
				+ "                JFSIA.FIGURE_TEXT as Role_Text,RISK.SORT,FSIT.Activity_Id"
				+ "           FROM RISK"
				+ "           LEFT JOIN JECN_CONTROLTARGET RC ON RC.RISK_ID = RISK.ID"// 控制目标
				+ "           LEFT JOIN JECN_CONTROL_POINT CPT ON CPT.TARGET_ID = RC.ID"// 控制点
				+ "           LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE FSIT ON FSIT.FIGURE_ID = CPT.ACTIVE_ID"// 活动表
				+ "           LEFT JOIN JECN_FLOW_STRUCTURE FST ON FST.FLOW_ID = FSIT.FLOW_ID"// 流程表
				+ "           LEFT JOIN JECN_FLOW_RELATED_ORG FROT ON FROT.FLOW_ID = FST.FLOW_ID"// 责任人表
				+ "           LEFT JOIN JECN_FLOW_ORG FO ON FO.ORG_ID = FROT.ORG_ID"// 部门表
				+ "           LEFT JOIN JECN_RISK_GUIDE JRG ON JRG.RISK_ID = RISK.ID"// 任务内控指引关联表
				+ "           LEFT JOIN JECN_CONTROL_GUIDE JCG ON JRG.GUIDE_ID = JCG.ID"// 任务内控指引表
				+ "           LEFT JOIN JECN_ROLE_ACTIVE JRA ON JRA.FIGURE_ACTIVE_ID=FSIT.FIGURE_ID"// 活动与角色管理表
				+ "           LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE JFSIA ON JFSIA.FIGURE_ID = JRA.FIGURE_ROLE_ID";// 角色表
		sql = sql + " union all";
		sql = sql
				+ "  select RISK.LEV,"
				+ "                RISK.GRADE,"
				+ "                RISK.ID AS RISK_ID,"
				+ "                RISK.IS_DIR,"
				+ "                RISK.RISK_CODE,"
				+ "                RISK.NAME AS RISK_NAME,"
				+ "                RISK.STANDARDCONTROL,"
				+ "                null AS GUIDE_ID,"// 内控指引Id
				+ "                null AS GUIDE_NAME,"// 内控指引名称
				+ "                null AS GUIDE_DESCRIPTION,"// 内控指引描述
				+ "                null AS RC_ID,"// 控制目标ID
				+ "                null AS RISK_DES,"// 控制目标说明
				+ "                null AS CONTROL_CODE,"// 控制点编号
				+ "                null AS ACTIVITY_SHOW,"// 控制点说明
				+ "                null AS FIGURE_ID,"// 控制点主键
				+ "                null AS FIGURE_TEXT,"// 控制点名称
				+ "                null AS FLOW_ID,"// 控制点对应流程ID
				+ "                null AS FLOW_NAME,"// 控制点对应流程名称
				+ "                RT.ID AS RULE_ID,"// 风险对应制度ID
				+ "                RT.RULE_NAME,"// 风险对应制度名称
				+ "                null AS ORG_ID,"// 风险对应流程责任部门ID
				+ "                null AS ORG_NAME,"// 风险对应流程责任部门名称
				+ "                RFO.ORG_ID AS RULE_ORG_ID,"// 风险对应制度责任部门ID
				+ "                RFO.ORG_NAME AS RULE_ORG_NAME,"// 风险对应制度责任部门名称
				+ "                null AS METHOD,"
				+ "                null AS CPT_TYPE,"
				+ "                null AS KEY_POINT,"
				+ "                null AS FREQUENCY,"
				+ "                RISK.PARENT_ID,"
				+ "                null AS Role_Text,RISK.SORT,null AS Activity_Id"
				+ "           FROM RISK"
				+ "           inner JOIN JECN_RULE_RISK RRT ON RRT.RISK_ID = RISK.ID"
				+ "           inner JOIN JECN_RULE RT ON RRT.RULE_ID = RT.ID"
				+ "           LEFT JOIN JECN_FLOW_ORG RFO ON RFO.ORG_ID = RT.ORG_ID where RISK.IS_DIR=1";
		sql = sql + " )tmp";

		sql = sql
				+ " ORDER BY tmp.PARENT_ID,tmp.SORT,tmp.RISK_ID,tmp.RISK_CODE,tmp.CONTROL_CODE,tmp.Activity_Id";
		return sql;
	}

	/**
	 * 风险清单 节点处在的级别
	 * 
	 * @author fuzhh 2013-12-13
	 * @param id
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int findRiskListLevel(Long id) throws Exception {
		JecnRisk jecnRisk = this.get(id);
		return jecnRisk.gettLevel();
	}

	@Override
	public List<Object[]> getRoleAuthChildsRisk(Long pid, Long projectId, Long peopleId) {
		String sql = "select distinct t.id,t.parent_id,t.risk_code,t.is_dir,t.grade,t.sort,"
			+ "t.standardcontrol,t.create_person,t.create_time,t.update_person,t.update_time,t.note,t.name, "
			+ " case when jr.id is null then 0 else 1 end as count "
			+ " from JECN_RISK t"
			+ " LEFT JOIN JECN_RISK JR ON JR.parent_id=T.ID ";
			sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(4, peopleId, projectId);
			sql = sql + " where t.parent_id=" + pid
			+ " and  t.PROJECT_ID =? order by t.parent_id,t.sort,t.id";
		return this.listNativeSql(sql, projectId);
	}

	@Override
	public List<Object[]> searchRoleAuthByName(String name, Long peopleId) {
		String sql = "select t.id,t.parent_id,t.risk_code,t.is_dir,t.grade,t.sort,t.standardcontrol,t.create_person,t.create_time,t.update_person,t.update_time,t.note,t.name, "
				+ " (select count(*) from JECN_RISK where parent_id=t.id) as count " + " from JECN_RISK t ";
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(4, peopleId, null);
		sql = sql + " where t.name like ? " + " order by t.id";
		return this.listNativeSql(sql, "%" + name + "%");
	}

}
