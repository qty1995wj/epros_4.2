package com.jecn.epros.domain;

/**
 * 组织类
 *
 * @author Administrator
 */
public class JecnFlowOrg implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private Long projectId;// 项目ID
    private Long orgId;// 主键Id
    private String orgName;// 部门名称
    private String orgRespon;// 部门职责
    private Long preOrgId;// 父类ID
    private String orgWidth;// 画图面板的宽
    private String orgHeight;// 画图面板的高
    private Long sortId;// 排序
    private Long delState;// 0代表没有删除，1代表删除
    private Long occupier;// 占用人
    private String tPath;// tpath 节点层级关系
    private Integer tLevel;// level 节点级别，默认0开始
    /**
     * 对接
     */
    private String orgNumber;// 组织编号ID
    private String preOrgNumber;// 上级组织编号ID

    public Integer gettLevel() {
        return tLevel;
    }

    public void settLevel(Integer tLevel) {
        this.tLevel = tLevel;
    }

    public String gettPath() {
        return tPath;
    }

    public void settPath(String tPath) {
        this.tPath = tPath;
    }

    public Long getOccupier() {
        return occupier;
    }

    public void setOccupier(Long occupier) {
        this.occupier = occupier;
    }

    public JecnFlowOrg() {

    }

    public JecnFlowOrg(Long orgId) {
        this.orgId = orgId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgRespon() {
        return orgRespon;
    }

    public void setOrgRespon(String orgRespon) {
        this.orgRespon = orgRespon;
    }

    public Long getPreOrgId() {
        return preOrgId;
    }

    public void setPreOrgId(Long preOrgId) {
        this.preOrgId = preOrgId;
    }

    public String getOrgWidth() {
        return orgWidth;
    }

    public void setOrgWidth(String orgWidth) {
        this.orgWidth = orgWidth;
    }

    public String getOrgHeight() {
        return orgHeight;
    }

    public void setOrgHeight(String orgHeight) {
        this.orgHeight = orgHeight;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public Long getDelState() {
        return delState;
    }

    public void setDelState(Long delState) {
        this.delState = delState;
    }

    public String getOrgNumber() {
        return orgNumber;
    }

    public void setOrgNumber(String orgNumber) {
        this.orgNumber = orgNumber;
    }

    public String getPreOrgNumber() {
        return preOrgNumber;
    }

    public void setPreOrgNumber(String preOrgNumber) {
        this.preOrgNumber = preOrgNumber;
    }
}
