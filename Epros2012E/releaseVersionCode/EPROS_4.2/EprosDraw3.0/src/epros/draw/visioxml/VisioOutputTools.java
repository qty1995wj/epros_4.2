package epros.draw.visioxml;

import visioxml.constant.Constants.FillPattern;
import visioxml.constant.Constants.FontStyle;
import visioxml.constant.Constants.LinePattern;
import visioxml.util.Unit;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;

/**
 * 导出visio 属性配置文件
 * 
 * @author admin
 * 
 */
public class VisioOutputTools {
	public static double change1DX(double pinX) {
		return Unit.pxToMM(pinX);
	}

	/**
	 * Visio中的Y轴值与Epros相反，所以需要用最大高度减去元素的Y值及偏移量
	 * 
	 * @param pinY
	 * @param panelHeight
	 * @return
	 */
	public static double change1DY(double pinY, double panelHeight) {
		pinY = panelHeight - pinY;
		return Unit.pxToMM(pinY);
	}

	public static double changeX(double pinX, double figureWidth) {
		return Unit.pxToMM(pinX + (figureWidth / 2));
	}

	/**
	 * Visio中的Y轴值与Epros相反，所以需要用最大高度减去元素的Y值及偏移量
	 * 
	 * @param pinY
	 * @param panelHeight
	 * @return
	 */
	public static double changeY(double pinY, double panelHeight, double figureHeight) {
		pinY = panelHeight - pinY - (figureHeight / 2);
		return Unit.pxToMM(pinY);
	}

	/**
	 * 处理双色 填充 水平填充 30 垂直 27 向下倾斜25 向上倾斜 39
	 * 
	 * @param fillType
	 * @param colorChangeType
	 * @return
	 */
	public static FillPattern getFillPattern(FillColorChangType colorChangeType) {
		// topTilt, // 1：向上倾斜 默认
		// bottom, // 2：向下倾斜
		// vertical, // 3：垂直
		// horizontal// 4：水平
		switch (colorChangeType) {
		case topTilt:
			return FillPattern._39;
		case bottom:
			return FillPattern._25;
		case vertical:
			return FillPattern._27;
		case horizontal:
			return FillPattern._30;
		default:
			return null;
		}
	}

	/**
	 * 只处理单色
	 * 
	 * @param fillType
	 * @return
	 */
	public static FillPattern getFillPattern(FillType fillType) {
		switch (fillType) {
		case none:
			return FillPattern._00;
		case one:
			return FillPattern._01;
		default:
			return FillPattern._01;
		}
	}

	/***** 默认普通样式PLAIN（0） ，1 加粗 **********/
	public static FontStyle getFontStyle(int fontStyle) {
		switch (fontStyle) {
		case 0:
			return FontStyle.DEFAULT;
		case 1:
			return FontStyle.JIACHU;
		default:
			return FontStyle.DEFAULT;
		}
	}

	/** 线条类型：默认实线 ； 0：实线；1：虚线；2：点化线；3：点线 */
	public static LinePattern getLinePattern(int lineType) {
		switch (lineType) {
		case 0:
			return LinePattern.DEFAULT;
		case 1:
			return LinePattern.LINE_02;
		case 2:
			return LinePattern.LINE_09;
		case 3:
			return LinePattern.LINE_03;
		default:
			return LinePattern.DEFAULT;
		}
	}
}
