package epros.designer.gui.autoNum;


/**
 * 文件自动编号管理页面
 * 
 * @author ZXH
 * 
 */
public class FileAutoNumManageDialog extends AutoNumManageDialog {
	public FileAutoNumManageDialog() {
		// 0 流程，1 文件 ， 2 制度
		super(1);
	}
}
