package com.jecn.epros.server.service.dataImport.sync.xmlImport.impl;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.dao.dataImport.xml.IXmpImportDAO;
import com.jecn.epros.server.dao.popedom.IOrganizationDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.service.dataImport.sync.xmlImport.XmlContants;
import com.jecn.epros.server.service.dataImport.sync.xmlImport.dbSource.IXmlDataImportService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.dataImport.ExcelConfigBean;
import com.jecn.epros.server.webBean.dataImport.OrgExcelBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.DataBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.DepartmentBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.MyNode;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.PersonBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.PositionBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.StationAndPeopleRelate;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.UserInformationBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.XmlConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.XmlStructureBean;

/**
 * 数据导入接口(大唐电信)
 * 
 */
public class XmlDataImportImpl extends UnicastRemoteObject implements
		IXmlDataImportService {
	private static Logger log = Logger.getLogger(XmlDataImportImpl.class);
	private IXmpImportDAO iXmpImportDAO;
	/** 人员 */
	private IPersonDao personDao;
	/** 组织 */
	private IOrganizationDao organizationDao;
	private String configFilePath = "dataImportCfg.xml";// 配置参数文件路径
	private String userFilePath = "./sameNamesExcel.xls";// 用户数据文件路径(配置参数文件获得)
	private String resultFilePath = "importResult.txt";// 匹配失败结果文件路径(配置参数文件获得)

	private Document doc = null;
	private DataBean dataBean = null;
	private UserInformationBean userInfBean = null;
	private Timer timer = null;
	private Map<String, String> listMap = new HashMap<String, String>();
	private static int EM_TYPE_NODE = 0;
	private static int EM_TYPE_ATTR = 1;
	private List<DepartmentBean> deptList;
	private String destFile = "importData.xml";

	private static String USER_NUMBER = "userNumber";
	private static String LOGIN_NAME = "loginName";
	private static String PASSWORD = "password";
	private static String TRUE_NAME = "trueName";
	private static String PHONE = "phone";
	private static String EMAIL = "email";
	private static String POSITION_ID = "positionId";
	private static String POSITION_NAME = "positionName";
	private static String DEPT_ID = "deptId";
	private static String DEPT_NAME = "deptName";
	private static List<ExcelConfigBean> excelFileList;// 人员岗位
	private static List<OrgExcelBean> orgExcelList;
	private static List<JecnFlowOrg> treeList;
	private static List<String> loginNames;
	public final static String ADMIN_ROLE = "admin";// 系统管理员

	public XmlDataImportImpl() throws RemoteException, DocumentException {
		super();
		doc = JecnFinal.getDocumentByXml(configFilePath);
	}

	// 读取配置文件
	@SuppressWarnings("unchecked")
	public DataBean readConfigFile() throws DocumentException {
		doc = JecnFinal.getDocumentByXml(configFilePath);
		if (doc != null && !"".equals(doc)) {
			Element root = doc.getRootElement();
			dataBean = new DataBean();
			List<Element> list = root.elements();
			if (list.size() < 0) {
				return null;
			}
			for (Element e : list) {
				List<Element> childList = e.elements();
				if ("filePath".equals(e.getName())) {
					dataBean.setFilePath(e.getTextTrim());
				}
				if ("time".equals(e.getName())) {
					dataBean.setTime(e.getTextTrim());
				}
				if ("param".equals(e.getName())) {
					dataBean.setTime(e.getTextTrim());
				}

				for (Element child : childList) {
					if ("userFilePath".equals(child.getName())) {
						dataBean.setUserFilePath(child.getTextTrim());
					}
					if ("resultFilePath".equals(child.getName())) {
						dataBean.setResultFilePath(child.getTextTrim());
					}
					if ("startTime".equals(child.getName())) {
						dataBean.setStartTime(child.getTextTrim());
					}
					if ("day".equals(child.getName())) {
						dataBean.setDay(child.getTextTrim());
					}
					if ("iaAuto".equals(child.getName())) {
						dataBean.setIaAuto(child.getTextTrim());
					}
				}
			}
		}
		return dataBean;
	}

	/**
	 * 同步人员
	 * 
	 * @throws Exception
	 */
	@Override
	public void synchronizationPeoples() throws Exception {
		List<PersonBean> listPersonBean = iXmpImportDAO.getAllPersonBean();
		// List<PositionBean> listPositionBean = dataImportDao
		// .getAllPosition();
		List<JecnUser> listJecnUser = personDao.findAllJecnUser();
		/*
		 * List<JecnUserInfo> listJecnUserInfo = jecnUserInfoDAO
		 * .findAllJecnUserInfo();
		 */
		List<JecnUser> listUpdate = new ArrayList<JecnUser>();
		List<Long> listdelete = new ArrayList<Long>();
		List<JecnUser> listNew = new ArrayList<JecnUser>();
		for (int i = 0; i < listPersonBean.size(); i++) {
			PersonBean personBean = listPersonBean.get(i);
			boolean flag = false;
			String numberId = personBean.getLoginName();
			for (int j = 0; j < listJecnUser.size(); j++) {
				JecnUser jecnUser = listJecnUser.get(j);
				if (jecnUser.getLoginName() != null && numberId != null
						&& numberId.equals(jecnUser.getLoginName())) {
					jecnUser.setLoginName(personBean.getLoginName());
					jecnUser.setTrueName(personBean.getTrueName());
					jecnUser.setEmail(personBean.getEmail());
					jecnUser.setPhoneStr(personBean.getPhone());
					jecnUser.setId(personBean.getId());
					listUpdate.add(jecnUser);
					flag = true;
					break;
				}
			}
			if (!flag) {
				JecnUser jecnUser = new JecnUser();
				jecnUser.setUserNumber(personBean.getUserNumber());
				jecnUser.setLoginName(personBean.getLoginName());
				jecnUser.setTrueName(personBean.getTrueName());
				jecnUser.setEmail(personBean.getEmail());
				jecnUser.setId(personBean.getId());
				jecnUser.setPhoneStr(personBean.getPhone());
				listNew.add(jecnUser);
			}
		}

		for (int i = 0; i < listJecnUser.size(); i++) {
			JecnUser jecnUser = listJecnUser.get(i);
			boolean flag = false;
			for (int j = 0; j < listUpdate.size(); j++) {
				if (jecnUser.getLoginName() != null
						&& listUpdate.get(j).getLoginName().equals(
								jecnUser.getLoginName())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				if (!ADMIN_ROLE.equals(jecnUser.getLoginName()))
					listdelete.add(jecnUser.getPeopleId());
			}
		}
		iXmpImportDAO.synchronizationPeoples(listUpdate, listdelete, listNew);
	}

	/**
	 * 将配置信息写入配置文件中
	 * 
	 * @param dataBean
	 */
	@Override
	public void writeConfigFile(DataBean dataBean) {
		XMLWriter xmlWriter = null;
		try {
			xmlWriter = new XMLWriter(new FileOutputStream(configFilePath));
			if (dataBean != null) {
				Element root = doc.getRootElement();
				List<Element> firstChildList = root.elements();
				if (firstChildList.size() < 0) {
					return;
				}
				for (Element firstChild : firstChildList) {
					List<Element> secendChildList = firstChild.elements();
					for (Element secendChild : secendChildList) {
						if (dataBean == null) {
							return;
						}
						if ("userFilePath".equals(secendChild.getName())) {
							secendChild.setText(dataBean.getUserFilePath());
						}
						if ("resultFilePath".equals(secendChild.getName())) {
							secendChild.setText(dataBean.getResultFilePath());
						}
						if ("startTime".equals(secendChild.getName())) {
							secendChild.setText(dataBean.getStartTime());
						}
						if ("day".equals(secendChild.getName())) {
							secendChild.setText(dataBean.getDay());
						}
						if ("iaAuto".equals(secendChild.getName())) {
							secendChild.setText(dataBean.getIaAuto());
						}
					}
				}
			}
			xmlWriter.write(doc);
		} catch (IOException e) {
			log.error("", e);
		}
		// System.out.println("写入xml完成!");
	}

	// 读取用户文件
	public void readUserFile() {
		System.out.println("read user file...");
		try {
			doc = JecnFinal.getDocumentByXml(userFilePath);
			if (!"".equals(doc) && doc != null) {
				Element root = doc.getRootElement();
				_iteratorChild(root);
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private void _iteratorChild(Element e) {
		List<Element> eList = e.elements();
		for (Element e1 : eList) {
			if (e1.getName().equals("Member")) {
				if (e1.attributeValue("Job_name") != null) {
					userInfBean.setUserPosition(e1.attributeValue("Job_name"));
				}
				if (e1.attributeValue("Staff_name") != null) {
					userInfBean.setTrueName(e1.attributeValue("Staff_name"));
				}
				StringBuffer sb = new StringBuffer();
				sb = getParent(e1, sb);
				// 封装到人员对象,并放入到List中,以备和数据库中人员数据对比
				// Staff_num(工号)作为唯一标识
				if (e1.attributeValue("Staff_num") != null) {
					listMap.put(e1.attributeValue("Staff_num"), sb.toString()
							+ e1.attributeValue("Job_name") + "-"
							+ e1.attributeValue("Staff_name"));
				}
				System.out.println(sb.toString()
						+ e1.attributeValue("Job_name") + "-"
						+ e1.attributeValue("Staff_name"));
				// userInfBean.getList().add(sb.toString() +
				// e1.attributeValue("Job_name") + "-" +
				// e1.attributeValue("Staff_name"));
			}
			if (e1.getName().equals("Dept")) {
				_iteratorChild(e1);
			}
		}
	}

	private StringBuffer getParent(Element e, StringBuffer sb) {
		Element parent = e.getParent();
		if (parent == null) {
			return sb;
		}
		if (parent.attributeValue("DeptName") != null) {
			sb.append(parent.attributeValue("DeptName") + "-");
			userInfBean.setDepartmentId(parent.attributeValue("DeptName"));
		}
		getParent(parent, sb);
		return sb;
	}

	// 进行匹配
	public void doImport() {
		System.out.println("开始匹配>>>>>>");
	}

	// // 秒表开始执行
	// public void timerStart() {
	// timer = new Timer();
	// String daysValue = null;
	// String timesValue = null;
	// String timeMinute = null;
	// String timeHour = null;
	// long timeValue = 0;// 时间间隔
	// Date date = new Date();
	//
	// if (doc != null && !"".equals(doc)) {
	// Element root = doc.getRootElement();
	// List<Element> list = root.elements();
	// if (list.size() < 0) {
	// return;
	// }
	// for (Element e : list) {
	// List<Element> childList = e.elements();
	// for (Element child : childList) {
	// if ("startTime".equals(child.getName())) {
	// timesValue = child.getTextTrim();
	// }
	// if ("day".equals(child.getName())) {
	// daysValue = child.getTextTrim();
	// }
	// }
	// }
	// String[] splitString = timesValue.split(":");
	// if (splitString.length > 0) {
	// timeHour = splitString[0];
	// if (splitString[0].equals("00")) {
	// timeHour = "24";
	// }
	// }
	// if (splitString.length > 1) {
	// timeMinute = splitString[1];
	// }
	// timeValue = Integer.parseInt(daysValue) * 24 * 3600;
	// // System.out.println("before task");
	// // 这是执行任务的类,即每隔一段时间要做的事情在这里
	// int timeMinutes = date.getHours() * 60 + date.getMinutes();
	// int valueMinutes = Integer.parseInt(timeHour) * 60
	// + Integer.parseInt(timeMinute);
	// if (timeMinutes > valueMinutes) {
	// timeValue = Math.abs(timeValue - (timeMinutes - valueMinutes)
	// * 60);
	// } else {
	// timeValue = Math.abs(timeValue + (valueMinutes - timeMinutes)
	// * 60);
	// }
	// if (timeValue != 0) {
	// // timer.schedule(new BugXmlTimerTask(), 0, timeValue *
	// // 1000);
	// BugXmlTimerTask bugXmlTimerTask = new BugXmlTimerTask();
	// bugXmlTimerTask.setDataImportBS(this);
	// timer.schedule(bugXmlTimerTask, 0, timeValue * 1000);
	// }
	//
	// }
	// // System.out.println("before task ************");
	// }

	public void timerStop() {
		if (timer != null)
			timer.cancel();
	}

	// 将匹配失败结果写入文件
	public void writeResultFile() throws IOException {
		List<Object[]> resultList = iXmpImportDAO.getUnMatchPosition();
		StringBuffer strBuf = new StringBuffer();
		if (resultList != null && resultList.size() > 0) {
			for (int i = 0; i < resultList.size(); i++) {
				Object[] objects = resultList.get(i);
				String deptName = "";
				if (objects[0] != null) {
					deptName = objects[0].toString();
				}
				strBuf.append(deptName + "-->");
				String posName = "";
				if (objects[1] != null) {
					posName = objects[1].toString();
				}
				strBuf.append(posName);
				if (i != (resultList.size() - 1)) {
					strBuf.append("\r\n");
				}
			}
			File f = new File(resultFilePath);
			if (!f.exists()) {
				f.createNewFile();
			}
			BufferedWriter output = new BufferedWriter(new FileWriter(f));
			output.write(strBuf.toString());
			output.close();
		}
	}

	/**
	 * 分析xml文件结构,并写入数据库
	 * 
	 * @param userFilePath
	 * @throws Exception
	 */
	@Override
	public void createXmlStructure(String userFilePath) throws Exception {
		List<XmlStructureBean> nodeList = new ArrayList<XmlStructureBean>();
		doc = JecnFinal.getDocumentByXml(userFilePath);
		if (!"".equals(doc) && doc != null) {
			iXmpImportDAO.clear();
			Element root = doc.getRootElement();
			XmlStructureBean rootNode = new XmlStructureBean();
			rootNode.setName(root.getName());
			rootNode.setPid(0L);
			rootNode.setFullName(root.getName());
			rootNode.setType(this.EM_TYPE_NODE);
			iXmpImportDAO.saveXmlStructureBean(rootNode);
			List<Attribute> attributes = root.attributes();
			for (Attribute attribute : attributes) {
				XmlStructureBean attributeBean = new XmlStructureBean();
				attributeBean.setName(attribute.getName());
				attributeBean.setPid(rootNode.getId());
				attributeBean.setFullName(rootNode.getName() + "<>"
						+ attribute.getName());
				attributeBean.setType(this.EM_TYPE_ATTR);
				iXmpImportDAO.saveXmlStructureBean(attributeBean);
			}
			nodeList.add(rootNode);
			_getXmlStructureChild(root, rootNode, nodeList);
		}
	}

	private void _getXmlStructureChild(Element root, XmlStructureBean rootNode,
			List<XmlStructureBean> nodeList) throws Exception {
		Iterator<Element> it = root.elementIterator();
		List<Element> eList = root.elements();
		for (Element e1 : eList) {
			XmlStructureBean node = new XmlStructureBean();
			node.setName(e1.getName());
			node.setPid(rootNode.getId());
			node.setFullName(e1.getName());
			node.setType(this.EM_TYPE_NODE);
			if (!nodeList.contains(node)) {
				iXmpImportDAO.saveXmlStructureBean(node);
				nodeList.add(node);
				List<Attribute> attributes = e1.attributes();
				for (Attribute attribute : attributes) {
					XmlStructureBean attributeBean = new XmlStructureBean();
					attributeBean.setName(attribute.getName());
					attributeBean.setPid(node.getId());
					attributeBean.setFullName(node.getName() + "<>"
							+ attribute.getName());
					attributeBean.setType(this.EM_TYPE_ATTR);
					iXmpImportDAO.saveXmlStructureBean(attributeBean);
				}
			}
			_getXmlStructureChild(e1, rootNode, nodeList);
		}
	}

	public void saveConfigInfo(XmlConfigBean xmlConfigBean,
			List<XmlStructureBean> xmlList) throws Exception {
		for (XmlStructureBean xsb : xmlList) {
			xsb.setColumnName("");
			iXmpImportDAO.updateXmlStructureBean(xsb);
		}
		if (xmlConfigBean.getUserNumberId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getUserNumberId() == xsb.getId()) {
					xsb.setColumnName(this.USER_NUMBER);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getLoginNameId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getLoginNameId() == xsb.getId()) {
					xsb.setColumnName(this.LOGIN_NAME);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getPasswordId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getPasswordId() == xsb.getId()) {
					xsb.setColumnName(this.PASSWORD);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getTrueNameId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getTrueNameId() == xsb.getId()) {
					xsb.setColumnName(this.TRUE_NAME);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getPhoneId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getPhoneId() == xsb.getId()) {
					xsb.setColumnName(this.PHONE);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getEmailId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getEmailId() == xsb.getId()) {
					xsb.setColumnName(this.EMAIL);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getPositionId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getPositionId() == xsb.getId()) {
					xsb.setColumnName(this.POSITION_ID);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getPositionNameId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getPositionNameId() == xsb.getId()) {
					xsb.setColumnName(this.POSITION_NAME);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getDeptId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getDeptId() == xsb.getId()) {
					xsb.setColumnName(this.DEPT_ID);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
		if (xmlConfigBean.getDeptNameId() != null) {
			for (XmlStructureBean xsb : xmlList) {
				if (xmlConfigBean.getDeptNameId() == xsb.getId()) {
					xsb.setColumnName(this.DEPT_NAME);
					iXmpImportDAO.updateXmlStructureBean(xsb);
				}
			}
		}
	}

	public XmlConfigBean getXmlConfigInfo(List<XmlStructureBean> xmlList) {
		XmlConfigBean xcb = new XmlConfigBean();
		for (XmlStructureBean xsb : xmlList) {
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.USER_NUMBER)) {
				xcb.setUserNumberId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.LOGIN_NAME)) {
				xcb.setLoginNameId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.PASSWORD)) {
				xcb.setPasswordId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.TRUE_NAME)) {
				xcb.setTrueNameId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.PHONE)) {
				xcb.setPhoneId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.EMAIL)) {
				xcb.setEmailId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.POSITION_ID)) {
				xcb.setPositionId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.POSITION_NAME)) {
				xcb.setPositionNameId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.DEPT_ID)) {
				xcb.setDeptId(xsb.getId());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.DEPT_NAME)) {
				xcb.setDeptNameId(xsb.getId());
			}
		}
		return xcb;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<XmlStructureBean> getXmlStructure() throws Exception {
		return iXmpImportDAO.getAllXmlStructure();
	}

	private String getNodeByAttrPid(Long attrPId, List<XmlStructureBean> xmlList) {
		for (XmlStructureBean xsb : xmlList) {
			if (xsb.getId() == attrPId.longValue()) {
				return xsb.getName();
			}
		}
		return null;
	}

	// 解析节点信息
	private MyNode getMyNode(List<XmlStructureBean> xmlList) {
		MyNode myNode = new MyNode();
		for (XmlStructureBean xsb : xmlList) {
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.USER_NUMBER)) {
				myNode
						.setUserNumberNode(getNodeByAttrPid(xsb.getPid(),
								xmlList));
				myNode.setUserNumberAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.LOGIN_NAME)) {
				myNode
						.setLoginNameNode(getNodeByAttrPid(xsb.getPid(),
								xmlList));
				myNode.setLoginNameAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.PASSWORD)) {
				myNode.setPasswordNode(getNodeByAttrPid(xsb.getPid(), xmlList));
				myNode.setPasswordAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.TRUE_NAME)) {
				myNode.setTrueNameNode(getNodeByAttrPid(xsb.getPid(), xmlList));
				myNode.setTrueNameAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.PHONE)) {
				myNode.setPhoneNode(getNodeByAttrPid(xsb.getPid(), xmlList));
				myNode.setPhoneAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.EMAIL)) {
				myNode.setEmailNode(getNodeByAttrPid(xsb.getPid(), xmlList));
				myNode.setEmailAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.POSITION_ID)) {
				myNode
						.setPositionIdNode(getNodeByAttrPid(xsb.getPid(),
								xmlList));
				myNode.setPositionIdAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.POSITION_NAME)) {
				myNode.setPositionNameNode(getNodeByAttrPid(xsb.getPid(),
						xmlList));
				myNode.setPositionNameAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.DEPT_ID)) {
				myNode.setDeptIdNode(getNodeByAttrPid(xsb.getPid(), xmlList));
				myNode.setDeptIdAttr(xsb.getName());
			}
			if (xsb.getColumnName() != null
					&& xsb.getColumnName().equals(this.DEPT_NAME)) {
				myNode.setDeptNameNode(getNodeByAttrPid(xsb.getPid(), xmlList));
				myNode.setDeptNameAttr(xsb.getName());
			}
		}

		return myNode;
	}

	// 保存xml数据
	public boolean saveXmlData(String userFilePath) throws Exception,
			DocumentException, IOException, Exception {
		List<XmlStructureBean> xmlList = getXmlStructure();
		MyNode myNode = getMyNode(xmlList);

		// 从指定地址下载文件
		saveFile(userFilePath, destFile);
		doc = JecnFinal.getDocumentByXml(destFile);
		if (!"".equals(doc) && doc != null) {
			deptList = new ArrayList<DepartmentBean>();
			Element root = doc.getRootElement();
			DepartmentBean departmentBean = new DepartmentBean();
			departmentBean.setDeptId(root.attributeValue("id"));
			departmentBean.setDeptName(root.attributeValue("name"));
			departmentBean.setPid(0L);
			deptList.add(departmentBean);

			PositionBean positionBean = new PositionBean();
			saveXmlDataIterator(root, departmentBean, positionBean, myNode);
		}

		writeResultFile();
		// 保存到数据库
		return iXmpImportDAO.saveDepartmentBean(deptList);
	}

	private void saveXmlDataIterator(Element e, DepartmentBean departmentBean,
			PositionBean positionBean, MyNode myNode) throws Exception {
		List<Element> eList = e.elements();
		for (Element e1 : eList) {
			// if (e1.getName().equals("Dept")) {
			// departmentBean = new DepartmentBean();
			// departmentBean.setDeptId(e1.attributeValue("ID"));
			// departmentBean.setDeptName(e1.attributeValue("DeptName"));
			// departmentBean.setParentDeptId(e.attributeValue("ID"));
			// deptList.add(departmentBean);
			// }
			// if (e1.getName().equals("Member")) {
			// PositionBean positionBean = new PositionBean();
			// positionBean.setPosName(e1.attributeValue("Job_name"));
			// departmentBean.getPositionList().add(positionBean);
			// PersonBean personBean = new PersonBean();
			// personBean.setLoginName(e1.attributeValue("Staff_num"));
			// personBean.setTrueName(e1.attributeValue("Staff_name"));
			// positionBean.getPersonList().add(personBean);
			// }
			if (e1.getName().equals(myNode.getDeptIdNode())) {
				departmentBean = new DepartmentBean();
				departmentBean.setDeptId(e1.attributeValue(myNode
						.getDeptIdAttr()));
				departmentBean.setDeptName(e1.attributeValue(myNode
						.getDeptNameAttr()));
				departmentBean.setParentDeptId(e.attributeValue(myNode
						.getDeptIdAttr()));
				deptList.add(departmentBean);
			}

			if (e1.getName().equals(myNode.getPositionNameNode())) {
				positionBean = new PositionBean();
				positionBean.setPosName(e1.attributeValue(myNode
						.getPositionNameAttr()));
				departmentBean.getPositionList().add(positionBean);
			}
			if (e1.getName().equals(myNode.getLoginNameNode())) {
				PersonBean personBean = new PersonBean();
				personBean.setLoginName(e1.attributeValue(myNode
						.getLoginNameAttr()));
				personBean.setTrueName(e1.attributeValue(myNode
						.getTrueNameAttr()));
				positionBean.getPersonList().add(personBean);
			}
			saveXmlDataIterator(e1, departmentBean, positionBean, myNode);
		}
	}

	/**
	 * XML岗位匹配根据部门主键ID获取部门下岗位信息
	 * 
	 * @param orgId
	 *            部门ID
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getPosByOrgId(Long orgId) throws Exception {
		String sql = "select t.POS_ID,t.POS_NAME,t.DEPT_ID,jfo.DEPT_NAME"
				+ "        from JECN_DATA_POSITION t,JECN_DATA_DEPARTMENT jfo where t.DEPT_ID=jfo.DEPT_ID"
				+ "        and t.DEPT_ID=? order by t.DEPT_ID";
		return iXmpImportDAO.listNativeSql(sql, orgId);

	}

	/**
	 * XML岗位匹配根据部门主键ID获取部门下岗位信息
	 * 
	 * @param id
	 * @param isLeaf
	 * @return
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public List<JecnTreeBean> getChildOrgsAndPositon(Long id, boolean isLeaf)
			throws Exception {
		String sql = "select t.DEPT_ID,t.DEPT_NAME,t.PID,"
				+ "       (select count(*) from JECN_DATA_DEPARTMENT where PID=t.DEPT_ID ) as org_count,"
				+ "       (select count(*) from JECN_DATA_POSITION where DEPT_ID=t.DEPT_ID ) as pos_count"
				+ "       from JECN_DATA_DEPARTMENT t where t.PID=? and t.projectid=?  order by t.PID,t.org_id";
		
		//String sql="select t.id,t.name,t.per_id,t.is_dir,case when t.is_dir=1 then (select count(*) from jecn_position_group where per_id=t.id when t.is_dir=0 then (select count(jr.figure_id) from JECN_POSITION_GROUP_R jr,Jecn_Flow_Org_Image jfom,Jecn_Flow_Org jfo where jr.figure_id=jfom.figure_id and jfom.org_id=jfo.org_id and jfo.del_state=0 and jr.group_id=t.id) end count	from jecn_position_group t where t.project_id=? order by t.per_id,t.sort_id,t.id";
		
		// 组织
		List<Object[]> listOrg = organizationDao.listNativeSql(sql, id);
		// 岗位
		List<Object[]> listPos = getPosByOrgId(id);

		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listOrg) {
			JecnTreeBean jecnTreeBean = JecnUtil
					.getJecnTreeBeanByObjectOrg(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		for (Object[] obj : listPos) {
			JecnTreeBean jecnTreeBean = JecnUtil
					.getJecnTreeBeanByObjectPos(obj);
			if (jecnTreeBean != null) {
				if (isLeaf) {
					if (obj[4] != null
							&& Integer.parseInt(obj[4].toString()) > 0) {
						jecnTreeBean.setChildNode(true);
					} else {
						jecnTreeBean.setChildNode(false);
					}
				} else {
					jecnTreeBean.setChildNode(false);
				}
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	//
	// private List<PositionBean> findPositionsByOrgId(List<PositionBean> list,
	// String orgId) {
	// List<PositionBean> positionsList = new ArrayList<PositionBean>();
	// for (int i = 0; i < list.size(); i++) {
	// PositionBean positionBean = list.get(i);
	// if (positionBean.getDeptId() != null
	// && orgId.equals(positionBean.getDeptId().toString())) {
	// positionsList.add(positionBean);
	// }
	// }
	// return positionsList;
	// }

	/**
	 * 把获取的xml文件保存到本地
	 * 
	 * @param destUrl
	 * @param fileName
	 * @throws IOException
	 */
	private void saveFile(String destUrl, String fileName) throws IOException {
		int BUFFER_SIZE = 8096;// 缓冲区大小
		FileOutputStream fos = null;
		BufferedInputStream bis = null;
		HttpURLConnection httpUrl = null;
		URL url = null;
		byte[] buf = new byte[BUFFER_SIZE];
		int size = 0;
		// 建立链接
		url = new URL(destUrl);
		httpUrl = (HttpURLConnection) url.openConnection();
		// 连接指定的资源
		httpUrl.connect();
		// 获取网络输入流
		bis = new BufferedInputStream(httpUrl.getInputStream());
		// 建立文件
		fos = new FileOutputStream(fileName);
		// 保存文件
		while ((size = bis.read(buf)) != -1)

			fos.write(buf, 0, size);
		fos.close();
		bis.close();
		httpUrl.disconnect();
	}

	public void sameNamesExcel() {
		InputStream is;
		Workbook rwb;
		excelFileList = new ArrayList<ExcelConfigBean>();
		orgExcelList = new ArrayList<OrgExcelBean>();
		try {
			is = new FileInputStream(userFilePath);
			rwb = Workbook.getWorkbook(is);
			Sheet[] sheets = rwb.getSheets();
			for (int i = 1; i < sheets[1].getRows(); i++) {
				ExcelConfigBean excelConfigBean = new ExcelConfigBean();
				excelConfigBean.setUerNumberId(sheets[1].getCell(0, i)
						.getContents().toString());
				excelConfigBean.setUserName(sheets[1].getCell(1, i)
						.getContents().toString());
				excelConfigBean.setStationNumberId(sheets[1].getCell(2, i)
						.getContents().toString());
				excelConfigBean.setStationName(sheets[1].getCell(3, i)
						.getContents().toString());
				excelConfigBean.setOrgNumberId(sheets[1].getCell(4, i)
						.getContents().toString());
				excelConfigBean.setOrgName(sheets[1].getCell(5, i)
						.getContents().toString());
				excelConfigBean.setEmailAdress(sheets[1].getCell(6, i)
						.getContents().toString());
				excelConfigBean.setTelephone(sheets[1].getCell(7, i)
						.getContents().toString());
				excelConfigBean.setEmailType(sheets[1].getCell(8, i)
						.getContents().toString());
				excelFileList.add(excelConfigBean);
			}
		} catch (Exception e) {
			log.error("", e);
		}

	}

	/**
	 * Excel文件读取
	 * 
	 * @return
	 */
	@Override
	public boolean importExcel() {
		InputStream is;
		Workbook rwb;
		excelFileList = new ArrayList<ExcelConfigBean>();
		orgExcelList = new ArrayList<OrgExcelBean>();
		try {
			is = new FileInputStream(userFilePath);
			rwb = Workbook.getWorkbook(is);
			Sheet[] sheets = rwb.getSheets();
			// for (int j = 0; j < sheets.length; j++) {
			// int rows = sheets[0].getRows();
			// if ("人员编号".equals(sheets[1].getCell(0,
			// 0).getContents().toString())
			// && "人员名称".equals(sheets[0].getCell(1, 0).getContents()
			// .toString())
			// && "岗位编号".equals(sheets[0].getCell(2, 0).getContents()
			// .toString())
			// && "岗位名称".equals(sheets[0].getCell(3, 0).getContents()
			// .toString())
			// && "组织编号".equals(sheets[0].getCell(4, 0).getContents()
			// .toString())
			// && "组织名称".equals(sheets[0].getCell(5, 0).getContents()
			// .toString())) {
			loginNames = new ArrayList<String>();
			XmlContants.sameNames.clear();
			for (int i = 1; i < sheets[1].getRows(); i++) {
				ExcelConfigBean excelConfigBean = new ExcelConfigBean();
				if (sheets[1].getCell(0, i).getContents() != null) {
					excelConfigBean.setUerNumberId(sheets[1].getCell(0, i)
							.getContents().toString());// 人员编号
				}
				if (sheets[1].getCell(1, i).getContents() != null) {
					excelConfigBean.setUserName(sheets[1].getCell(1, i)
							.getContents().toString());// 真实姓名
				}
				if (sheets[1].getCell(2, i).getContents() != null) {
					excelConfigBean.setStationNumberId(sheets[1].getCell(2, i)
							.getContents().toString());// 任职岗位编号
				}
				if (sheets[1].getCell(3, i).getContents() != null) {
					excelConfigBean.setStationName(sheets[1].getCell(3, i)
							.getContents().toString());
				}
				if (sheets[1].getCell(4, i).getContents() != null) {
					excelConfigBean.setOrgNumberId(sheets[1].getCell(4, i)
							.getContents().toString());// 岗位所属部门编号
				}
				if (sheets[1].getCell(5, i).getContents() != null) {
					excelConfigBean.setOrgName(sheets[1].getCell(5, i)
							.getContents().toString());
				}
				if (sheets[1].getCell(6, i).getContents() != null) {
					excelConfigBean.setEmailAdress(sheets[1].getCell(6, i)
							.getContents().toString());
				}
				if (sheets[1].getCell(7, i).getContents() != null) {
					excelConfigBean.setTelephone(sheets[1].getCell(7, i)
							.getContents().toString());
				}
				if (sheets[1].getCell(8, i).getContents() != null) {
					excelConfigBean.setEmailType(sheets[1].getCell(8, i)
							.getContents().toString());
				}
				// excelConfigBean.setPreOrgNumberId(sheets[0].getCell(6,
				// i).getContents().toString());
				// loginNames.add(getPinYin(excelConfigBean.getUserName()));
				excelFileList.add(excelConfigBean);
			}
			// }
			// if ("组织编号".equals(sheets[1].getCell(1,
			// 0).getContents().toString())
			// && "组织名称".equals(sheets[1].getCell(2, 0).getContents()
			// .toString())
			// && "上级部门编号".equals(sheets[1].getCell(3, 0).getContents()
			// .toString())) {
			for (int i = 1; i < sheets[0].getRows(); i++) {
				OrgExcelBean orgExcelBean = new OrgExcelBean();
				if (sheets[0].getCell(0, i).getContents() != null) {
					orgExcelBean.setOrgNumberId(sheets[0].getCell(0, i)
							.getContents().toString());
				}
				if (sheets[0].getCell(1, i).getContents() != null) {
					orgExcelBean.setOrgName(sheets[0].getCell(1, i)
							.getContents().toString());
				}
				if (sheets[0].getCell(2, i).getContents() != null) {
					orgExcelBean.setPreOrgNumberId(sheets[0].getCell(2, i)
							.getContents().toString());
				}
				if (!"".equals(orgExcelBean.getOrgName())
						&& orgExcelBean.getOrgNumberId() != null
						&& !orgExcelBean.getOrgNumberId().equals(
								orgExcelBean.getPreOrgNumberId())) {
					orgExcelList.add(orgExcelBean);
				}
			}
			if (orgExcelList.size() > 0 || excelFileList.size() > 0) {
				return true;
			}
		} catch (FileNotFoundException e) {
			log.error("", e);
		} catch (BiffException e) {
			log.error("", e);
		} catch (IOException e) {
			log.error("", e);
		}
		return false;
	}

	private List<JecnFlowOrg> findAllOrg(Long projectId) {
		// 获取所有部门
		String queryString = "from JecnFlowOrg where projectId=?  order by preOrgId,sortId,orgId";
		List<JecnFlowOrg> listOrgAlls = iXmpImportDAO.listHql(queryString,
				projectId);
		return listOrgAlls;

	}

	/**
	 * 获取所有岗位
	 * 
	 * @param projectId
	 * @return
	 */
	private List<JecnFlowOrgImage> findAllOrgImage(Long projectId) {
		// 获取所有部门
		String queryString = "from JecnFlowOrgImage where figureType='PositionFigure' order by orgId,sortId";
		List<JecnFlowOrgImage> listOrgAlls = iXmpImportDAO.listHql(queryString,
				projectId);
		return listOrgAlls;
	}

	/**
	 * 所有用户
	 * 
	 * @return
	 */
	private List<JecnUser> findAllJecnUser() {
		// 获取所有部门
		String queryString = "from JecnUser";
		List<JecnUser> listOrgAlls = iXmpImportDAO.listHql(queryString);
		return listOrgAlls;
	}

	/**
	 * 人员岗位关联表s
	 * 
	 * @return
	 */
	private List<JecnUserInfo> findAllJecnUserInfo() {
		// 获取所有部门
		String queryString = "from JecnUserInfo";
		List<JecnUserInfo> listOrgAlls = iXmpImportDAO.listHql(queryString);
		return listOrgAlls;
	}

	/**
	 * 组织匹配
	 * 
	 * @param projectId
	 * @return
	 */
	@Override
	public boolean saveOrg(String projectId) {
		try {
			// 获取所有部门
			List<JecnFlowOrg> listOrgAlls = findAllOrg(Long.valueOf(projectId));
			List<JecnFlowOrg> listNewJecnFlowOrg = new ArrayList<JecnFlowOrg>();
			// 组织
			for (int i = 0; i < orgExcelList.size(); i++) {
				OrgExcelBean orgExcelBean = orgExcelList.get(i);
				String orgId = orgExcelBean.getOrgNumberId();
				String pOrgId = orgExcelBean.getPreOrgNumberId();
				if (orgId == null || "".equals(orgId)) {
					continue;
				}
				if (pOrgId == null || "".equals(pOrgId)) {
					continue;
				}
				boolean orgIdFalse = false;
				for (int j = 0; j < listNewJecnFlowOrg.size(); j++) {
					JecnFlowOrg jecnFlowOrg = listNewJecnFlowOrg.get(j);
					String newOrgId = jecnFlowOrg.getOrgNumber();
					if (newOrgId != null && !"".equals(newOrgId)
							&& orgId.equals(newOrgId)) {
						orgIdFalse = true;
						break;
					}
				}
				if (!orgIdFalse) {
					JecnFlowOrg jecnFlowOrg = new JecnFlowOrg();
					jecnFlowOrg.setOrgNumber(orgId);
					jecnFlowOrg.setOrgName(orgExcelBean.getOrgName());
					jecnFlowOrg.setPreOrgNumber(pOrgId);
					jecnFlowOrg.setDelState(0L);
					listNewJecnFlowOrg.add(jecnFlowOrg);
				}
			}

			if (listOrgAlls.size() == 0) {
				if (listNewJecnFlowOrg.size() > 0) {
					iXmpImportDAO.saveNoDataOrgs(listNewJecnFlowOrg, Long
							.valueOf(projectId));
				}
				return true;
			}
			getTreeFlowOrg(listNewJecnFlowOrg);
			iXmpImportDAO.updateOrg(listOrgAlls, treeList, Long
					.valueOf(projectId));
			return true;

		} catch (Exception e) {
			log.error("", e);
		}
		return false;
	}

	// 得到树的根节点
	public void getTreeFlowOrg(List<JecnFlowOrg> listNewJecnFlowOrg) {
		List<JecnFlowOrg> treeFlowOrg = new ArrayList<JecnFlowOrg>();
		treeList = new ArrayList<JecnFlowOrg>();
		for (JecnFlowOrg jecnOrg : listNewJecnFlowOrg) {
			if (jecnOrg.getPreOrgNumber() != null
					&& !"".equals(jecnOrg.getPreOrgNumber())
					&& "0".equals(jecnOrg.getPreOrgNumber())) {
				treeFlowOrg.add(jecnOrg);
				treeList.add(jecnOrg);
			}
		}
		if (treeFlowOrg.size() > 0)
			getTreeFlowOrg(listNewJecnFlowOrg, treeFlowOrg);
	}

	// 桉树的排列顺序存储List
	public void getTreeFlowOrg(List<JecnFlowOrg> listNewJecnFlowOrg,
			List<JecnFlowOrg> treeFlowOrg) {
		List<JecnFlowOrg> newTreeList = new ArrayList<JecnFlowOrg>();
		for (JecnFlowOrg jecnFlowOrg : listNewJecnFlowOrg) {
			for (JecnFlowOrg jecn : treeFlowOrg) {
				if (jecnFlowOrg.getPreOrgNumber() != null
						&& jecnFlowOrg.getPreOrgNumber().equals(
								jecn.getOrgNumber())) {
					newTreeList.add(jecnFlowOrg);
					treeList.add(jecnFlowOrg);
				}
			}

		}
		if (newTreeList.size() > 0) {
			getTreeFlowOrg(listNewJecnFlowOrg, newTreeList);
		}
	}

	/**
	 * 岗位以及人员匹配
	 * 
	 * @param projectId
	 * @throws Exception
	 */
	@Override
	public void excelRead(String projectId) throws Exception {
		// 获取所有部门
		Long proId = Long.valueOf(projectId);
		List<JecnFlowOrg> listOrgAlls = findAllOrg(Long.valueOf(proId));// 组织
		List<JecnFlowOrgImage> getStationsList = findAllOrgImage(proId);// 岗位
		List<JecnUser> listJecnUser = findAllJecnUser();// 用户
		List<JecnUserInfo> listJecnUserInfo = findAllJecnUserInfo();// 用户和岗位的关联表
		List<JecnFlowOrgImage> listNewJecnFlowOrgImage = new ArrayList<JecnFlowOrgImage>();
		List<JecnUser> listNewJecnUser = new ArrayList<JecnUser>();
		List<StationAndPeopleRelate> listStationAndPeopleRelate = new ArrayList<StationAndPeopleRelate>();
		for (int i = 0; i < excelFileList.size(); i++) {
			ExcelConfigBean excelConfigBean = excelFileList.get(i);
			String numberId = excelConfigBean.getUerNumberId();
			String stationId = excelConfigBean.getStationNumberId();
			String orgId = excelConfigBean.getOrgNumberId();
			StationAndPeopleRelate stationAndPeopleRelate = new StationAndPeopleRelate();
			stationAndPeopleRelate.setPeopleIdNumber(numberId);
			stationAndPeopleRelate.setFigureIdNumber(stationId);
			listStationAndPeopleRelate.add(stationAndPeopleRelate);
			boolean stationIdFalse = false;
			for (int j = 0; j < listNewJecnFlowOrgImage.size(); j++) {
				JecnFlowOrgImage jecnFlowOrgImage = listNewJecnFlowOrgImage
						.get(j);
				String newStationId = jecnFlowOrgImage.getFigureNumberId();
				if (newStationId != null && !"".equals(newStationId)
						&& stationId.equals(newStationId)) {
					stationIdFalse = true;
					break;
				}
			}
			if (!stationIdFalse) {
				if (stationId != null && !"".equals(stationId)) {
					JecnFlowOrgImage jecnFlowOrgImage = new JecnFlowOrgImage();
					jecnFlowOrgImage.setFigureNumberId(stationId);
					jecnFlowOrgImage.setFigureText(excelConfigBean
							.getStationName());
					jecnFlowOrgImage.setOrgNumberId(orgId);
					// 岗位标识
					jecnFlowOrgImage.setFigureType("PositionFigure");
					listNewJecnFlowOrgImage.add(jecnFlowOrgImage);
				}
			}
			boolean peopleIdFalse = false;
			for (int j = 0; j < listNewJecnUser.size(); j++) {
				JecnUser jecnUser = listNewJecnUser.get(j);
				String newNumberId = jecnUser.getUserNumber();
				if (newNumberId != null && !"".equals(newNumberId)
						&& numberId.equals(newNumberId)) {
					stationIdFalse = true;
					break;
				}
			}
			if (!peopleIdFalse) {
				if (numberId != null && !"".equals(numberId)) {
					JecnUser jecnUser = new JecnUser();
					jecnUser.setUserNumber(numberId);
					jecnUser.setTrueName(excelConfigBean.getUserName());
					jecnUser.setPassword("CCC5C9CAC0CD");// 密码123456
					jecnUser.setIsLock(Long.valueOf("0"));
					if (JecnCommon.checkMailFormat(excelConfigBean
							.getEmailAdress())) {
						jecnUser.setEmail(excelConfigBean.getEmailAdress());
					}
					jecnUser.setEmailType(excelConfigBean.getEmailType());
					if (excelConfigBean.getTelephone() != null
							&& !"".equals(excelConfigBean.getTelephone())) {
						// jecnUser.setPhone(Long.valueOf(excelConfigBean.getTelephone()));
						jecnUser.setPhoneStr(excelConfigBean.getTelephone());
					}
					// if
					// (!loginNames.contains(getPinYin(excelConfigBean
					// .getUserName()))) {
					// jecnUser.setLoginName(getPinYin(excelConfigBean
					// .getUserName()));
					// loginNames.add(getPinYin(excelConfigBean
					// .getUserName()));
					// listNewJecnUser.add(jecnUser);
					// } else {
					// XmlContants.sameNames.add(excelConfigBean);
					// }
					if (!loginNames.contains(excelConfigBean.getUerNumberId())) {// 人员编号设置问登陆名称
						jecnUser.setLoginName(excelConfigBean.getUerNumberId());
						loginNames.add(excelConfigBean.getUerNumberId());
						listNewJecnUser.add(jecnUser);
					} else {
						XmlContants.sameNames.add(excelConfigBean);
					}
				}
			}
		}
		iXmpImportDAO.updateUserFH(listOrgAlls, getStationsList, listJecnUser,
				listJecnUserInfo, listNewJecnFlowOrgImage, listNewJecnUser,
				listStationAndPeopleRelate, Long.valueOf(projectId));
	}

	/**
	 * 将汉字转换为全拼
	 * 
	 * @param src
	 * @return
	 */
	public String getPinYin(String src) {
		char[] t1 = null;
		t1 = src.toCharArray();
		String[] t2 = new String[t1.length];
		HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();
		t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		t3.setVCharType(HanyuPinyinVCharType.WITH_V);
		String t4 = "";
		int t0 = t1.length;
		try {
			for (int i = 0; i < t0; i++) {
				// 判断是否为汉字字符
				if (java.lang.Character.toString(t1[i]).matches(
						"[\\u4E00-\\u9FA5]+")) {
					t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);
					t4 += t2[0];
				} else
					t4 += java.lang.Character.toString(t1[i]);
			}
			return t4;
		} catch (BadHanyuPinyinOutputFormatCombination e1) {
			log.error("", e1);
		}
		return t4;
	}

	/**
	 * 写入Excel
	 * 
	 * @param fileName
	 * @throws WriteException
	 */
	@Override
	public void writeExcel(String fileName) throws WriteException {
		WritableWorkbook wwb = null;
		try {
			// 首先要使用Workbook类的工厂方法创建一个可写入的工作薄(Workbook)对象
			wwb = Workbook.createWorkbook(new File(fileName));
		} catch (IOException e) {
			log.error("", e);
		}
		int lines = 0;
		if (XmlContants.sameNames != null && XmlContants.sameNames.size() > 0) {
			lines = XmlContants.sameNames.size();
		} else {
			return;
		}
		if (wwb != null) {
			// 创建一个可写入的工作表
			// Workbook的createSheet方法有两个参数,第一个是工作表的名称,第二个是工作表在工作薄中的位置
			WritableSheet ws = wwb.createSheet("sheet1", 0);

			// 下面开始添加单元格
			for (int i = 0; i < lines; i++) {
				Label labelC1 = new Label(0, i, XmlContants.sameNames.get(i)
						.getUerNumberId());
				Label labelC2 = new Label(1, i, XmlContants.sameNames.get(i)
						.getUserName());
				Label labelC3 = new Label(2, i, XmlContants.sameNames.get(i)
						.getStationNumberId());
				Label labelC4 = new Label(3, i, XmlContants.sameNames.get(i)
						.getStationName());
				Label labelC5 = new Label(4, i, XmlContants.sameNames.get(i)
						.getOrgNumberId());
				Label labelC6 = new Label(5, i, XmlContants.sameNames.get(i)
						.getOrgName());
				// Label labelC7 = new Label(6, i,
				// XmlContants.sameNames.get(i).get());
				// for (int j = 0; j < 5; j++) {
				// // 这里需要注意的是,在Excel中,第一个参数表示列,第二个表示行
				// Label labelC = new Label(j, i, "这是第" + (i + 1) + "行,第"
				// + (j + 1) + "列");
				try {
					// // 将生成的单元格添加到工作表中
					ws.addCell(labelC1);
					ws.addCell(labelC2);
					ws.addCell(labelC3);
					ws.addCell(labelC4);
					ws.addCell(labelC5);
					ws.addCell(labelC6);
				} catch (RowsExceededException e) {
					log.error("", e);
				} catch (WriteException e) {
					log.error("", e);
				}
				//
				// }
			}

			try {
				// 从内存中写入文件中
				wwb.write();
				// 关闭资源,释放内存
				wwb.close();
			} catch (IOException e) {
				log.error("", e);
			}
		}
	}
}
