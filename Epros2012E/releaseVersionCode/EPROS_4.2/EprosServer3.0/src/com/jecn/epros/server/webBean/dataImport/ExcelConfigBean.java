package com.jecn.epros.server.webBean.dataImport;

/*
 * 人员岗位表
 */
public class ExcelConfigBean {
	private String uerNumberId;// 人员编号
	private String userName;// 人员名称
	private String stationNumberId;// 岗位编号
	private String stationName;// 岗位名称
	private String orgNumberId;// 组织编号
	private String orgName;// 组织名称
	private String emailAdress;// 组织编号
	private String telephone;// 电话号码
	private String emailType;// 内外网表示,内网inner,外网outer

	public String getUerNumberId() {
		return uerNumberId;
	}

	public void setUerNumberId(String uerNumberId) {
		this.uerNumberId = uerNumberId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStationNumberId() {
		return stationNumberId;
	}

	public void setStationNumberId(String stationNumberId) {
		this.stationNumberId = stationNumberId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getOrgNumberId() {
		return orgNumberId;
	}

	public void setOrgNumberId(String orgNumberId) {
		this.orgNumberId = orgNumberId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getEmailAdress() {
		return emailAdress;
	}

	public void setEmailAdress(String emailAdress) {
		this.emailAdress = emailAdress;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
}
