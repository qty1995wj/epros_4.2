package com.jecn.webservice.zhongsh.rule;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import com.jecn.webservice.zhongsh.rule.download.SmisFileService;
import com.jecn.webservice.zhongsh.rule.download.SmisFileServiceSoap;
import com.jecn.webservice.zhongsh.rule.query.SmisSearchService;
import com.jecn.webservice.zhongsh.rule.query.SmisSearchServiceSoap;

public class ShiHuaRuleDownloadTest {

	private static final QName SERVICE_NAME = new QName("http://tempuri.org/", "SmisFileService");

	static SmisFileService ss = new SmisFileService(SmisFileService.WSDL_LOCATION, SERVICE_NAME);
	static SmisFileServiceSoap port = ss.getSmisFileServiceSoap();

	static String username = "zhangtl66";
	static String password = "123456";

	static String storedDirPath = "E:/yingke/";

	public static void main(String args[]) throws java.lang.Exception {
//		downloadSystemByDocId();
		// GetDocumentBySystemId();
		DownloadSystemDocumentBySystemId();
	}
	
	private static void DownloadSystemDocumentBySystemId() throws Exception {
		String bytes = port.getDocumentBySystemId(username, password, "159233");
		createFile(storedDirPath + "/download/", "DownloadSystemDocumentBySystemId_zip.rar", bytes);
	}

	private static void GetDocumentBySystemId() throws Exception {
		String bytes = port.getDocumentBySystemId(username, password, "159233");
		createFile(storedDirPath + "/download/", "GetDocumentBySystemId.xml", bytes);
	}

	private static void downloadSystemByDocId() throws Exception {
		byte[] bytes = port.downloadSystemByDocId(username, password, "693513");
		createFile(storedDirPath + "/download/", "downloadSystemByDocId_2_693513.doc", bytes);
	}

	private static void createFile(String dirPath, String fileName, String data) throws Exception {
		createFile(dirPath, fileName, data.getBytes());
	}

	private static void createFile(String dirPath, String fileName, byte[] data) throws Exception {
		File dir = new File(dirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File f = new File(dirPath + fileName);
		f.delete();
		f.createNewFile();
		OutputStream stream = new FileOutputStream(f);
		stream.write(data);
		stream.flush();
		stream.close();
	}

}
