package epros.designer.gui.rule;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleFileT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

public class RuleFilePanel extends JecnFileDescriptionTable {
	// 文件选择按钮初始化
	protected JButton selectBut = new JButton(JecnProperties.getValue("select"));
	private static Logger log = Logger.getLogger(RuleFilePanel.class);
	private List<JecnTreeBean> listFile = new ArrayList<JecnTreeBean>();
	private List<RuleFileT> listFileBean = new ArrayList<RuleFileT>();
	protected List<Long> listIds = new ArrayList<Long>();
	private Long titleId;

	public RuleFilePanel(int index, String name, JecnPanel contentPanel, List<RuleFileT> listFileBean, Long titleId,
			boolean isRequest) {
		super(index, name, isRequest, contentPanel);
		if (listFileBean != null) {
			this.listFileBean = listFileBean;
		}
		this.titleId = titleId;
		initData();
		titlePanel.add(selectBut);
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				select();
			}
		});
		initTable();
	}
	
	public void setButtonHidden(){
		selectBut.setVisible(false);
	}

	private void initData() {
		// 编辑“制度操作说明” 文件显示
		if (listFileBean != null && listFileBean.size() > 0) {
			Set<Long> ids = new HashSet<Long>();
			// 附件
			for (RuleFileT ruleFileT : listFileBean) {
				ids.add(ruleFileT.getRuleFileId());
			}
			if (ids.size() > 0) {
				try {
					listFile = ConnectionPool.getFileAction().getJecnTreeBeanByIds(ids);
				} catch (Exception e) {
					log.error("RuleFilePanel method initData error", e);
				}
			}
		}
	}

	public List<RuleFileT> getResultList() {
		List<RuleFileT> listRuleFileT = new ArrayList<RuleFileT>();
		int fileRow = getTable().getRowCount();
		for (int i = 0; i < fileRow; i++) {
			Long fileId = Long.valueOf(getTable().getValueAt(i, 0).toString());
			RuleFileT ruleFileT = getRuleFileT(fileId);
			if (ruleFileT == null) {
				ruleFileT = new RuleFileT();
				if (getTable().getValueAt(i, 1) != null) {
					ruleFileT.setFileInput(getTable().getValueAt(i, 1).toString());
				} else {
					ruleFileT.setFileInput("");
				}
				ruleFileT.setFileName(getTable().getValueAt(i, 2).toString());
				ruleFileT.setRuleFileId(fileId);
				ruleFileT.setRuleTitleId(titleId);
			}
			listRuleFileT.add(ruleFileT);
		}
		return listRuleFileT;
	}

	private RuleFileT getRuleFileT(Long fileId) {
		for (RuleFileT ruleFileT : listFileBean) {
			if (fileId.equals(ruleFileT.getRuleFileId())) {
				return ruleFileT;
			}
		}
		return null;
	}

	/***
	 * 文件选择
	 */
	private void select() {
		FileChooseDialog chooseDialog = new FileChooseDialog(listFile);
		chooseDialog.setVisible(true);
		if (chooseDialog.isOperation()) {// 操作
			Vector<Vector<String>> data = ((JecnTableModel) getTable().getModel()).getDataVector();
			if (listFile != null && listFile.size() > 0) {
				if (data != null) {
					data.clear();
				}
				for (JecnTreeBean jecnTreeBean : listFile) {
					((DefaultTableModel) table.getModel()).addRow(this.getRowData(jecnTreeBean));
				}
			} else {
				// 清空表格
				((DefaultTableModel) table.getModel()).setRowCount(0);
			}
		}
	}

	private Vector<String> getRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> vector = new Vector<String>();
		vector.add(String.valueOf(jecnTreeBean.getId()));
		vector.add(jecnTreeBean.getNumberId());
		vector.add(jecnTreeBean.getName());
		return vector;
	}

	@Override
	protected void dbClickMethod() {
		select();
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("fileNum"));
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		// 相关标准
		Vector<Vector<String>> orderContent = new Vector<Vector<String>>();
		for (JecnTreeBean jecnTreeBean : listFile) {
			orderContent.add(getRowData(jecnTreeBean));
			listIds.add(jecnTreeBean.getId());
		}
		return orderContent;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return JecnUtil.isChangeListLong(getResultListLong(), listIds);
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
