package com.jecn.epros.server.action.web.reports;

import java.awt.Font;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import net.sf.json.processors.DefaultValueProcessor;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieToolTipGenerator;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;

import com.jecn.epros.server.bean.process.temp.TempApproveTimeHeadersBean;
import com.jecn.epros.server.bean.process.temp.TempTaskApproveTime;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCount;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCountDetail;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.Pager;
import com.jecn.epros.server.common.PagerService;
import com.jecn.epros.server.service.propose.IRtnlProposeService;
import com.jecn.epros.server.service.reports.IReportDownExcelService;
import com.jecn.epros.server.service.reports.IWebReportsService;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.AgingProcessBean;
import com.jecn.epros.server.webBean.reports.ProcessAccessBean;
import com.jecn.epros.server.webBean.reports.ProcessActivitiesBean;
import com.jecn.epros.server.webBean.reports.ProcessAnalysisBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationDetailsBean;
import com.jecn.epros.server.webBean.reports.ProcessConstructionBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.server.webBean.reports.ProcessInputOutputBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIFollowBean;
import com.jecn.epros.server.webBean.reports.ProcessRoleBean;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * 报表统计
 * 
 * @author Administrator
 * 
 */
public class ReportsAction extends BaseAction {
	private static final Logger log = Logger.getLogger(ReportsAction.class);
	private IWebReportsService webReportsService;
	private IReportDownExcelService reportDownExcelService;
	private IRtnlProposeService rtnlProposeService;

	/** 责任部门ID */
	private long resOrgId = -1;
	/** 流程ID */
	private long processId = -1;
	/** 时间段(123....12) */
	private long time = -1;
	/** 是否更新状态 */
	private int updateType = -1;
	/** 流程责任人ID */
	private long processUserID = -1;
	/** 发布日期 */
	private String releaseDate;
	/** 建设类型 */
	private int constructionType;
	/** 类型（小于，等于，大于） */
	private int comparativeType;
	/** 类型值 */
	private int typeNumber;
	/** 岗位ID */
	private long posId = -1;
	/** 时间类型 */
	private long timeType = -1;
	/** 流程类别 */
	private long processType = -1;
	/** 用户ID */
	private long userId = -1;
	/** 时间段(开始时间) */
	private String startTime;
	/** 时间段(结束时间) */
	private String endTime;
	/** 部门名称ID */
	private long orgId = -1;

	/** 定义PagerService对象，用于传到页面 */
	private PagerService pagerService;
	/** 定义Pager对象，用于传到页面 */
	private Pager pager;
	/** 得到所有行数 */
	private String totalRows;
	/** 记录当前页号 */
	private String currentPage;
	/** 获取当前执行的方法，first:首页，previous:前一页，next:后一页，last:尾页 */
	private String pagerMethod;

	/** 饼状图存储路径 */
	private String imgPath;

	/** 流程时效性统计数据Bean */
	private AgingProcessBean agingProcessBean;
	/** 流程建设度数据Bean */
	private ProcessConstructionBean processConstructionBean;
	/** 流程角色数Bean */
	private List<ProcessRoleBean> processRoleList;
	/** 流程活动数Bean */
	private List<ProcessActivitiesBean> processActivitiesList;
	/** 流程应用人数excel下载 */
	private List<ProcessApplicationDetailsBean> processApplicationDetailsList;
	/** 流程应用人数详情Bean */
	private ProcessApplicationDetailsBean processApplicationDetails;
	/** 流程应用人数Bean */
	private List<ProcessApplicationBean> processApplicationList;
	/** 流程输入输出Bean */
	private List<ProcessInputOutputBean> processInputOutputList;
	/** 流程分析统计Bean */
	private List<ProcessAnalysisBean> processAnalysisList;
	/** 流程文档下载数和下载人,流程访问数和访问人Bean */
	private List<ProcessAccessBean> processAccessList;
	/** 流程显示数据 */
	private List<ProcessWebBean> processWebList;

	/** 下载产生的文件流 */
	private byte[] bytes;
	/** 下载文件名称 */
	private String fileName = "processStatistics.xls";

	/** 活动开始数 */
	private int startNum;
	/** 活动结束数 */
	private int endNum;
	/** 开始页 */
	private int start;
	/** 结束页 */
	private int limit;
	/** 任务类型 */
	private int taskType;
	/** 责任部门id 按照逗号分隔 */
	private String dutyOrgIds;
	/** 流程地图id 按照逗号分隔 */
	private String processMapIds;

	/** 责任部门id 按照逗号分隔 */
	private String dutyOrgNames;
	/** 流程地图id 按照逗号分隔 */
	private String processMapNames;

	/** 人员ids使用逗号分隔 **/
	private String personIds;
	/** 部门ids使用逗号分隔 **/
	private String orgIds;

	/**
	 * 查询流程时效性统计 3.06废弃
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findAgingProcess() {
		try {
			if (time == -1) {
				time = 12;
			}
			int totalPages = webReportsService.findAgingProcessCount(resOrgId, processId, time, updateType,
					getProjectId());
			processWebList = webReportsService.findAgingProcess(resOrgId, processId, time, updateType, getProjectId(),
					start, limit);
			JSONArray jsonArray = JSONArray.fromObject(processWebList);
			outJsonPage(jsonArray.toString(), totalPages);
		} catch (Exception e) {
			log.error("查询流程时效性统计异常！！！", e);
		}
		return null;
	}

	/**
	 * 生成流程时效性饼状图 3.06废弃
	 * 
	 * @author fuzhh Mar 5, 2013
	 * @return
	 */
	public String findAgingProcessPieCharts() {
		FileOutputStream fos = null;
		try {
			// 获得数据集
			agingProcessBean = webReportsService.findAgingProcessImage(resOrgId, processId, time, updateType,
					getProjectId());
			DefaultPieDataset dfp = new DefaultPieDataset();
			// 未更新
			dfp.setValue(this.getText("failedToUpdate") + "(" + agingProcessBean.getFailedToUpdate() + ")",
					agingProcessBean.getFailedToUpdate());
			// 已更新
			dfp.setValue(this.getText("hasBeenUpdated") + "(" + agingProcessBean.getHasBeenUpdated() + ")",
					agingProcessBean.getHasBeenUpdated());
			PiePlot3D plot = new PiePlot3D(dfp);
			plot.setLabelFont(new Font("宋体", Font.BOLD, 16));
			JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);
			chart.setBackgroundPaint(java.awt.Color.white);
			// 未更新流程统计
			chart.setTitle(new TextTitle(this.getText("failedToUpdateProcessStatistics"), new Font("黑体", Font.ITALIC,
					22)));
			chart.setBorderVisible(false);
			plot.setToolTipGenerator(new StandardPieToolTipGenerator());
			String path = JecnPath.APP_PATH;
			// 获取存储临时文件的目录
			String tempFileDir = path + "images/tempImage/" + "PROCESS_STATISTICS/";
			// 创建临时文件目录
			File fileDir = new File(tempFileDir);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			File[] delFile = fileDir.listFiles();
			for (int i = 0; i < delFile.length; i++) {// 目录下只能存在一个临时文件
				delFile[i].delete();
			}
			// 获取UUID随机数
			String timefile = UUID.randomUUID().toString();
			String fileName = tempFileDir + timefile + ".jpeg";
			fos = new FileOutputStream(fileName);
			plot.setDataset(dfp);
			ChartUtilities.writeChartAsJPEG(fos, new Float(1), chart, 300, 300, null);
			fos.flush();
			imgPath = "images/tempImage/" + "PROCESS_STATISTICS/" + timefile + ".jpeg";
		} catch (Exception e) {
			log.error("生成流程时效统计饼状图错误！！！", e);
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("关闭流异常", e);
				}
			}
		}
		return SUCCESS;
	}

	/**
	 * 查询流程建设度 3.06废弃
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessConstruction() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessConstructionCount(resOrgId, processId, constructionType,
					getProjectId());
			if (totalPages > 0) {
				processWebList = webReportsService.findProcessConstruction(resOrgId, processId, constructionType,
						getProjectId(), start, limit);
				JSONArray jsonArray = JSONArray.fromObject(processWebList);
				outJsonPage(jsonArray.toString(), totalPages);
			} else {
				outJsonPage("[]", totalPages);
			}

		} catch (Exception e) {
			log.error("查询流程建设度异常！！！", e);
		}
		return null;
	}

	/**
	 * 生成流程建设度饼状图 3.06废弃
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findprocessConstructionPieCharts() {
		FileOutputStream fos = null;
		try {
			// 获得数据集
			processConstructionBean = webReportsService.findProcessConstructionImage(resOrgId, processId,
					constructionType, getProjectId());
			DefaultPieDataset dfp = new DefaultPieDataset();
			// 审批中
			dfp.setValue(this.getText("examinationAndApprovalOf") + "(" + processConstructionBean.getEaaCount() + ")",
					processConstructionBean.getEaaCount());
			// 已发布
			dfp.setValue(this.getText("released") + "(" + processConstructionBean.getReleasedCount() + ")",
					processConstructionBean.getReleasedCount());
			// 待建
			dfp.setValue(this.getText("toBeBuilt") + "(" + processConstructionBean.getTbbCount() + ")",
					processConstructionBean.getTbbCount());
			PiePlot3D plot = new PiePlot3D(dfp);
			plot.setLabelFont(new Font("宋体", Font.BOLD, 16));
			JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);
			chart.setBackgroundPaint(java.awt.Color.white);
			// 流程建设覆盖度统计
			chart.setTitle(new TextTitle(this.getText("coverageStatisticsProcessConstruction"), new Font("黑体",
					Font.ITALIC, 22)));
			chart.setBorderVisible(false);
			plot.setToolTipGenerator(new StandardPieToolTipGenerator());
			String path = JecnPath.APP_PATH;
			// 获取存储临时文件的目录
			String tempFileDir = path + "images/tempImage/" + "PROCESS_STATISTICS/";
			// 创建临时文件目录
			File fileDir = new File(tempFileDir);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			File[] delFile = fileDir.listFiles();
			for (int i = 0; i < delFile.length; i++) {// 目录下只能存在一个临时文件
				delFile[i].delete();
			}
			// 获取UUID随机数
			String timefile = UUID.randomUUID().toString();
			String fileName = tempFileDir + timefile + ".jpeg";
			fos = new FileOutputStream(fileName);
			plot.setDataset(dfp);
			ChartUtilities.writeChartAsJPEG(fos, new Float(1), chart, 300, 300, null);
			fos.flush();
			imgPath = "images/tempImage/" + "PROCESS_STATISTICS/" + timefile + ".jpeg";
		} catch (Exception e) {
			log.error("生成流程时效统计饼状图错误！！！", e);
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("关闭流异常", e);
				}
			}
		}
		return SUCCESS;
	}

	/**
	 * 查询流程角色数统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessRole() {
		try {
			processRoleList = webReportsService.findProcessRole(processId, getProjectId());
		} catch (Exception e) {
			log.error("查询流程角色分析统计异常！！！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程角色详情
	 * 
	 * @author fuzhh Mar 6, 2013
	 * @return
	 */
	public String findProcessRoleDetails() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessRoleCount(processId, typeNumber, getProjectId());
			if (totalPages > 0) {
				processWebList = webReportsService.findProcessRoleDetails(processId, typeNumber, getProjectId(), start,
						limit);
				JSONArray jsonArray = JSONArray.fromObject(processWebList);
				outJsonPage(jsonArray.toString(), totalPages);
			} else {
				outJsonPage("[]", totalPages);
			}

		} catch (Exception e) {
			log.error("流程角色详情统计异常!!!", e);
		}
		return null;
	}

	public String processRoleDetailsLink() {
		return SUCCESS;
	}

	/**
	 * 查询流程活动数统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessActivities() {
		try {
			processActivitiesList = webReportsService.findProcessActivities(processId, getProjectId());
		} catch (Exception e) {
			log.error("查询流程活动数统计异常！！！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程活动详情
	 * 
	 * @author fuzhh Mar 6, 2013
	 * @return
	 */
	public String findProcessActivitiesDetails() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessActivitiesCount(processId, startNum, endNum, getProjectId());
			if (totalPages > 0) {
				processWebList = webReportsService.findProcessActivitiesDetails(processId, startNum, endNum,
						getProjectId(), start, limit);
				JSONArray jsonArray = JSONArray.fromObject(processWebList);
				outJsonPage(jsonArray.toString(), totalPages);
			} else {
				outJsonPage("[]", totalPages);
			}

		} catch (Exception e) {
			log.error("流程活动详情统计异常!!!", e);
		}
		return null;
	}

	public String processActivitiesDetailsLink() {
		return SUCCESS;
	}

	/**
	 * 流程应用人数统计详情
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessApplicationDetails() {
		try {
			processApplicationDetails = webReportsService.findProcessApplicationDetails(processId);
		} catch (Exception e) {
			log.error("查询流程应用人数统计详情异常！！！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程应用人数统计
	 * 
	 * @author fuzhh Mar 6, 2013
	 * @return
	 */
	public String findProcessApplication() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessApplicationCount(processId, resOrgId, getProjectId());
			if (totalPages == 0) {
				outJsonPage("[]", totalPages);
				return null;
			}
			processApplicationList = webReportsService.findProcessApplication(processId, resOrgId, getProjectId(),
					start, limit);
			JSONArray jsonArray = JSONArray.fromObject(processApplicationList);
			outJsonPage(jsonArray.toString(), totalPages);
		} catch (Exception e) {
			log.error("查询流程应用人数统计异常！！！", e);
		}
		return null;
	}

	/**
	 * 流程输入输出统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessInputOutput() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessInputOutputCount(processId, resOrgId, getProjectId());
			if (totalPages == 0) {
				outJsonPage("[]", totalPages);
				return null;
			}
			processInputOutputList = webReportsService.findProcessInputOutput(processId, resOrgId, getProjectId(),
					start, limit);
			for (ProcessInputOutputBean processInputOutput : processInputOutputList) {
				// 上游流程
				if (processInputOutput.getUpstreamProcess() != null
						&& processInputOutput.getUpstreamProcess().size() > 0) {
					StringBuffer upstreamProcessName = new StringBuffer();
					for (ProcessWebBean processWebBean : processInputOutput.getUpstreamProcess()) {
						upstreamProcessName.append("<div  ext:qtip='" + processWebBean.getFlowName()
								+ "'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="
								+ processWebBean.getFlowId() + "' >" + processWebBean.getFlowName() + "</a><br>");
					}
					if (upstreamProcessName != null) {
						String upstreamProcess = upstreamProcessName.substring(0, upstreamProcessName.length() - 4);
						processInputOutput.setUpstreamProcessName(upstreamProcess);
					}
				}

				// 下游流程
				if (processInputOutput.getDownstreamProcess() != null
						&& processInputOutput.getDownstreamProcess().size() > 0) {
					StringBuffer downstreamProcessName = new StringBuffer();
					for (ProcessWebBean processWebBean : processInputOutput.getDownstreamProcess()) {
						downstreamProcessName.append("<div  ext:qtip='" + processWebBean.getFlowName()
								+ "'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="
								+ processWebBean.getFlowId() + "' >" + processWebBean.getFlowName() + "</a><br>");
					}
					if (downstreamProcessName != null) {
						String downstreamProcess = downstreamProcessName.substring(0,
								downstreamProcessName.length() - 4);
						processInputOutput.setDownstreamProcessName(downstreamProcess);
					}
				}
			}
			JSONArray jsonArray = JSONArray.fromObject(processInputOutputList);
			outJsonPage(jsonArray.toString(), totalPages);
		} catch (Exception e) {
			log.error("查询流程输入输出统计异常！！！", e);
		}
		return null;
	}

	/**
	 * 流程分析统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessAnalysis() {
		try {
			processAnalysisList = webReportsService.findAllProcessAnalysis(processId, timeType, processType, posId,
					getProjectId());
		} catch (Exception e) {
			log.error("查询流程分析统计异常！！！", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程分析统计详情
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessAnalysisDetails() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessAnalysisCount(processId, timeType, processType, posId,
					getProjectId());
			if (totalPages > 0) {
				processWebList = webReportsService.findProcessAnalysis(processId, timeType, processType, posId,
						getProjectId(), start, limit);
				JSONArray jsonArray = JSONArray.fromObject(processWebList);
				outJsonPage(jsonArray.toString(), totalPages);
			} else {
				outJsonPage("[]", totalPages);
			}

		} catch (Exception e) {
			log.error("查询流程分析统计异常详情！！！", e);
		}
		return null;
	}

	/**
	 * 流程访问数统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessAccess() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessAccessCount(processId, resOrgId, startTime, endTime,
					getProjectId(), 0);
			if (totalPages > 0) {
				processAccessList = webReportsService.findProcessAccess(processId, resOrgId, startTime, endTime,
						getProjectId(), start, limit, 0);
				JSONArray jsonArray = JSONArray.fromObject(processAccessList);
				outJsonPage(jsonArray.toString(), totalPages);
			} else {
				outJsonPage("[]", totalPages);
			}

		} catch (Exception e) {
			log.error("查询流程访问数统计异常！！！", e);
		}
		return null;
	}

	/**
	 * 流程访问人统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessInterviewer() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessInterviewerCount(userId, processId, startTime, endTime,
					resOrgId, orgId, posId, getProjectId(), 0);
			if (totalPages > 0) {
				processAccessList = webReportsService.findProcessInterviewer(userId, processId, startTime, endTime,
						resOrgId, orgId, posId, getProjectId(), start, limit, 0);
				JSONArray jsonArray = JSONArray.fromObject(processAccessList);
				outJsonPage(jsonArray.toString(), totalPages);
			} else {
				outJsonPage("[]", totalPages);
			}

		} catch (Exception e) {
			log.error("查询流程访问人统计异常！！！", e);
		}
		return null;
	}

	/**
	 * 流程文档下载数统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessDocumentNumber() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessAccessCount(processId, resOrgId, startTime, endTime,
					getProjectId(), 1);
			processAccessList = webReportsService.findProcessAccess(processId, resOrgId, startTime, endTime,
					getProjectId(), start, limit, 1);
			JSONArray jsonArray = JSONArray.fromObject(processAccessList);
			outJsonPage(jsonArray.toString(), totalPages);
		} catch (Exception e) {
			log.error("查询 流程文档下载数统计异常！！！", e);
		}
		return null;
	}

	/**
	 * 流程文档下载人统计
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String findProcessDocumentPeople() {
		try {
			// 总分页数
			int totalPages = webReportsService.findProcessInterviewerCount(userId, processId, startTime, endTime,
					resOrgId, orgId, posId, getProjectId(), 1);
			processAccessList = webReportsService.findProcessInterviewer(userId, processId, startTime, endTime,
					resOrgId, orgId, posId, getProjectId(), start, limit, 1);
			JSONArray jsonArray = JSONArray.fromObject(processAccessList);
			outJsonPage(jsonArray.toString(), totalPages);
		} catch (Exception e) {
			log.error("查询流程文档下载人统计异常！！！", e);
		}
		return null;
	}

	/**
	 * 流程时效性统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadAgingProcess() {
		bytes = reportDownExcelService.getAgingProcessExcel(null);
		try {
			agingProcessBean = webReportsService.findAllAgingProcess(resOrgId, processId, time, updateType,
					getProjectId());

			if (agingProcessBean != null) {
				agingProcessBean.setTime(this.getTime());
			}

			bytes = reportDownExcelService.getAgingProcessExcel(agingProcessBean);
			fileName = this.getText("agingProcessStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadAgingProcess方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程建设度下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessConstruction() {
		try {
			processConstructionBean = webReportsService.findAllProcessConstruction(resOrgId, processId,
					constructionType, getProjectId());

			bytes = reportDownExcelService.getProcessBuildCoverageExcel(processConstructionBean);
			fileName = this.getText("theConstructionProcessOfCoverage") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessConstruction方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程角色数统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessRole() {
		try {
			processRoleList = webReportsService.findAllProcessRole(processId, getProjectId());

			bytes = reportDownExcelService.downloadProcessRole(processRoleList);
			fileName = this.getText("processRoleNumberStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessRole方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程活动数统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessActivities() {
		try {
			processActivitiesList = webReportsService.findAllProcessActivities(processId, getProjectId());

			bytes = reportDownExcelService.downloadProcessActivities(processActivitiesList);
			fileName = this.getText("processActivitiesNumberStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessActivities方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程应用人数统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessApplication() {
		try {
			processApplicationDetailsList = webReportsService.findProcessApplicationDetails(processId, resOrgId,
					getProjectId());

			bytes = reportDownExcelService.downloadProcessApplication(processApplicationDetailsList);
			fileName = this.getText("processApplicationNumberStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessApplication方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程输入输出统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessInputOutput() {
		try {
			processInputOutputList = webReportsService.findAllProcessInputOutput(processId, resOrgId, getProjectId());

			bytes = reportDownExcelService.downloadProcessInputOutput(processInputOutputList);
			fileName = this.getText("processInputOutputStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessInputOutput方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程分析统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessAnalysis() {
		try {
			processAnalysisList = webReportsService.findAllProcessAnalysisExcel(processId, timeType, processType,
					posId, getProjectId());

			bytes = reportDownExcelService.downloadProcessAnalysis(processAnalysisList);
			fileName = this.getText("processAnalysisStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessAnalysis方法：", e);
		}

		return SUCCESS;
	}

	/**
	 * 流程访问数统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessAccess() {
		try {
			// 0是流程访问数统计 1是流程下载数统计
			processAccessList = webReportsService.findAllProcessAccess(processId, resOrgId, startTime, endTime,
					getProjectId(), 0);

			bytes = reportDownExcelService.downloadProcessAccess(processAccessList);
			fileName = this.getText("processAccessNumberStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessAccess方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程访问人统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessInterviewer() {
		try {
			// 0是流程访问数统计 1是流程下载数统计
			processAccessList = webReportsService.findAllProcessInterviewer(userId, processId, startTime, endTime,
					resOrgId, orgId, posId, getProjectId(), 0);

			bytes = reportDownExcelService.downloadProcessInterviewer(processAccessList);
			fileName = this.getText("processInterviewerStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessInterviewer方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程文档下载数统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessDocumentNumber() {
		try {
			// 0是流程访问数统计 1是流程下载数统计
			processAccessList = webReportsService.findAllProcessAccess(processId, resOrgId, startTime, endTime,
					getProjectId(), 1);

			bytes = reportDownExcelService.downloadProcessDocumentNumber(processAccessList);
			fileName = this.getText("processDocumentDownloadNumberStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessDocumentNumber方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 流程文档下载人统计下载
	 * 
	 * @author fuzhh Feb 27, 2013
	 * @return
	 */
	public String downloadProcessDocumentPeople() {
		try {
			// 0是流程访问数统计 1是流程下载数统计
			processAccessList = webReportsService.findAllProcessInterviewer(userId, processId, startTime, endTime,
					resOrgId, orgId, posId, getProjectId(), 1);

			bytes = reportDownExcelService.downloadProcessDocumentPeople(processAccessList);
			fileName = this.getText("processDocumentDownloadPeopleStatistics") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessDocumentPeople方法：", e);
		}
		return SUCCESS;
	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		// 获得文件
		if (bytes == null) {
			// 下载文件信息异常
			outResultHtml(this.getText("fileInformation"), "");
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(bytes);
	}

	/**
	 * 任务审批时间统计
	 * 
	 * @author hyl
	 * @return
	 */
	public String countTaskApproveTime() {
		try {
			// 总分页数
			int totalPages = webReportsService.findTaskApproveTimeCount(taskType);
			if (totalPages > 0) {
				// 任务有不同的类型任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
				List<TempTaskApproveTime> taskList = webReportsService.findTaskApproveTime(start, limit, taskType);
				// 配置如果为审批时间为null的时候不转换为0
				JsonConfig jsonConfig = new JsonConfig();
				jsonConfig.registerDefaultValueProcessor(Integer.class, new DefaultValueProcessor() {
					public Object getDefaultValue(Class type) {
						return null;
					}
				});
				JSONArray jsonArray = JSONArray.fromObject(taskList, jsonConfig);
				String content = jsonArray.toString();
				outJsonPage(content, totalPages);
			} else {
				outJsonPage("[]", totalPages);
			}

		} catch (Exception e) {
			log.error("ReportsAction类countTaskApproveTime方法：", e);
		}
		return null;
	}

	/**
	 * 获取任务审批时间表头
	 * 
	 * @author hyl
	 * @return
	 */
	public String getTaskApproveTimeHeaders() {
		try {
			String columns = webReportsService.findTaskApproveTimeHeader(taskType);
			outJeon("columns", columns);
		}

		catch (Exception e) {
			log.error("ReportsAction类getTaskApproveTimeHeaders方法：", e);
		}
		return null;

	}

	/**
	 * 任务审批时间 -下载
	 * 
	 * @author hyl
	 * @return
	 */
	public String downloadTaskApproveTime() {
		try {
			List<TempTaskApproveTime> taskApproveTimeList = webReportsService.findAllTaskApproveTime(taskType);
			TempApproveTimeHeadersBean headersBean = webReportsService.findApproveTimeHeadersBean(taskType);
			bytes = reportDownExcelService.downloadTaskApproveTime(taskApproveTimeList, headersBean);

			fileName = this.getText("taskApproveTimeStatistics") + ".xls";
			// 流程任务(文件名)
			if (taskType == 0) {
				fileName = this.getText("processTaskApproveTimeStatistics") + ".xls";
			}
			// 流程地图任务
			else if (taskType == 4) {
				fileName = this.getText("processMapTaskApproveTimeStatistics") + ".xls";
			}
			// 制度任务
			else if (taskType == 2 || taskType == 3) {
				fileName = this.getText("ruleTaskApproveTimeStatistics") + ".xls";
			}
			// 文件任务
			else if (taskType == 1) {
				fileName = this.getText("fileTaskApproveTimeStatistics") + ".xls";
			}
		} catch (Exception e) {
			log.error("ReportsAction类downloadTaskApproveTime方法：任务类型：" + taskType, e);
		}
		return SUCCESS;
	}

	/**
	 * 下载流程清单报表
	 * 
	 * @return
	 */
	public String downloadProcessDetailList() {
//		try {
//			ProcessDetailBean processDetailBean = webReportsService.findProcessDetailBean(dutyOrgIds, processMapIds,
//					endTime, getProjectId());
//			bytes = reportDownExcelService.downloadProcessDetail(processDetailBean);
//			fileName = this.getText("processDetailListReport") + ".xls";
//		} catch (Exception e) {
//			log.error("ReportsAction类downloadProcessDetailList方法异常：\r\n责任部门id:" + dutyOrgIds + "\r\n责任部门name:"
//					+ dutyOrgNames + "\r\n流程地图id:" + processMapIds + "\r\n流程架构name:" + processMapNames + "\r\nendTime:"
//					+ endTime + "\r\n项目id:" + getProjectId(), e);
//		}
		return SUCCESS;
	}

	/**
	 * 下载流程审视优化统计表
	 * 
	 * @return
	 */
	public String downloadProcessScanOptimize() {
//		try {
//			ProcessScanOptimizeBean processScanOptimize = webReportsService.findProcessScanOptimizeBean(dutyOrgIds,
//					processMapIds, startTime, endTime, getProjectId(), 0);
//			bytes = reportDownExcelService.downloadProcessScanOptimize(processScanOptimize);
//			fileName = this.getText("processScanOptimizeReport") + ".xls";
//		} catch (Exception e) {
//			log.error("ReportsAction类downloadProcessScanOptimize方法异常：\r\n责任部门id:" + dutyOrgIds + "\r\n部门名称:"
//					+ dutyOrgNames + "\r\n流程地图id:" + processMapIds + "\r\n流程架构name:" + "\r\nstartTime:" + startTime
//					+ "\r\nendTime:" + endTime + "\r\n项目id:" + getProjectId(), e);
//		}
		return SUCCESS;
	}

	/**
	 * 下载流程KPI跟踪表
	 * 
	 * @return
	 */
	public String downloadProcessKPIFollow() {
		try {
			ProcessKPIFollowBean processKPIFlowBean = webReportsService.findProcessKPIFollowBean(dutyOrgIds,
					processMapIds, startTime, endTime, getProjectId());
			bytes = reportDownExcelService.downloadProcessKPIFollow(processKPIFlowBean);
			fileName = this.getText("processKPIFollowReport") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProcessKPIFollow方法异常：\r\n责任部门id:" + dutyOrgIds + "\r\n部门名称:"
					+ dutyOrgNames + "\r\n流程地图id:" + processMapIds + "\r\n流程架构name:" + "\r\nstartTime:" + startTime
					+ "\r\nendTime:" + endTime + "\r\n项目id：" + getProjectId(), e);
		}
		return SUCCESS;
	}

	/**
	 * 下载流程审视优化数据维护表
	 * 
	 * @return
	 */
	public String downloadExpiryMaintenance() {
		try {
			ProcessExpiryMaintenanceBean expiryMaintenanceBean = webReportsService.findProcessExpiryMaintenanceBean(
					dutyOrgIds, processMapIds, getProjectId(), 0);
			bytes = reportDownExcelService.downloadProcessExpiryMaintenanceBean(expiryMaintenanceBean);
			fileName = this.getText("expiryMaintenance") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadExpiryMaintenance方法异常：\r\n责任部门id:" + dutyOrgIds + "\r\n部门名称:"
					+ dutyOrgNames + "\r\n流程架构/流程id:" + processMapIds + "\r\n流程架构/流程name:" + "\r\n项目id："
					+ getProjectId(), e);
		}

		return SUCCESS;
	}

	/**
	 * 下载流程审视优化数据维护表错误数据
	 * 
	 * @return
	 */
	public String downloadExpiryMaintenanceErrorData() {
		try {
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
			bytes = reportDownExcelService.downloadProcessExpiryMaintenanceErrorDate(curPeopleId, 0);
			fileName = "errorData.xls";
			if (bytes == null || bytes.length == 0) {
				// 没有错误数据
				outOrgHtml(getText("notHasErrorData"));
				return NONE;
			}
		} catch (Exception e) {
			log.error("下载流程审视优化数据维护表错误数据异常downloadExpiryMaintenanceErrorData()", e);
		}

		return SUCCESS;
	}

	/**
	 * 下载合理化建议创建人的统计信息
	 * 
	 * @return
	 */
	public String downloadProposeCreateCount() {
		try {
			List<JecnRtnlProposeCountDetail> countList = rtnlProposeService.getProposeCreateCount(personIds, startTime,
					endTime, -1, -1);
			JecnRtnlProposeCount proposeCountBean = new JecnRtnlProposeCount();
			proposeCountBean.setType(1);
			proposeCountBean.setDateStr(startTime + "---" + endTime);
			proposeCountBean.setCountDetailList(countList);

			bytes = reportDownExcelService.downloadProposeCount(proposeCountBean);
			fileName = this.getText("rationalizationproposals") + ".xls";
		} catch (Exception e) {
			log
					.error("ReportsAction类downloadProposeCreate方法异常：personIds" + personIds + "\r\n项目id："
							+ getProjectId(), e);
		}

		return SUCCESS;
	}

	/**
	 * 下载合理化建议部门的统计信息
	 * 
	 * @return
	 */
	public String downloadProposeOrgCount() {
		try {
			List<JecnRtnlProposeCountDetail> countList = rtnlProposeService.getProposeOrgCount(orgIds, startTime,
					endTime, -1, -1);
			JecnRtnlProposeCount proposeCountBean = new JecnRtnlProposeCount();
			proposeCountBean.setType(2);
			proposeCountBean.setDateStr(startTime + "---" + endTime);
			proposeCountBean.setCountDetailList(countList);

			bytes = reportDownExcelService.downloadProposeCount(proposeCountBean);
			fileName = this.getText("rationalizationproposals") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProposeOrgCount方法异常：orgIds" + orgIds + "\r\n项目id：" + getProjectId(), e);
		}

		return SUCCESS;
	}

	/**
	 * 下载合理化建议的详细统计信息
	 * 
	 * @return
	 */
	public String downloadProposeDetailCount() {
		try {
			List<JecnRtnlProposeCountDetail> countList = rtnlProposeService.getProposeDetailCountList(personIds,
					orgIds, startTime, endTime, -1, -1);
			JecnRtnlProposeCount proposeCountBean = new JecnRtnlProposeCount();
			proposeCountBean.setType(3);
			proposeCountBean.setDateStr(startTime + "---" + endTime);
			proposeCountBean.setCountDetailList(countList);

			bytes = reportDownExcelService.downloadProposeCount(proposeCountBean);
			fileName = this.getText("rationalizationproposals") + ".xls";
		} catch (Exception e) {
			log.error("ReportsAction类downloadProposeDetailCount方法异常：personIds" + personIds + "\r\n项目id："
					+ getProjectId(), e);
		}

		return SUCCESS;
	}

	public long getResOrgId() {
		return resOrgId;
	}

	public void setResOrgId(long resOrgId) {
		this.resOrgId = resOrgId;
	}

	public long getProcessId() {
		return processId;
	}

	public void setProcessId(long processId) {
		this.processId = processId;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public int getUpdateType() {
		return updateType;
	}

	public void setUpdateType(int updateType) {
		this.updateType = updateType;
	}

	public IWebReportsService getWebReportsService() {
		return webReportsService;
	}

	public void setWebReportsService(IWebReportsService webReportsService) {
		this.webReportsService = webReportsService;
	}

	public long getProcessUserID() {
		return processUserID;
	}

	public void setProcessUserID(long processUserID) {
		this.processUserID = processUserID;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getComparativeType() {
		return comparativeType;
	}

	public void setComparativeType(int comparativeType) {
		this.comparativeType = comparativeType;
	}

	public int getTypeNumber() {
		return typeNumber;
	}

	public void setTypeNumber(int typeNumber) {
		this.typeNumber = typeNumber;
	}

	public long getPosId() {
		return posId;
	}

	public void setPosId(long posId) {
		this.posId = posId;
	}

	public long getTimeType() {
		return timeType;
	}

	public void setTimeType(long timeType) {
		this.timeType = timeType;
	}

	public long getProcessType() {
		return processType;
	}

	public void setProcessType(long processType) {
		this.processType = processType;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public PagerService getPagerService() {
		return pagerService;
	}

	public void setPagerService(PagerService pagerService) {
		this.pagerService = pagerService;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public String getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(String totalRows) {
		this.totalRows = totalRows;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public String getPagerMethod() {
		return pagerMethod;
	}

	public void setPagerMethod(String pagerMethod) {
		this.pagerMethod = pagerMethod;
	}

	public int getConstructionType() {
		return constructionType;
	}

	public void setConstructionType(int constructionType) {
		this.constructionType = constructionType;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public AgingProcessBean getAgingProcessBean() {
		return agingProcessBean;
	}

	public void setAgingProcessBean(AgingProcessBean agingProcessBean) {
		this.agingProcessBean = agingProcessBean;
	}

	public ProcessConstructionBean getProcessConstructionBean() {
		return processConstructionBean;
	}

	public void setProcessConstructionBean(ProcessConstructionBean processConstructionBean) {
		this.processConstructionBean = processConstructionBean;
	}

	public List<ProcessRoleBean> getProcessRoleList() {
		return processRoleList;
	}

	public void setProcessRoleList(List<ProcessRoleBean> processRoleList) {
		this.processRoleList = processRoleList;
	}

	public List<ProcessActivitiesBean> getProcessActivitiesList() {
		return processActivitiesList;
	}

	public void setProcessActivitiesList(List<ProcessActivitiesBean> processActivitiesList) {
		this.processActivitiesList = processActivitiesList;
	}

	public List<ProcessApplicationDetailsBean> getProcessApplicationDetailsList() {
		return processApplicationDetailsList;
	}

	public void setProcessApplicationDetailsList(List<ProcessApplicationDetailsBean> processApplicationDetailsList) {
		this.processApplicationDetailsList = processApplicationDetailsList;
	}

	public List<ProcessInputOutputBean> getProcessInputOutputList() {
		return processInputOutputList;
	}

	public void setProcessInputOutputList(List<ProcessInputOutputBean> processInputOutputList) {
		this.processInputOutputList = processInputOutputList;
	}

	public List<ProcessAnalysisBean> getProcessAnalysisList() {
		return processAnalysisList;
	}

	public void setProcessAnalysisList(List<ProcessAnalysisBean> processAnalysisList) {
		this.processAnalysisList = processAnalysisList;
	}

	public List<ProcessAccessBean> getProcessAccessList() {
		return processAccessList;
	}

	public void setProcessAccessList(List<ProcessAccessBean> processAccessList) {
		this.processAccessList = processAccessList;
	}

	public List<ProcessWebBean> getProcessWebList() {
		return processWebList;
	}

	public void setProcessWebList(List<ProcessWebBean> processWebList) {
		this.processWebList = processWebList;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), fileName, "");
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public IReportDownExcelService getReportDownExcelService() {
		return reportDownExcelService;
	}

	public void setReportDownExcelService(IReportDownExcelService reportDownExcelService) {
		this.reportDownExcelService = reportDownExcelService;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getStartNum() {
		return startNum;
	}

	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}

	public int getEndNum() {
		return endNum;
	}

	public void setEndNum(int endNum) {
		this.endNum = endNum;
	}

	public List<ProcessApplicationBean> getProcessApplicationList() {
		return processApplicationList;
	}

	public void setProcessApplicationList(List<ProcessApplicationBean> processApplicationList) {
		this.processApplicationList = processApplicationList;
	}

	public ProcessApplicationDetailsBean getProcessApplicationDetails() {
		return processApplicationDetails;
	}

	public void setProcessApplicationDetails(ProcessApplicationDetailsBean processApplicationDetails) {
		this.processApplicationDetails = processApplicationDetails;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getDutyOrgNames() {
		return dutyOrgNames;
	}

	public void setDutyOrgNames(String dutyOrgNames) {
		this.dutyOrgNames = dutyOrgNames;
	}

	public String getProcessMapNames() {
		return processMapNames;
	}

	public void setProcessMapNames(String processMapNames) {
		this.processMapNames = processMapNames;
	}

	public String getDutyOrgIds() {
		return dutyOrgIds;
	}

	public void setDutyOrgIds(String dutyOrgIds) {
		this.dutyOrgIds = dutyOrgIds;
	}

	public String getProcessMapIds() {
		return processMapIds;
	}

	public void setProcessMapIds(String processMapIds) {
		this.processMapIds = processMapIds;
	}

	public String getPersonIds() {
		return personIds;
	}

	public void setPersonIds(String personIds) {
		this.personIds = personIds;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public IRtnlProposeService getRtnlProposeService() {
		return rtnlProposeService;
	}

	public void setRtnlProposeService(IRtnlProposeService rtnlProposeService) {
		this.rtnlProposeService = rtnlProposeService;
	}

}
