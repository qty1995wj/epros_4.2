package epros.draw.gui.figure;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;

/**
 * 片段存储数据类
 * 
 * @author fuzhh
 * 
 */
public class JecnTemplateData {

	/** 模板片段原始的中心坐标点 */
	private Point templatePoint;
	/** 存储片段数据的List */
	private List<JecnBaseFlowElementPanel> templateElementList;
	/** 模板片段名称 */
	private String templateName;
	/** 模板片段图片路径 */
	private String imagerPath;
	/** 模板片段的图片名称 */
	private String imagerName;
	/** 片段的唯一ID */
	private String templateId;
	/** 片段类型 是流程图，地图，或组织图 */
	private String mapType;
	/** 片段存储线的数据 */
	private Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> manLineMapList = null;
	/** 选中区域的宽 */
	private String selectWidth;
	/** 选中区域的高 */
	private String selectHeight;

	/** 大线对应小线的Map集合 */
	public Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> getManLineMapList() {
		if (manLineMapList == null) {
			manLineMapList = new HashMap<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>>();
		}
		return manLineMapList;
	}

	public String getSelectWidth() {
		return selectWidth;
	}

	public void setSelectWidth(String selectWidth) {
		this.selectWidth = selectWidth;
	}

	public String getSelectHeight() {
		return selectHeight;
	}

	public void setSelectHeight(String selectHeight) {
		this.selectHeight = selectHeight;
	}

	public String getMapType() {
		return mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}

	public Point getTemplatePoint() {
		return templatePoint;
	}

	public void setTemplatePoint(Point templatePoint) {
		this.templatePoint = templatePoint;
	}

	public List<JecnBaseFlowElementPanel> getTemplateElementList() {
		if (templateElementList == null) {
			templateElementList = new ArrayList<JecnBaseFlowElementPanel>();
		}
		return templateElementList;
	}

	public void setTemplateElementList(
			List<JecnBaseFlowElementPanel> templateElementList) {
		this.templateElementList = templateElementList;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getImagerPath() {
		return imagerPath;
	}

	public void setImagerPath(String imagerPath) {
		this.imagerPath = imagerPath;
	}

	public String getImagerName() {
		return imagerName;
	}

	public void setImagerName(String imagerName) {
		this.imagerName = imagerName;
	}

}
