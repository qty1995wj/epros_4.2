package com.jecn.epros.server.connector.mengniu;

/**
 * 结束流程
 * 
 * @author xiaohu
 * 
 */
public class MengNiuFinishWorkFlowInterface {
	/** 1参数A模式 2参数B模式 */
	private int ParamMode = 2;

	private MengNiuFinishWorkFlowBean Fin_WF;

	public int getParamMode() {
		return ParamMode;
	}

	public void setParamMode(int paramMode) {
		ParamMode = paramMode;
	}

	public MengNiuFinishWorkFlowBean getFin_WF() {
		return Fin_WF;
	}

	public void setFin_WF(MengNiuFinishWorkFlowBean finWF) {
		Fin_WF = finWF;
	}

}
