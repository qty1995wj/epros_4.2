package com.jecn.epros.domain.keySearch;

import java.util.List;

/**
 * 关键字搜索返回结果集
 *
 * @author ZXH
 */
public class ResourceSearchResultBean {
    private int flowMapCount;
    private int flowCount;
    private int fileCount;
    private int ruleCount;
    private int standardCount;
    private int riskCount;
    private int resultNum;
    /**
     * 手机端显示
     */
    private int flowAndMapNum;

    private List<ResourceSearchBean> searchResultLists;

    public int getFlowMapCount() {
        return flowMapCount;
    }

    public void setFlowMapCount(int flowMapCount) {
        this.flowMapCount = flowMapCount;
    }

    public int getFlowCount() {
        return flowCount;
    }

    public void setFlowCount(int flowCount) {
        this.flowCount = flowCount;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public int getRuleCount() {
        return ruleCount;
    }

    public void setRuleCount(int ruleCount) {
        this.ruleCount = ruleCount;
    }

    public int getStandardCount() {
        return standardCount;
    }

    public void setStandardCount(int standardCount) {
        this.standardCount = standardCount;
    }

    public int getRiskCount() {
        return riskCount;
    }

    public void setRiskCount(int riskCount) {
        this.riskCount = riskCount;
    }

    public List<ResourceSearchBean> getSearchResultLists() {
        return searchResultLists;
    }

    public void setSearchResultLists(List<ResourceSearchBean> searchResultLists) {
        this.searchResultLists = searchResultLists;
    }

    public void setResultNum(int resultNum) {
        this.resultNum = resultNum;
    }

    public int getResultNum() {
        resultNum = this.flowMapCount + this.flowCount + this.fileCount + this.ruleCount + this.standardCount
                + this.riskCount;
        return resultNum;
    }

    public int getFlowAndMapNum() {
        return flowAndMapNum;
    }

    public void setFlowAndMapNum(int flowAndMapNum) {
        this.flowAndMapNum = flowAndMapNum;
    }
}
