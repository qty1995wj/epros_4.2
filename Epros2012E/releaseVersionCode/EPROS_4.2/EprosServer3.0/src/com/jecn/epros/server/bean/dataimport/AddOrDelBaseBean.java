package com.jecn.epros.server.bean.dataimport;

import java.io.Serializable;
import java.util.List;

/**
 * 数据传输封装bean
 * 
 * */
public class AddOrDelBaseBean implements Serializable {

	/**
	 * 
	 * 岗位组ID
	 * 
	 * **/
	private String posGroupId = null;

	/**
	 * 待删除的基准岗位集合
	 * 
	 * */
	private List<String> delJecnBasePosBeanList = null;
	/**
	 * 待添加的基准岗位集合
	 * */
	private List<JecnBasePosBean> addJecnBasePosBeanList = null;

	public List<String> getDelJecnBasePosBeanList() {
		return delJecnBasePosBeanList;
	}

	public void setDelJecnBasePosBeanList(List<String> delJecnBasePosBeanList) {
		this.delJecnBasePosBeanList = delJecnBasePosBeanList;
	}

	public List<JecnBasePosBean> getAddJecnBasePosBeanList() {
		return addJecnBasePosBeanList;
	}

	public void setAddJecnBasePosBeanList(List<JecnBasePosBean> addJecnBasePosBeanList) {
		this.addJecnBasePosBeanList = addJecnBasePosBeanList;
	}

	public String getPosGroupId() {
		return posGroupId;
	}

	public void setPosGroupId(String posGroupId) {
		this.posGroupId = posGroupId;
	}

}
