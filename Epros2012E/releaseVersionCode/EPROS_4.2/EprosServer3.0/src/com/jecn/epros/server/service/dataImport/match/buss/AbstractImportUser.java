package com.jecn.epros.server.service.dataImport.match.buss;

import java.util.List;

import com.caucho.hessian.client.HessianProxyFactory;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 
 * 读取用户数据，转换成通用接口的类
 * 
 * 读取方式有hessian、xml、excel等方式
 * 
 * @author Administrator
 * 
 */
public abstract class AbstractImportUser {

	/** 配置信息 */
	protected AbstractConfigBean configBean = null;
	protected HessianProxyFactory factory = null;

	public AbstractImportUser(AbstractConfigBean cnfigBean) {
		if (cnfigBean == null) {
			throw new IllegalArgumentException(
					"AbstractImportUser类的AbstractImportUser的cnfigBean参数不正确");
		}

		this.configBean = cnfigBean;
	}

	protected AbstractImportUser() {

	}

	/**
	 * 
	 * 获取Hessian工厂对象
	 * 
	 * @return
	 */
	protected HessianProxyFactory getHessianProxyFactory() {
		if (configBean == null) {
			return null;
		}
		if (factory == null) {
			// 获取Hessian工厂对象
			return new HessianProxyFactory();
		}
		return factory;

	}

	/**
	 * 
	 * 获取部门数据集合
	 * 
	 * @return
	 */
	public abstract List<DeptMatchBean> getDeptBeanList() throws Exception;

	/**
	 * 
	 * 获取岗位及岗位组数据集合
	 * 
	 * @return
	 */
	public abstract List<PosMatchBean> getPosBeanList() throws Exception;

	/**
	 * 
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	public abstract List<UserMatchBean> getUserBeanList() throws Exception;

	public AbstractConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(AbstractConfigBean configBean) {
		this.configBean = configBean;
	}

	public HessianProxyFactory getFactory() {
		return factory;
	}

	public void setFactory(HessianProxyFactory factory) {
		this.factory = factory;
	}

}
