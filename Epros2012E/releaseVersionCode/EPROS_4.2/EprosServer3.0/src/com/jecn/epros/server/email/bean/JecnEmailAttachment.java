package com.jecn.epros.server.email.bean;

import java.sql.Blob;

/**
 * 邮件内容表
 */

public class JecnEmailAttachment implements java.io.Serializable {

	private String id;
	/** 附件内容 和数据库关联*/
	private Blob attachment;
	/** 附件内容不与数据库关联**/
	private byte[] attachmentByte;
	/** 附件名称*/
	private String attachmentName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Blob getAttachment() {
		return attachment;
	}
	public void setAttachment(Blob attachment) {
		this.attachment = attachment;
	}
	public byte[] getAttachmentByte() {
		return attachmentByte;
	}
	public void setAttachmentByte(byte[] attachmentByte) {
		this.attachmentByte = attachmentByte;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

}