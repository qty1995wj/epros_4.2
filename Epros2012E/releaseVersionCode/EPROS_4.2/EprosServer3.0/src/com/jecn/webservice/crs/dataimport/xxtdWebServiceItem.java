package com.jecn.webservice.crs.dataimport;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 信息通达webService 接口数据获取
 * 
 *@author admin
 * @date 2016-7-1上午10:06:07
 */
public class xxtdWebServiceItem {

	private static final Logger log = Logger.getLogger(xxtdWebServiceItem.class);

	private static final String USERNAME = "bps";
	private static final String PASSWORD = "bps.2015";
	// 可以筛选接口提供的数据字段
	private static final String FILTER = "";
	// 错误的岗位数据编号
	private static final BigDecimal ERROR_POS_NUM = new BigDecimal("0");

	private static final String ERROR_EMPLOYEE_STATUS = "2";

	/**
	 * 人员同步获取 webservice 组织 接口获取的数据集合
	 */
	public static List<ORG_VIEWLine> syncOrgViews() throws Exception {
		// 分页读取客户数据
		BigInteger pageNo = new BigInteger("1");
		HrPublisherPortTypeProxy proxy = new HrPublisherPortTypeProxy();
		ORG_VIEWDataPackage pac;
		List<ORG_VIEWLine> allLines = new ArrayList<ORG_VIEWLine>();
		do {
			pac = proxy.queryORG_VIEWData(USERNAME, PASSWORD, FILTER, pageNo);
			allLines.addAll(Arrays.asList(pac.getDataList()));
			pageNo = pageNo.add(new BigInteger("1"));

		} while (pac.getHasMore());
		log.info("syncOrgViews： 获取组织数据成功！");
		return allLines;
	}

	/**
	 * 人员同步获取 webservice 人员岗位 接口获取的数据集合
	 */
	public static List<USER_POS_VIEWLine> syncUserPosViews() throws Exception {
		// 分页读取客户数据
		BigInteger pageNo = new BigInteger("1");
		HrPublisherPortTypeProxy proxy = new HrPublisherPortTypeProxy();
		USER_POS_VIEWDataPackage pac;
		USER_POS_VIEWLine[] dataList;
		List<USER_POS_VIEWLine> allLines = new ArrayList<USER_POS_VIEWLine>();
		do {
			pac = proxy.queryUSER_POS_VIEWData(USERNAME, PASSWORD, FILTER, pageNo);
			dataList = pac.getDataList();
			for (int i = dataList.length - 1; i >= 0; i--) {
				if (isDirty(dataList[i])) {
					dataList[i] = null;
				}
			}
			allLines.addAll(Arrays.asList(dataList));
			pageNo = pageNo.add(new BigInteger("1"));
		} while (pac.getHasMore());
		log.info("syncOrgViews： 获取组织数据成功！");
		return allLines;
	}

	private static boolean isDirty(USER_POS_VIEWLine userPOSVIEWLine) {
		return !userPOSVIEWLine.getC_employeestatus().equals(ERROR_EMPLOYEE_STATUS)
				|| userPOSVIEWLine.getPos_num().compareTo(ERROR_POS_NUM) == 0;
	}
}
