package epros.designer.gui.workflow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.service.JecnDesignerImpl;
import epros.designer.util.ConnectionPool;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 
 * 
 * 流程图/流程架构（流程地图）/组织图自动保存到本地
 * 
 * 
 */
public class WorkFlowBakTimer implements ActionListener {
	private Logger log = Logger.getLogger(WorkFlowBakTimer.class);

	// 备份定时器类
	private static WorkFlowBakTimer bakTimer;
	// 定时器
	private Timer timer;
	// 执行间隔(分钟)
	private int min = 5;
	// 是否启动 true 启动
	private boolean isRun = false;

	private WorkFlowBakTimer() {
		initTimerConfig();
		// 开启定时器
		timer = new Timer(min * 60 * 1000, this);
	}

	/**
	 * 初始化定时器参数
	 * 
	 * @author weidp
	 * @date 2014-10-27 下午05:17:52
	 */
	private void initTimerConfig() {
		try {
			// 获取计时器是否启动标志
			isRun = getIsRun(ConnectionPool.getConfigAciton().selectValue(5,
					ConfigItemPartMapMark.saveRecovery.toString()));

			// 获取计时器触发间隔（分钟）
			String spaceMin = ConnectionPool.getConfigAciton().selectValue(5,
					ConfigItemPartMapMark.saveRecoveryValue.toString());
			if (spaceMin != null && !"".equals(spaceMin.trim())) {
				min = Integer.valueOf(spaceMin);
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 是否启动 value =1 true
	 * 
	 * @author weidp
	 * @date 2014-10-28 上午10:59:04
	 * @param value
	 * @return
	 */
	private boolean getIsRun(String value) {
		return "1".equals(value) ? true : false;
	}

	/**
	 * 重新启动定时器
	 * 
	 * @author weidp
	 * @date 2014-10-27 下午05:27:04
	 */
	public void restart() {
		if (isRun) {
			this.timer.restart();
		} else {
			if (timer.isRunning()) {
				this.timer.stop();
			}
		}
	}

	/**
	 * 刷新计时器
	 * 
	 * @author weidp
	 * @date 2014-10-27 下午06:48:41
	 */
	public void refresh() {
		// 初始化参数
		initTimerConfig();
		restart();
	}

	/**
	 * 获取单例
	 * 
	 * @author weidp
	 * @date 2014-10-8 下午03:27:02
	 * @return
	 */
	public static WorkFlowBakTimer getInstance() {
		if (bakTimer == null) {
			bakTimer = new WorkFlowBakTimer();
		}
		return bakTimer;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// 备份
		runningBackUp();
		// 定时任务重启
		restart();
	}

	/**
	 * 进行备份操作
	 * 
	 * @author weidp
	 * @date 2014-10-28 上午10:43:59
	 */
	public void runningBackUp() {
		// 如果备份功能被开启，执行保存到本地
		if (isRun) {
			// 停止计时器
			JecnDesignerImpl designer = new JecnDesignerImpl();
			designer.saveWorkFlowAsBak(JecnDrawMainPanel.getMainPanel()
					.getWorkflow());
		}
	}

	/**
	 * 重新设置定时器的值
	 * 
	 * @author weidp
	 * @date 2014-10-27 下午07:20:55
	 * @param configItemBean
	 */
	public void reset(JecnConfigItemBean[] configItemBean) {
		// 是否启动自动备份
		if (configItemBean[0] != null) {
			this.isRun = getIsRun(configItemBean[0].getValue());

		}
		// 时间间隔
		if (configItemBean[1] != null) {
			this.min = Integer.valueOf(configItemBean[1].getValue());
		}
		// 重新启动
		restart();
	}
}
