package com.jecn.epros.domain.report;

/**
 * Created by user on 2017/4/20.
 */
public class InventoryBaseInfo {

    private int total;
    private int pub;
    private int notPub;
    private int approve;
    private int maxLevel;

    public int getApprove() {
        return approve;
    }

    public void setApprove(int approve) {
        this.approve = approve;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPub() {
        return pub;
    }

    public void setPub(int pub) {
        this.pub = pub;
    }

    public int getNotPub() {
        return notPub;
    }

    public void setNotPub(int notPub) {
        this.notPub = notPub;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }
}
