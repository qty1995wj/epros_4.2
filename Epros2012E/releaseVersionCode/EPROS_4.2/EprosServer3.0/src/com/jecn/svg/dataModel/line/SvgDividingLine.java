package com.jecn.svg.dataModel.line;

import com.jecn.svg.dataModel.SvgBaseGraph;

public class SvgDividingLine extends SvgBaseGraph {

	private SvgLineData data = new SvgLineData();

	public Long getEndX() {
		return data.getEndX();
	}

	public Long getEndY() {
		return data.getEndY();
	}

	public Long getStartX() {
		return data.getStartX();
	}

	public Long getStartY() {
		return data.getStartY();
	}

	public void setEndX(Long endX) {
		data.setEndX(endX);
	}

	public void setEndY(Long endY) {
		data.setEndY(endY);
	}

	public void setStartX(Long startX) {
		data.setStartX(startX);
	}

	public void setStartY(Long startY) {
		data.setStartY(startY);
	}

}
