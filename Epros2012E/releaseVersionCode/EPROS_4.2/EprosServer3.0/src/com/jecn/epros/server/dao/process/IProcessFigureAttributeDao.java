package com.jecn.epros.server.dao.process;

import com.jecn.epros.server.bean.process.JecnFlowFigureNow;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessFigureAttributeDao extends IBaseDao<JecnFlowFigureNow, Long> {

}
