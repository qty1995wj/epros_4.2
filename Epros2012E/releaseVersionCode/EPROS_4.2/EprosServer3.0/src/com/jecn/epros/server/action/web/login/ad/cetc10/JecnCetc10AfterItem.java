package com.jecn.epros.server.action.web.login.ad.cetc10;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

/**
 * 10所配置文件读取类（EprosServer3.0/src/cfgFile/swiee/swieeLdapServerConfig.properties）
 * 
 */
public class JecnCetc10AfterItem {

	private static Logger log = Logger.getLogger(JecnCetc10AfterItem.class);

	// 单点登录webservice
	public static String SSO_URL = null;
	// 任务代办
	public static String TASK_URL = null;

	public static String USER_NAME = null;
	public static String PASS_DORD = null;

	public static String FireFox_URL = null;
	public static String FireFox_URL1 = null;

	/**
	 * 读取29所配置文件
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午02:38:20
	 */
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/cetc10/cetc10.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			String ssoUrl = props.getProperty("ssoUrl");
			if (!JecnCommon.isNullOrEmtryTrim(ssoUrl)) {
				JecnCetc10AfterItem.SSO_URL = ssoUrl;
			}

			// taskUrl
			String taskUrl = props.getProperty("taskUrl");
			if (!JecnCommon.isNullOrEmtryTrim(taskUrl)) {
				JecnCetc10AfterItem.TASK_URL = taskUrl;
			}

			String username = props.getProperty("username");
			if (!JecnCommon.isNullOrEmtryTrim(username)) {
				JecnCetc10AfterItem.USER_NAME = username;
			}

			String password = props.getProperty("password");
			if (!JecnCommon.isNullOrEmtryTrim(password)) {
				JecnCetc10AfterItem.PASS_DORD = password;
			}
			FireFox_URL = props.getProperty("FireFox_URL");
			FireFox_URL1 = props.getProperty("FireFox_URL1");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}

	private static ResourceBundle cetc10Config;

	static {
		cetc10Config = ResourceBundle.getBundle("cfgFile/cetc10/cetc10");
	}

	public static String getValue(String key) {
		return cetc10Config.getString(key);
	}
}
