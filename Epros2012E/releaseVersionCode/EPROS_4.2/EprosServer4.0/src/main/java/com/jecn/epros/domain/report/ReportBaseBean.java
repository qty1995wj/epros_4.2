package com.jecn.epros.domain.report;

public class ReportBaseBean {

    /**
     * 流程架构、部门的id
     **/
    protected Long id;
    protected String name;
    /**
     * 0部门 1流程架构
     **/
    protected Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
