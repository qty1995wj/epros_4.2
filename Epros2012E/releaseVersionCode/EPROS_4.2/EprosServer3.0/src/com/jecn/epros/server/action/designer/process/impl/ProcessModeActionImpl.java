package com.jecn.epros.server.action.designer.process.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.process.IProcessModeAction;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessData;
import com.jecn.epros.server.bean.process.ProcessMapData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IProcessModeService;

/*******************************************************************************
 * 流程模板管理
 * 
 * @author 2012-06-26
 * 
 */
public class ProcessModeActionImpl implements IProcessModeAction {
	private IProcessModeService processModeService;
	private IFlowStructureService flowStructureService;

	public IFlowStructureService getFlowStructureService() {
		return flowStructureService;
	}

	public void setFlowStructureService(IFlowStructureService flowStructureService) {
		this.flowStructureService = flowStructureService;
	}

	public IProcessModeService getProcessModeService() {
		return processModeService;
	}

	public void setProcessModeService(IProcessModeService processModeService) {
		this.processModeService = processModeService;
	}

	/**
	 * 新建流程模板管理
	 * 
	 * @param jecnFlowStructureMode
	 * @param isXorY
	 *            0横向图，1 纵向图
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long addFlowModel(JecnFlowStructureT jecnFlowStructureT, int isXorY) throws Exception {
		return processModeService.saveFlowModel(jecnFlowStructureT, isXorY);

	}

	@Override
	public void deleteFlowModel(List<Long> listIds) throws Exception {
		processModeService.deleteFlowModel(listIds);
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId) throws Exception {
		processModeService.moveNodes(listIds, pId);
	}

	@Override
	public void reName(String newName, Long id, Long updatePersonId) throws Exception {
		processModeService.reName(newName, id, updatePersonId);
	}

	@Override
	public List<JecnTreeBean> searchByName(String name, Long projectId, int type) throws Exception {
		return processModeService.searchByName(name, type);
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId) throws Exception {
		processModeService.updateSortNodes(list, pId);
	}

	@Override
	public List<JecnTreeBean> getAllFlowMapModels() throws Exception {
		return processModeService.getAllFlowMapModels();
	}

	@Override
	public List<JecnTreeBean> getFlowModels(Long flowMapModelId) throws Exception {
		return processModeService.getFlowModels(flowMapModelId);
	}

	@Override
	public JecnFlowStructureT getFlowMapPerInfo(Long perId) throws Exception {
		return processModeService.get(perId);
	}

	@Override
	public ProcessOpenMapData getProcessMapModeData(long id) throws Exception {
		return flowStructureService.getProcessMapData(id, 0);
	}

	@Override
	public ProcessOpenData getProcessModeData(long id) throws Exception {
		ProcessOpenData processOpenData = flowStructureService.getProcessData(id, 0);
		return processOpenData;
	}

	@Override
	public void saveProcessMapMode(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePersionId) throws Exception {
		flowStructureService.saveProcessMap(mapData, flowImgeBytes, updatePersionId);
	}

	@Override
	public void saveProcessMode(ProcessData processData, byte[] flowImgeBytes, Long updatePersionId) throws Exception {
		flowStructureService.saveProcess(processData, flowImgeBytes, updatePersionId);
	}

	/**
	 * 
	 * 打开流程架构-集成关系图
	 * 
	 * @param id
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	@Override
	public ProcessOpenMapData getProcessMapRelatedData(long id, long projectId) throws Exception {
		return flowStructureService.getProcessMapRelatedData(id, projectId);
	}

	/**
	 * 保存集成关系图
	 * 
	 * @param mapData
	 * @param flowImgeBytes
	 * @param updatePeopleId
	 * @throws Exception
	 */
	@Override
	public void saveMapRelatedProcess(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePeopleId)
			throws Exception {
		flowStructureService.saveMapRelatedProcess(mapData, flowImgeBytes, updatePeopleId);
	}

}
