package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.dao.dataImport.excelorDB.IUserDataFileDownDao;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class UserDataFileDownBuss extends AbstractFileSaveDownBuss {
	/** 对应dao层处理类 */
	private IUserDataFileDownDao userDataFileDownDao = null;

	/**
	 * 
	 * 对外接口，导出人员数据（excel方式）
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean downUserDataExcel() {
		return getUserDataByDB();
	}

	/**
	 * 
	 * 获取从数据库人员数据生成excel保存到本地
	 * 
	 * @throws Exception
	 * 
	 */
	private boolean getUserDataByDB() {

		try {
			// 获取数据库数据
			// 部门
			List<DeptBean> deptBeanList = userDataFileDownDao.selectDeptList();
			// 人员岗位
			List<UserBean> userBeanList = userDataFileDownDao.selectUserPosList();
			// 人员导出模板文件
			this.setOutputExcelModelPath(SyncTool.getOutputExcelModelPath());
			// 人员导出数据文件
			this.setOutputExcelDataPath(SyncTool.getOutPutExcelDataPath());

			// 从DB中读数据写入到excel再保存到本地
			return editExcel(deptBeanList, userBeanList ,false);

		} catch (Exception ex) {
			log.error("人员数据导出失败：UserDataFileDownBuss类getUserDataByDB方法 " + ex.getMessage());

			return false;
		}

	}

	/**
	 * 
	 * 导出文件路径
	 * 
	 * @return String 文件路径
	 */
	protected String getOutFilepath() {
		return SyncTool.getOutPutExcelDataPath();
	}

	/**
	 * 
	 * 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	protected String getOutFileName() {
		return SyncConstant.OUTPUT_EXCEL_DATA_FILE_NAME;
	}

	public IUserDataFileDownDao getUserDataFileDownDao() {
		return userDataFileDownDao;
	}

	public void setUserDataFileDownDao(IUserDataFileDownDao userDataFileDownDao) {
		this.userDataFileDownDao = userDataFileDownDao;
	}

}
