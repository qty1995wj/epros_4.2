package com.jecn.epros.service.process;

import java.io.Serializable;

public class ProcessActiveWebBean implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 活动ID
     */
    private Long activeFigureId;
    /**
     * 活动编号
     */
    private String activeId;
    /**
     * 活动名称
     */
    private String activeName;

    public Long getActiveFigureId() {
        return activeFigureId;
    }

    public void setActiveFigureId(Long activeFigureId) {
        this.activeFigureId = activeFigureId;
    }

    public String getActiveId() {
        return activeId;
    }

    public void setActiveId(String activeId) {
        this.activeId = activeId;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }


}
