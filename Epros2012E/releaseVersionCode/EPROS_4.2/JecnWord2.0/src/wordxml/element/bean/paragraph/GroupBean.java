package wordxml.element.bean.paragraph;

/**
 * 图文框属性 单位全部为厘米
 * 
 * @author hyl
 */
public class GroupBean {

	/*** 尺寸 ***/
	private GroupW wType = GroupW.exact;
	private double wValue = 1;

	private GroupH hType = GroupH.exact;
	private double hValue = 1;

	/*** 水平 ***/
	// 水平位置
	private double x = 1;
	// 相对于
	private Hanchor hanchor = Hanchor.page;
	// 距离正文
	private double hspace = 1;

	/*** 垂直 ***/
	// 垂直位置
	private double y = 1;
	// 相对于
	private Vanchor vanchor = Vanchor.page;
	// 距离正文
	private double vspace = 1;
	/** 锁定标记 **/
	private boolean isLock = false;
	private GroupAlign align;
	private GroupVlign vlign;

	/**
	 * 尺寸
	 * 
	 * @param wType
	 *            宽度
	 * @param wValue
	 *            设置值
	 * @param hType
	 *            高度
	 * @param hValue
	 *            设置值
	 * @return
	 */
	public GroupBean setSize(GroupW wType, double wValue, GroupH hType, double hValue) {
		this.wType = wType;
		this.wValue = wValue;
		this.hType = hType;
		this.hValue = hValue;
		return this;
	}

	/**
	 * 水平
	 * 
	 * @param x
	 *            位置
	 * @param hanchor
	 *            相对于
	 * @param hspace
	 *            距正文
	 * @return
	 */
	public GroupBean setHorizontal(double x, Hanchor hanchor, double hspace) {
		this.x = x;
		this.hanchor = hanchor;
		this.hspace = hspace;
		return this;
	}

	/**
	 * 水平
	 * 
	 * @param align
	 *            位置
	 * @param hanchor
	 *            相对于
	 * @param hspace
	 *            距正文
	 * @return
	 */
	public GroupBean setHorizontal(GroupAlign align, Hanchor hanchor, double hspace) {
		this.align = align;
		this.hanchor = hanchor;
		this.hspace = hspace;
		return this;
	}

	/**
	 * 垂直
	 * 
	 * @param x
	 *            位置
	 * @param hanchor
	 *            相对于
	 * @param hspace
	 *            距正文
	 * @return
	 */
	public GroupBean setVertical(double y, Vanchor vanchor, double vspace) {
		this.y = y;
		this.vanchor = vanchor;
		this.vspace = vspace;
		return this;
	}

	/**
	 * 垂直
	 * 
	 * @param x
	 *            位置
	 * @param hanchor
	 *            相对于
	 * @param hspace
	 *            距正文
	 * @return
	 */
	public GroupBean setVertical(GroupVlign vlign, Vanchor vanchor, double vspace) {
		this.vlign = vlign;
		this.vanchor = vanchor;
		this.vspace = vspace;
		return this;
	}

	/**
	 * 锁定标记
	 * 
	 * @param isLock
	 * @return
	 */
	public GroupBean lock(boolean isLock) {
		this.isLock = isLock;
		return this;
	}

	/**
	 * 宽度类型
	 * 
	 * @author user
	 * 
	 */
	public enum GroupW {
		// 自动设置 固定值
		auto, exact
	}

	/**
	 * 高度类型
	 * 
	 * @author user
	 * 
	 */
	public enum GroupH {
		// 自动设置 最小 固定值
		auto, min, exact
	}

	/**
	 * 水平相对于
	 * 
	 * @author user
	 * 
	 */
	public enum Hanchor {
		// 页面
		page("page"),
		// 页边距
		margin("margin"),
		// 栏
		text("text");
		private String value = "page";

		private Hanchor(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * 垂直相对于
	 * 
	 * @author user
	 * 
	 */
	public enum Vanchor {
		// 页面
		page("page"),
		// 页边距
		margin("margin"),
		// 段落
		text("text");
		private String value = "page";

		private Vanchor(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum GroupAlign {
		left, right, center, inside, outside
	}

	public enum GroupVlign {
		top, bottom, center, inside, outside
	}

	public GroupW getwType() {
		return wType;
	}

	public void setwType(GroupW wType) {
		this.wType = wType;
	}

	public double getwValue() {
		return wValue;
	}

	public void setwValue(double wValue) {
		this.wValue = wValue;
	}

	public GroupH gethType() {
		return hType;
	}

	public void sethType(GroupH hType) {
		this.hType = hType;
	}

	public double gethValue() {
		return hValue;
	}

	public void sethValue(double hValue) {
		this.hValue = hValue;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public Hanchor getHanchor() {
		return hanchor;
	}

	public void setHanchor(Hanchor hanchor) {
		this.hanchor = hanchor;
	}

	public double getHspace() {
		return hspace;
	}

	public void setHspace(double hspace) {
		this.hspace = hspace;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Vanchor getVanchor() {
		return vanchor;
	}

	public void setVanchor(Vanchor vanchor) {
		this.vanchor = vanchor;
	}

	public double getVspace() {
		return vspace;
	}

	public void setVspace(double vspace) {
		this.vspace = vspace;
	}

	public boolean isLock() {
		return isLock;
	}

	public void setLock(boolean isLock) {
		this.isLock = isLock;
	}

	public GroupAlign getAlign() {
		return align;
	}

	public void setAlign(GroupAlign align) {
		this.align = align;
	}

	public GroupVlign getVlign() {
		return vlign;
	}

	public void setVlign(GroupVlign vlign) {
		this.vlign = vlign;
	}

	

}
