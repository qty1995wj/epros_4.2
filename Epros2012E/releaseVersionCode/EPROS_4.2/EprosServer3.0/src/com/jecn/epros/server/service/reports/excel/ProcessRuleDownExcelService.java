package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableSheet;

import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.ProcessRoleBean;

/**
 * 
 * 流程角色数统计
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessRuleDownExcelService extends BaseDownExcelService {
	private List<ProcessRoleBean> processRoleList = null;

	public ProcessRuleDownExcelService() {
		this.mouldPath = getPrefixPath() + "mould/processRuleReport.xls";
		this.startDataRow = 2;
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (processRoleList == null) {
			return true;
		}

		try {
			// 第一行
			this.addFirstRowData(dataSheet);

			// 数据行号
			int i = 0;
			// 第三行开始
			for (ProcessRoleBean roleBean : processRoleList) {// 0~15,16以上
				// 如果角色数为0 不下载
				if (roleBean.getAllCount() == 0) {
					continue;
				}
				// 一种角色数对应的数据集合
				List<ProcessWebBean> processWebList = roleBean
						.getProcessWebList();
				// 角色数
				String roleCount = null;
				int roleInt = roleBean.getRoleCount();
				if (roleInt >= 16) {
					roleCount = String.valueOf(">15");
				} else {
					roleCount = String.valueOf(roleInt);
				}

				if (processWebList == null || processWebList.size() == 0) {
					int row = startDataRow + i;
					// 角色数
					dataSheet.addCell(new Label(0, row, roleCount));
					// 合计
					dataSheet.addCell(new Label(5, row, "0"));
					i++;
					continue;
				}

				// 合计
				String allCount = String.valueOf(roleBean.getAllCount());
				// 待合并开始行号
				int startFirstRow = startDataRow + i;

				for (ProcessWebBean bean : processWebList) {
					int row = startDataRow + i;
					// 角色数
					dataSheet.addCell(new Label(0, row, roleCount));
					// 流程名称
					dataSheet.addCell(new Label(1, row, bean.getFlowName()));
					// 流程编号
					dataSheet.addCell(new Label(2, row, bean.getFlowIdInput()));
					// 流程责任人
					dataSheet
							.addCell(new Label(3, row, bean.getResPeopleName()));
					// 责任部门
					dataSheet.addCell(new Label(4, row, bean.getOrgName()));
					// 合计
					dataSheet.addCell(new Label(5, row, allCount));
					i++;
				}
				// 合并单元格，参数格式（开始列，开始行，结束列，结束行）
				// 角色数
				dataSheet.mergeCells(0, startFirstRow, 0, startDataRow + i - 1);
				// 合计
				dataSheet.mergeCells(5, startFirstRow, 5, startDataRow + i - 1);
			}
			return true;
		} catch (Exception e) {
			log.error("ProcessRuleDownExcelService类editExcle方法", e);
			return false;
		}
	}

	public List<ProcessRoleBean> getProcessRoleList() {
		return processRoleList;
	}

	public void setProcessRoleList(List<ProcessRoleBean> processRoleList) {
		this.processRoleList = processRoleList;
	}
}
