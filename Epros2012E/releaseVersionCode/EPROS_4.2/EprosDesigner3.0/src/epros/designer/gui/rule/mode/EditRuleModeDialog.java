package epros.designer.gui.rule.mode;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.bean.rule.RuleModeTitleBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 
 * 工具栏-设置-制度模板-编辑模板
 * 
 * @author zhangj
 * 
 */
public class EditRuleModeDialog extends RuleModeDialog {
	private static Logger log = Logger.getLogger(EditRuleModeDialog.class);
	private JecnTree jTree = null;
	private JecnTreeBean selectBean = null;
	private RuleModeBean ruleModeBean = null;

	public EditRuleModeDialog(JecnTreeBean selectBean, JecnTree jTree) {
		this.setTitle(JecnProperties.getValue("editMode"));
		this.selectBean = selectBean;
		this.jTree = jTree;
		try {
			List<RuleModeTitleBean> titleList = ConnectionPool.getRuleModeAction().getRuleModeTitles(selectBean.getId());
			this.titleTable.addRows(getRuleModeTitles(titleList));
			ruleModeBean = ConnectionPool.getRuleModeAction().getRuleModeById(selectBean.getId());
			this.getNameField().setText(ruleModeBean.getModeName());
		} catch (Exception e) {
			log.error("EditRuleModeDialog is error", e);
		}

	}

	public Vector<Vector<String>> getRuleModeTitles(List<RuleModeTitleBean> titleList){
		if (titleList != null && titleList.size() > 0) {
			Vector<Vector<String>> vs = new Vector<Vector<String>>();
			for (RuleModeTitleBean tb : titleList) {
				Vector<String> v = new Vector<String>();
				v.add(tb.getSortId().toString());
				v.add(tb.getTitleName());
				v.add(tb.getEnName());
				v.add(formatTitleTypeToString(tb.getType()));
				// 必填项
				v.add(tb.getRequiredType() == 1 ? JecnProperties.getValue("task_notEmptyBtn") : JecnProperties
						.getValue("task_emptyBtn"));
				vs.add(v);
				v = null;
			}

			return vs;
		}
		return new Vector<Vector<String>>();
		
	}
	private String formatTitleTypeToString(int t) {
		if (t == 0) {
			// "内容"
			return JecnProperties.getValue("content");
		} else if (t == 1) {
			//文件表单
			return JecnProperties.getValue("fileForm");
		} else if (t == 2) {
			//流程表单
			return JecnProperties.getValue("flowForm");
		} else if (t == 3) {
			//流程地图表单
			return JecnProperties.getValue("flowMapForm");
		} else if (t == 4) {
			//标准表单
			return JecnProperties.getValue("standarForm");
		} else if (t == 5) {
			//风险表单
			return JecnProperties.getValue("riskForm");
		} else if (t == 6) {
			//制度表单
			return JecnProperties.getValue("ruleForm");
		}
		return null;
	}
	
	@Override
	public boolean validateNodeRepeat(String name) {
		// 获得该节点父节点下有没有相同的名称
		JecnTreeNode selectNode = JecnTreeCommon.getTreeNode(selectBean, jTree);
		if (selectNode != null) {
			return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
		} else {
			try {
				return ConnectionPool.getRuleModeAction().validateRepeatNameEidt(name, selectBean.getId(),
						selectBean.getPid());
			} catch (Exception e) {
				log.error("EditRuleModeDialog validateNodeRepeat is error", e);
			}
		}
		return false;
	}

	@Override
	public void savaData(List<RuleModeTitleBean> titleList) throws Exception {
		this.ruleModeBean.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		this.ruleModeBean.setModeName(this.getNameField().getText().trim());
		ConnectionPool.getRuleModeAction().updateRuleMode(ruleModeBean, titleList);
		this.isOperation = true;
		this.dispose();

	}

	@Override
	public Vector<Vector<String>> getContent() {

		return null;
	}

}
