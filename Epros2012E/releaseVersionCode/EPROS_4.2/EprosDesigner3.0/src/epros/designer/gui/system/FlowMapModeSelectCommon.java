package epros.designer.gui.system;

import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;

public class FlowMapModeSelectCommon extends SingleSelectCommon {
	private static Logger log = Logger.getLogger(FlowMapModeSelectCommon.class);

	public FlowMapModeSelectCommon(int rows, JecnPanel infoPanel, Insets insets, JecnTreeBean jecnTreeBean,
			JecnDialog jecnDialog, String labName,boolean isRequest) {
		super(rows, infoPanel, insets, jecnTreeBean, jecnDialog, labName,isRequest);
	}

	@Override
	protected List<JecnTreeBean> searchName(String name, Long projectId) {
		try {
			return ConnectionPool.getProcessModeAction().searchByName(name, JecnConstants.projectId, 0);
		} catch (Exception e) {
			log.error("FlowMapModeSelectCommon searchName is error", e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	protected void selectButPerformed(List<JecnTreeBean> list, JSearchTextField jecnField, Long id) {
		JecnDesignerCommon.setFlowMode(jecnField, 0, list);
	}

}
