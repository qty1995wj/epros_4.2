package com.jecn.epros.domain.risk;

import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.ConfigUtils;

import java.util.List;

public class RiskBaseInfoBean {
    /**
     * 编号
     */
    private String riskCode;
    /**
     * 风险描述或目录名
     */
    private String riskName;
    /**
     * 等级
     */
    private int grade;
    /**
     * 标准化控制
     */
    private String standardControl;

    private String riskPointGuides;

    private String riskType;//风险类别

    private boolean showRiskType;

    public boolean isShowRiskType() {
        return ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.isShowRiskType);
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    private List<RiskRelatedTargetDescription> relatedTargets;

    public String getGradeStr() {
        return ConfigUtils.getGradeStrByType(grade);
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getRiskName() {
        return Common.replaceWarp(riskName);
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getStandardControl() {
        return Common.replaceWarp(standardControl);
    }

    public void setStandardControl(String standardControl) {
        this.standardControl = standardControl;
    }

    public List<RiskRelatedTargetDescription> getRelatedTargets() {
        return relatedTargets;
    }

    public void setRelatedTargets(List<RiskRelatedTargetDescription> relatedTargets) {
        this.relatedTargets = relatedTargets;
    }


    public String getRiskPointGuides() {
        return Common.replaceWarp(riskPointGuides);
    }

    public void setRiskPointGuides(String riskPointGuides) {
        this.riskPointGuides = riskPointGuides;
    }
}
