package com.jecn.epros.server.action.rmi.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.service.process.IViewAppletService;
import com.jecn.epros.server.service.reports.IReportDownExcelService;
import com.jecn.epros.server.service.reports.IWebReportsService;
import com.jecn.epros.server.webBean.reports.ProcessDetailBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIFollowBean;
import com.jecn.epros.server.webBean.reports.ProcessScanOptimizeBean;
import com.jecn.epros.service.IChartRPCService;
import com.jecn.svg.dataModel.SvgPanel;

public class ChartRPCServiceImpl implements IChartRPCService {

	private Logger log = LoggerFactory.getLogger(ChartRPCServiceImpl.class);

	@Autowired
	private IViewAppletService viewAppletService;

	@Autowired
	private IWebReportsService webReportsService;

	@Autowired
	private IReportDownExcelService reportDownExcelService;

	private SvgPanel findviewAppletService(boolean isPub, Long id, Long userId, Long projectId, String type,
			int languageType) throws Exception {
		SvgPanel svgPanel = null;
		if ("process".equals(type)) {
			if (isPub) {
				svgPanel = viewAppletService.getSvgBean(id, userId, projectId, languageType);
			} else {
				svgPanel = viewAppletService.getSvgBeanT(id, userId, projectId, null, languageType);
			}
		} else if ("processMap".equals(type)) {
			if (isPub) {
				svgPanel = viewAppletService.getSvgMapBean(id, userId, projectId, languageType);
			} else {
				svgPanel = viewAppletService.getSvgMapBeanT(id, userId, projectId, null, languageType);
			}
		} else if ("organization".equals(type)) {
			svgPanel = viewAppletService.getSvgOrg(id, userId, projectId, languageType);
		} else if ("integrationChart".equals(type)) {
			if (isPub) {
				svgPanel = viewAppletService.getSvgIntegrationChartBean(id, userId, projectId, languageType);
			} else {
				svgPanel = viewAppletService.getSvgIntegrationChartBeanT(id, userId, projectId, null, languageType);
			}
		}
		return svgPanel;
	}

	private SvgPanel findHistoryviewAppletService(boolean isPub, Long id, Long userId, Long projectId, Long historyId,
			String type, int languageType) throws Exception {
		SvgPanel svgPanel = null;
		if ("process".equals(type)) {
			svgPanel = viewAppletService.getSvgBeanT(id, userId, projectId, historyId, languageType);
		} else if ("processMap".equals(type)) {
			svgPanel = viewAppletService.getSvgMapBeanT(id, userId, projectId, historyId, languageType);
		} else if ("integrationChart".equals(type)) {
			svgPanel = viewAppletService.getSvgIntegrationChartBeanT(id, userId, projectId, historyId, languageType);
		}
		return svgPanel;
	}

	@Override
	public String getChartJSON(Long id, String type, Long userId, Map<String, Object> options) {
		String json = "";
		try {
			boolean isPub = (Boolean) options.get("isPub");
			Long projectId = (Long) options.get("projectId");
			Long historyId = (Long) options.get("historyId");
			int languageType = (Integer) options.get("languageType");
			SvgPanel svgPanel = null;
			if (historyId == null || historyId == 0) {// 文控Id为空查询
				svgPanel = findviewAppletService(isPub, id, userId, projectId, type, languageType);
			} else {// 不为空查询文控表
				svgPanel = findHistoryviewAppletService(isPub, id, userId, projectId, historyId, type, languageType);
			}
			initChartConfigItem(svgPanel);
			svgPanel.setHttpURL(options.get("httpURL") == null ? "" : options.get("httpURL").toString());
			svgPanel.setProjectName(JecnConfigTool.getNewProjectName());
			svgPanel.setFlowId(id + "");
			svgPanel.setIsPub(isPub ? 1 : 0);
			json = toJSON(svgPanel);
		} catch (Exception e) {
			log.error("获取流程图时出现异常", e);
		}

		return json;
	}

	/**
	 * 加载 SVG 所需要的配置
	 */
	private void initChartConfigItem(SvgPanel svgPanel) {
		// 元素链接显示位置：0 中上 ，1 右下 默认中上
		svgPanel.getEproConfigItem().put("elementLocation", JecnConfigTool.getElementLocation());
		// 浏览端是否显示 办理时限 时间轴
		svgPanel.getEproConfigItem().put("isShowTimeLimit", String.valueOf(JecnConfigTool.isShowTimeLimit()));

	}

	private static String toJSON(SvgPanel panel) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		return mapper.writeValueAsString(panel);
	}

	@Override
	public ByteFileResult downloadProcessDetailList(Map<String, Object> param) {
		ByteFileResult result = new ByteFileResult();
		try {
			int type = (Integer) param.get("type");
			ProcessDetailBean processDetailBean = webReportsService.findProcessDetailBean(param);
			processDetailBean.setType(type);
			result.setBytes(reportDownExcelService.downloadProcessDetail(processDetailBean));
			if (type == 0) {
				result.setFileName("流程清单报表.xls");
			} else {
				result.setFileName("制度清单报表.xls");
			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setResultMessage("下载异常");
			log.error("RMI downloadProcessDetailList()异常", e);
		}
		return result;
	}

	@Override
	public ByteFileResult downloadProcessScanOptimize(Map<String, Object> param) {
		ByteFileResult result = new ByteFileResult();
		try {
			int type = (Integer) param.get("type");
			ProcessScanOptimizeBean processScanOptimize = webReportsService.findProcessScanOptimizeBean(param);
			processScanOptimize.setType(type);
			result.setBytes(reportDownExcelService.downloadProcessScanOptimize(processScanOptimize));
			if (type == 0) {
				result.setFileName("流程审视优化统计表.xls");
			} else {
				result.setFileName("制度审视优化统计表.xls");
			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setResultMessage("下载异常");
			log.error("RMI downloadProcessScanOptimize()异常", e);
		}
		return result;
	}

	@Override
	public ByteFileResult downloadProcessKPIFollow(List<Long> orgIds, List<Long> mapIds, String startTime,
			String endTime, Long projectId) {
		ByteFileResult result = new ByteFileResult();
		try {
			ProcessKPIFollowBean processKPIFlowBean = webReportsService.findProcessKPIFollowBean(concatIds(orgIds),
					concatIds(mapIds), startTime, endTime, projectId);
			result.setBytes(reportDownExcelService.downloadProcessKPIFollow(processKPIFlowBean));
			result.setFileName("流程KPI跟踪表.xls");
		} catch (Exception e) {
			result.setSuccess(false);
			result.setResultMessage("下载异常");
			log.error("RMI downloadProcessKPIFollow()异常", e);
		}
		return result;
	}

	private String concatIds(List<Long> ids) {
		if (ids == null || ids.size() == 0) {
			return "";
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < ids.size(); i++) {
			if (i == 0) {
				buf.append(ids.get(i));
			} else {
				buf.append("," + ids.get(i));
			}
		}
		return buf.toString();
	}

}
