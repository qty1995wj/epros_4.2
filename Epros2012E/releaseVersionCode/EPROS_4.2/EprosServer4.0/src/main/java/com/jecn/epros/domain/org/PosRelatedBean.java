package com.jecn.epros.domain.org;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;

public class PosRelatedBean {
    private Long id;
    private String name;
    /**
     * 0是岗位、1 是制度
     */
    private int type;

    public LinkResource getLink() {
        if (type == 1) {
            return new LinkResource(id, name, ResourceType.RULE, TreeNodeUtils.NodeType.position);
        } else {
            return new LinkResource(id, name, ResourceType.POSITION);
        }
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    @JsonIgnore
    public int getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }
}
