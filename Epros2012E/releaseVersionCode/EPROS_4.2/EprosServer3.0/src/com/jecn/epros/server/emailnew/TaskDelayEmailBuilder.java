package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.util.JecnUtil;

public class TaskDelayEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {
		return createEmail(user);
	}

	private EmailBasicInfo createEmail(JecnUser user) {
		
		// 管理员点击页面的链接直接打开任务管理页面
		String httpUrl ="/loginMail.action?accessType=mailOpenTaskManagement&mailPeopleId="+user.getPeopleId();
		String subject =JecnUtil.getValue("delayTaskEmailTitle");
		String content =JecnUtil.getValue("delayTaskEmailContent");
		EmailContentBuilder builder=new EmailContentBuilder();
		builder.setContent(content);
		builder.setResourceUrl(httpUrl);
		EmailBasicInfo emailBasicInfo=new EmailBasicInfo();
		emailBasicInfo.setSubject(subject);
		emailBasicInfo.setContent(builder.buildContent());
		emailBasicInfo.addRecipients(user);
		return emailBasicInfo;

	}

	

}
