package epros.draw.visio;

/**
 * 连接线Bean
 * 
 * @author fuzhh 2013-6-28
 * 
 */
public class JecnConnectingLineBean {

	/** 线段开始点X */
	private String beginX;
	/** 线段开始图形 */
	private String beginXStr;
	/** 线段开始点Y */
	private String beginY;
	/** 线段开始图形 */
	private String beginYStr;
	/** 线段结束点x */
	private String endX;
	/** 线段结束图形 */
	private String endXStr;
	/** 线段结束点y */
	private String endY;
	/** 线段结束图形 */
	private String endYStr;

	public String getBeginX() {
		return beginX;
	}

	public void setBeginX(String beginX) {
		this.beginX = beginX;
	}

	public String getBeginXStr() {
		return beginXStr;
	}

	public void setBeginXStr(String beginXStr) {
		this.beginXStr = beginXStr;
	}

	public String getBeginY() {
		return beginY;
	}

	public void setBeginY(String beginY) {
		this.beginY = beginY;
	}

	public String getBeginYStr() {
		return beginYStr;
	}

	public void setBeginYStr(String beginYStr) {
		this.beginYStr = beginYStr;
	}

	public String getEndX() {
		return endX;
	}

	public void setEndX(String endX) {
		this.endX = endX;
	}

	public String getEndXStr() {
		return endXStr;
	}

	public void setEndXStr(String endXStr) {
		this.endXStr = endXStr;
	}

	public String getEndY() {
		return endY;
	}

	public void setEndY(String endY) {
		this.endY = endY;
	}

	public String getEndYStr() {
		return endYStr;
	}

	public void setEndYStr(String endYStr) {
		this.endYStr = endYStr;
	}

}
