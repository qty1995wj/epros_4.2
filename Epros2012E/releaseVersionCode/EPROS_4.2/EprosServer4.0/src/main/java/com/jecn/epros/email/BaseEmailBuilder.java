package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.email.EmailTip;

import java.util.List;

public abstract class BaseEmailBuilder {

    private List<JecnUser> users;
    private Object[] data;
    private EmailTip tip;

    /**
     * 构建Email数据对象
     *
     * @return
     */
    public abstract List<EmailBasicInfo> buildEmail();

    /**
     * 设置生成Email所需的数据
     *
     * @param users
     * @param data
     */
    public void setData(List<JecnUser> users, EmailTip tip, Object... data) {
        if (users == null || users.size() == 0) {
            throw new IllegalArgumentException("BaseEmailBuilder setData is error：[ users == null || users.size() == 0 ]");
        }
        this.users = users;
        this.data = data;
        this.tip = tip;
    }

    public List<JecnUser> getUsers() {
        return users;
    }

    public Object[] getData() {
        return data;
    }

    public EmailTip getTip() {
        return tip;
    }


}
