package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 
 * 基于Excel技术同步数据
 * 
 */
public class ImportUserByExcel extends AbstractImportUser {
	private final Log log = LogFactory.getLog(ImportUserByExcel.class);
	/** 部门 */
	private List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
	/** 岗位 */
	private List<UserBean> userBeanList = new ArrayList<UserBean>();
	/** 执行读取标识(true:表示执行过；false：标识没有执行过) */
	private boolean readedFlag = false;

	public ImportUserByExcel(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	public ImportUserByExcel() {
	}

	public List<DeptBean> getDeptBeanList() throws Exception {

		// 执行读取excel数据
		readExcel();

		return deptBeanList;
	}

	public List<UserBean> getUserBeanList() throws Exception {

		// 执行读取excel数据
		readExcel();

		return userBeanList;
	}

	/**
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	private void readExcel() throws Exception {
		// 执行过就不执行
		if (readedFlag) {
			return;
		}

		// Excel文件读取，部门、岗位、人员数据都全部取出来
		String errorInfo = readExcelFromLocal();

		if (errorInfo != null) {
			throw new IOException(SyncErrorInfo.IMPORT_FILE_FORMAT_VAIL);
		}

	}

	/**
	 * 
	 * Excel文件读取
	 * 
	 * @throws Exception
	 */
	private String readExcelFromLocal() {
		// 读取本地excel数据
		FileInputStream inputStream = null;
		// 创建excel工作表
		Workbook rwb = null;
		try {
			// 读取本地excel数据
			inputStream = new FileInputStream(SyncTool.getExcelDataPath());
			// 创建excel工作表
			rwb = Workbook.getWorkbook(inputStream);
			Sheet[] sheets = rwb.getSheets();

			// 验证excel是否正确
			String retErrorString = checkExcel(sheets);
			if (retErrorString != null) {
				return retErrorString;
			}

			// 获取部门数据
			readDeptSheet(sheets[0]);
			// 获取岗位以及人员数据
			readUserPosSheet(sheets[1]);
			// 表示执行过
			readedFlag = true;
			return null;
		} catch (Exception e) {
			log.error("读取本地Excel文件时错误：类ImportUserByExcel方法readExcelFromLocal" + e.getMessage());

			return SyncErrorInfo.SYSTEM_ERRROR;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("读取本地Excel文件时错误：类ImportUserByExcel方法readExcelFromLocal" + e.getMessage());
					return SyncErrorInfo.SYSTEM_ERRROR;
				}
			}
			if (rwb != null) {
				rwb.close();
			}
		}
	}

	/**
	 * 
	 * 读取部门sheet中内容
	 * 
	 * @param deptSheet
	 */
	private void readDeptSheet(Sheet deptSheet) {
		// 第四行开始读数据
		for (int i = SyncConstant.WIRTER_ROW_NUM; i < deptSheet.getRows(); i++) {
			DeptBean deptBean = new DeptBean();
			// 部门编号
			String deptNum = getCellText(deptSheet.getCell(0, i));
			// if (SyncTool.isNullOrEmtryTrim(deptNum)) {
			// continue;
			// }

			// 部门名称
			String deptName = getCellText(deptSheet.getCell(1, i));
			// 上级部门编号
			String perDeptNum = getCellText(deptSheet.getCell(2, i));

			deptBean.setDeptNum(deptNum);
			deptBean.setDeptName(deptName);
			deptBean.setPerDeptNum(perDeptNum);

			deptBeanList.add(deptBean);
		}
	}

	/**
	 * 
	 * 读取岗位、人员sheet中内
	 * 
	 * @param userPosSheet
	 */
	private void readUserPosSheet(Sheet userPosSheet) {
		log.info("userPosSheet.getRows()" + userPosSheet.getRows());

		// 第四行开始读数据
		for (int i = SyncConstant.WIRTER_ROW_NUM; i < userPosSheet.getRows(); i++) {

			UserBean userBean = new UserBean();

			// 登录名称
			String userNum = getCellText(userPosSheet.getCell(0, i));
			// 真实姓名
			String trueName = getCellText(userPosSheet.getCell(1, i));
			// 任职岗位编号
			String posNum = getCellText(userPosSheet.getCell(2, i));
			// 任职岗位名称
			String posName = getCellText(userPosSheet.getCell(3, i));
			// 岗位所属部门编号
			String deptNum = getCellText(userPosSheet.getCell(4, i));
			// 邮箱地址
			String email = getCellText(userPosSheet.getCell(5, i));
			// 内外网邮件标识(内网:0 外网:1)
			String emailType = getCellText(userPosSheet.getCell(6, i));
			// 联系电话
			String phone = getCellText(userPosSheet.getCell(7, i));

			// 登录名称
			userBean.setUserNum(userNum);
			// 真实姓名
			userBean.setTrueName(trueName);
			// 任职岗位编号
			userBean.setPosNum(posNum);
			// 任职岗位名称
			userBean.setPosName(posName);
			// 岗位所属部门编号
			userBean.setDeptNum(deptNum);
			// 邮箱地址
			userBean.setEmail(email);
			// 邮箱、邮箱类型不等于空且是“0”，“1”
			if (SyncTool.isEmailTypeStr(emailType)) {
				// 内外网邮件标识(内网:0 外网:1)
				userBean.setEmailType(Integer.valueOf(emailType));
			}
			// 联系电话
			userBean.setPhone(phone);

			// 属性值都为空情况
			if (userBean.isAttributesNull()) {
				continue;
			}
			userBeanList.add(userBean);
		}
	}

	/**
	 * 
	 * 验证excel是否正确
	 * 
	 * @param sheets
	 *            导入的excel的sheet数组
	 * @return String
	 */
	private String checkExcel(Sheet[] sheets) {

		if (sheets.length != 2) {
			return SyncErrorInfo.IMPORT_FILE_FORMAT_VAIL;
		}

		// 部门sheet校验
		String retDept = checkDeptExcel(sheets[0]);
		if (retDept != null) {
			return retDept;
		}

		// 人员及岗位sheet校验
		String retUserPos = checkUserPosExcel(sheets[1]);
		if (retUserPos != null) {
			return retUserPos;
		}
		return null;
	}

	/**
	 * 
	 * 部门sheet校验
	 * 
	 * @param sheet
	 * @return
	 */
	private String checkDeptExcel(Sheet sheet) {

		if (SyncTool.isNullObj(sheet) || sheet.getRows() < 3 || sheet.getRow(1).length != 3) {
			return SyncErrorInfo.IMPORT_FILE_FORMAT_VAIL;
		}

		// 获取第二行数据
		Cell[] cellArr = sheet.getRow(1);
		// 部门编号
		String deptNum = getCellText(cellArr[0]);
		// 部门名称
		String deptName = getCellText(cellArr[1]);
		// 上级部门编号
		String perDeptNum = getCellText(cellArr[2]);
		if (!"部门编号".equals(deptNum) || !"部门名称".equals(deptName) || !"上级部门编号".equals(perDeptNum)) {
			return SyncErrorInfo.IMPORT_FILE_FORMAT_VAIL;
		}
		return null;
	}

	/**
	 * 
	 * 人员及岗位sheet校验
	 * 
	 * @param sheet
	 * @return
	 */
	private String checkUserPosExcel(Sheet sheet) {
		if (SyncTool.isNullObj(sheet) || sheet.getRows() < 3 || sheet.getRow(1).length != 8) {
			return SyncErrorInfo.IMPORT_FILE_FORMAT_VAIL;
		}

		// 获取第二行数据
		Cell[] cellArr = sheet.getRow(1);

		// 登录名称
		String loginName = getCellText(cellArr[0]);
		// 真实姓名
		String trueName = getCellText(cellArr[1]);
		// 任职岗位编号
		String posNum = getCellText(cellArr[2]);
		// 任职岗位名称
		String posName = getCellText(cellArr[3]);
		// 岗位所属部门编号
		String deptNum = getCellText(cellArr[4]);
		// 邮箱地址
		String email = getCellText(cellArr[5]);
		// 内外网邮件标识(内网:0 外网:1)
		String emailType = getCellText(cellArr[6]);
		// 联系电话
		String phone = getCellText(cellArr[7]);

		if (!"登录名称".equals(loginName) || !"真实姓名".equals(trueName) || !"任职岗位编号".equals(posNum)
				|| !"任职岗位名称".equals(posName) || !"岗位所属部门编号".equals(deptNum) || !"邮箱地址".equals(email)
				|| !"内外网邮件标识(内网:0 外网:1)".equals(emailType) || !"联系电话".equals(phone)) {
			return SyncErrorInfo.IMPORT_FILE_FORMAT_VAIL;
		}
		return null;
	}

	/**
	 * 
	 * 获取单元格内容
	 * 
	 * @param cell
	 * @return
	 */
	private String getCellText(Cell cell) {
		if (SyncTool.isNullObj(cell)) {
			return null;
		} else {
			String cellContent = cell.getContents();
			return SyncTool.emtryToNull(cellContent);
		}
	}

}
