package com.jecn.epros.server.bean.file;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class FileInfoBean implements Serializable {
	/** 文件对象 */
	private JecnFileBeanT jecnFileBeanT;
	/** 岗位权限 */
	private List<JecnTreeBean> listPos;
	/** 部门权限 */
	private List<JecnTreeBean> lisOrg;
	/** 岗位组权限 */
	private List<JecnTreeBean> listPosGroup;
	/** 文件相关风险 */
	public List<JecnTreeBean> riskRelateds;
	/** 文件相关标准 */
	public List<JecnTreeBean> standardsRelateds;

	public List<JecnTreeBean> getRiskRelateds() {
		return riskRelateds;
	}

	public void setRiskRelateds(List<JecnTreeBean> riskRelateds) {
		this.riskRelateds = riskRelateds;
	}

	public List<JecnTreeBean> getStandardsRelateds() {
		return standardsRelateds;
	}

	public void setStandardsRelateds(List<JecnTreeBean> standardsRelateds) {
		this.standardsRelateds = standardsRelateds;
	}

	public List<JecnTreeBean> getListPosGroup() {
		return listPosGroup;
	}

	public void setListPosGroup(List<JecnTreeBean> listPosGroup) {
		this.listPosGroup = listPosGroup;
	}

	public JecnFileBeanT getJecnFileBeanT() {
		return jecnFileBeanT;
	}

	public void setJecnFileBeanT(JecnFileBeanT jecnFileBeanT) {
		this.jecnFileBeanT = jecnFileBeanT;
	}

	public List<JecnTreeBean> getListPos() {
		return listPos;
	}

	public void setListPos(List<JecnTreeBean> listPos) {
		this.listPos = listPos;
	}

	public List<JecnTreeBean> getLisOrg() {
		return lisOrg;
	}

	public void setLisOrg(List<JecnTreeBean> lisOrg) {
		this.lisOrg = lisOrg;
	}
}