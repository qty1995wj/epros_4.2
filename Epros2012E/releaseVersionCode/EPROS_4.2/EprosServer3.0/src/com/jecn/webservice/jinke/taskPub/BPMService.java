/**
 * BPMService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public interface BPMService extends javax.xml.rpc.Service {
    public java.lang.String getBPMServiceSoapAddress();

    public com.jecn.webservice.jinke.taskPub.BPMServiceSoap_PortType getBPMServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.jinke.taskPub.BPMServiceSoap_PortType getBPMServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
