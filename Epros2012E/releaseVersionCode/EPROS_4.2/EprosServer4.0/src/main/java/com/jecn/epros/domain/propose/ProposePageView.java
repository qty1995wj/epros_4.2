package com.jecn.epros.domain.propose;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 合理化建议页面显示bean
 *
 * @author user
 */
public class ProposePageView {

    /*** 合理化建议主键ID */
    private String id;
    /*** 合理化建议类型/相关文件类型：0流程，1制度，2流程架构，3文件，4风险，5标准 */
    private int type;
    /*** 关联的文件ID */
    private Long relationId;
    /*** 关联文件name */
    private String relationName;
    /*** 建议内容 */
    private String proposeContent;
    /*** 回复内容 */
    private String replyContent;
    /*** 合理化建议当前状态 0 未读 1 已读 2 采纳 3拒绝 */
    private int state;
    /*** 回复人 */
    private Long replyPeopleId;
    /*** 回复人名称 */
    private String replyName;
    /*** 回复日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date replyTime;
    /*** 创建人 */
    private Long createPeopleId;
    /*** 创建人名称**/
    private String createPeopleName;
    /*** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date createTime;
    /*** 0设置的处理人可见1所有人可见**/
    private Integer showType = 0;
    /*** 页面按钮显示配置 */
    private ProposeButtonShowConfig buttonShowConfig;
    /*** 附件**/
    private ProposeFile proposeFile;

    /**
     * 创建人是否离职
     *
     * @return
     */
    public boolean createPeopleLeave() {
        if (createTime != null && createPeopleId == null) {
            return true;
        }
        return false;
    }

    /**
     * 回复人是否离职
     *
     * @return
     */
    public boolean replyPeopleLeave() {
        if (org.apache.commons.lang3.StringUtils.isNotBlank(replyContent) && replyPeopleId == null) {
            return true;
        }
        return false;
    }

    //----------------


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public String getProposeContent() {
        return proposeContent;
    }

    public void setProposeContent(String proposeContent) {
        this.proposeContent = proposeContent;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Long getReplyPeopleId() {
        return replyPeopleId;
    }

    public void setReplyPeopleId(Long replyPeopleId) {
        this.replyPeopleId = replyPeopleId;
    }

    public String getReplyName() {
        return replyName;
    }

    public void setReplyName(String replyName) {
        this.replyName = replyName;
    }

    public Date getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Date replyTime) {
        this.replyTime = replyTime;
    }

    public Long getCreatePeopleId() {
        return createPeopleId;
    }

    public void setCreatePeopleId(Long createPeopleId) {
        this.createPeopleId = createPeopleId;
    }

    public String getCreatePeopleName() {
        return createPeopleName;
    }

    public void setCreatePeopleName(String createPeopleName) {
        this.createPeopleName = createPeopleName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public ProposeButtonShowConfig getButtonShowConfig() {
        return buttonShowConfig;
    }

    public void setButtonShowConfig(ProposeButtonShowConfig buttonShowConfig) {
        this.buttonShowConfig = buttonShowConfig;
    }

    public ProposeFile getProposeFile() {
        return proposeFile;
    }

    public void setProposeFile(ProposeFile proposeFile) {
        this.proposeFile = proposeFile;
    }

    public Integer getShowType() {
        return showType;
    }

    public void setShowType(Integer showType) {
        this.showType = showType;
    }
}
