package com.jecn.epros.server.bean.process;

import java.sql.Blob;
import java.util.Date;

/**
 * 流程支撑文件 内容表
 * 
 * @author ZXH
 * @date 2017-4-21上午09:57:19
 */
public class FlowFileContent implements java.io.Serializable {
	/** 主键 */
	private Long id;
	/** 关联的流程ID */
	private Long relatedId;
	/** 文件内容 存放数据库 */
	private Blob fileStream;
	private byte[] bytes;
	/** 文件路径 存放本地 */
	private String filePath;
	/** 创建时间 */
	private Date createTime;
	/** 创建人 */
	private Long createPeopleId;
	/** 文件名称 */
	private String fileName;
	/** 文件版本存储标识 0:本地；1：数据库 */
	private int isVersionLocal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public Blob getFileStream() {
		return fileStream;
	}

	public void setFileStream(Blob fileStream) {
		this.fileStream = fileStream;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getIsVersionLocal() {
		return isVersionLocal;
	}

	public void setIsVersionLocal(int isVersionLocal) {
		this.isVersionLocal = isVersionLocal;
	}

}
