<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<style>
		 .borderStyle {
			border: 1px;
			border-color: #CDCDCD;
			border-style: solid;
		 }
		</style>
		<script type="text/javascript">
              document.body.onresize=function(){
			    setShowProcessFileDivWH('ruleFileDiv');
			 }
		</script>
	</head>

	<body>
		<div id="div" class="div" align="center">
			<div id="ruleAttribute"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; width: 95%">
				<table width="98%">
					<tr>
						<td align="right">
						<%-- 下载 --%>
					            <a style="cursor: pointer;" href="downloadRuleFile.action?ruleId=<s:property value='ruleId'/>&isPub=<s:property value='listJecnRuleOperationCommon[0].showPub'/>"> <img
								src="${basePath}images/common/downLoadDocument.gif" border="0"></img> </a>
						</td>
					</tr>
					<tr>
						<td align="center"
							style="background-color: #E4E4E4; height: 30px; font: bold 15px ;">
							<s:property value="ruleShowName" />
						</td>
					</tr>
				</table>
               <div id="ruleFileDiv" style="width: 100%;overflow-y: scroll;">
				<table width="98%">
					<s:iterator value="listJecnRuleOperationCommon" id="str">
						<tr
							style="background-color: #F8F8F8; font: bold 12px ; height: 30px;"
							align="left">
							<td>
								<s:property value="ruleTitleT.orderNumber" />、<s:property value="ruleTitleT.titleName" />
							</td>
						</tr>
						<!-- 内容 -->
						<s:if test="ruleTitleT.type == 0">
							<tr>
								<td style="padding-left: 10px;word-break: break-all;" align="left">
									<s:if test="showPub==false">
										<s:if test="ruleContentT.contentStr==''">
											<s:text name="none" />
										</s:if>
										<s:else>
										    <s:property value="ruleContentT.contentStr" escape="false"/>
										</s:else>
									</s:if>
								<s:else>
										<s:if test="ruleContent.contentStr==''">
											<s:text name="none" />
										</s:if>
										<s:else>
										    <s:property value="ruleContent.contentStr" escape="false"/>
										</s:else>
								</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 文件表单 -->
						<s:elseif test="ruleTitleT.type == 1">
							<tr>
								<td style="padding-left: 10px;" align="left">
								   <%--正式表--%>
									<s:if test="listRuleFile.size > 0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="25%" height="25" class="t8"
													style="word-break: break-all;">
													<s:text name="fileNumber" />
												</td>
												<td bgcolor="#DDDDDD" class="t8"
													style="word-break: break-all;">
													<s:text name="fileName" />
												</td>
											</tr>
											<s:iterator value="listRuleFile">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td width="25%" class="t5" style="word-break: break-all;">
														<s:property value="fileInput" />
													</td>
													<td class="t5" style="word-break: break-all;">
													<a href="file.action?from=rule&reqType=public&visitType=downFile&fileId=<s:property value='ruleFileId'/>&isPub=${isPub}" style="cursor: pointer;" target='_blank' class="table">
														<s:property value="fileName" />
													</a>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<%--临时表--%>
									<s:elseif test="listFileBean.size > 0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="25%" height="25" class="t8"
													style="word-break: break-all;">
													<s:text name="fileNumber" />
												</td>
												<td bgcolor="#DDDDDD" class="t8"
													style="word-break: break-all;">
													<s:text name="fileName" />
												</td>
											</tr>
											<s:iterator value="listFileBean">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td width="25%" class="t5" style="word-break: break-all;">
														<s:property value="fileInput" />
													</td>
													<td class="t5" style="word-break: break-all;">
													<a href="file.action?from=rule&reqType=public&visitType=downFile&fileId=<s:property value='ruleFileId'/>&isPub=${isPub}" style="cursor: pointer;" target='_blank' class="table">
														<s:property value="fileName" />
													</a>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:elseif>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:elseif>
						<!-- 流程表单 -->
						<s:elseif test="ruleTitleT.type == 2">
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="listJecnFlowCommon.size > 0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="25%" height="25" class="t8"
													style="word-break: break-all;">
													<s:text name="processNumber" />
												</td>
												<td bgcolor="#DDDDDD" class="t8"
													style="word-break: break-all;">
													<s:text name="processName" />
												</td>
											</tr>
											<s:iterator value="listJecnFlowCommon">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td width="25%" class="t5" style="word-break: break-all;">
														<s:property value="flowNumber" />
													</td>
													<td class="t5" style="word-break: break-all;">
													<a href="process.action?type=process&flowId=<s:property value='flowId'/>&isPub=${isPub}"
															   target='_blank' style="cursor: pointer;" class="table">
														<s:property value="flowName" />
													</a>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:elseif>
						<!-- 流程地图表单 -->
						<s:elseif test="ruleTitleT.type == 3">
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="listJecnFlowMapCommon.size > 0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="25%" height="25" class="t8"
													style="word-break: break-all;">
												<s:text name="processMapNumbersC" />
												</td>
												<td bgcolor="#DDDDDD" class="t8"
													style="word-break: break-all;">
													<s:text name="processMapName" />
												</td>
											</tr>
											<s:iterator value="listJecnFlowMapCommon">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td width="25%" class="t5" style="word-break: break-all;">
														<s:property value="flowNumber" />
													</td>
													<td class="t5" style="word-break: break-all;">
													<a href="process.action?type=processMap&flowId=<s:property value='flowId'/>&isPub=${isPub}"
															   target='_blank' style="cursor: pointer;" class="table">
														<s:property value="flowName" />
													</a>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:elseif>
						<!-- 标准表单 -->
						<s:elseif test="ruleTitleT.type == 4">
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="listJecnStandarCommon.size > 0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="25%" height="25" class="t8"
													style="word-break: break-all;">
													<s:text name="type" />
												</td>
												<td bgcolor="#DDDDDD" class="t8"
													style="word-break: break-all;">
													<s:text name="standardName" />
												</td>
											</tr>
											<s:iterator value="listJecnStandarCommon">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
<%--													<td width="25%" class="t5" style="word-break: break-all;">--%>
<%--														<a href="standard.action?standardType=1&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"--%>
<%--															   target='_blank' style="cursor: pointer;" class="table">--%>
<%--														<s:property value="standarName" />--%>
<%--														</a>--%>
<%--													</td>--%>
<%--													<td class="t5" style="word-break: break-all;">--%>
<%--														<s:if test="standaType==1">--%>
<%--															<s:text name="fileStandar" />--%>
<%--														</s:if>--%>
<%--														<s:elseif test="standaType== 2">--%>
<%--															<s:text name="flowStandar" />--%>
<%--														</s:elseif>--%>
<%--														<s:elseif test="standaType== 3">--%>
<%--															<s:text name="flowMapStandar" />--%>
<%--														</s:elseif>--%>
<%--														<s:elseif test="standaType == 4">--%>
<%--															<s:text name="standarClause" />--%>
<%--														</s:elseif>--%>
<%--														<s:elseif test="standaType == 5">--%>
<%--															<s:text name="clauseRequest" />--%>
<%--														</s:elseif>--%>
<%--													</td>--%>
												<s:if test="standaType==1">
													<td class="t5" style="word-break: break-all;">
													<s:text name="fileStandar" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?from=rule&reqType=public&standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:if>
												<s:elseif test="standaType== 2">
													<td class="t5" style="word-break: break-all;">
													<s:text name="flowStandar" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
												<s:elseif test="standaType== 3">
													<td class="t5" style="word-break: break-all;">
													<s:text name="flowMapStandar" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>&fileId=<s:property value='relatedId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
												<s:elseif test="standaType == 4">
													<td class="t5" style="word-break: break-all;">
													<s:text name="standarClause" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
												<s:elseif test="standaType == 5">
													<td class="t5" style="word-break: break-all;">
													<s:text name="clauseRequest" />
													</td>
													<td width="25%" class="t5" style="word-break: break-all;">
														<a
															href="standard.action?standardType=<s:property value="standaType" />&standardId=<s:property value='standarId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="standarName" /> </a>
													</td>
												</s:elseif>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:elseif>
						<!-- 风险表单 -->
						<s:elseif test="ruleTitleT.type == 5">
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="listJecnRiskCommon.size > 0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="25%" height="25" class="t8"
													style="word-break: break-all;">
													<s:text name="riskNumberC" />
												</td>
												<td bgcolor="#DDDDDD" class="t8"
													style="word-break: break-all;">
													<s:text name="riskNameC" />
												</td>
											</tr>
											<s:iterator value="listJecnRiskCommon">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td width="25%" class="t5" style="word-break: break-all;">
														<s:property value="riskNumber" />
													</td>
													<td class="t5" style="word-break: break-all;">
													<a href="getRiskById.action?riskDetailId=<s:property value='riskId'/>"
															   target='_blank' style="cursor: pointer;" class="table">
														<s:property value="riskName" />
													</a>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:elseif>
					</s:iterator>
				</table>
				
			  </div>
			</div>
		</div>
	</body>
</html>