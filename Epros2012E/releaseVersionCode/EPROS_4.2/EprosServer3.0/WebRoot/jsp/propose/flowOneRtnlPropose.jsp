<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
		<link rel="stylesheet" type="text/css"
	href="ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="ext/resources/css/xtheme-gray.css" />
<link rel="stylesheet" type="text/css" href="css/info.css" />
<script type="text/javascript" src="ext/adapter/ext/ext-base.js" charset="UTF-8"></script>
<script type="text/javascript" src="ext/ext-all.js" charset="UTF-8"></script>
<script type="text/javascript" src="${basePath}jquery/jquery-1.6.2.js" charset="UTF-8"></script>
<script type="text/javascript" src="${basePath}jquery/ajaxfileupload.js" charset="UTF-8"></script>
<script type="text/javascript" src="${basePath}js/epros_zh_CN.js" charset="UTF-8"></script>
<script type="text/javascript" src="${basePath}js/rtnlPropose.js"></script>
<script type="text/javascript" src="${basePath}js/rule/singleRuleRtnlPropose.js"></script>
<script type="text/javascript" src="${basePath}js/rule/ruleSys.js"></script>
<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
<script type="text/javascript">
var basePath = "${basePath}";
</script>
	</head>
	<!-- 单个合理化建议的页面，虽然使用了iteartor循环但是由于其中只有一个元素。所以可以当做一个来使用。 -->
	<body>
		<div id="allProposeShowInfoId">

			<div id="rtnlProposeShowContent"
				style="overflow-x: hidden;">
                <!-- 循环所有的建议内容 -->
                <s:iterator value="flowRtnlProposeList" status="st">
					<TABLE id="tablePropose<s:property value="jecnRtnlPropose.id" />"
						style="width: 100%; border-width: 1px; border-style: solid; border-color: RGB(199, 199, 199); margin-top: 10px; margin-bottom: 15px;"
						border="0" cellspacing="0" cellpadding="0">
						<tr>
							<!-- 修改后的合理化建议页面 Time:2013-10-28 -->
							<td colspan="5" style="padding: 5px 10px 5px 20px;">
								<span
									style="vertical-align: top; color: gray; padding-top: 7px;"><s:if
										test="jecnRtnlPropose.createPeopleState == 1">
										<s:text name="peopleGone" />
									</s:if> <!-- 建议创建人 --> <s:else>
										<s:property value="jecnRtnlPropose.createUpPeopoleName" />
									</s:else> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <!-- 建议创建时间 --> <s:date
										name="jecnRtnlPropose.createTime" format="yyyy-MM-dd" />
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <!-- 建议状态 --> <span
									id="proStateId<s:property value="jecnRtnlPropose.id" />">
										<s:if test="jecnRtnlPropose.state == 0">
											<s:text name="noReadPropose" />
										</s:if> <s:elseif test="jecnRtnlPropose.state==1">
											<s:text name="readedPropose" />
										</s:elseif> 
										<s:elseif test="jecnRtnlPropose.state == 2">
											<s:text name="acceptPropose" />
										</s:elseif>
										<s:elseif test="jecnRtnlPropose.state == 3">
											<s:text name="refusePropose" />
										</s:elseif>
										
										 </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
								<!-- 流程编号 -->
								<span style="color: gray;"><s:if
										test="jecnRtnlPropose.rtnlType == 0">
										<s:text name="processNumberC" />
									</s:if>
									<s:elseif test="jecnRtnlPropose.rtnlType == 1">
										<s:text name="ruleNumberC" />
									</s:elseif>
								</span>
								<span><s:property value="flowNum" /> </span>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- 流程名称 -->
								<span style="color: gray;"><s:if
										test="jecnRtnlPropose.rtnlType == 0">
										<s:text name="processNameC" />
										<a
											href="${basePath}process.action?type=process&flowId=<s:property value='jecnRtnlPropose.relationId'/>"
											target='_blank' class="flowPropose"> <s:property
												value="flowName" /> </a>
									</s:if> <s:elseif test="jecnRtnlPropose.rtnlType == 1">
										<s:text name="ruleNameC" />
										<s:if test="ruleIsDir == 1">
											<a
												href="${basePath}rule.action?type=ruleModeFile&ruleId=<s:property value='jecnRtnlPropose.relationId'/>&ruleName=<s:property value='flowName'/>"
												target='_blank' class="flowPropose"> <s:property
													value="flowName" /> </a>
										</s:if>
										<s:elseif test="ruleIsDir == 2">
											<a
												href="${basePath}rule.action?type=ruleFile&ruleId=<s:property value='jecnRtnlPropose.relationId'/>&fileId=<s:property value='ruleFileId'/>&ruleName=<s:property value='flowName'/>"
												target='_blank' class="flowPropose"> <s:property
													value="flowName" /> </a>
										</s:elseif>
									</s:elseif>
								</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- 流程责任部门 -->
								<span style="color: gray;"><s:text
										name="responsibilityDepartmentC" />
								</span>
								<a
									href="${basePath}organization.action?orgId=<s:property value='orgId'/>"
									target='_blank' class="flowPropose"> <s:property
										value="orgName" /> </a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- 流程责任人 -->
								<span style="color: gray;"><s:text
										name="responsiblePersonsC" />
								</span>
								<span><s:property value="resFlowName" /> </span>
							</td>
						</tr>
						<tr>
							<td align="left" style="padding-top: 5px; vertical-align: top;">
								<span style="color: gray; padding-left: 20px;"><s:text
										name="mesnonullgcontent" />
								</span>
							</td>
							<td colspan="4"
								style="padding-top: 5px; padding-right: 10px; width: 93%; word-break: break-all;">
								<s:if
									test="jecnRtnlPropose.proposeContent == null || jecnRtnlPropose.proposeContent == ''">
									<s:text name="none" />
								</s:if>
								<s:else>
									<s:property value="jecnRtnlPropose.proposeContent"
										escape="false" />
								</s:else>
							</td>
						</tr>
						<tr>
							<td colspan="5" style="padding-top: 5px;">
								<span style="color: gray; margin-left: 20px;"><s:text
										name="accessories" />
								</span>
								<s:if test="jecnRtnlPropose.listFileNames == null">
									<s:text name="none" />
								</s:if>
								<s:else>
									<a
										href="downloadProposeFile.action?proposeFileId=<s:property  value="jecnRtnlPropose.listFileIds" />"
										class="flowPropose" target='_blank'><s:property
											value="jecnRtnlPropose.listFileNames" /> </a>
								</s:else>
							</td>
						</tr>
						
						<!-- 按钮显示开始-->
						<tr>
							<td colspan="5" align="right" style="padding-right: 10px;">
								<s:if test="jecnRtnlPropose.buttonShowConfig.acceptIsShow">
									<s:if test="jecnRtnlPropose.buttonShowConfig.acceptIsGray">
									    <!-- 采纳置灰 -->
										<img src="${basePath}images/propose/acceptGray.gif"
											id='acceptImgId<s:property value="jecnRtnlPropose.id" />' />
									</s:if>
									<s:else>
									    <!-- 采纳 -->
										<img src="${basePath}images/propose/accept.gif"
											style="cursor: pointer;"
											id="acceptImgId<s:property value="jecnRtnlPropose.id" />"
											onclick="updateToAccept('<s:property  value="jecnRtnlPropose.id" />',2)" />
									</s:else>
								</s:if>
							&nbsp;&nbsp;
							<s:if test="jecnRtnlPropose.buttonShowConfig.cancelAcceptIsShow">
							    <s:if test="jecnRtnlPropose.buttonShowConfig.cancelAcceptIsGray" >
							            <!-- 拒绝置灰 -->
										<img src="${basePath}images/propose/cancelAcceptGray.gif"
											id="cancelAcceptImgId<s:property value="jecnRtnlPropose.id" />"
											/>
								</s:if>
								<s:else>
								<!-- 取消采纳 -->
										<img src="${basePath}images/propose/cancelAccept.gif"
											style="cursor: pointer;"
											id="cancelAcceptImgId<s:property value="jecnRtnlPropose.id" />"
											onclick="updateToCancelAccept('<s:property  value="jecnRtnlPropose.id" />',3)" />
									   
								</s:else>
							</s:if>
							&nbsp;&nbsp;
							<!--隐藏出采纳和拒绝以外的按钮-->
							<div style="display:none;">
							<s:if test="jecnRtnlPropose.buttonShowConfig.replayIsShow" >
							<!-- 回复置灰 暂时没有-->
							    <s:if test="jecnRtnlPropose.buttonShowConfig.replayIsGray" >
										
								</s:if>	
								<s:else>
								
								<img src="${basePath}images/propose/replay.gif"
										style="cursor: pointer;"
										id="replyImgId<s:property value="jecnRtnlPropose.id" />"
										onclick="replyContentFunction('<s:property  value="jecnRtnlPropose.id" />')" />
										
								</s:else>
							</s:if>
								&nbsp;&nbsp;
							<s:if test="jecnRtnlPropose.buttonShowConfig.readIsShow">
										<s:if
										test="jecnRtnlPropose.buttonShowConfig.readIsGray">
										<!-- 已读置灰 -->
										<img src="${basePath}images/propose/readGray.gif"
											id="readeImgId<s:property value="jecnRtnlPropose.id" />"/>
										</s:if>
								     <s:else>
									    <!-- 已读 -->
										<img src="${basePath}images/propose/readed.gif"
											style="cursor: pointer;"
											id="readeImgId<s:property value="jecnRtnlPropose.id" />"
											onclick="updateSingleUnToRead('<s:property  value="jecnRtnlPropose.id" />',1)" />
									 </s:else>
								</s:if>
								
								<s:if test="jecnRtnlPropose.buttonShowConfig.editIsShow">
								    <!-- 编辑置灰-->
									<s:if
										test="jecnRtnlPropose.buttonShowConfig.editIsGray">
										<img src="${basePath}images/propose/editGray.gif"
											style="cursor: pointer;"
											id="editImgId<s:property value="jecnRtnlPropose.id" />" />

									</s:if>
									<s:else>
									    <span style="display: none"
											id="getProposeContent<s:property  value="jecnRtnlPropose.id" />"><s:property
												value="jecnRtnlPropose.proposeContent" escape="false" />
										</span>
										<img src="${basePath}images/propose/edit.gif"
											style="cursor: pointer;"
											id="editImgId<s:property value="jecnRtnlPropose.id" />"
											onclick="showEditDiv('<s:property  value="jecnRtnlPropose.id" />',
										'<s:property  value="jecnRtnlPropose.listFileIds" />',
										'<s:property  value="jecnRtnlPropose.listFileNames" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','<s:property value="jecnRtnlPropose.rtnlType" />','<s:property value="flowName" />','<s:property value='jecnRtnlPropose.relationId'/>',1)">
									</s:else>
								</s:if>
							&nbsp;&nbsp;
							
									<s:if
										test="jecnRtnlPropose.buttonShowConfig.delIsShow">
										<!-- 删除置灰-->
										<s:if test="jecnRtnlPropose.buttonShowConfig.delIsGray">
										<img src="${basePath}images/propose/deleteGray.gif"
											style="cursor: pointer;"
											id="deleteImgId<s:property value="jecnRtnlPropose.id" />"   
											/>
									</s:if>
									<s:else>
										<img src="${basePath}images/propose/delete.gif"
											style="cursor: pointer;"
											id="deleteImgId<s:property value="jecnRtnlPropose.id" />"
											onclick="deleteMyPropose('<s:property value="jecnRtnlPropose.id" />')" />
									</s:else>
								</s:if>
								</div>
							</td>
						</tr>
						
						<!-- 按钮显示结束-->
						<tr>
							<td colspan="5" style="border-bottom: 1px #e8e8e8 solid;"></td>
						</tr>
						<tr>
							<td style="padding-top: 5px; vertical-align: top;">
								<span style="padding-left: 20px;"
									id="replyFlagShow<s:property value="jecnRtnlPropose.id" />">
									<s:if test="jecnRtnlPropose.replyName == null"></s:if> <s:else>
										<span style="color: gray;"><s:text name="replyProposeC" />
										</span>
									</s:else> </span>
							</td>
							<td colspan="4"
								style="padding-top: 5px; width: 94%; word-break: break-all; padding-right: 10px;">
								<span
									id="spanContentId<s:property value="jecnRtnlPropose.id" />">
									<s:if
										test="jecnRtnlPropose.replyName == null"></s:if>
									<s:else>
										<s:property value="jecnRtnlPropose.replyContent"
											escape="false" />
									</s:else>
								</span>
								<!-- 回复的删除链接是否显示 -->
								
									<a href="#" class="flowPropose"
										onclick="deleteProposeContent('<s:property  value="jecnRtnlPropose.id" />')">
										<span
										      id="deleteReplyHrefId<s:property value="jecnRtnlPropose.id" />">
											<s:if test="jecnRtnlPropose.replyName == null"></s:if>
											    <s:if test="jecnRtnlPropose.buttonShowConfig.curPeopelIsCanDelAnswer">
												    <span>
													  <s:text name="delete" />
													</span>
												</s:if>
									</span> </a>
								
							</td>
						</tr>
						<!-- 回复的人员姓名与日期 -->
						<tr>
							<td colspan="5" style="padding-left: 20px; padding-top: 5px;">
								<span
									id="replyNameShow<s:property value="jecnRtnlPropose.id" />"
									style="color: gray;"> 
								<s:if test="jecnRtnlPropose.replyContent!=null">
									<s:if
										test="jecnRtnlPropose.replyName == null">
										<s:text name="peopleGone" />
									</s:if>
									<s:else>
										<s:property value="jecnRtnlPropose.replyName" />
									</s:else>
								</s:if>
								</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span style="color: gray;"
									id="replyTimeShow<s:property value="jecnRtnlPropose.id" />">
									<s:if test="jecnRtnlPropose.replyName != null">
										<s:date name="jecnRtnlPropose.replyTime" format="yyyy-MM-dd" />
									</s:if>
								</span>
							</td>
						</tr>
						<tr>
							<td colspan="5" style="width: 100%; padding-right: 10px;"
								align="right">
								<input type="hidden"
									id="isShowId<s:property value="jecnRtnlPropose.id" />"
									value="0" />
								<input type="hidden"
									id="isReply<s:property value="jecnRtnlPropose.id" />"
									value="<s:property value="jecnRtnlPropose.state" />" />
								<div style="width: 60%; display: none;" align="right"
									id="replyContentDivId<s:property value="jecnRtnlPropose.id" />">
									<table
										style="width: 100%; border-width: 1px; border-style: solid; border-color: RGB(199, 199, 199); margin-top: 10px; margin-bottom: 15px;">
										<tr>
											<td colspan="3" align="left" style="padding: 5px;">
												<input type="hidden"
													id="isShowId<s:property value="jecnRtnlPropose.id" />"
													value="0" />
												<input type="hidden"
													id="isReply<s:property value="jecnRtnlPropose.id" />"
													value="<s:property value="jecnRtnlPropose.state" />" />
												<s:text name="replyProposeC" />
												<s:property value="jecnRtnlPropose.createUpPeopoleName" />
											</td>
										</tr>
										<tr>
											<td style="width: 10px;"></td>
											<td>
												<textarea
													id="textContentId<s:property value="jecnRtnlPropose.id" />"
													style="width: 100%; height: 50px; border: 1px RGB(199, 199, 199) solid;"></textarea>
											</td>
											<td style="width: 10px;"></td>
										</tr>
										<tr>
											<td colspan="3" align="right" style="padding: 5px;">
												<img src="${basePath}images/propose/ok.gif"
													style="cursor: pointer;"
													onclick="submitContent('<s:property  value="jecnRtnlPropose.id" />',1)">
												&nbsp;&nbsp;
												<img src="${basePath}images/propose/cancel.gif"
													style="cursor: pointer;"
													onclick="cancelReply('<s:property  value="jecnRtnlPropose.id" />')">
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</TABLE>
					<!-- 点击提交建议按钮弹出窗体 start -->
					<div id="eidtbg" class="bg"></div>
					<div id="eidtshow" class="show"
						style="overFlow-x: hidden; overFlow-y: hidden;">
						<form action="uploadRtnlProposeFile.action" method="POST"
							enctype="multipart/form-data" id="editproposeFormId">
							<input type="hidden" id="editrtnlProposeId"
								name="editrtnlProposeId"
								style="padding: 3px 0px 3px 3px; margin: 5px 0px 5px 0px;" />
							<input type="hidden" id="editaddOrUpdateId"
								name="addOrUpdateName" value="1" />
							<input type="hidden" name="isDeleteFileId" id="isDeleteFileId"
								value="0" />
							<input type="hidden" name="editProposeFileId"
								id="editProposeFileId" />
							<textarea style="width: 100%; height: 280px;"
								name="editfileProposeContent" id="editProposeContentId">
					</textarea>
							<br>
							<div style="height: 12px;">
								<span id="updateUploadFileNameId" style="display: none">
									<span id="fileNameShowId"></span> &nbsp;&nbsp; <a
									class="flowPropose" href="#" onclick="deleteProposeFileU()"
									style="cursor: pointer;" id="uploadFileNameIdU"><s:text
											name="delete" />
								</a> </span>
							</div>
							<div style="height: 20px;">
								<span id="updateUploadFileSpan" style="display: none;"> <input
										type="file" name="editupload" id="editUpfileProposeId"
										style="padding: 3px 0px 3px 3px; margin: 5px 0px 5px 0px;" />
									<a href="#" class="flowPropose" id="hideUpdateFileInputId"
									onclick="hideUpdateFileInput();"><s:text name="delete" />
								</a> </span>
							</div>
							<span style="float: right; margin-bottom: 0px;"> <input
									type="button" value="上传附件" onclick="updateFilePropose()"
									id="buttonUpdateFile" style="margin-top: 5px;"> <input
									type="button" value="确定" onclick="editNavigationUploadFile()"
									style="width: 50px; margin-top: 5px;" /> <input type="button"
									value="取消" onclick="hidEditDiv()"
									style="width: 50px; margin-top: 5px;" /> </span>
						</form>
					</div>
					
					<!-- 点击提交建议按钮弹出窗体 end -->
				</s:iterator>
				<input type="hidden" id="loginName"
					value=${sessionScope.webLoginBean.jecnUser.trueName} />
			</div>
		</div>
	</body>
</html>
