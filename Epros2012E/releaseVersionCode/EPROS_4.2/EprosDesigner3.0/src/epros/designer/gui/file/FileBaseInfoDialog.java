package epros.designer.gui.file;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileInfoBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.gui.system.ResProcessSelectCommon;
import epros.designer.gui.system.field.JecnTipTextField;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 文件基本信息窗体
 * 
 * @author 2012-05-29
 * 
 */
public class FileBaseInfoDialog extends JecnPanel {
	private static Logger log = Logger.getLogger(FileBaseInfoDialog.class);

	private JecnDialog jecnDialog = null;

	private JecnTreeNode selectNode = null;

	/** 信息显示面板 */
	private JecnPanel infoShowPanel = null;

	/** 信息面板 */
	private JecnPanel infoPanel = null;

	/** 文件编号Lab */
	public JLabel fileNumLab = null;

	/** 文件编号Field */
	public JTextField fileNumField = null;

	/** 文件名称 */
	private JLabel fileNamLab = null;

	/** 文件名称Field */
	public JTextField fileNameField = null;

	/** 创建人Lab */
	private JLabel creatorLab = null;

	/** 创建人Field */
	public JTextField creatorField = null;
	private Long createId = null;

	/** 创建时间Lab */
	private JLabel creatimeLab = null;

	/** 创建时间Fileld */
	public JTextField creatimeField = null;

	/** 更新时间Lab */
	private JLabel updatmeLab = null;

	/** 更新时间Field */
	public JTextField updatimeField = null;

	/** 责任部门 */
	ResDepartSelectCommon resDepartSelectCommon = null;

	/** 查阅权限 */
	AccessAuthorityCommon accessAuthorityCommon = null;

	/** 所属流程 */
	ResProcessSelectCommon resProcessSelectCommon = null;

	/** 格式 */
	ImageIcon im = null;

	private JPanel fileNameImgPanel = new JPanel();

	private FileInfoBean fileBean = null;

	/** 是不是东软 */
	private boolean isDRCompany = JecnConfigTool.isDRYLOperType();

	/** 责任人 */
	PersonliableSelectCommon personliableSelect = null;
	/** 专员 */
	PeopleSelectCommon commissionerPeopleSelectCommon = null;
	/** 关键字 */
	JecnTipTextField keyWordField = null;

	public FileBaseInfoDialog(JecnTreeNode selectNode, JecnDialog jecnDialog, FileInfoBean fileBean) {
		this.jecnDialog = jecnDialog;
		this.selectNode = selectNode;
		initCompotens();

		// ===============获取文件基本信息===============
		// // 获取文件ID
		// Long fileId = selectNode.getJecnTreeBean().getId();
		// 通过文件ID查询文件基本信息
		this.fileBean = fileBean;
		String createTime = DateFormat.getDateInstance().format(fileBean.getJecnFileBeanT().getCreateTime());
		String updateTime = DateFormat.getDateInstance().format(fileBean.getJecnFileBeanT().getUpdateTime());
		if (this.fileNumField != null) {
			// 文件编号
			this.fileNumField.setText(fileBean.getJecnFileBeanT().getDocId());
		}
		// 文件名称
		this.fileNameField.setText(fileBean.getJecnFileBeanT().getFileName());
		// 创建人ID
		createId = fileBean.getJecnFileBeanT().getPeopleID();
		// /创建人名称
		this.creatorField.setText(fileBean.getJecnFileBeanT().getPeopleName());
		// 创建时间
		this.creatimeField.setText(createTime);
		// 更新时间
		this.updatimeField.setText(updateTime);
		// 文件格式

		List<String> fileLastNames = new ArrayList<String>();
		fileLastNames.add("bmp");
		fileLastNames.add("doc");
		fileLastNames.add("docx");
		fileLastNames.add("gif");
		fileLastNames.add("jpg");
		fileLastNames.add("pdf");
		fileLastNames.add("png");
		fileLastNames.add("ppt");
		fileLastNames.add("pptx");
		fileLastNames.add("txt");
		fileLastNames.add("vsd");
		fileLastNames.add("xls");
		fileLastNames.add("xlsx");
		fileLastNames.add("epros");
		// 文件格式后缀
		String lastFileNameStr = null;
		if (fileLastNames.contains(JecnUtil.getFileExtension(fileBean.getJecnFileBeanT().getFileName()).toString())) {
			lastFileNameStr = JecnUtil.getFileExtension(fileBean.getJecnFileBeanT().getFileName()).toString();
		} else {
			lastFileNameStr = "elsefile";
		}
		// 文件格式
		im = new ImageIcon("images/fileImage/" + lastFileNameStr + ".gif");
		initLayout();
		if (fileNumField != null && JecnConfigTool.isAutoNumber() && !JecnConfigTool.isMengNiuOperaType()
				&& !JecnConfigTool.isLiBangLoginType()) {
			fileNumField.setEditable(false);
		}
	}

	/***
	 * 初始化组件
	 */
	private void initCompotens() {

		// 信息显示面板
		infoShowPanel = new JecnPanel(600, 800);

		// 信息面板
		infoPanel = new JecnPanel();
		// 文件编号Lab
		fileNumLab = new JLabel(JecnProperties.getValue("fileNum") + "：");
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileNum)) {
			// 文件编号Field
			fileNumField = new JTextField();
		}

		// 文件名称
		fileNamLab = new JLabel(JecnProperties.getValue("fileNameC"));

		// 文件名称Field
		fileNameField = new JTextField();
		fileNameField.setEditable(false);

		// 创建人Lab
		creatorLab = new JLabel(JecnProperties.getValue("cretatePeopleC"));

		// 创建人Field
		creatorField = new JTextField();
		creatorField.setEditable(false);

		// 创建时间Lab
		creatimeLab = new JLabel(JecnProperties.getValue("createTimeC"));

		// 创建时间Fileld
		creatimeField = new JTextField();
		creatimeField.setEditable(false);

		// 更新时间Lab
		updatmeLab = new JLabel(JecnProperties.getValue("updateTimeC"));

		// 更新时间Field
		updatimeField = new JTextField();
		updatimeField.setEditable(false);

		this.setSize(600, 600);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		infoShowPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		fileNameImgPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		fileNameField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		creatorField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		creatimeField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		updatimeField.setBackground(JecnUIUtil.getDefaultBackgroundColor());

	}

	/***
	 * 布局
	 */
	private void initLayout() {
		this.setLayout(new GridBagLayout());

		GridBagConstraints c = null;

		Insets insets = new Insets(3, 5, 3, 5);

		// 信息面板=========================布局
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(infoShowPanel, c);
		infoShowPanel.setLayout(new GridBagLayout());

		// 信息面板 infoPanel

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				5, 5, 0, 5), 0, 0);
		infoShowPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());

		int rows = 0;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileNum)) {
			// 文件编号
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(fileNumLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(fileNumField, c);
			if (JecnConfigTool.isRequest("isShowFileNum")) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
		/*
		 * c = new GridBagConstraints(0, rows, 4, 1, 1.0, 0.0,
		 * GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,
		 * 0, 0, 0), 0, 0); infoPanel.add(fileNameImgPanel, c); rows++;
		 * 
		 * fileNameImgPanel.setLayout(new GridBagLayout());
		 */
		// 文件名称
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(fileNamLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(fileNameField, c);
		// 文件格式
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(new JLabel(im), c);
		rows++;

		// 创建人
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(creatorLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(creatorField, c);
		rows++;
		// 创建时间
		// JecnPanel timePanel = new JecnPanel();
		// addTimePanel(infoPanel, insets, rows);
		// timePanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(creatimeLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(creatimeField, c);
		rows++;
		// 更新时间
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(updatmeLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(updatimeField, c);
		rows++;
		/*
		 * c = new GridBagConstraints(0, rows, 4, 1, 1.0, 0.0,
		 * GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0,
		 * 0); infoPanel.add(timePanel, c);
		 */
		// 所属流程
		this.resProcessSelectCommon = new ResProcessSelectCommon(rows, infoPanel, insets, fileBean.getJecnFileBeanT()
				.getFlowId(), fileBean.getJecnFileBeanT().getFlowName(), jecnDialog, JecnProperties
				.getValue("belongsProcessC"), false);
		rows++;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileResPeople)) {
			// 流程责任人
			personliableSelect = new PersonliableSelectCommon(rows, infoPanel, insets, fileBean.getJecnFileBeanT()
					.getResPeopleId(), fileBean.getJecnFileBeanT().getTypeResPeople() == null ? 0 : fileBean
					.getJecnFileBeanT().getTypeResPeople().intValue(), fileBean.getJecnFileBeanT().getResPeopleName(),
					jecnDialog, 2, JecnConfigTool.isRequest("isShowFileResPeople"));
			rows++;
		}

		// 专员
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileCommissioner)) {
			this.commissionerPeopleSelectCommon = new PeopleSelectCommon(rows, infoPanel, insets, fileBean
					.getJecnFileBeanT().getCommissionerId(), fileBean.getJecnFileBeanT().getCommissionerName(),
					jecnDialog, JecnProperties.getValue("commissionerC"), JecnConfigTool
							.isRequest("isShowFileCommissioner"));
			rows++;
		}
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileKeyWord)) {
			this.keyWordField = new JecnTipTextField(rows, infoPanel, insets, JecnProperties.getValue("keyWordC"),
					fileBean.getJecnFileBeanT().getKeyword(), JecnProperties.getValue("keyWord"), JecnConfigTool
							.isRequest("isShowFileKeyWord"));
			rows++;
		}

		// 责任部门
		resDepartSelectCommon = new ResDepartSelectCommon(rows, infoPanel, insets, fileBean.getJecnFileBeanT()
				.getOrgId(), fileBean.getJecnFileBeanT().getOrgName(), jecnDialog, isDRCompany);
		rows++;
		// 查阅权限
		accessAuthorityCommon = new AccessAuthorityCommon(rows, infoPanel, insets,
				selectNode.getJecnTreeBean().getId(), 1, this.jecnDialog, false, true);
		rows++;
	}

	private void addTimePanel(JecnPanel timePanel, Insets insets, int rows) {
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(creatimeLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(creatimeField, c);
		rows++;
		// 更新时间
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(updatmeLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(updatimeField, c);
	}

	public boolean isUpdate() {
		// 编号
		String oldFileNum = "";
		if (this.fileNumField != null && this.fileBean.getJecnFileBeanT().getDocId() != null) {
			oldFileNum = this.fileBean.getJecnFileBeanT().getDocId();
		}
		if (this.fileNumField != null && !this.fileNumField.getText().trim().equals(oldFileNum)) {
			return true;
		}
		// 所属流程
		if (resProcessSelectCommon != null) {
			if (resProcessSelectCommon.isUpdate()) {
				return true;
			}
		}

		// 责任部门
		if (resDepartSelectCommon != null) {
			if (resDepartSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 责任人
		if (personliableSelect != null) {
			if (personliableSelect.getSingleSelectCommon().isUpdate()) {
				return true;
			}
		}
		// 查阅权限
		if (accessAuthorityCommon != null) {
			if (accessAuthorityCommon.isUpdate()) {
				return true;
			}
		}
		// 专员
		if (commissionerPeopleSelectCommon != null) {
			if (this.commissionerPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 关键字
		if (this.keyWordField != null) {
			if (keyWordField.isUpdate()) {
				return true;
			}
		}

		return false;
	}

}
