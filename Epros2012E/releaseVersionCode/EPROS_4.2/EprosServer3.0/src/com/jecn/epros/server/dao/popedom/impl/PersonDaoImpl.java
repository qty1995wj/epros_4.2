package com.jecn.epros.server.dao.popedom.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.dao.popedom.IPersonDao;

public class PersonDaoImpl extends AbsBaseDao<JecnUser, Long> implements IPersonDao {

	@Override
	public JecnUser getJecnUserByLoginNameAndPassword(String loginName, String passward) throws Exception {
		String hql = "from JecnUser where upper(loginName)=? and password = ? and isLock=0";
		return (JecnUser) this.getObjectHql(hql, loginName.toUpperCase(), passward);
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：通过用户名得用户对象
	 * @param LoginName
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public JecnUser getJecnUserByLoginName(String loginName) throws Exception {
		String hql = "from JecnUser where upper(loginName)=? and isLock=0";
		return (JecnUser) this.getObjectHql(hql, loginName.toUpperCase());
	}

	/**
	 * 根据邮箱获取用户
	 * 
	 * @param email
	 * @return
	 * @throws Exception
	 */
	@Override
	public JecnUser getJecnUserByEmail(String email) throws Exception {
		String hql = "from JecnUser where lower(email)=? and isLock=0";
		return (JecnUser) this.getObjectHql(hql, email);
	}

	/**
	 * 根据手机号获取用户
	 * 顾家多出的一个字段  钉钉暂用  2019年1月18日17:03:29
	 * @param phone
	 * @return
	 * @throws Exception
	 */
	@Override
	public JecnUser getJecnUserByPhone(String phone) throws Exception {
		String hql = "from JecnUser where phoneStr=? and isLock=0";
		return (JecnUser) this.getObjectHql(hql, phone);
	}
	
	/**
	 * @author hyl
	 * @description：通过peopleId得用户信息
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public JecnUser getJecnUserById(Long peopleId) {
		String hql = "from JecnUser where peopleId=? ";

		return (JecnUser) this.getObjectHql(hql, peopleId);
	}

	@Override
	public boolean isUserExist(String loginName) throws Exception {
		String hql = "select count(*) from JecnUser where upper(loginName)=? and isLock=0";
		if (this.countAllByParamsHql(hql, loginName.toUpperCase()) > 0) {
			return true;
		}
		return false;
	}

	@Override
	public int deletePeoples(List<Long> listIds) throws Exception {
		String hql = "select u.peopleId from UserRole as u,JecnRoleInfo as r where u.roleId=r.roleId and r.filterId='admin'";
		List<Long> listAdmin = this.listHql(hql);
		if (listIds.containsAll(listAdmin)) {
			return 1;
		}
		// // 流程-更新流程责任人-设计器
		// hql =
		// "update JecnFlowBasicInfoT set resPeopleId = -1 where typeResPeople =0 and resPeopleId in";
		// List<String> list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 流程-更新流程责任人-浏览端
		// hql =
		// "update JecnFlowBasicInfo set resPeopleId = -1 where typeResPeople =0 and resPeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 流程对象表-创建人-设计器
		// hql = "update JecnFlowStructureT set peopleId = -1 where peopleId
		// in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 流程对象表-更新人-设计器
		// hql = "update JecnFlowStructureT set updatePeopleId = -1 where
		// updatePeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		//
		// // 流程对象表-创建人-浏览端
		// hql = "update JecnFlowStructure set peopleId = -1 where peopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 流程对象表-更新人-浏览端
		// hql = "update JecnFlowStructure set updatePeopleId = -1 where
		// updatePeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 标准-创建人
		// hql = "update StandardBean set createPeopleId = -1 where
		// createPeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 标准-更新人
		// hql = "update StandardBean set updatePeopleId = -1 where
		// updatePeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 制度-创建人-设计器
		// hql = "update RuleT set createPeopleId=-1 where createPeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 制度-更新人-设计器
		// hql = "update RuleT set updatePeopleId=-1 where updatePeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 制度-创建人-浏览端
		// hql = "update Rule set createPeopleId=-1 where createPeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 制度-更新人-浏览端
		// hql = "update Rule set updatePeopleId=-1 where updatePeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 制度模板-创建人
		// hql = "update RuleModeBean set createPeopleId=-1 where createPeopleId
		// in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 制度模板-更新人
		// hql = "update RuleModeBean set updatePeopleId=-1 where updatePeopleId
		// in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		//
		// // 文件-创建人
		// hql = "update JecnFileBean set peopleID=-1 where peopleID in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 文件-更新人
		// hql = "update JecnFileBean set updatePersonId=-1 where updatePersonId
		// in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// //
		//
		// // 任务-创建人
		// hql = "update JecnTaskBeanNew set createPersonId = -1 where
		// createPersonId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 任务-源人
		// hql = "update JecnTaskBeanNew set fromPeopleId = -1 where
		// fromPeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// 任务-记录当前阶段操作人
		// hql = "delete from JecnTaskPeopleNew where approvePid in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 任务-记录所有审核人
		// hql = "delete from JecnTaskApprovePeopleConn where approvePid in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 任务-记录表-创建人
		// hql = "update JecnTaskForRecodeNew set createPersonId = -1 where
		// createPersonId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 任务-记录表-源人
		// hql = "update JecnTaskForRecodeNew set fromPeopleId = -1 where
		// fromPeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 任务-记录表-目标人
		// hql = "update JecnTaskForRecodeNew set toPeopleId = -1 where
		// toPeopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 岗位组-创建人
		// hql = "update JecnPositionGroup set createPersonId=-1 where
		// createPersonId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 岗位组-更新人
		// hql = "update JecnPositionGroup set updatePersonId=-1 where
		// updatePersonId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 角色-创建人
		// hql = "update JecnRoleInfo set createPersonId = -1 where
		// createPersonId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 角色-更新人
		// hql = "update JecnRoleInfo set updatePersonId = -1 where
		// updatePersonId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 消息-创建人
		// hql = "update JecnMessage set peopleId = -1 where peopleId in";
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 消息-收件人
		// hql = "update JecnMessage set inceptPeopleId = -1 where
		// inceptPeopleId in";
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 流程下载和访问统计
		// hql = "delete from JecnUserTotal where peopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// 人员角色
		// hql = "delete from UserRole where peopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// // 人员岗位
		// hql = "delete from JecnUserInfo where peopleId in";
		// list = JecnCommonSql.getListSqls(hql, listIds);
		// for (String str : list) {
		// execteHql(str);
		// }
		// 人员关联表
		hql = "update JecnUser set isLock=1 where peopleId in";
		List<String> list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			execteHql(str);
		}
		return 0;
	}

	@Override
	public void deletePeoples(Long projectId) throws Exception {
		String str = " (select peopleId from JecnUser where projectId = " + projectId + ")";
		// 流程-更新流程责任人-设计器
		String hql = "update JecnFlowBasicInfoT set resPeopleId = -1 where typeResPeople =0 and resPeopleId in" + str;
		execteHql(hql);
		// 流程-更新流程责任人-浏览端
		hql = "update JecnFlowBasicInfo set resPeopleId = -1 where typeResPeople =0 and resPeopleId in" + str;
		execteHql(hql);
		// // 流程对象表-创建人-设计器
		// hql = "update JecnFlowStructureT set peopleId = -1 where peopleId in"
		// + str;
		// execteHql(hql);
		// // 流程对象表-更新人-设计器
		// hql = "update JecnFlowStructureT set updatePeopleId = -1 where
		// updatePeopleId in"
		// + str;
		// execteHql(hql);
		//
		// // 流程对象表-创建人-浏览端
		// hql = "update JecnFlowStructure set peopleId = -1 where peopleId in"
		// + str;
		// execteHql(hql);
		// // 流程对象表-更新人-浏览端
		// hql = "update JecnFlowStructure set updatePeopleId = -1 where
		// updatePeopleId in"
		// + str;
		// execteHql(hql);
		// // 标准-创建人
		// hql = "update StandardBean set createPeopleId = -1 where
		// createPeopleId in"
		// + str;
		// execteHql(hql);
		// // 标准-更新人
		// hql = "update StandardBean set updatePeopleId = -1 where
		// updatePeopleId in"
		// + str;
		// execteHql(hql);
		// // 制度-创建人-设计器
		// hql = "update RuleT set createPeopleId=-1 where createPeopleId in"
		// + str;
		// execteHql(hql);
		// // 制度-更新人-设计器
		// hql = "update RuleT set updatePeopleId=-1 where updatePeopleId in"
		// + str;
		// execteHql(hql);
		// // 制度-创建人-浏览端
		// hql = "update Rule set createPeopleId=-1 where createPeopleId in" +
		// str;
		// execteHql(hql);
		// // 制度-更新人-浏览端
		// hql = "update Rule set updatePeopleId=-1 where updatePeopleId in" +
		// str;
		// execteHql(hql);
		// // 制度模板-创建人
		// hql = "update RuleModeBean set createPeopleId=-1 where createPeopleId
		// in"
		// + str;
		// execteHql(hql);
		// // 制度模板-更新人
		// hql = "update RuleModeBean set updatePeopleId=-1 where updatePeopleId
		// in"
		// + str;
		// execteHql(hql);
		//
		// // 文件-创建人
		// hql = "update JecnFileBean set peopleID=-1 where peopleID in" + str;
		// execteHql(hql);
		// // 文件-更新人
		// hql = "update JecnFileBean set updatePersonId=-1 where updatePersonId
		// in"
		// + str;
		// execteHql(hql);
		//
		// // 任务-创建人
		// hql = "update JecnTaskBeanNew set createPersonId = -1 where
		// createPersonId in"
		// + str;
		// execteHql(hql);
		// // 任务-源人
		// hql = "update JecnTaskBeanNew set fromPeopleId = -1 where
		// fromPeopleId in"
		// + str;
		// execteHql(hql);
		// // 任务-记录当前阶段操作人

		hql = "delete from JecnTaskPeopleNew where approvePid in" + str;
		execteHql(hql);
		// 任务-记录所有审核人
		hql = "delete from JecnTaskApprovePeopleConn where approvePid in" + str;
		execteHql(hql);
		// // 任务-记录表-创建人
		// hql = "update JecnTaskForRecodeNew set createPersonId = -1 where
		// createPersonId in"
		// + str;
		// execteHql(hql);
		// // 任务-记录表-源人
		// hql = "update JecnTaskForRecodeNew set fromPeopleId = -1 where
		// fromPeopleId in"
		// + str;
		// execteHql(hql);
		// // 任务-记录表-目标人
		// hql = "update JecnTaskForRecodeNew set toPeopleId = -1 where
		// toPeopleId in"
		// + str;
		// execteHql(hql);
		// // 岗位组-创建人
		// hql = "update JecnPositionGroup set createPersonId=-1 where
		// createPersonId in"
		// + str;
		// execteHql(hql);
		// // 岗位组-更新人
		// hql = "update JecnPositionGroup set updatePersonId=-1 where
		// updatePersonId in"
		// + str;
		// execteHql(hql);
		// // 角色-创建人
		// hql = "update JecnRoleInfo set createPersonId = -1 where
		// createPersonId in"
		// + str;
		// execteHql(hql);
		// // 角色-更新人
		// hql = "update JecnRoleInfo set updatePersonId = -1 where
		// updatePersonId in"
		// + str;
		// execteHql(hql);
		// // 消息-创建人
		// hql = "update JecnMessage set peopleId = -1 where peopleId in" + str;
		// execteHql(hql);
		// // 消息-收件人
		// hql = "update JecnMessage set inceptPeopleId = -1 where
		// inceptPeopleId in"
		// + str;
		// execteHql(hql);
		// // 流程下载和访问统计
		// hql = "delete from JecnUserTotal where peopleId in" + str;
		// execteHql(hql);

		// 人员角色
		hql = "delete from UserRole where peopleId in" + str;
		execteHql(hql);
		// 人员岗位
		hql = "delete from JecnUserInfo where peopleId in" + str;
		execteHql(hql);
		// 人员关联表
		hql = "update JecnUser set isLock = 1 where projectId =?";
		execteHql(hql, projectId);
	}

	@Override
	public int getCurUserInt() throws Exception {
		String sql = "select count(t.role_id) from jecn_role_info t,jecn_user_role jur,jecn_user ju"
				+ "        where t.filter_id in " + JecnCommonSql.getDesignDefaultAuthString()
				+ " and t.role_id=jur.role_id and jur.people_id=ju.people_id" + "              and ju.islock=0";
		return this.countAllByParamsNativeSql(sql);
	}

	@Override
	public int getCurUserInt(Long peopleId) throws Exception {
		String sql = "select count(t.role_id) from jecn_role_info t,jecn_user_role jur,jecn_user ju"
				+ "        where t.filter_id in " + JecnCommonSql.getDesignDefaultAuthString()
				+ " and t.role_id=jur.role_id and jur.people_id=ju.people_id"
				+ "              and ju.islock=0 and ju.people_id<>?";
		return this.countAllByParamsNativeSql(sql, peopleId);
	}

	/**
	 * 0:岗位ID，1：岗位名称；2：部门ID；3：部门名称；4：部门Tpath
	 */
	@Override
	public List<Object[]> getPosOrgInfo(Long peopleId) throws Exception {
		String sql = "select jfoi.figure_id,jfoi.figure_text,jfoi.org_id,jfo.org_name,jfo.T_PATH"
				+ "       from jecn_user_position_related jupr,jecn_flow_org_image jfoi, jecn_flow_org jfo"
				+ "       where jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and jupr.people_id=?";
		return this.listNativeSql(sql, peopleId);
	}

	/**
	 * 根据主键IDs 获取人员信息集合
	 * 
	 * @param setPeopleIds
	 *            人员主键集合
	 * @return 人员邮件信息集合 List<Object[]> 0:内外网；1：邮件地址;2:人员主键ID
	 * @throws Exception
	 */
	@Override
	public List<JecnUser> getJecnUserList(Set<Long> setPeopleIds) throws Exception {
		String hql = "from JecnUser where peopleId in";
		List<String> listSql = JecnCommonSql.getSetSqls(hql, setPeopleIds);
		List<JecnUser> listAll = new ArrayList<JecnUser>();
		List<JecnUser> listObj = null;
		for (String str : listSql) {
			listObj = this.listHql(str);
			for (JecnUser obj : listObj) {
				listAll.add(obj);
			}
		}
		return listAll;
	}

	@Override
	public void restorationPeoples(List<Long> listIds) throws Exception {
		// 人员关联表
		String hql = "update JecnUser set isLock=0 where peopleId in";
		List<String> list = JecnCommonSql.getListSqls(hql, listIds);
		for (String str : list) {
			execteHql(str);
		}

	}

	/**
	 * 获取所有用户
	 */
	@Override
	public List<JecnUser> findAllJecnUser() throws Exception {
		return this.listAll();
	}

	/**
	 * 获取人员用户总数
	 * 
	 * @return int
	 * @throws Exception
	 */
	@Override
	public int userTotalCount() throws Exception {
		String sql = "select count(*) from Jecn_User";
		return this.countAllByParamsNativeSql(sql);
	}

	/**
	 * 获取指定收件人、地址、类型
	 * 
	 * @return List<Object[]>
	 * @throws Exception
	 */
	@Override
	public List<JecnUser> getFixedEmail(int type) throws Exception {
		String sql = "select ju.* from JECN_FIXED_EMAIL jfe"
				+ " inner join jecn_user ju on jfe.person_id=ju.people_id "
				+ " where ju.islock=0 and ju.email is not null and ju.email_type is not null and jfe.type=" + type;
		return this.getSession().createSQLQuery(sql).addEntity(JecnUser.class).list();
	}

	@Override
	public boolean isPeopleAndPosAllExists(Long peopleId) throws Exception {
		String hql = "select count(*) from JecnUserInfo where peopleId=?";
		if (this.countAllByParamsHql(hql, peopleId) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 获得系统管理员的内外网类型 地址 人员id
	 * 
	 * @return List<Object[]> 0内外网类型 1地址 2人员id
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getAdminEmail() throws Exception {

		String sql = "select ju.email_type,ju.email,ju.people_id from jecn_user ju"
				+ " inner join JECN_USER_ROLE jur on ju.people_id=jur.people_id"
				+ " inner join JECN_ROLE_INFO jri on jri.role_id=jur.role_id"
				+ " where jri.filter_id='admin' and ju.islock=0 and ju.email is not null and ju.email_type is not null";
		return this.listNativeSql(sql);
	}

	@Override
	public List<Object> getAdminIds() throws Exception {
		String sql = "select ju.people_id from jecn_user ju"
				+ " inner join JECN_USER_ROLE jur on ju.people_id=jur.people_id"
				+ " inner join JECN_ROLE_INFO jri on jri.role_id=jur.role_id"
				+ " where jri.filter_id='admin' and ju.islock=0";
		return this.listNativeSql(sql);
	}

	@Override
	public List<Object> getFixedIds(int type) throws Exception {

		String sql = "select ju.people_id from JECN_FIXED_EMAIL jfe"
				+ " inner join jecn_user ju on jfe.person_id=ju.people_id " + " where ju.islock=0 and jfe.type=" + type;
		return this.listNativeSql(sql);
	}

	@Override
	public List<JecnUser> getAllAdminUser() throws Exception {
		String sql = "SELECT JU.*" + "  FROM JECN_USER JU"
				+ " INNER JOIN JECN_USER_ROLE JUR ON JU.PEOPLE_ID = JUR.PEOPLE_ID"
				+ " INNER JOIN JECN_ROLE_INFO JRI ON JUR.ROLE_ID = JRI.ROLE_ID" + " WHERE JRI.FILTER_ID = 'admin'";
		return this.getSession().createSQLQuery(sql).addEntity(JecnUser.class).list();
	}

	/**
	 * 角色权限获取人员选择结果集
	 * 
	 * @param name
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		String sql = "select ju.people_id,ju.true_name,jflo.figure_text,t.org_name,ju.login_name"
				+ "        from jecn_user ju,JECN_USER_POSITION_RELATED jup,jecn_flow_org_image jflo,jecn_flow_org t";
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(5, peopleId, projectId);
		sql = sql
				+ "        where ju.people_id=jup.people_id and jup.figure_id=jflo.figure_id and jflo.org_id =t.org_id"
				+ "        and t.del_state=0 and t.projectid=? and upper(ju.true_name) like upper('%" + name + "%')";
		return this.listNativeSql(sql, 0, JecnCommonSql.pageSize, projectId);
	}

	@Override
	public int findAdminUseCount() {
		String sql = "SELECT COUNT(JU.PEOPLE_ID)" + "  FROM JECN_ROLE_INFO RI" + " INNER JOIN JECN_USER_ROLE UR"
				+ "    ON UR.ROLE_ID = RI.ROLE_ID" + " INNER JOIN JECN_USER JU" + "    ON JU.PEOPLE_ID = UR.PEOPLE_ID"
				+ "   AND JU.ISLOCK = 0" + " WHERE RI.FILTER_ID IN ('admin','secondAdmin')";
		int count = this.countAllByParamsNativeSql(sql);
		return count;
	}

	@Override
	public int findDesignUseCount() {
		String sql = "SELECT COUNT(JU.PEOPLE_ID)" + " FROM JECN_ROLE_INFO RI " + " INNER JOIN JECN_USER_ROLE UR"
				+ "   ON UR.ROLE_ID = RI.ROLE_ID " + " INNER JOIN JECN_USER JU" + "   ON JU.PEOPLE_ID = UR.PEOPLE_ID"
				+ "  AND JU.ISLOCK = 0" + "WHERE RI.FILTER_ID IN ('design')";
		return this.countAllByParamsNativeSql(sql);
	}
	
	/**
	 * 获得某个人当前到他处审批的任务记录最新ID，作为单一待办ID   同时可以用作他审批完成后，取消代办的唯一ID
	 * 
	 * @param taskId peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long getJecnTaskForTodoRecodeIdByTaskIdPeopleId(Long taskId,Long peopleId) throws Exception{
		String hql = "select max(id) from JecnTaskForRecodeNew where taskId=? and toPeopleId = ?";
		Long t = this.getObjectHql(hql, taskId,peopleId);
		return t;
	}
}
