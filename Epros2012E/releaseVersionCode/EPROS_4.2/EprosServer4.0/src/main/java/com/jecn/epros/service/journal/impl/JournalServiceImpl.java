package com.jecn.epros.service.journal.impl;

import java.util.*;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.common.relatedfiles.RelatedFileBean;
import com.jecn.epros.domain.journal.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.common.CommonService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.Params;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.mapper.JournalMapper;
import com.jecn.epros.service.journal.JournalService;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;

@Service
public class JournalServiceImpl implements JournalService {

    @Autowired
    private JournalMapper journalMapper;
    @Autowired
    private CommonService commonService;


    @Override
    public List<UserVisitsResult> getSearchLogDetailResultBeans(Map<String, Object> map) {
        Paging paging = (Paging) map.get(Params.PG);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "OPERATOR_TIME DESC,RELATED_TYPE");
        List<UserVisitsResult> userVisitsResult = journalMapper.getLogDetailResult(map);
        IfytekCommon.getIflytekhUserVisitsName(userVisitsResult);
        return userVisitsResult;
    }

    @Override
    public List<ResourceVisitsResult> getSearchLogStatisticsResultBeans(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "RELATED_NAME,RELATED_TYPE");
        return journalMapper.getLogStatisticsResult(map);
    }

    @Override
    public int getSearchLogDetailResultBeanTotal(Map<String, Object> map) {
        return journalMapper.getLogDetailResultTotal(map);
    }

    @Override
    public int getSearchLogStatisticsResultBeanTotal(Map<String, Object> map) {
        return journalMapper.getLogStatisticsResultTotal(map);
    }

    @Override
    public void addWebLog(Map<String, Object> map) {
        Date curDate = new Date();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("relatedId", map.get("relatedId"));
        paramMap.put("relatedType", map.get("relatedType"));
        paramMap.put("operatorType", map.get("operatorType"));//0是查阅，1是下载，2是流程架构/流程图/组织图下载

        paramMap.put("id", Common.getUUID());
        paramMap.put("operatorTime", curDate);//操作日期
        paramMap.put("operatorPeople", AuthenticatedUserUtil.getPeopleId());//操作人员
        paramMap.put("historyId", getHistoryId(map));// 根据文件类型查询对应的文控id

        // 张臣要求查阅类型的操作半个小时重复访问不记录文控
        if (map.get("operatorType") != null && "0".equals(map.get("operatorType").toString())) {
            // 当前日期减去时间间隔和更新时间比较，如果大则返回
            Calendar instance = Calendar.getInstance();
            instance.setTime(curDate);
            instance.add(Calendar.MINUTE, -ConfigUtils.getLogRecordIntervalMinute(paramMap));
            instance.set(Calendar.SECOND, 1);
            paramMap.put("compareTime", instance.getTime());
            paramMap.put("compareType", 0);
            int count = journalMapper.operateTimeBigEqCount(paramMap);
            if (count > 0) {
                return;
            }
        }
        journalMapper.addWebLog(paramMap);
    }

    private Long getHistoryId(Map<String, Object> map) {
        // 日志类型：0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构
        // 文控信息类型： 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
        int logType = Integer.valueOf(map.get("relatedType").toString());
        if (!hasHistory(logType)) {
            return null;
        }
        Long historyId = journalMapper.getHistoryIdByLogType(map);
        return historyId;
    }


    /**
     * 类型是否含有文控信息
     *
     * @param logType 日志类型：0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构
     * @return
     */
    private boolean hasHistory(int logType) {
        if (logType == 0 || logType == 1 || logType == 2 || logType == 7) {
            return true;
        }
        return false;
    }

    @Override
    public List<SearchResultWebLogBean> searchNotice(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "pub_time desc");
        map.put("typeAuth", AuthSqlConstant.TYPEAUTH.FLOW);
        List<SearchResultWebLogBean> webLogBeans = journalMapper.searchProcessNotice(map);
        getNoticeParentDirectory(webLogBeans, map);
/*        for (SearchResultWebLogBean bean : webLogBeans) {
            map.put("historyId", 0L);
            switch (bean.getType()) {
                case PROCESS:
                    map.put("isPub", "");
                    map.put("type", 1);
                    map.put("flowId", bean.getFlowId());
                    break;
                case PROCESS_MAP:
                    map.put("isPub", "");
                    map.put("type", 0);
                    map.put("flowId", bean.getFlowId());
                    break;
                case RULE:
                    map.put("type", 2);
                    map.put("isPub", "");
                    map.put("id", bean.getFlowId());
                    break;
                default:
                    break;
            }
            List<RelatedFileBean> files = commonService.findRelatedFiles(map);
            bean.setRelatedFiles(files);
        }*/
        return webLogBeans;
    }


    private void getNoticeParentDirectory(List<SearchResultWebLogBean> webLogBeans, Map<String, Object> map) {
        List<Long> ids = new ArrayList<Long>();
        SearchWebLogBean searchBean = (SearchWebLogBean) map.get("searchBean");
        LinkResource.ResourceType type;
        for (SearchResultWebLogBean bean : webLogBeans) {
            bean.setDirName("");
            ids.add(bean.getFlowId());
        }
        Map<String, String> pName = commonService.getResourceParentDirectory(searchBean.getFileType(), ids);
        for (SearchResultWebLogBean bean : webLogBeans) {
            for (Map.Entry mapBean : pName.entrySet()) {
                if (bean.getFlowId().toString().equals(mapBean.getKey().toString())) {
                    bean.setDirName("");
                    bean.setDirName(mapBean.getValue().toString());
                }
            }
        }
    }

    @Override
    public int searchNoticeTotal(Map<String, Object> map) {
        return journalMapper.searchProcessNoticeTotal(map);
    }

    @Override
    public List<UserOperateResult> getSearchLogOperateResultBeans(Map<String, Object> map) {
        Paging paging = (Paging) map.get(Params.PG);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "UPDATE_TIME DESC,TYPE");
        return journalMapper.getLogOperateResult(map);
    }

    @Override
    public int getSearchLogOperateResultBeanTotal(Map<String, Object> map) {
        return journalMapper.getLogOperateResultTotal(map);
    }


}
