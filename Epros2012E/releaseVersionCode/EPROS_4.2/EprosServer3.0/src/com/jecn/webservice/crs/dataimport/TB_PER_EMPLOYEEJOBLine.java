/**
 * TB_PER_EMPLOYEEJOBLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public class TB_PER_EMPLOYEEJOBLine  implements java.io.Serializable {
    private java.lang.String c_appraise;

    private java.math.BigDecimal versioncode;

    private java.math.BigDecimal c_orgunitid;

    private java.util.Date c_begindate;

    private java.util.Date versiontime;

    private java.lang.String versiondelete;

    private java.lang.String c_remark;

    private java.lang.String c_jobstatus;

    private java.math.BigDecimal versionid;

    private java.lang.String c_jobname;

    private java.lang.String c_jobtype;

    private java.util.Date c_operatetime;

    private java.math.BigDecimal c_operator;

    private java.lang.String c_orgunitcode;

    private java.lang.String c_jobcode;

    private java.util.Date c_enddate;

    private java.math.BigDecimal c_oid_employeejob;

    private java.lang.String c_orgunitname;

    private java.util.Date c_oldenddate;

    private java.util.Date c_expectantenddate;

    private java.math.BigDecimal c_empoid;

    private java.lang.String c_type;

    private java.math.BigDecimal c_jobid;

    public TB_PER_EMPLOYEEJOBLine() {
    }

    public TB_PER_EMPLOYEEJOBLine(
           java.lang.String c_appraise,
           java.math.BigDecimal versioncode,
           java.math.BigDecimal c_orgunitid,
           java.util.Date c_begindate,
           java.util.Date versiontime,
           java.lang.String versiondelete,
           java.lang.String c_remark,
           java.lang.String c_jobstatus,
           java.math.BigDecimal versionid,
           java.lang.String c_jobname,
           java.lang.String c_jobtype,
           java.util.Date c_operatetime,
           java.math.BigDecimal c_operator,
           java.lang.String c_orgunitcode,
           java.lang.String c_jobcode,
           java.util.Date c_enddate,
           java.math.BigDecimal c_oid_employeejob,
           java.lang.String c_orgunitname,
           java.util.Date c_oldenddate,
           java.util.Date c_expectantenddate,
           java.math.BigDecimal c_empoid,
           java.lang.String c_type,
           java.math.BigDecimal c_jobid) {
           this.c_appraise = c_appraise;
           this.versioncode = versioncode;
           this.c_orgunitid = c_orgunitid;
           this.c_begindate = c_begindate;
           this.versiontime = versiontime;
           this.versiondelete = versiondelete;
           this.c_remark = c_remark;
           this.c_jobstatus = c_jobstatus;
           this.versionid = versionid;
           this.c_jobname = c_jobname;
           this.c_jobtype = c_jobtype;
           this.c_operatetime = c_operatetime;
           this.c_operator = c_operator;
           this.c_orgunitcode = c_orgunitcode;
           this.c_jobcode = c_jobcode;
           this.c_enddate = c_enddate;
           this.c_oid_employeejob = c_oid_employeejob;
           this.c_orgunitname = c_orgunitname;
           this.c_oldenddate = c_oldenddate;
           this.c_expectantenddate = c_expectantenddate;
           this.c_empoid = c_empoid;
           this.c_type = c_type;
           this.c_jobid = c_jobid;
    }


    /**
     * Gets the c_appraise value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_appraise
     */
    public java.lang.String getC_appraise() {
        return c_appraise;
    }


    /**
     * Sets the c_appraise value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_appraise
     */
    public void setC_appraise(java.lang.String c_appraise) {
        this.c_appraise = c_appraise;
    }


    /**
     * Gets the versioncode value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return versioncode
     */
    public java.math.BigDecimal getVersioncode() {
        return versioncode;
    }


    /**
     * Sets the versioncode value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param versioncode
     */
    public void setVersioncode(java.math.BigDecimal versioncode) {
        this.versioncode = versioncode;
    }


    /**
     * Gets the c_orgunitid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_orgunitid
     */
    public java.math.BigDecimal getC_orgunitid() {
        return c_orgunitid;
    }


    /**
     * Sets the c_orgunitid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_orgunitid
     */
    public void setC_orgunitid(java.math.BigDecimal c_orgunitid) {
        this.c_orgunitid = c_orgunitid;
    }


    /**
     * Gets the c_begindate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_begindate
     */
    public java.util.Date getC_begindate() {
        return c_begindate;
    }


    /**
     * Sets the c_begindate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_begindate
     */
    public void setC_begindate(java.util.Date c_begindate) {
        this.c_begindate = c_begindate;
    }


    /**
     * Gets the versiontime value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return versiontime
     */
    public java.util.Date getVersiontime() {
        return versiontime;
    }


    /**
     * Sets the versiontime value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param versiontime
     */
    public void setVersiontime(java.util.Date versiontime) {
        this.versiontime = versiontime;
    }


    /**
     * Gets the versiondelete value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return versiondelete
     */
    public java.lang.String getVersiondelete() {
        return versiondelete;
    }


    /**
     * Sets the versiondelete value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param versiondelete
     */
    public void setVersiondelete(java.lang.String versiondelete) {
        this.versiondelete = versiondelete;
    }


    /**
     * Gets the c_remark value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_remark
     */
    public java.lang.String getC_remark() {
        return c_remark;
    }


    /**
     * Sets the c_remark value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_remark
     */
    public void setC_remark(java.lang.String c_remark) {
        this.c_remark = c_remark;
    }


    /**
     * Gets the c_jobstatus value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_jobstatus
     */
    public java.lang.String getC_jobstatus() {
        return c_jobstatus;
    }


    /**
     * Sets the c_jobstatus value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_jobstatus
     */
    public void setC_jobstatus(java.lang.String c_jobstatus) {
        this.c_jobstatus = c_jobstatus;
    }


    /**
     * Gets the versionid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return versionid
     */
    public java.math.BigDecimal getVersionid() {
        return versionid;
    }


    /**
     * Sets the versionid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param versionid
     */
    public void setVersionid(java.math.BigDecimal versionid) {
        this.versionid = versionid;
    }


    /**
     * Gets the c_jobname value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_jobname
     */
    public java.lang.String getC_jobname() {
        return c_jobname;
    }


    /**
     * Sets the c_jobname value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_jobname
     */
    public void setC_jobname(java.lang.String c_jobname) {
        this.c_jobname = c_jobname;
    }


    /**
     * Gets the c_jobtype value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_jobtype
     */
    public java.lang.String getC_jobtype() {
        return c_jobtype;
    }


    /**
     * Sets the c_jobtype value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_jobtype
     */
    public void setC_jobtype(java.lang.String c_jobtype) {
        this.c_jobtype = c_jobtype;
    }


    /**
     * Gets the c_operatetime value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_operatetime
     */
    public java.util.Date getC_operatetime() {
        return c_operatetime;
    }


    /**
     * Sets the c_operatetime value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_operatetime
     */
    public void setC_operatetime(java.util.Date c_operatetime) {
        this.c_operatetime = c_operatetime;
    }


    /**
     * Gets the c_operator value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_operator
     */
    public java.math.BigDecimal getC_operator() {
        return c_operator;
    }


    /**
     * Sets the c_operator value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_operator
     */
    public void setC_operator(java.math.BigDecimal c_operator) {
        this.c_operator = c_operator;
    }


    /**
     * Gets the c_orgunitcode value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_orgunitcode
     */
    public java.lang.String getC_orgunitcode() {
        return c_orgunitcode;
    }


    /**
     * Sets the c_orgunitcode value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_orgunitcode
     */
    public void setC_orgunitcode(java.lang.String c_orgunitcode) {
        this.c_orgunitcode = c_orgunitcode;
    }


    /**
     * Gets the c_jobcode value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_jobcode
     */
    public java.lang.String getC_jobcode() {
        return c_jobcode;
    }


    /**
     * Sets the c_jobcode value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_jobcode
     */
    public void setC_jobcode(java.lang.String c_jobcode) {
        this.c_jobcode = c_jobcode;
    }


    /**
     * Gets the c_enddate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_enddate
     */
    public java.util.Date getC_enddate() {
        return c_enddate;
    }


    /**
     * Sets the c_enddate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_enddate
     */
    public void setC_enddate(java.util.Date c_enddate) {
        this.c_enddate = c_enddate;
    }


    /**
     * Gets the c_oid_employeejob value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_oid_employeejob
     */
    public java.math.BigDecimal getC_oid_employeejob() {
        return c_oid_employeejob;
    }


    /**
     * Sets the c_oid_employeejob value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_oid_employeejob
     */
    public void setC_oid_employeejob(java.math.BigDecimal c_oid_employeejob) {
        this.c_oid_employeejob = c_oid_employeejob;
    }


    /**
     * Gets the c_orgunitname value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_orgunitname
     */
    public java.lang.String getC_orgunitname() {
        return c_orgunitname;
    }


    /**
     * Sets the c_orgunitname value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_orgunitname
     */
    public void setC_orgunitname(java.lang.String c_orgunitname) {
        this.c_orgunitname = c_orgunitname;
    }


    /**
     * Gets the c_oldenddate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_oldenddate
     */
    public java.util.Date getC_oldenddate() {
        return c_oldenddate;
    }


    /**
     * Sets the c_oldenddate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_oldenddate
     */
    public void setC_oldenddate(java.util.Date c_oldenddate) {
        this.c_oldenddate = c_oldenddate;
    }


    /**
     * Gets the c_expectantenddate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_expectantenddate
     */
    public java.util.Date getC_expectantenddate() {
        return c_expectantenddate;
    }


    /**
     * Sets the c_expectantenddate value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_expectantenddate
     */
    public void setC_expectantenddate(java.util.Date c_expectantenddate) {
        this.c_expectantenddate = c_expectantenddate;
    }


    /**
     * Gets the c_empoid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_empoid
     */
    public java.math.BigDecimal getC_empoid() {
        return c_empoid;
    }


    /**
     * Sets the c_empoid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_empoid
     */
    public void setC_empoid(java.math.BigDecimal c_empoid) {
        this.c_empoid = c_empoid;
    }


    /**
     * Gets the c_type value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_type
     */
    public java.lang.String getC_type() {
        return c_type;
    }


    /**
     * Sets the c_type value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_type
     */
    public void setC_type(java.lang.String c_type) {
        this.c_type = c_type;
    }


    /**
     * Gets the c_jobid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @return c_jobid
     */
    public java.math.BigDecimal getC_jobid() {
        return c_jobid;
    }


    /**
     * Sets the c_jobid value for this TB_PER_EMPLOYEEJOBLine.
     * 
     * @param c_jobid
     */
    public void setC_jobid(java.math.BigDecimal c_jobid) {
        this.c_jobid = c_jobid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TB_PER_EMPLOYEEJOBLine)) return false;
        TB_PER_EMPLOYEEJOBLine other = (TB_PER_EMPLOYEEJOBLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.c_appraise==null && other.getC_appraise()==null) || 
             (this.c_appraise!=null &&
              this.c_appraise.equals(other.getC_appraise()))) &&
            ((this.versioncode==null && other.getVersioncode()==null) || 
             (this.versioncode!=null &&
              this.versioncode.equals(other.getVersioncode()))) &&
            ((this.c_orgunitid==null && other.getC_orgunitid()==null) || 
             (this.c_orgunitid!=null &&
              this.c_orgunitid.equals(other.getC_orgunitid()))) &&
            ((this.c_begindate==null && other.getC_begindate()==null) || 
             (this.c_begindate!=null &&
              this.c_begindate.equals(other.getC_begindate()))) &&
            ((this.versiontime==null && other.getVersiontime()==null) || 
             (this.versiontime!=null &&
              this.versiontime.equals(other.getVersiontime()))) &&
            ((this.versiondelete==null && other.getVersiondelete()==null) || 
             (this.versiondelete!=null &&
              this.versiondelete.equals(other.getVersiondelete()))) &&
            ((this.c_remark==null && other.getC_remark()==null) || 
             (this.c_remark!=null &&
              this.c_remark.equals(other.getC_remark()))) &&
            ((this.c_jobstatus==null && other.getC_jobstatus()==null) || 
             (this.c_jobstatus!=null &&
              this.c_jobstatus.equals(other.getC_jobstatus()))) &&
            ((this.versionid==null && other.getVersionid()==null) || 
             (this.versionid!=null &&
              this.versionid.equals(other.getVersionid()))) &&
            ((this.c_jobname==null && other.getC_jobname()==null) || 
             (this.c_jobname!=null &&
              this.c_jobname.equals(other.getC_jobname()))) &&
            ((this.c_jobtype==null && other.getC_jobtype()==null) || 
             (this.c_jobtype!=null &&
              this.c_jobtype.equals(other.getC_jobtype()))) &&
            ((this.c_operatetime==null && other.getC_operatetime()==null) || 
             (this.c_operatetime!=null &&
              this.c_operatetime.equals(other.getC_operatetime()))) &&
            ((this.c_operator==null && other.getC_operator()==null) || 
             (this.c_operator!=null &&
              this.c_operator.equals(other.getC_operator()))) &&
            ((this.c_orgunitcode==null && other.getC_orgunitcode()==null) || 
             (this.c_orgunitcode!=null &&
              this.c_orgunitcode.equals(other.getC_orgunitcode()))) &&
            ((this.c_jobcode==null && other.getC_jobcode()==null) || 
             (this.c_jobcode!=null &&
              this.c_jobcode.equals(other.getC_jobcode()))) &&
            ((this.c_enddate==null && other.getC_enddate()==null) || 
             (this.c_enddate!=null &&
              this.c_enddate.equals(other.getC_enddate()))) &&
            ((this.c_oid_employeejob==null && other.getC_oid_employeejob()==null) || 
             (this.c_oid_employeejob!=null &&
              this.c_oid_employeejob.equals(other.getC_oid_employeejob()))) &&
            ((this.c_orgunitname==null && other.getC_orgunitname()==null) || 
             (this.c_orgunitname!=null &&
              this.c_orgunitname.equals(other.getC_orgunitname()))) &&
            ((this.c_oldenddate==null && other.getC_oldenddate()==null) || 
             (this.c_oldenddate!=null &&
              this.c_oldenddate.equals(other.getC_oldenddate()))) &&
            ((this.c_expectantenddate==null && other.getC_expectantenddate()==null) || 
             (this.c_expectantenddate!=null &&
              this.c_expectantenddate.equals(other.getC_expectantenddate()))) &&
            ((this.c_empoid==null && other.getC_empoid()==null) || 
             (this.c_empoid!=null &&
              this.c_empoid.equals(other.getC_empoid()))) &&
            ((this.c_type==null && other.getC_type()==null) || 
             (this.c_type!=null &&
              this.c_type.equals(other.getC_type()))) &&
            ((this.c_jobid==null && other.getC_jobid()==null) || 
             (this.c_jobid!=null &&
              this.c_jobid.equals(other.getC_jobid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getC_appraise() != null) {
            _hashCode += getC_appraise().hashCode();
        }
        if (getVersioncode() != null) {
            _hashCode += getVersioncode().hashCode();
        }
        if (getC_orgunitid() != null) {
            _hashCode += getC_orgunitid().hashCode();
        }
        if (getC_begindate() != null) {
            _hashCode += getC_begindate().hashCode();
        }
        if (getVersiontime() != null) {
            _hashCode += getVersiontime().hashCode();
        }
        if (getVersiondelete() != null) {
            _hashCode += getVersiondelete().hashCode();
        }
        if (getC_remark() != null) {
            _hashCode += getC_remark().hashCode();
        }
        if (getC_jobstatus() != null) {
            _hashCode += getC_jobstatus().hashCode();
        }
        if (getVersionid() != null) {
            _hashCode += getVersionid().hashCode();
        }
        if (getC_jobname() != null) {
            _hashCode += getC_jobname().hashCode();
        }
        if (getC_jobtype() != null) {
            _hashCode += getC_jobtype().hashCode();
        }
        if (getC_operatetime() != null) {
            _hashCode += getC_operatetime().hashCode();
        }
        if (getC_operator() != null) {
            _hashCode += getC_operator().hashCode();
        }
        if (getC_orgunitcode() != null) {
            _hashCode += getC_orgunitcode().hashCode();
        }
        if (getC_jobcode() != null) {
            _hashCode += getC_jobcode().hashCode();
        }
        if (getC_enddate() != null) {
            _hashCode += getC_enddate().hashCode();
        }
        if (getC_oid_employeejob() != null) {
            _hashCode += getC_oid_employeejob().hashCode();
        }
        if (getC_orgunitname() != null) {
            _hashCode += getC_orgunitname().hashCode();
        }
        if (getC_oldenddate() != null) {
            _hashCode += getC_oldenddate().hashCode();
        }
        if (getC_expectantenddate() != null) {
            _hashCode += getC_expectantenddate().hashCode();
        }
        if (getC_empoid() != null) {
            _hashCode += getC_empoid().hashCode();
        }
        if (getC_type() != null) {
            _hashCode += getC_type().hashCode();
        }
        if (getC_jobid() != null) {
            _hashCode += getC_jobid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TB_PER_EMPLOYEEJOBLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hr.publisher.mdm", "TB_PER_EMPLOYEEJOBLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_appraise");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_appraise"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versioncode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versioncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_orgunitid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_orgunitid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_begindate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_begindate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versiontime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versiontime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versiondelete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versiondelete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_remark");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_remark"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobstatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_jobstatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versionid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_jobname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobtype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_jobtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_operatetime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_operatetime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_operator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_operator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_orgunitcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_orgunitcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_jobcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_enddate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_enddate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_oid_employeejob");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_oid_employeejob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_orgunitname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_orgunitname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_oldenddate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_oldenddate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_expectantenddate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_expectantenddate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_empoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_empoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_jobid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_jobid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
