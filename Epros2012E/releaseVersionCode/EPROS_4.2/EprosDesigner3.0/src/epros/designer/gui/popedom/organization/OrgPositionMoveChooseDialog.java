package epros.designer.gui.popedom.organization;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class OrgPositionMoveChooseDialog extends JecnMoveChooseDialog {
	private static Logger log = Logger.getLogger(OrgPositionMoveChooseDialog.class);
	/**设置统一部门下岗位名称相同 0:不允许 1：允许*/
	private Long allowSamePosName=null;
	public OrgPositionMoveChooseDialog(List<JecnTreeNode> listMoveNodes,JecnTree jTree) {
		super(listMoveNodes,jTree);
	}
   
	/**
     * 组织、岗位节点移动保存
     * @param ids 要移动的节点ID集合
     * @param pid 移动到的节点ID
     * @param orgNumber 组织编号
     * @return 
     *     boolean  true 节点移动成功  false 移动失败 
     */
	@Override
	public boolean savePosOrgData(List<Long> ids, Long pid,String orgNumber){
		try {
			//判断是组织移动还是岗位移动
			if(this.getMoveNodeType()==TreeNodeType.organization){
				ConnectionPool.getOrganizationAction().moveNodesOrg(ids, pid,orgNumber,null,this.getMoveNodeType());
				
			}else{
				ConnectionPool.getOrganizationAction().moveNodesPos(ids, pid,orgNumber,null,getMoveNodeType());
			}
			JecnOptionPane.showMessageDialog(JecnSystemStaticData
					.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("OrgPositionMoveChooseDialog savePosOrgData is error！",e);
			return false;
		}
	}
	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		
		return false;
	}

	@Override
	public JecnTree getJecnTree() {
		if (JecnConstants.isMysql) {
			return new RoutineOrgPositionMoveTree(this.getListIds());
		} else {
			return new HighEfficiencyOrgPositionMoveTree(this.getListIds(),getListMoveNodes().get(0).getJecnTreeBean().getTreeNodeType());
		}
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title=new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));//名称
		return title;
	}
	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			if(listMoveNodes!=null&&listMoveNodes.size()>0){
				TreeNodeType nodeType=listMoveNodes.get(0).getJecnTreeBean().getTreeNodeType();
				if(nodeType==TreeNodeType.position){//如果是岗位节点移动判断是否允许同一部门下岗位名称相同
					try {
						allowSamePosName = Long.valueOf(ConnectionPool.getConfigAciton()
								.selectValue(5, "allowSamePosName"));
					} catch (Exception e) {
						log.error("OrgPositionMoveChooseDialog line:86 is error", e);
					}
					if(Long.valueOf(allowSamePosName)==1){  //0:不允许 1:允许
						return false;
					}
				}
			}
			List<JecnTreeBean> list=ConnectionPool.getOrganizationAction().getChildOrgsAndPositon(pid,JecnConstants.projectId, false);
			for(JecnTreeNode node:listMoveNodes){
				for(JecnTreeBean tr:list){
					if(node.getJecnTreeBean().getTreeNodeType()==tr.getTreeNodeType()&&node.getJecnTreeBean().getName().equals(tr.getName())){
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved")+"\""+tr.getName()+"\"");
						return true;
					}
				}
			}
		} catch (Exception e) {
			log.error("OrgPositionMoveChooseDialog is error",e);
			return false;
		}
		return false;
	}

	

	
}
