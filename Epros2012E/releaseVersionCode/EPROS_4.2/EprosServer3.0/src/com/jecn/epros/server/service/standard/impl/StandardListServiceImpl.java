package com.jecn.epros.server.service.standard.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.standard.IStandardDao;
import com.jecn.epros.server.service.standard.IStandardListService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.standard.StandardInfoListBean;
import com.jecn.epros.server.webBean.standard.StandardRelatePRFBean;
import com.jecn.epros.server.webBean.standard.StandardWebInfoBean;

/**
 * 标准清单实现
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-14 时间：下午01:34:22
 */
public class StandardListServiceImpl extends AbsBaseService<String, String>
		implements IStandardListService {
	/** 标准Dao */
	private IStandardDao standardDao;

	/**
	 * 返回清单数据List集合
	 * 
	 * @param listObject
	 *            数据查出的对象集合
	 * @param standId
	 *            点击的标准阶段ID
	 * @return StandardWebInfoBean
	 * @throws Exception
	 */
	@Override
	public StandardWebInfoBean getStandardInfoList(Long standId,
			Long projectId, boolean showAll) throws Exception {
		// 当前阶段级别
		int curLevel = findFlowDepth(standId);
		List<Long> listIds=new ArrayList<Long>();
		listIds.add(standId);
		String sql = "WITH BZ AS("
				+ "  SELECT CC.*"
				+ "    FROM JECN_CRITERION_CLASSES CC INNER JOIN ("+JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.standardDir)+") stand on stand.CRITERION_CLASS_ID = cc.CRITERION_CLASS_ID"
				+ " ) "+getSelectSql();
		List<Object[]> listObject = this.standardDao.listNativeSql(sql);
		if (listObject == null) {
			return null;
		}
		// 获取数据库数据
		List<StandardInfoListBean> listStandardInfoListBean = new ArrayList<StandardInfoListBean>();

		// 记录标准信息
		Map<Long, StandardInfoListBean> standMap = new HashMap<Long, StandardInfoListBean>();

		StandardInfoListBean standardListBean = null;
		Long tmpStandId = null;
		// 最大级别
		int maxCount = 0;
		for (Object[] objects : listObject) {
			tmpStandId = valueToLong(objects[1]);
			standardListBean = standMap.get(tmpStandId);
			if (standardListBean == null) {
				standardListBean = new StandardInfoListBean();
				int level = valueToInt(objects[0]);
				// 级别
				standardListBean.setLevel(level);
				// 标准ID
				standardListBean.setStandardId(valueToLong(objects[1]));

				standardListBean.setPreStandId(valueToLong(objects[5]));
				// 标准名称
				standardListBean.setStandName(valueToString(objects[2]));
				// 条款要求
				standardListBean.setStandContent(valueToString(objects[3]));
				// 标准类型
				standardListBean.setStandType(valueToInt(objects[4]));

				standMap.put(tmpStandId, standardListBean);

				listStandardInfoListBean.add(standardListBean);
				if (standardListBean.getStandType() == 0 && maxCount < level) {
					maxCount = level;
				}
			}
			// 获取标准相关文件信息
			getRefFileInfo(objects, standardListBean);
		}
		// 带顺序的结果集
		List<StandardInfoListBean> listSortResult = new ArrayList<StandardInfoListBean>();
		processListSort(standId, listStandardInfoListBean, listSortResult,
				new HashSet<Long>());

		standMap.clear();
		listStandardInfoListBean.clear();

		StandardWebInfoBean standardWebInfoBean = new StandardWebInfoBean();

		standardWebInfoBean.setListInfoBean(listSortResult);
		standardWebInfoBean.setMaxLevel(maxCount+1);
		standardWebInfoBean.setCurLevel(curLevel);
		return standardWebInfoBean;
	}

	public byte[] getDownLodeStandFileBytes(Long standId, Long projectId)
			throws Exception {
		getStandardInfoList(standId, projectId, true);
		return null;
	}

	/**
	 * 标准清单
	 * 
	 * @param standId
	 *            传入标准ID
	 * @param listResult
	 *            根据传入ID获取的结果集
	 * @param listSortResult
	 *            返回带顺序的结果集
	 * @param standIdSet
	 *            记录已处理的标准ID
	 */
	private void processListSort(Long standId,
			List<StandardInfoListBean> listResult,
			List<StandardInfoListBean> listSortResult, Set<Long> standIdSet) {
		for (StandardInfoListBean standBean : listResult) {
			if (standId.equals(standBean.getStandardId())) {
				listSortResult.add(standBean);
			} else if (standId.equals(standBean.getPreStandId())) {
				boolean isQuery = false;
				for (Long processId : standIdSet) {
					if (processId.equals(standBean.getStandardId())) {
						isQuery = true;
					}
				}
				if (!isQuery) {
					standIdSet.add(standBean.getStandardId());
					processListSort(standBean.getStandardId(), listResult,
							listSortResult, standIdSet);
				}
			}
		}
	}

	/**
	 * 获取标准相关文件信息
	 * 
	 * @param objects
	 * @param standardListBean
	 */
	private void getRefFileInfo(Object[] objects,
			StandardInfoListBean standardListBean) {
		StandardRelatePRFBean prfBean = null;
		if (objects[7] != null) {// 标准为流程标准
			Long flowId = valueToLong(objects[7]);
			if (!standardListBean.getFlowSet().contains(flowId)) {// 没有添加该流程记录
				prfBean = new StandardRelatePRFBean();
				prfBean.setRefId(flowId);
				prfBean.setRefName(valueToString(objects[8]));
				// 1:流程，2：制度文件,3:活动，4：制度模板文件
				prfBean.setRefType(1);
				standardListBean.getListStandardRefBean().add(prfBean);

				standardListBean.getFlowSet().add(flowId);
			}

		}
		if (objects[9] != null) {// 标准关联的活动 9是 活动所属流程ID
			Long figureId = valueToLong(objects[11]);
			if (!standardListBean.getFlowSet().contains(figureId)) {// 没有添加该活动记录
				prfBean = new StandardRelatePRFBean();
				// 流程信息
				prfBean.setRefId(valueToLong(objects[9]));
				prfBean.setRefName(valueToString(objects[10]));
				// 活动信息
				prfBean.setFigureId(figureId);
				prfBean.setActiveName(valueToString(objects[12]));
				// 1:流程，2：制度文件,3:活动，4：制度模板文件
				prfBean.setRefType(3);
				standardListBean.getListStandardRefBean().add(prfBean);

				standardListBean.getActiveSet().add(figureId);
			}

		}
		if (objects[13] != null) {
			Long ruleId = valueToLong(objects[13]);
			if (!standardListBean.getFlowSet().contains(ruleId)) {// 没有添加该制度记录
				prfBean = new StandardRelatePRFBean();
				// 流程信息
				prfBean.setRefId(ruleId);
				int ruleType = valueToInt(objects[14]);
				if (ruleType == 1) {// 制度模板
					// 1:流程，2：制度文件,3:活动，4：制度模板文件
					prfBean.setRefType(4);
				} else if (ruleType == 2) {// 制度文件
					// 1:流程，2：制度文件,3:活动，4：制度模板文件
					prfBean.setRefType(2);
				}
				prfBean.setRefName(valueToString(objects[15]));

				standardListBean.getListStandardRefBean().add(prfBean);
			}
		}
	}

	/**
	 * 对象转换
	 * 
	 * @param object
	 * @return
	 */
	private String valueToString(Object object) {
		return object == null ? "" : object.toString();
	}

	/**
	 * 对象转换
	 * 
	 * @param object
	 * @return
	 */
	private Integer valueToInt(Object object) {
		return object == null ? 0 : Integer.valueOf(object.toString());
	}

	

	
	/**
	 * with 后的select 查询
	 * 
	 * @return
	 */
	private String getSelectSql() {
		return " select BZ.T_LEVEL, "
				+ "BZ.CRITERION_CLASS_ID,"
				+ "  BZ.CRITERION_CLASS_NAME,"
				+ " BZ.CRITERION_CLASS_NAME STAN_CONTENT, "
				+ "BZ.STAN_TYPE, "
				+ "BZ.PRE_CRITERION_CLASS_ID, "
				+ "BZ.SORT_ID,"
				+ "        FT.FLOW_ID,"
				+ "        FT.FLOW_NAME,"
				+ "        FT2.Flow_Id AS FIGURE_FLOW_ID,"
				+ "        FT2.FLOW_NAME AS FIGURE_FLOW,"
				+ "        FIT.FIGURE_ID,"
				+ "        FIT.FIGURE_TEXT,"
				+ "        RT.ID AS RULE_ID,"
				+ "        RT.IS_DIR AS RULE_TYPE,"
				+ "        RT.RULE_NAME"
				+ "   FROM BZ"
				+ "   LEFT JOIN JECN_FLOW_STANDARD FST ON BZ.CRITERION_CLASS_ID ="
				+ "                                         FST.CRITERION_CLASS_ID"
				+ "   LEFT JOIN JECN_FLOW_STRUCTURE FT ON FST.FLOW_ID = FT.FLOW_ID"
				+ "   LEFT JOIN JECN_ACTIVE_STANDARD AST ON BZ.CRITERION_CLASS_ID ="
				+ "                                           AST.STANDARD_ID"
				+ "   LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE FIT ON AST.ACTIVE_ID ="
				+ "                                                FIT.FIGURE_ID"
				+ "   LEFT JOIN JECN_FLOW_STRUCTURE FT2 ON FIT.FLOW_ID = FT2.FLOW_ID"
				+ "   LEFT JOIN JECN_RULE_STANDARD RST ON BZ.CRITERION_CLASS_ID ="
				+ "                                         RST.STANDARD_ID"
				+ "   LEFT JOIN JECN_RULE RT ON RT.ID = RST.RULE_ID "
				+ "   ORDER BY BZ.T_LEVEL,BZ.PRE_CRITERION_CLASS_ID, BZ.SORT_ID, BZ.CRITERION_CLASS_ID";
	}
	

	/**
	 * 查询标准处在的级别
	 * 
	 * @author ZHANGXIAOHU 2013-12-5
	 * @param standId
	 * @return
	 * @throws Exception
	 */
	private int findFlowDepth(Long standId) throws Exception {
		return this.standardDao.get(standId).gettLevel();
	}

	/**
	 * 对象转换
	 * 
	 * @param object
	 * @return
	 */
	private Long valueToLong(Object object) {
		return object == null ? 0 : Long.valueOf(object.toString());
	}

	public IStandardDao getStandardDao() {
		return standardDao;
	}

	public void setStandardDao(IStandardDao standardDao) {
		this.standardDao = standardDao;
	}
}
