package com.jecn.epros.server.bean.system;

import java.io.Serializable;

/***
 * 发邮件固定人员JECN_FIXED_EMAIL
 * 2014-04-09
 *
 */
public class JecnFixedEmail implements Serializable  {
	/**主键ID*/
	private String id;
	/**人员IDPERSON_ID**/
	private Long personId;
	/**人员名称*/
	private String personName;
	/**类型：0:流程1:制度2:文件*/
	private int type;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	

}
