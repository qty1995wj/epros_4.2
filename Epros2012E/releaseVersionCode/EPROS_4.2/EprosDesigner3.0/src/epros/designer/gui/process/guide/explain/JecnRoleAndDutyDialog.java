package epros.designer.gui.process.guide.explain;

import java.util.Map;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.process.roleDetail.RoleDetailDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.table.JecnTable;
import epros.draw.gui.figure.JecnBaseRoleFigure;

public class JecnRoleAndDutyDialog extends RoleDetailDialog {
	/** 点击table的行数 */

	private Map<String, JecnFileDescriptionComponent> listComponent;

	private ProcessOperationDialog processOperationDialog;

	public JecnRoleAndDutyDialog(JecnBaseRoleFigure baseRoleFigure, ProcessOperationDialog processOperationDialog) {
		super(baseRoleFigure);
		this.processOperationDialog = processOperationDialog;
		this.listComponent = processOperationDialog.getListComponent();
		this.setLocationRelativeTo(processOperationDialog);
	}

	/**
	 * 确定事件
	 * 
	 * @author fuzhh Oct 11, 2012
	 */
	public void okButPerformed() {
		super.okButPerformed();
		processOperationDialog.setFlowOperationBean();
		JecnFileDescriptionComponent jecnCompont = listComponent.get("a8");
		if (jecnCompont != null && jecnCompont instanceof JecnRoleAndDuty) {
			JecnRoleAndDuty jecnRoleAndDuty = (JecnRoleAndDuty) jecnCompont;
			jecnRoleAndDuty.getTable().refluseTable();
		}
		jecnCompont = listComponent.get("a10");
		if (jecnCompont != null && jecnCompont instanceof JecnActiveDesc) {
			JecnActiveDesc jecnActiveDesc = (JecnActiveDesc) jecnCompont;
			jecnActiveDesc.getTable().refluseTable();
		}
		this.dispose();
	}

}
