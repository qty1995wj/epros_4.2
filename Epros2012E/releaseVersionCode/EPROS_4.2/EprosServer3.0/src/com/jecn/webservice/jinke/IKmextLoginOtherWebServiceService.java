/**
 * IKmextLoginOtherWebServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke;

public interface IKmextLoginOtherWebServiceService extends javax.xml.rpc.Service {
    public java.lang.String getIKmextLoginOtherWebServicePortAddress();

    public com.jecn.webservice.jinke.IKmextLoginOtherWebService getIKmextLoginOtherWebServicePort() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.jinke.IKmextLoginOtherWebService getIKmextLoginOtherWebServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
