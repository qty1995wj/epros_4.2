package epros.draw.unredo.data;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * 撤销恢复数据记录类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUnRedoDataRecode {
	/** 记录撤销恢复数据集合 */
	private List<JecnUndoRedoData> undoRedoDataRecodeList = new ArrayList<JecnUndoRedoData>();

	/**
	 * 
	 * 添加分割线数据对象到List中
	 * 
	 * @param flowElementData
	 *            JecnAbstractFlowElementData 流程元素数据
	 * 
	 */
	public void addFlowElementData(JecnUndoRedoData undoRedoData) {
		if (undoRedoData == null
				|| undoRedoDataRecodeList.contains(undoRedoData)) {// 为空或已经存在直接返回
			return;
		}
		this.undoRedoDataRecodeList.add(undoRedoData);
	}

	public List<JecnUndoRedoData> getUndoRedoDataRecodeList() {
		return undoRedoDataRecodeList;
	}
}
