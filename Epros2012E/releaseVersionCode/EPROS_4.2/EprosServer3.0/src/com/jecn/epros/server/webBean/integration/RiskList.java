package com.jecn.epros.server.webBean.integration;

import java.util.List;

/**
 * 风险清单
 * 
 * @author fuzhh 2013-12-13
 * 
 * 修改人：chehuanbo
 * 描述：风险清单集合内容修改稿
 * @since V3.06
 */
public class RiskList {
	/** 风险总级别 */
	private int levelTotal;
	/** 风险总数 */
	private int riskCount;
	/** 风险集合 */
	private List<RiskListBean> riskList;	
	
	/**风险清单集合 C*/
	private List<RiskDataListBean> dataListBeans=null;

	public int getLevelTotal() {
		return levelTotal;
	}

	public void setLevelTotal(int levelTotal) {
		this.levelTotal = levelTotal;
	}

	public List<RiskListBean> getRiskList() {
		return riskList;
	}

	public void setRiskList(List<RiskListBean> riskList) {
		this.riskList = riskList;
	}

	public int getRiskCount() {
		return riskCount;
	}

	public void setRiskCount(int riskCount) {
		this.riskCount = riskCount;
	}

	public List<RiskDataListBean> getDataListBeans() {
		return dataListBeans;
	}

	public void setDataListBeans(List<RiskDataListBean> dataListBeans) {
		this.dataListBeans = dataListBeans;
	}
	
}
