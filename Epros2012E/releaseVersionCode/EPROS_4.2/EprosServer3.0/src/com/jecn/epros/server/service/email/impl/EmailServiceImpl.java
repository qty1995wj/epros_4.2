package com.jecn.epros.server.service.email.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.sql.BLOB;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.email.IEmailDao;
import com.jecn.epros.server.email.bean.JecnEmail;
import com.jecn.epros.server.email.bean.JecnEmailAttachment;
import com.jecn.epros.server.email.bean.JecnEmailContent;
import com.jecn.epros.server.email.bean.JecnEmailRelatedAttachment;
import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.emailnew.EmailAttachment;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.service.email.IEmailService;
import com.jecn.epros.server.util.JecnUtil;

@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class EmailServiceImpl extends AbsBaseService<JecnEmail, String> implements IEmailService {
	private final static Log log = LogFactory.getLog(EmailServiceImpl.class);
	private IEmailDao emailDao;

	public IEmailDao getEmailDao() {
		return emailDao;
	}

	public void setEmailDao(IEmailDao emailDao) {
		this.emailDao = emailDao;
	}

	@SuppressWarnings( { "null", "deprecation" })
	@Override
	public List<EmailMessageBean> saveEmail(List<EmailBasicInfo> basicInfos) throws Exception {

		List<EmailMessageBean> emails = new ArrayList<EmailMessageBean>();
		EmailMessageBean emailBean = null;
		// 邮件创建时间
		Date createDate = new Date();
		JecnEmail email = null;
		JecnEmailContent emailContent = null;
		JecnEmailAttachment emailAttachment = null;
		JecnEmailRelatedAttachment related = null;
		/** 附件id的集合 */
		List<String> attachmentIds = null;
		byte[] byteArr = null;
		Session s = this.emailDao.getSession();
		for (EmailBasicInfo basicInfo : basicInfos) {

			// 没有人员收取邮件
			if (basicInfo.getRecipients() == null || basicInfo.getRecipients().isEmpty()) {
				continue;
			}

			// 保存附件表
			attachmentIds = new ArrayList<String>();
			for (EmailAttachment attachment : basicInfo.getAttachments()) {

				emailAttachment = new JecnEmailAttachment();

				byteArr = JecnUtil.getByte(attachment.getAttachment());
				if (byteArr == null) {
					continue;
				}
				emailAttachment.setAttachmentName(attachment.getAttachmentName());

				if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
					emailAttachment.setAttachment(Hibernate.createBlob(byteArr));
					this.emailDao.saveObject(emailAttachment);
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					emailAttachment.setAttachment(Hibernate.createBlob(new byte[1]));
					this.emailDao.saveObject(emailAttachment);
					this.emailDao.getSession().refresh(emailAttachment, LockMode.UPGRADE);
					SerializableBlob sb = (SerializableBlob) emailAttachment.getAttachment();
					java.sql.Blob wrapBlob = sb.getWrappedBlob();
					BLOB blob = (BLOB) wrapBlob;
					OutputStream out = null;
					try {
						out = blob.getBinaryOutputStream();
						out.write(byteArr);
					} catch (SQLException e) {
						log.error("", e);
					} finally {
						if (out != null) {
							out.close();
						}
					}
				}
				attachmentIds.add(emailAttachment.getId());
			}
			s.flush();
			// 保存邮件内容
			emailContent = new JecnEmailContent();
			emailContent.setSubject(basicInfo.getSubject());
			emailContent.setContent(basicInfo.getContent());
			this.emailDao.saveObject(emailContent);
			s.flush();
			// 保存邮件主表
			List<JecnUser> recipients = basicInfo.getRecipients();
			for (JecnUser user : recipients) {
				email = new JecnEmail();
				email.setPeopleId(user.getPeopleId());
				email.setCreateDate(createDate);
				email.setSendState(0);
				email.setContentId(emailContent.getId());

				this.emailDao.saveObject(email);

				// 保存邮件和附件关联中间表
				for (String attachmentId : attachmentIds) {
					related = new JecnEmailRelatedAttachment();
					related.setEmailId(email.getId());
					related.setAttachmentId(attachmentId);
					this.emailDao.saveObject(related);
				}

				emailBean = new EmailMessageBean();
				emailBean.setUser(user);
				emailBean.setSubject(basicInfo.getSubject());
				emailBean.setContent(basicInfo.getContent());
				emailBean.setEmailId(email.getId());
				// 由于暂时只有一个附件所以取第一个，如果将来多个附件，此处以及发送邮件的地方需要更改
				if (!basicInfo.getAttachments().isEmpty()) {
					emailBean.setRunTestFile(basicInfo.getAttachments().get(0).getAttachment());
					emailBean.setFileName(basicInfo.getAttachments().get(0).getAttachmentName());
					emailBean.setAttach(true);
				}
				s.flush();
				emails.add(emailBean);
			}
		}
		return emails;
	}

	@Override
	public List<EmailMessageBean> getEmailMessageList() throws Exception {

		List<Object[]> emailObjs = this.emailDao.getNeedSendEmails();
		// select"
		// + "c.subject,"
		// + "c.content,"
		// + "u.true_name,"
		// + "u.email,"
		// + "u.email_type,"
		// + "at.attachment,"
		// + "at.attachment_name"
		EmailMessageBean bean = null;
		File attachment = null;
		JecnUser user = null;
		List<EmailMessageBean> emailList = new ArrayList<EmailMessageBean>();
		for (Object[] obj : emailObjs) {

			if (obj[0] == null || obj[1] == null || obj[2] == null || obj[7] == null) {
				continue;
			}

			bean = new EmailMessageBean();
			user = new JecnUser();
			bean.setSubject(obj[0].toString());
			bean.setContent(obj[1].toString());
			if (obj[3] != null) {
				user.setEmail(obj[3].toString());
			}
			if (obj[4] != null) {
				user.setEmailType(obj[4].toString());
			}
			if (obj[8] != null) {
				user.setPeopleId(Long.valueOf(obj[8].toString()));
			}
			if (obj[9] != null) {
				user.setPhoneStr(obj[9].toString());
			}
			bean.setUser(user);
			bean.setEmailId(obj[7].toString());

			if (obj[5] != null && obj[6] != null) {
				attachment = objToFile(obj[5]);
				if (attachment != null) {
					bean.setAttach(true);
					bean.setRunTestFile(attachment);
					bean.setFileName(obj[6].toString());
				}
			}

			emailList.add(bean);
		}

		return emailList;
	}

	private File objToFile(Object object) {
		Blob blob = (Blob) object;
		InputStream in = null;
		OutputStream out = null;
		BufferedInputStream bufIn = null;
		File file = null;
		try {
			in = blob.getBinaryStream();
			file = new File(JecnContants.jecn_path + JecnFinal.saveExcelTempFilePath());
			out = new FileOutputStream(file);
			bufIn = new BufferedInputStream(in);
			byte[] buf = new byte[1024];
			int len = -1;
			while ((len = bufIn.read(buf)) != -1) {
				out.write(buf, 0, len);
			}
			out.flush();

		} catch (Exception e) {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e1) {
					log.error("", e1);
				}
			}
			if (bufIn != null) {
				try {
					bufIn.close();
				} catch (IOException e1) {
					log.error("", e1);
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e1) {
					log.error("", e1);
				}
			}
		}
		return file;
	}

	@Override
	public void updateEmailSendState(int state, String emailId) throws Exception {
		JecnEmail email = this.emailDao.get(emailId);
		if (email != null) {
			email.setSendState(state);
			this.emailDao.update(email);
		} else {
			log.error("邮件未从邮件表中查询到数据 emailId:" + emailId);
		}

		// String sql = "UPDATE JECN_EMAIL SET SEND_STATE=? WHERE ID=?";
		// this.emailDao.execteNative(sql, state, emailId);
	}

	@Override
	public void updateEmailSuccessState(String emailId) throws Exception {
		Date sendDate = new Date();
		JecnEmail jecnEmail = this.emailDao.get(emailId);
		if (jecnEmail != null) {
			jecnEmail.setSendDate(sendDate);
			jecnEmail.setSendState(1);
			this.emailDao.update(jecnEmail);
		} else {
			log.error("updateEmailSuccessState()未查询到记录 emailid:" + emailId);
		}
	}
}
