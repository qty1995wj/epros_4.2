package epros.draw.visio;

/**
 * 
 * 流程元素临时表
 * 
 * @author Administrator
 * 
 */
public class JecnFlowStructureImage implements java.io.Serializable {
	private Long flowId;// 流程ID
	private Long figureId;// 主键ID
	private String figureType;// 元素类型
	private String figureText;// 元素名称
	private Long startFigure;// 连接线输出的图像元素的FigureId
	private Long endFigure;// 连接线输入的图像元素的FigureId
	private Long poLongx;// 图像元素开始点的X
	private Long poLongy;// 图像元素开始点的Y
	private Long width;// 图像元素的宽
	private Long height;// 图像元素的高
	private Long fontSize;// 线的粗细大小 或图形字体的大小
	/** *双色时为单色+第二种颜色+渐变方式 用逗号分隔 如（169,209,208,247,251,252,4） */
	private String BGColor;// 图像元素的填充色
	private String fontColor;// 字体颜色
	private Long linkFlowId;// 连接流程Id或者制度ID
	private String lineColor;// 关键活动
	// :1为PA,2为KSF,3为KCP//1是上游流程,2是下游流程,3是过程流程,4是子流程
	private String fontPosition;// 字体位置
	private Long havaShadow;// 是否有阴影
	private String circumgyrate;// 元素的选择角度
	private String bodyLine;// 线的样式
	private String bodyColor;// 边框颜色
	private Long isErect;// 字体竖排

	private String figureImageId;// 临时的唯一标识
	private Long orderIndex;// 层级
	private String activityShow;// 活动说明
	private String activityId;// 活动编号
	private Long fontBody;// 字体加粗
	private Long frameBody;// 边框加粗
	private String fontType;// 字体类型
	private String roleRes;// 角色职责或者关键活动的控制点
	private Integer is3DEffect;// 是否有3D效果
	/** 0：无填充；1：单色；2：双色 */
	private Integer fillEffects;// 填充效果
	private Integer lineThickness;// 线的深
	private String startFigureUUID;// 开始点元素UUID
	private String endFigureUUID;// 结束点元素UUID
	private String shadowColor;// 阴影颜色
	private int dividingX = 30;// 记录泳池分割点X坐标(目前为横向图)

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getFigureType() {
		return figureType;
	}

	public void setFigureType(String figureType) {
		this.figureType = figureType;
	}

	public String getFigureText() {
		return figureText;
	}

	public void setFigureText(String figureText) {
		this.figureText = figureText;
	}

	public Long getStartFigure() {
		return startFigure;
	}

	public void setStartFigure(Long startFigure) {
		this.startFigure = startFigure;
	}

	public Long getEndFigure() {
		return endFigure;
	}

	public void setEndFigure(Long endFigure) {
		this.endFigure = endFigure;
	}

	public Long getPoLongx() {
		return poLongx;
	}

	public void setPoLongx(Long poLongx) {
		this.poLongx = poLongx;
	}

	public Long getPoLongy() {
		return poLongy;
	}

	public void setPoLongy(Long poLongy) {
		this.poLongy = poLongy;
	}

	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}

	public Long getFontSize() {
		return fontSize;
	}

	public void setFontSize(Long fontSize) {
		this.fontSize = fontSize;
	}

	public String getBGColor() {
		return BGColor;
	}

	public void setBGColor(String color) {
		BGColor = color;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public Long getLinkFlowId() {
		return linkFlowId;
	}

	public void setLinkFlowId(Long linkFlowId) {
		this.linkFlowId = linkFlowId;
	}

	public String getLineColor() {
		return lineColor;
	}

	public void setLineColor(String lineColor) {
		this.lineColor = lineColor;
	}

	public String getFontPosition() {
		return fontPosition;
	}

	public void setFontPosition(String fontPosition) {
		this.fontPosition = fontPosition;
	}

	public Long getHavaShadow() {
		return havaShadow;
	}

	public void setHavaShadow(Long havaShadow) {
		this.havaShadow = havaShadow;
	}

	public String getCircumgyrate() {
		return circumgyrate;
	}

	public void setCircumgyrate(String circumgyrate) {
		this.circumgyrate = circumgyrate;
	}

	public String getBodyLine() {
		return bodyLine;
	}

	public void setBodyLine(String bodyLine) {
		this.bodyLine = bodyLine;
	}

	public String getBodyColor() {
		return bodyColor;
	}

	public void setBodyColor(String bodyColor) {
		this.bodyColor = bodyColor;
	}

	public Long getIsErect() {
		return isErect;
	}

	public void setIsErect(Long isErect) {
		this.isErect = isErect;
	}

	public String getFigureImageId() {
		return figureImageId;
	}

	public void setFigureImageId(String figureImageId) {
		this.figureImageId = figureImageId;
	}

	public Long getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(Long orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getActivityShow() {
		if (!isNullOrEmtryTrim(activityShow)) {
			return activityShow;
		} else {
			return "";
		}
	}

	public void setActivityShow(String activityShow) {
		this.activityShow = activityShow;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public Long getFontBody() {
		return fontBody;
	}

	public void setFontBody(Long fontBody) {
		this.fontBody = fontBody;
	}

	public Long getFrameBody() {
		return frameBody;
	}

	public void setFrameBody(Long frameBody) {
		this.frameBody = frameBody;
	}

	public String getFontType() {
		return fontType;
	}

	public void setFontType(String fontType) {
		this.fontType = fontType;
	}

	public String getRoleRes() {
		return roleRes;
	}

	public void setRoleRes(String roleRes) {
		this.roleRes = roleRes;
	}

	public Integer getIs3DEffect() {
		return is3DEffect;
	}

	public void setIs3DEffect(Integer is3DEffect) {
		this.is3DEffect = is3DEffect;
	}

	public Integer getFillEffects() {
		return fillEffects;
	}

	public void setFillEffects(Integer fillEffects) {
		this.fillEffects = fillEffects;
	}

	public Integer getLineThickness() {
		return lineThickness;
	}

	public void setLineThickness(Integer lineThickness) {
		this.lineThickness = lineThickness;
	}

	public String getStartFigureUUID() {
		return startFigureUUID;
	}

	public void setStartFigureUUID(String startFigureUUID) {
		this.startFigureUUID = startFigureUUID;
	}

	public String getEndFigureUUID() {
		return endFigureUUID;
	}

	public void setEndFigureUUID(String endFigureUUID) {
		this.endFigureUUID = endFigureUUID;
	}

	public String getShadowColor() {
		return shadowColor;
	}

	public void setShadowColor(String shadowColor) {
		this.shadowColor = shadowColor;
	}

	public int getDividingX() {
		return dividingX;
	}

	public void setDividingX(int dividingX) {
		this.dividingX = dividingX;
	}

	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	private boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

}