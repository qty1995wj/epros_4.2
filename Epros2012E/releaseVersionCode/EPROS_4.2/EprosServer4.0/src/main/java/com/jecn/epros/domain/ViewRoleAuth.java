package com.jecn.epros.domain;

import com.jecn.epros.domain.process.TreeNode;

import java.util.List;

/**
 * Created by Angus on 2017/5/6.
 */
public class ViewRoleAuth {
    public int[] getNodes() {
        return nodes;
    }

    public void setNodes(int[] nodes) {
        this.nodes = nodes;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    private int[] nodes;
    private String roleId;


}
