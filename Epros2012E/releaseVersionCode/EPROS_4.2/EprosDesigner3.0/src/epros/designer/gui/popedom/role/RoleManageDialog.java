package epros.designer.gui.popedom.role;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnManageDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class RoleManageDialog extends JecnManageDialog {
	private static Logger log = Logger.getLogger(RoleManageDialog.class);

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("roleManage");
	}

	@Override
	public String getTreePanelTitle() {
		return JecnProperties.getValue("roleManage");
	}

	@Override
	public String getSerachLabelName() {
		return JecnProperties.getValue("roleNameC");
	}

	@Override
	public JecnTree initJecnTree() {
		return new HighEfficiencyRoleTree(this);
		// if (JecnConstants.isMysql) {
		// return new RoutineRoleTree(this);
		// } else {
		// return new HighEfficiencyRoleTree(this);
		// }
	}

	/***************************************************************************
	 * 更新角色
	 */
	@Override
	public void buttonOneAction() {
		int[] rows = this.getJecnTable().getSelectedRows();
		if (rows.length != 1) {
			getjLabelTip().setText(JecnProperties.getValue("chooseOneRow"));
		} else {
			JecnTreeBean jecnTreeBean = this.getSearchList().get(rows[0]);
			RoleDialog editRoleDialog = null;
			if ("secondAdmin".equals(jecnTreeBean.getNumberId())) {// 二级管理员节点
				JecnTreeNode selectNode = JecnTreeCommon.getTreeNode(jecnTreeBean, this.getjTree());
				if (selectNode == null) {
					return;
				}
				editRoleDialog = new SecondAdminRoleDialog(selectNode, this.getjTree());
			} else {
				editRoleDialog = new EditRoleDialog(jecnTreeBean, this.getjTree());
			}
			editRoleDialog.setVisible(true);

			if (editRoleDialog.isOperation()) {
				getSearchList().get(rows[0]).setName(editRoleDialog.getRoleName());
				getJecnTable().getModel().setValueAt(editRoleDialog.getRoleName(), rows[0], 0);
				JecnTreeNode jecnTreeNode = JecnTreeCommon.getTreeNode(jecnTreeBean, this.getjTree());
				if (jecnTreeNode != null) {
					JecnTreeCommon.reNameNode(this.getjTree(), jecnTreeNode, editRoleDialog.getRoleName());
				}
				// 提示更新成功
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("updateSuccess"));
			}

		}
	}

	@Override
	public boolean isAddButtonOne() {
		return true;
	}

	/**
	 * 更新角色
	 */
	@Override
	public String buttonOneName() {
		return JecnProperties.getValue("updateRole");
	}

	@Override
	public void buttonTwoAction() {

	}

	@Override
	public String buttonTwoName() {
		return null;
	}

	@Override
	public boolean isAddButtonTwo() {
		return false;
	}

	@Override
	public void buttonThreeAction() {

	}

	@Override
	public String buttonThreeName() {
		return null;
	}

	@Override
	public boolean isAddButtonThree() {
		return false;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		// if (JecnConstants.isMysql) {
		// List<JecnTreeBean> list = JecnTreeCommon.findAllTreeNodes(
		// this.getjTree(), name);
		// return list;
		// } else {
		try {
			return ConnectionPool.getJecnRole().getRolesByName(JecnConstants.projectId, name,
					JecnDesignerCommon.isSecondAdmin() ? JecnConstants.getUserId() : null);
		} catch (Exception e) {
			log.error("RoleManageDialog searchByName is error！", e);
			return new ArrayList<JecnTreeBean>();
		}

		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> vector = new Vector<String>();
		vector.add(JecnProperties.getValue("role"));
		vector.add("ID");
		return vector;
	}

	@Override
	public Vector<Vector<String>> updateTableContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		for (JecnTreeBean jecnTreeBean : list) {
			Vector<String> v = new Vector<String>();
			v.add(jecnTreeBean.getName());
			v.add(jecnTreeBean.getId().toString());
			vs.add(v);
			v = null;
		}
		return vs;
	}

	@Override
	public boolean deleteData(List<Long> listIds) {
		try {
			ConnectionPool.getJecnRole().deleteRoles(listIds, JecnConstants.projectId, JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("RoleManageDialog deleteData is error！", e);
			return false;
		}
		return true;
	}
}
