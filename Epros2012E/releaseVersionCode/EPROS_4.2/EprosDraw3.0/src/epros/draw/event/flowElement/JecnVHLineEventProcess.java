package epros.draw.event.flowElement;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.List;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.JecnTempFigurePanel;
import epros.draw.gui.figure.unit.JecnDraggedVHLineUnit;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.figure.unit.SnapToGrid;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 横竖分割线鼠标事件处理类
 * 
 * @author ZHANGXH
 * @date： 日期：Jun 12, 2012 时间：4:22:47 PM
 */
public class JecnVHLineEventProcess {
	/** 是否执行拖动 */
	private static boolean isDragged = false;

	/** 拖动分割线生成虚拟分割线 */
	// private static JecnTempFigurePanel tempFigurePanel = null;
	/** 拖动前横向位置点 */
	private static int oldX = 0;
	/** 拖动前纵向位置点 */
	private static int oldY = 0;
	/** 横竖分割线拖动事件处理类 */
	public static JecnDraggedVHLineUnit lineUnit = null;

	/**
	 * 横竖分割线点击事件处理
	 * 
	 * @param e
	 */
	public void mouseVHLinePressed(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || desktopPane.getJecnTempFigurePanel() != null) {
			return;
		}
		// 不是左键返回（BUTTON1(1：左键）,BUTTON2（2：中键）,BUTTON3（3：右键））
		if (((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0)
				&& ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0)) {
			// 左右键同时按住时,不设置按下位置点
		} else {
			JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) e.getSource();
			// 获取拖动要生成的虚拟图形
			JecnTempFigurePanel tempFigurePanel = new JecnTempFigurePanel();

			// 获取拖动要生成的虚拟图形
			desktopPane.setJecnTempFigurePanel(tempFigurePanel);

			// 设置虚拟图形临时数据
			JecnTempFigureBean tempFigureBean = baseVHLinePanel.getTempFigureBean();

			Point tempPoint = null;
			switch (baseVHLinePanel.getLineDirectionEnum()) {
			case horizontal:
				// 鼠标点击横向分割线记录点击时鼠标的纵坐标
				oldY = e.getY();

				tempPoint = new Point(0, baseVHLinePanel.getLocation().y);
				// 设置图形位置点和大小
				tempFigureBean.reSetTempFigureBean(tempPoint, desktopPane.getFlowMapData().getWorkflowWidth(),
						baseVHLinePanel.getFlowElementData().getBodyWidth());
				tempFigurePanel.setBounds(tempPoint, desktopPane.getFlowMapData().getWorkflowWidth(), baseVHLinePanel
						.getFlowElementData().getBodyWidth());
				break;
			case vertical:
				oldX = e.getX();

				tempPoint = new Point(baseVHLinePanel.getLocation().x, 0);
				// 设置图形位置点和大小
				tempFigureBean.reSetTempFigureBean(tempPoint, baseVHLinePanel.getFlowElementData().getBodyWidth(),
						desktopPane.getFlowMapData().getWorkflowHeight());
				// 鼠标点击横向分割线记录点击时鼠标的横坐标
				tempFigurePanel.setBounds(new Point(baseVHLinePanel.getLocation().x, 0), baseVHLinePanel
						.getFlowElementData().getBodyWidth(), desktopPane.getFlowMapData().getWorkflowHeight());
				break;
			default:
				break;
			}

			tempFigurePanel.getTempFigureList().add(tempFigureBean);
		}
	}

	/**
	 * 横竖分割线鼠标释放事件处理
	 * 
	 * @param e
	 */

	public void mouseVHLineReleased(MouseEvent e) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getJecnTempFigurePanel() == null) {
			return;
		}

		if (isDragged) {
			// 还原拖动状态
			isDragged = false;
			JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) e.getSource();

			Point oldPoint = baseVHLinePanel.getLocation();
			int diffXY = 0;
			switch (baseVHLinePanel.getFlowElementData().getLineDirectionEnum()) {
			case horizontal:
				// 横线去垂直位移
				diffXY = workflow.getJecnTempFigurePanel().getLocation().y - oldPoint.y;
				lineUnit.moveAllFiguresAndLines(oldPoint.y, diffXY);
				break;
			case vertical:
				diffXY = workflow.getJecnTempFigurePanel().getLocation().x - oldPoint.x;
				lineUnit.moveAllFiguresAndLines(oldPoint.x, diffXY);
				break;
			default:
				break;
			}
			// 面板删除横竖分割线选中点
			if (baseVHLinePanel.isShowResizeHandle()) {
				baseVHLinePanel.hideParallelLinesResizeHandles();
			}
			// 清空分割线记录的临时数据集合
			lineUnit.cleanTempList();
			// 面板清除虚拟图形
			workflow.remove(workflow.getJecnTempFigurePanel());

			// 重新设置角色移动
			workflow.removeAddRoleMove();

		} else {
			if (!isDragged) {// 没有执行拖动
				// ***************5.右键菜单显示***************//
				JecnFlowElementEventProcess.rightPopupMenuProcess(e);
				return;
			}
		}

		// 清除临时线
		workflow.setJecnTempFigurePanel(null);
	}

	/**
	 * 
	 * 横竖分割线拖拽处理
	 * 
	 * @param e
	 */
	public void mouseVHLineDragged(MouseEvent e) {
		// 获取当前画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 不是左键返回（BUTTON1(1：左键）,BUTTON2（2：中键）,BUTTON3（3：右键））
		if ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) == 0 || desktopPane == null
				|| desktopPane.getJecnTempFigurePanel() == null) {// 点击的不是鼠标左键返回
			return;
		}

		// 当前待拖动图形
		JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) e.getSource();

		// 获取面板所有流程元素集合
		List<JecnBaseFlowElementPanel> allFlowList = desktopPane.getPanelList();
		if (!isDragged) {
			isDragged = true;
			initFigureList(allFlowList, baseVHLinePanel.getLineDirectionEnum(), baseVHLinePanel.getLocation(),
					desktopPane.getJecnTempFigurePanel());
		}
		draggedTempLine(baseVHLinePanel.getLineDirectionEnum(), e.getPoint(), baseVHLinePanel.getLocation());

		desktopPane.updateUI();
	}

	/**
	 * 时时拖动虚拟分割线
	 * 
	 * @param allFlowList
	 * @param lineDirectionEnum
	 * @param oldPoint
	 * @param tempFigurePanel
	 */
	public static void initFigureList(List<JecnBaseFlowElementPanel> allFlowList, LineDirectionEnum lineDirectionEnum,
			Point oldPoint, JecnTempFigurePanel tempFigurePanel) {
		// 获取当前画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		lineUnit = new JecnDraggedVHLineUnit(lineDirectionEnum);
		lineUnit.setOldPoint(oldPoint);
		if (tempFigurePanel != null) {
			// 添加虚拟图形到面板
			desktopPane.add(tempFigurePanel);
		}
		// 获取拖动前数据
		switch (lineDirectionEnum) {
		case horizontal:// 拖动的分割线是横向分割线
			initHorizontalValues(allFlowList, oldPoint);
			break;
		case vertical:// 拖动的分割线是纵向分割线
			initVerticalValues(allFlowList, oldPoint);

			break;
		default:
			break;
		}
	}

	public static void draggedTempLine(LineDirectionEnum lineDirectionEnum, Point ePoint, Point oldPoint) {
		// 获取当前画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || lineDirectionEnum == null || ePoint == null) {
			return;
		}
		// 获取分割线间拖动时未选中的图形的最大值(拖动为横线，横线下的图形为选中图形，横线上面的图形为未选中图形)
		int maxFigureXY = lineUnit.getMaxFigureDiff();
		// 拖动虚拟分割线
		reSetTempFigure(oldPoint, ePoint, lineDirectionEnum, desktopPane.getJecnTempFigurePanel(), maxFigureXY);
	}

	/**
	 * 获取点击时横向分割线相关数据
	 * 
	 * @param allFlowList
	 * @param baseVHLinePanel
	 */
	public static void initHorizontalValues(List<JecnBaseFlowElementPanel> allFlowList, Point horPoint) {
		if (allFlowList == null || horPoint == null) {
			return;
		}
		for (JecnBaseFlowElementPanel flowElementPanel : allFlowList) {
			if (flowElementPanel instanceof JecnBaseFigurePanel) {
				// 获取图形对象
				JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) flowElementPanel;
				if (figurePanel.getLocation().getY() >= horPoint.y) {
					// 图形
					lineUnit.getAllFigureList().add(figurePanel);
					// 获取图形相关连接线
					// JecnDraggedFiguresPaintLine.getManhattanLineList(
					// baseVHLinePanel.getAllManLineList(), figurePanel);
				} else {
					lineUnit.getOtherFigureList().add(figurePanel);
				}
			} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {// 分割线
				JecnBaseDividingLinePanel diviLine = (JecnBaseDividingLinePanel) flowElementPanel;

				if (diviLine.getStartPoint().getY() >= horPoint.y && diviLine.getEndPoint().getY() >= horPoint.y) {
					// 添加到数组
					lineUnit.getAllDividLineList().add(diviLine);
				}
			} else if (flowElementPanel instanceof JecnBaseVHLinePanel) {// 拖动的横线
				JecnBaseVHLinePanel linePanel = (JecnBaseVHLinePanel) flowElementPanel;
				if (linePanel.getY() >= horPoint.y) {// 拖动的横线Y坐标必须小于当前横线Y坐标
					lineUnit.getAllVHLineList().add(linePanel);
				}
			}
		}
	}

	/**
	 * 获取点击时纵向分割线相关数据
	 * 
	 * @param allFlowList
	 * @param baseVHLinePanel
	 */
	public static void initVerticalValues(List<JecnBaseFlowElementPanel> allFlowList, Point verPoint) {
		if (allFlowList == null || verPoint == null) {
			return;
		}
		for (JecnBaseFlowElementPanel flowElementPanel : allFlowList) {
			if (flowElementPanel instanceof JecnBaseFigurePanel) {// 图形
				// 获取图形对象
				JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) flowElementPanel;
				if (figurePanel.getLocation().getX() >= verPoint.x) {
					// 记录拖动前图形
					lineUnit.getAllFigureList().add(figurePanel);
					// 获取图形相关连接线
					// JecnDraggedFiguresPaintLine.getManhattanLineList(
					// baseVHLinePanel.getAllManLineList(), figurePanel);
				} else {
					lineUnit.getOtherFigureList().add(figurePanel);
				}
			} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {// 分割线
				JecnBaseDividingLinePanel diviLine = (JecnBaseDividingLinePanel) flowElementPanel;
				if (diviLine.getStartPoint().getX() >= verPoint.x && diviLine.getEndPoint().getX() >= verPoint.x) {
					lineUnit.getAllDividLineList().add(diviLine);
				}
			} else if (flowElementPanel instanceof JecnBaseVHLinePanel) {// 拖动的横竖分割线
				JecnBaseVHLinePanel linePanel = (JecnBaseVHLinePanel) flowElementPanel;
				if (linePanel.getX() >= verPoint.x) {
					lineUnit.getAllVHLineList().add(linePanel);
				}
			}
		}
	}

	/**
	 * 拖动虚拟分割线
	 * 
	 * @param oldPoint
	 *            鼠标点击时的位置点
	 * @param ePoint
	 *            鼠标当前位置点
	 * @param lineDirectionEnum
	 *            横线或竖线标识
	 * @param tempFigurePanel
	 *            拖动生成的虚拟图形
	 * @param maxFigureXY
	 *            获取分割线间拖动时未选中的图形的最大值(拖动为横线，横线下的图形为选中图形，横线上面的图形为未选中图形)
	 */
	public static void reSetTempFigure(Point oldPoint, Point ePoint, LineDirectionEnum lineDirectionEnum,
			JecnTempFigurePanel tempFigurePanel, int maxFigureXY) {
		if (oldPoint == null || ePoint == null || lineDirectionEnum == null) {
			return;
		}
		Point newPoint = null;
		int diffXY = 0;
		// 获取拖动前数据
		switch (lineDirectionEnum) {
		case horizontal:
			// 获取位移 横线获取垂直位移
			diffXY = ePoint.y - oldY;
			// if (baseVHLinePanel.getY() <= 1 && diffXY < 0) {
			// return;
			// }
			// 按网格标准获取新的新的坐标值：选中分割线位置+位移
			newPoint = SnapToGrid.checkIsGrid(0, oldPoint.y + diffXY);
			if (newPoint.y < maxFigureXY + 10) {
				newPoint.y = maxFigureXY + 10;
			}
			// 如果鼠标横坐标小于60 强制转换成60
			if (newPoint.y < 10) {
				newPoint.y = 10;
			}
			// 重新获取位移
			diffXY = newPoint.y - oldPoint.y;

			tempFigurePanel.setLocation(0, oldPoint.y + diffXY);
			break;
		case vertical:
			// 获取位移 竖线获取水平位移
			diffXY = ePoint.x - oldX;
			// if (baseVHLinePanel.getX() <= 1 && diffXY < 0) {
			// return;
			// }
			// 按网格标准获取新的新的坐标值：选中分割线位置+位移
			newPoint = SnapToGrid.checkIsGrid(oldPoint.x + diffXY, 0);
			if (newPoint.x < maxFigureXY + 10) {
				newPoint.x = maxFigureXY + 10;
			}
			// 如果鼠標横坐标小于60 强制转换成60
			if (newPoint.x < 10) {
				newPoint.x = 10;
			}
			// 重新获取位移
			diffXY = newPoint.x - oldPoint.x;

			tempFigurePanel.setLocation(oldPoint.x + diffXY, 0);
			break;
		default:
			break;
		}
	}

	/**
	 * 鼠标移动到分割线鼠标状态
	 * 
	 * @param e
	 */
	public void mouseVHLineMove(MouseEvent e) {
		if (e.getSource() == null || !(e.getSource() instanceof JecnBaseVHLinePanel)) {
			return;
		}
		JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) e.getSource();
		switch (baseVHLinePanel.getLineDirectionEnum()) {
		case horizontal:// 横向分割线，鼠标显示垂直方向双向箭头
			baseVHLinePanel.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			break;
		case vertical:// 纵向分割线，鼠标显示水平方向双向箭头
			baseVHLinePanel.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			break;
		default:
			break;
		}
	}

	public static int getOldX() {
		return oldX;
	}

	public static void setOldX(int oldX) {
		JecnVHLineEventProcess.oldX = oldX;
	}

	public static int getOldY() {
		return oldY;
	}

	public static void setOldY(int oldY) {
		JecnVHLineEventProcess.oldY = oldY;
	}

	public static boolean isDragged() {
		return isDragged;
	}

	public static void setDragged(boolean isDragged) {
		JecnVHLineEventProcess.isDragged = isDragged;
	}
}
