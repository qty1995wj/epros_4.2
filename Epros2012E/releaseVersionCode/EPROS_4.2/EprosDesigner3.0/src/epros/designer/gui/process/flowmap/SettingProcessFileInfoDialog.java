package epros.designer.gui.process.flowmap;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.common.JecnFinal;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.visio.JecnFileNameFilter;

public class SettingProcessFileInfoDialog extends JecnEditNameDialog {

	/** 上传文件Lab */
	private JLabel upLoadFileLab;

	/** 上传文件显示 */
	private JTextField upLoadFileTextField;
	/** 清空按钮 **/
	private JButton clearBut;

	private File file;

	private Long selectId = 0L;

	private FlowFileHeadData flowFileHeadData = null;

	public SettingProcessFileInfoDialog(Long selectId) {
		this.selectId = selectId;
		// 设置Dialog大小
		this.setSize(getWidthMin(), 180);
		upLoadFileTextField.setEnabled(false);
		this.setLocationRelativeTo(null);
		initData();// 初始化数据
	}

	private void initData() {
		// new FlowFileHeadData();获取
		flowFileHeadData = ConnectionPool.getProcessAction().findFlowFileHeadInfo(selectId);
		if (flowFileHeadData != null) {
			this.nameTextField.setText(flowFileHeadData.getCompanyName());
			this.eNameTextField.setText(flowFileHeadData.getCompanyEnName());
			this.upLoadFileTextField.setText(flowFileHeadData.getFileName());
		} else {
			flowFileHeadData = new FlowFileHeadData();
		}
		file = new File(StringUtils.isNotBlank(flowFileHeadData.getFilePath()) ? flowFileHeadData.getFilePath() : "");
	}

	/**
	 * 设置文件上传text
	 */
	protected void addUpfilePanel(Insets insets) {
		// 英文名称Lab
		upLoadFileLab = new JLabel("LOGO");
		GridBagConstraints c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(upLoadFileLab, c);
		// 英文名称填写框
		upLoadFileTextField = new JTextField();
		c = new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		topPanel.add(upLoadFileTextField, c);
		clearBut = new JButton(JecnProperties.getValue("clear"));
		c = new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(clearBut, c);

	}

	protected void addOtherBut() {
		upLoadFileTextField.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int c = e.getButton();
				if (c == MouseEvent.BUTTON1)// 判断是鼠标左键按下
				{
					uploadPicture();
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseReleased(MouseEvent e) {

			}
		});

		clearBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				file = new File("");
				upLoadFileTextField.setText("");
				if (flowFileHeadData != null) {
					flowFileHeadData.setFilePath("");
				}
			}
		});
	}

	/**
	 * 选择上传的图片
	 */
	private void uploadPicture() {
		JecnFileChooser fileChoose = new JecnFileChooser("");
		fileChoose.setEidtTextFiled(true);
		// 可以同时上传多个文件
		fileChoose.setMultiSelectionEnabled(false);
		String[] arrs = { "jpg", "JPG", "PNG", "png" };
		if (arrs != null) {
			// 设置过滤条件
			fileChoose.setFileFilter(new JecnFileNameFilter(arrs));
			// 移除系统给定的文件过滤器
			fileChoose.setAcceptAllFileFilterUsed(false);
		}
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
		int i = fileChoose.showSaveDialog(this);
		if (i == JFileChooser.APPROVE_OPTION) {
			file = fileChoose.getSelectedFile();// 获取选择的文件
			flowFileHeadData.setFilePath("");// 因为公用对象的 原因 每次选择文件 要清除 filepath
			upLoadFileTextField.setText(file.getName());// 获取文件名称赋值文本框赋值
		}
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("settingsInstruction");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		try {
			if (validateFlowFilePicture()) {// 判断文件不为空 创建本地文件
				createlocalFile();
			}
			// 中文名称（公司名称）
			flowFileHeadData.setCompanyName(this.nameTextField.getText());
			// 英文名称 （公司名称）
			flowFileHeadData.setCompanyEnName(this.eNameTextField.getText());
			// logo文件名称
			flowFileHeadData.setFileName(StringUtils.isBlank(this.upLoadFileTextField.getText()) ? " "
					: this.upLoadFileTextField.getText());
			// 流程Id
			flowFileHeadData.setFlowId(selectId);
			// 更新时间
			flowFileHeadData.setUpdateTime(new Date());
			ConnectionPool.getProcessAction().addFlowFileHeadInfo(flowFileHeadData);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("updateSuccess"));
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			e.printStackTrace();
		} finally {
			this.dispose();
		}
	}


	protected boolean validateFlowFilePicture() {
		if (!file.exists()) {
			return false;
		}
		return true;
	}

	/**
	 * 创建本地文件
	 */
	private void createlocalFile() {
		// 查询文件地址 等于空 的情况 为新建 创建本地文件
		if (StringUtils.isBlank(flowFileHeadData.getFilePath())) {
			try {
				// 获取文件后缀名 以及 保存的路径
				String suffixName = JecnFinal.getSuffixName(file.getName());
				flowFileHeadData.setFilePath(ConnectionPool.getFileAction().saveFilePath("flowHeadInfo/", new Date(),
						suffixName));
				// 在本地目录下创建文件
				byte[] fileByte = JecnUtil.changeFileToByte(file.getPath());
				ConnectionPool.getFileAction().createFile(flowFileHeadData.getFilePath(), fileByte);
				// JecnUtil.createFile(flowFileHeadData.getFilePath(),
				// JecnUtil.changeFileToByte(file.getPath()));
			} catch (IOException e) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return false;
	}

}
