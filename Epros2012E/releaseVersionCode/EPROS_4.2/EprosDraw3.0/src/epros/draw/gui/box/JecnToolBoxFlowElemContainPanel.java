package epros.draw.gui.box;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素面板，存放流程元素图标的容器
 * 
 * @author ZHOUXY
 * 
 */
class JecnToolBoxFlowElemContainPanel extends JecnToolBoxAbstractContainPanel {

	public JecnToolBoxFlowElemContainPanel(JecnToolBoxMainPanel toolBoxMainPanel) {
		super(toolBoxMainPanel);
		initComponent();
	}

	private void initComponent() {

		// 流程元素标题名称
		nameLabel.setText(toolBoxMainPanel.getResourceManager().getValue("title"));
		// 流程元素标题按钮：选择图标显示类型
		typeBtn.setIcon(JecnFileUtil.getToolBoxImageIcon("down"));
		// 流程元素添加符号按钮
		this.commonBtn.setIcon(JecnFileUtil.getToolBoxImageIcon("addFigure"));
		commonBtn.setToolTipText(toolBoxMainPanel.getResourceManager().getValue("addFigure"));
		// 大小
		Dimension flowElemSize = new Dimension(JecnUIUtil.getBoxBtnWidth(), 390);
		btnsPanel.setPreferredSize(flowElemSize);
	}

	/**
	 * 
	 * 点击图标按钮事件
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		if (toolBoxMainPanel.getCurrShowMapType() == null || toolBoxMainPanel.getCurrShowMapType() == MapType.none
				|| toolBoxMainPanel.getCurrShowMapType() == MapType.totalMapRelation) {// 不是流程图/流程地图/组织图
			return;
		}
		if (e.getSource() == commonBtn) {// 点击添加符号按钮
			// if (EprosType.eprosDesigner ==
			// JecnSystemStaticData.getEprosType())
			// {// 设计器
			// JecnDesignerProcess.getDesignerProcess()
			// .processAddGenFlowElem();
			// } else {// 画图工具
			addSignActionPerformed();
			// }

		} else if (e.getSource() == typeBtn) {// 点击显示类型
			// 按钮显示类型动作事件
			typeBtnActionPerformed();
		}
	}

	/**
	 * 
	 * 添加符号动作事件
	 * 
	 */
	private void addSignActionPerformed() {
		// 本地Properties文件显示流程元素
		List<MapElemType> showTypeList = getShowTypeList();
		// 对话框
		Dimension sizeDialog = getSizeDialog();

		// 添加符号
		JecnAddSignJDialog addSignJDialog = new JecnAddSignJDialog(toolBoxMainPanel.getToolBoxScrollPane(),
				showTypeList);
		// 大小
		addSignJDialog.setSize(sizeDialog);
		// 居中
		addSignJDialog.setLocationRelativeTo(null);

		addSignJDialog.setVisible(true);
	}

	private List<MapElemType> getShowTypeList() {
		List<MapElemType> showTypeList = null;
		switch (toolBoxMainPanel.getCurrShowMapType()) {
		case partMap:// 流程图
			showTypeList = getProcessElemType(0);
			break;
		case totalMap:// 流程地图
			showTypeList = getProcessElemType(1);
			break;
		case orgMap:// 组织图
			showTypeList = getProcessElemType(2);
			break;
		case none:
		default:
			break;
		}
		return showTypeList;
	}

	public List<MapElemType> getProcessElemType(int type) {
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(type);
		if (showTypeList == null || showTypeList.size() == 0) {
			showTypeList = JecnSystemData.readPartListByPropertyFile();
		}
		return showTypeList;
	}

	public List<MapElemType> getProcessMapElemType(int type) {
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(type);
		if (showTypeList == null || showTypeList.size() == 0) {
			showTypeList = JecnSystemData.readTotalListByPropertyFile();
		}
		return showTypeList;
	}

	public List<MapElemType> getOrgElemType(int type) {
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(type);
		if (showTypeList == null || showTypeList.size() == 0) {
			showTypeList = JecnSystemData.readOrgListByPropertyFile();
		}
		return showTypeList;
	}

	private Dimension getSizeDialog() {
		Dimension sizeDialog = null;
		switch (toolBoxMainPanel.getCurrShowMapType()) {
		case partMap:// 流程图
			sizeDialog = new Dimension(776, 621);
			break;
		case totalMap:// 流程地图
			sizeDialog = new Dimension(718, 614);
			break;
		case orgMap:// 组织图
			sizeDialog = new Dimension(610, 405);
			break;
		case none:
		default:
			break;
		}
		return sizeDialog;
	}

	/**
	 * 
	 * 按钮显示类型动作事件
	 * 
	 */
	private void typeBtnActionPerformed() {
		if (coverButtonPopupMenu == null) {
			coverButtonPopupMenu = new JecnCoverButtonPopupMenu(this);
		}
		coverButtonPopupMenu.getPopupMenu().show(typeBtn, 0, titlePanel.getHeight());
	}
}
