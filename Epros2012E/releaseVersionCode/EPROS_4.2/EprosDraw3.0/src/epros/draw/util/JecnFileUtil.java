package epros.draw.util;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import epros.draw.constant.JecnWorkflowConstant.FileTypeEnum;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnUserInfo;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 本地资源文件:图片、配置文件等
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFileUtil {
	/** 日志对象 */
	private static final Log log = LogFactory.getLog(JecnFileUtil.class);

	/** 图片文件路径前缀 */
	public static String IMAGES_PATH_PREFIX = "/epros/draw/images/";
	/** 系统使用的配置文件存放路径前缀 */
	private final static String PROPERTY_FILE_PATH_PREFIX = "/epros/draw/configFile/";

	/** 图片文件路径后缀 */
	private final static String IMAGES_PATH_PATH_Suffix = ".gif";

	/**
	 * 
	 * 获取流程元素属性XML文件路径(基于根路径下的路径)
	 * 
	 * @return String 基于根路径下的路径
	 */
	public static String getCurrentModelXMLPath() {
		return PROPERTY_FILE_PATH_PREFIX + "formatModel/currentModel.xml";
	}

	/**
	 * 
	 * 获取流程元素系统默认属性XML文件路径(基于根路径下的路径)
	 * 
	 * @return String 基于根路径下的路径
	 */
	public static String getSystemInitModelXMLPath() {
		return PROPERTY_FILE_PATH_PREFIX + "formatModel/systemInitModel.xml";
	}

	/**
	 * 获取visio图形配置对应数据
	 * 
	 * @author fuzhh 2013-7-12
	 * @return
	 */
	public static String getVisioModeXmlPath() {
		return PROPERTY_FILE_PATH_PREFIX + "visio/visioModeFigure.xml";
	}

	/**
	 * 
	 * 获取创建流程图时使用的图片
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param imageName
	 *            String 图片名称
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getCreatePartMapIcon(String imageName) {
		return getImageIcon("toolbar/dialog/" + imageName);
	}

	/**
	 * 
	 * Epros最小化、最大化、关闭按钮图片：
	 * 
	 * 通过图片名称获取图形对象
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param imageName
	 *            String 图片名称
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getFrameBtnIcon(String imageName) {
		return getImageIcon("frame/" + imageName);
	}

	/**
	 * 
	 * 工具栏标题栏图片：
	 * 
	 * 通过图片名称获取图形对象
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param imageName
	 *            String 图片名称
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getToolBarTitleImageIcon(String imageName) {
		return getImageIcon("header/" + imageName + IMAGES_PATH_PATH_Suffix);
	}

	/**
	 * 
	 * 获取文件类型对应图片
	 * 
	 * @param fileTypeEnum
	 *            FileTypeEnum 文件类型枚举
	 * @return ImageIcon 图片
	 */
	public static ImageIcon getFileTypeIcon(FileTypeEnum fileTypeEnum) {
		if (fileTypeEnum == null) {
			return null;
		}
		return getImageIcon("fileImage/" + fileTypeEnum.toString() + IMAGES_PATH_PATH_Suffix);
	}

	/**
	 * 
	 * 工具栏图片：
	 * 
	 * 通过图片名称获取图形对象
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param imageName
	 *            String 图片名称
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getToolBarImageIcon(String imageName) {
		return getImageIcon("toolbar/" + imageName + IMAGES_PATH_PATH_Suffix);
	}

	/**
	 * 
	 * 流程元素库：
	 * 
	 * 通过图片名称获取图形对象
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param imageName
	 *            String 图片名称
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getToolBoxImageIcon(String imageName) {
		return getImageIcon("box/" + imageName + IMAGES_PATH_PATH_Suffix);
	}

	/**
	 * 
	 * 画图面板：
	 * 
	 * 通过图片名称获取图形对象
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param imageName
	 *            String 图片名称
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getWorkflowImageIcon(String imageName) {
		return getImageIcon("workflow/" + imageName + IMAGES_PATH_PATH_Suffix);
	}

	/**
	 * 
	 * 通过图片名称获取图形对象
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param imageName
	 *            String 图片名称
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getImageIcon(String imageName) {
		if (DrawCommon.isNullOrEmtryTrim(imageName)) {
			log.error("ResourceManager类setResourceBundle方法：参数为null或“”");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		String pathImage = IMAGES_PATH_PREFIX + imageName;

		return getImageIconforPath(pathImage);
	}

	/**
	 * 
	 * 通过图片路径获取图形对象
	 * 
	 * 当给定的图片名称为null或“”时报错
	 * 
	 * 如果获取不到图片文件报错
	 * 
	 * @param path
	 *            String 路径
	 * @return ImageIcon 返回图片对象或报错
	 */
	public static ImageIcon getImageIconforPath(String path) {
		if (DrawCommon.isNullOrEmtryTrim(path)) {
			log.error("ResourceManager类setResourceBundle方法：参数为null或“”");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		try {
			return new ImageIcon(JecnResourceUtil.class.getResource(path));
		} catch (Exception ex) {
			// 正确情况下不应该执行到此分支
			log.error("ResourceManager类setResourceBundle方法：获取不到图片文件:" + path, ex);

			return null;
		}
	}

	/**
	 * 
	 * 获取给定的property文件内容
	 * 
	 * 返回为null时读取文件失败
	 * 
	 * @param propertyFileName
	 *            property文件名称
	 * @return Properties Properties对象或NULL
	 */
	public static Properties readProperties(String propertyFileName) {
		// 路径
		String path = getPropertiesPath(propertyFileName);
		return readPropertiesForFileName(path);
	}

	/**
	 * 
	 * 获取给定的property文件内容
	 * 
	 * 返回为null时读取文件失败
	 * 
	 * @param propertyFileName
	 *            property文件名称
	 * @return Properties Properties对象或NULL
	 */
	public static Properties readPropertiesForFileName(String propertyFileName) {
		log.info("ResourceManager类readProperties方法：读取开始，propertyFileName：" + propertyFileName);
		// 属性对象
		Properties properties = new Properties();
		FileInputStream inputStream = null;

		// 路径
		String path = null;
		try {
			// 获取指定文件全路径
			path = getSyncPath(propertyFileName);
			if (path == null) {
				return null;
			}
			// 读取文件
			inputStream = new FileInputStream(path);
			properties.load(inputStream);
			log.info("ResourceManager类readProperties方法：读取结束，文件路径：" + path);
		} catch (FileNotFoundException e) {
			log.error("ResourceManager类readProperties方法异常：指定文件找不到，文件路径：" + path, e);
			return null;
		} catch (IOException e) {
			log.error("ResourceManager类readProperties方法异常：读取文件出错，文件路径：" + path, e);
			return null;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("ResourceManager类readProperties方法异常：关闭FileInputStream流出错，文件路径：" + path, e);
					return null;
				}
			}
		}
		return properties;
	}

	/**
	 * 
	 * 往指定的Properties文件中写入内容
	 * 
	 * @param propertyFileName
	 *            String Properties文件路径
	 * @param properties
	 *            Properties 待写入的Properties对象
	 * @param comments
	 *            String 注释
	 * @return boolean true：写入成功 false：写入失败
	 */
	public static boolean writeProperties(String propertyFileName, Properties properties, String comments) {
		if (properties == null) {
			return false;
		}
		// 路径
		String path = getPropertiesPath(propertyFileName);

		// 获取指定文件全路径
		path = getSyncPath(path);
		if (path == null) {
			return false;
		}

		FileOutputStream outputStream = null;

		try {
			outputStream = new FileOutputStream(path);

			properties.store(outputStream, comments);

			log.info("JecnFileUtil类writeProperties方法：写入结束，文件路径：" + path);
			return true;
		} catch (FileNotFoundException e) {
			log.error("JecnFileUtil类writeProperties方法异常：指定文件找不到，文件路径：" + path, e);
			return false;
		} catch (IOException e) {
			log.error("JecnFileUtil类writeProperties方法异常：写入时文件出错，文件路径：" + path, e);
			return false;
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类writeProperties方法异常：关闭FileOutputStream流出错，文件路径：" + path, e);
					return false;
				}
			}
		}
	}

	/**
	 * 
	 * properties文件路径
	 * 
	 * @return String
	 */
	private static String getPropertiesPath(String propertyFileName) {
		return PROPERTY_FILE_PATH_PREFIX + propertyFileName;
	}

	/**
	 * 获取给定文件全路径
	 * 
	 * @return String 文件全路径 和NULL
	 */
	public static String getSyncPath(String fileName) {
		// 路径
		String path = null;

		// 打印文件路径
		log.info("JecnFileUtil类getSyncPath方法：参数文件名称fileName=" + fileName);
		if (DrawCommon.isNullOrEmtryTrim(fileName)) {
			log.info("JecnFileUtil类getSyncPath方法：文件名称为null或空。fileName=" + fileName);
			return path;
		}
		// 获取路径
		URL url = DrawCommon.class.getResource(fileName);
		if (url == null) {
			log.info("JecnFileUtil类getSyncPath方法：找不到指定的文件。fileName=" + fileName);
			return path;
		}

		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			log.error("JecnFileUtil类getSyncPath方法：path=" + path, e);
			path = url.getPath().replaceAll("%20", " ");
		}
		// 打印文件路径
		log.info("JecnFileUtil类getSyncPath方法：全路径path=" + path);
		return path;
	}

	/**
	 * 画图方法
	 * 
	 * @param fileLocation
	 *            文件存放路径
	 * @param image
	 *            图片数据
	 */
	public static boolean getCreateImage(String fileLocation, BufferedImage image) {
		boolean flag = true;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		JPEGImageEncoder encoder = null;
		try {
			// 判断路径是否为空
			if (fileLocation == null) {
				log.info("JecnFileUtil类getCreateImage方法路径为空！");
				flag = false;
			}
			// 获取输出流
			fos = new FileOutputStream(fileLocation);
			// 输出流的缓冲区
			bos = new BufferedOutputStream(fos);
			// 创建指定和输出流关联的JPEGImageEncoder对象
			encoder = JPEGCodec.createJPEGEncoder(bos);

			// 图片流缓冲区是否为空
			if (image == null) {
				log.info("JecnFileUtil类getCreateImage方法获取图片流缓冲区失败！");
				flag = false;
			}
			// 输出成图片
			encoder.encode(image);

		} catch (FileNotFoundException e) {
			log.error("JecnFileUtil类getCreateImage方法关错误！", e);
			JecnOptionPane.showMessageDialog(null, JecnUserInfo.getValue("saveFilePathError"));
			return false;
		} catch (Exception e) {
			log.error("JecnFileUtil类getCreateImage方法关错误！", e);
			JecnOptionPane.showMessageDialog(null, JecnUserInfo.getCreateFileError(), null, JOptionPane.ERROR_MESSAGE);
			return false;
		} finally {

			if (fos != null) { // 关闭流
				try {
					fos.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类getCreateImage方法关闭FileOutputStream流错误！", e);
				}
			}

			if (bos != null) { // 关闭流
				try {
					bos.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类getCreateImage方法关闭BufferedOutputStream流缓冲区错误！", e);
				}
			}
		}
		return flag;
	}

	/**
	 * 
	 * 把文件存储到本地(本地文件时在当前系统根目录下)
	 * 
	 * @param upLoadFile
	 *            待保存文件
	 * @param localPath
	 *            目的地路径
	 * @return String 成功:true，失败:false
	 */
	public static boolean writeFileToLocal(File upLoadFile, String localPath) {

		if (upLoadFile == null || localPath == null) {
			return false;
		}

		// 本地的输出流
		FileOutputStream outStream = null;
		// 远程excel输入流
		FileInputStream inputStream = null;
		try {

			// 获取指定文件全路径
			String path = getSyncPath(localPath);
			if (path == null) {
				String tmp = null;
				if (localPath.startsWith("/")) {
					tmp = localPath.substring(1);
				} else {
					tmp = localPath;
				}

				URL url = JecnFileUtil.class.getResource("");
				try {
					path = url.toURI().getPath();
				} catch (URISyntaxException e) {
					// 这儿一般是不进入此分支的
					log.error("JecnFileUtil类writeFileToLocal方法：path=" + path, e);
					path = url.getPath().replaceAll("%20", " ");
				}
				if (!DrawCommon.isNullOrEmtryTrim(path)) {// 后缀不为空
					path = path.substring(0, path.length() - "epros/draw/util/".length());
				}
				path = path + tmp;

				log.info("JecnFileUtil类writeFileToLocal方法：path=" + path);
			}

			File fileLocal = new File(path);
			// 判断文件是否存在
			if (!fileLocal.exists()) {
				fileLocal.createNewFile();
			}

			// 本地的输出流
			outStream = new FileOutputStream(fileLocal);

			// 远程excel输入流
			inputStream = new FileInputStream(upLoadFile);

			// 读取给定的文件数据存入到指定目录
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}

			return true;

		} catch (IOException ex) {
			log.error("JecnFileUtil类writeFileToLocal方法异常:", ex);
		} finally {
			if (outStream != null) {
				try {
					outStream.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类writeFileToLocal方法关闭FileOutputStream流错误！", e);
				}
			}

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类writeFileToLocal方法关闭FileInputStream流错误！", e);
				}
			}
		}

		return false;
	}

	/**
	 * 
	 * 把文件存储到本地：此方法针对一般文件写入
	 * 
	 * @param upLoadFile
	 *            待保存文件
	 * @param localPath
	 *            目的地路径
	 * @return String 成功:true，失败:false
	 */
	public static boolean writeFileToOPLocal(File upLoadFile, String localPath) {

		if (upLoadFile == null || localPath == null) {
			return false;
		}

		// 本地的输出流
		FileOutputStream outStream = null;
		// 远程excel输入流
		FileInputStream inputStream = null;
		try {

			File fileLocal = new File(localPath);
			// 判断文件是否存在
			if (!fileLocal.exists()) {
				fileLocal.createNewFile();
			} else {
				// 提示信息
				int isOk = JecnOptionPane.showConfirmDialog(JecnDrawMainPanel.getMainPanel(), JecnUserInfo
						.getExistFileInfo(), null, JecnOptionPane.YES_NO_OPTION);
				if (isOk == JecnOptionPane.NO_OPTION) {
					return true;
				}
			}

			// 本地的输出流
			outStream = new FileOutputStream(fileLocal);

			// 远程excel输入流
			inputStream = new FileInputStream(upLoadFile);

			// 读取给定的文件数据存入到指定目录
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inputStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, len);
			}

			return true;
		} catch (IOException ex) {
			log.error("JecnFileUtil类writeFileToOPLocal方法异常:", ex);
			JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnUserInfo.getSaveFileFail(), null,
					JecnOptionPane.ERROR_MESSAGE);
		} finally {
			if (outStream != null) {
				try {
					outStream.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类writeFileToOPLocal方法关闭FileOutputStream流错误！", e);
				}
			}

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类writeFileToOPLocal方法关闭FileInputStream流错误！", e);
				}
			}
		}

		return false;
	}

	/**
	 * 
	 * 把文件存储到本地：此方法针对一般文件写入
	 * 
	 * @param upLoadFile
	 *            待保存文件
	 * @param localPath
	 *            目的地路径
	 * @return String 成功:true，失败:false
	 */
	public static boolean writeLogoFileToOPLocal(File upLoadFile, String localPath) {

		if (upLoadFile == null || localPath == null) {
			return false;
		}

		// 本地的输出流
		FileOutputStream outStream = null;
		// 远程excel输入流
		FileInputStream inputStream = null;
		try {

			File fileLocal = new File(localPath);
			// 判断文件是否存在
			if (!fileLocal.exists()) {
				fileLocal.createNewFile();
			}

			// 本地的输出流
			outStream = new FileOutputStream(fileLocal);

			// 远程excel输入流
			inputStream = new FileInputStream(upLoadFile);

			// 读取给定的文件数据存入到指定目录
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inputStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, len);
			}

			return true;
		} catch (IOException ex) {
			log.error("JecnFileUtil类writeFileToOPLocal方法异常:", ex);
			JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnUserInfo.getSaveFileFail(), null,
					JecnOptionPane.ERROR_MESSAGE);
		} finally {
			if (outStream != null) {
				try {
					outStream.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类writeFileToOPLocal方法关闭FileOutputStream流错误！", e);
				}
			}

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("JecnFileUtil类writeFileToOPLocal方法关闭FileInputStream流错误！", e);
				}
			}
		}

		return false;
	}

	/**
	 * logo图片路径
	 * 
	 * @return
	 */
	public static String getLogoFilePath() {
		String filePath = PROPERTY_FILE_PATH_PREFIX + "operationConfig/logo.png";
		return getSyncPath(filePath);
	}

	/**
	 * 写入操作说明其他配置
	 * 
	 * @param
	 */
	public static void readCompName() {
		try {
			// 获取本地文件路径
			Properties properties = JecnFileUtil.readProperties("operationConfig/compName.properties");
			if (properties == null) {
				log.error("operationConfig/compName.properties获取文件中数据失败.properties为null");
				return;
			}
			// 公司名称
			String compNameValue = properties.getProperty("compName");
			String compName = (compNameValue == null) ? "" : compNameValue.trim();
			// 是否使用logo标识
			boolean whetherUse = ("true".equals(properties.getProperty("whetherUse"))) ? true : false;
			// 是否显示活动说明中输入输出项
			boolean showActiveIO = ("true".equals(properties.getProperty("showActiveIO"))) ? true : false;

			// 公司名称
			JecnSystemStaticData.setCompName(compName);
			// 是否使用logo标识 true:显示；false：隐藏
			JecnSystemStaticData.setShowLogoIcon(whetherUse);
			// 是否显示活动说明中输入输出项 true:显示；false：隐藏
			JecnSystemStaticData.setShowActiveIO(showActiveIO);
		} catch (Exception e) {
			log.error("operationConfig/compName.properties获取文件中数据失败", e);
		}
	}

	/**
	 * 写入操作说明其他配置
	 * 
	 * @param
	 */
	public static boolean writeCompName(String compName, boolean whetherUse, boolean showActiveIO) {
		// 获取本地文件路径
		Properties properties = new Properties();
		if (!DrawCommon.isNullOrEmtryTrim(compName)) {
			properties.setProperty("compName", compName.trim());
		}
		// 是否使用
		properties.setProperty("whetherUse", String.valueOf(whetherUse));
		// 是否显示活动说明中输入输出项 true:显示；false：隐藏
		properties.setProperty("showActiveIO", String.valueOf(showActiveIO));
		boolean ret = JecnFileUtil.writeProperties("operationConfig/compName.properties", properties, null);

		if (ret) {// 保存成功修改全局变量
			JecnSystemStaticData.setCompName(compName.trim());
			JecnSystemStaticData.setShowLogoIcon(whetherUse);
			JecnSystemStaticData.setShowActiveIO(showActiveIO);
		}
		return ret;
	}

	/**
	 * 打开本地文件
	 * 
	 * @param filePath
	 */
	public static void openFile(String filePath) {
		if (DrawCommon.isNullOrEmtryTrim(filePath)) {
			log.error("JecnFileUtil类中方法openFile : 文件不存在！");
			return;
		}
		File file = new File(filePath);
		if (file.exists()) {
			openLocalFile(file.getPath());
		}
	}

	/**
	 * @author yxw 2012-8-31
	 * @description:本地文件打开
	 * @param filePath
	 */
	public static void openLocalFile(String filePath) {
		ResultBean retBean = JecnCmd.openFile(filePath);
		if (retBean.getResultState() == -1) {
			JecnOptionPane.showMessageDialog(null, "打开文件异常！");
		}
	}

	public static ImageIcon getReplaceImageIcon(String imageName) {
		return getImageIcon("replace/" + imageName + IMAGES_PATH_PATH_Suffix);
	}
}
