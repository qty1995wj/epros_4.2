package com.jecn.epros.server.connector.Libang;

public class LibangTaskAppBean {
	/** 流程实例编号 */
	private String PROCINSTNO;
	/** 流程标题 */
	private String TOPIC;
	/** 申请人工号 */
	private String APPLYEMPNO;
	/** 申请人 */
	private String APPLYEMPNAME;
	/** 申请时间 格式:2017-11-11 */
	private String STARTDATE;
	/** 流程状态 格式：审批中、审批通过 */
	private String STATUS;
	/** 流程实例编号 */
	private String FORMURL;

	public String getPROCINSTNO() {
		return PROCINSTNO;
	}

	public void setPROCINSTNO(String pROCINSTNO) {
		PROCINSTNO = pROCINSTNO;
	}

	public String getTOPIC() {
		return TOPIC;
	}

	public void setTOPIC(String tOPIC) {
		TOPIC = tOPIC;
	}

	public String getAPPLYEMPNO() {
		return APPLYEMPNO;
	}

	public void setAPPLYEMPNO(String aPPLYEMPNO) {
		APPLYEMPNO = aPPLYEMPNO;
	}

	public String getAPPLYEMPNAME() {
		return APPLYEMPNAME;
	}

	public void setAPPLYEMPNAME(String aPPLYEMPNAME) {
		APPLYEMPNAME = aPPLYEMPNAME;
	}

	public String getSTARTDATE() {
		return STARTDATE;
	}

	public void setSTARTDATE(String sTARTDATE) {
		STARTDATE = sTARTDATE;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getFORMURL() {
		return FORMURL;
	}

	public void setFORMURL(String fORMURL) {
		FORMURL = fORMURL;
	}

}
