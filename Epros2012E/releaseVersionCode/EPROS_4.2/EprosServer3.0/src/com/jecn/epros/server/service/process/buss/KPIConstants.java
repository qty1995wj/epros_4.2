package com.jecn.epros.server.service.process.buss;

import com.jecn.epros.server.util.JecnUtil;

/**
 * 流程KPI曲线图 常量
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Jan 18, 2013 时间：4:10:41 PM
 */

public class KPIConstants {
	public final static int IMAGE_MIN_W = 600;
	public final static int BAR_WIDTH = 36;
	public final static int SCREEN_H_MIN = 768;
	public final static int SCREEN_H_MAX = 1024;
	public final static int IMAGE_H_MIN = 468;
	public final static int IMAGE_H_MAX = 690;
	public final static int IMAGE_H_OTHER = 588;
	public final static int PIE_MIN_WIDTH = 508;
	public final static int PIE_MIN_HEIGHT = 389;
	public final static int PIE_MAX_WIDTH = 720;
	public final static int PIE_MAX_HEIGHT = 670;
	public final static String IMAGE_PATH = "../img/";
	/** "KPI数值" */
	public final static String KPI_VALUE = JecnUtil.getValue("KPI_NUM");
	/** 左括号 */
	public final static String LEFT_ = "{";
	/** 右括号 */
	public final static String RIGHT_ = "}";
	/** "曲线图" */
	public final static String DIAGRAM_OF_CURVES = JecnUtil.getValue("Graph");
}
