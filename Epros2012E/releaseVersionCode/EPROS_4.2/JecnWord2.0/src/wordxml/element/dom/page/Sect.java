package wordxml.element.dom.page;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.GroupBean;
import wordxml.element.dom.IBody;
import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;

/**
 * 操作单节属性类
 * 
 * @author admin
 * 
 */
public class Sect extends IBody implements IBodyElement {
	/*** 节点属性对象 ******/
	private SectBean sectBean = new SectBean();
	/** 页眉 */
	private List<Header> hdrs = new ArrayList<Header>();
	/** 页脚 */
	private List<Footer> ftrs = new ArrayList<Footer>();
	/** 父 */
	private IBody parent;
	/** 是否最后一节 true 是 false 否 */
	private boolean isLast = false;

	public Sect(IBody parent) {
		this.parent = parent;
	}

	public Sect(IBody parent, SectBean sectBean) {
		this(parent);
		this.sectBean = sectBean;
	}

	public Header createHeader(HeaderOrFooterType type) {
		Header hdr = new Header(this, type);
		this.hdrs.add(hdr);
		return hdr;
	}

	public Footer createFooter(HeaderOrFooterType type) {
		Footer ftr = new Footer(this, type);
		this.ftrs.add(ftr);
		return ftr;
	}

	public Group createGroup() {
		Group group = new Group(this);
		this.bodyElements.add(group);
		return group;
	}

	public Group createGroup(GroupBean groupBean) {
		Group group = createGroup();
		group.setGroupBean(groupBean);
		return group;
	}

	public List<Header> getHeaders() {
		return Collections.unmodifiableList(hdrs);
	}

	public List<Footer> getFooters() {
		return Collections.unmodifiableList(ftrs);
	}

	public SectBean getSectBean() {
		return sectBean;
	}

	public void setSectBean(SectBean sectBean) {
		this.sectBean = sectBean;
	}

	public IBody getParent() {
		return parent;
	}

	public void setParent(IBody parent) {
		this.parent = parent;
	}

	public boolean isLast() {
		return isLast;
	}

	public void setLast(boolean isLast) {
		this.isLast = isLast;
	}

	public void breakPage() {
		this.addParagraph(new Paragraph(true));
	}

}
