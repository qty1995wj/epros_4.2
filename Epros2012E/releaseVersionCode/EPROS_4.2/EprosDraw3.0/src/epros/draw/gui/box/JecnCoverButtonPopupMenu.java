package epros.draw.gui.box;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;

import epros.draw.gui.box.JecnToolBoxMainPanel.ButtonShowType;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 流程元素面板/模型面板 切换按钮图标显示类型。
 * 
 * 按钮图片和内容布局： 图片左内容右;图片上内容下;显示详细信息
 * 
 * @author ZHOUXY
 * 
 */
public class JecnCoverButtonPopupMenu implements ItemListener {
	/** 资源文件管理对象 */
	private JecnResourceUtil resourceManager = null;
	/** 弹出菜单 */
	private JPopupMenu popupMenu = null;
	/** 图片、内容左右布局 */
	private JRadioButton leftRightCheckBox = null;
	/** 图片、内容上下布局 */
	private JRadioButton topBottonCheckBox = null;
	/** 图片、详细内容布局 */
	private JRadioButton titleTextCheckBox = null;
	/** 滚动面板 */
	private JecnToolBoxAbstractContainPanel containPanel = null;

	public JecnCoverButtonPopupMenu(JecnToolBoxAbstractContainPanel containPanel) {
		this.containPanel = containPanel;
		initComponent();
	}

	private void initComponent() {
		// 实例化资源文件管理对象
		resourceManager = JecnResourceUtil.getJecnResourceUtil();

		// 弹出菜单
		popupMenu = new JPopupMenu();
		// 图片、内容左右布局
		leftRightCheckBox = new JRadioButton(resourceManager.getValue("leftRight"));
		// 图片、内容上下布局
		topBottonCheckBox = new JRadioButton(resourceManager.getValue("topBotton"));
		// 图片、详细内容布局
		titleTextCheckBox = new JRadioButton(resourceManager.getValue("titleText"));

		ButtonGroup group = new ButtonGroup();

		// 添加到弹出菜单
		popupMenu.add(leftRightCheckBox);
		popupMenu.add(topBottonCheckBox);
		if (containPanel instanceof JecnToolBoxFlowElemContainPanel) {// 流程元素面板
			popupMenu.add(titleTextCheckBox);
		}

		// 添加到互斥组
		group.add(leftRightCheckBox);
		group.add(topBottonCheckBox);
		group.add(titleTextCheckBox);

		// 背景颜色
		popupMenu.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		leftRightCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		topBottonCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		titleTextCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 透明
		leftRightCheckBox.setOpaque(false);
		topBottonCheckBox.setOpaque(false);
		titleTextCheckBox.setOpaque(false);

		// 默认上下布局显示
		topBottonCheckBox.setSelected(true);

		// 事件
		leftRightCheckBox.addItemListener(this);
		topBottonCheckBox.addItemListener(this);
		titleTextCheckBox.addItemListener(this);
	}

	public JPopupMenu getPopupMenu() {
		return popupMenu;
	}

	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {// 选中

			if (e.getSource() == leftRightCheckBox) {// 图片、内容左右布局
				this.containPanel.setButtonShowType(ButtonShowType.leftRight);
			} else if (e.getSource() == topBottonCheckBox) {// 图片、内容上下布局
				this.containPanel.setButtonShowType(ButtonShowType.topBotton);
			} else if (e.getSource() == titleTextCheckBox) {// 图片、详细内容布局
				this.containPanel.setButtonShowType(ButtonShowType.titleText);
			} else {
				this.containPanel.setButtonShowType(ButtonShowType.topBotton);
			}

			if (containPanel instanceof JecnToolBoxFlowElemContainPanel) {// 流程元素面板
				// 切换流程元素面板按钮图标显示效果
				this.containPanel.toolBoxMainPanel.coverFlowElemButtonShowType();

				// 重新刷新布局
				JecnToolBoxAbstractScrollPane flowElemScrollPane = this.containPanel.toolBoxMainPanel
						.getCurrFlowElemScrollPane();

				// 改变大小重绘
				JecnScrollPaneComponentAdapter scrollPaneComponentAdapter = new JecnScrollPaneComponentAdapter();
				scrollPaneComponentAdapter.setJecnToolBoxAbstractScrollPaneSize(flowElemScrollPane);
				containPanel.revalidate();
				containPanel.repaint();
			} else if (containPanel instanceof JecnToolBoxModelContainPanel) {// 模型面板
				JecnTemplateScrollPane modelScrollPane = this.containPanel.toolBoxMainPanel.getCurrModelScrollPane();
				// 切换流程元素面板按钮图标显示效果
				this.containPanel.toolBoxMainPanel.coverTemplateButtonShowType();

				// 重新刷新布局
				JecnScrollPaneComponentAdapter scrollPaneComponentAdapter = new JecnScrollPaneComponentAdapter();
				scrollPaneComponentAdapter.setTemplateScrollPaneSize(modelScrollPane);
				containPanel.revalidate();
				containPanel.repaint();
			}

		}
	}
}
