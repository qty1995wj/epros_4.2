package com.jecn.epros.domain;

import com.jecn.epros.bean.MessageResult;

import java.util.List;

/**
 * 给页面返回的结果bean，只提示成功还是失败
 */
public class ViewLogsResult extends MessageResult {

    private List<String> logs = null;

    public ViewLogsResult(List<String> logs) {
        this.logs = logs;
    }

    public List<String> getLogs() {
        return logs;
    }

    public void setLogs(List<String> logs) {
        this.logs = logs;
    }
}
