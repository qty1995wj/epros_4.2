package com.jecn.epros.server.action.web.login.ad.zhongruitongxin;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 中睿通信单点登录配置信息
 * 
 */
public class ZhongRuiAfterItem {
	private static Logger log = Logger.getLogger(ZhongRuiAfterItem.class);
	public static String USER_LOGINNAME_KEY = null;
	public static String EPROS_LOGIN_URL;
	public static String TICKET_URL;
	public static String USERINFO_XML_URL;
	public static String EPROS_URL;

	public static void start() {
		// 读取配置文件
		InputStream fis = null;
		Properties properties = new Properties();
		try {
			String url = JecnPath.CLASS_PATH + "cfgFile/zhongrui/zhongRuiSSO.properties";
			log.info("配置文件路径:" + url);
			fis = new FileInputStream(url);
			properties.load(fis);

			EPROS_LOGIN_URL = properties.getProperty("EPROS_LOGIN_URL");
			TICKET_URL = properties.getProperty("TICKET_URL");
			USERINFO_XML_URL = properties.getProperty("USERINFO_XML_URL");
			USER_LOGINNAME_KEY = properties.getProperty("USER_LOGINNAME_KEY");
			EPROS_URL = properties.getProperty("EPROS_URL");
		} catch (Exception e) {
			log.error("解析中睿配置登录配置文件异常", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
	}
}
