package com.jecn.epros.mapper;

import com.jecn.epros.domain.home.JoinProcessRelatedFile;
import com.jecn.epros.domain.home.RoleRelatedActivity;
import com.jecn.epros.domain.home.RoleRelatedFlowInfo;
import com.jecn.epros.domain.home.TopNumberData;
import com.jecn.epros.sqlprovider.HomeSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

/**
 * 我的主页DAO接口
 *
 * @author admin
 */
public interface HomeMapper {
    /**
     * @param maps 流程中角色元素ID集合
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findRoleRelateFlowByPostIdsSql")
    public List<RoleRelatedFlowInfo> findRoleRelateFlowByPostIds(Map<String, Object> maps);


    /**
     * @param maps 流程中角色元素ID 条数
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findRoleRelateFlowByPostIdsSqlCount")
    public int findRoleRelateFlowByPostIdsCount(Map<String, Object> maps);

    /**
     * 角色相关活动
     *
     * @param maps
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findActiveByRoleListIdsSql")
    @ResultMap("mapper.Home.relatedActivity")
    public List<RoleRelatedActivity> findActiveByRoleListIds(Map<String, Object> maps);

    /**
     * 相关制度
     *
     * @param maps
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findFlowRelatedRulesSql")
    @ResultMap("mapper.Home.relatedFile")
    public List<JoinProcessRelatedFile> findFlowRelatedRules(Map<String, Object> maps);

    /**
     * 流程相关标准
     *
     * @param maps
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findFlowRelatedStandardsSql")
    @ResultMap("mapper.Home.relatedFile")
    public List<JoinProcessRelatedFile> findFlowRelatedStandards(Map<String, Object> maps);

    /**
     * 活动相关标准
     *
     * @param maps
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findActivityRelatedStandardsSql")
    @ResultMap("mapper.Home.relatedFile")
    public List<JoinProcessRelatedFile> findActivityRelatedStandards(Map<String, Object> maps);

    @SelectProvider(type = HomeSqlProvider.class, method = "findFlowRelatedRisksSql")
    @ResultMap("mapper.Home.relatedFile")
    public List<JoinProcessRelatedFile> findFlowRelatedRisks(Map<String, Object> maps);

    /**
     * 人员岗位名称
     *
     * @param peopleId
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findPostNameByPeopleIdSql")
    public List<Map<String, Object>> findPostNameByPeopleId(Long peopleId);

    /**
     * 流程访问Top10
     *
     * @param map
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findFlowVisitTopTen")
    @ResultMap("mapper.Home.topNumberData")
    public List<TopNumberData> findFlowVisitTopTen(Map<String, Object> map);

    /**
     * 流程合理化建议Top10
     *
     * @param map
     * @return
     */
    @SelectProvider(type = HomeSqlProvider.class, method = "findFlowProposeTopTen")
    @ResultMap("mapper.Home.topNumberData")
    public List<TopNumberData> findFlowProposeTopTen(Map<String, Object> map);
}
