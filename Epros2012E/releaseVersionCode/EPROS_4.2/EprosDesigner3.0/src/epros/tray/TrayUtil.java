package epros.tray;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.nio.channels.FileLock;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.SQLExec;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import epros.draw.util.JecnFileUtil;

public class TrayUtil {
	private static Logger log = Logger.getLogger(TrayUtil.class);
	public static Properties props = null;
	/** 数据库配置文件Bean */
	public static DataBaseBean dataBaseBean = null;

	/**
	 * 
	 * 此方法为了避免启动多次托盘，保证有且只有一个托盘运行
	 * 
	 */
	static void reStart() {
		File file = new File(JecnFileUtil.getSyncPath("/") + "start");
		try {
			if (!file.exists()) {
				file.createNewFile();// 如果"start"文件不存在,则创建此文件
			}
			FileLock lock = new FileOutputStream(file).getChannel().tryLock();// 生成一个输出流，用于判断托盘是否启动，不用关闭流
			if (lock == null) {
				System.exit(0);
			}
		} catch (Exception e) {
			log.error("", e);
			System.exit(0);
		}
	}

	/**
	 * 
	 * 获取前缀路径
	 * 
	 * @return String
	 */
	static String getRootPath() {
		return JecnFileUtil.getSyncPath("/");
	}

	/**
	 * @author yxw 2013-7-24
	 * @description:检查端口是否被占用
	 * @return true 端口可用，false 端口被占用
	 */
	public static boolean checkPort(int port) {
		ServerSocket s = null;
		try {
			s = new ServerSocket(port);
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					log.error("", e);
				}
			}
		}
	}

	/**
	 * @author yxw 2013-7-24
	 * @description:检查数据库连接
	 * @return true 数据库连接成功 false 数据库连接失败
	 */
	 static boolean checkDBConn(String url, String user, String password,
			String driverName) {
		Connection conn = null;
		try {
			Class.forName(driverName);
			conn = DriverManager.getConnection(url, user, password);
			return true;
		} catch (Exception e) {
			log.info("", e);
			return false;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					log.error("关闭流出错", e);
				}
			}
		}
	}

	/**
	 * 
	 * 判断是否能连接上数据库
	 * 
	 * @return boolean 连接成功返回true；连接失败返回false
	 */
	 static boolean checkDBConn() {
		Properties props = new Properties();
		// 读取配置文件输入流
		InputStream in = null;
		FileInputStream fileInput = null;
		try {

			// 获取配置文件database.properties
			fileInput = new FileInputStream(JecnServerUtil.getClassPath()
					+ JecnTrayConstants.DB_CONFIG_FILE);
			in = new BufferedInputStream(fileInput);
			props.load(in);
			String url = props.getProperty("jdbc.url");
			String userName = props.getProperty("jdbc.username");
			String password = props.getProperty("jdbc.password");
			String driverName = props.getProperty("jdbc.driverClassName");

			return checkDBConn(url, userName, password, driverName);
		} catch (Exception e) {
			log.info("mysql数据库连接异常");
			return false;
		} finally {
			if (fileInput != null) {
				try {
					fileInput.close();
				} catch (IOException e) {
					log.error("",e);
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error("",e);
				}
			}
		}

	}

	public static Connection getConnection() {

		Properties props = new Properties();
		// 读取配置文件输入流
		InputStream in = null;
		FileInputStream fileInput = null;
		try {

			// 获取配置文件database.properties
			fileInput = new FileInputStream(TrayUtil.getRootPath()
					+ JecnTrayConstants.DB_CONFIG_FILE);
			in = new BufferedInputStream(fileInput);
			props.load(in);
			String url = props.getProperty("jdbc.url");
			String userName = props.getProperty("jdbc.username");
			String password = props.getProperty("jdbc.password");
			String driverName = props.getProperty("jdbc.driverClassName");

			Connection conn = null;
			try {
				Class.forName(driverName);
				conn = DriverManager.getConnection(url, userName, password);
				return conn;
			} catch (Exception e) {
				log.error("", e);
			}

		} catch (Exception e) {
			log.error("",e);
		} finally {
			if (fileInput != null) {
				try {
					fileInput.close();
				} catch (IOException e) {
					log.error("",e);
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error("",e);
				}
			}
		}
		return null;

	}

	/**
	 * @author yxw 2013-8-6
	 * @description:导入数据
	 * @param url
	 * @param user
	 * @param password
	 * @param driverName
	 * @param sqlFile
	 */
	public static void importData(String url, String user, String password,
			String driverName, String sqlFile) {
		SQLExec sqlExec = new SQLExec();
		sqlExec.setDriver(driverName);
		sqlExec.setUrl(url);
		sqlExec.setUserid(user);
		sqlExec.setPassword(password);
		sqlExec.setSrc(new File(sqlFile));
		sqlExec.setPrint(true); // 设置是否输出
		// 输出到文件importOut.txt中；不设置该属性，默认输出到控制台
		sqlExec.setOutput(new File(JecnFileUtil.getSyncPath("/")
				+ "importOut.txt"));

		sqlExec.setProject(new Project()); // 要指定这个属性，不然会出错
		sqlExec.execute();
	}

	// /**
	// * @author yxw 2013-8-7
	// * @description:关闭EprosServer3.0进程
	// */
	// public static void killEprosServer() {
	// Process p = null;
	// try {
	// p = Runtime.getRuntime().exec(
	// "taskkill /f /t /im EprosServer3.0.exe");
	// } catch (IOException e) {
	// log.error("", e);
	// } finally {
	// TrayUtil.closeProcess(p);
	// }
	// }

	/**
	 * @author yxw 2013-8-7
	 * @description:检查tomcat端口是否被占用,如果被占用则自动修改
	 */
	public static boolean checkTomcatPort() {

		String path = TrayUtil.getRootPath()
				+ JecnTrayConstants.TOMCAT_CONFIG_FILE;

		// 写入xml对象
		XMLWriter output = null;
		FileWriter fileWriter = null;
		try {
			File file = new File(path);
			if (!file.exists()) {
				return false;
			}

			// 读取xml对象
			SAXReader saxReader = new SAXReader();
			// 读取xml数据
			Document doc = saxReader.read(file);

			// 获取Connector节点
			List listNode = doc.selectNodes("//Connector");

			boolean flag = false;
			for (Object connector : listNode) {
				Element ele = (Element) connector;
				//
				Attribute portObj = ele.attribute("port");
				String port = portObj.getValue();// 得到端口
				if (portObj != null) {
					int p = Integer.valueOf(port).intValue();
					if (!checkPort(p)) {// 配置文件中当前端口被专用
						p++;
						while (!checkPort(p)) {
							p++;
						}
						portObj.setValue(String.valueOf(p));
						flag = true;
					}
				}
			}

			listNode = doc.selectNodes("//Server");
			for (Object connector : listNode) {
				Element ele = (Element) connector;
				//
				Attribute portObj = ele.attribute("port");
				String port = portObj.getValue();// 得到端口
				if (portObj != null) {

					int p = Integer.valueOf(port).intValue();
					if (!checkPort(p)) {// 配置文件中当前端口被专用
						p++;
						while (!checkPort(p)) {
							p++;
						}
						portObj.setValue(String.valueOf(p));
						flag = true;
					}
				}
			}

			if (flag) {// 重新给定端口，写入配置文件
				fileWriter = new FileWriter(file);
				output = new XMLWriter(fileWriter);
				output.write(doc);
			}

			return true;
		} catch (Exception e) {
			log.info("", e);
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					log.info("输出流关闭异常", e);
				}
			}
			if (fileWriter != null) {
				try {
					fileWriter.close();
				} catch (IOException e) {
					log.info("输出流关闭异常", e);
				}
			}

		}
		return false;
	}

	/***************************************************************************
	 * 读数据库配置文件 将配置文件中的数据存入Bean
	 * 
	 * @return
	 */
	public static boolean loadDataBaseInfo(String dbPath) {
		if (props != null) {
			return true;
		}
		props = new Properties();
		dataBaseBean = new DataBaseBean();
		// 读取配置文件输入流
		InputStream in = null;
		FileInputStream fileInput = null;
		try {

			// 获取配置文件database.properties
			fileInput = new FileInputStream(dbPath);
			in = new BufferedInputStream(fileInput);
			props.load(in);
			Enumeration en = props.propertyNames();

			while (en.hasMoreElements()) {
				String key = (String) en.nextElement();
				if ("rmi.port".equals(key)) {// 设计器端口号
					dataBaseBean.setDataDesinPort(props.getProperty(key));
				}
				// if ("jdbc.username".equals(key)) {// 用户名
				// dataBaseBean.setUsname(props.getProperty(key));
				// } else if ("jdbc.password".equals(key)) {// 密码
				// dataBaseBean.setUspwd(props.getProperty(key));
				// } else if ("hbm.path".equals(key)) {// 数据库类型
				// dataBaseBean.setHbmPath(props.getProperty(key));
				// } else if ("jdbc.url".equals(key)) {// 数据库地址
				// dataBaseBean.setDataUrl(props.getProperty(key));
				// } else if ("hibernate.dialect".equals(key)) { // 方言
				// dataBaseBean.setDataDialect(props.getProperty(key));
				// dataBaseBean.readPropertiesSetDBTypeEnum(dataBaseBean
				// .getDataDialect());
				// } else if ("jdbc.driverClassName".equals(key)) {// driver驱动
				// dataBaseBean.setDataDriver(props.getProperty(key));
				// } else if ("rmi.ip".equals(key)) {
				// dataBaseBean.setDataHostIP(props.getProperty(key));
				// } else if ("rmi.port".equals(key)) {// 设计器端口号
				// dataBaseBean.setDataDesinPort(props.getProperty(key));
				// }
			}

		} catch (FileNotFoundException ex) {
			log.error("DataBaseBuss类loadDataBaseInfo方法：文件找不到。文件路径=" + dbPath,
					ex);
			return false;
		} catch (IOException ex) {
			log.error("DataBaseBuss类loadDataBaseInfo方法：读取数据库配置文件异常。文件路径="
					+ dbPath, ex);
			return false;
		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error(
							"DataBaseBuss类loadDataBaseInfo方法：关闭InputStream对象异常。 文件路径="
									+ dbPath, e);
					return false;
				}
			}

			if (fileInput != null) {
				try {
					fileInput.close();
				} catch (IOException e) {
					log.error(
							"DataBaseBuss类loadDataBaseInfo方法：关闭FileInputStream对象异常。 文件路径="
									+ dbPath, e);
					return false;
				}
			}

		}
		return true;
	}

	public static void closeProcess(Process p) {
		if (p != null) {
			try {
				if (p.getInputStream() != null) {
					p.getInputStream().close();
				}
			} catch (Exception e) {
				log.error("", e);
			}
			try {
				if (p.getErrorStream() != null) {
					p.getErrorStream().close();
				}
				p.getOutputStream().close();
			} catch (Exception e) {
				log.error("", e);
			}
			try {
				if (p.getOutputStream() != null) {
					p.getOutputStream().close();
				}
			} catch (Exception e) {
				log.error("", e);
			}
		}
	}
}
