package epros.draw.gui.workflow.aid;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import epros.draw.error.ErrorInfoConstant;
import epros.draw.event.flowElement.JecnEditPortEventProcess;
import epros.draw.event.flowElement.JecnFlowElementEventProcess;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 
 * 鼠标点击画图面板拖动时所圈选框
 * 
 * @author ZHOUXY
 */
public class JecnSelectAreaPanel extends JPanel {
	/** 画图面板 */
	private JecnDrawDesktopPane workflow = null;

	/** 拖动图形生成虚拟图形边框 */
	private Rectangle rectangle = null;
	/** 选中框的位置点 */
	private Point startPoint = null;
	/** 选中框结束点 */
	private Point endPoint = null;

	public JecnSelectAreaPanel(JecnDrawDesktopPane workflow) {
		if (workflow == null) {
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.workflow = workflow;
		initComponents();
	}

	private void initComponents() {
		// 透明
		this.setOpaque(false);
		// 边框
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		// 默认不显示
		this.setVisible(false);

		workflow.add(this);
		workflow.setLayer(this, 10000);
	}

	public Point getStartPoint() {
		return startPoint;
	}

	/**
	 * 
	 * 设置开始点，设置位置点
	 * 
	 * @param startPoint
	 */
	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
		if (startPoint != null) {
			// 位置点大小大小
			this.setBounds(startPoint.x, startPoint.y, 0, 0);
			// 显示
			if (!this.isVisible()) {
				this.setVisible(true);
			}
		}
	}

	/**
	 * 
	 * 设置结束点,且设置大小
	 * 
	 * @param endPoint
	 */
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
		if (startPoint == null || endPoint == null) {
			return;
		}
		if ((startPoint.x != endPoint.x) && (startPoint.y != endPoint.y)) {
			this.setSize(Math.abs(endPoint.x - startPoint.x), Math
					.abs(endPoint.y - startPoint.y));
			if (startPoint.x < endPoint.x && startPoint.y < endPoint.y) {
				this.setLocation(startPoint.x, startPoint.y);
			}
			if (startPoint.x < endPoint.x && startPoint.y > endPoint.y) {
				this.setLocation(startPoint.x, endPoint.y);
			}
			if (startPoint.x > endPoint.x && startPoint.y < endPoint.y) {
				this.setLocation(endPoint.x, startPoint.y);
			}
			if (startPoint.x > endPoint.x && startPoint.y > endPoint.y) {
				this.setLocation(endPoint.x, endPoint.y);
			}
			
			JecnEditPortEventProcess.draggedScrollPanel(endPoint);
			// 拖动设置结束点
			JecnFlowElementEventProcess.getReSetDrawPanelSize(endPoint, this
					.getWidth(), this.getHeight());
		}
	}

	/**
	 * panel 的结束端点
	 * 
	 * @return
	 */
	public Point getEndPoint() {
		return endPoint;
	}

	/**
	 * 
	 * 隐藏选中框
	 * 
	 */
	public void setVisibleFalse() {
		// 隐藏
		if (this.isVisible()) {
			this.setVisible(false);
		}
	}

	/**
	 * 获取图形边框
	 * 
	 * @return
	 */

	public Rectangle getRectangle() {
		if (rectangle == null) {
			rectangle = this.getBounds();
		}
		return rectangle;
	}

	/**
	 * 根据圈选框边框重设圈选框开始点和结束点
	 * 
	 */
	public void reSetAreaLanelPoint() {
		if (this.getStartPoint() == null || this.getEndPoint() == null) {
			return;
		}
		// 圈选框水平移动的位移
		int diffX = this.getStartPoint().x
				- this.getRectangle().getLocation().x;
		// 圈选框垂直移动的位移
		int diffY = this.getStartPoint().y
				- this.getRectangle().getLocation().y;
		// 重设圈选框开始坐标和结束坐标
		this.setStartPoint(this.getRectangle().getLocation());
		this.setEndPoint(new Point(this.getEndPoint().x - diffX, this
				.getEndPoint().y
				- diffY));
	}

	public void setRectangle(Rectangle rectangle) {
		this.rectangle = rectangle;
	}

}
