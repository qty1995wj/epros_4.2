package com.jecn.webservice.jbshihua.entity.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;
import com.jecn.webservice.jbshihua.entity.PostInfo;
import com.jecn.webservice.jbshihua.entity.UserViewDao;

public class UserViewDaoImpl extends AbsBaseDao<String, String> implements UserViewDao {

	@Override
	public int deleteUser(String code) throws Exception {
		String sql = "DELETE jecn_user_pos_info WHERE user_num = ? ";
		return this.execteNative(sql, code);
	}

	@Override
	public int insertUser(JecnUserPosView user) throws Exception {
		String sql = "INSERT INTO jecn_user_pos_info(user_num,login_name,true_name,pos_num,pos_name,org_num,email,email_type,phone_str,status)"
				+ "VALUES (?,?,?,?,?,?,?,?,?,?) ";
		return this.execteNative(sql, user.getUserNum(), user.getLoginName(), user.getTrueName(), user.getPosNum(),
				user.getPosName(), user.getOrgNum(), user.getEmail(), user.getEmailType(), user.getPhoneStr(), user
						.getStatus());
	}

	@Override
	public int updateUser(JecnUserPosView user) throws Exception {
		String sql = "UPDATE jecn_user_pos_info SET login_name = ?,true_name = ?,org_num = ?,email = ?,email_type = ?,phone_str = ?,status = ?,pos_num = ?,pos_name = ?"
				+ " WHERE user_num = ?";
		return this.execteNative(sql, user.getLoginName(), user.getTrueName(), user.getOrgNum(), user.getEmail(), user
				.getEmailType(), user.getPhoneStr(), user.getStatus(), user.getPosNum(), user.getPosName(), user
				.getUserNum());
	}

	@Override
	public int updateUserAndPost(JecnUserPosView user) throws Exception {
		String sql = "UPDATE jecn_user_pos_info SET login_name = ?,true_name = ?,pos_num = ?,pos_name = ?,org_num = ?,email = ?,email_type = ?,phone_str = ?,status = ?"
				+ " WHERE user_num = ?";
		return this.execteNative(sql, user.getLoginName(), user.getTrueName(), user.getPosNum(), user.getPosName(),
				user.getOrgNum(), user.getEmail(), user.getEmailType(), user.getPhoneStr(), user.getStatus(), user
						.getUserNum());
	}

	@Override
	public int updateUserJob(JecnUserPosView user) throws Exception {
		// TODO Auto-generated method stub
		String sql = "UPDATE jecn_user_pos_info SET pos_num = ?,pos_name = ?,status = ?,org_num = ? "
				+ " WHERE user_num = ?";
		return this.execteNative(sql, user.getPosNum(), user.getPosName(), user.getStatus(), user.getOrgNum(), user
				.getUserNum());
	}

	@Override
	public int updateUserOldJob(JecnUserPosView user) throws Exception {
		// TODO Auto-generated method stub
		String sql = "UPDATE jecn_user_pos_info SET pos_num = ?,pos_name = ?,status = ?,org_num = ? "
				+ " WHERE user_num = ? and pos_num = ?";
		return this.execteNative(sql, user.getPosNum(), user.getPosName(), user.getStatus(), user.getOrgNum(), user
				.getUserNum(), user.getPosNum());
	}

	@Override
	public List<JecnUserPosView> getUser(String code) throws Exception {
		// TODO Auto-generated method stub
		String hql = "from JecnUserPosView where userNum = ?";
		Session session = this.getSession();
		Query query = session.createQuery(hql).setParameter(0, code);
		return query.list();
	}

	@Override
	public int getUserCounts(String code) throws Exception {
		// TODO Auto-generated method stub
		String sql = "select count(*) from jecn_user_pos_info where user_num = ?";
		return this.countAllByParamsNativeSql(sql, code);
	}

	public JecnUserPosView getUserJob(String code, String jobNum, String status) throws Exception {
		// TODO Auto-generated method stub
		String hql = "from JecnUserPosView where userNum = ? and posNum = ? and status = ?";
		return this.getObjectHql(hql, code, jobNum, status);
	}

	public JecnUserPosView getUserJob(String code, String jobNum) throws Exception {
		// TODO Auto-generated method stub
		String hql = "from JecnUserPosView where userNum = ? and posNum = ?";
		return this.getObjectHql(hql, code, jobNum);
	}

	@Override
	public int updatePost(PostInfo postInfo) throws Exception {
		String sql = "UPDATE JECN_POST_INFO SET POST_NUM = ?,POST_NAME = ?,DEPT_NUM = ?,USER_CODE = ?,STATUS = ?"
				+ " WHERE UUID = ?";
		return this.execteNative(sql, postInfo.getPostNum(), postInfo.getPosName(), postInfo.getDeptNum(), postInfo
				.getUserCode(), postInfo.getStatus(), postInfo.getUuid());
	}

	@Override
	public int insertPost(PostInfo postInfo) throws Exception {
		String sql = "INSERT INTO JECN_POST_INFO(POST_NUM,POST_NAME,DEPT_NUM,USER_CODE,STATUS,UUID)"
				+ "VALUES (?,?,?,?,?,?) ";
		return this.execteNative(sql, postInfo.getPostNum(), postInfo.getPosName(), postInfo.getDeptNum(), postInfo
				.getUserCode(), postInfo.getStatus(), postInfo.getUuid());
	}

}
