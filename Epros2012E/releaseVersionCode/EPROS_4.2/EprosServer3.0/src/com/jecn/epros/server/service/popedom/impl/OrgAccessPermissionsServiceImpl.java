package com.jecn.epros.server.service.popedom.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.log.JecnPersonalPermRecord;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnFlowFilePosPerm;
import com.jecn.epros.server.bean.popedom.JecnOrgAccessPermissionsT;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPosGroupnAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.service.popedom.IOrgAccessPermissionsService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * @author yxw 2012-6-12
 * @description：
 */
@Transactional(rollbackFor = Exception.class)
public class OrgAccessPermissionsServiceImpl extends AbsBaseService<JecnOrgAccessPermissionsT, Long> implements
		IOrgAccessPermissionsService {
	private Logger log = Logger.getLogger(OrgAccessPermissionsServiceImpl.class);
	private IOrgAccessPermissionsDao orgAccessPermissionsDao;
	private IPositionAccessPermissionsDao positionAccessPermissionsDao;
	private IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao;

	public IPositionAccessPermissionsDao getPositionAccessPermissionsDao() {
		return positionAccessPermissionsDao;
	}

	public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
		this.positionAccessPermissionsDao = positionAccessPermissionsDao;
	}

	public IOrgAccessPermissionsDao getOrgAccessPermissionsDao() {
		return orgAccessPermissionsDao;
	}

	public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
		this.orgAccessPermissionsDao = orgAccessPermissionsDao;
	}

	public IPosGroupnAccessPermissionsDao getPosGroupAccessPermissionsDao() {
		return posGroupAccessPermissionsDao;
	}

	public void setPosGroupAccessPermissionsDao(IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao) {
		this.posGroupAccessPermissionsDao = posGroupAccessPermissionsDao;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type) throws Exception {
		try {
			return orgAccessPermissionsDao.getOrgAccessPermissions(relateId, type, false);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public LookPopedomBean getAccessPermissions(Long relateId, int type) throws Exception {
		return getAccessPermissions(relateId, type, false);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public LookPopedomBean getAccessPermissions(Long relateId, int type, boolean pub) throws Exception {
		try {
			LookPopedomBean lookPopedom = new LookPopedomBean();
			// 部门查阅权限
			List<JecnTreeBean> orgList = orgAccessPermissionsDao.getOrgAccessPermissions(relateId, type, pub);
			// 岗位查阅权限
			List<JecnTreeBean> posList = positionAccessPermissionsDao.getPositionAccessPermissions(relateId, type, pub);
			// 岗位组查阅权限
			List<JecnTreeBean> posGroupList = posGroupAccessPermissionsDao.getPosGroupAccessPermissions(relateId, type,
					pub);
			String hql = null;
			if (type == 0) {
				hql = "select isPublic,confidentialityLevel from JecnFlowStructureT where flowId=?";
			} else if (type == 1) {
				hql = "select isPublic,confidentialityLevel from JecnFileBeanT where fileID=?";
			} else if (type == 2) {
				hql = "select isPublic,confidentialityLevel from StandardBean where criterionClassId=?";
			} else if (type == 3) {
				hql = "select isPublic,confidentialityLevel from RuleT where id=?";
			}
			Object[] obj = orgAccessPermissionsDao.getObjectHql(hql, relateId);
			if (obj != null) {
				if (obj[0] != null) {
					lookPopedom.setIsPublic(Long.valueOf(obj[0].toString()));
				}
				if (obj[1] != null) {
					lookPopedom.setConfidentialityLevel(Integer.valueOf(obj[1].toString()));
				}
			}
			lookPopedom.setOrgList(orgList);
			lookPopedom.setPosList(posList);
			lookPopedom.setPosGroupList(posGroupList);
			return lookPopedom;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public LookPopedomBean getOnlyAccessPermissions(Long relateId, int type) throws Exception {
		try {
			LookPopedomBean lookPopedom = new LookPopedomBean();
			// 部门查阅权限
			List<JecnTreeBean> orgList = orgAccessPermissionsDao.getOrgAccessPermissions(relateId, type, false);
			// 岗位查阅权限
			List<JecnTreeBean> posList = positionAccessPermissionsDao.getPositionAccessPermissions(relateId, type,
					false);
			// 岗位组查阅权限
			List<JecnTreeBean> posGroupList = posGroupAccessPermissionsDao.getPosGroupAccessPermissions(relateId, type,
					false);

			lookPopedom.setOrgList(orgList);
			lookPopedom.setPosList(posList);
			lookPopedom.setPosGroupList(posGroupList);
			return lookPopedom;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 
	 * @author yxw 2012-10-25
	 * @description: 查阅权限 是否公开修改
	 * @param relateId
	 * @param type
	 *            0是流程，1是文件，2是标准，3是制度
	 * @param isPublic
	 * @param orgIds
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param posIds
	 * @param posType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0：管理员
	 * @param projectId
	 * @param saveMode
	 *            0覆盖 1批量添加 2批量删除
	 * @throws Exception
	 */
	@Override
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, int orgType, int posType, int userType, Long projectId, int posGroupType, int saveMode,
			AccessId accId, Long userId) throws Exception {

		// 设置查阅权限
		if (type == 2) {
			userType = 0;
		}
		if (saveMode == 0) {
			defaultSaveOrUpdateAccessPermissions(relateId, type, nodeType, isPublic, securityType, orgType, posType,
					userType, projectId, posGroupType, accId);
		} else if (saveMode == 1) {
			addAccessPermissions(relateId, type, nodeType, isPublic, securityType, orgType, posType, userType,
					projectId, posGroupType, accId);
		} else if (saveMode == 2) {
			delAccessPermissions(relateId, type, nodeType, isPublic, securityType, orgType, posType, userType,
					projectId, posGroupType, accId);
		}
		updatePersonalPermRecord(relateId, type, userId, saveMode, securityType, orgType, posType, posGroupType);
		// 更新主节点秘密和公开
		updateMainNodePerm(relateId, type, nodeType, isPublic, securityType, userType, projectId);
	}

	private void updatePersonalPermRecord(Long rId, int rType, Long userId, int saveMode, int secret, int org, int pos,
			int posGroup) {
		String hql = "from JecnPersonalPermRecord where rId=? and rType=? and userId=?";
		JecnPersonalPermRecord record = this.posGroupAccessPermissionsDao.getObjectHql(hql, rId, rType, userId);
		if (record == null) {
			record = new JecnPersonalPermRecord();
		}
		record.setHandle(saveMode);
		record.setOrg(org);
		record.setPos(pos);
		record.setPosGroup(posGroup);
		record.setSecret(secret);
		record.setrId(rId);
		record.setrType(rType);
		record.setUserId(userId);
		this.posGroupAccessPermissionsDao.getSession().saveOrUpdate(record);
	}

	/**
	 * 
	 * @author yxw 2012-10-25
	 * @description: 查阅权限 是否公开修改
	 * @param relateId
	 * @param type
	 *            0是流程，1是文件，2是标准，3是制度
	 * @param isPublic
	 * @param orgIds
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param posIds
	 * @param posType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0：管理员
	 * @param projectId
	 * @param saveMode
	 *            0覆盖 1批量添加 2批量删除
	 * @throws Exception
	 */
	@Override
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, String orgIds, int orgType, String posIds, int posType, int userType, Long projectId,
			String posGroupIds, int posGroupType, int saveMode) throws Exception {

		// 设置查阅权限
		if (type == 2) {
			userType = 0;
		}
		if (saveMode == 0) {
			defaultSaveOrUpdateAccessPermissions(relateId, type, nodeType, isPublic, securityType, orgIds, orgType,
					posIds, posType, userType, projectId, posGroupIds, posGroupType);
		} else if (saveMode == 1) {
			addAccessPermissions(relateId, type, nodeType, isPublic, securityType, orgIds, orgType, posIds, posType,
					userType, projectId, posGroupIds, posGroupType);
		} else if (saveMode == 2) {
			delAccessPermissions(relateId, type, nodeType, isPublic, securityType, orgIds, orgType, posIds, posType,
					userType, projectId, posGroupIds, posGroupType);
		}

		// 更新主节点秘密和公开
		updateMainNodePerm(relateId, type, nodeType, isPublic, securityType, userType, projectId);
	}

	/**
	 * 更新主节点(也就是给设置的那个节点和子节点)秘密和公开
	 * 
	 * @param relateId
	 * @param type
	 * @param nodeType
	 * @param isPublic
	 * @param securityType
	 * @param userType
	 * @param projectId
	 * @throws Exception
	 */
	private void updateMainNodePerm(Long relateId, int type, TreeNodeType nodeType, long isPublic, int securityType,
			int userType, Long projectId) throws Exception {
		String hql = null;
		if (securityType == 0) {
			switch (type) {
			case 0:
				hql = "update JecnFlowStructureT set isPublic=? where flowId=?";
				orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				if (userType == 0) {
					hql = "update JecnFlowStructure set isPublic=? where flowId=?";
					orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				}
				break;
			case 1:

				hql = "update JecnFileBeanT set isPublic=? where  fileID=?";
				orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				if (userType == 0) {
					hql = "update JecnFileBean set isPublic=? where fileID=?";
					orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				}

				if (nodeType == TreeNodeType.fileDir) {
					hql = "update JecnFileBeanT set isPublic=? where isDir=1 and perFileId=?";
					orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
					if (userType == 0) {
						hql = "update JecnFileBean set isPublic=? where isDir=1 and perFileId=?";
						orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
					}
				}
				break;
			case 2:
				hql = "update StandardBean set isPublic=? where criterionClassId=?";
				orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				if (nodeType == TreeNodeType.standardDir) {
					hql = "update StandardBean set isPublic=? where preCriterionClassId=? and stanType=1";
					orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				}
				break;
			case 3:
				hql = "update RuleT set isPublic=? where id=?";
				orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				if (userType == 0) {
					hql = "update Rule set isPublic=? where id=?";
					orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
				}
				if (nodeType == TreeNodeType.ruleDir) {
					hql = "update RuleT set isPublic=? where (isDir=1 or isDir=2) and perId=?";
					orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
					if (userType == 0) {
						hql = "update Rule set isPublic=? where (isDir=1 or isDir=2) and perId=?";
						orgAccessPermissionsDao.execteHql(hql, isPublic, relateId);
					}
				}
				break;
			}
		} else if (securityType == 1) {
			List<Long> listIds = JecnUtil.getNeedSetAuthNodesContainChildNode(relateId, type, projectId,
					orgAccessPermissionsDao);
			List<String> listStr = new ArrayList<String>();
			String sql = "";
			switch (type) {
			case 0:
				sql = "update jecn_flow_structure_t set is_public=? where flow_Id in";
				listStr = JecnCommonSql.getListSqls(sql, listIds);
				for (String str : listStr) {
					orgAccessPermissionsDao.execteNative(str, isPublic);
				}
				if (userType == 0) {
					sql = "update jecn_flow_structure set is_public=? where flow_Id in";
					listStr = JecnCommonSql.getListSqls(sql, listIds);
					for (String str : listStr) {
						orgAccessPermissionsDao.execteNative(str, isPublic);
					}
				}
				break;

			case 1:
				sql = "update jecn_file_t set is_public=? where file_id in";
				listStr = JecnCommonSql.getListSqls(sql, listIds);
				for (String str : listStr) {
					orgAccessPermissionsDao.execteNative(str, isPublic);
				}
				if (userType == 0) {
					sql = "update jecn_file set is_public=? where file_id in";
					listStr = JecnCommonSql.getListSqls(sql, listIds);
					for (String str : listStr) {
						orgAccessPermissionsDao.execteNative(str, isPublic);
					}
				}
				break;
			case 2:
				sql = "update JECN_CRITERION_CLASSES set is_public=? where criterion_class_id in";
				listStr = JecnCommonSql.getListSqls(sql, listIds);
				for (String str : listStr) {
					orgAccessPermissionsDao.execteNative(str, isPublic);
				}
				break;
			case 3:
				sql = "update jecn_rule_t set is_public=? where id in";
				listStr = JecnCommonSql.getListSqls(sql, listIds);
				for (String str : listStr) {
					orgAccessPermissionsDao.execteNative(str, isPublic);
				}
				if (userType == 0) {
					sql = "update jecn_rule set is_public=? where id in";
					listStr = JecnCommonSql.getListSqls(sql, listIds);
					for (String str : listStr) {
						orgAccessPermissionsDao.execteNative(str, isPublic);
					}
				}
				break;
			}
		}
	}

	private void delAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic, int securityType,
			String orgIds, int orgType, String posIds, int posType, int userType, Long projectId, String posGroupIds,
			int posGroupType) throws Exception {

		orgAccessPermissionsDao.del(relateId, type, orgIds, orgType, userType, projectId, nodeType);
		posGroupAccessPermissionsDao.del(relateId, type, posGroupIds, posGroupType, userType, projectId, nodeType);
		positionAccessPermissionsDao.del(relateId, type, posIds, posType, userType, projectId, nodeType);

	}

	private void delAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic, int securityType,
			int orgType, int posType, int userType, Long projectId, int posGroupType, AccessId accId) throws Exception {
		orgAccessPermissionsDao.del(relateId, type, accId.getOrgAccessId(), orgType, userType, projectId, nodeType);
		posGroupAccessPermissionsDao.del(relateId, type, accId.getPosGroupAccessId(), posGroupType, userType,
				projectId, nodeType);
		positionAccessPermissionsDao
				.del(relateId, type, accId.getPosAccessId(), posType, userType, projectId, nodeType);

	}

	private void addAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic, int securityType,
			String orgIds, int orgType, String posIds, int posType, int userType, Long projectId, String posGroupIds,
			int posGroupType) throws Exception {

		orgAccessPermissionsDao.add(relateId, type, orgIds, orgType, userType, projectId, nodeType);
		positionAccessPermissionsDao.add(relateId, type, posIds, posType, userType, projectId, nodeType);
		posGroupAccessPermissionsDao.add(relateId, type, posGroupIds, posGroupType, userType, projectId, nodeType);

	}

	private void addAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic, int securityType,
			int orgType, int posType, int userType, Long projectId, int posGroupType, AccessId accId) throws Exception {

		orgAccessPermissionsDao.add(relateId, type, accId.getOrgAccessId(), orgType, userType, projectId, nodeType);
		positionAccessPermissionsDao
				.add(relateId, type, accId.getPosAccessId(), posType, userType, projectId, nodeType);
		posGroupAccessPermissionsDao.add(relateId, type, accId.getPosGroupAccessId(), posGroupType, userType,
				projectId, nodeType);

	}

	private void defaultSaveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, String orgIds, int orgType, String posIds, int posType, int userType, Long projectId,
			String posGroupIds, int posGroupType) throws Exception {

		orgAccessPermissionsDao.savaOrUpdate(relateId, type, orgIds, orgType, userType, projectId, nodeType);
		positionAccessPermissionsDao.savaOrUpdate(relateId, type, posIds, posType, userType, projectId, nodeType);
		posGroupAccessPermissionsDao.savaOrUpdate(relateId, type, posGroupIds, posGroupType, userType, projectId,
				nodeType);
	}

	private void defaultSaveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, int orgType, int posType, int userType, Long projectId, int posGroupType, AccessId accId)
			throws Exception {

		orgAccessPermissionsDao.savaOrUpdate(relateId, type, accId.getOrgAccessId(), orgType, userType, projectId,
				nodeType);
		positionAccessPermissionsDao.savaOrUpdate(relateId, type, accId.getPosAccessId(), posType, userType, projectId,
				nodeType);
		posGroupAccessPermissionsDao.savaOrUpdate(relateId, type, accId.getPosGroupAccessId(), posGroupType, userType,
				projectId, nodeType);
	}

	@Override
	public boolean peopleHasFlowFileDownloadPerm(Set<Long> flowSet, Long userId) throws Exception {

		if (flowSet.size() == 0) {
			return false;
		}
		// 获得该人员所在的岗位
		String hql = "from JecnUserInfo where peopleId=?";
		List<JecnUserInfo> infos = orgAccessPermissionsDao.listHql(hql, userId);
		List<Long> userPosList = new ArrayList<Long>();
		for (JecnUserInfo info : infos) {
			userPosList.add(info.getFigureId());
		}

		// 流程文件的岗位下载权限的集合
		List<JecnFlowFilePosPerm> perms = orgAccessPermissionsDao.findFlowFileDownloadPerms(flowSet);

		if (infos.size() == 0 || perms.size() == 0) {
			return false;
		}

		// 将岗位下载权限根据各个流程分类
		Map<Long, List<Long>> flowIdMap = new HashMap<Long, List<Long>>();
		List<Long> permArr = null;
		for (JecnFlowFilePosPerm perm : perms) {
			if (flowIdMap.containsKey(perm.getRelateId())) {
				flowIdMap.get(perm.getRelateId()).add(perm.getFigureId());
			} else {
				permArr = new ArrayList<Long>();
				permArr.add(perm.getFigureId());
				flowIdMap.put(perm.getRelateId(), permArr);
			}
		}

		// 有流程没有岗位的下载权限
		if (flowIdMap.size() != flowSet.size()) {
			return false;
		}

		List<Long> flowPermPosList;
		for (Entry<Long, List<Long>> e : flowIdMap.entrySet()) {
			flowPermPosList = e.getValue();
			flowPermPosList.retainAll(userPosList);
			if (flowPermPosList.size() == 0) {
				return false;
			}
		}

		return true;
	}
}
