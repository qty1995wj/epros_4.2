package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

import java.io.Serializable;

public class DataBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String deptId = "";
	private String pId = "";
	private String companyName = "";
	private String deptName = "";
	private String configFilePath = "";// 配置参数文件路径
	private String userFilePath = "";// 用户数据文件路径(配置参数文件获得)
	private String resultFilePath = "";// 匹配失败结果文件路径(配置参数文件获得)
	private String startTime = "";
	private String time = "";
	private String filePath;
	private String day;
	private String loginName = "";
	private String jopName = "";
	private String tureName = "";
	private String iaAuto = "";

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getJopName() {
		return jopName;
	}

	public void setJopName(String jopName) {
		this.jopName = jopName;
	}

	public String getTureName() {
		return tureName;
	}

	public void setTureName(String tureName) {
		this.tureName = tureName;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getPId() {
		return pId;
	}

	public void setPId(String id) {
		pId = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getConfigFilePath() {
		return configFilePath;
	}

	public void setConfigFilePath(String configFilePath) {
		this.configFilePath = configFilePath;
	}

	public String getUserFilePath() {
		return userFilePath;
	}

	public void setUserFilePath(String userFilePath) {
		this.userFilePath = userFilePath;
	}

	public String getResultFilePath() {
		return resultFilePath;
	}

	public void setResultFilePath(String resultFilePath) {
		this.resultFilePath = resultFilePath;
	}

	public String getIaAuto() {
		return iaAuto;
	}

	public void setIaAuto(String iaAuto) {
		this.iaAuto = iaAuto;
	}
}
