package epros.draw.gui.workflow.aid;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 
 * 图形监听类，主要实现图形大小改变时编辑框大小也改变
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFigureComponentAdapter extends ComponentAdapter {

	public JecnFigureComponentAdapter() {
		super();
	}

	/**
	 * Invoked when the component's size changes.
	 */
	public void componentResized(ComponentEvent e) {
		componentActionPerformed(e);
	}

	private void componentActionPerformed(ComponentEvent e) {
		// 当前面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null) {
			return;
		}
		if (e.getSource() instanceof JecnBaseFigurePanel) {// 流程元素面板
			// 滚动面板
			JecnBaseFigurePanel figure = (JecnBaseFigurePanel) e.getSource();
			// 输入框
			JecnTextArea editTextArea = workflow.getAidCompCollection()
					.getEditTextArea();
			// 被操作流程元素
			JecnBaseFlowElementPanel flowElementPanel = editTextArea
					.getFlowElementPanel();

			if (flowElementPanel == null) {
				return;
			}
			if (figure == flowElementPanel) {
				editTextArea.getEditScrollPane().setBounds(
						flowElementPanel.getBounds());
			}
		}
	}
}
