package com.jecn.epros.server.dao.system;

import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.common.IBaseDao;

/***
 * 未读消息操作类
 * @author 
 * 2013-02-27
 *
 */
public interface IUnReadMessageDao  extends IBaseDao<JecnMessage, Long>  {
	/***
	 * 未读消息数量
	 */
	
	public int getUnReadMesgNum(Long inceptPeopleId) throws Exception;

}
