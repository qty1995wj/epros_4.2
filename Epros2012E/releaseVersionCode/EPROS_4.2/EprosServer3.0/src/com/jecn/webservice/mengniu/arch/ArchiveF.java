package com.jecn.webservice.mengniu.arch;

import org.apache.commons.lang.StringUtils;

public class ArchiveF {
	private String fileName;
	private String ftpPath;
	private String localPath;

	public ArchiveF(String fileName, String localPath) {
		this.fileName = fileName;
		this.localPath = localPath;
		initFtpPath();
	}

	private void initFtpPath() {
		String saveFilePath = ArchiveService.createSaveFilePath(fileName);
		// 获得后缀
		String endFix = ArchiveService.getEndFix(this.localPath).toLowerCase();
		if (StringUtils.isNotBlank(endFix) && !saveFilePath.toLowerCase().endsWith(endFix)) {
			saveFilePath += endFix;
		}
		this.ftpPath = saveFilePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public String getFtpPath() {
		return ftpPath;
	}

	public void setFtpPath(String ftpPath) {
		this.ftpPath = ftpPath;
	}

}
