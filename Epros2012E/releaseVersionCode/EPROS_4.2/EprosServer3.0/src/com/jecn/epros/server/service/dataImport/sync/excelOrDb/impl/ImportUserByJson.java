package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.SyncDataTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 *  基于Json同步数据
 * @author zhangft
 *
 */
public class ImportUserByJson extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(this.getClass());
	
	public ImportUserByJson(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 获取部门数据集合
	 */
	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取Json数据
		String result = new SyncDataTool().getDeptString();
		log.error(result);
		return getDpetList(result);
	}

	/**
	 * 获取人员数据集合
	 */
	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取Json数据
		String result = new SyncDataTool().getUserString();
		log.error(result);
		return getUserList(result);
	}

	
	/**
	 * 把数据从json取出存储到bean对象中
	 * @param result
	 */
	public List<DeptBean> getDpetList(String result) throws Exception {
		/** 部门 */
		List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
		DeptBean deptBean = null;
		JSONArray jsonArray = JSONArray.fromObject(result);
		for(int i = 0; i<jsonArray.size();i++){
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			deptBean = new DeptBean();
			deptBean.setDeptNum(jsonObject.getString("Code"));
			deptBean.setDeptName(jsonObject.getString("Name"));
			deptBean.setPerDeptNum(jsonObject.getString("ParCode"));
			deptBeanList.add(deptBean);
		}
		return deptBeanList;
	}

	/**
	 * 把数据从json取出存储到bean对象中
	 * @param result
	 */
	public List<UserBean> getUserList(String result) throws Exception {
		/** 人员 */
		List<UserBean> userBeanList = new ArrayList<UserBean>();
		UserBean userBean = null;
		JSONArray jsonArray = JSONArray.fromObject(result);
		for(int i = 0; i<jsonArray.size();i++){
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			userBean = new UserBean();
			userBean.setUserNum(jsonObject.getString("LoginName"));
			userBean.setTrueName(jsonObject.getString("UserName"));
			userBean.setPosNum(jsonObject.getString("PostCode"));
			userBean.setPosName(jsonObject.getString("PostName"));
			userBean.setDeptNum(jsonObject.getString("DeptId"));
			userBean.setEmail(jsonObject.getString("Email"));
			userBean.setEmailType(jsonObject.getInt("Flag"));
			userBean.setPhone(jsonObject.getString("HandSet"));
			userBeanList.add(userBean);
		}
		return userBeanList;
	}
//	
//	public static void main(String[] args) throws RemoteException  {
//		try {
////			List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
////			DeptBean deptBean = null;
////			String str = "[{Code:'97',Name:'石油物探技术研究院',ParCode:'-1'},{Code:'375',Name:'综合办公室',ParCode:'97'},{Code:'376',Name:'基地综合服务部',ParCode:'97'},{Code:'378',Name:'副总',ParCode:'97'},{Code:'379',Name:'院领导',ParCode:'97'},{Code:'380',Name:'地震处理解释中心',ParCode:'97'},{Code:'381',Name:'地球物理信息中心',ParCode:'97'},{Code:'382',Name:'地球物理软件研究所',ParCode:'97'},{Code:'383',Name:'地球物理实验中心',ParCode:'97'},{Code:'384',Name:'油藏地球物理研究所',ParCode:'97'},{Code:'385',Name:'地震成像技术研究所',ParCode:'97'},{Code:'386',Name:'计划财务部',ParCode:'97'},{Code:'387',Name:'科研生产部',ParCode:'97'},{Code:'389',Name:'人力资源部',ParCode:'97'},{Code:'391',Name:'院长助理',ParCode:'97'},{Code:'394',Name:'技术推广部',ParCode:'97'},{Code:'395',Name:'地震采集技术研究所',ParCode:'97'},{Code:'396',Name:'物探战略规划研究所',ParCode:'97'},{Code:'398',Name:'车队',ParCode:'97'},{Code:'399',Name:'基地综合服务部',ParCode:'376'},{Code:'400',Name:'首席专家',ParCode:'97'}]";
////			
////			JSONArray jsonArray = JSONArray.fromObject(str);
////			System.out.println(jsonArray.size());
////			for(int i = 0; i<jsonArray.size();i++){
////				JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
////				deptBean = new DeptBean();
////				deptBean.setDeptNum(jsonObject.getString("Code"));
////				deptBean.setDeptName(jsonObject.getString("Name"));
////				deptBean.setPerDeptNum(jsonObject.getString("ParCode"));
////				deptBeanList.add(deptBean);
////			}
////			for (DeptBean dept : deptBeanList) {
////				System.out.println(dept.getDeptNum()+"   "+dept.getDeptName() + "\t\t"+dept.getPerDeptNum());
////			}
//			
//			List<UserBean> userBeanList = new ArrayList<UserBean>();
//			UserBean userBean = null;
//			String result = "[{LoginName:'duaj',UserName:'杜爱军',PostCode:'',PostName:'主任',DeptId:'57',Email:'duaj@igp.cn',Flag:'0',HandSet:'13705175997'},{LoginName:'shaozd',UserName:'邵志东',PostCode:'',PostName:'副主任',DeptId:'57',Email:'shaozhd@igp.cn',Flag:'0',HandSet:'18651810533'}]";
//			JSONArray jsonArray = JSONArray.fromObject(result);
//			System.out.println(jsonArray.size());
//			for(int i = 0; i<jsonArray.size();i++){
//				JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
//				userBean = new UserBean();
//				userBean.setUserNum(jsonObject.getString("LoginName"));
//				userBean.setTrueName(jsonObject.getString("UserName"));
//				userBean.setPosNum(jsonObject.getString("PostCode"));
//				userBean.setPosName(jsonObject.getString("PostName"));
//				userBean.setDeptNum(jsonObject.getString("DeptId"));
//				userBean.setEmail(jsonObject.getString("Email"));
//				userBean.setEmailType(jsonObject.getInt("Flag"));
//				userBean.setPhone(jsonObject.getString("HandSet"));
//				userBeanList.add(userBean);
//			}
//			for (UserBean user : userBeanList) {
//				System.out.println(user.getUserNum()+"  "+user.getTrueName()+"  "+ user.getPosNum()
//						+"  "+user.getPosName()+"  "+user.getDeptNum()+"  "+
//						user.getEmail()+"   "+user.getEmailType()+"  "+user.getPhone());
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
////		System.out.println(new com.jecn.Test().getDeptString());
////		System.out.println(new com.jecn.Test().getUserString());
//		
//	}
}
