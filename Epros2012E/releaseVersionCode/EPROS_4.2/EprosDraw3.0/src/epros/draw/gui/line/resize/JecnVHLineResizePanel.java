package epros.draw.gui.line.resize;

import java.awt.Color;

import epros.draw.gui.swing.JecnBaseResizePanel;

/**
 * 横竖分割线选中点
 * 
 * @author ZHANGXH
 * @date： 日期：Jun 11, 2012 时间：3:39:54 PM
 */
public class JecnVHLineResizePanel extends JecnBaseResizePanel {
	private JecnVHLineResizePanel() {
		// 默认背景色为绿色
		this.setBackground(Color.GREEN);
		this.setSize(6, 6);
	}

	/**
	 * 
	 * 创建横竖分割线选中点
	 * 
	 * @return JecnVHLineResizePanel
	 */
	public static JecnVHLineResizePanel createVHLineResizePanel() {
		return new JecnVHLineResizePanel();
	}
}
