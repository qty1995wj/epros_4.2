package com.jecn.epros.server.dao.system;

import com.jecn.epros.server.bean.system.JecnFixedEmail;
import com.jecn.epros.server.common.IBaseDao;

/***
 * 发布 固定人员处理
 * 2014-04-09
 *
 */
public interface IJecnFixedEmailDao extends IBaseDao<JecnFixedEmail, String>{
	
}
