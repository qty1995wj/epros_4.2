package com.jecn.epros.server.download.customized.baoLong;

import java.awt.Point;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.HeaderDocBean;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.field.RtfPageNumber;
import com.lowagie.text.rtf.field.RtfTotalPageNumber;
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter;
import com.lowagie.text.rtf.style.RtfFont;

/**
 * 保隆下载通用类
 * 
 * @author fuzhh May 6, 2013
 * 
 */
public class DownBlUtil {
	private static final Logger log = Logger.getLogger(DownBlUtil.class);
	/**
	 * 生成word 页眉
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @param document
	 * @param headerDocBean
	 *            页眉的数据
	 */
	public static void getHeaderDoc(Document document,
			HeaderDocBean headerDocBean) {
		// HeaderFooter添加页眉
		// 名称
		String name = headerDocBean.getName();
		// 编号
		String inputNumber = headerDocBean.getInputNum();
		// 版本
		String stringV = headerDocBean.getVersion();
		// 发布日期
		String releaseDate = headerDocBean.getReleaseDate();
		// 生效日期
		String effectiveDate = headerDocBean.getEffectiveDate();

		try {
			Table table = new Table(3);
			table.setWidth(100f);
			table.setWidths(new int[] { 60, 15, 25 });
			table.setBorderWidth(1);
			// table.setBorderColor(new Color(255, 255, 255));

			// 保 隆 管 理 体 系 文 件
			Cell cell = new Cell(ST_BOLD_22_MIDDLE(JecnUtil
					.getValue("blDocShowName")));
			cell.setBorderWidthBottom(0);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(2);
			// cell.setBorderColor(new Color(255, 255, 255));
			cell.setRowspan(2);
			table.addCell(cell, new Point(0, 0));

			cell = new Cell(ST_BOLD_18_MIDDLE(name));
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			cell.setRowspan(3);
			table.addCell(cell, new Point(2, 0));

			// 文件编号
			cell = new Cell(ST_11(JecnUtil.getValue("fileNumber")));
			// cell.setBorderWidth(2);
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(1);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(2);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 1));
			cell = new Cell(ST_11(inputNumber));
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(2);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 2));

			// 文件版本
			cell = new Cell(ST_11(JecnUtil.getValue("theFileVersion")));
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(1);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 1));
			cell = new Cell(ST_11(stringV));
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 2));

			// 发布日期
			cell = new Cell(ST_11(JecnUtil.getValue("releaseDate")));
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(1);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(2, 1));
			cell = new Cell(releaseDate);
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(2, 2));

			// 生效日期
			cell = new Cell(ST_11(JecnUtil.getValue("becomeEffectiveDate")));
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(1);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(3, 1));
			cell = new Cell(effectiveDate);
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(3, 2));

			// 页码
			cell = new Cell(ST_11(JecnUtil.getValue("pageCode")));
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(1);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(4, 1));

			Paragraph parafooter = new Paragraph();
			// 第
			parafooter.add(new Phrase(ST_11(JecnUtil.getValue("inTheFirst"))));
			parafooter.add(new RtfPageNumber());
			// 页 共
			parafooter.add(new Phrase(ST_11(JecnUtil.getValue("pageATotal"))));
			parafooter.add(new RtfTotalPageNumber());
			// 页
			parafooter.add(new Phrase(ST_11(JecnUtil.getValue("page"))));

			cell = new Cell(parafooter);
			cell.setBorderWidthBottom(1);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			// cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(4, 2));

			document.setHeader(new RtfHeaderFooter(table));
		} catch (DocumentException e) {
			log.error("",e);
		}
	}

	/**
	 * 生成表格
	 * 
	 * @author fuzhh Nov 6, 2012
	 * @param tableWidth
	 *            表格的宽度
	 * @param tableInt
	 *            表格各个宽度的数组
	 * @param tableTopNameList
	 *            表格名称集合
	 * @param tableContentList
	 *            表格内容集合
	 * @return
	 */
	public static Table getTable(float tableWidth, int[] tableInt,
			List<String> tableTitleNameList, List<Object[]> tableContentList) {
		Table table = null;
		try {
			table = new Table(tableInt.length);
			// 设置各个表格宽度
			table.setWidths(tableInt);
			// 设置居左
			table.setAlignment(Table.ALIGN_LEFT);
			// 设置宽度
			table.setWidth(tableWidth);
			for (int i = 0; i < tableTitleNameList.size(); i++) {
				String tableTop = tableTitleNameList.get(i);
				Cell tableTopCell = new Cell(BODY_BOLD_11_MIDDLE(tableTop));
				tableTopCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(tableTopCell, new Point(0, i));
			}
			int tableCount = 1;
			for (Object[] obj : tableContentList) {
				for (int i = 0; i < tableInt.length; i++) {
					Object object = obj[i];
					if (object == null) {
						object = "";
					}
					table.addCell(BODY_11_MIDDLE(object.toString()), new Point(
							tableCount, i));
				}
				tableCount++;
			}
		} catch (BadElementException e) {
			log.error("",e);
		} catch (DocumentException e) {
			log.error("",e);
		}
		return table;
	}

	/**
	 * 生成word 页脚
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @param document
	 */
	public static void getFooterDoc(Document document) {
		HeaderFooter foot = null;
		foot = new HeaderFooter(new Phrase(BODY_11_MIDDLE(JecnUtil
				.getValue("wordTheFooter"))), false);
		foot.setAlignment(Rectangle.ALIGN_CENTER);
		document.setFooter(foot);
	}

	// 楷体_GB2312 居中,显示为11号
	public static Paragraph ST_11_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("楷 体 _GB2312", 11.2f));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 楷体_GB2312 居中,显示为11号
	public static Paragraph ST_11(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("楷 体 _GB2312", 11.2f));
		return p;
	}

	// 楷体_GB2312 显示为18号 加粗
	public static Paragraph ST_BOLD_18_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("楷 体 _GB2312", 18,
				Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 楷体_GB2312 显示为22号 加粗
	public static Paragraph ST_BOLD_22_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("楷 体 _GB2312", 22,
				Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 楷体_GB2312 居左,显示为11号 加粗
	public static Paragraph BODY_BOLD_11_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("楷 体 _GB2312", 11.2f,
				Font.BOLD));
		p.setLeading(12f);
		return p;
	}

	// 楷体_GB2312 居左,显示为11号
	public static Paragraph BODY_11_MIDDLE(String content) {
		RtfFont font = new RtfFont("楷 体 _GB2312", 11.2f);
		Paragraph p = new Paragraph(DownloadUtil.changeNetToWordStyle(content),
				font);
		p.setLeading(12f);
		return p;
	}
}
