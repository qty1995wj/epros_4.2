package com.jecn.epros.server.bean.download;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.rule.JecnRuleCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommon;

public class RuleDownloadBean implements Serializable {

	/** 制度名称 */
	private String ruleName;
	/** 制度密集 */
	private String ruleIsPublic;
	/** 制度编号 */
	private String ruleInputNum;
	/** 版本 */
	private String ruleVersion;
	/** 公司名称 */
	private String companyName;
	/** 责任部门名称 */
	private String orgName;
	/** 责任人 **/
	private String dutyName;
	/** 制度标题表 */
	private List<RuleTitleT> ruleTitleTList = new ArrayList<RuleTitleT>();
	/** 制度文件信息 0文件编号 1文件名称 */
	private List<RuleFileBean> ruleFileList = new ArrayList<RuleFileBean>();
	/** 相关流程 0流程编号 1流程名称 */
	private List<RuleFlowBean> ruleFlowList = new ArrayList<RuleFlowBean>();
	/** 相关流程 0流程编号 1流程名称 */
	private List<RuleFlowBean> ruleFlowMapList = new ArrayList<RuleFlowBean>();
	/** 相关风险 */
	private List<JecnRiskCommon> ruleRiskList = new ArrayList<JecnRiskCommon>();
	/** 相关制度 */
	private List<JecnRuleCommon> ruleRuleList = new ArrayList<JecnRuleCommon>();
	/** 相关标准 */
	private List<JecnStandarCommon> ruleStandardList = new ArrayList<JecnStandarCommon>();
	/** 制度内容 */
	private List<RuleContentBean> ruleContentList = new ArrayList<RuleContentBean>();
	/** 评审人集合 0 评审人类型 1名称 2日期 */
	private List<Object[]> peopleList = new ArrayList<Object[]>();
	/** 文控信息 **/
	private List<JecnTaskHistoryNew> historys;

	private JecnRuleBaiscInfoT baiscInfo = null;

	private boolean isPub;
	private JecnTaskHistoryNew lastHistory;

	public String getRuleIsPublicStr() {
		if (StringUtils.isBlank(ruleIsPublic)) {
			return "";
		}
		if ("0".equals(ruleIsPublic)) {
			return "秘密";
		} else if ("1".equals(ruleIsPublic)) {
			return "公开";
		}
		return "";
	}

	public List<JecnRuleCommon> getRuleRuleList() {
		return ruleRuleList;
	}

	public void setRuleRuleList(List<JecnRuleCommon> ruleRuleList) {
		this.ruleRuleList = ruleRuleList;
	}

	public String getVersion() {
		JecnTaskHistoryNew lastHistory = getLastHistory();
		if (lastHistory != null) {
			return lastHistory.getVersionId();
		}
		return "";
	}

	// ---------------------------

	public boolean isPub() {
		return isPub;
	}

	public void setPub(boolean isPub) {
		this.isPub = isPub;
	}

	public List<JecnTaskHistoryNew> getHistorys() {
		return historys;
	}

	public void setHistorys(List<JecnTaskHistoryNew> historys) {
		this.historys = historys;
	}

	public List<Object[]> getPeopleList() {
		return peopleList;
	}

	public void setPeopleList(List<Object[]> peopleList) {
		this.peopleList = peopleList;
	}

	public void setRuleTitleTList(List<RuleTitleT> ruleTitleTList) {
		this.ruleTitleTList = ruleTitleTList;
	}

	public void setRuleFileList(List<RuleFileBean> ruleFileList) {
		this.ruleFileList = ruleFileList;
	}

	public void setRuleFlowList(List<RuleFlowBean> ruleFlowList) {
		this.ruleFlowList = ruleFlowList;
	}

	public void setRuleContentList(List<RuleContentBean> ruleContentList) {
		this.ruleContentList = ruleContentList;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleIsPublic() {
		return ruleIsPublic;
	}

	public void setRuleIsPublic(String ruleIsPublic) {
		this.ruleIsPublic = ruleIsPublic;
	}

	public String getRuleInputNum() {
		return ruleInputNum;
	}

	public void setRuleInputNum(String ruleInputNum) {
		this.ruleInputNum = ruleInputNum;
	}

	public String getRuleVersion() {
		return ruleVersion;
	}

	public void setRuleVersion(String ruleVersion) {
		this.ruleVersion = ruleVersion;
	}

	public List<RuleTitleT> getRuleTitleTList() {
		return ruleTitleTList;
	}

	public List<RuleFileBean> getRuleFileList() {
		return ruleFileList;
	}

	public List<RuleFlowBean> getRuleFlowList() {
		return ruleFlowList;
	}

	public List<RuleContentBean> getRuleContentList() {
		return ruleContentList;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<RuleFlowBean> getRuleFlowMapList() {
		return ruleFlowMapList;
	}

	public void setRuleFlowMapList(List<RuleFlowBean> ruleFlowMapList) {
		this.ruleFlowMapList = ruleFlowMapList;
	}

	public List<JecnRiskCommon> getRuleRiskList() {
		return ruleRiskList;
	}

	public void setRuleRiskList(List<JecnRiskCommon> ruleRiskList) {
		this.ruleRiskList = ruleRiskList;
	}

	public List<JecnStandarCommon> getRuleStandardList() {
		return ruleStandardList;
	}

	public void setRuleStandardList(List<JecnStandarCommon> ruleStandardList) {
		this.ruleStandardList = ruleStandardList;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public List<JecnConfigItemBean> getJecnConfigItemBean() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPubDate() {
		if (historys == null || historys.size() == 0) {
			return "";
		}
		return JecnCommon.getStringbyDate(historys.get(0).getPublishDate());
	}

	public JecnRuleBaiscInfoT getBaiscInfo() {
		return baiscInfo;
	}

	public void setBaiscInfo(JecnRuleBaiscInfoT baiscInfo) {
		this.baiscInfo = baiscInfo;
	}

	public void setLatestHistoryNew(JecnTaskHistoryNew latestHistoryNew) {
		this.lastHistory = latestHistoryNew;
	}

	public JecnTaskHistoryNew getLastHistory() {
		return lastHistory;
	}

	public void setLastHistory(JecnTaskHistoryNew lastHistory) {
		this.lastHistory = lastHistory;
	}

	public String getDutyName() {
		return dutyName;
	}

	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

}
