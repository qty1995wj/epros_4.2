package com.jecn.epros.server.dao.define;

import java.util.List;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.common.IBaseDao;

public interface ITermDefinitionDao extends IBaseDao<TermDefinitionLibrary, Long>{

	List<Object[]> getRoleAuthChilds(long pId, Long projectId, long userId);

}
