package epros.designer.gui.process.activitiesProperty;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnModeFileT;
import epros.draw.designer.JecnTempletT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 活动明细：输出
 * 
 * @author 2012-07-27
 * 
 */
public class ActiveOutPanel extends JecnPanel implements CaretListener {
	private static Logger log = Logger.getLogger(ActiveOutPanel.class);
	/** 左侧面板 */
	private JecnPanel leftPanel;

	/** 右侧面板 */
	private JecnPanel rightPanel;

	/** 表单面板 */
	private JecnPanel formPanel;

	/** 表单滚动面板 */
	private JScrollPane formScrollPane = new JScrollPane();

	/** 表单 Table */
	private JTable formTable = null;

	/** 样例面板 */
	private JecnPanel samplePanel;

	/** 样例滚动面板 */
	private JScrollPane sampleScrollPane = new JScrollPane();

	/** 样例Table */
	private JTable sampleTable = null;

	/** * 表单按钮 */
	private JButton formBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** * 删除表单按钮 */
	private JButton deleteFormormBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** * 打开表单文件按钮 */
	private JButton openFormBut = new JButton(JecnProperties.getValue("openFile"));

	/** * 样例按钮 */
	private JButton sampleBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** * 删除样例按钮 */
	private JButton deleteSampleBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** * 打开样例文件按钮 */
	private JButton openSampleBut = new JButton(JecnProperties.getValue("openFile"));

	/** 左侧按钮面板 */
	private JecnPanel leftButPanel = new JecnPanel();

	/** 右侧按钮面板 */
	private JecnPanel rightButPanel = new JecnPanel();

	/** 活动的数据 */
	private JecnActiveData jecnActiveData;

	/** 框的拥有者 */
	private JecnDialog jecnDialog;

	/** 页面的显示结果 */
	private List<JecnModeFileT> listJecnModeFileT = new ArrayList<JecnModeFileT>();

	/** 样例表对应的表单对象* */
	private JecnModeFileT jecnModeFileT = null;
	// 说明输出
	private JecnPanel notePanel = null;
	private JecnTextArea noteTextArea = new JecnTextArea();
	/** 输出说明提示信息框 */
	protected JecnUserInfoTextArea noteInfoTextArea = new JecnUserInfoTextArea();

	public ActiveOutPanel(JecnActiveData jecnActiveData, JecnDialog jecnDialog) {
		this.jecnActiveData = jecnActiveData;
		this.jecnDialog = jecnDialog;

		if (JecnDesignerCommon.isShowEleAttribute()) {
			leftPanel = new JecnPanel();
			samplePanel = new JecnPanel();
			rightPanel = new JecnPanel();
			formPanel = new JecnPanel();
			Dimension dimension = new Dimension(230, 80);
			formScrollPane.setPreferredSize(dimension);
			sampleScrollPane.setPreferredSize(dimension);
		} else {
			this.setSize(480, 350);
			leftPanel = new JecnPanel(240, 320);
			samplePanel = new JecnPanel(230, 270);
			rightPanel = new JecnPanel(240, 320);
			formPanel = new JecnPanel(230, 270);
		}

		formScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		sampleScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// formBut表单按钮
		formBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				formButSampeFormed();
			}
		});
		// sampleBut样例按钮
		sampleBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sampleButButSampeFormed();
			}
		});
		// 删除样例
		deleteSampleBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteSampeFormed();
			}
		});
		// 打开文件样例
		openSampleBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 判断是否选中Table中的一行，没选中，提示选中一行
				int selectRow = sampleTable.getSelectedRow();
				if (selectRow != -1) {
					Long id = Long.valueOf(sampleTable.getValueAt(selectRow, 0).toString());
					JecnDesignerCommon.openFile(id);
				} else {
					JecnOptionPane.showMessageDialog(ActiveOutPanel.this, JecnProperties.getValue("pelaseChoosSample"));
				}
			}
		});
		// 删除表单
		deleteFormormBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteFormormButFormed();
			}
		});
		// 打开表单文件
		openFormBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectRow = formTable.getSelectedRow();
				if (selectRow != -1) {
					// 表单文件ID
					Long id = listJecnModeFileT.get(formTable.getSelectedRow()).getFileMId();
					JecnDesignerCommon.openFile(id);
				} else {
					JecnOptionPane.showMessageDialog(ActiveOutPanel.this, JecnProperties.getValue("pelaseChoosForm"));
				}

			}
		});

		initCom();
		initLaout();
		noteTextArea.addCaretListener(this);
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.jecnActiveData = jecnActiveData;
		removeAllTable();
		Vector<Vector<String>> fileData = getContent();
		for (Vector<String> v : fileData) {
			((DefaultTableModel) formTable.getModel()).addRow(v);
		}
	}

	private void removeAllTable() {
		listJecnModeFileT = new ArrayList<JecnModeFileT>();
		for (int index = formTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) formTable.getModel()).removeRow(index);
		}
	}

	/**
	 * true:显示文本
	 * 
	 * @return
	 */
	private boolean isShowInputText() {
		return JecnConfigTool.isShowInputText();
	}

	private void initCom() {
		notePanel = new JecnPanel(550, 90);
		notePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		notePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("actBase")));

	}

	private void initLaout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(1, 1, 1, 1);

		if (isShowInputText()) {
			// 说明
			JScrollPane noteTablePane = new JScrollPane();
			noteTablePane.setViewportView(noteTextArea);
			notePanel.setLayout(new GridBagLayout());
			c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			notePanel.add(noteTablePane, c);
			c = new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			this.add(notePanel, c);
			if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standActivityOutPutNotEmpty)) {
				c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				notePanel.add(new CommonJlabelMustWrite(), c);
			}
			// 输出说明信息提示
			c = new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					JecnUIUtil.getInsets0(), 0, 0);
			this.add(noteInfoTextArea, c);
		}

		// 表单
		c = new GridBagConstraints(0, 2, 1, 1, 0.5, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(leftPanel, c);
		addLeftPanel(c, insets);

		// 样例
		c = new GridBagConstraints(1, 2, 1, 1, 0.5, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(rightPanel, c);
		addRightPanel(c, insets);
	}

	private void addLeftPanel(GridBagConstraints c, Insets insets) {
		leftPanel.setLayout(new GridBagLayout());
		// formPanel
		c = new GridBagConstraints(0, 0, 3, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		leftPanel.add(formPanel, c);
		formPanel.setLayout(new GridBagLayout());
		formPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("actForm")));
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0);
		formPanel.add(formScrollPane, c);
		formTable = new JTable(getTableModel());
		formTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		formTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 单击表单表格数据 显示样例
		formTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				searchTableMousePressed();
			}
		});
		formTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = formTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(0);
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		formScrollPane.setViewportView(formTable);
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(1, 0, 1, 0), 0, 0);
		formPanel.add(leftButPanel, c);
		leftButPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		leftButPanel.add(formBut);
		leftButPanel.add(deleteFormormBut);
		leftButPanel.add(openFormBut);
	}

	private void addRightPanel(GridBagConstraints c, Insets insets) {
		rightPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 3, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		rightPanel.add(samplePanel, c);
		samplePanel.setLayout(new GridBagLayout());
		samplePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("actSample")));
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		samplePanel.add(sampleScrollPane, c);
		sampleTable = new JTable(getSampTableModel());
		sampleTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		sampleTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		sampleTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = sampleTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(0);
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		sampleScrollPane.setViewportView(sampleTable);
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(1, 0, 1, 0), 0, 0);
		samplePanel.add(rightButPanel, c);
		rightButPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		rightButPanel.add(sampleBut);
		rightButPanel.add(deleteSampleBut);
		rightButPanel.add(openSampleBut);
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	protected void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == noteTextArea) {// 编号输入框
			// 编号长度不能超过1200个字符或600个汉字
			checkTextFieldLength(noteInfoTextArea, noteTextArea);
		}
	}

	/**
	 * 
	 * 校验JTextField的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textField
	 *            JTextField
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JecnTextArea jecnTextArea) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkAreaNote(jecnTextArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 点击确认按钮提交前校验
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	protected boolean okBtnCheck() {
		boolean ret = true;
		// 编号
		if (!DrawCommon.isNullOrEmtry(noteTextArea.getText())) {
			ret = checkTextFieldLength(noteInfoTextArea, noteTextArea);
		}
		return ret;
	}

	/**
	 * formButSampeFormed 表单按钮 选择表单文件
	 */
	private void formButSampeFormed() {
		List<JecnTreeBean> listFormFile = new ArrayList<JecnTreeBean>();
		for (JecnModeFileT jecnModeFileT : this.listJecnModeFileT) {
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(jecnModeFileT.getFileMId());
			jecnTreeBean.setName(jecnModeFileT.getModeName());
			listFormFile.add(jecnTreeBean);
		}
		FileChooseDialog fileChooseDialog = new FileChooseDialog(listFormFile, jecnDialog);
		// 只能选择一个文件
		fileChooseDialog.setSelectMutil(true);
		fileChooseDialog.setVisible(true);
		if (!fileChooseDialog.isOperation()) {
			return;
		}
		// 选择的结果集
		List<JecnTreeBean> listJecnTreeBean = fileChooseDialog.getListJecnTreeBean();
		// 修改结果集
		List<JecnModeFileT> listResult = new ArrayList<JecnModeFileT>();
		for (JecnTreeBean jecnTreeBean : listJecnTreeBean) {
			boolean isExist = false;
			for (JecnModeFileT jecnModeFileT : this.listJecnModeFileT) {
				if (jecnTreeBean.getId().equals(jecnModeFileT.getFileMId())) {
					jecnModeFileT.setModeName(jecnTreeBean.getName());
					isExist = true;
					listResult.add(jecnModeFileT);
					break;
				}
			}
			// 新添加的文件
			if (!isExist) {
				JecnModeFileT jecnModeFileT = new JecnModeFileT();
				jecnModeFileT.setFileMId(jecnTreeBean.getId());
				jecnModeFileT.setModeName(jecnTreeBean.getName());
				jecnModeFileT.setFigureId(this.jecnActiveData.getFlowElementId());
				listResult.add(jecnModeFileT);
			}
		}
		this.setListJecnModeFileT(listResult);
		// 晴空表单列表
		for (int i = formTable.getRowCount() - 1; i >= 0; i--) {
			((JecnTableModel) formTable.getModel()).removeRow(i);
		}
		// 更新表单表单列表
		for (JecnTreeBean jecnTreeBean : listJecnTreeBean) {
			((JecnTableModel) formTable.getModel()).addRow(getVectorByJecnTreeBean(jecnTreeBean));
		}
	}

	/**
	 * @author zhangchen Aug 28, 2012
	 * @description：jecnTreeBean转换成Vector
	 * @param jecnTreeBean
	 * @return
	 */
	private Vector<String> getVectorByJecnTreeBean(JecnTreeBean jecnTreeBean) {
		Vector<String> vector = new Vector<String>();
		vector.add(jecnTreeBean.getId().toString());
		vector.add(jecnTreeBean.getName().toString());
		return vector;
	}

	/***************************************************************************
	 * 删除表单
	 */
	private void deleteFormormButFormed() {
		int[] selectRows = formTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseSelectDeleteForm"));
			return;
		}
		int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isDeleteC"), null,
				JecnOptionPane.YES_NO_OPTION);
		if (dialog == JecnOptionPane.YES_OPTION) {
			// 待删除的表单集合
			List<Long> listIds = new ArrayList<Long>();
			JecnTableModel jecnTableModel = (JecnTableModel) formTable.getModel();
			// 删除选中的表单的项
			for (int j = selectRows.length - 1; j >= 0; j--) {
				listIds.add(Long.valueOf(jecnTableModel.getValueAt(selectRows[j], 0).toString()));
				listJecnModeFileT.remove(selectRows[j]);
				((JecnTableModel) formTable.getModel()).removeRow(selectRows[j]);
			}
			// 清空样例的table
			for (int i = sampleTable.getRowCount() - 1; i >= 0; i--) {
				((JecnTableModel) sampleTable.getModel()).removeRow(i);
			}
		}
	}

	/***************************************************************************
	 * 样例按钮 选择样例文件
	 */
	private void sampleButButSampeFormed() {
		int[] selectRows = formTable.getSelectedRows();
		if (selectRows.length != 1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pelaseChoosForm"));
			return;
		}
		int selectRow = formTable.getSelectedRow();
		// 选中表单样例
		JecnModeFileT jecnModeFileT = this.listJecnModeFileT.get(selectRow);
		// 获取输入表格上的数据
		List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
		if (jecnModeFileT.getListJecnTempletT() != null) {
			for (JecnTempletT jecnTempletT : jecnModeFileT.getListJecnTempletT()) {
				JecnTreeBean jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(jecnTempletT.getFileId());
				jecnTreeBean.setName(jecnTempletT.getFileName());
				listJecnTreeBean.add(jecnTreeBean);
			}
		}
		FileChooseDialog fileChooseDialog = new FileChooseDialog(listJecnTreeBean, jecnDialog);
		// 只能选择一个文件
		fileChooseDialog.setSelectMutil(true);
		fileChooseDialog.setVisible(true);
		if (!fileChooseDialog.isOperation()) {
			return;
		}
		// 更新样例数据
		List<JecnTempletT> listJecnTempletTResult = new ArrayList<JecnTempletT>();
		for (JecnTreeBean jecnTreeBean : fileChooseDialog.getListJecnTreeBean()) {
			boolean isExist = false;
			if (jecnModeFileT.getListJecnTempletT() != null) {
				for (JecnTempletT jecnTempletT : jecnModeFileT.getListJecnTempletT()) {
					if (jecnTreeBean.getId().equals(jecnTempletT.getFileId())) {
						jecnTempletT.setFileName(jecnTreeBean.getName());
						isExist = true;
						listJecnTempletTResult.add(jecnTempletT);
						break;
					}
				}
			}
			if (!isExist) {
				JecnTempletT jecnTempletT = new JecnTempletT();
				jecnTempletT.setFileId(jecnTreeBean.getId());
				jecnTempletT.setFileName(jecnTreeBean.getName());
				jecnTempletT.setModeFileId(jecnModeFileT.getModeFileId());
				listJecnTempletTResult.add(jecnTempletT);
			}
		}
		jecnModeFileT.setListJecnTempletT(listJecnTempletTResult);
		// 先删除
		for (int i = sampleTable.getRowCount() - 1; i >= 0; i--) {
			((JecnTableModel) sampleTable.getModel()).removeRow(i);
		}
		// 再添加
		for (JecnTreeBean jecnTreeBean : fileChooseDialog.getListJecnTreeBean()) {
			((JecnTableModel) sampleTable.getModel()).addRow(getVectorByJecnTreeBean(jecnTreeBean));
		}
	}

	/***************************************************************************
	 * 删除样例
	 */
	private void deleteSampeFormed() {
		int[] selectRows = sampleTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseSelectDeleteSamples"));
			return;
		}
		int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isDeleteC"), null,
				JecnOptionPane.YES_NO_OPTION);
		if (dialog == JecnOptionPane.YES_OPTION) {
			JecnTableModel jecnTableModel = (JecnTableModel) sampleTable.getModel();
			for (int i = selectRows.length - 1; i >= 0; i--) {
				// 删除数据
				jecnModeFileT.getListJecnTempletT().remove(selectRows[i]);
				// 删除样例table
				jecnTableModel.removeRow(selectRows[i]);
			}
		}
	}

	/**
	 * @description:单击表单Table
	 */
	private void searchTableMousePressed() {
		int row = formTable.getSelectedRow();
		if (row != -1) {
			jecnModeFileT = this.listJecnModeFileT.get(row);
			if (jecnModeFileT == null) {
				return;
			}
			// 先删除
			for (int i = sampleTable.getRowCount() - 1; i >= 0; i--) {
				((JecnTableModel) sampleTable.getModel()).removeRow(i);
			}
			if (jecnModeFileT.getListJecnTempletT() != null) {
				// 再添加
				for (JecnTempletT jecnTempletT : jecnModeFileT.getListJecnTempletT()) {
					JecnTreeBean jecnTreeBean = new JecnTreeBean();
					jecnTreeBean.setId(jecnTempletT.getFileId());
					jecnTreeBean.setName(jecnTempletT.getFileName());
					((JecnTableModel) sampleTable.getModel()).addRow(getVectorByJecnTreeBean(jecnTreeBean));
				}
			}
		}
	}

	/***************************************************************************
	 * 初始化表单内容
	 * 
	 * @return
	 */
	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		if (jecnActiveData != null && jecnActiveData.getListModeFileT() != null) {
			// 初始化输出说明
			this.noteTextArea.setText(jecnActiveData.getActivityOutput());
			for (JecnModeFileT jecnModeFileT : jecnActiveData.getListModeFileT()) {
				Vector<String> row = new Vector<String>();
				row.add(String.valueOf(jecnModeFileT.getFileMId()));
				// 表单名称
				row.add(jecnModeFileT.getModeName());
				JecnModeFileT jecnModeFileTData = new JecnModeFileT();

				jecnModeFileTData.setModeFileId(jecnModeFileT.getModeFileId());
				jecnModeFileTData.setFigureId(jecnModeFileT.getFigureId());
				jecnModeFileTData.setFileMId(jecnModeFileT.getFileMId());
				jecnModeFileTData.setModeName(jecnModeFileT.getModeName());
				jecnModeFileTData.setFigureUUID(jecnModeFileT.getFigureUUID());
				jecnModeFileTData.setUUID(jecnModeFileT.getUUID());
				jecnModeFileTData.setListJecnTempletT(new ArrayList<JecnTempletT>());
				if (jecnModeFileT.getListJecnTempletT() != null) {
					for (JecnTempletT jecnTempletT : jecnModeFileT.getListJecnTempletT()) {
						JecnTempletT jecnTempletTData = new JecnTempletT();
						jecnTempletTData.setId(jecnTempletT.getId());
						jecnTempletTData.setModeFileId(jecnTempletT.getModeFileId());
						jecnTempletTData.setFileId(jecnTempletT.getFileId());
						jecnTempletTData.setFileName(jecnTempletT.getFileName());
						jecnTempletTData.setModeUUID(jecnTempletT.getModeUUID());
						jecnTempletTData.setUUID(jecnTempletT.getUUID());
						jecnModeFileTData.getListJecnTempletT().add(jecnTempletTData);
					}
				}
				this.listJecnModeFileT.add(jecnModeFileTData);
				vs.add(row);
			}
		}
		return vs;
	}

	/**
	 * 生成表单table的Model
	 * 
	 * @return
	 */
	private JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getContent());
	}

	/***************************************************************************
	 * 初始化样例内容
	 * 
	 * @return
	 */
	private Vector<Vector<String>> getSampContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		return vs;
	}

	/**
	 * 生成样例table的Model
	 * 
	 * @return
	 */
	private JecnTableModel getSampTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getSampContent());
	}

	public List<JecnModeFileT> getListJecnModeFileT() {
		return listJecnModeFileT;
	}

	public void setListJecnModeFileT(List<JecnModeFileT> listJecnModeFileT) {
		this.listJecnModeFileT = listJecnModeFileT;
	}

	public JecnTextArea getNoteTextArea() {
		return noteTextArea;
	}

	public void setNoteTextArea(JecnTextArea noteTextArea) {
		this.noteTextArea = noteTextArea;
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);

	}

	public boolean isUpdate() {
		if (isShowInputText()
				&& !DrawCommon.checkStringSame(this.getNoteTextArea().getText(), jecnActiveData.getActivityOutput())) {
			return true;
		}
		if (jecnActiveData.getListModeFileT().size() != listJecnModeFileT.size()) {
			return true;
		}
		for (JecnModeFileT newModeFileT : listJecnModeFileT) {
			boolean isExist = false;
			for (JecnModeFileT oldModeFileT : jecnActiveData.getListModeFileT()) {
				if (newModeFileT.getFileMId().toString().equals(oldModeFileT.getFileMId().toString())) {
					isExist = true;
					if (isUpdateTmp(newModeFileT.getListJecnTempletT(), oldModeFileT.getListJecnTempletT())) {
						return true;
					}
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断样例是否存在更改
	 * 
	 * @param newJecnTempletT
	 * @param oldJecnTempletT
	 * @return
	 */
	private boolean isUpdateTmp(List<JecnTempletT> newJecnTempletT, List<JecnTempletT> oldJecnTempletT) {
		if (newJecnTempletT == null) {
			newJecnTempletT = new ArrayList<JecnTempletT>();
		}
		if (oldJecnTempletT == null) {
			oldJecnTempletT = new ArrayList<JecnTempletT>();
		}
		if (newJecnTempletT.size() != oldJecnTempletT.size()) {
			return true;
		}
		for (JecnTempletT newTmpT : newJecnTempletT) {
			boolean isExist = false;
			for (JecnTempletT oldTmpT : oldJecnTempletT) {
				if (newTmpT.getFileId().toString().equals(oldTmpT.getFileId().toString())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}

}
