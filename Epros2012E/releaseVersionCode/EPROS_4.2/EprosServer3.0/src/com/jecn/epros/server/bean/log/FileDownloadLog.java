package com.jecn.epros.server.bean.log;

import java.io.Serializable;
import java.util.Date;

public class FileDownloadLog implements Serializable {
	private Long id;
	private Long fileId;
	private Long peopleId;
	private Date createDate;
	/** 0：文件，1：制度 */
	private int loadType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getLoadType() {
		return loadType;
	}

	public void setLoadType(int loadType) {
		this.loadType = loadType;
	}

}
