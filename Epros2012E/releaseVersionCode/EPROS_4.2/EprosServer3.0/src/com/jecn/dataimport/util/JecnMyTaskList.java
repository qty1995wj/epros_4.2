package com.jecn.dataimport.util;

public class JecnMyTaskList implements java.io.Serializable {
	private Long taskId;
	private String taskName;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/**
	 * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
	 */
	private int taskStage;
	/**
	 * 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
	 * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）
	 */
	private int taskElseState;// 1是驳回重新提交状态,2是评审意见整理状态
	private String isLock;// 0是锁定,1解锁
	private String isFileTask;// 1流程任务,2上传文件任务,3更新文件任务,4制度任务
	private String creater;// 任务创建人
	private String createName;// 任务创建人名称

	private String isTaskChecker;// 自己是否为任务审批人,或为自己创建的在重新提交或整理评审的任务.0为否
	private int taskType;// 0流程任务，1文件任务，2制度任务
	/** 关联ID */
	private Long rid;
	/** 关联名称 */
	private String rName;
	/** 审批阶段名称 */
	private String stageName;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getTaskElseState() {
		return taskElseState;
	}

	public void setTaskElseState(int taskElseState) {
		this.taskElseState = taskElseState;
	}

	public String getIsLock() {
		return isLock;
	}

	public void setIsLock(String isLock) {
		this.isLock = isLock;
	}

	public String getIsFileTask() {
		return isFileTask;
	}

	public void setIsFileTask(String isFileTask) {
		this.isFileTask = isFileTask;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getIsTaskChecker() {
		return isTaskChecker;
	}

	public void setIsTaskChecker(String isTaskChecker) {
		this.isTaskChecker = isTaskChecker;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public Long getRid() {
		return rid;
	}

	public void setRid(Long rid) {
		this.rid = rid;
	}

	public String getRName() {
		return rName;
	}

	public void setRName(String name) {
		rName = name;
	}

	public int getTaskStage() {
		return taskStage;
	}

	public void setTaskStage(int taskStage) {
		this.taskStage = taskStage;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}
}
