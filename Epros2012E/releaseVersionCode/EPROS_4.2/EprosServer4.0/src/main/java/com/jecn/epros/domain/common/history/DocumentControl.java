package com.jecn.epros.domain.common.history;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;

import java.util.Date;

/**
 * 发布流程、制度版本信息记录
 *
 * @author ZHANGXIAOHU
 */
public class DocumentControl {
    private int id;
    /**
     * 拟稿人
     */
    private String draftPerson;
    /**
     * 版本
     */
    private String version;
    /**
     * 发布日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date publishDate;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private String endTime;
    /**
     * 返工次数
     */
    private int approveCount;
    /**
     * 变更说明
     */
    private String modifyExplain;
    /**
     * 实施日期
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date implementDate;


    private boolean showImplDate;

    private int taskId;
    /*判断是否文控表中是否存在记录   1存在 -1 不存在*/
    private int JudgmentExistenceHistory;

    public int getJudgmentExistenceHistory() {
        return JudgmentExistenceHistory;
    }

    public void setJudgmentExistenceHistory(int judgmentExistenceHistory) {
        JudgmentExistenceHistory = judgmentExistenceHistory;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public boolean isShowImplDate() {
        showImplDate = !ConfigUtils.isHiddenHistoryDetailDate();
        return showImplDate;
    }

    public Date getImplementDate() {
        return implementDate;
    }

    public void setImplementDate(Date implementDate) {
        this.implementDate = implementDate;
    }

    public LinkResource getLink() {
        return new LinkResource(id, id + "/" + draftPerson, ResourceType.HISTORY);
    }

    @JsonIgnore
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public String getDraftPerson() {
        return draftPerson;
    }

    public void setDraftPerson(String draftPerson) {
        this.draftPerson = draftPerson;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getApproveCount() {
        return approveCount;
    }

    public void setApproveCount(int approveCount) {
        this.approveCount = approveCount;
    }

    public String getModifyExplain() {
        return Common.replaceWarp(modifyExplain);
    }

    public void setModifyExplain(String modifyExplain) {
        this.modifyExplain = modifyExplain;
    }
}
