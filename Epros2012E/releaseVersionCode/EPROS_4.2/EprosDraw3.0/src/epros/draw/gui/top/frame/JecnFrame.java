package epros.draw.gui.top.frame;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import epros.draw.gui.top.frame.listener.JecnFrameListener;

/**
 * 
 * 
 * 自定义窗体
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFrame extends JFrame {
	/** 窗体标题头 */
	private JecnFrameHeaderPanel frameHeaderPanel = null;
	/** 窗体内容面板 */
	private JecnFrameResizeContentPanel frameResizeContentPanel = null;
	/** 窗体内容面板内的提供给用户使用的面板 */
	private JPanel currContent = null;
	/** 用户给定面板 */
	private Container userContainer = null;

	/** 窗体监听对象 */
	private Map<JecnFrameListener, Object> frameListenerList = null;

	/** 当前大小，点击全屏后再点击后能回到此变量记录的大小 */
	private Rectangle normalRect = null;

	/** 是否最小化状态：true：是；false:不是 */
	private boolean isMinState = false;
	/** 是否为全屏：true：全屏 false：默认大小 */
	private boolean jecnMaxFlag = true;

	public JecnFrame() throws HeadlessException {
		super();
		initComponents();
	}

	public JecnFrame(GraphicsConfiguration gc) {
		super(gc);
		initComponents();
	}

	public JecnFrame(String title) throws HeadlessException {
		super(title);
		initComponents();
	}

	public JecnFrame(String title, GraphicsConfiguration gc) {
		super(title, gc);
		initComponents();
	}

	protected void initChildComponents() {

	}

	private void initComponents() {
		initChildComponents();
		// 窗体标题头
		frameHeaderPanel = new JecnFrameHeaderPanel(this);
		// 窗体内容面板
		frameResizeContentPanel = new JecnFrameResizeContentPanel(this);
		// 窗体内容面板内的提供给用户使用的面板
		currContent = new JPanel();
		// 边框
		frameResizeContentPanel.setBorder(new JecnFrameBorder());
		// 布局
		frameResizeContentPanel.setLayout(new BorderLayout());
		frameResizeContentPanel.add(frameHeaderPanel, BorderLayout.NORTH);

		currContent.setOpaque(false);
		currContent.setBorder(null);
		currContent.setLayout(new BorderLayout());
		frameResizeContentPanel.add(currContent, BorderLayout.CENTER);

		// 设置标题
		setTitle(getTitle());
		// 设置内容面板
		setContentPane(frameResizeContentPanel);
		// 图片
		setIconImage(getIconImage());
		// 禁用此窗体的装饰
		this.setUndecorated(true);
		// 设置此窗体是否可由用户调整大小
		this.setResizable(true);

		// 添加此事件为了最小化时，不提示自动保存对话框
		this.addWindowListener(new WindowAdapter() {
			public void windowIconified(WindowEvent e) {// 窗口正常->最小化
				isMinState = true;
			}

			public void windowDeiconified(WindowEvent e) {// 窗口最小化->正常
				isMinState = false;
			}
		});

	}

	/**
	 * 
	 * 不支持拖动且最大按钮隐藏
	 * 
	 * @param resizable
	 *            true：可改变窗体大小 false：不可改变窗体大小
	 */
	public void setResizable(boolean resizable) {
		super.setResizable(false);
		if (frameHeaderPanel != null) {
			frameResizeContentPanel.setCursor(Cursor.getDefaultCursor());
			if (frameHeaderPanel.getFrameTitlePanel() != null) {
				frameHeaderPanel.getFrameTitlePanel().setResizable(resizable);
				frameHeaderPanel.getToolTitlePanel().setResizable(resizable);
			}
		}

	}

	public boolean isResizable() {
		if (frameHeaderPanel != null) {
			if (frameHeaderPanel.getFrameTitlePanel() != null) {
				return frameHeaderPanel.getFrameTitlePanel().isResizable();
			}
		}
		return false;
	}

	/**
	 * 
	 * 是否是放大状态
	 * 
	 * @return
	 */
	public boolean isMaxFlag() {
		if (frameHeaderPanel != null) {
			if (frameHeaderPanel.getFrameTitlePanel() != null) {
				return frameHeaderPanel.getFrameTitlePanel().isMaxFlag();
			}
		}
		return false;
	}

	/**
	 * 
	 * 禁用或启用此窗体的装饰
	 * 
	 */
	public void setUndecorated(boolean undecorated) {
		super.setUndecorated(true);
	}

	/**
	 * 
	 * 添加标题
	 * 
	 */
	public void setTitle(String title) {
		super.setTitle(title);

		// 往自定义的窗体标题面板中添加标题
		if (frameHeaderPanel != null && frameHeaderPanel.getFrameTitlePanel() != null) {
			frameHeaderPanel.getFrameTitlePanel().setTitle(title);
		}
	}

	public Container getContentPane() {
		return currContent;
	}

	/**
	 * 
	 * 设置 contentPane 属性
	 * 
	 */
	public void setContentPane(Container contentPane) {
		if (contentPane == null) {
			return;
		} else if (frameResizeContentPanel == contentPane) {
			super.setContentPane(contentPane);
		} else {
			if (userContainer != null) {
				currContent.remove(userContainer);
			}
			userContainer = contentPane;
			currContent.add(userContainer, BorderLayout.CENTER);
		}
	}

	public Image getIconImage() {
		if (frameHeaderPanel != null && frameHeaderPanel.getFrameTitlePanel() != null) {
			return frameHeaderPanel.getFrameTitlePanel().getIcon();
		}
		return null;
	}

	public void setIconImage(Image image) {
		super.setIconImage(image);

		// 往自定义的窗体标题面板中添加标题
		if (frameHeaderPanel != null && frameHeaderPanel.getFrameTitlePanel() != null) {
			frameHeaderPanel.getFrameTitlePanel().setIcon(image);
		}
	}

	/**
	 * 
	 * 获取工具栏标题容器
	 * 
	 * @return
	 */
	public JecnFrameDropTitlePanel getToolTitlePanel() {
		return frameHeaderPanel.getToolTitlePanel();
	}

	public JecnFrameHeaderPanel getFrameHeaderPanel() {
		return frameHeaderPanel;
	}

	public JecnFrameDropTitlePanel getFreePanel() {
		return frameHeaderPanel.getFreePanel();
	}

	/**
	 * 
	 * 添加工具栏标题面板到左下角容器
	 * 
	 * @param comp
	 *            Component 工具栏标题面板
	 */
	public void addToolTitlePanel(Component comp) {
		this.getToolTitlePanel().add(comp);
	}

	/**
	 * 
	 * 添加组件到右下角容器
	 * 
	 * @param comp
	 *            Component 组件
	 */
	public void addRightBottomPanel(Component comp) {
		this.getFreePanel().add(comp);
	}

	/**
	 * 
	 * 全屏
	 * 
	 */
	public void setMaxFrameBounds() {
		getToolTitlePanel().setMaxFrameBounds();
	}

	public void setVisible(boolean b) {
		if (!b
				&& (JFrame.DISPOSE_ON_CLOSE == getDefaultCloseOperation() || JFrame.EXIT_ON_CLOSE == getDefaultCloseOperation())) {// 关闭、隐藏销毁对话框时
			frameHeaderPanel = null;
			frameResizeContentPanel = null;
			currContent = null;
			frameListenerList = null;
		}
		super.setVisible(b);
	}

	/**
	 * 
	 * 添加组件到窗体空闲区域
	 * 
	 * @param comp
	 *            JComponent 组件
	 * 
	 */
	public void addCompToFreePanel(JComponent comp) {
		frameHeaderPanel.addCompToFreePanel(comp);
	}

	/**
	 * 
	 * 添加监听
	 * 
	 * @param frameListener
	 */
	public void addJecnFrameListener(JecnFrameListener frameListener, Object obj) {
		if (frameListener == null || obj == null) {
			return;
		}
		if (frameListenerList == null) {
			frameListenerList = new HashMap<JecnFrameListener, Object>();
		}
		if (!frameListenerList.keySet().contains(frameListener)) {
			frameListenerList.put(frameListener, obj);
		}
	}

	Map<JecnFrameListener, Object> getFrameListenerList() {
		return frameListenerList;
	}

	Rectangle getNormalRect() {
		return normalRect;
	}

	void setNormalRect(Rectangle normalRect) {
		this.normalRect = normalRect;
	}

	public boolean isMinState() {
		return isMinState;
	}

	Container getCurrContent() {
		return currContent;
	}

	boolean isJecnMaxFlag() {
		return jecnMaxFlag;
	}

	void setJecnMaxFlag(boolean jecnMaxFlag) {
		this.jecnMaxFlag = jecnMaxFlag;
	}

}
