package epros.draw.system.data;

import epros.draw.constant.JecnToolbarConstant.TooBarTitleNumEnum;
import epros.draw.constant.JecnWorkflowConstant.EasyProcessType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.top.frame.JecnFrame;
import epros.draw.util.DrawCommon;

/**
 * 
 * 全局状态记录类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSystemStaticData {

	/** 画图面板显示网格：true显示；false：隐藏 默认显示 */
	private static boolean showGrid = true;
	/** 画图面板显示横分割线：true:显示；false:隐藏 默认显示 */
	private static boolean showParallelLines = true;
	/** 画图面板显示竖分割线：true:显示；false:隐藏 默认显示 */
	private static boolean showVerticalLine = true;
	/** 角色移动：true：显示；false：隐藏 默认隐藏 */
	private static boolean showRoleMove = false;
	/** 画图面板滚动面板的行竖表头是否显示：true：显示；false：隐藏 默认隐藏 */
	private static boolean workflowTitleShow = false;

	/** 画图面板放大缩小倍数最小有效值 */
	private static double minWorkflowSpace = 0.25;
	/** 画图面板放大缩小倍数最大有效值 */
	private static double maxWorkflowSpace = 4.0;

	/** 角色移动最大层级数 */
	private static int roleMaxLayer = 30000;

	/** Epros软件类型 默认是画图工具 */
	private static EprosType eprosType = EprosType.eprosDraw;
	/** 画图工具软件类型 */
	private static EasyProcessType easyProcessType = EasyProcessType.easyProcessDrawer;
	/** 工具栏标题个数 */
	private static TooBarTitleNumEnum tooBarTitleNumEnum = TooBarTitleNumEnum.five;

	/** 画图面板：图形右边距 */
	private static int workflowResizeWidth = 100;
	/** 画图面板：图形下边距 */
	private static int workflowResizeHeight = 100;

	/** 主窗体 */
	private static JecnFrame frame = null;

	/** 流程元素编辑点: 1 :左上进右下出（默认）；0 四个编辑点都能进出 ;2左进右出 */
	private static int editPortType = 0;

	/** logo公司名称 */
	private static String compName = "";
	/** logo图片是否显示在操作说明页眉上，显示true；隐藏false */
	private static boolean isShowLogoIcon = false;
	/** 是否显示活动说明中输入输出项，显示true；隐藏false */
	private static boolean isShowActiveIO = false;
	/** 默认勾选 合并泳池 */
	private static boolean isShowModel = false;
	
	/** 默认不勾选 隐藏编辑点 */
	private static boolean isHiddenLineEditPoint = false;

	public static int getRoleMaxLayer() {
		return roleMaxLayer;
	}

	public static boolean isShowGrid() {
		return showGrid;
	}

	public static void setShowGrid(boolean showGrid) {
		// 设置是否显示 流程图 背景网格线 到配置文件 实现本地永久化配置
		if (showGrid != JecnSystemStaticData.showGrid) {
			JecnDesignerProcess.getDesignerProcess().setGridState(showGrid);
		}
		JecnSystemStaticData.showGrid = showGrid;
	}

	public static boolean isShowParallelLines() {
		return showParallelLines;
	}

	public static void setShowParallelLines(boolean showParallelLines) {
		JecnSystemStaticData.showParallelLines = showParallelLines;
	}

	public static boolean isShowVerticalLine() {
		return showVerticalLine;
	}

	public static void setShowVerticalLine(boolean showVerticalLine) {
		JecnSystemStaticData.showVerticalLine = showVerticalLine;
	}

	public static double getMinWorkflowSpace() {
		return minWorkflowSpace;
	}

	public static void setMinWorkflowSpace(double minWorkflowSpace) {
		JecnSystemStaticData.minWorkflowSpace = minWorkflowSpace;
	}

	public static double getMaxWorkflowSpace() {
		return maxWorkflowSpace;
	}

	public static void setMaxWorkflowSpace(double maxWorkflowSpace) {
		JecnSystemStaticData.maxWorkflowSpace = maxWorkflowSpace;
	}

	public static boolean isWorkflowTitleShow() {
		return workflowTitleShow;
	}

	public static void setWorkflowTitleShow(boolean workflowTitleShow) {
		JecnSystemStaticData.workflowTitleShow = workflowTitleShow;
	}

	public static boolean isShowRoleMove() {
		return showRoleMove;
	}

	public static void setShowRoleMove(boolean showRoleMove) {
		JecnSystemStaticData.showRoleMove = showRoleMove;
	}

	public static EprosType getEprosType() {
		return eprosType;
	}

	public static EasyProcessType getEasyProcessType() {
		return easyProcessType;
	}

	public static void setEasyProcessType(EasyProcessType easyProcessType) {
		JecnSystemStaticData.easyProcessType = easyProcessType;
	}

	public static TooBarTitleNumEnum getTooBarTitleNumEnum() {
		return tooBarTitleNumEnum;
	}

	public static void setTooBarTitleNumEnum(TooBarTitleNumEnum tooBarTitleNumEnum) {
		JecnSystemStaticData.tooBarTitleNumEnum = tooBarTitleNumEnum;
	}

	/**
	 * 
	 * 是否是设计器
	 * 
	 * @return boolean true是设计器
	 */
	public static boolean isEprosDesigner() {
		return (EprosType.eprosDesigner == eprosType) ? true : false;
	}

	public static void setEprosType(EprosType eprosType) {
		if (eprosType == null) {
			return;
		}
		JecnSystemStaticData.eprosType = eprosType;
	}

	public static JecnFrame getFrame() {
		return frame;
	}

	public static void setFrame(JecnFrame frame) {
		JecnSystemStaticData.frame = frame;
	}

	public static int getWorkflowResizeWidth() {
		return workflowResizeWidth;
	}

	public static void setWorkflowResizeWidth(int workflowResizeWidth) {
		JecnSystemStaticData.workflowResizeWidth = workflowResizeWidth;
	}

	public static int getWorkflowResizeHeight() {
		return workflowResizeHeight;
	}

	public static void setWorkflowResizeHeight(int workflowResizeHeight) {
		JecnSystemStaticData.workflowResizeHeight = workflowResizeHeight;
	}

	public static int isEditPortType() {
		return editPortType;
	}

	public static void setEditPortType(int editPortType) {
		JecnSystemStaticData.editPortType = editPortType;
	}

	public static boolean isShowActiveIO() {
		return isShowActiveIO;
	}

	public static void setShowActiveIO(boolean isShowActiveIO) {
		JecnSystemStaticData.isShowActiveIO = isShowActiveIO;
	}

	public static String getCompName() {
		return compName;
	}

	public static void setCompName(String compName) {
		if (DrawCommon.isNullOrEmtryTrim(compName)) {
			JecnSystemStaticData.compName = "";
		} else {
			JecnSystemStaticData.compName = compName.trim();
		}
	}

	public static boolean isShowLogoIcon() {
		return isShowLogoIcon;
	}

	public static void setShowLogoIcon(boolean isShowLogoIcon) {
		JecnSystemStaticData.isShowLogoIcon = isShowLogoIcon;
	}

	public static boolean isShowModel() {
		return isShowModel;
	}

	public static void setShowModel(boolean isShowModel) {
		JecnSystemStaticData.isShowModel = isShowModel;
	}

	public static boolean isHiddenLineEditPoint() {
		return isHiddenLineEditPoint;
	}

	public static void setHiddenLineEditPoint(boolean isHiddenLineEditPoint) {
		JecnSystemStaticData.isHiddenLineEditPoint = isHiddenLineEditPoint;
	}
	
	

}
