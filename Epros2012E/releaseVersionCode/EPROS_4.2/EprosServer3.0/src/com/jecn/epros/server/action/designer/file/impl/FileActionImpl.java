package com.jecn.epros.server.action.designer.file.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.action.designer.file.IFileAction;
import com.jecn.epros.server.bean.file.FileAttributeBean;
import com.jecn.epros.server.bean.file.FileInfoBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileUseInfoBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class FileActionImpl implements IFileAction {

	private IFileService fileService;

	public IFileService getFileService() {
		return fileService;
	}

	public void setFileService(IFileService fileService) {
		this.fileService = fileService;
	}

	@Override
	public long addFileDir(JecnFileBeanT fileDir) throws Exception {
		return fileService.addFileDir(fileDir);
	}

	@Override
	public void reFileDirName(String name, long id, long updatePersonId) throws Exception {
		fileService.reFileDirName(name, id, updatePersonId);
	}

	@Override
	public void addFiles(List<JecnFileBeanT> fileList, AccessId accId, boolean isPub, int codeTotal) throws Exception {
		fileService.addFiles(fileList, accId, isPub, codeTotal);
	}

	@Override
	public List<JecnTreeBean> getChildFiles(long pId, long projectId) throws Exception {
		return fileService.getChildFiles(pId, projectId);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, long peopleId) throws Exception {
		return fileService.getRoleAuthChildFiles(pId, projectId, peopleId);
	}

	@Override
	public List<JecnTreeBean> getChildFileDirs(long pId, long projectId) throws Exception {
		return fileService.getChildFileDirs(pId, projectId);
	}

	@Override
	public List<JecnTreeBean> getAllFiles(long projectId) throws Exception {
		return fileService.getAllFiles(projectId);
	}

	@Override
	public List<JecnTreeBean> getAllFileDirs(long projectId) throws Exception {
		return fileService.getAllFileDirs(projectId);
	}

	@Override
	public List<JecnFileBeanT> getFilesByName(String name, long projectId, Long peopelId) throws Exception {
		if (peopelId == null) {
			return fileService.getFilesByName(name, projectId);
		} else {
			return fileService.searchRoleAuthByName(name, projectId, peopelId);
		}
	}

	@Override
	public void updateSortFiles(List<JecnTreeDragBean> list, long pId, long updatePersonId) throws Exception {
		fileService.updateSortFiles(list, pId, updatePersonId);
	}

	@Override
	public void moveFiles(List<Long> listIds, long pId, long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		fileService.moveFiles(listIds, pId, updatePersonId, moveNodeType);
	}

	@Override
	public FileInfoBean getFileBean(long id) throws Exception {
		return fileService.getFileBean(id, false);
	}

	/**
	 * 
	 * @author yxw 2012-5-29
	 * @description:文件删除
	 * @param listIds
	 * @return int 0 删除异常，1删除成功；2：存在节点处于任务中
	 * @throws Exception
	 */
	@Override
	public void deleteFiles(List<Long> listIds, long projectId, long updatePersonId) throws Exception {
		fileService.deleteFiles(listIds, projectId, updatePersonId);
	}

	@Override
	public void updateFile(JecnFileBeanT fileBean, long updatePersonId, boolean isPub, boolean isDesign)
			throws Exception {
		fileService.updateFile(fileBean, updatePersonId, isPub, isDesign);
	}

	@Override
	public FileOpenBean openFile(long id) throws Exception {
		return fileService.openFile(id, false);
	}

	@Override
	public void updateRecovery(long fileId, long versionId, long updatePersonId, boolean isPub, boolean isDesign)
			throws Exception {
		fileService.updateRecovery(fileId, versionId, updatePersonId, isPub, isDesign);

	}

	@Override
	public String[] validateAddName(List<String> names, long pId, int fileType) throws Exception {
		return fileService.validateAddName(names, pId, fileType);
	}

	@Override
	public String[] validateNamefullPath(List<String> names, long pId, int fileType) throws Exception {
		return fileService.validateNamefullPath(names, pId, fileType);
	}

	@Override
	public boolean validateUpdateName(String name, long id, long pId) throws Exception {
		return fileService.validateUpdateName(name, id, pId);
	}

	@Override
	public void deleteVersion(List<Long> ids, long fileId, long updatePersonId) throws Exception {
		fileService.deleteVersion(ids, fileId, updatePersonId);
	}

	@Override
	public JecnFileBeanT getFilBeanById(long id) throws Exception {
		return fileService.get(id);
	}

	@Override
	public FileOpenBean openVersion(long fileId, long versionId) throws Exception {
		return fileService.openVersion(fileId, versionId);
	}

	@Override
	public void updateFileProperty(JecnFileBeanT jecnFileBean, Set<Long> standardIds, Set<Long> riskIds, Long peopleId)
			throws Exception {
		fileService.updateFileProperty(jecnFileBean, standardIds, riskIds, peopleId);
	}

	@Override
	public List<JecnTreeBean> getJecnTreeBeanByName(String name, long projectId) throws Exception {
		return fileService.getJecnTreeBeanByName(name, projectId);
	}

	@Override
	public JecnTreeBean getJecnTreeBeanByIds(Long listIds, Long projectId) throws Exception {
		return fileService.getJecnTreeBeanByIds(listIds, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long fileId, Long projectId) throws Exception {
		return fileService.getPnodes(fileId, projectId);
	}

	@Override
	public boolean isFileAuth(Long fileId, Long projectId, Set<Long> setIds, TreeNodeType treeNodeType)
			throws Exception {
		return fileService.isFileAuth(fileId, projectId, setIds, treeNodeType);
	}

	@Override
	public boolean isFilesAuth(List<Long> fileIds, Long projectId, Set<Long> setIds) throws Exception {
		return fileService.isFilesAuth(fileIds, projectId, setIds);
	}

	@Override
	public void cancelRelease(Long id, Long peopleId) throws Exception {
		fileService.cancelRelease(id,peopleId);

	}

	/**
	 * 不记录文控发布
	 */
	@Override
	public void directRelease(Long id, Long peopleId) throws Exception {
		fileService.directRelease(id,peopleId);
	}

	@Override
	public List<JecnFileBeanT> getFileDirsByName(String name, Long projectId) throws Exception {
		return fileService.getFileDirsByName(name, projectId);
	}

	/**
	 * 验证当前选中记录是否为发布文件
	 * 
	 * @param fileId
	 *            文件ID
	 * @return true 为发布文件
	 * @throws Exception
	 */
	@Override
	public boolean isPublicFile(Long fileId) throws Exception {
		int count = fileService.isPubFileCount(fileId);
		if (count == 1) {// 存在一条记录，说明该文件已发布
			return true;
		}
		return false;
	}

	/**
	 * 验证当前选中记录是否为发布文件
	 * 
	 * @param fileId
	 *            文件ID
	 * @return true 存在已发布文件
	 * @throws Exception
	 */
	@Override
	public boolean isPublicFile(List<Long> fileIds) throws Exception {
		int count = fileService.isPubFileCount(fileIds);
		if (count > 0) {// ID集合存在已发布文件
			return true;
		}
		return false;
	}

	@Override
	public void updateHideFiles(Long folderId, int hideFlag) throws Exception {
		fileService.updateHideFiles(folderId, hideFlag);
	}

	@Override
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pId) throws Exception {
		return fileService.getRecycleJecnTreeBeanList(projectId, pId);
	}

	@Override
	public void updateRecycleFile(List<Long> ids) throws Exception {
		this.fileService.updateRecycleFile(ids);
	}

	@Override
	public void getRecycleFileIds(List<Long> listIds, Long projectId) throws Exception {
		this.fileService.updateRecycleFileOrDirIds(listIds, projectId);
	}

	@Override
	public void deleteFileManage(List<Long> listIds, Long projectId, Long updatePersonId, TreeNodeType treeNodeType)
			throws Exception {
		this.fileService.deleteFileManage(listIds, projectId, updatePersonId, treeNodeType);
	}

	@Override
	public List<JecnTreeBean> getAllFilesByName(String name, Long projectId, Set<Long> fileIds, boolean isAdmin)
			throws Exception {
		List<JecnTreeBean> list = fileService.getAllFilesByName(name, projectId, fileIds, isAdmin);
		return list;
	}

	@Override
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return fileService.getPnodesRecy(id, projectId);
	}

	@Override
	public int getMaxSortId(Long id) throws Exception {
		return fileService.getMaxSortId(id);
	}

	@Override
	public void batchReleaseNotRecord(List<Long> ids, Long peopleId) throws Exception {
		fileService.batchReleaseNotRecord(ids,  peopleId);

	}

	@Override
	public void batchCancelReleaseNotRecord(List<Long> ids, Long peopleId) throws Exception {
		fileService.batchCancelReleaseNotRecord(ids,  peopleId);
	}

	@Override
	public List<FileUseInfoBean> getFileUsages(Long id) throws Exception {
		return fileService.getFileUsages(id);
	}

	@Override
	public List<JecnFileContent> getJecnFileContents(Long id) throws Exception {
		return fileService.getJecnFileContents(id);
	}

	@Override
	public List<Long> getFileAuthByFlowAutoCreateDir(Map<Integer, Set<Long>> relatedsMap) throws Exception {
		return fileService.getFileAuthByFlowAutoCreateDir(relatedsMap);
	}

	@Override
	public List<JecnTreeBean> getJecnTreeBeanByIds(Set<Long> ids) throws Exception {
		return fileService.getJecnTreeBeanByIds(ids);
	}

	@Override
	public FileAttributeBean getFileAttributeBeanById(Long id) throws Exception {
		return fileService.getFileAttributeBeanById(id);
	}

	@Override
	public String getFlowNumByFileId(Long fileId) throws Exception {
		return fileService.getFlowNumByFileId(fileId);
	}

	@Override
	public void updateFileAttributeBeanById(FileAttributeBean fileAttributeBean, Long id, Long updatePersonId)
			throws Exception {
		fileService.updateFileAttributeBeanById(fileAttributeBean, id, updatePersonId);
	}

	@Override
	public Long getFileRelatedFlowId(Long fileId) throws Exception {
		return fileService.getFileRelatedFlowId(fileId);
	}

	@Override
	public boolean isNotAbolishOrNotDelete(Long id) throws Exception {
		return fileService.isNotAbolishOrNotDelete(id);
	}

	@Override
	public List<JecnDictionary> getFileTypeData() throws Exception {
		return fileService.getFileTypeData();
	}

	@Override
	public List<Long> getFileAuthByFlowAutoCreateDir(Set<Long> flowIds) throws Exception {
		return fileService.getFileAuthByFlowAutoCreateDir(flowIds);
	}

	@Override
	public void createFile(String filePath, byte[] changeFileToByte) throws IOException {
		fileService.createFile(filePath, changeFileToByte);
	}

	@Override
	public String saveFilePath(String flowHeadinfo, Date date, String suffixName) {
		return fileService.saveFilePath(flowHeadinfo, date, suffixName);
	}

	@Override
	public List<JecnFileBeanT> getMergeFilesByName(String name, Long projectId) {
		return fileService.getMergeFilesByName(name, projectId);
	}

	@Override
	public List<JecnFileBeanT> getFilesFullPath(List<Long> addIds) {
		return fileService.getFilesFullPath(addIds);
	}

	@Override
	public void merge(Long mainId, List<Long> mergeIds) {
		fileService.merge(mainId, mergeIds);
	}

	@Override
	public void updateViewSort(TreeNodeType treeNodeType, Long id) throws Exception {
		fileService.updateViewSort(treeNodeType, id);
	}

}
