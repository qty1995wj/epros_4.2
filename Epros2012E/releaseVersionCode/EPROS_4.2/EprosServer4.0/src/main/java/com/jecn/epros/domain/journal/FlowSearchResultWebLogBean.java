package com.jecn.epros.domain.journal;

import com.jecn.epros.domain.process.FlowSearchResultBean;

/**
 * Created by lenovo on 2017/1/5.
 */
public class FlowSearchResultWebLogBean extends FlowSearchResultBean {
    /**是否查阅 0是没有查阅，1是查阅，2是全部*/
    private int viewType;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
