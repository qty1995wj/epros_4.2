package epros.tray;


public class JecnTrayMain {
//
//	private static Logger log = Logger.getLogger(JecnTrayMain.class);
//
//	/** 退出 */
//	private MenuItem exitItem = null;
//	/** 启动 */
//	private MenuItem startItem = null;
//	/** 停止 */
//	private MenuItem stopItem = null;
//	/** 重新启动 */
//	private MenuItem restartItem = null;
//	/** 系统配置 */
//	private MenuItem sysSetItem = null;
//
//	/** 系统配置对话框 */
//	private SysSetDialog sysSetDialog = null;
//
//	/** 托盘主类对象 */
//	private static JecnTrayMain jecnTray = null;
//
//	private JecnTrayMain() {
//		initTray();
//		actionInit();
//	}
//
//	/**
//	 * 
//	 * 初始化托盘
//	 * 
//	 */
//	private void initTray() {
//		// 托盘
//		SystemTray tray = SystemTray.getSystemTray();
//
//		// 托盘右键弹出菜单
//		PopupMenu menu = new PopupMenu();
//		sysSetItem = new MenuItem(JecnProperties.getValue("systemSetLab"));// 系统设置
//		startItem = new MenuItem(JecnProperties.getValue("start"));// 启动
//		stopItem = new MenuItem(JecnProperties.getValue("stop"));// 停止
//		restartItem = new MenuItem(JecnProperties.getValue("reStart"));// 重新启动
//		exitItem = new MenuItem(JecnProperties.getValue("exitBut"));// 退出
//
//		menu.add(sysSetItem);
//		menu.add(startItem);
//		menu.add(stopItem);
//		menu.add(restartItem);
//		menu.add(exitItem);
//
//		// 托盘图标
//		ImageIcon image = new ImageIcon("images/icons/trayIcon.gif");
//		TrayIcon icon = new TrayIcon(image.getImage(), "EPROS", menu);
//
//		try {
//			tray.add(icon);
//		} catch (Exception e) {
//			log.error("", e);
//			JecnOptionPane.showMessageDialog(null,
//					JecnProperties.getValue("systemError"));// 系统错误！
//			System.exit(0);
//		}
//		icon.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseClicked(MouseEvent e) {
//				if (e.getButton() == MouseEvent.BUTTON3) {
//					if (JecnLoading.isDialogShow()) {
//						startItem.setLabel(JecnProperties.getValue("starting"));// 启动
//						sysSetItem.setEnabled(false);
//						startItem.setEnabled(false);
//						stopItem.setEnabled(false);
//						restartItem.setEnabled(false);
//						return;
//					}
//					// 判断epros软件是否已经存在
//					boolean ret = JecnCmd.cmdExec(
//							JecnTrayConstants.CMD_TASKLIST,
//							JecnTrayConstants.EPROS_NAME);
//					if (ret) {
//						startItem.setLabel(JecnProperties.getValue("started"));
//						sysSetItem.setEnabled(true);
//						startItem.setEnabled(false);
//						stopItem.setEnabled(true);
//						restartItem.setEnabled(true);
//					} else {
//						startItem.setLabel(JecnProperties.getValue("start"));// 启动
//						sysSetItem.setEnabled(true);
//						startItem.setEnabled(true);
//						stopItem.setEnabled(false);
//						restartItem.setEnabled(false);
//					}
//				}
//			}
//		});
//	}
//
//	/**
//	 * 
//	 * 初始化事件
//	 * 
//	 */
//	public void actionInit() {
//		exitItem.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				System.exit(0);
//			}
//		});
//		restartItem.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				reStartService();
//			}
//		});
//		stopItem.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				stopService();
//			}
//		});
//		startItem.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				TrayTask trayTask = new TrayTask();
//				JecnLoading.setShowText(JecnProperties.getValue("startingWait"));
//				JecnLoading.start(trayTask);
//			}
//		});
//		sysSetItem.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				if (sysSetDialog == null) {
//					if (!TrayUtil.loadDataBaseInfo(TrayUtil.getRootPath()
//							+ JecnTrayConstants.DB_CONFIG_FILE)) {// 数据库路径
//						JecnOptionPane.showMessageDialog(null,
//								JecnProperties.getValue("readSystemSetError"));
//						return;
//					}
//					sysSetDialog = new SysSetDialog();
//
//				}
//				// 给面板赋值
//				sysSetDialog.initData();
//				sysSetDialog.setVisible(true);
//			}
//		});
//	}
//
//	/**
//	 * @author yxw 2013-7-18
//	 * @description:服务启动
//	 */
//	private void startService() {
//		Connection conn = null;
//		ResultSet rs = null;
//		Statement st = null;
//		try {
//
//			this.startItem.setLabel(JecnProperties.getValue("starting"));
//			if (!TrayUtil.loadDataBaseInfo(TrayUtil.getRootPath()
//					+ JecnTrayConstants.DB_CONFIG_FILE)) {// 数据库路径
//				JecnOptionPane.showMessageDialog(null,
//						JecnProperties.getValue("readSystemSetError"));
//				this.startItem.setLabel(JecnProperties.getValue("starting"));
//				return;
//			}
//
//			// 验证RMI端口是否占用
//			if (!TrayUtil.checkPort(Integer.parseInt(TrayUtil.dataBaseBean
//					.getDataDesinPort()))) {
//				JecnOptionPane.showMessageDialog(null,
//						JecnProperties.getValue("designerProtC")
//								+ TrayUtil.dataBaseBean.getDataDesinPort()
//								+ JecnProperties.getValue("designerProtUsed"));
//				return;
//			}
//			// 验证数据库是否可以连接
//			conn = TrayUtil.getConnection();
//			if (conn == null) {
//				boolean ret = JecnCmd
//						.cmdExec(JecnTrayConstants.CMD_NET_START_ORACLE);
//				ret = JecnCmd
//						.cmdExec(JecnTrayConstants.CMD_NET_START_ORACLE_LISTENER);
//				long startTime = System.currentTimeMillis();
//				while (true) {
//					Thread.sleep(1000);
//					long nowTime = System.currentTimeMillis();
//					if ((nowTime - startTime) > 40000) {
//						break;
//					}
//					conn = TrayUtil.getConnection();
//					if (conn != null) {
//						st = conn.createStatement();
//						rs = st.executeQuery("select value from jecn_config_item where id=128");
//						String value = null;
//						while (rs.next()) {
//							value = rs.getString(1);
//							break;
//						}
//						if (value == null
//								|| !value.equals(JecnFileUtil.getSyncPath("/")
//										.substring(1, 4))) {
//							JecnLoading.stop();
//							JecnOptionPane
//									.showMessageDialog(
//											null,
//											JecnProperties
//													.getValue("initialiseDBerrorNoStartServer"));// 数据初始化失败，不能启动服务！
//							return;
//						}
//						break;
//					}
//				}
//			} else {
//				st = conn.createStatement();
//				rs = st.executeQuery("select value from jecn_config_item where id=128");
//				String value = null;
//				while (rs.next()) {
//					value = rs.getString(1);
//					break;
//				}
//				if (value == null
//						|| !value.equals(JecnFileUtil.getSyncPath("/")
//								.substring(1, 4))) {
//					JecnLoading.stop();
//					JecnOptionPane.showMessageDialog(null, JecnProperties
//							.getValue("initialiseDBerrorNoStartServer"));// 数据初始化失败，不能启动服务！
//					return;
//				}
//			}
//
//			// // 查看EprosServer3.0进程是否启动,若是启动，杀掉进程
//			// TrayUtil.killEprosServer();
//			// 读取tomcat端口,验证是否被占用，并自动修改端口
//			boolean portFree = TrayUtil.checkTomcatPort();
//			if (!portFree) {
//				JecnLoading.stop();
//				JecnOptionPane.showMessageDialog(null,
//						JecnProperties.getValue("GetPortError"));// 获取启动端口失败！
//				return;
//			}
//			boolean ret = JecnCmd
//					.cmdExec(JecnTrayConstants.CMD_NET_START_EPROSSERVER3);
//
//			if (!ret) {// 执行epros启动命令时失败
//				JecnLoading.stop();
//				JecnOptionPane.showMessageDialog(null,
//						JecnProperties.getValue("serverStartLose"));
//			}
//
//			// 连接浏览端
//			long startTime = System.currentTimeMillis();
//			while (true) {
//				Thread.sleep(1000);
//				long nowTime = System.currentTimeMillis();
//				if ((nowTime - startTime) > 40000) {
//					break;
//				}
//				try {
//					ConnectionPool.getProject().getListProject();
//					JecnLoading.setShowText(JecnProperties
//							.getValue("startSuccess"));// 启动成功！
//					Thread.sleep(300);
//					this.startItem.setLabel(JecnProperties.getValue("started"));// 已启动
//					this.startItem.setEnabled(false);
//					this.stopItem.setEnabled(true);
//					break;
//				} catch (Exception e) {
//					log.error("服务暂时没有启动", e);
//				}
//			}
//
//		} catch (Exception e) {
//			log.info("服务启动异常", e);
//			this.startItem.setLabel(JecnProperties.getValue("start"));
//			JecnOptionPane.showMessageDialog(null,
//					JecnProperties.getValue("serverStartLose"));
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException e) {
//				}
//			}
//			if (st != null) {
//				try {
//					st.close();
//				} catch (SQLException e) {
//				}
//			}
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//				}
//			}
//		}
//
//	}
//
//	/**
//	 * @author yxw 2013-7-18
//	 * @description:停止服务
//	 */
//	private boolean stopService() {
//		boolean ret = JecnCmd
//				.cmdExec(JecnTrayConstants.CMD_NET_STOP_EPROSSERVER3);
//		if (ret) {
//
//			startItem.setEnabled(true);
//			this.startItem.setLabel(JecnProperties.getValue("start"));
//			stopItem.setEnabled(false);
//			restartItem.setEnabled(false);
//		} else {
//			log.error("命令=" + JecnTrayConstants.CMD_NET_STOP_EPROSSERVER3
//					+ ";执行报错。");
//			JecnOptionPane.showMessageDialog(
//					null,
//					JecnProperties.getValue("serverStopError1")
//							+ JecnTrayConstants.EPROS_NAME
//							+ JecnProperties.getValue("serverStopError2"));
//		}
//		return ret;
//	}
//
//	/**
//	 * @author yxw 2013-8-13
//	 * @description:重新启动
//	 */
//	private void reStartService() {
//		if (stopService()) {
//			TrayTask trayTask = new TrayTask();
//			JecnLoading.setShowText(JecnProperties.getValue("startingWait"));// 正在启动，请稍等！
//			JecnLoading.start(trayTask);
//		}
//	}
//
//	/**
//	 * 
//	 * 创建JecnTrayMain对象
//	 * 
//	 */
//	private static void newInstance() {
//		if (jecnTray == null) {
//			jecnTray = new JecnTrayMain();
//		}
//	}
//
//	class TrayTask extends SwingWorker<Void, Void> {
//
//		@Override
//		protected Void doInBackground() throws Exception {
//			try {
//				startService();
//			} catch (Exception e) {
//				log.error("启动异常", e);
//				done();
//			}
//			return null;
//		}
//
//		@Override
//		public void done() {
//			JecnLoading.stop();
//		}
//	}
//
//	public static void main(String[] args) {
//
//		// 判断托盘是否启动多个，如果已经有启动托盘，此次启动退出
//		TrayUtil.reStart();
//
//		// 不支持系统托盘，退出主程序
//		if (!SystemTray.isSupported()) {
//			log.error("当前系统不支持系统托盘！");
//			JecnOptionPane.showMessageDialog(null,
//					JecnProperties.getValue("sysNoTray"));
//			System.exit(0);
//		}
//		// 1、 加载资源文件
//		JecnProperties.loadProperties();
//		// 外观
//		try {
//			UIManager
//					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//		} catch (Exception e) {
//			log.error("", e);
//		}
//
//		JecnTrayMain.newInstance();
//	}
}
