package com.jecn.epros.server.bean.process.through;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.util.JecnUtil;

public class ActivityInouts {
	/**
	 * 活动的inouts 0 流程id 1流程名称 2流程编号 3活动id 4活动名称 5活动编号 6输入输出id 7输入输出名称
	 */
	private List<Object[]> inouts;
	private IBaseDao baseDao;

	public ActivityInouts(IBaseDao baseDao, int type, Long... flowIds) {
		this.baseDao = baseDao;
		String condition = "";
		if (flowIds.length == 1) {
			condition = " = " + flowIds[0];
		} else {
			condition = " in " + JecnCommonSql.getIds(Arrays.asList(flowIds));
		}
		String sql = "select jfs.flow_id," + "       jfs.flow_name," + "       jfs.flow_id_input,"
				+ "       jfsi.figure_id," + "       jfsi.figure_text," + "       jfsi.activity_id,"
				+ "       jfio.id," + "       jfio.name" + "  from jecn_figure_in_out_t jfio"
				+ " inner join jecn_flow_structure_image_t jfsi"
				+ "    on jfio.figure_id = jfsi.figure_id and jfsi.figure_type in " + JecnCommonSql.getActiveString()
				+ " inner join jecn_flow_structure_t jfs                 "
				+ "    on jfsi.flow_id = jfs.flow_id and jfs.flow_id  " + condition + " where jfio.type=" + type
				+ " order by jfs.flow_id, jfsi.activity_id asc";
		List<Object[]> objs = baseDao.listNativeSql(sql);
		this.inouts = objs;
	}

	/**
	 * 获得相同名称输入输出的活动编号
	 * 
	 * @param name
	 * @return
	 */
	public List<String> getActivityNums(String name) {
		Set<String> result = new TreeSet<String>();
		for (Object[] str : inouts) {
			if (name.equals(JecnUtil.nullToEmpty(str[7]))) {
				result.add(JecnUtil.nullToEmpty(str[5]));
			}
		}
		return new ArrayList<String>(result);
	}

	public List<Object[]> getInouts() {
		return inouts;
	}
}
