package com.jecn.epros.domain.system;

import io.swagger.annotations.ApiParam;

/**
 * Created by user on 2017/5/18.
 */
public class DBSyncConfig {

    @ApiParam(value = "同步时间 (格式：13:01)")
    private String startTime;
    @ApiParam(value = "同步时间间隔天")
    private Integer intervalDay;
    @ApiParam(value = "是否自动同步 1自动，0非自动")
    private Integer isAuto;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Integer getIntervalDay() {
        return intervalDay;
    }

    public void setIntervalDay(Integer intervalDay) {
        this.intervalDay = intervalDay;
    }

    public Integer getIsAuto() {
        return isAuto;
    }

    public void setIsAuto(Integer isAuto) {
        this.isAuto = isAuto;
    }
}
