package com.jecn.epros.domain.user;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.org.SearchPosResultBean;

import java.util.List;

/**
 * Created by user on 2017/5/5.
 */
public class UserDetail {

    private JecnUser user;
    private List<SearchPosResultBean> orgAndPoss;

    public JecnUser getUser() {
        return user;
    }

    public void setUser(JecnUser user) {
        this.user = user;
    }

    public List<SearchPosResultBean> getOrgAndPoss() {
        return orgAndPoss;
    }

    public void setOrgAndPoss(List<SearchPosResultBean> orgAndPoss) {
        this.orgAndPoss = orgAndPoss;
    }
}
