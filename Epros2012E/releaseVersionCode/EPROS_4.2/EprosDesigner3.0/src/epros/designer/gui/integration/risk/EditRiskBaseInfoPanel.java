package epros.designer.gui.integration.risk;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.integration.MultiLineCellRenderer;
import epros.designer.gui.integration.internalControl.ControlGuideChooseDialog;
import epros.designer.gui.integration.table.RiskControlGuideTable;
import epros.designer.gui.system.combo.JecnRiskTypeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/***
 * 风险点属性====风险基本信息 2013-11-05
 * 
 */
public class EditRiskBaseInfoPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(EditRiskBaseInfoPanel.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(450, 330);

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel(430, 200);

	/** 风险编号Lab */
	private JLabel riskCodeLab = new JLabel(JecnProperties.getValue("riskNumC"));

	/** 风险编号Field */
	protected JecnTextField riskCodeFiled = new JecnTextField(315, 20);

	/** 风险编号必填提示符 */
	private JLabel riskCodeRequired = new JLabel(JecnProperties.getValue("required"));

	/** 风险编号验证提示信息 */
	// protected JLabel riskCodeVerfy = new JLabel();

	/** 风险等级 */
	private JLabel gradeLab = new JLabel(JecnProperties.getValue("riskGradeC"));

	/** 风险等级 必填提示符 */
	private JLabel gradeRequired = new JLabel(JecnProperties.getValue("required"));

	/** 密级Field */
	protected JComboBox gradeCombox = new JComboBox(new String[] { "", JecnProperties.getValue("heighShow"),
			JecnProperties.getValue("middleShow"), JecnProperties.getValue("lowShow") });

	/** 风险点描述Lab */
	private JLabel riskDescLab = new JLabel(JecnProperties.getValue("riskPointDesC"));

	/** 风险点描述Area */
	protected JecnTextArea riskDescArea = new JecnTextArea();
	/** 风险点描述滚动面板 */
	private JScrollPane riskDesScrollPane = new JScrollPane(riskDescArea);

	/** 风险点描述必填提示符 */
	private JLabel riskDesRequired = new JLabel(JecnProperties.getValue("required"));

	/** 标准化控制Lab */
	private JLabel standdardControlLab = new JLabel(JecnProperties.getValue("standardControlC"));

	/** 标准化控制Area */
	protected JecnTextArea standdardControlArea = new JecnTextArea();
	/** 标准化控制滚动面板 */
	private JScrollPane standdardControlScrollPane = new JScrollPane(standdardControlArea);

	/** 内控指引知识库Lab */
	private JLabel controlGuideLab = new JLabel(JecnProperties.getValue("controlGuideC"));

	/** 内控指引知识库Area */
	// protected JecnTextArea controlGuideArea = new JecnTextArea(250, 40);
	protected RiskControlGuideTable controlGuideTable = null;
	/** 表格行高 */
	private int oldRowHeight;

	/** 内控指引知识库滚动面板 */
	private JScrollPane controlGuideScrollPane = null;
	/** 内控指引知识库ids */
	protected String controlGuideIds = null;
	/** 内控指引知识库Button */
	private JButton controlGuideBut = new JButton(JecnProperties.getValue("selectBtn"));
	private List<JecnTreeBean> listControlGuide = new ArrayList<JecnTreeBean>();

	private boolean isShow = false;
	/** 设置大小 */
	Dimension dimension = null;
	/** 风险类别 */
	private JecnRiskTypeCommon riskType;
	private JecnRisk jecnRisk;

	public EditRiskBaseInfoPanel(JecnTreeNode selectNode, JTree jTree, JecnRisk jecnRisk) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.jecnRisk = jecnRisk;

		this.setSize(650, 500);
		dimension = new Dimension(315, 15);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		riskCodeRequired.setForeground(Color.red);
		gradeRequired.setForeground(Color.red);
		riskDesRequired.setForeground(Color.red);

		// 获取内控指引知识库
		try {
			listControlGuide = ConnectionPool.getJecnRiskAction().getJecnRiskGuide(jecnRisk.getId());
		} catch (Exception e1) {
			log.error("EditRiskBaseInfoPanel is error", e1);
		}

		// 内控指引知识库
		controlGuideTable = new RiskControlGuideTable(listControlGuide);
		controlGuideScrollPane = new JScrollPane();
		controlGuideScrollPane.setViewportView(controlGuideTable);
		controlGuideScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 风险点描述
		riskDescArea.setBorder(null);
		riskDesScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		riskDesScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		riskDesScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 换行
		riskDescArea.setLineWrap(true);
		// 标准化控制
		standdardControlArea.setBorder(null);
		standdardControlScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		standdardControlScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		standdardControlScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 换行
		standdardControlArea.setLineWrap(true);

		// 风险编号显示
		this.riskCodeFiled.setText(jecnRisk.getRiskCode());
		// 风险等级
		this.gradeCombox.setSelectedIndex(jecnRisk.getGrade());
		// 风险点描述
		this.riskDescArea.setText(jecnRisk.getRiskName());
		// 标准化控制
		this.standdardControlArea.setText(jecnRisk.getStandardControl());
		// 内控指引知识库

		// 原始行高
		oldRowHeight = controlGuideTable.getRowHeight();

		// 责任部门选择
		controlGuideBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controlGuideButAction();
			}
		});
		controlGuideTable.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				controlGuideTablemouseReleased(e);
			}
		});
		initLayout();

	}

	/***
	 * 布局
	 */
	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(mainPanel, c);
		// 信息面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		int rows = 0;

		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));

		addRiskCode(rows, insets);
		rows++;

		// 风险点描述Lab
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(riskDescLab, c);
		// 风险点描述TextArea
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(riskDesScrollPane, c);
		// 风险点描述必填符号 *
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(riskDesRequired, c);
		rows++;

		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRiskType)) {
			riskType = new JecnRiskTypeCommon(rows, infoPanel, insets, jecnRisk.getRiskType() + "", null, false);
			rows++;
		}

		// 标准化控制Lab
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(standdardControlLab, c);
		// 标准化控制TextArea
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(standdardControlScrollPane, c);
		rows++;

		// 内控指引知识库Lab
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(controlGuideLab, c);
		// 内控指引知识库ScrollPane
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(controlGuideScrollPane, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(controlGuideBut, c);
		rows++;
	}

	private void addRiskCode(int rows, Insets insets) {
		JecnPanel riskCodePanel = new JecnPanel();
		// 风险点编号、风险等级提示信息
		GridBagConstraints c = new GridBagConstraints(0, rows, 4, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0);
		infoPanel.add(riskCodePanel, c);

		riskCodePanel.setLayout(new GridBagLayout());

		// 风险点编号Lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		riskCodePanel.add(riskCodeLab, c);
		// 风险点编号Filed
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(3, 25, 3, 0), 0, 0);
		riskCodePanel.add(riskCodeFiled, c);
		// 风险点编号*
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		riskCodePanel.add(riskCodeRequired, c);

		// 风险等级Lab
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		riskCodePanel.add(gradeLab, c);
		// 风险等级combox
		c = new GridBagConstraints(4, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		riskCodePanel.add(gradeCombox, c);

	}

	/***
	 * 选择内控指引
	 */
	private void controlGuideButAction() {
		ControlGuideChooseDialog controlGuideChooseDialog = new ControlGuideChooseDialog(listControlGuide);
		controlGuideChooseDialog.setVisible(true);
		// if (ChooseDialog.isOperation()) {
		if (listControlGuide.size() > 0) {
			controlGuideTable.remoeAll();
			for (JecnTreeBean o : listControlGuide) {
				if (controlGuideIds == null) {
					controlGuideIds = String.valueOf(o.getId());
				} else {
					controlGuideIds = controlGuideIds + "," + String.valueOf(o.getId());
				}

				Vector<String> v = new Vector<String>();
				v.add(String.valueOf(o.getId()));
				if (o.getContent() != null) {
					v.add(o.getName() + o.getContent());
				} else {
					v.add(o.getName());
				}
				// if (TreeNodeType.ruleFile.toString().equals(
				((DefaultTableModel) this.controlGuideTable.getModel()).addRow(v);
			}
		} else {
			// 清空表格
			((DefaultTableModel) controlGuideTable.getModel()).setRowCount(0);
			controlGuideIds = null;
		}
		// }

	}

	/***
	 * 选中table行，显示该行全部数据
	 */
	private void controlGuideTablemouseReleased(MouseEvent evt) {
		if (!isShow) {
			isShow = true;
		} else {
			isShow = false;
		}
		if (evt.getClickCount() == 1) {
			int row = controlGuideTable.getSelectedRow();
			int newRowHeight = controlGuideTable.getRowHeight(row);
			if (row != -1) {
				if (oldRowHeight >= newRowHeight) {
					controlGuideTable.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 1, 0));
					return;
				} else if (oldRowHeight < newRowHeight) {
					// 选中后高度设置
					if (newRowHeight != oldRowHeight) {
						controlGuideTable.setRowHeight(row, oldRowHeight);
						controlGuideTable.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 1, 1));
					}
				}
			}
		}
	}

	public String getControlGuideIds() {
		return controlGuideIds;
	}

	public void setControlGuideIds(String controlGuideIds) {
		this.controlGuideIds = controlGuideIds;
	}

	/**
	 * 关联，点击详情，隐藏按钮
	 * 
	 */
	public void hiddenComps() {
		riskCodeFiled.setEditable(false);
		gradeCombox.setEnabled(false);
		riskDescArea.setEditable(false);
		standdardControlArea.setEditable(false);
		controlGuideBut.setVisible(false);
	}

	public JecnRiskTypeCommon getRiskType() {
		return riskType;
	}

}
