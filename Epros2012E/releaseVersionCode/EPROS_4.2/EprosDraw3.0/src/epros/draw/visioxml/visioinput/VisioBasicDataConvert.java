package epros.draw.visioxml.visioinput;

import java.awt.Point;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import visioxml.bean.CharProperty;
import visioxml.bean.ConnectProperty;
import visioxml.bean.FillProperty;
import visioxml.bean.LineProperty;
import visioxml.bean.ParaProperty;
import visioxml.bean.TextBlockProperty;
import visioxml.bean.XForm1DProperty;
import visioxml.bean.XFormProperty;
import visioxml.constant.Constants.FillPattern;
import visioxml.constant.Constants.FontVertical;
import visioxml.constant.Constants.LineRoute;
import visioxml.util.FileUtil;
import visioxml.util.Tools;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.util.DrawCommon;

/**
 * visio原始数据转换为epros中间数据
 * 
 * @author admin
 * 
 */
class VisioBasicDataConvert {
	private static final Log log = LogFactory.getLog(VisioBasicDataConvert.class);
	/** visio导入关系配置 key=nameU,value=figueType */
	private Map<String, String> configMap;
	private EPROSMiddleData eprosMiddleData;
	private String path;
	/** visio图形和epros图形的关系集合 key=visioFigueID,value=eprosFigueID */
	private Map<String, String> relationMap;
	/*** visio图形ID单个图形属性 key=visiofigueID,value=VisioShape ****/
	private Map<String, VisioShape> visioShape;

	public VisioBasicDataConvert(String allFullPath) {
		this.path = allFullPath;
		eprosMiddleData = new EPROSMiddleData();
		configMap = FileUtil.getProcessInPutVisioModeConfig();
	}

	public EPROSMiddleData read() {
		VisioBasicData visioData = VisioBasicReader.getInstance().readXML(FileUtil.getDocumentFile(path), configMap);
		// 转换页面
		converPage(visioData.getPanelWidth(), visioData.getPanelHeight());
		// 转换横竖分割线
		converHVDashLine(visioData.getvHLine());
		// 转换横线 竖线 分割线
		converdeDivedingLine(visioData.getDivedingLine());
		// 转换图形
		converShape(visioData.getShapeList());
		// 动态连接线
		converManLine(visioData.getManLine(), visioData.getBegConn(), visioData.getEndConn());
		return eprosMiddleData;
	}

	/**
	 * 动态连接线
	 * 
	 * @param manLine
	 * @param endConn
	 * @param begConn
	 */
	private void converManLine(List<VisioShape> manLine, Map<String, ConnectProperty> begConn,
			Map<String, ConnectProperty> endConn) {
		if (manLine == null || manLine.size() == 0) {
			return;
		}
		List<JecnManhattanLineData> manLines = new ArrayList<JecnManhattanLineData>();
		JecnManhattanLineData manhattanLineData = null;
		for (VisioShape shape : manLine) {
			String figureType = getFigureType(shape.getNameU());
			// 获取连接线的数据对象
			manhattanLineData = new JecnManhattanLineData(MapElemType.valueOf(figureType));
			ConnectProperty beg = begConn.get(shape.getId());
			ConnectProperty end = endConn.get(shape.getId());
			if (beg == null) {
				log.error("动态连接线ID=" + shape.getId() + ",没有找到开始连接图形");
				continue;
			}
			if (end == null) {
				log.error("动态连接线ID=" + shape.getId() + ",没有找到结束连接图形");
				continue;
			}
			// 设置连接线的开始连接图形
			manhattanLineData.setStartFigureUUID(getFigureUUID(beg.getToSheet() + ""));
			// 设置连接线的结束连接图形
			manhattanLineData.setEndFigureUUID(getFigureUUID(end.getToSheet() + ""));
			// 此处特殊处理visio直接导入的流程图连接线取交点
			// 获取 起点和终点的数据
			// 获取连接的图形的数据（4个点的位置（上，下，左，右））
			// 起点和起始图形的四个点进行匹配 相等的就是连接点
			if (beg.getLineRoute() == null || end.getLineRoute() == null) {
				double begx = shape.getXfrom1D().getBeginX();
				double begY = shape.getXfrom1D().getBeginY();
				double endx = shape.getXfrom1D().getEndX();
				double endY = shape.getXfrom1D().getEndY();
				if (beg.getLineRoute() == null) {
					VisioShape shapeV = getVisioShape(beg.getToSheet() + "");
					LineRoute lineRoute = getlineRoute(begx, begY, shapeV.getXfrom());
					beg.setLineRoute(lineRoute);
				}
				if (end.getLineRoute() == null) {
					VisioShape shapeV = getVisioShape(end.getToSheet() + "");
					LineRoute lineRoute = getlineRoute(endx, endY, shapeV.getXfrom());
					end.setLineRoute(lineRoute);
				}
				if (beg.getLineRoute() == null || end.getLineRoute() == null) {
					continue;
				}
			}
			// 设置连接线开始连接点
			manhattanLineData.setStartEditPointType(VisioInputTools.converLineRoute(beg.getLineRoute()));
			// 设置连接点结束连接点
			manhattanLineData.setEndEditPointType(VisioInputTools.converLineRoute(end.getLineRoute()));
			setGeneralFiguleAttr(manhattanLineData, shape);
			manLines.add(manhattanLineData);
		}
		eprosMiddleData.setManLines(manLines);
	}

	/**
	 * 
	 * @param xfrom1D
	 * @param xfrom
	 * @return
	 */
	private LineRoute getlineRoute(double x, double y, XFormProperty xfrom) {
		double pinX = xfrom.getPinX();
		double pinY = xfrom.getPinY();
		double width = xfrom.getWidth();
		double height = xfrom.getHeight();
		// top x=pinx,pinY+(hright/2)
		// bottom x=pinx,pinY-(hright/2)；
		// left x=pinX-(width/2),y=pinY;
		// right x=pinX+(width/2),y=pinY
		double topX = pinX;
		double topY = pinY + (height / 2);
		double bottomX = pinX;
		double bottomY = pinY - (height / 2);
		double leftX = pinX - (width / 2);
		double leftY = pinY;
		double rightX = pinX + (width / 2);
		double rightY = pinY;
		DecimalFormat df = new DecimalFormat("######0.00");
		x = Double.valueOf(df.format(x));
		y = Double.valueOf(df.format(y));
		pinX = Double.valueOf(df.format(pinX));
		pinY = Double.valueOf(df.format(pinY));
		topX = Double.valueOf(df.format(topX));
		topY = Double.valueOf(df.format(topY));
		leftX = Double.valueOf(df.format(leftX));
		leftY = Double.valueOf(df.format(leftY));
		rightX = Double.valueOf(df.format(rightX));
		rightY = Double.valueOf(df.format(rightY));
		bottomX = Double.valueOf(df.format(bottomX));
		bottomY = Double.valueOf(df.format(bottomY));

		// log.error("==================");
		// log.error("x==" + x + ":y==" + y);
		// // log.error("pinX==" + pinX + ":pinY==" + pinY);
		// log.error("topX==" + topX + ":topY==" + topY);
		// log.error("bottomX==" + bottomX + ":bottomY==" + bottomY);
		// log.error("leftX==" + leftX + ":leftY==" + leftY);
		// log.error("rightX==" + rightX + ":rightY==" + rightY);
		// log.error("width=" + width + ":height=" + height);
		if (x == topX && y == topY) {
			return LineRoute.TOP;
		} else if (x == bottomX && y == bottomY) {
			return LineRoute.BOTTOM;
		} else if (x == leftX && y == leftY) {
			return LineRoute.LEFT;
		} else if (x == rightX && y == rightY) {
			return LineRoute.RIGHT;
		}
		return null;
	}

	/**
	 * 图形
	 * 
	 * @param shapeList
	 */
	private void converShape(List<VisioShape> shapeList) {
		if (shapeList == null || shapeList.size() == 0) {
			return;
		}
		List<JecnFigureData> figureDataLists = new ArrayList<JecnFigureData>();
		for (VisioShape shape : shapeList) {
			// epros图形ID
			String flowElementUUID = DrawCommon.getUUID();
			// 添加visio图形和epros图形对应关系
			putRelationMap(shape.getId(), flowElementUUID);
			putVisioShape(shape.getId(), shape);
			String figureType = getFigureType(shape.getNameU());
			// 创建图形数据对象
			JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.valueOf(figureType));
			// 设置图形的UUID
			figureData.setFlowElementUUID(flowElementUUID);
			// 图形位置点
			XFormProperty xfrom = shape.getXfrom();
			// 图形宽度
			int width = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(xfrom.getWidth() + ""));
			// 图形高度
			int height = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(xfrom.getHeight() + ""));
			// 图形的X
			double pinX = VisioInputTools.changeX(xfrom.getPinX() + "", width);
			// 图形的Y
			double pinY = VisioInputTools.changeY(xfrom.getPinY() + "", eprosMiddleData.getPanelHeight(), height);
			// 图形旋转
			int visioAngle = VisioInputTools.DoubleToInt(xfrom.getAngDeg());
			double angle = Tools.visioAngleToEprosAngle(visioAngle);
			figureData.getFigureDataCloneable().getLeftUpXY().setXY(pinX, pinY);
			// 设置图形旋转
			figureData.setCurrentAngle(angle);
			// 图形的宽度
			figureData.setFigureSizeX(width);
			// 图型的高度
			figureData.setFigureSizeY(height);
			// 图形层级
			figureData.setZOrderIndex(shape.getzIndex());
			// 设置图形通用属性
			setGeneralFiguleAttr(figureData, shape);
			// 设置特殊图形位置属性（xfrom1D图形）
			setSpecialFiguleLocation(figureData, shape, figureType);
			// 添加到图形集合
			figureDataLists.add(figureData);
		}
		eprosMiddleData.setFigureDataLists(figureDataLists);
	}

	/**
	 * 解析特殊图形属性
	 * 
	 * @param figureData
	 * @param shape
	 * @param figureType
	 */
	private void setSpecialFiguleLocation(JecnFigureData figureData, VisioShape shape, String figureType) {
		if ("CommentText".equals(figureType)) {// 如果为注释框
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			// 获取连接线数据对象
			JecnBaseDivedingLineData lineData = new JecnBaseDivedingLineData(MapElemType.CommentLine);
			XForm1DProperty xFrom1D = shape.getXfrom1D();
			// 开始
			int startX = VisioInputTools.DoubleToInt(VisioInputTools.changeX1D(xFrom1D.getBeginX() + ""));
			// 结束
			int startY = VisioInputTools.DoubleToInt(VisioInputTools.changeY1D(xFrom1D.getBeginY() + "",
					eprosMiddleData.getPanelHeight()));
			// 图形的X
			int endX = VisioInputTools.DoubleToInt(VisioInputTools.changeX1D(xFrom1D.getEndX() + ""));
			// 图形的Y
			int endY = VisioInputTools.DoubleToInt(VisioInputTools.changeY1D(xFrom1D.getEndY() + "", eprosMiddleData
					.getPanelHeight()));
			// 开始点
			Point startPoint = new Point(startX, startY);
			// 结束点
			Point endPoint = new Point(endX, endY);
			lineData.getDiviLineCloneable().initOriginalArributs(startPoint, endPoint, figureData.getBodyWidth(),
					JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowScale());
			// 设置小线段
			commentData.setLineData(lineData);
			// 注释框当作0度处理(visio里面默认是90度不能旋转)
			commentData.setCurrentAngle(0);
			// 设置为左上
			commentData.setFontPosition(0);
			// 图形的宽度（visio中获取不到设置默认值110）
			commentData.setFigureSizeX(110);
			// 图型的高度（visio中获取不到设置默认值40）
			commentData.setFigureSizeY(40);
			// 图形的位置点
			commentData.getFigureDataCloneable().getLeftUpXY().setXY(endX, endY - commentData.getFigureSizeY() / 2);
		} else if ("OneArrowLine".equals(figureType) || "TwoArrowLine".equals(figureType)) {// 单项箭头和双向箭头
			XFormProperty xFrom = shape.getXfrom();
			XForm1DProperty xFrom1D = shape.getXfrom1D();
			// 图形高度
			int height = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(xFrom.getHeight() + ""));
			double begX = xFrom1D.getBeginX();
			double begY = xFrom1D.getBeginY();
			// 获取角度
			double angle = figureData.getCurrentAngle();
			double pinX = 0;
			double pinY = 0;
			if (angle == 0 || angle == 360) {
				pinX = VisioInputTools.changeX1D(begX + "");
				pinY = VisioInputTools.changeY(begY + "", eprosMiddleData.getPanelHeight(), height);
			}
			// 图形的宽度
			figureData.setFigureSizeX(110);
			// 图型的高度
			figureData.setFigureSizeY(50);
			// 图形的位置点
			figureData.getFigureDataCloneable().getLeftUpXY().setXY(pinX, pinY);
		} else if ("ReverseArrowhead".equals(figureType) || "RightArrowhead".equals(figureType)) {
			XFormProperty xFrom = shape.getXfrom();
			XForm1DProperty xFrom1D = shape.getXfrom1D();
			if (xFrom1D == null) { // xFrom1D不存在，说明是epros导出的图形，不用在设置位置点和大小，按照常规图形处理，所以直接返回
				return;
			}
			// 图形高度
			int height = VisioInputTools.DoubleToInt(VisioInputTools.feetToPx(xFrom.getHeight() + ""));
			double begX = xFrom1D.getBeginX();
			double begY = xFrom1D.getBeginY();
			// 获取角度
			double angle = figureData.getCurrentAngle();
			double pinX = 0;
			double pinY = 0;
			if (angle == 0 || angle == 360) {
				pinX = VisioInputTools.changeX1D(begX + "");
				pinY = VisioInputTools.changeY(begY + "", eprosMiddleData.getPanelHeight(), height);
			}
			// 图形的宽度
			figureData.setFigureSizeX(40);
			// 图型的高度
			figureData.setFigureSizeY(70);
			// 图形的位置点
			figureData.getFigureDataCloneable().getLeftUpXY().setXY(pinX, pinY);
		}
	}

	/**
	 * 解析图形的通用属性
	 * 
	 * @param figureData
	 * @param shape
	 */
	private void setGeneralFiguleAttr(JecnAbstractFlowElementData figureData, VisioShape shape) {
		setText(figureData, shape);
		setFontAttr(figureData, shape.getVisioChar(), shape.getTextBlock(), shape.getPara());
		setFillAttr(figureData, shape.getFill());
		setLineAttr(figureData, shape.getLine());
	}

	/**
	 * 解析 线属性
	 * 
	 * @param figureData
	 * @param line
	 * @param para
	 * @param textBlock
	 */
	private void setLineAttr(JecnAbstractFlowElementData figureData, final LineProperty line) {
		if (line == null) {
			return;
		}
		// 线条颜色
		figureData.setBodyColor(line.getLineColor());
		// 线条大小
		figureData.setBodyWidth((int) line.getLineWeight());
		// 线条类型
		figureData.setBodyStyle(VisioInputTools.converLinePattern(line.getLinePattern()));
		// PA KSF KCP 固定虚线
		if (MapElemType.PAFigure == figureData.getMapElemType() || MapElemType.KSFFigure == figureData.getMapElemType()
				|| MapElemType.KCPFigure == figureData.getMapElemType()|| MapElemType.KCPFigureComp == figureData.getMapElemType()) {
			figureData.setBodyStyle(1);
		}
		// DottedRect固定点线
		if (MapElemType.DottedRect == figureData.getMapElemType()) {
			figureData.setBodyStyle(2);
		}
	}

	/**
	 * 解析填充属性
	 * 
	 * @param figureData
	 * @param fill
	 */
	private void setFillAttr(JecnAbstractFlowElementData figureData, final FillProperty fill) {
		if (fill == null) {
			return;
		}
		FillType fillType = VisioInputTools.coverFillPattern(fill.getFillPattern());
		// 渐变方式
		FillColorChangType fillColorChangType = VisioInputTools.coverFillColorChangType(fill.getFillPattern());
		// 填充类型 单双色
		figureData.setFillType(fillType);
		// 颜色1
		figureData.setFillColor(fill.getFillForegnd());
		// 颜色2
		if (FillType.two == fillType) {
			figureData.setFillColor(fill.getFillBkgnd());
			figureData.setChangeColor(fill.getFillForegnd());
		}
		// 颜色渐变方向
		figureData.setFillColorChangType(fillColorChangType);
		// 是否存在阴影
		if (FillPattern._00 != fill.getShdwPattern()) {
			figureData.setShadowsFlag(true);
			// 阴影颜色
			figureData.setShadowColor(fill.getShdwForegnd());
		} else {
			figureData.setShadowsFlag(false);
		}

	}

	/**
	 * 解析字体属性
	 * 
	 * @param figureData
	 * @param charP
	 */
	private void setFontAttr(JecnAbstractFlowElementData figureData, final CharProperty charP,
			final TextBlockProperty textBlock, final ParaProperty para) {
		if (charP == null) {
			return;
		}
		// 字体颜色
		figureData.setFontColor(charP.getColor());
		// 字体大小
		figureData.setFontSize(charP.getSize());
		// 字体
		String fontName = VisioBasicReader.getInstance().getFaceName().get(charP.getAsianFont() + "");
		figureData.setFontName(fontName);
		if (VisioInputTools.isChineseChar(figureData.getFigureText())) {
			figureData.setFontName("宋体");
		}
		// 字体加粗
		figureData.setFontStyle(VisioInputTools.converFontStyle(charP.getStyle()));
		// 设置文字竖排
		if (FontVertical.VVertical == charP.getUseVertical()) {
			figureData.setFontUpRight(true);
		}
		if (para != null && textBlock != null) {
			// 字体位置
			figureData.setFontPosition(VisioInputTools.converFontPosition(para.getHorzAlign(), textBlock
					.getVerticalAlign()));
		}
	}

	/**
	 * 文本
	 * 
	 * @param figureData
	 * @param shape
	 */
	private void setText(JecnAbstractFlowElementData figureData, final VisioShape shape) {
		figureData.setFigureText(shape.getText());
		if (figureData instanceof JecnActiveData) {
			String activityNum = shape.getActNum();
			String figureText = shape.getText();
			if ("".equals(activityNum)) {
				activityNum = VisioInputTools.getActivityNum(shape.getText());
				figureText = figureText.replaceAll(activityNum, "").trim();
				figureData.setFigureText(figureText);
			}
			((JecnActiveData) figureData).setActivityNum(activityNum);
			figureData.setFigureText(figureText);
		}

	}

	/**
	 * 横线 竖线 斜线
	 * 
	 * @param divedingLine
	 */
	private void converdeDivedingLine(List<VisioShape> divedingLine) {
		if (divedingLine == null || divedingLine.size() == 0) {
			return;
		}
		List<JecnBaseDivedingLineData> divedingLineLists = new ArrayList<JecnBaseDivedingLineData>();
		JecnBaseDivedingLineData divedingLineData = null;
		for (VisioShape shape : divedingLine) {
			String figureType = getFigureType(shape.getNameU());
			divedingLineData = new JecnBaseDivedingLineData(MapElemType.valueOf(figureType));
			divedingLineData.setZOrderIndex(shape.getzIndex());
			XForm1DProperty xform1D = shape.getXfrom1D();
			int begX = (int) VisioInputTools.changeX1D(xform1D.getBeginX() + "");
			int begY = (int) VisioInputTools.changeY1D(xform1D.getBeginY() + "", eprosMiddleData.getPanelHeight());
			int endX = (int) VisioInputTools.changeX1D(xform1D.getEndX() + "");
			int endY = (int) VisioInputTools.changeY1D(xform1D.getEndY() + "", eprosMiddleData.getPanelHeight());
			divedingLineData.getDiviLineCloneable().getStartXY().setXY(begX, begY);
			divedingLineData.getDiviLineCloneable().getEndXY().setXY(endX, endY);
			setGeneralFiguleAttr(divedingLineData, shape);
			divedingLineLists.add(divedingLineData);
		}
		eprosMiddleData.setDivedingLines(divedingLineLists);
	}

	/**
	 * 解析横向分割线、竖向 分割线
	 * 
	 * @param vHlines
	 */
	private void converHVDashLine(List<VisioShape> vHlines) {
		if (vHlines == null || vHlines.size() == 0) {
			return;
		}
		List<JecnVHLineData> vHLines = new ArrayList<JecnVHLineData>();
		JecnVHLineData vhLineData = null;
		for (VisioShape shape : vHlines) {
			String figureType = getFigureType(shape.getNameU());
			XForm1DProperty xform1D = shape.getXfrom1D();
			int begX = (int) VisioInputTools.changeX1D(xform1D.getBeginX() + "");
			int begY = (int) VisioInputTools.changeY1D(xform1D.getBeginY() + "", eprosMiddleData.getPanelHeight());
			int endX = (int) VisioInputTools.changeX1D(xform1D.getEndX() + "");
			int endY = (int) VisioInputTools.changeY1D(xform1D.getEndY() + "", eprosMiddleData.getPanelHeight());
			LineDirectionEnum lineDirectionEnum = null;
			if (begX == endX) {
				lineDirectionEnum = LineDirectionEnum.vertical;
			} else if (begY == endY) {
				lineDirectionEnum = LineDirectionEnum.horizontal;
			}
			vhLineData = new JecnVHLineData(MapElemType.valueOf(figureType), lineDirectionEnum);
			vhLineData.setZOrderIndex(shape.getzIndex());
			if (LineDirectionEnum.horizontal == lineDirectionEnum) {
				vhLineData.setVhX(0);
				vhLineData.setVhY(begY);
			} else if (LineDirectionEnum.vertical == lineDirectionEnum) {
				vhLineData.setVhX(begX);
				vhLineData.setVhY(0);
			}
			vHLines.add(vhLineData);
		}
		eprosMiddleData.setVHLines(vHLines);
	}

	/**
	 * 转换页面属性
	 * 
	 * @param width
	 * @param height
	 */
	private void converPage(double width, double height) {
		double panelHeight = VisioInputTools.feetToPx(height + "");
		double panelWidth = VisioInputTools.feetToPx(width + "");
		eprosMiddleData.setPanelHeight(panelHeight);
		eprosMiddleData.setPanelWidth(panelWidth);
	}

	/**
	 * 根据NameU获取FigureType
	 * 
	 * @param nameU
	 * @return
	 */
	private String getFigureType(String nameU) {
		return configMap.get(nameU);
	}

	/**
	 * 获取epros图形ID
	 * 
	 * @param visioFigueID
	 * @return
	 */
	private String getFigureUUID(String visioFigueID) {
		return relationMap.get(visioFigueID);
	}

	/**
	 * 添加到关系集合key=visioFigueID,value=eprosFigueID
	 * 
	 * @param visioFigueID
	 * @param eprosFigueID
	 */
	private void putRelationMap(String visioFigueID, String eprosFigueID) {
		if (relationMap == null) {
			relationMap = new HashMap<String, String>();
		}
		relationMap.put(visioFigueID, eprosFigueID);
	}

	/**
	 * 添加到图形属性
	 * 
	 * @param visioFigueID
	 * @param shape
	 */
	private void putVisioShape(String visioFigueID, VisioShape shape) {
		if (visioShape == null) {
			visioShape = new HashMap<String, VisioShape>();
		}
		visioShape.put(visioFigueID, shape);
	}

	private VisioShape getVisioShape(String visioFigueID) {
		return visioShape.get(visioFigueID);
	}

}
