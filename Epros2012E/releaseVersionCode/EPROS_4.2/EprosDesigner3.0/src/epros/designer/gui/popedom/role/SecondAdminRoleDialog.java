package epros.designer.gui.popedom.role;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManageOrgChooseDialog;
import epros.designer.gui.popedom.role.choose.RoleManagePersonChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;

/**
 * 流程管理员角色权限设置
 * 
 *@author admin
 * @date 2016-5-5下午02:59:35
 */
public class SecondAdminRoleDialog extends RoleDialog {
	private JecnTreeNode pNode = null;
	private JecnTree jTree = null;

	public SecondAdminRoleDialog(JecnTreeNode pNode, JecnTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		if (pNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.roleSecondAdminDefaultDir) {
			initEditRoleInfo(pNode.getJecnTreeBean());
			// 获取角色对应流程管理员user
			getSecondAdminUser(pNode.getJecnTreeBean().getId());
		}
	}

	private void getSecondAdminUser(Long roleId) {
		try {
			List<Object[]> userLists = ConnectionPool.getJecnRole().selectUserRoleByRoleId(roleId);
			if (userLists.isEmpty()) {
				return;
			}
			StringBuffer ids = new StringBuffer();
			StringBuffer names = new StringBuffer();
			JecnTreeBean treeBean = new JecnTreeBean();

			for (Object[] objects : userLists) {
				// 科大讯飞需要重新查询人员名称
				String name = JecnIfytekSyncGetName(objects[1]);
				ids.append(objects[0].toString()).append(",");
				names.append(name);
				if (StringUtils.isNotBlank(name)) {
					names.append("/");
				}
				treeBean = new JecnTreeBean();
				treeBean.setId(Long.valueOf(objects[0].toString()));
				treeBean.setName(name);
				secondAdminPeoples.add(treeBean);
			}
			this.setPersonIds(ids.substring(0, ids.length() - 1));

			personArea.setText(names.substring(0, names.length() - 1));

		} catch (Exception e) {
			log.error("SecondAdminRoleDialog getSecondAdminUser is error！", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("RolePeopleError"));
		}
	}

	private String JecnIfytekSyncGetName(Object LoginName) {
		if (LoginName == null) {
			return "";
		}
		if (!JecnConfigTool.isIflytekLogin()) {
			return LoginName.toString();
		}
		String userName = ConnectionPool.getPersonAction().getListUserSearchBean(LoginName.toString());
		return userName == null ? "" : userName;
	}

	protected void initCompotents() {
		super.initCompotents();
		secondAdminPeoples = new ArrayList<JecnTreeBean>();
		// 组织权限初始化
		orgLab = new JLabel(JecnProperties.getValue("orgRightsC"));
		orgArea = new JecnTextArea(false);
		orgScrollPane = new JScrollPane(orgArea);
		orgScrollPane.setBorder(null);
		orgScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		orgScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		orgButton = new JButton(JecnProperties.getValue("selectBtn"));

		personLab = new JLabel(JecnProperties.getValue("flowManagePersonC"));
		personArea = new JecnTextArea(false);
		personScrollPane = new JScrollPane(personArea);
		personScrollPane.setBorder(null);
		personScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		personScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		personButton = new JButton(JecnProperties.getValue("selectBtn"));
	}

	protected void initLayout() {
		super.initLayout();
	}

	/**
	 * 添加组织权限和流程管理员人员选择
	 */
	protected int initOrgAndPersonPanel(int gridy, Insets insets) {
		// 组织权限
		GridBagConstraints c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST,
				GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(orgLab, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				insets, 0, 0);
		mainPanel.add(orgScrollPane, c);
		c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(orgButton, c);
		gridy++;

		// 流程管理员
		c = new GridBagConstraints(0, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(personLab, c);
		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				insets, 0, 0);
		mainPanel.add(personScrollPane, c);
		c = new GridBagConstraints(2, gridy, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		mainPanel.add(personButton, c);
		gridy++;
		return gridy;
	}

	protected void actionInit() {
		super.actionInit();
		/**
		 * 部门选择
		 */
		orgButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				orgChoosePermission();
			}
		});

		personButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				personChoosePermission();
			}
		});
	}

	private void orgChoosePermission() {
		JecnSelectChooseDialog orgChooseDialog = null;
		if (JecnDesignerCommon.isAdmin()) {
			orgChooseDialog = new OrgChooseDialog(listOrg);
		} else {
			orgChooseDialog = new RoleManageOrgChooseDialog(listOrg);
		}
		orgChooseDialog.setSelectMutil(false);
		// 设置标题
		orgChooseDialog.setTitle(JecnProperties.getValue("deptChoose"));
		orgChooseDialog.setVisible(true);

		if (orgChooseDialog.isOperation()) {
			if (listOrg.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : listOrg) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				orgIds = sbIds.substring(0, sbIds.length() - 1);
				orgArea.setText(sbNames.substring(0, sbNames.length() - 1));
			} else {
				orgArea.setText("");
				orgIds = "";
			}
		}
	}

	private void personChoosePermission() {
		JecnSelectChooseDialog personChoose = new RoleManagePersonChooseDialog(secondAdminPeoples);
		// 设置标题
		personChoose.setTitle(JecnProperties.getValue("personChoose"));
		personChoose.setVisible(true);

		if (personChoose.isOperation()) {
			if (secondAdminPeoples.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : secondAdminPeoples) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				personIds = sbIds.substring(0, sbIds.length() - 1);
				personArea.setText(sbNames.substring(0, sbNames.length() - 1));
			} else {
				personArea.setText("");
				personIds = "";
			}
		}
	}

	protected void editSecondAdminRole(String ids, String names) {
		if (!DrawCommon.isNullOrEmtryTrim(ids)) {// 判断是否为空
			this.setOrgIds(ids.substring(0, ids.length() - 1));
			if (!DrawCommon.isNullOrEmtryTrim(names)) {
				this.setOrgPerName(names.substring(0, names.length() - 1));
			}
		}

		// 初始化流程管理员
	}

	@Override
	public String getDialogTitle() {
		/*
		 * 流程管理员权限
		 */
		return JecnProperties.getValue("flowAdminJurisdiction");
	}

	@Override
	public void saveData() {
		// 0是流程、1是文件、2是标准、3是制度、4是风险、5组织 、 6 流程管理员（自定义角色对应 角色分配节点权限类型，6
		// 不存入角色和各节点关联的表中）
		Map<Integer, String> map = getRelateIdMap();
		// 流程管理员节点 可直接设置流程管理员，添加流程管理员peopleIds （更新角色和人员关联表JECN_USER_ROLE）
		map.put(6, personIds);
		if (pNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.roleSecondAdminDefaultDir) {// 更新
			updateRole(map);
		} else {
			addRole(map);
		}
	}

	private void addRole(Map<Integer, String> map) {
		JecnRoleInfo jecnRoleInfo = new JecnRoleInfo();
		jecnRoleInfo.setRoleName(getRoleName());
		// 角色
		jecnRoleInfo.setIsDir(0);
		jecnRoleInfo.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRoleInfo.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRoleInfo.setProjectId(JecnConstants.projectId);
		jecnRoleInfo.setRoleRemark(getRoleContent());
		jecnRoleInfo.setPerId(pNode.getJecnTreeBean().getId());
		jecnRoleInfo.setFilterId("secondAdmin");
		jecnRoleInfo.setSortId(JecnTreeCommon.getMaxSort(pNode));
		try {
			Long id = ConnectionPool.getJecnRole().addRoleSecondAdmin(jecnRoleInfo, map, JecnConstants.getUserId());
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(jecnRoleInfo.getRoleName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.roleSecondAdmin);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("addRole is error！" + JecnProperties.getValue("serverConnException"), e);
			JecnOptionPane.showMessageDialog(null, e.getMessage());
			return;
		}
	}

	private void updateRole(Map<Integer, String> map) {
		// 更新角色信息
		jecnRoleInfo.setRoleName(this.getRoleName());
		jecnRoleInfo.setRoleRemark(this.getRoleContent());
		jecnRoleInfo.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		try {
			ConnectionPool.getJecnRole().updateRoleSecondAdmin(jecnRoleInfo, map, JecnConstants.getUserId());
			this.setOperation(true);
			this.dispose();
		} catch (Exception e) {
			log.error("updateRole is error！", e);
			JecnOptionPane.showMessageDialog(null, e.getMessage());
			return;
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.roleSecondAdmin);
	}

	public void setOrgPerName(String name) {
		orgArea.setText(name);
	}
}
