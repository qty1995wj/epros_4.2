package com.jecn.epros.server.webBean.dataImport.match;
/**
 * 实际部门表（与流程匹配的部门临时表）
 * @author Administrator
 *
 */
public class ActualDeptBean {
	/**主键ID*/
	private Long id;
	
	/**部门编码*/
	private String deptNum;
	
	/**部门名称*/
	private String deptName;
	
	/**上级部门编码*/
	private String perDeptNum;
	
	/**项目ID*/
	private Long projectId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPerDeptNum() {
		return perDeptNum;
	}

	public void setPerDeptNum(String perDeptNum) {
		this.perDeptNum = perDeptNum;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	

}
