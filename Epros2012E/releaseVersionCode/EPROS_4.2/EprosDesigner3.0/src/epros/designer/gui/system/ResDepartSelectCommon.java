package epros.designer.gui.system;

import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class ResDepartSelectCommon extends SingleSelectCommon {
	private static Logger log = Logger.getLogger(ResDepartSelectCommon.class);

	public ResDepartSelectCommon(int rows, JecnPanel infoPanel, Insets insets, Long orgId, String departName,
			JecnDialog jecnDialog, boolean isRequest, String labName) {
		this(rows, infoPanel, insets, orgId, departName, jecnDialog, isRequest);
		super.getJecnLab().setText(labName);

	}

	public ResDepartSelectCommon(int rows, JecnPanel infoPanel, Insets insets, Long orgId, String departName,
			JecnDialog jecnDialog, boolean isRequest) {
		super(rows, infoPanel, insets, new JecnTreeBean(orgId, departName, TreeNodeType.organization), jecnDialog,
				JecnProperties.getValue("responsibleDepartC"), isRequest);
	}

	@Override
	protected List<JecnTreeBean> searchName(String name, Long projectId) {
		try {
			return ConnectionPool.getOrganizationAction().searchByName(name, projectId);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this.getJecnDialog(), JecnProperties.getValue("serverConnException"));
			log.error(JecnProperties.getValue("resDepartSearchException"), e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	protected void selectButPerformed(List<JecnTreeBean> list, JSearchTextField jecnField, Long id) {
		JecnDesignerCommon.setResponsibleDept(list, jecnField, getJecnDialog(), id);

	}
}
