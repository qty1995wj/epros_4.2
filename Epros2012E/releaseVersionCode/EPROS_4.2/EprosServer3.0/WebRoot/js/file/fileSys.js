function fileSys() {
	var fileRoot = new Ext.tree.AsyncTreeNode({
				text : i_fileSys,
				expanded : true,
				id : "0"
			})

	var fileLoader = new Ext.tree.TreeLoader({
				dataUrl : "getChildFiles.action"
			})
	var fileTree = new Ext.tree.TreePanel({
		root : fileRoot,
		height : Ext.getCmp("main-panel").getInnerHeight() - 25,
		loader : fileLoader,
		useArrows : true,
		autoScroll : true,
		rootVisible : false,// 隐藏根节点
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		border : false,
		listeners : {
			"dblclick" : function(node, e) {
				var length = fileTabPanel.items.length;
				if (node.attributes.type == "file") {
					for (var i = length - 2; i >= 0; i--) {
						var p = fileTabPanel.getComponent(i);
						fileTabPanel.remove(p, true);
					}
					Ext.Ajax.request({
						url : basePath + "file.action",
						params : {
							fileId : node.id,
							dbClick : 'dbClick'
						},
						success : function(response, r) {
							if (response.responseText == 'noPopedom') {
								alert(i_noPopedom);// 您没有访问权限!
								return;
							} else {
								fileTabPanel.insert(0, {
									title : "文件属性",// 文件基本信息
									id : "file_detail_id",
									autoLoad : {
										url : basePath
												+ "file.action?callType=1&visitType=infoFile&fileId="
												+ node.attributes.id

									}
								});
									if (isAdmin || isViewAdmin
										|| isDocControlPermission) {
									fileTabPanel.insert(1, {
										title : i_historyInfo,// 文控信息
										border : false,
										id : "file_historyInfo_id",
										autoLoad : {
											url : basePath
													+ "fileHistoryNew.action?fileId="
													+ node.id,
											callback : function() {
												setDocumentTemplate('fileDocumentDiv');
											}
										}
									});
								}
								fileTabPanel.setActiveTab(0);
							}
						}

					})

				} else if (node.attributes.type == "fileDir") {
					for (var i = length - 2; i >= 0; i--) {
						var p = fileTabPanel.getComponent(i);
						fileTabPanel.remove(p, true);
					}
					fileTabPanel.insert(0, {
								title : i_fileList,// 文件列表
								id : "fileSys_fileList_id",
								autoLoad : {
									url : basePath + "jsp/file/fileList.jsp",
									callback : function() {
										fileListByPid(node.id);
									}
								}
							});
					fileTabPanel.insert(1, {
								title : i_fileDetailList,// 文件清单
								id : "fileDetailList_id",
								autoLoad : {
									url : basePath + "jsp/file/fileDetailList.jsp",
									callback : function() {
										fileDetailListJson(node.id);
									}
								}
							});
					if (isShowDirList) {
						fileTabPanel.remove(Ext.getCmp("fileSys_fileList_id"));
					}
					fileTabPanel.setActiveTab(0);
				}

			}

		}

	});
	var fileTabPanel = new Ext.TabPanel({
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				height : Ext.getCmp("main-panel").getInnerHeight(),
				items : [{
							title : i_search,// 搜索
							id : "fileSearchTab_id",
							autoLoad : {
								url : basePath
										+ "jsp/file/fileSystemSeareh.jsp",
								scripts : false,
								callback : function(el, success, response) {
									fileSysGrid();
								}
							}

						}],
				listeners : {
					'tabchange' : function(tabPanel, panel) {
						if (panel.getId() == 'fileSearchTab_id') {
							if (typeof(fileSysGridGridPanel) != "undefined") {
								fileSysGridGridPanel
										.setSize(tabPanel.getWidth() - 2,
												tabPanel.getHeight() - 165);
							}

						} else if (panel.getId() == 'fileSys_fileList_id') {
							if (typeof(fileListGridGridPanel) != "undefined") {
								fileListGridGridPanel.setSize(tabPanel
												.getWidth()
												- 2, tabPanel.getHeight() - 28);
							}

						}
					}
				}
			});
	var filePanel = new Ext.Panel({
		renderTo : "fileSys_id",
		border : false,
		id : 'filePanel_index_id',
		layout : "border",
		height : Ext.getCmp("main-panel").getInnerHeight(),
		items : [{
			region : "west",
			border : 0,
			title : "<div  style='text-align:center;font-weight:bold'>"
					+ i_fileSys + "</div>",
			split : true,
			collapseMode : "mini",
			width : 300,
			items : [fileTree]

		}, {
			region : "center",
			border : 0,
			id : "file_center_panel_id",
			items : [fileTabPanel],
			listeners : {
				'bodyresize' : function(p, width, height) {
					if (fileTabPanel.getActiveTab() == null) {
						return;
					}
					fileTree.setHeight(height - 5);
					fileTabPanel.setSize(width, height);

					if (fileTabPanel.getActiveTab().getId() == 'fileSearchTab_id') {
						fileSysGridGridPanel.setSize(width, height - 165);
					} else if (fileTabPanel.getActiveTab().getId() == 'fileSys_fileList_id') {
						fileListGridGridPanel.setSize(width - 2, height - 28);
					}
				}
			}

		}]
	});
}

function fileSysGrid() {

	fileSysGridStore = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : "getFileList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "fileId",
									mapping : "fileId"
								}, {
									name : "fileName",
									mapping : "fileName"
								}, {
									name : "fileNum",
									mapping : "fileNum"
								}, {
									name : "orgName",
									mapping : "orgName"
								}, {
									name : "secretLevel",
									mapping : "secretLevel"
								}])

			});

	fileSysGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_fileName,
				dataIndex : "fileName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : fileNameRenderer
			}, {
				header : i_fileNum,
				dataIndex : "fileNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}

	]);
	fileSysGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : fileSysGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	fileSysGridGridPanel = new Ext.grid.GridPanel({
				id : "fileSysGridGridPanel_id",
				renderTo : 'fileGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 165,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				bodyStyle : 'border-left: 0px;',
				store : fileSysGridStore,
				colModel : fileSysGridColumnModel,
				bbar : fileSysGridBbar
			})

}

function fileNameRenderer(value, cellmeta, record) {
	return "<div  ext:qtip='"
			+ value
			+ "'><a target='_blank' class='a-operation' href='file.action?visitType=infoFile&fileId="
			+ record.data['fileId'] + "' >" + value + "</a>";
}
function fileDetailRenderer(value, cellmeta, record) {
	return "<div><a target='_blank' class='a-operation' href='file.action?visitType=infoFile&fileId="
			+ record.data['fileId'] + "' >" + i_detail + "</a>";
}

function fileSearch() {
	// 文件名称
	var fileName = document.getElementById("fileName_id").value;
	var namelength = 0;
	var fileNames = fileName.split(" ");
	for (var f = 0, len = fileNames.length; f < len; f++) {
		if (fileNames[f] != "") {
			namelength++;
		}
	}
	if (namelength > 3) {
		alert("文件名称最多输入三个！");
		return;
	}

	// 文件编号
	var fileNum = document.getElementById("fileNum_id").value;
	var numlength = 0;
	var fileNums = fileNum.split(" ");
	for (var n = 0, fnum = fileNums.length; n < fnum; n++) {
		if (fileNums[n] != "") {
			numlength++;
		}
	}
	if (numlength > 3) {
		alert("文件编号最多输入三个！");
		return;
	}

	// 责任部门名称
	var orgDutyName = document.getElementById("orgDutyName_file_id").value;
	// 责任部门ID
	var orgDutyId = document.getElementById("orgDutyId_file_id").value;
	// 查阅部门名称
	var orgLookName = document.getElementById("orgLookName_file_id").value;
	// 查阅部门ID
	var orgLookId = document.getElementById("orgLookId_file_id").value;
	// 查阅岗位名称
	var posLookName = document.getElementById("posLookName_file_id").value;
	// 查阅岗位ID
	var posLookId = document.getElementById("posLookId_file_id").value
			.split("_")[0];
	// 密级
	var secretLevel = document.getElementById("secretLevel_id").value;

	fileSysGridStore.setBaseParam("fileName", fileName);
	fileSysGridStore.setBaseParam("fileNum", fileNum);

	fileSysGridStore.setBaseParam("orgName", orgDutyName);
	if (orgDutyId && orgDutyId != "") {
		fileSysGridStore.setBaseParam("orgId", orgDutyId);
	}
	fileSysGridStore.setBaseParam("orgLookName", orgLookName);
	if (orgLookId && orgLookId != "") {
		fileSysGridStore.setBaseParam("orgLookId", orgLookId);
	}
	fileSysGridStore.setBaseParam("posLookName", posLookName);
	if (posLookId && posLookId != "") {
		fileSysGridStore.setBaseParam("posLookId", posLookId);
	}
	if (secretLevel && secretLevel != "") {
		fileSysGridStore.setBaseParam("secretLevel", secretLevel);
	}
	fileSysGridStore.load({
				params : {
					start : 0,
					limit : pageSize
				}
			});
}

/**
 * 文件查询条件重置
 */
function fileSearchReset() {
	// 文件名称
	document.getElementById("fileName_id").value = "";
	// 文件编号
	document.getElementById("fileNum_id").value = "";
	// 责任部门名称
	document.getElementById("orgDutyName_file_id").value = "";
	// 责任部门ID
	document.getElementById("orgDutyId_file_id").value = "-1"
	// 查阅部门名称
	document.getElementById("orgLookName_file_id").value = "";
	// 查阅部门ID
	document.getElementById("orgLookId_file_id").value = "-1";
	// 查阅岗位名称
	document.getElementById("posLookName_file_id").value = "";
	// 查阅岗位ID
	document.getElementById("posLookId_file_id").value == "-1";
	// 密级
	document.getElementById("secretLevel_id").options[0].selected = true;

}

function fileListByPid(id) {

	fileListGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				baseParams : {
					fileId : id
				},
				proxy : new Ext.data.HttpProxy({
							url : "fileListByPid.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "fileId",
									mapping : "fileId"
								}, {
									name : "fileName",
									mapping : "fileName"
								}, {
									name : "fileNum",
									mapping : "fileNum"
								}, {
									name : "orgName",
									mapping : "orgName"
								}, {
									name : "secretLevel",
									mapping : "secretLevel"
								}])

			});

	fileListGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_fileName,
				dataIndex : "fileName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : fileNameRenderer
			}, {
				header : i_fileNum,
				dataIndex : "fileNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}

	]);
	fileListGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : fileListGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	fileListGridGridPanel = new Ext.grid.GridPanel({
				id : "fileListGridGridPanel_id",
				renderTo : 'fileListGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 28,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : fileListGridStore,
				colModel : fileListGridColumnModel,
				bbar : fileListGridBbar
			})

}