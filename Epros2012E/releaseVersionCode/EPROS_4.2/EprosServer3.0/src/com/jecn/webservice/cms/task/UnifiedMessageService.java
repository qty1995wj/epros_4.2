/**
 * UnifiedMessageService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cms.task;

public interface UnifiedMessageService extends javax.xml.rpc.Service {
    public java.lang.String getUnifiedMessageServiceSoap12Address();

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap12() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getUnifiedMessageServiceSoapAddress();

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
