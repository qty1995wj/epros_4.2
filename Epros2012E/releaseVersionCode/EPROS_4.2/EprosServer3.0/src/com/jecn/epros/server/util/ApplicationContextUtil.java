package com.jecn.epros.server.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
/**
 * 获取ApplicationContex
 * @author yxw
 *
 */
public class ApplicationContextUtil implements ApplicationContextAware {
	private static ApplicationContext context;
	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		// TODO Auto-generated method stub
		this.context=context;
	}
	public static ApplicationContext getContext(){   
		return context;   
	}
}
