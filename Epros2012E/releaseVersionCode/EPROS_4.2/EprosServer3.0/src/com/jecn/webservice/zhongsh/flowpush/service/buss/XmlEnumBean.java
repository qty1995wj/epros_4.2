package com.jecn.webservice.zhongsh.flowpush.service.buss;

public class XmlEnumBean {
	public enum StateType {
		Normal, Start, // 开始节点
		End, // 结束
		ParallelSplit, // 分支
		ParallelJoin, // 会聚
		Decision
		// 自动判断的节点
	}
}
