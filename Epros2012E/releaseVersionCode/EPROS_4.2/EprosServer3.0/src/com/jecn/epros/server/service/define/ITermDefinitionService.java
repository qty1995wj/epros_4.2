package com.jecn.epros.server.service.define;

import java.util.List;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface ITermDefinitionService {

	TermDefinitionLibrary createDir(TermDefinitionLibrary termDefinitionLibrary) throws Exception;

	TermDefinitionLibrary createTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception;

	void delete(List<Long> listIds, Long updatePersonId) throws Exception;

	TermDefinitionLibrary editTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception;

	List<JecnTreeBean> getChilds(Long pId) throws Exception;

	boolean isExistAdd(String name, Long id, int type) throws Exception;

	boolean isExistEdit(String name, Long id, int type, Long pId) throws Exception;

	void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType) throws Exception;

	void reName(String name, Long id, Long updatePeopleId) throws Exception;

	List<JecnTreeBean> searchByName(String name) throws Exception;

	void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception;

	TermDefinitionLibrary getTermDefinitionLibraryById(Long id) throws Exception;

	List<JecnTreeBean> getChildDirs(Long pId)throws Exception;

	List<JecnTreeBean> getPnodes(Long id)throws Exception;

	List<TermDefinitionLibrary> listTermsByIds(List<Long> idList)throws Exception;

	List<JecnTreeBean> getRoleAuthChilds(long l, Long projectId, long userId);

	List<JecnTreeBean> searchRoleAuthByName(String name, Long peopleId,Long projectId);

}
