package com.jecn.epros.server.dao.task.impl;

import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.task.IJecnTaskTestRunDao;
import com.jecn.epros.server.service.task.TestRunCommon;

/**
 * 任务试运行到期提醒DAO
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Jan 15, 2013 时间：4:24:46 PM
 */
public class JecnTaskTestRunImpl extends AbsBaseDao<JecnTaskBeanNew, Long>
		implements IJecnTaskTestRunDao {
	/**
	 * 获取试运行任务（只有流程任务）相关信息及用户信息
	 */
	@Override
	public List<Object[]> getTaskTestRun() {
		String sql = null;
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = TestRunCommon.SEND_TEST_RUN_REPORT_SQL_SQLSERVER;
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = TestRunCommon.SEND_TEST_RUN_REPORT_SQL_ORACLE;
		}
		List<Object[]> listObjects = this.listNativeSql(sql);
		return listObjects;
	}

	/**
	 * 
	 * 试运行到期提醒
	 */
	@Override
	public List<Object[]> sendTestRunEndE() {
		String sql = null;
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = TestRunCommon.SEND_TEST_RUN_END_SQL_SQLSERVER;
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = TestRunCommon.SEND_TEST_RUN_END_SQL_ORACLE;
		}
		return this.listNativeSql(sql);
	}
}
