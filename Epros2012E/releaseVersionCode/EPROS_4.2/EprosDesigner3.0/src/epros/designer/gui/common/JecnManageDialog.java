package epros.designer.gui.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

/**
 * @author yxw 2012-5-4
 * @description：（角色管理、文件管理、支持工具）公共Dialog抽象类，具体某种功能管理Dialog需要继承些
 */
@SuppressWarnings("serial")
public abstract class JecnManageDialog extends JecnDialog implements ActionListener {

	/** 主面板 */
	private JPanel mainPanel = null;

	/** 树滚动面板+快速查询面板的容器· */
	private JecnSplitPane splitPane = null;

	/** 树面板 */
	private JScrollPane treeScrollPanel = null;
	/** 右侧面板 */
	private JPanel centerPanel = null;

	/** 搜索面板 */
	private JPanel searchPanel = null;
	/** 搜索名称 */
	private JLabel searchLabel = null;
	/** 搜索内容 */
	private JTextField searchText = null;

	/** 结果面板 */
	private JPanel resultPanel = null;
	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** Dialog的关闭按钮在此面板上 */
	private JPanel colsePanel = null;
	/** 关闭按钮 */
	private JButton closeButton = null;

	private JLabel jLabelTip = null;

	/** 按钮面板，上面放四个按钮 */
	private JPanel buttonsPanel = null;
	private JButton buttonOne = null;
	private JButton buttonTwo = null;
	private JButton buttonThree = null;
	private JButton deleteBtn = null;
	/** 搜索的结果 */
	private List<JecnTreeBean> searchList = new ArrayList<JecnTreeBean>();

	public List<JecnTreeBean> getSearchList() {
		return searchList;
	}

	public void setSearchList(List<JecnTreeBean> searchList) {
		this.searchList = searchList;
	}

	private JecnTable jecnTable = null;

	public JecnTable getJecnTable() {
		return jecnTable;
	}

	public void setJecnTable(JecnTable jecnTable) {
		this.jecnTable = jecnTable;
	}

	private JecnTree jTree = null;

	public JecnTree getjTree() {
		return jTree;
	}

	public void setjTree(JecnTree jTree) {
		this.jTree = jTree;
	}

	public JecnManageDialog() {
		initCompotents();
		initLayout();
		buttonInit();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:初始化组件
	 */
	public void initCompotents() {
		// 主面板
		mainPanel = new JPanel();
		// 树面板
		treeScrollPanel = new JScrollPane();
		// 右侧面板
		centerPanel = new JPanel();
		// 树滚动面板+快速查询面板的容器
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, mainPanel, treeScrollPanel, centerPanel);
		// 搜索面板
		searchPanel = new JPanel();
		// 结果面板
		resultPanel = new JPanel();
		// 结果滚动面板
		resultScrollPane = new JScrollPane();
		// 按钮面板
		buttonsPanel = new JPanel();
		// 关闭面板，此面板上只有一个关闭按钮
		colsePanel = new JPanel();
		//
		closeButton = new JButton(JecnProperties.getValue("closeBtn"));

		// 搜索名称
		searchLabel = new JLabel(getSerachLabelName());
		// 搜索内容
		searchText = new JTextField(60);

		jLabelTip = new JLabel();
		jLabelTip.setForeground(Color.red);
		// 快速查询面板中按钮
		this.buttonOne = new JButton(buttonOneName());
		this.buttonTwo = new JButton(buttonTwoName());
		this.buttonThree = new JButton(buttonThreeName());
		this.deleteBtn = new JButton(JecnProperties.getValue("deleteBtn"));

		// 把tree加入滚动面板
		jTree = initJecnTree();
		// 快速查询table
		jecnTable = new JecnManageTable(new ArrayList<JecnTreeBean>());
		// 树滚动面板
		treeScrollPanel.setViewportView(jTree);
		resultScrollPane.setViewportView(jecnTable);

		// 大小
		this.setSize(710, 510);
		// 模态
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		this.setTitle(getDialogTitle());

		// 主面板布局-开始
		mainPanel.setLayout(new BorderLayout());
		// 快速查询面板
		centerPanel.setLayout(new BorderLayout(0, 2));
		// 结果面板布局-开始
		resultPanel.setLayout(new BorderLayout());
		// 搜索面板布局,流布局 靠左
		searchPanel.setLayout(new BorderLayout());
		// 关闭面板布局
		colsePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 快速查询面板中按钮面板
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		// 树滚动面板大小
		Dimension size = new Dimension(200, 430);
		treeScrollPanel.setPreferredSize(size);
		treeScrollPanel.setMinimumSize(size);

		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treeScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		searchPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		colsePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 按钮命令标识
		buttonOne.setActionCommand("buttonOne");
		buttonTwo.setActionCommand("buttonTwo");
		buttonThree.setActionCommand("buttonThree");
		// 删除
		deleteBtn.setActionCommand("deleteBtn");
		// 关闭按钮
		closeButton.setActionCommand("closeBtn");

		// 边框
		// 树滚动面板
		treeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				getTreePanelTitle()));
		// 快速查询面板
		centerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("quickSearch")));

		// 左:右=3:7
		splitPane.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPane.setResizeWeight(0.0);
		splitPane.showBothPanel();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:dialog布局方法
	 */
	public void initLayout() {
		// **************第一层布局**************//
		// 内容区
		mainPanel.add(splitPane, BorderLayout.CENTER);
		// 关闭按钮
		mainPanel.add(colsePanel, BorderLayout.SOUTH);
		// **************第一层布局**************//

		// **************快速查询布局**************//
		// 搜索名称
		searchPanel.add(searchLabel, BorderLayout.WEST);
		// 搜索内容
		searchPanel.add(searchText, BorderLayout.CENTER);

		buttonsPanel.add(jLabelTip);
		buttonsPanel.add(buttonOne);
		buttonsPanel.add(buttonTwo);
		buttonsPanel.add(buttonThree);
		buttonsPanel.add(deleteBtn);

		resultPanel.add(resultScrollPane, BorderLayout.CENTER);
		resultPanel.add(buttonsPanel, BorderLayout.SOUTH);

		centerPanel.add(searchPanel, BorderLayout.NORTH);
		centerPanel.add(resultPanel, BorderLayout.CENTER);
		// **************快速查询布局**************//

		// 结果面板布局-结束
		colsePanel.add(closeButton);

		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);

	}

	/**
	 * 
	 * 按钮点击事件
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if ("buttonOne".equals(btn.getActionCommand())) {
				buttonOneAction();
			} else if ("buttonTwo".equals(btn.getActionCommand())) {
				buttonTwoAction();
			} else if ("buttonThree".equals(btn.getActionCommand())) {
				buttonThreeAction();
			} else if ("deleteBtn".equals(btn.getActionCommand())) { // 删除
				delAction();
			} else if ("closeBtn".equals(btn.getActionCommand())) { // 关闭
				closeDialog();
			}
		}
	}

	/**
	 * 
	 * 为按钮增加事件
	 * 
	 */
	private void buttonInit() {
		btnsVisible(false);
		if (isAddButtonOne()) {
			buttonOne.setVisible(true);
			buttonOne.addActionListener(this);
		}

		if (isAddButtonTwo()) {
			buttonTwo.setVisible(true);
			buttonTwo.addActionListener(this);
		}

		if (isAddButtonThree()) {
			this.buttonThree.setVisible(true);
			buttonThree.addActionListener(this);
		}
		// 删除
		this.deleteBtn.setVisible(true);
		deleteBtn.addActionListener(this);

		// 关闭按钮
		closeButton.addActionListener(this);

		// 搜索输入框事件
		searchText.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				searchTextAction();
			}

			public void insertUpdate(DocumentEvent e) {
				searchTextAction();
			}

			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:删除操作
	 */
	public void delAction() {
		int[] selectRows = jecnTable.getSelectedRows();
		if (selectRows.length == 0) {
			jLabelTip.setText(JecnProperties.getValue("chooseOneRow"));
			// JecnOptionPane.showMessageDialog(this,
			// JecnProperties.getValue("chooseOneRow"));
			return;
		}
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		// 保存要删除的id，传到后台删除数据
		List<Long> listIds = new ArrayList<Long>();
		// 保存要删除的树节点
		List<JecnTreeBean> listDel = new ArrayList<JecnTreeBean>();

		for (int i = selectRows.length - 1; i >= 0; i--) {
			listIds.add(searchList.get(selectRows[i]).getId());
			listDel.add(searchList.get(selectRows[i]));
			searchList.remove(selectRows[i]);

		}

		if (!deleteData(listIds)) {
			// 提示删除失败
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return;
		}

		// 删除table选中的数据
		jecnTable.removeRows(selectRows);
		// 提示删除成功
		// 删除节点
		for (JecnTreeBean jecnTreeBean : listDel) {
			JecnTreeNode node = JecnTreeCommon.getTreeNode(jecnTreeBean, jTree);
			if (node != null) {
				JecnTreeCommon.removeNode(jTree, node);
			}
		}

	}

	/***************************************************************************
	 * @author yxw 2012-5-10
	 * @description:删除数据库操作
	 * @param listIds
	 * @return
	 */
	public abstract boolean deleteData(List<Long> listIds);

	/**
	 * 搜索
	 * 
	 * @param evt
	 */
	private void searchTextAction() {
		String name = searchText.getText().trim();
		if (!"".equals(name)) {
			searchList = searchByName(name);
			jecnTable.reAddVectors(updateTableContent(searchList));
		} else {
			searchList.clear();
			jecnTable.remoeAll();
		}
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:搜索
	 * @param name
	 * @return
	 */
	public abstract List<JecnTreeBean> searchByName(String name);

	private void closeDialog() {
		this.dispose();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:设置dialog标题
	 * @return
	 */
	public abstract String getDialogTitle();

	/**
	 * @author yxw 2012-5-3
	 * @description:设置treePanel标题
	 * @return
	 */
	public abstract String getTreePanelTitle();

	public abstract String getSerachLabelName();

	/**
	 * @author yxw 2012-5-4
	 * @description:获得树对象
	 * @return
	 */
	public abstract JecnTree initJecnTree();

	// button抽象方法--开始

	// 是否显示
	public abstract boolean isAddButtonOne();

	public abstract boolean isAddButtonTwo();

	public abstract boolean isAddButtonThree();

	// 显示名称
	public abstract String buttonOneName();

	public abstract String buttonTwoName();

	public abstract String buttonThreeName();

	// 事件
	public abstract void buttonOneAction();

	public abstract void buttonTwoAction();

	public abstract void buttonThreeAction();

	// button抽象方法--结束
	class JecnManageTable extends JecnTable {

		private List<JecnTreeBean> listJecnTreeBeans = null;

		public JecnManageTable(List<JecnTreeBean> listJecnTreeBeans) {
			this.listJecnTreeBeans = listJecnTreeBeans;
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent(listJecnTreeBeans));
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 1 };
		}

	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得JTable的内容
	 * @param listJecnTreeBeans
	 * @return
	 */
	private Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		if (list != null) {
			return updateTableContent(list);
		}
		return new Vector<Vector<String>>();
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得Table的标题
	 * @return
	 */
	public abstract Vector<String> getTableTitle();

	/**
	 * @author yxw 2012-5-10
	 * @description:更新Table的内容
	 * @param list
	 * @return
	 */
	public abstract Vector<Vector<String>> updateTableContent(List<JecnTreeBean> list);

	// button抽角方法--结束

	protected void btnsVisible(boolean b) {
		buttonOne.setVisible(b);
		buttonTwo.setVisible(b);
		buttonThree.setVisible(b);
		deleteBtn.setVisible(b);
	}

	public JLabel getjLabelTip() {
		return jLabelTip;
	}
}
