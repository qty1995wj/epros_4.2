package com.jecn.epros.server.webBean.process;

import java.util.List;

import com.jecn.epros.server.webBean.KeyValueBean;
/**
 * @author yxw 2012-12-3
 * @description：角色流程活动对应bean
 */
public class RoleProcessBean {
	/**角色名称*/
	private String roleName;
	/**流程ID*/
	private Long processId;
	/**流程编号*/
	private String processNum;
	/**流程名称*/
	private String processName;
	/**活动List*/
	private List<KeyValueBean> activeList;
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Long getProcessId() {
		return processId;
	}
	public void setProcessId(Long processId) {
		this.processId = processId;
	}
	public String getProcessNum() {
		return processNum;
	}
	public void setProcessNum(String processNum) {
		this.processNum = processNum;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public List<KeyValueBean> getActiveList() {
		return activeList;
	}
	public void setActiveList(List<KeyValueBean> activeList) {
		this.activeList = activeList;
	}
}
