package com.jecn.epros.server.download.wordxml.base;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

public class GeneralProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);
	/*** 段落行距 1.5倍行距 *****/
	private ParagraphLineRule vLine1_5 = new ParagraphLineRule(1.5F,
			LineRuleType.AUTO);
	/**** 段落行距最小值16磅值 ********/
	private ParagraphLineRule vLine_atLeast16 = new ParagraphLineRule(16F,
			LineRuleType.AT_LEAST);
	/****** 段落行距固定值20磅 *********/
	private ParagraphLineRule lineRule = new ParagraphLineRule(20,
			LineRuleType.EXACT);

	/**
	 * 默认模版
	 * 
	 * @param processDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public GeneralProcessModel(ProcessDownloadBean processDownloadBean,
			String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(17.49F);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle("、", textTitleStyle());
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		String flowName = processDownloadBean.getFlowName();
		String flowIsPublic = getPubOrSec();
		String flowInputNum = processDownloadBean.getFlowInputNum();
		Table table = null;
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.xiaosi, true);
		pBean.setAlign(Align.center);
		Header hdr = sect.createHeader(type);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(1.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "", flowName, flowIsPublic });
		rowData.add(new String[] { "", "", flowInputNum });
		table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 5.72F, 7F,
				4.78F }, rowData, pBean);
		table.getRow(0).setHeight(0.78F);
		table.getRow(1).setHeight(0.78F);
		table.setRowStyle(0, Valign.center);
		table.setRowStyle(1, Valign.center);
		table.getRow(0).getCell(0).setRowspan(2);
		table.getRow(0).getCell(1).setRowspan(2);
		// 公司logo图标
		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(6.06F, 1.59F);
		Paragraph p = table.getRow(0).getCell(0).getParagraph(0);
		p.appendGraphRun(logo);
		hdr.createParagraph("", new ParagraphBean(PStyle.AF1));
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "Arial",
				Constants.xiaowu, false);
		Footer ftr = sect.createFooter(type);
		Paragraph p = ftr.createParagraph("", pBean);
		p.appendCurPageRun();
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.0F, 1.76F, 1.5F, 1.76F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		// 公司名称
		String companyName = processDownloadBean.getCompanyName();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		// 编制部门
		String orgName = processDownloadBean.getOrgName();
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		String flowVersion = processDownloadBean.getFlowVersion();
		ParagraphBean pBean = null;
		// 宋体 一号 加粗 居中 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.yihao, true);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph(companyName, pBean);
		// 单线条
		LineGraph line = new LineGraph(LineStyle.single, 1.25F);
		line.setFrom(7.9F, 8.75F);
		line.setTo(513.2F, 8.75F);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
		for (int i = 0; i < 8; i++) {
			titleSect.createParagraph("");
		}
		// 宋体 小一 加粗 居中 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoyi, true);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph(flowName, pBean);
		for (int i = 0; i < 5; i++) {
			titleSect.createParagraph("");
		}
		// 宋体 四号 左对齐 1.5倍行距
		pBean = new ParagraphBean("宋体", Constants.sihao, false);
		pBean.setSpace(0f, 0f, vLine1_5);
		pBean.setInd(3.7F, 0, 0, 0.25F);
		titleSect.createParagraph("编制部门：" + orgName, pBean);
		titleSect.createParagraph("文件编号：" + flowInputNum, pBean);
		titleSect.createParagraph("文件版号：" + flowVersion, pBean);
		for (int i = 0; i < 5; i++) {
			titleSect.createParagraph("");
		}

		// 宋体 五号 左对齐 最小值16磅值
		pBean = new ParagraphBean("宋体", Constants.wuhao, false);
		pBean.setSpace(0f, 0f, vLine_atLeast16);
		TableBean tabBean = new TableBean();
		tabBean.setBorder(1.5F);
		tabBean.setBorderInsideH(1);
		tabBean.setBorderInsideV(1);
		tabBean.setTableLeftInd(0.35F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		List<String[]> rowData = new ArrayList<String[]>();
		// 评审人集合
		/** 评审人集合 0 评审人类型 1名称 2日期 */
		List<Object[]> peopleList = processDownloadBean.getPeopleList();
		String[] str = null;
		rowData.add(new String[] { "审批栏", "姓名", "签名", "日期" });
		for (Object[] obj : peopleList) {
			str = new String[] { "" + obj[0], "" + obj[1], "", "" + obj[2] };
			rowData.add(str);
		}
		Table tab = JecnWordUtil.createTab(titleSect, tabBean, new float[] {
				4.36F, 4.36F, 4.36F, 4.36F }, rowData, pBean);
		/******* 设置第一行样式 黑体五号 *********/
		pBean = new ParagraphBean(Align.center, "黑体", Constants.wuhao, true);
		// 设置行样式
		tab.setRowStyle(0, pBean);
		pBean = new ParagraphBean("黑体", Constants.wuhao, true);
		// 设置第一列样式
		tab.setCellStyle(0, pBean);
		tab.getRow(0).setHeader(true);
		/**** 设置行高和 垂直居中 ******/
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.getRow(i).setHeight(0.6F);
			tab.setRowStyle(i, Valign.center);
		}
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left,
				new FontBean("宋体", Constants.wuhao, false));
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		return null;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center,
				new FontBean("宋体", Constants.wuhao, true));
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left,
				new FontBean("宋体", Constants.xiaosi, false));
		textContentStyle.setInd(0.52F, 0, 0, 0);
		textContentStyle.setSpace(0f, 0f, lineRule);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left,
				new FontBean("宋体", Constants.xiaosi, true));
		textTitleStyle.setInd(0, 0, 0.95F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(0.5F, 0.2F, lineRule); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
