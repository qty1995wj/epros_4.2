package epros.draw.gui.top.toolbar.io;

import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnAidCompCollection;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUndoRedoManager;

/**
 * 清空面板数据类
 * 
 * @author fuzhh Nov 20, 2012
 * 
 */
public class JecnIOUtil {
	/**
	 * 重置面板
	 */
	public static void resetWorkflow() {
		if (getWorkflow() == null) {
			return;
		}
		// 清除面板
		removeAllWorkflow();
		// 内存中图形的数据
		getWorkflow().getFlowMapData().getDesignerData().getOldFigureList().clear();
		// 内存中线的数据
		getWorkflow().getFlowMapData().getDesignerData().getOldLineList().clear();
	}

	/**
	 * 清除面板所有元素
	 * 
	 * @author fuzhh Aug 21, 2012
	 */
	public static void removeAllWorkflow() {
		if (getWorkflow() == null) {
			return;
		}
		// 回到原始视图
		getWorkflow().getWorkflowZoomProcess().zoomByNewWorkflowScale(1.0);
		// 清空面板
		getWorkflow().removeAll();
		// 重置面板上的List
		resetWorkflowList();
	}

	/**
	 * 重置面板的List集合
	 */
	public static void resetWorkflowList() {
		if (getWorkflow() == null) {
			return;
		}
		// 清空流程元素集合
		getWorkflow().getPanelList().clear();
		// 清空记录面板所有分割线集合
		getWorkflow().getVhLinePanelList().clear();
		// 清空记录面板所有连接线集合
		getWorkflow().getManLineList().clear();
		// 清空当前选中的所有控件
		getWorkflow().getCurrentSelectElement().clear();
		// 清空面板所有分割线
		getWorkflow().getDiviLineList().clear();
		// 清空记录剪贴板复制的流程元素集合:源流程元素（不是克隆后的流程元素）
		getWorkflow().getCurCopyList().clear();
		// 图形对象集合
		getWorkflow().getFlowMapData().getFigureList().clear();
		// 线对象集合
		getWorkflow().getFlowMapData().getLineList().clear();
		// 清空注释框小线段集合
		getWorkflow().getCommentLineList().clear();
		// 面板最大层级数置为0
		getWorkflow().setMaxZOrderIndex(0);
		// 面板编辑框，圈选面板初始化
		getWorkflow().setAidCompCollection(new JecnAidCompCollection(getWorkflow()));
		// 撤销恢复对象出始化
		getWorkflow().setUndoRedoManager(new JecnUndoRedoManager(getWorkflow()));
		// 同步工具栏的撤销恢复按钮
		getWorkflow().getUndoRedoManager().syncToolBar();
		// 清空面板分页符集合
		getWorkflow().getPageSetLines().clear();
		// 术语
		getWorkflow().getTermFigureList().clear();
		getWorkflow().getModelPanelList().clear();
	}

	/**
	 * 获取面板
	 * 
	 * @return 画图面板
	 */
	public static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

}
