package epros.draw.gui.top.toolbar.element;

import java.awt.Container;
import java.awt.event.ActionEvent;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 流程元素维护使用的元素面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementMangPanel extends JecnFlowElementPanel {
	/** 流程元素维护面板 */
	private JecnFlowElementMainPanel mainPanel = null;

	/**
	 * 
	 * 流程元素维护使用
	 * 
	 * @param comp
	 *            Container JecnFlowElementPanel的容器
	 * @param mainPanel
	 *            JecnFlowElementMainPanel 流程元素维护面板
	 * @param downLayoutFlag
	 *            boolean 浏览区域（previewPanel）：true：显示面板下面 ；false：显示在面板上面
	 * 
	 */
	public JecnFlowElementMangPanel(Container comp,
			JecnFlowElementMainPanel mainPanel, boolean downLayoutFlag) {
		super(comp, downLayoutFlag);
		if (mainPanel == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_MANG_PANEL
					.error("JecnFlowElementMangPanel类构造函数：参数为null。mainPanel="
							+ mainPanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.mainPanel = mainPanel;
	}

	protected void okBtnProcess(ActionEvent e) {
		// 更新流程元素数据到本地文件
		mainPanel.writeFlowElementDataToXML();
	}
}
