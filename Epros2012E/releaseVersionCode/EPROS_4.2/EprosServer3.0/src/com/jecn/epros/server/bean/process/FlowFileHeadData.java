package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.Date;

public class FlowFileHeadData implements Serializable{
	/**
	 * 主键ID
	 */
	private String id;
	/**
	 * 流程ID
	 */
	private Long flowId;
	/**
	 * 文件路径 
	 */
	private String filePath;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 公司名称
	 */
	private String companyName;
	/**
	 * 公司英文名称
	 */
	private String companyEnName;
	/**
	 * 文件名称
	 */
	private String fileName;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyEnName() {
		return companyEnName;
	}
	public void setCompanyEnName(String companyEnName) {
		this.companyEnName = companyEnName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	
	
	

}
