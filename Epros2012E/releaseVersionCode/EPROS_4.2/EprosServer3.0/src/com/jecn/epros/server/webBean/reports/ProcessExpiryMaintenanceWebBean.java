package com.jecn.epros.server.webBean.reports;

public class ProcessExpiryMaintenanceWebBean {
	/** 文控信息id */
	private String docId;
	/** 流程id */
	private String flowId;
	/** 流程name */
	private String flowName;
	/** 流程编号 */
	private String flowIdInput;
	/** 责任部门name */
	private String dutyOrgName;
	/** 责任人name */
	private String dutyPeopleName;
	/** 流程监护人 */
	private String guardianName;
	/** 拟稿人 */
	private String peopleMakeName;
	/** 发布日期 */
	private String pubDate;
	/** 流程有效期 */
	private String expiry;
	/** 下次审视需完成时间 */
	private String nextScandDate;
	/** 版本号 */
	private String versionId;
	/** 错误信息 */
	private String errorInfo;
	/** 该条记录是否错误 */
	private boolean isError = false;

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowIdInput() {
		return flowIdInput;
	}

	public void setFlowIdInput(String flowIdInput) {
		this.flowIdInput = flowIdInput;
	}

	public String getDutyOrgName() {
		return dutyOrgName;
	}

	public void setDutyOrgName(String dutyOrgName) {
		this.dutyOrgName = dutyOrgName;
	}

	public String getDutyPeopleName() {
		return dutyPeopleName;
	}

	public void setDutyPeopleName(String dutyPeopleName) {
		this.dutyPeopleName = dutyPeopleName;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getPeopleMakeName() {
		return peopleMakeName;
	}

	public void setPeopleMakeName(String peopleMakeName) {
		this.peopleMakeName = peopleMakeName;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getNextScandDate() {
		return nextScandDate;
	}

	public void setNextScandDate(String nextScandDate) {
		this.nextScandDate = nextScandDate;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
}
