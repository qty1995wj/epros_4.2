
package com.jecn.webservice.zhongsh.risk.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Task complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�����ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="Task">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaskId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaskStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreNodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reviewer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkflowTemplateName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Task_Create_Time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Workflow_Start_Time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Prereviewer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Executiver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XmlString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmisUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FormId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubmiterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StarterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TemplateId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TacheName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompileDept" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="URLType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsInvalid" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Task", propOrder = {
    "taskId",
    "instanceId",
    "taskStatus",
    "preNodeName",
    "reviewer",
    "workflowTemplateName",
    "taskCreateTime",
    "workflowStartTime",
    "prereviewer",
    "executiver",
    "xmlString",
    "smisUrl",
    "formId",
    "projectName",
    "submiterName",
    "starterName",
    "documentName",
    "nodeName",
    "templateId",
    "version",
    "tacheName",
    "compileDept",
    "urlType",
    "isInvalid"
})
public class Task {

    @XmlElement(name = "TaskId")
    protected String taskId;
    @XmlElement(name = "InstanceId")
    protected String instanceId;
    @XmlElement(name = "TaskStatus")
    protected String taskStatus;
    @XmlElement(name = "PreNodeName")
    protected String preNodeName;
    @XmlElement(name = "Reviewer")
    protected String reviewer;
    @XmlElement(name = "WorkflowTemplateName")
    protected String workflowTemplateName;
    @XmlElement(name = "Task_Create_Time", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar taskCreateTime;
    @XmlElement(name = "Workflow_Start_Time", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar workflowStartTime;
    @XmlElement(name = "Prereviewer")
    protected String prereviewer;
    @XmlElement(name = "Executiver")
    protected String executiver;
    @XmlElement(name = "XmlString")
    protected String xmlString;
    @XmlElement(name = "SmisUrl")
    protected String smisUrl;
    @XmlElement(name = "FormId")
    protected String formId;
    @XmlElement(name = "ProjectName")
    protected String projectName;
    @XmlElement(name = "SubmiterName")
    protected String submiterName;
    @XmlElement(name = "StarterName")
    protected String starterName;
    @XmlElement(name = "DocumentName")
    protected String documentName;
    @XmlElement(name = "NodeName")
    protected String nodeName;
    @XmlElement(name = "TemplateId")
    protected String templateId;
    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "TacheName")
    protected String tacheName;
    @XmlElement(name = "CompileDept")
    protected String compileDept;
    @XmlElement(name = "URLType")
    protected String urlType;
    @XmlElement(name = "IsInvalid")
    protected boolean isInvalid;

    /**
     * ��ȡtaskId���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * ����taskId���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskId(String value) {
        this.taskId = value;
    }

    /**
     * ��ȡinstanceId���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * ����instanceId���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstanceId(String value) {
        this.instanceId = value;
    }

    /**
     * ��ȡtaskStatus���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskStatus() {
        return taskStatus;
    }

    /**
     * ����taskStatus���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskStatus(String value) {
        this.taskStatus = value;
    }

    /**
     * ��ȡpreNodeName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreNodeName() {
        return preNodeName;
    }

    /**
     * ����preNodeName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreNodeName(String value) {
        this.preNodeName = value;
    }

    /**
     * ��ȡreviewer���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReviewer() {
        return reviewer;
    }

    /**
     * ����reviewer���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReviewer(String value) {
        this.reviewer = value;
    }

    /**
     * ��ȡworkflowTemplateName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkflowTemplateName() {
        return workflowTemplateName;
    }

    /**
     * ����workflowTemplateName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkflowTemplateName(String value) {
        this.workflowTemplateName = value;
    }

    /**
     * ��ȡtaskCreateTime���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaskCreateTime() {
        return taskCreateTime;
    }

    /**
     * ����taskCreateTime���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaskCreateTime(XMLGregorianCalendar value) {
        this.taskCreateTime = value;
    }

    /**
     * ��ȡworkflowStartTime���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWorkflowStartTime() {
        return workflowStartTime;
    }

    /**
     * ����workflowStartTime���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWorkflowStartTime(XMLGregorianCalendar value) {
        this.workflowStartTime = value;
    }

    /**
     * ��ȡprereviewer���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrereviewer() {
        return prereviewer;
    }

    /**
     * ����prereviewer���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrereviewer(String value) {
        this.prereviewer = value;
    }

    /**
     * ��ȡexecutiver���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutiver() {
        return executiver;
    }

    /**
     * ����executiver���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutiver(String value) {
        this.executiver = value;
    }

    /**
     * ��ȡxmlString���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlString() {
        return xmlString;
    }

    /**
     * ����xmlString���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlString(String value) {
        this.xmlString = value;
    }

    /**
     * ��ȡsmisUrl���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmisUrl() {
        return smisUrl;
    }

    /**
     * ����smisUrl���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmisUrl(String value) {
        this.smisUrl = value;
    }

    /**
     * ��ȡformId���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormId() {
        return formId;
    }

    /**
     * ����formId���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormId(String value) {
        this.formId = value;
    }

    /**
     * ��ȡprojectName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * ����projectName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectName(String value) {
        this.projectName = value;
    }

    /**
     * ��ȡsubmiterName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmiterName() {
        return submiterName;
    }

    /**
     * ����submiterName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmiterName(String value) {
        this.submiterName = value;
    }

    /**
     * ��ȡstarterName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStarterName() {
        return starterName;
    }

    /**
     * ����starterName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStarterName(String value) {
        this.starterName = value;
    }

    /**
     * ��ȡdocumentName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * ����documentName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentName(String value) {
        this.documentName = value;
    }

    /**
     * ��ȡnodeName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * ����nodeName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeName(String value) {
        this.nodeName = value;
    }

    /**
     * ��ȡtemplateId���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateId() {
        return templateId;
    }

    /**
     * ����templateId���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateId(String value) {
        this.templateId = value;
    }

    /**
     * ��ȡversion���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * ����version���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * ��ȡtacheName���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTacheName() {
        return tacheName;
    }

    /**
     * ����tacheName���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTacheName(String value) {
        this.tacheName = value;
    }

    /**
     * ��ȡcompileDept���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompileDept() {
        return compileDept;
    }

    /**
     * ����compileDept���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompileDept(String value) {
        this.compileDept = value;
    }

    /**
     * ��ȡurlType���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURLType() {
        return urlType;
    }

    /**
     * ����urlType���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURLType(String value) {
        this.urlType = value;
    }

    /**
     * ��ȡisInvalid���Ե�ֵ��
     * 
     */
    public boolean isIsInvalid() {
        return isInvalid;
    }

    /**
     * ����isInvalid���Ե�ֵ��
     * 
     */
    public void setIsInvalid(boolean value) {
        this.isInvalid = value;
    }

}
