function standardSys() {
	var standardRoot = new Ext.tree.AsyncTreeNode({
				text : i_standard,
				expanded : true,
				id : "0"
			});

	var standardLoader = new Ext.tree.TreeLoader({
				dataUrl : "getChildStandards.action"
			});
	var standardTree = new Ext.tree.TreePanel({
		root : standardRoot,
		height : Ext.getCmp("main-panel").getInnerHeight() - 25,
		loader : standardLoader,
		useArrows : true,
		border : false,
		autoScroll : true,
		rootVisible : false,// 隐藏根节点
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			"dblclick" : function(node, e) {
				var length = standardTabPanel.items.length;
				for (var i = length - 2; i >= 0; i--) {
					var p = standardTabPanel.getComponent(i);
					standardTabPanel.remove(p, true);
				}
				if (node.attributes.type == "standard") {
					standardTabPanel.insert(0, {
						title : i_stanDetail,// 标准详情
						id : "standSys_standardDetail_id",
						autoLoad : {
							url : basePath
									+ "standard.action?callType=1&standardType=1&standardId="
									+ node.id,
							callback : function() {

							}
						}
					});
					standardTabPanel.setActiveTab(0);
					// window.open("standardDetail.action?standardType=1&standardId="+node.id+"&fileId="
					// + node.attributes.relationId);
				} else if (node.attributes.type == "standardProcess") {
					window.open(basePath
							+ openProcess(node.attributes.relationId,'process'));
				} else if (node.attributes.type == "standardProcessMap") {
					window.open(basePath
							+ openProcess(node.attributes.relationId,'processMap'));
				} else if (node.attributes.type == "standardDir") {
					standardTabPanel.insert(0, {
						title : i_standardList,// 标准列表
						id : "standSys_standardList_id",
						autoLoad : {
							url : basePath + "jsp/standardSys/standardList.jsp",
							callback : function() {
								standardListByPid(node.id);
							}
						}
					});
					//if(isAdmin || isViewAdmin){//只有流程管理员和系统管理员登录才可以显示标准清单
						standardTabPanel.insert(1, {
									title : i_standardDirList,// 标准清单
									id : "standardInfoList",
									autoLoad : {
										url : basePath
												+ "jsp/standardSys/standardInventory.jsp",
										callback : function() {
											 standardSysListJson(node.id);
										}
									}
								});
					//}
					if (isShowDirList) {
						standardTabPanel.remove(Ext.getCmp("standSys_standardList_id"));
					}
					standardTabPanel.setActiveTab(0);
				} else if (node.attributes.type == "standardClause") {
					standardTabPanel.insert(0, {
						title : i_stanDetail,// 标准详情
						id : "standSys_standardDetail_id",
						autoLoad : {
							url : basePath
									+ "standard.action?callType=1&standardType=4&standardId="
									+ node.id,
							callback : function() {

							}
						}
					});
					standardTabPanel.setActiveTab(0);

				} else if (node.attributes.type == "standardClauseRequire") {
					standardTabPanel.insert(0, {
						title : i_stanDetail,// 标准详情
						id : "standSys_standardDetail_id",
						autoLoad : {
							url : basePath
									+ "standard.action?callType=1&standardType=5&standardId="
									+ node.id,
							callback : function() {

							}
						}
					});
					standardTabPanel.setActiveTab(0);
				}
			}

		}

	});
	var standardTabPanel = new Ext.TabPanel({
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				height : Ext.getCmp("main-panel").getInnerHeight(),
				items : [{
					title : i_search,// 搜索
					id : "standardSearchTab_id",
					border:false,
					autoLoad : {
						url : basePath
								+ "jsp/standardSys/standardSystemSearch.jsp",
						scripts : false,
						callback : function(el, success, response) {
							standardSysGrid();
						}
					}

				}],
				listeners : {
					'tabchange' : function(tabPanel, panel) {
						if (panel.getId() == 'standardSearchTab_id') {
							if (typeof(standardSysGridGridPanel) != "undefined") {
								standardSysGridGridPanel.setSize(tabPanel
												.getWidth(), tabPanel
												.getHeight()
												- 164);
							}

						} else if (panel.getId() == 'standSys_standardList_id') {
							if (typeof(standardListGridGridPanel) != "undefined") {
								standardListGridGridPanel.setSize(tabPanel
												.getWidth(), tabPanel
												.getHeight()
												- 29);
							}
						}
					}
				}

			});
	var standardPanel = new Ext.Panel({
		renderTo : "standard_id",
		border : false,
		id : 'standardPanel_index_id',
		layout : "border",
		height : Ext.getCmp("main-panel").getInnerHeight(),
		items : [{
			region : "west",
			border : 0,
			title : "<div  style='text-align:center;font-weight:bold'>"
					+ i_standardSys + "</div>",
			split : true,
			collapseMode : "mini",
			width : 300,
			items : [standardTree]

		}, {
			region : "center",
			border : 0,
			id : "standard_center_panel_id",
			items : [standardTabPanel],
			listeners : {
				'bodyresize' : function(p, width, height) {
					if (standardTabPanel.getActiveTab() == null) {
						return;
					}
					standardTree.setHeight(height);
					standardTabPanel.setSize(width, height);
					if (standardTabPanel.getActiveTab().getId() == 'standardSearchTab_id') {
						standardSysGridGridPanel.setSize(width, height - 164);
					} else if (standardTabPanel.getActiveTab().getId() == 'standSys_standardList_id') {
						standardListGridGridPanel.setSize(width, height - 29);
					}

				}
			}

		}]
	});
}

function standardSysGrid() {

	standardSysGridStore = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : "getStandardList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "id",
									mapping : "id"
								}, {
									name : "fileId",
									mapping : "fileId"
								}, {
									name : "name",
									mapping : "name"
								}, {
									name : "fileName",
									mapping : "fileName"
								}, {
									name : "type",
									mapping : "type"
								}, {
									name : "pname",
									mapping : "pname"
								}, {
									name : "secretLevel",
									mapping : "secretLevel"
								}])

			});

	standardSysGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_standardName,
				dataIndex : "name",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : standardNameRenderer
			}, {
				header : i_type,
				dataIndex : "type",
				align : "center",
				menuDisabled : true,
				renderer : standardTypeRenderer
			}, {
				header : i_BelongDir,
				dataIndex : "pname",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}
	]);
	standardSysGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : standardSysGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	standardSysGridGridPanel = new Ext.grid.GridPanel({
				id : "standardSysGridGridPanel_id",
				renderTo : 'standardGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 164,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				bodyStyle:'border-left: 0px;',
				store : standardSysGridStore,
				colModel : standardSysGridColumnModel,
				bbar : standardSysGridBbar
			});

}

/**
 * 标准体系搜索
 */
function standardSearch() {
	// 标准名称
	var standardName = document.getElementById("standardName_id").value;
	// var ruleNum=document.getElementById("ruleNum_id").value;
	// 标准类型
	var standardType = document.getElementById("standardType_id").value;
	// 查阅部门名称
	var orgLookName = document.getElementById("orgLookName_standard_id").value;
	// 查阅部门ID
	var orgLookId = document.getElementById("orgLookId_standard_id").value;
	// 查阅岗位名称
	var posLookName = document.getElementById("posLookName_standard_id").value;
	// 查阅岗位ID
	var posLookId = document.getElementById("posLookId_standard_id").value.split('_')[0];
	// 密级
	var secretLevel = document.getElementById("secretLevel_id").value;

	standardSysGridStore.setBaseParam("standardType", standardType);
	standardSysGridStore.setBaseParam("standardName", standardName);
	standardSysGridStore.setBaseParam("orgLookName", orgLookName);
	if (orgLookId && orgLookId != "") {
		standardSysGridStore.setBaseParam("orgLookId", orgLookId);
	}
	standardSysGridStore.setBaseParam("posLookName", posLookName);
	if (posLookId && posLookId != "") {
		standardSysGridStore.setBaseParam("posLookId", posLookId);
	}
	if (secretLevel && secretLevel != "") {
		standardSysGridStore.setBaseParam("secretLevel", secretLevel);
	}

	standardSysGridStore.load({
				params : {
					start : 0,
					limit : pageSize
				}
			});
}
function standardNameRenderer(value, cellmeta, record) {
	if (record.data['type'] == "1") {
		return "<div  ext:qtip='"
				+ value
				+ "'><a target='_blank' class='a-operation' href='standard.action?standardType=1&standardId="
				+ record.data['id'] + "&fileId=" + record.data['fileId']
				+ "' >" + value + "</a>";
	} else if (record.data['type'] == "2") {
		return "<div  ext:qtip='"
				+ value
				+ "'><a target='_blank' class='a-operation' href='"
				+ openProcess(record.data['fileId'],'process') + "' >" + value + "</a>";
	} else if (record.data['type'] == "3") {
		return "<div  ext:qtip='"
				+ value
				+ "'><a target='_blank' class='a-operation' href='"
				+ openProcess(record.data['fileId'],'processMap') + "' >" + value + "</a>";
	}else if(record.data['type'] == "4"){
				return "<div><a target='_blank' class='a-operation' href='standard.action?standardType=4&standardId="
				+ record.data['id']
				+ "' >" + value + "</a>";
	}else if(record.data['type'] == "5"){
				return "<div><a target='_blank' class='a-operation' href='standard.action?standardType=5&standardId="
				+ record.data['id']
				+ "' >" + value + "</a>";
	}

}
function standardDetailRenderer(value, cellmeta, record) {
	if (record.data['type'] == "1") {
		return "<div><a target='_blank' class='a-operation' href='standard.action?standardType=1&standardId="
				+ record.data['id']
				+ "&fileId="
				+ record.data['fileId']
				+ "' >" + i_detail + "</a>";
	} else if (record.data['type'] == "2") {
		return "<div><a target='_blank' class='a-operation' href='standard.action?standardType=2&standardId="
				+ record.data['id']
				+ "&flowId="
				+ record.data['fileId']
				+ "&name="
				+ record.data['fileName']
				+ "' >"
				+ i_detail
				+ "</a>";
	} else if (record.data['type'] == "3") {
		return "<div><a target='_blank' class='a-operation' href='standard.action?standardType=3&standardId="
				+ record.data['id']
				+ "&flowId="
				+ record.data['fileId']
				+ "&name="
				+ record.data['fileName']
				+ "' >"
				+ i_detail
				+ "</a>";
	}else if(record.data['type'] == "4"){
				return "<div><a target='_blank' class='a-operation' href='standardDetail.action?standardType=4&standardId="
				+ record.data['id']
				+ "' >" + i_detail + "</a>";
	}else if(record.data['type'] == "5"){
				return "<div><a target='_blank' class='a-operation' href='standardDetail.action?standardType=5&standardId="
				+ record.data['id']
				+ "' >" + i_detail + "</a>";
	}

}
/**
 * 标准类
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 */
function standardTypeRenderer(value, cellmeta, record) {
	if (value == "1") {
		return i_file;
	} else if (value == "2") {
		return i_procesChart;
	} else if (value == "3") {
		return i_processMap;
	}else if (value == "4") {
		return i_clause;
	}else if (value == "5") {
		return i_clauseRequire;
	}
}
/**
 * 标准搜索条件重置
 */
function standardSearchReset() {
	// 标准名称
	document.getElementById("standardName_id").value = "";
	// 标准类型
	document.getElementById("standardType_id").options[0].selected = true;
	// 查阅部门名称
	document.getElementById("orgLookName_standard_id").value = "";
	// 查阅部门ID
	orgLookId = document.getElementById("orgLookId_standard_id").value = -1;
	// 查阅岗位名称
	document.getElementById("posLookName_standard_id").value = "";
	// 查阅岗位ID
	document.getElementById("posLookId_standard_id").value = -1;
	// 密级
	document.getElementById("secretLevel_id").options[0].selected = true;

}

function standardListByPid(id) {

	standardListGridStore = new Ext.data.Store({
				baseParams : {
					standardId : id
				},
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "standardListByPid.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "id",
									mapping : "id"
								}, {
									name : "fileId",
									mapping : "fileId"
								}, {
									name : "name",
									mapping : "name"
								}, {
									name : "fileName",
									mapping : "fileName"
								}, {
									name : "type",
									mapping : "type"
								}, {
									name : "pname",
									mapping : "pname"
								}, {
									name : "secretLevel",
									mapping : "secretLevel"
								}])

			});

	standardListGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_standardName,
				dataIndex : "name",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : standardNameRenderer
			}, {
				header : i_type,
				dataIndex : "type",
				align : "center",
				menuDisabled : true,
				renderer : standardTypeRenderer
			}, {
				header : i_BelongDir,
				dataIndex : "pname",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}

	]);
	standardListGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : standardListGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	standardListGridGridPanel = new Ext.grid.GridPanel({
				id : "standardListGridGridPanel_id",
				renderTo : 'standardListGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 29,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				border:false,
				frame : false,
				store : standardListGridStore,
				colModel : standardListGridColumnModel,
				bbar : standardListGridBbar
			});

}

function openProcess(relationId,process){
	return "process.action?type="+process+"&flowId=" + relationId;
}

