package com.jecn.epros.server.connector.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekLoginItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.HttpRequest;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.iflytek.IflytekContent;
import com.jecn.epros.server.connector.iflytek.IflytekDeleteContent;
import com.jecn.epros.server.connector.iflytek.IflytekTaskBean;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;

/**
 * 科大讯飞
 * 
 * @author zhangxh
 * @date 2018-11-06
 * 
 */
public class IflytekTaskInfo extends JecnBaseTaskSend {
	private IPersonDao personDao;

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
			// 创建任务
			sendTasks(myTaskBean, 0, curPeopleId, beanNew);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 转已办
			sendTasks(myTaskBean, 1, curPeopleId, beanNew);
		} else {// 一条from的数据,一条to 的数据
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());
			// 上一个阶段，处理任务 转已办
			sendTasks(myTaskBean, 1, curPeopleId, beanNew);
			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
				// 审批任务，发送下一环节任务待办
				sendTasks(myTaskBean, 0, curPeopleId, beanNew);
			}
		}
	}

	/**
	 * 删除待办
	 * 
	 * @param myTaskBean
	 * @param handlerPeopleId
	 * @throws Exception
	 */
	private void deleteToDoTask(MyTaskBean myTaskBean, Long handlerPeopleId, IPersonDao personDap) throws Exception {

	}

	protected String getTaskUrl(JecnTaskBeanNew beanNew, boolean isApp, boolean isView) {
		Long taskId = beanNew.getId();
		if (!isApp) {
			return getPCUrl(taskId, isView, beanNew);
		} else {// App请求
			return getAppUrl(taskId);
		}
	}

	private String getPCUrl(Long taskId, boolean isView, JecnTaskBeanNew beanNew) {
		String basicEprosURL = JecnContants.NEW_APP_CONTEXT;
		String taskUrl = null;
		if (isView) {
			taskUrl = "/tasks/" + taskId + "/view";
		} else {
			taskUrl = "/task/" + taskId + "/operation";
		}
		return basicEprosURL + taskUrl;
	}

	private String getAppUrl(Long taskId) {
		String taskUrl = "taskLogin.action?accessType=openApproveDomino&mailTaskId=" + taskId + "&isApp=true";
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		return basicEprosURL + taskUrl;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		JecnUser createUser = personDao.get(beanNew.getCreatePersonId());
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setCreateTime(JecnCommon.getStringbyDateHMS(beanNew.getCreateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setRid(beanNew.getRid().toString());
		myTaskBean.setCreatePeopleLoginName(createUser.getLoginName());
		myTaskBean.setOperationType(beanNew.getTaskElseState());
		myTaskBean.setStageName(beanNew.getStateMark());
		myTaskBean.setCreatePeopleId(beanNew.getCreatePersonId());
		myTaskBean.setTaskType(beanNew.getTaskType());
		myTaskBean.setApproveType(beanNew.getApproveType());
		myTaskBean.setTaskName(beanNew.getTaskName());
		return myTaskBean;
	}

	/**
	 * 取消任务代办 10所 专业(设计缺陷)
	 */
	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew beanNew, JecnUser jecnUser, JecnUser createUser) {
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());
		try {
			deleteToDoTask(myTaskBean, jecnUser.getPeopleId(), this.personDao);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());
		if (beanNew.getIsLock() == 1) {
			// 批量删除待办
			deleteTasks(myTaskBean, cancelPeoples);
			return;
		}
		if (beanNew.getIsLock() == 0 || beanNew.getTaskElseState() == 11 || beanNew.getTaskElseState() == 12
				|| beanNew.getTaskElseState() == 13) {
			// 批量删除待办
			deleteTasks(myTaskBean, cancelPeoples);
		} else {
			for (Long cancelPeople : cancelPeoples) {
				// 审批任务，发送下一环节任务待办
				sendTasks(myTaskBean, 1, cancelPeople, beanNew);
			}
		}

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
		// 审批任务，发送下一环节任务待办
		sendTasks(myTaskBean, 0, null, beanNew);
	}

	/**
	 * 
	 * @param myTaskBean
	 * @param sendType
	 *            0 ：待办，1：已办
	 * @throws Exception
	 */
	private void sendTasks(MyTaskBean myTaskBean, int sendType, Long curPeopleId, JecnTaskBeanNew beanNew)
			throws Exception {
		JecnUser creater = personDao.get(myTaskBean.getCreatePeopleId());

		String receiver = null;
		if (sendType == 1) {
			JecnUser curPeople = personDao.get(curPeopleId);
			receiver = curPeople.getLoginName();
		} else {
			List<JecnUser> handlerUsers = personDao.getJecnUserList(myTaskBean.getHandlerPeopleIds());
			// 接收人（域账号），多个用户使用英文逗号隔开
			for (JecnUser handlerUser : handlerUsers) {
				if (receiver == null) {
					receiver = handlerUser.getLoginName();
				} else {
					receiver += "," + handlerUser.getLoginName();
				}
			}
		}
		IflytekTaskBean<IflytekContent> taskBean = createIflytekTask(creater, myTaskBean, receiver, sendType, beanNew);
		try {
			String result = HttpRequest.httpPost(JecnIflytekLoginItem.getValue("TASK_URL"), taskBean);
			log.info("result" + result);
		} catch (Exception e) {
			log.error("result error" + e.getMessage());
			e.printStackTrace();
		}
	}

	private void deleteTasks(MyTaskBean myTaskBean, List<Long> cancelPeoples) throws Exception {
		Set<Long> deleteIds = new HashSet<Long>();
		deleteIds.addAll(cancelPeoples);

		List<JecnUser> handlerUsers = personDao.getJecnUserList(deleteIds);
		// 接收人（域账号），多个用户使用英文逗号隔开
		String receiver = null;
		for (JecnUser handlerUser : handlerUsers) {
			if (receiver == null) {
				receiver = handlerUser.getLoginName();
			} else {
				receiver += "," + handlerUser.getLoginName();
			}
		}

		try {
			IflytekTaskBean<IflytekDeleteContent> taskBean = deleteIflytekTask(myTaskBean, receiver);
			String result = HttpRequest.httpPost(JecnIflytekLoginItem.getValue("TASK_URL"), taskBean);
			log.info("result" + result);
		} catch (Exception e) {
			log.error("result error" + e.getMessage());
			e.printStackTrace();
		}
	}

	private IflytekTaskBean<IflytekContent> createIflytekTask(JecnUser creater, MyTaskBean myTaskBean, String receiver,
			int sendType, JecnTaskBeanNew beanNew) {
		IflytekTaskBean<IflytekContent> iflytekTask = new IflytekTaskBean<IflytekContent>();
		// 发送系统ID,通过消息中心注册获取
		iflytekTask.setSenderSystem(JecnIflytekLoginItem.getValue("senderSystem"));
		iflytekTask.setAuthCode(JecnIflytekLoginItem.getValue("authCode"));
		iflytekTask.setSenderUser(JecnIflytekLoginItem.getValue("senderUser"));
		if (sendType == 0) {// 待办
			iflytekTask.setReceiverSystem(JecnIflytekLoginItem.getValue("receiverSystem0"));
			iflytekTask.setTitle("iBPMS待办流程接口");
		} else {
			iflytekTask.setReceiverSystem(JecnIflytekLoginItem.getValue("receiverSystem1"));
			iflytekTask.setTitle("iBPMS已办流程接口");
		}
		// 接收用户(传集中待办)
		iflytekTask.setReceiverUser("集中待办");
		// 私信：0 ;通知：1;短信：2 ;推送：3 ;邮件：4（传1）
		iflytekTask.setMsgType("1");

		iflytekTask.setContent(getCreateTaskContent(creater, myTaskBean, receiver, sendType == 1, beanNew));
		return iflytekTask;
	}

	private IflytekContent getCreateTaskContent(JecnUser creater, MyTaskBean myTaskBean, String receiver,
			boolean isView, JecnTaskBeanNew beanNew) {
		IflytekContent content = new IflytekContent();
		// 流程任务id
		content.setFlowId(String.valueOf(myTaskBean.getTaskId()));

		// 标题
		String stateName = null;
		if (isView) {
			stateName = beanNew.getUpStateMark();
		} else {
			stateName = beanNew.getStateMark();
		}
		content.setRequestName(beanNew.getTaskName() + "-" + stateName + "-" + myTaskBean.getUpdateTime());
		// 流程名称
		content.setWorkflowName(myTaskBean.getTaskName());
		// 步骤名称（节点名称）
		content.setNodeName(myTaskBean.getStageName());
		content.setPcUrl(getTaskUrl(beanNew, false, isView));
		content.setAppUrl(getTaskUrl(beanNew, true, isView));
		// 创建人（对应域账号）
		content.setCreator(creater.getLoginName());
		// 创建日期时间(yyyy-MM-dd HH:ss:mm)
		content.setCreateDateTime(myTaskBean.getUpdateTime());
		// 接收人（域账号），多个用户使用英文逗号隔开
		content.setReceiver(receiver);
		content.setMsgTypeCode("003");
		// 是否可以批量通过(0:否1:是)
		content.setBatchStatus("0");
		// 流程分类
		content.setWorkflowtype(getProcessTypeName(myTaskBean));
		// 流程备注信息
		content.setRequestnamenew("");
		/** 是否抄送单据(0:否1:是) */
		content.setCopy("0");
		/** 流程二级分类 */
		content.setTwoLevelClass("");
		return content;
	}

	private IflytekTaskBean<IflytekDeleteContent> deleteIflytekTask(MyTaskBean myTaskBean, String receiver) {
		IflytekTaskBean<IflytekDeleteContent> iflytekTask = new IflytekTaskBean<IflytekDeleteContent>();
		// 发送系统ID,通过消息中心注册获取
		iflytekTask.setSenderSystem(JecnIflytekLoginItem.getValue("senderSystem"));
		/** 授权码 */
		iflytekTask.setAuthCode(JecnIflytekLoginItem.getValue("authCode"));
		/** 发送用户(传OA系统) */
		iflytekTask.setSenderUser(JecnIflytekLoginItem.getValue("senderUser"));
		/** 接收系统ID(传notice_9d290ba1955b4acbbd7d46b77f5b1c04) */
		iflytekTask.setReceiverSystem(JecnIflytekLoginItem.getValue("receiverSystem2"));
		/** 接收用户(传集中待办) */
		iflytekTask.setReceiverUser("集中待办");
		/** 私信：0 通知：1短信：2 推送：3 邮件：4（传1） */
		iflytekTask.setMsgType("1");
		/** 标题(传OA已办流程接口) */
		iflytekTask.setTitle("iBPMS删除流程接口");

		iflytekTask.setContent(getDeleteTaskContent(myTaskBean, receiver));
		return iflytekTask;
	}

	private IflytekDeleteContent getDeleteTaskContent(MyTaskBean myTaskBean, String receiver) {
		IflytekDeleteContent content = new IflytekDeleteContent();
		/** 流程任务id */
		content.setFlowId(String.valueOf(myTaskBean.getTaskId()));
		/** 标题 */
		content.setRequestName(myTaskBean.getTaskName());
		/** 流程名称 */
		content.setWorkflowName(myTaskBean.getTaskName());
		/** 步骤名称（节点名称） */
		content.setNodeName(myTaskBean.getStageName());
		/** 类型编码(传004) */
		content.setMsgTypeCode("003");
		/** 接收人（域账号），多个用户使用英文逗号隔开 */
		content.setReceiver(receiver);
		return content;
	}

	/**
	 * 流程分类
	 * 
	 * @param myTaskBean
	 * @return
	 */
	private String getProcessTypeName(MyTaskBean myTaskBean) {
		// 审批类型 0审批 1借阅 2废止
		int approveType = myTaskBean.getApproveType();
		// 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		switch (myTaskBean.getTaskType()) {
		case 0:
			return approveType == 0 ? "流程审批任务" : "流程废止审批";
		case 1:
			return approveType == 0 ? "文件审批任务" : "文件废止审批";
		case 2:
			return approveType == 0 ? "制度审批任务" : "制度废止审批";
		case 3:
			return approveType == 0 ? "制度模板审批任务" : "制度模板废止审批";
		case 4:
			return approveType == 0 ? "流程架构审批任务" : "流程架构废止审批";
		default:
			break;
		}
		return null;
	}
}
