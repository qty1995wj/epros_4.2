package com.jecn.epros.server.service.task.search;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;

/**
 * 我的任务，任务计划 搜索、删除、显示记录 service接口
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 23, 2012 时间：10:13:23 AM
 */
public interface IJecnTaskSearchService extends IBaseService<JecnTaskBeanNew, Long> {

	/**
	 * 根据查询条件获取登录人相关任务
	 * 
	 * @param searchTempBean
	 *            TaskSearchTempBean页面返回的查询信息
	 * @param loginPeopleId
	 *            登录人人员主键ID
	 * @param start
	 *            每页开始行
	 * @param limit
	 *            每页最大行数
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @throws Exception
	 */
	public List<MyTaskBean> getMyTaskBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId, int start,
			int limit) throws Exception;

	/**
	 * 我的任务获取登录人相关任务总数
	 * 
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @param loginPeopleId
	 *            Long 登录
	 * @throws Exception
	 */
	public int getMyTaskCountBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId) throws Exception;

	/**
	 * 我的任务获取登录人相关任务总数
	 * 
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @param loginPeopleId
	 *            Long 登录
	 * @throws Exception
	 */
	public int getApproveMyTaskCountBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId) throws Exception;

	/**
	 * 根据查询条件获取任务管理相关任务集合
	 * 
	 * @param searchTempBean
	 *            TaskSearchTempBean页面返回的查询信息
	 * @param start
	 *            每页开始行
	 * @param limit
	 *            每页最大行数
	 * @param loginPeopleId
	 *            当前登录人人员主键ID
	 * @param isAdmin
	 *            true:系统管理员
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @throws Exception
	 */
	public List<MyTaskBean> getTaskManagementBySearch(TaskSearchTempBean searchTempBean, int start, int limit,
			Long loginPeopleId, boolean isAdmin) throws Exception;

	/**
	 * 任务管理任务总数
	 * 
	 * @param searchTempBean
	 * @param loginPeopleId
	 *            当前登录人人员主键ID
	 * @param isAdmin
	 *            true:系统管理员
	 * @return List<MyTaskBean>任务集合
	 * @throws Exception
	 */
	public int getTaskManagementCountBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId, boolean isAdmin)
			throws Exception;

	/**
	 * 
	 * 假删除
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @throws Exception
	 */
	public void falseDeleteTask(Long taskId) throws Exception;

	public JecnTaskBeanNew getJecnTaskBeanNew(Long taskId) throws Exception;

	/**
	 * 
	 * @param fromPeopleId
	 *            来源人的id
	 * @param toPeopleId
	 *            替换人的id
	 * @return
	 * @throws Exception
	 */
	public void replaceApprovePeople(String fromPeopleId, String toPeopleId) throws Exception;

	/**
	 * 任务管理下载
	 * 
	 * @param listTask
	 *            任务数据源
	 * @return
	 */
	public byte[] downloadTaskManagementBySearch(List<MyTaskBean> listTask) throws Exception;

	/**
	 * 我的任务（无拟稿状态）
	 * 
	 * @param searchTempBean
	 * @param loginPeopleId
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	List<MyTaskBean> getMyTaskNoDraftStateBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId, int start,
			int limit) throws Exception;

	/**
	 * 我的任务（数量）无拟稿状态
	 * 
	 * @param searchTempBean
	 * @param loginPeopleId
	 * @return
	 * @throws Exception
	 */
	int getMyTaskCountNoDraftStateBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId) throws Exception;

	/**
	 * 之获取可审批的任务数据接口
	 * 
	 * @param loginPeopleId
	 * @param start
	 * @param limit
	 * @return
	 */
	List<MyTaskBean> getToDotasksBean(Long loginPeopleId, int start, int limit);

	/**
	 * 
	 * 只获取可审批的任务总数
	 * 
	 * @param loginPeopleId
	 * @param start
	 * @param limit
	 * @return
	 */
	int getToDotasksCount(Long loginPeopleId);

	public int getSecondAdminTaskManagementCountBySearch(TaskSearchTempBean searchTempBean, Long curPeopleId)
			throws Exception;

	public List<MyTaskBean> getSecondAdminTaskManagementBySearch(TaskSearchTempBean searchTempBean, int start,
			int limit, Long curPeopleId) throws Exception;

	List<Map<String, Object>> getTasksByUser(JecnUser jecnUser);

}
