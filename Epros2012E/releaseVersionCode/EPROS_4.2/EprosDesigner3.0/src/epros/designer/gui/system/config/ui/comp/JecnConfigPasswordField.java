package epros.designer.gui.system.config.ui.comp;

import javax.swing.JPasswordField;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

/***
 * 密码格式文本输入框
 * 
 * @author Administrator
 * 
 */
public class JecnConfigPasswordField extends JPasswordField {

	/** 数据对象 */
	private JecnConfigItemBean itemBean = null;

	public JecnConfigItemBean getItemBean() {
		return itemBean;
	}

	public void setItemBean(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
	}

}
