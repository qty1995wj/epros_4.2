package com.jecn.epros.server.dao.rule;

import com.jecn.epros.server.bean.rule.JecnRuleUseRecord;
import com.jecn.epros.server.common.IBaseDao;
/***
 * 制度使用日志
 * 2013-12-02
 * 
 */
public interface IJecnRuleUseRecordDao extends IBaseDao<JecnRuleUseRecord, String> {

}
