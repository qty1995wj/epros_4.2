package epros.designer.gui.recycle;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnRecycleMenu;
import epros.designer.util.ConnectionPool;

/***
 * 文件回收站右键菜单
 * 
 * @author Administrator
 * @date 2015-03-10
 */
public class FileRecycleMenu extends JecnRecycleMenu {
	private static Logger log = Logger.getLogger(FileRecycleMenu.class);

	public FileRecycleMenu(RecycleTree jTree) {
		super(jTree);
	}

	@Override
	protected boolean doRecycleFullData() {
		boolean flag = true;
		try {
			// 恢复当前节点以及子节点
			ConnectionPool.getFileAction().getRecycleFileIds(
					super.getRecycleNodesIdList(),JecnConstants.projectId);
		} catch (Exception e) {
			flag = false;
			log.error("FileRecycleMenu doRecycleFullData is error", e);
		}
		
		return flag;
	}

	@Override
	protected boolean doRecycleData() {
		boolean flag = true;
		try {
			// 恢复当前节点
			ConnectionPool.getFileAction().updateRecycleFile(
					super.getRecycleNodesIdList());
		} catch (Exception e) {
			flag = false;
			log.error("FileRecycleMenu doRecycleData is error", e);
		}
		return flag;
	}

	@Override
	protected boolean trueDelete() {
		boolean flag = true;
		try {
			ConnectionPool.getFileAction().deleteFiles(
					super.getRecycleNodesIdList(), JecnConstants.projectId,
					JecnConstants.getUserId());
		} catch (Exception e) {
			flag = false;
			log.error("FileRecycleMenu trueDelete is error", e);
		}
		return flag;
	}
}
