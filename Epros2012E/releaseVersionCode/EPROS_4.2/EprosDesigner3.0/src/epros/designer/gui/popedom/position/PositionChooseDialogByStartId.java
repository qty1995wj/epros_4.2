package epros.designer.gui.popedom.position;

import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;

public class PositionChooseDialogByStartId extends PositionChooseDialog {
	
	private static Logger log = Logger.getLogger(PositionChooseDialogByStartId.class);
	
	public PositionChooseDialogByStartId(List<JecnTreeBean> list,Long permOrg) {
		super(list,permOrg);
	}
	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getOrganizationAction().searchPositionByName(
					name, JecnConstants.projectId,startId);
		} catch (Exception e) {
			log.error("PositionChooseDialogByStartId searchByName is error！", e);
		}
		return null;
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyOrgPositionTreeByStartId(startId);
	}

	/**
	 * 点击表格 定位树节点
	 */
	public void isClickTable() {
		int row = this.resultTable.getSelectedRow();
		if (row == -1) {
			return;
		}
		JecnTreeBean treeBean = getListJecnTreeBean().get(row);
		try {
			if (treeBean == null) {
				return;
			}
			treeBean = ConnectionPool.getOrganizationAction()
					.getJecnTreeBeanByposId(treeBean.getId());
			if (treeBean == null) {
				return;
			}
			HighEfficiencyOrgPositionTree jTree = ((HighEfficiencyOrgPositionTree) this
					.getjTree());
			jTree.setAllowExpand(false);
			if(JecnDesignerCommon.isSecondAdmin()){
				JecnTreeCommon.searchNodePos(treeBean, jTree ,startId);
			}else{
				JecnTreeCommon.searchNodePos(treeBean, jTree);
			}
			
			jTree.setAllowExpand(true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("PositionChooseDialogByStartId isClickTable is error！", e);
		}
	}
}
