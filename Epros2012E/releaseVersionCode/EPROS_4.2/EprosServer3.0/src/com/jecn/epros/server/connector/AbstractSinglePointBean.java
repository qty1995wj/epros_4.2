package com.jecn.epros.server.connector;

import com.jecn.epros.server.action.web.login.LoginAction;

public abstract class AbstractSinglePointBean {

	protected LoginAction loginAction;

	/**
	 * 返回登录人待办总数(包含拟稿人查阅记录)
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract void getGtasksCount() throws Exception;

	public abstract void getMyTaskJson() throws Exception;

	/**
	 * 
	 * 只获取可以审批的任务记录（不包含拟稿人的查阅记录）
	 * 
	 * @throws Exception
	 */
	public abstract void getToDotasksCount() throws Exception;

	public abstract void getToDOTasksJson() throws Exception;

	public abstract void getNumberOfBacklog() throws Exception;

	public abstract void getMyTaskXml() throws Exception;

	public abstract void getToDOTasksJsonNoCallBack() throws Exception;

	public abstract void getToDotasksCountNoCallBack() throws Exception;

}
