package epros.draw.gui.operationConfig;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.event.CaretListener;

import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.workflow.dialog.JecnActiveDetailsDialog;

/**
 * 
 * 活动明细
 * 
 * @author ZHOUXY
 * 
 */
public class DrawJecnActiveDetailsDialog extends JecnActiveDetailsDialog
		implements CaretListener, ActionListener {
	/** 活动说明的JTable */
	private JTable drawActiveDescTable;
	/** 关键活动(问题区域)JTable */
	private JTable questionTable;
	/** 关键活动(关键控制点)JTable */
	private JTable keyControlTable;
	/** 关键活动(关键成功因素)JTable */
	private JTable keySuccessTable;

	/** 选择的行数 */
	private int selectRow;
	/** 是否为活动明细数据 */
	private boolean isType;

	public DrawJecnActiveDetailsDialog(JecnActivityShowBean activityShowBean,
			JTable drawActiveDescTable, JTable questionTable,
			JTable keyControlTable, JTable keySuccessTable, int selectRow,
			boolean isType) {
		super(activityShowBean.getActiveFigure());
		this.isType = isType;
		this.drawActiveDescTable = drawActiveDescTable;
		this.questionTable = questionTable;
		this.keyControlTable = keyControlTable;
		this.keySuccessTable = keySuccessTable;
		this.selectRow = selectRow;
	}

	/**
	 * 
	 * 下拉框事件或按钮点击事件
	 * 
	 * @param e
	 *            ActionEvent
	 */
	public void actionPerformedProcess(ActionEvent e) {
		super.actionPerformedProcess(e);
		if (e.getSource() == okCancelPanel.getOkBtn()) {// 确认按钮
			updateActionFigure();
			// 关闭对话框
			this.setVisible(false);

		} else if (e.getSource() == okCancelPanel.getCancelBtn()) {// 取消按钮
			// 关闭对话框
			exitJecnDialog();
		}
	}

	/**
	 * 
	 * 更新活动相关属性属性
	 * 
	 */
	public void updateActionFigure() {
		// 编号
		String num = this.activeNumTextField.getText();
		// 名称
		String name = this.activeNameTextArea.getText();
		// 说明
		String note = this.activeNoteTextArea.getText();
		// 输入
		String input = this.inputArea.getText();
		// 输出
		String output = this.outputArea.getText();
		// PA/KSF/KCP
		String keyActiveNote = this.keyActiveNoteTextArea.getText();
		// 活动UIID
		String activeUIID = "";
		if (isType) {
			if (drawActiveDescTable != null) {
				activeUIID = drawActiveDescTable.getValueAt(selectRow, 0)
						.toString();
				drawActiveDescTable.setValueAt(num, selectRow, 1);

				drawActiveDescTable.setValueAt(name, selectRow, 3);

				drawActiveDescTable.setValueAt(note, selectRow, 4);

				drawActiveDescTable.setValueAt(input, selectRow, 5);

				drawActiveDescTable.setValueAt(output, selectRow, 6);
			}
		} else {
			if (questionTable != null) {
				activeUIID = questionTable.getValueAt(selectRow, 0).toString();
				questionTable.setValueAt(num, selectRow, 1);
				questionTable.setValueAt(name, selectRow, 2);
				questionTable.setValueAt(note, selectRow, 3);
				questionTable.setValueAt(keyActiveNote, selectRow, 4);
			} else if (keyControlTable != null) {
				activeUIID = keyControlTable.getValueAt(selectRow, 0)
						.toString();
				keyControlTable.setValueAt(num, selectRow, 1);
				keyControlTable.setValueAt(name, selectRow, 2);
				keyControlTable.setValueAt(note, selectRow, 3);
				keyControlTable.setValueAt(keyActiveNote, selectRow, 4);
			} else if (keySuccessTable != null) {
				activeUIID = keySuccessTable.getValueAt(selectRow, 0)
						.toString();
				keySuccessTable.setValueAt(num, selectRow, 1);
				keySuccessTable.setValueAt(name, selectRow, 2);
				keySuccessTable.setValueAt(note, selectRow, 3);
				keySuccessTable.setValueAt(keyActiveNote, selectRow, 4);
			}
		}
		if (questionTable != null) {
			for (int index = questionTable.getModel().getRowCount() - 1; index >= 0; index--) {
				String aUIID = questionTable.getValueAt(index, 0).toString();
				if (activeUIID.equals(aUIID)) {
					questionTable.setValueAt(num, index, 1);
					questionTable.setValueAt(name, index, 2);
					questionTable.setValueAt(note, index, 3);
					questionTable.setValueAt(keyActiveNote, index, 4);
				}
			}
		}
		if (keyControlTable != null) {
			for (int index = keyControlTable.getModel().getRowCount() - 1; index >= 0; index--) {
				String aUIID = keyControlTable.getValueAt(index, 0).toString();
				if (activeUIID.equals(aUIID)) {
					keyControlTable.setValueAt(num, index, 1);
					keyControlTable.setValueAt(name, index, 2);
					keyControlTable.setValueAt(note, index, 3);
					keyControlTable.setValueAt(keyActiveNote, index, 4);
				}
			}
		}
		if (keySuccessTable != null) {
			for (int index = keySuccessTable.getModel().getRowCount() - 1; index >= 0; index--) {
				String aUIID = keySuccessTable.getValueAt(index, 0).toString();
				if (activeUIID.equals(aUIID)) {
					keySuccessTable.setValueAt(num, index, 1);
					keySuccessTable.setValueAt(name, index, 2);
					keySuccessTable.setValueAt(note, index, 3);
					keySuccessTable.setValueAt(keyActiveNote, index, 4);
				}
			}
		}
		if (drawActiveDescTable != null) {
			for (int index = drawActiveDescTable.getModel().getRowCount() - 1; index >= 0; index--) {
				String aUIID = drawActiveDescTable.getValueAt(index, 0)
						.toString();
				if (activeUIID.equals(aUIID)) {
					drawActiveDescTable.setValueAt(num, index, 1);
					drawActiveDescTable.setValueAt(name, index, 3);
					drawActiveDescTable.setValueAt(note, index, 4);
					drawActiveDescTable.setValueAt(input, selectRow, 5);
					drawActiveDescTable.setValueAt(output, selectRow, 6);
				}
			}
		}
	}
}
