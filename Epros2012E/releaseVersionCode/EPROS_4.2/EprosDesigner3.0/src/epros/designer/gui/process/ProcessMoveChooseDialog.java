package epros.designer.gui.process;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * @author yxw 2013-3-5
 * @description：流程移动
 */
public class ProcessMoveChooseDialog extends JecnMoveChooseDialog {
	private static final Logger log = Logger.getLogger(ProcessMoveChooseDialog.class);

	public ProcessMoveChooseDialog(List<JecnTreeNode> listMoveNodes, JecnTree jTree) {
		super(listMoveNodes, jTree);
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		try {
			ConnectionPool.getProcessAction().moveFlows(ids, pid, JecnConstants.getUserId(), this.getMoveNodeType());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("ProcessMoveChooseDialog savaData is error", e);
			return false;
		}

	}

	@Override
	public JecnTree getJecnTree() {
		// if (JecnConstants.isMysql) {
		// return new RoutineProcessMoveTree(this.getListIds());
		// } else {
		return new HighEfficiencyProcessMoveTree(this.getListIds());
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 名称
		return title;
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = JecnDesignerCommon.getDesignerProcessTreeBeanList(pid, false ,true);

			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}

				}
			}
		} catch (Exception e) {
			log.error("节点移动验证重名出错", e);
			return false;

		}
		return false;
	}
}
