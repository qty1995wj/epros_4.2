package com.jecn.epros.server.service.dataImport.sync.excelOrDb;

import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

public interface ImportDataFactory {
	/**
	 * 
	 * 获取待导入数据
	 * 
	 * @param importType
	 *            导入方式（excel、xml、hessian），默认是excel导入
	 * @return BaseBean 待导入的部门、岗位以及人员
	 * @throws BsException
	 */
	public BaseBean getImportData(String importType) throws Exception;

	/**
	 * 
	 * 获取配置文件之值
	 * 
	 * @param importType
	 *            String
	 * @return
	 */
	public AbstractConfigBean getInitTime(String importType) throws Exception;

}
