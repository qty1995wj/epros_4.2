package epros.designer.gui.system.seting;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.designer.gui.system.JecnToolbarButton;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 当前选中按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSelectedButton extends JButton implements MouseListener {
	private final Log log = LogFactory.getLog(JecnSelectedButton.class);

	/** 装载按钮容器 */
	protected JToolBar toolBar = null;
	/** 图片的Button */
	private JButton imageButton = null;
	/** 右边 */
	private JButton rightButton = null;

	private ActionListener listener = null;

	public JecnSelectedButton(ActionListener listener) {
		if (listener == null) {
			log.error("JecnSelectedButton is null：listener=" + listener);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.listener = listener;

		initComponents();
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	private void initComponents() {
		Dimension thisSize = new Dimension(60, 20);

		// 右边
		rightButton = new JButton();
		// 不显示选中框
		rightButton.setFocusable(false);
		// 背景颜色
		rightButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 不让绘制内容区域
		rightButton.setContentAreaFilled(false);
		// 无边框
		rightButton.setBorder(null);
		// 透明
		rightButton.setOpaque(false);

		// 图片的Button
		imageButton = new JButton();
		imageButton.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 不显示选中框
		imageButton.setFocusable(false);
		// 不让绘制内容区域
		imageButton.setContentAreaFilled(false);
		// 边框为空
		imageButton.setBorder(null);
		// 透明
		imageButton.setOpaque(false);

		// 布局管理器
		this.setLayout(new BorderLayout());
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 不显示焦点状态
		this.setFocusPainted(false);
		// 大小
		this.setPreferredSize(thisSize);
		this.setMaximumSize(thisSize);
		this.setMinimumSize(thisSize);
		// 边框为空
		this.setBorder(null);
		// 透明
		this.setOpaque(false);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());

		// 布局
		toolBar.add(this);
		this.add(imageButton, BorderLayout.WEST);
		this.add(rightButton, BorderLayout.CENTER);
		
		imageButton.addActionListener(listener);
		rightButton.addActionListener(listener);
		// 为了和groupbutton同步显示
		imageButton.addMouseListener(this);
		rightButton.addMouseListener(this);
		
	}

	public void setTextIcon(JecnToolbarButton btn) {
		if (btn != null) {
			rightButton.setText(btn.getText());
			imageButton.setIcon(btn.getIcon());
			imageButton.setActionCommand(btn.getActionCommand());
			rightButton.setActionCommand(btn.getActionCommand());
		} else {
			imageButton.setActionCommand("");
			rightButton.setActionCommand("");
		}
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		mouseTransmit(e);
	}

	public void mouseExited(MouseEvent e) {
		mouseTransmit(e);
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	/**
	 * 
	 * 鼠标事件传递
	 * 
	 * @param e
	 */
	private void mouseTransmit(MouseEvent e) {
		// 源按钮
		JButton button = (JButton) e.getSource();
		// 目标按钮
		MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(button, e,
				JecnSelectedButton.this);
		// 触发给定事件
		JecnSelectedButton.this.dispatchEvent(desMouseEvent);
	}
}