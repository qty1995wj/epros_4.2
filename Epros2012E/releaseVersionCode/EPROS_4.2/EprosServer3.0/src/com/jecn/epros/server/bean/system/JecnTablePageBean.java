package com.jecn.epros.server.bean.system;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class JecnTablePageBean implements java.io.Serializable {
	private int totalCount;
	private int curPage;
	private int totalPage;
	private List<JecnTreeBean> listJecnTreeBean;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public List<JecnTreeBean> getListJecnTreeBean() {
		return listJecnTreeBean;
	}

	public void setListJecnTreeBean(List<JecnTreeBean> listJecnTreeBean) {
		this.listJecnTreeBean = listJecnTreeBean;
	}
}

