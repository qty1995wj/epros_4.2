package epros.draw.gui.top.dialog;

import javax.swing.ImageIcon;

import epros.draw.util.JecnFileUtil;

/**
 * 
 * 对话框右上角三个按钮图标
 * 
 * @author ZHOUXY
 * 
 */
class JecnDialogIcon {
	/** 关闭按钮图片：进入时显示 */
	static ImageIcon enterExitIcon = JecnFileUtil
			.getFrameBtnIcon("enterExitFrameBtn.gif");
	/** 关闭按钮图片 */
	static ImageIcon exitIcon = JecnFileUtil
			.getFrameBtnIcon("exitFrameBtn.jpg");

	/** 最大化按钮图片：进入时显示 */
	static ImageIcon enterResizeIcon = JecnFileUtil
			.getFrameBtnIcon("enterResizeIcon.gif");
	static ImageIcon enterResizeMaxIcon = JecnFileUtil
			.getFrameBtnIcon("enterMaxFrameBtn.gif");
	/** 最大化按钮图片 */
	static ImageIcon resizeIcon = JecnFileUtil
			.getFrameBtnIcon("resizeIcon.gif");
	static ImageIcon resizeMaxIcon = JecnFileUtil
			.getFrameBtnIcon("maxFrameBtn.jpg");
}
