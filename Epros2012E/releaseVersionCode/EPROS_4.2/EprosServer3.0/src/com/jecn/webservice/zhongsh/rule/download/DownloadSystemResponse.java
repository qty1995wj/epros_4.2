
package com.jecn.webservice.zhongsh.rule.download;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DownloadSystemResult" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="documentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docContentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "downloadSystemResult",
    "documentName",
    "documentExtension",
    "docContentType"
})
@XmlRootElement(name = "DownloadSystemResponse")
public class DownloadSystemResponse {

    @XmlElement(name = "DownloadSystemResult")
    protected byte[] downloadSystemResult;
    protected String documentName;
    protected String documentExtension;
    protected String docContentType;

    /**
     * 获取downloadSystemResult属性的值。
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDownloadSystemResult() {
        return downloadSystemResult;
    }

    /**
     * 设置downloadSystemResult属性的值。
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDownloadSystemResult(byte[] value) {
        this.downloadSystemResult = value;
    }

    /**
     * 获取documentName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * 设置documentName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentName(String value) {
        this.documentName = value;
    }

    /**
     * 获取documentExtension属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentExtension() {
        return documentExtension;
    }

    /**
     * 设置documentExtension属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentExtension(String value) {
        this.documentExtension = value;
    }

    /**
     * 获取docContentType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocContentType() {
        return docContentType;
    }

    /**
     * 设置docContentType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocContentType(String value) {
        this.docContentType = value;
    }

}
