package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.util.ConfigUtils;

import java.util.List;

/**
 * 流程概况信息 封装Bean
 *
 * @author ZXH
 */
public class ProcessBaseInfoBean {
    /**
     * 流程ID
     */
    private Long flowId;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程编号
     */
    private String flowIdInput;

    public LinkResource getLink() {
        return new LinkResource(flowId, flowName, ResourceType.PROCESS);
    }

    @JsonIgnore
    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    @JsonIgnore
    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFlowIdInput() {
        return flowIdInput;
    }

    public void setFlowIdInput(String flowIdInput) {
        this.flowIdInput = flowIdInput;
    }
}
