package com.jecn.epros.server.webBean.file;

/**
 * @author weidp
 * @description
 * @date： 日期：2014-4-17 时间：下午01:58:08
 */
public class FileDetailRelatedWebBean {

	/** 文件使用情况Id **/
	private Long relatedId;
	/** 文件使用情况Name **/
	private String relatedName;
	/** 文件使用情况类型 流程 流程地图 标准 制度  制度模板 **/
	private String relatedType;

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public String getRelatedName() {
		return relatedName;
	}

	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	public String getRelatedType() {
		return relatedType;
	}

	public void setRelatedType(String relatedType) {
		this.relatedType = relatedType;
	}

	public String getHrefOfProcess() {
		return "<a href='process.action?type=process&flowId=" + this.relatedId
				+ "&isPub="
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.relatedName + "</a>";
	}

	public String getHrefOfProcessMap() {
		return "<a href='process.action?type=processMap&flowId="
				+ this.relatedId + "&isPub="
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.relatedName + "</a>";
	}

	public String getHrefOfRuleFile() {
		return "<a href='rule.action?type=ruleFile&ruleId=" + this.relatedId
				+ "&isPub='"
				+ "target='_blank' style='cursor:pointer;' class='table'>"
				+ this.relatedName + "</a>";
	}

	public String getHrefOfRuleModeFile() {
		return "<a href='rule.action?type=ruleModeFile&ruleId="
				+ this.relatedId + "&isPub='"
				+ "target='_blank' style='cursor:pointer;' class='table'>"
				+ this.relatedName + "</a>";
	}

	public String getHrefOfStandard() {
		return "<a href='standard.action?standardType=1&standardId="
				+ this.relatedId + "'"
				+ "target='_blank' style='cursor:pointer;' class='table'>"
				+ this.relatedName + "</a>";
	}
	
	public String getHrefOfPos() {
		return "<a href='orgSpecification.action?posId="
				+ this.relatedId + "'"
				+ "target='_blank' style='cursor:pointer;' class='table'>"
				+ this.relatedName + "</a>";
	}
}
