package com.jecn.epros.server.service.process.buss;

import com.jecn.epros.server.common.JecnCommonSql;

/**
 * 流程检错sql
 * 
 * @author fuzhh 2013-9-10
 * 
 */
public class ProcessSqlConstant {

	/** 流程责任人不存在 sql */
	public static String notThoseResponsible(Long projectId) {
		return "select distinct jfs.flow_id, jfs.flow_name, jfs.Flow_Id_Input"
				+ "  from JECN_FLOW_BASIC_INFO jfbiz"
				+ "  left join JECN_FLOW_STRUCTURE jfs on jfs.flow_id = jfbiz.flow_id"
				+ " where not exists (select distinct jfbi.flow_id"
				+ "          from JECN_FLOW_BASIC_INFO jfbi"
				+ "          left join JECN_FLOW_ORG_IMAGE jfoi on jfbi.Res_People_Id ="
				+ "                                                jfoi.Figure_Id"
				+ "          left join JECN_USER_POSITION_RELATED jupr on jfoi.figure_id ="
				+ "                                                       jupr.figure_id"
				+ "          left join jecn_user ju on ju.people_id = jupr.people_id"
				+ "         where jfbi.type_res_people = 1"
				+ "           and jfbiz.flow_id = jfbi.flow_id"
				+ "           and ju.people_id is not null)"
				+ "   and jfbiz.type_res_people = 1"
				+ " and jfs.ISFLOW=1 "
				+ "  and jfs.DEL_STATE = 0"
				+ " and jfs.PROJECTID=" + projectId
				+ " union "
				+ "select distinct jfs.flow_id, jfs.flow_name, jfs.Flow_Id_Input"
				+ "  from JECN_FLOW_BASIC_INFO jfbiz"
				+ "  left join JECN_FLOW_STRUCTURE jfs on jfs.flow_id = jfbiz.flow_id"
				+ "  where not exists (select distinct jfbi.flow_id"
				+ "  from JECN_FLOW_BASIC_INFO jfbi"
				+ "  left join jecn_user ju on jfbi.Res_People_Id = ju.people_id"
				+ " where jfbi.Type_Res_People = 0"
				+ " and jfbiz.flow_id = jfbi.flow_id"
				+ "   and ju.people_id is not null) and jfs.ISFLOW=1 and jfs.DEL_STATE=0 "
				+ " and jfs.PROJECTID=" + projectId;
	}

	/** 流程责任人不存在sql 数量 */
	public static String notThoseResponsibleCount(Long projectId) {
		return "select count(*) from " + " ( " + notThoseResponsible(projectId)
				+ ") countSum";
	}

	/** 流程责任部门不存在sql */
	public static String notTheDepartment(Long projectId) {
		return "select distinct jfs.flow_id, jfs.flow_name, jfs.Flow_Id_Input"
				+ "        from JECN_FLOW_STRUCTURE jfs"
				+ "        left join JECN_FLOW_RELATED_ORG jfro on jfro.flow_id ="
				+ "                                                jfs.flow_id"
				+ "       where jfro.org_id is null"
				+ "         and jfs.DEL_STATE = 0"
				+ "         and jfs.ISFLOW = 1" + " and jfs.PROJECTID="
				+ projectId;
	}

	/** 流程责任不存在sql 数量 */
	public static String notTheDepartmentCount(Long projectId) {
		return "select count(*) from " + " (" + notTheDepartment(projectId)
				+ ") countSum";
	}

	/** 流程责任不存在sql(sql server) 数量 */
	public static String notTheDepartmentSqlserverCount(Long projectId) {
		return "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID"
				+ "    in(select jfro.org_id from JECN_FLOW_STRUCTURE jfs left join JECN_FLOW_RELATED_ORG"
				+ "     jfro on jfs.flow_id = jfro.flow_id and jfs.PROJECTID=)"
				+ projectId
				+ " UNION ALL"
				+ "  SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.per_org_id and JFS.PROJECTID=)"
				+ projectId
				+ " select count(*) from "
				+ " (select distinct jfsz.flow_id, jfsz.flow_name, jfsz.Flow_Id_Input"
				+ "  from JECN_FLOW_STRUCTURE jfsz"
				+ " where not exists"
				+ "   (SELECT distinct mo.ORG_ID FROM JECN_FLOW_STRUCTURE jfs"
				+ "          left join JECN_FLOW_RELATED_ORG jfro on jfs.flow_id = jfro.flow_id"
				+ "          left join MY_ORG mo on mo.org_id = jfro.org_id"
				+ "          left join JECN_FLOW_ORG_IMAGE jfoi on jfoi.org_id = mo.org_id"
				+ "          left join JECN_USER_POSITION_RELATED jupr on jupr.figure_id ="
				+ "                                                       jfoi.figure_id"
				+ "          left join jecn_user ju on jupr.people_id = ju.people_id"
				+ "         where"
				+ "          jfsz.flow_id = jfs.flow_id"
				+ "          and jfs.ISFLOW=1"
				+ "          and ju.people_id is not null"
				+ "  )"
				+ " and jfsz.ISFLOW=1 and jfsz.DEL_STATE=0 and jfsz.PROJECTID="
				+ projectId + ") countSum";
	}

	/** 角色没有对应的岗位 sql */
	public static String roleCorrPos(Long projectId) {
		return "select distinct jfs.flow_id, jfs.flow_name, jfs.Flow_Id_Input"
				+ "  from JECN_FLOW_STRUCTURE jfs"
				+ "  left join JECN_FLOW_STRUCTURE_IMAGE jfsi on jfs.flow_id = jfsi.flow_id"
				+ "  left join PROCESS_STATION_RELATED psr on jfsi.figure_id ="
				+ "                                           psr.figure_flow_id"
				+ " where jfsi.figure_type IN" + JecnCommonSql.getOnlyRoleString()
				+ "   and jfs.ISFLOW=1" + "   and jfs.DEL_STATE=0"
				+ "   and psr.flow_station_id is null and jfs.PROJECTID="
				+ projectId
				+ " union "
				+ "select distinct jfs.flow_id, jfs.flow_name, jfs.Flow_Id_Input"
				+ "  from JECN_FLOW_STRUCTURE jfs"
				+ "  left join JECN_FLOW_STRUCTURE_IMAGE jfsi on jfs.flow_id = jfsi.flow_id"
				+ "  left join PROCESS_STATION_RELATED psr on jfsi.figure_id ="
				+ "                                           psr.figure_flow_id"
				+ "  left join JECN_FLOW_ORG_IMAGE jfoi on psr.figure_position_id ="
				+ "                                        jfoi.figure_id"
				+ " where jfsi.figure_type IN" + JecnCommonSql.getOnlyRoleString()
				+ "   and jfs.ISFLOW=1"
				+ "   and psr.type = 0"
				+ "   and jfs.DEL_STATE=0"
				+ "   and jfoi.figure_id is null and jfs.PROJECTID="
				+ projectId
				+ " union "
				+ "select distinct jfs.flow_id, jfs.flow_name, jfs.Flow_Id_Input"
				+ "  from JECN_FLOW_STRUCTURE jfs"
				+ "  left join JECN_FLOW_STRUCTURE_IMAGE jfsi on jfs.flow_id = jfsi.flow_id"
				+ "  left join PROCESS_STATION_RELATED psr on jfsi.figure_id ="
				+ "                                           psr.figure_flow_id"
				+ " where jfsi.figure_type IN" + JecnCommonSql.getOnlyRoleString()
				+ "   and psr.type = 1"
				+ "   and jfs.ISFLOW=1"
				+ "   and jfs.DEL_STATE=0"
				+ "   and not EXISTS"
				+ " (SELECT 1"
				+ "          from JECN_FLOW_ORG_IMAGE jfoi"
				+ "          left join JECN_POSITION_GROUP_R jpgr on jfoi.figure_id ="
				+ "                                                  jpgr.figure_id"
				+ "          left join JECN_POSITION_GROUP jpg on jpg.id = jpgr.group_id"
				+ "         where psr.figure_position_id = jpg.id"
				+ "           and jfoi.figure_id is not null) and jfs.PROJECTID="
				+ projectId;
	}

	/** 角色没有对应的岗位 sql 数量 */
	public static String roleCorrPosCount(Long projectId) {
		return "select count(*) from " + " (" + roleCorrPos(projectId)
				+ ") countSum";
	}
}
