package com.jecn.epros.server.bean.download;

import java.io.Serializable;

public class JecnCreateDoc implements Serializable {
	// 文件流
	private byte[] bytes;
	// 文件名称
	private String fileName;
	
	private String idInput;

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getIdInput() {
		return idInput;
	}

	public void setIdInput(String idInput) {
		this.idInput = idInput;
	}
	
}
