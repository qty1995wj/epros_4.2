package epros.designer.gui.popedom.person;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 人员管理搜索结果table
 * 
 * @ClassName: PersonManageTable
 * @Description: TODO
 * @author ZXH
 */
public class PersonManageTable extends JTable {
	private PersonManageDialog manageDialog;

	PersonManageTable(PersonManageDialog manageDialog) {
		this.setModel(getTableModel());
		this.manageDialog = manageDialog;
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());

		// this.setRowSelectionAllowed(false);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {// 双击
					// 编辑处理
					isClickTableEdit();
				}
			}
		});
	}
	
	private void isClickTableEdit() {
		manageDialog.editBtnAction();
	}
	
	public PersonManageTableMode getTableModel() {
		Vector<String> title=new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("loginName"));
		title.add(JecnProperties.getValue("realName"));
		title.add(JecnProperties.getValue("email"));
		title.add(JecnProperties.getValue("isLogin"));
		return new PersonManageTableMode(null, title);
	}

	
	public boolean isSelectMutil() {
		return false;
	}

	
	public int[] gethiddenCols() {
		
		return new int[]{0};
	}
	
	class PersonManageTableMode extends DefaultTableModel{
		public PersonManageTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data,title);
			
		}

		@Override
		public Class getColumnClass(int columnIndex){
			return getValueAt(0, columnIndex).getClass();
		}
		public   boolean   isCellEditable(int   rowindex,int   colindex){ 
           return false;
		} 


	}

}
