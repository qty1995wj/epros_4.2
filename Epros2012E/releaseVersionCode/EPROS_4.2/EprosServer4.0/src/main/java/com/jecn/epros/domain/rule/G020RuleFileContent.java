package com.jecn.epros.domain.rule;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 制度文件内容表 本地上传方式存储到数据库
 *
 * @author hyl
 */
public class G020RuleFileContent implements java.io.Serializable {
    /**
     * 主键
     */
    private Long id;
    /**
     * 制度主表主键
     */
    private Long ruleId;
    /**
     * 文件内容 存放数据库
     */
    private byte[] fileStream;
    /**
     * 文件路径 存放本地
     */
    private String filePath;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateTime;
    /**
     * 更新人
     */
    private Long updatePeopleId;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件版本存储标识  0:本地；1：数据库
     */
    private int isVersionLocal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public byte[] getFileStream() {
        return fileStream;
    }

    public void setFileStream(byte[] fileStream) {
        this.fileStream = fileStream;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdatePeopleId() {
        return updatePeopleId;
    }

    public void setUpdatePeopleId(Long updatePeopleId) {
        this.updatePeopleId = updatePeopleId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getIsVersionLocal() {
        return isVersionLocal;
    }

    public void setIsVersionLocal(int isVersionLocal) {
        this.isVersionLocal = isVersionLocal;
    }


}
