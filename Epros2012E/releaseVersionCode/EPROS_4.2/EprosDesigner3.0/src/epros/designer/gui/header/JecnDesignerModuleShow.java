package epros.designer.gui.header;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

import epros.designer.util.JecnProperties;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.header.JecnModuleShowPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 
 * 设计器显示/隐藏树面板/流程向导处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesignerModuleShow implements ItemListener {

	/** 树面板显示复选框 */
	private JCheckBox treePaneCheckBox = null;
	/** 流程导向显示复选框 */
	private JCheckBox flowGuideCheckBox = null;

	/** 元素属性 */
	private JCheckBox eleFigureCheckBox = null;

	private JecnSplitPane allSplitPane = null;
	private JecnSplitPane partSplitPane = null;

	private JecnSplitPane leftSplitPane = null;
	/**  */
	private JecnModuleShowPanel moduleShowPanel = null;

	/** true：双击JecnDrawTabbedPane不执行监听 */
	private boolean isChecked = false;

	public JecnDesignerModuleShow(JecnSplitPane allSplitPane, JecnSplitPane partSplitPane,
			JecnModuleShowPanel moduleShowPanel, JecnSplitPane leftSplitPane) {
		this.allSplitPane = allSplitPane;
		this.partSplitPane = partSplitPane;
		this.moduleShowPanel = moduleShowPanel;
		this.leftSplitPane = leftSplitPane;
		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 树面板显示复选框
		treePaneCheckBox = new JCheckBox(JecnProperties.getValue("showTree"));
		// 流程导向显示复选框
		flowGuideCheckBox = new JCheckBox(JecnProperties.getValue("showFlowElemGuied"));

		eleFigureCheckBox = new JCheckBox(JecnProperties.getValue("flowElemAttr"));
		// 背景颜色
		treePaneCheckBox.setOpaque(false);
		flowGuideCheckBox.setOpaque(false);
		eleFigureCheckBox.setOpaque(false);

		// 选中是显示焦点框
		treePaneCheckBox.setFocusable(false);
		flowGuideCheckBox.setFocusable(false);
		eleFigureCheckBox.setFocusable(false);

		// 事件
		treePaneCheckBox.addItemListener(this);
		flowGuideCheckBox.addItemListener(this);
		eleFigureCheckBox.addItemListener(this);

		// 默认
		treePaneCheckBox.setSelected(true);
		flowGuideCheckBox.setSelected(true);
		eleFigureCheckBox.setSelected(false);
	}

	private void initLayout() {
		moduleShowPanel.insertCheckBox(eleFigureCheckBox, 0);
		moduleShowPanel.insertCheckBox(flowGuideCheckBox, 0);
		moduleShowPanel.insertCheckBox(treePaneCheckBox, 0);
	}

	public void itemStateChanged(ItemEvent e) {
		if (isChecked) {// 双击table面板，不执行复选框监听
			return;
		}
		proTwoSplitPane(allSplitPane, partSplitPane, treePaneCheckBox, flowGuideCheckBox, eleFigureCheckBox);
		JecnDrawMainPanel.getMainPanel().revalidate();
		JecnDrawMainPanel.getMainPanel().repaint();

	}

	/**
	 * 
	 * 条件：allSplitPane中一端包含partSplitPane组件
	 * partSplitPane中left面板和right面板显示/隐藏导致allSplitPane的变化
	 * 
	 * @param allSplitPane
	 *            JecnSplitPane 外面一个分割面板
	 * @param partSplitPane
	 *            JecnSplitPane allSplitPane的子组件，放置到其中一个面板
	 * @param leftBox
	 *            JCheckBox 对应分割面板左面板的单选框
	 * @param rightBox
	 *            JCheckBox 对应分割面板右面板的单选框
	 */
	private void proTwoSplitPane(JecnSplitPane allSplitPane, JecnSplitPane partSplitPane, JCheckBox leftBox,
			JCheckBox rightBox, JCheckBox eleFigureCheckBox) {
		// 记录分隔条的位置点
		// 外面一个分割面板
		allSplitPane.initdividerLocing();
		// 内部一个分割面板
		partSplitPane.initdividerLocing();

		if (leftBox.isSelected() || rightBox.isSelected()) {// 其中一个选中
			// 画图面板&流程元素库都显示
			allSplitPane.showBothPanel();
		} else {// 都没有选中
			// 只显示画图面板
			allSplitPane.showRightPanel();
		}

		if (leftBox.isSelected() && rightBox.isSelected()) {// 两个都选中
			// 树面板&流程设计向导都显示
			partSplitPane.showBothPanel();
		} else if (leftBox.isSelected()) {// 树面板选中
			// 只显示流程元素
			partSplitPane.showLeftPanel();
		} else if (rightBox.isSelected()) {// 流程设计向导选中
			// 只显示流程设计向导
			partSplitPane.showRightPanel();
		}

//		if (eleFigureCheckBox.isSelected()) {
//			leftSplitPane.showBothPanel();
//		} else {
//			if (leftSplitPane == null) {
//				return;
//			}
//			leftSplitPane.showLeftPanel();
//		}
	}

	public void hidderEleFigure() {
		leftSplitPane.showLeftPanel();
	}

	public void showEleFigure() {
		leftSplitPane.showBothPanel();
	}

	public JCheckBox getTreePaneCheckBox() {
		return treePaneCheckBox;
	}

	public JCheckBox getFlowGuideCheckBox() {
		return flowGuideCheckBox;
	}

	public JecnSplitPane getAllSplitPane() {
		return allSplitPane;
	}

	public JecnSplitPane getPartSplitPane() {
		return partSplitPane;
	}

	public JecnModuleShowPanel getModuleShowPanel() {
		return moduleShowPanel;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public JCheckBox getEleFigureCheckBox() {
		return eleFigureCheckBox;
	}

}
