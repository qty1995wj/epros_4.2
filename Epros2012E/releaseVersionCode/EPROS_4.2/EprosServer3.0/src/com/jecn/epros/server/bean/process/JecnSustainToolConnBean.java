package com.jecn.epros.server.bean.process;

import java.util.Date;

/**
 * 与支持工具关联临时表Bean
 * @Time 2014-10-24
 *
 */
public class JecnSustainToolConnBean  implements java.io.Serializable {
	
	/**主键ID*/
	private String id;
	/**支持工具Id*/
	private Long sustainToolId;
	/**关联Id*/
	private Long relatedId;
	/**关联类型*/
	private Integer relatedType;
	/**创建时间*/
	private Date createTime;
	/**更新时间*/
	private Date updateTime;
	/**创建人*/
	private Long createPeopleId;
	/**更新人*/
	private Long updatePeopleId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getSustainToolId() {
		return sustainToolId;
	}
	public void setSustainToolId(Long sustainToolId) {
		this.sustainToolId = sustainToolId;
	}
	public Long getRelatedId() {
		return relatedId;
	}
	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}
	public Integer getRelatedType() {
		return relatedType;
	}
	public void setRelatedType(Integer relatedType) {
		this.relatedType = relatedType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Long getCreatePeopleId() {
		return createPeopleId;
	}
	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}
	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}
	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

}
