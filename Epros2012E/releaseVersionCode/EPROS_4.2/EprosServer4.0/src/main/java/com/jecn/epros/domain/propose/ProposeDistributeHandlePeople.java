package com.jecn.epros.domain.propose;

import io.swagger.annotations.ApiModelProperty;

public class ProposeDistributeHandlePeople {

    @ApiModelProperty(value = "关联的流程、制度、文件等id")
    private String relatedId;
    @ApiModelProperty(value = "0流程，1制度，2流程架构，3文件，4风险，5标准")
    private Integer type;
    @ApiModelProperty(value = "处理人")
    private Long handlePeople;

    public String getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(String relatedId) {
        this.relatedId = relatedId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getHandlePeople() {
        return handlePeople;
    }

    public void setHandlePeople(Long handlePeople) {
        this.handlePeople = handlePeople;
    }


}
