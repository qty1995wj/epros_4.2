package com.jecn.epros.server.bean.file;

import java.io.Serializable;

public class FileOpenBean implements Serializable{
	private long id;
	private String name;
	private byte[] fileByte;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public byte[] getFileByte() {
		return fileByte;
	}
	public void setFileByte(byte[] fileByte) {
		this.fileByte = fileByte;
	}
	
}
