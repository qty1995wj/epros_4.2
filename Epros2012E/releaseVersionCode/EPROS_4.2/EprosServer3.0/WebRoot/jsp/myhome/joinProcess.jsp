<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<SCRIPT type="text/javascript">
		</SCRIPT>
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="joinProcessSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td height="25px" width="10%" align="right">
								<!-- 流程名称： -->
								<s:text name="processNameC" />
							</td>
							<td width="35%" align="left">
								<input style="width: 80%;" type="text" id="processName_id"
									maxLength="32" />
							</td>

							<td width="10%" align="right">
								<!-- 流程编号： -->
								<s:text name="processNumberC"></s:text>
							</td>
							<td width="35%" align="left">
								<input style="width: 80%;" type="text" id="processNumber_id"
									maxLength="32" />
							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a onClick="toggleDiv(joinProcessSearchText,'packUp','searchVal')"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td height="25px" align="right">

								<!-- 岗位名称: -->
								<s:text name="positionNameC" />
							</td>
							<td align="left">
								<select style="width: 80%;" id="positionName_id">
									<option value="-1">
										<s:text name="all" />
									</option>
									<s:iterator value="#session.webLoginBean.listPosBean">
										<option value='<s:property value="posId"/>'>
											<s:property value="posName" />
										</option>
									</s:iterator>
								</select>
							</td>

							<td></td>
							<td></td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/common/search.gif"
						onclick="joinProcessSearch();" />
					<img style="cursor: pointer;"
						src="${basePath}images/common/reset.gif"
						onclick="joinProcessReset()" />
				</div>
			</div>
			<div id="myJoinprocessShow_id"></div>
		</div>
	</body>
</html>