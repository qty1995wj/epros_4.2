package com.jecn.dataimport.util;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * 聚光科技,我的流程
 * 
 */
public class EipMyFlowBean implements Serializable {
	private String processId;// 流程ID
	private String positionName;// 岗位名称
	private String roleName;// 执行角色
	private String roleNameCount;// 执行角色个数
	private String flowInputId;// 流程编号
	private String processName;// 流程名称
	private List<ActivityShowBean> activityList;// 活动
	private Date createTime;// 流程创建时间

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleNameCount() {
		return roleNameCount;
	}

	public void setRoleNameCount(String roleNameCount) {
		this.roleNameCount = roleNameCount;
	}

	public String getFlowInputId() {
		return flowInputId;
	}

	public void setFlowInputId(String flowInputId) {
		this.flowInputId = flowInputId;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public List<ActivityShowBean> getActivityList() {
		return activityList;
	}

	public void setActivityList(List<ActivityShowBean> activityList) {
		this.activityList = activityList;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	};

}
