package com.jecn.epros.server.service.task.buss;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.temp.TaskSearchBean;
import com.jecn.epros.server.bean.task.temp.TaskTypeNameBean;
import com.jecn.epros.server.bean.task.temp.TempAuditPeopleBean;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.JecnSqlConstant;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.rule.RuleHistoryDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.download.word.SelectDownloadProcess;
import com.jecn.epros.server.service.control.buss.DocControlCommon;
import com.jecn.epros.server.service.vatti.buss.DominoOperation;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.webservice.mengniu.arch.ArchiveF;
import com.jecn.webservice.mengniu.arch.ArchiveFile;
import com.jecn.webservice.mengniu.arch.ArchiveService;

/**
 * 任务审批通用类
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：May 7, 2013 时间：11:10:15 AM
 */
public class JecnTaskCommon {
	/** 任务审批阶段个数 */
	private static final int TASK_STATE_VALUE = 14;

	public static final String ALL = JecnUtil.getValue("all");
	public static final String PROCESS_TASK = JecnUtil.getValue("processTask");
	public static final String FILE_TASK = JecnUtil.getValue("fileTask");
	public static final String RULE_TASK = JecnUtil.getValue("ruleTask");
	public static final String MAP_TASK = JecnUtil.getValue("mapTask");
	public static final String FINISH = JecnUtil.getValue("complete");
	public static final String FINISHING_VIEWS = JecnUtil.getValue("finishingViews");
	/** 整理意见状态 */
	private static final String finView = "views";

	public static final String DRAFT = "DRAFT";// JecnUtil.getValue("draft");
	/** 人员离职或岗位变更 */
	public static String STAFF_CHANGE = "STAFF_CHANGE";// JecnUtil.getValue("peoLeaveLinkManager");
	/** 人员已离职或没有岗位，请联系管理员！ */
	public static String STAFF_TURNOVER_OR_NO_JOB = "STAFF_TURNOVER_OR_NO_JOB";// JecnUtil.getValue("staffTurnoverOrNoJob");
	/** TASK_NOTHIN */
	public static String TASK_NOTHIN = "无";// JecnUtil.getValue("none");
	/** APPROVE_ERROR */
	public static String APPROVE_ERROR = "APPROVE_ERROR";// JecnUtil.getValue("taskApprovalException");
	/** "密级由(秘密)更改为(公开)" */
	public static String CHANGE_SECRET_TO_PUBLIC = JecnUtil.getValue("secretUpdatedPublic");
	/** "密级由(公开)更改为(秘密)" */
	public static String CHANGE_PUBLIC_TO_SECRET = JecnUtil.getValue("secretUpdatedSecret");
	/** "类别由(" */
	public static String TYPE_FROM = JecnUtil.getValue("typBy");
	/** ")更改为(" */
	public static String TO_TYPE = JecnUtil.getValue("changedTo");
	/** "部门权限由(" */
	public static String ROG_FROM = JecnUtil.getValue("depatCompetenceBy");
	/** "岗位权限由(" */
	public static String POS_FROM = JecnUtil.getValue("postionCompetenceBy");
	/** "岗位组权限由(" */
	public static String GRO_FROM = JecnUtil.getValue("groupCompetenceBy");

	/** 审批成功！ */
	public static String APPROVE_SUCESS = "APPROVE_SUCESS";// JecnUtil.getValue("APPROVE_SUCESS");
	/** 任务已审批！ */
	public static String APPROVE_IS_SUBMIT = "APPROVE_IS_SUBMIT";// JecnUtil.getValue("APPROVE_IS_SUBMIT");
	/** 访问出错！ */
	public static String visitError = "VISIT_ERROR";// JecnUtil.getValue("visitError");
	/** 当前审批阶段审批人不能为空！ */
	public static String CUR_APP_PEOPLE_NOT_NULL = "NOW_APP_PEOPLE_NOT_NULL";// JecnUtil.getValue("NOW_APP_PEOPLE_NOT_NULL");
	/** 编辑成功 */
	public static String EDIT_TASK_SUCESS = "EDIT_SUCESS";// JecnUtil.getValue("EDIT_SUCESS");
	/** 当前操作人和返回目标人相同！ */
	public static String CUR_PEOPLE_IS_SAME_TO_PEOPLE = "CUR_PEOPLESAMETO_PEOPLE"; // JecnUtil.getValue("curPeopleSameToPeople");
	/** 是否华帝登录 */
	private static boolean isVatti = JecnUtil.isVattiLogin();

	private static Logger log = JecnLog.getLog();

	/** （已离职） */
	public static String LEAVE_OFFICE = JecnUtil.getValue("leaveOffice");
	/** 任务已失效！ */
	public static String TASK_IS_EXPIRE = "TASK_IS_EXPIRE";// JecnUtil.getValue("taskIsExpire");
	/** 会审人员已离职或没有岗位，请联系管理员！ */
	public static String REVIEW_PEOPLE_NOT_NULL = "REVIEW_PEOPLE_NOT_NULL";// JecnUtil.getValue("reviewPeopleNotNull");
	/** 任务已审批或已失效！ */
	public static String TASK_APPROVE_OR_PEOPLE_LIVE = "TASK_APPROVE_OR_PEOPLE_LIVE";// JecnUtil.getValue("taskApproveOrPeopleLive");
	/** 任务审批参数异常 */
	public static String TASK_PARAM_ERROR = "TASK_PARAM_ERROR";// JecnUtil.getValue("taskParamError");
	/** 任务审批参数异常 */
	public static String TASK_APPROVE_REPEAT = "TASK_APPROVE_REPEAT";

	/** 撤回成功！ */
	public static String CALLBACK_SUCESS = "CALLBACK_SUCESS";// JecnUtil.getValue("CALLBACK_SUCESS");

	/**
	 * 字符串ID去空格拼装
	 * 
	 * @param accString
	 * @return
	 */
	public static String getAccessByStr(String accString) {
		if (accString == null || "".equals(accString)) {
			return "";
		}
		String[] strArr = accString.split(",");
		String accessIds = "";
		for (int i = 0; i < strArr.length; i++) {
			String str = strArr[i];
			if (str == null || "".equals(str.trim())) {
				continue;
			}
			if (accessIds == "") {
				accessIds = str.trim();
			} else {
				accessIds = accessIds + "," + str.trim();
			}
		}
		return accessIds;
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	public static boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * 根据获取的object对象返回intvalue
	 * 
	 * @param object
	 *            Object int值的对象
	 * @return int
	 */
	public static int getIntByObject(Object object) {
		if (object == null || "".equals(object.toString())) {
			return -1;
		}

		return Integer.valueOf(object.toString()).intValue();
	}

	/**
	 * 根据获取的object对象返回intvalue
	 * 
	 * @param object
	 *            Object int值的对象
	 * @return int
	 */
	public static long getlongByObject(Object object) {
		if (object == null || "".equals(object.toString())) {
			return -1;
		}

		return Long.valueOf(object.toString()).longValue();
	}

	/**
	 * 验证两个对象的值是否相同
	 * 
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static boolean isSameStr(Object obj1, Object obj2) {
		if (!isNullObj(obj1) && !isNullObj(obj2) && obj1.toString().trim().equals(obj2.toString().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * 验证开始时间和结束时间是否在
	 * 
	 * @param startTime
	 * @param endTime
	 * @return true 开始时间和结束时间都不为空
	 */
	public static boolean isRightTime(String startTime, String endTime) {
		if (JecnCommon.isNullOrEmtryTrim(startTime) || JecnCommon.isNullOrEmtryTrim(endTime)) {// 开始时间和结束时间不能为空
			return false;
		}
		return true;
	}

	/**
	 * 字符串换行符替换
	 * 
	 * @param str
	 * @return
	 */
	public static String replaceAll(String str) {
		if (JecnCommon.isNullOrEmtryTrim(str)) {
			return "";
		}
		return str.replaceAll("\n", "<br>");
	}

	/**
	 * 通过人员id获得人员的姓名
	 * 
	 * @param listObj
	 * @param peopleId
	 * @return String 人员真实姓名
	 */
	public static String getTrueNameByPeopleId(List<Object[]> listObj, Long peopleId) {
		for (Object[] obj : listObj) {
			// 0:peopleId；1：真实姓名
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			if (peopleId.toString().equals(obj[0].toString())) {// 存在相同人员ID
				if (obj[2].toString().equals("1")) {// 该人员记录已删除
					return obj[1].toString() + JecnTaskCommon.LEAVE_OFFICE;// （已离职）
				} else {
					return obj[1].toString();
				}
			}
		}
		// 人员离职
		return TASK_NOTHIN;
	}

	/**
	 * 打回整理验证 （审批人必须存在评审人会审阶段后）
	 * 
	 * @param orderList
	 *            审批顺序
	 * @param state
	 *            当前任务状态 当前阶段审批人必须在评审人会审阶段之后审批才可以存在打回整理按钮操作
	 * @return
	 * @throws BsException
	 */
	public static boolean isCallBack(List<Integer> orderList, int state, List<TempAuditPeopleBean> listAuditPeopleBean)
			throws Exception {
		if (listAuditPeopleBean == null || orderList == null) {
			throw new NullPointerException("打回整理状态验证异常！listAuditPeopleBean==null || orderList==null");
		}
		// 验证是否存在评审人阶段】
		if (!isExitReviewPeople(listAuditPeopleBean)) {// 不存在评审人阶段，责没有打回整理状态
			return false;
		}
		// // 当前任务状态标识
		// String stateType = JecnTaskCommon.getStateType(state);
		// 评审人对应数组中的位置
		int reviewCount = 0;
		// 当前阶段对应数组的位置
		int stateCount = 0;
		for (int i = 0; i < orderList.size(); i++) {
			if (state == orderList.get(i)) {
				stateCount = i;// 当前阶段位置
			} else if (3 == orderList.get(i)) {
				reviewCount = i;// 评审人所在位置
			}
		}
		if (stateCount > reviewCount) {// 当前阶段审批人必须在评审人会审阶段之后审批才可以存在打回整理按钮操作
			return true;
		}
		return false;
	}

	/**
	 * 验证是否存在评审人阶段
	 * 
	 * @param listAuditPeopleBean
	 *            各阶段审批人信息
	 * @return true 存在评审人阶段 false 不存在评审人阶段
	 */
	private static boolean isExitReviewPeople(List<TempAuditPeopleBean> listAuditPeopleBean) {
		if (listAuditPeopleBean == null) {
			throw new NullPointerException("验证是否存在评审人阶段异常！listAuditPeopleBean == null");
		}
		for (int i = 0; i < listAuditPeopleBean.size(); i++) {
			TempAuditPeopleBean auditPeopleBean = listAuditPeopleBean.get(i);
			if (auditPeopleBean.getState() == 3 && auditPeopleBean.getAuditId() != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 当前审批阶段是否为文控或部门审核阶段
	 * 
	 * @param state
	 *            任务审批阶段
	 * @return True:文控或部门审核阶段
	 */
	public static boolean isControlState(int state) {
		if (state == 1 || state == 2) {
			return true;
		}
		return false;
	}

	/**
	 * 根据系统配置获取对应的state阶段名称
	 * 
	 * @param state
	 *            任务阶段 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
	 *            7：IT总监审批阶段 8:事业部经理 9：总经理
	 * @param list
	 *            List<JecnConfigItemBean> 系统审批人配置
	 * @return
	 */
	public static TempAuditPeopleBean getTempAuditPeopleBean(int state, List<JecnConfigItemBean> list) {
		if (state == 0) {
			// 添加拟稿人阶段
			TempAuditPeopleBean peopleBean = getDraftPeopleBean();
			peopleBean.setState(state);
			peopleBean.setIsShow(1);
			return peopleBean;
		}
		// 根据任务审批状态获取任务配置对于的mark唯一标识
		String mark = getItemMark(state);
		if (JecnCommon.isNullOrEmtryTrim(mark)) {// 不存在审批阶段
			throw new IllegalArgumentException("根据任务审批状态获取系统配置mark标识失败！");
		}
		TempAuditPeopleBean auditPeopleBean = null;
		if (state == 5) {// 完成
			auditPeopleBean = new TempAuditPeopleBean();
			// 审核阶段名称 "完成"
			auditPeopleBean.setAuditLable(FINISH);
			// 记录当前审核阶段对应任务的状态
			auditPeopleBean.setState(state);
			return auditPeopleBean;
		} else if (state == 10) {// 整理意见
			auditPeopleBean = new TempAuditPeopleBean();
			// 审核阶段名称 ‘整理意见’
			auditPeopleBean.setAuditLable(FINISHING_VIEWS);
			// 记录当前审核阶段对应任务的状态
			auditPeopleBean.setState(state);
			auditPeopleBean.setIsShow(1);
			return auditPeopleBean;
		}
		for (JecnConfigItemBean itemBean : list) {
			if (mark.equals(itemBean.getMark())) {
				auditPeopleBean = new TempAuditPeopleBean();
				// 审核阶段名称
				auditPeopleBean.setAuditLable(itemBean.getDefaultName());
				// 记录当前审核阶段对应任务的状态
				auditPeopleBean.setState(state);
				// 是否显示
				auditPeopleBean.setIsShow(1);

				break;
			}
		}
		return auditPeopleBean;
	}

	/**
	 * 获取你搞阶段
	 * 
	 * @return
	 */
	private static TempAuditPeopleBean getDraftPeopleBean() {
		TempAuditPeopleBean peopleBean = new TempAuditPeopleBean();
		// 审核阶段名称 "拟稿"
		peopleBean.setAuditLable(DRAFT);
		peopleBean.setState(0);
		return peopleBean;
	}

	/**
	 * 根据任务类型获取对应的系统审批阶段配置项信息
	 * 
	 * @param taskType
	 *            任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
	 * @param state
	 *            任务阶段 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
	 *            7：IT总监审批阶段 8:事业部经理 9：总经理
	 * @return
	 */
	public static List<TempAuditPeopleBean> getTempAuditPeopleBeanList(JecnTaskBeanNew taskBeanNew,
			List<JecnTaskStage> taskStageList) {
		List<TempAuditPeopleBean> listPeopleBean = new ArrayList<TempAuditPeopleBean>();
		// 是否执行过 2:为当前任务 1：已审批，0 未审批
		int isExecute = 1;
		for (JecnTaskStage taskStage : taskStageList) {
			// 当前任务阶段
			if (taskStage.getStageMark().intValue() == taskBeanNew.getState()) {
				isExecute = 2;
				initTempAuditPeopleBean(listPeopleBean, isExecute, taskStage);
				isExecute = 0;
				continue;
			}
			initTempAuditPeopleBean(listPeopleBean, isExecute, taskStage);
		}
		return listPeopleBean;
	}

	private static void initTempAuditPeopleBean(List<TempAuditPeopleBean> listPeopleBean, int isExecute,
			JecnTaskStage taskStage) {
		TempAuditPeopleBean auditPeopleBean = new TempAuditPeopleBean();
		// 审核阶段名称
		auditPeopleBean.setAuditLable(taskStage.getStageName());
		// 记录当前审核阶段对应任务的状态
		auditPeopleBean.setState(taskStage.getStageMark());
		// 记录当前审核阶段信息主键
		auditPeopleBean.setStageId(taskStage.getId());
		// 执行过 是否审批 isExecute 2:为当前任务 1：已审批，0 未审批
		auditPeopleBean.setIsApproval(isExecute);
		// 添加是否显示
		auditPeopleBean.setIsShow(taskStage.getIsShow());
		// 添加是否为空
		auditPeopleBean.setIsEmpty(taskStage.getIsEmpty());
		// 添加是否选择人
		// 添加集合
		auditPeopleBean.setIsSelectedUser(taskStage.getIsSelectedUser());
		listPeopleBean.add(auditPeopleBean);
	}

	/**
	 * 根据任务审批状态获取任务配置对于的mark唯一标识
	 * 
	 * @param state
	 *            任务审批状态
	 * @return String MARK 任务审批环节唯一标识
	 */
	public static String getItemMark(int state) {
		String mark = "";
		switch (state) {
		case 0:
			mark = "draft";
			break;
		case 1:// 文控审核人
			mark = ConfigItemPartMapMark.taskAppDocCtrlUser.toString();
			break;
		case 2:// 部门审核人
			mark = ConfigItemPartMapMark.taskAppDeptUser.toString();
			break;
		case 3:// 评审人会审
			mark = ConfigItemPartMapMark.taskAppReviewer.toString();
			break;
		case 4:// 批准人审核
			mark = ConfigItemPartMapMark.taskAppPzhUser.toString();
			break;
		case 5:// 批准人审核
			mark = "Complete";
			break;
		case 6: // 各业务体系负责人
			mark = ConfigItemPartMapMark.taskAppOprtUser.toString();
			break;
		case 7: // IT总监
			mark = ConfigItemPartMapMark.taskAppITUser.toString();
			break;
		case 8:// 事业部经理
			mark = ConfigItemPartMapMark.taskAppDivisionManager.toString();
			break;
		case 9:// 总经理
			mark = ConfigItemPartMapMark.taskAppGeneralManager.toString();
			break;
		case 10:// 整理意见
			mark = finView;
			break;
		case 11:// 自定义1
			mark = ConfigItemPartMapMark.taskCustomApproval1.toString();
			break;
		case 12://
			mark = ConfigItemPartMapMark.taskCustomApproval2.toString();
			break;
		case 13:// 
			mark = ConfigItemPartMapMark.taskCustomApproval3.toString();
			break;
		case 14:// 
			mark = ConfigItemPartMapMark.taskCustomApproval4.toString();
			break;
		}
		return mark;
	}

	/**
	 * 根据任务审批阶段的mark获得该阶段的state值
	 * 
	 * @param state
	 *            任务审批状态
	 * @return String MARK 任务审批环节唯一标识
	 * @throws Exception
	 */
	public static int getStateByMark(ConfigItemPartMapMark mark) throws Exception {

		switch (mark) {
		case taskAppDocCtrlUser:// 文控审核人
			return 1;
		case taskAppDeptUser:// 部门审核人
			return 2;
		case taskAppReviewer:// 评审人会审
			return 3;
		case taskAppPzhUser:// 批准人审核
			return 4;
		case taskAppOprtUser: // 各业务体系负责人
			return 6;
		case taskAppITUser: // IT总监
			return 7;
		case taskAppDivisionManager:// 事业部经理
			return 8;
		case taskAppGeneralManager:// 总经理
			return 9;
		case taskCustomApproval1:// 自定义1
			return 11;
		case taskCustomApproval2://
			return 12;
		case taskCustomApproval3:// 
			return 13;
		case taskCustomApproval4:// 
			return 14;
		}

		throw new Exception("查找mark为" + mark.toString() + "对应的审批阶段异常");

	}

	/**
	 * 获取系统配置的任务审批环节
	 * 
	 * @return
	 */
	public static List<JecnConfigItemBean> getJecnConfigItemBeanList(int taskType) {
		// 获取系统配置审批环节
		List<JecnConfigItemBean> list = JecnContants.allApproveList;
		// switch (taskType) {
		// case 0:
		// list = JecnContants.partMapList;
		// break;
		// case 1:
		// list = JecnContants.fileList;
		// break;
		// case 2:
		// case 3:
		// list = JecnContants.roleList;
		// break;
		// case 4:
		// list = JecnContants.totalMapList;
		// break;
		//
		// default:
		// break;
		// }
		return list;
	}

	/**
	 * 获取个任务阶段任务类型名称
	 * 
	 * @return List<TaskTypeNameBean>
	 */
	public static List<TaskTypeNameBean> getTaskTypeNameBeanList() {
		List<TaskTypeNameBean> listTaskTypeNameBean = new ArrayList<TaskTypeNameBean>();
		TaskTypeNameBean taskTypeNameBean = new TaskTypeNameBean();
		taskTypeNameBean.setTaskType(-1);
		// "全部"
		taskTypeNameBean.setTypeName(ALL);
		listTaskTypeNameBean.add(taskTypeNameBean);

		// 1 隐藏制度
		String isHiddenSystem = JecnContants.getValue(ConfigItemPartMapMark.isHiddenSystem.toString());
		for (int i = 0; i < 5; i++) {
			taskTypeNameBean = new TaskTypeNameBean();
			taskTypeNameBean.setTaskType(i);
			switch (i) {
			case 0:// "流程任务"
				taskTypeNameBean.setTypeName(PROCESS_TASK);
				listTaskTypeNameBean.add(taskTypeNameBean);
				break;
			case 1:// "文件任务"
				taskTypeNameBean.setTypeName(FILE_TASK);
				listTaskTypeNameBean.add(taskTypeNameBean);
				break;
			case 2:// 制度模板文件、制度文件
				if ("0".equals(isHiddenSystem)) {
					// "制度任务"
					taskTypeNameBean.setTypeName(RULE_TASK);
					listTaskTypeNameBean.add(taskTypeNameBean);
				}

				break;
			case 4:
				// "地图任务"
				taskTypeNameBean.setTypeName(MAP_TASK);
				listTaskTypeNameBean.add(taskTypeNameBean);
				break;
			default:
				break;
			}
		}
		return listTaskTypeNameBean;
	}

	/**
	 * 我的任务查询获取个任务审批阶段系统配置的名称和状态
	 * 
	 * @return TaskSearchBean
	 */
	public static TaskSearchBean getTaskSearchBean() {
		TaskSearchBean taskSearchBean = new TaskSearchBean();
		List<JecnConfigItemBean> listItem = null;
		// 是否存在评审阶段
		boolean isView = true;
		for (int i = 0; i < 5; i++) {// 当前任务类型存在4个
			// 个任务类型下审批环节集合
			listItem = getJecnConfigItemBeanList(i);
			if (i == 3 || listItem == null) {// 制度任务只处理一种情况
				continue;
			}
			List<TempAuditPeopleBean> listPeopleBean = new ArrayList<TempAuditPeopleBean>();
			TempAuditPeopleBean allBean = new TempAuditPeopleBean();
			// "全部"
			allBean.setAuditLable(ALL);
			allBean.setState(-1);
			listPeopleBean.add(allBean);
			TempAuditPeopleBean auditPeopleBean = null;
			for (int j = 0; j <= TASK_STATE_VALUE; j++) {// 当前审批人 10 个阶段
				if (j == 5) {// 任务完成
					continue;
				} else {
					auditPeopleBean = getTempAuditPeopleBean(j, listItem);
				}
				if (auditPeopleBean == null) {
					if (auditPeopleBean != null && auditPeopleBean.getState() == 3) {
						isView = false;
					}
					continue;
				}
				if (j == 10 && !isView) {// 不显示评审阶段，整理意见阶段不存在
					continue;
				}
				listPeopleBean.add(auditPeopleBean);
			}
			isView = true;
			auditPeopleBean = new TempAuditPeopleBean();
			// 添加完成阶段
			auditPeopleBean.setState(5);
			auditPeopleBean.setAuditLable(FINISH);
			auditPeopleBean.setIsShow(1);
			listPeopleBean.add(auditPeopleBean);
			switch (i) {
			case 0:// 流程任务
				taskSearchBean.setListFlowTaskState(listPeopleBean);
				break;
			case 4:// 流程地图任务
				taskSearchBean.setListFlowMapTaskState(listPeopleBean);
				break;
			case 2:
				taskSearchBean.setListRuleTaskState(listPeopleBean);
				break;
			case 1:
				taskSearchBean.setListFileTaskState(listPeopleBean);
				break;
			}
		}
		return taskSearchBean;
	}

	/**
	 * 根据mark 类型获取对应的系统配置信息
	 * 
	 * @param list
	 * @param mark
	 * @return
	 */
	public static JecnConfigItemBean getJecnConfigItemBena(List<JecnConfigItemBean> list, String mark) {
		if (list == null || JecnCommon.isNullOrEmtryTrim(mark)) {
			throw new NullPointerException("根据mark 类型获取对应的系统配置信息 异常！");
		}
		for (JecnConfigItemBean itemBean : list) {
			if (mark.equals(itemBean.getMark())) {
				return itemBean;
			}
		}
		return null;
	}

	/**
	 * 
	 * 获取岗位查阅权限和部门查阅权限对应的人员ID集合
	 * 
	 * @param s
	 *            Session
	 * @return Set<Long> 查阅权限对应的人员ID集合
	 */
	@SuppressWarnings("unchecked")
	public static Set<Long> getAccessPeopleIds(Session s, Object... params) {
		if (params == null || params.length == 0) {
			throw new NullPointerException("JecnTaskCommon中方法getAccessPeopleIds（）params==null || params.length==0");
		}
		Set<Long> accessPeopleIds = new HashSet<Long>();
		// 获取岗位查阅权限对应的人员
		Query query = s.createSQLQuery(JecnSqlConstant.POS_ACCESS_PERMISS).addScalar("people_id", Hibernate.LONG);
		// query.getQueryString().indexOf('?')
		log.info(" 查阅权限执行开始 ： " + JecnSqlConstant.POS_ACCESS_PERMISS);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i, params[i]);
		}

		List<Long> ret = query.list();
		accessPeopleIds.addAll(ret);

		// 获取部门查阅权限对应的人员
		query = s.createSQLQuery(JecnSqlConstant.ORG_ACCESS_PERMISS).addScalar("people_id", Hibernate.LONG);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i, params[i]);
		}
		ret = query.list();
		accessPeopleIds.addAll(ret);
		// 获取岗位组查阅权限对应的人员
		query = s.createSQLQuery(JecnSqlConstant.GROUP_ACCESS_PERMISS).addScalar("people_id", Hibernate.LONG);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i, params[i]);
		}
		ret = query.list();
		accessPeopleIds.addAll(ret);
		ret = null;
		return accessPeopleIds;
	}

	/**
	 * 获取流程中角色参与的人员ID集合
	 * 
	 * @param docControlDao
	 * @param flowId
	 *            流程ID
	 * @return List<Long> 人员ID集合
	 */
	private static List<JecnUser> getPartInPeopleIds(IJecnDocControlDao docControlDao, Long flowId) {
		if (docControlDao == null || flowId == null) {
			throw new NullPointerException(
					"JecnTaskCommon中方法getPartInPeopleIds（）docControlDao == null || flowId == null");
		}

		/** 获取流程中参与的人员ID */
		String sql = "select distinct u.*"
				+ "       from jecn_user u,"
				+ "       jecn_user_position_related u_p"
				+ "       where u.people_id = u_p.people_id and u.islock=0  and"
				+ "      ( u_p.figure_id in"
				+ "       (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
				+ "       ,jecn_flow_structure_image_t jfsi"
				+ "       where psrt.type='0' and psrt.figure_position_id = jfoi.figure_id and jfoi.org_id = jfo.org_id"
				+ "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id="
				+ flowId
				+ ")"
				+ "       or"
				+ "       u_p.figure_id in"
				+ "        (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
				+ "       ,jecn_flow_structure_image_t jfsi,jecn_position_group_r jpgr"
				+ "       where psrt.type='1' and psrt.figure_position_id = jpgr.group_id and jpgr.figure_id=jfoi.figure_id"
				+ "        and jfoi.org_id = jfo.org_id"
				+ "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id=" + flowId
				+ "))";

		return docControlDao.getSession().createSQLQuery(sql).addEntity(JecnUser.class).list();
	}

	/**
	 * 发布任务，创建文控版本信息日志
	 * 
	 * @param historyNew
	 *            文控信息
	 * @param ruleHistory
	 * 
	 * @throws Exception
	 */
	public static void saveJecnTaskHistoryNew(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IFlowStructureDao flowStructureDao, IRuleDao ruleDao, RuleHistoryDao ruleHistoryDao, IFileDao fileDao,
			IJecnConfigItemDao configItemDao, IProcessBasicInfoDao processBasicDao, IProcessKPIDao processKPIDao,
			IProcessRecordDao processRecordDao) throws Exception {
		// 发布时间
		historyNew.setPublishDate(new Date());
		// 关联ID
		Long refId = historyNew.getRelateId();
		// 文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
		int type = historyNew.getType();
		if (refId == null) {
			return;
		}
		log.info("发布文件类型 = " + type);
		if (type == 0) {// 发布流程
			// 获取流程基本信息表中流程有效期添加到文控主表有效期
			JecnFlowBasicInfoT flowBasicInfoT = processBasicDao.getFlowBasicInfoT(refId);
			Long expiry = flowBasicInfoT.getExpiry();
			if (expiry == null) {
				// 有效期（永久）
				historyNew.setExpiry(0L);
				// 下一次审视时间
				historyNew.setNewScanDate(new Date());
			} else {
				// 有效期
				historyNew.setExpiry(Long.valueOf(expiry));
				// 下一次审视时间
				historyNew.setNewScanDate(JecnUtil.plusMonthOnDateForDate(new Date(), expiry.intValue()));
			}

			pubFlow(historyNew, docControlDao, flowStructureDao, ruleDao, fileDao, configItemDao, processBasicDao,
					processKPIDao, processRecordDao);
			// 华帝登录时写入数据至Domino平台
			if (isVatti) {
				DominoOperation.importDataToDomino(historyNew.getRelateId(), "Flow");
			}
		} else if (type == 4) {// 流程地图
			pubFlow(historyNew, docControlDao, flowStructureDao, ruleDao, fileDao, configItemDao, processBasicDao,
					processKPIDao, processRecordDao);
			// 华帝登录时写入数据至Domino平台
			if (isVatti) {
				DominoOperation.importDataToDomino(historyNew.getRelateId(), "Flow");
			}
		} else if (type == 1) { // 文件发布
			pubFile(historyNew, docControlDao, configItemDao, fileDao);
		} else if (type == 2) {// 发布制度模板文件
			setRuleExpiry(historyNew, ruleDao, refId);
			pubModeRuleFile(historyNew, docControlDao, ruleDao, configItemDao, fileDao);
			if (isVatti) {
				DominoOperation.importDataToDomino(historyNew.getRelateId(), "Rule");
			}
		} else if (type == 3) {// 发布制度文件
			RuleT ruleT = ruleDao.get(refId);
			setRuleExpiry(historyNew, ruleDao, refId);
			if (ruleT.getIsFileLocal() != null) {
				if (ruleT.getIsFileLocal() == 0) {
					pubRuleFile(historyNew, docControlDao, ruleDao, ruleHistoryDao, configItemDao, fileDao);
				} else if (ruleT.getIsFileLocal() == 1) {
					pubRuleLocalFile(historyNew, docControlDao, ruleDao, ruleHistoryDao, configItemDao, fileDao);
				} else if (ruleT.getIsFileLocal() == 2) {
					pubRuleUrlFile(historyNew, docControlDao, ruleDao, ruleHistoryDao, configItemDao, fileDao);
				}
			}

		}

	}

	private static void setRuleExpiry(JecnTaskHistoryNew historyNew, IRuleDao ruleDao, Long ruleId) {
		RuleT ruleT = ruleDao.get(ruleId);
		Long expiry = ruleT.getExpiry();
		if (expiry == null) {
			// 有效期（永久）
			historyNew.setExpiry(0L);
			// 下一次审视时间
			historyNew.setNewScanDate(new Date());
		} else {
			// 有效期
			historyNew.setExpiry(Long.valueOf(expiry));
			// 下一次审视时间
			historyNew.setNewScanDate(JecnUtil.plusMonthOnDateForDate(new Date(), expiry.intValue()));
		}
	}

	private static void pubRuleLocalFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IRuleDao ruleDao, RuleHistoryDao ruleHistoryDao, IJecnConfigItemDao configItemDao, IFileDao fileDao)
			throws Exception {

		Set<Long> accessPeopleIdS = new HashSet<Long>();

		Long refId = historyNew.getRelateId();
		// 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
		accessPeopleIdS = getAccessPeopleIds(docControlDao.getSession(), 3, refId);
		// 制度文件
		List<Long> listRuleFileIds = new ArrayList<Long>();
		listRuleFileIds.add(refId);

		RuleT ruleT = ruleDao.get(refId);
		// 设置更新时间
		Date pubTime = new Date();
		ruleT.setUpdateDate(pubTime);
		ruleT.setPubTime(pubTime);
		ruleDao.update(ruleT);
		ruleDao.getSession().flush();

		historyNew.setFileContentId(ruleT.getFileId());
		String prfName = ruleT.getRuleName();

		// 保存文控信息
		docControlDao.saveJecnTaskHistoryNew(historyNew);
		ruleT.setHistoryId(historyNew.getId());
		ruleDao.getSession().flush();

		// 发布制度
		ruleDao.releaseRuleFileData(listRuleFileIds);

		updateRuleDateAndHistory(docControlDao, ruleT.getId(), pubTime, historyNew.getId());

		// 发布制度相关文件
		pubRuleRelateFile(historyNew, docControlDao, fileDao);

		// 查阅权限 消息，邮件
		setMessageByAccess(docControlDao, configItemDao, accessPeopleIdS, historyNew.getRelateId(), prfName, historyNew
				.getType());
		archive(historyNew, ruleDao, ruleT, getFileFromTemp(ruleDao, fileDao, ruleT));
	}

	private static void updateRuleDateAndHistory(IBaseDao baseDao, Long id, Date pubTime, Long historyId) {
		String sql = "update jecn_rule set update_date=?,pub_time=?,history_id=? where id=?";
		baseDao.execteNative(sql, pubTime, pubTime, historyId, id);
	}

	private static void pubRuleUrlFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IRuleDao ruleDao, RuleHistoryDao ruleHistoryDao, IJecnConfigItemDao configItemDao, IFileDao fileDao)
			throws Exception {

		Set<Long> accessPeopleIdS = new HashSet<Long>();

		Long refId = historyNew.getRelateId();
		// 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
		accessPeopleIdS = getAccessPeopleIds(docControlDao.getSession(), 3, refId);
		// 制度文件
		List<Long> listRuleFileIds = new ArrayList<Long>();
		listRuleFileIds.add(refId);

		RuleT ruleT = ruleDao.get(refId);
		// 设置更新时间
		Date pubTime = new Date();
		ruleT.setUpdateDate(pubTime);
		ruleT.setPubTime(pubTime);
		ruleDao.update(ruleT);
		ruleDao.getSession().flush();
		historyNew.setFileContentId(ruleT.getFileId());
		String prfName = ruleT.getRuleName();

		// 保存文控信息
		docControlDao.saveJecnTaskHistoryNew(historyNew);
		ruleT.setHistoryId(historyNew.getId());

		// 发布制度相关文件
		pubRuleRelateFile(historyNew, docControlDao, fileDao);
		// 发布制度
		ruleDao.releaseRuleFileData(listRuleFileIds);
		// // 更新发布后的制度版本信息
		// Rule rule = ruleDao.getRuleById(refId);
		// rule.setHistoryId(historyNew.getId());

		// 查阅权限 消息，邮件
		setMessageByAccess(docControlDao, configItemDao, accessPeopleIdS, historyNew.getRelateId(), prfName, historyNew
				.getType());

	}

	/**
	 * 制度文件
	 * 
	 * @param historyNew
	 * @param docControlDao
	 * @param ruleDao
	 * @param ruleHistoryDao
	 * @param configItemDao
	 * @param fileDao
	 * @throws Exception
	 */
	private static void pubRuleFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao, IRuleDao ruleDao,
			RuleHistoryDao ruleHistoryDao, IJecnConfigItemDao configItemDao, IFileDao fileDao) throws Exception {
		Set<Long> accessPeopleIdS = new HashSet<Long>();

		Long refId = historyNew.getRelateId();
		// 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
		accessPeopleIdS = getAccessPeopleIds(docControlDao.getSession(), 3, refId);
		// 制度文件
		List<Long> listRuleFileIds = new ArrayList<Long>();
		listRuleFileIds.add(refId);

		RuleT ruleT = ruleDao.get(refId);// 制度临时表
		// 设置更新时间
		Date pubTime = new Date();
		ruleT.setUpdateDate(pubTime);
		ruleT.setPubTime(pubTime);
		ruleDao.update(ruleT);
		ruleDao.getSession().flush();

		String prfName = ruleT.getRuleName();
		Long fileId = ruleT.getFileId();
		if (fileId == null) {
			return;
		}
		JecnFileBeanT jecnFileBean = fileDao.get(fileId);
		if (jecnFileBean == null) {
			return;
		}
		historyNew.setFileContentId(jecnFileBean.getVersionId());
		// 保存文控信息
		docControlDao.saveJecnTaskHistoryNew(historyNew);
		ruleT.setHistoryId(historyNew.getId());
		ruleDao.getSession().flush();

		// 发布制度
		ruleDao.releaseRuleFileData(listRuleFileIds);

		updateRuleDateAndHistory(docControlDao, ruleT.getId(), pubTime, historyNew.getId());

		// 发布制度相关文件
		pubRuleRelateFile(historyNew, docControlDao, fileDao);
		// 查阅权限 消息，邮件
		setMessageByAccess(docControlDao, configItemDao, accessPeopleIdS, historyNew.getRelateId(), prfName, historyNew
				.getType());

		// 打开文件
		archive(historyNew, ruleDao, ruleT, getFileFromTemp(ruleDao, fileDao, ruleT));
	}

	private static String getFileFromTemp(IRuleDao ruleDao, IFileDao fileDao, RuleT rule) throws Exception {
		Long fileId = rule.getFileId();
		FileOpenBean openBean = null;
		if (rule.getIsFileLocal() == 1) {// 本地上传
			G020RuleFileContent openBeanG020 = ruleDao.getG020RuleFileContent(rule.getFileId());
			openBean = new FileOpenBean();
			openBean.setFileByte(JecnDaoUtil.blobToBytes(openBeanG020.getFileStream()));
			openBean.setName(openBeanG020.getFileName());
		} else {// 保存数据库
			openBean = SelectDownloadProcess.openFile(fileId, false, fileDao);
		}

		String path = JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.TEMP_DIR + UUID.randomUUID().toString()
				+ openBean.getName();
		FileUtils.writeByteArrayToFile(new File(path), openBean.getFileByte());
		return path;

	}

	/**
	 * 发布流程相关文件
	 * 
	 * @param historyNew
	 *            JecnTaskHistoryNew
	 * @param docControlDao
	 *            IJecnDocControlDao
	 * @param fileDao
	 *            IFileDao
	 * @throws Exception
	 */
	private static void pubFlowRelateFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IFileDao fileDao) throws Exception {
		if (!validateRelateFile(historyNew.getType())) {
			return;
		}
		// 获取流程未发布的文件
		List<Object[]> listFileObjects = findProcessNoPubFiles(historyNew.getRelateId(), docControlDao);
		// 发布流程、制度，处理其相关的未发布文件
		pubRefFiles(listFileObjects, historyNew, getFileVersionValue(), 1, fileDao, docControlDao);
	}

	/**
	 * 发布流程地图相关文件
	 * 
	 * @param historyNew
	 * @param docControlDao
	 * @param fileDao
	 * @throws Exception
	 */
	private static void pubFlowMapRelateFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IFileDao fileDao) throws Exception {
		if (!validateRelateFile(historyNew.getType())) {
			return;
		}
		// 获取流程未发布的文件
		List<Object[]> listFileObjects = findProcessMapNoPubFiles(historyNew.getRelateId(), docControlDao);
		// 发布流程、制度，处理其相关的未发布文件
		pubRefFiles(listFileObjects, historyNew, getFileVersionValue(), 1, fileDao, docControlDao);
	}

	/**
	 * 发布制度模板相关文件
	 * 
	 * @param historyNew
	 * @param docControlDao
	 * @param fileDao
	 * @throws Exception
	 */
	private static void pubRuleModelRelateFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IFileDao fileDao) throws Exception {
		if (!validateRelateFile(historyNew.getType())) {
			return;
		}
		// 未发布文件
		List<Object[]> listFileObjects = findRuleModeNoPubFiles(historyNew.getRelateId(), docControlDao);
		if (listFileObjects.size() > 0) {
			// 发布流程、制度，处理其相关的未发布文件
			pubRefFiles(listFileObjects, historyNew, getFileVersionValue(), 1, fileDao, docControlDao);
		}
	}

	/**
	 * 发布制度相关文件
	 * 
	 * @param historyNew
	 * @param docControlDao
	 * @param fileDao
	 * @throws Exception
	 */
	private static void pubRuleRelateFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IFileDao fileDao) throws Exception {
		// 未发布文件
		List<Object[]> listFileObjects = findRuleFileNoPubFiles(historyNew.getRelateId(), docControlDao);
		if (listFileObjects.size() > 0) {
			// 发布流程、制度，处理其相关的未发布文件
			pubRefFiles(listFileObjects, historyNew, getFileVersionValue(), 1, fileDao, docControlDao);
		}
	}

	/**
	 * 制度模板文件
	 * 
	 * @param historyNew
	 * @param docControlDao
	 * @param ruleDao
	 * @param configItemDao
	 * @param fileDao
	 * @throws Exception
	 */
	private static void pubModeRuleFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IRuleDao ruleDao, IJecnConfigItemDao configItemDao, IFileDao fileDao) throws Exception {
		Set<Long> accessPeopleIdS = new HashSet<Long>();
		// 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
		accessPeopleIdS = getAccessPeopleIds(docControlDao.getSession(), 3, historyNew.getRelateId());
		// 制度模板文件
		List<Long> listRuleModeFileIds = new ArrayList<Long>();
		listRuleModeFileIds.add(historyNew.getRelateId());

		RuleT ruleT = ruleDao.get(historyNew.getRelateId());
		// 设置更新时间
		Date pubTime = new Date();
		ruleT.setUpdateDate(pubTime);
		ruleT.setPubTime(pubTime);
		ruleDao.update(ruleT);
		ruleDao.getSession().flush();
		String prfName = ruleT.getRuleName();
		// 保存文控信息
		docControlDao.saveJecnTaskHistoryNew(historyNew);
		// 更新制度对应的版本信息ID
		ruleT.setHistoryId(historyNew.getId());
		ruleDao.getSession().flush();
		// 发布制度模板文件
		ruleDao.releaseRuleModeData(listRuleModeFileIds);
		updateRuleDateAndHistory(docControlDao, ruleT.getId(), pubTime, historyNew.getId());
		// 通过制度Id获得制度模板文件的程序文件
		ruleFilePath(historyNew.getRelateId(), historyNew, ruleDao, docControlDao, configItemDao);
		// 发布制度模板相关文件
		pubRuleModelRelateFile(historyNew, docControlDao, fileDao);
		// 查阅权限 消息，邮件
		setMessageByAccess(docControlDao, configItemDao, accessPeopleIdS, historyNew.getRelateId(), prfName, historyNew
				.getType());

		archive(historyNew, ruleDao, ruleT, JecnContants.jecn_path + historyNew.getFilePath());
	}

	/**
	 * 发布文件
	 * 
	 * @param historyNew
	 * @param docControlDao
	 * @param fileDao
	 * @throws Exception
	 */
	private static void pubFile(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IJecnConfigItemDao configItemDao, IFileDao fileDao) throws Exception {
		Set<Long> accessPeopleIdS = new HashSet<Long>();
		JecnFileBeanT jecnFileBeanT = fileDao.get(historyNew.getRelateId());
		if (jecnFileBeanT == null) {
			return;
		}
		// 设置更新时间
		Date pubTime = new Date();
		jecnFileBeanT.setUpdateTime(pubTime);
		jecnFileBeanT.setPubTime(pubTime);
		fileDao.getSession().update(jecnFileBeanT);
		fileDao.getSession().flush();

		String prfName = jecnFileBeanT.getFileName();
		// 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度
		accessPeopleIdS = getAccessPeopleIds(docControlDao.getSession(), 1, historyNew.getRelateId());
		historyNew.setFileContentId(jecnFileBeanT.getVersionId());
		// 保存文控信息
		docControlDao.saveJecnTaskHistoryNew(historyNew);
		List<Long> listFileIds = new ArrayList<Long>();
		listFileIds.add(historyNew.getRelateId());

		// 设置发布的版本id
		jecnFileBeanT.setHistoryId(historyNew.getId());
		String sql = "update jecn_file set update_time=?,pub_time=?,history_id=? where file_id=?";
		docControlDao.execteNative(sql, pubTime, pubTime, historyNew.getId(), jecnFileBeanT.getFileID());

		// 发布文件
		fileDao.releaseFiles(listFileIds);

		// 查阅权限 消息，邮件
		setMessageByAccess(docControlDao, configItemDao, accessPeopleIdS, historyNew.getRelateId(), prfName, historyNew
				.getType());
	}

	/**
	 * 是否发送消息或邮件给查阅权限的人
	 * 
	 * @param docControlDao
	 * @param configItemDao
	 * @param accessPeopleIdS
	 * @param refId
	 * @param prfName
	 * @param type
	 * @throws Exception
	 */
	private static void setMessageByAccess(IJecnDocControlDao docControlDao, IJecnConfigItemDao configItemDao,
			Set<Long> accessPeopleIdS, Long refId, String prfName, int type) throws Exception {
		// if (accessPeopleIdS.size() == 0) {
		// return;
		// }
		// // 发布给PRF查阅权限人员发送消息
		// boolean isAccessTrue =
		// JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.mesPulishCompetence.toString(),
		// selectMessageAndEmailItemBean(configItemDao));
		// if (isAccessTrue) {// 给查阅权限的人发送消息
		// createMessage(accessPeopleIdS, type, prfName,
		// docControlDao.getSession());
		// }

	}

	/**
	 * 发布流程 and 流程地图
	 * 
	 * @param historyNew
	 *            文控信息
	 * @param docControlDao
	 * @param flowStructureDao
	 * @param ruleDao
	 * @param fileDao
	 * @param configItemDao
	 * @param processBasicDao
	 * @param processKPIDao
	 * @param processRecordDao
	 * @throws Exception
	 */
	private static void pubFlow(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IFlowStructureDao flowStructureDao, IRuleDao ruleDao, IFileDao fileDao, IJecnConfigItemDao configItemDao,
			IProcessBasicInfoDao processBasicDao, IProcessKPIDao processKPIDao, IProcessRecordDao processRecordDao)
			throws Exception {
		Long flowId = historyNew.getRelateId();
		long startTime = 0;
		JecnFlowStructureT structureT = flowStructureDao.get(flowId);
		String prfName = structureT.getFlowName();
		String oldPath = "";
		JecnFlowStructure structure = flowStructureDao.findJecnFlowStructureById(flowId);
		if (structure != null) {// 删除历史图片
			oldPath = structure.getFilePath();
		}
		TreeNodeType nodeType = null;
		if (historyNew.getType() == 4) {// 流程地图发布
			flowStructureDao.releaseProcessMapData(flowId);
			log.info("流程地图发布，临时表数据插入真实表操作成功！");
			// 流程地图
			nodeType = TreeNodeType.processMap;
			// 发布流程地图相关文件
			pubFlowMapRelateFile(historyNew, docControlDao, fileDao);
		} else {// 流程发布
			// 发布时流程参与人 Object[] 0:内外网；1：邮件地址；2：人员主键ID
			startTime = System.currentTimeMillis();
			log.info("流程发布 数据处理开始 = " + (System.currentTimeMillis() - startTime));
			// 发布流程的数据
			flowStructureDao.releaseProcessData(flowId);
			log.info("流程图发布，临时表数据插入真实表操作成功！" + (System.currentTimeMillis() - startTime));
			// 流程图
			nodeType = TreeNodeType.process;
			// 发布流程图相关文件信息
			pubFlowRelateFile(historyNew, docControlDao, fileDao);

			log.info("流程图发布，流程相关的文件，问发布的处理成功！" + (System.currentTimeMillis() - startTime));
		}

		// 发布相关制度
		startTime = System.currentTimeMillis();
		pubRelateRule(historyNew, docControlDao, flowStructureDao, ruleDao, configItemDao);
		log.info("发布相关制度 end: " + (System.currentTimeMillis() - startTime));

		// 获取岗位查阅权限对应的人员ID集合 0是流程、1是文件、2是标准、3制度、4流程地图
		startTime = System.currentTimeMillis();
		Set<Long> accessPeopleIdS = getAccessPeopleIds(docControlDao.getSession(), 0, flowId);
		log.info("发布的流程文件相关查阅权限成功！方法 getAccessPeopleIds  = " + (System.currentTimeMillis() - startTime));
		// 查阅权限 消息，邮件
		startTime = System.currentTimeMillis();
		setMessageByAccess(docControlDao, configItemDao, accessPeopleIdS, flowId, prfName, historyNew.getType());
		log.info("查阅权限 消息，邮件 end: " + (System.currentTimeMillis() - startTime));

		startTime = System.currentTimeMillis();
		log.info("流程图发布，记录文控版本 start: " + startTime);
		Date pubTime = new Date(); // 更新发布时间
		structureT.setPubTime(pubTime);
		structureT.setUpdateDate(pubTime);
		docControlDao.getSession().update(structureT);
		docControlDao.getSession().flush();
		// 保存文控信息
		docControlDao.saveJecnTaskHistoryNew(historyNew);
		// 保存文件版本ID到流程主表
		structureT.setHistoryId(historyNew.getId());
		docControlDao.getSession().update(structureT);
		docControlDao.getSession().flush();

		String sql = "update jecn_flow_structure set update_date=?,pub_time=?,history_id=? where flow_id=?";
		docControlDao.execteNative(sql, pubTime, pubTime, historyNew.getId(), flowId);

		// 发布获取保存路径下的图片拷贝到发布图片路径下，并记录当前生成当前版本文件
		String[] filePaths = getPubImagePath(flowStructureDao, structureT, oldPath, docControlDao, configItemDao,
				processBasicDao, processKPIDao, processRecordDao, nodeType, true);
		historyNew.setFilePath(filePaths[0]);

		docControlDao.update(historyNew);
		docControlDao.getSession().flush();

		log.info("流程图发布，记录文控版本 end: " + (System.currentTimeMillis() - startTime));
		// 记录流程图历史图片
		if (historyNew.getType() != null
				&& (historyNew.getType().intValue() == 0 || historyNew.getType().intValue() == 4)) {
			saveProcessImageHistory(historyNew.getId(), historyNew.getRelateId(), filePaths[1], docControlDao);
		}
		long start = System.currentTimeMillis();
		// 归档
		archive(historyNew, processBasicDao, structureT);
		long end = System.currentTimeMillis();
		log.info("流程归档总耗时：" + (end - start) / 1000 + "s");
	}

	public static void archive(JecnTaskHistoryNew historyNew, IProcessBasicInfoDao processBasicDao,
			JecnFlowStructureT structureT) {
		if (!JecnConfigTool.isMengNiuLogin()) {
			return;
		}
		if (structureT.getIsFlow() == 0) {// 架构不用
			return;
		}
		try {
			String filePath = historyNew.getFilePath();
			File file = new File(JecnContants.jecn_path + filePath);
			if (file.exists()) {
				ArchiveService service = new ArchiveService();
				ArchiveFile af = trans(processBasicDao, structureT, historyNew);
				// -------
				List<ArchiveF> as = getSupportFilesAndSaveToTemp(processBasicDao, structureT.getFlowId(), 0);
				as.add(0, new ArchiveF(structureT.getFlowName(), file.getAbsolutePath()));
				// -------
				service.send(af, as);
			} else {
				log.error("归档文件不存在：" + filePath);
			}
		} catch (Exception e) {
			log.error("归档异常", e);
		}
	}

	private static List<ArchiveF> getSupportFilesAndSaveToTemp(IBaseDao baseDao, Long id, int type) throws Exception {
		List<ArchiveF> result = new ArrayList<ArchiveF>();
		String sql = "";
		if (type == 0) {// 流程
			sql = "SELECT B.* FROM FLOW_STANDARDIZED_FILE_T A"
					+ "  INNER JOIN JECN_FILE_T B ON A.FILE_ID=B.FILE_ID AND B.DEL_STATE=0 AND A.FLOW_ID=?";
		} else if (type == 2) {// 制度
			sql = "SELECT B.* FROM RULE_STANDARDIZED_FILE_T A"
					+ " INNER JOIN JECN_FILE_T B ON A.FILE_ID=B.FILE_ID AND B.DEL_STATE=0 AND A.RELATED_ID=?";
		} else {
			throw new IllegalArgumentException("错误的类型getSupportFilesAndSaveToTemp()" + type);
		}
		List<JecnFileBeanT> files = (List<JecnFileBeanT>) baseDao.listNativeAddEntity(sql, JecnFileBeanT.class, id);
		if (JecnUtil.isEmpty(files)) {
			return result;
		}
		for (JecnFileBeanT file : files) {
			Long versionId = file.getVersionId();
			if (versionId == null) {
				log.error("文件" + file.getFileName() + "对应的versionId为空");
				continue;
			}
			result.add(new ArchiveF(file.getFileName(), saveFileToTemp(file, baseDao)));
		}
		return result;
	}

	private static JecnTaskHistoryNew getFileLastHistory(IBaseDao<Serializable, Serializable> baseDao,
			JecnFileBeanT file) {
		Long historyId = file.getHistoryId();
		if (historyId != null) {
			String hql = "from JecnTaskHistoryNew where id=?";
			List<JecnTaskHistoryNew> ls = baseDao.listHql(hql, historyId);
			if (ls.size() > 0) {
				return ls.get(0);
			}
		}
		return null;
	}

	private static ArchiveFile trans(IBaseDao<Serializable, Serializable> fileDao, JecnFileBeanT file,
			JecnTaskHistoryNew historyNew) {
		ArchiveFile af = new ArchiveFile();
		af.setCompanycode("main");
		af.setRid(UUID.randomUUID().toString());
		af.setPid("0");
		af.setTitle(file.getFileName());
		af.setDocnumber(file.getDocId());
		af.setDocdate(JecnUtil.DateToStr(file.getPubTime(), "yyyy-MM-dd"));
		af.setYear(JecnUtil.DateToStr(file.getPubTime(), "yyyy"));
		af.setSecurity(getSectLevelStr(file.getIsPublic() == null ? 0 : file.getIsPublic().intValue()));
		// af.setSecuritytime(getExpiryStr(ruleT.getExpiry()));
		af.setCreatetime(JecnUtil.DateToStr(file.getCreateTime(), "yyyy-MM-dd"));
		// 级别
		af.setExt1(JecnConfigTool.getSectLevelStr(file.getConfidentialityLevel() == null ? 0 : file
				.getConfidentialityLevel()));
		// 发布状态
		af.setExt2(getPubState(fileDao, file.getFileID(), "(1)"));
		// 相关文件
		af.setExt3("");
		// 发布日期
		af.setExt6(JecnUtil.DateToStr(file.getPubTime(), "yyyyMMdd"));

		setDepart(af, fileDao, file.getOrgId());

		if (historyNew != null) {
			// 版本号
			af.setExt5(JecnUtil.nullToEmpty(historyNew.getVersionId()));
			// 拟稿人
			af.setExt7(historyNew.getDraftPerson());
		}

		return af;
	}

	public static String saveFileToTemp(JecnFileBeanT file, IBaseDao<Serializable, Serializable> baseDao)
			throws Exception {
		FileOpenBean fileOpenBean = JecnDaoUtil.getFileOpenBean(baseDao, file.getVersionId());
		File temp = new File(JecnContants.jecn_path + "/eprosFileLibrary/templet_dir/" + UUID.randomUUID()
				+ fileOpenBean.getName());
		log.info("归档临时文件地址：" + temp.getAbsolutePath());
		FileUtils.writeByteArrayToFile(temp, fileOpenBean.getFileByte());
		return temp.getAbsolutePath();
	}

	public static void archive(JecnTaskHistoryNew historyNew, IRuleDao ruleDao, IFileDao fileDao, RuleT ruleT) {
		if (!JecnConfigTool.isMengNiuLogin()) {
			return;
		}
		try {
			long start = System.currentTimeMillis();
			if (ruleT.getIsDir() == 1) {
				archive(historyNew, ruleDao, ruleT, JecnContants.jecn_path + historyNew.getFilePath());
			} else {
				if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 0) {
					archive(historyNew, ruleDao, ruleT, getFileFromTemp(ruleDao, fileDao, ruleT));
				} else if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 1) {
					archive(historyNew, ruleDao, ruleT, getFileFromTemp(ruleDao, fileDao, ruleT));
				}
			}
			long end = System.currentTimeMillis();
			log.info("制度归档总耗时：" + (end - start) / 1000 + "s");
		} catch (Exception e) {
			log.error("制度归档异常", e);
		}
	}

	private static void archive(JecnTaskHistoryNew historyNew, IRuleDao ruleDao, RuleT ruleT, String fileRealPath) {
		if (!JecnConfigTool.isMengNiuLogin()) {
			return;
		}
		try {
			File file = new File(fileRealPath);
			if (file.exists()) {
				ArchiveService service = new ArchiveService();
				ArchiveFile af = trans(ruleDao, ruleT, historyNew);
				// -------
				List<ArchiveF> as = getSupportFilesAndSaveToTemp(ruleDao, ruleT.getId(), 2);
				as.add(0, new ArchiveF(ruleT.getRuleName(), file.getAbsolutePath()));
				// -------
				service.send(af, as);
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private static ArchiveFile trans(IRuleDao ruleDao, RuleT ruleT, JecnTaskHistoryNew historyNew) throws Exception {
		ArchiveFile af = new ArchiveFile();
		af.setCompanycode("main");
		af.setRid(UUID.randomUUID().toString());
		af.setPid("0");
		af.setTitle(ruleT.getRuleName());
		af.setDocnumber(ruleT.getRuleNumber());
		af.setDocdate(JecnUtil.DateToStr(ruleT.getPubTime(), "yyyy-MM-dd"));
		af.setYear(JecnUtil.DateToStr(ruleT.getPubTime(), "yyyy"));
		af.setSecurity(getSectLevelStr(ruleT.getIsPublic() == null ? 0 : ruleT.getIsPublic().intValue()));
		// af.setSecuritytime(getExpiryStr(ruleT.getExpiry()));
		af.setCreatetime(JecnUtil.DateToStr(ruleT.getCreateDate(), "yyyy-MM-dd"));
		// 级别
		af.setExt1(JecnConfigTool.getSectLevelStr(ruleT.getConfidentialityLevel() == null ? 0 : ruleT
				.getConfidentialityLevel()));
		// 发布状态
		af.setExt2(getPubState(ruleDao, ruleT.getId(), "(2,3)"));
		// 相关文件
		af.setExt3("");
		// 版本号
		af.setExt5(JecnUtil.nullToEmpty(historyNew.getVersionId()));
		// 发布日期
		af.setExt6(JecnUtil.DateToStr(ruleT.getPubTime(), "yyyyMMdd"));
		af.setExt7(historyNew.getDraftPerson());
		setDepart(af, ruleDao, ruleT.getOrgId());

		return af;
	}

	private static void setDepart(ArchiveFile af, IBaseDao baseDao, Long dutyOrgId) {
		if (dutyOrgId == null) {
			return;
		}
		String hql = "from JecnFlowOrg where orgId=?";
		JecnFlowOrg org = (JecnFlowOrg) baseDao.getObjectHql(hql, dutyOrgId);
		if (org != null) {
			// 责任部门
			af.setExt4(org.getOrgName());
			// 设置一级部门也就是level=1的
			if (StringUtils.isNotBlank(org.gettPath())) {
				String[] ids = org.gettPath().split("-");
				if (ids.length >= 2) {
					JecnFlowOrg p = (JecnFlowOrg) baseDao.getObjectHql(hql, Long.valueOf(ids[1]));
					if (p != null) {
						af.setDepartment(JecnUtil.nullToEmpty(p.getOrgName()));
						af.setDepartmentid(JecnUtil.nullToEmpty(p.getOrgNumber()));
					}
				}
			}
		}
	}

	private static String getPubState(IBaseDao baseDao, Long id, String type) {
		String hql = "from JecnTaskHistoryNew where relateId=? and type in " + type;
		List<Object> objs = baseDao.listHql(hql, id);
		if (objs.size() > 1) {
			return "换版发布";
		}
		return "新建发布";
	}

	private static String getExpiryStr(Long expiry) {
		if (expiry == null) {
			return "";
		}
		if (expiry == 0) {
			return "永久";
		}
		return JecnUtil.nullToEmpty(expiry);
	}

	private static String getSectLevelStr(int i) {
		String[] se = new String[] { "秘密", "无密级" };
		return se[i];
	}

	private static ArchiveFile trans(IProcessBasicInfoDao processBasicDao, JecnFlowStructureT s,
			JecnTaskHistoryNew historyNew) throws Exception {
		JecnFlowBasicInfoT basic = (JecnFlowBasicInfoT) processBasicDao.getSession().get(JecnFlowBasicInfoT.class,
				s.getFlowId());
		Object[] objs = processBasicDao.getFlowRelateOrgIds(s.getFlowId(), false);
		ArchiveFile af = new ArchiveFile();
		af.setCompanycode("main");
		af.setRid(UUID.randomUUID().toString());
		af.setPid("0");
		af.setTitle(s.getFlowName());
		af.setDocnumber(s.getFlowIdInput());
		af.setDocdate(JecnUtil.DateToStr(s.getPubTime(), "yyyy-MM-dd"));
		af.setYear(JecnUtil.DateToStr(s.getPubTime(), "yyyy"));
		af.setSecurity(getSectLevelStr(s.getIsPublic() == null ? 0 : s.getIsPublic().intValue()));
		// af.setSecuritytime(JecnUtil.nullToEmpty(basic.getExpiry()));
		// af.setDepartment(dutyOrg);
		af.setCreatetime(JecnUtil.DateToStr(s.getCreateDate(), "yyyy-MM-dd"));
		// 级别
		af.setExt1(JecnConfigTool
				.getSectLevelStr(s.getConfidentialityLevel() == null ? 0 : s.getConfidentialityLevel()));
		// 发布状态
		af.setExt2(getPubState(processBasicDao, s.getFlowId(), "(0,4)"));
		// 相关文件
		af.setExt3("");
		// 版本号
		af.setExt5(JecnUtil.nullToEmpty(historyNew.getVersionId()));
		// 发布日期
		af.setExt6(JecnUtil.DateToStr(s.getPubTime(), "yyyyMMdd"));
		af.setExt7(historyNew.getDraftPerson());
		if (objs != null) {
			setDepart(af, processBasicDao, objs[0] == null ? null : Long.valueOf(objs[0].toString()));
		}
		return af;
	}

	private static void saveProcessImageHistory(Long historyId, Long processId, String filePath,
			IJecnDocControlDao docControlDao) {
		String sql = " update jecn_flow_structure_h set FILE_PATH=? where HISTORY_ID=? and FLOW_ID = ?";
		docControlDao.execteNative(sql, filePath, historyId, processId);
	}

	/**
	 * 发布相关制度
	 * 
	 * @param historyNew
	 * @param docControlDao
	 * @param flowStructureDao
	 * @param ruleDao
	 * @param configItemDao
	 * @throws Exception
	 */
	private static void pubRelateRule(JecnTaskHistoryNew historyNew, IJecnDocControlDao docControlDao,
			IFlowStructureDao flowStructureDao, IRuleDao ruleDao, IJecnConfigItemDao configItemDao) throws Exception {
		if (!validateRelateRule(historyNew.getType())) {// 验证流程、流程地图 相关制度是否关联发布
			return;
		}
		// }
		// 发布流程文控信息 --start
		// 发布流程是否存在相关制度为发布--start
		// 0:制度主键ID，1：是制度目录，制度模板文件，制度文件；2：制度文件对应的文件版本ID
		List<Object[]> listRuleIdsNoRelease = null;
		if (historyNew.getType() == 4) {
			listRuleIdsNoRelease = flowStructureDao.getRelateRuleByFlowMapId(historyNew.getRelateId());
			log.info("获取流程地图相关制度成功！");
		} else {
			listRuleIdsNoRelease = flowStructureDao.getRelateRuleByFlowId(historyNew.getRelateId());
			log.info("获取流程图相关制度成功！");
		}
		if (listRuleIdsNoRelease != null && listRuleIdsNoRelease.size() > 0) {
			// 发布制度
			pubRefRule(listRuleIdsNoRelease, historyNew, ruleDao, docControlDao, configItemDao);
			log.info("流程发布，发布流程相关制度成功！");
		}
	}

	/**
	 * 拷贝文件夹及文件夹下文件
	 * 
	 * @param String
	 *            copyPath 需要拷贝的目录
	 * @param filePath
	 *            拷贝后的目录
	 */
	private static void copyFileDir(String fromPath, String filePath) {
		if (JecnCommon.isNullOrEmtryTrim(filePath) || !filePath.endsWith(".jpg")) {
			return;
		}
		filePath = JecnContants.jecn_path + JecnCommon.getSavePorcessImagePath(filePath);
		fromPath = JecnContants.jecn_path + JecnCommon.getSavePorcessImagePath(fromPath);
		try {
			copyDirectiory(filePath, fromPath);
		} catch (IOException e) {
			log.error("", e);
		}
	}

	/**
	 * 拷贝文件夹及文件夹下文件
	 * 
	 * @param topath
	 * @param frompath
	 * @throws IOException
	 */
	public static void copyDirectiory(String topath, String frompath) throws IOException {
		if (JecnCommon.isNullOrEmtryTrim(topath) || JecnCommon.isNullOrEmtryTrim(frompath)) {
			return;
		}
		File copy = new File(topath);
		File bycopy = new File(frompath);

		if (copy.exists()) {
			copy.delete();
		}
		// 创建拷贝目录
		copy.mkdirs();
		// 开始拷贝
		File[] file = bycopy.listFiles();
		try {

			if (file.length != 0) {

				for (int i = 0; i < file.length; i++) {
					if (file[i].isFile()) {
						FileInputStream input = null;
						FileOutputStream output = null;
						try {
							input = new FileInputStream(file[i]);
							output = new FileOutputStream(copy + "\\" + file[i].getName());
							byte[] b = new byte[1024 * 5];
							int len;
							while ((len = input.read(b)) != -1) {
								output.write(b, 0, len);
							}
							output.flush();
						} finally {
							if (input != null) {
								input.close();
							}
							if (output != null) {
								output.close();
							}
						}
					}
					if (file[i].isDirectory()) {
						copyDirectiory(copy + "\\" + file[i].getName(), bycopy + "\\" + file[i].getName());
					}
				}
			}
		} catch (Exception e) {
			log.error("JecnTaskCommon.copyDirectiory 拷贝文件夹及文件夹下文件 异常", e);
		} finally {
			file = null;
		}
	}

	/**
	 * 获取主键ID对应的人员信息
	 * 
	 * @param setPeopleIds
	 * @param docControlDao
	 * @return
	 * @throws Exception
	 */
	public static List<JecnUser> getJecnUserList(Set<Long> setPeopleIds, IBaseDao docControlDao) throws Exception {
		List<JecnUser> result = new ArrayList<JecnUser>();
		if (setPeopleIds == null || setPeopleIds.size() == 0) {
			return result;
		}
		String sql = "select u.* from jecn_user u where u.people_id in ";
		List<String> listStr = JecnCommonSql.getSetSqls(sql, setPeopleIds);
		for (String s : listStr) {
			result.addAll(docControlDao.getSession().createSQLQuery(s).addEntity(JecnUser.class).list());
		}
		return result;
	}

	/**
	 * 发布获取保存路径下的图片拷贝到发布图片路径下，并记录当前生成当前版本文件
	 * 
	 * @param flowStructureDao
	 * @param refId
	 * @param savePath
	 * @param oldPath
	 * @param docControlDao
	 * @param configItemDao
	 * @param processBasicDao
	 * @param processKPIDao
	 * @param processRecordDao
	 * @param nodeType
	 * @param isRecordVersion
	 *            true：记录版本信息，false不记录版本信息
	 * @return 生成的版本文件路径
	 * @throws Exception
	 */
	public static String[] getPubImagePath(IFlowStructureDao flowStructureDao, JecnFlowStructureT structureT,
			String oldPath, IJecnDocControlDao docControlDao, IJecnConfigItemDao configItemDao,
			IProcessBasicInfoDao processBasicDao, IProcessKPIDao processKPIDao, IProcessRecordDao processRecordDao,
			TreeNodeType nodeType, boolean isRecordVersion) throws Exception {
		String[] paths = new String[2];
		Long refId = structureT.getFlowId();
		String savePath = structureT.getFilePath();
		// 生成bute[] 根据保存时生成的图片路径;
		byte[] bytes = JecnFinal.getBytesByFilePath(savePath);
		String pubImagePath = "";
		if (bytes != null) {
			pubImagePath = JecnFinal.getImagePathByPub(bytes);
		}
		String filePath = "";

		if (isRecordVersion) {// true记录版本信息
			// 获得流程地图和流程发布时候所生成的流程文件
			if (structureT.getNodeType() == 0) {
				JecnCreateDoc createDoc = DownloadUtil.getFlowFile(refId, nodeType, false, configItemDao,
						docControlDao, flowStructureDao, processBasicDao, processKPIDao, processRecordDao);
				if (createDoc.getBytes() != null) {
					log.info("获取word操作说明文件信息成功！");
					filePath = JecnFinal.saveFilePath(JecnFinal.FLOW_FILE_RELEASE, new Date(), ".doc");
					JecnFinal.createFile(filePath, createDoc.getBytes());
				}
			} else if (structureT.getNodeType() == 1) {// 流程（文件）
				// 获取流程-文件，保存到本地
				FileOpenBean openBean = openFileContent(flowStructureDao, structureT.getFlowId());
				filePath = JecnFinal.saveFilePath(JecnFinal.FLOW_FILE_RELEASE, new Date(), JecnFinal
						.getSuffixName(openBean.getName()));
				JecnFinal.createFile(filePath, openBean.getFileByte());
			}
		}
		if (!JecnCommon.isNullOrEmtryTrim(pubImagePath)) {// 生成图片路径
			// 删除历史存在的文件
			// if (!JecnCommon.isNullOrEmtryTrim(oldPath)) {
			// // 删除文件
			// JecnFinal.deleteFile(oldPath);
			// // 删除目录
			// JecnFinal.doDelete(JecnCommon.getSavePorcessImagePath(oldPath));
			// }

			if (structureT.getIsFlow() == 1 && structureT.getPagings() > 1) {// 流程，切存在分页
				copyFileDir(savePath, pubImagePath);
			}
			// 更新发布后新生成的图片路径
			String hql = "update JecnFlowStructure set filePath=? where flowId=?";
			flowStructureDao.execteHql(hql, pubImagePath, refId);
			flowStructureDao.getSession().flush();
			log.info("更新JecnFlowStructure （发布生成的图片路径）信息成功！");
		}
		if (filePath != null) {
			paths[0] = filePath;
		} else {
			paths[0] = "";
		}
		if (pubImagePath != null) {
			paths[1] = pubImagePath;
		} else {
			paths[1] = "";
		}
		return paths;
	}

	/**
	 * 获取流程-文件 内容
	 * 
	 * @param flowStructureDao
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	private static FileOpenBean openFileContent(IFlowStructureDao flowStructureDao, Long flowId) throws Exception {
		try {
			FileOpenBean fileOpenBean = new FileOpenBean();
			Object[] obj = flowStructureDao.getFlowFileContent(flowId, "_T");
			fileOpenBean.setId(valueOfLong(obj[1]));
			fileOpenBean.setFileByte(JecnDaoUtil.blobToBytes((Blob) obj[2]));
			fileOpenBean.setName(obj[0].toString());
			return fileOpenBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private static Long valueOfLong(Object obj) {
		return Long.valueOf(obj.toString());
	}

	/**
	 * 获得流程未发布/更新的文件
	 * 
	 * 对这个方法修改时，必须对JecnUtil类findProcessNoPubFiles方法进行修改
	 * 
	 * @param flowId
	 * @return
	 */
	public static List<Object[]> findProcessNoPubFiles(Long flowId, IBaseDao baseDao) throws Exception {
		String sql = "SELECT DISTINCT T.FILE_ID,T.VERSION_ID" + "  FROM JECN_FILE_T T" + " INNER JOIN ("
				+ "             SELECT JFBI.FILE_ID" + "               FROM JECN_FLOW_BASIC_INFO_T JFBI"
				+ "              WHERE JFBI.FLOW_ID = ?" + "             UNION"
				+ "             SELECT JFSI.LINK_FLOW_ID FILE_ID"
				+ "               FROM JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
				+ "              WHERE JFSI.FIGURE_TYPE = 'IconFigure'" + "                AND JFSI.FLOW_ID = ?"
				+ "             UNION" + "             SELECT JAF.FILE_S_ID FILE_ID"
				+ "               FROM JECN_ACTIVITY_FILE_T        JAF,"
				+ "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
				+ "              WHERE JAF.FIGURE_ID = JFSI.FIGURE_ID" + "                AND JFSI.FLOW_ID = ?"
				+ "             UNION" + "             SELECT JMF.FILE_M_ID FILE_ID"
				+ "               FROM JECN_MODE_FILE_T JMF, JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
				+ "              WHERE JMF.FIGURE_ID = JFSI.FIGURE_ID" + "                AND JFSI.FLOW_ID = ?"
				+ "             UNION" + "             SELECT JT.FILE_ID"
				+ "               FROM JECN_TEMPLET_T              JT,"
				+ "                     JECN_MODE_FILE_T            JMF,"
				+ "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
				+ "              WHERE JT.MODE_FILE_ID = JMF.MODE_FILE_ID"
				+ "                AND JMF.FIGURE_ID = JFSI.FIGURE_ID" + "                AND JFSI.FLOW_ID = ?"
				+ "             UNION" + "             SELECT JR.FILE_ID"
				+ "               FROM FLOW_RELATED_CRITERION_T FRC, JECN_RULE_T JR"
				+ "              WHERE JR.IS_DIR = 2 AND JR.IS_FILE_LOCAL=0 "
				+ "                AND JR.ID = FRC.CRITERION_CLASS_ID" + "                AND FRC.FLOW_ID = ?"
				+ " UNION			 " + "SELECT FT.FILE_ID" + "  FROM FLOW_STANDARDIZED_FILE_T FT WHERE FT.FLOW_ID = ?"
				+ "  UNION" + "             SELECT RF.RULE_FILE_ID FILE_ID"
				+ "               FROM FLOW_RELATED_CRITERION_T FRC,"
				+ "                     JECN_RULE_T              JR,"
				+ "                     RULE_TITLE_T             RT,"
				+ "                     RULE_FILE_T              RF" + "              WHERE RF.RULE_TITLE_ID = RT.ID"
				+ "                AND RT.RULE_ID = JR.ID" + "                AND JR.IS_DIR = 1"
				+ "                AND JR.ID = FRC.CRITERION_CLASS_ID" + "                AND FRC.FLOW_ID = ?"
				+ "             UNION" + "             SELECT FT.FILE_ID"
				+ "               FROM JECN_FIGURE_FILE_T FT, JECN_FLOW_STRUCTURE_IMAGE_T IT"
				+ "              WHERE FT.FIGURE_ID = IT.FIGURE_ID"
				+ "                AND IT.FIGURE_TYPE = 'FileImage'" + "                AND IT.FLOW_ID = ?"
				+ " UNION SELECT JFIO.FILE_ID FROM JECN_FIGURE_IN_OUT_T JFIO"
				+ " WHERE JFIO.FLOW_ID=? AND JFIO.FILE_ID IS NOT NULL UNION"
				+ " SELECT JFIOS.FILE_ID FROM JECN_FIGURE_IN_OUT_SAMPLE_T JFIOS"
				+ " WHERE JFIOS.FLOW_ID=? AND JFIOS.FILE_ID IS NOT NULL" + "             ) FILE_REF_TMP"
				+ "    ON T.FILE_ID = FILE_REF_TMP.FILE_ID                                             "
				+ " where T.DEL_STATE = 0 and exists (select 1"
				+ "           from JECN_FILE JF          where T.FILE_ID = JF.FILE_ID"
				+ "            AND (T.UPDATE_TIME <> JF.UPDATE_TIME or JF.UPDATE_TIME is null))" + "   or not exists("
				+ "   select 1 from JECN_FILE JF where T.FILE_ID = JF.FILE_ID )";
		return baseDao.listNativeSql(sql, flowId, flowId, flowId, flowId, flowId, flowId, flowId, flowId, flowId,
				flowId, flowId);
	}

	/**
	 * 获得流程地图未发布/更新的文件
	 * 
	 * 对这个方法修改时，必须对JecnUtil类findProcessMapNoPubFiles方法进行修改
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public static List<Object[]> findProcessMapNoPubFiles(Long flowId, IBaseDao docControlDao) throws Exception {
		String sql = "SELECT DISTINCT T.FILE_ID, T.VERSION_ID" + "  FROM JECN_FILE_T T" + "  INNER JOIN ("
				+ "             SELECT JMF.FILE_ID" + "               FROM JECN_MAIN_FLOW_T JMF"
				+ "              WHERE JMF.FLOW_ID = ?" + "             UNION"
				+ "             SELECT JFSI.LINK_FLOW_ID FILE_ID"
				+ "               FROM JECN_FLOW_STRUCTURE_IMAGE_T JFSI" + "              WHERE JFSI.FIGURE_TYPE = "
				+ JecnCommonSql.getIconString()
				+ "                AND JFSI.FLOW_ID = ?"
				+ "             UNION"
				+ "             SELECT JR.FILE_ID"
				+ "               FROM JECN_RULE_T JR,"
				+ "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
				+ "              WHERE JR.IS_DIR = 2 AND JR.IS_FILE_LOCAL=0 "
				+ "                AND JFSI.FIGURE_TYPE = "
				+ JecnCommonSql.getSystemString()
				+ "                AND JR.ID = JFSI.LINK_FLOW_ID"
				+ "                AND JFSI.FLOW_ID = ?"
				+ " UNION			 "
				+ "SELECT FT.FILE_ID"
				+ "  FROM FLOW_STANDARDIZED_FILE_T FT WHERE FT.FLOW_ID = ?"
				+ "             UNION"
				+ "             SELECT RF.RULE_FILE_ID FILE_ID"
				+ "               FROM JECN_RULE_T                 JR,"
				+ "                     RULE_FILE_T                 RF,"
				+ "                     RULE_TITLE_T                RT,"
				+ "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
				+ "              WHERE RF.RULE_TITLE_ID = RT.ID"
				+ "                AND RT.RULE_ID = JR.ID"
				+ "                AND JR.IS_DIR = 1"
				+ "                AND JFSI.FIGURE_TYPE = "
				+ JecnCommonSql.getSystemString()
				+ "                AND JR.ID = JFSI.LINK_FLOW_ID"
				+ "                AND JFSI.FLOW_ID = ?"
				+ "             UNION"
				+ "             SELECT FT.FILE_ID"
				+ "               FROM JECN_FIGURE_FILE_T FT, JECN_FLOW_STRUCTURE_IMAGE_T IT"
				+ "              WHERE FT.FIGURE_ID = IT.FIGURE_ID"
				+ "                AND IT.FLOW_ID = ?) FILE_REF_TMP"
				+ "    ON T.FILE_ID = FILE_REF_TMP.FILE_ID"
				+ " where T.DEL_STATE = 0 and exists (select 1"
				+ "           from JECN_FILE JF          where T.FILE_ID = JF.FILE_ID"
				+ "            AND (T.UPDATE_TIME <> JF.UPDATE_TIME or JF.UPDATE_TIME is null))"
				+ "   or not exists("
				+ "   select 1 from JECN_FILE JF where T.FILE_ID = JF.FILE_ID )";
		return docControlDao.listNativeSql(sql, flowId, flowId, flowId, flowId, flowId, flowId);
	}

	/**
	 * 获得制度（模版）未发布/更新的文件
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public static List<Object[]> findRuleModeNoPubFiles(Long ruleId, IJecnDocControlDao docControlDao) throws Exception {
		// String sql =
		// "select distinct t.file_id,t.VERSION_ID from jecn_file_t t LEFT JOIN jecn_file jf ON t.file_id = jf.file_id where"
		// +
		// "  jf.del_state=0 and t.file_id in (select jf.file_id from jecn_file_t jf,jecn_rule_t jr,rule_title_t rt,rule_file_t rf where jf.file_id=rf.rule_file_id"
		// +
		// "       and rf.rule_title_id=rt.id and rt.rule_id=jr.id and jr.is_dir=1 and jr.id=?)"
		// + "       and jf.file_id is null";
		String sql = "SELECT DISTINCT T.FILE_ID, T.VERSION_ID" + "  FROM JECN_FILE_T T"
				+ " INNER JOIN (SELECT JF.FILE_ID" + "               FROM JECN_FILE_T  JF,"
				+ "                    JECN_RULE_T  JR," + "                    RULE_TITLE_T RT,"
				+ "                    RULE_FILE_T  RF" + "              WHERE JF.FILE_ID = RF.RULE_FILE_ID"
				+ "                AND RF.RULE_TITLE_ID = RT.ID" + "                AND RT.RULE_ID = JR.ID"
				+ "                AND JR.IS_DIR = 1" + "                AND JR.ID = ?) FILE_REF_TMP"
				+ "    ON T.FILE_ID = FILE_REF_TMP.FILE_ID" + "  LEFT JOIN JECN_FILE JF"
				+ "    ON T.FILE_ID = JF.FILE_ID" + " WHERE T.DEL_STATE = 0"
				+ "   AND (T.UPDATE_TIME > JF.UPDATE_TIME OR JF.UPDATE_TIME IS NULL)";

		return docControlDao.listNativeSql(sql, ruleId);
	}

	/**
	 * 获得制度（文件）未发布的文件
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public static List<Object[]> findRuleFileNoPubFiles(Long ruleId, IJecnDocControlDao docControlDao) throws Exception {
		String sql = "SELECT T.FILE_ID,T.VERSION_ID" + "            FROM JECN_FILE_T T"
				+ "           INNER JOIN (SELECT JR.FILE_ID" + "                         FROM JECN_RULE JR"
				+ "                        WHERE JR.IS_DIR = 2" + "                          AND JR.IS_FILE_LOCAL = 0"
				+ "                          AND JR.ID = ?" + "                       UNION"
				+ "                       SELECT RT.FILE_ID"
				+ "                         FROM RULE_STANDARDIZED_FILE_T RT"
				+ "                        WHERE RT.RELATED_ID = ?) FILE_REF_TMP"
				+ "              ON T.FILE_ID = FILE_REF_TMP.FILE_ID" + "            LEFT JOIN JECN_FILE JF"
				+ "              ON T.FILE_ID = JF.FILE_ID"
				+ "             AND (T.UPDATE_TIME > JF.UPDATE_TIME or JF.UPDATE_TIME is null)";
		return docControlDao.listNativeSql(sql, ruleId, ruleId);
	}

	/**
	 * 获取系统配置文件默认版本号
	 * 
	 * @return
	 */
	public static String getFileVersionValue() {
		// 获取文件系统配置默认的版本号 暂用随机数，系统配置暂取消
		// return configItemDao.selectValue(3, "basicFileAutoPubVersion");
		return UUID.randomUUID().toString();
	}

	/**
	 * 获取系统配置制度默认版本号
	 * 
	 * @return
	 */
	public static String getRuleVersionValue() {
		// 获取文件系统配置默认的版本号 暂用随机数，系统配置暂取消
		// return configItemDao.selectValue(2, "basicRuleAutoPubVersion");
		return UUID.randomUUID().toString();
	}

	/**
	 * 发布流程、制度，处理其相关的未发布文件
	 * 
	 * @param listFileObjects
	 *            文件信息集合
	 * @param historyNew
	 *            版本信息
	 * @param configVersion
	 * @param type
	 *            相关类型 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @param fileDao
	 *            文件DAO接口
	 * @param docControlDao
	 *            文控信息处理接口
	 * @throws Exception
	 */
	public static void pubRefFiles(List<Object[]> listFileObjects, JecnTaskHistoryNew historyNew, String configVersion,
			int type, IFileDao fileDao, IJecnDocControlDao docControlDao) throws Exception {
		if (listFileObjects == null || listFileObjects.size() == 0) {
			return;
		}
		if (!validateRelateFile(type)) {// 相关文件是否关联发布
			return;
		}

		// 文件发布文件
		List<Long> listFileIds = new ArrayList<Long>();
		// 记录文件相关版本ID<key文件ID，value：文件对应的版本ID>
		Map<Long, Long> map = new HashMap<Long, Long>();
		for (Object[] obj : listFileObjects) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			// 文件ID
			Long fileId = Long.valueOf(obj[0].toString());
			// 文件版本ID
			Long versionId = Long.valueOf(obj[1].toString());
			// 记录未发布的文件Id
			listFileIds.add(fileId);
			map.put(fileId, versionId);
		}
		if (listFileIds.size() > 0) {
			String hql = "from JecnFileBeanT where delState=0 and fileID in " + JecnCommonSql.getIds(listFileIds);
			List<JecnFileBeanT> listFileT = fileDao.listHql(hql);
			// 文控信息
			for (Long fileId : listFileIds) {
				JecnTaskHistoryNew jecnTaskHistoryNew = DocControlCommon.getJecnTaskHistoryNew(historyNew);
				jecnTaskHistoryNew.setRelateId(fileId);
				jecnTaskHistoryNew.setType(type);
				// 文控版本号
				jecnTaskHistoryNew.setVersionId(configVersion);
				// 文件版本Id
				jecnTaskHistoryNew.setFileContentId(map.get(fileId));
				// 保存文控信息
				docControlDao.saveJecnTaskHistoryNew(jecnTaskHistoryNew);
				// 更新文件对应的文控信息版本ID
				updateFileT(listFileT, fileId, jecnTaskHistoryNew.getId(), fileDao);
			}
			// 发布相关文件
			fileDao.releaseFiles(listFileIds);
		}
	}

	/**
	 * 验证流程、流程地图、制度模板文件 相关附件是否关联发布
	 * 
	 * @param type
	 *            相关类型 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @return true:关联发布
	 */
	public static boolean validateRelateFile(int type) {
		// 验证配置是否发布关联的文件信息
		switch (type) {// 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
		case 0:
			if ("0".equals(JecnContants.pubItemMap.get(ConfigItemPartMapMark.pubFlowRelateFile.toString()))) {// 流程图相关附件0:不关联发布
				return false;
			}
		case 1:
			break;
		case 2:
			if ("0".equals(JecnContants.pubItemMap.get(ConfigItemPartMapMark.pubRuleFile.toString()))) {// 制度相关附件0:不关联发布
				return false;
			}
		case 3:
			break;
		case 4:
			if ("0".equals(JecnContants.pubItemMap.get(ConfigItemPartMapMark.pubFlowMapRelateFile.toString()))) {// 流程地图相关附件
				// ,0:不关联发布
				return false;
			}
		}
		return true;
	}

	/**
	 * 验证流程、流程地图 相关制度是否关联发布
	 * 
	 * @param type
	 *            相关类型 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @return true:关联发布
	 */
	public static boolean validateRelateRule(int type) {
		// 验证配置是否发布关联的文件信息
		switch (type) {// 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
		case 0:
			if ("0".equals(JecnContants.pubItemMap.get(ConfigItemPartMapMark.pubFlowRelateRule.toString()))) {// 流程图相关附件
				// ,0:不公开
				// ，所以不关联发布
				return false;
			}
		case 1:
		case 2:
		case 3:
			break;
		case 4:
			if ("0".equals(JecnContants.pubItemMap.get(ConfigItemPartMapMark.pubFlowMapRelateRule.toString()))) {// 流程地图相关附件
				// ,0:不公开
				// ，所以不关联发布
				return false;
			}
		}
		return true;
	}

	/**
	 * 更新文件对应的文控信息版本ID
	 * 
	 * @param listFileT
	 *            未发布文件
	 * @param fileId
	 *            文件I D
	 * @param historyId
	 *            文控信息版本ID
	 */
	private static void updateFileT(List<JecnFileBeanT> listFileT, Long fileId, Long historyId, IFileDao fileDao) {
		for (JecnFileBeanT jecnFileBeanT : listFileT) {
			if (jecnFileBeanT.getFileID().equals(fileId)) {
				jecnFileBeanT.setHistoryId(historyId);
				jecnFileBeanT.setUpdateTime(new Date());
				jecnFileBeanT.setPubTime(jecnFileBeanT.getUpdateTime());
				fileDao.update(jecnFileBeanT);
				fileDao.getSession().flush();
				return;
			}
		}
	}

	/**
	 * 发布流程对应的相关制度
	 * 
	 * @param listRuleIdsNoRelease
	 *            流程对应的相关制度
	 * @param historyNew
	 * @throws Exception
	 */
	private static void pubRefRule(List<Object[]> listRuleIdsNoRelease, JecnTaskHistoryNew historyNew,
			IRuleDao ruleDao, IJecnDocControlDao docControlDao, IJecnConfigItemDao configItemDao) throws Exception {
		// 制度文件
		List<Long> listRuleFileIds = new ArrayList<Long>();
		// (key:制度主键ID，value：文件版本ID)
		Map<Long, Long> mapRuleFileVersion = new HashMap<Long, Long>();
		// 制度模板文件
		List<Long> listRuleModeIds = new ArrayList<Long>();
		for (Object[] obj : listRuleIdsNoRelease) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			if ("1".equals(obj[1].toString())) {
				listRuleModeIds.add(Long.valueOf(obj[0].toString()));
			} else {
				listRuleFileIds.add(Long.valueOf(obj[0].toString()));
				if (obj[2] != null) {
					mapRuleFileVersion.put(Long.valueOf(obj[0].toString()), Long.valueOf(obj[2].toString()));
				}
			}
		}
		if (listRuleFileIds.size() > 0) {
			String hql = "from RuleT where id in " + JecnCommonSql.getIds(listRuleFileIds);
			List<RuleT> listRuleT = ruleDao.listHql(hql);
			// 文控信息
			for (Long ruleId : listRuleFileIds) {
				JecnTaskHistoryNew jecnTaskHistoryNew = DocControlCommon.getJecnTaskHistoryNew(historyNew);
				jecnTaskHistoryNew.setRelateId(ruleId);
				jecnTaskHistoryNew.setType(3);
				// 文控版本号
				jecnTaskHistoryNew.setVersionId(getRuleVersionValue());
				// 文件版本Id
				jecnTaskHistoryNew.setFileContentId(mapRuleFileVersion.get(ruleId));
				setRuleExpiry(jecnTaskHistoryNew, ruleDao, ruleId);
				// 保存文控信息
				docControlDao.saveJecnTaskHistoryNew(jecnTaskHistoryNew);
				// 记录制度发布对应当前版本的ID
				updateRuleT(listRuleT, ruleId, jecnTaskHistoryNew.getId(), ruleDao);
			}
			ruleDao.releaseRuleFileData(listRuleFileIds);
		}
		if (listRuleModeIds.size() > 0) {
			String hql = "from RuleT where id in " + JecnCommonSql.getIds(listRuleModeIds);
			List<RuleT> listRuleT = ruleDao.listHql(hql);
			// 文控信息
			for (Long id : listRuleModeIds) {
				JecnTaskHistoryNew jecnTaskHistoryNew = DocControlCommon.getJecnTaskHistoryNew(historyNew);
				jecnTaskHistoryNew.setRelateId(id);
				jecnTaskHistoryNew.setType(2);
				// 通过制度Id获得制度模板文件的程序文件
				ruleFilePath(id, jecnTaskHistoryNew, ruleDao, docControlDao, configItemDao);
				// 文控版本号
				jecnTaskHistoryNew.setVersionId(getRuleVersionValue());
				setRuleExpiry(jecnTaskHistoryNew, ruleDao, id);
				// 保存文控信息
				docControlDao.saveJecnTaskHistoryNew(jecnTaskHistoryNew);
				// 记录制度发布对应当前版本的ID
				updateRuleT(listRuleT, id, jecnTaskHistoryNew.getId(), ruleDao);
			}
			ruleDao.releaseRuleModeData(listRuleModeIds);
		}
	}

	/**
	 * 记录制度发布对应当前版本的ID
	 * 
	 * @param listRuleT
	 *            未发布文件
	 * @param ruleId
	 *            制度
	 * @param historyId
	 *            文控信息版本ID
	 */
	private static void updateRuleT(List<RuleT> listRuleT, Long ruleId, Long historyId, IRuleDao ruleDao) {
		for (RuleT ruleT : listRuleT) {
			if (ruleT.getId().equals(ruleId)) {
				ruleT.setHistoryId(historyId);
				ruleT.setUpdateDate(new Date());
				ruleT.setPubTime(ruleT.getUpdateDate());
				ruleDao.update(ruleT);
				ruleDao.getSession().flush();
				return;
			}
		}
	}

	/**
	 * 制度模板文件操作说明路径获取
	 * 
	 * @param rId
	 * @param historyNew
	 * @throws Exception
	 */
	private static void ruleFilePath(Long rId, JecnTaskHistoryNew historyNew, IRuleDao ruleDao,
			IJecnDocControlDao docControlDao, IJecnConfigItemDao configItemDao) throws Exception {
		// 通过制度Id获得制度模板文件的程序文件
		JecnCreateDoc createDoc = DownloadUtil.getRuleFile(rId, false, ruleDao, docControlDao, configItemDao);
		if (createDoc == null || createDoc.getBytes() == null) {
			return;
		}
		String filePath = JecnFinal.saveFilePath(JecnFinal.RULE_FILE_RELEASE, new Date(), ".doc");
		JecnFinal.createFile(filePath, createDoc.getBytes());
		historyNew.setFilePath(filePath);
	}

	/**
	 * 
	 * 发布给PRF查阅权限人员发送消息
	 * 
	 * @param peopleIds
	 *            人员主键ID集合
	 * @param prfName
	 *            PRF文件名称
	 */
	private static void createMessage(Set<Long> peopleIds, int type, String prfName, Session s) {
		// 已发布！
		String strPub = JecnUtil.getValue("is_public");
		for (Long peopleId : peopleIds) {
			if (peopleId != null) {
				JecnMessage jecnMessage = new JecnMessage();
				jecnMessage.setPeopleId(0L);
				// 消息类型：发布
				jecnMessage.setMessageType(2);
				jecnMessage.setMessageContent(prfName);
				// XX:XX名称已发布！
				jecnMessage.setMessageTopic(getPrfNameByType(type) + prfName + strPub);
				jecnMessage.setNoRead(Long.valueOf(0));
				jecnMessage.setInceptPeopleId(peopleId);
				jecnMessage.setCreateTime(new Date());
				s.save(jecnMessage);
				s.flush();
			}
		}
	}

	/**
	 * 根据关联类型获取中文注释
	 * 
	 * @param type
	 *            0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return
	 */
	private static String getPrfNameByType(int type) {
		switch (type) {
		case 0:// 流程：
			return JecnUtil.getValue("flowC");
		case 1:// 文件：
			return JecnUtil.getValue("fileC");
		case 2:// 制度：
		case 3:// 制度：
			return JecnUtil.getValue("ruleC");
		case 4:// 流程地图：
			return JecnUtil.getValue("flowMap") + JecnUtil.getValue("chinaColon");
		}
		return "";
	}

	/**
	 * 换行标识符替换
	 * 
	 * @param str
	 *            “带\n 的字符串”
	 * @return String
	 */
	public static String rebackReplaceString(String str) {
		if (JecnCommon.isNullOrEmtryTrim(str)) {
			return str;
		}
		return str.replaceAll("\n", "<br>");
	}

	/**
	 * 验证id是否存在 true:不存在
	 */
	public static boolean idIsNotNull(String auditIds) {
		if (JecnCommon.isNullOrEmtryTrim(auditIds) || "-1".equals(auditIds)) {
			return true;
		}
		return false;
	}

	/**
	 * 验证任务上一个阶段是否为拟稿阶段
	 * 
	 * @param upState
	 *            任务上一阶段状态
	 * @param taskElseState
	 *            任务操作
	 * @return
	 */
	public static boolean isDraft(int upState, int taskElseState) {
		if (upState == 0) {// 拟稿阶段

		}
		if (taskElseState == 1) {// 拟稿人创建任务
			return false;
		} else if (taskElseState == 6) {// 拟稿人执行二次评审
			return false;
		} else if (taskElseState == 7) {// 拟稿人重新提交审批
			return false;
		}
		return false;
	}

	/**
	 * 
	 * @param taskType
	 *            0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
	 * @return int大类：0：流程地图 1：流程图 2：制度 3：文件
	 */
	public static int getConfigTypeByTaskType(int taskType) {
		int conType = -1;
		switch (taskType) {
		case 0:// 流程任务
			conType = 1;
			break;
		case 1:// 文件任务
			conType = 3;
			break;
		case 2:// 制度模板文件任务
		case 3:// 制度文件任务
			conType = 2;
			conType = 1;
			break;
		case 4:// 流程地图任务
			conType = 0;
			break;
		}
		return conType;
	}

	/**
	 * 消息邮件配置 验证是否显示
	 * 
	 * @param mark
	 * @param list
	 * @return true:显示
	 */
	public static boolean isTrueMark(String mark, List<JecnConfigItemBean> list) {
		if (JecnCommon.isNullOrEmtryTrim(mark) || list == null) {
			return false;
		}
		String sysMark;
		for (JecnConfigItemBean jecnConfigItemBean : list) {
			sysMark = jecnConfigItemBean.getMark();
			if (mark.equals(sysMark) && "1".equals(jecnConfigItemBean.getValue())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 根据关联类型获取中文注释
	 * 
	 * @param type
	 *            0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @param mesType
	 *            : 1:任务审批；2：任务发布；3：创建任务
	 * @return
	 */
	private static String getMesContants(int mesType) {
		// 任务已审批!
		String strPub = null;
		if (mesType == 1) {// 任务审批
			strPub = "请审批";
		} else if (mesType == 2) {
			strPub = "已发布！";
		} else if (mesType == 3) {
			strPub = "请审批";
		}
		return strPub;
	}

	/**
	 * 任务审批过程发送消息
	 * 
	 * @param type
	 * @return
	 */
	private static String getStrTitle(int type) {
		String strTitle = null;
		if (type == 1) {// 任务审批
			strTitle = "审批任务：";
		} else if (type == 2) {
			strTitle = "发布任务：";
		} else if (type == 3) {
			strTitle = "创建任务：";
		}
		return strTitle;
	}

	/**
	 * 
	 * 发布给PRF查阅权限人员发送消息
	 * 
	 * @param peopleIds
	 *            人员主键ID集合
	 * @param prfName
	 *            PRF文件名称
	 * @param mesType
	 *            1:审批；2：发布；3：创建
	 */
	public static void createTaskMessage(Set<Long> peopleIds, int taskType, String prfName, Session s, int mesType) {
		if (peopleIds == null || JecnCommon.isNullOrEmtryTrim(prfName) || s == null) {
			return;
		}
		for (Long peopleId : peopleIds) {
			if (peopleId != null) {
				JecnMessage jecnMessage = new JecnMessage();
				jecnMessage.setPeopleId(0L);
				// 消息类型：任务
				jecnMessage.setMessageType(1);
				jecnMessage.setMessageContent(prfName);
				String strName = null;
				if (mesType == 2) {// 发布任务
					strName = getStrTitle(mesType) + prfName + getMesContants(mesType);
				} else {// 创建任务或审批任务
					strName = getStrTitle(mesType) + getMesContants(mesType) + prfName;
				}
				jecnMessage.setMessageTopic(strName);
				jecnMessage.setNoRead(Long.valueOf(0));
				jecnMessage.setInceptPeopleId(peopleId);
				jecnMessage.setCreateTime(new Date());
				s.save(jecnMessage);
				s.flush();
			}
		}
	}

	/**
	 * 消息邮件配置
	 * 
	 * @return
	 */
	public static List<JecnConfigItemBean> selectMessageAndEmailItemBean(IJecnConfigItemDao configItemDao) {
		return configItemDao.selectConfigItemBeanByType(JecnConfigContents.TYPE_BIG_ITEM_PUB,
				JecnConfigContents.TYPE_SMALL_MES_EMAIL_ITEM_PUB);
	}

	/**
	 * 回去任务操作配置
	 * 
	 * @return
	 */
	public static List<JecnConfigItemBean> findBackItemBean(int itemType, IJecnConfigItemDao configItemDao) {
		//
		// // 大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置//
		// int itemType = -1;
		// switch (taskType) {
		// case 0:// 流程图
		// itemType = 1;
		// break;
		// case 1:// 文件
		// itemType = 3;
		// break;
		// case 2:// 制度文件
		// case 3:
		// itemType = 2;
		// break;
		// case 4:// 流程地图
		// itemType = 0;
		// break;
		// }
		return configItemDao.selectConfigItemBeanByType(itemType, JecnConfigContents.TYPE_SMALL_TASK_OPERATE_ITEM_PUB);
	}

	/**
	 * 在jecnTaskStageList中按照state获取对应的Bean
	 * 
	 * @author weidp
	 * @date 2014-7-3 下午01:53:54
	 * @param jecnTaskStage
	 * @param state
	 * @return
	 */
	public static JecnTaskStage getCurTaskStage(List<JecnTaskStage> jecnTaskStageList, Integer state) {
		for (JecnTaskStage jecnTaskStage : jecnTaskStageList) {
			if (jecnTaskStage.getStageMark().intValue() == state) {
				return jecnTaskStage;
			}
		}
		return null;
	}

	/**
	 * 任务现阶段是否在文控所处阶段之前
	 * 
	 * @author weidp
	 * @date 2014-7-3 下午03:18:20
	 * @param state
	 * @param jecnTaskStageList
	 * @return
	 */
	public static boolean isStateBeforeDocControl(Integer state, List<JecnTaskStage> jecnTaskStageList) {
		int curSort = -1;
		int docControlSort = -1;
		for (JecnTaskStage taskStage : jecnTaskStageList) {
			if (state == 1 && taskStage.getStageMark().intValue() == state) {
				return true;
			}
			if (taskStage.getStageMark().intValue() == state) {
				curSort = taskStage.getSort();
			} else if (taskStage.getStageMark().intValue() == 1) {
				docControlSort = taskStage.getSort();
			}
		}
		if (docControlSort != -1 && curSort != -1) {
			return docControlSort > curSort;
		}
		return false;
	}

	/**
	 * 获取上个已选择人的阶段的阶段信息
	 * 
	 * @author weidp
	 * @date 2014-7-4 下午07:52:35
	 * @param taskStageList
	 * @param state
	 * @return
	 */
	public static JecnTaskStage getPreTaskStage(List<JecnTaskStage> taskStageList, int curState) {
		if (taskStageList == null || taskStageList.size() == 0) {
			return null;
		}
		// 记录前一个State在集合里的索引
		int preStateIndex = -1;
		// 如果前一个阶段未选择人，则激活此变量，继续向前一个状态搜索
		boolean beforeCurState = false;
		// 从集合的最后一个元素开始找
		for (int curStateIndex = taskStageList.size() - 1; curStateIndex >= 0; curStateIndex--) {
			// 找到当前元素所处的索引
			if (taskStageList.get(curStateIndex).getStageMark().intValue() == curState) {
				// 变更当前索引为--后的索引
				curStateIndex--;
				if (curStateIndex < 0) {
					break;
				}
				// 如果前一个索引处的元素已选择人，则得到其所在位置的索引，结束循环
				if (taskStageList.get(curStateIndex).getIsSelectedUser().intValue() == 1) {
					preStateIndex = curStateIndex;
					break;
				}
				// 当前一个索引的元素未选择人时，激活此元素
				beforeCurState = true;
			}
			// 找到curState前的第一个已选择人的Stage
			if (beforeCurState && taskStageList.get(curStateIndex).getIsSelectedUser() == 1) {
				preStateIndex = curStateIndex;
				break;
			}
		}
		// 返回结果
		if (preStateIndex != -1 && taskStageList.get(preStateIndex) != null) {
			return taskStageList.get(preStateIndex);
		}
		return null;
	}

	/**
	 * 获得下一阶段的JecnTaskStage
	 * 
	 * @author weidp
	 * @date 2014-7-4 下午07:53:39
	 * @param curState
	 * @param taskStageList
	 * @return
	 */
	public static JecnTaskStage getNextTaskStage(List<JecnTaskStage> taskStageList, int curState) {
		if (taskStageList == null || taskStageList.size() == 0) {
			return null;
		}
		// 下一个阶段的索引
		int nextStateIndex = -1;
		// 下一个阶段的是否选择人非已选择状态时，激活此元素
		boolean afterCurState = false;

		for (int curStateIndex = 0; curStateIndex < taskStageList.size(); curStateIndex++) {
			// 找到当前阶段时
			if (taskStageList.get(curStateIndex).getStageMark().intValue() == curState) {
				// 如果下个阶段为已选择人状态，保存下个阶段的索引，结束循环。否则激活afterCurState元素
				curStateIndex++;
				if (curStateIndex == taskStageList.size()) {
					break;
				}
				if (taskStageList.get(curStateIndex).getIsSelectedUser().intValue() == 1) {
					nextStateIndex = curStateIndex;
					break;
				}
				afterCurState = true;

			}
			// 找到任务当前状态后isSelectedUser=1的第一个Stage元素
			if (afterCurState && taskStageList.get(curStateIndex).getIsSelectedUser() == 1) {
				nextStateIndex = curStateIndex;
				break;
			}
		}
		// 返回结果
		if (nextStateIndex != -1 && taskStageList.get(nextStateIndex) != null) {
			return taskStageList.get(nextStateIndex);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static void inheritTaskTemplate(IBaseDao baseDao, Long id, int taskType, Long peopleId) {
		List<Long> ids = new ArrayList<Long>();
		ids.add(id);
		inheritTaskTemplate(baseDao, ids, taskType, peopleId);
	}

	/**
	 * 多文件继承上级节点的任务模板（要求多文件必须在同一个目录下） 继承上级模板不应该记录日志
	 * 
	 * @param baseDao
	 * @param ids
	 * @param taskType
	 * @param peopleId
	 */
	@SuppressWarnings("unchecked")
	public static void inheritTaskTemplate(IBaseDao baseDao, List<Long> ids, int taskType, Long peopleId) {
		String s = "";
		if (taskType == 0 || taskType == 4) {
			s = " select t_path,flow_name from jecn_flow_structure_t where flow_id=?";
		} else if (taskType == 2 || taskType == 3) {
			s = "  select t_path,rule_name from jecn_rule_t where id=?";
		} else if (taskType == 1) {
			s = "  select t_path,file_name from jecn_file_t where file_id=?";
		}
		List<Object[]> objs = baseDao.listNativeSql(s, ids.get(0));
		String tpath = objs.get(0)[0] == null ? null : objs.get(0)[0].toString();
		String name = objs.get(0)[1].toString();
		if (StringUtils.isBlank(tpath)) {
			log.error("创建节点继承模板获取tpath为空");
			return;
		}
		String sql = "select a.templet_id" + "  from prf_related_templet a" + "  where exists("
				+ "  select 1 from task_config_item b where a.templet_id=b.templet_id" + "  )"
				+ "  and a.related_id=? and a.type=? and a.templet_type=?";
		String approveTemplateId = null;
		String abolishTemplateId = null;
		String[] paths = tpath.split("-");
		// 跳过自己，从下向上找
		boolean hasApprove = false;
		boolean hasAbolish = false;
		for (int i = paths.length - 2; i >= 0; i--) {
			if (hasAbolish && hasApprove) {
				break;
			}
			Long pid = Long.valueOf(paths[i]);
			if (!hasApprove) {
				List<String> ts = baseDao.listNativeSql(sql, pid, taskType, 0);
				if (JecnUtil.isNotEmpty(ts)) {
					approveTemplateId = ts.get(0);
					hasApprove = true;
				}
			}
			if (!hasAbolish) {
				List<String> ts = baseDao.listNativeSql(sql, pid, taskType, 2);
				if (JecnUtil.isNotEmpty(ts)) {
					abolishTemplateId = ts.get(0);
					hasAbolish = true;
				}
			}
		}
		try {
			for (Long id : ids) {
				if (hasApprove) {
					JecnUtil.savePrfRelatedTemplet(baseDao, id, taskType, approveTemplateId, 0);
				}

				if (hasAbolish) {
					JecnUtil.savePrfRelatedTemplet(baseDao, id, taskType, abolishTemplateId, 2);
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}
}
