package com.jecn.dataimport.util;



import java.util.List;


public class ActivityShowBean implements java.io.Serializable {
	private String figureId;
	private String activityId;
	private String activityName;
	private String activityShow;
	private String roleName;
	private List<FileBean> ainputList;
	private List<FileActivityOutBean> aoutputList;
	private String activityShowControl;
	private List<JecnRefIndicators> indicatorsList;
	private String activeTime;
	private String sizeRefIndIcators;

	public String getSizeRefIndIcators() {
		return sizeRefIndIcators;
	}

	public void setSizeRefIndIcators(String sizeRefIndIcators) {
		this.sizeRefIndIcators = sizeRefIndIcators;
	}

	public String getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(String activeTime) {
		String date = "";
		if (activeTime != null && !"".equals(activeTime)) {
			String[] strArr = activeTime.split(",");
			if (strArr.length == 2) {
				if (!"".equals(strArr[0])) {
					date = Integer.parseInt(strArr[0]) + "月";
				}
				if (!"".equals(strArr[1])) {
					date = date + Integer.parseInt(strArr[1]) + "日";
				}
			}
		}
		this.activeTime = date;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getActivityShow() {
		return activityShow;
	}

	public void setActivityShow(String activityShow) {
		this.activityShow = activityShow;
	}

	public String getFigureId() {
		return figureId;
	}

	public void setFigureId(String figureId) {
		this.figureId = figureId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<FileBean> getAinputList() {
		return ainputList;
	}

	public void setAinputList(List<FileBean> ainputList) {
		this.ainputList = ainputList;
	}

	public List<FileActivityOutBean> getAoutputList() {
		return aoutputList;
	}

	public void setAoutputList(List<FileActivityOutBean> aoutputList) {
		this.aoutputList = aoutputList;
	}

	public String getActivityShowControl() {
		return activityShowControl;
	}

	public void setActivityShowControl(String activityShowControl) {
		this.activityShowControl = activityShowControl;
	}

	public List<JecnRefIndicators> getIndicatorsList() {
		return indicatorsList;
	}

	public void setIndicatorsList(List<JecnRefIndicators> indicatorsList) {
		this.indicatorsList = indicatorsList;
	}
}
