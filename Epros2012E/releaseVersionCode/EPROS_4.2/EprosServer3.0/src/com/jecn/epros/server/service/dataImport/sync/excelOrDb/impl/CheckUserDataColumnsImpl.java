package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.dataImport.excelorDB.IUserDataCheckDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ICheckUserDataColumns;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

/**
 * 人员同步数据行间校验
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：May 30, 2013 时间：3:39:11 PM
 */
@Transactional(rollbackFor = Exception.class)
public class CheckUserDataColumnsImpl implements ICheckUserDataColumns {
	/** 人员同步数据验证 */
	private IUserDataCheckDao userCheckDao;
	/** 系统配置 */
	private IJecnConfigItemDao configDao;

	/**
	 * 人员原始数据行间校验
	 * 
	 * isNotExistsDept true:不存在部门集合，不同步岗位和部门
	 * 
	 * @return boolean false 存在异常数据
	 */
	@Override
	public boolean checkUserPosColu(boolean isSyncUserOnly) {
		// 获取人员+岗位原始数据
		boolean ret = true;
		// 行间校验
		// 1 .获取重复数据
		List<JecnTmpPosAndUserBean> listUser = userCheckDao.userSameDataCheck();
		Session s = userCheckDao.getSession();
		if (listUser != null) {
			for (JecnTmpPosAndUserBean userBean : listUser) {
				// 有两条登录名称、任职岗位编号都相同数据
				userBean.addError(SyncErrorInfo.TWO_SANME_USER_DATA);
				s.update(userBean);
				ret = false;
			}
		}
		// 2.两个对象，人员编号相同 存在岗位编号不能为空
		List<JecnTmpPosAndUserBean> listUserPosNull = userCheckDao.loginNameSameAndPosNullCheck();
		if (listUserPosNull != null) {
			for (JecnTmpPosAndUserBean userBean : listUserPosNull) {
				// 一人多岗情况，人员对应的岗位编号不能为空
				userBean.addError(SyncErrorInfo.USER_ONE_POS_MUTIL_NULL);
				s.update(userBean);
				ret = false;
			}
		}

		// 3. 一人多岗位:真实姓名、邮箱地址、邮箱内外网表示、电话都应该一样

		// 3.1 一人多岗，真实姓名不同
		List<JecnTmpPosAndUserBean> listUserTreeNameNoSame = userCheckDao.trueNameNoSameCheck();
		if (listUserTreeNameNoSame != null) {
			for (JecnTmpPosAndUserBean userBean : listUserTreeNameNoSame) {
				// 一人多岗情况，人员对应的真实姓名必须相同
				userBean.addError(SyncErrorInfo.USER_TRUE_NAME_NOT_SAME);
				s.update(userBean);
				ret = false;
			}
		}

		// 3.2 一人多岗，邮箱不同
		List<JecnTmpPosAndUserBean> listUserEmailNoSame = userCheckDao.emailNoSameCheck();
		if (listUserEmailNoSame != null) {
			for (JecnTmpPosAndUserBean userBean : listUserEmailNoSame) {
				// 一人多岗情况，人员对应的邮箱、邮箱类型必须相同
				userBean.addError(SyncErrorInfo.USER_EMAIL_NOT_SAME);
				s.update(userBean);
				ret = false;
			}
		}
		// 3.3 一人多岗，真实姓名不同
		List<JecnTmpPosAndUserBean> listUserPhoneNoSame = userCheckDao.phoneNoSameCheck();
		if (listUserPhoneNoSame != null) {
			for (JecnTmpPosAndUserBean userBean : listUserPhoneNoSame) {
				// 一人多岗情况，人员对应的电话必须相同
				userBean.addError(SyncErrorInfo.USER_PHONE_NOT_SAME);
				s.update(userBean);
				ret = false;
			}
		}

		// 4.一人多岗位 岗位编号不能相同
		List<JecnTmpPosAndUserBean> listLoginNameAndSamePos = userCheckDao.sameLoginNameAndSamePos();
		if (listLoginNameAndSamePos != null) {
			for (JecnTmpPosAndUserBean userBean : listLoginNameAndSamePos) {
				if (!JecnCommon.isNullOrEmtryTrim(userBean.getPosNum())) {
					// 出现多条登录名称和岗位编号相同的数据
					userBean.addError(SyncErrorInfo.USER_DATA_NOT_ONLY);
					s.update(userBean);
					ret = false;
				}
			}
		}
		if (!isSyncUserOnly) {
			// 5. 岗位所属部门编号在部门集合中不存在
			List<JecnTmpPosAndUserBean> posNumRefDeptNumNoExists = userCheckDao.posNumRefDeptNumNoExists();
			if (listLoginNameAndSamePos != null) {
				for (JecnTmpPosAndUserBean userBean : posNumRefDeptNumNoExists) {
					if (!JecnCommon.isNullOrEmtryTrim(userBean.getPosNum())) {
						// 岗位所属部门编号在部门集合中不存在
						userBean.addError(SyncErrorInfo.POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST);
						s.update(userBean);
						ret = false;
					}
				}
			}
		}

		// 是否启用该验证
		String flag = configDao.selectValue(5, "allowSamePosName");
		if (!"1".equals(flag)) {
			// 6.同一部门下岗位名称不能重复
			List<JecnTmpPosAndUserBean> listPostBean = userCheckDao.deptHashSamePosName();
			if (listPostBean != null) {
				for (JecnTmpPosAndUserBean userBean : listPostBean) {
					// 同一部门下岗位名称不能重复
					userBean.addError(SyncErrorInfo.POS_NAME_NOT_ONLY);
					s.update(userBean);
					ret = false;
				}
			}
		}

		// 7.岗位编号不唯一
		List<JecnTmpPosAndUserBean> posNumNotOnly = userCheckDao.posNumNotOnly();
		if (posNumNotOnly != null) {
			for (JecnTmpPosAndUserBean userBean : posNumNotOnly) {
				// 岗位编号不唯一
				userBean.addError(SyncErrorInfo.POS_NUM_NOT_ONLY);
				s.update(userBean);
				ret = false;
			}
		}

		if (!isSyncUserOnly) {
			// 8.岗位编号相同，岗位名称必须相同
			List<JecnTmpPosAndUserBean> posNameNotSameList = userCheckDao.posNameNotSameList();
			if (posNameNotSameList != null) {
				for (JecnTmpPosAndUserBean userBean : posNameNotSameList) {
					// 岗位编号相同，岗位名称必须相同
					userBean.addError(SyncErrorInfo.POS_NAME_NOT_SAME);
					s.update(userBean);
					ret = false;
				}
			}

			// 9.岗位编号相同，岗位所属部门必须相同
			List<JecnTmpPosAndUserBean> posNumSameAndDeptSameList = userCheckDao.posNumSameAndDeptSameList();
			if (posNameNotSameList != null) {
				for (JecnTmpPosAndUserBean userBean : posNumSameAndDeptSameList) {
					// 岗位编号相同，岗位所属部门必须相同
					userBean.addError(SyncErrorInfo.POS_NUM_SAME_DEPT_SAME);
					s.update(userBean);
					ret = false;
				}
			}
		}

		return ret;
	}

	/**
	 * 部门外数据行间校验
	 * 
	 * @return
	 */
	@Override
	public boolean checkDeptColu() {
		boolean ret = true;
		Session s = userCheckDao.getSession();
		// 部门行间校验，部门编号不唯一
		List<JecnTmpOriDept> listSameDeptNum = userCheckDao.hasSameDeptNum();
		if (listSameDeptNum != null) {
			for (JecnTmpOriDept oriDept : listSameDeptNum) {
				// 部门编号不唯一
				oriDept.addError(SyncErrorInfo.DEPT_NUM_NOT_ONLY);
				s.update(oriDept);
				ret = false;
			}
		}
		// 海信同步时会传递父组织编号为0000000的数据，该组织可能会重复。因此不做此校验
		if (JecnContants.otherLoginType != 10) {
			// 同一部门下部门名称不能重复
			// List<JecnTmpOriDept> listSameDeptName = userCheckDao
			// .hasSameDeptName();
			// if (listSameDeptName != null) {
			// for (JecnTmpOriDept oriDept : listSameDeptName) {
			// // 同一部门下部门名称不能重复
			// oriDept.addError(SyncErrorInfo.DEPT_NAME_NOT_ONLY_BY_PDEPT);
			// s.update(oriDept);
			// ret = false;
			// }
			// }
		}
		// // 按照部门的树形顺序深度排序,出现部门循环关联（A->B->C-A）返回false 存在父节点编号在当前数据是否存在验证
		// ret = sortDept(deptBeanList);
		return ret;
	}

	public IUserDataCheckDao getUserCheckDao() {
		return userCheckDao;
	}

	public void setUserCheckDao(IUserDataCheckDao userCheckDao) {
		this.userCheckDao = userCheckDao;
	}

	public void setConfigDao(IJecnConfigItemDao configDao) {
		this.configDao = configDao;
	}
}
