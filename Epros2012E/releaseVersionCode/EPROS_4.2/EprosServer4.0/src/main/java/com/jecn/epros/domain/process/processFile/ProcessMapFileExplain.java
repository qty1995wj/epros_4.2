package com.jecn.epros.domain.process.processFile;

import com.jecn.epros.domain.LinkResource;

public class ProcessMapFileExplain {
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程地图ID
     */
    private Long flowId;
    /**
     * 目的
     */
    private String flowAim;
    /**
     * 范围
     */
    private String flowArea;
    /**
     * 术语定义
     */
    private String flowNounDefine;
    /**
     * 输入
     */
    private String flowInput;
    /**
     * 输出
     */
    private String flowOutput;
    /**
     * 步骤
     */
    private String flowShowStep;
    /**
     * 附件（不存放在数据库中）
     */
    private LinkResource fileLink;
    private LinkResource mapLink;

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowAim() {
        return flowAim;
    }

    public void setFlowAim(String flowAim) {
        this.flowAim = flowAim;
    }

    public String getFlowArea() {
        return flowArea;
    }

    public void setFlowArea(String flowArea) {
        this.flowArea = flowArea;
    }

    public String getFlowNounDefine() {
        return flowNounDefine;
    }

    public void setFlowNounDefine(String flowNounDefine) {
        this.flowNounDefine = flowNounDefine;
    }

    public String getFlowInput() {
        return flowInput;
    }

    public void setFlowInput(String flowInput) {
        this.flowInput = flowInput;
    }

    public String getFlowOutput() {
        return flowOutput;
    }

    public void setFlowOutput(String flowOutput) {
        this.flowOutput = flowOutput;
    }

    public String getFlowShowStep() {
        return flowShowStep;
    }

    public void setFlowShowStep(String flowShowStep) {
        this.flowShowStep = flowShowStep;
    }

    public LinkResource getFileLink() {
        return fileLink;
    }

    public void setFileLink(LinkResource fileLink) {
        this.fileLink = fileLink;
    }

    public LinkResource getMapLink() {
        return mapLink;
    }

    public void setMapLink(LinkResource mapLink) {
        this.mapLink = mapLink;
    }
}
