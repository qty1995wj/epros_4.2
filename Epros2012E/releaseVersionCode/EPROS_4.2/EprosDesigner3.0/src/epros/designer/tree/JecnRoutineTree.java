package epros.designer.tree;

import java.util.List;

/**
 * @author zhangchen Apr 28, 2012
 * @description：树节点一次load所有的子节点
 */
public abstract class JecnRoutineTree extends JecnTree {

	public JecnRoutineTree() {

	}

	public JecnRoutineTree(List<Long> listIds) {
		super(listIds);
	}

}
