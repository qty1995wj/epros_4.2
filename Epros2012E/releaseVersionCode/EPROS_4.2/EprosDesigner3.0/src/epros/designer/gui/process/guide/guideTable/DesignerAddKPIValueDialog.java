//package epros.designer.gui.process.guide.guideTable;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//
//import com.jecn.epros.server.bean.process.JecnFlowKpi;
//
//import epros.designer.gui.process.guide.AddKPIValueDialog;
//import epros.designer.util.JecnProperties;
//
///*******************************************************************************
// * 添加KPI值
// * 
// * @author 2012-07-10
// * 
// */
//public class DesignerAddKPIValueDialog extends AddKPIValueDialog {
//	private static Logger log = Logger.getLogger(DesignerAddKPIValueDialog.class);
//	protected boolean isOperation = false;
//
//	private String honValue = null;
//
//	private JecnFlowKpi flowKpi;
//
//	public DesignerAddKPIValueDialog(String verName, String honName,
//			String kpiName, Long flowKpiId, Long flowId, JecnFlowKpi flowKpi) {
//		super(verName, honName, kpiName, flowKpiId, flowId);
//		this.flowKpi = flowKpi;
//		if (honName != null || !"".equals(honName)) {
//			if (honName.equals(JecnProperties.getValue("dayLab"))) {
//				this.setFlagNum(0L);
//			} else if (honName.equals(JecnProperties.getValue("yearLab"))) {
//				this.setFlagNum(4L);
//			} else if (honName.equals(JecnProperties.getValue("weekLab"))) {
//				this.setFlagNum(1L);
//			} else if (honName.equals(JecnProperties.getValue("monthLab"))) {
//				this.setFlagNum(2L);
//			} else if (honName.equals(JecnProperties.getValue("quarterLab"))) {
//				this.setFlagNum(3L);
//			}
//		}
//		this.setLocationRelativeTo(null);
//	}
//
//	@Override
//	public void saveData() {
//		String reg = "^[0-9]*$";
//		String nreg = "^\\d{4}$";
//		String strLongitudinal = this.longitudinalField.getText();
//
//		String weekTex = this.longtwoField.getText();// 周 月 季度 天数
//		String yearTex = this.longOneField.getText();// 年
//		// SimpleDateFormat sdf = null;
//		// sdf = new SimpleDateFormat("yyyy");
//		// Date dateHonName = null;
//		if (strLongitudinal == null || "".equals(strLongitudinal)) {
//			this.okShowInfo.setText("请输入KPI纵坐标值");
//			return;
//		} else if (!strLongitudinal.matches(reg)) {
//			this.okShowInfo.setText("KPI纵坐标值只能输入数字");
//			return;
//		}
//		if (honName.equals(JecnProperties.getValue("dayLab"))) {
//			if (this.transverseDateField.getText() == null
//					|| "".equals(this.transverseDateField.getText().toString())) {
//				this.okShowInfo.setText("请输入KPI横坐标值！");
//				return;
//			}
//		} else {
//			if (!honName.equals(JecnProperties.getValue("yearLab"))) {
//
//				if (weekTex == null || "".equals(weekTex)) {
//					// 周
//					if (honName.equals(JecnProperties.getValue("weekLab"))) {
//						this.okShowInfo.setText("KPI横坐标值周不能为空");
//					}
//					// 月
//					else if (honName
//							.equals(JecnProperties.getValue("monthLab"))) {
//						this.okShowInfo.setText("KPI横坐标值月不能为空");
//					}
//					// 季度
//					else if (honName.equals(JecnProperties
//							.getValue("quarterLab"))) {
//						this.okShowInfo.setText("KPI横坐标值季度不能为空");
//					}
//					return;
//				} else {
//					// 周
//					if (honName.equals(JecnProperties.getValue("weekLab"))) {
//						if (Integer.parseInt(weekTex) > 52) {
//							this.okShowInfo.setText("KPI横坐标值周值不能大于52");
//							return;
//						}
//
//					}
//					// 月
//					if (honName.equals(JecnProperties.getValue("monthLab"))) {
//						if (Integer.parseInt(weekTex) > 12) {
//							this.okShowInfo.setText("KPI横坐标值月值不能大于12");
//							return;
//						}
//
//					}
//					// 季度
//					if (honName.equals(JecnProperties.getValue("quarterLab"))) {
//						if (Integer.parseInt(weekTex) > 4) {
//							this.okShowInfo.setText("KPI横坐标值季度值不能大于4");
//							return;
//						}
//
//					}
//				}
//			}
//			if (yearTex == null || "".equals(yearTex)) {
//				this.okShowInfo.setText("KPI横坐标值年不能为空");
//				return;
//			} else if (!yearTex.matches(nreg)) {
//				// dateHonName = sdf.parse(honValue);
//				this.okShowInfo.setText("KPI横坐标值年只能输入四位数字");
//				return;
//			}
//
//		}
//		SimpleDateFormat sdf = null;
//		String mothStr = null;
//		if (honName != null || !"".equals(honName)) {
//			if (honName.equals(JecnProperties.getValue("dayLab"))) {
//				honValue = this.transverseDateField.getText();
//				sdf = new SimpleDateFormat("yyyy-MM-dd");
//			} else if (honName.equals(JecnProperties.getValue("yearLab"))) {
//				honValue = this.yearField.getText();
//				sdf = new SimpleDateFormat("yyyy");
//			} else if (honName.equals(JecnProperties.getValue("weekLab"))) {// //////////////////////////////周
//				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//				mothStr = this.longtwoField.getText();
//				honValue = this.longOneField.getText() + "-01" + "-01 " + "01:"
//						+ mothStr + ":01";
//			} else if (honName.equals(JecnProperties.getValue("monthLab"))) {
//				mothStr = this.longtwoField.getText();
//				honValue = this.longOneField.getText() + "-" + mothStr;
//				sdf = new SimpleDateFormat("yyyy-MM");
//			} else if (honName.equals(JecnProperties.getValue("quarterLab"))) {
//				mothStr = this.longtwoField.getText();
//				if (mothStr.equals("1")) {
//					mothStr = "1";
//				} else if (mothStr.equals("2")) {
//					mothStr = "4";
//				} else if (mothStr.equals("3")) {
//					mothStr = "7";
//				} else if (mothStr.equals("4")) {
//					mothStr = "10";
//				}
//				honValue = this.longOneField.getText() + "-" + mothStr;
//				sdf = new SimpleDateFormat("yyyy-MM");
//			}
//		}
//		flowKpi.setKpiAndId(flowKpiId);
//
//		Date dateHonName = null;
//		try {
//			dateHonName = sdf.parse(honValue);
//		} catch (ParseException e) {
//			log.error("添加KPI值时间转换异常",e);
//		}
//		flowKpi.setKpiHorVlaue(dateHonName);
//		// 纵坐标
//		flowKpi.setKpiValue(this.longitudinalField.getText());
//		isOperation = true;
//		this.dispose();
//	}
//
//	@Override
//	public String getKPIValueTitle() {
//		return JecnProperties.getValue("addFlowKPIVal");
//	}
//
//	@Override
//	public String getLongField() {
//		return verName;
//	}
//
//	@Override
//	public String getTransverseField() {
//		return honName;
//	}
//
//	@Override
//	public String getKPIName() {
//		return kpiName;
//	}
//}
