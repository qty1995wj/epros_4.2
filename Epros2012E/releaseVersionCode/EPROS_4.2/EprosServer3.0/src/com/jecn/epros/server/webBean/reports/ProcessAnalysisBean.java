package com.jecn.epros.server.webBean.reports;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程分析统计Bean
 * 
 * @author fuzhh Feb 28, 2013
 * 
 */
public class ProcessAnalysisBean {

	/** 时间类型 1日 2周 3月 4季 5年*/
	private String timeType;
	/** 流程列表*/
	private List<ProcessWebBean> listProcessWebBean;
	/** 个数 */
	private int countTotal;
	public String getTimeType() {
		return timeType;
	}
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}
	public List<ProcessWebBean> getListProcessWebBean() {
		return listProcessWebBean;
	}
	public void setListProcessWebBean(List<ProcessWebBean> listProcessWebBean) {
		this.listProcessWebBean = listProcessWebBean;
	}
	public int getCountTotal() {
		return countTotal;
	}
	public void setCountTotal(int countTotal) {
		this.countTotal = countTotal;
	}
	
}
