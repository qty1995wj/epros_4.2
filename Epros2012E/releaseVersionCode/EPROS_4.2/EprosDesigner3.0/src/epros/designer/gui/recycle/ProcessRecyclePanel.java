package epros.designer.gui.recycle;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 文件回收站
 * 
 * @author Administrator
 * @date 2015-03-10
 */
public class ProcessRecyclePanel extends RecyclePanel {

	private ProcessRecycleTree recycleTree;
	private Logger log = Logger.getLogger(ProcessRecyclePanel.class);

	public ProcessRecyclePanel() {
		super();
	}

	@Override
	public String getSearchName() {
		return JecnProperties.getValue("flowNameC");
	}

	@Override
	public RecycleTree initTree() {
		recycleTree = new ProcessRecycleTree();
		recycleTree.setRecyclePanel(this);
		return recycleTree;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {

		try {

			return ConnectionPool.getProcessAction().getDelAuthorityByName(name, JecnConstants.projectId,
					JecnConstants.loginBean.getSetFlow(), JecnConstants.loginBean.isAdmin());

		} catch (Exception e) {
			log.error("ProcessRecyclePanel searchByName is error", e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	public void delDataAction(List<Long> listIds) {
		try {
			ConnectionPool.getProcessAction().trueDelete(listIds, JecnConstants.projectId, JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("ProcessRecyclePanel delDataAction is error", e);
		}
	}

	@Override
	public String getTreeBorderName() {

		return JecnProperties.getValue("flowManage");
	}
}
