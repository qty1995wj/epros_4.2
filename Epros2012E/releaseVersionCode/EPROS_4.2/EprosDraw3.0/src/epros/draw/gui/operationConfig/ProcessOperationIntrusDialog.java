package epros.draw.gui.operationConfig;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.operationConfig.bean.JecnFlowOperationBean;
import epros.draw.gui.operationConfig.data.JecnFlowOperationDataUnit;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程操作说明
 * 
 * @author 2012-07-05
 * 
 */
public class ProcessOperationIntrusDialog extends JecnDialog implements CaretListener, ActionListener {
	Insets insets = new Insets(5, 5, 5, 5);
	Insets insetp = new Insets(0, 0, 0, 0);

	/** 主面板 */
	protected JecnPanel mainPanel = new JecnPanel(600, 700);

	/** 文本Panel */
	protected JecnPanel infoPanel = new JecnPanel(590, 630);

	/** 滚动面板内信息面板 */
	protected JecnPanel contentPanel = new JecnPanel();

	/** 结果滚动面板 */
	protected JScrollPane resultScrollPane = new JScrollPane(contentPanel);

	/** 按钮面板 */
	protected JecnPanel buttonPanel = new JecnPanel(590, 40);

	// ****************************************************************
	/** 目的Lab */
	protected JLabel purposeLab = new JLabel();

	/** 目的Field */
	protected JecnTextArea purposeArea = new JecnTextArea();

	/** 目的错误提示框 */
	protected JLabel purposeErrorLab = new JLabel();

	// ****************************************************************

	/** 适用范围Lab */
	protected JLabel applicationScopeLab = new JLabel();

	/** 适用范围Field */
	protected JecnTextArea applicationScopeArea = new JecnTextArea();
	/** 适用范围错误提示 */
	protected JLabel applicationScopeErrorLab = new JLabel();

	// ****************************************************************
	/** 术语定义Lab */
	protected JLabel definedTermLab = new JLabel();

	/** 术语定义Field */
	protected JecnTextArea definedTermArea = new JecnTextArea();
	/** 术语定义错误提示 */
	protected JLabel definedTermErrorLab = new JLabel();

	// ****************************************************************
	/** 流程驱动规则Lab */
	protected JLabel flowDriveRuleLab = new JLabel();

	/** 流程驱动规则Panel */
	protected JecnPanel flowDrivePanel = new JecnPanel();

	/** 驱动类型Lab */
	protected JLabel driveTypeLab = new JLabel();

	/** 驱动类型ComboBox */
	protected JComboBox driveTypeCombox = new JComboBox(new String[] { JecnResourceUtil.getJecnResourceUtil().getValue("eventDriven"),
			JecnResourceUtil.getJecnResourceUtil().getValue("timeDrive"), JecnResourceUtil.getJecnResourceUtil().getValue("timeAndEventDriven") });

	/** 驱动规则Lab */
	protected JLabel driveRuleLab = new JLabel();

	/** 驱动规则Field */
	protected JecnTextArea driveRuleArea = new JecnTextArea();
	/** 驱动规则滚动面板 */
	protected JScrollPane driveRuleScrollPane = new JScrollPane(driveRuleArea);
	/**
	 * ************** 时间驱动下的组件 start *******************************************
	 */
	/** 类型选择Panel */
	protected JecnPanel typeChangePanel = new JecnPanel(530, 50);

	protected JLabel typeChangeLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("typeChoice"));

	/** 类型选择ComboBox */
	protected JComboBox typeChangeCombox = new JComboBox(new String[] { "", JecnResourceUtil.getJecnResourceUtil().getValue("day"),
			JecnResourceUtil.getJecnResourceUtil().getValue("weeks"), JecnResourceUtil.getJecnResourceUtil().getValue("month"), JecnResourceUtil.getJecnResourceUtil().getValue("season"),
			JecnResourceUtil.getJecnResourceUtil().getValue("years") });

	/** 类型选择空白 */
	protected JLabel emptyChange = new JLabel();

	/** 开始时间空白 */
	protected JLabel emptyStart = new JLabel();

	/** * 结束时间空白 */
	protected JLabel emptyEnd = new JLabel();

	/**
	 * 开始时间*****************************************************************
	 * Panel
	 */
	protected JecnPanel startTimePanel = new JecnPanel();

	/** 时间Panel */
	protected JecnPanel timePanel = new JecnPanel(540, 50);

	/** 日 类型 */
	protected JLabel startDayLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("dayType"));
	/** 开始小时单位 */
	protected JLabel startHourUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("when"));
	/** 开始分钟单位 */
	protected JLabel startMinuteUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("points"));
	/** 开始秒单位 */
	protected JLabel startSecondUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("seconds"));
	protected JComboBox startHourCombox = new JComboBox(returnStrNum(0, 23));
	protected JComboBox startMinuteCombox = new JComboBox(returnStrNum(0, 59));
	protected JComboBox startSecondCombox = new JComboBox(returnStrNum(0, 59));

	/** 周 类型 */
	protected JLabel startWeekLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("weeksType"));
	protected JComboBox startWeekCombox = new JComboBox(new String[] { JecnResourceUtil.getJecnResourceUtil().getValue("Monday"),
			JecnResourceUtil.getJecnResourceUtil().getValue("onTuesday"), JecnResourceUtil.getJecnResourceUtil().getValue("wednesday"),
			JecnResourceUtil.getJecnResourceUtil().getValue("thursday"), JecnResourceUtil.getJecnResourceUtil().getValue("friday"),
			JecnResourceUtil.getJecnResourceUtil().getValue("saturday"), JecnResourceUtil.getJecnResourceUtil().getValue("sunday") });

	/** 月 类型 */
	protected JLabel startMonthLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("monthType"));
	/** 开始日 单位 */
	protected JLabel startMonthDayUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("day"));
	protected JComboBox startMonthCombox = new JComboBox(returnStrNum(1, 31));
	/** 季 类型 */
	protected JLabel startSeasonLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("seasonType"));
	/** 开始季单位 */
	protected JLabel startSeaDayUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("day"));
	protected JComboBox startSeasonCombox = new JComboBox(returnStrNum(1, 92));
	/** 年 类型 */
	protected JLabel startYearLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("yearsType"));
	/** 年--月 单位 */
	protected JLabel startYearMonthUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("month"));
	/** 年--日 单位 */
	protected JLabel startYearDayUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("day"));
	protected JComboBox startYearCombox = new JComboBox(returnStrNum(1, 12));
	protected JComboBox startYearDayCombox = new JComboBox(returnStrNum(1, 31));
	/**
	 * 结束时间*******************************************************************
	 * Panel
	 */
	protected JecnPanel endTimePanel = new JecnPanel();
	/** 日 类型 */
	protected JLabel endDayLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("dayType"));
	/** 结束小时单位 */
	protected JLabel endHourUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("when"));
	/** 结束分钟单位 */
	protected JLabel endMinuteUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("points"));
	/** 结束秒单位 */
	protected JLabel endSecondUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("seconds"));
	protected JComboBox endHourCombox = new JComboBox(returnStrNum(0, 23));
	protected JComboBox endMinuteCombox = new JComboBox(returnStrNum(0, 59));
	protected JComboBox endSecondCombox = new JComboBox(returnStrNum(0, 59));

	/** 周 类型 */
	protected JLabel endWeekLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("weeksType"));
	protected JComboBox endWeekCombox = new JComboBox(new String[] { JecnResourceUtil.getJecnResourceUtil().getValue("Monday"),
			JecnResourceUtil.getJecnResourceUtil().getValue("onTuesday"), JecnResourceUtil.getJecnResourceUtil().getValue("wednesday"),
			JecnResourceUtil.getJecnResourceUtil().getValue("thursday"), JecnResourceUtil.getJecnResourceUtil().getValue("friday"),
			JecnResourceUtil.getJecnResourceUtil().getValue("saturday"), JecnResourceUtil.getJecnResourceUtil().getValue("sunday") });

	/** 月 类型 */
	protected JLabel endMonthLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("monthType"));
	/** 结束日 单位 */
	protected JLabel endMonthDayUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("day"));
	protected JComboBox endMonthCombox = new JComboBox(returnStrNum(1, 31));
	/** 季 类型 */
	protected JLabel endSeasonLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("seasonType"));
	/** 结束季单位 */
	protected JLabel endSeaDayUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("day"));
	protected JComboBox endSeanCombox = new JComboBox(returnStrNum(1, 92));

	/** 年 类型 */
	protected JLabel endYearLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("yearsType"));
	/** 结束年--月 单位 */
	protected JLabel endYearMonthUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("month"));
	/** 结束年--日 单位 */
	protected JLabel endYearDayUnitLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("day"));
	protected JComboBox endYearCombox = new JComboBox(returnStrNum(1, 12));
	protected JComboBox endYearDayCombox = new JComboBox(returnStrNum(1, 31));

	/** 频率Lab */
	protected JLabel frequencyLab = new JLabel();

	/** 频率Field */
	protected JecnTextField frequencyField = new JecnTextField(100);

	/** 频率次数Lab */
	protected JLabel orderLab = new JLabel();

	/** 驱动规则错误提示 */
	protected JLabel orderErrorLab = new JLabel();

	/** ************** 时间驱动下的组件 end ******************************************* */
	// ****************************************************************
	/** 输入Lab */
	protected JLabel inLab = new JLabel();

	/** 输入Field */
	protected JecnTextArea inField = new JecnTextArea();

	/** 输入错误提示 */
	protected JLabel inErrorLab = new JLabel();

	// ****************************************************************
	/** 输出Lab */
	protected JLabel outLab = new JLabel();

	/** 输出Field */
	protected JecnTextArea outField = new JecnTextArea();

	/** 输出错误提示 */
	protected JLabel outErrorLab = new JLabel();

	// ****************************************************************
	/** 关键活动Lab */
	protected JLabel keyActiveLab = new JLabel();

	/** 问题区域Lab */
	protected JLabel questionAreaLab = new JLabel();

	/** 问题区域Table */
	protected JTable questionAreaTable = null;

	/** 问题区域******************JScrollPane */
	protected GuideJScrollPane questionAreaScrollPane = new GuideJScrollPane();

	/** 关键成功因素Lab */
	protected JLabel keySuccessLab = new JLabel();

	/** 关键成功因素Table */
	protected JTable keySuccessTable = null;

	/** 关键成功因素******************JScrollPane */
	protected GuideJScrollPane keySuccessScrollPane = new GuideJScrollPane();

	/** 关键控制点Lab */
	protected JLabel keyContrPointLab = new JLabel();

	/** 关键控制点Table */
	protected JTable keyContrPointTable = null;

	/** 关键控制点******************JScrollPane */
	protected GuideJScrollPane keyContrPointScrollPane = new GuideJScrollPane();
	// ****************************************************************
	/** 角色职责Lab */
	protected JLabel roleResponseLab = new JLabel();

	/** * 角色职责Table */
	protected JTable roleResponseTable = null;

	/** 角色职责******************JScrollPane */
	protected GuideJScrollPane toleResponseScrollPane = new GuideJScrollPane();
	// ****************************************************************
	/** 流程图Lab */
	protected JLabel flowLab = new JLabel();

	/** 流程图Lab */
	protected JecnTextField flowValueField = new JecnTextField(540);

	// ****************************************************************
	/** 活动说明Lab */
	protected JLabel activeDescLab = new JLabel();

	/** * 活动说明Table */
	protected ActiveDescTable activeDescTable = null;
	/** 活动说明 */
	protected JTable drawActiveDescTable = null;

	/** 活动说明******************JScrollPane */
	protected GuideJScrollPane activeDescScrollPane = new GuideJScrollPane();
	// ****************************************************************
	/** 流程记录Lab */
	protected JLabel flowRecordLab = new JLabel();

	/** 流程记录Table */
	protected JTable flowFileTable = null;

	/** 流程记录******************JScrollPane */
	protected GuideJScrollPane flowFileScrollPane = new GuideJScrollPane();
	/** 流程记录 单机版输入框Field */
	protected JecnTextArea flowFileArea = new JecnTextArea();
	// ****************************************************************
	/** 操作规范Lab */
	protected JLabel practicesLab = new JLabel();

	/** 操作规范table */
	protected JTable practicesTable = null;

	/** 操作规范******************JScrollPane */
	protected GuideJScrollPane practicesScrollPane = new GuideJScrollPane();
	/** 操作规范 单机版输入框Field */
	protected JecnTextArea practicesArea = new JecnTextArea();
	// ****************************************************************
	/** 相关流程Lab */
	protected JLabel relatedFlowLab = new JLabel();

	/** 相关流程Table */
	protected RelatedFlowTable relatedFlowTable = null;

	/** 相关流程******************JScrollPane */
	protected GuideJScrollPane relatedFlowScrollPane = new GuideJScrollPane();
	/** 相关流程 单机版输入框Field */
	protected JecnTextArea relatedFlowArea = new JecnTextArea();
	// ****************************************************************
	/** 相关制度Lab */
	protected JLabel relatedRuleLab = new JLabel();

	/** 相关制度 */
	protected RealtedRuleTable realtedRuleTable = null;

	/** 相关制度******************JScrollPane */
	protected GuideJScrollPane relatedRuleScrollPane = new GuideJScrollPane();

	/** 相关风险Label */
	protected JLabel riskLab = new JLabel();
	/** 相关风险JScrollPane */
	protected GuideJScrollPane riskScrollPane = new GuideJScrollPane();
	/** 相关风险Table */
	protected JTable riskTable = null;
	/** 相关风险Field */
	protected JecnTextArea riskArea = new JecnTextArea();
	/** 相关风险滚动面板 */
	protected JScrollPane riskPane = new JScrollPane(riskArea);

	/** 相关制度 单机版输入框Field */
	protected JecnTextArea relatedRuleArea = new JecnTextArea();
	// ****************************************************************
	/** 流程关键测评指标版Lab Processes key evaluation indicators Edition */
	protected JLabel keyEvaluationIndiLab = new JLabel();

	/** 流程关键测评指标 */
	protected JTable processEvaluationTable = null;

	/** 流程关键测评指标版******************JScrollPane */
	protected GuideJScrollPane keyEvaluationIndiScrollPane = new GuideJScrollPane();
	/** 流程关键测评指标 单机版输入框Field */
	protected JecnTextArea keyEvaluationIndiArea = new JecnTextArea();
	// ****************************************************************

	/** 客户Lab */
	protected JLabel clientLab = new JLabel();

	/** 客户Field */
	protected JecnTextField clientField = new JecnTextField(540);

	/** 客户错误提示 */
	protected JLabel clientErrorLab = new JLabel();

	// ****************************************************************

	/** 不适用范围Lab */
	protected JLabel notAplicationScopeLab = new JLabel();

	/** 不适用范围Area */
	protected JecnTextArea notAplicationScopeArea = new JecnTextArea();

	/** 不适用范围错误提示 */
	protected JLabel notAplicationScopeErrorLab = new JLabel();

	// ****************************************************************

	/** 概述Lab */
	protected JLabel outlineLab = new JLabel();

	/** 概述Area */
	protected JecnTextArea outlineArea = new JecnTextArea();

	/** 概述错误提示 */
	protected JLabel outlineErrorLab = new JLabel();

	// ****************************************************************
	/** 补充说明Lab */
	protected JLabel supplementLab = new JLabel();

	/** 补充说明Area */
	protected JecnTextArea supplementArea = new JecnTextArea();

	/** 补充说明错误提示 */
	protected JLabel supplementErrorLab = new JLabel();

	// ****************************************************************
	/** 记录保存Lab */
	protected JLabel recordSaveLab = new JLabel();

	/** 记录保存Table */
	protected RecordSaveTable recordSaveTable = null;

	/** 记录保存******************JScrollPane */
	protected GuideJScrollPane recordSaveScrollPane = new GuideJScrollPane();
	// ****************************************************************
	/** recorButPanel */
	protected JecnPanel recordButPanel = new JecnPanel(530, 30);
	/** 添加 */
	protected JButton addBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("add"));

	/** 删除 */
	protected JButton deleteBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("delete"));

	/** 删除 */
	protected JButton editBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("edit"));

	// ====================相关标准
	/** 相关标准 */
	protected JLabel processOrderLab = new JLabel();

	/** 标准滚动面板 */
	protected GuideJScrollPane orderScrollPane = new GuideJScrollPane();

	/** 标准表 */
	protected OrderTable orderTable = null;
	/** 错误提示 */
	protected JLabel errorLab = new JLabel();

	/** 确定 */
	protected JButton okBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("confirm"));
	/** 取消 */
	protected JButton cancelBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancel"));

	/** 设置大小 */
	protected Dimension dimension = null;

	/** 制度选择按钮 */
	protected JButton selectRuleBut;
	/** 标准选择按钮 */
	protected JButton selectOrderBut;

	/** 获取活动及关联项的数据 */
	protected JecnFlowOperationBean flowOperationBean = JecnFlowOperationDataUnit.getJecnFlowOperationBean();

	/** 所有的活动对象 */
	protected List<JecnActivityShowBean> listAllActivityShow = flowOperationBean.getListAllActivityShow();

	/** 流程关键评测指标 更改按钮 */
	protected JButton changeBut;

	protected List<DrawOperationDescriptionConfigBean> operationConfigList = new ArrayList<DrawOperationDescriptionConfigBean>();

	/** 相关文件Lab */
	protected JLabel relatedFileLab = new JLabel();

	/** 相关文件Field */
	protected JecnTextArea relatedFileArea = new JecnTextArea();

	/** 相关文件错误提示框 */
	protected JLabel relatedFileErrorLab = new JLabel();

	/** 自定义一Lab */
	protected JLabel customOneLab = new JLabel();

	/** 自定义一Field */
	protected JecnTextArea customOneArea = new JecnTextArea();

	/** 自定义一错误提示框 */
	protected JLabel customOneErrorLab = new JLabel();

	/** 自定义二Lab */
	protected JLabel customTwoLab = new JLabel();

	/** 自定义二Field */
	protected JecnTextArea customTwoArea = new JecnTextArea();

	/** 自定义二错误提示框 */
	protected JLabel customTwoErrorLab = new JLabel();

	/** 自定义三Lab */
	protected JLabel customThreeLab = new JLabel();

	/** 自定义三Field */
	protected JecnTextArea customThreeArea = new JecnTextArea();

	/** 自定义三错误提示框 */
	protected JLabel customThreeErrorLab = new JLabel();

	/** 自定义四Lab */
	protected JLabel customFourLab = new JLabel();

	/** 自定义四Field */
	protected JecnTextArea customFourArea = new JecnTextArea();

	/** 自定义四错误提示框 */
	protected JLabel customFourErrorLab = new JLabel();

	/** 自定义五Lab */
	protected JLabel customFiveLab = new JLabel();

	/** 自定义五Field */
	protected JecnTextArea customFiveArea = new JecnTextArea();

	/** 自定义五错误提示框 */
	protected JLabel customFiveErrorLab = new JLabel();
	/*** 输入和输入Panel */
	protected JecnPanel inOutPanel = new JecnPanel();
	/** 输入和输出Lab */
	protected JLabel inOutLab = new JLabel();
	/** 输入和输出错误提示 */
	protected JLabel inOutErrorLab = new JLabel();
	// *************************流程边界***************************************
	/*** 流程边界Panel */
	protected JecnPanel flowActivePanel = new JecnPanel();
	/** 流程边界Lab */
	protected JLabel flowActiveLab = new JLabel();
	/** 流程边界错误提示 */
	protected JLabel flowActiveErrorLab = new JLabel();
	/** 起始活动Lab */
	protected JLabel startActiveLab = new JLabel();

	/** 起始活动Field */
	protected JecnTextArea startActiveField = new JecnTextArea();

	/** 终止活动Lab */
	protected JLabel endActiveLab = new JLabel();

	/** 终止活动Field */
	protected JecnTextArea endActiveField = new JecnTextArea();

	// *********************流程边界*******************************************

	// *************************流程责任属性***************************************
	/*** 流程责任属性Panel */
	protected JecnPanel flowPropertyPanel = new JecnPanel();
	/** 流程责任属性Lab */
	protected JLabel flowPropertyLab = new JLabel();
	/** 流程责任属性按钮Button */
	protected JButton flowPropertyBut;
	/** 流程责任属性错误提示 */
	protected JLabel flowPropertyErrorLab = new JLabel();
	/** 流程监护人 */
	protected JLabel guardianPeopleLab = new JLabel();
	/** 流程监护人Field */
	protected JSearchTextField guardianField = new JSearchTextField(300, 20);

	/** 流程责任人Lab */
	private JLabel mapAttchMentLab = new JLabel();
	/** 流程责任人Field */
	protected JSearchTextField attchMentField = new JSearchTextField(300, 20);

	/** 流程拟制人Lab */
	protected JLabel fictionPeopleLab = new JLabel();
	/** 流程拟制人Field */
	protected JSearchTextField fictionPeopleField = new JSearchTextField(300, 20);

	/** 责任部门Lab */
	private JLabel departLab = new JLabel();
	/** 责任部门Field */
	protected JSearchTextField departField = new JSearchTextField(300, 20);

	// *********************流程责任属性*******************************************
	// *********************流程文控信息*******************************************
	/** 流程文控信息Lab */
	protected JLabel flowControlLab = new JLabel();

	/** 流程文控信息Table */
	protected JTable flowControlTable = null;

	/** 流程文控信息******************JScrollPane */
	protected GuideJScrollPane flowControlScrollPane = new GuideJScrollPane();

	// *********************流程文控信息*******************************************

	/** 输入是否正确 */
	protected boolean isPurposeSave = true;
	protected boolean isApplicationScopeSave = true;
	// protected boolean isDefinedTermSave = true;
	protected boolean isOrderSave = true;
	protected boolean isCount = true;
	protected boolean isInSave = true;
	protected boolean isOutSave = true;
	protected boolean isClientSave = true;
	protected boolean isNotAplicationScopeSave = true;
	protected boolean isOutlineSave = true;
	protected boolean isSupplementSave = true;
	protected boolean isRealtedFileSave = true;
	protected boolean isCustomOneSave = true;
	protected boolean isCustomTwoSave = true;
	protected boolean isCustomThreeSave = true;
	protected boolean isCustomFourSave = true;
	protected boolean isCustomFiveSave = true;
	protected boolean isInOutSave = true;
	protected boolean isStartEndActive = true;

	/** 下载 */
	protected JButton downLoad = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("downloadOpDes"));

	public ProcessOperationIntrusDialog() {
		// 初始化界面
		initializeInterface();
		initLayoutTable();
		// 界面添加到面板上
		initLayout();
		// 初始化数据
		initializeData();
		// 初始化监听
		initializeMonitor();
	}

	public ProcessOperationIntrusDialog(Long flowId) {
		// 初始化界面
		initializeInterface();
		initLayoutTable();
		// 界面添加到面板上
		initLayout();
		// 初始化监听
		initializeMonitor();
	}

	// 初始化界面
	protected void initializeInterface() {
		this.setSize(680, 700);
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("processOperatingInstructions"));
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setResizable(true);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);
		// 制度选择按钮初始化
		selectRuleBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("select"));
		selectRuleBut.addActionListener(this);
		selectRuleBut.setName("rule");
		// 标准选择按钮初始化
		selectOrderBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("select"));
		selectOrderBut.addActionListener(this);
		selectOrderBut.setName("order");

		// 流程关键评测指标 更改按钮
		changeBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("change"));
		changeBut.addActionListener(this);
		changeBut.setName("change");

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		inOutPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		flowActivePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 把主面板放在带滚动条的ScrollPane上
		resultScrollPane.setVerticalScrollBarPolicy(GuideJScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		purposeErrorLab.setForeground(Color.red);

		relatedFileErrorLab.setForeground(Color.red);
		customOneErrorLab.setForeground(Color.red);
		customTwoErrorLab.setForeground(Color.red);
		customThreeErrorLab.setForeground(Color.red);
		customFourErrorLab.setForeground(Color.red);
		customFiveErrorLab.setForeground(Color.red);

		applicationScopeErrorLab.setForeground(Color.red);

		definedTermErrorLab.setForeground(Color.red);

		// 驱动规则
		driveRuleScrollPane.setBorder(null);
		driveRuleScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		driveRuleScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 驱动规则
		orderErrorLab.setForeground(Color.red);

		// 输入
		inErrorLab.setForeground(Color.red);

		// 输出
		outErrorLab.setForeground(Color.red);
		inOutErrorLab.setForeground(Color.red);
		// 流程责任属性
		flowActiveErrorLab.setForeground(Color.red);
		// 客户
		clientErrorLab.setForeground(Color.red);

		// 不适用范围
		notAplicationScopeErrorLab.setForeground(Color.red);
		// 概述
		outlineErrorLab.setForeground(Color.red);
		// 补充说明
		supplementErrorLab.setForeground(Color.red);

		dimension = new Dimension(540, 50);
		purposeArea.setRows(4);
		applicationScopeArea.setRows(4);
		inField.setRows(4);
		outField.setRows(4);
		definedTermArea.setRows(4);
		notAplicationScopeArea.setRows(4);
		outlineArea.setRows(4);
		flowFileArea.setRows(4);
		supplementArea.setRows(4);
		practicesArea.setRows(4);
		relatedFlowArea.setRows(4);
		relatedRuleArea.setRows(4);
		keyEvaluationIndiArea.setRows(4);
		relatedFileArea.setRows(4);
		customOneArea.setRows(4);
		customTwoArea.setRows(4);
		customThreeArea.setRows(4);
		customFourArea.setRows(4);
		customFiveArea.setRows(4);
		driveRuleArea.setRows(4);

		dimension = new Dimension(580, 610);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.getVerticalScrollBar().setUnitIncrement(10);

		dimension = new Dimension(450, 20);
		driveTypeCombox.setPreferredSize(dimension);
		driveTypeCombox.setMaximumSize(dimension);
		driveTypeCombox.setMinimumSize(dimension);

		// 类型选择
		dimension = new Dimension(100, 20);
		typeChangeCombox.setPreferredSize(dimension);
		typeChangeCombox.setMaximumSize(dimension);
		typeChangeCombox.setMinimumSize(dimension);

		dimension = new Dimension(150, 10);
		emptyChange.setPreferredSize(dimension);
		emptyChange.setMaximumSize(dimension);
		emptyChange.setMinimumSize(dimension);

		emptyStart.setPreferredSize(dimension);
		emptyStart.setMaximumSize(dimension);
		emptyStart.setMinimumSize(dimension);

		emptyEnd.setPreferredSize(dimension);
		emptyEnd.setMaximumSize(dimension);
		emptyEnd.setMinimumSize(dimension);

		errorLab.setForeground(Color.red);

		flowValueField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		clientField.setBorder(JecnUIUtil.getTootBarBorder());

		guardianField.setEditable(false);
		attchMentField.setEditable(false);
		fictionPeopleField.setEditable(false);
		departField.setEditable(false);
		guardianField.setOpaque(false);
		attchMentField.setOpaque(false);
		fictionPeopleField.setOpaque(false);
		departField.setOpaque(false);
		// 流程责任属性按钮初始化
		flowPropertyBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("select"));
		flowPropertyBut.addActionListener(this);
		flowPropertyBut.setName("flowProperty");

		// 驱动类型
		driveTypeCombox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				driveTypeComboxPerformed(e);
			}
		});

		typeChangeCombox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				typeChangePerformed(e);
			}
		});
		endYearCombox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				yearComboxPerformed(e, 1);
			}
		});
		startYearCombox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				yearComboxPerformed(e, 0);
			}
		});
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		addBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}
		});
		deleteBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}
		});
		editBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}
		});
		downLoad.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				downLoadFile();
			}

		});
	}

	protected void downLoadFile() {
	}

	protected void initLayoutTable() {
		// 活动明细
		drawActiveDescTable = new DrawActiveDescTable(flowOperationBean.getListAllActivityShow(), this);
		// 问题区域
		questionAreaTable = new QuestionAreaTable(flowOperationBean.getListPAActivityShow(), drawActiveDescTable);
		// 关键成功因素
		keySuccessTable = new KeySuccessFactorsTable(flowOperationBean.getListKSFActivityShow(), drawActiveDescTable);
		// 关键控制点
		keyContrPointTable = new KeyControlPointTable(flowOperationBean.getListKCPActivityShow(), drawActiveDescTable);
		// 角色职责
		roleResponseTable = new RoleResponseTable(flowOperationBean.getBaseRoleFigureList(), drawActiveDescTable);
		// 流程记录
		flowFileTable = new JTable();
		// 操作规范
		practicesTable = new JTable();
		// 流程关键评测指标
		processEvaluationTable = new JTable();
		// 相关风险
		riskTable = new JTable();

		// 流程记录
		flowFileTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 操作规范
		practicesTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 流程关键评测指标
		processEvaluationTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 相关风险
		riskTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 流程文控信息
		flowControlTable = new JTable();
		flowControlTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	protected void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(infoPanel, c);
		// 纵向滚动面板的组件存放面板
		infoPanel.setLayout(new GridBagLayout());
		// *************************start*****************************************************
		contentPanel.setLayout(new GridBagLayout());
		int flagNum = 0;
		int nameNum = 1;
		// 判断是否为设计器
		if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
			operationConfigList = JecnDesignerProcess.getDesignerProcess().getOperConfigList();
		} else {
			// 获取配置
			operationConfigList = OperationConfigUtil.readOperationConfigXml();
		}
		for (DrawOperationDescriptionConfigBean confiBean : operationConfigList) {
			if (confiBean.getIsShow() == 1) {
				// && !"a22".equals(confiBean.getMark())
				// && !"a23".equals(confiBean.getMark()) && !"a24"
				// .equals(confiBean.getMark())
				// 目的
				if (confiBean.getMark().equals("a1")) {
					purposeLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(purposeLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(purposeArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(purposeErrorLab, c);
				} else if (confiBean.getMark().equals("a2")) {// 适用范围
					applicationScopeLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(applicationScopeLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(applicationScopeArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(applicationScopeErrorLab, c);

				} else if (confiBean.getMark().equals("a3")) {// 术语定义
					definedTermLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(definedTermLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(definedTermArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(definedTermErrorLab, c);
				} else if (confiBean.getMark().equals("a4")) {// 流程驱动规则
					flowDriveRuleLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(flowDriveRuleLab, c);
					int countNum = ++flagNum;
					// 流程驱动规则Panel flowDrivePanel
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(flowDrivePanel, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					flowDrivePanel.setLayout(new GridBagLayout());
					flowDrivePanel.setBorder(BorderFactory.createTitledBorder(""));
					// 流程驱动类型
					driveTypeLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("drivingType"));
					c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
							insets, 0, 0);
					flowDrivePanel.add(driveTypeLab, c);
					// driveTypeLab.setBorder(BorderFactory.createTitledBorder(""));
					// 默认显示时间驱动
					driveTypeCombox.setSelectedIndex(1);
					c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					flowDrivePanel.add(driveTypeCombox, c);
					// 驱动规则
					driveRuleLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("driveRules"));
					c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
							insets, 0, 0);
					flowDrivePanel.add(driveRuleLab, c);

					c = new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
							insets, 0, 0);
					flowDrivePanel.add(driveRuleScrollPane, c);

					c = new GridBagConstraints(0, 2, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
							insets, 0, 0);
					flowDrivePanel.add(orderErrorLab, c);
					// 类型选择 typeChangePanel
					flagNum = addTimePanel(c, flagNum);

					// 结束时间**********************************************************Panel
					// endTimePanel
				} else if (confiBean.getMark().equals("a5")) {// 输入
					inLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(inLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					contentPanel.add(inField, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(inErrorLab, c);
				} else if (confiBean.getMark().equals("a6")) {// 输出
					outLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(outLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					contentPanel.add(outField, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(outErrorLab, c);
				} else if (confiBean.getMark().equals("a7")) {// 关键活动
					keyActiveLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(keyActiveLab, c);
					// 问题区域
					questionAreaLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("problemAreas"));
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(questionAreaLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(questionAreaScrollPane, c);
					questionAreaScrollPane.setViewportView(questionAreaTable);

					questionAreaScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

					// 关键成功因素
					keySuccessLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("keySuccessFactors"));
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(keySuccessLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(keySuccessScrollPane, c);
					keySuccessScrollPane.setViewportView(keySuccessTable);
					keySuccessScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

					// 关键控制点
					keyContrPointLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("keyControlPoint"));
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(keyContrPointLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(keyContrPointScrollPane, c);
					keyContrPointScrollPane.setViewportView(keyContrPointTable);
					keyContrPointScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
				} else if (confiBean.getMark().equals("a8")) {// 角色职责
					roleResponseLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(roleResponseLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(toleResponseScrollPane, c);
					toleResponseScrollPane.setViewportView(roleResponseTable);
					toleResponseScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
				} else if (confiBean.getMark().equals("a9")) {// 流程图
					flowLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(flowLab, c);
					flowValueField.setEditable(false);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					contentPanel.add(flowValueField, c);
					flowValueField.setText(JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getName());
				} else if (confiBean.getMark().equals("a10")) {// 活动说明
					activeDescLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(activeDescLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(activeDescScrollPane, c);
					activeDescScrollPane.setViewportView(drawActiveDescTable);
					activeDescScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

				} else if (confiBean.getMark().equals("a11")) {// 流程记录
					flowRecordLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(flowRecordLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					// 判断是否为设计器
					if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
						contentPanel.add(flowFileScrollPane, c);
						flowFileScrollPane.setViewportView(flowFileTable);
						flowFileScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
					} else {
						contentPanel.add(flowFileArea, c);
					}
				} else if (confiBean.getMark().equals("a12")) {// 操作规范
					practicesLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(practicesLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					// 判断是否为设计器
					if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
						contentPanel.add(practicesScrollPane, c);
						practicesScrollPane.setViewportView(practicesTable);
						practicesScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
					} else {
						contentPanel.add(practicesArea, c);
					}
				} else if (confiBean.getMark().equals("a13")) {// 相关流程
					relatedFlowLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(relatedFlowLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					// 判断是否为设计器
					if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
						contentPanel.add(relatedFlowScrollPane, c);
						relatedFlowTable = new RelatedFlowTable();
						relatedFlowScrollPane.setViewportView(relatedFlowTable);
						relatedFlowScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
					} else {
						contentPanel.add(relatedFlowArea, c);
					}
				} else if (confiBean.getMark().equals("a14")) {// 相关制度
					// 是否存在浏览端，如果不存在浏览端，隐藏：相关制度
					if (JecnDesignerProcess.getDesignerProcess().isDraw()
							|| JecnDesignerProcess.getDesignerProcess().isPubShow()) {
						relatedRuleLab.setText(nameNum + "、" + confiBean.getName());
						c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(relatedRuleLab, c);
						c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
								GridBagConstraints.BOTH, insets, 0, 0);
						// 判断是否为设计器
						if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
							contentPanel.add(relatedRuleScrollPane, c);
							realtedRuleTable = new RealtedRuleTable();
							relatedRuleScrollPane.setViewportView(realtedRuleTable);
							relatedRuleScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
							int num = flagNum - 1;
							c = new GridBagConstraints(0, num, 1, 1, 0, 0, GridBagConstraints.NORTHWEST,
									GridBagConstraints.NONE, new Insets(5, 100, 5, 5), 0, 0);
							contentPanel.add(selectRuleBut, c);
						} else {
							contentPanel.add(relatedRuleArea, c);
						}
					} else {
						continue;
					}
				} else if (confiBean.getMark().equals("a15")) {// 流程关键测评指标板
					keyEvaluationIndiLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(keyEvaluationIndiLab, c);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					// 判断是否为设计器
					if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
						contentPanel.add(keyEvaluationIndiScrollPane, c);
						keyEvaluationIndiScrollPane.setViewportView(processEvaluationTable);
						keyEvaluationIndiScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
						int num = flagNum - 1;
						c = new GridBagConstraints(0, num, 1, 1, 0, 0, GridBagConstraints.NORTHWEST,
								GridBagConstraints.NONE, new Insets(5, 150, 5, 5), 0, 0);
						contentPanel.add(changeBut, c);
					} else {
						contentPanel.add(keyEvaluationIndiArea, c);
					}
				} else if (confiBean.getMark().equals("a16")) {// 客户
					clientLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(clientLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					contentPanel.add(clientField, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(clientErrorLab, c);
				} else if (confiBean.getMark().equals("a17")) {// 不适用范围
					notAplicationScopeLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(notAplicationScopeLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(notAplicationScopeArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(notAplicationScopeErrorLab, c);
				} else if (confiBean.getMark().equals("a18")) {// 概述
					outlineLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(outlineLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(outlineArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(outlineErrorLab, c);
				} else if (confiBean.getMark().equals("a19")) {// 补充说明
					supplementLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(supplementLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(supplementArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(supplementErrorLab, c);
				} else if (confiBean.getMark().equals("a20")) {// 记录保存
					recordSaveLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(recordSaveLab, c);
					c = new GridBagConstraints(0, flagNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, new Insets(5, 100, 5, 5), 0, 0);
					contentPanel.add(recordButPanel, c);
					recordButPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
					recordButPanel.add(addBut);
					recordButPanel.add(editBut);
					recordButPanel.add(deleteBut);
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(recordSaveScrollPane, c);
					recordSaveTable = new RecordSaveTable();
					recordSaveScrollPane.setViewportView(recordSaveTable);
					recordSaveScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
				} else if (confiBean.getMark().equals("a21")) {// 相关标准
					// 是否存在浏览端，如果不存在浏览端，隐藏：相关标准
					if (JecnDesignerProcess.getDesignerProcess().isPubShow()) {
						processOrderLab.setText(nameNum + "、" + confiBean.getName());
						c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(processOrderLab, c);
						c = new GridBagConstraints(0, flagNum, 1, 1, 0, 0, GridBagConstraints.NORTHWEST,
								GridBagConstraints.NONE, new Insets(5, 100, 5, 5), 0, 0);
						contentPanel.add(selectOrderBut, c);
						c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
								GridBagConstraints.BOTH, insets, 0, 0);
						contentPanel.add(orderScrollPane, c);
						// 相关标准
						orderTable = new OrderTable(getStandardTitle());
						orderScrollPane.setViewportView(orderTable);
						orderScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
					} else {
						continue;
					}
				} else if (confiBean.getMark().equals("a25")) {// 相关风险
					// 是否存在浏览端，如果不存在浏览端，隐藏：相关制度
					if (JecnDesignerProcess.getDesignerProcess().isPubShow()) {
						riskLab.setText(nameNum + "、" + confiBean.getName());
						c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(riskLab, c);

						c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
								GridBagConstraints.BOTH, insets, 0, 0);
						// 判断是否为设计器
						if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
							contentPanel.add(riskScrollPane, c);
							riskScrollPane.setViewportView(riskTable);
							riskScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
						} else {
							contentPanel.add(riskPane, c);
						}
					} else {
						continue;
					}
				} else if (confiBean.getMark().equals("a26")) {// 相关文件
					relatedFileLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(relatedFileLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(relatedFileArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(relatedFileErrorLab, c);
				} else if (confiBean.getMark().equals("a27")) {// 自定义要素1
					customOneLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customOneLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(customOneArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customOneErrorLab, c);
				} else if (confiBean.getMark().equals("a28")) {// 自定义要素2
					customTwoLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customTwoLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(customTwoArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customTwoErrorLab, c);
				} else if (confiBean.getMark().equals("a29")) {// 自定义要素3
					customThreeLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customThreeLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(customThreeArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customThreeErrorLab, c);
				} else if (confiBean.getMark().equals("a30")) {// 自定义要素4
					customFourLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customFourLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(customFourArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customFourErrorLab, c);
				} else if (confiBean.getMark().equals("a31")) {// 自定义要素5
					customFiveLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customFiveLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(customFiveArea, c);
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(customFiveErrorLab, c);
				} else if (confiBean.getMark().equals("a32")) {// 输入和输出
					inOutLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(inOutLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					contentPanel.add(inOutPanel, c);
					inOutPanel.setBorder(BorderFactory.createTitledBorder(""));
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(inOutErrorLab, c);
					// 输入Lab
					inLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("inLabC"));
					c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					inOutPanel.add(inLab, c);
					// driveTypeLab.setBorder(BorderFactory.createTitledBorder(""));
					// 输入Field
					c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					inOutPanel.add(inField, c);
					// 输出Lab
					outLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("outLabC"));
					c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					inOutPanel.add(outLab, c);
					// 输出Filed
					c = new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
							insets, 0, 0);
					inOutPanel.add(outField, c);
				} else if (confiBean.getMark().equals("a22")) {// 流程边界
					flowActiveLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(flowActiveLab, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					contentPanel.add(flowActivePanel, c);
					flowActivePanel.setBorder(BorderFactory.createTitledBorder(""));
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(flowActiveErrorLab, c);
					// 起始活动Lab
					startActiveLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("startActiveC"));//
					c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					flowActivePanel.add(startActiveLab, c);
					// driveTypeLab.setBorder(BorderFactory.createTitledBorder(""));
					// 起始活动Field
					c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					flowActivePanel.add(startActiveField, c);
					// 终止活动Lab
					endActiveLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("endActiveC"));
					c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					flowActivePanel.add(endActiveLab, c);
					// 终止活动Filed
					c = new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
							insets, 0, 0);
					flowActivePanel.add(endActiveField, c);
				} else if (confiBean.getMark().equals("a23")) {// 流程责任属性
					flowPropertyLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(flowPropertyLab, c);
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, new Insets(5, 100, 5, 5), 0, 0);
					contentPanel.add(flowPropertyBut, c);
					int countNum = ++flagNum;
					c = new GridBagConstraints(0, countNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					contentPanel.add(flowPropertyPanel, c);

					flowPropertyPanel.setBorder(BorderFactory.createTitledBorder(""));
					if (confiBean.getIsEmpty() == 1) {
						JLabel isEmptyLab = new JLabel("*");
						isEmptyLab.setForeground(Color.red);
						c = new GridBagConstraints(1, countNum, 1, 1, 0.0, 0.0, GridBagConstraints.FIRST_LINE_START,
								GridBagConstraints.NONE, insets, 0, 0);
						contentPanel.add(isEmptyLab, c);
					}

					// 流程监护人Lab
					guardianPeopleLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("guardianPeopleC"));
					c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					flowPropertyPanel.add(guardianPeopleLab, c);
					// driveTypeLab.setBorder(BorderFactory.createTitledBorder(""));
					// 流程监护人Field
					c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					flowPropertyPanel.add(guardianField, c);
					// 流程责任人Lab
					mapAttchMentLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("mapAttchMentC"));
					c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					flowPropertyPanel.add(mapAttchMentLab, c);
					// 流程责任人Filed
					c = new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
							insets, 0, 0);
					flowPropertyPanel.add(attchMentField, c);

					// 流程拟制人Lab
					fictionPeopleLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("fictionPeopleC"));
					c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					flowPropertyPanel.add(fictionPeopleLab, c);
					// 流程拟制人Filed
					c = new GridBagConstraints(1, 2, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
							insets, 0, 0);
					flowPropertyPanel.add(fictionPeopleField, c);

					// 流程责任部门Lab
					departLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("departC"));
					c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					flowPropertyPanel.add(departLab, c);
					// 流程责任部门Filed
					c = new GridBagConstraints(1, 3, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
							insets, 0, 0);
					flowPropertyPanel.add(departField, c);
				} else if (confiBean.getMark().equals("a24")) {// 流程文控信息
					flowControlLab.setText(nameNum + "、" + confiBean.getName());
					c = new GridBagConstraints(0, flagNum, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
							GridBagConstraints.NONE, insets, 0, 0);
					contentPanel.add(flowControlLab, c);

					flowControlScrollPane.setViewportView(flowControlTable);
					flowControlScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
					c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
							GridBagConstraints.BOTH, insets, 0, 0);
					contentPanel.add(flowControlScrollPane, c);
				}

				flagNum++;
				nameNum++;
			}
		}

		// *************************end*****************************************************
		// 纵向滚动面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(resultScrollPane, c);

		resultScrollPane.setViewportView(contentPanel);

		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		// 添加按钮面板
		addButtonPanel();
		this.getContentPane().add(mainPanel);
	}

	protected void addButtonPanel() {
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(errorLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
	}

	protected void addRowToPanel(JecnPanel panel, JComponent leftLabel, int colsp) {
		GridBagConstraints c = new GridBagConstraints(colsp, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0);
		panel.add(leftLabel, c);
	}

	/**
	 * 驱动类型选择
	 */
	protected void driveTypeComboxPerformed(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {
			if (driveTypeCombox.getSelectedIndex() == 0 || driveTypeCombox.getSelectedIndex() == 2) {
				typeChangeCombox.setEnabled(false);
				frequencyField.setEditable(false);
				// typeChangeCombox.setSelectedIndex(0);
			}
			// else if(driveTypeCombox.getSelectedIndex() == 2){
			// typeChangeCombox.setEnabled(false);
			// frequencyField.setEditable(false);
			// typeChangeCombox.setSelectedIndex(2);}
			else {
				typeChangeCombox.setEnabled(true);
				frequencyField.setEditable(true);
			}
		}
	}

	/**
	 * 时间 类型选择
	 */
	protected void typeChangePerformed(ItemEvent e) {

		if (e.getStateChange() == e.SELECTED) {
			// 数据库类型
			String getItemSel = typeChangeCombox.getSelectedItem().toString();
			// 日
			startDayLab.setVisible(false);
			startHourCombox.setVisible(false);
			startMinuteCombox.setVisible(false);
			startSecondCombox.setVisible(false);

			endDayLab.setVisible(false);
			endHourCombox.setVisible(false);
			endMinuteCombox.setVisible(false);
			endSecondCombox.setVisible(false);

			// 周
			startWeekLab.setVisible(false);
			startWeekCombox.setVisible(false);
			endWeekLab.setVisible(false);
			endWeekCombox.setVisible(false);
			// 月
			startMonthLab.setVisible(false);
			startMonthCombox.setVisible(false);
			endMonthLab.setVisible(false);
			endMonthCombox.setVisible(false);
			// 季
			startSeasonLab.setVisible(false);
			startSeasonCombox.setVisible(false);
			endSeasonLab.setVisible(false);
			endSeanCombox.setVisible(false);
			// 年
			startYearLab.setVisible(false);
			startYearCombox.setVisible(false);
			startYearDayCombox.setVisible(false);
			endYearLab.setVisible(false);
			endYearCombox.setVisible(false);
			endYearDayCombox.setVisible(false);
			// 单位名称
			// 日
			startHourUnitLab.setVisible(false);
			startMinuteUnitLab.setVisible(false);
			startSecondUnitLab.setVisible(false);

			endHourUnitLab.setVisible(false);
			endMinuteUnitLab.setVisible(false);
			endSecondUnitLab.setVisible(false);
			// 月
			startMonthDayUnitLab.setVisible(false);
			endMonthDayUnitLab.setVisible(false);
			// 季
			startSeaDayUnitLab.setVisible(false);
			endSeaDayUnitLab.setVisible(false);
			// 年
			startYearMonthUnitLab.setVisible(false);
			startYearDayUnitLab.setVisible(false);
			endYearMonthUnitLab.setVisible(false);
			endYearDayUnitLab.setVisible(false);

			String getQua = null;
			String[] strStarts = null;
			String[] strEnds = null;
			if (typeChangeCombox.getItemAt(1).equals(getItemSel)) {// 日
				// 日类型
				startDayLab.setVisible(true);
				startHourCombox.setVisible(true);
				startMinuteCombox.setVisible(true);
				startSecondCombox.setVisible(true);

				endDayLab.setVisible(true);
				endHourCombox.setVisible(true);
				endMinuteCombox.setVisible(true);
				endSecondCombox.setVisible(true);

				startHourUnitLab.setVisible(true);
				startMinuteUnitLab.setVisible(true);
				startSecondUnitLab.setVisible(true);

				endHourUnitLab.setVisible(true);
				endMinuteUnitLab.setVisible(true);
				endSecondUnitLab.setVisible(true);

				// 赋值 jecnFlowDriverT

				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(1).equals(getQua)) {
						int intStartHour = 0;
						int intEndHour = 0;
						int intStartMinute = 0;
						int intEndMinute = 0;
						int intStartSecond = 0;
						int intEndSecond = 0;
						// 时
						// 时
						intStartHour = Integer.parseInt(strStarts[0].toString());
						intEndHour = Integer.parseInt(strEnds[0].toString());
						// 分
						intStartMinute = Integer.parseInt(strStarts[1].toString());
						intEndMinute = Integer.parseInt(strEnds[1].toString());
						// 秒
						intStartSecond = Integer.parseInt(strStarts[2].toString());
						intEndSecond = Integer.parseInt(strEnds[2].toString());

						startHourCombox.setSelectedIndex(intStartHour);
						startMinuteCombox.setSelectedIndex(intStartMinute);
						startSecondCombox.setSelectedIndex(intStartSecond);

						endHourCombox.setSelectedIndex(intEndHour);
						endMinuteCombox.setSelectedIndex(intEndMinute);
						endSecondCombox.setSelectedIndex(intEndSecond);

					}
				}
				addRowToPanel(startTimePanel, startDayLab, 0);
				addRowToPanel(startTimePanel, startHourCombox, 1);
				// 时 单位
				addRowToPanel(startTimePanel, startHourUnitLab, 2);
				addRowToPanel(startTimePanel, startMinuteCombox, 3);
				// 分 单位endHourUnitLab
				addRowToPanel(startTimePanel, startMinuteUnitLab, 4);
				addRowToPanel(startTimePanel, startSecondCombox, 5);
				// 秒 单位endHourUnitLab
				addRowToPanel(startTimePanel, startSecondUnitLab, 6);

				addRowToPanel(endTimePanel, endDayLab, 0);
				addRowToPanel(endTimePanel, endHourCombox, 1);
				// 时 单位endHourUnitLab
				addRowToPanel(endTimePanel, endHourUnitLab, 2);
				addRowToPanel(endTimePanel, endMinuteCombox, 3);
				// 分 单位endHourUnitLab
				addRowToPanel(endTimePanel, endMinuteUnitLab, 4);
				addRowToPanel(endTimePanel, endSecondCombox, 5);
				// 秒 单位endHourUnitLab
				addRowToPanel(endTimePanel, endSecondUnitLab, 6);

			} else if (typeChangeCombox.getItemAt(2).equals(getItemSel)) {// 周

				// 周类型 startWeekLab
				startWeekLab.setVisible(true);
				startWeekCombox.setVisible(true);
				endWeekLab.setVisible(true);
				endWeekCombox.setVisible(true);
				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(2).equals(getQua)) {

						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						// 开始周
						startWeekCombox.setSelectedIndex(intStart);
						// 结束周
						endWeekCombox.setSelectedIndex(intEnd);
					}
				}
				addRowToPanel(startTimePanel, startWeekLab, 0);
				addRowToPanel(startTimePanel, startWeekCombox, 1);
				// 结束
				addRowToPanel(endTimePanel, endWeekLab, 0);
				addRowToPanel(endTimePanel, endWeekCombox, 1);
			} else if (typeChangeCombox.getItemAt(3).equals(getItemSel)) {// 月
				// 月类型 startMonthLab
				startMonthLab.setVisible(true);
				startMonthCombox.setVisible(true);
				endMonthLab.setVisible(true);
				endMonthCombox.setVisible(true);
				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(3).equals(getQua)) {// 月
						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						startMonthCombox.setSelectedIndex(intStart);
						endMonthCombox.setSelectedIndex(intEnd);
					}
				}

				startMonthDayUnitLab.setVisible(true);
				endMonthDayUnitLab.setVisible(true);

				addRowToPanel(startTimePanel, startMonthLab, 0);
				addRowToPanel(startTimePanel, startMonthCombox, 1);
				addRowToPanel(startTimePanel, startMonthDayUnitLab, 2);
				addRowToPanel(endTimePanel, endMonthLab, 0);
				addRowToPanel(endTimePanel, endMonthCombox, 1);
				addRowToPanel(endTimePanel, endMonthDayUnitLab, 2);
			} else if (typeChangeCombox.getItemAt(4).equals(getItemSel)) {// 季度
				// 季类型
				startSeasonLab.setVisible(true);
				startSeasonCombox.setVisible(true);
				endSeasonLab.setVisible(true);
				endSeanCombox.setVisible(true);
				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(4).equals(getQua)) {
						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						startSeasonCombox.setSelectedIndex(intStart);
						endSeanCombox.setSelectedIndex(intEnd);
					}
				}
				startSeaDayUnitLab.setVisible(true);
				endSeaDayUnitLab.setVisible(true);

				addRowToPanel(startTimePanel, startSeasonLab, 0);
				addRowToPanel(startTimePanel, startSeasonCombox, 1);
				addRowToPanel(startTimePanel, startSeaDayUnitLab, 2);
				addRowToPanel(endTimePanel, endSeasonLab, 0);
				addRowToPanel(endTimePanel, endSeanCombox, 1);
				addRowToPanel(endTimePanel, endSeaDayUnitLab, 2);
			} else if (typeChangeCombox.getItemAt(5).equals(getItemSel)) {// 年
				// 年类型
				startYearLab.setVisible(true);
				startYearCombox.setVisible(true);
				startYearDayCombox.setVisible(true);
				endYearLab.setVisible(true);
				endYearCombox.setVisible(true);
				endYearDayCombox.setVisible(true);

				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(5).equals(getQua)) {
						int intStartMonth = 0;
						int intEndMonth = 0;
						int intStartDay = 0;
						int intEndDay = 0;
						// 开始月
						intStartMonth = Integer.parseInt(strStarts[0]);
						// 结束月
						intEndMonth = Integer.parseInt(strEnds[0]);
						// 开始天
						intStartDay = Integer.parseInt(strStarts[1]);
						// 结束天
						intEndDay = Integer.parseInt(strEnds[1]);
						startYearCombox.setSelectedIndex(intStartMonth);
						startYearDayCombox.setSelectedIndex(intStartDay);

						endYearCombox.setSelectedIndex(intEndMonth);
						endYearDayCombox.setSelectedIndex(intEndDay);
					}
				}

				startYearMonthUnitLab.setVisible(true);
				startYearDayUnitLab.setVisible(true);
				endYearMonthUnitLab.setVisible(true);
				endYearDayUnitLab.setVisible(true);

				addRowToPanel(startTimePanel, startYearLab, 0);
				addRowToPanel(startTimePanel, startYearCombox, 1);
				addRowToPanel(startTimePanel, startYearMonthUnitLab, 2);
				addRowToPanel(startTimePanel, startYearDayCombox, 3);
				addRowToPanel(startTimePanel, startYearDayUnitLab, 4);
				addRowToPanel(endTimePanel, endYearLab, 0);
				addRowToPanel(endTimePanel, endYearCombox, 1);
				addRowToPanel(endTimePanel, endYearMonthUnitLab, 2);
				addRowToPanel(endTimePanel, endYearDayCombox, 3);
				addRowToPanel(endTimePanel, endYearDayUnitLab, 4);
			}
			startTimePanel.validate();
			startTimePanel.repaint();
			endTimePanel.validate();
			endTimePanel.repaint();
		}
	}

	/***
	 * 年类型：切换月份时，对应日类型数据显示
	 * 
	 * @param e
	 * @param isStart
	 *            ：0：开始月份；1：结束月份
	 */
	protected void yearComboxPerformed(ItemEvent e, int isStart) {
		if (e.getStateChange() == e.SELECTED) {
			returnStrNumYear(1, 31, isStart);
		}
	}

	/**
	 * 初始化数据
	 * 
	 * @author fuzhh Oct 17, 2012
	 */
	protected void initializeData() {
		// 获取面板
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		JecnFlowOperationBean flowOperationData = drawDesktopPane.getFlowMapData().getFlowOperationBean();
		// 目的
		purposeArea.setText(flowOperationData.getFlowPurpose());
		// 适用范围
		applicationScopeArea.setText(flowOperationData.getApplicability());
		// 术语定义
		definedTermArea.setText(flowOperationData.getNoutGlossary());
		// 输入
		inField.setText(flowOperationData.getFlowInput());
		// 输出
		outField.setText(flowOperationData.getFlowOutput());
		// 流程记录
		flowFileArea.setText(flowOperationData.getFlowFileStr());
		// 操作规范
		practicesArea.setText(flowOperationData.getPracticesStr());
		// 相关流程
		relatedFlowArea.setText(flowOperationData.getRelatedFlowStr());
		// 相关制度
		relatedRuleArea.setText(flowOperationData.getRelatedRuleStr());
		// 流程关键测评指标
		keyEvaluationIndiArea.setText(flowOperationData.getKeyEvaluationIndiStr());
		// 客户
		clientField.setText(flowOperationData.getFlowCustom());
		// 不适用范围
		notAplicationScopeArea.setText(flowOperationData.getNoApplicability());
		// 概述
		outlineArea.setText(flowOperationData.getFlowSummarize());
		// 补充说明
		supplementArea.setText(flowOperationData.getFlowSupplement());
		// 相关文件
		relatedFileArea.setText(flowOperationData.getFlowRelatedFile());
		// 自定义一
		customOneArea.setText(flowOperationData.getCustomOne());
		// 自定义二
		customTwoArea.setText(flowOperationData.getCustomTwo());
		// 自定义三
		customThreeArea.setText(flowOperationData.getCustomThree());
		// 自定义四
		customFourArea.setText(flowOperationData.getCustomFour());
		// 自定义五
		customFiveArea.setText(flowOperationData.getCustomFive());
	}

	/**
	 * 初始化监听
	 * 
	 * @author fuzhh Oct 19, 2012
	 */
	protected void initializeMonitor() {
		// 目的
		purposeArea.addCaretListener(this);
		// 适用范围
		applicationScopeArea.addCaretListener(this);
		// 术语定义
		definedTermArea.addCaretListener(this);
		// 输入
		inField.addCaretListener(this);
		// 输出
		outField.addCaretListener(this);
		// 客户
		clientField.addCaretListener(this);
		// 不适用范围
		notAplicationScopeArea.addCaretListener(this);
		// 概述
		outlineArea.addCaretListener(this);
		// 补充说明
		supplementArea.addCaretListener(this);
		// 驱动规则
		driveRuleArea.addCaretListener(this);
		// 驱动次数
		frequencyField.addCaretListener(this);
		// 相关文件
		relatedFileArea.addCaretListener(this);
		// 自定义一
		customOneArea.addCaretListener(this);
		// 自定义二
		customTwoArea.addCaretListener(this);
		// 自定义三
		customThreeArea.addCaretListener(this);
		// 自定义四
		customFourArea.addCaretListener(this);
		// 自定义五
		customFiveArea.addCaretListener(this);
		// 流程边界--起始活动
		startActiveField.addCaretListener(this);
		// 流程边界--终止活动
		endActiveField.addCaretListener(this);
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		// 目的
		isPurposeSave = OperationConfigUtil.validateContent(purposeArea.getText(), purposeErrorLab);
		// 适用范围
		isApplicationScopeSave = OperationConfigUtil.validateContent(applicationScopeArea.getText(),
				applicationScopeErrorLab);
		// 术语定义
		// isDefinedTermSave =
		// OperationConfigUtil.validateContent(definedTermArea
		// .getText(), definedTermErrorLab);
		// 驱动规则
		isOrderSave = OperationConfigUtil.validateContent(driveRuleArea.getText(), orderErrorLab);
		// 驱动次数
		isCount = OperationConfigUtil.checkCountAndNum(frequencyField.getText(), orderErrorLab);
		// 输入
		isInSave = OperationConfigUtil.validateContent(inField.getText(), inErrorLab);
		// 输出
		isOutSave = OperationConfigUtil.validateContent(outField.getText(), outErrorLab);
		// 客户
		isClientSave = OperationConfigUtil.checkInOutContent(clientField.getText(), clientErrorLab);
		// 不适用范围
		isNotAplicationScopeSave = OperationConfigUtil.validateContent(notAplicationScopeArea.getText(),
				notAplicationScopeErrorLab);
		// 概述
		isOutlineSave = OperationConfigUtil.validateContent(outlineArea.getText(), outlineErrorLab);
		// 补充说明
		isSupplementSave = OperationConfigUtil.validateContent(supplementArea.getText(), supplementErrorLab);
		// 相关文件
		isRealtedFileSave = OperationConfigUtil.validateContent(relatedFileArea.getText(), relatedFileErrorLab);
		/****
		 * 自定义不限定长度 // 自定义一 isCustomOneSave =
		 * OperationConfigUtil.validateContent(customOneArea.getText(),
		 * customOneErrorLab); // 自定义二 isCustomTwoSave =
		 * OperationConfigUtil.validateContent(customTwoArea.getText(),
		 * customTwoErrorLab); // 自定义三 isCustomThreeSave =
		 * OperationConfigUtil.validateContent(customThreeArea.getText(),
		 * customThreeErrorLab); // 自定义四 isCustomFourSave =
		 * OperationConfigUtil.validateContent(customFourArea.getText(),
		 * customFourErrorLab); // 自定义五 isCustomFiveSave =
		 * OperationConfigUtil.validateContent(customFiveArea.getText(),
		 * customFiveErrorLab);
		 ***/

		// 输入和输出
		isInOutSave = OperationConfigUtil.validateContent(inField.getText(), inOutErrorLab);
		if (inOutErrorLab.getText() != null && !"".equals(inOutErrorLab.getText())) {
			inOutErrorLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("inLabC") + inOutErrorLab.getText());
		}
		if (isInOutSave) {
			isInOutSave = OperationConfigUtil.validateContent(outField.getText(), inOutErrorLab);
			if (inOutErrorLab.getText() != null && !"".equals(inOutErrorLab.getText())) {
				inOutErrorLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("outLabC") + inOutErrorLab.getText());
			}
		}
		// 流程边界：起始活动
		isStartEndActive = OperationConfigUtil.checkInOutContent(startActiveField.getText(), flowActiveErrorLab);
		if (flowActiveErrorLab.getText() != null && !"".equals(flowActiveErrorLab.getText())) {
			flowActiveErrorLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("startActiveC") + flowActiveErrorLab.getText());
		}
		// 流程边界：终止活动
		if (isStartEndActive) {
			isStartEndActive = OperationConfigUtil.checkInOutContent(endActiveField.getText(), flowActiveErrorLab);
			if (flowActiveErrorLab.getText() != null && !"".equals(flowActiveErrorLab.getText())) {
				flowActiveErrorLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("endActiveC") + flowActiveErrorLab.getText());
			}
		}

		// endActiveField
	}

	/***************************************************************************
	 * 确定
	 */
	protected void okButPerformed() {
		// 获取面板
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		JecnFlowOperationBean operationBean = drawDesktopPane.getFlowMapData().getFlowOperationBean();
		// 目的
		String purposeStr = purposeArea.getText();
		// 适用范围
		String applicationScopeStr = applicationScopeArea.getText();
		// 术语定义
		String definedTermStr = definedTermArea.getText();
		// 输入
		String inStr = inField.getText();
		// 输出
		String outStr = outField.getText();
		// 客户
		String clientStr = clientField.getText();
		// 不适用范围
		String notAplicationScopeStr = notAplicationScopeArea.getText();
		// 概述
		String outlineStr = outlineArea.getText();
		// 补充说明
		String supplementStr = supplementArea.getText();
		// 相关文件
		String relateFileStr = relatedFileArea.getText();
		// 自定义一
		String customOneStr = customOneArea.getText();
		// 自定义二
		String customTwoStr = customTwoArea.getText();
		// 自定义三
		String customThreeStr = customThreeArea.getText();
		// 自定义四
		String customFourStr = customFourArea.getText();
		// 自定义五
		String customFiveStr = customFiveArea.getText();

		if (isPurposeSave && isApplicationScopeSave && isInSave && isOutSave && isClientSave
				&& isNotAplicationScopeSave && isOutlineSave && isSupplementSave && isOrderSave && isCount
				&& isRealtedFileSave && isCustomOneSave && isCustomTwoSave && isCustomThreeSave && isCustomFourSave
				&& isCustomFiveSave) {
			errorLab.setText("");
			operationBean.setFlowPurpose(purposeStr);
			// 适用范围
			operationBean.setApplicability(applicationScopeStr);
			// 术语定义
			operationBean.setNoutGlossary(definedTermStr);
			// 输入
			operationBean.setFlowInput(inStr);
			// 输出
			operationBean.setFlowOutput(outStr);
			// 客户
			operationBean.setFlowCustom(clientStr);
			// 不适用范围
			operationBean.setNoApplicability(notAplicationScopeStr);
			// 概述
			operationBean.setFlowSummarize(outlineStr);
			// 补充说明
			operationBean.setFlowSupplement(supplementStr);
			// 流程记录
			String flowFile = flowFileArea.getText();
			operationBean.setFlowFileStr(flowFile);
			// 操作规范
			String practices = practicesArea.getText();
			operationBean.setPracticesStr(practices);
			// 相关流程
			String relatedFlow = relatedFlowArea.getText();
			operationBean.setRelatedFlowStr(relatedFlow);
			// 相关制度
			String relatedRule = relatedRuleArea.getText();
			operationBean.setRelatedRuleStr(relatedRule);
			// 流程评测及关键指标
			String keyEvaluationIndi = keyEvaluationIndiArea.getText();
			operationBean.setKeyEvaluationIndiStr(keyEvaluationIndi);
			// 相关文件
			operationBean.setFlowRelatedFile(relateFileStr);
			// 自定义一
			operationBean.setCustomOne(customOneStr);
			// 自定义二
			operationBean.setCustomTwo(customTwoStr);
			// 自定义三
			operationBean.setCustomThree(customThreeStr);
			// 自定义四
			operationBean.setCustomFour(customFourStr);
			// 自定义五
			operationBean.setCustomFive(customFiveStr);

			drawDesktopPane.repaint();
			this.setVisible(false);
		} else {
			errorLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("inputTheWrongDataDeterminedAfterPleaseAmend"));
		}

	}

	/**
	 * 返回数据是否修改
	 * 
	 * @author fuzhh Oct 16, 2012
	 * @param a
	 *            需要比较的条件
	 * @param b
	 *            需要比较的条件
	 * @return boolean结果
	 */
	protected boolean judgementIsUpdate(String a, String b) {
		boolean isUpdate = false;
		if ((a != null && !"".equals(a)) && (b == null && "".equals(b))) {
			isUpdate = true;
		} else if ((a == null && "".equals(a)) && (b != null && !"".equals(b))) {
			isUpdate = true;
		} else if ((a != null && !"".equals(a) && !a.equals(b)) || (b != null && !"".equals(b) && !b.equals(a))) {
			isUpdate = true;
		}
		return isUpdate;
	}

	protected String[] returnStrNum(int startNum, int endNum) {
		List<Integer> strList = new ArrayList<Integer>();
		for (int i = startNum; i <= endNum; i++) {
			strList.add(i);
		}
		Object[] objs = strList.toArray();
		String[] strs = new String[objs.length];
		for (int j = 0; j < objs.length; j++) {
			strs[j] = objs[j].toString();
		}
		return strs;
	}

	protected void returnStrNumYear(int startNum, int endNum, int isStart) {
		List<Integer> strList = new ArrayList<Integer>();
		int yearInt = 0;
		if (isStart == 0) {// 开始月份
			yearInt = startYearCombox.getSelectedIndex();
		} else if (isStart == 1) {// 结束月份
			yearInt = endYearCombox.getSelectedIndex();
		}
		if (yearInt == 0 || yearInt == 2 || yearInt == 4 || yearInt == 6 || yearInt == 7 || yearInt == 9
				|| yearInt == 11) {// 1、3、5、7、8、10、12月
			endNum = 31;
		} else if (yearInt == 3 || yearInt == 5 || yearInt == 8 || yearInt == 10) {// 4、6、9、11月
			endNum = 30;
		} else if (yearInt == 1) {// 2月
			endNum = 29;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			int date = Integer.parseInt(sdf.format(new Date()));
			if ((date % 4 == 0 && date % 100 != 0) || date % 400 == 0) {// 闰年
				endNum = 29;
			} else {
				endNum = 28;
			}
		}
		for (int i = startNum; i <= endNum; i++) {
			strList.add(i);
		}
		Object[] objs = strList.toArray();
		String[] strs = new String[objs.length];
		for (int j = 0; j < objs.length; j++) {
			// strs[j] = objs[j].toString();
			if (isStart == 0) {// 开始月份
				startYearDayCombox.addItem(objs[j].toString());
			} else if (isStart == 1) {// 结束月份
				endYearDayCombox.addItem(objs[j].toString());
			}
		}
		// return strs;
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
		this.setVisible(false);
	}

	/***************************************************************************
	 * 添加
	 */
	protected void addButPerformed() {

	}

	/***************************************************************************
	 * 删除
	 */
	protected void deleteButPerformed() {

	}

	/**
	 * 编辑
	 * 
	 * @author fuzhh Nov 5, 2012
	 */
	protected void editButPerformed() {

	}

	/**
	 * 流程文件：时间驱动，时间类型和时间选择项
	 * 
	 * @return int
	 * @author cheshaowei
	 * @date 2015-11-3 下午03:23:04
	 */
	protected int addTimePanel(GridBagConstraints c, int flagNum) {
		c = new GridBagConstraints(0, 2, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetp, 0,
				0);
		flowDrivePanel.add(typeChangePanel, c);
		typeChangePanel.setLayout(new GridBagLayout());
		typeChangePanel.setBorder(BorderFactory.createTitledBorder(""));
		// typeChangeLab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(typeChangeLab, c);
		// typeChangeCombox
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(typeChangeCombox, c);

		// emptyChange
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(emptyChange, c);

		// 频率 frequencyLab
		frequencyLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("frequency"));
		orderLab.setText(JecnResourceUtil.getJecnResourceUtil().getValue("number"));
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(frequencyLab, c);
		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);

		typeChangePanel.add(frequencyField, c);
		c = new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(orderLab, c);
		// 时间Panel timePanel
		// 开始时间***************************************************Panel
		// startTimePanel
		c = new GridBagConstraints(0, 3, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetp, 0,
				0);
		flowDrivePanel.add(timePanel, c);
		timePanel.setLayout(new GridBagLayout());

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insetp, 0, 0);
		timePanel.add(startTimePanel, c);
		startTimePanel.setLayout(new GridBagLayout());
		startTimePanel.setBorder(BorderFactory.createTitledBorder(JecnResourceUtil.getJecnResourceUtil().getValue("startTime")));
		// 结束时间**********************************************************Panel
		// endTimePanel
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insetp, 0, 0);
		timePanel.add(endTimePanel, c);
		endTimePanel.setLayout(new GridBagLayout());
		endTimePanel.setBorder(BorderFactory.createTitledBorder(JecnResourceUtil.getJecnResourceUtil().getValue("endTime")));
		c = new GridBagConstraints(0, ++flagNum, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		contentPanel.add(orderErrorLab, c);
		return flagNum;
	}

	protected String getStandardTitle() {
		return "标准名称";
	}

	class GuideJScrollPane extends JScrollPane {
		public GuideJScrollPane() {
			/** 设置大小 */
			Dimension dmion = new Dimension(540, 100);
			this.setPreferredSize(dmion);
			this.setMaximumSize(dmion);
			this.setMinimumSize(dmion);
		}
	}

	public ActiveDescTable getActiveDescTable() {
		return activeDescTable;
	}

	public JTable getQuestionAreaTable() {
		return questionAreaTable;
	}

	public JTable getKeySuccessTable() {
		return keySuccessTable;
	}

	public JTable getKeyContrPointTable() {
		return keyContrPointTable;
	}

	public JTable getDrawActiveDescTable() {
		return drawActiveDescTable;
	}

	public JTable getRoleResponseTable() {
		return roleResponseTable;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	public JTable getProcessEvaluationTable() {
		return processEvaluationTable;
	}

	public JTable getFlowFileTable() {
		return flowFileTable;
	}

	public JTable getPracticesTable() {
		return practicesTable;
	}

	public void setPracticesTable(JTable practicesTable) {
		this.practicesTable = practicesTable;
	}

	public void setFlowFileTable(JTable flowFileTable) {
		this.flowFileTable = flowFileTable;
	}
}
