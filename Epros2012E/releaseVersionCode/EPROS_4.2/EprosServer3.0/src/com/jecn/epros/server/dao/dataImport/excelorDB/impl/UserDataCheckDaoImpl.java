package com.jecn.epros.server.dao.dataImport.excelorDB.impl;

import java.util.List;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.dataImport.excelorDB.IUserDataCheckDao;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

/**
 * 人员同步数据行间校验DAO
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：May 30, 2013 时间：3:40:11 PM
 */
public class UserDataCheckDaoImpl extends AbsBaseDao<DeptBean, Long> implements
		IUserDataCheckDao {
	/**
	 * 
	 * 人员数据行间校验，是否存在重复数据
	 */
	@Override
	public List<JecnTmpPosAndUserBean> userSameDataCheck() {
		String sql = "with tmp_user as("
				+ "    select tu.user_num,"
				+ "           tu.true_name,"
				+ "           tu.pos_num,"
				+ "           tu.email,"
				+ "           tu.email_type,"
				+ "           tu.phone"
				+ "      from TMP_JECN_ORI_USER tu"
				+ "     group by tu.user_num,"
				+ "              tu.true_name,"
				+ "              tu.pos_num,"
				+ "              tu.email,"
				+ "              tu.email_type,"
				+ "              tu.phone"
				+ "    having count(tu.user_num) > 1)"
				+ ""
				+ "select t.* from  TMP_JECN_ORI_USER t where t.user_num in (select user_num from tmp_user)";

		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 人员登录名称相同数据验证 登录名称相同，存在岗位编号为空
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpPosAndUserBean> loginNameSameAndPosNullCheck() {
		String sql = "  with tmp_user as (" + "   select tu.user_num"
				+ "                         from TMP_JECN_ORI_USER tu"
				+ "                        group by tu.user_num"
				+ "                       having count(tu.user_num) > 1"
				+ "   )" + "select *" + "  from TMP_JECN_ORI_USER" + ""
				+ " where user_num in (select tu.user_num"
				+ "                      from TMP_JECN_ORI_USER tu, tmp_user t"
				+ "                     where tu.user_num = t.user_num"
				+ "                       and tu.pos_num is null)";

		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 一人多岗位情况 ，真实姓名不同
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpPosAndUserBean> trueNameNoSameCheck() {
		String sql = "with tmp_user as (select tu.user_num"
				+ "                                  from TMP_JECN_ORI_USER tu"
				+ "                                 group by tu.user_num"
				+ "                                having count(tu.user_num) > 1)"
				+ " select *"
				+ "   from TMP_JECN_ORI_USER"
				+ "  where user_num in"
				+ "        (select tu.user_num"
				+ "           from TMP_JECN_ORI_USER tu"
				+ "          inner join tmp_user t on tu.user_num = t.user_num"
				+ "                               and exists"
				+ "          (select 1"
				+ "                                      from TMP_JECN_ORI_USER tu1"
				+ "                                     where tu.user_num = tu1.user_num"
				+ "                                       and tu.pos_num <> tu1.pos_num"
				+ "                                       and tu.true_name <> tu1.true_name))";

		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 一人多岗位情况 ，邮箱地址和邮箱类型不同
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpPosAndUserBean> emailNoSameCheck() {
		String sql = "with tmp_user as (" + "select tu1.*"
				+ "  from TMP_JECN_ORI_USER tu1, TMP_JECN_ORI_USER tu2"
				+ " where tu1.user_num = tu2.user_num"
				+ "   and tu1.user_num in (select tu.user_num"
				+ "                          from TMP_JECN_ORI_USER tu"
				+ "                         group by tu.user_num"
				+ "                        having count(tu.user_num) > 1)"
				+ "   and tu1.POS_NUM <> tu2.POS_NUM" + ")" + ""
				+ " select tu1.*" + "  from tmp_user tu1, tmp_user tu2"
				+ " where tu1.user_num = tu2.user_num"
				+ "   and tu1.email is not null"
				+ "   and tu2.email is not null"
				+ "   and tu1.email <> tu2.email" 
				+ " union" 
				+ " select tu1.*"
				+ "  from tmp_user tu1, tmp_user tu2"
				+ " where tu1.user_num = tu2.user_num"
				+ "   and tu1.email is null" + "   and tu2.email is not null"
				+ " union"
				+ " select tu1.*"
				+ "  from tmp_user tu1, tmp_user tu2"
				+ " where tu1.user_num = tu2.user_num"
				+ "   and tu1.email is not null" + "   and tu2.email is null"
				+ " union"
				+ " select tu1.*"
				+ "  from tmp_user tu1, tmp_user tu2"
				+ " where tu1.user_num = tu2.user_num"
				+ "   and tu1.email_type <> tu2.email_type"
				+ "   and tu1.email_type is not null"
				+ "   and tu2.email_type is not null" 
				+ " union"
				+ " select tu1.*" + "  from tmp_user tu1, tmp_user tu2"
				+ " where tu1.user_num = tu2.user_num"
				+ "   and tu1.email_type is null"
				+ "   and tu2.email_type is not null" 
				+ " union"
				+ " select tu1.*" + "  from tmp_user tu1, tmp_user tu2"
				+ " where tu1.user_num = tu2.user_num"
				+ "   and tu1.email_type is not null"
				+ "   and tu2.email_type is null";
		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 一人多岗位情况 ，联系方式不同
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpPosAndUserBean> phoneNoSameCheck() {
		String sql = "with tmp_pos as ("
				+ "  select tu1.user_num,tu1.phone"
				+ "  from TMP_JECN_ORI_USER tu1"
				+ " inner join TMP_JECN_ORI_USER tu on tu.user_num = tu1.user_num"
				+ "                                and tu.pos_num <> tu1.pos_num"
				+ "  )  select tu.*    from TMP_JECN_ORI_USER tu"
				+ " inner join tmp_pos tp on tu.user_num = tp.user_num"
				+ "                      and tu.phone <> tp.phone  union "
				+ "select tu.*    from TMP_JECN_ORI_USER tu"
				+ " inner join tmp_pos tp on tu.user_num = tp.user_num"
				+ "                      and tu.phone is null"
				+ "                      and tp.phone is not null"
				+ "                      union  select tu.*"
				+ "  from TMP_JECN_ORI_USER tu"
				+ " inner join tmp_pos tp on tu.user_num = tp.user_num"
				+ "                               and tu.phone is not null"
				+ "                               and tp.phone is  null";

		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 一人多岗位清空，岗位编号不能相同
	 * 
	 * @return List<JecnTmpPosAndUserBean>
	 */
	@Override
	public List<JecnTmpPosAndUserBean> sameLoginNameAndSamePos() {
		String sql = "with tmp_user as (select tu.user_num, tu.pos_num"
				+ "                                   from TMP_JECN_ORI_USER tu"
				+ "                                  group by tu.user_num, tu.pos_num"
				+ "                                 having count(tu.user_num) > 1)"
				+ "  select *"
				+ "    from TMP_JECN_ORI_USER"
				+ "   where user_num in (select tu.user_num"
				+ "                        from TMP_JECN_ORI_USER tu, tmp_user t"
				+ "                       where tu.user_num = t.user_num"
				+ "                         and tu.pos_num = t.pos_num)";

		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();

	}

	/**
	 * 部门行间校验，部门编号不唯一
	 * 
	 * @return List<JecnTmpOriDept>
	 */
	@Override
	public List<JecnTmpOriDept> hasSameDeptNum() {
		String sql = "with tmp_dept as (select td.dept_num"
				+ "                                      from tmp_jecn_ori_dept td"
				+ "                                     group by td.dept_num"
				+ "                                    having count(td.dept_num) > 1)"
				+ "select jod.*" + "  from tmp_jecn_ori_dept jod"
				+ " where jod.dept_num in (select dept_num from tmp_dept)";

		return getSession().createSQLQuery(sql).addEntity(JecnTmpOriDept.class)
				.list();
	}

	/**
	 * 同一部门下部门名称不能重复
	 * 
	 * @return List<JecnTmpOriDept>
	 */
	@Override
	public List<JecnTmpOriDept> hasSameDeptName() {
		String sql = " with tmp_dept as ("
				+ "  select td1.p_dept_num, td1.dept_name"
				+ "    from tmp_jecn_ori_dept td1"
				+ "   group by td1.p_dept_num, td1.dept_name"
				+ "  having count(td1.dept_name) > 1" + ")" + ""
				+ "select td.*" + "  from tmp_jecn_ori_dept td"
				+ " where exists (select 1" + "          from tmp_dept"
				+ "         where td.dept_name = dept_name"
				+ "           and td.p_dept_num = p_dept_num)" + "";

		return getSession().createSQLQuery(sql).addEntity(JecnTmpOriDept.class)
				.list();
	}

	/**
	 * 部门内不能出现重复岗位
	 * 
	 * @return List<JecnTmpPosAndUserBean>
	 */
	@Override
	public List<JecnTmpPosAndUserBean> deptHashSamePosName() {
		String sql = "select tu.*"
				+ "  from tmp_jecn_ori_user tu"
				+ " inner join tmp_jecn_ori_user tu1 on tu.dept_num = tu1.dept_num"
				+ "                                 and tu.pos_num <> tu1.pos_num"
				+ "                                 and tu.pos_name = tu1.pos_name";

		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 岗位编号不唯一
	 * 
	 * @return
	 * @return List<JecnTmpPosAndUserBean>
	 */
	@Override
	public List<JecnTmpPosAndUserBean> posNumNotOnly() {
		String sql = "with tmp_pos as ("
				+ " select tu.pos_num"
				+ "  from tmp_jecn_ori_user tu"
				+ " inner join tmp_jecn_ori_user tu1 on tu.dept_num <> tu1.dept_num"
				+ "                                 and tu.pos_num = tu1.pos_num"
				+ ""
				+ ")"
				+ "select tu.*"
				+ "  from tmp_jecn_ori_user tu"
				+ " where exists (select 1 from tmp_pos tp where tu.pos_num = tp.pos_num)";
		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 岗位所属部门编号在部门集合中不存在
	 * 
	 * @return
	 * @return List<JecnTmpPosAndUserBean>
	 */
	@Override
	public List<JecnTmpPosAndUserBean> posNumRefDeptNumNoExists() {
		String sql = "select tu.*"
				+ "  from tmp_jecn_ori_user tu"
				+ " where not exists"
				+ " (select 1 from tmp_jecn_ori_dept td where tu.dept_num = td.dept_num) AND TU.DEPT_NUM IS NOT NULL";
		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();
	}

	/**
	 * 岗位编号相同，岗位名称必须相同
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpPosAndUserBean> posNameNotSameList() {
		String sql = "select tu.*"
				+ "  from tmp_jecn_ori_user tu"
				+ " inner join tmp_jecn_ori_user tu1 on tu.dept_num = tu1.dept_num"
				+ "                                 and tu.pos_num = tu1.pos_num"
				+ "                                 and tu.pos_name <> tu1.pos_name";
		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();

	}

	/**
	 * 岗位编号相同，岗位所属部门必须相同
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpPosAndUserBean> posNumSameAndDeptSameList() {
		String sql = "with tmp_pos as (" + "  select tu.pos_num,tu.dept_num"
				+ "    from tmp_jecn_ori_user tu"
				+ "   group by tu.pos_num,tu.dept_num" + "" + ")"
				+ "select tu.*" + "  from tmp_jecn_ori_user tu"
				+ " where tu.pos_num in (select pos_num"
				+ "                        from tmp_pos"
				+ "                       group by pos_num"
				+ "                      having count(pos_num) > 1)";

		return getSession().createSQLQuery(sql).addEntity(
				JecnTmpPosAndUserBean.class).list();

	}
}
