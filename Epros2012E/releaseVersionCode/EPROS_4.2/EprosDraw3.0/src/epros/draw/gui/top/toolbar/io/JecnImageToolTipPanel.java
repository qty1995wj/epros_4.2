package epros.draw.gui.top.toolbar.io;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JecnImageToolTipPanel extends JPanel{
	/** 日志对象 */
	private static final Log log = LogFactory.getLog(JecnImageToolTipPanel.class);
	/** 放大的倍数x */
	private double sx = 1;
	
	/** 放大的倍数 y */
	private double sy = 1;
	
	public void setSx(double sx) {
		this.sx = sx;
	}

	public void setSy(double sy) {
		this.sy = sy;
	}

	
	/** 画图面板容器 */
	private JComponent jcomponent;
	
	public JecnImageToolTipPanel(JComponent jcomponent){
		this.jcomponent = jcomponent;
	}
	
	
     /**
      * 通过传入的数据生成图片的Panel
      * @param g
      * @param jcomponent
      */
 	public void paintComponent(Graphics g) {
			// 画笔
			Graphics2D g2d = (Graphics2D) g;
			// 设置 Graphics2D 上下文的背景色
			g2d.setBackground(Color.WHITE);
			// 返回 Graphics2D 上下文中当前 Transform 的副本(坐标线性映射)
			AffineTransform at = g2d.getTransform();
			// 使用此 Graphics2D 中的 Transform 组合 AffineTransform 对象(沿着x，y方向放大的数)
			g2d.transform(AffineTransform.getScaleInstance(sx,sy));
			// 保存面板数据的集合
			ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
			// 将jcomponent中的组件加入到dbcomponents集合中，修改缓冲区设置为false
			updateDoubleBuffered(jcomponent, dbcomponents);
			// 绘制图
			jcomponent.paint(g);
//			jcomponent.paintComponents(g);
			// 修改缓冲区设置
			resetDoubleBuffered(dbcomponents);
			// 重写transform 用于调整分辨率
			g2d.setTransform(at);
			// 关闭画笔
			g2d.dispose();
	}
 	
 	/**
	 * 添加组件到dbcomponents集合， 修改缓冲区设置
	 * @param component
	 * @param dbcomponents
	 */
	private void updateDoubleBuffered(JComponent component,ArrayList<JComponent> dbcomponents) {
		if(component == null){
			log.info("JecnImageToolTip类中updateDoubleBuffered，传入的component参数为空");
			return;
		}
		if(dbcomponents == null){
			log.info("JecnImageToolTip类中updateDoubleBuffered，传入的dbcomponents参数为空");
			return;
		}
		// 判断是否使用双缓冲区进行缓冲
		if (component.isDoubleBuffered()) {
			// 把JComponent 添加到集合中
			dbcomponents.add(component);
			// 修改缓冲区设置
			component.setDoubleBuffered(false);
		}
		// 获取面板的组件数
		for (int i = 0; i < component.getComponentCount(); i++) {
			// 获取单个组件
			Component c = component.getComponent(i);
			// 如果组件属于 JComponent
			if (c instanceof JComponent) {
				updateDoubleBuffered((JComponent) c, dbcomponents);
			}
		}
	}
	
	/**
	 * 修改缓冲区设置
	 * 
	 * @param dbcomponents
	 */
	private void resetDoubleBuffered(ArrayList<JComponent> dbcomponents) {
		if(dbcomponents==null){
			log.info("JecnImageToolTip类中resetDoubleBuffered，传入的dbcomponents参数为空");
			return;
		}
		for (JComponent component : dbcomponents) {
			component.setDoubleBuffered(true);
		}
	}
}
