package epros.designer.table;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public abstract class JecnDBClickTable extends JecnTable {

	public JecnDBClickTable() {
		super();
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// 判断为左键双击
				if (e.getClickCount() == 2 && e.getButton() == 1) {
					dbClickMethod();
				}

			}
		});
	}

	protected abstract void dbClickMethod();

}
