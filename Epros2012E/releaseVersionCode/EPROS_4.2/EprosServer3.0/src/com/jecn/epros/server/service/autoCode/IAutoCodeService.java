package com.jecn.epros.server.service.autoCode;

import java.util.List;

import org.hibernate.HibernateException;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.autoCode.AutoCodeRelatedBean;
import com.jecn.epros.server.bean.autoCode.ProcessCodeTree;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

public interface IAutoCodeService {
	void deleteProcessCode(List<Long> ids) throws Exception;

	void updateName(Long id, String newName, Long updatePersonId) throws Exception;

	Long addPorcessCode(ProcessCodeTree codeTree) throws Exception;

	/**
	 * 获取子节点
	 * 
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	List<JecnTreeBean> getChildProcessCodes(Long pId, int type) throws Exception;

	/**
	 * 自动编号保存
	 * 
	 * @param codeBean
	 * @param codeRelateds
	 * @param relatedType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @throws Exception
	 */
	AutoCodeBean saveAutoCode(AutoCodeBean codeBean, Long id, int relatedType) throws HibernateException, Exception;

	AutoCodeBean getAutoCodeBean(Long relatedId, int relatedType) throws Exception;

	JecnTreeBean getTreeBeanByName(String name) throws Exception;

	AutoCodeRelatedBean getAutoCodeRelatedBean(Long relatedId, int relatedType) throws Exception;

	List<JecnTreeBean> getPnodes(Long id, int type) throws Exception;

	void updateAutoCodeTableByRelatedId(Long relatedId, int relatedType, int codeTotal) throws Exception;
}
