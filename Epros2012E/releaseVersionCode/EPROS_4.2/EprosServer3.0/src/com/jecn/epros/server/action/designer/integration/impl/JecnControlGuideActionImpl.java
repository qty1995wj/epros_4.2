package com.jecn.epros.server.action.designer.integration.impl;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.action.designer.integration.IJecnControlGuideAction;
import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.integration.IJecnControlGuideService;

/**
 * 内控指引知识库Action实现类
 * 
 * @author Administrator
 * 
 */
public class JecnControlGuideActionImpl implements IJecnControlGuideAction {
	/** 内控指引知识库Service接口 */
	private IJecnControlGuideService jecnControlGuideService;

	public IJecnControlGuideService getJecnControlGuideService() {
		return jecnControlGuideService;
	}

	public void setJecnControlGuideService(IJecnControlGuideService jecnControlGuideService) {
		this.jecnControlGuideService = jecnControlGuideService;
	}

	/**
	 * 添加内控指引目录
	 * 
	 * @param jecnControlGuide
	 *            内控指引知识库Bean
	 * @return
	 * @throws Exception
	 */
	@Override
	public long addJecnControlGuideDir(JecnControlGuide jecnControlGuide) throws Exception {
		return jecnControlGuideService.addJecnControlGuideDir(jecnControlGuide);
	}

	/**
	 * 根据父ID查询内控指引目录子节点
	 * 
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getChildJecnControlGuideDirs(Long pId) throws Exception {
		return jecnControlGuideService.getChildJecnControlGuideDirs(pId);
	}

	/**
	 * 移动节点
	 * 
	 * @param listIds
	 *            要移动的节点ID集合
	 * @param pId
	 *            当前节点父ID
	 * @param updatePersonId
	 *            更新人
	 * @throws Exception
	 */
	@Override
	public void moveSortControlGuide(List<Long> listIds, Long pId, Long updatePersonId) throws Exception {
		jecnControlGuideService.moveSortControlGuide(listIds, pId, updatePersonId);
	}

	/**
	 * 根据父ID查询内控指引子节点
	 * 
	 * @param pId
	 *            当前节点父ID
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getChildJecnControlGuides(Long pId, Long projectId) throws Exception {
		return jecnControlGuideService.getChildJecnControlGuides(pId, projectId);
	}

	/**
	 * 内控指引目录重命名
	 * 
	 * @param id
	 *            当前目录ID
	 * @param name
	 *            新名称
	 * @param updatePersonId
	 *            更改人
	 * @throws Exception
	 */
	@Override
	public void reControlGuideDirName(Long id, String name, Long updatePersonId) throws Exception {
		jecnControlGuideService.reControlGuideDirName(id, name, updatePersonId);
	}

	/**
	 * 根据所选ID集合删除
	 * 
	 * @param listIds
	 *            所选ID集合
	 * @throws Exception
	 */
	@Override
	public void deleteControlGuides(List<Long> listIds, Long peopleId) throws Exception {
		jecnControlGuideService.deleteControlGuides(listIds, peopleId);
	}

	/**
	 * 内控指引节点排序
	 * 
	 * @param list
	 *            节点集合
	 * @param pId
	 *            当前节点父ID
	 * @param updatePersonId
	 *            更改人
	 * @throws Exception
	 */
	@Override
	public void updateSortControlGuide(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception {
		jecnControlGuideService.updateSortControlGuide(list, pId, updatePersonId);
	}

	/**
	 * 根据名称查询内控指引知识库目录
	 * 
	 * @param name
	 *            内控指引目录名称
	 * @return JecnControlGuide集合
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getControlGuideByName(String name) throws Exception {
		return jecnControlGuideService.getControlGuideByName(name);
	}

	/**
	 * 根据选中节点查询具体条款内容
	 */
	@Override
	public JecnControlGuide getJecnControlGuideById(Long id) throws Exception {
		return jecnControlGuideService.getJecnControlGuideById(id);
	}

	/**
	 * 编辑知识库条款
	 */
	@Override
	public boolean updateJecnControlGuide(Long id, String name, String description, Long updatePerson, Date updateTime)
			throws Exception {
		return jecnControlGuideService.updateJecnControlGuide(id, name, description, updatePerson, updateTime);
	}
}
