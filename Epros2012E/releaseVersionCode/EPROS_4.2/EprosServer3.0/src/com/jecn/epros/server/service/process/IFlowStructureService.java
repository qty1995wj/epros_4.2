package com.jecn.epros.server.service.process;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.bean.checkout.CheckoutRoleItem;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.download.ProcessDownloadBaseBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.ProcessMapDownloadBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.FlowFileContent;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.JecnFlowFigureNow;
import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.process.JecnSaveProcessData;
import com.jecn.epros.server.bean.process.ProcessArchitectureDescriptionBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessData;
import com.jecn.epros.server.bean.process.ProcessFileNodeData;
import com.jecn.epros.server.bean.process.ProcessInfoBean;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.ProcessMapData;
import com.jecn.epros.server.bean.process.ProcessMapRelatedFileData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.process.entry.JecnEntry;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.process.temp.TmpKpiShowValues;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.PubSourceData;
import com.jecn.epros.server.bean.system.PubSourceSave;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IFlowStructureService extends IBaseService<JecnFlowStructureT, Long> {
	/**
	 * @author yxw 2012-6-6
	 * @description:加载所有的流程地图与流程图
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllFlows(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:根据父节点ID获得流程或流程地图的子节点
	 * @param flowId
	 * @param projectId
	 * @param isContainsDelNode
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildFlows(Long flowId, Long projectId, boolean isContainsDelNode) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:根据流程地图ID加载子节点 只包括流程地图
	 * @param flowId
	 *            父ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildProcessMap(Long flowMapId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:添加流程地图
	 * @param flowStructureBean
	 * @param flowMapBean
	 * @param posIds
	 * @param orgIds
	 * @throws Exception
	 */
	public JecnFlowStructureT addFlowMap(JecnFlowStructureT jecnFlowStructureT, JecnMainFlowT jecnMainFlowT,
			String posIds, String orgIds, String posGroupIds, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:添加流程
	 * @param jecnFlowStructureT
	 * @param jecnFlowBasicInfoT
	 * @param posIds
	 * @param orgIds
	 * @param sptIds
	 *            支持工具
	 * @param posGroupIds
	 * @throws Exception
	 */
	public JecnFlowStructureT addFlow(JecnSaveProcessData processData) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点排序
	 * @param list
	 * @param pId
	 * @param projectId
	 * @param updatePersonId
	 *            操作人
	 * @throws Exception
	 */
	public void updateSortFlows(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点移动
	 * @param listIds
	 * @param pId
	 * @param updatePersonId
	 *            操作人
	 * @throws Exception
	 */
	public void moveFlows(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description: 删除节点
	 * @param listIds
	 * @param updatePersonId
	 * @return 0 删除异常 1正常删除 2有节点处于任务中
	 */
	public void deleteFlows(List<Long> listIds, Long updatePersonId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:流程地图与流程重命名
	 * @param id
	 * @param newName
	 * @param updatePersonId
	 * @param userType
	 *            0：管理员
	 */
	public void updateName(Long id, String newName, Long updatePersonId, int userType) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:根据名称查询流程地图和流程图
	 * @param name
	 * @param projectId
	 *            @ type 0是所有，1是流程地图，2是流程
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchFlowByName(String name, Long projectId, int type) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:流程地图描述
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnMainFlowT getFlowMapInfo(Long id) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:更新流程地图描述
	 * @param flowMapBean
	 * @param posIds
	 * @param orgIds
	 * @param isPublic
	 *            1是公开，0是密码
	 * @updatePersonId 操作人ID
	 * @param flowNum
	 *            流程编号
	 * @throws Exception
	 */
	public void updateFlowMap(JecnMainFlowT jecnMainFlowT, Long updatePersonId, String flowNum) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:流程解除编辑
	 * @param id
	 * @param updatePersonId
	 * @throws Exception
	 */
	public void moveEdit(Long id, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description: 判断流程地图或流程图能否打开
	 * @param userId
	 *            登录用户ID
	 * @param processId
	 * @return true 可以打开 false 不可打开
	 * @throws Exception
	 */
	public boolean updateProcessCanOpen(Long userId, Long processId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：更新流程概括信息
	 * @param flowId
	 *            流程ID
	 * @param jecnFlowStructureT
	 *            流程对象
	 * @param jecnFlowBasicInfoT
	 *            流程基本信息对象
	 * @param supportToolIds
	 *            支持工具ID集合
	 * @param fileId
	 *            附件ID
	 * @throws Exception
	 */
	public void updateFlowBaseInformation(Long flowId, JecnFlowStructureT jecnFlowStructureT,
			JecnFlowBasicInfoT jecnFlowBasicInfoT, String orgIds, String posIds, String supportToolIds,
			Long updatePeopleId, String posGroupIds) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：获得流程中活动的操作规范文件的集合（设计器）
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowOeprationFilesByFlowIdDesign(Long flowId) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：获得流程中相关流程
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRelateByFlowIdDesign(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程记录保存
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowRecordT> getJecnFlowRecordTListByFlowId(Long flowId) throws Exception;

	/**
	 * 获得流程主表临时表数据
	 * 
	 * @param flowId流程主表ID
	 * @return
	 * @throws Exception
	 */
	public JecnFlowStructureT getJecnFlowStructureT(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程KPI
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiNameT> getJecnFlowKpiNameTList(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：添加KPI
	 * @param jecnFlowKpiNameT
	 * @throws Exception
	 */
	public Long addKPI(JecnFlowKpiNameT jecnFlowKpiNameT, Long updatePeopleId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：更新KPI
	 * @param jecnFlowKpiNameT
	 * @throws Exception
	 */
	public void updateKPI(JecnFlowKpiNameT jecnFlowKpiNameT, Long updatePeopleId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得KPI的值
	 * @param flowKpiId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpi> getJecnFlowKpiListByKPIId(Long flowKpiId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：添加KPI的值
	 * @param jecnFlowKpi
	 * @throws Exception
	 */
	public void addKPIVaule(JecnFlowKpi jecnFlowKpi, Long updatePeopleId, Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：更新KPI的值
	 * @param jecnFlowKpi
	 * @throws Exception
	 */
	public void updateKPIVaule(JecnFlowKpi jecnFlowKpi, Long updatePeopleId, Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：删除kpi
	 * @param jecnFlowKpi
	 * @throws Exception
	 */
	public void deleteKPIs(List<Long> listIds, Long updatePeopleId, Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：删除kpi
	 * @param jecnFlowKpi
	 * @throws Exception
	 */
	public void deleteKPIValues(List<Long> listIds, Long updatePeopleId, Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：删除kpi
	 * @param jecnFlowKpi
	 * @throws Exception
	 */
	public void deleteKPIValues(Long listId, Long updatePeopleId, Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description 批量导入KPI的值
	 * @param kpiId
	 * @param JecnFlowKpi
	 * @throws Exception
	 */
	public void addKPIVaules(Long kpiId, List<JecnFlowKpi> jecnFlowKpiList, Long updatePeopleId, Long flowId)
			throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：更新操作说明
	 * @param flowId
	 * @param jecnFlowStructureT
	 * @param jecnFlowBasicInfoT
	 * @param listJecnFlowRecordT
	 */
	public void updateFlowOperation(Long flowId, JecnFlowBasicInfoT jecnFlowBasicInfoT,
			JecnFlowDriverT jecnFlowDriverT, List<JecnFlowRecordT> listJecnFlowRecordT, Long updatePeopleId)
			throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：相关制度
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<RuleT> getRuleTByFlowId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：相关标准
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<StandardBean> getStandardBeanByFlowId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：更新相关制度,标准
	 * @param flowId
	 * @param ruleIds
	 * @throws Exception
	 */
	public void updateFlowRelateRuleT(Long flowId, String ruleIds, String standardIds, Long updatePeopleId)
			throws Exception;

	/***************************************************************************
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程属性
	 * @param flowId
	 * @return 
	 *         list一共返回7个值，0是流程类别ID，1是流程类别名称，2是流程责任部门ID，3是流程责任部门名称，4是流程责任类型，5是责任人ID
	 *         ，6是流程责任人名称
	 * @throws Exception
	 */
	public ProcessAttributeBean getFlowAttribute(Long flowId) throws Exception;

	/**
	 * 更新流程属性
	 * 
	 * @param processInfoBean
	 * @throws Exception
	 */
	public void updateFlowAttribute(ProcessInfoBean processInfoBean) throws Exception;

	/**
	 * @author yxw 2012-7-31
	 * @description:流程地图保存
	 * @param mapData
	 *            流程地图数据
	 * @param flowImgeBytes
	 * @param updatePersionId
	 * @throws Exception
	 */
	public void saveProcessMap(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-7-31
	 * @description:流程图保存
	 * @param processData
	 * @param flowImgeBytes
	 *            生成流程图
	 * @param type
	 *            0流程图 1模板
	 * @throws Exception
	 */
	public void saveProcess(ProcessData processData, byte[] flowImgeBytes, Long updatePersionId) throws Exception;

	/***************************************************************************
	 * 获得流程基本信息临时表
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowBasicInfoT getJecnFlowBasicInfoT(Long flowId) throws Exception;

	/**
	 * @author yxw 2012-7-24
	 * @description:打开流程地图
	 * @param id
	 *            流程 地图ID
	 * @param projectId
	 * @param type
	 *            0流程地图 1模板
	 * @return
	 */
	public ProcessOpenMapData getProcessMapData(long id, long projectId) throws Exception;

	/**
	 * @author yxw 2012-7-24
	 * @description:打开流程图
	 * @param id
	 * @param projectId
	 * @param type
	 *            0流程图 1模板
	 * @return
	 * @throws Exception
	 */
	public ProcessOpenData getProcessData(long id, long projectId) throws Exception;

	/***************************************************************************
	 * 根据流程ID获取流程时间驱动临时表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public JecnFlowDriverT getJecnFlowDriverT(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：流程搜索
	 * @param flowId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodes(Long flowId, Long projectId) throws Exception;

	/**
	 * 
	 * @author zhangchen Aug 6, 2012
	 * @description： 回收站-获得项目下删除的流程
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getDelsFlow(Long projectId, Set<Long> setIds, boolean isAdmin) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：回收站--删除流程
	 * @param setIds
	 * @param updatePersionId
	 * @throws Exception
	 */
	public void deleteIdsFlow(List<Long> ListIds, Long projectId, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-8-6
	 * @description:新增判断是否重名
	 * @param name
	 * @param pid
	 * @param isFlow
	 *            0:流程地图 1：流程
	 * @param projectId
	 * @return true 重名 false不重名
	 * @throws Exception
	 */
	public boolean validateAddName(String name, long pid, long isFlow, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-8-6
	 * @description:重命名时判断新名称是否与其它名称重名
	 * @param name
	 * @param id
	 * @param pid
	 * @param isFlow
	 *            0:流程地图 1：流程
	 * @return
	 * @throws Exception
	 */
	public boolean validateUpdateName(String name, long id, long pid, long isFlow, Long projectId) throws Exception;

	/**
	 * @author zhangchen Aug 7, 2012
	 * @description：获得父类节点的信息（包括子节点）（查看删除的路径）
	 * @param projectId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getParentsContainSelf(Long projectId, Long id) throws Exception;

	/**
	 * @author yxw 2012-8-9
	 * @description:下载流程文件
	 * @param flowId
	 * @param treeNodeType
	 * @param isPub
	 *            true是发布，false是不发布
	 * @return
	 * @throws Exception
	 */
	public JecnCreateDoc getFlowFile(Long flowId, TreeNodeType treeNodeType, boolean isPub) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:撤销发布
	 * @param id
	 *            流程ID
	 * @param type
	 *            节点类型
	 * @param peopleId
	 * @throws Exception
	 */
	public void cancelRelease(Long id, TreeNodeType type, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:直接发布-不记录版本
	 * @param id
	 *            流程ID
	 * @param type
	 *            节点类型
	 * @param peopleId
	 * @throws Exception
	 */
	public void directRelease(Long id, TreeNodeType type, Long peopleId) throws Exception;

	/**
	 * 获取流程图操作说明
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @return
	 * @throws Exception
	 */
	public ProcessDownloadBean getProcessFile(Long flowId) throws Exception;

	/**
	 * 获取流程地图操作说明
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @param isPub
	 *            true正式表，false临时表
	 * @return
	 * @throws Exception
	 */
	public ProcessMapDownloadBean getProcessMapFile(Long flowId, boolean isPub) throws Exception;

	/**
	 * 批量下载操作说明
	 * 
	 * @author fuzhh 2013-9-2
	 * @param processIdList
	 * @return
	 */
	public String getProcessFiles(List<Long> processIdList, boolean isPub, boolean isDownFile) throws Exception;

	/**
	 * 获取单个流程的操作说明
	 * 
	 * @author
	 * @param flowId
	 * @param isPub
	 * @param isDownFile
	 * @param isAdmin
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	public byte[] getProcessFile(Long flowId, boolean isPub, Long peopleId, Long projectId, boolean isAdmin)
			throws Exception;

	/**
	 * 获取单个流程附件集合
	 * 
	 * @param flowId
	 * @param isPub
	 * @param peopleId
	 * @param projectId
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public Map<EXCESS, List<FileOpenBean>> getProcessExcessFile(Long flowId, boolean isPub, Long peopleId,
			Long projectId, boolean isAdmin) throws Exception;

	/**
	 * 根据流程ID查询对应风险
	 * 
	 * @author fuzhh 2013-11-27
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRisk> getRiskByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * 根据风险ID集合查询对应风险
	 * 
	 * @author fuzhh 2013-11-27
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRisk> getRiskByRiskIds(Set<Long> riskIds) throws Exception;

	/***
	 * 根据流程名称模糊搜索回收站流程
	 * 
	 * @param flowName
	 *            流程名称
	 * @throws Exception
	 */
	public List<JecnFlowStructureT> searchRecycleFlowByName(String flowName, Long projectId, Set<Long> setIds,
			boolean isAdmin) throws Exception;

	/**
	 * 获取流程相关制度
	 * 
	 * @author fuzhh 2013-12-9
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRelateRulesWeb(Long flowId, boolean isPub) throws Exception;

	/***************************************************************************
	 * 获得流程基本信息临时表2
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowBasicInfoT2 getJecnFlowBasicInfoT2(Long flowId) throws Exception;

	/**
	 * @author huoyl
	 * @description:根据名称查询流程地图和流程图(已发布的流程、流程地图)
	 * @param name
	 * @param projectId
	 *            @ type 1是流程地图，2是流程
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchPubFlowByName(String name, Long projectId, int type) throws Exception;

	/**
	 * @description：获得流程下的KPI及相关数据
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiNameT> getFlowKpiNameTList(Long flowId) throws Exception;

	/**
	 * 推送流程图到第三方系统中
	 * 
	 * @param flowId
	 *            :流程ID
	 * @param sysNcType
	 *            公司类型 path 保存路径
	 * @return 0:同步失败 1： 同步成功 2：没有开始活动，同步失败
	 * */

	public int sysNcFlowProcess(Long flowId, int sysNcType, String path) throws Exception;

	/**
	 * 流程回收站：树
	 * 
	 * @param id
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long id, Long projectId) throws Exception;

	/**
	 * 流程回收站：恢复当前节点以及子节点
	 * 
	 * @param recycleNodesIdList
	 *            选中的节点的id
	 * @param projectId
	 *            项目id
	 * @throws Exception
	 */
	public void updateRecoverCurAndChild(List<Long> recycleNodesIdList, Long projectId) throws Exception;

	/**
	 * 流程回收站：恢复当前节点
	 * 
	 * @param recycleNodesIdList
	 * @throws Exception
	 */
	public void updateRecoverCur(List<Long> recycleNodesIdList, Long projectId) throws Exception;

	/**
	 * 流程回收站：搜索获得删除的且有权限操作的节点集合
	 * 
	 * @param name
	 *            搜索的名称
	 * @param projectId
	 *            项目id
	 * @param processIds
	 *            有权限的流程的id
	 * @param isAdmin
	 *            是否管理员
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId, Set<Long> processIds, boolean isAdmin)
			throws Exception;

	/**
	 * 流程回收站：定位
	 * 
	 * @param id
	 *            需要定位的节点的id
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception;

	/**
	 * @author huoyl
	 * @param upload
	 *            File 文件
	 * @param curPeopleId
	 * @param type
	 * @return int[] 大小为2 0为导入的数据个数 1为导出的错误数据
	 * @throws Exception
	 */
	public int[] importValidityData(File upload, Long curPeopleId, int type) throws Exception;

	/**
	 * 获得流程对象
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowStructure findJecnFlowStructureById(Long flowId) throws Exception;

	TmpKpiShowValues getKpiShowValues(Long flowId) throws Exception;

	List<String> getConfigKpiRowValues(JecnFlowKpiName flowKpiName, List<JecnConfigItemBean> configItemBeans);

	public List<String> getDesignAuthFlows(Long peopleId, Long projectId) throws Exception;

	List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, int type, Long peopelId) throws Exception;

	/**
	 * 安码BPM接口，插入流程版本发布记录
	 * 
	 * @param flowId
	 * @param version
	 * @return 0:插入成功，-1:插入失败：1：已存在当前版本，不允许重复推送
	 * @throws Exception
	 */
	int processPushUltimus(Long flowId, String flowName, String version) throws Exception;

	/**
	 * 根据人员ID获取流程占用人
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	String getFlowOccupierUserNameById(Long flowId, Long curPeopleId) throws Exception;

	public void batchReleaseNotRecord(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception;

	public void batchCancelRelease(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception;

	public void updateFlowOperation(Long flowId, JecnFlowBasicInfoT flowBasicInfoT, Map<String, Object> resultMap,
			Long peopleId) throws Exception;

	ProcessOpenMapData getProcessMapRelatedData(long id, long projectId) throws Exception;

	void saveMapRelatedProcess(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePeopleId) throws Exception;

	void updateFlowMap(ProcessArchitectureDescriptionBean processArchitectureDescriptionBean) throws Exception;

	ProcessArchitectureDescriptionBean getProcessArchitectureDescriptionBean(Long flowId) throws Exception;

	public List<ProcessInterface> findFlowInterfaces(Long id, boolean isPub) throws Exception;

	String excelImporteFlowKpiImage(Long flowId, Long kpiAndId, File upload, String kpiHorType, String startTime,
			String endTime, Long userId);

	Long getRelatedFileId(Long flowId) throws Exception;

	Long createRelatedFileDirByPath(Long id, Long projectId, Long createPeopleId) throws Exception;

	FileOpenBean openFileContent(Long id) throws Exception;

	List<JecnTreeBean> getStandardizedFiles(Long flowId) throws Exception;

	List<JecnTreeBean> getRelatedRiskList(Long flowId) throws Exception;

	JecnFlowStructureT createProcessFile(ProcessFileNodeData processFileData) throws Exception;

	JecnFlowStructureT updateProcessFile(ProcessFileNodeData processFileData) throws Exception;

	public void sendFlowDriverEmail() throws Exception;

	public void favoriteUpdateSendEmail() throws Exception;

	public void updateProcessMapRelatedFiles(ProcessMapRelatedFileData relatedFileData) throws Exception;

	public Object[] getFlowFileContentName(Long id, boolean isPub) throws Exception;

	Map<String, String> getProcessFileNameAndContentId(long id, boolean isPub) throws Exception;

	FileOpenBean openFileContent(Long flowId, boolean isPub) throws Exception;

	public List<JecnTreeBean> getProcessApplyOrgs(Long id) throws Exception;

	public List<JecnTreeBean> getDirverEmailPeopleByFlowId(Long flowId) throws Exception;

	FlowFileContent getFlowFileContent(Long contentId) throws Exception;

	Map<Long, String> getNodeParentById(List<Long> ids, boolean isPub, TreeNodeType nodeType) throws Exception;

	public void taskFinishSendEmail() throws Exception;

	public ProcessDownloadBaseBean getProcessDownloadBean(Long flowId, boolean isPub, String mark) throws Exception;

	public boolean isNotAbolishOrNotDelete(Long id) throws Exception;

	public void addFlowFileHeadInfo(FlowFileHeadData flowFileHeadData);

	public FlowFileHeadData findFlowFileHeadInfo(Long selectId);

	public List<Object[]> getFlowElementById(List flowId);

	public List<Object[]> getFlowElementByLinkId(List flowId);

	public void reNameFlowFigureText(Long flowLinkId, String flowName);

	public List<FlowFileHeadData> findFlowFileHeadInfoVerification(Long id);

	public PubSourceData getPubSourceData(Long rId, int rType) throws Exception;

	public void sendPubCopyNoteEmails(PubSourceSave pubNoteData) throws Exception;

	public void saveOrUpdatePubCopyNote(PubSourceSave pubNoteData) throws Exception;

	public void handPubSendEmail() throws Exception;

	void replaceAlllElementAttrs(List<JecnFlowFigureNow> list);

	public void pubEmailTip();

	public boolean belowFlowExistFile(List<Long> listIds) throws Exception;

	public List<Long> getRelatedFileIds(List<Long> listIds) throws Exception;

	public boolean belowRuleExistFile(List<Long> listIds) throws Exception;

	public String validateNamefullPath(String flowName, long id, long i, Long projectId);

	void copyNodeDatasHandle(List<Long> copyIds, Long copyToId, int option, Long peopleId) throws Exception;

	public List<JecnFlowInoutData> getFlowInoutsByType(int type, Long flowId, boolean isPub) throws Exception;

	public List<CheckoutResult> checkout(Long id);

	public ByteFile downLoadCheckout(Long id);

	public List<CheckoutRoleResult> checkoutRole(Long id);

	public void updateActivityPosRelation(Long flowId, List<CheckoutRoleItem> result);

	public ByteFile downLoadCheckoutRole(Long valueOf);

	public void sendFlowDriverEmailNew() throws Exception;

	public void mnInit(int type) throws Exception;

	public void transToProcess(List<Long> nodes, Long peopleId) throws Exception;

	public List<JecnFileContent> getJecnFlowFileContents(Long id) throws Exception;

	List<JecnTreeBean> getChildFlows(List<Long> pids) throws Exception;

	public List<JecnEntry> fetchEntrys();

	public void saveEntry(JecnEntry entry);

	public List<JecnTreeBean> fetchEntryLimit(String name, int count);
}
