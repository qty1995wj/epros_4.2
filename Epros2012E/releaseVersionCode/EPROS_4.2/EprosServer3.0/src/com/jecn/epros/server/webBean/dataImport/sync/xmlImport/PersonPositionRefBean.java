package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

public class PersonPositionRefBean {
	private Long id;
	private Long personId;
	private Long posId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public Long getPosId() {
		return posId;
	}
	public void setPosId(Long posId) {
		this.posId = posId;
	}
	
}
