package com.jecn.epros.server.action.web.dataImport.sync.excelorDB;

import java.io.File;

import net.sf.json.JSONObject;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.server.action.web.timer.TimerAction;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ImportDataFactory;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserImportBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.convert.ReadUserConfigXml;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl.ImportDataFactoryImpl.ImportType;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.WebConfigBean;

/**
 * 人员同步标准版Excel导入和DB数据同步导入
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Mar 1, 2013 时间：10:44:16 AM
 */
public class UserSyncAction extends TimerAction {
	/** 执行待导入数据导入的业务处理对象 */
	private UserImportBuss userImportBuss = null;
	/** 返回前台值 */
	private String stringResult = null;

	/** *****************excel****************** */
	/** 这两个属性是必须有的,有框架要用 */
	private String uploadContentType = null;
	/** 这两个属性是必须有的,有框架要用 */
	private String uploadFileName = null;
	/** excel文件名称 */
	private File upload = null;
	/** 获取外来的待导入数据对象 */
	protected ImportDataFactory importDataFactory = null;
	/** *****************配置文件:开始时间、间隔天数、自动还是手动 start****************** */
	protected AbstractConfigBean abstractConfigBean = null;

	private String callback;

	/**
	 * 
	 * 初始化界面获取页面所需值
	 * 
	 */
	public String initParams() {
		return initParams(getImportTypeByData());
	}

	public void getSyncConfig() throws Exception {
		AbstractConfigBean initTime = importDataFactory.getInitTime(getImportTypeByData());
		WebConfigBean config = new WebConfigBean();
		config.setIaAuto(initTime.getIaAuto());
		config.setIntervalDay(initTime.getIntervalDay());
		config.setStartTime(initTime.getStartTime());
		config.setSyncType(JecnContants.otherUserSyncType);
		JSONObject json = JSONObject.fromObject(config);
		String function = "callback(" + json.toString() + ")";
		outJsonString(function);
	}

	/**
	 * 
	 * 保存参数
	 * 
	 */
	public String saveParams() {

		log.info("保存定时参数开始...");

		// 保存参数
		MessageResult result = userAutoBuss.saveParams(abstractConfigBean.getStartTime(), abstractConfigBean
				.getIntervalDay(), abstractConfigBean.getIaAuto());

		if (!result.isSuccess()) {
			callBackPage(result.getResultMessage(), "false");
			return null;
		}

		// 保存参数没有出错且当给定是自动同步时
		if (result.isSuccess() && SyncConstant.AUTO_SYNC_USER_DATA_FLAG.equals(abstractConfigBean.getIaAuto())) {

			// 重新开始定时监听
			reStartTime();
		}
		log.info("保存定时参数结束...");
		// ResultHtml("保存成功！", "saveParams.action");
		if (result.isSuccess()) {
			// 获取json字符串
			outJsonString("true");
		} else {
			outJsonString("false");
		}
		return null;
	}

	/**
	 * 
	 * 导入数据
	 * 
	 * @return String
	 * @throws Exception
	 */
	public void importData() throws Exception {
		importDataBuss(false);
	}

	/**
	 * 
	 * 定时器执行 导入数据
	 * 
	 * @return String
	 * @throws Exception
	 */
	@Override
	public void synData() throws Exception {
		importDataBuss(true);
	}

	/**
	 * 执行同步
	 * 
	 * @param isTimer
	 *            True 执行定时
	 */
	private void importDataBuss(boolean isTimer) {
		long startTime = System.currentTimeMillis();
		log.info("同步人员数据开始" + System.currentTimeMillis());
		this.isTimer = isTimer;

		try {
			MessageResult importDataBuss = userImportBuss.importDataBuss(isTimer, importType, upload, this
					.getUploadFileName());
			if (importDataBuss.isSuccess()) {
				callBackPage(importDataBuss.getResultMessage(), "true");
			} else {
				callBackPage(importDataBuss.getResultMessage(), "false");
			}
		} catch (Exception ex) {
			log.error("UserSyncAction类importData方法：", ex);
			callBackPage(SyncErrorInfo.IMPORT_ERROR, "false");
			return;
		}

		log.info("同步人员数据结束,同步人员数据所用时间" + (System.currentTimeMillis() - startTime));
	}

	/**
	 * 页面返回参数
	 * 
	 */
	private void callBackPage(String str, String falg) {
		if (isTimer) {// 定时器不执行session
			return;
		}
		// 3. excel读取数据方式:上传excel到服务器本地
		if (ImportType.excel.toString().equals(importType)) {
			ResultHtml(str, getHrefString());
		} else {
			outJsonString(falg);
		}
	}

	/**
	 * 获取跳转后的URL
	 * 
	 * @param operation
	 * @return
	 */
	private String getHrefString() {
		String basePath = (String) this.getSession().getAttribute("basePath");
		return basePath + "index.jsp?index=systemManage";
	}

	/**
	 * 
	 * 保存参数
	 * 
	 * @param configBean
	 */
	private String saveParams(AbstractConfigBean configBean) {

		if (configBean == null) {
			// 拼装返回页面
			callBackPage(SyncErrorInfo.GET_CONFIG_FILE_FAIL, "false");
			return "error";
		}
		// 开始时间
		String startTime = configBean.getStartTime();
		// 间隔时间
		String intervalDay = configBean.getIntervalDay();
		// 是否手动或自动标识：0：手动；1：自动
		String iaAuto = configBean.getIaAuto();

		// hh:mm
		if (SyncTool.isNullOrEmtryTrim(startTime) || !startTime.matches("^(0\\d{1}|1\\d{1}|2[0-3]):([0-5]\\d{1})$")) {
			// 拼装返回页面
			callBackPage(SyncErrorInfo.START_TIME_FORMAT_FAIL, "false");
			return null;
		}

		if (SyncTool.isNullOrEmtryTrim(intervalDay) || !intervalDay.matches("^[1-9]\\d*$")) {
			// 拼装返回页面
			ResultHtml(SyncErrorInfo.INTERVAL_DAY_FORMAT_FAIL, null);
			return null;
		}

		if (SyncTool.isNullOrEmtryTrim(iaAuto) || !iaAuto.matches("^[01]$")) {
			// 拼装返回页面
			callBackPage(SyncErrorInfo.AUTO_FORMAT_FAIL, "false");
			return null;
		}

		// 保存到配置文件
		boolean ret = ReadUserConfigXml.writerConfigXml(configBean);
		if (!ret) {
			// 拼装返回页面
			callBackPage(SyncErrorInfo.SAVE_CONFIG_XML_FAIL, "false");
			return null;
		}
		return SyncConstant.RESULT_SUCCESS;
	}

	/**
	 * 
	 * 初始化界面获取页面所需值
	 * 
	 * @param importType
	 *            前台必须传值：导入方式（excel、xml、hessian,db），默认是excel导入
	 */
	public String initParams(String importType) {
		// 获取外来的待导入数据
		try {
			abstractConfigBean = importDataFactory.getInitTime(importType);

			return SyncConstant.RESULT_SUCCESS;
		} catch (Exception e) {
			log.error("UserSyncAction类initParams方法：", e);

			// 拼装返回页面
			callBackPage(SyncErrorInfo.GET_CONFIG_FILE_FAIL, "false");
			return null;
		}
	}

	/**
	 * 
	 * 重新开始定时监听
	 * 
	 */
	public void reStartTime() {
		if (userAutoBuss == null) {
			return;
		}
		// 停止定时
		userAutoBuss.timerStop();
		// 开始监听
		initParams(getImportTypeByData());
		userAutoBuss.setTimerAction(this);
		userAutoBuss.setAutoBean(abstractConfigBean);
		userAutoBuss.timerStart();

	}

	/**
	 * 根据DB标识获取人员同步类型
	 * 
	 * @return
	 */
	private String getImportTypeByData() {
		int tmpValue = JecnContants.otherUserSyncType;
		switch (tmpValue) {
		case 2:// DB
			importType = SyncConstant.IMPORT_TYPE_DB;
			break;
		case 4:// webService
			importType = SyncConstant.IMPORT_TYPE_WEBSERVICE;
			break;
		case 5:// sapHr
			importType = SyncConstant.IMPORT_TYPE_SAP_HR;
			break;
		case 6:// XML+岗位匹配 【大唐】
			importType = SyncConstant.IMPORT_TYPE_XML;
			break;
		case 7:// ad（Ldap数据读取）
			importType = SyncConstant.IMPORT_TYPE_AD;
			break;
		case 8:// DB 附带基准岗位
			importType = SyncConstant.IMPORT_TYPE_BASE_DB;
			break;
		}
		return importType;
	}

	public String getStringResult() {
		return stringResult;
	}

	public void setStringResult(String stringResult) {
		this.stringResult = stringResult;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public UserImportBuss getUserImportBuss() {
		return userImportBuss;
	}

	public void setUserImportBuss(UserImportBuss userImportBuss) {
		this.userImportBuss = userImportBuss;
	}

	public ImportDataFactory getImportDataFactory() {
		return importDataFactory;
	}

	public void setImportDataFactory(ImportDataFactory importDataFactory) {
		this.importDataFactory = importDataFactory;
	}

	public AbstractConfigBean getAbstractConfigBean() {
		return abstractConfigBean;
	}

	public void setAbstractConfigBean(AbstractConfigBean abstractConfigBean) {
		this.abstractConfigBean = abstractConfigBean;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

}
