package epros.designer.gui.system;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import epros.designer.gui.common.JecnTextField;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

public class ExpiryCommon implements BasicComponent {

	/** 有效期显示值 */
	private String expiry = null;
	/** 有效期Lab */
	private JLabel validityLab = new JLabel(JecnProperties.getValue("validMonthC"));
	/** 有效期Field */
	private JTextField validityField = new JecnTextField();
	/** 是否永久有效复选框 */
	private JCheckBox validityCheckBox = new JCheckBox(JecnProperties.getValue("foreverC"));

	/**
	 * 
	 * @param expiry
	 * @param rows
	 * @param infoPanel
	 * @param insets
	 * @param nodeType
	 *            0是流程 1是文件 2是制度
	 */
	public ExpiryCommon(String expiry, int rows, JecnPanel infoPanel, Insets insets, int nodeType) {
		validityCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 根据配置 禁用 有效期选择和输入
		if (nodeType == 0) { //流程
			validityCheckBox.setEnabled(JecnConfigTool.getResultValue("isShowProcessValidity") == 1);
			validityField.setEnabled(JecnConfigTool.getResultValue("isShowProcessValidity") == 1);
		} else if (nodeType == 1) {//制度
			validityCheckBox.setEnabled(JecnConfigTool.getResultValue("isShowRuleValidity") == 1);
			validityField.setEnabled(JecnConfigTool.getResultValue("isShowRuleValidity") == 1);
		}
		// 有效期
		this.expiry = expiry;
		if (expiry == null || "".equals(expiry)) {
			if (nodeType == 0) {
				expiry = JecnConfigTool.processValidityAllocation();
			} else if (nodeType == 1) {
				expiry = JecnConfigTool.ruleValidityAllocation();
			}
		}
		if (expiry == null || "".equals(expiry)) {
			expiry = "0";
		}
		if (expiry.equals("0")) {
			validityCheckBox.setSelected(true);
			validityField.setEditable(false);
		} else {
			validityCheckBox.setSelected(false);
			validityField.setEditable(true);
			validityField.setText(expiry);
		}
		// 有效期 validityLab
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(validityLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(validityField, c);
		// 是否永久有效validityCheckBox
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(validityCheckBox, c);
		c = new GridBagConstraints(4, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(new CommonJlabelMustWrite(), c);
		// 有效期永久性
		validityCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (validityCheckBox.isSelected()) {
					validityField.setEditable(false);
					validityField.setText("");
				} else {
					validityField.setEditable(true);
				}
			}
		});

	}

	public int getValueResult() {
		String newExpiry = "";
		if (this.validityCheckBox.isSelected()) {
			newExpiry = "0";
		} else {
			newExpiry = this.validityField.getText().trim();
		}
		if (!"".equals(newExpiry)) {
			return Integer.valueOf(newExpiry);
		}
		return 0;
	}

	@Override
	public boolean isUpdate() {
		// 有效期
		String newExpiry = "";
		if (this.validityCheckBox.isSelected()) {
			newExpiry = "0";
		} else {
			newExpiry = this.validityField.getText().trim();
		}
		if (!this.expiry.equals(newExpiry)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		if (this.validityCheckBox.isSelected()) {
			return false;
		}
		// 获取有效期
		String reg = "^[0-9]*$";
		String strValidity = this.validityField.getText().trim();
		// 有效期 值
		if (strValidity == null || "".equals(strValidity.trim())) {
			verfyLab.setText(JecnProperties.getValue("validNotNullC"));
			return true;
		} else if (!strValidity.matches(reg)) {
			verfyLab.setText(JecnProperties.getValue("integerGreaterThan"));
			return true;
		} else if (strValidity.length() > 5 || Integer.parseInt(strValidity) > 9000) {// /如果输入的有效期大于9000则提示重新输入
			verfyLab.setText(JecnProperties.getValue("validSuperfluousMonthC"));
			return true;
		} else if (Integer.parseInt(strValidity) <= 0) {
			verfyLab.setText(JecnProperties.getValue("validSuperfluousZeroC"));
			return true;
		}
		return false;
	}

	@Override
	public boolean isRequest() {
		return false;
	}

	@Override
	public void setEnabled(boolean enable) {

	}
}
