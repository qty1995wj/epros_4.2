package com.jecn.epros.security;

import java.io.Serializable;

/**
 * 普通登录数据Bean
 */
public class JwtAuthenticationRequest implements Serializable {

    private static final long serialVersionUID = -8445943548965154778L;

    private String username;
    private String password;
    /**
     * 验证码
     */
    private String verification;
    private boolean needVer;

    private int languageType;

    /**
     * 加密的验证码
     */
    private String encodeVerify;

    public JwtAuthenticationRequest() {
        super();
    }

    public JwtAuthenticationRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLanguageType() {
        return languageType;
    }

    public void setLanguageType(int languageType) {
        this.languageType = languageType;
    }

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    public boolean isNeedVer() {
        return needVer;
    }

    public void setNeedVer(boolean needVer) {
        this.needVer = needVer;
    }

    public String getEncodeVerify() {
        return encodeVerify;
    }

    public void setEncodeVerify(String encodeVerify) {
        this.encodeVerify = encodeVerify;
    }
}
