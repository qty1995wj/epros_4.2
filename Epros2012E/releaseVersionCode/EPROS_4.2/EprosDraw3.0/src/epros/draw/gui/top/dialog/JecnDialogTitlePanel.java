package epros.draw.gui.top.dialog;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 对话框标题栏面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDialogTitlePanel extends JecnDialogDropTitlePanel {
	/** Epros对话框标题 */
	private JLabel titleLabel = null;
	/** 标题内容 */
	private String title = "";
	/** 标题图片 */
	private Image icon;

	/** 最大化按钮 */
	private JButton resizeBtn = null;
	/** 关闭按钮 */
	private JButton exitBtn = null;

	/** 空闲面板 */
	private JPanel otherPanel = null;


	public JecnDialogTitlePanel(JecnDialog dialog) {
		super(dialog);
		// 实例化子组件
		initComponents();
		// 布局子组件
		layoutComponents();
		initMouseEvent();
	}

	/**
	 * 
	 * 实例化子组件
	 * 
	 */
	private void initComponents() {

		// **********************图片加载**********************//
		// 窗体默认图标
		icon = getDefaultImage();
		// **********************图片加载**********************//

		// Epros对话框标题
		titleLabel = new JLabel();

		// 最大化按钮
		resizeBtn = new JButton(JecnFileUtil.getFrameBtnIcon("resizeIcon.gif"));
		// 关闭按钮
		exitBtn = new JButton(JecnFileUtil.getFrameBtnIcon("exitFrameBtn.jpg"));
		// 空闲面板
		otherPanel = new JPanel();

		// 大小
		Dimension size = new Dimension(135, 25);
		this.setPreferredSize(size);

		// 设置布局管理器
		this.setLayout(new GridBagLayout());

		// 前景颜色
		titleLabel.setFont(new Font(null, Font.BOLD, 12));
		titleLabel.setHorizontalTextPosition(SwingConstants.RIGHT);
		titleLabel.setVerticalTextPosition(SwingConstants.CENTER);

		// 透明
		otherPanel.setOpaque(false);

		// 名称
		resizeBtn.setName("maxBtn");
		exitBtn.setName("exitBtn");

		// 透明
		titleLabel.setOpaque(false);
		// 不让绘制内容区域
		resizeBtn.setContentAreaFilled(false);
		exitBtn.setContentAreaFilled(false);

		// 设置按钮边框和标签之间的空白
		resizeBtn.setMargin(JecnUIUtil.getInsets0());
		exitBtn.setMargin(JecnUIUtil.getInsets0());

		// 隐藏边框
		resizeBtn.setBorder(null);
		exitBtn.setBorder(null);
		resizeBtn.setBorderPainted(false);
		exitBtn.setBorderPainted(false);

		// 隐藏选择焦点
		resizeBtn.setFocusable(false);
		exitBtn.setFocusable(false);

		// 事件
		resizeBtn.addActionListener(this);
		exitBtn.addActionListener(this);
	}

	/**
	 * 
	 * 初始化事件
	 * 
	 */
	private void initMouseEvent() {
		// 最大化按钮
		resizeBtn.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				if (!isMax) {// 不是全屏
					resizeBtn.setIcon(JecnDialogIcon.enterResizeIcon);
				} else {
					resizeBtn.setIcon(JecnDialogIcon.enterResizeMaxIcon);
				}
			}

			public void mouseExited(MouseEvent e) {
				if (!isMax) {// 不是全屏
					resizeBtn.setIcon(JecnDialogIcon.resizeIcon);
				} else {
					resizeBtn.setIcon(JecnDialogIcon.resizeMaxIcon);
				}
			}
		});

		// 关闭按钮
		exitBtn.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				exitBtn.setIcon(JecnDialogIcon.enterExitIcon);
			}

			public void mouseExited(MouseEvent e) {
				exitBtn.setIcon(JecnDialogIcon.exitIcon);
			}
		});
	}

	/**
	 * 
	 * 布局子组件
	 * 
	 */
	private void layoutComponents() {

		// Epros对话框标题
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
						.getInsets3(), 0, 0);
		this.add(titleLabel, c);

		// 空闲面板
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(otherPanel, c);

		// 最大化按钮
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(resizeBtn, c);

		// 关闭按钮
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(exitBtn, c);
	}

	/**
	 * 
	 * 添加标题
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
		this.titleLabel.setText(title);
		this.repaint();
	}

	public String getTitle() {
		return title;
	}

	public Image getIcon() {
		if (icon == null) {
			return getDefaultImage();
		}
		return icon;
	}

	/**
	 * 
	 * 对话框图标
	 * 
	 * @param iconImage
	 */
	public void setIcon(Image iconImage) {
		this.icon = iconImage;
		if (iconImage != null) {
			titleLabel.setIcon(new ImageIcon(iconImage));
			this.validate();
			this.repaint();
		}
	}

	public boolean isResizable() {
		return resizable;
	}

	/**
	 * 
	 * 隐藏按钮
	 * 
	 */
	public void hiddBtns() {
		this.resizeBtn.setVisible(false);
		this.exitBtn.setVisible(false);
	}

	/**
	 * 
	 * 
	 * 设置是否支持用户调整大小
	 * 
	 * @param resizable
	 */
	public void setResizable(boolean resizable) {
		this.resizable = resizable;
		resizeBtn.setVisible(resizable);
	}

	public JButton getResizeBtn() {
		return resizeBtn;
	}

	/**
	 * 
	 * 关闭按钮隐藏
	 * 
	 */
	void setExitBtnVisible(boolean b) {
		this.exitBtn.setVisible(b);
	}

	/**
	 * 
	 * 返回默认图标
	 * 
	 * @return
	 */
	private Image getDefaultImage() {
		return JecnFileUtil.getFrameBtnIcon("topicom.png").getImage();
	}

}
