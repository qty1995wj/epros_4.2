package epros.designer.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.recycle.RecyclePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 回收站TABLE
 * 
 * @author user
 * 
 */
public class JecnRecyleTable extends JTable {

	private RecyclePanel recyclePanel;

	public JecnRecyleTable(RecyclePanel recyclePanel) {
		this.recyclePanel = recyclePanel;
		this.refrehTable(new ArrayList<JecnTreeBean>());
	}

	/**
	 * 获得Table数据
	 * 
	 * @return
	 */
	private Object[][] getTableData(List<JecnTreeBean> dataList) {
		Object[][] obj = new Object[dataList.size()][6];
		for (int i = 0; i < dataList.size(); i++) {
			JecnTreeBean jecnTreeBean = dataList.get(i);
			if (jecnTreeBean == null || jecnTreeBean.getId() == null
					|| jecnTreeBean.getPid() == null
					|| jecnTreeBean.getName() == null) {
				continue;
			}
			obj[i][0] = new JCheckBox();
			// 主键ID
			obj[i][1] = jecnTreeBean.getName();
			// 文件类型
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.processMap) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.processMap.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.processMap.toString();
			} else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.process) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.process.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.process.toString();
			} else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.ruleDir) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.ruleDir.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.ruleDir.toString();
			} else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.ruleFile) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.ruleFile.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.ruleFile.toString();
			} else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.ruleModeFile) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.ruleModeFile.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.ruleModeFile.toString();
			}else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.file) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.file.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.file.toString();
			} else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.fileDir) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.fileDir.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.fileDir.toString();
			} else if (jecnTreeBean.getTreeNodeType() == TreeNodeType.organization) {
				obj[i][2] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.organization.toString() + ".gif");
				// 类型
				obj[i][5] = TreeNodeType.organization.toString();
			} else {
				obj[i][2] = "";
				obj[i][5] = "";
			}
			// 主键ID
			obj[i][3] = jecnTreeBean.getId().toString();
			// 父类主键ID
			obj[i][4] = jecnTreeBean.getPid().toString();

		}
		return obj;
	}

	/**
	 * 搜索Table
	 * 
	 * @param dataList
	 */
	public void refrehTable(List<JecnTreeBean> dataList) {
		this.setModel(new JecnRecyleTableMode(getTableData(dataList), this
				.getTableTitle()));
		TableColumnModel tableColumnModel = this.getColumnModel();
		TableColumn column0 = tableColumnModel.getColumn(0);
		column0.setCellEditor(new RecycleCheckButtonEditor(new JCheckBox(),
				recyclePanel));
		column0.setCellRenderer(new CheckBoxRenderer());
		column0.setMaxWidth(18);

		// // 名称
		// TableColumn column1 = tableColumnModel.getColumn(2);
		// column1.setMaxWidth(200);
		// column1.setMinWidth(200);

		// 类型图片
		TableColumn column2 = tableColumnModel.getColumn(2);
		column2.setMaxWidth(80);
		column2.setMinWidth(80);

		// 主键ID
		TableColumn column3 = tableColumnModel.getColumn(3);
		column3.setMaxWidth(0);
		column3.setMinWidth(0);
		column3.setPreferredWidth(0);

		// 父类主键ID
		TableColumn column4 = tableColumnModel.getColumn(4);
		column4.setMaxWidth(0);
		column4.setMinWidth(0);
		column4.setPreferredWidth(0);

		// 类型
		TableColumn column5 = tableColumnModel.getColumn(5);
		column5.setMaxWidth(0);
		column5.setMinWidth(0);
		column5.setPreferredWidth(0);

		this.getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
		this.updateUI();
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.recyclePanel.getSelectAllBox().setSelected(false);
		if (dataList.size() == 0) {
			this.recyclePanel.getSelectAllBox().setEnabled(false);
		} else {
			this.recyclePanel.getSelectAllBox().setEnabled(true);
		}
	}

	/**
	 * 获得Table的表头
	 * 
	 * @return
	 */
	private Object[] getTableTitle() {
		Object[] title = new Object[6];
		title[0] = ("");
		title[1] = JecnProperties.getValue("name");
		title[2] = JecnProperties.getValue("type");
		title[3] = ("");
		title[4] = ("");
		title[5] = ("");
		return title;
	}
}
