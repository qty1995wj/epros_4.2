package com.jecn.epros.server.dao.autoCode;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.autoCode.ProcessCodeTree;
import com.jecn.epros.server.common.IBaseDao;

public interface IAutoCodeDao extends IBaseDao<AutoCodeBean, String> {

	void deleteProcessCode(List<Long> ids) throws Exception;

	void updateName(Long id, String newName, Long updatePersonId) throws Exception;

	Long addPorcessCode(ProcessCodeTree codeTree) throws Exception;

	List<Object[]> getChildProcessCodes(Long pId, int treeType) throws Exception;

	List<Object[]> getTreeBeanByName(String name) throws Exception;

	List<Object[]> getPnodes(Set<Long> set, int treeType) throws Exception;

	String getCodePath(Long id) throws Exception;

	void updateAutoCodeTableByRelatedId(Long relatedId, int relatedType, int codeTotal) throws Exception;

	int updateStandardizedAutoCode(Long relatedId, int relatedType, int codeTotal) throws Exception;
}
