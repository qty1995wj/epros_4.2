package com.jecn.epros.service;

import com.jecn.epros.domain.process.TreeNode;

import java.util.Iterator;
import java.util.List;

public class TreeNodeUtils {

    public enum TreeType {
        process, rule, standard, risk, file, organization, position, people, auth
    }

    public enum NodeType {
        process, // 流程
        processFile,// 流程（文件）
        processMap, // 流程地图
        ruleDir, ruleModelFile, ruleFile,rule, // 制度
        standardDir, standard, standardProcess, standardProcessMap, standardClauseRequire, standardClause, // 标准,
        riskDir, risk, // 风险
        fileDir, file, // 文件
        organization, position, people,// 组织、岗位、人员
        auth, //权限
        task,
        inventory,
        search,
        fileContent,// 文件内容
        processFileContent,// 流程（文件）内容
        ruleFileContent,// 制度文件内容
        star,// 收藏
        main, //主页
        activity,//活动
        Abolish//废止
    }

    public static void setNodeType(TreeNode node, TreeType treeType) {
        if (node == null) {
            return;
        }
        if (treeType == null) {
            treeType = TreeType.process;
        }
        switch (treeType) {
            case process:
                setNodeTypeByTreeType(node, treeType);
                break;
            case rule:
                setNodeTypeByTreeType(node, treeType);
                break;
            case standard:
                setNodeTypeByTreeType(node, treeType);
                break;
            case risk:
                setNodeType(node, NodeType.riskDir, NodeType.risk);
                break;
            case file:
                setNodeType(node, NodeType.fileDir, NodeType.file);
                break;
            case position:
            case organization:
                setNodeType(node, NodeType.organization, NodeType.position);
                break;
            case people:
                setNodeTypeByTreeType(node, treeType);
                break;
            default:
                break;
        }
    }

    private static void setNodeTypeByTreeType(TreeNode node, TreeType treeType) {
        int type = node.getIsDir();
        NodeType nodeType = null;
        if (treeType == TreeType.rule) {
//            0：目录 1：制度模板文件 2：制度文件
            if (type == 0) {
                nodeType = NodeType.ruleDir;
            } else if (type == 1) {
                nodeType = NodeType.ruleModelFile;
            } else if (type == 2) {
                nodeType = NodeType.ruleFile;
            }

        } else if (treeType == TreeType.standard) {
//            0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
            if (type == 0) {
                nodeType = NodeType.standardDir;
            } else if (type == 1) {
                nodeType = NodeType.standard;
            } else if (type == 2) {
                nodeType = NodeType.standardProcess;
            } else if (type == 3) {
                nodeType = NodeType.standardProcessMap;
            } else if (type == 4) {
                nodeType = NodeType.standardClause;
            } else if (type == 5) {
                nodeType = NodeType.standardClauseRequire;
            }
        } else if (treeType == TreeType.people) {
            // 0部门 1岗位 2人员
            if (type == 0) {
                nodeType = NodeType.organization;
            } else if (type == 1) {
                nodeType = NodeType.position;
            } else if (type == 2) {
                nodeType = NodeType.people;
            }
        } else if (treeType == TreeType.process) {
            if (type == 1) {// 流程节点
                if (node.getNodeType() == 1) {//processFile节点
                    nodeType = NodeType.processFile;
                } else {
                    nodeType = NodeType.process;
                }
            } else {
                nodeType = NodeType.processMap;
            }
        }
        node.setType(nodeType);
    }

    private static void setNodeType(TreeNode node, NodeType dirType, NodeType baseType) {
        if (node.getIsDir() == 0) {
            node.setType(dirType);
        } else {
            node.setType(baseType);
        }
    }

    public static List<TreeNode> setNodeType(List<TreeNode> nodes, TreeType type) {
        if (nodes == null) {
            return nodes;
        }
        Iterator<TreeNode> iterator = nodes.iterator();
        while (iterator.hasNext()) {
            TreeNode next = iterator.next();
            setNodeType(next, type);
        }
        return nodes;
    }

    public static List<TreeNode> setNodeType(List<TreeNode> nodes, NodeType type) {
        if (nodes == null) {
            return nodes;
        }
        Iterator<TreeNode> iterator = nodes.iterator();
        while (iterator.hasNext()) {
            TreeNode next = iterator.next();
            next.setType(type);
        }
        return nodes;
    }

}
