package com.jecn.epros.domain.journal;

import com.jecn.epros.domain.process.SearchBean;

/**
 * Created by lenovo on 2017/1/5.
 */
public class SearchWebLogBean extends SearchBean {
    /**是否查阅 0是没有查阅，1是查阅，-1是全部*/
    private int viewType=0;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
