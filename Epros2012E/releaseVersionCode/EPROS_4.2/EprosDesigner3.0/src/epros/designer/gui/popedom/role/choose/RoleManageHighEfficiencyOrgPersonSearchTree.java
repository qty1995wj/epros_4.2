package epros.designer.gui.popedom.role.choose;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * @author yxw 2012-6-14
 * @description：岗位管理 选择组织查询
 */
public class RoleManageHighEfficiencyOrgPersonSearchTree extends JecnHighEfficiencyTree {
	protected static Logger log = Logger.getLogger(RoleManageHighEfficiencyOrgPersonSearchTree.class);
	protected List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleManageOrgPersonSearchTreeListener(jTree);
	}

	public RoleManageHighEfficiencyOrgPersonSearchTree(Long startId) {
		super(startId);
	}

	public RoleManageHighEfficiencyOrgPersonSearchTree() {

	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.organizationRoot, JecnProperties
				.getValue("dept"));
		try {
			list = ConnectionPool.getOrganizationAction().getRoleAuthChildFiles(0L, JecnConstants.projectId,
					JecnConstants.getUserId());
			JecnTreeCommon.addNLevelNodes(list, rootNode);
		} catch (Exception e) {
			log.error("RoleManageHighEfficiencyOrgPersonSearchTree getTreeModel is error！", e);
		}
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {

	}

}
