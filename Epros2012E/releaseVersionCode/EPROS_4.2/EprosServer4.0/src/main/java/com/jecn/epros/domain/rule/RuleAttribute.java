package com.jecn.epros.domain.rule;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.process.ProcessAttribute;

import java.util.List;

/**
 * Created by admin on 2017/8/18.
 */
public class RuleAttribute extends ProcessAttribute {
    private List<LinkResource> links;

    private String hrefURL;

    public RuleAttribute() {
    }

    public RuleAttribute(String title, String text, LinkResource link, List<LinkResource> links) {
        super(title, text, link);
        this.links = links;
    }

    public List<LinkResource> getLinks() {
        return links;
    }

    public void setLinks(List<LinkResource> links) {
        this.links = links;
    }

    @Override
    public String getHrefURL() {
        return hrefURL;
    }

    public void setHrefURL(String hrefURL) {
        this.hrefURL = hrefURL;
    }
}
