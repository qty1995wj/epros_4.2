package epros.designer.gui.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableCommon;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * @author yxw 2012-10-22
 * @description：数据选择公共界面，如部门查阅权限选择
 */
public abstract class JecnSelectChooseDialog extends JecnDialog {
	/** 是否支持多选择 true:支持多选 false单选 */
	protected boolean selectMutil = true;
	protected boolean isShowBut = false;
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 树滚动面板+快速查询面板的容器· */
	private JecnSplitPane splitPane = null;

	/** 树面板 */
	private JScrollPane treePanel = null;
	/** 右侧面板 */
	protected JPanel centerPanel = null;

	/** 搜索面板 */
	protected JPanel searchPanel = null;
	/** 结果面板 */
	private JPanel resultPanel = null;

	/** 搜索查询输入区域面板 */
	protected JPanel searchInputPanel = null;
	/** 岗位区域面板 */
	protected JPanel searchPostPanel = null;
	/** 搜索结果滚动面板 */
	protected JScrollPane searchScrollPane = null;
	/** 已选结果滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 批量添加查询输入区域面板 */
	protected JPanel addAllPanel = null;
	/** 添加 */
	private JButton addBtn = new JButton(JecnProperties.getValue("add"));
	/** 批量添加 */
	private JButton addAllBtn = new JButton(JecnProperties.getValue("addAll"));

	/** 搜索名称 */
	private JLabel searchLabel = null;
	/** 搜索内容 */
	protected JTextField searchText = null;
	/** 按钮面板，上面放四个按钮 */
	protected JPanel buttonsPanel = null;
	/** 提示信息 */
	private JLabel jLabelTip = null;
	/** 删除 */
	protected JButton deleteBtn = null;
	/** 确定 */
	private JButton okBtn = null;
	/** 清空 */
	protected JButton emptyBtn = null;
	/** 取消 */
	protected JButton cancelBtn = null;

	/** 搜索table */
	protected JecnTable searchTable = null;
	/** 结果table */
	protected JecnTable resultTable = null;

	/** 是否操作 true是操作，false是没有 */
	protected boolean isOperation = false;
	/** Tree 对象 */
	protected JecnTree jTree = null;
	/** 保存原始结果集，当点现取消时，恢复结果集 */
	private List<JecnTreeBean> saveOldSelectList = new ArrayList<JecnTreeBean>();
	/** 结果集 */
	private List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
	/** 搜索集 */
	protected List<JecnTreeBean> searchListJecnTreeBean = new ArrayList<JecnTreeBean>();
	/**
	 * 选择类型 0部门选择1岗位选择2文件选择3流程选择4标准选择5制度选择6角色选择7人员选择 8岗位组选择，9支持工具选择 10流程地图模板选择11
	 * 流程图模板12 制度模板选择 13只选流程14文件选择(目录和文件均可选择)15标准选择(目录和文件均可选择)
	 * 16制度选择(目录和文件均可选择)17只能选图片，18：只能选择文件目录，32角色 术语定义
	 */
	private int chooseType = 0;

	/** 树加载开始的id **/
	protected Long startId;
	/** 表格行高 */
	protected int oldRowHeight;

	/**
	 * 
	 * @param selectNode
	 * @param list
	 * @param chooseType
	 *            选择类型 0部门选择1岗位选择2文件选择3流程选择4标准选择5制度选择6角色选择7人员选择 8岗位组选择，9支持工具选择
	 *            10流程地图模板选择11 流程图模板12 制度模板选择 13只选流程14文件选择(目录和文件均可选择)
	 *            15标准选择(目录和文件均可选择 )16制度选择(目录和文件均可选择)17只能选图片
	 */
	public JecnSelectChooseDialog(List<JecnTreeBean> list, int chooseType, JecnDialog jecnDialog) {
		super(jecnDialog, true);

		this.listJecnTreeBean = list;
		this.chooseType = chooseType;

		initCompotents();
		initLayout();
		initEvent();
		initData();

		this.setLocationRelativeTo(jecnDialog);
	}

	/**
	 * 
	 * @param selectNode
	 * @param list
	 * @param chooseType
	 *            选择类型 0部门选择1岗位选择2文件选择3流程选择4标准选择5制度选择6角色选择7人员选择 8岗位组选择，9支持工具选择
	 *            10流程地图模板选择11 流程图模板12 制度模板选择 13只选流程14文件选择(目录和文件均可选择)
	 *            15标准选择(目录和文件均可选择 )16制度选择(目录和文件均可选择)17只能选图片
	 */
	public JecnSelectChooseDialog(List<JecnTreeBean> list, int chooseType, JecnDialog jecnDialog, boolean isShowBut) {
		super(jecnDialog, true);
		this.isShowBut = isShowBut;
		this.listJecnTreeBean = list;
		this.chooseType = chooseType;

		initCompotents();
		initLayout();
		initEvent();
		initData();

		this.setLocationRelativeTo(jecnDialog);
	}

	/**
	 * 
	 * @param selectNode
	 * @param list
	 * @param chooseType
	 *            选择类型 0部门选择1岗位选择2文件选择3流程选择4标准选择(不能选择流程图和流程地图)5制度选择6角色选择7人员选择
	 *            8岗位组选择，9支持工具选择 10流程模板选择11只能选图片12 制度模板选择
	 *            13只选流程15标准选择(目录和文件均可选择)16制度选择(目录和文件均可选择
	 *            )17只能选图片18流程模板选择(流程图模板和流程图地图模板均可选择)19流程模板选择(流程图模板和流程图地图模板均可选择)
	 *            20：只能选择标准文件 21:只能选择风险
	 *            22:只能选择内控指引知识库条款;23:文件夹选择；24：二级管理员流程选择;41 :只选择流程架构
	 */
	public JecnSelectChooseDialog(List<JecnTreeBean> list, int chooseType) {
		this.listJecnTreeBean = list;
		this.chooseType = chooseType;

		initCompotents();
		initLayout();
		initEvent();
		initData();

		this.setLocationRelativeTo(JecnSystemStaticData.getFrame());
	}

	/**
	 * 
	 * @param selectNode
	 * @param list
	 * @param chooseType
	 *            选择类型 0部门选择1岗位选择2文件选择3流程选择4标准选择(不能选择流程图和流程地图)5制度选择6角色选择7人员选择
	 *            8岗位组选择，9支持工具选择 10流程模板选择11只能选图片12 制度模板选择
	 *            13只选流程15标准选择(目录和文件均可选择)16制度选择(目录和文件均可选择
	 *            )17只能选图片18流程模板选择(流程图模板和流程图地图模板均可选择)19流程模板选择(流程图模板和流程图地图模板均可选择)
	 *            20：只能选择标准文件 21:只能选择风险 22:只能选择内控指引知识库条款;23:文件夹选择；24：二级管理员流程选择
	 */
	public JecnSelectChooseDialog(List<JecnTreeBean> list, int chooseType, boolean isShowBut) {
		this.listJecnTreeBean = list;
		this.isShowBut = isShowBut;
		this.chooseType = chooseType;

		initCompotents();
		initLayout();
		initEvent();
		initData();

		this.setLocationRelativeTo(JecnSystemStaticData.getFrame());
	}

	public JecnSelectChooseDialog(List<JecnTreeBean> list, int chooseType, JecnDialog jecnDialog, Long startId) {
		super(jecnDialog, true);

		this.listJecnTreeBean = list;
		this.chooseType = chooseType;
		this.startId = startId;

		initCompotents();
		initLayout();
		initEvent();
		initData();

		this.setLocationRelativeTo(jecnDialog);
	}

	public JecnSelectChooseDialog(List<JecnTreeBean> list, int chooseType, Long permOrgId) {
		this.listJecnTreeBean = list;
		this.chooseType = chooseType;
		this.startId = permOrgId;

		initCompotents();
		initLayout();
		initEvent();
		initData();
		this.setLocationRelativeTo(JecnSystemStaticData.getFrame());
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();
		// 树面板
		treePanel = new JScrollPane();
		// 右侧面板
		centerPanel = new JPanel();
		// 树滚动面板+快速查询面板的容器
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, mainPanel, treePanel, centerPanel);

		// Tree 对象
		jTree = getJecnTree();

		// 搜索面板
		searchPanel = new JPanel();
		addAllPanel = new JPanel();
		// 搜索查询输入区域面板
		searchInputPanel = new JPanel();
		// 岗位区域面板
		searchPostPanel = new JPanel();
		// 初始化搜索标签名称
		searchLabel = new JLabel(getSearchLabelName());
		// 搜索输入库
		searchText = new JTextField(60);
		// 搜索结果滚动面板设置大小
		searchScrollPane = new JScrollPane();
		// 搜索表
		searchTable = new SearchTable();

		// 已选结果滚动面板设置大小
		resultScrollPane = new JScrollPane();
		// 结果面板
		resultPanel = new JPanel();
		// 结果表
		resultTable = new ResultTable();

		// 按钮面板
		buttonsPanel = new JPanel();
		jLabelTip = new JLabel();
		jLabelTip.setForeground(Color.red);
		deleteBtn = new JButton(JecnProperties.getValue("deleteBtn"));
		okBtn = new JButton(JecnProperties.getValue("okBtn"));
		emptyBtn = new JButton(JecnProperties.getValue("emptyBtn"));
		cancelBtn = new JButton(JecnProperties.getValue("cancelBtn"));

		this.setSize(710, 510);
		this.setModal(true);

		mainPanel.setLayout(new GridBagLayout());

		// 左:右=3:7
		splitPane.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPane.setResizeWeight(0.0);
		splitPane.showBothPanel();

		Dimension dimension = new Dimension(200, 430);
		treePanel.setPreferredSize(dimension);
		treePanel.setMinimumSize(dimension);

		// 主面板布局-开始
		mainPanel.setLayout(new BorderLayout());
		// 快速查询面板
		// 搜索面板布局,流布局 靠左
		searchPanel.setLayout(new BorderLayout(0, 2));
		addAllPanel.setLayout(new FlowLayout());
		// 搜索查询输入区域面板
		searchInputPanel.setLayout(new BorderLayout());
		searchPostPanel.setLayout(new FlowLayout());
		// 结果面板布局-开始
		resultPanel.setLayout(new BorderLayout());
		// 按钮面板
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		// 边框
		// 树滚动面板
		treePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("pleaseSelect")));
		// 快速查询面板
		searchPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("quickSearch")));
		addAllPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), ""));
		// 结果面板
		resultPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("result")));

		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		searchPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		addAllPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		searchScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		searchScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		searchTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		mainPanel.setBorder(null);
		centerPanel.setBorder(null);

		searchInputPanel.setOpaque(false);
		searchInputPanel.setBorder(null);

		searchPostPanel.setOpaque(false);
		searchPostPanel.setBorder(null);

		// 获取table原始行高
		oldRowHeight = searchTable.getRowHeight();

		setComponentVisible();
	}

	private void setComponentVisible() {
		// 左侧树是否显示标示
		treePanel.setVisible(treePanelShow());
	}

	private boolean treePanelShow() {
		// 科大讯飞类型为人员的情况下 隐藏树面板
		if (JecnConfigTool.isIflytekLogin() && (chooseType == 7 || chooseType == 30)) {
			return false;
		}
		if (JecnConfigTool.isHiddenSelectTreePanel() && (chooseType == 7 || chooseType == 1 || chooseType == 0)) {
			return false;
		}
		return true;
	}

	/***
	 * 批量添加面板
	 * 
	 * @param c
	 * @param insets
	 */
	protected void addPanel(GridBagConstraints c, Insets insets) {

	}

	/**
	 * @author yxw 2012-5-4
	 * @description:dialog布局方法
	 */
	private void initLayout() {
		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);

		// **************第一层布局**************//
		// 内容区
		mainPanel.add(splitPane, BorderLayout.CENTER);
		// 关闭按钮
		mainPanel.add(buttonsPanel, BorderLayout.SOUTH);
		// **************第一层布局**************//

		// **************树面板布局**************//
		treePanel.setViewportView(jTree);
		// **************树面板布局**************//

		// **************右面第一层布局**************//
		Insets insets = new Insets(2, 2, 2, 2);
		centerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH,
				GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(searchPanel, c);
		if (isShowBut) {// 是否添加 批量添加组件
			addPanel(c, insets);
		}
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0,
				0);
		centerPanel.add(resultPanel, c);
		// **************右面第一层布局**************//

		// **************快速查询布局**************//
		// 搜索名称
		searchPostPanel.add(searchLabel);
		// 搜索内容
		searchPostPanel.add(searchText);
		// 搜索按钮(暂时只有科大讯飞有 因为他的人员搜索只要精确查询)
		showSearchButton(searchPostPanel);

		searchInputPanel.add(searchPostPanel, BorderLayout.NORTH);

		departmentAndPeopleComponents();// 岗位添加 部门人员输入框
		searchPanel.add(searchInputPanel, BorderLayout.NORTH);
		searchPanel.add(searchScrollPane, BorderLayout.CENTER);

		searchScrollPane.setViewportView(searchTable);
		// **************快速查询布局**************//
		// ***************批量添加布局********************//
		addAllPanel.add(addBtn);
		addAllPanel.add(addAllBtn);
		// *****************************************//
		// **************结果布局**************//
		resultPanel.add(resultScrollPane);
		resultScrollPane.setViewportView(resultTable);
		// **************结果布局**************//

		// **************按钮面板布局**************//
		addButton();
		// **************按钮面板布局**************//
	}

	protected void addButton() {
		buttonsPanel.add(jLabelTip);
		buttonsPanel.add(deleteBtn);
		buttonsPanel.add(emptyBtn);
		buttonsPanel.add(okBtn);
		buttonsPanel.add(cancelBtn);
	}

	protected void showSearchButton(JPanel searchPostPanel) {

	}

	protected void departmentAndPeopleComponents() {

	}

	/**
	 * 
	 * 添加事件
	 * 
	 */
	private void initEvent() {
		jTree.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});
		addBtn.addActionListener(new ActionListener() {// 单选添加
					@Override
					public void actionPerformed(ActionEvent e) {
						int[] rows = searchTable.getSelectedRows();
						for (int row : rows) {
							singleSearchTable(row);
						}
					}
				});
		addAllBtn.addActionListener(new ActionListener() {// 全选添加
					@Override
					public void actionPerformed(ActionEvent e) {
						searchTable.selectAll();
						int[] rows = searchTable.getSelectedRows();
						for (int row : rows) {
							singleSearchTable(row);
						}
					}
				});
		searchTable.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				searchTablemousePressed(evt);
			}
		});

		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteBtnAction();
			}
		});

		okBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okBtnAction();
			}
		});

		emptyBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				emptyBtnAction();
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 修改该事件的原因是为了让窗体关闭按钮和取消按钮一致
				actionPerformedCancel();
			}
		});

		/**
		 * 关闭按钮监听
		 * 
		 * */
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				// 修改该事件的原因是为了让窗体关闭按钮和取消按钮一致
				actionPerformedCancel();
				return true;
			}
		});

		searchText.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				searchAction();

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				searchAction();

			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	/**
	 * 取消按钮和窗体关闭按钮
	 * 
	 * */
	private void actionPerformedCancel() {
		listJecnTreeBean.clear();
		listJecnTreeBean.addAll(saveOldSelectList);
		closeDialog();
	}

	/**
	 * 
	 * 初始化数据
	 * 
	 */
	private void initData() {
		saveOldSelectList.addAll(listJecnTreeBean);
	}

	public List<JecnTreeBean> getListJecnTreeBean() {
		return listJecnTreeBean;
	}

	public void setListJecnTreeBean(List<JecnTreeBean> listJecnTreeBean) {
		this.listJecnTreeBean = listJecnTreeBean;
	}

	private List<Long> listIds = new ArrayList<Long>();

	public List<Long> getListIds() {
		return listIds;
	}

	public void setListIds(List<Long> listIds) {
		this.listIds = listIds;
	}

	public JecnTable getResultTable() {
		return resultTable;
	}

	public void setResultTable(JecnTable resultTable) {
		this.resultTable = resultTable;
	}

	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}

	public void setSelectMutil(boolean selectMutil) {
		this.selectMutil = selectMutil;
		if (!selectMutil) {
			buttonsPanel.remove(deleteBtn);
		}
	}

	/**
	 * @author yxw 2012-12-6
	 * @description:移除清空按钮
	 */
	public void removeEmptyBtn() {
		buttonsPanel.remove(emptyBtn);
	}

	public boolean isSelectMutil() {
		return selectMutil;
	}

	/**
	 * id是否存在，并加入结果集
	 * 
	 * @param strId
	 * @return
	 */
	public boolean listJTabelResult(JecnTreeBean jecnTreeBean) {
		for (JecnTreeBean newTreeBean : listJecnTreeBean) {
			if (newTreeBean.getId().equals(jecnTreeBean.getId())) {
				newTreeBean.setContent(jecnTreeBean.getContent());
				JecnTableCommon.getTableSelect(jecnTreeBean.getId(), resultTable);
				return false;
			}
		}
		if (isSelectMutil()) {
			listJecnTreeBean.add(jecnTreeBean);
		} else {
			listJecnTreeBean.clear();
			listJecnTreeBean.add(jecnTreeBean);
		}

		return true;
	}

	/**
	 * @author yxw 2012-5-11
	 * @description:jecnTreeBean 转换成可以加到表格里的数据
	 * @param jecnTreeBean
	 * @return
	 */
	public abstract Vector<String> convertRowData(JecnTreeBean jecnTreeBean);

	/**
	 * @author yxw 2012-5-10
	 * @description:转换成Table的内容
	 * @param list
	 * @return
	 */
	public abstract Vector<Vector<String>> getContent(List<JecnTreeBean> list);

	/**
	 * 双击树选择
	 * 
	 * @param evt
	 */
	public void jTreeMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jTree1MousePressed
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			if (evt.getClickCount() == 2) {
				if (!jTree.isSelectionEmpty()) {
					javax.swing.tree.TreePath treePath = jTree.getSelectionPath();
					JecnTreeNode node = (JecnTreeNode) treePath.getLastPathComponent();
					JecnTreeBean jecnTreeBean = node.getJecnTreeBean();

					// 组织选择
					if (chooseType == 0 && jecnTreeBean.getTreeNodeType() != TreeNodeType.organization) {
						return;
						// 岗位选择 只能选择岗位
					} else if (chooseType == 1 && jecnTreeBean.getTreeNodeType() != TreeNodeType.position) {
						return;
						// 文件选择
					} else if (chooseType == 2 && jecnTreeBean.getTreeNodeType() != TreeNodeType.file) {
						return;
						// 流程图选择
					} else if (chooseType == 3 && jecnTreeBean.getTreeNodeType() != TreeNodeType.process
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.processMap) {
						return;
						// 标准选择
					} else if (chooseType == 4 && jecnTreeBean.getTreeNodeType() != TreeNodeType.standard) {
						return;
						// 制度选择
					} else if (chooseType == 5 && jecnTreeBean.getTreeNodeType() != TreeNodeType.ruleFile
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.ruleModeFile) {
						return;
						// 角色选择
					} else if (chooseType == 6 && jecnTreeBean.getTreeNodeType() != TreeNodeType.role
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.roleDefault) {
						return;
						// 人员选择
					} else if (chooseType == 7 && jecnTreeBean.getTreeNodeType() != TreeNodeType.person) {
						return;
					}// 岗位组选择
					else if (chooseType == 8 && jecnTreeBean.getTreeNodeType() != TreeNodeType.positionGroup) {
						return;
					}// 支持工具选择
					else if (chooseType == 9 && jecnTreeBean.getTreeNodeType() != TreeNodeType.tool) {
						return;
						// 流程地图模板
					} else if (chooseType == 10 && jecnTreeBean.getTreeNodeType() != TreeNodeType.processMapMode) {
						return;
						// 流程图模板
					} else if (chooseType == 11 && jecnTreeBean.getTreeNodeType() != TreeNodeType.processMode) {
						return;

					} else if (chooseType == 12 && jecnTreeBean.getTreeNodeType() != TreeNodeType.ruleMode) {
						return;
						// 流程选择只能选流程图
					} else if (chooseType == 13 && !JecnDesignerCommon.isProcess(jecnTreeBean.getTreeNodeType())) {
						return;
						// 文件选择目录和文件均可选择
					} else if (chooseType == 14 && jecnTreeBean.getTreeNodeType() != TreeNodeType.file
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.fileDir) {
						return;
						// 15标准选择(目录和文件均可选择)
					} else if (chooseType == 15 && jecnTreeBean.getTreeNodeType() != TreeNodeType.standardDir
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.standard) {
						return;
						// 16制度选择(目录和文件均可选择)
					} else if (chooseType == 16 && jecnTreeBean.getTreeNodeType() != TreeNodeType.ruleDir
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.ruleFile
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.ruleModeFile) {
						return;
						// 17只能选图片
					} else if (chooseType == 17) {
						if (jecnTreeBean.getTreeNodeType() != TreeNodeType.file) {
							return;
						} else if (!JecnUtil.isImage(jecnTreeBean.getName())) {
							return;
						}
						// 18流程模板选择(流程图模板和流程图地图模板均可选择)
					} else if (chooseType == 18 && jecnTreeBean.getTreeNodeType() != TreeNodeType.processMapMode
							&& jecnTreeBean.getTreeNodeType() != TreeNodeType.processMode) {
						return;
					} else if (chooseType == 19 && jecnTreeBean.getTreeNodeType() != TreeNodeType.fileDir) {
						return;
						// 只能选择文件目录
					} else if (chooseType == 20
							&& !(jecnTreeBean.getTreeNodeType() == TreeNodeType.standard
									|| jecnTreeBean.getTreeNodeType() == TreeNodeType.standardClause || jecnTreeBean
									.getTreeNodeType() == TreeNodeType.standardClauseRequire)) {
						return;
						// 制度选择
					} else if (chooseType == 21 && jecnTreeBean.getTreeNodeType() != TreeNodeType.riskPoint) {
						return;
						// 风险选择
					} else if (chooseType == 22 && jecnTreeBean.getTreeNodeType() != TreeNodeType.innerControlClause) {
						return;
					}// 术语定义选择
					else if (chooseType == 40 && jecnTreeBean.getTreeNodeType() != TreeNodeType.termDefine) {
						return;
					} else if (chooseType == 23 && jecnTreeBean.getTreeNodeType() == TreeNodeType.posGroupRelPos) {
						return;
					} else if (isNoAuthNode(node)) {// 角色管理，流程权限选择
						return;
					} else if (chooseType == 41 && TreeNodeType.processMap != jecnTreeBean.getTreeNodeType()) {
						return;
						// 文件选择目录和文件均可选择
					}
					/** 判断是否已经存在结果行里面 */
					if (listJTabelResult(jecnTreeBean)) {
						if (isSelectMutil()) {// 判断是否许选择多条
							// 增加table一行数据
							resultTable.addRow(convertRowData(jecnTreeBean));
						} else {
							resultTable.remoeAll();
							// 增加
							resultTable.addRow(convertRowData(jecnTreeBean));
						}

					}

				}
			}
		}

	}

	/**
	 * 二级管理员 角色相关权限
	 * 
	 * @param node
	 * @return
	 */
	private boolean isNoAuthNode(JecnTreeNode node) {
		JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
		if (isRootNode(jecnTreeBean.getTreeNodeType())) {
			return true;
		}
		switch (chooseType) {
		case 24:
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.process
					|| jecnTreeBean.getTreeNodeType() == TreeNodeType.processMap) {
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			}
			break;
		case 25:// 角色管理：文件权限选择
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.fileDir
					|| jecnTreeBean.getTreeNodeType() == TreeNodeType.file) {//
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			}
			break;
		case 26:// 角色管理：标准权限选择
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.standardDir
					|| jecnTreeBean.getTreeNodeType() == TreeNodeType.standard) {//
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			}
			break;
		case 27:// 角色管理：制度权限选择
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.ruleDir
					|| jecnTreeBean.getTreeNodeType() == TreeNodeType.ruleFile
					|| jecnTreeBean.getTreeNodeType() == TreeNodeType.ruleModeFile) {//
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			}
			break;
		case 28:
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.riskDir) {//
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			}
			break;
		case 29:
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.organization) {//
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			}
			break;
		case 30:// 新建二级管理员,选择二级管理员
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.person) {//
				// 判断选择人员是否存在角色权限，存在提示
				if (isRolePersonAuthValidate(jecnTreeBean)) {
					return true;
				}
			} else {
				return true;
			}
			break;
		case 31:// 新建二级管理员,选择二级管理员
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.positionGroup
					|| jecnTreeBean.getTreeNodeType() == TreeNodeType.positionGroupDir) {//
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			} else {
				return true;
			}
			break;
		case 32:// 角色管理：术语权限选择
			if (jecnTreeBean.getTreeNodeType() == TreeNodeType.termDefine
					|| jecnTreeBean.getTreeNodeType() == TreeNodeType.termDefineDir) {
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType != 1 && authType != 2) {
					return true;
				}
			}
			break;
		default:
			break;
		}
		return false;
	}

	/**
	 * 角色管理选择人员，人员是否存在角色权限验证
	 * 
	 * @param jecnTreeBean
	 * @return
	 */
	protected boolean isRolePersonAuthValidate(JecnTreeBean jecnTreeBean) {
		try {
			boolean isRoleAuth = ConnectionPool.getJecnRole().isRoleAuthDesigner(jecnTreeBean.getId());
			if (isRoleAuth) {
				// 系统管理员和二级管理员和流程设计专家只能选择其中一个！
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("roleAuthDesignerOnlyOne"));
				return true;
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, e.getMessage());
			return true;
		}
		return false;
	}

	/**
	 * root 节点判断
	 * 
	 * @param rootNode
	 * @return
	 */
	private boolean isRootNode(TreeNodeType rootNode) {
		switch (rootNode) {
		case processRoot:
		case fileRoot:
		case ruleRoot:
		case standardRoot:
		case organizationRoot:
		case riskRoot:
		case positionGroupRoot:
		case termDefineRoot:
			return true;
		}
		return false;
	}

	protected void okBtnAction() {
		isOperation = true;
		closeDialog();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:关闭Dialog
	 */
	public void closeDialog() {
		this.dispose();
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:搜索结果
	 */
	public void searchAction() {
		String name = searchText.getText().trim();
		if (!DrawCommon.isNullOrEmtryTrim(name)) {
			searchListJecnTreeBean = searchByName(name);
			searchTable.reAddVectors(getContent(searchListJecnTreeBean));
		}
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:搜索
	 * @param name
	 * @return
	 */
	public abstract List<JecnTreeBean> searchByName(String name);

	/**
	 * @author yxw 2012-5-4
	 * @description:
	 * @return 返回搜索标签的名称
	 */
	public abstract String getSearchLabelName();

	/**
	 * @author yxw 2012-5-4
	 * @description:设置
	 * @return
	 */
	public abstract JecnTree getJecnTree();

	/**
	 * @author yxw 2012-5-9
	 * @description:删除操作
	 */
	public void deleteBtnAction() {
		int[] selectRows = resultTable.getSelectedRows();
		if (selectRows.length == 0) {
			jLabelTip.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		resultTable.removeRows(resultTable.getSelectedRows());
		for (int i = selectRows.length - 1; i >= 0; i--) {
			listJecnTreeBean.remove(selectRows[i]);
		}
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:清空操作
	 */
	public void emptyBtnAction() {
		// 增加是否要清空
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isClear"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		resultTable.remoeAll();
		listJecnTreeBean.clear();

	}

	/**
	 * @author yxw 2012-5-9
	 * @description:双击Table
	 */
	protected void searchTablemousePressed(MouseEvent evt) {
		if (evt.getClickCount() == 2) {
			if (this.selectMutil && isKeyCtrlorShift) {
				int[] rows = searchTable.getSelectedRows();
				for (int row : rows) {
					singleSearchTable(row);
				}
			} else {
				int row = searchTable.getSelectedRow();
				singleSearchTable(row);
			}

		}
	}

	protected void singleSearchTable(int row) {
		if (row != -1) {
			JecnTreeBean jecnTreeBean = this.searchListJecnTreeBean.get(row);
			switch (chooseType) {
			case 17:
				if (!JecnUtil.isImage(jecnTreeBean.getName())) {
					return;
				}
				break;
			default:
				break;
			}

			/** 判断是否已经存在结果行里面 */
			if (listJTabelResult(jecnTreeBean)) {
				if (isSelectMutil()) {// 判断是否许选择多条
					// 增加table一行数据
					resultTable.addRow(convertRowData(jecnTreeBean));
				} else {
					resultTable.remoeAll();
					// 增加
					resultTable.addRow(convertRowData(jecnTreeBean));
				}

			}
		}
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:获得搜索table的标题
	 * @return
	 */
	public abstract Vector<String> getTableTitle();

	/** 点击表格 */
	public void isClickTable() {

	}

	protected boolean isKeyCtrlorShift = false;

	class ResultTable extends JecnTable {

		public ResultTable() {
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {// 双击
						// 编辑处理
						JecnSelectChooseDialog.this.isClickTable();
					}
				}
			});
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent(listJecnTreeBean));
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return hiddenCols();
		}
	}

	class SearchTable extends JecnTable {

		public SearchTable() {
			this.addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {

				}

				@Override
				public void keyReleased(KeyEvent e) {
					isKeyCtrlorShift = false;

				}

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_SHIFT || e.getKeyCode() == KeyEvent.VK_CONTROL) {
						isKeyCtrlorShift = true;
					}

				}
			});
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent(null));
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return hiddenCols();
		}
	}

	public int[] hiddenCols() {
		return new int[] { 0 };
	}

	public int getChooseType() {
		return chooseType;
	}

	public void setChooseType(int chooseType) {
		this.chooseType = chooseType;
	}

	public JButton getEmptyBtn() {
		return emptyBtn;
	}

	public void setEmptyBtn(JButton emptyBtn) {
		this.emptyBtn = emptyBtn;
	}

	public JecnTable getSearchTable() {
		return searchTable;
	}

	public void setSearchTable(JecnTable searchTable) {
		this.searchTable = searchTable;
	}

	public JecnTree getjTree() {
		return jTree;
	}

	public JButton getDeleteBtn() {
		return deleteBtn;
	}

	public void setDeleteBtn(JButton deleteBtn) {
		this.deleteBtn = deleteBtn;
	}

}
