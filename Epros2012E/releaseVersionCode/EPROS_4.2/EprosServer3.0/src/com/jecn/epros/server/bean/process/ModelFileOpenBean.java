package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 打开模板文件Bean
 * @author 2012-06-30
 *
 */
public class ModelFileOpenBean implements Serializable{
	/** 文件ID*/
	private Long fileId;
	/** 文件名称*/
	private String fileName;
	/** 文件流*/
	private byte[] fileByte;
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getFileByte() {
		return fileByte;
	}
	public void setFileByte(byte[] fileByte) {
		this.fileByte = fileByte;
	}
	
	

}
