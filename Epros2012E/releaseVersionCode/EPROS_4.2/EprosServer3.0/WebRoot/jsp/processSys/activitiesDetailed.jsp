<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<script type="text/javascript" src="${basePath}js/firstPage.js"
			charset="UTF-8">
</script>
		<title><s:text name="activitiesDetailed" />
		</title>

		<style type="text/css">
.active_font {
	font: bold 12px ;
}
</style>
	</head>

	<body style="height: 100%; margin: 0px; padding: 0px;">
	
		<div style="height: 25px; width: 100%; overflow: hidden;"
			align="right" class="gradient minWidth">
			<%@include file="/navigation.jsp"%>
		</div>
		<div style="position: absolute; width: 100%;" align="center">

			<div id="activeAttribute" class="divBorder" style="height: 100%;"
				align="left">
				<table width="100%">
					<tr>
						<td align="center"
							style="background-color: #E4E4E4; height: 30px; font: bold 15px ;">
							<!-- 活动明细 -->
							<s:text name="activitiesDetailed" />
						</td>
					</tr>
				</table>
				<div id="activitiesDetailedDiv"
					style="margin-top: 10px; margin-left: 15px; margin-bottom: 15px; width: 98%;">
					<span class="active_font" align="left"> <img
							src="${basePath}images/common/posResponsibility.gif"
							style="vertical-align: middle;">&nbsp; <!--基本信息--> <s:text
							name="basicInformation" /> </span>
					<table style="margin-left: 15px; margin-top: 5px;">
						<tr>
							<td width="80px" align="left">
								<!-- 活动名称：-->
								<s:text name="activityTitleC" />
							</td>
							<td>
								<s:property value="processActiveWebBean.activeName" />
							</td>
						</tr>
						<tr>
							<td width="80px" align="left">
								<!-- 活动编号：-->
								<s:text name="activitynumbersC" />
							</td>
							<td>
								<s:property value="processActiveWebBean.activeId" />
							</td>
						</tr>
						<tr>
							<td width="80px" align="left">
								<!-- 执行角色：-->
								<s:text name="executiveRoleC" />
							</td>
							<td>
								<s:property value="processActiveWebBean.roleName" />
							</td>
						</tr>
						 <s:if test="#request.activityTypeIsShow">
							<tr>
								<td width="80px" align="left">
									<!-- 活动类别：-->
									<s:text name="activeTypeC" />
								</td>
								<td>
									<s:property value="processActiveWebBean.activeTypeName" />
								</td>
							 </tr>
						</s:if>
						<tr>
							<td width="80px" align="left" style="vertical-align: top;">
								<!-- 活动说明： -->
								<s:text name="activityIndicatingThatC" />
							</td>
							<td>
								<s:property value="processActiveWebBean.activeShow"
									escape="false" />
							</td>
						</tr>
						<tr>
							<td width="80px" align="left">
								<!-- 关键类型：-->
								<s:text name="activeKeyTypeC" />
							</td>
							<td>
								<s:property value="processActiveWebBean.strKeyActive" />
							</td>
						</tr>
						<tr>
							<td width="80px" align="left" style="vertical-align: top;">
								<!-- 关键说明：-->
								<s:text name="activeKeyShowC" />
							</td>
							<td>
								<s:property escape="false" value="processActiveWebBean.keyActiveShow" />
							</td>
						</tr>
					</table>

					<BR>
					<span class="active_font" align="left"><img
							src="${basePath}images/common/posResponsibility.gif"
							style="vertical-align: middle;">&nbsp; <!-- 相关文件 --> <s:text
							name="activeRelateFile" /> </span>
					<table width="98%" style="margin-left: 15px; margin-top: 5px;">
						<tr>
							<td width="80px" align="left" style="vertical-align: top">
								<!-- 输入：-->
								<s:text name="inputC" />
							</td>
							<td style="word-break: break-all;">
							<!--输入说明-->
							<s:if test="processActiveWebBean.activityNoteShow==1">
								<s:property escape="false" value="processActiveWebBean.activityInput" />
									<br>
									</s:if>
									
								<s:iterator value="processActiveWebBean.listInput">
									<a
										href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId' />&isPub=${isPub}"
										style="cursor: pointer;" target='_blank' class="table"> <s:property
											value="fileName" /> </a>
									<br>
								</s:iterator>
							</td>
						</tr>
						<tr>
							<td width="80px" align="left" style="vertical-align: top">
								<!-- 操作规范：-->
								<s:text name="operationSpecifications" />
								<s:text name="chinaColon" />
							</td>
							<td style="word-break: break-all;">
								<s:iterator value="processActiveWebBean.listOperation">
									<a
										href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId' />&isPub=${isPub}"
										style="cursor: pointer;" target='_blank' class="table"> <s:property
											value="fileName" /> </a>
									<br>
								</s:iterator>
							</td>
						</tr>
						<tr>
							<td width="80px" align="left" style="vertical-align: top">
								<!-- 输出说明：-->
								<s:text name="outputC" />
								
							</td>
							<td style="word-break: break-all;">
							<s:if test="processActiveWebBean.activityNoteShow==1">
							<s:property escape="false" value="processActiveWebBean.activityOutput" />
						<br>
						</s:if>
								
							</td>
						</tr>
					</table>


					<span style="margin-left: 19px; margin-top: 5px;"> <!-- 输出： -->
						
						
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#BFBFBF"
						style="table-layout: fixed; margin-left: 15px; margin-top: 5px;">
						<tr class="FixedTitleRow" align="center">
							<th bgcolor="#DDDDDD" width="50%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 表单 -->
								<s:text name="form" />
							</th>
							<th bgcolor="#DDDDDD" width="50%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 样例 -->
								<s:text name="sample" />
							</th>
						</tr>
						<s:iterator value="processActiveWebBean.listMode" id="modeList"
							status="st1">
							<tr id="tr0" bgcolor="#ffffff"
								onmouseover="style.backgroundColor='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';">
								<td class="t5" align="center" style="word-break: break-all;">
									<a
										href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='modeFile.fileId' />&isPub=${isPub}"
										style="cursor: pointer;" target='_blank' class="table">
										<s:property value="modeFile.fileName" /> </a>
									<br>
								</td>
								<s:if test='#modeList.templetSize>0'>
									<td class="t5" align="center" style="word-break: break-all;">
										<s:iterator value="#modeList.listTemplet" id="tempList">
											<a
												href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='#tempList.fileId' />&isPub=${isPub}"
												style="cursor: pointer;" target='_blank' class="table">
												<s:property value="#tempList.fileName" /> </a>
											<br>
										</s:iterator>
									</td>
								</s:if>
								<s:else>
									<td></td>
								</s:else>
							</tr>
						</s:iterator>
					</table>

					<BR>

					<span class="active_font" align="left"><img
							src="${basePath}images/common/posResponsibility.gif"
							style="vertical-align: middle;">&nbsp; <!-- 指标--> <s:text
							name="activityIndicators" /> </span>
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#BFBFBF"
						style="table-layout: fixed; margin-left: 15px; margin-top: 5px;">
						<tr class="FixedTitleRow" align="center">
							<th bgcolor="#DDDDDD" width="50%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- KPI -->
								<s:text name="KPI" />
							</th>
							<th bgcolor="#DDDDDD" width="50%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 目标值 -->
								<s:text name="targetValue" />
							</th>
						</tr>
						<s:iterator value="processActiveWebBean.indicatorsList"
							id="indicatList" status="st1">
							<tr id="tr0" bgcolor="#ffffff"
								onmouseover="style.backgroundColor='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';">
								<td class="t5" align="left" style="word-break: break-all;">
									<s:property value="#indicatList.indicatorName" />
								</td>
								<td class="t5" align="left" style="word-break: break-all;">
									<s:property value="#indicatList.indicatorValue" />
								</td>
							</tr>
						</s:iterator>
					</table>
					<s:if test="#application.versionType">
					<BR>
							<span class="active_font" align="left"><img
							src="${basePath}images/common/posResponsibility.gif"
							style="vertical-align: middle;">&nbsp; <!-- 信息化--> <s:text
							name="activeOnLine" /> </span>
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#BFBFBF"
						style="table-layout: fixed; margin-left: 15px; margin-top: 5px;">
						<tr class="FixedTitleRow" align="center">
							<th bgcolor="#DDDDDD" width="20%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 系统名称-->
								<s:text name="activeSysName" />
							</th>
							<th bgcolor="#DDDDDD" width="60%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 事务代码 -->
								<s:text name="transactionCode" />
							</th>

							<th bgcolor="#DDDDDD" width="20%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 上线时间 -->
								<s:text name="activeLineTime" />
							</th>

							<s:iterator value="processActiveWebBean.listJecnActiveOnLineBean"
								id="onLineList" status="st1">
								<tr id="tr0" bgcolor="#ffffff"
									onmouseover="style.backgroundColor='#EFEFEF';"
									onmouseout="style.backgroundColor='#ffffff';">
									<td align="center" class="t5" style="word-break: break-all;">
										<s:property value="#onLineList.sysName" />
									</td>
									<td style="word-break: break-all;" class="t5" align="left">
										<s:property escape="false" value="#onLineList.informationDescription" />
									</td>
									<td style="word-break: break-all;" class="t5" align="center">
										<s:property value="#onLineList.strTime" />
									</td>
								</tr>
							</s:iterator>

						</tr>
					</table>
					<s:if test="processActiveWebBean.activityTimeConfig==1">
					<BR>
					<span class="active_font" align="left"><img
							src="${basePath}images/common/posResponsibility.gif"
							style="vertical-align: middle;">&nbsp; <!--时间--> <s:text
							name="activeTime" /> </span>
					<table style="margin-left: 15px; margin-top: 5px;">
						<tr>
							<td width="100" align="left" style="vertical-align: top">
								<!-- 时间类型：-->
								<s:text name="timeTypeC" />
							</td>
							<td style="word-break: break-all;">
								<s:property value="processActiveWebBean.strTimeType" />
							</td>

						</tr>
						<tr>
							<td width="100" align="left" style="vertical-align: top">
								<!-- 开始时间：-->
								<s:text name="startTimeC" />
							</td>
							<td style="word-break: break-all;">
								<s:property value="processActiveWebBean.startTime" />
							</td>
						</tr>
						<tr>
							<td width="10%" align="left" style="vertical-align: top">
								<!-- 结束时间：-->
								<s:text name="endTimeC" />
							</td>
							<td style="word-break: break-all;">
								<s:property value="processActiveWebBean.stopTime" />
							</td>
						</tr>
					</table>
					</s:if>
				</s:if>

               <s:if test="#application.versionType">
					<BR>
					<span class="active_font" align="left"><img
							src="${basePath}images/common/posResponsibility.gif"
							style="vertical-align: middle;">&nbsp; <!-- 控制点 --> <s:text
							name="activeControlPoint" /> </span>
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#BFBFBF"
						style="table-layout: fixed; margin-left: 15px; margin-top: 5px;">
						<tr class="FixedTitleRow" align="center">
							<th bgcolor="#DDDDDD" width="10%" height="20px" class="gradient"
								style="word-break: break-all;">
								<!-- 控制点编号：-->
								<s:text name="controlPointNum" />
							</th>

							<th bgcolor="#DDDDDD" width="10%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 风险编号：-->
								<s:text name="riskNum" />
							</th>
							<th bgcolor="#DDDDDD" width="40%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 控制目标：-->
								<s:text name="controlTarget" />
							</th>
							<th bgcolor="#DDDDDD" width="10%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 控制方法：-->
								<s:text name="controlMethod" />
							</th>
							<th bgcolor="#DDDDDD" width="10%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 控制类型：-->
								<s:text name="controlType" />
							</th>
							<th bgcolor="#DDDDDD" width="10%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 控制频率：-->
								<s:text name="controlFrequency" />
							</th>
							<th bgcolor="#DDDDDD" width="10%" height="20" class="gradient"
								style="word-break: break-all;">
									<!-- 关键控制点：-->
								<s:text name="keyControlPoint" />
							</th>
						</tr>
                        <s:iterator
							value="processActiveWebBean.controlPointTs"
							id="controlPointT" status="ct1">
								<tr id="tr0" bgcolor="#ffffff">
									<td align="center" class="t5" style="word-break: break-all;">
										<!-- 控制编号 -->
										<s:property value="#controlPointT.controlCode" />
									</td>
									<!-- 风险编号 -->
									<td align="center" class="t5" style="word-break: break-all;">
										<s:property value="#controlPointT.riskNum" />
									</td>
									<!-- 控制目标 -->
									<td align="left" class="t5" style="word-break: break-all;">
										<s:property  escape="false" value="#controlPointT.controlTarget" />
									</td>
									<!-- 控制方法 -->
									<td align="center" class="t5" style="word-break: break-all;">

										<s:if test="#controlPointT.method==0">
											<s:text name="artificial" />
										</s:if>
										<s:if test="#controlPointT.method==1">
											<s:text name="it" />
										</s:if>
										<s:if test="#controlPointT.method==2">
											<s:text name="renOrIt" />                                                   
                                   </s:if>
									</td>
									<!-- 控制类型 -->
									<td align="center" class="t5" style="word-break: break-all;">
										<s:if test="#controlPointT.type==0">
											<s:text name="preventive" />
										</s:if>
										<s:if test="#controlPointT.type==1">
											<s:text name="discover" />
										</s:if>
									</td>
									<!--控制频率  -->
									<td align="center" class="t5" style="word-break: break-all;">
										<s:if test="#controlPointT.frequency==0">
											<!--不定期  -->
											<s:text name="noScheduled" />
										</s:if>
										<s:if test="#controlPointT.frequency==1">
											<!-- 日 -->
											<s:text name="day" />
										</s:if>
										<s:if test="#controlPointT.frequency==2">
											<!-- 周 -->
											<s:text name="weeks" />
										</s:if>
										<s:if test="#controlPointT.frequency==3">
											<!-- 月 -->
											<s:text name="month" />
										</s:if>
										<s:if test="#controlPointT.frequency==4">
											<!--  季度-->
											<s:text name="seasons" />
										</s:if>
										<s:if test="#controlPointT.frequency==5">
											<!-- 年度 -->
											<s:text name="year" />
										</s:if>
									</td>
									<!-- 关键控制点-->
									<td align="center" class="t5" style="word-break: break-all;">
										<s:if test="#controlPointT.keyPoint==0">
											<!-- 是 -->
											<s:text name="is" />
										</s:if>

										<s:if test="#controlPointT.keyPoint==1">
											<!-- 否 -->
											<s:text name="no" />
										</s:if>
									</td>
								</tr>
							</s:iterator>
					</table>
					
					<BR>

					<span class="active_font" align="left"><img
							src="${basePath}images/common/posResponsibility.gif"
							style="vertical-align: middle;">&nbsp; <!-- 关联标准 --> <s:text
							name="activeRelateStandard" /> </span>
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#BFBFBF"
						style="table-layout: fixed; margin-left: 15px; margin-top: 5px;">
						<tr class="FixedTitleRow" align="center">
							<th bgcolor="#DDDDDD" width="50%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 文件名称-->
								<s:text name="fileName" />
							</th>

							<th bgcolor="#DDDDDD" width="50%" height="20" class="gradient"
								style="word-break: break-all;">
								<!-- 类型-->
								<s:text name="type" />
							</th>
						</tr>

						<s:iterator
							value="processActiveWebBean.listJecnActiveStandardBean"
							id="standardList" status="st1">
							<tr id="tr0" bgcolor="#ffffff"
								onmouseover="style.backgroundColor='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';">
								<td align="center" class="t5" style="word-break: break-all;">
									<a
										href="standard.action?from=process&reqType=public&standardType=<s:property value='#standardList.relaType' />&standardId=<s:property value='#standardList.standardId' />"
										style="cursor: pointer;" target='_blank' class="table"> <s:property
											value="#standardList.relaName" /> </a>
								</td>
								<td align="center" class="t5" style="word-break: break-all;">
									<s:property value="#standardList.strType" />
								</td>
							</tr>
						</s:iterator>

					</table>
					
					</s:if>
				</div>
			</div>
		</div>
	</body>
</html>