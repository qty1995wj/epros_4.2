package com.jecn.epros.domain.process.relatedFile;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程相关文件
 *
 * @author ZXH
 */
public class ProcessRelatedFileBean {

    private List<RelatedStandard> relatedStandards = new ArrayList<RelatedStandard>();
    private List<RelatedProcess> relatedProcesses = new ArrayList<RelatedProcess>();
    private List<RelatedRisk> relatedRisks = new ArrayList<RelatedRisk>();
    private List<RelatedRule> relatedRules = new ArrayList<RelatedRule>();

    public List<RelatedStandard> getRelatedStandards() {
        return relatedStandards;
    }

    public void setRelatedStandards(List<RelatedStandard> relatedStandards) {
        this.relatedStandards = relatedStandards;
    }

    public List<RelatedProcess> getRelatedProcesses() {
        return relatedProcesses;
    }

    public void setRelatedProcesses(List<RelatedProcess> relatedProcesses) {
        this.relatedProcesses = relatedProcesses;
    }

    public List<RelatedRisk> getRelatedRisks() {
        return relatedRisks;
    }

    public void setRelatedRisks(List<RelatedRisk> relatedRisks) {
        this.relatedRisks = relatedRisks;
    }

    public List<RelatedRule> getRelatedRules() {
        return relatedRules;
    }

    public void setRelatedRules(List<RelatedRule> relatedRules) {
        this.relatedRules = relatedRules;
    }
}
