package epros.designer.gui.system.config.ui.comp;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

/**
 * 
 * 选择数值
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigSpinner extends JSpinner {
	/** 数据对象 */
	private JecnConfigItemBean itemBean = null;

	public JecnConfigSpinner(SpinnerNumberModel model) {
		super(model);
	}

	public JecnConfigItemBean getItemBean() {
		return itemBean;
	}

	public void setItemBean(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
	}

	/**
	 * 
	 * 设置数据对象并且获取Value字段值给此组件
	 * 
	 * @param itemBean
	 */
	public void setItemBeanAndValue(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
		initConfigSpinnerData();
	}

	/**
	 * 
	 * 设置设定值
	 * 
	 */
	public void initConfigSpinnerData() {
		if (itemBean != null) {
			this.setValue(Integer.parseInt(itemBean.getValue()));
		}
	}

	/**
	 * 
	 * 设置默认值
	 * 
	 */
	public void intConfigSpinnerDefaultData() {
		if (itemBean != null) {
			this.setValue(Integer.parseInt(itemBean.getValue()));
		}
	}
}
