package epros.designer.gui.process;

import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/*******************************************************************************
 * 流程重命名
 * 
 * @author 2012-06-12
 * 
 */
public class EditFlowNameDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditFlowNameDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditFlowNameDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		List ids = new ArrayList<Integer>();
		ids.add(selectNode.getJecnTreeBean().getId().intValue());
		List<Object[]> flowElement = ConnectionPool.getProcessAction().getFlowElementByLinkId(ids);
		if (flowElement != null && !flowElement.isEmpty()) {
			promptLab.setText(JecnProperties.getValue("flowByOtherProcesses"));
		}
		this.setLocationRelativeTo(null);
		try {
			this.setName(selectNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("EditFlowNameDialog is errror", e);
		}
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		// 需要重命名的流程ID
		Long id = selectNode.getJecnTreeBean().getId();
		// 新名称
		String newName = this.getName();

		// 更新人ID
		Long updatorId = JecnConstants.getUserId();

		try {
			ConnectionPool.getProcessAction().reName(id, newName, updatorId, JecnConstants.loginBean.isAdmin() ? 0 : 1);
			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (workflow != null && workflow.getFlowMapData().getFlowId() != null
					&& id.equals(workflow.getFlowMapData().getFlowId())) {
				workflow.getFlowMapData().setName(newName);
			}
			// 节点重命名
			JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		} catch (Exception e) {
			log.error("EditFlowNameDialog saveData is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}

		this.dispose();
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		try {
			JecnTreeBean b = selectNode.getJecnTreeBean();
			// 流程地图
			if (b.getTreeNodeType() == TreeNodeType.processMap) {
				return ConnectionPool.getProcessAction().validateUpdateName(name, b.getId(), b.getPid(), 0,
						JecnConstants.projectId);
				// 流程图
			} else if (b.getTreeNodeType() == TreeNodeType.process) {
				return ConnectionPool.getProcessAction().validateUpdateName(name, b.getId(), b.getPid(), 1,
						JecnConstants.projectId);
			}

		} catch (Exception e1) {
			log.error("EditFlowNameDialog validateNodeRepeat is error", e1);
		}
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}

	@Override
	public boolean validateName(String name, JLabel jLable) {
		return JecnValidateCommon.validateName(name, promptLab);
	}

}
