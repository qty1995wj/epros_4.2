package com.jecn.epros.server.download.wordxml;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.NFC;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.dom.WordDocument;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.AntiFake;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;
import wordxml.util.FileUtil;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.download.ActiveItSystem;
import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.ProcessInterfacesDescriptionBean;
import com.jecn.epros.server.bean.process.driver.JecnFlowDriverTimeBase;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.wordxml.EnumClass.FLOW_DERVER_RULE;
import com.jecn.epros.server.download.wordxml.shidaixincai.SDXCProcessModel;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.viewdoc.MSOffice.OfficeTools;

public abstract class ProcessFileModel implements IProcessFileDefaultStyle {
	/** log 日志 */
	protected final Log log = LogFactory.getLog(ProcessFileModel.class);
	// 空白内容标志
	protected static final String blank = JecnUtil.getValue("none");
	// 数据Bean
	protected ProcessDownloadBean processDownloadBean;
	// 操作说明的目标路径
	private String path;
	// word文档
	private WordDocument doc;
	// 文档常用属性
	protected ProcessFileProperty docProperty;

	// 记录当前多少项，为了显示15.1之类的
	protected int numberMark;

	protected static final String DEFAULT_DELIMITER = "/";

	protected WordPlex word;

	/**
	 * 
	 * @param processDownloadBean
	 *            数据Bean
	 * @param path
	 *            文档的路径
	 * @param flowChartDirection
	 *            true:横向显示流程图， false:纵向显示流程图
	 */
	protected ProcessFileModel(ProcessDownloadBean processDownloadBean, String path, boolean flowChartDirection) {
		this.processDownloadBean = processDownloadBean;
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			this.path = path;
		} else {
			this.path = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
		}
		// 创建文档
		this.doc = new WordDocument(FileUtil.getClassPathDocumentFile());
		// 生成文档属性
		docProperty = new ProcessFileProperty(doc, flowChartDirection, processDownloadBean.getJecnConfigItemBean());
		if (processDownloadBean.getFlowFileHeadData() != null
				&& StringUtils.isNotBlank(processDownloadBean.getFlowFileHeadData().getFilePath())
				&& new File(processDownloadBean.getFlowFileHeadData().getFilePath()).exists()) {
			docProperty.setLogoImage(processDownloadBean.getFlowFileHeadData().getFilePath());
		}
		word = new WordPlex(this, processDownloadBean);
	}

	/**
	 * 包括英文 数字 特殊符号) 默认字体
	 */
	protected void setDefAscii(String font) {
		doc.setAscii(font);
	}

	/**
	 * 设置(中文)默认字体
	 */
	protected void setDefFareast(String font) {
		doc.setFareast(font);

	}

	/**
	 * 设置标题样式
	 * 
	 * @param chart
	 *            标题序号和文本之间的字符 例如 、
	 * @param titlePBean
	 */
	protected void setDocStyle(String chart, ParagraphBean titlePBean) {
		doc.setStyle(chart, titlePBean);
	}

	/**
	 * 设置文档序号类型
	 * 
	 * @param nfc
	 */
	protected void setDocNFC(NFC nfc) {
		doc.setNfc(nfc);
	}

	/**
	 * 设置文档序号从多少开始
	 * 
	 * @param startNum
	 */
	protected void setDocStartNum(int startNum) {
		doc.setStartNum(startNum);
	}

	/**
	 * 初始化入口 通过调用子类的代码来覆盖默认样式，以及给各节添加个性化内容
	 */
	protected final void init() {
		docProperty.setTblContentStyle(tblContentStyle());
		docProperty.setTblTitleStyle(tblTitleStyle());
		docProperty.setTblStyle(tblStyle());
		docProperty.setTextContentStyle(textContentStyle());
		docProperty.setTextTitleStyle(textTitleStyle());
		initTitleSect(docProperty.getTitleSect());
		initFirstSect(docProperty.getFirstSect());
		initFlowChartSect(docProperty.getFlowChartSect());
		initSecondSect(docProperty.getSecondSect());
		addAntiFake(getAntiFake());
	}

	protected AntiFake getAntiFake() {
		return null;
	}

	/**
	 * 构建并输出文档
	 */
	public String write() {
		try {
			build();
			doc.write(path);
		} catch (Exception e) {
			log.error("构建操作说明文档异常", e);
		}
		return OfficeTools.xmlWordSaveAsOfficeWord(path);
	}

	/**
	 * 按照换行符截取字符串数据
	 * 
	 * @param content
	 * @return
	 */
	private String[] getContents(String content) {
		if (StringUtils.isBlank(content)) {
			return new String[] { blank };
		}
		return content.split("\n");
	}

	/**
	 * 调用子类的具体实现来构建文档
	 */
	protected void build() throws Exception {
		// 初始化 子类中的属性也就是通过调用子类的代码来覆盖默认样式
		init();
		beforeHandle();
		Method md = null;
		ProcessFileItem processFileItem = new ProcessFileItem();
		processFileItem.setDataBean(processDownloadBean);
		// 生成所有项
		for (JecnConfigItemBean configItem : processDownloadBean.getJecnConfigItemBean()) {
			if ("0".equals(configItem.getValue())) {
				continue;
			}
			numberMark++;
			// 目的
			md = ProcessFileModel.class.getDeclaredMethod("build_" + configItem.getMark(), ProcessFileItem.class);
			processFileItem.setItemTitle(configItem.getName());
			processFileItem.setBelongTo(docProperty.getMarkSect().get(configItem.getMark()));
			try {
				md.invoke(this, processFileItem);
			} catch (Exception e) {
				log.error("操作说明出错，反射调用方法:" + md.getName() + " 标题名称:" + processFileItem.getItemTitle());
				throw new Exception(e);
			}
			itemAfterHandle(configItem, processFileItem);
		}

		afterHandle();

	}

	protected void itemAfterHandle(JecnConfigItemBean configItem, ProcessFileItem processFileItem) {

	}

	protected void beforeHandle() {

	}

	protected void afterHandle() {

	}

	/**
	 * 目的
	 * 
	 * @param _count
	 * @param name
	 * @param flowPurpose
	 */
	private void build_a1(ProcessFileItem processFileItem) {
		a01(processFileItem, getContents(processFileItem.getDataBean().getFlowPurpose()));
	}

	/**
	 * 适用范围
	 * 
	 * @param _count
	 * @param name
	 * @param applicability
	 */
	private void build_a2(ProcessFileItem processFileItem) {
		a02(processFileItem, getContents(processFileItem.getDataBean().getApplicability()));
	}

	/**
	 * 术语定义
	 * 
	 * @param _count
	 * @param name
	 * @param noutGlossary
	 */
	private void build_a3(ProcessFileItem processFileItem) {
		// 农夫山泉
		if (JecnContants.otherOperaType == 11) {
			a03(processFileItem, JecnWordUtil.getContents(processFileItem.getDataBean().getNoutGlossary()));
		} else {
			a03(processFileItem, getContents(processFileItem.getDataBean().getNoutGlossary()));
		}
	}

	/**
	 * 驱动规则
	 * 
	 * @param _count
	 * @param name
	 * @param flowDriver
	 */
	private void build_a4(ProcessFileItem processFileItem) {
		a04(processFileItem, processFileItem.getDataBean().getFlowDriver());
	}

	/**
	 * 流程输入
	 * 
	 * @param _count
	 * @param name
	 * @param flowInput
	 */
	private void build_a5(ProcessFileItem processFileItem) {
		a05(processFileItem, getContents(processFileItem.getDataBean().getFlowInput()));
	}

	/**
	 * 流程输出
	 * 
	 * @param _count
	 * @param name
	 * @param flowOutput
	 */
	private void build_a6(ProcessFileItem processFileItem) {
		a06(processFileItem, getContents(processFileItem.getDataBean().getFlowOutput()));
	}

	/**
	 * 关键活动
	 * 
	 * @param _count
	 * @param name
	 * @param keyActivityShowList
	 */
	private void build_a7(ProcessFileItem processFileItem) {
		a07(processFileItem, processFileItem.getDataBean().getKeyActivityShowList());
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	private void build_a8(ProcessFileItem processFileItem) {
		// 11 农夫山泉 13 广州机电 (不需要岗位名称数据)
		if (JecnContants.otherOperaType == 11 || JecnContants.otherOperaType == 13) {
			List<Object[]> newDataList = new ArrayList<Object[]>();
			Object[] newData = null;
			for (Object[] data : processFileItem.getDataBean().getRoleList()) {
				newData = new Object[2];
				newData[0] = data[0];
				newData[1] = data[2];
				newDataList.add(newData);
			}
			a08(processFileItem, JecnWordUtil.obj2String(newDataList));
		} else {
			a08(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getRoleList()));
		}
	}

	/**
	 * 流程图
	 * 
	 * @param _count
	 * @param name
	 * @param imgPath
	 */
	private void build_a9(ProcessFileItem processFileItem) {
		// 不存在流程图
		if (StringUtils.isBlank(processFileItem.getDataBean().getImgPath())
				|| !new File(JecnContants.jecn_path + processFileItem.getDataBean().getImgPath()).exists()) {
			createTextItem(processFileItem, blank);
			return;
		}
		/** 1厘米所占用的像素点 */
		final float DPI = 1F / 96F * 2.54F;
		List<String> pathList = getFlowImgPathList(processFileItem.getDataBean().getImgPath());
		List<Image> imgList = new ArrayList<Image>();
		Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("jpg");
		ImageReader reader = readers.next();
		ImageInputStream iis = null;
		for (String path : pathList) {
			Image image = new Image(path);
			float width = 0;
			float height = 0;
			try {
				iis = ImageIO.createImageInputStream(new File(path));
				reader.setInput(iis);
				// 获取图片的宽高
				width = Float.valueOf(String.format("%.2f", reader.getWidth(0) * DPI));
				height = Float.valueOf(String.format("%.2f", reader.getHeight(0) * DPI));
				iis.flush();
			} catch (IOException e) {
				log.error("读取文件异常", e);
			} finally {
				try {
					if (iis != null) {
						iis.close();
					}
				} catch (IOException e) {
					log.error("关闭图片流异常", e);
				}
			}
			SectBean sectBean = null;
			// 最大高度
			float maxHeight;
			if (docProperty.isFlowChartDirection()) {
				sectBean = docProperty.getFlowChartSect().getSectBean();
				maxHeight = sectBean.getHeight() - docProperty.getFlowChartScaleValue();
			} else {
				sectBean = docProperty.getFirstSect().getSectBean();

				maxHeight = sectBean.getHeight() - sectBean.getTop() - sectBean.getBottom();
			}
			// 最大宽度
			float maxWidth = sectBean.getWidth() - sectBean.getLeft() - sectBean.getRight();
			// 获取宽、高的缩放比例
			float scaledW = maxWidth / width;
			float scaledH = maxHeight / height;
			// 取最小缩放比例
			float scaled = scaledW > scaledH ? scaledH : scaledW;
			image.setSize(width * scaled, height * scaled);
			imgList.add(image);
		}
		if (reader != null) {
			reader.dispose();
		}
		a09(processFileItem, imgList);
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	private void build_a10(ProcessFileItem processFileItem) {
		String[] titleArray = new String[7];
		// 活动编号
		titleArray[0] = JecnUtil.getValue("activitynumbers");
		// 执行角色
		titleArray[1] = JecnUtil.getValue("executiveRole");
		// 活动名称
		titleArray[2] = JecnUtil.getValue("activityTitle");
		// 活动说明
		titleArray[3] = JecnUtil.getValue("activityIndicatingThat");
		if (JecnContants.otherOperaType == 6) {
			// 对应内控矩阵风险点
			titleArray[4] = JecnUtil.getValue("innerControlRisk");
			// 对应标准条款
			titleArray[5] = JecnUtil.getValue("standardConditions");
		} else {
			// 输入
			titleArray[4] = JecnUtil.getValue("input");
			// 输出
			titleArray[5] = JecnUtil.getValue("output");
		}

		// 办理时限
		if (JecnConfigTool.showActTimeLine()) {
			titleArray[6] = JecnUtil.getValue("timeLimit") + "\n(现状/目标" + JecnConfigTool.getActiveTimeLineUtil() + ")";
		}
		List<String[]> dataRows = JecnWordUtil.obj2String(processFileItem.getDataBean().getAllActivityShowList());
		// 活动输入输出是否显示
		if (!JecnContants.showActInOutBox && !JecnConfigTool.showActTimeLine()) {
			titleArray = new String[] { titleArray[0], titleArray[1], titleArray[2], titleArray[3] };
			List<String[]> newDataRows = new ArrayList<String[]>();
			for (String[] data : dataRows) {
				data = new String[] { data[0], data[1], data[2], data[3] };
				newDataRows.add(data);
			}
			dataRows.clear();
			dataRows = newDataRows;
		} else if (!JecnConfigTool.showActTimeLine() && JecnContants.showActInOutBox) {
			titleArray = new String[] { titleArray[0], titleArray[1], titleArray[2], titleArray[3], titleArray[4],
					titleArray[5] };
			List<String[]> newDataRows = new ArrayList<String[]>();
			for (String[] data : dataRows) {
				data = new String[] { data[0], data[1], data[2], data[3], data[4], data[5] };
				newDataRows.add(data);
			}
			dataRows.clear();
			dataRows = newDataRows;
		} else if (JecnConfigTool.showActTimeLine() && !JecnContants.showActInOutBox) {
			titleArray = new String[] { titleArray[0], titleArray[1], titleArray[2], titleArray[3], titleArray[6] };
			List<String[]> newDataRows = new ArrayList<String[]>();
			for (String[] data : dataRows) {
				data = new String[] { data[0], data[1], data[2], data[3], getTimeLineValue(data) };
				newDataRows.add(data);
			}
			dataRows.clear();
			dataRows = newDataRows;
		} else {
			List<String[]> newDataRows = new ArrayList<String[]>();
			for (String[] data : dataRows) {
				data = new String[] { data[0], data[1], data[2], data[3], data[4], data[5], getTimeLineValue(data) };
				newDataRows.add(data);
			}
			dataRows.clear();
			dataRows = newDataRows;
		}
		a10(processFileItem, titleArray, dataRows);
	}

	private String getTimeLineValue(Object[] data) {
		String targetValue = StringUtils.isBlank(JecnUtil.objToStr(data[6])) ? "" : JecnUtil.objToStr(data[6]);
		String statusValue = StringUtils.isBlank(JecnUtil.objToStr(data[7])) ? "" : JecnUtil.objToStr(data[7]);
		// 去“.0”
		return concatTimeLineValue(targetValue, statusValue);
	}

	public static String concatTimeLineValue(String targetValue, String statusValue) {
		statusValue = statusValue.endsWith(".0") ? statusValue.substring(0, statusValue.indexOf(".0")) : statusValue;
		targetValue = targetValue.endsWith(".0") ? targetValue.substring(0, targetValue.indexOf(".0")) : targetValue;
		if (StringUtils.isBlank(statusValue) || StringUtils.isBlank(targetValue)) {
			return "";
		}
		return statusValue + "/" + targetValue;
	}

	/**
	 * 段落方式的活动说明
	 * 
	 * @param processFileItem
	 * @param dataRows
	 */
	protected void a10InParagraph(ProcessFileItem processFileItem, List<String[]> dataRows) {
		ParagraphBean titleBean = docProperty.getTextTitleStyle();
		ParagraphBean pBean = new ParagraphBean(titleBean.getAlign(), titleBean.getFontBean());
		// 标题
		createTitle(processFileItem);
		// 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
		String title = "";
		for (int i = 0; i < dataRows.size(); i++) {
			String[] str = dataRows.get(i);
			title = str[0] + "  " + str[2];
			processFileItem.getBelongTo().createParagraph(title, pBean);
			if (StringUtils.isNotBlank(str[1])) {
				processFileItem.getBelongTo().createParagraph("执行角色：", pBean).appendTextRun(str[1],
						docProperty.getTextContentStyle().getFontBean());
			}
			if (JecnContants.showActInOutBox) {
				if (JecnContants.otherOperaType == 6) {
					// 对应内控矩阵风险点 // 对应标准条款
					createActiveParagraph(processFileItem, str[4], JecnUtil.getValue("innerControlRiskC"), pBean);
					createActiveParagraph(processFileItem, str[5], JecnUtil.getValue("standardConditionsC"), pBean);
				} else {
					// 输入 // 输出
					createActiveParagraph(processFileItem, str[4], JecnUtil.getValue("inputC"), pBean);
					createActiveParagraph(processFileItem, str[5], JecnUtil.getValue("outputC"), pBean);
				}
			}
			// 活动说明 ：
			createActiveParagraph(processFileItem, str[3], JecnUtil.getValue("activityIndicatingThatC"), pBean);
		}
	}

	/**
	 * 创建段落活动说明
	 * 
	 * @param processFileItem
	 * @param content
	 * @param title
	 */
	private void createActiveParagraph(ProcessFileItem processFileItem, String content, String title,
			ParagraphBean pBean) {
		if (StringUtils.isBlank(content)) {
			return;
		}
		Paragraph p = processFileItem.getBelongTo().createParagraph(title, pBean);
		String[] data = JecnWordUtil.getContents(content);
		for (int index = 0; index < data.length; index++) {
			// 第一行直接追加到活动说明后面
			if (index == 0) {
				p.appendTextRun(data[0], docProperty.getTextContentStyle().getFontBean());
			} else {
				processFileItem.getBelongTo()
						.createParagraph("      " + data[index], docProperty.getTextContentStyle());
			}
		}
	}

	/**
	 * 流程记录
	 * 
	 * @param _count
	 * @param name
	 * @param processRecordList
	 */
	private void build_a11(ProcessFileItem processFileItem) {
		a11(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getProcessRecordList()));
	}

	/**
	 * 操作规范
	 * 
	 * @param _count
	 * @param name
	 * @param operationTemplateList
	 */
	private void build_a12(ProcessFileItem processFileItem) {
		a12(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getOperationTemplateList()));
	}

	/**
	 * 相关流程
	 * 
	 * @param _count
	 * @param name
	 * @param relatedProcessList
	 */
	private void build_a13(ProcessFileItem processFileItem) {
		if (processFileItem.getDataBean().getRelatedProcessList() == null
				|| processFileItem.getDataBean().getRelatedProcessList().size() == 0) {
			createTextItem(processFileItem, blank);
		} else {
			// 收集流程数据
			Map<String, List<RelatedProcessBean>> relatedProcessMap = new LinkedHashMap<String, List<RelatedProcessBean>>();
			for (RelatedProcessBean relatedProcessBean : processFileItem.getDataBean().getRelatedProcessList()) {
				// 初始化相关流程的结合
				if (!relatedProcessMap.containsKey(relatedProcessBean.getLinecolor())) {
					relatedProcessMap.put(relatedProcessBean.getLinecolor(), new ArrayList<RelatedProcessBean>());
				}
				relatedProcessMap.get(relatedProcessBean.getLinecolor()).add(relatedProcessBean);
			}
			a13(processFileItem, relatedProcessMap);
		}
	}

	/**
	 * 相关制度
	 * 
	 * @param _count
	 * @param name
	 * @param ruleNameList
	 */
	private void build_a14(ProcessFileItem processFileItem) {
		List<Object[]> newRuleNameList = new ArrayList<Object[]>();
		List<Object[]> ruleNameList = processFileItem.getDataBean().getRuleNameList();
		if (JecnContants.otherOperaType == 21) {
			for (Object[] objects : ruleNameList) {
				newRuleNameList.add(new Object[] { objects[2], objects[0], objects[1] });
			}
		} else {
			for (Object[] objects : ruleNameList) {
				newRuleNameList.add(objects);
			}
		}
		a14(processFileItem, JecnWordUtil.obj2String(newRuleNameList));
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	private void build_a15(ProcessFileItem processFileItem) {
		if (processFileItem.getDataBean().getFlowKpiList() == null
				|| processFileItem.getDataBean().getFlowKpiList().size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		if (JecnContants.otherOperaType != 11) {
			for (Object[] objects : processFileItem.getDataBean().getFlowKpiList()) {
				if (objects[1] != null && "1".equals(objects[1].toString())) {
					// 过程性指标
					objects[1] = JecnUtil.getValue("efficiencyIndex");
				} else if (objects[1] != null && "0".equals(objects[1].toString())) {
					// 结果性指标
					objects[1] = JecnUtil.getValue("effectIndex");
				} else {
					// 空
					objects[1] = "";
				}

			}
		}
		a15(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getFlowKpiList()));
	}

	/**
	 * 客户
	 * 
	 * @param _count
	 * @param name
	 * @param flowCustom
	 */
	private void build_a16(ProcessFileItem processFileItem) {
		a16(processFileItem, getContents(processFileItem.getDataBean().getFlowCustom()));
	}

	/**
	 * 不适用范围
	 * 
	 * @param _count
	 * @param name
	 * @param noApplicability
	 */
	private void build_a17(ProcessFileItem processFileItem) {
		a17(processFileItem, getContents(processFileItem.getDataBean().getNoApplicability()));
	}

	/**
	 * 概述
	 * 
	 * @param _count
	 * @param name
	 * @param flowSummarize
	 */
	private void build_a18(ProcessFileItem processFileItem) {
		a18(processFileItem, getContents(processFileItem.getDataBean().getFlowSummarize()));
	}

	/**
	 * 补充说明
	 * 
	 * @param _count
	 * @param name
	 * @param flowSupplement
	 */
	private void build_a19(ProcessFileItem processFileItem) {
		a19(processFileItem, getContents(processFileItem.getDataBean().getFlowSupplement()));
	}

	/**
	 * 记录保存
	 * 
	 * @param _count
	 * @param name
	 * @param flowRecordList
	 */
	private void build_a20(ProcessFileItem processFileItem) {
		a20(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getFlowRecordList()));
	}

	/**
	 * 相关标准
	 * 
	 * @param _count
	 * @param name
	 * @param standardList
	 */
	private void build_a21(ProcessFileItem processFileItem) {
		a21(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getStandardList()));
	}

	/**
	 * 流程边界
	 * 
	 * @param name
	 * @param startEndActive
	 * @param contentSect
	 */
	private void build_a22(ProcessFileItem processFileItem) {
		a22(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getStartEndActive()));
	}

	/**
	 * 流程责任属性
	 * 
	 * @param name
	 * @param responFlow
	 * @param contentSect
	 */
	private void build_a23(ProcessFileItem processFileItem) {
		a23(processFileItem, processFileItem.getDataBean().getResponFlow());
	}

	/**
	 * 流程文控信息
	 * 
	 * @param name
	 * @param documentList
	 * @param contentSect
	 */
	private void build_a24(ProcessFileItem processFileItem) {
		a24(processFileItem, JecnWordUtil.obj2String(processFileItem.getDataBean().getDocumentList()));
	}

	/**
	 * 相关风险
	 * 
	 * @param _count
	 * @param name
	 * @param riskList
	 */
	private void build_a25(ProcessFileItem processFileItem) {
		a25(processFileItem, processFileItem.getDataBean().getRilsList());
	}

	/**
	 * 相关文件
	 * 
	 * @param _count
	 * @param name
	 * @param flowRelatedFile
	 */
	private void build_a26(ProcessFileItem processFileItem) {
		a26(processFileItem, getContents(processFileItem.getDataBean().getFlowRelatedFile()));
	}

	/**
	 * 自定义1
	 * 
	 * @param _count
	 * @param name
	 * @param flowCustomOne
	 */
	private void build_a27(ProcessFileItem processFileItem) {
		a27(processFileItem, getContents(processFileItem.getDataBean().getFlowCustomOne()));
	}

	/**
	 * 自定义2
	 * 
	 * @param _count
	 * @param name
	 * @param flowCustomTwo
	 */
	private void build_a28(ProcessFileItem processFileItem) {
		a28(processFileItem, getContents(processFileItem.getDataBean().getFlowCustomTwo()));
	}

	/**
	 * 自定义3
	 * 
	 * @param _count
	 * @param name
	 * @param flowCustomThree
	 */
	private void build_a29(ProcessFileItem processFileItem) {
		a29(processFileItem, getContents(processFileItem.getDataBean().getFlowCustomThree()));
	}

	/**
	 * 自定义4
	 * 
	 * @param _count
	 * @param name
	 * @param flowCustomFour
	 */
	private void build_a30(ProcessFileItem processFileItem) {
		a30(processFileItem, getContents(processFileItem.getDataBean().getFlowCustomFour()));
	}

	/**
	 * 自定义5
	 * 
	 * @param _count
	 * @param name
	 * @param flowCustomFive
	 */
	private void build_a31(ProcessFileItem processFileItem) {
		a31(processFileItem, getContents(processFileItem.getDataBean().getFlowCustomFive()));
	}

	/**
	 * 输入和输出合并在一起
	 * 
	 * @param name
	 * @param input
	 * @param output
	 * @param contentSect
	 */
	private void build_a32(ProcessFileItem processFileItem) {
		a32(processFileItem, getContents(processFileItem.getDataBean().getFlowInput()), getContents(processFileItem
				.getDataBean().getFlowOutput()));

	}

	/**
	 * 流程范围
	 */
	private void build_a33(ProcessFileItem processFileItem) {
		String flowInput = processFileItem.getDataBean().getFlowInput();
		String flowOutput = processFileItem.getDataBean().getFlowOutput();
		String[] startAndEnd = JecnWordUtil.obj2String(processFileItem.getDataBean().getStartEndActive());
		a33(processFileItem, startAndEnd[0], startAndEnd[1], flowInput, flowOutput);
	}

	/**
	 * 流程范围
	 * 
	 */
	protected void a33(ProcessFileItem processFileItem, String... data) {
		createTitle(processFileItem);
		// 起始活动
		String startActive = JecnUtil.getValue("initialActivity");
		// 终止活动
		String endActive = JecnUtil.getValue("ceasingActivity");
		// 输入
		String input = JecnUtil.getValue("input");
		// 输出
		String output = JecnUtil.getValue("output");

		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { startActive, data[0] });
		rowData.add(new String[] { endActive, data[1] });
		rowData.add(new String[] { input, data[2] });
		rowData.add(new String[] { output, data[3] });
		float[] width = new float[] { 3.5F, 14F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());
	}

	/**
	 * 术语
	 */
	private void build_a34(ProcessFileItem processFileItem) {
		a34(processFileItem, processFileItem.getDataBean().getDefinitions());
	}

	protected void a34(ProcessFileItem processFileItem, List<TermDefinitionLibrary> definitions) {
		createTitle(processFileItem);
		// 名称
		String name = JecnUtil.getValue("name");
		// 定义
		String definition = JecnUtil.getValue("definition");

		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { name, definition });
		for (TermDefinitionLibrary term : definitions) {
			rowData.add(new String[] { term.getName(), term.getInstructions() });
		}
		float[] width = new float[] { 3.5F, 14F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());
	}

	/**
	 * 流程相关文件
	 */
	private void build_a35(ProcessFileItem processFileItem) {
		a35(processFileItem, processFileItem.getDataBean().getRelatedFiles());
	}

	protected void a35(ProcessFileItem processFileItem, List<FileWebBean> relatedFiles) {
		createTitle(processFileItem);
		// 文件名称
		String name = JecnUtil.getValue("fileName");
		// 文件编码
		String fileCode = JecnUtil.getValue("fileCode");

		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { fileCode, name });
		for (FileWebBean file : relatedFiles) {
			rowData.add(new String[] { file.getFileNum(), file.getFileName() });
		}
		float[] width = new float[] { 7F, 7F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());

	}

	/**
	 * 流程接口描述
	 */
	private void build_a36(ProcessFileItem processFileItem) {
		ProcessDownloadBean dataBean = processFileItem.getDataBean();
		a36(processFileItem, dataBean.getDescription());
	}

	/**
	 * 流程相关标准化文件
	 */
	private void build_a37(ProcessFileItem processFileItem) {
		a37(processFileItem, processFileItem.getDataBean().getStandardFileList());
	}

	protected void a37(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		List<String[]> data = new ArrayList<String[]>();
		for (String[] row : rowData) {
			data.add(new String[] { row[2], row[3] });
		}
		createTitle(processFileItem);
		// 文件名称
		String name = JecnUtil.getValue("fileName");
		// 文件编码
		String fileCode = JecnUtil.getValue("fileCode");
		data.add(0, new String[] { name, fileCode });
		float[] width = new float[] { 14, 3.5F };
		Table tbl = createTableItem(processFileItem, width, data);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	protected void a36(ProcessFileItem processFileItem, ProcessInterfacesDescriptionBean description) {
		createTitle(processFileItem);
		Sect sect = processFileItem.getBelongTo();
		ParagraphBean titleBean = new ParagraphBean(Align.left, textContentStyle().getFontBean().getFontFamily(),
				textContentStyle().getFontBean().getFontSize(), true);
		titleBean.setSpace(0.5f, 0f, new ParagraphLineRule(1.5f, LineRuleType.AUTO));
		if (description.getPid() != null && description.getPid() != 0) {
			sect.addParagraph(new Paragraph("*" + JecnUtil.getValue("correspondingUpProcess"), titleBean));
			List<String[]> rowData = new ArrayList<String[]>();
			rowData.add(new String[] { JecnUtil.getValue("fileCode"), JecnUtil.getValue("processFileName") });
			rowData.add(new String[] { description.getPnumber(), description.getPname() });
			float[] width = new float[] { 7F, 7F };
			Table tbl = createTableItem(processFileItem, width, rowData);
			tbl.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());
		}
		if (description.getUppers() != null && description.getUppers().size() > 0) {
			sect.addParagraph(new Paragraph("*" + JecnUtil.getValue("inProcessDescribe"), titleBean));
			createInterfaceTable(processFileItem, JecnUtil.getValue("topFlow"), description.getUppers());
		}

		if (description.getLowers() != null && description.getLowers().size() > 0) {
			sect.addParagraph(new Paragraph("*" + JecnUtil.getValue("outProcessDescribed"), titleBean));
			createInterfaceTable(processFileItem, JecnUtil.getValue("bottomFlow"), description.getLowers());
		}

		if (description.getProcedurals() != null && description.getProcedurals().size() > 0) {
			sect.addParagraph(new Paragraph("*" + JecnUtil.getValue("interfaceFlow"), titleBean));
			createInterfaceTable(processFileItem, JecnUtil.getValue("interfaceFlow"), description.getProcedurals());
		}

		if (description.getSons() != null && description.getSons().size() > 0) {
			sect.addParagraph(new Paragraph("*" + JecnUtil.getValue("sonFlow"), titleBean));
			createInterfaceTable(processFileItem, JecnUtil.getValue("sonFlow"), description.getSons());
		}
	}

	/**
	 * 创建一个接口的table,table的第二列的标题不同
	 * 
	 * @param processFileItem
	 * @param relatedProcessTitle
	 *            第二列的标题
	 * @param interfaces
	 *            数据
	 */
	protected void createInterfaceTable(ProcessFileItem processFileItem, String relatedProcessTitle,
			List<ProcessInterface> interfaces) {
		if (interfaces.size() == 0) {
			processFileItem.getBelongTo().addParagraph(getNoneParagraph());
		} else {
			List<String[]> rowData = new ArrayList<String[]>();
			rowData.add(new String[] { JecnUtil.getValue("flowInterface"), relatedProcessTitle,
					JecnUtil.getValue("processRequired"), JecnUtil.getValue("remark") });
			for (ProcessInterface i : interfaces) {
				rowData
						.add(new String[] { i.getImplName(), i.getRname(), i.getProcessRequirements(), i.getImplNote() });
			}
			float[] width = new float[] { 4.375F, 4.375F, 4.375F, 4.375F };
			Table tbl = createTableItem(processFileItem, width, rowData);
			tbl.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());
			tbl.getRow(0).setHeader(true);
		}
	}

	private Paragraph getNoneParagraph() {
		ParagraphBean pBean = textContentStyle();
		Paragraph none = new Paragraph(JecnUtil.getValue("none"), pBean);
		return none;
	}

	/**
	 * 初始化封面节
	 * 
	 * @param sect
	 */
	protected abstract void initTitleSect(final Sect titleSect);

	/**
	 * 初始化第一个内容节
	 * 
	 * @param sect
	 */
	protected abstract void initFirstSect(final Sect firstSect);

	/**
	 * 初始化流程图节
	 * 
	 * @param sect
	 */
	protected abstract void initFlowChartSect(final Sect flowChartSect);

	/**
	 * 初始化第二个节
	 * 
	 * @param sect
	 */
	protected abstract void initSecondSect(final Sect secondSect);

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	protected void createTextItem(ProcessFileItem processFileItem, String... content) {
		createTitle(processFileItem);
		processFileItem.getBelongTo().createParagraph(content, docProperty.getTextContentStyle());
	}

	/**
	 * 创建表格项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		return this.createTableItem(processFileItem.getBelongTo(), width, rowData);
	}

	/**
	 * 创建表格项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	protected Table createTableItem(Sect sect, float[] width, List<String[]> rowData) {
		if (docProperty.getNewTblWidth() > 0) {
			resizeTblWidth(width);
		}
		// 创建表格
		Table tbl = sect.createTable(docProperty.getTblStyle(), width);
		if (docProperty.getNewRowHeight() > 0) {
			tbl.createTableRow(rowData, docProperty.getTblContentStyle(), docProperty.getNewRowHeight());
		} else {
			tbl.createTableRow(rowData, docProperty.getTblContentStyle());
		}
		return tbl;
	}

	/**
	 * 重设表格宽度
	 * 
	 * @param oldWidth
	 */
	private void resizeTblWidth(float[] oldWidth) {
		float old = 0;
		for (float f : oldWidth) {
			old += f;
		}
		// 获取缩放比例
		float scale = Float.valueOf(String.format("%.2f", docProperty.getNewTblWidth() / old));
		for (int i = 0; i < oldWidth.length; i++) {
			oldWidth[i] = oldWidth[i] * scale;
		}
	}

	/**
	 * 创建标题
	 * 
	 * @param titleName
	 * @param contentSect
	 */
	public void createTitle(ProcessFileItem processFileItem) {
		processFileItem.getBelongTo().createParagraph(processFileItem.getItemTitle(), docProperty.getTextTitleStyle());
	}

	/**
	 * 目的
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a01(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 适用范围
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a02(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 术语定义
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a03(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 驱动规则
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param driveType
	 */
	protected void a04(ProcessFileItem processFileItem, FlowDriverBean flowDriver) {
		if (flowDriver == null) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 宽度
		float[] width = new float[] { 2.91F, 14.62F };
		// 驱动类型值
		String driverTypeVal = FLOW_DERVER_RULE.getName(flowDriver.getDriveType().intValue());
		// 数据集合
		List<String[]> rowData = new ArrayList<String[]>();
		// 驱动类型
		rowData.add(new String[] { JecnUtil.getValue("drivingType"), driverTypeVal });
		// 驱动规则
		String driverName = JecnUtil.getValue("drivingRules");
		if (this instanceof SDXCProcessModel) {
			driverName = "触发条件 ";
		}
		rowData.add(new String[] { driverName, flowDriver.getDriveRules() });
		if ("1".equals(processDownloadBean.getFlowDrivenRuleTimeConfig())) {
			// 频率
			rowData.add(new String[] { JecnUtil.getValue("frequency"), flowDriver.getFrequency() });
			if (JecnConfigTool.isUseNewDriver()) {
				// 类型
				String quantity = getDriverRuleType(flowDriver.getQuantity());
				rowData.add(new String[] { JecnUtil.getValue("type"), quantity });
				StringBuffer b = new StringBuffer();
				if (StringUtils.isNotBlank(quantity) && JecnUtil.isNotEmpty(flowDriver.getTimes())) {
					String q = flowDriver.getQuantity();
					concatDriverTime(flowDriver, b, q);
				}
				rowData.add(new String[] { "时间", b.toString() });
				rowData.add(new String[] { "提醒人", JecnUtil.concat(getDriverUsers(), "，") });
			} else {
				// 开始时间
				rowData.add(new String[] { JecnUtil.getValue("startTime"), flowDriver.getStartTime() });
				// 结束时间
				rowData.add(new String[] { JecnUtil.getValue("endTime"), flowDriver.getEndTime() });
				// 类型
				String quantity = getDriverRuleType(flowDriver.getQuantity());
				rowData.add(new String[] { JecnUtil.getValue("type"), quantity });
			}
		}
		// 生成表格
		Table tbl = createTableItem(processFileItem, width, rowData);
		// 设置第一列样式
		tbl.setCellStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow());
		tbl.setCellStyle(0, Valign.center);
		tbl.setCellStyle(1, Valign.center);
	}

	private List<String> getDriverUsers() {
		List<String> result = new ArrayList<String>();
		String pub = processDownloadBean.isPub() ? "" : "_T";
		String sql = "select b.true_name,b.people_id from jecn_flow_driver_email" + pub
				+ " a inner join jecn_user b on a.people_id=b.people_id and b.islock=0 where a.RELATED_ID=?";
		List<Object[]> objs = processDownloadBean.getBaseDao().listNativeSql(sql, processDownloadBean.getFlowId());
		for (Object[] obj : objs) {
			result.add(obj[0].toString());
		}
		return result;
	}

	private void concatDriverTime(FlowDriverBean flowDriver, StringBuffer b, String q) {
		String[] week = new String[] { "", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日" };
		/** 时间类型：1:日，2:周，3:月，4:季，5:年 */
		for (JecnFlowDriverTimeBase time : flowDriver.getTimes()) {
			if ("1".equals(q)) {
				b.append(time.getHour());
				b.append("时");
			} else if ("2".equals(q)) {
				b.append(week[time.getWeek()]);
				b.append(time.getHour());
				b.append("时");
			} else if ("3".equals(q)) {
				b.append(time.getDay());
				b.append("日");
				b.append(time.getHour());
				b.append("时");
			} else if ("4".equals(q)) {
				b.append("第");
				b.append(time.getMonth());
				b.append("月");
				b.append(time.getDay());
				b.append("日");
				b.append(time.getHour());
				b.append("时");
			} else if ("5".equals(q)) {
				b.append(time.getMonth());
				b.append("月");
				b.append(time.getDay());
				b.append("日");
				b.append(time.getHour());
				b.append("时");
			}
			b.append("\n");
		}
	}

	/**
	 * 获取驱动规则类型
	 * 
	 * @param quantity
	 * @return
	 */
	protected String getDriverRuleType(String quantity) {
		String timeType = "";
		if ("1".equals(quantity)) { // 日
			timeType = JecnUtil.getValue("day");
		} else if ("2".equals(quantity)) { // 周
			timeType = JecnUtil.getValue("weeks");
		} else if ("3".equals(quantity)) { // 月
			timeType = JecnUtil.getValue("month");
		} else if ("4".equals(quantity)) { // 季
			timeType = JecnUtil.getValue("season");
		} else if ("5".equals(quantity)) { // 年
			timeType = JecnUtil.getValue("years");
		}
		return timeType;
	}

	/**
	 * 流程输入
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a05(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 流程输出
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a06(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 关键活动
	 * 
	 * @param _count
	 * @param name
	 * @param keyActivityShowList
	 */
	protected void a07(ProcessFileItem processFileItem, List<KeyActivityBean> activeList) {
		if (activeList == null || activeList.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 关键活动
		String active = JecnUtil.getValue("keyActivities");
		// 活动编号
		String number = JecnUtil.getValue("activitynumbers");
		// 活动名称
		String title = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activeExplain = JecnUtil.getValue("activityIndicatingThat");
		// 重要说明
		String keyExplain = JecnUtil.getValue("keyShow");

		List<String[]> dataList = new ArrayList<String[]>();
		// 标题行
		dataList.add(new String[] { active, number, title, activeExplain, keyExplain });
		// 关键控制点(合规)是否显示
		boolean kcpAllow = kcpAllowShow();
		if (activeList != null) {
			for (KeyActivityBean keyActive : activeList) {
				if (!kcpAllow && "4".equals(keyActive.getActiveKey())) {
					continue;
				}
				String[] rowData = new String[5];
				if ("1".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("problemAreas");// 问题区域
				} else if ("2".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("keySuccessFactors");// 关键成功因素
				} else if ("3".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("keyControlPoint");// 关键控制点
				} else if ("4".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("keyControlPointAllow");// 关键控制点(合规)
				}
				rowData[1] = keyActive.getActivityNumber();
				rowData[2] = keyActive.getActivityName();
				rowData[3] = keyActive.getActivityShow();
				rowData[4] = keyActive.getActivityShowControl();
				dataList.add(rowData);
			}
		}
		float[] width = new float[] { 3, 2, 2.25F, 5.25F, 5F };
		Table tbl = createTableItem(processFileItem, width, dataList);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
		tbl.setCellStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow());
	}

	public boolean kcpAllowShow() {
		try {
			return JecnConfigTool.KCPFigureCompEnable(processDownloadBean.getBaseDao());
		} catch (Exception e) {
			log.error("", e);
		}
		return false;
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = JecnUtil.getValue("positionName");
		// 角色职责
		String roleResponsibility = JecnUtil.getValue("responsibilities");
		if (JecnConfigTool.isDongRuanYiLiaoLoginType()) {
			roleResponsibility = "职责";
		}
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData.add(0, new String[] { roleName, positionName, roleResponsibility });
		} else {
			List<String[]> newDataRows = new ArrayList<String[]>();
			String[] newData = null;
			for (String[] data : rowData) {
				newData = new String[] { data[0], data[2] };
				newDataRows.add(newData);
			}
			rowData.clear();
			rowData = newDataRows;
			// 标题行
			rowData.add(0, new String[] { roleName, roleResponsibility });
		}
		float[] width = new float[] { 4, 4, 9.5F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 流程图
	 * 
	 * @param titleName
	 *            标题
	 * @param imgPath
	 *            路径
	 */
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		createTitle(processFileItem);
		ParagraphBean pBean = new ParagraphBean(Align.center);
		for (Image img : images) {
			processFileItem.getBelongTo().createParagraph(img, pBean);
		}
	}

	/**
	 * 
	 * 流程图图片路径集合
	 * 
	 * @return List<String>
	 */
	private List<String> getFlowImgPathList(String path) {

		List<String> flowMapImgList = new ArrayList<String>();
		// 流程图整图路径
		String imgPath = JecnContants.jecn_path + path;
		if (StringUtils.isBlank(imgPath)) {
			return flowMapImgList;
		}
		// 获取和imgPath名称去后缀jpg的文件夹
		String dirPath = imgPath.substring(0, imgPath.lastIndexOf("."));
		if (StringUtils.isBlank(dirPath)) {
			return flowMapImgList;
		}
		File fileDir = new File(dirPath);
		if (fileDir.exists() && fileDir.isDirectory()) {
			String[] fileArray = fileDir.list();
			if (fileArray != null) {
				for (String jpgName : fileArray) {
					flowMapImgList.add(dirPath + "/" + jpgName);
				}
			}
		}
		if (flowMapImgList.size() == 0) {
			flowMapImgList.add(imgPath);
		}
		return flowMapImgList;
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {

		// 段落显示 1:段落；0:表格（默认）
		if (processDownloadBean.getFlowFileType() == 1) {
			a10InParagraph(processFileItem, rowData);
			return;
		}
		if (JecnContants.otherOperaType == 6) {// 南京石化
			// 标题
			createTitle(processFileItem);
			// 添加标题
			rowData.add(0, titles);
			float[] width = new float[] { 2.09F, 2.09F, 2.31F, 5, 3, 3 };
			// 南京石化，输入输出显示不根据配置项来
			if (JecnContants.otherOperaType != 6 && !JecnContants.showActInOutBox) {
				width = new float[] { 2.09F, 2.09F, 2.31F, 11 };
			}
			if (JecnConfigTool.showActTimeLine() && JecnContants.showActInOutBox) {
				width = new float[] { 2.09F, 2.09F, 2.31F, 5, 2, 2, 2 };
			} else if (JecnConfigTool.showActTimeLine() && !JecnContants.showActInOutBox) {
				width = new float[] { 2.09F, 2.09F, 2.31F, 8, 3 };
			}
			Table tbl = createTableItem(processFileItem, width, rowData);
			// 设置第一行居中
			tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
					.isTblTitleCrossPageBreaks());
		} else {
			word.createActivityTable(processFileItem);
		}
	}

	/**
	 * 流程记录
	 * 
	 * @param _count
	 * @param name
	 * @param processRecordList
	 */
	protected void a11(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 文件编号
		String fileNumber = JecnUtil.getValue("fileNumber");
		// 文件名称
		String fileName = JecnUtil.getValue("fileName");
		// 标题行
		rowData.add(0, new String[] { fileNumber, fileName });
		float[] width = new float[] { 3.5F, 14 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 操作规范
	 * 
	 * @param _count
	 * @param name
	 * @param operationTemplateList
	 */
	protected void a12(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 文件编号
		String fileNumber = JecnUtil.getValue("fileNumber");
		// 文件名称
		String fileName = JecnUtil.getValue("fileName");
		rowData.add(0, new String[] { fileNumber, fileName });
		float[] width = new float[] { 3.5F, 14 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 相关流程
	 * 
	 * @param _count
	 * @param name
	 * @param relatedProcessMap
	 */
	protected void a13(ProcessFileItem processFileItem, Map<String, List<RelatedProcessBean>> relatedProcessMap) {
		if (relatedProcessMap == null || relatedProcessMap.isEmpty() || relatedProcessMap.keySet().size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 类型
		String type = JecnUtil.getValue("type");
		// 流程编号
		String processNumber = JecnUtil.getValue("processNumber");
		// 流程名称
		String processName = JecnUtil.getValue("processName");
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { type, processNumber, processName });
		for (String key : relatedProcessMap.keySet()) {
			for (RelatedProcessBean relatedProcess : relatedProcessMap.get(key)) {
				// 添加数据
				rowData.add(new String[] { getRelatedProcessType(key), relatedProcess.getFlowNumber(),
						relatedProcess.getFlowName() });
			}
		}
		float[] width = new float[] { 3.5F, 3, 11 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
		tbl.setCellStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow());

	}

	protected String getRelatedProcessType(String key) {
		if ("1".equals(key)) {
			return JecnUtil.getValue("upstreamProcess"); // 上游流程
		} else if ("2".equals(key)) {
			return JecnUtil.getValue("downstreamProcess");// 下游流程
		} else if ("3".equals(key)) {
			return JecnUtil.getValue("processFlow");// 过程流程
		} else if ("4".equals(key)) {
			return JecnUtil.getValue("childProcess");// 子流程
		}
		return "";
	}

	/**
	 * 相关制度
	 * 
	 * @param _count
	 * @param name
	 * @param ruleNameList
	 */
	protected void a14(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);

		// 制度名称
		String name = JecnUtil.getValue("ruleName");
		// 类型
		String type = JecnUtil.getValue("type");
		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { name, type });
		for (String[] row : rowData) {
			data.add(new String[] { row[0], row[1] });
		}
		float[] width = new float[] { 14, 3.5F };
		Table tbl = createTableItem(processFileItem, width, data);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		// KPI配置bean
		List<JecnConfigItemBean> configItemBeanList = processDownloadBean.getConfigKpiItemBean();
		createTitle(processFileItem);
		ParagraphBean tPbean = docProperty.getTextTitleStyle();

		ParagraphBean pBean = new ParagraphBean(tPbean.getFontBean());
		pBean.getFontBean().setB(false);
		if (tPbean.getParagraphIndBean() != null) {
			pBean.setInd(tPbean.getParagraphIndBean().getLeft(), tPbean.getParagraphIndBean().getRight(), tPbean
					.getParagraphIndBean().getHanging(), 0);
		}
		// 表格标题
		List<String> title = null;
		// 表格内容
		List<List<String>> tabText = new ArrayList<List<String>>();

		for (String[] row : oldRowData) { // 对应一条配置数据
			title = new ArrayList<String>();
			List<String> rowList = new ArrayList<String>();
			int i = 0;
			for (JecnConfigItemBean config : configItemBeanList) {
				if ("0".equals(config.getValue())) {
					continue;
				}
				if (i > 4) {
					break;
				}
				i++;
				title.add(config.getName());
				rowList.add(getKpiContent(row, config.getMark()));
			}
			tabText.add(rowList);
		}
		List<String[]> tableData = new ArrayList<String[]>();// 一个表格数据
		String[] titleStr = (String[]) title.toArray(new String[title.size()]);
		tableData.add(titleStr);
		for (List<String> row : tabText) {
			String[] contentStr = (String[]) row.toArray(new String[row.size()]);
			tableData.add(contentStr);
		}
		Table tbl = createTableItem(processFileItem, new float[] { 3.18F, 3.66F, 3.66F, 3.3F, 3.4F }, tableData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * * 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6 KIP值单位名称 7 数据获取方式 8
	 * 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12 目的 13 测量点 14 统计周期 15 说明
	 */
	private String getKpiContent(String[] row, String mark) {
		if ("kpiName".equals(mark)) {// KPI名称
			return row[0];
		} else if ("kpiType".equals(mark)) {// KPI类型
			return row[1];
		} else if ("kpiUtilName".equals(mark)) {// KPI单位名称
			return JecnResourceUtil.getKpiVertical(Integer.valueOf(row[6]));
		} else if ("kpiTargetValue".equals(mark)) {// KPI目标值
			return row[2];
		} else if ("kpiTimeAndFrequency".equals(mark)) {// 数据统计时间频率
			return JecnResourceUtil.getKpiHorizontal(Integer.valueOf(row[5]));
		} else if ("kpiDefined".equals(mark)) {// KPI定义
			return row[3];
		} else if ("kpiDesignFormulas".equals(mark)) {// 流程KPI计算方式
			return row[4];
		} else if ("kpiDataProvider".equals(mark)) {// 数据提供者
			return row[10];
		} else if ("kpiDataAccess".equals(mark)) {// 数据获取方式
			return JecnResourceUtil.getKpiDataMethod(Integer.valueOf(row[7]));
		} else if ("kpiIdSystem".equals(mark)) {// IT 系统
			return row[16];
		} else if ("kpiSourceIndex".equals(mark)) {// 指标来源
			return JecnResourceUtil.getKpiTargetType(Integer.valueOf(row[9]));
		} else if ("kpiRrelevancy".equals(mark)) {// 相关度
			return JecnResourceUtil.getKpiRelevance(Integer.valueOf(row[8]));
		} else if ("kpiSupportLevelIndicator".equals(mark)) {// 支持的一级指标
			return row[11];
		} else if ("kpiPurpose".equals(mark)) {// 设置目的
			return row[12];
		} else if ("kpiPoint".equals(mark)) {// 测量点
			return row[13];
		} else if ("kpiPeriod".equals(mark)) {// 统计周期
			return row[14];
		} else if ("kpiExplain".equals(mark)) {// 说明
			return row[15];
		}
		return "";
	}

	/**
	 * 客户
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a16(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 不适用范围
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a17(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 概述
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a18(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 补充说明
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a19(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 记录保存
	 * 
	 * @param _count
	 * @param name
	 * @param flowRecordList
	 */
	protected void a20(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 记录名称
		String recordName = JecnUtil.getValue("recordName");
		// 保存责任人
		String saveResponsiblePersons = JecnUtil.getValue("saveResponsiblePersons");
		// 保存场所
		String savePlace = JecnUtil.getValue("savePlace");
		// 归档时间
		String filingTime = JecnUtil.getValue("filingTime");
		// 保存期限
		String expirationDate = JecnUtil.getValue("storageLife");
		// 到期处理方式
		String handlingDue = JecnUtil.getValue("treatmentDue");
		Table tbl = null;
		if (JecnConfigTool.isShowRecordTransferAndNum()) {
			float[] width = new float[] { 2.5F, 2.5F, 2.5F, 2.5F, 2.5F, 2.5F, 2.5F, 4 };
			List<String[]> rowData2 = new ArrayList<String[]>();
			rowData2.add(0, new String[] { recordName, "编号", "移交责任人", saveResponsiblePersons, savePlace, filingTime,
					expirationDate, handlingDue });
			for (String[] row : rowData) {
				rowData2.add(new String[] { row[0], row[6], row[7], row[1], row[2], row[3], row[4], row[5] });
			}
			tbl = createTableItem(processFileItem, width, rowData2);
		} else {
			rowData.add(0, new String[] { recordName, saveResponsiblePersons, savePlace, filingTime, expirationDate,
					handlingDue });
			float[] width = new float[] { 3.5F, 2.5F, 2.5F, 2.5F, 2.5F, 4 };
			tbl = createTableItem(processFileItem, width, rowData);
		}
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 相关标准
	 * 
	 * @param _count
	 * @param name
	 * @param standardList
	 */
	protected void a21(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 标准名称
		String name = JecnUtil.getValue("standardName");
		// 类型
		String type = "目录";
		rowData.add(0, new String[] { name, type });
		float[] width = new float[] { 8, 9.5F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());

	}

	/**
	 * 流程边界
	 * 
	 * @param name
	 * @param startEndActive
	 * @param contentSect
	 */
	protected void a22(ProcessFileItem processFileItem, String... startEndActive) {
		createTitle(processFileItem);
		// 起始活动
		String startActive = JecnUtil.getValue("initialActivity");
		// 终止活动
		String endActive = JecnUtil.getValue("ceasingActivity");

		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { startActive, startEndActive[0] });
		rowData.add(new String[] { endActive, startEndActive[1] });
		float[] width = new float[] { 3.5F, 14F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow());
	}

	/**
	 * 流程责任属性
	 * 
	 * @param name
	 * @param responFlow
	 * @param contentSect
	 */
	protected void a23(ProcessFileItem processFileItem, ProcessAttributeBean responFlow) {
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();

		if ("1".equals(JecnContants.getValue("guardianPeople"))) {
			// 流程监护人
			String guardian = JecnUtil.getValue("processTheGuardian");
			rowData.add(new String[] { guardian, responFlow.getGuardianName() });
		}
		if ("1".equals(JecnContants.getValue("3"))) {
			// 流程责任人
			String responsiblePerson = JecnUtil.getValue("processResponsiblePersons");
			rowData.add(new String[] { responsiblePerson, responFlow.getDutyUserName() });
		}
		if ("1".equals(JecnContants.getValue("fictionPeople"))) {
			// 流程拟制人
			String artificialPerson = JecnUtil.getValue("processArtificialPerson");
			rowData.add(new String[] { artificialPerson, responFlow.getFictionPeopleName() });
		}
		if ("1".equals(JecnContants.getValue("5"))) {
			// 流程责任部门
			String responsibleDept = JecnUtil.getValue("processResponsibilityDepartment");
			rowData.add(new String[] { responsibleDept, responFlow.getDutyOrgName() });
		}
		float[] width = new float[] { 3.5F, 14F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow());
	}

	/**
	 * 流程文控信息
	 * 
	 * @param name
	 * @param documentList
	 * @param contentSect
	 */
	protected void a24(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 版本号
		String version = JecnUtil.getValue("versionNumber");
		// 变更说明
		String descOfChange = JecnUtil.getValue("changeThat");
		// 发布日期
		String releaseDate = JecnUtil.getValue("releaseDate");
		// 流程拟稿人
		String artificialPerson = "拟稿人";
		// 审批人
		String approver = JecnUtil.getValue("theApprover");
		rowData.add(0, new String[] { version, descOfChange, releaseDate, artificialPerson, approver });
		float[] width = new float[] { 2, 6, 2.5F, 2.5F, 4.5F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 相关风险
	 * 
	 * @param _count
	 * @param name
	 * @param rilsList
	 */
	protected void a25(ProcessFileItem processFileItem, List<JecnRisk> riskList) {
		if (riskList == null || riskList.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 文件编号
		String riskNum = JecnUtil.getValue("riskNum");
		// 文件名称
		String riskName = JecnUtil.getValue("riskName");
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { riskNum, riskName });
		for (JecnRisk risk : riskList) {
			rowData.add(new String[] { risk.getRiskCode(), risk.getRiskName() });
		}
		float[] width = new float[] { 3.5F, 14 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 相关文件
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a26(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 自定义1
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a27(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 自定义2
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a28(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 自定义3
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a29(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 自定义4
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a30(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/**
	 * 自定义5
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a31(ProcessFileItem processFileItem, String... content) {
		createTextItem(processFileItem, content);
	}

	/****
	 * 输入和输出
	 * 
	 * @param titleName
	 * @param inputorout
	 */
	protected void a32(ProcessFileItem processFileItem, String[] input, String[] output) {
		createTitle(processFileItem);
		// 输入
		Paragraph p = processFileItem.getBelongTo().createParagraph(JecnUtil.getValue("inputC"),
				docProperty.getTextContentStyle());
		p.appendTextRun(input, docProperty.getTextContentStyle().getFontBean());
		// 输出
		p = processFileItem.getBelongTo().createParagraph(JecnUtil.getValue("outputC"),
				docProperty.getTextContentStyle());
		p.appendTextRun(output, docProperty.getTextContentStyle().getFontBean());
	}

	/**
	 * IT系统
	 */
	private void build_a38(ProcessFileItem processFileItem) {
		a38(processFileItem, processFileItem.getDataBean().getActiveItSystems());
	}

	protected void a38(ProcessFileItem processFileItem, List<ActiveItSystem> activeItSystems) {
		if (activeItSystems == null || activeItSystems.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 文件编码
		String code = JecnUtil.getValue("activeNum");
		// 文件名称
		String name = JecnUtil.getValue("activeName");
		String sysName = JecnUtil.getValue("systemName");
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { code, name, sysName });
		for (ActiveItSystem itSystem : activeItSystems) {
			rowData.add(new String[] { itSystem.getActiveNum(), itSystem.getActiveName(), itSystem.getSystemName() });
		}
		float[] width = new float[] { 5F, 4F, 5F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());

	}

	/**
	 * 条款化输入
	 */
	private void build_a39(ProcessFileItem processFileItem) {
		a39(processFileItem, processFileItem.getDataBean().getInData());
	}

	protected void a39(ProcessFileItem processFileItem, List<JecnFlowInoutData> list) {
		a39a40(processFileItem, list, 0);
	}

	/**
	 * 条款化输出
	 */
	private void build_a40(ProcessFileItem processFileItem) {
		a40(processFileItem, processFileItem, processFileItem.getDataBean().getOutData());
	}

	protected void a40(ProcessFileItem processFileItem, ProcessFileItem processFileItem2, List<JecnFlowInoutData> list) {
		a39a40(processFileItem, list, 1);
	}

	protected void a39a40(ProcessFileItem processFileItem, List<JecnFlowInoutData> list, int type) {
		if (list == null || list.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 名称
		String name = "名称";
		String explain = "说明";
		List<String[]> rowData = new ArrayList<String[]>();
		String a = "提供者";
		if (type == 1) {
			a = "接收者";
		}
		rowData.add(new String[] { name, a, explain });
		for (JecnFlowInoutData data : list) {
			String p = JecnUtil.nullToEmpty(data.getInout().getPos());
			String pos = concat(data.getPoses(), "\n");
			String group = concat(data.getPosGroups(), "\n");
			List<String> temp = new ArrayList<String>();
			if (StringUtils.isNotBlank(p)) {
				temp.add(p);
			}
			if (StringUtils.isNotBlank(pos)) {
				temp.add(pos);
			}
			if (StringUtils.isNotBlank(group)) {
				temp.add(group);
			}
			String posAndGroup = concat(temp, "\n");
			rowData.add(new String[] { data.getInout().getName(), posAndGroup, data.getInout().getExplain() });
		}
		float[] width = new float[] { 2F, 6F, 5F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	protected String concat(List<JecnTreeBean> treeBeans) {
		if (JecnUtil.collectIsEmpty(treeBeans)) {
			return "";
		}
		List<String> a = new ArrayList<String>();
		for (JecnTreeBean t : treeBeans) {
			a.add(t.getName());
		}
		return concat(a);
	}

	protected String concat(List<JecnTreeBean> treeBeans, String delimiter) {
		if (JecnUtil.collectIsEmpty(treeBeans)) {
			return "";
		}
		List<String> a = new ArrayList<String>();
		for (JecnTreeBean t : treeBeans) {
			a.add(t.getName());
		}
		return concat(a, delimiter);
	}

	public ProcessFileProperty getDocProperty() {
		return docProperty;
	}

	public String nullToEmpty(Object text) {
		return JecnUtil.nullToEmpty(text);
	}

	public String getPublicName() {
		return JecnUtil.custOmResourceMap.get("public");
	}

	public String getSecretName() {
		return JecnUtil.custOmResourceMap.get("secret");
	}

	/**
	 * 获得操作说明中配置获得公开和秘密的名称
	 * 
	 * @return
	 */
	public String getPubOrSec() {
		return "0".equals(processDownloadBean.getFlowIsPublic().trim()) ? getSecretName() : getPublicName();
	}

	/**
	 * 先从菜单设置的附件中找，再从images找，再从1.png中找，
	 * 
	 * @param width
	 * @param height
	 * @return
	 */
	public WordGraph getLogoImage(float width, float height) {
		Image logo = null;
		if (processDownloadBean.getFlowFileHeadData() != null
				&& StringUtils.isNotBlank(processDownloadBean.getFlowFileHeadData().getFilePath())
				&& new File(processDownloadBean.getFlowFileHeadData().getFilePath()).exists()) {
			logo = new Image(processDownloadBean.getFlowFileHeadData().getFilePath());
		} else if (new File(docProperty.getLogoImage()).exists()) {
			logo = new Image(docProperty.getLogoImage());
		} else {
			logo = getImageByClass(width, height, "1.png");
		}
		logo.setSize(width, height);
		return logo;
	}

	public WordGraph getImage(float width, float height, String imageName) {
		Image logo = new Image(JecnPath.APP_PATH + "images/" + imageName);
		logo.setSize(width, height);
		return logo;
	}

	public Image getImageByClass(float width, float height, String imageName) {
		Image logo = new Image("");
		try {
			logo = new Image(URLDecoder.decode(getClass().getResource(imageName).getFile(), "UTF-8"));
			logo.setSize(width, height);
		} catch (UnsupportedEncodingException e) {
			log.error("", e);
		}
		return logo;
	}

	public String dateToStr(Date date) {
		if (date == null) {
			return "";
		}
		return JecnUtil.DateToStr(date, "yyyy-MM-dd");
	}

	public String dateToStr(Date date, String formate) {
		if (date == null) {
			return "";
		}
		return JecnUtil.DateToStr(date, formate);
	}

	public String concat(Collection<? extends Object> cols, CharSequence delimiter) {
		return JecnUtil.concat(cols, delimiter);
	}

	public String concat(Collection<? extends Object> cols) {
		return concat(cols, DEFAULT_DELIMITER);
	}

	protected final void addAntiFake(AntiFake antiFake, Header... headers) {
		for (Header h : headers) {
			h.addAntiFake(antiFake);
		}
	}

	protected final void addAntiFake(AntiFake antiFake, List<Header> headers) {
		for (Header h : headers) {
			h.addAntiFake(antiFake);
		}
	}

	private final void addAntiFake(AntiFake antiFake) {
		if (antiFake == null) {
			return;
		}
		addAntiFake(antiFake, needAddAntiFakeSect());
	}

	protected Sect[] needAddAntiFakeSect() {
		return new Sect[] { docProperty.getTitleSect(), docProperty.getFirstSect(), docProperty.getSecondSect() };
	}

	protected final void addAntiFake(AntiFake antiFake, Sect... sects) {
		for (Sect sect : sects) {
			addAntiFake(antiFake, sect.getHeaders());
		}
	}

	protected void setTableParagraph(Table table, int[] rows, int[] cols, ParagraphBean paragraphBean) {
		for (int row : rows) {
			TableRow tRow = table.getRow(row);
			for (int cell : cols) {
				TableCell tCell = tRow.getCell(cell);
				setParagraphBean(tCell.getParagraphs(), paragraphBean);
			}
		}
	}

	protected void setParagraphBean(Paragraph p, ParagraphBean pBean) {
		p.setParagraphBean(pBean);
	}

	protected void setParagraphBean(List<Paragraph> ps, ParagraphBean pBean) {
		for (Paragraph p : ps) {
			setParagraphBean(p, pBean);
		}
	}

}
