package epros.designer.gui.system.config.ui.dialog;

import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.util.JecnProperties;

/**
 * 
 * 制度配置对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnRuleConfigJDialog extends JecnAbtractBaseConfigDialog {

	public JecnRuleConfigJDialog(JecnConfigDesgBean configBean) {
		super(configBean);
		this.setTitle(JecnProperties.getValue("setTitleRuleName"));
	}
}
