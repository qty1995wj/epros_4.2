package epros.designer.gui.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class StandardChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(StandardChooseDialog.class);
	// stardarType 0:只能选择标准文件，1：不能选择流程图和流程地图
	private int stardarType = 0;

	public StandardChooseDialog(List<JecnTreeBean> list) {
		super(list, 4);
		stardarType = 1;
		this.setTitle(JecnProperties.getValue("ISO_Standard"));
	}

	public StandardChooseDialog(List<JecnTreeBean> list, int stardarType) {
		super(list, 20);
		this.stardarType = stardarType;
		this.setTitle(JecnProperties.getValue("ISO_Standard"));
	}

	public StandardChooseDialog(List<JecnTreeBean> list, int stardarType, JecnDialog jecnDialog) {
		super(list, 20, jecnDialog);
		this.stardarType = stardarType;
		this.setTitle(JecnProperties.getValue("ISO_Standard"));
	}

	public StandardChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 4, jecnDialog);
		this.setTitle(JecnProperties.getValue("ISO_Standard"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			List<JecnTreeBean> listTreeBean = null;
			if (stardarType == 0) {
				listTreeBean = ConnectionPool.getStandardAction()
						.searchStandarFileByName(name, JecnConstants.projectId);
			} else if (stardarType == 1) {
				listTreeBean = ConnectionPool.getStandardAction().searchByName(name, JecnConstants.projectId,
						JecnDesignerCommon.getSecondAdminUserId());
			} else if (stardarType == 2) {
				// TODO
			}
			return listTreeBean;

		} catch (Exception e) {
			log.error("StandardChooseDialog searchByName is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("standardNameC");// 标准名称：
	}

	@Override
	public JecnTree getJecnTree() {
		HighEfficiencyStandardTree tree = new HighEfficiencyStandardTree();
		tree.setTreeType(1);
		return tree;
		// if (JecnConstants.isMysql) {
		// return new RoutineStandardTree();
		// } else {
		// return new HighEfficiencyStandardTree();
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("standardName"));// 标准名称：
		return title;
	}

}
