package com.jecn.epros.server.action.web.login.ad.jiangbolong;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnContants;

/**
 * 
 * 江波龙：单点登录
 * 
 * @author ZXH
 * 
 */
public class JecnJiangBLWebLoginAction extends JecnAbstractADLoginAction {

	public JecnJiangBLWebLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 单点登录入口
	 * 
	 * @return String
	 */
	@Override
	public String login() {
		try {
			// 单点登录传递过来的登录名称
			String adLoginName = null;

			Object loginlotus = loginAction.getSession().getAttribute("login_Name");

			// 测试
//			 Object loginlotus=loginAction.getRequest().getParameter("login_Name");

			log.info("loginlotus = " + loginlotus);
			if (loginlotus != null) {
				adLoginName = String.valueOf(loginlotus);
			}

			if (StringUtils.isBlank(adLoginName)) {// 单点登录名称为空，执行普通登录路径
				String callbackFunc = loginAction.getRequest().getParameter("callback");
				loginAction.outJsonString(callbackFunc + "({\"login_Name\":" + adLoginName + "})");
				return null;
			}
			this.loginAction.getSession().setAttribute(JecnContants.SESSION_KEY, "");
			// 验证登录
			return this.loginByLoginName(adLoginName);
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			loginAction.outJsonString("({\"timeOut\":" + "true" + "})");
			return null;
		}
	}
}
