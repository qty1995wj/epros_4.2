package com.jecn.epros.server.service.popedom;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.log.JecnPersonalPermRecord;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.LoginBean;
import com.jecn.epros.server.bean.popedom.UserAddReturnBean;
import com.jecn.epros.server.bean.popedom.UserEditBean;
import com.jecn.epros.server.bean.popedom.UserInfo;
import com.jecn.epros.server.bean.popedom.UserPageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.popedom.UserWebInfoBean;
import com.jecn.epros.server.webBean.popedom.UserWebSearchBean;

public interface IPersonService extends IBaseService<JecnUser, Long> {

	/**
	 * @author yxw 2012-5-22
	 * @description:增加人员
	 * @param jecnUser
	 * @param figureIds
	 *            岗位Id集合
	 * @param roleIds
	 *            角色Id集合
	 * @return
	 * @throws Exception
	 */
	public UserAddReturnBean savePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:编辑人员
	 * @param jecnUser
	 * @param figureIds
	 *            岗位Id集合
	 * @param roleIds
	 *            角色Id集合
	 * @return
	 * @throws Exception
	 */
	public UserAddReturnBean updatePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception;

	/**
	 * @author yxw 2012-7-23
	 * @description:根据岗位查询人员
	 * @param positionId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildPoss(Long positionId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:搜索(除mysql)
	 * @param orgId
	 * @param roleId
	 * @param loginName
	 * @param trueName
	 * @return
	 * @throws Exception
	 */
	public UserPageBean searchUser(Long orgId, String roleType, String loginName, String trueName, int curPage,
			String operationType, int pageSize, Long projectId, Long isDelete, boolean isLogin) throws Exception;

	/**
	 * @author yxw 2012-9-3
	 * @description:根据名称查询
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchUserByName(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:获得人员信息
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public UserEditBean getUserEditBeanByPeopleId(Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:解除登陆
	 * @param peopleId
	 * @throws Exception
	 */
	public void updateLogin(Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:删除人员
	 * @param listPeopleIds
	 * @return 0正确删除 1不能删除所有管理员 至少留有一个
	 * @throws Exception
	 */
	public int delUsers(List<Long> listPeopleIds) throws Exception;

	/**
	 * @author 2012-12-24
	 * @description:恢复人员
	 * @param listPeopleIds
	 * @throws Exception
	 */
	public void restorationUsers(List<Long> listPeopleIds) throws Exception;

	/**
	 * 
	 * @author yxw 2012-5-28
	 * @description: 新建用户时判断登录名是否重复
	 * @param loginName
	 * @return
	 * @throws Exception
	 */
	public List<JecnUser> getUserByLoginName(String loginName) throws Exception;

	/**
	 * @author yxw 2012-5-28
	 * @description:更新用户时判断登录名是否重复
	 * @param loginName
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<JecnUser> getUserByLoginName(String loginName, Long id) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：登录设计器
	 * @param loginName
	 * @param passward
	 * @param loginType
	 *            :2：域用户登录，验证登录名
	 * @return
	 * @throws Exception
	 */
	public LoginBean loginDesign(String loginName, String passward, String macAddressClient, int loginType)
			throws Exception;

	/**
	 * @author zhangchen Aug 7, 2012
	 * @description：添加人员：检查登录名是不是存在
	 * @param loginName
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean isExistLoginNameAdd(String loginName, Long projectId) throws Exception;

	/**
	 * @author zhangchen Aug 7, 2012
	 * @description：编辑人员：检查登录名是不是存在
	 * @param loginName
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean isExistLoginNameEdit(String loginName, Long projectId, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-8-7
	 * @description:根据组织查询人员
	 * @param orgId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPersonByOrg(Long orgId) throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description:解除此用户所占用的资源
	 * @param userId
	 * @throws Exception
	 */
	public void moveEditAllByLoginName(Long userId) throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description:解除所有占用的资源
	 * @param loginName
	 * @throws Exception
	 */
	public void moveEditAll() throws Exception;

	/**
	 * 获得登陆bean
	 * 
	 * @param loginName
	 * @param password
	 * @param loginType
	 *            true为验证密码，false为不验证密码
	 * @return
	 * @throws Exception
	 */
	public WebLoginBean getWebLoginBean(String loginName, String password, boolean loginType) throws Exception;

	/**
	 * @author hyl 根据用户peopleId获得用户信息
	 * @return
	 * @throws Exception
	 */
	public UserInfo getUserInfoById(Long peopleId) throws Exception;

	/**
	 * @author yxw 2013-1-11
	 * @description:用户密码修改
	 * @param userId
	 *            用户ID
	 * @param newPassword
	 *            新密码-- 加密后密码
	 * @throws Exception
	 */
	public void userPasswordUpdate(Long userId, String newPassword) throws Exception;

	/**
	 * @author yxw 2013-1-14
	 * @description:获取所有的管理员id集合
	 * @return
	 * @throws Exception
	 */
	public List<Long> getAllAdminList() throws Exception;

	/**
	 * 根据 人员主键ID获取登录人员信息
	 * 
	 * @param peopleId
	 *            登录人主键ID
	 * @return JecnUser 登录人人员信息
	 * @throws Exception
	 */
	public JecnUser getUserById(Long peopleId) throws Exception;

	/**
	 * @author yxw 2013-3-7
	 * @description:
	 * @param userWebSearchBean
	 * @return
	 * @throws Exception
	 */
	public int getTotalPerson(UserWebSearchBean userWebSearchBean) throws Exception;

	/**
	 * @author yxw 2013-3-7
	 * @description:人员管理列表
	 * @param userWebSearchBean
	 *            搜索条件
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页显示行数
	 * @return
	 * @throws Exception
	 */
	public List<UserWebInfoBean> getListPerson(UserWebSearchBean userWebSearchBean, int start, int limit)
			throws Exception;

	/**
	 * 根据岗位ID 获取人员岗位关联关系表数据
	 * 
	 * @param figureId
	 *            岗位Id
	 * @return
	 * @throws Exception
	 */
	public List<Long> getPosUserInfoList(Long figureId) throws Exception;

	/**
	 * 添加人员 验证最大用户
	 * 
	 * @return
	 * @throws Exception
	 */
	public int addPeopleValidate() throws Exception;

	/**
	 * 角色权限获取人员选择结果集
	 * 
	 * @param name
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;

	/**
	 * 
	 * @param peopleId
	 * @throws Exception
	 */
	public void updateJecnUser(JecnUser jecnUser) throws Exception;

	public JecnUser getJecnUserByPeopleId(Long peopleId);

	JecnUser getJecnUserByEmail(String email) throws Exception;
	
	JecnUser getJecnUserByPhone(String phone) throws Exception;

	JecnUser isLoginByDomainName(String userName);

	public int findAdminUseCount();

	public int findDesignUseCount();

	public JecnPersonalPermRecord getPersionalPermRecord(Long id, int type, Long peopleId) throws Exception;

	public Map<String, String> getJecnIftekSyncUser(List<String> name);

	public List<JecnTreeBean> searchUserByIftekSyncLoginName(List<String> names, Long projectId);

	public String getListUserSearchBean(String loginName);
}
