package com.jecn.webservice.wanhua;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.jecn.webservice.wanhua.bean.WanHuaDept;

/**
 * 万华- 人员组织同步接口
 * 
 * @author xiaohu
 * 
 */
@WebService(endpointInterface = "IWanHuaOADeptService")
public interface IWanHuaOADeptService {

	@WebMethod(operationName = "saveDepts")
	public String saveDepts(List<WanHuaDept> depts);
}
