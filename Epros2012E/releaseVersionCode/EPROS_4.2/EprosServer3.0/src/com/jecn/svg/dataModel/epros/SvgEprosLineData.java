package com.jecn.svg.dataModel.epros;

public class SvgEprosLineData {
	private Long id;// 主键ID
	private Long figureId;// 流程元素表主键ID：连接线这种元素，存在多个小线段，线段基本属性存储在流程元素表中，小线段数据存储在此表中。
	private Long startX;// 线的开始点的X
	private Long startY;// 线的开始点的Y
	private Long endX;// 线的结束点的X
	private Long endY;// 线的结束点的Y

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public Long getStartX() {
		return startX;
	}

	public void setStartX(Long startX) {
		this.startX = startX;
	}

	public Long getStartY() {
		return startY;
	}

	public void setStartY(Long startY) {
		this.startY = startY;
	}

	public Long getEndX() {
		return endX;
	}

	public void setEndX(Long endX) {
		this.endX = endX;
	}

	public Long getEndY() {
		return endY;
	}

	public void setEndY(Long endY) {
		this.endY = endY;
	}

	/**
	 * 横线
	 * 
	 * @return
	 */
	public boolean horizontalLine() {
		return this.getIntSY() == this.getIntEY() ? true : false;
	}

	/**
	 * 纵线
	 * 
	 * @return
	 */
	public boolean verticalLine() {
		return this.getIntSX() == this.getIntEX() ? true : false;
	}

	public int getIntSX() {
		return this.getStartX().intValue();
	}

	public int getIntEX() {
		return this.getEndX().intValue();
	}

	public int getIntSY() {
		return this.getStartY().intValue();
	}

	public int getIntEY() {
		return this.getEndY().intValue();
	}
}
