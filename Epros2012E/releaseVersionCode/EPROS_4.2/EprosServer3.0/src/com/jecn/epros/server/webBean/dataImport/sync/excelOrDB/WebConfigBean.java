package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;


public class WebConfigBean extends AbstractConfigBean{
	
	/***
	 * 同步类型
	 */
	private Integer syncType;

	public Integer getSyncType() {
		return syncType;
	}

	public void setSyncType(Integer syncType) {
		this.syncType = syncType;
	}
}
