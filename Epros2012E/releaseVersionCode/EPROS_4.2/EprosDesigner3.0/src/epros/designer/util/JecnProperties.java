package epros.designer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnResourceUtil;

public class JecnProperties {

	private final static Log log = LogFactory.getLog(JecnProperties.class);
	/** 资源文件库--国际化 */
	public static PropertyResourceBundle resourceBundle;

	public static PropertyResourceBundle configBundle;
	
	/** 数据库动态加载 配置*/
	public static Map<String, String> custOmResourceMap;

	public static void loadProperties() {
		resourceBundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("resources.JecnEprosDesigner_"+ JecnResourceUtil.getLocale());
		configBundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("resources.JecnConfig");
		custOmResourceMap = new HashMap<String, String>();
	}
	
	public static ResourceBundle getResourceBundle(){
		return resourceBundle;
	}
	
	public static String getBaseName(){
		return "resources.JecnEprosDesigner_"+ JecnResourceUtil.getLocale();
	}
	
	public static String getValue(String key) {
		try {
			if (custOmResourceMap.containsKey(key)) {
				return custOmResourceMap.get(key);
			}
			return resourceBundle.getString(key.trim());
		} catch (Exception e) {
			log.error("JecnProperties getValue is error", e);
			return "";
		}

	}

	public static String getConfigValue(String key) {
		return configBundle.getString(key.trim());
	}

	public static void setConfigValue(String key, String value) {
		Properties properties = new Properties();
		try {
			File file = new File(JecnProperties.class.getResource("/").toURI().getPath()
					+ "resources/JecnConfig.properties");
			FileInputStream in = new FileInputStream(file);
			properties.load(in);
			OutputStream out = new FileOutputStream(file);
			properties.setProperty(key, value);
			properties.store(out, null);
			out.flush();
			out.close();
			in.close();
		} catch (Exception ex) {
			log.error("JecnProperties setConfigValue is error", ex);
			JecnOptionPane.showMessageDialog(null, "");
			return;
		}
	}
}
