package com.jecn.epros.server.bean.system;

/**
 * 文件收藏的bean
 * @author user
 *
 */
public class FavoriteUpdate {
	
	private Long rId;
	private String rName;
	private Integer starType;
	private Integer taskType;
	private Long peopleId;
	
	
	public Integer getTaskType() {
		return taskType;
	}
	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}
	public Long getrId() {
		return rId;
	}
	public void setrId(Long rId) {
		this.rId = rId;
	}
	public String getrName() {
		return rName;
	}
	public void setrName(String rName) {
		this.rName = rName;
	}
	public Integer getStarType() {
		return starType;
	}
	public void setStarType(Integer starType) {
		this.starType = starType;
	}
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	
}
