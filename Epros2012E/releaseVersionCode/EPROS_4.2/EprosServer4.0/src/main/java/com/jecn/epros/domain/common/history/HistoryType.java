package com.jecn.epros.domain.common.history;

/**
 * Created by thinkpad on 2017/1/4.
 */
public enum HistoryType {
    process(0),
    file(1),
    ruleModelFile(2),
    ruleFile(3),
    processMap(4),
    processFile(0);


    private Integer value;

    HistoryType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
