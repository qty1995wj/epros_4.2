package com.jecn.epros.server.action.web.login.ad.cetc10;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.jecn.authentication.AuthenticationFirefoxRequestUtil;
import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.webservice.cetc10.sso.SSOWebServiceLocator;
import com.jecn.webservice.cetc10.sso.SSOWebServiceSoapStub;
import com.jecn.webservice.cetc10.sso.UserInfo;

/**
 * 十所单点登录
 * 
 * @author ZXH
 * @date 2016-7-5上午11:51:19
 */
public class Cetc10LoginAction extends JecnAbstractADLoginAction {
	static {
		JecnCetc10AfterItem.start();
	}

	public Cetc10LoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {
			if (StringUtils.isNotBlank(this.loginAction.getLoginName())
					&& StringUtils.isNotBlank(this.loginAction.getPassword())) {// 存在用户名和密码
				log.info("普通登录，登录名:" + this.loginAction.getLoginName());
				return loginAction.loginGen();
			}
			boolean isChrome = AuthenticationFirefoxRequestUtil.isChromeBrower(loginAction.getRequest());
			boolean isIE = isIEBrower(loginAction.getRequest());
			String agent = loginAction.getRequest().getHeader("User-Agent").toLowerCase();
			log.info(agent);
			if ((isChrome && chromeVersionSmallThan(loginAction.getRequest(), 49))
					|| (isIE && ieVersionSmallThan(loginAction.getRequest(), 11)) || (!isChrome && !isIE)) {
				AuthenticationFirefoxRequestUtil.fireChromeBrowserRequest(getLoginUrl(loginAction), loginAction
						.getResponse(), JecnCetc10AfterItem.FireFox_URL, JecnCetc10AfterItem.FireFox_URL1);
				return LoginAction.INPUT;
			}

			// AXIS 方式生成客户端
			SSOWebServiceLocator local = new SSOWebServiceLocator();
			SSOWebServiceSoapStub stub = (SSOWebServiceSoapStub) local.getSSOWebServiceSoap();
			UserInfo userInfo = stub.getUserInfo(loginAction.getRequest().getRemoteAddr());
			log.info("IP= " + loginAction.getRequest().getRemoteAddr());
			if (userInfo == null) {
				return loginAction.loginGen();
			}
			log.info("单点登录获取用户信息 userInfo = " + userInfo.getEmpID());
			// 普通登录
			return loginByLoginName(userInfo.getLoginID());
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	private boolean ieVersionSmallThan(HttpServletRequest request, int compareVersion) {
		String agent = loginAction.getRequest().getHeader("User-Agent").toLowerCase();
		log.info(agent);
		if (agent.indexOf("rv:") != -1) {
			return false;
		}
		String reg = "msie (\\d+)\\.\\d+";
		Pattern compile = Pattern.compile(reg);
		Matcher matcher = compile.matcher(agent);
		while (matcher.find()) {
			String version = matcher.group(1);
			log.info("ie版本:" + version);
			if (Integer.valueOf(version) < compareVersion) {
				return true;
			}
		}
		return false;
	}

	private boolean isIEBrower(HttpServletRequest request) {
		String agent = loginAction.getRequest().getHeader("User-Agent").toLowerCase();
		if (agent.indexOf("rv:") != -1) {
			return true;
		}
		return agent.indexOf("msie") != -1;
	}

	private boolean chromeVersionSmallThan(HttpServletRequest request, int compareVersion) {
		String agent = loginAction.getRequest().getHeader("User-Agent").toLowerCase();
		log.info(agent);
		String reg = "chrome/(\\d+)\\.\\d+";
		Pattern compile = Pattern.compile(reg);
		Matcher matcher = compile.matcher(agent);
		while (matcher.find()) {
			String version = matcher.group(1);
			log.info("谷歌版本:" + version);
			if (Integer.valueOf(version) < compareVersion) {
				return true;
			}
		}
		return false;
	}

	private static String getLoginUrl(LoginAction loginAction) {
		StringBuffer url = loginAction.getRequest().getRequestURL();
		if (StringUtils.isNotBlank(loginAction.getLoginName()) && StringUtils.isNotBlank(loginAction.getPassword())) {// 普通登录
			url.append("?").append("userName=").append(loginAction.getLoginName());
		}
		if (StringUtils.isNotBlank(loginAction.getRequest().getQueryString())) {
			url.append("?").append(loginAction.getRequest().getQueryString());
		}
		log.info(JecnCetc10AfterItem.FireFox_URL + " " + url.toString());
		return url.toString();
	}
}
