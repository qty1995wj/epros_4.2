package epros.designer.gui.process.guide.guideTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;

import epros.designer.gui.popedom.person.JecnPersonInfoPanel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;
/***
 * 相关制度
 * @author 2012-07-06
 *
 */
public class RealtedRuleTable extends JTable {
	private static Logger log = Logger.getLogger(RealtedRuleTable.class);
	/** 相关制度Area */
	private RealtedRuleTable realtedRuleTable = null;
	private List<RuleT> ListRuleT = new ArrayList<RuleT>();
	private Long flowId = null;
	public RealtedRuleTable(Long flowId) {
		this.flowId = flowId;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}
	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 获取流程记录保存数据
		try {
			ListRuleT = ConnectionPool.getProcessAction().getRuleTByFlowId(flowId);
			for (int i = 0; i < ListRuleT.size(); i++) {
				RuleT ruleT = ListRuleT.get(i);
				Vector<String> row = new Vector<String>();
				row.add(String.valueOf(ruleT.getId()));
				//标准名称
				row.add(ruleT.getRuleName());
				vs.add(row);
			}

		} catch (Exception e) {
			log.error("RealtedRuleTable getContent is error", e);
		}

		return vs;
	}
	public RealtedRuleTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("ruleName"));
		return new RealtedRuleTableMode(getContent(),title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}
	//获取数据
	public Vector<Vector<String>> updateTableContent(
			List<JecnTaskHistoryNew> list) {
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		return vector;
	}
	class RealtedRuleTableMode extends DefaultTableModel {
		public RealtedRuleTableMode(Vector<Vector<String>> data,
				Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}
}
