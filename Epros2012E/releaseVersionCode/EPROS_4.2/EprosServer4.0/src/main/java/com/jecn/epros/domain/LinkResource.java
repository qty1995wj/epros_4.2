package com.jecn.epros.domain;

import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.HttpUtil;

/**
 * @author weidp
 */
public class LinkResource {

    private String id;

    private String name;
    /**
     * 当前请求资源
     */
    private ResourceType type;
    /**
     * 属于哪个资源
     */
    private TreeNodeUtils.NodeType from;

    /**
     * 0 临时  1 正式
     **/
    private int isPublic = 1;

    private String url;

    private Long historyId;

    private boolean appRequest;

    private Integer approveType;//是否废止


    public enum ResourceType {
        PERSON("人员", "Personnel"), // 人员
        PROCESS("流程", "Process"), // 流程
        PROCESS_FILE("流程（文件）", "Process file"), // 流程（文件）
        PROCESS_MAP("流程架构", "Process Map"), // 流程地图
        ACTIVITY("活动", "Activitiy"), // 活动
        ORGANIZATION("组织", "Organization"), // 组织
        POSITION("岗位", "job"), // 岗位
        FILE("文件", "File"), // 文件
        RULE("制度", "Institution"), // 制度
        RULE_DIR("制度目录", "Institution"), // 制度
        STANDARD("标准", "Standard"), // 标准
        RISK("风险", "Risk"), // 风险
        FILE_VIEW, // 文件查看
        SUGGEST, // 合理化建议
        TASK_EDIT, // 编辑EDIT
        TASK_VIEW, // 阅读VIEW
        TASK_RETRACT, // 撤回RETRACT
        TASK_DELETE, // 删除DELETE
        TASK_APPROVE, // 审批APPROVE
        TASK_RESUBMIT, // 重新提交RESUBMIT
        HISTORY, // 文控
        TASK_ORGANIZE, // 整理意见ORGANIZE
        KPI, // 流程KPI
        TEST_RUN_FILE;// 任务试运行文件

        private String name = "";
        private String enName = "";

        ResourceType() {

        }

        ResourceType(String name, String enName) {
            this.name = name;
            this.enName = enName;
        }

        public String getName() {
            return this.name;
        }

        public String getEnName() {
            return enName;
        }

    }

    public LinkResource(Object id, String name) {
        this(id == null ? "" : id.toString(), name, null);
    }

    public LinkResource(Object id, String name, ResourceType type) {
        this(id == null ? "" : id.toString(), name, type);

    }

    public LinkResource(Object id, String name, ResourceType type, TreeNodeUtils.NodeType from) {
        this(id == null ? "" : id.toString(), name, type, from);
    }

/*    public LinkResource(Object id, String name, ResourceType type, TreeNodeUtils.NodeType from, String url) {
        this(StringUtils.toString(id), name, type, from);
        this.url = url;
    }*/

    public LinkResource(String id, String name, ResourceType type, int isPublic, TreeNodeUtils.NodeType from) {
        this(id, name, type);
        this.isPublic = isPublic;
        this.from = from;
    }

    public LinkResource(String id, String name, ResourceType type) {
        super();
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public LinkResource(String id, String name, ResourceType type, TreeNodeUtils.NodeType from) {
        this(id, name, type);
        this.from = from;
    }

    public LinkResource(String id, String name, ResourceType type, int isPublic, TreeNodeUtils.NodeType from, String url) {
        this(id, name, type);
        this.isPublic = isPublic;
        this.from = from;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public TreeNodeUtils.NodeType getFrom() {
        return from;
    }

    public void setFrom(TreeNodeUtils.NodeType from) {
        this.from = from;
    }

    public String getUrl() {
        if (type == null || id == null || id == "") {
            return url;
        }
        if (from == null && "FILE".equals(type.toString())) {
            url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "", appRequest);
        } else if (from == null && "RULE".equals(type.toString())) {
            url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "ruleLocal", appRequest);
        } else if (from == TreeNodeUtils.NodeType.process && "FILE".equals(type.toString()) && id != null) {
            url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "", appRequest);
        } else if (from == TreeNodeUtils.NodeType.processMap && "FILE".equals(type.toString()) && id != null) {
            url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "", appRequest);
        } else if (from == TreeNodeUtils.NodeType.position && "FILE".equals(type.toString()) && id != null) {
            url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "", appRequest);
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public boolean isAppRequest() {
        return appRequest;
    }

    public void setAppRequest(boolean appRequest) {
        this.appRequest = appRequest;
    }

    public Integer getApproveType() {
        return approveType;
    }

    public void setApproveType(Integer approveType) {
        this.approveType = approveType;
    }
}
