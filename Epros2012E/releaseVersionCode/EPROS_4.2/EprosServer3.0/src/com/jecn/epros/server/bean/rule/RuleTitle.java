package com.jecn.epros.server.bean.rule;

/**
 * 制度标题表
 * 
 * @author lihongliang
 * 
 */
public class RuleTitle implements java.io.Serializable {
	private Long id;
	private Long ruleId;// 制度ID
	private String titleName;// 标题名称
	private Integer orderNumber;// 序号
	private Integer type;// 类型(0是内容，1是文件表单，2是流程表单)

	/** 0:非必填，1：必填 */
	private int requiredType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public int getRequiredType() {
		return requiredType;
	}

	public void setRequiredType(int requiredType) {
		this.requiredType = requiredType;
	}
}