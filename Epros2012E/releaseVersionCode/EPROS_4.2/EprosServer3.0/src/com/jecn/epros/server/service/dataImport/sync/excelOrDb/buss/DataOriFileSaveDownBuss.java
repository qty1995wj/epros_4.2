package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.io.File;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

/**
 * 
 * 行间校验异常，信息保存到本地
 * 
 * @author ZHANGXIAOHU
 * 
 */
public class DataOriFileSaveDownBuss extends AbstractFileDownBuss {

	protected final Log log = LogFactory.getLog(this.getClass());
	/** 导出excel模板路径 */
	protected String outputExcelModelPath = null;
	/** 错误导出excel数据路径 */
	protected String outputExcelDataPath = null;

	/**
	 * 
	 * 从DB中读数据写入到excel再保存到本地
	 * 
	 * @param deptBeanList
	 *            部门
	 * 
	 * @param userBeanList
	 *            人员 岗位
	 * @throws Exception
	 * @return boolean
	 * 
	 */
	protected boolean editExcel(List<JecnTmpOriDept> deptBeanList,
			List<JecnTmpPosAndUserBean> userBeanList) {
		log.info("错误信息存储开始，错误信息存放路径：" + this.getOutputExcelDataPath());

		Workbook wbookMould = null;
		WritableWorkbook wbookData = null;
		try {

			// 判断模板文件路径和待保存文件路径是否
			if (SyncTool.isNullOrEmtryTrim(this.getOutputExcelModelPath())
					|| SyncTool
							.isNullOrEmtryTrim(this.getOutputExcelDataPath())) {
				return false;

			}

			// 人员导出模板文件 不存在返回
			File fileMould = new File(this.getOutputExcelModelPath());
			if (!fileMould.exists()) {
				return false;
			}
			log.info("错误信息存储路径验证成功");

			// 人员导出数据文件 不是文件返回
			File fileData = new File(this.getOutputExcelDataPath());

			wbookMould = Workbook.getWorkbook(fileMould);
			wbookData = Workbook.createWorkbook(fileData, wbookMould);

			try{//提前释放资源
				wbookMould.close();
				wbookMould=null;
			}catch (Exception e) {
				log.error("", e);
			}
			
			// 获取sheet
			WritableSheet[] sheetArry = wbookData.getSheets();
			if (sheetArry.length != 2) {
				return false;
			}

			// 部门
			WritableSheet deptSheet = sheetArry[0];
			// 人员岗位
			WritableSheet userPosSheet = sheetArry[1];

			// 编辑部门人员岗位数据
			editDept(deptSheet, deptBeanList);
			editUserPos(userPosSheet, userBeanList);

			// 写入本地
			wbookData.write();
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			log.info("错误信息存储结束，错误信息存放路径：" + this.getOutputExcelDataPath());
		} catch (Exception ex) {
			log.error("人员数据导出失败：" + this.getClass().getName() + "类editExcel方法 "
					+ ex.getMessage());

			return false;

		} finally {

			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("人员数据导出失败：" + this.getClass().getName()
							+ "类editExcel方法 " + e.getMessage());
					return false;
				}
			}
			if (wbookMould != null) {
				wbookMould.close();
			}
		}

		return true;
	}

	/**
	 * 
	 * 编辑部门sheet，把DB中部门数据写入给定sheet中
	 * 
	 * @param deptSheet
	 *            部门sheet
	 * @param deptBeanList部门数据
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	protected void editDept(WritableSheet deptSheet,
			List<JecnTmpOriDept> deptBeanList) throws RowsExceededException,
			WriteException {

		// 添加部门数据
		for (int i = 0; i < deptBeanList.size(); i++) {
			// 获取一条部门数据
			JecnTmpOriDept deptBean = deptBeanList.get(i);
			// 部门编号为空，退出本次循环
			if (SyncTool.isNullOrEmtryTrim(deptBean.getDeptNum())) {
				continue;
			}
			// 行数
			int rowNum = i + SyncConstant.WIRTER_ROW_NUM;

			// 部门编号
			deptSheet.addCell(new Label(0, rowNum, deptBean.getDeptNum()));
			// 部门名称
			deptSheet.addCell(new Label(1, rowNum, deptBean.getDeptName()));
			// 上级部门编号
			deptSheet.addCell(new Label(2, rowNum, deptBean.getPerDeptNum()));
		}
	}

	/**
	 * 
	 * 编辑人员sheet，把DB中人员数据写入给定sheet中
	 * 
	 * @param userPosSheet
	 *            人员Sheet
	 * @param userPosList人员数据
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	protected void editUserPos(WritableSheet userPosSheet,
			List<JecnTmpPosAndUserBean> userPosList)
			throws RowsExceededException, WriteException {
		for (int i = 0; i < userPosList.size(); i++) {
			// 获取一条人员数据
			JecnTmpPosAndUserBean userBean = userPosList.get(i);

			// 行数
			int rowNum = i + SyncConstant.WIRTER_ROW_NUM;

			// 登录名称
			userPosSheet.addCell(new Label(0, rowNum, userBean.getUserNum()));
			// 真实姓名
			userPosSheet.addCell(new Label(1, rowNum, userBean.getTrueName()));
			// 任职岗位编号
			userPosSheet.addCell(new Label(2, rowNum, userBean.getPosNum()));
			// 任职岗位名称
			userPosSheet.addCell(new Label(3, rowNum, userBean.getPosName()));
			// 岗位所属部门编号
			userPosSheet.addCell(new Label(4, rowNum, userBean.getDeptNum()));
			// 邮箱地址
			userPosSheet.addCell(new Label(5, rowNum, userBean.getEmail()));
			// 内外网邮件标识(内网:0 外网:1)
			String emailTypeStr = "";
			if (userBean.checkEmailType()) {
				emailTypeStr = userBean.getEmailType().toString();
			}
			userPosSheet.addCell(new Label(6, rowNum, emailTypeStr));
			// 联系电话
			userPosSheet.addCell(new Label(7, rowNum, userBean.getPhone()));
		}
	}

	public String getOutputExcelModelPath() {
		return outputExcelModelPath;
	}

	public void setOutputExcelModelPath(String outputExcelModelPath) {
		this.outputExcelModelPath = outputExcelModelPath;
	}

	public String getOutputExcelDataPath() {
		return outputExcelDataPath;
	}

	public void setOutputExcelDataPath(String outputExcelDataPath) {
		this.outputExcelDataPath = outputExcelDataPath;
	}

	@Override
	protected String getOutFileName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getOutFilepath() {
		// TODO Auto-generated method stub
		return null;
	}

}
