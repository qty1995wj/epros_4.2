package com.jecn.epros.domain.selectFile;

public class SearchSelectBean {

    private String name;
    // processMap:架构 process:流程 rule:制度 standard:标准 risk:风险 file:文件 organization:组织 position:岗位 user:人员
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
