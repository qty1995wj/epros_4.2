<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  </head>
  
  <body>
    <div id="div" style="height: 100%;">
    	<div id="search" class="divBorder" align="center">
    		<div id="riskSystemSearehText" style="overflow: hidden;"
					class="minWidth">
				<table width="100%">
					<tr>
						<td width="10%" height="25px" align="right">
							<!-- 风险名称： -->
							<s:text name="riskNumberC"></s:text>：
						</td>
						<td width="35%" align="left">
							<input type="text" style="width: 80%" 
									id="riskName_id" maxLength="32" />
						</td>
						<td width="10%" align="right">
							<!-- 风险编号： -->
							<s:text name="riskNameC"></s:text>：
						</td>
						<td width="35%" align="left">
							<input type="text" style="width: 80%" 
									id="riskNum_id" maxLength="32" />
						</td>
						<!-- 
						<td style="vertical-align: middle;" width="10%;" align="center">
							<a onClick="toggle(riskSystemSearehText)"
									style="cursor: pointer;" class="packUp"> 
								<!-- 收起 --> 
						<!-- 
								<img id="packUp" src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> 
									<s:text name="packUp" /> 
							</a>
						</td>
						 -->
					</tr>
					<tr>
							<td align="right">
								<!-- 责任部门: -->
								<s:text name="responsibilityDepartmentC" />
							</td>
							<td align="left">
								<input type="text" style="width: 65%; vertical-align: middle;"
									id="orgDutyName_risk_id"
									onkeyup="search(orgDutyName_risk_id,orgDutyId_risk_id,search_risk_orgDutyName,'org');"
									onblur="divNode(search_risk_orgDutyName)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","orgDutyName_risk_id","orgDutyId_risk_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_risk_orgDutyName"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td></td>
							<td></td>
					</tr>
				</table>
			</div>
			
			<div id="searchBut" align="center">
				<img src="${basePath}images/iParticipateInProcess/search.gif"
						style="cursor: pointer;" onclick="riskSearch()" />
				<img src="${basePath}images/iParticipateInProcess/reset.gif"
						style="cursor: pointer;" onclick="riskSearchReset()" />
			</div>
    	</div>
    	<div id="riskSearchGrid_id"></div>
    	<!-- 责任部门ID -->
		<input type="hidden" id="orgDutyId_risk_id" />
    </div>
  </body>
</html>
