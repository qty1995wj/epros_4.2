package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;

import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;

/**
 * 
 * 流程文件（流程操作说明）面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowFilePanel extends JecnAbstractPropertyBasePanel {

	public JecnFlowFilePanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 表面板
		tableScrollPane = new JecnThreeTableScrollPane(dialog);
		this.add(tableScrollPane, BorderLayout.CENTER);
	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		this.add(tableScrollPane, BorderLayout.CENTER);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		// 过滤隐藏项排序
		// 显示表数据
		tableScrollPane.initData(tableScrollPane.getShowItem(configTypeDesgBean.getTableItemList(),
				JecnConfigContents.TYPE_SMALL_ITEM_FLOWFILE));
	}

	@Override
	public boolean check() {
		return true;
	}
}
