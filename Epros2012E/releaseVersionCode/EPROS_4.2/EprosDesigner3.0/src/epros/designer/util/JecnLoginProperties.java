package epros.designer.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JecnLoginProperties {

	private final static Log log = LogFactory.getLog(JecnLoginProperties.class);
	/** 资源文件库--国际化 */
	public static PropertyResourceBundle resourceBundleZN;
	public static PropertyResourceBundle resourceBundleEN;

	public static Map<String, String> languageMaps = new HashMap<String, String>();

	public static void loadProperties() {
		resourceBundleZN = (PropertyResourceBundle) PropertyResourceBundle.getBundle("resources.JecnDesignerLogin_"
				+ Locale.CHINESE);
		resourceBundleEN = (PropertyResourceBundle) PropertyResourceBundle.getBundle("resources.JecnDesignerLogin_"
				+ Locale.ENGLISH);
	}

	public static void initLanguageMaps(int langhage) {
		PropertyResourceBundle bundle = null;
		if (langhage == 0) {
			bundle = resourceBundleZN;
		} else {
			bundle = resourceBundleEN;
		}
		for (String key : bundle.keySet()) {
			languageMaps.put(key, bundle.getString(key));
		}
	}

	public static String getValue(String key) {
		try {
			return languageMaps.get(key.trim());
		} catch (Exception e) {
			log.error("JecnProperties getValue is error", e);
			return "";
		}

	}
}
