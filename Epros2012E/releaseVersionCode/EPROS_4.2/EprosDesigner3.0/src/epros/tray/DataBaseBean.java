package epros.tray;

import epros.designer.util.ConnectionPool;


/***
 * 数据库配置文件Bean
 * 
 * @author zhangjie 2012-04-25
 * 
 */
public class DataBaseBean {
	/** 用户名称 */
	private String usname;
	/** 用户名内容 */
	private String uspwd;
	/** 数据库连接路径 */
	private String dataUrl;
	/**数据库方言*/
	private String dataDialect;
	/**数据库驱动*/
	private String dataDriver;
	/**数据库地址*/
	private String dataHostIP;
	/**hbm配置路径*/
	private String hbmPath;

	private String dataDesinPort;

	/** 数据库类型 */
	private DBTypeEnum dbTypeEnum;
	/** 数据库IP */
	private String dbIP = null;
	/** 数据库端口 */
	private String dbPort = null;
	/** 数据库实例名称 */
	private String dbName = null;
	/**
	 * 
	 * 数据库类型
	 * 
	 * @author Administrator
	 * 
	 */
	public enum DBTypeEnum {
		oracle, sqlserver      //, mysql
	}
	/**
	 * 
	 * 读取properties文件时，赋值数据库类型
	 * 
	 * @param str
	 */
	public void readPropertiesSetDBTypeEnum(String str){
		if("org.hibernate.dialect.OracleDialect".equals(str)){//oracle
			dbTypeEnum=DBTypeEnum.oracle;
		}else 	if("org.hibernate.dialect.SQLServerDialect".equals(str)){//sqlserver
			dbTypeEnum=DBTypeEnum.sqlserver;
		}else{
			dbTypeEnum=DBTypeEnum.oracle;
		}
	}
	/***
	 * 读取配置文件时拆分URL
	 */
	public void readUrl() {
		String[] spiltStr = null;

		switch (dbTypeEnum) {
		case oracle:
			spiltStr = dataUrl.split("@")[1].split(":");
			dbIP = spiltStr[0];
			dbPort = spiltStr[1];
			dbName = spiltStr[2];
			break;
		case sqlserver:
			spiltStr = dataUrl.split("\\/\\/")[1].split("/");
			dbName = spiltStr[1];

			spiltStr = spiltStr[0].split(":");
			dbIP = spiltStr[0];
			dbPort = spiltStr[1];
			break;
		}
	}
	/***
	 * 存储配置文件时合并URL
	 */
	public void saveUrl() {
		switch (dbTypeEnum) {
		case oracle:
			// jdbc:oracle:thin:@192.168.1.9:1521:XE
			dataUrl = "jdbc:oracle:thin:@" + dbIP + ":" + dbPort + ":" + dbName;
			break;
		case sqlserver:
			// jdbc:jtds:sqlserver://192.168.1.18:1433/DATA_TEST
			dataUrl = "jdbc:jtds:sqlserver://" + dbIP + ":" + dbPort + "/"
					+ dbName;
			break;

		}
	}
	

	public String getUsname() {
		return usname;
	}

	public void setUsname(String usname) {
		this.usname = usname;
	}

	public String getUspwd() {
		return uspwd;
	}

	public void setUspwd(String uspwd) {
		this.uspwd = uspwd;
	}

	public String getDataUrl() {
		return dataUrl;
	}

	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}

	public String getDataDialect() {
		return dataDialect;
	}

	public void setDataDialect(String dataDialect) {
		this.dataDialect = dataDialect;
	}

	public String getDataDriver() {
		return dataDriver;
	}

	public void setDataDriver(String dataDriver) {
		this.dataDriver = dataDriver;
	}

	public String getDataHostIP() {
		return dataHostIP;
	}

	public void setDataHostIP(String dataHostIP) {
		this.dataHostIP = dataHostIP;
	}



	public String getDataDesinPort() {
		return dataDesinPort;
	}

	public void setDataDesinPort(String dataDesinPort) {
		this.dataDesinPort = dataDesinPort;
		ConnectionPool.setServerPort(dataDesinPort);
	}

	public DBTypeEnum getDbTypeEnum() {
		return dbTypeEnum;
	}

	public String getHbmPath() {
		return hbmPath;
	}
	public void setHbmPath(String hbmPath) {
		this.hbmPath = hbmPath;
	}
	public void setDbTypeEnum(DBTypeEnum dbTypeEnum) {
		this.dbTypeEnum = dbTypeEnum;
	}


	public String getDbIP() {
		return dbIP;
	}

	public void setDbIP(String dbIP) {
		this.dbIP = dbIP;
	}

	public String getDbPort() {
		return dbPort;
	}

	public void setDbPort(String dbPort) {
		this.dbPort = dbPort;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	
}
