package com.jecn.epros.server.download.wordxml.gujiajiaju;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ActiveItSystem;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.process.ProcessInterfacesDescriptionBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.download.wordxml.approve.ApproveManager;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 顾家家居操作说明模版
 * 
 * @author hyl
 * 
 */
public class GJJJProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍行距 *****/
	private final ParagraphLineRule vLine2 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private final ParagraphLineRule leastVline12 = new ParagraphLineRule(12F, LineRuleType.AT_LEAST);

	public GJJJProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(false);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(16.88F);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(6.5F);
		setDocStyle(".", textTitleStyle());
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		float[] width = null;
		if (JecnContants.showPosNameBox) {
			width = new float[] { 3.5F, 7.5F, 3.5F };
		} else if (!JecnContants.showPosNameBox) {
			width = new float[] { 5.0F, 9.5F };
		}
		List<String[]> rowData2 = new ArrayList<String[]>();
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = "对应职位/岗位";
		// 角色职责
		String roleResponsibility = "职责";
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility, positionName });
		} else {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility });
		}
		for (String[] str : rowData) {
			if (JecnContants.showPosNameBox) {
				rowData2.add(new String[] { str[0], str[2], str[1] });
			} else {
				rowData2.add(new String[] { str[0], str[2] });
			}
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), getDocProperty()
				.isTblTitleCrossPageBreaks());
	}

	protected void a33(ProcessFileItem processFileItem, String... data) {
		createTitle(processFileItem);
		String startActive = "流程起点";
		String endActive = "流程终点";
		// 输入
		String input = JecnUtil.getValue("input");
		// 输出
		String output = JecnUtil.getValue("output");

		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { startActive, data[0] });
		rowData.add(new String[] { endActive, data[1] });
		rowData.add(new String[] { input, data[2] });
		rowData.add(new String[] { output, data[3] });
		float[] width = new float[] { 3.5F, 14F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		word.paragraphStyleKpi(processFileItem, oldRowData);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2F, 2F, 2F, 2F, 0.83F, 1.17F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2.54F, 3.17F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		titleSect.setSectBean(sectBean);

		ParagraphBean a = new ParagraphBean(Align.center, Fonts.HEI, Constants.yihao, false);
		a.setInd(0, -0.2F, 0, 0);
		a.setSpace(0F, 0F, vLine1);
		titleSect.createParagraph("顾家家居股份有限公司流程说明文件 ", a);

		ParagraphBean b = new ParagraphBean(Align.right, Fonts.HEI, Constants.sihao, false);
		b.setInd(0, -0.2F, 0, 0);
		b.setSpace(15F, 0F, 1, new ParagraphLineRule(17F, LineRuleType.EXACT));
		titleSect.createParagraph(nullToEmpty(processDownloadBean.getFlowInputNum()), b);

		createLine(titleSect, 0.95F, 45F, 420F, 45F);

		titleSect.createMutilEmptyParagraph(10);
		titleSect.createParagraph(nullToEmpty(processDownloadBean.getFlowName()), new ParagraphBean(Align.center,
				Fonts.HEI, Constants.yihao, false));

		createTitlehf(titleSect);
	}

	private void createLine(Sect titleSect, float startX, float startY, float endX, float endY) {
		LineGraph line = new LineGraph(LineStyle.single, 0.5F);
		line.setFrom(startX, startY);
		line.setTo(endX, endY);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		positionBean.setMarginRight(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
	}

	private void createTitlehf(Sect sect) {
		Footer footer = sect.createFooter(HeaderOrFooterType.odd);
		footer.createParagraph(nullToEmpty("顾家家居 版权所有"), new ParagraphBean(Align.left, Fonts.HEI, Constants.xiaosi,
				false));
		footer.createParagraph(nullToEmpty("顾家家居股份有限公司"), new ParagraphBean(Align.center, Fonts.HEI, Constants.sanhao,
				false));

	}

	/**
	 * 添加首页内容
	 * 
	 * @param titleSect
	 */
	private void addLevelContent(Sect titleSect) {

		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0F, 0F, 0F, 0F));
		ParagraphBean cellBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);

		// 为了简单表格将由四个表格组合而成
		Table nameTable = titleSect.createTable(tabBean, new float[] { 2F, 14.88F });
		List<String[]> titleLine = new ArrayList<String[]>();
		titleLine.add(new String[] { "文件名称", processDownloadBean.getFlowName() });
		nameTable.createTableRow(titleLine, cellBean);
		nameTable.setCellValignAndHeight(Valign.center, 0.7F);

		Table versionTable = titleSect.createTable(tabBean, new float[] { 2F, 5.92F, 2.32F, 6.64F });
		List<String[]> versionLine = new ArrayList<String[]>();
		String pubTime = getPubTime();
		versionLine.add(new String[] { "版    本", processDownloadBean.getFlowVersion(), "生效日期", pubTime });
		versionTable.createTableRow(versionLine, cellBean);
		versionTable.setCellValignAndHeight(Valign.center, 0.7F);

		// 级别中应该是不显示t_level=0的以及自己本身
		String L1 = "";
		String L2 = "";
		String L3 = "";
		String L4 = "";
		List<Object[]> parents = processDownloadBean.getParents();
		if (JecnUtil.isNotEmpty(parents)) {
			parents.remove(parents.size() - 1);
		}
		int parentLen = parents.size();
		if (parentLen > 1) {
			L1 = parents.get(1)[1].toString();
		}
		if (parentLen > 2) {
			L2 = parents.get(2)[1].toString();
		}
		if (parentLen > 3) {
			L3 = parents.get(3)[1].toString();
		}
		if (parentLen > 4) {
			L4 = parents.get(4)[1].toString();
		}
		ApproveManager manager = new ApproveManager(processDownloadBean.getBaseDao(), processDownloadBean
				.getLatestHistoryNew());
		Table codeTable = titleSect.createTable(tabBean, new float[] { 2F, 5.92F, 0.85F, 1.48F, 6.64F });
		List<String[]> codeData = new ArrayList<String[]>();
		codeData.add(new String[] { "文件编码", processDownloadBean.getFlowInputNum(), "流程架构", "L1", L1 });
		codeData
				.add(new String[] { "拟 制 人", processDownloadBean.getResponFlow().getFictionPeopleName(), "", "L2", L2 });
		// 部门审核人
		codeData.add(new String[] { "审 核 人", manager.getApprovePeopleName(2), "", "L3", L3 });
		codeData.add(new String[] { "批 准 人", manager.getApprovePeopleName(4), "", "L4", L4 });
		codeData.add(new String[] { "流程责任人 ", processDownloadBean.getResponFlow().getDutuUserPosName(), "", "", "" });
		codeTable.createTableRow(codeData, cellBean);
		// 合并流程架构单元格
		codeTable.getRow(0).getCell(2).setVmergeBeg(true);
		codeTable.getRow(0).getCell(2).setRowspan(5);
		codeTable.getRow(4).getCell(2).setVmerge(true);
		codeTable.setCellValignAndHeight(Valign.center, 0.7F);

		// 修改样式去除表格内容加粗
		ParagraphBean noBlobBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		setTableParagraph(nameTable, new int[] { 0 }, new int[] { 1 }, noBlobBean);
		setTableParagraph(versionTable, new int[] { 0 }, new int[] { 1, 3 }, noBlobBean);
		setTableParagraph(codeTable, new int[] { 0, 1, 2, 3, 4 }, new int[] { 1, 4 }, noBlobBean);

	}

	private String getPubTime() {
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		for (JecnTaskHistoryNew history : historys) {
			if (history != null) {
				return JecnUtil.DateToStr(history.getPublishDate(), "yyyy-MM-dd");
			}
		}
		return "";
	}

	/**
	 * 记录保存
	 */
	@Override
	protected void a20(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 记录名称
		String recordName = JecnUtil.getValue("recordName");
		// 保存责任人
		String saveResponsiblePersons = JecnUtil.getValue("saveResponsiblePersons");
		// 保存场所
		String savePlace = JecnUtil.getValue("savePlace");
		// 归档时间
		String filingTime = JecnUtil.getValue("filingTime");
		// 保存期限
		String expirationDate = JecnUtil.getValue("storageLife");
		// 到期处理方式
		String handlingDue = JecnUtil.getValue("treatmentDue");
		float[] width = new float[] { 2.12F, 2.83F, 2F, 2F, 2F, 2F, 2F, 2F, 1.5F };
		List<String[]> rowData2 = new ArrayList<String[]>();
		rowData2.add(0, new String[] { "编号", recordName, "移交责任人", saveResponsiblePersons, savePlace, filingTime,
				expirationDate, handlingDue });
		for (String[] row : rowData) {
			rowData2.add(new String[] { row[6], row[0], row[7], row[1], row[2], row[3], row[4], row[5] });
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 流程文控信息
	 * 
	 * @param name
	 * @param documentList
	 * @param contentSect
	 */
	protected void a24(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 版本号
		String version = JecnUtil.getValue("versionNumber");
		// 变更说明
		String descOfChange = "修订内容及理由";
		// 发布日期
		String releaseDate = "拟制/修订日期";
		// 流程拟稿人
		String artificialPerson = "拟制/修订责任人";
		// 审批人
		String approver = "批准人";
		List<String[]> rows = new ArrayList<String[]>();
		rows.add(new String[] { version, artificialPerson, releaseDate, descOfChange, approver });
		rows.addAll(getApproveData());

		float[] width = new float[] { 2, 3.5F, 3.5F, 6, 2.5F };
		Table tbl = createTableItem(processFileItem, width, rows);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {

		// 段落显示 1:段落；0:表格（默认）
		if (processDownloadBean.getFlowFileType() == 1) {
			a10InParagraph(processFileItem, rowData);
			return;
		}
		createActivityTable(processFileItem);
	}

	protected void a36(ProcessFileItem processFileItem, ProcessInterfacesDescriptionBean description) {
		createTitle(processFileItem);
		Sect sect = processFileItem.getBelongTo();
		ParagraphBean titleBean = new ParagraphBean(Align.left, textContentStyle().getFontBean().getFontFamily(),
				Constants.xiaosi, true);
		titleBean.setSpace(0.5f, 0f, new ParagraphLineRule(1.5f, LineRuleType.AUTO));
		int i = 0;
		if (description.getPid() != null && description.getPid() != 0) {
			sect.addParagraph(new Paragraph(++i + ") " + JecnUtil.getValue("correspondingUpProcess"), titleBean));
			List<String[]> rowData = new ArrayList<String[]>();
			rowData.add(new String[] { JecnUtil.getValue("fileCode"), JecnUtil.getValue("processFileName") });
			rowData.add(new String[] { description.getPnumber(), description.getPname() });
			float[] width = new float[] { 7F, 7F };
			Table tbl = createTableItem(processFileItem, width, rowData);
			tbl.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());
		}
		if (description.getUppers() != null && description.getUppers().size() > 0) {
			sect.addParagraph(new Paragraph(++i + ") " + JecnUtil.getValue("inProcessDescribe"), titleBean));
			createInterfaceTable(processFileItem, "输入流程", description.getUppers());
		}

		if (description.getLowers() != null && description.getLowers().size() > 0) {
			sect.addParagraph(new Paragraph(++i + ") " + JecnUtil.getValue("outProcessDescribed"), titleBean));
			createInterfaceTable(processFileItem, "输出流程", description.getLowers());
		}

		if (description.getProcedurals() != null && description.getProcedurals().size() > 0) {
			sect.addParagraph(new Paragraph(++i + ") " + JecnUtil.getValue("interfaceFlow"), titleBean));
			createInterfaceTable(processFileItem, JecnUtil.getValue("interfaceFlow"), description.getProcedurals());
		}

		if (description.getSons() != null && description.getSons().size() > 0) {
			sect.addParagraph(new Paragraph(++i + ") " + JecnUtil.getValue("sonFlow"), titleBean));
			createInterfaceTable(processFileItem, JecnUtil.getValue("sonFlow"), description.getSons());
		}
	}

	public void createActivityTable(ProcessFileItem processFileItem) {
		// 标题
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		boolean showTimeLine = JecnConfigTool.showActTimeLine();
		boolean showInOut = JecnContants.showActInOutBox;
		boolean showIT = false;

		// ---------标题
		List<String> titles = new ArrayList<String>();
		List<List<String>> datas = new ArrayList<List<String>>();
		// 活动编号
		titles.add(JecnUtil.getValue("activitynumbers"));
		// 活动名称
		titles.add(JecnUtil.getValue("activityTitle"));
		// 活动说明
		titles.add("活动内容");
		// 执行角色
		titles.add("角色");
		if (showInOut) {
			// 输入
			titles.add(JecnUtil.getValue("input"));
			// 输出
			titles.add(JecnUtil.getValue("output"));
		}
		if (showTimeLine) {
			// 现状目标
			titles.add(JecnUtil.getValue("timeLimit") + "\n(现状/目标" + JecnConfigTool.getActiveTimeLineUtil() + ")");
		}
		if (showIT) {
			// 信息化
			titles.add("信息化");
		}

		// 宽度
		List<Float> widthList = new ArrayList<Float>();
		widthList.add(2F);
		widthList.add(2F);
		widthList.add(2F);
		if (showInOut) {
			widthList.add(2F);
			widthList.add(2F);
		}
		if (showTimeLine) {
			widthList.add(2F);
		}
		if (showIT) {
			widthList.add(2F);
		}
		// 其余空间被活动说明占据
		float total = 0;
		for (float f : widthList) {
			total += f;
		}
		float max = docProperty.getNewTblWidth();
		// 活动说明的宽度
		float last = max - total > 2 ? max - total : 2;
		widthList.add(2, last);

		// --------内容
		Map<Long, String> idToKey = new HashMap<Long, String>();
		if (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent()) {
			List<KeyActivityBean> keyActivityShowList = processFileItem.getDataBean().getKeyActivityShowList();
			for (KeyActivityBean k : keyActivityShowList) {
				idToKey.put(k.getId(), k.getActivityShowControl());
			}
		}
		List<String[]> dataRows = JecnWordUtil.obj2String(processFileItem.getDataBean().getAllActivityShowList());
		Map<String, List<String>> activeIdToITMap = getActiveIdToITMap();
		for (String[] data : dataRows) {
			String content = JecnUtil.nullToEmpty(data[3]);
			if (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent()) {
				String keyContent = JecnUtil.nullToEmpty(idToKey.get(Long.valueOf(data[8].toString())));
				if (StringUtils.isNotBlank(content) && StringUtils.isNotBlank(keyContent)) {
					content = content + "\n以下是重要说明：\n" + keyContent;
				} else if (StringUtils.isNotBlank(keyContent)) {
					content = "以下是重要说明：\n" + keyContent;
				}
			}
			List<String> row = new ArrayList<String>(Arrays.asList(data[0], data[2], content, data[1]));
			if (showInOut) {
				// 输入
				row.add(data[4]);
				// 输出
				row.add(data[5]);
			}
			if (showTimeLine) {
				// 现状目标
				String targetValue = StringUtils.isBlank(JecnUtil.objToStr(data[6])) ? "" : JecnUtil.objToStr(data[6]);
				String statusValue = StringUtils.isBlank(JecnUtil.objToStr(data[7])) ? "" : JecnUtil.objToStr(data[7]);
				String timeLineValue = ProcessFileModel.concatTimeLineValue(targetValue, statusValue);
				row.add(timeLineValue);
			}
			if (showIT) {
				// 信息化
				List<String> its = activeIdToITMap.get(data[8]);
				if (its != null && its.size() > 0) {
					row.add(JecnUtil.concat(its, "/"));
				} else {
					row.add("");
				}
			}

			datas.add(row);
		}

		// 添加标题
		int columnSize = titles.size();
		rowData.add(titles.toArray(new String[columnSize]));
		for (List<String> data : datas) {
			rowData.add(data.toArray(new String[columnSize]));
		}
		float[] widths = new float[columnSize];
		for (int i = 0; i < columnSize; i++) {
			widths[i] = widthList.get(i);
		}
		Table tbl = createTableItem(processFileItem, widths, rowData);
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	private Map<String, List<String>> getActiveIdToITMap() {
		List<ActiveItSystem> activeItSystems = processDownloadBean.getActiveItSystems();
		Map<String, List<String>> m = new HashMap<String, List<String>>();
		for (ActiveItSystem s : activeItSystems) {
			List<String> ls = JecnUtil.getElseAddList(m, s.getActiveId().toString());
			ls.add(s.getSystemName());
		}
		return m;
	}

	private List<String[]> getApproveData() {
		List<String[]> result = new ArrayList<String[]>();
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (JecnUtil.isEmpty(historys)) {
			return result;
		}
		for (JecnTaskHistoryNew history : historys) {
			result.add(new String[] { history.getVersionId(), history.getDraftPerson(),
					super.dateToStr(history.getPublishDate(), "yyyy-MM-dd"), history.getModifyExplain(),
					new ApproveManager(processDownloadBean.getBaseDao(), history).getApprovePeopleName(4) });
		}
		return result;
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
		addLevelContent(firstSect);
		firstSect.addParagraph(new Paragraph(""));
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setSize(29.7F, 21F);
		flowChartSect.setSectBean(sectBean);
		createCharfhdr(flowChartSect);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		TableBean tableBean = new TableBean();
		tableBean.setBorderBottom(1F);
		tableBean.setTableLeftInd(0.2F);
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		table.createTableRow(new String[] { "", processDownloadBean.getFlowName(), "文档密级：" + getPubOrSec() }, pBean);
		ParagraphBean paragraphBean = new ParagraphBean(Align.right, "宋体", Constants.xiaowu, false);
		table.getRow(0).getCell(2).getParagraph(0).setParagraphBean(paragraphBean);
		table.setCellValignAndHeight(Valign.bottom, 1.38F);
		header.createMutilEmptyParagraph(1);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommfdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		Footer header = sect.createFooter(HeaderOrFooterType.odd);

		TableBean tableBean = new TableBean();
		tableBean.setBorderTop(1F);
		tableBean.setTableLeftInd(0.2F);
		// 3F,11F,3F
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		table.createTableRow(new String[] { processDownloadBean.getPubDate("yyyy-MM-dd"), "保密信息,未经授权禁止扩散", "" }, pBean);

		table.getRow(0).getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.left, "宋体", Constants.xiaowu, false));
		table.getRow(0).getCell(2).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.right, "宋体", Constants.xiaowu, false));
		Paragraph pageParagraph = table.getRow(0).getCell(2).getParagraph(0);
		pageParagraph.appendTextRun("第");
		pageParagraph.appendCurPageRun();
		pageParagraph.appendTextRun("页, 共");
		pageParagraph.appendTotalPagesRun();
		pageParagraph.appendTextRun("页");
		table.setCellValignAndHeight(Valign.top, 0.6F);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 3F, 11F, 3F);
		createCommfdr(sect, 3F, 11F, 3F);
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCommhdr(sect, 3F, 19.7F, 3F);
		createCommfdr(sect, 3F, 19.7F, 3F);
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		textContentStyle.setInd(0F, 0F, 0F, 1F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(12F, LineRuleType.AT_LEAST));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("宋体", Constants.xiaosi, true);
		textTitleStyle.setInd(0F, 0F, 0.76F, 0F);
		textTitleStyle.setSpace(0.5F, 0F, vLine2);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setTableAlign(Align.center);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

}
