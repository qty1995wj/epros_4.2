package com.jecn.epros.domain.inventory;

import java.util.List;
import java.util.Map;

public class FlowInventoryBean extends OrgInventoryRelatedBean {

    /**
     * 父节点ID
     */
    private Long pFlowId;
    /**
     * 责任部门
     */
    private String orgId;
    /**
     * 责任部门
     */
    private String orgName;
    /**
     * 责任部门包含父部门集合
     */
    private List<Map<String,Object>> dutyOrgList;
    /**
     * LEVEL
     */
    private int level;
    /**
     * 0是流程架构 1是流程
     */
    private int isFlow;

    private String path;

    /**
     * 保密级别
     */
    private Integer confidentialityLevel;

    public List<Map<String, Object>> getDutyOrgList() {
        return dutyOrgList;
    }

    public void setDutyOrgList(List<Map<String, Object>> dutyOrgList) {
        this.dutyOrgList = dutyOrgList;
    }

    public Long getpFlowId() {
        return pFlowId;
    }

    public void setpFlowId(Long pFlowId) {
        this.pFlowId = pFlowId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getIsFlow() {
        return isFlow;
    }

    public void setIsFlow(int isFlow) {
        this.isFlow = isFlow;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getConfidentialityLevel() {
        return confidentialityLevel;
    }

    public void setConfidentialityLevel(Integer confidentialityLevel) {
        this.confidentialityLevel = confidentialityLevel;
    }
}
