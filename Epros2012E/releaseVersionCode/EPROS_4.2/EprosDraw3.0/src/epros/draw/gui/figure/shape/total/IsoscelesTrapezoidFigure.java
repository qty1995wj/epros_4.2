package epros.draw.gui.figure.shape.total;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnPaintEnum;

/**
 * 等腰梯形
 * 
 * @author Administrator
 * @date： 日期：Jun 25, 2012 时间：11:34:33 AM
 */
public class IsoscelesTrapezoidFigure extends JecnBaseFigurePanel {
	// 定义坐标属性
	private int x1 = 0;
	private int x2 = 0;
	private int x3 = 0;
	private int x4 = 0;
	private int y1 = 0;
	private int y2 = 0;
	private int y3 = 0;
	private int y4 = 0;

	public IsoscelesTrapezoidFigure(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
	}

	@Override
	public void paintComponent(Graphics g) {
		// 获取图形的宽和高
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		if (this.getFlowElementData().getCurrentAngle() == 0) {// 默认零度
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = this.getWidth() - 25;
			y3 = userHeight;
			x4 = 25;
			y4 = userHeight;
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 25;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = this.getHeight() - 25;
		} else if (this.getFlowElementData().getCurrentAngle() == 180.0) {
			x1 = 25;
			y1 = 0;
			x2 = this.getWidth() - 25;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (this.getFlowElementData().getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 25;
			x3 = userWidth;
			y3 = this.getHeight() - 25;
			x4 = 0;
			y4 = userHeight;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		paintPolygon(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintTrapezoidTop(g2d, 0);
	}

	/**
	 * 阴影
	 * 
	 * @param g2d
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		paintShadows(g2d);
	}

	/**
	 * 
	 * 填充背景区域 3D
	 * 
	 * @param g2d
	 */
	public void paint3DLow(Graphics2D g2d) {
		// 无阴影缩进值为0
		paint3DTrapezoidLow(g2d, 0);
	}

	/**
	 * 3D效果填充线条
	 * 
	 * @param g2d
	 * @param shadowCount
	 *            缩进值
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		// 无阴影缩进值为0
		paint3DRectLine(g2d, 0);
	}

	/**
	 * 3D效果填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, indent3Dvalue,userWidth,userHeight);
	}

	/**
	 * 
	 * 填充背景区域 既有3D又有阴影(底层)
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DTrapezoidLow(g2d, linevalue);
	}

	/**
	 * 3D效果填充线条(边框) 既有3D又有阴影
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DRectLine(g2d, indent3Dvalue);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, indent3Dvalue,userWidth,userHeight);
	}

	/**
	 * 3D效果填充线条
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            缩进值
	 */
	private void paint3DRectLine(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3,
				x4, 0, 0, y1, y2, y3, y4, 0, 0,indent3Dvalue, indent3Dvalue,userWidth,userHeight);
	}

	/**
	 * 填充背景区域
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            缩进值
	 */
	private void paint3DTrapezoidLow(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, indent3Dvalue, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillPolygon(
				new int[] { x1 + shadowCount, x2, x3, x4 + shadowCount },
				new int[] { y1 + shadowCount, y2 + shadowCount, y3, y4 }, 4);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintTrapezoidTop(g2d, -linevalue);
	}

	/**
	 * 无填充，只有边框
	 * 
	 * @param g2d
	 */
	private void paintPolygon(Graphics2D g2d) {
		// 图形边框色
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, userWidth, userHeight);
	}

	/**
	 * 填充，有边框
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            阴影缩进值
	 */
	private void paintTrapezoidTop(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paintHandTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4, 0, 0, y1, y2, y3, y4, 0, 0, 
				indent3Dvalue, indent3Dvalue,userWidth, userHeight);
	}
}
