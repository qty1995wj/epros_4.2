package com.jecn.epros.controller;

import com.jecn.epros.domain.home.JoinProcess;
import com.jecn.epros.domain.task.MyTaskBean;
import com.jecn.epros.domain.task.TaskSearchTempBean;
import com.jecn.epros.domain.task.TaskViewBean;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.home.HomeService;
import com.jecn.epros.service.task.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/home")
@Api(value = "/home", description = "我的主页", position = 1)
@Deprecated
public class HomeController {

    @Autowired
    private HomeService homeService;
    @Autowired
    private TaskService taskService;

    @ApiOperation(value = "我参与的流程")
    @RequestMapping(value = "/myProcesses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<JoinProcess>> fetchMyProcesses() {
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("projectId", AuthenticatedUserUtil.getProjectId());
        maps.put("peopleId", AuthenticatedUserUtil.getPeopleId());
      //  List<JoinProcess> findMyJoinProcess = homeService.findMyJoinProcess(maps);
        return ResponseEntity.ok(null);
    }

    @ApiOperation(value = "我的任务")
    @RequestMapping(value = "/myTasks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MyTaskBean>> fetchMyTasks() {
        TaskViewBean taskView = new TaskViewBean();
        Long loginPeopleId = AuthenticatedUserUtil.getPeopleId();
        List<MyTaskBean> tasks = taskService.fetchMyTasks(new TaskSearchTempBean(), loginPeopleId, null);
        taskView.setData(tasks);
        return ResponseEntity.ok(tasks);
    }
}
