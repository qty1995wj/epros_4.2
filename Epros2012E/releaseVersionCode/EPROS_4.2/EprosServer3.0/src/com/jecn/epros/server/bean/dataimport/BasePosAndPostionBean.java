package com.jecn.epros.server.bean.dataimport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 设计器和服务器 数据交互返回值封装bean
 * 
 * 
 * **/
public class BasePosAndPostionBean implements Serializable {

	/**
	 * 所有基准岗位集合
	 * 
	 * */
	private List<JecnBasePosBean> jecnBasePosBeanList = null;

	/**
	 * 基准岗位与岗位关系集合
	 * 
	 * */
	private List<JecnBasePosRelGroup> jecnBasePosRelGroupList = null;

	/**
	 * 单个岗位组对应的基准岗位集合
	 * 
	 * */
	private List<JecnBasePosBean> bJecnBasePosBeanList = null;

	public List<JecnBasePosBean> getJecnBasePosBeanList() {
		if (this.jecnBasePosRelGroupList == null) {
			if (this.jecnBasePosRelGroupList == null) {
				jecnBasePosRelGroupList = new ArrayList<JecnBasePosRelGroup>();
			}
		}
		return jecnBasePosBeanList;
	}

	public void setJecnBasePosBeanList(List<JecnBasePosBean> jecnBasePosBeanList) {
		this.jecnBasePosBeanList = jecnBasePosBeanList;
	}

	public List<JecnBasePosRelGroup> getJecnBasePosRelGroupList() {
		if (this.jecnBasePosBeanList == null) {
			jecnBasePosBeanList = new ArrayList<JecnBasePosBean>();
		}
		return jecnBasePosRelGroupList;
	}

	public void setJecnBasePosRelGroupList(List<JecnBasePosRelGroup> jecnBasePosRelGroupList) {
		this.jecnBasePosRelGroupList = jecnBasePosRelGroupList;
	}

	public List<JecnBasePosBean> getbJecnBasePosBeanList() {
		if (this.bJecnBasePosBeanList == null) {
			bJecnBasePosBeanList = new ArrayList<JecnBasePosBean>();
		}
		return bJecnBasePosBeanList;
	}

	public void setbJecnBasePosBeanList(List<JecnBasePosBean> bJecnBasePosBeanList) {
		this.bJecnBasePosBeanList = bJecnBasePosBeanList;
	}

}
