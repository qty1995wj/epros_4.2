package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncSpecialDataFilter;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

public class Cetc29SyncDataFilter extends AbstractSyncSpecialDataFilter {

	public Cetc29SyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	@Override
	protected void initSpecialDataPath() {
		this.specialDataPath = JecnPath.CLASS_PATH + "cfgFile/cetc29/cetc29SyncSpecialData.xls";
	}

	@Override
	protected void filterDeptDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void filterUserPosDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeUnusefulDataAfterCheck() {
		// TODO Auto-generated method stub

	}

}
