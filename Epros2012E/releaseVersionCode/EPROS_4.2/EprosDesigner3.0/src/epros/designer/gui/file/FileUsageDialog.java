package epros.designer.gui.file;

import java.awt.Dimension;

import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

public class FileUsageDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(FileUsageDialog.class);
	/** 文件版本信息面板 */
	private JScrollPane fileUseInfoPanel = null;
	// 文件版本信息


	public FileUsageDialog(JecnTreeNode selectNode) {
		// 设置窗体大小
		this.setSize(650, 505);
		this.setMinimumSize(new Dimension(650, 505));
		this.setTitle(JecnProperties.getValue("fileUserInfoTab"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);
		this.setModal(true);
		// 文件版本信息面板
		fileUseInfoPanel = new JScrollPane();
		fileUseInfoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 文件使用情况
		FileUseInfoDialog fileUseInfoDialog = new FileUseInfoDialog(selectNode);
		fileUseInfoDialog.setVisible(true);
		fileUseInfoPanel.setViewportView(fileUseInfoDialog);
		this.getContentPane().add(fileUseInfoPanel);
	}
}
