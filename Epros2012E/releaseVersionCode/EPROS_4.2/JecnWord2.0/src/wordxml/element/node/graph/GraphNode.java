package wordxml.element.node.graph;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.paragraph.AntiFakeBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.paragraph.AntiFake;
import wordxml.util.Tools;
import wordxml.util.Unit;

/****
 * 图片节点操作
 * 
 * @author admin
 * 
 */
public class GraphNode {
	private static final Log log = LogFactory.getLog(GraphNode.class);

	public static void createGraph(WordGraph graph, Element parentEle) {
		if (graph instanceof Image) {
			createImage((Image) graph, parentEle);
		} else if (graph instanceof LineGraph) {
			createLine((LineGraph) graph, parentEle);
		} else if (graph instanceof AntiFake) {
			createAntiFake((AntiFake) graph, parentEle);
		}
	}

	private static void createAntiFake(AntiFake graph, Element parentEle) {
		if (graph == null || parentEle == null) {
			log.info("水印为空");
			return;
		}
		AntiFakeBean pictBean = graph.getAntiFakeBean();
		Element pictE = parentEle.addElement("w:p").addElement("w:r").addElement("w:pict");
		Element shape = pictE.addElement("v:shape");

		if (graph.isText()) {
			shape.addAttribute("type", "#_x0000_t136").addAttribute("style", getShapeStyle(pictBean)).addAttribute(
					"o:allowincell", "f").addAttribute("fillcolor", pictBean.getFontColor()).addAttribute("stroked",
					"f");
			shape.addElement("v:fill").addAttribute("opacity", String.valueOf(pictBean.getOpacity()));
			// <w10:wrap anchorx="margin" anchory="margin"/> // 居中
			shape.addElement("w10:wrap").addAttribute("anchorx", "margin").addAttribute("anchory", "margin");

			Element textpath = shape.addElement("v:textpath");
			textpath.addAttribute("style", getTextpathStyle(pictBean)).addAttribute("string", graph.getValue().trim());
		} else if (graph.isImage()) {

		}
	}

	private static String getTextpathStyle(AntiFakeBean shapBean) {
		StringBuffer style = new StringBuffer();
		style.append("font-family:");
		style.append(shapBean.getFont().trim());
		style.append(";");
		style.append("font-size:");
		style.append(shapBean.getFontSize());
		style.append("pt");
		return style.toString();
	}

	private static String getShapeStyle(AntiFakeBean shapBean) {
		StringBuffer buf = new StringBuffer();
		buf.append("position:absolute;left:0;text-align:center;margin-left:0;margin-top:0;width:22in;");
		buf.append("rotation:");
		buf.append(shapBean.getRotation());
		buf.append(";");
		buf
				.append("z-index:-1;mso-position-horizontal:center;mso-position-horizontal-relative:margin;mso-position-vertical:center;mso-position-vertical-relative:margin;");
		buf.append("height:");
		buf.append(shapBean.getHeight());
		buf.append("pt;");
		buf.append("width:");
		buf.append(shapBean.getWidth());
		buf.append("pt;");
		return buf.toString();
	}

	/**
	 * 创建一个图片节点
	 * 
	 * @param image
	 * @param parentEle
	 */
	private static void createImage(Image image, Element parentEle) {
		if (image == null || parentEle == null) {
			log.info("ImageNode class createImage Argument is not valid");
			return;
		}
		Element pict = parentEle.addElement("w:pict");
		pict.addElement("w:binData").addAttribute("w:name", image.getName()).addText(
				Tools.getBase64Img(image.getImgFile()));

		// 图片属性
		Element shape = pict.addElement("v:shape");
		// 设置图片样式
		shape.addAttribute("style", getImageStyle(image));
		shape.addElement("v:imagedata").addAttribute("src", image.getName());

	}

	/**
	 * 获取图片样式
	 * 
	 * @author weidp
	 * @date 2014-10-20 下午03:01:08
	 * @param image
	 * @return
	 */
	private static String getImageStyle(Image image) {
		StringBuffer sb = setStyle(image.getPositionBean());
		sb.append("width:").append(Unit.cmToPt(image.getWidth())).append("pt;").append("height:").append(
				Unit.cmToPt(image.getHeight())).append("pt;").append("rotation:").append(image.getRotation());
		return sb.toString();
	}

	// style样式
	// width:83.25pt;height:21pt;visibility:visible
	// position:absolute;
	// margin-left:-9pt;
	// margin-top:-2.95pt;
	// width:97.4pt;
	// height:28.15pt;
	// z-index:3;
	// mso-wrap-style:none

	// position:absolute;margin-left:-9pt;margin-top:-2.95pt;width:97.4pt;height:28.15pt;z-index:3;mso-wrap-style:none
	private static StringBuffer setStyle(PositionBean positionBean) {
		StringBuffer sb = new StringBuffer();
		// 位置bean
		if (positionBean != null) {
			sb.append("position:").append(positionBean.getPositionType().toString() + ";");

			if (positionBean.getMarginLeft() != 0) {
				sb.append("margin-left:").append("" + positionBean.getMarginLeft() + "pt;");
			}
			if (positionBean.getMarginTop() != 0) {
				sb.append("margin-top:").append("" + positionBean.getMarginTop() + "pt;");
			}
			if (positionBean.getMarginBottom() != 0) {
				sb.append("margin-bottom:").append("" + positionBean.getMarginBottom() + "pt;");
			}
			if (positionBean.getMarginRight() != 0) {
				sb.append("margin-right:").append("" + positionBean.getMarginRight() + "pt;");
			}
			if (positionBean.getzIndex() != 0) {
				sb.append("z-index:").append("" + positionBean.getzIndex() + ";");
			}

		}
		return sb;
	}

	private static void createLine(LineGraph lineGraph, Element parentEle) {
		if (lineGraph == null || parentEle == null) {
			log.info("ImageNode class createImage Argument is not valid");
			return;
		}
		Element pict = parentEle.addElement("w:pict");
		Element line = pict.addElement("v:line");
		line.addAttribute("id", lineGraph.getId() + "");
		line.addAttribute("style", setStyle(lineGraph.getPositionBean()).toString());
		line.addAttribute("from", lineGraph.getFrom_x() + "pt," + lineGraph.getFrom_y() + "pt");
		line.addAttribute("to", lineGraph.getTo_x() + "pt," + lineGraph.getTo_y() + "pt");
		if (lineGraph.getStrokecolor() != null) {
			line.addAttribute("strokecolor", Tools.convertColorToRGBStr(lineGraph.getStrokecolor()));
		}
		line.addAttribute("strokeweight", lineGraph.getStrokeweight() + "pt");
		line.addElement("v:stroke").addAttribute("linestyle", lineGraph.getLineStyle().toString());
	}
}
