package com.jecn.epros.domain.journal;

import io.swagger.annotations.ApiModelProperty;

/***
 * 用户访问日志
 *
 * @author lenovo
 *
 */
public class SearchUserVisits {
    /**
     * 用户ID
     */
    private Long operationUserId;
    @ApiModelProperty(value = "用户类型 0是人员，1是岗位，2是部门")
    private int userType;
    /**
     * 文件ID
     */
    private Long relatedId;
    @ApiModelProperty(value = "文件类型 0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构")
    private int relatedType;
    @ApiModelProperty(value = "资源名称 ")
    private String relatedName;

    @ApiModelProperty(value = "开始时间 ")
    private java.lang.String startTime;

    @ApiModelProperty("结束时间 ")
    private java.lang.String endTime;

    public Long getOperationUserId() {
        return operationUserId;
    }

    public void setOperationUserId(Long operationUserId) {
        this.operationUserId = operationUserId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public int getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(int relatedType) {
        this.relatedType = relatedType;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
