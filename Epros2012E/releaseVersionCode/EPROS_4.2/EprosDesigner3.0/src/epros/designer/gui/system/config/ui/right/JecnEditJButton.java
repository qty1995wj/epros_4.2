package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnEditItemJDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.figure.shape.part.FromFigure;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;

/**
 * 
 * 编辑按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEditJButton extends JecnAbstractBaseJButton {

	public JecnEditJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// 当前属性值面板
		JecnAbstractPropertyBasePanel selectedPropContPanel = dialog.getSelectedPropContPanel();
		JecnTableScrollPane tableScrollPane = selectedPropContPanel.getTableScrollPane();

		if (tableScrollPane == null || tableScrollPane.getSelectedRows().length != 1) {// 当前面板为NULL且没有只选中一行情况直接返回
			// 提示信息：有且只有选择一行再点击编辑即可
			String oldText = (DrawCommon.isNullOrEmtryTrim(getUserInfoTextArea().getText())) ? ""
					: getUserInfoTextArea().getText();
			if (oldText != null && oldText.indexOf(JecnProperties.getValue("editSelectedOnlyOneRow")) >= 0) {
				return;
			}
			String oneRow = " " + JecnProperties.getValue("editSelectedOnlyOneRow");
			getUserInfoTextArea().setText(oldText + oneRow);
			return;
		}

		if (!DrawCommon.isNullOrEmtryTrim(getUserInfoTextArea().getText())) {// 错误信息存在情况
			String oneRow = " " + JecnProperties.getValue("editSelectedOnlyOneRow");
			String newText = getUserInfoTextArea().getText().replaceAll(oneRow, "");
			// 提示信息：有且只有选择一行再点击编辑即可
			dialog.getOkCancelJButtonPanel().getInfoLabel().setText(newText);
		}

		// 获取选中显示项
		List<JecnConfigItemBean> selectedItemBeanList = selectedPropContPanel.getTableScrollPane()
				.getSelectedItemBeanList();
		JecnConfigItemBean configItem = selectedItemBeanList.get(0);
		// 任务界面显示项 不属于 自定义项 不可编辑
		if (configItem.getTypeSmallModel() == 1
				&& (!"isShowCustomOneInput".equals(configItem.getMark())
						&& !"isShowCustomTwoInput".equals(configItem.getMark()) && !"isShowCustomThreeInput"
						.equals(configItem.getMark()))) {
			getUserInfoTextArea().setText(JecnProperties.getValue("noEditer"));
			return;
		}

		// 制度属性不可编辑想
		if (isRuleNoEditable(configItem)) {
			getUserInfoTextArea().setText(JecnProperties.getValue("noEditer"));
			return;
		}

		JecnEditItemJDialog editItemJDialog = new JecnEditItemJDialog(dialog, tableScrollPane);
		editItemJDialog.setTitle(this.getText());
		editItemJDialog.setVisible(true);
	}

	private boolean isRuleNoEditable(JecnConfigItemBean configItem) {
		if (configItem.getTypeSmallModel() == 2 && ("ruleScurityLevel".equals(configItem.getMark()))) {
			return true;
		}
		return false;
	}

	private JecnUserInfoTextArea getUserInfoTextArea() {
		return dialog.getOkCancelJButtonPanel().getInfoLabel();
	}

}