/**
 * HrPublisher.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public interface HrPublisher extends javax.xml.rpc.Service {
    public java.lang.String getHrPublisherHttpSoap11EndpointAddress();

    public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getHrPublisherHttpSoap12EndpointAddress();

    public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.crs.dataimport.HrPublisherPortType getHrPublisherHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
