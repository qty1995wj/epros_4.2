package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseOval;

/**
 * 
 * 流程图 子流程
 * 
 * @author ZHOUXY
 * 
 */
public class OvalSubFlowFigure extends BaseOval {
	public OvalSubFlowFigure(JecnFigureData figureData) {
		super(figureData);
	}
}
