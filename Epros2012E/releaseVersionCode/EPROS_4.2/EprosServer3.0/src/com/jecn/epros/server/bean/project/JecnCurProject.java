package com.jecn.epros.server.bean.project;

public class JecnCurProject implements java.io.Serializable{
	// Fields
    private Long curId;//主键ID
    private Long curProjectId;//项目ID（主项目）
    public JecnCurProject(){}
    public JecnCurProject(Long curId){
    	this.curId = curId;
    }
    public JecnCurProject(Long curId,Long curProjectId){
        this.curId = curId;
        this.curProjectId = curProjectId;
    }
	public Long getCurId() {
		return curId;
	}
	public void setCurId(Long curId) {
		this.curId = curId;
	}
	public Long getCurProjectId() {
		return curProjectId;
	}
	public void setCurProjectId(Long curProjectId) {
		this.curProjectId = curProjectId;
	}

}
