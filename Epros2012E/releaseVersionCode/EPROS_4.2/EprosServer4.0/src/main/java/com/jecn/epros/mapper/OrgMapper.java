package com.jecn.epros.mapper;

import com.jecn.epros.domain.inventory.OrgInventoryBean;
import com.jecn.epros.domain.org.*;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.sqlprovider.OrgSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

/**
 * 组织
 *
 * @author ZXH
 */
public interface OrgMapper {

    @SelectProvider(type = OrgSqlProvider.class, method = "findPostNameByIdSql")
    public String findPostNameById(Long postId);

    @SelectProvider(type = OrgSqlProvider.class, method = "findOrgNameByIdSql")
    public String findOrgNameById(Long orgId);


    /**
     * 通过Id获得类别名称
     *
     * @param typeId
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findTypeNameById")
    public String findTypeNameById(Long typeId);

    /**
     * 组织树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findChildOrgs")
    @ResultMap("mapper.Org.orgTreeBeans")
    public List<TreeNode> findChildOrgs(Map<String, Object> map);

    /**
     * 组织基础信息
     *
     * @param map
     * @return
     */
    @Select("select org_id,org_name,ORG_RESPON,t_path,t_level from jecn_flow_org where org_id=#{orgId}")
    @ResultMap("mapper.Org.orgBaseInfoBean")
    public OrgBaseInfoBean findOrgBaseInfoBean(Long orgId);

    /**
     * 岗位基础信息
     *
     * @param posId
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findPosBaseCommonBean")
    @ResultMap("mapper.Org.posBaseCommonBean")
    public PosBaseCommonBean findPosBaseCommonBean(Long posId);

    /**
     * 上级岗位
     *
     * @param posId
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findPubPosPosRelatedBeans")
    @ResultMap("mapper.Org.posRelatedBean")
    public List<PosRelatedBean> findPubPosPosRelatedBeans(Long posId);

    /**
     * 下级岗位
     *
     * @param posId
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findSubPosPosRelatedBeans")
    @ResultMap("mapper.Org.posRelatedBean")
    public List<PosRelatedBean> findSubPosPosRelatedBeans(Long posId);

    /**
     * 部门内制度
     *
     * @param posId
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findInRulePosRelatedBeans")
    @ResultMap("mapper.Org.posRelatedBean")
    public List<PosRelatedBean> findInRulePosRelatedBeans(Long posId);

    /**
     * 部门外制度
     *
     * @param posId
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findOutRulePosRelatedBeans")
    @ResultMap("mapper.Org.posRelatedBean")
    public List<PosRelatedBean> findOutRulePosRelatedBeans(Long posId);

    /**
     * 获得岗位职责
     *
     * @param posId
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "findPosRelateRole")
    @ResultMap("mapper.Org.posResBean")
    public List<PosResBean> findPosRelateRole(Long posId);

    /**
     * 组织搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchOrg")
    @ResultMap("mapper.Org.searchOrgResultBean")
    public List<SearchOrgResultBean> searchOrg(Map<String, Object> map);

    /**
     * 组织搜索 总数
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchOrgTotal")
    @ResultType(Integer.class)
    public int searchOrgTotal(Map<String, Object> map);

    /**
     * 岗位搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchPos")
    @ResultMap("mapper.Org.searchPosResultBean")
    public List<SearchPosResultBean> searchPos(Map<String, Object> map);

    /**
     * 岗位组搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchPosGroup")
    @ResultMap("mapper.Org.searchPosResultBean")
    public List<SearchPosResultBean> searchPosGroup(Map<String, Object> map);

    /**
     * 岗位统计数据搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchPosStatistics")
    @ResultMap("mapper.Org.SearchPosStatisticsResultBean")
    public List<SearchPosStatisticsResultBean> searchPosStatistics(Map<String, Object> map);

    /**
     * 岗位组统计数据搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchPosGroupStatistics")
    @ResultMap("mapper.Org.SearchPosGroupStatisticsResultBean")
    public List<SearchPosGroupStatisticsResultBean> searchPosGroupStatistics(Map<String, Object> map);
    /**
     * 岗位搜索 总数
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchPosTotal")
    @ResultType(Integer.class)
    public int searchPosTotal(Map<String, Object> map);

    /**
     * 岗位组搜索 总数
     */
    @SelectProvider(type = OrgSqlProvider.class, method = "searchPosGroupTotal")
    @ResultType(Integer.class)
    public int searchPosGroupTotal(Map<String, Object> map);

    @SelectProvider(type = OrgSqlProvider.class, method = "fetchInventory")
    @ResultMap("mapper.Inventory.orgInventoryBean")
    public List<OrgInventoryBean> fetchInventory(Map<String, Object> paramMap);

    @SelectProvider(type = OrgSqlProvider.class, method = "findOrgPaterStrByOrgId")
    public List<Map<String, Object>>  findOrgPaterStrByOrgId(Long orgId);

}
