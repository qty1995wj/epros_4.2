package epros.designer.gui.process.flowtool;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnUtil;

public class FlowToolSortDialog extends JecnSortDialog {
	private static Logger log = Logger.getLogger(FlowToolSortDialog.class);

	public FlowToolSortDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree);
	}

	@Override
	public List<JecnTreeBean> getChild() {
		try {
			return ConnectionPool.getFlowTool().getChildSustainTools(this.getSelectNode().getJecnTreeBean().getId());
		} catch (Exception e) {
			log.error("FlowToolSortDialog getChild is error", e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			ConnectionPool.getFlowTool().updateSortSustainTools(list, this.getSelectNode().getJecnTreeBean().getId(),
					JecnUtil.getUserId());
			return true;
		} catch (Exception e) {
			log.error("FlowToolSortDialog saveData is error", e);
			return false;
		}
	}

}
