package com.jecn.epros.server.download.wordxml.jinfeng;

import java.util.List;
import java.util.Map;

import wordxml.element.dom.graph.Image;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessInterfacesDescriptionBean;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

public abstract class EmptyProcessModel extends ProcessFileModel{

	

	protected EmptyProcessModel(ProcessDownloadBean processDownloadBean, String path, boolean flowChartDirection) {
		super(processDownloadBean, path, flowChartDirection);
		
	}

	@Override
	protected void a01(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a02(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a03(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a04(ProcessFileItem processFileItem, FlowDriverBean flowDriver) {
		
		
	}

	@Override
	protected void a05(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a06(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a07(ProcessFileItem processFileItem, List<KeyActivityBean> activeList) {
		
		
	}

	@Override
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		
		
	}

	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a10InParagraph(ProcessFileItem processFileItem, List<String[]> dataRows) {
		
		
	}

	@Override
	protected void a11(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a12(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a13(ProcessFileItem processFileItem, Map<String, List<RelatedProcessBean>> relatedProcessMap) {
		
		
	}

	@Override
	protected void a14(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		
		
	}

	@Override
	protected void a16(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a17(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a18(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a19(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a20(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a21(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a22(ProcessFileItem processFileItem, String... startEndActive) {
		
		
	}

	@Override
	protected void a23(ProcessFileItem processFileItem, ProcessAttributeBean responFlow) {
		
		
	}

	@Override
	protected void a24(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}

	@Override
	protected void a25(ProcessFileItem processFileItem, List<JecnRisk> riskList) {
		
		
	}

	@Override
	protected void a26(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a27(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a28(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a29(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a30(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a31(ProcessFileItem processFileItem, String... content) {
		
		
	}

	@Override
	protected void a32(ProcessFileItem processFileItem, String[] input, String[] output) {
		
		
	}

	@Override
	protected void a33(ProcessFileItem processFileItem, String... data) {
		
		
	}

	@Override
	protected void a34(ProcessFileItem processFileItem, List<TermDefinitionLibrary> definitions) {
		
		
	}

	@Override
	protected void a35(ProcessFileItem processFileItem, List<FileWebBean> relatedFiles) {
		
		
	}

	@Override
	protected void a36(ProcessFileItem processFileItem, ProcessInterfacesDescriptionBean description) {
		
		
	}
	
	@Override
	protected void a37(ProcessFileItem processFileItem, List<String[]> rowData) {
		
		
	}
}
