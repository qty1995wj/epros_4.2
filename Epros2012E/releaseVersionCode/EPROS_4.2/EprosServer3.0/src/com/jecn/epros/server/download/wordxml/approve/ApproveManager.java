package com.jecn.epros.server.download.wordxml.approve;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.util.JecnUtil;

public class ApproveManager {
	protected final Log log = LogFactory.getLog(ApproveManager.class);
	private IBaseDao<Serializable, Serializable> baseDao;
	private List<ApproveState> states = new ArrayList<ApproveState>();
	private Map<Integer, ApproveState> statesMap = new HashMap<Integer, ApproveState>();

	public ApproveManager(IBaseDao<Serializable, Serializable> baseDao, Long historyId) {
		this.baseDao = baseDao;
		String hql = "from JecnTaskHistoryNew where id=?";
		JecnTaskHistoryNew history = baseDao.getObjectHql(hql, historyId);
		build(history);
	}

	public ApproveManager(IBaseDao<Serializable, Serializable> baseDao, JecnTaskHistoryNew history) {
		this.baseDao = baseDao;
		build(history);
	}

	private void build(JecnTaskHistoryNew history) {
		if (history == null) {
			return;
		}
		Long historyId = history.getId();
		String hql = "from JecnTaskHistoryFollow where historyId=? order by sort asc";
		List<JecnTaskHistoryFollow> fs = baseDao.listHql(hql, historyId);
		if (fs.isEmpty()) {
			return;
		}
		Date createTime = history.getPublishDate();
		boolean hasCreate = false;
		List<ApproveState> states = new ArrayList<ApproveState>();
		Set<Long> peopleIds = new TreeSet<Long>();
		Set<Long> tempIds = new TreeSet<Long>();
		for (JecnTaskHistoryFollow f : fs) {
			if (f.getStageMark() == null) {
				log.error("任务审批历史表（JecnTaskHistoryFollow）对应的mark为空，ID为空" + f.getId());
				continue;
			}
			if (f.getStageMark() == 0) {
				hasCreate = true;
			}
			tempIds.clear();
			String approveId = f.getApproveId();
			if (StringUtils.isNotBlank(approveId)) {
				String[] ss = approveId.split(",");
				for (String s : ss) {
					if (StringUtils.isBlank(s)) {
						continue;
					}
					tempIds.add(Long.valueOf(s));
				}
			}
			if (!tempIds.isEmpty()) {
				peopleIds.addAll(tempIds);
			}
			if (JecnUtil.collectIsEmpty(tempIds) && StringUtils.isBlank(f.getName())) {
				continue;
			}
			ApproveState state = new ApproveState();
			states.add(state);
			state.setState(f.getStageMark());
			state.setStateName(f.getAppellation());
			List<ApprovePeople> peoples = new ArrayList<ApprovePeople>();
			state.setPeoples(peoples);
			ApprovePeople people = null;
			if (tempIds.isEmpty()) {
				people = new ApprovePeople();
				people.setDate(JecnUtil.getOrElse(f.getApprovalTime(), createTime));
				people.setName(JecnUtil.nullToEmpty(f.getName()));
				peoples.add(people);
			} else {
				for (Long id : tempIds) {
					people = new ApprovePeople();
					people.setId(id);
					people.setDate(f.getApprovalTime());
					peoples.add(people);
				}
			}
		}

		if (!hasCreate) {// 未找到拟稿人(说明是发布记录文控方式)

			ApproveState state = new ApproveState();
			state.setState(0);
			state.setStateName("拟稿人");
			List<ApprovePeople> peoples = new ArrayList<ApprovePeople>();
			ApprovePeople people = new ApprovePeople();
			peoples.add(people);
			people.setName(history.getDraftPerson());
			people.setDate(createTime);
			state.setPeoples(peoples);
			states.add(0, state);

		}

		if (JecnUtil.collectIsNotEmpty(peopleIds)) {
			Map<Long, ApprovePeople> map = getApprovePeopleMap(peopleIds);
			for (ApproveState state : states) {
				List<ApprovePeople> peoples = state.getPeoples();
				if (JecnUtil.collectIsEmpty(peoples)) {
					continue;
				}
				for (ApprovePeople p : peoples) {
					ApprovePeople approvePeople = map.get(p.getId());
					if (approvePeople != null) {
						p.setName(approvePeople.getName());
						p.setPosAndOrgs(approvePeople.getPosAndOrgs());
					}
				}
			}
		}
		this.states.addAll(states);
		for (ApproveState state : states) {
			statesMap.put(state.getState(), state);
		}
	}

	/**
	 * 将多个阶段的审批人合并
	 * 
	 * @param states
	 * @return
	 */
	public ApproveState mergeState(Integer... states) {
		ApproveState result = new ApproveState();
		result.setPeoples(new ArrayList<ApprovePeople>());
		for (Integer state : states) {
			ApproveState approveState = this.statesMap.get(state);
			if (approveState != null && JecnUtil.collectIsNotEmpty(approveState.getPeoples())) {
				result.getPeoples().addAll(approveState.getPeoples());
			}
		}
		return result;
	}

	public ApproveState getState(Integer state) {
		return this.statesMap.get(state);
	}

	public String getApprovePeopleName(Integer... state) {
		ApproveState approveState = mergeState(state);
		if (approveState == null) {
			return "";
		}
		List<ApprovePeople> peoples = approveState.getPeoples();
		if (JecnUtil.isEmpty(peoples)) {
			return "";
		}
		StringBuilder b = new StringBuilder();
		for (ApprovePeople p : peoples) {
			b.append(p.getName());
			b.append("/");
		}
		String result = b.toString();
		if (StringUtils.isNotBlank(result)) {
			return result.substring(0, result.length() - 1);
		}
		return "";
	}

	public ApproveState getState(int state) {
		return getState(Integer.valueOf(state));
	}

	public List<ApproveState> getStates() {
		return this.states;
	}

	private Map<Long, ApprovePeople> getApprovePeopleMap(Set<Long> peopleIds) {
		return getApprovePeopleMap(this.baseDao, peopleIds);
	}

	public static Map<Long, ApprovePeople> getApprovePeopleMap(IBaseDao baseDao, Set<Long> peopleIds) {
		Map<Long, ApprovePeople> m = new HashMap<Long, ApprovePeople>();
		if (JecnUtil.collectIsEmpty(peopleIds)) {
			return m;
		}
		String sql = "select ju.people_id," + "       ju.true_name," + "       jfoi.figure_id,"
				+ "       jfoi.figure_text," + "       jfo.org_id," + "       jfo.org_name" + "  from jecn_user ju"
				+ "  left join jecn_user_position_related jupr" + "    on ju.people_id = jupr.people_id"
				+ "  left join jecn_flow_org_image jfoi" + "    on jupr.figure_id = jfoi.figure_id"
				+ "  left join jecn_flow_org jfo" + "    on jfoi.org_id = jfo.org_id" + "  where ju.people_id in "
				+ JecnCommonSql.getIdsSet(peopleIds);
		List<Object[]> objs = baseDao.listNativeSql(sql);
		for (Object[] obj : objs) {
			Long peopleId = Long.valueOf(obj[0].toString());
			ApprovePeople approvePeople = m.get(peopleId);
			if (approvePeople == null) {
				approvePeople = new ApprovePeople();
				approvePeople.setId(peopleId);
				approvePeople.setName(obj[1].toString());
				approvePeople.setPosAndOrgs(new ArrayList<String[]>());
				m.put(peopleId, approvePeople);
			}
			if (obj[2] != null && obj[3] != null && obj[4] != null && obj[5] != null) {
				List<String[]> poses = approvePeople.getPosAndOrgs();
				poses.add(new String[] { obj[2].toString(), obj[3].toString(), obj[4].toString(), obj[5].toString() });
			}

		}
		return m;
	}
}
