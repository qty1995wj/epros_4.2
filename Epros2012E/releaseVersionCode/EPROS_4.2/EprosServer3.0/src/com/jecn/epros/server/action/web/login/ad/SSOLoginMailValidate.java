package com.jecn.epros.server.action.web.login.ad;

import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.email.buss.SmtpAuth;

public class SSOLoginMailValidate {

	private static Logger log = Logger.getLogger(SSOLoginMailValidate.class);

	private static Session mailSession;

	/** 收件箱地址 */
	private static String PORT = "587";
	private static String SMTP = null;

	/**
	 * @param loginName发件箱的登陆名
	 * 
	 * @param password
	 *            发件箱的密码
	 * 
	 */
	private static void initSession(String name, String password) {
		log.info("name = " + name + "password  = " + password);
		// 是否使用ssl加密
		boolean isUseSSL = false;
		// 是否匿名发送
		boolean isUseAnonymity = false;
		// 是否使用tls加密
		boolean isUseTLS = false;

		Properties props = new Properties();

		// SMTP服务器
		props.put("mail.smtp.host", SMTP);
		props.put("mail.smtp.port", PORT);

		// 是否使用ssl加密
		if (isUseSSL) {
			props.put("mail.smtp.ssl.enable", "true");
		}

		// 匿名发送
		if (isUseAnonymity) {
			props.put("mail.smtp.auth", "false");
			mailSession = Session.getInstance(props); // 获得邮件会话对象
		} else {// 验证用户名密码
			props.put("mail.smtp.auth", "true");
			SmtpAuth smtpAuth = new SmtpAuth(name, password);
			mailSession = Session.getInstance(props, smtpAuth); // 获得邮件会话对象
		}

		if (isUseTLS) {
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.ssl.trust", SMTP);
		}

		// 设置debug模式，控制台可查看发送邮件过程
		mailSession.setDebug(true);
	}

	public static EmailMessageBean getEmailMessage() {
		EmailMessageBean messageBean = new EmailMessageBean();
		JecnUser user = new JecnUser();
		messageBean.setUser(user);
		return messageBean;
	}

	/**
	 * 发送邮件
	 * 
	 */
	private static boolean validateContentEmail() throws Exception {
		boolean sendSuccess = true;
		Transport trans = null;
		try {
			trans = mailSession.getTransport("smtp");
			trans.connect();
		} catch (AuthenticationFailedException e) {
			sendSuccess = false;
			log.error("邮件发送失败！错误原因：\n" + "身份验证错误!", e);
			throw e;
		} catch (Exception e) {
			sendSuccess = false;
			log.error("邮件其它错误", e);
			throw e;
		} finally {
			try {
				if (trans != null) {
					trans.close();
				}
			} catch (MessagingException e) {// 连接关闭错误不用处理，并不代表邮件发送失败
				log.error("Transport对象关闭异常", e);
			}
		}

		return sendSuccess;
	}

	public static boolean loginValidateByEmail(String name, String password, String port, String smtp) throws Exception {
		PORT = port;
		SMTP = smtp;
		// 1. 初始化session
		SSOLoginMailValidate.initSession(name, password);
		// 2. 验证用户名和密码
		return SSOLoginMailValidate.validateContentEmail();
	}
}
