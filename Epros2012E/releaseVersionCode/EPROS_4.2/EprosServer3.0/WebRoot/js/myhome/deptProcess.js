function deptProcessGrid() {

	deptProcessGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "searchDeptProcessList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "flowId",
									mapping : "flowId"
								}, {
									name : "flowName",
									mapping : "flowName"
								}, {
									name : "flowIdInput",
									mapping : "flowIdInput"
								}, {
									name : "resPeopleName",
									mapping : "resPeopleName"
								}, {
									name : "orgName",
									mapping : "orgName"
								}, {
									name : "secretLevel",
									mapping : "isPublic"
								}, {
									name : "pubDate",
									mapping : "pubDate"
								}])

			});

	deptProcessGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer:secretLevelRenderer
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	deptProcessGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : deptProcessGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	deptProcessGridPanel = new Ext.grid.GridPanel({
				id : "deptProcessGridPanel_id",
				renderTo : 'deptProcessGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-160,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				bodyStyle:'border-left: 0px;',
				store : deptProcessGridStore,
				colModel : deptProcessGridColumnModel,
				bbar : deptProcessGridBbar
			});
	//myhome_index_id = "myhome_deptProcess_id";
}

/**
 * 部门主导流程搜索
 */
function deptProcessGridSearch(){
	//流程名称
	deptProcessGridStore.setBaseParam("processName", $("#dept_lead_processName_id").attr("value"));
	//流程编号
	deptProcessGridStore.setBaseParam("processNum", $("#dept_lead_processNumber_id").attr("value"));
	//责任部门
	deptProcessGridStore.setBaseParam("orgId", $('#dept_lead_Select_id').val());
	deptProcessGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
}
/**
* 重置部门主导流程
*/
function departmentLeadingProcessReset(){
    document.getElementById("dept_lead_processName_id").value = "";
    document.getElementById("dept_lead_processNumber_id").value = "";
    document.getElementById("dept_lead_Select_id").options[0].selected=true;
}