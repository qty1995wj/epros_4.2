package wordxml.element.bean.table;

import wordxml.constant.Constants.Align;
import wordxml.element.bean.common.BorderBean;
import wordxml.exception.WordException;

/**
 * 表格相关类
 */
public class TableBean {
	/***** 表格属性 ******/
	/** 对齐方式 值只能是center 或者 right **/
	private Align tableAlign;
	/** 表格边框属性 **/
	private BorderBean borderTop;
	private BorderBean borderLeft;
	private BorderBean borderBottom;
	private BorderBean borderRight;
	private BorderBean borderInsideH;
	private BorderBean borderInsideV;

	/** 表格内所有单元格的边距 */
	private CellMarginBean cellMarginBean;

	/****** 表格缩进 单位厘米 ****/
	private Float tableLeftInd;

	public Align getTableAlign() {
		return tableAlign;
	}

	public void setTableAlign(Align tableAlign) {
		if (tableLeftInd != null) {
			throw new WordException("设置了左缩进时，不能再设置表格居中或居右！");
		}
		if (Align.right.equals(tableAlign) || Align.center.equals(tableAlign)) {
			this.tableAlign = tableAlign;
		}
	}

	public CellMarginBean getCellMarginBean() {
		return cellMarginBean;
	}

	public void setCellMarginBean(CellMarginBean cellMarginBean) {
		this.cellMarginBean = cellMarginBean;
	}

	public BorderBean getBorderTop() {
		return borderTop;
	}

	public void setBorderTop(float width) {
		if (width <= 0) {
			this.borderTop = null;
		} else {
			this.borderTop = BorderBean.newInstance(width);
		}

	}

	public BorderBean getBorderLeft() {
		return borderLeft;
	}

	public void setBorderLeft(float width) {
		if (width <= 0) {
			this.borderLeft = null;
		} else {
			this.borderLeft = BorderBean.newInstance(width);
		}
	}

	public BorderBean getBorderBottom() {
		return borderBottom;
	}

	public void setBorderBottom(float width) {
		if (width <= 0) {
			this.borderBottom = null;
		} else {
			this.borderBottom = BorderBean.newInstance(width);
		}

	}

	public BorderBean getBorderRight() {
		return borderRight;
	}

	public void setBorderRight(float width) {
		if (width <= 0) {
			this.borderRight = null;
		} else {
			this.borderRight = BorderBean.newInstance(width);
		}
	}

	public BorderBean getBorderInsideH() {
		return borderInsideH;
	}

	public void setBorderInsideH(float width) {
		if (width <= 0) {
			this.borderInsideH = null;
		} else {
			this.borderInsideH = BorderBean.newInstance(width);
		}
	}

	public BorderBean getBorderInsideV() {
		return borderInsideV;
	}

	public void setBorderInsideV(float width) {
		if (width <= 0) {
			this.borderInsideV = null;
		} else {
			this.borderInsideV = BorderBean.newInstance(width);
		}

	}

	public Float getTableLeftInd() {
		return tableLeftInd;
	}

	/**
	 * 表格左缩进 （单位：厘米）
	 * 
	 * @param tableLeftInd
	 */
	public void setTableLeftInd(float tableLeftInd) {
		if (tableAlign != null) {
			throw new WordException("表格居中或居右不能与表格左缩进同时存在！");
		}
		this.tableLeftInd = tableLeftInd;
	}

	/**
	 * 设置边框属性
	 * 
	 * @author weidp
	 * @date 2014-10-24 上午10:20:36
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 */
	public void setCellMargin(float top, float left, float bottom, float right) {
		this.cellMarginBean = new CellMarginBean(top, left, bottom, right);
	}

	/**
	 * 设置所有边框 （单位 磅）
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午09:23:17
	 * @param width
	 */
	public void setBorder(float width) {
		BorderBean border = BorderBean.newInstance(width);
		setBorder(border);
	}
	
	public void setBorder(BorderBean border) {
		setBorderTop(border);
		setBorderBottom(border);
		setBorderLeft(border);
		setBorderRight(border);
		setBorderInsideH(border);
		setBorderInsideV(border);
	}

	public void setBorderTop(BorderBean borderTop) {
		this.borderTop = borderTop;
	}

	public void setBorderLeft(BorderBean borderLeft) {
		this.borderLeft = borderLeft;
	}

	public void setBorderBottom(BorderBean borderBottom) {
		this.borderBottom = borderBottom;
	}

	public void setBorderRight(BorderBean borderRight) {
		this.borderRight = borderRight;
	}

	public void setBorderInsideH(BorderBean borderInsideH) {
		this.borderInsideH = borderInsideH;
	}

	public void setBorderInsideV(BorderBean borderInsideV) {
		this.borderInsideV = borderInsideV;
	}

}
