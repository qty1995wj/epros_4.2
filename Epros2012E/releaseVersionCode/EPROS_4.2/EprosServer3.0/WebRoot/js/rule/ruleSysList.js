function ruleSysListGrid(json) {
	ruleSysListGridStore = new Ext.data.JsonStore( {
		autoLoad : false,
		data : json.dataList,
		fields : json.stores
	});

	ruleSysListGridColumnModel = new Ext.grid.ColumnModel( {
		defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	});

	ruleSysListGridPanel = new Ext.grid.GridPanel( {
		id : "ruleSysListGridPanel_id",
		renderTo : 'ruleSysListShow_id',
		height : document.documentElement.clientHeight - 160,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		columnLines : true,//显示列的分隔线
		enableColumnMove : false,
		viewConfig : {
			//		 让列的宽度和grid一样	
		forceFit : true
	},
	frame : false,
	store : ruleSysListGridStore,
	colModel : ruleSysListGridColumnModel
	})
}

function ruleSysListJson(ruleId, isShow) {
	Ext.Ajax.request( {
		url : 'findRuleSystemList.action',  //调用查询制度清单Action 
		method : 'POST',
		timeout : 1000000,  //设置请求超时的毫秒数
		params : {      //请求附带的参数
			ruleId : ruleId,
			showAll : isShow
		},
		success : function(response, options) {
		// 获取response中的数据
		var json = Ext.util.JSON.decode(response.responseText);
		ruleSysListGrid(json);
		document.getElementById("allOrgTotal").innerHTML = "(" + json.allTotal
				+ ")";
		//发布数
		document.getElementById("pubRuleTotal").innerHTML = "(" + json.pubTotal
				+ ")";

		//未发布数
		document.getElementById("noPubRuleTotal").innerHTML = "(" + json.noPubTotal
				+ ")";

		//待审批数
		document.getElementById("approveRuleTotal").innerHTML = "(" + json.approveTotal
				+ ")";
		document.getElementById("ruleId").value = ruleId;
	},
		// 失败调用
		failure : function(response, options) {
			alert('系统错误请联系管理员！')
		}
	});
}

// 显示全部
function showAllRuleSysList() {
	var ruleId = document.getElementById("ruleId").value;
	ruleSysListGridPanel.destroy();
	ruleSysListJson(ruleId, true);
}

// 下载
function downloadRuleSysList() {
	var ruleId = document.getElementById("ruleId").value;
	window.open("downloadRuleSystem.action?ruleId=" + ruleId);
}
