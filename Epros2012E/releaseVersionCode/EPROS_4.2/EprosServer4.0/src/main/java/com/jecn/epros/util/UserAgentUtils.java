package com.jecn.epros.util;

import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.http.HttpServletRequest;

public class UserAgentUtils {

    public static UserAgent getUserAgentByRequest(HttpServletRequest request) {
        String userAgentString = request.getHeader("User-Agent");
        return UserAgent.parseUserAgentString(userAgentString);
    }
}
