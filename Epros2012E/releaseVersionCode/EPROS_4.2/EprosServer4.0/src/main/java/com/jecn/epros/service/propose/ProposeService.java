package com.jecn.epros.service.propose;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.propose.*;

import java.util.List;
import java.util.Map;

public interface ProposeService {

    public Integer getProposeNum(ProposeSearch proposeParam, Integer proposeState, Boolean isAdmin, Long loginPeopleId);

    public List<Propose> fetchProposes(ProposeSearch param, Boolean isAdmin, Long loginPeopleId);

    public int updateProposeState(String proposeId, Integer intflag, String strContent, JecnUser jecnUser);

    public Propose savePropose(ProposeCreate param, Long peopleId);

    public int editPropose(ProposeEdit param, Long peopleId);

    public void clearReply(String proposeId);

    public MyProposeTopic getMyProposesTopic(Long peopleId, Boolean isPaging);

    public List<ProposeTopic> searchProposeTopic(TopicSearch topicSearch, boolean isAdmin, Long peopleId, Paging paging);

    public ProposeTopic getProposeTopic(Map<String, Object> paramMap);

    public List<ProposeTopic> searchProposeTopic(Integer type, Paging paging);

    public void distributeHandlePeople(Map<String, Object> paramMap);

    public int searchProposeTopicNum(Integer type);

    public int searchProposeTopicNum(TopicSearch topicSearch, boolean isAdmin, Long peopleId);

    public int deletePropose(String proposeId, Long peopleId, boolean isAdmin);

    /**
     * 合理化统计图表
     *
     * @param map
     * @return
     */
    public ProposeTotalChart findProposeTotalChart(Map<String, Object> map);

    /**
     * 合理化建议统计 内容
     *
     * @param map
     * @return
     */
    public List<ProposeTotalData> findProposeTotalData(Map<String, Object> map);

    /**
     * 合理化建议统计 总计
     *
     * @param map
     * @return
     */
    public int findProposeTotal(Map<String, Object> map);

    List<ProposeFile> getProposeFileByProposeId(Map<String, Object> map);

    JecnUser getTopicHandler(Map<String, Object> map);
}
