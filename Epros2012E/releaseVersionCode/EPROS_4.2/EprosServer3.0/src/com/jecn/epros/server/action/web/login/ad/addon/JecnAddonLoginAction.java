package com.jecn.epros.server.action.web.login.ad.addon;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 
 * 华帝单点登录
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAddonLoginAction extends JecnAbstractADLoginAction {
	public JecnAddonLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		return null;
	}
}
