package com.jecn.webservice.mengniu.arch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

public class FtpTest {
	public static void main(String[] args) throws Exception {
		// 向FTP服务器上传文件
		try {
			FileInputStream in = new FileInputStream(new File("d:/1.txt"));
			boolean flag = uploadFile("127.0.0.1", -1, "kk", "1234", "1.txt", in);
			System.out.println(flag);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// 向FTP服务器下载文件
		// boolean flag = downloadFile("127.0.0.1", -1, "huozuoyang", "123456",
		// "D:/edoc", "555.txt","e:/updown" );
		// System.out.println(flag);

	}

	/**
	 * Description: 向FTP服务器上传文件
	 * 
	 * @param url
	 *            FTP服务器hostname
	 * @param port
	 *            FTP服务器端口，如果默认端口请写-1
	 * @param username
	 *            FTP登录账号
	 * @param password
	 *            FTP登录密码
	 * @param path
	 *            FTP服务器保存目录
	 * @param filename
	 *            上传到FTP服务器上的文件名
	 * @param input
	 *            输入流
	 * @return 成功返回true，否则返回false
	 */
	public static boolean uploadFile(String url, int port, String username, String password, String path,
			InputStream input) {

		if (path.lastIndexOf("/") >= 0) {
			String dirs = path.substring(0, path.lastIndexOf("/"));
			makeDir(url, port, username, password, dirs);
		}
		boolean success = false;
		FTPClient ftp = new FTPClient();
		try {
			int reply;

			// 连接FTP服务器
			if (port > -1) {
				ftp.connect(url, port);
			} else {
				ftp.connect(url);
			}

			// 登录FTP
			ftp.login(username, password);
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}
			ftp.changeWorkingDirectory(path);
			ftp.storeFile(path, input);

			input.close();
			ftp.logout();
			success = true;
		} catch (IOException e) {
			success = false;
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException e) {
				}
			}
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {

				}
			}
		}
		return success;
	}

	/**
	 * Description: 从FTP服务器下载文件
	 * 
	 * @Version1.0 Jul 27, 2008 5:32:36 PM by 崔红保（cuihongbao@d-heaven.com）创建
	 * @param url
	 *            FTP服务器hostname
	 * @param port
	 *            FTP服务器端口
	 * @param username
	 *            FTP登录账号
	 * @param password
	 *            FTP登录密码
	 * @param remotePath
	 *            FTP服务器上的相对路径
	 * @param fileName
	 *            要下载的文件名
	 * @param localPath
	 *            下载后保存到本地的路径
	 * @return
	 */
	public static boolean downloadFile(String url, int port, String username, String password, String remotePath,
			String fileName, String localPath) {
		boolean success = false;
		FTPClient ftp = new FTPClient();
		try {
			int reply;

			// 连接FTP服务器
			if (port > -1) {
				ftp.connect(url, port);
			} else {
				ftp.connect(url);
			}

			ftp.login(username, password);// 登录
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}
			ftp.changeWorkingDirectory(remotePath);// 转移到FTP服务器目录
			FTPFile[] fs = ftp.listFiles();
			for (FTPFile ff : fs) {
				if (ff.getName().equals(fileName)) {
					File localFile = new File(localPath + "/" + ff.getName());

					OutputStream is = new FileOutputStream(localFile);
					ftp.retrieveFile(ff.getName(), is);
					is.close();
				}
			}

			ftp.logout();
			success = true;
		} catch (IOException e) {
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException e) {
				}
			}
		}
		return success;
	}

	/**
	 * <删除FTP上的文件> <远程删除FTP服务器上的录音文件>
	 * 
	 * @param url
	 *            FTP服务器IP地址
	 * @param port
	 *            FTP服务器端口
	 * @param username
	 *            FTP服务器登录名
	 * @param password
	 *            FTP服务器密码
	 * @param remotePath
	 *            远程文件路径
	 * @param fileName
	 *            待删除的文件名
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	public static boolean deleteFtpFile(String url, int port, String username, String password, String remotePath,
			String fileName) {
		boolean success = false;
		FTPClient ftp = new FTPClient();
		try {
			int reply;

			// 连接FTP服务器
			if (port > -1) {
				ftp.connect(url, port);
			} else {
				ftp.connect(url);
			}

			// 登录
			ftp.login(username, password);
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}

			// 转移到FTP服务器目录
			ftp.changeWorkingDirectory(remotePath);
			success = ftp.deleteFile(remotePath + "/" + fileName);
			ftp.logout();
		} catch (IOException e) {
			success = false;
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException e) {
				}
			}
		}
		return success;
	}

	/**
	 * Description: 向FTP服务器上传文件
	 * 
	 * @param url
	 *            FTP服务器hostname
	 * @param port
	 *            FTP服务器端口，如果默认端口请写-1
	 * @param username
	 *            FTP登录账号
	 * @param password
	 *            FTP登录密码
	 * @param path
	 *            FTP服务器保存目录
	 * @param filename
	 *            上传到FTP服务器上的文件名
	 * @param input
	 *            输入流
	 * @return 成功返回true，否则返回false
	 */
	public static boolean makeDir(String url, int port, String username, String password, String path) {
		boolean success = false;
		FTPClient ftp = new FTPClient();
		try {
			int reply;

			// 连接FTP服务器
			if (port > -1) {
				ftp.connect(url, port);
			} else {
				ftp.connect(url);
			}

			// 登录FTP
			ftp.login(username, password);
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}
			if (ftp.changeWorkingDirectory(path)) {// 可能是有目录的意思
				return true;
			}
			String[] arr = path.split("/");
			// 循环生成子目录
			for (String s : arr) {
				// 尝试切入目录
				if (ftp.changeWorkingDirectory(s)) {
					continue;
				}
				if (!ftp.makeDirectory(s)) {
					System.out.println("[失败]ftp创建目录：" + s);
					return false;
				}
				System.out.println("[成功]创建ftp目录：" + s);
			}

			ftp.logout();
			success = true;
		} catch (IOException e) {
			success = false;
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException e) {
				}
			}
		}
		return success;
	}

}
