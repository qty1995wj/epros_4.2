<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div" style="height: 99%;">
			<form action="" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="searchText" style="overflow: hidden;" class="minWidth">
						<table width="100%">
							<tr>
								<td width="12%" height="25px" align="right">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="processName_application_id" maxLength="32" readonly="readonly"/>
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectProcessOrMaWindow("processName_application_id","processId_application_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
								</td>
								<td width="12%" align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_application_id"
										onkeyup="search(orgDutyName_application_id,orgDutyId_application_id,search_application_orgDutyName,'org');"
										onblur="divNode(search_application_orgDutyName)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_application_id","orgDutyId_application_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_application_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>
							</tr>
						</table>
					</div>

					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;" onclick="processApplicationGridSearch()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;" onclick="processApplicationReset()" />
					</div>
				</div>
				<!-- 流程ID -->
				<input type="hidden" id="processId_application_id" value="-1" />
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_application_id" value="-1" />
			</form>
			<div id="processApplicationGrid_id">

			</div>
			<div align="center" style="padding-top: 10px;">
				<a onclick="processApplicationDownloadExcel()"
					style="cursor: pointer; color: #005EA7;" class="table"><s:text
						name="downLoadExcel" /> </a>
			</div>
		</div>
	</body>
</html>