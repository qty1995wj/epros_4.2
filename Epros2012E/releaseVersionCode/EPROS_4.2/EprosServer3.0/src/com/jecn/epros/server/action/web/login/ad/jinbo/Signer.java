package com.jecn.epros.server.action.web.login.ad.jinbo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class Signer {
	private static Logger log = Logger.getLogger(Signer.class);
	private static char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	public static String calculateSign(String action, String secret, String sysCode, String userName, Long timestamp) {
		String signingText = secret + "|" + action + "|" + sysCode + "|" + userName + "|" + timestamp + "|" + secret;
		return Signer.md5(signingText);
	}

	public static String md5(String input) {
		java.security.MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e1) {
			// impossible to be here.
			e1.printStackTrace();
		}
		try {
			md.update(input.getBytes("UTF-8"));
		} catch (Exception e) {
			log.error("md5()异常", e);
		}
		byte[] byteDigest = md.digest();
		return toHexString(byteDigest);
	}

	private static String toHexString(byte[] byteDigest) {
		char[] chars = new char[byteDigest.length * 2];
		for (int i = 0; i < byteDigest.length; i++) {
			// left is higher.
			chars[i * 2] = HEX_DIGITS[byteDigest[i] >> 4 & 0x0F];
			// right is lower.
			chars[i * 2 + 1] = HEX_DIGITS[byteDigest[i] & 0x0F];
		}
		return new String(chars);
	}

	public static void main(String... args) {
		String secretMd5 = Signer.calculateSign("sso", "1f3df1da003b3ea1f1855917935c51c0", "jingbo", "admin", 1530848458000L);
		System.out.println(secretMd5);
		System.out.println(Signer.md5("jingbo"));
	}

}
