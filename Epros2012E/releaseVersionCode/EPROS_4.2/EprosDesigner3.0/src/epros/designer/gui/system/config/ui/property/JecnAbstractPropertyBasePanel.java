package epros.designer.gui.system.config.ui.property;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 基本信息面板
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAbstractPropertyBasePanel extends JPanel {
	/** 配置界面 */
	protected JecnAbtractBaseConfigDialog dialog = null;
	/** 表面板 */
	protected JecnTableScrollPane tableScrollPane = null;
	/** 选中节点 */
	protected JecnConfigTypeDesgBean configTypeDesgBean = null;

	public JecnAbstractPropertyBasePanel(JecnAbtractBaseConfigDialog dialog) {
		if (dialog == null) {
			throw new IllegalArgumentException("JecnAbstractPropertyBasePanel is error");
		}
		this.dialog = dialog;

		iniComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 设置背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setBorder(null);
		this.setLayout(new BorderLayout());

	}

	/**
	 * 
	 * 设置错误信息
	 * 
	 * @param addrInfoTextArea
	 *            JecnUserInfoTextArea
	 * @param info
	 * @return boolean true:有错；false：无错
	 */
	public boolean setErrorInfo(JecnUserInfoTextArea addrInfoTextArea,
			String info) {
		if (!DrawCommon.isNullOrEmtryTrim(info)) {// 有错
			addrInfoTextArea.setText(info);
			return true;
		} else {// 无错
			addrInfoTextArea.setText("");
			return false;
		}
	}

	public JecnAbtractBaseConfigDialog getDialog() {
		return dialog;
	}

	public JecnConfigTypeDesgBean getConfigTypeDesgBean() {
		return configTypeDesgBean;
	}

	public JecnTableScrollPane getTableScrollPane() {
		return tableScrollPane;
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public abstract void initData(JecnConfigTypeDesgBean configTypeDesgBean);

	/** 数据校验 true:成功；false：失败 */
	public abstract boolean check();

}
