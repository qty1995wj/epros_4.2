package epros.designer.gui.process.activitiesProperty;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;

import org.apache.log4j.Logger;

import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

/*******************************************************************************
 * 活动明细： 输入/输出
 * 
 * @author 2012-07-27
 * 
 */
public class ActivitiesInOutPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(ActivitiesInPanel.class);

	private static final long serialVersionUID = 1L;
	/** 组件的拥有者 */
	private JecnPanel contentPanel = new JecnPanel();
	// 标准条款化 输入
	private ActivitiesTermsInOutPanel activitiesTermsInPanel = null;

	// 标准条款化 输出
	private ActivitiesTermsInOutPanel activitiesTermsOutPanel = null;

	private List<JecnFigureInoutT> listFigureInTs = null;
	private List<JecnFigureInoutT> listFigureOutTs = null;

	/**
	 * 输入/输出
	 * 
	 * @param type
	 *            调用类型 0 输入 1 输出
	 * @throws Exception
	 */
	public ActivitiesInOutPanel(JecnActiveData jecnActiveData, JecnDialog jecnDialog) throws Exception {
		listFigureInTs = jecnActiveData.getListFigureInTs(); // 输入
		listFigureOutTs = jecnActiveData.getListFigureOutTs();// 输出
		initData(listFigureInTs, listFigureOutTs);
		initControl();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		listFigureInTs = jecnActiveData.getListFigureInTs(); // 输入
		listFigureOutTs = jecnActiveData.getListFigureOutTs();// 输出
		activitiesTermsInPanel.initInfoPanelData(listFigureInTs, 0);
		activitiesTermsOutPanel.initInfoPanelData(listFigureOutTs, 1);
	}

	/**
	 * 初始化 组件
	 */
	private void initControl() {
		GridBagConstraints c = null;
		Insets insets = new Insets(1, 1, 1, 1);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(contentPanel, c);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		contentPanel.add(activitiesTermsInPanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		contentPanel.add(activitiesTermsOutPanel, c);
	}

	/**
	 * 获取输入数据
	 * 
	 * @return
	 */
	protected List<JecnFigureInoutT> getInData() {
		return activitiesTermsInPanel.getData();
	}

	/**
	 * 获取输出数据
	 * 
	 * @return
	 */
	protected List<JecnFigureInoutT> getOutData() {
		return activitiesTermsOutPanel.getData();
	}

	/**
	 *是否更新
	 * 
	 * @return
	 */
	protected boolean outIsUpdate() {
		return activitiesTermsOutPanel.isUpdate();
	}

	/**
	 *是否更新
	 * 
	 * @return
	 */
	protected boolean inIsUpdate() {
		return activitiesTermsInPanel.isUpdate();
	}

	/**
	 * 初始化 数据
	 * 
	 * @throws Exception
	 */
	private void initData(List<JecnFigureInoutT> listFigureInTs, List<JecnFigureInoutT> listFigureOutTs)
			throws Exception {
		activitiesTermsInPanel = new ActivitiesTermsInOutPanel(listFigureInTs, 0);
		activitiesTermsOutPanel = new ActivitiesTermsInOutPanel(listFigureOutTs, 1);

	}

}
