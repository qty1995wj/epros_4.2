package com.jecn.epros.server.dao.popedom.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.popedom.IFlowOrgImageDao;
import com.jecn.epros.server.util.JecnUtil;

public class FlowOrgImageDaoImpl extends AbsBaseDao<JecnFlowOrgImage, Long> implements IFlowOrgImageDao {

	@Override
	public List<Object[]> getRoleGroupByList(Long peopleId, Long projectId) throws Exception {
		String sql = "SELECT JFSI.FIGURE_ID," + "       JFSI.FIGURE_TEXT," + "       JFSI.ROLE_RES,"
				+ "       JFS.FLOW_ID," + "       JFS.FLOW_NAME," + "       JFS.FLOW_ID_INPUT,"
				+ "       JFOI.FIGURE_ID    AS POS_ID," + "       JFS.PRE_FLOW_ID   AS PFLOW_ID,"
				+ "       PJFS.FLOW_NAME    AS PNAME," + "       PJFS.ISFLOW" + "  FROM JECN_FLOW_STRUCTURE_IMAGE JFSI"
				+ " INNER JOIN JECN_FLOW_STRUCTURE JFS" + "    ON JFSI.FLOW_ID = JFS.FLOW_ID"
				+ " INNER JOIN PROCESS_STATION_RELATED PRS" + "    ON JFSI.FIGURE_ID = PRS.FIGURE_FLOW_ID"
				+ "   AND PRS.TYPE = '1'" + " INNER JOIN JECN_POSITION_GROUP_R JPGR"
				+ "    ON PRS.FIGURE_POSITION_ID = JPGR.GROUP_ID" + " INNER JOIN JECN_FLOW_ORG_IMAGE JFOI"
				+ "    ON JPGR.FIGURE_ID = JFOI.FIGURE_ID" + " INNER JOIN JECN_FLOW_ORG JFO"
				+ "    ON JFOI.ORG_ID = JFO.ORG_ID" + "   AND JFO.DEL_STATE = 0" + "   AND JFO.PROJECTID = ?"
				+ " INNER JOIN JECN_USER_POSITION_RELATED JUPR" + "    ON JFOI.FIGURE_ID = JUPR.FIGURE_ID"
				+ "   AND JUPR.PEOPLE_ID = ?" + "  LEFT JOIN JECN_FLOW_STRUCTURE PJFS"
				+ "    ON JFS.PRE_FLOW_ID = PJFS.FLOW_ID" + " WHERE JFS.DEL_STATE = 0" + "   AND JFS.PROJECTID = ?"
				+ " ORDER BY JFS.UPDATE_DATE DESC, JFS.PRE_FLOW_ID, JFS.SORT_ID, JFS.FLOW_ID";
		return this.listNativeSql(sql, projectId, peopleId, projectId);
	}

	@Override
	public List<Object[]> getRoleNOGroupByList(Long peopleId, Long projectId) throws Exception {
		String sql = "SELECT JFSI.FIGURE_ID," + "       JFSI.FIGURE_TEXT," + "       JFSI.ROLE_RES,"
				+ "       JFS.FLOW_ID," + "       JFS.FLOW_NAME," + "       JFS.FLOW_ID_INPUT,"
				+ "       JFOI.FIGURE_ID    AS POS_ID," + "       JFS.PRE_FLOW_ID   AS PFLOW_ID,"
				+ "       PJFS.FLOW_NAME    AS PNAME," + "       PJFS.ISFLOW" + "  FROM JECN_FLOW_STRUCTURE_IMAGE JFSI"
				+ " INNER JOIN JECN_FLOW_STRUCTURE JFS" + "    ON JFSI.FLOW_ID = JFS.FLOW_ID"
				+ " INNER JOIN PROCESS_STATION_RELATED PRS" + "    ON JFSI.FIGURE_ID = PRS.FIGURE_FLOW_ID"
				+ "   AND PRS.TYPE = '0'" + " INNER JOIN JECN_FLOW_ORG_IMAGE JFOI"
				+ "    ON PRS.FIGURE_POSITION_ID = JFOI.FIGURE_ID" + " INNER JOIN JECN_FLOW_ORG JFO"
				+ "    ON JFOI.ORG_ID = JFO.ORG_ID" + "   AND JFO.DEL_STATE = 0" + "   AND JFO.PROJECTID = ?"
				+ " INNER JOIN JECN_USER_POSITION_RELATED JUPR" + "    ON JFOI.FIGURE_ID = JUPR.FIGURE_ID"
				+ "   AND JUPR.PEOPLE_ID = ?" + "  LEFT JOIN JECN_FLOW_STRUCTURE PJFS"
				+ "    ON JFS.PRE_FLOW_ID = PJFS.FLOW_ID" + " WHERE JFS.DEL_STATE = 0" + "   AND JFS.PROJECTID = ?"
				+ " ORDER BY JFS.UPDATE_DATE DESC, JFS.PRE_FLOW_ID, JFS.SORT_ID, JFS.FLOW_ID";
		return this.listNativeSql(sql, projectId, peopleId, projectId);
	}

	@Override
	public List<JecnUser> listPosUsers(Collection<Long> ids) throws Exception {
		if(JecnUtil.isEmpty(ids)){
			return new ArrayList<JecnUser>();
		}
		String idStr = JecnUtil.concatIds(ids);
		String sql = "select ju.* from jecn_user ju"
				+ "  inner join jecn_user_position_related jupr on ju.people_id=jupr.people_id"
				+ "  inner join jecn_flow_org_image jfoi on jfoi.figure_id=jupr.figure_id"
				+ "  where ju.islock=0 and jfoi.figure_id in" + idStr;
		return (List<JecnUser>) this.listNativeAddEntity(sql, JecnUser.class);
	}

	@Override
	public List<JecnUser> listGroupUsers(Collection<Long> ids) {
		if(JecnUtil.isEmpty(ids)){
			return new ArrayList<JecnUser>();
		}
		String idStr = JecnUtil.concatIds(ids);
		String sql = "select ju.* from jecn_user ju"
				+ "  inner join jecn_user_position_related jupr on ju.people_id=jupr.people_id"
				+ "  inner join jecn_flow_org_image jfoi on jfoi.figure_id=jupr.figure_id"
				+ "  inner join jecn_position_group_r jpgr on jfoi.figure_id=jpgr.figure_id"
				+ "  where ju.islock=0 and jpgr.group_id in" + idStr;
		return (List<JecnUser>) this.listNativeAddEntity(sql, JecnUser.class);
	}
}
