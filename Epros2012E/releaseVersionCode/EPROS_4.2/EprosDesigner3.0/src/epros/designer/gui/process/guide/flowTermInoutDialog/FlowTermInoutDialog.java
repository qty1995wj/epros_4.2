package epros.designer.gui.process.guide.flowTermInoutDialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.system.PosGroupMutilSelectCommon;
import epros.designer.gui.system.PosMutilSelectCommon;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

public class FlowTermInoutDialog extends JecnDialog {

	private JecnPanel contentPanel = new JecnPanel(300, 300);
	/** 名称Lab */
	private JLabel nameLab = new JLabel(JecnProperties.getValue("nameC"));
	/** 名称 */
	private JecnTextField nameField = new JecnTextField();

	/** 接收者 提供者 */
	private JLabel accessLab = new JLabel();
	/**
	 * 接收者 ： 提供者
	 */
	private JecnPanel accessPanel = new JecnPanel();

	/**
	 * 接收者 ： 提供者 输入框
	 */
	private JecnTextField accessNameField = new JecnTextField();

	/** 岗位多选 */
	PosMutilSelectCommon posMutilSelectCommon = null;
	/** 岗位组多选 */
	PosGroupMutilSelectCommon posGroupMutilSelectCommon = null;

	/** 说明Lab */
	private JLabel explainLab = new JLabel(JecnProperties.getValue("actBaseDes"));
	/** 说明 */
	private JecnTextArea explainText = new JecnTextArea();
	/** 所在的滚动面板 */
	private JScrollPane noteTablePane = new JScrollPane();

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(550, 30);

	private JButton addBut = new JButton(JecnProperties.getValue("task_okBtn"));

	private JButton delBut = new JButton(JecnProperties.getValue("task_cancelBtn"));

	private JLabel verfyLab = new JLabel();

	private JecnFlowInoutData data = new JecnFlowInoutData();

	/**
	 * 表名称
	 */
	private List<String> tableNames = new ArrayList<String>();
	// 是否确认
	protected boolean isOk;
	// 活动ID
	private Long figureId;
	// true 输入 false 输出
	private boolean type;

	private Long flowId;

	public FlowTermInoutDialog(JecnFlowInoutData data, List<String> names, boolean type, Long flowId) {
		this.isOk = false;
		this.type = type;
		this.data = data;
		this.flowId = flowId;
		this.tableNames = names;
		initializationInterface();
		initData();
		initEvent();

	}

	private void initData() {
		nameField.setText(data.getInout().getName());
		accessNameField.setText(data.getInout().getPos());
		explainText.setText(data.getInout().getExplain());
	}

	/**
	 * 初始化界面
	 * 
	 */
	protected void initializationInterface() {
		// 设置窗体大小
		this.setSize(600, 600);
		this.setTitle(JecnProperties.getValue("addBtn"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		// this.setResizable(false);
		this.setModal(true);
		this.setLayout(new GridBagLayout());
		contentPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		accessPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension1 = new Dimension(200, 220);
		accessPanel.setPreferredSize(dimension1);
		accessPanel.setMaximumSize(dimension1);
		accessPanel.setMinimumSize(dimension1);
		noteTablePane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(200, 220);
		noteTablePane.setPreferredSize(dimension);
		noteTablePane.setMaximumSize(dimension);
		noteTablePane.setMinimumSize(dimension);
		noteTablePane.setViewportView(explainText);
		verfyLab.setForeground(Color.red);
		accessPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));// 设置面板边框颜色
		if (type) {
			accessLab.setText(JecnProperties.getValue("offer"));
		} else {
			accessLab.setText(JecnProperties.getValue("recipient"));
		}
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(contentPanel, c);
		// 名称
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		contentPanel.add(nameLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(nameField, c);

		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		contentPanel.add(accessLab, c);

		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(accessPanel, c);

		c = new GridBagConstraints(0, 0, 5, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		accessPanel.add(accessNameField, c);
		// 岗位
		posMutilSelectCommon = new PosMutilSelectCommon(1, accessPanel, insets, data.getPoses(), this, false,
				JecnProperties.getValue("positionC"));
		// 岗位组
		posGroupMutilSelectCommon = new PosGroupMutilSelectCommon(2, accessPanel, insets, data.getPosGroups(),
				JecnProperties.getValue("positionGroupC"), this, false);
		// 说明
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);
		contentPanel.add(explainLab, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(noteTablePane, c);

		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(addBut);
		buttonPanel.add(delBut);

	}

	private void initEvent() {

		addBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ok();
			}
		});
		delBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
	}

	private void close() {
		this.setVisible(false);
	}

	private void ok() {
		// 名称 122个字符或60个汉字
		if (JecnValidateCommon.validateFieldNotPass(nameField.getText(), verfyLab, nameLab.getText())) {
			return;
		}
		// 接收者 tigongzhe 122个字符或60个汉字
		if (JecnValidateCommon.validateFieldNotPass(accessNameField.getText(), verfyLab, accessLab.getText())) {
			return;
		}
		// 验证重名
		if (validationRename(nameField.getText())) {
			return;
		}
		// 说明 1200个字符或600个汉字
		if (JecnValidateCommon.validateAreaNotPass(explainText.getText(), verfyLab, explainLab.getText())) {
			return;
		}

		if (StringUtils.isBlank(nameField.getText()) && StringUtils.isBlank(explainText.getText())
				&& StringUtils.isBlank(accessNameField.getText())
				&& (data.getPoses() == null || data.getPoses().isEmpty())
				&& (data.getPosGroups() == null || data.getPosGroups().isEmpty())) {
			verfyLab.setText(JecnProperties.getValue("fillHint"));
			return;
		}
		data.getInout().setName(nameField.getText());
		data.getInout().setExplain(explainText.getText());
		data.getInout().setType(type ? 0 : 1);
		data.getInout().setFlowId(flowId);
		data.getInout().setSortId(data.getInout().getSortId());
		data.getInout().setPos(accessNameField.getText());
		this.isOk = true;
		this.setVisible(false);
	}

	/**
	 * 验证重名
	 * 
	 * 447@param resourceMap44
	 * 
	 * @return
	 */
	private boolean validationRename(String name) {
		if (StringUtils.isNotBlank(name)) {
			if (tableNames != null && !tableNames.isEmpty()) {
				for (String tableName : tableNames) {
					if (name.equals(tableName)) {
						verfyLab.setText(JecnProperties.getValue("notRename"));
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}

}
