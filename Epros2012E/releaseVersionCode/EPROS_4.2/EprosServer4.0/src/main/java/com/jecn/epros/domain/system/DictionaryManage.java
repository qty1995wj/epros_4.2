package com.jecn.epros.domain.system;

import java.util.List;
import java.util.Map;

public class DictionaryManage implements java.io.Serializable{
    List<String> transName;
    List<Dictionary> oldData;
    List<Dictionary> newData;

    public List<String> getTransName() {
        return transName;
    }

    public void setTransName(List<String> transName) {
        this.transName = transName;
    }

    public List<Dictionary> getOldData() {
        return oldData;
    }

    public void setOldData(List<Dictionary> oldData) {
        this.oldData = oldData;
    }

    public List<Dictionary> getNewData() {
        return newData;
    }

    public void setNewData(List<Dictionary> newData) {
        this.newData = newData;
    }
}
