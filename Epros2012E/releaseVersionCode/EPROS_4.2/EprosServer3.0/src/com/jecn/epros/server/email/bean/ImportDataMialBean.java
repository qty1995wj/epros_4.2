package com.jecn.epros.server.email.bean;

import java.io.File;

/**
 * 
 * 人员同步，邮件信息获取
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-12-19 时间：下午05:03:24
 */
public class ImportDataMialBean {
	/** 人员变更，标题 */
	private String subjet = "人员同步存在变更，请查阅！";
	/** 人员变更，内容 */
	private String mailContent = "人员同步存在变更，请查阅！";
	/** 人员变更 附件 */
	private File file;

	public String getSubjet() {
		return subjet;
	}

	public void setSubjet(String subjet) {
		this.subjet = subjet;
	}

	public String getMailContent() {
		return mailContent;
	}

	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}
