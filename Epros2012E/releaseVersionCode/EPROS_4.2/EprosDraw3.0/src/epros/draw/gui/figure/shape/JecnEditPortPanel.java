package epros.draw.gui.figure.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnBaseResizePanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 图形上下左右四个编辑点标识
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEditPortPanel extends JecnBaseResizePanel {
	/** 上下左右四个编辑点标识 */
	private EditPointType editPointType = null;
	/** 编辑点相关图形 */
	private JecnBaseFigurePanel baseFigurePanel;
	/** 编辑点为开始端口或者结束端口标识 True 开始端口（输出端口）；false 结束端口（输入端口） */
	private boolean isStartPort = false;
	/**
	 * 当前编辑点是否为连接线正连接状态{true:正连接状态，false：未连接状态}；用于添加连接线时判断标识：
	 * JecnEditPortEventProcess true:编辑点大小为14背景为红色；false:大小为12,背景为绿色
	 */
	private boolean isAllowLink = false;

	private JecnEditPortPanel(EditPointType editPointType, JecnBaseFigurePanel baseFigurePanel) {
		if (editPointType == null || baseFigurePanel == null) {
			JecnLogConstants.LOG_JECN_BASE_RESIZE_PANEL.error("JecnFigureResizePanel类构造函数：参数为null。editPointType="
					+ editPointType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		// 设置大小
		this.setSize(16, 16);
		// 设置鼠标为十字光标类型
		this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		this.editPointType = editPointType;
		this.baseFigurePanel = baseFigurePanel;

		// 设置背景透明
		this.setOpaque(false);
		// 设置图形编辑点开始端口和结束端口
		setUpStartPort(editPointType);
	}

	/**
	 * 设置图形编辑点开始端口和结束端口
	 * 
	 * @param editPort
	 */
	private void setUpStartPort(EditPointType editPointType) {
		if (editPointType == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// True 开始端口（输出端口）；false 结束端口（输入端口）
		if (JecnWorkflowUtil.isPartMap(desktopPane)) {
			processStartPort(editPointType);
			return;
		}
		switch (editPointType) {
		case left:
		case top:
		case right:
		case bottom:
			isStartPort = true;
			break;
		}
	}

	/**
	 * 流程图 连接线开始点
	 * 
	 * @param editPointType
	 */
	private void processStartPort(EditPointType editPointType) {
		if (editPointType == null) {
			return;
		}
		if (JecnSystemStaticData.isEditPortType() == 1) {// 左上进右下出
			if (EditPointType.right.equals(editPointType) || EditPointType.bottom.equals(editPointType)) {
				isStartPort = true;
			}
		} else if (JecnSystemStaticData.isEditPortType() == 2) {// 左上进右下出
			if (EditPointType.right.equals(editPointType)) {
				isStartPort = true;
			}
		} else {// 默认0
			isStartPort = true;
		}
	}

	/**
	 * 
	 * 创建左编辑点
	 * 
	 * @return
	 */
	public static JecnEditPortPanel createLeftEditPortPanel(JecnBaseFigurePanel baseFigurePanel) {
		return new JecnEditPortPanel(EditPointType.left, baseFigurePanel);
	}

	/**
	 * 
	 * 创建右编辑点
	 * 
	 * @return
	 */
	public static JecnEditPortPanel createRightEditPortPanel(JecnBaseFigurePanel baseFigurePanel) {
		return new JecnEditPortPanel(EditPointType.right, baseFigurePanel);
	}

	/**
	 * 
	 * 创建上编辑点
	 * 
	 * @return
	 */
	public static JecnEditPortPanel createTopEditPortPanel(JecnBaseFigurePanel baseFigurePanel) {
		return new JecnEditPortPanel(EditPointType.top, baseFigurePanel);
	}

	/**
	 * 
	 * 创建下编辑点
	 * 
	 * @return
	 */
	public static JecnEditPortPanel createBottomEditPortPanel(JecnBaseFigurePanel baseFigurePanel) {
		return new JecnEditPortPanel(EditPointType.bottom, baseFigurePanel);
	}

	public static JecnEditPortPanel createPortPanelByEditPointType(JecnBaseFigurePanel baseFigurePanel,
			EditPointType editPointType) {
		switch (editPointType) {
		case top:
			return createTopEditPortPanel(baseFigurePanel);
		case bottom:
			return createBottomEditPortPanel(baseFigurePanel);
		case right:
			return createRightEditPortPanel(baseFigurePanel);
		case left:
			return createLeftEditPortPanel(baseFigurePanel);
		default:
			return null;
		}
	}

	/**
	 * 
	 * 添加连接线专用：鼠标进入此编辑点时样式
	 * 
	 */
	public void enterStyle() {
		// 设置编辑点位置和大小，背景色
		// this.setLocation(this.getX() - 2, this.getY() - 2);
		// this.setSize(14, 14);
		this.setBodyColor(Color.RED);
		this.isAllowLink = true;
		this.repaint();
	}

	/**
	 * 
	 * 添加连接线专用：鼠标退出此编辑点时样式
	 * 
	 */
	public void exitStyle() {
		// this.setLocation(this.getX() + 2, this.getY() + 2);
		// this.setSize(14, 14);
		this.setBodyColor(Color.GREEN);
		this.isAllowLink = false;
		this.repaint();
	}

	/**
	 * 
	 * 获取编辑点类型
	 * 
	 * @return EditPointType 编辑点类型
	 */
	public EditPointType getEditPointType() {
		return editPointType;
	}

	public JecnBaseFigurePanel getBaseFigurePanel() {
		return baseFigurePanel;
	}

	/**
	 * 获取编辑点原始状态下中心点位置
	 * 
	 * @return
	 */
	public Point getOriginalCenterPoint() {
		return baseFigurePanel.getJecnFigureDataCloneable().getSelectedOriginalEditPoint(editPointType);
	}

	/**
	 * 获取编辑点当前状态下中心点位置
	 * 
	 * @return
	 */
	public Point getZoomCenterPoint() {
		return baseFigurePanel.getJecnFigureDataCloneable().getSelectedEditPoint(editPointType,
				JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
	}

	public boolean isStartPort() {
		return isStartPort;
	}

	public boolean isAllowLink() {
		return isAllowLink;
	}

	// 内框 对应高和宽应该去的差值
	private static final int INNER_WIDTH = 7;
	// 内框元素起始坐标 INNER_LOCAL_XY
	private static final int INNER_LOCAL_XY = 3;

	@Override
	public void paintComponent(Graphics g) {
		// 画笔
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(bodyColor);
		g2d.setStroke(new BasicStroke(1));

		if (!isAllowLink) {
			g2d.fillRect(INNER_LOCAL_XY, INNER_LOCAL_XY, this.getWidth() - INNER_WIDTH, this.getHeight() - INNER_WIDTH);
			g2d.setColor(Color.BLACK);
			g2d.drawRect(INNER_LOCAL_XY, INNER_LOCAL_XY, this.getWidth() - INNER_WIDTH, this.getHeight() - INNER_WIDTH);
		} else {
			g2d.fillRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
		}
	}
}
