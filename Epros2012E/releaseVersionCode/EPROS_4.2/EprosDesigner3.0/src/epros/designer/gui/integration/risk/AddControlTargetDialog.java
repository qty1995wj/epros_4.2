package epros.designer.gui.integration.risk;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlTarget;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * 新建 控制目标
 * 2013-11-01
 *
 */
public class AddControlTargetDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddControlTargetDialog.class);
	/** 主面板*/
	private JecnPanel mainPanel = new JecnPanel(); //450,350
	
	/** 信息面板*/
	private JecnPanel infoPanel = new JecnPanel();//430,240
	
	/** 按钮面板*/
	private JecnPanel buttonPanel = new JecnPanel();//450,30
	
	/** 控制目标Lab*/
	private JLabel controlTargetLab = new JLabel(JecnProperties.getValue("controlTargetC"));
	
	/** 控制目标Area*/
	private JecnTextArea controlTargetArea = new JecnTextArea();//400,180
	/** 控制目标滚动面板 */
	private JScrollPane controlTargetScrollPane = new JScrollPane(
			controlTargetArea);

	
	/** 控制目标验证提示信息*/
	private JLabel controlTargetVerfy = new JLabel();
	
	/** 确定*/
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	
	/** 取消*/
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	
	private JecnTreeNode selectNode = null;
	/**控制目标主键ID*/
	private Long controlTargetId = null;
	private JecnControlTarget jecnControlTarget = null;
	
	public AddControlTargetDialog(JecnTreeNode selectNode){
		this.selectNode = selectNode;
		this.setTitle(JecnProperties.getValue("addBtn"));
		this.setModal(true);
		this.setSize(450,330);
		this.setLocationRelativeTo(null);
		
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 控制目标
		controlTargetArea.setBorder(null);
		controlTargetScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		controlTargetScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		controlTargetScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		//换行
		controlTargetArea.setLineWrap(true);
		controlTargetVerfy.setForeground(Color.red);
		
		//取消
		cancelBut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				cancelButPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				okButPerformed();
			}
		});
		
		initLayout();
		
	}
	/***
	 * 布局
	 */
	private void initLayout(){
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 8, 5, 8);
		//infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		//组织职责名称
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(controlTargetLab, c);
		//组织职责显示框
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(controlTargetScrollPane, c);
//		//组织职责验证提示信息
//		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0,
//				0);
//		infoPanel.add(controlTargetVerfy, c);
		//按钮
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(controlTargetVerfy);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		
		this.getContentPane().add(mainPanel);
	}
	/***
	 * 取消
	 */
	private void cancelButPerformed(){
//		if(check()){
			//关闭窗体
			this.dispose();
//		}
	}
	
	/**
	 * 确定
	 */
	private void okButPerformed(){
		controlTargetVerfy.setText("");
		String controlTargetDesc = this.controlTargetArea.getText();
		//控制目标长度验证
		if(controlTargetDesc!= null && !"".equals(controlTargetDesc)){
			if(DrawCommon.checkNoteLength(controlTargetDesc)){
				controlTargetVerfy.setText(JecnProperties.getValue("controlTarget") + JecnProperties.getValue("lengthNotOut"));
				return;
			}
		}else{
			controlTargetVerfy.setText(JecnProperties.getValue("conTargetNotNull"));
			return;
		}
		JecnControlTarget jecnControlTarget = new JecnControlTarget();
		//控制目标
		jecnControlTarget.setDescription(controlTargetDesc);
		jecnControlTarget.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnControlTarget.setCreateTime(new Date());
		jecnControlTarget.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnControlTarget.setUpdateTime(new Date());
		this.jecnControlTarget = jecnControlTarget;
		try {
			//向数据库中保存控制目标数据
//			this.jecnControlTarget=ConnectionPool.getJecnRiskAction().addJecnControlTarget(jecnControlTarget);
		} catch (Exception e) {
			log.error("AddControlTargetDialog okButPerformed is error",e);
		}
		this.dispose();
	}
	public JecnControlTarget getJecnControlTarget() {
		return jecnControlTarget;
	}
	public void setJecnControlTarget(JecnControlTarget jecnControlTarget) {
		this.jecnControlTarget = jecnControlTarget;
	}
	
}
