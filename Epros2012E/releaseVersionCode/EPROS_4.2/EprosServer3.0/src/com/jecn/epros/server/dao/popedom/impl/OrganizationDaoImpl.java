package com.jecn.epros.server.dao.popedom.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.popedom.IOrganizationDao;
import com.jecn.epros.server.util.JecnUtil;

public class OrganizationDaoImpl extends AbsBaseDao<JecnFlowOrg, Long> implements IOrganizationDao {

	@Override
	public List<Object[]> getAllPosByProjectId(Long projectId) throws Exception {
		// 岗位
		String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name"
				+ "        from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0"
				+ "        and jfo.projectid=? order by t.org_id,t.sort_id";
		return this.listNativeSql(sql, projectId);
	}

	@Override
	public List<Object[]> getAllOrgByProjectId(Long projectId) throws Exception {
		String sql = "select t.org_id,t.org_name,t.per_org_id,"
				+ "       (select count(*) from jecn_flow_org where per_org_id=t.org_id and del_state=0) as org_count,"
				+ "       (select count(*) from jecn_flow_org_image where org_id=t.org_id and figure_type='PositionFigure') as pos_count"
				+ "       from jecn_flow_org t where t.projectid=?  and del_state = 0 order by t.per_org_id,t.sort_id,t.org_id";
		return this.listNativeSql(sql, projectId);
	}

	@Override
	public List<Object[]> getPosByOrgId(Long orgId, Long projectId) throws Exception {
		String sql = "";
		// 29 所
		if (JecnContants.otherLoginType == 11) {
			sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name,t.FIGURE_NUMBER_ID"
					+ "        from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0"
					+ "        and t.org_id=? and jfo.projectid=? and t.figure_type = " + JecnCommonSql.getPosString()
					+ "		   and t.figure_text <> '" + JecnUtil.getValue("undetermined") + "'"
					+ " order by t.org_id,t.sort_id";
		} else {
			sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name,t.FIGURE_NUMBER_ID"
					+ "        from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0"
					+ "        and t.org_id=? and jfo.projectid=? and t.figure_type = " + JecnCommonSql.getPosString()
					+ " order by t.org_id,t.sort_id";
		}
		return this.listNativeSql(sql, orgId, projectId);

	}

	public List<Object[]> getPosAndPersonByOrgId(Long orgId, Long projectId) throws Exception {
		String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name,t.FIGURE_NUMBER_ID"
				+ ",(select count(*) from jecn_user_position_related where figure_id=t.figure_id) as count"
				+ "        from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0"
				+ "        and t.org_id=? and jfo.projectid=? and t.figure_type = " + JecnCommonSql.getPosString()
				+ " order by t.org_id,t.sort_id";
		return this.listNativeSql(sql, orgId, projectId);
	}

	@Override
	public List<Object[]> getSearchOrgIds(Long orgId, Long projectId) throws Exception {
		JecnFlowOrg jecnFlowOrg = this.get(orgId);
		if (jecnFlowOrg == null) {
			return new ArrayList<Object[]>();
		}
		Set<Long> setIds = JecnCommon.getPositionTreeIds(jecnFlowOrg.gettPath(), orgId);
		String sql = "Select DISTINCT JF.ORG_ID,JF.ORG_NAME,JF.PER_ORG_ID"
				+ "  ,CASE WHEN SUB.ORG_ID IS NULL THEN 0 ELSE 1 END AS count_org"
				+ "  ,CASE WHEN JFOM.FIGURE_ID IS NULL THEN 0 ELSE 1 END  AS COUNT_STATION,JF.SORT_ID,JF.T_PATH"
				+ "  FROM JECN_FLOW_ORG JF"
				+ "  LEFT JOIN JECN_FLOW_ORG SUB on SUB.PER_ORG_ID = JF.ORG_ID AND SUB.DEL_STATE=0"
				+ "  LEFT JOIN Jecn_Flow_Org_Image JFOM ON JFOM.FIGURE_TYPE='PositionFigure' AND JFOM.ORG_ID=JF.ORG_ID"
				+ " where JF.DEL_STATE=0 AND JF.Projectid=? AND JF.PER_ORG_ID in "+ JecnCommonSql.getIdsSet(setIds) 
				+ "  ORDER BY JF.PER_ORG_ID,JF.SORT_ID,JF.ORG_ID";
		return this.listNativeSql(sql, projectId);
	}

	@Override
	public void deleteOrgs(Set<Long> setOrgIds) throws Exception {
		// 岗位与人员的关系表
		String hql = "delete from JecnUserInfo where figureId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		List<String> list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 岗位与流程中角色的关系-浏览端
		hql = "delete from JecnFlowStation where  type = '0' and figurePositionId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 岗位与流程中角色的关系 设计器
		hql = "delete from JecnFlowStationT where  type = '0' and figurePositionId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 岗位与岗位组的关系表
		hql = "delete from JecnPositionGroupOrgRelated where figureId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 岗位的基本信息表
		hql = "delete from JecnPositionFigInfo where figureId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 岗位与制度的关系表
		hql = "delete from JecnPositionRefSystem where positionId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 上级岗位 、下属岗位
		hql = "delete from PositionUpperLowerBean where positionId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程责任人-岗位-设计器
		hql = "update JecnFlowBasicInfoT set resPeopleId = -1 where typeResPeople =1 and  resPeopleId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程责任人-岗位-浏览端
		hql = "update JecnFlowBasicInfo set resPeopleId = -1 where typeResPeople =1 and  resPeopleId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}

		// 查阅权限中岗位
		hql = "delete from JecnAccessPermissions where figureId in (select figureId from JecnFlowOrgImage where figureType="
				+ JecnCommonSql.getPosString() + " and orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 连接线
		hql = "delete from JecnLineSegmentDep where figureId in ( select figureId from JecnFlowOrgImage where orgId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 组织图标
		hql = "delete from JecnFlowOrgImage where orgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 更新组织关联图标
		hql = "update JecnFlowOrgImage  set linkOrgId=-1 where figureType=" + JecnCommonSql.getOrgString()
				+ " and linkOrgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程的责任部门 浏览端
		hql = "delete from FlowOrg where orgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程的责任部门 设计器
		hql = "delete from FlowOrgT where orgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 制度的责任部门-浏览端
		hql = "update Rule set orgId =-1 where orgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 制度的责任部门-设计器
		hql = "update RuleT set orgId =-1 where orgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 文件的责任部门
		hql = "update JecnFileBean set orgId =-1 where orgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 查阅部门
		hql = "delete from JecnOrgAccessPermissions where orgId in ";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除组织对象
		hql = "delete from JecnFlowOrg where orgId in";
		list = JecnCommonSql.getSetSqls(hql, setOrgIds);
		for (String str : list) {
			execteHql(str);
		}
	}

	@Override
	public void deleteOrgs(Long projectId) throws Exception {
		String hql = "select orgId from JecnFlowOrg where projectId=?";
		List<Long> list = this.listHql(hql, projectId);
		Set<Long> setOrgIds = new HashSet<Long>();
		for (Long id : list) {
			if (id != null) {
				setOrgIds.add(id);
			}
		}
		if (setOrgIds.size() > 0) {
			this.deleteOrgs(setOrgIds);
		}
	}

	@Override
	public List<Object[]> findResFlowsByOrgIdAndProjectId(Set<Long> setOrgIds, Long projectId) throws Exception {
		String sql = "select t.flow_id,t.flow_name,t.res_people_id,jf.create_date,jf.update_date,jf.flow_id_input,"
				+ "    t.Type_Res_People,jfo.org_name,"
				+ "	   case when t.type_res_people=0 then ju.true_name"
				+ "    when t.type_res_people=1 then pos.figure_text end as flow_res_name,t.expiry,jfo.ORG_RESPON,jfo.org_id"
				+ "    from jecn_flow_basic_info t"
				+ "    left join jecn_user ju on ju.people_id=t.RES_PEOPLE_ID"
				+ "    left join (select jfoi.figure_id,jfoi.figure_text from Jecn_Flow_Org_Image jfoi,jecn_flow_org jfo_t where jfoi.org_id=jfo_t.org_id and jfo_t.del_state=0 and jfo_t.projectid="
				+ projectId
				+ ") pos on pos.figure_id=t.RES_PEOPLE_ID"
				+ "    ,Jecn_Flow_Structure jf,jecn_flow_org jfo,Jecn_Flow_Related_Org jfro"
				+ "    where t.flow_id = jf.flow_id and jf.isflow=1 and jf.projectid=?"
				+ "    and jf.del_state=0"
				+ "    and t.flow_id = jfro.flow_id and jfro.org_id = jfo.org_id and jfo.del_state=0 and jfo.projectid="
				+ projectId + "    and jfo.org_id in";
		List<String> listStr = JecnCommonSql.getSetSqls(sql, setOrgIds);
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (String str : listStr) {
			List<Object[]> listObj = this.listNativeSql(str, projectId);
			for (Object[] obj : listObj) {
				if (obj != null) {
					listResult.add(obj);
				}
			}
		}
		return listResult;
	}

	@Override
	public List<Object[]> findKeyActivesByOrgIdAndProjectId(Set<Long> setOrgIds, Long projectId) throws Exception {
		String sql = "select jfsi.figure_id,jfsi.flow_id,jfsi.figure_text,jfsi.linecolor,jfsi.role_res from jecn_flow_structure_image jfsi where jfsi.flow_id in (select t.flow_id"
				+ "    from jecn_flow_basic_info t,Jecn_Flow_Structure jf,jecn_flow_org jfo,Jecn_Flow_Related_Org jfro"
				+ "    where t.flow_id = jf.flow_id and jf.isflow=1 and jf.projectid="
				+ projectId
				+ " and jf.del_state=0"
				+ " and t.flow_id = jfro.flow_id and jfro.org_id = jfo.org_id and jfo.del_state=0 and jfo.projectid="
				+ projectId
				+ " and jfsi.linecolor in ('1','2','3') and jfsi.figure_type in"
				+ JecnCommonSql.getActiveString() + " and jfo.org_id in";
		List<String> listStr = JecnCommonSql.getSetSqlAddBracket(sql, setOrgIds);
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (String str : listStr) {
			List<Object[]> listObj = this.listNativeSql(str);
			for (Object[] obj : listObj) {
				if (obj != null) {
					listResult.add(obj);
				}
			}
		}
		return listResult;
	}

	@Override
	public List<Object[]> findKPIByOrgIdAndProjectId(Set<Long> setOrgIds, Long projectId) throws Exception {
		String sql = "select jfsi.KPI_AND_ID,jfsi.flow_id,jfsi.KPI_NAME from JECN_FLOW_KPI_NAME jfsi where jfsi.flow_id in (select t.flow_id"
				+ "    from jecn_flow_basic_info t,Jecn_Flow_Structure jf,jecn_flow_org jfo,Jecn_Flow_Related_Org jfro"
				+ "    where t.flow_id = jf.flow_id and jf.isflow=1 and jf.projectid="
				+ projectId
				+ " and jf.del_state=0"
				+ "    and t.flow_id = jfro.flow_id and jfro.org_id = jfo.org_id and jfo.del_state=0 and jfo.projectid="
				+ projectId + "    and jfo.org_id in";
		List<String> listStr = JecnCommonSql.getSetSqlAddBracket(sql, setOrgIds);
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (String str : listStr) {
			List<Object[]> listObj = this.listNativeSql(str);
			for (Object[] obj : listObj) {
				if (obj != null) {
					listResult.add(obj);
				}
			}
		}
		return listResult;
	}

	@Override
	public List<JecnTreeBean> listFlowFileDownloadPosPerms(Long id) throws Exception {
	
		String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name"
				+ " from jecn_flow_org_image t,jecn_flow_org jfo,JECN_FLOW_FILE_POS_PERM jap where t.org_id=jfo.org_id and jfo.del_state=0"
				+ " and t.figure_id = jap.figure_id and jap.relate_id=?"
				+ " order by t.org_id,t.sort_id";
		List<Object[]> listOrg = listNativeSql(sql, id);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listOrg) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}
	
	public List<Object[]> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception {
		String sql = "select t.org_id,t.org_name,t.per_org_id,(select count(*) from jecn_flow_org where del_state=0 and per_org_id=t.org_id ) as count"
				+ " ,t.ORG_NUMBER"
				+ " from jecn_flow_org t ";
				sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(5, peopleId, projectId);
				sql = sql + " where t.per_org_id=? and t.projectid=? and t.del_state=0 order by t.per_org_id,t.sort_id,t.org_id";
		return this.listNativeSql(sql, pId, projectId);
	}

	/**
	 * 组织权限下人员选择对应组织
	 */
	@Override
	public List<Object[]> getRoleAuthChildPersonOrgs(Long pId, Long projectId, Long peopleId) {
		String sql = "select distinct  t.org_id, t.org_name, t.per_org_id,"
			+ "case when  org2.org_id is null then 0 else 1 end as org_count ,"
			+ "case when  image.figure_id  is null then 0 else 1 end  as  pos_count,t.sort_id,"
			+ "  t.org_number" + "  from jecn_flow_org t"
			+ "  left join jecn_flow_org org2 on t.org_id = org2.per_org_id" + "  and org2.del_state = 0"
			+ "  left join jecn_flow_org_image image on t.org_id = image.org_id"
			+ "  and image.figure_type = 'PositionFigure'" ;
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(5, peopleId, projectId);
		sql = sql + "  where t.per_org_id = ?"
			+ "  and t.projectid = ?" + "  and t.del_state = 0"
			+ " order by t.per_org_id, t.sort_id, t.org_id";
		return this.listNativeSql(sql, pId, projectId);
	}
	
	/**
	 * 组织权限下的岗位
	 * @param orgId
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getRoleAuthPosAndPersonByOrgId(Long orgId, Long projectId, Long peopleId) throws Exception {
		String sql = "select oi.figure_id,oi.figure_text,oi.org_id,t.org_name,oi.FIGURE_NUMBER_ID"
				+ ",(select count(*) from jecn_user_position_related where figure_id=oi.figure_id) as count"
				+ "        from jecn_flow_org_image oi,jecn_flow_org t ";
				sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(5, peopleId, projectId);
				sql = sql + "where oi.org_id=t.org_id and t.del_state=0"
				+ "        and t.org_id=? and t.projectid=? and oi.figure_type = " + JecnCommonSql.getPosString()
				+ " order by oi.org_id,oi.sort_id";
		return this.listNativeSql(sql, orgId, projectId);
	}
	
	@Override
	public List<Object[]> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		String sql = "select t.org_id,t.org_name,t.per_org_id,(select count(*) from jecn_flow_org where per_org_id=t.org_id ) as count"
			+ " from jecn_flow_org t " ;
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(5, peopleId, projectId);
		sql = sql +" where t.org_name like ? and t.projectid=? and t.del_state=0"
			+ " order by t.per_org_id,t.sort_id,t.org_id";
		return this.listNativeSql(sql, 0, 20, "%" + name + "%", projectId);
	}
}
