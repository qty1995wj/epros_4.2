package com.jecn.webservice.nine999;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sun.star.bridge.oleautomation.Date;

public class Nine999TaskWebServiceClient {

	private static Logger log = Logger.getLogger(Nine999TaskWebServiceClient.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		getDeptString();
//		Map<String,String> m = new HashMap<String,String>();
//		m.put("createTime","2018-09-27 15:25:03");
//		m.put("link", "http://10.145.98.108:8080/static-web/#/dashboard");
//		m.put("id", "2");
//		m.put("creator", "chenyiming");
//		m.put("subject", "测试");
//		m.put("target", "chenyiming");
////		sendTodo(m);
//		deleteTodo(m);
		
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		String time = format1.format(System.currentTimeMillis());
		System.out.println(time);

	}

	public static void sendTodo(Map<String,String> param){
		String s = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.notify.sys.kmss.landray.com/\">\n" +
		        "   <soapenv:Header/>\n" +
		        "   <soapenv:Body>\n" +
		        "      <web:sendTodo>\n" +
		        "         <arg0>\n" +
		        "            <appName>Epros</appName>\n" +
		        "            <createTime>"+param.get("createTime")+"</createTime>\n" +
		        "            <docCreator>{\"LoginName\":\""+param.get("creator")+"\"}</docCreator>\n" +
		        "            <extendContent></extendContent>\n" +
		        "            <key></key>\n" +
		        "            <level></level>\n" +
		        "            <link>"+param.get("link")+"</link>\n" +
		        "            <modelId>"+param.get("id")+"</modelId>\n" +
		        "            <modelName>Epros</modelName>\n" +
		        "            <others></others>\n" +
		        "            <param1></param1>\n" +
		        "            <param2></param2>\n" +
		        "            <subject>"+param.get("subject")+"</subject>\n" +
		        "            <targets>{\"LoginName\":\""+param.get("target")+"\"}</targets>\n" +
		        "            <type>1</type>\n" +
		        "            <weixinflag></weixinflag>\n" +
		        "            <weixintype></weixintype>\n" +
		        "         </arg0>\n" +
		        "      </web:sendTodo>\n" +
		        "   </soapenv:Body>\n" +
		        "</soapenv:Envelope>";
		String out = sendReq(s);
	}
	
	public static void setTodoDone(){
		
	}
	
	public static void deleteTodo(Map<String,String> param){
		String s = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.notify.sys.kmss.landray.com/\">\n" +
		        "   <soapenv:Header/>\n" +
		        "   <soapenv:Body>\n" +
		        "      <web:deleteTodo>\n" +
		        "         <arg0>\n" +
		        "            <appName>Epros</appName>\n" +
		        "            <key></key>\n" +
		        "            <modelId>"+param.get("id")+"</modelId>\n" +
		        "            <modelName>Epros</modelName>\n" +
		        "            <optType>1</optType>\n" +
		        "            <param1></param1>\n" +
		        "            <param2></param2>\n" +
		        "            <targets>{\"LoginName\":\""+param.get("target")+"\"}</targets>\n" +
		        "         </arg0>\n" +
		        "      </web:deleteTodo>\n" +
		        "   </soapenv:Body>\n" +
		        "</soapenv:Envelope>";
		String out = sendReq(s);
	}

	public static String sendReq(String paraXml) { // type: dept post person
		String beginTime = "";
		String urlStr = "http://km.999.com.cn/sys/webservice/sysNotifyTodoWebService";
		// System.out.println(paraXml);
		OutputStream out = null;
		String messageText = null;
		String respData = "";
		int count = 0;
		URL url;
		try {
			url = new URL(urlStr);

			HttpURLConnection con;
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestMethod("POST");
			con.setUseCaches(false);
			con.setConnectTimeout(1200000);
			con.setReadTimeout(1200000);
			con.setRequestProperty("Content-type", "text/xml; charset=UTF-8");
			// con.setRequestProperty("WSS-Password Type", "PasswordText");

			// con.setRequestProperty("SOAPAction", soapAction);
			// con.setRequestProperty("Encoding", "UTF-8");
			out = con.getOutputStream();
			System.out.println(paraXml);
			con.getOutputStream().write(paraXml.getBytes());
			out.flush();
			out.close();
			int code = con.getResponseCode();
			String tempString = null;
			StringBuffer sb1 = new StringBuffer();
			if (code == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				while ((tempString = reader.readLine()) != null) {
					sb1.append(tempString);
				}
				if (null != reader) {
					reader.close();
				}
			} else {
				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getErrorStream(), "UTF-8"));
				// 一次读入一行，直到读入null为文件结束
				while ((tempString = reader.readLine()) != null) {
					sb1.append(tempString);
				}
				if (null != reader) {
					reader.close();
				}
			}
			// 响应报文
			// String respData =
			// StringEscapeUtils.unescapeHtml4(sb1.toString());
			respData = sb1.toString();
			System.out.println(respData);
//			// 通过read方法读取一个文件 转换成Document对象
//			Document document = DocumentHelper.parseText(respData);
//			// 获取根节点元素对象
//			Element node = document.getRootElement();
//			// 遍历所有的元素节点
//			// listNodes(node);
//			DefaultXPath xpath = new DefaultXPath("//ns1:getUpdatedElementsResponse");
//			xpath.setNamespaceURIs(Collections.singletonMap("ns1",
//					"http://webservice.notify.sys.kmss.landray.com/"));
//
//			Element ns = (Element) node.selectNodes("/soap:Envelope/soap:Body").get(0);
//			Element re = (Element) xpath.selectNodes(ns).get(0);
//			Element returnEle = re.element("return");
//			Element message = returnEle.element("message");
//			count = Integer.parseInt(returnEle.element("count").getText());
//			beginTime = returnEle.element("timeStamp").getText();
//			messageText = message.getText();
			// String a = node.element("message").getText();
			// return messageText;
		} catch (MalformedURLException e) {
			log.error("链接出错");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("请求出错");
			e.printStackTrace();
		}
		return respData;
	}

	public static String getXml(String paramString) {
		StringBuffer template = new StringBuffer();
		template.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.notify.sys.kmss.landray.com/\">   <soapenv:Header/>   <soapenv:Body>");
		template.append(paramString);
		template.append("   </soapenv:Body></soapenv:Envelope>");
		return new String(template);
	}
	
}
