package wordxml.element.dom;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.NFC;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;

public class WordDocument extends IBody {
	protected final Log log = LogFactory.getLog(WordDocument.class);
	private Document doc;
	private Element body;
	private Element docPr;
	// 标题LVL节点
	private Element Lvl_1Ele;// 
	// 标题 style节点
	private Element Lvl_1EleStyle;// 

	// 二级标题LVL节点
	private Element Lvl_2Ele;
	// 二级标题 style节点
	private Element Lvl_2EleStyle;//
	//一级标题手动编号
	private boolean lvl_1_HandNumber;//
	// 一级标题编号格式(代表序号和文字之间的字符) 例如：1.目的 中的.
	private String chart;
	// 设置编号从多少开始
	private int startNum = -1;
	// NFC 标题序号转换
	private NFC nfc;
	// 标题段落属性Bean
	private ParagraphBean titlePBean;

	// 文档中的所有节元素
	protected List<Sect> sections = new ArrayList<Sect>();

	public List<Sect> getSections() {
		return Collections.unmodifiableList(sections);
	}

	/**
	 * 
	 * @param doc
	 */
	public WordDocument(Document doc) {
		this.doc = doc;
		body = doc.getRootElement().element("body");
		docPr = doc.getRootElement().element("docPr");
		// 初始化标题需要的节点
		initStyleAndLvlElement();

	}

	/**
	 * 根据模版初始化标题中的样式节点和 编号格式节点
	 */
	@SuppressWarnings("unchecked")
	private void initStyleAndLvlElement() {
		List<Element> listStyle = doc.getRootElement().element("styles")
				.elements("style");
		List<Element> listDef = doc.getRootElement().element("lists").elements(
				"listDef");
		// styles表示样式节点 其中style 节点中属性 styleId=1 代表一级标题样式
		for (Element ele : listStyle) {
			// 标题style
			if ("paragraph".equals(ele.attributeValue("type"))
					&& "1".equals(ele.attributeValue("styleId"))) {
				Lvl_1EleStyle = ele;
			}
			// 二级标题style
			else if ("paragraph".equals(ele.attributeValue("type"))
					&& "2".equals(ele.attributeValue("styleId"))) {
				Lvl_2EleStyle = ele;
			}

		}
		// 模版中lists 节点中的listDef节点 其实属性listDefId=0 是一级标题 listDefId=2二级标题
		for (Element ele : listDef) {
			// 标题
			if ("0".equals(ele.attributeValue("listDefId"))) {
				List<Element> listLvl = ele.elements("lvl");
				for (Element lvl : listLvl) {
					if (lvl.element("pStyle") != null) {
						Lvl_1Ele = lvl;
					}
				}
			}
			// 二级标题
			else if ("2".equals(ele.attributeValue("listDefId"))) {
				List<Element> listLvl = ele.elements("lvl");
				for (Element lvl : listLvl) {
					if (lvl.element("pStyle") != null) {
						Lvl_2Ele = lvl;
					}
				}
			}
		}
	}

	/**
	 * 生成XML
	 * 
	 * @author weidp
	 * @date 2014-10-15 上午09:41:31
	 * @param path
	 * @throws IOException
	 */
	public final void write(String path) {
		XMLWriter writer = null;
		try {
			OutputFormat format = OutputFormat.createPrettyPrint();
			format.setEncoding("utf-8");
			format.setTrimText(false);
			writer = new XMLWriter(new FileOutputStream(path), format);
			buildDocumentToXML();
			writer.write(this.doc);
			writer.flush();
		} catch (IOException e) {
			log.error("生成操作说明文件异常", e);
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				log.error("操作说明文件_关闭流异常", e);
			}
		}

	}

	/**
	 * 实例化为XML
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午02:57:14
	 * @param bodys
	 * @param target
	 */
	private void buildDocumentToXML() {
		// 存在节的情况下设置最后一节的属性为
		Sect lastSect = null;
		for (int i = sections.size() - 1; i >= 0; i--) {
			if (sections.get(i).bodyElements.size() != 0) {
				lastSect = sections.get(i);
				lastSect.setLast(true);
				break;
			}
		}

		boolean hasOddEven = false;
		// 判断是否存在奇偶页
		for (int i = 0; i < sections.size() && !hasOddEven; i++) {
			Sect sect = sections.get(i);
			for (Header hdr : sect.getHeaders()) {
				if (hdr.getType().equals(HeaderOrFooterType.even)) {
					hasOddEven = true;
					break;
				}
			}
			for (Footer ftr : sect.getFooters()) {
				if (ftr.getType().equals(HeaderOrFooterType.even)) {
					hasOddEven = true;
					break;
				}
			}
		}
		if (hasOddEven) {
			docPr.addElement("w:evenAndOddHeaders");
		}
		// 初始化一级标题样式
		initTitleAttr();
		// 特殊处理 添加书签，总页码引用书签(在最后一节末尾添加一个空的段落解决总页码问题)
		lastSect.getParagraphs().get(lastSect.getParagraphs().size() - 1)
				.getParent().addParagraph(new Paragraph(""));
		lastSect.getParagraphs().get(lastSect.getParagraphs().size() - 1)
				.setBookMark("EndPage");
		// 开始实例化body的子节点
		for (IBodyElement e : bodyElements) {
			WordElementFactory.produceElement(e, body);
		}

	}
	/**
	 * 初始化标题样式
	 * 
	 * @param lvlStyle
	 * @param lvlFormart
	 *            //
	 */
	private void initTitleAttr() {
		// 一级标题
		if (titlePBean == null) {
			throw new RuntimeException("titlePBean 为空");
		}
		SpecialStyle.setSpaceAndInd(Lvl_1EleStyle.element("pPr"), titlePBean);
		SpecialStyle.setFontAttr(Lvl_1EleStyle.element("rPr"), titlePBean
				.getFontBean());
		// 一级标题序号节点
		Element lvlText = Lvl_1Ele.element("lvlText");
		lvlText.remove(lvlText.attribute("val"));
		if(!lvl_1_HandNumber){
			lvlText.addAttribute("w:val", "%1" + chart);
			if (nfc != null) {
				Lvl_1Ele.addElement("w:nfc").addAttribute("w:val", nfc.getValue());
			}
			// 设置编号从多少开始
			if (startNum != -1) {
				Element startEle = Lvl_1Ele.element("start");
				startEle.remove(startEle.attribute("val"));
				startEle.addAttribute("w:val", startNum + "");
			}
		} else {
			lvlText.addAttribute("w:val", "");
		}
		// 二级标题
		SpecialStyle.setSpaceAndInd(Lvl_2EleStyle.element("pPr"), titlePBean);
		SpecialStyle.setFontAttr(Lvl_2EleStyle.element("rPr"), titlePBean
				.getFontBean());
		// 二级标题序号节点
		Element lvlText2 = Lvl_2Ele.element("lvlText");
		lvlText2.remove(lvlText2.attribute("val"));
		lvlText2.addAttribute("w:val", "");
		// lvlText2.addAttribute("w:val", chart + "%1");
		if (nfc != null) {
			Lvl_2Ele.addElement("w:nfc").addAttribute("w:val", nfc.getValue());
		}

	}

	/**
	 * 创建节对象
	 * 
	 * @author weidp
	 * @date 2014-10-16 下午04:19:15
	 * @return
	 */
	public Sect createSect() {
		Sect sect = new Sect(this);
		sections.add(sect);
		this.bodyElements.add(sect);
		return sect;
	}

	/**
	 * 创建节对象
	 * 
	 * @author weidp
	 * @date 2014-10-16 下午04:19:15
	 * @return
	 */
	public Sect createSect(SectBean sectBean) {
		Sect sect = new Sect(this, sectBean);
		sections.add(sect);
		this.bodyElements.add(sect);
		return sect;
	}

	public Element getBody() {
		return this.body;
	}

	/**
	 * 美国信息互换标准代码 (包括英文 数字 特殊符号)
	 */
	public void setAscii(String font) {
		doc.getRootElement().element("fonts").element("defaultFonts")
				.attribute("ascii").setValue(font);
	}

	/**
	 * 设置东亚默认字体(中文)
	 */
	public void setFareast(String font) {
		doc.getRootElement().element("fonts").element("defaultFonts")
				.attribute("fareast").setValue(font);
	}

	/**
	 * 设置转换的序号的NFC
	 * 
	 * @param nfc
	 */
	public void setNfc(NFC nfc) {
		this.nfc = nfc;
	}

	/**
	 * 设置标题样式
	 * 
	 * @param chart
	 *            序号和标题之间的字符
	 * @param titlePBean
	 *            标题样式bean
	 */
	public void setStyle(String chart, ParagraphBean titlePBean) {
		this.titlePBean = titlePBean;
		this.chart = chart;
	}

	/**
	 * 设置编号从多少开始
	 * 
	 * @param startNum
	 */
	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}

	/**
	 * 设置一级标题手动编号
	 * @param handNumber
	 */
	public void setHandNumber() {
		this.lvl_1_HandNumber = true;
	}
}
