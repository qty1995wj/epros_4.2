package com.jecn.epros.server.bean.system;

import java.util.Locale;

public class ProceeRuleTypeBean implements java.io.Serializable {
	private Long typeId;// 主键ID
	private String typeName;// 类型名称
	private String enName;// 英文类型名称

	public ProceeRuleTypeBean() {

	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public String getTypeName(int type) {
		return type == 0 ? typeName : enName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}
