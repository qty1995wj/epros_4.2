/*      Copyright (c) 2012 JECN Corporation                     */
/*      JECN CONFIDENTIAL AND PROPRIETARY                       */
/*      All rights reserved by JECN Corporation.                */
/*      This program must be used solely for the purpose for    */
/*      which it was furnished by JECN Corporation.   No part   */
/*      of this program may be reproduced  or  disclosed  to    */
/*      others,  in  any form,  without  the  prior  written    */
/*      permission of JECN Corporation. Use  of  copyright      */
/*      notice does not evidence publication of the program.    */
package epros.draw.unredo.data;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.JecnBaseVHLinePanel;

/**
 * 
 * 横竖分割线属性备份类，只备份横竖分割线重要的属性
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUnRedoVHLineData {
	/** 备份的横竖分割线数据对象 */
	private JecnVHLineData vhLineDataClone = null;
	/** 横竖分割线对象 */
	private JecnBaseVHLinePanel vhLines = null;

	public JecnUnRedoVHLineData(JecnBaseVHLinePanel vhLines) {
		if (vhLines == null) {
			JecnLogConstants.log_JecnUnRedoVHLineData
					.error("JecnUnRedoVHLineData(JecnBaseVHLinePanel vhLines)异常：参数为空");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.vhLineDataClone = vhLines.getFlowElementData().clone();
		this.vhLines = vhLines;
	}

	public JecnVHLineData getVhLineDataClone() {
		return vhLineDataClone;
	}

	public JecnBaseVHLinePanel getVhLines() {
		return vhLines;
	}

	/**
	 * 
	 * 获取横竖分割线的数据对象
	 * 
	 * @return JecnVHLineData 横竖分割线的数据对象
	 */
	public JecnVHLineData getVHLineData() {
		return this.vhLines.getFlowElementData();
	}
}
