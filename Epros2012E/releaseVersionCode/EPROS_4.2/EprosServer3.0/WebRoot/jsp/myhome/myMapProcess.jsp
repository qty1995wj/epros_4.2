<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="attentionMapProcesssearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td height="25px" width="10%" align="right">
								<!-- 流程地图名称： -->
								<s:text name="processMapNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;"
									id="attentionMapProcessName_id" maxLength="32" />
							</td>


							<td width="10%" align="right">
								<!-- 流程地图编号： -->
								<s:text name="processMapNumberC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;"
									id="attentionMapProcessNumber_id" maxLength="32" />

							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a
									onClick="toggleDiv(attentionMapProcesssearchText,'mproPackUp','mproSearchVal')"
									style="cursor: pointer;" class="packUp"> <img
										id="mproPackUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="mproSearchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td align="right" height="25px">
								<!-- 密集: -->
								<s:text name="intensiveC" />
							</td>
							<td align="left">
								<select style="width: 80%;" id="attentionMapProcess_intensive">
								    <option value="0" selected="selected">
										${secret}
									</option>
									<option value="1">
										<s:property value="#application.public"/>
									</option>
									<option value="-1">
										<s:text name="all" />
									</option>
									
								</select>
							</td>
							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="attentionMapProcessSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="attentionMapProcessReset()" />
				</div>
			</div>

			<div id="atentionMapProcessGrid_id">

			</div>
		</div>
	</body>
</html>