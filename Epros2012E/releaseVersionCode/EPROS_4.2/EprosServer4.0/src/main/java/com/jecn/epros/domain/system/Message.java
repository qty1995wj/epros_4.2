package com.jecn.epros.domain.system;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 信息表
 *
 * @author Administrator
 */
public class Message implements java.io.Serializable {
    private Long messageId;// 主键ID
    private Long peopleId; // peopleId 发件人
    private String messageContent;// 信息内容
    private String messageTopic;// 信息标题
    private Long noRead;// 0是新信息，1是已读信息
    private Long inceptPeopleId;// 收件人 userID
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date createTime;// 创建时间
    private String peopleTime;// 发送人/时间
    private String peopleName;// 发送人名称
    /**
     * 類型:1:任务、2:发布、3:建议
     */
    private int messageType;

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Long peopleId) {
        this.peopleId = peopleId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageTopic() {
        return messageTopic;
    }

    public void setMessageTopic(String messageTopic) {
        this.messageTopic = messageTopic;
    }

    public Long getNoRead() {
        return noRead;
    }

    public void setNoRead(Long noRead) {
        this.noRead = noRead;
    }

    public Long getInceptPeopleId() {
        return inceptPeopleId;
    }

    public void setInceptPeopleId(Long inceptPeopleId) {
        this.inceptPeopleId = inceptPeopleId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPeopleTime() {
        return peopleTime;
    }

    public void setPeopleTime(String peopleTime) {
        this.peopleTime = peopleTime;
    }

    public String getPeopleName() {
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

}
