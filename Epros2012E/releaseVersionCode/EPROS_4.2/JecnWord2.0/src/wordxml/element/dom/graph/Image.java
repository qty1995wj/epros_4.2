package wordxml.element.dom.graph;

import java.util.UUID;

import wordxml.element.bean.common.PositionBean;

/**
 * 操作图片节点类
 */
public class Image extends WordGraph {
	/** 图片文件路径 */
	private String fullFilePath;
	/** 图片名称 **/
	private final String name;
	/** 图片宽度单位 厘米 **/
	private float width = 0;
	/** 图片高度 单位 厘米 **/
	private float height = 0;
	/** 图片旋转值 默认不旋转 **/
	private int rotation = 0;
	/**** 图片位置属性bean ************/
	private PositionBean positionBean;

	public Image(String imgFile) {
		this.name = "wordml://" + UUID.randomUUID() + ".jpg";
		this.fullFilePath = imgFile;
	}

	public String getName() {
		return name;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public void setSize(float width, float height) {
		this.width = width;
		this.height = height;
	}

	public int getRotation() {
		return rotation;
	}

	public void setRotation(int rotation) {
		this.rotation = rotation;
	}

	public PositionBean getPositionBean() {
		return positionBean;
	}

	public void setPositionBean(PositionBean positionBean) {
		this.positionBean = positionBean;
	}

	public String getImgFile() {
		return fullFilePath;
	}

	public void setImgFile(String imgFile) {
		this.fullFilePath = imgFile;
	}

}
