package epros.draw.data.line;

import java.awt.Dimension;
import java.awt.Point;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnXYData;
import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 最小线段对应的数据类，次类要正确操作要满足： 线条两端坐标中x或者y至少有一个要相等 即x1==x2或y1==y2
 * 
 * @author ZHOUXY
 * 
 */

public class JecnLineSegmentDataCloneable implements Cloneable {
	/** 线开始点坐标 */
	private JecnXYData startXY = new JecnXYData();
	/** 线结束点坐标 */
	private JecnXYData endXY = new JecnXYData();
	/** 线段方向:正常情况是水平和垂直 */
	private LineDirectionEnum lineDirectionEnum = LineDirectionEnum.error;
	/** 线宽度 */
	private double width = 1.0D;
	/** 线高度 */
	private double height = 1.0D;
	/** 线条宽 */
	private int lineOriginaWidth = 0;

	/**
	 * 
	 * 构造函数
	 * 
	 * @param startPoint
	 *            线条一端
	 * @param endPoint
	 *            线条另外一端
	 * @param lineWidth
	 *            int 线条宽
	 * @param multiple
	 *            放大缩小倍数
	 */
	public JecnLineSegmentDataCloneable(Point startPoint, Point endPoint,
			int lineWidth, double multiple) {
		if (startPoint == null
				|| endPoint == null
				|| (startPoint.getX() != endPoint.getX() && startPoint.getY() != endPoint
						.getY())) {
			JecnLogConstants.LOG_LINE_SEGMENT_DATA_PROCESS
					.error("JecnLineSegmentDataCloneable类构造函数：有不合理参数.startPoint="
							+ startPoint + ";endPoint=" + endPoint
							+ ";lineWidth=" + lineWidth + ",multiple="
							+ multiple);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		// 初始化坐标点和大小
		reSetAttributes(startPoint, endPoint, lineWidth, multiple);
	}

	public JecnLineSegmentDataCloneable() {

	}

	/**
	 * 
	 * 重置LineSegmentCloneable对象的属性
	 * 
	 * @param startPoint
	 *            线条一端
	 * @param endPoint
	 *            线条另外一端
	 * @param lineWidth
	 *            int 线条宽
	 * @param multiple
	 *            放大缩小倍数
	 */
	public void reSetAttributes(Point startPoint, Point endPoint,
			int lineWidth, double multiple) {
		if (startPoint == null
				|| endPoint == null
				|| (startPoint.getX() != endPoint.getX() && startPoint.getY() != endPoint
						.getY())) {
			return;
		}
		initOriginalArributs(startPoint, endPoint, lineWidth, multiple);
	}

	/**
	 * 
	 * 获取线段的位置点
	 * 
	 * @return Point
	 */
	public Point getLocation() {
		return new Point(getMinXInt(), getMinYInt());
	}

	/**
	 * 
	 * 获取位置点X坐标
	 * 
	 * @return int
	 */
	public int getMinXInt() {
		return (int) Math.floor(getMinX() + 0.5);
	}

	/**
	 * 
	 * 获取位置点X坐标
	 * 
	 * @return double
	 */
	public double getMinX() {
		return Math.min(startXY.getX(), endXY.getX());
	}

	/**
	 * 
	 * 获取位置点Y坐标
	 * 
	 * @return int
	 */
	public int getMinYInt() {
		return (int) Math.floor(getMinY() + 0.5);
	}

	/**
	 * 
	 * 获取位置点Y坐标
	 * 
	 * @return double
	 */
	public double getMinY() {
		return Math.min(startXY.getY(), endXY.getY());
	}

	/**
	 * 
	 * 获取最大X坐标
	 * 
	 * @return double
	 */
	public double getMaxX() {
		return Math.max(startXY.getX(), endXY.getX());
	}

	/**
	 * 
	 * 获取最大Y坐标
	 * 
	 * @return double
	 */
	public double getMaxY() {
		return Math.max(startXY.getY(), endXY.getY());
	}

	/**
	 * 
	 * 初始化原始数据，根据放大倍数换算原始数据
	 * 
	 * @param startPoint
	 *            线条一端
	 * @param endPoint
	 *            线条另外一端
	 * @param lineOriginaWidth
	 *            原始状态下线条宽
	 * @param multiple
	 *            放大缩小倍数
	 */
	public void initOriginalArributs(Point startPoint, Point endPoint,
			int lineOriginaWidth, double multiple) {

		Point startOriginalPoint = getOriginalStartXY(startPoint, multiple);
		Point endOriginalPoint = getOriginalEndXY(endPoint, multiple);

		// 开始坐标
		this.startXY.setX(startOriginalPoint.getX());
		this.startXY.setY(startOriginalPoint.getY());
		// 结束坐标
		this.endXY.setX(endOriginalPoint.getX());
		this.endXY.setY(endOriginalPoint.getY());

		// 线段方向
		this.lineDirectionEnum = this.getLineType();
		// 线条宽
		this.lineOriginaWidth = lineOriginaWidth;
		// 宽度，高度
		initOriginalSize();
	}

	/**
	 * 
	 * 初始化线条大小
	 * 
	 */
	private void initOriginalSize() {
		switch (lineDirectionEnum) {
		case horizontal:// 水平
			// 宽
			this.width = Math.abs(this.startXY.getX() - this.endXY.getX());
			// 高
			this.height = this.lineOriginaWidth;
			break;
		case vertical:// 垂直
			// 宽
			this.width = this.lineOriginaWidth;
			// 高
			this.height = Math.abs(this.startXY.getY() - this.endXY.getY());
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 获取原始（100%）线条一端坐标
	 * 
	 * @param localPoint
	 *            Point 线左上端坐标
	 * @param multiple
	 *            double 放大缩小倍数
	 * @return Point 原始的线左上端坐标
	 */
	private Point getOriginalStartXY(Point startPoint, double multiple) {
		Point point = new Point();
		point.setLocation(startPoint.getX() / multiple, startPoint.getY()
				/ multiple);
		return point;
	}

	/**
	 * 
	 * 获取原始（100%）线条另外一端坐标
	 * 
	 * @param endPoint
	 *            Point 线右上端坐标
	 * @param multiple
	 *            double 放大缩小倍数
	 * @return Point 原始的线线右上端坐标
	 */
	private Point getOriginalEndXY(Point endPoint, double multiple) {
		Point point = new Point();
		point.setLocation(endPoint.getX() / multiple, endPoint.getY()
				/ multiple);
		return point;
	}

	/**
	 * 
	 * 获取线段对应数据类克隆放大缩小对象,如果开始点和结束点的坐标中x或者y至少有一个不相等返回null
	 * 
	 * @param zooMmultiple
	 * @return LineSegmentCloneable 放大后的复制对象或者null
	 */
	public JecnLineSegmentDataCloneable getZoomedLineSegmentCloneble(
			double zoomMultiple) {
		if ((startXY.getX() != endXY.getX() && startXY.getY() != endXY.getY())) {
			return null;
		}
		// 复制线段对象
		JecnLineSegmentDataCloneable lineSegmentCloneble = this.clone();

		lineSegmentCloneble.startXY = lineSegmentCloneble.startXY
				.cloneZoomXYCloneable(zoomMultiple);

		lineSegmentCloneble.endXY = lineSegmentCloneble.endXY
				.cloneZoomXYCloneable(zoomMultiple);

		setWidthAndHeight(this.lineDirectionEnum, lineSegmentCloneble,
				zoomMultiple);

		return lineSegmentCloneble;
	}

	/**
	 * 
	 * 此方法设置线条宽高，主要是让线条不变粗
	 * 
	 * @param lineType
	 *            LineDirectionEnum线方向类型
	 * @param lineSegmentCloneble
	 *            JecnLineSegmentDataCloneable
	 * @param zoomMultiple
	 *            double
	 */
	private void setWidthAndHeight(LineDirectionEnum lineType,
			JecnLineSegmentDataCloneable lineSegmentCloneble,
			double zoomMultiple) {

		// 线条长度 ＝ size 的长度 + 宽度 减去 默认的原始宽度１
		switch (lineType) {
		case horizontal:// 横线
			// 横线
			lineSegmentCloneble.width = Math.abs(lineSegmentCloneble.width
					* zoomMultiple + lineSegmentCloneble.height - 1);
			break;
		case vertical:// 竖线
			// 竖线
			lineSegmentCloneble.height = Math.abs(lineSegmentCloneble.height
					* zoomMultiple + lineSegmentCloneble.width - 1);
			break;
		default:
			// 不能判断线情况
			if (lineSegmentCloneble.height > lineSegmentCloneble.width) {
				lineSegmentCloneble.height = lineSegmentCloneble.height
						* zoomMultiple;
			} else if (lineSegmentCloneble.height < lineSegmentCloneble.width) {
				lineSegmentCloneble.width = lineSegmentCloneble.width
						* zoomMultiple;
			} else {
				lineSegmentCloneble.width = lineSegmentCloneble.width;
			}
			break;

		}
	}

	/**
	 * 
	 * 对调开始点和结束点
	 * 
	 */
	public void swapStartEndPoint() {
		JecnXYData start = this.startXY.clone();
		this.startXY = this.endXY;
		this.endXY = start;
	}

	/**
	 * 
	 * 设置属性
	 * 
	 * @param paramI
	 */
	private void copyArritures(JecnLineSegmentDataCloneable paramI) {
		// 坐标点
		this.startXY = paramI.startXY.clone();
		this.endXY = paramI.endXY.clone();
		// 高宽
		this.width = paramI.width;
		this.height = paramI.height;
		// 线条宽
		this.lineOriginaWidth = paramI.lineOriginaWidth;
		// 线条方向
		this.lineDirectionEnum = paramI.lineDirectionEnum;
	}

	/**
	 * 
	 * 得到线段方向 错误情况
	 * 
	 * @return LineDirectionEnum 线方向类型
	 */
	private LineDirectionEnum getLineType() {
		if (this.getStartXY().getY() == this.getEndXY().getY()) {// 横线
			return LineDirectionEnum.horizontal;
		} else if (this.getStartXY().getX() == this.getEndXY().getX()) {// 竖线
			return LineDirectionEnum.vertical;
		} else {
			JecnLogConstants.LOG_LINE_SEGMENT_DATA_PROCESS
					.error("JecnLineSegmentDataCloneable类getLineType方法：线的X坐标和Y坐标其中一个要相等.StartX="
							+ this.getStartXY().getX()
							+ ",EndX="
							+ this.getEndXY().getX()
							+ ";StartY="
							+ this.getStartXY().getY()
							+ ",EndY="
							+ this.getEndXY().getY());
			return LineDirectionEnum.error;
		}
	}

	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnLineSegmentDataCloneable clone() {
		JecnLineSegmentDataCloneable lineSegmentDataCloneable = new JecnLineSegmentDataCloneable();
		lineSegmentDataCloneable.copyArritures(this);
		return lineSegmentDataCloneable;
	}

	/**
	 * 
	 * 获取宽度
	 * 
	 * @return
	 */
	public int getWidth() {
		return getIntByDouble(width);
	}
	
	/**
	 * 
	 * 获取宽度
	 * 
	 * @return
	 */
	public double getWidthDouble() {
		return width;
	}

	/**
	 * 
	 * 获取高度
	 * 
	 * @return
	 */
	public int getHeight() {
		return getIntByDouble(height);
	}
	
	/**
	 * 
	 * 获取高度
	 * 
	 * @return
	 */
	public double getHeightDouble() {
		return height;
	}

	public Dimension getSize() {
		return new Dimension(this.getWidth(), this.getHeight());
	}

	public JecnXYData getStartXY() {
		return startXY;
	}

	public JecnXYData getEndXY() {
		return endXY;
	}

	public int getStartXInt() {
		return getIntByDouble(startXY.getX());
	}

	public int getStartYInt() {
		return getIntByDouble(startXY.getY());
	}

	public int getEndXInt() {
		return getIntByDouble(endXY.getX());
	}

	public int getEndYInt() {
		return getIntByDouble(endXY.getY());
	}

	public int getIntByDouble(double db) {
		return (int) Math.floor(db + 0.5);
	}

	public LineDirectionEnum getLineDirectionEnum() {
		return lineDirectionEnum;
	}

}