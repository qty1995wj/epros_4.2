package com.jecn.epros.server.action.rmi.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.email.buss.EmailConfigItemBean;
import com.jecn.epros.server.email.buss.SMTPSendEmailBuilder;
import com.jecn.epros.server.service.propose.IRtnlProposeService;
import com.jecn.epros.server.service.system.IJecnConfigItemService;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.service.IMixRPCService;

public class MixRPCServiceImpl implements IMixRPCService {

	private IRtnlProposeService rtnlProposeService;

	@Autowired
	private IJecnConfigItemService configItemService;

	@Override
	public MessageResult validateEmailPassword(boolean isOuter, String name, String pwd) {
		MessageResult result = new MessageResult(true, "验证成功");
		// 查询内外网配置项
		EmailConfigItemBean emailConfig = EmailConfigItemBean.getConfigItemBean();
		// 获取配置数据
		emailConfig.initEmailItem();
		if (isOuter) {
			emailConfig.setOuterLoginName(name);
			emailConfig.setOuterPWd(pwd);
			emailConfig.setOuterUserAddr("");
		} else {
			emailConfig.setInnerLoginName(name);
			emailConfig.setInnerPWd(pwd);
			emailConfig.setInnerUserAddr("");
		}
		SMTPSendEmailBuilder builder = new SMTPSendEmailBuilder(emailConfig, isOuter);
		boolean initSuccess = builder.initSession();
		if (!initSuccess) {
			result.setSuccess(false);
			result.setResultMessage("邮件配置项异常");
		}

		MessageResult authResult = builder.testEmailAuthentication();
		if (!authResult.isSuccess()) {
			return authResult;
		}
		return result;
	}

	@Override
	public MessageResult handleProposeSendEmail(String proposeId, int flag) {
		MessageResult result = new MessageResult();
		try {
			rtnlProposeService.handleProposeSendEmail(proposeId, flag);
		} catch (Exception e) {
			result.setSuccess(false);
			Log.error("", e);
		}
		return result;
	}

	@Override
	public MessageResult submitProposeSendEmail(String proposeId) {
		MessageResult result = new MessageResult();
		try {
			rtnlProposeService.submitProposeSendEmail(proposeId);
		} catch (Exception e) {
			result.setSuccess(false);
			Log.error("", e);
		}
		return result;
	}

	public IRtnlProposeService getRtnlProposeService() {
		return rtnlProposeService;
	}

	public void setRtnlProposeService(IRtnlProposeService rtnlProposeService) {
		this.rtnlProposeService = rtnlProposeService;
	}

	@Override
	public MessageResult syncDictinaryConfig() {
		Map<DictionaryEnum, List<JecnDictionary>> initDictionaryMap = configItemService.initDictionaryMap();
		if (initDictionaryMap != null && initDictionaryMap.size() > 0) {
			JecnContants.dictionaryMap.clear();
			JecnContants.dictionaryMap.putAll(initDictionaryMap);
		}
		return new MessageResult();
	}
	
	@Override
	public List<String> fetchLogNames() {
		List<String> result = new ArrayList<String>();
		File f = new File("epros_log");
		if (f.exists() && f.isDirectory()) {
			File[] files = f.listFiles();
			for (File e : files) {
				result.add(e.getName());
			}
		}
		return result;
	}

	@Override
	public byte[] getLogByte(String name) {
		byte[] bytes = new byte[0];
		File file = new File("epros_log/" + name);
		if (file.exists() && file.isFile()) {
			InputStream input = null;
			try {
				input = new FileInputStream(file);
				bytes = IOUtils.toByteArray(input);
			} catch (Exception e) {

			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {

					}
				}
			}
		}
		return bytes;
	}

}
