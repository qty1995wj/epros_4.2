package epros.draw.data.figure;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.designer.JecnActiveStandardBeanT;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnControlPointT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.designer.JecnRefIndicatorsT;
import epros.draw.util.DrawCommon;

/**
 * 活动图形，决策，IT决策，信息系统 的数据层
 * 
 * @author fuzhz
 * 
 */
public class JecnActiveData extends JecnFigureData {
	/** 角色是否与活动对应 */
	private boolean isRefRole = false;
	/** 活动编号 */
	private String activityNum = "";
	/** 活动说明 */
	private String avtivityShow = "";
	/** 关键活动类型：1为PA,2为KSF,3为KCP */
	private String activeKeyType = "";
	/** 关键说明 */
	private String avtivityShowAndControl = "";
	/** 输出 */
	private List<JecnModeFileT> listModeFileT = new ArrayList<JecnModeFileT>();
	/** 输入和操作规范 */
	private List<JecnActivityFileT> listJecnActivityFileT = new ArrayList<JecnActivityFileT>();

	/** 活动输入、输出 new */
	private List<JecnFigureInoutT> listFigureInTs = new ArrayList<JecnFigureInoutT>();

	/** 活动输入、输出 new */
	private List<JecnFigureInoutT> listFigureOutTs = new ArrayList<JecnFigureInoutT>();

	/** 指标 */
	private List<JecnRefIndicatorsT> listRefIndicatorsT = new ArrayList<JecnRefIndicatorsT>();
	/** 活动开始时间 */
	private String avtivityStartTime = "";
	/** 活动结束时间 */
	private String avtivityStopTime = "";
	/** 活动时间类型 1是日，2是周，3是月，4是季，5是年 */
	private Integer avtivityTimeType = 0;
	/** 对应内控矩阵风险点 */
	private String innerControlRisk = "";
	/*** 对应标准条款 */
	private String standardConditions = "";
	/** 活动类型ID */
	private Long activeTypeId;
	/** 是否线上 0：线下 ;1：线上 */
	private int isOnLine;
	/** 控制点 */
	private JecnControlPointT controlPoint;

	/** 控制点集合 C */
	private List<JecnControlPointT> jecnControlPointTList;

	/** 活动标准关联表 */
	private List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT;
	/** 线上信息 */
	private List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean;
	/** 活动输入 */
	private String activeInput;
	/** 活动输出 */
	private String activeOutPut;
	// 输入说明
	private String activityInput;
	private String activityOutput;

	/************** 时间轴 ***************************/
	/** 目标值 */
	private BigDecimal targetValue;
	/** 现状值 */
	private BigDecimal statusValue;
	/** 说明 */
	private String explain;

	/** 记录时间轴添加的活动顺序（包含不存在时间轴的活动） */
	private int timeLineSort;

	/** 自定义 （爱普生 活动担当） */
	private String customOne;

	/**
	 * 
	 * 关键活动类型：PAFigure:问题区域 KSFFigure:关键成功因素 KCPFigure:关键控制点
	 * 当在活动明细中取消勾选关键活动项时，此字段为NULL
	 * 
	 */
	private MapElemType activeKeyMapElemType = null;

	public JecnActiveData(MapElemType mapElemType) {
		super(mapElemType);
	}

	/**
	 * 
	 * 实例化对象
	 * 
	 */
	public JecnActiveData newInstance() {
		return new JecnActiveData(mapElemType);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnActiveData clone() {
		return (JecnActiveData) this.currClone();
	}

	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnActiveData) || !(destFlowElementData instanceof JecnActiveData)) {
			return;
		}

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		commonActiveAttrs(srcFlowElementData, destFlowElementData);
	}

	private void commonActiveAttrs(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {
		JecnActiveData src = (JecnActiveData) srcFlowElementData;
		JecnActiveData dest = (JecnActiveData) destFlowElementData;
		dest.setFigureText(src.getFigureText());
		// 角色是否与活动对应
		dest.setRefRole(src.isRefRole());
		// 活动编号
		dest.setActivityNum(src.getActivityNum());
		// 活动说明
		dest.setAvtivityShow(src.getAvtivityShow());
		// 关键说明
		dest.setAvtivityShowAndControl(src.getAvtivityShowAndControl());
		// 关键活动类型
		dest.setActiveKeyMapElemType(src.getActiveKeyMapElemType());
		// 输入
		dest.setActiveInput(src.getActiveInput());
		// 输出
		dest.setActiveOutPut(src.getActiveOutPut());
		// 活动线上
		dest.setIsOnLine(src.getIsOnLine());

		dest.setActivityInput(src.getActivityInput());
		dest.setActivityOutput(src.getActivityOutput());
		dest.setTargetValue(src.getTargetValue());
		dest.setStatusValue(src.getStatusValue());
		dest.setExplain(src.getExplain());
		dest.setCustomOne(src.getCustomOne());
		dest.setActiveTypeId(src.getActiveTypeId());

		if (src.getListJecnActiveOnLineTBean() != null) {
			List<JecnActiveOnLineTBean> beans = new ArrayList<JecnActiveOnLineTBean>();
			for (JecnActiveOnLineTBean activeOnLineTBean : src.getListJecnActiveOnLineTBean()) {
				beans.add(activeOnLineTBean.clone());
			}
			dest.setListJecnActiveOnLineTBean(beans);
		}
	}

	public void setFigureArrts(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		super.setFigureArrts(srcFlowElementData, destFlowElementData);

		if (!(srcFlowElementData instanceof JecnActiveData && destFlowElementData instanceof JecnActiveData)) {
			return;
		}
		JecnActiveData src = (JecnActiveData) srcFlowElementData;
		JecnActiveData dest = (JecnActiveData) destFlowElementData;
		commonActiveAttrs(srcFlowElementData, destFlowElementData);

		List<JecnModeFileT> modeFileTs = new ArrayList<JecnModeFileT>();
		for (JecnModeFileT modeFileT : src.getListModeFileT()) {
			modeFileTs.add(modeFileT.clone());
		}
		dest.setListModeFileT(modeFileTs);

		List<JecnActivityFileT> activityFileTs = new ArrayList<JecnActivityFileT>();
		for (JecnActivityFileT activityFile : src.getListJecnActivityFileT()) {
			activityFileTs.add(activityFile.clone());
		}
		dest.setListJecnActivityFileT(activityFileTs);

		List<JecnRefIndicatorsT> indicatorsTs = new ArrayList<JecnRefIndicatorsT>();
		for (JecnRefIndicatorsT refIndicatorsT : src.getListRefIndicatorsT()) {
			indicatorsTs.add(refIndicatorsT.clone());
		}
		dest.setListRefIndicatorsT(indicatorsTs);

		if (src.getListJecnActiveStandardBeanT() != null) {
			List<JecnActiveStandardBeanT> activeStandardBeanTs = new ArrayList<JecnActiveStandardBeanT>();
			for (JecnActiveStandardBeanT standardBeanT : src.getListJecnActiveStandardBeanT()) {
				activeStandardBeanTs.add(standardBeanT.clone());
			}
			dest.setListJecnActiveStandardBeanT(activeStandardBeanTs);
		}
		
		if (src.getJecnControlPointTList() != null) {
			List<JecnControlPointT> riskBeanTs = new ArrayList<JecnControlPointT>();
			for (JecnControlPointT riskBeanT : src.getJecnControlPointTList()) {
				riskBeanTs.add(riskBeanT.clone());
			}
			dest.setJecnControlPointTList(riskBeanTs);
		}

		if (src.listFigureInTs != null) {
			List<JecnFigureInoutT> cloneListFigureInTs = new ArrayList<JecnFigureInoutT>();
			for (JecnFigureInoutT conleInoutT : src.listFigureInTs) {
				cloneListFigureInTs.add(conleInoutT.clone());
			}
			dest.setListFigureInTs(cloneListFigureInTs);
		}
		if (src.listFigureOutTs != null) {
			List<JecnFigureInoutT> cloneListFigureInTs = new ArrayList<JecnFigureInoutT>();
			for (JecnFigureInoutT conleInoutT : src.listFigureOutTs) {
				cloneListFigureInTs.add(conleInoutT.clone());
			}
			dest.setListFigureOutTs(cloneListFigureInTs);
		}
		if(jecnControlPointTList!=null){
			List<JecnControlPointT> cloneControlPointTs = new ArrayList<JecnControlPointT>();
			for (JecnControlPointT controlPointT : jecnControlPointTList) {
				cloneControlPointTs.add(controlPointT.clone());
			}
		}
	}

	public List<JecnModeFileT> getListModeFileT() {
		return listModeFileT;
	}

	public void setListModeFileT(List<JecnModeFileT> listModeFileT) {
		this.listModeFileT = listModeFileT;
	}

	public List<JecnActivityFileT> getListJecnActivityFileT() {
		return listJecnActivityFileT;
	}

	public void setListJecnActivityFileT(List<JecnActivityFileT> listJecnActivityFileT) {
		this.listJecnActivityFileT = listJecnActivityFileT;
	}

	public boolean isInFiles() {
		if (listJecnActivityFileT == null || listJecnActivityFileT.size() == 0) {
			return false;
		}
		for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
			if (jecnActivityFileT.getFileType() != null && jecnActivityFileT.getFileType().intValue() == 0) {
				return true;
			}
		}
		return false;
	}

	public boolean isInTermFiles() {
		if (listFigureInTs == null || listFigureInTs.size() == 0) {
			return false;
		}
		return true;
	}

	public List<JecnRefIndicatorsT> getListRefIndicatorsT() {
		return listRefIndicatorsT;
	}

	public void setListRefIndicatorsT(List<JecnRefIndicatorsT> listRefIndicatorsT) {
		this.listRefIndicatorsT = listRefIndicatorsT;
	}

	public boolean isRefRole() {
		return isRefRole;
	}

	public void setRefRole(boolean isRefRole) {
		this.isRefRole = isRefRole;
	}

	public String getActivityNum() {
		return activityNum;
	}

	public void setActivityNum(String activityNum) {
		if (DrawCommon.checkStringSame(this.activityNum, activityNum)) {// 当前值和原来值相等是不更新
			return;
		}
		this.activityNum = activityNum;
	}

	public String getAvtivityShow() {
		return avtivityShow;
	}

	public void setAvtivityShow(String avtivityShow) {
		if (DrawCommon.checkStringSame(this.avtivityShow, avtivityShow)) {// 当前值和原来值相等是不更新
			return;
		}
		this.avtivityShow = avtivityShow;
	}

	public String getAvtivityShowAndControl() {
		return avtivityShowAndControl;
	}

	public void setAvtivityShowAndControl(String avtivityShowAndControl) {
		if (DrawCommon.checkStringSame(this.avtivityShowAndControl, avtivityShowAndControl)) {// 当前值和原来值相等是不更新
			return;
		}
		this.avtivityShowAndControl = avtivityShowAndControl;
	}

	public MapElemType getActiveKeyMapElemType() {
		return activeKeyMapElemType;
	}

	public void setActiveKeyMapElemType(MapElemType activeKeyMapElemType) {
		// 关键活动类型：PAFigure:问题区域 KSFFigure:关键成功因素 KCPFigure:关键控制点
		if (activeKeyMapElemType == null) {
			return;
		}
		if (MapElemType.PAFigure == activeKeyMapElemType || MapElemType.KSFFigure == activeKeyMapElemType
				|| MapElemType.KCPFigure == activeKeyMapElemType || MapElemType.KCPFigureComp == activeKeyMapElemType) {
			this.activeKeyMapElemType = activeKeyMapElemType;
		} else {
			this.activeKeyMapElemType = null;
		}
	}

	public String getActiveKeyType() {
		return activeKeyType;
	}

	public void setActiveKeyType(String activeKeyType) {
		this.activeKeyType = activeKeyType;
	}

	public String getAvtivityStartTime() {
		return avtivityStartTime;
	}

	public void setAvtivityStartTime(String avtivityStartTime) {
		this.avtivityStartTime = avtivityStartTime;
	}

	public String getAvtivityStopTime() {
		return avtivityStopTime;
	}

	public void setAvtivityStopTime(String avtivityStopTime) {
		this.avtivityStopTime = avtivityStopTime;
	}

	public Integer getAvtivityTimeType() {
		return avtivityTimeType;
	}

	public void setAvtivityTimeType(Integer avtivityTimeType) {
		this.avtivityTimeType = avtivityTimeType;
	}

	public String getInnerControlRisk() {
		return innerControlRisk;
	}

	public void setInnerControlRisk(String innerControlRisk) {
		if (DrawCommon.checkStringSame(this.innerControlRisk, innerControlRisk)) {// 当前值和原来值相等是不更新
			return;
		}
		this.innerControlRisk = innerControlRisk;
	}

	public String getStandardConditions() {
		return standardConditions;
	}

	public void setStandardConditions(String standardConditions) {
		if (DrawCommon.checkStringSame(this.standardConditions, standardConditions)) {// 当前值和原来值相等是不更新
			return;
		}
		this.standardConditions = standardConditions;
	}

	public Long getActiveTypeId() {
		return activeTypeId;
	}

	public void setActiveTypeId(Long activeTypeId) {
		this.activeTypeId = activeTypeId;
	}

	public int getIsOnLine() {
		return isOnLine;
	}

	public void setIsOnLine(int isOnLine) {
		this.isOnLine = isOnLine;
	}

	public List<JecnActiveOnLineTBean> getListJecnActiveOnLineTBean() {
		if (listJecnActiveOnLineTBean == null) {
			listJecnActiveOnLineTBean = new ArrayList<JecnActiveOnLineTBean>();
		}
		return listJecnActiveOnLineTBean;
	}

	public List<JecnActiveStandardBeanT> getListJecnActiveStandardBeanT() {
		if (listJecnActiveStandardBeanT == null) {
			listJecnActiveStandardBeanT = new ArrayList<JecnActiveStandardBeanT>();
		}
		return listJecnActiveStandardBeanT;
	}

	public JecnControlPointT getControlPoint() {
		return controlPoint;
	}

	public void setControlPoint(JecnControlPointT controlPoint) {
		this.controlPoint = controlPoint;
	}

	public void setListJecnActiveStandardBeanT(List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT) {
		this.listJecnActiveStandardBeanT = listJecnActiveStandardBeanT;
	}

	public void setListJecnActiveOnLineTBean(List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean) {
		this.listJecnActiveOnLineTBean = listJecnActiveOnLineTBean;
	}

	public String getActiveInput() {
		return activeInput;
	}

	public void setActiveInput(String activeInput) {
		this.activeInput = activeInput;
	}

	public String getActiveOutPut() {
		return activeOutPut;
	}

	public void setActiveOutPut(String activeOutPut) {
		this.activeOutPut = activeOutPut;
	}

	public String getActivityInput() {
		return activityInput;
	}

	public void setActivityInput(String activityInput) {
		this.activityInput = activityInput;
	}

	public String getActivityOutput() {
		return activityOutput;
	}

	public void setActivityOutput(String activityOutput) {
		this.activityOutput = activityOutput;
	}

	public List<JecnControlPointT> getJecnControlPointTList() {
		if (jecnControlPointTList == null) {
			jecnControlPointTList = new ArrayList<JecnControlPointT>();
		}
		return jecnControlPointTList;
	}

	public void setJecnControlPointTList(List<JecnControlPointT> jecnControlPointTList) {
		this.jecnControlPointTList = jecnControlPointTList;
	}

	public BigDecimal getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(BigDecimal targetValue) {
		this.targetValue = targetValue;
	}

	public BigDecimal getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(BigDecimal statusValue) {
		this.statusValue = statusValue;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public int getTimeLineSort() {
		return timeLineSort;
	}

	public void setTimeLineSort(int timeLineSort) {
		this.timeLineSort = timeLineSort;
	}

	public String getCustomOne() {
		return customOne;
	}

	public void setCustomOne(String customOne) {
		this.customOne = customOne;
	}

	public List<JecnFigureInoutT> getListFigureInTs() {
		return listFigureInTs;
	}

	public List<JecnFigureInoutT> getListFigureOutTs() {
		return listFigureOutTs;
	}

	public void setListFigureInTs(List<JecnFigureInoutT> listFigureInTs) {
		this.listFigureInTs = listFigureInTs;
	}

	public void setListFigureOutTs(List<JecnFigureInoutT> listFigureOutTs) {
		this.listFigureOutTs = listFigureOutTs;
	}

	public boolean isInNameNull() { // 判断新版输入中所有输入的名称是否为空
		for (JecnFigureInoutT in : listFigureInTs) {
			if (StringUtils.isNotBlank(in.getName())) {
				return false;
			}
		}
		return true;
	}

	public boolean isOutNameNull() {// 判断新版输出中所有输入的名称是否为空
		for (JecnFigureInoutT in : listFigureOutTs) {
			if (StringUtils.isNotBlank(in.getName())) {
				return false;
			}
		}
		return true;
	}

}
