package com.jecn.epros.server.action.web.login.ad.sfCsr;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

public class SFCSRLoginAction extends JecnAbstractADLoginAction {

	public SFCSRLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		// 登录名称
		String adLoginName = this.loginAction.getLoginName();
		// 登录密码
		String adPassWord = this.loginAction.getPassword();

		log.info("登录名密码： adLoginName = " + adLoginName + " adPassWord = " + adPassWord);
		if (StringUtils.isNotBlank(adLoginName) && StringUtils.isNotBlank(adPassWord)) {
			return loginAction.userLoginGen(true);
		}

		// 单点登录名称
		String userName = loginAction.getRequest().getParameter("username");
		String passWord = loginAction.getRequest().getParameter("password");

		String result = loginSessionValidate();
		if (LoginAction.SUCCESS.equals(result) || LoginAction.MAIN.equals(result)) {
			return result;
		}
		if (StringUtils.isBlank(userName) || StringUtils.isBlank(passWord)) {
			return LoginAction.INPUT;
		}
		loginAction.setLoginName(userName);
		loginAction.setPassword(passWord);
		log.info("单点用户名密码获取成功： userName = " + userName + " passWord = " + passWord);
		return loginAction.loginGen();
	}

	private String loginSessionValidate() {
		// session中用户信息map集合
		Map<String, Object> sessionMap = ActionContext.getContext().getSession();
		// 从session中获取用户信息
		Object obj = sessionMap.get(JecnContants.SESSION_KEY);
		if (obj != null && obj instanceof WebLoginBean) {// 用户信息存在情况
			WebLoginBean webLoginBean = (WebLoginBean) obj;
			if (webLoginBean.isAdmin()) {// 是管理员
				return LoginAction.MAIN;
			} else {
				return LoginAction.SUCCESS;
			}
		}
		return LoginAction.INPUT;
	}
}
