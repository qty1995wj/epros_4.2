package epros.designer.gui.process.guide.guideTable;

import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.kpi.TmpKpiShowValues;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程测评指标版
 * 
 * @author 2012-07-05
 * 
 */
public class ProcessEvaluationTable extends JTable {
	private Vector<Vector<String>> content;

	public ProcessEvaluationTable() {
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public void setTableModel(Long flowId) {
		this.setModel(getTableModel(flowId));
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
	}

	public ProcessEvaluationTableMode getTableModel(Long flowId) {
		Vector<String> title = new Vector<String>();
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		try {
			TmpKpiShowValues TmpKpiShowValues = JecnDesignerCommon.getShowValues(flowId);
			List<JecnConfigItemBean> kpiTitle = TmpKpiShowValues.getKpiTitles();
			List<List<String>> kpi = TmpKpiShowValues.getKpiRowValues();
			for (JecnConfigItemBean itemBean : kpiTitle) {
				if (JecnResourceUtil.getLocale() == Locale.ENGLISH) {// English
					title.add(itemBean.getEnName());
				} else {
					title.add(itemBean.getName());
				}
			}
			Vector<String> row = null;
			for (List<String> rowConTent : kpi) {
				row = new Vector<String>();
				for (String tdStr : rowConTent) {
					row.add(tdStr);
				}
				content.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
			new RuntimeException("ProcessEvaluationTable getTableModel is error");
		}
		return new ProcessEvaluationTableMode(content, title);
	}

	public void setTableModel() {
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
	}

	public ProcessEvaluationTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("name"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("type"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("KPIDefinition"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("statisticalMethod"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("targetValue"));
		return new ProcessEvaluationTableMode(getContent(), title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class ProcessEvaluationTableMode extends DefaultTableModel {
		public ProcessEvaluationTableMode(Vector<Vector<String>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public Vector<Vector<String>> getContent() {
		return content;
	}

	public void setContent(Vector<Vector<String>> content) {
		this.content = content;
	}
}
