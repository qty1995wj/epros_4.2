package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.Date;

public class JecnJournal implements Serializable {
	/** 主键ID */
	private Long id;
	/** 日志对象ID */
	private Long relateId;
	/** 日志对象名称 */
	private String relateName;
	/**
	 * 日志类型 1是流程，2是文件，3是角色,4是制度,5是标准,6是组织,9是风险,15 术语定义 16岗位组 ，17支持工具， 18风险点，
	 * 19制度模板 ， 20模型
	 * */
	private int type;
	/**
	 * 日志类型 1是新增，2是修改，3是删除，4移动，5排序，6假删除放到回收站,10自动编号，13设置任务模板
	 * 15记录恢复（本地上传的文件或者制度或者流程文件）16记录删除 17发布不记录文控 18撤销发布
	 */
	private int operatorType;
	/** 更新人ID */
	private Long updatePeopleId;
	/** 更新时间 */
	private Date updateTime;
	/**
	 * 当operatorType为2时 0 属性修改（默认值） 1 流程图修改 2 架构卡图修改 3 集成关系图修改 4 重命名 5 相关文件修改 6
	 * 流程文件修改（操作说明） 7 查阅权限  8 文件内容（更新内容） 9 流程文件转换为流程图 10 批量修改专员责任人
	 */
	private int secondOperatorType;

	public int getSecondOperatorType() {
		return secondOperatorType;
	}

	public void setSecondOperatorType(int secondOperatorType) {
		this.secondOperatorType = secondOperatorType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRelateId() {
		return relateId;
	}

	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}

	public String getRelateName() {
		return relateName;
	}

	public void setRelateName(String relateName) {
		this.relateName = relateName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(int operatorType) {
		this.operatorType = operatorType;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
