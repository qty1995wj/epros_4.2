/**
 * HrPublisherPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public interface HrPublisherPortType extends java.rmi.Remote {
    public com.jecn.webservice.crs.dataimport.BasePackage login(java.lang.String userName, java.lang.String password) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.SummaryPackage getPOSITION_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.SummaryPackage getORGANIZATION_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.SummaryPackage getTB_INF_EMPLOYEESummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.SummaryPackage getTB_INF_EMPCONTACTSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.SummaryPackage getTB_PER_EMPLOYEEJOBSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.SummaryPackage getUSER_POS_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.SummaryPackage getORG_VIEWSummary(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.POSITION_VIEWDataPackage getPOSITION_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.ORGANIZATION_VIEWDataPackage getORGANIZATION_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.TB_INF_EMPLOYEEDataPackage getTB_INF_EMPLOYEEPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.TB_INF_EMPCONTACTDataPackage getTB_INF_EMPCONTACTPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.TB_PER_EMPLOYEEJOBDataPackage getTB_PER_EMPLOYEEJOBPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.USER_POS_VIEWDataPackage getUSER_POS_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.ORG_VIEWDataPackage getORG_VIEWPackage(java.lang.String userName, java.lang.String password, java.math.BigInteger versionId, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.BasePackage logout() throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.POSITION_VIEWDataPackage queryPOSITION_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.ORGANIZATION_VIEWDataPackage queryORGANIZATION_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.TB_INF_EMPLOYEEDataPackage queryTB_INF_EMPLOYEEData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.TB_INF_EMPCONTACTDataPackage queryTB_INF_EMPCONTACTData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.TB_PER_EMPLOYEEJOBDataPackage queryTB_PER_EMPLOYEEJOBData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.USER_POS_VIEWDataPackage queryUSER_POS_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
    public com.jecn.webservice.crs.dataimport.ORG_VIEWDataPackage queryORG_VIEWData(java.lang.String userName, java.lang.String password, java.lang.String filter, java.math.BigInteger pageNo) throws java.rmi.RemoteException;
}
