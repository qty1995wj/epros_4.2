package wordxml.element.node.page;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.constant.Constants.DocGridType;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.WordElementFactory;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.util.Unit;

/**
 * 节
 * 
 * @author admin
 * 
 */
public class SectNode {
	/** 日志对象 */
	private static final Log log = LogFactory.getLog(SectNode.class);

	/***
	 * 创建一个节
	 * 
	 * @param sect
	 * @param parentEle
	 */
	public static void createSect(Sect sect, Element parentEle) {
		if (sect == null || parentEle == null || sect.getSectBean() == null) {
			log.info("SectNode class createSect Argument is not valid");
			return;
		}
		// 节内没有任何内容时直接返回
		if (sect.getBodyElements().size() == 0) {
			return;
		}

		Element sectEle = parentEle.addElement("wx:sect");

		// 实例化节中的内容
		for (IBodyElement bodyEle : sect.getBodyElements()) {
			WordElementFactory.produceElement(bodyEle, sectEle);
		}
		// 节属性节点 最后一节 节属性放在节里面 ，否者放在 段落中
		Element sectPr = null;
		if (!sect.isLast()) {
			sectPr = sectEle.addElement("w:p").addElement("w:pPr").addElement(
					"w:sectPr");
		} else {
			sectPr = sectEle.addElement("w:sectPr");
		}
		// 1.设置节的纸张大小等
		if (sect.getSectBean() != null) {
			setSectAttr(sectPr, sect.getSectBean());
		}
		// 2.调用数据写入页眉页脚
		List<Header> listHeader = sect.getHeaders();
		List<Footer> listFooter = sect.getFooters();
		if (listHeader != null && listHeader.size() > 0) {
			for (Header header : listHeader) {
				HeaderNode.createHeader(header, sectPr);
			}
		}
		if (listFooter != null && listFooter.size() > 0) {
			for (Footer footer : listFooter) {
				FooterNode.createFooter(footer, sectPr);
			}
		}

	}

	/**
	 * 设置属性节大小
	 * 
	 * @param sectEle
	 * @param sectBean
	 */
	private static void setSectAttr(Element sectPr, SectBean sectBean) {
		if (sectBean.isTitlePg()) {
			sectPr.addElement("w:titlePg");
		}
		// 设置页面长度和宽度
		Element pgSz = sectPr.addElement("w:pgSz");
		pgSz.addAttribute("w:w", Unit.convertCmToTwip(sectBean.getWidth()));
		pgSz.addAttribute("w:h", Unit.convertCmToTwip(sectBean.getHeight()));
		//<w:pgSz w:w="16840" w:h="11907" w:orient="landscape"/>
		//解决答应问题设置流程图横向
		if(sectBean.isHorizontal()){
			pgSz.addAttribute("w:orient","landscape");
		}
		
		// 设置当前节的页码从多少开始
		if (sectBean.getStartNum() > 0) {
			sectPr.addElement("w:pgNumType").addAttribute("w:start",
					sectBean.getStartNum() + "");
		}
		// 上 右 下 左 页眉 页脚 装订线 距离纸张边缘的距离
		Element pgMar = sectPr.addElement("w:pgMar");
		pgMar.addAttribute("w:top", Unit.convertCmToTwip(sectBean.getTop()));
		pgMar
				.addAttribute("w:right", Unit.convertCmToTwip(sectBean
						.getRight()));
		pgMar.addAttribute("w:bottom", Unit.convertCmToTwip(sectBean
				.getBottom()));
		pgMar.addAttribute("w:left", Unit.convertCmToTwip(sectBean.getLeft()));
		pgMar.addAttribute("w:header", Unit.convertCmToTwip(sectBean
				.getHeader()));
		pgMar.addAttribute("w:footer", Unit.convertCmToTwip(sectBean
				.getFooter()));
		pgMar.addAttribute("w:gutter", Unit.convertCmToTwip(sectBean
				.getGutter()));

		// 文档网格
		DocGrid grid = sectBean.getDocGrid();
		if (grid != null) {
			Element gridEle = sectPr.addElement("w:docGrid");
			gridEle.addAttribute("w:type", grid.getGridType().getValue());
			if (grid.getGridType() == DocGridType.LINES) { // 行网格
				gridEle.addAttribute("w:line-pitch", String.valueOf(grid
						.getLinePitch() * 20));
			} else if (grid.getGridType() == DocGridType.LINESANDCHARTS) {
				gridEle.addAttribute("w:line-pitch", String.valueOf(grid
						.getLinePitch() * 20));
				// 计算每行字符数
				gridEle.addAttribute("w:char-space", String.valueOf((grid
						.getCharSpace() - 10) * 4096));
			}
		}
	}
}
