package epros.designer.gui.error;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.swing.JScrollBar;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.designer.system.IJecnConfigItemAciton;
import com.jecn.epros.server.bean.file.FileInfoBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessArchitectureDescriptionBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.publish.PublicFileRequiredDialog;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.draw.constant.JecnToolBoxConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.shape.ErrorPanel;
import epros.draw.gui.line.JecnPageSetLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;

/**
 * 流程面板错误信息验证提示
 * 
 * @author ZHANGXH
 * @date： 日期：Oct 17, 2012 时间：4:40:30 PM
 */
public enum ErrorCommon {
	INSTANCE;
	private Logger log = Logger.getLogger(ErrorCommon.class);

	/** 压线执行滚动条移动 true：执行过 */
	private boolean isScrollBarMove = false;

	/**
	 * 获取错误信息从显示到页面
	 * 
	 * @param errorMessage
	 *            记录错误信息
	 * @param isSave
	 *            是否点击保存
	 * @return true 存在错误信息
	 */
	public boolean getErrorFigure(boolean isSave) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return false;
		}
		if (!"partMap".endsWith(desktopPane.getFlowMapData().getMapType().toString())) {// 当前面板不是流程图
			return false;
		}

		isScrollBarMove = false;
		// 记录读取配置文件流程图属性值
		ErrorTempBean errorTempBean = new ErrorTempBean();
		// 流程面板错误信息记录
		/** *************读取任务的“流程图建设规范”配置************** */
		IJecnConfigItemAciton configAciton = ConnectionPool.getConfigAciton();
		// 各阶段审批人显示项 typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置
		List<JecnConfigItemBean> itemBeanList = configAciton.selectShowPartMapStand();
		for (JecnConfigItemBean itemBean : itemBeanList) {
			if (!DrawCommon.isNullOrEmtryTrim(itemBean.getValue())) {
				int intValue = Integer.valueOf(itemBean.getValue());
				if ("standRoleNumberMax".equals(itemBean.getMark())) {
					// 角色上限
					errorTempBean.setRoleNumberMax(intValue);
				} else if ("standRoleNumberMin".equals(itemBean.getMark())) {
					// 角色下限
					errorTempBean.setRoleNumberMin(intValue);
				} else if ("standActivityNumberMax".equals(itemBean.getMark())) {
					// 活动上限
					errorTempBean.setActivityNumberMax(intValue);
				} else if ("standActivityNumberMin".equals(itemBean.getMark())) {
					// 活动下限
					errorTempBean.setActivityNumberMin(intValue);
				} else if ("standReworkNumber".equals(itemBean.getMark())) {
					// 正反返工符数量对应
					errorTempBean.setReworkNumber(intValue);
				} else if ("standMainPositionOnly".equals(itemBean.getMark())) {
					// 主责岗位唯一
					errorTempBean.setMainPosition(intValue);
				} else if ("figureExistLine".equals(itemBean.getMark())) {
					// 图形是否存在连接线
					errorTempBean.setFigureExistLine(intValue);
				} else if (ConfigItemPartMapMark.standActivitysExistsInAndOutLine.toString().equals(itemBean.getMark())) {// 判断活动是否必须存在一进一出连接线
					errorTempBean.setRhombusExistLine(intValue);
				} else if ("standExistRoleAndActivites".equals(itemBean.getMark())) {
					// 角色活动是否对应
					errorTempBean.setExistRoleAndActivites(intValue);
				} else if ("standActivityNumberNotEmpty".equals(itemBean.getMark())) {
					// 活动编号
					errorTempBean.setActivityNumber(intValue);
				} else if ("standActivityNameNotEmpty".equals(itemBean.getMark())) {
					// 活动名称
					errorTempBean.setActivityName(intValue);
				} else if ("standActivityInfoNotEmpty".equals(itemBean.getMark())) {
					// 活动说明
					errorTempBean.setActivityShow(intValue);
				} else if ("standRoleInfoNotEmpty".equals(itemBean.getMark())) {
					// 角色职责
					errorTempBean.setRoleResponsibilities(intValue);
				} else if ("standRoleExistPosition".equals(itemBean.getMark())) {
					// 角色是否存在岗位
					errorTempBean.setRoleExistPosition(intValue);
				} else if ("standKeyActivityInfoNotEmpty".equals(itemBean.getMark())) {
					// 角色是否存在岗位
					errorTempBean.setKeyActiveShow(intValue);
				} else if (ConfigItemPartMapMark.standCustomRoleNotEmpty.toString().equals(itemBean.getMark())) {
					// 客户是否必填岗位或岗位组
					errorTempBean.setCustomMustChosePost(intValue);
				} else if ("standActivityInputNotEmpty".equals(itemBean.getMark())) {
					// 活动输入
					errorTempBean.setActivityInput(intValue);
				} else if ("standActivityOutPutNotEmpty".equals(itemBean.getMark())) {
					// 活动输出
					errorTempBean.setActivityOutPut(intValue);
				}
			}
		}

		/** 主责岗位计数 */
		int mainRole = 0;
		CheckErrorData checkErrorData = new CheckErrorData();
		// 活动是否存在连接线
		checkErrorData.setIsExistLine(errorTempBean.getRhombusExistLine());
		// 获取面板上所有的活动和角色、活动对应泳池
		TaskTool.findActiveListAndRoleList(checkErrorData);
		// 获取角色活动关联
		TaskTool.setActiveIdForRoleFigure(checkErrorData.getListAllActiveFigure(), checkErrorData
				.getListAllRoleFigure());
		// 获取客户可以对应活动，也可以不对应关联
		TaskTool.setActiveIdForRoleFigure(checkErrorData.getListAllActiveFigure(), checkErrorData
				.getListAllCustomFigure());

		boolean falg = false;
		// 记录返入返工符集合
		Set<String> listReverseArrowhead = new HashSet<String>();
		// 记录返出返工符集合
		Set<String> listRightArrowhead = new HashSet<String>();

		for (int i = 0; i < desktopPane.getPanelList().size(); i++) {
			JecnBaseFlowElementPanel elementPanel = desktopPane.getPanelList().get(i);
			switch (elementPanel.getFlowElementData().getMapElemType()) {
			case ReverseArrowhead:// 正反返工符
				// 数量计算
				listReverseArrowhead.add(elementPanel.getFlowElementData().getFigureText());
				break;
			case RightArrowhead:// 返出 25
				listRightArrowhead.add(elementPanel.getFlowElementData().getFigureText());
				break;
			case PartitionFigure:// 阶段分隔符
				break;
			default:
				if (elementPanel instanceof JecnBaseFigurePanel) {
					// 获取其他元素状态
					int type = setErrorPanel((JecnBaseFigurePanel) elementPanel, errorTempBean, isSave, checkErrorData);
					if (!falg && type == 1) {// 存在错误信息
						falg = true;
					} else if (type == 2) {// 存在主责岗位
						// 主责岗位唯一判断
						mainRole++;
					}
				}
				break;
			}
		}
		// 角色活动上线、下线
		StringBuffer strBiffer = new StringBuffer();

		// 角色和活动上限和下限判断
		boolean isRoleAndActiveLimit = roleAndActiveLimit(strBiffer, errorTempBean, checkErrorData
				.getListAllRoleFigure().size(), checkErrorData.getListAllActiveFigure().size());
		if (isRoleAndActiveLimit) {
			falg = isRoleAndActiveLimit;
		}
		// 返工符数量不同或返工符名称不匹配
		if (errorTempBean.getReworkNumber() == 1) {
			// 返工符存在添加返工符提示信息
			String strReverse = isMatchReverse(listReverseArrowhead, listRightArrowhead);
			if (!DrawCommon.isNullOrEmtryTrim(strReverse)) {
				strBiffer.append(strReverse + "\n");
				// 返工符数量不匹配或名称不匹配
				falg = true;
			}
		}
		if (errorTempBean.getMainPosition() == 1 && mainRole > 1) {// 主责岗位唯一
			strBiffer.append(ErrorConstants.MAIN_POSITION + "\n");
			falg = true;
		}
		if (errorTempBean.getMainPosition() == 1 && mainRole == 0) {// 缺少主责岗位
			strBiffer.append(ErrorConstants.missing_main_position + "\n");
			falg = true;
		}

		/******* 显示分页符，存在压线 ******/
		if (isCoverLinePageSetLine()) {// 显示分页符，存在压线
			strBiffer.append(ErrorConstants.PAGE_SET_COVER_LINE + "\n");
			falg = true;
		}
		if (!isSave && falg && strBiffer.length() > 0) {
			ErrorPanel errorPanel = new ErrorPanel();
			String pText = strBiffer.toString();
			String[] splitStrings = pText.split("\n");
			if (splitStrings.length > 1) {
				errorPanel.setSize(errorPanel.getWidth() + 100, errorPanel.getHeight() + 15 * splitStrings.length);
			}
			errorPanel.setText(JecnTool.dropChars(strBiffer.toString(), 1));
			// errorPanel.setFont(font)
			errorPanel.setFont(new Font(JecnProperties.getValue("songTi"), Font.PLAIN, 14));
			errorPanel.setBackground(Color.red);
			errorPanel.setLocation(200, 80);
			errorPanel.repaintPanel();
		} else if (falg && desktopPane.getErrorPanelList().size() > 0) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("isErrModify"));// 存在错误信息，请修改！
		}
		if (desktopPane.getErrorPanelList().size() > 0) {// 存在错误信息清空，返回
			return true;
		}
		return falg;

	}

	/**
	 * 角色和活动上限和下限判断
	 * 
	 * @param strBiffer
	 *            组装错误信息
	 * @param errorTempBean
	 *            流程建设规范限制项
	 * @param roleSize
	 *            面板角色个数
	 * @param activeSize
	 *            面板活动个数
	 * @return true 存在错误信息
	 */
	public boolean roleAndActiveLimit(StringBuffer strBiffer, ErrorTempBean errorTempBean, int roleSize, int activeSize) {
		// 获取流程基本信息配置
		List<JecnConfigItemBean> listItemBean = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(1, 10);
		int roleMax = 0;
		int roleMin = 0;
		int activeMax = 0;
		int activeMin = 0;
		for (JecnConfigItemBean itemBean : listItemBean) {
			if (DrawCommon.isNullOrEmtryTrim(itemBean.getValue())) {
				continue;
			}
			if (itemBean.getMark().equals(ConfigItemPartMapMark.basicRoleMax.toString())) {// 角色上线
				roleMax = Integer.valueOf(itemBean.getValue());
			} else if (itemBean.getMark().equals(ConfigItemPartMapMark.basicRoleMin.toString())) {// 角色下线
				roleMin = Integer.valueOf(itemBean.getValue());
			} else if (itemBean.getMark().equals(ConfigItemPartMapMark.basicRoundMax.toString())) {// 活动上线
				activeMax = Integer.valueOf(itemBean.getValue());
			} else if (itemBean.getMark().equals(ConfigItemPartMapMark.basicRoundMin.toString())) {// 活动下线
				activeMin = Integer.valueOf(itemBean.getValue());
			}
		}
		boolean falg = false;
		// 角色上限
		if (errorTempBean.getRoleNumberMax() == 1 && roleSize > roleMax) {
			strBiffer.append(ErrorConstants.ROLE_MAX_COUNT + "\n");
			falg = true;
		}
		// 角色下限
		if (errorTempBean.getRoleNumberMin() == 1 && roleSize < roleMin) {
			strBiffer.append(ErrorConstants.ROLE_MIN_COUNT + "\n");
			falg = true;
		}
		// 活动上限
		if (errorTempBean.getActivityNumberMax() == 1 && activeSize > activeMax) {
			strBiffer.append(ErrorConstants.ACTIVE_MAX_COUNT).append("\n");
			falg = true;
		}
		// 活动下限
		if (errorTempBean.getActivityNumberMin() == 1 && activeSize < activeMin) {
			strBiffer.append(ErrorConstants.ACTIVE_MIN_COUNT + "\n");
			falg = true;
		}
		return falg;
	}

	/**
	 * 验证正反返工符数目、名称是否匹配
	 * 
	 * @param listReverseArrowhead
	 *            返入元素集合
	 * @param listRightArrowhead
	 *            反出元素集合
	 * @return 返入或反出 错误信息
	 */
	private String isMatchReverse(Set<String> listReverseArrowhead, Set<String> listRightArrowhead) {
		if (listReverseArrowhead == null || listRightArrowhead == null) {
			return null;
		} else if (listReverseArrowhead != null && listRightArrowhead == null) {
			return ErrorConstants.ARROW_HEAD + "\n";
		} else if (listReverseArrowhead == null && listRightArrowhead != null) {
			return ErrorConstants.ARROW_HEAD + "\n";
		}
		StringBuffer strBuffer = new StringBuffer();

		// if (listReverseArrowhead.size() != listRightArrowhead.size()) {//
		// 正反返工符数目不同
		// strBuffer.append(ErrorConstants.ARROW_HEAD + "\n");
		// return strBuffer.toString();
		// }

		for (String reverseArrowName : listReverseArrowhead) {// 返入集合
			if (DrawCommon.isNullOrEmtryTrim(reverseArrowName)) {
				// 返工符名称不能为空,请修改
				strBuffer.append(ErrorConstants.ARROW_HEAD_NAME_NULL + "\n");
				return strBuffer.toString();
			}
		}

		for (String rightArrowName : listRightArrowhead) {// 返入集合
			if (DrawCommon.isNullOrEmtryTrim(rightArrowName)) {
				// 返工符名称不能为空,请修改
				strBuffer.append(ErrorConstants.ARROW_HEAD_NAME_NULL + "\n");
				return strBuffer.toString();
			}
		}
		String str1 = validateArrowHead(listReverseArrowhead, listRightArrowhead);
		String str2 = validateArrowHead(listRightArrowhead, listReverseArrowhead);
		if (StringUtils.isNotBlank(str1)) {
			strBuffer.append(str1);
		}
		if (StringUtils.isNotBlank(str2)) {
			strBuffer.append(str2);
		}
		return strBuffer.toString();
	}

	/**
	 * 返入反出 是否匹配验证
	 * 
	 * @param listReverseArrowhead
	 * @param listRightArrowhead
	 * @return error 提示字符串
	 */
	private String validateArrowHead(Set<String> listReverseArrowhead, Set<String> listRightArrowhead) {
		StringBuffer strBuffer = new StringBuffer();
		for (String reverseArrowName : listReverseArrowhead) {// 返入集合
			if (DrawCommon.isNullOrEmtryTrim(reverseArrowName)) {
				continue;
			}
			boolean isExit = false;
			for (String rightArrowName : listRightArrowhead) {// 返出集合
				if (DrawCommon.isNullOrEmtryTrim(rightArrowName)) {
					continue;
				} else if (checkStringSame(reverseArrowName, rightArrowName)) {// 返工符名称不匹配,请修改
					isExit = true;
					break;
				}
			}
			if (!isExit) {
				strBuffer.append(JecnProperties.getValue("nameC") + reverseArrowName + " ").append(
						ErrorConstants.ARROW_HEAD + "\n");
			}
		}
		return strBuffer.toString();
	}

	/**
	 * 不是返工符和返工符外的其他元素 是否满足流程元素配置项要求
	 * 
	 * @param curFigure
	 * 
	 * @param errorTempBean
	 *            ErrorTempBean记录流程图属性规范
	 * @param isSave
	 *            是否点击保存 True 点击保存不显示错误信息
	 * @return int 1:图形不存在连接线；
	 */
	public int setErrorPanel(JecnBaseFigurePanel curFigure, ErrorTempBean errorTempBean, boolean isSave,
			CheckErrorData checkErrorData) {

		// 返回值
		int falg = 0;
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || curFigure == null) {
			return falg;
		}
		// 创建错误信息显示panel
		ErrorPanel errorPanel = null;

		if (!isSave) {// 不是点击保存显示错误信息
			errorPanel = new ErrorPanel();
		}

		// 获取图形元素map标识
		MapElemType mapElemType = curFigure.getFlowElementData().getMapElemType();
		/**
		 * 判断图形是否压线
		 */
		boolean isCover = TaskTool.checkFigureCoverLine(curFigure);

		// 活动错误信息处理
		if (DrawCommon.isActive(mapElemType)) {// 活动
			falg = errorActiveFigure(curFigure, errorTempBean, errorPanel, isCover, checkErrorData);
		} else if (DrawCommon.isRole(mapElemType)) {
			// 角色信息特殊处理
			falg = errorRoleFigure(curFigure, errorTempBean, errorPanel, isCover);
		}
		// 记录第一个压线图形的点
		Point coverPoint = null;
		if (errorPanel != null) {
			if (!DrawCommon.isNullOrEmtryTrim(errorPanel.getText())) {
				int width = curFigure.getLocation().x + (int) (50 * desktopPane.getWorkflowScale());
				int height = curFigure.getLocation().y + curFigure.getHeight()
						+ (int) (10 * desktopPane.getWorkflowScale());
				errorPanel.setLocation(width, height);
				if (coverPoint == null) {// 记录错误点的第一个元素坐标
					coverPoint = curFigure.getLocation();
				}
				errorPanel.repaintPanel();
			}
		}
		if (coverPoint != null) {
			// 压线处理 滚动条移动当前压线图形
			errorFigureScrollBarMove(coverPoint, curFigure.getSize());
		}
		return falg;
	}

	/**
	 * 错误元素-滚动条定位
	 * 
	 * @param errorFigurePoint
	 * @param figureSize
	 *            选中的当前图形大小
	 */
	private void errorFigureScrollBarMove(Point errorFigurePoint, Dimension figureSize) {
		/** 横向滚动条 */
		JScrollBar hBar = null;
		/** 纵向滚动条 */
		JScrollBar vBar = null;
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (isScrollBarMove || errorFigurePoint == null || desktopPane == null) {
			return;
		}

		// 执行过压线滚动条移动算法
		isScrollBarMove = true;
		// 获取横向滚动条
		hBar = desktopPane.getScrollPanle().getHorizontalScrollBar();
		// 获取横向滚动条
		vBar = desktopPane.getScrollPanle().getVerticalScrollBar();
		// 横向滚动条宽度
		int right = DrawCommon.convertDoubleToInt(desktopPane.getScrollPanle().getViewportBorderBounds().getWidth());
		// 纵向滚动条值 + 纵向滚动条高度
		int bottom = DrawCommon.convertDoubleToInt(desktopPane.getScrollPanle().getViewportBorderBounds().getHeight());
		if (errorFigurePoint.x >= right) {// 如果连接线结束点横坐标大于等于横向滚动条边界值
			hBar.setValue(errorFigurePoint.x - right / 2);
		} else {
			hBar.setValue(errorFigurePoint.x - figureSize.width);
		}
		if (errorFigurePoint.y >= bottom) {// 如果连接线结束点横坐标大于等于纵向向滚动条边界值
			vBar.setValue(errorFigurePoint.y - bottom / 2);
		} else {
			vBar.setValue(errorFigurePoint.y - figureSize.height);
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}

	/**
	 * 验证活动是否符合流程图属性规范
	 * 
	 * @param curFigure
	 *            待处理流程元素对象
	 * @param errorTempBean
	 *            记录流程图属性规范
	 * @param errorPanel
	 *            带显示的错误信息panel
	 * @param isCover
	 *            true 图形压线
	 * @return 1：存在错误信息
	 */
	public int errorActiveFigure(JecnBaseFigurePanel curFigure, ErrorTempBean errorTempBean, ErrorPanel errorPanel,
			boolean isCover, CheckErrorData checkErrorData) {
		JecnBaseActiveFigure rectAndRhombus = (JecnBaseActiveFigure) curFigure;
		StringBuffer buffer = new StringBuffer();
		int falg = 0;

		if (valFigureByLine(curFigure, checkErrorData, errorTempBean.getRhombusExistLine()) == 1) {// 活动是否存在连接线
			buffer.append(ErrorConstants.FIGURE_NO_LINE).append("\n");// 图形元素缺少连接线,请添加
			falg = 1;
		}
		/**
		 * 图形是否压线
		 */
		if (isCover) {
			buffer.append(ErrorConstants.ISCOVER).append("\n");// 图形压线,请修改
			falg = 1;
		}

		if (errorTempBean.getActivityNumber() == 1
				&& (rectAndRhombus.getFlowElementData().getActivityNum() == null || "".equals(rectAndRhombus
						.getFlowElementData().getActivityNum().trim()))) {// 活动编号
			buffer.append(ErrorConstants.ACTIVE_NUMBER).append("\n");// 活动编号不能为空,请修改
			falg = 1;
		}
		if (errorTempBean.getActivityName() == 1
				&& (rectAndRhombus.getFlowElementData().getFigureText() == null || "".equals(rectAndRhombus
						.getFlowElementData().getFigureText().trim()))) {// 活动名称
			buffer.append(ErrorConstants.ACTIVE_NANE).append("\n");// 活动名称不能为空,请修改
			falg = 1;
		}
		if (rectAndRhombus.getFlowElementData().getMapElemType() != JecnToolBoxConstant.MapElemType.ActivityOvalSubFlowFigure
				&& rectAndRhombus.getFlowElementData().getMapElemType() != JecnToolBoxConstant.MapElemType.ActivityImplFigure) {
			if (JecnConfigTool.useNewInout()) {
				if (errorTempBean.getActivityInput() == 1
						&& (rectAndRhombus.getFlowElementData().getListFigureInTs() == null || rectAndRhombus
								.getFlowElementData().getListFigureInTs().isEmpty())) {// 活动输入
					buffer.append(ErrorConstants.ACTIVE_INPUT).append("\n");// 活动名称不能为空,请修改
					falg = 1;
				}
				if (errorTempBean.getActivityOutPut() == 1
						&& (rectAndRhombus.getFlowElementData().getListFigureOutTs() == null || rectAndRhombus
								.getFlowElementData().getListFigureOutTs().isEmpty())) {// 活动输出新版
					buffer.append(ErrorConstants.ACTIVE_OUTPUT).append("\n");// 活动名称不能为空,请修改
					falg = 1;
				}
			} else {
				boolean inFlag = false;
				if (rectAndRhombus.getFlowElementData().getListJecnActivityFileT() != null) {
					List<JecnActivityFileT> listJecnActivityFileT = rectAndRhombus.getFlowElementData()
							.getListJecnActivityFileT();
					for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
						if (jecnActivityFileT.getFileType() == 0) {
							inFlag = true;
						}
					}
				}
				if (errorTempBean.getActivityInput() == 1
						&& (rectAndRhombus.getFlowElementData().getActivityInput() == null || "".equals(rectAndRhombus
								.getFlowElementData().getActivityInput().trim())
								&& !inFlag)) {// 活动输入]
					buffer.append(ErrorConstants.ACTIVE_INPUT).append("\n");// 活动名称不能为空,请修改
					falg = 1;
				}
				boolean outFlag = true;
				if (rectAndRhombus.getFlowElementData().getListModeFileT() == null
						|| rectAndRhombus.getFlowElementData().getListModeFileT().isEmpty()) {
					outFlag = false;
				}
				if (errorTempBean.getActivityOutPut() == 1
						&& (rectAndRhombus.getFlowElementData().getActivityOutput() == null || "".equals(rectAndRhombus
								.getFlowElementData().getActivityOutput().trim())
								&& !outFlag)) {// 活动输出
					buffer.append(ErrorConstants.ACTIVE_OUTPUT).append("\n");// 活动名称不能为空,请修改
					falg = 1;
				}
			}
		}

		if (errorTempBean.getActivityShow() == 1
				&& (rectAndRhombus.getFlowElementData().getAvtivityShow() == null || "".equals(rectAndRhombus
						.getFlowElementData().getAvtivityShow().trim()))) {// 活动说明
			buffer.append(ErrorConstants.ACTIVE_SHOW).append("\n");// 活动说明不能为空,请修改
			falg = 1;
		}
		if (errorTempBean.getKeyActiveShow() == 1
				&& !DrawCommon.isNullOrEmtryTrim(rectAndRhombus.getFlowElementData().getActiveKeyType())
				&& DrawCommon.isNullOrEmtryTrim(rectAndRhombus.getFlowElementData().getAvtivityShowAndControl())) {// 存在关键活动时，关键活动说明不能为空
			buffer.append(ErrorConstants.KEY_ACTIVE_SHOW).append("\n");// 关键说明不能为空,请修改
			falg = 1;
		}
		if (errorTempBean.getExistRoleAndActivites() == 1 && !rectAndRhombus.getFlowElementData().isRefRole()) {// 角色活动是否对应
			buffer.append(ErrorConstants.ROLE_HASH_ACTIVE).append("\n");// 存在角色、活动没有对应,请修改
			falg = 1;
		}
		if (errorPanel != null && falg == 1) {
			String pText = buffer.toString();
			String[] splitStrings = pText.split("\n");
			if (splitStrings.length > 1) {
				errorPanel.setSize(errorPanel.getWidth(), errorPanel.getHeight() + 15 * splitStrings.length);
			}
			if (buffer.length() > 0) {
				errorPanel.setText(JecnTool.dropChars(buffer.toString(), 1));
			}
		}
		return falg;
	}

	/**
	 * 验证角色是否符合流程图属性规范
	 * 
	 * @param curFigure
	 *            待处理流程元素对象
	 * @param errorTempBean
	 *            记录流程图属性规范
	 * @param errorPanel
	 *            带显示的错误信息panel
	 * @param isCover
	 *            true 图形压线
	 * @return 1：存在错误信息；2：主责岗位
	 */
	public int errorRoleFigure(JecnBaseFigurePanel curFigure, ErrorTempBean errorTempBean, ErrorPanel errorPanel,
			boolean isCover) {
		// 获取图形元素map标识
		MapElemType mapElemType = curFigure.getFlowElementData().getMapElemType();
		// 获取角色对象
		JecnBaseRoleFigure roleFigure = (JecnBaseRoleFigure) curFigure;

		StringBuffer buffer = new StringBuffer();
		int falg = 0;
		if ((roleFigure.getFlowElementData().getRoleRes() == null || "".equals(roleFigure.getFlowElementData()
				.getRoleRes().trim()))
				&& errorTempBean.getRoleResponsibilities() == 1) {// 角色职责
			buffer.append(ErrorConstants.ROLE_RULES).append("\n");
			falg = 1;
		}
		/**
		 * 图形是否压线
		 */
		if (isCover) {
			buffer.append(ErrorConstants.ISCOVER).append("\n");
			falg = 1;
		}

		if (!"CustomFigure".equals(mapElemType.toString())) {// 不是客户，是虚拟角色或角色时
			if ((errorTempBean.getExistRoleAndActivites() == 1 && roleFigure.getFlowElementData().isMoreRefActive())
					|| (errorTempBean.getExistRoleAndActivites() == 1 && (!roleFigure.getFlowElementData()
							.isRefActive()))) {// 角色活动是否对应
				buffer.append(ErrorConstants.ROLE_HASH_ACTIVE).append("\n");
				falg = 1;
			}
		}
		if (mapElemType == DrawCommon.getCreateFlowRoleType()) {// 角色
			falg = rolePostisNotEmpty(roleFigure, errorTempBean, buffer, falg);
			if ("4".equals(roleFigure.getFlowElementData().getActivityId())) {
				falg = 2;
			}
		}
		if ("CustomFigure".equals(mapElemType.toString())) {
			falg = customPostNotEmpty(roleFigure, errorTempBean, buffer, falg);
		}
		if (errorPanel != null && falg != 0) {
			if (buffer.length() > 0) {
				String pText = buffer.toString();
				String[] splitStrings = pText.split("\n");
				if (splitStrings.length > 1) {
					errorPanel.setSize(errorPanel.getWidth(), errorPanel.getHeight() + 15 * splitStrings.length);
				}
				errorPanel.setText(JecnTool.dropChars(buffer.toString(), 1));
			}
		}
		return falg;
	}

	private int rolePostisNotEmpty(JecnBaseRoleFigure roleFigure, ErrorTempBean errorTempBean, StringBuffer buffer,
			int falg) {
		if ((roleFigure.getFlowElementData().getFlowStationList() == null || roleFigure.getFlowElementData()
				.getFlowStationList().size() == 0)
				&& errorTempBean.getRoleExistPosition() == 1) {// 缺少岗位限制条件,
			buffer.append(
					ErrorConstants.ROLE_TEXT + roleFigure.getFlowElementData().getFigureText()
							+ ErrorConstants.ROLE_NO_POSITION).append("\n");
			falg = 1;
		}
		return falg;
	}

	private int customPostNotEmpty(JecnBaseRoleFigure roleFigure, ErrorTempBean errorTempBean, StringBuffer buffer,
			int falg) {
		if ((roleFigure.getFlowElementData().getFlowStationList() == null || roleFigure.getFlowElementData()
				.getFlowStationList().size() == 0)
				&& errorTempBean.getCustomMustChosePost() == 1) {// 缺少岗位限制条件,
			buffer.append(roleFigure.getFlowElementData().getFigureText() + ErrorConstants.ROLE_NO_POSITION).append(
					"\n");
			falg = 1;
		}
		return falg;
	}

	/**
	 * 元素是否存在连接线
	 * 
	 * @param curFigure
	 * @return
	 */
	public int valFigureByLine(JecnBaseFigurePanel curFigure, CheckErrorData checkErrorData, int rhombusExistLine) {
		if (rhombusExistLine != 1) {// 决策框是否存在连接线
			return 0;
		}
		if (DrawCommon.isActive(curFigure.getFlowElementData().getMapElemType())) {// 活动是否存在连接线
			if (!TaskTool.isExistManLine(curFigure, checkErrorData)) {
				return 1;
			}
		}
		return 0;
	}

	/**
	 * 清理错误提示
	 */
	public void clearErrorPanel() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || desktopPane.getErrorPanelList().size() == 0) {
			return;
		}
		for (ErrorPanel panel : desktopPane.getErrorPanelList()) {
			desktopPane.remove(panel);
		}
		desktopPane.getErrorPanelList().clear();
		desktopPane.updateUI();
	}

	/**
	 * 流程文控信息检查
	 * 
	 * @return boolean true 存在错误信息，false 无错误信息
	 * 
	 */
	public boolean checkControlError() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return true;
		} else if (desktopPane.getErrorPanelList().size() > 0) {
			clearErrorPanel();
		}
		Long flowId = desktopPane.getFlowMapData().getFlowId();
		JecnFlowStructureT flowStructureT = null;
		boolean isError = getErrorFigure(false);// 发布时提示信息
		// 流程文件 必填项信息验证
		try {
			flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);
			List<String> strList = getStrList();

			if (strList.size() > 0) {
				PublicFileRequiredDialog requiredDialog = new PublicFileRequiredDialog(strList);
				requiredDialog.setVisible(true);
				if (!isError) {
					isError = true;
				}
			}
			if (flowStructureT.getErrorState() == 0 && !isError) {// 存在错误信息
				// 0:错误 1:正确
				flowStructureT.setErrorState(1);
				ConnectionPool.getProcessAction().updateJecnFlowStructureT(flowStructureT);
				// // 系统配置信息已变更，错误验证信息不匹配已修改！
				// JecnOptionPane.showMessageDialog(null,
				// "系统配置信息已变更，错误验证信息不匹配已修改！");
				return false;
			}
		} catch (Exception e1) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			log.error("ErrorCommon.checkControlError is error！", e1);
		}
		return isError;
	}

	/**
	 * 流程文件文控信息检查
	 * 
	 * @return boolean true 存在错误信息，false 无错误信息
	 * 
	 */
	public boolean checkControlErrorProcessFile(JecnTreeNode selectNode) {
		Long flowId = selectNode.getJecnTreeBean().getId();
		if (flowId == null) {
			return false;
		}
		JecnFlowStructureT flowStructureT = null;
		// 流程文件 必填项信息验证
		try {
			flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);
			List<String> strList = getStrList(flowId);

			if (strList.size() > 0) {
				PublicFileRequiredDialog requiredDialog = new PublicFileRequiredDialog(strList);
				requiredDialog.setVisible(true);
				return true;
			}
			if (flowStructureT.getErrorState() == 0) {// 存在错误信息
				// 0:错误 1:正确
				flowStructureT.setErrorState(1);
				ConnectionPool.getProcessAction().updateJecnFlowStructureT(flowStructureT);
				// // 系统配置信息已变更，错误验证信息不匹配已修改！
				// JecnOptionPane.showMessageDialog(null,
				// "系统配置信息已变更，错误验证信息不匹配已修改！");
				return false;
			}
		} catch (Exception e1) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			log.error("ErrorCommon.checkControlError is error！", e1);
		}
		return false;
	}

	/**
	 * 流程架构文控信息检查
	 * 
	 * @return boolean true 存在错误信息，false 无错误信息
	 * 
	 */
	public boolean checkflowMapControlError(Long flowId) {
		List<String> strList = getFlowMapCheckErrorList(flowId);
		if (strList.size() == 0) {
			return false;
		}
		PublicFileRequiredDialog requiredDialog = new PublicFileRequiredDialog(strList);
		requiredDialog.setVisible(true);
		return true;
	}

	/**
	 * 制度错误信息监测
	 * 
	 * @param ruleId
	 * @return
	 */
	public boolean checkRuleBaseInfo(Long ruleId) {
		// 验证制度必填
		List<String> strList = getRuleCheckErrorList(ruleId);
		if (strList.size() == 0) {
			return false;
		}
		PublicFileRequiredDialog requiredDialog = new PublicFileRequiredDialog(strList);
		requiredDialog.setVisible(true);
		return true;
	}

	/**
	 * 文件错误信息监测
	 * 
	 * @param ruleId
	 * @return
	 */
	public boolean checkFileBaseInfo(Long ruleId) {
		// 验证制度必填
		List<String> strList = getFileCheckErrorList(ruleId);
		if (strList.size() == 0) {
			return false;
		}
		PublicFileRequiredDialog requiredDialog = new PublicFileRequiredDialog(strList);
		requiredDialog.setVisible(true);
		return true;
	}

	/**
	 * 选中流程不打开面板验证处理
	 * 
	 * @param treeNode
	 * @return true 存在错误信息
	 */
	public boolean checkErrorNoOpenWorlFlow(JecnTreeNode treeNode) {
		if (treeNode == null || !isFlow(treeNode)) {// 树节点不存在或选中的不为树节点
			return false;
		}
		// 流程信息
		JecnFlowStructureT flowStructureT = null;
		Long flowId = treeNode.getJecnTreeBean().getId();
		try {
			flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);

		} catch (Exception e) {
			log.error("", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
		if (flowStructureT == null) {
			throw new IllegalArgumentException("FlowResourceTreeMenu  submitAppItemAction is error！");
		}
		if (flowStructureT.getErrorState() == 0) {// 存在错误数据
			// JecnOptionPane.showMessageDialog(null, JecnProperties
			// .getValue("isErrorMessage"));
			return true;
		}
		return false;
	}

	/**
	 * 选中节点是否为流程
	 * 
	 * @param treeNode
	 *            选中树节点
	 * @return true 选中的为树节点
	 */
	public boolean isFlow(JecnTreeNode treeNode) {
		if (treeNode == null) {// 树节点不存在
			return false;
		}
		TreeNodeType nodeType = treeNode.getJecnTreeBean().getTreeNodeType();
		if (nodeType == TreeNodeType.process) {
			return true;
		}
		return false;
	}

	/**
	 * 获取面板内角色和活动数
	 * 
	 * @return
	 */
	public Map<String, Integer> getActiveAndRoleCount() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return null;
		}
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<JecnBaseFlowElementPanel> list = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();
		int countActive = 0;

		int countRole = 0;

		for (JecnBaseFlowElementPanel flowElementPanel : list) {
			MapElemType mapElemType = flowElementPanel.getFlowElementData().getMapElemType();
			if (DrawCommon.isActive(mapElemType)) {// 属于活动
				countActive++;
			} else if (MapElemType.CustomFigure != mapElemType && DrawCommon.isRole(mapElemType)) {// 角色或虚拟角色
				countRole++;
			}
		}
		map.put("countActive", countActive);
		map.put("countRole", countRole);
		return map;
	}

	/**
	 * 获取系统配置文件错误信息
	 * 
	 * @return List<String>
	 * @throws Exception
	 */
	public List<String> getStrList() throws Exception {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return null;
		}
		int languageType = JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1;
		// 流程文件必填项信息验证
		List<String> strList = ConnectionPool.getDocControlAction().getErrorList(
				desktopPane.getFlowMapData().getFlowId(), languageType);
		// 流程基本信息错误信息验证
		return selectBaseInfoErrorList(strList);
	}

	/**
	 * 获取系统配置文件错误信息
	 * 
	 * @return List<String>
	 * @throws Exception
	 */
	public List<String> getStrList(Long flowId) throws Exception {
		// 流程文件必填项信息验证
		List<String> strList = new ArrayList<String>();
		// 流程基本信息错误信息验证
		return selectProcessFileBaseInfoErrorList(strList, flowId);
	}

	/**
	 * 流程文件基本信息错误信息验证
	 * 
	 * @param strList
	 * @return
	 * @throws Exception
	 */
	public List<String> selectProcessFileBaseInfoErrorList(List<String> strList, Long flowId) throws Exception {
		if (flowId == null) {
			return strList;
		}
		List<JecnConfigItemBean> itemBeanList = ConnectionPool.getConfigAciton().selectBaseInfoFlowFile();

		// "不能为空！"
		String strName = JecnProperties.getValue("chinaColon") + JecnProperties.getValue("isNotEmpty");

		JecnFlowBasicInfoT flowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(flowId);
		// 获得流程主表临时表
		JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);

		for (JecnConfigItemBean confiBean : itemBeanList) {
			if (DrawCommon.isNullOrEmtryTrim(confiBean.getValue())) {
				continue;
			}
			int value = Integer.valueOf(confiBean.getValue()).intValue();
			String error = null;
			if (confiBean.getMark().equals("3")) {// 流程责任人
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowBasicInfoT.getResPeopleId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("4")) {// 流程类别
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty() && flowBasicInfoT.getTypeId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("processShowBussType")) {// 业务类型
				if (confiBean.getValue().equals("1")
						&& confiBean.isNotEmpty()
						&& (flowStructureT.getBussType() == null || "".equals(flowStructureT.getBussType()) || "0"
								.equals(flowStructureT.getBussType()))) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("5")) {// 责任部门
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()) {
					ProcessAttributeBean processAttributeBean = ConnectionPool.getProcessAction().getFlowAttribute(
							flowId);
					if (DrawCommon.isNullOrEmtryTrim(processAttributeBean.getDutyOrgName())) {
						error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}
			} else if (confiBean.getMark().equals("189")) {// 流程编号
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()) {
					if (DrawCommon.isNullOrEmtryTrim(flowStructureT.getFlowIdInput())) {
						error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.guardianPeople.toString())) {// 流程监护人
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowBasicInfoT.getGuardianId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.fictionPeople.toString())) {// 流程拟制人
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowBasicInfoT.getFictionPeopleId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.supportTools.toString())) {// 支持工具
				List<JecnTreeBean> listFlowTool = ConnectionPool.getFlowTool().getSustainToolsByFlowId(flowId);
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& (listFlowTool == null || listFlowTool.size() == 0)) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.enclosure.toString())) {// 附件
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty() && flowBasicInfoT.getFileId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessCommissioner.toString())) {// 专员
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowStructureT.getCommissionerId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessKeyWord.toString())) {// 关键字
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& (flowStructureT.getKeyword() == null || "".equals(flowStructureT.getKeyword()))) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			}
			if (error != null) {
				strList.add(error);
			}
		}
		return strList;
	}

	/**
	 * 流程基本信息错误信息验证
	 * 
	 * @param strList
	 * @return
	 * @throws Exception
	 */
	public List<String> selectBaseInfoErrorList(List<String> strList) throws Exception {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 流程ID
		Long flowId = desktopPane.getFlowMapData().getFlowId();
		if (desktopPane == null || flowId == null) {// 画图面板不存在
			return null;
		}
		List<JecnConfigItemBean> itemBeanList = ConnectionPool.getConfigAciton().selectBaseInfoFlowFile();
		Map<String, Integer> map = getActiveAndRoleCount();
		if (map == null) {
			return strList;
		}
		// 面板内活动数
		int countActive = map.get("countActive");
		// 面板内角色数
		int countRole = map.get("countRole");
		// "不能为空！"
		String strName = JecnProperties.getValue("chinaColon") + JecnProperties.getValue("isNotEmpty");

		JecnFlowBasicInfoT flowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(flowId);
		// 获得流程主表临时表
		JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);

		for (JecnConfigItemBean confiBean : itemBeanList) {
			if (DrawCommon.isNullOrEmtryTrim(confiBean.getValue())) {
				continue;
			}
			int value = Integer.valueOf(confiBean.getValue()).intValue();
			String error = null;
			if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoleMax.toString())) { // 角色上限
				if (countRole > value) {// 角色个数超过上限！
					error = JecnProperties.getValue("basicRoleMax");
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoleMin.toString())) { // 角色下限
				if (countRole < value) {// "角色个数低于下限！"
					error = JecnProperties.getValue("basicRoleMin");
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoundMax.toString())) { // 活动上限
				if (countActive > value) {// "活动个数超过上限！"
					error = JecnProperties.getValue("basicRoundMax");
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoundMin.toString())) { // 活动下限
				if (countActive < value) {// "活动个数低于下限！"
					error = JecnProperties.getValue("basicRoundMin");
				}
			} else if (confiBean.getMark().equals("1")) {// 起始活动
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(flowBasicInfoT.getFlowStartpoint())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("2")) {// 终止活动
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(flowBasicInfoT.getFlowEndpoint())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("3")) {// 流程责任人
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowBasicInfoT.getResPeopleId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("4")) {// 流程类别
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty() && flowBasicInfoT.getTypeId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("processShowBussType")) {// 业务类型
				if (confiBean.getValue().equals("1")
						&& confiBean.isNotEmpty()
						&& (flowStructureT.getBussType() == null || "".equals(flowStructureT.getBussType()) || "0"
								.equals(flowStructureT.getBussType()))) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("5")) {// 责任部门
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()) {
					ProcessAttributeBean processAttributeBean = ConnectionPool.getProcessAction().getFlowAttribute(
							flowId);
					if (DrawCommon.isNullOrEmtryTrim(processAttributeBean.getDutyOrgName())) {
						error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}
			} else if (confiBean.getMark().equals("189")) {// 流程编号
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()) {
					if (DrawCommon.isNullOrEmtryTrim(flowStructureT.getFlowIdInput())) {
						error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.guardianPeople.toString())) {// 流程监护人
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowBasicInfoT.getGuardianId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.fictionPeople.toString())) {// 流程拟制人
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowBasicInfoT.getFictionPeopleId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.supportTools.toString())) {// 支持工具
				List<JecnTreeBean> listFlowTool = ConnectionPool.getFlowTool().getSustainToolsByFlowId(flowId);
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& (listFlowTool == null || listFlowTool.size() == 0)) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.enclosure.toString())) {// 附件
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty() && flowBasicInfoT.getFileId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessCommissioner.toString())) {// 专员
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& flowStructureT.getCommissionerId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessKeyWord.toString())) {// 关键字
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty() && flowStructureT.getKeyword() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			}
			if (error != null) {
				strList.add(error);
			}
		}
		return strList;
	}

	/**
	 * 显示分页符的时候，分页符是否压图形
	 * 
	 * @return
	 */
	private boolean isCoverLinePageSetLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return false;
		}
		if (!desktopPane.getFlowMapData().getPageSetingBean().isShowPageSet()) {// 不显示分页符
			return false;
		}

		boolean isCover = false;
		List<JecnBaseFlowElementPanel> panelList = desktopPane.getPanelList();
		for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : panelList) {
			if (jecnBaseFlowElementPanel instanceof JecnBaseFigurePanel) {
				isCover = checFigureCoverPageSetLine((JecnBaseFigurePanel) jecnBaseFlowElementPanel);
				if (isCover) {// 存在压线，返回
					return isCover;
				}
			}
		}
		return false;
	}

	/**
	 * 判断图形是否压线
	 * 
	 * @param figure
	 *            图形元素
	 * @return true 存在图形压线
	 */
	public boolean checFigureCoverPageSetLine(JecnBaseFigurePanel figure) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return false;
		}
		List<JecnPageSetLine> listPageSetLines = desktopPane.getPageSetLines();
		if (desktopPane.getFlowMapData().isHFlag()) {// 横向图
			for (JecnPageSetLine pageLine : listPageSetLines) {
				if (figure.getX() <= pageLine.getX() && figure.getX() + figure.getWidth() > pageLine.getX()) {
					return true;

				}
			}
		}
		return isScrollBarMove;
	}

	/**
	 * 
	 * 判断字符串是否相等
	 * 
	 * @param str1
	 *            String 字符串1
	 * @param str2
	 *            String 字符串2
	 * @return boolean true：相等 false：不相等
	 */
	private boolean checkStringSame(String str1, String str2) {
		if (DrawCommon.isNullOrEmtryTrim(str1) && DrawCommon.isNullOrEmtryTrim(str2)) {// 都为空
			return true;
		} else if (!DrawCommon.isNullOrEmtryTrim(str1) && str1.trim().equals(str2.trim())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 制度发布，错误信息检测
	 * 
	 * @param ruleId
	 * @return
	 */
	public List<String> getRuleCheckErrorList(Long ruleId) {
		List<String> errorList = new ArrayList<String>();
		// "不能为空！"
		String strName = JecnProperties.getValue("chinaColon") + JecnProperties.getValue("isNotEmpty");
		// 制度模板文件
		getRuleModelFileCheckError(errorList, strName, ruleId);
		// 制度属性
		getRuleBasicInfoCheckError(errorList, ruleId, strName);
		// 制度属性 必填项验证
		return errorList;
	}

	/**
	 * 文件发布，错误信息检测
	 * 
	 * @param ruleId
	 * @return
	 */
	public List<String> getFileCheckErrorList(Long ruleId) {
		List<String> errorList = new ArrayList<String>();
		// "不能为空！"
		String strName = JecnProperties.getValue("chinaColon") + JecnProperties.getValue("isNotEmpty");
		// 文件属性
		getFileBasicInfoCheckError(errorList, ruleId, strName);
		// 文件属性 必填项验证
		return errorList;
	}

	/**
	 * 流程架构发布，错误信息检测
	 * 
	 * @param ruleId
	 * @return
	 */
	public List<String> getFlowMapCheckErrorList(Long flowId) {
		List<String> errorList = new ArrayList<String>();
		// "不能为空！"
		String strName = JecnProperties.getValue("chinaColon") + JecnProperties.getValue("isNotEmpty");
		// 架构卡
		getFlowMapCheckError(errorList, strName, flowId);
		// 制度属性 必填项验证
		return errorList;
	}

	private void getFileBasicInfoCheckError(List<String> errorList, Long fileId, String strName) {
		try {
			FileInfoBean fileBean = ConnectionPool.getFileAction().getFileBean(fileId);
			List<JecnConfigItemBean> itemBeanList = JecnConfigTool.getFileBasicConfigItems();
			if (itemBeanList == null) {
				return;
			}
			for (JecnConfigItemBean itemBean : itemBeanList) {
				String error = null;
				if (isMustWrite(itemBean, ConfigItemPartMapMark.isShowFileKeyWord)) {// 关键字
					if (fileBean.getJecnFileBeanT().getKeyword() == null
							|| StringUtils.isBlank(fileBean.getJecnFileBeanT().getKeyword())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}

				if (isMustWrite(itemBean, ConfigItemPartMapMark.isShowFileCommissioner)) {// 专员
					if (fileBean.getJecnFileBeanT().getCommissionerId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}

				if (isMustWrite(itemBean, ConfigItemPartMapMark.isShowFileNum)) {// 编号
					if (fileBean.getJecnFileBeanT().getDocId() == null
							|| StringUtils.isBlank(fileBean.getJecnFileBeanT().getDocId())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}
				if (isMustWrite(itemBean, ConfigItemPartMapMark.isShowFileResPeople)) {// 责任人
					if (fileBean.getJecnFileBeanT().getResPeopleId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}

				if (isMustWrite(itemBean, ConfigItemPartMapMark.fileScurityLevel)) {// 保密级别
					if (fileBean.getJecnFileBeanT().getConfidentialityLevel() == null
							|| fileBean.getJecnFileBeanT().getConfidentialityLevel() == 0) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}

				if (StringUtils.isNotBlank(error)) {
					errorList.add(error);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getRuleBasicInfoCheckError(List<String> errorList, Long ruleId, String strName) {
		try {
			RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(ruleId);
			List<JecnConfigItemBean> itemBeanList = JecnConfigTool.getRuleBasicConfigItems();
			if (itemBeanList == null) {
				return;
			}
			// 获取前言信息对象
			JecnRuleBaiscInfoT basicInfo = ruleT.getJecnRuleBaiscInfoT();
			boolean isRuleModeFile = true;// ruleT.getIsDir() == 1;
			for (JecnConfigItemBean itemBean : itemBeanList) {
				String error = null;
				if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleGuardianPeople)) {// 监护人
					if (ruleT.getGuardianId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleBusinessScope)) { // 业务范围
					if (StringUtils.isBlank(ruleT.getBusinessScope())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleOrgScope)) { // 组织范围
					if (ruleT.getOrgScopes() == null || ruleT.getOrgScopes().size() == 0) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleOtherScope)) { // 其它范围
					if (StringUtils.isBlank(ruleT.getOtherScope())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleType)) { // 制度类别
					if (ruleT.getTypeId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isRuleModeFile && isMustWrite(itemBean, ConfigItemPartMapMark.rulePurpose)) {// 目的
					if (StringUtils.isBlank(basicInfo.getPurpose())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isRuleModeFile && isMustWrite(itemBean, ConfigItemPartMapMark.ruleApplicability)) {// 适用范围
					if (StringUtils.isBlank(basicInfo.getApplicability())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isRuleModeFile && isMustWrite(itemBean, ConfigItemPartMapMark.ruleRriteDept)) {// 编写部门
					if (StringUtils.isBlank(basicInfo.getWriteDept())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isRuleModeFile && isMustWrite(itemBean, ConfigItemPartMapMark.ruleDraftMan)) {// 起草人
					if (StringUtils.isBlank(basicInfo.getDraftman())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isRuleModeFile && isMustWrite(itemBean, ConfigItemPartMapMark.ruleDraftingDept)) {// 起草单位
					if (StringUtils.isBlank(basicInfo.getDraftingUnit())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isRuleModeFile && isMustWrite(itemBean, ConfigItemPartMapMark.ruleTestRunFile)) {// 试运版文件
					if (StringUtils.isBlank(basicInfo.getTestRunFile())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isRuleModeFile && isMustWrite(itemBean, ConfigItemPartMapMark.ruleChangeVersionExplain)) {// 文件换版说明
					if (StringUtils.isBlank(basicInfo.getChangeVersionExplain())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleResPeople)) {// 责任人
					if (ruleT.getAttchMentId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleResDept)) {// 责任部门
					if (ruleT.getOrgId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleCode)) {// 编号
					if (StringUtils.isBlank(ruleT.getRuleNumber())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.isShowRuleCommissioner)) {// 专员
					if (ruleT.getCommissionerId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.isShowRuleKeyWord)) {// 关键字
					if (StringUtils.isBlank(ruleT.getKeyword())) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleScurityLevel)) {// 关键字
					if (ruleT.getConfidentialityLevel() == null || ruleT.getConfidentialityLevel() == 0) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.isShowEffectiveDate)) {// 生效日期
					if (ruleT.getEffectiveTime() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				} else if (isMustWrite(itemBean, ConfigItemPartMapMark.ruleFictionPeople)) {// 拟制人
					if (ruleT.getFictionPeopleId() == null) {
						error = itemBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}

				if (StringUtils.isNotBlank(error)) {
					errorList.add(error);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isMustWrite(JecnConfigItemBean itemBean, ConfigItemPartMapMark mark) {
		return itemBean.getMark().equals(mark.toString()) && itemBean.getValue().equals("1") && itemBean.isNotEmpty();
	}

	/**
	 * 制度模板 必填项监测
	 * 
	 * @param errorList
	 * @param strName
	 * @param ruleId
	 */
	private void getRuleModelFileCheckError(List<String> errorList, String strName, Long ruleId) {
		List<JecnRuleOperationCommon> ruleModelList = null;
		try {
			ruleModelList = ConnectionPool.getRuleAction().findRuleOperationShow(ruleId);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		if (ruleModelList == null) {
			return;
		}
		// 制度文件
		for (JecnRuleOperationCommon jecnRuleOperationCommon : ruleModelList) {
			String errorStr = null;
			String titleName = jecnRuleOperationCommon.getRuleTitleT().getTitleName(
					JecnResourceUtil.getLocale() == Locale.ENGLISH ? 1 : 0);
			// 标题类型
			int titleType = jecnRuleOperationCommon.getRuleTitleT().getType();
			// 必填项
			int requiredType = jecnRuleOperationCommon.getRuleTitleT().getRequiredType();
			if (requiredType != 1) {
				continue;
			}

			if (titleType == 0
					&& (jecnRuleOperationCommon.getRuleContentT() == null || StringUtils
							.isBlank(jecnRuleOperationCommon.getRuleContentT().getContentStr()))) {
				errorStr = titleName + strName;
			} else if (titleType == 1
					&& (jecnRuleOperationCommon.getListFileBean() == null || jecnRuleOperationCommon.getListFileBean()
							.size() == 0)) {
				errorStr = titleName + strName;
			} else if (titleType == 2
					&& (jecnRuleOperationCommon.getListJecnFlowCommon() == null || jecnRuleOperationCommon
							.getListJecnFlowCommon().size() == 0)) {
				errorStr = titleName + strName;
			} else if (titleType == 3
					&& (jecnRuleOperationCommon.getListJecnFlowMapCommon() == null || jecnRuleOperationCommon
							.getListJecnFlowMapCommon().size() == 0)) {
				errorStr = titleName + strName;
			} else if (titleType == 4
					&& (jecnRuleOperationCommon.getListJecnStandarCommon() == null || jecnRuleOperationCommon
							.getListJecnStandarCommon().size() == 0)) {
				// 标准表单添加
				errorStr = titleName + strName;
			} else if (titleType == 5
					&& (jecnRuleOperationCommon.getListJecnRiskCommon() == null || jecnRuleOperationCommon
							.getListJecnRiskCommon().size() == 0)) {
				errorStr = titleName + strName;
			} else if (titleType == 6
					&& (jecnRuleOperationCommon.getListJecnRuleCommon() == null || jecnRuleOperationCommon
							.getListJecnRuleCommon().size() == 0)) {
				errorStr = titleName + strName;
			}
			if (StringUtils.isNotBlank(errorStr)) {
				errorList.add(errorStr);
			}
		}
	}

	/**
	 * 制度模板 必填项监测
	 * 
	 * @param errorList
	 * @param strName
	 * @param ruleId
	 */
	private void getFlowMapCheckError(List<String> errorList, String strName, Long flowId) {
		ProcessArchitectureDescriptionBean processCord = null;
		try {
			processCord = ConnectionPool.getProcessAction().getProcessArchitectureDescriptionBean(flowId);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		if (processCord == null) {
			return;
		}
		List<JecnConfigItemBean> itemBeanList = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(0, 2);
		for (JecnConfigItemBean confiBean : itemBeanList) {
			String error = null;
			if (DrawCommon.isNullOrEmtryTrim(confiBean.getValue())) {
				continue;
			}
			if (confiBean.getMark().equals("isShowProcessMapNum")) {// 架构卡编号
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getNumId())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("isShowProcessMapSupplier")) {// 架构卡供应商
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getSupplier())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals("isShowProcessMapCustomer")) {// 架构卡客户
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()) {
					if (DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getCustomer())) {
						error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}
			} else if (confiBean.getMark().equals("isShowProcessMapRiskAndOpportunity")) {// 架构卡
				// 风险与编号
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()) {
					if (DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getRiskAndOpportunity())) {
						error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
					}
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapDescription.toString())) {// 架构卡
				// 描述
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getFlowDescription())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapPurpose.toString())) {// 架构卡目的
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getFlowAim())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapResPeople.toString())) {// 流程架构责任人
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty() && processCord.getDutyUserId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapCommissioner.toString())) {// 流程架构专员
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& processCord.getCommissionerId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapKeyWord.toString())) {// 关键字
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getKeyWord())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(
					ConfigItemPartMapMark.isShowProcessMapResponsibleDepartment.toString())) {// 责任部门
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty() && processCord.getDutyOrgId() == null) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapFlowInputInfo.toString())) {// 流程输入
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getFlowInput())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapFlowOutputInfo.toString())) {// 流程输出
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getFlowOutput())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapFlowStart.toString())) {// 流程起始
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getFlowStart())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapFlowEnd.toString())) {// 流程终止流程KPI
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getFlowEnd())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.isShowProcessMapFlowKPI.toString())) {// 流程KPI
				if (confiBean.getValue().equals("1") && confiBean.isNotEmpty()
						&& DrawCommon.isNullOrEmtryTrim(processCord.getJecnMainFlowT().getFlowKpi())) {
					error = confiBean.getName(JecnResourceUtil.getLocale()) + strName;
				}
			}
			if (error != null) {
				errorList.add(error);
			}
		}

	}

	class CheckErrorData {
		// 面板所有的角色元素
		private List<JecnBaseRoleFigure> listAllRoleFigure = new ArrayList<JecnBaseRoleFigure>();
		// 面板所有的活动元素
		private List<JecnBaseActiveFigure> listAllActiveFigure = new ArrayList<JecnBaseActiveFigure>();
		// 面板所有的客户元素集合
		private List<JecnBaseRoleFigure> listAllCustomFigure = new ArrayList<JecnBaseRoleFigure>();

		/** 协作框 */
		private List<JecnBaseFigurePanel> listAllDottedRect = new ArrayList<JecnBaseFigurePanel>();

		/** 协作框包含活动集合的map对象 */
		private Map<JecnBaseFigurePanel, List<JecnBaseFigurePanel>> dottedRectContainsActives = new HashMap<JecnBaseFigurePanel, List<JecnBaseFigurePanel>>();;

		private int isExistLine;

		public List<JecnBaseRoleFigure> getListAllRoleFigure() {
			return listAllRoleFigure;
		}

		public List<JecnBaseActiveFigure> getListAllActiveFigure() {
			return listAllActiveFigure;
		}

		public List<JecnBaseRoleFigure> getListAllCustomFigure() {
			return listAllCustomFigure;
		}

		public List<JecnBaseFigurePanel> getListAllDottedRect() {
			return listAllDottedRect;
		}

		public int getIsExistLine() {
			return isExistLine;
		}

		public void setIsExistLine(int isExistLine) {
			this.isExistLine = isExistLine;
		}

		public Map<JecnBaseFigurePanel, List<JecnBaseFigurePanel>> getDottedRectContainsActives() {
			return dottedRectContainsActives;
		}

	}
}
