<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="ext-strict">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript" src="${basePath}js/common.js"></script>
        <script type="text/javascript" src="${basePath}js/firstPage.js" charset="UTF-8"></script>
		<!--  流程应用人数详情 -->
		<title><s:text name="applicationProcessInDetails" /></title>
		<script type="text/javascript">
			var basePath = "${basePath}";
			  window.onresize=function(){
			    processAnalysisWH('processApplicationDiv');
			  }
		</script>
	</head>

	<body onload="processAnalysisWH('processApplicationDiv')"  class="xt-webkit ext-chrome">
		    <div style="height: 25px; width: 100%; overflow: hidden;"
				align="right" class="gradient minWidth">
				<%@include file="/navigation.jsp"%>
			</div>
			<div id="processApplicationDiv" 
			style="overflow-y: hidden;">
				<table width="100%" border="0" cellpadding="0" cellspacing="1"
					align="center" bgcolor="#BFBFBF">
					<tr class="FixedTitleRow" align="center">
					     <td width="2%" class="gradient">
					     </td>
						<td class="gradient" width="30%" height="25">
							<!-- 流程名称 -->
							<s:text name="processName" />
						</td>
						<td class="gradient" width="40%" height="25">
							<!-- 流程涉及角色 -->
							<s:text name="processInvolvingRole" />
						</td>
						<td class="gradient" width="26%" height="25">
							<!-- 个数 -->
							<s:text name="number" />
						</td>
						<td width="2%" class="gradient">
					    </td>
					</tr>
					<s:set id="count" value="1" />
					<s:iterator value="processApplicationDetails.listProcessRoleWebBean" status="st">
						<tr align="center" bgcolor="#ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#ffffff';" height="20">
							<td align="right" class="gradient">
								<s:property value="#count" />
								<s:set id="count" value="#count+1"/>
							</td>
							<s:if test="#st.index == 0">
								<td class="t5" style="word-break: break-all;"
									rowspan="<s:property value='processApplicationDetails.listProcessRoleWebBean.size'/>">
									<s:property
										value="processApplicationDetails.processWebBean.flowName" />
								</td>
							</s:if>
							<td class="t5" style="word-break: break-all;">
								<s:property value="roleName" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="totalUser" />
							</td>
							<td></td>
						</tr>
					</s:iterator>
				</table>
		</div>
	</body>
</html>