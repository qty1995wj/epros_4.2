package com.jecn.epros.server.bean.process;

import com.jecn.epros.server.service.process.ActivitySaveData;
import com.jecn.epros.server.service.process.ActivityUpdateData;

import java.io.Serializable;
import java.util.List;

public class ProcessData implements Serializable {
    /**
     * 流程主表
     */
    private JecnFlowStructureT jecnFlowStructureT;
    private JecnFlowBasicInfoT jecnFlowBasicInfoT;
    /**
     * 保存的活动
     */
    private List<ActivitySaveData> activitySaveList;

    /**
     * 更新的活动
     */
    private List<ActivityUpdateData> activityUpdateList;
    /**
     * 删除的活动
     */
    private List<String> activiteDeleteList;
    /**
     * 保存的角色
     */
    private List<RoleFigureData> roleSaveList;
    /**
     * 更新的角色
     */
    private List<RoleFigureData> roleUpdateList;
    /**
     * 删除的角色
     */
    private List<String> roleDeleteList;
    /**
     * 需要保存的其它元素
     */
    private List<JecnFlowStructureImageT> saveList;
    /**
     * 需要修改的其它元素
     */
    private List<JecnFlowStructureImageT> updateList;
    /**
     * 需要删除的连接线
     */
    private List<String> deleteListLine;
    /**
     * 需要删除的其它元素--图形
     */
    private List<String> deleteList;
    /**
     * 角色与活动的关联关系 增加
     */
    private List<JecnRoleActiveT> roleActiveTList;
    /**
     * 页面分页图片
     */
    private List<byte[]> pagingImageList;

    /************* 【关联文件元素 文档元素FileImage】 ********************/
    /**
     * 添加元素集合
     */
    private List<ProcessFileImageFigureData> saveFileImageDataList;
    /**
     * 修改元素集合
     */
    private List<ProcessFileImageFigureData> updateFileImageDataList;

    /**
     * to from 和活动的关系的集合
     */
    private List<JecnToFromActiveT> toFromActiveTList;

    /**
     * 当前画图数据是否保存标识： true：需要保存；false：不需要保存 (默认不保存)
     */
    private boolean isSave = false;

    public List<JecnToFromActiveT> getToFromActiveTList() {
        return toFromActiveTList;
    }

    public void setToFromActiveTList(List<JecnToFromActiveT> toFromActiveTList) {
        this.toFromActiveTList = toFromActiveTList;
    }

    public JecnFlowStructureT getJecnFlowStructureT() {
        return jecnFlowStructureT;
    }

    public void setJecnFlowStructureT(JecnFlowStructureT jecnFlowStructureT) {
        this.jecnFlowStructureT = jecnFlowStructureT;
    }

    public List<ActivitySaveData> getActivitySaveList() {
        return activitySaveList;
    }

    public void setActivitySaveList(List<ActivitySaveData> activitySaveList) {
        this.activitySaveList = activitySaveList;
    }

    public List<ActivityUpdateData> getActivityUpdateList() {
        return activityUpdateList;
    }

    public void setActivityUpdateList(List<ActivityUpdateData> activityUpdateList) {
        this.activityUpdateList = activityUpdateList;
    }

    public List<String> getActiviteDeleteList() {
        return activiteDeleteList;
    }

    public void setActiviteDeleteList(List<String> activiteDeleteList) {
        this.activiteDeleteList = activiteDeleteList;
    }

    public List<RoleFigureData> getRoleSaveList() {
        return roleSaveList;
    }

    public void setRoleSaveList(List<RoleFigureData> roleSaveList) {
        this.roleSaveList = roleSaveList;
    }

    public List<RoleFigureData> getRoleUpdateList() {
        return roleUpdateList;
    }

    public void setRoleUpdateList(List<RoleFigureData> roleUpdateList) {
        this.roleUpdateList = roleUpdateList;
    }

    public List<String> getRoleDeleteList() {
        return roleDeleteList;
    }

    public void setRoleDeleteList(List<String> roleDeleteList) {
        this.roleDeleteList = roleDeleteList;
    }

    public List<JecnFlowStructureImageT> getSaveList() {
        return saveList;
    }

    public void setSaveList(List<JecnFlowStructureImageT> saveList) {
        this.saveList = saveList;
    }

    public List<JecnFlowStructureImageT> getUpdateList() {
        return updateList;
    }

    public void setUpdateList(List<JecnFlowStructureImageT> updateList) {
        this.updateList = updateList;
    }

    public List<String> getDeleteListLine() {
        return deleteListLine;
    }

    public void setDeleteListLine(List<String> deleteListLine) {
        this.deleteListLine = deleteListLine;
    }

    public List<String> getDeleteList() {
        return deleteList;
    }

    public void setDeleteList(List<String> deleteList) {
        this.deleteList = deleteList;
    }

    public List<JecnRoleActiveT> getRoleActiveTList() {
        return roleActiveTList;
    }

    public void setRoleActiveTList(List<JecnRoleActiveT> roleActiveTList) {
        this.roleActiveTList = roleActiveTList;
    }

    public JecnFlowBasicInfoT getJecnFlowBasicInfoT() {
        return jecnFlowBasicInfoT;
    }

    public void setJecnFlowBasicInfoT(JecnFlowBasicInfoT jecnFlowBasicInfoT) {
        this.jecnFlowBasicInfoT = jecnFlowBasicInfoT;
    }

    public List<ProcessFileImageFigureData> getSaveFileImageDataList() {
        return saveFileImageDataList;
    }

    public void setSaveFileImageDataList(List<ProcessFileImageFigureData> saveFileImageDataList) {
        this.saveFileImageDataList = saveFileImageDataList;
    }

    public List<ProcessFileImageFigureData> getUpdateFileImageDataList() {
        return updateFileImageDataList;
    }

    public void setUpdateFileImageDataList(List<ProcessFileImageFigureData> updateFileImageDataList) {
        this.updateFileImageDataList = updateFileImageDataList;
    }

    public List<byte[]> getPagingImageList() {
        return pagingImageList;
    }

    public void setPagingImageList(List<byte[]> pagingImageList) {
        this.pagingImageList = pagingImageList;
    }

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean isSave) {
        this.isSave = isSave;
    }
}
