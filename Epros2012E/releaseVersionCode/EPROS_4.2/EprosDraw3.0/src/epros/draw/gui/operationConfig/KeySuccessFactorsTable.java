package epros.draw.gui.operationConfig;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 关键成功因素表格
 * 
 * @author 2012-07-05
 * 
 */
public class KeySuccessFactorsTable extends JTable implements MouseListener {
	/** 初始化的数据 */
	private List<JecnActivityShowBean> listKSFActivityShow;
	/** 活动明细table */
	private JTable drawActiveDescTable;

	/**
	 * 初始化JTable
	 * 
	 * @param listKSFActivityShow
	 *            用来初始化显示的数据
	 */
	public KeySuccessFactorsTable(List<JecnActivityShowBean> listKSFActivityShow, JTable drawActiveDescTable) {
		this.drawActiveDescTable = drawActiveDescTable;
		this.listKSFActivityShow = listKSFActivityShow;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.addMouseListener(this);
	}

	/**
	 * JTable中显示的数据
	 * 
	 * @author fuzhh Aug 28, 2012
	 * @return
	 */
	public KeySuccessFactorsTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activitiesNumbers"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityTitle"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityIndicatingThat"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("keySuccessFactors"));
		// 获取操作说明数据
		Vector<Vector<Object>> rowData = new Vector<Vector<Object>>();
		for (JecnActivityShowBean activityShowBean : listKSFActivityShow) {
			Vector<Object> row = new Vector<Object>();
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFigureText());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShowAndControl());
			rowData.add(row);
		}
		return new KeySuccessFactorsTableMode(rowData, title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class KeySuccessFactorsTableMode extends DefaultTableModel {
		public KeySuccessFactorsTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// 判断为左键双击
		if (e.getClickCount() == 2 && e.getButton() == 1) {
			int selectRow = this.getSelectedRow();
			DrawJecnActiveDetailsDialog drawKeyActivitiesDialog = new DrawJecnActiveDetailsDialog(listKSFActivityShow
					.get(selectRow), drawActiveDescTable, null, null, this, selectRow, false);
			drawKeyActivitiesDialog.setVisible(true);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}
}
