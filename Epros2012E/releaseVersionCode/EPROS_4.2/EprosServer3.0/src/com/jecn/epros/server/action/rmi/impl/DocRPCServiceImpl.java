package com.jecn.epros.server.action.rmi.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.download.customized.mengNiu.ActiveRoleDownloadExcel;
import com.jecn.epros.server.download.word.SelectDownloadProcess;
import com.jecn.epros.server.service.popedom.IOrganizationService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.process.ActiveRoleBean;
import com.jecn.epros.service.IDocRPCService;

public class DocRPCServiceImpl implements IDocRPCService {

	private Logger log = LoggerFactory.getLogger(DocRPCServiceImpl.class);
	private IWebProcessService webProcessService;
	private IRuleService ruleService;

	@Autowired
	private IOrganizationService organizationService;

	@Override
	public ByteFile downloadDoc(Map<String, Object> param) {
		long id = Long.valueOf(param.get("id").toString());
		String type = param.get("type").toString();
		boolean isp = (Boolean) param.get("isPub");
		Long curPeopleId = (Long) param.get("curPeopleId");
		Long historyId = (Long) param.get("historyId");
		String fileType = param.get("fileType") != null ? param.get("fileType").toString() : "WORD";
		ByteFile result = null;
		try {
			JecnCreateDoc createDoc = null;
			if ("processMap".equals(type)) {
				TreeNodeType nodeType = TreeNodeType.processMap;
				createDoc = webProcessService.printFlowDoc(id, nodeType, isp, curPeopleId, null);
			} else if ("process".equals(type)) {
				TreeNodeType nodeType = TreeNodeType.process;
				if ("PDF".equals(fileType)) {
					createDoc = webProcessService.printFlowPDF(id, nodeType, isp, curPeopleId,
							(historyId == null || historyId == 0) ? null : historyId);
				} else {
					createDoc = webProcessService.printFlowDoc(id, nodeType, isp, curPeopleId,
							(historyId == null || historyId == 0) ? null : historyId);
				}

			} else if ("rule".equals(type)) {
				createDoc = ruleService.downloadRuleFile(id, isp);
			}
			// 根据文件类型获取 文件名称
			getFileName(createDoc, fileType);
			result = new ByteFile();
			result.setFileName(createDoc.getFileName());
			result.setBytes(createDoc.getBytes());
		} catch (Exception e) {
			log.error("获取字节流错误！ type = " + type, e);
		}
		return result;

	}

	private void getFileName(JecnCreateDoc createDoc, String fileType) {
		String fileName = "操作说明.doc";
		if (createDoc != null) {
			fileName = createDoc.getFileName();
		}
		if ("WORD".equals(fileType)) {
			if (!fileName.endsWith(".doc")) {
				fileName = JecnUtil.getShowName(fileName, createDoc.getIdInput());
				fileName = fileName + ".doc";
			}
		} else if ("PDF".equals(fileType)) {
			if (!fileName.endsWith(".pdf")) {
				fileName = JecnUtil.getShowName(fileName, createDoc.getIdInput());
				fileName = fileName + ".pdf";
			}
		}
		createDoc.setFileName(fileName);
	}

	/**
	 * 
	 * @param name
	 * @param id
	 * @param type
	 *            0 :流程、1：文件、2：制度文件
	 * @return
	 * @throws Exception
	 */
	@Override
	public ByteFile downLoadDocContainEnclosure(Map<String, Object> param) throws Exception {
		long id = Long.valueOf(param.get("id").toString());
		// long id = 641L;
		String type = param.get("type").toString();

		if ("position".equals(type)) {// 岗位说明书
			return organizationService.getJobDescriptionByteFile(id);
		}
		return downloadZipFile(param);
	}

	private ByteFile downloadZipFile(Map<String, Object> param) throws Exception {
		long id = Long.valueOf(param.get("id").toString());
		// long id = 641L;
		String type = param.get("type").toString();
		// String type = "rule";
		boolean isPub = (Boolean) param.get("isPub");
		Long curPeopleId = (Long) param.get("curPeopleId");
		Map<EXCESS, List<FileOpenBean>> downloadMap = null;

		try {
			// 1、获取目录名称
			String path = JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.TEMP_DIR;

			// 2、获取附件
			if ("ruleFile".equals(type) || "ruleModelFile".equals(type)) {
				downloadMap = ruleService.ruleFilesDownLoad(id, isPub, curPeopleId);
			} else if ("process".equals(type)) {
				downloadMap = webProcessService.getFlowFilesDownLoad(id, isPub, curPeopleId);
			}
			// 3、获取文件本身内容
			FileOpenBean openBean = getFileOpenBean(id, type, isPub, curPeopleId);
			String name = openBean.getName();
			name = name.substring(0, name.lastIndexOf("."));
			// 创建临时目录
			path += File.separator + JecnCommon.getUUID();
			// 判断目录是否存在
			SelectDownloadProcess.getFilePath(path);
			// 4、把附件写入本地
			SelectDownloadProcess.writeEnclosureToLocal(downloadMap, path);
			// 5、当前文件写入本地
			writeCurFileToLocal(openBean, path);
			// 6、获取压缩文件
			ByteFile byteFile = getZipFile(name, path);
			if (byteFile != null) {
				// 清除本地文件
				SelectDownloadProcess.deleteDirectory(path);
			}
			return byteFile;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private ByteFile getZipFile(String name, String path) throws Exception {
		// 压缩文件路径
		String zipName = name + ".zip";
		String zipPath = path + File.separator + zipName;
		File zipFile = new File(zipPath);
		if (zipFile.exists()) {
			zipFile.delete();
		}
		SelectDownloadProcess.zipFile(path, zipPath);
		ByteFile byteFile = new ByteFile();
		byteFile.setBytes(JecnFinal.getByteByPath(zipPath));
		byteFile.setFileName(zipName);
		return byteFile;
	}

	private void writeCurFileToLocal(FileOpenBean openBean, String path) throws Exception {
		if (openBean == null) {
			return;
		}
		// 写入本地
		JecnFinal.wirteFileToTempDirByPath(path + File.separator + openBean.getName(), openBean.getFileByte());
	}

	private FileOpenBean getFileOpenBean(long id, String type, boolean isPub, Long curPeopleId) throws Exception {
		FileOpenBean openBean = null;
		if ("ruleFile".equals(type)) {
			openBean = getRuleFile(id, type, isPub);
		} else if ("ruleModelFile".equals(type)) {
			JecnCreateDoc createDoc = ruleService.downloadRuleFile(id, isPub);
			openBean = new FileOpenBean();
			openBean.setFileByte(createDoc.getBytes());
			String fileName = createDoc.getFileName();
			if (!fileName.endsWith(".doc")) {
				fileName = JecnUtil.getShowName(fileName, createDoc.getIdInput());
				fileName = fileName + ".doc";
			}
			openBean.setName(fileName);
		} else if ("process".equals(type)) {
			openBean = getFlowDocFile(id, type, isPub, curPeopleId, null);
		}
		return openBean;
	}

	private FileOpenBean getFlowDocFile(long id, String type, boolean isp, Long curPeopleId, Long historyId)
			throws Exception {
		JecnCreateDoc createDoc = webProcessService.printFlowDoc(id, TreeNodeType.process, isp, curPeopleId, historyId);
		FileOpenBean openBean = new FileOpenBean();
		openBean.setName(createDoc.getFileName() + ".doc");
		openBean.setFileByte(createDoc.getBytes());
		return openBean;
	}

	private FileOpenBean getRuleFile(long id, String type, boolean isPub) throws Exception {
		FileOpenBean openBean = null;
		if (isPub) {
			Rule rule = ruleService.getRule(id);
			if (rule.getIsFileLocal() == 1) {// 本地上传
				G020RuleFileContent openBeanG020 = ruleService.getG020RuleFileContent(rule.getFileId());
				openBean = new FileOpenBean();
				openBean.setFileByte(JecnDaoUtil.blobToBytes(openBeanG020.getFileStream()));
				openBean.setName(openBeanG020.getFileName());
			} else {// 保存数据库
				openBean = ruleService.openFile(rule.getFileId(), isPub);
			}
		} else {
			RuleT ruleT = ruleService.get(id);
			if (ruleT.getIsFileLocal() == 1) {// 本地上传
				G020RuleFileContent openBeanG020 = ruleService.getG020RuleFileContent(ruleT.getFileId());
				openBean = new FileOpenBean();
				openBean.setFileByte(JecnDaoUtil.blobToBytes(openBeanG020.getFileStream()));
				openBean.setName(openBeanG020.getFileName());
			} else {// 保存数据库
				openBean = ruleService.openFile(ruleT.getFileId(), isPub);
			}
		}
		return openBean;
	}

	@Override
	public ByteFile downloadExcel(Map<String, Object> param) {
		String isPub = param.get("isPub").toString();
		Long flowId = Long.valueOf(param.get("flowId").toString());

		boolean isp = true;
		if ("false".equals(isPub)) {
			isp = false;
		}
		ByteFile result = null;
		try {
			ActiveRoleBean activeRoleBean = webProcessService.getProcessActiveWeb(flowId, isp);
			ActiveRoleDownloadExcel activeRoleDownload = new ActiveRoleDownloadExcel();
			// 获取生成文件的路径
			String path = activeRoleDownload.activeRoleDownloadExcel(activeRoleBean);
			// 读取文件为字节流
			byte[] bytes = JecnFinal.getByteByPath(path);
			// 获取文件
			File file = new File(path);
			// 文件是否存在
			if (file.exists()) {
				// 删除文件
				file.delete();
			}
			result = new ByteFile();
			result.setBytes(bytes);
			String fileName = "roleActive.xls";
			result.setFileName(fileName);
		} catch (Exception e) {
			log.error("获取流程操作说明excel字节流错误！", e);
		}
		return result;
	}

	public IWebProcessService getWebProcessService() {
		return webProcessService;
	}

	public void setWebProcessService(IWebProcessService webProcessService) {
		this.webProcessService = webProcessService;
	}

	public IRuleService getRuleService() {
		return ruleService;
	}

	public void setRuleService(IRuleService ruleService) {
		this.ruleService = ruleService;
	}

}
