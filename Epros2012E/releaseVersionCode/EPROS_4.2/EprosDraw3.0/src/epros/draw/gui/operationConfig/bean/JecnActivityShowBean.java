package epros.draw.gui.operationConfig.bean;

import epros.draw.gui.figure.JecnBaseActiveFigure;

/**
 * 
 * 活动基本信息
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Aug 27, 2012 时间：11:32:10 AM
 */
public class JecnActivityShowBean {
	/** 执行角色名称 */
	private String roleName;
	/** 角色的UIID */
	private String roleUIID;
	/** 输入*/
	private String input;
	/** 输出*/
	private String output;
	/** 活动图形的数据 */
	private JecnBaseActiveFigure activeFigure;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleUIID() {
		return roleUIID;
	}

	public void setRoleUIID(String roleUIID) {
		this.roleUIID = roleUIID;
	}
	
	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public JecnBaseActiveFigure getActiveFigure() {
		return activeFigure;
	}

	public void setActiveFigure(JecnBaseActiveFigure activeFigure) {
		this.activeFigure = activeFigure;
	}
}
