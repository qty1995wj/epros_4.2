package com.jecn.epros.server.dao.system.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

/**
 * 
 * 配置信息表（JECN_CONFIG_ITEM）操作类
 * 
 * @author ZHOUXY
 * 
 */

public class JecnConfigItemDaoImpl extends AbsBaseDao<JecnConfigItemBean, String> implements IJecnConfigItemDao {

	/**
	 * 
	 * 查询流程图设置值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 流程图设置值
	 */
	@Override
	public List<JecnConfigItemBean> select(int typeBigModel) {
		// 查询流程图设置项SQL
		String sql = "FROM JecnConfigItemBean  WHERE typeBigModel=? ORDER BY SORT";
		List<JecnConfigItemBean> result = listHql(sql, typeBigModel);
		return alterName(result);
	}

	/**
	 * 
	 * 查询流程文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程文件排序后显示数据
	 */
	@Override
	public List<JecnConfigItemBean> selectShowFlowFile() {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE T.TYPE_BIG_MODEL=1 AND T.TYPE_SMALL_MODEL=4 AND T.IS_TABLE_DATA=1 AND T.VALUE='1' ORDER BY SORT";
		return listNativeAddEntity(sql);
	}

	/**
	 * 
	 * 查询流程文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程地图文件排序后显示数据
	 */
	public List<JecnConfigItemBean> selectFlowMapShowFlowFile() {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE T.TYPE_BIG_MODEL=0 AND T.TYPE_SMALL_MODEL=4 AND T.VALUE='1' ORDER BY SORT";
		return listNativeAddEntity(sql);
	}

	/**
	 * 
	 * 查询流程建设规范显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程建设规范显示数据
	 */
	@Override
	public List<JecnConfigItemBean> selectShowPartMapStand() {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE T.TYPE_BIG_MODEL=1 AND T.TYPE_SMALL_MODEL=3 AND T.VALUE='1'";
		return listNativeAddEntity(sql);
	}

	/**
	 * 
	 * 查询任务审批环节数据（带文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节数据（带文控类型）
	 */
	@Override
	public List<JecnConfigItemBean> selectShowTaskApp(int typeBigModel) {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE T.TYPE_BIG_MODEL=? AND T.TYPE_SMALL_MODEL=0 ORDER BY SORT";
		return listNativeAddEntity(sql, typeBigModel);
	}

	/**
	 * 
	 * 查询任务审批环节数据（除去文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节显示数据
	 */
	@Override
	public List<JecnConfigItemBean> selectTaskAppTable(int typeBigModel) {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE T.TYPE_BIG_MODEL=? AND T.TYPE_SMALL_MODEL=0 AND T.IS_TABLE_DATA=1 ORDER BY SORT";
		return alterName(listNativeAddEntity(sql, typeBigModel));
	}

	/**
	 * 
	 * 查询所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 */
	@Override
	public List<JecnConfigItemBean> selectTaskShowAllItems(int typeBigModel) {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE T.TYPE_BIG_MODEL=? AND T.TYPE_SMALL_MODEL=1";
		return alterName(listNativeAddEntity(sql, typeBigModel));
	}

	/**
	 * 
	 * 查询所有表示公开秘密数据集合
	 * 
	 * @return List<JecnConfigItemBean> 所有表示公开秘密数据集合
	 */
	@Override
	public List<JecnConfigItemBean> selectAllPub() {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE T.TYPE_BIG_MODEL=5 AND T.TYPE_SMALL_MODEL=6";
		return alterName(listNativeAddEntity(sql));
	}

	/**
	 * 
	 * 修改JECN_CONFIG_ITEM表
	 * 
	 * @param configItemList
	 *            List<JecnConfigItemBean>
	 * 
	 * @return boolean true:修改成功；false：修改失败
	 */
	@Override
	public boolean update(List<JecnConfigItemBean> configItemList) {
		if (configItemList == null) {
			return false;
		}
		for (JecnConfigItemBean configItemBean : configItemList) {
			// 排序
			Integer sort = configItemBean.getSort();
			// 必填项
			Integer isEmpty = configItemBean.getIsEmpty();

			String sql = "UPDATE JECN_CONFIG_ITEM SET NAME=? ,VALUE=?,SORT=?,IS_EMPTY=?,EN_NAME = ?,V1=?,V2=? WHERE ID=?";

			// 执行更新
			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				execteNative(sql, configItemBean.getName(), configItemBean.getValue(), (sort == null || sort == 0) ? ""
						: sort.intValue(), (isEmpty == null) ? "" : isEmpty.intValue(), configItemBean.getEnName(),
						configItemBean.getV1(), configItemBean.getV2(), configItemBean.getId());
			} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				execteNative(sql, configItemBean.getName(), configItemBean.getValue(),
						(sort == null || sort == 0) ? null : sort.intValue(), (isEmpty == null) ? null : isEmpty
								.intValue(), configItemBean.getEnName(), configItemBean.getV1(),
						configItemBean.getV2(), configItemBean.getId());
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				execteNative(sql, configItemBean.getName(), configItemBean.getValue(),
						(sort == null || sort == 0) ? null : sort.intValue(), (isEmpty == null) ? null : isEmpty
								.intValue(), configItemBean.getEnName(), configItemBean.getV1(),
						configItemBean.getV2(), configItemBean.getId());
			}
		}
		return true;
	}

	/**
	 * 
	 * 通过大类下唯一标示获取value字段值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String value字段值或NULL
	 */
	public String selectValue(int typeBigModel, String mark) {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM  WHERE TYPE_BIG_MODEL=? AND Mark=?";
		List<JecnConfigItemBean> itemBean = listNativeAddEntity(sql, typeBigModel, mark);

		if (itemBean != null && itemBean.size() == 1) {
			return itemBean.get(0).getValue();
		}
		return null;
	}

	/**
	 * 
	 * 通过大类下唯一标示获取数据
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String 一行数据字段值或NULL
	 */
	public JecnConfigItemBean selectConfigItemBean(int typeBigModel, String mark) {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM  WHERE TYPE_BIG_MODEL=? AND Mark=?";
		List<JecnConfigItemBean> itemBean = listNativeAddEntity(sql, typeBigModel, mark);

		if (itemBean != null && itemBean.size() == 1) {
			return itemBean.get(0);
		}
		return null;
	}

	/**
	 * 
	 * 查询流程基本信息
	 * 
	 * @return List<JecnConfigItemBean>查询流程基本信息
	 */
	@Override
	public List<JecnConfigItemBean> selectBaseInfoFlowFile() {
		String sql = "select * from jecn_config_item t where t.type_big_model=1 and t.type_small_model=2";
		return listNativeAddEntity(sql);
	}

	/**
	 * 
	 * 邮箱配置项
	 * 
	 * @return List<JecnConfigItemBean>邮箱配置项集合
	 */
	@Override
	public List<JecnConfigItemBean> selectEmailConfigItem() {
		String sql = "select * from jecn_config_item t where t.type_big_model=4 and t.type_small_model=5";
		return listNativeAddEntity(sql);
	}

	/**
	 * 获取系统配置项信息
	 * 
	 * @param typeBigModel
	 *            int 0：流程地图 ,1：流程图 ,2：制度, 3：文件, 4：邮箱配置, 5：权限配置
	 * @param typeSmallModel
	 *            int 0：审批环节配置, 1：任务界面显示项配置, 2：基本配置, 3：流程图建设规范 ,4：流程文件,
	 *            5：邮箱配置,6：权限配置
	 * @return List<JecnConfigItemBean> 系统配置项信息
	 * @throws Exception
	 */
	@Override
	public List<JecnConfigItemBean> selectConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
		String sql = "select * from jecn_config_item t where t.type_big_model=? and t.type_small_model=? order by t.sort";
		return alterName(listNativeAddEntity(sql, typeBigModel, typeSmallModel));
	}

	/**
	 * 获取小类对应的配置信息
	 * 
	 * @param typeSmallModel
	 *            0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
	 *            11:发布配置
	 * @return List<JecnConfigItemBean>
	 */
	@Override
	public List<JecnConfigItemBean> selectSmallTypeItem(int typeSmallModel) {
		String sql = "SELECT * FROM JECN_CONFIG_ITEM T WHERE  T.TYPE_SMALL_MODEL=?";
		return alterName(listNativeAddEntity(sql, typeSmallModel));
	}

	/**
	 * 获取排序后的结果值（value=1）
	 * 
	 * @param typeBigModel
	 * @param typeSmallModel
	 * @return
	 */
	@Override
	public List<JecnConfigItemBean> selectShowSortConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
		String sql = "select * from jecn_config_item t where t.type_big_model=? and t.type_small_model=? and t.value=1 order by t.sort";
		return alterName(listNativeAddEntity(sql, typeBigModel, typeSmallModel));
	}

	/**
	 * 获取表格数据
	 */
	@Override
	public List<JecnConfigItemBean> selectTableConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
		String sql = "select * from jecn_config_item t where t.type_big_model=? and t.type_small_model=? and t.value=1 AND T.IS_TABLE_DATA=1 order by t.sort";
		return alterName(listNativeAddEntity(sql, typeBigModel, typeSmallModel));
	}

	@Override
	public List<JecnConfigItemBean> selectKpiConfigItemBeanByTop(int top) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "select top "
					+ top
					+ " t.* from jecn_config_item t where t.type_big_model = 1 and t.type_small_model = 16 and t.value=1 order by t.sort asc";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "select * from (select t.* from jecn_config_item t where t.type_big_model = 1 and t.type_small_model = 16 and t.value=1  order by t.sort asc ) where rownum<="
					+ top;
		}

		return this.listNativeAddEntity(sql);
	}

	@Override
	public List<JecnConfigItemBean> selectAllItem() {
		// 查询流程图设置项SQL
		String sql = "FROM JecnConfigItemBean";
		List<JecnConfigItemBean> result = listHql(sql);
		return alterName(result);
	}

	@Override
	public List<Object[]> selectMaintainNode() {
		String sql = "select mark,name,defaultname,en_name,endefalutname from jecn_maintain_node ";
		return listNativeSql(sql);
	}

	private static List<JecnConfigItemBean> alterName(List<JecnConfigItemBean> configs) {
		return JecnUtil.alterName(configs);
	}
}
