package epros.draw.gui.workflow.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 活动明细
 * 
 * @author ZHOUXY
 * 
 */
public class JecnActiveDetailsDialog extends JecnDialog implements CaretListener, ActionListener {
	/** 活动对象 */
	protected JecnBaseActiveFigure activeFigure = null;

	/** 主面板 */
	protected JTabbedPane tabbedPanel = null;

	/** 基本信息 */
	protected JecnPanel basicInfoPanel = null;

	/** 活动信息 */
	protected JecnPanel activeAtrrPanel = null;
	/** 关键活动 */
	protected JecnPanel keyActivePanel = null;

	/** 编号标签 */
	protected JLabel activeNumLabel = null;
	/** 编号输入框 */
	protected JTextField activeNumTextField = null;
	/** 编号提示信息框 */
	protected JecnUserInfoTextArea activeNumInfoTextArea = null;

	/** 名称标签 */
	protected JLabel activeNameLabel = null;
	/** 名称输入框 */
	protected JTextArea activeNameTextArea = null;
	/** 名称输入框容器 */
	protected JScrollPane activeNameScrollPane = null;
	/** 名称提示信息框 */
	protected JecnUserInfoTextArea activeNameInfoTextArea = null;

	/** 说明标签 */
	protected JLabel activeNoteLabel = null;
	/** 说明输入区域 */
	protected JTextArea activeNoteTextArea = null;
	/** 说明输入区域容器 */
	protected JScrollPane activeNoteScrollPane = null;
	/** 说明提示信息框 */
	protected JecnUserInfoTextArea activeNoteInfoTextArea = null;

	/** 选中的单选框对应的流程元素类型 */
	protected MapElemType selectedMapElemType = null;

	/** PA\KSF\KCP类型标签 */
	protected JLabel keyActiveTypeLabel = null;
	/** PA\KSF\KCP类型内容 */
	protected JLabel keyActiveTypeContLabel = null;
	/** PA\KSF\KCP标签 */
	protected JLabel keyActiveNoteLabel = null;
	/** PA\KSF\KCP输入区域 */
	protected JTextArea keyActiveNoteTextArea = null;
	/** PA\KSF\KCP输入区域容器 */
	protected JScrollPane keyActiveNoteScrollPane = null;
	/** PA\KSF\KCP输入区提示信息框 */
	protected JecnUserInfoTextArea keyActiveNoteInfoTextArea = null;
	/** 确认取消按钮面板 */
	protected JecnOKCancelPanel okCancelPanel = null;

	/** 输入输出面板 */
	protected JecnPanel ioPanel = null;
	/** 输入Lab */
	protected JLabel inputLab;
	/** 输入Field */
	protected JTextArea inputArea;

	protected JScrollPane inputScrollPane = null;
	/** 输出Lab */
	protected JLabel outputLab;
	/** 输出Field */
	protected JTextArea outputArea;
	protected JScrollPane outputScrollPane = null;

	/** 输入输出 错误信息提示 */
	protected JecnUserInfoTextArea inputNoteInfoTextArea = null;

	/** 输入输出 错误信息提示 */
	protected JecnUserInfoTextArea outputNoteInfoTextArea = null;

	public JecnActiveDetailsDialog(JecnBaseActiveFigure activeFigure) {
		this.activeFigure = activeFigure;
		intComponents();
		initLayout();
		initData();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	protected void intComponents() {
		// 主面板
		tabbedPanel = new JTabbedPane();

		// 基本信息
		basicInfoPanel = new JecnPanel();

		// 活动信息
		activeAtrrPanel = new JecnPanel();
		// 关键活动
		keyActivePanel = new JecnPanel();

		// 编号标签
		activeNumLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeNum"));
		// 编号输入框
		activeNumTextField = new JTextField();
		// 编号提示信息框
		activeNumInfoTextArea = new JecnUserInfoTextArea();

		// 名称标签
		activeNameLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeName"));
		// 名称输入框
		activeNameTextArea = new JTextArea();
		// 名称输入框容器
		activeNameScrollPane = new JScrollPane(activeNameTextArea);
		// 名称提示信息框
		activeNameInfoTextArea = new JecnUserInfoTextArea();

		// 说明标签
		activeNoteLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeNote"));
		// 说明输入框
		activeNoteTextArea = new JTextArea();
		// 说明输入区域容器
		activeNoteScrollPane = new JScrollPane(activeNoteTextArea);
		// 说明提示信息框
		activeNoteInfoTextArea = new JecnUserInfoTextArea();

		// PA\KSF\KCP类型标签
		keyActiveTypeLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("keyActiveType"));
		// PA\KSF\KCP类型内容
		keyActiveTypeContLabel = new JLabel();
		// PA\KSF\KCP标签
		keyActiveNoteLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeNote"));
		// PA\KSF\KCP输入区域
		keyActiveNoteTextArea = new JTextArea();
		// PA\KSF\KCP输入区域容器
		keyActiveNoteScrollPane = new JScrollPane(keyActiveNoteTextArea);
		// PA/KSF/KCP提示信息框
		keyActiveNoteInfoTextArea = new JecnUserInfoTextArea();

		// 确认取消按钮面板
		okCancelPanel = new JecnOKCancelPanel();

		int minHeight = 404;
		int preHeight = 500;
		if (JecnSystemStaticData.isShowActiveIO()) {
			minHeight = 504;
			preHeight = 600;
		}
		// 大小
		Dimension size = new Dimension(480, minHeight);
		Dimension preferredSize = new Dimension(600, preHeight);
		this.setSize(preferredSize);
		this.setMinimumSize(size);
		this.setModal(true);
		this.setResizable(true);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
		// 标题
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.activeDetails));

		// 活动信息
		activeAtrrPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		activeAtrrPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("activeAtrrTitle")));

		// 名称输入框
		activeNameTextArea.setRows(1);
		activeNameTextArea.setLineWrap(true);
		activeNameTextArea.setWrapStyleWord(true);
		activeNameTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));
		activeNameTextArea.setBorder(null);

		// 说明输入区域容器
		activeNameScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		activeNameScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 关键活动
		keyActivePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		keyActivePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("keyActiveTitle")));

		// 说明输入区域容器
		activeNoteScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		activeNoteScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		activeNoteScrollPane.setBorder(null);
		// PA\KSF\KCP输入区域容器
		keyActiveNoteScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		keyActiveNoteScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		keyActiveNoteScrollPane.setBorder(null);

		// 说明输入区域
		activeNoteTextArea.setRows(2);
		activeNoteTextArea.setLineWrap(true);
		activeNoteTextArea.setWrapStyleWord(true);
		activeNoteTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));
		// PA\KSF\KCP输入区域
		keyActiveNoteTextArea.setRows(2);
		keyActiveNoteTextArea.setLineWrap(true);
		keyActiveNoteTextArea.setWrapStyleWord(true);
		keyActiveNoteTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		// 事件
		// 编号
		activeNumTextField.addCaretListener(this);
		// 名称
		activeNameTextArea.addCaretListener(this);
		// 说明
		activeNoteTextArea.addCaretListener(this);
		// PA/KSF/KCP
		keyActiveNoteTextArea.addCaretListener(this);
		// 确认按钮
		okCancelPanel.getOkBtn().addActionListener(this);
		// 取消按钮
		okCancelPanel.getCancelBtn().addActionListener(this);

		// 添加组件
		this.getContentPane().add(tabbedPanel);
		// 添加基本信息页
		tabbedPanel.addTab(JecnResourceUtil.getJecnResourceUtil().getValue("basicInfo"), basicInfoPanel);

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {

				if (isUpdate()) {// true：有更新；false：没有更新
					if (dialogCloseBeforeMsgTip()) {// true：需要执行关闭 false：不要执行关闭
						return true;
					}
					return false;
				}
				return true;
			}
		});

		initIOComponents();
	}

	/**
	 * 输入输出IO
	 * 
	 */
	private void initIOComponents() {
		// 输入
		inputLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("inputC"));
		// 输入
		inputArea = new JTextArea();

		inputArea.setRows(2);
		inputArea.setLineWrap(true);
		inputArea.setWrapStyleWord(true);
		inputArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		inputScrollPane = new JScrollPane(inputArea);
		inputScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		inputScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		inputScrollPane.setBorder(null);
		// 输出
		outputLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("outPutC"));
		// 输出
		outputArea = new JTextArea();
		outputArea.setRows(2);
		outputArea.setLineWrap(true);
		outputArea.setWrapStyleWord(true);
		outputArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		outputScrollPane = new JScrollPane(outputArea);
		outputScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		outputScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		outputScrollPane.setBorder(null);

		ioPanel = new JecnPanel();
		ioPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		inputNoteInfoTextArea = new JecnUserInfoTextArea();

		outputNoteInfoTextArea = new JecnUserInfoTextArea();
	}

	/**
	 * 
	 * 活动信息
	 * 
	 */
	protected void initLayout() {

		int h = 7;
		int v = 3;

		/** 标签间距 */
		Insets insetsLabel = new Insets(7, 11, 7, 3);
		/** 内容间距 */
		Insets insetsContent = new Insets(7, 0, 7, 5);

		// 活动信息
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, new Insets(v, h, 0, h), 0, 0);
		basicInfoPanel.add(activeAtrrPanel, c);

		if (JecnSystemStaticData.isShowActiveIO()) {
			// 输入输出
			c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					new Insets(v, h, v, h), 0, 0);
			basicInfoPanel.add(ioPanel, c);
		}

		// 关键活动
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				v, h, v, h), 0, 0);
		basicInfoPanel.add(keyActivePanel, c);

		// 确认取消按钮面板
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, h, 0, h), 0, 0);
		basicInfoPanel.add(okCancelPanel, c);

		// //////////////// 活动信息//////////////////
		// 编号标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		activeAtrrPanel.add(activeNumLabel, c);
		// 编号输入框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		activeAtrrPanel.add(activeNumTextField, c);
		// 编号信息提示
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(activeNumInfoTextArea, c);

		// 名称标签
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		activeAtrrPanel.add(activeNameLabel, c);
		// 名称输入框
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		activeAtrrPanel.add(activeNameScrollPane, c);
		// 编号信息提示
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(activeNameInfoTextArea, c);

		// 说明标签
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		activeAtrrPanel.add(activeNoteLabel, c);
		c = new GridBagConstraints(1, 4, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		activeAtrrPanel.add(activeNoteScrollPane, c);
		// 说明信息提示
		c = new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(activeNoteInfoTextArea, c);

		// ////////////////活动信息//////////////////

		// ////////////////关键活动//////////////////
		JecnPanel typePanel = new JecnPanel();
		// PA\KSF\KCP类型标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		typePanel.add(keyActiveTypeLabel, c);
		// PA\KSF\KCP类型内容
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		typePanel.add(keyActiveTypeContLabel, c);
		// 类型标签
		c = new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		keyActivePanel.add(typePanel, c);

		// PA\KSF\KCP标签
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		keyActivePanel.add(keyActiveNoteLabel, c);
		// PA\KSF\KCP输入区域
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		keyActivePanel.add(keyActiveNoteScrollPane, c);
		// 说明信息提示
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		keyActivePanel.add(keyActiveNoteInfoTextArea, c);
		// ////////////////关键活动//////////////////

		// IO布局
		ioLayout(c, insetsLabel, insetsContent);
	}

	/**
	 * IO布局
	 * 
	 * @param c
	 * @param insetsLabel
	 * @param insetsContent
	 */
	private void ioLayout(GridBagConstraints c, Insets insetsLabel, Insets insetsContent) {
		// inputLab
		ioPanel.setLayout(new GridBagLayout());
		ioPanel.setBorder(BorderFactory.createTitledBorder(""));

		// 输入
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		ioPanel.add(inputLab, c);

		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		ioPanel.add(inputScrollPane, c);

		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		ioPanel.add(inputNoteInfoTextArea, c);

		// 输出
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		ioPanel.add(outputLab, c);

		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		ioPanel.add(outputScrollPane, c);

		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		ioPanel.add(outputNoteInfoTextArea, c);
	}

	/**
	 * 
	 * 初始化数据
	 * 
	 */
	protected void initData() {
		if (activeFigure == null) {
			return;
		}
		// 编号
		this.activeNumTextField.setText(this.activeFigure.getFlowElementData().getActivityNum());

		// 名称
		this.activeNameTextArea.setText(this.activeFigure.getFlowElementData().getFigureText());
		// 说明
		this.activeNoteTextArea.setText(this.activeFigure.getFlowElementData().getAvtivityShow());

		// 输入
		this.inputArea.setText(this.activeFigure.getFlowElementData().getActiveInput());
		this.outputArea.setText(this.activeFigure.getFlowElementData().getActiveOutPut());

		// 活动关联的关键活动类型，如果活动区域没有关键活动返回NULL
		selectedMapElemType = activeFigure.getKeyActiveMapElemType();

		if (selectedMapElemType != null) {// 关键活动存在情况

			// PA\KSF\KCP输入区域启用
			this.keyActiveNoteTextArea.setEnabled(true);

			// PA/KSF/KCP的说明
			this.keyActiveNoteTextArea.setText(this.activeFigure.getFlowElementData().getAvtivityShowAndControl());

			// 设置类型
			keyActiveTypeContLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue(selectedMapElemType));

			this.keyActivePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
					JecnResourceUtil.getJecnResourceUtil().getValue(selectedMapElemType + "Title")));

		} else {// 不存在关键活动
			// PA\KSF\KCP输入区域不启用
			this.keyActiveNoteTextArea.setEnabled(false);

			// 设置类型
			keyActiveTypeContLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("noneFigure"));
		}
	}

	/**
	 * 
	 * 退出对话框操作,提供给取消按钮使用
	 * 
	 */
	public void exitJecnDialog() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			if (dialogCloseBeforeMsgTip()) {// 信息提示：true：需要执行关闭 false：不要执行关闭
				this.setVisible(false);
			}
		}
	}

	/**
	 * 
	 * 是否有更新
	 * 
	 * @return boolean true：有更新；false：没有更新
	 */
	protected boolean isUpdate() {
		// 编号
		String num = this.activeNumTextField.getText();
		// 名称
		String name = this.activeNameTextArea.getText();
		// 说明
		String note = this.activeNoteTextArea.getText();
		// PA/KSF/KCP
		String keyActiveNote = this.keyActiveNoteTextArea.getText();
		// 输入
		String input = this.inputArea.getText();
		// 输入
		String output = this.outputArea.getText();

		if (!isUpdate(num, name, note, keyActiveNote, input, output)) {// 没有更新
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 输入框的插入字符事件
	 * 
	 */
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	/**
	 * 
	 * 下拉框事件或按钮点击事件
	 * 
	 * @param e
	 *            ActionEvent
	 */
	public void actionPerformedProcess(ActionEvent e) {
		if (e.getSource() == okCancelPanel.getOkBtn()) {// 确认按钮

			// boolean true:校验成功；false：校验失败
			boolean ret = okBtnCheck();
			if (!ret) {// 校验失败
				return;
			}
			// 校验成功修改活动属性值，添加关键活动
			updateValuesToActionFigure();
			// 关闭对话框
			this.setVisible(false);

		} else if (e.getSource() == okCancelPanel.getCancelBtn()) {// 取消按钮
			// 关闭对话框
			exitJecnDialog();
		}
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	protected void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == activeNumTextField) {// 编号输入框
			// 编号长度超过122个字符或61个汉字
			checkTextFieldLength(activeNumInfoTextArea, activeNumTextField);
		} else if (e.getSource() == activeNameTextArea) {// 名称输入框
			// 名称长度超过122个字符或61个汉字
			checkTextFieldLength(activeNameInfoTextArea, activeNameTextArea);
		} else if (e.getSource() == keyActiveNoteTextArea) {// PA\KSF\KCP输入区域
			// 说明长度是否超过1200个字符或600个汉字
			checkTextAreaLength(keyActiveNoteInfoTextArea, keyActiveNoteTextArea);
		} else if (e.getSource() == inputArea) {
			// 说明长度是否超过1200个字符或600个汉字
			checkTextAreaLength(inputNoteInfoTextArea, inputArea);
		} else if (e.getSource() == outputArea) {
			// 说明长度是否超过1200个字符或600个汉字
			checkTextAreaLength(outputNoteInfoTextArea, outputArea);
		}
	}

	/**
	 * 
	 * 点击确认按钮提交前校验
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	protected boolean okBtnCheck() {
		boolean ret = true;
		// 编号
		ret = checkTextFieldLength(activeNumInfoTextArea, activeNumTextField);
		// 名称
		boolean nameRet = checkTextFieldLength(activeNameInfoTextArea, activeNameTextArea);
		if (!nameRet) {
			ret = nameRet;
		}

		// PA/KSF/KCP说明
		boolean keyActiveNoteRet = checkTextAreaLength(keyActiveNoteInfoTextArea, keyActiveNoteTextArea);
		if (!keyActiveNoteRet) {
			ret = keyActiveNoteRet;
		}

		// IO 验证
		boolean inputRet = checkTextAreaLength(inputNoteInfoTextArea, inputArea);
		if (!inputRet) {
			ret = inputRet;
		}

		boolean outputRet = checkTextAreaLength(outputNoteInfoTextArea, outputArea);
		if (!outputRet) {
			ret = outputRet;
		}

		return ret;
	}

	/**
	 * 
	 * 校验JTextField的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textField
	 *            JTextField
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JTextField textField) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNameLength(textField.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 校验JTextArea的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textArea
	 *            JTextArea
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JTextArea textArea) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNameLength(textArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 校验JTextArea的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textArea
	 *            JTextArea
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextAreaLength(JecnUserInfoTextArea infoTextArea, JTextArea textArea) {
		if (!textArea.isEnabled()) {// 未启用就不用校验
			return true;
		}
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNoteLength(textArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 更新活动相关属性属性
	 * 
	 */
	protected void updateValuesToActionFigure() {
		// 编号
		String num = this.activeNumTextField.getText();
		// 名称
		String name = this.activeNameTextArea.getText();
		// 说明
		String note = this.activeNoteTextArea.getText();
		// PA/KSF/KCP
		String keyActiveNote = this.keyActiveNoteTextArea.getText();
		// 输入
		String input = this.inputArea.getText();
		// 输入
		String output = this.outputArea.getText();

		if (!isUpdate(num, name, note, keyActiveNote, input, output)) {// 没有更新
			return;
		}

		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 操作前
		undoData.recodeFlowElement(activeFigure);

		// 编号
		this.activeFigure.getFlowElementData().setActivityNum(num);
		// 名称
		this.activeFigure.getFlowElementData().setFigureText(name);
		// 说明
		this.activeFigure.getFlowElementData().setAvtivityShow(note);

		// IO
		this.activeFigure.getFlowElementData().setActiveInput(input);
		this.activeFigure.getFlowElementData().setActiveOutPut(output);

		if (selectedMapElemType != null) {
			// PA/KSF/KCP
			this.activeFigure.getFlowElementData().setAvtivityShowAndControl(keyActiveNote);
		}

		// 关键活动类型：PAFigure:问题区域 KSFFigure:关键成功因素 KCPFigure:关键控制点
		this.activeFigure.getFlowElementData().setActiveKeyMapElemType(selectedMapElemType);
		this.activeFigure.repaint();

		// 操作后
		redoData.recodeFlowElement(activeFigure);

		// *************************撤销恢复*************************//
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		JecnDrawMainPanel.getMainPanel().getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
		// *************************撤销恢复*************************//
	}

	/**
	 * 
	 * 判断是否有更新，有返回true，没有返回false
	 * 
	 * @param newNum
	 * @param newName
	 * @param newNote
	 * @param newKeyActiveNote
	 * @return boolean 有返回true，没有返回false
	 */
	protected boolean isUpdate(String newNum, String newName, String newNote, String newKeyActiveNote, String input,
			String output) {
		JecnActiveData data = this.activeFigure.getFlowElementData();

		if (!DrawCommon.checkStringSame(newNum, data.getActivityNum())
				|| !DrawCommon.checkStringSame(newName, data.getFigureText())
				|| !DrawCommon.checkStringSame(newNote, data.getAvtivityShow())
				|| !DrawCommon.checkStringSame(input, data.getActiveInput())
				|| !DrawCommon.checkStringSame(output, data.getActiveOutPut())
				|| (selectedMapElemType != null && !DrawCommon.checkStringSame(newKeyActiveNote, data
						.getAvtivityShowAndControl()))) {
			return true;
		}

		return false;
	}
}
