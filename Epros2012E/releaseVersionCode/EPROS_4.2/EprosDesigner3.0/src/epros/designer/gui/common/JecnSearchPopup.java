package epros.designer.gui.common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.draw.gui.swing.textfield.search.JSearchTextField;
/**
 * @author yxw 2013-1-17
 * @description：快递查询弹出popup公共类
 */
public class JecnSearchPopup extends JPopupMenu {
	/** 搜索 table */
	private JTable searchTable = null;
	private JecnTreeBean selectTreeBean;
	private DefaultTableModel searchTableMode = null;
	/** 查询出的结果集 */
	private List<JecnTreeBean> list;
	/** 模糊查询输入框 */
	private JSearchTextField searchField;
	/** 是否执行后台查询：true：查询；false：不查询 */
	private boolean canSearch = true;

	public JecnSearchPopup(JSearchTextField searchField) {
		this.searchField = searchField;
		searchTable = new JTable();
		this.searchField.setSearchTable(searchTable);
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add("name");
		title.add("pname");
		searchTableMode = new SearchTableMode(title);
		searchTable.setModel(searchTableMode);
		searchTable.setRowMargin(0);
		searchTable.setShowGrid(false);
		searchTable.setRowHeight(25);

		this.setLayout(new BorderLayout());
		this.setBorder(null);
		this.add(searchTable);

		searchTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					searchTableMousePressed();
				}
			}
		});
	}

	public void searchTableMousePressed() {
		int index = searchTable.getSelectedRow();
		if (list == null || list.size() == 0 || index < 0
				|| index >= list.size()) {
			return;
		}
		selectTreeBean = list.get(searchTable.getSelectedRow());
		canSearch = false;
		searchField.setText(selectTreeBean.getName());
		this.setVisible(false);
	}

	/**
	 * @author yxw 2012-8-21
	 * @description:需要隐藏的列
	 * @param column
	 */
	public void hiddenColumn(int... column) {
		if (column != null) {
			for (int c : column) {
				TableColumn tableColumnOne = searchTable.getColumnModel()
						.getColumn(c);
				tableColumnOne.setMinWidth(0);
				tableColumnOne.setMaxWidth(0);
			}
		}
	}

	public void setTableData(List<JecnTreeBean> list) {
		this.list = list;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		if (list != null&&list.size()>0) {

			for (JecnTreeBean b : list) {
				Vector<Object> v = new Vector<Object>();
				v.add(b.getId());
				v.add(b.getName());
				v.add(b.getPname());
				data.add(v);
			}
			for (int index = searchTableMode.getRowCount() - 1; index >= 0; index--) {
				searchTableMode.removeRow(index);
			}
			// 增加
			for (Vector<Object> v : data) {
				searchTableMode.addRow(v);
			}

			int height = searchTable.getRowCount() * searchTable.getRowHeight();
			// 输入框宽度
			int width = searchField.getWidth();
			Dimension d = new Dimension(width, height);
			// 将弹出窗口的大小设置为指定的宽度和高度。
			this.setPopupSize(d);
			if (!this.isVisible()) {
				this.show(searchField, 0, searchField.getHeight());
				this.searchField.requestFocusInWindow();
			}
		}
	}

	class SearchTableMode extends DefaultTableModel {

		public SearchTableMode(Vector<String> title) {
			super(title, 0);
		}

		@Override
		public Class getColumnClass(int columnIndex) {
			return super.getColumnClass(columnIndex);
		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}
	}

	public JTable getSearchTable() {
		return searchTable;
	}

	public void setSearchTable(JTable searchTable) {
		this.searchTable = searchTable;
	}

	public JecnTreeBean getSelectTreeBean() {
		return selectTreeBean;
	}

	public void setSelectTreeBean(JecnTreeBean selectTreeBean) {
		this.selectTreeBean = selectTreeBean;
	}

	public DefaultTableModel getSearchTableMode() {
		return searchTableMode;
	}

	public void setSearchTableMode(DefaultTableModel searchTableMode) {
		this.searchTableMode = searchTableMode;
	}

	public boolean isCanSearch() {
		return canSearch;
	}

	public void setCanSearch(boolean canSearch) {
		this.canSearch = canSearch;
	}
}
