package com.jecn.epros.server.connector.mengniu;

public class MengNiuProcessTaskBean {

	/** 第三方流程实例编号或单据流水号 */
	private String EngineInstanceID;

	private String HandlerUserID;

	/** 任务处理人ID,唯一员工号. */
	private String ProcessUserID;

	private String ProcessResult;

	private String ProcessRemark;
	/** OA中PC端查看已办任务的链接 */
	private String TaskPCViewURL;
	/** OA中Mobile端查看已办任务的链接 */
	private String TaskMobileViewURL;

	public String getEngineInstanceID() {
		return EngineInstanceID;
	}

	public void setEngineInstanceID(String engineInstanceID) {
		EngineInstanceID = engineInstanceID;
	}

	public String getProcessUserID() {
		return ProcessUserID;
	}

	public void setProcessUserID(String processUserID) {
		ProcessUserID = processUserID;
	}

	public String getProcessResult() {
		return ProcessResult;
	}

	public void setProcessResult(String processResult) {
		ProcessResult = processResult;
	}

	public String getProcessRemark() {
		return ProcessRemark;
	}

	public void setProcessRemark(String processRemark) {
		ProcessRemark = processRemark;
	}

	public String getTaskPCViewURL() {
		return TaskPCViewURL;
	}

	public void setTaskPCViewURL(String taskPCViewURL) {
		TaskPCViewURL = taskPCViewURL;
	}

	public String getTaskMobileViewURL() {
		return TaskMobileViewURL;
	}

	public void setTaskMobileViewURL(String taskMobileViewURL) {
		TaskMobileViewURL = taskMobileViewURL;
	}

	public String getHandlerUserID() {
		return HandlerUserID;
	}

	public void setHandlerUserID(String handlerUserID) {
		HandlerUserID = handlerUserID;
	}

	public String toString() {
		return "EngineInstanceID = " + EngineInstanceID + "  TaskPCViewURL = " + TaskPCViewURL
				+ "  TaskMobileViewURL = " + TaskMobileViewURL;
	}
}
