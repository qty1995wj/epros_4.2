package epros.designer.gui.task.jComponent;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.rule.RuleT;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.task.jComponent.JecnTaskJDialog.TaskStateEnum;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class JecnDesignerTaskCommon {
	protected static Logger log = Logger.getLogger(JecnDesignerTaskCommon.class);

	/**
	 * 部门领导
	 * 
	 * @return
	 */
	public static String getManagerPeople() {
		JecnUser jecnUser = JecnConstants.loginBean.getJecnUser();
		if (jecnUser.getManagerId() != null) {
			return String.valueOf(jecnUser.getManagerId());
		}
		return null;
	}

	/**
	 * 流程监护人
	 * 
	 * @return
	 */
	public static String getCustodyPeople(JecnTreeNode treeNode) {
		if (treeNode == null) {
			return null;
		}
		try {
			Long id = treeNode.getJecnTreeBean().getId();
			JecnFlowBasicInfoT info = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(id);
			if (info.getGuardianId() != null) {
				return String.valueOf(info.getGuardianId());
			}
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	/**
	 * 九新药业 IT 总监 默认审批人为上级架构的 责任人
	 * 
	 * @return
	 */
	public static String getItDutyPeopleJiuXin(JecnTreeNode treeNode) {
		if (treeNode == null) {
			return null;
		}
		return getLevelDuty(treeNode, 2);
	}

	/**
	 * 九新药业 事业部经理 默认审批人为上上级架构的 责任人
	 * 
	 * @return
	 */
	public static String getDivisionManDutyPeopleJiuXin(JecnTreeNode treeNode) {
		if (treeNode == null) {
			return null;
		}
		return getLevelDuty(treeNode, 1);
	}

	private static String getLevelDuty(JecnTreeNode treeNode, int level) {
		try {
			String ids = treeNode.getJecnTreeBean().getT_Path(); // 父架构的 ID
			if (StringUtils.isNotBlank(ids)) {
				String[] split = ids.split("-");
				if (split.length - 1 < level) {
					log.info(treeNode.getJecnTreeBean().getName() + "未找到对应的level" + level);
					return null;
				}
				Long id = Long.valueOf(split[level]);
				JecnFlowStructureT flow = ConnectionPool.getProcessAction().getJecnFlowStructureT(id);
				if (flow == null) {
					return null;
				}
				if (flow.getIsFlow() == 1) {
					JecnFlowBasicInfoT jecnFlowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(id);
					if (jecnFlowBasicInfoT.getTypeResPeople() != null && jecnFlowBasicInfoT.getTypeResPeople() == 0
							&& jecnFlowBasicInfoT.getResPeopleId() != null) {
						return String.valueOf(jecnFlowBasicInfoT.getResPeopleId());
					}
				} else if (flow.getIsFlow() == 0) {
					JecnMainFlowT flowMapInfo = ConnectionPool.getProcessAction().getFlowMapInfo(id);
					if (flowMapInfo.getTypeResPeople() != null && flowMapInfo.getTypeResPeople() == 0
							&& flowMapInfo.getResPeopleId() != null) {
						return String.valueOf(flowMapInfo.getResPeopleId());
					}
				}
			} else {
				log.info(treeNode.getJecnTreeBean().getName() + "对应的t_path为空");
			}
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	/**
	 * 流程文件等的专员
	 * 
	 * @return
	 */
	public static String getCommissioner(int taskType, JecnTreeNode treeNode) {
		if (treeNode == null) {
			return null;
		}
		try {
			Long id = treeNode.getJecnTreeBean().getId();
			if (taskType == 0 || taskType == 4) {
				JecnFlowStructureT flow = ConnectionPool.getProcessAction().getJecnFlowStructureT(id);
				if (flow.getCommissionerId() != null) {
					return String.valueOf(flow.getCommissionerId());
				}
			} else if (taskType == 1) {
				JecnFileBeanT file = ConnectionPool.getFileAction().getFilBeanById(id);
				if (file.getCommissionerId() != null) {
					return String.valueOf(file.getCommissionerId());
				}
			} else if (taskType == 2 || taskType == 3) {
				RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(id);
				if (ruleT.getCommissionerId() != null) {
					return String.valueOf(ruleT.getCommissionerId());
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	/**
	 * 流程文件等的责任人
	 * 
	 * @return
	 */
	public static String getDutyPeople(int taskType, JecnTreeNode treeNode) {
		if (treeNode == null) {
			return null;
		}
		try {
			Long id = treeNode.getJecnTreeBean().getId();
			if (taskType == 0) {
				JecnFlowBasicInfoT jecnFlowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(id);
				if (jecnFlowBasicInfoT.getTypeResPeople() != null && jecnFlowBasicInfoT.getTypeResPeople() == 0) {
					if (jecnFlowBasicInfoT.getResPeopleId() != null) {
						return String.valueOf(jecnFlowBasicInfoT.getResPeopleId());
					}
				}
			} else if (taskType == 4) {
				JecnMainFlowT flowMapInfo = ConnectionPool.getProcessAction().getFlowMapInfo(id);
				if (flowMapInfo.getTypeResPeople() != null && flowMapInfo.getTypeResPeople() == 0) {
					if (flowMapInfo.getResPeopleId() != null) {
						return String.valueOf(flowMapInfo.getResPeopleId());
					}
				}

			} else if (taskType == 1) {
				JecnFileBeanT filBeanById = ConnectionPool.getFileAction().getFilBeanById(id);
				if (filBeanById.getTypeResPeople() != null && filBeanById.getTypeResPeople() == 0) {
					if (filBeanById.getResPeopleId() != null) {
						return String.valueOf(filBeanById.getResPeopleId());
					}
				}
			} else if (taskType == 2 || taskType == 3) {
				RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(id);
				if (ruleT.getTypeResPeople() != null && ruleT.getTypeResPeople() == 0) {
					if (ruleT.getAttchMentId() != null) {
						return String.valueOf(ruleT.getAttchMentId());
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	/**
	 * 审批人默认顺序
	 * 
	 * @param state
	 * @param taskType
	 * @param treeNode
	 * @return
	 */
	public static String getDefaultPeopleAppId(TaskStateEnum state, int taskType, JecnTreeNode treeNode) {
		String approveId = null;
		switch (state) {
		case a1:// 文控（专员）优先查找流程文件等专员
			approveId = JecnDesignerTaskCommon.getCommissioner(taskType, treeNode);
			break;
		case a2:// 部门审核优先查找拟稿人的部门管理员
			approveId = JecnDesignerTaskCommon.getManagerPeople();
			break;
		case a4:
			// 批准人优先选流程文件的责任人
			approveId = JecnDesignerTaskCommon.getDutyPeople(taskType, treeNode);
			break;
		default:
			break;
		}
		return approveId;
	}

	/**
	 * 审批人默认顺序 九新药业 // 起草人为拟稿人，审核人为三级流程责任人及二级流程责任人，批准人为一级流程责任人。 一级责任人是事业部经理
	 * 二级责任人是it总监 三级责任人是批准人
	 * 
	 * 如下是目前的更改 4、二级责任人 取值 上级责任人--》改为取架构卡中表示的二级责任人 5、一级责任人
	 * 取值上上级责任人--》改为取架构卡中表示的一级责任人
	 * 
	 * @param state
	 * @param taskType
	 * @param treeNode
	 * @return
	 */
	public static String getDefaultsPeopleAppId(TaskStateEnum state, int taskType, JecnTreeNode treeNode) {
		String approveId = null;
		switch (state) {
		case a1:// 文控（专员）优先查找流程文件等专员
			approveId = JecnDesignerTaskCommon.getCommissioner(taskType, treeNode);
			break;
		case a2:// 部门审核优先查找监护人
			approveId = JecnDesignerTaskCommon.getCustodyPeople(treeNode);
			break;
		case a4:
			// 批准人优先选流程文件的责任人
			approveId = JecnDesignerTaskCommon.getDutyPeople(taskType, treeNode);
			break;
		case a6:
			// IT 总监 选择上级架构的责任人（改为架构二级责任人）
			approveId = JecnDesignerTaskCommon.getItDutyPeopleJiuXin(treeNode);
			break;
		case a7:
			// 事业部经理 选择上上级架构的责任人（改为架构一级责任人）
			approveId = JecnDesignerTaskCommon.getDivisionManDutyPeopleJiuXin(treeNode);
			break;
		default:
			break;
		}
		return approveId == null ? "" : approveId;
	}
}
