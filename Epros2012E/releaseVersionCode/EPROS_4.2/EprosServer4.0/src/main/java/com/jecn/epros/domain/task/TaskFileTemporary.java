package com.jecn.epros.domain.task;

import java.io.Serializable;

/**
 * 上传文件临时bean
 *
 * @author xiaohu
 */
public class TaskFileTemporary implements Serializable {
    /**
     * 主键ID JecnFileTaskBeanNew的主键
     */
    private Long id;
    /**
     * 任务ID
     */
    private Long taskId;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件流
     */
    private byte[] fileContent;
    /**
     * 密级 :0是公开，1是秘密
     */
    private Long isPublic;
    /**
     * 编号
     */
    private String docId;
    /**
     * 文件查阅权限
     */
    private String accessIds;
    /**
     * 文件查阅权限
     */
    private String accessName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getAccessIds() {
        return accessIds;
    }

    public void setAccessIds(String accessIds) {
        this.accessIds = accessIds;
    }

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }

    public Long getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Long isPublic) {
        this.isPublic = isPublic;
    }
}
