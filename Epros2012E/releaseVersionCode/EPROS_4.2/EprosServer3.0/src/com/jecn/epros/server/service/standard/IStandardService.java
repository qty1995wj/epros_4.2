package com.jecn.epros.server.service.standard;

import java.util.List;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.standard.StandardFileContent;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.standard.StandardDetailBean;
import com.jecn.epros.server.webBean.standard.StandardSearchBean;
import com.jecn.epros.server.webBean.standard.StandardWebBean;

public interface IStandardService extends IBaseService<StandardBean, Long> {

	/**
	 * @author yxw 2012-6-7
	 * @description:根据父id查找子节点
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildStandards(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据父id查找子节点
	 * @param pid
	 * @param projectId
	 * @param peopleId
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildStandardsWeb(Long pid, Long projectId, Long peopleId, boolean isAdmin)
			throws Exception;

	/**
	 * @author yxw 2012-6-14
	 * @description::根据父id查找目录节点
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildStandardDirs(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:获取所有节点
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllStandard(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-14
	 * @description:获取所有目录节点
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllStandardDir(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:标准重命名
	 * @param newName
	 * @param id
	 * @param updatePersonId
	 * @throws Exception
	 */
	public void reName(String newName, Long id, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点排序
	 * @param list
	 * @param pId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteStandard(List<Long> listIds, Long projectId, Long updatePersonId) throws Exception;

	/**
	 * 
	 * @author yxw 2012-6-8
	 * @description:上传标准文件
	 * @param list
	 * @param posIds
	 * @param orgIds
	 */
	public void addStandardFile(List<StandardBean> list, String posIds, String orgIds, String posGroupIds,
			Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据名称搜索
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception;

	/**
	 * @description:根据名称搜索标准文件
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchStandarFileByName(String name, Long projectId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：搜索标准节点
	 * @param standardId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodes(Long standardId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-10-23
	 * @description:更新标准文件
	 * @param id
	 * @param fileId
	 * @param fileName
	 * @throws Exception
	 */
	public void updateStandardFile(long id, long fileId, String fileName, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-10-23
	 * @description:更新标准流程/流程地图
	 * @param id
	 * @param processId
	 * @param name
	 * @param type
	 *            2:标准流程 3标准流程地图
	 * @throws Exception
	 */
	public void updateStandardProcess(Long id, Long processId, String name, int type, Long updatePersonId)
			throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:标准关联流程
	 * @param standardId
	 * @param flowIds
	 * @param updatePersonId
	 * @throws Exception
	 */
	public void addRelationFlow(List<StandardBean> list, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共标准查询总数
	 * @param publicSearchBean搜索条件bean
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public int getTotalPublicStandard(PublicSearchBean publicSearchBean, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共标准查询
	 * @param publicSearchBean搜索条件bean
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<PublicFileBean> getPublicStandard(PublicSearchBean publicSearchBean, int start, int limit,
			Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:
	 * @param standardSearchBean查询条件
	 * @param projectId
	 *            项目id
	 * @param l
	 * @return
	 * @throws Exception
	 */
	public int getTotalStandardList(StandardSearchBean standardSearchBean, Long peopleId, Long projectId,
			boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2013-2-22
	 * @description:根据标准目录ID查询准总数
	 * @param pId
	 *            标准目录ID
	 * @param peopleId
	 * @param projectId
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public int getTotalStandardListByPid(Long pId, long peopleId, long projectId, boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:标准分页查询
	 * @param standardSearchBean
	 *            查询条件
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @param l
	 * @return
	 * @throws Exception
	 */
	public List<StandardWebBean> getStandardList(StandardSearchBean standardSearchBean, int start, int limit,
			Long peopleId, Long projectId, boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2013-2-22
	 * @description:根据准目录ID查询目录下的标准列表
	 * @param pId
	 *            标准目录ID
	 * @param start
	 * @param limit
	 * @param peopleId
	 * @param projectId
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public List<StandardWebBean> getStandardListByPid(Long pId, int start, int limit, long peopleId, long projectId,
			boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的标准
	 * @param attenTionSearchBean查询条件
	 * @param projectId项目id
	 * @return
	 * @throws Exception
	 */
	public int getTotalAttenTionStandard(AttenTionSearchBean attenTionSearchBean, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的标准
	 * @param attenTionSearchBean
	 *            查询条件
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<StandardWebBean> getAttenTionStandard(AttenTionSearchBean attenTionSearchBean, int start, int limit,
			Long projectId) throws Exception;

	/**
	 * @author yxw 2013-2-28
	 * @description:标准相关流程
	 * @param standardId标准ID
	 * @param standardType
	 *            标准类型
	 * @return
	 * @throws Exception
	 */
	public StandardDetailBean getStandardDetail(Long standardId, int standardType) throws Exception;

	/**
	 * @author yxw 2013-11-15
	 * @description:更新标准条款要求
	 * @param id
	 * @param name
	 *            名称
	 * @param context
	 *            要求内容
	 * @throws Exception
	 */
	public void updateStanClauseRequire(Long id, String name, String context, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2014年1月7日
	 * @description:增加
	 * @param standardBean
	 * @throws Exception
	 */
	public void updateStandardBean(StandardBean standardBean) throws Exception;

	/**
	 * @author yxw 2014年1月7日
	 * @description:更新
	 * @param standardBean
	 * @return
	 * @throws Exception
	 */
	public Long addStandard(StandardBean standardBean) throws Exception;

	/**
	 * 根据标准ID获取关联的流程节点ID 和 节点类型
	 * 
	 * @param standardId
	 * @return
	 * @throws Exception
	 */
	public Object[] getRelFlowObject(Long standardId) throws Exception;

	List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopelId) throws Exception;

	/**
	 * 角色权限下：标准权限选择
	 * 
	 * @param pid
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	List<JecnTreeBean> getRoleAuthChildStandards(Long pid, Long projectId, Long peopleId) throws Exception;

	/**
	 * 流程相关标准
	 * 
	 * @param flowId
	 * @throws Exception
	 */
	public List<JecnTreeBean> getStandardByFlowId(Long flowId) throws Exception;

	List<StandardFileContent> getStandardFileContents(Long relatedId) throws Exception;

	FileOpenBean openFileContent(Long id) throws Exception;

	public ByteFileResult importData(long userId, byte[] b, Long id, String type, Long projectId, boolean CH);
}
