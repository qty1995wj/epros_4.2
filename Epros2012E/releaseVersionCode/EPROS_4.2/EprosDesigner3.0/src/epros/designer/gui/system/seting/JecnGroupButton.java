package epros.designer.gui.system.seting;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 组合按钮：按钮内部存在左右两个按钮实现不同功能
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnGroupButton extends JButton implements MouseListener {
	/** 装载按钮容器 */
	protected JToolBar toolBar = null;
	/** 左面按钮 */
	private JecnSelectedButton leftBtn = null;
	/** 右面按钮 */
	private JecnSelectingButton rightBtn = null;

	public JecnGroupButton(JecnSelectedButton leftBtn,
			JecnSelectingButton rightBtn) {
		if (leftBtn == null || rightBtn == null) {
			JecnLogConstants.LOG_TOOLBAR_GROUP_BUTTON.error("JecnSetingGroupButton is null：leftBtn="
							+ leftBtn + ";rightBtn=" + rightBtn);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.leftBtn = leftBtn;
		this.rightBtn = rightBtn;

		initComponents();
	}

	/**
	 * 
	 * 布局组件
	 * 
	 */
	private void initComponents() {

		toolBar = new JToolBar();

		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.setBorder(null);
		toolBar.add(this);

		// 隐藏选中虚线边框
		leftBtn.setFocusable(false);
		rightBtn.setFocusable(false);
		// 图片内容显示方式：靠左
		rightBtn.setHorizontalAlignment(SwingConstants.CENTER);

		// 无边框
		leftBtn.getJToolBar().setBorder(null);
		rightBtn.getJToolBar().setBorder(null);

		// 注册鼠标事件
		leftBtn.addMouseListener(this);
		rightBtn.addMouseListener(this);

		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 图片内容显示方式：靠左
		this.setHorizontalAlignment(SwingConstants.LEFT);
		// 不显示焦点状态
		this.setFocusPainted(false);
		this.setLayout(new BorderLayout(0, 0));
		this.add(leftBtn.getJToolBar(), BorderLayout.CENTER);
		this.add(rightBtn.getJToolBar(), BorderLayout.EAST);
	}

	public JecnSelectedButton getLeftBtn() {
		return leftBtn;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		mouseTransmit(e);
	}

	public void mouseExited(MouseEvent e) {
		mouseTransmit(e);
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}
	
	/**
	 * 
	 * 鼠标事件传递
	 * 
	 * @param e
	 */
	private void mouseTransmit(MouseEvent e) {

		if (e.getSource() instanceof JecnSelectedButton) {
			// 源按钮
			JecnSelectedButton sourceBtn = (JecnSelectedButton) e
					.getSource();

			// 目标按钮
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(
					sourceBtn, e, JecnGroupButton.this);

			// 触发给定事件
			JecnGroupButton.this.dispatchEvent(desMouseEvent);

		} else if (e.getSource() instanceof JecnSelectingButton) {
			// 源按钮
			JecnSelectingButton sourceBtn = (JecnSelectingButton) e
					.getSource();

			// 目标按钮
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(
					sourceBtn, e, JecnGroupButton.this);

			// 触发给定事件
			JecnGroupButton.this.dispatchEvent(desMouseEvent);
		}
	}
}
