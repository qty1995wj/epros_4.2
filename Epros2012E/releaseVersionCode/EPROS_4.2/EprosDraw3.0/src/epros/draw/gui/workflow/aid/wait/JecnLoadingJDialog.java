package epros.draw.gui.workflow.aid.wait;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 等待对话框，主要用显示于长时间的功能
 * 
 * @author ZHOUXY
 */
class JecnLoadingJDialog extends JecnDialog {
	/** 内容面板 */
	private JPanel mainPanel = null;
	/** 等待内容标签 */
	private JLabel textLabel;

	private JecnResourceUtil res = null;

	JecnLoadingJDialog() {
		initComponents();

	}

	private void initComponents() {
		res = new JecnResourceUtil();
		// 内容面板
		mainPanel = new JPanel();
		// 等待内容标签
		textLabel = new JLabel();

		// 模态
		this.setModal(true);
		// 关闭隐藏
		this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		// 标题
		this.setTitle(res.getValue("loadDialogTitle"));
		// 设置窗口下次可见时应该出现的位置
		this.setLocationByPlatform(true);
		// 不可拖动
		this.setResizable(false);
		// 大小
		this.setSize(250, 70);
		// 面板居中
		this.setLocationRelativeTo(JecnSystemStaticData.getFrame());
		// 退出按钮隐藏
		this.setExitBtnVisible(false);

		// 主面板
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setLayout(new BorderLayout(10, 0));
		mainPanel.setBorder(null);

		// 内容标签
		initShowDefaultText();
		textLabel.setIcon(JecnFileUtil.getImageIconforPath("/epros/draw/images/workflow/wait.gif"));
		textLabel.setOpaque(false);
		/*
		 * textLabel.setFont(new Font(textLabel.getFont().getName(), Font.BOLD,
		 * 14));
		 */
		// 布局
		mainPanel.add(textLabel);
		this.getContentPane().add(mainPanel);
	}

	/**
	 * 
	 * 设置显示内容
	 * 
	 * @param text
	 *            String
	 */
	void setShowText(String text) {
		textLabel.setText((text == null) ? "" : text);
	}

	/**
	 * 
	 * 设置默认显示内容
	 * 
	 */
	void initShowDefaultText() {
		textLabel.setText(res.getValue("loadingText"));
	}
}
