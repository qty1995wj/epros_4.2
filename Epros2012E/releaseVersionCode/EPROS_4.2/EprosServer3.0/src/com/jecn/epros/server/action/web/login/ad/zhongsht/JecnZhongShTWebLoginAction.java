package com.jecn.epros.server.action.web.login.ad.zhongsht;

import org.apache.commons.lang.StringUtils;

import com.gdcn.portal.CryptionData;
import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 
 * 中数通信息有限公司：单点登录
 * 
 * @author zhouxy
 * 
 */
public class JecnZhongShTWebLoginAction extends JecnAbstractADLoginAction {

	public JecnZhongShTWebLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 单点登录入口
	 * 
	 * @return String
	 */
	@Override
	public String login() {
		try {
			String adLoginName = loginAction.getAdLoginName();
			if (StringUtils.isBlank(adLoginName)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}

			// 下面执行单点登录方式
			// 获取解密后登录名称
			adLoginName = isLongResult(adLoginName);
			// 验证登录
			return this.loginByLoginName(adLoginName);

		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 
	 * 解析登录名称
	 * 
	 * @return String :验证成功返回解析后的登录名称；验证失败返回null
	 * 
	 */
	private String isLongResult(String adLoginName) {
		// 获取解密后的登录名称
		try {
			if (adLoginName != null && adLoginName.length() > 0) {
				adLoginName = sso.StringUtil.decodeBase64(adLoginName);
				CryptionData crypData = new com.gdcn.portal.CryptionData("portaltt");
				adLoginName = crypData.DecryptionStringData(adLoginName);
				if (adLoginName != null && adLoginName.indexOf("uid=") != -1) {// 获取登录名称
					return adLoginName.substring(adLoginName.indexOf("uid=") + 4);
				}
			}
		} catch (Exception e) {
			log.error("登录名称解析异常。", e);
		}
		return null;
	}
}
