package epros.designer.gui.system.seting;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.JecnToolbarButton;
import epros.designer.gui.task.config.TaskConfigEditDialog;
import epros.designer.util.JecnProperties;

/**
 * 
 * 配置按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTaskSet implements ActionListener {
	/** 制度配置按钮标识 */
	private final String RULE_SET_CMD = "ruleSet";
	/** 流程配置按钮标识 */
	private final String PARTMAP_SET_CMD = "partMapSet";
	/** 流程地图配置按钮标识 */
	private final String TOTALMAP_SET_CMD = "totalMapSet";
	/** 文件配置按钮标识 */
	private final String FILE_SET_CMD = "fileSet";
	/** 选择按钮标识 */
	private final String SELECTEDING_CMD = "selectedingRecover";

	/** 配置容器 */
	private JecnGroupButton groupRecoverBtn = null;
	/** 当前选中配置 */
	private JecnSelectedButton currSelectedBtn = null;
	/** 选择配置按钮 */
	private JecnSelectingButton selectingBtn = null;

	/** 制度配置 */
	private JecnToolbarButton ruleTaskButton = null;
	/** 流程配置 */
	private JecnToolbarButton processTaskButton = null;
	/** 流程地图配置 */
	private JecnToolbarButton totalMapTaskButton = null;
	/** 文件配置 */
	private JecnToolbarButton fileTaskButton = null;

	private JecnPopupMenuButton popupMenu = null;

	public JecnTaskSet() {
		// 初始化组件
		initComponent();
	}

	/**
	 * 
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {
		// 当前选中项
		currSelectedBtn = new JecnSelectedButton(this);
		// 选择列表按钮
		selectingBtn = new JecnSelectingButton();
		// 容器
		groupRecoverBtn = new JecnGroupButton(currSelectedBtn, selectingBtn);
		// 流程配置
		processTaskButton = new JecnToolbarButton(JecnProperties.getValue("flowTaskTemplate"), "processTaskConfig");
		// 流程地图配置
		totalMapTaskButton = new JecnToolbarButton(JecnProperties.getValue("flowMapTaskTemplate"), "totalMapTaskConfig");
		// 制度配置
		ruleTaskButton = new JecnToolbarButton(JecnProperties.getValue("ruleTaskTemplate"), "ruleTaskConfig");
		// 文件配置
		fileTaskButton = new JecnToolbarButton(JecnProperties.getValue("fileTaskTemplate"), "fileTaskConifg");

		List<JecnToolbarButton> btnList = new ArrayList<JecnToolbarButton>();
		btnList.add(processTaskButton);// 流程配置

		if (JecnConstants.versionType != 1) {// 除去“无浏览端标准版”
			btnList.add(totalMapTaskButton);// 流程地图配置
		}
		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {// true：
			btnList.add(ruleTaskButton);// 制度配置
		}
		if (JecnConstants.versionType != 1) {// 除去“无浏览端标准版”
			btnList.add(fileTaskButton);// 文件配置
		}

		popupMenu = new JecnPopupMenuButton(btnList);

		selectingBtn.setActionCommand(SELECTEDING_CMD);
		processTaskButton.setActionCommand(PARTMAP_SET_CMD);// 流程配置
		totalMapTaskButton.setActionCommand(TOTALMAP_SET_CMD);// 流程地图配置
		ruleTaskButton.setActionCommand(RULE_SET_CMD);// 制度配置
		fileTaskButton.setActionCommand(FILE_SET_CMD);// 文件配置

		Dimension thisSize = new Dimension(100, 20);
		// 大小
		currSelectedBtn.setPreferredSize(thisSize);
		currSelectedBtn.setMaximumSize(thisSize);
		currSelectedBtn.setMinimumSize(thisSize);
		currSelectedBtn.setSize(thisSize);

		// 选择按钮
		selectingBtn.addActionListener(this);
		// 流程配置
		processTaskButton.addActionListener(this);
		// 流程地图配置
		totalMapTaskButton.addActionListener(this);
		// 制度配置
		ruleTaskButton.addActionListener(this);
		// 文件配置
		fileTaskButton.addActionListener(this);

		currSelectedBtn.setTextIcon(processTaskButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!JecnDesignerCommon.isAdminTip()) {
			return;
		}
		String cmdStr = e.getActionCommand();

		if (SELECTEDING_CMD.equals(cmdStr)) {// 选择按钮
			popupMenu.show(currSelectedBtn, 30, 25);
			popupMenu.setPopupSize(new Dimension(currSelectedBtn.getWidth()+10, popupMenu.getHeight()));
			popupMenu.repaint();
			return;
		}

		
		if (TOTALMAP_SET_CMD.equals(cmdStr)) {// 流程地图配置
			Dimension thisSize = new Dimension(120, 20);
			// 大小
			currSelectedBtn.setPreferredSize(thisSize);
			currSelectedBtn.setMaximumSize(thisSize);
			currSelectedBtn.setMinimumSize(thisSize);
			currSelectedBtn.setSize(thisSize);
		} else {
			if (currSelectedBtn.getWidth() != 100) {
				Dimension thisSize = new Dimension(100, 20);
				// 大小
				currSelectedBtn.setPreferredSize(thisSize);
				currSelectedBtn.setMaximumSize(thisSize);
				currSelectedBtn.setMinimumSize(thisSize);
				currSelectedBtn.setSize(thisSize);
			}
		}

		if (popupMenu.isVisible()) {
			popupMenu.setVisible(false);
		}
		if (PARTMAP_SET_CMD.equals(cmdStr)) {// 流程配置
			TaskConfigEditDialog dialog=new TaskConfigEditDialog(null, null, 0);
			dialog.setVisible(true);
			//currSelectedBtn.setTextIcon(processTaskButton);
		} else if (TOTALMAP_SET_CMD.equals(cmdStr)) {// 流程地图配置
			TaskConfigEditDialog dialog=new TaskConfigEditDialog(null, null, 4);
			dialog.setVisible(true);
			//currSelectedBtn.setTextIcon(totalMapTaskButton);
		} else if (RULE_SET_CMD.equals(cmdStr)) {// 制度配置
			TaskConfigEditDialog dialog=new TaskConfigEditDialog(null, null, 2);
			dialog.setVisible(true);
			//currSelectedBtn.setTextIcon(ruleTaskButton);
		} else if (FILE_SET_CMD.equals(cmdStr)) {// 文件配置
			TaskConfigEditDialog dialog=new TaskConfigEditDialog(null, null, 1);
			dialog.setVisible(true);
			//currSelectedBtn.setTextIcon(fileTaskButton);
		}
	}

	public JecnGroupButton getGroupRecoverBtn() {
		return groupRecoverBtn;
	}
}
