package org.apache.struts2.dispatcher.ng.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class JecnStrutsPrepareAndExecuteFilter extends
		StrutsPrepareAndExecuteFilter {
	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession();
		String url = request.getRequestURI().toLowerCase();
		boolean flag = false;
		try {
			// 拦截器拦截所有action请求，但是jsp请求不能拦截，所以此处添加对jsp请求拦截
			// 是给定这些.jsp以及session已经存在时直接让进入后台
			if (url.contains(".jsp")) {
				if (url.indexOf("login.jsp") > 0
						|| session.getAttribute("webLoginBean") != null
						|| url.indexOf("loginRedirect.jsp") > 0
						|| url.indexOf("aboutDaliog.jsp") > 0) {// login.jsp或session存在情况占访问比例大，所以代码这样写
					// 默认strut2请求
					super.doFilter(req, res, chain);
				} else {
					response.getWriter().write(
							"<script language='javascript'> location.href = '"
									+ request.getContextPath()
									+ "/login.jsp';</script>");
				}
			} else if (url.contains("/webservice/")) { // 判断是否是向WebService发出的请求
				// 如果是来自向CXFService发出的请求
				chain.doFilter(req, res);
			} else {
				flag = true;
				// 默认strut2请求
				super.doFilter(req, res, chain);
			}
		} finally {
			if (!flag) {// strut2请求已经清理
				this.prepare.cleanupRequest(request);
			}
		}
	}
}