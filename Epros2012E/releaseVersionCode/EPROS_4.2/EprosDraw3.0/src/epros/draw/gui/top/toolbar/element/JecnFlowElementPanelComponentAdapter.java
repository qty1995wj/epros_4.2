package epros.draw.gui.top.toolbar.element;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import epros.draw.gui.swing.JecnBaseFlowElementPanel;

/**
 * 
 * 流程元素面板浏览区域监听事件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementPanelComponentAdapter extends ComponentAdapter {

	public JecnFlowElementPanelComponentAdapter() {
		super();
	}

	/**
	 * Invoked when the component's size changes.
	 */
	public void componentResized(ComponentEvent e) {
		componentActionPerformed(e);
	}

	private void componentActionPerformed(ComponentEvent e) {
		if (e.getSource() instanceof JecnFlowElementPanel) {

			JecnFlowElementPanel elePanel = (JecnFlowElementPanel) e
					.getSource();
			// 选中流程元素
			JecnBaseFlowElementPanel selectedBasePanel = elePanel
					.getSelectedBasePanel();

			if (selectedBasePanel == null) {
				return;
			}

			int width = selectedBasePanel.getFlowElementData().getFigureSizeX();
			int height = selectedBasePanel.getFlowElementData()
					.getFigureSizeY();

			// 居中显示
			selectedBasePanel.setBounds(
					(elePanel.getPreviewPanel().getWidth() - width) / 2,
					(elePanel.getPreviewPanel().getHeight() - height) / 2,
					width, height);
		}
	}

}
