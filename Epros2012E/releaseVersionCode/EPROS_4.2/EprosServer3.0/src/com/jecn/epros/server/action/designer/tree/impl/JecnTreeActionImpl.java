package com.jecn.epros.server.action.designer.tree.impl;

import com.jecn.epros.server.action.designer.tree.IJecnTreeAction;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.service.tree.IJecnTreeService;

/**
 * 树接口处理(其它功能待迁移)
 * 
 * @author ZXH
 * @date 2015-12-22
 */
public class JecnTreeActionImpl implements IJecnTreeAction {
	private IJecnTreeService treeService;

	public IJecnTreeService getTreeService() {
		return treeService;
	}

	public void setTreeService(IJecnTreeService treeService) {
		this.treeService = treeService;
	}

	/**
	 * 获取树节点的子节点sort最大值
	 * 
	 * @param treeBean
	 * @return sort+1
	 * @throws Exception
	 */
	@Override
	public int getTreeChildMaxSortId(JecnTreeBean treeBean) throws Exception {
		return treeService.getTreeChildMaxSortId(treeBean);
	}
}
