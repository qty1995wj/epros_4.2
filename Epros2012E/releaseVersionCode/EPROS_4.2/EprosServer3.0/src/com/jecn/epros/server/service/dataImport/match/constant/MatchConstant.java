package com.jecn.epros.server.service.dataImport.match.constant;

public class MatchConstant {

	/** 导入根节点（部门）对应的父部门编号 */
	public final static String PID = "0";
	/** 邮箱格式 */
	public final static String EMAIL_FORMAT = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
	/** 邮箱内网标识 */
	public final static int EMAIL_TYPE_INNER_INT = 0;
	/** 邮箱内网标识 */
	public final static String EMAIL_TYPE_INNER = "inner";
	/** 邮箱外网标识 */
	public final static int EMAIL_TYPE_OUTER_INT = 1;
	/** 邮箱外网标识 */
	public final static String EMAIL_TYPE_OUTER = "outer";
	// /** 用户默认密码 */
	// public final static String USER_DEFAULT_PASSWORD="CCC5C9CAC0CD";
	/** 人员编号==admin */
	public final static String USER_ADMIN = "admin";
	/** 部门标识 */
	public final static String DEPT_FLAG = "dept";
	/** 岗位标识 */
	public final static String POS_FLAG = "pos";
	/** 人员标识 */
	public final static String USER_FLAG = "user";

	/** 空格 */
	public final static String EMTRY_SPACE = "    ";
	/** 错误信息文件名称 */
	public final static String IMPORT_ERROR_FILE_NAME = "importErrorResult.txt";
	/** 基于excel导入方式：模板名称 */
	public final static String IMPOT_EXCEL_MODEL_FILE_NAME = "inputUserMouldExcel.xls";
	/** 基于excel导出方式：模板名称 */
	public final static String OUTPUT_EXCEL_MODEL_FILE_NAME = "outputUserMouldExcel.xls";
	/** 基于excel导入方式：人员数据excel存放名称 */
	public final static String IMPORT_FILE_PAHT = "inputUserDataExcel.xls";
	/** 基于excel导出方式：人员数据excel存放名称 */
	public final static String OUTPUT_EXCEL_DATA_FILE_NAME = "outputUserExcel.xls";
	/** 基于excel导入方式：人员数据excel存放名称 */
	public final static String OUTPUT_EXCEL_ERROR_DATA_NAME = "outputErrowUserDataExcel.xls";
	/** 基于excel导出方式：人员数据excel存放名称 */
	public final static String OUTPUT_EXCEL_ERROR_MOULD_NAME = "outputErrowUserMouldExcel.xls";

	/** excel写入时，标记从第几行开始写 */
	public final static int WIRTER_ROW_NUM = 3;
	/** ******************返回值 Start******************* */
	/** 返回前台表示成功字符串 */
	public final static String RESULT_SUCCESS = "success";
	/** 返回前台表示失败字符串 */
	public final static String RESULT_FAIL = "fail";
	/** ******************返回值 end******************* */

	/** 大唐使用：dataImport_viewParams */

	/** ******************导入方式 Start******************* */
	/** 基于excel导入方式 */
	public final static String IMPORT_TYPE_EXCEL = "excel";
	/** 基于xml导入方式 */
	public final static String IMPORT_TYPE_XML = "xml";
	/** 基于hessian导入方式 */
	public final static String IMPORT_TYPE_HESSIAN = "hessian";
	/** 基于db导入方式 */
	public final static String IMPORT_TYPE_DB = "db";
	/** ******************导入方式 Start******************* */

	/** 自动同步标识 */
	public final static String AUTO_SYNC_USER_DATA_FLAG = "1";
	/** 手动同步标识 */
	public final static String CONTROL_SYNC_USER_DATA_FLAG = "0";
	/** 验证是否由中文、英文、数字及“_”、“-”组成 */
	public final static String IN_STRING = "[a-z]*[A-Z]*\\d*-*_*[/]*[(]*[)]*[,]*[（]*[）]*\\s*[\\u4e00-\\u9fa5]*";

	/** 若存在新加的岗位匹配关系 则跳转到当前页面 */
	public static final String EXTIS_REL_LOCATION = "pageData.do";

}
