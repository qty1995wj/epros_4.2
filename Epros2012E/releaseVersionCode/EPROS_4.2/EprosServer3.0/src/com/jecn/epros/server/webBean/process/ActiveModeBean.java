package com.jecn.epros.server.webBean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.file.FileWebBean;

public class ActiveModeBean implements Serializable {
	// 样例
	private List<FileWebBean> listTemplet;
	// 表单
	private FileWebBean modeFile;

	private int templetSize;

	public List<FileWebBean> getListTemplet() {
		return listTemplet;
	}

	public void setListTemplet(List<FileWebBean> listTemplet) {
		this.listTemplet = listTemplet;
	}

	public FileWebBean getModeFile() {
		return modeFile;
	}

	public void setModeFile(FileWebBean modeFile) {
		this.modeFile = modeFile;
	}

	public int getTempletSize() {
		if (listTemplet != null) {
			this.templetSize = listTemplet.size();
		}
		return templetSize;
	}
}
