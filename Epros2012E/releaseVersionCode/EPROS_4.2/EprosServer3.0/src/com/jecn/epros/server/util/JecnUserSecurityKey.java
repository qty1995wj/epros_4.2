package com.jecn.epros.server.util;

public class JecnUserSecurityKey {
	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：用户密码解密
	 * @param password
	 * @return
	 */

	public static String getdesecret(String password) {

		String value;
		char[] A1 = new char[40];
		char[] fname1 = new char[40];
		char[] A2 = new char[40];
		int t2[] = { 5, 14, 12, 9, 11, 2, 0, 15, 7, 3, 4, 13, 1, 6, 10, 8 };
		char t3[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		int i = 0, j = 0, k, p, q;
		int key = 32;
		int temp;
		fname1 = password.toCharArray();
		k = password.length();
		while ((i < k) && (fname1[i] != '\0')) {
			A1[j] = fname1[i];
			p = 0;
			while ((p < 16) && (A1[j] != t3[p++]))
				;
			A2[j] = fname1[i + 1];
			q = 0;
			while ((q < 16) && (A2[j] != t3[q++]))
				;
			A1[j] = (char) (t2[p - 1] * 16 + t2[q - 1]);
			temp = A1[j];
			temp = temp ^ key;
			A1[j] = (char) temp;
			i = i + 2;
			j++;
		}
		A1[j] = 0;
		value = String.valueOf(A1).trim();
		return (value);
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：用户密码加密
	 * @param password
	 * @return
	 */
	public static String getsecret(String password) {
		String value;
		char[] A1 = new char[40];
		char[] A2 = new char[40];
		int t1[] = { 6, 12, 5, 9, 10, 0, 13, 8, 15, 3, 14, 4, 2, 11, 1, 7 };
		char t3[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		int i = 0, j = 0, x, y, k;
		int key = 32;
		byte temp;
		int temp1;
		A1 = password.toCharArray();
		k = password.length();
		while (j < k) {
			temp = (byte) A1[j];
			temp = (byte) ((temp) ^ key);
			A1[j] = (char) temp;

			x = temp / 16;
			y = temp % 16;
			A2[i++] = t3[t1[x]];
			temp1 = i - 1;
			A2[i++] = t3[t1[y]];
			temp1 = i - 1;
			j++;
		}
		A2[i] = '\0';
		value = String.valueOf(A2).trim();
		return (value);
	}

	/**
	 * 判断此用户是不是在别的电脑上登录过
	 * 
	 * @param macAddressClient
	 * @param macAddress
	 * @return
	 */
	public static boolean isRegistration(String macAddressClient, String macAddress) {
		if (macAddress == null || "".equals(macAddress)) {
			return false;
		}
		String[] strMac = macAddressClient.split(",");
		for (String str : strMac) {
			if (str.equals(macAddress)) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {

		System.out.println(JecnUserSecurityKey.getdesecret("CDC0CAC9C5CC"));
	}
}
