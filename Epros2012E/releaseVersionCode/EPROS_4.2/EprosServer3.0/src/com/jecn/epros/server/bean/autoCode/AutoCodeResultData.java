package com.jecn.epros.server.bean.autoCode;

import java.io.Serializable;

public class AutoCodeResultData implements Serializable {
	private String code;
	/** 目录的编号规则 */
	private String dirCode;
	private int codeTotal;

	private String error;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getCodeTotal() {
		return codeTotal;
	}

	public void setCodeTotal(int codeTotal) {
		this.codeTotal = codeTotal;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getDirCode() {
		return dirCode;
	}

	public void setDirCode(String dirCode) {
		this.dirCode = dirCode;
	}

}
