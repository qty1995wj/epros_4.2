package epros.designer.gui.popedom.role.choose;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class RoleManagePosGroupChooseTreeListener extends JecnTreeListener {
	private static Logger log = Logger.getLogger(RoleManagePosGroupChooseTreeListener.class);
	private JecnHighEfficiencyTree jTree = null;

	public RoleManagePosGroupChooseTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = null;
				if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.positionGroup) {
					list = ConnectionPool.getPosGroup().getPosByPositionGroupId(node.getJecnTreeBean().getId());
				} else {
					list = ConnectionPool.getPosGroup().getChildPositionGroup(node.getJecnTreeBean().getId(),
							JecnConstants.projectId);
				}
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("PosGroupChooseTreeListener treeExpanded is error!", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}
}
