package epros.draw.designer;

public class JecnFlowInoutPosT {
	private String id;
	private Long flowId;
	private String inoutId;
	private String rName;
	private Long rId;
	/**
	 * 0岗位 1岗位组
	 */
	private Integer rType;

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInoutId() {
		return inoutId;
	}

	public void setInoutId(String inoutId) {
		this.inoutId = inoutId;
	}

	public Long getrId() {
		return rId;
	}

	public void setrId(Long rId) {
		this.rId = rId;
	}

	public Integer getrType() {
		return rType;
	}

	public void setrType(Integer rType) {
		this.rType = rType;
	}

	public String getrName() {
		return rName;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

}
