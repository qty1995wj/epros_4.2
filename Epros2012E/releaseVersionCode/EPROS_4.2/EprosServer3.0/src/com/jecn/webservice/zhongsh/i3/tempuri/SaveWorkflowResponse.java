
package com.jecn.webservice.zhongsh.i3.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.jecn.webservice.zhongsh.i3.core.EResult;


/**
 * <p>anonymous complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�����ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SaveWorkflowResult" type="{i3.Workflow.Core.EResult}EResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "saveWorkflowResult"
})
@XmlRootElement(name = "SaveWorkflowResponse")
public class SaveWorkflowResponse {

    @XmlElementRef(name = "SaveWorkflowResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
    protected JAXBElement<EResult> saveWorkflowResult;

    /**
     * ��ȡsaveWorkflowResult���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EResult }{@code >}
     *     
     */
    public JAXBElement<EResult> getSaveWorkflowResult() {
        return saveWorkflowResult;
    }

    /**
     * ����saveWorkflowResult���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EResult }{@code >}
     *     
     */
    public void setSaveWorkflowResult(JAXBElement<EResult> value) {
        this.saveWorkflowResult = value;
    }

}
