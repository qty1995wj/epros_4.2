package com.jecn.epros.server.common;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.jecn.epros.server.service.system.IJecnUserLoginLogService;
import com.jecn.epros.server.util.ApplicationContextUtil;

/***
 * 登录日志，在线用户session失效监听
 * @author 201305
 *
 */
public class OnlineUserListener implements  HttpSessionListener	 {

	@Override
	public void sessionCreated(HttpSessionEvent event) {
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		HttpSession session = event.getSession(); 
        //获得配置文件登录日志操作类的ID，得到该类借口
		IJecnUserLoginLogService userLoginLogService = (IJecnUserLoginLogService) ApplicationContextUtil
		.getContext().getBean("jecnUserLoginLogServiceImpl");
		//更新登录日志
		userLoginLogService.updateUserLoginLog(session.getId().toString(),1,0);
	}
}
