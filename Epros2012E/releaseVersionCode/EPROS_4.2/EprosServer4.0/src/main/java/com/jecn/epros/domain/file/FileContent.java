package com.jecn.epros.domain.file;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class FileContent implements java.io.Serializable {
    /**
     * 主键
     */
    private long id;
    /**
     * 文件主表主键
     */
    private long fileId;
    /**
     * 文件内容 存放数据库
     */
    private byte[] fileStream;
    private byte[] bytes;
    /**
     * 文件路径 存放本地
     */
    private String filePath;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateTime;
    /**
     * 更新人
     */
    private Long updatePeopleId;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 是否是临时版本 0是临时，1是正式
     */
    private int type;
    /**
     * 文件版本存储标识 0:本地；1：数据库
     */
    private int isVersionLocal;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public byte[] getFileStream() {
        return fileStream;
    }

    public void setFileStream(byte[] fileStream) {
        this.fileStream = fileStream;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdatePeopleId() {
        return updatePeopleId;
    }

    public void setUpdatePeopleId(Long updatePeopleId) {
        this.updatePeopleId = updatePeopleId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIsVersionLocal() {
        return isVersionLocal;
    }

    public void setIsVersionLocal(int isVersionLocal) {
        this.isVersionLocal = isVersionLocal;
    }


}
