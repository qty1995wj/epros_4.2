package epros.draw.gui.line;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.shape.JecnLineTextJPanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 
 * 线基类
 * 
 * 此类主要包括：线内容面板、开始结束点
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnBaseLinePanel extends JecnBaseFlowElementPanel {
	/** 记录连接线小线段集合 */
	protected List<LineSegment> lineSegments = null;
	/** 线上内容面板 */
	protected JecnLineTextJPanel lineTextJPanel = null;
	/** 拖动连接线 生成虚拟线段默认最原始点 */
	protected Point tempOldPoint = null;
	/** 线的开始图形 */
	protected JecnBaseFigurePanel startFigure = null;
	/** 线的结束图形 */
	protected JecnBaseFigurePanel endFigure = null;

	public JecnBaseLinePanel(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
		initComponents();
	}

	private void initComponents() {
		// 记录连接线小线段集合
		lineSegments = new ArrayList<LineSegment>();
		//
		lineTextJPanel = new JecnLineTextJPanel(this);
	}

	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		// 重新计算画图面板的大小
//		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
//				.getWorkflow();
//		if (workflow != null) {
//			workflow.getCurrDrawPanelSize(this);
//		}
	}

	public Point getTempOldPoint() {
		return tempOldPoint;
	}

	public void setTempOldPoint(Point tempOldPoint) {
		this.tempOldPoint = tempOldPoint;
	}

	public List<LineSegment> getLineSegments() {
		return lineSegments;
	}

	public JecnBaseFigurePanel getStartFigure() {
		return startFigure;
	}

	public void setStartFigure(JecnBaseFigurePanel startFigure) {
		this.startFigure = startFigure;
	}

	public JecnBaseFigurePanel getEndFigure() {
		return endFigure;
	}

	public void setEndFigure(JecnBaseFigurePanel endFigure) {
		this.endFigure = endFigure;
	}
}
