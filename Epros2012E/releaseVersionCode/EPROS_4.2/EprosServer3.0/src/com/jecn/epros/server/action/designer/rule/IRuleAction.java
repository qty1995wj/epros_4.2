package com.jecn.epros.server.action.designer.rule;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.integration.JecnRuleRiskBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleStandardBeanT;
import com.jecn.epros.server.bean.rule.AddRuleFileData;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.JecnRuleData;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.sync.SyncRules;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IRuleAction {

	/**
	 * @author yxw 2012-6-7
	 * @description:增加制度或制度目录
	 * @param rule
	 * @return
	 * @throws Exception
	 */
	public Long addRule(RuleT rulet) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildRules(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-14
	 * @description:根据PID查询出制度目录
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildRuleDirs(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-14
	 * @description:查询所有的制度目录
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllRuleDir(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:重命名
	 * @param newName
	 * @param id
	 * @param updatePersonId
	 *            修改人
	 * @param userType
	 *            0：管理员
	 * @throws Exception
	 */
	public void reName(String newName, Long id, Long updatePersonId, int userType) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据名称搜索
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String name, Long projectId, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点排序
	 * @param list
	 * @param pId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:真删除制度
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteRule(List<Long> listIds, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:假删除制度
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteFalseRule(Set<Long> setIds, Long updatePersonId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:上传制度文件
	 * @param ruleId
	 * @param ids
	 * @throws Exception
	 */
	public void addRuleFile(AddRuleFileData ruleFileData) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:更新制度信息
	 * @param ruelBean
	 * @param ruleContentList
	 * @param ruleFileList
	 * @throws Exception
	 */
	public Long updateRule(JecnRuleData ruleData) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:新建制度信息
	 * @param ruelBean
	 * @param ruleContentList
	 * @param ruleFileList
	 * @throws Exception
	 */
	public Long addRule(RuleT ruelBean, String posIds, String orgIds, Long modeId, Long updatePersonId)
			throws Exception;

	/**
	 * @author zhangchen Oct 8, 2010
	 * @description： 根据制度ID获得制度操作说明信息
	 * @param ruleId
	 * @return
	 * @throws Exception
	 */
	public List<JecnRuleOperationCommon> findRuleOperationShow(Long ruleId) throws Exception;

	/**
	 * @author yxw 2012-6-14
	 * @description:根据制度ID查询制度的基本信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public RuleT getRuleT(Long id) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description 根据制度ID查出父节点及兄弟节点，用于树定位
	 * @param ruleId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodes(Long ruleId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-10-23
	 * @description:更新制度文件
	 * @param id
	 * @param fileId
	 * @param fileName
	 * @throws Exception
	 */
	public void updateRuleFile(long id, long fileId, String fileName, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-10-25
	 * @description:设置制度模板
	 * @param ruleId
	 * @param modeId
	 * @throws Exception
	 */
	public void updateRuleMode(long ruleId, long modeId, Long updatePersonId, Long longFlag) throws Exception;

	/**
	 * 下载制度操作说明
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public JecnCreateDoc getRuleFile(Long ruleId, boolean isPub) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:撤销发布
	 * @param id
	 *            制度ID
	 * @param type
	 *            节点类型
	 * @param peopleId
	 * @throws Exception
	 */
	public void cancelRelease(Long id, TreeNodeType type, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:直接发布
	 * @param id
	 *            制度ID
	 * @param type
	 *            节点类型
	 * @param peopleId
	 * @throws Exception
	 */
	public void directRelease(Long id, TreeNodeType type, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-12-12
	 * @description:制度模板文件新增验证重名
	 * @param name
	 *            名称
	 * @param pid
	 *            上级Id
	 * @return true 重名 false 不重名
	 * @throws Exception
	 */
	public boolean validateAddName(String name, Long pid) throws Exception;

	/***
	 * 验证上传的文件是否已经存在
	 * 
	 * @param names
	 * @param pId
	 *            父节点ID
	 * @return
	 * @throws Exception
	 */
	public String[] validateAddNames(List<String> names, Long pId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据名称搜索制度文件，制度模板文件
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchISNotRuleDirByName(String name, Long projectId) throws Exception;

	/**
	 * 制度相关风险
	 * 
	 * @param riskTList
	 *            制度相关风险集合
	 * @param ruleId
	 *            制度主键ID
	 */
	void editRuleRiskT(List<JecnRuleRiskBeanT> riskTList, Long ruleId) throws Exception;

	/**
	 * 
	 * 制度相关标准
	 * 
	 * @param standardTList
	 *            制度相关标准集合
	 * @param ruleId
	 *            主动主键ID
	 */
	void editRuleStandardT(List<JecnRuleStandardBeanT> standardTList, Long ruleId) throws Exception;

	/**
	 * G020打开制度文件
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public FileOpenBean g020OpenRuleFile(long id) throws Exception;

	/**
	 * 更新本地上传的制度文件的内容
	 * 
	 * @param ruleT
	 */
	public Long updateLocalRuleFile(RuleT ruleT) throws Exception;

	List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception;

	public List<JecnTreeBean> getRuleTreeBeanByFlowId(Long ruleId) throws Exception;

	public List<JecnTreeBean> getRuleTreeBeanByRuleId(Long flowId) throws Exception;

	List<JecnTreeBean> getStandardizedFiles(Long ruleId) throws Exception;

	/***
	 * 获取回收树节点数据（model）
	 * 
	 * @param projectId
	 *            项目ID
	 * @param peopleId
	 *            人员id
	 * @param isAdmin
	 *            是否管理员
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pId) throws Exception;

	/**
	 * 搜索 回收站
	 * 
	 * @param name
	 *            搜索的名称
	 * @param projectId
	 *            项目id
	 * @param fileIds
	 *            有权限的文件与文件夹的id的集合
	 * @param isAdmin
	 *            是否为管理员
	 */
	public List<JecnTreeBean> getAllFilesByName(String name, Long projectId, Set<Long> ruleIds, boolean isAdmin)
			throws Exception;

	/***
	 * 获取要恢复的文件目录的子节点
	 * 
	 * @return
	 * @param listIds
	 *            文件目录Id集合
	 * @throws Exception
	 */
	public void getRecycleFileIds(List<Long> listIds, Long projectId) throws Exception;

	/**
	 * 文件回收站：恢复删除的文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	public void updateRecycleFile(List<Long> ids, Long projectId) throws Exception;

	/**
	 * 制度回收站:定位
	 * 
	 * @param id
	 *            需要定位的节点的id
	 * @param projectId
	 *            项目id
	 * @return
	 */
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception;

	public List<G020RuleFileContent> getG020RuleFileContents(Long fileId);

	public void updateRecovery(long fileId, long versionId, long updatePersonId) throws Exception;

	public void deleteVersion(List<Long> listIds, Long fileId, long userId) throws Exception;

	public FileOpenBean openVersion(Long valueOf, Long valueOf2) throws Exception;

	Long createRelatedFileDirByPath(Long id, Long projectId, Long createPeopleId) throws Exception;

	Long getRelatedFileId(Long ruleId) throws Exception;

	public boolean isNotAbolishOrNotDelete(Long id) throws Exception;

	boolean validateAddNameDirRepeat(Long pid, String name) throws Exception;

	public String validateNamefullPath(String ruleName, Long id, int type);

	public String[] validateNamefullPath(List<String> names, Long id, int type);

	public void batchReleaseNotRecord(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception;

	public void batchCancelRelease(List<Long> ids, TreeNodeType treeNodeType, Long peopleId);

	public void syncRules(SyncRules syncRule) throws Exception;

	public List<JecnTreeBean> getChildRules(Long pid, Long projectId, int... delState) throws Exception;

}
