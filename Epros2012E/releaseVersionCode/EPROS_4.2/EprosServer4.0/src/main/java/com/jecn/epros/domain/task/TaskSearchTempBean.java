package com.jecn.epros.domain.task;

import io.swagger.annotations.ApiModelProperty;

public class TaskSearchTempBean {

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty(value = "创建人Id")
    private Long createPeopleId;

    @ApiModelProperty(value = "任务阶段<br>[0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段]", allowableValues = "0,1,2,3,4,5,6,7")
    private int taskStage = -1;

    @ApiModelProperty(value = "任务类型<br>[0-流程任务,1-文件任务,2-,3-制度任务,4-流程地图任务]", allowableValues = "0,1,2,3,4")
    private int taskType = -1;

    @ApiModelProperty(value = "开始时间 ")
    private String startTime;

    @ApiModelProperty("结束时间 ")
    private String endTime;

    @ApiModelProperty("搁置任务")
    private boolean delayTask = false;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Long getCreatePeopleId() {
        return createPeopleId;
    }

    public void setCreatePeopleId(Long createPeopleId) {
        this.createPeopleId = createPeopleId;
    }

    public int getTaskStage() {
        return taskStage;
    }

    public void setTaskStage(int taskStage) {
        this.taskStage = taskStage;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isDelayTask() {
        return delayTask;
    }

    public void setDelayTask(boolean delayTask) {
        this.delayTask = delayTask;
    }

}
