﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!--  流程地图详情 -->
		<title><s:text name="flowMapChartForDetails" /></title>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript" src="${basePath}js/common.js">
</script>
		<script type="text/javascript"
			src="${basePath}js/epros_${language}_${country}.js">
</script>
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js">
</script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js">
</script>
		<script type="text/javascript" src="${basePath}jquery/jquery-1.6.2.js">
</script>
		<script type="text/javascript" src="${basePath}js/firstPage.js"
			charset="UTF-8">
</script>
		<script type="text/javascript"
			src="${basePath}js/processSys/process.js">
</script>
		<script type="text/javascript" src="${basePath}js/rule/ruleSys.js">
</script>
		<script type="text/javascript"
			src="${basePath}js/processSys/processList.js">
</script>
<script type="text/javascript" src="${basePath}js/browserDetect.js" charset="UTF-8"></script>
		<script type="text/javascript">
var versionType = ${versionType};
Ext.onReady(function() {
	var processMapMainPanel = new Ext.TabPanel( {
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				region : 'center',
				items : [
						{
							title : i_processMap,// 流程地图
							id : "ruleSysContent_id",
							autoLoad : "${basePath}"
									+ "jsp/processSys/"+processMapContainerJsp+"?isPub=true&mapID="
									+ "${param.flowId}" + "&mapName=" + "${param.name}"
									+ "&mapType=processMap" +"&target="+"${param.target}"
						},
						{
							title : i_fileDescription,// 文件说明
							id : "processOperateDes_id",
							autoLoad : {
								url : "${basePath}" + "processMapFile.action?flowId="
										+ "${param.flowId}",
								scripts : true,
								callback : function() {
									setShowProcessFileDivWH('processMapFileDiv');
								}
							}
						},
						{
							title : i_relateRule,// 相关制度
							id : "processMapRelateRule_id",
							autoLoad : {
								     url : "${basePath}" + 
								        "jsp/processSys/processMap/ProcessMapRelateRule.jsp",
										scripts:true,
								     callback:function(){
										processMapRelateRuleGrid(${param.flowId},true);
									}
							}
							},
						{
							title : i_processList,// 流程清单
							id : "processList_id",
							autoLoad : {
								url : "${basePath}"
										+ "jsp/processSys/processMap/processList.jsp",
								callback : function() {
									processListJson("${param.flowId}",false,true);
								}
							}
						},
						{
							title : i_historyInfo,// 文控信息
							id : "historyInfo_id",
							autoLoad : {
								url : "${basePath}"
										+ "processHistoryNew.action?flowId="
										+ "${param.flowId}",
								scripts : true,
								callback : function() {
									setShowDocument('processDocumentDiv');
								}
							}
						} ],
				listeners : {
		
					'bodyresize' : function(p, width, height) {
						if (p.getActiveTab() == null) {
							return;
						}
						if (p.getActiveTab().getId() == 'processOperateDes_id') {
							setShowProcessFileDivWH('processMapFileDiv');
						} else if (p.getActiveTab().getId() == 'processList_id') {
							setShowDivWidthHeight('showDivList');
							setProcessListDivWidth();
						} else if (p.getActiveTab().getId() == 'historyInfo_id') {
							setShowDocument('processDocumentDiv');
						}
					},
					'tabchange' : function(tabPanel, panel) {
						if (typeof (panel) == "undefined") {
							return;
						}
						if (panel.getId() == 'processOperateDes_id') {
							setShowProcessFileDivWH('processMapFileDiv');
						} else if (panel.getId() == 'processList_id') {
							if (document.getElementById('showDivList')) {
								setShowDivWidthHeight('showDivList');
								setProcessListDivWidth();
							}
		
						} else if (panel.getId() == 'historyInfo_id') {
							setShowDocument('processDocumentDiv');
						}
		
					}
		
				}
		
			});
	if(!${mapFileShow}){
		processMapMainPanel.remove('processOperateDes_id');
	}
	if(!${webLoginBean.isAdmin} && !${webLoginBean.isViewAdmin}){
		processMapMainPanel.remove('processList_id');
	}
	if(!${isDocControlPermission}&&!${webLoginBean.isAdmin}&&!${webLoginBean.isViewAdmin}){
		processMapMainPanel.remove('historyInfo_id');
	}
	if(!versionType){
		processMapMainPanel.remove('processMapRelateRule_id');
	}
	var northPanle = new Ext.Panel( {
		height : 20,
		region : 'north',
		contentEl : 'homePage_id'
	});
	new Ext.Viewport( {
		layout : 'border',
		items : [ northPanle, processMapMainPanel ]
	});
});
var basePath = "${basePath}";
</script>
	</head>

	<body>
		<div id="homePage_id"  style="height: 25px; width: 100%; overflow: hidden;"
				align="right" class="gradient minWidth">
				<%@include file="/navigation.jsp"%>
		</div>
	</body>
</html>