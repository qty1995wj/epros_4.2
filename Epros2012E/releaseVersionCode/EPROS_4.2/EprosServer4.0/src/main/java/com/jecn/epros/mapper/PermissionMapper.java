package com.jecn.epros.mapper;

import com.jecn.epros.sqlprovider.PermissionSqlProvider;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

public interface PermissionMapper {


    @SelectProvider(type = PermissionSqlProvider.class, method = "getOrgAccessPermissions")
    public List<Map<String, Object>> getOrgAccessPermissions(Map<String, Object> map);

    @SelectProvider(type = PermissionSqlProvider.class, method = "getPositionAccessPermissions")
    public List<Map<String, Object>> getPositionAccessPermissions(Map<String, Object> paramMap);

    @SelectProvider(type = PermissionSqlProvider.class, method = "getPositionAccessPermissionsGroup")
    public List<Map<String, Object>> getPositionAccessPermissionsGroup(Map<String, Object> paramMap);

    @DeleteProvider(type = PermissionSqlProvider.class, method = "deletePosPermission")
    public void deletePosPermission(Map<String, Object> paramMap);

    @DeleteProvider(type = PermissionSqlProvider.class, method = "deleteOrgPermission")
    public void deleteOrgPermission(Map<String, Object> paramMap);

    @DeleteProvider(type = PermissionSqlProvider.class, method = "deletePosGroupPermission")
    public void deletePosGroupPermission(Map<String, Object> paramMap);

    @DeleteProvider(type = PermissionSqlProvider.class, method = "insertOrgPerm")
    public void insertOrgPerm(Map<String, Object> paramMap);

    @DeleteProvider(type = PermissionSqlProvider.class, method = "insertPosPerm")
    public void insertPosPerm(Map<String, Object> paramMap);

    @DeleteProvider(type = PermissionSqlProvider.class, method = "insertPosGroupPerm")
    public void insertPosGroupPerm(Map<String, Object> paramMap);

}
