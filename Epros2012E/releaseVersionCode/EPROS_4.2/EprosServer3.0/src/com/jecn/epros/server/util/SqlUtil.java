package com.jecn.epros.server.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;

public class SqlUtil {

	/**
	 * 获得日期相等函数，精确到日
	 * 
	 * @param dbTimeColumn
	 *            数据库中要比较的字段
	 * @param inputTime
	 *            要比较的实际时间
	 * @return
	 */
	public static String getEqualAccuracyDay(String dbTimeColumn, Date inputTime) {
		String formater = "yyyy-MM-dd";
		String result = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			SimpleDateFormat f = new SimpleDateFormat(formater);
			String time = f.format(inputTime);
			result = "to_date(to_char(" + dbTimeColumn + ", '" + formater + "'),'" + formater + "')=to_date('" + time
					+ "','" + formater + "')";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			SimpleDateFormat f = new SimpleDateFormat(formater);
			String time = f.format(inputTime);
			result = "CONVERT(varchar(10), " + dbTimeColumn + ", 23)='" + time + "'";
		} else {
			throw new IllegalArgumentException("getEqualAccuracyDay错误的数据库类型");
		}
		return result;
	}

	/**
	 * 昨天，精确到日
	 * 
	 * @return
	 */
	public static Date getYesterdayAccuracyDay() {
		Date now = new Date();
		Calendar instance = Calendar.getInstance();
		instance.setTime(now);
		instance.set(Calendar.DAY_OF_MONTH, instance.get(Calendar.DAY_OF_MONTH) - 1);
		instance.set(Calendar.HOUR, 0);
		instance.set(Calendar.MINUTE, 0);
		instance.set(Calendar.MILLISECOND, 0);
		Date yesterDay = instance.getTime();
		return yesterDay;
	}

}
