package com.jecn.epros.server.service.system;

import com.jecn.epros.server.common.IBaseService;

/**
 * 邮件发送给系统管理员通知系统到期
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-25 时间：下午02:47:27
 */
public interface ISystemSendEmailToAdmin extends IBaseService<Long, Long> {
	/**
	 * 服务监听，到期发送邮件提醒
	 * 
	 * @throws Exception
	 */
	void systemServerSendEmail() throws Exception;
}
