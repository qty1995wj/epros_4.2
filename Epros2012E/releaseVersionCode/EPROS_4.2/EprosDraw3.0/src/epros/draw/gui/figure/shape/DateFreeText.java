package epros.draw.gui.figure.shape;

import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseRect;

/**
 * 最后更改时间文本框
 * 
 * @author ZHANGXH
 * @date： 日期：Jun 25, 2012 时间：11:35:43 AM
 */
public class DateFreeText extends BaseRect {

	public DateFreeText(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
	}
}
