package com.jecn.epros.server.service.task.app.impl;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.task.IJecnTaskApproveTipDao;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.task.app.IJecnTaskApproveTipService;
import com.jecn.epros.server.util.JecnUtil;

/**
 * @author weidp
 * @date： 日期：2014-5-14 时间：下午05:00:11
 */
public class JecnTaskApproveTipServiceImpl extends AbsBaseService<JecnUser, Long> implements IJecnTaskApproveTipService {

	/** 任务超时提醒数据接口 */
	private IJecnTaskApproveTipDao jecnTaskApproveTipDao;

	@Override
	public void sendTipEmail() throws Exception {
		
		String tipEnable = JecnConfigTool.getItemValue(ConfigItemPartMapMark.mailTaskApproveTip.toString());

		// 未起用就直接退出
		if ("0".equals(tipEnable)) {
			return;
		}
		// 获取超时提醒天数
		int tipDay = Integer.valueOf(JecnConfigTool.getItemValue(ConfigItemPartMapMark.mailTaskApproveTipValue
				.toString()));
		// 根据超时提醒天数来获取 超过提醒天数的人员信息
		List<Object[]> tipUserList = jecnTaskApproveTipDao.getTaskTipUserListByTipDay(tipDay);

		List<JecnUser> users = null;
		JecnUser user = null;
		BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_APPROVE_DELAY);
		for (Object[] obj : tipUserList) {
			if (obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
				continue;
			}
			user = new JecnUser();
			user.setPeopleId(Long.valueOf(obj[1].toString()));
			user.setEmail(obj[2].toString());
			user.setEmailType(obj[3].toString());
			users = new ArrayList<JecnUser>();
			users.add(user);
			JecnTaskBeanNew taskBean = new JecnTaskBeanNew();
			taskBean.setId(Long.valueOf(obj[0].toString()));
			taskBean.setTaskName(obj[4].toString());
			taskBean.setTaskType(Integer.valueOf(obj[5].toString()));
			emailBuilder.setData(users, taskBean);
			List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
			// TODO :EMAIL
			JecnUtil.saveEmail(buildEmail);
		}
	}

	public IJecnTaskApproveTipDao getJecnTaskApproveTipDao() {
		return jecnTaskApproveTipDao;
	}

	public void setJecnTaskApproveTipDao(IJecnTaskApproveTipDao jecnTaskApproveTipDao) {
		this.jecnTaskApproveTipDao = jecnTaskApproveTipDao;
	}

}
