package com.jecn.epros.server.webBean.dataImport.match;

/**
 * 
 * 岗位匹配组织页面获取数据
 * 
 * @author Administrator
 * 
 */
public class StationMatchTempBean {
	private Long flowPosId;
	private String hrPosId;
	private int type;
	private String flowPosName;
	private String hrPosName;

	public String getHrPosId() {
		return hrPosId;
	}

	public void setHrPosId(String hrPosId) {
		this.hrPosId = hrPosId;
	}

	public Long getFlowPosId() {
		return flowPosId;
	}

	public void setFlowPosId(Long flowPosId) {
		this.flowPosId = flowPosId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getFlowPosName() {
		return flowPosName;
	}

	public void setFlowPosName(String flowPosName) {
		this.flowPosName = flowPosName;
	}

	public String getHrPosName() {
		return hrPosName;
	}

	public void setHrPosName(String hrPosName) {
		this.hrPosName = hrPosName;
	}
}
