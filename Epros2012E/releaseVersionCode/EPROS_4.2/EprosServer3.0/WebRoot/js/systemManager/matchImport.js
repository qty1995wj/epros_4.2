/**
 * 岗位匹配初始化
 */

var postMatchTabPanel = null;
/*******************************************************************************
 * 当前显示的岗位类型 0：岗位匹配关系，1：未匹配流程岗位，2：未匹配岗位，3：未匹配基准岗位
 */
var posType = null;
function postMatch() {
	postMatchTabPanel = new Ext.TabPanel({
		border : false,
		id : 'postMatchTab_id',
		activeTab : 0,
		height : Ext.getCmp("main-panel").getInnerHeight() - 10,
		items : [{
					title : i_macthPost,// 岗位匹配
					id : "matchPost_id",
					autoLoad : {
						url : basePath
								+ "jsp/systemManager/match/matchPost.jsp;",
						callback : function() {
							// 初始化页面加载
							postMatchRecord();
							posType = 0;
						}
					}
				}, {
					title : i_notMatchFlowPost,// 未匹配流程岗位
					id : "noMatchProcess_id",
					autoLoad : {
						url : basePath
								+ "jsp/systemManager/match/noMatchProcess.jsp",
						// url : "initParams.action",
						callback : function() {
							// 初始化页面加载
							noMatchProcesstRecord("showNoMatchProcessResult.action");
							posType = 1;
						}
					}
				}, {
					title : i_notMatchPost,// 未匹配岗位
					id : "noMatchPost_id",
					autoLoad : {
						url : basePath
								+ "jsp/systemManager/match/noMatchPost.jsp",
						callback : function() {
							noMatchPostRecord("showNoMatchTruePostResult.action");
							posType = 2;
						}
					}
				}, {
					title : i_notMatchBasePost,// 未匹配基准岗位
					id : "noMatchBasePost_id",
					autoLoad : {
						url : basePath
								+ "jsp/systemManager/match/noMatchBasePost.jsp",
						callback : function() {
							noMatchBasePostRecord("showNoMatchBasePostResult.action");
							posType = 3;
						}
					}
				}],
		listeners : {
			'bodyresize' : function(p, width, height) {
				if (p.getActiveTab() == null) {
					return;
				}
				if (p.getActiveTab().getId() == 'matchPost_id') {
					Ext.getCmp('matchPostGridPanel_id').setSize(width,
							height - 135);
				} else if (p.getActiveTab().getId() == 'noMatchProcess_id') {
					Ext.getCmp('noMatchProcessGridPanel_id').setSize(width,
							height - 120);
				} else if (p.getActiveTab().getId() == 'noMatchPost_id') {
					Ext.getCmp('noMatchPostGridPanel_id').setSize(width,
							height - 120);
				} else if (p.getActiveTab().getId() == 'noMatchBasePost_id') {
					Ext.getCmp('noMatchBasePostGridPanel_id').setSize(width,
							height - 120);
				}

			},
			'tabchange' : function(tabPanel, panel) {

				if (panel.getId() == 'matchPost_id') {
					if (typeof(postMatchGridPanel) != "undefined") {
						postMatchGridPanel.setSize(tabPanel.getWidth() - 2,
								tabPanel.getHeight() - 182);
					}
					// 岗位匹配
					posType = 0;
				} else if (panel.getId() == 'noMatchProcess_id') {
					if (typeof(postMatchProcessPosGridPanel) != "undefined") {
						postMatchProcessPosGridPanel.setSize(tabPanel
										.getWidth()
										- 2, tabPanel.getHeight() - 155);
					}
					// 未匹配流程岗位
					posType = 1;
				} else if (panel.getId() == 'noMatchPost_id') {
					if (typeof(postMatchPosGridPanel) != "undefined") {
						postMatchPosGridPanel.setSize(tabPanel.getWidth() - 2,
								tabPanel.getHeight() - 155);
					}
					// 未匹配岗位
					posType = 2;
				} else if (panel.getId() == 'noMatchBasePost_id') {
					if (typeof(postMatchBasePosGridPanel) != "undefined") {
						postMatchBasePosGridPanel.setSize(tabPanel.getWidth()
										- 2, tabPanel.getHeight() - 155);
					}
					// 未匹配基准岗位
					posType = 3;
				}
			}
		}

	});
	Ext.getCmp("sysManagerCenter_id").add(postMatchTabPanel);
}
/**
 * 岗位匹配初始化加载
 * 
 */
function postMatchRecord() {
	postMatchStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "matchPostAction.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "epsPosId"
						}, [{
									name : "epsPosId",
									mapping : "epsPosId"
								}, {
									name : "flowPosName",
									mapping : "flowPosName"
								}, {
									name : "hrPosName",
									mapping : "hrPosName"
								}, {
									name : "posNum",
									mapping : "posNum"
								}, {
									name : "basePosName",
									mapping : "basePosName"
								}, {
									name : "relType",
									mapping : "relType"
								}])

			});

	var postMatchColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : "主键ID",
				dataIndex : "epsPosId",
				align : "center",
				menuDisabled : true,
				sortable : true,
				hidden : true
			}, {
				header : i_flowPostion,// 流程岗位
				dataIndex : "flowPosName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_matchPostion,// "匹配岗位"
				dataIndex : "hrPosName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_treePosCode,// 实际(基准)岗位编码
				dataIndex : "posNum",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_basePostion,// 基准岗位
				dataIndex : "basePosName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_matchType,// 匹配类型
				dataIndex : "relType",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_operation,
				align : "center",
				menuDisabled : true,
				renderer : postMatchRenderer
			}]);
	var postMatchBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : postMatchStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	postMatchGridPanel = new Ext.grid.GridPanel({
				renderTo : 'matchPostGrid_id',
				id : 'matchPostGridPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 192,
				width : '100%',
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样。
				},
				frame : false,
				store : postMatchStore,
				colModel : postMatchColumnModel,
				bbar : postMatchBbar
			})
}

/*******************************************************************************
 * 未匹配岗位初始化数据
 * 
 * @param actionUrl
 * @return
 */
function noMatchPostRecord(actionUrl) {
	postMatchPosStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : actionUrl
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "actPosNum"
						}, [{
									name : "actPosNum",
									mapping : "actPosNum"
								}, {
									name : "deptNum",
									mapping : "deptNum"
								}, {
									name : "actPosName",
									mapping : "actPosName"
								}, {
									name : "basePosName",
									mapping : "basePosName"
								}])

			});

	var postMatchPosColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_postionCode,// 岗位编号
				dataIndex : "actPosNum",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_orgName,// 部门名称
				dataIndex : "deptNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_positionName,// 岗位名称
				dataIndex : "actPosName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_basePosName,// "基准岗位名称"
				dataIndex : "basePosName",
				align : "center",
				menuDisabled : true
			}]);
	var postMatchPosBbar = new Ext.PagingToolbar({
				pageSize : 20,
				store : postMatchPosStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	postMatchPosGridPanel = new Ext.grid.GridPanel({
				renderTo : 'noMatchPostGrid_id',
				id : 'noMatchPostGridPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 165,
				width : '100%',
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样。
				},
				frame : false,
				store : postMatchPosStore,
				colModel : postMatchPosColumnModel,
				bbar : postMatchPosBbar
			})
}
/*******************************************************************************
 * 未匹配基准岗位初始化数据
 * 
 * @param actionUrl
 * @return
 */
function noMatchBasePostRecord(actionUrl) {
	postMatchBasePosStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : actionUrl
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "actPosName"
						}, [{
									name : "actPosName",
									mapping : "actPosName"
								}, {
									name : "basePosName",
									mapping : "basePosName"
								}])

			});

	var postMatchBasePosColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_basePosCode,// 基准岗位编号
				dataIndex : "actPosName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_basePosName,// "基准岗位名称",
				dataIndex : "basePosName",
				align : "center",
				menuDisabled : true
			}]);
	var postMatchBasePosBbar = new Ext.PagingToolbar({
				pageSize : 20,
				store : postMatchBasePosStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	postMatchBasePosGridPanel = new Ext.grid.GridPanel({
				renderTo : 'noMatchBasePostGrid_id',
				id : 'noMatchBasePostGridPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 165,
				width : '100%',
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样。
				},
				frame : false,
				store : postMatchBasePosStore,
				colModel : postMatchBasePosColumnModel,
				bbar : postMatchBasePosBbar
			})
}
/**
 * 未匹配流程岗位初始化加载
 * 
 */
function noMatchProcesstRecord(actionUrl) {
	postMatchProcessPosStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : actionUrl
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "id",
									mapping : "id"
								}, {
									name : "actPosName",
									mapping : "actPosName"
								}, {
									name : "basePosName",
									mapping : "basePosName"
								}])

			});

	var postMatchProcessPosColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : "流程岗位ID",
				dataIndex : "id",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_orgName,// "部门名称",
				dataIndex : "actPosName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_positionName,// "岗位名称"
				dataIndex : "basePosName",
				align : "center",
				menuDisabled : true
			}]);
	var postMatchProcessPosBbar = new Ext.PagingToolbar({
				pageSize : 20,
				store : postMatchProcessPosStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	postMatchProcessPosGridPanel = new Ext.grid.GridPanel({
				renderTo : 'noMatchProcessGrid_id',
				id : 'noMatchProcessGridPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 165,
				width : '100%',
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样。
				},
				frame : false,
				store : postMatchProcessPosStore,
				colModel : postMatchProcessPosColumnModel,
				bbar : postMatchProcessPosBbar
			})
}
// 未匹配流程岗位搜索
function matchSearch() {
	if (posType == 0) {
		var posName = document.getElementById("mactchPosposName").value;
		postMatchStore.setBaseParam("posName", posName);
		postMatchStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
	} else if (posType == 1) {
		var nomatchProposName = document.getElementById("nomatchProposName").value;
		postMatchProcessPosStore.setBaseParam("posName", nomatchProposName);
		postMatchProcessPosStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
	} else if (posType == 2) {
		var noMatchPostposName = document.getElementById("noMatchPostposName").value;
		postMatchPosStore.setBaseParam("posName", noMatchPostposName);
		postMatchPosStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
	} else if (posType == 3) {
		var noMatchBaseposName = document.getElementById("noMatchBaseposName").value;
		postMatchBasePosStore.setBaseParam("posName", noMatchBaseposName);
		postMatchBasePosStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
	}

}
/**
 * 任务管理重置
 */
function taskSearchResetPos() {
	document.getElementById("mactchPosposName").value = "";
}
function taskSearchNomatchPro() {
	document.getElementById("nomatchProposName").value = "";
}
function taskSearchNoMatchPost() {
	document.getElementById("noMatchPostposName").value = "";
}
function taskSearchNoMatchBase() {
	document.getElementById("noMatchBaseposName").value = "";
}

/**
 * 
 * 
 */
function noMatchMethod(radioType) {
	if (radioType == 5) {// 未匹配流程岗位
		// 部门名称
		actPosName = i_orgName;
	} else if (radioType = 1) {// 未匹配岗位
		// 岗位名称
		actPosName = i_positionName;
	} else if (radioType = 6) {// 未匹配基准岗位
		actPosName = i_basePosCode;
	}
}
/**
 * 
 * 
 */
function noMatchBaseNameMethod(type) {
	if (type == 5) {// 未匹配流程岗位
		// 岗位名称
		actPosName = i_positionName;
	} else {
		// 基准岗位名称
		actPosName = i_basePosName;
	}
}
/**
 * 任务列表操作
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 */
function postMatchRenderer(value, cellmeta, record) {
	// 0：流程岗位ID；1：流程岗位名称；2：匹配类型；3：匹配编号；4：匹配岗位名称
	// 匹配类型
	var relType = record.data["relType"];
	// 流程岗位主键ID
	var epsPosId = record.data["epsPosId"];
	// 流程岗位名称
	var flowPosName = record.data["flowPosName"];
	// 岗位编码
	var posNum = record.data["posNum"];
	// 实际岗位名称
	var hrPosName = record.data["hrPosName"];
	// 基准岗位名称
	var basePosName = record.data["basePosName"];

	// 审批操作连接
	var varEdit;
	// 取消操作连接
	var vardelete;
	// div
	var varDiv;

	if (relType == null || epsPosId == null) {// epsPosIdID不存在
		return;
	}
	// 默认URL链接为任务ID和任务类型
	var appUrl = "epsPosId=" + epsPosId;

	var posBaseName;
	if (relType == 0) {// 实际岗位匹配
		posBaseName = hrPosName;
	} else if (relType == 1) {
		posBaseName = basePosName;
	}
	if (epsPosId != null) {// 拟稿人阶段
		// 删除操作
		vardelete = "<a onclick='deleteMatchPost()' class='a-operation'>"
				+ i_Delete + "</a>";
		// 管理员编辑任务
		varEdit = "<a onclick='editMatchPost()'" + appUrl
				+ "' class='a-operation'>" + i_Edit + "</a>";
		// 任务管理显示的按钮
		varDiv = "<div >" + varEdit + " | " + vardelete + "</a><a></a></div>";
	}
	return varDiv;
}
/**
 * 添加岗位匹配 (isFlag 0：新增 1：编辑)
 */
function addPosMatch(isFlag) {
	var type = 'position';
	// 显示名称
	var inputDomId;
	// 隐藏ID
	var hiddenDomId;
	// Ext.Button
	var matchFormPanel = new Ext.form.FormPanel({
				renderTo : Ext.getBody(),
				labelWidth : 70,
				bodyStyle : 'padding:5 5 5 5',
				labelAlign : "left",
				frame : true,
				defaults : {
					xtype : "textfield",
					width : 360
				},
				items : [{
					fieldLabel : i_flowPostion,// '流程岗位',
					xtype : "panel",// panel
					layout : "column",
					isFormField : true,
					items : [{
								name : "posName",
								id : "posName_id",
								// width : 290,
								disabled : true,
								xtype : "textfield",
								columnWidth : 1
							}, {
								name : "epsPosId",// 隐藏的流程岗位ID
								id : "epsPosId",// 隐藏的流程岗位ID
								xtype : "hidden"
							}, {
								text : i_selectBut,// 选择
								xtype : "button",
								width : 50,
								handler : function() {
									orgPosPerOneSelect('position',
											'posName_id', 'epsPosId');
								},
								style : {
									marginLeft : '5px'
								}
							}]
				}, {
					fieldLabel : i_matchType,
					xtype : "panel",// panel
					layout : "column",
					isFormField : true,
					items : [{
								boxLabel : i_truePostion,
								name : 'typeRadio',
								inputValue : '0',
								id : 'trueRadio',
								checked : true,// 默认选中
								xtype : "radio",// 单选扭类型
								width : 80
							}, {
								boxLabel : i_basePostion,// '基准岗位'
								id : 'baseRadio',
								name : 'typeRadio',
								inputValue : '1',
								xtype : "radio"
							}]
				}, {
					fieldLabel : i_truePostion,
					id : 'pos_lable',
					xtype : "panel",// panel
					layout : "column",
					isFormField : true,
					items : [{
								xtype : "textarea",
								id : 'mathPosName_id',
								name : "intro",
								// disabled:true, //disabled:true 不显示滚动条
								readOnly : true,
								// width : 290,
								height : 100,
								columnWidth : 1,
								autoScroll : true
							}, {
								xtype : "hidden",
								id : 'positionIds'
							}, {
								text : i_selectBut,// 选择
								xtype : "button",
								width : 50,
								handler : function() {
									// 选择实际岗位
									matchPosPerMultipleSelect(type,
											'mathPosName_id', 'positionIds');
								},
								style : {
									marginLeft : '5px'
								}
							}]
				}, {
					xtype : "hidden",
					id : 'relType',
					value : '0'
				}, {
					xtype : "hidden",
					id : 'isFlag',
					value : isFlag
				}]
			});

	var trueRadio = Ext.getCmp('trueRadio');
	trueRadio.on('check', function(trueRadio, isChecked) {
		// 选择实际岗位
		var rg = Ext.getCmp('trueRadio');
		// 是否选中
		if (rg.checked) {// 实际岗位
			// 实际或基准岗位选择的panel 实际岗位：
			Ext.getCmp('pos_lable').el.dom.parentNode.parentNode.firstChild.innerHTML = i_truePostionC;
			// 实际岗位标识
			type = 'position';
			// 隐藏字段 单选按钮值
			document.getElementById('relType').value = rg.getGroupValue();
			matchFormPanel.getForm().findField("mathPosName_id").setValue('');
			matchFormPanel.getForm().findField("positionIds").setValue('');
		}
	});
	var trueRadio = Ext.getCmp('baseRadio');
	trueRadio.on('check', function(trueRadio, isChecked) {
		// 选择基准岗位
		var baseRadio = Ext.getCmp('baseRadio');
		// 是否选中
		if (baseRadio.checked) {// 基准岗位
			// 实际或基准岗位选择的panel '基准岗位：
			Ext.getCmp('pos_lable').el.dom.parentNode.parentNode.firstChild.innerHTML = i_basePostionC;
			// 实际岗位标识
			type = 'basePosition';
			// 隐藏字段 单选按钮值
			document.getElementById('relType').value = baseRadio
					.getGroupValue();
			matchFormPanel.getForm().findField("mathPosName_id").setValue('');
			matchFormPanel.getForm().findField("positionIds").setValue('');
		}
	});
	var posMatchWindow = new Ext.Window({
				title : i_addMatchPostion,// 添加岗位匹配
				height : 235,
				width : 481,
				closeAction : "close",
				resizable : false, // 不允许改变大小
				constrainHeader : true,// 保证窗口顶部不超出浏览器
				modal : true,
				items : matchFormPanel,
				buttons : [{
					text : i_save,// 保存
					id : 'addButton',
					disabled : false,
					handler : function() {
						// 点击后按钮禁用
						Ext.getCmp('addButton').disable();
						// 流程岗位
						var posName = matchFormPanel.getForm()
								.findField("posName_id").getValue();
						// 匹配岗位
						var mathPosName = matchFormPanel.getForm()
								.findField("mathPosName_id").getValue();

						if (isNotNull(posName)) {// true 为空
							// 请选择流程岗位！
							Ext.Msg.alert(i_tip, i_selectFlowPostion);
							Ext.getCmp('addButton').setDisabled(false);
							return;
						}

						if (isNotNull(mathPosName)) {// true 为空
							// "请选择匹配岗位！
							Ext.Msg.alert(i_tip, i_selectMatchPostion);
							Ext.getCmp('addButton').setDisabled(false);
							return;
						}

						// 流程岗位在匹配列表中唯一
						matchFormPanel.getForm().submit({
							url : "addPosEpsRel.action",// 这里是接收数据的程序
							success : function(_form, _action) {
								var msg = _action.result.msg;
								if (msg == null || msg == 0) {
									Ext.Msg.alert(i_tip, i_AccessServerAnomaly);
								} else if (msg == 1) {
									// 该匹配关系已存在！
									Ext.Msg.alert(i_tip, i_matchHasExists);
									Ext.getCmp('addButton').setDisabled(false);
								} else if (msg == 3) {// 添加成功
									// 流程主键ID
									var epsPosId = matchFormPanel.getForm()
											.findField("epsPosId").getValue();
									// 0实际岗位；1基准岗位
									var relType = matchFormPanel.getForm()
											.findField("relType").getValue();
									var sr = new Ext.data.Record();
									sr.set("epsPosId", epsPosId);
									sr.set("flowPosName", posName);

									// ‘\n’替换BR
									mathPosName = replaceAlln(mathPosName);
									if (relType == 0) {
										sr.set("hrPosName", mathPosName);
									} else if (relType == 1) {
										sr.set("basePosName", mathPosName);
									}
									// 匹配的岗位编号
									sr.set("posNum", matchFormPanel.getForm()
													.findField("positionIds")
													.getValue());

									sr.set("relType", relType);
									postMatchGridPanel.getStore().add(sr);
									posMatchWindow.close();
								}
							},
							failure : function(_form, _action) {
								Ext.Msg.alert(i_tip, i_AccessServerAnomaly);
								Ext.getCmp('addButton').setDisabled(false);
							}
						})

					}
				}, {
					text : i_close,// 关闭
					handler : function() {
						posMatchWindow.close();
					}// 关闭当前的弹出Window
				}]
			})
	posMatchWindow.show();
}
/**
 * 删除匹配岗位
 * 
 */
// function deleteMatchPost() {
// if (window.confirm(i_HAS_DELETE_TASK)) {
// Ext.Ajax.request({
// url : basePath + "deletePosEpsRel.action",
// params : {
// epsPosId :
// postMatchGridPanel.getSelectionModel().getSelected().data["epsPosId"]
// },
// success : function() {
// postMatchGridPanel.getStore().remove(postMatchGridPanel
// .getSelectionModel().getSelected());
// postMatchGridPanel.getStore().reload();
// }
// })
// }
// }
function deleteMatchPost() {
	// "是否删除！"
	Ext.Msg.confirm(i_tip, i_HAS_DELETE_TASK, function(button, text) {
		if (button == "yes") {
			Ext.Ajax.request({
						url : basePath + "deletePosEpsRel.action",
						params : {
							epsPosId : postMatchGridPanel.getSelectionModel()
									.getSelected().data["epsPosId"]
						},
						success : function() {
							postMatchGridPanel.getStore()
									.remove(postMatchGridPanel
											.getSelectionModel().getSelected());
							postMatchGridPanel.getStore().reload();
						}
					});
		}
	});
}
/**
 * 把字符串中所有《br》 替换为\n
 * 
 * @param {}
 *            obj 待<br>
 *            的字符串
 * @return {} 返回带\n的字符串
 */
function replaceAllbr(obj) {
	return obj.replace(/\<br>/g, "\n");
}
/**
 * 把字符串中所有\n 替换为<br>
 * 
 * @param {}
 *            obj 待\n 的字符串
 * @return {} 返回带<br>
 *         的字符串
 */
function replaceAlln(obj) {
	var reg = new RegExp("\n", "g");
	return obj.replace(reg, "<br>");;
}
/**
 * 编辑匹配岗位
 * 
 * 
 */
function editMatchPost() {
	// 选中的一行
	var record = postMatchGridPanel.getSelectionModel().getSelected();
	if (!record) {
		Ext.Msg.show({
					title : i_errorTip,// '错误提示',
					msg : i_pleaseFixData,// 请选择您要修改的数据！
					buttons : Ext.Msg.OK,
					icon : Ext.Msg.ERROR
				});
	}
	// 匹配类型
	var relType = record.data["relType"];
	// 编辑前流程节点ID
	var oldEpsPosId = record.data["epsPosId"];

	var trueRadioCheck;
	var baseRadioCheck;
	var type = 'position';
	var mathPosName;
	if (relType == 0) {// HR实际岗位匹配
		// 选择实际岗位
		trueRadioCheck = true;
		baseRadioCheck = false;
		mathPosName = record.data["hrPosName"];
		// 实际岗位标识
		type = 'position';
	} else if (relType == 1) {
		trueRadioCheck = false;
		baseRadioCheck = true;
		// 实际岗位标识
		type = 'basePosition';
		mathPosName = record.data["basePosName"];
	}
	// 替换<br>
	mathPosName = replaceAllbr(mathPosName);

	// 显示名称
	var inputDomId;
	// 隐藏ID
	var hiddenDomId;
	// Ext.Button
	var matchFormPanel = new Ext.form.FormPanel({ // 编辑 update
		renderTo : Ext.getBody(),
		labelWidth : 70,
		bodyStyle : 'padding:5 5 5 5',
		labelAlign : "left",
		frame : true,
		defaults : {
			xtype : "textfield",
			width : 360
		},
		items : [{
			fieldLabel : i_flowPostion,// 流程岗位
			xtype : "panel",// panel
			layout : "column",
			isFormField : true,
			items : [{
						name : "posName",
						id : "posName_id",
						// width : 290,
						xtype : "textfield",
						disabled : true, // 禁用,
						columnWidth : 1,
						value : record.data["flowPosName"]
					}, {
						name : "epsPosId",// 隐藏的流程岗位ID
						id : "epsPosId",// 隐藏的流程岗位ID
						xtype : "hidden",
						value : record.data["epsPosId"]
					}, {
						text : i_selectBut,// 选择
						xtype : "button",
						width : 50,
						handler : function() {
							orgPosPerOneSelect('position', 'posName_id',
									'epsPosId');
						},
						style : {
							marginLeft : '5px'
						}
					}]
		}, {
			xtype : "panel",// panel
			fieldLabel : i_matchType,
			layout : "column",
			isFormField : true,
			items : [{
				boxLabel : i_truePostion,
				name : 'typeRadio',
				inputValue : '0',
				id : 'trueRadio',
				checked : trueRadioCheck,// 默认选中
				xtype : "radio",// 单选扭类型
				width : 80,
				listeners : {
					'check' : function(trueRadio, isChecked) {
						// 选择实际岗位
						var rg = Ext.getCmp('trueRadio');
						// 是否选中
						if (rg.checked) {// 实际岗位
							// 实际或基准岗位选择的panel
							Ext.getCmp('pos_lable').el.dom.parentNode.parentNode.firstChild.innerHTML = i_truePostionC;
							// 实际岗位标识
							type = 'position';
							// 隐藏字段 单选按钮值
							document.getElementById('relType').value = rg
									.getGroupValue();
							matchFormPanel.getForm()
									.findField("mathPosName_id").setValue('');
							matchFormPanel.getForm().findField("positionIds")
									.setValue('');
						}
					}
				}
			}, {
				boxLabel : i_basePostion,// '基准岗位'
				id : 'baseRadio',
				name : 'typeRadio',
				inputValue : '1',
				xtype : "radio",
				checked : baseRadioCheck,
				listeners : {
					'check' : function(trueRadio, isChecked) {
						// 选择基准岗位
						var baseRadio = Ext.getCmp('baseRadio');
						// 是否选中
						if (baseRadio.checked) {// 基准岗位
							// 实际或基准岗位选择的panel
							Ext.getCmp('pos_lable').el.dom.parentNode.parentNode.firstChild.innerHTML = i_basePostionC;
							// 实际岗位标识
							type = 'basePosition';
							// 隐藏字段 单选按钮值
							document.getElementById('relType').value = baseRadio
									.getGroupValue();
							matchFormPanel.getForm()
									.findField("mathPosName_id").setValue('');
							matchFormPanel.getForm().findField("positionIds")
									.setValue('');
						}
					}
				}
			}]
		}, {
			fieldLabel : i_truePostion,
			id : 'pos_lable',
			xtype : "panel",// panel
			layout : "column",
			isFormField : true,
			items : [{
						xtype : "textarea",
						id : 'mathPosName_id',
						// width : 290,
						height : 100,
						// disabled:true, //disabled:true 不显示滚动条
						readOnly : true,
						columnWidth : 1,
						autoScroll : true,
						value : mathPosName
					}, {
						xtype : "hidden",
						id : 'positionIds',
						value : record.data["posNum"]
					}, {
						text : i_selectBut,// 选择
						xtype : "button",
						width : 50,
						handler : function() {
							// 选择实际岗位
							matchPosPerMultipleSelect(type, 'mathPosName_id',
									'positionIds');
						},
						style : {
							marginLeft : '5px'
						}
					}]
		}, {
			xtype : "hidden",
			id : 'relType',
			value : record.data["relType"]
				// 编辑
			}, {
			xtype : "hidden",
			id : 'isFlag',
			value : 1
		}, {	// 0：流程岗位不变，1 流程岗位更改
					xtype : "hidden",
					id : 'changeId',
					value : 0
				}]
	});
	var posMatchWindow = new Ext.Window({
				title : i_editMatchPostion,// 编辑岗位匹配
				height : 235,
				width : 481,
				closeAction : "close",
				resizable : false, // 不允许改变大小
				constrainHeader : true,// 保证窗口顶部不超出浏览器
				items : matchFormPanel,
				modal : true,
				buttons : [{
					text : i_save,// 保存
					id : 'editButton',
					disabled : false,
					handler : function() {
						// 点击后按钮禁用
						Ext.getCmp('editButton').disable();
						// 流程岗位
						var posName = matchFormPanel.getForm()
								.findField("posName_id").getValue();
						// 匹配岗位
						var mathPosName = matchFormPanel.getForm()
								.findField("mathPosName_id").getValue();
						// 替换\n为<br>
						mathPosName = replaceAlln(mathPosName);

						if (isNotNull(posName)) {// true 为空
							// 请选择流程岗位！
							Ext.Msg.alert(i_tip, i_selectFlowPostion);
							Ext.getCmp('editButton').setDisabled(false);
							return;
						}

						if (isNotNull(mathPosName)) {// true 为空
							// "请选择匹配岗位！
							Ext.Msg.alert(i_tip, i_selectMatchPostion);
							Ext.getCmp('editButton').setDisabled(false);
							return;
						}
						// 流程主键ID
						var epsPosId = matchFormPanel.getForm()
								.findField("epsPosId").getValue();

						// 0：流程岗位不变，1 流程岗位更改
						if (oldEpsPosId != epsPosId) {
							matchFormPanel.getForm().findField("changeId")
									.setValue(1);
						} else {
							matchFormPanel.getForm().findField("changeId")
									.setValue(0);
						}

						// 流程岗位在匹配列表中唯一
						matchFormPanel.getForm().submit({
							url : "addPosEpsRel.action",// 这里是接收数据的程序
							success : function(_form, _action) {
								var msg = _action.result.msg;
								if (msg == null || msg == 0) {
									Ext.Msg.alert(i_tip, i_AccessServerAnomaly);
								} else if (msg == 1) {
									// 该匹配关系已存在！
									Ext.Msg.alert(i_tip, i_matchHasExists);
									Ext.getCmp('editButton').setDisabled(false);
								} else if (msg == 3) {// 添加成功
									// 0实际岗位；1基准岗位
									var relType = matchFormPanel.getForm()
											.findField("relType").getValue();
									record.set("epsPosId", epsPosId);
									record.set("flowPosName", posName);
									if (relType == 0) {// 选择实际岗位匹配
										record.set("hrPosName", mathPosName);
										record.set("basePosName", '');
									} else if (relType == 1) {// 1基准岗位
										record.set("hrPosName", '');
										record.set("basePosName", mathPosName);
									}
									// 匹配的岗位编号
									record.set("posNum", matchFormPanel
													.getForm()
													.findField("positionIds")
													.getValue());
									record.set("relType", relType);
									if (record.dirty) {
										record.commit();
										postMatchGridPanel.getStore()
												.commitChanges();
									}
									posMatchWindow.close();
								}
							},
							failure : function(_form, _action) {
								Ext.Msg.alert(i_tip, i_AccessServerAnomaly);
							}
						})

					}
				}, {
					text : i_close,// 关闭
					handler : function() {
						posMatchWindow.close();
					}// 关闭当前的弹出Window
				}]
			})

	if (trueRadioCheck) {
		// 实际或基准岗位选择的panel
		Ext.getCmp('pos_lable').el.dom.parentNode.parentNode.firstChild.innerHTML = i_truePostionC;
	} else {
		// 基准岗位：
		Ext.getCmp('pos_lable').el.dom.parentNode.parentNode.firstChild.innerHTML = i_basePostionC;
	}
	// 0：流程岗位ID；1：流程岗位名称；2：匹配类型；3：匹配编号；4：匹配岗位名称
	posMatchWindow.show();
	matchFormPanel.getForm().loadRecord(record);
}

var btn_modify_manager = new Ext.Button({
			text : i_fixedUser,// 修改用户
			iconCls : 'icon-edit',
			handler : function() {
				var record = postMatchGridPanel.getSelectionModel()
						.getSelected();
				if (!record) {
					Ext.Msg.show({
								title : i_errorTip,// '错误提示',
								msg : i_pleaseFixData,// 请选择您要修改的数据！
								buttons : Ext.Msg.OK,
								icon : Ext.Msg.ERROR
							});
				} else {
					if (record) {
						window_edit_manager.show();
						managerEditForm.getForm().loadRecord(record);
					}
				}
			}
		});
