package com.jecn.epros.server.service.dataImport.sync.tempBean;

/**
 * 人员同步部门变更信息
 * 
 * @author Administrator
 * @date： 日期：May 17, 2013 时间：3:28:26 PM
 */
public class DataChangeInfoDept {
	/** 部门名称 */
	private String oldDeptName;
	private String curDeprName;
	/** 部门父节点名称 */
	private String oldPreDeptName;
	private String curPreDeptName;
	/** 增删改查 */
	private int type;

	public String getOldDeptName() {
		return oldDeptName;
	}

	public void setOldDeptName(String oldDeptName) {
		this.oldDeptName = oldDeptName;
	}

	public String getCurDeprName() {
		return curDeprName;
	}

	public void setCurDeprName(String curDeprName) {
		this.curDeprName = curDeprName;
	}

	public String getOldPreDeptName() {
		return oldPreDeptName;
	}

	public void setOldPreDeptName(String oldPreDeptName) {
		this.oldPreDeptName = oldPreDeptName;
	}

	public String getCurPreDeptName() {
		return curPreDeptName;
	}

	public void setCurPreDeptName(String curPreDeptName) {
		this.curPreDeptName = curPreDeptName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
