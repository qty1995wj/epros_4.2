package com.jecn.epros.server.dao.project;

import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.common.IBaseDao;

public interface ICurProjectDao extends IBaseDao<JecnCurProject, Long> {
	/**
	 * 获得主项目
	 * @return
	 * @throws Exception
	 */
	public JecnCurProject getJecnCurProjectById() throws Exception;
}
