package com.jecn.epros.server.bean.process;

import java.io.Serializable;

public class StandardFlowRelationBean implements Serializable{
	private Long id;//主键ID
	private Long flowId;//流程ID
	private Long criterionClassId;//标准ID
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public Long getCriterionClassId() {
		return criterionClassId;
	}
	public void setCriterionClassId(Long criterionClassId) {
		this.criterionClassId = criterionClassId;
	}
}
