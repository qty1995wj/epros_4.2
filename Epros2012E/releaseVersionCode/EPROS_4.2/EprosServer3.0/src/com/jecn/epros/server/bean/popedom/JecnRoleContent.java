package com.jecn.epros.server.bean.popedom;

public class JecnRoleContent implements java.io.Serializable {
	private Long id; //主键Id
	private Long roleId;//角色Id
	private Long relateId;//关联Id
	private Integer type;//0是流程、1是文件、2是标准、3是制度、4是风险、5：组织
	private String rName;//关联ID的名称
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getRelateId() {
		return relateId;
	}
	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getrName() {
		return rName;
	}
	public void setrName(String rName) {
		this.rName = rName;
	}
	
}
