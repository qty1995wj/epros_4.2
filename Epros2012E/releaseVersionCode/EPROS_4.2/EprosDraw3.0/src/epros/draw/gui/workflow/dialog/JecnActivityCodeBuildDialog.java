package epros.draw.gui.workflow.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnToFromRelatedData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 活动自动编号
 * 
 * @author fuzhh
 * 
 */
public class JecnActivityCodeBuildDialog extends JecnDialog implements ActionListener {
	/** 主panel */
	private JPanel mainPanel;
	/** 编号选择panel */
	private JPanel numberPanel;
	/** 确定取消panel */
	private JecnOKCancelPanel confirmCancelPanel;
	/** 编号类型下拉框label */
	private JLabel numberTypeLabel;
	/** 下拉框 */
	protected JComboBox numberTypeJcomboBox;
	/** 编号名称 */
	private JLabel numberLabel;
	/** 编号名称输入框 */
	private JTextField numberTextField;
	/** 错误提示Label */
	private JLabel errorLabel;
	/** 显示编号Label */
	private JTextArea showTextArea;
	/** 面板 */
	protected JecnDrawDesktopPane workflow;
	/** 记录活动编号（截取数字部分） */
	private Integer strActInt = 0;

	/** 判断编号名称输入是否合法 */
	private boolean numberNameFlag = false;

	public JecnActivityCodeBuildDialog(JecnDrawDesktopPane workflow) {
		this.workflow = workflow;

		initComponents();
		initLayout();
	}

	/**
	 * 初始化界面
	 */
	private void initComponents() {

		// 初始化主panel
		mainPanel = new JPanel();
		// 初始化确定取消panelt
		confirmCancelPanel = new JecnOKCancelPanel();
		// 初始化panel
		numberPanel = new JPanel();
		// 初始化编号label
		numberLabel = new JLabel();
		// 初始化下拉框
		numberTypeLabel = new JLabel();
		// 初始化 错误提示Label
		errorLabel = new JLabel();
		// 初始化编号样式Label
		showTextArea = new JTextArea();
		// 初始化下拉框
		numberTypeJcomboBox = new JComboBox();
		// 初始化编号输入框
		numberTextField = new JTextField();

		// 设置界面大小
		if (!isHiddenActiveType()) {
			this.setSize(400, 100);
		} else {
			this.setSize(400, 200);
		}

		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("automaticNumbers"));
		// 设置不可调整大小
		this.setResizable(true);
		// 设置为模态窗口
		this.setModal(true);
		// 关闭销毁
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// 设置居中
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());

		// 设置背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置布局样式
		mainPanel.setLayout(new BorderLayout());

		// 设置背景色
		numberPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置布局方式
		numberPanel.setLayout(new GridBagLayout());

		numberLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("numbersType"));

		// 初始化下拉框
		numberTypeLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("LabelnumStyle"));

		// 初始化编号样式Label
		showTextArea.setOpaque(false);
		showTextArea.setEditable(false);
		showTextArea.setBorder(null);
		showTextArea.setWrapStyleWord(true);
		showTextArea.setLineWrap(true);

		// 设置Label颜色
		errorLabel.setForeground(Color.red);

		initNumBox();

		// 初始化编号输入框
		numberTextField.getDocument().addDocumentListener(new JecnDocumentListener());

		// 确定按钮
		confirmCancelPanel.getOkBtn().addActionListener(this);
		// 取消按钮
		confirmCancelPanel.getCancelBtn().addActionListener(this);

		// 把编号panel 放入主panel
		mainPanel.add(numberPanel, BorderLayout.CENTER);
		// 把确定取消panel 放入主panel
		mainPanel.add(confirmCancelPanel, BorderLayout.SOUTH);

		// 把主panel放入自定义的panel
		this.getContentPane().add(mainPanel);
	}

	protected void initNumBox() {
		// JList存放的数据
		String[] strings = { "1,2,3,4,5,6,...", "01,02,03,04,05,06,07,08,09,10,11...",
				"001,002,003,004,005,006,007,008,009,010..." };
		// 初始化下拉框
		numberTypeJcomboBox.setModel(new DefaultComboBoxModel(strings));
		numberTypeJcomboBox.setSelectedIndex(1);
		numberTypeJcomboBox.addActionListener(this);
	}

	protected boolean isHiddenActiveType() {
		return false;
	}

	/**
	 * 初始化编号panel
	 */
	private void initLayout() {

		Insets oneLeftInsets = new Insets(15, 18, 5, 3);
		Insets oneRightInsets = new Insets(15, 0, 5, 18);

		Insets leftInsets = new Insets(5, 18, 5, 3);
		Insets rightInsets = new Insets(5, 0, 5, 18);
		int row = 0;
		GridBagConstraints c = null;
		// *************第一行*************//
		if (isHiddenActiveType()) {
			c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					oneLeftInsets, 0, 0);
			numberPanel.add(numberLabel, c);
			c = new GridBagConstraints(1, row, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, oneRightInsets, 0, 0);
			numberPanel.add(numberTextField, c);
			row++;
		}
		// *************第一行*************//

		// *************第二行*************//
		c = new GridBagConstraints(1, row, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		numberPanel.add(errorLabel, c);
		row++;
		// *************第二行*************//

		// *************第三行*************//
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				leftInsets, 0, 0);
		numberPanel.add(numberTypeLabel, c);
		c = new GridBagConstraints(1, row, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				rightInsets, 0, 0);
		numberPanel.add(numberTypeJcomboBox, c);
		row++;
		// *************第三行*************//
		if (isHiddenActiveType()) {
			// *************第四行*************//
			c = new GridBagConstraints(1, row, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
					rightInsets, 0, 0);
			numberPanel.add(showTextArea, c);
		}
		// *************第四行*************//
	}

	/**
	 * 判断输入的编号名称是否合法
	 * 
	 * @return ture不合法，false合法
	 */
	private boolean initErrorLabel() {
		// 获取编号名称
		String numberName = numberTextField.getText();
		if (numberName != null) {
			numberName = numberName.trim();
		}
		// 计算编号名称的字符
		int numberLength = JecnUserCheckUtil.getTextLength(numberName);
		if (numberName == null || "".equals(numberName)) {
			errorLabel.setText("");
			showTextArea.setText("");
			return false;
		} else if (numberLength > 16) {
			errorLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("checkDigital"));
			showTextArea.setText("");
			return true;
		} else {
			errorLabel.setText("");
			showTextArea.setText(numberPreview(numberName));
		}
		return false;
	}

	/**
	 * 编号预览
	 * 
	 * @param numberName
	 *            编号名称
	 * @return 编号预览的字符串
	 */
	private String numberPreview(String numberName) {
		if (numberTextField.getText() == null || "".equals(numberTextField.getText().trim())) {
			return null;
		}
		int index = numberTypeJcomboBox.getSelectedIndex();
		StringBuffer numberStr = new StringBuffer();
		if (index == 0) {
			for (int i = 1; i < 7; i++) {
				numberStr.append(numberName + "-" + i + ",");
			}
		} else if (index == 1) {
			for (int i = 1; i < 7; i++) {
				numberStr.append(numberName + "-0" + i + ",");
			}

		} else if (index == 2) {
			for (int i = 1; i < 7; i++) {
				numberStr.append(numberName + "-00" + i + ",");
			}
		} else if (index == 3) {
			for (int i = 1; i < 7; i++) {
				numberStr.append(numberName + "-0" + i + "0,");
			}
		}
		return numberStr.substring(0, numberStr.length() - 1);
	}

	/**
	 * 自动编号的算法
	 * 
	 * @param indexType
	 */
	public void buildArabia(int indexType, boolean flag) {
		if (numberNameFlag) {
			return;
		}
		String activeText = "";
		boolean isActiveCode = false;
		// 记录活动编号（截取数字部分）
		String curActivityId = "";
		// 获取输入的编号类别
		String numberValue = numberTextField.getText();

		// 获取面板选择的图形集合
		List<JecnBaseFlowElementPanel> currentSelectElement = workflow.getCurrentSelectElement();
		// 如果选择了多个图形就返回
		if (currentSelectElement.size() > 1) {
			JecnOptionPane.showMessageDialog(this, JecnResourceUtil.getJecnResourceUtil().getValue("oneActivities"),
					JecnResourceUtil.getJecnResourceUtil().getValue("tip"), JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (currentSelectElement.size() == 1) {

			// 获取选择的图形
			JecnAbstractFlowElementData flowElement = currentSelectElement.get(0).getFlowElementData();

			// 判断选择的图形是否为活动
			if (!isActive(flowElement.getMapElemType())) {
				JecnOptionPane.showMessageDialog(null, JecnResourceUtil.getJecnResourceUtil().getValue("noActivities"),
						JecnResourceUtil.getJecnResourceUtil().getValue("tip"), JOptionPane.ERROR_MESSAGE);
				return;
			}
			JecnBaseActiveFigure activeFigure = (JecnBaseActiveFigure) currentSelectElement.get(0);
			if (numberValue != null && !"".equals(numberValue.trim())) {
				if (activeFigure != null && activeFigure.getFlowElementData().getActivityNum() != null
						&& !"".equals(activeFigure.getFlowElementData().getActivityNum().trim())) {
					// 截取自动编号后面的数字
					curActivityId = activeFigure.getFlowElementData().getActivityNum().substring(
							activeFigure.getFlowElementData().getActivityNum().lastIndexOf("-") + 1,
							activeFigure.getFlowElementData().getActivityNum().length());
					// 判断传入的是否为数字
					if (!checkIsNumber(curActivityId)
							|| checkIsNumber(activeFigure.getFlowElementData().getActivityNum())) {// 如果选择活动编号只有数字,执行所有活动自动编号
						int value = JecnOptionPane.showConfirmDialog(this, JecnResourceUtil.getJecnResourceUtil()
								.getValue("confirmNewNumber"), JecnResourceUtil.getJecnResourceUtil().getValue("tip"),
								JOptionPane.YES_NO_OPTION);
						if (value == JOptionPane.YES_OPTION) {
							isActiveCode = false;
						} else {
							return;
						}
					} else if (checkIsNumber(curActivityId)) {// 如果活动以数字结尾,以该活动编号数字开始,后面的自动编号
						isActiveCode = true;
					}
				}
			}
		}

		if (numberValue != null && !"".equals(numberValue.trim())) {
			activeText = numberValue + "-";
		} else {
			int value = JecnOptionPane.showConfirmDialog(this, JecnResourceUtil.getJecnResourceUtil().getValue(
					"confirmNewNumber"), JecnResourceUtil.getJecnResourceUtil().getValue("tip"),
					JOptionPane.YES_NO_OPTION);
			if (value == JOptionPane.YES_OPTION) {
				isActiveCode = false;
			} else {
				return;
			}
		}
		// 面板所有图形元素
		List<JecnBaseFlowElementPanel> figureList = workflow.getPanelList();
		// 需要编号的活动集合
		List<JecnBaseFlowElementPanel> activityList = new ArrayList<JecnBaseFlowElementPanel>();

		// to、from集合
		Map<String, JecnBaseFlowElementPanel> toFromMaps = new HashMap<String, JecnBaseFlowElementPanel>();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		// 循环所有元素
		for (int i = 0; i < figureList.size(); i++) {
			JecnBaseFlowElementPanel baseFlowElement = figureList.get(i);
			// 判断是否属于活动
			if (isActive(baseFlowElement.getFlowElementData().getMapElemType())) {
				JecnBaseActiveFigure aFigure = (JecnBaseActiveFigure) baseFlowElement;
				// 为true 说明 不能全部编号
				if (isActiveCode) {
					// 记录操作前数据
					undoData.recodeFlowElement(baseFlowElement);
					String activeId = null;
					// 判断编号是否为空
					if (aFigure.getFlowElementData().getActivityNum() != null
							&& !"".equals(aFigure.getFlowElementData().getActivityNum().trim())) {
						// 截取编号后面的数字
						activeId = aFigure.getFlowElementData().getActivityNum().substring(
								aFigure.getFlowElementData().getActivityNum().lastIndexOf("-") + 1,
								aFigure.getFlowElementData().getActivityNum().length());

					}
					if (!"".equals(curActivityId)) {
						if (activeId != null) {
							// 获取所有的活动的编号数字
							int strInt = Integer.valueOf(activeId).intValue();
							// 获取点击的活动的编号数字
							strActInt = Integer.valueOf(curActivityId);
							// 判断如果活动的编号数字大于点击活动的编号数字就把活动存入List中
							if (strInt >= (int) strActInt) {
								// 判断此list是否包含此活动
								if (!activityList.contains(baseFlowElement)) {
									activityList.add(baseFlowElement);
								}
							}
						} else {
							activityList.add(baseFlowElement);
						}
					}
					// 记录操作后数据
					redoData.recodeFlowElement(baseFlowElement);
				} else {
					if (!activityList.contains(baseFlowElement)) {// 当不包含时再添加
						activityList.add(baseFlowElement);
					}
				}
			} else if (isToFromFigure(baseFlowElement.getFlowElementData().getMapElemType())) {// to/from
				JecnToFromRelatedData toFromRelatedData = (JecnToFromRelatedData) baseFlowElement.getFlowElementData();
				toFromMaps.put(toFromRelatedData.getRelatedUUId(), baseFlowElement);
			}
		}
		if (activityList.size() == 0) {
			this.dispose();
			return;
		}
		if (flag) {
			// 活动排序从左至右 自上至下
			leftToRightSort(activityList);
		} else {
			// 活动排序从上到下 自左到右
			topToDownSort(activityList);
		}
		Integer actCode = 0;

		// 协作框内的编号是否保持一致
		if (JecnDesignerProcess.getDesignerProcess().isSameNumberInDottedRect()) {
			// 记录面板内的协作框
			final String KEY_FIRSTNUMBER = "FIRST_NUMBER"; // 协作框内第一个活动的编号 KEY
			final String KEY_DOTTEDRECT = MapElemType.DottedRect.toString(); // 协作框实例
			List<Map<String, Object>> dottedRects = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < figureList.size(); i++) {
				JecnBaseFlowElementPanel baseFlowElement = figureList.get(i);
				if (isDottedRect(baseFlowElement.getFlowElementData().getMapElemType())) {
					Map<String, Object> dr = new HashMap<String, Object>();
					dr.put(KEY_DOTTEDRECT, baseFlowElement);
					dottedRects.add(dr);
				}
			}
			int activityRottedCount = 0;
			int temp = 0;
			for (int i = 0; i < activityList.size(); i++) {
				JecnBaseFlowElementPanel activefigure = activityList.get(i);
				// 记录操作前数据
				undoData.recodeFlowElement(activefigure);
				JecnActiveData activeData = (JecnActiveData) activefigure.getFlowElementData();
				if (isActiveCode && strActInt != 0) {
					actCode = Integer.valueOf(i) + strActInt;
				} else {
					actCode = Integer.valueOf(i + 1);
				}
				// 编号规则
				actCode = actCode - activityRottedCount;
				temp = i - activityRottedCount;
				numsRule(activeData, indexType, actCode, activeText, temp);

				// 处理协作框内的活动的编号
				Map<String, Object> parentDottedRect = getParentDottedRect(activefigure, dottedRects);
				if (parentDottedRect != null) {
					if (parentDottedRect.get(KEY_FIRSTNUMBER) == null) {
						parentDottedRect.put(KEY_FIRSTNUMBER, activeData.getActivityNum());
					} else {
						activeData.setActivityNum(parentDottedRect.get(KEY_FIRSTNUMBER).toString());
						activityRottedCount++;
					}
				}

				activefigure.updateUI();
				// 存在关联元素TO和from，更新文本
				isRelatedToFromFigure(activefigure, toFromMaps);
				// 记录操作后数据
				redoData.recodeFlowElement(activefigure);

			}
		} else {
			for (int i = 0; i < activityList.size(); i++) {
				JecnBaseFlowElementPanel activefigure = activityList.get(i);
				// 记录操作前数据
				undoData.recodeFlowElement(activefigure);
				JecnActiveData activeData = (JecnActiveData) activefigure.getFlowElementData();
				if (isActiveCode && strActInt != 0) {
					actCode = Integer.valueOf(i) + strActInt;
				} else {
					actCode = Integer.valueOf(i + 1);
				}
				numsRule(activeData, indexType, actCode, activeText, i);

				activefigure.updateUI();
				// 存在关联元素TO和from，更新文本
				isRelatedToFromFigure(activefigure, toFromMaps);
				// 记录操作后数据
				redoData.recodeFlowElement(activefigure);

			}
		}

		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);

		workflow.getUndoRedoManager().addEdit(unredoProcess);
		this.dispose();
	}

	private Map<String, Object> getParentDottedRect(JecnBaseFlowElementPanel activefigure,
			List<Map<String, Object>> dottedRects) {
		Map<String, Object> parentDottedRect = null;
		for (Map<String, Object> v : dottedRects) {
			JecnBaseFlowElementPanel dottedRect = (JecnBaseFlowElementPanel) v.get(MapElemType.DottedRect.toString());
			if (DrawCommon.isInside(activefigure, dottedRect)) {
				parentDottedRect = v;
			}
		}
		return parentDottedRect;
	}

	/**
	 * 编号规则
	 * 
	 * @param activeData
	 * @param indexType
	 * @param actCode
	 * @param activeText
	 * @param i
	 */
	protected void numsRule(JecnActiveData activeData, int indexType, Integer actCode, String activeText, int i) {
		if (indexType == 0) {
			activeData.setActivityNum(activeText + actCode.toString());
		} else if (indexType == 1) {
			if (i < 9) {
				activeData.setActivityNum(activeText + "0" + actCode.toString());
			} else {
				activeData.setActivityNum(activeText + actCode.toString());
			}
		} else if (indexType == 2) {
			if (i < 9) {
				activeData.setActivityNum(activeText + "00" + actCode.toString());
			}
			if (9 <= i && i < 99) {
				activeData.setActivityNum(activeText + "0" + actCode.toString());
			}
			if (i >= 99) {
				activeData.setActivityNum(activeText + actCode.toString());
			}
		}
	}

	/**
	 * 存在关联元素TO和from，更新文本
	 * 
	 * @param activefigure
	 * @param toFromMaps
	 */
	protected void isRelatedToFromFigure(JecnBaseFlowElementPanel activefigure,
			Map<String, JecnBaseFlowElementPanel> toFromMaps) {
		JecnActiveData figureData = (JecnActiveData) activefigure.getFlowElementData();
		if (toFromMaps.containsKey(figureData.getFlowElementUUID())) {
			JecnBaseFlowElementPanel toFromFigure = toFromMaps.get(figureData.getFlowElementUUID());
			// 创建记录撤销恢复的流程元素数据的对象
			JecnUndoRedoData undoData = new JecnUndoRedoData();
			// 创建记录撤销恢复的流程元素数据的对象
			JecnUndoRedoData redoData = new JecnUndoRedoData();
			// 记录操作前数据
			undoData.recodeFlowElement(toFromFigure);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getAidCompCollection().getRelatedActivePanel()
					.refreshRelatedText(toFromFigure, figureData);
			redoData.recodeFlowElement(toFromFigure);
			// 记录这次操作的数据集合
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
			workflow.getUndoRedoManager().addEdit(unredoProcess);
		}
	}

	/**
	 * 活动排序从左至右 自上至下
	 * 
	 * @param activityList
	 *            需要排序的活动的集合
	 */
	private void leftToRightSort(List<JecnBaseFlowElementPanel> activityList) {
		// 循环需要排序的活动的集合
		for (int i = 0; i < activityList.size(); i++) {
			for (int j = activityList.size() - 1; j > i; j--) {
				JecnActiveData activeData = (JecnActiveData) activityList.get(j).getFlowElementData();
				JecnActiveData activeNextData = (JecnActiveData) activityList.get(j - 1).getFlowElementData();
				// 判断X的坐标来判断顺序
				if (activeData.getOriginalLocation().getX() < activeNextData.getOriginalLocation().getX()) {
					JecnBaseFlowElementPanel activity = activityList.get(j - 1);
					activityList.set(j - 1, activityList.get(j));
					activityList.set(j, activity);
				}
			}
		}

		// 判断如果X的坐标一样，就比较Y的坐标
		for (int i = 0; i < activityList.size(); i++) {
			for (int j = activityList.size() - 1; j > i; j--) {
				JecnActiveData activeData = (JecnActiveData) activityList.get(j).getFlowElementData();
				JecnActiveData activeNextData = (JecnActiveData) activityList.get(j - 1).getFlowElementData();
				if (activeData.getOriginalLocation().getX() == activeNextData.getOriginalLocation().getX()) {
					if (activeData.getOriginalLocation().getY() <= activeNextData.getOriginalLocation().getY()) {
						JecnBaseFlowElementPanel activity = activityList.get(j - 1);
						activityList.set(j - 1, activityList.get(j));
						activityList.set(j, activity);
					}
				}
			}
		}
	}

	/**
	 * 活动排序从上到下 自左到右
	 * 
	 * @param activityList
	 *            需要排序的活动的集合
	 */
	private void topToDownSort(List<JecnBaseFlowElementPanel> activityList) {
		// 循环需要排序的活动的集合
		for (int i = 0; i < activityList.size(); i++) {
			// 冒泡排序
			for (int j = activityList.size() - 1; j > i; j--) {
				JecnActiveData activeData = (JecnActiveData) activityList.get(j).getFlowElementData();
				JecnActiveData activeNextData = (JecnActiveData) activityList.get(j - 1).getFlowElementData();
				if (activeData.getOriginalLocation().getY() < activeNextData.getOriginalLocation().getY()) {
					JecnBaseFlowElementPanel activity = activityList.get(j - 1);
					activityList.set(j - 1, activityList.get(j));
					activityList.set(j, activity);
				}
			}
		}

		// 判断如果Y的坐标一样，就比较X的坐标
		for (int i = 0; i < activityList.size(); i++) {
			// 冒泡排序
			for (int j = activityList.size() - 1; j > i; j--) {
				JecnActiveData activeData = (JecnActiveData) activityList.get(j).getFlowElementData();
				JecnActiveData activeNextData = (JecnActiveData) activityList.get(j - 1).getFlowElementData();
				if (activeData.getOriginalLocation().getY() == activeNextData.getOriginalLocation().getY()) {
					if (activeData.getOriginalLocation().getX() <= activeNextData.getOriginalLocation().getX()) {
						JecnBaseFlowElementPanel activity = activityList.get(j - 1);
						activityList.set(j - 1, activityList.get(j));
						activityList.set(j, activity);
					}
				}
			}
		}
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param curStr
	 *            传入的参数
	 * @return
	 */
	private boolean checkIsNumber(String curStr) {
		if (curStr.matches("[0-9]*")) {
			return true;
		}
		return false;
	}

	/**
	 * 判断是否是活动
	 * 
	 * @param figure
	 *            图形的标识
	 * @return
	 */
	private boolean isActive(MapElemType figure) {
		return DrawCommon.isActive(figure);
	}

	private boolean isToFromFigure(MapElemType figure) {
		if ("ToFigure".equals(figure.toString()) || "FromFigure".equals(figure.toString())) {
			return true;
		}
		return false;
	}

	private boolean isDottedRect(MapElemType figureType) {
		if (MapElemType.DottedRect.equals(figureType)) {
			return true;
		}
		return false;
	}

	/** button的点击事件 */
	public void actionPerformed(ActionEvent e) {
		// 点击是否为 JButton
		if (e.getSource() instanceof JButton) {
			// 取得点击的JButton
			JButton button = (JButton) e.getSource();
			String butName = button.getActionCommand();
			if (JecnActionCommandConstants.OK_BUTTON.equals(butName)) {// OK按钮
				// true 为横向 false 为纵向
				boolean flag = workflow.getFlowMapData().isHFlag();
				int index = numberTypeJcomboBox.getSelectedIndex();
				buildArabia(index, flag);
			} else if (JecnActionCommandConstants.CANCEL_BUTTON.equals(butName)) {
				this.dispose();
			}
		} else if (e.getSource() instanceof JComboBox) {
			if (numberNameFlag) {
				return;
			}
			String numberName = numberTextField.getText();
			showTextArea.setText(numberPreview(numberName));
		}
	}

	public JComboBox getNumberTypeJcomboBox() {
		return numberTypeJcomboBox;
	}

	public void setNumberTypeJcomboBox(JComboBox numberTypeJcomboBox) {
		this.numberTypeJcomboBox = numberTypeJcomboBox;
	}

	public JecnDrawDesktopPane getWorkflow() {
		return workflow;
	}

	public void setWorkflow(JecnDrawDesktopPane workflow) {
		this.workflow = workflow;
	}

	/**
	 * 
	 * 输入框编辑监听
	 * 
	 * @author ZHOUXY
	 * 
	 */
	class JecnDocumentListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent e) {
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			numberNameFlag = initErrorLabel();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			numberNameFlag = initErrorLabel();
		}
	}
}
