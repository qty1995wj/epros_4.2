package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.FlowDriver;
import com.jecn.epros.server.email.buss.ToolEmail;

public class DriverTipBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {
		Object[] data = getData();
		FlowDriver flowDriver = (FlowDriver) data[0];
		return createEmail(flowDriver, user);
	}

	private EmailBasicInfo createEmail(FlowDriver flowDriver,JecnUser user) {
		// 每个流程参与者都要接收邮件，点击邮件连接地址直接查看流程
		EmailBasicInfo basicInfo = new EmailBasicInfo();
		String subject = "流程驱动提醒";
		// 邮件内容
		String emailContent = '"' + flowDriver.getFlowName() + '"' + "流程需在" + flowDriver.getDriverTime()
				+ "时间启动，请注意按期执行，点击链接查看，谢谢！";
		EmailContentBuilder builder = new EmailContentBuilder();
		builder.setContent(emailContent);
		builder.setResourceUrl(ToolEmail.getHttpUrl(flowDriver.getFlowId(), "process", user.getPeopleId()));
		basicInfo.setContent(builder.buildContent());
		basicInfo.setSubject(subject);
		basicInfo.addRecipients(user);
		return basicInfo;
	}

}
