package epros.draw.gui.top.frame;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.top.frame.listener.JecnFrameEvent;
import epros.draw.gui.top.frame.listener.JecnFrameListener;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 执行窗体标题区拖动的面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFrameDropTitlePanel extends JPanel implements
		MouseMotionListener, MouseListener, ActionListener {
	/** 日志对象 */
	private final Log log = LogFactory.getLog(JecnFrameDropTitlePanel.class);
	/** 窗体 */
	protected JecnFrame frame = null;
	/** 用于拖动的虚图 */
	protected JFrame virtualFrame = null;

	/** 是否可由用户调整大小（最大化按钮是否显示，拖动窗体边框是否让拖动）：true可以调整 false不可以调整 */
	protected boolean resizable = true;

	/** 按下时坐标 */
	protected Point pressedPoint = null;
	/** 释放时基于屏幕坐标 */
	protected Point destPoint = null;

	/** 按下标识： true：按下 false：未按下 */
	protected boolean isPressed = false;
	/** 拖动标识 true：拖动 false：未拖动 */
	protected boolean isDragged = false;

	public JecnFrameDropTitlePanel(JecnFrame frame) {
		if (frame == null) {
			log.error("JecnHeaderPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.frame = frame;

		initComponents();
	}

	private void initComponents() {
		// 透明
		this.setOpaque(false);
		// 布局管理器
		this.setLayout(new BorderLayout());
		// 事件
		this.addMouseListener(this);
		this.addMouseMotionListener(this);

		virtualFrame = new JFrame();
		virtualFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		// 禁用此对话框的装饰
		virtualFrame.setUndecorated(true);
		// 设置此对话框是否可由用户调整大小
		virtualFrame.setResizable(true);
		virtualFrame.getContentPane().setBackground(
				JecnUIUtil.getDialogDragColor());

		// 布局
		virtualFrame.getContentPane().setLayout(new GridBagLayout());
		//
		JPanel label = new JPanel();
		label.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0);
		virtualFrame.getContentPane().add(label, c);
	}

	/**
	 * 
	 * 按下
	 * 
	 */
	public void mousePressed(MouseEvent event) {
		if (isMaxFlag()) {
			// 属性设置初始值
			initArris();
			return;
		}
		// 按下
		isPressed = true;

		// 获取按下坐标
		pressedPoint = event.getPoint();

		virtualFrame.setBounds(frame.getBounds());
	}

	/**
	 * 
	 * 释放
	 * 
	 */
	public void mouseReleased(MouseEvent event) {
		if (!isMaxFlag() && isDragged && isPressed && destPoint != null) {// 有按下且拖动才执行
			// 设置窗体位置点
			frame.setLocation(destPoint);
		} else {
			if (event.getClickCount() == 2
					&& SwingUtilities.isLeftMouseButton(event)
					&& (event.getSource() instanceof JecnFrameDropTitlePanel)) {// 左键双击
				if (!isMaxFlag()) {
					if (!resizable) {
						return;
					}
					setMaxFrameBounds();
					// 设置图片
					frame.getFrameHeaderPanel().getFrameTitlePanel()
							.resizeBtnMaxIcon();
				} else {
					setNormalFrameBounds();
					// 设置图片
					frame.getFrameHeaderPanel().getFrameTitlePanel()
							.resizeBtnResizeIcon();
				}
			}
		}

		// 属性设置初始值
		initArris();
	}

	/**
	 * 
	 * 拖动
	 * 
	 * 
	 */
	public void mouseDragged(MouseEvent event) {
		// 界面最大化时，不用拖动
		if (!isPressed || frame == null
				|| frame.getExtendedState() == JFrame.MAXIMIZED_BOTH
				|| isMaxFlag()) {
			isDragged = false;
			return;
		}
		if (!virtualFrame.isVisible()) {
			virtualFrame.setVisible(true);
		}

		destPoint = new Point(frame.getLocation().x + event.getX()
				- pressedPoint.x, frame.getLocation().y + event.getY()
				- pressedPoint.y);
		virtualFrame.setLocation(destPoint);

		isDragged = true;
	}

	public void mouseEntered(MouseEvent paramMouseEvent) {

	}

	public void mouseExited(MouseEvent paramMouseEvent) {

	}

	public void mouseMoved(MouseEvent paramMouseEvent) {

	}

	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * 
	 * 
	 * 窗体最小化、最大化、关闭按钮事件
	 * 
	 */
	public void actionPerformed(ActionEvent paramActionEvent) {
		if (paramActionEvent.getSource() instanceof AbstractButton) {
			// 事件源
			AbstractButton btn = (AbstractButton) paramActionEvent.getSource();
			if ("minBtn".equals(btn.getName())) {
				frame.setExtendedState(JFrame.ICONIFIED); // 最小化
			} else if ("maxBtn".equals(btn.getName())) {
				if (!isMaxFlag()) {
					setMaxFrameBounds();
				} else {
					setNormalFrameBounds();
				}
			} else if ("exitBtn".equals(btn.getName())) {// 关闭
				// 添加事件
				Map<JecnFrameListener, Object> frameListenerList = frame
						.getFrameListenerList();

				if (frameListenerList == null) { // 没有监听
					System.exit(0);
					return;
				}

				// 有监听
				for (JecnFrameListener frameListener : frameListenerList
						.keySet()) {
					// value值
					Object obj = frameListenerList.get(frameListener);
					// 执行关闭前方法是否关闭
					boolean isClose = frameListener
							.FrameCloseBefore(new JecnFrameEvent(obj));
					if (isClose) {// 关闭
						System.exit(0);
					} else {// 不关闭
						continue;
					}
				}
			}
		}
	}

	/**
	 * 
	 * 全屏
	 * 
	 */
	void setMaxFrameBounds() {
		frame.setNormalRect(frame.getBounds());

		// 屏幕大小
		Rectangle frameRect = frame.getGraphicsConfiguration().getBounds();
		Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(
				frame.getGraphicsConfiguration());
		frameRect.x += insets.left;
		frameRect.y += insets.top;
		frameRect.width -= insets.left + insets.right;
		frameRect.height -= insets.top + insets.bottom;

		// 最大化
		// frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		setFrameBounds(frameRect);
		// 是否需要显示为全屏：true：全屏 false：默认大小
		frame.setJecnMaxFlag(true);
	}

	/**
	 * 
	 * 默认大小
	 * 
	 */
	void setNormalFrameBounds() {
		setFrameBounds(frame.getNormalRect());
		// 是否需要显示为全屏：true：全屏 false：默认大小
		frame.setJecnMaxFlag(false);;
	}

	private void setFrameBounds(Rectangle rect) {
		if (frame == null || rect == null) {
			return;
		}
		frame.setBounds(rect);
	}

	/**
	 * 
	 * 属性设置初始值
	 * 
	 */
	private void initArris() {
		// 设置按下标识
		isPressed = false;
		// 设置拖动标识
		isDragged = false;

		if (virtualFrame.isVisible()) {
			virtualFrame.setVisible(false);
		}
	}

	public boolean isResizable() {
		return resizable;
	}

	/**
	 * 
	 * 
	 * 设置是否支持用户调整大小
	 * 
	 * @param resizable
	 */
	public void setResizable(boolean resizable) {
		this.resizable = resizable;
	}

	boolean isMaxFlag() {
		return frame.isJecnMaxFlag();
	}
}
