package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.caucho.hessian.client.HessianProxyFactory;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.HessianConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 
 * 基于Hessian技术同步数据
 * 
 */
public class ImportUserByHessian extends AbstractImportUser {

	public ImportUserByHessian(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 
	 * 获取部门数据集合
	 * 
	 * @return
	 */
	public List<DeptBean> getDeptBeanList() throws Exception {

		List<DeptBean> deptList = new ArrayList<DeptBean>();

		HessianConfigBean hesConfigBean = (HessianConfigBean) configBean;

		// 获取远程数据
		List<String[]> strList = getHessianData(hesConfigBean.getDeptIClass(),
				hesConfigBean.getDeptMothed(), hesConfigBean.getUrl());

		if (strList != null) {
			for (String[] filed : strList) {
				if (filed == null || filed.length != 3) {
					continue;
				}
				DeptBean deptBean = new DeptBean();
				// 给部门属性赋值
				deptBean.setAttributes(filed);
				deptList.add(deptBean);
			}
		}

		return deptList;
	}

	/**
	 * 
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	public List<UserBean> getUserBeanList() throws Exception {

		List<UserBean> userList = new ArrayList<UserBean>();

		HessianConfigBean hesConfigBean = (HessianConfigBean) configBean;

		// 获取远程数据
		List<String[]> strList = getHessianData(hesConfigBean.getUserIClass(),
				hesConfigBean.getUserMothed(), hesConfigBean.getUrl());

		if (strList != null) {
			for (String[] filed : strList) {
				if (filed == null || filed.length != 3) {
					continue;
				}
				UserBean userBean = new UserBean();
				// 给部门属性赋值
				userBean.setAttributes(filed);
				userList.add(userBean);
			}
		}
		return userList;
	}

	/**
	 * 
	 * 获取远程数据
	 * 
	 * @param iClass
	 *            远程接口名称
	 * @param iMothed
	 *            远程接口对应方法
	 * @param url
	 *            远程url
	 * @return List<String[]>
	 * @throws Exception
	 */
	private List<String[]> getHessianData(String iClass, String iMothed,
			String url) throws Exception {

		// 获取Hessian工厂对象
		HessianProxyFactory factory = getHessianProxyFactory();

		// 接口
		Class clazz = Class.forName(iClass);

		// 获取远程对象对应方法
		Method method = clazz.getMethod(iMothed);

		// 获取远程对象
		Object obj = factory.create(clazz, url);

		// 执行调用方法
		return (List<String[]>) method.invoke(obj);
	}
}
