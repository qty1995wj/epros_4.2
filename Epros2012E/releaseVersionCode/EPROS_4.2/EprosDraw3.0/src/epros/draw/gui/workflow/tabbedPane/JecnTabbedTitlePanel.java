package epros.draw.gui.workflow.tabbedPane;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.JecnFlowMapData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.shape.MapNameText;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;

/**
 * 
 * 分页标题面板：包括画图面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTabbedTitlePanel extends JPanel {
	/** 左面图标标签 */
	private JLabel leftIconLabel = null;
	/** 中间内容区标签 */
	private JLabel contentLabel = null;
	/** 右面关闭按钮标签 */
	private JecnTabbedTitleButton closeBtn = null;
	/** 标题名称 */
	private String tabText = null;
	/** 分页面板：JecnDrawTabbedPane对象 */
	private JecnDrawTabbedPane tabbedPane = null;
	/** 当前流程图或流程地图对应的数据对象 */
	private JecnFlowMapData flowMapData = null;
	/** 画图面板 */
	private JecnDrawDesktopPane currDesktopPane = null;

	public JecnTabbedTitlePanel(JecnDrawTabbedPane tabbedPane,
			JecnFlowMapData flowMapData) {
		if (tabbedPane == null || flowMapData == null) {
			JecnLogConstants.LOG_TABBED_TITLE_PANEL
					.error("JecnTabbedTitlePanel类构造函数：参数为null。 tabbedPane="
							+ tabbedPane + ";flowMapData=" + flowMapData);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.tabbedPane = tabbedPane;
		this.flowMapData = flowMapData;

		// 初始化组件
		initCompotents();

		// 添加新选项卡
		addTab();
	}

	private void initCompotents() {

		// 左面图标标签
		leftIconLabel = new JLabel();
		// 中间内容区标签
		contentLabel = new JLabel();
		// 右面关闭按钮标签
		closeBtn = new JecnTabbedTitleButton(this);
		// 实例化画图面板
		currDesktopPane = new JecnDrawDesktopPane(flowMapData, this);

		// 布局
		this.setLayout(new BorderLayout(2, 2));

		// 边框
		leftIconLabel.setBorder(null);
		contentLabel.setBorder(null);

		// 透明的
		this.setOpaque(false);
		leftIconLabel.setOpaque(false);
		contentLabel.setOpaque(false);
		closeBtn.setOpaque(false);

		this.add(leftIconLabel, BorderLayout.WEST);
		this.add(contentLabel, BorderLayout.CENTER);
		this.add(closeBtn, BorderLayout.EAST);
	}

	/**
	 * 
	 * 添加新选项卡
	 * 
	 */
	private void addTab() {
		// 添加选项卡中内容区域
		tabbedPane.addTab(null, currDesktopPane.getScrollPanle());

		// 当前画图面板索引
		int index = tabbedPane.indexOfComponent(currDesktopPane
				.getScrollPanle());

		// 设置标题名称
		this.setTabText(flowMapData.getName());
		// 添加到选项卡中标题区域
		tabbedPane.setTabComponentAt(index, this);

		// 设置 流程图或流程地图的图标
		setMapImageIcon();

		// 选中新添加的选项卡，此代码不能去掉，它是触发tabblepane组件change事件的
		tabbedPane.setSelectedComponent(currDesktopPane.getScrollPanle());
	}

	/**
	 * 
	 * 设置流程图或流程地图图片
	 * 
	 */
	private void setMapImageIcon() {
		ImageIcon icon = null;
		switch (flowMapData.getMapType()) {
		case partMap:
			icon = JecnWorkflowUtil.getPartMapIcon();
			break;
		case totalMap:
			icon = JecnWorkflowUtil.getTotalMapIcon();
			break;
		case orgMap:
			icon = JecnWorkflowUtil.getOrgMapIcon();
			break;
		default:
			break;
		}
		if (icon != null) {// 图标不为空
			this.setTabIcon(icon);
		}
	}

	/**
	 * 
	 * 设置标题内容
	 * 
	 * @param text
	 *            标题内容
	 */
	public void setTabText(String tabText) {
		contentLabel.setText(tabText);
		this.tabText = tabText;
		List<JecnBaseFlowElementPanel> alFigureList = currDesktopPane
				.getPanelList();
		for (JecnBaseFlowElementPanel baseFlowElementPanel : alFigureList) {
			if (baseFlowElementPanel instanceof MapNameText) {
				baseFlowElementPanel.getFlowElementData()
						.setFigureText(tabText);
			}
		}
		currDesktopPane.repaint();
	}

	/**
	 * 
	 * 设置流程类型图片
	 * 
	 * @param icon
	 *            流程类型图片
	 */
	public void setTabIcon(Icon icon) {
		leftIconLabel.setIcon(icon);
	}

	public JecnDrawDesktopPane getCurrDesktopPane() {
		return currDesktopPane;
	}

	public JecnDrawTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public JecnTabbedTitleButton getCloseBtn() {
		return closeBtn;
	}

	public String getTabText() {
		return tabText;
	}
}
