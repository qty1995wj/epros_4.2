package epros.draw.gui.top.toolbar;

import java.awt.Color;
import java.util.List;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.shape.ManhattanLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.dialog.JecnActivityCodeBuildDialog;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemOftenColorData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;

/**
 * 元素属性修改通用类
 * 
 * @author fuzhz
 * 
 */
public class JecnElementAttributesUtil {

	/**
	 * 修改字体的大小
	 * 
	 * @param elementPanel
	 *            图形
	 * @param fontSize
	 *            大小
	 */
	public static void updateFontSize(JecnBaseFlowElementPanel elementPanel, int fontSize) {
		// 设置图形的大小
		elementPanel.getFlowElementData().setFontSize(fontSize);
	}

	/**
	 * 修改字体样式
	 * 
	 * @param elementPanel
	 *            图形
	 * @param fontStyle
	 *            字形样式（普通、加粗BOLD、斜线ITALIC） 默认普通样式PLAIN（0） ，1 加粗
	 */
	public static void updateFontStyle(JecnBaseFlowElementPanel elementPanel, int fontStyle) {
		// 设置图形字体样式
		elementPanel.getFlowElementData().setFontStyle(fontStyle);
	}

	/**
	 * 修改字体类型
	 * 
	 * @param elementPanel
	 *            图形
	 * @param fontStyle
	 *            字体类型 默认为宋体
	 */
	public static void updateFontType(JecnBaseFlowElementPanel elementPanel, String fontType) {
		// 设置图形字体类型
		elementPanel.getFlowElementData().setFontName(fontType);
	}

	/**
	 * 设置字体的横竖排
	 * 
	 * @param elementPanel
	 *            图形
	 * @param isUpRight
	 *            false:字体横排 true:字体竖排 默认横排
	 */
	public static void updateFontUpRight(JecnBaseFlowElementPanel elementPanel, boolean isUpRight) {
		// 设置图形字体横竖排
		elementPanel.getFlowElementData().setFontUpRight(isUpRight);
	}

	/**
	 * 修改字体的位置
	 * 
	 * @param elementPanel
	 *            图形
	 * @param position
	 *            位置点 （0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下）
	 */
	public static void updateFontPosition(JecnBaseFlowElementPanel elementPanel, int position) {
		// 设置图形字体位置
		elementPanel.getFlowElementData().setFontPosition(position);
	}

	/**
	 * 修改字体的颜色
	 * 
	 * @param elementPanel
	 *            图形
	 * @param fontColor
	 *            字体颜色
	 */
	public static void updateFontColor(JecnBaseFlowElementPanel elementPanel, Color fontColor) {
		// 设置图形字体的颜色
		elementPanel.getFlowElementData().setFontColor(fontColor);
	}

	/**
	 * 修改线条颜色
	 * 
	 * @param elementPanel
	 *            图形
	 * @param LineColor
	 *            线条颜色
	 */
	public static void updateLineColor(JecnBaseFlowElementPanel elementPanel, Color LineColor) {
		// 设置线条的颜色
		elementPanel.getFlowElementData().setBodyColor(LineColor);
	}

	/**
	 * 修改线条的粗细
	 * 
	 * @param elementPanel
	 *            图形
	 * @param lineWidth
	 *            线条粗细
	 */
	public static void updateLineWidth(JecnBaseFlowElementPanel elementPanel, int lineWidth) {
		// 设置线条的粗细
		elementPanel.getFlowElementData().setBodyWidth(lineWidth);
	}

	/**
	 * 修改线条类型
	 * 
	 * @param elementPanel
	 *            图形
	 * @param lineType
	 *            0：实线；1：虚线；2：点线；3：双线
	 */
	public static void updateLineType(JecnBaseFlowElementPanel elementPanel, int lineType) {
		// 设置线条的类型
		elementPanel.getFlowElementData().setBodyStyle(lineType);
	}

	/**
	 * 修改图形3D设置
	 * 
	 * @param elementPanel
	 *            图形
	 * @param flag
	 *            标识
	 */
	public static void updateFlag3D(JecnBaseFlowElementPanel elementPanel, boolean flag) {
		// 设置图形3D
		elementPanel.getFlowElementData().setFlag3D(flag);
	}

	/**
	 * 修改各颜色选择时常用颜色的List
	 */
	public static void alertColorList(JecnSystemOftenColorData systemOftenColorData, ToolBarElemType toolBarElemType,
			Color color) {
		// 是否修改标识
		boolean flag = false;
		switch (toolBarElemType) {
		case editFillSelectColor: // 填充选择颜色
			for (Color deColor : systemOftenColorData.getFillColorList()) {
				if (deColor.equals(color)) {
					flag = true;
				}
			}
			// 判断颜色不重复
			if (!flag) {
				systemOftenColorData.getFillColorList().remove(7);
				systemOftenColorData.getFillColorList().add(0, color);
			}
			break;
		case editShadowSelectColor: // 阴影选择颜色
			for (Color deColor : systemOftenColorData.getShadowColorList()) {
				if (deColor.equals(color)) {
					flag = true;
				}
			}
			// 判断颜色不重复
			if (!flag) {
				systemOftenColorData.getShadowColorList().remove(7);
				systemOftenColorData.getShadowColorList().add(0, color);
			}
			break;
		case editLineSelectColor: // 线条选择颜色
			for (Color deColor : systemOftenColorData.getLineColorList()) {
				if (deColor.equals(color)) {
					flag = true;
				}
			}
			// 判断颜色不重复
			if (!flag) {
				systemOftenColorData.getLineColorList().remove(7);
				systemOftenColorData.getLineColorList().add(0, color);
			}
			break;
		case editfontSelectColor: // 字体选择颜色
			for (Color deColor : systemOftenColorData.getFontColorList()) {
				if (deColor.equals(color)) {
					flag = true;
				}
			}
			// 判断颜色不重复
			if (!flag) {
				systemOftenColorData.getFontColorList().remove(7);
				systemOftenColorData.getFontColorList().add(0, color);
			}
			break;
		}
	}

	/**
	 * 修改工具栏
	 */
	public static void updateEditTool(JecnBaseFlowElementPanel baseFlowElementPanel) {

		// 获取面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow.getCurrentSelectElement().size() != 1) {
			return;
		}

		// 修改工具栏上的字体类型
		getEidtToolBarItemPanel().getFontNameComboBox().setSelectedItem(
				baseFlowElementPanel.getFlowElementData().getFontName());
		// 修改工具栏上的字体大小
		getEidtToolBarItemPanel().getFontSizeComboBox().setSelectedItem(
				baseFlowElementPanel.getFlowElementData().getFontSize());
		// 修改工具栏上的字体位置
		getEidtToolBarItemPanel().getFontLocationComboBox().setSelectedIndex(
				baseFlowElementPanel.getFlowElementData().getFontPosition());
		// 修改工具栏上的边框大小
		getEidtToolBarItemPanel().getBodyWidthComboBox().setSelectedItem(
				baseFlowElementPanel.getFlowElementData().getBodyWidth());
		// 修改工具栏上的边框类型
		getEidtToolBarItemPanel().getStyleTypeComboBox().setSelectedIndex(
				baseFlowElementPanel.getFlowElementData().getBodyStyle());

		if (baseFlowElementPanel instanceof ManhattanLine) {
			ManhattanLine line = (ManhattanLine) baseFlowElementPanel;
			getEidtToolBarItemPanel().getTwoArrowCheckBox().setSelected(line.getFlowElementData().isTwoArrow());
		} else {
			getEidtToolBarItemPanel().getTwoArrowCheckBox().setSelected(false);
		}
	}

	/**
	 * 修改元素颜色
	 * 
	 * @param colorComm
	 *            动作事件标识
	 */
	public static void updateElementColor(String colorComm) {
		if (getWorkflow() == null) {
			return;
		}
		if ("editFill".equals(colorComm)) {// 填充选择颜色
			Color color = JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel()
					.getStyleFillColorBtn().getColorButonPanel().getBackground();
			updateElementFillColor(getCurrentSelectElement(), color);
		} else if ("editShadow".equals(colorComm)) {// 阴影选择颜色
			Color color = JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getStyleShadowsBtn()
					.getColorButonPanel().getBackground();
			updateElementShadowColor(getCurrentSelectElement(), color);
		} else if ("editLine".equals(colorComm)) {// 线条选择颜色
			Color color = JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel()
					.getStyleLineColorBtn().getColorButonPanel().getBackground();
			updateElementLineColor(getCurrentSelectElement(), color);
		} else if ("editfontOftenColor".equals(colorComm)) {// 字体选择颜色
			Color color = JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getFontColorBtn()
					.getColorButonPanel().getBackground();
			updateElementFontColor(getCurrentSelectElement(), color);
		}
		getWorkflow().repaint();
	}

	/**
	 * 修改元素填充色
	 * 
	 * @param currentSelectElement
	 *            元素list
	 */
	public static void updateElementFillColor(List<JecnBaseFlowElementPanel> baseFlowElementList, Color color) {
		if (baseFlowElementList == null || baseFlowElementList.size() == 0 || color == null) {
			return;
		}
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
			if (DrawCommon.isNoSetFillColor(baseFlowElement.getFlowElementData().getMapElemType())) {// 流程等级图形不设置填充色
				continue;
			}
			// 记录操作前数据
			undoData.recodeFlowElement(baseFlowElement);

			// 设置图形填充类型
			baseFlowElement.getFlowElementData().setFillType(FillType.one);
			// 设置图形的颜色
			baseFlowElement.getFlowElementData().setFillColor(color);

			// 创建记录撤销恢复的流程元素数据的对象
			redoData.recodeFlowElement(baseFlowElement);
		}

		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
	}

	/**
	 * 修改元素阴影色
	 * 
	 * @param currentSelectElement
	 *            元素list
	 */
	public static void updateElementShadowColor(List<JecnBaseFlowElementPanel> baseFlowElementList, Color color) {
		if (baseFlowElementList == null || baseFlowElementList.size() == 0 || color == null) {
			return;
		}
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
			if (FillType.none.equals(baseFlowElement.getFlowElementData().getFillType())) {// 无填充不设置阴影
				continue;
			}
			// 判断是否为图形
			if (baseFlowElement instanceof JecnBaseFigurePanel) {
				// 记录操作前数据
				undoData.recodeFlowElement(baseFlowElement);

				// 设置图形填充类型
				baseFlowElement.getFlowElementData().setShadowsFlag(true);
				// 设置阴影颜色
				baseFlowElement.getFlowElementData().setShadowColor(color);

				// 设置阴影层级
				baseFlowElement.reSetFlowElementZorder();
				// 创建记录撤销恢复的流程元素数据的对象
				redoData.recodeFlowElement(baseFlowElement);
			}
		}
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
	}

	/**
	 * 修改元素线条
	 * 
	 * @param currentSelectElement
	 *            元素list
	 */
	public static void updateElementLineColor(List<JecnBaseFlowElementPanel> baseFlowElementList, Color color) {
		if (baseFlowElementList == null || baseFlowElementList.size() == 0 || color == null) {
			return;
		}
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
			// 记录操作前数据
			undoData.recodeFlowElement(baseFlowElement);

			// 设置线条颜色
			baseFlowElement.getFlowElementData().setBodyColor(color);

			if (baseFlowElement instanceof ManhattanLine) {
				ManhattanLine line = (ManhattanLine) baseFlowElement;
				line.setArrowColor();
			}
			// 创建记录撤销恢复的流程元素数据的对象
			redoData.recodeFlowElement(baseFlowElement);
		}
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
	}

	/**
	 * 修改元素字体色
	 * 
	 * @param currentSelectElement
	 *            元素list
	 */
	public static void updateElementFontColor(List<JecnBaseFlowElementPanel> baseFlowElementList, Color color) {
		if (baseFlowElementList == null || baseFlowElementList.size() == 0 || color == null) {
			return;
		}
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
			// 记录操作前数据
			undoData.recodeFlowElement(baseFlowElement);

			// 设置字体颜色
			baseFlowElement.getFlowElementData().setFontColor(color);

			// 创建记录撤销恢复的流程元素数据的对象
			redoData.recodeFlowElement(baseFlowElement);
		}
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
	}

	/**
	 * 获取面板
	 * 
	 * @return
	 */
	public static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 
	 * 获取画图面板上选中流程元素集合
	 * 
	 * @return List<JecnBaseFlowElementPanel> 选中流程元素集合
	 */
	public static List<JecnBaseFlowElementPanel> getCurrentSelectElement() {
		if (getWorkflow() == null) {
			return null;
		}
		return getWorkflow().getCurrentSelectElement();
	}

	/**
	 * 获取编辑工具栏
	 * 
	 * @return
	 */
	public static JecnEidtToolBarItemPanel getEidtToolBarItemPanel() {
		return JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel();
	}

	/**
	 * 流程活动自动编号
	 * 
	 */
	public static void formatAntoNum() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		if (JecnDesignerProcess.getDesignerProcess().isDraw()) {
			JecnActivityCodeBuildDialog activityNumber = new JecnActivityCodeBuildDialog(desktopPane);
			activityNumber.setVisible(true);
		} else {// 设计器 自动编号
			JecnDesignerProcess.getDesignerProcess().activeAutoNums();
		}
	}
}
