package com.jecn.epros.server.download.word;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.ActiveItSystem;
import com.jecn.epros.server.bean.download.DocConfig;
import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.HeaderDocBean;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.download.ProcessDownloadBaseBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.ProcessMapDownloadBean;
import com.jecn.epros.server.bean.download.RuleContentBean;
import com.jecn.epros.server.bean.download.RuleDownloadBean;
import com.jecn.epros.server.bean.download.RuleFileBean;
import com.jecn.epros.server.bean.download.RuleFlowBean;
import com.jecn.epros.server.bean.download.doc.ActiveInfo;
import com.jecn.epros.server.bean.download.doc.RoleInfo;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.ProcessInterfacesDescriptionBean;
import com.jecn.epros.server.bean.process.driver.JecnFlowDriverTimeBase;
import com.jecn.epros.server.bean.rule.JecnFlowCommon;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfo;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.rule.JecnRuleCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleFileT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.download.word.huadi.HuadiRuleDownload;
import com.jecn.epros.server.download.wordxml.aipusheng.APSProcessModel;
import com.jecn.epros.server.download.wordxml.badafu.BDFProcessModel;
import com.jecn.epros.server.download.wordxml.baolong.BLProcessModel;
import com.jecn.epros.server.download.wordxml.base.GeneralProcessModel;
import com.jecn.epros.server.download.wordxml.base.OldGeneralProcessModel;
import com.jecn.epros.server.download.wordxml.chengdu29.CD29ProcessModel;
import com.jecn.epros.server.download.wordxml.datangyidong.DTProcessModel;
import com.jecn.epros.server.download.wordxml.donghang.DHProcessModel;
import com.jecn.epros.server.download.wordxml.dongruanyiliao.DRYLProcessModel;
import com.jecn.epros.server.download.wordxml.fenghuo.FHRuleModel;
import com.jecn.epros.server.download.wordxml.gujiajiaju.GJJJProcessModel;
import com.jecn.epros.server.download.wordxml.gzjd.GZJDProcessModel;
import com.jecn.epros.server.download.wordxml.haikangweishi.HKWSProcessModel;
import com.jecn.epros.server.download.wordxml.hengshen.HSProcessModel;
import com.jecn.epros.server.download.wordxml.huadi.HDProcessModel;
import com.jecn.epros.server.download.wordxml.jiangbolong.JBLProcessFileMode;
import com.jecn.epros.server.download.wordxml.jinfeng.JFProcessModel2;
import com.jecn.epros.server.download.wordxml.jinlvkeche.JLKCProcessModel;
import com.jecn.epros.server.download.wordxml.jiuxinyaoye.JXYYProcessModel;
import com.jecn.epros.server.download.wordxml.kedaxunfei.KDXFProcessModel;
import com.jecn.epros.server.download.wordxml.lianhedianzi.LHDZProcessModel;
import com.jecn.epros.server.download.wordxml.libang.LBProcessModel;
import com.jecn.epros.server.download.wordxml.mengniu.MNProcessModel;
import com.jecn.epros.server.download.wordxml.mengniu.MNRuleModel;
import com.jecn.epros.server.download.wordxml.mulinsen.MLSProcessModel;
import com.jecn.epros.server.download.wordxml.nianjingshihua.NJSHProcessModel;
import com.jecn.epros.server.download.wordxml.nongfushanquan.NFSQProcessModel;
import com.jecn.epros.server.download.wordxml.sailun.SLProcessModel;
import com.jecn.epros.server.download.wordxml.shidaixincai.SDXCProcessModel;
import com.jecn.epros.server.download.wordxml.shikanyuan.SKYProcessModel;
import com.jecn.epros.server.download.wordxml.sifang.SFProcessModel;
import com.jecn.epros.server.download.wordxml.siwei.SWProcessModel;
import com.jecn.epros.server.download.wordxml.wanhua.WHProcessModel;
import com.jecn.epros.server.download.wordxml.yage.YGProcessModel;
import com.jecn.epros.server.download.wordxml.yuexiu.YXProcessModel;
import com.jecn.epros.server.download.wordxml.yutong.YTProcessModel;
import com.jecn.epros.server.download.wordxml.zhuhaicusuanxianwei.ZHCSProcessModel;
import com.jecn.epros.server.download.wordxml.zhuzhouzhongche.ZZZCProcessModel;
import com.jecn.epros.server.download.wordxml.zhuzhouzhongche.ZZZCRuleModel;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.graphic.RtfShape;
import com.lowagie.text.rtf.graphic.RtfShapePosition;
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter;
import com.lowagie.text.rtf.style.RtfFont;
import com.lowagie.text.rtf.style.RtfParagraphStyle;

public class DownloadUtil {
	private static final Logger log = Logger.getLogger(DownloadUtil.class);

	/**
	 * 生成流程操作说明
	 * 
	 * @author fuzhh 2013-9-2
	 * @param flowId
	 * @param processDownloadBean
	 * @param docControlDao
	 * @param processBasicDao
	 * @param path
	 * @return
	 */
	public static String createProcessFile(Long flowId, ProcessDownloadBean processDownloadBean,
			IJecnDocControlDao docControlDao, IProcessBasicInfoDao processBasicDao, IJecnConfigItemDao configItemDao,
			String path, boolean isPub) {
		// 临时文件存储路径
		String templetPath = "";
		String flowFileType = configItemDao.selectValue(1, ConfigItemPartMapMark.flowFileType.toString());
		if (!JecnCommon.isNullOrEmtryTrim(flowFileType)) {
			JecnContants.flowFileType = Integer.valueOf(flowFileType).intValue();
		}
		try {
			// JecnContants.otherOperaType = 6;
			switch (JecnContants.otherOperaType) {
			case 0: // 老版本版本的默认操作手册（烽火）
				templetPath = new OldGeneralProcessModel(processDownloadBean, path).write();
				break;
			case 1: // 保隆操作说明下载
				templetPath = new BLProcessModel(processDownloadBean, templetPath).write();
				break;
			case 2: // 东航操作说明
				templetPath = new DHProcessModel(processDownloadBean, path).write();
				break;
			case 6: // 南京石化下载
				templetPath = new NJSHProcessModel(processDownloadBean, templetPath).write();
				break;
			case 8: // 华帝操作说明下载
				templetPath = new HDProcessModel(processDownloadBean, templetPath).write();
				break;
			case 9:// 恒神
				templetPath = new HSProcessModel(processDownloadBean, path).write();
				break;
			case 11:// 农夫山泉
				templetPath = new NFSQProcessModel(processDownloadBean, path).write();
				break;
			case 12:// 成都29所
				templetPath = new CD29ProcessModel(processDownloadBean, path).write();
				break;
			case 13:// 广州机电
				templetPath = new GZJDProcessModel(processDownloadBean, path).write();
				break;
			case 14:// 联合电子 ???????????
				templetPath = new LHDZProcessModel(processDownloadBean, path).write();
				break;
			case 15:// 
				templetPath = new GeneralProcessModel(processDownloadBean, path).write();
				break;
			case 16:// 巴德富操作说明模版
				templetPath = new BDFProcessModel(processDownloadBean, templetPath).write();
				break;
			case 17:// 赛轮操作说明模版
				templetPath = new SLProcessModel(processDownloadBean, templetPath).write();
				break;
			case 18:// 江波龙操作说明模版
				templetPath = new JBLProcessFileMode(processDownloadBean, templetPath).write();
				break;
			case 19:// 河南思维操作说明模版
				templetPath = new SWProcessModel(processDownloadBean, templetPath).write();
				break;
			case 20:// 大唐移动
				templetPath = new DTProcessModel(processDownloadBean, path).write();
				break;
			case 21:// 株洲中车时代电气股份有限公司
				templetPath = new ZZZCProcessModel(processDownloadBean, path).write();
				break;
			case 22:// 珠海醋酸纤维有限公司
				templetPath = new ZHCSProcessModel(processDownloadBean, path).write();
				break;
			case 23:// 蒙牛
				templetPath = new MNProcessModel(processDownloadBean, path).write();
				break;
			case 24:// 立邦
				templetPath = new LBProcessModel(processDownloadBean, path).write();
				break;
			case 25:// 石勘院
				templetPath = new SKYProcessModel(processDownloadBean, path).write();
				break;
			case 26:// 海康威视
				templetPath = new HKWSProcessModel(processDownloadBean, path).write();
				break;
			case 27:// 东软医疗
				templetPath = new DRYLProcessModel(processDownloadBean, path).write();
				break;
			case 28:// 金旅客车
				templetPath = new JLKCProcessModel(processDownloadBean, path).write();
				break;
			case 29:// 雅阁（博美）
				templetPath = new YGProcessModel(processDownloadBean, path).write();
				break;
			case 30:// 四方
				templetPath = new SFProcessModel(processDownloadBean, path).write();
				break;
			case 31:// 爱普生
				templetPath = new APSProcessModel(processDownloadBean, path).write();
				break;
			case 32:// 金风
				templetPath = new JFProcessModel2(processDownloadBean, path).write();
				break;
			case 33:// 科大讯飞
				templetPath = new KDXFProcessModel(processDownloadBean, path).write();
				break;
			case 34:// 木林森
				templetPath = new MLSProcessModel(processDownloadBean, path).write();
				break;
			case 35:// 裕同
				templetPath = new YTProcessModel(processDownloadBean, path).write();
				break;
			case 36:// 时代新材
				templetPath = new SDXCProcessModel(processDownloadBean, path).write();
				break;
			case 37:// 九新药业
				templetPath = new JXYYProcessModel(processDownloadBean, path).write();
				break;
			case 38:// 顾家家居
				templetPath = new GJJJProcessModel(processDownloadBean, path).write();
				break;
			case 39:// 万华
				templetPath = new WHProcessModel(processDownloadBean, path).write();
				break;
			case 40:// 越秀
				templetPath = new YXProcessModel(processDownloadBean, path).write();
				break;
			default:
				// 新版本的默认操作手册 (烽火)
				templetPath = new GeneralProcessModel(processDownloadBean, templetPath).write();
			}
		} catch (Exception e) {
			log.error("生成操作说明异常！", e);
		}
		return templetPath;
	}

	/**
	 * 获取操作说明 头信息 1.根据流程的一级节点 和二级节点 获取 2.一级二级流程统一倒序方式查出 取第一个值
	 * 
	 * @return
	 */
	private static FlowFileHeadData getFlowFileHeadData(ProcessDownloadBean processDownloadBean,
			IProcessBasicInfoDao processBasicDao) {
		List<FlowFileHeadData> Head = null;
		if (processDownloadBean.getFlowStructureT() == null) {
			Head = processBasicDao.findFlowFileHeadInfo(processDownloadBean.getFlowStructure().getFlowId());
		} else {
			Head = processBasicDao.findFlowFileHeadInfo(processDownloadBean.getFlowStructureT().getFlowId());
		}
		if (Head == null || Head.isEmpty()) {
			return null;
		}
		return (FlowFileHeadData) Head.get(0);
	}

	/**
	 * 生成制度操作说明
	 * 
	 * @author fuzhh 2014-1-21
	 * @param ruleDownloadBean
	 * @return
	 */
	public static String createRuleFile(RuleDownloadBean ruleDownloadBean) {
		// 临时文件存储路径
		String templetPath = "";
		try {
			// JecnContants.otherOperaType = 21;
			switch (JecnContants.otherOperaType) {
			case 0:// 烽火(不知道从什么时候开始从15变成了0)
				templetPath = new FHRuleModel(ruleDownloadBean, null).write();
				break;
			case 8: // 华帝操作说明下载
				templetPath = HuadiRuleDownload.create(ruleDownloadBean);
				break;
			case 21:// 株洲中车时代电气股份有限公司
				templetPath = new ZZZCRuleModel(ruleDownloadBean, null).write();
				break;
			case 23:// 蒙牛
				templetPath = new MNRuleModel(ruleDownloadBean, null).write();
				break;
			default:
				templetPath = RuleDownload.create(ruleDownloadBean);
				templetPath = JecnFinal.getAllTempPath(templetPath);
				break;
			}

		} catch (Exception e) {
			log.error("生成操作说明异常！", e);
		}
		return templetPath;
	}

	/**
	 * 根据需求调用不同的操作说明下载
	 * 
	 * @author fuzhh May 10, 2013
	 * @param flowId
	 * @param processDownloadBean
	 * @param docControlDao
	 * @param processBasicDao
	 * @return
	 */
	public static byte[] docDownType(Long flowId, ProcessDownloadBean processDownloadBean,
			IJecnDocControlDao docControlDao, IProcessBasicInfoDao processBasicDao, IJecnConfigItemDao configItemDao,
			String path, boolean isPub) {
		// 生成bute[];
		byte[] bytes = null;
		// 临时文件存储路径

		String templetPath = createProcessFile(flowId, processDownloadBean, docControlDao, processBasicDao,
				configItemDao, path, isPub);
		try {
			bytes = JecnFinal.getByteByPath(templetPath);
		} catch (Exception e) {
			log.error("操作说明读取流程异常:id=" + flowId, e);
		}

		if (templetPath != null) {
			// 获取临时文件
			File file = new File(templetPath);
			if (file.exists()) {
				file.delete();
			}
		}

		return bytes;
	}

	/**
	 * 生成word 页眉
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @param document
	 * @param headerDocBean
	 *            页眉的数据
	 * @param headerImg
	 *            页眉左上角图片
	 */
	public static void getHeaderDoc(Document document, HeaderDocBean headerDocBean, Image headerImg) {
		// HeaderFooter添加页眉
		// 名称
		String name = headerDocBean.getName();
		// 是否公开
		String isPublic = headerDocBean.getIsPublic();
		if ("1".equals(isPublic)) {
			isPublic = JecnUtil.getValue("public");
		} else {
			isPublic = JecnUtil.getValue("secret");
		}
		// 编号
		String inputNumber = headerDocBean.getInputNum();
		// 版本
		String stringV = headerDocBean.getVersion();
		try {
			Table table = new Table(5);
			table.setWidth(100f);
			table.setWidths(new int[] { 33, 16, 17, 16, 17 });
			table.setBorderWidth(1);
			table.setBorderColor(new Color(255, 255, 255));
			Cell cell = new Cell("");
			if (headerImg != null) {
				cell = new Cell(headerImg);
				headerImg.scaleAbsolute(160, 40);
			}
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			cell.setRowspan(2);
			table.addCell(cell, new Point(0, 0));

			cell = new Cell(ST_5_BOLD_MIDDLE(JecnUtil.getValue("fileNameC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 1));
			cell = new Cell(name);
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 2));

			cell = new Cell(ST_5_BOLD_MIDDLE(JecnUtil.getValue("fileNumberC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 1));
			cell = new Cell(inputNumber);
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 2));

			cell = new Cell(ST_5_BOLD_MIDDLE(JecnUtil.getValue("intensiveC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 3));
			cell = new Cell(isPublic);
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 4));

			cell = new Cell(ST_5_BOLD_MIDDLE(JecnUtil.getValue("versionC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 3));
			cell = new Cell(stringV);
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 4));

			document.setHeader(new RtfHeaderFooter(table));
		} catch (DocumentException e) {
			log.error("", e);
		}
	}

	/**
	 * 生成word 页眉
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @param document
	 * @param headerDocBean
	 *            页眉的数据
	 * @param headerImg
	 *            页眉左上角图片
	 */
	public static void getHuadiHeaderDoc(Document document, HeaderDocBean headerDocBean, Image headerImg) {
		try {
			StringBuffer strbuf = new StringBuffer();
			// 文件编号
			String fileNumber = headerDocBean.getInputNum();
			strbuf.append(fileNumber + " ");
			// 文件版本
			String fileVersion = headerDocBean.getVersion();
			strbuf.append(fileVersion + " ");

			Phrase headerPara = new Phrase(ST_5_BOLD_RIGHT(strbuf.toString()));
			HeaderFooter header = new HeaderFooter(headerPara, false);
			header.setAlignment(HeaderFooter.ALIGN_CENTER);

			document.setHeader(header);
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 生成word首页table的信息
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @param document
	 * @param isEnglish
	 *            英文和中文标示
	 * @param recordBeanList
	 *            需要显示的list
	 * @param fileNum
	 *            文件编号
	 * @param fileVersion
	 *            文件版本
	 */
	public static void getReturnTable(Document document, List<Object[]> recordBeanList, String fileNum,
			String fileVersion) {
		try {
			String titleFileNo = "";
			titleFileNo = "\n\t\t\t" + JecnUtil.getValue("fileNumberC");
			if (fileNum != null && !fileNum.equals("") && fileNum != "null") {
				titleFileNo += fileNum;
			}
			document.add(ST_3(titleFileNo));
			String titleFileVersion = "";
			titleFileVersion = "\n\t\t\t" + JecnUtil.getValue("filePlateNumberC");
			if (fileVersion != null && !fileVersion.equals("") && fileVersion != "null") {
				titleFileVersion += fileVersion;
			}
			document.add(ST_3(titleFileVersion));
			if (recordBeanList.size() > 0) {
				Table table = new Table(4);
				table.setWidth(100f);
				table.setWidths(new int[] { 25, 25, 25, 25 });
				table.setBorderWidth(1);
				int tableCount = 0;
				for (Object[] obj : recordBeanList) {
					// 阶段名称
					String stateName = obj[0].toString();
					// 审批人
					String approvePeople = obj[1].toString();
					// 审批时间
					String approveTime = obj[2].toString();

					table.addCell(stateName, new Point(tableCount, 0));
					table.addCell(approvePeople, new Point(tableCount, 1));
					// 审批时间（标题）
					table.addCell(JecnUtil.getValue("theDateC"), new Point(tableCount, 2));
					table.addCell(approveTime, new Point(tableCount, 3));
					tableCount++;
				}
				document.add(table);
			}
		} catch (DocumentException e) {
			log.error("", e);
		}
		// 分页
		document.newPage();
	}

	public static void huaDiTwoPage(Document document, String flowName, String companyName, String version,
			String flowNum) {
		try {
			// 公司名称
			Paragraph companyPar = ST_16(companyName);
			companyPar.setAlignment(1);
			document.add(companyPar);
			// 版本号
			StringBuffer strbuf = new StringBuffer(flowNum);
			strbuf.append(" " + version);
			Paragraph strpar = ST_5_BOLD_RIGHT(strbuf.toString());
			document.add(strpar);
			// 流程名称
			Paragraph flowPar = ST_22(flowName);
			flowPar.setAlignment(Element.ALIGN_CENTER);
			document.add(flowPar);

			RtfShapePosition position = new RtfShapePosition(150, 0, 10400, 150);
			position.setXRelativePos(RtfShapePosition.POSITION_X_RELATIVE_MARGIN);
			position.setYRelativePos(RtfShapePosition.POSITION_Y_RELATIVE_PARAGRAPH);
			RtfShape shape = new RtfShape(RtfShape.SHAPE_LINE, position);
			Paragraph par = new Paragraph();
			par.add(shape);
			document.add(par);
		} catch (Exception e) {
		}
	}

	/**
	 * 生成word 页脚
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @param document
	 */
	public static void getFooterDoc(Document document) {
		HeaderFooter foot = null;
		foot = new HeaderFooter(new Phrase(JecnUtil.getValue("wordTheFooter")), false);
		foot.setAlignment(Rectangle.ALIGN_CENTER);
		document.setFooter(foot);
	}

	/**
	 * 操作说明标题
	 * 
	 * @author fuzhh Nov 7, 2012
	 * @param title
	 *            标题名称
	 * @return 编辑后的样式
	 */
	public static Paragraph getTitle(String title) {
		Paragraph titleP = new Paragraph(title, RtfParagraphStyle.STYLE_HEADING_1);
		RtfParagraphStyle.STYLE_HEADING_1.setAlignment(Element.ALIGN_CENTER);
		return titleP;
	}

	/**
	 * 操作说明下载 段落标题
	 * 
	 * @author fuzhh Nov 6, 2012
	 * @param title
	 *            标题名称
	 * @return 编辑好的标题样式
	 */
	public static Paragraph getParagraphTitle(String title) {
		Paragraph paragraphTitle = new Paragraph(title, RtfParagraphStyle.STYLE_HEADING_3);
		return paragraphTitle;
	}

	/**
	 * 操作说明下载 内容显示
	 * 
	 * @author fuzhh Nov 6, 2012
	 * @param content
	 *            显示的内容
	 * @return 编辑好的内容样式
	 */
	public static Paragraph getContent(String content) {
		Paragraph paragraphContent = new Paragraph(changeNetToWordStyle(content), new RtfFont("Arial", 12));
		paragraphContent.setSpacingBefore(5f);
		paragraphContent.setSpacingAfter(5f);
		return paragraphContent;
	}

	/**
	 * 操作说明下载 换行转换,和空字符串转换
	 * 
	 * @author fuzhh Nov 6, 2012
	 * @param str
	 *            需要转换的数据
	 * @return 转换好的数据
	 */
	public static String changeNetToWordStyle(String str) {
		if (str != null && !"".equals(str)) {
			str = str.replaceAll("<br>", "\n");
			str = str.replaceAll("&nbsp;", "");
		}
		return str;
	}

	/**
	 * 换行转换
	 * 
	 * @author fuzhh Nov 7, 2012
	 * @param title
	 *            需要转换的数据
	 * @return 转换好的数据
	 */
	public static String repaceAllInfo(String title) {
		if (title != null && !title.equals("")) {
			title = title.replaceAll("<br>", "\n");
		}
		return title;
	}

	/**
	 * 生成表格
	 * 
	 * @author fuzhh Nov 6, 2012
	 * @param tableWidth
	 *            表格的宽度
	 * @param tableInt
	 *            表格各个宽度的数组
	 * @param tableTopNameList
	 *            表格名称集合
	 * @param tableContentList
	 *            表格内容集合
	 * @return
	 */
	public static Table getTable(float tableWidth, int[] tableInt, List<String> tableTitleNameList,
			List<Object[]> tableContentList) {
		Table table = null;
		try {
			table = new Table(tableInt.length);
			// 设置各个表格宽度
			table.setWidths(tableInt);
			// 设置居左
			table.setAlignment(Table.ALIGN_LEFT);
			// 设置宽度
			table.setWidth(tableWidth);
			for (int i = 0; i < tableTitleNameList.size(); i++) {
				String tableTop = tableTitleNameList.get(i);
				Cell tableTopCell = new Cell(ST_5_BOLD_MIDDLE(tableTop));
				tableTopCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(tableTopCell, new Point(0, i));
			}
			int tableCount = 1;
			for (Object[] obj : tableContentList) {
				for (int i = 0; i < tableInt.length; i++) {
					Object object = obj[i];
					if (object == null) {
						object = "";
					}
					table.addCell(object.toString(), new Point(tableCount, i));
				}
				tableCount++;
			}
		} catch (BadElementException e) {
			log.error("", e);
		} catch (DocumentException e) {
			log.error("", e);
		}
		return table;
	}

	/**
	 * 获取流程图操作说明和流程地图操作说明
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param flowId
	 *            流程ID
	 * @param treeNodeType
	 *            TreeNodeType.process为流程
	 * @param isPub
	 *            true为临时表 ，false为正式表
	 * @param processOperationConfigDao
	 * @param docControlDao
	 * @param flowStructureDao
	 * @param processBasicDao
	 * @param processKPIDao
	 * @param processRecordDao
	 * @return
	 * @throws Exception
	 */
	public static JecnCreateDoc getFlowFile(Long flowId, TreeNodeType treeNodeType, boolean isPub,
			IJecnConfigItemDao configItemDao, IJecnDocControlDao docControlDao, IFlowStructureDao flowStructureDao,
			IProcessBasicInfoDao processBasicDao, IProcessKPIDao processKPIDao, IProcessRecordDao processRecordDao)
			throws Exception {
		/***** 流程图操作说明下载 *******/
		if (treeNodeType == TreeNodeType.process) {
			// 获取流程操作说明信息
			ProcessDownloadBean processDownloadBean = getProcessDownloadBean(flowId, isPub, configItemDao,
					docControlDao, flowStructureDao, processBasicDao, processKPIDao, processRecordDao);
			// 生成bute[];
			byte[] bytes = docDownType(flowId, processDownloadBean, docControlDao, processBasicDao, configItemDao,
					null, isPub);
			if (bytes == null) {
				return null;
			}
			JecnCreateDoc jecnCreateDoc = new JecnCreateDoc();
			jecnCreateDoc.setBytes(bytes);
			jecnCreateDoc.setFileName(processDownloadBean.getFlowName());
			jecnCreateDoc.setIdInput(processDownloadBean.getFlowInputNum());
			return jecnCreateDoc;
		}
		/** ****** 流程地图操作说明下载 ************* */
		else if (treeNodeType == TreeNodeType.processMap) {
			ProcessMapDownloadBean processMapDownloadBean = getProcessMapDownloadBean(flowId, isPub, configItemDao,
					docControlDao, flowStructureDao);
			String path = ProcessMapDownload.create(processMapDownloadBean);
			// 生成bute[];
			byte[] bytes = JecnFinal.getByteByPath(path);
			File file = new File(path);
			if (file.exists()) {
				file.delete();
			}
			if (bytes == null) {
				return null;
			}
			JecnCreateDoc jecnCreateDoc = new JecnCreateDoc();
			jecnCreateDoc.setBytes(bytes);
			jecnCreateDoc.setFileName(processMapDownloadBean.getFlowName());
			jecnCreateDoc.setIdInput(processMapDownloadBean.getFlowInputNum());
			return jecnCreateDoc;
		}
		return null;
	}

	/**
	 * 获取流程操作说明信息
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @param flowId
	 *            流程ID
	 * @param isPub
	 *            true为临时表 ，false为正式表
	 * @param configItemDao
	 * @param docControlDao
	 * @param flowStructureDao
	 * @param processBasicDao
	 * @param processKPIDao
	 * @param processRecordDao
	 * @return
	 * @throws Exception
	 */
	public static ProcessDownloadBean getProcessDownloadBean(Long flowId, boolean isPub,
			IJecnConfigItemDao configItemDao, IJecnDocControlDao docControlDao, IFlowStructureDao flowStructureDao,
			IProcessBasicInfoDao processBasicDao, IProcessKPIDao processKPIDao, IProcessRecordDao processRecordDao)
			throws Exception {
		ProcessDownloadBean processDownloadBean = new ProcessDownloadBean();
		processDownloadBean.setBaseDao(configItemDao);
		processDownloadBean.setPub(isPub);
		// 操作说明配置
		List<JecnConfigItemBean> configItemBeanList = configItemDao.selectShowFlowFile();
		// 流程驱动规则时间驱动配置
		String flowDrivenRuleTimeConfig = configItemDao.selectValue(6, ConfigItemPartMapMark.isShowDateFlowFile
				.toString());
		// KPI配置BEAN排序
		List<JecnConfigItemBean> configKpiItemBean = configItemDao.selectTableConfigItemBeanByType(1, 16);
		// 获取公司名称
		String companyName = configItemDao.selectValue(JecnConfigContents.TYPE_BIG_ITEM_PUB,
				ConfigItemPartMapMark.basicCmp.toString());
		// 获取流程文件
		String flowFileType = configItemDao.selectValue(1, ConfigItemPartMapMark.flowFileType.toString());
		if (!JecnCommon.isNullOrEmtryTrim(flowFileType)) {
			processDownloadBean.setFlowFileType(Integer.valueOf(flowFileType));
		}
		processDownloadBean.setFlowDrivenRuleTimeConfig(flowDrivenRuleTimeConfig);
		processDownloadBean.setCompanyName(companyName);
		processDownloadBean.setJecnConfigItemBean(configItemBeanList);
		processDownloadBean.setConfigKpiItemBean(configKpiItemBean);
		// 保密级别
		int confidentialityLevel = 0;
		Long pId = null;
		Long flowTypeId = null;
		Long expiry = null;
		if (!isPub) {
			JecnFlowStructureT jecnFlowStructureT = flowStructureDao.get(flowId);
			JecnFlowBasicInfoT jecnFlowBasicInfoT = processBasicDao.get(flowId);
			processBasicDao.getSession().evict(jecnFlowBasicInfoT);
			if (jecnFlowStructureT == null) {
				throw new IllegalArgumentException("临时表该流程不存在，流程id:" + flowId);
			}
			flowTypeId = jecnFlowBasicInfoT.getTypeId();
			processDownloadBean.setFlowStructureT(jecnFlowStructureT);
			pId = jecnFlowStructureT.getPerFlowId();
			expiry = jecnFlowBasicInfoT.getExpiry();
			// 流程创建人
			if (jecnFlowStructureT.getPeopleId() != null) {
				// 获取用户名称
				String userName = processBasicDao.getUserName(jecnFlowStructureT.getPeopleId());
				processDownloadBean.setCreateName(userName);
			}

			// 起始活动 终止活动
			Object[] startEndActive = new Object[] { jecnFlowBasicInfoT.getFlowStartpoint(),
					jecnFlowBasicInfoT.getFlowEndpoint() };
			// 流程责任属性
			if (jecnFlowBasicInfoT.getTypeResPeople() != null) {
				processDownloadBean.setDutyUserType(jecnFlowBasicInfoT.getTypeResPeople().longValue());
			}
			// 责任人ID
			processDownloadBean.setDutyUserId(jecnFlowBasicInfoT.getResPeopleId());
			// 监护人ID
			processDownloadBean.setGuardianId(jecnFlowBasicInfoT.getGuardianId());
			// 拟制人ID
			processDownloadBean.setFictionPeopleId(jecnFlowBasicInfoT.getFictionPeopleId());

			processDownloadBean.setStartEndActive(startEndActive);
			// 图片文件路径
			processDownloadBean.setImgPath(jecnFlowStructureT.getFilePath());
			// 流程名称
			processDownloadBean.setFlowName(jecnFlowStructureT.getFlowName());
			// 密级
			if (jecnFlowStructureT.getIsPublic() != null) {
				processDownloadBean.setFlowIsPublic(jecnFlowStructureT.getIsPublic().toString());
			} else {
				processDownloadBean.setFlowIsPublic("0");
			}
			// 保密级别
			if (jecnFlowStructureT.getConfidentialityLevel() != null) {
				confidentialityLevel = jecnFlowStructureT.getConfidentialityLevel().intValue();
			}
			// 更新日期
			if (jecnFlowStructureT.getUpdateDate() != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String dateString = formatter.format(jecnFlowStructureT.getUpdateDate());
				processDownloadBean.setUpdateDate(dateString);
			}

			// 流程编号
			if (jecnFlowStructureT.getFlowIdInput() != null) {
				processDownloadBean.setFlowInputNum(jecnFlowStructureT.getFlowIdInput());
			} else {
				processDownloadBean.setFlowInputNum("");
			}
			// 流程客户
			if (jecnFlowBasicInfoT.getFlowCustom() != null) {
				processDownloadBean.setFlowCustom(jecnFlowBasicInfoT.getFlowCustom());
			} else {
				processDownloadBean.setFlowCustom("");
			}
			// 审核人 数据
			List<Object[]> peopleList = new ArrayList<Object[]>();
			String version = ProcessDownDataUtil.getJecnTaskHistoryNew(peopleList, jecnFlowStructureT.getHistoryId(),
					docControlDao);
			// 版本
			if (version != null) {
				processDownloadBean.setFlowVersion(version);
			} else {
				processDownloadBean.setFlowVersion("");
			}
			processDownloadBean.setPeopleList(peopleList);
			// 目的
			if (jecnFlowBasicInfoT.getFlowPurpose() != null) {
				processDownloadBean.setFlowPurpose(jecnFlowBasicInfoT.getFlowPurpose());
			} else {
				processDownloadBean.setFlowPurpose("");
			}
			// 适用范围
			if (jecnFlowBasicInfoT.getApplicability() != null) {
				processDownloadBean.setApplicability(jecnFlowBasicInfoT.getApplicability());
			} else {
				processDownloadBean.setApplicability("");
			}
			// 术语定义
			if (jecnFlowBasicInfoT.getNoutGlossary() != null) {
				processDownloadBean.setNoutGlossary(jecnFlowBasicInfoT.getNoutGlossary());
			} else {
				processDownloadBean.setNoutGlossary("");
			}
			// 流程输入
			if (jecnFlowBasicInfoT.getInput() != null) {
				processDownloadBean.setFlowInput(jecnFlowBasicInfoT.getInput());
			} else {
				processDownloadBean.setFlowInput("");
			}
			// 流程输出
			if (jecnFlowBasicInfoT.getOuput() != null) {
				processDownloadBean.setFlowOutput(jecnFlowBasicInfoT.getOuput());
			} else {
				processDownloadBean.setFlowOutput("");
			}
			// 不适用范围
			if (jecnFlowBasicInfoT.getNoApplicability() != null) {
				processDownloadBean.setNoApplicability(jecnFlowBasicInfoT.getNoApplicability());
			} else {
				processDownloadBean.setNoApplicability("");
			}
			// 补充说明
			if (jecnFlowBasicInfoT.getFlowSupplement() != null) {
				processDownloadBean.setFlowSupplement(jecnFlowBasicInfoT.getFlowSupplement());
			} else {
				processDownloadBean.setFlowSupplement("");
			}
			// 概况信息
			if (jecnFlowBasicInfoT.getFlowSummarize() != null) {
				processDownloadBean.setFlowSummarize(jecnFlowBasicInfoT.getFlowSummarize());
			} else {
				processDownloadBean.setFlowSummarize("");
			}

			// 驱动规则的类型
			if (jecnFlowBasicInfoT.getDriveType() != null) {
				FlowDriverBean flowDriverBean = ProcessDownDataUtil.getFlowDriverBean(flowId, jecnFlowBasicInfoT, null,
						flowStructureDao);
				processDownloadBean.setFlowDriver(flowDriverBean);
			}

			/** ** v3.05新添加的要素*** */
			// a26,相关文件
			processDownloadBean.setFlowRelatedFile(jecnFlowBasicInfoT.getFlowRelatedFile());
			// a27,自定义要素1
			processDownloadBean.setFlowCustomOne(jecnFlowBasicInfoT.getFlowCustomOne());
			// a28,自定义要素2
			processDownloadBean.setFlowCustomTwo(jecnFlowBasicInfoT.getFlowCustomTwo());
			// a29,自定义要素3
			processDownloadBean.setFlowCustomThree(jecnFlowBasicInfoT.getFlowCustomThree());
			// a30,自定义要素4
			processDownloadBean.setFlowCustomFour(jecnFlowBasicInfoT.getFlowCustomFour());
			// a31,自定义要素5
			processDownloadBean.setFlowCustomFive(jecnFlowBasicInfoT.getFlowCustomFive());
			/** ** v3.05新添加的要素*** */
		} else {
			JecnFlowStructure jecnFlowStructure = (JecnFlowStructure) flowStructureDao.getSession().get(
					JecnFlowStructure.class, flowId);
			JecnFlowBasicInfo jecnFlowBasicInfo = (JecnFlowBasicInfo) processBasicDao.getSession().get(
					JecnFlowBasicInfo.class, flowId);
			processBasicDao.getSession().evict(jecnFlowBasicInfo);
			if (jecnFlowStructure == null) {
				throw new IllegalArgumentException("正式该流程不存在，流程id:" + flowId);
			}
			flowTypeId = jecnFlowBasicInfo.getTypeId();
			processDownloadBean.setFlowStructure(jecnFlowStructure);
			pId = jecnFlowStructure.getPerFlowId();
			expiry = jecnFlowBasicInfo.getExpiry();
			// 流程创建人
			if (jecnFlowStructure.getPeopleId() != null) {
				// 获取用户名称
				String userName = processBasicDao.getUserName(jecnFlowStructure.getPeopleId());
				processDownloadBean.setCreateName(userName);
			}

			// 起始活动 终止活动
			Object[] startEndActive = new Object[] { jecnFlowBasicInfo.getFlowStartpoint(),
					jecnFlowBasicInfo.getFlowEndpoint() };
			// 流程责任属性
			if (jecnFlowBasicInfo.getTypeResPeople() != null) {
				processDownloadBean.setDutyUserType(jecnFlowBasicInfo.getTypeResPeople().longValue());
			}
			// 责任人ID
			processDownloadBean.setDutyUserId(jecnFlowBasicInfo.getResPeopleId());
			// 监护人ID
			processDownloadBean.setGuardianId(jecnFlowBasicInfo.getGuardianId());
			// 拟制人ID
			processDownloadBean.setFictionPeopleId(jecnFlowBasicInfo.getFictionPeopleId());

			processDownloadBean.setStartEndActive(startEndActive);
			// 图片文件路径
			processDownloadBean.setImgPath(jecnFlowStructure.getFilePath());
			// 流程名称
			processDownloadBean.setFlowName(jecnFlowStructure.getFlowName());
			// 密级
			if (jecnFlowStructure.getIsPublic() != null) {
				processDownloadBean.setFlowIsPublic(jecnFlowStructure.getIsPublic().toString());
			} else {
				processDownloadBean.setFlowIsPublic("0");
			}
			// 保密级别
			if (jecnFlowStructure.getConfidentialityLevel() != null) {
				confidentialityLevel = jecnFlowStructure.getConfidentialityLevel().intValue();
			}
			// 更新日期
			if (jecnFlowStructure.getUpdateDate() != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String dateString = formatter.format(jecnFlowStructure.getUpdateDate());
				processDownloadBean.setUpdateDate(dateString);
			}
			// 流程编号
			if (jecnFlowStructure.getFlowIdInput() != null) {
				processDownloadBean.setFlowInputNum(jecnFlowStructure.getFlowIdInput());
			} else {
				processDownloadBean.setFlowInputNum("");
			}
			// 流程客户
			if (jecnFlowBasicInfo.getFlowCustom() != null) {
				processDownloadBean.setFlowCustom(jecnFlowBasicInfo.getFlowCustom());
			} else {
				processDownloadBean.setFlowCustom("");
			}
			// 审核人 数据
			List<Object[]> peopleList = new ArrayList<Object[]>();
			String version = ProcessDownDataUtil.getJecnTaskHistoryNew(peopleList, jecnFlowStructure.getHistoryId(),
					docControlDao);
			// 版本
			if (version != null) {
				processDownloadBean.setFlowVersion(version);
			} else {
				processDownloadBean.setFlowVersion("");
			}
			processDownloadBean.setPeopleList(peopleList);

			// 目的
			if (jecnFlowBasicInfo.getFlowPurpose() != null) {
				processDownloadBean.setFlowPurpose(jecnFlowBasicInfo.getFlowPurpose());
			} else {
				processDownloadBean.setFlowPurpose("");
			}
			// 适用范围
			if (jecnFlowBasicInfo.getApplicability() != null) {
				processDownloadBean.setApplicability(jecnFlowBasicInfo.getApplicability());
			} else {
				processDownloadBean.setApplicability("");
			}
			// 术语定义
			if (jecnFlowBasicInfo.getNoutGlossary() != null) {
				processDownloadBean.setNoutGlossary(jecnFlowBasicInfo.getNoutGlossary());
			} else {
				processDownloadBean.setNoutGlossary("");
			}
			// 流程输入
			if (jecnFlowBasicInfo.getInput() != null) {
				processDownloadBean.setFlowInput(jecnFlowBasicInfo.getInput());
			} else {
				processDownloadBean.setFlowInput("");
			}
			// 流程输出
			if (jecnFlowBasicInfo.getOuput() != null) {
				processDownloadBean.setFlowOutput(jecnFlowBasicInfo.getOuput());
			} else {
				processDownloadBean.setFlowOutput("");
			}
			// 不适用范围
			if (jecnFlowBasicInfo.getNoApplicability() != null) {
				processDownloadBean.setNoApplicability(jecnFlowBasicInfo.getNoApplicability());
			} else {
				processDownloadBean.setNoApplicability("");
			}
			// 补充说明
			if (jecnFlowBasicInfo.getFlowSupplement() != null) {
				processDownloadBean.setFlowSupplement(jecnFlowBasicInfo.getFlowSupplement());
			} else {
				processDownloadBean.setFlowSupplement("");
			}
			// 概况信息
			if (jecnFlowBasicInfo.getFlowSummarize() != null) {
				processDownloadBean.setFlowSummarize(jecnFlowBasicInfo.getFlowSummarize());
			} else {
				processDownloadBean.setFlowSummarize("");
			}
			// 驱动规则的类型
			if (jecnFlowBasicInfo.getDriveType() != null) {
				FlowDriverBean flowDriverBean = ProcessDownDataUtil.getFlowDriverBean(flowId, null, jecnFlowBasicInfo,
						flowStructureDao);
				processDownloadBean.setFlowDriver(flowDriverBean);
			}

			/** ** v3.05新添加的要素*** */
			// a26,相关文件
			processDownloadBean.setFlowRelatedFile(jecnFlowBasicInfo.getFlowRelatedFile());
			// a27,自定义要素1
			processDownloadBean.setFlowCustomOne(jecnFlowBasicInfo.getFlowCustomOne());
			// a28,自定义要素2
			processDownloadBean.setFlowCustomTwo(jecnFlowBasicInfo.getFlowCustomTwo());
			// a29,自定义要素3
			processDownloadBean.setFlowCustomThree(jecnFlowBasicInfo.getFlowCustomThree());
			// a30,自定义要素4
			processDownloadBean.setFlowCustomFour(jecnFlowBasicInfo.getFlowCustomFour());
			// a31,自定义要素5
			processDownloadBean.setFlowCustomFive(jecnFlowBasicInfo.getFlowCustomFive());
			/** ** v3.05新添加的要素*** */
		}

		// 保密级别
		processDownloadBean.setConfidentialityLevelName(JecnContants.getValueByCode(String
				.valueOf(confidentialityLevel), DictionaryEnum.JECN_SECURITY_LEVEL));

		List<Object[]> listActiveShow = null;
		/** ******************** 南京石化多两数据 *********************8 */
		if (JecnContants.otherOperaType == 6) {
			// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6对应内控矩阵风险点 7对应标准条款
			listActiveShow = flowStructureDao.getActiveByFlowId(flowId, isPub);
		} else {
			// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6 活动输入 7 活动输出 8链接id 9链接类型
			// 10办理时限目标值 11办理时限原始值
			listActiveShow = flowStructureDao.getActiveShowByFlowId(flowId, isPub);
		}
		// 活动排序
		getActivitySort(listActiveShow);
		/** ****************************************************** */
		List<Object[]> listRefIndicators = null;
		/** ****************** 长春轨道数据(加入指标) *********************** */
		if (JecnContants.otherOperaType == 7) {
			// 0主键ID ， 1指标名 ， 2指标值 , 3活动ID
			listRefIndicators = flowStructureDao.getRefIndicators(flowId, isPub);
		}
		/** ****************************************************** */
		// 角色明细数据 0是角色主键ID 1角色名称 2角色职责
		List<Object[]> roles = null;
		if (JecnContants.otherOperaType == 16) {// 巴德富
			roles = flowStructureDao.getBadfRoleShowByFlowId(flowId, isPub);
		} else {
			roles = flowStructureDao.getRoleShowByFlowId(flowId, isPub);
		}

		// 流程下所有的角色和活动关系表 0是角色对应的活动主键ID 1是对应的许多同一行的活动主键ID
		List<Object[]> roleRelateActive = flowStructureDao.findAllRoleAndActiveRelateByFlowId(flowId, isPub);
		// 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号 7 活动文本输入
		List<Object[]> listInputFiles = null;
		if (JecnContants.otherOperaType == 11) {// 农夫山泉 多添加一列：责任部门
			listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowIdNFSQ(flowId, isPub);
		} else {
			listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowId(flowId, isPub);
		}

		// 流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号 5活动文本输出
		List<Object[]> listOutFiles = flowStructureDao.findAllActiveOutputByFlowId(flowId, isPub);

		/** **************** 南京石化,长春轨道 活动说明数据 ****************** */
		if (JecnContants.otherOperaType == 6) { // 南京石化
			// 活动明细数据
			processDownloadBean.setAllActivityShowList(ProcessDownDataUtil.njshProcessDownloadBean(listActiveShow,
					roles, roleRelateActive));
		} else if (JecnContants.otherOperaType == 11) {// 农夫山泉
			// 活动明细数据 农夫山泉
			processDownloadBean.setAllActivityShowList(ProcessDownDataUtil.nongfushanquan_getActiveShow(listActiveShow,
					roles, roleRelateActive, listInputFiles, listOutFiles, listRefIndicators));
		} else {
			// 活动明细数据
			processDownloadBean.setAllActivityShowList(ProcessDownDataUtil.getActiveShow(listActiveShow, roles,
					roleRelateActive, listInputFiles, listOutFiles, listRefIndicators));
		}

		// 如果启用的新版的输入输出
		alterInout(processDownloadBean);

		/** ****************************************************** */
		if (JecnContants.otherOperaType == 11) { // 农夫山泉关键活动
			processDownloadBean.setKeyActivityShowList(ProcessDownDataUtil.nongfushanquan_getListKeyActivityBean(
					listActiveShow, roleRelateActive, roles));
		} else {
			// 关键活动
			processDownloadBean.setKeyActivityShowList(ProcessDownDataUtil.getListKeyActivityBean(listActiveShow));
		}
		// 流程下所有的角色和岗位关系表 0是角色所在的活动的ID 1是岗位主键ID 2是岗位名称 3部门id
		List<Object[]> roleRelatePos = flowStructureDao.finaAllRoleAndPosRelates(flowId, isPub);
		// 流程下所有的角色和岗位组关系表 0是角色所在的活动的ID 1是岗位主键ID 2是岗位名称
		List<Object[]> roleRelatePosGroupPos = flowStructureDao.finaAllRoleAndPosGroupPosRelates(flowId, isPub);
		// 流程下所有的角色和岗位组关系表 0是角色所在的活动的ID 1是岗位组主键ID 2是岗位组名称
		List<Object[]> roleRelatePosGroup = flowStructureDao.finaAllRoleAndPosGroupRelates(flowId, isPub);
		processDownloadBean.setRoleList(ProcessDownDataUtil.getRoleShow(roles, roleRelatePos, roleRelatePosGroupPos));
		// 农夫山泉
		if (JecnContants.otherOperaType == 11) {
			// 流程关键评测指标数据 0 名称 1统计方法 2 统计频率 3 数据提供者
			processDownloadBean.setFlowKpiList(processKPIDao.nongfushanquan_getKpiNameList(flowId, isPub));
		} else {
			// 流程关键评测指标数据 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6
			// KIP值单位名称 7 数据获取方式 8 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12
			// 12 目的 13 测量点 14 统计周期 15 说明 16 IT系统Id集合
			List<Object[]> oldKpi = processKPIDao.getKpiNameList(flowId, isPub);
			for (Object[] obj : oldKpi) {
				String itSys = "";
				long kpiAndId = Long.valueOf(obj[17].toString());
				List<Object[]> iTSystem = processKPIDao.getKPIITSysTem(flowId, kpiAndId, isPub);
				for (int i = 0; i < iTSystem.size(); i++) {
					itSys += String.valueOf(iTSystem.get(i)[0]) + "/";
					if (i == iTSystem.size() - 1) {
						itSys = itSys.substring(0, itSys.length() - 1);
					}
				}
				obj[16] = itSys;
			}
			processDownloadBean.setFlowKpiList(oldKpi);
		}
		// 相关流程findAllRelateFlowsNFSQ
		List<Object[]> listObjectsRelateFlows = null;
		if (JecnContants.otherOperaType == 11) { // 农夫山泉
			listObjectsRelateFlows = flowStructureDao.findAllRelateFlowsNFSQ(flowId, isPub);
			processDownloadBean.setRelatedProcessList(ProcessDownDataUtil
					.getRelatedProcessBeanListNFSQ(listObjectsRelateFlows));
		} else {
			listObjectsRelateFlows = flowStructureDao.findAllRelateFlows(flowId, isPub);
			processDownloadBean.setRelatedProcessList(ProcessDownDataUtil
					.getRelatedProcessBeanList(listObjectsRelateFlows));
		}

		// 记录保存数据 0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式
		processDownloadBean.setFlowRecordList(processRecordDao.getFlowRecordListByFlowId(flowId, isPub));
		// 流程记录 0 文件编号 1 文件名称 输入和输出集合
		List<Object[]> processRecordList = new ArrayList<Object[]>();
		// 操作规范 0 文件编号 1 文件名称
		List<Object[]> operationTemplateList = new ArrayList<Object[]>();

		if (JecnContants.otherOperaType == 11) { // 农夫山泉
			ProcessDownDataUtil.nongfushanquan_getFlowFiles(processRecordList, operationTemplateList, listInputFiles,
					listOutFiles, processDownloadBean);
		} else if (JecnContants.otherOperaType == 13) { // 广机 按照活动排序来排序相关文件
			ProcessDownDataUtil.guangji_getFlowFiles(processRecordList, operationTemplateList, listInputFiles,
					listOutFiles, listActiveShow);
		} else {
			ProcessDownDataUtil.getFlowFiles(processRecordList, operationTemplateList, listInputFiles, listOutFiles);
		}
		processDownloadBean.setProcessRecordList(processRecordList);
		// FIXME 农夫山泉获取操作规范文件数据（临时）
		processDownloadBean.setOperationTemplateList(operationTemplateList);
		// 相关制度名称
		if (JecnContants.otherOperaType == 11) {// 农夫山泉
			processDownloadBean.setRuleNameList(processBasicDao.getFlowRelateRulesNFSQ(flowId, isPub));
		} else {
			processDownloadBean.setRuleNameList(ProcessDownDataUtil.getRelateRules(processBasicDao.getFlowRelateRules(
					flowId, isPub)));
		}

		// 相关标准
		if (JecnContants.otherOperaType == 11) {// 农夫山泉
			processDownloadBean.setStandardList(flowStructureDao.getFlowStandardsNFSQ(flowId, isPub));
		} else {
			List<Object[]> standardList = flowStructureDao.getFlowStandards(flowId, isPub);
			List<Object[]> resultObj = getStandardParentNames(flowStructureDao, standardList);
			processDownloadBean.setStandardList(ProcessDownDataUtil.getRelateStandards(resultObj));
		}

		// 流程责任部门
		Object[] flowOrg = processBasicDao.getFlowRelateOrgIds(flowId, isPub);
		if (flowOrg != null && flowOrg[0] != null && flowOrg[1] != null) {
			processDownloadBean.setOrgId(Long.valueOf(flowOrg[0].toString()));
			processDownloadBean.setOrgName(flowOrg[1].toString());
		}

		// 相关风险
		processDownloadBean.setRilsList(flowStructureDao.getRiskByFlowId(flowId, isPub));
		// 设置新的的评审人数据集合 仅仅适用用广州机电 添加把巴德富支持
		if (JecnContants.otherOperaType == 13 || JecnContants.otherOperaType == 16) { //
			// 获取主项目Id
			String hql = "select curProjectId from JecnCurProject";
			List<Long> list = processRecordDao.listHql(hql);
			Long projectId = null;
			if (list.size() > 0) {
				projectId = list.get(0);
			}
			// 获取项目ID
			processDownloadBean.setPeopleListNew(docControlDao.getTaskCheckStageInfo(flowId, projectId));
		}

		// 江波龙 株洲中车 珠海醋酸
		if (JecnContants.otherOperaType == 18 || JecnContants.otherOperaType == 21 || JecnContants.otherOperaType == 22) {
			// 部门查阅权限
			List<JecnFlowOrg> orgs = processBasicDao.getFlowOrgPerm(flowId, isPub);
			processDownloadBean.setPermOrgs(orgs);
		}
		// 获取IT系统
		processDownloadBean.setActiveItSystems(getItSystems(flowStructureDao, flowId, isPub, listActiveShow));

		// 发布后查询最新的文控信息
		JecnTaskHistoryNew latestHistoryNew = docControlDao.getLatestHistory(flowId);
		processDownloadBean.setLatestHistoryNew(latestHistoryNew);

		// 所有的文控记录
		List<JecnTaskHistoryNew> historys = docControlDao.getJecnTaskHistoryNewList(flowId, 0);
		processDownloadBean.setHistorys(historys);
		if (JecnContants.otherOperaType == 23 && historys != null && historys.size() > 0) {// 蒙牛
			getHistoryNewApproveOrgName(docControlDao, historys.get(0));
		}

		// 定义（关联定义库）
		processDownloadBean.setDefinitions(flowStructureDao.getDefinitionsByFlowId(flowId, isPub));
		// 流程相关文件
		processDownloadBean.setRelatedFiles(getRelatedFiles(flowId, isPub, flowStructureDao));
		// 接口描述信息
		processDownloadBean.setDescription(getProcessInterfacesDescriptionBean(flowId, pId, isPub, flowStructureDao));
		// 上级
		processDownloadBean.setParents(getParents(flowId, isPub, flowStructureDao));
		// 流程相关标准化文件
		processDownloadBean.setStandardFileList(processBasicDao.getFlowRelateStandardFile(flowId, isPub));

		processDownloadBean.setApplicability(getApplicability(flowStructureDao, flowId, isPub, processDownloadBean
				.getApplicability()));

		processDownloadBean.setApplyOrgs(flowStructureDao.getApplyOrgs(flowId, isPub));

		List<Object[]> activiteRisks = flowStructureDao.getActivityRisks(flowId, isPub);
		List<Object[]> activiteIts = flowStructureDao.getActivityIts(flowId, isPub);
		List<JecnFlowSustainTool> its = flowStructureDao.getFlowIts(flowId, isPub);

		RoleInfo info = new RoleInfo(configItemDao, roles, roleRelatePos, roleRelatePosGroup, roleRelateActive);
		processDownloadBean.setRoleInfo(info);
		// 相关风险
		ActiveInfo activeInfo = new ActiveInfo(processBasicDao, processDownloadBean.getRilsList(), activiteRisks, its,
				activiteIts);
		processDownloadBean.setActiveInfo(activeInfo);

		ProcessDownDataUtil.processDownloadBean(flowId, processDownloadBean, docControlDao, processBasicDao);
		ProcessAttributeBean responFlow = processDownloadBean.getResponFlow();
		responFlow.setProcessTypeId(flowTypeId);
		responFlow.setProcessTypeName(getFlowTypeById(flowStructureDao, flowTypeId));
		if (expiry != null) {
			responFlow.setExpiry(expiry.toString());
		}
		processDownloadBean.setInData(flowStructureDao.getFlowInoutsByType(0, flowId, isPub));
		processDownloadBean.setOutData(flowStructureDao.getFlowInoutsByType(1, flowId, isPub));
		if (processDownloadBean.getFlowDriver() != null) {
			processDownloadBean.getFlowDriver().setTimes(getFlowDriverTimes(flowStructureDao, flowId, isPub));
		}

		// 设置查阅权限
		initPerms(processDownloadBean, flowStructureDao, flowId, isPub);
		// 设置操作说明头信息
		FlowFileHeadData flowFileHeadData = getFlowFileHeadData(processDownloadBean, processBasicDao);
		processDownloadBean.setFlowFileHeadData(flowFileHeadData);
		processDownloadBean.setConfig(getDocConfig());

		return processDownloadBean;
	}

	private static DocConfig getDocConfig() {
		DocConfig config = new DocConfig();
		// 顾家家居
		config.setActivityContentHasKeyContent(JecnContants.otherOperaType == 38);
		return config;
	}

	private static void initPerms(ProcessDownloadBean processDownloadBean, IFlowStructureDao flowStructureDao,
			Long flowId, boolean isPub) throws Exception {
		processDownloadBean.getPerms().setOrgList(flowStructureDao.getOrgAccessPermissions(flowId, 0, isPub));
	}

	private static List<? extends JecnFlowDriverTimeBase> getFlowDriverTimes(IBaseDao baseDao, Long processId,
			boolean isPub) {
		String tableName = isPub ? "JecnFlowDriverTime" : "JecnFlowDriverTimeT";
		String hql = "from " + tableName + " where flowId=?";
		return baseDao.listHql(hql, processId);
	}

	private static void alterInout(ProcessDownloadBean processDownloadBean) {
		List<Object[]> allActivityShowList = processDownloadBean.getAllActivityShowList();
		if (!(JecnConfigTool.useNewInout() && !JecnUtil.isEmpty(allActivityShowList))) {
			return;
		}
		if (JecnContants.otherOperaType == 6 || JecnContants.otherOperaType == 11) {
			return;
		}
		IBaseDao baseDao = processDownloadBean.getBaseDao();
		// 获得活动id
		boolean pub = processDownloadBean.isPub();
		String pubT = pub ? "" : "_T";
		String sql = "select jfio.figure_id,jfio.type,jfio.name,jfio.explain from jecn_figure_in_out" + pubT + " jfio"
				+ "  inner join jecn_flow_structure_image" + pubT
				+ " jfsi on jfio.figure_id=jfsi.figure_id and jfsi.flow_id=? order by jfio.sort_id asc";
		List<Object[]> objs = baseDao.listNativeSql(sql, processDownloadBean.getFlowId());
		Map<String, List<Object[]>> in = new HashMap<String, List<Object[]>>();
		Map<String, List<Object[]>> out = new HashMap<String, List<Object[]>>();
		for (Object[] obj : objs) {
			String id = obj[0].toString();
			if ("0".equals(JecnUtil.nullToEmpty(obj[1]))) {
				List<Object[]> list = in.get(id);
				if (list == null) {
					list = new ArrayList<Object[]>();
					in.put(id, list);
				}
				list.add(obj);
			} else if ("1".equals(JecnUtil.nullToEmpty(obj[1]))) {
				List<Object[]> list = out.get(id);
				if (list == null) {
					list = new ArrayList<Object[]>();
					out.put(id, list);
				}
				list.add(obj);
			}
		}

		sql = "select a.figure_id,b.file_id,b.file_name,a.type,c.type as s_type" + "    from jecn_figure_in_out" + pubT
				+ " a" + "   inner join jecn_figure_in_out_sample" + pubT + " c on a.id = c.in_out_id"
				+ "   inner join jecn_file" + pubT + " b" + "      on c.file_id = b.file_id" + "   where a.flow_id=? order by b.file_name asc";
		Map<String, List<Object[]>> sample = new HashMap<String, List<Object[]>>();
		objs = baseDao.listNativeSql(sql, processDownloadBean.getFlowId());
		for (Object[] obj : objs) {
			String id = obj[0].toString();
			List<Object[]> list = sample.get(id);
			if (list == null) {
				list = new ArrayList<Object[]>();
				sample.put(id, list);
			}
			list.add(obj);
		}
		for (Object[] os : allActivityShowList) {
			String id = os[8].toString();
			os[4] = concatIn(id, in, sample);
			os[5] = concatOut(id, out, sample);
		}
	}

	private static String concatIn(String id, Map<String, List<Object[]>> inouts, Map<String, List<Object[]>> sample) {
		StringBuilder b = new StringBuilder();
		if (inouts != null) {
			// 名称 文件名称
			List<Object[]> list = inouts.get(id);
			if (JecnUtil.isNotEmpty(list)) {
				for (Object[] ss : list) {
					if (StringUtils.isNotBlank(JecnUtil.nullToEmpty(ss[2]))) {
						b.append(ss[2]);
						b.append("\n");
					}
				}
			}
		}

		if (sample != null) {
			List<Object[]> list = sample.get(id);
			if (JecnUtil.isNotEmpty(list)) {
				for (Object[] ss : list) {
					if ("0".equals(JecnUtil.nullToEmpty(ss[3])) && "0".equals(JecnUtil.nullToEmpty(ss[4]))) {
						if (StringUtils.isNotBlank(JecnUtil.nullToEmpty(ss[2]))) {
							b.append("[模板]" + ss[2]);
							b.append("\n");
						}
					}
				}
			}
		}

		String result = b.toString();
		if (StringUtils.isNotBlank(result) && result.endsWith("\n")) {
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}

	private static String concatOut(String id, Map<String, List<Object[]>> inouts, Map<String, List<Object[]>> sample) {
		StringBuilder b = new StringBuilder();
		if (inouts != null) {
			// 名称 文件名称
			List<Object[]> list = inouts.get(id);
			if (JecnUtil.isNotEmpty(list)) {
				for (Object[] ss : list) {
					if (StringUtils.isNotBlank(JecnUtil.nullToEmpty(ss[2]))) {
						b.append(ss[2]);
						b.append("\n");
					}
				}
			}
		}

		if (sample != null) {
			List<Object[]> list = sample.get(id);
			if (JecnUtil.isNotEmpty(list)) {
				for (Object[] ss : list) {
					if ("1".equals(JecnUtil.nullToEmpty(ss[3])) && "0".equals(JecnUtil.nullToEmpty(ss[4]))) {
						if (StringUtils.isNotBlank(JecnUtil.nullToEmpty(ss[2]))) {
							b.append("[模板]" + ss[2]);
							b.append("\n");
						}
					}
				}
				for (Object[] ss : list) {
					if ("1".equals(JecnUtil.nullToEmpty(ss[3])) && "1".equals(JecnUtil.nullToEmpty(ss[4]))) {
						if (StringUtils.isNotBlank(JecnUtil.nullToEmpty(ss[2]))) {
							b.append("[样例]" + ss[2]);
							b.append("\n");
						}
					}
				}
			}
		}

		String result = b.toString();
		if (StringUtils.isNotBlank(result) && result.endsWith("\n")) {
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}

	private static String getFlowTypeById(IBaseDao dao, Long flowTypeId) {
		if (flowTypeId != null) {
			String hql = "from ProceeRuleTypeBean where typeId=?";
			ProceeRuleTypeBean bean = (ProceeRuleTypeBean) dao.getObjectHql(hql, flowTypeId);
			if (bean != null) {
				return bean.getTypeName();
			}
		}
		return "";
	}

	/**
	 * 
	 * 
	 * @param flowStructureDao
	 * @param flowId
	 * @param isPub
	 * @param listActiveShow
	 *            0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6对应内控矩阵风险点 7对应标准条款
	 * @return
	 * @throws Exception
	 */
	private static List<ActiveItSystem> getItSystems(IFlowStructureDao flowStructureDao, Long flowId, boolean isPub,
			List<Object[]> listActiveShow) throws Exception {

		List<ActiveItSystem> activeItSystems = new ArrayList<ActiveItSystem>();
		// 支撑IT系统
		List<JecnActiveOnLineTBean> listOnLineT = flowStructureDao.findActiveOnLineTBeans(flowId, isPub);
		if (listOnLineT == null) {
			return activeItSystems;
		}
		ActiveItSystem itSystem = null;
		for (Object[] objects : listActiveShow) {
			if (objects[0] == null) {
				continue;
			}
			String sysName = getSysName(listOnLineT, objects);
			if (sysName == null) {
				continue;
			}
			itSystem = new ActiveItSystem();
			itSystem.setActiveId(Long.parseLong(objects[0].toString()));
			itSystem.setActiveNum(objects[1] == null ? "" : objects[1].toString());
			itSystem.setActiveName(objects[2] == null ? "" : objects[2].toString());
			itSystem.setSystemName(sysName);
			activeItSystems.add(itSystem);
		}
		return activeItSystems;
	}

	private static String getSysName(List<JecnActiveOnLineTBean> listOnLineT, Object[] activeObj) {
		String sysName = null;
		for (JecnActiveOnLineTBean lineTBean : listOnLineT) {
			Long activeId = Long.parseLong(activeObj[0].toString());
			if (activeId.equals(lineTBean.getActiveId())) {
				if (sysName == null) {
					sysName = lineTBean.getSysName();
				} else {
					sysName += "/" + lineTBean.getSysName();
				}

			}
		}
		return sysName;
	}

	/**
	 * 获取审批人员的所属部门
	 * 
	 * @param latestHistoryNew
	 */
	private static void getHistoryNewApproveOrgName(IJecnDocControlDao docControlDao,
			JecnTaskHistoryNew latestHistoryNew) {
		if (latestHistoryNew == null || latestHistoryNew.getListJecnTaskHistoryFollow() == null) {
			return;
		}
		// 各阶段对应人员集合 key JecnTaskHistoryFollow ID,value :人员ID集合
		Map<Long, List<Long>> approveIdMap = new HashMap<Long, List<Long>>();
		// 审批人集合
		List<Long> approveIds = getApproveIdsByHistory(latestHistoryNew, approveIdMap);
		if (approveIds.isEmpty()) {
			return;
		}
		// 获取人员所属部门
		String sql = JecnCommonSqlTPath.INSTANCE.getOrgNameByPeopleIds(approveIds);
		List<Object[]> approveOrgNames = docControlDao.listNativeSql(sql);

		// 获取各阶段人员对应部门
		updateApproveOrgName(approveOrgNames, approveIdMap, latestHistoryNew);
	}

	private static void updateApproveOrgName(List<Object[]> approveOrgNames, Map<Long, List<Long>> approveIdMap,
			JecnTaskHistoryNew latestHistoryNew) {
		for (Entry<Long, List<Long>> entry : approveIdMap.entrySet()) {
			// 1、获取人员对应部门
			String orgName = getOrgNameByIds(entry.getValue(), approveOrgNames);
			updateHistoryFllowOrgName(orgName, latestHistoryNew, entry.getKey());
		}
	}

	private static void updateHistoryFllowOrgName(String orgName, JecnTaskHistoryNew latestHistoryNew, Long id) {
		for (JecnTaskHistoryFollow historyFollow : latestHistoryNew.getListJecnTaskHistoryFollow()) {
			if (id.toString().equals(historyFollow.getId().toString())) {
				historyFollow.setApproveOrgName(orgName);
				return;
			}
		}
	}

	private static String getOrgNameByIds(List<Long> appIds, List<Object[]> approveOrgNames) {
		String result = null;
		for (Long id : appIds) {
			for (Object[] objects : approveOrgNames) {
				if (id.toString().equals(objects[0].toString()) && objects[2] != null) {
					if (result == null) {
						result = objects[2].toString();
					} else {
						result += "/" + objects[2].toString();
					}
				}
			}
		}
		return result;
	}

	private static List<Long> getApproveIdsByHistory(JecnTaskHistoryNew latestHistoryNew,
			Map<Long, List<Long>> approveIdMap) {
		// 审批人集合
		List<Long> approveIds = new ArrayList<Long>();
		for (JecnTaskHistoryFollow historyFollow : latestHistoryNew.getListJecnTaskHistoryFollow()) {
			if (StringUtils.isBlank(historyFollow.getApproveId())) {
				continue;
			}
			if (!approveIdMap.containsKey(historyFollow.getId())) {
				approveIdMap.put(historyFollow.getId(), new ArrayList<Long>());
			}
			String[] approveArr = historyFollow.getApproveId().split(",");
			for (String approveId : approveArr) {
				if (StringUtils.isBlank(approveId)) {
					continue;
				}
				approveIds.add(Long.valueOf(approveId));
				approveIdMap.get(historyFollow.getId()).add(Long.valueOf(approveId));
			}
		}
		return approveIds;
	}

	private static String getApplicability(IFlowStructureDao flowStructureDao, Long flowId, boolean isPub,
			String applicability) {
		if (!JecnConfigTool.isShowOrgScope() || JecnConfigTool.isJinFeng()) {
			return applicability;
		}
		List<String> applyOrgs = flowStructureDao.getApplyOrgs(flowId, isPub);
		String detail = "";
		if (applyOrgs != null && applyOrgs.size() > 0) {
			for (int i = 0; i < applyOrgs.size(); i++) {
				if (i == 0) {
					detail += "1（组织范围）：" + applyOrgs.get(i);
				} else {
					detail += "/" + applyOrgs.get(i);
				}
			}
		}
		detail += "\n";
		detail += "2（业务范围 / 产品范围）：";
		if (StringUtils.isNotBlank(applicability)) {
			detail += applicability;
		}
		return detail;
	}

	private static List<Object[]> getStandardParentNames(IFlowStructureDao flowStructureDao, List<Object[]> standardList) {
		List<Object[]> parentNamelist = getStandardParentNames(standardList, flowStructureDao);
		List<Object[]> resultObj = new ArrayList<Object[]>();
		for (Object[] objects : standardList) {
			String parentPathName = "";
			for (Object[] obj : parentNamelist) {
				if (obj[0].toString().equals(objects[0].toString())) {
					if (StringUtils.isBlank(parentPathName)) {
						parentPathName = obj[1].toString();
					} else {
						parentPathName += "/" + obj[1].toString();
					}
				}
			}
			int length = objects.length;
			objects = addStandardObjParentName(objects);
			objects[length] = parentPathName;
			resultObj.add(objects);
		}
		return resultObj;
	}

	private static Object[] addStandardObjParentName(Object[] standard) {
		Object[] obj = new Object[standard.length + 1];
		for (int i = 0; i < standard.length; i++) {
			obj[i] = standard[i];
		}
		return obj;
	}

	private static List<Object[]> getStandardParentNames(List<Object[]> list, IFlowStructureDao flowStructureDao) {
		Set<Long> ids = new HashSet<Long>();
		for (Object[] resultBean : list) {
			ids.add(Long.valueOf(resultBean[0].toString()));
		}
		if (ids.isEmpty()) {
			return new ArrayList<Object[]>();
		}
		return flowStructureDao.getPathNameByIds(ids);
	}

	private static List<Object[]> getParents(Long flowId, boolean isPub, IFlowStructureDao flowStructureDao)
			throws Exception {
		return flowStructureDao.getParents(flowId, isPub);
	}

	private static ProcessInterfacesDescriptionBean getProcessInterfacesDescriptionBean(Long processId, Long pId,
			boolean isPub, IFlowStructureDao flowStructureDao) throws Exception {
		ProcessInterfacesDescriptionBean descriptionBean = new ProcessInterfacesDescriptionBean();
		if (pId != 0) {
			if (isPub) {
				JecnFlowStructure jecnFlowStructure = flowStructureDao.findJecnFlowStructureById(pId);
				descriptionBean.setPid(jecnFlowStructure.getFlowId());
				descriptionBean.setPname(jecnFlowStructure.getFlowName());
				descriptionBean.setPnumber(jecnFlowStructure.getFlowIdInput());
			} else {
				JecnFlowStructureT jecnFlowStructure = flowStructureDao.get(pId);
				descriptionBean.setPid(jecnFlowStructure.getFlowId());
				descriptionBean.setPname(jecnFlowStructure.getFlowName());
				descriptionBean.setPnumber(jecnFlowStructure.getFlowIdInput());
			}
		}
		List<ProcessInterface> interfaces = flowStructureDao.findFlowInterfaces(processId, isPub);
		for (ProcessInterface i : interfaces) {
			if (i.getImplType() == 1) {
				descriptionBean.getUppers().add(i);
			} else if (i.getImplType() == 2) {
				descriptionBean.getLowers().add(i);
			} else if (i.getImplType() == 3) {
				descriptionBean.getProcedurals().add(i);
			} else if (i.getImplType() == 4) {
				descriptionBean.getSons().add(i);
			}
		}
		return descriptionBean;
	}

	private static List<FileWebBean> getRelatedFiles(Long flowId, boolean isPub, IFlowStructureDao flowStructureDao)
			throws Exception {
		// 活动输入、操作规范文件
		List<Object[]> listActiveInputAndOperation = flowStructureDao.findAllActiveInputAndOperationByFlowId(flowId,
				isPub);
		Set<String> idSet = new HashSet<String>();
		List<FileWebBean> files = new ArrayList<FileWebBean>();
		for (Object[] objs : listActiveInputAndOperation) {
			if (objs[0] == null || objs[2] == null || objs[3] == null) {
				continue;
			}
			String id = objs[2].toString();
			if (idSet.contains(id)) {
				continue;
			} else {
				idSet.add(id);
				FileWebBean file = new FileWebBean();
				files.add(file);
				file.setFileId(Long.valueOf(objs[2].toString()));
				file.setFileName(JecnUtil.objToStr(objs[3]));
				file.setFileNum(JecnUtil.objToStr(objs[5]));
			}
		}
		// 活动输出
		List<Object[]> listActiveMode = flowStructureDao.findAllActiveOutputByFlowId(flowId, isPub);
		for (Object[] objs : listActiveMode) {
			if (objs[2] == null || objs[3] == null) {
				continue;
			}
			String id = objs[2].toString();
			if (idSet.contains(id)) {
				continue;
			} else {
				idSet.add(id);
				FileWebBean file = new FileWebBean();
				files.add(file);
				file.setFileId(Long.valueOf(objs[2].toString()));
				file.setFileName(JecnUtil.objToStr(objs[3]));
				file.setFileNum(JecnUtil.objToStr(objs[4]));
			}
		}
		return files;
	}

	/**
	 * 获取流程地图操作说明
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @param flowId
	 *            流程ID
	 * @param isPub
	 *            false为临时表， true为正式表
	 * @param configItemDao
	 * @param docControlDao
	 * @param flowStructureDao
	 * @return
	 * @throws Exception
	 */
	public static ProcessMapDownloadBean getProcessMapDownloadBean(Long flowId, boolean isPub,
			IJecnConfigItemDao configItemDao, IJecnDocControlDao docControlDao, IFlowStructureDao flowStructureDao)
			throws Exception {
		// 流程地图数据
		ProcessMapDownloadBean processMapDownloadBean = new ProcessMapDownloadBean();

		// 操作说明配置
		List<JecnConfigItemBean> configItemBeanList = configItemDao.selectFlowMapShowFlowFile();
		processMapDownloadBean.setJecnConfigItemBean(configItemBeanList);

		// 获取公司名称
		String companyName = configItemDao.selectValue(JecnConfigContents.TYPE_BIG_ITEM_PUB,
				ConfigItemPartMapMark.basicCmp.toString());
		processMapDownloadBean.setCompanyName(companyName);
		// 流程名称
		String flowName = "";
		// 流程密集
		String flowIsPublic = "";
		// 流程编号
		String flowInputNum = "";
		// 版本
		String flowVersion = "";

		// 目的
		String flowAim = "";
		// 范围
		String flowArea = "";
		// 术语定义
		String flowNounDefine = "";
		// 输入
		String flowInput = "";
		// 输出
		String flowOutput = "";
		// 流程地图图片
		Image img = null;
		// 步骤
		String flowShowStep = "";
		// 附件（不存放在数据库中）
		String fileName = "";
		// 文件ID
		long fileId = -1;
		// 审核人 数据
		List<Object[]> peopleList = new ArrayList<Object[]>();
		if (!isPub) {
			JecnFlowStructureT jecnFlowStructureT = flowStructureDao.get(flowId);
			// 流程名称
			flowName = jecnFlowStructureT.getFlowName();
			// 密级
			if (jecnFlowStructureT.getIsPublic() != null) {
				flowIsPublic = jecnFlowStructureT.getIsPublic().toString();
			}
			// 流程编号
			if (jecnFlowStructureT.getFlowIdInput() != null) {
				flowInputNum = jecnFlowStructureT.getFlowIdInput();
			}
			String version = ProcessDownDataUtil.getJecnTaskHistoryNew(peopleList, jecnFlowStructureT.getHistoryId(),
					docControlDao);
			// 版本
			if (version != null) {
				flowVersion = version;
			}
			Object[] mapObjArr = flowStructureDao.findFlowMap(flowId, isPub);

			if (mapObjArr != null) {
				// 目的
				if (mapObjArr[0] != null) {
					flowAim = mapObjArr[0].toString();
				}
				// 范围
				if (mapObjArr[1] != null) {
					flowArea = mapObjArr[1].toString();
				}
				// 术语定义
				if (mapObjArr[2] != null) {
					flowNounDefine = mapObjArr[2].toString();
				}
				// 输入
				if (mapObjArr[3] != null) {
					flowInput = mapObjArr[3].toString();
				}
				// 输出
				if (mapObjArr[4] != null) {
					flowOutput = mapObjArr[4].toString();
				}
				// 步骤
				if (mapObjArr[5] != null) {
					flowShowStep = mapObjArr[5].toString();
				}
				// 附件（不存放在数据库中）
				if (mapObjArr[6] != null) {
					fileName = mapObjArr[6].toString();
				}
				if (mapObjArr[7] != null) {
					fileId = Long.valueOf(mapObjArr[7].toString());
				}
			}
			// 流程地图图片
			img = null;
			// 图片文件路径
			String filePath = jecnFlowStructureT.getFilePath();
			if (filePath != null && !"".equals(filePath)) {
				img = JecnFinal.getImage(filePath);
			}
		} else {
			JecnFlowStructure jecnFlowStructure = (JecnFlowStructure) flowStructureDao.getSession().get(
					JecnFlowStructure.class, flowId);
			// 流程名称
			flowName = jecnFlowStructure.getFlowName();
			// 密级
			if (jecnFlowStructure.getIsPublic() != null) {
				flowIsPublic = jecnFlowStructure.getIsPublic().toString();
			}
			// 流程编号
			if (jecnFlowStructure.getFlowIdInput() != null) {
				flowInputNum = jecnFlowStructure.getFlowIdInput();
			}
			String version = ProcessDownDataUtil.getJecnTaskHistoryNew(peopleList, jecnFlowStructure.getHistoryId(),
					docControlDao);
			// 版本
			if (version != null) {
				flowVersion = version;
			}

			Object[] mapObjArr = flowStructureDao.findFlowMap(flowId, isPub);
			if (mapObjArr != null) {
				// 目的
				if (mapObjArr[0] != null) {
					flowAim = mapObjArr[0].toString();
				}
				// 范围
				if (mapObjArr[1] != null) {
					flowArea = mapObjArr[1].toString();
				}
				// 术语定义
				if (mapObjArr[2] != null) {
					flowNounDefine = mapObjArr[2].toString();
				}
				// 输入
				if (mapObjArr[3] != null) {
					flowInput = mapObjArr[3].toString();
				}
				// 输出
				if (mapObjArr[4] != null) {
					flowOutput = mapObjArr[4].toString();
				}
				// 步骤
				if (mapObjArr[5] != null) {
					flowShowStep = mapObjArr[5].toString();
				}
				// 附件（不存放在数据库中）
				if (mapObjArr[6] != null) {
					fileName = mapObjArr[6].toString();
				}
				if (mapObjArr[7] != null) {
					fileId = Long.valueOf(mapObjArr[7].toString());
				}
			}
			// 流程地图图片
			img = null;
			// 图片文件路径
			String filePath = jecnFlowStructure.getFilePath();
			if (filePath != null && !"".equals(filePath)) {
				img = JecnFinal.getImage(filePath);
			}
		}
		// 审核人数据
		processMapDownloadBean.setPeopleList(peopleList);
		// 流程名称
		processMapDownloadBean.setFlowName(flowName);
		// 流程密集
		processMapDownloadBean.setFlowIsPublic(flowIsPublic);
		// 流程编号
		processMapDownloadBean.setFlowInputNum(flowInputNum);
		// 版本
		processMapDownloadBean.setFlowVersion(flowVersion);

		// 目的
		processMapDownloadBean.setFlowAim(flowAim);
		// 范围
		processMapDownloadBean.setFlowArea(flowArea);
		// 术语定义
		processMapDownloadBean.setFlowNounDefine(flowNounDefine);
		// 输入
		processMapDownloadBean.setFlowInput(flowInput);
		// 输出
		processMapDownloadBean.setFlowOutput(flowOutput);
		// 流程地图图片
		processMapDownloadBean.setImg(img);
		// 步骤
		processMapDownloadBean.setFlowShowStep(flowShowStep);
		// 附件（不存放在数据库中）
		processMapDownloadBean.setFileName(fileName);
		// 文件ID
		processMapDownloadBean.setFileId(fileId);
		// 设置流程ID
		processMapDownloadBean.setFlowId(flowId);
		return processMapDownloadBean;
	}

	/**
	 * 制度操作说明下载数据
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @param ruleId
	 *            制度ID
	 * @param isPub
	 *            false为临时表 true为正式表
	 * @param ruleDao
	 * @param docControlDao
	 * @param configItemDao
	 * @return
	 * @throws Exception
	 */
	public static JecnCreateDoc getRuleFile(Long ruleId, boolean isPub, IRuleDao ruleDao,
			IJecnDocControlDao docControlDao, IJecnConfigItemDao configItemDao) throws Exception {
		RuleDownloadBean ruleDownloadBean = getRuleDownloadBean(ruleId, isPub, ruleDao, docControlDao, configItemDao);
		// 生成bute[];
		String templetPath = createRuleFile(ruleDownloadBean);
		byte[] bytes = JecnFinal.getByteByPath(templetPath);
		if (bytes == null) {
			return null;
		}
		JecnCreateDoc jecnCreateDoc = new JecnCreateDoc();
		jecnCreateDoc.setBytes(bytes);
		jecnCreateDoc.setFileName(ruleDownloadBean.getRuleName());
		if (templetPath != null) {
			// 获取临时文件
			File file = new File(templetPath);
			if (file.exists()) {
				file.delete();
			}
		}
		return jecnCreateDoc;
	}

	/**
	 * 获取制度操作说明数据
	 * 
	 * @author fuzhh Dec 22, 2012
	 * @param ruleId
	 *            制度ID
	 * @param isPub
	 *            false查询临时表 true查询正式表
	 * @param ruleDao
	 * @param docControlDao
	 * @param configItemDao
	 * @return
	 * @throws Exception
	 */
	public static RuleDownloadBean getRuleDownloadBean(Long ruleId, boolean isPub, IRuleDao ruleDao,
			IJecnDocControlDao docControlDao, IJecnConfigItemDao configItemDao) throws Exception {
		RuleDownloadBean ruleDownloadBean = new RuleDownloadBean();
		ruleDownloadBean.setPub(isPub);
		// 获取公司名称
		String companyName = configItemDao.selectValue(JecnConfigContents.TYPE_BIG_ITEM_PUB,
				ConfigItemPartMapMark.basicCmp.toString());
		ruleDownloadBean.setCompanyName(companyName);
		// 制度名称
		String ruleName = "";
		// 制度密集
		String ruleIsPublic = "";
		// 制度编号
		String ruleInputNum = "";
		// 版本
		String ruleVersion = "";
		// 相关内容
		List<RuleContentBean> ruleContentList = new ArrayList<RuleContentBean>();
		// 制度文件
		List<RuleFileBean> ruleFileList = new ArrayList<RuleFileBean>();
		// 审核人 数据
		List<Object[]> peopleList = new ArrayList<Object[]>();
		// 责任部门ID
		Long orgId = null;
		Rule rule = null;
		JecnRuleBaiscInfoT baiscInfo = null;
		if (!isPub) {
			// 查询制度信息
			RuleT ruleT = ruleDao.get(ruleId);
			rule = new Rule();
			PropertyUtils.copyProperties(rule, ruleT);
			baiscInfo = ruleDao.getJecnRuleBaiscInfoTById(ruleId);
		} else {
			// 查询制度信息
			rule = (Rule) ruleDao.getSession().get(Rule.class, ruleId);
			JecnRuleBaiscInfo ruleBaiscInfo = ruleDao.getJecnRuleBaiscInfoById(ruleId);
			if (ruleBaiscInfo != null) {
				baiscInfo = new JecnRuleBaiscInfoT();
				PropertyUtils.copyProperties(baiscInfo, ruleBaiscInfo);
			}
		}
		if (baiscInfo == null) {
			baiscInfo = new JecnRuleBaiscInfoT();
		}
		ruleDownloadBean.setBaiscInfo(baiscInfo);

		// 制度名称
		ruleName = rule.getRuleName();
		// 制度密集
		if (rule.getIsPublic() != null) {
			ruleIsPublic = rule.getIsPublic().toString();
		}
		// 制度编号
		if (rule.getRuleNumber() != null) {
			ruleInputNum = rule.getRuleNumber();
		}
		String version = ProcessDownDataUtil.getJecnTaskHistoryNew(peopleList, rule.getHistoryId(), docControlDao);
		// 版本
		if (version != null) {
			ruleVersion = version;
		}
		if (rule.getOrgId() != null) {
			orgId = rule.getOrgId();
		}

		// // 制度内容
		List<RuleContentT> ruleContentTList = ruleDao.findRuleContentT(ruleId);
		for (RuleContentT ruleContentT : ruleContentTList) {
			RuleContentBean ruleContentBean = new RuleContentBean();
			ruleContentBean.setRuleContent(ruleContentT.getContentStr());
			ruleContentBean.setRuleTitleId(ruleContentT.getRuleTitleId());
			ruleContentList.add(ruleContentBean);
		}
		// 制度文件
		List<RuleFileT> ruleFileTList = ruleDao.findRuleFileT(ruleId);
		for (RuleFileT ruleFileT : ruleFileTList) {
			RuleFileBean ruleFileBean = new RuleFileBean();
			ruleFileBean.setFileInput(ruleFileT.getFileInput());
			ruleFileBean.setFileName(ruleFileT.getFileName());
			ruleFileBean.setRuleTitleId(ruleFileT.getRuleTitleId());
			ruleFileList.add(ruleFileBean);
		}
		// 审核人数据
		ruleDownloadBean.setPeopleList(peopleList);
		// 制度内容
		ruleDownloadBean.setRuleContentList(ruleContentList);
		// 制度文件
		ruleDownloadBean.setRuleFileList(ruleFileList);
		// 制度标题信息
		List<RuleTitleT> ruleTitleList = ruleDao.findRuleTitle(ruleId);
		ruleDownloadBean.setRuleTitleTList(ruleTitleList);
		// 制度流程关联
		List<RuleFlowBean> ruleFlowList = new ArrayList<RuleFlowBean>();
		List<JecnFlowCommon> jecnFlowCommonList = ruleDao.findRuleFlow(ruleId, 1L, 0, isPub);
		for (JecnFlowCommon jecnFlowCommon : jecnFlowCommonList) {
			RuleFlowBean ruleFlowBean = new RuleFlowBean();
			ruleFlowBean.setFlowInput(jecnFlowCommon.getFlowNumber());
			ruleFlowBean.setFlowName(jecnFlowCommon.getFlowName());
			ruleFlowBean.setRuleTitleId(jecnFlowCommon.getRuleTitleId());
			ruleFlowList.add(ruleFlowBean);
		}
		ruleDownloadBean.setRuleFlowList(ruleFlowList);
		// 制度流程地图关联关系
		List<RuleFlowBean> ruleFlowMapList = new ArrayList<RuleFlowBean>();
		List<JecnFlowCommon> jecnFlowMapCommonList = ruleDao.findRuleFlow(ruleId, 0L, 0, isPub);
		for (JecnFlowCommon jecnFlowCommon : jecnFlowMapCommonList) {
			RuleFlowBean ruleFlowBean = new RuleFlowBean();
			ruleFlowBean.setFlowInput(jecnFlowCommon.getFlowNumber());
			ruleFlowBean.setFlowName(jecnFlowCommon.getFlowName());
			ruleFlowBean.setRuleTitleId(jecnFlowCommon.getRuleTitleId());
			ruleFlowMapList.add(ruleFlowBean);
		}
		ruleDownloadBean.setRuleFlowMapList(ruleFlowMapList);

		// 制度风险关联关系
		List<JecnRiskCommon> jecnRiskCommonList = ruleDao.findRuleRisk(ruleId, 0);
		ruleDownloadBean.setRuleRiskList(jecnRiskCommonList);
		// 制度关联关系
		List<JecnStandarCommon> jecnStandarCommonList = ruleDao.findRuleStandar(ruleId, 0);
		ruleDownloadBean.setRuleStandardList(jecnStandarCommonList);

		List<JecnRuleCommon> jecnRuleCommonList = ruleDao.findRule(ruleId, 0, isPub);
		ruleDownloadBean.setRuleRuleList(jecnRuleCommonList);

		// 制度名称
		ruleDownloadBean.setRuleName(ruleName);
		// 制度密集
		ruleDownloadBean.setRuleIsPublic(ruleIsPublic);
		// 制度编号
		ruleDownloadBean.setRuleInputNum(ruleInputNum);
		// 版本
		ruleDownloadBean.setRuleVersion(ruleVersion);
		// 责任部门名称
		String orgName = ruleDao.getOrgName(orgId);
		ruleDownloadBean.setOrgName(orgName);
		ruleDownloadBean.setDutyName(getDutyName(ruleDao, rule.getAttchMentId(), rule.getTypeResPeople()));
		// 所有的文控记录
		List<JecnTaskHistoryNew> historys = docControlDao.getJecnTaskHistoryNewList(ruleId, 2);
		ruleDownloadBean.setHistorys(historys);
		if (JecnContants.otherOperaType == 23 && historys != null && historys.size() > 0) {// 蒙牛
			getHistoryNewApproveOrgName(docControlDao, historys.get(0));
		}
		JecnTaskHistoryNew latestHistoryNew = docControlDao.getLatestHistory(rule.getId(), 2);
		ruleDownloadBean.setLatestHistoryNew(latestHistoryNew);

		return ruleDownloadBean;
	}

	private static String getDutyName(IRuleDao ruleDao, Long dutyId, Integer dutyType) {
		if (dutyId == null || dutyType == null) {
			return "";
		}
		String result = "";
		if (dutyId != null) {
			if (dutyType == 0) {// 人员
				String hql = "from JecnUser where peopleId=?";
				JecnUser user = ruleDao.getObjectHql(hql, dutyId);
				if (user != null) {
					result = user.getTrueName();
				}
			} else if (dutyType == 1) {// 岗位
				String hql = "from JecnFlowOrgImage where figureId=?";
				JecnFlowOrgImage pos = ruleDao.getObjectHql(hql, dutyId);
				if (pos != null) {
					result = pos.getFigureText();
				}
			}
		}
		return result;
	}

	/**
	 * 排序活动
	 * 
	 * @param list
	 * @return
	 */
	public static void getActivitySort(List<Object[]> list) {
		// 外循环控制比较的次数
		for (int i = 0; i < list.size(); i++) {
			// 内循环控制比较后移位
			for (int j = list.size() - 1; j > i; j--) {
				Object[] processActiveBeanOne = list.get(j - 1);
				Object[] processActiveBeanTwo = list.get(j);
				Object oneActivesNumber = processActiveBeanOne[1];
				Object twoActivesNumber = processActiveBeanTwo[1];
				if (oneActivesNumber != null && twoActivesNumber != null) {
					if (oneActivesNumber.toString().indexOf("-") > 0) {
						oneActivesNumber = oneActivesNumber.toString().substring(
								oneActivesNumber.toString().lastIndexOf("-") + 1, oneActivesNumber.toString().length());
					}
					if (!oneActivesNumber.toString().matches("[0-9]+")) {
						list.set(j - 1, processActiveBeanTwo);
						list.set(j, processActiveBeanOne);
						continue;
					}

					if (twoActivesNumber.toString().indexOf("-") > 0) {
						twoActivesNumber = twoActivesNumber.toString().substring(
								twoActivesNumber.toString().lastIndexOf("-") + 1, twoActivesNumber.toString().length());
					}
					if (!twoActivesNumber.toString().matches("[0-9]+")) {
						continue;
					}

					if (Long.valueOf(oneActivesNumber.toString()) > Long.valueOf(twoActivesNumber.toString())) {
						list.set(j - 1, processActiveBeanTwo);
						list.set(j, processActiveBeanOne);
					}
				} else if (oneActivesNumber == null) {
					list.set(j - 1, processActiveBeanTwo);
					list.set(j, processActiveBeanOne);
				}
			}
		}
	}

	// 宋体五号,加粗,居中,显示为11号,没有五号
	public static Paragraph ST_5_BOLD_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 11, Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 宋体五号,加粗,居右,显示为11号,没有五号
	public static Paragraph ST_5_BOLD_RIGHT(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 11, Font.BOLD));
		p.setAlignment(Element.ALIGN_RIGHT);
		return p;
	}

	// 宋体小二,加粗,居中

	public static Paragraph ST_X2_BOLD_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 18, Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 宋体小一,显示为16号,没有小一
	public static Paragraph ST_24(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 24, Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 宋体三号,显示为16号,没有三号
	public static Paragraph ST_3(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 16));
	}

	// 宋体小三,居中
	public static Paragraph ST_X3_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 15));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 宋体四号
	public static Paragraph ST_4(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 14));
	}

	// 宋体小四,加粗
	public static Paragraph ST_X4_BOLD(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 12, Font.BOLD));
	}

	// 宋体小五号,加粗
	public static Paragraph ST_X5_BOLD(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 9, Font.BOLD));
	}

	// 宋体五号,显示为11号,没有五号
	public static Paragraph ST_5(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 11));
	}

	// 宋体初号,显示为42号,没有初号
	public static Paragraph ST_36(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 42));
	}

	// 宋体三号,显示为16号,没有三号
	public static Paragraph ST_16(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 16, Font.BOLD));
	}

	// 宋体二号 ,显示为22号,没有二号
	public static Paragraph ST_22(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 22, Font.BOLD));
	}

	// 宋体一号,显示为26号,没有一号
	public static Paragraph ST_26(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 26, Font.BOLD));
	}

	// 宋体五号,加粗,显示为11号,没有五号
	public static Paragraph ST_5_BOLD(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 11, Font.BOLD));
	}

	// Arial小五号
	public static Paragraph Arial_5(String content) {
		return new Paragraph(content, new RtfFont("Arial", 11));
	}

	// 宋体 五号 黑体
	public static Paragraph contentTitle(String title) {
		return new Paragraph(title, new Font(Font.SYMBOL, 11, Font.BOLD));
	}

	// Arial小五号,加粗
	public static Paragraph Arial_5_BOLD(String content) {
		return new Paragraph(content, new RtfFont("Arial", 9, Font.BOLD));
	}

	// 宋体 居左,小四 显示为11号 加粗
	public static Paragraph huadi_BODY_12_MIDDLE(String content) {
		RtfFont font = new RtfFont("宋 体", 12, Font.BOLD);
		Paragraph p = new Paragraph(DownloadUtil.changeNetToWordStyle(content), font);
		p.setAlignment(Element.ALIGN_LEFT);
		p.setLeading(22.5f);
		return p;
	}

	// 宋体 居左,小四 显示为11号 不加粗
	public static Paragraph huadi_ST_12_MIDDLE(String content) {
		RtfFont font = new RtfFont("宋 体", 12);
		Paragraph p = new Paragraph(DownloadUtil.changeNetToWordStyle(content), font);
		p.setAlignment(Element.ALIGN_LEFT);
		p.setLeading(22.5f);
		return p;
	}

	// 宋体 小四,加粗,居中,显示为11号,没有小四
	public static Paragraph huadi_ST_5_BOLD_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 12, Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 宋体 小四,加粗,居中,显示为12号,没有小四
	public static Paragraph huadi_ST_5_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 12));
		p.setAlignment(Element.ALIGN_LEFT);
		return p;
	}

	/**
	 * 生成表格
	 * 
	 * @author fuzhh Nov 6, 2012
	 * @param tableWidth
	 *            表格的宽度
	 * @param tableInt
	 *            表格各个宽度的数组
	 * @param tableTopNameList
	 *            表格名称集合
	 * @param tableContentList
	 *            表格内容集合
	 * @return
	 */
	public static Table getHuadiTable(float tableWidth, int[] tableInt, List<String> tableTitleNameList,
			List<Object[]> tableContentList) {
		Table table = null;
		try {
			table = new Table(tableInt.length);
			// 设置各个表格宽度
			table.setWidths(tableInt);
			// 设置居左
			table.setAlignment(Table.ALIGN_LEFT);
			// 设置宽度
			table.setWidth(tableWidth);
			for (int i = 0; i < tableTitleNameList.size(); i++) {
				String tableTop = tableTitleNameList.get(i);
				Cell tableTopCell = new Cell(huadi_ST_5_BOLD_MIDDLE(tableTop));
				tableTopCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(tableTopCell, new Point(0, i));
			}
			int tableCount = 1;
			for (Object[] obj : tableContentList) {
				for (int i = 0; i < tableInt.length; i++) {
					Object object = obj[i];
					if (object == null) {
						object = "";
					}
					table.addCell(huadi_ST_5_MIDDLE(object.toString()), new Point(tableCount, i));
				}
				tableCount++;
			}
		} catch (BadElementException e) {
			log.error("", e);
		} catch (DocumentException e) {
			log.error("", e);
		}
		return table;
	}

	public static ProcessDownloadBaseBean getProcessDownloadBean(Long flowId, boolean isPub,
			IJecnConfigItemDao configItemDao, IJecnDocControlDao docControlDao, IFlowStructureDao flowStructureDao,
			IProcessBasicInfoDao processBasicDao, IProcessKPIDao processKPIDao, IProcessRecordDao processRecordDao,
			String mark) throws Exception {
		ProcessDownloadBaseBean processDownloadBean = new ProcessDownloadBaseBean();
		if (StringUtils.isBlank(mark)) {
			return processDownloadBean;
		}
		if (mark.equals("a1")) {// 目的

		} else if (mark.equals("a2")) {// 适用范围

		} else if (mark.equals("a3")) {// 术语定义

		} else if (mark.equals("a4")) {// 流程驱动规则

		} else if (mark.equals("a5")) {// 输入

		} else if (mark.equals("a6")) {// 输出

		} else if (mark.equals("a7")) {// 关键活动

		} else if (mark.equals("a8")) {// 角色职责

		} else if (mark.equals("a9")) {// 流程图

		} else if (mark.equals("a10")) {// 活动说明

		} else if (mark.equals("a11")) {// 流程记录
			List<Object[]> listActiveShow = null;
			/** ******************** 南京石化多两数据 *********************8 */
			if (JecnContants.otherOperaType == 6) {
				// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6对应内控矩阵风险点 7对应标准条款
				listActiveShow = flowStructureDao.getActiveByFlowId(flowId, isPub);
			} else {
				// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6 活动输入 7 活动输出 8链接id
				// 9链接类型
				// 10办理时限目标值 11办理时限原始值
				listActiveShow = flowStructureDao.getActiveShowByFlowId(flowId, isPub);
			}
			// 活动排序
			getActivitySort(listActiveShow);
			// 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号 7
			// 活动文本输入
			List<Object[]> listInputFiles = null;
			if (JecnContants.otherOperaType == 11) {// 农夫山泉 多添加一列：责任部门
				listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowIdNFSQ(flowId, isPub);
			} else {
				listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowId(flowId, isPub);
			}

			// 流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号 5活动文本输出
			List<Object[]> listOutFiles = flowStructureDao.findAllActiveOutputByFlowId(flowId, isPub);
			// 流程记录 0 文件编号 1 文件名称 输入和输出集合
			List<Object[]> processRecordList = new ArrayList<Object[]>();
			// 操作规范 0 文件编号 1 文件名称
			List<Object[]> operationTemplateList = new ArrayList<Object[]>();

			if (JecnContants.otherOperaType == 11) { // 农夫山泉
				ProcessDownDataUtil.nongfushanquan_getFlowFiles(processRecordList, operationTemplateList,
						listInputFiles, listOutFiles, processDownloadBean);
			} else if (JecnContants.otherOperaType == 13) { // 广机 按照活动排序来排序相关文件
				ProcessDownDataUtil.guangji_getFlowFiles(processRecordList, operationTemplateList, listInputFiles,
						listOutFiles, listActiveShow);
			} else {
				ProcessDownDataUtil
						.getFlowFiles(processRecordList, operationTemplateList, listInputFiles, listOutFiles);
			}
			processDownloadBean.setProcessRecordList(processRecordList);

		} else if (mark.equals("a12")) {// 操作规范
			List<Object[]> listActiveShow = null;
			/** ******************** 南京石化多两数据 *********************8 */
			if (JecnContants.otherOperaType == 6) {
				// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6对应内控矩阵风险点 7对应标准条款
				listActiveShow = flowStructureDao.getActiveByFlowId(flowId, isPub);
			} else {
				// 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6 活动输入 7 活动输出 8链接id
				// 9链接类型
				// 10办理时限目标值 11办理时限原始值
				listActiveShow = flowStructureDao.getActiveShowByFlowId(flowId, isPub);
			}
			// 活动排序
			getActivitySort(listActiveShow);
			// 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号 7
			// 活动文本输入
			List<Object[]> listInputFiles = null;
			if (JecnContants.otherOperaType == 11) {// 农夫山泉 多添加一列：责任部门
				listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowIdNFSQ(flowId, isPub);
			} else {
				listInputFiles = flowStructureDao.findAllActiveInputAndOperationByFlowId(flowId, isPub);
			}

			// 流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号 5活动文本输出
			List<Object[]> listOutFiles = flowStructureDao.findAllActiveOutputByFlowId(flowId, isPub);
			// 流程记录 0 文件编号 1 文件名称 输入和输出集合
			List<Object[]> processRecordList = new ArrayList<Object[]>();
			// 操作规范 0 文件编号 1 文件名称
			List<Object[]> operationTemplateList = new ArrayList<Object[]>();

			if (JecnContants.otherOperaType == 11) { // 农夫山泉
				ProcessDownDataUtil.nongfushanquan_getFlowFiles(processRecordList, operationTemplateList,
						listInputFiles, listOutFiles, processDownloadBean);
			} else if (JecnContants.otherOperaType == 13) { // 广机 按照活动排序来排序相关文件
				ProcessDownDataUtil.guangji_getFlowFiles(processRecordList, operationTemplateList, listInputFiles,
						listOutFiles, listActiveShow);
			} else {
				ProcessDownDataUtil
						.getFlowFiles(processRecordList, operationTemplateList, listInputFiles, listOutFiles);
			}
			processDownloadBean.setProcessRecordList(processRecordList);
			// FIXME 农夫山泉获取操作规范文件数据（临时）
			processDownloadBean.setOperationTemplateList(operationTemplateList);
		} else if (mark.equals("a13")) {// 相关流程

		} else if (mark.equals("a14")) {// 相关制度

		} else if (mark.equals("a15")) {// 流程关键测评指标板

		} else if (mark.equals("a16")) {// 客户

		} else if (mark.equals("a17")) {// 不适用范围

		} else if (mark.equals("a18")) {// 概述

		} else if (mark.equals("a19")) {// 补充说明

		} else if (mark.equals("a20")) {// 记录保存

		} else if (mark.equals("a21")) {// 相关标准

		} else if (mark.equals("a25")) {// 相关风险

		} else if (mark.equals("a27")) {// 自定义要素1

		} else if (mark.equals("a28")) {// 自定义要素2

		} else if (mark.equals("a29")) {// 自定义要素3

		} else if (mark.equals("a30")) {// 自定义要素4

		} else if (mark.equals("a31")) {// 自定义要素5

		} else if (mark.equals("a32")) {// 输入和输出

		} else if (mark.equals("a22")) {// 流程边界

		} else if (mark.equals("a23")) {// 流程责任属性

		} else if (mark.equals("a24")) {// 流程文控信息

		} else if (mark.equals("a33")) {// 流程范围
			// 包含起始结束输入输出

		} else if (mark.equals("a34")) {// 术语
		} else if (mark.equals("a35")) {// 相关文件
			// 流程相关文件
			processDownloadBean.setRelatedFiles(getRelatedFiles(flowId, isPub, flowStructureDao));
		} else if (mark.equals("a36")) {// 流程接口描述

		} else if (mark.equals("a37")) {// 流程标准文件

		} else if (mark.equals("a38")) {// 信息化

		}
		return processDownloadBean;
	}
}
