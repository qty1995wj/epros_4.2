package com.jecn.epros.server.util;

import java.io.InputStream;
import java.sql.Blob;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.lob.SerializableBlob;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.FlowStandardizedFileT;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.RuleStandardizedFileT;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.task.IJecnAbstractTaskDao;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class JecnDaoUtil {
	private static Logger log = Logger.getLogger(JecnDaoUtil.class);

	/**
	 * 根据文件ID查阅文件版本信息
	 * 
	 * @param fileDao
	 *            文件接口类
	 * @param log
	 *            日志
	 * @param id
	 *            文件主键ID
	 * @return
	 * @throws Exception
	 */
	private static FileOpenBean openVersion(JecnFileContent jecnFileContent) throws Exception {
		if (jecnFileContent == null) {
			return null;
		}
		byte[] bytes = null;
		if (jecnFileContent.getIsVersionLocal() == 0) {// 保存方式，0本地保存，1数据库大字段保存
			bytes = JecnFinal.getBytesByFilePath(jecnFileContent.getFilePath());
		} else {
			bytes = blobToBytes(jecnFileContent.getFileStream());
		}
		FileOpenBean fileOpenBean = new FileOpenBean();
		fileOpenBean.setId(jecnFileContent.getId());
		fileOpenBean.setName(jecnFileContent.getFileName());
		fileOpenBean.setFileByte(bytes);
		return fileOpenBean;

	}

	/**
	 * 获得文件内容
	 * 
	 * @param fileDao
	 * @param contentId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static FileOpenBean getFileOpenBean(IBaseDao fileDao, Long contentId) throws Exception {
		// 1、获取版本文件
		JecnFileContent fileContent = (JecnFileContent) fileDao.getSession().get(JecnFileContent.class, contentId);
		// 根据文件ID查阅文件版本信息
		if (fileContent.getIsVersionLocal() == 0) {
			FileOpenBean fileOpenBean = new FileOpenBean();
			fileOpenBean.setId(fileContent.getId());
			fileOpenBean.setName(fileContent.getFileName());
			fileOpenBean.setFileByte(JecnFinal.getBytesByFilePath(fileContent.getFilePath()));
			return fileOpenBean;
		} else {
			return JecnDaoUtil.openVersion(fileContent);
		}

	}

	public static FileOpenBean openRuleLocalFile(IRuleDao ruleDao, Long id) throws Exception {

		FileOpenBean fileOpenBean = null;
		try {

			fileOpenBean = new FileOpenBean();
			String hql = "from G020RuleFileContent where id=?";
			G020RuleFileContent fileContent = ruleDao.getObjectHql(hql, id);
			if (fileContent != null) {
				fileOpenBean.setId(fileContent.getId());
				byte[] content = null;
				// 数据库文件
				if (fileContent.getIsVersionLocal() == 1) {
					content = blobToBytes(fileContent.getFileStream());
				} else {
					// 本地文件
					content = JecnFinal.getBytesByFilePath(fileContent.getFilePath());
				}
				fileOpenBean.setFileByte(content);
				fileOpenBean.setName(fileContent.getFileName());
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return fileOpenBean;

	}

	/**
	 * 批量删除树节点验证树节点是否处于任务中
	 * 
	 * @param rids
	 *            选中的树节点ID集合（包含叶子节点）
	 * @param nodeType
	 *            节点类型
	 * @return 拼装后的sql
	 */
	public static List<String> getSetRefIdsInTask(Set<Long> rids, TreeNodeType nodeType) {
		String sql = "";
		List<String> list = null;
		switch (nodeType) {
		case process:// 流程图 或流程地图
			sql = "select f.flow_name from jecn_task_bean_new t,JECN_FLOW_STRUCTURE_T f where "
					+ "t.r_id=f.FLOW_ID and t.state<>5 and t.is_lock=1 and (task_Type=0 or task_Type=4) and f.flow_id in";
			list = JecnCommonSql.getSetSqls(sql, rids);
			break;
		case file:// 文件
			sql = "select f.file_name from jecn_task_bean_new t,jecn_file_t f where "
					+ "t.r_id=f.FILE_ID and t.state<>5 and t.is_lock=1 and task_Type=1 and f.FILE_ID in";
			list = JecnCommonSql.getSetSqls(sql, rids);
			break;
		case ruleModeFile:// 制度模板文件或制度文件
			sql = "select r.rule_name from jecn_task_bean_new t,JECN_RULE_T r where "
					+ "t.r_id=r.ID and t.state<>5 and t.is_lock=1 and (task_Type=2 or task_Type=3) and r.ID in";
			list = JecnCommonSql.getSetSqls(sql, rids);
			break;
		}
		return list;
	}

	/**
	 * 批量删除树节点验证树节点（包括子节点）是否处于任务中（流程）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllChildFlow(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JFT WHERE JFT.FLOW_ID in "
					+ JecnCommonSql.getIds(rids)
					+ " UNION ALL"
					+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.Flow_Id = JFS.Pre_Flow_Id)";
			sql = sql
					+ " select count(t.id) from jecn_task_bean_new t,JECN_FLOW_STRUCTURE_T f where "
					+ "t.r_id=f.FLOW_ID and t.state<>5 and t.is_lock=1 and (t.task_Type=0 or t.task_Type=4) and f.flow_id in (select distinct flow_id from MY_FLOW)";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " select count(t.id) from jecn_task_bean_new t,JECN_FLOW_STRUCTURE_T f where "
					+ "t.r_id=f.FLOW_ID and t.state<>5 and t.is_lock=1 and (t.task_Type=0 or t.task_Type=4) and f.flow_id in (";
			sql = sql + " select distinct JFT.FLOW_ID from JECN_FLOW_STRUCTURE_T JFT"
					+ " connect by prior JFT.FLOW_ID = JFT.Pre_Flow_Id START WITH JFT.FLOW_ID in "
					+ JecnCommonSql.getIds(rids);
			sql = sql + ")";

		}
		return sql;
	}

	/**
	 * 批量删除树节点验证树节点（包括子节点）是否处于任务中（文件）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllChildFile(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_FILE AS(SELECT * FROM JECN_FILE_T JFT WHERE JFT.FILE_ID in " + JecnCommonSql.getIds(rids)
					+ " UNION ALL"
					+ " SELECT JFS.* FROM MY_FILE INNER JOIN JECN_FILE_T JFS ON MY_FILE.FILE_ID = JFS.PER_FILE_ID)";
			sql = sql
					+ " select count(t.id) from jecn_task_bean_new t,JECN_FILE_T f where "
					+ "t.r_id=f.FILE_ID and t.state<>5 and t.is_lock=1 and t.task_Type=1 and f.FILE_ID in (select distinct FILE_ID from MY_FILE WHERE IS_DIR=1)";

		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " select count(t.id) from jecn_task_bean_new t,JECN_FILE_T f where "
					+ "t.r_id=f.FILE_ID and t.state<>5 and t.is_lock=1 and t.task_Type=1 and f.FILE_ID in (";
			sql = sql + " select distinct JFT.FILE_ID from JECN_FILE_T JFT  where JFT.IS_DIR=1 "
					+ " connect by prior JFT.FILE_ID = JFT.PER_FILE_ID START WITH JFT.FILE_ID in "
					+ JecnCommonSql.getIds(rids);
			sql = sql + ")";
		}
		return sql;
	}

	/**
	 * 批量删除树节点验证树节点（包括子节点）是否处于任务中（制度）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllChildRule(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_RULE AS(SELECT * FROM JECN_RULE_T JFT WHERE JFT.ID in " + JecnCommonSql.getIds(rids)
					+ " UNION ALL"
					+ " SELECT JFS.* FROM MY_RULE INNER JOIN JECN_RULE_T JFS ON MY_RULE.ID = JFS.PER_ID)";
			sql = sql
					+ " select count(t.id) from jecn_task_bean_new t,JECN_RULE_T f where "
					+ "t.r_id=f.ID and t.state<>5 and t.is_lock=1 and (t.task_Type=2 or t.task_Type=3) and f.ID in (select distinct ID from MY_RULE WHERE (IS_DIR=1 OR IS_DIR=2))";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " select count(t.id) from jecn_task_bean_new t,JECN_RULE_T f where "
					+ "t.r_id=f.ID and t.state<>5 and t.is_lock=1 and (t.task_Type=2 or t.task_Type=3) and f.ID in (";
			sql = sql + " select distinct JFT.ID from JECN_RULE_T JFT  where (JFT.IS_DIR=1 OR JFT.IS_DIR=2) "
					+ " connect by prior JFT.ID = JFT.PER_ID START WITH JFT.ID in " + JecnCommonSql.getIds(rids);
			sql = sql + ")";

		}
		return sql;
	}

	/**
	 * 批量删除树节点验证树节点是否处于任务中
	 * 
	 * @param rids
	 *            选中的树节点ID集合（包含叶子节点）
	 * @param nodeType
	 *            节点类型
	 * @return 拼装后的sql
	 */
	public static String getListRefIdsInTask(List<Long> rids, TreeNodeType nodeType) {
		String sql = "";
		switch (nodeType) {
		case process:// 流程图 或流程地图
			sql = getAllChildFlow(rids);
			break;
		case file:// 文件
			sql = getAllChildFile(rids);
			break;
		case ruleModeFile:// 制度模板文件或制度文件
			sql = getAllChildRule(rids);
			break;
		}
		return sql;
	}

	/**
	 * 批量删除树节点验证树节点（包括子节点）是否处于发布（流程）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllChildFlowPub(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JFT WHERE JFT.FLOW_ID in "
					+ JecnCommonSql.getIds(rids)
					+ " UNION ALL"
					+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.Flow_Id = JFS.Pre_Flow_Id)";
			sql = sql
					+ " select count(jfs.flow_id) from MY_FLOW,JECN_FLOW_STRUCTURE jfs where MY_FLOW.flow_id=jfs.flow_id";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " select count(f.flow_id) from JECN_FLOW_STRUCTURE f where f.flow_id in (";
			sql = sql + " select distinct JFT.FLOW_ID from JECN_FLOW_STRUCTURE_T JFT"
					+ " connect by prior JFT.FLOW_ID = JFT.Pre_Flow_Id START WITH JFT.FLOW_ID in "
					+ JecnCommonSql.getIds(rids);
			sql = sql + ")";

		}
		return sql;
	}

	/**
	 * 批量删除树节点验证树节点（包括子节点）是否处于发布（文件）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllChildFilePub(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_FILE AS(SELECT * FROM JECN_FILE_T JFT WHERE JFT.FILE_ID in " + JecnCommonSql.getIds(rids)
					+ " UNION ALL"
					+ " SELECT JFS.* FROM MY_FILE INNER JOIN JECN_FILE_T JFS ON MY_FILE.FILE_ID = JFS.PER_FILE_ID)";
			sql = sql
					+ " select count(JF.FILE_ID) from MY_FILE,JECN_FILE JF WHERE  MY_FILE.IS_DIR=1 and JF.IS_DIR=1 and JF.FILE_ID=MY_FILE.FILE_ID";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " select count(f.FILE_ID) from JECN_FILE f where f.IS_DIR=1 and f.FILE_ID in (";
			sql = sql + " select distinct JFT.FILE_ID from JECN_FILE_T JFT  where JFT.IS_DIR=1 "
					+ " connect by prior JFT.FILE_ID = JFT.PER_FILE_ID START WITH JFT.FILE_ID in "
					+ JecnCommonSql.getIds(rids);
			sql = sql + ")";

		}
		return sql;
	}

	/**
	 * 批量删除树节点验证树节点（包括子节点）是否处于发布（制度）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllChildRulePub(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_RULE AS(SELECT * FROM JECN_RULE_T JFT WHERE JFT.ID in " + JecnCommonSql.getIds(rids)
					+ " UNION ALL"
					+ " SELECT JFS.* FROM MY_RULE INNER JOIN JECN_RULE_T JFS ON MY_RULE.ID = JFS.PER_ID)";
			sql = sql
					+ " select count(t.id) from JECN_RULE t, MY_RULE WHERE (MY_RULE.IS_DIR=1 OR MY_RULE.IS_DIR=2) and MY_RULE.id=t.id and (t.IS_DIR=1 OR t.IS_DIR=2)";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " select count(f.id) from JECN_RULE f where (f.IS_DIR=1 OR f.IS_DIR=2) and f.ID in (";
			sql = sql + " select distinct JFT.ID from JECN_RULE_T JFT where (JFT.IS_DIR=1 OR JFT.IS_DIR=2) "
					+ " connect by prior JFT.ID = JFT.PER_ID START WITH JFT.ID in " + JecnCommonSql.getIds(rids);
			sql = sql + ")";

		}
		return sql;
	}

	/**
	 * 批量删除树节点验证树节点是否处于发布
	 * 
	 * @param rids
	 *            选中的树节点ID集合（包含叶子节点）
	 * @param nodeType
	 *            节点类型
	 * @return 拼装后的sql
	 */
	public static String getSqlExistPub(List<Long> rids, TreeNodeType nodeType) {
		String sql = "";
		switch (nodeType) {
		case process:// 流程图 或流程地图
			sql = getAllChildFlowPub(rids);
			break;
		case file:// 文件
			sql = getAllChildFilePub(rids);
			break;
		case ruleModeFile:// 制度模板文件或制度文件
			sql = getAllChildRulePub(rids);
			break;
		}
		return sql;
	}

	/**
	 * 文件删除审批验证*********************** 批量删除树文件节点验证文件树节点是否在制度文件中处于任务中（文件、制度文件）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllRuleTaskByChildFileId(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = " WITH MY_FILE AS" + " (SELECT *" + "  FROM JECN_FILE_T JFT" + " WHERE JFT.FILE_ID in "
					+ JecnCommonSql.getIds(rids) + " UNION ALL" + " SELECT JFS.*" + "  FROM MY_FILE"
					+ " INNER JOIN JECN_FILE_T JFS ON MY_FILE.FILE_ID = JFS.PER_FILE_ID)" + "  select count(t.id)"
					+ "    from jecn_rule_t        ju," + "         jecn_task_bean_new t,"
					+ "         JECN_FILE_T        f" + "   where t.r_id = ju.id" + "     and t.state <> 5"
					+ "     and t.is_lock = 1" + "     and (t.task_Type = 2 or t.task_Type = 3)"
					+ "   and ju.is_dir = 2" + "     and ju.file_id = f.file_id" + "     and f.FILE_ID in"
					+ "         (select distinct FILE_ID" + "            from MY_FILE" + "           WHERE IS_DIR = 1)";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " select count(t.id)" + "  from jecn_rule_t ju, jecn_task_bean_new t, JECN_FILE_T f"
					+ " where t.r_id = ju.id" + "   and ju.is_dir = 2" + "   and t.state <> 5" + "   and t.is_lock = 1"
					+ "   and (t.task_Type = 2 or t.task_Type = 3)" + "   and ju.file_id = f.file_id"
					+ "   and f.FILE_ID in (select distinct JFT.FILE_ID"
					+ "                       from JECN_FILE_T JFT" + "                      where JFT.IS_DIR = 1"
					+ "                     connect by prior JFT.FILE_ID = JFT.PER_FILE_ID"
					+ "                      START WITH JFT.FILE_ID in " + JecnCommonSql.getIds(rids) + ")";
		}
		return sql;
	}

	/**
	 * 文件删除审批验证*********************** 批量删除树文件节点验证文件树节点是否在制度文件中处于任务中（文件、制度文件）
	 * 
	 * @param rids
	 *            选中的树节点ID集合（包含叶子节点）
	 * @param nodeType
	 *            节点类型
	 * @return 拼装后的sql
	 */
	public static String getListRefIdsInRuleTask(List<Long> rids, TreeNodeType nodeType) {
		String sql = "";
		switch (nodeType) {
		case file:// 文件
			sql = getAllRuleTaskByChildFileId(rids);
			break;
		}
		return sql;
	}

	/**
	 * 文件删除发布状态验证*********************** 批量删除树文件节点验证文件树节点在制度文件中否处于发布状态（文件、制度文件）
	 * 
	 * @param rids
	 * @return
	 */
	private static String getAllRulePubByChildFile(List<Long> rids) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = " WITH MY_FILE AS(SELECT *" + "  FROM JECN_FILE_T JFT" + " WHERE JFT.FILE_ID in  "
					+ JecnCommonSql.getIds(rids) + " UNION ALL" + " SELECT JFS.*" + "  FROM MY_FILE"
					+ " INNER JOIN JECN_FILE_T JFS ON MY_FILE.FILE_ID = JFS.PER_FILE_ID)" + "  select count(JU.ID)"
					+ "    from MY_FILE, JECN_RULE JU" + "   WHERE MY_FILE.IS_DIR = 1" + "     and JU.IS_DIR = 2"
					+ "     and JU.FILE_ID = MY_FILE.FILE_ID";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "select count(f.FILE_ID)"
					+ "  from jecn_rule ju, JECN_FILE_t f"
					+ " where ju.file_id = f.file_id"
					+ "   and ju.is_dir = 2"// 制度类型：0：目录；1：制度模板文件；2：制度文件
					+ "   and f.FILE_ID in (select distinct JFT.FILE_ID"
					+ "                       from JECN_FILE_T JFT"
					+ "                      where JFT.IS_DIR = 1"// 文件类型：0：目录；1：目录
					+ "                     connect by prior JFT.FILE_ID = JFT.PER_FILE_ID"
					+ "                      START WITH JFT.FILE_ID in " + JecnCommonSql.getIds(rids) + ")";
		}
		return sql;
	}

	/**
	 * 文件删除发布状态验证*********************** 批量删除树文件节点验证文件树节点在制度文件中否处于发布状态（文件、制度文件）
	 * 
	 * @param rids
	 *            选中的树节点ID集合（包含叶子节点）
	 * @param nodeType
	 *            节点类型
	 * @return 拼装后的sql
	 */
	public static String getSqlExistRulePub(List<Long> rids, TreeNodeType nodeType) {
		String sql = "";
		switch (nodeType) {
		case file:// 文件
			sql = getAllRulePubByChildFile(rids);
			break;
		}
		return sql;
	}

	public static byte[] blobToBytes(Blob blob) throws Exception {
		if (blob == null) {
			return null;
		}
		SerializableBlob sb = (SerializableBlob) blob;
		Blob wrapBlob = sb.getWrappedBlob();
		byte[] content = new byte[(int) wrapBlob.length()];
		InputStream in = null;
		try {
			in = wrapBlob.getBinaryStream();
			in.read(content);
			return content;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		} finally {
			in.close();
			in = null;
		}
	}

	/**
	 * 删除合理化建议
	 * 
	 * @param relatedIds
	 * @param projectId
	 * @param type
	 *            0流程，1制度，2流程架构，3文件，4风险，5标准
	 * @param baseDao
	 */
	@SuppressWarnings("unchecked")
	public static void deleteProposeByRelatedIds(Set<Long> relatedIds, int type, IBaseDao baseDao) {
		// 删除合理化建议附件表
		String sql = "delete from JECN_RTNL_PROPOSE_FILE where PROPOSE_ID in (select id from jecn_rtnl_propose where  RELATION_TYPE = "
				+ type + " and RELATION_ID in";
		List<String> list = JecnCommonSql.getSetSqlAddBracket(sql, relatedIds);
		for (String str : list) {
			baseDao.execteNative(str);
		}
		// 删除制度相关的合理化建议
		sql = "delete from jecn_rtnl_propose where RELATION_TYPE = " + type + " and RELATION_ID in ";
		list = JecnCommonSql.getSetSqls(sql, relatedIds);
		for (String str : list) {
			baseDao.execteNative(str);
		}

		// 删除主题表
		sql = "DELETE FROM propose_topic WHERE TYPE = " + type + " AND RELATED_ID IN ";
		list = JecnCommonSql.getSetSqls(sql, relatedIds);
		for (String str : list) {
			baseDao.execteNative(str);
		}
	}

	/**
	 * 删除合理化建议
	 * 
	 * @param relatedIds
	 * @param projectId
	 * @param type
	 *            0流程，1制度，2流程架构，3文件，4风险，5标准
	 * @param baseDao
	 */
	@SuppressWarnings("unchecked")
	public static void deleteProposeByRelatedIds(List<Long> relatedIds, int type, IBaseDao baseDao) {
		// 删除合理化建议附件表
		String sql = "delete from JECN_RTNL_PROPOSE_FILE where PROPOSE_ID in (select id from jecn_rtnl_propose where  RELATION_TYPE = "
				+ type + " and RELATION_ID in";
		List<String> list = JecnCommonSql.getListSqlAddBracket(sql, relatedIds);
		for (String str : list) {
			baseDao.execteNative(str);
		}
		// 删除制度相关的合理化建议
		sql = "delete from jecn_rtnl_propose where RELATION_TYPE = " + type + " and RELATION_ID in ";
		list = JecnCommonSql.getListSqls(sql, relatedIds);
		for (String str : list) {
			baseDao.execteNative(str);
		}

		// 删除主题表
		sql = "DELETE FROM propose_topic WHERE TYPE = " + type + " AND RELATED_ID IN ";
		list = JecnCommonSql.getListSqls(sql, relatedIds);
		for (String str : list) {
			baseDao.execteNative(str);
		}
	}

	public static Map<Long, String> getNodeParentSqlByFlowRelateFiles(Long flowId, IJecnAbstractTaskDao abstractTaskDao) { // 流程关联文件
		String sql = "SELECT distinct PARENT.FILE_ID CUR_ID, PARENT.FILE_NAME FROM JECN_FILE_T T"
				+ "  LEFT JOIN JECN_FILE_T PARENT ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT")
				+ " WHERE T.FILE_ID IN (SELECT JF.FILE_ID FROM JECN_FILE_T JF WHERE JF.FLOW_ID = " + flowId
				+ " AND JF.IS_DIR=0)";
		List<Object[]> parentNames = abstractTaskDao.listNativeSql(sql);
		Map<Long, String> parentNamesMap = findParentMap(parentNames);
		return parentNamesMap;
	}

	private static Map<Long, String> findParentMap(List<Object[]> parentNames) {
		// 按流程分组 获取流程对应 父节点名称集合
		Map<Long, String> parentNameMap = new HashMap<Long, String>();

		if (parentNames == null || parentNames.size() == 0) {
			return null;
		}

		for (Object[] obj : parentNames) {
			if (obj[0] == null || obj[1] == null) {
				continue;
			}
			Long id = Long.valueOf(obj[0].toString());
			parentNameMap.put(id, obj[1].toString());
		}
		return parentNameMap;
	}

	public static Map<Long, String> getNodeParentSqlByRuleRelateFiles(Long ruleId, IJecnAbstractTaskDao abstractTaskDao) { // 制度关联文件
		String sql = "SELECT distinct PARENT.FILE_ID CUR_ID, PARENT.FILE_NAME FROM JECN_FILE_T T"
				+ "  LEFT JOIN JECN_FILE_T PARENT ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT")
				+ " WHERE T.FILE_ID IN (SELECT JF.FILE_ID FROM JECN_FILE_T JF WHERE JF.RULE_ID = " + ruleId
				+ " AND JF.IS_DIR=0)";
		List<Object[]> parentNames = abstractTaskDao.listNativeSql(sql);
		Map<Long, String> parentNamesMap = findParentMap(parentNames);
		return parentNamesMap;
	}

	/**
	 * 获取节点对应父级名称 集合
	 * 
	 * @param id
	 * @param nodeType
	 * @return
	 */
	public static String getNodeParentSql(List<Long> ids, TreeNodeType nodeType, String isPub) {
		String sql = null;
		switch (nodeType) {
		case process:
		case processMap:
			sql = "SELECT T.FLOW_ID CUR_ID, PARENT.FLOW_NAME FROM JECN_FLOW_STRUCTURE" + isPub + " T"
					+ "  LEFT JOIN JECN_FLOW_STRUCTURE" + isPub + " PARENT ON "
					+ JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT") + " WHERE T.FLOW_ID IN "
					+ JecnCommonSql.getIds(ids) + " ORDER BY PARENT.T_PATH";
			break;
		case standard:
			sql = "SELECT T.CRITERION_CLASS_ID CUR_ID, PARENT.CRITERION_CLASS_NAME FROM JECN_CRITERION_CLASSES T"
					+ "  LEFT JOIN JECN_CRITERION_CLASSES PARENT ON "
					+ JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT") + " WHERE T.CRITERION_CLASS_ID IN "
					+ JecnCommonSql.getIds(ids) + " ORDER BY PARENT.T_PATH";
			break;
		case file:
			sql = "SELECT T.FILE_ID CUR_ID, PARENT.FILE_NAME FROM JECN_FILE" + isPub + " T" + "  LEFT JOIN JECN_FILE"
					+ isPub + " PARENT ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT")
					+ " WHERE T.FILE_ID IN " + JecnCommonSql.getIds(ids) + " ORDER BY PARENT.T_PATH";
			break;
		case ruleFile:
			sql = "SELECT T.ID CUR_ID, PARENT.RULE_NAME FROM JECN_RULE" + isPub + " T" + "  LEFT JOIN JECN_RULE"
					+ isPub + " PARENT ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT") + " WHERE T.ID IN "
					+ JecnCommonSql.getIds(ids) + " ORDER BY PARENT.T_PATH";
			break;
		case organization:
			sql = "select  t.org_id CUR_ID," + "   PARENT.org_name " + " from jecn_flow_org  t "
					+ " left join jecn_flow_org PARENT on " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT")
					+ "where t.org_id in  " + JecnCommonSql.getIds(ids) + "  and t.del_state = 0 "
					+ " ORDER BY PARENT.T_PATH";
			break;
		case position:
			sql = "select pos.figure_id CUR_ID," + "   PARENT.org_name   from JECN_FLOW_ORG_IMAGE pos" + " inner join jecn_flow_org t"
					+ "    on t.org_id = pos.org_id" + "  left join jecn_flow_org PARENT" + "    on "
					+ JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT") + " where pos.figure_id in "
					+ JecnCommonSql.getIds(ids) + "   and t.del_state = 0" + " ORDER BY PARENT.T_PATH";
			break;
		default:
			break;
		}
		return sql;
	}

	@SuppressWarnings("unchecked")
	public static JecnUser isLoginByDomainName(String userName, IPersonDao personDao) throws Exception {
		String domainName = JecnContants.DOMAIN_SUFFIX;
		if (StringUtils.isBlank(domainName)) {
			log.error("设计器域名登录异常！ domainName = " + domainName);
			return null;
		}
		log.info("domainName = " + domainName);
		String sql = "SELECT T.* FROM JECN_USER T WHERE LOWER(T.EMAIL) =LOWER ('" + userName + domainName + "')";

		log.info("domainNameSQL = " + sql);
		List<JecnUser> list = personDao.getSession().createSQLQuery(sql).addEntity(JecnUser.class).list();
		if (list.size() == 0 || list.get(0) == null) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * 相关标准化文件 (制度)
	 * 
	 * @param ruleId
	 * @param ids
	 */
	public static void saveOrUpdateRuleRelatedStandardizedFiles(IRuleDao ruleDao, Long ruleId, List<Long> ids) {
		String hql = "from RuleStandardizedFileT where relatedId=?";
		List<RuleStandardizedFileT> listStandardizedFile = ruleDao.listHql(hql, ruleId);
		for (Long id : ids) {
			boolean isExist = false;
			for (RuleStandardizedFileT standardizedFile : listStandardizedFile) {
				if (id.equals(standardizedFile.getFileId())) {
					isExist = true;
					listStandardizedFile.remove(standardizedFile);
					break;
				}
			}
			if (!isExist) {
				RuleStandardizedFileT standardizedFile = new RuleStandardizedFileT();
				standardizedFile.setRelatedId(ruleId);
				standardizedFile.setFileId(id);
				standardizedFile.setId(JecnCommon.getUUID());
				ruleDao.getSession().save(standardizedFile);
			}
		}
		for (RuleStandardizedFileT standardizedFile : listStandardizedFile) {
			ruleDao.getSession().delete(standardizedFile);
		}

		ruleDao.getSession().flush();
	}

	/**
	 * 相关标准化文件 (流程)
	 * 
	 * @param ruleId
	 * @param ids
	 */
	public static void saveOrUpdateFlowRelatedStandardizedFiles(IFlowStructureDao flowStructureDao, Long flowId,
			List<Long> ids) {
		String hql = "from FlowStandardizedFileT where flow_id=?";
		List<FlowStandardizedFileT> listStandardizedFile = flowStructureDao.listHql(hql, flowId);
		for (Long id : ids) {
			boolean isExist = false;
			for (FlowStandardizedFileT standardizedFile : listStandardizedFile) {
				if (id.equals(standardizedFile.getFileId())) {
					isExist = true;
					listStandardizedFile.remove(standardizedFile);
					break;
				}
			}
			if (!isExist) {
				FlowStandardizedFileT standardizedFile = new FlowStandardizedFileT();
				standardizedFile.setFlowId(flowId);
				standardizedFile.setFileId(id);
				standardizedFile.setId(JecnCommon.getUUID());
				flowStructureDao.getSession().save(standardizedFile);
			}
		}
		for (FlowStandardizedFileT standardizedFile : listStandardizedFile) {
			flowStructureDao.getSession().delete(standardizedFile);
		}

		flowStructureDao.getSession().flush();
	}
}
