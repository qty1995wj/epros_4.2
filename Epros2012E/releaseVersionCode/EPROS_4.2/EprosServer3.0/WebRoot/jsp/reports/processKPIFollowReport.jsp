<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div" style="height: 99%;">
			<div id="search" class="divBorder" align="center">

				<table width="100%">
					<div align="right" style="margin-top: 4px">
						<!-- 开始日期: -->
						<s:text name="startTimeC" />
						<s:textfield readonly="true" cssClass="Wdate" id="startTime"
							cssStyle="border-color: #7F9DB9;" onclick="WdatePicker();"
							theme="simple" />
						<!-- 结束日期: -->
						<s:text name="endTimeC" />
						<s:textfield readonly="true" cssClass="Wdate" id="endTime"
							cssStyle="border-color: #7F9DB9;margin-right: 4px"
							onclick="WdatePicker();" theme="simple" />
					</div>

				</table>
				<table width="100%" style="margin-top: 4px" style="margin-left: 4px">
					<tr>
						<td align="left" width="100px">
							<s:text name="responsibilityDepartmentC"></s:text>
						</td>

						<td>
							<div style="width: 100%" id="select_organization_id">
								&nbsp;
							</div>
						</td>
						<td align="right" width="240px">
							<div align="right">
								<span
									onclick='orgPosPerMultipleSelectHasLimit("organization","orgDutyName_access_id","orgDutyId_access_id","select_organization_id",20)'><img
										style="vertical-align: right;cursor: pointer;"
										src="${basePath}images/common/select.gif" /> </span>
								<span onclick='closeAllOrganization("orgDutyName_access_id","orgDutyId_access_id","select_organization_id")'><img
										style="vertical-align: right; margin-right: 4px;cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>

					    </tr>
					    
						<tr style="margin-top: 4px">
						<td align="left" width="100px" style="margin-left: 4px">
							<s:text name="processMapC"></s:text>

						</td>
						<td>
						<div id="select_processMap_id" style="width: 100%">
						&nbsp;
						</div>
						</td>

						<td align="right" width="240px">
							<div align="right">
								<span
									onclick='processAndProcessMapSelectHasLimit("processMap","processMapName_access_id","processMapId_access_id","select_processMap_id",20)'><img
										style="vertical-align: left;cursor: pointer;"
										src="${basePath}images/common/select.gif" /> </span>
								<span onclick='closeAllProcessMap("processMapName_access_id","processMapId_access_id","select_processMap_id")'><img
										style="vertical-align: left; margin-right: 4px;cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>

					</tr>
				</table>
			</div>


			<div align="center" style="padding-top: 20px;">
				<a onclick="processKPIFollowReportDownloadExcel()"
					style="cursor: pointer; color: #005EA7;" class="table"><s:text
						name="downLoadExcel" /> </a>
			</div>
		</div>

		<div>
			<form action="" id="processKPIFollowForm" method="post">
				<!-- 责任部门ID -->
				<input name="dutyOrgIds" type="hidden" id="orgDutyId_access_id"
					value="-1" />
				<input name="dutyOrgNames" type="hidden" id="orgDutyName_access_id"
					value="" />
				<!-- 流程地图ID -->
				<input name="processMapIds" type="hidden"
					id="processMapId_access_id" value="-1" />
				<input name="processMapNames" type="hidden"
					id="processMapName_access_id" value="" />
				<!-- 开始时间 -->
				<input name="startTime" type="hidden" id="startTimeSub" value="" />
				<!-- 结束时间 -->
				<input name="endTime" type="hidden" id="endTimeSub" value="" />
			</form>
		</div>
	</body>
</html>