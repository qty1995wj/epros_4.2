package com.jecn.epros.server.bean.rule;

import java.util.List;

public class JecnRuleOperationCommon implements java.io.Serializable {
	/** 标题 */
	private RuleTitleT ruleTitleT;
	
	/** 内容 */
	private RuleContentT ruleContentT;
	/** 文件表单 */
	private List<RuleFileT> listFileBean;

	/** 内容 */
	private RuleContent ruleContent;
	/** 文件表单 */
	private List<RuleFile> listRuleFile;

	/** 流程表单 */
	private List<JecnFlowCommon> listJecnFlowCommon;
	/** 流程地图表单 */
	private List<JecnFlowCommon> listJecnFlowMapCommon;
	/** 标准表单 */
	private List<JecnStandarCommon> listJecnStandarCommon;
	/** 风险表单 */
	private List<JecnRiskCommon> listJecnRiskCommon;
	/** 制度表单 */
	private List<JecnRuleCommon> listJecnRuleCommon;
	/** 制度名称 */
	private String ruleName;
	/** 制度ID */
	private Long ruleId;
	/** 正式表和临时表 */
	private boolean showPub;
	
	
	

	public List<JecnRuleCommon> getListJecnRuleCommon() {
		return listJecnRuleCommon;
	}

	public void setListJecnRuleCommon(List<JecnRuleCommon> listJecnRuleCommon) {
		this.listJecnRuleCommon = listJecnRuleCommon;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	/**
	 * @return the ruleTitleT
	 */
	public RuleTitleT getRuleTitleT() {
		return ruleTitleT;
	}

	/**
	 * @param ruleTitleT
	 *            the ruleTitleT to set
	 */
	public void setRuleTitleT(RuleTitleT ruleTitleT) {
		this.ruleTitleT = ruleTitleT;
	}

	/**
	 * @return the ruleContentT
	 */
	public RuleContentT getRuleContentT() {
		return ruleContentT;
	}

	/**
	 * @param ruleContentT
	 *            the ruleContentT to set
	 */
	public void setRuleContentT(RuleContentT ruleContentT) {
		this.ruleContentT = ruleContentT;
	}

	/**
	 * @return the listFileBean
	 */
	public List<RuleFileT> getListFileBean() {
		return listFileBean;
	}

	/**
	 * @param listFileBean
	 *            the listFileBean to set
	 */
	public void setListFileBean(List<RuleFileT> listFileBean) {
		this.listFileBean = listFileBean;
	}

	/**
	 * @return the listJecnFlowCommon
	 */
	public List<JecnFlowCommon> getListJecnFlowCommon() {
		return listJecnFlowCommon;
	}

	/**
	 * @param listJecnFlowCommon
	 *            the listJecnFlowCommon to set
	 */
	public void setListJecnFlowCommon(List<JecnFlowCommon> listJecnFlowCommon) {
		this.listJecnFlowCommon = listJecnFlowCommon;
	}

	public List<RuleFile> getListRuleFile() {
		return listRuleFile;
	}

	public void setListRuleFile(List<RuleFile> listRuleFile) {
		this.listRuleFile = listRuleFile;
	}

	public RuleContent getRuleContent() {
		return ruleContent;
	}

	public void setRuleContent(RuleContent ruleContent) {
		this.ruleContent = ruleContent;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public List<JecnFlowCommon> getListJecnFlowMapCommon() {
		return listJecnFlowMapCommon;
	}

	public void setListJecnFlowMapCommon(
			List<JecnFlowCommon> listJecnFlowMapCommon) {
		this.listJecnFlowMapCommon = listJecnFlowMapCommon;
	}

	public List<JecnStandarCommon> getListJecnStandarCommon() {
		return listJecnStandarCommon;
	}
	

	public void setListJecnStandarCommon(
			List<JecnStandarCommon> listJecnStandarCommon) {
		this.listJecnStandarCommon = listJecnStandarCommon;
	}

	public List<JecnRiskCommon> getListJecnRiskCommon() {
		return listJecnRiskCommon;
	}

	public void setListJecnRiskCommon(List<JecnRiskCommon> listJecnRiskCommon) {
		this.listJecnRiskCommon = listJecnRiskCommon;
	}

	public boolean isShowPub() {
		return showPub;
	}

	public void setShowPub(boolean showPub) {
		this.showPub = showPub;
	}
}
