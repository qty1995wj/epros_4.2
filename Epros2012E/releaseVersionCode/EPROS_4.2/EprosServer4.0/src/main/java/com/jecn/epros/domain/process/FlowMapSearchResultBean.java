package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.util.ConfigUtils;

import java.util.Date;

public class FlowMapSearchResultBean {
    private Long flowId;
    private String flowName;
    private String flowNum;
    private Long pFlowId;
    private String pFlowName;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date pubTime;
    private int isPublic;

    public LinkResource getLink() {
        return new LinkResource(flowId, flowName, ResourceType.PROCESS);
    }

    public LinkResource getParentLink() {
        return new LinkResource(pFlowId, pFlowName, ResourceType.PROCESS_MAP);
    }

    @JsonIgnore
    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    @JsonIgnore
    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    @JsonIgnore
    public Long getpFlowId() {
        return pFlowId;
    }

    public void setpFlowId(Long pFlowId) {
        this.pFlowId = pFlowId;
    }

    @JsonIgnore
    public String getpFlowName() {
        return pFlowName;
    }

    public void setpFlowName(String pFlowName) {
        this.pFlowName = pFlowName;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public String getPublicStr() {
        return ConfigUtils.getSecretByType(isPublic);
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public String getFlowNum() {
        return flowNum;
    }

    public void setFlowNum(String flowNum) {
        this.flowNum = flowNum;
    }
}
