package epros.designer.gui.popedom.person;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.jecn.epros.server.bean.popedom.JecnUser;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

/**
 * 
 * 显示登录人信息面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPersonInfoPanel extends JecnPanel implements MouseListener {
	/** 个人信息标签 */
	private JLabel loginNameLabel = null;
	/** 默认字体颜色 */
	private Color defaultColor = new Color(11, 114, 164);

	public JecnPersonInfoPanel() {
		initComponents();
		initLayout();
		initData();
	}

	private void initComponents() {
		// 个人信息标签
		loginNameLabel = new JLabel();

		loginNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		loginNameLabel.setVerticalAlignment(SwingConstants.CENTER);
		loginNameLabel.setHorizontalTextPosition(SwingConstants.LEFT);

		loginNameLabel.setForeground(defaultColor);
		loginNameLabel.addMouseListener(this);

		this.setOpaque(false);
		loginNameLabel.setOpaque(false);
	}

	private void initLayout() {
		Insets insetsRight = new Insets(5, 5, 5, 30);
		// 个人信息标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST,
				GridBagConstraints.BOTH, insetsRight, 0, 0);
		this.add(new JLabel(), c);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				insetsRight, 0, 0);
		this.add(loginNameLabel, c);
	}

	public void initData() {
		JecnUser user = JecnConstants.loginBean.getJecnUser();
		loginNameLabel.setText(JecnProperties.getValue("personInfoWelcome") + "[" + getTrueName(user) + "]"
				+ JecnProperties.getValue("personInfoLogin"));
	}

	private String getTrueName(JecnUser user) {
		if(!JecnConfigTool.isIflytekLogin()){
			return user.getTrueName();
		}
		return ConnectionPool.getPersonAction().getListUserSearchBean(user.getLoginName());
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		JecnPersonInfoDialog personInfoDialog = new JecnPersonInfoDialog();
		personInfoDialog.setVisible(true);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		loginNameLabel.setForeground(Color.RED);
		this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		loginNameLabel.setForeground(defaultColor);
		this.setCursor(Cursor.getDefaultCursor());
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}
}
