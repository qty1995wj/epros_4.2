package epros.designer.util;

import com.jecn.epros.server.bean.process.JecnActivityFileT;
import com.jecn.epros.server.bean.process.JecnFlowStationT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnModeFileT;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.bean.process.ProcessFigureData;
import com.jecn.epros.server.bean.process.ProcessOpenData;

import epros.designer.service.process.JecnProcess;
import epros.draw.gui.workflow.JecnDrawMainPanel;

public class JecnProcessUtil {

	/**
	 * 添加流程模板
	 * 
	 * @param flowModeId
	 * @param jecnFlowStructureT
	 * @param isXorY
	 * @throws Exception
	 */
	public static void initWorkflowByModeId(Long flowModeId, JecnFlowStructureT jecnFlowStructureT) throws Exception {
		if (flowModeId != null) {
			ProcessOpenData processOpenData = getProcessOpenData(flowModeId, jecnFlowStructureT);
			JecnProcess.openJecnProcessMap(processOpenData, true);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSaveTrue();
		}
	}

	private static ProcessOpenData getProcessOpenData(Long flowModeId, JecnFlowStructureT jecnFlowStructureT)
			throws Exception {
		ProcessOpenData processOpenData = ConnectionPool.getProcessModeAction().getProcessModeData(flowModeId);
		processOpenData.setJecnFlowStructureT(jecnFlowStructureT);
		for (ProcessFigureData processFigureData : processOpenData.getProcessFigureDataList()) {
			if (processFigureData.getJecnFlowStructureImageT().getLinkFlowId() != null) {
				processFigureData.getJecnFlowStructureImageT().setLinkFlowId(null);
			}
			processFigureData.getJecnFlowStructureImageT().setFlowId(jecnFlowStructureT.getFlowId());
			if (processFigureData.getListRolePosition() != null) {
				for (JecnFlowStationT JecnFlowStationT : processFigureData.getListRolePosition()) {
					JecnFlowStationT.setFlowStationId(null);
				}
			}

			if (processFigureData.getListModeFileT() != null) {
				for (JecnModeFileT modeFileT : processFigureData.getListModeFileT()) {
					modeFileT.setModeFileId(null);
				}
			}

			if (processFigureData.getListJecnActivityFileT() != null) {
				for (JecnActivityFileT activityFileT : processFigureData.getListJecnActivityFileT()) {
					activityFileT.setFileId(null);
				}
			}

			if (processFigureData.getListRefIndicatorsT() != null) {
				for (JecnRefIndicatorsT refIndicatorsT : processFigureData.getListRefIndicatorsT()) {
					refIndicatorsT.setId(null);
				}
			}
		}
		return processOpenData;
	}

	/**
	 * 重置流程图
	 * 
	 * @param flowModeId
	 * @param jecnFlowStructureT
	 * @throws Exception
	 */
	public static void resetWorkflowByModeId(Long flowModeId, JecnFlowStructureT jecnFlowStructureT) throws Exception {
		if (flowModeId != null) {
			ProcessOpenData processOpenData = getProcessOpenData(flowModeId, jecnFlowStructureT);
			JecnProcess.setProcessMapData(processOpenData, true);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSaveTrue();
		}
	}
}
