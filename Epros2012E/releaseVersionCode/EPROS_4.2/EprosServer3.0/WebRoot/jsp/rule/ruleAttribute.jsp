<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div">
			<div id="ruleAttribute"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; float: left; width: 100%">
				<table align="left" width="100%" style="padding-left: 0px;">
					<tr>
						<td width="100px;" class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 制度名称： -->
							<s:text name="ruleNameC" />
						</td>
						<td style="padding-left: 0px;"
							class="td_dashed" align="left">
							<s:if test="ruleInfoBean.ruleName==null || ruleInfoBean.ruleName==''">
							     <s:text name="none"></s:text>
							</s:if>
							<s:else>
							  ${ruleInfoBean.ruleName}
							</s:else>
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 制度编号： -->
							<s:text name="ruleNumberC" />
						</td>
						<td style="padding-left: 0px;"
							class="td_dashed">
							<s:if test="ruleInfoBean.ruleNum==null||ruleInfoBean.ruleNum==''">
							     <s:text name="none"></s:text>
							</s:if>
							<s:else>
							  ${ruleInfoBean.ruleNum}
							</s:else>
						</td>
					</tr>
					<s:if test="#request.ruleTypeIsShow">
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 制度类别： -->
							<s:text name="ruleTypeC" />
						</td>
						<td style="padding-left: 0px;"
							class="td_dashed">
							<s:if test="ruleInfoBean.ruleTypeName==null || ruleInfoBean.ruleTypeName=='' ">
							     <s:text name="none"></s:text>
							</s:if>
							<s:else>
							   <s:property value="ruleInfoBean.ruleTypeName" />
							</s:else>
						</td>
					</tr>
					</s:if>
					<tr>
						<td class="td_dashed" >
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 制度责任人： -->
							<s:text name="ruleResponsibleNameC" />
						</td>
						<td style="padding-left: 0px;"
							class="td_dashed">
							<s:if test="ruleInfoBean.responsibleName == null||ruleInfoBean.responsibleName == ''">
							     <s:text name="none"></s:text>
							</s:if>
							<s:else>
							<s:property value='ruleInfoBean.responsibleName'/>
							</s:else>
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 责任部门： -->
							<s:text name="responsibilityDepartmentC" />
						</td>
						<td style="padding-left: 0px;"
							class="td_dashed">
							<s:if test="ruleInfoBean.dutyOrg == null||ruleInfoBean.dutyOrg==''">
							     <s:text name="none"></s:text>
							</s:if>
							<s:else>
							<a
								href="organization.action?orgId=<s:property value='ruleInfoBean.dutyOrgId'/>"
								target='_blank' style="cursor: pointer;" class="table"> <s:property
									value="ruleInfoBean.dutyOrg" /> </a>
						    </s:else>
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 密级： -->
							<s:text name="intensiveC" />
						</td>
						<td style="padding-left: 0px;"
							class="td_dashed">
							<s:if test="ruleInfoBean.secretLevel== 1">
							    <s:property value="#application.public"/>
							</s:if>
							<s:elseif test="ruleInfoBean.secretLevel == 0">
								${secret}
							</s:elseif>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>