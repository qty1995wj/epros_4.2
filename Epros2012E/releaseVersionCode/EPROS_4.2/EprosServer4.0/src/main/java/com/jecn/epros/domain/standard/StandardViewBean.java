package com.jecn.epros.domain.standard;

import java.util.List;

public class StandardViewBean {
    private int total;
    private List<SearchStandardResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SearchStandardResultBean> getData() {
        return data;
    }

    public void setData(List<SearchStandardResultBean> data) {
        this.data = data;
    }
}
