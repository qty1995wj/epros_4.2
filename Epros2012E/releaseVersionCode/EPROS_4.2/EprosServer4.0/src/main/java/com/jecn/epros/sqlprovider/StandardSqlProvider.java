package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.standard.SearchStandardBean;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.Contants;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StandardSqlProvider extends BaseSqlProvider {
    /**
     * 树展开
     *
     * @param map
     * @return
     */
    public String findChildStandards(Map<String, Object> map) {
        Long pId = Long.valueOf(map.get("pid").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "";
        sql += "select distinct t.criterion_class_id, t.criterion_class_name,"
                + " t.pre_criterion_class_id,t.stan_type, t.related_id,"
                + " case when jc.criterion_class_id is null then 0 else 1 end as count,"
                + " t.sort_id,t.pre_criterion_class_id,t.sort_id from jecn_criterion_classes t"
                + " left join jecn_criterion_classes jc on jc.pre_criterion_class_id=t.criterion_class_id";
        sql += " where t.pre_criterion_class_id = " + pId + "   and t.project_id = " + projectId;
        sql += " order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
        return sql;
    }

    /**
     * 标准相关流程
     *
     * @param standardId
     * @return
     */
    public String findRelatedFlows(Long standardId) {
        return "SELECT JFS.FLOW_ID," + "       JFS.FLOW_NAME," + "       JFS.FLOW_ID_INPUT," + "       JFO.ORG_NAME,"
                + "       CASE WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN JU.TRUE_NAME"
                + "       WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN" + "          JFOI.FIGURE_TEXT"
                + "       END AS RES_PEOPLE" + "  FROM (SELECT JFS.FLOW_ID, JFS.FLOW_NAME, JFS.FLOW_ID_INPUT"
                + "          FROM JECN_FLOW_STRUCTURE JFS"
                + "         INNER JOIN JECN_FLOW_STANDARD SF ON JFS.FLOW_ID = SF.FLOW_ID AND JFS.DEL_STATE = 0"
                + "                                         AND SF.CRITERION_CLASS_ID = " + standardId + "        UNION"
                + "        SELECT FS.FLOW_ID, FS.FLOW_NAME, FS.FLOW_ID_INPUT"
                + "          FROM JECN_ACTIVE_STANDARD AST"
                + "          INNER JOIN JECN_FLOW_STRUCTURE_IMAGE FSI ON AST.ACTIVE_ID = FSI.FIGURE_ID"
                + "          INNER JOIN JECN_FLOW_STRUCTURE FS ON FSI.FLOW_ID = FS.FLOW_ID AND FS.DEL_STATE=0"
                + "          WHERE AST.STANDARD_ID = " + standardId + ") JFS," + "       JECN_FLOW_BASIC_INFO JFBI"
                + "  LEFT JOIN JECN_FLOW_RELATED_ORG JFRO ON  JFRO.FLOW_ID = JFBI.FLOW_ID"
                + "  LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID= JFRO.ORG_ID AND JFO.DEL_STATE=0"
                + "  LEFT JOIN JECN_USER JU ON JFBI.TYPE_RES_PEOPLE=0 AND JU.ISLOCK = 0  AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID"
                + "  LEFT JOIN  JECN_FLOW_ORG_IMAGE JFOI ON JFBI.TYPE_RES_PEOPLE=1 AND JFOI.FIGURE_ID= JFBI.RES_PEOPLE_ID"
                + "  WHERE JFS.FLOW_ID = JFBI.FLOW_ID";
    }

    /**
     * 相关制度
     *
     * @param standardId
     * @return
     */
    public String findRelatedRules(Long standardId) {
        return "select r.id, r.rule_name," +
                "        case when r.is_dir=2 and r.is_file_local=0 then jf.doc_id else r.rule_number END as rule_code" +
                "        ,o.org_name," +
                "        case when  r.TYPE_RES_PEOPLE=0 then ju.true_name" +
                "                when r.type_res_people=1 then jfoi.figure_text         END AS RES_PEOPLE" +
                "           from JECN_RULE_STANDARD rs ,JECN_RULE r " +
                "           left join JECN_FLOW_ORG o on r.org_id = o.org_id and o.del_state=0" +
                "           LEFT JOIN JECN_USER JU ON r.TYPE_RES_PEOPLE=0 AND JU.ISLOCK = 0  AND JU.PEOPLE_ID = r.RES_PEOPLE_ID" +
                "           LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON r.TYPE_RES_PEOPLE=1 AND JFOI.FIGURE_ID= r.RES_PEOPLE_ID" +
                "           LEFT JOIN JECN_FILE JF ON r.is_dir=2 and r.is_file_local=0 and JF.FILE_ID= r.File_Id AND JF.DEL_STATE = 0" +
                "           where rs.rule_id = r.id and r.del_state=0 and rs.standard_id =" + standardId;
    }

    /**
     * 相关文件
     *
     * @return
     */
    public String findRelatedFiles(Long standardId) {
        return "	select f.version_id as id,f.file_name as name from FILE_RELATED_STANDARD a" +
                "	INNER JOIN jecn_file f on a.file_id=f.file_id and f.del_state=0" +
                "	inner join jecn_criterion_classes jcc on jcc.criterion_class_id=a.standard_id" +
                "   where jcc.criterion_class_id=" + standardId;
    }

    /**
     * 获得标准搜索的Sql
     *
     * @param map
     * @return
     */
    public String searchStandard(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchStandardBean searchBean = (SearchStandardBean) map.get("searchStandardBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        String sql = searchStandardSQLCommon(projectId, searchBean);
        sql = "select  t.* from (" + sql + ") T";
        if (!isAdmin && !Contants.isCriterionAdmin) {
            sql = sql + "  inner join "
                    + CommonSqlTPath.INSTANCE.getFileSearch(map)
                    + " on MY_AUTH_FILES.criterion_class_id=t.criterion_class_id";
        }
        return sql;
    }


    /**
     * 获得标准搜索总数的Sql
     *
     * @param map
     * @return
     */
    public String searchStandardTotal(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchStandardBean searchBean = (SearchStandardBean) map.get("searchStandardBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        String sql = searchStandardSQLCommon(projectId, searchBean);
        sql = "select  count(t.criterion_class_id) from (" + sql + ") T";
        if (!isAdmin && !Contants.isCriterionAdmin) {
            map.put("typeAuth", AuthSqlConstant.TYPEAUTH.STANDARD);
            sql = sql + "  inner join "
                    + CommonSqlTPath.INSTANCE.getFileSearch(map)
                    + " on MY_AUTH_FILES.criterion_class_id=t.criterion_class_id";
        }
        return sql;
    }

    /**
     * 标准搜索通用sql
     *
     * @param projectId
     * @return
     */
    private String searchStandardSQLCommon(Long projectId, SearchStandardBean searchBean) {
        String sql = "select distinct ju.criterion_class_id," + "  ju.criterion_class_name,ju.stan_type,JU.RELATED_ID"
                + " from jecn_criterion_classes ju " + " where ju.stan_type<>0  and ju.project_id=" + projectId;
        // 标准名称
        if (StringUtils.isNotBlank(searchBean.getName())) {
            sql = sql + " and ju.criterion_class_name like '%" + searchBean.getName() + "%'";
        }
        // 密级
        if (searchBean.getStandType() != -1) {
            sql = sql + " and ju.stan_type=" + searchBean.getStandType();
        }
        return sql;
    }

    public String fetchInventory(Map<String, Object> paramMap) {
        String sql = getStandards(paramMap);
        sql += " SELECT" +
                "	BZ.T_LEVEL," +
                "	BZ.T_PATH," +
                "	BZ.CRITERION_CLASS_ID," +
                "	BZ.CRITERION_CLASS_NAME," +
                "	BZ.STAN_CONTENT," +
                "	BZ.STAN_TYPE," +
                "	BZ.PRE_CRITERION_CLASS_ID," +
                "	BZ.SORT_ID," +
                "	BZ.RELATED_ID," +
                "   BZZ.relatedId," +
                "   BZZ.relatedName," +
                "   BZZ.relatedType" +
                " FROM" +
                "	BZ" +
                " left join (" +
                " SELECT DISTINCT" +
                "  BZ.CRITERION_CLASS_ID AS standardId," +
                "	FT.FLOW_ID AS relatedId," +
                "	FT.FLOW_NAME as relatedName," +
                "  1 as relatedType" +
                " FROM" +
                "	BZ" +
                " INNER JOIN JECN_FLOW_STANDARD FST ON BZ.CRITERION_CLASS_ID = FST.CRITERION_CLASS_ID" +
                " INNER JOIN JECN_FLOW_STRUCTURE FT ON FST.FLOW_ID = FT.FLOW_ID AND FT.DEL_STATE = 0 " +
                " union " +
                "SELECT DISTINCT" +
                "  BZ.CRITERION_CLASS_ID AS standardId," +
                "	FIT.FIGURE_ID AS relatedId," +
                "	FIT.FIGURE_TEXT as relatedName," +
                "  3 as relatedType" +
                " FROM" +
                "	BZ" +
                " INNER JOIN JECN_ACTIVE_STANDARD AST ON BZ.CRITERION_CLASS_ID = AST.STANDARD_ID" +
                " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE FIT ON AST.ACTIVE_ID = FIT.FIGURE_ID " +
                " INNER JOIN JECN_FLOW_STRUCTURE JY ON JY.FLOW_ID = FIT.FLOW_ID AND JY.DEL_STATE = 0 " +
                " INNER JOIN JECN_RULE_STANDARD RST ON BZ.CRITERION_CLASS_ID = RST.STANDARD_ID" +
                " union " +
                "SELECT DISTINCT" +
                "  BZ.CRITERION_CLASS_ID AS standardId," +
                "	RT.ID AS relatedId," +
                "	RT.RULE_NAME AS relatedName," +
                "  2 as relatedType" +
                " FROM" +
                "	BZ" +
                " INNER JOIN JECN_RULE_STANDARD RST ON BZ.CRITERION_CLASS_ID = RST.STANDARD_ID" +
                " INNER JOIN JECN_RULE RT ON RT.ID = RST.RULE_ID AND RT.DEL_STATE=0" +
                " union " +
                "SELECT DISTINCT" +
                "  BZ.CRITERION_CLASS_ID AS standardId," +
                "	RT.FILE_ID AS relatedId," +
                "	RT.FILE_NAME AS relatedName," +
                "  4 as relatedType" +
                " FROM" +
                "	BZ" +
                " INNER JOIN FILE_RELATED_STANDARD RST ON BZ.CRITERION_CLASS_ID = RST.STANDARD_ID" +
                " INNER JOIN JECN_FILE RT ON RT.FILE_ID = RST.FILE_ID " +
                " )BZZ ON BZZ.standardId=BZ.CRITERION_CLASS_ID" +
                " ORDER BY" +
                "	BZ.VIEW_SORT";
        return sql;
    }

    public String fetchRelatedFlow(Map<String, Object> paramMap) {
        String sql = getStandards(paramMap);
        sql += " SELECT DISTINCT" +
                "  BZ.CRITERION_CLASS_ID AS standardId," +
                "	FT.FLOW_ID AS relatedId," +
                "	FT.FLOW_NAME as relatedName," +
                "  1 as relatedType" +
                " FROM" +
                "	BZ" +
                " INNER JOIN JECN_FLOW_STANDARD FST ON BZ.CRITERION_CLASS_ID = FST.CRITERION_CLASS_ID" +
                " INNER JOIN JECN_FLOW_STRUCTURE FT ON FST.FLOW_ID = FT.FLOW_ID";
        return sql;
    }

    public String fetchRelatedActivity(Map<String, Object> paramMap) {
        String sql = getStandards(paramMap);
        sql += "SELECT DISTINCT" +
                "  BZ.CRITERION_CLASS_ID AS standardId," +
                "	FIT.FIGURE_ID AS relatedId," +
                "	FIT.FIGURE_TEXT as relatedName," +
                "  3 as relatedType" +
                " FROM" +
                "	BZ" +
                " INNER JOIN JECN_ACTIVE_STANDARD AST ON BZ.CRITERION_CLASS_ID = AST.STANDARD_ID" +
                " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE FIT ON AST.ACTIVE_ID = FIT.FIGURE_ID" +
                " INNER JOIN JECN_RULE_STANDARD RST ON BZ.CRITERION_CLASS_ID = RST.STANDARD_ID";
        return sql;
    }

    public String fetchRelatedRule(Map<String, Object> paramMap) {
        String sql = getStandards(paramMap);
        sql += "SELECT DISTINCT" +
                "  BZ.CRITERION_CLASS_ID AS standardId," +
                "	RT.ID AS relatedId," +
                "	RT.RULE_NAME AS relatedName," +
                "  2 as relatedType" +
                " FROM" +
                "	BZ" +
                " INNER JOIN JECN_RULE_STANDARD RST ON BZ.CRITERION_CLASS_ID = RST.STANDARD_ID" +
                " INNER JOIN JECN_RULE RT ON RT.ID = RST.RULE_ID";
        return sql;
    }

    private String getStandards(Map<String, Object> paramMap) {
        Long id = (Long) paramMap.get("id");
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(id);
        String sql = "WITH BZ AS(" +
                InventorySqlProvider.getStandards(paramMap)
                + " ) ";
        return sql;
    }

    public String fetchInventoryParent(Map<String, Object> map) {
        String sql = "select p.criterion_class_id,p.pre_criterion_class_id,p.stan_type,p.criterion_class_name,p.t_level " +
                "	from JECN_CRITERION_CLASSES c" +
                "	inner join JECN_CRITERION_CLASSES p on c.criterion_class_id=#{id} and c.criterion_class_id<>p.criterion_class_id and c.t_path is not null and p.t_path is not null and c.t_path like p.t_path" + BaseSqlProvider.getConcatChar() + "'%'";
        return sql;
    }

    /**
     * 获取节点的父节点名称
     *
     * @param paramMap
     * @return
     */
    public String getPathNameByIdsSql(Map<String, Object> paramMap) {
        Set<Long> ids = (Set<Long>) paramMap.get("ids");
        String sql = "SELECT JCC.CRITERION_CLASS_ID, T.CRITERION_CLASS_NAME" +
                "  FROM jecn_criterion_classes JCC" +
                "  LEFT JOIN jecn_criterion_classes T" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("JCC") +
                "  AND JCC.CRITERION_CLASS_ID <> T.CRITERION_CLASS_ID" +
                " WHERE JCC.CRITERION_CLASS_ID IN " + SqlCommon.getIdsSet(ids) +
                " ORDER BY JCC.CRITERION_CLASS_ID, T.T_PATH";
        return sql;
    }

    public String getFileContentsSql(Long standardId) {
        return "SELECT T.ID id, T.FILE_NAME name FROM STANDARD_FILE_CONTENT T WHERE T.RELATED_ID = " + standardId;
    }
}
