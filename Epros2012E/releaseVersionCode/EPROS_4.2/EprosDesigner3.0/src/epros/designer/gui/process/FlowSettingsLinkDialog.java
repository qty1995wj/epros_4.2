package epros.designer.gui.process;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.process.mode.choose.FlowModeChooseDialog;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.event.JecnFileActionProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/****
 * 设置链接
 * 
 * @author Administrator 2012-11-14
 */
public class FlowSettingsLinkDialog extends JecnDialog {
	private Logger log = Logger.getLogger(FlowSettingsLinkDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/*** 单选按钮面板 **/
	private JecnPanel radButPanel = new JecnPanel();

	/*** 元素名称Lab */
	private JLabel elementNameLab = new JLabel(JecnProperties.getValue("elementNameC"));

	/*** 元素名称Filed */
	private JTextField elementNameField = new JTextField();

	/*** 元素名称按钮 */
	private JButton elementNameBut = new JButton(JecnProperties.getValue("selectBtn"));

	/*** 目录名Lab */
	private JLabel dirLab = new JLabel(JecnProperties.getValue("dirNameC"));

	/*** 目录名Filed */
	private JTextField dirField = new JTextField();

	/*** 目录名按钮 */
	private JButton dirBut = new JButton(JecnProperties.getValue("selectBtn"));
	/** 错误提示信息 */
	private JLabel tipText = new JLabel();
	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/*** 清空 */
	// private JButton clearBut = new
	// JButton(JecnProperties.getValue("emptyBtn"));

	/*** 创建流程地图单选按钮 */
	private JRadioButton flowMapRadBut = new JRadioButton(JecnProperties.getValue("createProcessMap"));
	/*** 创建流程单选按钮 */
	private JRadioButton flowRadBut = new JRadioButton(JecnProperties.getValue("createProcess"));
	/*** 选择节点单选按钮 */
	private JRadioButton nodeRadBut = new JRadioButton(JecnProperties.getValue("choseNode"));
	/** 0:创建流程地图1：创建流程图：2：选择节点 */
	private int selectRadBut = 0;
	private int widthNum = 1;
	private int dirNum = 1;
	/** 父节点ID */
	private Long parentId = null;
	/** 关联流程ID */
	private Long relationId = null;
	private JecnTreeNode selectNode;
	private JecnBaseFigurePanel elementPanel;
	private MapType mapType;

	private String name = "";
	private String num = "";

	public FlowSettingsLinkDialog(JecnTreeNode selectNode, JecnBaseFigurePanel elementPanel, MapType mapType) {
		this.selectNode = selectNode;
		this.parentId = selectNode.getJecnTreeBean().getId();
		this.elementPanel = elementPanel;
		elementNameField.setText(elementPanel.getFlowElementData().getFigureText());
		this.mapType = mapType;
		this.dirField.setText(selectNode.getJecnTreeBean().getName());
		this.setSize(500, 200);
		this.setTitle(JecnProperties.getValue("setLink"));
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);
		// 提示信息设为红色字体
		tipText.setForeground(Color.red);
		/** 单选 按钮实现 单选效果 */
		ButtonGroup bgp = new ButtonGroup();
		bgp.add(flowMapRadBut);
		bgp.add(flowRadBut);
		bgp.add(nodeRadBut);
		// 默认横向按钮选中
		flowMapRadBut.setSelected(true);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		radButPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		flowMapRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		flowRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		nodeRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		elementNameBut.setVisible(false);
		dirBut.setVisible(false);
		dirField.setEditable(false);
		if (flowMapRadBut.isSelected()) {
			selectRadBut = 0;
		} else if (flowRadBut.isSelected()) {
			selectRadBut = 1;
		} else if (nodeRadBut.isSelected()) {
			selectRadBut = 2;
		}
		flowAct();
		flowMapRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectRadBut = 0;
				flowAct();

			}
		});
		flowRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectRadBut = 1;
				flowAct();
			}
		});

		nodeRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectRadBut = 2;
				flowAct();
			}
		});
		elementNameBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				elementNameButPerformed();
			}
		});
		dirBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dirButPerformed();
			}
		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 确定

		okBut.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getDesignerData().getModeType() != ModeType.none) {
					relationMode();
				} else {
					okButPerformed();
				}
			}
		});

		initLayout();
	}

	private void flowAct() {
		if (selectRadBut == 1 || selectRadBut == 0) {
			widthNum = 2;
			dirNum = 1;
			elementNameBut.setVisible(false);
			dirBut.setVisible(true);
		} else {
			widthNum = 1;
			dirNum = 2;
			elementNameBut.setVisible(true);
			dirBut.setVisible(false);
		}
		initLayout();
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		// 元素名称

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(elementNameLab, c);
		c = new GridBagConstraints(1, 0, widthNum, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(elementNameField, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(elementNameBut, c);

		// 目录名
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(dirLab, c);
		c = new GridBagConstraints(1, 1, dirNum, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(dirField, c);
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(dirBut, c);
		// 单选按钮
		c = new GridBagConstraints(1, 2, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		infoPanel.add(radButPanel, c);
		radButPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		radButPanel.add(flowMapRadBut, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		radButPanel.add(flowRadBut, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		radButPanel.add(nodeRadBut, c);
		// 按钮
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(tipText);
		buttonPanel.add(okBut, c);
		// buttonPanel.add(clearBut, c);
		buttonPanel.add(cancelBut, c);
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 10.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(new JLabel(), c);

		this.getContentPane().add(mainPanel);
	}

	private void elementNameButPerformed() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		JecnSelectChooseDialog d = null;
		if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getDesignerData().getModeType() != ModeType.none) {
			d = new FlowModeChooseDialog(list, 18);
		} else {
			d = new ProcessChooseDialog(list);
		}
		d.setSelectMutil(false);
		d.setVisible(true);
		if (d.isOperation()) {
			if (list.size() == 1) {
				JecnTreeBean treeBean = list.get(0);
				relationId = treeBean.getId();
				elementNameField.setText(treeBean.getName());
				this.name = treeBean.getName();
				this.num = treeBean.getNumberId();
			}
		}
	}

	private void dirButPerformed() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		list.add(selectNode.getJecnTreeBean());
		JecnSelectChooseDialog d = null;
		if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getDesignerData().getModeType() != ModeType.none) {
			d = new FlowModeChooseDialog(list, 18);
		} else {
			// 流程地图设置连接
			d = new LinkProcessChooseDialog(list);
		}
		d.removeEmptyBtn();
		d.setSelectMutil(false);
		d.setVisible(true);
		if (d.isOperation()) {
			if (list.size() == 1) {
				JecnTreeBean treeBean = list.get(0);
				parentId = treeBean.getId();
				dirField.setText(treeBean.getName());
				try {
					selectNode = JecnTreeCommon.searchNode(treeBean.getId(), treeBean.getTreeNodeType(),
							JecnDesignerMainPanel.getDesignerMainPanel().getTree());
				} catch (Exception e) {
					log.error("", e);
				}
			}
		}
	}

	/***
	 * 取消
	 */
	private void cancelButPerformed() {
		// 关闭窗体
		this.dispose();
	}

	/**
	 * 记录撤销操作
	 * 
	 * @param undoData
	 * @param redoData
	 */
	private void unredoProcess(JecnUndoRedoData undoData, JecnUndoRedoData redoData) {
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		JecnDrawMainPanel.getMainPanel().getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
	}
	/***
	 * 确定
	 * 
	 * @param args
	 */
	private void okButPerformed() {
		// 操作前数据
		 JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		 JecnUndoRedoData redoData = new JecnUndoRedoData();
		String name = elementNameField.getText();
		if (name != null) {
			name = name.trim();
		}
		// 验证名称是否正确
		if (!JecnValidateCommon.validateName(name, tipText)) {
			return;
		}
		undoData.recodeFlowElement(elementPanel);// 记录撤销操作
		Long oldLinkId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
		if (selectRadBut == 2) {// 关联流程
			// 如果没有设置链接直接关闭返回
			if (relationId == null) {
				this.dispose();
				return;
			}
			// 元素增加链接ID
			elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(relationId);
			elementPanel.getFlowElementData().setFigureText(JecnTreeCommon.getShowName(name, num));
			elementPanel.updateUI();
			JecnDesignerCommon.addElemLink(elementPanel, mapType);
		} else {
			try {
				// 判断是否重名
				if (ConnectionPool.getProcessAction().validateAddName(name, parentId, selectRadBut,
						JecnConstants.projectId)) {
					// 名称已存在
					tipText.setText(JecnProperties.getValue("nameHaved"));
					return;
				}
				// 获取父节点的密级，部门权限，岗位权限
				JecnFlowStructureT jecnFlowStructureT = null;

				LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(parentId,
						0);
				// 岗位权限
				String posIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosList());
				// 组织权限
				String deptIds = JecnDesignerCommon.getLook(lookPopedomBean.getOrgList());
				// 岗位组权限
				String posGroupIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosList());

				if (selectRadBut == 0) {// 创建流程地图
					if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("processNoMap"));
						return;
					}
					jecnFlowStructureT = JecnProcessCommon.createProcessMap(name, lookPopedomBean.getIsPublic(),
							selectNode, "", new JecnMainFlowT(), posIds, deptIds, JecnDesignerMainPanel
									.getDesignerMainPanel().getTree(), posGroupIds);
				} else if (selectRadBut == 1) {// 创建流程图
					// 流程图基本信息表
					// if (selectNode.getJecnTreeBean().getPid() == 0) {
					// JecnOptionPane.showMessageDialog(this,
					// JecnProperties.getValue("level0CannotProcess"));
					// return;
					// }
					JecnFlowBasicInfoT jecnFlowBasicInfoT = new JecnFlowBasicInfoT();
					jecnFlowBasicInfoT.setFlowName(name);
					jecnFlowBasicInfoT.setIsXorY(0L);
					jecnFlowStructureT = JecnProcessCommon.createProcess(name, "", new Long(lookPopedomBean
							.getIsPublic()), deptIds, posIds, "", selectNode, jecnFlowBasicInfoT, JecnDesignerMainPanel
							.getDesignerMainPanel().getTree(), posGroupIds);
				}

				// 元素增加链接ID
				elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(jecnFlowStructureT.getFlowId());
				elementPanel.getFlowElementData().setFigureText(elementNameField.getText().trim());
				elementPanel.updateUI();
				JecnDesignerCommon.addElemLink(elementPanel, mapType);
			} catch (Exception e) {
				log.error("FlowSettingsLinkDialog okButPerformed is error", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}

		}
		if (!DrawCommon.checkLongSame(elementPanel.getFlowElementData().getDesignerFigureData().getLinkId(), oldLinkId)) {
			redoData.recodeFlowElement(elementPanel);
			unredoProcess(undoData, redoData);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		}
		this.dispose();
	}

	private void relationMode() {

		String name = elementNameField.getText();
		// 验证名称是否正确
		if (!JecnValidateCommon.validateName(name, tipText)) {
			return;
		}
		if (selectRadBut == 2) {// 关联流程
			// 如果没有设置链接直接关闭返回
			if (relationId == null) {
				this.dispose();
				return;
			}
			// 元素增加链接ID
			elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(relationId);
			JecnDesignerCommon.addElemLink(elementPanel, mapType);
		} else {
			try {
				Long id = null;
				// 判断是否重名
				// if (ConnectionPool.getProcessAction().validateAddName(name,
				// parentId, selectRadBut)) {
				// // 名称已存在
				// tipText.setText(JecnProperties.getValue("nameHaved"));
				// return;
				// }
				if (selectRadBut == 0) {// 创建流程地图
					if (JecnTreeCommon.validateRepeatNameAdd(selectNode, name, TreeNodeType.processMapMode)) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("nameHaved"));
						return;
					}
					// 向流程模板Bean中添加数据JecnFlowStructureMode
					JecnFlowStructureT jecnFlowStructureT = new JecnFlowStructureT();
					// 流程地图模板名称
					jecnFlowStructureT.setFlowName(name);
					jecnFlowStructureT.setDataType(1);
					// 类别：0：流程地图模板 1：流程模板
					jecnFlowStructureT.setIsFlow(0L);
					// 父节点
					jecnFlowStructureT.setPerFlowId(selectNode.getJecnTreeBean().getId());
					// 排序ID
					jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(selectNode)));
					Date createDatime = new Date();
					jecnFlowStructureT.setCreateDate(createDatime);
					jecnFlowStructureT.setProjectId(-1L);
					jecnFlowStructureT.setOccupier(0L);
					jecnFlowStructureT.setDelState(0L);
					jecnFlowStructureT.setPeopleId(JecnConstants.getUserId());
					jecnFlowStructureT.setUpdateDate(createDatime);
					jecnFlowStructureT.setUpdatePeopleId(JecnConstants.getUserId());

					// 执行数据库表的添加保存
					try {
						id = ConnectionPool.getProcessModeAction().addFlowModel(jecnFlowStructureT, -1);
						// 向树节点添加流程模板节点
						JecnTreeBean jecnTreeBean = new JecnTreeBean();
						jecnTreeBean.setId(id);
						jecnTreeBean.setName(name);
						jecnTreeBean.setPid(selectNode.getJecnTreeBean().getId());
						jecnTreeBean.setPname(selectNode.getJecnTreeBean().getName());
						jecnTreeBean.setTreeNodeType(TreeNodeType.processMapMode);
						JecnTreeCommon.addNodeAndSelectNode(JecnDesignerMainPanel.getDesignerMainPanel()
								.getModelPanel().getTree(), jecnTreeBean, selectNode);
						this.dispose();
					} catch (Exception e) {
						log.error("FlowSettingsLinkDialog relationMode is error", e);
					}

				} else if (selectRadBut == 1) {// 创建流程图
					if (selectNode.getJecnTreeBean().getPid() == 0) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("level0CannotProcess"));
						return;
					}
					if (JecnTreeCommon.validateRepeatNameAdd(selectNode, name, TreeNodeType.processMode)) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("nameHaved"));
						return;
					}
					// 向流程模板Bean中添加数据JecnFlowStructureMode
					JecnFlowStructureT jecnFlowStructureT = new JecnFlowStructureT();
					// 流程地图模板名称
					jecnFlowStructureT.setFlowName(name);
					// 类别：0：流程地图模板 1：流程模板
					jecnFlowStructureT.setIsFlow(1L);
					jecnFlowStructureT.setDataType(1);
					// 是否删除
					jecnFlowStructureT.setDelState(0L);
					// 父节点
					jecnFlowStructureT.setPerFlowId(selectNode.getJecnTreeBean().getId());
					// 排序ID
					jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(selectNode)));
					jecnFlowStructureT.setOccupier(0l);
					Date createDatime = new Date();
					jecnFlowStructureT.setCreateDate(createDatime);
					jecnFlowStructureT.setPeopleId(JecnConstants.getUserId());
					jecnFlowStructureT.setUpdateDate(createDatime);
					jecnFlowStructureT.setUpdatePeopleId(JecnConstants.getUserId());
					// 排序ID
					jecnFlowStructureT.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(selectNode)));
					// 执行数据库表的添加保存
					try {
						id = ConnectionPool.getProcessModeAction().addFlowModel(jecnFlowStructureT, 0);
						// 向树节点添加流程模板节点
						JecnTreeBean jecnTreeBean = new JecnTreeBean();
						jecnTreeBean.setId(id);
						jecnTreeBean.setName(name);
						jecnTreeBean.setPid(selectNode.getJecnTreeBean().getId());
						jecnTreeBean.setPname(selectNode.getJecnTreeBean().getName());
						jecnTreeBean.setTreeNodeType(TreeNodeType.processMode);
						JecnTreeCommon.addNodeAndSelectNode(JecnDesignerMainPanel.getDesignerMainPanel()
								.getModelPanel().getTree(), jecnTreeBean, selectNode);
						// 生成一个面板
						JecnFlowMapData totalMapData = JecnDesignerCommon.createFlowMapData(MapType.totalMap, id);
						totalMapData.getDesignerData().setModeType(ModeType.partMapMode);
						totalMapData.setName(name);
						totalMapData.setFlowId(id);
						totalMapData.setRoleCount(5);
						totalMapData.setRoundRectCount(5);
						totalMapData.setHFlag(true);
						JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);
						JecnFileActionProcess.getCreateFlow(totalMapData);
						this.dispose();
					} catch (Exception e) {
						log.error("FlowSettingsLinkDialog relationMode is error", e);
						return;
					}

				}

				// 元素增加链接ID
				elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(id);
				elementPanel.getFlowElementData().setFigureText(elementNameField.getText().trim());
				elementPanel.updateUI();
				JecnDesignerCommon.addElemLink(elementPanel, mapType);
			} catch (Exception e) {
				log.error("FlowSettingsLinkDialog relationMode is error", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}

		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		this.dispose();

	}
}
