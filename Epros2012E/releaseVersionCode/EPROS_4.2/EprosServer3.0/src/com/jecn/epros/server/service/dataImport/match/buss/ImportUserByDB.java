package com.jecn.epros.server.service.dataImport.match.buss;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DBConfigBean;

/**
 * 
 * 基于访问数据技术同步数据
 * 
 */
public class ImportUserByDB extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(this.getClass());
	/** 部门 */
	private List<DeptMatchBean> deptBeanList = new ArrayList<DeptMatchBean>();
	/** 部门 */
	private List<PosMatchBean> posBeanList = new ArrayList<PosMatchBean>();
	/** 人员 */
	private List<UserMatchBean> userBeanList = new ArrayList<UserMatchBean>();
	/** 执行读取标识(true:表示执行过；false：标识没有执行过) */
	private boolean readedFlag = false;

	public ImportUserByDB(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 
	 * 获取部门数据集合
	 * 
	 * @return
	 */
	public List<DeptMatchBean> getDeptBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return deptBeanList;
	}

	/**
	 * 
	 * 获取岗位及岗位组数据集合
	 * 
	 * @return
	 */
	public List<PosMatchBean> getPosBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();
		return posBeanList;
	}

	/**
	 * 
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	public List<UserMatchBean> getUserBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return userBeanList;
	}

	/**
	 * @throws BsException
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	private void readDB() throws Exception {
		// 执行过就不执行
		if (readedFlag) {
			return;
		}
		// Excel文件读取，部门、岗位、人员数据都全部取出来
		selectDB((DBConfigBean) configBean);
	}

	/**
	 * 
	 * 查询数据库返回查询结果
	 * 
	 * @param configBean
	 */
	private void selectDB(DBConfigBean configBean) throws Exception {
		// 部门查询返回记录集
		ResultSet resDept = null;
		// 人员查询返回记录集
		ResultSet resUser = null;
		// 岗位查询返回记录集
		ResultSet resPos = null;
		Statement stmt = null;
		Connection conn = null;
		try {
			// 加载的驱动类
			Class.forName(configBean.getDriver());
			// 创建数据库连接
			conn = DriverManager.getConnection(configBean.getUrl(), configBean
					.getUsername(), configBean.getPassword());

			// 创建一个Statement
			stmt = conn.createStatement();
			// 执行查询，返回结果集
			// 部门
			resDept = stmt.executeQuery(configBean.getDeptSql());
			getDpetList(resDept);
			// 岗位
			resPos = stmt.executeQuery(configBean.getPosSql());
			// 组装岗位集合
			getPosList(resPos);
			// 人员
			resUser = stmt.executeQuery(configBean.getUserSql());
			getUserList(resUser);

			readedFlag = true;
		} catch (ClassNotFoundException e) {
			log.error("三福版ImportUserByDB类的selectDB方法加驱动时出错：" + e);
			throw e;
		} catch (SQLException e) {
			log.error("三福版ImportUserByDB类的selectDB方法创建连接时出错：" + e);
			throw e;
		} finally {
			if (resUser != null) {// 关闭记录集
				try {
					resUser.close();
				} catch (SQLException e) {
					log
							.error("三福版ImportUserByDB类的selectDB方法关闭人员记录集（ResultSet）时出错："
									+ e);
					throw e;
				}
			}
			if (resPos != null) {// 关闭记录集
				try {
					resPos.close();
				} catch (SQLException e) {
					log
							.error("三福版ImportUserByDB类的selectDB方法关闭部门记录集（ResultSet）时出错："
									+ e);
					throw e;
				}
			}
			if (resDept != null) {// 关闭记录集
				try {
					resDept.close();
				} catch (SQLException e) {
					log
							.error("三福版ImportUserByDB类的selectDB方法关闭部门记录集（ResultSet）时出错："
									+ e);
					throw e;
				}
			}
			if (stmt != null) { // 关闭声明
				try {
					stmt.close();
				} catch (SQLException e) {
					log
							.error("三福版ImportUserByDB类的selectDB方法关闭声明（Statement）时出错："
									+ e);
					throw e;
				}
			}
			if (conn != null) { // 关闭连接对象
				try {
					conn.close();
				} catch (SQLException e) {
					log
							.error("三福版ImportUserByDB类的selectDB方法关闭连接对象（Connection）时出错："
									+ e);
					throw e;
				}
			}
		}
	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resDept
	 * @throws SQLException
	 */
	private void getDpetList(ResultSet resDept) throws SQLException {
		while (resDept.next()) {
			DeptMatchBean deptBean = new DeptMatchBean();
			// 部门编号
			String deptNum = resDept.getString(1);
			// 部门名称
			String deptName = resDept.getString(2);
			// 上级部门编号
			String perDeptNum = resDept.getString(3);

			// 部门编号
			deptBean.setDeptNum(MatchTool.emtryToNull(deptNum));
			// 部门名称
			deptBean.setDeptName(MatchTool.emtryToNull(deptName));
			// 上级部门编号
			deptBean.setPerDeptNum(MatchTool.emtryToNull(perDeptNum));

			this.deptBeanList.add(deptBean);
		}
	}

	/**
	 * 
	 * 把数据取出的岗位结果集存储到bean对象中
	 * 
	 * @param resDept
	 * @throws SQLException
	 */
	private void getPosList(ResultSet resPos) throws SQLException {
		while (resPos.next()) {
			PosMatchBean posBean = new PosMatchBean();
			// 基准岗位编号
			String basePosNum = resPos.getString(1);
			// 基准岗位名称
			String basePosName = resPos.getString(2);
			// 部门编号
			String deptNum = resPos.getString(3);
			// 岗位编号
			String posNum = resPos.getString(4);
			// 岗位名称
			String posName = resPos.getString(5);

			posBean.setBasePosNum(MatchTool.emtryToNull(basePosNum));
			posBean.setBasePosName(MatchTool.emtryToNull(basePosName));
			posBean.setDeptNum(MatchTool.emtryToNull(deptNum));
			posBean.setActPosNum(MatchTool.emtryToNull(posNum));
			posBean.setActPosName(MatchTool.emtryToNull(posName));
			this.posBeanList.add(posBean);
		}
	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resUser
	 * @throws SQLException
	 */
	private void getUserList(ResultSet resUser) throws SQLException {
		while (resUser.next()) {
			UserMatchBean userBean = new UserMatchBean();
			// 下面的顺序不要轻易改变
			// 登录名称
			String userNum = resUser.getString(1);
			// 真实姓名
			String trueName = resUser.getString(2);
			// 任职岗位编号
			String posNum = resUser.getString(3);
			// 邮箱地址
			String email = resUser.getString(4);
			// 内外网邮件标识(内网:0 外网:1)
			String emailType = resUser.getString(5);
			// 联系电话
			String phone = resUser.getString(6);

			// 登录名称
			userBean.setLoginName(MatchTool.emtryToNull(userNum));
			// 真实姓名
			userBean.setTrueName(MatchTool.emtryToNull(trueName));
			// 任职岗位编号
			userBean.setPosNum(MatchTool.emtryToNull(posNum));
			// 邮箱地址
			userBean.setEmail(MatchTool.emtryToNull(email));
			// 邮箱、邮箱类型不等于空且是“0”，“1”
			if (MatchTool.isEmailTypeStr(emailType)) {
				// 内外网邮件标识(内网:0 外网:1)
				userBean.setEmailType(Integer.valueOf(emailType));
			}
			// 联系电话
			userBean.setPhone(MatchTool.emtryToNull(phone));

			this.userBeanList.add(userBean);
		}

	}

}
