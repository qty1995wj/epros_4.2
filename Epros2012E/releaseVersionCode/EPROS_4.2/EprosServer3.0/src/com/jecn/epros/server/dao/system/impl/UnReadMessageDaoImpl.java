package com.jecn.epros.server.dao.system.impl;

import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.system.IUnReadMessageDao;


/***
 * 未读消息操作类
 * @author 
 * 2013-03-05
 *
 */
public class UnReadMessageDaoImpl  extends AbsBaseDao<JecnMessage, Long> implements IUnReadMessageDao   {

	@Override
	public int getUnReadMesgNum(Long inceptPeopleId) throws Exception {
		String sql = "select count(*)" 
			+ " from jecn_message jm, jecn_user ju " 
			+ "where jm.incept_people_id = ju.people_id and jm.incept_people_id = '"+inceptPeopleId+"'";
		return this.countAllByParamsNativeSql(sql);
	}

}
