package epros.designer.gui.rule.mode;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.util.JecnUIUtil;

public class RuleModeTitleProcessPanel extends JPanel {


	private JScrollPane scrollPane=null;
	private JecnTable  table=null;
	public RuleModeTitleProcessPanel(String title){
		this.setLayout(new BorderLayout());
//		this.setBorder(BorderFactory.createTitledBorder(
//				BorderFactory.createEmptyBorder(1, 1, 1, 1), title,
//				TitledBorder.DEFAULT_JUSTIFICATION,
//				TitledBorder.DEFAULT_POSITION,
//				new java.awt.Font(JecnProperties.getValue("songTi"), 1, 12), Color.BLACK));
		this.scrollPane=new JScrollPane();
		Dimension size =new Dimension(680, 120);
		this.scrollPane.setPreferredSize(size);
		this.table=new TitleFileTable();
		this.scrollPane.setViewportView(this.table);
		scrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.add(this.scrollPane,BorderLayout.CENTER);
	}
	
	
	
	class TitleFileTable extends JecnTable{

		@Override
		public JecnTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("flowName"));
			title.add(JecnProperties.getValue("flowNum"));
			
			Vector<Vector<String>> vs=new	Vector<Vector<String>>();
			return new JecnTableModel(title, vs);
		}

		@Override
		public boolean isSelectMutil() {
			
			return false;
		}

		@Override
		public int[] gethiddenCols() {
			
			return new int[]{};
		}
		
	}

}
