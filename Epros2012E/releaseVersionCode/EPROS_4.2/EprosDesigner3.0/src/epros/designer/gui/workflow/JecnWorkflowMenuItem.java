package epros.designer.gui.workflow;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.JecnDesignerFrame;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.define.TermDefineChooseDialog;
import epros.designer.gui.error.ErrorCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.file.HighEfficiencyFileTree;
import epros.designer.gui.header.JecnDesignerModuleShow;
import epros.designer.gui.pagingImage.PageSetDialog;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.gui.process.FlowSetFlowInterfaceDialogNew;
import epros.designer.gui.process.FlowSettingsLinkDialog;
import epros.designer.gui.process.JecnProcessCommon;
import epros.designer.gui.process.RoleCheckDialog;
import epros.designer.gui.process.ThroughCheckDialog;
import epros.designer.gui.process.flow.FlowRoleActiveDialog;
import epros.designer.gui.process.roleDetail.RoleDetailDialog;
import epros.designer.gui.rule.RuleChooseDialog;
import epros.designer.gui.timeline.TimeLimitLineBuilder;
import epros.designer.service.JecnDesignerImpl;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.designer.JecnFigureFileTBean;
import epros.draw.event.JecnOftenActionProcess;
import epros.draw.gui.designer.JecnDesignerLinkPanel;
import epros.draw.gui.designer.JecnIDesigner;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.figure.shape.part.FileImage;
import epros.draw.gui.figure.shape.part.TermFigureAR;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnTipPopupMenu;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.menu.JecnReplaceActivePopupMenu;
import epros.draw.gui.top.toolbar.menu.JecnReplaceProcessMapPopupMenu;
import epros.draw.gui.top.toolbar.menu.JecnReplaceRolePopupMenu;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.popup.JecnFlowElemPopupMenu;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 画图面板上右键菜单
 * 
 * @author ZHOUXY
 * 
 */
public class JecnWorkflowMenuItem {
	private Logger log = Logger.getLogger(JecnWorkflowMenuItem.class);
	/** 选中节点 */
	private JecnTreeNode selectNode;

	/** 面板对应的节点 */
	private JecnTreeNode workFlowNode;

	private MapType mapType;
	/** 重置流程图 */
	private JMenuItem roleActiveModeItem = null;
	/** 检查文件控制信息 */
	private JMenuItem checFileControlItem = null;
	// 流程贯通性检查
	private JMenuItem flowCheckout = new JMenuItem(JecnProperties.getValue("flowCheckout"));
	// 角色规范性检查
	private JMenuItem roleCheckout = new JMenuItem(JecnProperties.getValue("roleCheckout"));
	// /** 流程发布 */
	// private JMenuItem processPubItem = null;

	/** 设置链接 流程地图元素右键 */
	private JMenuItem setConnProcessMapItem = null;
	/** 取消链接 */
	private JMenuItem cancelConnProcessMapItem = null;

	/** 创建流程地图 */
	private JMenuItem createProcessMapItem = null;
	/** 创建流程 */
	private JMenuItem createProcessItem = null;
	/** 关联流程地图 */
	private JMenuItem relaProcessMapItem = null;
	/** 关联流程 */
	private JMenuItem relaProcessItem = null;
	/** 关联制度 */
	private JMenuItem relaRuleItem = null;

	/** 流程接口描述 */
	private JMenuItem setConnProcessItem = null;
	/** 取消链接 */
	private JMenuItem cancelConnProcessItem = null;
	/** 关联上游流程 */
	private JMenuItem relaUpProcessItem = null;
	/** 关联下游流程 */
	private JMenuItem relaDownProcessItem = null;
	/** 关联接口流程 */
	private JMenuItem relaInterfaceProcessItem = null;
	/** 关联子流程 */
	private JMenuItem relaSonProcessItem = null;
	/** 创建子流程 */
	private JMenuItem createSonProcessItem = null;
	/** 添加角色 */
	private JMenuItem roleDetailItem = null;

	/** 组织元素设置链接 */
	private JMenuItem setConnOrgItem = null;
	/** 取消链接 */
	private JMenuItem cancelConnOrgItem = null;
	/** 关联组织 */
	private JMenuItem relaOrgItem = null;
	/** 流程图插入图片 */
	private JMenuItem processInsertImageItem = null;
	/** 流程地图插入图片 */
	private JMenuItem processMapInsertImageItem = null;
	/** 组织图插入图片 */
	private JMenuItem orgInsertImageItem = null;

	/** 流程图：导入 */
	private JMenuItem partMapInputItem = null;
	/** 流程图：导出 */
	private JMenuItem partMapOutputItem = null;
	/** 流程地图：导入 */
	private JMenuItem totalMapInputItem = null;
	/** 流程地图：导出 */
	private JMenuItem totalMapOutputItem = null;
	/** 组织图：导入 */
	private JMenuItem orgMapInputItem = null;
	/** 组织图：导出 */
	private JMenuItem orgMapOutputItem = null;

	/** 流程地图 :描述；添加附件 */
	/** 添加附件 */
	private JMenuItem totalMapAddFileItem = null;

	/** 流程的，流程地图，组织 文档上传右键菜单 */
	private JMenuItem mapUplodeFileItem = null;
	private JMenuItem processUplodeFileItem = null;
	private JMenuItem orgUplodeFileItem = null;

	/** 流程图：画图面板右键菜单 */
	private List<JMenuItem> processMenuItemList = null;
	/** 流程地图：画图面板右键菜单 */
	private List<JMenuItem> processMapMenuItemList = null;
	/** 组织图：画图面板右键菜单 */
	private List<JMenuItem> orgMapMenuItemList = null;
	/** 流程图：流程元素右键菜单 */
	private List<JMenuItem> processElemMenuItemList = null;
	/** 流程地图：流程元素右键菜单 */
	private List<JMenuItem> processMapElemwMenuItemList = null;
	/** 组织图：流程元素右键菜单 */
	private List<JMenuItem> orgMapElemMenuItemList = null;
	/** 选中的元素 */
	private JecnBaseFigurePanel elementPanel = null;

	/** 流程图：分页设置 */
	private JMenuItem pageSetingItem = new JMenuItem(JecnProperties.getValue("pageSetIng"), new ImageIcon(
			"images/menuImage/renumber.gif"));

	/** 流程图/流程地图/组织图：恢复本地数据 */
	private JMenuItem processDataRecoveryMenuItem = null;
	private JMenuItem processMapDataRecoveryMenuItem = null;
	private JMenuItem orgDataRecoveryMenuItem = null;

	// 操作前数据
	private JecnUndoRedoData undoData = new JecnUndoRedoData();
	// 操作后数据
	private JecnUndoRedoData redoData = new JecnUndoRedoData();

	/** 术语选择 */
	private JMenuItem termItem = null;
	/** 办理时限 */
	private JMenuItem timeLineItem = null;
	/** 默认不显示时间轴 */
	public static boolean isShowTimeLine = false;

	private static JecnWorkflowMenuItem jecnWorkflowMenuItem = null;

	public static JecnWorkflowMenuItem getWorkflowMenuItem() {
		if (jecnWorkflowMenuItem == null) {
			jecnWorkflowMenuItem = new JecnWorkflowMenuItem();
		}
		return jecnWorkflowMenuItem;
	}

	public JecnWorkflowMenuItem() {
		roleActiveModeItem = new JMenuItem(JecnProperties.getValue("reProcess"), new ImageIcon(
				"images/menuImage/reProcess.gif"));
		checFileControlItem = new JMenuItem(JecnProperties.getValue("checFileControl"), new ImageIcon(
				"images/menuImage/checFileControl.gif"));

		setConnProcessMapItem = new JMenuItem(JecnProperties.getValue("setConn"), new ImageIcon(
				"images/menuImage/setConn.gif"));
		cancelConnProcessMapItem = new JMenuItem(JecnProperties.getValue("cancelConn"), new ImageIcon(
				"images/menuImage/cancelConn.gif"));
		createProcessMapItem = new JMenuItem(JecnProperties.getValue("createProcessMap"), new ImageIcon(
				"images/menuImage/createProcessMap.gif"));
		createProcessItem = new JMenuItem(JecnProperties.getValue("createProcess"), new ImageIcon(
				"images/menuImage/createProcess.gif"));
		relaProcessItem = new JMenuItem(JecnProperties.getValue("relaProcess"), new ImageIcon(
				"images/menuImage/relaProcess.gif"));
		relaProcessMapItem = new JMenuItem(JecnProperties.getValue("relaProcessMap"), new ImageIcon(
				"images/menuImage/relaProcessMap.gif"));
		roleDetailItem = new JMenuItem(JecnProperties.getValue("addRole"),
				new ImageIcon("images/menuImage/addRole.gif"));
		relaRuleItem = new JMenuItem(JecnProperties.getValue("relaRule"),
				new ImageIcon("images/menuImage/relaRule.gif"));

		setConnProcessItem = new JMenuItem(JecnProperties.getValue("setFlowInterface"), new ImageIcon(
				"images/menuImage/setConn.gif"));
		cancelConnProcessItem = new JMenuItem(JecnProperties.getValue("cancelConn"), new ImageIcon(
				"images/menuImage/cancelConn.gif"));
		relaUpProcessItem = new JMenuItem(JecnProperties.getValue("relaUpProcess"), new ImageIcon(
				"images/menuImage/relaUpProcess.gif"));
		relaDownProcessItem = new JMenuItem(JecnProperties.getValue("relaDownProcess"), new ImageIcon(
				"images/menuImage/relaDownProcess.gif"));
		relaInterfaceProcessItem = new JMenuItem(JecnProperties.getValue("relaInterfaceProcess"), new ImageIcon(
				"images/menuImage/relaInterfaceProcess.gif"));
		relaSonProcessItem = new JMenuItem(JecnProperties.getValue("relaSonProcess"), new ImageIcon(
				"images/menuImage/relaSonProcess.gif"));
		createSonProcessItem = new JMenuItem(JecnProperties.getValue("createSonProcess"), new ImageIcon(
				"images/menuImage/createSonProcess.gif"));
		setConnOrgItem = new JMenuItem(JecnProperties.getValue("setConn"),
				new ImageIcon("images/menuImage/setConn.gif"));
		cancelConnOrgItem = new JMenuItem(JecnProperties.getValue("cancelConn"), new ImageIcon(
				"images/menuImage/cancelConn.gif"));
		relaOrgItem = new JMenuItem(JecnProperties.getValue("relationOrg"), new ImageIcon(
				"images/menuImage/relaOrg.gif"));

		processInsertImageItem = new JMenuItem(JecnProperties.getValue("insertImage"), new ImageIcon(
				"images/menuImage/insertImage.gif"));
		processMapInsertImageItem = new JMenuItem(JecnProperties.getValue("insertImage"), new ImageIcon(
				"images/menuImage/insertImage.gif"));
		orgInsertImageItem = new JMenuItem(JecnProperties.getValue("insertImage"), new ImageIcon(
				"images/menuImage/insertImage.gif"));
		// 添加附件
		totalMapAddFileItem = new JMenuItem(JecnProperties.getValue("mapAddFile"), new ImageIcon(
				"images/menuImage/mapAddFile.gif"));
		// 流程的，流程地图，组织 文档上传右键菜单 （关联文件）
		mapUplodeFileItem = new JMenuItem(JecnProperties.getValue("ralationFile"), new ImageIcon(
				"images/menuImage/ralationFile.gif"));
		processUplodeFileItem = new JMenuItem(JecnProperties.getValue("ralationFile"), new ImageIcon(
				"images/menuImage/ralationFile.gif"));
		orgUplodeFileItem = new JMenuItem(JecnProperties.getValue("ralationFile"), new ImageIcon(
				"images/menuImage/ralationFile.gif"));

		// --------=======恢复本地数据右键菜单实例化C====================
		processDataRecoveryMenuItem = new JMenuItem(JecnProperties.getValue("dataRecovery"), new ImageIcon(
				"images/menuImage/dataRecovery.gif"));
		processMapDataRecoveryMenuItem = new JMenuItem(JecnProperties.getValue("dataRecovery"), new ImageIcon(
				"images/menuImage/dataRecovery.gif"));
		orgDataRecoveryMenuItem = new JMenuItem(JecnProperties.getValue("dataRecovery"), new ImageIcon(
				"images/menuImage/dataRecovery.gif"));

		timeLineItem = new JMenuItem(JecnProperties.getValue("timeLimit"), JecnFileUtil
				.getToolBarImageIcon("formatRoleMove"));
		// -------=================end===================================

		termItem = new JMenuItem(JecnProperties.getValue("termSelect"),new ImageIcon(
		"images/menuImage/termDefineRoot.gif"));
		// 流程图：导入
		partMapInputItem = initInputMenuItem();
		// 流程图：导出
		partMapOutputItem = initInOutputMenuItem();
		// 流程地图：导入
		totalMapInputItem = initInputMenuItem();
		// 流程地图：导出
		totalMapOutputItem = initInOutputMenuItem();
		// 组织图：导入
		orgMapInputItem = initInputMenuItem();
		// 组织图：导出
		orgMapOutputItem = initInOutputMenuItem();

		// 设置透明
		/** 角色与活动模板 */
		roleActiveModeItem.setOpaque(false);
		/** 检查文件控制信息 */
		checFileControlItem.setOpaque(false);
		flowCheckout.setOpaque(false);
		roleCheckout.setOpaque(false);
		/** 设置链接 */
		setConnProcessMapItem.setOpaque(false);
		cancelConnProcessMapItem.setOpaque(false);
		/** 创建流程地图 */
		createProcessMapItem.setOpaque(false);
		/** 创建流程 */
		createProcessItem.setOpaque(false);
		/** 关联流程地图 */
		relaProcessMapItem.setOpaque(false);
		/** 关联流程 */
		relaProcessItem.setOpaque(false);
		/** 关联制度 */
		relaRuleItem.setOpaque(false);

		setConnProcessItem.setOpaque(false);
		cancelConnProcessItem.setOpaque(false);
		/** 关联上游流程 */
		relaUpProcessItem.setOpaque(false);
		/** 关联下游流程 */
		relaDownProcessItem.setOpaque(false);
		/** 关联接口流程 */
		relaInterfaceProcessItem.setOpaque(false);
		/** 关联子流程 */
		relaSonProcessItem.setOpaque(false);
		/** 创建子流程 */
		createSonProcessItem.setOpaque(false);
		/** 角色明细 */
		roleDetailItem.setOpaque(false);

		setConnOrgItem.setOpaque(false);
		cancelConnOrgItem.setOpaque(false);
		/** 关联组织 */
		relaOrgItem.setOpaque(false);
		/** 流程图插入图片 */
		processInsertImageItem.setOpaque(false);
		/** 流程地图插入图片 */
		processMapInsertImageItem.setOpaque(false);
		/** 组织图插入图片 */
		orgInsertImageItem.setOpaque(false);

		// 添加附件
		totalMapAddFileItem.setOpaque(false);
		/** 流程的，流程地图，组织 文档上传右键菜单 */
		mapUplodeFileItem.setOpaque(false);
		processUplodeFileItem.setOpaque(false);
		orgUplodeFileItem.setOpaque(false);

		timeLineItem.setOpaque(false);

		termItem.setOpaque(false);

		// 流程面板右键菜单
		processMenuItemList = new ArrayList<JMenuItem>();
		// 流程图：导入
		processMenuItemList.add(partMapInputItem);
		// 流程图：导出
		processMenuItemList.add(partMapOutputItem);
		// 分割线
		processMenuItemList.add(null);
		processMenuItemList.add(roleActiveModeItem);
		processMenuItemList.add(checFileControlItem);
		processMenuItemList.add(flowCheckout);
		processMenuItemList.add(roleCheckout);
		// 分割线
		processMenuItemList.add(null);
		// 流程图：分页设置
		processMenuItemList.add(pageSetingItem);
		/** 流程图恢复本地数据 */
		processMenuItemList.add(processDataRecoveryMenuItem);
		// 办理时限
		processMenuItemList.add(timeLineItem);

		// 流程地图面板右键
		processMapMenuItemList = new ArrayList<JMenuItem>();
		// 流程地图：导入
		processMapMenuItemList.add(totalMapInputItem);
		// 流程地图：导出
		processMapMenuItemList.add(totalMapOutputItem);
		/** 流程地图恢复本地数据 */
		processMapMenuItemList.add(processMapDataRecoveryMenuItem);

		// 组织图面板右键
		orgMapMenuItemList = new ArrayList<JMenuItem>();
		// 组织图：导入
		orgMapMenuItemList.add(orgMapInputItem);
		// 组织图：导出
		orgMapMenuItemList.add(orgMapOutputItem);
		/** 组织图恢复本地数据 */
		orgMapMenuItemList.add(orgDataRecoveryMenuItem);

		// 流程图元素右键
		processElemMenuItemList = new ArrayList<JMenuItem>();
		processElemMenuItemList.add(setConnProcessItem);
		processElemMenuItemList.add(cancelConnProcessItem);

		// 关联上游流程
		processElemMenuItemList.add(relaUpProcessItem);
		// 关联下游流程
		processElemMenuItemList.add(relaDownProcessItem);
		// 关联接口流程
		processElemMenuItemList.add(relaInterfaceProcessItem);
		// 关联子流程
		processElemMenuItemList.add(relaSonProcessItem);
		// 创建子流程
		processElemMenuItemList.add(createSonProcessItem);
		// 角色明细
		processElemMenuItemList.add(roleDetailItem);

		// 插入图片
		processElemMenuItemList.add(processInsertImageItem);
		// 流程文档上传文件
		processElemMenuItemList.add(processUplodeFileItem);

		processElemMenuItemList.add(termItem);

		// 流程地图元素
		processMapElemwMenuItemList = new ArrayList<JMenuItem>();
		// 设置链接
		processMapElemwMenuItemList.add(setConnProcessMapItem);
		processMapElemwMenuItemList.add(cancelConnProcessMapItem);
		// 创建流程地图
		processMapElemwMenuItemList.add(createProcessMapItem);
		// 创建流程
		processMapElemwMenuItemList.add(createProcessItem);
		// 关联流程地图
		processMapElemwMenuItemList.add(relaProcessMapItem);
		// 关联流程
		processMapElemwMenuItemList.add(relaProcessItem);
		// 关联制度
		processMapElemwMenuItemList.add(relaRuleItem);
		// 插入图片
		processMapElemwMenuItemList.add(processMapInsertImageItem);
		// 添加附件
		processMapElemwMenuItemList.add(totalMapAddFileItem);
		/** 流程地图的 文档上传右键菜单 */
		processMapElemwMenuItemList.add(mapUplodeFileItem);

		// 组织图元素右键
		orgMapElemMenuItemList = new ArrayList<JMenuItem>();
		orgMapElemMenuItemList.add(setConnOrgItem);
		orgMapElemMenuItemList.add(cancelConnOrgItem);
		// 关联组织
		orgMapElemMenuItemList.add(relaOrgItem);
		// 插入图片
		orgMapElemMenuItemList.add(orgInsertImageItem);
		/** 组织的 文档上传右键菜单 */
		orgMapElemMenuItemList.add(orgUplodeFileItem);

		// 事件初始化
		menuItemActionInit();
	}

	private void menuItemActionInit() {
		// 角色与活动模板
		roleActiveModeItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FlowRoleActiveDialog d = new FlowRoleActiveDialog();
				d.setVisible(true);
			}
		});
		// 检查文件控制信息
		checFileControlItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ErrorCommon.INSTANCE.checkControlError();
			}
		});
		// 设置链接
		setConnProcessMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setConnProcessMap();
			}
		});
		cancelConnProcessMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelConn();
			}
		});
		// 创建流程地图
		createProcessMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				createProcessMap();
			}
		});
		// 创建流程
		createProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				createProcess();
			}
		});
		// 关联流程地图
		relaProcessMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (selectNode == null || selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.processMap) {

					JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties
							.getValue("selectProcessMapNode"));
					return;
				}
				if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getFlowId().equals(
						selectNode.getJecnTreeBean().getId())) {
					JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties
							.getValue("cannotRelateThis"));
					return;
				}
				relationNode();
			}
		});
		// 关联流程
		relaProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (selectNode == null || !JecnDesignerCommon.isProcess(selectNode.getJecnTreeBean().getTreeNodeType())) {

					JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties
							.getValue("selectProcessNode"));
					return;
				}
				relationNode();
			}
		});

		setConnProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setConnProcess();
			}
		});
		// 流程图元素取消链接
		cancelConnProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelConn();
			}
		});
		// 关联上游流程
		relaUpProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!relationNodeVerdict()) {
					return;
				}
				relationProcessNode("1");
			}
		});
		// 关联下游流程
		relaDownProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!relationNodeVerdict()) {
					return;
				}
				relationProcessNode("2");
			}
		});
		// 关联接口流程
		relaInterfaceProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!relationNodeVerdict()) {
					return;
				}
				relationProcessNode("3");
			}
		});
		// 关联子流程
		relaSonProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!relationNodeVerdict()) {
					return;
				}
				relationProcessNode("4");
			}
		});
		// 关联制度
		relaRuleItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (selectNode == null
						|| (selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.ruleFile && selectNode
								.getJecnTreeBean().getTreeNodeType() != TreeNodeType.ruleModeFile)) {
					JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties
							.getValue("selectRuleNode"));
					return;
				}
				relationNode();
			}
		});
		// 创建子流程
		createSonProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				elementPanel.getFlowElementData().getDesignerFigureData().setLineType("4");
				createProcess();

			}
		});
		// 角色明细
		roleDetailItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RoleDetailDialog rd = new RoleDetailDialog((JecnBaseRoleFigure) elementPanel);
				rd.setVisible(true);
				if (rd.isOk()) {
					// 为角色元素增加岗位图标
					JecnDesignerCommon.addElemLink(elementPanel, mapType);
				}
			}
		});
		// 插入图片
		processInsertImageItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				insertImage();
			}
		});
		processMapInsertImageItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				insertImage();
			}
		});
		/**
		 * 添加附件
		 */
		totalMapAddFileItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				totalMapAddFile();
			}
		});

		setConnOrgItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setConnOrg();
			}
		});
		cancelConnOrgItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelConn();
			}
		});
		relaOrgItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (selectNode == null || selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.organization) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("selectOrgNode"));
					return;
				} else if (selectNode.getJecnTreeBean().getId() == workFlowNode.getJecnTreeBean().getId()) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("nodeRelationOrgMyself"));
					return;
				}
				relationNode();
			}
		});
		orgInsertImageItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				insertImage();
			}
		});

		/***************** 【文档上传附件监听】 **************************/
		processUplodeFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				uplodeFile();
			}
		});
		mapUplodeFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				uplodeFile();
			}
		});
		orgUplodeFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				uplodeFile();
			}
		});

		/**
		 * 流程图：分页设置
		 */
		pageSetingItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PageSetDialog pageSetDialog = new PageSetDialog();
				pageSetDialog.setVisible(true);
			}
		});

		// ---=========================================================
		/**
		 * 流程图恢复本地数据
		 * */
		processDataRecoveryMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				importBakData();
			}
		});

		/***
		 * 流程地图恢复本地数据
		 */
		processMapDataRecoveryMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				importBakData();
			}

		});

		/**
		 * 组织图恢复本地数据
		 * */
		orgDataRecoveryMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				importBakData();
			}
		});

		timeLineItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				timeLineMenuAction();
			}
		});

		termItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				termItemMenuAction();
			}
		});

		flowCheckout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (flowEditAbile()) {
					JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
					if (workflow == null) {
						return;
					}
					JecnOftenActionProcess.saveWokfFlow(workflow);
				}
				ThroughCheckDialog dialog = new ThroughCheckDialog(workFlowNode.getJecnTreeBean().getId());
				dialog.setVisible(true);
			}
		});

		roleCheckout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean saveable = flowEditAbile()
						&& JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().isAuthSave() == 1;
				if (saveable) {
					JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
					if (workflow == null) {
						return;
					}
					JecnOftenActionProcess.saveWokfFlow(workflow);
				}
				RoleCheckDialog dialog = new RoleCheckDialog(workFlowNode.getJecnTreeBean().getId(), saveable);
				dialog.setVisible(true);
			}
		});
	}

	private boolean flowEditAbile() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		boolean saveable = true;
		if (desktopPane.getFlowMapData().isAuthSave() != 1) {
			saveable = false;
		}
		return saveable;
	}

	/**
	 * 术语选择
	 */
	private void termItemMenuAction() {
		if (!(elementPanel instanceof TermFigureAR)) {
			return;
		}
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		TermFigureAR termFigureAR = (TermFigureAR) elementPanel;
		Long linkId = termFigureAR.getFlowElementData().getDesignerFigureData().getLinkId();
		undoData.recodeFlowElement(termFigureAR);// 记录撤销操作
		// 初始化
		List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();
		if (linkId != null) {
			JecnTreeBean treeBean = new JecnTreeBean();
			treeBean.setId(linkId);
			treeBean.setName(elementPanel.getFlowElementData().getFigureText());
			treeBeans.add(treeBean);
		}

		// 弹出选择框
		TermDefineChooseDialog termDialog = new TermDefineChooseDialog(treeBeans, null);
		termDialog.setVisible(true);

		// 返回结果
		if (treeBeans.size() > 0) {
			JecnTreeBean treeBean = treeBeans.get(0);
			termFigureAR.getFlowElementData().setTermDefine(treeBean.getContent());
			termFigureAR.getFlowElementData().setFigureText(treeBean.getName());
			termFigureAR.getFlowElementData().getDesignerFigureData().setLinkId(treeBean.getId());
		} else {
			termFigureAR.getFlowElementData().setTermDefine(null);
			termFigureAR.getFlowElementData().getDesignerFigureData().setLinkId(null);
		}
		redoData.recodeFlowElement(termFigureAR);
		unredoProcess(undoData, redoData);
		termFigureAR.updateUI();

	}

	private void timeLineMenuAction() {
		// 判断角色移动是否选中
		if (isShowTimeLine) {
			// 清除
			TimeLimitLineBuilder.INSTANCE.clearTimeLimitPanel();
			timeLineItem.setIcon(JecnFileUtil.getToolBarImageIcon("formatRoleMove"));
			isShowTimeLine = false;
		} else {
			timeLineItem.setIcon(JecnFileUtil.getToolBarImageIcon("formatRoleMoveSelected"));
			// 绘制时间轴
			TimeLimitLineBuilder.INSTANCE.buildTimeLinePanel();
			isShowTimeLine = true;
		}

		JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
	}

	/**
	 * 导入备份文件（执行到此方法时，备份文件是一定存在的）
	 * 
	 * @author weidp
	 * @date 2014-11-3 下午03:02:43
	 */
	private void importBakData() {
		// 获取当前面板
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		JecnFlowMapData mapData = workFlow.getFlowMapData();
		// 获取备份文件
		File bakDir = new File(JecnUtil.getFlowBakPath(mapData));
		// 默认只有一个备份文件
		File bakFile = bakDir.listFiles()[0];
		// 获取文件名称
		String fileName = bakFile.getName();
		// 通过文件名称获取备份时间
		String time = JecnUtil.formatDateToString(new Date(Long.valueOf(fileName
				.substring(0, fileName.lastIndexOf(".")))));
		// 提示语句
		String msg = "";
		switch (mapData.getMapType()) {
		case totalMap:
			msg = JecnProperties.getValue("processMap");
			break;
		case partMap:
			msg = JecnProperties.getValue("processFlow");
			break;
		case orgMap:
			msg = JecnProperties.getValue("orgMap");
			break;
		}
		int result = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("recoveryInfo0")
				+ time + JecnProperties.getValue("recoveryInfo1") + msg, null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.QUESTION_MESSAGE);
		if (result == JecnOptionPane.NO_OPTION) {
			return;
		}
		// 执行导入
		JecnIDesigner designer = new JecnDesignerImpl();
		designer.recoveryFlowFile(bakFile);
	}

	/**
	 * 文档 上传文件
	 * 
	 */
	private void uplodeFile() {
		if (!(elementPanel instanceof FileImage)) {
			return;
		}
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		FileImage fileImage = (FileImage) elementPanel;
		undoData.recodeFlowElement(elementPanel);// 记录撤销操作
		List<JecnTreeBean> listFile = new ArrayList<JecnTreeBean>();
		JecnFigureFileTBean fileTBean = null;
		if (fileImage.getFlowElementData().getListFigureFileTBean().size() > 0) {
			fileTBean = fileImage.getFlowElementData().getListFigureFileTBean().get(0);
		}

		if (fileTBean != null && fileTBean.getFileId() != null) {
			JecnTreeBean bean = new JecnTreeBean();
			bean.setId(fileTBean.getFileId());
			bean.setName(fileTBean.getFileName());
			listFile.add(bean);
		}
		// 将输入表格上的数据传到文件选择框中显示
		FileChooseDialog fileChooseDialog = new FileChooseDialog(listFile);
		// 只能选择一个文件
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			Long newLinkId = null;
			if (listFile.size() == 1) {
				newLinkId = listFile.get(0).getId();
			}
			Long oldLinkId = null;
			if (elementPanel.getFlowElementData().getListFigureFileTBean().size() == 1) {
				oldLinkId = elementPanel.getFlowElementData().getListFigureFileTBean().get(0).getFileId();
			}
			if (DrawCommon.checkLongSame(newLinkId, oldLinkId)) {
				return;
			}
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
			if (listFile.size() > 0) {
				if (fileTBean == null) {
					fileTBean = new JecnFigureFileTBean();
					fileTBean.setFigureId(fileImage.getFlowElementData().getFlowElementId());
					fileTBean.setFigureType(fileImage.getFlowElementData().getMapElemType().toString());
					fileImage.getFlowElementData().getListFigureFileTBean().add(fileTBean);
				}
				fileTBean.setFileName(listFile.get(0).getName());
				fileTBean.setFileId(listFile.get(0).getId());
				elementPanel.getFlowElementData().setFigureText(listFile.get(0).getName());
				JecnDesignerCommon.addElemLink(elementPanel, mapType);
				elementPanel.repaint();
			} else {
				fileImage.getFlowElementData().getListFigureFileTBean().clear();
				JecnDesignerCommon.addElemLink(elementPanel, mapType);
				elementPanel.repaint();
			}
			redoData.recodeFlowElement(elementPanel);
			unredoProcess(undoData, redoData);
		}
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:置灰全部右键菜单项
	 */
	public void setMenuItemDisabled() {
		this.roleActiveModeItem.setEnabled(false);
		this.checFileControlItem.setEnabled(false);
		this.flowCheckout.setEnabled(false);
		this.roleCheckout.setEnabled(false);
		this.setConnProcessMapItem.setEnabled(false);
		this.createProcessMapItem.setEnabled(false);
		this.createProcessItem.setEnabled(false);
		this.relaProcessMapItem.setEnabled(false);
		this.roleDetailItem.setEnabled(false);
		this.relaProcessItem.setEnabled(false);
		this.relaRuleItem.setEnabled(false);
		this.processInsertImageItem.setEnabled(false);
		this.processMapInsertImageItem.setEnabled(false);
		this.orgInsertImageItem.setEnabled(false);
		this.setConnProcessItem.setEnabled(false);
		this.relaUpProcessItem.setEnabled(false);
		this.relaDownProcessItem.setEnabled(false);
		this.relaInterfaceProcessItem.setEnabled(false);
		this.relaSonProcessItem.setEnabled(false);
		this.createSonProcessItem.setEnabled(false);
		this.relaOrgItem.setEnabled(false);
		/** 添加附件 */
		totalMapAddFileItem.setEnabled(false);

		// 关联文件默认不显示
		mapUplodeFileItem.setEnabled(false);

		processUplodeFileItem.setEnabled(false);
		orgUplodeFileItem.setEnabled(false);

		setConnOrgItem.setEnabled(false);
		cancelConnProcessMapItem.setEnabled(false);
		cancelConnProcessItem.setEnabled(false);
		cancelConnOrgItem.setEnabled(false);
		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
			setConnProcessMapItem.setVisible(true);
			cancelConnProcessMapItem.setVisible(true);
			relaRuleItem.setVisible(true);
			/** 添加附件 */
			totalMapAddFileItem.setVisible(true);
		}
		// 是否显示流程面板上的右键菜单的分页设置
		showPageSetItem();

		// 是否恢复本地备份
		processDataRecoveryMenuItem.setEnabled(false);
		processMapDataRecoveryMenuItem.setEnabled(false);
		orgDataRecoveryMenuItem.setEnabled(false);
		selectNode = getSelectNode();
		this.workFlowNode = JecnTreeCommon.getWorkNode();
		// 术语
		termItem.setEnabled(false);
	}

	private JecnTreeNode getSelectNode() {
		List<JecnTreeNode> listJecnTreeNode = JecnTreeCommon.getSelectJecnTreeNodes(JecnDesignerMainPanel
				.getDesignerMainPanel().getTree());
		if (listJecnTreeNode.size() == 1) {
			return listJecnTreeNode.get(0);
		}
		return null;
	}

	/**
	 * 图形通用
	 * 
	 * @param jecnFlowElemPopupMenu
	 * @param popupMenu
	 */
	private void initElement(JecnFlowElemPopupMenu jecnFlowElemPopupMenu, JPopupMenu popupMenu) {
		// 流程地图菜单项
		jecnFlowElemPopupMenu.initTotalMapMenuItems();
		// 剪切
		jecnFlowElemPopupMenu.getCutMenuItem().setEnabled(true);
		// 复制
		jecnFlowElemPopupMenu.getCopyMenuItem().setEnabled(true);
		// 删除
		jecnFlowElemPopupMenu.getDeleteMenuItem().setEnabled(true);
		// 截图
		jecnFlowElemPopupMenu.getScreenshotMenuItem().setEnabled(true);
		// 置于顶层
		jecnFlowElemPopupMenu.getFrontLayerMenuItem().setEnabled(true);
		// 置于底层
		jecnFlowElemPopupMenu.getBackLayerMenuItem().setEnabled(true);
		// 上移一层
		jecnFlowElemPopupMenu.getUpLayerMenuItem().setEnabled(true);
		// 下移一层
		jecnFlowElemPopupMenu.getDownLayerMenuItem().setEnabled(true);
		// 模板片段
		jecnFlowElemPopupMenu.getTemplateMenuItem().setEnabled(true);
		// 元素属性
		if (JecnConfigTool.isIflytekLogin()) {
			jecnFlowElemPopupMenu.getFlowElemAttrMenuItem().setEnabled(false);
		} else {
			jecnFlowElemPopupMenu.getFlowElemAttrMenuItem().setEnabled(true);
		}
	}

	/**
	 * 流程其他
	 */
	private void initProcessELSE() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		initSubstitutionFlow(jecnFlowElemPopupMenu, popupMenu);
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	private void initSubstitutionFlowMap(JecnFlowElemPopupMenu jecnFlowElemPopupMenu, JPopupMenu popupMenu) {
		// 是否添加批量置换
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		boolean isShow = true;
		if (workflow != null) {
			List<JecnBaseFlowElementPanel> currentSelectElement = workflow.getCurrentSelectElement();
			if (currentSelectElement != null) {
				for (JecnBaseFlowElementPanel eleBean : currentSelectElement) {
					MapElemType mapElemType = eleBean.getFlowElementData().getMapElemType();
					if (mapElemType == mapElemType.Oval || mapElemType == mapElemType.Triangle
							|| mapElemType == mapElemType.FunctionField || mapElemType == mapElemType.Pentagon
							|| mapElemType == mapElemType.RightHexagon || mapElemType == mapElemType.InputHandFigure
							|| mapElemType == mapElemType.IsoscelesTrapezoidFigure) {
						continue;
					} else {
						return;
					}

				}
				popupMenu.add(new JecnReplaceProcessMapPopupMenu());
			}
		}
	}

	private void initSubstitutionFlow(JecnFlowElemPopupMenu jecnFlowElemPopupMenu, JPopupMenu popupMenu) {
		// 是否添加批量置换
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow != null) {
			List<JecnBaseFlowElementPanel> currentSelectElement = workflow.getCurrentSelectElement();
			if (currentSelectElement != null) {
				for (JecnBaseFlowElementPanel eleBean : currentSelectElement) {
					MapElemType mapElemType = eleBean.getFlowElementData().getMapElemType();
					if (mapElemType == mapElemType.ActiveFigureAR || mapElemType == mapElemType.RoundRectWithLine
							|| mapElemType == mapElemType.Rhombus || mapElemType == mapElemType.DataImage
							|| mapElemType == mapElemType.ITRhombus || mapElemType == mapElemType.ActivityImplFigure
							|| mapElemType == mapElemType.ActivityOvalSubFlowFigure
							|| mapElemType == mapElemType.OvalSubFlowFigure || mapElemType == mapElemType.ImplFigure
							|| mapElemType == mapElemType.InterfaceRightHexagon) {
						continue;
					} else {
						return;
					}
				}
				popupMenu.add(new JecnReplaceActivePopupMenu());
			}
		}
	}

	/**
	 * 接口/子流程（活动）菜单初始化
	 */
	private void initProcessImplActivity() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();

		if (!isShowElementAttribute()) {
			// 活动明细
			popupMenu.add(jecnFlowElemPopupMenu.getRoundRectMenuItem());
			jecnFlowElemPopupMenu.getRoundRectMenuItem().setEnabled(true);
			popupMenu.addSeparator();
		}

		initProcessImplCommon(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程 接口
	 */
	private void initProcessImplMenu() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		initProcessImplCommon(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 接口元素菜单初始化
	 * 
	 * @param jecnFlowElemPopupMenu
	 * @param popupMenu
	 */
	private void initProcessImplCommon(JecnFlowElemPopupMenu jecnFlowElemPopupMenu, JPopupMenu popupMenu) {
		if (DrawCommon.isStandardImpl(elementPanel.getFlowElementData().getMapElemType())) {
			popupMenu.add(setConnProcessItem);
			setConnProcessItem.setEnabled(true);
		}

		popupMenu.add(cancelConnProcessItem);
		if (elementPanel.getFlowElementData().getDesignerFigureData().getLinkId() != null) {
			cancelConnProcessItem.setEnabled(true);
		}
		if (!JecnConfigTool.isIflytekLogin()) {
			popupMenu.add(createSonProcessItem);
			this.createSonProcessItem.setEnabled(true);
			popupMenu.add(relaUpProcessItem);
			popupMenu.add(relaDownProcessItem);
			popupMenu.add(relaInterfaceProcessItem);
			popupMenu.add(relaSonProcessItem);
		}
		if (selectNode != null && JecnDesignerCommon.isProcess(selectNode.getJecnTreeBean().getTreeNodeType())) {
			// 选中节点必须是流程
			this.relaUpProcessItem.setEnabled(true);
			this.relaDownProcessItem.setEnabled(true);
			this.relaInterfaceProcessItem.setEnabled(true);
			this.relaSonProcessItem.setEnabled(true);
		}
		popupMenu.addSeparator();
		// 元素置换
		popupMenu.add(new JecnReplaceActivePopupMenu());
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程 图片插入框
	 */
	private void initProcessIconItems() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		// 插入图片
		popupMenu.add(processInsertImageItem);
		processInsertImageItem.setEnabled(true);
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程 角色插入框
	 */
	private void initProcessRoleItems() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();

		if (!isShowElementAttribute()) {
			popupMenu.add(roleDetailItem);
			roleDetailItem.setEnabled(true);
		}
		// 角色置换
		popupMenu.add(new JecnReplaceRolePopupMenu());
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/***
	 * 元素属性是否选中
	 * 
	 * @return
	 */
	private boolean isShowElementAttribute() {
		JecnDesignerModuleShow designModuleShow = JecnDesignerFrame.getDesignModuleShow();
		return designModuleShow.getEleFigureCheckBox().isSelected();
	}

	/**
	 * 流程 文档符号
	 */
	private void initProcessUploadItems() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		// 关联文件
		popupMenu.add(processUplodeFileItem);
		processUplodeFileItem.setEnabled(true);
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程 活动插入框
	 */
	private void initProcessActiveItems() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();

		if (!isShowElementAttribute()) {
			// 活动明细
			popupMenu.add(jecnFlowElemPopupMenu.getRoundRectMenuItem());
			jecnFlowElemPopupMenu.getRoundRectMenuItem().setEnabled(true);
			popupMenu.addSeparator();
		}
		// 元素置换
		popupMenu.add(new JecnReplaceActivePopupMenu());
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 线
	 */
	private void initProcessLine() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
		// 模板片段
		jecnFlowElemPopupMenu.getTemplateMenuItem().setEnabled(false);
	}

	/**
	 * 横竖分割线
	 */
	private void intProcessVHLine() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
		// 模板片段
		jecnFlowElemPopupMenu.getTemplateMenuItem().setEnabled(false);
		// 剪切
		jecnFlowElemPopupMenu.getCutMenuItem().setEnabled(false);
		// 复制
		jecnFlowElemPopupMenu.getCopyMenuItem().setEnabled(false);
		// 粘贴
		jecnFlowElemPopupMenu.getPasteMenuItem().setEnabled(false);
		// 截图
		jecnFlowElemPopupMenu.getScreenshotMenuItem().setEnabled(false);
	}

	/**
	 * 流程右键元素菜单初始化
	 * 
	 * @param flowElementPanel
	 */
	private void initProcessMenu(JecnBaseFlowElementPanel flowElementPanel) {
		// 图形
		if (flowElementPanel instanceof JecnBaseFigurePanel) {
			elementPanel = (JecnBaseFigurePanel) flowElementPanel;
			MapElemType elemType = elementPanel.getFlowElementData().getMapElemType();
			switch (elemType) {
			case OvalSubFlowFigure:// 子流程
			case ImplFigure:// 接口
			case InterfaceRightHexagon:
				initProcessImplMenu();
				break;
			case IconFigure:// 插入图片
				initProcessIconItems();
				break;
			case RoleFigure: // 角色
			case CustomFigure: // 客户
			case VirtualRoleFigure:// 虚拟角色
			case RoleFigureAR:
				initProcessRoleItems();
				break;
			case FileImage:// 文档 29
				initProcessUploadItems();
				break;
			case ActiveFigureAR:
			case RoundRectWithLine:
			case Rhombus:
			case DataImage:
			case ITRhombus:
			case ExpertRhombusAR: // 评审
				this.initProcessActiveItems();
				break;
			case ActivityImplFigure:// 接口（活动）
			case ActivityOvalSubFlowFigure:
				// 接口/子流程（活动）菜单初始化
				initProcessImplActivity();
				break;
			case TermFigureAR:
				initTermFigureMenus();
				break;
			default:
				this.initProcessELSE();
				break;
			}
		}// 线
		else if (flowElementPanel instanceof JecnBaseLinePanel) {
			initProcessLine();
			if (flowElementPanel instanceof JecnBaseVHLinePanel) {
				this.intProcessVHLine();
			}
		}
	}

	/**
	 * 流程架构 图片插入框
	 */
	private void initProcessMapIconItems() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		popupMenu.add(processMapInsertImageItem);
		processMapInsertImageItem.setEnabled(true);
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程架构 文档符号
	 */
	private void initProcessMapUploadItems() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		// 关联文件
		if (!JecnDesignerCommon.isRelatedTotalMap()) {
			popupMenu.add(mapUplodeFileItem);
			mapUplodeFileItem.setEnabled(true);
		}
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程架构 文档符号
	 */
	private void initProcessMapRelaRuleItem() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		popupMenu.add(setConnProcessMapItem);
		setConnProcessMapItem.setEnabled(true);
		popupMenu.add(relaRuleItem);
		relaRuleItem.setEnabled(true);
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程架构 设置流程或流程地图链接的右键菜单
	 */
	private void initProcessMapLinkProcessItems(JecnBaseFlowElementPanel elementPanel, JecnTreeNode selectNode,
			boolean isShow) {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		// 设置链接
		popupMenu.add(setConnProcessMapItem);
		setConnProcessMapItem.setEnabled(true);
		// 取消链接
		popupMenu.add(cancelConnProcessMapItem);
		// 存在链接时候，取消链接才能显示TRUE
		if (elementPanel.getFlowElementData().getDesignerFigureData().getLinkId() != null) {
			cancelConnProcessMapItem.setEnabled(true);
		}

		if (selectNode != null && selectNode.getJecnTreeBean().getNodeType() == 0) {// 非流程架构图不允许创建架构和清单
			// 创建流程地图
			popupMenu.add(createProcessMapItem);
			createProcessMapItem.setEnabled(true);
			// 创建流程
			popupMenu.add(createProcessItem);
			createProcessItem.setEnabled(true);
		}

		// 只允许管理员编辑流程架构时，设计者菜单控制
		if (!JecnDesignerCommon.isAdmin() && JecnConfigTool.isAdminEditFlowMapNode()) {
			createProcessMapItem.setEnabled(false);
			createProcessItem.setEnabled(false);
		}

		// 关联流程地图
		popupMenu.add(relaProcessMapItem);
		if (selectNode != null && selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap) {
			relaProcessMapItem.setEnabled(true);
		}

		// 关联流程
		popupMenu.add(relaProcessItem);
		if (selectNode != null && JecnDesignerCommon.isProcess(selectNode.getJecnTreeBean().getTreeNodeType())) {
			relaProcessItem.setEnabled(true);
		}
		if (!JecnDesignerCommon.isRelatedTotalMap()) {
			// 添加附件
			popupMenu.add(totalMapAddFileItem);
			totalMapAddFileItem.setEnabled(true);
		}

		// 元素置换
		if (isShow) {
			popupMenu.add(new JecnReplaceProcessMapPopupMenu());
		}

		// 描述
		popupMenu.add(jecnFlowElemPopupMenu.getTotalMapInstructItem());
		jecnFlowElemPopupMenu.getTotalMapInstructItem().setEnabled(true);
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 流程架构 其他
	 */
	private void initProcessMapELSE() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		initSubstitutionFlowMap(jecnFlowElemPopupMenu, popupMenu);
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	/**
	 * 线
	 */
	private void initProcessMapLine() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
		// 模板片段
		jecnFlowElemPopupMenu.getTemplateMenuItem().setEnabled(false);
	}

	/**
	 * 横竖分割线
	 */
	private void intProcessMapVHLine() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
		// 模板片段
		jecnFlowElemPopupMenu.getTemplateMenuItem().setEnabled(false);
		// 剪切
		jecnFlowElemPopupMenu.getCutMenuItem().setEnabled(false);
		// 复制
		jecnFlowElemPopupMenu.getCopyMenuItem().setEnabled(false);
		// 粘贴
		jecnFlowElemPopupMenu.getPasteMenuItem().setEnabled(false);
		// 截图
		jecnFlowElemPopupMenu.getScreenshotMenuItem().setEnabled(false);
	}

	/**
	 * 流程架构 右键元素菜单
	 * 
	 * @param flowElementPanel
	 */
	private void initProcessMapMenu(JecnBaseFlowElementPanel flowElementPanel) {

		if (MapElemType.FreeText == flowElementPanel.getFlowElementData().getEleShape()) {
			this.initProcessMapELSE();
		} else if (flowElementPanel instanceof JecnBaseFigurePanel) {// 图形
			elementPanel = (JecnBaseFigurePanel) flowElementPanel;
			MapElemType elemType = elementPanel.getFlowElementData().getMapElemType();
			switch (elemType) {
			case IconFigure: // 图片插入框
				this.initProcessMapIconItems();
				break;
			case FileImage:// 文档 29
				this.initProcessMapUploadItems();
				break;
			case SystemFigure:// 制度
				this.initProcessMapRelaRuleItem();
				break;
			case Oval:// 圆形
			case Triangle:// 三角形
			case FunctionField:// 矩形:
			case Pentagon:// 五边形:
			case RightHexagon:// 箭头六边形:
			case InputHandFigure:// 手动输入
			case IsoscelesTrapezoidFigure:// 等腰梯形
				this.initProcessMapLinkProcessItems(elementPanel, selectNode, true);
				break;
			case MapRhombus:// 菱形
			case ModelFigure:// 泳池:
				this.initProcessMapLinkProcessItems(elementPanel, selectNode, false);
				break;
			default:
				this.initProcessMapELSE();
				break;
			}
		}// 线
		else if (flowElementPanel instanceof JecnBaseLinePanel) {
			initProcessMapLine();
			if (flowElementPanel instanceof JecnBaseVHLinePanel) {
				this.intProcessMapVHLine();
			}
		}
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:根据元素类型激活某些菜单
	 */
	public void setElemMenuItemEnabled(JecnBaseFlowElementPanel flowElementPanel) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		if (workflow.getCurrentSelectElement().size() > 1) {
			return;
		}
		mapType = workflow.getFlowMapData().getMapType();
		elementPanel = null;

		switch (workflow.getFlowMapData().getMapType()) {
		case partMap:// 流程图
			JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu().setElementPanel(flowElementPanel);
			initProcessMenu(flowElementPanel);
			break;
		case totalMap:
		case totalMapRelation:// 流程架构
			JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu().setElementPanel(flowElementPanel);
			initProcessMapMenu(flowElementPanel);
			break;
		case orgMap:// 组织图
			elementPanel = (JecnBaseFigurePanel) flowElementPanel;
			MapElemType elemType = elementPanel.getFlowElementData().getMapElemType();
			JecnDrawMainPanel.getMainPanel().getOrgMapFlowElemPopupMenu().setElementPanel(flowElementPanel);
			switch (elemType) {
			case Rect:// 组织
				setConnOrgItem.setEnabled(true);
				if (elementPanel.getFlowElementData().getDesignerFigureData().getLinkId() != null) {
					cancelConnOrgItem.setEnabled(true);
				}
				this.relaOrgItem.setEnabled(true);
				break;
			case IconFigure: // 图片插入框
				this.orgInsertImageItem.setEnabled(true);
				break;
			case FileImage:// 文档 29
				orgUplodeFileItem.setEnabled(true);
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:多选元素类型激活某些菜单
	 */
	public void setElemMenusItemEnabled() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		if (workflow.getCurrentSelectElement().size() < 1) {
			return;
		}
		mapType = workflow.getFlowMapData().getMapType();
		elementPanel = null;
		switch (workflow.getFlowMapData().getMapType()) {
		case partMap:// 流程图
			initProcessELSE();
		case totalMap:// 流程架构
			initProcessMapELSE();
			break;
		default:
			break;
		}
	}

	/**
	 * 改变是否恢复本地备份数据按钮的值（流程图、流程地图、组织图）
	 * 
	 * @author weidp
	 * @date 2014-11-3 下午04:07:03
	 * @param recoveryMenuItem
	 * @param mapData
	 */
	private void changeRecoveryMenuItemEnabled(JMenuItem recoveryMenuItem, JecnFlowMapData mapData) {
		// 获取备份文件路径
		String bakFilePath = JecnUtil.getFlowBakPath(mapData);
		File bakDir = new File(bakFilePath);
		// 该文件夹下存在文件时激活
		if (bakDir.listFiles() != null) {
			recoveryMenuItem.setEnabled(true);
		}
	}

	/**
	 * @author yxw 2012-12-11
	 * @description:激活相应的面板右键菜单
	 */
	public void setWorkflowMenuItemEnabled() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		switch (workflow.getFlowMapData().getMapType()) {
		case partMap:// 流程图
			if (workflow.getFlowMapData().getDesignerData().getModeType() == ModeType.none) {
				roleActiveModeItem.setEnabled(true);
				checFileControlItem.setEnabled(true);
				if (JecnConfigTool.checkoutVisible()) {
					flowCheckout.setEnabled(true);
				}
				roleCheckout.setEnabled(true);
				flowCheckout.setVisible(JecnConfigTool.checkoutVisible());
			}
			// 改变流程图 是否恢复本地备份数据按钮
			changeRecoveryMenuItemEnabled(processDataRecoveryMenuItem, workflow.getFlowMapData());
			break;
		case totalMap:// 流程地图
			// 改变流程地图 是否恢复本地备份数据按钮
			changeRecoveryMenuItemEnabled(processMapDataRecoveryMenuItem, workflow.getFlowMapData());
			break;
		case orgMap:// 组织图
			// 改变组织图 是否恢复本地备份数据按钮
			changeRecoveryMenuItemEnabled(orgDataRecoveryMenuItem, workflow.getFlowMapData());
			break;
		}

	}

	private void setConnProcessMap() {
		if (elementPanel.getFlowElementData().getMapElemType() == MapElemType.SystemFigure) {
			List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
			if (elementPanel.getFlowElementData().getDesignerFigureData().getLinkId() != null) {
				JecnTreeBean treeBean = new JecnTreeBean();
				try {
					RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(
							elementPanel.getFlowElementData().getDesignerFigureData().getLinkId());
					if (ruleT != null) {
						treeBean.setId(ruleT.getId());
						treeBean.setName(ruleT.getRuleName());
						list.add(treeBean);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			RuleChooseDialog d = new RuleChooseDialog(list, 1L);
			d.removeEmptyBtn();
			d.setSelectMutil(false);
			d.setVisible(true);
			if (d.isOperation()) {
				Long newLinkId = null;
				if (list.size() == 1) {
					newLinkId = list.get(0).getId();
				}
				Long oldLinkId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
				undoData = new JecnUndoRedoData();
				// 操作后数据
				redoData = new JecnUndoRedoData();
				undoData.recodeFlowElement(elementPanel);// 记录撤销操作
				if (DrawCommon.checkLongSame(newLinkId, oldLinkId)) {
					return;
				}
				JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
				if (list.size() == 1) {
					if (DrawCommon.checkLongSame(elementPanel.getFlowElementData().getDesignerFigureData().getLinkId(),
							list.get(0).getId())) {
						return;
					}
					// 元素增加链接ID
					elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(list.get(0).getId());
					JecnDesignerCommon.addElemLink(elementPanel, mapType);
					elementPanel.getFlowElementData().setFigureText(list.get(0).getName());
					elementPanel.updateUI();
					JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
				} else {
					elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(null);
					JecnDesignerCommon.addElemLink(elementPanel, mapType);
				}
				redoData.recodeFlowElement(elementPanel);
				unredoProcess(undoData, redoData);
			}
		} else {
			FlowSettingsLinkDialog d = new FlowSettingsLinkDialog(workFlowNode, elementPanel, mapType);
			d.setVisible(true);
		}

	}

	private void setConnProcess() {
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(elementPanel);// 记录撤销操作
		FlowSetFlowInterfaceDialogNew dialogNew = new FlowSetFlowInterfaceDialogNew(elementPanel, mapType);
		dialogNew.setVisible(true);
		redoData.recodeFlowElement(elementPanel);
		unredoProcess(undoData, redoData);
	}

	private void createProcessMap() {
		if (elementPanel == null) {
			return;
		}
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(elementPanel);// 记录撤销操作
		try {
			String name = elementPanel.getFlowElementData().getFigureText();
			if (name != null) {
				name = name.trim();
			}
			// 验证名称格式
			String vn = JecnUserCheckUtil.checkName(name);
			if (!DrawCommon.isNullOrEmtryTrim(vn)) {
				JecnTipPopupMenu tTipPopupMenu = new JecnTipPopupMenu();
				tTipPopupMenu.setText(vn);
				tTipPopupMenu.getPopupMenu().show(JecnDrawMainPanel.getMainPanel().getWorkflow(),
						elementPanel.getX() + elementPanel.getWidth(), elementPanel.getY());
				return;
			}
			// 判断是否重名
			if (ConnectionPool.getProcessAction().validateAddName(name, workFlowNode.getJecnTreeBean().getId(), 0,
					JecnConstants.projectId)) {
				JecnTipPopupMenu tTipPopupMenu = new JecnTipPopupMenu();
				// 名称已存在
				tTipPopupMenu.setText(JecnProperties.getValue("nameHaved"));
				tTipPopupMenu.getPopupMenu().show(JecnDrawMainPanel.getMainPanel().getWorkflow(),
						elementPanel.getX() + elementPanel.getWidth(), elementPanel.getY());
				return;
			}
			// 获取父节点的密级，部门权限，岗位权限

			LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
					workFlowNode.getJecnTreeBean().getId(), 0);

			// 岗位权限
			String posIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosList());
			// 组织权限
			String deptIds = JecnDesignerCommon.getLook(lookPopedomBean.getOrgList());
			// 岗位组权限
			String posGroupIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosGroupList());
			JecnFlowStructureT jecnFlowStructureT = JecnProcessCommon.createProcessMap(name, lookPopedomBean
					.getIsPublic(), workFlowNode, "", new JecnMainFlowT(), posIds, deptIds, JecnDesignerMainPanel
					.getDesignerMainPanel().getTree(), posGroupIds);
			// 元素增加链接ID
			elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(jecnFlowStructureT.getFlowId());
			JecnDesignerCommon.addElemLink(elementPanel, mapType);
			redoData.recodeFlowElement(elementPanel);
			unredoProcess(undoData, redoData);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		} catch (Exception e1) {
			log.error("右键元素创建流程地图出错", e1);
		}

	}

	private void createProcess() {
		if (elementPanel == null) {
			return;
		}
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(elementPanel);// 记录撤销操作
		redoData = new JecnUndoRedoData();
		try {

			String name = elementPanel.getFlowElementData().getFigureText();
			if (name != null) {
				name = name.trim();
			}
			// 验证名称格式
			String vn = JecnUserCheckUtil.checkName(name);
			if (!DrawCommon.isNullOrEmtryTrim(vn)) {
				JecnTipPopupMenu tTipPopupMenu = new JecnTipPopupMenu();
				tTipPopupMenu.setText(vn);
				tTipPopupMenu.getPopupMenu().show(JecnDrawMainPanel.getMainPanel().getWorkflow(),
						elementPanel.getX() + elementPanel.getWidth(), elementPanel.getY());
				return;
			}
			// 判断是否重名
			if (ConnectionPool.getProcessAction().validateAddName(name, workFlowNode.getJecnTreeBean().getId(), 1,
					JecnConstants.projectId)) {
				JecnTipPopupMenu tTipPopupMenu = new JecnTipPopupMenu();
				// 名称已存在
				tTipPopupMenu.setText(JecnProperties.getValue("nameHaved"));
				tTipPopupMenu.getPopupMenu().show(JecnDrawMainPanel.getMainPanel().getWorkflow(),
						elementPanel.getX() + elementPanel.getWidth(), elementPanel.getY());
				return;
			}
			// 获取父节点的密级，部门权限，岗位权限
			LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
					workFlowNode.getJecnTreeBean().getId(), 0);

			// 岗位权限
			String posIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosList());
			// 组织权限
			String deptIds = JecnDesignerCommon.getLook(lookPopedomBean.getOrgList());
			// 岗位组权限
			String posGroupIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosGroupList());

			// 流程图基本信息表
			JecnFlowBasicInfoT jecnFlowBasicInfoT = new JecnFlowBasicInfoT();
			jecnFlowBasicInfoT.setFlowName(name);
			jecnFlowBasicInfoT.setIsXorY(0L);
			JecnFlowStructureT jecnFlowStructureT = JecnProcessCommon.createProcess(name, "", new Long(lookPopedomBean
					.getIsPublic()), deptIds, posIds, "", workFlowNode, jecnFlowBasicInfoT, JecnDesignerMainPanel
					.getDesignerMainPanel().getTree(), posGroupIds);

			// 元素增加链接ID
			elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(jecnFlowStructureT.getFlowId());
			JecnDesignerCommon.addElemLink(elementPanel, mapType);
			redoData.recodeFlowElement(elementPanel);
			unredoProcess(undoData, redoData);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		} catch (Exception e1) {
			log.error("JecnWorkflowMenuItem createProcess is error", e1);
		}
	}

	/**
	 * @author yxw 2012-8-22
	 * @description:流程关联
	 */
	private void relationNode() {
		Long relaId = selectNode.getJecnTreeBean().getId();
		if (!DrawCommon.checkLongSame(relaId, elementPanel.getFlowElementData().getDesignerFigureData().getLinkId())) {
			undoData = new JecnUndoRedoData();
			// 操作后数据
			redoData = new JecnUndoRedoData();
			undoData.recodeFlowElement(elementPanel);
			elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(relaId);
			JecnDesignerCommon.addElemLink(elementPanel, mapType);
			setElemetPanelName();
			redoData.recodeFlowElement(elementPanel);
			unredoProcess(undoData, redoData);
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		}
	}

	/**
	 * 记录撤销操作
	 * 
	 * @param undoData
	 * @param redoData
	 */
	private void unredoProcess(JecnUndoRedoData undoData, JecnUndoRedoData redoData) {
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		JecnDrawMainPanel.getMainPanel().getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
	}

	/**
	 * @author yxw 2012-8-22
	 * @description:流程关联
	 */
	private void relationProcessNode(String lineType) {
		Long relaId = selectNode.getJecnTreeBean().getId();
		if (DrawCommon.checkStringSame(elementPanel.getFlowElementData().getDesignerFigureData().getLineType(),
				lineType)
				&& DrawCommon.checkLongSame(relaId, elementPanel.getFlowElementData().getDesignerFigureData()
						.getLinkId())) {
			return;
		}
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(elementPanel);
		elementPanel.getFlowElementData().getDesignerFigureData().setLineType(lineType);
		// 元素增加链接ID
		elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(relaId);
		JecnDesignerCommon.addElemLink(elementPanel, mapType);
		setElemetPanelName();
		redoData.recodeFlowElement(elementPanel);
		unredoProcess(undoData, redoData);
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
	}

	private void initTermFigureMenus() {
		JecnFlowElemPopupMenu jecnFlowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
		JPopupMenu popupMenu = jecnFlowElemPopupMenu.getFlowElemPopupMenu();
		popupMenu.removeAll();
		// 插入图片
		popupMenu.add(termItem);
		termItem.setEnabled(true);
		popupMenu.addSeparator();
		this.initElement(jecnFlowElemPopupMenu, popupMenu);
	}

	// 设置元素的名称
	private void setElemetPanelName() {
		elementPanel.getFlowElementData().setFigureText(selectNode.getJecnTreeBean().getName());
		elementPanel.updateUI();
	}

	private void setElemetPanelName(JecnTreeBean jecnTreeBean) {
		elementPanel.getFlowElementData().setFigureText(jecnTreeBean.getName());
		elementPanel.updateUI();
	}

	/**
	 * @author yxw 2012-12-11
	 * @description:关联前验证
	 * @return true可以头联 false不可以关联
	 */
	private boolean relationNodeVerdict() {
		if (selectNode == null || !JecnDesignerCommon.isProcess(selectNode.getJecnTreeBean().getTreeNodeType())) {
			JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties
					.getValue("selectProcessNode"));
			return false;
		}
		return true;
	}

	/**
	 * @author yxw 2012-12-6
	 * @description:组织元素设置链接
	 */
	private void setConnOrg() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		Long orgId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(elementPanel);// 记录撤销操作
		try {
			JecnFlowOrg jecnFlowOrg = ConnectionPool.getOrganizationAction().getOrgInfo(orgId);
			if (jecnFlowOrg != null) {
				JecnTreeBean jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(orgId);
				jecnTreeBean.setName(jecnFlowOrg.getOrgName());
				list.add(jecnTreeBean);
			}
		} catch (Exception e1) {
			log.error("JecnWorkflowMenuItem setConnOrg is error", e1);
		}
		OrgChooseDialog orgChooseDialog = new OrgChooseDialog(list);
		orgChooseDialog.setSelectMutil(false);
		orgChooseDialog.setVisible(true);
		if (orgChooseDialog.isOperation()) {
			if (list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(jecnTreeBean.getId());
				JecnDesignerCommon.addElemLink(elementPanel, mapType);
				setElemetPanelName(jecnTreeBean);
				redoData.recodeFlowElement(elementPanel);
				unredoProcess(undoData, redoData);
				JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);

			}

		}
	}

	/**
	 * @author yxw 2012-12-7
	 * @description:取消链接
	 */
	private void cancelConn() {
		elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(null);
		elementPanel.getFlowElementData().getDesignerFigureData().setLineType("");
		JecnDesignerLinkPanel designerLinkPanel = elementPanel.getDesLinkPanel();
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		designerLinkPanel.removeAll();
		designerLinkPanel.updateUI();
	}

	/**
	 * @author yxw 2012-12-7
	 * @description:插入图片
	 */
	private void insertImage() {

		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		FileChooseDialog fileChooseDialog = new FileChooseDialog(list);
		// 单选
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.removeEmptyBtn();
		((HighEfficiencyFileTree) fileChooseDialog.getjTree()).setChooseType(1);
		// 只能选择图片
		fileChooseDialog.setChooseType(17);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			Long newLinkId = null;
			if (list.size() == 1) {
				newLinkId = list.get(0).getId();
			}
			Long oldLinkId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
			if (DrawCommon.checkLongSame(newLinkId, oldLinkId)) {
				return;
			}
			undoData = new JecnUndoRedoData();
			// 操作后数据
			redoData = new JecnUndoRedoData();
			undoData.recodeFlowElement(elementPanel);// 记录撤销操作
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
			if (list.size() > 0) {
				JecnTreeBean o = list.get(0);
				try {
					FileOpenBean fileOpenBean = ConnectionPool.getFileAction().openFile(o.getId());
					String path = JecnUtil.downFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
					if (DrawCommon.isNullOrEmtryTrim(path)) {
						// 图片不存在！
						JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("imageNotExists"));
						return;
					}
					IconFigure iconFigure = (IconFigure) elementPanel;

					// 获取上传图片的原始大小
					getOriFigureSize(path, iconFigure);

					java.awt.Image img = Toolkit.getDefaultToolkit().getImage(path);
					iconFigure.setImg(img);
					iconFigure.repaint();
					elementPanel.getFlowElementData().setFigureText(null);
					elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(o.getId());
					elementPanel.updateUI();
					redoData.recodeFlowElement(elementPanel);
					unredoProcess(undoData, redoData);

					// 计算面板大小
					JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSize(elementPanel);
				} catch (Exception e) {
					log.error("JecnWorkflowMenuItem insertImage is error", e);
				}
			}
		}
	}

	/**
	 * 获取上传图片的原始大小
	 * 
	 * @param imagePath
	 * @param iconFigure
	 */
	private void getOriFigureSize(String imagePath, IconFigure iconFigure) {
		BufferedImage sourceImg;
		try {
			sourceImg = ImageIO.read(new FileInputStream(imagePath));
			int width = sourceImg.getWidth();
			int height = sourceImg.getHeight();
			iconFigure.setSize(width, height);
			iconFigure.getJecnFigureDataCloneable().reSetAttributes(iconFigure.getLocation(), iconFigure.getSize(),
					JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
			// 设置当前状态下显示点位置
			JecnPaintFigureUnit.reSetCurFigure(iconFigure);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 导入初始化
	 * 
	 * @return JMenuItem
	 */
	private JMenuItem initInputMenuItem() {
		// 导入
		JMenuItem inputItem = new JMenuItem(JecnProperties.getValue("importFile") + " Ctrl+O", JecnFileUtil
				.getToolBarImageIcon(ToolBarElemType.fileOpen.toString()));

		// 导入
		inputItem.setOpaque(false);

		inputItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getFileToolBarItemPanel().getOpenMapBtn());
				// 执行
				JecnDrawMainPanel.getMainPanel().actionPerformed(e);
			}

		});

		return inputItem;
	}

	/**
	 * 
	 * 导出初始化
	 * 
	 * @return JMenuItem
	 */
	private JMenuItem initInOutputMenuItem() {
		// 导出
		JMenuItem outputItem = new JMenuItem(JecnProperties.getValue("exportFile") + " Ctrl+Shift+A", JecnFileUtil
				.getToolBarImageIcon(ToolBarElemType.oftenSaveAS.toString()));

		// 导出
		outputItem.setOpaque(false);

		outputItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				e.setSource(JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel().getSaveAsBtn());
				// 执行
				JecnDrawMainPanel.getMainPanel().actionPerformed(e);
			}

		});
		return outputItem;
	}

	/**
	 * 流程地图添加附件(可设置连接的元素)
	 */
	private void totalMapAddFile() {
		if (!DrawCommon.isInfoFigure(elementPanel.getFlowElementData().getMapElemType())) {// 是否为设置超链接的图形
			return;
		}
		// 获取地图数据层
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(elementPanel);// 记录撤销操作
		JecnFigureData figureData = elementPanel.getFlowElementData();
		List<JecnFigureFileTBean> figureFileTList = figureData.getListFigureFileTBean();
		// 文件集合
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		JecnTreeBean jecnTreeBean = null;
		for (JecnFigureFileTBean jecnFigureFileTBean : figureFileTList) {
			jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(jecnFigureFileTBean.getFileId());
			jecnTreeBean.setName(jecnFigureFileTBean.getFileName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.file);
			list.add(jecnTreeBean);
		}

		FileChooseDialog fileChooseDialog = new FileChooseDialog(list);
		// 多选
		fileChooseDialog.setSelectMutil(true);
		// 只能选择文件
		fileChooseDialog.setChooseType(2);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {// 是否操作 true是操作，false是没有
			try {
				if (!isUpdate(figureFileTList, list)) {
					return;
				}
				List<JecnFigureFileTBean> deleteFileTList = new ArrayList<JecnFigureFileTBean>();

				List<Long> listFile = new ArrayList<Long>();
				for (JecnTreeBean treeBean : list) {// 循环添加数据
					long fileId = treeBean.getId();

					// 集合是否存在选择的文件
					JecnFigureFileTBean figureFileT = getJecnFigureFileTBean(figureFileTList, fileId);
					if (figureFileT == null) {// 不存在，添加
						figureFileT = new JecnFigureFileTBean();
						figureFileT.setFileId(fileId);
						figureFileT.setFigureType(figureData.getMapElemType().toString());
						figureFileT.setFigureId(figureData.getFlowElementId());
						figureFileT.setFileName(treeBean.getName());
						figureFileTList.add(figureFileT);
					}
					// 记录存在的文件集合
					listFile.add(fileId);
				}

				// 删除不存在的记录
				for (JecnFigureFileTBean figureFileT : figureFileTList) {
					if (!listFile.contains(figureFileT.getFileId())) {
						deleteFileTList.add(figureFileT);
					}
				}
				figureFileTList.removeAll(deleteFileTList);
				// 面板保存标识
				JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
				// TODO
				JecnDesignerCommon.addElemLink(elementPanel, mapType);
				redoData.recodeFlowElement(elementPanel);
				unredoProcess(undoData, redoData);
				elementPanel.updateUI();

			} catch (Exception e) {
				log.error("JecnWorkflowMenuItem totalMapAddFile is error", e);
			}
		}
	}

	private boolean isUpdate(List<JecnFigureFileTBean> oldFigureFileTBeans, List<JecnTreeBean> newTreeBeans) {
		if (oldFigureFileTBeans.size() != newTreeBeans.size()) {
			return true;
		}
		for (JecnTreeBean jecnTreeBean : newTreeBeans) {
			boolean isExist = false;
			for (JecnFigureFileTBean figureFileTBean : oldFigureFileTBeans) {
				if (jecnTreeBean.getId().equals(figureFileTBean.getFileId())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取当前文件ID对应的JecnFigureFileTBean
	 * 
	 * @param figureFileTList
	 * @param fileId
	 * @return
	 */
	private JecnFigureFileTBean getJecnFigureFileTBean(List<JecnFigureFileTBean> figureFileTList, Long fileId) {
		if (figureFileTList == null || fileId == null) {
			return null;
		}
		JecnFigureFileTBean figureFileTBean = null;
		for (JecnFigureFileTBean jecnFigureFileTBean : figureFileTList) {
			if (jecnFigureFileTBean.getFileId().toString().equals(fileId.toString())) {
				return jecnFigureFileTBean;
			}
		}
		return figureFileTBean;

	}

	/**
	 * 是否显示流程面板上的右键菜单的分页设置
	 * 
	 */
	private void showPageSetItem() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().isHFlag()) {// 横向
			pageSetingItem.setOpaque(false);
			pageSetingItem.setEnabled(true);
		} else {
			pageSetingItem.setEnabled(false);
		}
	}

	public JMenuItem getRoleActiveModeItem() {
		return roleActiveModeItem;
	}

	public void setRoleActiveModeItem(JMenuItem roleActiveModeItem) {
		this.roleActiveModeItem = roleActiveModeItem;
	}

	public JMenuItem getChecFileControlItem() {
		return checFileControlItem;
	}

	public void setChecFileControlItem(JMenuItem checFileControlItem) {
		this.checFileControlItem = checFileControlItem;
	}

	public JMenuItem getCreateProcessMapItem() {
		return createProcessMapItem;
	}

	public void setCreateProcessMapItem(JMenuItem createProcessMapItem) {
		this.createProcessMapItem = createProcessMapItem;
	}

	public JMenuItem getCreateProcessItem() {
		return createProcessItem;
	}

	public void setCreateProcessItem(JMenuItem createProcessItem) {
		this.createProcessItem = createProcessItem;
	}

	public JMenuItem getRelaProcessItem() {
		return relaProcessItem;
	}

	public void setRelaProcessItem(JMenuItem relaProcessItem) {
		this.relaProcessItem = relaProcessItem;
	}

	public JMenuItem getRelaUpProcessItem() {
		return relaUpProcessItem;
	}

	public void setRelaUpProcessItem(JMenuItem relaUpProcessItem) {
		this.relaUpProcessItem = relaUpProcessItem;
	}

	public JMenuItem getRelaDownProcessItem() {
		return relaDownProcessItem;
	}

	public void setRelaDownProcessItem(JMenuItem relaDownProcessItem) {
		this.relaDownProcessItem = relaDownProcessItem;
	}

	public JMenuItem getRelaInterfaceProcessItem() {
		return relaInterfaceProcessItem;
	}

	public void setRelaInterfaceProcessItem(JMenuItem relaInterfaceProcessItem) {
		this.relaInterfaceProcessItem = relaInterfaceProcessItem;
	}

	public JMenuItem getRelaSonProcessItem() {
		return relaSonProcessItem;
	}

	public void setRelaSonProcessItem(JMenuItem relaSonProcessItem) {
		this.relaSonProcessItem = relaSonProcessItem;
	}

	public JMenuItem getCreateSonProcessItem() {
		return createSonProcessItem;
	}

	public void setCreateSonProcessItem(JMenuItem createSonProcessItem) {
		this.createSonProcessItem = createSonProcessItem;
	}

	public List<JMenuItem> getProcessMenuItemList() {
		return processMenuItemList;
	}

	public List<JMenuItem> getProcessMapMenuItemList() {
		return processMapMenuItemList;
	}

	public void setProcessMapMenuItemList(List<JMenuItem> processMapMenuItemList) {
		this.processMapMenuItemList = processMapMenuItemList;
	}

	public List<JMenuItem> getOrgMapMenuItemList() {
		return orgMapMenuItemList;
	}

	public void setOrgMapMenuItemList(List<JMenuItem> orgMapMenuItemList) {
		this.orgMapMenuItemList = orgMapMenuItemList;
	}

	public List<JMenuItem> getProcessElemMenuItemList() {
		return processElemMenuItemList;
	}

	public void setProcessElemMenuItemList(List<JMenuItem> processElemMenuItemList) {
		this.processElemMenuItemList = processElemMenuItemList;
	}

	public List<JMenuItem> getProcessMapElemwMenuItemList() {
		return processMapElemwMenuItemList;
	}

	public void setProcessMapElemwMenuItemList(List<JMenuItem> processMapElemwMenuItemList) {
		this.processMapElemwMenuItemList = processMapElemwMenuItemList;
	}

	public List<JMenuItem> getOrgMapElemMenuItemList() {
		return orgMapElemMenuItemList;
	}

	public void setOrgMapElemMenuItemList(List<JMenuItem> orgMapElemMenuItemList) {
		this.orgMapElemMenuItemList = orgMapElemMenuItemList;
	}

}
