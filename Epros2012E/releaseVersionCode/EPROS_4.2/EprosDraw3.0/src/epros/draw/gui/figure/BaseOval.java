/*
 * Oval.java
 *椭圆
 * Created on 2008年11月3日, 上午11:27
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.shape.PaintOvalFigure;

/**
 * 
 * Oval和OvalSubFlowFigure 基类 椭圆
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：4:57:48 PM
 */
public class BaseOval extends JecnBaseFigurePanel {

	private int x1;
	private int y1;

	private PaintOvalFigure figurePaint;

	public BaseOval(JecnFigureData flowElementData) {
		super(flowElementData);
		figurePaint = new PaintOvalFigure(this);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d, x1, 0, 0, 0,
				0, 0, y1, 0, 0, 0, 0, 0, userWidth, userHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintInputHandTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintInputHandTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintInputHandTop(Graphics2D g2d, int shadowCount) {
		JecnPaintEnum.INTANCE.paintHandTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, 0, 0, 0,
				0, 0, y1, 0, 0, 0, 0, 0, shadowCount, shadowCount, userWidth, userHeight);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DHandLow(g2d, 0);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d, int indent3Dvalue) {
		paint3DHandLow(g2d, indent3Dvalue);
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	private void paint3DHandLow(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, 0, 0, 0,
				0, 0, y1, 0, 0, 0, 0, 0, indent3Dvalue, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DOvalLine(g2d, 0);
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paint3DOvalLine(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d,  x1, 0, 0, 0,
				0, 0, y1, 0, 0, 0, 0, 0,indent3Dvalue, indent3Dvalue,userWidth,userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DOvalLine(g2d, linevalue);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d,  x1, 0, 0, 0,
				0, 0, y1, 0, 0, 0, 0, 0, 0,userWidth,userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DAndShadowsPartTop(flowElementData.getMapElemType(), flowElementData, g2d,  x1, 0, 0, 0,
				0, 0, y1, 0, 0, 0, 0, 0, 0, userWidth, userHeight);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		// 阴影
		g2d.setPaint(flowElementData.getShadowColor());
		figurePaint.paintShadows(g2d, shadowCount, x1, y1, userWidth, userHeight);
	}
}
