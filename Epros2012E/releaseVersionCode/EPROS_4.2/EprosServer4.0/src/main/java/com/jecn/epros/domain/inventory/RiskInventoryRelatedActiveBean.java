package com.jecn.epros.domain.inventory;

/**
 * 控制目标的关联活动
 *
 * @author user
 */
public class RiskInventoryRelatedActiveBean {
    /**
     * 活动主键ID JecnFlowStructureImage
     */
    private Long activeId;
    /**
     * 活动名称 JecnFlowStructureImage
     */
    private String activeName;

    /**
     * 控制点编号 JecnControlPoint
     */
    private String controlCode;

    /**
     * 控制活动简描述 即活动说明 JecnFlowStructureImage
     */
    private String activeShow;

    /**
     * 流程角色名称 JecnFlowStructureImage JecnRoleActive
     */
    private String roleName;

    /**
     * 流程ID JecnFlowStructure
     */
    private Long flowId;
    /**
     * 流程名称 JecnFlowStructure
     */
    private String flowName;

    /**
     * 责任部门
     **/
    private String orgName;
    /**
     * 是否是关键控制点：0：是，1：否 JecnControlPoint
     */
    private int keyPoint;
    /**
     * 控制频率 0:随时 1:日 2: 周 3:月 4:季度 5:年度  JecnControlPoint
     */
    private int frequency;
    /**
     * 控制方法 0:收到1:自动 JecnControlPoint
     */
    private int method;
    /**
     * 控制类型 0:预防性1:发现性  JecnControlPoint
     */
    private int type;

    public Long getActiveId() {
        return activeId;
    }

    public void setActiveId(Long activeId) {
        this.activeId = activeId;
    }

    public String getControlCode() {
        return controlCode;
    }

    public void setControlCode(String controlCode) {
        this.controlCode = controlCode;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public String getActiveShow() {
        return activeShow;
    }

    public void setActiveShow(String activeShow) {
        this.activeShow = activeShow;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getKeyPoint() {
        return keyPoint;
    }

    public void setKeyPoint(int keyPoint) {
        this.keyPoint = keyPoint;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
