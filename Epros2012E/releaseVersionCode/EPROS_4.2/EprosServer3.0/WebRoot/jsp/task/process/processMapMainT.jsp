<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><s:text name="flowMapChartForDetails"></s:text></title>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript"
			src="${basePath}js/epros_${language}_${country}.js"></script>
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js"></script>
		<script type="text/javascript" src="${basePath}js/common.js"></script>
        <script type="text/javascript" src="${basePath}js/firstPage.js" charset="UTF-8"></script>
        <script type="text/javascript" src="${basePath}js/browserDetect.js" charset="UTF-8"></script>
        <script type="text/javascript">
			var basePath = "${basePath}";
		</script>
	<script type="text/javascript">
Ext.onReady(function() {
	var processMapMainPanel = new Ext.TabPanel({
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				region : 'center',
				items : [{
					title : i_processMap,// 流程地图
					id : "ruleSysContent_id",
					autoLoad : "${basePath}"
							+ "jsp/processSys/"+processMapContainerJsp+"?isPub=false&mapID="
							+ "${param.flowId}"+"&mapName="+"${param.name}"
							+ "&mapType=processMap"
				},{
					title : i_fileDescription,// 文件说明
					id : "processOperateDes_id",
					autoLoad :{url:"${basePath}"
							+ "processMapFile.action?isPub=false&flowId="
							+ "${param.flowId}",
							scripts : true,
							callback:function(){
								setShowProcessFileDivWH('processMapFileDiv');
							 }
						}
				}
				//,{
				//	title : i_overviewInfo,// 概况信息
				//	id : "processMapOvervicwInfo_id",
				//	autoLoad : "${basePath}"
				//			+ "processMapBaseInfoT.action?isPub=false&flowId="
				//			+ "${param.flowId}"
				//}
				],
				listeners : {

					'bodyresize' : function(p, width, height) {
						if (p.getActiveTab() == null) {
							return;
						}
						if (p.getActiveTab().getId() == 'processOperateDes_id') {
							setShowProcessFileDivWH('processMapFileDiv');
						}
					},
					'tabchange' : function(tabPanel, panel) {
						if(typeof(panel) == "undefined"){
							return;
						}
						if (panel.getId() == 'processOperateDes_id') {
							setShowProcessFileDivWH('processMapFileDiv');
						}

					}

					}				
			});
	if(!${mapFileShow}){
		processMapMainPanel.remove('processOperateDes_id');
	}
	var northPanle=new Ext.Panel({
		height:20,
		region : 'north',
		contentEl:'homePage_id'				
	});
	new Ext.Viewport({
		layout : 'border',
		items : [northPanle,processMapMainPanel]
	});
});

</script>
	</head>

	<body>
		<div id="homePage_id" style="height: 25px; width: 100%; overflow: hidden;"
				align="right" class="gradient minWidth">
				<%@include file="/navigation.jsp"%>
		</div>
	</body>
</html>