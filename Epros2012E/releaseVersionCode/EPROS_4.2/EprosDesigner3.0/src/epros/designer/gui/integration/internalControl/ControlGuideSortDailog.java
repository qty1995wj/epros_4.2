package epros.designer.gui.integration.internalControl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

/**
 * 内控指引知识库节点排序
 * 
 * @author Administrator
 * 
 */
public class ControlGuideSortDailog extends JecnSortDialog {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(ControlGuideSortDailog.class);

	/**
	 * 内控指引知识库节点排序构造方法
	 * 
	 * @param selectNode
	 *            当前选择节点
	 * @param tree
	 *            树
	 */
	public ControlGuideSortDailog(JecnTreeNode selectNode, JecnTree tree) {
		super(selectNode, tree);
	}

	/**
	 * 获得子集的集合
	 */
	@Override
	public List<JecnTreeBean> getChild() {
		try {
			return ConnectionPool.getControlGuideAction()
					.getChildJecnControlGuides(
							this.getSelectNode().getJecnTreeBean().getId(),
							JecnConstants.projectId);
		} catch (Exception e) {
			log.error("ControlGuideSortDailog getChild is error", e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	/**
	 * 修改数据
	 */
	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			ConnectionPool.getControlGuideAction().updateSortControlGuide(list,
					this.getSelectNode().getJecnTreeBean().getId(),
					JecnConstants.getUserId());
			return true;
		} catch (Exception e) {
			log.error("ControlGuideSortDailog saveData is error！", e);
		}
		return false;
	}

}
