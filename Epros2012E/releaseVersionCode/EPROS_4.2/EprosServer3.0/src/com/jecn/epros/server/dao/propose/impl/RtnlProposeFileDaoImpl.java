package com.jecn.epros.server.dao.propose.impl;

import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.propose.IRtnlProposeFileDao;

/***
 * 合理化 建议附件表dao
 * 2013-09-02
 *
 */
public class RtnlProposeFileDaoImpl extends AbsBaseDao<JecnRtnlProposeFile, String> implements IRtnlProposeFileDao {

}
