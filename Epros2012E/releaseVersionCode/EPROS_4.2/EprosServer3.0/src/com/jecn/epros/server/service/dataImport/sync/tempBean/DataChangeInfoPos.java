package com.jecn.epros.server.service.dataImport.sync.tempBean;

import java.util.List;

/**
 * 人员同步岗位变更相关信息
 * 
 * @author Administrator
 * @date： 日期：May 17, 2013 时间：3:36:41 PM
 */
public class DataChangeInfoPos {
	/** 岗位名称 */
	private String oldPosName;
	private String curPosName;
	/** 岗位所属部门名称 */
	private String oldDeptName;
	private String curDeprName;
	/** 增删改查 */
	private int type;
	/** 相关流程 */
	private List<String> oldFlowNameList;
	private List<String> curFlowNameList;
	/** 相关任务 */
	private List<String> oldTaskNameList;
	private List<String> curTaskNameList;

	public String getOldPosName() {
		return oldPosName;
	}

	public void setOldPosName(String oldPosName) {
		this.oldPosName = oldPosName;
	}

	public String getCurPosName() {
		return curPosName;
	}

	public void setCurPosName(String curPosName) {
		this.curPosName = curPosName;
	}

	public String getOldDeptName() {
		return oldDeptName;
	}

	public void setOldDeptName(String oldDeptName) {
		this.oldDeptName = oldDeptName;
	}

	public String getCurDeprName() {
		return curDeprName;
	}

	public void setCurDeprName(String curDeprName) {
		this.curDeprName = curDeprName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<String> getOldFlowNameList() {
		return oldFlowNameList;
	}

	public void setOldFlowNameList(List<String> oldFlowNameList) {
		this.oldFlowNameList = oldFlowNameList;
	}

	public List<String> getCurFlowNameList() {
		return curFlowNameList;
	}

	public void setCurFlowNameList(List<String> curFlowNameList) {
		this.curFlowNameList = curFlowNameList;
	}

	public List<String> getOldTaskNameList() {
		return oldTaskNameList;
	}

	public void setOldTaskNameList(List<String> oldTaskNameList) {
		this.oldTaskNameList = oldTaskNameList;
	}

	public List<String> getCurTaskNameList() {
		return curTaskNameList;
	}

	public void setCurTaskNameList(List<String> curTaskNameList) {
		this.curTaskNameList = curTaskNameList;
	}
}
