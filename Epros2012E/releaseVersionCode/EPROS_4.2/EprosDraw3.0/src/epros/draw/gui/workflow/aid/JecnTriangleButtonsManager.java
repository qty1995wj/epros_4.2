package epros.draw.gui.workflow.aid;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 
 * 小三角形按钮管理类
 * 
 * 主组件：被操作组件，通过其变化能改变辅助组件的显示位置等等状态。
 * 
 * 辅助组件：是通过主组件来操作控制的。
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTriangleButtonsManager {
	/** 进入计时 */
	private Timer enterTimer;
	/** 退出计时 */
	private Timer exitTimer;
	/** 快捷添加图形的主类 */
	private JecnLinkFigureMainProcess linkFigureMainProcess = null;
	/** 主组件:图形 */
	private JecnBaseFlowElementPanel flowElementPanel = null;
	/** 是否显示小三角图标：true显示；false隐藏 */
	private boolean isTriangleShow = false;

	public JecnTriangleButtonsManager(
			JecnLinkFigureMainProcess linkFigureMainProcess) {
		this.linkFigureMainProcess = linkFigureMainProcess;

		// 进入计时
		enterTimer = new Timer(500, new EnterTimerActionListener());
		enterTimer.setRepeats(false);
		// 退出计时
		exitTimer = new Timer(1000, new ExitTimerActionListener());
		exitTimer.setRepeats(false);
	}

	/**
	 * 
	 * 注册JComponent
	 * 
	 * @param component
	 *            JecnBaseFlowElementPanel 组件
	 * @param trianglesButton
	 *            boolean true：点击是小三角形 false：点击不是小三角形
	 * 
	 */
	public void registerComponent(JecnBaseFlowElementPanel component,
			boolean trianglesButton) {
		if (flowElementPanel != null && flowElementPanel == component) {// 是同一个对象
		} else {
			this.flowElementPanel = component;
			if (!trianglesButton) {// 不是小三角形
				// 小三角不显示，快捷图形入口不显示
				this.linkFigureMainProcess.hiddTrianglesBtnAndLinkPanel();
			}
		}

		if (!JecnDrawMainPanel.getMainPanel().getWorkflow().isEditFail()) {// 正在编辑且内容不合法
			return;
		}

		// 退出时间停止
		exitTimer.stop();

		// 进入时间开始
		if (enterTimer.isRunning()) {
			enterTimer.restart();
		} else {
			enterTimer.start();
		}
	}

	/**
	 * 
	 * 取消JComponent
	 * 
	 */
	public void unregisterComponent() {
		// 进入时间停止
		enterTimer.stop();

		// 退出时间开始
		if (exitTimer.isRunning()) {
			exitTimer.restart();
		} else {
			exitTimer.start();
		}
	}

	/**
	 * 
	 * 立即中断
	 * 
	 */
	public void interruptComponent() {
		// 进入时间停止
		if (enterTimer.isRunning()) {
			enterTimer.stop();
		}
		// 退出时间停止
		if (exitTimer.isRunning()) {
			exitTimer.stop();
		}
		// 清除
		exitTimerActionPerformed();
	}

	/**
	 * 
	 * 进入组件触发的动作事件类
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class EnterTimerActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			enterTimerActionPerformed(e);
		}
	}

	/**
	 * 
	 * 退出组件触发的动作事件类
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class ExitTimerActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			exitTimerActionPerformed();
		}
	}

	private void enterTimerActionPerformed(ActionEvent e) {
		if (flowElementPanel != null && flowElementPanel.isShowing()) {// 主组件不为空且显示在屏幕上时

			// 显示辅助组件(小三角形的)
			linkFigureMainProcess.trianglesBtnsVisibleTrue();

			// 停止退出动作事件
			exitTimer.stop();

			// 设置小三角形的大小和位置点
			linkFigureMainProcess.resetTrianglesPanel();
			// 小三角形显示
			isTriangleShow = true;
		}
	}

	private void exitTimerActionPerformed() {

		// 小三角不显示，快捷图形入口不显示
		linkFigureMainProcess.hiddTrianglesBtnAndLinkPanel();

		// 主组件
		flowElementPanel = null;
		// 清空对应流程元素图形
		linkFigureMainProcess.setFigure(null);
		// 隐藏
		isTriangleShow = false;
	}

	/**
	 * 
	 * 小三角形是处于显示状态
	 * 
	 * @return
	 */
	boolean isTriangleShow() {
		return isTriangleShow;
	}

	/**
	 * 
	 * 小三角形处于隐藏状态
	 * 
	 */
	void setTriangleShowHidding() {
		isTriangleShow = false;
	}
}
