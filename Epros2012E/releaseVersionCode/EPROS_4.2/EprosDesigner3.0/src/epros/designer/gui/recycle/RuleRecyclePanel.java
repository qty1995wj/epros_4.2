package epros.designer.gui.recycle;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class RuleRecyclePanel extends RecyclePanel {

	private RuleRecycleTree recycleTree;
	private Logger log = Logger.getLogger(RuleRecyclePanel.class);

	public RuleRecyclePanel() {
		super();
	}

	@Override
	public String getSearchName() {
		return JecnProperties.getValue("fileNameC");
	}

	@Override
	public RecycleTree initTree() {
		recycleTree = new RuleRecycleTree();
		recycleTree.setRecyclePanel(this);
		return recycleTree;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {

		try {
			return ConnectionPool.getRuleAction().getAllFilesByName(name, JecnConstants.projectId,
					JecnConstants.loginBean.getSetRule(), JecnConstants.loginBean.isAdmin());
		} catch (Exception e) {
			log.error("RuleRecyclePanel method searchByName error", e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	public void delDataAction(List<Long> listIds) {
		try {
			ConnectionPool.getRuleAction().deleteRule(listIds,JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("RuleRecyclePanel delDataAction searchByName error", e);
		}
	}

	@Override
	public String getTreeBorderName() {
		return JecnProperties.getValue("ruleRecover");
	}
}
