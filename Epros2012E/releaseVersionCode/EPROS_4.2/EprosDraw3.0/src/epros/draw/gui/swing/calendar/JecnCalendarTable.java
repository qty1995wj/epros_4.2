package epros.draw.gui.swing.calendar;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;

import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 日期表
 * 
 * @author ZHOUXY
 * 
 */
class JecnCalendarTable extends JTable {

	/** 日历对话框 */
	private JecnCalendarDialog dialog = null;
	/** 表模型 */
	private JecnCalendarDefaultTableModel tableModel;

	/** 鼠标点对应行 */
	private int mouseRow = -1;
	/** 鼠标点对应列 */
	private int mouseColumn = -1;

	JecnCalendarTable(JecnCalendarDialog dialog) {
		this.dialog = dialog;

		initCompoments();
		initShortCutEvent();
		initEvent();
	}

	private void initCompoments() {
		// 中间表模型
		tableModel = new JecnCalendarDefaultTableModel();

		this.setModel(tableModel);
		// 表头不可拖动
		this.getTableHeader().setReorderingAllowed(false);
		// 不可调整列大小
		this.getTableHeader().setResizingAllowed(false);
		// 一次选择一个列表索引
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// 设置此表是否允许同时存在行选择和列选择。当允许时，表将行选择模型和列选择模型的相交部分作为选定的单元格
		this.setCellSelectionEnabled(true);
		// 用来绘制网格线的颜色
		this.setGridColor(JecnUIUtil.getDefaultBackgroundColor());
		this.setRowMargin(0);

		// 表第一行数据
		tableModel.setColumnIdentifiers(dialog.getTableHeaderNames());
		tableModel.addRow(dialog.getTableHeaderNames());
		// 添加列渲染器
		for (int i = 0; i < 7; i++) {
			this.getColumnModel().getColumn(i).setCellRenderer(
					new JecnCalendarTableCellRenderer(dialog));
		}

	}

	public void changeSelection(int rowIndex, int columnIndex, boolean toggle,
			boolean extend) {
		String value = selectedCellValue(rowIndex, columnIndex);
		if (value == null) {// 不是有效日期
			return;
		}
		dialog.setData(Integer.parseInt(value));
		super.changeSelection(rowIndex, columnIndex, toggle, extend);
	}

	public void repaint(int x, int y, int width, int height) {
		super.repaint(x, y, width, height);
		if (height > 0) {
			makeTableRowHeight();
		}
	}

	protected void validateTree() {
		super.validateTree();
		makeTableRowHeight();
	}

	protected void processMouseMotionEvent(MouseEvent e) {
		super.processMouseMotionEvent(e);

		if (MouseEvent.MOUSE_MOVED == e.getID()) {// 移动
			int rowIndex = this.rowAtPoint(e.getPoint());
			int columnIndex = this.columnAtPoint(e.getPoint());

			if (mouseRow == rowIndex && mouseColumn == columnIndex) {
				return;
			}
			String value = this.selectedCellValue(rowIndex, columnIndex);
			if (value == null) {
				setMouseRowColumn(-1, -1);
				return;
			}
			setMouseRowColumn(rowIndex, columnIndex);

			this.changeSelection(rowIndex, columnIndex, false, false);
		}
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (MouseEvent.MOUSE_EXITED == e.getID()) {// 退出
			super.changeSelection(0, 0, false, false);
		}
	}

	/**
	 * 
	 * 表行高
	 * 
	 */
	private void makeTableRowHeight() {
		int rowHeight = (int) ((this.getHeight() / 7) * 0.6);
		if (rowHeight > 0) {
			this.setRowHeight(0, rowHeight);
		}
		rowHeight = (int) ((this.getHeight() - rowHeight) / 6);
		if (rowHeight > 0) {
			for (int i = 1; i < 8; i++) {
				this.setRowHeight(i, rowHeight);
			}
		}
	}

	private void initEvent() {
		// 表鼠标事件
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				mouseClickedProcess(e);
			}
		});
	}

	/**
	 * 
	 * 添加快捷键
	 * 
	 */
	private void initShortCutEvent() {
		// **************快捷键**************//
		AbstractAction right = (new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedRight(e);
			}
		});
		AbstractAction left = (new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedLeft(e);
			}
		});

		AbstractAction enter = (new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedEnter(e);
			}
		});

		InputMap inputMap = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		// 方向键-右
		KeyStroke rightKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0);
		// 方向键-左
		KeyStroke leftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0);
		// 方向键-Enter
		KeyStroke enterKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);

		this.getActionMap().put(inputMap.get(rightKeyStroke), right);
		this.getActionMap().put(inputMap.get(leftKeyStroke), left);
		this.getActionMap().put(inputMap.get(enterKeyStroke), enter);
		// **************快捷键**************//
	}

	/**
	 * 
	 * 方向键-右事件
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void actionPerformedRight(ActionEvent e) {
		if (this.getColumnCount() == this.getSelectedColumn() + 1) {
			this.changeSelection(this.getSelectedRow() + 1, 0, false, false);
		} else {
			this.changeSelection(this.getSelectedRow(), this
					.getSelectedColumn() + 1, false, false);
		}
	}

	/**
	 * 
	 * 方向键-左事件
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void actionPerformedLeft(ActionEvent e) {
		if (0 > this.getSelectedColumn() - 1) {
			this.changeSelection(this.getSelectedRow() - 1, this
					.getColumnCount() - 1, false, false);
		} else {
			this.changeSelection(this.getSelectedRow(), this
					.getSelectedColumn() - 1, false, false);
		}
	}

	/**
	 * 
	 * 键盘Enter事件
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void actionPerformedEnter(ActionEvent e) {
		// 选中单元格
		int rowIndex = this.getSelectedRow();
		int columnIndex = this.getSelectedColumn();

		dialog.execute(rowIndex, columnIndex);
	}

	/**
	 * 
	 * 鼠标单击事件
	 * 
	 * @param e
	 *            MouseEvent
	 */
	private void mouseClickedProcess(MouseEvent e) {
		int rowIndex = this.rowAtPoint(e.getPoint());
		int columnIndex = this.columnAtPoint(e.getPoint());

		dialog.execute(rowIndex, columnIndex);
	}

	/**
	 * 
	 * 给定行和列对应的单元格不是具体天数返回NULL，是返回天数值
	 * 
	 * @param rowIndex
	 *            int 行
	 * @param columnIndex
	 *            int 列
	 * @return String 合法时：1~31 不合法时：NULL
	 */
	String selectedCellValue(int rowIndex, int columnIndex) {
		if (rowIndex <= 0 || columnIndex <= -1
				|| rowIndex >= this.getRowCount()
				|| columnIndex >= this.getColumnCount()) {
			return null;
		}
		Object obj = this.getValueAt(rowIndex, columnIndex);
		if (obj == null) {
			return null;
		}
		String value = String.valueOf(obj);
		if (DrawCommon.isNullOrEmtryTrim(value)) {
			return null;
		}
		return value;
	}

	/**
	 * 
	 * 设置鼠标对应行列
	 * 
	 * @param row
	 * @param column
	 */
	private void setMouseRowColumn(int row, int column) {
		mouseRow = row;
		mouseColumn = column;
	}

	public JecnCalendarDefaultTableModel getTableModel() {
		return tableModel;
	}

}