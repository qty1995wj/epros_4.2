<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="attentionFilesearchText" style="overflow: hidden;"
					align="center">
					<table width="100%">
						<tr>
							<td height="25px" width="10%" align="right">
								<!-- 文件名称： -->
								<s:text name="fileNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;" id="fileName"
									maxLength="32" />
							</td>

							<td width="10%" align="right">
								<!-- 文件编号： -->
								<s:text name="fileNumberC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;" id="fileNumber"
									maxLength="32" />
							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a onClick="toggle(attentionFilesearchText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td height="25px" align="right">
								<!-- 责任部门名称: -->
								<s:text name="responsibilityDepartmentC" />
							</td>
							<td align="left">
								<%--								<select style="width: 80%;" id="attentionFile_positionName">--%>
								<%--									<option value="-1">--%>
								<%--										<s:text name="all" />--%>
								<%--									</option>--%>
								<%--									<s:iterator value="#session.webLoginBean.listPosBean">--%>
								<%--										<option value='<s:property value="posId"/>'>--%>
								<%--											<s:property value="posName" />--%>
								<%--										</option>--%>
								<%--									</s:iterator>--%>
								<%--								</select>--%>
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="orgDutyName_rule_id" readonly="readonly" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","orgDutyName_rule_id","orgDutyId_rule_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_rule_orgDutyName"
									style="display: none; z-index: 1; position: absolute;"></div>

							</td>

							<td align="right">
								<!-- 密集: -->
								<s:text name="intensiveC" />
							</td>
							<td align="left">
								<select style="width: 80%;" id="attentionFile_intensive">
									<option value="-1">
										<s:text name="all" />
									</option>
									<option value="1">
										<s:property value="#application.public"/>
									</option>
									<option value="0">
										${secret}
									</option>
								</select>
							</td>
							<td></td>
						</tr>

					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="attentionFileSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="attentionFileReset()" />
				</div>
				<!-- 查阅部门ID -->
				<input type="hidden" id="orgLookId_id" />
			</div>

			<div id="atentionFileGrid_id">

			</div>
		</div>
		<!-- 责任部门ID -->
		<input type="hidden" id="orgDutyId_rule_id" />
	</body>
</html>