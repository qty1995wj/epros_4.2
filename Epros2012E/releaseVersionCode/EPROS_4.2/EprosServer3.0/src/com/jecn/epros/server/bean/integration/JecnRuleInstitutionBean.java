package com.jecn.epros.server.bean.integration;

import java.io.Serializable;

/**
 * 
 * 制度与制度关系临时表
 * 
 * @author ZHR
 * @date： 日期：2018-7-10 
 */
public class JecnRuleInstitutionBean implements Serializable {
	/** 主键ID */
	private String id;
	/** 制度ID */
	private Long ruleId;
	/** 关联制度主键ID */
	private Long relatedId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	
	
}
