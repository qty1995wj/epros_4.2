<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="searchText" style="overflow: hidden;" class="minWidth">
					<div style="height: 5%; margin-top: 10px; margin-left: 15px;"
						align="left">
						<input type="radio" name="orgPos" value="org" id="org"
							onclick="posOrgSwitching()" checked>
						<!-- 部门 -->
						<s:text name="org" />
						<input type="radio" name="orgPos" value="pos"
							onclick="posOrgSwitching()" id="pos">
						<!-- 岗位 -->
						<s:text name="pos" />
					</div>
					<div id="orgDiv" align="center">
						<table width="100%" align="center">
							<tr>
								<td height="25px" align="center">
									<!-- 部门名称： -->
									<s:text name="orgNameC"></s:text>
									<input type="text" style="width: 25%" id="orgName_id"
										maxLength="32" />
								</td>
							</tr>
						</table>
					</div>

					<div id="posDiv" align="center" style="display: none;">
						<table width="100%" align="center">
							<tr>
								<td height="25px" align="center">
									<!-- 岗位名称： -->
									<s:text name="positionNameC"></s:text>
									<input type="text" style="width: 25%" id="posName_id"
										maxLength="32" />
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div id="searchBut" align="center">
					<img src="${basePath}images/iParticipateInProcess/search.gif"
						style="cursor: pointer;" onclick="orgOrPosSearch()" />
					<img src="${basePath}images/iParticipateInProcess/reset.gif"
						style="cursor: pointer;" onclick="orgOrPosReset()" />
				</div>
			</div>

			<div id="orgGrid_id"></div>
			<div id="positionGrid_id" style="display: none;"></div>

		</div>
		<!-- 岗位名称ID -->
		<input type="hidden" id="posLookId_id" />
		<!-- 所属部门ID -->
		<input type="hidden" id="orgLookId_id" />
		<!-- 部门ID -->
		<input type="hidden" id="orgId_id" />
	</body>
</html>