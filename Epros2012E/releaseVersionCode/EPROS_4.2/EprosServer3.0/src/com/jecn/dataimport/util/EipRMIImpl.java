package com.jecn.dataimport.util;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.MyOrder;

/**
 * 
 * 聚光科技,待接办中心远程接口实现
 * 
 */
public class EipRMIImpl extends UnicastRemoteObject implements EipRMI {
	private final Log log = LogFactory.getLog(EipRMIImpl.class);
	/**
	 * 
	 * 接口
	 */
	private EipDaoImpl eipDao = null;

	public EipRMIImpl() throws RemoteException {
		super();
	}

	/**
	 * 获取当前登录人 的流程信息 和任务信息
	 * 
	 * @param loginName
	 *            登录名称
	 * @throws Exception
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public List<EipDataBean> getEipDataBeanList(String loginName) throws Exception {
		// 返回EIP数据
		List<EipDataBean> listEipDataBean = new ArrayList<EipDataBean>();
		log.info("获取用户名称  : " + loginName);

		if (loginName == null || "".equals(loginName.trim())) {// 用户不存在
			log.error("用户" + loginName + "不存在!");
			return new ArrayList<EipDataBean>();
		}
		try {
			// 获取 loginName 下任务信息
			List<JecnMyTaskList> listJecnMyTaskList = getJecnMyTaskList(loginName);
			if (listJecnMyTaskList == null) {
				log.error("用户" + loginName + "不存在任务!");
				return new ArrayList<EipDataBean>();
			}
			for (int i = 0; i < listJecnMyTaskList.size(); i++) {
				// 获取当前任务信息
				JecnMyTaskList taskList = listJecnMyTaskList.get(i);
				EipDataBean eipDataBean = getEipDataBean(taskList);
				if (eipDataBean != null) {
					listEipDataBean.add(eipDataBean);
				}
			}
		} catch (Exception ex) {
			log.error("获取当前登录人 的流程信息 和任务信息 getEipDataBeanList 异常！", ex);
		}
		return listEipDataBean;
	}

	/**
	 * 所有任务数据集合
	 * 
	 * @return EipDataBean集合
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public List<EipDataBean> getEipAllDataBeanList() throws Exception {
		List<EipDataBean> listEipDataBean = new ArrayList<EipDataBean>();
		try {
			// 获取流程任务
			List<JecnMyTaskList> listJecnTaskBeanNew = findAllTask();
			if (listJecnTaskBeanNew == null) {
				log.error("获取所有任务异常！");
				return new ArrayList<EipDataBean>();
			}
			for (JecnMyTaskList taskList : listJecnTaskBeanNew) {
				EipDataBean eipDataBean = getEipDataBean(taskList);
				listEipDataBean.add(eipDataBean);
			}
		} catch (Exception ex) {
		}
		return listEipDataBean;
	}

	/**
	 * 待接办中心相应员工的流程总数
	 * 
	 * @param personId
	 *            员工工号
	 * @return 流程总数
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public Integer getFlowCount(String loginName) throws Exception {
		List<JecnMyTaskList> list = getJecnMyTaskList(loginName);
		if (list == null) {
			log.error("用户" + loginName + "不存在任务！");
			return 0;
		}
		return list.size();
	}

	/**
	 * 所有流程数据集合
	 * 
	 * @return EipDataBean集合
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public List<EipFlowBean> getEipAllFlowBeanList() throws Exception {
		Long projectId = getCurProjectId();
		log.info("所有流程数据集合 获取当前项目ID = " + projectId);
		if (projectId == null) {
			log.error("获取项目ID失败！");
			return new ArrayList<EipFlowBean>();
		}
		List<EipFlowBean> listEipFlowBeanList = new ArrayList<EipFlowBean>();
		// 获取所有流程
		List<JecnFlowStructure> jecnFlowStructureList = findAllFlowByProjectId(projectId);
		// 获取所有流程任务
		List<JecnTaskBeanNew> listJecnTaskBeanNew = getAllFlowTask();
		if (jecnFlowStructureList == null) {
			log.error("获取所有流程异常！");
			return new ArrayList<EipFlowBean>();
		}
		for (int i = 0; i < jecnFlowStructureList.size(); i++) {
			JecnFlowStructure jecnFlowStructure = jecnFlowStructureList.get(i);
			EipFlowBean eipFlowBean = new EipFlowBean();
			eipFlowBean.setProjectId(projectId);
			eipFlowBean.setFlowId(jecnFlowStructure.getFlowId());
			eipFlowBean.setFlowName(jecnFlowStructure.getFlowName());
			eipFlowBean.setFlowIdInput(jecnFlowStructure.getFlowIdInput());
			for (int j = 0; j < listJecnTaskBeanNew.size(); j++) {
				JecnTaskBeanNew jecnFlowTaskBean = listJecnTaskBeanNew.get(j);
				if (jecnFlowTaskBean.getRid().equals(jecnFlowStructure.getFlowId())) {
					eipFlowBean.setTaskId(jecnFlowTaskBean.getId());
					eipFlowBean.setTaskName(jecnFlowTaskBean.getTaskName());
				}
			}
			listEipFlowBeanList.add(eipFlowBean);
		}
		return listEipFlowBeanList;
	}

	/**
	 * 获得我的流程
	 * 
	 * @param loginName
	 *            员工工号
	 * @return
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public List<EipMyFlowBean> getEipMyFlowBeanList(String loginName) throws Exception {
		Long projectId = getCurProjectId();
		log.info("所有流程数据集合 获取当前项目ID = " + projectId);
		if (projectId == null) {
			return new ArrayList<EipMyFlowBean>();
		}
		List<FlowRalatedBean> listResult = new ArrayList<FlowRalatedBean>();
		List<Long> listStationIds = new ArrayList<Long>();
		// 岗位相关角色,及流程
		List<Object[]> listRoleNoGroup = new ArrayList<Object[]>();
		List<Object[]> listRoleGroup = new ArrayList<Object[]>();
		List<Object[]> listStation = new ArrayList<Object[]>();
		// 岗位下相关流程的活动
		List<Object[]> listActive = new ArrayList<Object[]>();
		try {
			// 人员对应的岗位信息（id，名称）
			listStation = getPositionByLoginName(loginName);
			log.error("获取登录人的岗位信息成功！");
			for (Object[] objArr : listStation) {
				if (objArr[0] != null) {
					listStationIds.add(Long.valueOf(objArr[0].toString()));
				}
			}
			if (listStationIds.size() > 0) {
				listRoleNoGroup = getRoleNOGroupByList(listStationIds, projectId);
				listRoleGroup = getRoleGroupByList(listStationIds, projectId);
			}

			Set<Long> setRoleIds = new HashSet<Long>();
			for (Object[] obj : listRoleNoGroup) {
				if (obj[0] != null) {
					setRoleIds.add(Long.valueOf(obj[0].toString()));
				}
			}
			for (Object[] obj : listRoleGroup) {
				if (obj[0] != null) {
					setRoleIds.add(Long.valueOf(obj[0].toString()));
				}
			}
			// 岗位相关角色
			if (setRoleIds.size() > 0) {// 通过角色Id获得活动
				listActive = getActiveByRoleListIds(setRoleIds);
			}

		} catch (Exception ex) {
			log.error("获得我的流程信息获取异常！", ex);
			return null;
		}

		if (listStation == null) {
			return new ArrayList<EipMyFlowBean>();
		}
		for (int i = 0; i < listStation.size(); i++) {
			Object[] objects = (Object[]) listStation.get(i);
			if (objects[0] != null) {
				String positionName = "";
				if (objects[1] != null) {
					positionName = objects[1].toString();
				}
				Long figureId = Long.valueOf(objects[0].toString());
				List<Object[]> list = this.getRoleByFigureId(figureId, listRoleNoGroup, listRoleGroup);
				this.getStationFlow(listResult, positionName, list, listActive, false, projectId, new HashSet<Long>());
			}
		}
		if (listResult.size() == 0) {
			return new ArrayList<EipMyFlowBean>();
		}
		List<EipMyFlowBean> eipMyFlowBeanList = new ArrayList<EipMyFlowBean>();
		List<Object[]> allFlow = null;
		try {
			allFlow = getAllFlowWithCreatTime();
		} catch (Exception ex) {
			log.error(" 获得流程创建时间", ex);
			return null;
		}
		for (FlowRalatedBean flowRalatedBean : listResult) {
			EipMyFlowBean eipMyFlowBean = new EipMyFlowBean();
			eipMyFlowBean.setProcessId(flowRalatedBean.getProcessId());
			eipMyFlowBean.setPositionName(flowRalatedBean.getPositionName());
			eipMyFlowBean.setRoleName(flowRalatedBean.getRoleName());
			eipMyFlowBean.setRoleNameCount(flowRalatedBean.getSizeIndex());
			eipMyFlowBean.setFlowInputId(flowRalatedBean.getFlowInputId());
			eipMyFlowBean.setProcessName(flowRalatedBean.getProcessName());
			eipMyFlowBean.setActivityList(flowRalatedBean.getActivityList());
			for (Object[] obj : allFlow) {
				if (obj[0].toString().equals(flowRalatedBean.getProcessId().toString())) {
					eipMyFlowBean.setCreateTime((Date) obj[1]);
					break;
				}
			}
			eipMyFlowBeanList.add(eipMyFlowBean);
		}

		return eipMyFlowBeanList;
	}

	/**
	 * 
	 * 组装EIP信息
	 * 
	 * @param taskList
	 * @return
	 */
	private EipDataBean getEipDataBean(JecnMyTaskList taskList) {
		if (taskList.getTaskId() == null || taskList.getRid() == null) {
			return new EipDataBean();
		}
		// 聚光科技,待接办中心需要数据
		EipDataBean eipDataBean = new EipDataBean();
		// 设置流程ID（关联ID ）
		eipDataBean.setFlowId(taskList.getRid());
		// 设置流程名称（关联名称 ）
		eipDataBean.setFlowName(taskList.getTaskName());
		/** 任务创建人 */
		eipDataBean.setCreatePerson(taskList.getCreateName());
		// 任务显示阶段
		eipDataBean.setState(taskList.getStageName());

		switch (taskList.getTaskType()) {
		case 0:
		case 4:
			// 流程
			eipDataBean.setTaskType("流程任务");
			break;
		case 2:
		case 3:
			eipDataBean.setTaskType("制度任务");
			break;
		case 1:
			eipDataBean.setTaskType("文件任务");
			break;
		}
		eipDataBean.setTaskName(taskList.getTaskName());
		if (taskList.getStartTime() != null) {
			eipDataBean.setStartTime(JecnCommon.getDateByString(taskList.getStartTime()));
		}
		if (taskList.getStartTime() != null) {
			eipDataBean.setEndTime(JecnCommon.getDateByString(taskList.getEndTime()));
		}
		return eipDataBean;
	}

	/**
	 * 个人信息任务初始化
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	private List<JecnMyTaskList> getJecnMyTaskList(String loginName) throws Exception {
		List<JecnMyTaskList> taskObject = null;
		try {
			String hql = "from JecnUser where loginName=?";
			List<JecnUser> listUser = eipDao.listHql(hql, loginName);
			if (listUser == null || listUser.size() == 0) {
				log.error("EIP登录名称获取人员ID失败 ：" + loginName);
				return taskObject;
			}
			Long peopleId = listUser.get(0).getPeopleId();
			if (peopleId == null) {
				return new ArrayList<JecnMyTaskList>();
			}
			// 获取处于审批的任务（未锁定，未完成）
			// Object[]
			// :(0:任务主键；1：任务名称；2：任务阶段；3：任务操作;4：任务类型；5：登录人真实姓名；：6：创建人主键ID)
			taskObject = findTaskByPeopleId(peopleId);
		} catch (Exception ex) {
			log.error("个人信息任务初始化getJecnMyTaskList 异常！", ex);
		}
		return taskObject;
	}

	/**
	 * Object[]
	 * :(0:任务主键；1：任务名称；2：任务阶段；3：任务操作;4：任务类型；5：登录人真实姓名；：6：创建人主键ID,7:是否锁定,
	 * 8:开始时间；9：结束时间,10:关联ID)
	 * 
	 * @param peopleId
	 * @return List<Object[]>
	 */
	private List<JecnMyTaskList> findTaskByPeopleId(Long peopleId) throws Exception {
		String sql = "select a.id," + "               a.TASK_NAME," + "               a.STATE,"
				+ "               a.TASK_ELSE_STATE," + "               a.task_Type," + "               c.true_Name,"
				+ "               a.CREATE_PERSON_ID," + "               a.IS_LOCK," + "a.START_TIME," + "a.END_TIME,"
				+ "a.R_ID" + "          from JECN_TASK_BEAN_NEW a"
				+ "         inner join Jecn_Task_People_New b on a.id = b.task_Id"
				+ "                                          and b.APPROVE_PID = ?"
				+ "         inner join Jecn_User c on a.CREATE_PERSON_ID = c.people_Id"
				+ "         where Exists (select 1" + "                  from jecn_user_position_related u_p"
				+ "                 where u_p.people_Id = c.people_Id)" + "           and a.IS_LOCK = 1"
				+ "           and a.STATE <> 5";
		List<Object[]> list = eipDao.listNativeSql(sql, peopleId);
		return getJecnMyTaskListList(list);
	}

	/**
	 * Object[]
	 * :(0:任务主键；1：任务名称；2：任务阶段；3：任务操作;4：任务类型；5：登录人真实姓名；：6：创建人主键ID,7:是否锁定,
	 * 8:开始时间；9：结束时间,10:关联ID)
	 * 
	 * 获取所有任务
	 * 
	 * @return
	 */
	private List<JecnMyTaskList> findAllTask() throws Exception {
		String sql = "select a.id," + "               a.TASK_NAME," + "               a.STATE,"
				+ "               a.TASK_ELSE_STATE," + "               a.task_Type," + "               c.true_Name,"
				+ "               a.CREATE_PERSON_ID," + "               a.IS_LOCK," + "a.START_TIME," + "a.END_TIME,"
				+ "a.R_ID" + " from JECN_TASK_BEAN_NEW a "
				+ "LEFT JOIN Jecn_User c ON a.CREATE_PERSON_ID = c.people_Id" + " where a.IS_LOCK = 1"
				+ " order by a.UPDATE_TIME DESC";
		// 0:任务主键ID，1：任务名称，2：任务阶段，3：任务操作状态，4：任务类型，5：创建人真实姓名，6:创建人主键ID
		List<Object[]> objectList = eipDao.listNativeSql(sql);
		return getJecnMyTaskListList(objectList);
	}

	/**
	 * 根据object数组获取JecnMyTaskList集合
	 * 
	 * @param objectList
	 *            List<Object[]>
	 *            0:任务主键；1：任务名称；2：任务阶段；3：任务操作;4：任务类型；5：登录人真实姓名；：6：创建人主键ID
	 * @param loginPeopleId
	 *            人员主键ID 登录人的
	 * @param type
	 *            0：我的任务，1：任务管理，登录人不是管理，2：任务管理，登录人为管理员
	 * @return List<JecnMyTaskList>登录人相关任务集合
	 */
	private List<JecnMyTaskList> getJecnMyTaskListList(List<Object[]> objectList) {
		if (objectList == null) {
			throw new NullPointerException("getJecnMyTaskListList获取任务记录异常！objectList == null || loginPeopleId == null");
		}
		// 记录登录人相关任务信息
		List<JecnMyTaskList> list = new ArrayList<JecnMyTaskList>();
		for (Object[] objects : objectList) {
			// if (objects.length != 7) {
			// continue;
			// }
			// 我的任务信息
			JecnMyTaskList taskBean = new JecnMyTaskList();

			if (!isNullObj(objects[0])) {
				// 0任务主键ID
				taskBean.setTaskId(JecnTaskCommon.getlongByObject(objects[0]));
			}
			if (!isNullObj(objects[1])) {
				// 1任务名称
				taskBean.setTaskName(objects[1].toString());
			}
			if (!isNullObj(objects[2])) {
				// 2任务阶段
				taskBean.setTaskStage(JecnTaskCommon.getIntByObject(objects[2]));
			}
			if (!isNullObj(objects[4])) {
				// 4任务类型
				taskBean.setTaskType(JecnTaskCommon.getIntByObject(objects[4]));
			}
			if (!isNullObj(objects[5])) {
				// 5创建人真实姓名
				taskBean.setCreateName(objects[5].toString());
			}
			if (!isNullObj(objects[7])) {
				// 7是否锁定
				taskBean.setIsLock(objects[7].toString());
			}
			// 获取当前审批阶段名称
			String sql = "SELECT T.STAGE_NAME FROM JECN_TASK_STAGE T WHERE T.TASK_ID = ? AND T.STAGE_MARK = ?";
			String stageName = eipDao.getObjectNativeSql(sql, taskBean.getTaskId(), taskBean.getTaskStage());
			if (StringUtils.isNotBlank(stageName)) {
				taskBean.setStageName(stageName);
			}

			if (!isNullObj(objects[8])) {
				taskBean.setStartTime(objects[8].toString());
			}
			if (!isNullObj(objects[9])) {
				taskBean.setEndTime(objects[9].toString());
			}
			if (!isNullObj(objects[10])) {
				taskBean.setRid(Long.valueOf(objects[10].toString()));
			}
			list.add(taskBean);
		}
		return list;
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	private static boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * 获取主项目ID
	 * 
	 * @return
	 */
	private Long getCurProjectId() {
		String hql = "from JecnCurProject";
		List<JecnCurProject> list = this.eipDao.listHql(hql);
		if (list != null && list.size() > 0) {
			return list.get(0).getCurProjectId();
		}
		return null;
	}

	private List<Object[]> getRoleByFigureId(Long figureId, List<Object[]> listRoleNoGroup, List<Object[]> listRoleGroup) {
		List<Object[]> listResult = new ArrayList<Object[]>();
		List<Long> list = new ArrayList<Long>();
		for (Object[] obj : listRoleNoGroup) {
			if (obj[0] != null && obj[6] != null) {
				Long roleId = Long.valueOf(obj[0].toString());
				if (figureId.equals(Long.valueOf(obj[6].toString()))) {
					if (!isExistRole(roleId, list)) {
						listResult.add(obj);
					}
				}
			}
		}
		for (Object[] obj : listRoleGroup) {
			if (obj[0] != null && obj[6] != null) {
				Long roleId = Long.valueOf(obj[0].toString());
				if (figureId.equals(Long.valueOf(obj[6].toString()))) {
					if (!isExistRole(roleId, list)) {
						listResult.add(obj);
					}
				}
			}
		}
		return listResult;
	}

	private void getStationFlow(List<FlowRalatedBean> listResult, String positionName, List<Object[]> list,
			List<Object[]> listActive, boolean isIndicator, Long projectId, Set<Long> listActiveId) throws Exception {
		List<JecnRefIndicators> listJecnRefIndicatorsAll = new ArrayList<JecnRefIndicators>();
		if (isIndicator && listActiveId.size() > 0) {
			try {
				listJecnRefIndicatorsAll = getActivesJecnRefIndicatorsTBySet(listActiveId);
			} catch (Exception ex) {
			}
		}
		int size = list.size();
		for (int i = 0; i < size; i++) {
			Object[] objects = (Object[]) list.get(i);
			if (objects[0] != null) {
				FlowRalatedBean flowRalatedBean = new FlowRalatedBean();
				String roleName = "";
				if (objects[1] != null) {
					roleName = objects[1].toString();
					roleName = roleName.replaceAll("\n", "<br>");
				}
				if (i == 0) {
					flowRalatedBean.setPositionName(positionName);
				} else {
					flowRalatedBean.setPositionName("");
				}
				flowRalatedBean.setRoleName(roleName);
				String roleRes = "";
				if (objects[2] != null) {
					roleRes = objects[2].toString();
					roleRes = roleRes.replaceAll("\n", "<br>");
				}
				flowRalatedBean.setRoleRes(roleRes);
				String flowId = "";
				String flowName = "";
				String flowIdInput = "";
				if (objects[3] != null) {
					flowId = objects[3].toString();
				}
				if (objects[4] != null) {
					flowName = objects[4].toString();
				}
				if (objects[5] != null) {
					flowIdInput = objects[5].toString();
				}
				flowRalatedBean.setProcessId(flowId);
				flowRalatedBean.setProcessName(flowName);
				flowRalatedBean.setFlowInputId(flowIdInput);
				flowRalatedBean.setSizeIndex(String.valueOf(size));
				this.getActiveByRole(flowRalatedBean, Long.valueOf(objects[0].toString()), listActive, isIndicator,
						listJecnRefIndicatorsAll);
				listResult.add(flowRalatedBean);
			}
		}
	}

	private void getActiveByRole(FlowRalatedBean flowRalatedBean, Long roleFigureId, List<Object[]> listActive,
			boolean isIndicator, List<JecnRefIndicators> listJecnRefIndicatorsAll) {
		List<ActivityShowBean> objList = new ArrayList<ActivityShowBean>();
		int total = 0;
		for (Object[] objectActives : listActive) {
			if (objectActives[0] != null && objectActives[3] != null) {
				Long roleId = Long.valueOf(objectActives[3].toString());
				if (roleId.equals(roleFigureId)) {
					ActivityShowBean activityShowBean = new ActivityShowBean();
					activityShowBean.setFigureId(objectActives[0].toString());
					String activeName = "";
					if (objectActives[2] != null) {
						activeName = objectActives[2].toString();
						activeName = activeName.replaceAll("\n", "<br>");
					}
					activityShowBean.setActivityName(activeName);
					String activityId = "";
					if (objectActives[1] != null) {
						activityId = objectActives[1].toString();
					}
					activityShowBean.setActivityId(activityId);
					if (isIndicator) {
						Long mainId = Long.valueOf(objectActives[0].toString());
						List<JecnRefIndicators> indicatorsList = new ArrayList<JecnRefIndicators>();
						this.getActiveIndicator(mainId, listJecnRefIndicatorsAll, indicatorsList);
						activityShowBean.setIndicatorsList(indicatorsList);

						int size = indicatorsList.size();
						if (size == 0) {
							total++;
						} else {
							total = total + size;
						}
						activityShowBean.setSizeRefIndIcators(String.valueOf(size));
					}
					objList.add(activityShowBean);
				}
			}
		}
		if (isIndicator) {
			if (total == 0) {
				total = 1;
			}
			flowRalatedBean.setFlowActiveSize(String.valueOf(total));
		}
		flowRalatedBean.setActivityList(getActivitySort(objList));

	}

	private static List<ActivityShowBean> getActivitySort(List<ActivityShowBean> list) {
		List<ActivityShowBean> listResult = new ArrayList<ActivityShowBean>();
		List<String> listActivityId = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			ActivityShowBean activityShowBean = list.get(i);
			listActivityId.add(activityShowBean.getActivityId());
		}
		List<String> listSortStr = new MyOrder().order(listActivityId);
		for (int j = 0; j < listSortStr.size(); j++) {
			for (int i = 0; i < list.size(); i++) {
				ActivityShowBean activityShowBean = list.get(i);
				String activityId = activityShowBean.getActivityId();
				if (activityId.equals(listSortStr.get(j))) {
					if (!listResult.contains(activityShowBean)) {
						listResult.add(activityShowBean);
						break;
					}
				}

			}
		}
		return listResult;
	}

	private void getActiveIndicator(Long figureId, List<JecnRefIndicators> listJecnRefIndicatorsAll,
			List<JecnRefIndicators> List) {
		for (int i = 0; i < listJecnRefIndicatorsAll.size(); i++) {
			JecnRefIndicators jecnRefIndicators = listJecnRefIndicatorsAll.get(i);
			if (figureId.equals(jecnRefIndicators.getActivityId())) {
				List.add(jecnRefIndicators);
			}
		}
	}

	private boolean isExistRole(Long roleId, List<Long> list) {
		for (Long id : list) {
			if (id.equals(roleId)) {
				return true;
			}
		}
		list.add(roleId);
		return false;
	}

	/**
	 * 获取当前项目下所有流程所有流程
	 * 
	 * @param projectId
	 * @return List<JecnFlowStructure>
	 */
	private List<JecnFlowStructure> findAllFlowByProjectId(Long projectId) throws Exception {
		StringBuffer str = new StringBuffer();
		str.append("from JecnFlowStructure  where delState = 0 and ");
		str.append("isFlow=? and projectId =? order by perFlowId,sortId,flowId");
		return this.eipDao.listHql(str.toString(), 1L, projectId);
	}

	/**
	 * 所有流程数据集合
	 * 
	 * @return
	 * @throws DaoException
	 */
	private List<JecnTaskBeanNew> getAllFlowTask() throws Exception {
		String hql = "from JecnTaskBeanNew where taskType=0";
		return this.eipDao.listHql(hql);
	}

	/**
	 * 获取登录人的岗位信息
	 * 
	 * @param loginName
	 * @return
	 */
	private List<Object[]> getPositionByLoginName(String loginName) throws Exception {
		StringBuffer str = new StringBuffer();
		str
				.append("select a.figureId,a.figureText from JecnFlowOrgImage as a,JecnUserInfo as b,JecnFlowOrg as c,JecnUser as d where");
		str
				.append(" a.figureType='PositionFigure' and a.figureId=b.figureId and b.peopleId=d.peopleId and d.loginName=?");
		str.append(" and a.orgId = c.orgId and c.delState = 0");
		return this.eipDao.listHql(str.toString(), loginName);
	}

	/**
	 * 
	 * 获得人员在项目下参与的角色(没有岗位组)
	 */
	private List<Object[]> getRoleNOGroupByList(List<Long> listStationIds, Long projectId) throws Exception {
		StringBuffer str = new StringBuffer();
		str
				.append("select b.figureId,b.figureText,b.roleRes,a.flowId,a.flowName,a.flowIdInput,c.figurePositionId from");
		str.append(" JecnFlowStructureImage as b,JecnFlowStructure as a,JecnFlowStation as c where");
		str.append("  b.figureType = 'RoleFigure' and b.flowId = a.flowId and a.delState=0 and a.projectId =?");
		str.append("  and b.figureId = c.figureFlowId");
		str.append(" and c.type = '0' and c.figurePositionId in" + JecnCommonSql.getIds(listStationIds));
		str.append("  order by a.perFlowId,a.sortId,a.flowId");
		return this.eipDao.listHql(str.toString(), projectId);
	}

	/**
	 * 
	 * 获得人员在项目下参与的角色(岗位组)
	 */
	private List<Object[]> getRoleGroupByList(List<Long> listStationIds, Long projectId) throws Exception {
		StringBuffer str = new StringBuffer();
		str.append("select b.figureId,b.figureText,b.roleRes,a.flowId,a.flowName,a.flowIdInput,d.figureId from");
		str
				.append(" JecnFlowStructureImage as b,JecnFlowStructure as a,JecnFlowStation as c,JecnPositionGroupOrgRelated as d where");
		str.append("  b.figureType = 'RoleFigure' and b.flowId = a.flowId and a.delState=0 and a.projectId =?");
		str.append("  and b.figureId = c.figureFlowId");
		str.append(" and c.type='1' and c.figurePositionId = d.groupId and d.figureId in"
				+ JecnCommonSql.getIds(listStationIds));
		str.append("  order by a.perFlowId,a.sortId,a.flowId");
		return this.eipDao.listHql(str.toString(), projectId);
	}

	/**
	 * 通过角色Id获得活动
	 */

	private List<Object[]> getActiveByRoleListIds(Set<Long> figureIdsList) throws Exception {
		StringBuffer str = new StringBuffer();
		str.append("select distinct a.figureId,a.activityId,a.figureText,b.figureRoleId");
		str.append(" from JecnFlowStructureImage as a,JecnRoleActive as b where");
		str.append(" a.figureType in" + JecnCommonSql.getActiveString()
				+ " and a.figureId=b.figureActiveId and b.figureRoleId in" + JecnCommonSql.getIdsSet(figureIdsList));
		return this.eipDao.listHql(str.toString());
	}

	/**
	 * 获得流程创建时间
	 */
	private List<Object[]> getAllFlowWithCreatTime() throws Exception {
		String hql = "select flowId,createDate from JecnFlowStructure";
		return this.eipDao.listHql(hql);
	}

	/**
	 * 通过流程获得活动指标
	 */
	private List<JecnRefIndicators> getActivesJecnRefIndicatorsTBySet(Set<Long> listFlowId) throws Exception {
		String hql = "from JecnRefIndicators where activityId in" + JecnCommonSql.getIdsSet(listFlowId);
		return this.eipDao.listHql(hql);
	}

	public EipDaoImpl getEipDao() {
		return eipDao;
	}

	public void setEipDao(EipDaoImpl eipDao) {
		this.eipDao = eipDao;
	}
}
