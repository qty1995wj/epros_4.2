package epros.draw.gui.swing.textfield.search;

import java.awt.event.KeyEvent;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 带有快速查询功能和手动选择内容的输入框文档监听
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnSearchDocumentListener implements DocumentListener {
	/** 输入框 */
	private JSearchTextField searchTextField = null;

	public JecnSearchDocumentListener(JSearchTextField searchTextField) {
		if (searchTextField == null) {
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FIAL);
		}
		this.searchTextField = searchTextField;
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		if (!searchTextField.isSearchFlag()) {// 非快速查询
			searchTextField.searchFlagTrue();
			return;
		}
		insertUpdateJecn(e);
//		textFieldRequestFocus();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		removeUpdateJecn(e);
//		textFieldRequestFocus();
	}

	/**
	 * 
	 * 输入框获取焦点
	 * 
	 */
	private void textFieldRequestFocus() {
		this.searchTextField.requestFocusInWindow();
	}

	/**
	 * 
	 * 插入执行事件
	 * 
	 * @param e
	 *            DocumentEvent
	 */
	public abstract void insertUpdateJecn(DocumentEvent e);

	/**
	 * 
	 * 刪除执行事件
	 * 
	 * @param e
	 *            DocumentEvent
	 */
	public abstract void removeUpdateJecn(DocumentEvent e);

	/**
	 * 
	 * 点击键盘Enter执行动作
	 * 
	 * @param e
	 */
	public abstract void keyEnterReleased(KeyEvent e);
}
