package com.jecn.epros.server.bean.task.temp;

import java.util.Date;
import java.util.List;

/**
 * 任务提交信息
 * 
 * @author Administrator
 * @date： 日期：Nov 10, 2012 时间：4:48:23 PM
 */
public class SimpleSubmitMessage {
	/** 任务阶段**/
	private int taskStage;
	/** 提交审核信息 */
	private String opinion;
	/** 任务主键ID */
	private String taskId;
	/** 操作人PeopleId */
	private String curPeopleId;
	/** 交办和转批人ID */
	private String userAssignedId;
	/** 二次评审人IDS */
	private String newViewerIds;
	/** 术语定义 */
	private String definitionStr;
    /**
     * 自定义输入项1
     **/
    private String customInputItemOne;

    /**
     * 自定义输入项2
     **/
    private String customInputItemTwo;

    /**
     * 自定义输入项名称1
     **/
    private String customInputItemThree;
	/** 任务变更说明 */
	private String taskDesc;
	/**
	 * 任务审批人的操作状态 
	 * 0：拟稿人提交审批 
	 * 1：通过 
	 * 2：交办
	 * 3：转批
	 * 4：打回
	 * 5：提交意见（提交审批意见(评审-------->拟稿人)
	 * 6：二次评审 拟稿人------>评审 
	 * 7：拟稿人重新提交审批 
	 * 8：完成 
	 * 9：打回整理意见(批准------>拟稿人)）
	 * 10：交办人提交
	 * 11编辑打回、12：编辑 提交(系统管理员编辑任务) 
	 * 13：撤回 
	 * 14：返回
	 */
	private String taskOperation;
	/** 密级 0秘密，1 公开 */
	private String strPublic;
	/** 查阅权限组织ID集合 ','分隔 */
	private String orgIds;
	/** 查阅权限岗位ID集合 ','分隔 */
	private String posIds;
   	
	/** 查阅权限相关部门名称‘/’分离 */
	private String orgNames;
	/** 查阅权限相关岗位名称‘/’分离 */
	private String posNames;
	
	/** 查阅岗位组ID集合 ','分隔 */
	private String groupIds;
	/** 查阅岗位组名称集合 ','分隔 */
	private String groupNames;
	
	
	/** PRF当前类别 */
	private String prfType;
	/** PRF当前类别prfTypeName */
	private String prfTypeName;
	/** 页面提交新增审批人 */
	private List<TempAuditPeopleBean> listAuditPeople;
    /** 预估结束时间*/
	private Date endTime;
	/** 执行日期**/
	private Date implementDate;
	
	/** 4.0 版本调用标识 */
	private boolean isVersion4;
	
	
	
	public String getCustomInputItemOne() {
		return customInputItemOne;
	}

	public void setCustomInputItemOne(String customInputItemOne) {
		this.customInputItemOne = customInputItemOne;
	}

	public String getCustomInputItemTwo() {
		return customInputItemTwo;
	}

	public void setCustomInputItemTwo(String customInputItemTwo) {
		this.customInputItemTwo = customInputItemTwo;
	}

	public String getCustomInputItemThree() {
		return customInputItemThree;
	}

	public void setCustomInputItemThree(String customInputItemThree) {
		this.customInputItemThree = customInputItemThree;
	}

	public Date getImplementDate() {
		return implementDate;
	}

	public void setImplementDate(Date implementDate) {
		this.implementDate = implementDate;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	public String getGroupNames() {
		return groupNames;
	}

	public void setGroupNames(String groupNames) {
		this.groupNames = groupNames;
	}

	public String getOpinion() {
		return opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getCurPeopleId() {
		return curPeopleId;
	}

	public void setCurPeopleId(String curPeopleId) {
		this.curPeopleId = curPeopleId;
	}

	public String getUserAssignedId() {
		return userAssignedId;
	}

	public void setUserAssignedId(String userAssignedId) {
		this.userAssignedId = userAssignedId;
	}

	public String getNewViewerIds() {
		return newViewerIds;
	}

	public void setNewViewerIds(String newViewerIds) {
		this.newViewerIds = newViewerIds;
	}

	public String getTaskOperation() {
		return taskOperation;
	}

	public void setTaskOperation(String taskOperation) {
		this.taskOperation = taskOperation;
	}

	public String getStrPublic() {
		return strPublic;
	}

	public void setStrPublic(String strPublic) {
		this.strPublic = strPublic;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public String getPosIds() {
		return posIds;
	}

	public void setPosIds(String posIds) {
		this.posIds = posIds;
	}

	public String getOrgNames() {
		return orgNames;
	}

	public void setOrgNames(String orgNames) {
		this.orgNames = orgNames;
	}

	public String getPosNames() {
		return posNames;
	}

	public void setPosNames(String posNames) {
		this.posNames = posNames;
	}

	public String getPrfType() {
		return prfType;
	}

	public void setPrfType(String prfType) {
		this.prfType = prfType;
	}

	public String getPrfTypeName() {
		return prfTypeName;
	}

	public void setPrfTypeName(String prfTypeName) {
		this.prfTypeName = prfTypeName;
	}

	public List<TempAuditPeopleBean> getListAuditPeople() {
		return listAuditPeople;
	}

	public void setListAuditPeople(List<TempAuditPeopleBean> listAuditPeople) {
		this.listAuditPeople = listAuditPeople;
	}

	public String getDefinitionStr() {
		return definitionStr;
	}

	public void setDefinitionStr(String definitionStr) {
		this.definitionStr = definitionStr;
	}

	public String getTaskDesc() {
		return taskDesc;
	}

	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	public int getTaskStage() {
		return taskStage;
	}

	public void setTaskStage(int taskStage) {
		this.taskStage = taskStage;
	}

	public boolean isVersion4() {
		return isVersion4;
	}

	public void setVersion4(boolean isVersion4) {
		this.isVersion4 = isVersion4;
	}
	
	

}
