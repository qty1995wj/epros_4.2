package com.jecn.epros.server.webBean;

/**
 * @author yxw 2012-12-5
 * @description：我关注的(流程、制度、标准、文件)
 */
public class AttenTionSearchBean {
	/** 用户ID */
	private Long userId;
	/** 名称 */
	private String name;
	/** 编号 */
	private String number;
	/** 岗位ID */
	private long posId = 2;
	/** 密级 */
	private long secretLevel = -1;
	/** 责任部门 */
	private long orgId = -1;
	/** 是流程还是流程地图 1:流程图，0：流程地图 */
	private int isFlow = 1;
	/**是否是管理员*/
	private boolean isAdmin;
	/**项目Id*/
	private Long projectId;
	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public int getIsFlow() {
		return isFlow;
	}

	public void setIsFlow(int isFlow) {
		this.isFlow = isFlow;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public long getPosId() {
		return posId;
	}

	public void setPosId(long posId) {
		this.posId = posId;
	}

	public long getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
