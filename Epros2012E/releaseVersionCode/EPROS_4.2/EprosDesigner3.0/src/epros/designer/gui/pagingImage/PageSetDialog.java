package epros.designer.gui.pagingImage;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import epros.designer.util.JecnProperties;
import epros.draw.designer.TmpPageSetingBean;
import epros.draw.gui.line.JecnPageSetLine;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnUIUtil;

/**
 * 分页设置窗体
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2014-2-18 时间：上午10:57:19
 */
public class PageSetDialog extends JecnDialog {

	/** 分页：页数选择；初始值1：最大3 */
	private JSpinner jSpinner = null;

	/** 主面板 */
	private JecnPanel mainPanel = null;
	/** 分页数 显示面板 */
	private JecnPanel pageCountPanel = null;
	private JLabel pageCountLabel = null;

	/** 是否显示分页 */
	private JCheckBox pageSetBox = null;
	/** 根据分页符设置：预览 */
	private JButton buttonView = null;

	/** 全景图 */
	private ImageToolTip imageToolTip;

	/** 添加一个滑块 */
	private JSlider slider;

	/** 按钮面板 */
	private JecnPanel buttonPanel;
	/** 关闭按钮 */
	private JButton okButton;

	/** 全景图缩放比例 */
	private double scale = 1.0;

	/** 记录拖动前最后一根线的X坐标 */
	private int beforeSliderDraged;

	/** 分页符对象 */
	private TmpPageSetingBean pageSetingBean = null;

	public PageSetDialog() {
		initComponents();
		initLayout();

		init();
	}

	/**
	 * 初始化控件
	 * 
	 */
	private void initComponents() {
		this.setSize(500, 500);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);

		/** 添加一个滑块 */
		slider = new JSlider(0, JecnDrawMainPanel.getMainPanel().getWorkflow().getWidth());
		// 设置绘制刻度
		slider.setPaintTicks(true);
		// 设置主、次刻度的间距
		// slider.setMajorTickSpacing(20);
		// slider.setMinorTickSpacing(5);
		slider.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置滑块移动事件
		addSlider(slider);
		// 页数：
		pageCountLabel = new JLabel(JecnProperties.getValue("pageSize"));
		mainPanel = new JecnPanel();
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		pageCountPanel = new JecnPanel();
		pageCountPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		jSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 3, 1));
		jSpinner.setSize(100, 20);
		// 显示分页符
		pageSetBox = new JCheckBox(JecnProperties.getValue("showPageSet"));
		pageSetBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 预览
		buttonView = new JButton(JecnProperties.getValue("previewBtn"));
		buttonView.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		imageToolTip = new ImageToolTip();
		imageToolTip.setBorder(BorderFactory.createEtchedBorder());
		// "分页设置"
		this.setTitle(JecnProperties.getValue("pageSetIng"));

		buttonPanel = new JecnPanel();
		// 关闭
		okButton = new JButton(JecnProperties.getValue("closeBtn"));

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okButton);

		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		/**
		 * 关闭
		 * 
		 */
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		/**
		 * 分页符显示
		 * 
		 */
		pageSetBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				showPageSetBoxAction();
			}
		});

		/**
		 * 预览
		 * 
		 */
		buttonView.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonViewAction();
			}
		});

		jSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				int jSpinnerValue = Integer.valueOf(jSpinner.getValue().toString());
				if (jSpinnerValue == 1) {// 一页，分页符不勾选
					pageSetBox.setEnabled(false);
					pageSetBox.setSelected(false);
				} else {
					pageSetBox.setEnabled(true);
					pageSetBox.setSelected(true);
				}
				if (pageSetingBean.getPageSets() != jSpinnerValue) {// 更改分页符条数，更改分页符设置
					pageSetingBean.setPageWidth(0);
					pageSetingBean.setPageSets(jSpinnerValue - 1);
					pageSetingBean.setPageX(0);
					// 清空面板分页符；
					removePageSetLine();

					JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
					// 重新生成分页符
					desktopPane.getPageSetLines().clear();
					// 添加面板分页符
					addPageSetLine();
					// 分页符是否继续移动
					isDragged();
				}
			}
		});
	}

	/**
	 * 预览监听
	 * 
	 */
	private void buttonViewAction() {
		if (pageSetBox.isSelected() && pageSetBox.isEnabled()) {// 是否显示分页
			// 复选框必须显示，并且勾选\
			// 删除分页符
			this.removePageSetLine();
			// 预览
			SplitWorkflowDialog dialog = new SplitWorkflowDialog();
			dialog.setVisible(true);
			// 预览面板关闭，添加分页符
			this.addList();
		}
	}

	/**
	 * 确定按钮
	 * 
	 */
	private void okButtonAction() {
		this.dispose();
	}

	/**
	 * 显示分页符
	 * 
	 */
	private void showPageSetBoxAction() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null && pageSetingBean == null) {// 面板不存在
			return;
		}
		if (pageSetBox.isSelected()) {// 显示分页符
			addPageSetLine();
		} else {// 不显示分页符
			removePageSetLine();
			pageSetingBean.setShowPageSet(false);
		}

	}

	/**
	 * 获取分页符信息Bean对象
	 * 
	 * @return TmpPageSetingBean
	 */
	private TmpPageSetingBean getPageSetingBean() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {// 面板不存在
			return null;
		}
		return desktopPane.getFlowMapData().getPageSetingBean();
	}

	/**
	 * 删除分页符
	 * 
	 * @param listPageSet
	 */
	private void removePageSetLine() {
		PageUtil.removePageSetLine();
		// 刷新面板
		repaintPanel();
	}

	/**
	 * 添加线段(存在的情况下)
	 * 
	 */
	private void addList() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		for (JecnPageSetLine jecnPageSetLine : workflow.getPageSetLines()) {
			workflow.add(jecnPageSetLine);
		}
		pageSetBox.setSelected(true);
		pageSetingBean.setShowPageSet(true);
		// 刷新面板
		repaintPanel();
	}

	/**
	 * 刷新面板
	 * 
	 */
	private void repaintPanel() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		desktopPane.repaint();
		imageToolTip.repaint();
	}

	/**
	 * 面板分页页面宽度未设置，初始化算法
	 * 
	 * @param pageSetInts
	 * @return
	 */
	private int getPageWidth(int pageSetInts) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 面板宽度
		int workFlowWidth = desktopPane.getWidth();
		// 角色移动宽度
		int roleWidth = PageUtil.getReduceRoleWorkWidth();

		// 分页显示 每页宽度
		return (workFlowWidth - roleWidth) / (pageSetInts + 1);
	}

	/**
	 * 面板添加分页符
	 * 
	 * @param pageSetInts
	 */
	private void addPageSetLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取分割线条数
		int pageSetInts = getPageNum();
		if (pageSetInts < 1) {// 一页
			return;
		}
		List<JecnPageSetLine> listPageSet = desktopPane.getPageSetLines();
		int pageSetWidth = 0;
		if (desktopPane.getFlowMapData().getPageSetingBean().getPageWidth() > 0) {
			pageSetWidth = desktopPane.getFlowMapData().getPageSetingBean().getPageWidth();
		} else {
			pageSetWidth = getPageWidth(pageSetInts);
		}

		JecnPageSetLine jecnPageSetLine = null;
		// 第一条竖向分割线的X坐标
		int pageSentX = PageUtil.getReduceRoleWorkWidth();

		if (listPageSet.size() == 0 || listPageSet.size() != (pageSetInts)) {// 两页，一根线，三爷两根线
			for (int i = 1; i <= pageSetInts; i++) {// 循环分页符条数
				jecnPageSetLine = new JecnPageSetLine();
				// 角色移动宽度 + 分页显示的每页宽度* 页数
				jecnPageSetLine.setIntX(pageSentX + pageSetWidth * i);
				// 添加集合
				listPageSet.add(jecnPageSetLine);
				// 添加到面板上
				desktopPane.add(jecnPageSetLine);
				// 设置显示位置
				jecnPageSetLine.setVHLineBounds();
			}
		} else if (!pageSetingBean.isShowPageSet()) {
			for (JecnPageSetLine jecnPageSetLine2 : listPageSet) {
				desktopPane.add(jecnPageSetLine2);
				// 设置显示位置
				jecnPageSetLine2.setVHLineBounds();
			}
		}
		pageSetingBean.setShowPageSet(true);
		// 最后一根线X坐标
		int lastIntX = listPageSet.get(listPageSet.size() - 1).getIntX();
		// 设置滑块初始值 最后一根竖线的X坐标值
		slider.setValue(lastIntX);

		pageSetingBean.setPageX(lastIntX);
		// 每页宽度
		pageSetingBean.setPageWidth(pageSetWidth);
		// 页数
		pageSetingBean.setPageSets(pageSetInts + 1);

		imageToolTip.repaint();

	}

	/**
	 * 布局
	 * 
	 * 
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 3, 5);

		JecnPanel pagePanel = new JecnPanel();
		pageCountPanel.setLayout(new BorderLayout());
		// 页数
		pagePanel.add(pageCountLabel);
		pagePanel.add(jSpinner);
		pagePanel.add(new JLabel("   "));
		// 是否显示分页符
		pagePanel.add(pageSetBox);

		pageCountPanel.add(pagePanel, BorderLayout.WEST);
		// 预览
		pageCountPanel.add(buttonView, BorderLayout.EAST);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		mainPanel.add(pageCountPanel, c);

		JecnPanel jecnPanel = new JecnPanel();

		jecnPanel.setLayout(new BorderLayout());

		jecnPanel.add(slider, BorderLayout.NORTH);
		jecnPanel.add(imageToolTip, BorderLayout.CENTER);

		jecnPanel.setBorder(BorderFactory.createEtchedBorder());
		// 滑块
		// c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0,
		// GridBagConstraints.EAST,
		// GridBagConstraints.HORIZONTAL, insets, 0, 0);
		// mainPanel.add(slider, c);
		// 全景图
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(jecnPanel, c);

		// 按钮
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		this.add(mainPanel);

	}

	/**
	 * 初始化控件
	 */
	private void init() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 全景图缩放比例
		scale = this.getSize().getWidth() / workflow.getSize().getWidth();
		// 是否显示分页符
		pageSetingBean = getPageSetingBean();
		if (pageSetingBean == null) {
			return;
		}
		if (pageSetingBean.getPageSets() > 1) {
			// pageSetBox.setSelected(true);
			jSpinner.setValue(pageSetingBean.getPageSets());
			pageSetBox.setSelected(true);
			// 初始化添加分页符
			addPageSetLine();
			// 分页符是否继续移动
			isDragged();
		} else {
			pageSetBox.setEnabled(false);
		}
	}

	/**
	 * 面板全景图
	 * 
	 * @author ZHANGXIAOHU
	 * @date： 日期：2014-3-21 时间：上午10:11:59
	 */
	class ImageToolTip extends JPanel {
		public ImageToolTip() {
			this.setBorder(BorderFactory.createEmptyBorder());
		}

		public void paintComponent(Graphics g) {
			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (workflow != null) {
				JComponent jcomponent = (JComponent) workflow;
				Graphics2D g2d = (Graphics2D) g;
				AffineTransform at = g2d.getTransform();
				// 缩放 scale缩放倍数
				g2d.transform(AffineTransform.getScaleInstance(scale, scale));
				ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
				updateDoubleBuffered(jcomponent, dbcomponents);
				jcomponent.paint(g);
				resetDoubleBuffered(dbcomponents);
				g2d.setTransform(at);
				g2d.dispose();
			}
		}

		private void updateDoubleBuffered(JComponent component, ArrayList<JComponent> dbcomponents) {
			if (component.isDoubleBuffered()) {
				dbcomponents.add(component);
				component.setDoubleBuffered(false);
			}
			for (int i = 0; i < component.getComponentCount(); i++) {
				Component c = component.getComponent(i);
				if (c instanceof JComponent)
					updateDoubleBuffered((JComponent) c, dbcomponents);
			}
		}

		private void resetDoubleBuffered(ArrayList<JComponent> dbcomponents) {
			for (JComponent component : dbcomponents)
				component.setDoubleBuffered(true);
		}
	}

	/**
	 * 定义一个方法，用于将滑动条添加到容器中
	 * 
	 * @param slider
	 */
	private void addSlider(JSlider slider) {
		ChangeListener listener;
		listener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				// 取出滑动条的值，并在文本中显示出来
				JSlider source = (JSlider) event.getSource();
				// 拖动滑块 处理
				sliderChangedAction(source.getValue());
			}
		};
		slider.addChangeListener(listener);
	}

	/**
	 * 拖动滑块 处理
	 * 
	 * @param sliderValue
	 *            滑块当前坐标值
	 */
	private void sliderChangedAction(int sliderValue) {
		if (pageSetBox.isSelected()) {// 显示分页符
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			List<JecnPageSetLine> list = desktopPane.getPageSetLines();

			// 减去 角色移动宽度后的面板宽度
			int roleWidth = PageUtil.getReduceRoleWorkWidth();
			// 分页符 页数
			int pageSetInts = Integer.parseInt(jSpinner.getValue().toString());

			JecnPageSetLine lastPageLine = list.get(list.size() - 1);

			// 页数大于两页 拖动时，最后页的宽度不能大于前面页面宽度
			if (pageSetInts > 2 && beforeSliderDraged > slider.getValue()) {
				// 设置临界值，表面显示不能拖动
				lastPageLine.setIntX(beforeSliderDraged);
				lastPageLine.setVHLineBounds();
				return;
			}

			// 页数等于两页时 拖动时，第一页的宽度不能小于 流程图的一半
			if (pageSetInts == 2 && sliderValue < (desktopPane.getWidth() - roleWidth) / 2 + roleWidth) {
				// 设置临界值，表面显示不能拖动
				lastPageLine.setIntX((desktopPane.getWidth() - roleWidth) / 2 + roleWidth);
				lastPageLine.setVHLineBounds();
				return;
			}
			lastPageLine.setIntX(sliderValue);
			lastPageLine.setVHLineBounds();

			// 每页宽度 最后一页宽度不考虑，算前面的，最后一页宽度时小于或等于前面每页宽度，生成图片，不足补。
			int pageWidth = (lastPageLine.getX() - roleWidth) / (pageSetInts - 1);
			// 记录
			for (int i = 0; i < list.size() - 1; i++) {
				JecnPageSetLine jecnPageSetLine = list.get(i);
				jecnPageSetLine.setIntX(roleWidth + pageWidth * (i + 1));
				jecnPageSetLine.setVHLineBounds();
			}
			// 每页宽度
			pageSetingBean.setPageWidth(pageWidth);
			// 最后一页X坐标
			pageSetingBean.setPageX(lastPageLine.getX());
			// 刷新面板
			repaintPanel();
			// 滑块拖动位移不能超出最后一页宽度
		}
	}

	/**
	 * 分页符是否继续移动(计算开始点0到最后一条分页符X坐标的距离)
	 * 
	 * @return true 继续移动
	 */
	private void isDragged() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		int pageListSize = desktopPane.getPageSetLines().size();
		if (pageListSize < 2) {// 连接线个数小于2的时候，不运算
			return;
		}
		// 角色移动宽度
		int reduceRoleWidth = PageUtil.getReduceRoleWorkWidth();
		// 每页平均宽度
		int widthBetween = (desktopPane.getWidth() - reduceRoleWidth)
				/ (Integer.valueOf(jSpinner.getValue().toString()));
		// 等分情况下，计算开始点0到最后一条分页符X坐标的距离
		beforeSliderDraged = reduceRoleWidth + widthBetween * (pageListSize);
	}

	/**
	 * 获取分页符条数
	 * 
	 * @return
	 */
	private int getPageNum() {
		if (jSpinner == null) {
			return 0;
		}
		// 获取页数， 默认值为1
		int pageSize = Integer.valueOf(jSpinner.getValue().toString());
		return pageSize - 1;
	}
}
