package com.jecn.webservice.zhongsh;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.1.1
 * 2015-11-20T07:20:59.760+08:00
 * Generated source version: 3.1.1
 * 
 */
@WebService(targetNamespace = "http://tempuri.org/", name = "GetUserByTokenServiceSoap")
@XmlSeeAlso({ObjectFactory.class})
public interface GetUserByTokenServiceSoap {

    @WebResult(name = "CheckTokenResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CheckToken", targetNamespace = "http://tempuri.org/", className = "webservice.zhongsh.CheckToken")
    @WebMethod(operationName = "CheckToken", action = "http://tempuri.org/CheckToken")
    @ResponseWrapper(localName = "CheckTokenResponse", targetNamespace = "http://tempuri.org/", className = "webservice.zhongsh.CheckTokenResponse")
    public ArrayOfString checkToken(
        @WebParam(name = "sToken", targetNamespace = "http://tempuri.org/")
        java.lang.String sToken
    );
}
