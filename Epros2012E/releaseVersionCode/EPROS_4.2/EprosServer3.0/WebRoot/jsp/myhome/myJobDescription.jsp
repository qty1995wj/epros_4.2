<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div  align="center">
			<div id="attribute"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; width: 95%"
				align="left">
				<table width="100%">
					<tr>
						<td width="10%" height="25px">
							<s:text name="selectPositionC"></s:text>
						</td>
						<td align="left" width="15%">
							<select style="width: 90%" id="posSelect_id"
								onchange="posonChanged()">
								<s:iterator value="#session.webLoginBean.listPosBean">
									<option value='<s:property value="posId"/>'>
										<s:property value="posName" />
									</option>
								</s:iterator>
							</select>
						</td>
						<td align="right">
							<s:if test="#session.webLoginBean.listPosBean.size>0">
								<span style="cursor: pointer;" onclick="posDownload()"><img
										id="download_img"
										src="${basePath}images/common/downLoadDocument.gif" border="0"></img>
								</span>
							</s:if>
						</td>
					</tr>
				</table>
				<div id="myJobDescription_id"
					style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; width: 98%; height: 95%; overflow-y: scroll;"
					align="left">

				</div>
			</div>
		</div>
	</body>
</html>