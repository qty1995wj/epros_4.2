package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.timeline.ActiveTimeLimitPanel;
import epros.designer.gui.timeline.TimeLimitLineBuilder;
import epros.designer.gui.workflow.JecnWorkflowMenuItem;
import epros.designer.service.system.JecnElementAttributePanel;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnDesignerData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnUIUtil;

public class ActivitiesDetailsJpanel extends JecnElementAttributePanel {
	protected Logger log = Logger.getLogger(ActivitiesDetailsDialog.class);
	/** 活动的数据 */
	protected JecnActiveData jecnActiveData;

	/** 活动明细切换面板 JTabbedPane */
	protected JTabbedPane tabPanel = new JTabbedPane();

	/** 基本信息面板 */
	protected JScrollPane baseInfoPanel = new JScrollPane();
	protected ActiveBaseInfoPanel actBaseInfoPanel = null;

	/** 输入面板 */
	protected JScrollPane inPanel = new JScrollPane();
	protected ActivitiesInPanel activitiesInPanel = null;

	/** 输入面板 */
	protected JScrollPane inOutPanel = new JScrollPane();
	protected ActivitiesInOutPanel activitiesInOutPanel = null;

	/** 输出面板 */
	protected JScrollPane outPanel = new JScrollPane();
	protected ActiveOutPanel activeOutPanel = null;

	/** 操作说明面板 */
	protected JScrollPane operationPanel = new JScrollPane();
	protected ActivitiesOperationPanel activitiesOperationPanel = null;

	/** 指标面板 */
	protected JScrollPane targetPanel = new JScrollPane();
	protected ActiveTargetPanel activeTargetPanel = null;

	/** 时间面板 */
	protected JScrollPane timePanel = new JScrollPane();
	protected ActiveTimePanel activeTimePanel = null;

	protected JLabel errorLab = new JLabel("");
	/** 按钮面板 */
	protected JPanel buttonPanel = new JecnPanel();
	/** 控制点面板 */
	protected JScrollPane conPoint = new JScrollPane();

	/** 控制点面板 C */
	private ActiveControlPointTablPanel activeControlPointTablPanel;

	/** 信息化面板 */
	private ActiveOnLineInfoPanel onLineInfoPanel;
	protected JScrollPane activeInfoPoint = new JScrollPane();
	/** 相关标准 */
	private ActiveRefStandardsPanel refStandardsPanel;
	protected JScrollPane refStandScrollPanel = new JScrollPane();

	/** 办理时限 */
	private ActiveTimeLimitPanel timeLimitPanel;
	protected JScrollPane timeLimitScrollPanel = new JScrollPane();

	/** 点击的图形 */
	protected JecnBaseActiveFigure baseActiveFigure;

	private ActivitiesDetailsDialog detailsDialog;

	public ActivitiesDetailsJpanel(JecnActiveData jecnActiveData, JecnBaseActiveFigure baseActiveFigure) {
		super(jecnActiveData, baseActiveFigure);
		this.jecnActiveData = jecnActiveData;
		this.baseActiveFigure = baseActiveFigure;
		// 初始化界面
		initializationInterface();
		initLayout();
	}

	public void initElementPanelData(JecnBaseFigurePanel figurePanel) {
		this.baseActiveFigure = (JecnBaseActiveFigure) figurePanel;
		this.jecnActiveData = baseActiveFigure.getFlowElementData();
		// 基本信息
		actBaseInfoPanel.initInfoPanelData(jecnActiveData, baseActiveFigure);
		if (activitiesInPanel != null) {
			// 输入
			activitiesInPanel.initInfoPanelData(jecnActiveData);
		}
		if (activitiesInOutPanel != null) {
			// 条款化输入和输出
			activitiesInOutPanel.initInfoPanelData(jecnActiveData);
		}
		if (activeOutPanel != null) {
			// 输出
			activeOutPanel.initInfoPanelData(jecnActiveData);
		}
		if (activitiesOperationPanel != null) {// 操作规范
			activitiesOperationPanel.initInfoPanelData(jecnActiveData);
		}
		if (activeTargetPanel != null) {// 指标
			// 指标
			activeTargetPanel.initInfoPanelData(jecnActiveData);
		}
		if (activeTimePanel != null) {// 活动时间
			activeTimePanel.initInfoPanelData(jecnActiveData);
		}

		if (onLineInfoPanel != null) {// 信息化
			onLineInfoPanel.initInfoPanelData(jecnActiveData);
		}

		if (refStandardsPanel != null) {// 相关标准
			refStandardsPanel.initInfoPanelData(jecnActiveData);
		}
		if (activeControlPointTablPanel != null) {// 关键控制点
			activeControlPointTablPanel.initInfoPanelData(jecnActiveData);
		}

		if (timeLimitPanel != null) {// 办理时限
			timeLimitPanel.initInfoPanelData(jecnActiveData);
		}
	}

	/**
	 * 初始化界面
	 * 
	 * @author fuzhh Oct 24, 2012
	 */
	protected void initializationInterface() {
		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		baseInfoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		inPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		inOutPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		outPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		operationPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		targetPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		timePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		conPoint.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		activeInfoPoint.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		refStandScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		timeLimitScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		errorLab.setForeground(Color.red);
		// 添加ui
		tabPanel.setUI(new JecnWindowsTabbedPaneUI());

		this.add(tabPanel);
	}

	protected void initLayout() {
		int num = 0;
		tabPanel.add(baseInfoPanel, num);
		tabPanel.setTitleAt(num, JecnProperties.getValue("actBaseInfo"));

		// 添加活动输入输出、信息化、指标、相关风险等
		addActivityOtherPanel();

		// 基本信息
		actBaseInfoPanel = new ActiveBaseInfoPanel(jecnActiveData, baseActiveFigure);
		baseInfoPanel.setViewportView(actBaseInfoPanel);

		// 输入
		activitiesInPanel = new ActivitiesInPanel(jecnActiveData, detailsDialog);
		inPanel.setViewportView(activitiesInPanel);

		// 输入/输出
		try {
			activitiesInOutPanel = new ActivitiesInOutPanel(jecnActiveData, detailsDialog);
			inOutPanel.setViewportView(activitiesInOutPanel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 操作规范
		activitiesOperationPanel = new ActivitiesOperationPanel(jecnActiveData, detailsDialog);
		operationPanel.setViewportView(activitiesOperationPanel);
		// 指标
		activeTargetPanel = new ActiveTargetPanel(jecnActiveData, detailsDialog);
		targetPanel.setViewportView(activeTargetPanel);
		// 输出
		activeOutPanel = new ActiveOutPanel(jecnActiveData, detailsDialog);
		outPanel.setViewportView(activeOutPanel);
		// 时间
		activeTimePanel = new ActiveTimePanel(jecnActiveData);
		timePanel.setViewportView(activeTimePanel);

		// 控制点 C
		activeControlPointTablPanel = new ActiveControlPointTablPanel(jecnActiveData);
		conPoint.setViewportView(activeControlPointTablPanel);

		// 信息化
		onLineInfoPanel = new ActiveOnLineInfoPanel(jecnActiveData);
		activeInfoPoint.setViewportView(onLineInfoPanel);

		// 关联标准
		refStandardsPanel = new ActiveRefStandardsPanel(jecnActiveData);
		refStandardsPanel.setVerifyInputWhenFocusTarget(true);
		refStandScrollPanel.setViewportView(refStandardsPanel);

		// 办理时限
		timeLimitPanel = new ActiveTimeLimitPanel(jecnActiveData);
		timeLimitPanel.setVerifyInputWhenFocusTarget(true);
		timeLimitScrollPanel.setViewportView(timeLimitPanel);

		// 按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		buttonPanel.add(errorLab);
	}

	/**
	 * 添加活动输入输出、信息化、指标、相关风险等
	 */
	private void addActivityOtherPanel() {
		if (isProcessImplActivity()) {
			return;
		}
		int num = 1;
		JecnDesignerData jecnDesignerData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
				.getDesignerData();
		if (jecnDesignerData != null && jecnDesignerData.getModeType() == ModeType.none) { // 流程
			if (!JecnConfigTool.useNewInout()) {
				tabPanel.add(inPanel, num);
				tabPanel.setTitleAt(num, JecnProperties.getValue("actIn"));
				num++;
				tabPanel.add(outPanel, num);
				tabPanel.setTitleAt(num, JecnProperties.getValue("actOut"));
				num++;
			} else {
				tabPanel.add(inOutPanel, num);
				tabPanel.setTitleAt(num, JecnProperties.getValue("actIn") + "/" + JecnProperties.getValue("actOut"));
				num++;
			}
			tabPanel.add(operationPanel, num);
			tabPanel.setTitleAt(num, JecnProperties.getValue("actOperation"));
			num++;

			tabPanel.add(targetPanel, num);
			tabPanel.setTitleAt(num, JecnProperties.getValue("actTarget"));
			num++;

			if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
				if (JecnConfigTool.isShowTimeText()) {
					// 时间
					tabPanel.add(timePanel, num);
					tabPanel.setTitleAt(num, JecnProperties.getValue("actTime"));
					num++;
				}
				// 控制点
				tabPanel.add(conPoint, num);
				tabPanel.setTitleAt(num, JecnProperties.getValue("conPoint"));
				num++;

				// 信息化
				tabPanel.add(activeInfoPoint, num);
				tabPanel.setTitleAt(num, JecnProperties.getValue("activeInfo"));
				num++;
				// 关联标准
				tabPanel.add(refStandScrollPanel, num);
				tabPanel.setTitleAt(num, JecnProperties.getValue("refStand"));
				num++;

				// 办理时限
				tabPanel.add(timeLimitScrollPanel, num);
				tabPanel.setTitleAt(num, JecnProperties.getValue("timeLimit"));
				num++;
				// 判断驱动规则时间显示
			}
		}
	}

	/**
	 * true：接口（活动）
	 * 
	 * @return
	 */
	private boolean isProcessImplActivity() {
		if (jecnActiveData.getMapElemType() == MapElemType.ActivityImplFigure
				|| jecnActiveData.getMapElemType() == MapElemType.ActivityOvalSubFlowFigure) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 是否有更新
	 * 
	 * @return boolean true：有更新；false：没有更新
	 */
	protected boolean isUpdate() {
		if (actBaseInfoPanel.isUpdate() || activitiesInPanel.isUpdate() || activitiesOperationPanel.isUpdate()
				|| activeOutPanel.isUpdate() || activeTargetPanel.isUpdate() || activeTimePanel.isUpdate()
				|| activeControlPointTablPanel.isUpdate() || onLineInfoPanel.isUpdate() || refStandardsPanel.isUpdate()
				|| timeLimitPanel.isUpdate() || activitiesInOutPanel.inIsUpdate() || activitiesInOutPanel.outIsUpdate()) {
			return true;
		}
		return false;
	}

	public JPanel getButtonPanel() {
		return buttonPanel;
	}

	public JTabbedPane getTabPanel() {
		return tabPanel;
	}

	public boolean saveData() {
		log.info(" starts --->!");
		boolean ret = actBaseInfoPanel.okBtnCheck();
		if (!ret) {// 校验失败
			log.info(" 1");
			return false;
		}
		boolean rett = activitiesInPanel.okBtnCheck();
		if (!rett) {// 校验失败
			log.info(" 2");
			return false;
		}
		boolean rettt = activeOutPanel.okBtnCheck();
		if (!rettt) {// 校验失败
			log.info(" 3");
			return false;
		}

		/*
		 * if (!onLineInfoPanel.isOnLineCheck()) {// 线上信息验证 log.info(" 4");
		 * return; }
		 */
		if (!timeLimitPanel.okBtnCheck()) {
			log.info(" 5");
			return false;
		}
		// 活动类型信息获取
		actBaseInfoPanel.setActiveTypeResult();
		// 活动编号
		jecnActiveData.setActivityNum(actBaseInfoPanel.getActiveNumTextField().getText());
		// 活动名称
		jecnActiveData.setFigureText(actBaseInfoPanel.getActiveNameTextArea().getText());
		// 活动说明
		jecnActiveData.setAvtivityShow(actBaseInfoPanel.getActiveNoteTextArea().getText());
		// 关键活动的类型
		jecnActiveData.setActiveKeyType(actBaseInfoPanel.getJecnActiveData().getActiveKeyType());
		// 关键活动的控制说明
		jecnActiveData.setAvtivityShowAndControl(actBaseInfoPanel.getKeyActiveNoteTextArea().getText());
		// 对应内控矩阵风险点
		jecnActiveData.setInnerControlRisk(actBaseInfoPanel.innerControlRiskTextArea.getText());
		// 对应标准条款
		jecnActiveData.setStandardConditions(actBaseInfoPanel.standardConditionsTextArea.getText());
		// 活动担当(自定义元素1)
		jecnActiveData.setCustomOne(actBaseInfoPanel.getCustonOne());
		// 活动的输入
		activitiesInPanel.setInputResult();

		// 输出说明
		if (activeOutPanel.getNoteTextArea() != null) {
			jecnActiveData.setActivityOutput(activeOutPanel.getNoteTextArea().getText());
		}
		// 活动的输出
		jecnActiveData.setListModeFileT(this.activeOutPanel.getListJecnModeFileT());

		// 标准条款化 输入 -- 输出
		jecnActiveData.setListFigureInTs(activitiesInOutPanel.getInData());
		jecnActiveData.setListFigureOutTs(activitiesInOutPanel.getOutData());

		// 活动的操作规范
		activitiesOperationPanel.setOperationResult();

		// 活动的指标
		jecnActiveData.setListRefIndicatorsT(this.activeTargetPanel.getListJecnRefIndicatorsT());
		// 控制点
		jecnActiveData.setJecnControlPointTList(activeControlPointTablPanel.getJecnControlPointTList());

		// 线上信息执行保存
		onLineInfoPanel.okButListener();
		// 控制点信息
		// 该方法的调用已经移动到AddOrEditPointDialog中的okButon（）方法中去
		// controlPointPanel.okButton();
		// 活动关联标准确定按钮
		refStandardsPanel.okButton();
		// 办理时限
		timeLimitPanel.isOkBut();

		if (baseActiveFigure != null) {
			JecnDesignerCommon.addElemLink(baseActiveFigure, MapType.partMap);
		}
		if (JecnWorkflowMenuItem.isShowTimeLine) {
			// 绘制时间轴
			TimeLimitLineBuilder.INSTANCE.buildTimeLinePanel();
		}
		return true;
	}

	public boolean saveButAction() {
		// 是否更新
		boolean isUpdate = isUpdate();

		boolean saveResult = saveData();
		if (!saveResult) {
			return false;
		}
		// 面板保存
		if (isUpdate) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
			JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
		}
		return true;
	}

	public ActiveTargetPanel getActiveTargetPanel() {
		return activeTargetPanel;
	}

	public JecnActiveData getJecnActiveData() {
		return jecnActiveData;
	}

	protected JComponent getElementAttrubutePanel() {
		return this.tabPanel;
	}

}
