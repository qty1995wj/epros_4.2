package com.jecn.epros.controller;

import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.risk.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.risk.RiskService;
import com.jecn.epros.util.AttachmentDownloadUtils;
import com.jecn.epros.util.excel.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/risk", description = "风险体系", position = 5)
public class RiskController {

    @Autowired
    private RiskService riskService;

    @ApiOperation(value = "风险-搜索")
    @RequestMapping(value = "/risks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RiskViewBean> search(
            @ModelAttribute SearchRiskBean searchRiskBean, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        map.put("searchRiskBean", searchRiskBean);
        map.put("paging", paging);
        RiskViewBean riskViewBean = new RiskViewBean();
        int total = riskService.searchRiskTotal(map);
        List<SearchRiskResultBean> data = riskService.searchRisk(map);
        riskViewBean.setTotal(total);
        riskViewBean.setData(data);
        return ResponseEntity.ok(riskViewBean);
    }

    @PreAuthorize("hasAuthority('riskBaseInfo')")
    @ApiOperation(value = "风险-概况信息")
    @RequestMapping(value = "/risks/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RiskBaseInfoBean> findBaseInfo(@PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        return ResponseEntity.ok(riskService.findBaseInfoBean(map));
    }

    @ApiOperation(value = "风险-树节点展开")
    @RequestMapping(value = "/risks/{id}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> fetchChildren(
            @ApiParam(value = "父节点Id", required = true) @PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // 父节点ID
        map.put("pId", id);
        return ResponseEntity.ok(riskService.findChildRisks(map));
    }

    @ApiOperation(value = "风险-相关文件")
    @RequestMapping(value = "/risks/{id}/files", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RiskRelatedFileBean> findRelatedFiles(@PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        return ResponseEntity.ok(riskService.findRelatedFiles(map));
    }
}