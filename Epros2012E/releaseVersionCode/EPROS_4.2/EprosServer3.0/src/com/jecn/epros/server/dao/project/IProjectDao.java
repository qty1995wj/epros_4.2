package com.jecn.epros.server.dao.project;

import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 项目管理Dao接口
 * 
 * @author 2012-05-23
 * 
 */
public interface IProjectDao extends IBaseDao<JecnProject, Long> {
	
}
