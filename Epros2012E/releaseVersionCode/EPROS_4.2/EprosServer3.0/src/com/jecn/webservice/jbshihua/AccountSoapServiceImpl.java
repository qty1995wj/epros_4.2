package com.jecn.webservice.jbshihua;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;
import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;
import com.jecn.webservice.jbshihua.entity.PostInfo;
import com.jecn.webservice.jbshihua.service.DeptService;
import com.jecn.webservice.jbshihua.service.UserService;

public class AccountSoapServiceImpl implements IAccountSoapService {
	Logger logger = LoggerFactory.getLogger(AccountSoapServiceImpl.class);
	String retMsg = "<ESB><CODE>0</CODE><DESC/><DATA/><DATAINFOS UUID=\"%s\"><DATAINFO uuid=\"%s\" status=\"%s\" errorText=\"%s\"/></DATAINFOS></ESB>";

	public String saveDepts(String deptMsg) {
		System.out.println("----- save depts ,the request msg = {}" + deptMsg);
		DeptService deptService = (DeptService) ApplicationContextUtil.getContext().getBean("DeptServiceImpl");
		this.logger.info("----- save depts ,the request msg = {}", deptMsg);
		String msg = "SUCESS";
		List<JecnOrgView> depts = new ArrayList<JecnOrgView>();
		String UUID = "";
		List<String> uuids = new ArrayList<String>();
		if (StringUtils.isNotBlank(deptMsg)) {
			try {
				if (deptMsg.indexOf("<![CDATA[") == 0) {
					deptMsg = deptMsg.substring(9);
				}
				if (deptMsg.endsWith("]]>")) {
					deptMsg = deptMsg.substring(0, deptMsg.length() - 3);
				}
				Document document = DocumentHelper.parseText(deptMsg);
				UUID = document.getRootElement().attributeValue("uuid");
				List<Element> eles = document.getRootElement().elements();
				for (Element element : eles) {
					List<Element> childs = element.elements();
					String deptName = "";
					String deptCode = "";
					String deptParentCode = "";
					String deptStatus = "";
					String uuid = null;
					String codeId = null;
					JecnOrgView orgView = null;
					for (Element child : childs) {
						orgView = new JecnOrgView();
						if (child.getName().equals("DESC1")) {
							deptName = child.getText();
						}
						if (child.getName().equals("CODE")) {
							deptCode = child.getText();
						}
						if (child.getName().equals("PARENTCODE")) {
							deptParentCode = child.getText();
						}
						if (child.getName().equals("DESC17")) {
							deptStatus = child.getText();
						}
						if ("".equals(deptParentCode)) { // 如果顶级部门父部门0
							deptParentCode = "0";
						}
						if ("UUID".equals(child.getName())) {
							uuid = child.getText();
						}
						
						if ("CODEID".equals(child.getName())) {
							codeId = child.getText();
						}
					}
					uuids.add(uuid);
					orgView = new JecnOrgView(deptCode, deptName, deptParentCode, deptStatus);
					orgView.setUUID(codeId);
					depts.add(orgView);
				}
			 deptService.recevieOrgJB(depts);
			} catch (DocumentException e) {
				msg = "请求信息为空或者格式不对";
				e.printStackTrace();
			} catch (Exception e) {
				msg = "处理失败";
				e.printStackTrace();
			}

		}

		
		String ret = getResultMessage(msg, UUID, uuids);
		this.logger.info("----- save depts ,the ret msg = {}", ret);
		return ret;
	}
	
	private String getResultMessage(String msg,String sendUUID,List<String> sendUUIDS){
		String ret = null;
		if ("SUCESS".equals(msg)) {
			StringBuilder sb = new StringBuilder("");
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			sb.append("<ESB><CODE>S</CODE><DESC>处理成功</DESC>");
			sb.append("<DATAINFOS UUID=\"" + sendUUID + "\">");
			for (String u : sendUUIDS) {
				sb.append("<DATAINFO uuid=\"" + u + "\" status=\"0\" errorText=\"\"/>");
			}

			sb.append("</DATAINFOS>");
			sb.append("</ESB>");
			ret = sb.toString();
		} else {
			ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ESB><CODE>E</CODE><DESC>" + msg + "</DESC></ESB>";
		}
		return ret;
	}

	public String saveUsers(String userMsg) {
		System.out.println("----- save users ,the request msg = {}" + userMsg);
		UserService userService = (UserService) ApplicationContextUtil.getContext().getBean("UserServiceImpl");
		this.logger.info("----- save users ,the request msg = {}", userMsg);
		String msg = "SUCESS";
		List<JecnUserPosView> users = new ArrayList<JecnUserPosView>();
		String UUID = "";
		List<String> uuids = new ArrayList<String>();
		if (StringUtils.isNotBlank(userMsg)) {
			if (userMsg.indexOf("<![CDATA[") == 0) {
				userMsg = userMsg.substring(9);
			}
			if (userMsg.endsWith("]]>")) {
				userMsg = userMsg.substring(0, userMsg.length() - 3);
			}
			try {
				Document document = DocumentHelper.parseText(userMsg);
				UUID = document.getRootElement().attributeValue("uuid");
				List<Element> eles = document.getRootElement().elements("DATAINFO");

				JecnUserPosView userPosView = null;
				for (Element element : eles) {
					List<Element> childs = element.elements();
					String userCode = "";
					String realName = "";
					String userDept = "";
					String userPosNum = "";
					String userPosName = "";
					String phone = "";
					String email = "";
					String userStatus = "";
					String uuid = "";
					String codeId = null;
					userPosView = new JecnUserPosView();
					for (Element child : childs) {
						if (child.getName().equals("CODE")) {
							userCode = child.getText();
						}
						if (child.getName().equals("DESC1")) {
							realName = child.getText();
						}
						if (child.getName().equals("DESC3")) {
							userDept = child.getText();
						}
						if (child.getName().equals("DESC6")) {
							userPosName = child.getText();
						}
						if (child.getName().equals("DESC6")) {
							userPosName = child.getText();
						}
						if (child.getName().equals("DESC21")) {
							phone = child.getText();
						}
						if (child.getName().equals("DESC33")) {
							email = child.getText();
						}
						if (child.getName().equals("DESC43")) {
							userStatus = child.getText();
						}
						if (child.getName().equals("UUID")) {
							uuid = child.getText();
						}
						if ("CODEID".equals(child.getName())) {
							codeId = child.getText();
						}
					}
					userPosView.setUserNum(codeId);
					userPosView.setLoginName(userCode);
					userPosView.setTrueName(realName);
					userPosView.setOrgNum(userDept);
					userPosView.setPhoneStr(phone);
					userPosView.setEmail(email);
					userPosView.setPosName(userPosName);
					userPosView.setPosNum(userPosNum);
					userPosView.setStatus(userStatus);
					uuids.add(uuid);
					users.add(userPosView);
				}
				userService.receiveUser(users);
			} catch (DocumentException e) {
				msg = "请求信息为空或者格式不对";
				e.printStackTrace();
			} catch (Exception e) {
				msg = "处理失败";
				e.printStackTrace();
			}
		} else {
			msg = "请求信息为空或者格式不对";
		}

		String ret = getResultMessage(msg, UUID, uuids);
		this.logger.info("----- save users ,the ret msg = {}", ret);
		return ret;
	}

	public String saveUserJobs(String userMsg) {
		System.out.println("----- save jobs ,the request msg = {}" + userMsg);
		UserService userService = (UserService) ApplicationContextUtil.getContext().getBean("UserServiceImpl");

		this.logger.info("----- save jobs ,the request msg = {}", userMsg);
		String msg = "SUCESS";
		String UUID = "";
		List<String> uuids = new ArrayList<String>();
		if (StringUtils.isNotBlank(userMsg)) {
			if (userMsg.indexOf("<![CDATA[") == 0) {
				userMsg = userMsg.substring(9);
			}
			if (userMsg.endsWith("]]>")) {
				userMsg = userMsg.substring(0, userMsg.length() - 3);
			}
			try {
				Document document = DocumentHelper.parseText(userMsg);
				UUID = document.getRootElement().attributeValue("uuid");
				List<Element> eles = document.getRootElement().elements();

				List<PostInfo> postList = new ArrayList<PostInfo>();
				PostInfo postInfo = null;
				for (Element element : eles) {
					List<Element> childs = element.elements();
					String userCode = "";
					String deptNum = "";
					String postNum = "";
					String posName = "";
					String status = "";
					String uuid = "";
					String codeId = null;
					for (Element child : childs) {
						if (child.getName().equals("DESC2")) { // 一卡通号 对应人员的CODE
							userCode = child.getText();
						}
						if (child.getName().equals("DESC5")) { // 
							deptNum = child.getText();
						}
						if (child.getName().equals("DESC7")) { // 职位编码
							postNum = child.getText();
						}
						if (child.getName().equals("DESC8")) { // 职位名称
							posName = child.getText();
						}
						if (child.getName().equals("DESC10")) { // 状态
							status = child.getText();
						}
						if (child.getName().equals("UUID")) {
							uuid = child.getText();
						}
						if ("CODEID".equals(child.getName())) {
							codeId = child.getText();
						}
					}
					uuids.add(uuid);
					postInfo = new PostInfo(codeId, userCode, deptNum, postNum, posName, status);
					postList.add(postInfo);
				}
				userService.receiveJob(postList);
			} catch (DocumentException e) {
				msg = "请求信息为空或者格式不对";
				e.printStackTrace();
			} catch (Exception e) {
				msg = "处理失败";
				e.printStackTrace();
			}
		} else {
			msg = "请求信息为空或者格式不对";
		}

		String ret = getResultMessage(msg, UUID, uuids);
		this.logger.info("----- save saveJobs ,the ret msg = {}", ret);
		return ret;
	}

}
