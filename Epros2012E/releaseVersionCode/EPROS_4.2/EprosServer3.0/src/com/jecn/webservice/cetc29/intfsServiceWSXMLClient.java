package com.jecn.webservice.cetc29;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

import com.jecn.epros.server.action.web.login.ad.cetc29.JecnCetc29AfterItem;

public class intfsServiceWSXMLClient {

	private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
	private HashMap endpoints = new HashMap();
	private Service service0;

	public intfsServiceWSXMLClient() {
		create0();
		Endpoint intfsServiceWSXMLPortEP = service0.addEndpoint(new QName(
				"http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLPort"), new QName(
				"http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLPortBinding"),
				JecnCetc29AfterItem.WSDL_URL);
		endpoints.put(new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLPort"),
				intfsServiceWSXMLPortEP);
		Endpoint intfsServiceWSXMLLocalEndpointEP = service0.addEndpoint(new QName(
				"http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLLocalEndpoint"), new QName(
				"http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLLocalBinding"),
				"xfire.local://intfsServiceWSXML");
		endpoints.put(new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLLocalEndpoint"),
				intfsServiceWSXMLLocalEndpointEP);
	}

	public Object getEndpoint(Endpoint endpoint) {
		try {
			return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
		} catch (MalformedURLException e) {
			throw new XFireRuntimeException("Invalid URL", e);
		}
	}

	public Object getEndpoint(QName name) {
		Endpoint endpoint = ((Endpoint) endpoints.get((name)));
		if ((endpoint) == null) {
			throw new IllegalStateException("No such endpoint!");
		}
		return getEndpoint((endpoint));
	}

	public Collection getEndpoints() {
		return endpoints.values();
	}

	private void create0() {
		TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
		HashMap props = new HashMap();
		props.put("annotations.allow.interface", true);
		AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm,
				new AegisBindingProvider(new JaxbTypeRegistry()));
		asf.setBindingCreationEnabled(false);
		service0 = asf.create((com.jecn.webservice.cetc29.intfsServiceWSXML.class), props);
		{
			AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName(
					"http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLPortBinding"),
					"http://schemas.xmlsoap.org/soap/http");
		}
		{
			AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName(
					"http://www.meritit.com/ws/IntfsServiceWSXML", "intfsServiceWSXMLLocalBinding"),
					"urn:xfire:transport:local");
		}
	}

	public intfsServiceWSXML getintfsServiceWSXMLPort() {
		return ((intfsServiceWSXML) (this).getEndpoint(new QName("http://www.meritit.com/ws/IntfsServiceWSXML",
				"intfsServiceWSXMLPort")));
	}

	public intfsServiceWSXML getintfsServiceWSXMLPort(String url) {
		intfsServiceWSXML var = getintfsServiceWSXMLPort();
		org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
		return var;
	}

	public intfsServiceWSXML getintfsServiceWSXMLLocalEndpoint() {
		return ((intfsServiceWSXML) (this).getEndpoint(new QName("http://www.meritit.com/ws/IntfsServiceWSXML",
				"intfsServiceWSXMLLocalEndpoint")));
	}

	public intfsServiceWSXML getintfsServiceWSXMLLocalEndpoint(String url) {
		intfsServiceWSXML var = getintfsServiceWSXMLLocalEndpoint();
		org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
		return var;
	}

	public static void main(String[] args) {

		intfsServiceWSXMLClient client = new intfsServiceWSXMLClient();

		// create a default service endpoint
		intfsServiceWSXML service = client.getintfsServiceWSXMLPort();

		// TODO: Add custom client code here
		//
		// service.yourServiceOperationHere();
		String str = service.getModelDatasXML("RY", "; IsGuard='Y' ; ADCode IS NOT NULL",
				"ADCode,UserName,JobType,UserStation,DeptCode,Mail,PhoneNumber");
		System.out.println(str);
		System.out.println("test client completed");
		System.exit(0);
	}

}
