/*      Copyright (c) 2012 JECN Corporation                     */
/*      JECN CONFIDENTIAL AND PROPRIETARY                       */
/*      All rights reserved by JECN Corporation.                */
/*      This program must be used solely for the purpose for    */
/*      which it was furnished by JECN Corporation.   No part   */
/*      of this program may be reproduced  or  disclosed  to    */
/*      others,  in  any form,  without  the  prior  written    */
/*      permission of JECN Corporation. Use  of  copyright      */
/*      notice does not evidence publication of the program.    */
package epros.draw.unredo;

import epros.draw.constant.JecnWorkflowConstant.UnRedoType;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 
 * 一次操作前后数据记录工具类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUnRedoProcessFactory {

	/**
	 * 
	 * 添加
	 * 
	 * @param redoData
	 *            JecnUndoRedoData 操作前的数据对象
	 * @return JecnUnRedoProcess 一次操作记录的数据处理类
	 */
	public static JecnUnRedoProcess createAdd(JecnUndoRedoData redoData) {
		if (redoData == null) {
			throw new IllegalArgumentException("参数不合法！");
		}
		return new JecnUnRedoProcess(null, redoData, UnRedoType.add);
	}

	/**
	 * 
	 * 删除
	 * 
	 * @param undoData
	 *            JecnUndoRedoData 操作后的数据对象
	 * @return JecnUnRedoProcess 一次操作记录的数据处理类
	 */
	public static JecnUnRedoProcess createDelete(JecnUndoRedoData undoData) {
		if (undoData == null) {
			throw new IllegalArgumentException("参数不合法！");
		}
		return new JecnUnRedoProcess(undoData, null, UnRedoType.delete);
	}

	/**
	 * 
	 * 修改
	 * 
	 * @param undoData
	 *            JecnUndoRedoData 操作前的数据对象
	 * @param redoData
	 *            JecnUndoRedoData 操作后的数据对象
	 * @return JecnUnRedoProcess 一次操作记录的数据处理类
	 */
	public static JecnUnRedoProcess createEdit(JecnUndoRedoData undoData, JecnUndoRedoData redoData) {
		if (undoData == null || redoData == null) {
			throw new IllegalArgumentException("参数不合法！");
		}
		return new JecnUnRedoProcess(undoData, redoData, UnRedoType.edit);
	}

	/**
	 * 
	 * 添加且修改（泳池、协作框、关节活动（PA、KSF、KCP）添加置底）
	 * 
	 * @param undoData
	 *            JecnUndoRedoData 操作前的数据对象
	 * @param redoData
	 *            JecnUndoRedoData 操作后的数据对象
	 * @return JecnUnRedoProcess 一次操作记录的数据处理类
	 * 
	 */
	public static JecnUnRedoProcess createAddAndEdit(JecnUndoRedoData undoData, JecnUndoRedoData redoData) {
		if (undoData == null || redoData == null) {
			throw new IllegalArgumentException("参数不合法！");
		}
		return new JecnUnRedoProcess(undoData, redoData, UnRedoType.addAndEdit);
	}

	/**
	 * 删除且添加;元素置换
	 * 
	 * @param undoData
	 * @param redoData
	 * @return
	 */
	public static JecnUnRedoProcess createDeleteAndAdd(JecnUndoRedoData undoData, JecnUndoRedoData redoData) {
		if (undoData == null || redoData == null) {
			throw new IllegalArgumentException("参数不合法！");
		}
		return new JecnUnRedoProcess(undoData, redoData, UnRedoType.deleteAndAdd);
	}
}
