package com.jecn.epros.server.service.dataImport.sync.excelOrDb.convert;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl.XMLConfigBean;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl.ImportDataFactoryImpl.ImportType;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DBConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.HessianConfigBean;

/**
 * 
 * 读取Xml配置文件
 * 
 * @author Administrator
 * 
 */
public class ReadUserConfigXml {
	private final Log log = LogFactory.getLog(ReadUserConfigXml.class);

	private ReadUserConfigXml() {
	}

	/**
	 * 
	 * 获取配置文件的内容
	 * 
	 * @param importType
	 *            导入方式（excel、xml、hessian），默认是excel导入
	 * 
	 * @return AbstractConfigBean 配置文件的内容
	 */
	public static AbstractConfigBean getAbstractConfigBean(ImportType importType) {
		ReadUserConfigXml configXml = new ReadUserConfigXml();
		if (ImportType.excel.equals(importType)) {// excel
			return null;
		} else if (ImportType.hessian.equals(importType)) {// hessian
			return configXml.readConfigFileByHessian();
		} else if (ImportType.xml.equals(importType)) {// xml
			return configXml.readConfigFileByXML();
		} else if (ImportType.db.equals(importType) || ImportType.baseDB.equals(importType)) {// 数据库访问方式
			return configXml.readConfigFileByDB();
		} else if (ImportType.webService.equals(importType)) {// 通过webService获取数据
			return configXml.readConfigFileByDB();
		} else if (ImportType.sapHr.equals(importType)) {// sapHr通过sap连接获取HR数据
			return configXml.readConfigFileByDB();
		} else {
			return configXml.readConfigFileByDB();
		}
	}

	/**
	 * 
	 * 写xml配置文件
	 * 
	 * @param configBean
	 * @return
	 */
	public static boolean writerConfigXml(AbstractConfigBean configBean) {

		ReadUserConfigXml configXml = new ReadUserConfigXml();
		return configXml.writeXml(configBean);
	}

	/**
	 * 
	 * 写xml配置文件
	 * 
	 * @param configBean
	 * @return
	 */
	private boolean writeXml(AbstractConfigBean configBean) {

		if (configBean == null) {
			return false;
		}
		Document doc = getUserConfigXml();
		if (!SyncTool.isNullObj(doc)) {
			// 获取根节点
			Element root = doc.getRootElement();
			// 根节点下与元素
			List<Element> eleList = root.elements();
			if (eleList == null || eleList.size() == 0) {
				return false;
			}

			// 开始时间
			String startTime = SyncTool.emtryToNull(configBean.getStartTime());
			// 间隔时间
			String intervalDay = SyncTool.emtryToNull(configBean.getIntervalDay());
			// 是否手动或自动标识：0：手动；1：自动
			String iaAuto = SyncTool.emtryToNull(configBean.getIaAuto());

			for (Element ele2 : eleList) {
				if ("startTime".equals(ele2.getName())) {
					// 开始时间hh:mm
					if (startTime == null || !startTime.matches("^(0\\d{1}|1\\d{1}|2[0-3]):([0-5]\\d{1})$")) {
						return false;
					} else {
						ele2.setText(startTime);
					}
				} else if ("intervalDay".equals(ele2.getName())) {

					// 间隔天数
					if (intervalDay == null || !intervalDay.matches("^[1-9]\\d*$")) {
						return false;
					} else {
						ele2.setText(intervalDay);
					}
				} else if ("iaAuotParam".equals(ele2.getName())) {
					// 手动或自动更新标识:手动：0；自动：1
					if (iaAuto == null || !iaAuto.matches("^[01]$")) {
						return false;
					} else {
						ele2.setText(iaAuto);
					}
				}
			}

			// 输出流
			FileOutputStream outputStream = null;
			// 写入对象
			XMLWriter xmlWriter = null;
			try {

				outputStream = new FileOutputStream(SyncTool.getUserConfigPath());
				// 写入
				xmlWriter = new XMLWriter(outputStream);

				xmlWriter.write(doc);

			} catch (IOException e) {
				log.error("ReadUserConfigXml类writeXml方法出异常：", e);
				return false;
			} finally {
				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException e) {
						log.error("ReadUserConfigXml类writeXml方法出异常：", e);
						return false;
					}
				}

				if (xmlWriter != null) {
					try {
						xmlWriter.close();
					} catch (IOException e) {
						log.error("ReadUserConfigXml类writeXml方法出异常：", e);
						return false;
					}
				}
			}
		} else {
			return false;
		}

		return true;
	}

	/**
	 * 
	 * 获取配置文件内容
	 * 
	 * @return
	 */
	private Document getUserConfigXml() {
		return JecnFinal.getDocumentByXml(SyncTool.getUserConfigPath());
	}

	/**
	 * 
	 * 基于数据库访问方式
	 * 
	 * @return
	 */
	private AbstractConfigBean readConfigFileByDB() {
		// 获取配置文件内容
		Document doc = getUserConfigXml();

		// 创建bean对象
		DBConfigBean configBean = null;
		if (!SyncTool.isNullObj(doc)) {
			configBean = new DBConfigBean();

			// 获取根节点
			Element root = doc.getRootElement();
			// 根节点下与元素
			List<Element> eleList = root.elements();
			if (eleList == null || eleList.size() == 0) {
				return null;
			}

			for (Element ele2 : eleList) {

				// 开始时间、间隔天数、手动或自动更新标识:手动：0；自动：1
				getTime(ele2, configBean);

				// type节点
				if ("type".equals(ele2.getName()) && "db".equals(ele2.attributeValue("readType"))) {

					// 获取type节点子节点
					List<Element> childList = ele2.elements();
					for (Element ele3 : childList) {
						getValueByDB(ele3, configBean);
					}
				}
			}
		}
		return configBean;
	}

	/**
	 * 
	 * 开始时间、间隔天数、手动或自动更新标识:手动：0；自动：1
	 * 
	 * @param ele2
	 * @param configBean
	 */
	private void getTime(Element ele2, AbstractConfigBean configBean) {

		if ("startTime".equals(ele2.getName())) {

			// 开始时间
			String startTime = SyncTool.emtryToNull(ele2.getTextTrim());
			if (startTime == null) {
				return;
			}
			configBean.setStartTime(startTime);
		} else if ("intervalDay".equals(ele2.getName())) {

			// 间隔天数
			String intervalDay = SyncTool.emtryToNull(ele2.getTextTrim());
			if (intervalDay == null) {
				return;
			}
			configBean.setIntervalDay(intervalDay);
		} else if ("iaAuotParam".equals(ele2.getName())) {
			// 手动或自动更新标识:手动：0；自动：1
			String isAuto = SyncTool.emtryToNull(ele2.getTextTrim());
			if (isAuto == null) {
				return;
			}
			configBean.setIaAuto(isAuto);
		}
	}

	/**
	 * 
	 * 获取属性值，基于数据库方式
	 * 
	 * @param ele3
	 *            type节点子节点
	 * @param configBean
	 *            DBConfigBean
	 */
	private void getValueByDB(Element ele3, DBConfigBean configBean) {
		if ("jdbc-dialect".equals(ele3.getName())) {// 数据库方言
			configBean.setDialect(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("jdbc-driver".equals(ele3.getName())) {// 数据库驱动
			configBean.setDriver(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("jdbc-url".equals(ele3.getName())) {// 数据库访问地址
			configBean.setUrl(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("jdbc-username".equals(ele3.getName())) {// 数据库登录名
			configBean.setUsername(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("jdbc-password".equals(ele3.getName())) {// 数据库登录密码
			configBean.setPassword(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("dept-sql".equals(ele3.getName())) {// 部门查询sql
			configBean.setDeptSql(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("user-sql".equals(ele3.getName())) {// 用户查询sql
			configBean.setUserSql(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("pos-sql".equals(ele3.getName())) {
			configBean.setBasePosSql(SyncTool.emtryToNull(ele3.getTextTrim())); // 基准岗位查询sql
		} else if ("procedure-sql".equals(ele3.getName())) {// 东航存储过程验证
			configBean.setProcedureSql(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("posGrpup-sql".equals(ele3.getName())) {// 岗位组同步，岗位组sql查询
			configBean.setPosGroupSql(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("validate-sql".equals(ele3.getName())) {// 数据同步前，校验同步是否正常
			configBean.setValidateSql(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("update-sql".equals(ele3.getName())) {// 数据同步前，校验同步是否正常
			configBean.setUpdateSql(SyncTool.emtryToNull(ele3.getTextTrim()));
		}
	}

	/**
	 * 
	 * 读取Hessian所必须读配置文件
	 * 
	 * @return AbstractConfigBean HessianConfigBean对象或者null
	 */
	private AbstractConfigBean readConfigFileByHessian() {
		// 获取配置文件内容
		Document doc = getUserConfigXml();
		// 创建bean对象
		HessianConfigBean configBean = null;
		if (!SyncTool.isNullObj(doc)) {
			configBean = new HessianConfigBean();

			// 获取根节点
			Element root = doc.getRootElement();
			// 根节点下与元素
			List<Element> eleList = root.elements();
			if (eleList == null || eleList.size() == 0) {
				return null;
			}

			for (Element ele2 : eleList) {

				// 开始时间、间隔天数、手动或自动更新标识:手动：0；自动：1
				getTime(ele2, configBean);

				// type节点
				if ("type".equals(ele2.getName()) && "hessian".equals(ele2.attributeValue("readType"))) {

					// 获取type节点子节点
					List<Element> childList = ele2.elements();
					for (Element ele3 : childList) {
						getValueByHessian(ele3, configBean);
					}
				}
			}
		}
		return configBean;
	}

	/**
	 * 
	 * 获取属性值，基于hessian方式
	 * 
	 * @param ele3
	 *            type节点子节点
	 * @param configBean
	 *            HessianConfigBean
	 */
	private void getValueByHessian(Element ele3, HessianConfigBean configBean) {
		if ("port".equals(ele3.getName())) {// 端口
			configBean.setPort(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("url".equals(ele3.getName())) { // URL地址
			configBean.setUrl(SyncTool.emtryToNull(ele3.getTextTrim()));
		} else if ("person".equals(ele3.getName())) {// 人员
			// 接口
			configBean.setUserIClass(SyncTool.emtryToNull(ele3.attributeValue("person-iclass")));
			// 类
			configBean.setUserClass(SyncTool.emtryToNull(ele3.attributeValue("person-class")));
			// 执行方法
			configBean.setUserMothed(SyncTool.emtryToNull(ele3.attributeValue("person-method")));
		} else if ("position".equals(ele3.getName())) {// 岗位
			// 接口
			configBean.setPosIClass(SyncTool.emtryToNull(ele3.attributeValue("position-iclass")));
			// 类
			configBean.setPosClass(SyncTool.emtryToNull(ele3.attributeValue("position-class")));
			// 执行方法
			configBean.setPosMothed(SyncTool.emtryToNull(ele3.attributeValue("position-method")));
		} else if ("dept".equals(ele3.getName())) {// 部门
			// 接口
			configBean.setDeptIClass(SyncTool.emtryToNull(ele3.attributeValue("dept-iclass")));
			// 类
			configBean.setDeptClass(SyncTool.emtryToNull(ele3.attributeValue("dept-class")));
			// 执行方法
			configBean.setDeptMothed(SyncTool.emtryToNull(ele3.attributeValue("dept-method")));
		}
	}

	private AbstractConfigBean readConfigFileByXML() {
		// 获取配置文件内容
		Document doc = getUserConfigXml();
		// 创建bean对象
		XMLConfigBean configBean = null;
		if (!SyncTool.isNullObj(doc)) {
			configBean = new XMLConfigBean();

			// 获取根节点
			Element root = doc.getRootElement();
			// 根节点下与元素
			List<Element> eleList = root.elements();
			if (eleList == null || eleList.size() == 0) {
				return null;
			}

			for (Element ele2 : eleList) {

				// 开始时间、间隔天数、手动或自动更新标识:手动：0；自动：1
				getTime(ele2, configBean);

				// type节点
				if ("type".equals(ele2.getName()) && "xml".equals(ele2.attributeValue("readType"))) {

					// 获取type节点子节点
					List<Element> childList = ele2.elements();
					for (Element ele3 : childList) {
						if ("xmlFilePath".equals(ele3.getName())) {// xml文件路径
							configBean.setSrcXmlFilePath(SyncTool.emtryToNull(ele3.getTextTrim()));
						}
					}
				}
			}
		}
		return configBean;
	}
}
