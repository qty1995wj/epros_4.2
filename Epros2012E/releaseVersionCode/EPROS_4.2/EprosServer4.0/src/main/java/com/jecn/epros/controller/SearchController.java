package com.jecn.epros.controller;

import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.keySearch.ResourceSearchResultBean;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.keySearch.KeySearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@Api(value = "/search", description = "资源检索", position = 12)
public class SearchController {

    @Autowired
    private KeySearchService searchService;

    @ApiOperation(value = "资源检索")
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResourceSearchResultBean> keyWordSearch(
            @ApiParam(value = "关键词") @RequestParam String key,
            @ApiParam(value = "类型[0:全部 1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;7:流程和架构]", required = true, allowableValues = "0,1,2,3,4,5,6")
            @RequestParam int type, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("searchName", key);
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        map.put("relatedType", type);
        map.put("paging", paging);
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        return ResponseEntity.ok(searchService.keyWordSearch(map));
    }
}
