package epros.draw.gui.swing.popup.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * 
 * 表单元格渲染器
 * 
 * @author ZHOUXY
 * 
 */
class JecnPopupTableCellRenderer extends DefaultTableCellRenderer {
	/** 表 */
	private JecnPopupTable popupTable = null;
	/** 选中 */
	private boolean isSelected = false;
	/** 是否有焦点 true：有；false：没有 */
	private boolean hasFocus = false;

	/** 表第一行渐变颜色 */

	/** 选中框边框渐变靠外颜色 */
	private final Color selectedBorderOutColor = new Color(211, 227, 248);
	/** 选中框边框渐变靠内颜色 */
	private final Color selectedBorderInnerColor = Color.WHITE;

	private final Font tableFont = new Font(Font.DIALOG, Font.PLAIN, 12);

	public JecnPopupTableCellRenderer(JecnPopupTable popupTable) {
		this.popupTable = popupTable;
		this.setOpaque(false);
		this.setBorder(null);
		this.setHorizontalAlignment(SwingConstants.LEFT);
		this.setFont(tableFont);
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		this.isSelected = isSelected;
		this.hasFocus = hasFocus;

		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);

		// 给定参数是图片对象时，给渲染器赋图片并且清空内容
		imageIconCellRenderer(value);

		return this;
	}

	/**
	 * 
	 * 绘制组件
	 * 
	 */
	public void paint(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		int w = this.getWidth();
		int h = this.getHeight();

		if (isSelected || hasFocus) {
			Color oldColor = g2d.getColor();

			// 整体渐变
			g2d.setPaint(new GradientPaint(0, 0, selectedBorderInnerColor, 0,
					this.getHeight() / 4 * 3, selectedBorderOutColor));
			g2d.fillRect(0, 0, w, h);
			g2d.setColor(oldColor);

		} else {
			g2d.setColor(this.getBackground());
			g2d.fillRect(0, 0, w, h);
		}
		super.paint(g);
	}

	public void setBorder(Border paramBorder) {
	}

	/**
	 * 
	 * 给定参数是图片对象时，给渲染器赋图片并且清空内容
	 * 
	 * @param value
	 *            Object 单元格对应数据对象
	 */
	private void imageIconCellRenderer(Object value) {
		if (value != null && value instanceof ImageIcon) {
			this.setText("");
			this.setIcon((ImageIcon) value);
		}
	}
}