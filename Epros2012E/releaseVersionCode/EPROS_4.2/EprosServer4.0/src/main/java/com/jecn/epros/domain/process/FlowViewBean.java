package com.jecn.epros.domain.process;

import java.util.List;

public class FlowViewBean {
    private int total;
    private List<FlowSearchResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<FlowSearchResultBean> getData() {
        return data;
    }

    public void setData(List<FlowSearchResultBean> data) {
        this.data = data;
    }
}
