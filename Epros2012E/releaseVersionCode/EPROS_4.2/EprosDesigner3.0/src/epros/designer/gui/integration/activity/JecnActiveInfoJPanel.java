package epros.designer.gui.integration.activity;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 活动类别信息面板
 * 
 * @author ZHANGXIOAHU
 * @date： 日期：2013-10-30 时间：下午02:36:04
 */
public class JecnActiveInfoJPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(JecnActiveInfoJPanel.class);
	/** 滚动面板 */
	private JScrollPane processScrollPane;
	/** 操作按钮面板 */
	private JecnPanel operaButPanel;
	/** 添加按钮 */
	private JButton addBut;
	/** 编辑 */
	private JButton editBut;
	/** 删除 */
	private JButton deleteBut;

	private ProcessTable processTable = null;

	private List<JecnActiveTypeBean> listActiveTypeBean;

	public JecnActiveInfoJPanel(int width, int height) {
		super(width, height);
		init();
	}

	private void init() {
		// 信息面板所属的滚动面板
		processScrollPane = new JScrollPane();
		// 操作按钮面板
		operaButPanel = new JecnPanel(420, 30);
		// 添加按钮
		addBut = new JButton(JecnProperties.getValue("addBtn"));
		// 编辑
		editBut = new JButton(JecnProperties.getValue("editBtn"));
		// 删除
		deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

		// 添加监听
		addBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}

		});

		editBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}

		});

		deleteBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}

		});
		// 设置面板默认背景色
		processScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		operaButPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 流程滚动面板大小
		Dimension dimension = new Dimension(410, 180);
		processScrollPane.setPreferredSize(dimension);
		processScrollPane.setMaximumSize(dimension);
		processScrollPane.setMinimumSize(dimension);
		processScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());

		GridBagConstraints c = null;
		Insets insets = new Insets(1, 1, 1, 1);

		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createTitledBorder(""));
		// 类别
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		this.add(processScrollPane, c);
		// processScrollPane
		processTable = new ProcessTable();
		processScrollPane.setViewportView(processTable);

		// 操作按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		this.add(operaButPanel, c);
		// 设置按钮布局
		operaButPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 添加按钮到按钮面板
		operaButPanel.add(addBut);
		operaButPanel.add(editBut);
		operaButPanel.add(deleteBut);
	}

	/**
	 * 添加
	 */
	private void addButPerformed() {
		AddActivityTypeDialog addActiveTypeDialog = new AddActivityTypeDialog();
		addActiveTypeDialog.setVisible(true);
		if (addActiveTypeDialog.isOperation) {// 添加成功
			Vector<String> v = new Vector<String>();
			if(addActiveTypeDialog.getTypeId()!= null){
				v.add(String.valueOf(addActiveTypeDialog.getTypeId()));
			}else {
				return;
			}
			v.add(addActiveTypeDialog.getName());
			((DefaultTableModel) processTable.getModel()).addRow(v);
		}
	}

	/**
	 * 编辑
	 */
	@SuppressWarnings("unchecked")
	private void editButPerformed() {
		int rowCount = processTable.getSelectedRowCount();
		if (rowCount == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseChooseEditType"));
			return;
		} else if (rowCount > 1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		Vector<Vector<String>> vectors = ((DefaultTableModel) processTable
				.getModel()).getDataVector();
		String typeName = null;
		Long typeId = null;
		int selectedRow = processTable.getSelectedRow();
		for (int j = 0; j < vectors.size(); j++) {
			Vector<String> vector = vectors.get(selectedRow);
			typeId = Long.valueOf(vector.get(0));
			typeName = vector.get(1);
		}
		JecnActiveTypeBean activeTypeBean = new JecnActiveTypeBean();
		activeTypeBean.setTypeId(typeId);
		activeTypeBean.setTypeName(typeName);
		Long ruletypeId = Long.valueOf(processTable.getModel().getValueAt(
				selectedRow, 0).toString());

		EditActivityTypeDialog editProcessTypeDialog = new EditActivityTypeDialog(
				ruletypeId, activeTypeBean);
		editProcessTypeDialog.setVisible(true);
		if (editProcessTypeDialog.isOperation) {
			processTable.getModel().setValueAt(editProcessTypeDialog.getName(),
					processTable.getSelectedRow(), 1);
		}
	}

	/**
	 * 删除
	 */
	private void deleteButPerformed() {
		int[] selectRows = processTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties
					.getValue("pleaseChooseDeleteType"));
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		Vector<Vector<String>> vectors = ((DefaultTableModel) processTable
				.getModel()).getDataVector();
		List<Long> activeTypeIds = new ArrayList<Long>();
		for (int i = 0; i < selectRows.length; i++) {
			Vector<String> vector = vectors.get(selectRows[i]);
			activeTypeIds.add(Long.valueOf(vector.get(0)));
		}
		// 删除数据库中的流程类别数据
		try {
			ConnectionPool.getActivityAction().delteFlowArributeType(
					activeTypeIds);
		} catch (Exception e) {
			log.error("JecnActiveInfoJPanel deleteButPerformed is error", e);
		}

		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) processTable.getModel())
					.removeRow(selectRows[i]);
		}
		processTable = new ProcessTable();
		processScrollPane.setViewportView(processTable);
	}

	class ProcessTable extends JTable {

		ProcessTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		public Vector<Vector<String>> getContent() {
			Vector<Vector<String>> vs = new Vector<Vector<String>>();
			// 获取流程KPI值数据
			// 获取类型数据
			try {
				listActiveTypeBean = ConnectionPool.getActivityAction()
						.findJecnActiveTypeBeanList();
				for (JecnActiveTypeBean activeTypeBean : listActiveTypeBean) {
					Vector<String> row = new Vector<String>();
					row.add(String.valueOf(activeTypeBean.getTypeId()));
					row.add(activeTypeBean.getTypeName());
					vs.add(row);
				}
			} catch (Exception e1) {
				log.error("JecnActiveInfoJPanel is error", e1);
			}
			return vs;
		}

		public ProcessTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("id"));
			// title.add("类别编号");
			title.add(JecnProperties.getValue("typeName"));
			return new ProcessTableMode(getContent(), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0 };
		}

		class ProcessTableMode extends DefaultTableModel {
			public ProcessTableMode(Vector<Vector<String>> data,
					Vector<String> title) {
				super(data, title);
			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}
		}
	}

	public List<JecnActiveTypeBean> getListActiveTypeBean() {
		if (listActiveTypeBean == null) {
			return new ArrayList<JecnActiveTypeBean>();
		}
		return listActiveTypeBean;
	}
}
