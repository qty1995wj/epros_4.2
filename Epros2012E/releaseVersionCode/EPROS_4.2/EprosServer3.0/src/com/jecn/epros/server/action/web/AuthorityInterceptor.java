package com.jecn.epros.server.action.web;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.jecn.epros.server.service.popedom.IJecnRoleService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 权限过滤器
 * 
 * @author Administrator
 * 
 */
public class AuthorityInterceptor extends AbstractInterceptor {
	private Logger log = Logger.getLogger(AuthorityInterceptor.class);
	private IJecnRoleService jecnRoleService;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		ActionContext ctx = invocation.getInvocationContext();
		WebLoginBean webLoginBean = (WebLoginBean) ctx.getSession().get("webLoginBean");
		if (webLoginBean == null) {
			return "login";
		}

		// 系统管理员和浏览管理员
//		if (webLoginBean.isAdmin() || webLoginBean.isViewAdmin()) {
			return invocation.invoke();
//		}
//		HttpServletRequest request = ServletActionContext.getRequest();
//
//		if (jecnRoleService.isViewAuth(webLoginBean, ctx.getName(), request)) {
//			return invocation.invoke();
//		} else {
//			if ("dbClick".equals(request.getParameter("dbClick"))) {
//				PrintWriter out = null;
//				try {
//					out = ServletActionContext.getResponse().getWriter();
//					// 您没有访问权限或未发布！
//					out.write("noPopedom");
//					out.flush();
//				} catch (Exception e) {
//					throw e;
//				} finally {
//					if (out != null) {
//						try {
//							out.close();
//						} catch (Exception e) {
//							log.error("关闭流异常", e);
//						}
//					}
//				}
//			}
//			return "noPopedom";
//		}
	}

	public IJecnRoleService getJecnRoleService() {
		return jecnRoleService;
	}

	public void setJecnRoleService(IJecnRoleService jecnRoleService) {
		this.jecnRoleService = jecnRoleService;
	}

}
