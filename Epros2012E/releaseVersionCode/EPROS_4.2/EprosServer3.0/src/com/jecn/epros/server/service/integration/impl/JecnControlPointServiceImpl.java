package com.jecn.epros.server.service.integration.impl;

import com.jecn.epros.server.bean.integration.JecnControlPoint;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.service.integration.IJecnControlPointService;

public class JecnControlPointServiceImpl extends AbsBaseService<JecnControlPoint, String> implements IJecnControlPointService {

}
