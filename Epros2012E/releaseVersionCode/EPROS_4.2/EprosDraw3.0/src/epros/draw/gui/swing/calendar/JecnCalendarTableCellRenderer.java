package epros.draw.gui.swing.calendar;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 表单元格渲染器
 * 
 * @author ZHOUXY
 * 
 */
class JecnCalendarTableCellRenderer extends DefaultTableCellRenderer {
	/** 日历渲染器 */
	private JecnCalendarDialog dialog = null;
	/** 选中 */
	private boolean isSelected = false;
	/** 是否有焦点 true：有；false：没有 */
	private boolean hasFocus = false;

	/** 表第一行渐变颜色 */
	private final Color topNoColor = new Color(237, 237, 237);
	private final Color buttomNoColor = new Color(207, 207, 207);
	/** 表第一行渐变颜色 */

	/** 选中框背景颜色 */
	private final Color selectedBackgroudColor = new Color(94, 153, 228);
	/** 选中框边框渐变靠外颜色 */
	private final Color selectedBorderOutColor = new Color(64, 145, 255);
	/** 选中框边框渐变靠内颜色 */
	private final Color selectedBorderInnerColor = new Color(187, 234, 252);

	/** 第一行字体 */
	private final Font tableHeaderFont = new Font(Font.DIALOG, Font.PLAIN, 10);
	/** 其他行字体 */
	private final Font tableFont = new Font(Font.DIALOG, Font.BOLD, 12);

	/** 默认背景颜色 */
	private Color jecnBackgroudColor = Color.WHITE;

	public JecnCalendarTableCellRenderer(JecnCalendarDialog dialog) {
		this.dialog = dialog;
		this.setOpaque(false);
		this.setBorder(null);
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		this.isSelected = isSelected;
		this.hasFocus = hasFocus;

		Component component = super.getTableCellRendererComponent(table, value,
				isSelected, hasFocus, row, column);

		this.setHorizontalAlignment(SwingConstants.CENTER);

		if (row == 0) {
			this.setFont(tableHeaderFont);
			component.setForeground(Color.BLACK);
		} else {
			this.setFont(tableFont);

			if (isSelected) {// 选中
				component.setForeground(Color.WHITE);
			} else {
				if (column == 0 || column == 6) {
					component.setForeground(Color.red);
				} else {
					component.setForeground(Color.BLACK);
				}
			}
		}
		return component;
	}

	/**
	 * 
	 * 绘制组件
	 * 
	 */
	public void paint(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		int w = this.getWidth();
		int h = this.getHeight();

		if (isSelected || hasFocus) {
			Color oldColor = g2d.getColor();

			g2d.setColor(selectedBackgroudColor);
			g2d.fillRect(0, 0, w, h);

			int s = 6;
			int s2 = s / 2;

			// 左上角
			g2d.setPaint(new GradientPaint(0, 0, selectedBorderOutColor,
					s * 3 / 4, s * 3 / 4, selectedBorderInnerColor));
			g2d.fillRect(0, 0, s, s);
			// 右上角
			g2d.setPaint(new GradientPaint(w, 0, selectedBorderOutColor, w - s
					* 3 / 4, s * 3 / 4, selectedBorderInnerColor));
			g2d.fillRect(w - s, 0, s, s);
			// 左下角
			g2d.setPaint(new GradientPaint(0, h, selectedBorderOutColor,
					s * 3 / 4, h - s * 3 / 4, selectedBorderInnerColor));
			g2d.fillRect(0, h - s, s, s);
			// 右下角
			g2d.setPaint(new GradientPaint(w, h, selectedBorderOutColor, w - s
					* 3 / 4, h - s * 3 / 4, selectedBorderInnerColor));
			g2d.fillRect(w - s, h - s, s, s);

			// 上中
			g2d.setPaint(new GradientPaint(s2, 0, selectedBorderOutColor, s2,
					s2, selectedBorderInnerColor));
			g2d.fillRect(s2, 0, w - s2 * 2, s);
			// 左中
			g2d.setPaint(new GradientPaint(0, s2, selectedBorderOutColor, s2,
					s2, selectedBorderInnerColor));
			g2d.fillRect(0, s2, s, h - s2 * 2);
			// 右中
			g2d.setPaint(new GradientPaint(w, s2, selectedBorderOutColor, w
					- s2, s2, selectedBorderInnerColor));
			g2d.fillRect(w - s, s2, s, h - s2 * 2);
			// 下中
			g2d.setPaint(new GradientPaint(s2, h, selectedBorderOutColor, s2, h
					- s2, selectedBorderInnerColor));
			g2d.fillRect(s2, h - s, w - s2 * 2, s);

			g2d.setColor(oldColor);

		} else {
			g2d.setColor(jecnBackgroudColor);
			g2d.fillRect(0, 0, w, h);
		}

		// 画网格
		g2d.setColor(JecnUIUtil.getDefaultBackgroundColor());
		g2d.drawLine(0, h - 1, w, h - 1);

		for (String name : dialog.getTableHeaderNames()) {// 第一行
			if (name.equals(this.getText())) {
				// 边框颜色
				g2d.setColor(JecnUIUtil.getDialogBorderColor());
				// 渐变
				g2d.setPaint(new GradientPaint(0, 0, topNoColor, 0, h / 2,
						buttomNoColor));
				g2d.fillRect(0, 0, w, h);
				// 底边框线
				g2d.drawLine(0, h - 1, w, h - 1);
			}
		}

		super.paint(g);
	}

	public void setBorder(Border paramBorder) {
	}

	public void setJecnBackgroudColor(Color jecnBackgroudColor) {
		this.jecnBackgroudColor = jecnBackgroudColor;
	}
}