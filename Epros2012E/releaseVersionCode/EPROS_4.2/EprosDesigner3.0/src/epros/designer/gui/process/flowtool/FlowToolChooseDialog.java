package epros.designer.gui.process.flowtool;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;

public class FlowToolChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(FlowToolChooseDialog.class);
	public FlowToolChooseDialog(List<JecnTreeBean> list) {
		super(list, 9);
		this.setTitle(JecnProperties.getValue("flowToolSelect"));
	}

	public FlowToolChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 9, jecnDialog);
		this.setTitle(JecnProperties.getValue("flowToolSelect"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		List<JecnTreeBean> listTreeBean = new ArrayList<JecnTreeBean>();
		try {
			listTreeBean = ConnectionPool.getFlowTool().getSustainToolsByName(
					name);
		} catch (Exception e) {
			log.error("FlowToolChooseDialog searchByName is error",e);
		}
		return listTreeBean;
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyFlowToolTree();
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 名称
		return title;
	}

}
