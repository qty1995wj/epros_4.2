package epros.draw.gui.figure;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.JecnAbstractFlowElementData;

/**
 * 
 * 流程等级图形
 * 
 * @author ZHAGNXH
 * @date： 日期：Jun 25, 2012 时间：11:32:48 AM
 */
public class BaseFlowLevelFigure extends JecnBaseFigurePanel {
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private int x3;
	private int y3; // 定义坐标属性
	/** 流程等级图形 1,2,3,4 四个级别 */
	protected int intLevel = 1;

	public BaseFlowLevelFigure(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; // 定义画笔
		// 定义字体颜色
		g2d.setColor(this.getFlowElementData().getFontColor());
		paintFigure(g);

		x1 = this.getWidth() / 2 - 1;
		y1 = 0;

		x2 = this.getWidth() - 1;
		y2 = this.getHeight() - 1;

		x3 = 0;
		y3 = this.getHeight() - 1;

		// 绘制底层三角形
		g2d.setPaint(Color.WHITE);
		g2d.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);

		// 分隔图形区域
		int standW = this.getWidth() * 22 / 100;

		int standH = this.getHeight() * 22 / 100;

		int spaceW = this.getWidth() * 4 / 100;

		int spaceH = this.getHeight() * 4 / 100;

		/** 等级外的其他框框颜色 */
		Color otherColor = null;
		if (FillType.one == flowElementData.getFillType()
				|| FillType.two == flowElementData.getFillType()) {
			otherColor = this.getFlowElementData().getFillColor();
		}
		/** 流程等级颜色 */
		Color levelColor = null;
		if (FillType.two == flowElementData.getFillType()) {
			levelColor = this.getFlowElementData().getChangeColor();
		} else {
			levelColor = otherColor;
		}

		if (intLevel == 1) {
			g2d.setPaint(levelColor);
		} else {
			g2d.setPaint(otherColor);
		}
		// 一级填充
		g2d.fillPolygon(new int[] { x1, this.getWidth() / 2 - standW / 2,
				this.getWidth() / 2 + standW / 2 }, new int[] { y1, standH,
				standH }, 3);

		// 二级填充
		if (intLevel == 2) {
			g2d.setPaint(levelColor);
		} else {
			g2d.setPaint(otherColor);
		}
		g2d.fillPolygon(new int[] {
				this.getWidth() / 2 - standW / 2 - spaceW / 2,
				this.getWidth() / 2 + standW / 2 + spaceW / 2,
				this.getWidth() / 2 + standW + spaceW / 2,
				this.getWidth() / 2 - standW - spaceW / 2 }, new int[] {
				standH + spaceH, standH + spaceH, standH + spaceH + standH,
				standH + spaceH + standH }, 4);

		// 三级级填充
		// 二级填充
		if (intLevel == 3) {
			g2d.setPaint(levelColor);
		} else {
			g2d.setPaint(otherColor);
		}
		g2d.fillPolygon(new int[] { this.getWidth() / 2 - standW - spaceW,
				this.getWidth() / 2 + standW + spaceW,
				this.getWidth() / 2 + standW + spaceW + standW / 2,
				this.getWidth() / 2 - standW - spaceW - standW / 2 },
				new int[] { standH + standH + spaceH + spaceH,
						standH + standH + spaceH + spaceH,
						standH + spaceH + standH + spaceH + standH,
						standH + spaceH + standH + spaceH + standH }, 4);

		// // 四级级填充
		// 二级填充
		if (intLevel == 4) {
			g2d.setPaint(levelColor);
		} else {
			g2d.setPaint(otherColor);
		}
		g2d
				.fillPolygon(
						new int[] {
								this.getWidth() / 2 - standW - spaceW - spaceW
										/ 2 - standW / 2,
								this.getWidth() / 2 + standW + spaceW + standW
										/ 2 + spaceW / 2,
								this.getWidth() / 2 + standW + spaceW + standW
										+ spaceW,
								this.getWidth() / 2 - standW - spaceW - standW
										- spaceW }, new int[] {
								standH * 3 + spaceH * 3,
								standH * 3 + spaceH * 3,
								standH * 4 + spaceH * 4,
								standH * 4 + spaceH * 4 }, 4);
	}
}
