package epros.draw.gui.figure;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;

import sun.swing.SwingUtilities2;
import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.GeoEnum;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.designer.JecnDesignerLinkPanel;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.figure.shape.JecnFigureResizePanel;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnFigureComponentAdapter;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 所有图形的基类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnBaseFigurePanel extends JecnBaseFlowElementPanel {

	/** 图形上添加图标； 角色框如果存在岗位， 或接口存在关联时 */
	protected JLabel imageLabel = null;

	/** 图形宽度 */
	protected int userWidth = 0;
	/** 图形H */
	protected int userHeight = 0;

	// **************图形连接线的四个编辑点 **************//
	/** 连接线位置点 左 */
	protected JecnEditPortPanel editLeftPortPanel = null;
	/** 连接线位置点 上 */
	protected JecnEditPortPanel editTopPortPanel = null;
	/** 连接线位置点 右 */
	protected JecnEditPortPanel editRightPortPanel = null;
	/** 连接线位置点 下 */
	protected JecnEditPortPanel editBottomPortPanel = null;

	// **************图形连接线的四个编辑点 **************//

	// **************** 图形八个选中点 ****************//
	/** 上左 */
	protected JecnFigureResizePanel resizeTopLeftPanel = null;
	/** 上中 */
	protected JecnFigureResizePanel resizeTopCenterPanel = null;
	/** 上右 */
	protected JecnFigureResizePanel resizeTopRightPanel = null;
	/** 左中 */
	protected JecnFigureResizePanel resizeLeftCenterPanel = null;
	/** 右中 */
	protected JecnFigureResizePanel resizeRightCenterPanel = null;
	/** 下左 */
	protected JecnFigureResizePanel resizeBottomLeftPanel = null;
	/** 下中 */
	protected JecnFigureResizePanel resizeBottomCenterPanel = null;
	/** 下右 */
	protected JecnFigureResizePanel resizeBottomRightPanel = null;

	// **************** 图形八个选中点 ****************//

	/** 每次改变大小、位置点后记录当前状态下克隆值 此字段使用场合：重绘图形即系统paintComponent */
	protected JecnFigureDataCloneable oldDataCloneable = null;
	/** 阴影缩进值 */
	protected int shadowCount = 0;
	/** 3D效果顶层缩进值 */
	protected int indent3Dvalue = 4;
	/** 3D加阴影 图形边框缩进值 */
	protected int linevalue = 0;

	/** 图形相关连接线集合 */
	protected List<JecnFigureRefManhattanLine> listFigureRefManhattanLine = new ArrayList<JecnFigureRefManhattanLine>();

	/** 设计器专用：连接组件的容器 */
	protected JecnDesignerLinkPanel desLinkPanel = null;
	/** 设计器专用：连接组件的容器位置在南面 */
	protected JecnDesignerLinkPanel desLinkPanelSouth = null;

	private ShadowPanel shadowPanel = null;

	public JecnBaseFigurePanel(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
		init();
		initDesigner();
		// 鼠标移动监听
		initMouseEvent();
	}

	/**
	 * 
	 * 初始化属性
	 * 
	 */
	protected void init() {
		// **************图形连接线的四个编辑点 **************//
		// 是否显示编辑四个点 true:显示 false：不显示
		// 连接线位置点 左
		editLeftPortPanel = JecnEditPortPanel.createLeftEditPortPanel(this);
		// 连接线位置点 右
		editRightPortPanel = JecnEditPortPanel.createRightEditPortPanel(this);
		// 连接线位置点 上
		editTopPortPanel = JecnEditPortPanel.createTopEditPortPanel(this);
		// 连接线位置点 下
		editBottomPortPanel = JecnEditPortPanel.createBottomEditPortPanel(this);
		// **************图形连接线的四个编辑点 **************//

		// **************** 图形八个选中点 ****************//
		// 上左
		resizeTopLeftPanel = JecnFigureResizePanel.createTopLeftResizePanel();
		// 上中
		resizeTopCenterPanel = JecnFigureResizePanel.createTopCenterResizePanel();
		// 上右
		resizeTopRightPanel = JecnFigureResizePanel.createTopRightResizePanel();
		// 左中
		resizeLeftCenterPanel = JecnFigureResizePanel.createLeftCenterResizePanel();
		// 右中
		resizeRightCenterPanel = JecnFigureResizePanel.createRightCenterResizePanel();
		// 下左
		resizeBottomLeftPanel = JecnFigureResizePanel.createBottomLeftResizePanel();
		// 下中
		resizeBottomCenterPanel = JecnFigureResizePanel.createBottomLCenterResizePanel();
		// 下右
		resizeBottomRightPanel = JecnFigureResizePanel.createBottomLRightResizePanel();
		// **************** 图形八个选中点 ****************//

		// 事件：图形大小改变时编辑框大小也改变
		this.addComponentListener(new JecnFigureComponentAdapter());
	}

	/**
	 * 
	 * 初始化设计器组件
	 * 
	 */
	public void initDesigner() {
		if (JecnSystemStaticData.isEprosDesigner()) {// 如果是设计器

			desLinkPanel = new JecnDesignerLinkPanel(this);
			// 设计器专用：连接组件的容器位置在南面
			desLinkPanelSouth = new JecnDesignerLinkPanel(this);
			desLinkPanelSouth.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

			MapElemType mapElemType = this.getFlowElementData().getMapElemType();
			if (MapElemType.Rhombus == mapElemType || MapElemType.ITRhombus == mapElemType) {// 决策、IT决策时
				this.setLayout(new GridLayout(3, 1));

				JPanel panel = new JPanel();
				panel.setOpaque(false);
				panel.setBorder(null);
				panel.setLayout(new BorderLayout());

				this.add(new JLabel());
				this.add(desLinkPanel);
				this.add(panel);
				panel.add(desLinkPanelSouth, BorderLayout.SOUTH);
			} else {
				// 非决策、IT决策图形时
				this.setLayout(new BorderLayout());
				this.add(desLinkPanel, BorderLayout.NORTH);
				this.add(desLinkPanelSouth, BorderLayout.SOUTH);
			}
		}
	}

	/**
	 * 画图
	 * 
	 * @param g
	 */
	protected void paintFigure(Graphics g) {
		// 获取原始绘画呈现处理对象
		Graphics2D graphics2D = (Graphics2D) g;
		RenderingHints originalRenderingHints = graphics2D.getRenderingHints();
		// 获取原始绘画颜色
		Color originalColor = graphics2D.getColor();

		// 指定RenderingHints
		graphics2D.setRenderingHints(JecnUIUtil.getRenderingHints());

		// 绘制图形
		paintFigureProcess(graphics2D);

		// 还原原始RenderingHints对象
		graphics2D.setRenderingHints(originalRenderingHints);
		// 还原原始绘画颜色
		graphics2D.setColor(originalColor);
		//
		graphics2D.setColor(flowElementData.getFontColor());

		// 字体位置 默认中间剧中
		this.setFontPosition(graphics2D, flowElementData.getFontPosition());
	}

	/**
	 * 字体旋转角度计算
	 * 
	 * @param g
	 *            2D画笔
	 * @param currentAngle
	 *            旋转角度
	 * @param fontX
	 *            字体位置点X
	 * @param fontY
	 *            字体位置点Y
	 */
	private void getFontLocation(Graphics2D g) {
		if (flowElementData.getCurrentAngle() == 90.0) {
			g.rotate(Math.PI / 2, this.getWidth() / 2, this.getHeight() / 2);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g.rotate(Math.PI, this.getWidth() / 2, this.getHeight() / 2);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g.rotate(Math.PI / 2 + Math.PI, this.getWidth() / 2, this.getHeight() / 2);
		}
	}

	/**
	 * 
	 * 绘制图形
	 * 
	 * @param g
	 */
	private void paintFigureProcess(Graphics2D g) {
		if (DrawCommon.isNOShadowAnd3D(flowElementData.getMapElemType())) {// 没有3D和阴影效果的图形默认执行普通画图
			paintGeneralFigure(g);
			return;
		}
		GeoEnum geoEnum = flowElementData.getGeoEnum();
		switch (geoEnum) {
		case general:
			paintGeneralFigure(g);
			break;
		case f3d:
			paint3DFigure(g);
			break;
		case shadow:
			paintShadowFigure(g);
			break;
		case shadowAnd3D:
			paintShadowAnd3DFigure(g);
			break;
		default:
			JecnLogConstants.LOG_BASE_FIGURE.error("JecnBaseFigure类中paintFigure处理逻辑异常! GeoEnum=" + geoEnum);
			break;
		}
	}

	/**
	 * 背景渐变色 颜色渐变填充
	 * 
	 * @param g2d
	 * @param jPanelColorChange
	 */
	protected void paintBackColor(Graphics2D g2d) {
		FillType fillType = flowElementData.getFillType();
		switch (fillType) {
		case none:// 没有填充色
			g2d.setPaint(null);
			break;
		case one:
			// 如果双色时存在颜色为空,取面板默认色
			if (flowElementData.getFillColor() == null) {
				g2d.setPaint(JecnUIUtil.getDefaultBackgroundColor());
			} else {
				g2d.setPaint(flowElementData.getFillColor());
			}
			break;
		case two:

			// // topTilt, // 1：向上倾斜 bottom, // 2：向下倾斜 vertical, //
			// 3：垂直horizontal
			FillColorChangType fillColorChangType = flowElementData.getFillColorChangType();
			// 背景渐变色 颜色渐变填充
			JecnPaintFigureUnit.paintBackColor(this, g2d, fillColorChangType, flowElementData.getFillColor(),
					flowElementData.getChangeColor());
			break;
		default:
			JecnLogConstants.LOG_BASE_FIGURE.error("JecnBaseFigure类中paintBackColor处理逻辑异常! FillType=" + fillType);
			break;
		}
	}

	/**
	 * 
	 * 线条色 渐变
	 * 
	 * @param g2d
	 */
	protected void paintLineColor(Graphics2D g2d) {
		if (flowElementData.getFillColor() == null) {
			JecnLogConstants.LOG_BASE_FIGURE.info(ErrorInfoConstant.PARA_OBJ_NULL + "MapElemType:"
					+ this.getFlowElementData().getMapElemType());
			return;
		}
		g2d.setPaint(flowElementData.getFillColor().darker());
	}

	/**
	 * 图形3D渐变效果
	 * 
	 * @param g2d
	 */
	protected void paint3DFig(Graphics2D g2d) {
		// 背景色不能为空
		if (flowElementData.getFillColor() == null) {
			JecnLogConstants.LOG_BASE_FIGURE.info(ErrorInfoConstant.PARA_OBJ_NULL + "MapElemType:"
					+ this.getFlowElementData().getMapElemType());
			return;
		}
		// 背景色渐变
		g2d.setPaint(new GradientPaint(this.getWidth(), 0.0f, flowElementData.getFillColor().brighter().brighter()
				.brighter(), this.getWidth(), this.getHeight() / 2, flowElementData.getFillColor().darker()));
	}

	/**
	 * 
	 * 普通效果
	 * 
	 * @param g
	 */
	protected void paintGeneralFigure(Graphics2D g2d) {
		paintBackColor(g2d);
		if (flowElementData.getFillType().equals(FillType.none)) {
			// 不存在填充色
			paintNoneFigure(g2d);
		} else {
			// 如果存在填充色
			paintGeneralFigurePart(g2d);
		}
	}

	/**
	 * 普通效果组件 不执行填充
	 * 
	 * @param g2d
	 */
	protected void paintGeneralFigurePart(Graphics2D g2d) {

	}

	/**
	 * 
	 * 阴影效果
	 * 
	 * @param g
	 */
	protected void paintShadowFigure(Graphics2D g2d) {
		if (flowElementData.getFillType().equals(FillType.none)) {
			paintGeneralFigure(g2d);
		} else {
			// 阴影效果
			// paintShadowsOnly(g2d);
			// 顶层填充色
			paintBackColor(g2d);
			// 顶层画图
			paintTop(g2d);
		}
	}

	/**
	 * 添加阴影
	 */
	public void addShadowPanel() {
		if (!this.getFlowElementData().isShadowsFlag()) {
			return;
		}
		if (shadowPanel == null) {
			shadowPanel = new ShadowPanel(this.getFlowElementData());
			this.getParent().add(shadowPanel);
		}
		shadowPanel.setBounds(this.getX() + 3, this.getY() + 3, this.getWidth(), this.getHeight());
	}

	/**
	 * 重绘阴影
	 */
	public void repaintShadowPanel() {
		if (shadowPanel == null) {
			return;
		}
		shadowPanel.validate();
	}

	/**
	 * 取消阴影
	 */
	public void removeRelatedPanel() {
		if (shadowPanel != null) {
			this.getParent().remove(shadowPanel);
			shadowPanel = null;
		}
	}

	/**
	 * 阴影
	 * 
	 * @param g2d
	 */
	protected void paintShadowsOnly(Graphics2D g2d) {

	}

	/**
	 * 
	 * 顶层画图
	 * 
	 * @param g2d
	 * @param shadowCount
	 *            带阴影时缩进值
	 */
	protected void paintTop(Graphics2D g2d) {

	}

	/**
	 * 既有阴影也有3D效果
	 * 
	 * @param g
	 */
	protected void paintShadowAnd3DFigure(Graphics2D g2d) {
		if (flowElementData.getFillType().equals(FillType.none)) {
			JecnLogConstants.LOG_BASE_FIGURE.info(ErrorInfoConstant.PARA_OBJ_NULL + "MapElemType:"
					+ this.getFlowElementData().getMapElemType());

			// 执行无填充
			paintNoneFigure(g2d);
			return;
		}
		// 阴影效果
		// paintShadowsOnly(g2d);
		// addShadowPanel();
		// // 3D背景色
		// paint3DFig(g2d);
		// // 填充背景区域
		// paint3DAndShadowsLow(g2d);
		// // 3D线条颜色
		// paintLineColor(g2d);
		// // 3D效果填充线条
		// paint3DAndShadowsPartLine(g2d);
		// // 顶层 颜色填充,渐变色
		// paintBackColor(g2d);
		// // 顶层 颜色填充,渐变色区域
		// paint3DAndShadowsPartTop(g2d);

		// 3D背景色
		paint3DFig(g2d);
		// 填充背景区域
		paint3DLow(g2d);
		// 3D线条颜色
		paintLineColor(g2d);
		// 3D效果填充线条
		paint3DPartLine(g2d);
		// 顶层 颜色填充,渐变色
		paintBackColor(g2d);
		// 顶层 颜色填充,渐变色区域
		paint3DPartTop(g2d);
	}

	/**
	 * 
	 * 3D背景渐变色
	 * 
	 * @param g
	 */
	protected void paint3DFigure(Graphics2D g2d) {
		if (flowElementData.getFillType().equals(FillType.none)) {
			JecnLogConstants.LOG_BASE_FIGURE.info(ErrorInfoConstant.PARA_OBJ_NULL + "MapElemType:"
					+ this.getFlowElementData().getMapElemType());

			// 执行无填充
			paintNoneFigure(g2d);
			return;
		}
		// 3D背景色
		paint3DFig(g2d);
		// 填充背景区域
		paint3DLow(g2d);
		// 3D线条颜色
		paintLineColor(g2d);
		// 3D效果填充线条
		paint3DPartLine(g2d);
		// 顶层 颜色填充,渐变色
		paintBackColor(g2d);
		// 顶层 颜色填充,渐变色区域
		paint3DPartTop(g2d);
	}

	/**
	 * 
	 * 填充背景区域
	 * 
	 * @param g2d
	 */
	protected void paint3DLow(Graphics2D g2d) {

	}

	/**
	 * 
	 * 填充背景区域
	 * 
	 * @param g2d
	 */
	protected void paint3DAndShadowsLow(Graphics2D g2d) {

	}

	/**
	 * 3D效果填充线条
	 * 
	 * @param g2d
	 */
	protected void paint3DPartLine(Graphics2D g2d) {

	}

	/**
	 * 3D效果填充线条
	 * 
	 * @param g2d
	 */
	protected void paint3DAndShadowsPartLine(Graphics2D g2d) {

	}

	/**
	 * 3D效果填充顶层
	 * 
	 * @param g2d
	 */
	protected void paint3DPartTop(Graphics2D g2d) {

	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	protected void paint3DAndShadowsPartTop(Graphics2D g2d) {

	}

	protected void paintNoneFigure(Graphics2D g2d) {

	}

	/**
	 * true 是英文和数字，false 是汉字
	 * 
	 * @param c
	 * @return
	 */
	private boolean isCnorEn(char c) {
		if (c >= 0x0391 && c <= 0xFFE5) {
			return false;
		}
		return true;
	}

	/**
	 * 设置字的位置
	 * 
	 * @param g
	 * @param type
	 */
	protected void setFontPosition(Graphics2D g, int type) {
		String text = flowElementData.getFigureText();
		if (DrawCommon.isNullOrEmtryTrim(text) || g == null) {
			return;
		}
		g.setFont(this.getCurScaleFont());
		int width = this.getWidth();
		if (this instanceof ModelFigure) {
			ModelFigure modelFigure = (ModelFigure) this;
			width = modelFigure.getZoomDividingX();
		}
		// 字体显示位置点
		drawNoUprightSort(g, text, type, this.getCurScaleFont(), width);

	}

	/**
	 * 更改字体显示样式
	 * 
	 * @param g
	 *            画笔
	 * @param text
	 *            文本内容
	 * @param font
	 *            字体
	 * @return
	 */
	public List<String> changeFontType(Graphics2D g, String text, Font font, int width) {
		List<String> strList = new ArrayList<String>();
		// 获取panel 的宽度
		String[] splitStrings = text.split("\n");

		int space = 5;
		for (String strSplit : splitStrings) {
			// 字符串的宽度
			int strWidth = g.getFontMetrics(font).stringWidth(strSplit);
			if (strWidth + space < width) {
				strList.add(strSplit);
				continue;
			}
			int rows = 0;
			for (int i = 0; i < strSplit.length(); i++) {
				String strSub = strSplit.substring(rows, i + 1);
				strWidth = g.getFontMetrics(font).stringWidth(strSub);
				if (i == strSplit.length() - 1) {
					if (strWidth + space > width) {
						if (strSub.length() > 1) {
							strList.add(strSub.substring(0, strSub.length() - 1));
							strList.add(strSub.substring(strSub.length() - 1, strSub.length()));
						} else if (strSub.length() == 1) {
							strList.add(strSub);
						}
					} else {
						strList.add(strSub);
					}
				} else {
					if (strWidth + space > width) {
						if (strSub.length() > 1) {
							strList.add(strSub.substring(0, strSub.length() - 1));
						}
						rows = i;
					}
				}
			}
		}
		return strList;
	}

	/**
	 * 字体显示位置点
	 * 
	 * @param g
	 *            画笔
	 * @param text
	 *            文本内容
	 * @param type
	 *            0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
	 * @param font
	 *            字体
	 */
	protected void drawNoUprightSort(Graphics2D g, String text, int type, Font font, int width) {
		List<String> list = changeFontType(g, text, font, width);
		int textHeight = g.getFontMetrics(font).getHeight();
		switch (type) {
		case 0:// 左上
			for (int i = 0; i < list.size(); i++) {
				SwingUtilities2.drawString(this, g, list.get(i), 5, textHeight + textHeight * i);
			}
			break;
		case 1:// 正上
			for (int i = 0; i < list.size(); i++) {
				int textLength = g.getFontMetrics(font).stringWidth(list.get(i));
				SwingUtilities2.drawString(this, g, list.get(i), width / 2 - textLength / 2, textHeight + textHeight
						* i);
			}
			break;
		case 2:// 右上
			for (int i = 0; i < list.size(); i++) {
				int textLength = g.getFontMetrics(font).stringWidth(list.get(i));
				SwingUtilities2.drawString(this, g, list.get(i), width - textLength, textHeight + textHeight * i);
			}
			break;
		case 3:// 左中
			for (int i = 0; i < list.size(); i++) {
				SwingUtilities2.drawString(this, g, list.get(i), 5, this.getHeight() / 2 + 3 * textHeight / 4
						- ((list.size() * textHeight) / 2) + textHeight * i);
			}
			break;
		case 4:// 正中
			for (int i = 0; i < list.size(); i++) {
				int textLength = g.getFontMetrics(font).stringWidth(list.get(i));

				SwingUtilities2.drawString(this, g, list.get(i), width / 2 - textLength / 2, this.getHeight() / 2 + 3
						* textHeight / 4 - ((list.size() * textHeight) / 2) + textHeight * i);
			}
			break;
		case 5:// 右中
			for (int i = 0; i < list.size(); i++) {
				int textLength = g.getFontMetrics(font).stringWidth(list.get(i));
				SwingUtilities2.drawString(this, g, list.get(i), width - textLength, this.getHeight() / 2 + 3
						* textHeight / 4 - ((list.size() * textHeight) / 2) + textHeight * i);
			}
			break;
		case 6:// 左下
			for (int i = 0; i < list.size(); i++) {
				SwingUtilities2.drawString(this, g, list.get(i), 5, this.getHeight() + textHeight * i - textHeight
						* list.size());
			}
			break;
		case 7:// 正下
			for (int i = 0; i < list.size(); i++) {
				int textLength = g.getFontMetrics(font).stringWidth(list.get(i));
				SwingUtilities2.drawString(this, g, list.get(i), width / 2 - textLength / 2, this.getHeight()
						+ textHeight * i - textHeight * list.size());
			}
			break;
		case 8:// 右下
			for (int i = 0; i < list.size(); i++) {
				int textLength = g.getFontMetrics(font).stringWidth(list.get(i));
				SwingUtilities2.drawString(this, g, list.get(i), width - textLength, this.getHeight() + textHeight * i
						- textHeight * list.size());
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 画双线
	 * 
	 * @param g
	 * @param text
	 * @param type
	 *            0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
	 */
	public void drawUprightSort(Graphics2D g, String text, int type) {
		String[] splitStrings = text.split("\n");
		StringBuffer strBuffer = new StringBuffer();
		for (String strArr : splitStrings) {
			strBuffer.append(strArr);
		}
		String str = strBuffer.toString();
		// 获取单个单个字体长度
		int charWidth = getCharWidth(str, g, type);
		/** 字符串的长度 */
		int textLength = 0;
		if (type != 0 && type != 1 && type != 2) {
			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);
				String singleStr = String.valueOf(c);
				/** 单个字体长度 */
				int textSingleLength = g.getFontMetrics(this.getFont()).stringWidth(singleStr);
				if (isCnorEn(c)) {
					textSingleLength = textSingleLength * 3;
				} else {
					textSingleLength = textSingleLength * 2;
				}
				/** 字体相对位置 */
				textLength = textLength + textSingleLength;
			}
		}
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			String singleStr = String.valueOf(c);
			/** 单个字体长度 */
			int textSingleLength = g.getFontMetrics(this.getFont()).stringWidth(singleStr);
			if (isCnorEn(c)) {
				textSingleLength = textSingleLength * 3;
			} else {
				textSingleLength = textSingleLength * 2;
			}
			/** 字体相对位置 */
			count = count + textSingleLength;
			// getFontLocation(g);
			if (type == 0) {
				/** 单个字体长度 */
				SwingUtilities2.drawString(this, g, singleStr, 3, count);
			} else if (type == 1) {
				SwingUtilities2.drawString(this, g, singleStr, (this.getWidth() - charWidth) / 2, count);
			} else if (type == 2) {
				SwingUtilities2.drawString(this, g, singleStr, this.getWidth() - charWidth, count);
			} else if (type == 3) {
				SwingUtilities2.drawString(this, g, singleStr, 3, this.getHeight() / 2 - textLength / 2 + count);
			} else if (type == 4) {
				SwingUtilities2.drawString(this, g, singleStr, this.getWidth() / 2 - charWidth / 2, this.getHeight()
						/ 2 - textLength / 2 + count);
			} else if (type == 5) {
				SwingUtilities2.drawString(this, g, singleStr, this.getWidth() - charWidth, this.getHeight() / 2
						- textLength / 2 + count);
			} else if (type == 6) {
				SwingUtilities2.drawString(this, g, singleStr, 3, this.getHeight() - textLength + count);
			} else if (type == 7) {
				SwingUtilities2.drawString(this, g, String.valueOf(text.charAt(i)),
						this.getWidth() / 2 - charWidth / 2, this.getHeight() - textLength + count);
			} else if (type == 8) {
				SwingUtilities2.drawString(this, g, String.valueOf(text.charAt(i)), this.getWidth() - charWidth, this
						.getHeight()
						- textLength + count);
			}
		}
	}

	/**
	 * 
	 * 
	 * @param str
	 *            图形名称
	 * @param g
	 *            画笔
	 * @param type
	 *            0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
	 * @return
	 */
	public int getCharWidth(String str, Graphics g, int type) {
		int charWidth = 0;
		int value = 0;
		switch (type) {
		case 0:
			value = -1;
			break;
		case 1:
			value = 0;
			break;
		case 2:
			value = 3;
			break;
		case 3:
			value = -1;
			break;
		case 4:
			value = 0;
			break;
		case 5:
			value = 3;
			break;
		case 6:
			value = -1;
			break;
		case 7:
			value = 0;
			break;
		case 8:
			value = 3;
			break;
		default:
			break;
		}
		if (value == -1) {
			return charWidth;
		}
		if (this.isChinaChar(str)) {
			charWidth = g.getFontMetrics(this.getFont()).charWidth('哈') + value;
		} else {
			charWidth = g.getFontMetrics(this.getFont()).charWidth('a') + value;
		}
		return charWidth;
	}

	/**
	 * 判断字符串有没有汉字
	 * 
	 * @param s
	 */
	private boolean isChinaChar(String str) {
		char[] chars = str.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			byte[] bytes = ("" + chars[i]).getBytes();
			if (bytes.length == 2) {
				int[] ints = new int[2];
				ints[0] = bytes[0] & 0xff;
				ints[1] = bytes[1] & 0xff;
				if (ints[0] >= 0x81 && ints[0] <= 0xFE && ints[1] >= 0x40 && ints[1] <= 0xFE) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 根据图形克隆数据获取当前放大缩小倍数下图形数据
	 * 
	 * @return
	 */
	public JecnFigureDataCloneable getZoomJecnFigureDataCloneable() {
		// 获取当前放大缩小倍数下图形位置点和坐标点、编辑点
		return this.getFlowElementData().getFigureDataCloneable().getZoomFigureDataProcess(
				JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
	}

	/**
	 * 
	 * 获取图形的克隆数据
	 * 
	 * @return JecnFigureDataCloneable
	 */
	public JecnFigureDataCloneable getJecnFigureDataCloneable() {
		JecnFigureDataCloneable figureCloneble = this.getFlowElementData().getFigureDataCloneable();
		// // 原始状态下图形数据
		return figureCloneble;
	}

	/**
	 * 编辑图形编辑点和位置点
	 * 
	 */
	protected void editConnectPort() {
		// 获取100%状态下的数据
		JecnFigureDataCloneable figureCloneble = getZoomJecnFigureDataCloneable();
		// 添加图形4个编辑点
		JecnPaintFigureUnit.addConnectPort(figureCloneble, this);
		// 添加图形8个方向点
		JecnPaintFigureUnit.addResetPort(figureCloneble, this);
	}

	/**
	 * 重新父类 添加、移动组件并调整其大小。由 x 和 y 指定左上角的新位置，由 width 和 height 指定新的大小
	 * 
	 * @param int x
	 * @param int y
	 * @param int width
	 * @param int height
	 */
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		this.editConnectPort();

		if (shadowPanel != null) {
			shadowPanel.setBounds(this.getX() + 3, this.getY() + 3, this.getWidth(), this.getHeight());
		}
	}

	/**
	 * 
	 * 
	 * 根据设置图形大小和位置点
	 * 
	 * @param location
	 *            Point 位置点
	 */
	public void setSizeAndLocation(Point location) {
		if (location == null) {
			return;
		}
		// 设置属性值
		this.getFlowElementData().getFigureDataCloneable().reSetAttributes(location,
				new Dimension(getFlowElementData().getFigureSizeX(), getFlowElementData().getFigureSizeY()),
				JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());

		// 设置大小
		this.reSetFigureSize();
	}

	/**
	 * 
	 * 
	 * 设置图形大小
	 * 
	 */
	public void reSetFigureSize() {
		// 放大缩小后的图形大小和位置点
		JecnFigureDataCloneable currFigureDataCloneable = this.getFlowElementData().getFigureDataCloneable()
				.getZoomFigureDataProcess(JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
		// 设置图形属性
		this.setBounds(currFigureDataCloneable.getX(), currFigureDataCloneable.getY(), currFigureDataCloneable
				.getWidth(), currFigureDataCloneable.getHeight());
	}

	public JecnFigureData getFlowElementData() {
		return (JecnFigureData) flowElementData;
	}

	public JecnEditPortPanel getEditLeftPortPanel() {
		return editLeftPortPanel;
	}

	public JecnEditPortPanel getEditTopPortPanel() {
		return editTopPortPanel;
	}

	public JecnEditPortPanel getEditRightPortPanel() {
		return editRightPortPanel;
	}

	public JecnEditPortPanel getEditBottomPortPanel() {
		return editBottomPortPanel;
	}

	public List<JecnFigureRefManhattanLine> getListFigureRefManhattanLine() {
		return listFigureRefManhattanLine;
	}

	/**
	 * 图形中心点X坐标
	 * 
	 * @return
	 */
	public int getCenterPointX() {
		return this.getEditTopPortPanel().getZoomCenterPoint().x;
	}

	/**
	 * 图形中心点Y坐标
	 * 
	 * @return
	 */
	public int getCenterPointY() {
		return this.getEditLeftPortPanel().getZoomCenterPoint().y;
	}

	/**
	 * 设置图形及其相关组件层级
	 * 
	 */
	public void setRefPanelLayer(JecnDrawDesktopPane workflow) {
		// 设置图形编辑点层级
		JecnPaintFigureUnit.addEditPortLayer(workflow, this);
		// 图形8个方向点层级设置
		JecnPaintFigureUnit.addResizeToWorkFlow(workflow, this);
	}

	public JecnFigureResizePanel getResizeTopLeftPanel() {
		return resizeTopLeftPanel;
	}

	public JecnFigureResizePanel getResizeTopCenterPanel() {
		return resizeTopCenterPanel;
	}

	public JecnFigureResizePanel getResizeTopRightPanel() {
		return resizeTopRightPanel;
	}

	public JecnFigureResizePanel getResizeLeftCenterPanel() {
		return resizeLeftCenterPanel;
	}

	public JecnFigureResizePanel getResizeRightCenterPanel() {
		return resizeRightCenterPanel;
	}

	public JecnFigureResizePanel getResizeBottomLeftPanel() {
		return resizeBottomLeftPanel;
	}

	public JecnFigureResizePanel getResizeBottomCenterPanel() {
		return resizeBottomCenterPanel;
	}

	public JecnFigureResizePanel getResizeBottomRightPanel() {
		return resizeBottomRightPanel;
	}

	/**
	 * 获取克隆数据对象
	 * 
	 */
	public JecnBaseFigurePanel clone() {
		JecnBaseFigurePanel elementPanel = (JecnBaseFigurePanel) this.currClone();
		// 把源图形属性赋值给目标图形属性
		setFigureAttributes(this, elementPanel);
		return elementPanel;
	}

	/**
	 * 
	 * 把源图形属性赋值给目标图形属性
	 * 
	 * @param srcFigure
	 *            源属性
	 * @param destFigure
	 *            目标属性
	 */
	public void setFigureAttributes(JecnBaseFigurePanel srcFigure, JecnBaseFigurePanel destFigure) {
		if (srcFigure == null || destFigure == null) {
			return;
		}
		if (srcFigure.getFlowElementData().getFigureDataCloneable() == null) {
			srcFigure.getFlowElementData().setFigureDataCloneable(new JecnFigureDataCloneable());
		}
		// 备份克隆对象JecnFigureDataCloneable
		destFigure.getFlowElementData().setFigureDataCloneable(
				srcFigure.getFlowElementData().getFigureDataCloneable().clone());
	}

	/**
	 * 
	 * 获取当前比例下的图形位置点
	 * 
	 * @return
	 */
	public Point getCurrLocation() {
		return this.getFlowElementData().getFigureDataCloneable().getZoomFigureDataProcess(
				JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale()).getLeftUpXY().getPoint();
	}

	/**
	 * 拖动图形重设图形位置点、大小
	 * 
	 */
	public void moveFigureResetLocation() {
		// 获取当前画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取拖动时生成的虚拟图形
		JecnTempFigureBean tempFigurePanel = this.getTempFigureBean();
		// 获取新位置的图形克隆数据（100%状态下的位置点及大小）
		this.getJecnFigureDataCloneable().moveFigure(
				new Point(tempFigurePanel.getLocalX(), tempFigurePanel.getLocalY()), desktopPane.getWorkflowScale());
		// 设置图形位置点
		this.setBounds(tempFigurePanel.getLocalX(), tempFigurePanel.getLocalY(), tempFigurePanel.getUserWidth(),
				tempFigurePanel.getUserHeight());
	}

	/**
	 * 根据枚举类型获取编辑点Port
	 * 
	 * @param editPointType
	 *            编辑点枚举类型
	 * @param figurePanel
	 *            图形
	 * @return JecnEditPortPanel 编辑点
	 */
	public JecnEditPortPanel getEditPortPanel(EditPointType editPointType, JecnBaseFigurePanel figurePanel) {
		switch (editPointType) {
		case left:
			return figurePanel.getEditLeftPortPanel();
		case right:
			return figurePanel.getEditRightPortPanel();
		case top:
			return figurePanel.getEditTopPortPanel();
		case bottom:
			return figurePanel.getEditBottomPortPanel();
		}
		return null;
	}

	public JecnBaseFigurePanel getRectanflePanel() {
		JecnBaseFigurePanel figurePanelClone = this.clone();

		// 普通状态
		figurePanelClone.getFlowElementData().setGeoEnum(GeoEnum.general);
		figurePanelClone.getFlowElementData().setFillType(FillType.none);

		return figurePanelClone;
	}

	/**
	 * 
	 * 设计器专用：连接组件的容器
	 * 
	 * @return JecnDesignerLinkPanel
	 */
	public JecnDesignerLinkPanel getDesLinkPanel() {
		return desLinkPanel;
	}

	/**
	 * 
	 * 设计器专用：连接组件的容器位置在南面
	 * 
	 * @return JecnDesignerLinkPanel
	 */
	public JecnDesignerLinkPanel getDesLinkPanelSouth() {
		return desLinkPanelSouth;
	}

	/**
	 * 
	 * 提示信息事件
	 * 
	 */
	protected void initMouseEvent() {

		this.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent paramMouseEvent) {
				if (!DrawCommon.isInfoFigure(flowElementData.getMapElemType())) {// 是否为设置超链接的图形
					return;
				}
				// 初始化
				setToolTipText(null);
				boolean avtiveNote = DrawCommon.isNullOrEmtryTrim(flowElementData.getAvtivityShow());
				if (avtiveNote) {// 如果存在活动说明或关键说明都为空
					return;
				}

				// 指定取消工具提示的延迟值 设置为一小时 3600秒
				ToolTipManager.sharedInstance().setDismissDelay(3600 * 1000);
				// 组织HTML格式字符串
				StringBuffer sbuf = new StringBuffer();
				sbuf
						.append("<html><div style= 'white-space:normal; display:block;min-width:200px; word-break:break-all'>");

				if (!avtiveNote) {// 说明
					sbuf.append("<br><b>");
					sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("descNote"));
					sbuf.append("</b><br><br>");
					// 说明
					sbuf.append(DrawCommon.getsubstring(flowElementData.getAvtivityShow()));
					sbuf.append("<br><br>");
				}

				sbuf.append("</div><br></html>");

				// 注册要在工具提示中显示的文本。光标处于该组件上时显示该文本。如果 参数 为 null，则关闭此组件的工具提示
				setToolTipText(sbuf.toString());
			}

			public void mouseExited(MouseEvent paramMouseEvent) {
				// 返回初始延迟值
				int intD = ToolTipManager.sharedInstance().getInitialDelay();
				// 取消工具提示的延迟值
				ToolTipManager.sharedInstance().setDismissDelay(intD);
			}
		});
	}

	public ShadowPanel getShadowPanel() {
		return shadowPanel;
	}

	public void setShadowPanel(ShadowPanel shadowPanel) {
		this.shadowPanel = shadowPanel;
	}

	/**
	 * 重构父类层级设置
	 */
	public void reSetFlowElementZorder() {
		super.reSetFlowElementZorder();
		// 没有3D和阴影效果的图形默认执行普通画图
		if (DrawCommon.isNOShadowAnd3D(flowElementData.getMapElemType())) {
			return;
		}
		setFigureShadow();
	}

	/**
	 * 设置元素阴影
	 */
	public void setFigureShadow() {
		if (this.getFlowElementData().isShadowsFlag()) {// 显示阴影
			// 设置层级判断阴影是否存在阴影 (打开面板是，)
			addShadowPanel();
			int order = this.getFlowElementData().getZOrderIndex();
			// 阴影层级在元上下
			JecnDrawMainPanel.getMainPanel().getWorkflow().setLayer(shadowPanel, order - 1);
		} else {
			if (shadowPanel == null) {
				return;
			}
			// 撤销恢复是,重置阴影层级,不存在阴影需删除
			this.removeRelatedPanel();
		}
	}
}
