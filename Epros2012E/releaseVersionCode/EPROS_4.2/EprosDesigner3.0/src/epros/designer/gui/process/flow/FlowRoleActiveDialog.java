package epros.designer.gui.process.flow;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.FlowModeSelectCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProcessUtil;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.event.JecnFileActionProcess;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.toolbar.io.JecnIOUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程图 角色活动
 * 
 * @author 2012-08-17
 * 
 */
public class FlowRoleActiveDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(FlowRoleActiveDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 角色面板 */
	private JecnPanel rolePanel = new JecnPanel();

	/** 流程模板面板 */
	private JecnPanel flowModelPanel = new JecnPanel();

	/** 横纵向面板 */
	private JecnPanel radioButPanel = new JecnPanel();
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();
	/** 角色、活动 总面板 */
	private JecnPanel roleRoundRectPanel = new JecnPanel();
	/** 预估角色 */
	private JLabel roleLabel = new JLabel(JecnProperties.getValue("estimateRole"));
	private JSpinner roleSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));
	/** 预估活动 */
	private JLabel roundRectLabel = new JLabel(JecnProperties.getValue("estimateActive"));
	private JSpinner roundRectSpinner = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));

	// /** 流程模板Lab */
	// private JLabel flowModelLab = new
	// JLabel(JecnProperties.getValue("flowModelC"));
	/** 流程模板Field */
	private FlowModeSelectCommon flowModeSelectCommon;

	private boolean isShowMode = false;

	/** 横向 单选按钮 */
	private JRadioButton horizontalRadBut = new JRadioButton(JecnProperties.getValue("horizontal"));
	private JLabel horizonImg = new JLabel();
	ImageIcon horizontalIm = new ImageIcon("images/icons/horizontal.gif");

	/** 纵向 单选按钮 */
	private JRadioButton verticalRadBut = new JRadioButton(JecnProperties.getValue("vertical"));

	private JLabel verticalImg = new JLabel();

	ImageIcon verticalIm = new ImageIcon("images/icons/vertical.gif");

	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 项的内容大小 */
	Dimension dimension = null;

	private String vhConfigValue = "";

	public FlowRoleActiveDialog() {
		initSize();
		this.setResizable(true);
		this.setModal(true);
		this.setTitle(JecnProperties.getValue("reProcess"));
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
		isShowMode = JecnConfigTool.isShowProcessMode();

		// flowModelField.setEditable(false);

		/** 横向 纵向 按钮实现 单选效果 */
		ButtonGroup bgp = new ButtonGroup();
		bgp.add(horizontalRadBut);
		bgp.add(verticalRadBut);
		// 默认横向按钮选中
		horizontalRadBut.setSelected(true);
		// 显示图片
		horizonImg.setIcon(horizontalIm);
		verticalImg.setIcon(verticalIm);

		horizontalRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		verticalRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		horizontalRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});
		verticalRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		// 布局
		initLayout();
	}

	private void initSize() {
		// 默认横向：0; 1：纵向； 2：横向和纵向;
		vhConfigValue = ConnectionPool.getConfigAciton().selectValueFromCache(ConfigItemPartMapMark.vhFlowSelect);
		if ("2".equals(vhConfigValue)) {
			this.setSize(400, 290);
		} else {
			this.setSize(400, 140);
		}
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 0, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(roleRoundRectPanel, c);
		// rolePanel.setLayout(new GridBagLayout());
		//		
		// =============================角色活动================
		// c = new GridBagConstraints(0, 19, 3, 1, 1.0, 0.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
		// insetsCo, 0, 0);
		// rolePanel.add(roleRoundRectPanel, c);
		roleRoundRectPanel.setLayout(new GridBagLayout());
		// roleRoundRectPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),JecnProperties.getValue("roleActive")));
		// //roleActive
		// 预估角色
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		roleRoundRectPanel.add(roleLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		roleRoundRectPanel.add(roleSpinner, c);
		// 预估活动
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		roleRoundRectPanel.add(roundRectLabel, c);
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		roleRoundRectPanel.add(roundRectSpinner, c);
		// =============================流程模板======================

		if (isShowMode) {
			c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			mainPanel.add(flowModelPanel, c);
			this.flowModeSelectCommon = new FlowModeSelectCommon(1, flowModelPanel, insets, null, this, JecnProperties
					.getValue("flowModelC"), false);
		}

		// =====================单选按钮面板===================================
		vhRadioSelect(c, insets);

		// c = new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
		// 0, 0);
		// mainPanel.add(new JLabel(), c);
		// 按钮面板
		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 根据配置显示 横向纵向流程
	 * 
	 * @param c
	 * @param insetsImg
	 * @param insetss
	 */
	private void vhRadioSelect(GridBagConstraints c, Insets insets) {
		if ("1".equals(vhConfigValue)) {
			verticalRadBut.setSelected(true);
		}
		if (!"2".equals(vhConfigValue)) {
			return;
		}

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(radioButPanel, c);

		radioButPanel.setLayout(new GridBagLayout());
		radioButPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("horVer")));
		// 第一行
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		radioButPanel.add(new JLabel(), c);
		// 横向图标
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		radioButPanel.add(horizonImg, c);
		// 空白
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		radioButPanel.add(new JLabel(), c);
		// 纵向图标
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		radioButPanel.add(verticalImg, c);

		// 空白
		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		radioButPanel.add(new JLabel(), c);
		// 第二行
		// 横向单选按钮
		// 空白
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		radioButPanel.add(new JLabel(), c);
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		radioButPanel.add(horizontalRadBut, c);
		// 空白
		c = new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		radioButPanel.add(new JLabel(), c);
		// 纵向单选按钮
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		radioButPanel.add(verticalRadBut, c);
		c = new GridBagConstraints(4, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		radioButPanel.add(new JLabel(), c);
	}

	private void addVRadio(GridBagConstraints c, Insets insetsImg, Insets insetss, int i) {

	}

	private void addHRadio(GridBagConstraints c, Insets insetsImg, Insets insetss, int i) {

	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelButPerformed() {
		// 关闭窗体
		this.dispose();
	}

	/***************************************************************************
	 * 确定
	 */
	private void okButPerformed() {
		int roles = Integer.valueOf(roleSpinner.getValue().toString()).intValue();
		int actives = Integer.valueOf(roundRectSpinner.getValue().toString()).intValue();

		// 点击确定验证流程面板活动和角色个数是否超出范围
		if (JecnValidateCommon.checkRoleAndActiveLimit(roles, actives)) {
			return;
		}

		boolean isHFlag = horizontalRadBut.isSelected() ? true : false;

		JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		Long flowId = flowMapData.getFlowId();
		JecnIOUtil.removeAllWorkflow();
		if (isShowMode && flowModeSelectCommon.getIdResult() != null) {
			try {
				JecnFlowStructureT jecnFlowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(flowId);
				JecnProcessUtil.resetWorkflowByModeId(flowModeSelectCommon.getIdResult(), jecnFlowStructureT);
			} catch (Exception e) {
				log.error("", e);
			}
		} else {
			JecnFlowMapData jecnFlowMapData = new JecnFlowMapData(MapType.partMap);
			jecnFlowMapData.setRoleCount(roles);
			jecnFlowMapData.setRoundRectCount(actives);
			jecnFlowMapData.setHFlag(isHFlag);
			jecnFlowMapData.setAuthSave(flowMapData.isAuthSave());
			JecnFileActionProcess.getCreateFlow(jecnFlowMapData);
		}
		// 角色移动
		if (JecnSystemStaticData.isShowRoleMove()) {
			getWorkflow().removeJecnRoleMobile();
			// 添加角色移动
			getWorkflow().createAddJecnRoleMobile();
		} else {
			// 移除角色移动
			getWorkflow().removeJecnRoleMobile();
		}
		getWorkflow().getCurrDrawPanelSizeByPanelList();
		// 面板保存标识为true
		getWorkflow().getFlowMapData().setSave(true);
		getWorkflow().updateUI();
		// 关闭窗体
		this.dispose();
	}

	/**
	 * 获取面板
	 * 
	 * @return 画图面板
	 */
	public static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	public JecnPanel getRadioButPanel() {
		return radioButPanel;
	}
}
