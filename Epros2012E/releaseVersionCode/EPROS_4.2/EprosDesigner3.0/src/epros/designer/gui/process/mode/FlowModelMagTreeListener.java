package epros.designer.gui.process.mode;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class FlowModelMagTreeListener extends JecnTreeListener  {
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(FlowModelMagTreeListener.class);
	
	public FlowModelMagTreeListener(JecnHighEfficiencyTree jTree){
		this.jTree = jTree;
	}
	
	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			List<JecnTreeBean> list=null;
			try {
				JecnTreeBean treeBean=node.getJecnTreeBean();
				switch(treeBean.getTreeNodeType()){
				case processModeRoot:
				case processMapMode:
				case processMode:
					list=ConnectionPool.getProcessModeAction().getFlowModels(treeBean.getId());
					break;
				case processModeFileRoot:
				case processModeFileDir:
				case processModeFile:	
					break;
				default:
					break;
				}
				if( list != null && list.size() > 0){
					JecnTreeCommon.expansionTreeNode(jTree, list, node);
				}
				
			} catch (Exception e) {
				log.error("FlowModelMagTreeListener treeExpanded is error", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

}
