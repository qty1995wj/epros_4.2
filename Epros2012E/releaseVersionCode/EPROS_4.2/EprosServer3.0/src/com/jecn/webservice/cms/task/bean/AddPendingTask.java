package com.jecn.webservice.cms.task.bean;

import com.jecn.epros.server.common.JecnCommon;

public class AddPendingTask {
	// 流程实例唯一标识
	private String sn;
	// 流程名称
	private String processName;
	// 节点名称
	private String activityName;
	// 流程实例发起人
	private String originator;
	private String originatorName;
	// 发起人部门ID
	private String originatorDeptId;
	// -发起人部门名称
	private String originatorDeptName;
	// 目标接受者（多人用逗号分割)
	private String sendTo;
	// 转交人
	private String redirectUser;
	// 主题
	private String subject;
	// 处理地址 
	private String url;
	// 流程实例发起时间
	java.util.Calendar processBuildDate;
	// 任务产生时间
	java.util.Calendar createDate;
	// 待办来源系统名称
	private String inComingSysName = "EPROS";
	private String comments;
	private String extendCol1;
	private String extendCol2;
	private String extendCol3;
	private String extendCol4;
	private String extendCol5;

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getOriginator() {
		return originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public String getOriginatorName() {
		return originatorName;
	}

	public void setOriginatorName(String originatorName) {
		this.originatorName = originatorName;
	}

	public String getOriginatorDeptId() {
		return originatorDeptId;
	}

	public void setOriginatorDeptId(String originatorDeptId) {
		this.originatorDeptId = originatorDeptId;
	}

	public String getOriginatorDeptName() {
		return originatorDeptName;
	}

	public void setOriginatorDeptName(String originatorDeptName) {
		this.originatorDeptName = originatorDeptName;
	}

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getRedirectUser() {
		return redirectUser;
	}

	public void setRedirectUser(String redirectUser) {
		this.redirectUser = redirectUser;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public java.util.Calendar getProcessBuildDate() {
		return processBuildDate;
	}

	public void setProcessBuildDate(java.util.Calendar processBuildDate) {
		this.processBuildDate = processBuildDate;
	}

	public java.util.Calendar getCreateDate() {
		return createDate;
	}

	public void setCreateDate(java.util.Calendar createDate) {
		this.createDate = createDate;
	}

	public String getInComingSysName() {
		return inComingSysName;
	}

	public void setInComingSysName(String inComingSysName) {
		this.inComingSysName = inComingSysName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getExtendCol1() {
		return extendCol1;
	}

	public void setExtendCol1(String extendCol1) {
		this.extendCol1 = extendCol1;
	}

	public String getExtendCol2() {
		return extendCol2;
	}

	public void setExtendCol2(String extendCol2) {
		this.extendCol2 = extendCol2;
	}

	public String getExtendCol3() {
		return extendCol3;
	}

	public void setExtendCol3(String extendCol3) {
		this.extendCol3 = extendCol3;
	}

	public String getExtendCol4() {
		return extendCol4;
	}

	public void setExtendCol4(String extendCol4) {
		this.extendCol4 = extendCol4;
	}

	public String getExtendCol5() {
		return extendCol5;
	}

	public void setExtendCol5(String extendCol5) {
		this.extendCol5 = extendCol5;
	}

	public String toString() {
		return "sn=" + sn + "&processName=" + processName + "&activityName=" + activityName + "&originator="
				+ originator + "&originatorName=" + originatorName + "&originatorDeptId=" + originatorDeptId
				+ "&originatorDeptName=" + originatorDeptName + "&sendTo=" + sendTo + "&redirectUser=" + redirectUser
				+ "&subject=" + subject + "&url=" + url + "&processBuildDate="
				+ JecnCommon.getStringbyDateHMS(processBuildDate.getTime()) + "&createDate="
				+ JecnCommon.getStringbyDateHMS(createDate.getTime()) + "&inComingSysName=" + inComingSysName
				+ "&comments=" + comments + "&extendCol1=" + extendCol1 + "&extendCol2=" + extendCol2 + "&extendCol3="
				+ extendCol3 + "&extendCol4=" + extendCol4 + "&extendCol5=" + extendCol5;
	}
}
