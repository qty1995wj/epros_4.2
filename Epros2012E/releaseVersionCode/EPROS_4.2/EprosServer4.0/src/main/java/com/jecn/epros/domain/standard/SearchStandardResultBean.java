package com.jecn.epros.domain.standard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;

public class SearchStandardResultBean {
    private Long id;
    private String name;
    /**
     * 2:流程标准，3：架构标准
     */
    private int type;
    private Long relatedId;
    /**
     * 父节点名称
     */
    private String parentName;

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public LinkResource getLink() {
        LinkResource linkResource = null;
        if (type == 2) {
            linkResource = new LinkResource(relatedId, name, ResourceType.PROCESS, TreeNodeUtils.NodeType.search);
        } else if (type == 3) {
            linkResource = new LinkResource(relatedId, name, ResourceType.PROCESS_MAP,TreeNodeUtils.NodeType.search);
        } else {
            linkResource = new LinkResource(id, name, ResourceType.STANDARD,TreeNodeUtils.NodeType.search);
        }
        return linkResource;
    }

    /**
     * 0是目录,1文件标准,2流程标准,3.流程地图标准,4标准条款,5条款要求
     **/
    @JsonIgnore
    public String getStandTypeStr() {
        return Common.standardTypeVal(type);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
}
