package epros.designer.gui.popedom.organization;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class HighEfficiencyOrgPositionMoveTree extends JecnHighEfficiencyTree {
	private Logger log = Logger.getLogger(HighEfficiencyOrgPositionMoveTree.class);

	public HighEfficiencyOrgPositionMoveTree(List<Long> listIds) {
		super(listIds);
	}
	public HighEfficiencyOrgPositionMoveTree(List<Long> listIds,TreeNodeType nodeType) {
		super(listIds,nodeType);
	}
	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new OrgPositionMoveTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getOrganizationAction().getChildOrgs(
					Long.valueOf(0), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighEfficiencyOrgPositionMoveTree getTreeModel is error！", e);
		}
		// 根节点 "组织"
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.organizationRoot,JecnProperties.getValue("organization"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds(),getNodeType());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
