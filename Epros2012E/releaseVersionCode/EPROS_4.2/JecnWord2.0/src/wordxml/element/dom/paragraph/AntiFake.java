package wordxml.element.dom.paragraph;

import wordxml.element.bean.paragraph.AntiFakeBean;
import wordxml.element.dom.graph.WordGraph;

/**
 * 水印
 */
public class AntiFake extends WordGraph {

	public static int IMAGE = 1;
	public static int TEXT = 2;

	private AntiFakeBean antiFakeBean = null;
	private String value;
	private int type = 0;

	public AntiFake(int type, String value, AntiFakeBean antiFakeBean) {
		this.type = type;
		this.value = value;
		this.antiFakeBean = antiFakeBean;
	}

	public static AntiFake createImageAntiFake(String value, AntiFakeBean antiFakeBean) {
		return new AntiFake(AntiFake.IMAGE, value, antiFakeBean);
	}

	public static AntiFake createTextAntiFake(String value, AntiFakeBean antiFakeBean) {
		return new AntiFake(AntiFake.TEXT, value, antiFakeBean);
	}

	public boolean isImage() {
		if (AntiFake.IMAGE == this.type) {
			return true;
		}
		return false;
	}

	public boolean isText() {
		if (AntiFake.TEXT == this.type) {
			return true;
		}
		return false;
	}
	
	public boolean isNone() {
		return !isImage() && !isText();
	}
	
	//--------------

	public AntiFakeBean getAntiFakeBean() {
		return antiFakeBean;
	}

	public void setAntiFakeBean(AntiFakeBean antiFakeBean) {
		this.antiFakeBean = antiFakeBean;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
}
