package com.jecn.epros.server.download.wordxml;

import wordxml.element.dom.page.Sect;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;

/**
 * 操作说明数据项Bean
 * 
 * @author weidp
 * 
 */
public class ProcessFileItem {

	/** 项标题 */
	private String _itemTitle;
	/** 项所属节 */
	private Sect _belongTo;
	/** 数据Bean */
	private ProcessDownloadBean _dataBean;

	public ProcessFileItem() {
	}

	public ProcessFileItem(String itemTitle, Sect belongTo,
			ProcessDownloadBean dataBean) {
		_itemTitle = itemTitle;
		_belongTo = belongTo;
		_dataBean = dataBean;
	}

	public String getItemTitle() {
		return _itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this._itemTitle = itemTitle;
	}

	public Sect getBelongTo() {
		return _belongTo;
	}

	public void setBelongTo(Sect belongTo) {
		this._belongTo = belongTo;
	}

	public ProcessDownloadBean getDataBean() {
		return _dataBean;
	}

	public void setDataBean(ProcessDownloadBean dataBean) {
		_dataBean = dataBean;
	}

}
