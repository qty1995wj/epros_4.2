package com.jecn.epros.server.action;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.web.context.ServletContextAware;

import com.jecn.epros.server.action.designer.system.impl.TimerEmailSendAction;
import com.jecn.epros.server.action.web.dataImport.match.UserMatchImportAcrion;
import com.jecn.epros.server.action.web.dataImport.sync.excelorDB.UserSyncAction;
import com.jecn.epros.server.action.web.login.ad.addon.JecnAddonAfterItem;
import com.jecn.epros.server.action.web.login.ad.anjianzhiyuan.JecnAnJianZhiYuanAfterItem;
import com.jecn.epros.server.action.web.login.ad.cetc29.JecnCetc29AfterItem;
import com.jecn.epros.server.action.web.login.ad.crrssdxc.JecnCRS_SDXCAfterItem;
import com.jecn.epros.server.action.web.login.ad.csr.JecnCsrAfterItem;
import com.jecn.epros.server.action.web.login.ad.datangyidong.DaTangYiDongAfterItem;
import com.jecn.epros.server.action.web.login.ad.dongh.JecnDonghAfterItem;
import com.jecn.epros.server.action.web.login.ad.guangzhouCsr.JecnGuangZhouJiDianAfterItem;
import com.jecn.epros.server.action.web.login.ad.hisense.JecnHisenseAfterItem;
import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekLoginItem;
import com.jecn.epros.server.action.web.login.ad.jinbo.JinBoAfterItem;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LianHeDianZiAfterItem;
import com.jecn.epros.server.action.web.login.ad.libang.LiBangAfterItem;
import com.jecn.epros.server.action.web.login.ad.mengniu.MengNiuAfterItem;
import com.jecn.epros.server.action.web.login.ad.neusoft.NeusoftAfterItem;
import com.jecn.epros.server.action.web.login.ad.sailun.SaiLunAfterItem;
import com.jecn.epros.server.action.web.login.ad.sanfu.JecnSanfuAfterItem;
import com.jecn.epros.server.action.web.login.ad.shih.JecnShihuaAfterItem;
import com.jecn.epros.server.action.web.login.ad.zhongdiantou.JecnZhongDTAfterItem;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnMaintainNode;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.service.email.IEmailService;
import com.jecn.epros.server.service.popedom.IOrganizationService;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.process.buss.EntryManager;
import com.jecn.epros.server.service.system.IJecnConfigItemService;
import com.jecn.epros.server.service.system.IJecnUserLoginLogService;
import com.jecn.epros.server.service.system.JecnCofigItemSyscDB;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.viewdoc.convert.ViewDocsManager;

/**
 * @author yxw 2012-8-14
 * @description：tomcat启动完成后调用 （spring容器加载完成后）
 */
public class StaredAfterListener implements ApplicationListener, ServletContextAware {
	private Logger log = Logger.getLogger(StaredAfterListener.class);
	private ServletContext servletContext;

	@Override
	public void onApplicationEvent(ApplicationEvent arg0) {
		try {
			IPersonService personService = (IPersonService) ApplicationContextUtil.getContext().getBean(
					"PersonServiceImpl");
			IJecnUserLoginLogService userLoginLogService = (IJecnUserLoginLogService) ApplicationContextUtil
					.getContext().getBean("jecnUserLoginLogServiceImpl");

			personService.moveEditAll();
			// 登录日志异常退出
			userLoginLogService.updateLogiOutTime();
			log.info("启动完成");
		} catch (Exception e) {
			log.error("清理占用资源异常", e);
		}

		// 获取数据源 判断数据库类型
		BasicDataSource dbSource = (BasicDataSource) ApplicationContextUtil.getContext().getBean("dataSource");
		String driverClassName = dbSource.getDriverClassName();
		if ("oracle.jdbc.driver.OracleDriver".equals(driverClassName)) {
			JecnContants.dbType = DBType.ORACLE;
		} else if ("com.mysql.jdbc.Driver".equals(driverClassName)) {
			JecnContants.dbType = DBType.MYSQL;
		} else if ("net.sourceforge.jtds.jdbc.Driver".equals(driverClassName)) {
			JecnContants.dbType = DBType.SQLSERVER;
		} else {
			JecnContants.dbType = DBType.NONE;
		}

		// 设计器用户个数
		// 初始化配置表中数据
		try {
			initConfigItemData();
			onlineView();
			initConfig();
			getDictionarys();
			initLangure();
			initEntryManager();
		} catch (Exception e) {
			log.error("初始化系统配置异常！", e);
			System.exit(0);
		}
		// 流程地图文件是否显示
		boolean isMapFileShow = false;
		for (JecnConfigItemBean configItemBean : JecnContants.processMapItemList) {
			if ("1".equals(configItemBean.getValue())) {
				isMapFileShow = true;
				break;
			}
		}

		servletContext.setAttribute("mapFileShow", isMapFileShow);

		// 数据库JECN_CONFIG_ITEM value/name
		JecnUtil.setConfigItemValues(servletContext);
		// 添加AD验证需要的配置信息
		addADCfg();
		// 添加人员同步定时器
		addImportTimerAction();
		initHistoryEmailToQueue();

	}

	private void initEntryManager() {
		// JecnContants.ENTRY_MANAGER = new EntryManager();
	}

	private void initLangure() {
		IOrganizationService organizationService = (IOrganizationService) ApplicationContextUtil.getContext().getBean(
				"OrganizationServiceImpl");
		try {
			List<JecnMaintainNode> list = organizationService.getLangues();
			for (JecnMaintainNode n : list) {
				JecnContants.LANGUAGES.put(n.getMark(), n);
			}
		} catch (Exception e) {
			log.error("国际化初始失败", e);
		}
	}

	private void onlineView() throws FileNotFoundException {
		if (ViewDocsManager.enabledOnlineView()) {
			// File file = new File(ViewDocsPathManager.OPENOFFICE);
			// if (!file.exists()) {
			// throw new FileNotFoundException("OpenOffice 程序路径不存在于此目录:" +
			// ViewDocsPathManager.OPENOFFICE);
			// }
		}
	}

	/**
	 * 将数据库中的历史未发送的邮件数据初始化到队列中
	 */
	private void initHistoryEmailToQueue() {
		try {
			// 查询数据库
			IEmailService emailService = (IEmailService) ApplicationContextUtil.getContext()
					.getBean("emailServiceImpl");
			TimerEmailSendAction timerEmailSendAction = (TimerEmailSendAction) ApplicationContextUtil.getContext()
					.getBean("timerEmailSendAction");
			List<EmailMessageBean> emailMessageList = emailService.getEmailMessageList();
			timerEmailSendAction.putAll(emailMessageList);
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 
	 * 单点登录分支入口
	 * 
	 */
	private void addADCfg() {
		// JecnContants.otherLoginType = 28;
		switch (JecnContants.otherLoginType) {// 登录类型(0：普通登录(默认))
		case 1:// 1:东航登录类型
			JecnDonghAfterItem.start();
			break;
		case 3:// 3:三幅百货单点登录
			JecnSanfuAfterItem.start();
			break;
		case 4:// 中石化（南京物探）
			JecnShihuaAfterItem.start();
			break;
		case 8:// 华帝
			JecnAddonAfterItem.start();
			break;
		case 9:// 9：中电投
			JecnZhongDTAfterItem.start();
			break;
		case 10:// 10：海信
			JecnHisenseAfterItem.start();
			break;
		case 11:// 29所
			JecnCetc29AfterItem.start();
			break;
		case 12:// 安健致远
			JecnAnJianZhiYuanAfterItem.start();
			break;
		case 13:// 大唐移动
			DaTangYiDongAfterItem.start();
			break;
		case 14:// 联合电子
			LianHeDianZiAfterItem.start();
			break;
		case 15:// 赛轮
			SaiLunAfterItem.start();
			break;
		case 16:// 广州机电
			JecnGuangZhouJiDianAfterItem.start();
			break;
		case 28:// 立邦
			LiBangAfterItem.start();
			break;
		case 30:// 东软医疗
			NeusoftAfterItem.start();
			break;
		case 23:// 株洲中车
			JecnCsrAfterItem.start();
			break;
		case 39:// 株洲时代新材
			JecnCRS_SDXCAfterItem.start();
			break;
		case 37:
			MengNiuAfterItem.start();
			break;
		case 40:// 科大讯飞
			JecnIflytekLoginItem.start();
			break;
		case 44:// 金博
			JinBoAfterItem.start();
			break;
		}
	}

	/**
	 * 获取给定文件全路径 (d2版本获取本地文件路径方法)
	 * 
	 * @return String 文件全路径 和NULL
	 */
	private String getSyncPath() {
		// 路径
		String path = null;

		// 获取路径
		URL url = StaredAfterListener.class.getResource("");
		if (url == null) {
			return path;
		}

		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			path = url.getPath().replaceAll("%20", " ");
		}
		if (path != null) {
			path = path.substring(0, path.lastIndexOf("server/jecn/EprosServer3.0")) + "data/";
		}
		return path;
	}

	/**
	 * 
	 * 初始化配置表中数据
	 * 
	 * @throws Exception
	 * 
	 */
	private void initConfigItemData() throws Exception {
		// 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
		IJecnConfigItemService configItemService = (IJecnConfigItemService) ApplicationContextUtil.getContext()
				.getBean("configServiceImpl");
		// 配置初始化
		getConfigItem(configItemService);

		// 文件保存路径前缀/文件保存方式
		List<JecnConfigItemBean> otherList = configItemService.select(JecnConfigContents.TYPE_BIG_ITEM_OTHER);
		for (JecnConfigItemBean itemBean : otherList) {
			JecnCofigItemSyscDB.startInitConfigItemData(itemBean);
		}

		// ** 文件存储的位置 */
		if (JecnContants.versionType == 1) {// D2版本
			String jecnPath = this.getSyncPath();
			if (jecnPath != null) {
				JecnContants.jecn_path = jecnPath;
			}
		}

		// 初始化本地文件目录
		JecnFinal.startService();

		// 制度任务默认初次发布版本号
		JecnConfigItemBean pubVersionBean = configItemService.selectConfigItemBean(
				JecnConfigContents.TYPE_BIG_ITEM_ROLE, ConfigItemPartMapMark.basicRuleAutoPubVersion.toString());
		JecnCofigItemSyscDB.startInitConfigItemData(pubVersionBean);

		// 合理化建议
		List<JecnConfigItemBean> rationalizationProposalBeanList = configItemService.getRationalizationProposal();
		for (JecnConfigItemBean itemBean : rationalizationProposalBeanList) {
			JecnCofigItemSyscDB.startInitConfigItemData(itemBean);
		}
		// ***************审批环节 按序号排序***************//
		// 流程图
		List<JecnConfigItemBean> partMapShowTaskAppList = configItemService
				.selectTaskAppTable(JecnConfigContents.TYPE_BIG_ITEM_PARTMAP);
		JecnCofigItemSyscDB.startInitTaskAppData(JecnContants.partMapList, partMapShowTaskAppList);
		// 流程地图
		List<JecnConfigItemBean> totalMapShowTaskAppList = configItemService
				.selectTaskAppTable(JecnConfigContents.TYPE_BIG_ITEM_TOTALMAP);
		JecnCofigItemSyscDB.startInitTaskAppData(JecnContants.totalMapList, totalMapShowTaskAppList);
		// 文件
		List<JecnConfigItemBean> fileShowTaskAppList = configItemService
				.selectTaskAppTable(JecnConfigContents.TYPE_BIG_ITEM_FILE);
		JecnCofigItemSyscDB.startInitTaskAppData(JecnContants.fileList, fileShowTaskAppList);
		// 制度
		List<JecnConfigItemBean> roleShowTaskAppList = configItemService
				.selectTaskAppTable(JecnConfigContents.TYPE_BIG_ITEM_ROLE);
		JecnCofigItemSyscDB.startInitTaskAppData(JecnContants.roleList, roleShowTaskAppList);
		// ***************审批环节 按序号排序***************//

		// ***************权限(公开/秘密)***************//
		List<JecnConfigItemBean> pubList = configItemService.selectAllPub();
		for (JecnConfigItemBean itemBean : pubList) {
			JecnCofigItemSyscDB.startInitConfigItemData(itemBean);
		}

		// 记录所有配置
		List<JecnConfigItemBean> allItems = configItemService.selectAllItem();
		for (JecnConfigItemBean jecnConfigItemBean : allItems) {
			JecnContants.configMaps.put(jecnConfigItemBean.getMark(), jecnConfigItemBean);
		}
		// 所有的审批类型任务的审批阶段
		List<JecnConfigItemBean> allApproveList = configItemService
				.selectTaskAppTable(JecnConfigContents.TYPE_BIG_ITEM_PARTMAP);
		JecnCofigItemSyscDB.startInitTaskAppData(JecnContants.allApproveList, allApproveList);

		// ***************权限(公开/秘密)***************//
		log.info("结束读取系统配置表中相关数据......");
	}

	/**
	 * 添加人员同步定时器
	 */
	private void addImportTimerAction() {
		if (!JecnUtil.isPubShow()) {// 无浏览端情况下直接返回
			return;
		}

		// 人员同步类型（1:Excel 2:DB;3:岗位匹配;4:webService）
		int importValue = JecnContants.otherUserSyncType;
		UserSyncAction userSyncAction = null;
		UserMatchImportAcrion matchImportAcrion = null;
		switch (importValue) {
		case 3: // 3:岗位匹配
			matchImportAcrion = (UserMatchImportAcrion) ApplicationContextUtil.getContext()
					.getBean("matchImportAcrion");
			matchImportAcrion.matchInitParams();
			break;
		default:// 默认人员同步
			userSyncAction = (UserSyncAction) ApplicationContextUtil.getContext().getBean("userSyncAction");
			userSyncAction.initParams();
			break;
		}

		AbstractConfigBean absConfigBean = null;
		if (userSyncAction != null) {
			absConfigBean = userSyncAction.getAbstractConfigBean();
		} else if (matchImportAcrion != null) {
			absConfigBean = matchImportAcrion.getAbstractConfigBean();
		}
		if (absConfigBean == null) {
			return;
		}
		if ("1".equals(absConfigBean.getIaAuto())) {// 自动执行定时器
			if (userSyncAction != null) {
				userSyncAction.reStartTime();
			} else if (matchImportAcrion != null) {
				matchImportAcrion.reStartTime();
			}
		}
	}

	/***
	 * 数据字典 初始化
	 */

	private void getDictionarys() {
		IOrganizationService organizationService = (IOrganizationService) ApplicationContextUtil.getContext().getBean(
				"OrganizationServiceImpl");
		try {
			List<JecnDictionary> listDictionary = organizationService.getDictionarys();
			List<JecnDictionary> listSingle = null;
			for (DictionaryEnum s : DictionaryEnum.values()) {
				listSingle = new ArrayList<JecnDictionary>();
				for (JecnDictionary dictionary : listDictionary) {
					if (s.toString().equals(dictionary.getName())) {
						listSingle.add(dictionary);
					}
				}
				JecnContants.dictionaryMap.put(s, listSingle);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("数据字典初始化失败", e);
		}
	}

	/**
	 * 配置初始化
	 * 
	 * @author fuzhh 2013-12-11
	 */
	private void getConfigItem(IJecnConfigItemService configItemService) {
		// 流程配置内的流程其他配置
		List<JecnConfigItemBean> peopleItemList = configItemService.selectConfigItemBeanByType(1, 10);
		JecnContants.peopleItemList = peopleItemList;
		// 获取流程地图的显示和隐藏
		List<JecnConfigItemBean> processMapItemList = configItemService.selectConfigItemBeanByType(0, 4);
		JecnContants.processMapItemList = processMapItemList;

		// 邮件
		JecnContants.emailList = configItemService.selectEmailConfigItem();
		// 系统配置-消息邮件配置
		JecnContants.flowEndDateList = configItemService.selectConfigItemBeanByType(5, 8);
		// 系统-权限
		List<JecnConfigItemBean> sysPubItemList = configItemService
				.selectByTypeBigModel(JecnConfigContents.TYPE_BIG_ITEM_PUB);
		setMapValues(sysPubItemList, JecnContants.sysPubItemMap);
		// 发布配置
		List<JecnConfigItemBean> pubItemList = configItemService
				.selectSmallTypeItem(JecnConfigContents.TYPE_SMALL_PUBLIC_RELATE_FILE);
		setMapValues(pubItemList, JecnContants.pubItemMap);

		// 文件下载次数配置
		List<JecnConfigItemBean> fileDownLoadSetList = configItemService.selectConfigItemBeanByType(
				JecnConfigContents.TYPE_BIG_ITEM_PUB, JecnConfigContents.TYPE_SMALL_DOWNLOAD_COUNT);
		if (fileDownLoadSetList != null && fileDownLoadSetList.size() > 0) {
			for (JecnConfigItemBean configItemBean : fileDownLoadSetList) {
				JecnCofigItemSyscDB.startInitConfigItemData(configItemBean);
			}
		}
		// 流程配置-流程文件定义-角色/职责属性配置：显示岗位名称
		JecnConfigItemBean showPosNameItemBean = configItemService.selectConfigItemBean(
				JecnConfigContents.TYPE_BIG_ITEM_PARTMAP, "showPosNameBox");
		JecnCofigItemSyscDB.startInitConfigItemData(showPosNameItemBean);
		// 流程配置-流程文件定义-活动说明属性配置：显示活动输入、输出
		JecnConfigItemBean showActInOutItemBean = configItemService.selectConfigItemBean(
				JecnConfigContents.TYPE_BIG_ITEM_PARTMAP, "showActInOutBox");
		JecnCofigItemSyscDB.startInitConfigItemData(showActInOutItemBean);
		// 文控信息查阅权限配置：0：不具有;1：具有流程、流程架构、文件、制度查阅权限的人
		JecnConfigItemBean docControlPermissionBean = configItemService.selectConfigItemBean(
				JecnConfigContents.TYPE_BIG_ITEM_PUB, "documentControl");
		JecnCofigItemSyscDB.startInitConfigItemData(docControlPermissionBean);
		// 流程、架构树目录的编号是否显示
		JecnContants.processShowNumber = "1".equals(JecnContants.getValue(ConfigItemPartMapMark.processShowNumber
				.toString())) ? true : false;
	}

	/**
	 * 配置项是否显示
	 * 
	 * @param peopleItemList
	 *            配置项的list
	 * @param processshownumber
	 *            ConfigItemPartMapMark
	 * @return
	 */
	private boolean isShow(List<JecnConfigItemBean> peopleItemList, ConfigItemPartMapMark processshownumber) {
		boolean isShow = false;
		for (JecnConfigItemBean item : peopleItemList) {
			if (item.getMark().toString().equals(processshownumber.toString()) && item.getValue() != null
					&& "1".equals(item.getValue().trim())) {
				isShow = true;
				break;
			}
		}
		return isShow;
	}

	/**
	 * 用map存取配置信息的mark和value
	 * 
	 * @param list
	 *            配置信息集合
	 * @param map
	 *            map记录唯一标识，和显示状态值
	 */
	private void setMapValues(List<JecnConfigItemBean> list, Map<String, String> map) {
		if (list == null || map == null) {
			return;
		}
		for (JecnConfigItemBean itemBean : list) {
			map.put(itemBean.getMark(), itemBean.getValue());
		}
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	/**
	 * 新版本UI 配置项加载
	 */
	private void initConfig() {
		Properties props = new Properties();

		String url = JecnPath.CLASS_PATH + "database.properties";
		log.info("配置文件路径:" + url);

		FileInputStream in = null;
		try {

			in = new FileInputStream(url);
			props.load(in);

			// 新服务网页地址：项目名称
			String newProjectName = props.getProperty("def.server.name");
			if (!JecnCommon.isNullOrEmtryTrim(newProjectName)) {
				JecnContants.NEW_PROJECT_NAME = newProjectName;
			}
			// cookin 域
			String cookieDomain = props.getProperty("cookieDomain");
			if (!JecnCommon.isNullOrEmtryTrim(cookieDomain)) {
				JecnContants.COOKIE_DOMAIN = cookieDomain;
			}
			// 新服务地址
			String newAppContext = props.getProperty("newAppContext");
			if (!JecnCommon.isNullOrEmtryTrim(newAppContext)) {
				JecnContants.NEW_APP_CONTEXT = newAppContext;
			}

			// app
			String newAppContextApp = props.getProperty("newAppContextApp");
			if (!JecnCommon.isNullOrEmtryTrim(newAppContextApp)) {
				JecnContants.NEW_APP_CONTEXT_APP = newAppContextApp;
			}

			// 新服务后台服务地址
			String newAppServerAddress = props.getProperty("newAppServerAddress");
			if (!JecnCommon.isNullOrEmtryTrim(newAppServerAddress)) {
				JecnContants.NEW_APP_SERVER_ADDRESS = newAppServerAddress;
			}

		} catch (Exception e) {
			log.error("读取新服务 COOKIE_DOMAIN 获取失败", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("关闭流错误！", e1);
				}
			}
		}
	}
}
