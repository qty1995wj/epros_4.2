package com.jecn.epros.server.emailnew;

import java.io.File;

public class EmailAttachment {

	/** 邮件附件 */
	private File attachment;
	/** 附件名称 */
	private String attachmentName;

	public File getAttachment() {
		return attachment;
	}

	public void setAttachment(File attachment) {
		this.attachment = attachment;
	}

	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
}
