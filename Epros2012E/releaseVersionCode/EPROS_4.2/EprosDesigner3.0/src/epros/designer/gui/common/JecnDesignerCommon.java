package epros.designer.gui.common;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;
import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.JecnDesignerFrame;
import epros.designer.gui.autoNum.AutoNumSelectDialog;
import epros.designer.gui.autoNum.MengNiuAutoNumSelectDialog;
import epros.designer.gui.autoNum.mulinsen.MulinsenAutoNumSelectDialog;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.header.JecnDesignerModuleShow;
import epros.designer.gui.integration.risk.RiskPropertyDialog;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.gui.popedom.person.PersonChooseDialog;
import epros.designer.gui.popedom.position.PositionChooseDialog;
import epros.designer.gui.popedom.position.PositionChooseDialogByStartId;
import epros.designer.gui.popedom.positiongroup.choose.PosGropChooseDialog;
import epros.designer.gui.process.flowtool.FlowToolChooseDialog;
import epros.designer.gui.process.kpi.KpiTitleAndValues;
import epros.designer.gui.process.kpi.TmpKpiShowValues;
import epros.designer.gui.process.mode.choose.FlowModeChooseDialog;
import epros.designer.gui.rule.RuleChooseDialog;
import epros.designer.gui.rule.ViewRuleInfoDialog;
import epros.designer.gui.standard.ShowRefStanClauseDialog;
import epros.designer.gui.standard.ShowStanClauseRequireDialog;
import epros.designer.gui.system.listener.SelectChangeEvent;
import epros.designer.gui.system.listener.SelectChangeListener;
import epros.designer.gui.task.buss.TaskApproveConstant;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.service.process.JecnProcess;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnFigureInoutSampleT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.gui.designer.JecnDesignerLinkPanel;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitlePanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public class JecnDesignerCommon {
	private static Logger log = Logger.getLogger(JecnDesignerCommon.class);
	/** 设计器配置文件 */
	private final static String DESIGNER_PROPERTIES_URL = "resources/fileProperties/CommonConfig.properties";

	/**
	 * @author yxw 2012-6-21
	 * @description:给查阅权限TextArea赋值
	 * @param ta
	 * @return
	 */
	public static String getLook(List<JecnTreeBean> list, JTextArea ta) {
		if (list != null && list.size() > 0) {
			StringBuffer sbIds = new StringBuffer();
			StringBuffer sbNames = new StringBuffer();
			for (JecnTreeBean o : list) {
				sbIds.append(o.getId() + ",");
				sbNames.append(o.getName() + "/");
			}
			String ids = sbIds.substring(0, sbIds.length() - 1);
			ta.setText(sbNames.substring(0, sbNames.length() - 1));
			return ids;
		}
		ta.setText("");
		return "";
	}

	/**
	 * @author yxw 2012-8-13
	 * @description:把权限集合转换成ids
	 * @param list
	 * @return
	 */
	public static String getLook(List<JecnTreeBean> list) {
		if (list != null && list.size() > 0) {
			StringBuffer sbIds = new StringBuffer();
			StringBuffer sbNames = new StringBuffer();
			for (JecnTreeBean o : list) {
				sbIds.append(o.getId() + ",");
				sbNames.append(o.getName() + "/");
			}
			String ids = sbIds.substring(0, sbIds.length() - 1);
			return ids;
		}
		return "";
	}

	/**
	 * @description:给责任部门JTextField赋值
	 * @param ta
	 * @return
	 */
	public static Long getFieldLook(List<JecnTreeBean> list, JTextField ta) {
		if (list != null && list.size() > 0) {
			JecnTreeBean jecnTreeBean = list.get(0);
			ta.setText(jecnTreeBean.getName());
			return jecnTreeBean.getId();

		} else {
			ta.setText("");
		}

		return null;
	}

	/**
	 * @author yxw 2012-6-27
	 * @description:设置流程模板
	 * @param list
	 * @param textField
	 * @return
	 */
	public static Long setFlowMode(JTextField textField, int flagInt, List<JecnTreeBean> list) {
		FlowModeChooseDialog d = null;
		if (flagInt == 0) { // 流程地图
			d = new FlowModeChooseDialog(list, 10, 0);
		} else if (flagInt == 1) {// 流程图
			d = new FlowModeChooseDialog(list, 11, 1);
		}

		d.setSelectMutil(false);
		d.setVisible(true);
		if (d.isOperation()) {
			if (list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				textField.setText(jecnTreeBean.getName());
				return jecnTreeBean.getId();
			} else {
				list.clear();
				textField.setText("");
			}

		}
		if (list != null && list.size() > 0) {
			return list.get(0).getId();
		}
		return null;
	}

	/**
	 * @author yxw 2012-6-27
	 * @description:设置责任部门
	 * @param list
	 * @param textField
	 * @param orgIds
	 *            初始责任部门ID
	 * @return
	 */
	public static Long setResponsibleDept(List<JecnTreeBean> list, JSearchTextField textField, JecnDialog jecnDialog,
			Long orgIds) {
		OrgChooseDialog orgChooseDialog = new OrgChooseDialog(list, jecnDialog);
		orgChooseDialog.setSelectMutil(false);
		orgChooseDialog.setVisible(true);
		if (orgChooseDialog.isOperation()) {
			if (list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				textField.setTextJecn(jecnTreeBean.getName());
				return jecnTreeBean.getId();
			} else {
				textField.setTextJecn("");
				return null;
			}

		}
		return orgIds;
	}

	/**
	 * @description:设置责任人
	 * @param list
	 * @param textField
	 * @param attchMentId
	 *            初始责任人ID
	 * @return
	 */
	public static Long setResponsiblePerson(List<JecnTreeBean> list, JSearchTextField textField, JecnDialog jecnDialog,
			Long attchMentId) {
		PersonChooseDialog personChooseDialog = new PersonChooseDialog(list, jecnDialog);
		personChooseDialog.setSelectMutil(false);
		personChooseDialog.setVisible(true);
		if (personChooseDialog.isOperation()) {
			if (list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				textField.setTextJecn(jecnTreeBean.getName());
				return jecnTreeBean.getId();
			} else {
				textField.setTextJecn("");
				return null;
			}
		}
		return attchMentId;
	}

	/**
	 * @description:设置责任人
	 * @param list
	 * @param textField
	 * @param attchMentId
	 *            初始责任人ID
	 * @return
	 */
	public static Long setResponsiblePerson(List<JecnTreeBean> list, JSearchTextField textField, JecnDialog jecnDialog,
			Long attchMentId, List<SelectChangeListener> listeners) {
		PersonChooseDialog personChooseDialog = new PersonChooseDialog(list, jecnDialog);
		personChooseDialog.setSelectMutil(false);
		personChooseDialog.setVisible(true);
		if (personChooseDialog.isOperation()) {
			if (listeners != null && listeners.size() > 0) {
				SelectChangeEvent event = new SelectChangeEvent(list);
				for (SelectChangeListener listener : listeners) {
					listener.actionPerformed(event);
				}
			}
			if (list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				textField.setTextJecn(jecnTreeBean.getName());
				return jecnTreeBean.getId();
			} else {
				textField.setTextJecn("");
				return null;
			}
		}
		return attchMentId;
	}

	/**
	 * @description:设置流程责任人（岗位）
	 * @param list
	 * @param ta
	 * @return
	 */
	public static Long setPositionPeoson(List<JecnTreeBean> list, JTextField textField, JecnDialog jecnDialog) {
		PositionChooseDialog positionChooseDialog = new PositionChooseDialog(list, jecnDialog);
		positionChooseDialog.setVisible(true);
		if (positionChooseDialog.isOperation()) {
			if (list != null && list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				textField.setText(jecnTreeBean.getName());
				return jecnTreeBean.getId();
			} else {
				textField.setText("");
			}
		}

		return null;
	}

	/**
	 * @description:设置流程责任人（岗位）
	 * @param list
	 * @param ta
	 * @param attchMentId
	 *            初始岗位ID
	 * @return
	 */
	public static Long setSinglePositionPeoson(List<JecnTreeBean> list, JSearchTextField textField,
			JecnDialog jecnDialog, Long attchMentId) {
		PositionChooseDialog positionChooseDialog = new PositionChooseDialog(list, jecnDialog);
		positionChooseDialog.setSelectMutil(false);
		positionChooseDialog.setVisible(true);
		if (positionChooseDialog.isOperation()) {
			if (list != null && list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				textField.setTextJecn(jecnTreeBean.getName());
				return jecnTreeBean.getId();
			} else {
				textField.setTextJecn("");
				return null;
			}
		}

		return attchMentId;
	}

	/**
	 * @description:设置文件
	 * @param list
	 * @param ta
	 * @param attchMentId
	 *            初始岗位ID
	 * @return
	 */
	public static Long setSingleFile(List<JecnTreeBean> list, JSearchTextField textField, JecnDialog jecnDialog,
			Long attchMentId) {
		FileChooseDialog fileChooseDialog = new FileChooseDialog(list, jecnDialog);
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			if (list != null && list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				textField.setTextJecn(jecnTreeBean.getName());
				return jecnTreeBean.getId();
			} else {
				textField.setTextJecn("");
				return null;
			}
		}

		return attchMentId;
	}

	/**
	 * @author yxw 2012-6-18
	 * @description:树节点设置组织查阅权限
	 * @param selectNode
	 * @param list
	 * @param type
	 *            0是流程、1是文件、2是标准、3制度
	 */
	// public static void setDeptLook(JecnTreeNode selectNode,
	// List<JecnTreeBean> list, int type) {
	// OrgChooseDialog orgChooseDialog = new OrgChooseDialog(list);
	// orgChooseDialog.setChoosePopemPanelVisible(true);
	// orgChooseDialog.setVisible(true);
	//
	// if (orgChooseDialog.isOperation()) {
	// Vector<Vector<String>> data = ((JecnTableModel) orgChooseDialog
	// .getResultTable().getModel()).getDataVector();
	// if (data.size() > 0) {
	// StringBuffer orgIds = new StringBuffer();
	// for (Vector<String> v : data) {
	// orgIds.append(v.get(0) + ",");
	// }
	// try {
	// String ids = orgIds.substring(0, orgIds.length() - 1);
	// ConnectionPool.getOrganizationAction()
	// .saveOrUpdateOrgAccessPermissions(
	// selectNode.getJecnTreeBean().getId(), type,
	// ids);
	// orgChooseDialog.dispose();
	// } catch (Exception e) {
	// e.printStackTrace();
	// log.error("设置组织查阅权限出错", e);
	// JOptionPane.showMessageDialog(null, JecnProperties
	// .getValue("serverConnException"));
	// }
	// } else {
	// try {
	// ConnectionPool.getOrganizationAction()
	// .saveOrUpdateOrgAccessPermissions(
	// selectNode.getJecnTreeBean().getId(), type,
	// "");
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// }
	// }
	/**
	 * @author yxw 2012-6-27
	 * @description:设置部门查阅权限（textarea）
	 * @param list
	 * @param ta
	 * @return
	 */
	public static String setDeptLook(List<JecnTreeBean> list, JTextArea ta, JecnDialog jecnDialog) {
		OrgChooseDialog orgChooseDialog = new OrgChooseDialog(list);
		orgChooseDialog.setVisible(true);
		if (orgChooseDialog.isOperation()) {

			// 判断是否选择了部门
			if (list != null && list.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : list) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				String ids = sbIds.substring(0, sbIds.length() - 1);
				ta.setText(sbNames.substring(0, sbNames.length() - 1));
				return ids;
			} else {
				ta.setText("");
			}
		}

		return "";
	}

	/**
	 * @description:岗位组选择（textarea）
	 * @param list
	 * @param ta
	 * @return
	 */
	public static String setPositionGroupLook(List<JecnTreeBean> list, JSearchTextField ta, JecnDialog jecnDialog) {
		PosGropChooseDialog posGropChooseDialog = new PosGropChooseDialog(list, jecnDialog);
		posGropChooseDialog.setVisible(true);
		if (posGropChooseDialog.isOperation()) {
			if (list != null && list.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : list) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				String ids = sbIds.substring(0, sbIds.length() - 1);
				ta.setTextJecn(sbNames.substring(0, sbNames.length() - 1));
				return ids;
			} else {
				ta.setTextJecn("");
			}
		}

		return "";
	}

	public static String setFlowToolLook(List<JecnTreeBean> list, JTextArea ta, JecnDialog jecnDialog) {
		FlowToolChooseDialog flowToolChooseDialog = new FlowToolChooseDialog(list, jecnDialog);
		flowToolChooseDialog.setVisible(true);
		if (flowToolChooseDialog.isOperation()) {

			// 判断是否选择了部门
			if (list != null && list.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : list) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				String ids = sbIds.substring(0, sbIds.length() - 1);
				ta.setText(sbNames.substring(0, sbNames.length() - 1));
				return ids;
			} else {
				ta.setText("");
			}
		}

		return "";
	}

	/**
	 * @author yxw 2012-7-16
	 * @description:制度选择
	 * @param list
	 * @param ta
	 * @param deptInRuleIds
	 *            初始值
	 * @return
	 */
	public static String setRuleLook(List<JecnTreeBean> list, JTextArea ta, JecnDialog jecnDialog, int type,
			String deptInRuleIds) {
		RuleChooseDialog ruleChooseDialog = new RuleChooseDialog(list, 1L);
		ruleChooseDialog.setChooseType(type);
		ruleChooseDialog.setVisible(true);
		if (ruleChooseDialog.isOperation()) {
			// 判断是否选择了部门
			if (list != null && list.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : list) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				String ids = sbIds.substring(0, sbIds.length() - 1);
				ta.setText(sbNames.substring(0, sbNames.length() - 1));
				return ids;
			} else {
				ta.setText("");
				return "";
			}
		}

		return deptInRuleIds;
	}

	/**
	 * 
	 * @description:岗位说明书附件选择
	 * @param list
	 *            结果集
	 * @param ta
	 * @param jecnDialog
	 * @param posFileId
	 * @return
	 */
	public static String setFileLook(List<JecnTreeBean> list, JTextArea ta, JecnDialog jecnDialog, String posFileId) {
		FileChooseDialog fileChooseDialog = new FileChooseDialog(list);
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			// 判断是否选择了附件
			if (list != null && list.size() > 0) {
				ta.setText(list.get(0).getName());
				return String.valueOf(list.get(0).getId());
			} else {
				ta.setText("");
				return null;
			}
		}
		return posFileId;
	}

	/**
	 * 
	 * @description:岗位说明书附件选择
	 * @param list
	 *            结果集
	 * @param field
	 * @param jecnDialog
	 * @param fileId
	 * @return
	 */
	public static Long selectFile(List<JecnTreeBean> list, JTextField field, JecnDialog jecnDialog, Long fileId) {
		FileChooseDialog fileChooseDialog = new FileChooseDialog(list);
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			// 判断是否选择了附件
			if (list != null && list.size() > 0) {
				field.setText(list.get(0).getName());
				return list.get(0).getId();
			} else {
				field.setText("");
				return null;
			}
		}
		return fileId;
	}

	/**
	 * 
	 * @author yxw 2012-6-27
	 * @description:设置岗位查阅权限（textarea）
	 * @param list
	 * @param ta
	 * @param higtherLelId
	 *            初始值ID
	 * @return
	 */
	public static String setPositionLook(List<JecnTreeBean> list, JTextArea ta, JecnDialog jecnDialog,
			String higtherLelId) {
		PositionChooseDialog positionChooseDialog = new PositionChooseDialog(list);
		positionChooseDialog.setVisible(true);
		if (positionChooseDialog.isOperation()) {
			if (list != null && list.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : list) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				String ids = sbIds.substring(0, sbIds.length() - 1);
				ta.setText(sbNames.substring(0, sbNames.length() - 1));
				return ids;
			} else {
				ta.setText("");
				return "";
			}
		}

		return higtherLelId;
	}

	public static String setPositionLook(List<JecnTreeBean> list, JTextArea ta, JecnDialog jecnDialog,
			String higtherLelId, Long orgId) {
		PositionChooseDialog positionChooseDialog = new PositionChooseDialogByStartId(list, orgId);
		positionChooseDialog.setVisible(true);
		if (positionChooseDialog.isOperation()) {
			if (list != null && list.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : list) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				String ids = sbIds.substring(0, sbIds.length() - 1);
				ta.setText(sbNames.substring(0, sbNames.length() - 1));
				return ids;
			} else {
				ta.setText("");
				return "";
			}
		}

		return higtherLelId;
	}

	/**
	 * @description:设置岗位查阅权限（JTextField）
	 * @param list
	 * @param ta
	 * @return
	 */
	public static String setPositionLook(List<JecnTreeBean> list, JSearchTextField ta, JecnDialog jecnDialog) {
		PositionChooseDialog positionChooseDialog = new PositionChooseDialog(list, jecnDialog);
		positionChooseDialog.setVisible(true);
		if (positionChooseDialog.isOperation()) {
			if (list != null && list.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : list) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				String ids = sbIds.substring(0, sbIds.length() - 1);
				ta.setTextJecn(sbNames.substring(0, sbNames.length() - 1));
				return ids;
			} else {
				ta.setTextJecn("");
			}
		}

		return "";
	}

	/**
	 * @author yxw 2012-8-14
	 * @description:递归获取制度模板
	 * @param node
	 * @return
	 */
	public static Long getRuleModeId(JecnTreeNode node) {
		if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.ruleRoot) {
			return null;
		} else {
			Long id = node.getJecnTreeBean().getModeId();
			if (id != null) {
				return id;
			} else {
				getRuleModeId((JecnTreeNode) node.getParent());
			}
		}
		return null;

	}

	/**
	 * 这个方法表明是否已经打开，是否可以打开（不包含权限判断）
	 * 
	 * @return true 可以打开 false 不可以打开
	 * @throws Exception
	 */
	public static boolean canOpen(Long id, TreeNodeType type, Component c) throws Exception {
		if (JecnDesignerCommon.isCanOpenDeskTopPane()) {// true：可以多个人访问同一面板
			return true;
		} else {
			boolean b = isOpen(id, type);
			if (!b) {
				JecnOptionPane.showMessageDialog(c, JecnProperties.getValue("opened"));
			}
			return b;
		}

	}

	/**
	 * true:当前面板未被打开
	 * 
	 * @param id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public static boolean isOpen(Long id, TreeNodeType type) throws Exception {
		if ((TreeNodeType.processMap == type || TreeNodeType.processMapRelation == type)
				&& JecnConfigTool.isAdminEditFlowMapNode()) {
			return false;
		}
		boolean b = false;
		switch (type) {
		case process:
		case processMap:
		case processMapRelation:
			b = ConnectionPool.getProcessAction().updateProcessCanOpen(JecnConstants.getUserId(), id);
			break;
		case organization:
			b = ConnectionPool.getOrganizationAction().updateOrgCanOpen(JecnConstants.getUserId(), id);
			break;
		case processMapMode:
			b = true;
			break;
		case processMode:
			b = true;
			break;
		default:
			b = false;
			break;
		}
		return b;
	}

	/**
	 * @author yxw 2012-8-17
	 * @description:元素增加链接图标
	 * @param elementPanel
	 * @param mapType
	 */
	public static void addElemLink(JecnBaseFigurePanel elementPanel, MapType mapType) {

		MapElemType elemType = elementPanel.getFlowElementData().getMapElemType();
		JecnFigureData figureData = elementPanel.getFlowElementData();
		JecnDesignerLinkPanel designerLinkPanel = elementPanel.getDesLinkPanel();
		designerLinkPanel.removeAll();
		elementPanel.getDesLinkPanelSouth().removeAll();

		if (figureData.getDesignerFigureData().getLinkId() != null) {// 设置链接
			if (elemType != MapElemType.IconFigure) {
				seetingIconFigureLocationPanel(designerLinkPanel, elementPanel, mapType);
			}
		}

		if (DrawCommon.isRole(elemType)) {
			JecnRoleData roleData = (JecnRoleData) figureData;
			if (roleData.getFlowStationList() != null && roleData.getFlowStationList().size() > 0) {
				designerLinkPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
				JButton b = new JecnLinkButton(elementPanel, mapType, 1);
				designerLinkPanel.add(b);
			}
		} else if (DrawCommon.isActive(elemType) && !DrawCommon.isImplFigure(elemType)) {
			addActiveLink(designerLinkPanel, figureData, elementPanel, mapType);
		} else if (isOtherAddLinkFigure(elemType)) {
			if (figureData.getListFigureFileTBean().size() > 0) {
				JButton b = new JecnLinkButton(elementPanel, mapType, 5);
				elementPanel.getDesLinkPanelSouth().add(b);
			}
		}
		// 刷新面板布局，呈现面板上新增有图标
		designerLinkPanel.updateUI();
	}

	// 设置链接 获取元素链接面板的位置 信息 0 中上 1右下 默认是 中上
	private static void seetingIconFigureLocationPanel(JecnDesignerLinkPanel designerLinkPanel,
			JecnBaseFigurePanel elementPanel, MapType mapType) {
		// 获取连接对象
		JButton b = new JecnLinkButton(elementPanel, mapType, 0);
		// 获取连接位置
		String locationType = JecnConfigTool.getElementLocation();
		JecnDesignerLinkPanel desLinkPanelSouth = elementPanel.getDesLinkPanelSouth();
		// 1右下
		if ("1".equals(locationType)) {
			desLinkPanelSouth.setLayout(new BorderLayout());
			desLinkPanelSouth.add(b, BorderLayout.EAST);
		} else {
			designerLinkPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
			designerLinkPanel.add(b);
		}

	}

	/**
	 * 活动元素设置连接
	 * 
	 * @param designerLinkPanel
	 * @param figureData
	 * @return
	 */
	private static void addActiveLink(JecnDesignerLinkPanel designerLinkPanel, JecnFigureData figureData,
			JecnBaseFigurePanel elementPanel, MapType mapType) {
		JecnActiveData activeData = (JecnActiveData) figureData;
		List<JecnActivityFileT> listJecnActivityFileT = activeData.getListJecnActivityFileT();
		// 设置面板布局方式
		designerLinkPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		// 是否存在输入文件
		boolean isExistInput = false;
		// 是否存在操作规范文件
		boolean isExistOperation = false;
		// 是否存在输出文件
		boolean isExistoutPut = false;
		// 是否存在控制点
		boolean isExistConPoint = false;
		// 是否存在标准
		boolean isExistStandard = false;

		int linkInputType = 0;
		int linkOutputType = 0;

		boolean useNewInout = JecnConfigTool.useNewInout();
		if (listJecnActivityFileT != null) {
			for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
				if (jecnActivityFileT.getFileSId() == null || jecnActivityFileT.getFileType() == null) {
					continue;
				}
				if (jecnActivityFileT.getFileType().intValue() == 0 && !useNewInout) {
					isExistInput = true;
					linkInputType = 2;
				} else if (jecnActivityFileT.getFileType().intValue() == 1) {
					isExistOperation = true;
				}
			}
		}

		// 输入模板不能为空
		if (activeData.getListFigureInTs() != null && activeData.getListFigureInTs().size() > 0 && useNewInout) {
			for (JecnFigureInoutT jecnFigureInoutT : activeData.getListFigureInTs()) {
				if (jecnFigureInoutT.getListSampleT() != null && !jecnFigureInoutT.getListSampleT().isEmpty()) {
					List<JecnFigureInoutSampleT> listSampleT = jecnFigureInoutT.getListSampleT();
					for (JecnFigureInoutSampleT jecnFigureInoutSampleT : listSampleT) {
						if (jecnFigureInoutSampleT.getType() == 0) {
							if (jecnFigureInoutSampleT.getFileId() != null) {
								isExistInput = true;
								linkInputType = 9;
								break;
							}
						}
					}
				}
				if (!isExistInput) {
					isExistInput = true;
					linkInputType = 2;
				}
			}
		}

		// 输出模板不能为空
		if (activeData.getListFigureOutTs() != null && activeData.getListFigureOutTs().size() > 0 && useNewInout) {
			for (JecnFigureInoutT jecnFigureInoutT : activeData.getListFigureOutTs()) {
				if (jecnFigureInoutT.getListSampleT() != null && !jecnFigureInoutT.getListSampleT().isEmpty()) {
					List<JecnFigureInoutSampleT> listSampleT = jecnFigureInoutT.getListSampleT();
					for (JecnFigureInoutSampleT jecnFigureInoutSampleT : listSampleT) {
						if (jecnFigureInoutSampleT.getType() == 0) {
							if (jecnFigureInoutSampleT.getFileId() != null) {
								// 存在输入或输出
								isExistoutPut = true;
								linkOutputType = 10;
								break;
							}
						}
					}
				}
				if (!isExistoutPut) {
					// 存在输入或输出
					isExistoutPut = true;
					linkOutputType = 3;
				}
			}
		}
		boolean isShowLogo = JecnConfigTool.isShowActiveTextPutLogo();
		boolean isShowText =  JecnConfigTool.isShowInputText();
		if (StringUtils.isNotBlank(activeData.getActivityInput()) && isShowLogo && !useNewInout && isShowText) {
			isExistInput = true;
			linkInputType = 2;
		}
		if (StringUtils.isNotBlank(activeData.getActivityOutput()) && isShowLogo && !useNewInout && isShowText) {
			isExistoutPut = true;
			linkOutputType = 2;
		}
		if (!useNewInout) {
			if (activeData.getListModeFileT() != null && activeData.getListModeFileT().size() > 0) {
				isExistoutPut = true;
				linkOutputType = 3;
			}
		}
		if (activeData.getJecnControlPointTList() != null && activeData.getJecnControlPointTList().size() > 0) {
			isExistConPoint = true;
		}

		if (activeData.getListJecnActiveStandardBeanT().size() > 0) {
			isExistStandard = true;
		}
		// 输入
		if (isExistInput) {
			JButton b = new JecnLinkButton(elementPanel, mapType, linkInputType);

			c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0202(), 0, 0);
			designerLinkPanel.add(b, c);
		} else {
			// 空闲区域
			c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0(), 0, 0);
			designerLinkPanel.add(getJLable(), c);
		}

		// 控制点
		if (isExistConPoint) {
			JButton b = new JecnLinkButton(elementPanel, mapType, 6);

			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0202(), 0, 0);
			designerLinkPanel.add(b, c);
		} else {
			// 空闲区域
			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0(), 0, 0);
			designerLinkPanel.add(getJLable(), c);
		}

		// 空闲区域
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		designerLinkPanel.add(new JLabel(), c);

		if (isExistStandard) {
			JButton b = new JecnLinkButton(elementPanel, mapType, 8);
			c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0202(), 0, 0);
			designerLinkPanel.add(b, c);
		} else {
			// 空闲区域
			c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0(), 0, 0);
			designerLinkPanel.add(getJLable(), c);
		}
		// 输出
		if (isExistoutPut) {
			JButton b = new JecnLinkButton(elementPanel, mapType, linkOutputType);
			c = new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0202(), 0, 0);
			designerLinkPanel.add(b, c);
		} else {
			// 空闲区域
			c = new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
					JecnUIUtil.getInsets0(), 0, 0);
			designerLinkPanel.add(new JLabel(), c);
		}

		// 操作规范
		if (isExistOperation) {
			JButton b = new JecnLinkButton(elementPanel, mapType, 4);
			elementPanel.getDesLinkPanelSouth().add(b);
		}
	}

	/**
	 * 可以添加链接的元素
	 * 
	 * @param elemType
	 * @return
	 */
	private static boolean isOtherAddLinkFigure(MapElemType elemType) {
		switch (elemType) {
		case Oval:// 流程地图：圆形 9
		case Triangle: // 流程地图 三角形 10
		case FunctionField: // 流程地图 矩形功能域 11
		case Pentagon: // 五边形 12
		case RightHexagon: // 箭头六边形 13
		case ModelFigure: // 泳池 14
		case InputHandFigure: // 手动输入 19
		case SystemFigure: // 制度 20
		case IsoscelesTrapezoidFigure: // 等腰梯形
		case FileImage:// 文档
		case MapRhombus:// 菱形
			return true;
		}
		return false;
	}

	/**
	 * 获取空白区域lable大小
	 * 
	 * @param isExistPoint
	 * @param isExistStandard
	 * @return JLabel
	 */
	private static JLabel getJLable() {
		// 空白区域
		JLabel lable = new JLabel();
		lable.setBorder(null);
		lable.setOpaque(false);
		Dimension size = new Dimension(16, 16);
		lable.setPreferredSize(size);
		lable.setMaximumSize(size);
		lable.setMinimumSize(size);

		return lable;
	}

	/**
	 * @author yxw 2012-8-17
	 * @description:得到面板ID
	 * @return
	 */
	public static Long getWorkFlowId() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("pleaseOpenProcess"));
			return -1L;
		} else {
			if (MapType.partMap == workflow.getFlowMapData().getMapType()
					|| MapType.totalMap == workflow.getFlowMapData().getMapType()
					|| MapType.totalMapRelation == workflow.getFlowMapData().getMapType()) {
				return workflow.getFlowMapData().getFlowId();
			}

		}
		return -1L;
	}

	/**
	 * @author yxw 2012-8-17
	 * @description:是不是管理员
	 * @return true 管理员 false 非管理员
	 */
	public static boolean isAdmin() {
		if (JecnConstants.loginBean == null) {
			return false;
		}
		// 是不是管理员
		if (JecnConstants.loginBean.isAdmin()) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为二级管理员
	 * 
	 * @return
	 */
	public static boolean isSecondAdmin() {
		if (JecnConstants.loginBean == null) {
			return false;
		}
		// 是不是管理员
		if (isShowSecondAdmin() && JecnConstants.loginBean.isSecondAdmin()) {
			return true;
		}
		return false;
	}

	/**
	 *管理员权限
	 * 
	 * @return
	 */
	public static boolean isAuthorAdmin() {
		return isAdmin() || isSecondAdmin() ? true : false;
	}

	/**
	 * @author yxw 2012-12-11
	 * @description:判断是否为管理员并提示
	 * @return true管理员 false非管理员
	 */
	public static boolean isAdminTip() {
		if (!isAdmin()) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("toAddSymbol"));
			return false;
		}
		return true;
	}

	/**
	 * 管理员权限判断提示
	 * 
	 * @return
	 */
	public static boolean isAdminAuthTip() {
		if (isShowSecondAdmin()) {
			if (!isAdmin() && !isSecondAdmin()) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("toAddSymbol"));// "只有管理员和二级管理员才可以操作！"
				return false;
			}
		} else {
			return isAdminTip();
		}
		return true;
	}

	/**
	 * true 启用二级管理员
	 * 
	 * @return boolean
	 */
	public static boolean isShowSecondAdmin() {
		String value = ConnectionPool.getConfigAciton().selectValueFromCache(
				ConfigItemPartMapMark.showSecondAdminAccessConfig);
		return "1".equals(value) ? true : false;
	}

	/**
	 * 节点类型装换成面板类型
	 * 
	 * @param treeNodeType
	 * @return
	 */
	private static MapType getMapType(TreeNodeType treeNodeType) {
		switch (treeNodeType) {
		case organization:
			return MapType.orgMap;
		case processMap:
			return MapType.totalMap;
		case process:
			return MapType.partMap;
		default:
			return MapType.none;
		}
	}

	/**
	 * @author yxw 2012-11-15
	 * @description: 通过树节点打开面板
	 * @param node
	 * @return true
	 */
	public static boolean processOpenValidate(JecnTreeNode node) {
		// 验证是否有操作权限
		if (!JecnTreeCommon.isAuthNodeAddTip(node, null)) {
			return false;
		}
		return processOpenValidateCommon(node.getJecnTreeBean().getId(), node.getJecnTreeBean().getTreeNodeType());
	}

	/**
	 * @author yxw 2012-11-15
	 * @description: 通过树节点打开面板 是否有设计权限
	 * @param node
	 * @return true
	 */
	public static boolean processOpenValidateDesignPermissions(JecnTreeNode node) {
		// 验证是否有操作权限
		if (!JecnTreeCommon.isAuthNode(node, null)) {
			return false;
		}
		return true;
	}

	/**
	 * @description: 通过树节点打开面板 是否有查阅权限
	 * @param node
	 * @return true 查阅权限
	 */
	public static boolean processOpenValidateAccess(JecnTreeNode node) {
		// 验证是否查阅权限
		if (JecnTreeCommon.isAccessAuthority(node.getJecnTreeBean().getId(), node.getJecnTreeBean().getTreeNodeType(),
				null)) {
			return true;
		}
		return false;
	}

	/**
	 * @author yxw 2012-11-15
	 * @description: 通过树节点打开面板 被占用
	 * @param node
	 * @return true
	 */
	public static boolean processOpenValidateIsOpen(JecnTreeNode node) {
		return processOpenValidateCommon(node.getJecnTreeBean().getId(), node.getJecnTreeBean().getTreeNodeType());
	}

	/**
	 * 判断面板是否只有编辑权限
	 * 
	 * @param node
	 * @return true：只有编辑权限
	 */
	public static boolean isOnlyReadAuth(Long flowId, TreeNodeType nodeType) {
		// 搜索节点
		JecnTreeNode node;
		try {
			// 架构 集成关系图 根据架构来判断是否有编辑权限
			nodeType = nodeType == TreeNodeType.processMapRelation ? TreeNodeType.processMap : nodeType;
			node = JecnTreeCommon.searchNode(flowId, nodeType, JecnDesignerMainPanel.getDesignerMainPanel().getTree());

			boolean isPub = node.getJecnTreeBean().isPub();

			// true：画图面板编辑按钮可操作
			boolean result = false;
			if (TreeNodeType.process == nodeType) {// 流程图
				if (JecnConfigTool.editAuthProcessRelease() && isPub) {
					result = true;
				}
				if (JecnConfigTool.editAuthProcessNotRelease() && !isPub) {
					result = true;
				}
			} else if (TreeNodeType.processMap == nodeType) {// 流程图
				if (JecnConfigTool.editAuthProcessMapRelease() && isPub) {
					result = true;
				}
				if (JecnConfigTool.editAuthProcessMapNotRelease() && !isPub) {
					result = true;
				}
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @author yxw 2012-11-15
	 * @description: 通过连接打开面板
	 * @param node
	 * @return true
	 */
	public static boolean processOpenValidate(Long id, TreeNodeType treeNodeType) {
		// 验证是否有操作权限
		if (!JecnTreeCommon.isAuth(id, treeNodeType, JecnSystemStaticData.getFrame())) {
			return false;
		}
		return processOpenValidateCommon(id, treeNodeType);
	}

	public static boolean processOpenValidateCommon(Long id, TreeNodeType treeNodeType) {
		if (TreeNodeType.processFile == treeNodeType) {// 流程（文件） 暂时不判断是否打开
			return true;
		}
		// 节点类型装换成面板类型
		MapType maptype = getMapType(treeNodeType);
		if (maptype == MapType.none) {
			return false;
		}

		// 判断是否已打开
		if (JecnProcess.isOpen(id.intValue(), maptype)) {
			return false;
		}
		// try {
		// // 是不是占用
		// if (!JecnDesignerCommon.canOpen(id, treeNodeType, null)) {
		// return false;
		// }
		// // 判断是不是组织节点
		// if (treeNodeType != TreeNodeType.organization &&
		// !JecnDesignerCommon.isCanOpenDeskTopPane()) {// 面板只允许单个人访问
		// // true:任务中
		// boolean isBeInTask = JecnTool.isTaskIng(id, treeNodeType);
		// if (JecnTreeCommon.isDesigner(treeNodeType) && isBeInTask) {//
		// 如果是设计者，并且处于任务中退出
		// JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(),
		// JecnProperties
		// .getValue("isInTask"));
		// return false;
		// }
		// }
		// } catch (Exception e) {
		// log.error("JecnDesignerCommon processOpenValidate is error", e);
		// JecnOptionPane.showMessageDialog(null,
		// JecnProperties.getValue("serverConnException"));
		// }
		return true;
	}

	public static MapType treeNodeTypeToMapType(TreeNodeType treeNodeType) {
		MapType mapType = MapType.none;
		if (treeNodeType == TreeNodeType.processMap) {
			mapType = MapType.totalMap;
		} else if (treeNodeType == TreeNodeType.process) {
			mapType = MapType.partMap;
		}
		return mapType;
	}

	/**
	 * @author yxw 2012-12-11
	 * @description:更改打开面板的显示图标
	 * @param node
	 * @param icon
	 */
	public static void changWorkTabIcon(JecnTreeNode node, String icon) {
		MapType mapType = JecnDesignerCommon.treeNodeTypeToMapType(node.getJecnTreeBean().getTreeNodeType());
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane drawDesktopPane = tabbedTitlePanel.getCurrDesktopPane();
				if (drawDesktopPane.getFlowMapData().getMapType() == mapType
						&& drawDesktopPane.getFlowMapData().getFlowId().intValue() == node.getJecnTreeBean().getId()) {
					try {
						tabbedTitlePanel.setTabIcon((Icon) JecnTreeRenderer.class.getField(icon).get(null));
					} catch (Exception e) {
						log.error("JecnDesignerCommon changWorkTabIcon is error", e);
					}
				}
			}
		}

	}

	/**
	 * @author yxw 2012-12-11
	 * @description:更改打开面板标题
	 * @param node
	 */
	public static void updateWorkTabTitleName(JecnTreeNode node) {
		MapType mapType = JecnDesignerCommon.treeNodeTypeToMapType(node.getJecnTreeBean().getTreeNodeType());
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane drawDesktopPane = tabbedTitlePanel.getCurrDesktopPane();
				if (drawDesktopPane.getFlowMapData().getMapType() == mapType
						&& drawDesktopPane.getFlowMapData().getFlowId().intValue() == node.getJecnTreeBean().getId()
								.intValue()) {
					try {
						tabbedTitlePanel.setTabText(node.getJecnTreeBean().getName());
					} catch (Exception e) {
						log.error("JecnDesignerCommon updateWorkTabTitleName is error", e);
					}
				}
			}
		}

	}

	/**
	 * 点击树节点，切换主面板
	 * 
	 * @param flowId
	 *            Long 流程ID
	 */
	public static void checkFlowTreeTabPanel(Long flowId) {
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane desktopPane = tabbedTitlePanel.getCurrDesktopPane();
				if (desktopPane.getFlowMapData().getMapType() == MapType.partMap
						&& desktopPane.getFlowMapData().getFlowId().intValue() == flowId) {
					try {
						JecnDrawMainPanel.getMainPanel().getTabbedPane().setSelectedComponent(
								desktopPane.getScrollPanle());
						break;
					} catch (Exception e) {
						log.error("JecnDesignerCommon checkFlowTreeTabPanel is error", e);
					}
				}
			}
		}
	}

	/**
	 * 验证当前面板是否打开
	 * 
	 * @param flowId
	 * @return true 存在流程ID对应的面板
	 */
	public static boolean checkWorkIsCurPanel(Long flowId) {
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane desktopPane = tabbedTitlePanel.getCurrDesktopPane();

				if ((desktopPane.getFlowMapData().getMapType() == MapType.partMap || desktopPane.getFlowMapData()
						.getMapType() == MapType.totalMap)
						&& desktopPane.getFlowMapData().getFlowId().intValue() == flowId) {// 流程图或流程地图节点
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 点击的
	 * 
	 * @return
	 */
	public static boolean isOpenWorkFlow(JecnTreeNode treeNode) {
		if (treeNode == null) {
			throw new IllegalArgumentException("JecnDesignerCommon isOpenWorkFlow is error！");
		}
		// 节点ID
		Long flowId = treeNode.getJecnTreeBean().getId();
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || !JecnDesignerCommon.checkWorkIsCurPanel(flowId)) {
			if (treeNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.process)) {
				// "请打开流程图！"
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("pleaseOpenProcess"));
				return true;
			}

		}
		return false;
	}

	/**
	 * 点击的
	 * 
	 * @return
	 */
	public static boolean isOpenWorkFlowMap(JecnTreeNode treeNode) {
		if (treeNode == null) {
			throw new IllegalArgumentException("JecnDesignerCommon isOpenWorkFlow is error！");
		}
		// 节点ID
		Long flowId = treeNode.getJecnTreeBean().getId();
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || !JecnDesignerCommon.checkWorkIsCurPanel(flowId)) {
			if (treeNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.processMap)) {
				// "请打开流程图！"
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("pleaseOpenProcess"));
				return true;
			}

		}
		return false;
	}

	/**
	 * 判断界面是否打开
	 * 
	 * @author fuzhh Aug 24, 2012
	 * @param flowId
	 *            流程ID
	 * @param mapType
	 *            面板类型
	 * @return true 表示打开 ， false 表示未打开
	 */
	public static boolean isOpen(int flowId, MapType mapType) {
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane drawDesktopPane = tabbedTitlePanel.getCurrDesktopPane();
				if (drawDesktopPane.getFlowMapData().getMapType() == mapType
						&& drawDesktopPane.getFlowMapData().getFlowId().intValue() == flowId) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @author yxw 2013-1-6
	 * @description:树节点集合排序,目的是与树节点显示顺序一致
	 * @param list
	 */
	public static void sortJecnTreeBeanList(List<JecnTreeNode> list) {
		if (list == null || list.size() == 0) {
			return;
		}

		Collections.sort(list, new Comparator<JecnTreeNode>() {
			public int compare(JecnTreeNode r1, JecnTreeNode r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("JecnDesignerCommon sortJecnTreeBeanList is error");
				}
				return treeNodeCompare(r1, r2);

			}
		});

	}

	/**
	 * @author yxw 2013-1-14
	 * @description:节点对比规则
	 * @param node1
	 * @param node2
	 * @return
	 */
	public static int treeNodeCompare(JecnTreeNode node1, JecnTreeNode node2) {
		if (node1.getPreviousSibling() == null) {
			return -1;
		} else if (node1.getPreviousSibling() == node2) {
			return 1;
		} else {
			return treeNodeCompare((JecnTreeNode) node1.getPreviousSibling(), node2);
		}
	}

	/**
	 * @author yxw 2013-1-14
	 * @description:节点移动时，判断有没有选择相同名称的节点
	 * @param nodes
	 * @return true:没有 ; false 有相同的
	 */
	public static boolean validateRepeatNodesName(List<JecnTreeNode> nodes) {
		for (JecnTreeNode node1 : nodes) {
			if (node1.getJecnTreeBean().getTreeNodeType() == TreeNodeType.standardClauseRequire) {
				return true;
			}
			for (JecnTreeNode node2 : nodes) {
				if (!node1.equals(node2) && node1.getJecnTreeBean().getName().equals(node2.getJecnTreeBean().getName())) {
					JecnOptionPane
							.showMessageDialog(JecnSystemStaticData.getFrame(), "\""
									+ node1.getJecnTreeBean().getName() + "\""
									+ JecnProperties.getValue("nodeMovenNameRepeat"));
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 制度类型
	 * 
	 * @author fuzhh 2013-12-27
	 * @param ruleType
	 * @return
	 */
	public static String ruleTypeVal(int ruleType) {
		String type = "";
		if (ruleType == 1) {
			// 制度模板
			type = JecnProperties.getValue("ruleModeFile");
		} else if (ruleType == 2) {
			// 制度文件
			type = JecnProperties.getValue("ruleFile");
		}
		return type;
	}

	/**
	 * 制度类型
	 * 
	 * @author fuzhh 2013-12-27
	 * @param ruleType
	 * @return
	 */
	public static String ruleTypeVal(TreeNodeType treeNodeType) {
		String type = "";
		if (TreeNodeType.ruleModeFile == treeNodeType) {
			// 制度模板
			type = JecnProperties.getValue("ruleModeFile");
		} else if (TreeNodeType.ruleFile == treeNodeType) {
			// 制度文件
			type = JecnProperties.getValue("ruleFile");
		}
		return type;
	}

	/**
	 * 标准类型
	 * 
	 * @author fuzhh 2013-12-27
	 * @param standardType
	 * @return
	 */
	public static String standardTypeVal(int standardType) {
		// 标准类型
		String stanType = "";
		if (standardType == 1) {// 0是目录，1文件标准 ，2流程标准 3流程地图标准4标准条款5条款要求
			stanType = JecnProperties.getValue("fileStandar");
		} else if (standardType == 2) {// 流程标准
			stanType = JecnProperties.getValue("flowStandar");
		} else if (standardType == 3) {// 流程地图标准
			stanType = JecnProperties.getValue("flowMapStandar");
		} else if (standardType == 4) {// 标准条款
			stanType = JecnProperties.getValue("standarClause");
		} else if (standardType == 5) {// 条款要求
			stanType = JecnProperties.getValue("stanClauseRequire");
		}
		return stanType;
	}

	/**
	 * 标准类型
	 * 
	 * @author fuzhh 2013-12-27
	 * @param standardType
	 * @return
	 */
	public static String standardTypeVal(TreeNodeType treeNodeType) {
		// 标准类型
		String stanType = "";
		if (TreeNodeType.standard == treeNodeType) {// 0是目录，1文件标准 ，2流程标准
			// 3流程地图标准4标准条款5条款要求
			stanType = JecnProperties.getValue("fileStandar");
		} else if (TreeNodeType.standardProcess == treeNodeType) {// 流程标准
			stanType = JecnProperties.getValue("flowStandar");
		} else if (TreeNodeType.standardProcessMap == treeNodeType) {// 流程地图标准
			stanType = JecnProperties.getValue("flowMapStandar");
		} else if (TreeNodeType.standardClause == treeNodeType) {// 标准条款
			stanType = JecnProperties.getValue("standarClause");
		} else if (TreeNodeType.standardClauseRequire == treeNodeType) {// 条款要求
			stanType = JecnProperties.getValue("stanClauseRequire");
		}
		return stanType;
	}

	/**
	 * 打开文件（权限验证）
	 * 
	 * @author weidp
	 * @param id
	 * @return 0 没有权限 1 文件不存在
	 */
	private static int baseOpenFile(long id) {
		// 权限验证
		boolean result = JecnTreeCommon.isAuth(id, TreeNodeType.file, null);
		if (!result) {
			return 0;
		}
		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getFileAction().openFile(id);
		} catch (Exception e) {
			log.error("JecnDesignerCommon baseOpenFile is error", e);
		}
		if (fileOpenBean != null) {
			JecnUtil.openFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
		} else {
			return 1;
		}
		return 0;
	}

	public static int openStandardContentFile(Long id) {
		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getStandardAction().openFileContent(id);
		} catch (Exception e) {
			log.error("JecnDesignerCommon openStandardContentFile is error", e);
		}
		if (fileOpenBean != null) {
			JecnUtil.openFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
		} else {
			return 1;
		}
		return 0;
	}

	/**
	 * 打开文件，从服务端得到文件
	 * 
	 * @author weidp
	 * @param id
	 *            文件ID
	 * @throws Exception
	 */
	public static void openFile(long id) {
		int result = baseOpenFile(id);
		if (result == 1) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileNotExit"));
		}
	}

	public static void openProcessFile(long id) {
		int result = baseOpenProcessFile(id);
		if (result == 1) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileNotExit"));
		}
	}

	/**
	 * 流程节点，单点流程文件
	 * 
	 * @param id
	 * @return
	 */
	private static int baseOpenProcessFile(long id) {
		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getProcessAction().openFileContent(id);
		} catch (Exception e) {
			log.error("JecnDesignerCommon baseOpenProcessFile is error", e);
		}
		if (fileOpenBean != null) {
			JecnUtil.openFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
		} else {
			return 1;
		}
		return 0;
	}

	/**
	 * 打开文件，从服务端得到文件
	 * 
	 * @author weidp
	 * @param id
	 *            文件ID
	 * @throws Exception
	 */
	public static void openFile(long id, JLabel label) {
		int result = baseOpenFile(id);
		if (result == 1) {
			label.setText(JecnProperties.getValue("fileNotExit"));
		}
	}

	/**
	 * 打开制度文件/制度模板文件
	 * 
	 * @author weidp
	 * @param ruleId
	 * @param treeNodeType
	 *            制度类型
	 * @param resultLabel
	 *            使用Label显示异常信息
	 */
	private static void baseOpenRuleInfoDialog(Long ruleId, TreeNodeType treeNodeType, JLabel resultLabel) {
		try {
			// 权限验证
			boolean flag = JecnTreeCommon.isAuth(ruleId, treeNodeType, null);
			if (!flag) {
				return;
			}
			RuleT rule = ConnectionPool.getRuleAction().getRuleT(ruleId);
			if (rule.getDelState() != null && rule.getDelState().intValue() != 0) {
				/*
				 * 文件处于假删或废止中
				 */
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileDelOrAbolish"));
				return;
			}
			// 制度模板文件
			if (TreeNodeType.ruleModeFile.equals(treeNodeType)) {
				ViewRuleInfoDialog viewRuleInfo = new ViewRuleInfoDialog(ruleId, treeNodeType);
				viewRuleInfo.setVisible(true);
			} else if (TreeNodeType.ruleFile.equals(treeNodeType)) { // 制度文件
				if (rule.getIsFileLocal() != null && rule.getIsFileLocal() == 1) {
					openRuleFile(rule.getFileId());
				} else if (rule.getIsFileLocal() != null && rule.getIsFileLocal() == 0) {
					if (resultLabel != null) {
						openFile(rule.getFileId(), resultLabel);
					} else {
						openFile(rule.getFileId());
					}
				} else {
					JecnUtil.runBroswer(rule.getRuleUrl());
				}
			}
		} catch (Exception e) {
			log.error("JecnDesignerCommon baseOpenRuleInfoDialog is error", e);
		}
	}

	/**
	 * 打开制度文件/制度模板文件
	 * 
	 * @author weidp
	 * @param ruleId
	 *            制度ID
	 * @param treeNodeType
	 *            制度类型
	 */
	public static void openRuleInfoDialog(Long ruleId, TreeNodeType treeNodeType) {
		baseOpenRuleInfoDialog(ruleId, treeNodeType, null);
	}

	/**
	 * 打开制度文件/制度模板文件
	 * 
	 * @author weidp
	 * @param ruleId
	 *            制度ID
	 * @param treeNodeType
	 *            制度类型
	 * @param resultLabel
	 *            使用Label展示结果
	 */
	public static void openRuleInfoDialog(Long ruleId, TreeNodeType treeNodeType, JLabel label) {
		baseOpenRuleInfoDialog(ruleId, treeNodeType, label);
	}

	/**
	 * 打开制度文件/制度模板文件 (查询数据库得到类型)
	 * 
	 * @author weidp
	 * @param ruleId
	 * @param resultLabel
	 *            使用Label显示异常信息
	 */
	private static void baseOpenRuleInfoDialog(Long ruleId, JLabel resultLabel) {
		if (ruleId == null) {
			return;
		}
		try {
			RuleT rule = ConnectionPool.getRuleAction().getRuleT(ruleId);
			TreeNodeType nodeType = null;
			if (rule.getIsDir().intValue() == 1) {
				nodeType = TreeNodeType.ruleModeFile;
			} else if (rule.getIsDir().intValue() == 2) {
				nodeType = TreeNodeType.ruleFile;
			}
			// 结果展示使用Label
			if (resultLabel != null) {
				openRuleInfoDialog(ruleId, nodeType, resultLabel);
			} else {// 弹窗提示
				openRuleInfoDialog(ruleId, nodeType);
			}

		} catch (Exception e) {
			log.error("JecnDesignerCommon baseOpenRuleInfoDialog is error", e);
		}
	}

	/**
	 * 打开制度文件/制度模板文件（使用Label展示结果）
	 * 
	 * @author weidp
	 * @param ruleId
	 * @param resultLabel
	 *            使用Label显示异常信息
	 */
	public static void openRuleInfoDialog(Long ruleId, JLabel resultLabel) {
		baseOpenRuleInfoDialog(ruleId, resultLabel);
	}

	/**
	 * 打开制度文件/制度模板文件
	 * 
	 * @author weidp
	 * @param ruleId
	 * @param type
	 */
	public static void openRuleInfoDialog(Long ruleId) {
		baseOpenRuleInfoDialog(ruleId, null);
	}

	/**
	 * 打开标准
	 * 
	 * @author weidp
	 * @param standardId
	 *            主键
	 * @param relateType
	 *            类型
	 */
	public static void openStandard(Long standardId, int relateType) {
		TreeNodeType nodeType = null;
		switch (relateType) {
		case 1:
			nodeType = TreeNodeType.standard;
			break;
		case 4:
			nodeType = TreeNodeType.standardClause;
			break;
		case 5:
			nodeType = TreeNodeType.standardClauseRequire;
			break;

		}
		if (nodeType == null) {
			return;
		}
		// 权限验证
		boolean result = JecnTreeCommon.isAuth(standardId, nodeType, null);
		if (!result) {
			return;
		}
		if (relateType == 1) {// 文件标准
			StandardBean standardBean = null;
			try {
				standardBean = ConnectionPool.getStandardAction().getStandardBean(standardId);
			} catch (Exception e) {
				log.error("JecnDesignerCommon openStandard is error", e);
			}
			// 打开文件（权限验证）
			JecnDesignerCommon.openFile(standardBean.getRelationId());
		} else if (relateType == 4) {// 标准条款
			ShowRefStanClauseDialog d = new ShowRefStanClauseDialog(standardId);
			if (d.isExists()) {
				d.setVisible(true);
			}
		} else if (relateType == 5) {// 条款要求
			ShowStanClauseRequireDialog d = new ShowStanClauseRequireDialog(standardId);
			if (d.isExists()) {
				d.setVisible(true);
			}
		}
	}

	/**
	 * 打开风险公用方法
	 * 
	 * @author chehuanbo
	 * @since V3.06
	 * */

	public static void openRisk(Long riskId) {

		if (riskId == null) {
			return;
		}

		JecnTreeBean treeBean = new JecnTreeBean();
		treeBean.setId(riskId);

		// 权限验证
		boolean result = JecnTreeCommon.isAuth(riskId, TreeNodeType.riskPoint, null);
		if (!result) {
			return;
		}

		JecnTreeNode node = new JecnTreeNode(treeBean);
		// 风险详细dialog
		RiskPropertyDialog riskPropertyDialog = new RiskPropertyDialog(node, null);

		if (riskPropertyDialog.isExists()) {
			riskPropertyDialog.hiddenComs();
			riskPropertyDialog.setVisible(true);
		}
	}

	/**
	 * 双击打开制度文件
	 */
	/**
	 * 打开文件，从服务端得到文件
	 * 
	 * @author hyl
	 * @param id
	 *            制度文件ID
	 * @throws Exception
	 */
	public static void openRuleFile(long id) {
		int result = baseOpenRuleFile(id);
		if (result == 1) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileNotExit"));
		}
	}

	/**
	 * 打开制度文件(数据库存放的文件,本地上传方式)
	 * 
	 * @author hyl
	 * @param id
	 * @return 0 没有权限 1 文件不存在
	 */
	private static int baseOpenRuleFile(long id) {

		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getRuleAction().g020OpenRuleFile(id);
		} catch (Exception e) {
			log.error("JecnDesignerCommon baseOpenRuleFile is error", e);
		}
		if (fileOpenBean != null) {
			JecnUtil.openFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
		} else {
			return 1;
		}
		return 0;
	}

	/**
	 * 是否显示创建流程标准
	 * 
	 * @Title: isShowFlowStandart
	 * @return boolean
	 * @author ZXH
	 * @date 2015-11-2 下午02:56:30
	 */
	public boolean isShowFlowStandart() {
		String value = ConnectionPool.getConfigAciton().selectValue(6, "");
		return false;
	}

	/**
	 * 是不只显示有设计权限的流程节点
	 * 
	 * @param isRole
	 * @return
	 */
	public static boolean isOnlyViewDesignAuthor() {
		if (JecnDesignerCommon.isAdmin()) {
			return false;
		}
		// 获取配置
		return "1".equals(JecnDesignerCommon.redCommonConfigProperties(CommonConfigEnum.designAccessMenu)) ? true
				: false;
	}

	/**
	 * 日期转换成字符串时间格式 到时分秒
	 * 
	 * @param date
	 *            Date日期
	 * @return String 日期格式
	 */
	public static String getStringbyDate(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	private static Properties redProperties(String url) {
		Properties properties = new Properties();
		if (DrawCommon.isNullOrEmtryTrim(url)) {// 路径不存在
			log.error("JecnDesignerCommon redProperties is error");
			return properties;
		}
		try {
			File file = new File(JecnDesignerCommon.class.getResource("/").toURI().getPath() + url);
			// 文件、制度、流程
			InputStream in = new FileInputStream(file);
			properties.load(in);
			in.close();
		} catch (IOException e) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("JecnDesignerCommon redProperties is error", e);
			return null;
		} catch (URISyntaxException e) {
			log.error("JecnDesignerCommon redProperties is error", e);
		}
		return properties;
	}

	private static OutputStream writeProperties(String url) {
		OutputStream fos = null;
		if (DrawCommon.isNullOrEmtryTrim(url)) {// 路径不存在
			log.error("JecnDesignerCommon writeProperties is error！");
			return fos;
		}
		try {
			File file = new File(JecnDesignerCommon.class.getResource("/").toURI().getPath() + url);
			// 文件、制度、流程
			fos = new FileOutputStream(file);
			return fos;
		} catch (IOException ex) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("JecnDesignerCommon writeProperties is error", ex);
			return null;
		} catch (URISyntaxException e) {
			log.error("JecnDesignerCommon writeProperties is error", e);
		}
		return fos;
	}

	public static String redCommonConfigProperties(CommonConfigEnum key) {
		Properties properties = redProperties(DESIGNER_PROPERTIES_URL);
		if (properties == null || key == null) {
			return null;
		}
		return properties.getProperty(key.toString());
	}

	public enum CommonConfigEnum {
		designAccessMenu, relatedFileDir, domainLogin, isReadIPAddress
	}

	/**
	 * 设计器根据权限展示树节点
	 * 
	 * @param value
	 *            :1 设计者权限展示流程体系；0 展开所有节点
	 * @return true:保存文件成功
	 */
	public static boolean writeDesignerProperties() {
		Properties prop = redProperties(DESIGNER_PROPERTIES_URL);
		if (prop == null) {
			return false;
		}
		OutputStream fos = null;
		try {
			fos = writeProperties(DESIGNER_PROPERTIES_URL);
			String value = prop.getProperty("designAccessMenu");
			if ("1".equals(value)) {
				value = "0";
			} else {
				value = "1";
			}
			prop.setProperty("designAccessMenu", value);
			prop.store(fos, null);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		} finally {
			if (fos == null) {
				return false;
			}
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
				log.error("JecnDesignerCommon writeDesignerProperties is error", e);
			}
			fos = null;
		}
		return true;
	}

	public static List<JecnTreeBean> getDesignerProcessTreeBeanList(Long pid, boolean isOnlyDesigner) throws Exception {
		if (isOnlyDesigner // || JecnDesignerCommon.isSecondAdmin()
		) {// 设计者或二级管理员，通过自定义权限范围过滤数据
			if (JecnConstants.getDesignAuthFlows == null || JecnConstants.getDesignAuthFlows.size() == 0) {
				return new ArrayList<JecnTreeBean>();
			} else {
				List<JecnTreeBean> list = ConnectionPool.getProcessAction().getChildFlows(pid, JecnConstants.projectId,
						false);
				return getDesignerResultList(list);
			}
		} else {// 系统管理员
			return ConnectionPool.getProcessAction().getChildFlows(pid, JecnConstants.projectId, false);
		}
	}

	/**
	 * 角色管理树 展开
	 * 
	 * @param pid
	 * @param isOnlyDesigner
	 * @return
	 * @throws Exception
	 */
	public static List<JecnTreeBean> getRoleManageProcessTreeBeanList(Long pid, boolean isOnlyDesigner)
			throws Exception {
		if (isOnlyDesigner || JecnDesignerCommon.isSecondAdmin()) {// 设计者或二级管理员，通过自定义权限范围过滤数据
			if (JecnConstants.getDesignAuthFlows == null || JecnConstants.getDesignAuthFlows.size() == 0) {
				return new ArrayList<JecnTreeBean>();
			} else {
				List<JecnTreeBean> list = ConnectionPool.getProcessAction().getChildFlows(pid, JecnConstants.projectId,
						false);
				return getDesignerResultList(list);
			}
		} else {// 系统管理员
			return ConnectionPool.getProcessAction().getChildFlows(pid, JecnConstants.projectId, false);
		}
	}

	public static List<JecnTreeBean> getDesignerProcessTreeBeanList(Long pid, boolean isOnlyDesigner,
			boolean isContainsDelNode) throws Exception {

		if (isOnlyDesigner || JecnDesignerCommon.isSecondAdmin()) {// 设计者或二级管理员，通过自定义权限范围过滤数据
			if (JecnConstants.getDesignAuthFlows == null || JecnConstants.getDesignAuthFlows.size() == 0) {
				return new ArrayList<JecnTreeBean>();
			} else {
				List<JecnTreeBean> list = ConnectionPool.getProcessAction().getChildFlows(pid, JecnConstants.projectId,
						isContainsDelNode);
				return getDesignerResultList(list);
			}
		} else {// 系统管理员
			return ConnectionPool.getProcessAction().getChildFlows(pid, JecnConstants.projectId, isContainsDelNode);
		}
	}

	/**
	 * 过滤不能显示
	 * 
	 * @param list
	 * @return
	 */
	public static List<JecnTreeBean> getDesignerResultList(List<JecnTreeBean> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (JecnTreeBean jecnTreeBean : list) {
			for (String t_path : JecnConstants.getDesignAuthFlows) {
				if (t_path.length() >= jecnTreeBean.getT_Path().length()) {
					if (isStartsWith(jecnTreeBean.getT_Path(), t_path)) {
						listResult.add(jecnTreeBean);
						break;
					}
				} else {
					if (isStartsWith(t_path, jecnTreeBean.getT_Path())) {
						listResult.add(jecnTreeBean);
						break;
					}
				}
			}
		}
		return listResult;
	}

	private static boolean isStartsWith(String minStr, String maxStr) {
		return maxStr.startsWith(minStr);
	}

	/**
	 * 获取二级管理员所属子公司ID（组织ID）
	 * 
	 * @return
	 */
	public static Long getSecondAdminOrgId() {
		if (JecnConstants.loginBean.getSetOrg() == null || JecnConstants.loginBean.getSetOrg().isEmpty()) {
			return -2L;
		}
		return JecnConstants.loginBean.getSetOrg().iterator().next();
	}

	public static Long getSecondAdminUserId() {
		return isSecondAdmin() ? JecnConstants.getUserId() : null;
	}

	/**
	 * true：可以多个人访问同一面板
	 * 
	 * @return
	 */
	public static boolean isCanOpenDeskTopPane() {
		String value = ConnectionPool.getConfigAciton().selectValueFromCache(ConfigItemPartMapMark.canOpenDesktopPane);
		return "1".equals(value) ? true : false;
	}

	/**
	 * true：有流程的编辑权限
	 * 
	 * @return
	 */
	public static boolean isAuthSave() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return false;
		}
		return desktopPane.getFlowMapData().isAuthSave() == 1;
	}

	/**
	 * 创建面板数据对象
	 * 
	 * @param mapType
	 * @return
	 */
	public static JecnFlowMapData createFlowMapData(MapType mapType, Long id) {
		JecnFlowMapData mapData = new JecnFlowMapData(mapType);
		mapData.setFlowId(id);
		if (isCanOpenDeskTopPane()) {// 不同用户可以打开同一个面板
			TreeNodeType type = null;
			switch (mapType) {
			case partMap:
				type = TreeNodeType.process;
				break;
			case totalMap:
				type = TreeNodeType.processMap;
				break;
			case orgMap:
				type = TreeNodeType.organization;
				break;
			case totalMapRelation:
				type = TreeNodeType.processMapRelation;
				break;
			}
			if (type != null) {
				try {
					if (JecnDesignerCommon.isOnlyReadAuth(id, type)) {
						return mapData;
					}
					setSaveAuthValue(mapData, type, id);
				} catch (Exception e) {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("validationPanel"));
					return null;
				}
			}
		}
		return mapData;
	}

	/**
	 * 设置保存权限
	 * 
	 * @param mapData
	 * @param type
	 * @param id
	 * @throws Exception
	 */
	public static void setSaveAuthValue(JecnFlowMapData mapData, TreeNodeType type, Long id) throws Exception {
		int status = getSaveAuthStatus(type, id);
		mapData.setAuthSave(status);
		if (status == 4) {
			mapData.setAuthSave(1);
			isOpen(id, type);
		} else if (status == 5) {
			mapData.setOccupierName(getOccupierName(type, id));
		}
	}

	/**
	 * @return 0:无权限；1：可编辑，2：任务中，3 ：只读模式 ； 4：只读模式，可编辑 ；5：占用
	 */
	public static int getSaveAuthStatus(TreeNodeType treeNodeType, Long id) throws Exception {

		treeNodeType = treeNodeType == TreeNodeType.processMapRelation ? TreeNodeType.processMap : treeNodeType;
		JecnTreeNode node = JecnTreeCommon.getTreeNode(JecnTreeCommon.getNodeUniqueIdentifier(id, treeNodeType),
				JecnDesignerMainPanel.getDesignerMainPanel().getTree());
		boolean isDesigner = JecnDesignerCommon.processOpenValidateDesignPermissions(node);

		if (isDesigner) {// 设计权限验证
			// 文件操作状态 1：可编辑，2：任务中，3 ：只读模式 ； 4：只读模式，可编辑 ；5：占用
			if (!JecnConfigTool.isCetc10Login()) {
				if (!JecnDesignerCommon.isFlowAdmin() && JecnTool.isTaskIng(id, treeNodeType)) {
					return 2;
				}
			}
			// 流程被占用
			String occupierName = getOccupierName(treeNodeType, id);
			if (StringUtils.isNotBlank(occupierName)) {
				return 5;
			}
			boolean isPub = node.getJecnTreeBean().isPub();
			// true：画图面板编辑按钮可操作
			if (TreeNodeType.process == treeNodeType) {// 流程图
				if (JecnConfigTool.editAuthProcessRelease() && isPub) {
					return 4;
				}
				if (JecnConfigTool.editAuthProcessNotRelease() && !isPub) {
					return 4;
				}
			} else if (TreeNodeType.processMap == treeNodeType) {// 流程图
				if (JecnConfigTool.editAuthProcessMapRelease() && isPub) {
					return 4;
				}
				if (JecnConfigTool.editAuthProcessMapNotRelease() && !isPub) {
					return 4;
				}
			}
			isOpen(id, treeNodeType);
			return 1;
		}
		if (JecnConfigTool.isShowProcessAccess()
				&& (treeNodeType == TreeNodeType.processMapRelation || treeNodeType == TreeNodeType.processMap || treeNodeType == TreeNodeType.process)) {
			if (JecnDesignerCommon.processOpenValidateAccess(node)) {// 查阅权限
				return 3;
			}

		}
		JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noPopedomOp"));
		return 0;
	}

	public static String getOccupierName(TreeNodeType type, long id) {
		String occupierName = "";
		try {
			switch (type) {
			case process:
			case processMap:
			case processMapRelation:
				occupierName = ConnectionPool.getProcessAction().getFlowOccupierUserNameById(id,
						JecnConstants.loginBean.getJecnUser().getPeopleId());
				break;
			case organization:
				break;
			default:
				break;
			}
		} catch (Exception e) {
			log.error("JecnDesignerCommon getOccupierName is error！", e);
			return null;
		}
		return occupierName;
	}

	public static String getUsedActiveFigureValue() {
		return ConnectionPool.getConfigAciton().selectValueFromCache(ConfigItemPartMapMark.isUsedActiveFigureAR);
	}

	/**
	 * 获取流程图客户元素text
	 * 
	 * @return
	 */
	public static String getCustomValue() {
		List<JecnBaseFlowElementPanel> elementPanels = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();
		for (JecnBaseFlowElementPanel elementPanel : elementPanels) {
			if (elementPanel.getFlowElementData().getMapElemType() == MapElemType.CustomFigure) {
				return elementPanel.getFlowElementData().getFigureText();
			}
		}
		return "";
	}

	/**
	 * 打开集成关系图
	 * 
	 * @param jecnTreeBean
	 * @throws Exception
	 */
	public static void openMapRelatedProcess(Long flowId, TreeNodeType nodeType) throws Exception {
		// 判断是否已打开
		if (JecnProcess.isOpen(flowId.intValue(), MapType.totalMapRelation)) {
			return;
		}

		ProcessOpenMapData processMapData = ConnectionPool.getProcessModeAction().getProcessMapRelatedData(flowId,
				JecnConstants.projectId);
		JecnProcess.openProcessMapRelated(processMapData, false);
		// 更新table标签图标
		JecnDrawMainPanel.getMainPanel().getWorkflow().getTabbedTitlePanel().setTabIcon(
				new ImageIcon("images/treeNodeImages/" + nodeType + ".gif"));
	}

	/**
	 * @description:是不是文件管理员
	 * @return true 管理员 false 非管理员
	 */
	public static boolean isFileAdmin() {
		if (JecnConstants.loginBean == null) {
			return false;
		}
		// 是不是管理员
		if (JecnConstants.loginBean.isDesignFileAdmin()) {
			return true;
		}
		return false;
	}

	/**
	 * @description:是不是流程管理员
	 * @return true 管理员 false 非管理员
	 */
	public static boolean isProcessAdmin() {
		if (JecnConstants.loginBean == null) {
			return false;
		}
		// 是不是管理员
		if (JecnConstants.loginBean.isDesignProcessAdmin()) {
			return true;
		}
		return false;
	}

	/**
	 * @description:是不是制度管理员
	 * @return true 管理员 false 非管理员
	 */
	public static boolean isRuleAdmin() {
		if (JecnConstants.loginBean == null) {
			return false;
		}
		// 是不是管理员
		if (JecnConstants.loginBean.isDesignRuleAdmin()) {
			return true;
		}
		return false;
	}

	/**
	 * 集成关系图
	 * 
	 * @return
	 */
	public static boolean isRelatedTotalMap() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType() == MapType.totalMapRelation;
	}

	public static boolean isProcess(TreeNodeType nodeType) {
		return nodeType == TreeNodeType.process || nodeType == TreeNodeType.processFile;
	}

	public static boolean isProcessFile(TreeNodeType nodeType) {
		return nodeType == TreeNodeType.processFile;
	}

	public static String getSuffixName(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."), fileName.length());
	}

	/**
	 * 获取 支撑文件目录
	 * 
	 * @param selectNode
	 * @return
	 * @throws Exception
	 */
	public static Long getArchitectureFileDirId(JecnTreeNode selectNode) throws Exception {
		Long id = selectNode.getJecnTreeBean().getId();
		Long relatedFileId = null;
		if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process
				|| selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processFile
				|| selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap) {
			// 获取 可维护的文件目录ID
			relatedFileId = ConnectionPool.getProcessAction().getRelatedFileId(id);
			// 1、判断流程对应的树节点是否存在
			if (relatedFileId == null) {
				// 2、不存在创建树节点
				relatedFileId = ConnectionPool.getProcessAction().createRelatedFileDirByPath(id,
						JecnConstants.projectId, JecnConstants.getUserId());
				if (JecnConstants.loginBean.getSetFlow() != null && JecnConstants.loginBean.getSetFlow().size() > 0
						&& JecnConstants.loginBean.getSetFlow().contains(id)) {
					JecnConstants.loginBean.getSetFile().add(relatedFileId);
				}
			}
		} else if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.ruleFile
				|| selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.ruleModeFile) {
			// 获取 可维护的文件目录ID
			relatedFileId = ConnectionPool.getRuleAction().getRelatedFileId(id);
			// 1、判断流程对应的树节点是否存在
			if (relatedFileId == null) {
				// 2、不存在创建树节点
				relatedFileId = ConnectionPool.getRuleAction().createRelatedFileDirByPath(id, JecnConstants.projectId,
						JecnConstants.getUserId());
				if (JecnConstants.loginBean.getSetRule() != null && JecnConstants.loginBean.getSetRule().size() > 0
						&& JecnConstants.loginBean.getSetRule().contains(id)) {
					JecnConstants.loginBean.getSetFile().add(relatedFileId);
				}
			}
		}
		return relatedFileId;
	}

	/**
	 * 文件支撑文件- 文件目录节点权限获取
	 */
	public static void addDesignerFileIdsByArchitectureNode() {
		Map<Integer, Set<Long>> relatedsMap = new HashMap<Integer, Set<Long>>();
		if (JecnConfigTool.isArchitectureUploadFile() && JecnConstants.loginBean.getSetFlow() != null
				&& JecnConstants.loginBean.getSetFlow().size() > 0) {
			relatedsMap.put(1, JecnConstants.loginBean.getSetFlow());
		}

		if (JecnConfigTool.isArchitectureRuleUploadFile() && JecnConstants.loginBean.getSetRule() != null
				&& JecnConstants.loginBean.getSetRule().size() > 0) {
			relatedsMap.put(2, JecnConstants.loginBean.getSetRule());
		}

		if (relatedsMap.size() == 0) {
			return;
		}
		try {
			List<Long> listIds = ConnectionPool.getFileAction().getFileAuthByFlowAutoCreateDir(relatedsMap);
			for (Long id : listIds) {
				JecnConstants.loginBean.getSetFile().add(id);
			}
		} catch (Exception e1) {
			log.error("JecnDesignerCommon getFileAuthByFlowAutoCreateDir is error", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}
	}

	/**
	 * 文件、制度、流程不是假删和废止 type 0是流程，1是文件，2是制度
	 * 
	 * @return
	 * @throws Exception
	 */
	public static boolean isNotAbolishOrNotDelete(Long id, int type) {
		try {
			switch (type) {
			case 0:
				return ConnectionPool.getProcessAction().isNotAbolishOrNotDelete(id);
			case 1:
				return ConnectionPool.getFileAction().isNotAbolishOrNotDelete(id);
			case 2:
				return ConnectionPool.getRuleAction().isNotAbolishOrNotDelete(id);
			default:
				return false;
			}
		} catch (Exception e) {
			log.error("JecnDesignerCommon isNotAbolishOrNotDelete is error", e);
		}
		return false;

	}

	/**
	 * 处于任务中不能修改
	 * 
	 * @param node
	 * @return
	 */
	public static boolean isEditByTaskNode(JecnTreeNode node, boolean fileAdmin) {
		// if (JecnDesignerCommon.isAuthorAdmin() || fileAdmin ||
		// node.getJecnTreeBean().canEdit()) {
		// return true;
		// }
		// JecnFlowMapData flowMapData =
		// JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		// // 不为编辑模式下 或者占用人不为空
		// if (flowMapData.isAuthSave() != 1 || flowMapData.getOccupierName() !=
		// null) {
		// return false;
		// }
		// if (node.getJecnTreeBean().getApproveType() == 0 &&
		// node.getJecnTreeBean().getState() != 0) {
		// return false;
		// } else if (node.getJecnTreeBean().getApproveType() == 2) {
		// return false;
		// }
		// return true;

		if (JecnDesignerCommon.isAuthorAdmin() || fileAdmin || node.getJecnTreeBean().canEdit()) {
			return true;
		}
		if (node.getJecnTreeBean().getApproveType() == 0 && node.getJecnTreeBean().getState() != 0) {
			return false;
		} else if (node.getJecnTreeBean().getApproveType() == 2) {
			return false;
		}
		return true;
	}

	public static boolean isEditByTaskNode(JecnTreeBean jecnTreeBean, JTree tree, boolean fileAdmin) {
		JecnTreeNode node = JecnTreeCommon.getTreeNode(jecnTreeBean, tree);
		if (node == null) {
			return false;
		}
		return isEditByTaskNode(node, fileAdmin);
	}

	public static boolean isEditByTaskNode(Long id, TreeNodeType type, JTree tree, boolean fileAdmin) {
		JecnTreeNode node = JecnTreeCommon.getTreeNode(JecnTreeCommon.getNodeUniqueIdentifier(id, type), tree);
		if (node == null) {
			return false;
		}
		return isEditByTaskNode(node, fileAdmin);
	}

	// /**
	// * 标准自动编号获取
	// *
	// * @param codeTempData
	// * @return
	// */
	// public static String getStandardizedAutoCode(Long nodeId, int nodeType) {
	// if (nodeId == null || nodeId == null) {
	// throw new
	// IllegalArgumentException("Parameter codeTempData error in method autoCodeNum!");
	// }
	// if
	// (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode))
	// {
	// AutoCodeNodeData codeNodeData = new AutoCodeNodeData();
	// codeNodeData.setNodeId(nodeId);
	// codeNodeData.setNodeType(nodeType);
	// try {
	// AutoCodeResultData resultData = ConnectionPool.getAutoCodeAction()
	// .getStandardizedAutoCode(codeNodeData);
	// return resultData.getCode();
	// } catch (Exception e) {
	// log.error("", e);
	// }
	// }
	// return "";
	// }

	public static AutoCodeResultData getStandardizedAutoCodeResultData(Long nodeId, int nodeType) {
		if (nodeId == null || nodeId == null) {
			throw new IllegalArgumentException("Parameter codeTempData error in method autoCodeNum!");
		}
		AutoCodeNodeData codeNodeData = new AutoCodeNodeData();
		codeNodeData.setNodeId(nodeId);
		codeNodeData.setNodeType(nodeType);
		try {
			AutoCodeResultData resultData = ConnectionPool.getAutoCodeAction().getStandardizedAutoCode(codeNodeData);
			return resultData;
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * 自动编号
	 * 
	 * @date 2018-2-1
	 * @param selectNode
	 */
	public static void autoCodeAction(JecnTreeNode selectNode, JecnDialog dialog, boolean isView) {
		if (JecnConfigTool.isLiBangLoginType()) {
			AutoNumSelectDialog selectDialog = new AutoNumSelectDialog(selectNode, dialog);
			selectDialog.setVisible(true);
		} else if (JecnConfigTool.isMengNiuOperaType()) {
			MengNiuAutoNumSelectDialog selectDialog = new MengNiuAutoNumSelectDialog(selectNode, dialog, isView);
			selectDialog.setVisible(true);
		} else {
			if (JecnConfigTool.isArchitectureUploadFile()) {
				try {
					String flowNum = ConnectionPool.getFileAction().getFlowNumByFileId(
							selectNode.getJecnTreeBean().getId());
					MulinsenAutoNumSelectDialog selectDialog = new MulinsenAutoNumSelectDialog(selectNode, dialog,
							isView, flowNum);
					selectDialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("", e);
				}
			}

		}
	}

	public static TmpKpiShowValues getShowValues(Long flowId) throws Exception {
		return KpiTitleAndValues.INSTANCE.getConfigKpiNameTAndValues(flowId);
	}

	public static boolean isFlowAdmin() {
		return JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignProcessAdmin();
	}

	/**
	 * 是否显示元素属性
	 * 
	 * @return
	 */
	public static boolean isShowEleAttribute() {
		JecnDesignerModuleShow designModuleShow = JecnDesignerFrame.getDesignModuleShow();
		if (designModuleShow == null) {
			return false;
		}
		return designModuleShow.getEleFigureCheckBox().isSelected();
	}
}
