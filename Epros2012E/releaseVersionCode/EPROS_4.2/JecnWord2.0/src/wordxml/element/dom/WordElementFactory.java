package wordxml.element.dom;

import org.dom4j.Element;

import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.paragraph.Run;
import wordxml.element.dom.table.Table;
import wordxml.element.node.graph.GraphNode;
import wordxml.element.node.page.SectNode;
import wordxml.element.node.paragraph.GroupNode;
import wordxml.element.node.paragraph.ParagraphNode;
import wordxml.element.node.paragraph.RunNode;
import wordxml.element.node.table.TableNode;

public class WordElementFactory {

	/**
	 * 转换dom对象为XML
	 * 
	 * @param bodyEle
	 *            所有dom对象
	 * @param parentEle
	 *            父节点
	 */
	public static void produceElement(IBodyElement bodyEle, Element parentEle) {
		if (bodyEle instanceof Paragraph) {
			ParagraphNode.createParagraph((Paragraph) bodyEle, parentEle);
		} else if (bodyEle instanceof Run) {
			RunNode.createRun((Run) bodyEle, parentEle);
		} else if (bodyEle instanceof Table) {
			TableNode.createTable((Table) bodyEle, parentEle);
		} else if (bodyEle instanceof Sect) {
			SectNode.createSect((Sect) bodyEle, parentEle);
		} else if (bodyEle instanceof WordGraph) {
			GraphNode.createGraph((WordGraph) bodyEle, parentEle);
		} else if (bodyEle instanceof Group){
			GroupNode.createGroup((Group) bodyEle, parentEle);
		}
	}
}
