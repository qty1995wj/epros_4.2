package com.jecn.epros.server.service.task.app;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.IBaseService;

/**
 * @author huoyl
 * 
 */
public interface IJecnTaskApproveDelayService extends
		IBaseService<JecnUser, Long> {

	/**
	 * 发送任务搁置（或者叫人员缺失）提醒邮件
	 * 
	 * @author huoyl
	 * @param delayTaskDay
	 */
	public void sendDelayEmail() throws Exception;

}
