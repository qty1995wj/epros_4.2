package com.jecn.epros.server.service.popedom.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.dataimport.AddOrDelBaseBean;
import com.jecn.epros.server.bean.dataimport.BasePosAndPostionBean;
import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.bean.popedom.JecnPositionGroup;
import com.jecn.epros.server.bean.popedom.JecnPositionGroupOrgRelated;
import com.jecn.epros.server.bean.system.JecnTablePageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.Pager;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.popedom.IPositoinGroupDao;
import com.jecn.epros.server.service.popedom.IPositionGroupService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

@Transactional
public class PositionGroupServiceImpl extends AbsBaseService<JecnPositionGroup, Long> implements IPositionGroupService {
	private Logger log = Logger.getLogger(PositionGroupServiceImpl.class);
	private IPositoinGroupDao positionGroupDao;

	public IPositoinGroupDao getPositionGroupDao() {
		return positionGroupDao;
	}

	public void setPositionGroupDao(IPositoinGroupDao positionGroupDao) {
		this.positionGroupDao = positionGroupDao;
		this.baseDao = positionGroupDao;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：岗位组子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjects(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}

			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			if (obj.length > 3) {
				if ("1".equals(obj[3].toString())) {
					// 岗位组目录
					jecnTreeBean.setTreeNodeType(TreeNodeType.positionGroupDir);
				} else {
					// 岗位组节点
					jecnTreeBean.setTreeNodeType(TreeNodeType.positionGroup);
				}
			} else {
				// 岗位组节点
				jecnTreeBean.setTreeNodeType(TreeNodeType.positionGroup);
			}

			// 岗位组节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 岗位组节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 岗位组节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 岗位组节点父类名称
			jecnTreeBean.setPname("");
			if (obj.length > 4) {
				if (Integer.parseInt(obj[4].toString()) > 0) {
					// 岗位组节点是否有子节点
					jecnTreeBean.setChildNode(true);
				} else {
					jecnTreeBean.setChildNode(false);
				}
			} else {
				jecnTreeBean.setChildNode(false);
			}
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：岗位组子节点通用查询
	 * @param list
	 * @return
	 */
	private JecnTreeBean findByPos(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		// 岗位
		jecnTreeBean.setTreeNodeType(TreeNodeType.posGroupRelPos);

		// 岗位节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 岗位节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 岗位组节点ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 岗位组节点父类名称 组织名称
		if (obj[3] != null) {
			jecnTreeBean.setPname(obj[3].toString());
		}
		// 岗位节点是否有子节点
		jecnTreeBean.setChildNode(false);
		return jecnTreeBean;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：岗位组子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByPos(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = findByPos(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:树加载，通过父类Id和项目ID，获得子节点(目录和岗位组)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	@Override
	public List<JecnTreeBean> getChildPositionGroup(Long pId, Long projectId) throws Exception {
		try {
			String sql = "select t.id,t.name,t.per_id,t.is_dir"
					+ "       ,case when t.is_dir=1 then (select count(*) from jecn_position_group where per_id=t.id)"
					+ "             when t.is_dir=0 then (select count(jr.figure_id) from JECN_POSITION_GROUP_R jr,Jecn_Flow_Org_Image jfom,"
					+ "             Jecn_Flow_Org jfo where jr.figure_id=jfom.figure_id and jfom.org_id=jfo.org_id and jfo.del_state=0 and jr.group_id=t.id)"
					+ "             end count" + "	from jecn_position_group t where t.project_id=?"
					+ " and t.per_id=? order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, projectId, pId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:树加载，通过父类Id和项目ID 人员ID，获得子节点(目录和岗位组)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	@Override
	public List<JecnTreeBean> getChildPositionGroup(Long pId, Long projectId, Long peopleId) throws Exception {
		try {
			String sql = "select t.id,t.name,t.per_id,t.is_dir"
					+ "       ,case when t.is_dir=1 then (select count(*) from jecn_position_group where per_id=t.id)"
					+ "             when t.is_dir=0 then (select count(jr.figure_id) from JECN_POSITION_GROUP_R jr,Jecn_Flow_Org_Image jfom,"
					+ "             Jecn_Flow_Org jfo where jr.figure_id=jfom.figure_id and jfom.org_id=jfo.org_id and jfo.del_state=0 and jr.group_id=t.id)"
					+ "             end count" + "	from jecn_position_group t ";
			sql += JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(7, peopleId, projectId);
			sql += " where t.project_id=?" + " and t.per_id=? order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, projectId, pId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description: 树加载，通过父类Id和项目ID，获得岗位组目录
	 * @param pId
	 * @param projectId
	 * @return
	 */
	@Override
	public List<JecnTreeBean> getChildPositionGroupDirs(Long pId, Long projectId) throws Exception {
		try {
			String sql = "SELECT DISTINCT T.ID," + "       T.NAME," + "       T.PER_ID," + "       T.IS_DIR,"
					+ "       CASE WHEN T.IS_DIR=1 AND SUB.ID IS NOT NULL THEN 1 ELSE 0 END AS COUNT,T.SORT_ID"
					+ "  FROM JECN_POSITION_GROUP T"
					+ "  LEFT JOIN JECN_POSITION_GROUP SUB ON T.IS_DIR = 1 AND SUB.PER_ID = T.ID"
					+ " WHERE T.IS_DIR = 1" + "   AND T.PER_ID = ?" + "   AND T.PROJECT_ID = ?"
					+ " ORDER BY T.PER_ID, T.SORT_ID, T.ID";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, pId, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:树加载，通过项目ID，获得Role所有的子节点(目录、岗位组、岗位)
	 * @param projectId
	 * @return
	 */
	@Override
	public List<JecnTreeBean> getAllPositionGroup(Long projectId) throws Exception {
		try {
			String sql = "select t.id,t.name,t.per_id,t.is_dir"
					+ "       ,case when t.is_dir=1 then (select count(*) from jecn_position_group where per_id=t.id)"
					+ "             when t.is_dir=0 then (select count(jr.figure_id) from JECN_POSITION_GROUP_R jr,Jecn_Flow_Org_Image jfom,"
					+ "             Jecn_Flow_Org jfo where jr.figure_id=jfom.figure_id and jfom.org_id=jfo.org_id and jfo.del_state=0 and jr.group_id=t.id)"
					+ "             end count" + "	from jecn_position_group t where t.project_id=?"
					+ " order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, projectId);
			/** 所有岗位组和目录 */
			List<JecnTreeBean> listJecnTreeBean = findByListObjects(list);
			sql = "select jr.figure_id,jfoi.figure_text,jr.group_id,jfo.ORG_NAME from jecn_position_group_r jr,jecn_position_group j,jecn_flow_org_image jfoi,jecn_flow_org jfo"
					+ "       where jr.figure_id = jfoi.figure_id and jfoi.org_id = jfo.org_id and jfo.del_state=0 and jfo.projectid="
					+ projectId
					+ "       and jr.group_id = j.id and j.project_id="
					+ projectId
					+ " order by jr.group_id";
			list = positionGroupDao.listNativeSql(sql);
			for (Object[] obj : list) {
				JecnTreeBean jecnTreeBean = findByPos(obj);
				if (jecnTreeBean != null) {
					listJecnTreeBean.add(jecnTreeBean);
				}
			}
			return listJecnTreeBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getAllPositionGroupDir(Long projectId) throws Exception {
		try {
			String sql = "select t.id, t.name,t.per_id,t.is_dir,"
					+ "(select count(*) from jecn_position_group where per_id = t.id and is_dir=1) as count"
					+ " from jecn_position_group t where is_dir=1  and t.project_id=? order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchPositionGroupsByName(Long projectId, String name) throws Exception {
		try {
			String sql = "select t.id,t.name,t.per_id,t.is_dir,"
					+ "             (select count(jr.figure_id) from JECN_POSITION_GROUP_R jr,Jecn_Flow_Org_Image jfom,"
					+ "             Jecn_Flow_Org jfo where jr.figure_id=jfom.figure_id and jfom.org_id=jfo.org_id and jfo.del_state=0 and jr.group_id=t.id)"
					+ "             as count" + "	from jecn_position_group t where t.project_id=" + projectId
					+ " and t.is_dir = 0 and t.name like ? order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, 0, 20, "%" + name + "%");
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchPositionGroupsByName(Long projectId, String name, Long peopleId) throws Exception {
		try {
			String sql = "select t.id,t.name,t.per_id,t.is_dir,"
					+ "             (select count(jr.figure_id) from JECN_POSITION_GROUP_R jr,Jecn_Flow_Org_Image jfom,"
					+ "             Jecn_Flow_Org jfo where jr.figure_id=jfom.figure_id and jfom.org_id=jfo.org_id and jfo.del_state=0 and jr.group_id=t.id)"
					+ "             as count" + "	from jecn_position_group t ";
			sql += JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(7, peopleId, projectId);
			sql += " where t.project_id=" + projectId
					+ " and t.is_dir = 0 and t.name like ? order by t.per_id,t.sort_id,t.id";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, 0, 20, "%" + name + "%");
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateSortPosGroup(List<JecnTreeDragBean> list, Long pId, Long projectId, Long peopleId)
			throws Exception {
		try {
			String hql = "from JecnPositionGroup where perId=? and projectId=?";
			List<JecnPositionGroup> listJecnPositionGroup = positionGroupDao.listHql(hql, pId, projectId);
			List<JecnPositionGroup> listUpdate = new ArrayList<JecnPositionGroup>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (JecnPositionGroup jecnPositionGroup : listJecnPositionGroup) {
					if (jecnTreeDragBean.getId().equals(jecnPositionGroup.getId())) {
						if (jecnPositionGroup.getSortId() == null
								|| (jecnTreeDragBean.getSortId() != jecnPositionGroup.getSortId().intValue())) {
							jecnPositionGroup.setSortId(jecnTreeDragBean.getSortId());
							listUpdate.add(jecnPositionGroup);
							break;
						}
					}
				}
			}

			for (JecnPositionGroup jecnPositionGroup : listUpdate) {
				positionGroupDao.update(jecnPositionGroup);
				JecnUtil.saveJecnJournal(jecnPositionGroup.getId(), jecnPositionGroup.getName(), 16, 5, peopleId,
						positionGroupDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void movePositionGroups(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		try {
			String t_path = "";
			int level = -1;
			if (pId.intValue() != 0) {
				JecnPositionGroup bean = this.get(pId);
				t_path = bean.gettPath();
				level = bean.gettLevel();
			}
			JecnCommon.updateNodesChildTreeBeans(listIds, pId, "", new Date(), updatePersonId, t_path, level,
					positionGroupDao, moveNodeType, 1);
			JecnUtil.saveJecnJournals(listIds, 16, 4, updatePersonId, positionGroupDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void deletePositionGroups(List<Long> listIds, Long projectId, Long peopleId) throws Exception {
		try {
			Set<Long> setIds = new HashSet<Long>();
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM jecn_position_group RT WHERE RT.id in"
						+ JecnCommonSql.getIds(listIds) + " UNION ALL"
						+ " SELECT RTT.* FROM MY_JECN INNER JOIN jecn_position_group RTT"
						+ " ON MY_JECN.id=RTT.per_id)" + " SELECT distinct id FROM MY_JECN";
				List<Object> list = positionGroupDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select distinct t.id from jecn_position_group t" + "       connect by prior t.id = t.per_id"
						+ "       start with t.id in" + JecnCommonSql.getIds(listIds);
				List<Object> list = positionGroupDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				sql = "select id,per_id from JECN_POSITION_GROUP where PROJECT_ID=?";
				List<Object[]> listAll = positionGroupDao.listNativeSql(sql, projectId);
				setIds = JecnCommon.getAllChilds(listIds, listAll);

			}
			if (setIds.size() > 0) {
				positionGroupDao.deletePositionGroup(setIds);
				JecnUtil.saveJecnJournals(setIds, 16, 3, peopleId, positionGroupDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updatePositionGroup(Long positionGroupId, List<Long> positionIds, Long updatePersonId) throws Exception {
		try {
			JecnPositionGroup jecnPositionGroup = positionGroupDao.get(positionGroupId);
			jecnPositionGroup.setUpdatePersonId(updatePersonId);
			jecnPositionGroup.setUpdateTime(new Date());
			positionGroupDao.update(jecnPositionGroup);
			String hql = "from JecnPositionGroupOrgRelated where groupId=?";
			if (JecnContants.otherUserSyncType == 8) {
				hql += " and state =0";
			}
			// 获得岗位组的岗位关联信息
			List<JecnPositionGroupOrgRelated> listJecnPositionGroupOrgRelated = positionGroupDao.listHql(hql,
					positionGroupId);
			// 不需要删除的id集合
			List<Long> listNotDel = new ArrayList<Long>();
			// 需要保持的关联信息
			List<JecnPositionGroupOrgRelated> saveList = new ArrayList<JecnPositionGroupOrgRelated>();
			if (positionIds != null && positionIds.size() > 0) {
				for (Long posId : positionIds) {
					boolean isExist = false;
					for (JecnPositionGroupOrgRelated jecnPositionGroupOrgRelated : listJecnPositionGroupOrgRelated) {
						if (posId.equals(jecnPositionGroupOrgRelated.getFigureId())) {
							isExist = true;
							break;
						}
					}
					if (isExist) {
						listNotDel.add(posId);
					} else {
						JecnPositionGroupOrgRelated jecnPositionGroupOrgRelated = new JecnPositionGroupOrgRelated();
						jecnPositionGroupOrgRelated.setFigureId(posId);
						jecnPositionGroupOrgRelated.setGroupId(positionGroupId);
						jecnPositionGroupOrgRelated.setState(0L);
						saveList.add(jecnPositionGroupOrgRelated);
					}
				}
			}
			// 删除数据库中多余关系信息
			String sql = null;
			if (listNotDel.size() > 0) {
				if (JecnContants.otherUserSyncType != 8) {
					hql = "delete from JecnPositionGroupOrgRelated where groupId=? and figureId not in"
							+ JecnCommonSql.getIds(listNotDel);
					positionGroupDao.execteHql(hql, positionGroupId);
				} else {
					// 删除时需要判断当前岗位是否被基准岗位引用
					sql = "DELETE FROM JECN_POSITION_GROUP_R WHERE GROUP_ID =?" + " AND FIGURE_ID NOT IN "
							+ JecnCommonSql.getIds(listNotDel) + " AND FIGURE_ID NOT IN ("
							+ "   SELECT JFOI.FIGURE_ID FROM JECN_BASEPOS_REL_GROUP JBRG"
							+ "   INNER JOIN JECN_BASE_REL_POS JBRP" + "   ON JBRG.BASE_ID = JBRP.BASE_ID"
							+ "   INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + "   ON JBRP.POS_ID = JFOI.FIGURE_NUMBER_ID"
							+ "   AND JBRG.GROUP_ID = ?" + " )";
					positionGroupDao.execteNative(sql, positionGroupId, positionGroupId);
					// 修改
					sql = "UPDATE JECN_POSITION_GROUP_R  SET STATE = 1" + "   WHERE GROUP_ID =?"
							+ "   AND FIGURE_ID NOT IN " + JecnCommonSql.getIds(listNotDel) + "   AND FIGURE_ID IN ("
							+ "   SELECT JFOI.FIGURE_ID FROM JECN_BASEPOS_REL_GROUP JBRG"
							+ "   INNER JOIN JECN_BASE_REL_POS JBRP" + "   ON JBRG.BASE_ID = JBRP.BASE_ID"
							+ "   INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + "   ON JBRP.POS_ID = JFOI.FIGURE_NUMBER_ID"
							+ "   AND JBRG.GROUP_ID = ?" + ")";
					positionGroupDao.execteNative(sql, positionGroupId, positionGroupId);
				}
			} else {
				if (JecnContants.otherUserSyncType != 8) {
					hql = "delete from JecnPositionGroupOrgRelated where groupId=?";
					positionGroupDao.execteHql(hql, positionGroupId);
				} else {
					// 删除
					sql = "DELETE FROM JECN_POSITION_GROUP_R WHERE GROUP_ID =?" + " AND FIGURE_ID NOT IN ("
							+ "   SELECT JFOI.FIGURE_ID FROM JECN_BASEPOS_REL_GROUP JBRG"
							+ "   INNER JOIN JECN_BASE_REL_POS JBRP" + "   ON JBRG.BASE_ID = JBRP.BASE_ID"
							+ "   INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + "   ON JBRP.POS_ID = JFOI.FIGURE_NUMBER_ID"
							+ "   AND JBRG.GROUP_ID = ?" + " )";
					positionGroupDao.execteNative(sql, positionGroupId, positionGroupId);
					// 修改
					sql = "UPDATE JECN_POSITION_GROUP_R  SET STATE = 1" + " WHERE GROUP_ID =?" + " AND FIGURE_ID IN ("
							+ "   SELECT JFOI.FIGURE_ID FROM JECN_BASEPOS_REL_GROUP JBRG"
							+ "   INNER JOIN JECN_BASE_REL_POS JBRP" + "   ON JBRG.BASE_ID = JBRP.BASE_ID"
							+ "   INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + "   ON JBRP.POS_ID = JFOI.FIGURE_NUMBER_ID"
							+ "   AND JBRG.GROUP_ID = ?" + ")";
					positionGroupDao.execteNative(sql, positionGroupId, positionGroupId);
				}
			}
			// 岗位组中基准岗位中所有岗位集合
			List<Object> objects = null;
			if (JecnContants.otherUserSyncType == 8) {
				// 查找当前岗位组所关联的岗位是否包含当前要添加的岗位
				sql = "SELECT JFOI.FIGURE_ID FROM JECN_FLOW_ORG_IMAGE JFOI" + " INNER JOIN JECN_BASEPOS_REL_GROUP JBRG"
						+ " ON JBRG.GROUP_ID = ?" + " INNER JOIN JECN_BASE_REL_POS JBRP"
						+ " ON JBRP.BASE_ID = JBRG.BASE_ID" + " AND JFOI.FIGURE_NUMBER_ID = JBRP.POS_ID";
				Session session = positionGroupDao.getSession();
				Query query = session.createSQLQuery(sql);
				query.setParameter(0, positionGroupId);
				objects = query.list();
			}
			for (JecnPositionGroupOrgRelated jecnPositionGroupOrgRelated : saveList) {
				if (JecnContants.otherUserSyncType == 8) {
					boolean isExit = false;
					for (Object obj : objects) {
						if (jecnPositionGroupOrgRelated.getFigureId().toString().equals(obj.toString())) {
							sql = "UPDATE JECN_POSITION_GROUP_R SET STATE = 0" + " WHERE FIGURE_ID =?"
									+ " AND GROUP_ID =?";
							positionGroupDao.execteNative(sql, jecnPositionGroupOrgRelated.getFigureId(),
									positionGroupId);
							isExit = true;
							break;
						}
					}
					if (isExit) {
						continue;
					}
				}
				positionGroupDao.getSession().save(jecnPositionGroupOrgRelated);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	@Override
	public void rePositionGroupName(String newName, Long id, Long updatePersonId) throws Exception {
		try {
			String sqlT = null; // 查询临时表SQL
			String sql = null; // 查询正式表SQL
			if (JecnCommon.isSQLServer()) { // 由于sqlserver别名的使用方式不同，所以需要单独判断是否是sqlserver版本
				// 更新流程元素中的角色的文本为新的岗位组名称
				sqlT = "UPDATE JFSI SET JFSI.FIGURE_TEXT=? FROM JECN_FLOW_STRUCTURE_IMAGE_T AS JFSI"
						+ "	WHERE EXISTS (SELECT 1" + " FROM PROCESS_STATION_RELATED_T PSR"
						+ " LEFT JOIN JECN_POSITION_GROUP JPG" + " ON  PSR.FIGURE_POSITION_ID=JPG.ID"
						+ " WHERE PSR.FIGURE_POSITION_ID=?" + " AND JFSI.FIGURE_ID=PSR.FIGURE_FLOW_ID"
						+ " AND JFSI.FIGURE_TEXT=JPG.NAME)";

				sql = "UPDATE JFSI SET JFSI.FIGURE_TEXT=? FROM JECN_FLOW_STRUCTURE_IMAGE JFSI"
						+ "	WHERE EXISTS (SELECT 1" + " FROM PROCESS_STATION_RELATED PSR"
						+ " LEFT JOIN JECN_POSITION_GROUP JPG" + " ON  PSR.FIGURE_POSITION_ID=JPG.ID"
						+ " WHERE PSR.FIGURE_POSITION_ID=?" + " AND JFSI.FIGURE_ID=PSR.FIGURE_FLOW_ID"
						+ " AND JFSI.FIGURE_TEXT=JPG.NAME)";
			} else {
				// 更新流程元素中的角色的文本为新的岗位组名称
				sqlT = "UPDATE JECN_FLOW_STRUCTURE_IMAGE_T JFSI SET JFSI.FIGURE_TEXT=?" + "	WHERE EXISTS (SELECT 1"
						+ " FROM PROCESS_STATION_RELATED_T PSR" + " LEFT JOIN JECN_POSITION_GROUP JPG"
						+ " ON  PSR.FIGURE_POSITION_ID=JPG.ID" + " WHERE PSR.FIGURE_POSITION_ID=?"
						+ " AND JFSI.FIGURE_ID=PSR.FIGURE_FLOW_ID" + " AND JFSI.FIGURE_TEXT=JPG.NAME)";

				sql = "UPDATE JECN_FLOW_STRUCTURE_IMAGE JFSI SET JFSI.FIGURE_TEXT=?" + "	WHERE EXISTS (SELECT 1"
						+ " FROM PROCESS_STATION_RELATED PSR" + " LEFT JOIN JECN_POSITION_GROUP JPG"
						+ " ON  PSR.FIGURE_POSITION_ID=JPG.ID" + " WHERE PSR.FIGURE_POSITION_ID=?"
						+ " AND JFSI.FIGURE_ID=PSR.FIGURE_FLOW_ID" + " AND JFSI.FIGURE_TEXT=JPG.NAME)";
			}
			// 更新临时表
			positionGroupDao.execteNative(sqlT, newName, id);
			// 更新正式表
			positionGroupDao.execteNative(sql, newName, id);
			// 更新岗位组信息
			JecnPositionGroup jecnPositionGroup = positionGroupDao.get(id);
			jecnPositionGroup.setName(newName);
			jecnPositionGroup.setUpdatePersonId(updatePersonId);
			jecnPositionGroup.setUpdateTime(new Date());
			positionGroupDao.update(jecnPositionGroup);
			JecnUtil.saveJecnJournal(jecnPositionGroup.getId(), jecnPositionGroup.getName(), 16, 2, jecnPositionGroup
					.getUpdatePersonId(), positionGroupDao, 4);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPositionGroupDirsByNameMove(String name, List<Long> listIds, Long projectId)
			throws Exception {
		try {
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM jecn_position_group JFF WHERE JFF.id in"
						+ JecnCommonSql.getIds(listIds) + " UNION ALL"
						+ " SELECT JFS.* FROM MY_JECN INNER JOIN jecn_position_group JFS ON MY_JECN.id=JFS.per_id)"
						+ "    select t.id, t.name,t.per_id,t.is_dir"
						+ "                ,(select count(*) from jecn_position_group where per_id=t.id and is_dir=1)"
						+ "         from jecn_role_info t where t.project_id = " + projectId
						+ "         and t.name like ? and t.is_dir=1 and t.id not in (select role_id from MY_JECN)";
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select t.id, t.name,t.per_id,t.is_dir"
						+ "                ,(select count(*) from jecn_position_group where per_id=t.id and is_dir=1)"
						+ "         from jecn_position_group t where t.project_id = "
						+ projectId
						+ "         and t.name like ? and t.is_dir=1 and t.id not in (select t.id from jecn_position_group t"
						+ " connect by prior t.id = t.per_id START WITH t.id in " + JecnCommonSql.getIds(listIds)
						+ " )";
			}
			List<Object[]> list = positionGroupDao.listNativeSql(sql, 0, 20, "%" + name + "%");
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid, Long projectId) throws Exception {
		try {
			String hql = "select count(*) from JecnPositionGroup where name=? and perId=? and id<>? and isDir=0 and projectId=?";
			return positionGroupDao.countAllByParamsHql(hql, newName, pid, id, projectId) > 0 ? true : false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id, Long projectId) throws Exception {
		try {
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM jecn_position_group JFF WHERE JFF.id ="
						+ id
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_JECN INNER JOIN jecn_position_group JFS ON MY_JECN.per_id=JFS.id)"
						+ " select t.id,t.name,t.per_id,t.is_dir,(select count(*) from jecn_position_group where per_id=t.id)"
						+ " from jecn_position_group t where t.project_id = " + projectId
						+ " and (t.per_id in (select id from MY_JECN) or t.per_id=0) and t.per_id<>" + id
						+ " order by t.per_id,t.sort_id,t.id";
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select t.id, t.name,t.per_id,t.is_dir,(select count(*) from jecn_position_group where per_id=t.id)"
						+ " from jecn_position_group t where t.project_id = "
						+ projectId
						+ " and  (t.per_id  in (select t.id from jecn_position_group t"
						+ " connect by prior t.per_id = t.id START WITH t.id = "
						+ id
						+ " ) or t.per_id=0) and t.per_id<>" + id + " order by t.per_id,t.sort_id,t.id";
			}
			List<Object[]> list = positionGroupDao.listNativeSql(sql);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPosByPositionGroupId(Long id) throws Exception {
		try {
			String sql = "select distinct jr.figure_id,jfoi.figure_text,jr.group_id,jfo.ORG_NAME from jecn_position_group_r jr,jecn_position_group j,jecn_flow_org_image jfoi,jecn_flow_org jfo"
					+ "       where jr.figure_id = jfoi.figure_id and jfoi.org_id = jfo.org_id and jfo.del_state=0 "
					+ "       and jr.group_id = ?";
			// if (JecnContants.otherUserSyncType == 8) {// 基准岗位同步
			// sql += " and jr.state=0";
			// }
			sql += " order by jr.group_id";
			List<Object[]> list = positionGroupDao.listNativeSql(sql, id);
			return findByPos(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 查询基准岗位
	 * 
	 * @author cheshaowei
	 * 
	 * @param baseName
	 *            搜索的基准岗位名称 posGroupId 选中岗位组ID
	 * */
	@Override
	public BasePosAndPostionBean getJecnBaseBean(Long projectId, String baseName, String posGroupId) {
		BasePosAndPostionBean basePosAndPostionBean = new BasePosAndPostionBean();
		// 基准岗位集合
		List<JecnBasePosBean> jecnBasePosBeanList = positionGroupDao.getJecnBasePosBean(projectId, baseName);
		basePosAndPostionBean.setJecnBasePosBeanList(jecnBasePosBeanList);
		if (StringUtils.isBlank(baseName)) {
			// 查询岗位组对应的基准岗位
			List<JecnBasePosBean> basePosBeanList = positionGroupDao.getJecnbasePosRelBean(projectId, posGroupId);
			basePosAndPostionBean.setbJecnBasePosBeanList(basePosBeanList);
		}
		return basePosAndPostionBean;
	}

	/**
	 * 根据ID查询基准岗位对应岗位
	 * 
	 * @author cheshaowei
	 * @param baseId
	 *            基准岗位ID
	 * @throws Exception
	 * */
	@Override
	public List<Object[]> getBaseRelPos(String baseId, String pageNum, String pageSize, String operationType)
			throws Exception {
		return positionGroupDao.getBaseRelPos(baseId, pageNum, pageSize, operationType);
	}

	/**
	 * 
	 * 添加新关联的基准岗位，关联基准岗位
	 * 
	 * @author cheshaowei
	 * @param addOrDelBaseBean
	 *            只存在一条岗位组合基准岗位对应关系 待添加数据和待更新数据封装bean
	 * 
	 * */
	public void addOrUpdate(AddOrDelBaseBean addOrDelBaseBean) throws Exception {
		if (addOrDelBaseBean == null || JecnCommon.isNullOrEmtryTrim(addOrDelBaseBean.getPosGroupId())) {
			return;
		}
		Long groupId = Long.valueOf(addOrDelBaseBean.getPosGroupId());
		// 清空岗位组对应岗位和基准岗位相关数据
		if (addOrDelBaseBean.getAddJecnBasePosBeanList().size() == 0) {
			deleteGroupRef(groupId);
			return;
		}

		// 1、获取岗位组关联的最新基准岗位ID集合
		List<String> addBaseIds = new ArrayList<String>();
		for (JecnBasePosBean bean : addOrDelBaseBean.getAddJecnBasePosBeanList()) {
			addBaseIds.add(bean.getBasePosNum());
		}
		String sql = null;
		// 2、获取已存在的基准岗位编号
		sql = "SELECT RG.BASE_ID FROM JECN_BASEPOS_REL_GROUP RG WHERE RG.GROUP_ID = " + groupId;

		List<Object> list = positionGroupDao.listNativeSql(sql);

		// 3、删除和添加的基准岗位编号
		List<String> deleteBaseIds = new ArrayList<String>();
		String existsBaseId = "";

		List<String> updateOrInsetBaseIds = new ArrayList<String>();
		for (Object object : list) {// 已存在的基准岗位编号集合
			existsBaseId = object.toString();
			if (!addBaseIds.contains(existsBaseId)) {
				deleteBaseIds.add(existsBaseId);
			} else {
				updateOrInsetBaseIds.add(existsBaseId);
			}
			// 去除已存在的剩余为新增
			addBaseIds.remove(existsBaseId);
		}

		// 4、删除
		positionGroupDao.deleteBasePosRel(deleteBaseIds, groupId);

		// 5、添加 (已存在的基准岗位和新增的基准岗位 可能存在相同岗位ID,添加的时候把存在的岗位ID过掉)
		// 已存在的 + 新增的 = 需要更新的基准岗位ID集合
		updateOrInsetBaseIds.addAll(addBaseIds);
		positionGroupDao.addBasePosRelGroup(addBaseIds, updateOrInsetBaseIds, groupId);

	}

	/**
	 * 删除岗位组关联岗位信息
	 * 
	 * @param @param groupId
	 * @return void
	 * @date 2015-7-20 下午06:14:43
	 * @throws
	 */
	private void deleteGroupRef(Long groupId) {
		try {
			// 删除基准岗位对应岗位组下的岗位
			String sql = "DELETE FROM JECN_POSITION_GROUP_R" + " WHERE GROUP_ID = ? AND STATE =1";
			positionGroupDao.execteNative(sql, groupId);
			// 删除基准岗位与岗位关系表
			sql = "DELETE FROM JECN_BASEPOS_REL_GROUP  WHERE GROUP_ID = ?";
			positionGroupDao.execteNative(sql, groupId);
		} catch (HibernateException e) {
			log.error("删除基准岗位与岗位关系出现异常！" + this.getClass() + " addOrUpdate() ", e);
			throw e;
		}
	}

	/**
	 * 判断是否存在岗位组和基准岗位关联
	 * 
	 * @param @param groupId
	 * @param @return 1:基准岗位匹配，2岗位匹配 ;0:未匹配
	 * @author ZXH
	 * @date 2015-7-17 下午02:33:16
	 * @throws
	 */
	@Override
	public int posGroupRelCount(Long groupId) throws Exception {
		String sql = "SELECT COUNT(GROUP_ID) FROM JECN_BASEPOS_REL_GROUP WHERE GROUP_ID = ?";
		int count = positionGroupDao.countAllByParamsNativeSql(sql, groupId);
		if (count > 0) {
			return 1;
		}
		sql = "SELECT COUNT(GROUP_ID) FROM JECN_POSITION_GROUP_R WHERE GROUP_ID = ? AND STATE = 0";
		count = positionGroupDao.countAllByParamsNativeSql(sql, groupId);
		if (count > 0) {
			return 2;
		}
		return count;
	}

	@Override
	public void AddSelectPoss(Set<Long> selectIds, Long groupId) throws Exception {
		for (Long figureId : selectIds) {
			JecnPositionGroupOrgRelated jecnPositionGroupOrgRelated = new JecnPositionGroupOrgRelated();
			jecnPositionGroupOrgRelated.setFigureId(figureId);
			jecnPositionGroupOrgRelated.setGroupId(groupId);
			jecnPositionGroupOrgRelated.setState(0L);
			positionGroupDao.getSession().save(jecnPositionGroupOrgRelated);
		}

	}

	@Override
	public void AddSearchResultPoss(Long orgId, String posName, Long groupId, Long projectId) throws Exception {
		Long state = 0L;
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			String sql = "insert INTO JECN_POSITION_GROUP_R(ID,GROUP_ID,FIGURE_ID,STATE)  select JECN_POSITION_GROUP_R_SQUENCE.NEXTVAL AS ID,"
					+ groupId
					+ " AS GROUP_ID,pos.figure_id AS FIGURE_ID,"
					+ state
					+ " AS STATE"
					+ this.getSearchCommon(orgId, posName);
			positionGroupDao.execteNative(sql, projectId, groupId);
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sql = "insert INTO JECN_POSITION_GROUP_R(GROUP_ID,FIGURE_ID,STATE) select " + groupId
					+ " AS GROUP_ID,pos.figure_id AS FIGURE_ID," + state + " AS STATE"
					+ this.getSearchCommon(orgId, posName);
			positionGroupDao.execteNative(sql, projectId, groupId);
		}

	}

	@Override
	public JecnTablePageBean searchPos(Long orgId, String posName, int curPage, String operationType, int pageSize,
			Long groupId, Long projectId) throws Exception {
		String sqlCommon = this.getSearchCommon(orgId, posName);
		String sqlTotalCount = "select count(pos.figure_id) " + sqlCommon;
		int totalCount = positionGroupDao.countAllByParamsNativeSql(sqlTotalCount, projectId, groupId);
		Pager pager = JecnCommon.getPager(curPage, pageSize, operationType, totalCount);
		int startRow = pager.getStartRow();
		int totalPage = pager.getTotalPages();
		String sql = "select pos.figure_id,pos.figure_text,org.org_id,org.org_name " + sqlCommon;
		List<Object[]> list = positionGroupDao.listNativeSql(sql, startRow, pageSize, projectId, groupId);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		listResult = JecnUtil.fillPosOrgPath(listResult, positionGroupDao);
		JecnTablePageBean jecnTablePageBean = new JecnTablePageBean();
		jecnTablePageBean.setListJecnTreeBean(listResult);
		jecnTablePageBean.setTotalCount(totalCount);
		jecnTablePageBean.setCurPage(pager.getCurrentPage());
		jecnTablePageBean.setTotalPage(totalPage);
		return jecnTablePageBean;
	}

	private String getSearchCommon(Long orgId, String posName) {
		String sqlCommon = " from jecn_flow_org_image pos"
				+ "     inner join jecn_flow_org org on  pos.org_id = org.org_id and org.del_state=0 and org.projectid=?";
		if (orgId != null) {
			List<Long> listIds = new ArrayList<Long>();
			listIds.add(orgId);
			sqlCommon = sqlCommon + "     inner join (" + JecnCommonSql.getChildObjectsSqlGetSingleIdOrg(listIds)
					+ ") s_org on s_org.org_id = pos.org_id";
		}
		sqlCommon = sqlCommon
				+ "     where pos.figure_type="
				+ JecnCommonSql.getPosString()
				+ "     and pos.figure_id  not in (select jpgr.figure_id from jecn_position_group_r jpgr where jpgr.group_id=?)";
		if (posName != null && !"".equals(posName)) {
			sqlCommon += " and pos.figure_text like '%" + posName + "%'";
		}
		return sqlCommon;
	}

	@Override
	public void delResultSelectPoss(Set<Long> selectIds, Long groupId) throws Exception {
		if (selectIds.size() > 0) {
			String sql = "delete from jecn_position_group_r where GROUP_ID =? and FIGURE_ID in"
					+ JecnCommonSql.getIdsSet(selectIds);
			positionGroupDao.execteNative(sql, groupId);
		}
	}

	@Override
	public void cancelResultPoss(Long groupId) throws Exception {
		String sql = "delete from jecn_position_group_r where GROUP_ID =?";
		positionGroupDao.execteNative(sql, groupId);
	}

	@Override
	public JecnTablePageBean showPos(int curPage, String operationType, int pageSize, Long groupId) throws Exception {
		String sqlCommon = "from jecn_flow_org_image pos"
				+ "     inner join jecn_flow_org org on  pos.org_id = org.org_id and org.del_state=0"
				+ "     inner join jecn_position_group_r jpgr on jpgr.figure_id = pos.figure_id and jpgr.group_id=?"
				+ "     where pos.figure_type=" + JecnCommonSql.getPosString();
		String sqlTotalCount = "select count(pos.figure_id) " + sqlCommon;
		int totalCount = positionGroupDao.countAllByParamsNativeSql(sqlTotalCount, groupId);
		Pager pager = JecnCommon.getPager(curPage, pageSize, operationType, totalCount);
		int startRow = pager.getStartRow();
		int totalPage = pager.getTotalPages();
		String sql = "select pos.figure_id,pos.figure_text,org.org_id,org.org_name " + sqlCommon + "  order by jpgr.id";
		List<Object[]> list = positionGroupDao.listNativeSql(sql, startRow, pageSize, groupId);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		listResult = JecnUtil.fillPosOrgPath(listResult, positionGroupDao);
		JecnTablePageBean jecnTablePageBean = new JecnTablePageBean();
		jecnTablePageBean.setListJecnTreeBean(listResult);
		jecnTablePageBean.setTotalCount(totalCount);
		jecnTablePageBean.setCurPage(pager.getCurrentPage());
		jecnTablePageBean.setTotalPage(totalPage);
		return jecnTablePageBean;
	}

	@Override
	public JecnTablePageBean searchPos(String orgName, String posName, int curPage, String operationType, int pageSize,
			Long groupId, Long projectId, boolean isSelect) {
		JecnTablePageBean jecnTablePageBean = new JecnTablePageBean();
		jecnTablePageBean.setTotalCount(0);
		jecnTablePageBean.setCurPage(0);
		if (StringUtils.isBlank(orgName) && StringUtils.isBlank(posName)) {
			return jecnTablePageBean;
		}
		String sqlCommon = this.getSearchCommon(orgName, posName, isSelect);
		String sqlTotalCount = "select count(figure_id) from (" + sqlCommon + ") a";
		int totalCount = positionGroupDao.countAllByParamsNativeSql(sqlTotalCount, groupId);
		Pager pager = JecnCommon.getPager(curPage, pageSize, operationType, totalCount);
		int startRow = pager.getStartRow();
		int totalPage = pager.getTotalPages();
		String sql = sqlCommon;
		List<Object[]> list = positionGroupDao.listNativeSql(sql, startRow, pageSize, groupId);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		listResult = JecnUtil.fillPosOrgPath(listResult, positionGroupDao);
		jecnTablePageBean.setListJecnTreeBean(listResult);
		jecnTablePageBean.setTotalCount(totalCount);
		jecnTablePageBean.setCurPage(pager.getCurrentPage());
		jecnTablePageBean.setTotalPage(totalPage);
		return jecnTablePageBean;
	}

	@Override
	public JecnTablePageBean searchPos(List<Long> orgIds, String posName, int curPage, String operationType,
			int pageSize, Long groupId, Long projectId, boolean isSelect) {
		JecnTablePageBean jecnTablePageBean = new JecnTablePageBean();
		jecnTablePageBean.setTotalCount(0);
		jecnTablePageBean.setCurPage(0);
		if (orgIds == null || orgIds.isEmpty()) {
			return jecnTablePageBean;
		}
		String sqlCommon = this.getSearchCommon(orgIds, posName, isSelect);
		String sqlTotalCount = "select count(figure_id) from (" + sqlCommon + ") a";
		int totalCount = positionGroupDao.countAllByParamsNativeSql(sqlTotalCount, groupId);
		Pager pager = JecnCommon.getPager(curPage, pageSize, operationType, totalCount);
		int startRow = pager.getStartRow();
		int totalPage = pager.getTotalPages();
		String sql = sqlCommon;
		List<Object[]> list = positionGroupDao.listNativeSql(sql, startRow, pageSize, groupId);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		listResult = JecnUtil.fillPosOrgPath(listResult, positionGroupDao);
		jecnTablePageBean.setListJecnTreeBean(listResult);
		jecnTablePageBean.setTotalCount(totalCount);
		jecnTablePageBean.setCurPage(pager.getCurrentPage());
		jecnTablePageBean.setTotalPage(totalPage);
		return jecnTablePageBean;
	}

	private String getSearchCommon(String orgName, String posName, boolean isSelect) {
		String sql = JecnCommonSql.getPosByOrgAndPos(orgName, posName, isSelect);
		sql += "  and jfoi.figure_id  not in (select jpgr.figure_id from jecn_position_group_r jpgr where jpgr.group_id=?)";
		return sql;
	}

	private String getSearchCommon(String orgName, String posName) {
		String sql = JecnCommonSql.getPosByOrgAndPos(orgName, posName);
		sql += "  and jfoi.figure_id  not in (select jpgr.figure_id from jecn_position_group_r jpgr where jpgr.group_id=?)";
		return sql;
	}

	private String getSearchCommon(List<Long> orgIds, String posName, boolean isSelect) {
		String sql = JecnCommonSql.getPosByOrgAndPos(orgIds, posName, isSelect);
		sql += "  and jfoi.figure_id  not in (select jpgr.figure_id from jecn_position_group_r jpgr where jpgr.group_id=?)";
		return sql;
	}

	@Override
	public void AddSearchResultPoss(String orgName, String posName, Long groupId, Long projectId, boolean isSelect) {
		Long state = 0L;
		String key = "";
		String value = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			key = " ID,";
			value = " JECN_POSITION_GROUP_R_SQUENCE.NEXTVAL AS ID,";
		}
		String sql = "insert INTO JECN_POSITION_GROUP_R(" + key + "GROUP_ID,FIGURE_ID,STATE)  select " + value + ""
				+ groupId + " AS GROUP_ID,figure_id AS FIGURE_ID," + state + " AS STATE" + " from ("
				+ this.getSearchCommon(orgName, posName, isSelect) + ")a";
		positionGroupDao.execteNative(sql, groupId);

	}

	@Override
	public void AddSearchResultPoss(List<Long> ids, String posName, Long groupId, Long projectId, boolean isSelect) {
		Long state = 0L;
		String key = "";
		String value = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			key = " ID,";
			value = " JECN_POSITION_GROUP_R_SQUENCE.NEXTVAL AS ID,";
		}
		String sql = "insert INTO JECN_POSITION_GROUP_R(" + key + "GROUP_ID,FIGURE_ID,STATE)  select " + value + ""
				+ groupId + " AS GROUP_ID,figure_id AS FIGURE_ID," + state + " AS STATE" + " from ("
				+ this.getSearchCommon(ids, posName, isSelect) + ")a";
		positionGroupDao.execteNative(sql, groupId);

	}

	@Override
	public String getPosGroupExplain(Long id) throws SQLException, IOException {
		String sql = "SELECT EXPLAIN FROM JECN_POSITION_GROUP WHERE ID = ?";
		Clob text = positionGroupDao.getObjectNativeSql(sql, id);
		if (text == null) {
			return "";
		}
		return JecnCommonSql.ClobToString(text);
	}

	@Override
	public boolean getPosGroupExplain(Long id, String posGroupExplain, Long peopleId) throws Exception {
		String sql = "update jecn_position_group set explain = ? where id = ?";
		int execteNative = positionGroupDao.execteNative(sql, posGroupExplain, id);
		JecnPositionGroup group = positionGroupDao.get(id);
		JecnUtil.saveJecnJournal(id, group.getName(), 16, 2, peopleId, positionGroupDao);
		return execteNative == 1;
	}

	@Override
	public Long addPositionGroup(JecnPositionGroup positionGroup) throws Exception {
		positionGroup.setCreateTime(new Date());
		positionGroup.setUpdateTime(new Date());
		Long id = positionGroupDao.save(positionGroup);
		// 更新tpath 和tlevel
		updateTpathAndTLevel(positionGroup);
		JecnUtil.saveJecnJournal(positionGroup.getId(), positionGroup.getName(), 16, 1, positionGroup
				.getCreatePersonId(), positionGroupDao);
		return id;
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 * @param jecnFlowStructureT
	 * @throws Exception
	 */
	private void updateTpathAndTLevel(JecnPositionGroup positionGroup) throws Exception {
		// 获取上级节点
		JecnPositionGroup prePos = positionGroupDao.get(positionGroup.getPerId());
		if (prePos == null) {
			positionGroup.settLevel(0);
			positionGroup.settPath(positionGroup.getId() + "-");
			positionGroup.setViewSort(JecnUtil.concatViewSort("", positionGroup.getSortId()));
		} else {
			positionGroup.settLevel(prePos.gettLevel() + 1);
			positionGroup.settPath(prePos.gettPath() + positionGroup.getId() + "-");
			positionGroup.setViewSort(JecnUtil.concatViewSort(prePos.getViewSort(), positionGroup.getSortId()));
		}
		// 更新
		positionGroupDao.update(positionGroup);
	}

}
