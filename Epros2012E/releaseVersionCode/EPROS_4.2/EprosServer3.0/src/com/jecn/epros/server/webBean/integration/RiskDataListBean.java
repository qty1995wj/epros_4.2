package com.jecn.epros.server.webBean.integration;
import java.util.*;
/**
 * 风险清单
 * 
 * @author chehuanbo
 * @since V3.06
 */
public class RiskDataListBean {
	
    /**风险ID*/
	private Long riskId=null;
	/**是否为目录*/
	private int isDir;
	/**目录级别*/
	private int level;
	/**父节点ID*/
	private Long parentId=null;
	/**风险编号*/
	private String riskCode=null;
	/**风险描述或者目录名*/
	private String riskName=null;
	/**风险等级 （当前风险系数 高 中 低）*/
	private int  riskGrade;
	/**标准化控制*/
	private String standardControl=null;
	/**企业内部控制相关要求*/
	private String guideVal=null;
	/**制度ID*/
	private Long ruld=null;
	/**制度名称*/
	private String ruleName=null;
	/**制度责任部门ID*/
	private Long ruleOrgId=null;
	/**制度责任部门名称*/
	private String ruleOrgName=null;
	/**控制目标ID*/
	private Long conrolId=null;
	/**控制点描述*/
	private String controlDescroption=null;
	/**活动ID*/
	private Long activeId=null;
	/**活动名称*/
	private String activeName=null;
	/**控制点编号*/
	private String controlNum=null;
	/**活动说明*/
	private String activeShow=null;;
	/**所属流程ID*/
    private Long flowId=null;
    /**所属流程名称*/
	private String flowName=null;
	/**流程责任人部门ID*/
	private Long resOrgId=null;
	/**流程责任人名称*/
	private String resOrgName=null;
	/**控制方法*/
	private int controlMethod;
	/**控制类型*/
	private int controlType;
	/**是否为关键控制点*/
	private int keyControl;
	/**控制频率*/
	private int controlFrequency;
	/**角色名称*/
	private String roleName;
	
	
	
	
	
	public Long getRiskId() {
		return riskId;
	}
	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}
	public int getIsDir() {
		return isDir;
	}
	public void setIsDir(int isDir) {
		this.isDir = isDir;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	public String getRiskName() {
		return riskName;
	}
	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}
	public int getRiskGrade() {
		return riskGrade;
	}
	public void setRiskGrade(int riskGrade) {
		this.riskGrade = riskGrade;
	}
	public String getStandardControl() {
		return standardControl;
	}
	public void setStandardControl(String standardControl) {
		this.standardControl = standardControl;
	}
	public String getGuideVal() {
		return guideVal;
	}
	public void setGuideVal(String guideVal) {
		this.guideVal = guideVal;
	}
	public Long getRuld() {
		return ruld;
	}
	public void setRuld(Long ruld) {
		this.ruld = ruld;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	
	public Long getRuleOrgId() {
		return ruleOrgId;
	}
	public void setRuleOrgId(Long ruleOrgId) {
		this.ruleOrgId = ruleOrgId;
	}
	public String getRuleOrgName() {
		return ruleOrgName;
	}
	public void setRuleOrgName(String ruleOrgName) {
		this.ruleOrgName = ruleOrgName;
	}
	public Long getConrolId() {
		return conrolId;
	}
	public void setConrolId(Long conrolId) {
		this.conrolId = conrolId;
	}
	public String getControlDescroption() {
		return controlDescroption;
	}
	public void setControlDescroption(String controlDescroption) {
		this.controlDescroption = controlDescroption;
	}
	public Long getActiveId() {
		return activeId;
	}
	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}
	public String getActiveName() {
		return activeName;
	}
	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}
	public String getControlNum() {
		return controlNum;
	}
	public void setControlNum(String controlNum) {
		this.controlNum = controlNum;
	}
	public String getActiveShow() {
		return activeShow;
	}
	public void setActiveShow(String activeShow) {
		this.activeShow = activeShow;
	}
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public Long getResOrgId() {
		return resOrgId;
	}
	public void setResOrgId(Long resOrgId) {
		this.resOrgId = resOrgId;
	}
	public String getResOrgName() {
		return resOrgName;
	}
	public void setResOrgName(String resOrgName) {
		this.resOrgName = resOrgName;
	}
	
	public int getControlMethod() {
		return controlMethod;
	}
	public void setControlMethod(int controlMethod) {
		this.controlMethod = controlMethod;
	}
	public int getControlType() {
		return controlType;
	}
	public void setControlType(int controlType) {
		this.controlType = controlType;
	}
	public int getKeyControl() {
		return keyControl;
	}
	public void setKeyControl(int keyControl) {
		this.keyControl = keyControl;
	}
	public int getControlFrequency() {
		return controlFrequency;
	}
	public void setControlFrequency(int controlFrequency) {
		this.controlFrequency = controlFrequency;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
   	
}
