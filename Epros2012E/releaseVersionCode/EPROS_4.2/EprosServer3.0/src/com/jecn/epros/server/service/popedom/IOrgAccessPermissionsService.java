package com.jecn.epros.server.service.popedom;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnOrgAccessPermissionsT;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IOrgAccessPermissionsService extends IBaseService<JecnOrgAccessPermissionsT, Long> {

	/**
	 * @author yxw 2012-6-15
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的组织权限
	 * @param relateId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type) throws Exception;

	/**
	 * @author yxw 2012-10-24
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的查阅权限和是否公开
	 * @param relateId
	 * @param type0是流程1是文件2是标准3制度
	 * @return
	 * @throws Exception
	 */
	public LookPopedomBean getAccessPermissions(Long relateId, int type) throws Exception;

	/**
	 * @author yxw 2012-11-8
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的查阅权限（不包括密级）
	 * @param relateId
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public LookPopedomBean getOnlyAccessPermissions(Long relateId, int type) throws Exception;

	/**
	 * 
	 * @author yxw 2012-10-25
	 * @description: 查阅权限 是否公开修改
	 * @param relateId
	 * @param type
	 *            0是流程，1是文件，2是标准，3是制度
	 * @param isPublic
	 * @param orgIds
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param posIds
	 * @param posType
	 *            0:本节点1：包括子节点
	 * @param posGroupIds
	 * @param posGroupType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0：管理员
	 * @param projectId
	 * @param saveMode 
	 * @param userId 
	 * @throws Exception
	 */
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, int orgType, int posType, int userType, Long projectId, int posGroupType, int saveMode,
			AccessId accId, Long userId) throws Exception;
	/**
	 * 
	 * @author yxw 2012-10-25
	 * @description: 查阅权限 是否公开修改
	 * @param relateId
	 * @param type
	 *            0是流程，1是文件，2是标准，3是制度
	 * @param isPublic
	 * @param orgIds
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param posIds
	 * @param posType
	 *            0:本节点1：包括子节点
	 * @param posGroupIds
	 * @param posGroupType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0：管理员
	 * @param projectId
	 * @param saveMode
	 * @throws Exception
	 */
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, String orgIds, int orgType, String posIds, int posType, int userType, Long projectId,
			String posGroupIds, int posGroupType, int saveMode) throws Exception;

	public boolean peopleHasFlowFileDownloadPerm(Set<Long> flowSet, Long userId) throws Exception;

	LookPopedomBean getAccessPermissions(Long relateId, int type, boolean pub) throws Exception;

}
