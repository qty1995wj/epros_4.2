package epros.draw.gui.operationConfig;

import epros.draw.util.JecnResourceUtil;

/**
 * 编辑 配置流程操作说明 名称
 * 
 * @author 2012-07-04
 * 
 */
public class EditConfigDialog extends JecnEditNameDialog {

	private DrawOperationDescriptionConfigBean configurBean = null;
	public boolean isOperation = false;

	public EditConfigDialog(DrawOperationDescriptionConfigBean configurBean) {
		this.configurBean = configurBean;

		this.setLocationRelativeTo(null);
		// 给名称显示框赋值
		this.setName(configurBean.getName());
	}

	@Override
	public String getDialogTitle() {
		// 编辑名称
		return JecnResourceUtil.getJecnResourceUtil().getValue("editName");
	}

	@Override
	public String getNameLab() {
		// 名称：
		return JecnResourceUtil.getJecnResourceUtil().getValue("nameC");
	}

	@Override
	public void saveData() {
		configurBean.setName(this.getName());
		this.dispose();
		isOperation = true;
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return false;
	}
}
