package com.jecn.epros.server.action.designer.rule.impl;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.jecn.epros.server.action.designer.rule.IRuleModeAction;
import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.bean.rule.RuleModeTitleBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.rule.IRuleModeService;
import com.jecn.epros.server.util.JecnUtil;

/**
 * @author yxw 2012-5-14
 * @description：
 */
public class RuleModeActionImpl implements IRuleModeAction {

	private IRuleModeService ruleModeService;

	public IRuleModeService getRuleModeService() {
		return ruleModeService;
	}

	public void setRuleModeService(IRuleModeService ruleModeService) {
		this.ruleModeService = ruleModeService;
	}

	@Override
	public List<JecnTreeBean> getChildRuleMode(Long pId) throws Exception {
		return ruleModeService.getChildRuleMode(pId);
	}

	@Override
	public void updateSortRuleMode(List<JecnTreeDragBean> list, Long pId) throws Exception {
		ruleModeService.updateSortRuleMode(list, pId);

	}

	@Override
	public List<JecnTreeBean> getAllNode() throws Exception {

		return ruleModeService.getAllNode();
	}

	@Override
	public List<JecnTreeBean> getChildRuleModeDir(Long pId) throws Exception {
		return ruleModeService.getChildRuleModeDir(pId);
	}

	@Override
	public List<JecnTreeBean> getAllRuleModeDir() throws Exception {
		return ruleModeService.getAllRuleModeDir();
	}

	@Override
	public List<JecnTreeBean> getRuleModeDirsByNameMove(String name, List<Long> listIds) throws Exception {
		return ruleModeService.getRuleModeDirsByNameMove(name, listIds);
	}

	@Override
	public void moveRuleModes(List<Long> listIds, Long pId, Long peopleId) throws Exception {
		ruleModeService.moveRuleModes(listIds, pId, peopleId);
	}

	@Override
	public Long addRuleModeDir(RuleModeBean ruleModeBean) throws Exception {
		return ruleModeService.addRuleModeDir(ruleModeBean);
	}

	@Override
	public Long addRuleMode(RuleModeBean ruleModeBean, List<RuleModeTitleBean> ruleModeTitleBean) throws Exception {

		ruleModeBean.setUpdateDate(new Date());
		return ruleModeService.addRuleMode(ruleModeBean, ruleModeTitleBean);
	}

	@Override
	public void updateName(String newName, Long id, Long updatePersonId) throws Exception {
		ruleModeService.updateName(newName, id, updatePersonId);

	}

	@Override
	public void updateRuleMode(RuleModeBean ruleModeBean, List<RuleModeTitleBean> ruleModeTitleBeanList)
			throws Exception {
		ruleModeService.updateRuleMode(ruleModeBean, ruleModeTitleBeanList);

	}

	@Override
	public RuleModeBean getRuleModeById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return ruleModeService.get(id);
	}

	@Override
	public void deleteRuleMode(List<Long> ids, Long peopleId) throws Exception {
		ruleModeService.deleteRuleMode(ids, peopleId);

	}

	@Override
	public List<JecnTreeBean> findRuleModeByName(String name) throws Exception {
		return ruleModeService.findRuleModeByName(name);
	}

	@Override
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid) throws Exception {
		return ruleModeService.validateRepeatNameEidt(newName, id, pid);
	}

	@Override
	public List<RuleModeTitleBean> getRuleModeTitles(Long modeId) throws Exception {
		List<RuleModeTitleBean> titleList = null;
		if (null != modeId) {
			titleList = ruleModeService.getRuleModeTitles(modeId);
		}
		return titleList;
	}

	@Override
	public List<RuleModeTitleBean> getRuleModeTitle(Long modeId) throws Exception {
		List<RuleModeTitleBean> titleList = ruleModeService.getRuleModeTitles(modeId);
		return titleList;
	}

}
