package com.jecn.epros.domain.report.base;

import java.util.*;

/**
 * 折线Echart 数据类
 */
public class LineChartOptions {

    private List<SerieData> series;
    private List<String> xAxis;

    public List<String> getLegend() {
        List<String> legend = new ArrayList<>();
        if (series == null) {
            return legend;
        }
        for (SerieData series : series) {
            legend.add(series.getName());
        }
        return legend;
    }

    public List<SerieData> getSeries() {
        return series;
    }

    public void setSeries(List<SerieData> series) {
        this.series = series;
    }

    public List<String> getxAxis() {
        return xAxis;
    }

    public void setxAxis(List<String> xAxis) {
        this.xAxis = xAxis;
    }
}
