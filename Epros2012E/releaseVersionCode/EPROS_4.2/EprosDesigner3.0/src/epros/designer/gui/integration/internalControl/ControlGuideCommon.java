package epros.designer.gui.integration.internalControl;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreePath;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 内控指引知识库管理公用方法
 * 
 * @author Administrator
 * 
 */
public class ControlGuideCommon {

	// private static Logger log = Logger.getLogger(ControlGuideCommon.class);
	/** 树节点 */
	private static List<JecnTreeNode> node;

	/**
	 * 获取所有内控指引知识库信息
	 * 
	 * @param list
	 *            所有节点
	 * @return
	 */
	public static JecnTreeModel getControlGuideTreeModel(List<JecnTreeBean> list) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.innerControlRoot, JecnProperties
						.getValue("controlApp"));
		JecnTreeCommon.addNLevelNodes(list, rootNode);
		return new JecnTreeModel(rootNode);
	}

	/**
	 * 移动内控指引知识库节点
	 * 
	 * @param list
	 *            所有节点
	 * @param listIds
	 *            ID集合
	 * @return
	 */
	public static JecnTreeModel getTreeMoveModel(List<JecnTreeBean> list,
			List<Long> listIds) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.innerControlRoot, JecnProperties
						.getValue("controlApp"));
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, listIds);
		return new JecnTreeModel(rootNode);
	}

	/**
	 * 双击、单击、右键树节点的操作
	 * 
	 * @param evt
	 *            鼠标事件
	 * @param jTree
	 *            树
	 * @param controlGuideDialog
	 *            内控指引知识库界面
	 */
	public static void treeMousePressed(MouseEvent evt, JecnTree jTree,
			ControlGuideManageDialog controlGuideDialog) {
		HighEfficiencyJecnControlGuideTree highEfficiencyJecnControlGuideTree = (HighEfficiencyJecnControlGuideTree) jTree;
		// 点击左键
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			// 双击左键
			if (evt.getClickCount() == 2) {
				TreePath[] treePath = jTree.getSelectionPaths();
				if (treePath != null && treePath.length == 1) {
					JecnTreeNode node = (JecnTreeNode) treePath[0]
							.getLastPathComponent();
					// 如果双击的节点是内控指引知识库目录则展开节点，如果是条款，则显示条款
					if (node.getJecnTreeBean().getTreeNodeType().toString()
							.equals(TreeNodeType.innerControlRoot.toString())) {
						if (highEfficiencyJecnControlGuideTree.getIsOpen() == 0) {
							if (JecnTreeCommon.isAuthNode(node) == 0) {
								JecnOptionPane.showMessageDialog(
										JecnSystemStaticData.getFrame(),
										"frame");
								return;
							}
						} else {
							JecnTreeCommon.autoExpandNode(jTree, node);
						}
					}
				}
			} else if (evt.getClickCount() == 1) {
				// TreePath[] treePath = jTree.getSelectionPaths();
				// if (treePath != null && treePath.length == 1) {
				// JecnTreeNode node = (JecnTreeNode) treePath[0]
				// .getLastPathComponent();
				// // 如果单击击的节点是内控指引知识库目录无反应，如果是条款，则将对应数据显示在右侧
				// if (node.getJecnTreeBean().getTreeNodeType().toString()
				// .equals(TreeNodeType.innerControlClause.toString())) {
				// Long id = node.getJecnTreeBean().getId();
				// //获取内控条款数据
				// JecnControlGuide controlGuide = null;
				// try {
				// controlGuide = ConnectionPool.getControlGuideAction().
				// getJecnControlGuideById(id);
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				// if
				// (highEfficiencyJecnControlGuideTree.getIsControlGuideMange()
				// == 0) {
				// controlGuideDialog.guideNameText.setEditable(true);
				// controlGuideDialog.controlGuideContent.setEditable(true);
				// //条款名称
				// controlGuideDialog.guideNameText.setText(node.getJecnTreeBean().getName());
				// //条款内容
				// if(controlGuide != null && controlGuide.getDescription()!=
				// null ){
				// controlGuideDialog.controlGuideContent.setText(controlGuide.getDescription());
				// }
				// controlGuideDialog.setSelectNode(node);
				// }
				// }else{
				// if
				// (highEfficiencyJecnControlGuideTree.getIsControlGuideMange()
				// == 0) {
				// controlGuideDialog.guideNameText.setText("");
				// controlGuideDialog.controlGuideContent.setText("");
				// controlGuideDialog.guideNameText.setEditable(false);
				// controlGuideDialog.controlGuideContent.setEditable(false);
				// }
				// }
				// }
			}
		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = jTree.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick
						.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean()
						.getTreeNodeType();
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath
								.getLastPathComponent();
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean()
								.getTreeNodeType())) {
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}
				if (listSelectNode.size() > 0) {
					ControlGuideTreeMenu controlGuideTreeMenu = null;
					if (highEfficiencyJecnControlGuideTree
							.getIsControlGuideMange() == 0) {
						controlGuideTreeMenu = new ControlGuideTreeMenu(
								highEfficiencyJecnControlGuideTree,
								controlGuideDialog);
					} else if (highEfficiencyJecnControlGuideTree
							.getIsControlGuideMange() == 1) {
						controlGuideTreeMenu = new ControlGuideTreeMenu(
								highEfficiencyJecnControlGuideTree,
								highEfficiencyJecnControlGuideTree
										.getControlGuideChooseDialog());
					}
					if (listSelectNode.size() == 1) {
						jTree.setSelectionPath(listTreePaths.get(0));
						controlGuideTreeMenu.setListNode(listSelectNode);
						controlGuideTreeMenu.show(evt.getComponent(), evt
								.getX(), evt.getY());
						node = listSelectNode;
						return;
					}
					// 去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon
							.repeatChildNodes(listSelectNode, jTree);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild
								.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath
										.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(
										nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							jTree.setSelectionPath(treePaths[0]);
						} else {
							jTree.setSelectionPaths(treePaths);
						}
						controlGuideTreeMenu.setListNode(listRemoveChild);
						controlGuideTreeMenu.show(evt.getComponent(), evt
								.getX(), evt.getY());
					}
				}
			}
		}
	}

	public static List<JecnTreeNode> getNode() {
		return node;
	}

	public static void setNode(List<JecnTreeNode> node) {
		ControlGuideCommon.node = node;
	}

}
