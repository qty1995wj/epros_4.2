package com.jecn.epros.server.service.system.impl;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.dao.system.IJecnElementsLibraryDao;
import com.jecn.epros.server.service.system.IJecnElementsLibraryServie;

@Transactional
public class JecnElementsLibraryServieImpl implements IJecnElementsLibraryServie {
	private IJecnElementsLibraryDao elementsLibraryDao;

	public IJecnElementsLibraryDao getElementsLibraryDao() {
		return elementsLibraryDao;
	}

	public void setElementsLibraryDao(IJecnElementsLibraryDao elementsLibraryDao) {
		this.elementsLibraryDao = elementsLibraryDao;
	}

	public void updateElementsLibary(List<String> list, int type) throws Exception {
		elementsLibraryDao.updateElementsLibary(list, type);
	}

	/**
	 * 
	 * @param type
	 *            0：流程架构；1：流程;2：组织
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public List<JecnElementsLibrary> getShowElementsLibraryByType(int type) throws Exception {
		return elementsLibraryDao.getShowElementsLibraryByType(type);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public List<JecnElementsLibrary> getAllElementsLibraryByType(int type) throws Exception {
		return elementsLibraryDao.getAllElementsLibraryByType(type);
	}

	/**
	 * 待添加元素集合
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnElementsLibrary> getAddElementsLibraryByType(int type) throws Exception {
		return elementsLibraryDao.getAddElementsLibraryByType(type);
	}
}
