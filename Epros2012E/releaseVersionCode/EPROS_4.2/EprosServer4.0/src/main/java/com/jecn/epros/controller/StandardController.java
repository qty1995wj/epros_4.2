package com.jecn.epros.controller;

import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.standard.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.standard.StandardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/standard", description = "标准体系", position = 4)
public class StandardController {

    @Autowired
    private StandardService standardService;

    @ApiOperation(value = "标准-搜索")
    @RequestMapping(value = "/standards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StandardViewBean> searchStandard(@ModelAttribute SearchStandardBean searchStandardBean, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("searchStandardBean", searchStandardBean);
        map.put("paging", paging);
        StandardViewBean standardViewBean = new StandardViewBean();
        int total = standardService.searchStandardTotal(map);
        List<SearchStandardResultBean> listSearchStandardResultBean = standardService.searchStandard(map);
        standardViewBean.setTotal(total);
        standardViewBean.setData(listSearchStandardResultBean);
        return ResponseEntity.ok(standardViewBean);
    }

    @PreAuthorize("hasAuthority('standardBaseInfo')")
    @ApiOperation(value = "标准-概况信息")
    @RequestMapping(value = "/standards/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StandardBaseInfoBean> findBaseInfoBean(@PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        return ResponseEntity.ok(standardService.findBaseInfoBean(map));
    }

    @ApiOperation(value = "标准-树节点查询")
    @RequestMapping(value = "/standards/{id}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> findChildStandards(
            @ApiParam(value = "父节点Id", required = true) @PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pid", id);
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        return ResponseEntity.ok(standardService.findChildStandards(map));
    }
    //standard(#id) &&
   @PreAuthorize("hasAuthority('standardRelatedFiles')")
    @ApiOperation(value = "标准-相关文件")
    @RequestMapping(value = "/standards/{id}/files", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StandardRelatedFileBean> findRelatedFiles(@PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        return ResponseEntity.ok(standardService.findRelatedFiles(map));
    }

}
