package com.jecn.webservice.wanhua.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;
import com.jecn.webservice.jbshihua.service.UserService;
import com.jecn.webservice.wanhua.IWanHuaOAUserService;
import com.jecn.webservice.wanhua.bean.WanHuaPostUser;

@WebService
public class WanHuaOAUserServiceImpl implements IWanHuaOAUserService {
	private Logger log = Logger.getLogger(WanHuaOAUserServiceImpl.class);

	private static final String EMAIL_STR = "@whchem.com";

	@Override
	@WebMethod
	public String saveUsers(List<WanHuaPostUser> users) {
		Map<String, String> map = new HashMap<String, String>();
		if (users == null || users.size() == 0) {
			map.put("status", "false");
			map.put("result", "人员数据不能为空！");
			JSONArray jsonArray = JSONArray.fromObject(map);
			String json = jsonArray.toString();
			log.error(json);
			return json;
		}
		log.info("获取人员总数：" + users.size());
		// 根据数据对象获取代保存的数据
		List<JecnUserPosView> saveUsers = new ArrayList<JecnUserPosView>();
		JecnUserPosView addUser = null;

		for (WanHuaPostUser user : users) {
			addUser = new JecnUserPosView(null, user.getAccount(), user.getFullName(), user.getOrgId(), user
					.getPosCode(), user.getPosName(), null, user.getAccount() + EMAIL_STR, "", null, "0");
			if (StringUtils.isBlank(addUser.getPosNum()) || StringUtils.isBlank(addUser.getPosName())
					|| StringUtils.isBlank(addUser.getOrgNum())) {
				addUser.setPosNum("");
				addUser.setPosName("");
				addUser.setOrgNum("");
			}
			addUser.setUserNum(JecnCommon.getUUID());
			saveUsers.add(addUser);
		}

		UserService userService = (UserService) ApplicationContextUtil.getContext().getBean("UserServiceImpl");
		int result = 0;
		try {

			// 获取已存在的用户
			List<JecnUserPosView> existsUsers = userService.getAllUsers();
			// 批量修改 数据状态
			if (existsUsers != null && existsUsers.size() > 0) {
				for (JecnUserPosView existUser : existsUsers) {
					if (StringUtils.isBlank(existUser.getLoginName())) {
						continue;
					}
					for (JecnUserPosView saveUser : saveUsers) {// 接口获取用户
						if (StringUtils.isBlank(saveUser.getLoginName())) {
							continue;
						}
						if (existUser.getLoginName().equals(saveUser.getLoginName())) {// 根据用户名判断是否更新
							saveUser.setUserNum(existUser.getUserNum());
							// 更新
							saveUser.setStatus("1");
							log.info("更新人员信息 : " + saveUser.toString());
						}
					}
				}
			}

			log.info("处理后，人员总数: " + saveUsers.size());
			result = userService.saveUsers(saveUsers);
			map.put("status", 0 == result ? "success" : "false");
			map.put("result", "");
		} catch (Exception e) {
			map.put("status", 0 == result ? "success" : "false");
			map.put("result", e.getMessage());
			e.printStackTrace();
		}
		return JSONArray.fromObject(map).toString();
	}
}
