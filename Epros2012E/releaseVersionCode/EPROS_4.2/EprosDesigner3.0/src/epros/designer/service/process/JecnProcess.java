package epros.designer.service.process;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.epros.server.bean.process.ProcessFigureData;
import com.jecn.epros.server.bean.process.ProcessMapFigureData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSearchPopup;
import epros.designer.gui.pagingImage.PageUtil;
import epros.designer.gui.timeline.TimeLimitLineBuilder;
import epros.designer.gui.workflow.JecnWorkflowMenuItem;
import epros.designer.service.JecnDesignerImpl;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnImplData;
import epros.draw.data.figure.JecnModeFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.data.figure.JecnToFromRelatedData;
import epros.draw.data.figure.TermFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.designer.JecnActiveStandardBeanT;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.designer.JecnRefIndicatorsT;
import epros.draw.designer.TmpPageSetingBean;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitlePanel;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

/**
 * 打开流程图和流程地图
 * 
 * @author fuzhh
 * 
 */
public class JecnProcess {
	private static Logger log = Logger.getLogger(JecnProcess.class);

	/**
	 * 判断界面是否打开
	 * 
	 * @author fuzhh Aug 24, 2012
	 * @param flowId
	 *            流程ID
	 * @param mapType
	 *            面板类型
	 * @return true 表示打开 ， false 表示未打开
	 */
	public static boolean isOpen(int flowId, MapType mapType) {
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane drawDesktopPane = tabbedTitlePanel.getCurrDesktopPane();
				if (drawDesktopPane.getFlowMapData().getMapType() == mapType
						&& drawDesktopPane.getFlowMapData().getFlowId().intValue() == flowId) {
					JecnDrawMainPanel.getMainPanel().getTabbedPane().setSelectedComponent(
							drawDesktopPane.getScrollPanle());
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 打开流程地图
	 * 
	 * @param processMapData
	 */
	public static void openJecnProcessTotalMap(ProcessOpenMapData openMapData, boolean isOpenMode) {
		// 创建流程地图
		JecnFlowMapData totalMapData = JecnDesignerCommon.createFlowMapData(MapType.totalMap, openMapData
				.getJecnFlowStructureT().getFlowId());
		// 设置流程地图名称
		totalMapData.setName(openMapData.getJecnFlowStructureT().getFlowName());
		// // 面板宽
		// totalMapData.setWorkflowWidth(Integer.valueOf(processMapData
		// .getJecnFlowStructureT().getStrWidth()));
		// // 面板高
		// totalMapData.setWorkflowHeight(Integer.valueOf(processMapData
		// .getJecnFlowStructureT().getStrHeight()));
		ModeType modeType = ModeType.none;
		if (openMapData.getJecnFlowStructureT().getDataType() == 1) {
			modeType = ModeType.totalMapMode;
		}

		totalMapData.setAuthSave(openMapData.getIsAuthSave());
		totalMapData.setOccupierName(openMapData.getOccupierName());
		// 设置类型
		totalMapData.getDesignerData().setModeType(modeType);
		JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);

		// 设置流程地图数据
		setPorcessTotalMapData(openMapData, isOpenMode);

		JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 打开集成关系图
	 * 
	 * @param processMapData
	 */
	/**
	 * 打开流程地图
	 * 
	 * @param processMapData
	 */
	public static void openProcessMapRelated(ProcessOpenMapData openMapData, boolean isOpenMode) {

		JecnFlowMapData processMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		// 创建流程地图
		JecnFlowMapData totalMapData = JecnDesignerCommon.createFlowMapData(MapType.totalMapRelation, openMapData
				.getJecnFlowStructureT().getFlowId());

		int status;
		try {
			status = JecnDesignerCommon.getSaveAuthStatus(TreeNodeType.processMap, processMapData.getFlowId());
			if (status == 0) {
				return;
			}
			totalMapData.setAuthSave(status);
			if (status == 5) {
				totalMapData.setOccupierName(JecnDesignerCommon.getOccupierName(TreeNodeType.processMap, processMapData
						.getFlowId()));
			}
		} catch (Exception e) {
			log.error("JecnDesignerCommon processOpenValidate is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

		// 设置流程地图名称
		totalMapData.setName(openMapData.getJecnFlowStructureT().getFlowName());
		ModeType modeType = ModeType.none;
		// 设置类型
		totalMapData.getDesignerData().setModeType(modeType);
		JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);

		// 设置流程地图数据
		setPorcessTotalMapData(openMapData, isOpenMode);

		JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 设置流程地图数据
	 * 
	 * @param processMapData
	 *            流程地图数据bean
	 */
	public static void setPorcessTotalMapData(ProcessOpenMapData processMapData, boolean isOpenMode) {
		// 存储线的数据集合
		Map<JecnFlowStructureImageT, Integer> lineDataMap = new HashMap<JecnFlowStructureImageT, Integer>();
		// 获取所有图形集合
		List<JecnBaseFlowElementPanel> baseFlowElementList = new ArrayList<JecnBaseFlowElementPanel>();
		// 数据根据层级排序
		sortAllFlowStructureImageList(processMapData.getMapFigureDataList());
		if (processMapData.getMapFigureDataList() == null) {
			log.error("JecnProcess setPorcessTotalMapData is error");
			return;
		}
		for (ProcessMapFigureData mapFigureData : processMapData.getMapFigureDataList()) {
			// 生成除连接线外的所有图形
			getProcessData(mapFigureData, lineDataMap, baseFlowElementList, null, isOpenMode);
		}
		// 生成连接线
		getLineData(lineDataMap, baseFlowElementList, isOpenMode);

	}

	/**
	 * 分页符数据获取
	 * 
	 * @param flowStructureT
	 */
	private static void setPageSetBean(JecnFlowStructureT flowStructureT) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || flowStructureT == null) {
			return;
		}
		// 分页信息Bean
		TmpPageSetingBean pageSetingBean = desktopPane.getFlowMapData().getPageSetingBean();
		pageSetingBean.setPageSets(flowStructureT.getPagings());
		pageSetingBean.setPageX(flowStructureT.getXpoint());
		// 每页宽度
		pageSetingBean.setPageWidth(PageUtil.getPageWidth());
		// 横纵向
		pageSetingBean.setPagingVh(flowStructureT.getImagvh() == 0 ? true : false);
		pageSetingBean.setShowPageSet(false);
	}

	/**
	 * 创建流程图
	 * 
	 * @param processData
	 *            流程图数据bean
	 */
	public static void openJecnProcessMap(ProcessOpenData processOpenData, boolean isOpenMode) {
		JecnFlowMapData partMapData = JecnDesignerCommon.createFlowMapData(MapType.partMap, processOpenData
				.getJecnFlowStructureT().getFlowId());
		// 名称
		partMapData.setName(processOpenData.getJecnFlowStructureT().getFlowName());

		ModeType modeType = ModeType.none;
		if (processOpenData.getJecnFlowStructureT().getDataType() == 1) {
			modeType = ModeType.partMapMode;
		}
		// 设置类型
		partMapData.getDesignerData().setModeType(modeType);

		// 是否存在编辑权限，不存在 提示已只读打开
		// if (JecnDesignerCommon
		// .isOnlyReadAuth(processOpenData.getJecnFlowStructureT().getFlowId(),
		// TreeNodeType.process)) {
		// partMapData.setAuthSave(4);
		// }
		// if (processOpenData.isAccess()) {
		// partMapData.setAuthSave(3);
		// }

		partMapData.setAuthSave(processOpenData.getIsAuthSave());
		partMapData.setOccupierName(processOpenData.getOccupierName());
		JecnDrawMainPanel.getMainPanel().addWolkflow(partMapData);

		setProcessMapData(processOpenData, isOpenMode);

		/** 设置 操作说明 目的.... 等数据 */
		// 目的
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowPurpose(
				processOpenData.getJecnFlowBasicInfoT().getFlowPurpose());
		// 范围
		getWorkflow().getFlowMapData().getFlowOperationBean().setApplicability(
				processOpenData.getJecnFlowBasicInfoT().getApplicability());
		// 术语定义
		getWorkflow().getFlowMapData().getFlowOperationBean().setNoutGlossary(
				processOpenData.getJecnFlowBasicInfoT().getNoutGlossary());
		// 输入
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowInput(
				processOpenData.getJecnFlowBasicInfoT().getInput());
		// 输出
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowOutput(
				processOpenData.getJecnFlowBasicInfoT().getOuput());

		// 新版 流程输入输出；条款化方式展示
		// 输入
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowInTs(
				JecnSaveProcess.switchServerJecnFlowInoutT(processOpenData.getFlowInTs()));
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowOutTs(
				JecnSaveProcess.switchServerJecnFlowInoutT(processOpenData.getFlowOutTs()));

		// 起始活动
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowStartActive(
				processOpenData.getJecnFlowBasicInfoT().getFlowStartpoint());
		// 终止活动
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowEndActive(
				processOpenData.getJecnFlowBasicInfoT().getFlowEndpoint());
		// 不适用范围
		getWorkflow().getFlowMapData().getFlowOperationBean().setNoApplicability(
				processOpenData.getJecnFlowBasicInfoT().getNoApplicability());
		// 补充说明
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowSupplement(
				processOpenData.getJecnFlowBasicInfoT().getFlowSupplement());
		// 概况信息
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowSummarize(
				processOpenData.getJecnFlowBasicInfoT().getFlowSummarize());
		// 客户
		getWorkflow().getFlowMapData().getFlowOperationBean().setFlowCustom(
				processOpenData.getJecnFlowBasicInfoT().getFlowCustom());

		// 自定义要素1
		getWorkflow().getFlowMapData().getFlowOperationBean().setCustomOne(
				processOpenData.getJecnFlowBasicInfoT().getFlowCustomOne());

		// 自定义要素2
		getWorkflow().getFlowMapData().getFlowOperationBean().setCustomTwo(
				processOpenData.getJecnFlowBasicInfoT().getFlowCustomTwo());

		// 自定义要素3
		getWorkflow().getFlowMapData().getFlowOperationBean().setCustomThree(
				processOpenData.getJecnFlowBasicInfoT().getFlowCustomThree());

		// 自定义要素4
		getWorkflow().getFlowMapData().getFlowOperationBean().setCustomFour(
				processOpenData.getJecnFlowBasicInfoT().getFlowCustomFour());

		// 自定义要素5
		getWorkflow().getFlowMapData().getFlowOperationBean().setCustomFive(
				processOpenData.getJecnFlowBasicInfoT().getFlowCustomFive());

		// 相关文件(D2版本特有的)
		if (!JecnConstants.isPubShow()) {
			getWorkflow().getFlowMapData().getFlowOperationBean().setFlowRelatedFile(
					processOpenData.getJecnFlowBasicInfoT().getFlowRelatedFile());
		}
		/** 结束 */
		// 角色移动
		if (JecnSystemStaticData.isShowRoleMove()) {
			getWorkflow().removeJecnRoleMobile();
			// 添加角色移动
			getWorkflow().createAddJecnRoleMobile();
		} else {
			// 移除角色移动
			getWorkflow().removeJecnRoleMobile();
		}

		JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 设置流程图数据
	 * 
	 * @param processData
	 *            流程图数据bean
	 */
	public static void setProcessMapData(ProcessOpenData processOpenData, boolean isOpenMode) {
		// 存储线的数据集合
		Map<JecnFlowStructureImageT, Integer> lineDataMap = new HashMap<JecnFlowStructureImageT, Integer>();
		// 获取所有图形集合
		List<JecnBaseFlowElementPanel> baseFlowElementList = new ArrayList<JecnBaseFlowElementPanel>();
		// 数据根据层级排序
		sortAllProcessFigureDataList(processOpenData.getProcessFigureDataList());
		// 横竖标识
		boolean isHFlag = true;
		if (processOpenData.getJecnFlowBasicInfoT() != null) {
			if (processOpenData.getJecnFlowBasicInfoT().getIsXorY() != null) {
				if (1 == processOpenData.getJecnFlowBasicInfoT().getIsXorY()) {
					isHFlag = false;
				}
			}
		}
		getWorkflow().getFlowMapData().setHFlag(isHFlag);
		if (processOpenData.getJecnFlowStructureT().getPagings() > 1) {
			// 分页符数据信息获取
			setPageSetBean(processOpenData.getJecnFlowStructureT());
		}

		for (ProcessFigureData processFigureData : processOpenData.getProcessFigureDataList()) {
			// 生成除连接线外的所有图形
			getProcessData(null, lineDataMap, baseFlowElementList, processFigureData, isOpenMode);
		}
		// 生成连接线
		getLineData(lineDataMap, baseFlowElementList, isOpenMode);

		if (JecnWorkflowMenuItem.isShowTimeLine) {
			// 绘制时间轴
			TimeLimitLineBuilder.INSTANCE.buildTimeLinePanel();
		}
	}

	/**
	 * 把图形和分割线添加到面板
	 * 
	 * @param flowStructureImageT
	 *            服务器图形的数据
	 * @param lineDataMap
	 *            存储服务器线的数据和层级
	 * @param baseFlowElementList
	 *            存储转换后图形的数据
	 * @param processFigureData
	 *            流程图属性数据
	 */
	public static void getProcessData(ProcessMapFigureData mapFigureData,
			Map<JecnFlowStructureImageT, Integer> lineDataMap, List<JecnBaseFlowElementPanel> baseFlowElementList,
			ProcessFigureData processFigureData, boolean isOpenMode) {
		// 面板为空返回
		if (getWorkflow() == null || (mapFigureData == null && processFigureData == null)) {
			return;
		}
		JecnFlowStructureImageT flowStructureImageT = null;
		if (mapFigureData == null) {
			flowStructureImageT = processFigureData.getJecnFlowStructureImageT();
		} else if (processFigureData == null) {
			flowStructureImageT = mapFigureData.getJecnFlowStructureImageT();
		}
		if ((MapElemType.CommonLine.toString().equals(flowStructureImageT.getFigureType()))
				|| (MapElemType.ManhattanLine.toString().equals(flowStructureImageT.getFigureType()))) { // 连接线
			lineDataMap.put(flowStructureImageT, getOrderIndex());
		} else if (MapElemType.ParallelLines.toString().equals(flowStructureImageT.getFigureType())) { // 横分割线
			// 创建横分割线数据
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.ParallelLines, LineDirectionEnum.horizontal);
			// 线主键
			vhLineData.setFlowElementId(flowStructureImageT.getFigureId());
			vhLineData.setUUID(flowStructureImageT.getUUID());

			if (flowStructureImageT.getPoLongy() != null) {
				int pointY = flowStructureImageT.getPoLongy().intValue();
				// 层级
				vhLineData.setZOrderIndex(getOrderIndex());

				// //适应visio导出添加UUID 以前是流程ID
				vhLineData.setFlowElementUUID(DrawCommon.getUUID());
				// 线条类型
				if (flowStructureImageT.getBodyLine() != null) {
					vhLineData.setBodyStyle(Integer.valueOf(flowStructureImageT.getBodyLine()));
				}
				// 线条颜色
				String lineColor = flowStructureImageT.getLineColor();
				if (!DrawCommon.isNullOrEmtryTrim(lineColor)) {
					String[] backColors = lineColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					vhLineData.setBodyColor(newBackColor);
				}
				// 线条宽带
				if (flowStructureImageT.getLineThickness() != null) {
					int lineWidth = flowStructureImageT.getLineThickness();
					vhLineData.setBodyWidth(lineWidth);
				}
				// 判断如果为生成模板的数据
				if (isOpenMode) {
					vhLineData.setFlowElementId(-1);
				}
				// 生成分割线
				ParallelLines parallelLine = new ParallelLines(vhLineData);
				// 将分割线添加到面板
				JecnAddFlowElementUnit.addVHLine(parallelLine, new Point(0, pointY));
				JecnVHLineData vHLineDataClone = vhLineData.clone();
				vHLineDataClone.setFlowElementId(vhLineData.getFlowElementId());
				vHLineDataClone.setUUID(vhLineData.getUUID());
				getLineDataList().add(vHLineDataClone);
				// 设置分割线层级
				parallelLine.reSetFlowElementZorder();
			}
		} else if (MapElemType.VerticalLine.toString().equals(flowStructureImageT.getFigureType())) { // 竖分割线
			// 创建竖分割线
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.VerticalLine, LineDirectionEnum.vertical);
			// 线主键
			vhLineData.setFlowElementId(flowStructureImageT.getFigureId());
			vhLineData.setUUID(flowStructureImageT.getUUID());
			if (flowStructureImageT.getPoLongx() != null) {
				int pointX = Integer.valueOf(flowStructureImageT.getPoLongx().intValue());
				// 层级
				vhLineData.setZOrderIndex(getOrderIndex());
				// 线的UIID唯一标识，放入的是主键ID （画图面板上的唯一标识，不代表数据库）
				vhLineData.setFlowElementUUID(flowStructureImageT.getFlowId().toString());
				// 线条类型
				if (flowStructureImageT.getBodyLine() != null) {
					vhLineData.setBodyStyle(Integer.valueOf(flowStructureImageT.getBodyLine()));
				}
				// 线条颜色
				String lineColor = flowStructureImageT.getLineColor();
				if (!DrawCommon.isNullOrEmtryTrim(lineColor)) {
					String[] backColors = lineColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					vhLineData.setBodyColor(newBackColor);
				}
				// 线条宽带
				if (flowStructureImageT.getLineThickness() != null) {
					int lineWidth = flowStructureImageT.getLineThickness();
					vhLineData.setBodyWidth(lineWidth);
				}
				// 判断如果为生成模板的数据
				if (isOpenMode) {
					vhLineData.setFlowElementId(-1);
				}
				// 生成竖分割线
				VerticalLine verticalLine = new VerticalLine(vhLineData);
				JecnAddFlowElementUnit.addVHLine(verticalLine, new Point((int) pointX, 0));
				// 把线的克隆数据放入内存中
				JecnVHLineData vHLineDataClone = vhLineData.clone();
				vHLineDataClone.setFlowElementId(vhLineData.getFlowElementId());
				vHLineDataClone.setUUID(vhLineData.getUUID());
				getLineDataList().add(vHLineDataClone);
				// 设置分割线层级
				verticalLine.reSetFlowElementZorder();
			}
		} else if ((MapElemType.HDividingLine.toString().equals(flowStructureImageT.getFigureType()))
				|| (MapElemType.VDividingLine.toString().equals(flowStructureImageT.getFigureType()))
				|| (MapElemType.DividingLine.toString().equals(flowStructureImageT.getFigureType()))) { // 横线,竖线,分割线
			addDividingLineData(flowStructureImageT, getOrderIndex(), isOpenMode);
		} else {
			getFigureImagerData(mapFigureData, baseFlowElementList, getOrderIndex(), processFigureData, isOpenMode);
		}

	}

	/**
	 * * 设置图形数据
	 * 
	 * @param flowStructureImageT
	 *            服务图形数据的bean
	 * @param baseFlowElementList
	 *            新建存储转换图形的List
	 * @param processFigureData
	 *            流程图属性数据
	 * @param orderIndex
	 *            图形的层级
	 */
	public static void getFigureImagerData(ProcessMapFigureData mapFigureData,
			List<JecnBaseFlowElementPanel> baseFlowElementList, int orderIndex, ProcessFigureData processFigureData,
			boolean isOpenMode) {
		JecnFlowStructureImageT flowStructureImageT = null;
		if (mapFigureData == null) {
			flowStructureImageT = processFigureData.getJecnFlowStructureImageT();
		} else if (processFigureData == null) {
			flowStructureImageT = mapFigureData.getJecnFlowStructureImageT();
		}
		// 图形标识
		String figureType = flowStructureImageT.getFigureType();
		// 创建图形数据对象
		JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.valueOf(figureType));
		figureData.setEleShape(StringUtils.isBlank(flowStructureImageT.getEleShape()) ? null : MapElemType
				.valueOf(flowStructureImageT.getEleShape()));
		// 图形主键
		figureData.setFlowElementId(flowStructureImageT.getFigureId());
		figureData.setUUID(flowStructureImageT.getUUID());
		// 图形UIID唯一标识，放入的是主键ID （画图面板上的唯一标识，不代表数据库）
		figureData.setFlowElementUUID(flowStructureImageT.getFigureId().toString());

		// 图形上添加的字符
		if (MapElemType.FreeText == figureData.getEleShape()) {
			figureData.setFigureText(flowStructureImageT.getActivityShow());
		} else {
			figureData.setFigureText(flowStructureImageT.getFigureText());
		}
		if (MapElemType.MapNameText == figureData.getMapElemType()) {
			// 名称文本框 记录 当前元素名称，保存时判断是否更新
			figureData.setOldNameText(flowStructureImageT.getFigureText());
			JecnFlowMapData flowMapData = getWorkflow().getFlowMapData();
			if (!flowMapData.getName().equals(figureData.getOldNameText())) {
				flowMapData.setSaveTrue();
			}
		}
		// 图形的X点
		int pointX = flowStructureImageT.getPoLongx().intValue();
		// 图形的Y点
		int pointY = flowStructureImageT.getPoLongy().intValue();

		// 图形的连接ID
		if (flowStructureImageT.getLinkFlowId() != null) {
			figureData.getDesignerFigureData().setLinkId(flowStructureImageT.getLinkFlowId());
			// 图形连接类型
			if (flowStructureImageT.getImplType() != 0) {
				figureData.getDesignerFigureData().setLineType(String.valueOf(flowStructureImageT.getImplType()));
			}
		}

		// 图形的位置点
		Point point = new Point(pointX, pointY);
		// 图形的宽度
		figureData.setFigureSizeX(flowStructureImageT.getWidth().intValue());
		// 图形的高度
		figureData.setFigureSizeY(flowStructureImageT.getHeight().intValue());
		// 字体的大小
		figureData.setFontSize(flowStructureImageT.getFontSize().intValue());
		// 图形的层级
		if (flowStructureImageT.getOrderIndex() != null) {
			figureData.setZOrderIndex(flowStructureImageT.getOrderIndex().intValue());
		}
		// 图形填充效果
		int figureFillEffects = flowStructureImageT.getFillEffects() == null ? 1 : flowStructureImageT.getFillEffects();
		if (0 == figureFillEffects) {
			figureData.setFillType(FillType.none);
		} else if (1 == figureFillEffects) {
			figureData.setFillType(FillType.one);
		} else {
			figureData.setFillType(FillType.two);
		}
		// 图形的填充颜色
		String figureFillCololr = flowStructureImageT.getBGColor();
		if (!DrawCommon.isNullOrEmtryTrim(figureFillCololr)) {
			String[] backColors = figureFillCololr.split(",");
			if (backColors.length == 3) {// 单色效果
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				figureData.setFillColor(newBackColor);
			} else if (backColors.length == 4) {// 双色效果+渐变方向
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				String colorChangType = backColors[3];
				if ("1".equals(colorChangType)) {
					// 向上倾斜
					figureData.setFillColorChangType(FillColorChangType.topTilt);
				} else if ("2".equals(colorChangType)) {
					// 向下倾斜
					figureData.setFillColorChangType(FillColorChangType.bottom);
				} else if ("3".equals(colorChangType)) {
					// 垂直
					figureData.setFillColorChangType(FillColorChangType.vertical);
				} else {
					// 水平
					figureData.setFillColorChangType(FillColorChangType.horizontal);
				}
				figureData.setFillColor(newBackColor);
			} else if (backColors.length == 7) {// 双色效果+渐变方向
				Color oneBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				Color twoBackColor = new Color(Integer.parseInt(backColors[3]), Integer.parseInt(backColors[4]),
						Integer.parseInt(backColors[5]));
				figureData.setFillColor(oneBackColor);
				figureData.setChangeColor(twoBackColor);

				String colorChangType = backColors[6];
				if ("1".equals(colorChangType)) {
					// 向上倾斜
					figureData.setFillColorChangType(FillColorChangType.topTilt);
				} else if ("2".equals(colorChangType)) {
					// 向下倾斜
					figureData.setFillColorChangType(FillColorChangType.bottom);
				} else if ("3".equals(colorChangType)) {
					// 垂直
					figureData.setFillColorChangType(FillColorChangType.vertical);
				} else {
					// 水平
					figureData.setFillColorChangType(FillColorChangType.horizontal);
				}

			}
		}
		// 字体颜色
		String fontColor = flowStructureImageT.getFontColor();
		if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
			String[] backColors = fontColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setFontColor(newBackColor);
		}

		// 字体是否加粗0为正常，1为加粗，2为斜体
		int fontBody = flowStructureImageT.getFontBody().intValue();
		figureData.setFontStyle(fontBody);
		// 字体类型:默认为（宋体）
		String fontType = flowStructureImageT.getFontType();
		figureData.setFontName(fontType);
		// 字体位置
		figureData.setFontPosition(Integer.valueOf(flowStructureImageT.getFontPosition()));
		// 是否存在阴影
		Long favaShadow = flowStructureImageT.getHavaShadow();
		if (favaShadow.intValue() == 1) {
			figureData.setShadowsFlag(true);
		} else {
			figureData.setShadowsFlag(false);
		}
		// 阴影颜色
		String shadowColor = flowStructureImageT.getShadowColor();
		if (!DrawCommon.isNullOrEmtryTrim(shadowColor)) {
			String[] backColors = shadowColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setShadowColor(newBackColor);
		}

		// 旋转角度值
		figureData.setCurrentAngle(Double.valueOf(flowStructureImageT.getCircumgyrate()));
		// 边框样式
		figureData.setBodyStyle(Integer.valueOf(flowStructureImageT.getBodyLine()));
		// 边框宽度
		if (flowStructureImageT.getLineThickness() != null) {
			figureData.setBodyWidth(flowStructureImageT.getLineThickness());
		}
		// 边框颜色
		String bodyColor = flowStructureImageT.getBodyColor();
		if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
			String[] backColors = bodyColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			figureData.setBodyColor(newBackColor);
		}
		// 是否显示3D
		Integer figure3D = flowStructureImageT.getIs3DEffect();
		figureData.setFlag3D(figure3D != null && figure3D == 1 ? true : false);
		// 判断是否为活动
		if (DrawCommon.isActive(MapElemType.valueOf(figureType))) {
			JecnActiveData activeData = (JecnActiveData) figureData;
			// 活动编号
			String activityId = flowStructureImageT.getActivityId();
			activeData.setActivityNum(activityId);
			// 活动说明
			String activityShow = flowStructureImageT.getActivityShow();
			activeData.setAvtivityShow(activityShow);
			// 关键活动
			String avtivityShowAndControl = flowStructureImageT.getRoleRes();
			activeData.setAvtivityShowAndControl(avtivityShowAndControl);
			// 关键活动类型
			activeData.setActiveKeyType(flowStructureImageT.getLineColor());
			// 对应内控矩阵风险点
			activeData.setInnerControlRisk(flowStructureImageT.getInnerControlRisk());
			// 对应标准条款
			activeData.setStandardConditions(flowStructureImageT.getStandardConditions());
			// 活动类型ID
			activeData.setActiveTypeId(flowStructureImageT.getActiveTypeId());
			// 是否线上
			activeData.setIsOnLine(flowStructureImageT.getIsOnLine());
			// 输入说明
			activeData.setActivityInput(flowStructureImageT.getActivityInput());
			// 说出说明
			activeData.setActivityOutput(flowStructureImageT.getActivityOutput());

			// 时间轴
			activeData.setTargetValue(flowStructureImageT.getTargetValue());
			activeData.setStatusValue(flowStructureImageT.getStatusValue());
			activeData.setExplain(flowStructureImageT.getExplain());
			activeData.setCustomOne(flowStructureImageT.getCustomOne());
			// 活动时间类型
			if (flowStructureImageT.getTimeType() != null) {
				int avtivityTimeType = flowStructureImageT.getTimeType();
				activeData.setAvtivityTimeType(avtivityTimeType);
				// 活动开始时间
				String avtivityStartTime = flowStructureImageT.getStartTime();
				activeData.setAvtivityStartTime(avtivityStartTime);
				// 活动结束时间
				String avtivityStopTime = flowStructureImageT.getStopTime();
				activeData.setAvtivityStopTime(avtivityStopTime);
			}

			if (processFigureData != null) {
				// 活动输出数据
				if (processFigureData.getListModeFileT() != null) {
					List<JecnModeFileT> modeFileTList = JecnSaveProcess.switchServerJecnModeFileT(processFigureData
							.getListModeFileT());
					activeData.setListModeFileT(modeFileTList);
				}
				// 活动的输入数据和操作规范
				if (processFigureData.getListJecnActivityFileT() != null) {
					List<JecnActivityFileT> activityFileTList = JecnSaveProcess
							.switchServerActivityFileT(processFigureData.getListJecnActivityFileT());
					activeData.setListJecnActivityFileT(activityFileTList);
				}
				// 活动指标数据
				if (processFigureData.getListRefIndicatorsT() != null) {
					List<JecnRefIndicatorsT> refIndicatorsTList = JecnSaveProcess
							.switchServerJecnRefIndicatorsT(processFigureData.getListRefIndicatorsT());
					activeData.setListRefIndicatorsT(refIndicatorsTList);
				}
				// 活动线上信息
				if (processFigureData.getListJecnActiveOnLineTBean() != null) {
					List<JecnActiveOnLineTBean> onLineTBeanList = JecnSaveProcess
							.switchServerJecnActiveOnLineTBean(processFigureData.getListJecnActiveOnLineTBean());
					activeData.setListJecnActiveOnLineTBean(onLineTBeanList);
				}
				// 活动关联标准集合
				if (processFigureData.getListJecnActiveStandardBeanT() != null) {
					List<JecnActiveStandardBeanT> standardBeanTList = JecnSaveProcess
							.switchServerJecnActiveStandardBeanT(processFigureData.getListJecnActiveStandardBeanT());
					activeData.setListJecnActiveStandardBeanT(standardBeanTList);
				}
				if (processFigureData != null) {// 控制点信息
					activeData.setJecnControlPointTList(JecnSaveProcess
							.serverJecnActiveControlPointTList(processFigureData.getListControlPoint()));
				}
				// 新版 活动输入
				if (processFigureData.getFigureInList() != null) {
					List<JecnFigureInoutT> figureInoutTs = JecnSaveProcess
							.switchServerJecnFigureInoutT(processFigureData.getFigureInList());
					activeData.setListFigureInTs(figureInoutTs);
				}
				// 新版活动输出
				if (processFigureData.getFigureOutList() != null) {
					List<JecnFigureInoutT> figureInoutTs = JecnSaveProcess
							.switchServerJecnFigureInoutT(processFigureData.getFigureOutList());
					activeData.setListFigureOutTs(figureInoutTs);
				}
			}

		} else if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			// 获取连接线数据对象
			JecnBaseDivedingLineData lineData = new JecnBaseDivedingLineData(MapElemType.CommentLine);
			// 注释框小线段开始点
			int startX = 0;
			if (flowStructureImageT.getStartFigure() != null) {
				startX = flowStructureImageT.getStartFigure().intValue();
			}
			// 注释框小线段开始点
			int startY = 0;
			if (flowStructureImageT.getEndFigure() != null) {
				startY = flowStructureImageT.getEndFigure().intValue();
			}
			Point startPoint = new Point(startX, startY);
			// 注释框结束点
			Point endPoint = reCommonLineEPoint(flowStructureImageT.getPoLongx().intValue(), flowStructureImageT
					.getPoLongy().intValue(), Double.valueOf(flowStructureImageT.getCircumgyrate()),
					flowStructureImageT.getWidth().intValue(), flowStructureImageT.getHeight().intValue());
			// 设置线段大小 记录线段原始状态下数值
			lineData.getDiviLineCloneable().initOriginalArributs(startPoint, endPoint, figureData.getBodyWidth(),
					getWorkflow().getWorkflowScale());
			commentData.setLineData(lineData);

		} else if (DrawCommon.isRole(MapElemType.valueOf(figureType))) { // 判断是否为角色
			JecnRoleData roleData = (JecnRoleData) figureData;
			if (processFigureData != null) {
				// 角色与岗位关联
				if (processFigureData.getListRolePosition() != null) {
					List<JecnFlowStationT> rolePositionList = JecnSaveProcess
							.switchServerJecnFlowStationT(processFigureData.getListRolePosition());
					roleData.setFlowStationList(rolePositionList);

					// add 岗位名称和岗位组名称
					addPosAndGroupNames(roleData);
				}

			}
			// 主责岗位
			roleData.setActivityId(flowStructureImageT.getActivityId());
			// 角色职责
			roleData.setRoleRes(flowStructureImageT.getRoleRes());
		} else if (MapElemType.valueOf(figureType) == MapElemType.IconFigure) { // 图标插入框
			figureData.getDesignerFigureData().setLinkId(flowStructureImageT.getLinkFlowId());
		} else if (MapElemType.valueOf(figureType) == MapElemType.ModelFigure) { // 泳池
			JecnModeFigureData modeFigureData = (JecnModeFigureData) figureData;
			modeFigureData.setDividingX(flowStructureImageT.getDividingX());
		} else if (MapElemType.valueOf(figureType) == MapElemType.ToFigure
				|| MapElemType.valueOf(figureType) == MapElemType.FromFigure) {// TO元素或者From元素
			if (processFigureData != null) {
				JecnToFromRelatedData relatedData = (JecnToFromRelatedData) figureData;
				relatedData.setRelatedId(processFigureData.getRelatedId());
				relatedData.setRelatedUUId(processFigureData.getRelatedId() != null ? String.valueOf(processFigureData
						.getRelatedId()) : null);
			}

		} else if (DrawCommon.isImplFigure(MapElemType.valueOf(figureType))) {// 是否是接口
			JecnImplData implData = (JecnImplData) figureData;
			implData.setImplName(flowStructureImageT.getImplName());
			implData.setImplNote(flowStructureImageT.getImplNote());
			implData.setProcessRequirements(flowStructureImageT.getProcessRequirements());
		} else if (DrawCommon.isTermFigure(MapElemType.valueOf(figureType))) {// 术语
			TermFigureData termFigureData = (TermFigureData) figureData;
			termFigureData.setTermDefine(processFigureData.getTermDefine());
		}
		// 生成图形
		JecnBaseFigurePanel baseFigureElement = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowFigure(MapElemType
				.valueOf(figureType), figureData);
		if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
			CommentText commentText = (CommentText) baseFigureElement;
			// 默认算法 取数据层
			commentText.isDefault();
		}

		// 初始化图标元素
		initIconFile(baseFigureElement);
		/********** 【流程地图关联文件】 ***********/
		initFigureFileMapData(mapFigureData, figureData, flowStructureImageT);
		/********** 【流程地图关联文件】 ***********/

		/** 【流程管理附件数据】 */
		initFigureFileData(processFigureData, figureData, flowStructureImageT);

		// 增加链接
		JecnDesignerCommon.addElemLink(baseFigureElement, getWorkflow().getFlowMapData().getMapType());
		// 判断如果为生成模板的数据
		if (isOpenMode) {
			baseFigureElement.getFlowElementData().setFlowElementId(-1);
		}
		baseFlowElementList.add(baseFigureElement);
		// 设置层级
		baseFigureElement.getFlowElementData().setZOrderIndex(orderIndex);
		JecnAddFlowElementUnit.addFigure(baseFigureElement);
		// 设置图形的位置
		baseFigureElement.setSizeAndLocation(point);
		// 给面板内存数据赋值
		getFigureDataList().add(JecnDesignerImpl.getCloneData(figureData));
	}

	private static void initIconFile(JecnBaseFigurePanel baseFigureElement) {
		if (!(baseFigureElement instanceof IconFigure)) {
			return;
		}
		JecnFigureData figureData = baseFigureElement.getFlowElementData();
		if (figureData.getDesignerFigureData().getLinkId() != null) {
			FileOpenBean fileOpenBean = null;
			try {
				fileOpenBean = ConnectionPool.getFileAction().openFile(figureData.getDesignerFigureData().getLinkId());
			} catch (Exception e) {
				log.error("JecnProcess is error", e);
			}
			if (fileOpenBean != null) {
				String path = JecnUtil.downFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
				IconFigure iconFigure = null;

				iconFigure = (IconFigure) baseFigureElement;
				if (path != null) {
					java.awt.Image img = Toolkit.getDefaultToolkit().getImage(path);
					iconFigure.setImg(img);
					iconFigure.repaint();
				}
			}
		}
	}

	private static void addPosAndGroupNames(JecnRoleData roleData) {
		if (!roleData.getFlowStationList().isEmpty()) {
			String posGropName = "";
			String posName = "";
			for (JecnFlowStationT jecnFlowStationT : roleData.getFlowStationList()) {
				if ("0".equals(jecnFlowStationT.getType())) {
					posName += jecnFlowStationT.getStationName() + "/";
				} else {
					posGropName += jecnFlowStationT.getStationName() + "/";
				}
			}
			if (StringUtils.isNotBlank(posName)) {
				posName = posName.substring(0, posName.length() - 1);
				roleData.setPosName(posName);
			}
			if (StringUtils.isNotBlank(posGropName)) {
				posGropName = posGropName.substring(0, posGropName.length() - 1);
				roleData.setPosGroupName(posGropName);
			}
		}
	}

	/**
	 * 获取地图带连接或者关联文件的数据 (地图数据)
	 * 
	 * @param mapFigureData
	 *            服务获取流程地图对象关联文件数据
	 * @param figureData
	 *            设计端元素对象数据层
	 * @param flowStructureImageT
	 *            流程数据库数据
	 */
	private static void initFigureFileMapData(ProcessMapFigureData mapFigureData, JecnFigureData figureData,
			JecnFlowStructureImageT flowStructureImageT) {
		if (mapFigureData == null) {
			return;
		}
		if (DrawCommon.isInfoFigure(figureData.getMapElemType())) {// 能发添加附件
			figureData.getListFigureFileTBean().addAll(
					JecnSaveProcess.switchServerFigureFileTBean(mapFigureData.getListJecnFigureFileTBean()));
			// 说明
			figureData.setAvtivityShow(flowStructureImageT.getActivityShow());
		}
	}

	/**
	 * 流程关联文件数据获取
	 * 
	 * @param processFigureData
	 *            服务获取流程对象关联文件数据
	 * @param figureData
	 *            设计端元素对象数据层
	 * @param flowStructureImageT
	 *            流程数据库数据
	 */
	private static void initFigureFileData(ProcessFigureData processFigureData, JecnFigureData figureData,
			JecnFlowStructureImageT flowStructureImageT) {
		if (processFigureData == null) {
			return;
		}
		if (figureData.getMapElemType().equals(MapElemType.FileImage)) {// FileImage
			// 关联文件
			figureData.getListFigureFileTBean().addAll(
					JecnSaveProcess.switchServerFigureFileTBean(processFigureData.getListJecnFigureFileTBean()));
		}
	}

	/**
	 * 
	 * 获取小线段结束点
	 * 
	 * @param localX
	 *            注释框位置点X坐标
	 * @param localY
	 *            注释框位置点Y坐标
	 * @param currentAngle
	 *            旋转角度
	 * @param width
	 *            注释框宽度
	 * @param height
	 *            注释框高度
	 * @return
	 */
	public static Point reCommonLineEPoint(int localX, int localY, double currentAngle, int width, int height) {
		// 换算结束点位置
		Point endPoint = new Point();
		if (currentAngle == 0.0) {
			endPoint = new Point(localX, localY + height / 2);
		} else if (currentAngle == 90.0) {
			endPoint = new Point(localX + width / 2, localY);
		} else if (currentAngle == 180.0) {
			endPoint = new Point(localX + width, localY + height / 2);
		} else if (currentAngle == 270.0) {
			endPoint = new Point(localX + width / 2, localY + height);
		}
		// 设置分割线结束点
		return endPoint;
	}

	/**
	 * 连接线
	 * 
	 * @param lineDataMap
	 *            存储线和线的层级
	 * @param baseFlowElementList
	 *            存储图形的集合
	 */
	public static void getLineData(Map<JecnFlowStructureImageT, Integer> lineDataMap,
			List<JecnBaseFlowElementPanel> baseFlowElementList, boolean isOpenMode) {
		Iterator<JecnFlowStructureImageT> iterator = lineDataMap.keySet().iterator();
		while (iterator.hasNext()) {
			JecnFlowStructureImageT flowStructureImageT = iterator.next();
			// 获取线的标识
			String figureType = flowStructureImageT.getFigureType();
			// 获取连接线的数据对象
			JecnManhattanLineData manhattanLineData = new JecnManhattanLineData(MapElemType.valueOf(figureType));
			// 线的主键
			if (flowStructureImageT.getFigureId() != null) {
				manhattanLineData.setFlowElementId(flowStructureImageT.getFigureId());
				manhattanLineData.setUUID(flowStructureImageT.getUUID());
			}

			// 线的宽度
			if (flowStructureImageT.getLineThickness() != null) {
				manhattanLineData.setBodyWidth(flowStructureImageT.getLineThickness());
			}
			// 线的类型
			manhattanLineData.setBodyStyle(Integer.valueOf(flowStructureImageT.getBodyLine() == null ? "0"
					: flowStructureImageT.getBodyLine()));
			// 字体颜色
			String fontColor = flowStructureImageT.getFontColor();
			if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
				String[] backColors = fontColor.split(",");
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				manhattanLineData.setFontColor(newBackColor);
			}

			// 字体加粗样式
			if (flowStructureImageT.getFontBody() != null) {
				manhattanLineData.setFontStyle(flowStructureImageT.getFontBody().intValue());
			}
			// 字体类型
			if (flowStructureImageT.getFontType() != null) {
				manhattanLineData.setFontName(flowStructureImageT.getFontType());
			}
			// 字体大小
			if (flowStructureImageT.getFontSize() != null) {
				manhattanLineData.setFontSize(flowStructureImageT.getFontSize().intValue());
			}
			// 图形的开始UIID
			if (flowStructureImageT.getStartFigure() != null) {
				manhattanLineData.setStartFigureUUID(flowStructureImageT.getStartFigure().toString());
				// 图形的开始ID
				manhattanLineData.setStartId(flowStructureImageT.getStartFigure());
			}
			// 图形的结束UIID
			if (flowStructureImageT.getEndFigure() != null) {
				manhattanLineData.setEndFigureUUID(flowStructureImageT.getEndFigure().toString());
				// 图形的结束ID
				manhattanLineData.setEndId(flowStructureImageT.getEndFigure());
			}

			// 获取开始点的编辑点
			int poLongx = flowStructureImageT.getPoLongx().intValue();
			EditPointType startEditPointType = DrawCommon.getLineType(poLongx);
			manhattanLineData.setStartEditPointType(startEditPointType);
			// 获取结束点的编辑点
			int poLongy = flowStructureImageT.getPoLongy().intValue();
			EditPointType editPointType = DrawCommon.getLineType(poLongy);
			manhattanLineData.setEndEditPointType(editPointType);

			// 线段颜色
			String bodyColor = flowStructureImageT.getLineColor();
			if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
				String[] backColors = bodyColor.split(",");
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				manhattanLineData.setBodyColor(newBackColor);
			}

			// 给线段赋值
			manhattanLineData.setFigureText(flowStructureImageT.getFigureText());
			// 获取层级
			int orderIndex = lineDataMap.get(flowStructureImageT);
			manhattanLineData.setZOrderIndex(orderIndex);

			// 连接线是否显示双箭头
			manhattanLineData.setTwoArrow(flowStructureImageT.getIsOnLine() == 1 ? true : false);
			// 根据标识位获取连接线对象
			JecnBaseManhattanLinePanel baseManhattanLine = JecnPaintFigureUnit.getJecnBaseManhattanLinePanel(
					figureType, manhattanLineData);

			JecnFlowImport flowImport = new JecnFlowImport();
			// 设置连接线的开始图形和结束图形
			flowImport.setManLineRefFigure(baseFlowElementList, baseManhattanLine);

			// 获取连接线数据对象
			List<JecnManhattanRouterResultData> routerResultList = new ArrayList<JecnManhattanRouterResultData>();

			Map<JecnManhattanRouterResultData, Long> map = new HashMap<JecnManhattanRouterResultData, Long>();
			if (flowStructureImageT.getListLineSegmet() != null) {
				// 获取小线段的数据
				for (JecnLineSegmentT lineSegmentT : flowStructureImageT.getListLineSegmet()) {
					Point startPoint = new Point(lineSegmentT.getStartX().intValue(), lineSegmentT.getStartY()
							.intValue());
					Point endPoint = new Point(lineSegmentT.getEndX().intValue(), lineSegmentT.getEndY().intValue());
					// 收集小线段数据
					JecnManhattanRouterResultData data = new JecnManhattanRouterResultData();
					data.setStartPoint(startPoint);
					data.setEndPint(endPoint);
					data.setUUID(lineSegmentT.getUUID());
					routerResultList.add(data);

					if (!isOpenMode) {
						map.put(data, lineSegmentT.getFigureId());
					}
				}
			}
			if (routerResultList == null) {
				return;
			}
			// 排序
			routerResultList = baseManhattanLine.reOrderLinePoint(routerResultList);
			for (JecnManhattanRouterResultData data : routerResultList) {
				JecnLineSegmentData lineSegmentData = baseManhattanLine.addLineSegment(data.getStartPoint(), data
						.getEndPint());
				if (!isOpenMode) {
					Long figureId = map.get(data);
					lineSegmentData.setFlowLineID(figureId);
					lineSegmentData.setUUID(lineSegmentData.getUUID());
				}
			}
			// 判断如果为生成模板的数据
			if (isOpenMode) {
				baseManhattanLine.getFlowElementData().setFlowElementId(-1);
				baseManhattanLine.getFlowElementData().setStartId(-1);
				baseManhattanLine.getFlowElementData().setEndId(-1);
			}
			if (StringUtils.isNotBlank(flowStructureImageT.getFigureText()) && flowStructureImageT.getWidth() != null
					&& flowStructureImageT.getWidth() != 0) {
				manhattanLineData.setTextPoint(new Point(flowStructureImageT.getWidth().intValue(), flowStructureImageT
						.getHeight().intValue()));
			}
			// 面板添加流程元素
			JecnAddFlowElementUnit.addImportManLine(baseManhattanLine);
			// 添加数据到线段显示
			baseManhattanLine.insertTextToLine();
			// 把线的克隆数据放入内存中
			JecnManhattanLineData manhattanLineDataClone = manhattanLineData.clone();
			manhattanLineDataClone.setFlowElementId(baseManhattanLine.getFlowElementData().getFlowElementId());
			manhattanLineDataClone.setUUID(baseManhattanLine.getFlowElementData().getUUID());
			getLineDataList().add(manhattanLineDataClone);
		}
	}

	/**
	 * 获取直线，斜线，横线的数据
	 * 
	 * @param flowStructureImageT
	 *            线数据的数据库bean
	 */
	public static void addDividingLineData(JecnFlowStructureImageT flowStructureImageT, int orderIndex,
			boolean isOpenMode) {
		// 线标识
		String lineType = flowStructureImageT.getFigureType();
		// 创建线
		JecnBaseDivedingLineData baseDivedingLineData = new JecnBaseDivedingLineData(MapElemType.valueOf(lineType));
		if (baseDivedingLineData == null || flowStructureImageT.getStartFigure() == null
				|| flowStructureImageT.getEndFigure() == null || flowStructureImageT.getPoLongx() == null
				|| flowStructureImageT.getPoLongy() == null) {
			return;
		}
		// 线主键
		baseDivedingLineData.setFlowElementId(flowStructureImageT.getFigureId());
		baseDivedingLineData.setUUID(flowStructureImageT.getUUID());
		// 开始点X
		int startX = Integer.valueOf(flowStructureImageT.getStartFigure().toString());
		// 开始点Y
		int startY = Integer.valueOf(flowStructureImageT.getEndFigure().toString());
		// 结束点X
		int endX = Integer.valueOf(flowStructureImageT.getPoLongx().toString());
		// 结束点Y
		int endY = Integer.valueOf(flowStructureImageT.getPoLongy().toString());
		// 线条颜色
		String lineColor = flowStructureImageT.getLineColor();
		if (!DrawCommon.isNullOrEmtryTrim(lineColor)) {
			String[] backColors = lineColor.split(",");
			Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]), Integer
					.parseInt(backColors[2]));
			baseDivedingLineData.setBodyColor(newBackColor);
		}
		// 线条宽带
		if (flowStructureImageT.getLineThickness() != null) {
			int lineWidth = flowStructureImageT.getLineThickness();
			baseDivedingLineData.setBodyWidth(lineWidth);
		}
		// 线的层级
		baseDivedingLineData.setZOrderIndex(orderIndex);
		// 线的类型
		baseDivedingLineData.setBodyStyle(Integer.valueOf(flowStructureImageT.getBodyLine()));
		Point startPoint = new Point(startX, startY);
		Point endPoint = new Point(endX, endY);
		// //适应visio导出添加UUID 以前是流程ID
		baseDivedingLineData.setFlowElementUUID(DrawCommon.getUUID());
		// 获取线
		JecnBaseDividingLinePanel dividingLinePanel = getJecnBaseDiviLinePanel(baseDivedingLineData);
		// 横线，竖线
		if (dividingLinePanel instanceof HDividingLine || dividingLinePanel instanceof VDividingLine) {
			if (MapElemType.HDividingLine == baseDivedingLineData.getMapElemType()) {// 如果是横线，纵坐标为线宽
				// 图形的宽度
				baseDivedingLineData.setFigureSizeX(Math.abs(startX - endX));
				// 图形的高度
				baseDivedingLineData.setFigureSizeY(flowStructureImageT.getLineThickness());
			} else {// 如果是纵线，横坐标为线宽
				// 图形的宽度
				baseDivedingLineData.setFigureSizeX(flowStructureImageT.getLineThickness());
				// 图形的高度
				baseDivedingLineData.setFigureSizeY(Math.abs(startY - endY));
			}
			// 记录100%状态下位置点
			dividingLinePanel.reSetAttributes(startPoint, endPoint);
			// 设置图形位置点
			dividingLinePanel.setBounds(startPoint, endPoint);
		}
		if (dividingLinePanel instanceof DividingLine) {// 如果添加的是分割线，记录分割线鼠标点击和释放的坐标值
			DividingLine dividingLine = (DividingLine) dividingLinePanel;
			// 记录分割线100%状态下的克隆值
			dividingLine.originalCloneData(startPoint, endPoint);
		}
		// 判断如果为生成模板的数据
		if (isOpenMode) {
			baseDivedingLineData.setFlowElementId(-1);
		}
		// 把线的克隆数据放入内存中
		JecnBaseDivedingLineData baseDivedingLineDataClone = baseDivedingLineData.clone();
		baseDivedingLineDataClone.setFlowElementId(baseDivedingLineData.getFlowElementId());
		baseDivedingLineDataClone.setUUID(baseDivedingLineData.getUUID());
		getLineDataList().add(baseDivedingLineDataClone);

		JecnAddFlowElementUnit.importAddDiviLine((JecnBaseDividingLinePanel) dividingLinePanel);
		// 设置线层级
		dividingLinePanel.reSetFlowElementZorder();
	}

	/**
	 * 
	 * 流程地图给定参数按照序号排序
	 * 
	 * @param itemBeanList
	 */
	public static void sortAllFlowStructureImageList(List<ProcessMapFigureData> mapDataList) {

		if (mapDataList == null || mapDataList.size() == 0) {
			return;
		}
		Collections.sort(mapDataList, new Comparator<ProcessMapFigureData>() {
			public int compare(ProcessMapFigureData r1, ProcessMapFigureData r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("JecnProcess sortAllFlowStructureImageList is error");
				}

				Long s1 = r1.getJecnFlowStructureImageT().getOrderIndex();
				int sortId1 = s1 == null ? 0 : s1.intValue();

				Long s2 = r2.getJecnFlowStructureImageT().getOrderIndex();
				int sortId2 = s2 == null ? 0 : s2.intValue();
				// int sortId1 = r1.getOrderIndex().intValue();
				// int sortId2 = r2.getOrderIndex().intValue();
				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 
	 * 流程图给定参数按照序号排序
	 * 
	 * @param itemBeanList
	 */
	public static void sortAllProcessFigureDataList(List<ProcessFigureData> processFigureDataList) {
		if (processFigureDataList == null || processFigureDataList.size() == 0) {
			return;
		}
		Collections.sort(processFigureDataList, new Comparator<ProcessFigureData>() {
			public int compare(ProcessFigureData r1, ProcessFigureData r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("JecnProcess sortAllProcessFigureDataList is error");
				}
				Long s1 = r1.getJecnFlowStructureImageT().getOrderIndex();
				int sortId1 = s1 == null ? 0 : s1.intValue();

				Long s2 = r2.getJecnFlowStructureImageT().getOrderIndex();
				int sortId2 = s2 == null ? 0 : s2.intValue();

				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 获取面板最大层级，并把层级数加一
	 * 
	 * @return 面板最大层级
	 */
	public static int getOrderIndex() {
		if (getWorkflow() == null) {
			return -1;
		}
		// 获取最大层级
		int orderIndex = getWorkflow().getMaxZOrderIndex();
		// 最大层级加一
		getWorkflow().setMaxZOrderIndex(orderIndex + 2);
		return orderIndex;
	}

	/**
	 * 根据标识位获取连接线对象
	 * 
	 * @param lineFlag
	 * @param manhattanLineData
	 * @return
	 */
	private static JecnBaseDividingLinePanel getJecnBaseDiviLinePanel(JecnBaseDivedingLineData diviLineData) {
		switch (diviLineData.getMapElemType()) {
		case DividingLine:// 分隔符（分割线）
			return new DividingLine(diviLineData);
		case HDividingLine:// 分隔符（分割线）
			return new HDividingLine(diviLineData);
		case VDividingLine:// 分隔符（分割线）
			return new VDividingLine(diviLineData);
		}
		return null;
	}

	/**
	 * 获取面板
	 * 
	 * @return 画图面板
	 */
	public static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 获取保存图形的集合
	 * 
	 * @return 保存图形的集合
	 */
	public static List<JecnFigureData> getFigureDataList() {
		if (getWorkflow() == null) {
			return null;
		}
		return getWorkflow().getFlowMapData().getDesignerData().getOldFigureList();
	}

	/**
	 * 获取保存线的集合
	 * 
	 * @return 保存线的集合
	 */
	public static List<JecnBaseLineData> getLineDataList() {
		if (getWorkflow() == null) {
			return null;
		}
		return getWorkflow().getFlowMapData().getDesignerData().getOldLineList();
	}

	/**
	 * 流程责任人查询
	 * 
	 * @param searchPopup
	 * @param searchField
	 * @param peopleType
	 * @return
	 */
	public static List<JecnTreeBean> resPeopleFieldSearch(JecnSearchPopup searchPopup, JSearchTextField searchField,
			Long peopleType) {
		List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
		try {
			String name = searchField.getText().trim();
			if (!"".equals(name)) {
				if (searchPopup.isCanSearch()) {
					if (peopleType.intValue() == 0) {
						listJecnTreeBean = ConnectionPool.getPersonAction().searchUserByName(name,
								JecnConstants.projectId);
					} else if (peopleType.intValue() == 1) {
						listJecnTreeBean = ConnectionPool.getOrganizationAction().searchPositionByName(name,
								JecnConstants.projectId);
					}
					searchPopup.setTableData(listJecnTreeBean);
				}
				searchPopup.setCanSearch(true);
			}

		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			log.error("JecnProcess resPeopleFieldSearch is error", e);
		}
		return listJecnTreeBean;
	}

	/***************************************************************************
	 * 流程责任部门查询
	 */
	public static List<JecnTreeBean> departFieldSearch(JecnSearchPopup departSearchPopup, JSearchTextField departField) {
		List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
		try {

			String name = departField.getText().trim();
			if (!"".equals(name)) {
				if (departSearchPopup.isCanSearch()) {
					listJecnTreeBean = ConnectionPool.getOrganizationAction().searchByName(name,
							JecnConstants.projectId);
					departSearchPopup.setTableData(listJecnTreeBean);
				}
				departSearchPopup.setCanSearch(true);
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			log.error("JecnProcess departFieldSearch is error", e);
		}
		return listJecnTreeBean;
	}

	/**
	 * 流程责任人 类型
	 * 
	 * @return
	 */
	public static Long getPeopleType() {
		try {
			return Long.valueOf(ConnectionPool.getConfigAciton().selectValue(1, "basicDutyOfficer"));
		} catch (Exception e) {
			log.error("JecnProcess getPeopleType is error", e);
		}
		return 0L;
	}
}
