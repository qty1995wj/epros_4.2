package com.jecn.epros.mapper;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.propose.*;
import com.jecn.epros.sqlprovider.ProposeSqlProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ProposeMapper {

    @SelectProvider(type = ProposeSqlProvider.class, method = "getProposeNum")
    public Integer getProposeNum(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "fetchProposes")
    @ResultMap("mapper.Propose.proposesResult")
    public List<Propose> fetchProposes(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "fetchProposeRelatedFlows")
    @ResultMap("mapper.Propose.proposesRelatedResult")
    public List<ProposeRelated> fetchProposeRelatedFlows(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "fetchProposeRelatedRules")
    @ResultMap("mapper.Propose.proposesRelatedResult")
    public List<ProposeRelated> fetchProposeRelatedRules(Map<String, Object> paramMap);

    @Select("select * from jecn_rtnl_propose where id=#{proposeId}")
    @ResultMap("mapper.Propose.proposesResult")
    public Propose getPropose(String proposeId);

    @UpdateProvider(type = ProposeSqlProvider.class, method = "updatePropose")
    public void updatePropose(Propose propose);

    @InsertProvider(type = ProposeSqlProvider.class, method = "savePropose")
    public void savePropose(Propose propose);

    @InsertProvider(type = ProposeSqlProvider.class, method = "saveProposeFile")
    public void saveProposeFile(ProposeFile proposeFile);

    @Select("select count(*) from propose_topic where RELATED_ID=#{id} and type=#{type}")
    @ResultType(Integer.class)
    public int topicIsExist(Map<String, Object> paramMap);

    @InsertProvider(type = ProposeSqlProvider.class, method = "saveProposeTopic")
    public void saveProposeTopic(ProposeTopic topic);

    @Delete("delete from JECN_RTNL_PROPOSE_FILE where propose_id=#{proposeId}")
    public void deleteProposeFileByProposeId(String proposeId);

    @Select("select * from JECN_RTNL_PROPOSE_FILE where propose_id=#{proposeId}")
    @ResultMap("mapper.Propose.getProposeFile")
    public List<ProposeFile> getProposeFileByProposeId(String proposeId);

    @UpdateProvider(type = ProposeSqlProvider.class, method = "updateProposeFile")
    public void updateProposeFile(ProposeFile file);

    @Delete("update jecn_rtnl_propose set REPLY_CONTENT = '',REPLY_PERSON = -1,REPLY_TIME = '' where id = #{proposeId}")
    public void clearReyly(String proposeId);

    @SelectProvider(type = ProposeSqlProvider.class, method = "getRelated")
    @ResultType(String.class)
    public Map<String, Object> getRelated(Propose propose);


    @SelectProvider(type = ProposeSqlProvider.class, method = "fetchMyTopic")
    @ResultMap("mapper.Propose.proposeTopicWithTable")
    public List<ProposeTopic> fetchMyTopic(Map<String, Object> paramMap);

    @Update("update propose_topic set update_time=#{updateTime} where related_id=#{relatedId} and type=#{type}")
    public void updateProposeTopicUpdateTime(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "searchProposeTopic")
    @ResultMap("mapper.Propose.proposeTopicWithTable")
    public List<ProposeTopic> searchProposeTopic(Map<String, Object> paramMap);

    @Select("select * from propose_topic where related_Id=#{relatedId} and type=#{type}")
    @ResultMap("mapper.Propose.proposeTopicWithTable")
    public ProposeTopic getProposeTopic(Map<String, Object> paramMap);

    /**
     * 获得需要分配处理人的主题和未分配处理人的主题
     *
     * @param type0需要分配处理人的主题 1已经分配处理人的主题
     * @return
     */
    @SelectProvider(type = ProposeSqlProvider.class, method = "searchProposeTopicByType")
    @ResultMap("mapper.Propose.proposeTopicWithTable")
    public List<ProposeTopic> searchProposeTopicByType(Integer type);

    @SelectProvider(type = ProposeSqlProvider.class, method = "searchProposeTopicNumByType")
    public int searchProposeTopicNumByType(Integer type);

    @Update("update propose_topic set handle_people=#{peopleId} where type=#{relatedType} and related_id=#{relatedId}")
    public void updateProposeSetHandlePeople(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "searchProposeTopicNum")
    public int searchProposeTopicNum(Map<String, Object> paramMap);

    @Delete("delete from jecn_rtnl_propose where id=#{proposeId}")
    public void deletePropose(String proposeId);

    @Select("select count(1) from propose_topic where related_id=#{relatedId} and type=#{type} and handle_people=#{peopleId}")
    public int logingPeopleIsProposeHandlePeople(Map<String, Object> paramMap);

    /**
     * 合理化统计图表
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = ProposeSqlProvider.class, method = "findProposeTotalChart")
    @ResultMap("mapper.Propose.proposeTotalChartData")
    public List<ProposeTotalChartData> findProposeTotalChart(Map<String, Object> paramMap);

    /**
     * 合理化统计 内容
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = ProposeSqlProvider.class, method = "findProposeTotalData")
    @ResultMap("mapper.Propose.proposeTotalData")
    public List<ProposeTotalData> findProposeTotalData(Map<String, Object> paramMap);

    /**
     * 合理化统计 总数
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = ProposeSqlProvider.class, method = "findProposeTotal")
    @ResultType(Integer.class)
    public int findProposeTotal(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "getMaxHistoryId")
    Long getMaxHistoryId(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "getTopicHandler")
    @ResultMap("mapper.SystemManage.UserManageResult")
    JecnUser getTopicHandler(Map<String, Object> map);

    @Select("select count(*) from JECN_RTNL_PROPOSE where RELATION_ID=#{relatedId} and RELATION_TYPE=#{type}")
    int findProposeTotalByRelated(Map<String, Object> paramMap);

    @Delete("delete from PROPOSE_TOPIC where RELATED_ID=#{relatedId} and type=#{type}")
    void deleteProposeTopic(Map<String, Object> paramMap);

    @SelectProvider(type = ProposeSqlProvider.class, method = "getHandlePeopleSql")
    List<Long> getHandlePeopleSql(Propose propose);


}
