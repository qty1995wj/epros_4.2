package epros.draw.system;

import java.util.Properties;

import epros.draw.system.data.JecnSystemFileIOData;
import epros.draw.util.JecnFileUtil;

/**
 * 
 * 保存、导出等选择路径处理对象
 * 
 * @author ZHOUXY
 * 
 */
class JecnSystemFileIOProcess {
	/** 各种导出、保存路径记录对象 */
	private static JecnSystemFileIOData systemIOData = null;

	private static String getPath() {
		return "uploadFile.properties";
	}

	/**
	 * 
	 * 读取保存路径、导出路径
	 * 
	 * @return
	 */
	static JecnSystemFileIOData readSystemFileIOData() {
		if (systemIOData == null) {
			// 各种导出、保存路径记录对
			systemIOData = new JecnSystemFileIOData();
			// 读取配置文件数据
			readFilePathsByPropertiesFile();
		}
		return systemIOData;
	}

	/**
	 * 
	 * 写入操作路径
	 * 
	 * @return boolean 写入成功或无写入返回true；写入失败返回false
	 */
	static boolean writeSystemFileIODataToPropertyFile() {
		// 待写入的properties对象
		Properties properties = new Properties();

		// 数据对象
		JecnSystemFileIOData systemIOData = readSystemFileIOData();

		// 保存路径
		properties.setProperty("save.file.directory", systemIOData
				.getSaveFileDirectory());
		// 导出为图像路径
		properties.setProperty("save.image.directory", systemIOData
				.getSaveImageDirectory());

		// 下载流程操作说明
		properties.setProperty("download.flow.directory", systemIOData
				.getDownLoadOperationDir());
		//导入出路径
		properties.setProperty("save.visio.directory", systemIOData
				.getSaveVisioDirectory());
		//制度下载操作说明路径		
		properties.setProperty("save.ruleOp.directory", systemIOData
				.getSaveRuleOpDirectory());
		//文件管理路径
		properties.setProperty("save.fileManage.directory", systemIOData
				.getSaveFileManageDirectory());
		//流程文件下载路径
		properties.setProperty("save.flow.directory", systemIOData
				.getSaveFlowDirectory());
		//制度本地更新
		properties.setProperty("save.ruleupdate.directory", systemIOData
				.getSavaRuleUpdateDirectory()
				);
		// 执行写入操作
		boolean ret = JecnFileUtil.writeProperties(getPath(), properties, null);
		return ret;
	}

	/**
	 * 
	 * 从Properties文件中读取路径
	 * 
	 */
	private static void readFilePathsByPropertiesFile() {
		// 获取本地文件路径
		Properties properties = JecnFileUtil.readProperties(getPath());
		if (properties != null) {
			// 保存路径
			String saveFileDirectory = properties
					.getProperty("save.file.directory");
			// 导出为图像路径
			String saveImageDirectory = properties
					.getProperty("save.image.directory");
			// 下载流程操作说明
			String downLoadOperationDir = properties
					.getProperty("download.flow.directory");
			//visio导入出路径
			String saveVisioDirectory = properties
					.getProperty("save.visio.directory");
//			制度下载操作说明路径
			String saveRuleOpDirectory = properties
			.getProperty("save.ruleOp.directory");
			//文件管理路径
			String saveFileManageDirectory = properties
			.getProperty("save.fileManage.directory");
			//流程文件下载路径
			String saveFlowDirectory = properties
			.getProperty("save.flow.directory");
			//制度本地更新
			String savaRuleUpdateDirectory = properties
			.getProperty("save.ruleupdate.directory");
			
			systemIOData.setSaveFileDirectory(saveFileDirectory);
			systemIOData.setSaveImageDirectory(saveImageDirectory);
			systemIOData.setDownLoadOperationDir(downLoadOperationDir);
			systemIOData.setSaveVisioDirectory(saveVisioDirectory);
			systemIOData.setSaveRuleOpDirectory(saveRuleOpDirectory);
			systemIOData.setSaveFileManageDirectory(saveFileManageDirectory);
			systemIOData.setSaveFlowDirectory(saveFlowDirectory);
			systemIOData.setSavaRuleUpdateDirectory(savaRuleUpdateDirectory);
		}
	}
}
