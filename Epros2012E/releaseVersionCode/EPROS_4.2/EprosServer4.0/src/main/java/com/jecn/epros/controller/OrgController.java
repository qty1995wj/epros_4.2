package com.jecn.epros.controller;

import com.jecn.epros.configuration.rpc.RPCServiceConfiguration;
import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.org.OrgViewBean;
import com.jecn.epros.domain.org.SearchOrgResultBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.IChartRPCService;
import com.jecn.epros.service.org.OrgService;
import com.jecn.epros.util.AttachmentDownloadUtils;
import com.jecn.epros.util.Params;
import com.jecn.epros.util.excel.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/organizations", description = "组织", position = 7)
public class OrgController {
    @Autowired
    private OrgService orgService;

    @Autowired
    private IChartRPCService chartRPCService;

    @ApiOperation(value = "组织-搜索")
    @RequestMapping(value = "/organizations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrgViewBean> search(
            @RequestParam(defaultValue = "") String name, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("paging", paging);
        map.put("name", name);
        OrgViewBean orgViewBean = new OrgViewBean();
        int total = orgService.searchOrgTotal(map);
        List<SearchOrgResultBean> listSearchOrgResultBean = orgService.searchOrg(map);
        orgViewBean.setTotal(total);
        orgViewBean.setData(listSearchOrgResultBean);
        return ResponseEntity.ok(orgViewBean);
    }

    @PreAuthorize(" hasAuthority('orgSearch')")
    @ApiOperation(value = "组织-图")
    @RequestMapping(value = "/organizations/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> findBaseInfo(@PathVariable Long id) {
        Params params = new Params("projectId", AuthenticatedUserUtil.getProjectId());
        params.add("isPub", true);
        Map<String, Object> map = params.toMap();
        map.put("httpURL", RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL);
        map.put("languageType",AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getLanguage());
        String chartJSON = chartRPCService.getChartJSON(id, "organization", AuthenticatedUserUtil.getPeopleId(), map);
        return ResponseEntity.ok(chartJSON);
    }

    @ApiOperation(value = "组织-树节点展开")
    @RequestMapping(value = "/organizations/{id}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> fetchChildren(
            @ApiParam(value = "父节点Id", required = true) @PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // 父节点ID
        map.put("pId", id);
        return ResponseEntity.ok(orgService.findChildOrgs(map));
    }

}
