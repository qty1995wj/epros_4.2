package epros.draw.gui.top.toolbar;

import javax.swing.JPanel;

import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.top.button.JecnToolbarTitleButton;

/**
 * 
 * 工具栏标题按钮以及对应面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolBarTitleData {
	/** 工具栏标题按钮 */
	private JecnToolbarTitleButton titleButton = null;
	/** 工具栏标题按钮对应的显示面板 */
	private JPanel pnael = null;

	public JecnToolBarTitleData(JecnToolbarTitleButton titleButton, JPanel pnael) {
		if (titleButton == null || pnael == null) {
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.titleButton = titleButton;
		this.pnael = pnael;
	}

	public JecnToolbarTitleButton getTitleButton() {
		return titleButton;
	}

	public JPanel getPnael() {
		return pnael;
	}
}
