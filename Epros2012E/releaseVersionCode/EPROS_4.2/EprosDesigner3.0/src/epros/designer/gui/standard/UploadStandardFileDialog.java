package epros.designer.gui.standard;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

public class UploadStandardFileDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(UploadStandardFileDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 属性面板，包括密级、岗位权限、组织权限 */
	private JecnPanel proPanel = new JecnPanel(); // 530,160
	/** 文件面板 */
	private JecnPanel filePanel = new JecnPanel(); // 530,240
	/** 滚动面板，加入fileTable */
	private JScrollPane fileTableScrollPanel = new JScrollPane();
	private JTable fileTable = new UploadStandardFileTable();
	private JecnPanel fileButtonPanel = new JecnPanel(); // 520,30
	/** 主面板底部按钮面板，包括确定、取消按钮 */
	private JecnPanel botButtonPanel = new JecnPanel(); // 530,30
	/** 选择按钮 */
	private JButton uploadBut = new JButton(JecnProperties.getValue("selectBtn"));
	/** 打开按钮 */
	private JButton openBut = new JButton(JecnProperties.getValue("openBtn"));
	/** 删除 */
	private JButton deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** 查阅权限 */
	private AccessAuthorityCommon accessAuthorityCommon = null;

	/*** 确定按钮验证提示显示Label */
	private JLabel verfyLab = new JLabel();
	/** 文件按钮验证提示显示Label */
	private JLabel fileVerfyLab = new JLabel();
	// 确定
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	// 取消
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	private JecnTreeNode pNode = null;
	private JecnTree jTree = null;

	public UploadStandardFileDialog(JecnTreeNode pNode, JecnTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		fileTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		fileTableScrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		verfyLab.setForeground(Color.red);
		fileVerfyLab.setForeground(Color.red);
		initCompotents();
		initLayout();
		buttonActionInit();
	}

	private void initCompotents() {
		this.setTitle(JecnProperties.getValue("ralationFile"));
		this.setSize(540, 480);
		this.setResizable(true);
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);

		filePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("addFile")));// 添加文件

		// 取消事件，关闭窗口
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UploadStandardFileDialog.this.dispose();
			}
		});

	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());

		Insets insets = new Insets(5, 5, 5, 5);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(proPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				0, 5, 0, 5), 0, 0);
		mainPanel.add(filePanel, c);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		mainPanel.add(botButtonPanel, c);
		proPanel.setLayout(new GridBagLayout());
		accessAuthorityCommon = new AccessAuthorityCommon(0, proPanel, insets, pNode.getJecnTreeBean().getId(), 2,
				this,true,true);
		filePanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				0, 5, 0, 5), 0, 0);
		filePanel.add(fileTableScrollPanel, c);
		fileTableScrollPanel.setViewportView(fileTable);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		filePanel.add(fileButtonPanel, c);
		fileButtonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		fileButtonPanel.add(uploadBut);
		fileButtonPanel.add(openBut);
		fileButtonPanel.add(deleteBut);
		fileButtonPanel.add(fileVerfyLab);
		botButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		botButtonPanel.add(verfyLab);
		botButtonPanel.add(okBut);
		botButtonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

	private void buttonActionInit() {
		uploadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileVerfyLab.setText("");
				verfyLab.setText("");
				FileChooseDialog fileChooseDialog = new FileChooseDialog(new ArrayList<JecnTreeBean>());
				fileChooseDialog.setVisible(true);
				if (fileChooseDialog.isOperation()) {
					Vector<Vector<String>> data = ((JecnTableModel) fileChooseDialog.getResultTable().getModel())
							.getDataVector();
					if (data.size() > 0) {
						for (Vector<String> v : data) {
							((DefaultTableModel) fileTable.getModel()).addRow(v);
						}

					}
				}
			}
		});
		openBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileVerfyLab.setText("");
				// 判断是否选中Table中的一行，没选中，提示选中一行
				int[] selectRows = fileTable.getSelectedRows();
				if (selectRows.length == 1) {
					Long id = Long.valueOf((String) fileTable.getValueAt(selectRows[0], 0));
					JecnDesignerCommon.openFile(id, fileVerfyLab);
				} else {
					fileVerfyLab.setText(JecnProperties.getValue("chooseOneRow"));
				}
			}
		});

		deleteBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileVerfyLab.setText("");
				int[] selectRows = fileTable.getSelectedRows();
				if (selectRows.length == 0) {
					fileVerfyLab.setText(JecnProperties.getValue("chooseOneRow"));
					return;
				}
				for (int i = selectRows.length - 1; i >= 0; i--) {
					((DefaultTableModel) fileTable.getModel()).removeRow(selectRows[i]);
				}
			}
		});

		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				verfyLab.setText("");
				Vector<Vector<String>> data = ((DefaultTableModel) fileTable.getModel()).getDataVector();
				if (data.size() <= 0) {
					verfyLab.setText(JecnProperties.getValue("pleaseChooseUploadFile"));
					return;
				} else if (data.size() > 0) {
					List<StandardBean> list = new ArrayList<StandardBean>();
					StandardBean standardBean = null;
					int i = 0;
					for (Vector<String> v : data) {
						// 上传文件重复判断
						if (JecnTreeCommon.validateRepeatNameAdd(pNode, v.get(1), TreeNodeType.standard)) {
							verfyLab.setText(v.get(1) + JecnProperties.getValue("isExist"));
							return;
						}
						standardBean = new StandardBean();
						standardBean.setCriterionClassName(v.get(1));
						standardBean.setPreCriterionClassId(pNode.getJecnTreeBean().getId());
						standardBean.setProjectId(JecnConstants.projectId);
						standardBean.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(pNode) + i));
						standardBean.setRelationId(Long.valueOf(v.get(0)));
						standardBean.setStanType(1);
						standardBean.setIsPublic(Long.valueOf(accessAuthorityCommon.getPublicStatic()));
						standardBean.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
						standardBean.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
						list.add(standardBean);
						i++;
					}
					try {
						ConnectionPool.getStandardAction().addStandardFile(list, accessAuthorityCommon.getPosIds(),
								accessAuthorityCommon.getOrgIds(), accessAuthorityCommon.getPosGroupIds(),
								JecnConstants.getUserId());
						List<JecnTreeBean> listNode = ConnectionPool.getStandardAction().getChildStandards(
								pNode.getJecnTreeBean().getId(), JecnConstants.projectId);
						JecnTreeCommon.refreshNode(pNode, listNode, jTree);
						JecnTreeCommon.autoExpandNode(jTree, pNode);
						UploadStandardFileDialog.this.setVisible(false);
					} catch (Exception ex) {
						JecnOptionPane.showMessageDialog(UploadStandardFileDialog.this, JecnProperties.getValue("uploadStandardFileException"));
						log.error(JecnProperties.getValue("uploadStandardFileException"), ex);
					}

				}
			}
		});
	}

	class UploadStandardFileTable extends JTable {

		public UploadStandardFileTable() {

			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			TableColumn column = columnModel.getColumn(0);
			column.setMinWidth(100);
			column.setMaxWidth(150);
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}
			// this.addKeyListener(new CTableKeyAdapter(this));
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
		}

		public DefaultTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("id"));
			// title.add(JecnProperties.getValue("fileNum"));
			title.add(JecnProperties.getValue("fileName"));
			return new DefaultTableModel(null, title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {
			return new int[] { 0 };
		}

	}
}
