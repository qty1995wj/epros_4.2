package epros.draw.gui.operationConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JLabel;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.util.JecnXmlUtil;

/**
 * 流程操作说明util类
 * 
 * @author fuzhh Aug 27, 2012
 * 
 */
public class OperationConfigUtil {

	/**
	 * 修改流程操作说明到XML
	 * 
	 * @author fuzhh Aug 27, 2012
	 * @param operationConfigList
	 *            需要修改的数据
	 * @param updateType
	 *            修改类型， update为修改，add为添加，delete为删除 ，default为默认
	 */
	public static boolean updateOperationConfigXml(List<DrawOperationDescriptionConfigBean> operationConfigList) {
		// 修改是否成功标识
		boolean updateFlag = false;
		// 获取路径
		String xmlPath = getXmlPath();
		// 获取dom
		Document doc = JecnXmlUtil.getDocumentByAllPath(xmlPath);
		if (doc != null) {
			// 获取主节点
			NodeList operationNodeData = doc.getElementsByTagName("operationNodeData");
			if (operationNodeData != null && operationNodeData.getLength() > 0) {
				for (DrawOperationDescriptionConfigBean operationBean : operationConfigList) {
					for (int i = 0; i < operationNodeData.getLength(); i++) {
						Element operationElement = (Element) operationNodeData.item(i);
						if (operationBean.getMark().equals(operationElement.getAttribute("mark"))) {
							// 修改是否显示
							operationElement.setAttribute("isShow", String.valueOf(operationBean.getIsShow()));
							// 修改名称
							operationElement.setAttribute("name", String.valueOf(operationBean.getName()));
							// 修改排序
							operationElement.setAttribute("turn", String.valueOf(operationBean.getTurn()));
						}
					}
				}
			} else {
				return false;
			}
			updateFlag = JecnXmlUtil.writeXmlByDocumentAndAllPath(xmlPath, doc);
		}
		return updateFlag;
	}

	/**
	 * 读取XML中的流程操作说明配置
	 * 
	 * @author fuzhh Aug 27, 2012
	 */
	public static List<DrawOperationDescriptionConfigBean> readOperationConfigXml() {
		List<DrawOperationDescriptionConfigBean> operationConfigList = new ArrayList<DrawOperationDescriptionConfigBean>();
		// 读取xml的document
		Document doc = JecnXmlUtil.getDocumentByAllPath(getXmlPath());
		if (doc == null) {
			return null;
		}
		// 获取读取XML figures节点的数据
		NodeList operationNodeData = doc.getElementsByTagName("operationNodeData");
		for (int i = 0; i < operationNodeData.getLength(); i++) {
			DrawOperationDescriptionConfigBean operationConfigBean = new DrawOperationDescriptionConfigBean();
			Element operationNodeElement = (Element) operationNodeData.item(i);
			// 获取名称
			String name = operationNodeElement.getAttribute("name");
			operationConfigBean.setName(name);
			// 获取默认名称
			String defaultName = operationNodeElement.getAttribute("defaultName");
			operationConfigBean.setDefaultName(defaultName);
			// 标识
			String mark = operationNodeElement.getAttribute("mark");
			operationConfigBean.setMark(mark);
			// 排序
			String turn = operationNodeElement.getAttribute("turn");
			operationConfigBean.setTurn(Integer.valueOf(turn));
			// 默认排序
			String defaultSort = operationNodeElement.getAttribute("defaultSort");
			operationConfigBean.setDefaultSort(Integer.valueOf(defaultSort));
			// 是否显示
			String isShow = operationNodeElement.getAttribute("isShow");
			operationConfigBean.setIsShow(Integer.valueOf(isShow));
			// 默认是否显示
			String defaultIsShow = operationNodeElement.getAttribute("isDefault");
			operationConfigBean.setIsDefault(Integer.valueOf(defaultIsShow));
			// 把数据添加到集合
			operationConfigList.add(operationConfigBean);
		}
		// 根据序号排序
		sortAllFlowElementCloneList(operationConfigList);
		return operationConfigList;
	}

	/**
	 * 
	 * 给定参数按照序号排序
	 * 
	 * @param itemBeanList
	 */
	public static void sortAllFlowElementCloneList(List<DrawOperationDescriptionConfigBean> operationConfigList) {

		if (operationConfigList == null || operationConfigList.size() == 0) {
			return;
		}

		Collections.sort(operationConfigList, new Comparator<DrawOperationDescriptionConfigBean>() {
			public int compare(DrawOperationDescriptionConfigBean r1, DrawOperationDescriptionConfigBean r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("参数不合法");
				}
				int sortId1 = r1.getTurn();
				int sortId2 = r2.getTurn();
				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证输入的名称是否正常
	 * @param name
	 * @param jLable
	 * @return 正确返回true 不正确定返回false
	 */
	public static boolean validateName(String name, JLabel jLable) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			jLable.setText(JecnResourceUtil.getJecnResourceUtil().getValue("nameNotNull"));
			return false;
		} else if (!JecnUserCheckUtil.checkLength(name.trim(), 50)) {
			jLable.setText(JecnResourceUtil.getJecnResourceUtil().getValue("nameLength50"));
			return false;
		}
		jLable.setText("");
		return true;
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证输入的名称是否正常
	 * @param content
	 * @param jLable
	 * @return
	 */
	public static boolean validateContent(String content, JLabel jLable) {
		String info = JecnUserCheckUtil.checkNoteLength(content);
		if (!DrawCommon.isNullOrEmtryTrim(info)) {
			jLable.setText(info);
			return false;
		}
		jLable.setText("");
		return true;
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证输入的名称是否为空
	 * @param content
	 * @param jLable
	 * @return
	 */
	public static boolean isContentNotNull(String content, JLabel jLable, String str) {
		if (content == null || "".equals(content)) {
			if ("".equals(jLable.getText())) {
				jLable.setText(str);
			}
			return false;
		}
		if ("".equals(jLable.getText())) {
			jLable.setText("");
		}
		return true;
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证输入的名称是否正常
	 * @param content
	 * @param jLable
	 * @return
	 */
	public static boolean checkInOutContent(String content, JLabel jLable) {
		String info = JecnUserCheckUtil.checkNameLength(content);
		if (!DrawCommon.isNullOrEmtryTrim(info)) {
			jLable.setText(info);
			return false;
		}
		jLable.setText("");
		return true;
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证输入的名称是否正常
	 * @param content
	 * @param jLable
	 * @return
	 */
	public static boolean checkInOutIsNullContent(String content, JLabel jLable) {
		if (DrawCommon.isNullOrEmtryTrim(content)) {
			// 名称不能为空
			jLable.setText(JecnUserCheckInfoData.getNameNotNull());
			return false;
		}
		return checkInOutContent(content, jLable);
	}

	/**
	 * 验证是否为数字
	 * 
	 * @author fuzhh Dec 12, 2012
	 * @param num
	 * @param jLable
	 * @return
	 */
	public static boolean checkNum(String num, JLabel jLable) {
		boolean isNum = num.matches("[0-9]*");
		if (!isNum) {
			if ("".equals(jLable.getText())) {
				jLable.setText(JecnResourceUtil.getJecnResourceUtil().getValue("pleaseEnterTheNumber"));
			}
			return false;
		}
		if ("".equals(jLable.getText())) {
			jLable.setText("");
		}
		return true;
	}

	/**
	 * 判断是否为数字，数字长度不超过11位
	 * 
	 * @author fuzhh Dec 12, 2012
	 * @param num
	 * @param jLable
	 * @return
	 */
	public static boolean checkCountAndNum(String num, JLabel jLable) {
		if (checkNum(num, jLable)) {
			if (!JecnUserCheckUtil.checkLength(num, 11)) {
				if ("".equals(jLable.getText())) {
					jLable.setText(JecnResourceUtil.getJecnResourceUtil().getValue("frequencyBeyondLength"));
				}
				return false;
			}
			if ("".equals(jLable.getText())) {
				jLable.setText("");
			}
			return true;
		}
		return false;
	}

	/**
	 * 返回文件路径
	 * 
	 * @param type
	 *            是否为标题
	 * @return 路径
	 */
	public static String getXmlPath() {
		// 文件路径
		String xmlPath = "/epros/draw/configFile/operationConfig/operationConfig.xml";
		return JecnFileUtil.getSyncPath(xmlPath);
	}

}
