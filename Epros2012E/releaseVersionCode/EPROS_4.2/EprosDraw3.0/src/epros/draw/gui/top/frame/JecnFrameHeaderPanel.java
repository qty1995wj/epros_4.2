package epros.draw.gui.top.frame;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 窗体头
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFrameHeaderPanel extends JPanel {
	/** 日志对象 */
	private final Log log = LogFactory.getLog(JecnFrameHeaderPanel.class);

	/** 窗体 */
	private JecnFrame frame = null;

	/** 窗体标题栏 */
	private JecnFrameTitlePanel frameTitlePanel = null;
	/** 存放工具栏标题的面板 */
	private JecnFrameDropTitlePanel toolTitlePanel = null;
	/** 空闲区域 */
	private JecnFrameDropTitlePanel freePanel = null;

	public JecnFrameHeaderPanel(JecnFrame frame) {
		if (frame == null) {
			log.error("JecnHeaderPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.frame = frame;

		initComponents();
		layoutComponents();
	}

	/**
	 * 
	 * 实例化子组件
	 * 
	 */
	private void initComponents() {
		// 设置布局
		this.setLayout(new GridBagLayout());
		// 背景颜色
		this.setBackground(JecnUIUtil.getFrameTitleColor());

		// 窗体标题栏
		frameTitlePanel = new JecnFrameTitlePanel(frame);
		// 存放工具栏标题的面板
		toolTitlePanel = new JecnFrameDropTitlePanel(frame);
		// 空闲区域
		freePanel = new JecnFrameDropTitlePanel(frame);
	}

	/**
	 * 
	 * 绘制组件
	 * 
	 */
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;

		// 整体渐变
		g2d.setPaint(new GradientPaint(0, 0,
				JecnUIUtil.getFrameTitleTopColor(), 0, this.getHeight() / 2,
				JecnUIUtil.getFrameTitleColor()));
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());

		// 上部分局部高亮
		g2d.setColor(Color.WHITE);
		g2d.fillRect(100, 3, this.getWidth() - 200, 3);

		// 下半部渐变
		g2d.setPaint(new GradientPaint(0, this.getHeight() - 25, JecnUIUtil
				.getFrameTitleColor(), 0, this.getHeight(), JecnUIUtil
				.getFrameTitleBottomColor()));
		g2d.fillRect(0, this.getHeight() - 25, this.getWidth(), this
				.getHeight());

		// 绘制窗体底部线条
		JecnUIUtil.paintFrameBottomLine(this, g2d);
	}

	/**
	 * 
	 * 布局子组件
	 * 
	 */
	private void layoutComponents() {
		// 窗体标题栏
		GridBagConstraints c = new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(frameTitlePanel, c);

		// 存放工具栏标题面板的面板
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(toolTitlePanel, c);

		// 空闲区域
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(freePanel, c);

	}

	/**
	 * 
	 * 添加组件到窗体空闲区域
	 * 
	 * @param comp
	 *            JComponent 组件
	 */
	public void addCompToFreePanel(JComponent comp) {
		if (comp == null || frameTitlePanel == null) {
			return;
		}
		frameTitlePanel.getOtherPanel().add(comp);
	}

	public JecnFrameTitlePanel getFrameTitlePanel() {
		return frameTitlePanel;
	}

	public JecnFrameDropTitlePanel getToolTitlePanel() {
		return toolTitlePanel;
	}

	public JecnFrameDropTitlePanel getFreePanel() {
		return freePanel;
	}

}
