package epros.draw.gui.top.toolbar.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.gui.workflow.util.JecnRemoveFlowElementUnit;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 排列 右键菜单( 自动对齐、角色、虚拟角色、客户、IT角色、垂直居中、底端对齐)
 * 
 * @author ZXH
 * @date： 日期：12 30, 2017 时间：11:28:56 AM
 */
public class JecnReplaceRolePopupMenu extends JMenu implements ActionListener {
	/** 角色 */
	private JMenuItem roleFigureMenu;
	/** 虚拟角色 */
	private JMenuItem virtualRoleFigureMenu;
	/** 客户 */
	private JMenuItem customFigureMenu;

	public JecnReplaceRolePopupMenu() {
		this.setText(JecnResourceUtil.getJecnResourceUtil().getValue("roleReplacement"));
		this.setIcon(JecnFileUtil.getReplaceImageIcon("replaceEle"));
		// 角色
		roleFigureMenu = new JMenuItem();
		roleFigureMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("role"));// 角色
		// 添加事件监听
		roleFigureMenu.addActionListener(this);
		roleFigureMenu.setName(MapElemType.RoleFigure.toString());
		roleFigureMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.RoleFigure.toString()));

		// 虚拟角色
		virtualRoleFigureMenu = new JMenuItem();
		virtualRoleFigureMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("virtualRole"));// 虚拟角色
		// 添加事件监听
		virtualRoleFigureMenu.addActionListener(this);
		virtualRoleFigureMenu.setName(MapElemType.VirtualRoleFigure.toString());
		virtualRoleFigureMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.VirtualRoleFigure.toString()));

		// 客户
		customFigureMenu = new JMenuItem();
		customFigureMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("customer"));// 客户
		// 添加事件监听
		customFigureMenu.addActionListener(this);
		customFigureMenu.setName(MapElemType.CustomFigure.toString());
		customFigureMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.CustomFigure.toString()));

		// 流程图显示节点
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(0);
		if (showTypeList.contains(MapElemType.RoleFigure)) {
			this.add(roleFigureMenu);
		}
		if (showTypeList.contains(MapElemType.CustomFigure)) {
			this.add(customFigureMenu);
		}
		if (showTypeList.contains(MapElemType.VirtualRoleFigure)) {
			this.add(virtualRoleFigureMenu);
		}
	}

	public void actionPerformed(ActionEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		List<JecnBaseFlowElementPanel> selectFigureList = desktopPane.getCurrentSelectElement();
		if (selectFigureList.size() != 1) {
			return;
		}
		if (e.getSource() instanceof JMenuItem) {
			// 获取JMenuItem
			JMenuItem menuItem = (JMenuItem) e.getSource();
			String menuName = menuItem.getName();

			// 创建元素
			JecnBaseRoleFigure newFigure = (JecnBaseRoleFigure) JecnCreateFlowElement.createFlowElementObj(MapElemType
					.valueOf(menuName));

			JecnBaseRoleFigure selectFigure = (JecnBaseRoleFigure) selectFigureList.get(0);
			// 操作前数据
			JecnUndoRedoData undoData = new JecnUndoRedoData();
			// 操作后数据
			JecnUndoRedoData redoData = new JecnUndoRedoData();

			JecnRemoveFlowElementUnit.removeReplaceFigure(selectFigure);

			undoData.recodeFlowElement(selectFigure);

			// 复制属性
			newFigure.getFlowElementData().setActiveArrts(selectFigure.getFlowElementData(),
					newFigure.getFlowElementData());

			// 
			for (JecnFlowStationT flowStationT : selectFigure.getFlowElementData().getFlowStationList()) {
				newFigure.getFlowElementData().getFlowStationList().add(flowStationT.clone());
			}

			if (newFigure.getFlowElementData().getMapElemType() == MapElemType.CustomFigure) {
				newFigure.getFlowElementData().setActivityId("");
			}
			if (newFigure.getFlowElementData().getMapElemType() == MapElemType.VirtualRoleFigure) {
				newFigure.getFlowElementData().getFlowStationList().clear();
			} else {
				JecnDesignerProcess.getDesignerProcess().addElemLink(newFigure, null);
			}
			JecnAddFlowElementUnit.addFigure(selectFigure.getLocation(), newFigure);
			redoData.recodeFlowElement(newFigure);

			// 清空虚拟图形
			desktopPane.updateUI();

			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createDeleteAndAdd(undoData, redoData);
			desktopPane.getUndoRedoManager().addEdit(unredoProcess);
		}
	}

}
