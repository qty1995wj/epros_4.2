package com.jecn.webservice.cetc10.sso;

public class SSOWebServiceSoapProxy implements com.jecn.webservice.cetc10.sso.SSOWebServiceSoap {
  private String _endpoint = null;
  private com.jecn.webservice.cetc10.sso.SSOWebServiceSoap sSOWebServiceSoap = null;
  
  public SSOWebServiceSoapProxy() {
    _initSSOWebServiceSoapProxy();
  }
  
  public SSOWebServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSSOWebServiceSoapProxy();
  }
  
  private void _initSSOWebServiceSoapProxy() {
    try {
      sSOWebServiceSoap = (new com.jecn.webservice.cetc10.sso.SSOWebServiceLocator()).getSSOWebServiceSoap();
      if (sSOWebServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sSOWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sSOWebServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sSOWebServiceSoap != null)
      ((javax.xml.rpc.Stub)sSOWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.jecn.webservice.cetc10.sso.SSOWebServiceSoap getSSOWebServiceSoap() {
    if (sSOWebServiceSoap == null)
      _initSSOWebServiceSoapProxy();
    return sSOWebServiceSoap;
  }
  
  public int userLogon(java.lang.String logonName, java.lang.String IPAddrStr) throws java.rmi.RemoteException{
    if (sSOWebServiceSoap == null)
      _initSSOWebServiceSoapProxy();
    return sSOWebServiceSoap.userLogon(logonName, IPAddrStr);
  }
  
  public int userLogoff(java.lang.String IPAddrStr) throws java.rmi.RemoteException{
    if (sSOWebServiceSoap == null)
      _initSSOWebServiceSoapProxy();
    return sSOWebServiceSoap.userLogoff(IPAddrStr);
  }
  
  public com.jecn.webservice.cetc10.sso.UserInfo getUserInfo(java.lang.String IPAddrStr) throws java.rmi.RemoteException{
    if (sSOWebServiceSoap == null)
      _initSSOWebServiceSoapProxy();
    return sSOWebServiceSoap.getUserInfo(IPAddrStr);
  }
  
  public java.lang.String getUserInfoStr(java.lang.String IPAddrStr) throws java.rmi.RemoteException{
    if (sSOWebServiceSoap == null)
      _initSSOWebServiceSoapProxy();
    return sSOWebServiceSoap.getUserInfoStr(IPAddrStr);
  }
  
  public com.jecn.webservice.cetc10.sso.ApplicationInfo[] getAppInfo(java.lang.String IPAddrStr) throws java.rmi.RemoteException{
    if (sSOWebServiceSoap == null)
      _initSSOWebServiceSoapProxy();
    return sSOWebServiceSoap.getAppInfo(IPAddrStr);
  }
  
  
}