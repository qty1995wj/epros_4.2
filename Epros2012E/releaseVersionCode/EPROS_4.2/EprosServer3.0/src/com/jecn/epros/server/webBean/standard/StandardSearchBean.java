package com.jecn.epros.server.webBean.standard;
/**
 * @author yxw 2012-12-4
 * @description：搜索条件封装 Bean
 */
public class StandardSearchBean {
	/**标准名称*/
	private String standardName;
	/**标准类型 -1是全部 1文件标准，2流程标准，3流程地图标准*/
	private int standardType;
	/** 查阅部门 */
	private long orgLookId = -1;
	/** 查阅部门名称 */
	private String orgLookName;
	/** 查阅岗位 */
	private long posLookId = -1;
	/** 查阅岗位名称 */
	private String posLookName;
	/** 密级 */
	private long secretLevel = -1;
	/**所属目录ID*/
	private long perId;
	/**所属目录名称*/
	private String perName;
	public long getPerId() {
		return perId;
	}
	public void setPerId(long perId) {
		this.perId = perId;
	}
	public String getPerName() {
		return perName;
	}
	public void setPerName(String perName) {
		this.perName = perName;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public int getStandardType() {
		return standardType;
	}
	public void setStandardType(int standardType) {
		this.standardType = standardType;
	}
	public long getOrgLookId() {
		return orgLookId;
	}
	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}
	public String getOrgLookName() {
		return orgLookName;
	}
	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}
	public long getPosLookId() {
		return posLookId;
	}
	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}
	public String getPosLookName() {
		return posLookName;
	}
	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}
	public long getSecretLevel() {
		return secretLevel;
	}
	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}
}
