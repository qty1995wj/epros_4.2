package epros.designer.gui.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class ResourceSortDialog extends JecnSortDialog {
	private static Logger log = Logger.getLogger(ResourceSortDialog.class);
	public ResourceSortDialog(JecnTreeNode selectNode, JecnTree jTree) {
		super(selectNode, jTree);
	}

	@Override
	public List<JecnTreeBean> getChild() {
		 List<JecnTreeBean> list=new ArrayList<JecnTreeBean>();
		try {
			
			switch (this.getSelectNode().getJecnTreeBean().getTreeNodeType()) {
			case process:
			case processMap:
			case processRoot:
				list = JecnDesignerCommon.getDesignerProcessTreeBeanList(this.getSelectNode().getJecnTreeBean().getId(),false);
				break;
			case organization:
			case organizationRoot:
				list=ConnectionPool.getOrganizationAction().getChildOrgsAndPositon(this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId,false);
				break;
			case standard:
			case standardDir:
			case standardRoot:
				list=ConnectionPool.getStandardAction().getChildStandards(this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId);
				break;
			case ruleFile:
			case ruleDir:
			case ruleRoot:
			case ruleModeFile:
				list=ConnectionPool.getRuleAction().getChildRules(this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId);
				break;
			case riskDir:
			case riskRoot:
			case riskPoint:
				list=ConnectionPool.getJecnRiskAction().getChildsRisk(this.getSelectNode().getJecnTreeBean().getId(),JecnConstants.projectId);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			log.error("ResourceSortDialog getChild is error",e);
		}
		return list;
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			
			switch (this.getSelectNode().getJecnTreeBean().getTreeNodeType()) {
			case process:
			case processMap:
			case processRoot:
				ConnectionPool.getProcessAction().updateSortFlows(list,this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId,JecnConstants.getUserId());
				break;
			case organization:
			case organizationRoot:
				ConnectionPool.getOrganizationAction().updateSortNodes(list, this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId);
				break;
			case standard:
			case standardDir:
			case standardRoot:
				ConnectionPool.getStandardAction().updateSortNodes(list, this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId,JecnConstants.getUserId());
				break;
			case ruleFile:
			case ruleDir:
			case ruleRoot:
			case ruleModeFile:
				ConnectionPool.getRuleAction().updateSortNodes(list, this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.projectId,JecnConstants.getUserId());
				break;
			case riskDir:
			case riskRoot:
			case riskPoint:
				ConnectionPool.getJecnRiskAction().updateSortNodes(list,this.getSelectNode().getJecnTreeBean().getId(),JecnConstants.getUserId());
				break;
			default:
				break;
			}
			return true;
		} catch (Exception e) {
			log.error("ResourceSortDialog saveData is error",e);
			return false;
		}
		
	}

}
