package epros.draw.gui.top.toolbar.io;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;
/**
 * 打印纸张页边距
 * @author fuzhh
 */
public class PrinterPageBorderJPanel extends JPanel {

    private int w;
    private int h;

    public PrinterPageBorderJPanel(int w, int h, int pageBorderLeft, int pageBorderRight, int pageBorderTop, int pageBorderBottom) {
        this.w = w;
        this.h = h;
        this.setBackground(Color.white);
        this.setBorder(new MatteBorder(pageBorderTop, pageBorderLeft, pageBorderBottom, pageBorderRight, Color.white));
    }

    @Override
    public Dimension getPreferredSize() {
        Insets ins = getInsets();
        return new Dimension(this.w + ins.left + ins.right, this.h + ins.top
                + ins.bottom);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
}
