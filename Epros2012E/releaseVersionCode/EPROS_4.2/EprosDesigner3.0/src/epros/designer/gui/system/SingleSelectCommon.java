package epros.designer.gui.system;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSearchPopup;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.swing.textfield.search.JecnSearchDocumentListener;
import epros.draw.gui.top.dialog.JecnDialog;

public abstract class SingleSelectCommon extends SelectCommon {

	/** Lab */
	private JLabel jecnLab = new JLabel();
	/** Field */
	protected JSearchTextField jecnField = new JSearchTextField(300, 20);

	/** popup */
	private JecnSearchPopup jecnSearchPopup = null;
	/** 结果 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** ID */
	private Long id = null;

	/** 选择按钮 */
	private JButton selectBut = new JButton(JecnProperties.getValue("selectBtn"));

	private JecnDialog jecnDialog = null;

	private boolean isRequest;

	private String labName;

	public JecnDialog getJecnDialog() {
		return jecnDialog;
	}

	public SingleSelectCommon(int rows, JecnPanel infoPanel, Insets insets, JecnTreeBean jecnTreeBean,
			JecnDialog jecnDialog, String labName, boolean isRequest) {
		this.jecnDialog = jecnDialog;
		this.isRequest = isRequest;
		this.labName = labName;
		jecnLab.setText(labName);
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(jecnLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(jecnField, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(selectBut, c);
		if (isRequest) {
			c = new GridBagConstraints(4, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(3, 1, 3, 1), 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
		jecnSearchPopup = new JecnSearchPopup(jecnField);
		// 隐藏第1列和第3列
		jecnSearchPopup.hiddenColumn(0, 2);
		if (jecnTreeBean != null && jecnTreeBean.getId() != null) {
			// 科大讯飞需要 重新查询人员名称
			getIflytekPeopleName(jecnTreeBean);
			// 流程责任部门
			jecnField.setText(jecnTreeBean.getName());
			id = jecnTreeBean.getId();
			list.add(jecnTreeBean);
		}
		jecnField.addDocumentListener(new JecnSearchDocumentListener(jecnField) {
			@Override
			public void removeUpdateJecn(DocumentEvent e) {
				fieldSearch();
			}

			@Override
			public void insertUpdateJecn(DocumentEvent e) {
				fieldSearch();
			}

			@Override
			public void keyEnterReleased(KeyEvent e) {
				jecnSearchPopup.searchTableMousePressed();
			}
		});
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jecnSearchPopup.setCanSearch(false);
				selectButPerformed(list, jecnField, id);
				jecnSearchPopup.setCanSearch(true);
			}

		});
		jecnSearchPopup.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				if (jecnSearchPopup.getSelectTreeBean() != null) {
					list.clear();
					list.add(jecnSearchPopup.getSelectTreeBean());
					jecnSearchPopup.setSelectTreeBean(null);
				}
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});
	}

	protected void getIflytekPeopleName(JecnTreeBean jecnTreeBean) {

	}

	/***************************************************************************
	 * 流程责任部门查询
	 */
	private void fieldSearch() {
		String name = jecnField.getText().trim();
		if (!"".equals(name)) {
			if (jecnSearchPopup.isCanSearch()) {
				jecnSearchPopup.setTableData(searchName(name, JecnConstants.projectId));
			}
			jecnSearchPopup.setCanSearch(true);
		}
	}

	public void disableButton() {
		selectBut.setEnabled(false);
		jecnField.setEditable(false);
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	public boolean isUpdate() {
		Long newId = getIdResult();
		if (newId == null) {
			if (this.id != null) {
				return true;
			}
		} else {
			if (this.id == null) {
				return true;
			} else {
				if (!newId.equals(id)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 验证必填项
	 * 
	 * @param verfyLab
	 * @return
	 */
	public boolean validateRequired(JLabel verfyLab) {
		if (this.isRequest) {
			if (getIdResult() == null) {
				verfyLab.setText(labName.substring(0, labName.length() - 1) + JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		return true;
	}

	/**
	 * 返回ID
	 * 
	 * @return
	 */
	public Long getIdResult() {
		if (this.list.size() == 1) {
			JecnTreeBean jecnTreeBean = list.get(0);
			return jecnTreeBean.getId();
		}
		return null;
	}

	public String getStringIdResult() {
		if (this.list.size() == 1) {
			JecnTreeBean jecnTreeBean = list.get(0);
			return jecnTreeBean.getId().toString();
		}
		return "";
	}

	public String getNameResult() {
		if (this.list.size() == 1) {
			JecnTreeBean jecnTreeBean = list.get(0);
			return jecnTreeBean.getName();
		}
		return "";
	}

	public JLabel getJecnLab() {
		return jecnLab;
	}

	/***************************************************************************
	 * 选择
	 */
	protected abstract void selectButPerformed(List<JecnTreeBean> list, JSearchTextField jecnField, Long id);

	protected abstract List<JecnTreeBean> searchName(String name, Long projectId);

	public JSearchTextField getJecnField() {
		return jecnField;
	}

	public String getLabName() {
		return labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}
	
	
}
