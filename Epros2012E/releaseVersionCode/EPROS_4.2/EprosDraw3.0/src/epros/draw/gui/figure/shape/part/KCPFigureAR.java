package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;

/**
 * 关键控制点 石勘院新增 元素
 * 
 *@author ZXH
 * @date 2016-8-27上午11:31:50
 */
public class KCPFigureAR extends Triangle {

	public KCPFigureAR(JecnFigureData keyActiveData) {
		super(keyActiveData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = userWidth / 2;
			y1 = userHeight;
			x2 = 0;
			y2 = 0;
			x3 = userWidth;
			y3 = 0;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth;
			y2 = userHeight;
			x3 = 0;
			y3 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = userWidth;
			y1 = userHeight / 2;
			x2 = 0;
			y2 = userHeight;
			x3 = 0;
			y3 = 0;
		}
		paintFigure(g);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintTriangleTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount }, new int[] { y1 - shadowCount / 2, y2,
					y3 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount }, new int[] { y1 - shadowCount / 2, y2,
					y3 - shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount, x3 }, new int[] { y1, y2 - shadowCount,
					y3 - shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 - shadowCount / 2, x2, x3 }, new int[] { y1 - shadowCount / 2,
					y2 - shadowCount, y3 }, 3);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1 - shadowCount / 2, x2, x3 }, new int[] { y1 - shadowCount / 2,
					y2 - shadowCount, y3 }, 3);
		}
	}

	/**
	 * 3D图形底层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	public void paint3DTriangleLow(Graphics2D g2d, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2, x2, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue, y2, y3 }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue / 2, y2, y3 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2, x2 - indent3Dvalue, x3 }, new int[] { y1,
					y2 - indent3Dvalue, y3 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue, x2, x3 }, new int[] { y1 - indent3Dvalue / 2,
					y2 - indent3Dvalue, y3 }, 3);
		}
		g2d.setStroke(flowElementData.getBasicStroke());
	}

	/**
	 * 3D效果设置线条 底层边框
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	protected void paint3DTriangleLine(Graphics2D g2d, int indent3Dvalue) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue / 2, x2, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue, y2, y3 }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue }, new int[] {
					y1 - indent3Dvalue / 2, y2, y3 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue / 2, x2 - indent3Dvalue, x3 }, new int[] { y1,
					y2 - indent3Dvalue, y3 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.drawPolygon(new int[] { x1 - indent3Dvalue, x2, x3 }, new int[] { y1 - indent3Dvalue / 2,
					y2 - indent3Dvalue, y3 }, 3);
		}
		g2d.setStroke(flowElementData.getBasicStroke());
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1, x2 + indent3Dvalue, x3 - indent3Dvalue }, new int[] { y1 - indent3Dvalue,
					y2 + indent3Dvalue, y3 + indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - indent3Dvalue, x3 - indent3Dvalue }, new int[] { y1,
					y2 + indent3Dvalue, y3 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 + indent3Dvalue }, new int[] {
					y1 + 2 * indent3Dvalue, y2 - indent3Dvalue, y3 - indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue, x2 + indent3Dvalue, x3 + indent3Dvalue }, new int[] { y1,
					y2 - indent3Dvalue, y3 + indent3Dvalue }, 3);
		}
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2, x2 + indent3Dvalue, x3 - 2 * indent3Dvalue },
					new int[] { y1 - 2 * indent3Dvalue, y2 + indent3Dvalue, y3 + indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + indent3Dvalue, x2 - 2 * indent3Dvalue, x3 - 2 * indent3Dvalue },
					new int[] { y1 - indent3Dvalue / 2, y2 + indent3Dvalue, y3 - 2 * indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2 - 2 * indent3Dvalue, x3 + indent3Dvalue }, new int[] {
					y1 + indent3Dvalue, y2 - 2 * indent3Dvalue, y3 - 2 * indent3Dvalue }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 - 2 * indent3Dvalue, x2 + indent3Dvalue, x3 + indent3Dvalue }, new int[] {
					y1 - indent3Dvalue / 2, y2 - 2 * indent3Dvalue, y3 + indent3Dvalue }, 3);
		}
		g2d.setStroke(flowElementData.getBasicStroke());
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	public void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount }, new int[] {
					y1 + shadowCount, y2 + shadowCount, y3 + shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount / 2, x3 + shadowCount / 2 }, new int[] {
					y1 + shadowCount, y2 + shadowCount, y3 + shadowCount / 2 }, 3);

		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount }, new int[] {
					y1 + shadowCount, y2 + shadowCount, y3 + shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount }, new int[] {
					y1 + shadowCount, y2, y3 + shadowCount }, 3);
		}
	}

}
