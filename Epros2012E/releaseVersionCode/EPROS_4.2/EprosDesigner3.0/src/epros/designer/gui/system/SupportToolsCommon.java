package epros.designer.gui.system;

import java.awt.Insets;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.process.flowtool.FlowToolChooseDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

public class SupportToolsCommon extends MutilSelectCommon {

	/** 支持工具 */

	public SupportToolsCommon(int rows, JecnPanel infoPanel, Insets insets, JecnDialog jecnDialog,
			List<JecnTreeBean> list,boolean isRequest) {
		super(rows, infoPanel, insets, list, JecnProperties.getValue("flowToolC"), jecnDialog,isRequest,false);

	}

	@Override
	protected JecnSelectChooseDialog getJecnSelectChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		return new FlowToolChooseDialog(list, jecnDialog);
	}
}
