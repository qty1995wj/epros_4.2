package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

/***
 * 文控信息配置面板
 * 
 * @author
 * @Date 2015-03-06
 */
public class JecnDocumentControlPanel extends JecnAbstractPropertyBasePanel
		implements ActionListener {

	/** 信息显示面板 **/
	private JecnPanel mainPanel = new JecnPanel();
	/** 具有流程架构、流程、制度、文件查阅权限的人 */
	private JecnConfigCheckBox permissionsRadio = new JecnConfigCheckBox(
			JecnProperties.getValue("docControlPermissionsPeopel"));

	private JecnConfigTypeDesgBean configTypeDesgBean = null;

	public JecnDocumentControlPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	private void initComponents() {
		permissionsRadio.setSelected(false);
		permissionsRadio.addActionListener(this);
	}

	private void initLayout() {
		Insets insets = new Insets(5, 5, 5, 5);
		// 文控信息查阅权限
		this.add(mainPanel, BorderLayout.NORTH);
		// GridBagConstraints c = null;
		mainPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("docControlPermissionsName")));
		permissionsRadio.setOpaque(false);
		// 具有流程架构、流程、制度、文件查阅权限的人
		mainPanel.add(permissionsRadio);
		
		
		// c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
		// 0, 0);
		// mainPanel.add(permissionsRadio, c);

	}

	@Override
	public boolean check() {
		return false;
	}

	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;
		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean
				.getOtherItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();

			if (ConfigItemPartMapMark.documentControl.toString().equals(// 文控信息查阅权限
					mark)) {
				permissionsRadio.setItemBean(itemBean);
				if ("0".equals(value)) {// 0:不具有
					permissionsRadio.setSelected(false);
				} else {// 1是具有
					permissionsRadio.setSelected(true);
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// if (e.getSource() instanceof JecnConfigCheckBox) {
		JecnConfigCheckBox checkBox = (JecnConfigCheckBox) e.getSource();
		if (checkBox.getItemBean() == null) {
			throw new NullPointerException();
		}

		if (checkBox.isSelected()) {// 选中
			checkBox.getItemBean().setValue(
					JecnConfigContents.ITEM_AUTO_SAVE_YES);
		} else {
			checkBox.getItemBean().setValue(
					JecnConfigContents.ITEM_AUTO_SAVE_NO);
		}
		// }
	}

	public JecnConfigTypeDesgBean getConfigTypeDesgBean() {
		return configTypeDesgBean;
	}

	public void setConfigTypeDesgBean(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;
	}
}
