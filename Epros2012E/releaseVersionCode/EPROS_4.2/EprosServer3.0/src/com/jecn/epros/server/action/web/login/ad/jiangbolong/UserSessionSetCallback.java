package com.jecn.epros.server.action.web.login.ad.jiangbolong;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.util.ApplicationContextUtil;

import edu.yale.its.tp.cas.client.CASReceipt;
import edu.yale.its.tp.cas.client.callback.IUserSessionSetCallback;

public class UserSessionSetCallback implements IUserSessionSetCallback {
	private static final Logger log = Logger.getLogger(UserSessionSetCallback.class);
	private IPersonService personService = null;

	@Override
	public void checkSessionInfAgain(ServletRequest request, ServletResponse response, String casServerHost,
			CASReceipt receipt) {
		HttpSession session = ((HttpServletRequest) request).getSession();
		if (session == null) {// session 不存在
			reBackError(response, "登录超时", casServerHost);
		}
		String loginUsername = ((String) session.getAttribute("login_Name")).toLowerCase();
		
		log.info("checkSessionInfAgain loginUsername = " + loginUsername);
		if (receipt != null && loginUsername != null && loginUsername.equals(receipt.getUserName().toCharArray())) {
			log.info("用户已登录！ " + loginUsername);
		} else {
			try {
				JecnUser user = getUser(loginUsername);
				log.info("user = " + user.getLoginName());
				if (user != null) {
					session.setAttribute("login_Name", user.getLoginName());
				} else {
					String source = "Longsys Platform Console";
					String msg = receipt.getUserName() + " no permission";
					log.info("casServerHost = " + casServerHost);
					noUserInforamtion(response, casServerHost, source, msg);
				}
			} catch (Exception e) {
				log.error("checkSessionInfAgain 检测人员信息异常！ login_Name = " + loginUsername + "\n" + e);
			}
		}
	}

	@Override
	public void createClientOwnSessionInfo(ServletRequest request, ServletResponse response, String casServerHost,
			CASReceipt receipt) {
		String loginUsername = receipt.getUserName();
		if (StringUtils.isEmpty(loginUsername)) {
			log.error("createClientOwnSessionInfo 初始化获取登录名称异常！receipt.getUserName()" + loginUsername);
			return;
		}
		try {
			log.info("loginUsername = " + loginUsername);
			JecnUser user = getUser(loginUsername);
			if (user != null) {
				HttpSession session = ((HttpServletRequest) request).getSession();
				session.setAttribute("login_Name", user.getLoginName());
				log.info("user.getLoginName() = " + user.getLoginName());
			} else {
				reBackError(response, loginUsername, casServerHost);
			}
		} catch (Exception e) {
			log.error("createClientOwnSessionInfo 初始化session异常！", e);
		}
	}

	/**
	 * 请求异常
	 * 
	 * @param response
	 * @param loginUsername
	 * @param casServerHost
	 */
	private void reBackError(ServletResponse response, String loginUsername, String casServerHost) {
		String source = "江波龙运营管理平台";
		String msg = "您的账号[" + loginUsername + "]在江波龙运营管理平台中尚未建立，请联系信息部金威(分机：832)帮您建立帐号";
		noUserInforamtion(response, casServerHost, source, msg);
	}

	private void noUserInforamtion(ServletResponse response, String casServerHost, String source, String msg) {
		try {
			String url = casServerHost + "/information?source=" + source + "&msg=" + msg;
			log.info("noUserInforamtion中url = " + url);
			((HttpServletResponse) response).sendRedirect(new String(url.getBytes("utf-8"), "iso-8859-1"));
		} catch (IOException e) {
			log.error("noUserInforamtion : 无人员返回 URL定向异常！", e);
		}
	}

	private JecnUser getUser(String loginUsername) throws Exception {
		if (personService == null) {
			personService = (IPersonService) ApplicationContextUtil.getContext().getBean("PersonServiceImpl");
		}
		List<JecnUser> list = personService.getUserByLoginName(loginUsername);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}
}
