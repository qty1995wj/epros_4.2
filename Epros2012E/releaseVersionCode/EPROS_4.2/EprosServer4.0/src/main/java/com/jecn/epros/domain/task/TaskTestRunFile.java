package com.jecn.epros.domain.task;

import java.sql.Blob;

/**
 * 试运行文件
 *
 * @author Administrator
 */
public class TaskTestRunFile implements java.io.Serializable {
    private Long id;
    /**
     * 任务主键Id
     */
    private Long taskId;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件内容
     */
    private byte[] fileStream;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileStream() {
        return fileStream;
    }

    public void setFileStream(byte[] fileStream) {
        this.fileStream = fileStream;
    }
}
