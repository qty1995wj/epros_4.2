<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div" style="overflow-y: scroll;height: 98%">
			<div id="inDetailDocument">
				<table align="left" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td width="120px" class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程名称： -->
							<s:text name="processNameC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							align="left" class="td_dashed">
							
							<s:property value="processBaseInfoBean.flowName" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程编号： -->
							<s:text name="processNumberC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:property value="processBaseInfoBean.flowIdInput" />
						</td>
					</tr>
					<s:if test="#request.processTypeIsShow">
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程类别： -->
							<s:text name="processTypeC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:property value="processBaseInfoBean.flowTypeName" />
						</td>
					</tr>
					</s:if>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程责任人： -->
							<s:text name="processResponsiblePersonsC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:property value="processBaseInfoBean.resPeopleName" />
						</td>
					</tr>
					<s:if test="processBaseInfoBean.showGuardian">
						<tr>
							<td class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 流程监护人： -->
								<s:text name="processTheGuardianC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								
								<s:property value="processBaseInfoBean.guardianName" />
							</td>
						</tr>
					</s:if>
					<s:if test="processBaseInfoBean.showFiction">
						<tr>
							<td class="td_dashed">
								<img src="${basePath}images/common/triangle.gif"
									style="vertical-align: middle;">
								<!-- 流程拟制人： -->
								<s:text name="processArtificialPersonC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								
								<s:property value="processBaseInfoBean.fictionPeopleName" />
							</td>
						</tr>
					</s:if>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 责任部门： -->
							<s:text name="responsibilityDepartmentC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<a
								href="organization.action?orgId=<s:property value='processBaseInfoBean.orgId'/>"
								target='_blank' style="cursor: pointer;" class="table"> <s:property
									value="processBaseInfoBean.orgName" /> </a>
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 有效期（月）： -->
							<s:text name="validityC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:if
								test="processBaseInfoBean.expiry == 0 || processBaseInfoBean.expiry ==''">
								<s:text name="permanent" />
							</s:if>
							<s:else>
								<s:property value="processBaseInfoBean.expiry" />
							</s:else>
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 密级： -->
							<s:text name="intensiveC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:if test="processBaseInfoBean.isPublic == 0">
								${secret}
							</s:if>
							<s:elseif test="processBaseInfoBean.isPublic == 1">
								<s:property value="#application.public"/>
							</s:elseif>
						</td>
					</tr>
					<s:if test="#request.supportToolsIsShow">
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 支持工具： -->
							<s:text name="supportTools" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:iterator value="processBaseInfoBean.listTools" status="st"
								id="tool">
						      [<s:property value="#tool" />]
						    </s:iterator>
						</td>
					</tr>
					</s:if>
					<s:if test="#request.accessoriesIsShow">
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 附件： -->
							<s:text name="accessories" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:if test="processBaseInfoBean.fileName!=null">
								<a
									href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='processBaseInfoBean.fileId'/>"
									style="cursor: pointer;" class="table" target='_blank'> <s:property
										value="processBaseInfoBean.fileName" /> </a>
							</s:if>
						</td>
					</tr>
					</s:if>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程ID： -->
							数据主键ID
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							
							<s:property value="processBaseInfoBean.flowId" />
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>