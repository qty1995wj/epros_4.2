package com.jecn.epros.domain.process;

import com.jecn.epros.sqlprovider.server3.Common;

import java.math.BigDecimal;

public class FlowActiveBaseBean {
    /**
     * 活动ID
     */
    private Long id;
    /**
     * 活动编号
     */
    private String activeNum;
    /**
     * 活动名称
     */
    private String activeName;
    /**
     * 活动说明
     */
    private String activeShow;
    /**
     * 角色
     */
    private String roleName;
    /**
     * 活动类别名称
     */
    private String activeTypeName;
    /**
     * 输入说明
     */
    private String activityInput;
    /**
     * 输出说明
     */
    private String activityOutput;


    /**
     * 1为PA,2为KSF,3为KCP,4合规
     */
    private String activeType;
    /**
     * 关键说明
     */
    private String activeKeyShow;

    /**
     * 流程ID
     */
    private Long flowId;
    /**
     * 流程名称
     */
    private String flowName;
    /************** 活动时间 ***************************/
    /**
     * 活动开始时间
     */
    private String startTime;
    /**
     * 活动结束时间
     */
    private String stopTime;
    /**
     * 活动时间类型
     */
    private Integer timeType;
    /************** 办理时限 ***************************/
    /**
     * 目标值
     */
    private BigDecimal targetValue;
    /**
     * 现状值
     */
    private BigDecimal statusValue;
    /**
     * 说明
     */
    private String explain;
    /**
     * 活动担当
     */
    private String customOne;
    /**
     * 是否显示活动担当
     */
    private boolean showCustomOne;
    /**
     * 是否现实活动类别
     */
    private boolean isShowActivityType;

    public boolean isShowActivityType() {
        return isShowActivityType;
    }

    public void setShowActivityType(boolean showActivityType) {
        isShowActivityType = showActivityType;
    }

    public String getActivityInput() {
        return Common.replaceWarp(activityInput);
    }

    public void setActivityInput(String activityInput) {
        this.activityInput = activityInput;
    }

    public String getActivityOutput() {
        return Common.replaceWarp(activityOutput);
    }

    public void setActivityOutput(String activityOutput) {
        this.activityOutput = activityOutput;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActiveNum() {
        return activeNum;
    }

    public void setActiveNum(String activeNum) {
        this.activeNum = activeNum;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public String getActiveShow() {
        return Common.replaceWarp(activeShow);
    }

    public void setActiveShow(String activeShow) {
        this.activeShow = activeShow;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getActiveTypeName() {
        return activeTypeName;
    }

    public void setActiveTypeName(String activeTypeName) {
        this.activeTypeName = activeTypeName;
    }

    public String getActiveType() {
        return Common.getActivityKeyTypeName(activeType);
    }

    public void setActiveType(String activeType) {
        this.activeType = activeType;
    }

    public String getActiveKeyShow() {
        return Common.replaceWarp(activeKeyShow);
    }

    public void setActiveKeyShow(String activeKeyShow) {
        this.activeKeyShow = activeKeyShow;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public BigDecimal getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(BigDecimal targetValue) {
        this.targetValue = targetValue;
    }

    public BigDecimal getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(BigDecimal statusValue) {
        this.statusValue = statusValue;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public String getCustomOne() {
        return customOne;
    }

    public void setCustomOne(String customOne) {
        this.customOne = customOne;
    }

    public boolean isShowCustomOne() {
        return showCustomOne;
    }

    public void setShowCustomOne(boolean showCustomOne) {
        this.showCustomOne = showCustomOne;
    }
}
