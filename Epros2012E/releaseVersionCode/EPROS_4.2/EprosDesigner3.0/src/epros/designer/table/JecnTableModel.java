package epros.designer.table;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

/**
 * @author yxw 2012-5-3
 * @description：Table的model
 */
public class JecnTableModel extends DefaultTableModel {

	private int editCol = -1;

	public JecnTableModel(int editCol, Vector<String> title,
			Vector<Vector<String>> content) {
		this.editCol = editCol;
		this.setDataVector(content, title);
	}

	public JecnTableModel(Vector<String> title, Vector<Vector<String>> content) {
		this.setDataVector(content, title);
	}

	/**
	 * @author yxw 2012-5-3
	 * @description:设置Table单元格是否编辑
	 * @row
	 * @column
	 * @return
	 */
	@Override
	public boolean isCellEditable(int row, int column) {
		if (editCol >= 0) {
			if (column == editCol)
				return true;
		}
		return false;
	}
}
