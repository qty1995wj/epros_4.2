package epros.designer.gui.rule.mode;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 更新 目录名称
 * 
 * @author zhangjie 2012-05-09
 * 
 */
public class EditRuleModelDirNameDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(EditRuleModelDirNameDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public EditRuleModelDirNameDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		// 窗体居中显示
		this.setLocationRelativeTo(null);
		this.setName(pNode.getJecnTreeBean().getName());
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("updateName");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		// 数据库更新目录名称
		try {
			ConnectionPool.getRuleModeAction().updateName(getName(), pNode.getJecnTreeBean().getId(),
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			// 重命名，树节点上目录名称的更新
			JecnTreeCommon.reNameNode(jTree, pNode, this.getName());
			this.dispose();
		} catch (Exception e) {
			log.error("EditRuleModelDirNameDialog saveData is error", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name, pNode);
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
