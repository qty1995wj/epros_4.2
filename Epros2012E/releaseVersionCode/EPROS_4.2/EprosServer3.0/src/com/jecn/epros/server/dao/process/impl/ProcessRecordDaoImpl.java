package com.jecn.epros.server.dao.process.impl;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowRecord;
import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;

public class ProcessRecordDaoImpl extends AbsBaseDao<JecnFlowRecordT, Long>
		implements IProcessRecordDao {

	@Override
	public List<JecnFlowRecordT> getJecnFlowRecordTListByFlowId(Long flowId)
			throws Exception {
		String hql = "from JecnFlowRecordT where flowId=?";
		return this.listHql(hql, flowId);
	}
	
	@Override
	public List<JecnFlowRecord> getJecnFlowRecordListByFlowId(Long flowId)
			throws Exception {
		String hql = "from JecnFlowRecord where flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
    //	记录保存数据 0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式 6编号 7移交责任人
	public List<Object[]> getFlowRecordListByFlowId(Long flowId, boolean isPub)
			throws Exception {
		if (isPub) {
			String hql = "select recordName,recordSavePeople,saveLaction,fileTime,saveTime,recordApproach,docId,recordTransferPeople from JecnFlowRecord where flowId=?";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select recordName,recordSavePeople,saveLaction,fileTime,saveTime,recordApproach,docId,recordTransferPeople from JecnFlowRecordT where flowId=?";
			return this.listHql(hql, flowId);
		}
	}

}
