package com.jecn.epros.server.util;

public class JecnTreeEnumConstant {
	public enum TreeNodeType {
		project, // 项目
		processRoot, // 流程根节点
		processMap, // 流程地图
		processMapRelation, // 集成关系图
		process, // 流程
		processFile,// 流程（文件）
		modeRoot, // 模板树根节点
		processModeRoot, // 流程模板根节点
		processMapMode, // 流程地图模板
		processMode, // 流程图模板
		processModeFileRoot, // 流程模板文件根节点
		processModeFileDir, // 流程模板文件目录
		processModeFile, // 流程模板文件
		organizationRoot, // 组织根节点
		organization, // 组织
		position, // 岗位
		person, // 人员
		standardRoot, // 标准根节点
		standardDir, // 标准目录
		standard, // 文件标准
		standardProcess, // 流程标准
		standardProcessMap, // 流程地图标准
		standardClause, // 标准条款
		standardClauseRequire, // 条款要求
		ruleRoot, // 制度根节点
		ruleDir, // 制度目录
		ruleFile, // 制度文件
		ruleModeFile, // 制度模板文件
		fileRoot, // 文件根节点
		fileDir, // 文件目录
		file, // 文件
		positionGroupRoot, // 岗位组根节点
		positionGroupDir, // 岗位组目录
		positionGroup, // 岗位组
		roleRoot, // 角色根节点
		roleDir, // 角色目录
		role, // 角色
		roleDefaultDir, // 默认角色目录
		roleDefault, // 默认角色节点
		roleSecondAdminDefaultDir, // 默认二级管理员节点目录
		roleSecondAdmin, toolRoot, // 支持工具根节点
		tool, // 支持工具
		ruleModeRoot, // 制度模板根节点
		ruleModeDir, // 制度模板目录
		ruleMode, // 制度模板
		riskRoot, // 风险根目录
		riskDir, // 风险目录
		riskPoint, // 风险点
		innerControlRoot, // 内控根目录
		innerControlDir, // 内控目录
		innerControlClause, // 内控条款
		posGroupRelPos, // 岗位组关联岗位类型
		processCodeRoot, // 过程代码 tree根节点
		processCode,
		termDefineDir,
		termDefineRoot, // 术语定义目录
		termDefine
		// 术语定义
	}

}
