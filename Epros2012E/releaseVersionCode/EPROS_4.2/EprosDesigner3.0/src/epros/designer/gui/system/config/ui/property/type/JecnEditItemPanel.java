package epros.designer.gui.system.config.ui.property.type;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ButtonCommendEnum;

import epros.designer.gui.system.config.ui.bottom.JecnOKCancelJButtonPanel;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnEditItemJDialog;
import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 编辑按钮弹出对话框中主面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEditItemPanel extends JecnPanel implements ActionListener, CaretListener {

	private final Log log = LogFactory.getLog(JecnEditItemPanel.class);

	/** 配置界面 */
	private JecnAbtractBaseConfigDialog dialog = null;
	/** 配置界面 */
	private JecnEditItemJDialog editItemJDialog = null;
	/** 确认取消按钮面板 */
	private JecnOKCancelJButtonPanel okCancelBtnAddItemPanel = null;

	/** 名称标签 */
	private JLabel nameLabel = null;
	/** 名称标签 */
	private JLabel enNameLabel = null;
	/** 名称输入框 */
	private JTextField nameTextFeild = null;
	/** 英文名称输入框 */
	private JTextField enNameTextFeild = null;
	/** 必填*标签 */
	private JLabel isNotLabel = null;
	/** 必填*标签 */
	private JLabel isEnNotLabel = null;
	/** 提示信息框 */
	// private JecnUserInfoTextArea infoTextArea = null;
	private JLabel textLabel = null;
	/** 选中数据对象 */
	private JecnConfigItemBean selectedConfigItemBean = null;

	public JecnEditItemPanel(JecnAbtractBaseConfigDialog dialog, JecnEditItemJDialog editItemJDialog) {
		if (dialog == null || editItemJDialog == null) {
			throw new IllegalArgumentException("JecnEditItemPanel is error");
		}

		this.dialog = dialog;
		this.editItemJDialog = editItemJDialog;

		initComponents();
		initLayout();

		// 初始化值
		initData();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	protected void initComponents() {
		// 名称标签
		nameLabel = new JLabel(JecnProperties.getValue("nameC"));
		enNameLabel = new JLabel(JecnProperties.getValue("enNameC"));
		// 名称输入框
		nameTextFeild = new JTextField();
		enNameTextFeild = new JTextField();
		// 必填*标签
		isNotLabel = new JLabel(JecnProperties.getValue("required"));
		isEnNotLabel = new JLabel(JecnProperties.getValue("required"));
		// 提示信息框
		textLabel = new JLabel();

		// 提示信息框
		// infoTextArea = new JecnUserInfoTextArea();
		// 确认取消按钮面板
		okCancelBtnAddItemPanel = new JecnOKCancelJButtonPanel(dialog);

		this.setLayout(new GridBagLayout());
		this.setBorder(null);

		textLabel.setForeground(Color.red);
		textLabel.setVerticalTextPosition(SwingConstants.CENTER);
		textLabel.setHorizontalTextPosition(SwingConstants.CENTER);

		isNotLabel.setForeground(Color.red);
		isNotLabel.setVerticalTextPosition(SwingConstants.CENTER);
		isNotLabel.setHorizontalTextPosition(SwingConstants.CENTER);

		isEnNotLabel.setForeground(Color.red);
		isEnNotLabel.setVerticalTextPosition(SwingConstants.CENTER);
		isEnNotLabel.setHorizontalTextPosition(SwingConstants.CENTER);

		// 事件
		nameTextFeild.addCaretListener(this);
		enNameTextFeild.addCaretListener(this);
		okCancelBtnAddItemPanel.getOkJButton().addActionListener(this);
		okCancelBtnAddItemPanel.getCancelJButton().addActionListener(this);

		// 按钮命令
		okCancelBtnAddItemPanel.getOkJButton().setActionCommand(ButtonCommendEnum.addOKBtn.toString());
		okCancelBtnAddItemPanel.getCancelJButton().setActionCommand(ButtonCommendEnum.addCancelBtn.toString());
	}

	private void initLayout() {

		Insets insetsLeft = new Insets(26, 17, 0, 3);
		Insets insetsRight = new Insets(26, 3, 0, 17);
		Insets insetsInfo = new Insets(0, 3, 0, 17);

		// 名称标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		this.add(nameLabel, c);
		// 名称输入框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(26, 3, 0, 3), 0, 0);
		this.add(nameTextFeild, c);
		// 必填*标签
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				26, 0, 0, 17), 0, 0);
		this.add(isNotLabel, c);
		// 名称标签
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
				insetsLeft, 0, 0);
		this.add(enNameLabel, c);
		// 英文名称输入框
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(26, 3, 0, 3), 0, 0);
		this.add(enNameTextFeild, c);

		// 必填*标签
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				26, 0, 0, 17), 0, 0);
		this.add(isEnNotLabel, c);

		// 提示信息框
		c = new GridBagConstraints(0, 3, 3, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
				insetsInfo, 0, 0);
		this.add(textLabel, c);

		// 确认取消按钮面板
		c = new GridBagConstraints(0, 3, 3, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(okCancelBtnAddItemPanel, c);

	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData() {
		JecnTableScrollPane tableScrollPane = editItemJDialog.getTableScrollPane();

		List<JecnConfigItemBean> iTemBeanList = tableScrollPane.getSelectedItemBeanList();

		if (iTemBeanList.size() != 1) {
			log.error("JecnEditItemPanel initData is error");
			throw new IllegalArgumentException("JecnEditItemPanel initData is error");
		}
		selectedConfigItemBean = iTemBeanList.get(0);
		nameTextFeild.setText(selectedConfigItemBean.getName());
		enNameTextFeild.setText(selectedConfigItemBean.getEnName());
	}

	/**
	 * 
	 * 点击事件处理方法
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (ButtonCommendEnum.addOKBtn.toString().equals(btn.getActionCommand())) {// 确认按钮
				okActionPerformed(e);
			} else if (ButtonCommendEnum.addCancelBtn.toString().equals(btn.getActionCommand())) {// 取消按钮
				cancelActionPerformed(e);
			}
		}

	}

	/**
	 * 
	 * 确认按钮处理方法
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void okActionPerformed(ActionEvent e) {
		if (okCancelBtnAddItemPanel.getOkJButton().isEnabled()) {
			String zh_Info = checkName();
			String en_Info = checkEnName();
			if (StringUtils.isNotBlank(zh_Info)) {
				textLabel.setText(nameLabel.getText() + "" + zh_Info);
				return;
			}
			if (StringUtils.isNotBlank(en_Info)) {
				textLabel.setText(enNameLabel.getText() + "" + en_Info);
				return;
			}
			JecnTableScrollPane tableScrollPane = editItemJDialog.getTableScrollPane();
			tableScrollPane.editName(nameTextFeild.getText(), enNameTextFeild.getText());
			editItemJDialog.setVisible(false);
		}
	}

	/**
	 * 
	 * 取消按钮处理方法
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void cancelActionPerformed(ActionEvent e) {
		editItemJDialog.setVisible(false);
	}

	/**
	 * 
	 * 输入框输入事件
	 * 
	 */
	@Override
	public void caretUpdate(CaretEvent e) {
		/*
		 * boolean ret = StringUtils.isNotBlank(checkName()); boolean en_ret =
		 * StringUtils.isNotBlank(checkEnName()); JButton okBtn =
		 * okCancelBtnAddItemPanel.getOkJButton(); if (!ret || !en_ret) { if
		 * (okBtn.isEnabled()) { okBtn.setEnabled(false); } } else { if
		 * (!okBtn.isEnabled()) { okBtn.setEnabled(true); } }
		 */
	}

	/**
	 * 
	 * 在名称输入框中输入字符时，验证内容是否正确
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	private String checkName() {
		String text = nameTextFeild.getText();

		// 提示信息
		String info = null;
		if (DrawCommon.isNullOrEmtryTrim(text)) {// 空
			// 名称不能为空
			info = JecnUserCheckInfoData.getNameNotNull();
		} else if (JecnUserCheckUtil.getTextLength(text) > 20) {// 长度
			// 名称不能超过20个字符或10个汉字
			info = JecnUserCheckInfoData.getNameLengthInfo().replaceAll("122", "20").replaceAll("61", "10");
		} else if (checkNamesSame(text)) {
			// 名称重复
			info = JecnProperties.getValue("editNameSame");
		}

		if (!DrawCommon.isNullOrEmtryTrim(info)) {
			return info;
		}
		return "";
	}

	/**
	 * 
	 * 在名称输入框中输入字符时，验证内容是否正确
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	private String checkEnName() {
		String text = enNameTextFeild.getText();

		// 提示信息
		String info = null;
		if (DrawCommon.isNullOrEmtryTrim(text)) {// 空
			// 名称不能为空
			info = JecnUserCheckInfoData.getNameNotNull();
		} else if (JecnUserCheckUtil.getTextLength(text) > 20) {// 长度
			// 名称不能超过20个字符或10个汉字
			info = JecnUserCheckInfoData.getNameLengthInfo().replaceAll("122", "20").replaceAll("61", "10");
		} else if (checkNamesSame(text)) {
			// 名称重复
			info = JecnProperties.getValue("editNameSame");
		}

		if (!DrawCommon.isNullOrEmtryTrim(info)) {
			return info;
		}
		return "";
	}

	/**
	 * 
	 * 判断给定名称是否有重复
	 * 
	 * @param text
	 *            String 给定名称
	 * @return boolean true:参数为空或选中行不为1或有相同行；false：没有重复名称
	 */
	private boolean checkNamesSame(String text) {
		JecnTableScrollPane tableScrollPane = editItemJDialog.getTableScrollPane();

		// 给定名称是否有重复情况 true:参数为空或选中行不为1或有相同行；false：没有重复名称
		return tableScrollPane.existsNameSame(text);
	}
}