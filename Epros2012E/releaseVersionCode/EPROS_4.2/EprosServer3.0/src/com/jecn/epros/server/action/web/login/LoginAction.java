package com.jecn.epros.server.action.web.login;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.jecn.authentication.AuthenticationFirefoxRequestUtil;
import com.jecn.epros.server.action.web.login.ad.BDF.BDFLoginAction;
import com.jecn.epros.server.action.web.login.ad.addon.JecnAddonLoginAction;
import com.jecn.epros.server.action.web.login.ad.anjianzhiyuan.AnJianZhiYuanLoginAction;
import com.jecn.epros.server.action.web.login.ad.boyu.BoyuLoginAction;
import com.jecn.epros.server.action.web.login.ad.cetc10.Cetc10LoginAction;
import com.jecn.epros.server.action.web.login.ad.cetc29.Cetc29LoginAction;
import com.jecn.epros.server.action.web.login.ad.cms.CMSLoginAction;
import com.jecn.epros.server.action.web.login.ad.cnni.JecnCNNILoginAction;
import com.jecn.epros.server.action.web.login.ad.crrssdxc.CRS_SDXCLoginAction;
import com.jecn.epros.server.action.web.login.ad.csr.CsrLoginAction;
import com.jecn.epros.server.action.web.login.ad.csxw.CsxwLoginAction;
import com.jecn.epros.server.action.web.login.ad.datangyidong.DaTangYiDongLoginAction;
import com.jecn.epros.server.action.web.login.ad.dongh.DonghWebLoginAciton;
import com.jecn.epros.server.action.web.login.ad.envicool.EnvicoolLoginAction;
import com.jecn.epros.server.action.web.login.ad.epson.EpsonLoginAction;
import com.jecn.epros.server.action.web.login.ad.fenghuo.FengHuoLoginAction;
import com.jecn.epros.server.action.web.login.ad.fuyao.JecnFuYaoLoginAction;
import com.jecn.epros.server.action.web.login.ad.guangzhouCsr.GuangZhouCsrLoginAction;
import com.jecn.epros.server.action.web.login.ad.heertai.HeTaiLoginAction;
import com.jecn.epros.server.action.web.login.ad.henansiwei.HNSiWeiLoginAction;
import com.jecn.epros.server.action.web.login.ad.hisense.HisenseLoginAction;
import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekLoginAction;
import com.jecn.epros.server.action.web.login.ad.jiangbolong.JecnJiangBLWebLoginAction;
import com.jecn.epros.server.action.web.login.ad.jinbo.JinBoLoginAction;
import com.jecn.epros.server.action.web.login.ad.jinke.JinkeLoginAction;
import com.jecn.epros.server.action.web.login.ad.juguang.JecnJuGuangWebLoginAction;
import com.jecn.epros.server.action.web.login.ad.kinglong.KingLongLoginAction;
import com.jecn.epros.server.action.web.login.ad.kuka.KukaLoginAction;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LianHeDianZiLoginAction;
import com.jecn.epros.server.action.web.login.ad.libang.LiBangLoginAction;
import com.jecn.epros.server.action.web.login.ad.lingnan.LingNanLoginAction;
import com.jecn.epros.server.action.web.login.ad.mengniu.MengNiuLoginAction;
import com.jecn.epros.server.action.web.login.ad.mulinsen.MulinsenLoginAction;
import com.jecn.epros.server.action.web.login.ad.net99.Net99LoginAction;
import com.jecn.epros.server.action.web.login.ad.neusoft.NeusoftLoginAction;
import com.jecn.epros.server.action.web.login.ad.nfspring.NongFuSpringLoginAction;
import com.jecn.epros.server.action.web.login.ad.nine999.Nine999WebLoginAciton;
import com.jecn.epros.server.action.web.login.ad.powerlong.PowerLongLoginAction;
import com.jecn.epros.server.action.web.login.ad.sailun.SaiLunLoginAction;
import com.jecn.epros.server.action.web.login.ad.sanfu.SanfuReadSSOAction;
import com.jecn.epros.server.action.web.login.ad.sfCsr.SFCSRLoginAction;
import com.jecn.epros.server.action.web.login.ad.shih.ShihWebLoginAciton;
import com.jecn.epros.server.action.web.login.ad.topband.TopbandLoginAction;
import com.jecn.epros.server.action.web.login.ad.ultimus.UltimusLoginAction;
import com.jecn.epros.server.action.web.login.ad.wanhua.WanHuaLoginAction;
import com.jecn.epros.server.action.web.login.ad.yuexiu.YueXiuLoginAction;
import com.jecn.epros.server.action.web.login.ad.zhongdiantou.ZhongDTLoginAction;
import com.jecn.epros.server.action.web.login.ad.zhongruitongxin.ZhongRuiLoginAction;
import com.jecn.epros.server.action.web.login.ad.zhongsh.ZhongSHLoginAtion;
import com.jecn.epros.server.action.web.login.ad.zhongsht.JecnZhongShTWebLoginAction;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.connector.AbstractSinglePointBean;
import com.jecn.epros.server.connector.SinglePointBean;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.system.IJecnUserLoginLogService;
import com.jecn.epros.server.service.task.search.IJecnTaskSearchService;
import com.jecn.epros.server.util.JecnUserSecurityKey;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * 用户登录
 * 
 * @author ZHOUXY
 * 
 */
public class LoginAction extends BaseAction {
	private static final Logger log = Logger.getLogger(LoginAction.class);
	// 登录返回值类型 ：1：INPUT(登录界面) 2：main（管理员功能浏览主界面） 3：SUCCESS（主界面） 4：null/
	/** 登录返回值类型 ：main（管理员功能浏览主界面） */
	public static final String MAIN = "main";
	/** 自定义重定向 重定向URL需设置redirectURL属性值 */
	public static final String REDIRECT = "redirect";
	/** 跳转至对外Jsp(包括内嵌的流程体系、知识库) */
	public static final String EXTERNAL_RESOURCE = "externalResource";
	/** 登录返回值类型 ： */
	public static final String NULL = null;

	/** 我的任务，任务计划 搜索、删除、显示记录 service接口 */
	private IJecnTaskSearchService searchService;
	private IPersonService personService;
	private IJecnUserLoginLogService userLoginLogService;

	/** 登录名称 */
	private String loginName;
	/** 真实名称 */
	private String trueName;
	/** 登录密码 */
	private String password;
	/** 保持登录 */
	private String keepLogin;

	/** 单点登录对应的登录名称 */
	private String adLoginName;
	/** 单点登录对应的登录密码 */
	private String adPassWord;

	/** ********东航单点登录********* */
	/** 东航单点登录用户名 */
	private String UserID;
	/** 东航单点登录密码 */
	private String UserPwd;
	/** 东航单点登录 标记本次验证是否需要登录 true为需要登录，false为不需要登录 */
	private String Login = "false";
	/** ********东航单点登录********* */

	/** ****** 中国科学院计算机网络信息中心单点登录 start ******* */
	/** 中国科学院计算机网络信息中心：凭证 ；账号：loginName */
	private String token = null;
	/** ****** 中国科学院计算机网络信息中心单点登录 end ******* */

	/** ********邮件登录********* */
	/** 访问类型：邮件登录到审批页面:mailApprove;邮件登录到流程页面:mailPubApprove; */
	private String accessType = null;
	/** 人员id */
	private String mailPeopleId = null;
	/** 任务主键ID */
	private long mailTaskId;

	/** 流程图：process；流程地图：processMap */
	private String mailFlowType = null;
	/** 流程图/流程地图主键ID */
	private String mailFlowId = null;
	/** 流程图活动主键ID */
	private String mailActiveId = null;
	/** 制度主键ID */
	private String mailRuleId = null;
	/** 制度类型 */
	private String mailRuleType = null;
	/** 文件主键ID */
	private String mailFileId = null;

	/** 原密码 */
	String oldPwd = null;
	/** 新密码 */
	String newPwd = null;

	/** 是否单点登录 true:是 false：否 */
	private boolean isSSO = true;

	private String rtnlProposeId;

	private String handleFlag;

	/** 自定义的重定向地址 */
	private String customRedirectURL;
	/** 外部访问的页面资源内容类型 支持的结果在initExternalResource.js中的loadContent()方法查看 */
	private String externalResourceContentType;
	/**
	 * 用于标识邮件打开流程图方式时，根据target的值决定打开流程图的呈现方式：_self（本页面打开）_blank（新页面打开），
	 * 详细的介绍参见html中target属性
	 */
	private String target;

	private final int PASSWORD_LENGTH = 40;

	private String isPub = "true";
	/** input返回页面URL */
	private String inputUrl = JecnContants.NEW_APP_CONTEXT;

	private boolean isApp;

	private boolean isView;

	private String ticket;

	/** ********邮件登录********* */

	/**
	 * 
	 * 直接登录系统方式
	 * 
	 */
	public enum MailLoginEnum {
		mailApprove, // 任务审批：邮件登录到审批页面
		mailPubApprove, // 任务审批：邮件登录到流程页面
		mailOpenMap, // 邮件方式直接打开流程图/流程地图页面
		mailOpenActiveInfo, // 邮件方式直接打开活动明细页面
		mailOpenApprovingList, // 任务审批列表数据页面
		mailOpenButtPublicFlow, // 流程发布的信息数据显示界面(包括：我关注的流程、我参与的流程)
		mailOpenDominoRuleModelFile, // 华帝Domino平台打开制度模板文件页面
		mailOpenDominoMap, // 华帝domino平台打开流程图/流程地图页面
		mailOpenDominoMapSearchJSP, // 华帝Domino平台打开流程地图搜索页面
		mailOpenDominoFile, // 打开文件详情
		openApproveDomino, // 单点-打开任务审批页面
		mailOpenTaskManagement, // 管理员打开任务管理页面
		mailOpenRule, // 邮件方式直接打开制度模板/制度文件页面
		mailOpenFile, // 邮件方式直接打开文件
		mailProposeHandle, // 邮件方式处理合理化建议
		mailOpenPropose,
		// 邮件方式打开合理化建议
		gtasksCount, // 登录人待办任务总数
		myTaskJson, // 我的任务返回json
		numberOfBack, // 待办总数：输出纯数字
		myTaskXml, // 广机输出任务XML
		toDOTasks, // 待审批任务（不包含查阅） 只提供任务名称和连接地址
		toDOTasksCount,
		// 待审批任务（不包含查阅） 只提供任务名称和连接地址

		toDOTasksNCB, // 待审批任务（不包含查阅） 只提供任务名称和连接地址(不是跨域请求)
		toDOTasksCountNCB
		// 待审批任务（不包含查阅） 只提供任务名称和连接地址(不是跨域请求)
	}

	/**
	 * 
	 * 用户登录
	 * 
	 * @return String login;main;success;null
	 * 
	 */
	public String login() {
		if (isXss(loginName)) {// 请求特殊字符判断，防止XSS漏洞
			// 返回null不处理，（返回登录页面右会嵌入代码在login.jsp中，所以返回null不处理）
			return ERROR;
		}
		if (isXss(password)) {// 请求特殊字符判断，防止XSS漏洞
			// 返回null不处理，（返回登录页面右会嵌入代码在login.jsp中，所以返回null不处理）
			return ERROR;
		}

		String result = "login";
		//JecnContants.otherLoginType = 54;
		switch (JecnContants.otherLoginType) {
		case 0:// 0：普通登录（默认）
			result = loginGen();
			break;
		case 1:// 东航登录方式
			DonghWebLoginAciton dhLoginAction = new DonghWebLoginAciton(this);
			result = dhLoginAction.login();
			break;
		case 2:// 中国科学院计算机网络信息中心单点登录
			JecnCNNILoginAction cnniLoginAction = new JecnCNNILoginAction(this);
			result = cnniLoginAction.login();
			break;
		case 3:// 三幅百货单点登录
			SanfuReadSSOAction sanfuReadSSOAction = new SanfuReadSSOAction(this);
			result = sanfuReadSSOAction.login();
			break;
		case 4:// 南京物探院
			ShihWebLoginAciton shihWebLoginAciton = new ShihWebLoginAciton(this);
			result = shihWebLoginAciton.login();
			break;
		case 5:// 中数通信息有限公司
			JecnZhongShTWebLoginAction zhongShTWebLoginAction = new JecnZhongShTWebLoginAction(this);
			result = zhongShTWebLoginAction.login();
			break;
		case 6:// 聚光
			JecnJuGuangWebLoginAction juGuangWebLoginAction = new JecnJuGuangWebLoginAction(this);
			result = juGuangWebLoginAction.login();
			break;
		case 8:// 8：华帝
			JecnAddonLoginAction addonLoginAction = new JecnAddonLoginAction(this);
			result = addonLoginAction.login();
			break;
		case 9:// 9：中电投
			ZhongDTLoginAction zdtLoginAction = new ZhongDTLoginAction(this);
			result = zdtLoginAction.login();
			break;
		case 10:// 10:海信
			HisenseLoginAction hisenseLoginAction = new HisenseLoginAction(this);
			result = hisenseLoginAction.login();
			break;
		case 11:// 29所
			Cetc29LoginAction swieeLoginAction = new Cetc29LoginAction(this);
			result = swieeLoginAction.login();
			break;
		case 12:// 安健致远
			AnJianZhiYuanLoginAction ajzyLoginAction = new AnJianZhiYuanLoginAction(this);
			result = ajzyLoginAction.login();
			break;
		case 13:// 大唐移动“
			DaTangYiDongLoginAction dtydLoginAction = new DaTangYiDongLoginAction(this);
			result = dtydLoginAction.login();
			break;
		case 14:// 联合电子
			LianHeDianZiLoginAction lhdzLoginAction = new LianHeDianZiLoginAction(this);
			result = lhdzLoginAction.login();
			break;
		case 15:// 赛轮
			SaiLunLoginAction saiLunLoginAction = new SaiLunLoginAction(this);
			result = saiLunLoginAction.login();
			break;
		case 16:// 广州机电
			GuangZhouCsrLoginAction gzjdLoginAction = new GuangZhouCsrLoginAction(this);
			result = gzjdLoginAction.login();
			break;
		case 17:// 中睿通信
			result = new ZhongRuiLoginAction(this).login();
			break;
		case 18:// 江波龙
			result = new JecnJiangBLWebLoginAction(this).login();
			break;
		case 19: // 烽火通信
			result = new FengHuoLoginAction(this).login();
			break;
		case 20: // 中石化石勘院
			result = new ZhongSHLoginAtion(this).login();
			break;
		case 21:// 河南思维
			result = new HNSiWeiLoginAction(this).login();
			break;
		case 22:// 安码BPM
			result = new UltimusLoginAction(this).login();
			break;
		case 23:// 株洲中车-时代电气
			result = new CsrLoginAction(this).login();
			break;
		case 24:// 十所
			result = new Cetc10LoginAction(this).login();
			break;
		case 25:// 99网
			result = new Net99LoginAction(this).login();
			break;
		case 26:// 拓邦
			result = new TopbandLoginAction(this).login();
			break;
		case 27:// 醋酸纤维
			result = new CsxwLoginAction(this).login();
			break;
		case 28:// 立邦
			String ticket = this.getRequest().getParameter("Ticket");
			if(!"".equals(ticket)){
				result = new LiBangLoginAction(this).WXlogin();
			}else{
				result = new LiBangLoginAction(this).login();
			}
			break;
		case 29:// 农夫山泉
			result = new NongFuSpringLoginAction(this).login();
			break;
		case 30:// 东软医疗
			result = new NeusoftLoginAction(this).login();
			break;
		case 31:// 厦门金旅
			result = new KingLongLoginAction(this).login();
			break;
		case 33: // 青岛四方中车
			result = new SFCSRLoginAction(this).login();
			break;
		case 34:// 巴德富
			result = new BDFLoginAction(this).login();
			break;
		case 36:// 英维克
			result = new EnvicoolLoginAction(this).login();
			break;
		case 37:// 蒙牛
			result = new MengNiuLoginAction(this).login();
			break;
		case 38:// 爱普生
			result = new EpsonLoginAction(this).login();
			break;
		case 39:// 株洲中车时代新材
			result = new CRS_SDXCLoginAction(this).login();
			break;
		case 40:// 科大讯飞
			result = new JecnIflytekLoginAction(this).login();
			break;
		case 41:// 木林森
			result = new MulinsenLoginAction(this).login();
			break;
		case 42:// 招商证券
			result = new CMSLoginAction(this).login();
			break;
		case 43: // 和而泰
			result = new HeTaiLoginAction(this).login();
			break;
		case 44:// 金博
			result = new JinBoLoginAction(this).login();
			break;
		case 45: // 福耀
			result = new JecnFuYaoLoginAction(this).login();
			break;
		case 46:// 万华
			result = new WanHuaLoginAction(this).login();
			break;
		case 47:// 华润九新
			result = new Nine999WebLoginAciton(this).login();
			break;
		case 48:// 贝豪婴童
			result = loginGen();
			break;
		case 49:// 宝龙地产
			result = new PowerLongLoginAction(this).login();
			break;
		case 50:// 金科地产
			log.error("jinke");
			result = new JinkeLoginAction(this).login();
			break;
		case 51:// 博宇
			result = new BoyuLoginAction(this).login();
			break;
		case 52:// 越秀地产
			result = new YueXiuLoginAction(this).login();
			break;		
		case 53:// 顾家家居
			result = new KukaLoginAction(this).login();
			break;
		case 54:// 岭南股份
			result = new LingNanLoginAction(this).login();
			break;
		default:// 普通登录（默认）
			result = loginGen();
			break;
		}
		if (result == REDIRECT) {
			return result;
		}

		if (!JecnUtil.isVersionType() && MAIN.equals(result)) {// 非完整版登录成功返回我的主页
			result = SUCCESS;
		}
		// 登录失败时，清除cookie信息
		loginFailClearCookie();
		// 设置对外JSP的资源内容类别
		setExternalResourceContentType(this.getRequest().getParameter("epros_forward"));
		if (loginSuccess() && needForwardToExternalJsp()) {
			return EXTERNAL_RESOURCE;
		}
		return result;
	}

	/**
	 * 是否需要跳转至对外JSP页面
	 * 
	 * @return
	 */
	private boolean needForwardToExternalJsp() {
		return StringUtils.isNotBlank(externalResourceContentType);
	}

	/**
	 * 
	 * 普通登录方式
	 * 
	 * @return String 登录返回值类型 ：1：INPUT(登录界面) 2：main（管理员功能浏览主界面） 3：SUCCESS（主界面）
	 *         4：null
	 */
	public String loginGen() {
		isSSO = false;
		return userLoginGen(true);
	}

	/**
	 * 
	 * 邮件登录方式
	 * 
	 * @return String error：验证失败
	 */
	public String userLoginEMail() {
		// 返回标识
		String result = ERROR;
		try {
			MailLoginEnum mailLoginEnum = MailLoginEnum.valueOf(accessType);
			if (mailLoginEnum != null) {
				// IE跳转火狐特殊处理(株洲中车)
				if (AuthenticationFirefoxRequestUtil.requestToFirefox(this.getRequest(), this.getResponse())) {
					return null;
				}
				// IE跳转谷歌特殊处理(巴德富 )
				if (AuthenticationFirefoxRequestUtil.requestToChrome(this.getRequest(), this.getResponse())) {
					return null;
				}
				// 邮件登录，特殊处理
				result = signleLoginHandle(mailLoginEnum);
			}
		} catch (Exception e) {
			log.error("获取accessType对应枚举异常。accessType=" + accessType, e);
			if (MailLoginEnum.mailOpenApprovingList.toString().equals(accessType)) {
				result = "errorButt";
			}
		}
		// 登录失败时，清除cookie信息
		loginFailClearCookie();

		return result;
	}

	/**
	 * 任务代办接口(此接口用于，CAS 过滤器之前执行，XML中应添加此action地址)
	 * 
	 * @return
	 */
	public String taskLogin() {
		return userLoginEMail();
	}

	/**
	 * 单点验证处理
	 * 
	 * @param mailLoginEnum
	 * @return
	 */
	private String signleLoginHandle(MailLoginEnum mailLoginEnum) throws Exception {
		String result = null;
		switch (MailLoginEnum.valueOf(accessType)) {
		case mailOpenApprovingList:
		case mailOpenButtPublicFlow:
		case mailOpenDominoMap:
		case mailOpenDominoRuleModelFile:
		case mailOpenDominoMapSearchJSP:
		case mailOpenDominoFile:
		case openApproveDomino:// 单点打开审批
			// 走单点认证请求
			result = getMailResultEnumString();
			break;
		case gtasksCount:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.gtasksCount.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getGtasksCount();
				return null;
			}
			break;
		case myTaskJson:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.myTaskJson.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getMyTaskJson();
				return null;
			}
			break;
		case numberOfBack:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.numberOfBack.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getNumberOfBacklog();
				return null;
			}
			break;
		case myTaskXml:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.myTaskXml.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getMyTaskXml();
				return null;
			}
			break;
		case toDOTasksCount:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.toDOTasksCount.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getToDotasksCount();
				return null;
			}
			break;
		case toDOTasks:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.toDOTasks.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getToDOTasksJson();
				return null;
			}
			break;
		case toDOTasksNCB:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.toDOTasksNCB.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getToDOTasksJsonNoCallBack();
				return null;
			}
			break;
		case toDOTasksCountNCB:
			result = getMailResultEnumString();
			if (result.equals(MailLoginEnum.toDOTasksCountNCB.toString())) {
				// 登录成功，获取任务总数
				getSinglePointBean().getToDotasksCountNoCallBack();
				return null;
			}
			break;
		default:
			if (userLoginEMailProcess()) {// 验证成功
				result = accessType;
			}
			break;
		}
		return result;
	}

	private String getMailResultEnumString() {
		// 登录验证
		String ret = login();
		if (MAIN.equals(ret) || SUCCESS.equals(ret)) {// 登录成功
			return accessType;
		} else if (StringUtils.isNotBlank(accessType) && LoginAction.INPUT.equals(ret)) {// 邮件跳转
			return "error";
		} else {
			return "errorButt";
		}
	}

	/**
	 * 
	 * 邮件登录方式
	 * 
	 * @return boolean true 登录成功；false：登录失败
	 */
	private boolean userLoginEMailProcess() {
		try {
			if (StringUtils.isBlank(mailPeopleId)) {
				if (StringUtils.isNotBlank(adLoginName)) {
					List<JecnUser> userList = getPersonService().getUserByLoginName(adLoginName);

					if (userList.size() != 1) {
						log.error("通过登录名称获取人员信息不是只有一条数据：loginName=" + adLoginName);
						return false;
					}
					JecnUser jecnUser = userList.get(0);
					// 给登录名称/密码赋值
					setLoginName(jecnUser.getLoginName());
					setPassword(jecnUser.getPassword());

					// 验证登录
					String ret = userLoginGen(false);
					if (MAIN.equals(ret) || SUCCESS.equals(ret)) {// 验证成功
						return true;
					} else {
						return false;
					}
				}
				return false;
			}

			JecnUser jecnUser = personService.getUserById(Long.valueOf(mailPeopleId));
			if (jecnUser == null) {
				return false;
			}
			// 给登录名称/密码赋值
			loginName = jecnUser.getLoginName();
			// password = jecnUser.getPassword();

			// 获取用户信息，添加到session
			String ret = userLoginGen(false);

			if (MAIN.equals(ret) || SUCCESS.equals(ret)) {// 验证成功
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("", e);
			return false;
		}
	}

	/**
	 * 
	 * 登录页面登录
	 * 
	 * @param pwdType
	 *            true为验证密码，false为不验证密码
	 * @return nulland1 LOGIN 2 main 3 SUCCESS 4 null
	 */
	public String userLoginGen(boolean pwdType) {
		// 用户信息
		WebLoginBean webLoginBean = null;

		// session中用户信息map集合
		// Map<String, Object> sessionMap =
		// ActionContext.getContext().getSession();
		// 从session中获取用户信息
		// Object obj = sessionMap.get(JecnContants.SESSION_KEY);
		// if (obj != null && obj instanceof WebLoginBean) {// 用户信息存在情况
		// webLoginBean = (WebLoginBean) obj;
		// if (webLoginBean.isAdmin()) {// 是管理员
		// return MAIN;
		// } else {
		// return SUCCESS;
		// }
		// }

		// 登录名称，密码只要有一个为空,直接返回
		boolean flag = false;
		if (pwdType) {
			if (StringUtils.isBlank(loginName) || StringUtils.isBlank(password)) {
				flag = true;
			} else {
				Pattern pat = Pattern.compile("[\u4E00-\u9FA5]");
				if (pat.matcher(password).find()) {
					clearCookie();
					this.addFieldError("loginMessage", getText("nameOrPasswordError"));
					return INPUT;
				}
			}
		} else {
			if (StringUtils.isBlank(loginName)) {
				flag = true;
			}
		}
		if (flag) {
			this.addFieldError("loginMessage", getText("nameOrPasswordBlank"));
			// 清除Cookie
			clearCookie();
			return INPUT;
		}

		// 第一次登录执行代码
		try {
			// cookie方式登录先解密密码，其他正常使用
			password = getDecodePwd(password);

			if (JecnUtil.getTextLength(password) > PASSWORD_LENGTH) {
				this.addFieldError("loginMessage", getText("passwordLengthWarn"));
				log.error("保持登录情况下，密码长度超过40位");
				clearCookie();
				return INPUT;
			}

			// 从DB中获取用户信息
			webLoginBean = personService.getWebLoginBean(loginName, password, pwdType);
			// 添加用户信息到session
			String ret = addUserInfo(webLoginBean);
			if (JecnContants.ERROR_NOT_PROJECT.equals(ret)) {
				ret = INPUT;
			}
			return ret;
		} catch (Exception e) {
			log.error("用户登录出错", e);
			clearCookie();
			return INPUT;
		}
	}

	/**
	 * 
	 * 添加用户信息到session
	 * 
	 * @param webLoginBean
	 * @return
	 * @throws Exception
	 */
	public String addUserInfo(WebLoginBean webLoginBean) throws Exception {
		// 0为用户密码不正确，1是登陆成功
		if (webLoginBean.getLoginState() == 1) {// 登录成功

			if (webLoginBean.getProjectName() == null) {// 项目不存在
				clearCookie();
				this.addFieldError("loginMessage", getText("noMainProject"));
				return JecnContants.ERROR_NOT_PROJECT;
			}

			if (!isSSO) {// 普通登录且非大唐移动
				// /判断是否是默认密码
				String defPwd = JecnUserSecurityKey.getsecret("123456").trim();
				if (defPwd.equals(webLoginBean.getJecnUser().getPassword())) {
					// this.loginPeopleId =
					// webLoginBean.getJecnUser().getPeopleId();
					this.getSession().setAttribute("loginPeopleId", webLoginBean.getJecnUser().getPeopleId());
					oldPwd = webLoginBean.getJecnUser().getPassword();
					// 原密码解密
					oldPwd = JecnUserSecurityKey.getdesecret(oldPwd);
					return "updatepassword";
				}
			}

			// ////////////前台页面使用参数//////////////
			HttpServletRequest request = getRequest();
			HttpSession session = getSession();
			String path = request.getContextPath();
			String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
			// 海信登录情况下设置BasePath为 ""
			// 问题：海信门户网站集成时，指定了epros系统上下文环境为epros，
			// 门户网站跳转到epros系统不能以绝对路径访问，所以这把basepath值空
			if (JecnContants.otherLoginType == 10) {
				basePath = "";
			}
			session.setAttribute("basePath", basePath);
			session.setAttribute("language", "zh");
			session.setAttribute("country", "CN");
			// ////////////前台页面使用参数//////////////

			// 用户信息存储到session中
			ActionContext.getContext().getSession().put(JecnContants.SESSION_KEY, webLoginBean);

			// 添加登录日志
			userLoginLogService
					.addUserLoginLog(webLoginBean.getJecnUser().getPeopleId(), session.getId().toString(), 0);
			if ("keepLogin".equals(keepLogin)) {// 是保存登录时，记录cookie
				writeCookie();
			}
			if (webLoginBean.isAdmin()) {// 是管理员
				return MAIN;
			} else {
				return SUCCESS;
			}

		} else {// 登录失败
			clearCookie();
			// 安健致远登录时，用户不存在Epros系统中时
			if (JecnContants.otherLoginType == 12) {
				this.addFieldError("loginMessage", getText("anJianZhiYuanNoAuthToLogin"));
			} else {
				this.addFieldError("loginMessage", getText("nameOrPasswordError"));
			}
			return INPUT;
		}
	}

	/**
	 * 修改密码
	 * 
	 * @return
	 */
	public String updateLoginPwd() {
		// 获取界面密码信息
		// 更新数据库对应用户密码
		try {
			Long loginPeopleId = (Long) this.getSession().getAttribute("loginPeopleId");
			if (loginPeopleId == null) {
				return null;
			}
			JecnUser user = personService.get(Long.valueOf(loginPeopleId));
			HttpServletRequest request = ServletActionContext.getRequest();
			oldPwd = request.getParameter("oldPassword");
			newPwd = request.getParameter("newPassword");
			String reNewPassword = request.getParameter("reNewPassword");
			Pattern pat = Pattern.compile("[\u4E00-\u9FA5]");
			// 原密码验证
			if (oldPwd == null || "".equals(oldPwd)) {
				this.addFieldError("oldPwdPerf", getText("oldPwdNotNull"));
				return ERROR;
			} else if (pat.matcher(oldPwd).find()) {
				this.addFieldError("oldPwdPerf", getText("newPwdNoCN"));
				return ERROR;
			} else if (!user.getPassword().equals(JecnUserSecurityKey.getsecret(oldPwd.trim()))) {
				this.addFieldError("oldPwdPerf", getText("oldPwdNotRight"));
				return ERROR;
			}
			// 新密码验证
			if (newPwd == null || "".equals(newPwd)) {
				this.addFieldError("newPwdPerf", getText("newPwdNotnull"));
				return ERROR;
			} else if (JecnUtil.getTextLength(newPwd) > PASSWORD_LENGTH) {// 密码长度限制
				this.addFieldError("newPwdPerf", getText("newPwdLong"));
				return ERROR;
			} else if (pat.matcher(newPwd).find()) {
				this.addFieldError("newPwdPerf", getText("newPwdNoCN"));
				return ERROR;
			}
			// 确认密码验证
			if (reNewPassword == null || "".equals(reNewPassword)) {
				this.addFieldError("reNewPwdPerf", getText("reNewPwdNotnull"));
				return ERROR;
			}
			if (!newPwd.equals(reNewPassword)) {
				this.addFieldError("reNewPwdPerf", getText("twoNewPwdNotSame"));
				return ERROR;
			}

			newPwd = JecnUserSecurityKey.getsecret(newPwd.trim()).trim();
			if (user.getPassword().equals(JecnUserSecurityKey.getsecret(oldPwd.trim()))) {

				personService.userPasswordUpdate(Long.valueOf(loginPeopleId), newPwd);
				return SUCCESS;
				// // 修改成功
				// outJsonString("{success:true,msg:1}");
			} else {
				// 原密码错误
				// outJsonString("{success:true,msg:2}");

			}
		} catch (Exception e) {
			// 修改密码异常！
			// outJsonString("{success:true,msg:3}");
			log.error(e);
			return null;
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2013-3-8
	 * @description:写cookie
	 */
	private void writeCookie() {
		try {
			String pass = new BASE64Encoder().encodeBuffer(password.getBytes()).trim();
			Cookie namecookie = new Cookie("loginName", URLEncoder.encode(loginName, "UTF-8"));

			Cookie passwordcookie = new Cookie("password", pass);
			Cookie checkBoxcookie = new Cookie("keepLogin", keepLogin);
			int saveDate = 60 * 60 * 24 * 365;
			namecookie.setMaxAge(saveDate);
			passwordcookie.setMaxAge(saveDate);
			checkBoxcookie.setMaxAge(saveDate);
			getResponse().addCookie(namecookie);
			getResponse().addCookie(passwordcookie);
			getResponse().addCookie(checkBoxcookie);
		} catch (Exception ex) {
			log.error("", ex);
		}

	}

	private boolean existsCookie() {
		Cookie cookies[] = getRequest().getCookies();
		if (cookies != null) {
			for (Cookie c : cookies) {
				if ("keepLogin".equals(c.getName()) && "keepLogin".equals(c.getValue())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @author yxw 2013-3-8
	 * @description:清楚Cookie
	 */
	private void exitClearCookie() {

		if (existsCookie()) {// 是保存登录时，记录cookie
			try {
				Cookie namecookie = new Cookie("loginName", null);
				Cookie passwordcookie = new Cookie("password", null);
				Cookie checkBoxcookie = new Cookie("keepLogin", null);
				namecookie.setMaxAge(0);
				passwordcookie.setMaxAge(0);
				checkBoxcookie.setMaxAge(0);
				getResponse().addCookie(namecookie);
				getResponse().addCookie(passwordcookie);
				getResponse().addCookie(checkBoxcookie);
			} catch (Exception ex) {
				log.error("", ex);
			}
		}
	}

	/**
	 * @author yxw 2013-3-8
	 * @description:清楚Cookie
	 */
	private void clearCookie() {
		if ("keepLogin".equals(keepLogin)) {
			exitClearCookie();
		}
	}

	/**
	 * 
	 * 登录失败后清除cookie信息
	 * 
	 */
	private void loginFailClearCookie() {
		if ("keepLogin".equals(keepLogin)) {// 保持登录情况
			if (!loginSuccess()) {
				this.clearCookie();
			}
		}
	}

	/**
	 * 是否登录成功
	 * 
	 * @return
	 */
	private boolean loginSuccess() {
		Object obj = this.getSession().getAttribute(JecnContants.SESSION_KEY);
		if (obj != null && obj instanceof WebLoginBean) {// 用户信息存在情况
			WebLoginBean webLoginBean = (WebLoginBean) obj;
			if (webLoginBean.getLoginState() == 1) {// 1是登陆成功,非1情况都失败
				return true;
			}
		}
		return false;
	}

	/**
	 * 退出登录
	 * 
	 * @author fuzhh Jan 25, 2013
	 * @return
	 */
	public String exit() {
		// 清空session
		Map<String, Object> sessionMap = ActionContext.getContext().getSession();
		if (sessionMap != null) {
			sessionMap.clear();
		}
		HttpSession session = getSession();

		if (session != null) {
			getSession().invalidate();
		}
		log.info("EXIT 退出成功！");
		exitClearCookie();
		return SUCCESS;
	}

	/**
	 * 
	 * cookie方式登录先解密密码，其他正常使用
	 * 
	 * @return String
	 * @throws IOException
	 */
	public String getDecodePwd(String password) throws IOException {
		if ("keepLogin".equals(keepLogin) && existsCookie()) {// 是cookie方式登录分支
			return new String(new BASE64Decoder().decodeBuffer(password));
		} else {
			return password;
		}
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		if (loginName != null) {
			this.loginName = loginName.trim();
		}

	}

	public String getMailRuleId() {
		return mailRuleId;
	}

	public void setMailRuleId(String mailRuleId) {
		this.mailRuleId = mailRuleId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public IPersonService getPersonService() {
		return personService;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getKeepLogin() {
		return keepLogin;
	}

	public void setKeepLogin(String keepLogin) {
		this.keepLogin = keepLogin;
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getUserPwd() {
		return UserPwd;
	}

	public void setUserPwd(String userPwd) {
		UserPwd = userPwd;
	}

	public String getLogin() {
		return Login;
	}

	public void setLogin(String login) {
		Login = login;
	}

	public IJecnTaskSearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(IJecnTaskSearchService searchService) {
		this.searchService = searchService;
	}

	public String getMailPeopleId() {
		return mailPeopleId;
	}

	public void setMailPeopleId(String mailPeopleId) {
		this.mailPeopleId = mailPeopleId;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public long getMailTaskId() {
		return mailTaskId;
	}

	public void setMailTaskId(long mailTaskId) {
		this.mailTaskId = mailTaskId;
	}

	public String getMailFlowType() {
		return mailFlowType;
	}

	public void setMailFlowType(String mailFlowType) {
		this.mailFlowType = mailFlowType;
	}

	public String getMailFlowId() {
		return mailFlowId;
	}

	public void setMailFlowId(String mailFlowId) {
		this.mailFlowId = mailFlowId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMailActiveId() {
		return mailActiveId;
	}

	public void setMailActiveId(String mailActiveId) {
		this.mailActiveId = mailActiveId;
	}

	public IJecnUserLoginLogService getUserLoginLogService() {
		return userLoginLogService;
	}

	public void setUserLoginLogService(IJecnUserLoginLogService userLoginLogService) {
		this.userLoginLogService = userLoginLogService;
	}

	public String getAdLoginName() {
		return adLoginName;
	}

	public void setAdLoginName(String adLoginName) {
		this.adLoginName = adLoginName;
	}

	public String getOldPwd() {
		return oldPwd;
	}

	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public String getAdPassWord() {
		return adPassWord;
	}

	public void setAdPassWord(String adPassWord) {
		this.adPassWord = adPassWord;
	}

	public String getMailRuleType() {
		return mailRuleType;
	}

	public void setMailRuleType(String mailRuleType) {
		this.mailRuleType = mailRuleType;
	}

	/** 是否单点登录 true:是 false：否 */
	public void setIsSSo(boolean flag) {
		this.isSSO = flag;
	}

	public String getMailFileId() {
		return mailFileId;
	}

	public void setMailFileId(String mailFileId) {
		this.mailFileId = mailFileId;
	}

	public String getCustomRedirectURL() {
		return customRedirectURL;
	}

	public void setCustomRedirectURL(String customRedirectURL) {
		this.customRedirectURL = customRedirectURL;
	}

	public String getExternalResourceContentType() {
		return externalResourceContentType;
	}

	public void setExternalResourceContentType(String externalResourceContentType) {
		this.externalResourceContentType = externalResourceContentType;
	}

	private boolean isXss(String text) {
		if (StringUtils.isBlank(text)) {
			return false;
		}
		Pattern p = Pattern.compile("[<>'\"]");
		Matcher m = p.matcher(text);
		if (m.find()) {// 返回null不处理，（返回登录页面右会嵌入代码在login.jsp中，所以返回null不处理）
			return true;
		}
		return false;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getRtnlProposeId() {
		return rtnlProposeId;
	}

	public void setRtnlProposeId(String rtnlProposeId) {
		this.rtnlProposeId = rtnlProposeId;
	}

	public String getHandleFlag() {
		return handleFlag;
	}

	public void setHandleFlag(String handleFlag) {
		this.handleFlag = handleFlag;
	}

	/**
	 * 单点处理对象
	 * 
	 * @return
	 */
	public AbstractSinglePointBean getSinglePointBean() {
		return new SinglePointBean(this);
	}

	public String getIsPub() {
		return isPub;
	}

	public void setIsPub(String isPub) {
		this.isPub = isPub;
	}

	public String getInputUrl() {
		if (JecnConfigTool.isNotEnableNewApp()) {
			return "login.jsp";
		}
		return inputUrl;
	}

	public void setInputUrl(String inputUrl) {
		this.inputUrl = inputUrl;
	}

	public boolean getIsApp() {
		return isApp;
	}

	public void setIsApp(boolean isApp) {
		this.isApp = isApp;
	}

	public boolean getIsView() {
		return isView;
	}

	public void setIsView(boolean isView) {
		this.isView = isView;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
