function ruleSys() {
	var ruleRoot = new Ext.tree.AsyncTreeNode({
				text : i_ruleSys,
				expanded : true,
				id : "0"
			})

	var ruleLoader = new Ext.tree.TreeLoader({
				dataUrl : "getChildRules.action"
			})
	var ruleTabPanel = new Ext.TabPanel({
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				height : Ext.getCmp("main-panel").getInnerHeight(),
				items : [{
							title : i_search,// 搜索
							id : "ruleSearchTab_id",
							border:false,
							autoLoad : {
								url : basePath + "getRuleType.action",
								scripts : false,
								callback : function(el, success, response) {
									ruleSysGrid();
								}
							}

						}],
				listeners:{
					'bodyresize' : function(p, width, height) {
						if (p.getActiveTab() == null) {
							return;
						}
						if(p.getActiveTab().getId()=='rtnlPropose_id'){
							setAllStateProposeDivWH('flowAllProposeJspId',false);
						}
					},
					'tabchange':function(tabPanel,panel){
						if(panel.getId()=='ruleSearchTab_id'){
							if (typeof(ruleSysGridPanel) != "undefined"){
								ruleSysGridPanel.setSize(tabPanel.getWidth()-2,tabPanel.getHeight()-193);
							}
								
						}else if(panel.getId()=='tab_ruleList_id'){
							if (typeof(ruleListGridPanel) != "undefined"){
								ruleListGridPanel.setSize(tabPanel.getWidth()-2,tabPanel.getHeight()-29);
							}
								
						}
					}
					
				}

			});
	var ruleTree = new Ext.tree.TreePanel({
		root : ruleRoot,
		height : Ext.getCmp("main-panel").getInnerHeight() - 25,
		loader : ruleLoader,
		useArrows : true,
		border:false,
		autoScroll : true,
		rootVisible:false,//隐藏根节点
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			"dblclick" : function(node, e) {
				if (node.attributes.type == "ruleDir") {
					var length = ruleTabPanel.items.length;
					for (var i = length - 2; i >= 0; i--) {
						var p = ruleTabPanel.getComponent(i);
						ruleTabPanel.remove(p, true);
					}
					ruleTabPanel.insert(0, {
								title : i_ruleList,// 制度列表
								id : "tab_ruleList_id",
								autoLoad : {
									url : basePath + "jsp/rule/ruleList.jsp",
									callback : function() {
										ruleListByPid(node.id);
									}
								}
							});
					//if(isAdmin || isViewAdmin){//只有流程管理员和系统管理员登录才可以显示制度清单
						ruleTabPanel.insert(1, {
									title : i_ruleSysList,// 制度清单
									id : "ruleSysList",
									autoLoad : {
										url : basePath
												+ "jsp/rule/ruleSysList.jsp",
										callback : function() {
											 ruleSysListJson(node.id,false);
										}
									}
								});
					//}
					if (isShowDirList) {
						ruleTabPanel.remove(Ext.getCmp("tab_ruleList_id"));
					}
					ruleTabPanel.setActiveTab(0);
				}else{ 
					
					Ext.Ajax.request({
						url : basePath + "rule.action",
						params : {ruleId:node.id,dbClick:'dbClick'},
						success : function(response,r){
							if(response.responseText=='noPopedom'){
							alert(i_noPopedom);//您没有访问权限!
							return;
							}else{
							 if (node.attributes.type == "ruleFile") {// 制度文件类型
								var length = ruleTabPanel.items.length;
								for (var i = length - 2; i >= 0; i--) {
									var p = ruleTabPanel.getComponent(i);
									ruleTabPanel.remove(p, true);
								}
								ruleTabPanel.insert(0, {
											title : "基本信息",// 制度文件===概况信息
											id : "ruleSysContent_id",
											autoLoad :{ 
											    url:basePath
													+ "getRuleContent.action?ruleId="
													+ node.id,
												scripts : true,
												callback : function() {
													setRuleRelatedProcessWH('ruleRelatedProcessShowId');
												}
											 }
										});
								 ruleTabPanel.insert(1, {
											title : i_relateFile,// 相关文件
											id : "ruleFileInfo_id",
											autoLoad : {
											    url:basePath
													+ "getRuleRelateFile.action?ruleId="
													+ node.id,
												callback : function() {
													setDocumentTemplate('ruleRelateFileId');
												}
											}
										});
								 ruleTabPanel.insert(2, {
											title : i_rationalization,// 制度合理化建议
											id : "rtnlPropose_id",
											autoLoad : {
											    url:basePath
													+ "allRulertnlPropose.action?flowId="
													+ node.id+"&isFlowRtnl=1",
												callback : function() {
													onloadSingleRulePropose(node.id,false,node.text);
												}
											}
										});
								 if(isAdmin || isViewAdmin || isDocControlPermission){//只有流程管理员和系统管理员登录才可以显示制度文控信息
				                   ruleTabPanel.insert(3, {
												title : i_ruleHistoryInfo,// 制度文控信息
												id : "ruleHistoryInfo_id",
												autoLoad : {
												    url:basePath
														+ "getRuleHistoryList.action?isRuleFile=true&ruleId="
														+ node.id,
													callback : function() {
														setDocumentTemplate('ruleDocumentDiv');
													}
												}
											});
			                   }
								  
								  ruleTabPanel.setActiveTab(0);
							 } else if (node.attributes.type == "ruleModeFile") {// 制度模板文件
								var length = ruleTabPanel.items.length;
								for (var i = length - 2; i >= 0; i--) {
									var p = ruleTabPanel.getComponent(i);
									ruleTabPanel.remove(p, true);
								}
								ruleTabPanel.insert(0, {
											title : i_operateDec,// 操作说明
											id : "ruleDescription_id",
											autoLoad : {
												url : basePath
														+ "getRuleExplanation.action?ruleId="
														+ node.id,
												callback : function() {
													setRuleRelatedProcessWH('ruleFileDiv');
												}
											}
										});
								ruleTabPanel.insert(1, {
											title : "基本信息",// 概况信息
											id : "ruleProperty_id",
											autoLoad : basePath
													+ "getRuleProperty.action?ruleId="
													+ node.id
										});
								 ruleTabPanel.insert(2, {
											title : i_relateFile,// 相关文件
											id : "ruleFileInfo_id",
											autoLoad : {
											    url:basePath
													+ "getRuleRelateFile.action?ruleId="
													+ node.id,
												callback : function() {
													setDocumentTemplate('ruleRelateFileId');
												}
											}
										});
								  ruleTabPanel.insert(3, {
											title : i_rationalization,// 制度合理化建议
											border : false,
											id : "rtnlPropose_id",
											autoLoad : {
								   				scripts : true,
											    url:basePath
													+ "allRulertnlPropose.action?flowId="
													+ node.id+"&isFlowRtnl=1",
												callback : function() {
													onloadSingleRulePropose(node.id,false,node.text);
												}
											}
										});
							 if(isAdmin || isViewAdmin || isDocControlPermission){//只有流程管理员和系统管理员登录才可以显示制度文控信息
			                   ruleTabPanel.insert(4, {
											title : i_ruleHistoryInfo,// 制度文控信息
											id : "ruleHistoryInfo_id",
											autoLoad : {
											    url:basePath
													+ "getRuleHistoryList.action?ruleId="
													+ node.id,
												callback : function() {
													setDocumentTemplate('ruleDocumentDiv');
												}
												}
										});
			                   }
							  ruleTabPanel.setActiveTab(0);
							}
							}
						}
					
					})
					}
			}
		}

	})
	var rulePanel = new Ext.Panel({
				renderTo : "rule_id",
				border : false,
				id:'rulePanel_index_id',
				layout : "border",
				height : Ext.getCmp("main-panel").getInnerHeight(),
				items : [{
					region : "west",
					border : 0,
					title : "<div  style='text-align:center;font-weight:bold'>"
							+ i_ruleSys + "</div>",
					split : true,
					collapseMode : "mini",
					width : 300,
					items : [ruleTree]
				}, {
					region : "center",
					border : 0,
					// contentEl : "ruleCenter_id",
					id : "rule_center_panel",
					items : [ruleTabPanel],
					listeners : {
						'bodyresize' : function(p,width,height) {
							if(ruleTabPanel.getActiveTab()==null){
								return;
							}
							ruleTree.setHeight(height);
							ruleTabPanel.setSize(width,height);
							if(ruleTabPanel.getActiveTab().getId()=='ruleSearchTab_id'){
								ruleSysGridPanel.setSize(width-2,height-193);
							}else if(ruleTabPanel.getActiveTab().getId()=='tab_ruleList_id'){
								ruleListGridPanel.setSize(width-2,height-29);
							}
//							if (typeof(ruleSysGridPanel) != "undefined") {
//								ruleSysGridPanel.setSize(width-2,height-233);
//							}
//							if (typeof(ruleListGridPanel) != "undefined") {
//								ruleListGridPanel.setSize(width-2,height-25);
//							}
							
						}
					}
				}]
			});

}

function ruleSysGrid() {

	ruleSysGridStore = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : "getRuleList.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "ruleId",
									mapping : "ruleId"
								}, {
									name : "ruleName",
									mapping : "ruleName"
								}, {
									name : "ruleNum",
									mapping : "ruleNum"
								}, {
									name : "ruleTypeName",
									mapping : "ruleTypeName"
								}, {
									name : "dutyOrg",
									mapping : "dutyOrg"
								}, {
									name : "secretLevel",
									mapping : "secretLevel"
								}, {
									name : "isDir",
									mapping : "isDir"
								}, {
									name : "fileId",
									mapping : "fileId"
								}, {
									name : "pubDate",
									mapping : "pubDate"
								} ])

			});

	ruleSysGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : "ruleId",
				dataIndex : "ruleId",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_ruleName,
				dataIndex : "ruleName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : ruleNameRenderer
			}, {
				header : i_ruleNum,
				dataIndex : "ruleNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_ruleType,
				dataIndex : "ruleTypeName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "dutyOrg",
				align : "center",
				menuDisabled : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			}

	]);
	ruleSysGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : ruleSysGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	ruleSysGridPanel = new Ext.grid.GridPanel({
				id : "ruleSysGridPanel_id",
				renderTo : 'ruleSysGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 193,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				bodyStyle:'border-left: 0px;',
				store : ruleSysGridStore,
				colModel : ruleSysGridColumnModel,
				bbar : ruleSysGridBbar
			})

}
/**
 * 制度查询
 */
function ruleSearch() {
	// 制度名称
	var ruleName = document.getElementById("ruleName_id").value;
	// 制度编号
	var ruleNum = document.getElementById("ruleNum_id").value;
	// 制度类别
	var ruleType = document.getElementById("ruleType_id").value;
	// 责任部门名称
	var orgDutyName = document.getElementById("orgDutyName_rule_id").value;
	// 责任部门ID
	var orgDutyId = document.getElementById("orgDutyId_rule_id").value;
	// 查阅部门名称
	var orgLookName = document.getElementById("orgLookName_rule_id").value;
	// 查阅部门ID
	var orgLookId = document.getElementById("orgLookId_rule_id").value;
	// 查阅岗位名称
	var posLookName = document.getElementById("posLookName_rule_id").value;
	// 查阅岗位ID
	var posLookId = document.getElementById("posLookId_rule_id").value.split("_")[0];
	// 密级
	var secretLevel = document.getElementById("secretLevel_id").value;
	ruleSysGridStore.setBaseParam("ruleName", ruleName);
	ruleSysGridStore.setBaseParam("ruleNum", ruleNum);
	if (ruleType && ruleType != "") {
		ruleSysGridStore.setBaseParam("ruleType", ruleType);
	}

	ruleSysGridStore.setBaseParam("orgName", orgDutyName);
	if (orgDutyId && orgDutyId != "") {
		ruleSysGridStore.setBaseParam("orgId", orgDutyId);
	}
	ruleSysGridStore.setBaseParam("orgLookName", orgLookName);
	if (orgLookId && orgLookId != "") {
		ruleSysGridStore.setBaseParam("orgLookId", orgLookId);
	}
	ruleSysGridStore.setBaseParam("posLookName", posLookName);
	if (posLookId && posLookId != "") {
		ruleSysGridStore.setBaseParam("posLookId", posLookId);
	}
	if (secretLevel && secretLevel != "") {
		ruleSysGridStore.setBaseParam("secretLevel", secretLevel);
	}

	ruleSysGridStore.load({
				params : {
					start : 0,
					limit : pageSize
				}
			});
}
/**
 * 制度名称增加链接
 * 
 * @param {}
 *            value
 * @param {}
 *            cellmeta
 * @param {}
 *            record
 * @return {}
 */
function ruleNameRenderer(value, cellmeta, record) {
	// 制度类型
	var isDir = record.data["isDir"];
	if (isDir == 1) {
		return "<div  ext:qtip='" + value
				+ "'><a target='_blank' class='a-operation' href='" + basePath
				+ "rule.action?type=ruleModeFile&ruleId="
				+ record.data['ruleId'] +"&ruleName="+record.data['ruleName']+ "'>" + value + "</a>";
	} else if (isDir == 2) {
		return "<div  ext:qtip='" + value
				+ "'><a target='_blank' class='a-operation' href='" + basePath
				+ "rule.action?type=ruleFile&ruleId="+record.data['ruleId']+"&fileId=" + record.data['fileId'] +"&ruleName="+record.data['ruleName']
				+ "'>" + value + "</a>";
	}
}

function ruleDetailRenderer(value, cellmeta, record){
	// 制度类型
	var isDir = record.data["isDir"];
	if (isDir == 1) {
		return "<div><a target='_blank' class='a-operation' href='" + basePath
				+ "rule.action?type=ruleModeFile&ruleId="
				+ record.data['ruleId'] + "'>" + i_detail + "</a>";
	} else if (isDir == 2) {
		return "<div><a target='_blank' class='a-operation' href='" + basePath
				+ "rule.action?type=ruleFile&ruleId=" + record.data['ruleId']
				+ "'>" + i_detail + "</a>";
	}
}
function ruelSearchReset() {
	// 制度名称
	document.getElementById("ruleName_id").value = "";
	// 制席编号
	document.getElementById("ruleNum_id").value = "";
	// 制度类别
	document.getElementById("ruleType_id").options[0].selected = true;
	// 责任部门名称
	document.getElementById("orgDutyName_rule_id").value = "";
	// 责任部门ID
	document.getElementById("orgDutyId_rule_id").value = "-1"
	// 查阅部门名称
	document.getElementById("orgLookName_rule_id").value = "";
	// 查阅部门ID
	document.getElementById("orgLookId_rule_id").value = "-1";
	// 查阅岗位名称
	document.getElementById("posLookName_rule_id").value = "";
	// 查阅岗位ID
	document.getElementById("posLookId_rule_id").value == "-1";
	// 密级
	document.getElementById("secretLevel_id").options[0].selected = true;

}

function ruleListByPid(id) {

	ruleListGridStore = new Ext.data.Store({
				baseParams: {ruleId : id},  
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "ruleListByPid.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "ruleId",
									mapping : "ruleId"
								}, {
									name : "ruleName",
									mapping : "ruleName"
								}, {
									name : "ruleNum",
									mapping : "ruleNum"
								}, {
									name : "ruleTypeName",
									mapping : "ruleTypeName"
								}, {
									name : "dutyOrg",
									mapping : "dutyOrg"
								}, {
									name : "secretLevel",
									mapping : "secretLevel"
								}, {
									name : "isDir",
									mapping : "isDir"
								}, {
									name : "fileId",
									mapping : "fileId"
								}])

			});

	ruleListGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : "ruleId",
				dataIndex : "ruleId",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_ruleName,
				dataIndex : "ruleName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : ruleNameRenderer
			}, {
				header : i_ruleNum,
				dataIndex : "ruleNum",
				align : "center",
				menuDisabled : true
			}, {
				header : i_ruleType,
				dataIndex : "ruleTypeName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "dutyOrg",
				align : "center",
				menuDisabled : true
			}, {
				header : i_secretLevel,
				dataIndex : "secretLevel",
				align : "center",
				menuDisabled : true,
				renderer : secretLevelRenderer
			}

	]);
	ruleListGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : ruleListGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	ruleListGridPanel = new Ext.grid.GridPanel({
				id : "ruleListGridPanel_id",
				renderTo : 'ruleListGrid_id',
				height : Ext.getCmp("main-panel").getInnerHeight()-29,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				border:false,
				store : ruleListGridStore,
				colModel : ruleListGridColumnModel,
				bbar : ruleListGridBbar
			})
}
