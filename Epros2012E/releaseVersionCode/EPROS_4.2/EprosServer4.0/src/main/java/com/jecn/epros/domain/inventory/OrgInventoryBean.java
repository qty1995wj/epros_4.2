package com.jecn.epros.domain.inventory;

import java.util.List;

public class OrgInventoryBean {
    private Long orgId;
    private String orgName;
    private Long perOreId;
    private int tLevel;
    private List<OrgInventoryRelatedBean> listOrgInventoryRelatedBean;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getPerOreId() {
        return perOreId;
    }

    public void setPerOreId(Long perOreId) {
        this.perOreId = perOreId;
    }

    public int gettLevel() {
        return tLevel;
    }

    public void settLevel(int tLevel) {
        this.tLevel = tLevel;
    }


    public List<OrgInventoryRelatedBean> getListOrgInventoryRelatedBean() {
        return listOrgInventoryRelatedBean;
    }

    public void setListOrgInventoryRelatedBean(List<OrgInventoryRelatedBean> listOrgInventoryRelatedBean) {
        this.listOrgInventoryRelatedBean = listOrgInventoryRelatedBean;
    }
}
