package epros.designer.gui.process.guide.guideTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.guide.KPIShowDialog;
import epros.designer.gui.process.kpi.TmpKpiShowValues;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 流程设计向导流程KPI表格
 * 
 * @author 2012-07-09
 * 
 */
public class ProcessKPIPropertyTable extends JTable {
	private static Logger log = Logger.getLogger(ProcessKPIPropertyTable.class);
	private Long flowId = null;
	private KPIShowDialog kpiDialog;

	private TmpKpiShowValues kpiShowValues;

	public KPIShowDialog getKpiDialog() {
		return kpiDialog;
	}

	public void setKpiDialog(KPIShowDialog kpiDialog) {
		this.kpiDialog = kpiDialog;
	}

	public ProcessKPIPropertyTable(Long flowId, KPIShowDialog kpiDialog, TmpKpiShowValues kpiShowValues) {
		this.flowId = flowId;
		this.kpiDialog = kpiDialog;
		this.kpiShowValues = kpiShowValues;
		this.setModel(getTableModel());
		initialization();
	}

	public void initialization() {
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		List<Integer> cols = gethiddenCols();
		if (cols != null && !cols.isEmpty() && this.getColumnCount() > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		if (kpiShowValues == null) {
			return vs;
		}
		// 获取流程KPI
		List<JecnFlowKpiNameT> list = kpiShowValues.getKpiNameList();
		// 获取流程KPI数据
		List<List<String>> valueLists = kpiShowValues.getKpiRowValues();
		int valueSize = valueLists.size();
		for (int i = 0; i < valueSize; i++) {
			vs.add(getKpiRow(valueLists.get(i), list.get(i)));
		}
		return vs;
	}

	public Vector<Vector<String>> getKpiContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 获取流程KPI
		List<JecnFlowKpiNameT> list = kpiShowValues.getKpiNameList();

		List<List<String>> valueLists = kpiShowValues.getKpiRowValues();
		int valueSize = valueLists.size();
		for (int i = 0; i < valueSize; i++) {
			vs.add(getKpiRow(valueLists.get(i), list.get(i)));
		}
		return vs;
	}

	public Vector<String> getKpiRow(List<String> rowValues, JecnFlowKpiNameT flowKpiName) {
		Vector<String> row = new Vector<String>();
		row.add(String.valueOf(flowKpiName.getKpiAndId()));
		for (String rowValue : rowValues) {
			row.add(rowValue);
		}
		// 创建时间
		row.add(JecnDesignerCommon.getStringbyDate(flowKpiName.getCreatTime()));
		return row;
	}

	public Vector<String> getKpiRow(JecnFlowKpiName flowKpiNameT) {
		Vector<String> row = new Vector<String>();
		row.add(String.valueOf(flowKpiNameT.getKpiAndId()));

		// 创建时间
		row.add(JecnDesignerCommon.getStringbyDate(flowKpiNameT.getCreatTime()));
		return row;
	}

	public ProcessKPIPropertyTableMode getKpiTableModel() {
		return new ProcessKPIPropertyTableMode(getKpiContent(), getTitle());
	}

	public ProcessKPIPropertyTableMode getTableModel() {
		return new ProcessKPIPropertyTableMode(getContent(), getTitle());
	}

	public Vector<String> getTitle() {
		Vector<String> title = new Vector<String>();
		if (kpiShowValues == null || kpiShowValues.getKpiTitles().size() == 0) {
			return title;
		}
		title.add("id");
		for (JecnConfigItemBean itemBean : kpiShowValues.getKpiTitles()) {
			if (JecnResourceUtil.getLocale() == Locale.ENGLISH) {// English
				title.add(itemBean.getEnName());
			} else {
				title.add(itemBean.getName());
			}
		}
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("createTime"));// 创建时间
		return title;
	}

	public boolean isSelectMutil() {
		return false;
	}

	public List<Integer> gethiddenCols() {
		List<Integer> hiddenList = new ArrayList<Integer>(Arrays.asList(new Integer[] { 0 }));
		return hiddenList;
	}

	class ProcessKPIPropertyTableMode extends DefaultTableModel {
		public ProcessKPIPropertyTableMode(Vector<Vector<String>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}
}
