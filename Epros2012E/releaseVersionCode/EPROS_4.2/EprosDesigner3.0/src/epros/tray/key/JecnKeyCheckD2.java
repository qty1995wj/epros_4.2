package epros.tray.key;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 密钥验证
 * 
 * @author ZHOUXY
 * 
 */
public class JecnKeyCheckD2 {
	private static final Log log = LogFactory.getLog(Math.class);
	/** true：打印log；false：不打印log */
	private static boolean isLog=false;


	private JecnKeyProcesD2 keyProces = null;

	public JecnKeyCheckD2() {
		init();
	}

	private void init() {
		keyProces = new JecnKeyProcesD2();
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 验证密钥
	 * 
	 */
	public void process() {
		checkKey();
	}
	
	/**
	 * 
	 * 打印log
	 * 
	 * @param info
	 */
	static void printLog(String info){
		if(isLog){
			log.info("::="+info);
		}
	}
	/**
	 * 
	 * 打印log
	 * 
	 * @param info
	 * @param e
	 */
	static void printLog(String info,Exception e){
		if(isLog){
			log.error("::="+info,e);
		}
	}

	/**
	 * 
	 * 密钥验证，返回验证信息
	 * 
	 * @return String 验证信息或NULL
	 */
	String checkKeyString() {
		return keyProces.checkKey();
	}

	/**
	 * 
	 * 执行验证
	 * 
	 */
	private void checkKey() {
		// 密钥验证
		try {
			if (!checkKeyProcess()) {
				JecnKeyCheckD2.printLog("JecnKeyCheck checkKey is error");
				System.exit(0);
			}
		} catch (Exception ex) {// 此try必须要，防止密钥验证失败还能进入系统
			// 正常情况不走此分支
			JecnKeyCheckD2.printLog("JecnKeyCheck is error", ex);
			System.exit(0);
		}
	}

	/**
	 * 
	 * 验证密钥是否通过：true通过；false不通过
	 * 
	 * @return boolean true通过；false不通过
	 */
	private boolean checkKeyProcess() {
		// 密钥验证：NULL成功
		String info = keyProces.checkKey();

		if (DrawCommon.isNullOrEmtryTrim(info)) {
			// 提示用户信息
			String tipInfo = keyProces.checkKeyTipUser();
			if (!DrawCommon.isNullOrEmtryTrim(tipInfo)) {
				JecnOptionPane.showMessageDialog(JecnDrawMainPanel
						.getMainPanel(), tipInfo, null,
						JOptionPane.INFORMATION_MESSAGE);
			}
			return true;
		} else {
			JecnKeyDialogD2 keyDialog = new JecnKeyDialogD2();
			keyDialog.setInfoLabelText(info);
			keyDialog.setVisible(true);

			if (keyDialog.isOk()) {// 标识重新上传密钥后验证成功
				return true;
			}
			return false;
		}
	}
}
