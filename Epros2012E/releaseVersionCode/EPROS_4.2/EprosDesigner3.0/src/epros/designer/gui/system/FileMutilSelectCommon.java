package epros.designer.gui.system;

import java.awt.Insets;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

public class FileMutilSelectCommon extends MutilSelectCommon {
	//是否可以多选
	private boolean inMutil = true;
	
	public FileMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest) {
		super(rows, infoPanel, insets, list, JecnProperties.getValue("StandardDocument"), jecnDialog, isRequest);
	}
	
	public FileMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest,boolean isDownload) {
		super(rows, infoPanel, insets, list, JecnProperties.getValue("StandardDocument"), jecnDialog, isRequest,isDownload);
	}


	public FileMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest, String lableName,boolean isMutil) {
		super(rows, infoPanel, insets, list, lableName, jecnDialog, isRequest,false);
		this.inMutil= isMutil;
	}

	@Override
	protected JecnSelectChooseDialog getJecnSelectChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		FileChooseDialog fileChooseDialog = new FileChooseDialog(list, jecnDialog);
		fileChooseDialog.setSelectMutil(inMutil);
		return fileChooseDialog;
	}

}
