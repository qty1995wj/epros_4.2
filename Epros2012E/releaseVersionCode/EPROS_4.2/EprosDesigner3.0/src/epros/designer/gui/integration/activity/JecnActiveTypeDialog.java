package epros.designer.gui.integration.activity;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;

import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 活动类别
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-10-30 时间：下午02:21:10
 */
public class JecnActiveTypeDialog extends JecnDialog {
	private  Logger log = Logger.getLogger(JecnActiveTypeDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = null;
	/** 信息面板 */
	private JecnActiveInfoJPanel infoPanel;

	/** 关闭按钮面板 */
	private JecnPanel buttonPanel;
	/** 关闭 */
	private JButton closeBut = new JButton(JecnProperties.getValue("closeBtn"));

	public JecnActiveTypeDialog() {
		this.setSize(450, 300);
		// 类别
		this.setTitle(JecnProperties.getValue("typeDif"));
		this.setResizable(false);
		this.setModal(true);
		// 居中
		this.setLocationRelativeTo(null);

		init();

		initLayout();
	}

	private void init() {
		// 主面板
		mainPanel = new JecnPanel(450, 300);
		// 信息面板
		infoPanel = new JecnActiveInfoJPanel(430, 225);
		// 背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 关闭按钮面板
		buttonPanel = new JecnPanel(440, 30);
		// 关闭
		closeBut = new JButton(JecnProperties.getValue("closeBtn"));

		closeBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		mainPanel.add(infoPanel, c);

		// 按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(closeBut);

		this.getContentPane().add(mainPanel);
	}

	public List<JecnActiveTypeBean> getListActiveTypeBean() {
		return infoPanel.getListActiveTypeBean();
	}

	/**
	 * 关闭
	 */
	private void cancelButPerformed() {
		this.dispose();
	}
}
