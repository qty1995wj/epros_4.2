package com.jecn.epros.server.action.web.login.ad.lianhedianzi;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class LianHeDianZiLoginAction extends JecnAbstractADLoginAction {

	public LianHeDianZiLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {
			// 过滤掉admin
			if ("admin".equals(loginAction.getLoginName())) {
				return loginAction.userLoginGen(true);
			}
			this.loginAction.setIsSSo(true);

			// 获取用户输入的Ldap账号
			String ldapAccount = loginAction.getLoginName();
			// 获取用户输入的密码
			String ldapPassword = loginAction.getDecodePwd(loginAction.getPassword());

			// 拼接域名
			if (!ldapAccount.startsWith(LianHeDianZiAfterItem.DOMAIN)) {
				ldapAccount = LianHeDianZiAfterItem.DOMAIN + ldapAccount;
			}
			// 进行AD域验证
			int result = LdapAuthVerifier.newInstance(ldapAccount, ldapPassword, LianHeDianZiAfterItem.LDAP_URL)
					.verifyAuth();

			log.info("权限验证结果:" + result);
			// 验证成功
			if (result == LdapAuthVerifier.SUCCESS) {
				// 不进行密码验证 登录
				return loginAction.userLoginGen(false);
			} else if (result == LdapAuthVerifier.FAILED) {
				// 用户名或密码不正确！
				this.loginAction.addFieldError("loginMessage", loginAction.getText("nameOrPasswordError"));
			} else if (result == LdapAuthVerifier.CONNECTION_ERROR) {
				// 无法验证用户，请联系系统管理员!
				this.loginAction.addFieldError("loginMessage", loginAction.getText("anJianZhiYuanADConnectFailed"));
			}
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
		}
		return LoginAction.INPUT;
	}

}
