package com.jecn.epros.server.action.web.login.ad.jinbo;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 金博单点登录
 * 
 */
public class JinBoLoginAction extends JecnAbstractADLoginAction {

	public JinBoLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	public String login() {
		// http://xyz.21tb.com/login/sso.init.do?userName=0000022596&timestamp=1342580600244&sysCode=xyz&sign=cbb82111618b3a8a839f1258f5862dd3
		String userName = loginAction.getRequest().getParameter("userName");
		String timestamp = loginAction.getRequest().getParameter("timestamp");
		String sysCode = loginAction.getRequest().getParameter("sysCode");
		String sign = loginAction.getRequest().getParameter("sign");
		if (!validate(userName, timestamp, sysCode, sign)) {
			loginAction.setCustomRedirectURL(JinBoAfterItem.SSOLoginUrl);
			log.error("校验数据未通过，跳转登录页面");
			return LoginAction.REDIRECT;
		}
		return loginByLoginName(userName);
	}

	private boolean validate(String userName, String timestamp, String sysCode, String sign) {
		log.info("请求登录数据为：" + " userName=" + userName + " timestamp=" + timestamp + " sysCode=" + sysCode + " sign="
				+ sign);
		if (StringUtils.isBlank(userName) || StringUtils.isBlank(timestamp) || StringUtils.isBlank(sysCode)
				|| StringUtils.isBlank(sign)) {
			log.error("某项数据为空：" + " userName=" + userName + " timestamp=" + timestamp + " sysCode=" + sysCode
					+ " sign=" + sign);
			return false;
		}
		long currentTimeMillis = System.currentTimeMillis();
		long otherTimeMillis = Long.valueOf(timestamp);
		if (Math.abs(currentTimeMillis - otherTimeMillis) > 24 * 60 * 60 * 1000) {
			log.error("时间差值超过了24h");
			return false;
		}
		if (!"jingbo".equals(sysCode)) {
			log.error("sysCode错误：" + sysCode);
			return false;
		}
		if (!sign.equals(Signer.calculateSign("sso", Signer.md5("jingbo"), sysCode, userName, otherTimeMillis))) {
			log.error("sign与生成的不相等");
			return false;
		}
		log.info("登录数据校验通过");
		return true;
	}

	public static void main(String[] args) {
		System.out.println(System.currentTimeMillis());
	}
}
