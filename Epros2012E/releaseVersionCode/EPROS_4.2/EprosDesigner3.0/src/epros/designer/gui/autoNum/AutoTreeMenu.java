package epros.designer.gui.autoNum;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class AutoTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(AutoTreeMenu.class);
	private JecnTree jecnTree = null;
	/** 创建代码 */
	private JMenuItem createCode;
	/** 重命名 */
	private JMenuItem reName;

	/** 刷新 */
	private JMenuItem refresh;

	/** 删除 */
	private JMenuItem deleteMenu;

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;

	private int treeType;

	public AutoTreeMenu(JecnTree jecnTree, int treeType) {
		this.jecnTree = jecnTree;
		this.treeType = treeType;
		initCompotents();
	}

	private void initCompotents() {

		// 创建代码
		createCode = new JMenuItem(JecnProperties.getValue("createCode"));
		// 重命名
		reName = new JMenuItem(JecnProperties.getValue("rename"));
		// 刷新
		refresh = new JMenuItem(JecnProperties.getValue("refresh"));
		// 删除
		deleteMenu = new JMenuItem(JecnProperties.getValue("delete"));

		this.add(createCode);
		this.add(reName);
		this.add(refresh);
		this.add(deleteMenu);

		// 创建代码
		createCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createCodePerformed();
			}
		});
		// 刷新
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refurbishPerformed();
			}
		});

		// 重命名
		reName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reNamePerformed();
			}
		});

		// 删除
		deleteMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteMenuPerformed();
			}
		});
	}

	protected void deleteMenuPerformed() {
		// 是否删除提示框
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		if (listNode.size() == 0) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode node : listNode) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		// 删除节点
		try {
			ConnectionPool.getAutoCodeAction().deleteProcessCode(listIds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jecnTree, listNode);
	}

	protected void reNamePerformed() {
		ProcessCodeEditNameDialog editNameDialog = new ProcessCodeEditNameDialog(selectNode, jecnTree);
		editNameDialog.setVisible(true);
	}

	/**
	 * 刷新节点
	 */
	protected void refurbishPerformed() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getAutoCodeAction().getChildProcessCodes(id, treeType);
			JecnTreeCommon.refreshNode(selectNode, list, jecnTree);
		} catch (Exception ex) {
			log.error("refurbishPerformed is error", ex);
		}
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		if (JecnTreeCommon.isAuthNodes(listNode)) {
			// 删除
			deleteMenu.setVisible(true);
		}
	}

	public void setListNode(List<JecnTreeNode> listNode) {
		this.listNode = listNode;
		if (listNode.size() == 1) {
			this.selectNode = listNode.get(0);
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jecnTree, selectNode);

		if (selectNode != null && selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processCodeRoot) {
			reName.setEnabled(false);
			refresh.setEnabled(false);
			deleteMenu.setEnabled(false);
		}
	}

	protected void createCodePerformed() {
		if (selectNode == null) {
			return;
		}
		AddProcessCodeDialog addProcessCode = new AddProcessCodeDialog(selectNode, jecnTree, treeType);
		addProcessCode.setVisible(true);
	}
}
