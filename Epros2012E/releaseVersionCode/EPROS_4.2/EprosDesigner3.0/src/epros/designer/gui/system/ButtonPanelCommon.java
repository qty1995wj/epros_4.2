package epros.designer.gui.system;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;

import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

public class ButtonPanelCommon {
	/** 提示Lab */
	private JLabel verfyLab = new JLabel();
	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	public ButtonPanelCommon(JecnPanel mainPanel, Insets insets) {
		verfyLab.setForeground(Color.red);
		// 按钮面板
		GridBagConstraints c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		mainPanel.add(buttonPanel, c);
	}

	public JLabel getVerfyLab() {
		return verfyLab;
	}

	public JButton getOkBut() {
		return okBut;
	}

	public JButton getCancelBut() {
		return cancelBut;
	}
}
