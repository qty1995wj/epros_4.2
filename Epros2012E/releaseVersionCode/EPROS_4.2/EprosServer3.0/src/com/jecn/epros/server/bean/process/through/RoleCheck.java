package com.jecn.epros.server.bean.process.through;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.bean.checkout.CheckoutRoleItem;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.service.process.buss.FullPathManager;
import com.jecn.epros.server.util.JecnUtil;

public class RoleCheck {

	private Long flowId;
	private IBaseDao baseDao;
	private FullPathManager flowManager;
	private FullPathManager orgManager;
	private FullPathManager groupManager;
	private List<Object[]> data = new ArrayList<Object[]>();

	public RoleCheck(IBaseDao baseDao, Long flowId) {
		this.flowId = flowId;
		this.baseDao = baseDao;
		orgManager = new FullPathManager(baseDao, 5);
		groupManager = new FullPathManager(baseDao, 7);
		flowManager = new FullPathManager(baseDao, 0);
	}

	public CheckoutRoleResult check() {
		String roleType = JecnCommonSql.getRoleString();
		String sql = "select distinct a.flow_id,"
				+ "       a.figure_id,"
				+ "       a.figure_text,"
				+ "       a.pos_id,"
				+ "       a.pos_name,"
				+ "       a.org_id,"
				+ "       a.type,"
				+ "       a.exist,"
				+ "       a.x_point,a.y_point"
				+ "  from ("
				+
				// 精确查询有关联关系的
				"select jfsi.flow_id,  " + "               jfsi.figure_id,   " + "               jfsi.figure_text,"
				+ "               jfoi.figure_id      as pos_id," + "               jfoi.figure_text    as pos_name,"
				+ "               jfoi.org_id         as org_id," + "               0                   as type,"
				+ "               jfoi.figure_id as exist,jfsi.x_point,jfsi.y_point"
				+ "          from jecn_flow_structure_image_t jfsi"
				+ "          left join PROCESS_STATION_RELATED_T psr"
				+ "            on psr.figure_flow_id = jfsi.figure_id and psr.type = 0"
				+ "          left join jecn_flow_org_image jfoi on jfoi.figure_id = psr.figure_position_id"
				+ "           where jfsi.figure_type in "
				+ roleType
				+ " and jfsi.flow_id="
				+ flowId
				+ "        union"
				+ "        select jfsi.flow_id,"
				+ "               jfsi.figure_id,"
				+ "               jfsi.figure_text,"
				+ "               jfoi.id             as pos_id,"
				+ "               jfoi.name           as pos_name,"
				+ "               null                as org_id,"
				+ "               1                   as type,"
				+ "               jfoi.id             as exist,"
				+ "                jfsi.x_point,jfsi.y_point"
				+ "          from jecn_flow_structure_image_t jfsi"
				+ "         left join PROCESS_STATION_RELATED_T psr"
				+ "            on psr.figure_flow_id = jfsi.figure_id"
				+ "           and psr.type = 1"
				+ "         left join jecn_position_group jfoi on psr.figure_position_id = jfoi.id"
				+ "           where jfsi.figure_type in "
				+ roleType
				+ " and jfsi.flow_id="
				+ flowId

				+ " union "

				// 模糊查询文本相同的,并且不在上面的结果集中
				+ " select jfsi.flow_id,"
				+ "               jfsi.figure_id,"
				+ "               jfsi.figure_text,"
				+ "               jfoi.figure_id      as pos_id,"
				+ "               jfoi.figure_text    as pos_name,"
				+ "               jfoi.org_id         as org_id,"
				+ "               0                   as type,"
				+ "               null      as exist,"
				+ "               jfsi.x_point,jfsi.y_point"
				+ "          from jecn_flow_structure_image_t jfsi"
				+ "          inner join jecn_flow_org_image jfoi"
				+ "            on replace(jfsi.figure_text,'\n','') = jfoi.figure_text"
				+ "           where jfsi.figure_type in "
				+ roleType
				+ " and jfsi.flow_id="
				+ flowId
				+ " and not exists ("
				+ " select 1 from PROCESS_STATION_RELATED_T psr"
				+ "           where psr.figure_flow_id = jfsi.figure_id"
				+ "           and psr.figure_position_id = jfoi.figure_id"
				+ "           and psr.type = 0"
				+ " )"
				+ "        union"
				+ "        select jfsi.flow_id,"
				+ "               jfsi.figure_id,"
				+ "               jfsi.figure_text,"
				+ "               jfoi.id             as pos_id,"
				+ "               jfoi.name           as pos_name,"
				+ "               null                as org_id,"
				+ "               1                   as type,"
				+ "               null as exist,"
				+ "               jfsi.x_point,jfsi.y_point"
				+ "          from jecn_flow_structure_image_t jfsi"
				+ "         inner join jecn_position_group jfoi"
				+ "            on replace(jfsi.figure_text,'\n','') = jfoi.name and jfoi.is_dir=0 "
				+ "           where jfsi.figure_type in "
				+ roleType
				+ " and jfsi.flow_id="
				+ flowId
				+ " and not exists("
				+ "         select 1 from PROCESS_STATION_RELATED_T psr"
				+ "            where psr.figure_flow_id = jfsi.figure_id"
				+ "           and psr.figure_position_id = jfoi.id"
				+ "           and psr.type = 1"
				+ " )"
				+ ") a"
				+ " order by a.y_point asc,a.type";
		List<Object[]> data = baseDao.listNativeSql(sql);

		Set<Long> existsFigurePosAndGroup = new HashSet<Long>();
		Set<Long> delFigurePosAndGroup = new HashSet<Long>();
		for (Object[] obj : data) {
			if (obj[2] == null) {
				continue;
			}
			String figureText = obj[2].toString();
			if (StringUtils.isBlank(figureText)) {
				continue;
			}
			int type = Integer.valueOf(obj[6].toString());
			Long figureId = Long.valueOf(obj[1].toString());
			if (existsFigurePosAndGroup.contains(figureId) && StringUtils.isBlank(JecnUtil.nullToEmpty(obj[3]))) {// 去除多余的关联的岗位岗位组为空的数据
				continue;
			}
			if (existsFigurePosAndGroup.contains(figureId)) {
				delFigurePosAndGroup.add(figureId);
			}
			existsFigurePosAndGroup.add(figureId);
			if (type == 0) {
				if (obj[5] != null) {
					Long orgId = Long.valueOf(obj[5].toString());
					orgManager.add(orgId);
				}
			} else if (type == 1) {
				if (obj[3] != null) {
					Long groupId = Long.valueOf(obj[3].toString());
					groupManager.add(groupId);
				}
			}
			this.data.add(obj);
		}

		flowManager.add(flowId);
		flowManager.generate();
		orgManager.generate();
		groupManager.generate();

		CheckoutRoleResult result = new CheckoutRoleResult();
		result.setName(flowManager.getFullPath(this.flowId));
		List<CheckoutRoleItem> ds = new ArrayList<CheckoutRoleItem>();
		result.setData(ds);
		CheckoutRoleItem item = null;

		Map<Long, List<CheckoutRoleItem>> roleItems = new HashMap<Long, List<CheckoutRoleItem>>();
		// 拼接全路径 删除有数据的空行
		for (Object[] obj : this.data) {
			int type = Integer.valueOf(obj[6].toString());
			Long figureId = Long.valueOf(obj[1].toString());
			if (delFigurePosAndGroup.contains(figureId) && StringUtils.isBlank(JecnUtil.nullToEmpty(obj[3]))) {// 去除多余的关联的岗位岗位组为空的数据
				continue;
			}

			item = new CheckoutRoleItem();
			String a = JecnUtil.nullToEmpty(obj[2]);
			String b = JecnUtil.nullToEmpty(obj[4]);
			if (StringUtils.isBlank(a) || StringUtils.isBlank(b)) {
				item.setFpEqual(false);
			} else {
				item.setFpEqual(a.replace("\n", "").equals(b.replace("\n", "")));
			}
			if (obj[7] == null) {
				item.setRelated(false);
			} else {
				item.setRelated(true);
			}

			if (type == 0) {
				if (obj[5] != null && obj[4] != null) {
					Long orgId = Long.valueOf(obj[5].toString());
					String fullPath = orgManager.getFullPath(orgId);
					if (StringUtils.isNotBlank(fullPath)) {
						obj[4] = fullPath + "/" + obj[4];
					}
				}
			} else if (type == 1) {
				if (obj[3] != null && obj[4] != null) {
					Long groupId = Long.valueOf(obj[3].toString());
					String fullPath = groupManager.getFullPath(groupId);
					obj[4] = fullPath;
				}
			}
			item.setType(type);
			item.setFigureId(Long.valueOf(obj[1].toString()));
			item.setFigureText(JecnUtil.nullToEmpty(obj[2]));
			if (obj[3] != null) {
				item.setPosId(Long.valueOf(obj[3].toString()));
			}
			item.setPosName(JecnUtil.nullToEmpty(obj[4]));

			// 分组
			if (!roleItems.containsKey(item.getFigureId())) {
				roleItems.put(item.getFigureId(), new ArrayList<CheckoutRoleItem>());
			}
			roleItems.get(item.getFigureId()).add(item);

			ds.add(item);
		}

		// 去重复
		reduceSameRoleFigures(roleItems, ds);
		return result;
	}

	private void reduceSameRoleFigures(Map<Long, List<CheckoutRoleItem>> roleItems, List<CheckoutRoleItem> ds) {
		for (Entry<Long, List<CheckoutRoleItem>> entry : roleItems.entrySet()) {
			boolean isRelated = false;
			List<CheckoutRoleItem> deRelatedItems = new ArrayList<CheckoutRoleItem>();
			for (CheckoutRoleItem checkoutRoleItem : entry.getValue()) {
				if (checkoutRoleItem.isFpEqual() && checkoutRoleItem.isRelated()) {// 关联
					deRelatedItems.add(checkoutRoleItem);
					isRelated = true;
				}
			}
			if (isRelated) {
				entry.getValue().removeAll(deRelatedItems);
				ds.removeAll(entry.getValue());
			}
		}
	}
}
