package epros.designer.gui.file;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class RoutineFileTree extends JecnRoutineTree {

	private Logger log = Logger.getLogger(RoutineFileTree.class);
	/**所有节点*/
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	/** 管理对话框 */
	private FileManageDialog jecnManageDialog = null;
	
	public RoutineFileTree(FileManageDialog jecnManageDialog){
		this.jecnManageDialog = jecnManageDialog;
	}
	
	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFileAction().getAllFiles(JecnConstants.projectId);
		} catch (Exception e) {
			log.error("RoutineFileTree getTreeModel is error", e);
		}
		return JecnFileCommon.getFileTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		JecnFileCommon.treeMousePressed(evt, this,jecnManageDialog);
	}

}
