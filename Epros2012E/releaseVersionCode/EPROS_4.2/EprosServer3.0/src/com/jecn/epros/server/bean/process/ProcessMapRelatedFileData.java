package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class ProcessMapRelatedFileData implements Serializable {

	private Long flowId;

	private Long updatePeopleId;

	private List<JecnTreeBean> relatedRules;
	/** 标准化文件 */
	private List<JecnTreeBean> relatedStandardizeds;

	public List<JecnTreeBean> getRelatedRules() {
		return relatedRules;
	}

	public void setRelatedRules(List<JecnTreeBean> relatedRules) {
		this.relatedRules = relatedRules;
	}

	public List<JecnTreeBean> getRelatedStandardizeds() {
		return relatedStandardizeds;
	}

	public void setRelatedStandardizeds(List<JecnTreeBean> relatedStandardizeds) {
		this.relatedStandardizeds = relatedStandardizeds;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

}
