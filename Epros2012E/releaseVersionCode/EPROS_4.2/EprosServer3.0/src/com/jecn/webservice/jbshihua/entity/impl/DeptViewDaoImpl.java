package com.jecn.webservice.jbshihua.entity.impl;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;
import com.jecn.webservice.jbshihua.entity.DeptViewDao;

public class DeptViewDaoImpl extends AbsBaseDao<String, String> implements DeptViewDao {

	@Override
	public int deleteDept(String code) throws Exception {
		// TODO Auto-generated method stub
		String sql = "DELETE jecn_org_info WHERE org_num = ? ";
		return this.execteNative(sql, code);
	}

	@Override
	public int insertDept(JecnOrgView dept) throws Exception {
		String sql = "insert into jecn_org_info(org_num,org_name,per_org_num,org_status) values (?,?,?,?) ";
		return this.execteNative(sql, dept.getOrgNum(), dept.getOrgName(), dept.getPerOrgNum(), dept.getOrgStatus());
	}

	@Override
	public int insertDeptByUUID(JecnOrgView dept) throws Exception {
		String sql = "insert into jecn_org_info(org_num,org_name,per_org_num,org_status,UUID) values (?,?,?,?,?) ";
		return this.execteNative(sql, dept.getOrgNum(), dept.getOrgName(), dept.getPerOrgNum(), dept.getOrgStatus(),
				dept.getUUID());
	}

	@Override
	public int updateDeptByUUID(JecnOrgView dept) throws Exception {
		String sql = "UPDATE jecn_org_info SET org_num = ? ,org_name = ? ,per_org_num = ?,org_status = ? WHERE UUID = ? ";
		return this.execteNative(sql, dept.getOrgNum(), dept.getOrgName(), dept.getPerOrgNum(), dept.getOrgStatus(),
				dept.getUUID());
	}

	@Override
	public int updateDept(JecnOrgView dept) throws Exception {
		String sql = "UPDATE jecn_org_info SET org_num = ? ,org_name = ? ,per_org_num = ?,org_status = ? WHERE org_num = ? ";
		return this.execteNative(sql, dept.getOrgNum(), dept.getOrgName(), dept.getPerOrgNum(), dept.getOrgStatus(),
				dept.getOrgNum());
	}

	@Override
	public JecnOrgView getDept(String code) throws Exception {
		// TODO Auto-generated method stub
		String hql = "from JecnOrgView where orgNum = ?";
		return this.getObjectHql(hql, code);

	}

}
