var myhome_index_id = "";
function myhome() {
	var myhomePanel = new Ext.Panel({
		renderTo : "myhome_id",
		border : false,
		id : 'myhome_index_id',
		layout : 'border',
		height : Ext.getCmp("main-panel").getInnerHeight(),
		items : [{
			region : 'west',
			border : 0,
			title : '<div id="left_title_id" style="text-align:center;font-weight:bold">我的主页</div>',
			contentEl : 'myhomeLeft_id',
			split : true,
			collapseMode : 'mini',
			width : 180

		}, {
			region : 'center',
			border : 0,
			id : 'myhomeCenter_id',
			border : 'fit',
			title : i_myTask,
			listeners : {
				'bodyresize' : function(p, width, height) {
					if (myhome_index_id != "") {
						if (myhome_index_id == "myhome_joinProcess_id") {
							setMyProcessDivWH('joinprocess_div_id');
						} else if (myhome_index_id == "myhome_positionRepository_id"
								&& Ext.getCmp('positionRepositoryTabPanel_id') != null) {
							Ext.getCmp('positionRepositoryTabPanel_id')
									.setSize(width, height);
						} else if (myhome_index_id == "myhome_myRule_id"
								&& Ext.getCmp('atentionRuleGridGridPanel_id') != null) {
							Ext.getCmp('atentionRuleGridGridPanel_id').setSize(
									width, height - 110);
						} else if (myhome_index_id == "myhome_myJobDescription_id") {
							setMyhomeMaxjobDesWH('myJobDescription_id');
						} else if (myhome_index_id == "myhome_myRepository_id"
								&& Ext.getCmp('atentionFileGridGridPanel_id') != null) {
							Ext.getCmp('atentionFileGridGridPanel_id').setSize(
									width, height - 110);
						} else if (myhome_index_id == "myhome_publicFile_id"
								&& Ext.getCmp('publicFileGridGridPanel_id') != null) {
							Ext.getCmp('publicFileGridGridPanel_id').setSize(
									width, height - 110);
						} else if (myhome_index_id == "myhome_atentionProcess_id"
								&& Ext
										.getCmp('atentionProcessGridGridPanel_id') != null) {
							Ext.getCmp('atentionProcessGridGridPanel_id')
									.setSize(width, height - 110);
						} else if (myhome_index_id == "myhome_deptProcess_id"
								&& Ext.getCmp('deptProcessGridPanel_id') != null) {
							Ext.getCmp('deptProcessGridPanel_id').setSize(
									width, height - 110);
						} else if (myhome_index_id == "myhome_mytask_id"
								&& Ext.getCmp('myTaskGridPanel_id') != null) {
							Ext.getCmp('myTaskGridPanel_id').setSize(width,
									height - 136);
						} else if (myhome_index_id == "myhome_myWorkActive_id") {
							setMyActivitiesDivWH('myWorkActive_id_List_div');
						}

					}

				}
			}

		}]
	});
	var selectedLi;
	$("#myhomeUl_id").children().bind("click", function(e) {

		$(selectedLi).css("background",
				"url(" + basePath + "images/common/libackground.jpg)");
		e.target.style.background = "url(" + basePath
				+ "images/common/libackgroundClicked.jpg)";		
		selectedLi = e.target;
		Ext.getCmp("myhomeCenter_id").setTitle(selectedLi.innerHTML);
		Ext.getCmp("myhomeCenter_id").removeAll(true);
		Ext.getCmp("myhomeCenter_id").load({
			url : selectedLi.attributes.id.value.split(';')[0] + '?timsStamp='
					+ getTimeStamp(),
			scripts : true,
			callback : function(el, success, response) {
				if (selectedLi.attributes.id.value.split(';').length == 2) {
					eval(selectedLi.attributes.id.value.split(';')[1])();
				} else {
					myhome_index_id = "";
				}
			}
		});
			// $("#myhomeCenter_id").children().remove();
			// $("#myhomeCenter_id").load(selectedLi.attributes.url.value,function(){
			// eval(selectedLi.attributes.id.value)
			//						
			// });
	});
}

/**
 * 我参与的流程搜索
 */
function joinProcessSearch() {
	myJoinprocessGridStore.setBaseParam("processName", $("#processName_id")
					.attr("value"));
	myJoinprocessGridStore.setBaseParam("processNum", $("#processNumber_id")
					.attr("value"));
	myJoinprocessGridStore.setBaseParam("posId", $("#positionName_id").val());
	myJoinprocessGridStore.load({callback:function(){
		myJoinprocessGridPanel.getView().expandAllGroups();
	}});
}

/**
 * 重置我的任务
 */
function myTaskReset() {
	// 获取任务类型
	var taskType = document.getElementById("searchTaskType").value;
	if (taskType == -1) {// 查询所有任务
	} else if (taskType == 0) {// 流程任务
		document.getElementById("flowState").style.display = "none";
	} else if (taskType == 1) {// 文件任务
		document.getElementById("fileState").style.display = "none";
	} else if (taskType == 2) {// 制度任务
		document.getElementById("ruleState").style.display = "none";
	} else if (taskType == 4) {// 流程地图任务
		document.getElementById("flowMapState").style.display = "none";
	}
	// 显示默认的阶段
	document.getElementById("taskType").style.display = "";

	document.getElementById("taskName").value = "";
	document.getElementById("createUserName_myTask").value = "";
	document.getElementById("createUserId_myTask").value = "";
	document.getElementById("searchTaskType").options[0].selected = true;
	document.getElementById("taskType").options[0].selected = true;
	document.getElementById("startTime").value = "";
	document.getElementById("endTime").value = "";
}

/**
 * 重置我的工作活动
 */
function myActivitiesReset() {
	document.getElementById("activeName_id").value = "";
	document.getElementById("startTime").value = "";
	document.getElementById("endTime").value = "";
	document.getElementById("processName_id").value = "";
	document.getElementById("processNumber_id").value = "";
}

function posonChanged() {
	var pId = $("#posSelect_id").val();
	if (pId == "" || pId == null) {
		pId = -1;
	}
	// 提交数据
	$("#myJobDescription_id").children().remove();
	$("#myJobDescription_id").load('orgSpecificationDetail.action', {
				"posId" : pId
			}, function() {
				setMyhomeMaxjobDesWH('myJobDescription_id');
			});
	myhome_index_id == "myhome_myJobDescription_id";
}
/**
 * 我的工作活动
 */
function myWorkActive() {
	var startti = document.getElementById("startTime").value;
	var endti = document.getElementById("endTime").value
	$("#myWorkActive_id").children().remove();
	$("#myWorkActive_id").load('myprocessActive.action', {
				"processName" : document.getElementById("processName_id").value,
				"processNum" : document.getElementById("processNumber_id").value,
				"startDate" : document.getElementById("startTime").value,
				"endDate" : document.getElementById("endTime").value,
				"activeName" : document.getElementById("activeName_id").value
			}, function() {
				if ((startti != "" && endti == "")
						|| (startti == "" && endti != "")) {
					alert("请选择时间段，时间段的前后两个时间不能为空！");
					return;
				}
				setMyActivitiesDivWH('myWorkActive_id_List_div');
			});
	myhome_index_id = "myhome_myWorkActive_id";
}

function getMyJoinProcess() {
	var reader = new Ext.data.ArrayReader({}, [{
						name : 'flowId'
					},{
						name : 'posName'
					}, {
						name : 'roleName'
					}, {
						name : 'process'
					}, {
						name : 'active'
					}, {
						name : 'belongMap'
					}, {
						name : 'relatedRule'
					}, {
						name : 'relatedStan'
					}, {
						name : 'relatedRisk'
					}]);
	myJoinprocessGridStore = new Ext.data.GroupingStore({
				autoLoad : true,
				reader : reader,
				proxy : new Ext.data.HttpProxy({
							url : 'getJoinFlow.action'
						}),
				groupField : 'posName'
			});

	myJoinprocessGridModel = new Ext.grid.ColumnModel({
				defaults : {
//					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_positionName,// 岗位名称
							hidden : true,
							align : 'center',
							dataIndex : 'posName'
						}, {
							header :'<div style="text-align:center">'+i_process+'</div>',//流程
							width : 200,
							dataIndex : "process",
							menuDisabled : true,
							renderer:processNameRenderer
						}, {
							header : i_executiveRole,// 执行角色
							menuDisabled : true,
							align : 'center',
							dataIndex : "roleName"
						},  {
							header :'<div style="text-align:center">'+i_active+'</div>',//活动
							menuDisabled : true,
							width : 200,
							dataIndex : "active",
							//style:'.x-grid3-td{text-align: left;}'
							baseCls:'x-grid-left'
						},  {
							header :'<div style="text-align:center">'+i_belongProcessMap+'</div>',//所属流程地图
							menuDisabled : true,
							dataIndex : "belongMap"
						},  {
							header :'<div style="text-align:center">'+i_relatedRule+'</div>',//相关制度
							menuDisabled : true,
							hidden : isHiddenSystem==1?true:false,
							dataIndex : "relatedRule"
						},  {
							header :'<div style="text-align:center">'+i_relatedStan+'</div>',//相关标准
							menuDisabled : true,
							hidden : isHiddenStandard==1?true:false,
							dataIndex : "relatedStan"
						},  {
							header :'<div style="text-align:center;">'+i_relatedRisk+'</div>',//相关风险
							menuDisabled : true,
							hidden : isHiddenRisk==1?true:false,
							dataIndex : "relatedRisk"
						}]
			});

	myJoinprocessGridPanel = new Ext.grid.GridPanel({
				id:"myJoinprocessGrid_id",
				store : myJoinprocessGridStore,
				colModel : myJoinprocessGridModel,
				view : new Ext.grid.GroupingView({
							forceFit : true,
							groupTextTpl : '{text}'
						}),
				frame : false,
				bodyStyle:'border-left: 0px;',
				height : Ext.getCmp("main-panel").getInnerHeight() - 160,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				columnLines : true,// 显示列的分隔线
				enableColumnMove : false,
				renderTo : 'myJoinprocessShow_id'

			});
}