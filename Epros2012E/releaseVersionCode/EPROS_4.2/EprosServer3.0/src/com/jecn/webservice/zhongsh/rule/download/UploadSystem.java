
package com.jecn.webservice.zhongsh.rule.download;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="smisFileSystem" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="sytemFilePathName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fileType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "username",
    "password",
    "systemId",
    "smisFileSystem",
    "sytemFilePathName",
    "fileType"
})
@XmlRootElement(name = "UploadSystem")
public class UploadSystem {

    protected String username;
    protected String password;
    protected String systemId;
    protected byte[] smisFileSystem;
    protected String sytemFilePathName;
    protected int fileType;

    /**
     * 获取username属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置username属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * 获取password属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置password属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * 获取systemId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * 设置systemId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemId(String value) {
        this.systemId = value;
    }

    /**
     * 获取smisFileSystem属性的值。
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSmisFileSystem() {
        return smisFileSystem;
    }

    /**
     * 设置smisFileSystem属性的值。
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSmisFileSystem(byte[] value) {
        this.smisFileSystem = value;
    }

    /**
     * 获取sytemFilePathName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSytemFilePathName() {
        return sytemFilePathName;
    }

    /**
     * 设置sytemFilePathName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSytemFilePathName(String value) {
        this.sytemFilePathName = value;
    }

    /**
     * 获取fileType属性的值。
     * 
     */
    public int getFileType() {
        return fileType;
    }

    /**
     * 设置fileType属性的值。
     * 
     */
    public void setFileType(int value) {
        this.fileType = value;
    }

}
