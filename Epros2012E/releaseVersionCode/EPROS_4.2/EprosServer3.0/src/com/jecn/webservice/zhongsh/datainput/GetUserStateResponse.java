
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserStateResult" type="{http://tempuri.org/}User" minOccurs="0"/>
 *         &lt;element name="userState" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserStateResult",
    "userState"
})
@XmlRootElement(name = "GetUserStateResponse")
public class GetUserStateResponse {

    @XmlElement(name = "GetUserStateResult")
    protected User getUserStateResult;
    protected boolean userState;

    /**
     * 获取getUserStateResult属性的值。
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getGetUserStateResult() {
        return getUserStateResult;
    }

    /**
     * 设置getUserStateResult属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setGetUserStateResult(User value) {
        this.getUserStateResult = value;
    }

    /**
     * 获取userState属性的值。
     * 
     */
    public boolean isUserState() {
        return userState;
    }

    /**
     * 设置userState属性的值。
     * 
     */
    public void setUserState(boolean value) {
        this.userState = value;
    }

}
