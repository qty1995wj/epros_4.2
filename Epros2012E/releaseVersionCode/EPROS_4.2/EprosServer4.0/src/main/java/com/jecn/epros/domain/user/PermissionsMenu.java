package com.jecn.epros.domain.user;

import java.util.List;

/**
 * 权限菜单
 * Created by ZXH on 2017/2/28.
 */
public class PermissionsMenu {
    private String id;
    private String module;
    private String route;
    private String name;
    private String sortId;
    private String pId;
    /**
     * '0：菜单节点
     * 1：功能模块节点'
     */
    private int type;
    /**
     * 权限子菜单
     */
    private List<PermissionsMenu> children;
    /**
     * 图标路径
     */
    private String icon;

    private String contentType;

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<PermissionsMenu> getChildren() {
        return children;
    }

    public void setChildren(List<PermissionsMenu> children) {
        this.children = children;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    
}
