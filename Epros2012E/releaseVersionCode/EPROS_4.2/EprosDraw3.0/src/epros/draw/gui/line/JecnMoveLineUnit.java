package epros.draw.gui.line;

import java.awt.Cursor;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.gui.workflow.zoom.JecnWorkflowZoomProcess;
import epros.draw.util.DrawCommon;

/**
 * 拖动曼哈顿小线段算法
 * 
 * @author Administrator
 * @date： 日期：Apr 16, 2012 时间：11:19:41 AM
 */
public class JecnMoveLineUnit {
	/** 拖动线时 第一次点击鼠标的坐标 */
	private Point mouseOldPoint = null;
	/** 拖动时生成的虚拟线段 */
	private JecnTempLine tempDragLine;
	/** 曼哈顿线开始点 */
	private Point manLineSPoint = null;
	/** 曼哈顿线结束点 */
	private Point manLineEPoint = null;
	/** 鼠标拖动相对鼠标原始状态横向位移 */
	private int diffX = 0;
	/** 鼠标拖动相对鼠标原始状态纵向位移 */
	private int diffY = 0;
	/** 拖动生成的临时线段开始点X坐标 */
	private int tempStX = 0;
	/** 拖动生成的临时线段开始点Y坐标 */
	private int tempEtX = 0;
	/** 拖动生成的临时线段结束点X坐标 */
	private int tempStY = 0;
	/** 拖动生成的临时线段结束点Y坐标 */
	private int tempEtY = 0;
	/** 小线段备份集合 */
	private List<LineSegment> lineSegmentsClone = null;
	/** 线段过短标准值 当线段长度小于10时视为线段过短 */
	private int length = 10;

	/**
	 * 拖动当前线段线段包含开始点或结束点状态
	 * 
	 * @author ZHANGXH
	 * @date： 日期：Apr 17, 2012 时间：10:14:41 AM
	 */
	public enum JecnMoveLineUnitType {
		containStartPoint, // 包含开始点
		containEndPoint, // 包含结束点
		containTwoPoints, // 包含两个端点
		noContainPoint, // 既不包含开始点也不包含结束点
		error
		// 错误线段
	}

	/**
	 * 获取拖动当前小线段包含端点的状态
	 * 
	 * @param startPoint
	 *            当前线段的开始点
	 * @param endPoint
	 *            当前线段的结束点
	 * @return JecnMoveLineUnitType 拖动线段的类型标识
	 */
	private JecnMoveLineUnitType getLineUnitType(Point startPoint,
			Point endPoint) {
		if (isContainsPort(startPoint) && isContainsPort(endPoint)) {// 包含两个端点
			return JecnMoveLineUnitType.containTwoPoints;
		} else if (isContainsPort(startPoint)) {// 包含开始点
			return JecnMoveLineUnitType.containStartPoint;
		} else if (isContainsPort(endPoint)) {// 包含结束点
			return JecnMoveLineUnitType.containEndPoint;
		} else if (!isContainsPort(startPoint) && !isContainsPort(endPoint)) {// 既不包含开始点也不包含结束点
			return JecnMoveLineUnitType.noContainPoint;
		}
		return JecnMoveLineUnitType.error;
	}

	/**
	 * 输入坐标是否与开始端点或结束端点相同
	 * 
	 * @param point
	 * @return
	 */
	private boolean isContainsPort(Point point) {
		if ((manLineSPoint.x == point.x && manLineSPoint.y == point.y)
				|| (manLineEPoint.x == point.x && manLineEPoint.y == point.y)) {
			return true;
		}
		return false;
	}

	/**
	 * 根据线的方向重新获取线的开始点和结束点
	 * 
	 * @param lineDirectionEnum
	 */
	private void changePointByType(LineDirectionEnum lineDirectionEnum) {
		// 临时端点
		Point tempPoint = null;
		switch (lineDirectionEnum) {
		case horizontal: // 横线 默认开始点横坐标小于结束点横坐标
			if (manLineSPoint.x > manLineEPoint.x) {
				tempPoint = manLineSPoint;
				manLineSPoint = manLineEPoint;
				manLineEPoint = tempPoint;
			}
			break;
		case vertical:// 纵线 默认开始点纵坐标小于结束点纵坐标
			if (manLineSPoint.y > manLineEPoint.y) {
				tempPoint = manLineSPoint;
				manLineSPoint = manLineEPoint;
				manLineEPoint = tempPoint;
			}
			break;
		}
	}

	/**
	 * 拖动线段为水平线时虚拟线段
	 * 
	 * @param curLineSegment
	 */
	private void drawTempHVLine(LineSegment curLineSegment) {
		if (curLineSegment == null || curLineSegment.lineSegmentData == null) {
			return;
		}
		// 根据线的方向重新获取线的开始点和结束点
		changePointByType(curLineSegment.lineSegmentData.getLineDirectionEnum());
		// TODO 获取拖动当前小线段包含端点的状态
		JecnMoveLineUnitType jecnMoveLineUnitType = getLineUnitType(
				curLineSegment.getStartPoint(), curLineSegment.getEndPoint());
		// 清空面板上虚线
		JecnWorkflowUtil.disposeAllTempLine();

		switch (jecnMoveLineUnitType) {
		case containTwoPoints:// 包含两个端点
			drawTempHVContainTwoPoints(curLineSegment);
			break;
		case containStartPoint:// 包含开始点
			drawTempHVContainStartPoint(curLineSegment);
			break;
		case containEndPoint: // 包含结束点
			drawTempHVContainEndPoint(curLineSegment);
			break;
		case noContainPoint:// 既不包含开始点也不包含结束点
			drawTempHValNoContainPoint(curLineSegment);
			break;
		}
		curLineSegment.baseManhattanLine.setTempOldPoint(null);
		// 添加虚线到面板
		curLineSegment.baseManhattanLine.paintTempLine();
		// 拖动线段箭头方向
		mouseEvent(tempDragLine, curLineSegment.lineSegmentData
				.getLineDirectionEnum());
	}

	/**
	 * 拖动线，包含两个端点
	 * 
	 * @param curLineSegment
	 */
	private void drawTempHVContainTwoPoints(LineSegment curLineSegment) {
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;

		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:// 线段方向 水平
			// 获取横线开始点和结束点的距离
			double distanceXValue = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getWidthDouble();
			// 当前线段横线端点横坐标最小值
			double minX = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getMinX();

			// 离原点近x坐标
			int x1 = DrawCommon.convertDoubleToInt(minX + distanceXValue / 4);
			// 离原点远x坐标
			int x2 = DrawCommon.convertDoubleToInt(minX + 3 * distanceXValue / 4);
			// y坐标
			int y = curLineSegment.getStartPoint().y;

			point1 = new Point(x1, y);
			point2 = new Point(x1, y + diffY);
			point3 = new Point(x2, y + diffY);
			point4 = new Point(x2, y);
			break;
		case vertical:// 线段方向 垂直
			// 获取纵线开始点和结束点的距离
			double distanctempEtYValue = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getHeightDouble();
			// 当前线段纵线端点纵坐标最小值
			double minY = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getMinY();

			// 离原点近y坐标
			int y1 = DrawCommon.convertDoubleToInt(minY + distanctempEtYValue / 4);
			// 离原点远y坐标
			int y2 = DrawCommon
					.convertDoubleToInt(minY + distanctempEtYValue * 3 / 4);
			// x坐标
			int x = curLineSegment.getStartPoint().x;

			point1 = new Point(x, y1);
			point2 = new Point(x + diffX, y1);
			point3 = new Point(x + diffX, y2);
			point4 = new Point(x, y2);
			break;
		}
		// 拖动线段错误点纠正
		isErrorPoint(point3, point2);
		// 根据获取的点画线
		curLineSegment.baseManhattanLine.setTempOldPoint(point1);
		curLineSegment.baseManhattanLine.addTempPoint(point2);
		tempDragLine = curLineSegment.baseManhattanLine.addTempPoint(point3);
		curLineSegment.baseManhattanLine.addTempPoint(point4);
	}

	/**
	 * 拖动线为横线，包含开始点
	 * 
	 * @param curLineSegment
	 */
	private void drawTempHVContainStartPoint(LineSegment curLineSegment) {
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;

		// 获取小线段开始点和结束点横纵坐标值
		double curLineStX = curLineSegment.getLineSegmentData()
				.getOriginalStartXYData().getX();
		double curLineStY = curLineSegment.getLineSegmentData()
				.getOriginalStartXYData().getY();
		double curLineEndX = curLineSegment.getLineSegmentData()
				.getOriginalEndXYData().getX();
		double curLineEndY = curLineSegment.getLineSegmentData()
				.getOriginalEndXYData().getY();

		int curLineEndXInt = DrawCommon.convertDoubleToInt(curLineEndX);
		int curLineEndYInt = DrawCommon.convertDoubleToInt(curLineEndY);

		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			// 获取横线开始点和结束点的距离
			double distanceXValue = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getWidthDouble();

			// 当线段长度小于40时不允许拖动
			if (Math.abs(curLineSegment.getStartPoint().x
					- curLineSegment.getEndPoint().x) <= 40) {
				curLineSegment.remove(curLineSegment.lineResizePanelEnd);
				return;
			}

			int curLineEndXInt2 = DrawCommon.convertDoubleToInt(curLineEndX
					+ distanceXValue * 3 / 4);
			int curLineEndYInt2 = DrawCommon.convertDoubleToInt(curLineEndY + diffY);

			int curLineStYInt = DrawCommon.convertDoubleToInt(curLineStY);
			int curLineStYInt2 = DrawCommon.convertDoubleToInt(curLineStY + diffY);

			if (curLineStX > curLineEndX) {// 小线段开始点横坐标大于结束点横坐标
				point1 = new Point(curLineEndXInt, curLineEndYInt);
				point2 = new Point(curLineEndXInt, curLineEndYInt2);
				point3 = new Point(curLineEndXInt2, curLineStYInt2);
				point4 = new Point(curLineEndXInt2, curLineStYInt);
			} else {
				int curLineStXInt2 = DrawCommon.convertDoubleToInt(curLineStX
						+ distanceXValue / 4);
				point1 = new Point(curLineStXInt2, curLineStYInt);
				point2 = new Point(curLineStXInt2, curLineStYInt2);
				point3 = new Point(curLineEndXInt, curLineEndYInt2);
				point4 = new Point(curLineEndXInt, curLineEndYInt);
			}
			if (curLineSegment.baseManhattanLine.getFlowElementData()
					.getLineSegmentDataList().size() == 2) {// 如果小线段条数为2条时虚拟线到结束点的距离不得小于21
				if (Math.abs(point3.y - manLineEPoint.y) <= 21
						&& (point3.x == manLineEPoint.x || point2.x == manLineEPoint.x)) {
					if (point3.y - manLineEPoint.y > 0) {
						point3.y = manLineEPoint.y + 21;
						point2.y = point3.y;
					} else {
						point3.y = manLineEPoint.y - 21;
						point2.y = point3.y;
					}
				}
			}
			break;
		case vertical:
			// 获取横线开始点和结束点的距离
			double distanctempEtYValue = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getHeightDouble();

			if (curLineStY > curLineEndY) {// 比较开始点和结束点纵坐标的大小
				int curLineEndXInt22 = DrawCommon.convertDoubleToInt(curLineEndX
						+ diffX);
				int curLineEndYInt22 = DrawCommon.convertDoubleToInt(curLineEndY
						+ distanctempEtYValue * 3 / 4);
				point1 = new Point(curLineEndXInt, curLineEndYInt);
				point2 = new Point(curLineEndXInt22, curLineEndYInt);
				point3 = new Point(curLineEndXInt22, curLineEndYInt22);
				point4 = new Point(curLineEndXInt, curLineEndYInt22);
			} else {
				int curLineStXInt = DrawCommon.convertDoubleToInt(curLineStX);
				int curLineStXInt2 = DrawCommon
						.convertDoubleToInt(curLineStX + diffX);

				int curLineStYInt22 = DrawCommon.convertDoubleToInt(curLineStY
						+ distanctempEtYValue / 4);

				point1 = new Point(curLineStXInt, curLineStYInt22);
				point2 = new Point(curLineStXInt2, curLineStYInt22);
				point3 = new Point(curLineStXInt2, curLineEndYInt);
				point4 = new Point(curLineStXInt, curLineEndYInt);
			}
			if (curLineSegment.baseManhattanLine.getFlowElementData()
					.getLineSegmentDataList().size() == 2) {// 如果小线段条数为2条时虚拟线到结束点的距离不得小于21
				if (Math.abs(point3.y - manLineEPoint.y) <= 21
						&& (point3.x == manLineEPoint.x || point2.x == manLineEPoint.x)) {
					if (point3.y - manLineEPoint.y > 0) {
						point3.y = manLineEPoint.y + 21;
						point2.y = point3.y;
					} else {
						point3.y = manLineEPoint.y - 21;
						point2.y = point3.y;
					}
				}
			}
			break;
		}
		// 拖动线段错误点纠正
		isErrorPoint(point3, point2);
		curLineSegment.baseManhattanLine.setTempOldPoint(point1);
		curLineSegment.baseManhattanLine.addTempPoint(point2);
		tempDragLine = curLineSegment.baseManhattanLine.addTempPoint(point3);
		curLineSegment.baseManhattanLine.addTempPoint(point4);
	}

	/**
	 * 拖动线为横线，包含结束点
	 * 
	 * @param curLineSegment
	 */
	private void drawTempHVContainEndPoint(LineSegment curLineSegment) {
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;

		// 获取小线段开始点和结束点横纵坐标值
		double curLineStX = curLineSegment.getLineSegmentData()
				.getOriginalStartXYData().getX();
		double curLineStY = curLineSegment.getLineSegmentData()
				.getOriginalStartXYData().getY();
		double curLineEndX = curLineSegment.getLineSegmentData()
				.getOriginalEndXYData().getX();
		double curLineEndY = curLineSegment.getLineSegmentData()
				.getOriginalEndXYData().getY();

		int curLineStXInt = DrawCommon.convertDoubleToInt(curLineStX);
		int curLineStYInt = DrawCommon.convertDoubleToInt(curLineStY);

		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			// 获取横线开始点和结束点的距离
			double distanceXValue = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getWidthDouble();

			if (distanceXValue <= 40) {
				curLineSegment.remove(curLineSegment.lineResizePanelCenter);
				return;
			}

			int curLineStXInt2 = DrawCommon.convertDoubleToInt(curLineStX
					+ distanceXValue * 3 / 4);

			int curLineStYInt2 = DrawCommon.convertDoubleToInt(curLineStY + diffY);

			if (curLineEndX < curLineStX) {// 如果位移lineSt小于位移lineEnd
				int curLineEndXInt2 = DrawCommon.convertDoubleToInt(curLineEndX
						+ distanceXValue * 1 / 4);

				point1 = new Point(curLineEndXInt2, curLineStYInt);
				point2 = new Point(curLineEndXInt2, curLineStYInt2);
				point3 = new Point(curLineStXInt, curLineStYInt2);
				point4 = new Point(curLineStXInt, curLineStYInt);
			} else {
				point1 = new Point(curLineStXInt, curLineStYInt);
				point2 = new Point(curLineStXInt, curLineStYInt2);
				point3 = new Point(curLineStXInt2, curLineStYInt2);
				point4 = new Point(curLineStXInt2, curLineStYInt);
			}

			if (curLineSegment.baseManhattanLine.getFlowElementData()
					.getLineSegmentDataList().size() == 2) {// 如果小线段条数为2条时虚拟线到结束点的距离不得小于21
				if (Math.abs(point3.y - manLineSPoint.y) <= 21
						&& (point3.x == manLineSPoint.x || point2.x == manLineSPoint.x)) {
					if (point3.y - manLineSPoint.y > 0) {
						point3.y = manLineSPoint.y + 21;
						point2.y = point3.y;
					} else {
						point3.y = manLineSPoint.y - 21;
						point2.y = point3.y;
					}
				}
			}
			break;
		case vertical:
			// 当线段长度小于25时不允许拖动
			// if (Math.abs(curLineSegment.getStartPoint().y
			// - curLineSegment.getEndPoint().y) <= 40) {
			// curLineSegment.remove(curLineSegment.lineResizeHandle3);
			// return;
			// }
			// 获取横线开始点和结束点的距离
			double distanctempEtYValue = curLineSegment.getLineSegmentData()
					.getLineSegmentData().getHeightDouble();

			int curLineStXInt22 = DrawCommon.convertDoubleToInt(curLineStX + diffX);
			int curLineStYInt22 = DrawCommon.convertDoubleToInt(curLineStY
					+ distanctempEtYValue * 3 / 4);
			if (curLineEndY < curLineStY) {// 比较开始点和结束点到ManhattanLine纵坐标的位移大小
				int curLineEndYInt2 = DrawCommon.convertDoubleToInt(curLineEndY
						+ distanctempEtYValue * 1 / 4);

				point1 = new Point(curLineStXInt, curLineEndYInt2);
				point2 = new Point(curLineStXInt22, curLineEndYInt2);
				point3 = new Point(curLineStXInt22, curLineStYInt);
				point4 = new Point(curLineStXInt, curLineStYInt);
			} else {
				point1 = new Point(curLineStXInt, curLineStYInt);
				point2 = new Point(curLineStXInt22, curLineStYInt);
				point3 = new Point(curLineStXInt22, curLineStYInt22);
				point4 = new Point(curLineStXInt, curLineStYInt22);
			}
			break;
		}
		// 拖动线段错误点纠正
		isErrorPoint(point3, point2);
		curLineSegment.baseManhattanLine.setTempOldPoint(point1);
		curLineSegment.baseManhattanLine.addTempPoint(point2);
		tempDragLine = curLineSegment.baseManhattanLine.addTempPoint(point3);
		curLineSegment.baseManhattanLine.addTempPoint(point4);
	}

	/**
	 * 拖动线为横线，不包含端点
	 * 
	 * @param curLineSegment
	 */
	private void drawTempHValNoContainPoint(LineSegment curLineSegment) {
		// 错误线段情况
		// if (!curLineSegment.baseManhattanLine.checkLine()) {
		// return;
		// }
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;

		// 获取小线段开始点和结束点横纵坐标值
		double curLineStX = curLineSegment.getLineSegmentData()
				.getOriginalStartXYData().getX();
		double curLineStY = curLineSegment.getLineSegmentData()
				.getOriginalStartXYData().getY();
		double curLineEndX = curLineSegment.getLineSegmentData()
				.getOriginalEndXYData().getX();
		double curLineEndY = curLineSegment.getLineSegmentData()
				.getOriginalEndXYData().getY();

		int curLineStXInt = DrawCommon.convertDoubleToInt(curLineStX);
		int curLineStYInt = DrawCommon.convertDoubleToInt(curLineStY);

		int curLineEndXInt = DrawCommon.convertDoubleToInt(curLineEndX);
		int curLineEndYInt = DrawCommon.convertDoubleToInt(curLineEndY);

		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			// 当小线段为3条切当前线段距离曼哈顿线的开始点和结束点的距离均小于20
			if (curLineSegment.baseManhattanLine.getFlowElementData()
					.getLineSegmentDataList().size() == 3) {
				if (manLineSPoint.y >= manLineEPoint.y
						&& (manLineSPoint.y > curLineSegment.getStartPoint().y && curLineSegment
								.getStartPoint().y > manLineEPoint.y)) {
					if (Math.abs(manLineSPoint.y - manLineEPoint.y) <= 40
							&& Math.abs(curLineSegment.getStartPoint().y
									- manLineSPoint.y) <= 30
							&& Math.abs(curLineSegment.getStartPoint().y
									- manLineEPoint.y) <= 30) {
						// curLineSegment.remove(curLineSegment.lineResizeHandle3);
						return;
					}
				}
				if (manLineSPoint.y <= manLineEPoint.y
						&& (manLineSPoint.y < curLineSegment.getStartPoint().y && curLineSegment
								.getStartPoint().y < manLineEPoint.y)) {
					if (Math.abs(manLineSPoint.y - manLineEPoint.y) <= 40
							&& Math.abs(curLineSegment.getStartPoint().y
									- manLineSPoint.y) <= 30
							&& Math.abs(curLineSegment.getStartPoint().y
									- manLineEPoint.y) <= 30) {
						// curLineSegment.remove(curLineSegment.lineResizeHandle3);
						return;
					}
				}
			}

			int curLineStYInt2 = DrawCommon.convertDoubleToInt(curLineStY + diffY);
			int curLineEndYInt2 = DrawCommon.convertDoubleToInt(curLineEndY + diffY);

			point1 = new Point(curLineStXInt, curLineStYInt);
			point2 = new Point(curLineStXInt, curLineStYInt2);
			point3 = new Point(curLineEndXInt, curLineEndYInt2);
			point4 = new Point(curLineEndXInt, curLineEndYInt);
			// 拖动时临时线两端点的距离到输入输出点的距离不得小于21
			if (Math.abs(manLineSPoint.y - point2.y) <= 21
					&& (point3.x == manLineSPoint.x || point2.x == manLineSPoint.x)) {
				if (manLineSPoint.y - point2.y > 0) {
					point2.y = manLineSPoint.y - 21;
					point3.y = point2.y;
				} else {
					point2.y = manLineSPoint.y + 21;
					point3.y = point2.y;
				}
			} else if (Math.abs(point2.y - manLineEPoint.y) <= 21
					&& (point2.x == manLineEPoint.x || point3.x == manLineEPoint.x)) {
				if (point2.y - manLineEPoint.y > 0) {
					point2.y = manLineEPoint.y + 21;
					point3.y = point2.y;
				} else {
					point2.y = manLineEPoint.y - 21;
					point3.y = point2.y;
				}
			}
			break;
		case vertical:
			// 当小线段为3条切当前线段距离曼哈顿线的开始点和结束点的距离均小于20
			if (curLineSegment.baseManhattanLine.getFlowElementData()
					.getLineSegmentDataList().size() == 3) {
				if (manLineSPoint.x >= manLineEPoint.x
						&& (manLineSPoint.x > curLineSegment.getStartPoint().x && curLineSegment
								.getStartPoint().x > manLineEPoint.x)) {
					if (Math.abs(manLineSPoint.x - manLineEPoint.x) <= 40
							&& Math.abs(curLineSegment.getStartPoint().x
									- manLineSPoint.x) <= 30
							&& Math.abs(curLineSegment.getStartPoint().x
									- manLineEPoint.x) <= 30) {
						// curLineSegment.remove(curLineSegment.lineResizeHandle3);
						return;
					}
				}
				if (manLineSPoint.x <= manLineEPoint.x
						&& (manLineSPoint.x < curLineSegment.getStartPoint().x && curLineSegment
								.getStartPoint().x < manLineEPoint.x)) {
					if (Math.abs(manLineSPoint.x - manLineEPoint.x) <= 40
							&& Math.abs(curLineSegment.getStartPoint().x
									- manLineSPoint.x) <= 30
							&& Math.abs(curLineSegment.getStartPoint().x
									- manLineEPoint.x) <= 30) {
						// curLineSegment.remove(curLineSegment.lineResizeHandle3);
						return;
					}
				}
			}

			int curLineStXInt2 = DrawCommon.convertDoubleToInt(curLineStX + diffX);
			int curLineEndXInt2 = DrawCommon.convertDoubleToInt(curLineEndX + diffX);

			point1 = new Point(curLineStXInt, curLineStYInt);
			point2 = new Point(curLineStXInt2, curLineStYInt);
			point3 = new Point(curLineEndXInt2, curLineEndYInt);
			point4 = new Point(curLineEndXInt, curLineEndYInt);
			// 拖动时临时线两端点的距离到输入输出点的距离不得小于25
			if (Math.abs(manLineSPoint.x - point2.x) <= 21
					&& (point3.y == manLineSPoint.y || point2.y == manLineSPoint.y)) {
				if (manLineSPoint.x - point2.x > 0) {
					point2.x = manLineSPoint.x - 21;
					point3.x = point2.x;
				} else {
					point2.x = manLineSPoint.x + 21;
					point3.x = point2.x;
				}
			} else if (Math.abs(point2.x - manLineEPoint.x) <= 21
					&& (point2.y == manLineEPoint.y || point3.y == manLineEPoint.y)) {
				if (point2.x - manLineEPoint.x > 0) {
					point2.x = manLineEPoint.x + 21;
					point3.x = point2.x;
				} else {
					point2.x = manLineEPoint.x - 21;
					point3.x = point2.x;
				}
			}
			break;
		}
		// 拖动线段错误点纠正
		isErrorPoint(point3, point2);
		curLineSegment.baseManhattanLine.setTempOldPoint(point1);
		curLineSegment.baseManhattanLine.addTempPoint(point2);
		tempDragLine = curLineSegment.baseManhattanLine.addTempPoint(point3);
		curLineSegment.baseManhattanLine.addTempPoint(point4);
	}

	/**
	 * 拖动线段箭头方向
	 * 
	 * @param tempLine
	 *            虚拟线段
	 * @param lineDirectionEnum
	 *            线段是横线还是纵线 horizontal : 横线 vertical : 纵线
	 */
	public void mouseEvent(JecnTempLine tempLine,
			LineDirectionEnum lineDirectionEnum) {
		if (tempLine == null) {
			return;
		}
		switch (lineDirectionEnum) {
		case horizontal:
			tempLine.setCursor(Cursor
					.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			break;
		case vertical:
			tempLine.setCursor(Cursor
					.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			break;
		default:
			break;
		}
	}

	/**
	 * 鼠标拖曳,虚拟绘图(baseLinePanel不规则)
	 * 
	 * @param mousePoint
	 *            鼠标当前的坐标值*
	 * @param curLineSegment
	 *            鼠标拖动的当前小线段
	 */
	public void drawTempLine(Point mousePoint, LineSegment curLineSegment) {
		if (mouseOldPoint == null) {
			return;
		}

		// 如果拖动线段长度过短不允许拖动

		// 1. 获取鼠标移动相对位移

		// 鼠标拖动相对鼠标原始状态横向位移
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		diffX = DrawCommon.convertDoubleToInt(mousePoint.x / scale - mouseOldPoint.x
				/ scale);
		// 鼠标拖动相对鼠标原始状态纵向向位移
		diffY = DrawCommon.convertDoubleToInt(mousePoint.y / scale - mouseOldPoint.y
				/ scale);

		// 2.获取图相应的inPort对应的克隆数据中心点(100%)
		manLineSPoint = curLineSegment.baseManhattanLine.getFlowElementData()
				.getOriginalStartFigurePoint().getPoint();

		manLineEPoint = curLineSegment.baseManhattanLine.getFlowElementData()
				.getOriginalEndFigurePoint().getPoint();
		if (manLineSPoint == null || manLineEPoint == null) {
			JecnLogConstants.LOG_JECN_MOVE_LINE_UNIT
					.error("JecnMoveLineUnit类构造函数：参数为null.manLineSPoint="
							+ manLineSPoint + "manLineEPoint = "
							+ manLineEPoint);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		// 拖动线段生成虚拟线段
		drawTempHVLine(curLineSegment);

		JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
	}

	/**
	 * 
	 * 释放鼠标，根据生产的虚拟线段生成新的连接线
	 * 
	 * @param curLineSegment
	 * @return MoveEnum
	 */
	public MoveEnum drawNewLine(LineSegment curLineSegment) {
		try {
			if (JecnDrawMainPanel.getMainPanel() == null
					|| tempDragLine == null || curLineSegment == null) {// 拖动线段不存在
				return MoveEnum.none;
			}
			// 拖动连接线画线算法
			MoveEnum ret = drawLine(curLineSegment);
			if (MoveEnum.success == ret) {// 执行成功
				// 设置拖动标识为true
				curLineSegment.getBaseManhattanLine().setDrraged(true);
				// 是否执行放大缩小算法
				if (JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale() != 1.0) {
					new JecnWorkflowZoomProcess(JecnDrawMainPanel
							.getMainPanel().getWorkflow())
							.zoomManhattanLine(curLineSegment.baseManhattanLine);
				}
			}
			// 点击连接线显示线的选中点
			curLineSegment.getBaseManhattanLine()
					.showManhattanLineResizeHandles();
			// 刷新面板
			JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();

			return ret;
		} catch (Exception ex) {
			JecnLogConstants.LOG_JECN_MOVE_LINE_UNIT.error(
					"JecnMoveLineUnit类drawNewLine方法：", ex);
			return MoveEnum.none;
		}
	}

	/**
	 * 拖动连接线画线算法
	 * 
	 * @param curLineSegment
	 * @return MoveEnum
	 * 
	 */
	private MoveEnum drawLine(LineSegment curLineSegment) {
		if (tempDragLine.getOriginalStartPt() == null
				|| tempDragLine.getOriginalEndPt() == null) {
			JecnLogConstants.LOG_JECN_MOVE_LINE_UNIT
					.error("JecnMoveLineUnit类构造函数：参数为null.tempDragLine.getOriginalStartPt()="
							+ tempDragLine.getOriginalStartPt()
							+ "tempDragLine.getOriginalEndPt() = "
							+ tempDragLine.getOriginalEndPt());
			return MoveEnum.none;
		}
		// 虚线原始状态下开始点
		Point tempStartP = tempDragLine.getOriginalStartPt();
		// 虚线原始状态下结束点
		Point tempEndP = tempDragLine.getOriginalEndPt();

		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			if (tempStartP.x < tempEndP.x) {
				tempStX = tempStartP.x;
				tempEtX = tempEndP.x;
			} else {
				tempStX = tempEndP.x;
				tempEtX = tempStartP.x;
			}
			tempStY = tempStartP.y;
			tempEtY = tempEndP.y;
			if (Math.abs(tempStartP.getY() - curLineSegment.getY() / scale) < 10) {
				// 清空面板上虚线
				JecnWorkflowUtil.disposeAllTempLine();
				return MoveEnum.none;
			}
			break;
		case vertical:
			if (tempStartP.y < tempEndP.y) {
				tempStY = tempStartP.y;
				tempEtY = tempEndP.y;
			} else {
				tempStY = tempEndP.y;
				tempEtY = tempStartP.y;
			}
			tempStX = tempStartP.x;
			tempEtX = tempEndP.x;
			if (Math.abs(tempStartP.getX() - curLineSegment.getX() / scale) < 10) {
				// 清空面板上虚线
				JecnWorkflowUtil.disposeAllTempLine();
				return MoveEnum.none;
			}
			break;
		}
		return drawVHLine(curLineSegment);
	}

	/**
	 * 
	 * 删除老小线段，添加新小线段
	 * 
	 * @param curLineSegment
	 * 
	 * @return MoveEnum
	 * 
	 */
	private MoveEnum drawVHLine(LineSegment curLineSegment) {
		if (curLineSegment.getBaseManhattanLine().checkLine()) {// 错误线段
			return MoveEnum.none;
		}
		// TODO 获取拖动当前小线段包含端点的状态
		JecnMoveLineUnitType jecnMoveLineUnitType = getLineUnitType(
				curLineSegment.getStartPoint(), curLineSegment.getEndPoint());

		try {
			// 清空面板上虚线
			JecnWorkflowUtil.disposeAllTempLine();

			// 清空面板上线段
			curLineSegment.baseManhattanLine.disposeLines();
			List<Point> listTempPoint = null;
			switch (jecnMoveLineUnitType) {
			case containTwoPoints:// 包含两个端点
				listTempPoint = drawHVContainTwoPoints(curLineSegment);
				break;
			case containStartPoint:// 包含开始点
				listTempPoint = drawHVContainStartPoint(curLineSegment);
				break;
			case containEndPoint: // 包含结束点
				listTempPoint = drawHVContainEndPoint(curLineSegment);
				break;
			case noContainPoint:// 既不包含开始点也不包含结束点
				listTempPoint = drawHValNoContainPoint(curLineSegment);
				break;
			}
			if (listTempPoint == null || listTempPoint.size() == 0) {
				return MoveEnum.rollback;
			}

			// 获取连接线数据对象
			List<JecnManhattanRouterResultData> routerResultList = curLineSegment.baseManhattanLine
					.getRouterResultData(listTempPoint);

			if (lineSegmentsClone.size() > 0) {
				for (LineSegment lineSegment : lineSegmentsClone) {
					JecnManhattanRouterResultData data = new JecnManhattanRouterResultData();
					data.setStartPoint(lineSegment.getStartPoint());
					data.setEndPint(lineSegment.getEndPoint());
					routerResultList.add(data);
				}
			}

			if (routerResultList.size() == 0) {
				return MoveEnum.rollback;
			}

			// 排序
			routerResultList = curLineSegment.baseManhattanLine
					.reOrderLinePoint(routerResultList);

			if (routerResultList.size() == 0) {
				return MoveEnum.rollback;
			}

			// 添加连接线
			curLineSegment.baseManhattanLine
					.drawLineSegmentByListData(routerResultList);
			return MoveEnum.success;
		} catch (Exception ex) {
			JecnLogConstants.LOG_JECN_MOVE_LINE_UNIT.error(
					"JecnMoveLineUnit类drawVHLine方法添加新小线段", ex);
			return MoveEnum.rollback;
		}

	}

	/**
	 * 包含两个端点
	 * 
	 * @param curLineSegment
	 */
	private List<Point> drawHVContainTwoPoints(LineSegment curLineSegment) {
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;

		// 清除所有线段
		// curLineSegment.getManhattanLine().disposeNoRemoveArrow();
		List<Point> listTempPoint = new ArrayList<Point>();

		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			point1 = new Point(tempStX, manLineSPoint.y);
			point2 = new Point(tempStX, tempDragLine.getOriginalStartPt().y);
			point3 = new Point(tempEtX, tempDragLine.getOriginalStartPt().y);
			point4 = new Point(tempEtX, manLineEPoint.y);

			break;
		case vertical:
			point1 = new Point(manLineSPoint.x, tempStY);
			point2 = new Point(tempDragLine.getOriginalStartPt().x, tempStY);
			point3 = new Point(tempDragLine.getOriginalStartPt().x, tempEtY);
			point4 = new Point(manLineEPoint.x, tempEtY);
			break;
		}
		listTempPoint.add(manLineSPoint);
		listTempPoint.add(point1);
		listTempPoint.add(point2);
		listTempPoint.add(point3);
		listTempPoint.add(point4);
		listTempPoint.add(manLineEPoint);
		// 清空备份数据
		lineSegmentsClone.clear();
		return listTempPoint;
	}

	/**
	 * 包含开始点
	 * 
	 * @param curLineSegment
	 */
	private List<Point> drawHVContainStartPoint(LineSegment curLineSegment) {
		// 包含开始点
		Point refPoint = null;
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;
		Point point5 = null;
		boolean isShort = false;

		// 清除所有线段
		// curLineSegment.getManhattanLine().disposeNoRemoveArrow();
		List<Point> listTempPoint = new ArrayList<Point>();
		// 清除原始拖动线及其关联线 获取拖动线段相关联的端点集合 (包含开始点拖动 集合只存在一个值)
		Point[] points = disposeTempLine(curLineSegment, true);
		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			if (points[0] != null) {
				refPoint = new Point(points[0].x, points[0].y);
			} else {
				refPoint = new Point(points[1].x, points[1].y);
			}
			if (curLineSegment.getStartPoint().x < curLineSegment.getEndPoint().x
					&& refPoint.x == curLineSegment.getStartPoint().x
					|| curLineSegment.getStartPoint().x > curLineSegment
							.getEndPoint().x
					&& refPoint.x == curLineSegment.getEndPoint().x) {
				point1 = curLineSegment.getStartPoint();
				point2 = new Point(tempEtX, point1.y);
				point3 = new Point(tempEtX, tempEtY);
				point4 = new Point(tempStX, tempStY);
				point5 = refPoint;
			} else {
				point1 = curLineSegment.getStartPoint();
				point2 = new Point(tempStX, curLineSegment.getEndPoint().y);
				point3 = new Point(tempStX, tempStY);
				point4 = new Point(tempEtX, tempEtY);
				point5 = refPoint;
			}

			isShort = isTooShort(point4, refPoint,
					curLineSegment.lineSegmentData.getLineDirectionEnum());
			if (isShort) {
				point3.x = point2.x;
				point3.y = point5.y;
				point5 = disposeLineByPoint(point5);
			}
			break;
		case vertical:
			// 清除原始拖动线及其关联线
			if (points[1] != null) {
				refPoint = new Point(points[1].x, points[1].y);
			} else {
				refPoint = new Point(points[0].x, points[0].y);
			}
			if (curLineSegment.getStartPoint().y > curLineSegment.getEndPoint().y
					&& refPoint.y == curLineSegment.getEndPoint().y
					|| curLineSegment.getStartPoint().y < curLineSegment
							.getEndPoint().y
					&& refPoint.y == curLineSegment.getStartPoint().y) {
				point1 = curLineSegment.getStartPoint();
				point2 = new Point(point1.x, tempEtY);
				point3 = new Point(tempEtX, tempEtY);
				point4 = new Point(tempStX, tempStY);
				point5 = refPoint;
			} else {
				point1 = curLineSegment.getStartPoint();
				point2 = new Point(point1.x, tempStY);
				point3 = new Point(tempStX, tempStY);
				point4 = new Point(tempEtX, tempEtY);
				point5 = refPoint;
			}

			isShort = isTooShort(tempDragLine.getOriginalEndPt(), refPoint,
					curLineSegment.lineSegmentData.getLineDirectionEnum());
			if (isShort) {
				point5 = disposeLineByPoint(refPoint);
				if (curLineSegment.getStartPoint().y > curLineSegment
						.getEndPoint().y
						&& refPoint.y == curLineSegment.getEndPoint().y
						|| curLineSegment.getStartPoint().y < curLineSegment
								.getEndPoint().y
						&& refPoint.y == curLineSegment.getStartPoint().y) {
					point3 = new Point(point5.x, tempEtY);
				} else {
					point3 = new Point(point5.x, tempStY);
				}
			}
			break;
		}
		if (isShort) {
			listTempPoint.add(point1);
			listTempPoint.add(point2);
			listTempPoint.add(point3);
			listTempPoint.add(point5);
		} else {
			listTempPoint.add(point1);
			listTempPoint.add(point2);
			listTempPoint.add(point3);
			listTempPoint.add(point4);
			listTempPoint.add(point5);
		}
		return listTempPoint;
	}

	/**
	 * 
	 * 包含结束点
	 * 
	 * @param curLineSegment
	 */
	private List<Point> drawHVContainEndPoint(LineSegment curLineSegment) {
		// 包含开始点
		Point refPoint = null;
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;
		Point point5 = null;
		boolean isShort = false;

		// 清除所有线段
		// curLineSegment.getManhattanLine().disposeNoRemoveArrow();
		List<Point> listTempPoint = new ArrayList<Point>();
		// 清除原始拖动线及其关联线 获取拖动线段相关联的端点集合 (包含开始点拖动 集合只存在一个值)
		Point[] points = disposeTempLine(curLineSegment, true);
		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			if (points[1] != null) {
				refPoint = new Point(points[1].x, points[1].y);
			} else {
				refPoint = new Point(points[0].x, points[0].y);
			}
			// 清除原始拖动线及其关联线
			if (curLineSegment.getStartPoint().x > curLineSegment.getEndPoint().x
					&& refPoint.x == curLineSegment.getStartPoint().x
					|| curLineSegment.getStartPoint().x < curLineSegment
							.getEndPoint().x
					&& refPoint.x == curLineSegment.getEndPoint().x) {
				point1 = refPoint;
				point2 = new Point(tempEtX, tempEtY);
				point3 = new Point(tempStX, tempStY);
				point4 = new Point(tempStX, curLineSegment.getStartPoint().y);
				point5 = curLineSegment.getEndPoint();
			} else {
				point1 = refPoint;
				point2 = new Point(tempStX, tempStY);
				point3 = new Point(tempEtX, tempEtY);
				point4 = new Point(tempEtX, curLineSegment.getStartPoint().y);
				point5 = curLineSegment.getEndPoint();
			}

			isShort = isTooShort(point1, point2, curLineSegment.lineSegmentData
					.getLineDirectionEnum());
			if (isShort) {
				point2.x = point4.x;
				point2.y = point1.y;
				point1 = disposeLineByPoint(point1);
			}
			break;
		case vertical:
			// 清除原始拖动线及其关联线
			if (points[0] != null) {
				refPoint = new Point(points[0].x, points[0].y);
			} else {
				refPoint = new Point(points[1].x, points[1].y);
			}
			if (curLineSegment.getStartPoint().y < curLineSegment.getEndPoint().y
					&& refPoint.y == curLineSegment.getEndPoint().y
					|| curLineSegment.getStartPoint().y > curLineSegment
							.getEndPoint().y
					&& refPoint.y == curLineSegment.getStartPoint().y) {
				point1 = refPoint;
				point2 = new Point(tempEtX, tempEtY);
				point3 = new Point(tempStX, tempStY);
				point4 = new Point(curLineSegment.getStartPoint().x, tempStY);
				point5 = curLineSegment.getEndPoint();
			} else {
				point1 = refPoint;
				point2 = new Point(tempStX, tempStY);
				point3 = new Point(tempEtX, tempEtY);
				point4 = new Point(curLineSegment.getStartPoint().x, tempEtY);
				point5 = curLineSegment.getEndPoint();
			}

			isShort = isTooShort(tempDragLine.getOriginalEndPt(), refPoint,
					curLineSegment.lineSegmentData.getLineDirectionEnum());
			if (isShort) {
				point1 = disposeLineByPoint(refPoint);
				if (curLineSegment.getStartPoint().y < curLineSegment
						.getEndPoint().y
						&& refPoint.y == curLineSegment.getEndPoint().y
						|| curLineSegment.getStartPoint().y > curLineSegment
								.getEndPoint().y
						&& refPoint.y == curLineSegment.getStartPoint().y) {
					point2 = new Point(point1.x, tempStY);
				} else {
					point2 = new Point(point1.x, tempEtY);
				}
			}
			break;
		}
		if (isShort) {
			listTempPoint.add(point1);
			listTempPoint.add(point2);
			listTempPoint.add(point4);
			listTempPoint.add(point5);
		} else {
			listTempPoint.add(point1);
			listTempPoint.add(point2);
			listTempPoint.add(point3);
			listTempPoint.add(point4);
			listTempPoint.add(point5);
		}
		return listTempPoint;
	}

	/**
	 * 
	 * 不包含端点
	 * 
	 * @param curLineSegment
	 */
	private List<Point> drawHValNoContainPoint(LineSegment curLineSegment) {
		// 包含开始点
		Point point1 = null;
		Point point2 = null;
		Point point3 = null;
		Point point4 = null;

		// 清除所有线段
		// curLineSegment.getManhattanLine().disposeNoRemoveArrow();
		List<Point> listTempPoint = null;
		// 清除原始拖动线及其关联线 获取拖动线段相关联的端点集合 (包含开始点拖动 集合只存在一个值)
		Point[] points = disposeTempLine(curLineSegment, true);

		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			if (points[1] != null) {
				if (points[1].x < points[0].x) {
					point1 = new Point(points[1].x, points[1].y);
					point4 = new Point(points[0].x, points[0].y);
				} else {
					point1 = new Point(points[0].x, points[0].y);
					point4 = new Point(points[1].x, points[1].y);
				}
			}
			point2 = new Point(tempStX, tempStY);
			point3 = new Point(tempEtX, tempEtY);

			break;
		case vertical:
			// 清除原始拖动线及其关联线
			if (points[1].y < points[0].y) {
				point1 = new Point(points[1].x, points[1].y);
				point4 = new Point(points[0].x, points[0].y);
			} else {
				point1 = new Point(points[0].x, points[0].y);
				point4 = new Point(points[1].x, points[1].y);
			}
			point2 = new Point(tempStX, tempStY);
			point3 = new Point(tempEtX, tempEtY);
			break;
		}

		// 验证线段是否过短
		listTempPoint = drawToShortLeftAndRight(curLineSegment, point1, point2,
				point3, point4);
		if (listTempPoint == null) {// 线段不存在过短情况
			listTempPoint = new ArrayList<Point>();
			listTempPoint.add(point1);
			listTempPoint.add(point2);
			listTempPoint.add(point3);
			listTempPoint.add(point4);
		}
		return listTempPoint;
	}

	/**
	 * 如果拖动线段距离两侧过短 处理
	 * 
	 * @param curLineSegment
	 *            当前拖动线
	 * @param point1
	 * @param point2
	 * @param point3
	 * @param point4
	 * @return
	 */
	private List<Point> drawToShortLeftAndRight(LineSegment curLineSegment,
			Point point1, Point point2, Point point3, Point point4) {

		// 当生成的线段过短时
		boolean isLeftShort = false;
		boolean isRightShort = false;

		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			// 左边相邻线段长度
			int leftSort = Math.abs(point1.y - point2.y);
			// 右侧相邻线段长度
			int rightSort = Math.abs(point3.y - point4.y);
			if (leftSort <= 10) {
				isLeftShort = true;
			}
			if (rightSort <= 10) {
				isRightShort = true;
			}
			if (isLeftShort && isRightShort) {
				if (leftSort > rightSort) {
					isLeftShort = false;
				} else if (leftSort < rightSort) {
					isRightShort = false;
				} else {

				}
			}
			break;

		case vertical:
			// 左边相邻线段长度
			int upSort = Math.abs(point1.x - point2.x);
			// 右侧相邻线段长度
			int downSort = Math.abs(point3.x - point4.x);
			if (upSort <= 10) {
				isLeftShort = true;
			}
			if (downSort <= 10) {
				isRightShort = true;
			}
			if (isLeftShort && isRightShort) {
				if (upSort > downSort) {
					isLeftShort = false;
				} else if (upSort < downSort) {
					isRightShort = false;
				}
			}
			break;
		}
		if (isLeftShort && !isRightShort) {
			return mergerLeftLine(curLineSegment, point1, point2, point3,
					point4);
		} else if (isRightShort && !isLeftShort) {
			return mergerRightLine(curLineSegment, point1, point2, point4);
		} else if (isLeftShort && isRightShort) {
			return mergerLeftAndRightLine(curLineSegment, point1, point2,
					point3, point4);
		}
		return null;
	}

	/**
	 * 检查线段长度 虚线到达图形开始点或结束点10像素内不能拖动
	 * 
	 * @param baseLinePt
	 *            (曼哈顿线的开始点或结束点)
	 * @param tempPt
	 *            拖动生成的虚拟线段(鼠标拖动的虚拟线段的开始点或结束点)
	 * @param direction
	 * @return 距离小于10 True，大于10 false
	 */
	public boolean isTooShort(Point baseLinePt, Point tempPt,
			LineDirectionEnum lineDirectionEnum) {
		int x1 = baseLinePt.x;
		int y1 = baseLinePt.y;
		int x2 = tempPt.x;
		int y2 = tempPt.y;
		switch (lineDirectionEnum) {
		case horizontal:
			if (Math.abs(y1 - y2) <= length) {
				return true;
			}
			break;
		case vertical:
			if (Math.abs(x1 - x2) <= length) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * 
	 * 备份小线段
	 * 
	 * @param curLineSegment
	 * @param zoomDouble
	 */
	public void getCurLineSegmentClone(LineSegment curLineSegment) {
		if (curLineSegment == null || curLineSegment.lineSegmentData == null
				|| curLineSegment.lineSegmentData.getLineSegmentData() == null) {// 线段数据对象不存在
			return;
		}
		List<LineSegment> lineSegments = curLineSegment.getBaseManhattanLine()
				.getLineSegments();
		// 初始化小线段备份数据集
		lineSegmentsClone = new ArrayList<LineSegment>();

		for (LineSegment lineSegment : lineSegments) {
			LineSegment tmpLine = lineSegment.clone();
			lineSegmentsClone.add(tmpLine);
		}
	}

	/**
	 * 
	 * 获取传入点对应线段的另一端端点
	 * 
	 * @param point
	 * @return
	 */
	public Point disposeLineByPoint(Point point) {
		Point resPoint = null;
		for (int i = 0; i < lineSegmentsClone.size(); i++) {
			LineSegment innerineSegment = lineSegmentsClone.get(i);
			if (innerineSegment.isContainsPort(point.x, point.y)) {// innerineSegment线段包含输入点
				// 备份数据中删除符合条件的线段
				lineSegmentsClone.remove(innerineSegment);
				if (innerineSegment.getStartPoint().x == point.x
						&& innerineSegment.getStartPoint().y == point.y) {
					return resPoint = innerineSegment.getEndPoint();// 开始点相同，返回对应的结束点
				} else {
					return resPoint = innerineSegment.getStartPoint();// 结束点相同，返回开始点
				}
			}
		}
		return resPoint;
	}

	/**
	 * 
	 * 清除原始拖动线及其关联线
	 * 
	 * @param lineSegment
	 * @param deleteRef
	 * @return
	 */
	public Point[] disposeTempLine(LineSegment lineSegment, boolean deleteRef) {

		if (lineSegmentsClone == null || lineSegmentsClone.size() == 0
				|| lineSegment == null) {
			return null;
		}
		// 记录拖动的原始线段和线段关联的关联线
		List<LineSegment> deleteLineSegments = new ArrayList<LineSegment>();
		// 记录关联线段的端点（该端点不在拖动线段上）
		Point[] points = new Point[2];
		Point point1;
		Point point2;
		// 删除圆弧
		for (int i = 0; i < lineSegmentsClone.size(); i++) {
			LineSegment tempLineSegment = lineSegmentsClone.get(i);
			if (tempLineSegment.getStartPoint().equals(
					lineSegment.getStartPoint())
					&& tempLineSegment.getEndPoint().equals(
							lineSegment.getEndPoint())) {
				lineSegment = tempLineSegment;
			}
		}
		lineSegmentsClone.remove(lineSegment);
		// 清除原始拖动线及其关联线
		if (deleteRef == true) {
			for (int i = 0; i < lineSegmentsClone.size(); i++) {
				if (lineSegment.isContainsPort(lineSegmentsClone.get(i)
						.getStartPoint().x, lineSegmentsClone.get(i)
						.getStartPoint().y)) {
					point1 = lineSegmentsClone.get(i).getEndPoint();
					if (points[0] != null) {
						points[1] = point1;
					} else {
						points[0] = point1;
					}
					deleteLineSegments.add(lineSegmentsClone.get(i));
				}
				if (lineSegment.isContainsPort(lineSegmentsClone.get(i)
						.getEndPoint().x, lineSegmentsClone.get(i)
						.getEndPoint().y)) {
					point2 = lineSegmentsClone.get(i).getStartPoint();
					if (points[1] != null) {
						points[0] = point2;
					} else {
						points[1] = point2;
					}
					deleteLineSegments.add(lineSegmentsClone.get(i));
				}
			}
			lineSegmentsClone.removeAll(deleteLineSegments);
		}
		return points;
	}

	/**
	 * 左边过短合并左边线段处理
	 * 
	 * @param curLineSegment
	 *            当前拖动线段
	 * @param point1
	 * @param point2
	 * @param point4
	 * @return List<Point> tempPointList
	 */
	private List<Point> mergerLeftAndRightLine(LineSegment curLineSegment,
			Point point1, Point point2, Point point3, Point point4) {
		List<Point> tempPointList = new ArrayList<Point>();
		// 获取传入点对应线段的另一端端点
		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			// 合并左边线段
			// 当生成的线段距离 曼哈顿线的开始点和结束点距离都过短时执行 if语句
			if (Math.abs(tempStY - manLineSPoint.y) <= 10
					&& Math.abs(tempEtY - manLineEPoint.y) <= 10
					&& manLineSPoint.y != manLineEPoint.y) {
				point4.y = manLineEPoint.y;
				Point curPoint = new Point(point4.x, manLineSPoint.y);

				tempPointList.add(manLineSPoint);
				tempPointList.add(curPoint);
				tempPointList.add(point4);
				tempPointList.add(manLineEPoint);
			} else {
				// 删除包含point1的线段并得到其开始点
				point1 = disposeLineByPoint(point1);
				Point newPoint1 = new Point(point1.x, point1.y);
				// 删除包含point4的线段并得到其结束点
				point4 = disposeLineByPoint(point4);
				Point newPoint4 = new Point(point4.x, point1.y);
				// 用新点绘制
				Point newPoint = disposeLineByPoint(point4);
				tempPointList.add(newPoint1);
				tempPointList.add(newPoint4);
				if (newPoint != null) {
					tempPointList.add(newPoint);
				}
			}
			break;
		case vertical:// 垂直 up方向距离过短
			if (Math.abs(tempStX - manLineSPoint.x) <= 10
					&& Math.abs(tempEtX - manLineEPoint.x) <= 10
					&& manLineSPoint.x != manLineEPoint.x) {
				point4.x = manLineEPoint.x;
				Point curPoint = new Point(manLineSPoint.x, point4.y);

				tempPointList.add(manLineSPoint);
				tempPointList.add(curPoint);
				tempPointList.add(point4);
				tempPointList.add(manLineEPoint);

			} else {
				// 删除包含point1的线段并得到其开始点
				point1 = disposeLineByPoint(point1);
				// 删除包含point4的线段并得到其结束点
				Point newPoint1 = new Point(point1.x, point1.y);
				point4 = disposeLineByPoint(point4);
				Point newPoint4 = new Point(point1.x, point4.y);
				Point newPoint = disposeLineByPoint(point4);
				// 用新点绘制
				tempPointList.add(newPoint1);
				tempPointList.add(newPoint4);
				if (newPoint != null) {
					tempPointList.add(newPoint);
				}
			}
			break;
		}
		return tempPointList;
	}

	/**
	 * 左边过短合并左边线段处理
	 * 
	 * @param curLineSegment
	 *            当前拖动线段
	 * @param point1
	 * @param point2
	 * @param point4
	 * @return List<Point> tempPointList
	 */
	private List<Point> mergerLeftLine(LineSegment curLineSegment,
			Point point1, Point point2, Point point3, Point point4) {
		List<Point> tempPointList = new ArrayList<Point>();
		// 获取传入点对应线段的另一端端点
		point1 = disposeLineByPoint(point1);
		// 获取线段对应的另一端端点
		Point point = disposeLineByPoint(point1);
		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			// 合并左边线段
			point2.x = point4.x;
			point2.y = point1.y;
			if (point != null && Math.abs(point.x - point4.x) <= 8) {
				Point newPoint = disposeLineByPoint(point);
				point.x = point4.x;
				tempPointList.add(point4);
				tempPointList.add(point);
				if (newPoint != null) {
					tempPointList.add(newPoint);
				}
			} else {
				if (point != null) {
					tempPointList.add(point);
					tempPointList.add(point1);
				} else {
					tempPointList.add(point1);
				}
				tempPointList.add(point2);
				tempPointList.add(point4);
			}
			break;
		case vertical:// 垂直 up方向距离过短
			point3.x = point1.x;
			if (point != null && Math.abs(point.y - point4.y) <= 8) {
				Point newPoint = disposeLineByPoint(point);
				point.y = point4.y;
				tempPointList.add(point4);
				tempPointList.add(point);
				if (newPoint != null) {
					tempPointList.add(newPoint);
				}
			} else {
				if (point != null) {
					tempPointList.add(point);
					tempPointList.add(point1);
				} else {
					tempPointList.add(point1);
				}
				tempPointList.add(point3);
				tempPointList.add(point4);
			}
			break;
		}
		return tempPointList;
	}

	/**
	 * 右边边过短合并左边线段处理
	 * 
	 * @param curLineSegment
	 *            当前拖动线段
	 * @param point1
	 * @param point2
	 * @param point4
	 * @return List<Point> tempPointList
	 */
	private List<Point> mergerRightLine(LineSegment curLineSegment,
			Point point1, Point point2, Point point4) {
		List<Point> tempPointList = new ArrayList<Point>();
		point4 = disposeLineByPoint(point4);
		Point point = disposeLineByPoint(point4);
		switch (curLineSegment.lineSegmentData.getLineDirectionEnum()) {
		case horizontal:
			// 合并右边线段
			point2.x = point1.x;
			point2.y = point4.y;
			if (point != null && Math.abs(point.x - point1.x) <= 8) {
				Point newPoint = disposeLineByPoint(point);
				point.x = point1.x;
				tempPointList.add(point1);
				tempPointList.add(point);
				if (newPoint != null) {
					tempPointList.add(newPoint);
				}
			} else {
				tempPointList.add(point1);
				tempPointList.add(point2);
				tempPointList.add(point4);
				if (point != null) {
					tempPointList.add(point);
				}
			}
			break;
		case vertical:
			point2.x = point4.x;
			if (point != null && Math.abs(point4.y - point1.y) <= 8) {// 包含1和4的两条线几乎在同一水平线上
				Point newPoint = disposeLineByPoint(point);
				point.y = point1.y;
				tempPointList.add(point1);
				tempPointList.add(point);
				if (newPoint != null) {
					tempPointList.add(newPoint);
				}
			} else {
				tempPointList.add(point1);
				tempPointList.add(point2);
				tempPointList.add(point4);
				if (point != null) {
					tempPointList.add(point);
				}
			}
			break;
		}

		return tempPointList;
	}

	/**
	 * 拖动线段错误点纠正
	 * 
	 * @param point1
	 *            拖动生成的虚拟线段一端端点
	 * @param point2
	 *            拖动生成的虚拟线段一端端点
	 */
	public void isErrorPoint(Point point1, Point point2) {
		if (point1.x <= 1) {
			point1.x = 1;
		}
		if (point1.y <= 1) {
			point1.y = 1;
		}
		if (point2.x <= 1) {
			point2.x = 1;
		}
		if (point2.y <= 1) {
			point2.y = 1;
		}
	}

	public void setMouseOldPoint(Point mouseOldPoint) {
		this.mouseOldPoint = mouseOldPoint;
	}

	public enum MoveEnum {
		success, // 执行成功
		none, // 执行失败，不需要撤销恢复
		rollback
		// 执行失败，需要撤销恢复
	}
}
