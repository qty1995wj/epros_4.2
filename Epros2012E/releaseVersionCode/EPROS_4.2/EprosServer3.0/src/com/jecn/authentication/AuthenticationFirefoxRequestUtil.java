package com.jecn.authentication;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.BDF.JecnBDFAfterItem;
import com.jecn.epros.server.action.web.login.ad.crrssdxc.JecnCRS_SDXCAfterItem;
import com.jecn.epros.server.action.web.login.ad.csr.JecnCsrAfterItem;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;

/**
 * 火狐浏览器处理 （只支持IE浏览器访问）
 * 
 * @author admin
 * 
 */
public class AuthenticationFirefoxRequestUtil {
	protected static final Logger log = Logger.getLogger(AuthenticationFirefoxRequestUtil.class);

	/**
	 * 火狐跳转
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public static boolean requestToFirefox(HttpServletRequest request, HttpServletResponse response) {
		String FireFox_URL = null;
		String FireFox_URL1 = null;
		if (JecnContants.otherLoginType == 23) {
			FireFox_URL = JecnCsrAfterItem.FireFox_URL;
			FireFox_URL1 = JecnCsrAfterItem.FireFox_URL1;
		} else if (JecnContants.otherLoginType == 39) {
			FireFox_URL = JecnCRS_SDXCAfterItem.FireFox_URL;
			FireFox_URL1 = JecnCRS_SDXCAfterItem.FireFox_URL1;
		}
		if (JecnConfigTool.isFirefoxRequest() && !AuthenticationFirefoxRequestUtil.isFireFoxBrower(request)) {
			AuthenticationFirefoxRequestUtil.requestToFirefox(request, response, request.getRequestURL() + "?"
					+ request.getQueryString(), FireFox_URL, FireFox_URL1);
			return true;
		}
		return false;
	}

	// 谷歌跳转 现阶段只有巴德富邮件会调用此处 如果不是巴德富的 统一返回 不做处理
	public static boolean requestToChrome(HttpServletRequest request, HttpServletResponse response) {
		String FireFox_URL = null;
		String FireFox_URL1 = null;
		// 自定义 跳转浏览器的验证规则 默认不是谷歌的 全部跳转谷歌
		boolean validation = !AuthenticationFirefoxRequestUtil.isChromeBrower(request);
		// 巴德富
		if (JecnContants.otherLoginType == 34) {
			FireFox_URL = JecnBDFAfterItem.BROWSER_ADDRESS;
			FireFox_URL1 = JecnBDFAfterItem.BROWSER_ADDRESS_TWO;

			boolean isChrome = AuthenticationFirefoxRequestUtil.isChromeBrower(request);
			boolean isIE = isIEBrower(request);
			// 巴德富自定义规则 必须是谷歌 而且不能低于 49 版本 IE 不能低于11
			validation = ((isChrome && chromeVersionSmallThan(request, 49))
					|| (isIE && ieVersionSmallThan(request, 11)) || (!isChrome && !isIE));
		} else {
			return false;
		}
		if (validation) {
			AuthenticationFirefoxRequestUtil.requestToChrome(request, response, request.getRequestURL() + "?"
					+ request.getQueryString(), FireFox_URL, FireFox_URL1);
			return true;
		}
		return false;
	}

	public static void requestToFirefox(HttpServletRequest request, HttpServletResponse response, String url,
			String FireFox_URL, String FireFox_URL1) {
		if (JecnConfigTool.isFirefoxRequest() && !AuthenticationFirefoxRequestUtil.isFireFoxBrower(request)) {// 非火狐
			AuthenticationFirefoxRequestUtil.fireFoxBrowserRequest(url, response, FireFox_URL, FireFox_URL1);
		}
	}

	public static void requestToChrome(HttpServletRequest request, HttpServletResponse response, String url,
			String FireFox_URL, String FireFox_URL1) {
		AuthenticationFirefoxRequestUtil.fireFoxBrowserRequest(url, response, FireFox_URL, FireFox_URL1);
	}

	public static boolean isFireFoxBrower(HttpServletRequest request) {
		String agent = request.getHeader("User-Agent").toLowerCase();
		if (agent.indexOf("firefox") > 0) {
			return true;
		}
		return false;
	}

	public static boolean isChromeBrower(HttpServletRequest request) {
		String agent = request.getHeader("User-Agent").toLowerCase();
		if (agent.indexOf("chrome") > 0) {
			return true;
		}
		return false;
	}

	public static void fireFoxBrowserRequest(String url, HttpServletResponse response, String FireFox_URL,
			String FireFox_URL1) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			response.setContentType("text/html;charset=UTF-8");
			out = response.getWriter();
			if (null == out) {
				return;
			}

			out.println("<script language='javascript'>");
			// out.println("var path = '" +
			// JecnCsrAfterItem.FireFox_URL+"?strUserId=admin'");
			out.println("var path;");
			out.println("var fso=new ActiveXObject('Scripting.FileSystemObject');");
			out.println("if(fso.FileExists('" + FireFox_URL + "')){");
			out.println(" path = '" + FireFox_URL + " " + url.toString() + "';");
			// out.println(" console.log(path);");
			out.println(" }else if(fso.FileExists('" + FireFox_URL1 + "')){");
			out.println(" path = '" + FireFox_URL1 + " " + url.toString() + "';");
			// out.println(" console.log(path);");
			out.println(" }");
			out.println("var browser=navigator.appName;");
			out.println(" try{  ");
			// out.println(" alert('111')");
			out.println("var objShell = new ActiveXObject('WScript.Shell');");
			// out.println(" alert('22')");
			out.println("objShell.exec(path);");
			// out.println(" alert('33')");
			out.println("objShell = null;");
			out.println(" window.opener=null; ");
			out.println(" window.open('','_self',''); ");
			out.println(" window.close();");
			out.println(" }catch(e){");
			out.println(" try{");
			out.println(" var objShell = new ActiveXObject('WScript.Shell');");
			out.println(" objShell.exec(path);");
			out.println("  objShell = null;");
			out.println(" window.opener=null;");
			out.println(" window.open('','_self',''); ");
			out.println(" window.close();");
			out.println("  }catch(e){");
			if (JecnContants.otherLoginType == 24 || JecnContants.otherLoginType == 34) {// 谷歌
				// 十所
				// 巴德富
				out.println(" window.location.href = 'gugeGuide.htm'; ");
			} else {// 火狐
				out.println(" window.location.href = 'zzsGuide.htm'; ");
			}
			out.println(" }  }");
			out.println("</script>");
			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
					log.error("outOrgHtml方法：关闭PrintWriter out流程异常", e);
				}
			}
		}
	}

	public static void fireChromeBrowserRequest(String url, HttpServletResponse response, String FireFox_URL,
			String FireFox_URL1) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			response.setContentType("text/html;charset=UTF-8");
			out = response.getWriter();
			if (null == out) {
				return;
			}

			out.println("<script language='javascript'>");
			// out.println("var path = '" +
			// JecnCsrAfterItem.FireFox_URL+"?strUserId=admin'");
			out.println(" window.opener=null; ");
			out.println(" window.open('','_self',''); ");
			out.println(" window.location.href = 'gugeGuide.htm'; ");
			out.println("</script>");
			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
					log.error("outOrgHtml方法：关闭PrintWriter out流程异常", e);
				}
			}
		}
	}

	private static boolean isIEBrower(HttpServletRequest request) {
		String agent = request.getHeader("User-Agent").toLowerCase();
		if (agent.indexOf("rv:") != -1) {
			return true;
		}
		return agent.indexOf("msie") != -1;
	}

	private static boolean chromeVersionSmallThan(HttpServletRequest request, int compareVersion) {
		String agent = request.getHeader("User-Agent").toLowerCase();
		log.info(agent);
		String reg = "chrome/(\\d+)\\.\\d+";
		Pattern compile = Pattern.compile(reg);
		Matcher matcher = compile.matcher(agent);
		while (matcher.find()) {
			String version = matcher.group(1);
			log.info("谷歌版本:" + version);
			if (Integer.valueOf(version) < compareVersion) {
				return true;
			}
		}
		return false;
	}

	private static boolean ieVersionSmallThan(HttpServletRequest request, int compareVersion) {
		String agent = request.getHeader("User-Agent").toLowerCase();
		log.info(agent);
		if (agent.indexOf("rv:") != -1) {
			return false;
		}
		String reg = "msie (\\d+)\\.\\d+";
		Pattern compile = Pattern.compile(reg);
		Matcher matcher = compile.matcher(agent);
		while (matcher.find()) {
			String version = matcher.group(1);
			log.info("ie版本:" + version);
			if (Integer.valueOf(version) < compareVersion) {
				return true;
			}
		}
		return false;
	}

}
