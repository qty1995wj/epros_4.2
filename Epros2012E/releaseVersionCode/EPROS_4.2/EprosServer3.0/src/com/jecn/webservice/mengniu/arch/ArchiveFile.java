package com.jecn.webservice.mengniu.arch;

import java.io.Serializable;

public class ArchiveFile implements Serializable {

	// 数据来源域
	private String appid;// 调用接口名称,如"oa"

	private String apptoken;// 调用接口校验码"wwwww"

	private String companycode;// 接口调用方的单位编码

	// 数据域
	private String rid;// 主键,提交数据索引值(必填)

	private String pid;// 关联父主键
	private String archcode;// 文件档号
	private String doccode;// 件号

	private String title;// 档案标题(必填)

	private String docnumber;// 文件编号

	private String author;// 责任者

	private String docdate;// 文件日期, 格式:2017-06-26(月日不足两位补零)

	private String year;// 年度, 格式:2017

	private String retentionperiod;// 保管期限

	private String security;// 密级

	private String securitytime;// 保密年限, 填写数字, 10年填写10即可

	private String pagenum;// 页数

	private String department;// 所属(归档)部门名称

	private String departmentid;// 所属(归档)部门编码

	private String departmentsn;// 隶属（归档）部门代字
	private String section;// 隶属（归档）处室名称
	private String sectionid;// 隶属（归档）处室代码

	private String creater;// 创建(归档)人userid

	private String creatername;// 创建(归档)人姓名

	private String createtime;// 创建(归档)时间, 格式:2017-06-26 10:27:56

	private String subject;// 主题词

	private String memo;// 备注

	private String relatearchives;// 关注公文, 二维数组,
									// 包含:公文主键(rid)、文件题名(title)、字段信息,可传多个.
									// 数据格式为json

	private String knownscope;// 知悉范围, 值:1 公开, 2 控制, 3 不公开

	private String relatepersons;// 知悉人(多个人员id)

	private String relatedepartments;// 知悉部门(多个部门ID)

	private String attachment;// 附件原文, 二维数组,
								// 包含:文件名(name)、文件扩展名(ext)、文件地址(file),可传多个.
								// 数据格式为json

	// 扩展字段
	private String ext1;
	private String ext2;
	private String ext3;
	private String ext4;
	private String ext5;
	private String ext6;
	private String ext7;
	private String ext8;
	private String ext9;
	private String ext10;
	private String ext11;
	private String ext12;
	private String ext13;
	private String ext14;
	private String ext15;
	private String ext16;
	private String ext17;
	private String ext18;
	private String ext19;
	private String ext20;

	public ArchiveFile() {
		super();
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getApptoken() {
		return apptoken;
	}

	public void setApptoken(String apptoken) {
		this.apptoken = apptoken;
	}

	public String getCompanycode() {
		return companycode;
	}

	public void setCompanycode(String companycode) {
		this.companycode = companycode;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDocnumber() {
		return docnumber;
	}

	public void setDocnumber(String docnumber) {
		this.docnumber = docnumber;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDocdate() {
		return docdate;
	}

	public void setDocdate(String docdate) {
		this.docdate = docdate;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public String getSecuritytime() {
		return securitytime;
	}

	public void setSecuritytime(String securitytime) {
		this.securitytime = securitytime;
	}

	public String getPagenum() {
		return pagenum;
	}

	public void setPagenum(String pagenum) {
		this.pagenum = pagenum;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDepartmentid() {
		return departmentid;
	}

	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getCreatername() {
		return creatername;
	}

	public void setCreatername(String creatername) {
		this.creatername = creatername;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getRelatearchives() {
		return relatearchives;
	}

	public void setRelatearchives(String relatearchives) {
		this.relatearchives = relatearchives;
	}

	public String getKnownscope() {
		return knownscope;
	}

	public void setKnownscope(String knownscope) {
		this.knownscope = knownscope;
	}

	public String getRelatepersons() {
		return relatepersons;
	}

	public void setRelatepersons(String relatepersons) {
		this.relatepersons = relatepersons;
	}

	public String getRelatedepartments() {
		return relatedepartments;
	}

	public void setRelatedepartments(String relatedepartments) {
		this.relatedepartments = relatedepartments;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getArchcode() {
		return archcode;
	}

	public void setArchcode(String archcode) {
		this.archcode = archcode;
	}

	public String getDoccode() {
		return doccode;
	}

	public void setDoccode(String doccode) {
		this.doccode = doccode;
	}

	public String getRetentionperiod() {
		return retentionperiod;
	}

	public void setRetentionperiod(String retentionperiod) {
		this.retentionperiod = retentionperiod;
	}

	public String getDepartmentsn() {
		return departmentsn;
	}

	public void setDepartmentsn(String departmentsn) {
		this.departmentsn = departmentsn;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSectionid() {
		return sectionid;
	}

	public void setSectionid(String sectionid) {
		this.sectionid = sectionid;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

	public String getExt3() {
		return ext3;
	}

	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}

	public String getExt4() {
		return ext4;
	}

	public void setExt4(String ext4) {
		this.ext4 = ext4;
	}

	public String getExt5() {
		return ext5;
	}

	public void setExt5(String ext5) {
		this.ext5 = ext5;
	}

	public String getExt6() {
		return ext6;
	}

	public void setExt6(String ext6) {
		this.ext6 = ext6;
	}

	public String getExt7() {
		return ext7;
	}

	public void setExt7(String ext7) {
		this.ext7 = ext7;
	}

	public String getExt8() {
		return ext8;
	}

	public void setExt8(String ext8) {
		this.ext8 = ext8;
	}

	public String getExt9() {
		return ext9;
	}

	public void setExt9(String ext9) {
		this.ext9 = ext9;
	}

	public String getExt10() {
		return ext10;
	}

	public void setExt10(String ext10) {
		this.ext10 = ext10;
	}

	public String getExt11() {
		return ext11;
	}

	public void setExt11(String ext11) {
		this.ext11 = ext11;
	}

	public String getExt12() {
		return ext12;
	}

	public void setExt12(String ext12) {
		this.ext12 = ext12;
	}

	public String getExt13() {
		return ext13;
	}

	public void setExt13(String ext13) {
		this.ext13 = ext13;
	}

	public String getExt14() {
		return ext14;
	}

	public void setExt14(String ext14) {
		this.ext14 = ext14;
	}

	public String getExt15() {
		return ext15;
	}

	public void setExt15(String ext15) {
		this.ext15 = ext15;
	}

	public String getExt16() {
		return ext16;
	}

	public void setExt16(String ext16) {
		this.ext16 = ext16;
	}

	public String getExt17() {
		return ext17;
	}

	public void setExt17(String ext17) {
		this.ext17 = ext17;
	}

	public String getExt18() {
		return ext18;
	}

	public void setExt18(String ext18) {
		this.ext18 = ext18;
	}

	public String getExt19() {
		return ext19;
	}

	public void setExt19(String ext19) {
		this.ext19 = ext19;
	}

	public String getExt20() {
		return ext20;
	}

	public void setExt20(String ext20) {
		this.ext20 = ext20;
	}

}
