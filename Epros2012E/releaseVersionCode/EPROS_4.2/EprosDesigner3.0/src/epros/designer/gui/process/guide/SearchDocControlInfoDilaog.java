package epros.designer.gui.process.guide;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 查看文控信息
 * @author 2012-07-05
 *
 */
public class SearchDocControlInfoDilaog extends JecnDialog {
	/** 主面板*/
	private JecnPanel mainPanel = new JecnPanel();
	
	/** 信息面板*/
	private JecnPanel infoPanel = new JecnPanel();
	
	/** 按钮面板*/
	private JecnPanel buttonPanel = new JecnPanel();
	
	/** 版本号Lab*/
	private JLabel versionNumLab = new JLabel(JecnProperties.getValue("versionNumC"));
	
	/** 版本号Field*/
	private JecnTextField versionNumField = new JecnTextField();
	
	/** 拟稿人Lab*/
	private JLabel creatorLab = new JLabel(JecnProperties.getValue("draftPeopleC"));
	
	/** 拟稿人Field*/
	private JecnTextField creatorField = new JecnTextField();
	
	/** 开始时间Lab*/
	private JLabel startTimeLab = new JLabel(JecnProperties.getValue("stTimeC"));
	
	/** 开始时间Field*/
	private JecnTextField startTimeField = new JecnTextField();
	
	/** 结束时间Lab*/
	private JLabel endTimeLab = new JLabel(JecnProperties.getValue("eTimeC"));
	
	/** 结束时间Field*/
	private JecnTextField endTimeField = new JecnTextField();
	
	/** 返工次数Lab*/
	private JLabel reworkLab = new JLabel(JecnProperties.getValue("reworkNumC"));
	
	/** 返工次数Field*/
	private JecnTextField reworkField = new JecnTextField();
	
	/** 文控审核人Lab*/
	private JLabel docControlReviewedLab = new JLabel(JecnProperties.getValue("documentControlAuditC"));
	
	/** 文控审核人Field*/
	private JecnTextField docControlReviewedField = new JecnTextField();
	
	/** 部门审核人Lab*/
	private JLabel departReviewedLab = new JLabel(JecnProperties.getValue("deptAuditC"));
	
	/** 部门审核人Field*/
	private JecnTextField departReviewedField = new JecnTextField();
	
	/** 评审人Lab*/
	private JLabel reviewersLab = new JLabel(JecnProperties.getValue("reviewersAuditC"));
	
	/** 评审人Field*/
	private JecnTextField reviewersField = new JecnTextField();
	
	/** 批准人Lab*/
	private JLabel approverLab = new JLabel(JecnProperties.getValue("approvalAuditC"));
	
	/** 批准人Field*/
	private JecnTextField approverField = new JecnTextField();
	
	/** 变更说明Lab*/
	private JLabel changeDescLab = new JLabel(JecnProperties.getValue("descofChangeC"));
	
	/** 变更说明Area */
	private JecnTextArea changeDescArea = new JecnTextArea();
	
	/** 变更说明滚动面板 */
	private JScrollPane changeDescScrollPane = new JScrollPane(changeDescArea);
	
	/** 确定*/
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	
	/** 取消*/
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	
	/** 设置大小*/
	Dimension dimension = null;
	
	
	
	public SearchDocControlInfoDilaog(){
		this.setSize(570,440);
		this.setTitle(JecnProperties.getValue("lookHistoryRecord"));
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setResizable(true);
		
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		
		changeDescScrollPane.setBorder(null);
		changeDescScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		changeDescScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		okBut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		cancelBut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		//初始化布局
		initLayout();
	}
	
	private void initLayout(){
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 1, 5, 1);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		//版本号
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(versionNumLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(versionNumField, c);
		//拟稿人
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(creatorLab, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(creatorField, c);
		//开始时间
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(startTimeLab, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(startTimeField, c);
		//结束时间
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(endTimeLab, c);
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(endTimeField, c);
		//返工次数
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(reworkLab, c);
		c = new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(reworkField, c);
		
		//文控审核人
		c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(docControlReviewedLab, c);
		c = new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(docControlReviewedField, c);
		
		//部门审核人
		c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(departReviewedLab, c);
		c = new GridBagConstraints(1, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(departReviewedField, c);
		
		//评审人
		c = new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(reviewersLab, c);
		c = new GridBagConstraints(1, 7, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(reviewersField, c);
		
		//批准人
		c = new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(approverLab, c);
		c = new GridBagConstraints(1, 8, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(approverField, c);
		
		//变更说明
		c = new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(changeDescLab, c);
		c = new GridBagConstraints(1, 9, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(changeDescScrollPane, c);
		
		//按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		
		this.getContentPane().add(mainPanel);
		
	}
	
	/**
	 * 确定
	 */
	private void okButPerformed(){
	}
	/**
	 * 取消
	 */
	private void cancelButPerformed(){
		this.dispose();
	}
}
