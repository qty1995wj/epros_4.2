package epros.designer.gui.popedom.positiongroup;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.OldJecnMoveChooseDialog;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;
/**
 * 获得岗位组树
 * @author 2012-05-18
 *
 */
public class RoutinePosGroupMoveTree extends JecnRoutineTree{

	private static Logger log = Logger.getLogger(RoutinePosGroupMoveTree.class);
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();

	private OldJecnMoveChooseDialog jecnChooseDialog;
	
	public RoutinePosGroupMoveTree(List<Long> listIds,OldJecnMoveChooseDialog jecnChooseDialog){
		super(listIds);
		this.listIds = listIds;
		this.jecnChooseDialog = jecnChooseDialog;
	}
	
	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getPosGroup().getAllPositionGroupDir(
					JecnConstants.projectId);
		} catch (Exception e) {
			log.error("RoutinePosGroupMoveTree getTreeModel is error", e);
		}
		return PosGroupCommon.getTreeMoveModel(list, listIds);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		jecnChooseDialog.jTreeMousePressed(evt);
	}

}
