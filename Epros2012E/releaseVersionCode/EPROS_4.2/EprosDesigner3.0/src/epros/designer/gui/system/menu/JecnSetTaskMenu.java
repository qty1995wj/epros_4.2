package epros.designer.gui.system.menu;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.designer.gui.system.JecnToolbarButton;
import epros.designer.gui.task.config.TaskConfigEditDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 排列 右键菜单( 自动对齐、左对齐、水平居中、右对齐、顶端对齐、垂直居中、底端对齐)
 * 
 * @author ZXH
 * @date： 日期：12 30, 2017 时间：11:28:56 AM
 */
public class JecnSetTaskMenu extends JPopupMenu implements ActionListener {
	/** 制度配置 */
	private JMenuItem ruleTaskButton;
	/** 流程配置 */
	private JMenuItem processTaskButton;
	/** 流程地图配置 */
	private JMenuItem totalMapTaskButton;
	/** 文件配置 */
	private JMenuItem fileTaskButton;

	public JecnSetTaskMenu() {
		/**
		 * 制度任务
		 */
		ruleTaskButton = new JMenuItem();
		ruleTaskButton.setText(JecnProperties.getValue("ruleTaskTemplate"));
		// 添加事件监听
		ruleTaskButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				TaskConfigEditDialog dialog = new TaskConfigEditDialog(null, null, 2);
				dialog.setVisible(true);
			}
		});
		ruleTaskButton.setIcon(new ImageIcon("images/setingImages/ruleTaskConfig.gif"));

		/**
		 * 流程任务
		 */
		processTaskButton = new JMenuItem();
		processTaskButton.setText(JecnProperties.getValue("flowTaskTemplate"));// 流程任务
		// 添加事件监听
		processTaskButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				TaskConfigEditDialog dialog = new TaskConfigEditDialog(null, null, 0);
				dialog.setVisible(true);
			}
		});
		processTaskButton.setIcon(new ImageIcon("images/setingImages/processTaskConfig.gif"));

		// 流程地图
		totalMapTaskButton = new JMenuItem();
		totalMapTaskButton.setText(JecnProperties.getValue("flowMapTaskTemplate"));// 流程地图任务
		// 添加事件监听
		totalMapTaskButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				TaskConfigEditDialog dialog = new TaskConfigEditDialog(null, null, 4);
				dialog.setVisible(true);
			}
		});
		totalMapTaskButton.setIcon(new ImageIcon("images/setingImages/totalMapTaskConfig.gif"));

		// 文件配置
		fileTaskButton = new JMenuItem();
		fileTaskButton.setText(JecnProperties.getValue("fileTaskTemplate"));// 文件任务
		// 添加事件监听
		fileTaskButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				TaskConfigEditDialog dialog = new TaskConfigEditDialog(null, null, 1);
				dialog.setVisible(true);
			}
		});
		fileTaskButton.setIcon(new ImageIcon("images/setingImages/fileTaskConifg.gif"));
		this.add(ruleTaskButton);

		this.add(processTaskButton);
		this.add(totalMapTaskButton);
		this.add(fileTaskButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
