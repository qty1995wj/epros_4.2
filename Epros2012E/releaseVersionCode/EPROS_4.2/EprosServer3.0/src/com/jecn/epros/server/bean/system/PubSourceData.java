package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class PubSourceData implements Serializable {

	private List<PubSourceCopyRelated> all = new ArrayList<PubSourceCopyRelated>();

	public List<JecnTreeBean> getOrgs() {
		return byType(0);
	}

	public List<JecnTreeBean> getPosGroups() {
		return byType(1);
	}

	public List<JecnTreeBean> getPoses() {
		return byType(2);
	}

	public List<JecnTreeBean> getPeoples() {
		return byType(3);
	}

	private List<JecnTreeBean> byType(int type) {
		List<JecnTreeBean> result = new ArrayList<JecnTreeBean>();
		JecnTreeBean bean = null;
		for (PubSourceCopyRelated a : all) {
			if (a.getcType() == type) {
				bean = new JecnTreeBean();
				bean.setId(a.getcId());
				bean.setName(a.getcName());
				result.add(bean);
			}
		}
		return result;
	}

	// ---------------

	public List<PubSourceCopyRelated> getAll() {
		return all;
	}

	public void setAll(List<PubSourceCopyRelated> all) {
		this.all = all;
	}

}
