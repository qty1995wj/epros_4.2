package epros.designer.gui.process.flowtool;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnManageDialog;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class RoutineFlowToolTree extends JecnRoutineTree {
	private Logger log = Logger.getLogger(RoutineFlowToolTree.class);
	/**所有节点*/
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 管理对话框 */
	private JecnManageDialog jecnManageDialog = null;
	
	public RoutineFlowToolTree(JecnManageDialog jecnManageDialog){
		this.jecnManageDialog = jecnManageDialog;
	}
	
	
	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFlowTool().getSustainTools();
		} catch (Exception e) {
			log.error("RoutineFlowToolTree getTreeModel is error", e);
		}
		return JecnFlowToolCommon.getFlowToolTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		JecnFlowToolCommon.treeMousePressed(evt, this,null);
	}

}
