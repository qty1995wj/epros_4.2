package epros.designer.gui.popedom.role.choose;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class RoleManageStandardChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RoleManageStandardChooseDialog.class);
	// stardarType 0:只能选择标准文件，1：不能选择流程图和流程地图
	private int stardarType = 0;

	public RoleManageStandardChooseDialog(List<JecnTreeBean> list) {
		super(list, 26);
		this.setTitle(JecnProperties.getValue("standardChoose"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			List<JecnTreeBean> listTreeBean = ConnectionPool.getStandardAction().searchByName(name,
					JecnConstants.projectId, JecnDesignerCommon.getSecondAdminUserId());
			return listTreeBean;
		} catch (Exception e) {
			log.error("RoleManageStandardChooseDialog searchByName is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("standardNameC");// 标准名称：
	}

	@Override
	public JecnTree getJecnTree() {
		RoleManageHighEfficiencyStandardTree tree = new RoleManageHighEfficiencyStandardTree();
		tree.setTreeType(1);
		return tree;
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("standardName"));// 标准名称：
		return title;
	}

}
