package epros.designer.gui.system.config.bean;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

/**
 * 
 * 系统设置Bean对应的设计器数据类
 * 
 * 按照类型把数据分别存储在各自list中
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigDesgBean {
	/** 对应服务端（JECN_CONFIG_ITEM）表值 */
	private List<JecnConfigItemBean> configItemBeanList = null;

	/** 大类：0：流程地图 1：流程图 2：制度 3：文件 4：邮箱配置 5：权限配置 6:其他设置 */
	private int bigType = -1;

	/**
	 * 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
	 * 7:任务操作配置8：消息邮件配置
	 */
	/** 审批环节配置 */
	private JecnConfigTypeDesgBean taskApprTypeDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_ITEM_TASKAPP);
	/** 任务页面显示配置项 */
	private JecnConfigTypeDesgBean taskShowTypeDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_ITEM_TASKSHOW);
	/** 基本信息设置 */
	private JecnConfigTypeDesgBean basicTypeDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_ITEM_BASICINFO);
	/** 流程图建设规范 */
	private JecnConfigTypeDesgBean standedTypeDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_ITEM_STAND);
	/** 流程文件（流程操作说明） */
	private JecnConfigTypeDesgBean flowFileTypeDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_ITEM_FLOWFILE);
	/** 邮箱配置 */
	private JecnConfigTypeDesgBean mailTypeDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_ITEM_MAIL);
	/** 权限配置 */
	private JecnConfigTypeDesgBean pubSetTypeDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_ITEM_PUB);

	/** *任务操作配置 */
	private JecnConfigTypeDesgBean taskOperatDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_TASK_OPERATE_ITEM_PUB);
	/** *消息邮件配置 */
	private JecnConfigTypeDesgBean messageEmailDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_MES_EMAIL_ITEM_PUB);

	/*** 合理化建议配置 */
	private JecnConfigTypeDesgBean rationProDesgBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_RATION_PRO_ITEM_PUB);
	/** 流程属性 */
	private JecnConfigTypeDesgBean flowBasicBean = new JecnConfigTypeDesgBean(JecnConfigContents.TYPE_SMALL_FLOW_BASIC);
	/** 11发布流程、流程地图、制度；相关文件是否发布相关配置 */
	private JecnConfigTypeDesgBean pubRelateFileBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_PUBLIC_RELATE_FILE);
	/** 文件下载次数配置 */
	private JecnConfigTypeDesgBean downLoadCountBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_DOWNLOAD_COUNT);
	/** 文控信息配置 **/
	private JecnConfigTypeDesgBean documentControlBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_DOCUMENT_CONTROL);

	/** 流程KPI属性名称配置 **/
	private JecnConfigTypeDesgBean flowKPIAttributeBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_SMALL_KPI_ATTRIBUTE);

	/** 流程架构-其它配置 **/
	private JecnConfigTypeDesgBean flowMapOtherBean = new JecnConfigTypeDesgBean(
			JecnConfigContents.TYPE_PROCESS_MAP_OTHER);

	public JecnConfigDesgBean(List<JecnConfigItemBean> configItemBeanList, int bigType) {
		this.configItemBeanList = configItemBeanList;
		this.bigType = bigType;
	}

	public List<JecnConfigItemBean> getConfigItemBeanList() {
		return configItemBeanList;
	}

	public JecnConfigTypeDesgBean getTaskApprTypeDesgBean() {
		return taskApprTypeDesgBean;
	}

	public JecnConfigTypeDesgBean getTaskShowTypeDesgBean() {
		return taskShowTypeDesgBean;
	}

	public JecnConfigTypeDesgBean getBasicTypeDesgBean() {
		return basicTypeDesgBean;
	}

	public JecnConfigTypeDesgBean getStandedTypeDesgBean() {
		return standedTypeDesgBean;
	}

	public JecnConfigTypeDesgBean getFlowFileTypeDesgBean() {
		return flowFileTypeDesgBean;
	}

	public JecnConfigTypeDesgBean getMailTypeDesgBean() {
		return mailTypeDesgBean;
	}

	public JecnConfigTypeDesgBean getPubSetTypeDesgBean() {
		return pubSetTypeDesgBean;
	}

	public JecnConfigTypeDesgBean getTaskOperatDesgBean() {
		return taskOperatDesgBean;
	}

	public void setTaskOperatDesgBean(JecnConfigTypeDesgBean taskOperatDesgBean) {
		this.taskOperatDesgBean = taskOperatDesgBean;
	}

	public JecnConfigTypeDesgBean getMessageEmailDesgBean() {
		return messageEmailDesgBean;
	}

	public void setMessageEmailDesgBean(JecnConfigTypeDesgBean messageEmailDesgBean) {
		this.messageEmailDesgBean = messageEmailDesgBean;
	}

	public int getBigType() {
		return bigType;
	}

	public JecnConfigTypeDesgBean getRationProDesgBean() {
		return rationProDesgBean;
	}

	public void setRationProDesgBean(JecnConfigTypeDesgBean rationProDesgBean) {
		this.rationProDesgBean = rationProDesgBean;
	}

	public JecnConfigTypeDesgBean getFlowBasicBean() {
		return flowBasicBean;
	}

	public JecnConfigTypeDesgBean getPubRelateFileBean() {
		return pubRelateFileBean;
	}

	public JecnConfigTypeDesgBean getDownLoadCountBean() {
		return downLoadCountBean;
	}

	public JecnConfigTypeDesgBean getDocumentControlBean() {
		return documentControlBean;
	}

	public void setDocumentControlBean(JecnConfigTypeDesgBean documentControlBean) {
		this.documentControlBean = documentControlBean;
	}

	public JecnConfigTypeDesgBean getFlowKPIAttributeBean() {
		return flowKPIAttributeBean;
	}

	public JecnConfigTypeDesgBean getFlowMapOtherBean() {
		return flowMapOtherBean;
	}

}
