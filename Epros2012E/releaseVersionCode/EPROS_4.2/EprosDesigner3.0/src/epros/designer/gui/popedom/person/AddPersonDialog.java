package epros.designer.gui.popedom.person;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.UserAddReturnBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;

/***
 * 添加用户
 * 
 * @author 2012-05-23
 * 
 */
public class AddPersonDialog extends PersonDialog {

	private static Logger log = Logger.getLogger(AddPersonDialog.class);

	private PersonManageDialog personManageDialog = null;

	public AddPersonDialog(PersonManageDialog personManageDialog) {
		this.personManageDialog = personManageDialog;
		initComponents();
		initLayout();
		actionInit();
		// title：添加用户
		this.setTitle(JecnProperties.getValue("addUser"));
	}

	public AddPersonDialog() {
		this.setLocationRelativeTo(null);
	}

	@Override
	public void saveData() {
		JecnUser user = new JecnUser();
		user.setLoginName(this.loginNameField.getText().trim());
		user.setPassword(JecnUtil.getSecret(new String(pwdField.getPassword()).trim()));
		user.setTrueName(this.realNameField.getText().trim());
		user.setEmail(this.emailField.getText().trim());
		user.setIsLock(isLock);
		// user.setMacAddress("");
		user.setProjectId(JecnConstants.projectId);
		user.setEmailType(emailType);
		user.setPhoneStr(this.phoneField.getText().trim());
		user.setManagerId(selectCommon.getIdResult());
		try {
			UserAddReturnBean userAddReturnBean = ConnectionPool.getPersonAction().savePerson(user, positionIds,
					systemRoleIds);
			// 增加成功
			if (userAddReturnBean.getType() == 0) {
				Vector<Object> v = new Vector<Object>();
				v.add(userAddReturnBean.getUserId());
				v.add(user.getLoginName());
				v.add(user.getTrueName());
				v.add(user.getEmail());
				v.add(JecnProperties.getValue("notLab"));
				((DefaultTableModel) this.personManageDialog.getPersonTable().getModel()).addRow(v);
				this.dispose();
			} else if (userAddReturnBean.getType() == 1) {
				// 角色人数已达上限!
				this.systemEmpLab.setText(JecnProperties.getValue("roleOver"));
			} else if (userAddReturnBean.getType() == 2) {
				// 二级管理员总数已达上限！
				this.systemEmpLab.setText(JecnProperties.getValue("secondAdminRoleOver"));
			} else if (userAddReturnBean.getType() == 3) {
				// 二级管理员总数已达上限！
				this.systemEmpLab.setText(JecnProperties.getValue("adminRoleOver"));
			}
		} catch (Exception e) {
			log.error("AddPersonDialog saveData is error！", e);
		}
	}

	@Override
	public boolean validateLoginName(String loginName) {

		try {
			return ConnectionPool.getPersonAction().validateAddLoginName(loginName);
		} catch (Exception e) {
			log.error("AddPersonDialog validateLoginName is error！", e);
		}
		return false;
	}
}
