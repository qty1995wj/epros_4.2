package epros.draw.event;

import java.awt.Point;
import java.awt.event.ActionEvent;

import javax.swing.JComponent;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.designer.JecnDesigner;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.MapNameText;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.dialog.JecnCreatePartMapDialog;
import epros.draw.gui.top.toolbar.dialog.JecnCreateTotalMapDialog;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.top.toolbar.io.JecnPrinterViewJDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.popup.JecnCloseableTabbedPopupMenu;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.visio.JecnVisioCreateFigure;

/**
 * 
 * 工具栏文件大类中动作处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFileActionProcess extends JecnAbstractActionProcess {
	public void actionPerformed(ActionEvent e, ToolBarElemType toolBarElemType) {
		if (toolBarElemType == null) {
			return;
		}
		switch (toolBarElemType) {
		case fileCreateTotalMap:// 新建流程地图
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {// 设计器

				JecnDesignerProcess.getDesignerProcess().processNewTotalMap();

			} else {// 画图工具

				// 新建流程地图
				createTotalMap();
			}
			break;
		case fileCreatePartMap:// 新建流程图
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {// 设计器

				JecnDesignerProcess.getDesignerProcess().processNewPartMap();

			} else {// 画图工具

				// 新建流程图
				createPartMap();
			}

			// *************角色移动*************//
			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (workflow == null) {// 正常情况不会为空
				return;
			}
			// 重新添加角色移动：这主要是选中画图面板时创建了角色移动，所以需要重新添加（之前创建的没有角色）
			workflow.removeAddRoleMove();

			break;
		case fileOpen: // 打开文件
			// 判断是否为设计器
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
				if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
					return;
				}
			}
			JecnFlowImport flowImport = new JecnFlowImport();
			flowImport.initialize();

			// 角色移动显示/隐藏
			if (JecnDrawMainPanel.getMainPanel().getWorkflow() != null) {
				JecnDrawMainPanel.getMainPanel().getWorkflow().removeAddRoleMove();
			}
			break;
		case visioOpen: // 打开Visio
			// 判断是否为设计器
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
				if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
					return;
				}
			}
			JecnVisioCreateFigure visioCreateFigure = new JecnVisioCreateFigure();
			visioCreateFigure.initialize();

			// 角色移动显示/隐藏
			if (JecnDrawMainPanel.getMainPanel().getWorkflow() != null) {
				JecnDrawMainPanel.getMainPanel().getWorkflow().removeAddRoleMove();
			}
			break;
		case fileViewPrint: // 预览打印
			// 判断主界面是否存在
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if ((desktopPane == null) || (!(desktopPane instanceof JComponent))) {
				return;
			}
			// 角色移动
			if (JecnSystemStaticData.isShowRoleMove()) {
				// 面板
				JecnDrawMainPanel.getMainPanel().getToolbar().getFormatToolBarItemPanel().getRoleMoveCheckBox()
						.setSelected(false);
				JecnSystemStaticData.setShowRoleMove(false);
				// 移除角色移动
				desktopPane.removeJecnRoleMobile();
				desktopPane.repaint();
			}
			if (desktopPane.getTipPanel() != null) {
				desktopPane.getTipPanel().setVisible(false);
			}
			JecnPrinterViewJDialog printerViewJDialog = new JecnPrinterViewJDialog();
			printerViewJDialog.setVisible(true);
			
			if (desktopPane.getTipPanel() != null) {
				desktopPane.getTipPanel().setVisible(true);
			}
			break;
		case fileExit:// 退出
			boolean isExitAll = false;
			if (JecnSystemStaticData.isEprosDesigner()) {
				isExitAll = JecnDesigner.removeAllPanelAndLoginOut(true);
			} else {
				isExitAll = JecnCloseableTabbedPopupMenu.removeAllPanel(true);
			}

			if (isExitAll) {
				System.exit(0);
			}
			break;
		}

	}

	/**
	 * 
	 * 新建流程地图
	 * 
	 */
	private void createTotalMap() {
		// 打开创建流程地图对话框
		JecnCreateTotalMapDialog totalMapDialog = new JecnCreateTotalMapDialog();
		totalMapDialog.setVisible(true);
		String name = totalMapDialog.getName();
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			return;
		}

		JecnFlowMapData totalMapData = new JecnFlowMapData(MapType.totalMap);
		totalMapData.setName(name);

		// 是否保存
		totalMapData.setSave(true);
		JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);
	}

	/**
	 * 
	 * 
	 * 新建流程图
	 * 
	 */
	private void createPartMap() {
		// 打开创建流程图对话框
		JecnCreatePartMapDialog partMapDialog = new JecnCreatePartMapDialog();
		partMapDialog.setVisible(true);

		if (!partMapDialog.isOkFlag()) {// 取消按钮
			return;
		}

		JecnFlowMapData partMapData = new JecnFlowMapData(MapType.partMap);
		// 名称
		partMapData.setName(partMapDialog.getPartMapCreateData().getName());
		// 横竖标识
		partMapData.setHFlag(partMapDialog.getPartMapCreateData().isHFlag());
		// 预估角色
		partMapData.setRoleCount(partMapDialog.getPartMapCreateData().getRoleCount());
		// 预估活动
		partMapData.setRoundRectCount(partMapDialog.getPartMapCreateData().getRoundRectCount());
		// 是否保存
		partMapData.setSave(false);
		JecnDrawMainPanel.getMainPanel().addWolkflow(partMapData);
		// 新建是创建默认角色和活动
		getCreateFlow(partMapData);
	}

	/**
	 * 创建流程图
	 */
	public static void getCreateFlow(JecnFlowMapData partMapData) {
		// 获取面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		workflow.getFlowMapData().setHFlag(partMapData.isHFlag());
		JecnPaintFigureUnit.addOpenTipPanel();
		// 横纵向
		if (partMapData.isHFlag()) {
			// 存在时间轴 添加元素坐标下移45像素
			int valueY = JecnDesignerProcess.getDesignerProcess().isShowProcessCustomer() ? 45 : -10;
			for (int k = 0; k <= partMapData.getRoleCount(); k++) {
				// 添加横分割线
				JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.ParallelLines, LineDirectionEnum.horizontal);
				ParallelLines baseVHLine = new ParallelLines(vhLineData);
				// 设置层级
				baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
				// 设置位置
				JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(0, 160 + valueY + 100 * k));
				// 面板最大层级加一
				workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
			}

			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.VerticalLine, LineDirectionEnum.vertical);
			VerticalLine baseVHLine = new VerticalLine(vhLineData);
			// 设置层级
			baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
			JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(160, 0));
			// 面板最大层级加一
			workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
			// 添加角色
			for (int i = 0; i < partMapData.getRoleCount(); i++) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowRoleType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(30, 180 + valueY + i * 100);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}

			int x = 180;
			if (JecnDesignerProcess.getDesignerProcess().isShowBeginningEndElement()) {
				// 添加流程开始
				JecnBaseFlowElementPanel startFigureAR = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.FlowFigureStart);
				JecnBaseFigurePanel startFigureARFigure = (JecnBaseFigurePanel) startFigureAR;
				Point startPoint = new Point(x, 190 + valueY);
				JecnAddFlowElementUnit.addFigure(startPoint, startFigureARFigure);
				x = 330;
			}
			// 添加活动
			int index = 0;
			for (int j = 0; j < partMapData.getRoundRectCount(); j++) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowActiveType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(x + j * 150, 180 + valueY);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
				index = j;
			}

			if (JecnDesignerProcess.getDesignerProcess().isShowBeginningEndElement()) {
				// 添加流程结束
				JecnBaseFlowElementPanel startFigureAR = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.FlowFigureStop);
				JecnBaseFigurePanel startFigureARFigure = (JecnBaseFigurePanel) startFigureAR;
				Point startPoint = new Point(x + (index + 1) * 150, 190 + valueY);
				JecnAddFlowElementUnit.addFigure(startPoint, startFigureARFigure);
			}

			// 添加客户
			if (JecnDesignerProcess.getDesignerProcess().isShowProcessCustomer()) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.CustomFigure);
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(30, 80 + valueY);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}
			// 重置面板大小
			workflow.getCurrDrawPanelSizeByPanelList();

			// 添加自由文本框
			JecnBaseFlowElementPanel freeNameText = JecnCreateFlowElement.getFlowEmelentByType(MapElemType.MapNameText);
			MapNameText mapNameText = (MapNameText) freeNameText;
			Point freeTextPoint = new Point(workflow.getPreferredSize().width / 2, 30);
			mapNameText.resizeByFigureText();
			JecnAddFlowElementUnit.addFigure(freeTextPoint, mapNameText);
		} else {
			// 横竖分割线
			for (int k = 0; k <= partMapData.getRoleCount(); k++) {
				// 添加竖分割线
				JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.VerticalLine, LineDirectionEnum.vertical);
				VerticalLine baseVHLine = new VerticalLine(vhLineData);
				// 设置层级
				baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
				JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(195 + 150 * k, 0));
				// 面板最大层级加一
				workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
			}
			// 添加横分割线
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.ParallelLines, LineDirectionEnum.horizontal);
			ParallelLines baseVHLine = new ParallelLines(vhLineData);
			// 设置层级
			baseVHLine.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
			JecnAddFlowElementUnit.addVHLine(baseVHLine, new Point(0, 110));
			// 面板最大层级加一
			workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
			// 添加角色
			for (int i = 0; i < partMapData.getRoleCount(); i++) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowRoleType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(220 + i * 150, 30);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}

			// 添加活动
			for (int j = 0; j < partMapData.getRoundRectCount(); j++) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
						.getCreateFlowActiveType());
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(220, 130 + j * 100);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}
			// 添加客户
			if (JecnDesignerProcess.getDesignerProcess().isShowProcessCustomer()) {
				JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement
						.getFlowEmelentByType(MapElemType.CustomFigure);
				JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
				Point point = new Point(70, 30);
				JecnAddFlowElementUnit.addFigure(point, baseFigure);
			}

			// 添加自由文本框
			JecnBaseFlowElementPanel freeText = JecnCreateFlowElement.getFlowEmelentByType(MapElemType.MapNameText);
			MapNameText mapNameText = (MapNameText) freeText;
			mapNameText.getFlowElementData().setFigureText(workflow.getFlowMapData().getName());

			// 重置面板大小
			workflow.getCurrDrawPanelSizeByPanelList();

			Point freeTextPoint = new Point(30, workflow.getPreferredSize().height / 2);
			mapNameText.resizeByFigureText();
			JecnAddFlowElementUnit.addFigure(freeTextPoint, mapNameText);
		}

	}
}
