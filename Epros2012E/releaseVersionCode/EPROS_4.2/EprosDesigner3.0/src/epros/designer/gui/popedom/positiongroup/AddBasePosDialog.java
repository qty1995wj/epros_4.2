package epros.designer.gui.popedom.positiongroup;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;

import com.jecn.epros.server.bean.dataimport.BasePosAndPostionBean;
import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;

import epros.designer.gui.common.JecnTextField;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * @ClassName: AddBasePosDialog
 * @Description: TODO
 * @author ZHANGXH
 * 
 * @date 2015-7-13 下午01:47:46
 */
public class AddBasePosDialog extends JecnDialog {

	private JecnPanel mainPanel = null;

	private JecnPanel basePosTablePanel = null;

	private JecnPanel baseShowTablePanel = null;

	private JecnPanel buttonPanel = null;

	/** table滚动面板 */
	private JScrollPane scrollPanel = null;
	/** table数据 */
	protected BasePosTable basePosable = null;
	/** 结果集 */
	private JScrollPane resuletScrollPanel = null;
	private BasePosResultTable basePosResultTable = null;

	/** 名称 */
	private JLabel nameLabel = null;
	/** 名称输入框 */
	private JecnTextField nameTextField = null;
	/** 搜索按钮 */
	private JButton searchButton = null;
	/** 确定 **/
	private JButton okButton = null;
	/** 取消 **/
	private JButton canelButton = null;
	/** 清除 */
	private JButton clearButton = null;
	/** 删除 */
	private JButton delButton = null;
	/** 树的单选节点 */
	private JecnTreeNode selectNode = null;
	/** 所有的基准岗位集合 */
	private List<JecnBasePosBean> jecnPosBeanList = null;
	/** 岗位组对应的基准岗位集合 */
	private List<JecnBasePosBean> basePosBeanList = null;

	public AddBasePosDialog(BasePosAndPostionBean basePosAndPostionBean) {
		this.jecnPosBeanList = basePosAndPostionBean.getJecnBasePosBeanList();
		this.basePosBeanList = basePosAndPostionBean.getbJecnBasePosBeanList();
		this.setResizable(true); // 设置窗体不能改变大小
		this.setModal(true); // 模态化窗体
		this.setSize(450, 600);
		this.setLocationRelativeTo(null); // 居中
		this.setTitle(JecnProperties.getValue("relBasePostion"));
		initCom();
		initLayout();
	}

	/** 初始化参数 */
	private void initCom() {
		// 名称
		nameLabel = new JLabel(JecnProperties.getValue("nameC"));
		// 名称输入框
		nameTextField = new JecnTextField();
		scrollPanel = new JScrollPane();
		basePosable = new BasePosTable(this);
		resuletScrollPanel = new JScrollPane();
		basePosResultTable = new BasePosResultTable(this);
		// 基准岗位显示table
		searchButton = new JButton(JecnProperties.getValue("search"));
		// 确定
		okButton = new JButton(JecnProperties.getValue("okBtn"));
		// 取消
		canelButton = new JButton(JecnProperties.getValue("cancelBtn"));
		// 清除
		clearButton = new JButton(JecnProperties.getValue("emptyBtn"));
		// 删除
		delButton = new JButton(JecnProperties.getValue("deleteBtn"));

		mainPanel = new JecnPanel();

		basePosTablePanel = new JecnPanel();
		baseShowTablePanel = new JecnPanel();
		buttonPanel = new JecnPanel();
		scrollPanel = new JScrollPane();
		Dimension d = new Dimension(440, 200);
		scrollPanel.setPreferredSize(d);
		scrollPanel.setMinimumSize(d);
		resuletScrollPanel.setPreferredSize(d);
		resuletScrollPanel.setMinimumSize(d);

		scrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		scrollPanel.setViewportView(basePosable);
		resuletScrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resuletScrollPanel.setViewportView(basePosResultTable);

		// 取消
		canelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canelButtonPerformed();
			}
		});

		// 确定
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonPerfomed();
			}
		});
		// 删除
		delButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				basePosResultTable.delBasePos();
			}
		});
		// 清除
		clearButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				basePosResultTable.clearBasePos();
			}
		});
		// 搜索
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				basePosable.searchBasePos(nameTextField.getText());
			}
		});

	}

	private void canelButtonPerformed() {
		this.dispose();
	}

	// 确定
	private void okButtonPerfomed() {
		SysNcTask ncTask = new SysNcTask();
		JecnLoading.start(ncTask);
		this.dispose();
	}

	class SysNcTask extends SwingWorker<Void, Void> {
		@Override
		protected Void doInBackground() throws Exception {
			try {
				basePosResultTable.updateBaseRelPos(selectNode.getJecnTreeBean().getId().toString());
			} catch (Exception e) {
				e.printStackTrace();
				/*
				 * 岗位组关联基准岗位异常
				 */
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("groupRelationPrompt"));
				throw e;
			} finally {
				done();
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	/** 初始化布局 */
	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(basePosTablePanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(baseShowTablePanel, c);

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);

		basePosTablePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("hrBasePos")));
		baseShowTablePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("result")));
		baseShowTablePanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		baseShowTablePanel.add(resuletScrollPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(clearButton);
		buttonPanel.add(delButton);
		buttonPanel.add(okButton);
		buttonPanel.add(canelButton);

		// HR基准岗位控件添加
		basePosTablePanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		basePosTablePanel.add(nameLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		basePosTablePanel.add(nameTextField, c);

		Insets insets2 = new Insets(1, 3, 1, 1);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets2, 0, 0);
		basePosTablePanel.add(searchButton, c);

		c = new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets2, 0, 0);
		basePosTablePanel.add(scrollPanel, c);

		this.getContentPane().add(mainPanel);

	}

	public List<JecnBasePosBean> getJecnPosBeanList() {
		return jecnPosBeanList;
	}

	public void setJecnPosBeanList(List<JecnBasePosBean> jecnPosBeanList) {
		this.jecnPosBeanList = jecnPosBeanList;
	}

	public List<JecnBasePosBean> getBasePosBeanList() {
		return basePosBeanList;
	}

	public void setBasePosBeanList(List<JecnBasePosBean> basePosBeanList) {
		this.basePosBeanList = basePosBeanList;
	}

	public BasePosResultTable getBasePosResultTable() {
		return basePosResultTable;
	}

	public void setBasePosResultTable(BasePosResultTable basePosResultTable) {
		this.basePosResultTable = basePosResultTable;
	}

	public JecnTreeNode getSelectNode() {
		return selectNode;
	}

	public void setSelectNode(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
	}

	// public static void main(String[] args) {
	// try {
	// // 解决ABC输入法BUG：java Swing 里面的文本框在输入的时候会弹出一个“输入窗口”
	// System.setProperty("java.awt.im.style", "on-the-spot");
	// // 外观
	// UIManager
	// .setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	// } catch (Exception e) {
	// }
	// AddBasePosDialog addBasePosDialog = new AddBasePosDialog();
	// addBasePosDialog.setVisible(true);
	// }

}
