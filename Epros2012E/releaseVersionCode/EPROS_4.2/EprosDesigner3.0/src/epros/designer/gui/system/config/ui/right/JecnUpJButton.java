package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;

/**
 * 
 * 上移按钮
 * 
 * @author Administrator
 * 
 */
public class JecnUpJButton extends JecnAbstractBaseJButton {

	public JecnUpJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// 属性面板
		JecnAbstractPropertyBasePanel selectedPanel = dialog
				.getSelectedPropContPanel();
		if (selectedPanel == null) {
			return;
		}

		// 移动处理
		selectedPanel.getTableScrollPane().upShowUItem();

		// 校验数据
		selectedPanel.check();
	}
}
