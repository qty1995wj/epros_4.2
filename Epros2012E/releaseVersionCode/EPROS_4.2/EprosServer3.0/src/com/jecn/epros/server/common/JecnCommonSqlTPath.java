package com.jecn.epros.server.common;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.util.JecnCommonSqlConstant;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.webBean.WebLoginBean;

public enum JecnCommonSqlTPath {
	INSTANCE;

	/**
	 * 查阅权限公共
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param typeAuth
	 * @return
	 */
	public String getAuthSQLCommon(Long projectId, TYPEAUTH typeAuth, WebLoginBean bean) {
		return getAuthSQLCommon(projectId, typeAuth, bean, 0);
	}

	/**
	 * 查阅权限公共
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param typeAuth
	 * @return
	 */
	public String getAuthSQLCommon(Long projectId, TYPEAUTH typeAuth, WebLoginBean bean, int accessType) {
		TmpNodeBean nodeBean = getNodeTable(typeAuth);
		Set<Long> listFigureIds = bean.getListPosIds();
		Set<Long> listOrgPathPIds = bean.getListOrgPathPIds();
		String tableName = nodeBean.getTableName();
		String id = nodeBean.getId();
		String sqlCommon = "";
		if (accessType == 0) {
			sqlCommon = " SELECT T.T_PATH ,T." + id + "   FROM " + tableName + " T" + "   WHERE 1 = 1 "
					+ isHide(typeAuth) + isDelete(typeAuth) + isDir(typeAuth) + "     AND " + projectid(typeAuth)
					+ projectId + "     AND T.IS_PUBLIC = 1" + getTableIsDirSql(typeAuth);
		} else if (accessType == 1) {
			sqlCommon = " SELECT T.T_PATH ,T." + id + "   FROM " + tableName + " T" + "   WHERE 1 <> 1 ";
		}
		if (listFigureIds == null || listFigureIds.size() == 0) {
			return sqlCommon;
		}
		String a = "";
		String b = "";
		String c = "";
		if (accessType == 1) {
			a = "     AND JOAP.ACCESS_TYPE=" + accessType;
			b = "     AND JAP.ACCESS_TYPE=" + accessType;
			c = "     AND JGP.ACCESS_TYPE=" + accessType;
		}
		return "SELECT T.T_PATH ,T. " + id + "    FROM " + tableName + " T"
				+ "   INNER JOIN JECN_ORG_ACCESS_PERMISSIONS JOAP" + "      ON JOAP.RELATE_ID = T." + id
				+ "   AND JOAP.ORG_ID IN  " + JecnCommonSql.getIdsSet(listOrgPathPIds) + "   WHERE "
				+ projectid(typeAuth) + projectId + "     AND JOAP.TYPE = " + typeAuth.getValue() + isHide(typeAuth)
				+ isDir(typeAuth) + isDelete(typeAuth) + a + "  UNION ALL" + "  SELECT T.T_PATH, T." + id + "    FROM "
				+ tableName + " T" + "   INNER JOIN JECN_ACCESS_PERMISSIONS JAP" + "      ON JAP.RELATE_ID = T." + id
				+ "   WHERE 1=1" + isDelete(typeAuth) + isHide(typeAuth) + isDir(typeAuth)
				+ "     AND JAP.FIGURE_ID IN " + JecnCommonSql.getIdsSet(listFigureIds) + "     AND "
				+ projectid(typeAuth) + projectId + "     AND JAP.TYPE = " + typeAuth.getValue() + b + "  UNION ALL"
				+ "  SELECT T.T_PATH ,T." + id + "    FROM " + tableName + " T"
				+ "   INNER JOIN JECN_GROUP_PERMISSIONS JGP" + "      ON JGP.RELATE_ID = T." + id
				+ "   INNER JOIN JECN_POSITION_GROUP_R JPGR" + "      ON JGP.POSTGROUP_ID = JPGR.GROUP_ID"
				+ "   INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + "      ON JFOI.FIGURE_ID = JPGR.FIGURE_ID"
				+ "   WHERE 1=1 " + isDelete(typeAuth) + isDir(typeAuth) + isHide(typeAuth) + "     AND "
				+ projectid(typeAuth) + projectId + "     AND JGP.TYPE = " + typeAuth.getValue()
				+ "     AND JFOI.FIGURE_ID IN " + JecnCommonSql.getIdsSet(listFigureIds) + c + "  UNION " + sqlCommon;
	}

	private String getTableIsDirSql(TYPEAUTH typeAuth) {
		switch (typeAuth) {
		case FILE:
			return "  AND T.IS_DIR<>0 ";
		case RULE:
			return "  AND T.IS_DIR<>0 ";
		case STANDARD:
			return "  AND T.stan_type<>0 ";
		default:
			return "";
		}
	}

	/**
	 * 数展开
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param typeAuth
	 * @param bean
	 * @return
	 */
	public String getTreeAppend(Long peopleId, Long projectId, TYPEAUTH typeAuth) {
		WebLoginBean bean = JecnCommon.getWebLoginBean();
		if (bean == null) {
			throw new IllegalArgumentException("JecnCommon.getWebLoginBean() 参数非法！");
		}
		return "with MY_AUTH_FILES as (" + getAuthSQLCommon(projectId, typeAuth, bean) + ")";
	}

	/**
	 * 文件搜索
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param typeAuth
	 * @param bean
	 * @return
	 */
	public String getFileSearch(Long peopleId, Long projectId, TYPEAUTH typeAuth) {
		WebLoginBean bean = JecnCommon.getWebLoginBean();
		if (bean == null) {
			throw new IllegalArgumentException("JecnCommon.getWebLoginBean() 参数非法！");
		}
		return " (" + getAuthSQLCommon(projectId, typeAuth, bean) + ") MY_AUTH_FILES";
	}

	private String isHide(TYPEAUTH typeAuth) {
		switch (typeAuth) {
		case FLOW:
			return " ";
		case FILE:
			return "  AND T.HIDE = 1";
		case STANDARD:
			return " ";
		case RULE:
			return " ";
		default:
			return " ";
		}
	}

	private String projectid(TYPEAUTH typeAuth) {
		switch (typeAuth) {
		case FLOW:
			return "T.PROJECTID=";
		case FILE:
			return "T.PROJECT_ID=";
		case STANDARD:
			return "T.PROJECT_ID=";
		case RULE:
			return "T.PROJECT_ID=";
		default:
			return " ";
		}

	}

	private String isDir(TYPEAUTH typeAuth) {
		switch (typeAuth) {
		case FLOW:
			return " ";
		case FILE:
			return "  AND T.IS_DIR = 1";
		case STANDARD:
			return " ";
		case RULE:
			return " ";
		default:
			return " ";
		}
	}

	private String isDelete(TYPEAUTH typeAuth) {
		switch (typeAuth) {
		case FLOW:
			return " AND  T.DEL_STATE = 0 ";
		case FILE:
			return " AND  T.DEL_STATE = 0 ";
		case STANDARD:
			return "  ";
		case RULE:
			return " ";
		default:
			return "";
		}
	}

	/**
	 * 查询权限类型（流程、文件、标准、制度）
	 * 
	 * @param typeAuth
	 * @return
	 */
	public TmpNodeBean getNodeTable(TYPEAUTH typeAuth) {
		switch (typeAuth) {
		case FLOW:
			return new TmpNodeBean(JecnCommonSqlConstant.FLOW_TABLE_NAME, JecnCommonSqlConstant.FLOW_ID,
					JecnCommonSqlConstant.FLOW_PID);
		case FILE:
			return new TmpNodeBean(JecnCommonSqlConstant.FILE_TABLE_NAME, JecnCommonSqlConstant.FILE_ID,
					JecnCommonSqlConstant.FILE_PID);
		case STANDARD:
			return new TmpNodeBean(JecnCommonSqlConstant.STANDART_TABLE_NAME, JecnCommonSqlConstant.STANDART_ID,
					JecnCommonSqlConstant.STANDART_PID);
		case RULE:
			return new TmpNodeBean(JecnCommonSqlConstant.RULE_TABLE_NAME, JecnCommonSqlConstant.RULE_ID,
					JecnCommonSqlConstant.RULE_PID);
		default:
			return null;
		}
	}

	class TmpNodeBean {
		private String tableName;
		private String id;
		private String pId;

		public TmpNodeBean(String tableName, String id, String pId) {
			this.tableName = tableName;
			this.id = id;
			this.pId = pId;
		}

		public String getTableName() {
			return tableName;
		}

		public void setTableName(String tableName) {
			this.tableName = tableName;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getpId() {
			return pId;
		}

		public void setpId(String pId) {
			this.pId = pId;
		}
	}

	/*
	 * 获取sql截取字符串函数封装
	 * 
	 * @return
	 */
	public String getSqlFuncSubStr() {
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			return " INNER JOIN MY_AUTH_FILES MAF ON T.T_PATH = SUBSTR(MAF.T_PATH, 0, LENGTH(T.T_PATH))";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			return " INNER JOIN MY_AUTH_FILES MAF ON  T.T_PATH = SUBSTRING(MAF.T_PATH, 0, LEN(T.T_PATH)+1)";
		} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
			// return
			// " INNER JOIN MY_AUTH_FILES MAF ON SUBSTR(MAF.T_PATH, 0, LENGTH(T.T_PATH))";
		}
		return "";
	}

	/**
	 * 0是流程、1是文件、2是标准、3是制度、4是风险、5：组织
	 * 
	 * @param type
	 * @param peopleId
	 * @return
	 */
	private String getDesignerAuthSqlCommonByType(int type, Long peopleId, Long projectId) {
		String sql = "";
		switch (type) {
		case 0:// 流程
			sql = "SELECT T.T_PATH" + "  FROM JECN_FLOW_STRUCTURE_T T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.FLOW_ID" + "   AND JRC.TYPE = 0" + " INNER JOIN JECN_USER_ROLE JUR"
					+ "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = " + peopleId
					+ "   AND T.PROJECTID = " + projectId;

			break;
		case 1:// 文件
			sql = "SELECT T.T_PATH" + "  FROM JECN_FILE_T T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.FILE_ID" + "   AND JRC.TYPE = 1" + " INNER JOIN JECN_USER_ROLE JUR"
					+ "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = " + peopleId
					+ "   AND T.PROJECT_ID = " + projectId;
			break;
		case 2:// 标准
			sql = "SELECT T.T_PATH" + "  FROM JECN_CRITERION_CLASSES T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.CRITERION_CLASS_ID" + "   AND JRC.TYPE = 2"
					+ " INNER JOIN JECN_USER_ROLE JUR" + "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = "
					+ peopleId + "   AND T.PROJECT_ID =  " + projectId;
			break;
		case 3:// 制度
			sql = "SELECT T.T_PATH" + "  FROM JECN_RULE_T T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.ID" + "   AND JRC.TYPE = 3" + " INNER JOIN JECN_USER_ROLE JUR"
					+ "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = " + peopleId
					+ "   AND T.PROJECT_ID =  " + projectId;
			break;
		case 4:// 风险
			sql = "SELECT T.T_PATH" + "  FROM JECN_RISK T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.ID" + "   AND JRC.TYPE = 4" + " INNER JOIN JECN_USER_ROLE JUR"
					+ "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = " + peopleId;
			break;
		case 5:// 组织
			sql = "SELECT T.T_PATH" + "  FROM JECN_FLOW_ORG T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.ORG_ID" + "   AND JRC.TYPE = 5" + " INNER JOIN JECN_USER_ROLE JUR"
					+ "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = " + peopleId
					+ "   AND T.PROJECTID = " + projectId;
			break;
		case 7:// 组织
			sql = "SELECT T.T_PATH" + "  FROM JECN_POSITION_GROUP T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.ID" + "   AND JRC.TYPE = 7" + " INNER JOIN JECN_USER_ROLE JUR"
					+ "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = " + peopleId
					+ "   AND T.PROJECT_ID = " + projectId;
			break;
		case 8:// 术语
			sql = "SELECT T.T_PATH" + "  FROM TERM_DEFINITION_LIBRARY T" + " INNER JOIN JECN_ROLE_CONTENT JRC"
					+ "    ON JRC.RELATE_ID = T.ID" + "   AND JRC.TYPE = 8" + " INNER JOIN JECN_USER_ROLE JUR"
					+ "    ON JUR.ROLE_ID = JRC.ROLE_ID" + " WHERE JUR.PEOPLE_ID = " + peopleId;
			break;
		default:
			break;
		}
		return " INNER JOIN (" + sql + ")MY_AUTH_FILES";
	}

	/**
	 * 二级管理员角色权限搜索
	 * 
	 * @param type
	 *            0是流程、1是文件、2是标准、3是制度、4是风险、5：组织
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	public String getRoleAuthSqlCommon(int type, Long peopleId, Long projectId) {
		String dbSubStr = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			dbSubStr = "  ON MY_AUTH_FILES.T_PATH = SUBSTR(T.T_PATH, 0, LENGTH(MY_AUTH_FILES.T_PATH))";
		} else {
			dbSubStr = "  ON  MY_AUTH_FILES.T_PATH = SUBSTRING(T.T_PATH, 0, LEN(MY_AUTH_FILES.T_PATH)+1)";
		}
		return getDesignerAuthSqlCommonByType(type, peopleId, projectId) + dbSubStr;
	}

	public String getRoleAuthChildsSqlCommon(int type, Long peopleId, Long projectId) {
		String dbSubStr = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			dbSubStr = "  ON (T.T_PATH = SUBSTR(MY_AUTH_FILES.T_PATH, 0, LENGTH(T.T_PATH)) OR MY_AUTH_FILES.T_PATH = SUBSTR(T.T_PATH, 0, LENGTH(MY_AUTH_FILES.T_PATH)))";
		} else {
			dbSubStr = "  ON (T.T_PATH = SUBSTRING(MY_AUTH_FILES.T_PATH, 0, LEN(T.T_PATH)+1) OR  MY_AUTH_FILES.T_PATH = SUBSTRING(T.T_PATH, 0, LEN(MY_AUTH_FILES.T_PATH)+1))";
		}
		return getDesignerAuthSqlCommonByType(type, peopleId, projectId) + dbSubStr;
	}

	/*
	 * 获取sql截取字符串函数封装
	 * 
	 * @return
	 */
	public String getSqlSubStr(String alias) {
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			return "" + alias + ".T_PATH = SUBSTR(T.T_PATH, 0, LENGTH(" + alias + ".T_PATH))";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			return "" + alias + ".T_PATH = SUBSTRING(T.T_PATH, 0, LEN(" + alias + ".T_PATH)+1)";
		}
		return "";
	}

	/**
	 * 根据查阅权限获取关联文件的文件ID集合
	 * 
	 * @param fileIdColumn
	 *            文件主键ID (FILE_ID)
	 * @param peopleId
	 * @param projectId
	 * @param isPub
	 *            是否发布
	 * @return String
	 */
	public String getAuthorFileIdsSql(String fileIdColumn, WebLoginBean bean, String isPub) {
		return getAuthorFileIdsSql(fileIdColumn, bean, isPub, 0);
	}

	/**
	 * 根据查阅权限获取关联制度文件的文件ID集合
	 * 
	 * @param fileIdColumn
	 *            文件主键ID (FILE_ID)
	 * @param peopleId
	 * @param projectId
	 * @param isPub
	 *            是否发布
	 * @return String
	 */
	public String getAuthorRuleIdsSql(String fileIdColumn, WebLoginBean bean, String isPub) {
		return getAuthorRuleIdsSqlByAccessType(bean, isPub, 0);
	}

	public String getAuthorRuleIdsSql(String fileIdColumn, WebLoginBean bean, String isPub, int accessType) {
		return getAuthorRuleIdsSqlByAccessType(bean, isPub, accessType);
	}

	private String getAuthorRuleIdsSqlByAccessType(WebLoginBean bean, String isPub, int accessType) {
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isBlank(isPub) && !bean.isAdmin() && !JecnContants.isSystemAdmin) {
			builder.append(" INNER JOIN ").append(
					" (" + getAuthSQLCommon(bean.getProjectId(), TYPEAUTH.RULE, bean, accessType) + ") MY_AUTH_FILES");
			builder.append("    ON MY_AUTH_FILES.id = T.").append("RELATED_ID");
		}
		return builder.toString();
	}

	/**
	 * 获取人员所属单位
	 * 
	 * @param peopleIds
	 * @return
	 */
	public String getOrgNameByPeopleIds(List<Long> peopleIds) {
		return "SELECT JU.PEOPLE_ID, JU.TRUE_NAME, O.ORG_NAME" + "  FROM JECN_USER JU"
				+ " INNER JOIN JECN_USER_POSITION_RELATED JUP" + "    ON JU.PEOPLE_ID = JUP.PEOPLE_ID"
				+ " INNER JOIN JECN_FLOW_ORG_IMAGE OI" + "    ON JUP.FIGURE_ID = OI.FIGURE_ID"
				+ " INNER JOIN JECN_FLOW_ORG O" + "    ON O.ORG_ID = OI.ORG_ID" + " WHERE JU.PEOPLE_ID IN "
				+ JecnCommonSql.getIds(peopleIds);
	}

	public String getAuthorIdsSqlByType(String fileIdColumn, WebLoginBean bean, String isPub, TYPEAUTH type) {
		return getAuthorFileIdsSql(fileIdColumn, bean, isPub, 0);
	}

	public String getAuthorFileIdsSql(String fileIdColumn, WebLoginBean bean, String isPub, int accessType) {
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isBlank(isPub) && !bean.isAdmin() && !JecnContants.isFileAdmin) {
			builder.append(" INNER JOIN ").append(
					" (" + getAuthSQLCommon(bean.getProjectId(), TYPEAUTH.FILE, bean, accessType) + ") MY_AUTH_FILES");
			builder.append("    ON MY_AUTH_FILES.FILE_ID = T.").append(fileIdColumn);
		}
		return builder.toString();
	}

	public String getAuthorIdsSqlByType(String fileIdColumn, WebLoginBean bean, String isPub, TYPEAUTH type,
			int accessType) {
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isBlank(isPub) && !bean.isAdmin() && !JecnContants.isFileAdmin) {
			TmpNodeBean nodeBean = getNodeTable(type);
			String id = nodeBean.getId();
			builder.append(" INNER JOIN ").append(
					" (" + getAuthSQLCommon(bean.getProjectId(), type, bean, accessType) + ") MY_AUTH_FILES");
			builder.append("    ON MY_AUTH_FILES." + id + " = T.").append(fileIdColumn);
		}
		return builder.toString();
	}
}
