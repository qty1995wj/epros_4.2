package com.jecn.epros.server.webBean.integration;

/**
 * 风险清单 控制点相关制度bean
 * 
 * @author fuzhh 2013-12-13
 * 
 */
public class RiskRuleBean {
	/** 制度ID */
	private Long ruleId;
	/** 制度名称 */
	private String ruleName;
	/** 制度责任部门ID */
	private Long ruleOrgId;
	/** 制度责任部门名称 */
	private String ruleOrgName;

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public Long getRuleOrgId() {
		return ruleOrgId;
	}

	public void setRuleOrgId(Long ruleOrgId) {
		this.ruleOrgId = ruleOrgId;
	}

	public String getRuleOrgName() {
		if (ruleOrgName == null) {
			ruleOrgName = "";
		}
		return ruleOrgName;
	}

	public void setRuleOrgName(String ruleOrgName) {
		this.ruleOrgName = ruleOrgName;
	}

}
