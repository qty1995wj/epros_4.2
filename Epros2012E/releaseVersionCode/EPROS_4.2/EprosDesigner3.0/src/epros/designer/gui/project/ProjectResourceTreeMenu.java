package epros.designer.gui.project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

public class ProjectResourceTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(ProjectResourceTreeMenu.class);
	/** 重命名 */
	private JMenuItem renameItem = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon(
			"images/menuImage/rename.gif"));

	private JecnTree jTree;
	private JecnTreeNode selectNode;

	public ProjectResourceTreeMenu(JecnTree jTree, JecnTreeNode selectNode) {
		this.jTree = jTree;
		this.selectNode = selectNode;
		if (JecnDesignerCommon.isAdmin()) {
			this.add(renameItem);// 重命名
			menuItemActionInit();
		}
	}

	private void menuItemActionInit() {
		renameItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renameItemAction();
			}
		});
	}

	private void renameItemAction() {
		if (selectNode == null || jTree == null) {
			return;
		}
		EditProjectNameDialog projectNameDialog = new EditProjectNameDialog(selectNode, jTree);
		projectNameDialog.setVisible(true);
	}
}
