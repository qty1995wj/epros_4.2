<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="attentionStandardSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td height="25px" align="right" width="10%">
								<!-- 标准名称： -->
								<s:text name="standardNameC" />
							</td>
							<td align="left" width="35%">
								<input type="text" style="width: 80%;" id="standardName"
									maxLength="32" />

							</td>

							<td align="right" width="10%">
								<!-- 岗位名称: -->
								<s:text name="positionNameC" />
							</td>
							<td align="left" width="35%">
								<select style="width: 80%;" id="attentionStandard_positionName">
									<option value="-1">
										<s:text name="all" />
									</option>
									<s:iterator value="#session.webLoginBean.listPosBean">
										<option value='<s:property value="posId"/>'>
											<s:property value="posName" />
										</option>
									</s:iterator>
								</select>
							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a onClick="toggle(attentionStandardSearchText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td align="right" height="25px;">
								<!-- 密集: -->
								<s:text name="intensiveC" />
							</td>
							<td align="left">
								<select style="width: 80%;" id="attentionStandard_intensive">
									<option value="-1">
										<s:text name="all" />
									</option>
									<option value="1">
										<s:property value="#application.public"/>
									</option>
									<option value="0">
										${secret}
									</option>
								</select>
							</td>

							<td></td>
							<td></td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="attentionStandardSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="attentionStandarReset()" />
				</div>
			</div>

			<div id="atentionStandardGrid_id">

			</div>
		</div>
	</body>
</html>