package com.jecn.epros.server.action.web.task;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.JecnTaskTestRunFile;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.service.task.design.IJecnTaskRecordService;

/**
 * 下载文件
 * 
 * @author Administrator
 * @date： 日期：Jan 10, 2013 时间：5:55:16 PM
 */
public class DownloadAction extends BaseAction {
	private static final Logger log = Logger.getLogger(DownloadAction.class);
	/** 下载文件临时目录 */
	private String filePath = JecnFinal.getAllTempPath("document");
	/** 文件名称 */
	private String fileName = "EPROSV使用向导-执行者.doc";
	/** 文件ID */
	private String fileId;
	/** 文件类型 */
	private int fileType;

	private IJecnTaskRecordService taskRecordService;
	/** 试运行文件 */
	private JecnTaskTestRunFile taskTestRunFile;
	/** 打开文件文件流 */
	private byte[] content = null;

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getDownloadFile() throws Exception {
		// 获得文件
		if (content == null) {
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(content);
	}

	/**
	 * 打开试运行文件
	 * 
	 * @throws ActionException
	 */
	public String downLoadTaskRunFile() {
		// HttpServletRequest request = org.apache.struts2.ServletActionContext
		// .getRequest();
		// HttpServletResponse response =
		// org.apache.struts2.ServletActionContext
		// .getResponse();
		try {
			// request.setCharacterEncoding("utf-8");
			// response.reset();
			// response.setContentType("application/x-msdownload;charset=GBK");
			// String testRunFileId =
			// this.getRequest().getParameter("testRunFileId");
			if (fileId == null || "".equals(fileId)) {
				File file = new File(getFileName());
				// 获取文件生成的字节数组
				content = getContent();
			} else {
				content = getContent();
				fileName = taskTestRunFile.getFileName();
			}
		} catch (Exception ex) {
			log.error("",ex);
		}
		return "success";
	}

	private byte[] getContent() {
		if (JecnCommon.isNullOrEmtryTrim(fileId)) {
			throw new NullPointerException("打开文件获取getContent 异常！");
		}
		FileInputStream fis=null;
		try {
			switch (fileType) {// 试运行文件
			case 1:
				taskTestRunFile = taskRecordService.getJecnTaskTestRunFile(Long
						.valueOf(fileId.trim()));
				return taskTestRunFile.getBytes();
			default:
				File f1 = new File(filePath + getFileName());
				 fis = new FileInputStream(f1);
				return new byte[fis.available()];
			}
		} catch (NumberFormatException e) {
			log.error("",e);
		} catch (Exception e) {
			log.error("",e);
		}finally {
			if(fis!=null){
				try {
					fis.close();
				} catch (IOException e) {
					log.error("",e);
				}
			}
		}
		return null;
	}

	// 对于配置中的 ${fileName}, 获得下载保存时的文件名
	public String getFileName() {
		try {
			// 中文文件名也是需要转码为 ISO8859-1,否则乱码
			return new String(fileName.getBytes(), "ISO8859-1");
		} catch (UnsupportedEncodingException e) {
			return "impossible.txt";
		}
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public IJecnTaskRecordService getTaskRecordService() {
		return taskRecordService;
	}

	public void setTaskRecordService(IJecnTaskRecordService taskRecordService) {
		this.taskRecordService = taskRecordService;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
}
